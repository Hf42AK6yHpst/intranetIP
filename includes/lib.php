<?php
// Modifying by : 
/**
 * NOTE:
 * 		When copying functions from ec40 lib.php , please place those functions within the if(!$BLOCK_LIB_LOADING)
 *		to prevent duplicated function names being declared. Search "END OF BLOCK" to place function at the end.
 *
 *		Some php pages includes both IP25 & EC40 lib.php which will cause php error.
 */
/********************** Change Log ***********************///
#  Date:     2020-10-14 (Crystal) [ip.2.5.11.11.1]
#			- added SameSite in session cookie
#
#  Date:	2020-10-14 (Henry)
#			- removed the second calling of @session_start()
#
#  Date:    2020-09-24 (Ray)
#			add $sys_custom['StudentAttendance']['RecordBodyTemperature']
#
#  Date:    2020-09-15 (Crystal) [ip.2.5.11.9.1]
#           Reversed change(2020-07-27) : for power speech
#
#  Date:	2020-09-07 (Bill)	[2020-0604-1821-16170]
#			modified UPDATE_CONTROL_VARIABLE(), to store issue right of different notice type
#
#  Date:	2020-08-28 (Bill)	[2020-0812-0939-52066]
#			modified getDecryptedText(), to set expiry minutes for decryption checking	($sys_custom['DecryptTextExpiryMinutes'])
#
#  Date:    2020-07-30 (Henry) [ip.2.5.11.8.1]
#           disable auto redirect function [Case#A115163]
#
#  Date:	2020-09-24 (Cameron)
#			- fix $intranet_session_language, assign value to $_SESSION['intranet_session_language'] so that it can be reassigned at the end when handling session variables [case #S196712]
#
#  Date:	2020-09-18 (Cameron)
#			cast var to array before foreach to suppress error in getSelectSemester2()
#
#  Date:    2020-07-27 (Crystal) [ip.2.5.11.8.1]
#           For temporary only : added checking for disable power speech if using https protocol
#
#  Date:    2020-06-18 (Crystal)
#           Added return in session_is_registered_general(), session_register_general(), session_unregister_general()
#
#  Date:    2020-05-28 (Tommy)
#           modified get_client_region(), added $sys_custom['schoolnews']['ClassTeacherSelectOwnClassStudentOnly'] for zh_TW
#
#  Date:    2020-05-22 (Bill)   [2020-0522-1459-13207]
#			modified output2browserByPath(), to handle output buffer > prevent cannot export file
#
#  Date:    2020-05-21 (Ray)
#     		- modified encrypt_string and in_encrypt_string_blacklist
#
#   Date:	2020-05-08 (Cameron)
# 			- added 'SENTypeConfirmDate_ID' to $____exclude_var_names____
#
#   Date:   2020-05-06 (Tommy)
#           - use $sys_custom['PowerClass_GWP'] to separate parent and student app
#
#   Date:   2020-05-05 (Henry)
#           - Modified block file upload extensions logic
#
#	Date:	2020-05-04 (Bill)	[2020-0407-1445-44292]
#			turn on $sys_custom['eNotice']['ClassTeacherSelectOwnClassStudentOnly'] in taiwan
#
#   Date:   2020-05-04 (Henry)
#           - Modified blockIpWithDifferentRegion() to fix curl problem
#
#   Date:   2020-04-30 (Henry)
#           - Added $____block_file_upload_extensions____, checkIPandAccess() and logBlockFileUploadExtensions()
#
#   Date:   2020-04-28 (Tommy)
#           - modified $favicon_image, change $favicon_image when it is KIS
#
#   Date:   2020-04-27 (Henry)
#           - Added blockIpWithDifferentRegion() and ip_is_private()
#
#   Date:   2020-04-17 (Henry)
#           - bug fix for the variable $____exclude_global_register_var_names____
#
#   Date:   2020-04-16 (Henry)
#           - Define variable $____exclude_global_register_var_names____ to exclude variable register globally
#
#   Date:   2020-03-31 (Pun)
#           - Added InputApplicationID to $____exclude_var_names____
#
#   Date:   2020-02-10 (Thomas)
#           - Turn on PL1 if $sys_custom['power_lesson_extend'] is set even when PL2 is installed
#
#   Date:   2020-02-07 (Thomas)
#           - Fixed the problem of redirecting to a wrong page when forcing SSO to STEM x PL2 central site
#
#	Date:	2020-01-08 (Frankie)
#			- add Define variable (IGNORE_SECURITY_CHECK) for ignoring security convert GET/POST/REQUEST/COOKIES
#
#	Date:	2020-01-07 (Siuwan)	[ip.2.5.11.1.1]
#			- disable PL1 if installed PL2
#
#	Date:	2019-12-23 (Sam)	[2019-1204-1049-37235]
#			- modified generateCsrfToken(), set more_entropy=true to ensure the uniqid can generate an unique token id
#
#	Date:	2019-12-18 (Bill)	[2019-1210-1003-48170]
#			add $sys_custom['SLRS']["enableCancelLesson"] as general function
#
#	Date:	2019-11-29 (Bill)	[2019-1118-1527-06313]
#			- Handle PL20 direct login + PowerClass > PHP error due to implode()
#
#   Date:   2019-11-06 (Ray)
#           - Added logTapCardResults for log
#
#	Date:	2019-10-17 (Philips) [2019-1014-1003-27066]
#			- modified UPDATE_CONTROL_VARIABLE(), Added Check Class Teacher if eClassApp is on
#
#   Date:   2019-09-03 (Thomas)
#           - Added mechanism to handle direct SSO of STEM x PL2 central site
#
#	Date:	2019-8-20 (Carlos)
#			- added 'MerchantUID' to $____exclude_var_names____
#
#	Date:	2019-08-12 (Bill)	[2019-0812-1014-25206]
#			- modified output2browser(), to handle chinese files name when export using MS Edge
#
#	Date:	2019-07-15 (Carlos)
#			- Added aes_cbc_128_encrypt() and aes_cbc_128_decrypt() for card api to encrypt/decrypt data, it matches with C#'s encryption/decryption.
#
#   Date:   2019-7-12 (Anna) [#164934]
#          !!!!!!IF NEED CD after ip.2.5.10.6.1 (1) version, please check case 164934 and deploy with related files!!!
#          dynamic report encrypt related problem
#           - modified getEncryptedText(), remove ignoretimecheking
#
#   Date:   2019-07-10 (Anna) [#164748]
#           -  modified getEncryptedText(), Added $TimeStamp parametr
#
#   Date:   2019-07-09 (Anna) [#164642]
#           - modified getDecryptedText, getEncryptedText() - if have $ignoreTimeChecking, don't add timestamp
#
#   Date:   2019-07-02 (Bill)
#           - added 'FromSubjectID' to $____exclude_var_names____
#
#	Date:	2019-06-24 (Carlos)
#			- added sanitizeEvalInput($value,$removeStatementChars=false) to clean input for eval().
#
#   Date:   2019-06-21 (Bill)   [2019-0618-1504-34066]
#           - modified getHtmlByUrl(), returnSimpleTokenByDateTime(), performSimpleTokenByDateTimeChecking(), to add token for curl target page checking
#
#   Date:   2019-06-11 (Anna)
#           - Modified getDecryptedText(), added parameter $ignoreTimeChecking to ignore timestamp checking
#
#   Date:   2019-06-04 (Anna)
#           - added 'RecipientID' to $____exclude_var_names____
#
#	Date:	2019-06-04 (Carlos)
#			- Added CSRF related functions generateCsrfToken(), cleanCsrfTokenSessions(), verifyCsrfToken() and csrfTokenHtml().
#
#	Date:	2019-06-03 (Carlos)
#			- Added generateCardApiAccessCode($timestamp) to generate a hashed code with timestamp for card api to verify valid access.
#
#   Date:   2019-05-23 (Marco)
#           - modified intranet_auth() to CHECK USER STATUS EVERY 5 MINS
#
#   Date:   2019-05-20 (Bill)
#           - modified getEncryptedText(), getDecryptedText(), getDecryptedAry(), to use $intranet_root instead of $PATH_WRT_ROOT, prevent php error message if $PATH_WRT_ROOT is not defined
#
#	Date:	2019-05-20 (Bill)   [2019-0520-0948-22207]
#			- added 'FromYearTermID', 'ToYearTermID' to $____exclude_var_names____, for SDAS reports
#
#   Date:   2019-05-20  Ivan
#           modified getDecryptedText() to add param $expiryMinute for customized expiry time, use die() instead of throwing exception for coding compatibility
#
#	Date:	2019-05-18  Ryan
#     Security fix:
#     - Added Session check for getEncryptedText(), getDecryptedText()
#
#	Date:	2019-05-18  Ryan
#     Security fix:
#     - Implemented timestamp checking in getEncryptedText(), getDecryptedText()
#			- Added ExpiredContentExpection
#
#	Date:	2019-05-17 (Carlos)
#			changed the default key for getEncryptedText(), getDecryptedText(), getDecryptedAry().
#
#   Date:   2019-05-16 (Bill)
#           modified insertModuleTag(), returnModuleTagIDByTagName(), to prevent SQL Injection - Module Tag Name
#
#	Date:	2019-05-15 (Pun) [161100]
#			- added 'SurveyID' to $____exclude_var_names____, refers to SBA_LS
#
#	Date:	2019-05-08 (Carlos)
#			- added 'activityID' to $____exclude_var_names____, refers to eEnrolment activity attendance api /cardapi/attendance/Attend_csharp.php
# 	Date:	2019-05-07 (Carlos)
# 			- added 'CardID','RFID','HKID','DeviceID' to $____exclude_var_names____, and exclude pattern *SessionID and *LoginID.
#
#   Date:   2019-05-07 (Henry)
#           - added 'ApplicationID' to $____exclude_var_names____
#
#	Date:	2019-05-07 (Carlos)
#			- added 'CategoryID' (Student Attendance > System Settings > eNotice template) to $____exclude_var_names____
#
#   Date:   2019-05-06 (Anna)
#           - added 'YearTermID','form_id' to $____exclude_var_names____
#
#   Date:   2019-05-06 (Henry)
#           - added 'YearClassID' to $____exclude_var_names____
#
#   Date:   2019-05-02 (Henry)
#           - added 'uID' to $____exclude_var_names____
#
#   Date:   2019-04-30 (Anna)
#           - added 'formClassId' to $____exclude_var_names____
#
#   Date:   2019-04-30 (Henry)
#           - added 'BookIDUniqueID' to $____exclude_var_names____
#
#   Date:   2019-04-30 (Anna)
#           - added 'performID', 'commentID','achievementID' to $____exclude_var_names____
#
# 	Date:	2019-04-29 (Carlos)
# 			- handle early PHP (before PHP5.4) auto registered variables for *ID handling.
#	Date:	2019-04-26 (Carlos)
#			- Handle DBTable required parameters [$field,$order,$pageNo,$numPerPage,$page_size,$page_size_change] casting to integer if is set, to prevent sql injection and cross site scripting. (within the block of $BLOCK_LIB_LOADING)
#			- Cast $_POST/$_GET/$_REQUEST/$_COOKIE to integer for variable names that are *ID/_ID/Id/_Id/_id patterns, and values not in patterns (single non-digit character and numbers) such as "U1", "U1,U200,U3000". (within the block of $BLOCK_LIB_LOADING)
#			- Modified IntegerSafe() to preserve the original value type.
#			- Copied from EJ intranet_validateDate($date) for validating date format YYYY-MM-DD
#			- Copied from EJ cleanCrossSiteScriptingCode($value) to remove javascript codes in simple parameters.
#			- Copied from EJ OsCommandSafe() to prevent inject command in shell functions.
#			- Copied from EJ ReturnUrlSafe($url) to handle redirect link to prevent being redirected to external site and redirect to current url indefinitely.
#
#   Date:   2019-03-18 (Philips)
#           added $_SESSION["SSV_PRIVILEGE"]["reportcard_kindergarten"]["is_admin_group_member"]
#
#   Date:   2019-03-07 (Bill)
#           added returnSimpleTokenByDateTime(), performSimpleTokenByDateTimeChecking(), for sample token usage using date time
#
#   Date:   2019-02-27 (Pun)
#           moderified intranet_validateEmail(), added accept '+' for email name
#
#   Date:   2019-02-14 (Isaac)
#           added function convertOverflowNumber() to add '+' to overflow numbers.
#
#   Date:   2019-02-01 (Cameron)
#           add function wordwrapByLength()
#
#	Date:	2019-01-11 (Carlos)
#			added returnTagIDToModuleTagNameByTagID($tagID='', $Module='') for getting an associative array with TagID as key to tag names. Digital Routing is using it to batch get tag names.
#
#	Date:	2018-12-14 (Henry)
#			comment the coding for class_backup checking [Case#W154185]
#
#	Date:	2018-10-11 (Pun)
#			Modified BadWordFilter(), fix display error while bad word contains '/'
#
#	Date:	2018-10-18 (Carlos)
#			Make [Staff Account > Personal Photo] as general function by enabling flags $sys_custom['eAdmin']['AllowStaffPersonalPhotoBatchUpload'] and $special_feature['personal_photo_upload'].
#
#	Date:	2018-09-07 (Carlos)
#			modified intranet_auth(), check against $UserID if being modified and restore it to originally real value.
#
#   Date:   2018-09-04 (Bill)   [2018-0628-1634-01096]
#           modified UPDATE_CONTROL_VARIABLE(), support Kao Yip Class Teacher access    ($sys_custom['eSports']['KaoYipRelaySettings'])
#
#   Date:   2018-08-21 (Cameron)
#           - hardcode $school_name for HKPF in UPDATE_CONTROL_VARIABLE()
#           - hardcode $website for HKPF in get_website()
#           - hardcode $webmaster for HKPF in get_webmaster()
#
#   Date:   2018-08-14 (Cameron)
#           - add parameter $enableUpperCase to intranet_random_passwd()
#
#	Date:	2018-08-08 (Carlos)
#			- Added cleanHtmlJavascript($html) for removing script tags and onXXX event handlers.
#
#   Date:   2018-07-05 (Bill)   [2018-0703-1107-42207]
#           modified my_round(), to cast non numeric to float - PHP 5.4
#
#	Date:	2018-06-29 (Carlos)
#			modified getCkEditor() detect https with checkHttpsWebProtocol().
#
#   Date:   2018-06-11 (Bill)
#		  	modified UPDATE_CONTROL_VARIABLE(), added $_SESSION["SSV_PRIVILEGE"]["reportcard_kindergarten"] for class & subject teacher access right
#
#	Date:	2018-05-08 (Henry)
#			- Enabled $sys_custom['power_lesson_2']['directLoginExceptAdminUser'] to allow user except admin user to direct login PowerLesson 2
#
#	Date:	2018-04-20 (Carlos)
#			- added flag $sys_custom['DisableUseStrongPassword'] to disable strong password policy.
#
#	Date:	2018-04-11 (Carlos)
#			- Do not apply strong password policy if client is KIS.
#
#	Date:	2018-03-20 (Pun) [ip.2.5.9.3.1]
#			- Added function_exists checking for isSecureHttps() and checkHttpsWebProtocol()
#
#	Date:	2018-03-20 (Carlos)
#			- Enabled $sys_custom['SendModuleMailByBcc'] to use strong password policy.
#
#	Date:   2018-02-26 (Thomas)
#           - Allow Teacher to skip PowerLesson 2 direct login customization
#
#   Date:	2018-02-21 (Bill) [ip.2.5.9.4.1]    [2017-0901-1525-31265]
#			- modified UPDATE_CONTROL_VARIABLE(), update eHomework access right for Student & Parent ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_view_access"])
#           - not allow to access eAdmin > eHomework - except Subject Leader
#
#	Date:	2018-01-04 (Carlos)
#			- modified intranet_auth() to check force change password session variable and redirect to change password page.
#
#   Date:   2017-12-20 (Thomas)
#           - exclude PL2 direct login if the request is from /elearningapi
#
#	Date:	2017-12-06 (Cameron) [ip.2.5.9.1.1]
#			- add function getWords() [case #F129849]
#
# 	Date:	2017-11-09 (Thomas) [ip.2.5.9.1.1]
#           - exclude PL2 direct login if the request is from /file
#
# 	Date:	2017-11-09 (Ivan) [ip.2.5.9.1.1]
# 			- copied checkHttpsWebProtocol() from EJ
# 			- modified curPageUrl(), check https by checkHttpsWebProtocol() instead of $_SERVER["HTTPS"]
#
#	Date:	2017-10-30 (Pun) [ip.2.5.9.1.1]
#			- Modified check_powerspeech_is_expired(), generate_texttospeech_ui(),
#               fixed always shows expired alert
#               removed 30 date expired
#
#	Date:	2017-09-26 (Anna) [ip.2.5.8.10.1]
#			- modified UPDATE_CONTROL_VARIABLE() - added check helper in SESSION for enrollment
#
#	Date:	2017-08-25 (Ivan) [ip.2.5.8.10.1]
#			- added downloadAttachmentRelativePathToAbsolutePath() to change "../../../" relative path to absolute path
#
#	Date:	2017-07-11 (Paul) [ip2.5.8.10.1]
#			- Exclude teacher in PowerClass with right in either of modules from the mechanism of PowerLesson 2 portal page redirection
#
#	Date:	2017-08-14 (Cameron) [ip2.5.8.10.1] [case #P121750]
#			- medical caring system preset: showing student log in portal if client has bought student log module
#			- show hotspot icon for medical caring system in portal, link it to receive message if client has bought any module of medical caring system
#
#	Date:	2017-08-04 (Carlos)
#			- Overwrite $_SERVER['REMOTE_ADDR'] with $_SERVER["HTTP_X_REAL_IP"] if is set.
#
#	Date:	2017-07-27 (Cameron)
#			- fix bug: add parameter $presetRowHeight to GetPresetText() so that the row can be shown when Chrome is set to normal display (100%) [case#P120778]
#
#	Date:	2017-07-26 (Carlos)
#			- added isSecureHttps() for checking server is using https or not.
#
#	Date:	2017-07-25 (Cameron)
#			- add parameter $append to GetPresetText() so that it support appending preset text to specific field [case#P120778]
#
#	Date:	2017-07-11 (Thomas) [ip2.5.8.7.1]
#			- Modified the mechanism of PowerLesson 2 portal page redirection
#
#	Date:	2017-07-07 (Anna)
#			- added checkCurrentIP() to check access right
#
#	Date:	2017-05-24 (Anna)
#			- Modified getNameFieldByLang() ,getNameFieldByLang2(), add $displayNickname
#
#	Date:	2017-05-15 (Bill)	[2017-0502-1024-21096]
#			- Modified UPDATE_CONTROL_VARIABLE(), support View Group Member of eReportCard
#
#	Date:	2017-04-28 (Carlos)
#			- Added getRemoteIpAddress() to get client ip address.
#
#	Date:	2017-04-24 (Carlos)
#			- Modified Get_User_Array_From_Common_Choose(), cater DHL common choose target selections.
#
#	Date:	2017-04-20 (Omas) [ip.2.5.8.4.1]
#			- php5.4 global register add $_COOKIE
#
#	Date:	2017-04-10 (Ivan) [ip.2.5.8.4.1] [P115666]
#			- remove related message center admin right if eClass App / Teacher App is disabled
#
#   Date:   2017-04-07 (Thomas)
#           - Set $session_expiry_time = ini_get('session.gc_maxlifetime')
#           - Modified update_login_session(), update the SessionLastUpdated of INTRANET_USER to now()
#
#	Date:	2017-04-03	(Omas)
#			- set timezone for Malaysia client (if zh_MY, then Asia/Kuala_Lumpur)- #L115395
#
#	Date:	2017-03-10 (Carlos)
#			- modified update_login_session(), optimize the query by searching UserID first.
#
#	Date:	2017-02-25 (Cameron)
#			- add parameter $minuteStep to getTimeSel()
#
# 	Date:	2017-02-09 (Cameron)
# 			- modify remove_dummy_chars_for_json(), don't replace back slash for php5.4+
#
#	Date:	2017-01-12	(Paul)
#			- Added getCkEditor(), general functions to call CkEditor
#
#	Date:	2017-01-11	(Bill)	[DM#3136]
#			- Modified my_round(), cast $Decimal to integer when call number_format(), to fix PHP 5.4 error
#
#	Date:	2017-01-10	(Ivan) [ip.2.5.8.1.1] [Z111420]
# 			- fixed $_SESSION["USER_BASIC_INFO"]["is_class_teacher"] to ignore ClassID=0 records
# 			- fixed $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage"] to check the user access right instead of general settings enable status
#
#	Date:	2016-12-23 (Frankie)
#			- Modified UPDATE_CONTROL_VARIABLE() -Add Teacher Portfolio
#
#	Date:	2016-12-01 (Villa) [ip.2.5.8.1.1] [N109600]
#			- Modified get_client_region () -Add date_default_timezone_set ('Asia/Hong_Kong')
#
# 	Date: 	2016-12-01 (Carlos)  [ip.2.5.8.1.1]
#			- Define $REMOTE_ADDR if missing in PHP5.4+
#
#	Date:	2016-10-19 (Pun) [ip2.5.7.10.1]
#			- Added redirect to PowerLesson 2.0 portal page for customization
#
#	Date:	2016-10-04 (Carlos) [ip2.5.7.10.1]
#			- register array type session variables $_SESSION['SESSION_ACCESS_RIGHT'], $_SESSION['SSV_USER_ACCESS'] and $_SESSION['SSV_USER_TARGET'] as global vars.
#
#	Date:	2016-10-03 (Ivan) [ip.2.5.7.10.1]
#			- modified handleFormPost(), normalizeFormParameter() for DR php5.4 handling
#
#	Date:	2016-08-31 (Ivan) [N101966] [ip.2.5.7.10.1]
#			- modified GetCurrentTimeZoneInfo() to fix cannot get timezone for the last day of academic year, change "now()" to "curdate()" in SQL
#
#	Date: 2016-08-26 (Omas) [ip.2.5.7.10.1]
#			- added Get_Array_Last_Key() - for getting array last key
#
#	Date: 2016-08-23 (Ivan) [ip.2.5.7.10.1]
#			- modified UPDATE_CONTROL_VARIABLE() to auto disable app admin if the app flag is disabled
#
#	Date: 2016-08-21 (Jason) [ip.2.5.7.7.1#2, ip.2.5.7.10.1]
# 			- add centOS release differentiation on mac address checking
#
#	Date: 2016-08-03 (Ivan) [ip.2.5.7.10.1]
#			- added getInitialNameField()
#
#	Date: 2016-07-18 (Paul) [ip.2.5.7.7.1]
#			- add flag control for F5 checking
#
#	Date: 2016-07-14 (Ivan) [ip.2.5.7.7.1]
#			- if enabled parent app, auto enable student app too
#
#	Date: 2016-07-05 (Ivan) [ip.2.5.7.7.1]
#			- added $sys_custom['eClassApp']['disableGroupMessage'] to disable group message function for app
#
#	Date: 2016-06-30 (Ivan) [ip.2.5.7.7.1]
#			- added $sys_custom['INTRANET_USER'] = 'INTRANET_USER';
#
#   Date: 2016-06-27 (Cameron) [ip.2.5.7.7.1]
#			- add function remove_dummy_chars_for_json()
#
#	Date: 2016-06-15 (Paul) [ip.2.5.7.7.1]
#			- included new library abuse() to monitor the refresh action by the user
#
#	Date: 2016-05-25 (Paul) [ip.2.5.7.7.1]
#			- modified get_website() - replace $HTTP_SERVER_VARS with $_SERVER as $HTTP_SERVER_VARS is removed in later PHP version and fail to retrieve information now
#
#	Date: 2016-04-26 (Omas)	[ip.2.5.7.7.1]
#			- added output2browserByPath() - Better memory usage for file download
#
#	Date: 2016-04-20 (Omas) [ip.2.5.7.7.1]
#			- modified UPDATE_CONTROL_VARIABLE() - added eProcurement session registry -> $_SESSION['ePCM']
#
#	Date: 2016-04-20 (Carlos) [ip.2.5.7.7.1]
#			- Added start_log() and stop_log(), and perform logging at intranet_opendb() and intranet_closedb() to log down request url, elapsed time and memory usage to file.
#			- Controlled with flag $sys_custom['PerformanceLog'] and $sys_custom['PerformanceLogPath']
#
# 	Date: 2016-04-01 (Pun) [ip.2.5.7.4.1]
# 			- Added session_is_registered_general(), session_register_general(), session_unregister_general() for both intranet and eclass use
#
# 	Date: 2016-02-26 (Siuwan) [ip.2.5.7.3.1]
# 			- modified function get_file_basename(), added second parameter $extension just like general function basename()
#
# 	Date:	2016-02-22 (Cameron) [ip.2.5.7.3.1]
# 			- Add function get_number_from_text();
#
#	Date:	2015-12-17 (Ivan) [ip.2.5.7.1.1]
#			-  add global register $_FILES for PHP5.4 for type and size info of $_FILES
#
#	Date:	2015-12-14 (Ivan) [ip.2.5.7.1.1]
#			- added SSO iCalendar from app cannot access other modules's pages logic
#
#	Date:	2015-12-11 (Paul) [ip.2.5.7.1.1]
#			- modified function check_powerspeech_is_expired() and generate_texttospeech_ui_IP() to make powervoice unseen after expire
#
#	Date:	2015-12-11 (Omas) [ip.2.5.7.1.1]
#			-  add global register $_FILES for PHP5.4
#
#	Date:	2015-12-10 (Cameron) [ip.2.5.7.1.1]
#			- add function getImageFromText()
#
#	Date:	2015-12-10 (Ivan) [ip.2.5.7.1.1]
#			- modified the handling of session $intranet_session_language to fix the cannot change password problem in PHP5.4
#
#	Date:	2015-12-03 (Carlos) [ip.2.5.7.1.1]
#			- Added convertDateTimeToStandardFormat() to format all acceptable datetime input into the standard datetime format YYYY-MM-DD hh:mm:ss .
#
#	Date:	2015-12-01 (Henry) [ip2.5.7.1.1]
#			- Modified trim_array to initialize $b = array();
#
#	Date:	2015-11-30 (Carlos) [ip2.5.7.1.1]
#			- Modified testip($range,$ip), intranet_validateLogin($x), intranet_validateEmail($email, $strict=false), intranet_validateURL($url), intranet_convertURLS($text),
#				intranet_convertMail($text), valid_code($Code), checkSemesterExists($xdate=''), getSelectSemester($tags, $selected="", $ParQuoteValue=1), GET_SCHOOL_NAME(),
#				UPDATE_CONTROL_VARIABLE(), iportfolio_getSBSTitle($course_id, $single_lang=true), getFileExtention($myFile, $isLower=0), replace ereg() with preg_match() and split() with explode() for PHP 5.4.
#
#	Date:	2015-11-24 (Kenneth)
# 			- modify getNameFieldWithClassNumberByLang() and getNameFieldByLang, add flag ($sys_custom['hideTeacherTitle']) to hide teacher title
#
#	Date:	2015-10-30 (Cameron)
# 			- add function escape_double_quotes() - for showing value in input field
#
#	Date:	2015-10-28 (Ivan) [ip.2.5.7.1.1]
#			- modified UPDATE_CONTROL_VARIABLE() to add eRC subject panel session
#
#	Date:	2015-10-13 (Yuen) [ip.2.5.6.10.1]
#			- modified removePotentialAttackChar() to not assign to any variable which is in PHP SESSION
#
#	Date:	2015-09-22 (Cameron)
#			- add function special_sql_str(), pass as keyword string to sql
#
#	Date:	2015-09-04 (Ivan) [ip.2.5.6.10.1]
#			- modified removePotentialAttackChar(), if $_POST and $_COOKIE has the value of a key, do not update $_REQUEST value
#
#	Date:	2015-08-19 (Ivan) [ip.2.5.6.10.1]
#			- modified removePotentialAttackChar(), only update $_REQUEST value if the values of $_GET and $_REQUEST are the same
#
#	Date:	2015-08-13 (Ivan) [ip.2.5.6.7.1(CD1)]
#			- enabled flag of group message, school info and student performance here for general deployment
#
#	Date:	2015-08-12 (Jason) [ip.2.5.6.10.1]
#			- change mysql_db_query() to mysql_query()
#
#	Date:	2015-07-24 (Thomas)
#			Copy function encode_filenames(), function encode_url_for_flashvars(), function encrypt_string(), function decrypt_string() from eClass40
#
#	Date:	2015-07-16 (Ivan)
#			Added function removePotentialAttackChar() to prevent CRLF Injection
#			Deploy: ip.2.5.6.7.1
#
#	Date:	2015-06-16 (Jason)
#			Added session_is_registered_intranet() to replace session_is_registered() completely
#			Added session_register_intranet() to handle session_register() and gobal_register is on by $_SESSION
#			Added session_unregister_intranet() to replace session_unregister() completely
#			Modifed intranet_auth(), update_login_session()
#
#	Date:	2015-06-16 (Ivan)
#			Added logic to replicate the magic_quotes_gpc functionality in PHP 5.4
#
#	Date:	2015-06-08 (Omas)
#			Add Get_String_from_Option_Array()
#
#	Date:	2015-05-18 (Thomas)
#			Add a block of code to transfer $plugin['power_lesson_expiry_date'] to $plugin_expiry['power_lesson']
#
#	Date:	2015-05-15 (Henry)
#			allow edit bulk item purchase price, unit price and quantity for SKH
#
#	Date:	2015-05-04 (Bill)
#			modified GET_ACADEMIC_YEAR2(), fixed PHP 5.3 Warning: substr() expects parameter 2 to be long, string given
#
#	Date:	2015-04-14 (Bill) [2015-0323-1602-46073]
#			modified UPDATE_CONTROL_VARIABLE(), add eNotice PIC checking result to $_SESSION["SSV_PRIVILEGE"]["notice"]["eNoticePIC"]
#
#	Date:	2015-03-24 (Jason)
#			add get_formatted_datetime() to generate a standard date time in different lang
#			Deploy: ip.2.5.6.5.1
#
#	Date:	2015-03-17 (Omas)
#			add function USortCmp3() sortByColumn3(), for sorting of third field
#			Deploy: ip.2.5.6.5.1
#
#	Date:	2015-02-10 (Thomas)
#			Modified function undo_htmlspecialchars(), for $level = 0, put the replacement of '&amp;' to the last item to avoid undo_htmlspecialchars('&amp;quot;') become a '"' instead of '&quot;'
#			Deploy: ip.2.5.6.3.1
#
#	Date:	2015-01-29 (Carlos) Add missing exit() after header("Location: /") to prevent cloud server auto redirect
#
#	Date:	2014-12-11 (Ivan)
#			Modified UPDATE_CONTROL_VARIABLE() to add app group meesaage group admin checking
#			Deploy: ip.2.5.6.1.1
#
#	Date:	2014-11-18 (Carlos)
#			Modified getHtmlByUrl() - added curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:')); to avoid use http header "Expect: 100-continue"
#			Deploy: ip.2.5.5.12.1
#
#	Date:	2014-10-30 (Bill)
#			added $sys_custom['eBooking_ReserveBookingOverwriteAlert'] as general function
#			Deploy: ip.2.5.5.10.1
#
#	Date:	2014-10-23 (Roy)
#			update UPDATE_CONTROL_VARIABLE(), check Class Teacher / Club PIC / Activity PIC send push message right (staff only)
#
#	Date:	2014-10-09 (YatWoon)
#			Revised get_client_region(), Fixed: failed to retrieve settings_region.php setting
#			Requested by Solution, revised the check eclass_update and eclass_backup logic:
#			if no flag setting in settings.php, then check with settings_region.php
#			Deploy: ip.2.5.5.10.1
#
#	Date:	2014-10-07 (YatWoon)
#			Requested by Tony, $special_feature['eclass_update'] and $special_feature['eclass_backup'] on/off are overrided by region settings
#			Deploy: ip.2.5.5.10.1
#
#	Date:	2014-09-29 (Carlos) (ip.2.5.5.10.1)
#			modified intranet_auth(), temporary set a session variable 'REDIRECT_URL' for auto redirection to the request page before login
#
#	Date:	2014-08-28 (Bill)
#			modified GetPresetText() to set target width of preset window
#			version: ip.2.5.5.10.1
#
#	Date:	2014-06-16 (Ivan)
#			added function isKIS()
#			version: ip.2.5.5.8.1
#
#	Date:	2014-06-05 (Henry)
#			added numberToLetter() to convert a number to letter(s) (eg. 1 -> a, 27 -> aa)
#			version: ip.2.5.5.7.1
#
#	Date:	2014-06-03 (Siuwan)
#			modified output2browser(), add checking for Chromes
#
#	Date:	2014-05-30 (Siuwan)
#			copy convert_basename_from_utf8_to_big5(), rename_filename_from_utf8_to_big5() from eclass40
#
#	Date:	2014-05-29 (Carlos)
#			modified intranet_validateEmail($email) to intranet_validateEmail($email,$strict=false), if $strict is true, it does not accept use ip address as the domain name.
#
#	Date:	2014-05-28 (Carlos)
#			copy phpversion_compare() from EJ
#
#	Date:	2014-05-28 (Siuwan) [A62374]
#			add $special_feature['studentpromotion'] as general function
#
#	Date:	2014-04-11 (Ivan) [A61155]
#			commented coding of security checking of "union" "select" "from" as self-account in iPortfolio may really includes these wordings
#
#	Date: 	2013-12-29 (Jason)
#			add $ByPassLibPage and skip to load lib.php completely if it is defined as true (it's used in powerlesson app login /eclass40/loginpl.php)
#
#	Date:	2013-12-18 (YatWoon)
#			update UPDATE_CONTROL_VARIABLE(), change eclass default school logo
#
#	Date:	2013-12-15 (fai)
#			update function UPDATE_CONTROL_VARIABLE for check Medical Access right
#
#	Date:	2013-10-30 (Cameron)
#			added parameter $imail_forwarded_email_splitter
#
#	Date:	2013-09-17 (Siuwan)
#			added fckimage setting $cfg['fck_image']['eNotice'], $cfg['fck_image']['eCircular']
#
#	Date:	2013-08-06 (YatWoon)
#			added function returnSelectedStudentList()
#
#	Date:	2013-07-09 (Ivan)
#			added function getMonthDifferenceBetweenDates()
#
#	Date:	2013-06-17 (Siuwan)
#			Copy getClassNumberForOrderBy() from EJ lib.php
#
#	Date:	2013-05-14 (Siuwan)
#			Copy getFileExtention() from eclass lib-filemanager.php
#
#	Date:	2013-04-19 (Yuen)
#			modified getNameFieldWithClassNumberByLang() to fix the problem of getting empty user name when YearOfLeft or Classname is null
#
#	Date:	2013-04-18 (Jason)
#			add the checking to prevent from access iPortfolio and Normal classroom at the same time
#
#	Date:	2013-03-28 (Jason)
#			add validateIntranetPlugin() to check the expiry data of plugin if set
#
#	Date:	2013-03-28 [Ivan] [2013-0328-1236-41071]
#			modified getAvailableTimetableTemplateWithDaysChecking() so that a 5-day timetable can be applied to timezone with 1, 2, 3, 4 or 5 cycle days now
#
#	Date:	2013-02-08 [Carlos]
#			added isVideoFile() for checking general video file types
#
#	Date:	2013-02-05 [Rita]
#			add $_SESSION["SSV_PRIVILEGE"]["reportcard"]["EnableVerificationPeriod"]
#
#	Date:	2013-01-18 [YatWoon]
#			check the OS and set the flag for export pdf [Case#2013-0117-0950-14066]
#
#	Date:	2012-12-04 [Carlos]
#			comment out unknown usage header header('Vary: Accept-Language'); which cause IE download file fail
#
#	Date:	2012-11-23 [Ivan]
#			modified sortByColumn2(), USortCmp(), URSortCmp() to support second criteria sorting
#
#	Date:	2012-11-20 [Carlos]
#			modified normalizeFormParameter(), replace urldecode() with rawurldecode() because plus sign becomes a space by urldecode()
#
#	Date:	2012-09-05 [YatWoon]
#			modified FormatSmartCardID(), add flag checking $special_feature['SkipFormatSmartCardID'] [case#2012-0905-1901-58147]
#
#	Date:	2012-09-03 [YatWoon]
#			on the flag $plugin['WebSAMS'] for HK client
#
#	Date:	2012-08-30 [YatWoon]
#			update UPDATE_CONTROL_VARIABLE(), add checking parent and child (notice)
#
#	Date:	2012-08-28 [YatWoon]
#			update UPDATE_CONTROL_VARIABLE(), add "UpdateStudentPwdPopUp" checking
#
#	Date:	2012-08-21 [fai]
#			modified f:UPDATE_CONTROL_VARIABLE includes libeclass40.php instead of libeclass.php
#
#	Date:	2012-07-17 [Carlos]
#			modified Get_Attachment_Download_Link() download_attachment.php target to target_e
#
#	Date:  2012-06-27 [Rita Tin]
#		   added getDefaultDateFormat() to format import file's default date.
#
#	Date:	2012-06-20 [Ivan]
#			added function getEncryptedText(), getDecryptedText(), getDecryptedAry() for url encryption and decryption
#
#	Date:	2012-06-19 [Henry Chow]
#			modified UPDATE_CONTROL_VARIABLE(), checking session for SBA
#
#	Date:	2012-06-07 [YatWoon]
#			add $sys_custom['SendModuleMailByBcc'] = true; (requested by CS)
#
#	Date:	2012-06-06 [Carlos]
#			modified intranet_validateEmail() and intranet_convertMail() email regular expression suffix part to accept 2 to 4 characters because some client needs email suffix up to 4 character long
#
#	Date:	2012-05-21 [Ivan] [2012-0516-1551-30066]
#			modified UPDATE_CONTROL_VARIABLE to add session variable $_SESSION["eBooking"]["canAccesseService"] to hide "eService > eBooking" if the user has no right for booking
#
#	Date:	2012-04-24 (Ivan) [2012-0423-1425-24073]
#			modified getHtmlByUrl() to pass all cookies values instead of passing "PHPSESSID" only
#
#	Date:	2012-03-07 (Joe)
#			modified intranet_undo_htmlspecialchars() to handle more speical characters: "&mdash;",  "&ndash;"
#
#	Date:	2012-02-16	[Thomas]
#			Add $skip_PortNo_diff_check to skip the redirection to logout.php in the situation of same IP address but with different port numbers
#
#	Date:	2012-02-10	[YatWoon]
#			update UPDATE_CONTROL_VARIABLE(), missing eHomework plugin checking
#
#	Date:	2012-02-07	[Yuen]
#			prevent the access from different systems which are hosted in the same IP address but with different port numbers

#	Date:	2011-12-19 (Ivan)
#			modified str_lang(), commented echo substr($str, $i-1, 2)."<br />";
#
#	Date:	2011-12-08 (Joe)
#			modified intranet_undo_htmlspecialchars() to handle more speical characters: "&lsquo;",  "&rsquo;", "&ldquo;", "&rdquo;"
#
#	Date:	2011-11-15 (Ivan)
#			added function Get_Standardized_PDF_Table_Data_Display() to cater the "<" and ">" characters
#
#	Date:	2011-11-14	(Marcus)
#			modified Get_String_Display(), add $CustomEmptySymbol
#
#	Date:	2011-11-10	(Carlos)
#			modified output2browser(), Firefox header remove double quote at Content-Disposition: attachment; filename*=utf8''encoded_filename and output filename should be encoded
#
# 	Date:	2011-11-02  (Carlos)
# 			add FormatSmartCardID() for formatting SmartCardID to ten digits pre-padding with zero
#
#	Date:	2011-11-03 (Marcus)
#			added Get_String_Display(), Format string before display e.g. nl2br, htmlspecialchars, display empty Symbol for empty string
#
#	Date:	2011-11-01 (Henry Chow)
#			update displayBubbleMessage(), order by VersionID in descending order
#
#	Date:	2011-10-25 (Marcus)
#			updated getSelectByAssoArray(), modified checking of $selected != "" to $selected !== "" to support 0 as a selected value
#
#	Date:	2011-10-17 (YatWoon)
#			add $special_feature['ePaymentNotice'] as general function
#
#	Date:	2011-10-10 (YatWoon)
#			updated getSelectByAssoArray(), add checking of isset($selected) to prevent value "" and "0" problem
#
#	Date:	2011-10-03 (Marcus)
#			added Get_Dates_By_Repetitive_WeekDay_Within_Range.
#
#	Date:	2011-09-20 (Carlos)
#			added AES_128_Encrypt(), AES_128_Decrypt() and GetDecryptedPassword()
#
#	Date:	2011-08-19 (Ivan)
#			modified getAcademicYearAndYearTermByDate(), return Academic Year Info also
#
#	Date:	2011-08-16 (Marcus)
#			added Get_Attachment_Download_Link(), Get_FileList_By_FolderPath()
#
#	Date:	2011-08-11 (Fai)
#			added breakEncodedURI
#
#	Date:	2011-08-05 (Marcus)
#			added Time_Range_Array_Intersect(), Time_Range_Intersect(),
#
#	Date:	2011-08-02 (Henry Chow)
#			modified returnModuleAvailableTag(), use INNER JOIN instead of LEFT JOIN
#
#	Date:	2011-07-18 (Marcus)
#			added getPeriodOfAcademicYear(), get start/end date of academic year
#
#	Date:	2011-06-21 (Henry Chow)
#			modified removeTagModuleRelation(), typo error of SQL
#
#	Date:	2011-05-23 (Ivan)
#			added function is_date_empty() to check if the date field is not input before
#
#	Date:	2011-05-12 (Henry Chow)
#			added removeTagModuleRelation(), remove relation between "TAG" & Module
#
#	Date:	2011-04-20 (Henry Chow)
#			added createTagModuleRelation(), create relation between "TAG" & Module
#			modified insertModuleTag(), checkTagRelation(), returnModuleTagIDByTagName(), revise the coding for the usage by other Modules
#
#	Date:	2011-04-18 (Henry Chow)
#			added file_size(), display file size in different units
#
#	Date:	2011-04-18 (Carlos)
#			added getParentNameWithStudentInfo2() with option to left pad class number for better sorting
#
#	Date:	2011-04-18 Marcus
#			added intranet_time_diff, calculate diff between 2 time
#
#	Date:	2011-04-14	Henry Chow
#			modified UPDATE_CONTROL_VARIABLE(), for eAdmin > Dis, should check Section access right rather than just check Module access right since student can also be assigned to access right group (eService > eDis also stored in Module "DISCIPLINE")
#
#	Date:	2011-04-07	YatWoon
#			add function returnRemarkLayer()
#
#	Date:	2011-04-06 (Marcus)
#			add function Get_Plural_Display(), add s/es for noun
#
#	Date:	2011-03-30 (YatWoon)
#			add function chopString(), countString()
#
# Date	:	2011-03-28	[YatWoon]
#			add function get_website(), get_webmaster()
#
# Date	:	2011-03-21 [Henry Chow]
#			update UPDATE_CONTROL_VARIABLE(), store access right checking on "Offical Photo"
#
# Date	:	2011-03-16 (Henry Chow)
#			modified returnMinute(), display every 1 minute if $displayEveryMinute=1
#
# Date	:	2011-03-15 (YatWoon)
#			add vairable $NoUTF8 to skip charset=utf-8.  Used in /help/gb/
#
# Date	:	2011-03-09 (Yuen)
#			added get_client_region() for region of current client
#
# Date	:	2011-03-04 (Henry Chow)
#			added asorti(), case in-sensitive sorting of asort()
#
# Date	:	2011-03-02 (YatWoon)
#			update $system_reserved_account, remove "amanda" from the list (requested by CS & Solution with case 2011-0301-1328-01067)
#
# Date	:	2011-03-01 [Marcus]
#			modified getSelectByAssoArray to support optgroup.
#				- pass $ary[$optionGroupName][$val][$display] to use optgroup
#				- pass $ary[$val][$display] to use the old logic
#			added function array_depth to return the depth of a array tree.
#
# Date	:	2011-02-25 [Ivan]
#			modified Get_Last_Modified_Remark added para $DisplayText to display own last updated text
#
# Date	:	2011-02-22 [Yuen]
#			modified returnSelection() to support OPTGROUP
#
# Date	:	2011-02-22 [Henry Chow]
#			update UPDATE_CONTROL_VARIABLE(), revise the checking on eClass version to session
#
# Date	:	2011-02-21 [Henry Chow]
#			update displayBubbleMessage(), revise the checking on IP version when display "eclass bubble"
#
# Date	:	2011-02-21 [Henry Chow]
#			update Get_IP_Version(), remove "global" to $versions and use "include" instead of "inclide_once"
#
# Date	:	2011-02-17 [Henry Chow]
#			add checkDateIsValidFor2Format(), valid the date format "YYYY/MM/DD' and "YYYY-MM-DD"
#
# Date	:	2011-02-14 [Henry Chow]
#			update Get_IP_Version(), set "global" to $versions
#
# Date	:	2011-02-11 [YatWoon]
#			move the updateGetCookies(), clearCookies() to lib-extend.php
#
# Date	:	2011-02-10 [Kenneth Chung]
#			Added Get_Common_Search_User_List, for IP25 standard search/ select list
#
# Date	:	2011-02-09 [Henry Chow]
#			modified UPDATE_CONTROL_VARIABLE(), store the Module version into $_SESSION
#
# Date	:	2011-02-09 [Ivan]
#			modified parseFileName, added to remove special characters "\" and "|" in the filename also as the file system does not support these two characters
#
# Date	: 	2011-02-06 [Marcus]
#		  	modified BuildMultiKeyAssoc, added param $BuildNumericArray
#
# Date	: 	2011-01-31 [Henry Chow]
#		  	add getEclassModuleVersionInUse() & updateEclassModuleVersion(0, for "eclass" update (by client)
#
# Date	: 	2011-01-25 [Ivan]
#		  	modified UPDATE_CONTROL_VARIABLE(), added $_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["PageAccessRightArr"] for Admin Group Access Right info
#
# Date	:	2011-01-21 (YatWoon)
#			update $system_reserved_account, remove "mail" from the list (requested by Solution with case 2011-0121-1438-05035)
#
# Date	:	2011-01-18 (Ivan)
#			added Get_User_Array_From_Common_Choose() to get the UserID Array after adding users from the "common_choose" pop up
#
# Date 	:	2011-01-10 (Henry Chow)
#			added showWarningMsg(), display the message with "Legend"
#
# Date 	:	2010-12-31 Marcus
#			added build_duplicate_file_name, build file name like abc(2).jpg, abc(3).jpg by passing filename and seq no.
#
# Date 	:	2010-12-29 Marcus
#			modified getSelectByAssoArray, allow multiple selected value
#
# Date	:	2010-12-14 YatWoon
#		 Check force default lang, Customization [PJ-2010-1331]
#
# Date	: 2010-12-10 [Ivan]
#		  modified UPDATE_CONTROL_VARIABLE(), added $_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["is_class_teacher"] and $_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["is_subject_teacher"] for class teacher ans subject teacher access right
#
# Date	: 2010-12-08 [Henry Chow]
#		  added remove_item_by_value(), remove item from array
#
# Date	: 2010-11-23 [Henry Chow]
#		  added insertModuleTag(), returnModuleTagIDByTagName(), returnModuleTagIDByTagName(), returnModuleAvailableTag(), return the "TAG"
#
#				modified GET_CSV() added para $AppendUnicodeSuffix to control the auto append of "unicode" wordings in the csv filename
#
# Date	: 2010-11-02 [Yuen]
#				disabled the include of customized language file as this doesn't work (instead, should include customized lang once after loading default lang file)
#
#
# Date	: 2010-10-15 [Marcus]
#				add getNameFieldInactiveUserIndicator, if ClassName OR ClassNumber is empty in INTRANET_USER, add a symbol(default ^) next to name_field
#
# Date	: 2010-10-11 [Ivan]
#				Modified function getParentNameWithStudentInfo(), added para $includeParentWithoutStudent to include the namefield of the parent who is not linked to any students (default: not include)
#
# Date  : 2010-10-11 [Marcus]
#				Added function Get_Sequence_Number(), 1 > 1st, 2 > 2nd, 11 > 11th, 223 > 223rd ....etc.
#
# Date  : 2010-10-08 [Kenneth Chung]
#				Added function Get_IP_Version(), to get current IP source code version
#
#	Date	:	2010-10-06 [YatWoon]
#				if current user identity cannot select lang icon, then MUST display with lang which set in admin console
#
#	Date	: 	2010-10-05 [Ronald]
#				new function : getAcademicYearByAcademicYearID($year='',$ParLang='en')
#				used to get AcademicYearName by passing an AcademicYearID, if AcademicYearID is empty, then will get CurrentAcademicYearID
#
#	Date	:   2010-09-27 [FAI]
#               new function f: DetectDataEncoding() copy from /home/web/eclass40/eclass40/src/includes/php/lib.php
#
#	Date 	: 	2010-09-13 [Marcus]
#				update returnGroupAvailableFunctions, default enable photo album, question bank
#
#	Date	:	2010-09-10 [YatWoon]
#				update getNameFieldForRecord(), getNameFieldByLang()
#				Don't display Title, but display TitleEnglish or TitleChinese
#
#	Date	:	2010-09-09	[Marcus]
#				modified getSelectByAssoArray, add intranet_htmlspecialchars to  $element['value']
#
#	Date	:	2010-09-07	[Marcus]
#				fixed output2browser, download attachment, spaces in name will not be replaced by '_', added chrome case for browser handling.
#
#	Date	:	2010-09-03	[YatWoon]
#				Check last language selection by user and default display with last lang selectioni
#
#	Date	:	2010-09-02	[Carlos]
#				Add function Get_Gamma_Identity_Access_Rights to be called at home portal to control showing iMail Gamma icon
#
#	Date 	: 	2010-09-01 	[Marcus]
#				modifed DownloadAttachment , add param $custom_name
#
#	Date 	: 	2010-09-01 	[Kenneth Chung]
#				Add function Get_Previous_Academic_Year_ID to get previous academic year
#
#	Date 	: 	2010-08-18 	[Marcus]
#				added valid_code to check whether a code start with letter.
#
#	Date 	: 	2010-08-05 	[Kelvin]
#				modified gen_online_help_btn_and_layer , support GB version
#
#	Date 	: 	2010-08-05 	[Sandy]
#				modified $eclass_root , set it to eclass40 root path (instead of pointing to eclass30 root path)
#
#	Date	:	2010-08-03	[YatWoon]
#				improve: getCurrentSemester(), can return specific lang of semester data
#
#	Date 	:	2010-07-30 [Marcus]
#	Details	:	modified BuildMultiKeyAssoc Add Param SingleValue , return Single Value instead of an array
#
#	Date 	:	2010-06-22 [Henry Chow]
#	Details	:	update UPDATE_CONTROL_VARIABLE(), check whether staff can access "eService>eDiscipline" to view his/her own class
#
#	Date 	:	2010-06-21 [YatWoon]
#	Details	:	update gen_online_help_btn_and_layer(), don't display anything if the data not exists in array
#
#	Date 	:	2010-06-21 [Ivan]
#	Details	:	modified my_round() to handle very small number like "-1.9388995891205E-05"
#
#	Date 	:	2010-06-04 [Ivan]
#	Details	:	modified UPDATE_CONTROL_VARIABLE() to add eEnrol and eRC admin checking
#
#	Date 	:	2010-06-02 [Yuen]
#	Details	:	add $NoLangWordings and don't load lang files if it is defined as true (it's used in /login.php)
#
#	Date 	:	2010-05-31 [Henry Chow]
#	Details	:	add $sys_custom['Discipline_ShowRemarkInPersonalReport'] for all clients
#
#	Date 	:	2010-05-17 [Kenenth Chung]
#	Details	:	modified gen_online_help_btn_and_layer, which makes the $current_lang parameter as non-neccesary field
#	Date 	:	2010-05-14 [Marcus ]
#	Details	:	modified getSelectByArray, cater passing an array of  selected options for multiple selection box
#
#	Date 	:	2010-05-04 [Henry Chow]
#	Details	:	add $plugin['eHomework'] for eHomwork
#
#	Date 	:	2010-04-21 [Henry Yuen]
#	Details	:	add $cfg['fck_image'] for dynamic report template
#
#	Date 	:	2010-04-13 [YatWoon]
#	Details	:	update UPDATE_CONTROL_VARIABLE(), Clear user upload files via fckeditor while user login
#				add settings for fck editor
#
#	Date 	:	2010-04-12 [YatWoon]
#	Details	:	add flag for session timeout warning, $special_feature['TimeOutByJS'] = true;
#
#	Date 	:	2010-04-09 [Kelvin]
#	Details	:	include file ($intranet_root."/includes/ip_help_config.php");
#				add function gen_online_help_btn_and_layer($module,$module_function,$current_lang) for online help
#
#	Date 	:	2010-04-07 [Max]
#	Details	:	add function getAYNameByAyId()
#
#	Date 	:	2010-03-30 [Marcus]
#	Details	:	add function sortByColumn2 and its sub-function USortCmp,URSortCmp
#
#   Date 	:	2010-03-26 [Kelvin]
#	Details	:	add $plugin['ScrabbleFC'] in $access2specialrm session variable
#
#   Date 	:	2010-02-19 [Marcus]
#	Details	:	add BuildMultiKeyAssoc($DBReturnAry, $AssocKeys)
#				build assoc array from array, returned by db, by providing one or more db field name as $AssocKeys
#
# 	Date	:	2010-02-05 [Shing]
#	Details	:	update GetPresetText()
#				prevent a possible popup display problem on first load in IE7.
#
# 	Date	:	2009-12-31 [YatWoon]
#	Details	:	update UPDATE_CONTROL_VARIABLE()
#				update the access right of student
#
# 	Date	:	2009-12-11 [Ivan]
#	Details	:	update getSelectAcademicYear()
#				added parameter $pastAndCurrentYearOnly to show past academic year for selection only (for copy subject group)
#
# 	Date	:	2009-12-04 [YatWoon]
#	Details	:	update intranet_opendb()
#				check with $sys_custom['MySQL_Server_Host'] to decide connect to localhost or external mysql db server
#
/******************* End Of Change Log *******************/
//ob_start();

$____exclude_global_register_var_names____ = array('setting_path_ip_rel','kis_admission_school');

if(!$ByPassLibPage)
{


if(!$NoUTF8)		# for /help/gb/
{
	header('Content-type: text/html; charset=utf-8');
}
if ($sys_custom['project']['HKPF']) {
    // cannot include the site in iframe
    header('X-Frame-Options: DENY');
}
//header('Vary: Accept-Language');

#########################################################################
# Customized functions to be put in standard package
$sys_custom['SmartCardAttendance_Report_Search'] = true;
$sys_custom['Student_Attendance_Entry_Log'] = true;
$sys_custom['Discipline_show_AP_converted_data'] = true;
$sys_custom['Discipline_ShowRemarkInPersonalReport'] = true;
if (!isset($sys_custom['SmartCardAttendance_Teacher_ShowPhoto']))
{
	$sys_custom['SmartCardAttendance_Teacher_ShowPhoto'] = true;
}
$sys_custom['SendModuleMailByBcc'] = true;
$sys_custom['StudentAttendance']['RecordBodyTemperature'] = true;
if($plugin["platform"]!="KIS" && !$sys_custom['DisableUseStrongPassword']){
	$sys_custom['UseStrongPassword'] = true;
}
$special_feature['circular'] = true;
$special_feature['imail_richtext']=true;
$special_feature['announcement_approval'] = true; # Announcement Approval
$special_feature['TimeOutByJS'] = true;		# Sesstion Timeout warning msg 2010-04-12 [YatWoon]
$special_feature['ePaymentNotice'] = true;	# used to control the payment notice in ePayment > System properties
$special_feature['studentpromotion'] = true;	# Promotion System in admin console
$intranet_version="3.0";

# allow edit bulk item purchase price, unit price and quantity for SKH
if ($sys_custom['eInventoryCustForSKH'])
{
	$special_feature['eInventory']['CanEditBulkQty'] = true;
	$sys_custom['eInventoryCanEditBulkInvoicePrice']  = true;
}

if ($plugin['SLRS'])
{
    $sys_custom['SLRS']["NoAvailTeachers"] = true;
    $sys_custom['SLRS']["disallowSubstitutionAtOneDate"] = false;
    $sys_custom['SLRS']["dailyReportWithAssignedTeacherOption"] = true;
    $sys_custom['SLRS']["allowSubstitutionRemarks"] = true;

    // [2019-1210-1003-48170]
    $sys_custom['SLRS']["enableCancelLesson"] = true;
}

if (!$BLOCK_LIB_LOADING){
	if(!function_exists('isSecureHttps')){
		function isSecureHttps()
		{
			if (
				( !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
				|| ( !empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
				|| ( !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on')
				|| (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == 443)
				|| (isset($_SERVER['HTTP_X_FORWARDED_PORT']) && $_SERVER['HTTP_X_FORWARDED_PORT'] == 443)
				|| (isset($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https')
			) {
				return true;
			} else {
				return false;
			}
		}
	}

	if(!function_exists('checkHttpsWebProtocol')){
		function checkHttpsWebProtocol()
		{
			global $web_protocol, $sys_custom;

			if((isset($web_protocol) && $web_protocol == "https") || ($sys_custom["HTTP_USED"] == "https://") || isSecureHttps())
			{
				return true;
			}

			return false;
		}
	}
}
# iMail-specific params
$imail_feature_allowed['mail_search'] = true;
$imail_feature_allowed['bad_words_filter'] = true;
$imail_bad_words_replacement = "***";
$imail_forwarded_email_splitter = "**";

## Once eBooking module is ready and user use eBooking instead of ResourcesBooking,
## Please remove $plugin['ResourcesBooking']  and add eBooking plugin setting in settings.php
$plugin['ResourcesBooking'] = true;

# eHomework > "Homework not submitted" report plugin checking
$plugin['eHomework'] = true;

# disable PL1 if installed PL2
if(
    !(isset($sys_custom['power_lesson_extend']) && $sys_custom['power_lesson_extend']) &&
    isset($plugin['power_lesson']) && $plugin['power_lesson'] != '' &&
    isset($plugin['power_lesson_2']) && $plugin['power_lesson_2'] != ''
) {
    $plugin['power_lesson'] = false;
}

# Dislay STRN field in user info
$special_feature['ava_strn'] = true;

# default to allow SMS if sms plugin is setup
$sys_custom['send_sms'] = true;

$special_feature['ava_hkid'] = true; # Dislay HKID field in user info
$special_feature['ava_strn'] = true; # Dislay STRN field in user info

# default allow reserve booking overwrite alert
$sys_custom['eBooking_ReserveBookingOverwriteAlert'] = true;

# default to enable OLE websams export
$sys_custom['iPf']['Report']['OLE']['WebSAMSReport'] = true;

if ($plugin['ReadingScheme'])
{
	$sys_custom['ReadingGarden']['PopularAwardScheme'] = true;
}

# eAdmin > Account Mgmt > Staff Account > Personal Photo
$sys_custom['eAdmin']['AllowStaffPersonalPhotoBatchUpload'] = true;
$special_feature['personal_photo_upload'] = true;

$sys_custom['SendModuleMailTraceLog'] = true;

// [2020-0407-1445-44292] enable in Taiwan
if (function_exists('get_client_region')) {
	if(get_client_region() == 'zh_TW') {
		$sys_custom['eNotice']['ClassTeacherSelectOwnClassStudentOnly'] = true;
	}
}

//include_once($intranet_root."/includes/libdb.php");
//include_once($intranet_root."/includes/libuser.php");

# check the OS and set the flag for export pdf
$os = trim(shell_exec("/bin/cat /etc/redhat-release"));
if(strpos($os, "CentOS release 5.")!==false)
	$plugin["pdf_RCRE_support_bypass"] = true;

if ($plugin['medical']) {
	if ($plugin['medical_module']['studentLog'] && !isset($plugin['medical_module']['homepageHint']['studentLog'])) {
		$plugin['medical_module']['homepageHint']['studentLog'] = true;
	}
	if (!isset($plugin['medical_module']['message'])) {
		$plugin['medical_module']['message'] = true;
	}
}

$sys_custom['INTRANET_USER'] = 'INTRANET_USER';

#########################################################################
# settings for fck editor
#########################################################################
$cfg['fck_image']['SchoolNews'] = "UserUploadFiles_SchoolNews";
$cfg['fck_image_prefix'][$cfg['fck_image']['SchoolNews']] = "SN";
$cfg['fck_image']['eNotice'] = "UserUploadFiles_eNotice";
$cfg['fck_image_prefix'][$cfg['fck_image']['eNotice']] = "N";
$cfg['fck_image']['eCircular'] = "UserUploadFiles_eCircular";
$cfg['fck_image_prefix'][$cfg['fck_image']['eCircular']] = "C";
$cfg['fck_image']['SchoolInfo'] = "UserUploadFiles_SchoolInfo";
$cfg['fck_image_prefix'][$cfg['fck_image']['SchoolInfo']] = "SI";
# Begin Henry Yuen (2010-04-21): fck image for dynamic report template
$cfg['fck_image']['DynamicReport'] = "UserUploadFiles_DynamicReport";
$cfg['fck_image_prefix'][$cfg['fck_image']['DynamicReport']] = "DR";
# End Henry Yuen (2010-04-21): fck image for dynamic report template
#########################################################################


#########################################################################
# settings for eClass App [start]
#########################################################################
if (!$sys_custom['eClassApp']['disableGroupMessage']) {
	$sys_custom['eClassApp']['groupMessage'] = true;
}
$sys_custom['eClassApp']['schoolInfo'] = true;
$sys_custom['eClassApp']['studentPerformance'] = true;

if(!$sys_custom['PowerClass_GWP']){
    if ($plugin['eClassApp']) {
    	$plugin['eClassStudentApp'] = true;
    }
}
#########################################################################
# settings for eClass App [end]
#########################################################################


$favicon_image = "favicon.ico";
if ($sys_custom["favicon"]!="")
{
	$favicon_image = $sys_custom["favicon"];
}

#########################################################################
# settings, new for IP2.5
#########################################################################
# Student Photo dimension default
$default_photo_width = 100;
$default_photo_height = 130;
#########################################################################
if(intranet_phpversion_compare('5.4') !== 'ELDER' && !$BLOCK_LIB_LOADING){
	if(checkHttpsWebProtocol()){
	$cookie_lifetime = ini_get('session.cookie_lifetime');
	$cookie_path = ini_get('session.cookie_path').';SameSite=none';

	session_set_cookie_params($cookie_lifetime, $cookie_path, $_SERVER['HTTP_HOST'], 1, 1);
}
}

@session_start();
if (!$NOT_KEEP_ALIVE)
{
	$_SESSION['UserLastAccessTime'] = time();
}
include_once("lib-extend.php");

#########################################################################
# IP2.5 Constant Variables
#########################################################################
# User Type
define("USERTYPE_STAFF",		"1");
define("USERTYPE_STUDENT",		"2");
define("USERTYPE_PARENT",		"3");
define("USERTYPE_ALUMNI",		"4");

# Others
define("TOP_MENU_MODE_eService",	"0");
define("TOP_MENU_MODE_eADMIN",		"1");
#########################################################################


# To compensate missing variable in client site
$eclass_root = $eclass40_filepath;

# layout skin since IP 2.5
$LAYOUT_SKIN = "2020a";

if (file_exists($PATH_WRT_ROOT.'includes/libuserbrowser.php') && !isset($userBrowser))
{
	include_once($PATH_WRT_ROOT.'includes/libuserbrowser.php');
	$userBrowser = new browser();
}

# initialize private key if not set in settings.php
if (!isset($special_feature['ParentApp_PrivateKey']) || $special_feature['ParentApp_PrivateKey']=="")
{
	$special_feature['ParentApp_PrivateKey'] = "ebcrloaasdslearning";
}


# sample
//$sys_custom['elib_block_user'] = array(233, 226, 113, 104, 266, 270, 283, 285);
if (isset($sys_custom['elib_block_user']) && in_array($_SESSION["UserID"], $sys_custom['elib_block_user']))
{
	$plugin['eLib'] = false;
	$_SESSION["SSV_PRIVILEGE"]["plugin"]["eLib"] = 0;
}

# prevent from access iPortfolio with correct classroom session info
$server_script_name = str_replace('//','/',$_SERVER['SCRIPT_NAME']);

if(isset($ck_room_type) && $ck_room_type <> 'iPORTFOLIO' &&
	preg_match('/^'.str_replace('/','\\/',$intranet_httppath).'\/home\/portfolio\//', $server_script_name) > 0
) {
	session_unregister('ck_room_type');		# must run this code, otherwise, it would fall into redirect loop.
	header('location:'.$intranet_httppath.'/home/portfolio/');
	exit();
}

# security check
// commented for case [A61155]
//if (isset($_POST) && sizeof($_POST)>0)
//{
//	$QueryStrArr = $_POST;
//	$QueryStrArr["FORM_METHOD"] = "POST";
//} elseif (isset($_GET) && sizeof($_GET)>0)
//{
//	$QueryStrArr = $_GET;
//	$QueryStrArr["FORM_METHOD"] = "GET";
//}
//
//
//if (isset($QueryStrArr) && sizeof($QueryStrArr)>1)
//{
//	foreach ($QueryStrArr as  $VarKey => $QueryStrVariable)
//	{
//		$QueryStrVariable = strtoupper($QueryStrVariable);
//
//		# avoid SQL Injection using UNION syntax
//		if (strstr($QueryStrVariable, "UNION") && strstr($QueryStrVariable, "SELECT ") && strstr($QueryStrVariable, "FROM"))
//		{
//			# rewrite to avoid execution in DBMS
//			$_POST[$VarKey] = str_ireplace("union", "<label>u</label>nion", $QueryStrVariable);
//			${$VarKey} = $_POST[$VarKey];
//
//			# log the attempt
//			include_once("$intranet_root/includes/libfilesystem.php");
//
//			$lf = new libfilesystem();
//			$logfile_path = $intranet_root."/file/"."sql_injection_attempt.log";
//
//			$logfile_content = @$lf->file_read($logfile_path);
//			if ($logfile_content!="")
//			{
//				# load previous records
//				$logfile_entry_arr = unserialize($logfile_content);
//			} else
//			{
//				$logfile_entry_arr = array();
//			}
//
//			# add new record
//			$logfile_entry_arr[] = array("UserID"=>$UserID, "IP"=>$REMOTE_ADDR, "Script"=>$_SERVER["REQUEST_URI"], "Datetime"=>date("Y-m-d H:i:s"), "Content"=>$QueryStrVariable);
//			//var_dump($logfile_entry_arr);
//			@$lf->file_write(serialize($logfile_entry_arr), $logfile_path);
//		}
//	}
//}


if (!$BLOCK_LIB_LOADING)
{


# Hardcode BroadlearningNews
$BroadlearningCheckNews = false;

/*
 * function for
 * 		- registering session in general on all php versions
 * 		- replacing the session_register() and support php 5.4+
 */
function session_is_registered_intranet($session_name){
	if($session_name != ''){
		return (!isset($_SESSION[$session_name])) ? false : true;
	}
	return false;
}
function session_is_registered_general($session_name){
	return session_is_registered_intranet($session_name);
}

function session_register_intranet($session_name, $session_value){
	if($session_name != ''){
		$_SESSION[$session_name] = $session_value;
		global ${$session_name};
		$$session_name = $session_value;
	}
}
function session_register_general($session_name, $session_value){
	return session_register_intranet($session_name, $session_value);
}

function session_unregister_intranet($session_name){
	if($session_name != ''){
		unset($_SESSION[$session_name]);
		global $$session_name;
   		unset($$session_name);
	}
}
function session_unregister_general($session_name){
	return session_unregister_intranet($session_name);
}

/*
 * function for replicating the magic_quotes_gpc functionality in PHP5.4
 */
if (function_exists('get_magic_quotes_gpc') && !get_magic_quotes_gpc()) {
	if (!function_exists('magicQuotes_addslashes')) {
		function magicQuotes_addslashes(&$value, $key) {
			$value = addslashes($value);
		}
	}

    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, 'magicQuotes_addslashes');
}

if (!function_exists('removePotentialAttackChar')) {
	function removePotentialAttackChar(&$value, $key) {
		// remove potential attack char in $_GET
		$attackCharAry = array(chr(13), chr(10), '">', '\'>', '<!--');
		$value = str_replace($attackCharAry, '', strip_tags($value));
		//$value = htmlspecialchars($value);

		// assign $_GET value to global register variable and $_REQUEST if required
		global ${$key};
		if (isset($_POST[$key]) || isset($_COOKIE[$key])) {
			// ignore if the key has value in $_POST or $_COOKIES
		}
		else if (isset($_GET[$key]) && !isset($_SESSION[$key])){
			global $____exclude_global_register_var_names____;
			if(!in_array($key,$____exclude_global_register_var_names____)){
				// Assign value of $_GET into global register variable
				${$key} = $value;
				// Assign value of $_GET into $_REQUEST
				$_REQUEST[$key] = $value;
			}
		}
	}
}
$gpc = array(&$_GET);
array_walk_recursive($gpc, 'removePotentialAttackChar');

// prevent UserID being changed
if(session_is_registered_intranet("UserID") && session_is_registered_intranet("LOGIN_INTRANET_SESSION_USERID")){
	global $UserID;
	if(!is_null($UserID) && ($UserID != $_SESSION["LOGIN_INTRANET_SESSION_USERID"] || $_SESSION['UserID'] != $_SESSION["LOGIN_INTRANET_SESSION_USERID"])){
		$UserID = $_SESSION["LOGIN_INTRANET_SESSION_USERID"];
		$_SESSION['UserID'] = $_SESSION["LOGIN_INTRANET_SESSION_USERID"];
	}
}

/**
 * Function for Start Timer
 * @return boolean
 */
function StartTimer($variableName="") {

        $mtime = microtime();
        $mtime = explode(" ",$mtime);
        $mtime = $mtime[1] + $mtime[0];

        if ($variableName!="")
        {
        	global ${$variableName};
        	${$variableName} = $mtime;
        } else
        {
        	global $timestart;
        	$timestart = $mtime;
        }

        return true;
}

/**
 * Function for Stop Timer
 * @param number $precision Precision of output time
 * @return number Time
 */
function StopTimer($precision=5, $NoNumFormat=false, $variableName="") {
        if ($variableName!="")
        {
        	global ${$variableName};
        	$timestart = ${$variableName};
        } else
        {
        	global $timestart;
        }
        $mtime = microtime();
        $mtime = explode(" ",$mtime);
        $mtime = $mtime[1] + $mtime[0];
        $timeend = $mtime;
        $timetotal = $timeend-$timestart;
    	$scripttime = ($NoNumFormat==false) ? number_format($timetotal,$precision) : $timetotal;
        return $scripttime;
}

function debug() {
        $xStr = "";
        for ($i=0; $i<func_num_args(); $i++)
        {
                $xStr .= func_get_arg($i)." ";
        }
        echo "<i>[debug]</i><br>\n".$xStr."\n<br>\n";
}

function hdebug() {
        $xStr = "";
        for ($i=0; $i<func_num_args(); $i++)
        {
                $xStr .= func_get_arg($i)." ";
        }
        echo "<!--\n[debug]\n".$xStr."\n-->\n";
}

function debug_r($arr) {
        echo "<i>[debug]</i>\n<PRE>\n";
        var_dump($arr);
        echo "\n</PRE>\n<br>\n";
}

function hdebug_r($arr) {
        echo "<!--\n[debug]\n";
        var_dump($arr);
        echo "\n-->\n";
}

function debug_pr($arr) {
        echo "<i>[debug]</i>\n<PRE>\n";
        print_r($arr);
        echo "\n</PRE>\n<br>\n";
}

function hdebug_pr($arr) {
        echo "<!--\n[debug]\n";
        print_r($arr);
        echo "\n-->\n";
}

function debug_rt($resultSet)
{

	$noOfResult = sizeof($resultSet);
	$HeaderTitleArr  = array();
	$titleStack = array();
	$htmlTitleRow  = "";
	$noOfField  = 0;
	$htmlData  = "";
	if($noOfResult != 0)
	{
		$HeaderTitleArr = array_keys($resultSet[0]);

		$htmlTitleRow .= "<tr>";
		foreach ($HeaderTitleArr as $HeaderTitle)
		{

			if(is_int($HeaderTitle)>0)
			{

			}
			else
			{
				$htmlTitleRow .= '<th>'."\n";
				$htmlTitleRow .= trim($HeaderTitle);
				$htmlTitleRow .= '</th>'."\n";

				$titleStack[] = $HeaderTitle;
			}

		}
		$noOfField = sizeof($titleStack);
		$htmlTitleRow .= "</tr>";

		for($i  = 0; $i <$noOfResult;$i++)
		{
			//DISPLAY EACH ROW
//			$htmlData  .= "<tr>";
			$htmlData  .= '<tr  onmouseover="this.style.backgroundColor=\'#eeeeee\';" onmouseout="this.style.backgroundColor=\'#FFF\';">';
			for($j =0;$j < $noOfField;$j++)
			{
				//DISPLAY EACH DATA
				$_data = $resultSet[$i][$titleStack[$j]];
				$htmlData .= "<td>".$_data."&nbsp;</td>";
			}
			$htmlData  .= "</tr>";
		}

		$htmlSummary = "<tr><td colspan = \"".$noOfField."\">Total Record : ".$noOfResult."</td></tr>";
	}else{
		$htmlSummary = '<tr><td><font color = "blue">No record</font></td></tr>';
	}

//	$htmlResult = "<div class=\"move_drag\"><table border = \"1\">";
	$htmlResult = "<div class=\"\"><table border = \"1\">";
	$htmlResult .=  $htmlTitleRow;
	$htmlResult .=  $htmlData;
	$htmlResult .=  $htmlSummary;
	$htmlResult .=  "</table></div>";
	echo  $htmlResult;
}

# read file function
function get_file_content($file){

		clearstatcache();
        if(file_exists($file) && is_file($file) && filesize($file)!=0){
                $x =  ($fd = fopen($file, "r")) ? fread($fd,filesize($file)) : "";
                if ($fd)
                    fclose ($fd);
        }

        return $x;
}

function write_file_content($body, $file)
{
        $x = (($fd = fopen($file, "w")) && (fputs($fd, $body))) ? 1 : 0;
        if ($x)
            fclose ($fd);
        return $x;
}

// to replace the usage of basename() in php function
function get_file_basename($file,$extension=""){
	$returnStr = '';
	if($file != ''){
		$tmp = explode('/', $file);
		$returnStr = trim($tmp[count($tmp)-1]);
		if(!empty($extension)){
			$returnStr = trim(str_replace($extension,"",$returnStr));
		}
	}
	return $returnStr;
}

global $extra_setting;
if ($extra_setting['show_process_time'])
{
	$memory_init2 = memory_get_usage();
}
@StartTimer("GLB_StartTime");


# server checking
$ip_changed_err_msg = "The server configuration has been modified.<br>Please contact <a href=\"mailto:support@broadlearning.com\">BroadLearning Technical Services</a>.";
$expiry_msg = "This system is already expired. If you have any problems, <br>please contact <a href=\"mailto:support@broadlearning.com\">BroadLearning Technical Services</a>.";
//if($HTTP_SERVER_VARS["SERVER_ADDR"] != $server_ip) die($ip_changed_err_msg);

if(!$sys_custom['skip_MAC_check'])
{
	# Check MAC Address
	exec("/sbin/ifconfig | grep Ethernet",$output_lines);
	exec("cat /etc/redhat-release", $output_lines_1);

	$mac_matched = false;
	for ($i=0; $i<sizeof($output_lines); $i++)
	{
		if(strpos($output_lines_1[0], "release 7") !== false){
			# for CentOS7
			$mac_tmp = explode(" ether ", $output_lines[$i]);
			$MACaddr = substr($mac_tmp[1], 0, 17);
		} else {
			# for CentOS5
	     	$MACaddr = substr($output_lines[$i], -17);
		}
	     if ($MACaddr == $irc_MAC)
	     {
	         $mac_matched = true;
	         break;
	     }
	}
	if (!$mac_matched)
	{
	     # Send Mail
	     die($ip_changed_err_msg);
	}
}

# Check Expiry Date
$today = date('Y-m-d');
if ($expiry != "" && $today > $expiry)
{
	# report to central server
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$CurrentScript = urlencode($_SERVER["REQUEST_URI"]);
	curl_setopt($ch, CURLOPT_URL, "http://ip25-tc-demo2.eclass.com.hk/api/school_report.php?ProcessTime=999&TS=".time()."&uid=".$_SESSION['UserID']."&SchoolURL=".urlencode($_SERVER['HTTP_HOST'])."&Script=SysExpired&UserName=".urlencode($UserName));
	// grab URL and pass it to the browser
	$data_from_checking = curl_exec($ch);

    die($expiry_msg);
}

# Check Restricted IP address
function testip($range,$ip)
{
         $result = 1;

         # IP Pattern Matcher
         # J.Adams <jna@retina.net>
         #
         # Matches:
         #
         # xxx.xxx.xxx.xxx        (exact)
         # xxx.xxx.xxx.[yyy-zzz]  (range)
         # xxx.xxx.xxx.xxx/nn     (nn = # bits, cisco style -- i.e. /24 = class C)
         #
         # Does not match:
         # xxx.xxx.xxx.xx[yyy-zzz]  (range, partial octets not supported)


         //if (ereg("([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)/([0-9]+)",$range,$regs))
         if (preg_match("/([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)\/([0-9]+)/i",$range,$regs))
         {

             # perform a mask match
             $ipl = ip2long($ip);
             $rangel = ip2long($regs[1] . "." . $regs[2] . "." . $regs[3] . "." . $regs[4]);

             $maskl = 0;

             for ($i = 0; $i< 31; $i++)
             {
                  if ($i < $regs[5]-1)
                  {
                      $maskl = $maskl + pow(2,(30-$i));
                  }
             }

             if (($maskl & $rangel) == ($maskl & $ipl))
             {
                  return 1;
             }
             else
             {
                 return 0;
             }
         }
         else
         {

             # range based
             //$maskocts = split("\.",$range);
             //$ipocts = split("\.",$ip);
			 $maskocts = explode(".",$range);
             $ipocts = explode(".",$ip);

             # perform a range match
             for ($i=0; $i<4; $i++)
             {
                //if (ereg("\[([0-9]+)\-([0-9]+)\]",$maskocts[$i],$regs))
                if (preg_match("/\[([0-9]+)\-([0-9]+)\]/i",$maskocts[$i],$regs))
                {
                    if ( ($ipocts[$i] > $regs[2]) || ($ipocts[$i] < $regs[1]))
                    {
                       $result = 0;
                    }
                }
                else
                {
                    if ($maskocts[$i] <> $ipocts[$i])
                    {
                        $result = 0;
                    }
                }
             }
         }
         return $result;
}
if(isset($_SERVER['PHP_AUTH_USER']) && !isset($PHP_AUTH_USER)) $PHP_AUTH_USER = $_SERVER['PHP_AUTH_USER'];
if(isset($_SERVER['PHP_AUTH_PW']) && !isset($PHP_AUTH_PW)) $PHP_AUTH_PW = $_SERVER['PHP_AUTH_PW'];
if(!isset($REMOTE_ADDR) || $REMOTE_ADDR == '')
{
	// missing global registered variable in PHP5.4+
	if(isset($_SERVER["HTTP_X_REAL_IP"]) && $_SERVER["HTTP_X_REAL_IP"] != '')
	{
		$REMOTE_ADDR = $_SERVER["HTTP_X_REAL_IP"];
		$_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_X_REAL_IP"];
	}else{
		$REMOTE_ADDR = $_SERVER['REMOTE_ADDR'];
	}
}
if (isset($blocked_ip) && is_array($blocked_ip))
{
    for ($i=0; $i<sizeof($blocked_ip); $i++)
    {
         if (testip($blocked_ip[$i],$REMOTE_ADDR))
         {
             die("Your access is not allowed. Please make sure that you are not using proxy or contact System Administrators.");
         }
    }
}

# browser checking
/*
$isIE = (strpos($HTTP_USER_AGENT, "MSIE")) ? 1 : 0;
$version = (int) substr($HTTP_USER_AGENT, strpos($HTTP_USER_AGENT, "/")+1, 1);
if($version < 4) die();
*/

# page size
$page_size = 50;

# single source code for multiple schools
//$setting_path_ip_rel = str_replace("/", "", str_replace(":", "_", str_replace("http://", "", $_SERVER["HTTP_HOST"])));
$setting_path_ip_rel = $kis_admission_school ? $kis_admission_school : str_replace("/", "", str_replace(":", "_", str_replace("http://", "", $_SERVER["HTTP_HOST"])));

if (file_exists("$intranet_root/{$setting_path_ip_rel}"))
{
	# multiple sites
	$file_path = "$intranet_root/{$setting_path_ip_rel}";
	//debug($file_path);
} else
{
	$file_path = $intranet_root;
}

$default_wrap_length = 30;
$default_chop_length = 10;
$session_expiry_time = ini_get('session.gc_maxlifetime') / 60; // minutes
$eclass_linkup_time = 30; // seconds

# set default since Sept 2012
if (!isset($intranet_authentication_method))
{
	$intranet_authentication_method = "HASH";
}
$default_password_salt = "1ekfx1";
if ($intranet_password_salt=="")
{
	$intranet_password_salt = $default_password_salt;
}

# Reserved Account name
$system_reserved_account = array (
"adm","admin","apache","bin","canna","daemon","eclass","eclassadm","ftp",
"games","gdm","gopher","halt","ident","junior","ldap","lp","mailman",
"mailnull","mysql","named","netdump","news","nfsnobody","nobody","nscd","ntp",
"operator","pcap","postfix","postgres","privoxy","root","rpc","rpcuser",
"rpm","shutdown","smmsp","squid","sshd","sync","uucp","vcsa","webalizer","webmailadm","wnn","xfs"
,"irc","imail","ifolder","postmaster","sys"
);

if (!isset($intranet_default_lang_set))
{
     $intranet_default_lang_set = array("b5");
}

# default language

if ($_SESSION['intranet_default_lang']=="")
{
	$langID = get_file_content($intranet_root."/file/language.txt");
	if ($langID == "") $langID = 1;
	switch ($langID){
	        case 0: $intranet_default_lang = "en"; break;
	        case 1: $intranet_default_lang = "b5"; break;
	        case 2: $intranet_default_lang = "gb"; break;
	        default: $intranet_default_lang = "$intranet_default_lang"; break;
	}
	$_SESSION['intranet_default_lang'] = $intranet_default_lang;
}



# includes of plugins conf file

# Open webmail
if ($plugin['webmail'])
{
    if (is_file("$intranet_root/plugins/webmail_conf.php"))
    {
        include ("$intranet_root/plugins/webmail_conf.php");
    }
    else
    {
        $plugin['webmail'] = false;
    }
}

# Open gamma
if ($plugin['imail_gamma'])
{
    if (is_file("$intranet_root/plugins/gamma_mail_conf.php"))
    {
        include ("$intranet_root/plugins/gamma_mail_conf.php");
        $sys_custom['iMailPlus']['QuotaAlert'] = true;
    }
    else
    {
        $plugin['imail_gamma'] = false;
    }
}

# SLS Library
if ($plugin['library'])
{
    if (is_file("$intranet_root/plugins/sls_conf.php"))
    {
        include ("$intranet_root/plugins/sls_conf.php");
    }
    else
    {
        $plugin['library'] = false;
    }
}

# SMS
if ($plugin['sms'])
{
    if (is_file("$intranet_root/plugins/sms_conf.php"))
    {
        include ("$intranet_root/plugins/sms_conf.php");
    }
    else
    {
        $plugin['sms'] = false;
    }
}

# SchoolPlus
if ($plugin['octopus'])
{
    if (is_file("$intranet_root/plugins/oct_conf.php"))
    {
        include ("$intranet_root/plugins/oct_conf.php");
    }
    else
    {
        $plugin['octopus'] = false;
    }
}

# Central Question Bank
if ($plugin['QB'])
{
    if (is_file("$intranet_root/plugins/qb_conf.php"))
    {
        include ("$intranet_root/plugins/qb_conf.php");
    }
    else
    {
        $plugin['QB'] = false;
    }
}

# Aerodrive
if ($plugin['aerodrive'])
{
    if (is_file("$intranet_root/plugins/aero_conf.php"))
    {
        include ("$intranet_root/plugins/aero_conf.php");
    }
    else
    {
        $plugin['aerodrive'] = false;
    }
}

if ($plugin['personalfile'])
{
    if (is_file("$intranet_root/plugins/file_conf.php"))
    {
        include ("$intranet_root/plugins/file_conf.php");
    }
    else
    {
        $plugin['personalfile'] = false;
    }
}

if ($plugin['tv'])
{
    if (is_file("$intranet_root/plugins/tv_conf.php"))
    {
        include ("$intranet_root/plugins/tv_conf.php");
    }
    else
    {
        $plugin['tv'] = false;
    }
}

if ($plugin['attendancestudent'])
{
    if (is_file("$intranet_root/plugins/attendst_conf.php"))
    {
        include ("$intranet_root/plugins/attendst_conf.php");
    }
    else
    {
        $plugin['attendancestudent'] = false;
    }
}
if ($plugin['payment'])
{
    if (is_file("$intranet_root/plugins/payment_conf.php"))
    {
        include ("$intranet_root/plugins/payment_conf.php");
    }
    else
    {
        $plugin['payment'] = false;
    }
}
if ($plugin['SchoolNet'])
{
    if (is_file("$intranet_root/plugins/schoolnet_conf.php"))
    {
        include ("$intranet_root/plugins/schoolnet_conf.php");
    }
    else
    {
        $plugin['SchoolNet'] = false;
    }
}

if ($plugin['ServiceMgmt'])
{
    if (is_file("$intranet_root/plugins/service_mgmt_conf.php"))
    {
        include ("$intranet_root/plugins/service_mgmt_conf.php");
    }
    else
    {
        $plugin['ServiceMgmt'] = false;
    }
}


if ($special_feature['alumni'])
{
    if (is_file("$intranet_root/plugins/alumni_conf.php"))
    {
        include ("$intranet_root/plugins/alumni_conf.php");
    }
    else
    {
        $special_feature['alumni'] = false;
    }
}


if ($intranet_authentication_method == 'LDAP')
{
    if (is_file("$intranet_root/plugins/ldap_conf.php"))
    {
        include ("$intranet_root/plugins/ldap_conf.php");
    }
    else
    {
        $intranet_authentication_method = 'Normal';
    }
}

if (is_file("$intranet_root/plugins/system_api_conf.php"))
{
    include ("$intranet_root/plugins/system_api_conf.php");
}
else
{
    $ex_api['system'] = false;
}

# session management
if ($eclass_middle_inclusion)
{
    $intranet_session_language = $intranet_default_lang;
}
else
{
    //@session_start();
    //if(!session_is_registered_intranet("intranet_session_language") || $intranet_session_language=="")
    if(!session_is_registered_intranet("intranet_session_language") || $_SESSION['intranet_session_language']=="")
    {
    	session_register_intranet("intranet_session_language", $intranet_default_lang);
        //@session_register("intranet_session_language");
        //$intranet_session_language = $intranet_default_lang;
    }
    else {
    	session_register_intranet("intranet_session_language", $_SESSION['intranet_session_language']);
    }
}

// 	### check user last lang
if($_SESSION['LastLang'] != "" && !empty($_SESSION['UserID']))
{
	$intranet_session_language = $_SESSION['LastLang'];
	session_register_intranet("intranet_session_language", $intranet_session_language);
}


##### 2010-10-06
##### if current user identity cannot select lang icon, then MUST display with lang which set in admin console
$UserType = $_SESSION['UserType'];
if(isset($sys_custom['hide_lang_selectioin'][$UserType]) && $sys_custom['hide_lang_selectioin'][$UserType] && !isset($sys_custom['default_lang_cust'][$UserType]) && !($sys_custom['hide_lang_selectioin_except_school_admin'] && $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]))
{
	$intranet_session_language = $intranet_default_lang;
}

if (isset($_SESSION['intranet_hardcode_lang']) && $_SESSION['intranet_hardcode_lang'] != '') {
	//$intranet_session_language = $intranet_hardcode_lang;
	session_register_intranet("intranet_session_language", $_SESSION['intranet_hardcode_lang']);
}

// to be compatible in php 5.4 or above
$_SESSION['intranet_session_language'] = $intranet_session_language;

# may support different schemes in the future
if (!isset($PORTAL_SCHEME) || $PORTAL_SCHEME == "")
{
     $PORTAL_SCHEME = "s1";
}
$image_path_portal = "/images/portal_{$PORTAL_SCHEME}/$intranet_session_language";
if (file_exists("$intranet_root/templates/Portal_Css_{$PORTAL_SCHEME}.php"))
{
        include_once("$intranet_root/templates/Portal_Css_{$PORTAL_SCHEME}.php");
}

# HARDCODE -- Mark Big5 Only
#$intranet_session_language = "b5";

# session authentication
function intranet_auth(){
        global $UserID, $PATH_WRT_ROOT;

        // prevent UserID being changed
		if(session_is_registered_intranet("UserID") && session_is_registered_intranet("LOGIN_INTRANET_SESSION_USERID")){
			if(!is_null($UserID) && ($UserID != $_SESSION["LOGIN_INTRANET_SESSION_USERID"] || $_SESSION['UserID'] != $_SESSION["LOGIN_INTRANET_SESSION_USERID"])){
				$UserID = $_SESSION["LOGIN_INTRANET_SESSION_USERID"];
				$_SESSION['UserID'] = $_SESSION["LOGIN_INTRANET_SESSION_USERID"];
			}
		}

        if(!session_is_registered_intranet("UserID") || $UserID=="")
        {
        	// disable auto redirect function [Case#A115163]
//        	if(isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != ''){
//        		$uri_parts = explode('?',$_SERVER['REQUEST_URI']);
//        		$request_uri = $uri_parts[0]; // abandom the query string
//        		$exclude_urls = array('/','/templates','/templates/','/templates/index.php','/home','/home/','/home/index.php','/index.php','/login.php');
//        		if(!in_array($request_uri,$exclude_urls)){
//        			// this session variable REDIRECT_URL will be used at /templates/index.php and assign to DirectLink
//        			$_SESSION['REDIRECT_URL'] = $_SERVER['REQUEST_URI'];
//        		}
//        	}
            header("Location: /");
            exit();
        }else if(session_is_registered_intranet("UserID") && isset($_SESSION['FORCE_CHANGE_PASSWORD']) && $_SESSION['FORCE_CHANGE_PASSWORD']){
        	if(isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != ''){
        		$uri_parts = explode('?',$_SERVER['REQUEST_URI']);
        		$request_uri = $uri_parts[0]; // abandom the query string
	        	// force user to change password
	    		if(!in_array($request_uri,array('/home/iaccount/account/login_password.php','/home/iaccount/account/login_password_update.php','/lang.php'))){
	    			header("Location:/home/iaccount/account/login_password.php");
	    			exit;
	    		}
        	}
        }

		// CHECK USER STATUS EVERY 5 MINS
		if (is_null($_SESSION["userstatus_checking"]))
		{
			$_SESSION["userstatus_checking"] = time();
		}
		if (time() - (int) $_SESSION["userstatus_checking"] > 5*60)
		{
			if (!is_null($PATH_WRT_ROOT))
			{
				include_once($PATH_WRT_ROOT."includes/libdb.php");
			} else
			{
				include_once("libdb.php");
			}
			$li = new libdb();

			// check status
			$sql = "SELECT RecordStatus FROM INTRANET_USER WHERE UserID='".$_SESSION['UserID']."'";
			intranet_opendb();
			$sql_row_result = $li->returnResultSet($sql);
			unset($li);

			# force to logout if not active status
			if ($sql_row_result[0]['RecordStatus'] != "1")
			{
				header("Location: /logout.php?tte=".$sql_row_result[0][0]);
				exit();
			} else
			{

				// update last check time
				$_SESSION["userstatus_checking"] = time();

				//echo "testing msg: NEW check on ". $date_time_now;
			}
		}
		else
		{
			// no need to check
			//echo "testing msg: last check on ". date('m/d/Y h:i:s a', $_SESSION["userstatus_checking"]);
		}

        update_login_session();
}

# Update Login session in database
# Not database independent, use MySQL only for less code change
function update_login_session()
{
         global $UserID, $iSession_LoginSessionID,$intranet_db, $DO_NOT_LOG_PERFORMANCE;
         if(!session_is_registered_intranet("UserID") || $UserID=="" || !session_is_registered_intranet("iSession_LoginSessionID") || $iSession_LoginSessionID=="")
         {
             return;
         }
         else
         {

             $DO_NOT_LOG_PERFORMANCE = 1;
             intranet_opendb();

             $sql = "UPDATE ".$intranet_db.".INTRANET_LOGIN_SESSION SET DateModified = now() WHERE UserID='$UserID' AND LoginSessionID='$iSession_LoginSessionID'";
             $log_success = mysql_query($sql);

             $sql = "UPDATE ".$intranet_db.".INTRANET_USER SET SessionLastUpdated = now() WHERE UserID = '$UserID'";
             mysql_query($sql);

             intranet_closedb();
             $DO_NOT_LOG_PERFORMANCE = 0;
         }
}


# cache function
function intranet_cache(){
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");                                // Date in the past
        header("Last-Modified:".gmdate("D, d M Y H:i:s")." GMT");                // always modified
        header("Cache-Control: no-cache, must-revalidate");                                // HTTP/1.1
        header("Pragma: no-cache");                                                                                // HTTP/1.0
}

# database function
function intranet_opendb()
{
        global $intranet_db_user, $intranet_db_pass, $sys_custom;

		start_log();

        $db_host = ($sys_custom['MySQL_Server_Host']=="")? "localhost" : $sys_custom['MySQL_Server_Host'];
        $result = mysql_connect($db_host , $intranet_db_user, $intranet_db_pass);

        mysql_query("set character_set_database='utf8'");
		mysql_query("set names utf8");
}

function intranet_closedb(){
		$memory_used = number_format((memory_get_usage())/1024/1024, 0);

		if ($memory_used>50)
		{
			$DB_Connecting = @mysql_ping();

			if ($DB_Connecting)
			{

				# log the record for reference including $UserID, $Path, Time
				global $UserID, $intranet_root;

				include_once($intranet_root."/includes/libdb.php");

				$server_script_name = str_replace('//','/',$_SERVER['SCRIPT_NAME']);

				$libdb = new libdb();

				$LogType = "MEMORY";
				$sql = "INSERT INTO INTRANET_SYSTEM_LOG (UserID, Script, LogType, LogValue, LogText, DateInput) VALUES ('{$UserID}', '".addslashes($server_script_name)."', '{$LogType}', '{$memory_used}', NULL, now()) ";

				$libdb->db_db_query($sql);
			}

		}
        @mysql_close();

        stop_log();
}

# string function

function intranet_validateLogin($x) {
         global $system_reserved_account;
         if (is_array($system_reserved_account))
         {
             if (in_array($x,$system_reserved_account))
             {
                 return false;
             }
         }
         global $special_option;
         if ($special_option['login_hypen'])
         {
             //$myRE="^[a-z]([_a-z0-9-]{0,30})$";
             $myRE="/^[a-z]([_a-z0-9-]{0,30})$/";
         }
         else
         {
             //$myRE="^[a-z]([_a-z0-9]{0,30})$";
             $myRE="/^[a-z]([_a-z0-9]{0,30})$/";
         }
         //return ereg($myRE,$x);
         return preg_match($myRE,$x);
         #return eregi("^[_a-z0-9]+$", $x);
}

function intranet_validateEmail($email, $strict=false) {
        //return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
        //return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z0-9-]{2,3})$", $email);
        //return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z".($strict?"":"0-9-")."]{2,4})$", $email);
        return preg_match("/^[_a-z0-9-\+]+(\.[_a-z0-9-\+]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z".($strict?"":"0-9-")."]{2,4})$/i", $email);
}

// Format: YYYY-MM-DD
function intranet_validateDate($date)
{
    $matches = array();
    if(!preg_match('/^(\d\d\d\d)-(\d\d)-(\d\d)$/',$date, $matches)){
        return false;
    }

    $year = $matches[1];
    $month = $matches[2];
    $day = $matches[3];

    $ts = mktime(0,0,0,intval($month),intval($day),intval($year));
    $date2 = date('Y-m-d',$ts);

    return $date == $date2;
}

function intranet_validateURL($url) {
        //return eregi("^((ht|f)tp://)((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)$", $url);
        return preg_match("/^((http|ftp|https):\/\/)((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((\/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)$/i", $url);
}

function intranet_convertURLS($text) {
        #$text = eregi_replace("((ht|f)tp://www\.|www\.)([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})((/|\?)[a-z0-9~#%&\\/'_\+=:\?\.-]*)*)", "http://www.\\3", $text);
        #$text = eregi_replace("((ht|f)tp://)((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)", "<a class=tableContent href=\"\\0\" target=_blank>\\0</a>", $text);
        //$text = ereg_replace("[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]","<a target=_blank href=\"\\0\">\\0</a>", $text);
        $text = preg_replace("/[[:alpha:]]+:\/\/[^<>[:space:]]+[[:alnum:]\/]/i","<a target=_blank href=\"\\0\">\\0</a>", $text);
        return $text;
}

function intranet_convertMail($text) {
        //$text = eregi_replace("([_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))", "<a class=tableContent href='mailto:\\0'>\\0</a>", $text);
        //$text = eregi_replace("([_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4}))", "<a class=tableContent href='mailto:\\0'>\\0</a>", $text);
        $text = preg_replace("/([_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4}))/i", "<a class=tableContent href='mailto:\\0'>\\0</a>", $text);
        return $text;
}

function intranet_convertAllLinks($text) {
        $text = intranet_convertURLS($text);
        $text = intranet_convertMail($text);
        return $text;
}

function intranet_undo_htmlspecialchars($string){
        $string=str_replace("&amp;", "&", $string);
        $string=str_replace("&quot;", "\"", $string);

        $string=str_replace("&lt;", "<", $string);
        $string=str_replace("&gt;", ">", $string);
        $string=str_replace("&#039;","'",$string);

        $string=str_replace("&lsquo;","'",$string);
		$string=str_replace("&rsquo;","'",$string);
		$string=str_replace("&ldquo;","\"",$string);
		$string=str_replace("&rdquo;","\"",$string);

		$string=str_replace("&mdash;","-",$string);
		$string=str_replace("&ndash;","-",$string);

        return $string;
}
function intranet_htmlspecialchars($string)
{
         # For unicode handling
         $string = str_replace("&amp;#", "&#", htmlspecialchars($string));
         $string = str_replace("'","&#039;",$string);
         return $string;
}

function intranet_handle_url ($url)
{
         $url = str_replace(" ", "%20", $url);
         $url = str_replace("&#", urlencode("&#"), $url);
         return $url;
}

function displayArrow(){
        global $image_path;
        # $x = "<img src=$image_path/dotted_arrow.gif border=0 width=20 height=15 hspace=5 vspace=0>";
        $x = "<img src=$image_path/arrow.gif border=0 align=absmiddle hspace=5>";
        return $x;
}

function newIcon(){
        global $image_path;
        # $x = "<img src=$image_path/new_icon.gif border=0 hspace=5 vspace=0>";
        $x = "<img src=$image_path/admin/icon_new.gif border=0 hspace=1 vspace=0 align=absmiddle>";
        return $x;
}

function quickAddIcon(){
        global $image_path;
        $x = "<img src=$image_path/quickadd_icon.gif border=0 hspace=5 vspace=0 align=absmiddle>";
        return $x;
}

function printIcon(){
        global $image_path;
        $x = "<img src=$image_path/admin/icon_print.gif border=0 hspace=1 vspace=0 align=absmiddle>";
        return $x;
}

function analysisIcon(){
        global $image_path;
        $x = "<img src=$image_path/admin/icon_analyze.gif border=0 hspace=1 vspace=0 align=absmiddle>";
        return $x;
}

function importIcon(){
        global $image_path;
        # $x = "<img src=$image_path/import_icon.gif border=0 hspace=5 vspace=0>";
        $x = "<img src=$image_path/admin/icon_import.gif border=0 hspace=1 vspace=0 align=absmiddle>";
        return $x;
}

function importIcon2()
{
         global $image_path;
         $x = "<img src=$image_path/import_icon.gif border=0>";
         return $x;
}

function exportIcon(){
        global $image_path;
        # $x = "<img src=$image_path/export_icon.gif border=0 hspace=5 vspace=0>";
        $x = "<img src=$image_path/admin/icon_export.gif border=0 hspace=1 vspace=0 align=absmiddle>";
        return $x;
}

function exportIcon2()
{
         global $image_path;
         $x = "<img src=$image_path/export_icon.gif border=0>";
         return $x;
}

function exportIcon3()
{
     global $image_path;

     $x = "<img src='{$image_path}/2007a/icon_export.gif' width='18' height='18' border='0' align='absmiddle' >";

     return $x;
}

function emailIcon(){
        global $image_path;
        # $x = "<img src=$image_path/email_icon.gif border=0 hspace=5 vspace=0>";
        $x = "<img src=$image_path/admin/icon_email.gif border=0 hspace=1 vspace=0 align=absmiddle>";
        return $x;
}

function newFolderIcon(){
        global $image_path;
        $x = "<img src=$image_path/new_folder_icon.gif border=0 hspace=5 vspace=0 align=absmiddle>";
        return $x;
}

function plannerViewIcon(){
        global $image_path;
        $x = "<img src=$image_path/planner_view_icon.gif border=0 hspace=5 vspace=0 align=absmiddle>";
        return $x;
}

function listViewIcon(){
        global $image_path;
        $x = "<img src=$image_path/list_view_icon.gif border=0 hspace=5 vspace=0>";
        return $x;
}

function groupIcon(){
        global $image_path;
        $x = "<img src=$image_path/admin/password.gif border=0 hspace=5 vspace=0 align=absmiddle>";
        return $x;
}
function detailIcon(){
        global $image_path;
        $x = "<img src=$image_path/admin/icon_detail.gif border=0 hspace=1 vspace=0 align=absmiddle>";
        return $x;
}

function searchBarSpacer(){
        global $image_path;
        $x = "<img src=$image_path/space.gif width=50 height=10 border=0 hspace=0 vspace=0>";
        return $x;
}

function toolBarSpacer(){
        global $image_path;
        //$x = "<img src=$image_path/space.gif width=5 height=10 border=0 hspace=0 vspace=0>";
        return $x;
}

function jumpIcon()
{
         global $image_path;
         $x .= "<img src=$image_path/groupinfo/jump.gif width=32 height=18 border=0>";
         return $x;
}

function newIcon2()
{
         global $image_path;
         $x .= "<img border=0 src='$image_path/icon_new.gif'>";
         return $x;
}

function getStepIcons($total, $target,$alt)
{
         global $i_Step_Name;
         $x = "$i_Step_Name";
         for ($i=1; $i<=$total; $i++)
         {
              $off = ($i==$target? "":"_off");
              $text = $alt[$i];
              $x .= "<img src=/images/step/step$i$off.gif border=0 alt=\"$text\">";
         }
         return $x;
}

function bulletinIcon($altText="")
{
         global $image_path;
         $x .= "<img border=0 src=$image_path/icon_bulletin.gif border=0 alt=\"$altText\">";
         return $x;
}

function btnSearch()
{
        global $intranet_session_language, $image_path;
        return "<input type='image' src='$image_path/admin/button/s_btn_find_$intranet_session_language.gif' border='0'>";
}

function btnSubmit()
{
        global $intranet_session_language, $image_path;
        return "<input type='image' src='$image_path/admin/button/s_btn_submit_$intranet_session_language.gif' border='0'>";
}

function btnExport()
{
        global $intranet_session_language, $image_path;
        return "<img src='$image_path/admin/button/s_btn_export_$intranet_session_language.gif' border='0'>";
}

function btnExport1()
{
        global $intranet_session_language, $image_path;
        return "<input type='image' src='$image_path/admin/button/s_btn_export_$intranet_session_language.gif' border='0'>";
}

function btnReset($frmName="form1")
{
        global $intranet_session_language, $image_path;
        return "<a href='javascript:document.$frmName.reset()'><img src='$image_path/admin/button/s_btn_reset_$intranet_session_language.gif' border='0'></a>";
}


function intranet_random_passwd($length, $enableUpperCase=false){
        # $possible = "0123456789";
        # $possible = "23456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
        if ($enableUpperCase) {
            $possible = "23456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
        }
        else {
            $possible = "23456789abcdefghijkmnpqrstuvwxyz";
        }
        $str = "";
        while(strlen($str)<$length){
                mt_srand((double)microtime()*1000000);
                $str .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
        }
        return $str;
}

function intranet_group_role($RecordType="", $index){
        global $i_GroupRole;
        for($i=$index; $i<sizeof($i_GroupRole); $i++){
                $x .= "<input type=radio name=RecordType value=$i";
                $x .= (($RecordType==$i) || ($RecordType=="" && $i==0) || ($RecordType==-1 && $i==0)) ? " CHECKED" : "";
                $x .= "> ".$i_GroupRole[$i]." <br> \n";
        }
        return $x;
}

###########################################################################
# Utility functions

# compare date in timestamp
# if a > b, return 1
# a = b, return 0
# a < b, return -1
function compareDate ($a, $b)
{
         $day_a = getdate($a);
         $day_b = getdate($b);

         if ($day_a['year'] > $day_b['year'])
         {
             return 1;
         }
         else if ($day_a['year'] < $day_b['year'])
         {
              return -1;
         }
         else if ($day_a['mon'] > $day_b['mon'])
         {
              return 1;
         }
         else if ($day_a['mon'] < $day_b['mon'])
         {
              return -1;
         }
         else if ($day_a['mday'] > $day_b['mday'])
         {
              return 1;
         }
         else if ($day_a['mday'] < $day_b['mday'])
         {
              return -1;
         }
         else return 0;
}

function isBig5First($s)
{
     $firstStart = 161;
     $firstEnd = 254;
     $s = ord($s);
     return  ($s >= $firstStart && $s <= $firstEnd);
}

# Check a string contains Big5 character or not

function isBig5($s)
{
     $firstStart = 161;
     $firstEnd = 254;
     $secondStart1 = 64;
     $secondEnd1 = 126;

     for ($i=0; $i<strlen($s); $i++)
     {
          $curr = ord(substr($s, $i, 1));
          if ($curr >= $firstStart && $curr <= $firstEnd)
          {
               $next = ord(substr($s,$i+1,1));
               if ( ($next >= $firstStart && $next <= $firstEnd) || ($next >= $secondStart1 && $next <= $secondEnd1) )
                    return true;
          }
     }
     return false;

}

// Check character maybe Big5
function inBig5Range($s)
{
     $firstStart = 161;
     $firstEnd = 254;
     $secondStart1 = 64;
     $secondEnd1 = 126;
     $s = ord($s);

     return ( ($s >= $firstStart && $s <= $firstEnd) || ($s >= $secondStart1 && $s <= $secondEnd1) );
}

function isBig5pattern($prev,$target,$next)
{
         if (isBig5First($target) && (isBig5First($prev) || inBig5Range($next)) )
             return true;
         if (inBig5Range($target) && isBig5First($prev) )
             return true;
         else return false;
}

// wordwrap ignoring HTML tags and Big5 chars
function wordwrap2($str,$cols,$cut=" ") {
         $len=strlen($str);
         $tag=0;
         for ($i=0;$i<$len;$i++)
         {
              $chr = substr($str,$i,1);
              $prev = ($i==0)?"":substr($str,$i-1,1);
              $next = substr($str,$i+1,1);
              if ($chr=="<")
                  $tag++;
              elseif ($chr==">")
                  $tag--;
              elseif (!$tag && ($chr==" " ||$chr=="-"|| isBig5pattern($prev,$chr,$next)))
                  $wordlen=0;
              elseif (!$tag)
                  $wordlen++;

              if (!$tag && !($wordlen%$cols) && $wordlen && !isBig5pattern($prev,$chr,$next))
                   $chr .= $cut;

              $result .= $chr;
         }
         return $result;
}

function intranet_wordwrap($text, $length,$cutter="",$cut=0)
{
         //if (isBig5($text)) return $text;            // Not wrap Chinese chars

         if (!$cut && $cutter!="")
         {
             return wordwrap2($text,$length);
         }
         else
         {
             return wordwrap2($text,$length,$cutter);
         }

}

# This can only wrap 1st hyperlink anchor
function wraplink ($text,$count = "")
{
         global $default_wrap_length;
         $wrap_length = ($count=="")?$default_wrap_length:$count;
         # Locate text inside anchor
         $pos_start = strpos($text, ">");
         $pos_end = strpos($text,"<",$pos_start+1);
         $inside_text = substr($text,$pos_start+1,$pos_end - $pos_start-1);
         # wrap inside text only
         $new_text = intranet_wordwrap($inside_text,$wrap_length,"\n",1);
         $newlink = substr($text,0,$pos_start+1).$new_text.substr($text,$pos_end);
         return $newlink;
}

# This can only chop 1st hyperlink anchor
function choplink ($text, $count="")
{
         global $default_chop_length;
         $wrap_length = ($count=="" || $count < $default_chop_length)?$default_chop_length:$count;
         # Locate text inside anchor
         $pos_start = strpos($text, ">");
         $pos_end = strpos($text,"<",$pos_start+1);
         $inside_text = substr($text,$pos_start+1,$pos_end - $pos_start-1);

         if (strlen($inside_text)<=$count) return $text;
         # wrap inside text only
         $new_text = substr($inside_text,0,$count-6)."...".substr($inside_text,-3);
         $newlink = substr($text,0,$pos_start)." title='$inside_text'>".$new_text.substr($text,$pos_end);
         return $newlink;
}

function chopword ($text,$count="")
{
         global $default_chop_length;
         $wrap_length = ($count=="" || $count < $default_chop_length)?$default_chop_length:$count;

         if (strlen($text)<=$count) return $text;
         $new_text = substr($text,0,$count-6)."...".substr($text,-3);
         return $new_text;
}

function get_file_ext($file)
{
         $file = basename($file);
         // return strtolower(substr($file, strpos($file,".")));
         return strtolower(substr($file, strrpos($file,".")));
}

$image_exts = array(
".gif"
,".jpg"
,".jpe"
,".jpeg"
,".png"
,".bmp"
);
function isImage($filename)
{
         if (is_file($filename))
         {
             global $image_exts;
             $ext = get_file_ext($filename);
             return in_array($ext,$image_exts);
         }
         else return false;

}

/*
 * @param string $filename: real full file path
 */
function isVideoFile($filename)
{
	if(is_file($filename)) {
		$ext_ary = array(".mov",".avi",".mpeg",".mpeg4",".mpg",".mp4",".wmv");
		$ext = strtolower(get_file_ext($filename));
        return in_array($ext,$ext_ary);
	}
	return false;
}

# prefix is the table name/alias with "." , e.g. INTRANET_USER.
function getNameFieldByLang2 ($prefix ="", $displayLang="", $displayNickname=false)
{
         global $intranet_session_language;

         $displayLang = $displayLang ? $displayLang : $intranet_session_language;
         $chi = ($displayLang =="b5" || $displayLang == "gb");
         if ($chi)
         {
             $firstChoice = "ChineseName";
             $altChoice = "EnglishName";
         }
         else
         {
             $firstChoice = "EnglishName";
             $altChoice = "ChineseName";
         }
         $username_field = "IF($prefix$firstChoice IS NULL OR TRIM($prefix$firstChoice) = '',$prefix$altChoice,$prefix$firstChoice)";
         //debug_pr($username_field);

         if ($displayNickname) {
         	$username_field = " CONCAT(".$username_field.",
         								IF ( NickName is null or NickName = '', '', CONCAT(' (', ".$prefix."NickName, ') ') )
         						)";
         }

         return $username_field;
}

function getInitialNameField($prefix="") {
	$nameField = $prefix.'EnglishName';

    $nameFieldSql = "CONCAT(
						    IFNULL(LEFT($nameField, 1), ''),
						    CASE
								WHEN LENGTH(EnglishName)-LENGTH(REPLACE($nameField,' ',''))>2
								THEN LEFT(SUBSTRING_INDEX($nameField, ' ', -3), 1)
								ELSE ''
						    END,
						    CASE
								WHEN LENGTH($nameField)-LENGTH(REPLACE($nameField,' ',''))>1
								THEN LEFT(SUBSTRING_INDEX($nameField, ' ', -2), 1)
								ELSE ''
						    END,
						    CASE
								WHEN LENGTH($nameField)-LENGTH(REPLACE($nameField,' ',''))>0
								THEN LEFT(SUBSTRING_INDEX($nameField, ' ', -1), 1)
								ELSE ''
						    END
					)";

    return $nameFieldSql;
}


/*
# This UserNameLang function will display:
# Mr / Ms => TitleEnglish/TitleChinese => (empty) = Teacher
function getNameFieldByLang($prefix="", $displayLang="")
{
         global $intranet_session_language;
         global $i_title_mr,$i_title_miss,$i_title_mrs,$i_title_ms,$i_title_dr,$i_title_prof;
         global $i_general_Teacher;

         global $lib_title_disabled;      # Disable title display or not

		# Disable title display or not (from user info setting),  by Wah, 2008-07-03
         global $lib_using_user_info_setting, $lib_title_disabled_teacher, $lib_title_disabled_parent;

		$displayLang = $displayLang ? $displayLang : $intranet_session_language;
         $chi = ($displayLang =="b5" || $displayLang == "gb");
         $name_field = getNameFieldByLang2($prefix, $displayLang);
         $title_field = "CASE $prefix"."Title
                         WHEN 0 THEN '$i_title_mr '
                         WHEN 1 THEN '$i_title_miss '
                         WHEN 2 THEN '$i_title_mrs '
                         WHEN 3 THEN '$i_title_ms '
                         WHEN 4 THEN '$i_title_dr '
                         WHEN 5 THEN '$i_title_prof '
                         ELSE '' END
                         ";
         $title_teacher = $i_general_Teacher;


        // Modified by Wah, 2008-07-03
        // If $lib_using_user_info_setting = true, checking both teacher / parent
		if ($lib_using_user_info_setting)
		{
			$title_field_parent = $title_field;
			$title_field_teacher = $title_field;
			if ($lib_title_disabled_teacher)
			{
				$title_field_teacher = "''";
				$title_teacher = "";
			}
			if ($lib_title_disabled_parent)
			{
				$title_field_parent = "''";
			}

	        if ($chi)
	        {
				$field = "	TRIM(CONCAT($name_field,
	             				IF($prefix"."TitleChinese!='',
	             					CONCAT('', TRIM($prefix"."TitleChinese)) ,
	             					IF($prefix"."Teaching=1,'$title_teacher',
	             					IF($prefix"."RecordType=1 , $title_field_teacher,if ($prefix"."RecordType=3,$title_field_parent,''))
	             					)
	             				)
								))
	             			";
	         }
	         else
	         {
	             $field = "	TRIM(CONCAT(
	             					IF($prefix"."TitleEnglish!='',
	             					CONCAT($prefix"."TitleEnglish,' ') ,
	             					IF($prefix"."RecordType=1 ,$title_field_teacher,if ($prefix"."RecordType=3,$title_field_parent,'')
	             					)
	             					),
	             					$name_field
	             				))
	             			";
	         }
         }
         else
         {
	         if ($lib_title_disabled)
	         {
	             $title_field = "''";
	             $title_teacher = "";
	         }

	         if ($chi)
	         {
	             $field = "	TRIM(CONCAT($name_field, IF($prefix"."TitleChinese!='', CONCAT('', $prefix"."TitleChinese) ,IF($prefix"."Teaching=1,'$title_teacher',IF($prefix"."RecordType=1 OR $prefix"."RecordType=3, $title_field,'')))))";
	         }
	         else
	         {
	             $field = "TRIM(CONCAT( IF($prefix"."TitleEnglish!='', CONCAT($prefix"."TitleEnglish,' ') ,  IF($prefix"."RecordType=1 OR $prefix"."RecordType=3,$title_field,'')),$name_field))";
	         }
         }
         return $field;
}
*/

# This UserNameLang function will display:
# TitleEnglish/TitleChinese => (empty) = nothing

function getNameFieldByLang($prefix="", $displayLang="", $isTitleDisabled=false, $displayNickname=false)
{

         global $intranet_session_language;

		$displayLang = $displayLang ? $displayLang : $intranet_session_language;
         $chi = ($displayLang =="b5" || $displayLang == "gb");
         $name_field = getNameFieldByLang2($prefix, $displayLang, $displayNickname);
			if($isTitleDisabled){
				$field = " TRIM(CONCAT($name_field))";
			}
			else{
		        if ($chi)
		        {
		            $field = "	TRIM(CONCAT($name_field, IF($prefix"."TitleChinese!='', CONCAT('', $prefix"."TitleChinese), '')))";
		        }
		        else
		        {
		        	$field = "TRIM(CONCAT( IF($prefix"."TitleEnglish!='', CONCAT($prefix"."TitleEnglish,' ') ,  ''),$name_field))";
		        }
			}
         return $field;
}

function getNameFieldWithClassNumberByLang ($prefix="", $isTitleDisabled=false, $displayNickname=false)
{
# Add case of graduates

         $username_field = getNameFieldByLang($prefix,$displayLang="",$isTitleDisabled, $displayNickname);
         $field = "IF($prefix"."RecordType=4,CONCAT($username_field, if($prefix"."YearOfLeft IS NOT NULL AND ($prefix"."YearOfLeft<>''), CONCAT( ' (',$prefix"."YearOfLeft, if( $prefix"."ClassName IS NOT NULL AND $prefix"."ClassName<>'', CONCAT('-',$prefix"."ClassName), ''),')'), '')) , CONCAT($username_field,IF($prefix"."ClassNumber IS NULL OR $prefix"."ClassNumber = '','',CONCAT(' (',$prefix"."ClassName,'-',$prefix"."ClassNumber,')'))) )";
         //debug_pr($username_field);
         return $field;
}

function getNameFieldWithLoginByLang ($prefix="")
{
         $username_field = getNameFieldByLang($prefix);
         $field = "CONCAT($username_field,CONCAT(' (',$prefix"."UserLogin,')'))";
         return $field;
}

function getNameFieldWithLoginByLang2 ($prefix="")
{
         $username_field = getNameFieldByLang2($prefix);
         $field = "CONCAT($username_field,CONCAT(' (',$prefix"."UserLogin,')'))";
         return $field;
}
function getNameFieldWithLoginClassNumberByLang($prefix="")
{
         $username_field = getNameFieldByLang($prefix);
         $field = "CONCAT($username_field,CONCAT(' (',$prefix"."UserLogin,IF($prefix"."ClassNumber IS NULL OR $prefix"."ClassNumber = '','',CONCAT(' ,',$prefix"."ClassName,'-',$prefix"."ClassNumber)),')'))";
         return $field;
}

# Fix to English Name ONLY
# prefix is the table name/alias with "." , e.g. INTRANET_USER.
function getNameFieldEng2 ($prefix ="")
{
         $username_field = "TRIM($prefix"."EnglishName)";
//         $username_field = ($chi? "ChineseName": "EnglishName");
         return $username_field;
}
function getNameFieldEng($prefix="")
{
         $name_field = getNameFieldEng2($prefix);
         $title_field = "CASE $prefix"."Title
                         WHEN 0 THEN ' Mr. '
                         WHEN 1 THEN ' Miss '
                         WHEN 2 THEN ' Mrs. '
                         WHEN 3 THEN ' Ms. '
                         WHEN 4 THEN ' Dr. '
                         WHEN 5 THEN ' Prof. '
                         ELSE '' END
                         ";
         $field = "TRIM(CONCAT(IF($prefix"."RecordType=1 OR $prefix"."RecordType=3,$title_field,''),$name_field))";
         return $field;
}

// for eClass use only
function getNameFieldForRecord2eClassOnly($prefix ="", $displayLang="")
{
	global $intranet_session_language;

	$displayLang = $displayLang ? $displayLang : $intranet_session_language;
	# Checking with lang session
	$chi = ($displayLang =="b5" || $displayLang == "gb");


	if ($chi)
	{
		$firstChoice = $prefix."chinesename";
		//$altChoice = "concat($prefix"."lastname, if($prefix"."lastname is not null, ' ', ''), $prefix"."firstname)";
		$altChoice = "concat(if($prefix"."lastname is not null, concat($prefix"."lastname, ' '), ''), if($prefix"."firstname is not null, $prefix"."firstname, ''))";
	}
	else
	{
		//$firstChoice = "concat($prefix"."lastname, if($prefix"."lastname is not null, ' ', ''), $prefix"."firstname)";
		$firstChoice = "concat(if($prefix"."lastname is not null, concat($prefix"."lastname, ' '), ''), if($prefix"."firstname is not null, $prefix"."firstname, ''))";
		$altChoice = $prefix."chinesename";
	}
	$username_field = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '', $altChoice, $firstChoice)";
	return $username_field;
}

function getNameFieldForRecord2 ($prefix ="")
{
         global $langID;
         $chi = ($langID == 1 || $langID == 2);
         if ($chi)
         {
             $firstChoice = "ChineseName";
             $altChoice = "EnglishName";
         }
         else
         {
             $firstChoice = "EnglishName";
             $altChoice = "ChineseName";
         }
         $username_field = "IF($prefix$firstChoice IS NULL OR TRIM($prefix$firstChoice) = '',$prefix$altChoice,$prefix$firstChoice)";
         return $username_field;
}

/*
# This UserNameLang function will display:
# Mr / Ms => TitleEnglish/TitleChinese => (empty) = Teacher
function getNameFieldForRecord($prefix="")
{
         global $i_title_mr,$i_title_miss,$i_title_mrs,$i_title_ms,$i_title_dr,$i_title_prof;
         global $i_general_Teacher;
         global $langID;

         global $lib_title_disabled;      # Disable title display or not

		# Disable title display or not (from user info setting),  by Wah, 2008-07-03
         global $lib_using_user_info_setting, $lib_title_disabled_teacher, $lib_title_disabled_parent;

         $chi = ($langID == 1 || $langID == 2);
         $name_field = getNameFieldForRecord2($prefix);
         $title_field = "CASE $prefix"."Title
                         WHEN 0 THEN ' $i_title_mr '
                         WHEN 1 THEN ' $i_title_miss '
                         WHEN 2 THEN ' $i_title_mrs '
                         WHEN 3 THEN ' $i_title_ms '
                         WHEN 4 THEN ' $i_title_dr '
                         WHEN 5 THEN ' $i_title_prof '
                         ELSE '' END
                         ";
         $title_teacher = $i_general_Teacher;

        // Modified by Wah, 2008-07-03
        // If $lib_using_user_info_setting = true, checking both teacher / parent
		if ($lib_using_user_info_setting)
		{
			$title_field_parent = $title_field;
			$title_field_teacher = $title_field;
			if ($lib_title_disabled_teacher)
			{
				$title_field_teacher = "''";
				$title_teacher = "";
			}
			if ($lib_title_disabled_parent)
			{
				$title_field_parent = "''";
			}

         	if ($chi)
         	{
				$field = "TRIM(CONCAT($name_field,IF($prefix"."Teaching=1,'$title_teacher',IF($prefix"."RecordType=1, $title_field_teacher,if ($prefix"."RecordType=3,$title_field_parent,'')))))";
         	}
         	else
         	{
            	 $field = "TRIM(CONCAT(IF($prefix"."RecordType=1,$title_field_teacher,if ($prefix"."RecordType=3,$title_field_parent,'')),$name_field))";
         	}

		}
		else
		{
         	if ($lib_title_disabled)
         	{
				$title_field = "''";
             	$title_teacher = "";
         	}
         	if ($chi)
         	{
				$field = "TRIM(CONCAT($name_field,IF($prefix"."Teaching=1,' $title_teacher',IF($prefix"."RecordType=1 OR $prefix"."RecordType=3, $title_field,''))))";
         	}
         	else
         	{
            	 $field = "TRIM(CONCAT(IF($prefix"."RecordType=1 OR $prefix"."RecordType=3,$title_field,''),$name_field))";
         	}
		}
		return $field;

}
*/

# This UserNameLang function will display:
# TitleEnglish/TitleChinese => (empty) = nothing
function getNameFieldForRecord($prefix="")
{
	global $langID;

	$chi = ($langID == 1 || $langID == 2);
	$name_field = getNameFieldForRecord2($prefix);

	if ($chi)
	{
		$field = "	TRIM(CONCAT($name_field, IF($prefix"."TitleChinese!='', CONCAT('', $prefix"."TitleChinese), '')))";
	}
	else
	{
		$field = "TRIM(CONCAT( IF($prefix"."TitleEnglish!='', CONCAT($prefix"."TitleEnglish,' ') ,  ''),$name_field))";
	}

	return $field;
}

function getNameFieldWithClassNumberForRecord($prefix="")
{
         $username_field = getNameFieldForRecord($prefix);
         $field = "CONCAT($username_field,IF($prefix"."ClassNumber IS NULL OR $prefix"."ClassNumber = '','',CONCAT(' (',$prefix"."ClassName,'-',$prefix"."ClassNumber,')')))";
         return $field;
}

function getNameFieldWithClassNumberEng ($prefix="")
{
         //$username_field = getNameFieldEng($prefix);
         ## Ronald
         $username_field = getNameFieldByLang($prefix);
         $field = "CONCAT($username_field,IF($prefix"."ClassNumber IS NULL OR $prefix"."ClassNumber = '','',CONCAT(' (',$prefix"."ClassName,'-',$prefix"."ClassNumber,')')))";
         return $field;
}
function getNameFieldWithLoginEng ($prefix="")
{
         $username_field = getNameFieldEng($prefix);
         $field = "CONCAT($username_field,CONCAT(' (',$prefix"."UserLogin,')'))";
         return $field;
}
function getNameFieldWithLoginClassNumberEng($prefix="")
{
         $username_field = getNameFieldEng($prefix);
         $field = "CONCAT($username_field,CONCAT(' (',$prefix"."UserLogin,IF($prefix"."ClassNumber IS NULL OR $prefix"."ClassNumber = '','',CONCAT(' ,',$prefix"."ClassName,'-',$prefix"."ClassNumber)),')'))";
         return $field;
}

function getNameFieldInactiveUserIndicator($name_field, $prefix, $withStyle=1,$indicator='^')
{
	if($withStyle)
		$indicator = "<font color=red>".$indicator."</font>";

	$x = "CONCAT(IF($prefix"."RecordStatus <> 1 OR ($prefix"."RecordType = 2 AND ($prefix"."ClassName IS NULL OR $prefix"."ClassNumber IS NULL OR TRIM($prefix"."ClassName) = '' OR TRIM($prefix"."ClassNumber) = '' )),'$indicator',''),$name_field)";

	return $x;
}

// DHTML scroller
function getMarqueeTicker($text)
{
         $text = addslashes($text);
         $x = "
			<script language=\"JavaScript1.2\">

			/*
			Cross browser Marquee script- ?Dynamic Drive (www.dynamicdrive.com)
			For full source code, 100's more DHTML scripts, and Terms Of Use, visit http://www.dynamicdrive.com
			Credit MUST stay intact
			*/

			//Specify the marquee's width (in pixels)
			var marqueewidth=345
			//Specify the marquee's height
			var marqueeheight=15
			//Specify the marquee's marquee speed (larger is faster 1-10)
			var marqueespeed=1
			//configure background color:
			var marqueebgcolor=\"#FFCB00\"
			//Pause marquee onMousever (0=no. 1=yes)?
			var pauseit=1

			//Specify the marquee's content (don't delete <nobr> tag)
			//Keep all content on ONE line, and backslash any single quotations (ie: that\'s great):

			var marqueecontent='<nobr><font style=\"font-size: 15px\" face=\"Mingliu\">$text</font></nobr>'


			////NO NEED TO EDIT BELOW THIS LINE////////////
			marqueespeed=(document.all)? marqueespeed : Math.max(1, marqueespeed-1) //slow speed down by 1 for NS
			var copyspeed=marqueespeed
			var pausespeed=(pauseit==0)? copyspeed: 0
			var iedom2=document.all||document.getElementById
			if (iedom2)
			document.write('<span id=\"temp\" style=\"visibility:hidden;position:absolute;top:-100;left:-1000\">'+marqueecontent+'</span>')
			var actualwidth=''
			var cross_marquee, ns_marquee

			function populate(){
			if (iedom2){
			cross_marquee=document.getElementById? document.getElementById(\"iemarquee\") : document.all.iemarquee
			cross_marquee.style.left=marqueewidth+8
			cross_marquee.innerHTML=marqueecontent
			actualwidth=document.all? cross_marquee.offsetWidth : document.getElementById(\"temp\").offsetWidth
			}
			else if (document.layers){
			ns_marquee=document.ns_marquee.document.ns_marquee2
			ns_marquee.left=marqueewidth+8
			ns_marquee.document.write(marqueecontent)
			ns_marquee.document.close()
			actualwidth=ns_marquee.document.width
			}
			lefttime=setInterval(\"scrollmarquee()\",20)
			}
			//window.onload=populate

			function scrollmarquee(){
			if (iedom2){
			if (parseInt(cross_marquee.style.left)>(actualwidth*(-1)+8))
			cross_marquee.style.left=parseInt(cross_marquee.style.left)-copyspeed
			else
			cross_marquee.style.left=marqueewidth+4

			}
			else if (document.layers){
			if (ns_marquee.left>(actualwidth*(-1)+8))
			ns_marquee.left-=copyspeed
			else
			ns_marquee.left=marqueewidth+4
			}
			}

			if (iedom2||document.layers){
			with (document){
			document.write('<table border=0 cellspacing=0 cellpadding=0><td>')
			if (iedom2){
			write('<div style=\"position:relative;width:'+marqueewidth+';height:'+marqueeheight+';overflow:hidden\">')
			write('<div style=\"position:absolute;width:'+marqueewidth+';height:'+marqueeheight+';background-color:'+marqueebgcolor+'\" onMouseover=\"copyspeed=pausespeed\" onMouseout=\"copyspeed=marqueespeed\">')
			write('<div id=\"iemarquee\" style=\"position:absolute;left:0;top:0\"></div>')
			write('</div></div>')
			}
			else if (document.layers){
			write('<ilayer width='+marqueewidth+' height='+marqueeheight+' name=\"ns_marquee\" bgColor='+marqueebgcolor+'>')
			write('<layer name=\"ns_marquee2\" left=0 top=0 onMouseover=\"copyspeed=pausespeed\" onMouseout=\"copyspeed=marqueespeed\"></layer>')
			write('</ilayer>')
			}
			document.write('</td></table>')
			}
			}

			populate()

			</script>
			<script>
			</script>
         ";
         return $x;
}

// DHTML scroller
function getMarqueeTicker_20($text)
{
	$text = addslashes($text);

	$x = "
			<script language=\"JavaScript\">

			/*
			Cross browser Marquee script- ?Dynamic Drive (www.dynamicdrive.com)
			For full source code, 100's more DHTML scripts, and Terms Of Use, visit http://www.dynamicdrive.com
			Credit MUST stay intact
			*/

			//Specify the marquee's width (in pixels)
// 			alert(screen.availWidth);
			var marqueewidth = (screen.availWidth-500);
			//Specify the marquee's height
			var marqueeheight=15;
			//Specify the marquee's marquee speed (larger is faster 1-10)
			var marqueespeed=2;
			//configure background color:
			var marqueebgcolor=\"#FFF299\";
			//Pause marquee onMousever (0=no. 1=yes)?
			var pauseit=1;

			//Specify the marquee's content (don't delete <nobr> tag)
			//Keep all content on ONE line, and backslash any single quotations (ie: that\'s great):

			var marqueecontent='<nobr><span class=\"indexscrolltext\" style=\"z-index:-2\">$text</span></nobr>';


			////NO NEED TO EDIT BELOW THIS LINE////////////
			marqueespeed=(document.all)? marqueespeed : Math.max(1, marqueespeed-1) //slow speed down by 1 for NS
			var copyspeed=marqueespeed
			var pausespeed=(pauseit==0)? copyspeed: 0
			var iedom2=document.all||document.getElementById
			if (iedom2)
			document.write('<span id=\"temp\" style=\"visibility:hidden;position:absolute;top:-100;left:-1000;z-index:-1\">'+marqueecontent+'</span>')
			var actualwidth=''
			var cross_marquee, ns_marquee

			function populate(){
			if (iedom2){
			cross_marquee=document.getElementById? document.getElementById(\"iemarquee\") : document.all.iemarquee
			cross_marquee.style.left=marqueewidth+8
			cross_marquee.innerHTML=marqueecontent
			actualwidth=document.all? cross_marquee.offsetWidth : document.getElementById(\"temp\").offsetWidth
			}
			else if (document.layers){
			ns_marquee=document.ns_marquee.document.ns_marquee2
			ns_marquee.left=marqueewidth+8
			ns_marquee.document.write(marqueecontent)
			ns_marquee.document.close()
			actualwidth=ns_marquee.document.width
			}
			lefttime=setInterval(\"scrollmarquee()\",20)
			}
			//window.onload=populate

			function scrollmarquee(){
			if (iedom2){
			if (parseInt(cross_marquee.style.left)>(actualwidth*(-1)+8))
			cross_marquee.style.left=parseInt(cross_marquee.style.left)-copyspeed
			else
			cross_marquee.style.left=marqueewidth+4

			}
			else if (document.layers){
			if (ns_marquee.left>(actualwidth*(-1)+8))
			ns_marquee.left-=copyspeed
			else
			ns_marquee.left=marqueewidth+4;
			}
			}

			if (iedom2||document.layers)
			{
				with (document)
				{
					//document.write('<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>')
					if (iedom2)
					{
						write('<span id=\"MovingTextDiv\" style=\"position:relative;width:'+marqueewidth+';height:'+marqueeheight+';overflow:hidden\">')
						write('<span style=\"position:absolute;width:'+marqueewidth+';height:'+marqueeheight+';background-color:'+marqueebgcolor+'\" onMouseover=\"copyspeed=pausespeed\" onMouseout=\"copyspeed=marqueespeed\">')
						write('<span id=\"iemarquee\" style=\"position:absolute;left:5;top:2;vertical-align:middle\" ></span></span></span>')
					}
					else
					if (document.layers)
					{
						write('<ilayer width='+marqueewidth+' height='+marqueeheight+' name=\"ns_marquee\" bgColor='+marqueebgcolor+'>')
						write('<layer name=\"ns_marquee2\" left=5 top=2 onMouseover=\"copyspeed=pausespeed\" onMouseout=\"copyspeed=marqueespeed\"></layer>')
						write('</ilayer>')
					}
					//document.write('</td></tr></table>')
				}
			}

			//populate();

			</script>
         ";

         return $x;
}

function cut_string($text, $length, $symbol = "..."){
     $length_text = strlen($text);
     $length_symbol = strlen($symbol);

     if($length_text <= $length || $length_text <= $length_symbol || $length <= $length_symbol)
          return($text);
     else
          return(substr($text, 0, $length - $length_symbol) . $symbol);
}

function makeQuestionToolTipContent ($code, $lvl, $cat, $diff, $eng, $chi, $score)
{
         global $i_QB_QuestionCode,$i_QB_Category, $i_QB_Level, $i_QB_Difficulty,$i_QB_EngVer,$i_QB_ChiVer;
         global $qb_record_score, $i_QB_AwardPts,$i_QB_Pts;
         $eng = str_replace(">","&gt",$eng);
         $eng = str_replace("<","&lt",$eng);
         $chi = str_replace(">","&gt",$chi);
         $chi = str_replace("<","&lt",$chi);
         $x .= "<table border=0 cellspacing=1 cellpadding=2>";
         $x .= "<tr><td align=right width=100>$i_QB_QuestionCode:</td><td>$code</td></tr>";
         $x .= "<tr><td align=right >$i_QB_Level:</td><td>$lvl</td></tr>";
         $x .= "<tr><td align=right >$i_QB_Category:</td><td>$cat</td></tr>";
         $x .= "<tr><td align=right >$i_QB_Difficulty:</td><td>$diff</td></tr>";
         $x .= "<tr><td align=right >$i_QB_EngVer:</td><td>$eng</td></tr>";
         $x .= "<tr><td align=right >$i_QB_ChiVer:</td><td>$chi</td></tr>";
         if ($qb_record_score && $score != 0)
         {
             $x .= "<tr><td align=right >$i_QB_AwardPts:</td><td>$score $i_QB_Pts</td></tr>";
         }
         $x .= "</table>";
         $x = str_replace("\n","<br>",$x);
         $x = str_replace("\r","",$x);
         $x = addslashes($x);
         return $x;
}
function makeTooltip($str)
{
         $x = $str;
         $x = str_replace("\n","<br>",$x);
         $x = str_replace("\r","",$x);
         $x = addslashes($x);
         $x = htmlspecialchars($x);
         $x = "onMouseMove='overhere()' onMouseOut='hideTip()' onBlur='hideTip()' onMouseOver=\"showTip('ToolTip',makeTip('$x'))\"";
         return $x;
}

function getSelectSchoolDayType ($tag , $selected="", $ParQuoteValue=0)
{
	global $i_DayTypeArray;
	$x = "<SELECT $tag>\n";
	for ($i=1; $i<sizeof($i_DayTypeArray); $i++)
	{
		$selected_tag = ($i==$selected? "SELECTED":"");

		# Eric Yip : Quote values in pull-down list
		switch($ParQuoteValue)
		{
			case 0:
				$x .= "<OPTION value=$i $selected_tag>".$i_DayTypeArray[$i]."</OPTION>\n";
				break;
			case 2:
				$x .= "<OPTION value=\"$i\" $selected_tag>".$i_DayTypeArray[$i]."</OPTION>\n";
				break;
		}
	}

	$x .= "</SELECT>\n";
	return $x;
}

function getStartOfAcademicYear($ts='', $AcademicYearID='')
{
	if ($AcademicYearID == '') {
		$AcademicYearID = Get_Current_Academic_Year_ID();
	}
	$StartDate = getStartDateOfAcademicYear($AcademicYearID);
	return strtotime(substr($StartDate,0,10));
         /*if (date('m',$ts) >= 9)
         {
             return mktime(0,0,0,9,1,date('Y',$ts));
         }
         else
         {
             return mktime(0,0,0,9,1,date('Y',$ts)-1);
         }*/
}

function getEndOfAcademicYear($ts='', $AcademicYearID='')
{
	if ($AcademicYearID == '') {
		$AcademicYearID = Get_Current_Academic_Year_ID();
	}
	$EndDate = getEndDateOfAcademicYear($AcademicYearID);
	return strtotime(substr($EndDate,0,10));
         /*if (date('m',$ts) >= 9)
         {
             return mktime(0,0,0,8,31,date('Y',$ts)+1);
         }
         else
         {
             return mktime(0,0,0,8,31,date('Y',$ts));
         }*/
}

function getAllSemesterByYearID($yearID="")
{
	include_once("libdb.php");
	$li = new libdb();

	if($yearID=="") $yearID = Get_Current_Academic_Year_ID();

	$termTitle = Get_Lang_Selection("YearTermNameB5","YearTermNameEN");
	$sql = "SELECT YearTermID, $termTitle AS TermTitle FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID=$yearID";
	$result = $li->returnArray($sql, 2);
	unset($li);
	return $result;

}

function getCurrentAcademicYear($ParLang='en')
{
		global $intranet_root;

		include_once("libdb.php");
		$li = new libdb();

		$year = Get_Current_Academic_Year_ID();
		$sql = "SELECT * FROM ACADEMIC_YEAR WHERE AcademicYearID=$year";
		$academicInfo = $li->returnArray($sql);

		//return Get_Lang_Selection($academicInfo[0]['YearNameB5'], $academicInfo[0]['YearNameEN']);
		unset($li);
		//return $academicInfo[0]['YearNameEN'];

		if ($ParLang=='en')
			return $academicInfo[0]['YearNameEN'];
		else if ($ParLang=='b5')
			return $academicInfo[0]['YearNameB5'];
		else if ($ParLang=='')
			return Get_Lang_Selection($academicInfo[0]['YearNameB5'], $academicInfo[0]['YearNameEN']);

         /*
         ### commented by henry (20090727)
         ### original coding in IP20 (START) ###

         $academic_yr = get_file_content("$intranet_root/file/academic_yr.txt");
         if ($academic_yr == "") $academic_yr = date("Y");
         return $academic_yr;

         ### original coding in IP20 (END) ###
         */
}

function getTargetYearSemesterByIDs($thisYearID, $thisSemesterID="", $ParLang="") {
	global $intranet_root;

	$dataAry = array();

	include_once("libdb.php");
	$li = new libdb();

	// Get Target Year Info
	$sql = "SELECT * FROM ACADEMIC_YEAR WHERE AcademicYearID = '$thisYearID'";
	$targetYearInfo = $li->returnArray($sql);
	unset($li);

	if ($ParLang=="en")
		$dataAry['YearName'] = $targetYearInfo[0]['YearNameEN'];
	else if ($ParLang=="b5")
		$dataAry['YearName'] = $targetYearInfo[0]['YearNameB5'];
	else
		$dataAry['YearName'] = Get_Lang_Selection($targetYearInfo[0]['YearNameB5'], $targetYearInfo[0]['YearNameEN']);

	// Get Target Semester Info
	if($thisSemesterID > 0) {
		include_once("form_class_manage.php");
		$objTerm = new academic_year_term($thisSemesterID, false);
		$termNameEN = $objTerm->YearTermNameEN;
		$termNameB5 = $objTerm->YearTermNameB5;
		unset($objTerm);

		if ($ParLang=="en")
			$dataAry['SemesterName'] = $termNameEN;
		else if ($ParLang=="b5")
			$dataAry['SemesterName'] = $termNameB5;
		else
			$dataAry['SemesterName'] = Get_Lang_Selection($termNameB5, $termNameEN);
	}

	return $dataAry;
}

function getCurrentDayType()
{
         global $intranet_root, $Test;

         $current_daytype = get_file_content("$intranet_root/file/schooldaytype.txt");
         if ($current_daytype != 2 && $current_daytype != 3)
             $current_daytype = 1;
         return $current_daytype;
}

function getSemesterMode()
{
	global $intranet_root;
	$semester_mode = get_file_content("$intranet_root/file/semester_mode.txt");
	if ($semester_mode == "") $semester_mode = 1;
    return $semester_mode;
}

#modified by Marcus 20091014 - add $lang to select lang of return value
function getSemesters($AcademicYearID='', $ReturnAsso=1, $ParLang='')
{
	/*
        global $intranet_root;

        $semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
        return $semester_data;
	*/

	include_once("form_class_manage.php");
	include_once("libdb.php");

	$li = new libdb();
	if ($AcademicYearID == '')
		$libAcademicYear = new academic_year();
	else
		$libAcademicYear = new academic_year($AcademicYearID);

	return $libAcademicYear->Get_Term_List($ReturnAsso , $ParLang);
}

function getCurrentSemester($specific_lang='')
{
	$TermInfoArr = getCurrentAcademicYearAndYearTerm();
	$curYearTermID = $TermInfoArr['YearTermID'];

	include_once("form_class_manage.php");
	$objTerm = new academic_year_term($curYearTermID, false);
	//$termName = $objTerm->Get_Year_Term_Name();
	$termNameEN = $objTerm->YearTermNameEN;
	$termNameB5 = $objTerm->YearTermNameB5;

	unset($objTerm);
	if(!$specific_lang)
		return Get_Lang_Selection($termNameB5, $termNameEN);
	else
	{
		return ${"termName".$specific_lang};
	}

}

function getCurrentSemesterID()
{
	$TermInfoArr = getCurrentAcademicYearAndYearTerm();
	$curYearTermID = $TermInfoArr['YearTermID'];

	return $curYearTermID;
}

function checkSemesterExists($xdate='')
{
	$semester_mode = getSemesterMode();
	$semester_data = getSemesters();

	if($semester_mode==2)
	{
		for ($i=0; $i<sizeof($semester_data); $i++)
		{
			$line = explode("::",$semester_data[$i]);
			list ($name,$current,$start,$end) = $line;
			if($xdate>=$start && $xdate<=$end)	return true;
		}
	}

	return false;
}

function retrieveSemester($xdate='')
{

	$semInfo = getAcademicYearAndYearTermByDate($xdate);
	return $semInfo[1];		# only return Term Name
	/*
	$semester_mode = getSemesterMode();
	$semester_data = getSemesters();

	if($semester_mode==2)
	{
		for ($i=0; $i<sizeof($semester_data); $i++)
		{
			$line = split("::",$semester_data[$i]);
			list ($name,$current,$start,$end) = $line;
			if($xdate>=$start && $xdate<=$end)	return $name;
		}
	}

	return false;
	*/
}

function getSelectSemester($tags, $selected="", $ParQuoteValue=1)
{
	$semester_data = getSemesters();

	$x = "<SELECT $tags>\n";
	for ($i=0; $i<sizeof($semester_data); $i++)
	{
		$line = explode("::",$semester_data[$i]);
		list ($name,$current) = $line;

		$selected_str = (($selected==$name || ($selected==""&&$current==1))? "SELECTED":"");

		# Eric Yip : Quote values in pull-down list
		switch($ParQuoteValue)
		{
			case 1:
				$x .= "<OPTION value='".htmlspecialchars($name,ENT_QUOTES)."' $selected_str>$name</OPTION>\n";
				break;
			case 2:
				$x .= "<OPTION value=\"".htmlspecialchars($name,ENT_QUOTES)."\" $selected_str>$name</OPTION>\n";
				break;
		}
	}

	$x .= "</SELECT>\n";
	return $x;
}

function getSelectSemester2($tags, $selected="", $ParQuoteValue=1, $AcademicYearID="")
{
	$semester_data = ($AcademicYearID=="") ? getSemesters() : getSemesters($AcademicYearID);

	$x = "<SELECT $tags>\n";

	foreach ((array)$semester_data as $id=>$name)
	{
		$selected_str = ($selected==$id) ? "SELECTED":"";

		# Eric Yip : Quote values in pull-down list
		switch($ParQuoteValue)
		{
			case 1:
				$x .= "<OPTION value='$id' $selected_str>$name</OPTION>\n";
				break;
			case 2:
				$x .= "<OPTION value=\"$id\" $selected_str>$name</OPTION>\n";
				break;
		}
	}

	$x .= "</SELECT>\n";
	return $x;
}

function getSelectByArray($data, $tags, $selected="", $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1)
{
	global $button_select,$i_status_all,$i_general_NotSet;
	$x = "<SELECT $tags>\n";
	if ($noFirst == 0)
	{
		$empty_selected = ($selected == '')? "SELECTED":"";
		if($FirstTitle=="")
		{
			if ($all==0)
			{
				$title = "-- $button_select --";
			}
			else if ($all == 2)
			{
				$title = "$i_general_NotSet";
			}
			else
			{
				$title = "$i_status_all";
			}
		}
		else
			$title = $FirstTitle;

		# Eric Yip : Quote values in pull-down list
		switch($ParQuoteValue)
		{
			case 1:
				$x .= "<OPTION value='' $empty_selected> $title </OPTION>\n";
				break;
			case 2:
				$x .= "<OPTION value=\"\" $empty_selected> $title </OPTION>\n";
				break;
		}
	}

	for ($i=0; $i<sizeof($data); $i++)
	{
		list ($id, $name) = $data[$i];
		if(is_array($selected))
			$sel_str = (in_array($id,$selected)? "SELECTED":"");
		else
		{
			$sel_str = ($selected == $id? "SELECTED":"");
		}

		# Eric Yip : Quote values in pull-down list
		switch($ParQuoteValue)
		{
			case 1:
				$x .= "<OPTION value='".htmlspecialchars($id,ENT_QUOTES)."' $sel_str>$name</OPTION>\n";
				break;
			case 2:
				$x .= "<OPTION value=\"".htmlspecialchars($id,ENT_QUOTES)."\" $sel_str>$name</OPTION>\n";
				break;
		}
	}

	$x .= "</SELECT>\n";
	return $x;
}

function getSelectByValue($data,$tags,$selected="",$all=0,$noFirst=0, $allName="", $ParQuoteValue=1)
{
	global $button_select,$i_status_all;
	$x = "<SELECT $tags>\n";
	if ($noFirst == 0)
	{
		$empty_selected = ($selected == '')? "SELECTED":"";
		$title = ($all==0? "-- $button_select --": ($allName==""? "$i_status_all":"$allName") );

		# Eric Yip : Quote values in pull-down list
		switch($ParQuoteValue)
		{
			case 1:
				$x .= "<OPTION value='' $empty_selected> $title </OPTION>\n";
				break;
			case 2:
				$x .= "<OPTION value=\"\" $empty_selected> $title </OPTION>\n";
				break;
		}
	}
	for ($i=0; $i<sizeof($data); $i++)
	{
		$name = $data[$i];
		$sel_str = ($selected == $name? "SELECTED":"");
		$name = intranet_htmlspecialchars($name);

		# Eric Yip : Quote values in pull-down list
		switch($ParQuoteValue)
		{
			case 1:
				$x .= "<OPTION value='".htmlspecialchars($name,ENT_QUOTES)."' $sel_str>$name</OPTION>\n";
				break;
			case 2:
				$x .= "<OPTION value=\"".htmlspecialchars($name,ENT_QUOTES)."\" $sel_str>$name</OPTION>\n";
				break;
		}
	}

	$x .= "</SELECT>\n";
	return $x;
}

function getSelectByValueDiffName($data,$name,$tags,$selected="",$all=0,$noFirst=0, $firstName="", $ParQuoteValue=1)
{
	global $button_select,$i_status_all;
	$x = "<SELECT $tags>\n";
	if ($noFirst == 0)
	{
		$empty_selected = ($selected == '')? "SELECTED":"";
		if(!$firstName)
			$title = ($all==0? "-- $button_select --":"$i_status_all");
		else
			$title = ($all==0? "-- $button_select --":"$firstName");

		# Eric Yip : Quote values in pull-down list
		switch($ParQuoteValue)
		{
			case 1:
				$x .= "<OPTION value='' $empty_selected> $title </OPTION>\n";
				break;
			case 2:
				$x .= "<OPTION value=\"\" $empty_selected> $title </OPTION>\n";
				break;
		}
	}

	for ($i=0; $i<sizeof($data); $i++)
	{
		$this_data = $data[$i];
		$this_name = $name[$i];
		$sel_str = ($selected == $this_data) ? "SELECTED":"";
		$this_name = intranet_htmlspecialchars($this_name);

		# Eric Yip : Quote values in pull-down list
		switch($ParQuoteValue)
		{
			case 1:
				$x .= "<OPTION value='".htmlspecialchars($this_data,ENT_QUOTES)."' $sel_str>$this_name</OPTION>\n";
				break;
			case 2:
				$x .= "<OPTION value=\"".htmlspecialchars($this_data,ENT_QUOTES)."\" $sel_str>$this_name</OPTION>\n";
				break;
		}
	}

	$x .= "</SELECT>\n";
	return $x;
}

function getSelectByArrayTitle($data, $tags, $title, $selected="", $skipTitle=false, $ParQuoteValue=0)
{
	$x = "<SELECT $tags>\n";

	if($skipTitle == false)
	{
		# Eric Yip : Quote values in pull-down list
		switch($ParQuoteValue)
		{
			case 0:
				$x .= "<OPTION value=''>$title</OPTION>\n";
				break;
			case 2:
				$x .= "<OPTION value=\"\">$title</OPTION>\n";
				break;
		}
	}

	for ($i=0; $i<sizeof($data); $i++)
	{
		list ($id, $name) = $data[$i];
		$sel_str = ($selected == $id? "SELECTED":"");

		# Eric Yip : Quote values in pull-down list
		switch($ParQuoteValue)
		{
			case 0:
				$x .= "<OPTION value='".htmlspecialchars($id,ENT_QUOTES)."' $sel_str>$name</OPTION>\n";
				break;
			case 2:
				$x .= "<OPTION value=\"".htmlspecialchars($id,ENT_QUOTES)."\" $sel_str>$name</OPTION>\n";
				break;
		}
	}

	$x .= "</SELECT>\n";
	return $x;
}

//function getSelectByAssoArray($data, $tags, $selected="", $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1)
//{
//	global $button_select,$i_status_all,$i_general_NotSet;
//	$x = "<SELECT $tags>\n";
//	if ($noFirst == 0)
//	{
//		$empty_selected = ($selected == '')? "SELECTED":"";
//		if($FirstTitle=="")
//		{
//			if ($all==0)
//			{
//				$title = "-- $button_select --";
//			}
//			else if ($all == 2)
//			{
//				$title = "$i_general_NotSet";
//			}
//			else
//			{
//				$title = "$i_status_all";
//			}
//			//echo $title;
//		}
//		else
//		{
//			$title = $FirstTitle;
//		}
//
//		# Eric Yip : Quote values in pull-down list
//		switch($ParQuoteValue)
//		{
//			case 1:
//				$x .= "<OPTION value='' $empty_selected> $title </OPTION>\n";
//				break;
//			case 2:
//				$x .= "<OPTION value=\"\" $empty_selected> $title </OPTION>\n";
//				break;
//		}
//	}
//
//	while($element = each($data))
//	{
//		$tempKey = $element['key'];
////		$tempValue = intranet_htmlspecialchars($element['value']);
//		$tempValue = $element['value'];
////		$sel_str = ($selected == $tempKey?" SELECTED":"");
//		if(is_array($selected))
//			$sel_str = (in_array($tempKey,$selected)? "SELECTED":"");
//		else
//			$sel_str = ($selected == $tempKey? "SELECTED":"");
//
//		# Eric Yip : Quote values in pull-down list
//		switch($ParQuoteValue)
//		{
//			case 1:
//				$x .= "<OPTION value='".htmlspecialchars($tempKey,ENT_QUOTES)."' $sel_str>$tempValue</OPTION>\n";
//				break;
//			case 2:
//				$x .= "<OPTION value=\"".htmlspecialchars($tempKey,ENT_QUOTES)."\" $sel_str>$tempValue</OPTION>\n";
//				break;
//		}
//	}
//
//	$x .= "</SELECT>\n";
//	return $x;
//}

function getSelectByAssoArray($data, $tags, $selected="", $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1)
{
// debug_pr($selected);
	if(array_depth($data)==1)
		$data = array(''=>(array)$data);

	global $button_select,$i_status_all,$i_general_NotSet;
	$x = "<SELECT $tags>\n";
	if ($noFirst == 0)
	{
		$empty_selected = ($selected == '' && trim(strlen($selected)==0))? "SELECTED":"";
		if($FirstTitle=="")
		{
			if ($all==0)
			{
				$title = "-- $button_select --";
			}
			else if ($all == 2)
			{
				$title = "$i_general_NotSet";
			}
			else
			{
				$title = "$i_status_all";
			}
			//echo $title;
		}
		else
		{
			$title = $FirstTitle;
		}

		# Eric Yip : Quote values in pull-down list
		switch($ParQuoteValue)
		{
			case 1:
				$x .= "<OPTION value='' $empty_selected> $title </OPTION>\n";
				break;
			case 2:
				$x .= "<OPTION value=\"\" $empty_selected> $title </OPTION>\n";
				break;
		}
	}

	while($element1 = each($data))
	{
		$OptGroupName = $element1['key'];
		$OptionArr = $element1['value'];

		if(trim($OptGroupName)!=='')
			$x .= "<optgroup label=\"".$OptGroupName."\">\n";

		while($element2 = each($OptionArr))
		{
			$tempKey = $element2['key'];
	//		$tempValue = intranet_htmlspecialchars($element['value']);
			$tempValue = $element2['value'];
	//		$sel_str = ($selected == $tempKey?" SELECTED":"");
			if(is_array($selected))
				$sel_str = (in_array($tempKey,$selected) && isset($selected)? "SELECTED":"");
			else
			{
				$sel_str = ($selected == $tempKey && $selected!=="" ? "SELECTED":"");
			}

			# Eric Yip : Quote values in pull-down list
			switch($ParQuoteValue)
			{
				case 1:
					$x .= "<OPTION value='".htmlspecialchars($tempKey,ENT_QUOTES)."' $sel_str $disabled>$tempValue</OPTION>\n";
					break;
				case 2:
					$x .= "<OPTION value=\"".htmlspecialchars($tempKey,ENT_QUOTES)."\" $sel_str $disabled>$tempValue</OPTION>\n";
					break;
			}
		}

		if(trim($OptGroupName)!='')
			$x .= "</optgroup>\n";

	}

	$x .= "</SELECT>\n";
	return $x;
}

function array_depth($ary)
{
	$hasArray = 0;
	$maxDepth = $depth = 0;

	foreach((array)$ary as $item)
	{
		if(is_array($item))
		{
			$depth = max(array_depth($item),$maxDepth);
			$hasArray = 1;
		}
	}
	if($hasArray)
		return $depth+1;
	else
		return 1;
}

# Implement array_union for 2 arguments
# Merge and reindex
function array_union($a1,$a2)
{
         $result = array_merge($a1,$a2);
         $result = array_unique($result);
         $result = array_values($result);
         return $result;
}

# Change 2D array to 1D with the first column as index
function build_assoc_array($array, $emptyname="build_assoc_array_null")
{
         for ($i=0; $i<sizeof($array); $i++)
         {
              list($id,$data) = $array[$i];
              if ($id == "")
              {
                  $id = $emptyname;
              }
              $result[$id] = $data;
         }
         return $result;
}

# navigation bar for Admin Console
function displayNavTitle() {
        $x = "";
        $y = "";

        $numargs = func_num_args();
        for ($i=0; $i<$numargs; $i++) {
                $y .= ($i>1) ? "<img src='/images/arrow.gif' width='7' height='9' hspace='5'>" : "";
                $title = func_get_arg($i);
                $link = func_get_arg(++$i);
                $y .= (trim($link)=="") ? "<span class='admin_title'>$title</span>" : "<a href='$link' class='admin_title'>$title</a>";
        }
        $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='10'>\n";
        $x .= "<tr><td height='34'><b>\n";
        $x .= $y;
        $x .= "</b></td></tr></table>\n";

        return $x;
}

# navigation bar for Staff Attendance Console
function displayStaffAttendNavTitle() {
        $x = "";
        $y = "";

        $numargs = func_num_args();
        for ($i=0; $i<$numargs; $i++) {
                $y .= ($i>1) ? "<img src='/images/staffattend/arrow.gif' width='7' height='9' hspace='5'>" : "";
                $title = func_get_arg($i);
                $link = func_get_arg(++$i);
                $y .= (trim($link)=="") ? "<span class='title'>$title</span>" : "<a href='$link' class='title'>$title</a>";
        }
       //$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>\n";
       // $x .= "<tr><td>\n";
        $x .= $y;
       // $x .= "</td></tr></table>\n";

        return $x;
}

# menu options
function displayOption()
{
        $x = "";
        $j = 0;

        $x .= "<table width='387' border='0' cellspacing='0' cellpadding='0'>\n";
        $numargs = func_num_args();
        for ($i=0; $i<$numargs; $i++) {
                $title = func_get_arg($i);
                $link = func_get_arg(++$i);
                $allow_access = func_get_arg(++$i);
                if ($allow_access)
                {
                        $num = ($j%2) ? "1" : "2";
                        $x .= "<tr><td width='48' height='40'><img src='/images/admin/options_".$num."_head.gif' border='0' height='40' width='48'></td><td width='339' style=\"vertical-align:middle; background-image:url('/images/admin/options_".$num."_tail.gif')\">".((trim($link)=="") ? $title : "<a href='$link'>$title</a>")."</td></tr>\n";
                        $x .= "<tr><td colspan='2'><img src='/images/space.gif' height='5' width='1' border='0'></td></tr>\n";
                        $j++;
                }
        }
        $x .= "</table>\n";

        return $x;
}

# menu options
function displayOption2()
{
        $x = "";
        $j = 0;

        $x .= "<table width='387' border='0' cellspacing='0' cellpadding='0'>\n";
        $numargs = func_num_args();
        for ($i=0; $i<$numargs; $i++) {
                $title = func_get_arg($i);
                $link = func_get_arg(++$i);
                $allow_access = func_get_arg(++$i);
                if ($allow_access)
                {
                        $num = ($j%2) ? "1" : "2";
                        $x .= "<tr><td width='48' height='40'><img src='/images/admin/options2_".$num."_head.gif' border='0' height='40' width='48'></td><td width='339' style=\"vertical-align:middle; background-image:url('/images/admin/options2_".$num."_tail.gif')\">".((trim($link)=="") ? $title : "<a href='$link'>$title</a>")."</td></tr>\n";
                        $x .= "<tr><td colspan='2'><img src='/images/space.gif' height='7' width='1' border='0'></td></tr>\n";
                        $j++;
                }
        }
        $x .= "</table>\n";

        return $x;
}

# tag for Admin Console
function displayTag($image, $msg=0, $displayMsg="") {
        global $xmsg;

        $x = "<table width='560' border='0' cellspacing='0' cellpadding='0' align='center'>\n";
        $x .= "<tr><td><img src='/images/admin/tag/$image' border='0'></td></tr>\n";
        $x .= "<tr><td height='25' align='right'><hr size=1>".(($displayMsg!="") ? $displayMsg : $xmsg)."</td></tr></table>\n";

        return $x;
}

# For development in middle
if (is_file("$intranet_root/plugins/release.php"))
{
    include_once("$intranet_root/plugins/release.php");
}

function returnGroupAvailableFunctions ()
{
         global $i_frontpage_schoolinfo_groupinfo_group_timetable,
                $i_frontpage_schoolinfo_groupinfo_group_chat,
                $i_frontpage_schoolinfo_groupinfo_group_bulletin,
                $i_frontpage_schoolinfo_groupinfo_group_links,
                $i_frontpage_schoolinfo_groupinfo_group_files,
                $i_adminmenu_plugin_qb,
                $i_frontpage_schoolinfo_groupinfo_group_photoalbum,
                $i_GroupOnlyAcademicAllowed,
                $i_GroupSettingsSurvey,
                $plugin;

         global $inRelease;

         $function_list = array ($i_frontpage_schoolinfo_groupinfo_group_timetable,
                $i_frontpage_schoolinfo_groupinfo_group_chat,
                $i_frontpage_schoolinfo_groupinfo_group_bulletin,
                $i_frontpage_schoolinfo_groupinfo_group_links,
                $i_frontpage_schoolinfo_groupinfo_group_files);

         if ($plugin['QB'])
         {
             $function_list[] = "$i_adminmenu_plugin_qb ($i_GroupOnlyAcademicAllowed)";
         }
         else
         {
             $function_list[] = "";
         }
//         if ($inRelease['album'])
//         {
             $function_list[] = $i_frontpage_schoolinfo_groupinfo_group_photoalbum;
//         }
//         else
//         {
//             $function_list[] = "";
//         }
         $function_list[] = $i_GroupSettingsSurvey;

         return $function_list;
}
function returnFormStatWords()
{
         global $i_Form_ShowHide,$i_Form_TotalNo, $i_Form_HideDetails, $i_Form_ShowDetails;
         $x = "
         		var w_show = \"$i_Form_ShowHide\";
				var w_total_no = \"$i_Form_TotalNo\";
				var w_show_details = \"$i_Form_ShowDetails\";
				var w_hide_details = \"$i_Form_HideDetails\";

				var w_view_details_img = \" \";
				var w_hide_details_img = \" \";
               ";
         return $x;
}

# Only for continuous integer index in both dimensions
function sortByColumn($array,$col,$order=1)
{
         for ($i=0; $i<sizeof($array); $i++)
         {
              for ($j=$i+1; $j<sizeof($array); $j++)
              {
                   if ($order == 1)          # Asc
                   {
                       if ($array[$i][$col] > $array[$j][$col] )
                       {
                           $tmp = $array[$i];
                           $array[$i] = $array[$j];
                           $array[$j] = $tmp;
                       }
                   }
                   else
                   {
                       if ($array[$i][$col] < $array[$j][$col] )
                       {
                           $tmp = $array[$i];
                           $array[$i] = $array[$j];
                           $array[$j] = $tmp;
                       }
                   }
              }
         }
         return $array;
}

# Only for continuous integer index
function in_array_col($needle, $array, $col)
{
         for ($i=0; $i<sizeof($array); $i++)
         {
              if ($array[$i][$col]==$needle)
              {
                  return $i;
              }
         }
         return false;
}

# Only for continuous integer index
function in_array_2col($value1, $value2, $array)
{
     for ($i=0; $i<sizeof($array); $i++)
     {
          if ($array[$i][0]==$value1 && $array[$i][1]==$value2)
          {
               return true;
          }
     }
     return false;
}

function output2browserByPath($file, $filename, $content_type="")
{
    // [2020-0522-1459-13207] clear current output buffer
    while (@ob_get_clean());

    if ($content_type == "")
	{
		$content_type = "application/octet-stream";
	}
	$filename = str_replace(" ", "_", $filename);

	global $userBrowser;

	if (isset($userBrowser))
	{
		$browser = $userBrowser->browsertype;
		$operation_system = $userBrowser->platform;
        if ($operation_system == "Andriod")
		{
			//$ToUnicode = true;
			/*
				if (str_lang($filename)!="ENG")
				{
				$file_extention = getFileExtention($filename, 1);
				$filename = time().".".$file_extention;
				}*/
		}
	}
	else
	{
		$useragent = $_SERVER['HTTP_USER_AGENT'];
		if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
			$browser_version = $matched[1];
			$browser = 'IE';
		} elseif (preg_match( '|Opera ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
			$browser_version = $matched[1];
			$browser = 'Opera';
		} elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
			$browser_version = $matched[1];
			$browser = 'Firefox';
		} elseif(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
			$browser_version = $matched[1];
			$browser = 'Safari';
		} else {
			// browser not recognized!
			$browser_version = 0;
			$browser = 'other';
		}
	}

	$encoded_filename = $filename;
	$encoded_filename = urlencode($filename);
	$encoded_filename = str_replace("+", "%20", $encoded_filename);

	header("Pragma: public");
	header("Expires: 0"); // set expiration time
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-type: $content_type");
	header("Content-Length: ".filesize($file));

	if ($browser == "IE")
	{
		header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');
	}
	else if ($browser == "Firefox")
	{
		//header('Content-Disposition: attachment; filename*="utf8\'\'' . $filename . '"');
		header('Content-Disposition: attachment; filename*=utf8\'\'' . $encoded_filename);
	}
	else if($browser == "Safari") // for Chrome
	{
		header('Content-Disposition: attachment; filename="' . $filename . '"');
	}
	else if($browser == "Chrome") // for Chrome
	{
		header('Content-Disposition: attachment; filename="' . $filename . '"');
	}
	else
	{
		header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');
	}
	readfile($file);

    // [2020-0522-1459-13207] output buffer contents
    while (@ob_end_flush());

	exit;
}

function output2browser($content, $filename,$content_type="")
{
		if ($content_type=="")
		{
			$content_type = "application/octet-stream";
		}
		$filename = str_replace(" ", "_", $filename);

		while (@ob_end_clean());

		global $userBrowser;

		if (isset($userBrowser))
		{
			$browser = $userBrowser->browsertype;
			$operation_system = $userBrowser->platform;
			if ($operation_system=="Andriod")
			{
				//$ToUnicode = true;
				/*
				if (str_lang($filename)!="ENG")
				{
					$file_extention = getFileExtention($filename, 1);
					$filename = time().".".$file_extention;
				}*/
			}
			else if (preg_match('|Edge/(\d+)|',$userBrowser->useragent,$matched))
			{
                $browser = 'Edge';
			}
		}
		else
		{
			$useragent = $_SERVER['HTTP_USER_AGENT'];
            if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
			    $browser_version=$matched[1];
			    $browser = 'IE';
			} elseif(preg_match('|Edge/(\d+)|',$useragent,$matched)) {
                $browser_version = $matched[1];
                $browser = 'Edge';
            } elseif(preg_match( '|Opera ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
			    $browser_version=$matched[1];
			    $browser = 'Opera';
			} elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
			    $browser_version=$matched[1];
			    $browser = 'Firefox';
			} elseif(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
			    $browser_version=$matched[1];
			    $browser = 'Safari';
			} else {
				// browser not recognized!
			    $browser_version = 0;
			    $browser= 'other';
			}
		}

		$encoded_filename = $filename;
        $encoded_filename = urlencode($filename);
		$encoded_filename = str_replace("+", "%20", $encoded_filename);

		header("Pragma: public");
		header("Expires: 0"); // set expiration time
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-type: $content_type");
		header("Content-Length: ".strlen($content));

		if ($browser == "IE" || $browser == "Edge")
		{
			header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');
		}
		else if ($browser == "Firefox")
		{
			//header('Content-Disposition: attachment; filename*="utf8\'\'' . $filename . '"');
			header('Content-Disposition: attachment; filename*=utf8\'\'' . $encoded_filename);
		}
		else if($browser == "Safari") // for Chrome
		{
			header('Content-Disposition: attachment; filename="' . $filename . '"');
		}
		else if($browser == "Chrome") // for Chrome
		{
			header('Content-Disposition: attachment; filename="' . $filename . '"');
		}
		else
		{
			header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');
		}

		echo $content;
		exit;
}
function Big5ToUnicode($string)
{
         global $function_undefined;
         if ($function_undefined['iconv'])
         {
             return $string;
         }
         else
         {
             //return iconv("Big5", "UTF-8", $string);
             return iconv("Big5", "UTF-8//IGNORE", $string);
         }
}

function GB2312_ToUnicode($string){
         global $function_undefined;
         if ($function_undefined['iconv'])
         {
             return $string;
         }
         else
         {
             return iconv("GB2312", "UTF-8", $string);
         }
}

function utf8Encode ($source) {
   $utf8Str = '';
   $entityArray = explode ("&#", $source);
   $size = count ($entityArray);
   for ($i = 0; $i < $size; $i++) {
       $subStr = $entityArray[$i];
       $nonEntity = strstr ($subStr, ';');
       if ($nonEntity !== false) {
           $unicode = intval (substr ($subStr, 0, (strpos ($subStr, ';') + 1)));
           // determine how many chars are needed to reprsent this unicode char
           if ($unicode < 128) {
               $utf8Substring = chr ($unicode);
           }
           else if ($unicode >= 128 && $unicode < 2048) {
               $binVal = str_pad (decbin ($unicode), 11, "0", STR_PAD_LEFT);
               $binPart1 = substr ($binVal, 0, 5);
               $binPart2 = substr ($binVal, 5);

               $char1 = chr (192 + bindec ($binPart1));
               $char2 = chr (128 + bindec ($binPart2));
               $utf8Substring = $char1 . $char2;
           }
           else if ($unicode >= 2048 && $unicode < 65536) {
               $binVal = str_pad (decbin ($unicode), 16, "0", STR_PAD_LEFT);
               $binPart1 = substr ($binVal, 0, 4);
               $binPart2 = substr ($binVal, 4, 6);
               $binPart3 = substr ($binVal, 10);

               $char1 = chr (224 + bindec ($binPart1));
               $char2 = chr (128 + bindec ($binPart2));
               $char3 = chr (128 + bindec ($binPart3));
               $utf8Substring = $char1 . $char2 . $char3;
           }
           else {
               $binVal = str_pad (decbin ($unicode), 21, "0", STR_PAD_LEFT);
               $binPart1 = substr ($binVal, 0, 3);
               $binPart2 = substr ($binVal, 3, 6);
               $binPart3 = substr ($binVal, 9, 6);
               $binPart4 = substr ($binVal, 15);

               $char1 = chr (240 + bindec ($binPart1));
               $char2 = chr (128 + bindec ($binPart2));
               $char3 = chr (128 + bindec ($binPart3));
               $char4 = chr (128 + bindec ($binPart4));
               $utf8Substring = $char1 . $char2 . $char3 . $char4;
           }

           if (strlen ($nonEntity) > 1)
               $nonEntity = substr ($nonEntity, 1); // chop the first char (';')
           else
               $nonEntity = '';

           $utf8Str .= $utf8Substring . $nonEntity;
       }
       else {
           $utf8Str .= $subStr;
       }
   }

   return $utf8Str;
}

function generateFileUploadNameHandler()
{
         $numargs = func_num_args();
         $form_name = func_get_arg(0);
         $x = "<script language=\"JavaScript\">
function Big5FileUploadHandler() {
";
         for ($i=1; $i<$numargs; $i++)
         {
              $file_ctrl = func_get_arg($i);
              $file_name_ctrl = func_get_arg(++$i);

              $x .= "
              if (document.$form_name.$file_ctrl.value != '')
              {
              var Ary = document.$form_name.$file_ctrl.value.split('\\\\');
  document.$form_name.$file_name_ctrl.value=Ary[Ary.length-1];
  }
  ";

         }
         $x .= "
         return true;
         }
</script>";
return $x;

}

        function generateFileUploadNameHandler2($Form,$ParArr)
        {
                 $numargs = count($ParArr);
                 $form_name = $Form;
                 $x = "<script language=\"JavaScript\">
                                        function Big5FileUploadHandler() {
                                        ";
                                                for ($i=0; $i<$numargs; $i++)
                                                {
                                      $file_ctrl = $ParArr[$i];
                                      $file_name_ctrl = $ParArr[++$i];

                                      $x .= "
                                      if (document.$form_name.$file_ctrl.value != '')
                                      {
                                              var Ary = document.$form_name.$file_ctrl.value.split('\\\\');
                                                          document.$form_name.$file_name_ctrl.value=Ary[Ary.length-1];
                                                  }
                                                  ";
                                 }
                                 $x .= "
                                 return true;
                                 }
                        </script>";
                        return $x;
        }

function generateFileUploadNameHandlerByCount($form_name, $filectrl_name, $filehidden_name, $num)
{
         $x = "<script language=\"JavaScript\">
function Big5FileUploadHandler() {
";
         for ($i=0; $i<$num; $i++)
         {
              $file_ctrl = $filectrl_name.$i;
              $file_name_ctrl = $filehidden_name.$i;
              $x .= "
              if (document.$form_name.$file_ctrl.value != '')
              {
                  var Ary = document.$form_name.$file_ctrl.value.split('\\\\');
                  document.$form_name.$file_name_ctrl.value=Ary[Ary.length-1];
              }
              ";

         }
         $x .= "
         return true;
         }
</script>";
return $x;

}

#########################################################################################
######################### From eClass lib.php ###########################################
function returnCharset(){
	/*
        global $ck_lang, $ck_default_lang, $g_encoding_unicode, $g_chinese, $intranet_default_lang, $intranet_session_language, $intranet_default_lang_set;

        if ($g_encoding_unicode)
        {
			$charset = "UTF-8";
        } else
        {
            $lang_used = (isset($ck_lang) && $ck_lang!="") ? substr($ck_lang, 0, (strlen($ck_lang)-1)) : $ck_default_lang;
            if ($lang_used=="" && $ck_default_lang=="")
            {
                    $charset = ($intranet_default_lang=="gb") ? "GB2312" : "big5";
            } else
            {
                    $charset = ($ck_default_lang=="chigb" || ($g_chinese=="gb" && $ck_default_lang=="eng")) ? "GB2312" : "big5";
            }
        }
	*/
	$charset = "utf-8";
        return $charset;
}

function returnHtmlMETA($myContent=""){

		global $favicon_image;

        $contentType = ($myContent!="") ? $myContent : "text/html";
        $charset = returnCharset();
        $rx = "<meta http-equiv='content-type' content='$contentType; charset={$charset}' />\n";
        $rx .= '<link id="page_favicon" href="/images/'.$favicon_image.'" rel="icon" type="image/x-icon" />';
        $rx .= "\n";

        return $rx;
}

function getClassNumberField($prefix="")
{
        return "if(CHAR_LENGTH({$prefix}ClassNumber)=1, CONCAT('0',{$prefix}ClassNumber), {$prefix}ClassNumber)";
}


function returnHour($name, $selected, $isEndTime=1) {
        // 2nd parameter is the hour selected
        if ($selected=="") {
                $selected = ($isEndTime) ? 23 : 0;
        }
        $x = "<select name='$name'>\n";
        for($i=0;$i<24;$i++)
                $x .= ($selected==$i) ? "<option value=\"$i\" selected>".str_pad($i,2,"0",STR_PAD_LEFT)."</option>\n" : "<option value='$i'>".str_pad($i,2,"0",STR_PAD_LEFT)."</option>\n";
        $x .= "</select>\n";
        return $x;
}

function returnMinute($name, $selected, $isEndTime=1, $displayEveryMinute=0) {
        // 2nd parameter is the minute selected
        if ($selected=="") {
                $selected = ($isEndTime) ? 55 : 0;
        }
        $x = "<select name='$name'>\n";

        $interval = ($displayEveryMinute==1) ? 1 : 5;

        for($i=0;$i<60;$i++) {
                if ($i%$interval==0) {
                        $x .= ($selected==$i) ? "<option value=\"$i\" selected>".str_pad($i,2,"0",STR_PAD_LEFT)."</option>\n" : "<option value='$i'>".str_pad($i,2,"0",STR_PAD_LEFT)."</option>\n";
                }
        }
        $x .= "</select>\n";
        return $x;
}

function splitTime($datetime) {
        if ($datetime!="") {
                $datetime = strtotime($datetime);
                $date = date("Y-m-d", $datetime);
                $hour = (int)date("H", $datetime);
                $min = (int)date("i", $datetime);
                $result = array($date, $hour, $min);
        }
        return $result;
}

function TimeStrToTimeStamp($datetime) {
	if($datetime!="")
	{
		list($date, $time) = explode(" ", $datetime);
		list($year, $month, $day) = explode("-", $date);
		list($hour, $min, $sec) = explode(":", $time);
		$result = mktime($hour, $min, $sec, $month, $day, $year);
	}
	return $result;
}

function validatePeriod($starttime, $endtime)
{
        $time_now = date('Y-m-d G:i:s');

        if($starttime=="" && $endtime=="")
                $parValid = 1;
        else if($starttime!="" && $starttime!="0000-00-00 00:00:00" && $endtime!="0000-00-00 00:00:00" && $endtime!="")
                $parValid = ($starttime<$time_now && $endtime>$time_now) ? 1 : 0;
        else
                $parValid = (($starttime!="" && $starttime!="0000-00-00 00:00:00" && $starttime<$time_now) || ($endtime!="" && $endtime!="0000-00-00 00:00:00" && $endtime>$time_now)) ? 1 : 0;

        return $parValid;
}

function validatePeriodByTimeStamp($starttime, $endtime)
{
        $time_now = time();

        if($starttime=="" && $endtime=="")
                $parValid = 1;
        else if($starttime!="" && $starttime!="0000-00-00 00:00:00" && $endtime!="0000-00-00 00:00:00" && $endtime!="")
                $parValid = (TimeStrToTimeStamp($starttime)<$time_now && TimeStrToTimeStamp($endtime)>$time_now) ? 1 : 0;
        else
                $parValid = (($starttime!="" && $starttime!="0000-00-00 00:00:00" && TimeStrToTimeStamp($starttime)<$time_now) || ($endtime!="" && $endtime!="0000-00-00 00:00:00" && TimeStrToTimeStamp($endtime)>$time_now)) ? 1 : 0;

        return $parValid;
}

##########################################################################################


	function GetColumnSize($array)
	{
		$size_unique = sizeof(array_unique($array));
		if ($size_unique==sizeof($array)/2)
		{
			return $size_unique;
		} elseif ($size_unique<sizeof($array)/2 && sizeof($array)%2==0)
		{
			# more precious detection
			$array_key = (array_keys($array));
			$int_count = 0;
			for ($i=0; $i<sizeof($array_key); $i++)
			{
				if (is_int($array_key[$i]))
				{
					$int_count ++;
				}
			}
			if ($int_count==sizeof($array)/2)
			{
				return $int_count;
			}
		}

		return sizeof($array);

	}

function removeHTMLtags($message)
{
         return strip_tags ($message);
}

#### Seems not working properly
 Function QuotedPrintableEncode($text,$header_charset="",$break_lines=1)
 {
  $length=strlen($text);
  if(strcmp($header_charset,""))
  {
   $break_lines=0;
   for($index=0;$index<$length;$index++)
   {
    $code=Ord($text[$index]);
    if($code<32
    || $code>127)
     break;
   }
   if($index>0)
    return(substr($text,0,$index).QuotedPrintableEncode(substr($text,$index),$header_charset,0));
  }
  for($whitespace=$encoded="",$line=0,$index=0;$index<$length;$index++)
  {
   $character=$text[$index];
   $order=Ord($character);
   $encode=0;
   switch($order)
   {
    case 9:
    case 32:
     if(!strcmp($header_charset,""))
     {
      $previous_whitespace=$whitespace;
      $whitespace=$character;
      $character="";
     }
     else
     {
      if(!strcmp($order,32))
       $character="_";
      else
       $encode=1;
     }
     break;
    case 10:
    case 13:
     if(strcmp($whitespace,""))
     {
      if($break_lines
      && $line+3>75)
      {
       $encoded.="=\n";
       $line=0;
      }
      $encoded.=sprintf("=%02X",Ord($whitespace));
      $line+=3;
      $whitespace="";
     }
     $encoded.=$character;
     $line=0;
     continue 2;
    default:
     if($order>127
     || $order<32
     || !strcmp($character,"=")
     || (strcmp($header_charset,"")
     && (!strcmp($character,"?")
     || !strcmp($character,"_")
     || !strcmp($character,"(")
     || !strcmp($character,")"))))
      $encode=1;
     break;
   }
   if(strcmp($whitespace,""))
   {
    if($break_lines
    && $line+1>75)
    {
     $encoded.="=\n";
     $line=0;
    }
    $encoded.=$whitespace;
    $line++;
    $whitespace="";
   }
   if(strcmp($character,""))
   {
    if($encode)
    {
     $character=sprintf("=%02X",$order);
     $encoded_length=3;
    }
    else
     $encoded_length=1;
    if($break_lines
    && $line+$encoded_length>75)
    {
     $encoded.="=\n";
     $line=0;
    }
    $encoded.=$character;
    $line+=$encoded_length;
   }
  }
  if(strcmp($whitespace,""))
  {
   if($break_lines
   && $line+3>75)
    $encoded.="=\n";
   $encoded.=sprintf("=%02X",Ord($whitespace));
  }
  if(strcmp($header_charset,"")
  && strcmp($text,$encoded))
   return("=?$header_charset?q?$encoded?=");
  else
   return($encoded);
 }

#### Functions for Staff Attendance System
function make_staff_attend_button($text, $hyperlink="", $target="", $tags="")
{
         $link_html = "<a href=\"$hyperlink\" target=\"$target\" $tags>$text</a>";
         return $link_html;

}

function parseDateTime($ParDate,$ParID="",$ParShowDate=true)
{
        global $i_frontpage_campusmail_ago, $i_frontpage_campusmail_after;

        # pls move to lang
        if ($ParID =="")
        {
	        $ParID2 = "PastDateDiv";
        }
        else
        {
	        $ParID2 = $ParID;
        }

        $ago['days'] = $i_frontpage_campusmail_ago['days'];
        $ago['hours'] = $i_frontpage_campusmail_ago['hours'];
        $ago['minutes'] = $i_frontpage_campusmail_ago['minutes'];
        $ago['seconds'] = $i_frontpage_campusmail_ago['seconds'];

        $after['days'] = $i_frontpage_campusmail_after['days'];
        $after['hours'] = $i_frontpage_campusmail_after['hours'];
        $after['minutes'] = $i_frontpage_campusmail_after['minutes'];
        $after['seconds'] = $i_frontpage_campusmail_after['seconds'];

        $diff = time() - strtotime($ParDate);

        if ($diff == 0) {
        	return;
        } else if ($diff > 0) {
                $Lang = $ago;
        } else {
                $Lang = $after;
                $diff = 0 - $diff;
        }

        if( $days = intval((floor($diff/86400))) ) $diff = $diff % 86400;
        if( $hours = intval((floor($diff/3600))) ) $diff = $diff % 3600;
        if( $minutes = intval((floor($diff/60))) ) $diff = $diff % 60;
        $seconds = intval( $diff );

        if ($days != "") {
                $DisplayStr = $days.$Lang['days'];
        } else if ($hours != "") {
                $DisplayStr = $hours.$Lang['hours'];
        } else if ($minutes != "") {
                $DisplayStr = $minutes.$Lang['minutes'];
        } else {
                $DisplayStr = $seconds.$Lang['seconds'];
        }
        if (!$ParShowDate)
        	return "<span title=\"".$ParDate."\" id=\"".$ParID2."\" ></span>";
        else
        	return "<span title=\"".$ParDate."\" id=\"".$ParID2."\" >".$DisplayStr."</span>";
}

// charset=unicode
function convert2unicode($myText, $isAbsOn="", $direction=1)
{
        global $ck_lang, $ck_default_lang, $g_encoding_unicode, $function_undefined, $intranet_default_lang;

        if (($g_encoding_unicode || $isAbsOn) && !$function_undefined['iconv'])
        {
                $lang_used = (isset($ck_lang) && $ck_lang!="") ? substr($ck_lang, 0, (strlen($ck_lang)-1)) : $ck_default_lang;
                if ($lang_used == "")
                {
                	$lang_used = $intranet_default_lang;
            	}

                $source_encoding = ($lang_used=="chigb" || $lang_used=="gb") ? "GB2312" : "BIG5";
                $myText = ($direction==1) ? iconv($source_encoding, "UTF-8//IGNORE", $myText) : iconv("UTF-8", $source_encoding."//IGNORE", $myText);
        }
        return $myText;
}

//function make_staff_attend_button($text,$hyperlink="",$target="",$tags=""){
//}

/*
* return academic year
*/
function GET_ACADEMIC_YEAR($ParDate = "")
{
	global $academic_year_start_month;

	if($academic_year_start_month == "")
	$academic_year_start_month = 9;

	if ($ParDate == "") $ParDate = Date('Y-m-d');

	$Year = date('Y', strtotime($ParDate));
	$Month = date('n', strtotime($ParDate));

	if ($Month < $academic_year_start_month) {
		$ReturnStr = $Year - 1;
	} else {
		$ReturnStr = $Year;
	}

	$ReturnStr .= " - ".($ReturnStr + 1);

	return $ReturnStr;
}

function GET_ACADEMIC_YEAR_WITH_FORMAT($ParDate)
{
	global $academic_year_start_month;

	if($academic_year_start_month == "")
	$academic_year_start_month = 9;

	if ($ParDate == "") $ParDate = Date('Y-m-d');

	$Year = date('Y', strtotime($ParDate));
	$Month = date('n', strtotime($ParDate));

	if ($Month < $academic_year_start_month) {
		$StartYear = $Year - 1;
	} else {
		$StartYear = $Year;
	}

	list($start_year, $end_year, $c, $prefix, $suffix) = GET_ACADEMIC_YEAR2();

	$ay = substr($StartYear, 4-$prefix, $prefix);
	if($c != "")
	{
		$ay .= $c;
		$ay .= substr($StartYear+1, 4-$suffix, $suffix);
	}

	return $ay;
}

function GET_ACADEMIC_YEAR2()
{
	global $intranet_root;

	$academic_yr = get_file_content("$intranet_root/file/academic_yr.txt");

        $MiddlePos 	= "";

        if(strpos($academic_yr,"-"))
		{
			$MiddlePos = strpos($academic_yr,"-");
			$c_len = 1;
		}

		if(strpos($academic_yr,"/"))
		{
			$MiddlePos = strpos($academic_yr,"/");
			$c_len = 1;
		}

        if(strpos($academic_yr," - "))
		{
			$MiddlePos = strpos($academic_yr," - ");
			$c_len = 3;
		}

		if(strpos($academic_yr," / "))
		{
			$MiddlePos = strpos($academic_yr," / ");
			$c_len = 3;
		}

		// PHP 5.3
		// Warning: substr() expects parameter 2 to be long, string given in /home/web/eclass40/intranetIP25/includes/lib.php on line 4154
        //$c = $MiddlePos > -1 ? substr($academic_yr, $MiddlePos, $c_len) : "";
        $c = ($MiddlePos > -1 && $MiddlePos!="")? substr($academic_yr, $MiddlePos, $c_len) : "";
        if($MiddlePos > -1 && $MiddlePos!="")
        {
            $Prefix 	= substr($academic_yr,0,$MiddlePos);
            $Suffix 	= substr($academic_yr,$MiddlePos+$c_len);
		}
        else
        {
        	$Prefix 	= $academic_yr;
		}
        $StartYear 	= $Prefix<2000 ? $Prefix+2000 : $Prefix;

        if(!$StartYear)
        {
        	if(date("m-d") >= "09-01")	$StartYear = date("Y");
		else
        	$StartYear = date("Y")-1;
        }

        $EndYear 	= $StartYear + 1;

        return array($StartYear, $EndYear, $c, strlen($Prefix), strlen($Suffix));
}

function GET_ACADEMIC_YEAR3($ParDate)
{
	$li = new libdb();

	$sql = "SELECT ADM.YearNameEN, ADM.YearNameB5 FROM ACADEMIC_YEAR ADM LEFT OUTER JOIN ACADEMIC_YEAR_TERM TRM ON (ADM.AcademicYearID=TRM.AcademicYearID) WHERE TermStart<='$ParDate' AND TermEnd>='$ParDate'";
	$result = $li->returnArray($sql, 2);

	return Get_Lang_Selection($result[0][1], $result[0][0]);
}

/*
* get school badge
*/
function GET_SCHOOL_BADGE()
{
	global $intranet_root;

	$imgfile = get_file_content($intranet_root."/file/schoolbadge.txt");
	$SchoolLogo = ($imgfile != "") ? "<img src=\"/file/{$imgfile}\" width=120 height=60>\n" : "";

	return $SchoolLogo;
}

/*
* get school name
*/
function GET_SCHOOL_NAME()
{
	global $intranet_root;

	$school_data = explode("\n",get_file_content($intranet_root."/file/school_data.txt"));
	$SchoolName = $school_data[0];

	return $SchoolName;
}


/**
 * Update setting sessions for IP 2.0
 */
function UPDATE_CONTROL_VARIABLE()
{
	global $SSV_PRIVILEGE;
	global $intranet_root, $plugin, $special_feature, $sys_custom,$ex_api, $webmail_SystemType;
	global $lu, $UserID, $image_path, $module_version, $intranet_db, $intranet_session_language, $eclass_db;
	global $debugMemory, $debugMemoryUserId, $accumulateMemory, $eclassAppConfig;


	$_SESSION["SSV_PRIVILEGE"]["plugin"] = $plugin;
	$_SESSION["SSV_PRIVILEGE"]["special_feature"] = $special_feature;
	$_SESSION["SSV_PRIVILEGE"]["sys_custom"] = $sys_custom;
	$_SESSION["SSV_PRIVILEGE"]["ex_api"] = $ex_api;

	####################################################################################################
	## School Data
	####################################################################################################
	if ($sys_custom['project']['HKPF']) {
	    $school_name = 'Hong Kong Police Force';
	}
	else {
    	$school_data = explode("\n",get_file_content($intranet_root."/file/school_data.txt"));
    	$school_name = $school_data[0];
	}
	$_SESSION["SSV_PRIVILEGE"]["school"]["name"] = $school_name;
	$imgfile = get_file_content($intranet_root."/file/schoolbadge.txt");
	$logo_img = $imgfile == "" ? (($_SESSION["platform"]=="KIS")? "{$image_path}/kis/logo_kis.png" : "{$image_path}/2009a/topbar_25/eclass_logo.png") : "/file/$imgfile";
	$_SESSION["SSV_PRIVILEGE"]["school"]["logo"] = $logo_img;

	####################################################################################################
	## eHomework
	####################################################################################################
	if($plugin['eHomework'])
	{
		include_once ("libhomework.php");
		include_once ("libhomework2007a.php");

		$lhomework = new libhomework2007();

		$homeworkAccess = false;
		if(!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"])
		{
			if($_SESSION['UserType']==USERTYPE_PARENT)
			{
				if($_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"])
				{
					$footer_children = $lu->getChildren();
					if (sizeof($footer_children)!=0)
					{
					    //$homeworkAccess = true;
					    $_SESSION["SSV_PRIVILEGE"]["homework"]["is_view_access"] = true;
					}
				}
			}
			else if($_SESSION['UserType']==USERTYPE_STUDENT)
			{
				//$homeworkAccess = true;
			    $_SESSION["SSV_PRIVILEGE"]["homework"]["is_view_access"] = true;
				$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] = false;

				if($_SESSION["SSV_PRIVILEGE"]["homework"]["subjectLeaderAllowed"])
				{
					if($lhomework->isSubjectLeader($UserID))
					{
					    $homeworkAccess = true;
						$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] = true;
					}
				}
			}
			else if (!$_SESSION["SSV_PRIVILEGE"]["homework"]["nonteachingAllowed"] && !$_SESSION['isTeaching'])
			{
				$homeworkAccess = false;
			}

			else
			{
				$homeworkAccess = true;
				if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
				{
					$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] = true;
				}
			}
		}
		$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] = $homeworkAccess;

		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] = true;
			$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] = true;
		}
	}
	else
	{
		$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] = true;
	}

	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	 	$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After homework = '.$memoryDiff.' / '.$accumulateMemory);
	}

	####################################################################################################
	## Access
	####################################################################################################
	include_once("libaccess.php");
	$LibAccess = new libaccess($UserID);
	$_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"] = $LibAccess->isAccessCampusmail();
	$_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_send_right"] = $LibAccess->hasSendRight();
	$_SESSION["SSV_PRIVILEGE"]["eclass"]["is_access"] = $LibAccess->retrieveAccessEClass();
	$_SESSION["SSV_PRIVILEGE"]["resource"]["is_access"] = $LibAccess->isAccessResource();

	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	 	$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After libaccess = '.$memoryDiff.' / '.$accumulateMemory);
	}

	####################################################################################################
	## Mail related
	####################################################################################################

	include_once ("libwebmail.php");
	$LibWebmail = new libwebmail();
	$_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_webmail"] = $LibWebmail->hasWebmailAccess($UserID);
	$_SESSION["SSV_PRIVILEGE"]["webmail"]["webmail_SystemType"] = ($webmail_SystemType!=""? $webmail_SystemType:1);

	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	 	$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After libwebmail = '.$memoryDiff.' / '.$accumulateMemory);
	}


	$access2eServiceeOffice = false;


	####################################################################################################
	## eNotice
	####################################################################################################
	if(isset($plugin['notice']) && $plugin['notice'])
	{
		include_once ("libnotice.php");
		$LibNotice = new libnotice();

		$_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"] = $LibNotice->hasIssueRight();
		// [2020-0604-1821-16170] store issue right of different notice type
        $_SESSION["SSV_PRIVILEGE"]["notice"]["hasSchoolNoticeIssueRight"] = $LibNotice->hasSchoolNoticeIssueRight();
        $_SESSION["SSV_PRIVILEGE"]["notice"]["hasPaymentNoticeIssueRight"] = $LibNotice->hasPaymentNoticeIssueRight();
		$_SESSION["SSV_PRIVILEGE"]["notice"]["canAccess"] = true;
		// Add eNotice PIC checking result to $_SESSION [2015-0323-1602-46073]
		$_SESSION["SSV_PRIVILEGE"]["notice"]["eNoticePIC"] = $LibNotice->isNoticePIC();

		if($_SESSION['UserType']==USERTYPE_PARENT)
		{
			# Grab children ids
			$sql = "SELECT StudentID FROM INTRANET_PARENTRELATION WHERE ParentID = '$UserID'";
			$children = $LibNotice->returnVector($sql);
			if (sizeof($children)==0)
			{
				$_SESSION["SSV_PRIVILEGE"]["notice"]["canAccess"] = false;
			}
        }

		unset($LibNotice);
	}
	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	 	$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After notice = '.$memoryDiff.' / '.$accumulateMemory);
	}
	####################################################################################################
	## eSports
	####################################################################################################
	if ($plugin['Sports'])
	{
		include_once("libsports.php");
		$lsports = new libsports();
		if ($lsports->isAdminOrHelper($UserID) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
		{
			//session_register('intranet_sports_right');
 			$_SESSION['intranet_sports_right'] = 1;

			$_SESSION["SSV_PRIVILEGE"]["eSports"]["isAdminOrHelper"] = 1;
		}
		// [2018-0628-1634-01096] for Kao Yip Class Teacher access
		else if($_SESSION['UserType'] == USERTYPE_STAFF && $sys_custom['eSports']['KaoYipRelaySettings'])
		{
		    $sql = "SELECT
    					yc.YearClassID
        			FROM
    					YEAR_CLASS as yc
          				INNER JOIN YEAR_CLASS_TEACHER yct ON (yc.YearClassID = yct.YearClassID)
			        WHERE
					    yct.UserID = '".$_SESSION['UserID']."' AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' ";
		    $resultClassAry = $lsports->returnVector($sql);
		    $_SESSION["SSV_PRIVILEGE"]["eSports"]["is_class_teacher"] = (count($resultClassAry) > 0)? true : false;

// 		    if($_SESSION["SSV_PRIVILEGE"]["eSports"]["is_class_teacher"]) {
// 		        $_SESSION['intranet_sports_right'] = 1;
// 		    }
		}
		$_SESSION["SSV_PRIVILEGE"]["eSports"]["inEnrolmentPeriod"] = $lsports->inEnrolmentPeriod();
		unset($lsports);

		if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
			$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		 	$accumulateMemory = convert_size(memory_get_usage(true));
			debug_pr('After Sports = '.$memoryDiff.' / '.$accumulateMemory);
		}

	}


	####################################################################################################
	## Swimming Gala
	####################################################################################################
	if ($plugin['swimming_gala'])
	{
		include_once("libswimminggala.php");
		$lswimminggala = new libswimminggala();
		if ($lswimminggala->isAdminOrHelper($UserID) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
		{
			//session_register('intranet_swimminggala_right');
 			$_SESSION['intranet_swimminggala_right'] = 1;

			$_SESSION["SSV_PRIVILEGE"]["swimminggala"]["isAdminOrHelper"] = 1;
		}
		$_SESSION["SSV_PRIVILEGE"]["swimminggala"]["inEnrolmentPeriod"] = $lswimminggala->inEnrolmentPeriod();
		unset($lswimminggala);

		if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
			$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		 	$accumulateMemory = convert_size(memory_get_usage(true));
			debug_pr('After libswimminggala = '.$memoryDiff.' / '.$accumulateMemory);
		}
	}

	####################################################################################################
	## eCircular - only staff include circular module
	####################################################################################################
	if($special_feature['circular'] && $_SESSION['UserType']==USERTYPE_STAFF)
	{
		include_once ("libcircular.php");
		$LibCircular = new libcircular();
		$_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] = $LibCircular->disabled;

		include_once ("libadminjob.php");
		$LibAdminJob = new libadminjob($UserID);

		# check is Circular admin (both full control or normal control)
		$_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] = $LibAdminJob->isCircularAdmin();
		if($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"])
		{
			$_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"] = $LibAdminJob->returnAdminLevel($UserID, $LibCircular->admin_id_type);
		}

		if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
			$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		 	$accumulateMemory = convert_size(memory_get_usage(true));
			debug_pr('After circular = '.$memoryDiff.' / '.$accumulateMemory);
		}
	}

	####################################################################################################
	## Check is SchoolSettings Admin
	####################################################################################################
	include_once ("libgeneralsettings.php");
	$LibGeneralSettings = new libgeneralsettings();
	$SchoolSettingsAdmin = $LibGeneralSettings->Get_General_Setting("SchoolSettings",array("'admin_user'"));
	$SchoolSettingsAdminAry = explode(",", $SchoolSettingsAdmin[admin_user]);
	$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] = in_array($UserID, $SchoolSettingsAdminAry);

	if ($sys_custom['project']['HKPF']) {
	    if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"]) {
	        $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] = true;
	    }
	}

	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	 	$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After libgeneralsettings = '.$memoryDiff.' / '.$accumulateMemory);
	}


	####################################################################################################
	## check access right for eDisciplinev12
	####################################################################################################
	if($plugin['Disciplinev12'])
	{
		include_once ("libdisciplinev12.php");
		$LibDisciplinev12 = new libdisciplinev12();

		# since student can assign to access right group, should not check "Module" access right only
		$_SESSION["SSV_PRIVILEGE"]["disciplinev12"]["has_access_right"] = ($LibDisciplinev12->CHECK_SECTION_ACCESS("DISCIPLINE-MGMT") || $LibDisciplinev12->CHECK_SECTION_ACCESS("DISCIPLINE-REPORTS") || $LibDisciplinev12->CHECK_SECTION_ACCESS("DISCIPLINE-STAT") || $LibDisciplinev12->CHECK_SECTION_ACCESS("DISCIPLINE-SETTINS")) ? true : false;

		# commented by Henry Chow on 20110414
		//$_SESSION["SSV_PRIVILEGE"]["disciplinev12"]["has_access_right"] = $LibDisciplinev12->CHECK_MODULE_ACCESS("DISCIPLINE");

		//$_SESSION["SSV_PRIVILEGE"]["disciplinev12"]["Access2eDiscipline"] = $LibDisciplinev12->CHECK_MODULE_ACCESS();

//		if($LibDisciplinev12->IS_HOY_GROUP_MEMBER()) {
//		 	$access2portfolio = true;
//		}

		if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
			$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		 	$accumulateMemory = convert_size(memory_get_usage(true));
			debug_pr('After libdisciplinev12 = '.$memoryDiff.' / '.$accumulateMemory);
		}
	}

	####################################################################################################
	## load access right for staff attendance v3
	####################################################################################################
	if($_SESSION['UserType']==USERTYPE_STAFF && $plugin['attendancestaff'] && $module_version['StaffAttendance'] == 3.0)
	{
		include_once ("libstaffattend3.php");
		$StaffAttend3 = new libstaffattend3();

		$StaffAttend3->Get_Module_Access_Right();

		if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
			$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		 	$accumulateMemory = convert_size(memory_get_usage(true));
			debug_pr('After libstaffattend3 = '.$memoryDiff.' / '.$accumulateMemory);
		}
	}

	####################################################################################################
	## Special Room
	####################################################################################################
	global $eclass_filepath, $eclass_version, $_SESSION;
//	include_once ("libeclass.php");
	include_once ("libeclass40.php");
	$header_leclass = new libeclass();


	$sql = "	SELECT
					a.user_course_id, b.RoomType,
	          		if(b.StartDate IS NULL OR UNIX_TIMESTAMP(b.StartDate)=0 OR UNIX_TIMESTAMP(now()) >UNIX_TIMESTAMP(b.StartDate), 1, 0),
	          		if(b.EndDate IS NULL OR UNIX_TIMESTAMP(b.EndDate)=0 OR UNIX_TIMESTAMP(now())<UNIX_TIMESTAMP(b.EndDate), 1, 0)
	          	FROM
					$eclass_db.user_course as a
	        	LEFT OUTER JOIN
	        		$eclass_db.course as b ON a.course_id = b.course_id
	        	WHERE
	        		a.status IS NULL AND (b.RoomType>0) AND a.user_email = '".$lu->UserEmail."'
	        ";
	$temp = $header_leclass->returnArray($sql, 4);

	for ($i=0; $i<sizeof($temp); $i++)
	{
		if ($temp[$i][1]==1)
		{
		        $access2readingrm = true;
		        $readingRmID = $temp[$i][0];
		}
		elseif ($temp[$i][1]==2 && $header_leclass->license_elp!=0)
		{
		        $access2elprm = true;
		}
		elseif ($temp[$i][1]==3 && $temp[$i][2] && $temp[$i][3])
		{
		        $access2ssrm = true;
		}
		elseif ($temp[$i][1]==4 && $header_leclass->license_iPortfolio!=0)
		{
		     # check whether the account is suspended
		     $sql = "	SELECT
		     				IsSuspend
		     			FROM
		     				{$eclass_db}.PORTFOLIO_STUDENT
		     			Where
		     				UserID = '$UserID'
		     			";
		     $row = $header_leclass->returnVector($sql);

			if($row[0]!=1)
				$access2portfolio = true;
		}
	}
	# check parent's right to iPortfolio
	if($_SESSION['UserType']==USERTYPE_PARENT)
	{
	        $special_rooms = $header_leclass->getChildrenSpecialCourseMenu($UserID);

	        for ($i=0; $i<sizeof($special_rooms); $i++)
	        {
	                if ($special_rooms[$i][12]==4 && $header_leclass->license_iPortfolio!=0)
	                {
	                        $access2portfolio = true;
	                }
	        }
	}


	/* commented by Henry Chow n 2012-07-09
	if(isset($_SESSION['intranet_iportfolio_admin'] ) && $_SESSION['intranet_iportfolio_admin']==1) {
		$access2portfolio = true;
	}
	*/
	# add $plugin['ScrabbleFC'] by Kelvin 2010-03-26
	$access2specialrm = ($access2readingrm || $access2elprm || $access2ssrm || $plugin['ScrabbleFC']);
	$_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2SpecialRm"] = $access2specialrm;
	$_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"] = $access2portfolio;
	$_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Readingrm"] = $access2readingrm;
	$_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Elprm"] = $access2elprm;
	$_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Ssrm"] = $access2ssrm;

	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	 	$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After special room = '.$memoryDiff.' / '.$accumulateMemory);
	}



	####################################################################################################
	## Clear user upload files via fckeditor
	####################################################################################################
	include_once("libfilesystem.php");
	$lf = new libfilesystem();
	$lf->deleteAllFckImageTmpFiles();

	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	 	$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After Clear user upload files via fckeditor = '.$memoryDiff.' / '.$accumulateMemory);
	}

	####################################################################################################
	## Clear user upload files via fckeditor [End]
	####################################################################################################


	####################################################################################################
	## eReportCard - Check if the user is class/subject teacher in the ACTIVE year of eRC [Start]
	####################################################################################################
	if ($plugin['ReportCard2008'])
	{
		### Do not include the libreportcard.php due to large memory usage...
		$eRC_ActiveYear = get_file_content($intranet_root."/file/reportcard2008/db_year.txt");

		include_once("form_class_manage.php");

		$libAcademicYear = new academic_year();
		$AcademicYearInfoArr = $libAcademicYear->Get_Academic_Year_Info_By_YearName($eRC_ActiveYear);
		$eRC_ActiveYearID = $AcademicYearInfoArr[0]['AcademicYearID'];

		### Check Class Teacher
		$sql = "Select
						Distinct(yct.YearClassID)
				From
						YEAR_CLASS_TEACHER as yct
						Inner Join
						YEAR_CLASS as yc
						On (yct.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$eRC_ActiveYearID."')
				Where
						yct.UserID = '".$UserID."'
				";
		$resultSet = $libAcademicYear->returnVector($sql);
		$_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_class_teacher"] = (count($resultSet) > 0)? true : false;

		### Check Subject Teacher
		$sql = "Select
						Distinct(stc.SubjectGroupID)
				From
						SUBJECT_TERM_CLASS_TEACHER as stct
						Inner Join
						SUBJECT_TERM_CLASS as stc
						ON (stct.SubjectGroupID = stc.SubjectGroupID AND stct.UserID = '$UserID')
						Inner Join
						SUBJECT_TERM as st ON (stc.SubjectGroupID = st.SubjectGroupID)
						Inner Join
						ACADEMIC_YEAR_TERM as ayt ON (st.YearTermID = ayt.YearTermID AND ayt.AcademicYearID = '".$eRC_ActiveYearID."')
				";
		$resultSet = $libAcademicYear->returnVector($sql);
		$_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_subject_teacher"] = (count($resultSet) > 0)? true : false;

		### Check Subject Panel
		$sql = "SELECT UserID From ASSESSMENT_ACCESS_RIGHT WHERE AccessType = '1' And UserID = '".$UserID."'";
		$resultSet = $libAcademicYear->returnVector($sql);
		$_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_subject_panel"] = (count($resultSet) > 0)? true : false;

		### Check View Group Member
		if($sys_custom['eRC']['Settings']['ViewGroupUserType'])
		{
			$table = $intranet_db."_DB_REPORT_CARD_".$eRC_ActiveYear.".RC_GROUP_MEMBER";
			$sql = "SELECT UserID FROM $table WHERE GroupType = 'VIEW' And UserID = '".$UserID."' ";
			$resultSet = $libAcademicYear->returnVector($sql);
			$_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_view_group_memeber"] = (count($resultSet) > 0)? true : false;

//			if($_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_view_group_memeber"] && $UserID==1) {
//				$_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard"] = false;
//			}
		}

		# Load Settings
		$table = $intranet_db."_DB_REPORT_CARD_".$eRC_ActiveYear.".RC_SETTINGS";
		$sql  = "SELECT SettingKey, SettingValue
				FROM $table
				WHERE
				SettingCategory = 'AccessSettings'
				";
		$accessSettingResult = $libAcademicYear->returnArray($sql);

		$accessSetting = array();
		for ($i=0; $i<sizeof($accessSettingResult); $i++) {
			$thisSettingValue[$accessSettingResult[$i]["SettingKey"]]= $accessSettingResult[$i]["SettingValue"];
		}
		$_SESSION["SSV_PRIVILEGE"]["reportcard"]["EnableVerificationPeriod"] = $thisSettingValue[EnableVerificationPeriod];

		unset($libAcademicYear);
		unset($eRC_ActiveYear);
		unset($eRC_ActiveYearID);
		unset($sql);
		unset($resultSet);

		if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
			$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		 	$accumulateMemory = convert_size(memory_get_usage(true));
			debug_pr('After eReportCard = '.$memoryDiff.' / '.$accumulateMemory);
		}
	}
	####################################################################################################
	## eReportCard - Check if the user is class/subject teacher in the ACTIVE year of eRC [End]
	####################################################################################################


	####################################################################################################
	## eReportCard (Rucrics) - Check if the user is class/subject teacher in the ACTIVE year of eRC Rubrics [Start]
	####################################################################################################
	if ($plugin['ReportCard_Rubrics'])
	{
		$SettingsArr = $LibGeneralSettings->Get_General_Setting('eReportCard_Rubrics', array("'ActiveAcademicYearID'"));
		$eRC_ActiveYearID = $SettingsArr['ActiveAcademicYearID'];

		### Check Class Teacher
		$sql = "Select
						Distinct(yct.YearClassID)
				From
						YEAR_CLASS_TEACHER as yct
						Inner Join
						YEAR_CLASS as yc
						On (yct.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$eRC_ActiveYearID."')
				Where
						yct.UserID = '".$UserID."'
				";
		$resultSet = $LibGeneralSettings->returnVector($sql);
		$_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["is_class_teacher"] = (count($resultSet) > 0)? true : false;

		### Check Subject Teacher
		$sql = "Select
						Distinct(stc.SubjectGroupID)
				From
						SUBJECT_TERM_CLASS_TEACHER as stct
						Inner Join
						SUBJECT_TERM_CLASS as stc
						ON (stct.SubjectGroupID = stc.SubjectGroupID AND stct.UserID = '$UserID')
						Inner Join
						SUBJECT_TERM as st ON (stc.SubjectGroupID = st.SubjectGroupID)
						Inner Join
						ACADEMIC_YEAR_TERM as ayt ON (st.YearTermID = ayt.YearTermID AND ayt.AcademicYearID = '".$eRC_ActiveYearID."')
				";
		$resultSet = $LibGeneralSettings->returnVector($sql);
		$_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["is_subject_teacher"] = (count($resultSet) > 0)? true : false;

		### Check Page Access Right
		$ActiveDB = $intranet_db."_DB_REPORT_CARD_RUBRICS_".$eRC_ActiveYearID;
		$RC_ADMIN_GROUP = $ActiveDB.'.RC_ADMIN_GROUP';
		$RC_ADMIN_GROUP_USER = $ActiveDB.'.RC_ADMIN_GROUP_USER';
		$RC_ADMIN_GROUP_RIGHT = $ActiveDB.'.RC_ADMIN_GROUP_RIGHT';
		$sql = "Select
						agr.AdminGroupRightName
				From
						$RC_ADMIN_GROUP_USER as agu
						Inner Join
						$RC_ADMIN_GROUP as ag On (ag.AdminGroupID = agu.AdminGroupID)
						Inner Join
						$RC_ADMIN_GROUP_RIGHT as agr On (ag.AdminGroupID = agr.AdminGroupID)
				Where
						agu.UserID = '".$_SESSION['UserID']."'
						And ag.RecordStatus = 1
				";
		$_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["PageAccessRightArr"] = $LibGeneralSettings->returnVector($sql);

		unset($eRC_ActiveYearID);
		unset($sql);
		unset($resultSet);

		if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
			$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		 	$accumulateMemory = convert_size(memory_get_usage(true));
			debug_pr('After eReportCard Rubrics = '.$memoryDiff.' / '.$accumulateMemory);
		}
	}
	####################################################################################################
	## eReportCard (Rucrics) - Check if the user is class/subject teacher in the ACTIVE year of eRC Rubrics [End]
	####################################################################################################


	####################################################################################################
	## eReportCard (Kindergarten) - Check if the user is class/subject teacher in the ACTIVE year of eRC [Start]
	####################################################################################################
	if ($plugin['ReportCardKindergarten'])
	{
	    include_once("includes/reportcard_kindergarten/config.inc.php");
	    $SettingsArr = $LibGeneralSettings->Get_General_Setting($ercKindergartenConfig['moduleCode'], array("'ActiveAcademicYearID'"));
	    $eRC_ActiveYearID = $SettingsArr['ActiveAcademicYearID'];

	    ### Check Class Teacher
	    $sql = "SELECT
					DISTINCT(yct.YearClassID)
				FROM
                    YEAR_CLASS_TEACHER as yct
					INNER JOIN YEAR_CLASS as yc ON (yct.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$eRC_ActiveYearID."')
					INNER JOIN YEAR as y ON (yc.YearID = y.YearID)
				WHERE
                    yct.UserID = '".$UserID."' AND
					y.YearName IN ('".implode("', '", (array)$ercKindergartenConfig['formLevel'])."')";
	    $resultSet = $LibGeneralSettings->returnVector($sql);
	    $_SESSION["SSV_PRIVILEGE"]["reportcard_kindergarten"]["is_class_teacher"] = (count($resultSet) > 0)? true : false;

	    ### Check Subject Teacher
        $sql = "SELECT
	                DISTINCT(stc.SubjectGroupID)
	            FROM
	                SUBJECT_TERM_CLASS_TEACHER as stct
	                INNER JOIN SUBJECT_TERM_CLASS as stc ON (stct.SubjectGroupID = stc.SubjectGroupID AND stct.UserID = '$UserID')
	                INNER JOIN SUBJECT_TERM as st ON (stc.SubjectGroupID = st.SubjectGroupID)
	                INNER JOIN ACADEMIC_YEAR_TERM as ayt ON (st.YearTermID = ayt.YearTermID AND ayt.AcademicYearID = '".$eRC_ActiveYearID."')
                    INNER JOIN SUBJECT_TERM_CLASS_YEAR_RELATION as stcy ON (stcy.SubjectGroupID = stc.SubjectGroupID)
					INNER JOIN YEAR as y ON (stcy.YearID = y.YearID)
                    INNER JOIN ASSESSMENT_SUBJECT as s ON (st.SubjectID = s.RecordID AND (CMP_CODEID IS NULL OR CMP_CODEID = ''))
                WHERE
					y.YearName IN ('".implode("', '", (array)$ercKindergartenConfig['formLevel'])."')
                    /*AND s.CODEID IN ('".implode("', '", (array)$ercKindergartenConfig['subjectCode'])."')*/ ";
	    $resultSet = $LibGeneralSettings->returnVector($sql);
	    $_SESSION["SSV_PRIVILEGE"]["reportcard_kindergarten"]["is_subject_teacher"] = (count($resultSet) > 0)? true : false;

	    ### Check Admin Group Access
	    global $intranet_db;
	    $prefix = $intranet_db."_DB_REPORT_CARD_KINDERGARTEN_";
	    $thisdbName = $prefix.$eRC_ActiveYearID;
	    $RC_ADMIN_GROUP = $thisdbName.'.RC_ADMIN_GROUP';
	    $RC_ADMIN_GROUP_USER = $thisdbName.'.RC_ADMIN_GROUP_USER';
	    $RC_ADMIN_GROUP_RIGHT = $thisdbName.'.RC_ADMIN_GROUP_RIGHT';

	    $sql = "Select
							agr.AdminGroupRightName
					From
							$RC_ADMIN_GROUP_USER as agu
							Inner Join
							$RC_ADMIN_GROUP as ag On (ag.AdminGroupID = agu.AdminGroupID)
							Inner Join
							$RC_ADMIN_GROUP_RIGHT as agr On (ag.AdminGroupID = agr.AdminGroupID)
					Where
							agu.UserID = '".$UserID."'
							And ag.RecordStatus = 1
					";
	    $resultSet = $LibGeneralSettings->returnVector($sql);
	    $_SESSION["SSV_PRIVILEGE"]["reportcard_kindergarten"]["is_admin_group_member"] = (count($resultSet) > 0)? true : false;

	    unset($RC_ADMIN_GROUP);
	    unset($RC_ADMIN_GROUP_USER);
	    unset($RC_ADMIN_GROUP_RIGHT);
	    unset($eRC_ActiveYearID);
	    unset($sql);
	    unset($resultSet);

	    if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
	        $memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	        $accumulateMemory = convert_size(memory_get_usage(true));
	        debug_pr('After eReportCard Kindergarten = '.$memoryDiff.' / '.$accumulateMemory);
	    }
	}
	####################################################################################################
	## eReportCard (Kindergarten) - Check if the user is class/subject teacher in the ACTIVE year of eRC [End]
	####################################################################################################


	####################################################################################################
	## eEnrol - Get Admin Status [Start]
	####################################################################################################
	if ($plugin['eEnrollment']) {
		include_once("includes/libclubsenrol.php");
		$libenroll = new libclubsenrol();

		$_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_enrol_admin"] = $libenroll->IS_ENROL_ADMIN($UserID);
		$_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_enrol_master"] = $libenroll->IS_ENROL_MASTER($UserID);
		$_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_club_pic"] = $libenroll->IS_CLUB_PIC();
		$_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_activity_pic"] = $libenroll->IS_EVENT_PIC();
		$_SESSION["SSV_PRIVILEGE"]["enrollment"]["have_club_mgt"] = $libenroll->HAVE_CLUB_MGT();
		$_SESSION["SSV_PRIVILEGE"]["enrollment"]["have_activity_mgt"] = $libenroll->HAVE_EVENT_MGT();
		$_SESSION["SSV_PRIVILEGE"]["enrollment"]["haveGroupAdminWithUserMgmtRight"] = $libenroll->checkAnyIntranetGroupAdminWithUserMgmtRight();
		$_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_club_helper"] = $libenroll->IS_CLUB_HELPER();
		$_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_activity_helper"] = $libenroll->IS_EVENT_HELPER();

		unset($libenroll);

		if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
			$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		 	$accumulateMemory = convert_size(memory_get_usage(true));
			debug_pr('After libclubsenrol = '.$memoryDiff.' / '.$accumulateMemory);
		}
	}
	####################################################################################################
	## eEnrol - Get Admin Status [End]
	####################################################################################################


	####################################################################################################
	## eService - eDisciplinev12 [Start], check teacher has access right to view his/her own class
	####################################################################################################
	include_once("includes/libdisciplinev12.php");
	$ldiscipline = new libdisciplinev12();
	$result = $ldiscipline->checkTeacherAccessEserviceRight($UserID);
	$_SESSION["SSV_PRIVILEGE"]["disciplinev12"]["canAccess_eServive"] = $result;

	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	 	$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After libdisciplinev12 = '.$memoryDiff.' / '.$accumulateMemory);
	}

	####################################################################################################
	## eService - eDisciplinev12 [End]
	####################################################################################################


	if($plugin['IES']) {
		####################################################################################################
		## eLearning - IES, check whether the user can access IES [Start]
		####################################################################################################
		$_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"] = 0;
		$_SESSION["SSV_PRIVILEGE"]["IES"]["isStudent"] = 0;

		include_once("includes/ies/libies.php");
		$libies = new libies();
		$result = $libies->isIES_Teacher($UserID);

		if($result == 1){
			//is a teacher , set the session
			$_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"] = $result;
		}else{
			//not a teacher , check whether is a student
			$result = $libies->isIES_Student($UserID);
			$_SESSION["SSV_PRIVILEGE"]["IES"]["isStudent"] = $result;
		}

		if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
			$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		 	$accumulateMemory = convert_size(memory_get_usage(true));
			debug_pr('After IES = '.$memoryDiff.' / '.$accumulateMemory);
		}


		####################################################################################################
		## eLearning - IES [End]
		####################################################################################################
	}
	if($plugin['medical']) {
		####################################################################################################
		## eAdmin - Medical, check whether the user can access Medical [Start]
		####################################################################################################
		$_SESSION["SSV_PRIVILEGE"]["medical"]["isMedicalUser"] = 0;
//		$_SESSION["SSV_PRIVILEGE"]["medical"]["parent"] = 0;

		include_once("includes/cust/medical/libMedical.php");
		$libMedical = new libMedical();
		$result = $libMedical->isMedicalUser($UserID);

		if($result){
			$_SESSION["SSV_PRIVILEGE"]["medical"]["isMedicalUser"] = 1;
		}

		if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
			$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		 	$accumulateMemory = convert_size(memory_get_usage(true));
			debug_pr('After medical = '.$memoryDiff.' / '.$accumulateMemory);
		}

		####################################################################################################
		## eAdmin - Medical [End]
		####################################################################################################
	}

	###################################################################
	### eClass & modules version (store versions in $_SESSION) [Start]
	###################################################################
	include_once("libdb.php");
	$libdb = new libdb();

	$sql = "SELECT DISTINCT Module FROM ECLASS_MODULE_VERSION_LOG";
	$module = $libdb->returnVector($sql);

	for($i=0; $i<sizeof($module); $i++) {
		$version = getEclassModuleVersionInUse($module[0]);
		$_SESSION['VERSION'][$module[0]] = $version;
	}

	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	 	$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After eClass & modules version (store versions in $_SESSION) = '.$memoryDiff.' / '.$accumulateMemory);
	}

	###################################################################
	### eClass & modules version (store versions in $_SESSION) [End]
	###################################################################


	###################################################################
	### check Access Right of upload "Offical Photo" [Start]
	###################################################################

	include_once($intranet_root."/includes/libaccountmgmt.php");
	$laccount = new libaccountmgmt();
	$officalPhotoMgmtMember = $laccount->getOfficalPhotoMgmtMemberID();
	if(sizeof($officalPhotoMgmtMember) && in_array($UserID, $officalPhotoMgmtMember))
		$_SESSION["SSV_PRIVILEGE"]["canAccessOfficalPhotoSetting"] = 1;

	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	 	$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After check Access Right of upload "Offical Photo" = '.$memoryDiff.' / '.$accumulateMemory);
	}

	###################################################################
	### check Access Right of upload "Offical Photo" [End]
	###################################################################


	####################################################################################################
	## Class Teacher
	####################################################################################################
	$sql = "Select
					Count(*)
			From
					YEAR_CLASS as yc
      				Inner Join YEAR_CLASS_TEACHER yct On (yc.YearClassID = yct.YearClassID)
			Where
					yct.UserID = '".$_SESSION['UserID']."'
					And yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
			";
	$ClassCountArr = $libdb->returnArray($sql);
	$ClassCount = $ClassCountArr[0][0];
	$_SESSION["USER_BASIC_INFO"]["is_class_teacher"] = ($ClassCount==0)? 0 : 1;
	####################################################################################################
	## Class Teacher [End]
	####################################################################################################


	####################################################################################################
	## eBooking [Start]
	####################################################################################################
	if ($plugin['eBooking']) {
		include_once($intranet_root."/includes/libebooking.php");
		$lebooking = new libebooking();
		$ruleInfoAry = $lebooking->Get_User_Booking_Rule_Of_User($_SESSION['UserID']);
		if (count($ruleInfoAry) > 0) {
			$_SESSION["eBooking"]["canAccesseService"] = true;
		}

		if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
			$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		 	$accumulateMemory = convert_size(memory_get_usage(true));
			debug_pr('After eBooking = '.$memoryDiff.' / '.$accumulateMemory);
		}
	}
	####################################################################################################
	## eBooking [End]
	####################################################################################################

	####################################################################################################
	## eProcurement [Start]
	####################################################################################################
	if($plugin['ePCM'] && $_SESSION['UserType']==USERTYPE_STAFF ){
		if( !$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePCM"]){
			$sql = "select
					iu.UserID ,
					GROUP_CONCAT( groupmem.GroupMemberID) as isGroupUser,
					GROUP_CONCAT( rolemem.RoleUserID) as isRoleUser
					FROM
					INTRANET_USER as iu
					left join INTRANET_PCM_GROUP_MEMBER as groupmem on iu.UserID = groupmem.UserID
					left join INTRANET_PCM_ROLE_USER as rolemem on iu.UserID = rolemem.UserID
					where iu.UserID = '".$_SESSION['UserID']."'
					Group By iu.UserID ;";
			$result = $libdb->returnResultSet($sql);

			if($result[0]['isGroupUser'] || $result[0]['isRoleUser']){
				$_SESSION['ePCM']['isUser'] = 1;
			}
		}
		else{
			$_SESSION['ePCM']['isAdmin'] = 1;
		}
		if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
			$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
			$accumulateMemory = convert_size(memory_get_usage(true));
			debug_pr('After eProcurement = '.$memoryDiff.' / '.$accumulateMemory);
		}
	}
	####################################################################################################
	## eProcurement [End]
	####################################################################################################

	######################################################################################
	## SBA [Start]
	######################################################################################
	if(count($plugin['SBA']) > 0) {

		#############################################################################################
		## eLearning - SBA, check whether the user can access SBA [Start]
		#############################################################################################
		$_SESSION["SSV_PRIVILEGE"]["SBA"]["isTeacher"] = 0;
		$_SESSION["SSV_PRIVILEGE"]["SBA"]["isStudent"] = 0;

		include_once($intranet_root."/includes/sba/libSba.php");
		$libSba = new libSba();
		$result = $libSba->isSBA_Teacher($UserID);

		if($result == 1){
			//is a teacher , set the session
			$_SESSION["SSV_PRIVILEGE"]["SBA"]["isTeacher"] = $result;
		}else{
			//not a teacher , check whether is a student
			$result = $libSba->isSBA_Student($UserID);
			$_SESSION["SSV_PRIVILEGE"]["SBA"]["isStudent"] = $result;
		}

		if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
			$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		 	$accumulateMemory = convert_size(memory_get_usage(true));
			debug_pr('After SBA = '.$memoryDiff.' / '.$accumulateMemory);
		}
		#############################################################################################
		## eLearning - SBA [End]
		#############################################################################################
	}
	#############################################################################################
	## eAdmin - Teacher Portfolio
	#############################################################################################
	if ($plugin["TeacherPortfolio"]) {
		$sql = "SELECT TPUserID FROM INTRANET_TEACHER_PORTFOLIO_USER WHERE UserID='" . $_SESSION['UserID'] . "'";
		$result = $libdb->returnResultSet($sql);
		if ($result != null && count($result) > 0) {
			$_SESSION["SSV_PRIVILEGE"]["TeacherPortfolio"]["TPUserID"] = $result[0]["TPUserID"];
			$_SESSION["SSV_PRIVILEGE"]["TeacherPortfolio"]["isViewerUser"] = 1;
		}
	}
	#############################################################################################
	## eAdmin - Teacher Portfolio [End]
	#############################################################################################

	#############################################################################################
	## eService - eAppraisal [Start]
	#############################################################################################
	if ($plugin["eAppraisal"] && $_SESSION['UserType']==USERTYPE_STAFF) {
		$sql = "SELECT COUNT(CycleID) as Total FROM INTRANET_PA_C_CYCLE WHERE NOW() BETWEEN CycleStart AND CycleClose";
		$result = $libdb->returnResultSet($sql);
		if ($result != null && count($result) > 0) {
			if ($result["0"]["Total"] > 0) {
				$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["hasAppraisal"] = true;
			}
		}
	}
	#############################################################################################
	## eService - eAppraisal [End]
	#############################################################################################

	#############################################################################################
	## Check user can access "student update password (pop-up)" [START]
	## Can access = role admin + staff (set with staff account setting option)
	#############################################################################################
	$_SESSION["CanAccessUpdateStudentPwdPopup"] = false;
	if($_SESSION["SSV_USER_ACCESS"]["other-UpdateStudentPwdPopUp"])
	{
		$_SESSION["CanAccessUpdateStudentPwdPopup"] = true;
	}
	#############################################################################################
	## Check user can access "student update password (pop-up)"  [END]
	#############################################################################################

	#############################################################################################
	## Check Class Teacher / Club PIC / Activity PIC Send Push Message Right (Staff only) [START]
	#############################################################################################
	if ($_SESSION['UserType']==USERTYPE_STAFF && $plugin['eClassApp'] &&
	($_SESSION["USER_BASIC_INFO"]["is_class_teacher"]
	 || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_club_pic"]
	 || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_activity_pic"]
	 )) {
		include_once($intranet_root."/includes/eClassApp/eClassAppConfig.inc.php");

		$sql = "SELECT SettingValue FROM INTRANET_APP_SETTINGS WHERE SettingName = '".$eclassAppConfig['pushMessageSendRight']['classTeacher'] ."'";
		$result = $libdb->returnArray($sql);
		$classTeacherPushMessageRight = $result[0][0];
		$sql = "SELECT SettingValue FROM INTRANET_APP_SETTINGS WHERE SettingName = '".$eclassAppConfig['pushMessageSendRight']['clubAndActivityPIC'] ."'";
		$result = $libdb->returnArray($sql);
		$PICPushMessageRight = $result[0][0];

		//$_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage"] = ($classTeacherPushMessageRight || $PICPushMessageRight) ? 1 : 0;
		$_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_classTeacher"] = ($_SESSION["USER_BASIC_INFO"]["is_class_teacher"] && $classTeacherPushMessageRight) ? 1 : 0;
		$_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_clubAndActivityPIC"] = (($_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_club_pic"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_activity_pic"]) && $PICPushMessageRight) ? 1 : 0;
		$_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage"] = ($_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_classTeacher"] || $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_clubAndActivityPIC"]) ? 1 : 0;
	}
	#############################################################################################
	## Check Class Teacher / Club PIC / Activity PIC Send Push Message Right (Staff only) [END]
	#############################################################################################

	#############################################################################################
	## Check App Group Message group admin [START]
	#############################################################################################
	if ($plugin['eClassApp'] || $plugin['eClassTeacherApp']) {
		include_once($intranet_root."/includes/eClassApp/eClassAppConfig.inc.php");
		$sql = "Select
						gm.MemberID
				From
						INTRANET_APP_MESSAGE_GROUP as g
						INNER JOIN INTRANET_APP_MESSAGE_GROUP_MEMBER as gm ON (g.GroupID = gm.GroupID)
				Where
						g.RecordStatus = '".$eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['RecordStatus']['active']."'
						AND gm.MemberType = '".$eclassAppConfig['INTRANET_APP_MESSAGE_GROUP_MEMBER']['MemberType']['admin']."'
						AND gm.UserID = '".$_SESSION['UserID']."'
				";
		$memberAry = $libdb->returnResultSet($sql);
		if (count($memberAry) > 0) {
			$_SESSION["SSV_PRIVILEGE"]["eClassApp"]["GroupMessage_GroupAdmin"] = 1;
		}
	}
	#############################################################################################
	## Check App Group Message group admin [END]
	#############################################################################################

	#############################################################################################
	## Check Class Teacher if the flag is on [START]
	#############################################################################################
	if($plugin['eClassApp']){
		$sql = "SELECT yc.YearClassID
					FROM YEAR_CLASS yc
					INNER JOIN YEAR_CLASS_TEACHER yct
					ON yc.YearClassID = yct.YearClassID
				WHERE yc.AcademicYearID = '" . Get_Current_Academic_Year_ID() . "'
					  AND yct.UserID = '" . $_SESSION['UserID'] . "'
				";
		$classAry = $libdb->returnVector($sql);
		if (count($classAry) > 0){
			$_SESSION["SSV_PRIVILEGE"]["eClassApp"]['classTeacher'] = $classAry;
		}
	}
	#############################################################################################
	## Check Class Teacher if the flag is on [END]
	#############################################################################################

	#############################################################################################
	## Disable app admin if the flag is turned off [START]
	#############################################################################################
	if (!$plugin['eClassApp']) {
		unset($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassApp"]);
		unset($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassStudentApp"]);
		unset($_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"]);
		unset($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAppNotify"]);
	}
	if (!$plugin['eClassTeacherApp']) {
		unset($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"]);
		unset($_SESSION["SSV_USER_ACCESS"]["eAdmin-TeacherAppNotify"]);
	}
	#############################################################################################
	## Disable app admin if the flag is turned off [END]
	#############################################################################################


	unset($lhomework, $lf, $header_leclass, $header_leclass, $LibAccess, $LibWebmail);

	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	 	$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('End UPDATE_CONTROL_VARIABLE = '.$memoryDiff.' / '.$accumulateMemory);
	}
}

function curPageURL($withQueryString=1, $withPageSuffix=1) {
	 $pageSuffix = ($withQueryString==1)? $_SERVER["REQUEST_URI"] : $_SERVER["SCRIPT_NAME"];
	 $pageURL = 'http';
	 //if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	 if (checkHttpsWebProtocol()) {$pageURL .= "s";}
	 $pageURL .= "://";
	 if ($_SERVER["SERVER_PORT"] != "80") {
	  //$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
	 } else {
	  //$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	  $pageURL .= $_SERVER["SERVER_NAME"];
	 }

	 if ($withPageSuffix) {
	 	$pageURL .= $pageSuffix;
	 }

	 return $pageURL;
}

function GET_CSV($ParCsvName, $ParSpecificPath='', $AppendUnicodeSuffix=1) {

	global $g_encoding_unicode, $intranet_httppath;

	//if ($g_encoding_unicode) {
		$ext = strtolower(substr($ParCsvName, strrpos($ParCsvName,".")));
		$filename = substr($ParCsvName, 0, strpos($ParCsvName,"."));
		//$ParCsvName = $filename."_unicode".$ext;
		$ParCsvName = ($AppendUnicodeSuffix)? $filename."_unicode".$ext : $filename.$ext;

	//}

	return $intranet_httppath."/templates/get_sample_csv.php?file=".$ParCsvName."&path=".$ParSpecificPath;
}

function logTapCardResults($ParStr)
{
	global $log_tapcard_results, $PATH_WRT_ROOT, $file_path, $log_tapcard_extra_data;
	if($log_tapcard_results !== true) {
		return;
	}

	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	$lf = new libfilesystem();
	if(!is_dir($file_path."/file/log_tap_card_results")){
		$lf->folder_new($file_path."/file/log_tap_card_results");
	}

	$files = $lf->return_files($file_path."/file/log_tap_card_results");
	if($files) {
		$filedate_to_delete = strtotime(date("Y-m-01", strtotime("-3 month")));
		foreach($files as $temp) {
			$path_parts = pathinfo($temp);
			$filename = str_replace(array('log_tap_card_result_', '.txt'), '', $path_parts['basename']);
			$filename = str_replace('_', '-', $filename);
			$filename .= '-01';
			$filedate = strtotime($filename);
			if($filedate != false && $filedate != -1) {
				if($filedate_to_delete > $filedate) {
					$lf->file_remove($temp);
				}
			}
		}
	}

	$log_filepath = "$file_path/file/log_tap_card_results/log_tap_card_result_".date("Y_m").".txt";
	$log_content = date("Y-m-d H:i:s")." ".$_SERVER['REQUEST_URI']."\r\n";
	$trace = debug_backtrace();
	if(is_array($trace)) {
		foreach ($trace as $temp) {
			if($temp['function'] == 'output_env_str') {
				$log_content .= "line no: ".$temp['line']."\r\n";
				break;
			}
		}
	}
	if($log_tapcard_extra_data != '') {
		$log_content .= $log_tapcard_extra_data."\r\n";
	}
	$log_content .= $ParStr."\r\n";
	$fp = fopen($log_filepath, 'a');
	if($fp) {
		fwrite($fp, $log_content);
		fclose($fp);
	}
}

function logBlockFileUploadExtensions($filename)
{
	global $intranet_root, $REMOTE_ADDR;

	include_once("$intranet_root/includes/libfilesystem.php");

	$lf = new libfilesystem();
	$logfile_path = $intranet_root."/file/"."block_file_upload_extensions.log";

	$logfile_content = @$lf->file_read($logfile_path);

	$logfile_entry = '';
	if ($logfile_content!="")
	{
		# load previous records
		$logfile_entry .= $logfile_content;
	}

	# add new record
	$logfile_entry .= 'UserID: '.$_SESSION['UserID'].', IP: '.$REMOTE_ADDR.', Url: '.$_SERVER["REQUEST_URI"].', Datetime: '.date("Y-m-d H:i:s").', FileName: '.$filename."\r\n";
	//var_dump($logfile_entry_arr);
	@$lf->file_write($logfile_entry, $logfile_path);
}

function output_env_str($ParStr) {
	// commented by kenneth chung on 2009-08-14, as this kind of convertion consider useless
	/*global $g_encoding_unicode, $intranet_default_lang;

	if ($g_encoding_unicode) {
		if ($intranet_default_lang == "b5") $ChangeType = "BIG5";
		if ($intranet_default_lang == "gb") $ChangeType = "GB2312";
		$ParStr = iconv('UTF-8', "$ChangeType//IGNORE", $ParStr);
	}*/
	logTapCardResults($ParStr);
	return $ParStr;
}

// get Time selection box
function getTimeSel($ParName, $ParSelHour = 0, $ParSelMin = 0, $OnChange="", $minuteStep=5) {

	$ReturnStr = "<select id=\"{$ParName}Hour\" name=\"{$ParName}Hour\" onchange=\"$OnChange\">";
	for ($i = 0; $i < 24; $i++) {
		($i < 10) ? $temp = "0".$i : $temp = $i;
		$ReturnStr .= "<option value=\"{$temp}\"";
		if ($temp == $ParSelHour) $ReturnStr .= "selected";
		$ReturnStr .= ">{$temp}</option>";
	}
	$ReturnStr .= "</select> : ";
	$ReturnStr .= "<select id=\"{$ParName}Min\" name=\"{$ParName}Min\" onchange=\"$OnChange\">";
	$minUpper = ($minuteStep==1) ? 60 : 56;
	for ($i = 0; $i < $minUpper; $i += $minuteStep) {
		($i < 10) ? $temp = "0".$i : $temp = $i;
		$ReturnStr .= "<option value=\"{$temp}\"";
		if ($temp == $ParSelMin) $ReturnStr .= "selected";
		$ReturnStr .= ">{$temp}</option>";
	}
	$ReturnStr .= "</select>";

	return $ReturnStr;
}

function multiarray_search($arrayVet, $campo, $valor)
{
    while(isset($arrayVet[key($arrayVet)])){
        if($arrayVet[key($arrayVet)][$campo] == $valor){
            return key($arrayVet);
        }
        next($arrayVet);
    }
    return -1;
}

## updated on 23 Feb 2008 by Ivan
## old function is not accurate - my_round(71.5945, 2) gets 71.6 instead of 71.59
/*
function my_round($value, $precision=0)
{
    return number_format(round(round($value*pow(10, $precision+1), 0), -1)/pow(10, $precision+1), $precision);
}
*/
## The rounding scheme of negative number is Round-Half-Up Symmetric Implementation
## ref: http://www.diycalculator.com/sp-round.shtml
## e.g. my_round(-6.4, 0) = -6		my_round(-6.5, 0) = -7		my_round(-6.6, 0) = -7
function my_round($Value, $Decimal=0)
{
    // [2018-0703-1107-42207] Cast to float if not numeric
    if (!is_numeric($Value)){
        $Value = (float)$Value;
    }

	### Handle very small number like "-1.9388995891205E-05"
	$tmpStrPos = strpos($Value, 'E-');
	$isTinyNumber = ($tmpStrPos == '')? 0 : 1;
	$tmpDP = substr($Value, $tmpStrPos + 2, 2);

	$ValueArr = explode(".", $Value);
	$integerValue = $ValueArr[0];
	$fractionalValue = $ValueArr[1];

	if ($fractionalValue != NULL || $fractionalValue != 0)
	{
		$first_n_Digit = substr($fractionalValue, 0, $Decimal);
		$n_plus_one_Digit = substr($fractionalValue, $Decimal, 1);
		if ($n_plus_one_Digit > 4)
		{
			$first_n_Digit += 1;

			# e.g. my_round(1.069, 2) => 06+1 = 7 => 1.07
			$first_n_Digit = str_pad($first_n_Digit, $Decimal, 0, STR_PAD_LEFT);

			# e.g. my_round(2.998, 2) => 99+1 = 100 => 3.00
			if (strlen($first_n_Digit) > $Decimal)
			{
				if ($Value < 0)
				{
					$integerValue = abs($integerValue);
					$isNegative = true;
				}
				else
				{
					$isNegative = false;
				}

				$integerValue += 1;

				if ($isNegative) {
					$integerValue = "-".$integerValue;
				}

				$first_n_Digit = substr($first_n_Digit, 1, $Decimal);
			}

			$roundedValue = $integerValue.".".$first_n_Digit;
		}
		else
		{
			$roundedValue = $Value;
		}
	}
	else
	{
		$roundedValue = $Value;
	}

	if ($isTinyNumber){
		$roundedValue = $roundedValue / pow(10, $tmpDP);
	}

	# [DM#3136] Cast to integer
	$formatedValue = number_format($roundedValue, (int)$Decimal);
	$returnValue = str_replace(",","",$formatedValue);

	return $returnValue;
}

if (!$eclass_middle_inclusion)
{
    @intranet_cache();
}

# calculate standard deviation of the array (biased estimate)
function standard_deviation($std)
{
    $total;
	if(sizeof($std)>1)
	{
		while(list($key,$val) = each($std))
		{
			$total += $val;
		}

		reset($std);
		$mean = $total/count($std);

		while(list($key,$val) = each($std))
		{
			$sum += pow(($val-$mean),2);
		}
		$var = sqrt($sum/count($std));
	}
	else
		$var = 0;

    return $var;
}

function generateFileUploadNameHandlerNew($my_form, $my_file_prefix)
{
	$field_prefix = "document.{$my_form}.{$my_file_prefix}";
	$rx = "
		<script language=\"JavaScript\">
		function Big5FileUploadHandler() {
			var i = 0;
			while (typeof(eval(\"$field_prefix\"+i))!='undefined')
			{
				if (eval(\"$field_prefix\"+i+\".value\") != '')
				{
					var Ary = eval(\"$field_prefix\"+i+\".value.split('\\\\\\\\')\");
					eval(\"$field_prefix\"+i+\"_hidden.value = Ary[Ary.length-1]\");
				}
				i ++;
			}

			return true;
		}
		</script>
		";

	return $rx;
}

function iportfolio_auth($ParAuthStr){
	global $ck_has_iportfolio, $EC_BL_ACCESS_HIGH;

	$AuthTempArr = preg_split('//', $ParAuthStr, -1, PREG_SPLIT_NO_EMPTY);

	if(is_array($AuthTempArr))
	{
		foreach($AuthTempArr as $Auth)
		{
			switch($Auth)
			{
				case "T":
					$AuthArr[] = "1";
					break;
				case "S":
					$AuthArr[] = "2";
					break;
				case "P":
					$AuthArr[] = "3";
					break;
			}
		}
	}

	if(is_array($AuthArr) && in_array($_SESSION['UserType'], $AuthArr) && $ck_has_iportfolio)
	{
		update_login_session();
		if (isset($EC_BL_ACCESS_HIGH) && $EC_BL_ACCESS_HIGH){
			header("Location: /home/portfolio");
			exit();
		}
//		DenyAccess();
	}
	else{
		header("Location: /");
		exit();
	}
}

function iportfolio_getSBSTitle($course_id, $single_lang=true)
{
	global $eclass_db, $intranet_session_language;

	$sql =	"
						SELECT
							Remark
						FROM
							".$eclass_db.".course
						WHERE
							course_id = '".$course_id."'
					";
	intranet_opendb();
	$result = mysql_query($sql);
	$row = mysql_fetch_array($result);
//	intranet_closedb();

	$arr_tmp = explode("=|=", $row[0]);
	for ($i=0; $i<sizeof($arr_tmp); $i++)
	{
		list($item_index, $item_title_e, $item_title_c, $item_enabled) = explode("|", $arr_tmp[$i]);
		if (trim($item_index)=="growth")
		{
			if($single_lang)
				$ra = $intranet_session_language == "b5" ? $item_title_c : $item_title_e;
			else
				$ra = array(
								"en" => $item_title_e,
								"b5" => $item_title_c
								);
		}
	}

	return $ra;
}

function undo_htmlspecialchars($string, $level=0){

	if ($level==0)
	{
		$patterns = array (
					"/&lt;/",
					"/&gt;/",
					"/&quot;/",
					"/&amp;/"
					);
		$replacements = array (
						"<",
						">",
						"\"",
						"&"
						);
	} elseif ($level==1)
	{
		# convert $amp;lt; back to <   ...etc
		$patterns = array (
				"/&amp;/",
				"/&quot;/",
				"/&lt;/",
				"/&gt;/"
				);
		$replacements = array (
					"&",
					"\"",
					"<",
					">"
					);
	} elseif ($level==2)
	{
		# convert $amp;lt; back to <   ...etc
		$patterns = array (
				"/&amp;/",
				"/&quot;/",
				"/&lt;/",
				"/&gt;/"
				);
		$replacements = array (
					"&",
					'\"',
					"<",
					">"
					);
	}

	return preg_replace($patterns, $replacements, $string);
}

function in_multi_array($needle, $haystack)
	{
		$in_multi_array = false;

		if(is_array($haystack))
		{
			if(in_array($needle, $haystack))
			{
				$in_multi_array = true;
			}
			else
			{
			   foreach ($haystack as $key => $val)
			   {
				   if(is_array($val))
				   {
						if(in_multi_array($needle, $val))
						{
							$in_multi_array = true;
							break;
						}
				   }
			   }
		   }
		}

		return $in_multi_array;
	}

	function getSystemMessage($msg, $err=0){
	global $con_msg_add, $con_msg_update, $con_msg_del;

	if ($msg!="")
	{
		switch ($msg)
		{
			case 1:	$message = $con_msg_add;	 break;
			case 2:	$message = $con_msg_update;	 break;
			case 3:	$message = $con_msg_del;	 break;
		}


		//$myClass = ($msg==3) ? "notice2" : "notice1";
		$rx = "<table><tr><td class='bodycolor2'>&nbsp;&nbsp;<b><font color=red>\n";
		$rx .= $message . "</font></b>\n &nbsp;&nbsp;</td></tr></table>\n";
	}

	return $rx;
}

function returnSelection($arr2D, $curSelected, $objName, $objProperty){
	$rx = "<select class='inputfield' name=\"$objName\" $objProperty>\n";

	$selectedAlready = false;
	for ($i=0; $i<sizeof($arr2D); $i++)
	{
		list($value, $name) = $arr2D[$i];
		if ($value=="OPTGROUP" || ($value==null && !($value==0) && !($value=="")))
		{
			if ($isOptionGroupBefore)
			{
				$rx .= "</optgroup>\n";
			}

			$rx .= "<optgroup label=\"".$name."\">\n";
			$isOptionGroupBefore = true;
		} else
		{
			$optionSelect = "";
			if ($curSelected==$value && !$selectedAlready)
			{
				$optionSelect =  "selected='selected'";
				$selectedAlready = true;
			}

			$rx .= "<option value=\"$value\" $optionSelect>$name</option>\n";
		}
	}
	if ($isOptionGroupBefore)
	{
		$rx .= "</optgroup>\n";
	}
	$rx .= "</select>\n";

	return $rx;
}

	# Modified by Key (2008-08-13)
	# Copy from eclass30 lib.php used by ipf2.5 data export function
	# handle double quote and special characters for export
	function handleCSVExportContent($ParContent)
	{
		return str_replace("\"", "\"\"", undo_htmlspecialchars($ParContent));
	}


	// generate an icon button which calls the preset text for select
	function GetPresetText($jArrName, $btnID, $targetName, $jOtherFunction="",$divIDPrefix = '',$targetDivWidth = '', $append='', $presetRowHeight='')
	{

		global $image_path, $button_select, $button_hide, $flag_is_loaded_before, $is_calendar_used_before, $ec_words;
		$divIDPrefix = ($divIDPrefix == '')? 'listContent': $divIDPrefix;
		//$ReturnStr .=  "<a href=\"javascript:void(0)\" onClick=\"jSelectPresetText(eval('".$jArrName."'+document.form1.".$jArrName."ID.value), '$targetName', 'listContent', {$btnID});\">";

		$ReturnStr .=  "<a href=\"javascript:void(0)\" onClick=\"jSelectPresetTextECLASS(".$jArrName.", '$targetName', '{$divIDPrefix}_{$btnID}', {$btnID}, '{$jOtherFunction}', '{$targetDivWidth}','{$append}','{$presetRowHeight}');\">";
		$ReturnStr .=  "<img src=\"$image_path/icon/preset_option.gif\" id=\"posimg{$btnID}\" border=\"0\" alt=\"$button_select\"></a>&nbsp;";
		$ReturnStr .=  "<script language='javascript'>js_btn_hide=\"".$button_hide."\";\n js_preset_no_record=\"".$ec_words['preset_no_record']."\";\n</script>\n";

		// Output several div for each button to prevent a possible popup display problem on first load in IE7.
//		if (!$flag_is_loaded_before)
//		{
			$ReturnStr .=  "<div id=\"{$divIDPrefix}_{$btnID}\"></div>";
			// iframe to cover pull-down menus
			if (!$is_calendar_used_before)
			{
				$ReturnStr .= "<iframe id='lyrShim' src='javascript:false;' scrolling='no' frameborder='0' style='position:absolute; top:0px; left:0px; display:none;'></iframe>\n";
			}
//			$flag_is_loaded_before = true;
//		}
		$is_calendar_used_before = true;


		return $ReturnStr;
	}

	function n_sizeof($arrData)
	{

		$count = 0;
		if (is_array($arrData))
		{
			while (list($key, $value) = each($arrData))
			{
				if (is_int($key))
				{
					$count++;
				}
			}
		} else
		{
			$count = sizeof($arrData);
		}
		return $count;
	}

	// convert PHP array to JS array (2D)
	function ConvertToJSArray($arrData, $jArrName, $fromDB = 0){

		$ReturnStr = "<SCRIPT LANGUAGE=\"JavaScript\">\n";
		$ReturnStr .= "var ".$jArrName." = new Array(".sizeof($arrData).");\n";

		for ($i = 0; $i < sizeof($arrData); $i++)
		{
			$ReturnStr .= $jArrName."[".$i."] = new Array(".n_sizeof($arrData[$i]).");\n";

			for ($j = 0; $j < n_sizeof($arrData[$i]); $j++)
			{
				if (sizeof($arrData[$i]) > 1)
				{
					if ($fromDB)
					{
						$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("'", "&#96;", $arrData[$i][$j])."\";\n";
					} else
					{
						$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("'", "&#96;", htmlspecialchars($arrData[$i][$j]))."\";\n";
					}
				} else
				{
					if ($fromDB)
					{
						$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("'", "&#96;", $arrData[$i])."\";\n";
					} else
					{
						$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("'", "&#96;", htmlspecialchars($arrData[$i]))."\";\n";
					}
				}
			}
		}

		$ReturnStr .= "</SCRIPT>\n";

		return $ReturnStr;
	}

	# remove empty field in array 20081016 yatwoon
	function array_remove_empty($arr)
	{
	    $narr = array();
	    while(list($key, $val) = each($arr)){
	        if (is_array($val)){
	            $val = array_remove_empty($val);
	            if (count($val)!=0){
	                $narr[$key] = $val;
	            }
	        }
	        else {
	            if (trim($val) != ""){
	                $narr[$key] = $val;
	            }
	        }
	    }
	    unset($arr);
	    return $narr;
	}

	function Get_Lang_Selection($ChineseName, $EnglishName) {
		if ($_SESSION['intranet_session_language'] == 'en') {
			if (is_array($EnglishName)) {
				$result = $EnglishName;
				foreach ($EnglishName as $kk => $vv) {
					$result[$kk] = (trim($vv) == "")? $ChineseName[$kk]:$vv;
				}
				return $result;
			} else {
				return (trim($EnglishName) == "")? $ChineseName:$EnglishName;
			}
		}
		else {
			if (is_array($ChineseName)) {
				$result = $ChineseName;
				foreach ($ChineseName as $kk => $vv) {
					$result[$kk] = (trim($vv) == "")? $EnglishName[$kk]:$vv;
				}
				return $result;
			} else {
				return (trim($ChineseName) == "")? $EnglishName:$ChineseName;
			}
		}
	}

	function Get_Table_Display_Content($Content)
	{
		$Content = trim($Content);
		$display = ($Content == "")? "&nbsp;" : nl2br($Content);

		return $display;
	}

	function Get_Selection_First_Title($Title)
	{
		return '- '.$Title.' -';
	}

	# trim all elements in the array
	function Array_Trim($arr)
	{
		$narr = array();
		while(list($key, $val) = each($arr)){
			if (is_array($val)){
				$val = Array_Trim($val);
				$narr[$key] = $val;
			}
			else
			{
				$narr[$key] = trim($val);
			}
		}
		unset($arr);
		return $narr;
	}

	function Convert_Date_To_DaysAgo($date)
	{
		$modifiedDate = strtotime($date);
		$now = time();
		$dateDiff = $now - $modifiedDate;
		$fullDays = floor($dateDiff/(60*60*24));

		return $fullDays;
	}

	function Get_Last_Modified_Remark($date, $byUserName='', $byUserID='', $DisplayText='')
	{
		global $Lang, $PATH_WRT_ROOT;

		/*
		# Days Ago Format
		$daysAgo = Convert_Date_To_DaysAgo($date);
		$thisDisplay = $Lang['General']['LastModifiedInfoRemark'];

		# Days Ago
		if ($daysAgo == 0)
			$thisDisplay = str_replace('<!--DaysAgo-->', $Lang['General']['Today'], $thisDisplay);
		else
			$thisDisplay = str_replace('<!--DaysAgo-->', $daysAgo.$Lang['General']['DaysAgo'], $thisDisplay);

		# Modified By
		$thisDisplay = str_replace('<!--LastModifiedBy-->', $byUserName, $thisDisplay);
		*/

		if ($byUserID != '')
		{
			include_once($PATH_WRT_ROOT."includes/libdb.php");
			$libdb = new libdb();

			$nameField = getNameFieldByLang();
			$sql = 'Select '.$nameField.' From INTRANET_USER Where UserID = \''.$byUserID.'\' ';
			$nameArr = $libdb->returnVector($sql);
			$byUserName = $nameArr[0];
		}

		# Date Format
		$thisDisplay = ($DisplayText=='')? $Lang['General']['LastModifiedInfoRemark'] : $DisplayText;
		$thisDisplay = str_replace('<!--DaysAgo-->', $date, $thisDisplay);
		$thisDisplay = str_replace('<!--LastModifiedBy-->', $byUserName, $thisDisplay);

		return $thisDisplay;
	}

	// Below is new functions for IP25 //
	function getSelectAcademicYear($objName, $tag='', $noFirst=0, $noPastYear=0, $targetYearID='', $displayAll=0, $pastAndCurrentYearOnly=0, $OrderBySequence=1, $excludeCurrentYear=0, $excludeYearIDArr=array())
	{
		global $Lang, $i_general_all;
		include_once("form_class_manage.php");
		$lfcm = new form_class_manage();
		$arrResult = $lfcm->Get_Academic_Year_List('', $OrderBySequence, $excludeYearIDArr, $noPastYear, $pastAndCurrentYearOnly, $excludeCurrentYear);

		$academic_year_selection .= "<SELECT name='$objName' id='$objName' $tag>";

		if($displayAll)
		{
			$academic_year_selection .= "<OPTION value='' >". Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['All']['AcademicYear'])."</OPTION>";
		}

		if($noFirst == ""){
			$academic_year_selection .= "<OPTION value='' >".Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['SelectSchoolYear'])."</OPTION>";
		}

		if(sizeof($arrResult)>0){
			for($i=0; $i<sizeof($arrResult); $i++){
				list($AcademicYearID, $YearNameEN, $YearNameB5, $Sequence, $CurrentSchoolYear, $StartDate, $EndDate) = $arrResult[$i];

				// Preset target Year if specified, else preset current academic year
				if ($targetYearID == '')
				{
					if(!$displayAll)
						$strSelected = ($CurrentSchoolYear==1)?" SELECTED ":" ";
				}
				else
				{
					$strSelected = ($AcademicYearID==$targetYearID)?" SELECTED ":" ";
				}

				$academic_year_selection .= "<OPTION value='$AcademicYearID' $strSelected>".Get_Lang_Selection($YearNameB5,$YearNameEN)."</OPTION>";

				$selected = $AcademicYearID;
			}
		}
		$academic_year_selection .= "</SELECT>";

		return $academic_year_selection;
	}

	function getSelectAcademicYear2($objName, $tag='', $all=0, $noFirst=0, $FirstTitle='', $noPastYear=0)
	{
		global $Lang;
		include_once("form_class_manage.php");
		$lfcm = new form_class_manage();
		$arrResult = $lfcm->Get_Academic_Year_List2();

		if ($noPastYear)
			$startFlag = false;

		$academic_year_selection .= "<SELECT name='$objName' id='$objName' $tag>";

		if($noFirst == 0){

			//$empty_selected = ($selected == '')? "SELECTED":"";
			if($FirstTitle=="")
			{
				if ($all==0)
				{
					$title = "-- ".$Lang['Btn']['Select']." --";
				}
				else if ($all == 2)
				{
					$title = $Lang['Btn']['NotSet'];
				}
				else
				{
					$title = $Lang['Btn']['All'];
				}
			}
			else
				$title = $FirstTitle;

			$academic_year_selection .= "<OPTION value='' >".$title."</OPTION>";
		}

		if(sizeof($arrResult)>0){
			for($i=0; $i<sizeof($arrResult); $i++){
				list($AcademicYearID, $YearNameEN, $YearNameB5) = $arrResult[$i];

				if ($noPastYear && $CurrentSchoolYear)
					$startFlag = true;

				if ($noPastYear && $startFlag == false)
					continue;

				$strSelected = ($CurrentSchoolYear==1)?" SELECTED ":" ";
				$academic_year_selection .= "<OPTION value='$AcademicYearID' $strSelected>".Get_Lang_Selection($YearNamB5,$YearNameEN)."</OPTION>";

				$selected = $AcademicYearID;
			}
		}
		$academic_year_selection .= "</SELECT>";

		return $academic_year_selection;
	}

	function getStartDateOfAcademicYear($academicYearID, $yearTermID=''){
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$lfcm = new form_class_manage();

		intranet_opendb();

		if($yearTermID != '') $conds = " AND YearTermID = '$yearTermID'";

		$sql = "SELECT MIN(TermStart) FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$academicYearID' $conds";
		$result = $lfcm->returnVector($sql);

		if(sizeof($result)>0){
			return $result[0];
		}else{
			return 0;
		}
	}

	function getEndDateOfAcademicYear($academicYearID, $yearTermID=''){
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$lfcm = new form_class_manage();

		intranet_opendb();

		if($yearTermID != '') $conds = " AND YearTermID = '$yearTermID'";

		$sql = "SELECT MAX(TermEnd) FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$academicYearID' $conds";
		$result = $lfcm->returnVector($sql);

		if(sizeof($result)>0){
			return $result[0];
		}else{
			return 0;
		}
	}

	function getPeriodOfAcademicYear($academicYearID, $yearTermID=''){
		include_once("libdb.php");
		$li = new libdb();

		intranet_opendb();

		if($yearTermID != '') $conds = " AND YearTermID = '$yearTermID'";

		$sql = "SELECT MIN(TermStart) StartDate, MAX(TermEnd) EndDate FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$academicYearID' $conds";
		$result = $li->returnArray($sql);

		return $result[0];
	}


	function getCurrentAcademicYearAndYearTerm()
	{
		global $intranet_root;

		if ($_SESSION['CurrentSchoolYearInfo'] == "") {
			include_once("form_class_manage.php");
			$lfcm = new form_class_manage();
			$arrResult = $lfcm->Get_Current_Academic_Year_And_Year_Term();

			$returnArr = array();
			$returnArr[0] = $arrResult[0]['AcademicYearID'];
			$returnArr['AcademicYearID'] = $arrResult[0]['AcademicYearID'];
			$returnArr[1] = $arrResult[0]['YearTermID'];
			$returnArr['YearTermID'] = $arrResult[0]['YearTermID'];

			$_SESSION['CurrentSchoolYearInfo'] = $returnArr;
		}

		return $_SESSION['CurrentSchoolYearInfo'];
	}

	function getAcademicYearAndYearTermByDate($Date)
	{
		include_once("form_class_manage.php");
		$lfcm = new form_class_manage();

		$sql = "SELECT
						ayt.YearTermID, ayt.YearTermNameEN, ayt.YearTermNameB5, ay.AcademicYearID, ay.YearNameEN, ay.YearNameB5
				FROM
						ACADEMIC_YEAR_TERM as ayt
						Inner Join ACADEMIC_YEAR as ay On (ayt.AcademicYearID = ay.AcademicYearID)
				WHERE
						'$Date' BETWEEN TermStart AND TermEnd
				";
		$result = $lfcm->returnArray($sql);

		if(sizeof($result)>0){
			list($TermID, $YearTermNameEN, $YearTermNameB5, $AcademicYearID, $YearNameEN, $YearNameB5) = $result[0];
			$TermName = Get_Lang_Selection($YearTermNameB5,$YearTermNameEN);
			$AcademicYearName = Get_Lang_Selection($YearNameB5,$YearNameEN);

			$term_arr[] = $TermID;
			$term_arr[] = $TermName;
			$term_arr[] = $AcademicYearID;
			$term_arr[] = $AcademicYearName;
			$term_arr['YearTermID'] = $TermID;
			$term_arr['YearTermName'] = $TermName;
			$term_arr['AcademicYearID'] = $AcademicYearID;
			$term_arr['AcademicYearName'] = $AcademicYearName;
		}

		return $term_arr;
	}

	function Get_Current_Academic_Year_ID() {
		if ($_SESSION['CurrentSchoolYearID'] == "") {
			include_once("form_class_manage.php");
			$lfcm = new form_class_manage();
			$AcademicYearID = $lfcm->getCurrentAcademicaYearID();

			$_SESSION['CurrentSchoolYearID'] = $AcademicYearID;
		}

		return $_SESSION['CurrentSchoolYearID'];
	}

	function Get_Previous_Academic_Year_ID() {
		include_once("libdb.php");

		$li = new libdb();
		$StartOfCurYear = getStartDateOfAcademicYear(Get_Current_Academic_Year_ID());

		$sql = "select
							AcademicYearID
						from
							ACADEMIC_YEAR_TERM
						where
							TermEnd < '".$StartOfCurYear."'
						order by
							TermEnd Desc
						limit 0,1";
		$PrevYearID = $li->returnVector($sql);

		return $PrevYearID[0];
	}

	function getAvailableTimetableTemplate($TargetSchoolYear,$TargetTermID='',$StartDate='')
	{
		include_once("libdb.php");

		$li = new libdb();
		if($StartDate != "") {
			$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE '$StartDate' BETWEEN TermStart AND TermEnd";
			$arrTermID = $li->returnVector($sql);
			$TargetTermID = $arrTermID[0];
		}

		$sql = "SELECT DISTINCT a.TimeTableID, a.TimetableName FROM INTRANET_SCHOOL_TIMETABLE AS a INNER JOIN INTRANET_TIMETABLE_ROOM_ALLOCATION AS b ON (a.TimeTableID = b.TimeTableID) WHERE a.AcademicYearID = '$TargetSchoolYear' AND a.YearTermID = '$TargetTermID'";
		$arrTimeTable = $li->returnArray($sql,2);
		return $arrTimeTable;
	}

	function getAvailableTimetableTemplateWithDaysChecking($TargetSchoolYear, $TargetTermID='', $StartDate='', $PeriodType, $NumOfCycleDay, $MinNumOfCycleDay='')
	{
		include_once("libdb.php");

		$li = new libdb();
		if($StartDate != "") {
			$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE '$StartDate' BETWEEN TermStart AND TermEnd";
			$arrTermID = $li->returnVector($sql);
			$TargetTermID = $arrTermID[0];
		}

		if($PeriodType == "cycle"){
			//$sql = "SELECT DISTINCT a.TimeTableID, a.TimetableName FROM INTRANET_SCHOOL_TIMETABLE AS a INNER JOIN INTRANET_TIMETABLE_ROOM_ALLOCATION AS b ON (a.TimeTableID = b.TimeTableID) WHERE a.AcademicYearID = '$TargetSchoolYear' AND a.YearTermID = '$TargetTermID' AND a.CycleDays = $NumOfCycleDay";

			$conds_cycleDays = '';
			if ($MinNumOfCycleDay == '') {
				//2013-0328-1236-41071
				//$conds_cycleDays = " AND a.CycleDays = '".$NumOfCycleDay."' ";
				$conds_cycleDays = " AND a.CycleDays >= '".$NumOfCycleDay."' ";
			}
			else {
				$conds_cycleDays = " AND a.CycleDays >= '".$MinNumOfCycleDay."' And a.CycleDays <= '".$NumOfCycleDay."' ";
			}

			$sql = "SELECT DISTINCT a.TimeTableID, a.TimetableName FROM INTRANET_SCHOOL_TIMETABLE AS a WHERE a.AcademicYearID = '$TargetSchoolYear' AND a.YearTermID = '$TargetTermID' $conds_cycleDays";
		}else{
			//$sql = "SELECT DISTINCT a.TimeTableID, a.TimetableName FROM INTRANET_SCHOOL_TIMETABLE AS a INNER JOIN INTRANET_TIMETABLE_ROOM_ALLOCATION AS b ON (a.TimeTableID = b.TimeTableID) WHERE a.AcademicYearID = '$TargetSchoolYear' AND a.YearTermID = '$TargetTermID' AND (a.CycleDays = 5 OR a.CycleDays = 6)";

			if ($NumOfCycleDay === '') {
				$conds_CycleDays = " And (a.CycleDays = 5 OR a.CycleDays = 6  OR a.CycleDays = 7)";
			}
			else {
				$conds_CycleDays = " And a.CycleDays >= '".$NumOfCycleDay."'";
			}
			$sql = "SELECT DISTINCT a.TimeTableID, a.TimetableName FROM INTRANET_SCHOOL_TIMETABLE AS a WHERE a.AcademicYearID = '$TargetSchoolYear' AND a.YearTermID = '$TargetTermID' $conds_CycleDays";
		}
		//echo $sql;
		$arrTimeTable = $li->returnArray($sql,2);

		return $arrTimeTable;
	}

	function convertKeyword($keyword="")
	{
		$keyword = trim($keyword);
		$keyword = str_replace("_", "\_", $keyword);
		$keyword = str_replace("%", "\%", $keyword);

		return $keyword;
	}

	/* use this function instead of using htmlspecialchars */
	function HTMLtoDB($xStr, $type=0) {
		$xStr = htmlspecialchars($xStr);
		if ($type==0)
			return str_replace("&amp;#", "&#", $xStr);		// normal text to DB
		else
			return str_replace("&amp;", "&", $xStr);		// filename to DB
	}

	function getParentNameWithStudentInfo($StudentTablePrefix="", $ParentTablePrefix="", $displayLang="", $includeParentWithoutStudent=0)
	{
		global $intranet_session_language, $Lang;

        $displayLang = $displayLang ? $displayLang : $intranet_session_language;
        $chi = ($displayLang =="b5" || $displayLang == "gb");
        if ($chi)
        {
            $firstChoice = "ChineseName";
            $altChoice = "EnglishName";
        }
        else
        {
            $firstChoice = "EnglishName";
            $altChoice = "ChineseName";
        }
        $StudentNameField = "IF($StudentTablePrefix$firstChoice IS NULL OR TRIM($StudentTablePrefix$firstChoice) = '',$StudentTablePrefix$altChoice,$StudentTablePrefix$firstChoice)";
        $ParentNameField = "IF($ParentTablePrefix$firstChoice IS NULL OR TRIM($ParentTablePrefix$firstChoice) = '',$ParentTablePrefix$altChoice,$ParentTablePrefix$firstChoice)";

		$username_field = '';
		if ($includeParentWithoutStudent)
       		$username_field .= " IF(".$StudentTablePrefix."UserID Is Null,
									$ParentNameField,";

			$username_field .= "IF(	".$StudentTablePrefix."ClassName = '' OR ".$StudentTablePrefix."ClassName IS NULL,
	       							CONCAT('(','','-','',') ',$StudentNameField,'".$Lang['iMail']['FieldTitle']['TargetParent']."',' (',".$ParentNameField.",')') ,
	       							CONCAT('(',".$StudentTablePrefix."ClassName,'-',".$StudentTablePrefix."ClassNumber,') ',$StudentNameField,'".$Lang['iMail']['FieldTitle']['TargetParent']."',' (',".$ParentNameField.",')')
								)";
		if ($includeParentWithoutStudent)
			$username_field .= ")";

        return $username_field;
	}

	function getParentNameWithStudentInfo2($StudentTablePrefix="", $ParentTablePrefix="", $displayLang="", $includeParentWithoutStudent=0, $ClassNumberPaddingWord='',$ClassNumberPaddingLen=2)
	{
		global $intranet_session_language, $Lang;

        $displayLang = $displayLang ? $displayLang : $intranet_session_language;
        $chi = ($displayLang =="b5" || $displayLang == "gb");
        if ($chi)
        {
            $firstChoice = "ChineseName";
            $altChoice = "EnglishName";
        }
        else
        {
            $firstChoice = "EnglishName";
            $altChoice = "ChineseName";
        }
        $StudentNameField = "IF($StudentTablePrefix$firstChoice IS NULL OR TRIM($StudentTablePrefix$firstChoice) = '',$StudentTablePrefix$altChoice,$StudentTablePrefix$firstChoice)";
        $ParentNameField = "IF($ParentTablePrefix$firstChoice IS NULL OR TRIM($ParentTablePrefix$firstChoice) = '',$ParentTablePrefix$altChoice,$ParentTablePrefix$firstChoice)";

		$username_field = '';
		if ($includeParentWithoutStudent)
       		$username_field .= " IF(".$StudentTablePrefix."UserID Is Null,
									$ParentNameField,";

		if($ClassNumberPaddingWord!=''){
			$classnumber_field = "LPAD(".$StudentTablePrefix."ClassNumber,$ClassNumberPaddingLen,'".$ClassNumberPaddingWord."')";
		}else{
			$classnumber_field = $StudentTablePrefix."ClassNumber";
		}

			$username_field .= "IF(	".$StudentTablePrefix."ClassName = '' OR ".$StudentTablePrefix."ClassName IS NULL,
	       							CONCAT($StudentNameField,'".$Lang['iMail']['FieldTitle']['TargetParent']."',' (',".$ParentNameField.",')') ,
	       							CONCAT('(',".$StudentTablePrefix."ClassName,'-',".$classnumber_field.",') ',$StudentNameField,'".$Lang['iMail']['FieldTitle']['TargetParent']."',' (',".$ParentNameField.",')')
								)";
		if ($includeParentWithoutStudent)
			$username_field .= ")";

        return $username_field;
	}

	function getAcademicYearInfoAndTermInfoByDate($Date="")
	{
		include_once("libdb.php");
		$li = new libdb();

		$Date = $Date ? $Date : date("Y-m-d");

		$sql = "SELECT a.AcademicYearID, a.YearNameEN, a.YearNameB5, b.YearTermID, b.YearTermNameEN, b.YearTermNameB5 FROM ACADEMIC_YEAR AS a INNER JOIN ACADEMIC_YEAR_TERM AS b ON (a.AcademicYearID = b.AcademicYearID) WHERE '$Date' BETWEEN b.TermStart and b.TermEnd";
		$result = $li->returnArray($sql,6);

		if(sizeof($result)>0){
			list($YearID, $YearNameEn, $YearNameB5, $TermID, $YearTermNameEN, $YearTermNameB5) = $result[0];
			$YearName = $YearNameEn;
			$TermName = $YearTermNameEN;

			$year_info_arr[] = $YearID;
			$year_info_arr[] = $YearName;
			$year_info_arr[] = $TermID;
			$year_info_arr[] = $TermName;
		}

		return $year_info_arr;
	}

	function GetAllAcademicYearInfo()
	{
		include_once("libdb.php");
		$li = new libdb();

		$sql = "SELECT distinct(a.AcademicYearID), a.YearNameEN, a.YearNameB5 FROM ACADEMIC_YEAR AS a INNER JOIN ACADEMIC_YEAR_TERM AS b ON (a.AcademicYearID = b.AcademicYearID) order by a.Sequence";
		$result = $li->returnArray($sql);

		return $result;
	}

	function GetCurrentTimeZoneInfo($ParDate='')
	{
		if ($ParDate == '') {
			//$ParDate = 'now()';
			$ParDate = 'CURDATE()';
		}
		else {
			$ParDate = "'".$ParDate."'";
		}

		include_once("libdb.php");
		$li = new libdb();
		$sql = "Select
						PeriodID, PeriodStart, PeriodEnd, PeriodType, CycleType, PeriodDays, FirstDay, SaturdayCounted
				From
						INTRANET_CYCLE_GENERATION_PERIOD
				Where
						$ParDate Between PeriodStart And PeriodEnd
				";
		$result = $li->returnArray($sql);
		return $result;
	}

	function DownloadAttachment($ModuleName,$real_url, $custom_name='')
	{
		include_once("libfilesystem.php");
		include_once("libfiletable.php");
		global $file_path;

			$target_filename = stripslashes($real_url);
		$real_filename = stripslashes(substr($real_url,strpos($real_url,"/")+1,strlen($real_url)));

		$path = "$file_path/file/".$ModuleName;
		$a = new libfiletable("", $path, 0, 0, "");
		$files = $a->files;
		$valid = true;

		if ($valid)
		{
		    $target_filepath = $path."/".$target_filename;
		    $content = get_file_content($target_filepath);

		    //$encoded_name = urlencode($target_filename);
			//$real_filename = utf8Encode2($real_filename); # convert octets to represented chars in UTF-8, see above utf8Encode2()
			//output2browser($content, $real_filename);
			if(trim($custom_name)!='')
				$target_filename = $custom_name;


			output2browser($content, $target_filename);

		}
		else
		{
		    echo "You don't have privileges or the file has been corrupted.";
		}
	}

	function isUTF8($str) {
        if ($str === mb_convert_encoding(mb_convert_encoding($str, "UTF-32", "UTF-8"), "UTF-8", "UTF-32")) {
            return true;
        } else {
            return false;
        }
    }

    function wordwrap_utf8($string, $length=75, $break="<br>", $cut=false) {
		preg_match_all("/./u", $string, $matches);
		$s = $matches[0];
		$ct = count( $s );
		for($i=0; $i<ceil($ct/$length) ; $i++) {
			$ns .= implode("", array_slice($s, $i*$length, $length) ) . $break;
		}
		return $ns;
	}

	function get_wrap_content($string, $length, $break, $cut){
		//echo mb_strlen($string,"utf8")."<BR>";
		$tmp = explode(' ', $string);
		$str = '';
		if(count($tmp) > 0){
			for($i=0 ; $i<count($tmp) ; $i++){
				$str .= ($i == 0) ? '' : $break;
				$str .= (strlen($tmp[$i]) > $length) ? wordwrap_utf8($tmp[$i], $length, $break, $cut) : $tmp[$i];
			}
		}
		return $str;
	}

	// wrap text to the nearest of $length chars
	function wordwrapByLength($string, $length)
	{
	    $tmp = explode(' ', $string);

	    $strAry = array();
	    $str = '';
	    if(count($tmp) > 1){
	        for($i=0,$iMax=count($tmp); $i<$iMax; $i++){
	            $nextCheck = $str .' '. $tmp[$i];

	            if (strlen($nextCheck) > $length) {
	                if (strlen($str) > $length) {
	                    $wrapStr = wordwrap_utf8($str, $length, "^^~^^", false);
	                    $wrapAry = explode("^^~^^",$wrapStr);
	                    $strAry = array_merge($strAry, $wrapAry);
	                }
	                else {
	                    $strAry[] = trim($str);
	                    $str = $tmp[$i] . ' ';
	                }
	            }
	            else {
	                $str .= $tmp[$i] . ' ';
	            }

	            if ($i == $iMax - 1) {
	                $strAry[] = trim($str);
	            }
	        }

	        $retStr = implode("<br>", $strAry);
	    }
	    else {
	        $retStr = wordwrap_utf8($string, $length, "<br>", false);
	    }
	    return $retStr;
	}

	# Trim empty element in Array
	function trim_array($a)
	{
		$b = array();
		$j = 0;
		for ($i = 0; $i < count($a); $i++) {
			if ($a[$i] != "") {
				$b[$j++] = $a[$i];
			}
		}
		return $b;
	}

	function Get_Array_By_Key($SourceArr, $Key)
	{
		$numOfElement = count($SourceArr);

		$returnArr = array();
		for ($i=0; $i<$numOfElement; $i++)
			$returnArr[] = $SourceArr[$i][$Key];

		return $returnArr;
	}

	function checkDateIsValid($date){
		//match the format of the date
		if (preg_match ("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $date, $parts))
		{
			//check weather the date is valid of not
			if(checkdate($parts[2],$parts[3],$parts[1]))
				return true;
			else
				return false;
		}
		else
		{
			return false;
		}
	}

	function checkDateIsValidFor2Format($date){
		//match the format of the date
		if (preg_match ("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $date, $parts))
		{
			//check weather the date is valid of not
			if(checkdate($parts[2],$parts[3],$parts[1]))
				return true;
			else
				return false;
		}
		else if (preg_match ("/^([0-9]{4})\/([0-9]{2})\/([0-9]{2})$/", $date, $parts)) // YYYY/MM/DD
		{
			//check weather the date is valid of not
			if(checkdate($parts[2],$parts[3],$parts[1]))
				return true;
			else
				return false;
		}else if (preg_match ("/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/", $date, $parts)) // DD/MM/YYYY
		{
			//check weather the date is valid of not
			if(checkdate($parts[2],$parts[1],$parts[3]))
				return true;
			else
				return false;
		}
		else
		{
			return false;
		}
	}

	function checkTimeIsValid($time){
		//match the format of the date
		if (preg_match ("/^([0-9]{2}):([0-9]{2}):([0-9]{2})$/", $time, $parts))
		{
			if(($parts[1] <= 24) && ($parts[2] <= 59) && ($parts[3] <= 59))
				return true;
			else
				return false;
		}
		else
		{
			return false;
		}
	}

	/* $BuildNumericArray= 1,
	 * $ReturnAry[$Key1][$Key2][] = $val1;
	 * $ReturnAry[$Key1][$Key2][] = $val2;
	 * $ReturnAry[$Key1][$Key2][] = $val3;
	 */
 	function BuildMultiKeyAssoc($DBReturnAry, $AssocKey, $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0)
	{
		$returnAry = array();

		if(empty($IncludedDBField))
		{
			$returnField = '$Rec';
		}
		else if(count((array)$IncludedDBField)==1 && $SingleValue == 1 )
		{
			$thisFieldName = is_array($IncludedDBField)?$IncludedDBField[0]:$IncludedDBField;
			$returnField = '$Rec["'.$thisFieldName.'"]';
		}
		else
		{
			foreach((array)$IncludedDBField as $thisFieldName)
			{
				$returnFieldAry[] = '"'.$thisFieldName.'" => $Rec["'.$thisFieldName.'"]';
			}
			$returnField = 'array('.implode(",",(array)$returnFieldAry).')';
		}

		$AssocKey = (array)$AssocKey;
		if($BuildNumericArray == 1)
		{
			$cmd = '$returnAry';
			for($i=0; $i<count($AssocKey); $i++)
			{
				$cmd .= '[$Rec["'.$AssocKey[$i].'"]]';
			}
			$cmd .= '[] = '.$returnField.' ;';
		}
		else
		{
			$cmd = '$returnAry';
			for($i=0; $i<count($AssocKey); $i++)
			{
				$cmd .= '[$Rec["'.$AssocKey[$i].'"]]';
			}
			$cmd .= ' = '.$returnField.' ;';
		}


//		if(is_array($AssocKey))
//		{
//			$cmd = '$returnAry';
//			for($i=0; $i<count($AssocKey); $i++)
//			{
//				$cmd .= '[$Rec["'.$AssocKey[$i].'"]]';
//			}
//			$cmd .= ' = '.$returnField.' ;';
//		}
//		else
//			$cmd = '$returnAry[$Rec["'.$AssocKey.'"]] = '.$returnField.' ;';

		foreach((array)$DBReturnAry as $Rec)
		{
			eval($cmd);
		}

		return $returnAry;
	}

	function Get_Array_Last_Key($arr){
		end($arr);
		return key($arr);
	}

	function Get_Sequence_Number($no)
	{
		$tmp = $no%100;
		if(in_array($tmp,array(11,12,13)))
			$x = "th";
		else
		{
			switch ($tmp%10)
			{
				case 1 : $x = "st"; break;
				case 2 : $x = "nd"; break;
				case 3 : $x = "rd"; break;
				default: $x = "th"; break;
			}
		}
		return trim($no).$x;
	}

	function sortByColumn3(&$ParArr,$ParCmpField,$ParCmpField2='',$ParCmpField3='')
	{// not able to reverse

		global $CmpField, $CmpField2, $CmpField3;

		$CmpField = $ParCmpField;
		$CmpField2 = $ParCmpField2;
		$CmpField3 = $ParCmpField3;

		$CmpFunction = "USortCmp3";

		usort($ParArr,$CmpFunction);

		unset($CmpField);

	}

	function USortCmp3($a,$b)
	{
	global $CmpField, $CmpField2, $CmpField3;

	if(!is_numeric($a[$CmpField])||!is_numeric($b[$CmpField])) {
		if ($CmpField2 != '' && $a[$CmpField] == $b[$CmpField]) {
			if ($a[$CmpField2] == $b[$CmpField2]) {
				if($a[$CmpField3] == $b[$CmpField3]){
					return 0;
				}else{
					return strcasecmp($a[$CmpField3],$b[$CmpField3]);
				}
			}else{
				return strcasecmp($a[$CmpField2],$b[$CmpField2]);
			}
		}
		else {
			return strcasecmp($a[$CmpField],$b[$CmpField]);
		}
	}

	if ($a[$CmpField] == $b[$CmpField]) {
		if ($CmpField2 != '') {
			if ($a[$CmpField2] == $b[$CmpField2]) {
				if($a[$CmpField3] == $b[$CmpField3]){
					$result = 0;
				}else{
					return strcasecmp($a[$CmpField3],$b[$CmpField3]);
				}
			}
			else {
				$result = strcasecmp($a[$CmpField2], $b[$CmpField2]);
			}
		}
		else {
			$result =  0;
		}
    }
    else
    {
    	$result = strcasecmp($a[$CmpField], $b[$CmpField]);
    }
    return $result;
	}


	//sort by column functions start, not only for continuous integer index, can also sort array with index
	function sortByColumn2(&$ParArr,$ParCmpField,$reverse=0,$sortWithIndex=0,$ParCmpField2='')
	{
		global $CmpField, $CmpField2;

		$CmpField = $ParCmpField;
		$CmpField2 = $ParCmpField2;

		$CmpFunction = $reverse?"URSortCmp":"USortCmp";

		if($sortWithIndex){
			uasort($ParArr,$CmpFunction);
		}
		else{
			usort($ParArr,$CmpFunction);
		}
		unset($CmpField);
	}

	function USortCmp($a,$b)
	{
		global $CmpField, $CmpField2;

		if(!is_numeric($a[$CmpField])||!is_numeric($b[$CmpField])) {
			if ($CmpField2 != '' && $a[$CmpField] == $b[$CmpField]) {
				return strcasecmp($a[$CmpField2],$b[$CmpField2]);
			}
			else {
				return strcasecmp($a[$CmpField],$b[$CmpField]);
			}
		}


		if ($a[$CmpField] == $b[$CmpField]) {
			if ($CmpField2 != '') {
				if ($a[$CmpField2] == $b[$CmpField2]) {
					$result =  0;
				}
				else {
					$result = ($a[$CmpField2] < $b[$CmpField2]) ? -1 : 1;
				}
			}
			else {
				$result =  0;
			}
	    }
	    else
	    {
	    	$result = ($a[$CmpField] < $b[$CmpField]) ? -1 : 1;
	    }
	    return $result;
	}

	function URSortCmp($a,$b)
	{
		global $CmpField, $CmpField2;

		if(!is_numeric($a[$CmpField])||!is_numeric($b[$CmpField])) {
			if ($CmpField2 != '' && $a[$CmpField] == $b[$CmpField]) {
				return strcasecmp($b[$CmpField2],$a[$CmpField2]);
			}
			else {
				return strcasecmp($b[$CmpField],$a[$CmpField]);
			}
		}

		if ($a[$CmpField] == $b[$CmpField]) {
	        if ($CmpField2 != '') {
				if ($a[$CmpField2] == $b[$CmpField2]) {
					$result =  0;
				}
				else {
					$result = ($a[$CmpField2] > $b[$CmpField2]) ? -1 : 1;
				}
			}
			else {
				$result =  0;
			}
	    }
	    else
	    {
	    	$result = ($a[$CmpField] > $b[$CmpField]) ? -1 : 1;
	    }
	    return $result;
	}
	//sort by column functions end

	function DebugCtr($prefix='',$reset=0)
	{
		global $DebugMode, $QueryCtr;

		$DebugMode = 1;

		$prefix = $prefix?$prefix.":":$prefix;
		debug($prefix.($GLOBALS[debug][db_query_count]-$QueryCtr));

		if($reset==1)
			$QueryCtr = $GLOBALS[debug][db_query_count];
	}

	/**
	 * Get Academic Year Name by Academic Year Id
	 * @param String||Int $ParAyId - The academic year id
	 * @param String $ParLang - The langage required, e.g. "EN", "B5"
	 * @return String The academic year name
	 */
	function getAYNameByAyId($ParAyId="", $ParLang="") {
		global $intranet_db,$intranet_session_language;
		$libdb = new libdb();
		if (empty($ParAyId)) {
			$ParAyId = Get_Current_Academic_Year_ID();
		}
		if (empty($ParLang)) {
			$ParLang = $intranet_session_language;
		}
		$sql = "SELECT AY.YEARNAME" . strtoupper($ParLang) ." FROM $intranet_db.ACADEMIC_YEAR AY
				WHERE AY.ACADEMICYEARID=$ParAyId";

		$returnVector = $libdb->returnVector($sql);
		return $returnVector[0];
	}

	/*
	function updateGetCookies($arrCookies)
	{
		if(!empty($arrCookies) && count($arrCookies) > 0)
		{
			for($i=0 ; $i<count($arrCookies) ; $i++)
			{
				global ${$arrCookies[$i][0]}, ${$arrCookies[$i][1]};

				//if(isset(${$arrCookies[$i][1]}) && ${$arrCookies[$i][1]} != '')
				if(isset(${$arrCookies[$i][1]}))
				{
					# e.g. setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
					setcookie($arrCookies[$i][0], ${$arrCookies[$i][1]}, 0, "", "", 0);

					# e.g. $ck_page_size = $numPerPage;
					${$arrCookies[$i][0]} = ${$arrCookies[$i][1]};
				}
				else
				{
					# e.g. $numPerPage = $ck_page_size;
					${$arrCookies[$i][1]} = ${$arrCookies[$i][0]};
				}
			}
		}
		return;
	}

	function clearCookies($arrCookies)
	{
		# clear cookies process...
		if(!empty($arrCookies) && count($arrCookies) > 0)
		{
			for($i=0 ; $i<count($arrCookies) ; $i++)
			{
				global ${$arrCookies[$i][0]};

				if(isset(${$arrCookies[$i][0]}))
				{
					# e.g. setcookie("ck_page_size", null, time()-999999, "", "", 0);
					setcookie($arrCookies[$i][0], null, time()-999999, "", "", 0);

					# e.g. $ck_page_size = $numPerPage;
					//${$arrCookies[$i][0]} = null;
				}
			}
		}
		return;
	}
	*/

	// valid code must be start with letter
	function valid_code($Code)
	{
		//return eregi('^[a-z]', $Code);
		return preg_match('/^[a-z]/i', $Code);
	}

	function check_powerspeech_is_expired($ingore_user_type=false){
		global $plugin, $US_Intranet_IDType;
		if(isset($plugin['power_speech']) && $plugin['power_speech']) {
			if(isset($plugin['power_speech_expire'])){
				$timestamp = date("Y-m-d");
				$today = strtotime($timestamp);
				$expire_date =  $plugin['power_speech_expire'];
				$expire_date = strtotime($expire_date);
				return ($today > $expire_date) ? 1 : 0;
			}else{
				return 0;
			}
		}else{
			return 1;
		}
	}

	function Get_Gamma_Identity_Access_Rights()
	{
		 global $intranet_root, $_SESSION;
          if ($_SESSION['campusmail_set_content']=="")
          {
          	$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");
          	$_SESSION['campusmail_set_content'] = (trim($file_content)=="")?"NULL_CONTENT":$file_content;
          } else
          {
          	$file_content = ($_SESSION['campusmail_set_content']=="NULL_CONTENT") ? "":$_SESSION['campusmail_set_content'];
          }

		if ($file_content == ""){
		    $gamma_access_right = array(1,1,1,1);
		}
		else
		{
		    unset($campusmail_set_line);
		    $campusmail_set_line = array();

		    $content = explode("\n", $file_content);
		    $campusmail_set_line[] = explode(":",$content[0]);
		    $campusmail_set_line[] = explode(":",$content[1]);
		    $campusmail_set_line[] = explode(":",$content[3]);
		    $campusmail_set_line[] = explode(":",$content[2]);
		    $gamma_access_right = array($campusmail_set_line[0][0],$campusmail_set_line[1][0],$campusmail_set_line[2][0],$campusmail_set_line[3][0]);
		}
		return $gamma_access_right;
	}

	//THIS FUNCTION COPY FROM /home/web/eclass40/eclass40/src/includes/php/lib.php,
	//Please update this funciton in 40 if it need any fix
	function DetectDataEncoding($string)
	{
		static $list = array("Big5", "Big5-HKSCS", "UTF-8", "GB2312", "GBK", "ISO-8859-1");
		$first_detect = 'UTF-8';
		foreach ($list as $item) {
			$sample = iconv($item, $item, $string);
			if (md5($sample) == md5($string)){
				$first_detect = $item;
				break;
			}
		}
		$encode = mb_detect_encoding($string,"UTF-8,EUC-CN,BIG-5,EUC-TW");
		//echo $string.'<br>';
		//echo $encode.'<br>';
		//echo $first_detect.'<br>';exit();
		if ($encode == 'EUC-CN')
			$first_detect = "GBK";
		return $first_detect;
	}

	function showWarningMsg($title, $content, $others="")
	{
		$showmsg =  "<fieldset class='instruction_box_v30'>";
		$showmsg .=  "<legend class='instruction_title'>";
		$showmsg .=  $title;
		$showmsg .=  "</legend>";
		$showmsg .=  "<span $others>";
		$showmsg .=  $content;
		$showmsg .=  "</span>";
		$showmsg .=  "</fieldset>";

		return $showmsg;
	}

	function parseFileName($filename) {
		//return preg_replace('(\n|\/|\<|\>|\*|\:|\"|\?|\|)', "", str_replace("\\", "", trim($filename)));
		//return preg_replace('(\n|\/|\<|\>|\*|\:|\"|\?|\#|\%|)', "", trim($filename));

		return preg_replace('(\n|\/|\<|\>|\*|\:|\"|\?|\#|\%|\\\|\||)', "", trim($filename));
	}

	function getMostUpdateEclassModuleVersion($Module="")
	{
		$libdb = new libdb();

		//$Module = strtoupper($Module);

		$sql = "SELECT ROUND(MAX(VersionNo) ,2) FROM ECLASS_MODULE_VERSION_LOG WHERE Module='$Module'";
		$result = $libdb->returnVector($sql);

		return $result[0];
	}

	function getEclassModuleVersionInUse($Module="")
	{
		$libdb = new libdb();
		//$Module = strtoupper($Module);
		$sql = "SELECT ROUND(MAX(Version ),2) FROM ECLASS_UPDATE_LOG WHERE Module='$Module'";
		$result = $libdb->returnVector($sql);

		return $result[0];
	}

	function updateEclassModuleVersion($Module="", $Version="")
	{
		if($Module!="" && $Version!="") {
			$libdb = new libdb();

			$sql = "SELECT COUNT(*) FROM ECLASS_UPDATE_LOG WHERE Module='$Module' AND ROUND(Version,2)=ROUND($Version,2)";
			$count = $libdb->returnVector($sql);

			if($count[0] == 0) {
				$sql = "INSERT INTO ECLASS_UPDATE_LOG(Module, Version, DateInput) VALUES ('$Module', $Version, NOW())";
				return $libdb->db_db_query($sql);
			}
		}
	}

	function displayBubbleMessage($version="", $userID="")
	{
		global $UserID;
		//include_once("version.php");
		$libdb = new libdb();

		if($userID=="") $userID = $UserID;
		//if($version =="") $version = $versions[0][0];
		if($version =="") $version = Get_IP_Version();

		$versionID = getBubbleVersionID($version);

		# version pattern ip.2.5.x1.x2.x3.x4, only compare ip.2.5.x1.x2.x3 (x4 is for critical deploy version)
		$sql = "SELECT v.Version FROM ECLASS_BUBBLE_READ r INNER JOIN ECLASS_BUBBLE_VERSION v ON (v.VersionID=r.VersionID) WHERE (v.Version IS NOT NULL AND v.Version != '') AND r.UserID='$UserID' ORDER BY v.VersionID DESC LIMIT 1";
		$verAry = $libdb->returnVector($sql);

		$ver = $verAry[0];
		$t = substr($ver, 0, -2);
		$ver = substr($t,-1)=="." ? substr($t,0,-1) : $t;

		$version = substr($version, 0, -2);
		$version = substr($version,-1)=="." ? substr($version,0,-1) : $version;

		return ($version==$ver) ? 0 : 1;
	}

	function getBubbleVersionID($version="")
	{
		$libdb = new libdb();

		$sql = "SELECT VersionID FROM ECLASS_BUBBLE_VERSION WHERE Version='$version'";
		$result = $libdb->returnVector($sql);

		if(sizeof($result)==0) {
			$sql = "INSERT INTO ECLASS_BUBBLE_VERSION (Version, DateInput) VALUES ('$version', NOW())";
			$libdb->db_db_query($sql);
			return $libdb->db_insert_id();
		} else {
			return $result[0];
		}
	}

	function asorti($arr) {
		$arr2 = $arr;
		foreach($arr2 as $key => $val) {
			$arr2[$key] = strtolower($val);
   		}

		asort($arr2);

		foreach($arr2 as $key => $val) {
			$arr2[$key] = $arr[$key];
		}

		return $arr2;
	}

	function file_size($size) {

		$filesizename = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
		return $size ? round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $filesizename[$i] : '0 Bytes';

	}

# determine eClass url root (optional config $web_protocol can be set in settings.php)
if($eclass_version >= 4){
	$eclass_url_root = ((isset($web_protocol) && $web_protocol!="") ? $web_protocol : "http") . "://".$eclass40_httppath;
	$eclass30_url_root = ((isset($web_protocol) && $web_protocol!="") ? $web_protocol : "http") . "://".$eclass_httppath;
}
else{

	$eclass_url_root = ((isset($web_protocol) && $web_protocol!="") ? $web_protocol : "http") . "://".$eclass_httppath;
}

// start of include language
### Lang from IP20

if ($_SESSION["platform"]=="KIS")
{
	$urlSrc = (getenv("SCRIPT_NAME")!="") ? getenv("SCRIPT_NAME") : $_SERVER["SCRIPT_NAME"];
	if (strstr($urlSrc, "/kis/"))
	{
		$NoLangWordings = true;
	}
}

if (!$NoLangWordings && !$NoLangOld20)
{
	/*
	if ($Test)
	{
		include_once($intranet_root."/lang/lang.b5.ip20_test.php");
	} else
	*/
	include_once($intranet_root."/lang/lang.$intranet_session_language.ip20.php");
}

### Lang in IP25
if (!$NoLangWordings)
	include_once($intranet_root."/lang/lang.$intranet_session_language.php");

if ($_SESSION["platform"]=="KIS")
{
	$Lang['Header']['HomeTitle'] = "eClass KIS 1.0";
	$favicon_image = "kis/kis_favicon.png";
}



function gen_online_help_btn_and_layer($module,$module_function,$current_lang="")
{
	global $OnlineHType,$OnlineH,$Lang,$intranet_root,$intranet_session_language;

	if ($_SESSION["platform"]=="KIS")
	{
		return "";
	}

	$current_lang = ($current_lang == "")? $intranet_session_language:$current_lang;

	$help_config_file = $intranet_root."/includes/ip_help_config.php";
	if(file_exists($help_config_file))
	{
		include_once($help_config_file);
	}

	# convert system lang to config file lang
	switch($current_lang)
	{
		case "chib5":
		case "b5":
			$online_help_lang = 'big5';
		break;
		case "gb":
		case "chigb":
			$online_help_lang = 'gb';
		break;
		case "en":
			$online_help_lang = 'eng';
		break;
		default:
			$online_help_lang = $current_lang;
	}

	$menu_arr = $OnlineH[$module][$module_function];

	if(is_array($menu_arr))
	{
		foreach($menu_arr as $type=>$link_arr)
		{
			$link = $link_arr[$online_help_lang];
			$list.='<li><a href="'.$link.'" target="_blank">'.$OnlineHType[$type][$online_help_lang].'</a></li>';
		}

		$html='<div class="online_help">
          <a href="javascript:void(0)" title="'.$Lang['General']['Help'].'" onClick="switchOnlineHelp(\''.$module.'_'.$module_function.'\',event)" class="help_icon" id="online_'.$module.'_'.$module_function.'_help_btn">'.$Lang['General']['Help'].'</a>
          <div class="help_board" id="online_'.$module.'_'.$module_function.'_help_board" style="position:absolute;z-index:99" onClick="switchOnlineHelp(\''.$module.'_'.$module_function.'\')">
	          <h1>'.$Lang['General']['Help'].'</h1>
	          <ul>'.$list.'</ul>
          </div>
         </div><p class="spacer"></p>';
	}

     return $html;
}

function generate_texttospeech_ui_IP($msg_id, $player_type=1, $buttonFull=1, $buttonHighLight=1, $buttonSetting=1){
	 global $tts_server_url, $plugin;

	if ($tts_server_url != ""){

		$isExpired = check_powerspeech_is_expired();

		if ($isExpired){
			return '';
		}else{

			return '<div style="min-height:30px;padding:0 0 0 0" >
						<div class="TTS_PlayButton"
						ttsTarget="'.$msg_id.'"
						ttsPlayer="divPlayer_'.$msg_id.'"
						ttsPlayerType="'.$player_type.'"
						ttsButtonFull="'.$buttonFull.'"
						ttsButtonHighLight="'.$buttonHighLight.'"
						ttsButtonSetting="'.$buttonSetting.'"
						ttsExpired="'.$isExpired.'"
						id="PS_Container_'.$msg_id.'" ></div>
						<div style="height:5px;">&nbsp;</div>
						<div id="divPlayer_'.$msg_id.'" style=""></div>
					</div>';
			}
	}
}

/**
 * generate the output xml
 * @param	array	ParElementsArr	the child elements for the xml output, string [0] - child tag name, string [1] - child content
 * @return	xml	the xml to output
 */
function generate_XML($ParElementsArr, $ParCData=false, $ParSpcChar=false) {
	$xml .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	$numofElements = count($ParElementsArr);
	for ($i=0;$i<$numofElements;$i++) {
		list($TAG_NAME, $CONTENT) = $ParElementsArr[$i];
		$x .= simpleXMLembed($TAG_NAME, $CONTENT, $ParCData, $ParSpcChar);
	}
	$xml .= simpleXMLembed("elements", $x);
	return $xml;
}


/**
 * simpleXMLembed - simple xml embed with format <xx> yy </xx>
 * @param	string	ParTag	the tag name of an xml element
 * @param	string	ParValue	corresponding content for an element
 * @param	boolean	ParCData	quote the content with <![CDATA[ content ]]>	// already html format string
 * @param	boolean	ParSpcChar	escape special characters (single quote and double quotes)	// for strings to put into html
 * @return	string	xml tag
 */
function simpleXMLembed($ParTag="element", $ParValue="content", $ParCData=false, $ParSpcChar=false) {
	return "<".$ParTag.">".($ParCData?"<![CDATA[":"").($ParSpcChar?intranet_htmlspecialchars($ParValue):$ParValue).($ParCData?"]]>":"")."</".$ParTag.">";
}

function getAcademicYearByAcademicYearID($year='',$ParLang='en')
{
		global $intranet_root;

		include_once("libdb.php");
		$li = new libdb();

		if($year == "")
			$year = Get_Current_Academic_Year_ID();

		//$year = Get_Current_Academic_Year_ID();
		$sql = "SELECT * FROM ACADEMIC_YEAR WHERE AcademicYearID=$year";
		$academicInfo = $li->returnArray($sql);

		//return Get_Lang_Selection($academicInfo[0]['YearNameB5'], $academicInfo[0]['YearNameEN']);
		unset($li);
		//return $academicInfo[0]['YearNameEN'];

		if ($ParLang=='en')
			return $academicInfo[0]['YearNameEN'];
		else if ($ParLang=='b5')
			return $academicInfo[0]['YearNameB5'];
		else if ($ParLang=='')
			return Get_Lang_Selection($academicInfo[0]['YearNameB5'], $academicInfo[0]['YearNameEN']);

         /*
         ### commented by henry (20090727)
         ### original coding in IP20 (START) ###

         $academic_yr = get_file_content("$intranet_root/file/academic_yr.txt");
         if ($academic_yr == "") $academic_yr = date("Y");
         return $academic_yr;

         ### original coding in IP20 (END) ###
         */
}

function Get_IP_Version() {
	$JustWantVersionData = true;
	include("version.php");
	$eClassVersion = $versions[0][0];
	return $versions[0][0];

}

function getUrlParaByAssoAry($paraAssoAry) {
		$paraAry = array();
	foreach ((array)$paraAssoAry as $_paraName => $_paraValue) {
		$paraAry[] = $_paraName.'='.$_paraValue;
	}
	return implode('&', $paraAry);
}

function returnModuleAvailableTag($Module='', $assocAry=0)
{
	include_once("libdb.php");
	$li = new libdb();

	if($Module!="")
		$cond = " WHERE r.Module='$Module'";

	$sql = "SELECT t.TagID, t.TagName FROM TAG t INNER JOIN TAG_RELATIONSHIP r ON (r.TagID=t.TagID) $cond ORDER BY t.TagName";
	$result = $li->returnArray($sql);

	if($assocAry==1) {
		$newAry = array();
		for($i=0; $i<sizeof($result); $i++) {
			list($id, $name) = $result[$i];
			$newAry[$id] = $name;
		}
		return $newAry;
	} else {
		return $result;
	}
}

function returnModuleTagNameByTagID($tagID='', $Module='')
{
	if($tagID!='') {
		include_once("libdb.php");
		$li = new libdb();
		if($Module!='') $cond = " AND r.Module='$Module'";

		$sql = "SELECT t.TagName FROM TAG t LEFT OUTER JOIN TAG_RELATIONSHIP r ON (r.TagID=t.TagID) WHERE t.TagID IN ($tagID) $cond ORDER BY t.TagName ";
		return $li->returnVector($sql);
	}
}

function returnTagIDToModuleTagNameByTagID($tagID='', $Module='')
{
	$tagIdToTagNameAry = array();
	if($tagID!='') {
		include_once("libdb.php");
		$li = new libdb();
		if($Module!='') $cond = " AND r.Module='$Module'";

		$sql = "SELECT t.TagID,t.TagName FROM TAG t LEFT OUTER JOIN TAG_RELATIONSHIP r ON (r.TagID=t.TagID) WHERE t.TagID IN ($tagID) $cond ORDER BY t.TagName ";
		$records = $li->returnResultSet($sql);
		$records_size = count($records);
		for($i=0;$i<$records_size;$i++){
			if(!isset($tagIdToTagNameAry[$records[$i]['TagID']])){
				$tagIdToTagNameAry[$records[$i]['TagID']] = array();
			}
			$tagIdToTagNameAry[$records[$i]['TagID']][] = $records[$i]['TagName'];
		}
	}

	return $tagIdToTagNameAry;
}

function returnModuleTagIDByTagName($tagName='', $Module='')
{
	if($tagName != '')
	{
		include_once("libdb.php");
		$li = new libdb();
		$isMagicQuotesEnabled = $li->isMagicQuotesOn();

		if($Module!='') $cond = " AND r.Module='$Module'";

		$tagName = trim($tagName);
		if(substr($tagName,-1)==",") $tagName = substr($tagName,0,-1);
		$tagAry = explode(',', $tagName);

		$tempAry = array();
		foreach($tagAry as $tag)
		{
		    $target_tag = trim($tag);
		    if(!$isMagicQuotesEnabled){
		        $target_tag = $li->Get_Safe_Sql_Query($target_tag);
		    }

		    $sql = "SELECT t.TagID FROM TAG t LEFT OUTER JOIN TAG_RELATIONSHIP r ON (r.TagID=t.TagID) WHERE t.TagName='".$target_tag."' $cond";
		    $result = $li->returnVector($sql);
		    if($result[0] != "") {	# can retrieve TagID from existing data
		        $tempAry[] = $result[0];
		    } else {				# cannot retrieve TagID, then create a new tag
		        $tagid = insertModuleTag(trim($tag), $Module);
		        $tempAry[] = $tagid;
		    }
		}
		return $tempAry;
	}
}

function checkTagRelation($tagID, $Module)
{
	include_once("libdb.php");
	$li = new libdb();

	$sql = "SELECT COUNT(*) FROM TAG_RELATIONSHIP WHERE TagID='$tagID' AND Module='$Module'";
	$result = $li->returnVector($sql);

	return $result[0];
}

function insertModuleTag($tagName='', $Module='')
{
	global $UserID;

	if($tagName != '')
	{
		include_once("libdb.php");
		$li = new libdb();
		$isMagicQuotesEnabled = $li->isMagicQuotesOn();

		$tagName = trim($tagName);
		if(!$isMagicQuotesEnabled){
		    $tagName = $li->Get_Safe_Sql_Query($tagName);
		}

		# Check any existing TAG (but without linkage to Module)
		$sql = "SELECT TagID FROM TAG WHERE TagName='$tagName'";
		$temp = $li->returnVector($sql);
		$tagid = $temp[0];

		if($tagid=="") {
			# insert TAG
			$sql = "INSERT INTO TAG (TagName, DateInput, InputBy) VALUES ('$tagName', NOW(), '$UserID')";
			$li->db_db_query($sql);
			$tagid = $li->db_insert_id();
		}
		# create relationship
		if($Module!='') {
			//$sql = "INSERT INTO TAG_RELATIONSHIP (TagID, Module, DateInput, InputBy, DateModified, ModifyBy) VALUES ('$tagid', '$Module', NOW(), '$UserID', NOW(), '$UserID')";
			//$li->db_db_query($sql);
			createTagModuleRelation($tagid, $Module);
		}

		return $tagid;
	}
}

function createTagModuleRelation($tagid='', $Module='')
{
	global $UserID;

	include_once("libdb.php");
	$li = new libdb();

	if($tagid!='' && $Module!='') {
		$sql = "INSERT INTO TAG_RELATIONSHIP (TagID, Module, DateInput, InputBy, DateModified, ModifyBy) VALUES ('$tagid', '$Module', NOW(), '$UserID', NOW(), '$UserID')";
		$li->db_db_query($sql);
	}
}

function removeTagModuleRelation($tagid='', $Module='')
{
	include_once("libdb.php");
	$li = new libdb();

	if($tagid!='' && $Module!='') {
		$sql = "DELETE FROM TAG_RELATIONSHIP WHERE TagID='$tagid' AND Module='$Module'";

		$li->db_db_query($sql);
	}

}

function remove_item_by_value($array, $val='', $preserve_keys=true)
{
	$key = array_search($val, $array);
	unset($array[$key]);
	$array = array_values($array);
	return ($preserve_keys == true) ? $array : array_values($array);
}

function build_duplicate_file_name($filename,$s, $format='$fn($s).$ext')
{
	$token = explode('.',$filename);
	list($fn, $ext) = $token;
	eval('$filename = "'.$format.'";');

	return 	$filename;
}

/*
 * Get the UserID Array after adding users from the "common_choose" pop up
 * para: $UserTypeArr - array(1), array(1,2), array(1,2,3), etc...
 */
function Get_User_Array_From_Common_Choose($SelectionValueArr, $UserTypeArr='')
{
	global $PATH_WRT_ROOT, $intranet_root, $sys_custom;

	if($sys_custom['DHL']){
		include_once($intranet_root."/includes/DHL/libdhl.php");

		$libdhl = new libdhl();
		$ary = $libdhl->convertTargetSelectionToUserAry($SelectionValueArr);

		$userIdAry = Get_Array_By_Key($ary,0);
		return $userIdAry;
	}

	include_once($PATH_WRT_ROOT.'includes/libdb.php');
	include_once($PATH_WRT_ROOT.'includes/libgrouping.php');
	$libgrouping = new libgrouping();

	$UserIDArr = array();
	foreach((array)$SelectionValueArr as $key => $value)
	{
		$thisType = strtoupper(substr($value, 0, 1));	// U: User, G: Group, C: Class
		$thisType_ID = substr($value, 1);
		switch($thisType)
		{
			case "U":
				$UserIDArr[] = $thisType_ID;
				break;
			case "G":
			case "C":
				//$objGroup = new libgroup($thisType_ID);
				//$GroupMemberID = $objGroup->Get_Group_User($UserTypeList);
				$GroupMemberInfoArr = $libgrouping->returnGroupUsersInIdentity(array($thisType_ID), $UserTypeArr);
				foreach($GroupMemberInfoArr as $k1 => $d1)
					$UserIDArr[] = $d1['UserID'];
				break;
			default:
				$UserIDArr[] = $value;
				break;
		}
	}

	$UserIDArr = array_values(array_unique($UserIDArr));
	return $UserIDArr;
}

function No_Access_Right_Pop_Up($msg="", $redirect_link="")
{
	global $PATH_WRT_ROOT;

	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT($msg, $redirect_link);
	intranet_closedb();
	exit;
}

function Get_Standardized_Table_Data_Display($ParData)
{
	global $Lang;

	$ParData = trim($ParData);
	$Data = ($ParData=='')? $Lang['General']['EmptySymbol'] : $ParData;

	return $Data;
}

function Flush_Screen_Display($SleepTime='')
{
	@ob_flush();
	@flush();

	if ($SleepTime != '') {
		usleep($SleepTime);
	}
}

function Get_Export_SQL_From_DB_Table_SQL($ParSql)
{
	$LimitIndex = strripos($ParSql, "LIMIT");
	$ExportSQL = substr($ParSql, 0, $LimitIndex);

	return $ExportSQL;
}

function Get_Common_Search_User_List($UserLogin="",$Keyword="",$UserType=array(1,2,3,4),$ExcludeUserList=array()) {
	$ExcludeUserList = is_array($ExcludeUserList)? $ExcludeUserList:array();

	include_once("libdb.php");
	$db = new libdb();

	$NameField = getNameFieldByLang('u.');
	$sql = "SELECT
						UserID,
						RecordType,
						".$NameField." as Name,
						ClassName,
						ClassNumber
					FROM
						INTRANET_USER as u
					WHERE
						u.RecordType in ('".implode("','",$UserType)."')
						";
	if (trim($UserLogin) != "") {
		$sql .= "AND
						u.UserLogin like '%".$db->Get_Safe_Sql_Like_Query($UserLogin)."%' ";
	}
	if (trim($Keyword) != "") { // format [ClassName][ClassNumber][StudenName]
		list($ClassName,$ClassNumber,$StudentName) = explode(" ",$Keyword);

		if ($ClassName != "") {
			$sql .= "AND
							u.ClassName like '%".$db->Get_Safe_Sql_Like_Query($ClassName)."%' ";
		}
		if ($ClassName != "") {
			$sql .= "AND
							u.ClassNumber like '%".$db->Get_Safe_Sql_Like_Query($ClassNumber)."%' ";
		}
		if ($StudentName != "") {
			$sql .= "
							AND
							(
							u.EnglishName like '%".$db->Get_Safe_Sql_Like_Query($StudentName)."%'
							OR
							u.ChineseName like '%".$db->Get_Safe_Sql_Like_Query($StudentName)."%'
							)";
		}
	}
	if (sizeof($ExcludeUserList) > 0) {
		$sql .= "
						AND
						UserID not in ('".implode("','",$ExcludeUserList)."') ";
	}
	//debug_r($sql);
	$result = $db->returnArray($sql);
	unset($db);
	return $result;
}


/*
 * DETERMINE THE REGION OF CLIENT
 * default zh_HK if this file does not exist or $region_of_client is not set
 * zh_HK (Traditional Chinese, Hong Kong)
 * zh_MO (Traditional Chinese, Macau)
 * zh_TW (Traditional Chinese, Taiwan)
 * zh_CN (Simplified Chinese, China)
 * zh_MY (Simplified Chinese, Malaysia)
*/
function get_client_region()
{
	global $region_of_client, $file_path;

	if ($region_of_client == '' && file_exists($file_path."/includes/settings_region.php"))
	{
		//include_once($PATH_WRT_ROOT."includes/settings_region.php");
		include_once("settings_region.php");
	}
	$region_of_client = ($region_of_client=="") ? "zh_HK" : $region_of_client;
	return $region_of_client;
}

if(get_client_region()=="zh_HK"){
	$plugin['WebSAMS'] = true;
	if($plugin['attendancestudent'])	$special_feature['websams_attendance_export'] = true;
	date_default_timezone_set ('Asia/Hong_Kong');
}
else if(get_client_region() == "zh_MY"){
	date_default_timezone_set ('Asia/Kuala_Lumpur');
}
// [2020-0407-1445-44292] enable flag for Taiwan clients
else if(get_client_region() == 'zh_TW') {
    $sys_custom['eNotice']['ClassTeacherSelectOwnClassStudentOnly'] = true;
    $sys_custom['schoolnews']['ClassTeacherSelectOwnClassStudentOnly'] = true;
}

############################
# check for eclass_update
############################
if(!isset($special_feature['eclass_update']))
{
	if(get_client_region()=="zh_HK")
	{
		$special_feature['eclass_update'] = true;
	}
	else
	{
		$special_feature['eclass_update'] = false;
	}
}

############################
# check for class_backup
############################
//if(!isset($special_feature['eclass_backup']))
//{
//	if(get_client_region()=="zh_HK")
//	{
//		$special_feature['eclass_backup'] = true;
//	}
//	else
//	{
//		$special_feature['eclass_backup'] = false;
//	}
//}


function get_website()
{
    global $intranet_root, $sys_custom;

	include_once("libfilesystem.php");
	$lf = new libfilesystem();

	if ($sys_custom['project']['HKPF']) {
	    $website = "Departmental Portal (DP) -> Management -> PTTS";
	}
	else {
	    $website = $lf->file_read($intranet_root."/file/email_website.txt");
	}
	$website = ($website=="") ? "http://".$_SERVER["HTTP_HOST"] : $website;

	return $website;
}

function get_webmaster()
{
    global $intranet_root, $sys_custom;

	include_once("libfilesystem.php");
	$lf = new libfilesystem();

	if ($sys_custom['project']['HKPF']) {
	    $webmaster = "ICNMC";
	}
	else {
    	$webmaster = $lf->file_read($intranet_root."/file/email_webmaster.txt");
	}

	return $webmaster;
}

function chopString($text, $count="", $append_txt="...")
{
    if(!$count)		return $text;

    $charset = returnCharset();
    mb_internal_encoding($charset);
	return mb_strlen($text) > $count ? mb_substr($text,0,$count) . $append_txt : $text;
}

function countString($text)
{
    $charset = returnCharset();
    mb_internal_encoding($charset);
	return mb_strlen($text);
}

function Get_Plural_Display($num, $name, $type='s')
{
	$NameCh = $num." ".$name;
	switch(true)
	{
		case $num==1:
			$NameEn = $num." ".$name;
		break;
		default:
			$NameEn = $num." ".$name.$type;
	}
	$NameEn .= " ";
	return Get_Lang_Selection($NameCh, $NameEn);
}

function returnRemarkLayer($table_content, $close_btn_action,$width="100%", $height="150px")
{
	global $image_path, $LAYOUT_SKIN;
	global $i_InventorySystem_NewItem_PurchasePrice_Help;

	$imgPath = $image_path."/".$LAYOUT_SKIN;
	$tempLayer .= "<table width='". $width."' border='0' cellpadding='0' cellspacing='0' class='layer_table'>";
	$tempLayer .= "<tr><td height='19'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	$tempLayer .= "<tr><td width='5' height='19'><img src='".$imgPath."/can_board_01.gif' width='5' height='19'></td>";
	$tempLayer .= "<td height='19' valign='middle' background='".$imgPath."/can_board_02.gif'></td>";
	$tempLayer .= "<td width='19' height='19'><a href='javascript:;' onClick=\"". $close_btn_action ."\"><img src='".$imgPath."/can_board_close_off.gif' name='pre_close".$id."' width='19' height='19' border='0' id='pre_close".$id."' onMouseOver=\"MM_swapImage('pre_close".$id."','','".$imgPath."/can_board_close_on.gif',1)\" onMouseOut='MM_swapImgRestore()'></a></td>";
	$tempLayer .= "</tr></table></td></tr>";
	$tempLayer .= "<tr><td><table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>";
	$tempLayer .= "<td width='5' background='".$imgPath."/can_board_04.gif'><img src='".$imgPath."/can_board_04.gif' width='5' height='19'></td>";
	$tempLayer .= "<td bgcolor='#FFFFF7'><div style='height:". $height ."; overflow:auto;'>";

	# need revise later for cater table header
// 	$tempLayer .= "<table width='98%' border='0' cellpadding='0' cellspacing='0' bgcolor='#CCCCCC' class='layer_table_detail'><tr><td>";

	$tempLayer .= $table_content;

// 	$tempLayer .= "</td></tr>";
// 	$tempLayer .= "<tr class='row_approved'><td>text text</td></tr>";
// 	$tempLayer .= "</table>";
	$tempLayer .= "</div><br></td>";
	$tempLayer .= "<td width='6' background='".$imgPath."/can_board_06.gif'><img src='".$imgPath."/can_board_06.gif' width='6' height='6'></td>";
	$tempLayer .= "</tr><tr>";
	$tempLayer .= "<td width='5' height='6'><img src='".$imgPath."/can_board_07.gif' width='5' height='6'></td>";
	$tempLayer .= "<td height='6' background='".$imgPath."/can_board_08.gif'><img src='".$imgPath."/can_board_08.gif' width='5' height='6'></td>";
	$tempLayer .= "<td width='6' height='6'><img src='".$imgPath."/can_board_09.gif' width='6' height='6'></td>";
	$tempLayer .= "</tr></table></td></tr></table>";

	return $tempLayer;

}

function memory_get_usage2()
{
	return convert_size(memory_get_usage());
}

function convert_size($size)
{
	$unit=array('b','kb','mb','gb','tb','pb');
	return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}

function intranet_time_diff($t1, $t2, $returnUnit='day')
{
	$diff = strtotime($t1) - strtotime($t2);
	switch($returnUnit)
	{
		case "year":
			$diff = $diff/12;
		case "month":
			$diff = $diff/30;
		case "day":
			$diff = $diff/24;
		case "hour":
			$diff = $diff/60;
		case "min":
			$diff = $diff/60;
		case "sec":
			$diff = $diff;
		break;
	}

	return $diff;
}

function is_date_empty($date)
{
	return ($date=="" || $date=="0000-00-00" || $date=="0000-00-00 00:00:00" || $date=="00:00:00")? true : false;
}

function getYearByDateTime($dataTime) {
	return substr($dataTime, 0, 4);
}

function getDateByDateTime($dataTime) {
	return substr($dataTime, 0, 10);
}

function getHourByDateTime($dataTime) {
	return substr($dataTime, 11, 2);
}

function getMinuteByDateTime($dataTime) {
	return substr($dataTime, 14, 2);
}

function getSecondByDateTime($dataTime) {
	return substr($dataTime, 17, 2);
}


function StartMemory()
{
	global $debug_GlobalMemory;
	$debug_GlobalMemory = memory_get_usage();
}

function StopMemory()
{
	global $debug_GlobalMemory;
	return convert_size(memory_get_usage() - $debug_GlobalMemory);
}


function array_values_recursive($array) {
   $flat = array();

   foreach ((array)$array as $value) {
           if (is_array($value)) $flat = array_merge($flat, array_values_recursive($value));
           else $flat[] = $value;
   }
   return $flat;
}

// Date format: YYYY-mm-dd
function Get_Date_Within_A_Date_Range($StartDate, $EndDate) {
	$StartDateTS = strtotime($StartDate);
	$EndDateTS = strtotime($EndDate);

	$thisDateTS = $StartDateTS;
	$DateArr = array();
	while($thisDateTS <= $EndDateTS) {
		$DateArr[] = date('Y-m-d', $thisDateTS);
		$thisDateTS = strtotime("+1 day", $thisDateTS);
	}

	return $DateArr;
}

function GetCommonAttachmentFolderPath($AttachmentArr, $FolderPath)
{
	if(empty($FolderPath) || trim(str_replace("/","",$FolderPath))=='') return false;

	global $file_path, $PATH_WRT_ROOT;
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/libfiletable.php");

	$AttachmentStr = (sizeof($AttachmentArr)==0) ? "" : implode(",", $AttachmentArr);
	$AttachmentStr = stripslashes($AttachmentStr);

	$path = $file_path.$FolderPath;
	$lu = new libfilesystem();

	if(!empty($FolderPath) && dirname($path) != dirname($file_path) && is_dir($path))// avoid remove_recursive from root path
	{
		$lu->folder_remove_recursive($path);
	}

	if (is_dir($path."tmp") && !is_dir($path))
	{
		$command ="mv $path"."tmp $path";
	    exec($command);
	}

	$lo = new libfiletable("", $path, 0, 0, "");
	$files = $lo->files;

	while (list($key, $value) = each($files)) {
	     if(!strstr($AttachmentStr,$files[$key][0])){
	          $lu->file_remove($path."/".$files[$key][0]);
	     }
	}

	$IsAttachment = (isset($AttachmentArr)) ? 1 : 0;

	if (!$IsAttachment)
	{
	    if ($FolderPath != ""){
	    	 $lu->lfs_remove($path);
	    }
	    return '';
	}
	else
		return $FolderPath;

}

function Get_FileList_By_FolderPath($FolderPath)
{
	if(empty($FolderPath)) return false;
	global $file_path, $PATH_WRT_ROOT;
	include_once($PATH_WRT_ROOT."includes/libfiletable.php");

	$path = $file_path.$FolderPath;

	$lo = new libfiletable("", $path, 0, 0, "");
	$files = $lo->files;

	return $files;
}

function Get_Attachment_Download_Link($FolderPath, $FileName)
{
	global $file_path;

	$path = $file_path.$FolderPath;
	$target_e = getEncryptedText($path.'/'.$FileName);
	//return 'http://'.$_SERVER['HTTP_HOST'].'/home/download_attachment.php?target='.urlencode($path.'/'.$FileName);
	return 'http://'.$_SERVER['HTTP_HOST'].'/home/download_attachment.php?target_e='.$target_e;
}


/*
 * $TimeRangeArr Sample
Array
(
    [2011-09-01] => Array
        (
            [0] => Array
                (
                    [StartTime] => 08:00:00
                    [EndTime] => 13:30:00
                )
            [1] => Array
                (
                    [StartTime] => 14:30:00
                    [EndTime] => 16:00:00
                )
        )
)
 */
function Time_Range_Array_Intersect($TimeRangeArr1, $TimeRangeArr2)
{
	if(sizeof($TimeRangeArr1)==0 || sizeof($TimeRangeArr2)==0)
		return array();

	$IntersectDate = array_intersect(array_keys((array)$TimeRangeArr1),array_keys((array)$TimeRangeArr2));

	$TimeIntersect = array();
	foreach($IntersectDate as $eachIntersectDate )
	{
		foreach($TimeRangeArr1[$eachIntersectDate] as $Time1)
		{
			foreach($TimeRangeArr2[$eachIntersectDate] as $Time2)
			{
				if(Time_Range_Intersect($Time1,$Time2))
					$TimeIntersect[$eachIntersectDate][] = Time_Range_Intersect($Time1,$Time2); //Time_Range_Intersect(array($Time1,$Time2));
			}
		}
	}
	return $TimeIntersect;

}

# e.g.
# $TimeRange1 = array("StartTime"=>"12:00:00", "EndTime"=>"15:00:00")
# $TimeRange2 = array("StartTime"=>"11:00:00", "EndTime"=>"13:00:00")
# $ReturnValue =  array("StartTime"=>"12:00:00", "EndTime"=>"13:00:00")
function Time_Range_Intersect($TimeRange1,$TimeRange2)
{
	$Start1 = strtotime($TimeRange1['StartTime']);
	$End1 = strtotime($TimeRange1['EndTime']);
	$Start2 = strtotime($TimeRange2['StartTime']);
	$End2 = strtotime($TimeRange2['EndTime']);

	if($Start1>=$Start2 && $Start1<$End2 || $End1>$Start2 && $End1<=$End2 || $Start1<=$Start2 && $End1>=$End2)
	{

		$IntersectStart = max($Start1,$Start2);
		$IntersectEnd = min($End1,$End2);

		$IntersectPeriod = array("StartTime"=>Date("H:i:s",$IntersectStart),"EndTime"=>Date("H:i:s",$IntersectEnd));
		return $IntersectPeriod;
	}
}


	/**
	* Generate break a URL from urlencode
	* @param : String $ParKey , eg en encode URL with base64_encode('id=12&action=add&msg=error')
	* @return : ASSOCIATIVE  ARRAY $parameters ,eg $parameters['id'] = 12 , $parameters['action'] = 'add', $parameters['msg'] = 'error',
	*
	*/
	function breakEncodedURI($ParKey) {
		$decodedKey = base64_decode($ParKey);
		$parameters = array();
		foreach(explode("&",$decodedKey) as $pKey => $parameter) {
			$paraNameValuePair = explode("=",$parameter);
			$paraName = $paraNameValuePair[0];
			$parVar = $paraNameValuePair[1];
			$parameters[$paraName] = $parVar;
		}
		return $parameters;
	}



	function ConvertBytes($number,$IsConvertUnite=true)
	{
		if (!$IsConvertUnite)
			return $number;
	    $len = strlen($number);
	    if($len < 4)
	    {
	        return sprintf("%d b", $number);
	    }
	    if($len >= 4 && $len <=6)
	    {
	        return sprintf("%0.2f Kb", $number/1024);
	    }
	    if($len >= 7 && $len <=9)
	    {
	        return sprintf("%0.2f Mb", $number/1024/1024);
	    }

	    return sprintf("%0.2f Gb", $number/1024/1024/1024);

	}


	function ep_hwinfo($intranet_root,$mysql_disk_root='/var/lib/mysql',$IsConvertUnite=true) {
		# r1
		# In Bytes

		$tmp_disk_root="/tmp";

		$hwinfo['mysql_total_space'] = disk_total_space($mysql_disk_root);
		$hwinfo['mysql_free_space'] = disk_free_space($mysql_disk_root);
		//$hwinfo['mysql_used_space'] = disk_used_space($mysql_disk_root);
		$hwinfo['mysql_used_space'] = ConvertBytes($hwinfo['mysql_total_space'] - $hwinfo['mysql_free_space'], $IsConvertUnite);
		$hwinfo['mysql_total_space'] = ConvertBytes($hwinfo['mysql_total_space'], $IsConvertUnite);
		$hwinfo['mysql_free_space'] = ConvertBytes($hwinfo['mysql_free_space'], $IsConvertUnite);
		$hwinfo['eclass_total_space'] = disk_total_space($intranet_root);
		$hwinfo['eclass_free_space'] = disk_free_space($intranet_root);
		//$hwinfo['eclass_used_space'] = disk_used_space($intranet_root);
		$hwinfo['eclass_used_space'] = ConvertBytes($hwinfo['eclass_total_space'] - $hwinfo['eclass_free_space'], $IsConvertUnite);
		$hwinfo['eclass_total_space'] = ConvertBytes($hwinfo['eclass_total_space'], $IsConvertUnite);
		$hwinfo['eclass_free_space'] = ConvertBytes($hwinfo['eclass_free_space'], $IsConvertUnite);
		$hwinfo['tmp_total_space'] = disk_total_space($tmp_disk_root);
		$hwinfo['tmp_free_space'] = disk_free_space($tmp_disk_root);
		$hwinfo['tmp_used_space'] = ConvertBytes($hwinfo['tmp_total_space'] - $hwinfo['tmp_free_space'], $IsConvertUnite);
		$hwinfo['tmp_total_space'] = ConvertBytes($hwinfo['tmp_total_space'], $IsConvertUnite);
		$hwinfo['tmp_free_space'] = ConvertBytes($hwinfo['tmp_free_space'], $IsConvertUnite);

		$hwinfo['sys_total_memory'] = ConvertBytes(trim(str_replace('kB','',shell_exec('cat /proc/meminfo |grep MemTotal |cut -d: -f2'))) * 1024, $IsConvertUnite);
		$hwinfo['sys_free_memory'] = ConvertBytes(trim(str_replace('kB','',shell_exec('cat /proc/meminfo |grep MemFree |cut -d: -f2')))* 1024, $IsConvertUnite);
		$hwinfo['sys_total_swap'] = ConvertBytes(trim(str_replace('kB','',shell_exec('cat /proc/meminfo |grep SwapTotal |cut -d: -f2')))* 1024, $IsConvertUnite);
		$hwinfo['sys_free_swap'] = ConvertBytes(trim(str_replace('kB','',shell_exec('cat /proc/meminfo |grep SwapFree |cut -d: -f2')))* 1024, $IsConvertUnite);
		$hwinfo['sys_used_swap'] = ConvertBytes(($hwinfo['sys_total_swap'] - $hwinfo['sys_free_swap'])* 1024, $IsConvertUnite);
		$hwinfo['sys_virtual_cpu'] = trim(shell_exec('cat /proc/cpuinfo |grep \'physical id\'|wc -l'));
		$hwinfo['sys_physical_cpu'] = trim(shell_exec('cat /proc/cpuinfo |grep \'physical id\' |tail -n 1 |cut -d: -f2')) + 1;
		$hwinfo['sys_cpu_model'] = trim(shell_exec('cat /proc/cpuinfo |grep \'model name\' |head -1|cut -d: -f2')) ;


		return $hwinfo;
	}


	function convert_line_breaks($string, $line_break=PHP_EOL) {
	    $patterns = array(
	                        "/(<br>|<br \/>|<br\/>)\s*/i",
	                        "/(\r\n|\r|\n)/"
	    );
	    $replacements = array(
	                            PHP_EOL,
	                            $line_break
	    );
	    $string = preg_replace($patterns, $replacements, $string);
	    return $string;
	}



	function format_datetime_weekday_sql($field_name){
                global $jr_hw_cal, $intranet_session_language;

                $x = ($intranet_session_language=="en") ? "date_format({$field_name}, '%W  %b %d, %Y %l:%i %p')" : "date_format({$field_name}, '%Y".$jr_hw_cal['year']."%m".$jr_hw_cal['month']."%d".$jr_hw_cal['day']."(%a) %l:%i%p')";

                return $x;
        }

        function format_date_weekday_sql($field_name){
                global $jr_hw_cal, $intranet_session_language;

                $x = ($intranet_session_language=="en") ? "date_format({$field_name}, '%W  %b %d, %Y')" : "date_format({$field_name}, '%Y".$jr_hw_cal['year']."%m".$jr_hw_cal['month']."%d".$jr_hw_cal['day']."(%a)')";

                return $x;
        }

        function format_datetime_sql($field_name){
                global $jr_hw_cal, $intranet_session_language;

                $x = ($intranet_session_language=="en") ? "date_format({$field_name}, '%b %d, %Y %l:%i %p')" : "date_format({$field_name}, '%Y".$jr_hw_cal['year']."%m".$jr_hw_cal['month']."%d".$jr_hw_cal['day']." %l:%i%p')";

                return $x;
        }

        function format_date_sql($field_name){
                global $jr_hw_cal, $intranet_session_language;

                $x = ($intranet_session_language=="en") ? "date_format({$field_name}, '%b %d, %Y')" : "date_format({$field_name}, '%Y".$jr_hw_cal['year']."%m".$jr_hw_cal['month']."%d".$jr_hw_cal['day']."')";

                return $x;
        }

        function format_date_no_year_sql($field_name){
                global $jr_hw_cal, $intranet_session_language;

                $x = ($intranet_session_language=="en") ? "date_format({$field_name}, '%b %d')" : "date_format({$field_name}, '%m".$jr_hw_cal['month']."%d".$jr_hw_cal['day']."')";

                return $x;
        }

        function format_month_day_week_sql($field_name){
                global $jr_hw_cal, $intranet_session_language;

                $x = ($intranet_session_language=="en") ? "date_format({$field_name}, '%b %d (%a)')" : "date_format({$field_name}, '%m".$jr_hw_cal['month']."%d".$jr_hw_cal['day']."(%a)')";

                return $x;
        }

        function parseWeekday($date){
                global $i_frontpage_day, $intranet_session_language;

                if ($intranet_session_language=="en")
                        return $date;

                $patterns[0] = "/Sun/";
                $patterns[1] = "/Mon/";
                $patterns[2] = "/Tue/";
                $patterns[3] = "/Wed/";
                $patterns[4] = "/Thu/";
                $patterns[5] = "/Fri/";
                $patterns[6] = "/Sat/";
                $replacements = $i_frontpage_day;

                return preg_replace($patterns, $replacements, $date);
        }



	function DisAllowiPadAndriod()
	{
		global $userBrowser, $Lang;
		if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
		{
            $rx .= "<script language=\"javascript\" >\n";
			$rx .= "
				alert(\"".$Lang['General']['ReturnMessage']['NotiPadRichtextEditor']."\");
				history.back();
			 	";
			$rx .= "</script>\n";
		}

		echo $rx;
	}

	function safe_b64encode($string) {
	        $data = base64_encode($string);
	        $data = str_replace(array('+','/','='),array('-','_',''),$data);
	        return $data;
	    }

	function safe_b64decode($string) {
	        $data = str_replace(array('-','_'),array('+','/'),$string);
	        $mod4 = strlen($data) % 4;
	        if ($mod4) {
	            $data .= substr('====', $mod4);
	        }
	        return base64_decode($data);
	    }

	function AES_128_Decrypt($encrypted_text, $encrypt_key)
	{
		if(!function_exists('mcrypt_get_iv_size')) return safe_b64decode($encrypted_text);
		$size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$iv   = mcrypt_create_iv($size, MCRYPT_DEV_URANDOM);

		preg_match('/([\x20-\x7E]*)/',mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $encrypt_key, @pack("H*", $encrypted_text), MCRYPT_MODE_ECB, $iv), $a);

		return $a[0];
	} // End of function

	function AES_128_Encrypt($text,$encrypt_key)
	{
		if(!function_exists('mcrypt_get_iv_size')) return safe_b64encode($text);

		$size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$iv = mcrypt_create_iv($size, MCRYPT_DEV_URANDOM);

		// The following line was needed because I didn't get the same hex value as expected by forwarding agency
		// I think its their bug
		// Try to remove the line. If it works, too - fine!
		$text .= chr(3).chr(3).chr(3);

		return bin2hex(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $encrypt_key, $text, MCRYPT_MODE_ECB, $iv));

	} // End of function

	function GetDecryptedPassword($ParamPassword){
		global $intranet_password_salt;
		return AES_128_Decrypt($ParamPassword, $intranet_password_salt);
	}

	function Get_Dates_By_Repetitive_WeekDay_Within_Range($StartDate, $EndDate, $WeekDayArr='')
	{
		$StartDateTS = strtotime($StartDate);
		$EndDateTS = strtotime($EndDate);
		$thisDateTS = $StartDateTS;

		while($thisDateTS <= $EndDateTS)
		{
			$thisDate = date('Y-m-d', $thisDateTS);
			$thisDateDay = date('w', $thisDateTS);

			if ($WeekDayArr=='' ||  in_array($thisDateDay, (array)$WeekDayArr)) {
				$ReturnArr[$thisDate] = $thisDateDay;
			}
			$thisDateTS = strtotime("+1 day", $thisDateTS);

		}
		return $ReturnArr;
	}


	function utf8_strlen($str, $ChineseCountAs=1)
    {
	    $count = 0;
	    $CountAs = 1;

	    for($i = 0; $i < strlen($str); $i++)
	    {
	        $value = ord($str[$i]);
	        if($value > 127)
	        {
	            if($value >= 192 && $value <= 223)
	            {
	                $i++;
	                $CountAs = 1;
	            } elseif($value >= 224 && $value <= 239)
	            {
	                $i = $i + 2;
	                $CountAs = $ChineseCountAs;
	            } elseif($value >= 240 && $value <= 247)
	            {
	                $i = $i + 3;
	                $CountAs = $ChineseCountAs;
	            }
	        }

	   		$count += $CountAs;
	    }

	    return $count;
    }


    # for SMS, cut the message before sending
	function sms_substr($str)
	{
		# remove line breaks
		$str = str_replace("\r\n", "", $str);

	    $count = 0;
	    $CountAs = 1;

	    $EnglishMax = 160;
	    $ChineseMax = 70;
	    $IsChinese = false;

	    $ReturnStr = "";

	    for($i = 0; $i < strlen($str); $i++)
	    {
	        $value = ord($str[$i]);
	        $pre_i = $i;
	        $char_length = 1;
	        if($value > 127)
	        {
	            if($value >= 192 && $value <= 223)
	            {
	                $i++;
	                $char_length = 2;
	            } elseif($value >= 224 && $value <= 239)
	            {
	                $i = $i + 2;
	                $IsChinese = true;
	                $char_length = 3;
	            } elseif($value >= 240 && $value <= 247)
	            {
	                $i = $i + 3;
	                $IsChinese = true;
	                $char_length = 4;
	            }
	        }

	        if (($IsChinese && $count>=$ChineseMax) || $count>=$EnglishMax)
	    	{
	    		break;
	    	} else
	    	{
	   			$count += $CountAs;
	   			$ReturnStr .= substr($str, $pre_i, $char_length);
	    	}
	    }

	    return $ReturnStr;
	}


	# detect the language of wordings inside
	function str_lang($str)
    {
	    $count = 0;

		$IsChinese = false;

	    for($i = 0; $i < strlen($str); $i++)
	    {
	        $value = ord($str[$i]);
	        if($value > 127)
	        {
	            if($value >= 192 && $value <= 223)
	            {
	                $i++;
	                //echo substr($str, $i-1, 2)."<br />";
	            } elseif($value >= 224 && $value <= 239)
	            {
	                $i = $i + 2;
	        		$LangFound["CHI"] = true;
	            } elseif($value >= 240 && $value <= 247)
	            {
	                $i = $i + 3;
	        		$LangFound["CHI"] = true;
	            }
	        } else
	        {
	        	$LangFound["ENG"] = true;
	        }
	    }

	    if ($LangFound["ENG"] && $LangFound["CHI"])
	    {
	    	$LangInside = "MIXED";
	    } elseif ($LangFound["ENG"])
	    {
	    	$LangInside = "ENG";
	    } elseif ($LangFound["CHI"])
	    {
	    	$LangInside = "CHI";
	    }

	    return $LangInside;
    }


    # get the text of particular languages (Eng, Chi)
	function substr_lang($str, $Language="ENG")
	{
	    $count = 0;
	    $CountAs = 1;


	    $ReturnStr = "";

	    for($i = 0; $i < strlen($str); $i++)
	    {
	        $value = ord($str[$i]);
	        $pre_i = $i;
	        $char_length = 1;
	    	$IsChinese = false;
	        if($value > 127)
	        {
	            if($value >= 192 && $value <= 223)
	            {
	                $i++;
	                $CountAs = 1;
	                $char_length = 2;
	            } elseif($value >= 224 && $value <= 239)
	            {
	                $i = $i + 2;
	                $IsChinese = true;
	                $char_length = 3;
	            } elseif($value >= 240 && $value <= 247)
	            {
	                $i = $i + 3;
	                $IsChinese = true;
	                $char_length = 4;
	            }
	        }
	   		$count += $CountAs;
	   		if (($Language=="ENG" && !$IsChinese) || ($Language=="CHI" && $IsChinese) )
	   			$ReturnStr .= substr($str, $pre_i, $char_length);
	    }

	    return $ReturnStr;
	}

	function Get_String_Display($String, $checkEmptyOnly=0, $CustomEmptySymbol=NULL)
	{
		global $Lang;

		$EmptySymbol = $CustomEmptySymbol!==NULL?$CustomEmptySymbol:$Lang['General']['EmptySymbol'];

		if(trim($String)=='')
			return $EmptySymbol;
		else if($checkEmptyOnly)
			return $String;
		else
			return nl2br(htmlspecialchars($String));
	}

	function Get_String_from_Options_Array($value, $AssocArray, $usingEmptyString=0){

		if(!empty($AssocArray[$value])){
			$string = $AssocArray[$value];
		}
		else if($usingEmptyString){
			$string = Get_String_Display($value);
		}
		else{
			$string = $value;
		}

		return $string;
	}

	// format SmartCardID to ten characters pre-padding with zero if length not enough ten
	function FormatSmartCardID($CardID)
	{
		global $special_feature;

		if(!$special_feature['SkipFormatSmartCardID'])
		{
			if(trim($CardID) == "") return "";
			$CardID = sprintf("%010s",$CardID);

			//return sprintf("%010s",$CardID);
		}
		return $CardID;
	}

	function Get_Standardized_PDF_Table_Data_Display($ParData)
	{
		global $Lang;

		$Data = trim($ParData);
		$Data = str_replace('<', '&lt;', $Data);
		$Data = str_replace('>', '&gt;', $Data);
		$Data = nl2br($Data);

		return $Data;
	}

	function floatcmp($f1, $sym, $f2, $threshold = 1e-8)
	{

		$boo = null;
		switch($sym)
		{
			case "==":
				$boo = (abs($f1-$f2) < $threshold);
			break;

			case "<=":
			case ">=":
				eval('$boo = $f1 '.$sym.' $f2;');
				$boo = $boo || (abs($f1-$f2) < $threshold);
			break;

			case ">":
			case "<":
				eval('$boo = $f1 '.$sym.' $f2;');
				$boo = $boo && !(abs($f1-$f2) < $threshold);
			break;
		}

	    return $boo;
	}

	function getHtmlByUrl($url, $tokenExtraStr='') {
   		// Set $_COOKIE string
		//$strCookie = 'PHPSESSID=' . $_COOKIE['PHPSESSID'] . '; path=/';

		$strCookie = '';
		foreach ((array)$_COOKIE as $_key => $_value) {
			$strCookie .= $_key.'='.$_value.';';
		}
		$strCookie .= '; path=/';
		// $generatedCsrf = generateCsrfToken();

		// [2019-0618-1504-34066] Handle data passes to target url
		$_POST_DATA = $_SESSION;
		if($tokenExtraStr != '') {
		    $_POST_DATA['token'] = returnSimpleTokenByDateTime($tokenExtraStr);
		}

		session_write_close();
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));  // Added on 2014-11-18 - avoid use http header "Expect: 100-continue"
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_COOKIE, $strCookie);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST_DATA);
		$html = curl_exec($ch);
		curl_close($ch);

		return str_replace("\n", "\r\n", trim($html));
   	}

   	/*
	* Encrypt Password
	*/
	function ENCRYPT_PASSWORD($ParPassword, $Length=0){

		$Encrypt1 = base64_encode($ParPassword);
		$MidPos = ceil(strlen($Encrypt1)/2);
		$Encrypta = substr($Encrypt1, 0, $MidPos);
		$Encryptb = substr($Encrypt1, $MidPos);

		$password_enc = base64_encode($Encryptb.$Encrypta."=".(strlen($Encrypt1)-$MidPos));
		if ($Length>0 && strlen($password_enc)>$Length+1)
		{
			$password_enc = substr($password_enc, 1, $Length);
		}

		return $password_enc;
	}



	/*
	* Decrypt Password
	*/
	function DECRYPT_PASSWORD($ParPassword){

		$Encrypt1 = base64_decode($ParPassword);
		$SplitSizePos = strrpos($Encrypt1, "=");
		$MidPos = (int) substr($Encrypt1, $SplitSizePos+1);
		$Encrypt1 = substr($Encrypt1, 0, $SplitSizePos);
		$Encrypta = substr($Encrypt1, 0, $MidPos);
		$Decryptb = substr($Encrypt1, $MidPos);

		return base64_decode($Decryptb.$Encrypta);
	}

	// if SSO iCalendar from app => cannot access other pages
	if ($_SESSION['eClassAppSSO_standaloneModule_iCalendar'] && stripos($_SERVER["SCRIPT_NAME"], '/home/iCalendar/')===false) {
		if ((stripos($_SERVER["SCRIPT_NAME"], 'logout.php') !== false) || (stripos($_SERVER["SCRIPT_NAME"], 'login.php') !== false) || (stripos($_SERVER["SCRIPT_NAME"], '/api/eClassApp/sso_intranet.php') !== false)) {
			// exclude "logout.php", "login.php", and "sso_intranet.php" to do logout action
		}
		else {
			header("location: /logout.php");
			die();
		}
	}


	$current_host = $_SERVER["HTTP_HOST"];
   	if (!$skip_PortNo_diff_check && $current_host!="" && $_SESSION["SSV_HTTP_HOST"]!="" && $current_host!=$_SESSION["SSV_HTTP_HOST"] && !strstr($_SERVER["REQUEST_URI"], "logout.php"))
	{
		# cross session happens when using different port number
		# now, block such case by forcing to logout
		//echo $eclass_httppath . " vs " . $_SESSION["eclass_httppath_ss"];
		header("location: /logout.php");
		die();
	}
	$_SESSION["SSV_HTTP_HOST"] = $current_host;


	$global_bad_word_list = "null";

	function getBadWordList()
    {
		global $intranet_root, $global_bad_word_list;

		$base_dir = "$intranet_root/file/templates/";

		$target_file = "$base_dir"."mail_badwords.txt";

		if ($global_bad_word_list=="null")
		{
			$global_bad_word_list = get_file_content($target_file);
		}

		return $global_bad_word_list;
    }

	# filter bad words (according to iMail bad words preset)
	function BadWordFilter($Text)
	{
		global $intranet_root;

		$data = getBadWordList();

		$phrase = explode("\n",$data);
		for($i=0; $i<count($phrase); $i++)
		{
			if(trim($phrase[$i])!='')
			{
				$bad_word_preg[] = "/".preg_quote(trim($phrase[$i]),'/')."/i";
			}
		}

		return preg_replace((array)$bad_word_preg,"***",$Text);

	}

	function standardizeFormPostValue($val) {
		return trim(stripslashes($val));
	}
	function standardizeFormGetValue($val) {
		return trim(rawurldecode(stripslashes($val)));
	}

	function getEncryptedText($plainText, $key='bXwKs7S93J2') {
	    global $intranet_root;

		// Check if json.php included
		if(!class_exists('JSON_obj')) {
		    include_once($intranet_root."/includes/json.php");
		}

		// Add this encrypted record into session so that all string encrypted within this login
		// can be decrypted no matter how long it has been generated
		if(!isset($_SESSION['ENCRYPTED_TOKEN'])){
		    $_SESSION['ENCRYPTED_TOKEN'] = intranet_random_passwd(16);
		}

		// Take the current timestamp into the encrypted payload
		$_json = new JSON_obj;

		$payload = $_json->encode(array('t' => time(), 'c' => $plainText, 's' => $_SESSION['ENCRYPTED_TOKEN']));

		include_once($intranet_root."/includes/liburlparahandler.php");

		$lurlparahandler = new liburlparahandler(liburlparahandler::actionEncrypt, $payload, $key);
		return $lurlparahandler->getParaEncrypted();
	}

	function getDecryptedText($encryptedText, $key='bXwKs7S93J2', $expiryMinute=10, $ignoreTimeChecking=false) {
 	    global $intranet_root, $sys_custom;
 	    include_once($intranet_root."/includes/liburlparahandler.php");

		$lurlparahandler = new liburlparahandler(liburlparahandler::actionDecrypt, $encryptedText, $key);
		$decrypted = $lurlparahandler->getParaDecrypted();

		// Check if json.php included
		if(!class_exists('JSON_obj')) {
		    include_once($intranet_root."/includes/json.php");
		}

		// Take the current timestamp into the encrypted payload
		$_json = new JSON_obj;
		$payload = $_json->decode($decrypted);

		// Check if encrypted string under the same session. If so, there should be no time limit
		$token = isset($_SESSION['ENCRYPTED_TOKEN']) ? $_SESSION['ENCRYPTED_TOKEN'] : null;

		// Check if payload timestamp within 10mins ( or make a conf time for it )
		# $now = strtotime('2099-05-18 02:30:00');
		$now = time();
		if(isset($sys_custom['DecryptTextExpiryMinutes']) && $sys_custom['DecryptTextExpiryMinutes'] > 0) {
			$expiryMinute = $sys_custom['DecryptTextExpiryMinutes'];
		}
		//$expired = ($now - $payload['t']) / 60 > 10 ;
		$expired = (($now - $payload['t']) / 60) > $expiryMinute;

		# Return a fatal error and stop. Or catch this exception when using this function
		if($expired && $_SESSION['ENCRYPTED_TOKEN'] != $payload['s'] && !$ignoreTimeChecking) {
			// throw new ExpiredContentExpection('Accessing Expired Content');
			die("E001 - Code expired");
		}

		# Or Just return null and let it go
		// reutn $expired ? null : $payload['c']
        return $payload['c'];


	}

	# thorw this Exception if getDecryptedText() fails on timestamp checking
	Class ExpiredContentExpection extends Exception {}

	function getDecryptedAry($encryptedText, $key='bXwKs7S93J2') {
	    global $intranet_root;
	    include_once($intranet_root."/includes/liburlparahandler.php");

		$lurlparahandler = new liburlparahandler(liburlparahandler::actionDecrypt, $encryptedText, $key);
		return $lurlparahandler->getParaAry();
	}

	function EncryptModuleFilePars($Module, $UserLogin, $UID, $RecordID)
	{
		return getEncryptedText("Module=$Module&UserLogin=$UserLogin&UID=$UID&RecordID=$RecordID");
	}

	function DecryptModuleFilePars($Key)
	{
		$key_de = getDecryptedText($Key);
		$key_array = explode("&", $key_de);
		for ($i=0; $i<sizeof($key_array); $i++)
		{
			$key_array_sub = explode("=", $key_array[$i]);
			global ${trim($key_array_sub[0])};
			${trim($key_array_sub[0])} = trim($key_array_sub[1]);
		}
	}


	function getDefaultDateFormat($RecordDate){

		$record = explode('/',$RecordDate);

		if((strpos($RecordDate, '/') == true) && (count($record) == 3)){
			list($day, $month, $year) = $record;
			#D/M/YYYY or DD/MM/YYYY
			$year = trim($year);
			$day = trim($day);
			$month = trim($month);

			$day = str_pad($day, 2, '0', STR_PAD_LEFT);
			$month = str_pad($month, 2, '0', STR_PAD_LEFT);

			$RecordDate = $year . '-' . $month . '-' . $day;
		}

		return $RecordDate;
	}

	function handleFormPost(&$item, $key, $parMethod='') {
		$item = normalizeFormParameter($item);

		if (intranet_phpversion_compare('5.4')=='SAME' || intranet_phpversion_compare('5.4')=='LATER') {
			global ${$key};
			${$key} = $item;
		}
	}

	/* normalize the form parameter, e.g. strip slash */
	function normalizeFormParameter($var) {
		if (!isset($var)) return NULL;

		$var = (get_magic_quotes_gpc() || intranet_phpversion_compare('5.4')=='SAME' || intranet_phpversion_compare('5.4')=='LATER')? stripslashes($var) : $var;
		$var = rawurldecode(trim($var));

		return $var;
	}


	# Get AMFphp GateWay Path
	function getAMFphpGateWay($src_folder_referer, $src_folder){
		global $HTTP_SERVER_VARS, $_SERVER;

		if (($referer=$HTTP_SERVER_VARS["HTTP_REFERER"])=="")
		{
			$referer = getenv("HTTP_REFERER");
		}
		if ($referer!="" &&  $src_folder_referer!="" && strstr($referer, $src_folder_referer))
		{
			$ecGatewayPath = substr($referer, 0, strpos($referer, $src_folder_referer))."/includes";
		} else
		{
			$urlHost = (getenv("HTTP_HOST")!="") ? getenv("HTTP_HOST") : $_SERVER["HTTP_HOST"];
			$urlSrc = (getenv("SCRIPT_NAME")!="") ? getenv("SCRIPT_NAME") : $_SERVER["SCRIPT_NAME"];
			$ecGatewayPath = "http://".$urlHost.substr($urlSrc, 0, strpos($urlSrc, $src_folder))."/includes";
			//Wrong modification?
			//$ecGatewayPath = "http://".$urlHost.substr($urlSrc, 0, strpos($urlSrc, $src_folder))."/eclass40/src/includes";
		}

		return $ecGatewayPath;
	}



	# house keeping is usually carried out when the first logout takes place
	function IsHouseKeepingDoneToday()
	{
		global $li;

		if (!isset($li))
		{
			$li = new libdb();
		}
		$sql = "SELECT HouseKeepingID FROM INTRANET_HOUSEKEEPING_LOG WHERE DATE(InputTime) = DATE(NOW())";
		$rows = $li->returnVector($sql);

		return (sizeof($rows)>0);

	}


	# house keeping is usually carried out when the first logout takes place
	function LogHouseKeeping()
	{
		global $li;

		$uid = $_SESSION["UserID"];
		if (!isset($li))
		{
			$li = new libdb();
		}
		$sql = "INSERT INTO INTRANET_HOUSEKEEPING_LOG (UserID, InputTime) VALUES ('$uid', NOW())";

		return $rows = $li->db_db_query($sql);

	}


	# dummy -- for Library Management System
	function tolog()
	{
		return;
	}

	function synWSRegnoToIntranetUser(){
		//syn WebSamsRegno From Ipf To intranet_user
		global $eclass_db;
		$NoOfRecord  = 0;
		$ldb = new libdb();

		$sql = "SELECT u.UserID, s.WebSAMSRegNo FROM $eclass_db.PORTFOLIO_STUDENT s INNER JOIN INTRANET_USER u ON (u.UserID=s.UserID AND u.RecordType=2 AND s.WebSAMSRegNo is not null AND u.WebSAMSRegNo='')";
		$result = $ldb->returnResultSet($sql);

		$NoOfRecord = count($result);
		for($i=0; $i<$NoOfRecord; $i++) {
			$userid = $result[$i]['UserID'];
			$websams = $result[$i]['WebSAMSRegNo'];

			$sql = "UPDATE INTRANET_USER SET WebSAMSRegNo='$websams' WHERE UserID='$userid'";
			$ldb->db_db_query($sql);

		}
		return $NoOfRecord;
	}


	function CacheUniqueID($user_id)
	{
		global $intranet_session_language, $REMOTE_ADDR, $intranet_root, $intranet_db;
		# user_id, type, language, refer, src_version

		if (is_file($intranet_root."/includes/version.php"))
		{
			$eClassVersion = Get_IP_Version();

			$pos = strrpos($eClassVersion, ".");
			if($pos>10)	$eClassVersion = substr($eClassVersion, 0, $pos);

		} else
		{
			$eClassVersion = "--";
		}

		$cacheID = "U".$user_id."V".str_replace(".","_",$eClassVersion)."L".$intranet_session_language."IP".str_replace(".","_",$REMOTE_ADDR)."_".$intranet_db;
		//debug($cacheID);

		return $cacheID;
	}

	//Copy from eclass lib-filemanager.php
	function getFileExtention($myFile, $isLower=0) {
		$filetmp = explode(".", $myFile);
		$rx = $filetmp[sizeof($filetmp)-1];
		$rx = ($isLower) ? strtolower($rx) : strtoupper($rx);

		return $rx;
	}
 	//Copy from EJ intranet lib.php
	function getClassNumberForOrderBy($prefix="")
	{
		$col_field = $prefix."ClassNumber";
		$orderby_field = " CONCAT(
							IF(".$col_field." is null || ".$col_field." = '', NULL, LPAD(TRIM(".$col_field."), 10, '0'))
						  ) ";
		return $orderby_field;
	}

	function getMonthDifferenceBetweenDates($date1, $date2) {
		$ts1 = strtotime($date1);
		$ts2 = strtotime($date2);

		$year1 = date('Y', $ts1);
		$year2 = date('Y', $ts2);

		$month1 = date('m', $ts1);
		$month2 = date('m', $ts2);

		return (($year2 - $year1) * 12) + ($month2 - $month1);
	}

	function returnSelectedStudentList($student=array(), $permitted)
	{
		include_once("libgrouping.php");
		include_once("libclass.php");
		$li = new libgrouping();
		$lclass = new libclass();

		foreach($student as $k=>$d)
		{
			$type = substr($d, 0, 1);
			$type_id = substr($d, 1);
			switch($type)
			{
				case "U":
					$student2[] = $type_id;
					break;
				case "G":
					$groupIDAry[] = $type_id;
					$temp = $li->returnGroupUsersInIdentity($groupIDAry, $permitted);
					foreach($temp as $k1=>$d1)
					{
						$student2[] = $d1['UserID'];
					}
					break;
				case "C":
					$temp = $lclass->getStudentListByClassID($type_id);
					for($i=0; $i<sizeof($temp); $i++) {
						$student2[] = $temp[$i][0];
					}
					break;
				default:
					$student2[] = $d;
					break;

			}
		}

		if(is_array($student2))
		{
			$student= array_keys(array_count_values($student2));
		}

		return($student);
	}


	# convert to integer value (support multiple integers by default)
	# set $Separater='' if don't want to support multiple integers
	# $returnString is not used anymore, for backward compatible only
	function IntegerSafe($iVal, $Separater=",", $returnString=false)
	{
		$type = gettype($iVal);
		if(in_array($type,array("boolean","integer","double","object","resource","resource (closed)","NULL","unknown type"))){
			return $iVal;
		}

		if (is_array($iVal))
		{
			if(count($iVal)==0){
				return $iVal;
			}
			# keep in array format
			$iReturn = array();

			foreach($iVal as $k => $v)
			{
				$iReturn[$k] = IntegerSafe($v, $Separater, $returnString);
			}
		} elseif ($iVal=="")
		{
			# for empty, return empty rather than 0
			$iReturn = $iVal;
		} elseif ($Separater!="")
		{
			# handle for multiple values separated by a character
			$newArr = array();
			$tmpArr = explode($Separater, $iVal);

			for ($i=0; $i<sizeof($tmpArr); $i++)
			{
				$newArr[] = (int)$tmpArr[$i];
			}

			$iReturn = implode($Separater, $newArr);
		} else
		{
			$iReturn = (string)(int)$iVal;
		}

		return $iReturn;
	}

	# Check and Compare PHP version
	# use to determinate whether new PHP function can be used
	function phpversion_compare($myVersion){
		$sys_arr = explode(".", phpversion());
		$my_arr = explode(".", $myVersion);

		$result = "SAME";
		for ($i=0; $i<sizeof($my_arr); $i++)
		{
			if ($my_arr[$i]>$sys_arr[$i])
			{
				$result = "ELDER";
				break;
			} elseif ($my_arr[$i]<$sys_arr[$i])
			{
				$result = "LATER";
				break;
			}
		}
		return $result;
	}

	/**
	 * Convert the basename of a given path from utf8 to big5-hkscs, and return the new path with the basename converted.
	 */
	function convert_basename_from_utf8_to_big5($path)
	{
		global $g_chinese;

		$final_encode = $g_chinese == "gb"?"GBK":"big5-hkscs";

		$base_name = get_file_basename($path);
		$dir_name = dirname($path);
		if ($base_name != '.' && iconv('utf8', 'utf8', $base_name) === $base_name) {
			// Basename is of UTF8, do conversion.
			$new_base_name = iconv('utf8', $final_encode, $base_name);
			//$new_base_name = convertUTF8Big5GB($base_name);
			if ($new_base_name != '' && $new_base_name != $base_name) {
				if ($dir_name != '.') {
					return "$dir_name/$new_base_name";
				}
				else {
					return $new_base_name;
				}
			}
		}
		return $path;
	}

	/**
	 * Rename a file or all files/directories under a directory from utf8 to big5-hkscs.
	 */
	function rename_filename_from_utf8_to_big5($path)
	{
		// Find files/directories recursively.
		if (is_dir($path)) {
			$handle = opendir($path);
			while (false !== ($resource = readdir($handle))) {
				if (!in_array($resource, array('.', '..'))) {
					rename_filename_from_utf8_to_big5("$path/$resource");
				}
			}
			closedir($handle);
		}

		$new_path = convert_basename_from_utf8_to_big5($path);
		if ($new_path != $path) {
			// File/directory name is of UTF8, rename.
			rename($path, $new_path);
		}
	}

	/**
	 * convert a number to letter(s) (eg. 1 -> a, 27 -> aa)
	 */
	function numberToLetter ($number, $uppercase = false) {
		$number--;
		$letter = chr($number % 26 + 97);
		if ($number >= 26) {
			$letter = numberToLetter(floor($number/26),$uppercase).$letter;
		}
		return ($uppercase ? strtoupper($letter) : $letter);
	}

	function isKIS($parUserLogin='') {
		global $DebugMode, $plugin;

		$isKIS = false;
		if ((strstr($parUserLogin,"kis_") && $DebugMode) || $plugin["platform"]=="KIS") {
			$isKIS = true;
		}

		return $isKIS;
	}

	# b5: 2015 [Year] 3 [Month] 25 [Day] [AM] 10:48
	# en: 2015-3-25 10:48 AM
	function get_formatted_datetime($lang=''){
		global $intranet_session_language, $Lang;
		$str = '';

		if($lang == ''){
			$lang = $intranet_session_language;
		}

		switch ($lang){
			case 'en':
				$str = date("Y-m-d h g:i")." ".$Lang['eSurvey'][date("A")];
				break;
			case 'b5':
				$str = date("Y")." ".$Lang['eSurvey']['Year']." ".date("n")." ".$Lang['eSurvey']['Month']." ".date("j")." ".$Lang['eSurvey']['Day']." ".$Lang['eSurvey'][date("A")]." ".date("g:i");
				break;
		}
		return $str;
	}

	# Copy from eclass40 by Thomas on 2015-07-24
	# Encode Only Folder and File Names
	# won't affect "/"
	if(!function_exists('encode_filenames')){
		function encode_filenames($filename){
			$name_arr = explode("/", $filename);
			for ($i=0; $i<sizeof($name_arr); $i++)
			{
				if (trim($name_arr[$i])!="")
				{
					$rx .= rawurlencode($name_arr[$i]);
				}
				$rx .= ($i<sizeof($name_arr)-1) ? "/" : "";
			}
			return $rx;
		}
	}

	# Copy from eclass40 by Thomas on 2015-07-24
	// For encoding the path in a URL twice, so that Chinese path works in Flash.
	// The path may be decoded once when Flash receives the flashvars from HTML.
	// The path may then be decoded again when the web server receives the path.
	// This can prevent Flash from encoding the path sent to the web server improperly using the code page setting of the client (when using streaming functions).
	if(!function_exists('encode_url_for_flashvars')){
		function encode_url_for_flashvars($url) {
			$http_host = preg_replace('/(http:\/\/[^\/]*)?(.*)/', '$1', $url);
			$path = preg_replace('/(http:\/\/[^\/]*)?(.*)/', '$2', $url);
			//$path =rawurlencode($path);
			$path = str_replace('%2F', '/', rawurlencode($path));
			$path = str_replace('%2F', '/', rawurlencode($path));
			return $http_host . $path;
		}
	}

	if(!function_exists('in_encrypt_string_blacklist')) {
		function in_encrypt_string_blacklist($str)
		{
			$blacklist = array("sudo");
			$str = strtolower($str);
			foreach($blacklist as $temp) {
				if (strpos($str, $temp) !== false) {
					return true;
				}
			}
			return false;
		}
	}

	# Copy from eclass40 by Thomas on 2015-07-24
	# Encrypt String
	# the input path must be encrypt 2 times and swrap via Encrypt_String()
	if(!function_exists('encrypt_string')){
		function encrypt_string($str){
			$Encrypt1 = base64_encode($str);
			$MidPos = ceil(strlen($Encrypt1)/2);
			$Encrypta = substr($Encrypt1, 0, $MidPos);
			$Encryptb = substr($Encrypt1, $MidPos);

			$str_encode = base64_encode($Encryptb.$Encrypta."=".(strlen($Encrypt1)-$MidPos));
			$in_blacklist = in_encrypt_string_blacklist($str_encode);
			if($in_blacklist) {
				$count = 0;
				$new_str = $str.'{RANDOMSTRING}';
				while($in_blacklist && $count < 1024) {
					$count++;
					$new_str .= rand(0,9);
					$Encrypt1 = base64_encode($new_str);
					$MidPos = ceil(strlen($Encrypt1)/2);
					$Encrypta = substr($Encrypt1, 0, $MidPos);
					$Encryptb = substr($Encrypt1, $MidPos);
					$str_encode = base64_encode($Encryptb.$Encrypta."=".(strlen($Encrypt1)-$MidPos));
					$in_blacklist = in_encrypt_string_blacklist($str_encode);
				}
			}
			return $str_encode;
		}
	}

	# Copy from eclass40 by Thomas on 2015-07-24
	# Decrypt String
	if(!function_exists('decrypt_string')){
		function decrypt_string($str){
			$Encrypt1 = base64_decode($str);
			$SplitSizePos = strrpos($Encrypt1, "=");
			$MidPos = (int) substr($Encrypt1, $SplitSizePos+1);
			$Encrypt1 = substr($Encrypt1, 0, $SplitSizePos);
			$Encrypta = substr($Encrypt1, 0, $MidPos);
			$Decryptb = substr($Encrypt1, $MidPos);

			$str_decode = base64_decode($Decryptb.$Encrypta);
			$pos = strpos($str_decode, '{RANDOMSTRING}');
			if($pos !== false) {
				$str_decode = substr($str_decode, 0, $pos);
			}
			return $str_decode;
		}
	}

	/*
	 * 	Purpose:	for keyword search like 					$str = Book <>/'"&\ 20150512
	 * 				where the value stored in db like this: 	Book &lt;&gt;/'&quot;&amp;\ 20150512
	 */
	if(!function_exists('special_sql_str')){
		function special_sql_str($str){
			return addslashes(htmlspecialchars(str_replace("\\","\\\\",$str)));
		}
	}

	/*
	 * 	replace " to &quot;		-- for showing value in input field
	 */
	if(!function_exists('escape_double_quotes')){
		function escape_double_quotes($str){
			return str_replace("\"","&quot;",$str);
		}
	}

	/*
	 * Convert acceptable datetime formats to the standard format YYYY-MM-DD hh:mm:ss
	 * Acceptable date formats:
	 * MM-DD-YYYY or M-D-YYYY
	 * MM/DD/YYYY or M/D/YYYY
	 * YYYY/MM/DD or YYYY/M/D
	 * YYYY-MM-DD or YYYY-M-D
	 * Acceptable time formats:
	 * h:m:s or hh:mm:ss
	 * Set $withTime to false if only date is inputted, i.e. no time component.
	 */
	function convertDateTimeToStandardFormat($datetime, $withTime=true)
	{
		if($withTime)
		{
			$parts = explode(' ',trim($datetime)); // use the space character to separate date and time component
			$date_part = $parts[0];
			$time_part = $parts[1];
			if($time_part == '') // if no time component, set it to 00:00:00
			{
				$time_part = '00:00:00';
			}
		}else{
			$date_part = trim($datetime);
		}
		$return_datetime = '';
		$matches = array();
		if(preg_match('/^(\d{1,2})-(\d{1,2})-(\d\d\d\d)$/',$date_part, $matches)) ### MM-DD-YYYY or M-D-YYYY
		{
			$date_part = sprintf('%04d-%02d-%02d', $matches[3], $matches[1], $matches[2]);
		}else if(preg_match('/^(\d{1,2})\/(\d{1,2})\/(\d\d\d\d)$/',$date_part, $matches)){ ### MM/DD/YYYY or M/D/YYYY
			$date_part = sprintf('%04d-%02d-%02d', $matches[3], $matches[1], $matches[2]);
		}else if(preg_match('/^(\d\d\d\d)\/(\d{1,2})\/(\d{1,2})$/',$date_part, $matches)){ ### YYYY/MM/DD or YYYY/M/D
			$date_part = sprintf('%04d-%02d-%02d', $matches[1], $matches[2], $matches[3]);
		}else if(preg_match('/^(\d\d\d\d)-(\d{1,2})-(\d{1,2})$/',$date_part, $matches)){ ### YYYY-MM-DD or YYYY-M-D
			$date_part = sprintf('%04d-%02d-%02d', $matches[1], $matches[2], $matches[3]);
		}
		$return_datetime = $date_part;
		if($withTime)
		{
			$time_components = explode(':',$time_part);
			$time_part = sprintf('%02d:%02d:%02d',$time_components[0],$time_components[1],$time_components[2]); // pad 0 to time components
			$return_datetime .= ' '.$time_part;
		}

		return $return_datetime;
	}

	/*
	 * 	return array of images from richtext
	 */
	function getImageFromText($s){
		$ret = array();
		if (stripos($s,"&lt;img") !== false) {
			$s = htmlspecialchars_decode($s);
		}
		$start = stripos($s,"<img");
		if ($start !== false) {
			$finish = false;
			while (!$finish && $start !== false) {
				$end = strpos($s,">",$start);
				$ret[] = substr($s,$start,$end-$start+1);
				if (strlen($s) > $end) {
					$s = substr($s,$end+1);
					$start = stripos($s,"<img");
				}
				else {
					$finish = true;
				}
			}
		}
		return $ret;
	}	// getImageFromText


	/*
	 * 	Provide string like "US$103.82" return number parts: 103.82
	 */
	function get_number_from_text($str) {
		if (preg_match("/^([A-Za-z$]*)([\d]+)(\.*)([\d]*)$/i", $str, $matches)) {

			return $matches[2].$matches[3].$matches[4];
		}
		else {
			return 0;
		}
	}

	/*
	 *  remove new line, carriage return, tab and back slash before calling json_encode() for ajax return
	 */
	function remove_dummy_chars_for_json($str){
		$from[] = "\n";
		$from[] = "\r";
		$from[] = "\t";
		$to[] = "";
		$to[] = "";
		$to[] = "";
		if (get_magic_quotes_gpc()) {
			$from[] = "\\";
			$to[] = "\\\\";
		}
		return str_replace($from,$to,$str);
	}

	function start_log()
	{
		global $sys_custom, $GLOBAL_LOG_TIME, $DO_NOT_LOG_PERFORMANCE;

		if(!$sys_custom['PerformanceLog'] || $DO_NOT_LOG_PERFORMANCE) return;

		$mtime = microtime();
		$mtime = explode(" ",$mtime);
		$mtime = $mtime[1] + $mtime[0];

		$GLOBAL_LOG_TIME = $mtime;
	}

	function stop_log()
	{
		global $intranet_root, $file_path, $sys_custom, $GLOBAL_LOG_TIME, $DO_NOT_LOG_PERFORMANCE, $GLOBAL_LOG_QUERY;

		if(!$sys_custom['PerformanceLog'] || !is_array($sys_custom['PerformanceLogPath'])) return;
		if($GLOBAL_LOG_TIME == '' || $DO_NOT_LOG_PERFORMANCE) return;
		$request_uri = @parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH);
		$request_uri = rtrim($request_uri,'/');
		$request_uri = rtrim($request_uri,'#');
		if(!in_array($request_uri,$sys_custom['PerformanceLogPath'])){
			return false;
		}

		$num_line_to_keep = isset($sys_custom['PerformanceLogMaxLine'])? $sys_custom['PerformanceLogMaxLine'] : 200000;
		$log_file_path = $file_path."/file/performance_log";
		$log_file_path_tmp = $file_path."/file/performance_log_tmp";

		$mtime = microtime();
		$mtime = explode(" ",$mtime);
		$mtime = $mtime[1] + $mtime[0];

		$elapsed_time = $mtime - $GLOBAL_LOG_TIME;

		$log_row = sprintf("%s %s %fS %dB %d", date("Y-m-d H:i:s"), $_SERVER['REQUEST_URI'], $elapsed_time, memory_get_usage(), isset($_SESSION['UserID'])?$_SESSION['UserID']:0);
		if($sys_custom['PerformanceLogPostValues'] && count($_POST)>0){
			$post_values = str_replace(array("\r\n","\n","\r")," ", serialize($_POST));
			$log_row .= ' '.$post_values;
		}else{
			$log_row .= ' ';
		}
		if($sys_custom['QueryLog'] && count($GLOBAL_LOG_QUERY)>0){
			$log_row .= ' '.serialize($GLOBAL_LOG_QUERY);
		}
		if($sys_custom['PerformanceLogDebugBackTrace']){
			$back_trace = serialize(debug_backtrace());
			$log_row .= ' '.$back_trace;
		}
		shell_exec("echo '$log_row' >> $log_file_path"); // append to log file
		$line_count = intval(shell_exec("cat '$log_file_path' | wc -l"));
		if($line_count > $num_line_to_keep){
			shell_exec("tail -n ".($num_line_to_keep/2)." '$log_file_path' > '$log_file_path_tmp'"); // keep the last $num_line_to_keep/2 lines
			shell_exec("rm '$log_file_path'");
			shell_exec("mv '$log_file_path_tmp' '$log_file_path'");
		}
	}

	// to log and alert user keep refreshing the page
	if (file_exists($intranet_root.'/includes/libuserabuse.php') && (isset($sys_custom['doNotCheckF5'])==false||$sys_custom['doNotCheckF5']==false))
	{
		include_once($intranet_root.'/includes/libuserabuse.php');
		$monitor_refresh_interal = (isset($monitor_refresh_interal))?$monitor_refresh_interal:60;
		$max_allowed_refresh = (isset($max_allowed_refresh))?$max_allowed_refresh:50;
		$userAbuse = new abuse($monitor_refresh_interal, $max_allowed_refresh, $_SESSION['UserID']);
	}


	// Customization for PL20 direct login
	if(
	    ($sys_custom['power_lesson_2']['directLogin'] &&
	    $plugin['power_lesson_2'] || $sys_custom['PowerClass_GWP']) &&
	    $_SESSION['UserID'] &&
	    (strpos($_SERVER['REQUEST_URI'], '/pl2/portalLogin.php') !== 0) &&
	    (strpos($_SERVER['REQUEST_URI'], '/logout.php') !== 0) &&
	    (strpos($_SERVER['REQUEST_URI'], '/webserviceapi/') !== 0) &&
	    (strpos($_SERVER['REQUEST_URI'], '/file/') !== 0) &&
	    (strpos($_SERVER['REQUEST_URI'], '/elearningapi/') !== 0)
	){
	    if($sys_custom['PowerClass']){
	    	$isSkipDirectLogin = strstr(implode(",",(array)$_SESSION['SSV_USER_ACCESS']), '1');
	    	unset($result);
	    } else {
	    	if($sys_custom['power_lesson_2']['directLoginExceptAdminUser']){
		    	$isSkipDirectLogin = $_SESSION['SSV_PRIVILEGE']['schoolsettings']['isAdmin'];
		    } else {
		        include_once($intranet_root."/includes/libdb.php");
		        include_once($intranet_root."/includes/libuser.php");

		        intranet_opendb();

		        $libuser = new libuser($_SESSION["UserID"]);
		        $isSkipDirectLogin = $libuser->isTeacherStaff();

		        intranet_closedb();
	    	}
	    }

	    if(
	        !$isSkipDirectLogin &&
	        !(
	            $sys_custom['PowerClass'] &&
	            strpos($_SERVER['REQUEST_URI'], '/home/index.php') !== 0 &&
	            strpos($_SERVER['REQUEST_URI'], '/home/PowerClass/index.php') !== 0
	        )
	    ){
	        if($sys_custom['PowerClass_GWP']){
	            $url = '/home/PowerClass/gowith.php';
	        }else{
	           $url = '/pl2/portalLogin.php';
	        }
	        header("Location: $url");
	        exit;
	    }
	}

	// Customization for STEM x PL2 SSO
	if(
	    $plugin['stem_x_pl2'] &&
	    $sys_custom['StemPL2Client'] &&
	    $_SESSION['UserID'] &&
	    !$_SESSION['SSV_PRIVILEGE']['schoolsettings']['isAdmin'] &&
	    (strpos($_SERVER['REQUEST_URI'], '/home/stem/powerlesson/sso.php') !== 0) &&
	    (strpos($_SERVER['REQUEST_URI'], '/logout.php') !== 0) &&
	    (strpos($_SERVER['REQUEST_URI'], '/webserviceapi/') !== 0) &&
	    (strpos($_SERVER['REQUEST_URI'], '/file/') !== 0) &&
	    (strpos($_SERVER['REQUEST_URI'], '/elearningapi/') !== 0)
	){
	    include_once( $intranet_root . "/includes/libdb.php");
	    include_once( $intranet_root . "/includes/libuser.php");

	    intranet_opendb();

	    $luser = new libuser($_SESSION['UserID']);

	    $stemLicenseConfig = require $intranet_root."/includes/StemLicense/StemLicense.inc.php";
	    $stemLicenseApiUrl = $stemLicenseConfig['serverPath'] . 'eclass40/src/stem/license/api';

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $stemLicenseApiUrl . '/accessibility/school/'.$config_school_code . '/user/' . $luser->UserLogin);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 300);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Accept: application/json"));
	    curl_exec($ch);

	    $statusCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);

	    intranet_closedb();

	    if(!(curl_errno($ch) > 0) && $statusCode >= 200 && $statusCode <= 299){
	        header("Location: /home/stem/powerlesson/sso.php");
	        exit;
	    }

	    header("Location: /templates/StemPL2Client/login.php?err=403");
	    exit;
	}

	function Get_GoogleSSO_Service_Provider_Name($email){
		global $ssoservice;

		$spName = '';
		if($email != '' && strpos($email, "@") !== false){
			# get mail domain
			$temp1 = explode('@', $email);

			$domain = (count($temp1) > 0) ? $temp1[1] : '';

			if($domain != ''){
				if($ssoservice["Google"]['Enable_SubDomain']['blers'])
				{
					# for blers / blers7 only
					if($domain == 'eclass.uccke.edu.hk')
					{
						# for testing the uccke education on bler7 site only
						$spName = 'ucckeasidp-sp';
					}
					else
					{
						 $temp2 = explode(".", $domain);
						 $prefix = (count($temp2) > 0) ? $temp2[0] : '';
						switch($prefix){
							# Service Provider Name is set in /simplesaml-sp/config/authsources.php which document is set the same level as /intranetIP/
							case 'g1': $spName = 'googleasidp-sp'; break;
							case 'g2': $spName = 'g2asidp-sp'; break;
							case 'g3': $spName = 'g3asidp-sp'; break;
							case 'g4': $spName = 'g4asidp-sp'; break;
							default : break;
						}
					}
				}
				else
				{
					# each school would have its independent and fined SP Name - which is set in global.php or settings.php
					$spName = $ssoservice["Google"]["SimpleSAML_ServiceProvide_Name"];
				}
			}
		}
		else{
			# each school would have its independent and fined SP Name - which is set in global.php or settings.php
			$spName = $ssoservice["Google"]["SimpleSAML_ServiceProvide_Name"];
		}
		return $spName;
	}

	/*
	 * $textAreaId (String): the textarea name used to be replaced
	 * $defaultText (String): to reload the text editted before
	 * $toolSet (String): which tools would be shown on the toolbar (default set / sba set)
	 * $height (int): height of the editor
	 * $width (int): width of the editor
	 */
	function getCkEditor($textAreaId="editor", $defaultText="", $toolSet="default",$height="auto", $width="auto"){
		global $PATH_WRT_ROOT, $intranet_session_language,$intranet_root;
		include($intranet_root.'/templates/ckeditor/config.php');
		$intranet_httppath = $_SERVER['HTTP_HOST'].'/';
		$langCode = ($intranet_session_language=='b5')?'zh':(($intranet_session_language=="gb")?'zh-cn':'en');
		$editor = "";

		//$http_protocol = ($_SERVER['SERVER_PORT']  == 443)?'https':'http';
		$http_protocol = checkHttpsWebProtocol()?'https':'http';
		$intranet_httppath = $http_protocol.'://'.$intranet_httppath;
		$editor .= '<script>window.CKEDITOR_BASEPATH="'.$intranet_httppath.'templates/ckeditor/";</script>';
		$editor .= '<link rel="stylesheet" href="'.$intranet_httppath.'templates/ckeditor/toolbar.css">';
		$editor .= '<script src="'.$intranet_httppath.'templates/ckeditor/ckeditor.js"></script>';
		$editor .= '<script src="'.$intranet_httppath.'templates/ckeditor/fckToCk.js"></script>'; //This file is to provide map functions in fckeditor to the correpsonding functions in ckeditor.
		$editor .= '<script>';
		$editor .= "if ( CKEDITOR.env.ie && CKEDITOR.env.version < 9 )
						CKEDITOR.tools.enableHtml5Elements( document );

					// The trick to keep the editor in the sample quite small
					// unless user specified own height.";
		$extraPlugins = "";
		if(!isset($toolbarGroups[$toolSet])){
			$toolSet = "default";
		}else{
			foreach($extraTool[$toolSet] as $tool){
				$extraPlugins .= $tool.",";
			}
		}
		$extraPlugins .= $defaultExtraTool;
		$editor .= "
					var init".$textAreaId." = ( function() {
						var wysiwygareaAvailable = isWysiwygareaAvailable(),
							isBBCodeBuiltIn = !!CKEDITOR.plugins.get( 'bbcode' );

						return function() {
							var editorElement = CKEDITOR.document.getById( '".$textAreaId."' );

							// :(((
							if ( isBBCodeBuiltIn ) {
								editorElement.setHtml(
									'". str_replace("<br />", "", trim(strip_tags(urlencode($defaultText))))."'
								);
							}
							var ".$textAreaId." = CKEDITOR.replace( '".$textAreaId."', {
							    customConfig: '',
							  	height: '".$height."',
							  	width: '".$width."',
								enterMode: CKEDITOR.ENTER_BR,
							  	toolbarCanCollapse : true,
							    extraPlugins: '".$extraPlugins."',
							    filebrowserBrowseUrl: '',
							    filebrowserUploadUrl: '',
							    language: '".$langCode."',
							    toolbar :  [";
		$editor .= "
													".$toolbarGroups[$toolSet]."";
		$editor .= "
												]
							});
							".$textAreaId.".on( 'change', function( evt ) {
							    // getData() returns CKEditor's HTML content.
							    document.getElementById('".$textAreaId."').innerHTML = evt.editor.getData();
							});
							// Depending on the wysiwygare plugin availability initialize classic or inline editor.

							if ( wysiwygareaAvailable ) {
								//CKEDITOR.replace( '".$textAreaId."' );
							} else {
								editorElement.setAttribute( 'contenteditable', 'true' );
								CKEDITOR.inline( '".$textAreaId."' );

								// TODO we can consider displaying some info box that
								// without wysiwygarea the classic editor may not work.
							}
						};

						function isWysiwygareaAvailable() {
							// If in development mode, then the wysiwygarea must be available.
							// Split REV into two strings so builder does not replace it :D.
							if ( CKEDITOR.revision == ( '%RE' + 'V%' ) ) {
								return true;
							}

							return !!CKEDITOR.plugins.get( 'wysiwygarea' );
						}
					} )();";
		$editor .= '</script>';

		$editor .= '<textarea id="'.$textAreaId.'"  name="'.$textAreaId.'">';
		$editor .= $defaultText;
		$editor .= '</textarea>';

		$editor .= '<script>';
		$editor .= "	init".$textAreaId."();

						var FCKeditorAPI = CKEDITOR;
						FCKeditorAPI.GetInstance = function(name){
							return CKEDITOR.instances[name];
						};
						";
		$editor .= '</script>';
		return $editor;
	}

	function getRemoteIpAddress()
	{
		if(isset($_SERVER["HTTP_X_REAL_IP"]) && $_SERVER["HTTP_X_REAL_IP"] != '')
		{
			$remote_addr = $_SERVER["HTTP_X_REAL_IP"];
		}else{
			$remote_addr = $_SERVER['REMOTE_ADDR'];
		}

		return $remote_addr;
	}

	function checkCurrentIP($parIPAddressAry, $parModuleName){
		$ip_allowed = false;

		// check current IP within allowed IP addresses

		for($i=0; $i<count($parIPAddressAry); $i++) {
			$parIPAddressAry[$i] = trim($parIPAddressAry[$i]);
			if(testip($parIPAddressAry[$i],getRemoteIpAddress())) {
				$ip_allowed = true;
			}
		}
		if(in_array('0.0.0.0',$parIPAddressAry) || count($parIPAddressAry)==0 || $parIPAddressAry[0] == '') {
			$ip_allowed = true;
		}

		// display alert message if current IP not allowed
		if(!$ip_allowed){
			global $Lang;

			$script .= '<html>'."\n";
			$script .= '<head>'."\n";
			$script .= '<meta http-equiv="content-type" content="text/html;charset=Big5" />'."\n";
			$script .= '<script>'."\n";
			$script .= 'alert("'.str_replace("<!--ModuleName-->", $parModuleName, $Lang['Security']['WarnMsgArr']['ComputerIpNotAllowed']).'");'."\n";
			$script .= 'window.location.href = "/home/index.php";'."\n";
			$script .= '</script>'."\n";
			$script .= '</head>'."\n";
			$script .= '</html>'."\n";

			echo $script;
			exit;
		}
		return;
	}

	function downloadAttachmentRelativePathToAbsolutePath($parRelativePath) {
		global $intranet_root, $PATH_WRT_ROOT;

		$rootPieces = explode('/', $intranet_root);
		array_pop($rootPieces);
		$intranetDataRoot = implode('/', $rootPieces).'/';

		return str_replace($PATH_WRT_ROOT.'../', $intranetDataRoot, $parRelativePath);
	}

	/*
	 * 	given a book title (including Chinese), return words in array
	 * 	e.g. $title = "A new English course"
	 * 	return Array
			(
			    [0] => A
			    [1] => new
			    [2] => English
			    [3] => course
		    )
	 */

	function getWords($title) {
		$new_words = array();
		$words = explode(' ',$title);
		foreach((array)$words as $word) {
			if ($word != '') {
				if (!mb_check_encoding($word, 'ASCII')) {	// non-ascii
					$len = mb_strlen($word, "utf-8");
					$ascii_word = array();
					for($i=0;$i<$len;$i++){
						$character = mb_substr($word, $i,1,"utf-8");
						if (mb_check_encoding($character, 'ASCII')) {	// ascii
							$ascii_word[] = $character;
						}
						else {
							if (count($ascii_word)) {
								$new_words[] = implode('',$ascii_word);	// concat character to ascii word
								$ascii_word = array();
							}
							$new_words[] = $character;	// non-ascii
						}
					}
					if (count($ascii_word)) {
						$new_words[] = implode('',$ascii_word);	// concat character to ascii word
					}
				}
				else {	// ascii
					$new_words[] = $word;
				}
			}
		}
		return $new_words;
	}

	/* Remove script tags and onxxxx event handlers */
	function cleanHtmlJavascript($html)
	{
		$max_iteration = 10000000;

		$scripttag_start_pos = 0;
		$scripttag_end_pos = 0;
		$open_script_tag = '<script';
		$close_script_tag = '</script>';

		$iteration_count = 0;
		do
		{
			$iteration_count++;
			$scripttag_start_pos = stripos($html, '<script', $scripttag_start_pos); // find next <script position
			if($scripttag_start_pos !== false){ // if find next <script, find </script> position
				//echo '$scripttag_start_pos='.$scripttag_start_pos.'<br />';
				$scripttag_end_pos = stripos($html,$close_script_tag,$scripttag_start_pos);
				if($scripttag_end_pos !== false){ // found </script>
					//echo '$scripttag_end_pos='.$scripttag_end_pos.'<br />';
					$str_length = $scripttag_end_pos + 9 - $scripttag_start_pos;
					$substr = substr($html,$scripttag_start_pos,$str_length); // extract string in between <script and </script>
					//echo '$substr='.htmlspecialchars($substr).'<br />';
					$head_str = substr($html,0,$scripttag_start_pos);
					$tail_str = substr($html,$scripttag_end_pos + 9);
					$html = $head_str.$tail_str; // construct the new html text with the script tag cleaned

				}else{ // no </script> found, can end the finding
					$scripttag_start_pos = false;
				}
			}
			if($iteration_count > $max_iteration){ // avoid dead loop
				break;
			}
		}while($scripttag_start_pos !== false);

		$start_pos = 0;
		$end_pos = 0;
		$open_bracket = '<';
		$close_bracket = '>';
		$onevent_pattern = '/\s*on\w+\s*=\s*("[^"]+"|\'[^\']+\')\s*/';
		$whole_str_length = strlen($html);

		$iteration_count = 0;
		do{
			$iteration_count++;
			$start_pos = stripos($html,$open_bracket,$start_pos); // find next < position
			if($start_pos !== false){ // if find next <, find > position
				//echo '$start_pos='.$start_pos.'<br />';
				$end_pos = stripos($html,$close_bracket,$start_pos);
				if($end_pos !== false){ // found >
					//echo '$end_pos='.$end_pos.'<br />';
					$str_length = $end_pos + 1 - $start_pos;
					$substr = substr($html,$start_pos,$str_length); // extract string in between < and >
					//echo '$substr='.htmlspecialchars($substr).'<br />';
					$cleaned_str = preg_replace($onevent_pattern,' ',$substr); // replace all onxxxx="" attributes
					$cleaned_str_length = strlen($cleaned_str);
					if($cleaned_str_length <  $str_length){ // if has replaced onxxx attributes
						//echo '$cleaned_str='.htmlspecialchars($cleaned_str).'<br />';
						$head_str = substr($html,0,$start_pos);
						$tail_str = substr($html,$end_pos+1);
						$html = $head_str.$cleaned_str.$tail_str; // construct the new html text with the attributes cleaned string
					}
					$start_pos = $start_pos + $cleaned_str_length;
				}else{ // no > found, can end the finding
					$start_pos = false;
				}
			}
			if($iteration_count > $max_iteration){ // avoid dead loop
				break;
			}
		}while($start_pos !== false);

		return $html;
	}

	// clean parameters javascript codes
	function cleanCrossSiteScriptingCode($value)
	{
		if(is_array($value)){
			foreach($value as $k => $v){
				$value[$k] = cleanCrossSiteScriptingCode($v);
			}
		}else{
			$value = str_replace(array('<', '>', ';', 'javascript:', '"', '\'', '(', ')'),'',$value);
		}

		return $value;
	}

	if(!function_exists('OsCommandSafe')){
	    function OsCommandSafe($cmd){
	        return str_replace(';', '', $cmd);
	        //return escapeshellcmd($cmd);
	        //return escapeshellarg($cmd);
	    }
	}

	if(!function_exists('ReturnUrlSafe'))
	{
		function ReturnUrlSafe($url)
		{
			// removed "http://", "https://" to prevent being redirected to external site
			// removed current url path to avoid infinite redirect to self page
			// removed domain name and non 80|443 port
			$port = (!in_array($_SERVER["SERVER_PORT"],array(80,443))?":".$_SERVER["SERVER_PORT"]:"");
			$domain_name = $_SERVER['SERVER_NAME'].$port;
			$safe_url = str_replace(array('http://','https://',$domain_name,$_SERVER['SCRIPT_NAME'],substr($_SERVER['SCRIPT_NAME'], strrpos($_SERVER['SCRIPT_NAME'],'/')+1)),'',$url);
			return $safe_url;
		}
	}

	function returnSimpleTokenByDateTime($extraStr='')
	{
	    global $intranet_root, $special_feature;

	    $delimiter = "###";
	    $keyStr = $special_feature['ParentApp_PrivateKey'].$delimiter.date('Y-m-d H:i');
	    //$keyStr = getEncryptedText(md5($keyStr));

	    // [2019-0618-1504-34066] append extra text
	    if($extraStr != '') {
	        $keyStr .= $delimiter.$extraStr;
	    }

	    include_once($intranet_root."/includes/liburlparahandler.php");
	    $lurlparahandler = new liburlparahandler(liburlparahandler::actionEncrypt, md5($keyStr), 'bXwKs7S93J2');

	    return $lurlparahandler->getParaEncrypted();
	}

	function performSimpleTokenByDateTimeChecking($token, $extraStr='')
	{
	    global $intranet_root, $special_feature;
	    include_once($intranet_root."/includes/liburlparahandler.php");

	    $delimiter = "###";
	    $keyStrBase = $special_feature['ParentApp_PrivateKey'].$delimiter;

	    // valid with 5 mins
	    $keyStrArr = array();
	    $keyStrArr[] = $keyStrBase.date('Y-m-d H:i');
	    for($i=1; $i<5; $i++) {
	        $keyStrArr[] = $keyStrBase.date('Y-m-d H:i', strtotime("-".$i." minutes"));
	    }

	    $isMatched = false;
	    foreach((array)$keyStrArr as $thisKeyStr)
	    {
	        // [2019-0618-1504-34066] append extra text
	        if($extraStr != '') {
	            $thisKeyStr .= $delimiter.$extraStr;
	        }

	        // $thisKeyStr = getEncryptedText(md5($thisKeyStr));
	        $lurlparahandler = new liburlparahandler(liburlparahandler::actionEncrypt, md5($thisKeyStr), 'bXwKs7S93J2');
	        $thisKeyStr = $lurlparahandler->getParaEncrypted();
	        if($token == $thisKeyStr) {
	            $isMatched = true;
	            break;
	        }
	    }

	    return $isMatched;
	}

	function generateCardApiAccessCode($timestamp)
	{
		$key = base64_decode('NVVBRlgwV1Zsb1BzYlZsUTExVGVNWGt6UGwzZU1iS2s=');
		$access_code = sha1($timestamp.'_'.$key);
		return $access_code;
	}

	// generate csrf token and its key, saved to session, and return the data
	function generateCsrfToken()
	{
		$id = uniqid('',true);
		$ts = time();
		$csrf_token = sha1($ts.'_'.$id);
		$csrf_token_key = sha1($id);

		if(!isset($_SESSION['CSRF_TOKEN_SESSIONS'])){
			$_SESSION['CSRF_TOKEN_SESSIONS'] = array();
		}
		$_SESSION['CSRF_TOKEN_SESSIONS'][$csrf_token_key] = array('token'=>$csrf_token,'time'=>$ts);
		$csrf_token_data = array('csrf_token'=>$csrf_token,'csrf_token_key'=>$csrf_token_key);
		return $csrf_token_data;
	}
	// clean up expired saved in session csrf token data
	function cleanCsrfTokenSessions($expire_seconds=3600)
	{
		if(isset($_SESSION['CSRF_TOKEN_SESSIONS']) && count($_SESSION['CSRF_TOKEN_SESSIONS'])>0){
			$cur_time = time();
			foreach($_SESSION['CSRF_TOKEN_SESSIONS'] as $key => $token_data)
			{
				if(($cur_time > $_SESSION['CSRF_TOKEN_SESSIONS'][$key]['time'] + $expire_seconds)){
					unset($_SESSION['CSRF_TOKEN_SESSIONS'][$key]);
				}
			}
		}
	}
	// check and verify submitted csrf token and its key, compare with the saved in session token, and delete the token data
	function verifyCsrfToken($csrf_token, $csrf_token_key,$do_not_remove_token_data=false)
	{
		if(!isset($_SESSION['CSRF_TOKEN_SESSIONS'])){
			return false;
		}
		if(!isset($_SESSION['CSRF_TOKEN_SESSIONS'][$csrf_token_key])){
			return false;
		}
		$tmp_token_data = $_SESSION['CSRF_TOKEN_SESSIONS'][$csrf_token_key];
		$is_valid_token = $csrf_token == $tmp_token_data['token'];
		if(!$do_not_remove_token_data){
			unset($_SESSION['CSRF_TOKEN_SESSIONS'][$csrf_token_key]);
		}else{
			$_SESSION['CSRF_TOKEN_SESSIONS'][$csrf_token_key]['time'] = time();
		}
		cleanCsrfTokenSessions();

		return $is_valid_token;
	}
	// helper function to get the hidden fields html that save typing
	function csrfTokenHtml($csrf_token_data)
	{
		$html = '<input type="hidden" id="csrf_token" name="csrf_token" value="'.escape_double_quotes($csrf_token_data['csrf_token']).'" />'."\n";
		$html.= '<input type="hidden" id="csrf_token_key" name="csrf_token_key" value="'.escape_double_quotes($csrf_token_data['csrf_token_key']).'" />'."\n";

		return $html;
	}

	// set second argument $removeStatementChars to true to break php statements by removing the language symbols.
	function sanitizeEvalInput($value,$removeStatementChars=false)
	{
		if(is_array($value)){
			foreach($value as $k => $v){
				$value[$k] = sanitizeEvalInput($v,$removeStatementChars);
			}
		}else{
			if($removeStatementChars)
			{
				$value = str_replace(array('[', ']', ';', '"', '\'', '(', ')'),'',$value);
			}
			$func_list = array("eval", "base64_decode", "base64_encode", "gzdeflate", "gzuncompress", "exec", "passthru", "shell_exec", "system", "file_get_contents", "file_put_contents", "print_r", "print", "var_dump");
			$value = str_ireplace($func_list,'',$value);
		}

		return $value;
	}

	// Use empty iv, otherwise result would differs with other languages's encryption/decryption, e.g. C#
	function aes_cbc_128_encrypt($message, $key, $iv='')
	{
	    $srcdata = $message;
	    $block_size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
	    $padding_char = $block_size - (strlen($message) % $block_size);
	    $srcdata .= str_repeat(chr($padding_char),$padding_char); // padding pkcs7
	    return @mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $srcdata, MCRYPT_MODE_CBC, $iv);
	}

	// Use empty iv, otherwise result would differs with other languages's encryption/decryption, e.g. C#
	function aes_cbc_128_decrypt($xcrypt, $key, $iv='')
	{
	    $content = @mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $xcrypt, MCRYPT_MODE_CBC, $iv);
	    $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
	    $pad = ord($content[($len = strlen($content)) - 1]);
	    return substr($content, 0, strlen($content) - $pad);
	}

	// recursively check array style of $_POST /$_GET / $_COOKIE / Big Nested Array value
	function recursiveIntegerSafeHandling($param)
	{
		if(is_array($param)){
			foreach($param as $key=>$value){
				$param[$key] = recursiveIntegerSafeHandling($param[$key]);
			}
			return $param;
		}else{
			if(!preg_match('/^(\w\d+,?)+$/', $param)){
				return IntegerSafe($param);
			}else{
				return $param;
			}
		}
	}

	function blockIpWithDifferentRegion($path) {
		global $intranet_root, $REMOTE_ADDR, $region_of_client;

		if(isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != ''){
			if(strpos(str_replace('/','',$_SERVER['REQUEST_URI']),str_replace('/','',$path))===0 && $REMOTE_ADDR != '203.80.242.177' && !ip_is_private($REMOTE_ADDR)){

				$clientRegion = $region_of_client;

				$url = 'http://api.ipgeolocationapi.com/geolocate/'.$REMOTE_ADDR;
				$ch = curl_init();

				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_URL, $url);
			    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 300);
			    $geolocateData = curl_exec($ch);
				curl_close($ch);

				if(!class_exists('JSON_obj')) {
				    include_once($intranet_root."/includes/json.php");
				}
				$_json = new JSON_obj;
				$geolocateData = $_json->decode($geolocateData);

				if($geolocateData['alpha2'] && $clientRegion && strpos($clientRegion,$geolocateData['alpha2']) !== false){
					return false;
				}
				else{
					return true;
				}
			}
			else{
				return false;
			}
		}
		return false;
	}

	function ip_is_private ($ip) {
	    $pri_addrs = array (
	                      '10.0.0.0|10.255.255.255', // single class A network
	                      '172.16.0.0|172.31.255.255', // 16 contiguous class B network
	                      '192.168.0.0|192.168.255.255', // 256 contiguous class C network
	                      '169.254.0.0|169.254.255.255', // Link-local address also refered to as Automatic Private IP Addressing
	                      '127.0.0.0|127.255.255.255' // localhost
	                     );

	    $long_ip = ip2long ($ip);
	    if ($long_ip != -1) {

	        foreach ($pri_addrs as $pri_addr) {
	            list ($start, $end) = explode('|', $pri_addr);

	             // IF IS PRIVATE
	             if ($long_ip >= ip2long ($start) && $long_ip <= ip2long ($end)) {
	                 return true;
	             }
	        }
	    }

	    return false;
	}

	function checkIPandAccess($module){
		global $intranet_root;

		intranet_opendb();

		include_once($intranet_root."/includes/libdb.php");
		include_once($intranet_root."/includes/libgeneralsettings.php");

		$lgs = new libgeneralsettings();
		$settings = $lgs->Get_General_Setting($module);
		$allowed_IPs = $settings["AllowAccessIP"];

		// check current IP within allowed IP addresses
		$ip_addresses = explode("\n", $allowed_IPs);
		$ip_allowed = false;
		for($i=0; $i<count($ip_addresses); $i++) {
			$ip_addresses[$i] = trim($ip_addresses[$i]);
			if(testip($ip_addresses[$i], getRemoteIpAddress())) {
				$ip_allowed = true;
			}
		}
		if(in_array('0.0.0.0',$ip_addresses) || $allowed_IPs=='') {
			$ip_allowed = true;
		}

		// display message if current IP not allowed
		if(!$ip_allowed){
			die('Your computer is not allowed to enter Admin Console. Please contact System Administrator.');
		}
	}

	// handle DBTable required parameters to prevent sql injection and cross site scripting
	if(isset($field)){
		$field = IntegerSafe($field);
	}
	if(isset($order)){
		$order = IntegerSafe($order);
	}
	if(isset($pageNo)){
		$pageNo = IntegerSafe($pageNo);
	}
	if(isset($numPerPage)){
		$numPerPage = IntegerSafe($numPerPage);
	}
	if(isset($page_size)){
		$page_size = IntegerSafe($page_size);
	}
	if(isset($page_size_change)){
		$page_size_change = IntegerSafe($page_size_change);
	}

	// cast to integer for variables that are *ID/_ID/Id/_Id/_id patterns, and not in patterns (single non-digit character and numbers) such as "U1", "U1,U200,U3000"
	// ID used in timetable selection box name/id in /home/system_settings/timetable/view.php
	// 'performID', 'commentID','achievementID' used in eEnrollment update member performance and achievement
	// 'formClassId' find in eAdmin> eClassApp > ParentApp> Authorization Code > class selection box
	// 'ChooseDivisionID','ChooseDepartmentID' added for dhl common choose
	// 'calID' find in iCalendar /home/iCalendar/cal_visible.php
	// 'YearTermID' used in SDAS generate report e.g.Class Subject Performance Distribution
	// 'form_id' used in SDAS DSE prediction
	// 'AliasID' is used in iMail to classify address book or compose email choose recipient
	// 'FromYearTermID', 'ToYearTermID' used in SDAS generate report, e.g. Internal Value Added Index
	// 'RecipientID' used in staffMgmt circular edit page when get Group
	// 'FromSubjectID' used in SDAS Subject Stats
	$____exclude_var_names____ = array('RefID','ClassID','SelectionID','ObjID','fieldId','studentFieldId','ItemID','ID','editEventID','LocationID','LoginID','lsession_id','session_id','loginID','CatID','temp_id','userSelectedFieldId','performID', 'commentID','achievementID','BookIDUniqueID','formClassId','uID','ChooseDivisionID','ChooseDepartmentID'
	    ,'calID', 'YearClassID','YearTermID','form_id','CategoryID','ApplicationID','InputApplicationID','CardID','RFID','HKID','DeviceID','activityID','AliasID','SurveyID','FromYearTermID','ToYearTermID','RecipientID','ELE_ID','FromSubjectID','MerchantUID','SENTypeConfirmDate_ID');

	if (!defined("IGNORE_SECURITY_CHECK"))
	{
		if(count($_POST)){
			foreach($_POST as $tmp_key => $tmp_val){
				if(!in_array($tmp_key,$____exclude_var_names____))
				{
					if(preg_match('/^\w*(ID|_ID|Id|_Id|_id)$/',$tmp_key) && !preg_match('/^(\w)*(Sel|Div|Td|Tb|Slot|Session|Login)(ID|_ID|Id|_Id|_id)$/',$tmp_key)){
						$_POST[$tmp_key] = recursiveIntegerSafeHandling($_POST[$tmp_key]);
						if(isset($$tmp_key)){ // handle early PHP5 auto registered variables
							$$tmp_key = $_POST[$tmp_key];
						}
					}
				}
			}
		}
		if(count($_GET)){
			foreach($_GET as $tmp_key => $tmp_val){
				if(!in_array($tmp_key,$____exclude_var_names____))
				{
					if(preg_match('/^\w*(ID|_ID|Id|_Id|_id)$/',$tmp_key) && !preg_match('/^(\w)*(Sel|Div|Td|Tb|Slot|Session|Login)(ID|_ID|Id|_Id|_id)$/',$tmp_key)){
						$_GET[$tmp_key] = recursiveIntegerSafeHandling($_GET[$tmp_key]);
						if(isset($$tmp_key)){ // handle early PHP5 auto registered variables
							$$tmp_key = $_GET[$tmp_key];
						}
					}
				}
			}
		}
		if(count($_REQUEST)){
			foreach($_REQUEST as $tmp_key => $tmp_val){
				if(!in_array($tmp_key,$____exclude_var_names____))
				{
					if(preg_match('/^\w*(ID|_ID|Id|_Id|_id)$/',$tmp_key) && !preg_match('/^(\w)*(Sel|Div|Td|Tb|Slot|Session|Login)(ID|_ID|Id|_Id|_id)$/',$tmp_key)){
						$_REQUEST[$tmp_key] = recursiveIntegerSafeHandling($_REQUEST[$tmp_key]);
						if(isset($$tmp_key)){ // handle early PHP5 auto registered variables
							$$tmp_key = $_REQUEST[$tmp_key];
						}
					}
				}
			}
		}
		if(count($_COOKIE)){
			foreach($_COOKIE as $tmp_key => $tmp_val){
				if(!in_array($tmp_key,$____exclude_var_names____))
				{
					if(preg_match('/^\w*(ID|_ID|Id|_Id|_id)$/',$tmp_key) && !preg_match('/^(\w)*(Sel|Div|Td|Tb|Slot|Session|Login)(ID|_ID|Id|_Id|_id)$/',$tmp_key)){
						$_COOKIE[$tmp_key] = recursiveIntegerSafeHandling($_COOKIE[$tmp_key]);
						if(isset($$tmp_key)){ // handle early PHP5 auto registered variables
							$$tmp_key = $_COOKIE[$tmp_key];
						}
					}
				}
			}
		}
	}

	// blocked access admin console from different region of IP address
	if(blockIpWithDifferentRegion('admin')){
		die("Your access is not allowed. Please contact System Administrators.");
	}
	else{
		if(isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != '' && strpos(str_replace('/','',$_SERVER['REQUEST_URI']),str_replace('/','','admin'))===0 && $REMOTE_ADDR != '203.80.242.177'){
			checkIPandAccess('AdminConsole');
		}
	}

	$____block_file_upload_extensions____ = array('.php','.htaccess','.htgroup','.exe','.bat','.sh','.shtm','.shtml');

	if (isset($_FILES) && $_FILES) {
		foreach ((array)$_FILES as $_key => $_valueAry) {
			if(is_array($_valueAry['name'])){
				for($i=0; $i<count($_valueAry['name']); $i++) {
					$file = get_file_basename($_valueAry['name'][$i]);
			        $fileExt = strtolower(substr($file, strrpos($file,".")));

					if(in_array($fileExt, $____block_file_upload_extensions____)){
						logBlockFileUploadExtensions($_valueAry['name'][$i]);
						unset($_FILES[$_key]['tmp_name'][$i]);
						unset($_FILES[$_key]['name'][$i]);
						unset($_FILES[$_key]['type'][$i]);
						unset($_FILES[$_key]['size'][$i]);

						if(isset($$_key[$i])){
							unset($$_key[$i]);
						}

						$_varName = $_key.'_name';
						if(isset($$_varName[$i])){
							unset($$_varName[$i]);
						}

						$_varType = $_key.'_type';
						if(isset($$_varType[$i])){
							unset($$_varType[$i]);
						}

						$_varSize = $_key.'_size';
						if(isset($$_varSize[$i])){
							unset($$_varSize[$i]);
						}
					}
				}
			}
			else {
				$file = get_file_basename($_valueAry['name']);
		        $fileExt = strtolower(substr($file, strrpos($file,".")));

				if(in_array($fileExt, $____block_file_upload_extensions____)){
					logBlockFileUploadExtensions($_valueAry['name']);
					unset($_FILES[$_key]);

					if(isset($$_key)){
						unset($$_key);
					}

					$_varName = $_key.'_name';
					if(isset($$_varName)){
						unset($$_varName);
					}

					$_varType = $_key.'_type';
					if(isset($$_varType)){
						unset($$_varType);
					}

					$_varSize = $_key.'_size';
					if(isset($$_varSize)){
						unset($$_varSize);
					}
				}
			}
		}
	}
}
############### END OF BLOCK LIB LOADING
###############################################

} # End of if case of $ByPassLibPage

## END OF BLOCK LOADING


# Transfer $plugin['power_lesson_expiry_date'] to $plugin_expiry['power_lesson'] - Added by Thomas on 2015-05-15
if(isset($plugin['power_lesson_expiry_date']) && !isset($plugin_expiry['power_lesson'])){
	$plugin_expiry['power_lesson'] = $plugin['power_lesson_expiry_date'];
}
unset($plugin['power_lesson_expiry_date']);

function validateIntranetPlugin()
{
	global $plugin_expiry, $plugin;

	if(!empty($plugin_expiry))
	{
		# current date
		$today = date('Y-m-d');

		foreach($plugin_expiry as $plugin_name => $plugin_expiry_date)
		{
			# if not set plugin -> ignore checking
			if(isset($plugin[$plugin_name]) && $plugin[$plugin_name] != '')
			{
				# Case: passed the expiry date
				if(strtotime($today) > strtotime($plugin_expiry_date))
				{
					# disable the plugin;
					$plugin[$plugin_name] = false;
				}
			}
		}
	}
}

# to turn off the plugins if they get expired already
# no function call after this line except validateIntranetPlugin()
validateIntranetPlugin();

# Check and Compare PHP version for php 5.4+ use
# use to determinate whether new PHP function can be used
function intranet_phpversion_compare($myVersion){
	$sys_arr = explode(".", phpversion());
	$my_arr = explode(".", $myVersion);

	$result = "SAME";
	for ($i=0; $i<sizeof($my_arr); $i++)
	{
		if ($my_arr[$i]>$sys_arr[$i])
		{
			$result = "ELDER";
			break;
		} elseif ($my_arr[$i]<$sys_arr[$i])
		{
			$result = "LATER";
			break;
		}
	}
	return $result;
}

# it is used to handle the session variables which are created by session_register() &
# assign to the variable directly to be compatible in php 5.4 or above
if(isset($_SESSION)){
	foreach($_SESSION as $ss_key => $ss_data){
		if(!is_array($ss_data)){
			$$ss_key = $ss_data;
		}
	}
	if(isset($_SESSION['SSV_USER_ACCESS'])){
		global $SSV_USER_ACCESS;
		$SSV_USER_ACCESS = $_SESSION['SSV_USER_ACCESS'];
	}
	if(isset($_SESSION['SESSION_ACCESS_RIGHT'])){
		global $SESSION_ACCESS_RIGHT;
		$SESSION_ACCESS_RIGHT = $_SESSION['SESSION_ACCESS_RIGHT'];
	}
	if(isset($_SESSION['SSV_USER_TARGET'])){
		global $SSV_USER_TARGET;
		$SSV_USER_TARGET = $_SESSION['SSV_USER_TARGET'];
	}
}

switch(intranet_phpversion_compare('5.4')){
	case 'SAME':
	case 'LATER':
		// This replicates register_globals in PHP 5.4+
		foreach (array_merge($_GET, $_POST, $_COOKIE) as $global_register_key => $global_register_val) {
			if(!in_array($global_register_key,$____exclude_global_register_var_names____)){
				global $$global_register_key;
			  	$$global_register_key = $global_register_val;
			}
		}

		// global register $_FILES
		if (isset($_FILES)) {
			foreach ((array)$_FILES as $_key => $_valueAry) {
				$$_key = $_valueAry['tmp_name'];

				$_varName = $_key.'_name';
				$$_varName = $_valueAry['name'];

				$_varType = $_key.'_type';
				$$_varType = $_valueAry['type'];

				$_varSize = $_key.'_size';
				$$_varSize = $_valueAry['size'];
			}
		}
		break;
	default:
		break;

}

function convertOverflowNumber($number, $digitlimit){
    $maxNum = pow(10, $digitlimit) -1;
    $number>$maxNum? $convertedNumber=$maxNum.'+' : $convertedNumber=$number;
    return $convertedNumber;
}

// DON't DECLARE FUNCTION BELOW!!  DON't DECLARE FUNCTION BELOW!! DON't DECLARE FUNCTION BELOW!! DON't DECLARE FUNCTION BELOW!!
?>
