<?php
// using by

// ################ Change Log [Start] #####
//	Date: 2020-11-02 Philips [2020-1027-1009-54054]
//  - modified getTermAndTermAssessment(), added academic year field into query
//
//	Date: 2020-09-30 Philips [#V195915]
//	- modified getStudentSchoolExamAllSubjectResult(), use getStudentFormAcademicYearWithSubjectRecord() to get correct academic year
//
//  Date: 2020-09-15 Bill   [DM#3774]
//  - modified handleDseRawData(), chagned to apply predicted grade if absent parent subject
//
//  Date: 2020-08-14 Bill   [2020-0812-1602-15066]
//  - modified handleDseRawData(), fixed cannot import component subject result if absent parent subject has predicted grade
//
//	Date: 2020-07-15 Philips
//  - modified getHighChartJs_GroupedBarChart(), fix mode = '%' display
//  - modified getJupasVsDseReport(), Fixed incorrect DSE data collect ( add target academic year in parameter 1 of getExamData)
//
//	Date: 2019-11-26 Philips [2019-1118-0949-46206]
//	- added getExamYear(), to get year available
//
//  Date: 2019-08-09 Bill [2019-0808-1001-54235]
//  - modified getExamData(), to support return dse exam data of all cat subjects   (default true for DSE appeal)
//
//  Date: 2019-07-22 Anna
//  - Open HKAT for all school for pre-s1 report
//
//  Date: 2019-07-11 Anna [164850]
//  - modified  getElectivesLevel(), added getElectivesLevel to make U at last
//
//  Date: 2019-03-29 Isaac
//  - modified getJupasVsDseReport() - fixed no table issue when no Jupas Offer Data
//
//  Date: 2019-03-27 Anna
// - modified getClassListByYearName(), added $AcademicYearArr [#151664]
//
//  Date: 2019-01-14 Anna
// - added new function about STUNDET_EXIT_TO_INFO for twghs exit report
//
//  Date: 2018-09-19 Anna
//  - Modifed getExamData -added primarySchoolCode par
//
//  Date: 2018-06-26 Pun [ip.2.5.9.7.1]
//  - modified getStudentsYearInfoByID() - added support INTRANET_ARCHIVE_USER
//  Date: 2018-06-22 Anna
//  - modified getSchoolExamData() - added YearID
// Date : 2017-11-10 Omas
// - modified getCountOfLevelAbove() - fix divide 0 warning
//
// Date : 2017-11-03 Anna
// - modified saveDsePredictSchema() - added SelectedYearID
//
// Date : 2017-08-31 Omas
// - added getTermAndTermAssessment() - get school assessment type for dse prediction
// - modified getStudentSchoolExamAllSubjectResult() -support Term Assessment for dse prediction
// - modified getDseDistribution() , getDseAttendace() - support $sys_custom['SDAS']['DSE']['SubjectPanelOwnSubject']
//
// Date : 2017-07-11 Omas
// - modified getExamData() - for fixing M120256
// - modifed getDseDistribution() - fix sorting following school setting- for U120178
//
// Date : 2017-07-11 Villa
// - add getStudentExamDetail() - generate the Student DSE exam Detail
// - modified getBestStudent() - separate displayname To ClassName/ ClassNumber/ StudentName
// - support thickbox
// Date : 2017-06-21 Pun
// - modified getClassListByAcademicYearIDAndForm() - added return YearID
// - added getDsePredictResultAcademicYearId(), getDsePredictResult(), saveDsePredictSchema(), saveDsePredictResult(), getLastDsePredictSchema()
// Date : 2017-06-16 Villa
// - modified getExamData() - add DSEAPPEAL
// - modified deleteExamData() - add DSEAPPEAL
// - modified checkCompulsoryField() - add DSEAPPEAL
// - modified updateExamData() - add DSEAPPEAL
// - modified getExamDataForDisplay() -add DSEAPPEAL
// - modified getClassWithRecords() -add DSEAPPEAL
//
// Date : 2017-06-15 Omas
// - modified getStudentsInfoByID() - fix duplicated student problem
// - modified getCountOfLevelAbove(), getDseDistribution()- dse distribution exclude abs in % calculation -#Z112834
//
// Date : 2017-05-22 Omas
// - modified getJupasVsDseReport() - show No offer in program title
// - modified checkCompulsoryField() - #L117176
// - modified getJupasVsDseReport() - fix class, class number get from YEAR_CLASS_USER
// Date : 2017-05-02 Omas
// - modified getStudentSchoolExamAllSubjectResult() get only score > -1
// Date : 2017-04-12 Villa
// - modified getFormListByAcademicYearID
// Date : 2017-03-10 Villa
// - modified getBestStudent - support Student wthout can be shown in the result
//
// Date : 2017-03-10 Omas
// - modified checkCompulsoryField - add DSE websams checking
//
// Date : 2017-03-08 Villa
// - modified getDseYearWithRecords sorting By Term Start
//
// Date : 2017-03-03 Omas
// - added getStudentByAcademicYearIDAndForm()
//
// Date : 2017-02-23 Omas
// - modiifed checkCompulsoryField() - add JUPAS checking
//
// Date : 2017-02-15 Villa
// - getStudentsYearInfoByID() add return WEDSAMS, EnglishName
//
// Date : 2017-02-15 Villa
// - modified getJupasVsDseReport() - add class, class number, student name
//
// Date : 2016-12-15 Omas
// - modified getStudentFormYearTerm(), getStudentFormAcademicYear() - fixing if S6 year name don't have 6 cannot show any result problem #M110079
// - modified getClassListByYearName() - add a parm to ensure only return secondary classes
//
// Date : 2016-12-09 Villa
// - modified getFormListByAcademicYearID() - add return YearID
//
// Date : 2016-09-29 Omas
// - modifed getStudentsInfoByID() - support archive user
// - modified getBestStudent() - change $AcademicYearArr to $AcademicYear
//
// Date : 2016-09-27 OMas
// - modified processDseScore(), getDSEScore() - cast level to string
//
// Date : 2016-09-19 Villa #K103120
// - added getClassListByAcademicYearIDAndForm() - for display class from selected form and AcademicYearID
// - added getFormListByAcademicYearID() - for display form from selected AcademicYearID
//
// Date : 2016-09-07 Omas
// - added getStudentsYearInfoByID() - for display previous year class & classno
//
// Date : 2016-08-09 Omas
// - new reports from CEES
//
// Date : 2016-07-29 Omas
// - modified getExamData() - add skip cmpSubject
// - added getDseCompulsorySubjectID(), convertDseLevelToScore()
//
// Date : 2016-07-04 Omas
// - modified getClassListByYearName() - modified to support array and change to use REGEX instead of like
// - modified handleDseRawData() - add hard code cust for A161,A162,A163
//
// Date : 2016-06-06 Omas
// - added getPredictionGrade(), getStudentsInfoByID(), processDseScore(), getStudentSchoolExamAllSubjectResult(), getDSEScore()
// - modified getSubjectCodeAndID(), getSchoolExamData()
//
// Date : 2016-05-10 Omas
// - modified getStudentFormAcademicYear(), getStudentFormYearTerm() - fix archived student cannot retrieve PROFILE_CLASS_HISTORY Problem
//
// Date : 2016-04-13 Omas
// - modified getExamData() HKAt default output grade
// - modified getExamDataForDisplay() - sql temp use CLASS_HISTORY to get classname classno
// Date : 2016-04-08 Omas
// - modified getClassWithRecords()
// - modified getSubjectCodeAndID() - add support search by websams code
// Date : 2016-03-23 Omas
// - added getUserWebSAMSByID()
// - fixed updateExamData() - HKAT score get wrongly
// - fixed mapping wrongly to a deleted subjectID
// Date : 2016-03-15 Omas
// - added getUserNameByID()
// Date : 2016-03-03 Omas
// - adding flag $plugin['StudentDataAnalysisSystem_Style'] for HKAT
//
// ################# Change Log [End] ######
include_once ($intranet_root . '/includes/libpf-exam-config.php');
if (! defined("LIBPF_EXAM_ANALYSIS_DEFINED")) {
    define("LIBPF_EXAM_ANALYSIS_DEFINED", true);

    class libpf_exam extends libdb
    {

        var $EXAM_INFO_ARR = array();

        var $USING_EXAM_ID = '';

        var $USING_EXAM_NAME = array();

        var $DSE_MAP_WEBSAMS = array();

        var $DSE_MAP_WEBSAMS_CMP = array();

        var $DSE_W_CMP_SUBJECT = array();

        public function __construct($ExamID = '')
        {
            global $Lang, $plugin;
            global $DSE_MAP_WEBSAMS, $DSE_MAP_WEBSAMS_CMP;
            
            $this->libdb();
            
            // #############################
            // # From config file (start) ##
            // #############################
            // EXAM ID
            // define("EXAMID_HKAT", 1);
            // define("EXAMID_HKDSE", 2);
            // define("EXAMID_MOCK", 3);
            // DSE code Map to Websams code
            $this->DSE_MAP_WEBSAMS = $DSE_MAP_WEBSAMS;
            $this->DSE_MAP_WEBSAMS_CMP = $DSE_MAP_WEBSAMS_CMP;
            $this->DSE_W_CMP_SUBJECT = array_keys($this->DSE_MAP_WEBSAMS_CMP);
            // #############################
            // # From config file (end) ##
            // #############################
            
            // EXAM_INFO_ARR
            $exam_info_arr = array();
//             if ($plugin['StudentDataAnalysisSystem_Style'] == "tungwah") {
                $exam_info_arr[] = array(
                    $Lang['SDAS']['Exam']['HKAT'],
                    EXAMID_HKAT
                );
//             }
            $exam_info_arr[] = array(
                $Lang['SDAS']['Exam']['HKDSE'],
                EXAMID_HKDSE
            );
            if ($plugin['StudentDataAnalysisSystem_Style'] == "tungwah") {
                $exam_info_arr[] = array(
                    $Lang['SDAS']['Exam']['HKDSE_APPEAL'],
                    EXAMID_HKDSE_APPEAL
                );
            }
            $exam_info_arr[] = array(
                $Lang['SDAS']['Exam']['Mock'],
                EXAMID_MOCK
            );
            
            $this->EXAM_INFO_ARR = $exam_info_arr;
            if ($ExamID != '') {
                // initialize
                $this->USING_EXAM_ID = $ExamID;
            } else {
                $this->USING_EXAM_ID = '';
            }
        }

        public function getExamName()
        {
            $examID = $this->getUsingExamID();
            $tempExamNameArr = BuildMultiKeyAssoc($this->EXAM_INFO_ARR, '1', array(
                '0'
            ), 1);
            return $tempExamNameArr[$examID];
        }

        public function checkCompulsoryField($csvHeaderRowArr)
        {
            $examID = $this->getUsingExamID();
            switch ($examID) {
                case EXAMID_HKDSE_APPEAL:
                case EXAMID_HKDSE:
                    // WebSAMS format checking
                    if (count($csvHeaderRowArr) == 10) {
                        $importHeader = array(
                            'EXAMCODE',
                            'examyear',
                            'classcode',
                            'classno',
                            'regno',
                            'ENNAME',
                            'sex',
                            'Subject Code',
                            'Subject Name',
                            'Subject Grade'
                        );
                    } else {
                        $importHeader = array(
                            'EXAMCODE',
                            'examyear',
                            'classcode',
                            'classno',
                            'regno',
                            'ENNAME',
                            'sex',
                            'Subject Code',
                            'Subject Name',
                            'Subject Grade',
                            'Subject Comp Code',
                            'Subject Comp Name',
                            'Subject Comp Grade'
                        );
                    }
                    foreach ((array) $importHeader as $_fieldName) {
                        if (! in_array($_fieldName, (array) $csvHeaderRowArr)) {
                            $wrongHeader = 1;
                            break;
                        }
                    }
                    break;
                case EXAMID_HKAT:
                    if (count($csvHeaderRowArr) != 5) {
                        $wrongHeader = 1;
                    }
                    break;
                case EXAMID_MOCK:
                    $importHeader = $this->getImportCompulsoryField();
                    $numOfComField = count($importHeader);
                    for ($x = 0; $x < $numOfComField; $x ++) {
                        if (str_replace('*', '', $csvHeaderRowArr[$x]) != $importHeader[$x]) {
                            $wrongHeader = 1;
                            break;
                        }
                    }
                    break;
                case EXAMID_JUPAS:
                    $compulsoryColumnArr = array(
                        'Application No. / WebSAMS RegNo.',
                        'Offer',
                        'Institution',
                        'Funding Category',
                        'Band',
                        'Offer Round',
                        'Acceptance Status',
                        'Programme Full Title'
                    );
                    foreach ((array) $compulsoryColumnArr as $_key => $_fieldName) {
                        if ($_key == 0) {
                            if (! in_array($_fieldName, (array) $csvHeaderRowArr) && ! in_array('Application No.', (array) $csvHeaderRowArr)) {
                                $wrongHeader = 1;
                                break;
                            }
                        } elseif ($_key == 2) {
                            if (! in_array($_fieldName, (array) $csvHeaderRowArr) && ! in_array('Institution / Scheme', (array) $csvHeaderRowArr)) {
                                $wrongHeader = 1;
                                break;
                            }
                        } else {
                            if (! in_array($_fieldName, (array) $csvHeaderRowArr)) {
                                $wrongHeader = 1;
                                break;
                            }
                        }
                    }
                    break;
                case EXAMID_EXITTO:
                    
                    $compulsoryColumnArr = array(
                    'Application No. / WebSAMS RegNo.',
                    'Applicant Name', 
                    'Class',
                    'Final Choice',
                    'Remarks'
                     );
                    
                    foreach ((array) $compulsoryColumnArr as $_key => $_fieldName) {
                        if (! in_array($_fieldName, (array) $csvHeaderRowArr)) {
                            $wrongHeader = 1;
                            break;
                        }
                    }
                    break;
                // default:
                
                // break;
            }
            
            if ($wrongHeader) {
                return false;
            } else {
                return true;
            }
        }

        public function getImportCompulsoryField()
        {
            switch ($this->USING_EXAM_ID) {
                case EXAMID_MOCK:
                    $importHeader = array(
                        "School ID",
                        "School Year",
                        "School Level",
                        "School Session",
                        "Class Level",
                        "Class",
                        "Class Number",
                        "Student Name",
                        "Reg. No."
                    );
                    break;
                default:
                    $importHeader = array();
                    $importHeader[] = "ClassName";
                    $importHeader[] = "ClassNumber";
                    $importHeader[] = "WebSAMS Num";
                    break;
            }
            
            return $importHeader;
        }
        
        // getSubjectCodeAndID now responsible for both import HKAT & mock exam
        public function getSubjectCodeAndID($SubjectIDArr = array(), $WebSAMSCodeArr = array(), $parCmpOnly = false)
        {
            global $intranet_db;
            
            if (! empty($SubjectIDArr)) {
                $subjectID_conds = " AND c.RecordID IN ( '" . implode("','", (array) $SubjectIDArr) . "' )";
            }
            
            if (! empty($WebSAMSCodeArr)) {
                $websams_conds = " AND m.CODEID IN ('" . implode("','", $WebSAMSCodeArr) . "')";
            }
            if ($parCmpOnly) {
                $cmpOnly_conds = " AND (c.CMP_CODEID IS NULL or TRIM(c.CMP_CODEID) = '') ";
            }
            
            $sql = "SELECT 
							m.en_sname, 
							IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', '', c.en_sname) as code, 
							m.EN_DES as main_name, 
							c.EN_DES as component_name, 
							m.CH_DES as ch_main_name, 
							c.CH_DES as ch_component_name, 
							m.DisplayOrder, 
							IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', 0, c.DisplayOrder) as cmp_displayOrder ,
							c.RecordID as SubjectID,
							m.CODEID,
							m.EN_SNAME,
							m.CH_SNAME,
							IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', m.CH_DES, CONCAT(m.CH_DES,' - ',c.CH_DES)) as displayNameCh,
							IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', m.EN_DES, CONCAT(m.EN_DES,' - ',c.EN_DES)) as displayNameEn
						FROM 
							{$intranet_db}.ASSESSMENT_SUBJECT AS m 
							LEFT JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS c ON m.codeid = c.codeid AND c.RecordStatus = 1 
							LEFT JOIN {$intranet_db}.LEARNING_CATEGORY AS lc ON m.LearningCategoryID = lc.LearningCategoryID 
						WHERE 
							(m.CMP_CODEID IS NULL or TRIM(m.CMP_CODEID) = '') 
							AND m.RecordStatus = 1
							$subjectID_conds 
							$websams_conds
							$cmpOnly_conds
						ORDER BY 
							lc.DisplayOrder, m.DisplayOrder, cmp_displayOrder";
            return $this->returnResultSet($sql);
        }

        public function getExportHeader()
        {
            $examID = $this->getUsingExamID();
            
            if ($examID == '') {
                $header = array();
            } else {
                switch ($examID) {
                    case EXAMID_HKAT:
                        $header = array(
                            'AcademicYear',
                            'ClassName',
                            'ClassNumber',
                            'StudentName',
                            'Subject',
                            'Score'
                        );
                        break;
                    case EXAMID_MOCK:
                        $header = array(
                            'AcademicYear',
                            'ClassName',
                            'ClassNumber',
                            'StudentName',
                            'Subject',
                            'Score'
                        );
                        break;
                    case EXAMID_HKDSE:
                    case EXAMID_HKDSE_APPEAL:
                        $header = array(
                            'AcademicYear',
                            'ClassName',
                            'ClassNumber',
                            'StudentName',
                            'Subject',
                            'Grade'
                        );
                        break;
                }
            }
            return $header;
        }

        public function getImportHeader($SubjectIDArr = array())
        {
            
            // $importHeader = $this->getImportCompulsoryField();
            
            // $subjectArr = $this->getSubjectCodeAndID($SubjectIDArr);
            // foreach((array)$subjectArr as $_subjectInfo){
            // if($_subjectInfo['en_sname'] == ''){
            // // skip those empty element
            // continue;
            // }
            // if($_subjectInfo['code'] == ''){
            // // subject
            // $_subjectMappingCode = $_subjectInfo['en_sname'];
            // }else{
            // // subject component
            // $_subjectMappingCode = $_subjectInfo['en_sname'].'___'.$_subjectInfo['code'];
            // }
            // $importHeader[] = $_subjectMappingCode;
            // }
            
            // return $importHeader;
        }

        public function getClassExamResult($academicYearID, $examID, $ClassNameArr = '')
        {
            if ($ClassNameArr == '') {
                $userIDArr = '';
            } else {
                $userIDArr = $this->getStudentIDByClassNameAndYear($academicYearID, $ClassNameArr);
            }
            // return $this->getExamDataForDisplay($academicYearID , $examID, '',$userIDArr);
            return $this->getExamData($academicYearID, $examID, '', $userIDArr);
        }

        public function getWithComponentDseSubject()
        {
            return $this->DSE_W_CMP_SUBJECT;
        }

        public function getDseWebSAMSCmpSubjectMapping($ReverseMappinp = 0)
        {
            if ($ReverseMappinp) {
                return 'not ready for reverse';
                // return array_flip($this->$DSE_MAP_WEBSAMS_CMP);
            } else {
                return $this->DSE_MAP_WEBSAMS_CMP;
            }
        }

        var $CMPSUBJECTID_MAP_WEBSAMS = array();

        public function getCmpSubjectIDWebSAMSMapping($ReverseMappinp = 0)
        {
            if (empty($this->CMPSUBJECTID_MAP_WEBSAMS)) {
                $sql = "SELECT RecordID as subjectID , CODEID ,CMP_CODEID FROM ASSESSMENT_SUBJECT  WHERE CMP_CODEID is not null and CMP_CODEID != '' AND RecordStatus = 1";
                $subjectAssocArr = BuildMultiKeyAssoc($this->returnResultSet($sql), 'CMP_CODEID', array(
                    'subjectID'
                ), 1);
            } else {
                $subjectAssocArr = $this->CMPSUBJECTID_MAP_WEBSAMS;
            }
            
            if ($ReverseMappinp) {
                return array_flip($subjectAssocArr);
            } else {
                return $subjectAssocArr;
            }
        }

        public function getDseWebSAMSSubjectMapping($ReverseMappinp = 0)
        {
            if ($ReverseMappinp) {
                return array_flip($this->DSE_MAP_WEBSAMS);
            } else {
                return $this->DSE_MAP_WEBSAMS;
            }
        }

        var $SUBJECTID_MAP_WEBSAMS = array();

        public function getSubjectIDWebSAMSMapping($ReverseMappinp = 0)
        {
            if (empty($this->SUBJECTID_MAP_WEBSAMS)) {
                $sql = "SELECT RecordID as subjectID , CODEID ,CMP_CODEID FROM ASSESSMENT_SUBJECT WHERE CMP_CODEID is null or CMP_CODEID = '' AND RecordStatus = 1";
                $subjectAssocArr = BuildMultiKeyAssoc($this->returnResultSet($sql), 'CODEID', array(
                    'subjectID'
                ), 1);
            } else {
                $subjectAssocArr = $this->SUBJECTID_MAP_WEBSAMS;
            }
            
            if ($ReverseMappinp) {
                return array_flip($subjectAssocArr);
            } else {
                return $subjectAssocArr;
            }
        }

        public function getSubjectIDByDseSubjectCode($DseSubjectCode, $CmpSubjectCode = '')
        {
            if ($CmpSubjectCode == '') {
                $DseMapWebsams = $this->getDseWebSAMSSubjectMapping(1);
                $WebsamsMapSubjectID = $this->getSubjectIDWebSAMSMapping(0);
                $tempWebSams = $DseMapWebsams[$DseSubjectCode];
                $returnSubjectID = $WebsamsMapSubjectID[$tempWebSams];
            } else {
                $DseMapWebsams = $this->getDseWebSAMSCmpSubjectMapping(0);
                $WebsamsMapSubjectID = $this->getCmpSubjectIDWebSAMSMapping(0);
                $tempWebSams = $DseMapWebsams[$DseSubjectCode][$CmpSubjectCode];
                $returnSubjectID = $WebsamsMapSubjectID[$tempWebSams];
            }
            return ($returnSubjectID == '' ? '0' : $returnSubjectID);
            // return $returnSubjectID;
        }

        public function getCmpSubjectIDArr()
        {
            $sql = "SELECT RecordID as subjectID, CODEID,CMP_CODEID 
					FROM ASSESSMENT_SUBJECT 
					WHERE CMP_CODEID is not null and CMP_CODEID != '' AND RecordStatus = 1";
            return Get_Array_By_Key($this->returnResultSet($sql), 'subjectID');
        }

        public function getUsingExamID()
        {
            return $this->USING_EXAM_ID;
        }

        public function insertExamData($insertVarArr)
        {
            $examID = $this->getUsingExamID();
            
            switch ($examID) {
                case EXAMID_HKAT:
                    $sql = "INSERT INTO
						EXAM_STUDENT_SCORE
						( AcademicYearID , ExamID, SubjectID, StudentID , Score, Grade, InputDate, InputBy, ModifiedDate, ModifiedBy)
					Values
						" . implode(',', $insertVarArr) . "";
                    break;
                case EXAMID_MOCK:
                    $sql = "INSERT INTO
						EXAM_STUDENT_SCORE
						( AcademicYearID , ExamID, SubjectID, StudentID , Score, Grade, InputDate, InputBy, ModifiedDate, ModifiedBy)
					Values
						" . implode(',', $insertVarArr) . "";
                    break;
                case EXAMID_HKDSE:
                case EXAMID_HKDSE_APPEAL:
                    $sql = "INSERT INTO
								EXAM_DSE_STUDENT_SCORE
								( AcademicYearID , ExamID, 
								SubjectID, ParentSubjectID, 
								SubjectCategory, SubjectCode, ParentSubjectCode,  
								StudentID , Grade, InputDate, InputBy, ModifiedDate, ModifiedBy)
							Values
								" . implode(',', $insertVarArr) . "";
                    break;
            }
            
            $success[] = $this->db_db_query($sql);
            
            if (in_array('0', $success)) {
                return 0;
            } else {
                return $this->db_affected_rows();
            }
        }

        public function updateExamData($updateArr)
        {
            global $UserID;
            $examID = $this->getUsingExamID();
            
            foreach ((array) $updateArr as $_recordID => $_grade) {
                switch ($examID) {
                    case EXAMID_HKAT:
                        $sql = "UPDATE
								EXAM_STUDENT_SCORE
							SET
								Grade = '" . $_grade['Grade'] . "',
								Score = '" . $_grade['Score'] . "',
								ModifiedDate = now(),
								ModifiedBy = '" . $UserID . "'
							WHERE
								RecordID = '$_recordID' ";
                        break;
                    case EXAMID_HKDSE:
                        if (isset($_grade['SubjectID'])) {
                            $subject_update = " SubjectID = '" . $_grade['SubjectID'] . "', ";
                        }
                        if (isset($_grade['ParentSubjectID'])) {
                            $parent_update = " ParentSubjectID = '" . $_grade['ParentSubjectID'] . "', ";
                        }
                        $sql = "UPDATE
								EXAM_DSE_STUDENT_SCORE
							SET
								$subject_update
								$parent_update
								Grade = '" . $_grade['Grade'] . "',
								ModifiedDate = now(),
								ModifiedBy = '" . $UserID . "'
							WHERE
								RecordID = '$_recordID' ";
                        break;
                    case EXAMID_HKDSE_APPEAL:
                        if (isset($_grade['SubjectID'])) {
                            $subject_update = " SubjectID = '" . $_grade['SubjectID'] . "', ";
                        }
                        if (isset($_grade['ParentSubjectID'])) {
                            $parent_update = " ParentSubjectID = '" . $_grade['ParentSubjectID'] . "', ";
                        }
                        $sql = "UPDATE
						EXAM_DSEAPPEAL_STUDENT_SCORE 
						SET
						$subject_update
						$parent_update
						Grade = '" . $_grade['Grade'] . "',
								ModifiedDate = now(),
								ModifiedBy = '" . $UserID . "'
								WHERE
								RecordID = '$_recordID' ";
                        break;
                    case EXAMID_MOCK:
                        $sql = "UPDATE
								EXAM_STUDENT_SCORE
							SET
								Grade = '" . $_grade['Grade'] . "',
								Score = '" . $_grade['Score'] . "',
								ModifiedDate = now(),
								ModifiedBy = '" . $UserID . "'
							WHERE
								RecordID = '$_recordID' ";
                        break;
                }
                $success[] = $this->db_db_query($sql);
            }
            return $success;
        }

        public function rollBackDseAppeal($parAcademicYearID)
        {
            global $intranet_db;
            if ($parAcademicYearID != '') {
                // delete records in EXAM_STUDENT_SCORE
                $sql = "DELETE FROM {$intranet_db}.EXAM_DSE_STUDENT_SCORE where ExamID = '7' AND AcademicYearID = '$parAcademicYearID'";
                $this->db_db_query($sql);
                
                $sql = "UPDATE {$intranet_db}.EXAM_DSE_STUDENT_SCORE SET isArchive = '0' WHERE ExamID = '2' AND AcademicYearID = '$parAcademicYearID' and isArchive = '1'";
                $this->db_db_query($sql);
            }
        }

        public function deleteExamData($examID, $academicYearID, $targetUserIDArr = array(), $excludeUserIDArr = array())
        {
            global $intranet_db;
            
            if ($examID == $this->getUsingExamID()) {
                if (count($excludeUserID) > 0) {
                    $notInArchived_conds = " AND StudentID not IN('" . implode("','", $excludeUserIDArr) . "') ";
                }
                if (count($targetUserIDArr) > 0) {
                    $include_conds = " AND StudentID IN('" . implode("','", $targetUserIDArr) . "') ";
                }
                
                $tableName = '';
                if ($examID == EXAMID_HKDSE || $examID == EXAMID_HKDSE_APPEAL) {
                    $tableName = 'EXAM_DSE_STUDENT_SCORE';
                } else {
                    $tableName = 'EXAM_STUDENT_SCORE';
                }
                
                $totalNum = 0;
                if ($tableName != '') {
                    // delete records in EXAM_STUDENT_SCORE
                    $sql = "DELETE FROM {$intranet_db}.$tableName where ExamID = '$examID' AND AcademicYearID = '$academicYearID' $include_conds $notInArchived_conds";
                    $success = $this->db_db_query($sql);
                    $totalNum = $this->db_affected_rows();
                }
            }
            if ($examID == EXAMID_HKDSE_APPEAL) {
                $this->rollBackDseAppeal($academicYearID);
            }
            return $totalNum;
        }

        public function getUserWebSAMSByID($UserIDArr)
        {
            global $intranet_db;
            
            if (! empty($UserIDArr)) {
                // including archived user
                $sql = "SELECT UserID, WebSAMSRegNo FROM {$intranet_db}.INTRANET_USER WHERE USERID iN ('" . implode("','", (array) $UserIDArr) . "'); ";
                $result['currentUser'] = BuildMultiKeyAssoc($this->returnResultSet($sql), 'UserID', array(
                    'WebSAMSRegNo'
                ), 1);
                
                if (count($UserIDArr) != count($result['currentUser'])) {
                    $sql = "SELECT UserID, WebSAMSRegNo FROM {$intranet_db}.INTRANET_ARCHIVE_USER WHERE USERID iN ('" . implode("','", (array) $UserIDArr) . "'); ";
                    $result['archivedUser'] = BuildMultiKeyAssoc($this->returnResultSet($sql), 'UserID', array(
                        'WebSAMSRegNo'
                    ), 1);
                }
            }
            return $result;
        }

        public function getStudentsYearInfoByID($UserIDArr, $academicYearID = '')
        {
            $academicYearID = ($academicYearID == '') ? Get_Current_Academic_Year_ID() : $academicYearID;
            
            $userIdSql = implode("','", (array) $UserIDArr);
            $result = array();
            
            #### Get User START ####
            $Name_field = getNameFieldByLang2('iu.');
            $Name_field2 = getNameFieldByLang2('iau.');
            $sql = "SELECT
		        IF(iu.UserID, iu.UserID, iau.UserID) as UserID, 
		        yc.ClassTitleEN as ClassName, 
		        ycu.ClassNumber, 
		        IF(iu.UserID, $Name_field, $Name_field2) as Name,
		        IF(iu.UserID, iu.WebSAMSRegNo, iau.WebSAMSRegNo) as WebSAMSRegNo,
		        IF(iu.UserID, iu.EnglishName, iau.EnglishName) as EnglishName,
		        IF(iu.UserID, iu.Gender, iau.Gender) as Gender
		    FROM
		        {$intranet_db}.YEAR_CLASS as yc
		    INNER JOIN 
		        {$intranet_db}.YEAR_CLASS_USER as ycu 
	        ON
	            yc.YearClassID = ycu.YearClassID
		    LEFT JOIN 
		        {$intranet_db}.INTRANET_USER as iu 
	        ON
	            iu.UserID = ycu.UserID
		    LEFT JOIN 
		        {$intranet_db}.INTRANET_ARCHIVE_USER as iau 
	        ON
	            iau.UserID = ycu.UserID
		    WHERE
		        yc.AcademicYearID = '{$academicYearID}'
            AND
	            (
		            iu.USERID iN ('{$userIdSql}')
		        OR
		            iau.USERID iN ('{$userIdSql}')
                )
			ORDER BY 
		          ClassName, ycu.ClassNumber";
            $result = $this->returnResultSet($sql);
            #### Get User END ####
            
            return $result;
        }

        public function getStudentsInfoByID($UserIDArr, $AcademicYear = '')
        {
            $Name_field = Get_Lang_Selection('IF(ChineseName !=\'\' AND ChineseName is not null, ChineseName, EnglishName) as Name', 'EnglishName as Name');
            if ($AcademicYear == '') {
                $sql = "SELECT UserID, ClassName, ClassNumber, $Name_field 
						FROM {$intranet_db}.INTRANET_USER
						WHERE USERID iN ('" . implode("','", (array) $UserIDArr) . "') 
						ORDER BY ClassName, ClassNumber; ";
            } else {
                $sql = "SELECT YearClassID from YEAR_CLASS where AcademicYearID = '" . $AcademicYear . "'";
                $yearClassIdArr = $this->returnVector($sql);
                
                $sql = "SELECT iu.UserID, yc.ClassTitleEN as ClassName,  if(yc.ClassTitleEN is not null, ycu.ClassNumber, NULL) as ClassNumber, $Name_field
						FROM {$intranet_db}.INTRANET_USER as iu
						LEFT JOIN YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID AND YearClassID IN ('" . implode("','", (array) $yearClassIdArr) . "') )
						LEFT JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '" . $AcademicYear . "')
						WHERE iu.USERID iN ('" . implode("','", (array) $UserIDArr) . "')
						ORDER BY yc.ClassTitleEN, ycu.ClassNumber; ";
            }
            $result = $this->returnResultSet($sql);
            
            if (count($result) < count($UserIDArr)) {
                if ($AcademicYear == '') {
                    $sql = "SELECT UserID, ClassName, ClassNumber, $Name_field
							FROM {$intranet_db}.INTRANET_ARCHIVE_USER
							WHERE USERID iN ('" . implode("','", (array) $UserIDArr) . "')
							ORDER BY ClassName, ClassNumber; ";
                } else {
                    $sql = "SELECT YearClassID from YEAR_CLASS where AcademicYearID = '" . $AcademicYear . "'";
                    $yearClassIdArr = $this->returnVector($sql);
                    
                    $sql = "SELECT iu.UserID, yc.ClassTitleEN as ClassName,  if(yc.ClassTitleEN is not null, ycu.ClassNumber, NULL) as ClassNumber, $Name_field
						FROM {$intranet_db}.INTRANET_ARCHIVE_USER as iu
						LEFT JOIN YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID AND YearClassID IN ('" . implode("','", (array) $yearClassIdArr) . "') )
						LEFT JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '" . $AcademicYear . "')
						WHERE iu.USERID iN ('" . implode("','", (array) $UserIDArr) . "')
						ORDER BY yc.ClassTitleEN, ycu.ClassNumber; ";
                }
                $archive_result = $this->returnResultSet($sql);
            }
            
            return array_merge((array) $result, (array) $archive_result);
        }

        public function getUserNameByID($UserIDArr)
        {
            global $intranet_db;
            
            if (! empty($UserIDArr)) {
                // including archived user
                $Name_field = Get_Lang_Selection('IF(ChineseName !=\'\' AND ChineseName is not null, ChineseName, EnglishName) as Name', 'EnglishName as Name');
                $sql = "SELECT UserID, $Name_field FROM {$intranet_db}.INTRANET_USER WHERE USERID iN ('" . implode("','", (array) $UserIDArr) . "'); ";
                $result['currentUser'] = BuildMultiKeyAssoc($this->returnResultSet($sql), 'UserID', array(
                    'Name'
                ), 1);
                
                if (count($UserIDArr) != count($result['currentUser'])) {
                    $sql = "SELECT UserID, $Name_field FROM {$intranet_db}.INTRANET_ARCHIVE_USER WHERE USERID iN ('" . implode("','", (array) $UserIDArr) . "'); ";
                    $result['archivedUser'] = BuildMultiKeyAssoc($this->returnResultSet($sql), 'UserID', array(
                        'Name'
                    ), 1);
                }
            }
            return $result;
        }

        /* Dont use this any more... now only for export will */
        public function getExamDataForDisplay($academicYearID, $examID = '', $subjectID = '', $UserID = '')
        {
            global $intranet_db;
            
            if ($examID == '') {
                // $examID = $this->USING_EXAM_ID;
                $examID = $this->getUsingExamID();
            }
            
            if ($examID != '') {
                $exam_conds = " AND ess.ExamID = '" . $examID . "' ";
            }
            if ($academicYearID != '') {
                $academicYear_conds = " AND ess.AcademicYearID IN ('" . implode("','", (array) $academicYearID) . "') ";
            }
            if ($subjectID != '') {
                $subject_conds = " AND ess.SubjectID IN ('" . implode("','", (array) $subjectID) . "') ";
            }
            if ($UserID != '') {
                $student_conds = " AND ess.StudentID IN ('" . implode("','", (array) $UserID) . "') ";
            }
            
            if ($examID == EXAMID_HKDSE || EXAMID_HKDSE_APPEAL) {
                $tableName = 'EXAM_DSE_STUDENT_SCORE';
                $fieldName = ' ess.Grade';
                $dse_conds = " AND ess.isArchive = 0 ";
                if ($examID == EXAMID_HKDSE) {
                    $exam_conds = "";
                }
            } else {
                $tableName = 'EXAM_STUDENT_SCORE';
                $fieldName = ' ess.Score, ess.Grade';
            }
            
            $sql = "SELECT
						ess.RecordID,
						ess.AcademicYearID,
						ess.SubjectID,
						ess.StudentID,
						/*yc.ClassTitleEN as ClassName,
						ycu.ClassNumber as ClassNumber,*/
						pch.ClassName as ClassName,
						pch.ClassNumber as ClassNumber,
						ay.YearNameEN as Year,
						IF(SUBJ.CMP_CODEID is null OR SUBJ.CMP_CODEID =''  , SUBJ.EN_DES, CONCAT(CMP.EN_DES,' - ',SUBJ.EN_DES)) as SubjectNameEng,
						IF(SUBJ.CMP_CODEID is null OR SUBJ.CMP_CODEID ='', SUBJ.CH_DES, CONCAT(CMP.CH_DES,' - ',SUBJ.CH_DES)) as SubjectNameChi,
						$fieldName
					FROM
						{$intranet_db}.$tableName as ess
						/*LEFT JOIN {$intranet_db}.YEAR_CLASS_USER ycu on (ess.StudentID = ycu.UserID) 
						LEFT JOIN {$intranet_db}.YEAR_CLASS as yc on (ycu.YearClassID = yc.YearClassID AND ess.AcademicYearID = yc.AcademicYearID)*/
						LEFT JOIN {$intranet_db}.PROFILE_CLASS_HISTORY  AS pch on (ess.StudentID = pch.UserID and ess.AcademicYearID = pch.AcademicYearID)
						INNER JOIN {$intranet_db}.ACADEMIC_YEAR as ay on (ess.AcademicYearID = ay.AcademicYearID)
						INNER JOIN {$intranet_db}.ASSESSMENT_SUBJECT as SUBJ on (ess.SubjectID = SUBJ.RecordID AND SUBJ.RecordStatus = 1)
						LEFT JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS CMP ON (   (CMP.CMP_CODEID ='' or CMP.CMP_CODEID is null) AND SUBJ.CODEID = CMP.CODEID And CMP.RecordStatus = 1)
					WHERE
						1
						$exam_conds
						$academicYear_conds
						$subject_conds
						$student_conds
						$dse_conds
					ORDER BY
						ClassName, ClassNumber, StudentID, CMP.DisplayOrder";
            return $this->returnResultSet($sql);
        }

        public function getSchoolExamOverallData($academicYearID, $termCondition = '', $assessmentCondition = '', $userIdArr = '')
        {
            global $eclass_db, $intranet_db;
            
            if ($termCondition != '') {
                $termConds = " AND a.YearTermID = '$termCondition'";
            } else {
                if ($assessmentCondition == '' || $assessmentCondition == 'NULL') {
                    $termConds = " AND a.IsAnnual = 1 AND (a.YearTermID = 0 or a.YearTermID is null) ";
                } else {
                    $termConds = " AND a.IsAnnual = 0";
                }
            }
            
            if ($assessmentCondition != '') {
                if ($assessmentCondition == 'NULL') {
                    $assessment_conds = " AND a.TermAssessment IS NULL ";
                } else {
                    $assessment_conds = " AND a.TermAssessment = '$assessmentCondition'";
                }
            }
            
            if ($userIdArr != '') {
                $user_conds = " AND a.UserID IN ('" . implode("','", (array) $userIdArr) . "')";
            }
            
            $sql = "SELECT DISTINCT
						a.UserID as StudentID,
						a.Score,
						a.Grade,
						a.OrderMeritClass,
						a.OrderMeritClassTotal,
						a.OrderMeritForm,
						a.OrderMeritFormTotal,
						IF(y.WEBSAMSCode = 'NA', y.YearName, y.WEBSAMSCode) as YearName
					FROM
						{$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD as a
						left join {$intranet_db}.YEAR_CLASS as yc on yc.YearClassID = a.YearClassID
						left join  {$intranet_db}.YEAR as y on y.YearID = yc.YearID
					WHERE
						a.AcademicYearID = '$academicYearID'
						$termConds
						$assessment_conds
						$user_conds
						";
            return $this->returnResultSet($sql);
        }

        public function getSchoolExamData($academicYearID, $mainSubjectOnly = false, $termCondition = '', $assessmentCondition = '', $userIdArr = '')
        {
            global $eclass_db, $intranet_db;
            
            if ($termCondition != '') {
                $termConds = " AND a.YearTermID = '$termCondition'";
            } else {
                if ($assessmentCondition == '' || $assessmentCondition == 'NULL') {
                    $termConds = " AND a.IsAnnual = 1 AND (a.YearTermID = 0 or a.YearTermID is null) ";
                } else {
                    $termConds = " AND a.IsAnnual = 0";
                }
            }
            
            if ($assessmentCondition != '') {
                if ($assessmentCondition == 'NULL') {
                    $assessment_conds = " AND a.TermAssessment IS NULL ";
                } else {
                    $assessment_conds = " AND a.TermAssessment = '$assessmentCondition'";
                }
            }
            
            if ($mainSubjectOnly) {
                $mainOnly_conds = " AND (a.SubjectComponentID IS NULL OR a.SubjectComponentID = '')";
            }
            
            if ($userIdArr != '') {
                $user_conds = " AND a.UserID IN ('" . implode("','", (array) $userIdArr) . "')";
            }
            
            $sql = "SELECT DISTINCT
						a.UserID as StudentID,
						a.SubjectCode,
						/*a.SubjectID,
						a.SubjectComponentID,*/
						IF(a.SubjectComponentID is not null, a.SubjectComponentID, a.SubjectID ) as SubjectID,
						a.SubjectComponentCode,
						a.Score,
						a.Grade,
						a.OrderMeritClass,
						a.OrderMeritClassTotal,
						a.OrderMeritForm,
						a.OrderMeritFormTotal,
						IF(y.WEBSAMSCode = 'NA', y.YearName, y.WEBSAMSCode) as YearName,
                        yc.YearID
					FROM
						{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as a
						left join {$intranet_db}.YEAR_CLASS as yc on yc.YearClassID = a.YearClassID
						left join  {$intranet_db}.YEAR as y on y.YearID = yc.YearID
					WHERE
						a.AcademicYearID = '$academicYearID'
						$termConds
						$assessment_conds
						$mainOnly_conds
						$user_conds";
            return $this->returnResultSet($sql);
        }

        public function removeExistingAppealRecord($parAcademicYearID, $parUserID, $parSubjectCode, $parCmpSbjCode = '')
        {
            if ($parCmpSbjCode == '') {
                $subject_conds = " AND SubjectCode = '$parSubjectCode' ";
            } else {
                $subject_conds = " AND SubjectCode = '$parCmpSbjCode' AND ParentSubjectCode = '$parSubjectCode'";
            }
            
            $sql = "SELECT COUNT(*) FROM EXAM_DSE_STUDENT_SCORE WHERE ExamID = '" . EXAMID_HKDSE_APPEAL . "' AND AcademicYearID = '$parAcademicYearID' $subject_conds AND StudentID = '$parUserID' AND isArchive = 0";
            $appealCount = $this->returnVector($sql);
            if ($appealCount[0] > 0) {
                $sql = "DELETE FROM EXAM_DSE_STUDENT_SCORE  WHERE ExamID = '" . EXAMID_HKDSE_APPEAL . "' AND AcademicYearID = '$parAcademicYearID' AND StudentID = '$parUserID' $subject_conds AND isArchive = 0";
                $this->db_db_query($sql);
            }
        }

        public function getExamData($academicYearID, $examID = '', $subjectID = '', $UserID = '', $excludeCmp = false, $excludeUnmatchSubject = false , $getPrimarySchoolCode = false, $getAllDSECatSubject = false)
        {
            global $intranet_db;
            
            if ($examID == '') {
                // $examID = $this->USING_EXAM_ID;
                $examID = $this->getUsingExamID();
            }
            // [2019-0808-1001-54235]
            $getAllDSECatSubject = $getAllDSECatSubject || $examID == EXAMID_HKDSE_APPEAL;

            if ($academicYearID != '') {
                $academicYear_conds = " AND AcademicYearID IN ('" . implode("','", (array) $academicYearID) . "') ";
            }
            if ($examID != '') {
                // $exam_conds = " AND ExamID IN ('".implode("','",(array)$examID)."') ";
                $exam_conds = " AND ExamID = '" . $examID . "' ";
            }
            if ($subjectID != '') {
                $subject_conds = " AND SubjectID IN ('" . implode("','", (array) $subjectID) . "') ";
            }
            if ($UserID != '') {
                $student_conds = " AND StudentID IN ('" . implode("','", (array) $UserID) . "') ";
            }
            if ($excludeCmp) {
                $cmpSubjectIDArr = $this->getCmpSubjectIDArr();
                $exclude_conds = " AND SubjectID NOT IN ('" . implode("','", (array) $cmpSubjectIDArr) . "') ";
            }
            if ($excludeUnmatchSubject) {
                $excludeUnmatchSubject_cond = " AND SubjectID != '-999' ";
            }
            $OrderBy = '';
            switch ($examID) {
                case EXAMID_HKAT:
                    $tableName = 'EXAM_STUDENT_SCORE';
                    $field = 'Score ,Grade ';
                    if($getPrimarySchoolCode){
                        $tableName = 'EXAM_STUDENT_SCORE 
                        LEFT JOIN INTRANET_USER AS iu ON (iu.UserID = StudentID)';
                        $field = 'EXAM_STUDENT_SCORE.Score ,EXAM_STUDENT_SCORE.Grade, iu.PrimarySchoolCode,iu.EnglishName, iu.ChineseName';
                    }
                    
                    break;
                case EXAMID_MOCK:
                    $tableName = 'EXAM_STUDENT_SCORE';
                    $field = 'Score  ,Grade ';
                    $mock_exclude_conds = " AND Score > -1";
                    break;
                case EXAMID_HKDSE_APPEAL:
                case EXAMID_HKDSE:
                    $tableName = 'EXAM_DSE_STUDENT_SCORE';
                    $field = 'Grade as Score  , SubjectCode , ParentSubjectCode, IF( ParentSubjectCode = "", SubjectCode, CONCAT(ParentSubjectCode, "-",SubjectCode ) ) as realSubjectCode, isArchive, ExamID';
                    // [2019-0808-1001-54235]
                    if ($getAllDSECatSubject) {
                        // do nothing
                    }
                    else {
                        $catA_only_conds = " AND SubjectCategory = 'A' ";
                    }
                    if ($excludeCmp) {
                        $dse_exclude_conds = " AND (ParentSubjectCode is Null or ParentSubjectCode = '') ";
                    }
                    $dse_conds = " AND isArchive = 0 ";
                    if ($examID == EXAMID_HKDSE) {
                        $exam_conds = "";
                    }
                    break;
            }
            
            $sql = "SELECT
						RecordID,
						AcademicYearID,
						SubjectID,
						StudentID,
						$field
					FROM
						{$intranet_db}.$tableName
					WHERE
						1
						$exam_conds 
						$academicYear_conds
						$subject_conds
						$student_conds
						$exclude_conds
						$excludeUnmatchSubject_cond
						$catA_only_conds
						$dse_exclude_conds
						$dse_conds
						$mock_exclude_conds";
            
            return $this->returnResultSet($sql);
        }

        public function getClassListByAcademicYearID($academicYearID = '')
        {
            // only search current year classname
            if ($academicYearID == '') {
                $academicYearID = Get_Current_Academic_Year_ID();
            }
            
            $sql = "Select YearClassID, ClassTitleEN, ClassTitleB5 From YEAR_CLASS as yc inner join YEAR as y ON yc.YearID = y.YearID  where AcademicYearID = $academicYearID";
            
            return $this->returnResultSet($sql);
        }

        public function getFormListByAcademicYearID($academicYearID = '', $WEBSAMSCode = '')
        {
            // only search current year classname
            if ($academicYearID == '') {
                $academicYearID = Get_Current_Academic_Year_ID();
            }
            $cond = " AND y.WEBSAMSCode in ('S4', 'S5', 'S6', 'F4', 'F5', 'F6')"; // $plugin['StudentRegistry']
            if ($WEBSAMSCode) {
                $cond .= " AND y.WEBSAMSCode in $WEBSAMSCode"; // filter Class Teacher
            }
            $sql = "Select y.YearID, y.Yearname From YEAR_CLASS as yc inner join YEAR as y ON yc.YearID = y.YearID  where AcademicYearID = $academicYearID  $cond Group by yc.YearID";
            return $this->returnResultSet($sql);
        }

        public function getStudentByAcademicYearIDAndForm($academicYearID = '', $Form = '')
        {
            $sql = "Select ycu.UserID
			From
			YEAR_CLASS_USER as ycu
			inner join YEAR_CLASS as yc ON yc.YearClassID = ycu.YearClassID
			inner join YEAR as y ON yc.YearID = y.YearID
			where AcademicYearID = $academicYearID and y.YearName = '" . "$Form" . "'";
            return $this->returnResultSet($sql);
        }

        public function getClassListByAcademicYearIDAndForm($academicYearID = '', $Form = '')
        {
            // only search current year classname
            $sql = "Select yc.YearClassID, yc.ClassTitleEN, yc.ClassTitleB5, y.WEBSAMSCode, y.YearID From YEAR_CLASS as yc inner join YEAR as y ON yc.YearID = y.YearID  where AcademicYearID = $academicYearID and y.YearName = '" . "$Form" . "'";
            return $this->returnResultSet($sql);
        }

        public function getClassListByAcademicYearIDAndYearID($academicYearID = '', $yearID = '')
        {
            // only search current year classname
            $sql = "Select yc.YearClassID, yc.ClassTitleEN, yc.ClassTitleB5, y.WEBSAMSCode From YEAR_CLASS as yc inner join YEAR as y ON yc.YearID = y.YearID  where AcademicYearID = $academicYearID and y.YearID = '" . "$yearID" . "'";
            return $this->returnResultSet($sql);
        }

        public function getClassListByYearName($stringArr, $onlySecondaryClass = false ,$AcademicYearArr='')
        {
            if(empty($AcademicYearArr)){
                // only search current year classnameYEAR_CLASS
                $academicYearID = Get_Current_Academic_Year_ID();
               
            }else{
                $academicYearID = implode('\',\'',$AcademicYearArr);
            }
            $academicYearConds = "AND  AcademicYearID IN  ('$academicYearID') ";
           
            
            if ($onlySecondaryClass) {
                if (is_array($stringArr)) {
                    foreach ((array) $stringArr as $key => $val) {
                        $stringArr[$key] = 'S' . $val;
                    }
                } else {
                    $stringArr = 'S' . $stringArr;
                }
            }
            
            // $sql = "Select YearClassID, ClassTitleEN, ClassTitleB5 From YEAR_CLASS as yc inner join YEAR as y ON yc.YearID = y.YearID where y.YearName like '%$string%' and AcademicYearID = $academicYearID";
            $sql = "Select YearClassID, ClassTitleEN, ClassTitleB5, y.WEBSAMSCode as formName
                    From YEAR_CLASS as yc 
                    inner join YEAR as y ON yc.YearID = y.YearID  
                    where y.WEBSAMSCode REGEXP '" . implode('|', (array) $stringArr) . "' 
                    $academicYearConds
                    order By y.WEBSAMSCode, ClassTitleEN";
            
            return $this->returnResultSet($sql);
        }

        public function getStudentIDByClassNameAndYear($academicYearIDArr, $classNameArr)
        {
            $academicYearIDArr = (array) $academicYearIDArr;
            
            $sql = "Select YearClassID From YEAR_CLASS where ClassTitleEN IN ('" . implode("','", (array) $classNameArr) . "') and AcademicYearID IN ('" . implode("','", (array) $academicYearIDArr) . "')";
            
            $classIDArr = $this->returnVector($sql);
            $sql = "Select UserID from YEAR_CLASS_USER  where YearClassID IN ('" . implode("','", (array) $classIDArr) . "')";
            
            return $this->returnVector($sql);
        }

        public function getStudentFormAcademicYear($formString, $StudentIDAry = array())
        {
            // form string - e.g F.4 = 4, F.5 = 5 , etc~
            // $sql = "Select * from (Select pch.UserID, pch.AcademicYearID
            // From PROFILE_CLASS_HISTORY as pch
            // where
            // pch.userid IN ('".implode("','",(array)$StudentIDAry)."')
            // AND pch.ClassName REGEXP '".$formString."' ORDER BY pch.AcademicYear desc ) AS tmptable
            // Group By tmptable.UserID";
            // $sql = "SELECT
            // pch.UserID, pch.AcademicYearID
            // FROM
            // PROFILE_CLASS_HISTORY as pch
            // WHERE
            // pch.userid IN ('".implode("','",(array)$StudentIDAry)."')
            // AND pch.ClassName REGEXP '".$formString."'
            // ORDER BY UserID, AcademicYear asc";
            if (! empty($StudentIDAry)) {
                $user_conds = " AND ycu.UserID IN ('" . implode("','", (array) $StudentIDAry) . "')";
            }
            $sql = "SELECT
						ycu.UserID, yc.AcademicYearID
					FROM
						YEAR_CLASS_USER as ycu
						inner join YEAR_CLASS as yc ON ycu.YearClassID = yc.YearClassID
						inner join YEAR as y on y.YearID = yc.YearID
					WHERE
						/*y.YearName REGEXP '" . $formString . "'*/
						y.WEBSAMSCode = 'S" . $formString . "'
						$user_conds
					ORDER BY ycu.UserID, ycu.DateModified asc";
            
            // one user only return one year !
            $result = $this->returnResultSet($sql);
            
            // one user only return one year , lastest yeartermID will be returned !!!
            $final = BuildMultiKeyAssoc($result, 'UserID', array(
                'UserID',
                'AcademicYearID'
            ));
            
            return $final;
        }
        
        
        public function getStudentFormAcademicYearWithSubjectRecord($formString, $StudentIDAry = array())
        {
        	# 20200930 - (Philips) - Choose Academic Year with subject record first
        	global $eclass_db;
        	if (! empty($StudentIDAry)) {
        		$user_conds = " AND ycu.UserID IN ('" . implode("','", (array) $StudentIDAry) . "')";
        	}
        	$sql = "SELECT
        	ycu.UserID, yc.AcademicYearID
        	FROM
        	YEAR_CLASS_USER as ycu
        	inner join YEAR_CLASS as yc ON ycu.YearClassID = yc.YearClassID
        	inner join YEAR as y on y.YearID = yc.YearID
        	left outer join {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr
        	ON ycu.UserID = assr.UserID AND assr.AcademicYearID = yc.AcademicYearID AND assr.YearTermID IS NULL
        	WHERE
        	/*y.YearName REGEXP '" . $formString . "'*/
						y.WEBSAMSCode = 'S" . $formString . "'
						$user_conds
						GROUP BY ycu.UserID, yc.AcademicYearID
						ORDER BY ycu.UserID, assr.AcademicYearID asc, ycu.DateModified asc";
						
						// one user only return one year !
						$result = $this->returnResultSet($sql);
						
						// one user only return one year , lastest yeartermID will be returned !!!
						$final = BuildMultiKeyAssoc($result, 'UserID', array(
								'UserID',
								'AcademicYearID'
						));
						
						return $final;
        }

        public function getRelatedTermByYearTermID($YearTermID)
        {
            // get all 1st term/ 2nd term
            $sql = "Select AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE yeartermID = '$YearTermID'";
            $result = $this->returnVector($sql);
            $tempAYID = $result[0];
            $sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$tempAYID' ORDER BY TermStart";
            $yTermID_To_TermNum = array_flip($this->returnVector($sql));
            
            $targetTermNum = $yTermID_To_TermNum[$YearTermID];
            
            $sql = "SELECT AcademicYearID,YearTermID FROM ACADEMIC_YEAR_TERM ORDER BY TermStart";
            $aYearArr = $this->returnResultSet($sql);
            $aYearAssocArr = array();
            foreach ((array) $aYearArr as $_aYearArr) {
                $_YearTermID = $_aYearArr['YearTermID'];
                $_AcademicYearID = $_aYearArr['AcademicYearID'];
                $aYearAssocArr[$_AcademicYearID][] = $_YearTermID;
            }
            
            $termIDArr = array();
            foreach ((array) $aYearAssocArr as $_aYearID => $_termArr) {
                foreach ((array) $_termArr as $__key => $__termID) {
                    if ($__key == $targetTermNum) {
                        $termIDArr[] = $__termID;
                    }
                }
            }
            return $termIDArr;
        }

        public function getStudentFormYearTerm($formString, $StudentIDAry = array(), $YearTermID)
        {
            // form string - e.g F.4 = 4, F.5 = 5 , etc~
            $termIDArr = $this->getRelatedTermByYearTermID($YearTermID);
            if (! empty($StudentIDAry)) {
                $user_conds = " AND ycu.UserID IN ('" . implode("','", (array) $StudentIDAry) . "')";
            }
            // $sql = "SELECT
            // pch.UserID, ayt.YearTermID
            // FROM
            // PROFILE_CLASS_HISTORY as pch
            // INNER JOIN ACADEMIC_YEAR_TERM as ayt on pch.AcademicYearID = ayt.AcademicYearID
            // WHERE
            // pch.userid IN ('".implode("','",(array)$StudentIDAry)."')
            // AND pch.ClassName REGEXP '".$formString."'
            // AND ayt.YearTermID IN ('".implode("','", (array)$termIDArr )."')
            // ORDER BY
            // pch.UserID, ayt.TermStart asc
            // ";
            $sql = "SELECT
						ycu.UserID, ayt.YearTermID
					FROM
						YEAR_CLASS_USER as ycu
						inner join YEAR_CLASS as yc ON ycu.YearClassID = yc.YearClassID
						inner join YEAR as y on y.YearID = yc.YearID
						INNER JOIN ACADEMIC_YEAR_TERM as ayt on yc.AcademicYearID = ayt.AcademicYearID
					WHERE
						/*y.YearName REGEXP '" . $formString . "'*/
						y.WEBSAMSCode = 'S" . $formString . "'
						AND ayt.YearTermID IN ('" . implode("','", (array) $termIDArr) . "')
						$user_conds 
					ORDER BY ycu.UserID, ayt.TermStart asc";
            $result = $this->returnResultSet($sql);
            
            // one user only return one year , lastest yeartermID will be returned !!!
            $final = BuildMultiKeyAssoc($result, 'UserID', array(
                'UserID',
                'YearTermID'
            ));
            
            return $result;
        }

        var $IsCmpArr = array();

        public function checkIsCmptSubject($subjectID)
        {
            global $intranet_db;
            
            if (isset($this->IsCmpArr[$subjectID])) {
                return $this->IsCmpArr[$subjectID];
            } else {
                
                $sql = "SELECT * FROM {$intranet_db}.ASSESSMENT_SUBJECT WHERE RecordID = '$subjectID' AND (CMP_CODEID is not null  AND CMP_CODEID != '') AND RecordStatus = 1 ";
                $result = $this->returnArray($sql);
                
                $IsCmp = (count($result) > 0) ? true : false;
                $this->IsCmpArr[$subjectID] = $IsCmp;
                
                return $IsCmp;
            }
            // if(count($result) > 0){
            // return true;
            // }
            // else{
            // return false;
            // }
        }

        public function getStudentSchoolExamAllSubjectResult($targetForm, $StudentIDArr = array(), $YearTermID = '')
        {
            global $eclass_db;
            
            if ($YearTermID == '') {
                // Whole Year Result
            	$academicYearStudentArr = $this->getStudentFormAcademicYearWithSubjectRecord($targetForm, $StudentIDArr);
                
                $aysAssoArr = array();
                foreach ((array) $academicYearStudentArr as $_infoArr) {
                    $_UserID = $_infoArr['UserID'];
                    $_AcademicYearID = $_infoArr['AcademicYearID'];
                    $aysAssoArr[$_AcademicYearID][] = $_UserID;
                }
                $SQL_conds_Arr = array();
                foreach ((array) $aysAssoArr as $_academicYearID => $_userIDArr) {
                    $SQL_conds_Arr[] = "( AcademicYearID = '$_academicYearID' AND UserID IN ('" . implode("','", $_userIDArr) . "') )";
                }
                $year_user_conds = "( " . implode(" OR ", (array) $SQL_conds_Arr) . " )";
                
                $sql = "SELECT
				/*AcademicYearID,*/ UserID as StudentID, Score, SubjectID, OrderMeritForm, OrderMeritFormTotal
				FROM
				{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
				WHERE
				IsAnnual = 1 AND( YearTermID IS NULL OR YEARTERMID = '' )
				AND $year_user_conds
				AND (SubjectComponentCode IS NULL OR SubjectComponentCode = '') 
				AND Score > -1";
                $result = $this->returnResultSet($sql);
            } else {
                $underlinePosition = strpos($YearTermID, '_');
                $isTermAssessment = is_int($underlinePosition) && $underlinePosition > 0;
                $termAssessment = '';
                if ($isTermAssessment) {
                    $termAssessmentInfoArr = explode('_', $YearTermID);
                    $YearTermID = $termAssessmentInfoArr[0];
                    $termAssessment = $termAssessmentInfoArr[1];
                }
                
                // Term Result
                $academicYearStudentArr = $this->getStudentFormYearTerm($targetForm, $StudentIDArr, $YearTermID);
                
                $aysAssoArr = array();
                foreach ((array) $academicYearStudentArr as $_infoArr) {
                    $_UserID = $_infoArr['UserID'];
                    $_YearTermID = $_infoArr['YearTermID'];
                    $aysAssoArr[$_YearTermID][] = $_UserID;
                }
                $SQL_conds_Arr = array();
                foreach ((array) $aysAssoArr as $_yearTermID => $_userIDArr) {
                    $SQL_conds_Arr[] = "( YearTermID = '$_yearTermID' AND UserID IN ('" . implode("','", $_userIDArr) . "') )";
                }
                $yearterm_user_conds = "( " . implode(" OR ", (array) $SQL_conds_Arr) . " )";
                
                $termAssessment_conds = " AND TermAssessment IS NULL ";
                if ($termAssessment != '') {
                    $termAssessment_conds = " AND TermAssessment = '$termAssessment'";
                }
                
                $sql = "SELECT
				/*YearTermID,*/ UserID as StudentID, Score, SubjectID, OrderMeritForm, OrderMeritFormTotal
				FROM
				{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
				WHERE
				1
				$termAssessment_conds
				AND $yearterm_user_conds
				AND (SubjectComponentCode IS NULL OR SubjectComponentCode = '')  
				AND Score > -1";
                $result = $this->returnResultSet($sql);
            }
            
            return $result;
        }

        public function getStudentSchoolExamResult($targetForm, $StudentIDArr, $SubjectID, $YearTermID = '')
        {
            global $eclass_db;
            
            $isCmp = $this->checkIsCmptSubject($SubjectID);
            if ($isCmp) {
                $cmp_conds = " AND (SubjectComponentCode IS NOT NULL AND SubjectComponentCode != '') ";
            } else {
                $cmp_conds = " AND (SubjectComponentCode IS NULL OR SubjectComponentCode = '') ";
            }
            
            if ($YearTermID == '') {
                // Whole Year Result
                $academicYearStudentArr = $this->getStudentFormAcademicYear($targetForm, $StudentIDArr);
                
                $aysAssoArr = array();
                foreach ((array) $academicYearStudentArr as $_infoArr) {
                    $_UserID = $_infoArr['UserID'];
                    $_AcademicYearID = $_infoArr['AcademicYearID'];
                    $aysAssoArr[$_AcademicYearID][] = $_UserID;
                }
                $SQL_conds_Arr = array();
                foreach ((array) $aysAssoArr as $_academicYearID => $_userIDArr) {
                    $SQL_conds_Arr[] = "( AcademicYearID = '$_academicYearID' AND UserID IN ('" . implode("','", $_userIDArr) . "') )";
                }
                $year_user_conds = "( " . implode(" OR ", (array) $SQL_conds_Arr) . " )";
                
                $sql = "SELECT 
							/*AcademicYearID,*/ UserID as StudentID, Score 
						FROM 
							{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD 
						WHERE 
							IsAnnual = 1 AND( YearTermID IS NULL OR YEARTERMID = '' ) 
							AND SUBJECTID = '$SubjectID' 
							AND $year_user_conds
							$cmp_conds";
                $result = $this->returnResultSet($sql);
            } else {
                // Term Result
                $academicYearStudentArr = $this->getStudentFormYearTerm($targetForm, $StudentIDArr, $YearTermID);
                
                $aysAssoArr = array();
                foreach ((array) $academicYearStudentArr as $_infoArr) {
                    $_UserID = $_infoArr['UserID'];
                    $_YearTermID = $_infoArr['YearTermID'];
                    $aysAssoArr[$_YearTermID][] = $_UserID;
                }
                $SQL_conds_Arr = array();
                foreach ((array) $aysAssoArr as $_yearTermID => $_userIDArr) {
                    $SQL_conds_Arr[] = "( YearTermID = '$_yearTermID' AND UserID IN ('" . implode("','", $_userIDArr) . "') )";
                }
                $yearterm_user_conds = "( " . implode(" OR ", (array) $SQL_conds_Arr) . " )";
                
                $sql = "SELECT 
							/*YearTermID,*/ UserID as StudentID, Score 
						FROM 
							{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD 
						WHERE 
							SUBJECTID = '$SubjectID' 
							AND SubjectComponentID IS NULL 
							AND TermAssessment IS NULL 
							AND $yearterm_user_conds
							$cmp_conds";
                $result = $this->returnResultSet($sql);
            }
            
            return $result;
        }

        public function getDSEScore($level = '')
        {
            $lv = (string) $level;
            if ($lv == '5*') {
                return '6';
            } else 
                if ($lv == '5**') {
                    return '7';
                } else 
                    if ($lv == 'U' || $lv == 'X' || $lv == 'TT' || $lv == 'TD' || $lv == 'UT') {
                        return '0';
                    } else {
                        return $lv;
                    }
        }

        public function getDSELevel($int = '')
        {
            if ($int == '6') {
                return '5*';
            } else 
                if ($int == '7') {
                    return '5**';
                } else 
                    if ($int == '0') {
                        return 'U';
                    } else 
                        if ($int == 'X') {
                            return 'X';
                        } else {
                            return $int;
                        }
        }

        public function getDseLevelArr($returnLang = false)
        {
            global $Lang;
            
            $dseArr = array();
            for ($i = 1; $i <= 7; $i ++) {
                if ($returnLang == true) {
                    $dseArr[$i] = $Lang['SDAS']['DSEprediction']['DseLevel'][$i];
                } else {
                    $dseArr[$i] = $this->getDSELevel($i);
                }
            }
            return $dseArr;
        }

        public function getStatisData($NumberArr)
        {
            $result['Q1'] = $this->getQuartile($NumberArr, 0.25);
            $result['Q3'] = $this->getQuartile($NumberArr, 0.75);
            $result['Median'] = $this->getQuartile($NumberArr, 0.50);
            $result['Min'] = min($NumberArr);
            $result['Max'] = max($NumberArr);
            $result['Num'] = count($NumberArr);
            $result['IQR'] = $result['Q3'] - $result['Q1'];
            $result['Upper Outliers'] = $result['Q3'] + 1.5 * $result['IQR'];
            $result['Lower Outliers'] = $result['Q1'] - 1.5 * $result['IQR'];
            $result['Mean'] =  round(array_sum($NumberArr) / count($NumberArr),2);
            
            return $result;
        }

        public function getQuartile($parAry, $Quartile, $decimalPlace = '')
        {
            $parAry = array_values((array) $parAry);
            sort($parAry, SORT_NUMERIC);
            
            $pos = (count($parAry) - 1) * $Quartile;
            $base = floor($pos);
            $rest = $pos - $base;
            
            $quartile = '';
            if (count($parAry) > 0) {
                if (isset($parAry[$base + 1])) {
                    $quartile = $parAry[$base] + $rest * ($parAry[$base + 1] - $parAry[$base]);
                } else {
                    $quartile = $parAry[$base];
                }
                
                if ($decimalPlace) {
                    $quartile = my_round($quartile, $decimalPlace);
                }
            }
            return $quartile;
        }

        public function getAssessmentLastInputDate()
        {
            global $eclass_db;
            $sql = "SELECT ModifiedDate FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD ORDER BY ModifiedDate DESC LIMIT 1";
            $row = $this->returnVector($sql);
            $LastModified_Subject = $row[0];
            
            $sql = "SELECT ModifiedDate FROM {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD ORDER BY ModifiedDate DESC LIMIT 1";
            $row = $this->returnVector($sql);
            $LastModified_Main = $row[0];
            
            // $date = (sizeof($row)==0) ? '-' : $row[0];
            $date = max($LastModified_Subject, $LastModified_Main);
            $date = ($date == '') ? '-' : $date;
            return $date;
        }

        public function getExamLastInputDate()
        {
            $sql = "SELECT ModifiedDate FROM {$intranet_db}.EXAM_STUDENT_SCORE ORDER BY ModifiedDate DESC LIMIT 1";
            $row = $this->returnVector($sql);
            $LastModified = $row[0];
            
            $sql = "SELECT ModifiedDate FROM {$intranet_db}.EXAM_DSE_STUDENT_SCORE ORDER BY ModifiedDate DESC LIMIT 1";
            $row = $this->returnVector($sql);
            $LastModified_Dse = $row[0];
            
            if (strtotime($LastModified_Dse) > strtotime($LastModified)) {
                $tempDate = $LastModified_Dse;
            } else {
                $tempDate = $LastModified;
            }
            
            $date = ($tempDate == '') ? '-' : $tempDate;
            return $date;
        }

        public function getJupasLastInputDate()
        {
            global $intranet_db;
            $sql = "SELECT ModifiedDate FROM {$intranet_db}.JUPAS_STUDENT_OFFER ORDER BY ModifiedDate DESC LIMIT 1";
            $row = $this->returnVector($sql);
            $LastModified = $row[0];
            return (($LastModified == '') ? '-' : $LastModified);
        }

        public function getExamRecordNumV2($exam, $academicYearID)
        {
            if ($exam == EXAMID_HKDSE || $exam == EXAMID_HKDSE_APPEAL) {
                $tableName = 'EXAM_DSE_STUDENT_SCORE';
            } else {
                $tableName = 'EXAM_STUDENT_SCORE';
            }
            if ($academicYearID != '') {
                $sql = "SELECT count(*) FROM {$intranet_db}.$tableName where ExamID = '$exam' AND AcademicYearID = '$academicYearID'";
                $row = $this->returnVector($sql);
            }
            $num = ($row[0] === '' || ! isset($row[0])) ? 0 : $row[0];
            return $num;
        }

        public function getExamRecordNum($exam, $academicYearID, $className)
        {
            global $intranet_db;
            
            $userIDArr = $this->getStudentIDByClassNameAndYear($academicYearID, $className);
            
            if ($exam == EXAMID_HKDSE) {
                $tableName = 'EXAM_DSE_STUDENT_SCORE';
            } elseif ($exam == EXAMID_HKDSE_APPEAL) {
                $tableName = 'EXAM_DSEAPPEAL_STUDENT_SCORE';
            } else {
                $tableName = 'EXAM_STUDENT_SCORE';
            }
            
            if (! empty($userIDArr)) {
                $sql = "SELECT count(*) FROM {$intranet_db}.$tableName where ExamID = '$exam' AND AcademicYearID = '$academicYearID' AND StudentID IN ('" . implode("','", (array) $userIDArr) . "')";
                $row = $this->returnVector($sql);
            }
            $num = ($row[0] === '' || ! isset($row[0])) ? 0 : $row[0];
            return $num;
        }

        public function handleDseRawData($data)
        {
            if ($data != '') {
                // split row into arr
                $arr = preg_split('/\r\n/', $data);
                
                // split column into arr
                $new_arr = array();
                foreach ((array) $arr as $_key => $_string) {
                    $new_arr[$_key] = preg_split('/\s\s+/', $_string);
                }

                $schoolSubject = array();
                $readable_asso_arr = array();
                foreach ((array) $new_arr as $_key => $_array) {
                    // student info data array
                    if ($_array[0] != '') {
                        $readable_asso_arr[$_key]['DSE'] = array();
                        $_checkedSubject = array();
                        $subjectComponent = false;
                        $thisSubject = '';
                        foreach ((array) $_array as $__key => $__value) {
                            if ($__key <= 6) {
                                // student info
                                switch ($__key) {
                                    case 1:
                                        $readable_asso_arr[$_key]['STRN'] = $__value;
                                        break;
                                }
                            } else {
                                if ($__key > 6) {
                                    // student dse data
                                    // if(preg_match('/[AB]\d{3}/',$__value)){
                                    if (empty($__value)) {
                                        // skip empty value
                                        continue;
                                    }
                                    
                                    if (preg_match('/[ABC]\d{3}/', $__value) && !in_array($__value, array(
                                        'A161',
                                        'A162',
                                        'A163'
                                    ))) {
                                        // the value is a subject
                                        if (!in_array($__value, $_checkedSubject)) {
                                            $_checkedSubject[] = $__value;
                                            $readable_asso_arr[$_key]['DSE'][$__value] = array();
                                        } else {
                                            $subjectComponent = true;
                                        }
                                        $thisSubject = $__value;
                                        
                                        // collection of all array
                                        if (!in_array($__value, $schoolSubject)) {
                                            $schoolSubject[] = $__value;
                                        }
                                    } else {
                                        // N = if they are not required for SBA
                                        // Y = for appeal
                                        if ($__value == 'N' || $__value == 'Y') {
                                            continue;
                                        }

                                        // the value is a score or an index
                                        if (!$subjectComponent) {
                                            // [DM#3774] changed to apply predicted grade
                                            // [2020-0812-1602-15066] skip if is major subject result + predicted grade for absent subject
                                            if(!empty($readable_asso_arr[$_key]['DSE'][$thisSubject]) && $readable_asso_arr[$_key]['DSE'][$thisSubject][0] == 'X' &&
                                                !in_array($__value, array(
                                                    'A161',
                                                    'A162',
                                                    'A163'
                                            ))) {
                                                $readable_asso_arr[$_key]['DSE'][$thisSubject][0] = $__value;
                                                continue;
                                            }
                                            $readable_asso_arr[$_key]['DSE'][$thisSubject][] = $__value;
                                        } else {
                                            $readable_asso_arr[$_key]['DSE'][$thisSubject][] = $__value;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        // skip empty row
                    }
                }
            }

            return $readable_asso_arr;
        }

        public function getSchoolExamYearTermWithRecords($withAssessment = false)
        {
            global $eclass_db;
            if ($withAssessment) {
                $termAssessment_field = ", TermAssessment";
            }
            $sql = "SELECT DISTINCT AcademicYearID, YearTermID $termAssessment_field FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD ";
            return $this->returnResultSet($sql);
        }

        public function getExamYearWithRecords($examID)
        {
            global $intranet_db;
            
            $sql = "SELECT
			distinct(academicYearID)
			FROM
			{$intranet_db}.EXAM_STUDENT_SCORE as ess
			where ExamID = '$examID' ";
            return $this->returnVector($sql);
        }

        public function getDseYearWithRecords()
        {
            global $intranet_db;
            
            $sql = "SELECT
				distinct(ess.academicYearID)
				FROM
				{$intranet_db}.EXAM_DSE_STUDENT_SCORE as ess
				INNER JOIN 
					(Select * From ACADEMIC_YEAR_TERM Group By AcademicYearID) ayt
						ON ess.academicYearID = ayt.academicYearID
				ORDER BY
					ayt.TermStart desc
				";
            return $this->returnVector($sql);
        }

        public function getClassWithRecords($academicYearID, $examID = '')
        {
            global $intranet_db;
            
            if ($examID == '') {
                $examID = $this->getUsingExamID();
            }
            
            if ($examID == EXAMID_HKDSE) {
                $tableName = 'EXAM_DSE_STUDENT_SCORE';
            } elseif ($examID == EXAMID_HKDSE_APPEAL) {
                $tableName = 'EXAM_DSEAPPEAL_STUDENT_SCORE';
            } else {
                $tableName = 'EXAM_STUDENT_SCORE';
            }
            $sql = "SELECT 
						yc.YearClassID, yc.ClassTitleEN,yc.ClassTitleB5 
					FROM 
						{$intranet_db}.$tableName as ess 
						INNER JOIN YEAR_CLASS_USER as ycu on ess.StudentID = ycu.UserID 
						INNER JOIN YEAR_CLASS yc ON  ycu.YearClassID = yc.YearClassID and ess.academicYearID = yc.academicYearID 
					WHERE 
						ess.academicYearID = '$academicYearID' AND ExamID = '$examID'
					GROUP BY 
						YearClassID; ";
            return $this->returnResultSet($sql);
        }

        public function getPredictionGrade($studentPercentile, $subjectPercentileArr)
        {
            foreach ((array) $subjectPercentileArr as $_lv => $_lvPercentile) {
                if ($studentPercentile <= $_lvPercentile['result']) {
                    return $this->getDSELevel($_lv);
                } else {
                    continue;
                }
            }
            return 'U';
        }

        public function processDseScore($dseLvArr)
        {
            $sum = 0;
            foreach ((array) $dseLvArr as $_lv) {
                switch ((string) $_lv) {
                    case '5*':
                        $sum += 6;
                        break;
                    case '5**':
                        $sum += 7;
                        break;
                    default:
                        $sum += $_lv;
                        break;
                }
            }
            return $sum;
        }

        public function getDseCompulsorySubjectID()
        {
            $sql = "SELECT 
					RecordID as SubjectID,
					CODEID
					FROM ASSESSMENT_SUBJECT 
					WHERE CODEID IN ('080','165','22S','265') AND (CMP_CODEID IS NULL OR CMP_CODEID = '') AND RecordStatus = 1
					ORDER BY CODEID";
            return $this->returnResultSet($sql);
        }

        public function convertDseLevelToScore($studentGotLvArr)
        {
            foreach ((array) $studentGotLvArr as $_subjectID => $_grade) {
                $studentGotLvArr[$_subjectID] = $this->getDSEScore($_grade);
            }
            return $studentGotLvArr;
        }

        public function getDseDistribution($academicYearIDArr, $parDisplayArr)
        {
            global $Lang, $objSDAS, $sys_custom;
            
            $subjectIdFilterArr = '';
            if ($sys_custom['SDAS']['DSE']['SubjectPanelOwnSubject']) {
                $accessRight = $objSDAS->getAssessmentStatReportAccessRight();
                $subjectIdFilterArr = '';
                if (! $accessRight['admin'] && count($accessRight['subjectPanel'])) {
                    $subjectIdFilterArr = array_keys($accessRight['subjectPanel']);
                }
            }
            
            // getExamData($academicYearID , $examID='',$subjectID='',$UserID='', $excludeCmp=false)
            $examDataArr = $this->getExamData($academicYearIDArr, EXAMID_HKDSE, $subjectIdFilterArr, $UserID = '', $excludeCmpSubject = true, true);
            // $numYear = count(array_unique(Get_Array_By_Key($examDataArr, 'AcademicYearID')));
            $subjectIdArr = array_values(array_unique(Get_Array_By_Key($examDataArr, 'SubjectID')));
            $dataAssoc = BuildMultiKeyAssoc($examDataArr, array(
                'SubjectID',
                'AcademicYearID'
            ), array(), 0, 1);
            
            $academicYearInfoAry_keyID = BuildMultiKeyAssoc(GetAllAcademicYearInfo(), 'AcademicYearID');
            $subjectInfoArr = $this->getSubjectCodeAndID($subjectIdArr, array(), true, false);
            $subjectAssoc = BuildMultiKeyAssoc($subjectInfoArr, 'SubjectID', array(
                Get_Lang_Selection('ch_main_name', 'main_name')
            ), 1);
            
            $result = array();
            $count = 0;
            foreach ((array) $dataAssoc as $__subjectID => $_subjectArr) {
                foreach ((array) $_subjectArr as $_academicYearID => $__studentResultArr) {
                    $dseLvArr = Get_Array_By_Key($__studentResultArr, 'Score');
                    $dseLvCountArr = array_count_values($dseLvArr);
                    $result[$count] = $this->getCountOfLevelAbove($dseLvCountArr);
                    $result[$count]['SubjectID'] = $__subjectID;
                    $result[$count]['AcademicYear'] = $academicYearInfoAry_keyID[$_academicYearID][Get_Lang_Selection('YearNameB5', 'YearNameEN')];
                    $result[$count]['YearSorting'] = $academicYearInfoAry_keyID[$_academicYearID]['YearNameEN'];
                    $result[$count]['AcademicYearID'] = $_academicYearID;
                    $count ++;
                }
            }
            // sortByColumn2($result, 'YearSorting');
            $resultAssoc = BuildMultiKeyAssoc($result, array(
                'SubjectID',
                'AcademicYearID'
            ), array(), 0);
            
            if (in_array('Q', $parDisplayArr))
                $displayByQ = true;
            if (in_array('P', $parDisplayArr))
                $displayByP = true;
            $displayColNum = count($parDisplayArr);
            
            $html = '';
            $html .= '<div class="chart_tables">';
            $html .= '<table class="common_table_list_v30 view_table_list_v30">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th rowspan="2">' . $Lang['Header']['Menu']['Subject'] . '</th>';
            $html .= '<th rowspan="2">' . $Lang['General']['SchoolYear'] . '</th>';
            $html .= '<th colspan="' . $displayColNum . '">' . $Lang['SDAS']['DSEstat']['DSELevel']['5**'] . '</th>';
            $html .= '<th colspan="' . $displayColNum . '">' . $Lang['SDAS']['DSEstat']['DSELevel']['5*'] . $Lang['SDAS']['DSEstat']['orAbove'] . '</th>';
            $html .= '<th colspan="' . $displayColNum . '">' . $Lang['SDAS']['DSEstat']['DSELevel']['5'] . $Lang['SDAS']['DSEstat']['orAbove'] . '</th>';
            $html .= '<th colspan="' . $displayColNum . '">' . $Lang['SDAS']['DSEstat']['DSELevel']['4'] . $Lang['SDAS']['DSEstat']['orAbove'] . '</th>';
            $html .= '<th colspan="' . $displayColNum . '">' . $Lang['SDAS']['DSEstat']['DSELevel']['3'] . $Lang['SDAS']['DSEstat']['orAbove'] . '</th>';
            $html .= '<th colspan="' . $displayColNum . '">' . $Lang['SDAS']['DSEstat']['DSELevel']['2'] . $Lang['SDAS']['DSEstat']['orAbove'] . '</th>';
            $html .= '<th colspan="' . $displayColNum . '">' . $Lang['SDAS']['DSEstat']['DSELevel']['1'] . $Lang['SDAS']['DSEstat']['orAbove'] . '</th>';
            $html .= '<th colspan="' . $displayColNum . '">' . $Lang['SDAS']['DSEstat']['DSELevel']['U'] . '</th>';
            $html .= '<th colspan="' . $displayColNum . '">' . $Lang['General']['Total'] . '</th>';
            $html .= '<th colspan="1">' . $Lang['SDAS']['DSEstat']['DSELevel']['X'] . '</th>';
            $html .= '</tr>';
            $html .= '<tr>';
            for ($i = 0; $i < 10; $i ++) {
                if ($i == 9) {
                    $html .= '<th>' . $Lang['SDAS']['ColumnNumberShort'] . '</th>';
                } else {
                    if ($displayByQ)
                        $html .= '<th>' . $Lang['SDAS']['ColumnNumberShort'] . '</th>';
                    if ($displayByP)
                        $html .= '<th>' . $Lang['SDAS']['ColumnPercentShort'] . '</th>';
                }
            }
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            // foreach((array)$resultAssoc as $_subjectID =>$rowArr){
            foreach ((array) $subjectAssoc as $_subjectID => $_subjectName) {
                $rowArr = $resultAssoc[$_subjectID];
                $numYear = count($rowArr);
                $rowNum = 0;
                foreach ((array) $academicYearInfoAry_keyID as $__academicYearID => $__yearInfo) {
                    $row = $rowArr[$__academicYearID];
                    if (! empty($row)) {
                        $html .= '<tr>';
                        if ($rowNum == 0) {
                            $html .= '<td rowspan="' . $numYear . '">' . $_subjectName . '</td>';
                        }
                        $html .= '<td style="font-weight:normal;">' . $row['AcademicYear'] . '</td>';
                        if ($displayByQ)
                            $html .= '<td>' . $row['num_5ssplus'] . '</td>';
                        if ($displayByP)
                            $html .= '<td>' . $row['percent_5ssplus'] . '</td>';
                        if ($displayByQ)
                            $html .= '<td>' . $row['num_5splus'] . '</td>';
                        if ($displayByP)
                            $html .= '<td>' . $row['percent_5splus'] . '</td>';
                        if ($displayByQ)
                            $html .= '<td>' . $row['num_5plus'] . '</td>';
                        if ($displayByP)
                            $html .= '<td>' . $row['percent_5plus'] . '</td>';
                        if ($displayByQ)
                            $html .= '<td>' . $row['num_4plus'] . '</td>';
                        if ($displayByP)
                            $html .= '<td>' . $row['percent_4plus'] . '</td>';
                        if ($displayByQ)
                            $html .= '<td>' . $row['num_3plus'] . '</td>';
                        if ($displayByP)
                            $html .= '<td>' . $row['percent_3plus'] . '</td>';
                        if ($displayByQ)
                            $html .= '<td>' . $row['num_2plus'] . '</td>';
                        if ($displayByP)
                            $html .= '<td>' . $row['percent_2plus'] . '</td>';
                        if ($displayByQ)
                            $html .= '<td>' . $row['num_1plus'] . '</td>';
                        if ($displayByP)
                            $html .= '<td>' . $row['percent_1plus'] . '</td>';
                        if ($displayByQ)
                            $html .= '<td>' . $row['num_U'] . '</td>';
                        if ($displayByP)
                            $html .= '<td>' . $row['percent_U'] . '</td>';
                        if ($displayByQ)
                            $html .= '<td>' . $row['num_Total'] . '</td>';
                        if ($displayByP)
                            $html .= '<td>' . $row['percent_Total'] . '</td>';
                            // if($displayByQ)
                        $html .= '<td>' . $row['num_X'] . '</td>';
                        // if($displayByP)
                        // $html .= '<td>'.$row['percent_X'].'</td>';
                        $html .= '</tr>';
                        $rowNum ++;
                    }
                }
            }
            $html .= '</tbody>';
            $html .= '</table>';
            $html .= '</div">';
            return $html;
        }

        public function getDseLevelSequenceArr()
        {
            return array(
                'U',
                '1',
                '2',
                '3',
                '4',
                '5',
                '5*',
                '5**'
            );
        }

        public function getCountOfLevelAbove($dseLvCountArr, $prefex = '')
        {
            $dseLvArr = $this->getDseLevelSequenceArr();
            
            // # Z112834
            $totalStudent = array_sum(array_values($dseLvCountArr)) - (int) $dseLvCountArr['X'];
            
            $count = 0;
            $resultArr = array();
            for ($x = 7; $x >= 1; $x --) {
                $level = $dseLvArr[$x];
                $numOfLevelX = $dseLvCountArr[$level];
                $count = $count + $numOfLevelX;
                // replace * -> s for chart_set.js
                $level = str_replace('*', 's', $level);
                $resultArr[$prefex . 'num_' . $level . 'plus'] = $count;
                if ($count == 0) {
                    $resultArr[$prefex . 'percent_' . $level . 'plus'] = '0';
                } else {
                    $resultArr[$prefex . 'percent_' . $level . 'plus'] = number_format($count / $totalStudent * 100, 1) . '';
                }
            }
            $U_number = $dseLvCountArr['U'];
            if ($U_number == '') {
                $resultArr['num_U'] = 0;
                $resultArr['percent_U'] = '0';
            } else {
                $resultArr['num_U'] = $U_number;
                $resultArr['percent_U'] = number_format($U_number / $totalStudent * 100, 1) . '';
            }
            $X_number = $dseLvCountArr['X'];
            if ($X_number == '') {
                $resultArr['num_X'] = 0;
                $resultArr['percent_X'] = '0';
            } else {
                $resultArr['num_X'] = $X_number;
                // don't need to show X (absent percentage)
                // $resultArr['percent_X'] = number_format($X_number/$totalStudent*100, 1).'';
                $resultArr['percent_X'] = '0';
            }
            $resultArr['num_Total'] = $totalStudent;
            // $resultArr['percent_Total'] = ($totalStudent/$totalStudent*100).'';
            $resultArr['percent_Total'] = '100';
            
            return $resultArr;
        }

        public function getDseBestFiveScoreStat($reportConfigArr, $academicYearIDArr, $parDisplayArr)
        {
            global $Lang, $intranet_root;
            
            $configStartScore = $reportConfigArr['StartScore'];
            $configInterval = $reportConfigArr['Interval'];
            $academicYearInfoAssoArr = BuildMultiKeyAssoc(GetAllAcademicYearInfo(), 'AcademicYearID');
            // $examDataArr = $this->getExamData($AcademicYearArr, EXAMID_HKDSE, $subjectID='', $schoolCode, $excludeArr='', true, '', true, 'A');
            $examDataArr = $this->getExamData($academicYearIDArr, EXAMID_HKDSE, $subjectID = '', $UserID = '', $excludeCmpSubject = true);
            $dataAssoc = BuildMultiKeyAssoc($examDataArr, array(
                'AcademicYearID',
                'StudentID'
            ), array(), 0, 1);
            
            $result = array();
            $totalArr = array();
            $numYear = 0;
            foreach ((array) $dataAssoc as $_academicYearID => $_yearStuArr) {
                $totalArr[$_academicYearID] = count($_yearStuArr);
                foreach ((array) $_yearStuArr as $__studentID => $__subjectResultArr) {
                    $studentGotLvArr = Get_Array_By_Key($__subjectResultArr, 'Score');
                    // // replace U by 0 , 5* by 6 , 5** by 7
                    $studentGotLvScoreArr = $this->convertDseLevelToScore($studentGotLvArr);
                    rsort($studentGotLvScoreArr);
                    $bestFive = array_slice($studentGotLvScoreArr, 0, 5, true);
                    $studentsScoreArr = array_sum($bestFive);
                    $result[$_academicYearID][$__studentID] = $studentsScoreArr;
                    $countScoreArr[$_academicYearID] = array_count_values($result[$_academicYearID]);
                }
                $numYear ++;
            }
            
            $rangeInterval = $configInterval - 1;
            foreach ((array) $academicYearIDArr as $__academicYearID) {
                $__scoreCountArr = $countScoreArr[$__academicYearID];
                // foreach((array)$countScoreArr as $__academicYearID => $__scoreCountArr){
                // foreach((array)$_yearResultArr as $__academicYearID => $__scoreCountArr){
                if (! empty($__scoreCountArr)) {
                    ksort($__scoreCountArr);
                    $totalNum = array_sum($__scoreCountArr);
                    
                    $__count = 0;
                    $__rangeCount = 0;
                    $scoreRangeArr = array();
                    $scoreRangeNameArr = array();
                    $scoreRangePercentArr = array();
                    for ($x = $configStartScore; $x <= 35; $x ++) {
                        if ($__count == 0) {
                            // start a new range
                            $rangeStart = $x;
                            $rangeEnd = (($x + $rangeInterval) > 35) ? 35 : $x + $rangeInterval;
                            $scoreRangeNameArr[$__rangeCount] = $rangeStart . '-' . $rangeEnd;
                            $scoreRangeArr[$__rangeCount] = 0;
                        }
                        $scoreNum = (is_int($__scoreCountArr[$x]) ? $__scoreCountArr[$x] : 0);
                        $scoreRangeArr[$__rangeCount] = $scoreRangeArr[$__rangeCount] + $scoreNum;
                        $__count ++;
                        if ($__count == $configInterval || $x == 35) {
                            $scoreRangePercentArr[$__rangeCount] = number_format($scoreRangeArr[$__rangeCount] / $totalNum * 100, 1);
                            $scoreRangePercentArr_forChart[$__rangeCount] = floatval($scoreRangeArr[$__rangeCount] / $totalNum * 100);
                            $__count = 0;
                            $__rangeCount ++;
                        }
                    }
                    $dataArr['data']['highChart']['percent'][] = array(
                        'name' => $academicYearInfoAssoArr[$__academicYearID]['YearNameEN'],
                        'data' => $scoreRangePercentArr_forChart
                    );
                    $dataArr['data']['highChart']['number'][] = array(
                        'name' => $academicYearInfoAssoArr[$__academicYearID]['YearNameEN'],
                        'data' => $scoreRangeArr
                    );
                    $tempResultArr[$__academicYearID]['Percent'] = $scoreRangePercentArr;
                    $tempResultArr[$__academicYearID]['Number'] = $scoreRangeArr;
                }
            }
            
            $resultArr = array();
            foreach ((array) $scoreRangeNameArr as $_key => $_rangeName) {
                foreach ((array) $academicYearIDArr as $_academicYearID) {
                    $resultArr[$_key]['rowName'] = $_rangeName;
                    // $_academicYearName = $academicYearInfoAssoArr[$_academicYearID];
                    if (isset($tempResultArr[$_academicYearID])) {
                        $resultArr[$_key][$_academicYearID . '_Percent'] = $tempResultArr[$_academicYearID]['Percent'][$_key];
                        $resultArr[$_key][$_academicYearID . '_Num'] = $tempResultArr[$_academicYearID]['Number'][$_key];
                    } else {
                        $resultArr[$_key][$_academicYearID . '_Percent'] = '--';
                        $resultArr[$_key][$_academicYearID . '_Num'] = '--';
                    }
                }
            }
            
            $resultArr2 = array();
            foreach ((array) $tempResultArr as $_academicYearID => $__tmpResultAry) {
                $tmpRowAry = array();
                // $totalTemp['rowName'] = $Lang['SDAS']['DSEstat']['NumApplied'];
                $totalTemp = array();
                $totalTemp['rowName'] = $Lang['SDAS']['DSEstat']['NumApplied'];
                $totalTemp['academicYear'] = $academicYearInfoAssoArr[$_academicYearID][Get_Lang_Selection('YearNameB5', 'YearNameEN')];
                $totalTemp['academicYearID'] = $_academicYearID;
                $totalTemp['Percent'] = '100.0';
                $totalTemp['Number'] = $totalArr[$_academicYearID];
                
                foreach ($__tmpResultAry['Percent'] as $___rowKey => $___data) {
                    $tmpRowAry['rowName'] = $scoreRangeNameArr[$___rowKey];
                    $tmpRowAry['academicYear'] = $academicYearInfoAssoArr[$_academicYearID][Get_Lang_Selection('YearNameB5', 'YearNameEN')];
                    $tmpRowAry['academicYearID'] = $_academicYearID;
                    $tmpRowAry['Percent'] = $___data;
                    $tmpRowAry['Number'] = $__tmpResultAry['Number'][$___rowKey];
                    $resultArr2[] = $tmpRowAry;
                }
                $resultArr3[] = $totalTemp;
            }
            $resultArr2 = array_merge($resultArr2, $resultArr3);
            $resultAssoc = BuildMultiKeyAssoc($resultArr2, array(
                'rowName',
                'academicYearID'
            ), 0, 1);
            
            if (in_array('Q', $parDisplayArr))
                $displayByQ = true;
            if (in_array('P', $parDisplayArr))
                $displayByP = true;
            $displayColNum = count($parDisplayArr);
            
            $html = '';
            $html .= '<div class="chart_tables">';
            $html .= '<table class="common_table_list_v30 view_table_list_v30">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th>' . $Lang['SDAS']['DSEBestFive']['Title'] . '</th>';
            $html .= '<th>' . $Lang['General']['SchoolYear'] . '</th>';
            if ($displayByQ)
                $html .= '<th>' . $Lang['SDAS']['ColumnNumber'] . '</th>';
            if ($displayByP)
                $html .= '<th>' . $Lang['SDAS']['ColumnPercent'] . '</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            foreach ((array) $resultAssoc as $_rowName => $_yearResultArr) {
                $rowNum = 0;
                foreach ((array) $academicYearIDArr as $_academicYearID) {
                    $rowInfo = $_yearResultArr[$_academicYearID];
                    if (! empty($rowInfo)) {
                        $html .= '<tr>';
                        if ($rowNum == 0) {
                            $html .= '<td rowspan="' . $numYear . '">' . $_rowName . '</td>';
                        }
                        $html .= '<td style="font-weight:normal;">' . $rowInfo['academicYear'] . '</td>';
                        if ($displayByQ)
                            $html .= '<td>' . $rowInfo['Number'] . '</td>';
                        if ($displayByP)
                            $html .= '<td>' . $rowInfo['Percent'] . '</td>';
                        $html .= '</tr>';
                    }
                    $rowNum ++;
                }
            }
            $html .= '</tbody>';
            $html .= '</table>';
            $html .= '</div>';
            
            $groupNameArr = array();
            foreach ((array) $scoreRangeNameArr as $name) {
                $groupNameArr[] = $name;
            }
            
            if ($displayByQ)
                $highchart = $this->getHighChartJs_GroupedBarChart($dataArr['data']['highChart']['number'], $groupNameArr, $targetDivID = 'BarChart', $Lang['SDAS']['DSEBestFive']['Title'] . $Lang['SDAS']['TitleNumber'], $Lang['SDAS']['DSEstat']['number'], $mode = '');
            if ($displayByP)
                $highchart2 = $this->getHighChartJs_GroupedBarChart($dataArr['data']['highChart']['percent'], $groupNameArr, $targetDivID = 'BarChartPercent', $Lang['SDAS']['DSEBestFive']['Title'] . $Lang['SDAS']['TitlePercent'], $Lang['SDAS']['DSEstat']['percent'], $mode = '%', 'chartDataB');
            
            $script = '<script>' . '$("#BarChart").html("");' . '$("#BarChartPercent").html("");' . $highchart . '' . $highchart2 . '</script>';
            
            return $script . $html;
        }

        public function getDsePredictResultAcademicYearId()
        {
            $sql = "SELECT DISTINCT AcademicYearID FROM EXAM_DSE_PREDICT_SCORE";
            $rs = $this->returnVector($sql);
            return $rs;
        }

        public function getDsePredictResult($academicYearID, $subjectIdArr = array())
        {
            $subjectIdSql = implode("','", (array) $subjectIdArr);
            
            if ($subjectIdArr) {
                $sql = "SELECT * FROM 
		            EXAM_DSE_PREDICT_SCORE
	            WHERE
		            AcademicYearID='{$academicYearID}' 
		        AND 
		            SubjectID IN ('{$subjectIdSql}')";
            } else {
                $sql = "SELECT * FROM 
		            EXAM_DSE_PREDICT_SCORE
	            WHERE
		            AcademicYearID='{$academicYearID}'";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getLastDsePredictSchema()
        {
            $sql = "SELECT * FROM EXAM_DSE_PREDICT_SCORE_SCHEMA ORDER BY RecordID DESC LIMIT 1";
            $rs = $this->returnResultSet($sql);
            if (count($rs)) {
                return $rs[0];
            }
            return array();
        }

        public function saveDsePredictSchema($academicYearID, $data)
        {
            $result = array();
            $this->Start_Trans();
            
            $sql = "INSERT INTO `EXAM_DSE_PREDICT_SCORE_SCHEMA` (
		        `AcademicYearID`, 
		        `YearID`, 
		        `ReferenceFromDseAcademicYearID`, 
		        `ReferenceToDseAcademicYearID`, 
		        `ExamType`, 
		        `ExamDetails`, 
		        `PredictMethod`,
                `InputDate`, 
                `InputBy`,
				`SelectedYearID`
				) VALUES (
		        '{$academicYearID}',
		        '{$data['YearID']}',
		        '{$data['FromDseAcademicYearID']}',
		        '{$data['ToDseAcademicYearID']}',
		        '{$data['ExamType']}',
		        '{$data['ExamDetails']}',
		        '{$data['PredictMethod']}',
                NOW(),
                '{$_SESSION['UserID']}',
				'{$data['SelectedDseAcademicYearIDs']}'
	        );";
            $result[] = $this->db_db_query($sql);
            
            if (! in_array(false, $result, true)) {
                $schemaId = $this->db_insert_id();
                $this->Commit_Trans();
                return $schemaId;
            } else {
                $this->RollBack_Trans();
            }
            return false;
        }

        public function saveDsePredictResult($academicYearID, $schemaId, $data)
        {
            $result = array();
            
            $this->Start_Trans();
            
            foreach ($data as $subjectID => $d1) {
                $sql = "DELETE FROM EXAM_DSE_PREDICT_SCORE WHERE AcademicYearID='{$academicYearID}' AND SubjectID='{$subjectID}'";
                $result[] = $this->db_db_query($sql);
                
                foreach ($d1 as $studentID => $predictData) {
                    $sql = "INSERT INTO 
		                `EXAM_DSE_PREDICT_SCORE` 
	                (
		                `AcademicYearID`, 
		                `SchemaID`, 
		                `SubjectID`, 
		                `StudentID`, 
		                `ExamScore`, 
		                `PredictGrade`, 
		                `InputDate`, 
		                `InputBy`, 
		                `ModifiedDate`, 
		                `ModifiedBy`
	                ) VALUES (
		                '{$academicYearID}',
		                '{$schemaId}',
		                '{$subjectID}',
		                '{$studentID}',
		                '{$predictData['Score']}',
		                '{$predictData['PredictGrade']}',
		                NOW(),
		                '{$_SESSION['UserID']}',
		                NOW(),
		                '{$_SESSION['UserID']}'
	                );";
                    $result[] = $this->db_db_query($sql);
                }
            }
            
            if (! in_array(false, $result, true)) {
                $this->Commit_Trans();
                return true;
            } else {
                $this->RollBack_Trans();
            }
            return false;
        }

        public function getUniversityReqirementStat($academicYearIDArr, $parDisplayArr)
        {
            global $Lang;
            
            // $examDataArr = $scoreDateObj->getExamData($academicYearIDArr, EXAMID_HKDSE, $subjectID='', $schoolCode, $excludeArr='', true, '', true, 'A');
            $examDataArr = $this->getExamData($academicYearIDArr, EXAMID_HKDSE, $subjectID = '', $UserID = '', $excludeCmpSubject = true);
            $existingAcademicYearIDAry = array_values(array_unique(Get_Array_By_Key($examDataArr, 'AcademicYearID')));
            $dataAssoc = BuildMultiKeyAssoc($examDataArr, array(
                'AcademicYearID',
                'StudentID',
                'SubjectID'
            ), array(
                'Score'
            ), 1, 0);
            $coreSubjectIDArr = Get_Array_By_Key($this->getDseCompulsorySubjectID(), 'SubjectID');
            $coreSubjectIDAssocArr = BuildMultiKeyAssoc($this->getDseCompulsorySubjectID(), 'CODEID', array(
                'SubjectID'
            ), 1);
            $academicYearInfoAssoArr = BuildMultiKeyAssoc(GetAllAcademicYearInfo(), 'AcademicYearID');
            
            // //////////////////// count for 3322 different combination start //////////////////////
            $resultCount = array();
            foreach ((array) $dataAssoc as $__academicYearID => $__studentResultAssoc) {
                $resultCount[$__academicYearID]['55'] = 0;
                $resultCount[$__academicYearID]['5'] = 0;
                $resultCount[$__academicYearID]['44'] = 0;
                $resultCount[$__academicYearID]['4'] = 0;
                $resultCount[$__academicYearID]['33'] = 0;
                $resultCount[$__academicYearID]['3'] = 0;
                $resultCount[$__academicYearID]['coreCount'] = 0;
                $resultCount[$__academicYearID]['22'] = 0;
                $resultCount[$__academicYearID]['2'] = 0;
                foreach ((array) $__studentResultAssoc as $___studentID => $___dseSubjectResultArr) {
                    $___dseSubjectScoreArr = $this->convertDseLevelToScore($___dseSubjectResultArr);
                    // debug_pr($___dseSubjectScoreArr);
                    $____compulsoryScoreArr = array();
                    $____electiveScoreArr = array();
                    foreach ((array) $___dseSubjectScoreArr as $____subjectID => $____score) {
                        // separate compulsory & elective here
                        if (in_array($____subjectID, (array) $coreSubjectIDArr)) {
                            // core 3322 here
                            $____compulsoryScoreArr[$____subjectID] = $____score;
                        } else {
                            // elective
                            $____electiveScoreArr[$____subjectID] = $____score;
                        }
                    }
                    arsort($____compulsoryScoreArr);
                    arsort($____electiveScoreArr);
                    
                    $conditions = '3,3,2,2';
                    $elective_conditions = '2,2';
                    $compulConditionArr = explode(',', $conditions);
                    $electiveConditionArr = explode(',', $elective_conditions);
                    // $compulConditionCountArr = array_count_values($compulConditionArr);
                    $electiveConditionCountArr = array_count_values($electiveConditionArr);
                    
                    // initialize condition checking Arr
                    // compulsory 3322 = Chi 3 , Eng 3, Math 2, LS 2
                    $compulCondition = array();
                    $electiveCondition = array();
                    foreach ((array) $compulConditionArr as $_num => $_lvRequired) {
                        switch ($_num) {
                            case '0':
                                // Chi
                                $_subjectID = $coreSubjectIDAssocArr['080'];
                                break;
                            case '1':
                                // Eng
                                $_subjectID = $coreSubjectIDAssocArr['165'];
                                break;
                            case '2':
                                // Math
                                $_subjectID = $coreSubjectIDAssocArr['22S'];
                                break;
                            case '3':
                                // LS
                                $_subjectID = $coreSubjectIDAssocArr['265'];
                                break;
                        }
                        $compulCondition[$_subjectID] = $_lvRequired;
                    }
                    // for any electives subject
                    foreach ((array) $electiveConditionCountArr as $_lv => $_number) {
                        $electiveCondition[$_lv] = array();
                        for ($i = 0; $i < $_number; $i ++) {
                            $electiveCondition[$_lv][] = '';
                        }
                    }
                    
                    // prepare checking for compulsory subject requirement
                    if (! empty($____compulsoryScoreArr)) {
                        $meetCoreRequirement = true;
                        foreach ((array) $____compulsoryScoreArr as $_subjectID => $_dseScore) {
                            $_requirement = $compulCondition[$_subjectID];
                            if ($_dseScore >= $_requirement) {
                                continue;
                            } else {
                                $meetCoreRequirement = false;
                                break;
                            }
                        }
                    } else {
                        $meetCoreRequirement = false;
                    }
                    // prepare checking for elective subject requirement
                    foreach ((array) $____electiveScoreArr as $_dseScore) {
                        foreach ((array) $electiveConditionCountArr as $_lv => $_number) {
                            if (in_array('', (array) $electiveCondition[$_lv])) {
                                $firstNotFilledKey = array_search('', $electiveCondition[$_lv]);
                                // $thisLvScore = $libpf_exam->getDSEScore($_lv);
                                $thisLvScore = $_lv;
                                if ($_dseScore >= $thisLvScore) {
                                    $electiveCondition[$_lv][$firstNotFilledKey] = $_dseScore;
                                    break;
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                    // debug_pr($compulCondition);
                    // debug_pr($electiveCondition);
                    
                    if ($meetCoreRequirement) {
                        $resultCount[$__academicYearID]['coreCount'] ++;
                        $bestTwoElective = $electiveCondition[2];
                        $gradeCountArr = array_count_values($bestTwoElective);
                        
                        if (count($gradeCountArr) == 1) {
                            foreach ((array) $gradeCountArr as $score => $number) {
                                for ($x = 5; $x >= 2; $x --) {
                                    if ($x > $score) {
                                        continue;
                                    } else {
                                        $resultCount[$__academicYearID][$x . $x] ++;
                                        $resultCount[$__academicYearID][$x] ++;
                                    }
                                }
                                // only loop once for safety reason break here;
                                break;
                            }
                        } else {
                            $higherScore = '';
                            $lowerScore = '';
                            foreach ((array) $gradeCountArr as $score => $number) {
                                if ($higherScore == '') {
                                    $higherScore = $score;
                                } else 
                                    if ($lowerScore == '') {
                                        $lowerScore = $score;
                                    }
                            }
                            // $resultCount[$lowerScore] ++;
                            for ($x = 5; $x >= 2; $x --) {
                                if ($x > $higherScore) {
                                    continue;
                                } else {
                                    $resultCount[$__academicYearID][$x] ++;
                                }
                            }
                            if ($lowerScore != '') {
                                for ($x = 2; $x <= 5; $x ++) {
                                    if ($x > $lowerScore) {
                                        break;
                                    } else {
                                        $resultCount[$__academicYearID][$x . $x] ++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // //////////////////// count for 3322 different combination end //////////////////////
            
            // ////////////////////// count for 22222 include 4 core start //////////////////////
            // $resultCount2 = array();
            foreach ((array) $dataAssoc as $__academicYearID => $__studentResultAssoc) {
                // 22222 = core 2222 + elective 2
                $resultCount[$__academicYearID]['core22222'] = 0;
                foreach ((array) $__studentResultAssoc as $___studentID => $___dseSubjectResultArr) {
                    $___dseSubjectScoreArr = $this->convertDseLevelToScore($___dseSubjectResultArr);
                    
                    $____compulsoryScoreArr = array();
                    $____electiveScoreArr = array();
                    foreach ((array) $___dseSubjectScoreArr as $____subjectID => $____score) {
                        // separate compulsory & elective here
                        if (in_array($____subjectID, (array) $coreSubjectIDArr)) {
                            // core 2222 here
                            $____compulsoryScoreArr[$____subjectID] = $____score;
                        } else {
                            // elective
                            $____electiveScoreArr[$____subjectID] = $____score;
                        }
                    }
                    arsort($____compulsoryScoreArr);
                    arsort($____electiveScoreArr);
                    
                    $conditions = '2,2,2,2';
                    $elective_conditions = '2';
                    $compulConditionArr = explode(',', $conditions);
                    $electiveConditionArr = explode(',', $elective_conditions);
                    $compulConditionCountArr = array_count_values($compulConditionArr);
                    $electiveConditionCountArr = array_count_values($electiveConditionArr);
                    
                    // initialize condition checking Arr
                    $compulCondition = array();
                    $electiveCondition = array();
                    foreach ((array) $compulConditionCountArr as $_lv => $_number) {
                        $compulCondition[$_lv] = array();
                        for ($i = 0; $i < $_number; $i ++) {
                            $compulCondition[$_lv][] = '';
                        }
                    }
                    foreach ((array) $electiveConditionCountArr as $_lv => $_number) {
                        $electiveCondition[$_lv] = array();
                        for ($i = 0; $i < $_number; $i ++) {
                            $electiveCondition[$_lv][] = '';
                        }
                    }
                    
                    // prepare checking for compulsory subject requirement
                    foreach ((array) $____compulsoryScoreArr as $_subjectID => $_dseScore) {
                        foreach ((array) $compulConditionCountArr as $_lv => $_number) {
                            if (in_array('', (array) $compulCondition[$_lv])) {
                                $firstNotFilledKey = array_search('', $compulCondition[$_lv]);
                                // $thisLvScore = $libpf_exam->getDSEScore($_lv);
                                $thisLvScore = $_lv;
                                if ($_dseScore >= $thisLvScore) {
                                    // $compulCondition[$_lv][$firstNotFilledKey] = $_subjectID.'~~~~~~'.$_dseScore;
                                    $compulCondition[$_lv][$firstNotFilledKey] = $_subjectID;
                                    break;
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                    
                    // prepare checking for elective subject requirement
                    foreach ((array) $____electiveScoreArr as $_dseScore) {
                        foreach ((array) $electiveConditionCountArr as $_lv => $_number) {
                            if (in_array('', (array) $electiveCondition[$_lv])) {
                                $firstNotFilledKey = array_search('', $electiveCondition[$_lv]);
                                // $thisLvScore = $libpf_exam->getDSEScore($_lv);
                                $thisLvScore = $_lv;
                                if ($_dseScore >= $thisLvScore) {
                                    $electiveCondition[$_lv][$firstNotFilledKey] = $_dseScore;
                                    break;
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                    
                    $meetCoreRequirement = true;
                    foreach ((array) $compulConditionCountArr as $_lv => $_number) {
                        if (in_array('', $compulCondition[$_lv])) {
                            $meetCoreRequirement = false;
                        }
                    }
                    $meetElectiveRequirement = true;
                    foreach ((array) $electiveConditionCountArr as $_lv => $_number) {
                        if (in_array('', $electiveCondition[$_lv])) {
                            $meetElectiveRequirement = false;
                        }
                    }
                    if ($meetCoreRequirement && $meetElectiveRequirement) {
                        $resultCount[$__academicYearID]['core22222'] ++;
                    }
                }
            }
            // ////////////////////// count for 22222 include 4 core end //////////////////////
            
            // //////////////////////// count for 22222 include Chi & Eng start //////////////////////
            $chiEngIDArr = Get_Array_By_Key($this->getChiEngSubjectID(), 'SubjectID');
            // $resultCount3 = array();
            foreach ((array) $dataAssoc as $__academicYearID => $__studentResultAssoc) {
                // 22222 = chi eng 22 + elective 222
                $resultCount[$__academicYearID]['CE22222'] = 0;
                foreach ((array) $__studentResultAssoc as $___studentID => $___dseSubjectResultArr) {
                    $___dseSubjectScoreArr = $this->convertDseLevelToScore($___dseSubjectResultArr);
                    
                    $____compulsoryScoreArr = array();
                    $____electiveScoreArr = array();
                    foreach ((array) $___dseSubjectScoreArr as $____subjectID => $____score) {
                        // separate compulsory & elective here
                        if (in_array($____subjectID, (array) $chiEngIDArr)) {
                            // chi & eng here
                            $____compulsoryScoreArr[$____subjectID] = $____score;
                        } else {
                            // elective
                            $____electiveScoreArr[$____subjectID] = $____score;
                        }
                    }
                    arsort($____compulsoryScoreArr);
                    arsort($____electiveScoreArr);
                    
                    $conditions = '2,2';
                    $elective_conditions = '2,2,2';
                    $compulConditionArr = explode(',', $conditions);
                    $electiveConditionArr = explode(',', $elective_conditions);
                    $compulConditionCountArr = array_count_values($compulConditionArr);
                    $electiveConditionCountArr = array_count_values($electiveConditionArr);
                    
                    // initialize condition checking Arr
                    $compulCondition = array();
                    $electiveCondition = array();
                    foreach ((array) $compulConditionCountArr as $_lv => $_number) {
                        $compulCondition[$_lv] = array();
                        for ($i = 0; $i < $_number; $i ++) {
                            $compulCondition[$_lv][] = '';
                        }
                    }
                    foreach ((array) $electiveConditionCountArr as $_lv => $_number) {
                        $electiveCondition[$_lv] = array();
                        for ($i = 0; $i < $_number; $i ++) {
                            $electiveCondition[$_lv][] = '';
                        }
                    }
                    
                    // prepare checking for compulsory subject requirement
                    foreach ((array) $____compulsoryScoreArr as $_subjectID => $_dseScore) {
                        foreach ((array) $compulConditionCountArr as $_lv => $_number) {
                            if (in_array('', (array) $compulCondition[$_lv])) {
                                $firstNotFilledKey = array_search('', $compulCondition[$_lv]);
                                // $thisLvScore = $libpf_exam->getDSEScore($_lv);
                                $thisLvScore = $_lv;
                                if ($_dseScore >= $thisLvScore) {
                                    // $compulCondition[$_lv][$firstNotFilledKey] = $_subjectID.'~~~~~~'.$_dseScore;
                                    $compulCondition[$_lv][$firstNotFilledKey] = $_subjectID;
                                    break;
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                    
                    // prepare checking for elective subject requirement
                    foreach ((array) $____electiveScoreArr as $_dseScore) {
                        foreach ((array) $electiveConditionCountArr as $_lv => $_number) {
                            if (in_array('', (array) $electiveCondition[$_lv])) {
                                $firstNotFilledKey = array_search('', $electiveCondition[$_lv]);
                                // $thisLvScore = $libpf_exam->getDSEScore($_lv);
                                $thisLvScore = $_lv;
                                if ($_dseScore >= $thisLvScore) {
                                    $electiveCondition[$_lv][$firstNotFilledKey] = $_dseScore;
                                    break;
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                    // debug_pr($compulCondition);
                    // debug_pr($electiveCondition);
                    
                    $meetCoreRequirement = true;
                    foreach ((array) $compulConditionCountArr as $_lv => $_number) {
                        if (in_array('', $compulCondition[$_lv])) {
                            $meetCoreRequirement = false;
                        }
                    }
                    $meetElectiveRequirement = true;
                    foreach ((array) $electiveConditionCountArr as $_lv => $_number) {
                        if (in_array('', $electiveCondition[$_lv])) {
                            $meetElectiveRequirement = false;
                        }
                    }
                    if ($meetCoreRequirement && $meetElectiveRequirement) {
                        $resultCount[$__academicYearID]['CE22222'] ++;
                    }
                }
            }
            // //////////////////////// count for 22222 include Chi & Eng end //////////////////////
            
            // //////////////////////// count for any 22222 start //////////////////////
            foreach ((array) $dataAssoc as $__academicYearID => $__studentResultAssoc) {
                // 22222 = chi eng 22 + elective 222
                $resultCount[$__academicYearID]['22222'] = 0;
                foreach ((array) $__studentResultAssoc as $___studentID => $___dseSubjectResultArr) {
                    $___dseSubjectScoreArr = $this->convertDseLevelToScore($___dseSubjectResultArr);
                    // arsort($___dseSubjectScoreArr);
                    rsort($___dseSubjectScoreArr);
                    array_splice($___dseSubjectScoreArr, 5);
                    // if last subject is level 2 or above = meeting requirement
                    if ($___dseSubjectScoreArr[4] >= 2) {
                        $resultCount[$__academicYearID]['22222'] ++;
                    }
                }
            }
            // ////////////////////// count for any 22222 end //////////////////////
            
            // ///////////////////// calculating percentage start ///////////////////////
            foreach ((array) $dataAssoc as $__academicYearID => $__studentResultAssoc) {
                $numStudent = count($__studentResultAssoc);
                $resultCount[$__academicYearID]['totalNum'] = $numStudent;
                foreach ((array) $resultCount[$__academicYearID] as $_type => $_num) {
                    $resultPercentage[$__academicYearID][$_type] = number_format(($_num / $numStudent) * 100, 1);
                }
            }
            // ///////////////////// calculating percentage end ///////////////////////
            
            $rowCount = 0;
            $tmpArr = array();
            foreach ((array) $resultPercentage as $_academicYearID => $_resultArr) {
                $tmpArr[$rowCount]['name'] = $academicYearInfoAssoArr[$_academicYearID]['YearNameEN'];
                foreach ((array) $_resultArr as $__resultType => $__percentage) {
                    $tmpArr[$rowCount]['data'][] = floatval($__percentage);
                }
                $rowCount ++;
            }
            $dataArr['data']['highChart']['percent'] = $tmpArr;
            // $dataArr['data']['Yaxis']['percent'] = $Lang['SDAS']['DSEUniversity']['HighChart']['Yaxis']['Percentage'];
            
            $rowCount = 0;
            $tmpArr = array();
            foreach ((array) $resultCount as $_academicYearID => $_resultArr) {
                $tmpArr[$rowCount]['name'] = $academicYearInfoAssoArr[$_academicYearID]['YearNameEN'];
                foreach ((array) $_resultArr as $__resultType => $__num) {
                    $tmpArr[$rowCount]['data'][] = $__num;
                }
                $rowCount ++;
            }
            
            $dataArr['data']['highChart']['number'] = $tmpArr;
            // $dataArr['data']['Yaxis']['number'] = $Lang['SDAS']['DSEUniversity']['HighChart']['Yaxis']['Number'];
            // Chart x-axis
            // $dataArr['data']['rangeNameArr'] = array_values($Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']);
            // $dataArr['data']['Title'] = $Lang['SDAS']['DSEUniversity']['HighChart']['Title'];
            unset($tmpArr);
            
            // rearrange for table
            $tmpResultArr = array();
            $tmpArr = array();
            foreach ($resultCount as $academicYearID => $ary) {
                $tmpArr = $ary;
                $tmpArr['academicYearID'] = $academicYearID;
                $tmpResultArr[] = $tmpArr;
            }
            
            // if(empty($tmpResultArr)){
            // $errorObj = array($Lang['SDAS']['NoRecord']);
            // }
            
            $rowAry = array(
                '55',
                '5',
                '44',
                '4',
                '33',
                '3',
                'coreCount',
                '22',
                '2',
                'core22222',
                'CE22222',
                '22222',
                'totalNum'
            );
            $resultArr = array();
            foreach ($rowAry as $key) {
                $tmpArr = array();
                $tmpArr['rowName'] = $Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis'][$key];
                foreach ($tmpResultArr as $_key => $__dataAry) {
                    ($usingExactNum) ? $tmpArr[$__dataAry['academicYearID'] . '_num'] = $__dataAry[$key] : '';
                    ($usingPercent) ? $tmpArr[$__dataAry['academicYearID'] . '_percent'] = $resultPercentage[$__dataAry['academicYearID']][$key] : '';
                }
                $resultArr[] = $tmpArr;
            }
            
            $dataArr['data']['table'] = $resultArr;
            // $dataArr['error'] = $errorObj;
            // Academic Year Ary for table
            $academicYearAry = array();
            foreach ($existingAcademicYearIDAry as $ayID) {
                $academicYearAry[$ayID] = $academicYearInfoAssoArr[$ayID];
            }
            
            if (in_array('Q', $parDisplayArr))
                $displayByQ = true;
            if (in_array('P', $parDisplayArr))
                $displayByP = true;
            $displayColNum = count($parDisplayArr);
            
            $html = '';
            $html .= '<div class="chart_tables">';
            $html .= '<table class="common_table_list_v30 view_table_list_v30 resultTable">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th rowspan="2">' . '' . '</th>';
            foreach ((array) $academicYearIDArr as $_academicYearID) {
                if (isset($resultCount[$_academicYearID])) {
                    $html .= '<th colspan="' . $displayColNum . '">' . $academicYearInfoAssoArr[$_academicYearID][Get_Lang_Selection('YearNameB5', 'YearNameEN')] . '</th>';
                }
            }
            $html .= '</tr>';
            $html .= '<tr>';
            foreach ((array) $academicYearIDArr as $_academicYearID) {
                if (isset($resultCount[$_academicYearID])) {
                    if ($displayByQ)
                        $html .= '<th>' . $Lang['SDAS']['ColumnNumberShort'] . '</th>';
                    if ($displayByP)
                        $html .= '<th>' . $Lang['SDAS']['ColumnPercentShort'] . '</th>';
                }
            }
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            foreach ((array) $rowAry as $_rowName) {
                $html .= '<tr>';
                $html .= '<td>' . $Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis'][$_rowName] . '</td>';
                foreach ((array) $academicYearIDArr as $_academicYearID) {
                    if (isset($resultCount[$_academicYearID])) {
                        $_num = $resultCount[$_academicYearID][$_rowName];
                        $_percent = $resultPercentage[$_academicYearID][$_rowName];
                        if ($displayByQ)
                            $html .= '<td>' . $_num . '</td>';
                        if ($displayByP)
                            $html .= '<td>' . $_percent . '</td>';
                    }
                }
                $html .= '</tr>';
            }
            $html .= '</tbody>';
            $html .= '</table>';
            $html .= '</div>';
            
            $xAxisArr = array();
            foreach ((array) $Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis'] as $lang) {
                $xAxisArr[] = $lang;
            }
            
            if ($displayByQ)
                $highchart = $this->getHighChartJs_GroupedBarChart($dataArr['data']['highChart']['number'], $xAxisArr, $targetDivID = 'BarChart', $Lang['SDAS']['DSEUniversity']['Title'] . $Lang['SDAS']['TitleNumber'], $Lang['SDAS']['DSEstat']['number'], $mode = '');
            if ($displayByP)
                $highchart2 = $this->getHighChartJs_GroupedBarChart($dataArr['data']['highChart']['percent'], $xAxisArr, $targetDivID = 'BarChartPercent', $Lang['SDAS']['DSEUniversity']['Title'] . $Lang['SDAS']['TitlePercent'], $Lang['SDAS']['DSEstat']['percent'], $mode = '%', 'chartDataB');
            
            $script = '<script>' . '$("#BarChart").html("");' . '$("#BarChartPercent").html("");' . $highchart . '' . $highchart2 . '</script>';
            return $script . $html;
        }

        private $chiEngArr = array();

        public function getChiEngSubjectID()
        {
            if (empty($this->chiEngArr)) {
                $sql = "SELECT
						RecordID as SubjectID,
						CODEID
						FROM ASSESSMENT_SUBJECT
						WHERE CODEID IN ('080','165') AND (CMP_CODEID IS NULL OR CMP_CODEID = '') AND RecordStatus = 1
						ORDER BY CODEID";
                return $this->returnResultSet($sql);
            } else {
                return $this->chiEngArr;
            }
        }

        public function getBestStudent($AcademicYear, $configArr)
        {
            global $Lang, $linterface;
            
            $configBestNSubject = $configArr['NumSubject'];
            $configCondition = $configArr['scoreConfig'];
            $configTopStudent = $configArr['TopStudentScore'];
            
            // $examDataArr = $this->getExamData($AcademicYearArr, EXAMID_HKDSE, $subjectID = '', $schoolCode, $excludeArr = '', true, '', true, 'A' );
            // $dataAssoc = BuildMultiKeyAssoc($examDataArr, array('AcademicYearID','SchoolCode','StudentID'), array(), 0, 1);
            $examDataArr = $this->getExamData($AcademicYear, EXAMID_HKDSE, $subjectID = '', $UserID = '', $excludeCmpSubject = true, true);
            $dataAssoc = BuildMultiKeyAssoc($examDataArr, array(
                'AcademicYearID',
                'StudentID'
            ), array(), 0, 1);
            
            $studentsBestNScore = array();
            $nSubjectnScoreSortingStringArr = array();
            $sixSubjectStringArr = array();
            foreach ((array) $dataAssoc as $_academicYearID => $__studentDseArr) {
                foreach ((array) $__studentDseArr as $___studentID => $___subjectResultArr) {
                    // for best score of 4 compulsory + 2 elective
                    $subejctScoreAssoc = BuildMultiKeyAssoc($___subjectResultArr, 'SubjectID', array(
                        'Score'
                    ), 1);
                    $sixSubjectStringArr[$___studentID] = $this->getConditionalSixSubjectScore($subejctScoreAssoc);
                    $sixSubjectStringArr[$___studentID]['numSubject'] = count($___subjectResultArr);
                    $sixSubjectStringArr[$___studentID]['StudentID'] = $___studentID;
                    
                    $studentGotLvArr = Get_Array_By_Key($___subjectResultArr, 'Score');
                    // replace U by 0 , 5* by 6 , 5** by 7
                    $studentGotLvScoreArr = $this->convertDseLevelToScore($studentGotLvArr);
                    rsort($studentGotLvScoreArr);
                    $bestNSubject = array_slice($studentGotLvScoreArr, 0, $configBestNSubject, true);
                    $studentTotalScore = array_sum($bestNSubject);
                    // $result[$_academicYearID][$__schoolCode][$___studentID] = $studentTotalScore;
                    $studentsBestNScore[$___studentID] = $studentTotalScore;
                    
                    // for sorting before print
                    $thisSortingString = $this->getSortingStudentString($bestNSubject);
                    $nSubjectnScoreSortingStringArr[$___studentID] = $thisSortingString;
                }
                arsort($studentsBestNScore);
                
                sortByColumn2($sixSubjectStringArr, 'total', $reverse = 1, 0, 'sortString');
                // $bestFiveStudentArr = array_slice($sixSubjectStringArr, 0, 5, true);
                // unset($sixSubjectStringArr);
                
                $result = array();
                $sortingArrKeyArr = array(
                    7,
                    6,
                    5,
                    4,
                    3,
                    2,
                    1
                );
                $bestStudentCount = 0;
                foreach ((array) $sixSubjectStringArr as $_key => $_studentResult) {
                    $_studentScoreArr = array_combine($sortingArrKeyArr, explode(',', $_studentResult['sortString']));
                    $_studentScoreArr['StudentID'] = $_studentResult['StudentID'];
                    $_studentScoreArr['total'] = $_studentResult['total'];
                    if ($_studentResult['total'] >= $configTopStudent && $_studentResult['numSubject'] >= 6) {
                        $result[] = $_studentScoreArr;
                        $bestStudentCount ++;
                        // if($bestStudentCount >= 5 ){
                        // // check is next student score the same
                        // if($_studentResult['total'] == $sixSubjectStringArr[$_key+1]['total']){
                        // continue;
                        // }
                        // else{
                        // break;
                        // }
                        // }
                    } else {
                        continue;
                    }
                }
                $dataArr['data']['BestStudent'] = $result;
                unset($result);
                
                // N subject N score Top student
                // filter out student meeting the condition and mark down the score
                $studentSortingArr = array();
                foreach ((array) $studentsBestNScore as $_studentID => $_totalScore) {
                    if ($_totalScore >= $configCondition) {
                        $studentSortingArr[$_studentID] = $nSubjectnScoreSortingStringArr[$_studentID];
                        $studentSortingArr[$_studentID]['StudentID'] = $_studentID;
                    } else {
                        break;
                    }
                }
                unset($nSubjectnScoreSortingStringArr);
                sortByColumn2($studentSortingArr, 'total', $reverse = 1, 0, 'sortString');
                
                // prepare result DataArr
                $result = array();
                foreach ((array) $studentSortingArr as $_studentResult) {
                    $_studentScoreArr = array_combine($sortingArrKeyArr, explode(',', $_studentResult['sortString']));
                    $_studentScoreArr['StudentID'] = $_studentResult['StudentID'];
                    $_studentScoreArr['total'] = $_studentResult['total'];
                    $result[] = $_studentScoreArr;
                }
                $dataArr['data']['nPointsInNSubject'] = $result;
                
                // only support one year now break for safety purpose
                break;
            }
            
            $studentIdArr = array_unique(array_merge((array) Get_Array_By_Key($dataArr['data']['BestStudent'], 'StudentID'), (array) Get_Array_By_Key($dataArr['data']['nPointsInNSubject'], 'StudentID')));
            $studentInfoAssoc = BuildMultiKeyAssoc($this->getStudentsInfoByID($studentIdArr, $AcademicYear), 'UserID');
            $studentInfoAssoc_temp = BuildMultiKeyAssoc($this->getStudentsInfoByID($studentIdArr, $AcademicYear), 'UserID');
            $studentInfoAssoc_NoYearClass = BuildMultiKeyAssoc($this->getStudentsInfoByID($studentIdArr), 'UserID');
            foreach ($studentIdArr as $studentID) {
                if ($studentInfoAssoc_temp[$studentID]) {
                    $studentInfoAssoc[$studentID] = $studentInfoAssoc_temp[$studentID];
                } else {
                    $studentInfoAssoc[$studentID] = $studentInfoAssoc_NoYearClass[$studentID];
                }
            }
            $Lang['SDAS']['DSETopStudent']['Top5']['Title'] = str_replace('<!--score-->', $configTopStudent, $Lang['SDAS']['DSETopStudent']['Top5']['Title']);
            $Lang['SDAS']['DSETopStudent']['PointOver24']['Title'][0] = str_replace(array(
                '<!--subject-->',
                '<!--score-->'
            ), array(
                $configBestNSubject,
                $configCondition
            ), $Lang['SDAS']['DSETopStudent']['PointOver24']['Title'][0]);
            $Lang['SDAS']['DSETopStudent']['PointOver24']['Title'][1] = str_replace(array(
                '<!--subject-->',
                '<!--score-->'
            ), array(
                $configBestNSubject,
                $configCondition
            ), $Lang['SDAS']['DSETopStudent']['PointOver24']['Title'][1]);
            
            $html = '';
            $html .= $linterface->GET_NAVIGATION2_IP25($Lang['SDAS']['DSETopStudent']['Table'] . ' 1 - ' . $Lang['SDAS']['DSETopStudent']['Top5']['Title'][0]);
            $html .= '<div class="chart_tables">';
            $html .= '<table class="common_table_list_v30 view_table_list_v30 resultTable">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['ClassName'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['ClassNumber'] . '</th>';
            $html .= '<th>' . $Lang['AccountMgmt']['StudentName'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['DSELevel']['5**'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['DSELevel']['5*'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['DSELevel']['5'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['DSELevel']['4'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['DSELevel']['3'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['DSELevel']['2'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['DSELevel']['1'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['TotalScore'] . '</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            if (count($dataArr['data']['BestStudent']) > 0) {
                foreach ((array) $dataArr['data']['BestStudent'] as $_studentResultArr) {
                    $thisInfo = $studentInfoAssoc[$_studentResultArr['StudentID']];
                    $displayInfo = $thisInfo['Name'] . ' (' . $thisInfo['ClassName'] . '-' . $thisInfo['ClassNumber'] . ')';
                    $html .= '<tr>';
                    // $html .= '<td>'.$displayInfo.'</td>';
                    $html .= '<td>' . $thisInfo['ClassName'] . '</td>';
                    $html .= '<td>' . $thisInfo['ClassNumber'] . '</td>';
                    $html .= '<td>' . '<a href="javascript:void(0);" data-studentID="' . $_studentResultArr['StudentID'] . '" data-academicYear="' . $AcademicYear . '" data-title="' . $displayInfo . '" class="thickbox studentRs">' . $thisInfo['Name'] . '</a>' . '</td>';
                    $html .= '<td>' . $_studentResultArr['7'] . '</td>';
                    $html .= '<td>' . $_studentResultArr['6'] . '</td>';
                    $html .= '<td>' . $_studentResultArr['5'] . '</td>';
                    $html .= '<td>' . $_studentResultArr['4'] . '</td>';
                    $html .= '<td>' . $_studentResultArr['3'] . '</td>';
                    $html .= '<td>' . $_studentResultArr['2'] . '</td>';
                    $html .= '<td>' . $_studentResultArr['1'] . '</td>';
                    $html .= '<td>' . $_studentResultArr['total'] . '</td>';
                    $html .= '</tr>';
                }
                $html .= '<td colspan="11">' . $Lang['SDAS']['DSEstat']['Totalnumber'] . ': ' . count($dataArr['data']['BestStudent']) . '</td>';
            } else {
                $html .= '<tr>';
                $html .= '<td colspan="11" style="text-align:center;">' . $Lang['SDAS']['DSETopStudent']['Top5']['Title'][1] . '</td>';
                $html .= '</tr>';
            }
            $html .= '</tbody>';
            $html .= '</table>';
            $html .= '</div>';
            $html .= '<br><br>';
            
            $html .= $linterface->GET_NAVIGATION2_IP25($Lang['SDAS']['DSETopStudent']['Table'] . ' 2 - ' . $Lang['SDAS']['DSETopStudent']['PointOver24']['Title'][0]);
            $html .= '<div class="chart_tables">';
            $html .= '<table class="common_table_list_v30 view_table_list_v30 resultTable">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['ClassName'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['ClassNumber'] . '</th>';
            $html .= '<th>' . $Lang['AccountMgmt']['StudentName'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['DSELevel']['5**'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['DSELevel']['5*'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['DSELevel']['5'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['DSELevel']['4'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['DSELevel']['3'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['DSELevel']['2'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['DSEstat']['DSELevel']['1'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['TotalScore'] . '</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            if (count($dataArr['data']['nPointsInNSubject']) > 0) {
                foreach ((array) $dataArr['data']['nPointsInNSubject'] as $_studentResultArr) {
                    $thisInfo = $studentInfoAssoc[$_studentResultArr['StudentID']];
                    $displayInfo = $thisInfo['Name'] . ' (' . $thisInfo['ClassName'] . '-' . $thisInfo['ClassNumber'] . ')';
                    $html .= '<tr>';
                    $html .= '<td>' . $thisInfo['ClassName'] . '</td>';
                    $html .= '<td>' . $thisInfo['ClassNumber'] . '</td>';
                    $html .= '<td>' . '<a href="javascript:void(0);" data-studentID="' . $_studentResultArr['StudentID'] . '" data-academicYear="' . $AcademicYear . '" data-title="' . $displayInfo . '"class="thickbox studentRs">' . $thisInfo['Name'] . '</a>' . '</td>';
                    $html .= '<td>' . $_studentResultArr['7'] . '</td>';
                    $html .= '<td>' . $_studentResultArr['6'] . '</td>';
                    $html .= '<td>' . $_studentResultArr['5'] . '</td>';
                    $html .= '<td>' . $_studentResultArr['4'] . '</td>';
                    $html .= '<td>' . $_studentResultArr['3'] . '</td>';
                    $html .= '<td>' . $_studentResultArr['2'] . '</td>';
                    $html .= '<td>' . $_studentResultArr['1'] . '</td>';
                    $html .= '<td>' . $_studentResultArr['total'] . '</td>';
                    $html .= '</tr>';
                }
                $html .= '<td colspan="11">' . $Lang['SDAS']['DSEstat']['Totalnumber'] . ': ' . count($dataArr['data']['nPointsInNSubject']) . '</td>';
            } else {
                $html .= '<tr>';
                $html .= '<td colspan="11" style="text-align:center;">' . $Lang['SDAS']['DSETopStudent']['PointOver24']['Title'][1] . '</td>';
                $html .= '</tr>';
            }
            $html .= '</tbody>';
            $html .= '</table>';
            $html .= '</div>';
            return $html;
        }

        public function getSortingStudentString($bestNSubjectScore)
        {
            $separator = ',';
            $scoreCountArr = array_count_values($bestNSubjectScore);
            $scoreSortingArr = array(
                7,
                6,
                5,
                4,
                3,
                2,
                1
            );
            $string = '';
            $totalScoreCount = 0;
            foreach ((array) $scoreSortingArr as $score) {
                
                $numberOfThisScore = isset($scoreCountArr[$score]) ? $scoreCountArr[$score] : 0;
                $totalScoreCount += $numberOfThisScore * $score;
                $string .= $numberOfThisScore;
                
                if ($score == 1) {
                    // $string = $totalScoreCount.','.$string;
                } else {
                    $string .= ',';
                }
            }
            $returnArr['sortString'] = $string;
            $returnArr['total'] = $totalScoreCount;
            return $returnArr;
        }

        public function getConditionalSixSubjectScore($subejctLvAssoc)
        {
            $numElective = 2;
            $compulsoryArr = $this->getDseCompulsorySubjectID();
            $compulsorySubjectIDArr = Get_Array_By_Key($compulsoryArr, 'SubjectID');
            $subejctScoreAssoc = $this->convertDseLevelToScore($subejctLvAssoc);
            arsort($subejctScoreAssoc);
            
            $sixSubjectScoreArr = array();
            // get compulsory subject Score
            foreach ((array) $compulsorySubjectIDArr as $_subjectID) {
                if (isset($subejctScoreAssoc[$_subjectID])) {
                    $sixSubjectScoreArr[] = $subejctScoreAssoc[$_subjectID];
                    unset($subejctScoreAssoc[$_subjectID]);
                }
            }
            // get elective subject score
            $count = 0;
            foreach ((array) $subejctScoreAssoc as $_subjectID => $_score) {
                $sixSubjectScoreArr[] = $_score;
                unset($subejctScoreAssoc[$_subjectID]);
                $count ++;
                if ($count == $numElective) {
                    break;
                }
            }
            
            $resultString = $this->getSortingStudentString($sixSubjectScoreArr);
            return $resultString;
        }

        public function getDseAttendace($AcademicYearArr)
        {
            global $Lang, $objSDAS, $sys_custom;
            
            $subjectIdFilterArr = '';
            if ($sys_custom['SDAS']['DSE']['SubjectPanelOwnSubject']) {
                $accessRight = $objSDAS->getAssessmentStatReportAccessRight();
                $subjectIdFilterArr = '';
                if (! $accessRight['admin'] && count($accessRight['subjectPanel'])) {
                    $subjectIdFilterArr = array_keys($accessRight['subjectPanel']);
                }
            }
            
            // now support single school only
            $mockCountArr = $this->getExamSummary($AcademicYearArr, EXAMID_MOCK, $subjectIdFilterArr, true, true);
            $dseCountArr = $this->getExamSummary($AcademicYearArr, EXAMID_HKDSE, $subjectIdFilterArr, true, true);
            $mockCountAssoc = BuildMultiKeyAssoc($mockCountArr, array(
                'SubjectID',
                'AcademicYearID'
            ), array(
                'count'
            ), 1);
            $dseCountAssoc = BuildMultiKeyAssoc($dseCountArr, array(
                'SubjectID',
                'AcademicYearID'
            ), array(
                'count'
            ), 1);
            $dseAbsCountArr = $this->getDseAbsNumber($AcademicYearArr, EXAMID_HKDSE, $subjectIdFilterArr, true, true);
            $dseAbsCountAssoc = BuildMultiKeyAssoc($dseAbsCountArr, array(
                'SubjectID',
                'AcademicYearID'
            ), array(
                'count'
            ), 1);
            
            $subjectIDMockArr = array_unique(Get_Array_By_Key($mockCountArr, 'SubjectID'));
            $subjectIDDseArr = array_unique(Get_Array_By_Key($dseCountArr, 'SubjectID'));
            $subjectIDArr = array_unique(array_merge((array) $subjectIDMockArr, (array) $subjectIDDseArr));
            
            $subjectNameArr = BuildMultiKeyAssoc($this->getSubjectCodeAndID($subjectIDArr), 'SubjectID', array(
                Get_Lang_Selection('ch_main_name', 'main_name')
            ), 1);
            // $academicYearNameArr = $this->getAcademYearNameById($AcademicYearArr);
            $academicYearInfoAssoArr = BuildMultiKeyAssoc(GetAllAcademicYearInfo(), 'AcademicYearID');
            
            unset($mockCountArr);
            unset($dseCountArr);
            unset($dseAbsCountArr);
            
            $resultArr = array();
            foreach ((array) $subjectIDArr as $_subjectID) {
                $tmpArr = array();
                foreach ((array) $AcademicYearArr as $__academicYearID) {
                    $__enterCount = $mockCountAssoc[$_subjectID][$__academicYearID] != '' ? $mockCountAssoc[$_subjectID][$__academicYearID] : '--';
                    $__appliedCount = $dseCountAssoc[$_subjectID][$__academicYearID] != '' ? $dseCountAssoc[$_subjectID][$__academicYearID] : '--';
                    $__satCount = '--';
                    
                    if (isset($dseAbsCountAssoc[$_subjectID][$__academicYearID])) {
                        $__satCount = $dseCountAssoc[$_subjectID][$__academicYearID] - $dseAbsCountAssoc[$_subjectID][$__academicYearID];
                    } else {
                        $__satCount = $dseCountAssoc[$_subjectID][$__academicYearID];
                    }
                    if ($__satCount == 0 || $__satCount == '') {
                        $__satCount = '--';
                    }
                    
                    $tmpArr[$__academicYearID . '_Enter'] = $__enterCount;
                    $tmpArr[$__academicYearID . '_Applied'] = $__appliedCount;
                    $tmpArr[$__academicYearID . '_Sat'] = $__satCount;
                }
                $tmpArr['rowName'] = $subjectNameArr[$_subjectID];
                $resultArr[] = $tmpArr;
            }
            
            $html = '';
            $html .= '<div class="chart_tables">';
            $html .= '<table class="common_table_list_v30 view_table_list_v30 resultTable">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th rowspan="2">' . $Lang['Header']['Menu']['Subject'] . '</th>';
            foreach ((array) $AcademicYearArr as $_academicYearID) {
                $html .= '<th colspan="3">' . $academicYearInfoAssoArr[$_academicYearID][Get_Lang_Selection('YearNameB5', 'YearNameEN')] . '</th>';
            }
            $html .= '</tr>';
            $html .= '<tr>';
            foreach ((array) $AcademicYearArr as $_academicYearID) {
                $html .= '<th>' . $Lang['SDAS']['DseDropEnterSatReport']['column'][0] . '</th>';
                $html .= '<th>' . $Lang['SDAS']['DseDropEnterSatReport']['column'][1] . '</th>';
                $html .= '<th>' . $Lang['SDAS']['DseDropEnterSatReport']['column'][2] . '</th>';
            }
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            foreach ((array) $resultArr as $_row) {
                $html .= '<tr>';
                $html .= '<td>' . $_row['rowName'] . '</td>';
                foreach ((array) $AcademicYearArr as $_academicYearID) {
                    $html .= '<td>' . $_row[$_academicYearID . '_Enter'] . '</td>';
                    $html .= '<td>' . $_row[$_academicYearID . '_Applied'] . '</td>';
                    $html .= '<td>' . $_row[$_academicYearID . '_Sat'] . '</td>';
                }
                $html .= '</tr>';
            }
            $html .= '</tbody>';
            $html .= '</table>';
            $html .= '</div>';
            return $html;
        }

        public function getExamSummary($academicYearID, $examID, $subjectID = '', $excludeCmpSubject = false, $excludeUnmatchSubject = false)
        {
            global $intranet_db;
            
            if ($academicYearID != '') {
                $academicYear_conds = " AND a.AcademicYearID IN ('" . implode("','", (array) $academicYearID) . "') ";
            }
            if ($examID != '') {
                $exam_conds = " AND a.ExamID = '" . $examID . "' ";
            }
            if ($subjectID != '') {
                $subject_conds = " AND a.SubjectID IN ('" . implode("','", (array) $subjectID) . "') ";
            }
            if ($excludeArr != '') {
                $exclude_conds = " AND a.Grade not IN ('" . implode("','", (array) $excludeArr) . "')";
            }
            if ($excludeCmpSubject) {
                // $excludeCmp_conds = " AND (ParentSubjectCode IS NULL OR ParentSubjectCode = '')";
                $excludeCmp_conds = " AND a.SubjectID NOT IN ('" . implode("','", (array) $this->getCmpSubjectIDArr()) . "')";
            }
            if ($excludeUnmatchSubject) {
                $excludeUnmatchSubject_cond = " AND a.SubjectID != '-999' ";
            }
            
            switch ($examID) {
                case EXAMID_HKAT:
                    $tableName = 'EXAM_STUDENT_SCORE';
                    $field = 'a.Score';
                    break;
                case EXAMID_MOCK:
                    $tableName = 'EXAM_STUDENT_SCORE';
                    $field = 'a.Score, a.Grade ';
                    $extra_conds = ' AND a.Score > 0';
                    break;
                case EXAMID_HKDSE:
                    $tableName = 'EXAM_DSE_STUDENT_SCORE';
                    $field = 'a.Grade as Score, a.Grade as Level, a.SubjectCode , a.ParentSubjectCode';
                    $catA_only_conds = " AND SubjectCategory = 'A' ";
                    $dse_conds = " AND a.isArchive = 0 ";
                    break;
            }
            
            $sql = "SELECT
						AcademicYearID, SubjectID,  count(*) as count
					FROM
						{$intranet_db}.$tableName AS a
					WHERE
						1
						$exam_conds
						$academicYear_conds
						$subject_conds
						$exclude_conds
						$excludeCmp_conds
						$excludeUnmatchSubject_cond
						$extra_conds
						$catA_only_conds
						$dse_conds
					GROUP BY
						AcademicYearID, SubjectID
			";
            return $this->returnResultSet($sql);
        }

        public function getDseAbsNumber($academicYearID, $examID, $subjectID = '', $excludeCmpSubject = false, $excludeUnmatchSubject = false)
        {
            global $intranet_db;
            if ($academicYearID != '') {
                $academicYear_conds = " AND a.AcademicYearID IN ('" . implode("','", (array) $academicYearID) . "') ";
            }
            if ($examID != '') {
                $exam_conds = " AND a.ExamID = '" . $examID . "' ";
            }
            if ($subjectID != '') {
                $subject_conds = " AND a.SubjectID IN ('" . implode("','", (array) $subjectID) . "') ";
            }
            if ($excludeArr != '') {
                $exclude_conds = " AND a.Grade not IN ('" . implode("','", (array) $excludeArr) . "')";
            }
            if ($excludeCmpSubject) {
                // only for dse
                $excludeCmp_conds = " AND (ParentSubjectCode IS NULL OR ParentSubjectCode = '')";
            }
            if ($excludeUnmatchSubject) {
                $excludeUnmatchSubject_cond = " AND a.SubjectID != '-999' ";
            }
            
            switch ($examID) {
                case EXAMID_HKDSE:
                    $tableName = 'EXAM_DSE_STUDENT_SCORE';
                    $field = 'a.Grade as Score, a.Grade as Level, a.SubjectCode , a.ParentSubjectCode';
                    $dse_conds = " AND a.isArchive = 0 ";
                    break;
                default:
                    die();
                    break;
            }
            
            $sql = "SELECT
						AcademicYearID, SubjectID,  count(*) as count
					FROM
						{$intranet_db}.$tableName AS a
					WHERE
						Grade = 'X'
						$exam_conds
						$academicYear_conds
						$subject_conds
						$exclude_conds
						$excludeCmp_conds
						$excludeUnmatchSubject_cond
						$extra_conds
						$dse_conds
					GROUP BY
						AcademicYearID, SubjectID
			";
            
            return $this->returnResultSet($sql);
        }

        public function getHighChartJs_GroupedBarChart($seriesAssocArr, $groupNameArr, $targetDivID = 'chart', $chartTitle = 'Title', $yAxis = 'yAxis', $mode = '', $jsObjName = 'chartData')
        {
            global $intranet_root;
            include_once ($intranet_root . "/includes/json.php");
            $json = new JSON_obj();
            
            $chartData = $json->encode($seriesAssocArr);
            
            $display = '.0f';
            if ($mode == '%') {
            	$max = ' max : 100, ';
            	$display = '.1f';
            }
            
            $highChart = '';
            $highChart .= '
					$("#' . $targetDivID . '").highcharts({
		        chart: {
		            type: \'column\'
		        },
		        title: {
		            text: \'' . $chartTitle . '\'
		        },
		        xAxis: {
		            categories: ' . $json->encode($groupNameArr) . ',
		            crosshair: true
		        },
		        yAxis: {
		            min: 0,
            		' . $max . '
		            title: {
		                text: \'' . $yAxis . '\'
		            }
		        },
		        tooltip: {
		            headerFormat: \'<span style="font-size:10px">{point.key}</span><table>\',
		            pointFormat: \'<tr><td style="color:{series.color};padding:0">{series.name}: </td>\' +
		                \'<td style="padding:0"><b>{point.y:'.$display.'}'.$mode.'</b></td></tr>\',
		            footerFormat: \'</table>\',
		            shared: true,
		            useHTML: true
		        },
		        plotOptions: {
		            column: {
		                pointPadding: 0.2,
		                borderWidth: 0
		            }
		        },
		        credits: {
		           	enabled: false
		        },
		        series: ' . $jsObjName . 'Obj
		    });
					';
            
            $js = '';
            $js .= '
					var ' . $jsObjName . ' = "' . addslashes($chartData) . '";
						' . $jsObjName . 'Obj = JSON.parse(' . $jsObjName . ');
							' . $highChart . '
					';
            
            return $js;
        }

        public function getHighChartJs_StackedBarChart($seriesAssocArr, $groupNameArr, $targetDivID = 'chart', $chartTitle = 'Title', $yAxis = 'yAxis', $mode = '', $jsObjName = 'chartData')
        {
            global $intranet_root;
            include_once ($intranet_root . "/includes/json.php");
            $json = new JSON_obj();
            
            $chartData = $json->encode($seriesAssocArr);
            
            if ($mode == '%') {
                $max = ' max : 100, ';
                $unit = '%';
            }
            
            $highChart = '';
            $highChart .= '
					$("#' . $targetDivID . '").highcharts({
		        chart: {
		            type: \'column\'
		        },
		        title: {
		            text: \'' . $chartTitle . '\'
		        },
		        xAxis: {
		            categories: ' . $json->encode($groupNameArr) . ',
		        },
		        yAxis: {
		            reversedStacks : false,
		            min: 0,
            		' . $max . '
		            title: {
		                text: \'' . $yAxis . '\'
		            },
                		stackLabels: {
		                enabled: false,
		                style: {
		                    fontWeight: \'bold\',
		                    color: (Highcharts.theme && Highcharts.theme.textColor) || \'gray\'
		                }
		            }
		        },
		        tooltip: {
		            shared: true,
		            headerFormat: \'<b>{point.x}</b><br/>\',
		            pointFormat: \'{series.name}: {point.y}\'+ \'' . $unit . '\' +\'<br>\'
		        },
		        plotOptions: {
		            column: {
	                stacking: \'normal\',
	                dataLabels: {
	                    enabled: false,
	                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || \'white\',
	                    style: {
	                        textShadow: \'0 0 3px black\'
	                    }
	                }
	            }
		        },
		        credits: {
		           	enabled: false
		        },
		        series: ' . $jsObjName . 'Obj
		    });
					';
            
            $js = '';
            $js .= '
					var ' . $jsObjName . ' = "' . addslashes($chartData) . '";
						' . $jsObjName . 'Obj = JSON.parse(' . $jsObjName . ');
							' . $highChart . '
					';
            
            return $js;
        }

        public function getDsePerformanceTrend($AcademicYearArr, $parDseArr, $parDisplayArr, $SubjectIDArr = array())
        {
            global $Lang;
            
            $arr = array(
                'X',
                'U',
                '1',
                '2',
                '3',
                '4',
                '5',
                '5*',
                '5**'
            );
            // $academicYearInfoAssocArr = $this->getAcademYearNameById($AcademicYearArr);
            $academicYearInfoAssocArr = BuildMultiKeyAssoc(GetAllAcademicYearInfo(), 'AcademicYearID');
            // unset unselected year info
            $xAxisArr = array();
            foreach ((array) $academicYearInfoAssocArr as $key => $yearArr) {
                if (! in_array($yearArr['AcademicYearID'], $AcademicYearArr)) {
                    unset($academicYearInfoAssocArr[$key]);
                } else {
                    $xAxisArr[] = $yearArr['YearNameEN'];
                }
            }
            // $examDataArr = $this->getExamData($AcademicYearArr, $examID, $subjectID, $schoolCode, $excludeArr='', true, '', true, 'A');
            $examDataArr = $this->getExamData($AcademicYearArr, EXAMID_HKDSE, $SubjectIDArr, $UserID = '', $excludeCmpSubject = true, true);
            
            if (count($parDseArr) > 0) {
                foreach ((array) $parDseArr as $levelKey) {
                    $dseTargetArr[] = $this->getDseLevel($levelKey);
                }
            }
            $summaryArr = array();
            $_countAssocArr = array();
            $examDataAssocArr = BuildMultiKeyAssoc($examDataArr, 'AcademicYearID', array(), 0, 1);
            // debug_pr($examDataAssocArr);
            foreach ((array) $academicYearInfoAssocArr as $_academicYearID => $_yearName) {
                $_dataArr = $examDataAssocArr[$_academicYearID];
                $_yearLevelCountArr = array();
                if (! empty($_dataArr)) {
                    $_total = count($_dataArr);
                    $_yearLevel = Get_Array_By_Key($_dataArr, 'Score');
                    $_yearLevelCountArr = array_count_values($_yearLevel);
                    $_yearLevelCountArr['total'] = $_total;
                    foreach ((array) $arr as $_lvName) {
                        if (! isset($_yearLevelCountArr[$_lvName])) {
                            $_yearLevelCountArr[$_lvName] = 0;
                        }
                    }
                } else {
                    $_total = 0;
                    $_yearLevelCountArr['total'] = $_total;
                    foreach ((array) $arr as $_lvName) {
                        if (! isset($_yearLevelCountArr[$_lvName])) {
                            $_yearLevelCountArr[$_lvName] = $_total;
                        }
                    }
                }
                $_yearLevelCountArr['academicYear'] = $_yearName['YearNameEN'];
                $_countAssocArr[] = $_yearLevelCountArr;
                
                $_percentAssocArr = array();
                foreach ((array) $_countAssocArr as $_countArr) {
                    if ($_countArr['total'] > 0) {
                        $__total = $_countArr['total'];
                        $_yearLevelPercentArr = array();
                        foreach ((array) $dseTargetArr as $__lv) {
                            
                            if (isset($_countArr[$__lv])) {
                                // this level is set
                                $__thisLvCount = $_countArr[$__lv];
                                $__thisLvPercent = (float) number_format($__thisLvCount / $__total * 100, 1);
                                $__thisLvFloat = $__thisLvCount / $__total;
                            } else {
                                $__thisLvCount = 0;
                                $__thisLvPercent = 0;
                                $__thisLvFloat = 0;
                            }
                            $_yearLevelPercentArr[$__lv] = $__thisLvPercent;
                        }
                        $_yearLevelPercentArr['total'] = (float) number_format($__total / $__total * 100, 1);
                    } else {
                        $_total = 0;
                        $_yearLevelPercentArr['total'] = $_total;
                        $_yearLevelPercentArr['X'] = $_total;
                        $_yearLevelPercentArr['U'] = $_total;
                        $_yearLevelPercentArr['1'] = $_total;
                        $_yearLevelPercentArr['2'] = $_total;
                        $_yearLevelPercentArr['3'] = $_total;
                        $_yearLevelPercentArr['4'] = $_total;
                        $_yearLevelPercentArr['5'] = $_total;
                        $_yearLevelPercentArr['5*'] = $_total;
                        $_yearLevelPercentArr['5**'] = $_total;
                    }
                    $_yearLevelPercentArr['academicYear'] = $_countArr['academicYear'];
                    $_percentAssocArr[] = $_yearLevelPercentArr;
                }
            }
            
            $count = 0;
            foreach ((array) $dseTargetArr as $_key => $_lv) {
                $display = $Lang['SDAS']['DSEstat']['DSELevel'][$_lv];
                $highChartArr[$count]['name'] = $display;
                $highChart2Arr[$count]['name'] = $display;
                foreach ((array) $_countAssocArr as $_yearResultArr) {
                    $highChartArr[$count]['data'][] = $_yearResultArr[$_lv];
                }
                foreach ((array) $_percentAssocArr as $_yearResultArr) {
                    $highChart2Arr[$count]['data'][] = $_yearResultArr[$_lv];
                }
                $count ++;
            }
            
            if (in_array('Q', $parDisplayArr))
                $displayByQ = true;
            if (in_array('P', $parDisplayArr))
                $displayByP = true;
            $displayColNum = count($parDisplayArr);
            
            $html = '';
            $html .= '<div class="chart_tables">';
            $html .= '<table class="common_table_list_v30 view_table_list_v30 resultTable">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th rowspan="2">' . $Lang['General']['SchoolYear'] . '</th>';
            foreach ((array) $dseTargetArr as $_lv) {
                $displayLv = $Lang['SDAS']['DSEstat']['DSELevel'][$_lv];
                $html .= '<th colspan="' . $displayColNum . '">' . $displayLv . '</th>';
            }
            $html .= '</tr>';
            $html .= '<tr>';
            foreach ((array) $dseTargetArr as $_lv) {
                if ($displayByQ)
                    $html .= '<th>' . $Lang['SDAS']['ColumnNumberShort2'] . '</th>';
                if ($displayByP)
                    $html .= '<th>' . $Lang['SDAS']['ColumnPercentShort'] . '</th>';
            }
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            foreach ((array) $_countAssocArr as $key => $resultArr) {
                $html .= '<tr>';
                $html .= '<td>' . $resultArr['academicYear'] . '</td>';
                foreach ((array) $dseTargetArr as $_lv) {
                    if ($displayByQ)
                        $html .= '<td>' . $resultArr[$_lv] . '</td>';
                    if ($displayByP)
                        $html .= '<td>' . $_percentAssocArr[$key][$_lv] . '</td>';
                }
                $html .= '</tr>';
            }
            $html .= '</tbody>';
            $html .= '</table>';
            $html .= '</div>';
            //
            if ($displayByQ)
                $highchart = $this->getHighChartJs_StackedBarChart($highChartArr, $xAxisArr, $targetDivID = 'BarChart', $Lang['SDAS']['DsePerformanceTrend']['Title'] . $Lang['SDAS']['TitleNumber2'], $Lang['SDAS']['DSEstat']['number2'], $mode = '');
            if ($displayByP)
                $highchart2 = $this->getHighChartJs_StackedBarChart($highChart2Arr, $xAxisArr, $targetDivID = 'BarChartPercent', $Lang['SDAS']['DsePerformanceTrend']['Title'] . $Lang['SDAS']['TitlePercent'], $Lang['SDAS']['DSEstat']['percent'], $mode = '%', 'chartDataB');
            
            $script = '<script>' . '$("#BarChart").html("");' . '$("#BarChartPercent").html("");' . $highchart . $highchart2 . '</script>';
            
            return $script . $html;
        }

        public function deleteJupasStudentData($academicYearID, $targetUserIDArr = array(), $excludeUserIDArr = array())
        {
            global $intranet_db;
            
            if ($academicYearID != '') {
                if (count($excludeUserID) > 0) {
                    $notInArchived_conds = " AND StudentID not IN('" . implode("','", $excludeUserIDArr) . "') ";
                }
                if (count($targetUserIDArr) > 0) {
                    $include_conds = " AND StudentID IN('" . implode("','", $targetUserIDArr) . "') ";
                }
                
                // delete records in EXAM_STUDENT_SCORE
                $sql = "DELETE FROM {$intranet_db}.JUPAS_STUDENT_OFFER where AcademicYearID = '$academicYearID' $include_conds $notInArchived_conds";
                $success = $this->db_db_query($sql);
                $totalNum = $this->db_affected_rows();
            }
            return $totalNum;
        }

        public function insertJupasProgrammeData($insertVarArr)
        {
            global $intranet_db;
            
            if (count($insertVarArr) > 50) {
                $affected_rows = 0;
                $insertChunkArr = array_chunk($insertVarArr, 50);
                foreach ((array) $insertChunkArr as $_insertVarArr) {
                    $result = $this->insertJupasProgrammeData($_insertVarArr);
                    $affected_rows = $affected_rows + $result;
                }
                return $affected_rows;
            } else {
                $sql = "INSERT INTO
				{$intranet_db}.JUPAS_PROGRAMME
				( JupasCode , ProgrammeFullName, Institution, ProgrammeType
				/*InputDate, InputBy, ModifiedDate, ModifiedBy*/)
				Values
				" . implode(',', $insertVarArr) . "";
                
                $success = $this->db_db_query($sql);
                if ($success == 0) {
                    return 0;
                } else {
                    return $this->db_affected_rows();
                }
            }
        }

        public function getJupasYearWithRecords()
        {
            global $intranet_db;
            
            $sql = "SELECT
			distinct(jso.academicYearID)
			FROM
			{$intranet_db}.JUPAS_STUDENT_OFFER as jso 
			INNER JOIN ACADEMIC_YEAR as ay ON ay.AcademicYearID = jso.AcademicYearID
			INNER JOIN ACADEMIC_YEAR_TERM as ayt ON ayt.AcademicYearID = ay.AcademicYearID
			ORDER BY
			ayt.TermStart desc";
            return $this->returnVector($sql);
        }

        public function insertJupasStudentData($insertVarArr)
        {
            global $intranet_db;
            
            if (count($insertVarArr) > 50) {
                $affected_rows = 0;
                $insertChunkArr = array_chunk($insertVarArr, 50);
                foreach ((array) $insertChunkArr as $_insertVarArr) {
                    $result = $this->insertJupasStudentData($_insertVarArr);
                    $affected_rows = $affected_rows + $result;
                }
                return $affected_rows;
            } else {
                $sql = "INSERT INTO
				{$intranet_db}.JUPAS_STUDENT_OFFER
				( AcademicYearID , StudentID,
				JupasAppNo, JupasCode,
				Institution, Band, Funding,
				OfferRound , AcceptanceStatus,
				InputDate, InputBy, ModifiedDate, ModifiedBy)
				Values
				" . implode(',', $insertVarArr) . "";
                
                $success = $this->db_db_query($sql);
                
                if ($success == 0) {
                    return 0;
                } else {
                    return $this->db_affected_rows();
                }
            }
        }

        public function getAllJupasProgramme()
        {
            $sql = "SELECT 
					 JupasCode 
					FROM 
					 JUPAS_PROGRAMME ";
            return $this->returnVector($sql);
        }

        public function getAllJupasProgrammeInfo()
        {
            $sql = "SELECT
					 *
					FROM
					 JUPAS_PROGRAMME ";
            return $this->returnResultSet($sql);
        }

        public function getStudentJupasOfferByYear($AcademicYearID)
        {
            $sql = "SELECT
					 JupasAppNo,  JupasCode, Institution, Band, Funding, OfferRound, AcceptanceStatus, StudentID
					FROM
					 JUPAS_STUDENT_OFFER 
					WHERE 
					 AcademicYearID = '" . $AcademicYearID . "'";
            return $this->returnResultSet($sql);
        }
        
        public function getStudentJupasOfferByStudentID($StudentID = '')
        {
            $sql = "SELECT
					   jso.StudentID, jso.AcademicYearID, jso.JupasAppNo,  jso.JupasCode, jso.Institution,jp.ProgrammeFullName                 
					FROM
					 JUPAS_STUDENT_OFFER as jso
                     INNER JOIN JUPAS_PROGRAMME AS jp ON jp.JupasCode = jso.JupasCode AND jp.Institution = jso.Institution
					WHERE
					 jso.StudentID  = '" . $StudentID. "'";
            return $this->returnResultSet($sql);
        }

        public function getJupasProgTypeByName($string)
        {
            global $JUPAS_CONFIG;
            
            $degreeIdentifier = array(
                'Bachelor',
                'BACHELOR',
                '(Hons)',
                '(Honours)',
                'BA',
                'BBA',
                'BSc',
                'BEng',
                'BCM',
                'BCOMM',
                'BPharm',
                'BSocSc',
                'BEd',
                'BSW'
            );
            $degreeRegx = '/' . implode('|', $degreeIdentifier) . '/';
            $subDegreeIdentifier = array(
                'Higher Diploma',
                'Associate',
                'Diploma',
                'HD',
                'ASc'
            );
            $subDegreeRegx = '/' . implode('|', $subDegreeIdentifier) . '/';
            
            $isDegree = preg_match($degreeRegx, $string);
            if (! $isDegree) {
                $isSubDegree = preg_match($subDegreeRegx, $string);
                if (! $isSubDegree) {
                    // unclassified
                }
            }
            
            if ($isDegree) {
                $type = $JUPAS_CONFIG['ProgType']['DEGREE'];
            } else 
                if ($isSubDegree) {
                    $type = $JUPAS_CONFIG['ProgType']['SUBDEGREE'];
                } else {
                    $type = $JUPAS_CONFIG['ProgType']['UNCLASSIFIED'];
                }
            
            return $type;
        }

        public function getJupasOffer($AcademicYearArr, $excludeNoOffer = false, $excludeNoIdentityStudent = false)
        {
            global $intranet_db;
            
            if ($excludeNoOffer) {
                $offer_conds = " AND offer.JupasCode != -999 ";
            }
            if ($excludeNoIdentityStudent) {
                $student_conds = " AND offer.StudentID != -999 ";
            }
            
            $sql = "SELECT
					offer.AcademicYearID,
					offer.JupasAppNo,
					offer.JupasCode,
					offer.Institution,
					offer.Band,
					offer.Funding,
					offer.OfferRound,
					offer.AcceptanceStatus,
					programme.ProgrammeType,
					offer.StudentID,
					offer.StudentID as IdentityID,
					programme.ProgrammeFullName
					FROM
					{$intranet_db}.JUPAS_STUDENT_OFFER as offer
					LEFT JOIN JUPAS_PROGRAMME as programme ON offer.JupasCode = programme.JupasCode and offer.JupasCode != -999
					WHERE
					offer.AcademicYearID IN ('" . implode("','", (array) $AcademicYearArr) . "')
					$offer_conds
					$student_conds";
            $result = $this->ReturnResultSet($sql);
            return $result;
        }

        public function getJupasProgrammeCodeMapping()
        {
            $sql = "SELECT
					 JupasCode,
					 ProgrammeFullName
					FROM
					 JUPAS_PROGRAMME ";
            return BuildMultiKeyAssoc($this->returnResultSet($sql), 'JupasCode', array(
                'ProgrammeFullName'
            ), 1);
        }
        
        
        public function getJUPASCodeBySchoolNameProgram($UniversityCode = '',$ProgrammeTitle = ''){
            $sql = "SELECT
					  JupasCode
					FROM
					   JUPAS_PROGRAMME
                    WHERE
                       Institution = '" . $UniversityCode. "'
                       AND ProgrammeFullName='".$ProgrammeTitle."'";
            
            return $this->returnVector($sql);
        }
        
        public function UpdateStudentJUPASRecord($StudentID, $AcademicYearID ,$DataAry){
            global $UserID;
            $sql = "
                    UPDATE JUPAS_STUDENT_OFFER
                    SET
                    ";
            foreach($DataAry as $key=>$val)
            {
                $sql .= $key."='".$this->Get_Safe_Sql_Query($val)."', ";
            }
            $sql .= "ModifiedDate = NOW(), ModifiedBy = '$UserID' WHERE StudentID='$StudentID' AND AcademicYearID = '$AcademicYearID'";
            
            
            $success = $this->db_db_query($sql) or die(mysql_error());
            return $success;
        }
        
        
        public function DeleteStudentJUPASRecord($StudentID= ''){
            $sql = "DELETE
					FROM
					   JUPAS_STUDENT_OFFER
                    WHERE
                       StudentID = '" . $StudentID. "'";
            
            $success = $this->db_db_query($sql);
            return $success;
        }
        
        
        public function getStudentExitToRecordByYear($AcademicYearID= '')
        {
            $sql = "SELECT
					   StudentID,FinalChoice,Remark
					FROM
					   STUNDET_EXIT_TO_INFO 
                    WHERE
                       AcademicYearID = '" . $AcademicYearID . "'";
            return $this->returnResultSet($sql);
        }
    
        public function getExitToLastInputDate()
        {
            global $intranet_db;
            $sql = "SELECT ModifiedDate FROM {$intranet_db}.STUNDET_EXIT_TO_INFO ORDER BY ModifiedDate DESC LIMIT 1";
            $row = $this->returnVector($sql);
            $LastModified = $row[0];
            return (($LastModified == '') ? '-' : $LastModified);
        }
        
        
        function Get_User_ExitTo_Info($StudentID='')
        {
            
            $sql = "
            SELECT * FROM STUNDET_EXIT_TO_INFO
            WHERE StudentID = '$StudentID'
            ";
            
            $result = $this->returnArray($sql);
            
            return $result[0];
        }
        
        function getExamYear($examID = ''){
        	if ($examID == '') {
        		$examID = $this->getUsingExamID();
        	}
        	
        	if ($examID == EXAMID_HKDSE) {
        		$tableName = 'EXAM_DSE_STUDENT_SCORE';
        	} elseif ($examID == EXAMID_HKDSE_APPEAL) {
        		$tableName = 'EXAM_DSEAPPEAL_STUDENT_SCORE';
        	} else {
        		$tableName = 'EXAM_STUDENT_SCORE';
        	}
        	
        	$sql =  "
	        	SELECT DISTINCT
	        		exam.AcademicYearID AS AcademicYearID, ay.YearNameEn AS YearNameEn
	        	FROM {$tableName} exam
				INNER JOIN ACADEMIC_YEAR ay
					ON exam.AcademicYearID = ay.AcademicYearID
	        	WHERE
	        		exam.AcademicYearID IS NOT NULL
	        	ORDER BY ay.Sequence
        	";
        	$AcademicYearIDAry = $this->returnArray($sql);
        	return $AcademicYearIDAry;
        }
        
        function Update_User_ExitTo_Info($StudentID='',$DataAry=array())
        {
            global $UserID,$intranet_root,$sys_custom;
            
            $sql = "
            UPDATE STUNDET_EXIT_TO_INFO
            SET
            ";
            foreach($DataAry as $key=>$val)
            {
                $sql .= $key."='".$this->Get_Safe_Sql_Query($val)."', ";
            }
            $sql .= "ModifiedDate=NOW(), ModifiedBy='$UserID' WHERE StudentID='$StudentID'";
            
            
            $success = $this->db_db_query($sql) or die(mysql_error());
            return $success;
        }
        
        function Insert_User_ExitTo_Info($DataAry=array())
        {
            global $UserID,$intranet_root,$sys_custom;
            
            $sql= "INSERT INTO STUNDET_EXIT_TO_INFO
                        (AcademicYearID, StudentID,FinalChoice,Remark ,
                        InputDate,InputBy,ModifiedDate ,ModifiedBy)
                        VALUES
                        ('".$DataAry['AcademicYearID']."','".$DataAry['StudentID']."',
                        '".$DataAry['FinalChoice']."','".$DataAry['Remark']."',
                        NOW(),'".$UserID."' , NOW(),'".$UserID."')";
            
            $success = $this->db_db_query($sql);
            return $success;
        }
        
        function Delete_ExitTo_Info($AcademicYearID= ''){
            $sql = "
                    DELETE FROM STUNDET_EXIT_TO_INFO
                    WHERE AcademicYearID = '$AcademicYearID'";
            
            $success = $this->db_db_query($sql);
            return $success;
        }

        private function getDseScoreByMode($studentResultArr, $mode)
        {
            $compulsoryArr = array(
                'A010',
                'A020',
                'A030',
                'A040'
            );
            
            $studentResultArr = $this->convertDseLevelToScore($studentResultArr);
            
            $compulsoryScoreArr = array();
            $electiveScoreArr = array();
            switch ((string) $mode) {
                case 'B5':
                    // best 5
                    rsort($studentResultArr);
                    $best5Subject = array_slice($studentResultArr, 0, 5, true);
                    $score = array_sum($best5Subject);
                    break;
                case '4C+1E':
                    foreach ((array) $studentResultArr as $_subjectCode => $_score) {
                        // separate compulsory & elective here
                        if (in_array($_subjectCode, (array) $compulsoryArr)) {
                            // core 3322 here
                            $compulsoryScoreArr[$_subjectCode] = $_score;
                        } else {
                            // elective
                            $electiveScoreArr[$_subjectCode] = $_score;
                        }
                    }
                    $complScore = array_sum($compulsoryScoreArr);
                    rsort($electiveScoreArr);
                    $score = $complScore + $electiveScoreArr[0];
                    break;
                case '4C+2E':
                    foreach ((array) $studentResultArr as $_subjectCode => $_score) {
                        // separate compulsory & elective here
                        if (in_array($_subjectCode, (array) $compulsoryArr)) {
                            // core 3322 here
                            $compulsoryScoreArr[$_subjectCode] = $_score;
                        } else {
                            // elective
                            $electiveScoreArr[$_subjectCode] = $_score;
                        }
                    }
                    $complScore = array_sum($compulsoryScoreArr);
                    rsort($electiveScoreArr);
                    $score = $complScore + $electiveScoreArr[0] + $electiveScoreArr[1];
                    break;
            }
            return $score;
        }

        private function getM1M2Level($studentResultArr)
        {
            // $studentResultArr = array( 'HKEAAsubjectCode' => 'Level')
            if ($studentResultArr['A031'] == '') {
                return Get_String_Display($studentResultArr['A032']);
            } else {
                return Get_String_Display($studentResultArr['A031']);
            }
        }

        private function getElectivesLevel($studentResultArr, $rank = 1)
        {
            // $studentResultArr = array( 'HKEAAsubjectCode' => 'Level')
            // unset Compulsory
            $compulsoryArr = array(
                'A010',
                'A020',
                'A030',
                'A040'
            );
            foreach ((array) $compulsoryArr as $_compluSubjectCode) {
                unset($studentResultArr[$_compluSubjectCode]);
            }
            $studentResultArr = $this->convertDseLevelToScore($studentResultArr);
            // arsort($studentResultArr);
            rsort($studentResultArr);
            return Get_String_Display($studentResultArr[$rank - 1]);
        }

        public function average($arr)
        {
            return number_format(array_sum($arr) / count($arr), 1);
        }

        public function getJupasVsDseAvgReport($AcademicYearArr, $displayByArr = array('Min','Avg','Max'))
        {
            $displayMin = (in_array('Min', (array) $displayByArr)) ? true : false;
            $displayAvg = (in_array('Avg', (array) $displayByArr)) ? true : false;
            $displayMax = (in_array('Max', (array) $displayByArr)) ? true : false;
            if (! $displayMin && ! $displayAvg && ! $displayMax) {
                $displayMin = true;
                $displayAvg = true;
                $displayMax = true;
            }
            $colspanNum = count($displayByArr) > 0 ? count($displayByArr) : 3;
            
            global $Lang;
            
            $jupasDataArr = $this->getJupasOffer($AcademicYearArr, $excludeNoOffer = true, $excludeNoIdentityStudent = true);
            $studentJupasAssocArr = BuildMultiKeyAssoc($jupasDataArr, array(
                'StudentID'
            ), array());
            $progNameAssoc = $this->getJupasProgrammeCodeMapping();
            
            $dseResultArr = $this->getExamData($AcademicYearArr, EXAMID_HKDSE, '', '', true, true);
            $studentDseAssocArr = BuildMultiKeyAssoc($dseResultArr, array(
                'StudentID',
                'SubjectCode'
            ), array(
                'Score'
            ), 1, 0);
            
            $resultAssocArr = array();
            $programInfoAssoc = array();
            foreach ((array) $studentJupasAssocArr as $_identity => $_studentJupasResult) {
                $_studentDseResult = $studentDseAssocArr[$_identity];
                if (! empty($_studentDseResult)) {
                    $_jupasCode = $_studentJupasResult['JupasCode'];
                    $programInfoAssoc[$_jupasCode]['Institution'] = $_studentJupasResult['Institution'];
                    $programInfoAssoc[$_jupasCode]['ProgrammeFullName'] = $_studentJupasResult['ProgrammeFullName'];
                    $resultAssocArr[$_jupasCode]['B5'][] = $this->getDseScoreByMode($_studentDseResult, 'B5');
                    $resultAssocArr[$_jupasCode]['4C+1E'][] = $this->getDseScoreByMode($_studentDseResult, '4C+1E');
                    $resultAssocArr[$_jupasCode]['4C+2E'][] = $this->getDseScoreByMode($_studentDseResult, '4C+2E');
                }
            }
            
            $html = '';
            $html .= '<table id="dataTable" class="table_generic_row_span" width="100%">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th rowspan="2">' . $Lang['SDAS']['Jupas']['Institution'] . '</th>';
            $html .= '<th rowspan="2">' . $Lang['SDAS']['Jupas']['ProgrammeName'] . '</th>';
            $html .= '<th rowspan="2">' . $Lang['SDAS']['Jupas']['NumOffer'] . '</th>';
            $html .= '<th colspan="' . $colspanNum . '">Best 5</th>';
            $html .= '<th colspan="' . $colspanNum . '">4C + 1E</th>';
            $html .= '<th colspan="' . $colspanNum . '">4C + 2E</th>';
            $html .= '</tr>';
            $html .= '<tr>';
            if ($displayMin)
                $html .= '<th>' . $Lang['SDAS']['Jupas']['JupasVsDseAvg']['Min'] . '</th>';
            if ($displayAvg)
                $html .= '<th>' . $Lang['SDAS']['Jupas']['JupasVsDseAvg']['Avg'] . '</th>';
            if ($displayMax)
                $html .= '<th>' . $Lang['SDAS']['Jupas']['JupasVsDseAvg']['Max'] . '</th>';
            if ($displayMin)
                $html .= '<th>' . $Lang['SDAS']['Jupas']['JupasVsDseAvg']['Min'] . '</th>';
            if ($displayAvg)
                $html .= '<th>' . $Lang['SDAS']['Jupas']['JupasVsDseAvg']['Avg'] . '</th>';
            if ($displayMax)
                $html .= '<th>' . $Lang['SDAS']['Jupas']['JupasVsDseAvg']['Max'] . '</th>';
            if ($displayMin)
                $html .= '<th>' . $Lang['SDAS']['Jupas']['JupasVsDseAvg']['Min'] . '</th>';
            if ($displayAvg)
                $html .= '<th>' . $Lang['SDAS']['Jupas']['JupasVsDseAvg']['Avg'] . '</th>';
            if ($displayMax)
                $html .= '<th>' . $Lang['SDAS']['Jupas']['JupasVsDseAvg']['Max'] . '</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            if (count($jupasDataArr) == 0 || count($dseResultArr) == 0) {
                $html .= '<tr>';
                $html .= '<td colspan="12">' . $Lang['SDAS']['DSEstat']['Alert']['NoData'] . '</td>';
                $html .= '</tr>';
            } else {
                $sortableResultArr = array();
                foreach ((array) $resultAssocArr as $_jupasCode => $_progStats) {
                    $sortableResultArr[$_jupasCode][] = $Lang['SDAS']['Jupas']['InstitutionName'][$programInfoAssoc[$_jupasCode]['Institution']];
                    $sortableResultArr[$_jupasCode][] = $_jupasCode . ' - ' . $programInfoAssoc[$_jupasCode]['ProgrammeFullName'];
                    $sortableResultArr[$_jupasCode][] = count($_progStats['B5']);
                    if ($displayMin)
                        $sortableResultArr[$_jupasCode][] = min($_progStats['B5']);
                    if ($displayAvg)
                        $sortableResultArr[$_jupasCode][] = $this->average($_progStats['B5']);
                    if ($displayMax)
                        $sortableResultArr[$_jupasCode][] = max($_progStats['B5']);
                    if ($displayMin)
                        $sortableResultArr[$_jupasCode][] = min($_progStats['4C+1E']);
                    if ($displayAvg)
                        $sortableResultArr[$_jupasCode][] = $this->average($_progStats['4C+1E']);
                    if ($displayMax)
                        $sortableResultArr[$_jupasCode][] = max($_progStats['4C+1E']);
                    if ($displayMin)
                        $sortableResultArr[$_jupasCode][] = min($_progStats['4C+2E']);
                    if ($displayAvg)
                        $sortableResultArr[$_jupasCode][] = $this->average($_progStats['4C+2E']);
                    if ($displayMax)
                        $sortableResultArr[$_jupasCode][] = max($_progStats['4C+2E']);
                }
                sortByColumn3($sortableResultArr, '0', '1', '3');
                foreach ((array) $sortableResultArr as $_sid => $_progResultArr) {
                    $html .= '<tr>';
                    foreach ((array) $_progResultArr as $__cellValue) {
                        $html .= '<td>' . $__cellValue . '</td>';
                    }
                    $html .= '</tr>';
                }
            }
            $html .= '</tbody>';
            $html .= '</table>';
            return $html;
        }

        public function getJupasVsDseReport($AcademicYearArr, $sortBy = 1)
        {
            global $Lang;
            
            $jupasDataArr = $this->getJupasOffer($AcademicYearArr, $excludeNoOffer = false, $excludeNoIdentityStudent = true);
            
            // if(count($jupasDataArr) == 0){
            // return 'fail';
            // }
            $studentJupasAssocArr = BuildMultiKeyAssoc($jupasDataArr, array(
                'StudentID'
            ), array());
            $progNameAssoc = $this->getJupasProgrammeCodeMapping();
            
            $studentIDArr = Get_Array_By_Key($jupasDataArr, 'StudentID');
            
            $html = '';
            $html .= '<table id="dataTable" class="table_generic_row_span" width="100%">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th>' . $Lang['SDAS']['Jupas']['Institution'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['Jupas']['ProgrammeName'] . '</th>';
            // $html .= '<th>'.$Lang['SDAS']['Jupas']['Band'].'</th>';
            $html .= '<th>' . $Lang['SDAS']['Jupas']['OfferRound'] . '</th>';
            // $html .= '<th>'.$Lang['SDAS']['Jupas']['JUPASNo'].'</th>';
            $html .= '<th>' . $Lang['SDAS']['Class'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['ClassNumber'] . '</th>';
            $html .= '<th>' . $Lang['General']['Name'] . '</th>';
            $html .= '<th>Best 5</th>';
            $html .= '<th>4C + 1E</th>';
            $html .= '<th>4C + 2E</th>';
            $html .= '<th>Chi</th>';
            $html .= '<th>Eng</th>';
            $html .= '<th>Math</th>';
            $html .= '<th>M1/M2</th>';
            $html .= '<th>LS&nbsp;</th>';
            $html .= '<th>Best Elective</th>';
            $html .= '<th>2nd Elective</th>';
            $html .= '<th>3rd Elective</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            
            if (count($studentIDArr) > 0) {
                
            	// 2020-07-15 (Philips) - Fix Wrong DSE Data Collect
            	$studentDseResultArr = $this->getExamData($AcademicYearArr, EXAMID_HKDSE, '', $studentIDArr, true, true);
                // $studentDseResultArr = CEES_Stat::selectData($dseResultArr, array('StudentID' => $studentIDArr));
                unset($dseResultArr);
                
                $studentDseAssocArr = BuildMultiKeyAssoc($studentDseResultArr, array(
                    'StudentID',
                    'SubjectCode'
                ), array(
                    'Score'
                ), 1, 0);
                
                $StudentInfo = BuildMultiKeyAssoc($this->getStudentsInfoByID($studentIDArr, $AcademicYearArr), 'UserID');
                
                $sortableResultArr = array();
                foreach ((array) $studentJupasAssocArr as $_sid => $_studentJupasResult) {
                    $_studentDseResult = $studentDseAssocArr[$_sid];
                    if (! empty($_studentDseResult) && ! empty($_studentJupasResult)) {
                        $sortableResultArr[$_sid][] = $Lang['SDAS']['Jupas']['InstitutionName'][$_studentJupasResult['Institution']];
                        $sortableResultArr[$_sid][] = $_studentJupasResult['ProgrammeFullName'] == '' ? $Lang['SDAS']['Jupas']['NoOffer'] : $_studentJupasResult['ProgrammeFullName'];
                        $sortableResultArr[$_sid][] = $Lang['SDAS']['Jupas']['RoundName'][$_studentJupasResult['OfferRound']];
                        // $sortableResultArr[$_sid][] = $_studentJupasResult['JupasAppNo'];
                        $sortableResultArr[$_sid][] = $StudentInfo[$_sid]['ClassName'];
                        $sortableResultArr[$_sid][] = $StudentInfo[$_sid]['ClassNumber'];
                        $sortableResultArr[$_sid][] = $StudentInfo[$_sid]['Name'];
                        
                        $sortableResultArr[$_sid][] = $this->getDseScoreByMode($_studentDseResult, 'B5');
                        $sortableResultArr[$_sid][] = $this->getDseScoreByMode($_studentDseResult, '4C+1E');
                        $sortableResultArr[$_sid][] = $this->getDseScoreByMode($_studentDseResult, '4C+2E');
                        
                        $sortableResultArr[$_sid][] = Get_String_Display($_studentDseResult['A010']);
                        $sortableResultArr[$_sid][] = Get_String_Display($_studentDseResult['A020']);
                        $sortableResultArr[$_sid][] = Get_String_Display($_studentDseResult['A030']);
                        $sortableResultArr[$_sid][] = $this->getM1M2Level($_studentDseResult);
                        $sortableResultArr[$_sid][] = Get_String_Display($_studentDseResult['A040']);
                        
                        $sortableResultArr[$_sid][] = $this->getElectivesLevel($_studentDseResult, 1);
                        $sortableResultArr[$_sid][] = $this->getElectivesLevel($_studentDseResult, 2);
                        $sortableResultArr[$_sid][] = $this->getElectivesLevel($_studentDseResult, 3);
                    }
                }
                switch ((int) $sortBy) {
                    case 1:
                        sortByColumn3($sortableResultArr, '0', '1', '4');
                        break;
                    case 2:
                        sortByColumn3($sortableResultArr, '4', '0', '1');
                        break;
                    case 3:
                        sortByColumn3($sortableResultArr, '5', '0', '1');
                        break;
                    case 4:
                        sortByColumn3($sortableResultArr, '6', '0', '1');
                        break;
                }
                if (count($sortableResultArr) > 0) {
                    foreach ((array) $sortableResultArr as $_sid => $_studentResultArr) {
                        $html .= '<tr>';
                        foreach ((array) $_studentResultArr as $__cellValue) {
                            $html .= '<td>' . Get_String_Display($__cellValue) . '</td>';
                        }
                        $html .= '</tr>';
                    }
                } else {
                    $html .= '<tr>';
                    $html .= '<td colspan="17">' . $Lang['SDAS']['DSEstat']['Alert']['NoData'] . '</td>';
                    $html .= '</tr>';
                }
                $html .= '</tbody>';
            } else {
                $html .= '<tr>';
                $html .= '<td colspan="17">' . $Lang['SDAS']['DSEstat']['Alert']['NoData'] . '</td>';
                $html .= '</tr>';
            }
            
            return $html;
        }

        public function getJUPASconfig()
        {
            global $JUPAS_CONFIG;
            return $JUPAS_CONFIG;
        }

        public function getJupasOfferReport($AcademicYearArr)
        {
            global $Lang;
            
            $JUPAS_CONFIG = $this->getJUPASconfig();
            $InstitutionArr = array_unique(array_values($JUPAS_CONFIG['Institution']));
            sort($InstitutionArr);
            
            $jupasDataArr = $this->getJupasOffer($AcademicYearArr);
            
            // target School
            // $targetSchool_DataArr = CEES_Stat::selectData($jupasDataArr, array('SchoolCode' => $schoolCode));
            $targetSchool_DataArr = $jupasDataArr;
            
            $targetSchool_NumApplied = count($targetSchool_DataArr);
            if ($targetSchool_NumApplied == 0) {
                return 'fail';
            }
            $targetSchool_NumSubD = CEES_Stat::countData($targetSchool_DataArr, array(
                'ProgrammeType' => 'SUB-D'
            ));
            $targetSchool_NumD = CEES_Stat::countData($targetSchool_DataArr, array(
                'ProgrammeType' => array(
                    'D',
                    'U'
                )
            ));
            $targetSchool_NumBNoOffer = CEES_Stat::countData($targetSchool_DataArr, array(
                'JupasCode' => '-999'
            ));
            
            $targetSchool_OfferDataArr = CEES_Stat::selectData($targetSchool_DataArr, array(
                'ProgrammeType' => array(
                    'D',
                    'U',
                    'SUB-D'
                )
            ));
            $targetSchool_InstitutionAssocArr = BuildMultiKeyAssoc($targetSchool_OfferDataArr, 'Institution', array(), 0, 1);
            $targetSchool_ResultAssocArr = array();
            foreach ((array) $targetSchool_InstitutionAssocArr as $_Institution => $_iDataArr) {
                if (! empty($_Institution)) {
                    $tempCountArr_SubD = CEES_Stat::countDataGroup($_iDataArr, array(
                        'AcademicYearID'
                    ), array(
                        'ProgrammeType' => 'SUB-D'
                    ));
                    $tempCountArr_D = CEES_Stat::countDataGroup($_iDataArr, array(
                        'AcademicYearID'
                    ), array(
                        'ProgrammeType' => array(
                            'D',
                            'U'
                        )
                    ));
                }
                $targetSchool_ResultAssocArr[$_Institution]['SUB-D'] = $tempCountArr_SubD;
                $targetSchool_ResultAssocArr[$_Institution]['D'] = $tempCountArr_D;
            }
            $targetSchool_allCountArr = array();
            $targetSchool_allCountArr[$JUPAS_CONFIG['ProgType']['SUBDEGREE']] = CEES_Stat::countDataGroup($targetSchool_DataArr, array(
                'AcademicYearID'
            ), array(
                'ProgrammeType' => 'SUB-D'
            ));
            $targetSchool_allCountArr[$JUPAS_CONFIG['ProgType']['DEGREE']] = CEES_Stat::countDataGroup($targetSchool_DataArr, array(
                'AcademicYearID'
            ), array(
                'ProgrammeType' => array(
                    'D',
                    'U'
                )
            ));
            $targetSchool_allallCountArr = CEES_Stat::countDataGroup($targetSchool_DataArr, array(
                'AcademicYearID'
            ), array());
            
            // get AcademicYearDisplay
            $academicYearAry = array();
            // $academicYearInfoAssoArr = $this->getAcademYearNameById('');
            $academicYearInfoAssoArr = BuildMultiKeyAssoc(GetAllAcademicYearInfo(), 'AcademicYearID');
            foreach ($AcademicYearArr as $ayID) {
                $academicYearAry[$ayID] = $academicYearInfoAssoArr[$ayID];
            }
            asort($academicYearAry);
            
            $numYear = count($AcademicYearArr);
            $arr['data']['AcademicYearIDArr'] = $AcademicYearArr;
            $arr['data']['AcademicYear'] = $academicYearAry;
            $arr['data']['School'] = $targetSchool_ResultAssocArr;
            $arr['data']['Ref'] = $twSchool_ResultAssocArr;
            // $arr['data']['institutionConfig'] = $institutionArr;
            
            // Build html
            $html = '';
            $html .= '<table id="dataTable" class="common_table_list_v30 view_table_list_v30">';
            // thead
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th>' . $Lang['SDAS']['Jupas']['Institution'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['Jupas']['ProgrammeName'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['SchoolYear'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['Jupas']['No'] . '</th>';
            $html .= '<th>' . $Lang['SDAS']['Jupas']['%'] . '</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            // tbody
            $html .= '<tbody>';
            foreach ((array) $InstitutionArr as $_institutionKey) {
                $html .= '<tr>';
                $html .= '<td rowspan="' . ($numYear * 2) . '">' . $Lang['SDAS']['Jupas']['InstitutionName'][$_institutionKey] . '</td>';
                for ($type = 0; $type < 2; $type ++) {
                    if ($type > 0) {
                        $currentType = $JUPAS_CONFIG['ProgType']['SUBDEGREE'];
                        $html .= '</tr>';
                        $html .= '<tr>';
                        $html .= '<td rowspan="' . ($numYear * 1) . '" style="font-weight:normal;">' . $Lang['SDAS']['Jupas']['SubDegree'] . '</td>';
                    } else {
                        $currentType = $JUPAS_CONFIG['ProgType']['DEGREE'];
                        $html .= '<td rowspan="' . ($numYear * 1) . '" style="font-weight:normal;">' . $Lang['SDAS']['Jupas']['Degree'] . '</td>';
                    }
                    
                    $key = 0;
                    foreach ((array) $academicYearAry as $_ayId => $_name) {
                        if ($key > 0) {
                            $html .= '</tr>';
                            $html .= '<tr>';
                        }
                        // year
                        $html .= '<td style="font-weight:normal;">' . $academicYearInfoAssoArr[$_ayId][Get_Lang_Selection('YearNameB5', 'YearNameEN')] . '</td>';
                        
                        // stats data
                        $html .= '<td>' . (isset($targetSchool_allallCountArr[$_ayId]) ? ($targetSchool_ResultAssocArr[$_institutionKey][$currentType][$_ayId] > 0 ? $targetSchool_ResultAssocArr[$_institutionKey][$currentType][$_ayId] : 0) : '--') . '</td>';
                        $html .= '<td>' . (isset($targetSchool_allallCountArr[$_ayId]) ? ($targetSchool_ResultAssocArr[$_institutionKey][$currentType][$_ayId] == 0 ? 0 : number_format($targetSchool_ResultAssocArr[$_institutionKey][$currentType][$_ayId] / $targetSchool_allallCountArr[$_ayId] * 100, 2)) . '%' : '--') . '</td>';
                        $key ++;
                    }
                }
            }
            $html .= '</tr>';
            
            $html .= '<tr>';
            $html .= '<td rowspan="' . ($numYear * 2) . '">' . $Lang['SDAS']['Jupas']['AllInstitution'] . '</td>';
            for ($type = 0; $type < 2; $type ++) {
                if ($type > 0) {
                    $currentType = $JUPAS_CONFIG['ProgType']['SUBDEGREE'];
                    $html .= '</tr>';
                    $html .= '<tr>';
                    $html .= '<td rowspan="' . ($numYear * 1) . '" style="font-weight:normal;">' . $Lang['SDAS']['Jupas']['SubDegree'] . '</td>';
                } else {
                    $currentType = $JUPAS_CONFIG['ProgType']['DEGREE'];
                    $html .= '<td rowspan="' . ($numYear * 1) . '" style="font-weight:normal;">' . $Lang['SDAS']['Jupas']['Degree'] . '</td>';
                }
                // foreach((array)$AcademicYearArr as $key => $_ayId ){
                $key = 0;
                foreach ((array) $academicYearAry as $_ayId => $_name) {
                    if ($key > 0) {
                        $html .= '</tr>';
                        $html .= '<tr>';
                    }
                    // year
                    $html .= '<td style="font-weight:normal;">' . $academicYearInfoAssoArr[$_ayId][Get_Lang_Selection('YearNameB5', 'YearNameEN')] . '</td>';
                    // stats data
                    $html .= '<td>' . (isset($targetSchool_allallCountArr[$_ayId]) ? ($targetSchool_allCountArr[$currentType][$_ayId] > 0 ? $targetSchool_allCountArr[$currentType][$_ayId] : 0) : '--') . '</td>';
                    $html .= '<td>' . (isset($targetSchool_allallCountArr[$_ayId]) ? ($targetSchool_allCountArr[$currentType][$_ayId] == 0 ? 0 : number_format($targetSchool_allCountArr[$currentType][$_ayId] / $targetSchool_allallCountArr[$_ayId] * 100, 2)) . '%' : '--') . '</td>';
                    // if($usingAll){
                    // $html .= '<td>'. ($twSchool_allCountArr[$currentType][$_ayId] > 0 ? $twSchool_allCountArr[$currentType][$_ayId] : 0 ).'</td>';
                    // $html .= '<td>'. ($twSchool_allCountArr[$currentType][$_ayId] == 0 ? 0 : number_format($twSchool_allCountArr[$currentType][$_ayId] / $twSchool_allallCountArr[$_ayId] * 100, 2) ).'%</td>';
                    // }
                    $key ++;
                }
            }
            $html .= '</tr>';
            
            $html .= '<tr>';
            $html .= '<td rowspan="' . ($numYear * 1) . '" colspan="2">' . $Lang['SDAS']['Jupas']['All'] . '</td>';
            // foreach((array)$AcademicYearArr as $key => $_ayId ){
            $key = 0;
            foreach ((array) $academicYearAry as $_ayId => $_name) {
                if ($key > 0) {
                    $html .= '</tr>';
                    $html .= '<tr>';
                }
                $html .= '<td style="font-weight:normal;">' . $academicYearInfoAssoArr[$_ayId][Get_Lang_Selection('YearNameB5', 'YearNameEN')] . '</td>';
                $html .= '<td>' . (isset($targetSchool_allallCountArr[$_ayId]) ? $targetSchool_allallCountArr[$_ayId] : '--') . '</td>';
                $html .= '<td>' . (isset($targetSchool_allallCountArr[$_ayId]) ? '100%' : '--') . '</td>';
                $key ++;
            }
            $html .= '</tr>';
            $html .= '</tbody>';
            $html .= '</table>';
            
            return $html;
        }

        public function getStudentExamDetail($studentID, $FromAcademicYearID)
        {
            global $Lang;
            if ($studentID) {
                $examDataArr_temp = $this->getExamData($FromAcademicYearID, EXAMID_HKDSE, $subjectID = '', $studentID, $excludeCmpSubject = false, true);
                $examDataArr = BuildMultiKeyAssoc($examDataArr_temp, 'SubjectID');
                $subjectIDArr = Get_Array_By_Key($examDataArr_temp, 'SubjectID');
                $subjectArr_temp = $this->getSubjectCodeAndID((array) $subjectIDArr, $WebSAMSCodeArr = array(), $parCmpOnly = false);
                $subjectArr = BuildMultiKeyAssoc($subjectArr_temp, 'SubjectID');
                
                if ($examDataArr) {
                    $html = '<div class="chart_tables" id="thickboxContentDiv">';
                    $html .= '<table class="common_table_list_v30 view_table_list_v30 resultTable">';
                    $html .= '<colgroup>';
                    $html .= '<col style="width:65%;">';
                    $html .= '<col style="width:35%;">';
                    $html .= '</colgroup>';
                    $html .= '<thead>';
                    $html .= '<tr>';
                    $html .= '<th>' . $Lang['SysMgr']['SubjectClassMapping']['Subject'] . '</th>';
                    $html .= '<th>' . $Lang['SDAS']['DSEstat']['DSELevel']['Title'] . '</th>';
                    $html .= '</tr>';
                    $html .= '</thead>';
                    $html .= '<tbody>';
                    foreach ((array) $subjectArr as $subjectID => $_subjectArr) {
                        if ($_subjectArr['cmp_displayOrder']) {
                            $displaySubjectName = '&nbsp;&nbsp;&nbsp;';
                            $displaySubjectName .= Get_Lang_Selection($_subjectArr['ch_component_name'], $_subjectArr['component_name']);
                        } else {
                            $displaySubjectName = Get_Lang_Selection($_subjectArr['ch_main_name'], $_subjectArr['main_name']);
                        }
                        $html .= '<tr>';
                        $html .= '<td>' . $displaySubjectName . '</td>';
                        $html .= '<td>' . $examDataArr[$subjectID]['Score'] . '</td>';
                        $html .= '</tr>';
                    }
                    $html .= '</tbody>';
                    $html .= '</table>';
                    $html .= '<div>';
                } else {
                    $html = '<table>';
                    $html .= '<tr>';
                    $html .= '<td>' . $Lang['General']['NoRecordAtThisMoment'] . '</td>';
                    $html .= '</tr>';
                    $html .= '</table>';
                }
            }
            
            return $html;
        }

        public function getTermAndTermAssessment()
        {
            global $eclass_db;
            // AND ayt.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
            // ayt.YearTermNameB5, ayt.YearTermNameEN, TermAssessment
            $sql = "
    		SELECT
    			distinct assr.Semester, assr.YearTermID, assr.TermAssessment, ayt.YearTermNameEN, ayt.YearTermNameB5, ay.AcademicYearID, ay.YearNameEN, ay.YearNameB5
    		FROM
    			{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr
    		INNER JOIN
    			ACADEMIC_YEAR_TERM ayt
    				ON ayt.YearTermID = assr.YearTermID
			INNER JOIN
				ACADEMIC_YEAR ay
					ON ayt.AcademicYearID = ay.AcademicYearID
    		WHERE
    			assr.Semester !='' 
    		Group by 
				ay.AcademicYearID, ayt.YearTermID, TermAssessment
    		ORDER BY
    			ayt.TermStart, assr.TermAssessment
    		";
            return $this->returnResultSet($sql);
        }

        public function calculatePercentile($percentile, $values)
        {
            sort($values);
            $index = ($percentile / 100) * count($values);
            if (floor($index) == $index) {
                $result = ($values[$index - 1] + $values[$index]) / 2;
            } else {
                $result = $values[floor($index)];
            }
            return $result;
        }
    }
    // end of class
}

class CEES_Stat
{

    public function __construct()
    {}

    private function buildEqualCondition($fieldName, $wantValue, $rowVarName = '$row')
    {
        $condition = '';
        if (is_array($wantValue)) {
            
            $orConditionArr = array();
            foreach ((array) $wantValue as $_value) {
                $orConditionArr[] = CEES_Stat::buildEqualCondition($fieldName, $_value, $rowVarName);
            }
            $condition = '(' . implode(' || ', $orConditionArr) . ')';
        } else {
            $condition .= ' ' . $rowVarName . '[\'' . $fieldName . '\'] == \'' . $wantValue . '\'';
        }
        
        return $condition;
    }

    public function countDataGroup($data, $group, $condition)
    {
        
        // ready condition
        $conditionArr = array();
        if (! empty($condition)) {
            foreach ((array) $condition as $_targetField => $_wantedValue) {
                // $conditionArr[] = ' $row[\''.$_targetField.'\'] == '.$_wantedValue;
                $conditionArr[] = CEES_Stat::buildEqualCondition($_targetField, $_wantedValue, '$_row');
            }
            $conditionString = 'return ' . implode(' && ', $conditionArr) . ';';
        }
        // debug_pr($conditionString);
        
        $groupedData = BuildMultiKeyAssoc($data, $group, array(), 0, 1);
        
        $countArr = array();
        foreach ((array) $groupedData as $key => $dataArr) {
            if (! empty($condition)) {
                $newdata = array();
                foreach ((array) $dataArr as $_key => $_row) {
//                     if (eval($conditionString)) {
                    if (eval(sanitizeEvalInput($conditionString))) {
                        $newdata[] = $_row;
                    }
                }
            } else {
                $newdata = $dataArr;
            }
            $countArr[$key] = count($newdata);
        }
        
        return $countArr;
    }

    public function countData($data, $condition)
    {
        $conditionArr = array();
        foreach ((array) $condition as $_targetField => $_wantedValue) {
            // $conditionArr[] = ' $row[\''.$_targetField.'\'] == '.$_wantedValue;
            $conditionArr[] = CEES_Stat::buildEqualCondition($_targetField, $_wantedValue);
        }
        $conditionString = 'return ' . implode(' && ', $conditionArr) . ';';
        // debug_pr($conditionString);
        
        $newdata = array();
        foreach ((array) $data as $key => $row) {
//             if (eval($conditionString)) {
            if (eval(sanitizeEvalInput($conditionString))) {
                $newdata[] = $row;
            }
        }
        
        return count($newdata);
    }

    public function selectData($data, $condition)
    {
        $conditionArr = array();
        foreach ((array) $condition as $_targetField => $_wantedValue) {
            // $conditionArr[] = ' $row[\''.$_targetField.'\'] == '.$_wantedValue;
            $conditionArr[] = CEES_Stat::buildEqualCondition($_targetField, $_wantedValue);
        }
        $conditionString = 'return ' . implode(' && ', $conditionArr) . ';';
        // debug_pr($conditionString);
        
        $newdata = array();
        foreach ((array) $data as $key => $row) {
//             if (eval($conditionString)) {
            if (eval(sanitizeEvalInput($conditionString))) {           
                $newdata[] = $row;
            }
        }
        
        return $newdata;
    }
}

?>