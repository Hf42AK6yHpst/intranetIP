<?php
if (!defined("POWERLESSON_DEFINED")) {
    define("POWERLESSON_DEFINED", true);

    class PowerLesson
    {
        // const MINIMUM_COMPATIBLE_VERSION = '2.0.12.2';
        const MINIMUM_COMPATIBLE_VERSION = '2.0.12.3';

        const MAX_CACHED_COURSE_DB_CONNECTION = 10;

        protected static $publicAccessProcedure = [
            'getUsers'
        ];

        protected $powerLessonVersion = null;
        
        protected $db;

        protected $courseDbs = [];
        
        public function __construct($db = null)
        {
            if(is_null($db)){
                include_once __DIR__ . '/../libdb.php';
                $this->db = new libdb();
            }else{
                $this->db = $db;
            }
        }

        public static function isCompatible($version)
        {
            return version_compare($version, self::MINIMUM_COMPATIBLE_VERSION) >= 0;
        }

        public static function getMinimumCompatibleVersion()
        {
            return self::MINIMUM_COMPATIBLE_VERSION;
        }

        public function run($procedure, $args)
        {
            global $UserID;

            if (method_exists($this, $procedure)) {
                $reflection = new ReflectionMethod($this, $procedure);

                if (
                    !$reflection->isPrivate() &&
                    ($UserID != '' || in_array($procedure, self::$publicAccessProcedure))
                    ) {
                        $args = is_array($args) ? $args : [];

                        return call_user_func_array(
                            array($this, $procedure),
                            [$args]
                            );
                    } else {
                        return 202; // Error Code - No Permission
                    }
            } else {
                return 3; // Error Code - Unknown Method
            }
        }

        public function getPowerLessonVersion()
        {
            if(is_null($this->powerLessonVersion)){
                $response = $this->powerLessonApiCaller('/version');
                if($response['result']){
                    $this->setPowerLessonVersion($response['apiResponse']['version']);
                }
            }
            
            return $this->powerLessonVersion;
        }
        
        private function setPowerLessonVersion($powerLessonVersion)
        {
            $this->powerLessonVersion = $powerLessonVersion;
        }
        
        protected function getCourseDb($courseId)
        {
            global $intranet_db_user, $intranet_db_pass, $sys_custom;
                
            if(!isset($this->courseDbs[$courseId])){
                if(count($this->courseDbs) > static::MAX_CACHED_COURSE_DB_CONNECTION){
                    array_shift($this->courseDbs);
                }

                $host    = $sys_custom['MySQL_Server_Host']? "localhost" : $sys_custom['MySQL_Server_Host'];
                $dbName  = classNamingDB($courseId);
                $charset = 'utf8';
                $dsn     = 'mysql:host=' . $host . ';dbname=' . $dbName . ';charset=' . $charset;
                $options = [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'];
                
                $this->courseDbs[$courseId] = new PDO($dsn, $intranet_db_user, $intranet_db_pass, $options);
            }
            
            return $this->courseDbs[$courseId];
        }
        
        protected function getIntranetUserCoursesAndOwnUserCourses()
        {
            global $UserID;

            $intranetUsers = $this->getIntranetUsers(['intranetUserId' => $UserID]);
            $courses       = $this->getCourses();
            $userCourses   = $this->getUserCourses(['intranetUserId' => $UserID]);

            if (!is_array($intranetUsers)) {
                return $intranetUsers;
            } elseif (!is_array($courses)) {
                return $courses;
            } elseif (!is_array($userCourses)) {
                return $userCourses;
            } else {
                return [
                    'intranetUsers' => $intranetUsers,
                    'courses'       => $courses,
                    'userCourses'   => $userCourses
                ];
            }
        }
        
        protected function getIntranetUserOwnCoursesAndOwnUserCourses()
        {
            global $UserID;
            
            $intranetUsers = $this->getIntranetUsers(['intranetUserId' => $UserID]);
            if (!is_array($intranetUsers)) {
                return $intranetUsers;
            }
            
            $userCourses = $this->getUserCourses(['intranetUserId' => $UserID]);
            if (!is_array($userCourses)) {
                return $userCourses;
            }
            
            $courses = $this->getCourses([
                'courseIds' => array_map(function($userCourse){
                    return $userCourse['courseId'];
                }, $userCourses)
            ]);
            if (!is_array($courses)) {
                return $courses;
            }
            
            return [
                'intranetUsers' => $intranetUsers,
                'courses'       => $courses,
                'userCourses'   => $userCourses
            ];
        }

        protected function getIntranetTeachers()
        {
            return $this->getIntranetUsers(['recordType' => 1]);
        }

        protected function getIntranetUsers($args)
        {
            global $intranet_db;

            if (!(isset($args['intranetUserId']) || isset($args['userLogin'])) && !isset($args['recordType'])) {
                return 102; // Error Code - Invalid parameters
            }

            $intranetUsers = array();

            $sql = "
                SELECT
                    UserID AS id,
                    TitleEnglish AS englishTitle,
                    TitleChinese AS chineseTitle,
                    EnglishName AS englishName,
                    ChineseName AS chineseName,
                    RecordType AS recordType,
                    RecordStatus AS recordStatus,
                    UserLogin AS userLogin
                FROM
                    " . $intranet_db . ".INTRANET_USER
                WHERE
                    1 = 1
            ";

            if(isset($args['intranetUserId'])){
                $sql .= " AND UserID = '" . $args['intranetUserId'] . "'";
            }

            if(isset($args['userLogin'])){
                $sql .= " AND UserLogin = '" . $args['userLogin'] . "'";
            }

            if(isset($args['recordType'])){
                $sql .= " AND RecordType = '" . $args['recordType'] . "'";
            }

            if (isset($args['recordStatus'])) {
                $sql .= " AND RecordStatus = '". $args['recordStatus'] ."'";
            }

            $result = $this->db->returnArray($sql);

            if(count($result) > 0){
                $intranetUserIds = array_map(function($intranetUser){
                    return $intranetUser['id'];
                }, $result);

                    /* Admin */
                    $sql = "SELECT
					UserID
				FROM
				    " . $intranet_db . ".ROLE_MEMBER rm
				INNER JOIN
				    " . $intranet_db . ".ROLE_RIGHT rr
					ON
						rm.RoleID = rr.RoleID
					AND
						rm.UserID IN ('" . implode("','", $intranetUserIds) . "')
				WHERE
					rr.FunctionName = 'eLearning-PowerLesson2'";
                    $adminIntranetUserIds = $this->db->returnVector ( $sql );

                    include_once __DIR__ . '/../libuser.php';
                    $lu = new libuser ();
                    for($i=0; $i<count($result); $i++){
                        /* PhotoLink */
                        $userPhotoAry = $lu->GET_USER_PHOTO ( $result[$i]['userLogin'], $result[$i]['id'], false );

                        $intranetUsers[] = array(
                            'id'           => intval($result[$i]['id']),
                            'recordType'   => intval($result[$i]['recordType']),
                            'recordStatus' => intval($result[$i]['recordStatus']),
                            'englishTitle' => intranet_undo_htmlspecialchars( $result[$i]['englishTitle'] ),
                            'chineseTitle' => intranet_undo_htmlspecialchars( $result[$i]['chineseTitle'] ),
                            'englishName'  => intranet_undo_htmlspecialchars ( $result[$i]['englishName'] ),
                            'chineseName'  => intranet_undo_htmlspecialchars ( $result[$i]['chineseName'] ),
                            'photoLink'    => $userPhotoAry[2],
                            'admin'        => in_array($result[$i]['id'], $adminIntranetUserIds) ? 1 : 0
                        );
                    }
            }

            return $intranetUsers;
        }

        protected function getCourse($args)
        {
            if (!isset($args['courseId'])){
                return 102; // Error Code - Invalid parameters
            }

            $courses = $this->getCourses(['courseIds' => [$args['courseId']]]);

            return count($courses) > 0 ? $courses[0] : array();
        }

        protected function getCourses($args = array())
        {
            global $eclass_db;

            if (isset($args['courseIds']) && !is_array($args['courseIds'])){
                return 102; // Error Code - Invalid parameters
            }

            $courses = array();

            $sql = "SELECT
                        c.course_id AS id,
                        c.course_name AS name,
                        c.language AS language,
                        uc.numberOfStudents AS numberOfStudents
                    FROM
                        " . $eclass_db . ".course c LEFT JOIN
                        (
                            SELECT
                                course_id,
                                COUNT(*) AS numberOfStudents
                            FROM
                                " . $eclass_db . ".user_course
                            WHERE
                                memberType = 'S' AND
                                status IS NULL
                            GROUP BY
                                course_id
                        ) uc ON c.course_id = uc.course_id
                    WHERE
                        c.RoomType = 0";

            if(isset($args['courseIds'])){
                $sql .= " AND c.course_id IN ('" . implode("','", $args['courseIds']) . "')";
            }

            $sql .= " ORDER BY
                        c.course_name";

            $result = $this->db->returnArray($sql);
            for($i=0; $i<count($result); $i++){
                $courses[] = array(
                    "id"               => intval($result[$i]['id']),
                    "name"             => intranet_undo_htmlspecialchars($result[$i]['name']),
                    "language"         => $result[$i]['language'],
                    "numberOfStudents" => !is_null($result[$i]['numberOfStudents'])? intval($result[$i]['numberOfStudents']) : 0
                );
            }

            return $courses;
        }

        protected function getUserCourses($args)
        {
            global $eclass_db;

            if (!(isset($args['courseId']) || isset($args['intranetUserId'])) || (isset($args['intranetUserIds']) && !is_array($args['intranetUserIds']))){
                return 102; // Error Code - Invalid parameters
            }

            $userCourses = array();

            $sql = "
                SELECT
                    uc.user_course_id AS id,
                    uc.course_id as courseId,
                    uc.user_id AS userId,
                    uc.intranet_user_id AS intranetUserId,
                    uc.memberType,
                    uc.auth_token as authToken
                FROM
                    " . $eclass_db . ".user_course uc
                INNER JOIN
                    " . $eclass_db . ".course c
                ON
                    uc.course_id = c.course_id
                WHERE
                    c.RoomType = 0
        	    AND
        	       uc.memberType IN ('T', 'S')
            ";

            if(isset($args['intranetUserId'])){
                $sql .= " AND uc.intranet_user_id = '" . $args['intranetUserId'] . "'";
            }

            if(isset($args['intranetUserIds'])){
                $sql .= " AND uc.intranet_user_id IN ('" . implode( "','", $args['intranetUserIds']) . "')";
            }

            if(isset($args['courseId'])){
                $sql .= " AND uc.course_id = '" . $args['courseId'] . "'";
            }

            $result = $this->db->returnArray($sql);

            for($i=0; $i<count($result); $i++){
                $userCourses[] = array(
                    "id" => intval($result[$i]['id']),
                    "courseId" => intval($result[$i]['courseId']),
                    "userId" => intval($result[$i]['userId']),
                    "intranetUserId" => intval($result[$i]['intranetUserId']),
                    "memberType" => $result[$i]['memberType'],
                    "authToken" => $result[$i]['authToken']
                );
            }

            return $userCourses;
        }

        protected function getTeachersAndTeacherClassroomRoles($args)
        {
            if (!isset($args['courseIds'])) {
                return 102; // Error Code - Invalid parameters
            }

            $result = [];
            $userArgs = array();

            if(isset($args['withoutPhoto']) && $args['withoutPhoto']){
                $userArgs['withoutPhoto'] = $args['withoutPhoto'];
            }

            foreach ($args['courseIds'] as $courseId) {
                $userArgs['courseId'] = $courseId;

                $users                 = $this->getTeachers($userArgs);
                $teacherClassroomRoles = $this->getTeacherClassroomRoles(['courseId' => $courseId]);

                if (!is_array($users)) {
                    return $users;
                } elseif (!is_array($teacherClassroomRoles)) {
                    return $teacherClassroomRoles;
                } else {
                    $result[$courseId] = [
                        'users'                 => $users,
                        'teacherClassroomRoles' => $teacherClassroomRoles
                    ];
                }
            }

            return $result;
        }

        protected function getTeachersOfCourses($args)
        {
            if (! isset($args['courseIds'])) {
                return 102; // Error Code - Invalid parameters
            }

            $result = [];
            $userArgs = array();

            if (isset($args['withoutPhoto']) && $args['withoutPhoto']) {
                $userArgs['withoutPhoto'] = $args['withoutPhoto'];
            }

            foreach ($args['courseIds'] as $courseId) {
                $userArgs['courseId'] = $courseId;

                $users = $this->getTeachers($userArgs);

                if (! is_array($users)) {
                    return $users;
                } else {
                    $result[$courseId] = $users;
                }
            }

            return $result;
        }

        protected function getTeacherClassroomRolesOfCourses($args)
        {
            if (!isset($args['courseIds'])) {
                return 102; // Error Code - Invalid parameters
            }

            $result = [];

            foreach ($args['courseIds'] as $courseId) {

                $teacherClassroomRoles = $this->getTeacherClassroomRoles([
                    'courseId' => $courseId
                ]);

                if (!is_array($teacherClassroomRoles)) {
                    return $teacherClassroomRoles;
                } else {
                    $result[$courseId] = $teacherClassroomRoles;
                }
            }

            return $result;
        }

        protected function getTeachers($args)
        {
            if (!isset($args['courseId'])) {
                return 102; // Error Code - Invalid parameters
            }

            $args['memberType'] = 'T';

            return $this->getUsers($args);
        }

        protected function getUsers($args)
        {
            global $intranet_db;

            if (!isset($args['courseId']) || (isset($args['intranetUserIds']) && !is_array($args['intranetUserIds'])))
            {
                return 102; // Error Code - Invalid parameters
            }

            $courseDb = classNamingDB($args['courseId']);
             
            $users = array();

            $sql = "
                SELECT
    	           u.user_id AS id,
    	           u.user_email AS userEmail,
    	           u.TitleEnglish AS englishTitle,
    	           u.TitleChinese AS chineseTitle,
    	           u.firstname AS firstName,
    	           u.lastname AS lastName,
    	           u.chinesename AS chineseName,
    	           u.intranet_user_id AS intranetUserId,
    	           u.memberType,
    	           u.class_number AS classNameAndClassNumber,
    	           u.status,
	               iu.UserLogin as userLogin
    	       FROM
    	           " . $courseDb . ".usermaster	u
    	       LEFT JOIN
        	       " . $intranet_db . ".INTRANET_USER iu
    	       ON
    	           u.user_email = iu.UserEmail
        	   WHERE
        	       1 = 1
            ";

            if(isset($args['memberType'])){
                $sql .= " AND u.memberType = '" . $args['memberType'] . "'";
            }else{
                $sql .= " AND u.memberType IN ('T', 'S')";
            }

            if(isset($args['intranetUserId'])){
                $sql .= " AND u.intranet_user_id = '" . $args['intranetUserId'] . "'";
            }

            if(isset($args['intranetUserIds'])){
                $sql .= " AND u.intranet_user_id IN ('" . implode( "','", $args['intranetUserIds']) . "')";
            }

            if(isset($args['isCurrentUser'])){
                $sql .= $args['isCurrentUser'] ? " AND u.status IS NULL" : " AND u.status = 'deleted'";
            }

            $result = $this->db->returnArray($sql);

            include_once __DIR__ . '/../libuser.php';
            $lu = new libuser ();

            for($i=0; $i<count($result); $i++){
                if (! empty ( $result [$i] ['classNameAndClassNumber'] )) {
                    $classNameAndClassNumberAry = explode ( " - ", $result [$i] ['classNameAndClassNumber'] );
                    $className = count ( $classNameAndClassNumberAry ) > 0 ? $classNameAndClassNumberAry [0] : null;
                    $classNumber = count ( $classNameAndClassNumberAry ) > 1 ? $classNameAndClassNumberAry [1] : null;
                } else {
                    $className = null;
                    $classNumber = null;
                }

                if(isset($args['withoutPhoto']) && $args['withoutPhoto']){
                    $intranetPhotoLink = null;
                }else{
                    $userPhotoAry = $lu->GET_USER_PHOTO ( $result [$i] ['userLogin'], $userResults [$i] ['intranetUserId'], false );
                    $intranetPhotoLink = $userPhotoAry[1];
                }

                $users[] = array(
                    "id" => intval($result[$i]['id']),
                    "userEmail" => $result[$i]['userEmail'],
                    "englishTitle" => intranet_undo_htmlspecialchars($result[$i]['englishTitle']),
                    "chineseTitle" => intranet_undo_htmlspecialchars($result[$i]['chineseTitle']),
                    "firstName" => $result[$i]['firstName'],
                    "lastName" => $result[$i]['lastName'],
                    "englishName" => intranet_undo_htmlspecialchars ( trim ( $result [$i] ['firstName'] . ' ' . $result [$i] ['lastName'] )),
                    "chineseName" => intranet_undo_htmlspecialchars($result[$i]['chineseName']),
                    "intranetUserId" => intval($result[$i]['intranetUserId']),
                    "className" => $className,
                    "classNumber" => $classNumber,
                    "memberType" => $result[$i]['memberType'],
                    "status" => $result[$i]['status'],
                    "intranetPhotoLink" => $intranetPhotoLink
                );
            }

            return $users;
        }

        protected function getTeacherClassroomRoles($args)
        {
            if (!isset($args['courseId'])) {
                return 102; // Error Code - Invalid parameters
            }

            $courseDb = classNamingDB($args['courseId']);
             
            $teacherClassroomRoles = array();

            $sql = "SELECT * FROM " . $courseDb . ".powerlesson_teacher_classroom_roles WHERE 1 = 1";

            if(isset($args['teacherId'])){
                $sql .= " AND user_id = '" . $args['teacherId'] . "'";
            }
            
            $result = $this->db->returnArray($sql);

            for($i=0; $i<count($result); $i++){
                $teacherClassroomRoles[] = [
                    'id'             => intval($result[$i]['classroom_role_id']),
                    'identifier'     => $result[$i]['identifier'],
                    'teacherId'      => intval($result[$i]['user_id']),
                    'role'           => [
                        'role' => $result[$i]['role']
                    ],
                    'createByUserId' => intval($result[$i]['create_by']),
                    'createDate'     => (new DateTime($result[$i]['create_date']))->format('Y-m-d\TH:i:sP'),
                    'modifyByUserId' => intval($result[$i]['modify_by']),
                    'modifyDate'     => (new DateTime($result[$i]['modify_date']))->format('Y-m-d\TH:i:sP')
                ];
            }

            /* Comment out by Thomas on 201805151200 dur to poor performance */
            //             $teacherClassroomRoles = array();

            //             $apiUri = '/teacherClassroomRoles';
            //             $getTeacherClassroomRolesApi = $this->powerLessonApiCaller ( $apiUri, 'GET', $args['courseId'], false, array(), null, true);

            //             if ($getTeacherClassroomRolesApi ['result']) {
            //                 $teacherClassroomRoles = $getTeacherClassroomRolesApi ['apiResponse'];
            //             }

            return $teacherClassroomRoles;
        }

        protected function addTeachers($args)
        {
            if (!isset($args['courseIds']) || !isset($args['intranetUserIds'])) {
                return 102; // Error Code - Invalid parameters
            }

            $args['memberType'] = 'T';

            return $this->addUsers($args);
        }

        protected function addUsers($args){
            global $eclass_db, $UserID, $ec_file_path, $intranet_db;

            if (!isset($args['courseIds']) || !isset($args['intranetUserIds']) || !isset($args['memberType'])) {
                return 102; // Error Code - Invalid parameters
            }
            include_once __DIR__ . '/../libeclass40.php';
            include_once __DIR__ . '/../icalendar.php';

            $icalendar = new icalendar();

            for($i = 0; $i < count($args['courseIds']); $i++){
                $courseId = $args['courseIds'][$i];
                $libeclass = new libeclass($courseId);

                if(sizeof($args['intranetUserIds']) > 0){
                    $libeclass->eClassUserAddFullInfoByUserID($args['intranetUserIds'], $args['memberType']);
                    $libeclass->syncUserToSubjectGroup($courseId);

                    /* Insert calendar viewer */
                    $sql = "
                          SELECT
                            CalID
                          FROM
                            " . $eclass_db . ".course
                          WHERE
                            course_id = '" . $courseId . "'";
                    $calId = $libeclass->returnVector($sql);

                    $sql = "
                          SELECT
                            UserID
                          FROM
                            " . $intranet_db . ".CALENDAR_CALENDAR_VIEWER
    			          WHERE
                            CalID = '" . $calId[0] . "'";
                    $existingUserIds = $icalendar->returnVector($sql);

                    $newUserIds = array_diff($args['intranetUserIds'], $existingUserIds);

                    $icalendar->insertCalendarViewer($calId[0], $newUserIds, "W", $courseId, 'C');
                }
            }

            if(in_array($UserID, $args['intranetUserIds'])){
                $this->loginCourses();
            }

            $returnResult = array();
            $userArgs = array();
            $userArgs['intranetUserIds'] = $args['intranetUserIds'];
            $userArgs['memberType'] = $args['memberType'];
            $userArgs['isCurrentUser'] = true;
            foreach ($args['courseIds'] as $courseId) {
                $userArgs['courseId'] = $courseId;
                $returnResult[$courseId] = array(
                    'users' => $this->getUsers($userArgs),
                    'userCourses' => $this->getUserCourses($userArgs)
                );
            }

            return $returnResult;
        }

        protected function loginCourses()
        {
            global $UserID, $eclass_db;
             
            include_once __DIR__ . '/../libuser.php';
            $user = new libuser($UserID);
             
            $sql = "SELECT
    	               uc.user_course_id, uc.course_id, uc.user_id, uc.auth_token
    	            FROM
    	               " . $eclass_db . ".user_course AS uc INNER JOIN
    	               " . $eclass_db . ".course AS c ON uc.course_id = c.course_id
    	            WHERE
    	               c.RoomType = 0 AND
    	               uc.user_email = '".$user->UserEmail."' AND
    	               (
    	                   uc.status is NULL OR
    	                   uc.status NOT IN ('deleted')
    	               )";
            $result = $this->db->returnArray($sql);
             
            $resultAry = array();
             
            for($i=0; $i<count($result); $i++){
                $userCourseId = $result[$i]['user_course_id'];
                $courseId     = $result[$i]['course_id'];
                $userId       = $result[$i]['user_id'];
                $authToken    = $result[$i]['auth_token'];
                 
                if(!$authToken){
                    $sql = "UPDATE
    	                       " . $eclass_db . ".user_course
    	                    SET
    	                       auth_token = UUID()
    	                    WHERE
    	                       user_course_id = '" . $userCourseId . "'";
                    $this->db->db_db_query($sql);
                     
                    $sql = "SELECT
    	                       auth_token
    	                    FROM
    	                       " . $eclass_db . ".user_course
    	                    WHERE
    	                       user_course_id = '" . $userCourseId . "'";
                    $authToken = current($this->db->returnVector($sql));
                     
                    $sql = "UPDATE
    	                       " . classNamingDB($courseId) . ".usermaster
    	                    SET
    	                       auth_token = '".$authToken."'
    	                    WHERE
    	                       user_id = '".$userId."'";
                    $this->db->db_db_query($sql);
                }
                 
                $resultAry[$courseId] = $authToken;
            }
             
            return $resultAry;
        }

        protected function addIntranetUser($args)
        {
            global $intranet_db, $intranet_session_language, $intranet_root, $intranet_password_salt;
            if (! isset($args['userLogin']) || ! isset($args['password']) || ! isset($args['userEmail']) ||
                ! isset($args['englishName']) || ! isset($args['recordType']) || ! isset($args['gender']) ||
                ! isset($args['recordStatus'])) {
                    return 102; // Error Code - Invalid parameters
                }

                include_once (__DIR__ . "/../libauth.php");
                include_once (__DIR__ . "/../libregistry.php");

                $li = new libregistry();
                $lauth = new libauth();

                /* Check userLogin and userEmail */
                $sql = "
                    SELECT
                        COUNT(*)
                    FROM
                        " . $intranet_db . ".INTRANET_USER
                   WHERE
                            UserLogin = '" . $args['userLogin'] . "'
                                OR
                                UserEmail = '" . $args['userEmail'] . "'
               ";

                $result = current($this->db->returnVector($sql));

                if ($result > 0) {
                    return 102;
                }

                $chineseName = isset($args['chineseName']) ? $args['chineseName'] : '';
                $chineseTitle = isset($args['chineseTitle']) ? $args['chineseTitle'] : '';
                $englishTitle = isset($args['englishTitle']) ? $args['englishTitle'] : '';

                $optionalUpdateFields = '';
                $optionalUpdateFields .= isset($args['className']) ? "ClassName = '" . $args['className'] . "'," : "";
                $optionalUpdateFields .= isset($args['classNumber']) ? "ClassNumber = '" . $args['classNumber'] . "'," : "";
                $optionalUpdateFields .= $args['recordStatus'] === 1 ? "teaching = 1," : "";

                if ($li->UserNewAdd($args['userLogin'], $args['userEmail'], $args['englishName'], $chineseName,
                    $args['recordStatus'])) {
                        $intranetUserId = $li->db_insert_id();

                        $sql = "
                        UPDATE
                            " . $intranet_db . ".INTRANET_USER
                        SET
                            TitleEnglish = '" . $englishTitle . "',
                            TitleChinese = '" . $chineseTitle . "',
                            Gender = '" . $args['gender'] . "',
                            RecordType = '" . $args['recordType'] .
                            "',
                            HashedPass = MD5('" . $args['userLogin'] . $args['password'] . $intranet_password_salt . "'),
                            " . $optionalUpdateFields . "
                            DateModified = NOW()
                        WHERE
                            UserID = '" . $intranetUserId . "'
                        ";
                        $li->db_db_query($sql);

                        $lauth->UpdateEncryptedPassword($intranetUserId, $args['password']);

                        return $this->getIntranetUsers([
                            'intranetUserId' => $intranetUserId
                        ]);
                    } else {
                        return 807;
                    }
        }

        protected function addCourse($args)
        {
            global $intranet_session_language, $eclass_version, $eclass40_filepath, $intranet_root, $UserID, $SqlEclassUpdate;

            if (!isset($args['courseCode']) || !isset($args['courseName'])) {
                return 102; // Error Code - Invalid parameters
            }

            $courseDescription    = isset($args['courseDescription']) ? $args['courseDescription'] : '';
            $enablePowerLesson    = isset($args['enablePowerLesson']) ? $args['enablePowerLesson'] : true;
            $enableEquationEditor = isset($args['enableEquationEditor']) ? $args['enableEquationEditor'] : true;
            $maxUser              = isset($args['maxUser']) ? $args['maxUser'] : 1000;

            include_once(__DIR__ . "/../libuser.php");
            include_once(__DIR__ . "/../libeclass40.php");
            include_once(__DIR__ . "/../libaccess.php");
            include_once(__DIR__ . "/../user_right_target.php");
            include_once(__DIR__ . "/../icalendar.php");

            $libuser         = new libuser($UserID);
            $libeclass       = new libeclass();
            $libaccess       = new libaccess($UserID);
            $userRightTarget = new user_right_target();

            $libaccess->retrieveAccessEClass();
            $userAccessRight = $userRightTarget->Load_User_Right($UserID);
            if (!$userAccessRight['eLearning-eClass']) {
                if (
                    ($libuser->teaching && !$libaccess->isAccessEClassMgtCourse()) ||
                    (!$libuser->teaching && $libuser->RecordType == 1 && !$libaccess->isAccessEClassNTMgtCourse()) ||
                    $libuser->RecordType != 1 ||
                    !$libaccess->isAccessEClassMgt()
                    ) {
                        return 202;
                    }
            }

            if ($libeclass->license != "" && $libeclass->ticket() == 0) {
                return 202;
            } else {
                $courseId = $libeclass->eClassAdd($args['courseCode'], $args['courseName'], $courseDescription, $maxUser, '', $UserID);
            }

            if (!$courseId) {
                return 807;
            }

            # insert course Calendar
            $iCal  = new icalendar();
            $calID = $iCal->createSystemCalendar($args['courseName'], 3, "P", $courseDescription);
            $sql   = "Update course set CalID = '$calID' where course_id = '$courseId'";
            $libeclass->db_db_query($sql);

            # Set Equation Editor Rights
            if ($enableEquationEditor) {
                $content                     = trim(get_file_content($libeclass->filepath . "/files/equation.txt"));
                $coursesWithEquationEditor   = ($content === "") ? array() : explode(",", $content);
                $coursesWithEquationEditor[] = $courseId;
                write_file_content(implode(",", $coursesWithEquationEditor),
                    $libeclass->filepath . "/files/equation.txt");
            }

            # Set PowerLesson Rights
            if ($enablePowerLesson == 1) {
                $plcontent                = trim(get_file_content($libeclass->filepath . "/files/powerlesson.txt"));
                $coursesWithPowerLesson   = ($plcontent === "") ? array() : explode(",", $plcontent);
                $coursesWithPowerLesson[] = $courseId;
                write_file_content(implode(",", $coursesWithPowerLesson),
                    $libeclass->filepath . "/files/powerlesson.txt");
            }

            return $this->getCourse(['courseId' => $courseId]);
        }

        protected function getLessonProgressState($args)
        {
            if (!isset($args['courseId']) || !isset($args['lessonId'])) {
                return 102; // Error Code - Invalid parameters
            }
            
            $statement = $this->getCourseDb($args['courseId'])->prepare(
                "SELECT progress_state FROM powerlesson_lessons WHERE lesson_id = :lessonId"
            );
            $statement->execute(['lessonId' => $args['lessonId']]);
            $rows = $statement->fetch(PDO::FETCH_ASSOC);
            
            return ['progressState' => $rows['progress_state']];
        }
        
        protected function getLessonsOfCoursesForELearningTimetable($args)
        {
            global $UserID;
            
            if (!isset($args['startDate']) || !isset($args['endDate'])) {
                return 102; // Error Code - Invalid parameters
            }
            
            $args['intranetUserId'] = intval(isset($args['intranetUserId']) && $args['intranetUserId'] !== ''? $args['intranetUserId'] : $UserID);
            
            if(version_compare($this->getPowerLessonVersion(), '2.28', '<')){
                return []; // Return empty array because current PowerLesson Version does not support Elearning Timetable 
            }
            
            $startDate = $args['startDate'] instanceof DateTime? clone $args['startDate'] : new DateTime($args['startDate']);
            $startDate->setTimeZone(new DateTimeZone('UTC'));
            $startDate = $startDate->format('Y-m-d H:i:s');
            
            $endDate = $args['endDate'] instanceof DateTime? clone $args['endDate'] : new DateTime($args['endDate']);
            $endDate->setTimeZone(new DateTimeZone('UTC'));
            $endDate = $endDate->format('Y-m-d H:i:s');
            
            $getUserCourseArgs = ['intranetUserId'=>$args['intranetUserId']];
            if(isset($args['courseId'])){
                $getUserCourseArgs['courseId'] = $args['courseId'];
            }
            $userCourses = $this->getUserCourses($getUserCourseArgs);
            
            $courses = [];
            foreach($userCourses as $userCourse){
                $sql = "SELECT
                            l.lesson_id, l.title, l.subject_id,
                            p.phase_id, p.phase_type, p.start_date, p.end_date
                        FROM
                            powerlesson_phases AS p 
                        INNER JOIN
                            powerlesson_lessons AS l 
                        ON 
                            p.lesson_id = l.lesson_id AND
                            l.privacy_setting = 2 AND
                            p.show_in_elearning_timetable = 1 AND
                            p.start_date BETWEEN :startDate AND :endDate";

                switch ($userCourse['memberType']){
                    case 'S':
                        $sql .= "
                        INNER JOIN 
                            powerlesson_lesson_students AS ls 
                        ON 
                            l.lesson_id = ls.lesson_id
                        WHERE
                            ls.user_id = :userId";
                        break;
                    case 'T':
                        $sql .= "
                        INNER JOIN 
                            powerlesson_lesson_role_users AS lru 
                        ON 
                            l.lesson_id = lru.lesson
                        WHERE
                            lru.user = :userId AND
                            role = 'TEACHER_IN_CHARGE'";
                        break;
                    default:
                        continue;
                }

                $statement = $this->getCourseDb($userCourse['courseId'])->prepare($sql);
                $statement->execute([
                    'startDate' => $startDate,
                    'endDate'   => $endDate,
                    'userId'    => $userCourse['userId']
                ]);
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

                if(count($rows)){
                    $lessons = [];
                    foreach($rows as $row){
                        if(!isset($lessons[$row['lesson_id']])){
                            $lessons[$row['lesson_id']] = [
                                'id'        => $row['lesson_id'],
                                'title'     => $row['title'],
                                'subjectId' => $row['subject_id'],
                                'phases'    => []
                            ];
                        }

                        $phaseStartDate = DateTime::createFromFormat('Y-m-d H:i:s', $row['start_date'], new DateTimeZone('UTC'));
                        $phaseStartDate->setTimeZone(new DateTimeZone(date_default_timezone_get()));
                        $phaseStartDate = $phaseStartDate->format('Y-m-d H:i:s');

                        $phaseEndDate = DateTime::createFromFormat('Y-m-d H:i:s', $row['end_date'], new DateTimeZone('UTC'));
                        $phaseEndDate->setTimeZone(new DateTimeZone(date_default_timezone_get()));
                        $phaseEndDate = $phaseEndDate->format('Y-m-d H:i:s');

                        $lessons[$row['lesson_id']]['phases'][] = [
                            'id'        => $row['phase_id'],
                            'phaseType' => $row['phase_type'],
                            'startDate' => $phaseStartDate,
                            'endDate'   => $phaseEndDate
                        ];
                    }

                    $courses[] = [
                        'id'      => $userCourse['courseId'],
                        'lessons' => array_values($lessons)
                    ];
                }
            }
            
            return $courses;
        }
        
        protected function getPushMessageForPhase($args)
        {
            global $UserID;
            
            if (!isset($args['courseId']) || !isset($args['phaseId'])) {
                return 102; // Error Code - Invalid parameters
            }
            
            $courseDb = $this->getCourseDb($args['courseId']);
            
            # Get Title, Subject ID, Phase Type and Start Date
            $statement = $courseDb->prepare("
                SELECT
                    l.lesson_id, l.title, l.subject_id, p.phase_type, p.start_date
                FROM
                    powerlesson_lessons AS l INNER JOIN
                    powerlesson_phases AS p ON l.lesson_id = p.lesson_id
                WHERE
                    l.privacy_setting = 2 AND
                    p.phase_id = :phaseId
            ");
            $statement->execute(['phaseId'=>$args['phaseId']]);
            if($statement->rowCount() == 0){
                return 815; // No phase found with supplied phase id
            }
            list($lessonId, $lessonTitle, $subjectId, $phaseType, $startDate) = $statement->fetch(PDO::FETCH_NUM);
            
            # Check Permission - Teacher-in-charge / Lesson Collaborator / Classroom Collaborator
            $statement = $courseDb->prepare("
                SELECT 
                    lru.role_user_id 
                FROM 
                    powerlesson_lesson_role_users AS lru INNER JOIN
                    usermaster AS u ON lru.user = u.user_id
                WHERE
                    lru.lesson = :lessonId AND
                    lru.role IN ('TEACHER_IN_CHARGE', 'COLLABORATOR') AND
                    u.intranet_user_id = :intranetUserId 
            ");
            $statement->execute(['lessonId'=>$lessonId, 'intranetUserId'=>$UserID]);
            if($statement->rowCount() == 0){
                $statement = $courseDb->prepare("
                    SELECT
                        tcr.classroom_role_id
                    FROM
                        powerlesson_teacher_classroom_roles AS tcr INNER JOIN
                        usermaster AS u ON tcr.user_id = u.user_id
                    WHERE
                        tcr.role = 'COLLABORATOR' AND
                        u.intranet_user_id = :intranetUserId 
                ");
                $statement->execute(['intranetUserId'=>$UserID]);
                if($statement->rowCount() == 0){
                    return 815; // No permission to send push message
                }
            }
            
            # Get Target Students
            $statement = $courseDb->prepare("
                SELECT
                    DISTINCT u.intranet_user_id
                FROM
                    powerlesson_lessons AS l INNER JOIN
                    powerlesson_lesson_students AS ls ON l.lesson_id = ls.lesson_id INNER JOIN
                    usermaster AS u ON ls.user_id = u.user_id
                WHERE
                    l.lesson_id = :lessonId AND
                    memberType = 'S' AND 
                    (status IS NULL OR status NOT IN ('deleted'))
            ");
            $statement->execute(['lessonId'=>$lessonId]);
            $targetStudents = $statement->fetchAll(PDO::FETCH_COLUMN);
            
            return [
                'title'          => $lessonTitle,
                'subjectId'      => $subjectId,
                'phaseType'      => $phaseType,
                'startDate'      => (new DateTime($startDate, new DateTimeZone('UTC')))->format('Y-m-d\TH:i:sP'),
                'targetStudents' => $targetStudents
            ];
        }
        
        protected function getIntranetSettings($args)
        {
            global $intranet_root, $plugin, $eclassAppConfig;
            
            include_once __DIR__ . '/../libeclass40.php';
            $libEclass = new libeclass();
            
            include_once __DIR__ . '/../eClassApp/libeClassApp.php';
            $libEClassApp = new libeClassApp();
            
            return [
                'enableTimetable'  => $libEclass->isEnableELearningTimetable()? 1:0,
                'enableParentApp'  => ($plugin['eClassApp'] && $libEClassApp->isSchoolInLicense($eclassAppConfig['appType']['Parent'])? 1:0),
                'enableStudentApp' => ($plugin['eClassStudentApp'] && $libEClassApp->isSchoolInLicense($eclassAppConfig['appType']['Student'])? 1:0)
            ];
        }
        
        private function powerLessonApiCaller($apiUri, $method = 'GET', $courseId = null, $isEncryptCourseId = false, $requestData = array(), $authToken = null, $includeSystemToken = false) {
            global $web_protocol, $eclass40_httppath, $eclass_db, $config_school_code, $powerlesson2_config;

            include_once __DIR__ . '/../json.php';
            $jsonObj = new JSON_obj ();
            $method = strtoupper ( $method );

            /* Set apiUrl */
            $apiBaseUrl = (isset ( $web_protocol ) && preg_match ( '/^https?$/', $web_protocol ) === 1 ? $web_protocol : 'http') . '://';
            $apiBaseUrl .= rtrim ( $eclass40_httppath, '/' ) . '/src/powerlesson/api';
            $apiUrl = $apiBaseUrl . $apiUri;

            /* Set headers */
            $headers = array ();
            if(!is_null($courseId)){
                if ($isEncryptCourseId) {
                    $headers [] = 'X-EncryptCourseId: ' . $courseId;
                } else {
                    $headers [] = 'X-CourseId: ' . $courseId;
                }
            }
            if (! is_null ( $authToken )) {
                $headers [] = 'X-AuthToken: ' . $authToken;
            } elseif ($includeSystemToken) {
                $systemToken = md5($config_school_code . $powerlesson2_config['systemTokenSalt']);
                $headers [] = 'X-SystemToken: ' . $systemToken;
            }
            $headers [] = 'Content-Type: application/json';
            $headers [] = 'Accept: application/json';

            /* Set request data */
            $requestData = count ( $requestData ) ? $jsonObj->encode ( $requestData ) : '';

            /* Set curl option */

            $ch = curl_init ();
            curl_setopt ( $ch, CURLOPT_URL, $apiUrl );
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
            switch ($method) {
                case 'GET' :
                    break;
                case 'POST' :
                    curl_setopt ( $ch, CURLOPT_POST, true );
                    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $requestData );
                    break;
                case 'PUT' :
                    curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, "PUT" );
                    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $requestData );
                    $headers [] = 'Content-Length: ' . strlen ( $requestData );
                    break;
                case 'DELETE' :
                    curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, "DELETE" );
                    break;
            }
            curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
            curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 60 );
            curl_setopt ( $ch, CURLOPT_TIMEOUT, 300 );

            $rawResponse = curl_exec ( $ch );
            $apiResponse = $jsonObj->decode ( $rawResponse );
            $httpStatusCode = intval ( curl_getinfo ( $ch, CURLINFO_HTTP_CODE ) );
            $result = $httpStatusCode >= 200 && $httpStatusCode <= 299 ? true : false;

            return array (
                'result' => $result,
                'apiResponse' => $apiResponse
            );
        }
    }
}