<?php
# Editing by Bill

include_once($intranet_root."/lang/reportcard_custom/$ReportCardCustomSchoolName.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom()
	{
		$this->libreportcard();
		$this->configFilesType = array("summary", "award", "remark");
        
		// Temp control variables to enable / disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 0;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
        
		$this->EmptySymbol = '--';
	}
    
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID)
	{
	    global $eReportCard, $s, $numOfStudent;
	    
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
			$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
			$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
			$TableTop .= "<tr><td>".$MSTable."</td></tr>";
			$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
        
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		    $TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
	    $TableBottom .= "</table>";
        
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
// 				$x .= "<tr height='1135px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr height='1200px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		$x .= "</table>";
		$x .= "</td></tr>";
		$x .= "</table>";
		$x .= "</div>";
		
		$_pageBreak = (isset($s) && isset($numOfStudent) && ($s < $numOfStudent - 1))? ' style="page-break-after:always;" ' : '';
		
		$x .= "<div id='container2' ".$_pageBreak.">";
		$x .= "<table width='100%' border='0' cellpadding='02' cellspacing='0' valign='top'>";
		
		$x .= "<tr><td>";
    		$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
    			$x .= "<tr valign='top'><td>".$this->getReportRemarksPage($ReportID)."</td></tr>";
    		$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
    
	function getReportHeader($ReportID, $StudentID='')
	{
	    global $eReportCard;
	    
	    $ReportTitleArr = array();
	    $SemesterTitleArr = array();
	    if($ReportID)
	    {
	        # Retrieve Display Settings
	        $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
	        $SemID = $ReportSetting['Semester'];
	        $SemNum = intval($this->Get_Semester_Seq_Number($SemID));
	        
	        # Report Title
	        $ReportTitle = $ReportSetting['ReportTitle'];
	        $ReportTitleArr = explode(":_:", $ReportTitle);
	        if($ReportTitleArr[0] == '') {
	            $ReportTitleArr[0] = '&nbsp;';
	        }
	        if($ReportTitleArr[1] == '') {
	            $ReportTitleArr[1] = '&nbsp;';
	        }
	        
	        # Year & Semester Title
	        $ActiveYearName = $this->GET_ACTIVE_YEAR(" / ");
	        $SemesterTitleArr[0] = '';
	        $SemesterTitleArr[0] .= $ActiveYearName;
	        $SemesterTitleArr[0] .= ' ';
	        $SemesterTitleArr[0] .= $eReportCard['Template']['AcademicYearCh'];
	        $SemesterTitleArr[0] .= ' ';
	        $SemesterTitleArr[0] .= $eReportCard['Template']['Term'.$SemNum.'Ch'];
	        $SemesterTitleArr[1] = '';
	        $SemesterTitleArr[1] .= $eReportCard['Template']['AcademicYearEn'];
	        $SemesterTitleArr[1] .= ' ';
	        $SemesterTitleArr[1] .= $ActiveYearName;
	        $SemesterTitleArr[1] .= ' ';
	        $SemesterTitleArr[1] .= $this->Get_Term_Name_Display($eReportCard['Template']['Term'.$SemNum.'En']);
	    }
	    
	    # School Logo
	    $schoolLogo = '/file/reportcard2008/templates/sao_jose_5_log.png';
	    
		$html = '';
		$html .= "<table class='report_header' width='100%' border='0' cellspacing='0' cellpadding='0' align='center'>";
        	$html .= "<tr class='font_14pt'>";
        	    $html .= "<td rowspan='6' width='22%'>";
        	        $html .= "<img src='".$schoolLogo."' height='119px'>";
        	        $html .= "</td>";
    	        $html .= "<td rowspan='6' width='12%'>&nbsp;</td>";
                $html .= "<td rowspan='1' width='38%'>".$eReportCard['Template']['SchoolNameCh']."</td>";
                $html .= "<td rowspan='6' width='28%'>&nbsp;</td>";
            $html .= "</tr>";
        	$html .= "<tr class='font_14pt'><td>".$eReportCard['Template']['SchoolNameEn']."</td></tr>";
        	$html .= "<tr class='font_10pt' style='padding: 2px 0px;'><td>".$ReportTitleArr[0]."</td></tr>";
        	$html .= "<tr class='font_10pt' style='padding: 2px 0px;'><td>".$ReportTitleArr[1]."</td></tr>";
        	$html .= "<tr class='font_9pt'><td>".$SemesterTitleArr[0]."</td></tr>";
        	$html .= "<tr class='font_9pt'><td>".$SemesterTitleArr[1]."</td></tr>";
		$html .= "</table>";
		
		return $html;
	}
    
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;

		if($ReportID)
		{
		    # Retrieve Display Settings
		    $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		    $ClassLevelID = $ReportSetting['ClassLevelID'];
		    $SchoolType = $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		    $FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
		    $isPrimarySchool = $SchoolType == 'P';
		    $isHighSchool = !$isPrimarySchool && $FormNumber > 3;
		    
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			
			// Default value
			$defaultVal = 'XXX';
			$defaultVal2 = $StudentID? $this->EmptySymbol : '#';
			$data['TotalEnrolled'] = $defaultVal2;
			$data['Ranking'] = $defaultVal2;
			
			### Issue Date
			$DateOfIssue = $ReportSetting['Issued'];
			if (is_date_empty($DateOfIssue)) {
			    $data['DateOfIssue'] = '';
			} else {
			    $data['DateOfIssue'] = date('Y-m-d', strtotime($DateOfIssue));
			}
			
			# Retrieve Student Info
			if($StudentID)
			{
			    ### Student Name
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$StudentNameEn = $StudentInfoArr[0]['EnglishName'];
				$StudentNameCh = $StudentInfoArr[0]['ChineseName'];
				$data['StudentName'] = $StudentNameCh.'　'.$StudentNameEn;
				
				### Class Name
				$ClassNameCh = $StudentInfoArr[0]['ClassNameCh'];
                $data['Class'] = $ClassNameCh;
				//$ClassNameEn = $StudentInfoArr[0]['ClassName'];
				//$data['Class'] = $ClassNameCh.' '.$ClassNameEn;
				
				### ID & DSEJ Number
				$data['IDNumber'] = $StudentInfoArr[0]['HKID'];
				$data['DSEJNumber'] = $StudentInfoArr[0]['STRN'];
	           
				### Birth Date
				$DateOfBirth = $StudentInfoArr[0]['DateOfBirth'];
				if (is_date_empty($DateOfBirth)) {
				    $data['DateOfBirth'] = '';
				} else {
				    $data['DateOfBirth'] = date('Y-m-d', strtotime($DateOfBirth));
				}
				
				### Overall result data - Ranking & Total Enrolled
			    $result = $this->getReportResultScore($ReportID, 0, $StudentID);
			    $selfSubjectStream = $this->GET_STUDENT_SUBJECT_STREAM(array($StudentID));
			    $selfSS = $selfSubjectStream[0]['SubjectStream'];
			    if(!empty($result))
			    {
			        $data['TotalEnrolled'] = $result['ClassNoOfStudent'] > 0 ? $result['ClassNoOfStudent'] : $this->EmptySymbol;
			        if(!$isPrimarySchool){
				        if(!$isHighSchool){
				        	$data['Ranking'] = $result['OrderMeritClass'] > 0 && $result['OrderMeritClass'] <= 10 ? $result['OrderMeritClass'] : $this->EmptySymbol;
				        } else {
					        if($selfSS != ""){
					        	$classSubjectStream = $this->GET_CLASSCOUNT_SUBJECT_STREAM($StudentInfoArr[0]['ClassID']);
					        	// $data['TotalEnrolled'] = $selfSS == 'A' ? $classSubjectStream[0]['ArtCount'] : $classSubjectStream[0]['ScienceCount'];
					        	$data['TotalEnrolled'] = $result['StreamNoOfStudent'];
					        	$data['Ranking'] = $result['OrderMeritStream'] > 0 && $result['OrderMeritStream'] <= 10 ? $result['OrderMeritStream'] : $this->EmptySymbol;
					        } else { // 20191223 (Philips) - Secondary display top 3 only
					        	$data['Ranking'] = $result['OrderMeritClass'] > 0 && $result['OrderMeritClass'] <= 10 ? $result['OrderMeritClass'] : $this->EmptySymbol;
					        }
				        }
			        } else {
			        	$data['TotalEnrolled'] = $result['ClassNoOfStudent'];
			        	$data['Ranking'] = $result['OrderMeritClass'] > 0 && $result['OrderMeritClass'] <= 10 ? $result['OrderMeritClass'] : $this->EmptySymbol;
			        }
			    }
			    
			    ### Check High School Student Study Type
			    if($isHighSchool) {
                    $studentStudyType = $this->getStudentStudyType($StudentID);
			    }
			}
			
			$StudentInfoTable = '';
			$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='student_info'>";
				$StudentInfoTable .= "<colgroup>
                                        <col width='17.5%'/>
                                        <col width='14.5%'/>
                                        <col width='19%'/>
                                        <col width='15%'/>
                                        <col width='22%'/>
                                        <col width='12%'/> 
                                    </colgroup>";
				
				// loop Student Info cols
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
				    $SettingID = trim($StudentTitleArray[$i]);
				    
				    # Title & Data
				    $InfoTitle = $eReportCard['Template']['StudentInfo'][$SettingID.'Ch'] . ' ' . $eReportCard['Template']['StudentInfo'][$SettingID.'En'];
				    if($isHighSchool && ($SettingID == 'TotalEnrolled' || $SettingID == 'Ranking')) {
				        $InfoTitle = $eReportCard['Template']['StudentInfo'][$studentStudyType.$SettingID.'Ch'] . ' ' . $eReportCard['Template']['StudentInfo'][$studentStudyType.$SettingID.'En'];
				    }
				    if($SettingID == 'TotalEnrolled' && $selfSS != ''){
				    	$InfoTitle = $eReportCard['Template']['StudentInfo']['SubjectStream'][$selfSS]['Ch'] . ' ' . $eReportCard['Template']['StudentInfo']['SubjectStream'][$selfSS]['En'];
				    }
				    if($SettingID == 'Ranking' && $selfSS != ''){
				    	$ssType = $selfSS == 'A' ? 'Arts' : 'Sci';
				    	$InfoTitle = $eReportCard['Template']['StudentInfo'][$ssType.'RankingCh'] . ' ' . $eReportCard['Template']['StudentInfo'][$ssType.'RankingEn'];
				    }
				    $InfoData = $data[$SettingID]? $data[$SettingID] : $defaultVal;
				    
				    $title_colspan = "";
				    if($i == 0) {
				        $title_colspan = " colspan='3' ";
				    }
				    
				    if($i % $StudentInfoTableCol == 0) {
				        $StudentInfoTable .= "<tr>";
				    }
    				    $StudentInfoTable .= "<td class='tabletext'>".$InfoTitle."</td>";
    				    $StudentInfoTable .= "<td class='tabletext' $title_colspan>: &nbsp;".$InfoData."</td>";
				    if(($i+1) % $StudentInfoTableCol == 0) {
				        $StudentInfoTable .= "</tr>";
				    }
				    
				    if($i == 0) {
				        $i++;
				    }
				}
			$StudentInfoTable .= "</table>";
		}
		
		return $StudentInfoTable;
	}
    
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
        
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
        
		# Retrieve Column Header
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;
        
		# Retrieve Subject
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID);
		if (sizeof($MainSubjectArray) > 0) {
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		}
		$SubjectIDArray = array();
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID);
		if (sizeof($SubjectArray) > 0)
		{
			foreach($SubjectArray as $thisSubjectID => $SubjectNameArray[])
			{
				$SubjectIDArray[] = $thisSubjectID;			
			}
		}
		
		# Retrieve Marks
		//$MarksAry = $this->getMarks($ReportID, $StudentID);
		
		# Retrieve Mark Table
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		$isAllDropedAry = $MSTableReturned['isAllDroped'];
		$isAllColumnZeroWeightAry = $MSTableReturned['isAllColumnZeroWeight'];
		
		# Retrieve Subject columns
		$SubjectCol = $this->returnTemplateSubjectCol($ReportID, $ClassLevelID, $isAllColumnZeroWeightAry);
		$sizeofSubjectCol = sizeof($SubjectCol);
        
		##########################################
		# Start Generate Table
		##########################################
		
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='2' class='report_details'>";
		
		# Column Header
		$DetailsTable .= $ColHeader;
		
		// loop Main Subjects
		//for($i=0; $i<$sizeofSubjectCol; $i++)
		foreach($MainSubjectIDArray as $thisSubjectID)
		{
			if($thisSubjectID == '112'){
				$OralRow .= "<tr>";
				
				# Subject Col
				$OralRow .= $SubjectCol[$thisSubjectID];
				
				# Marks Col
				$OralRow .= $MarksDisplayAry[$thisSubjectID];
				
				$OralRow .= "</tr>";
				continue;
			}
// 			$isSub = 0;
// 			$thisSubjectID = $SubjectIDArray[$i];
//            
// 			# If all the marks is "*", then don't display
// 			if (sizeof($MarksAry[$thisSubjectID]) > 0)
// 			{
// 				$Droped = 1;
// 				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
// 					if($da['Grade']!="*")	$Droped=0;
// 			}
// 			if($Droped)	continue;
//			
// 			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
// 			if ($isSub == true)
// 				continue;
//
// 			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
// 				continue;
			
			# Check if display subject row
            if (($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false) && $isAllDropedAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				
				# Subject Col
				$DetailsTable .= $SubjectCol[$thisSubjectID];
				
				# Marks Col
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				
				$DetailsTable .= "</tr>";
			}
		}
        
		# Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr, $OralRow);
        
		$DetailsTable .= "</table>";
		
		##########################################
		# End Generate Table
		##########################################
        
		return $DetailsTable;
	}
    
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
        
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$SchoolType = $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		$isPrimarySchool = $SchoolType == 'P';
	    
		# Get Column Weight
		$ColumnData = $this->returnReportTemplateColumnData($ReportID);
		if($ReportType == 'T')
		{
		    $ColumnData = array();
		    $ConsolidatedReportArr = $this->Get_Consolidated_Report_List($ClassLevelID, $ReportID);
		    if(!empty($ConsolidatedReportArr))
		    {
		        $ColumnData = $this->returnReportTemplateColumnData($ConsolidatedReportArr[0]);
		        
		        // for 146 testing
		        //$ColumnData = $this->returnReportTemplateColumnData($ConsolidatedReportArr[1]);
		    }
		}
		$numOfColumn = count($ColumnData);
// 		$numOfRelatedTerms = $isPrimarySchool? 4 : 2;
		$numOfRelatedTerms = 2;
        
		// Column Display Style
//         $columnNameStyle = $isPrimarySchool? ' style="letter-spacing: -1px" ' : '';
		$columnNameStyle = ' style="letter-spacing: -1px" ';
        
		$row1 = '';
		$row2 = '';
		$row1 .= '<tr class="table_header">';
		$row2 .= '<tr class="table_header">';
		
			### Subject
			$thisTitleCh = $eReportCard['Template']['SubjectCh'];
			$thisTitleEn = $eReportCard['Template']['SubjectEn'];
			$row1 .= "<td valign='middle' class='border_top'>". $thisTitleCh. "</td>";
			$row2 .= "<td valign='middle' class='border_bottom'>". $thisTitleEn. "</td>";
			
			### Units
			$thisTitleCh = $eReportCard['Template']['UnitsCh'];
			$thisTitleEn = $eReportCard['Template']['UnitsEn'];
			$row1 .= "<td valign='middle' class='border_top border_right'>". $thisTitleCh. "</td>";
			$row2 .= "<td valign='middle' class='border_bottom border_right'>". $thisTitleEn. "</td>";
			
			### Terms
			$columnDataCount = 0;
			for($i=1; $i<=$numOfRelatedTerms; $i++)
			{
			    $thisTitleCh = $eReportCard['Template']['Term'.$i.'Ch'];
			    $thisTitleEn = $this->Get_Term_Name_Display($eReportCard['Template']['Term'.$i.'En']);
			    $row1 .= "<td colspan='2' valign='middle' class='border_top border_right'>". $thisTitleCh. " ". $thisTitleEn. "</td>";
			    
			    $thisTitleCh = $eReportCard['Template']['CACh'];
			    $thisTitleEn = $eReportCard['Template']['CAEn'];
			    $thisColumnWeight = $ColumnData[$columnDataCount]['DefaultWeight']? $ColumnData[$columnDataCount]['DefaultWeight'] * 100 : 0;
			    $row2 .= "<td colspan='1' valign='middle' class='border_bottom' $columnNameStyle>". $thisTitleCh. " ". $thisTitleEn. " ". $thisColumnWeight. "%</td>";
			    $columnDataCount++;
			    
			    $thisTitleCh = $eReportCard['Template']['ExamCh'];
			    $thisTitleEn = $eReportCard['Template']['ExamEn'];
			    $thisColumnWeight = $ColumnData[$columnDataCount]['DefaultWeight']? $ColumnData[$columnDataCount]['DefaultWeight'] * 100 : 0;
			    $row2 .= "<td colspan='1' valign='middle' class='border_bottom border_right' $columnNameStyle>". $thisTitleCh. " ". $thisTitleEn. " ". $thisColumnWeight. "%</td>";
			    $columnDataCount++;
			}
			
			/*if($SchoolType == 'P')
			{
			    ### Average
			    $thisTitleCh = $eReportCard['Template']['Average2Ch'];
			    $thisTitleEn = $eReportCard['Template']['Average2En'];
			    $row1 .= "<td valign='middle' class='border_top'>". $thisTitleCh. "</td>";
			    $row2 .= "<td valign='middle' class='border_bottom'>". $thisTitleEn. "</td>";
			}
			else
			{*/
			    ### Average
			    $thisTitleCh = $eReportCard['Template']['AverageCh'];
			    $thisTitleEn = $eReportCard['Template']['AverageEn'];
			    $row1 .= "<td valign='middle' class='border_top border_right'>". $thisTitleCh. "</td>";
			    $row2 .= "<td valign='middle' class='border_bottom border_right'>". $thisTitleEn. "</td>";
			    
			    ### Ranking
			    $thisTitleCh = $eReportCard['Template']['RankingCh'];
			    $thisTitleEn = $eReportCard['Template']['RankingEn'];
			    $row1 .= "<td valign='middle' class='border_top'>". $thisTitleCh. "</td>";
			    $row2 .= "<td valign='middle' class='border_bottom'>". $thisTitleEn. "</td>";
			/*}*/
		
		$row1 .= '</tr>';
		$row2 .= '</tr>';
		
		$x = '';
		/*if($SchoolType == 'P')
		{
    		$x .= '<colgroup>';
        		$x .= '<col style="width: 20%">';
        		$x .= '<col style="width: 6%">';
        		for($i=1; $i<=$numOfRelatedTerms; $i++)
        		{
            		$x .= '<col style="width: 8%">';
            		$x .= '<col style="width: 8.5%">';
        		}
                $x .= '<col style="width: 8%">';
    		$x .= '</colgroup>';
    		
		}
		else
		{*/
		    $x .= '<colgroup>';
    		    $x .= '<col style="width: 25%">';
    		    $x .= '<col style="width: 6%">';
    		    for($i=1; $i<=$numOfRelatedTerms; $i++)
    		    {
    		        $x .= '<col style="width: 11.5%">';
    		        $x .= '<col style="width: 11.5%">';
    		    }
    		    $x .= '<col style="width: 11.5%">';
    		    $x .= '<col style="width: 11.5%">';
		    $x .= '</colgroup>';
		/*}*/
		$x .= $row1;
		$x .= $row2;
		
		return array($x, $n, $e, $t);
	}
    
	function returnTemplateSubjectCol($ReportID, $ClassLevelID, $isAllColumnZeroWeightAry=array())
	{
		global $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$SchoolType = $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		$SectionType = $this->GET_SECTION_TYPE_BY_CLASSLEVEL($ClassLevelID);
		$isPrimarySchool = $SchoolType == 'P';
		$isPrimaryEnglishSection = $isPrimarySchool && $SectionType == 'E';
		
 		# Subject Cols
 		$x = array();
 		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID);
		if (sizeof($SubjectArray) > 0)
		{
		    // Get Subject Code Mapping
		    if($isPrimaryEnglishSection) {
                $SubjectCodeMapAry = $this->GET_SUBJECTS_CODEID_MAP($withComponent = 0, $mapByCode = 0);
		    }
		    
	 		foreach($SubjectArray as $SubjectID => $SubjectArr)
	 		{
		 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubjectID, "EN");
		 		$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubjectID, "CH");
		 		
		 		// Check if need to add special prefix to subject name
		 		$SubjectPrefix = '';
		 		if($isPrimaryEnglishSection) {
		 		    $SubjectCode = $SubjectCodeMapAry[$SubjectID];
		 		    foreach((array)$eRCTemplateSetting['SpecialSubjectNamePrefix'] as $thisPrefix => $SubjectCodeList) {
		 		        if(in_array($SubjectCode, (array)$SubjectCodeList)) {
		 		            $SubjectPrefix = $thisPrefix;
		 		            break;
		 		        }
		 		    }
		 		}
		 		
		 		if(strstr($SubjectChn, '(c)')){
		 			$SubjectPrefix = '(c)';
		 			$SubjectChn = str_replace('(c)', '', $SubjectChn);
		 		}

		 		// [2020-0602-1211-10164] 請幫我將帶"＊" 的選修課，不要左右對齊
                $SubjectPostfix = '';
                if(strstr($SubjectChn, '*') && strlen($SubjectChn) <= 13){
                    $SubjectChn = trim(str_replace('*', '', $SubjectChn));
                    $SubjectPostfix = '*';
                }
		 		
		 		if(strstr($SubjectEng, 'Physical Education')){
		 			$SubjectEng = 'Physical Ed.';
		 		}
		 		
		 		$t = "";
			 	$t .= "<td height='{$LineHeight}' valign='middle'>";
					$t .= "<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
 					    //$t .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}' style='text-align: left'>".$SubjectPrefix.$SubjectChn." &nbsp; ".$SubjectEng."</td></tr>";
						$t .= "<tr><td width='1'>&nbsp;&nbsp;</td>";
							$t .= "<td height='{$LineHeight}' style='text-align: left; width:5%; font-size:9px;'>". ($SubjectPrefix ? $SubjectPrefix : '&nbsp;' ) ."</td>";
							if(strlen($SubjectChn) > 12){
								$t .= "<td height='{$LineHeight}' style='text-align: left; width:44%;'>".$SubjectChn."</td>";
							} else {
								$t .= "<td height='{$LineHeight}' style='text-align: justify; text-align-last: justify; width:25%;'>".$SubjectChn."</td>";
								$t .= "<td height='{$LineHeight}' style='text-align: left; width:19%;'>".$SubjectPostfix."&nbsp;</td>";
							}
							$t .= "<td height='{$LineHeight}' style='text-align: left; width:49%;'>".$SubjectEng."</td>";
						$t .= "</tr>";
					$t .= "</table>";
				$t .= "</td>";
				$x[$SubjectID] = $t;
		 	}
		}
        
 		return $x;
	}
    
	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
 		# Retrieve Display Settings
 		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
 		$SignatureTitleArray = $eRCTemplateSetting['Signature']['Selection'];
 		
 		$row1 .= '<tr class="font_8pt">';
 		$row2 .= '<tr class="font_8pt footer_row">';
 		$row3 .= '<tr class="font_8pt footer_row">';
 		
 		for($i=0; $i<sizeof($SignatureTitleArray); $i++)
 		{
 		    $SettingID = trim($SignatureTitleArray[$i]);
 		    
 		    $row1 .= '<td>&nbsp;</td>';
 		    $row1 .= '<td class="border_bottom" height="35px">&nbsp;</td>';
 		    $row1 .= '<td>&nbsp;</td>';
 		    
 		    $row2 .= '<td>&nbsp;</td>';
 		    $row2 .= '<td>'.$eReportCard['Template'][$SettingID.'Ch'].'</td>';
 		    $row2 .= '<td>&nbsp;</td>';
 		    
 		    $row3 .= '<td>&nbsp;</td>';
 		    $row3 .= '<td>'.$eReportCard['Template'][$SettingID.'En'].'</td>';
 		    $row3 .= '<td>&nbsp;</td>';
	    }
	    
	    $row1 .= '</tr>';
	    $row2 .= '</tr>';
	    $row3 .= '</tr>';
	    
	    $x = '';
	    $x .= '<table width="100%" height="100%" cellspacing="0" cellpadding="2">';
	    $x .= '<colgroup>';
    	    for($i=0; $i<sizeof($SignatureTitleArray); $i++) {
    	        $x .= '<col width="1%"/>';
    	        $x .= '<col width="18%"/>';
    	        $x .= '<col width="1%"/>';
    	    }
	    $x .= '</colgroup>';
	    $x .= $row1;
	    $x .= $row2;
	    $x .= $row3;
	    
 		return $x;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array(), $OralRow = null)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
		$SemNum = intval($this->Get_Semester_Seq_Number($SemID));
		$ReportType = $SemID == "F" ? "W" : "T";
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$SchoolType = $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		
// 		$LineHeight 				= $ReportSetting['LineHeight'];
// 		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
// 		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
// 		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
// 		//$ShowNumOfStudentClass =true;
// 		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
// 		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
// 		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
///		//$ShowGrandTotal =false;
// 		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
// 		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
// 		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
// 		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
// 		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
//      
// 		$CalSetting = $this->LOAD_SETTING("Calculation");
// 		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
//      
// 		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# Retrieve Term Reports
		$TermReportIDArr = $this->GET_ALL_RELATED_TERM_REPORT_LIST($ReportID);
		
		# Retrieve Overall CSV data
		$ary = array();
		if($StudentID && $ReportType == 'W') {
    		$ary = $this->getReportOtherInfoData($ReportID, $StudentID);
    		$ary = $ary[$StudentID];
		}

        # Retrieve Overall Generated Conduct
        $conductAry = array();
        if($StudentID && $ReportType == 'W') {
            $studentConductGrade = $this->Get_Student_Conduct_Grade($ReportID, $StudentID);
            if(!empty($studentConductGrade)) {
                $conductAry[0] = $studentConductGrade[0]['FinalGrade'];
            }
        }
		
		# Retrieve Overall result data
		if($ReportType == 'T')
		{
		    // do nothing
		}
		else 
		{
		    if($StudentID) {
    		    $result = $this->getReportResultScore($ReportID, 0, $StudentID);
		    }
    		//$averageMark = $StudentID ? $result['GrandAverage'] : 'S';
            $averageMark = $StudentID ? $this->Get_Score_Display_HTML($this->ROUND_MARK($result['GrandAverage'], 'GrandAverage'), $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
		}
		
// 		$ClassPosition = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? $this->EmptySymbol: $result['OrderMeritClass']) : $this->EmptySymbol) :  $this->EmptySymbol;
//   	$ClassNumOfStudent = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? $this->EmptySymbol: $result['ClassNoOfStudent']) : $this->EmptySymbol) :  $this->EmptySymbol;
//   	$FormPosition = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? $this->EmptySymbol: $result['OrderMeritForm']) : $this->EmptySymbol) :  $this->EmptySymbol;
//   	$FormNumOfStudent = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? $this->EmptySymbol: $result['FormNoOfStudent']) : $this->EmptySymbol) :  $this->EmptySymbol;
		
		// loop Term Reports
		$isFirst = 1;
		foreach((array)$TermReportIDArr as $thisTermReportID)
		{
		    // No need to display next term report
		    if($ReportType == 'T' && $thisTermReportID == '')
		    {
		        continue;
		    }
		    
		    # Retrieve Term result data
		    if($StudentID) {
		        $columnResult = $this->getReportResultScore($thisTermReportID, 0, $StudentID);
		    }
		    //$columnAverage[$thisTermReportID] = $StudentID ? $columnResult['GrandAverage'] : 'S';
            $columnAverage[$thisTermReportID] = $StudentID ? $this->Get_Score_Display_HTML($this->ROUND_MARK($columnResult['GrandAverage'], 'GrandAverage'), $thisTermReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
		    
		    # Retrieve Term CSV data
		    if($StudentID && $ReportType == 'T')
		    {
		        $term_ary = $this->getReportOtherInfoData($thisTermReportID, $StudentID);
		        $term_ary = $term_ary[$StudentID];
		        $ary = $ary + $term_ary;
		    }

            # Retrieve Term Generated Conduct
		    // if($StudentID && $ReportType == 'T')
            if($StudentID)
            {
                $studentConductGrade = $this->Get_Student_Conduct_Grade($thisTermReportID, $StudentID);
                if(!empty($studentConductGrade)) {
                    $conductAry[$thisTermReportID] = $studentConductGrade[0]['FinalGrade'];
                }
            }
		}

		# Average Result
// 		if($ShowGrandTotal)
// 		{
// 			$thisTitleEn = $eReportCard['Template']['TotalAverageEn'];
// 			$thisTitleCh = $eReportCard['Template']['TotalAverageCh'];
// 			$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnAverage, $averageMark, $first);
// 		}
        
		if ($eRCTemplateSetting['HideCSVInfo'] != true)
		{
		    # Total Average
// 			if($ShowGrandAvg)
		    {
		        $thisTitleEn = $eReportCard['Template']['TotalAverageEn'];
		        $thisTitleCh = $eReportCard['Template']['TotalAverageCh'];
		        $x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnAverage, $averageMark, $first);
		    }
		    if($OralRow){
		    	$x .= $OralRow;
		    }
//		    
// 			# Number of Students in Form
// 			if($ShowNumOfStudentForm)
// 			{
// 				$thisTitleEn = $eReportCard['Template']['FormNumOfStudentEn'];
// 				$thisTitleCh = $eReportCard['Template']['FormNumOfStudentCh'];
// 				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormNumOfStudent, $FormNumOfStudent, $first);
//
// 				$first = 0;
// 			}
//
// 			# Position in Form
// 			if($ShowOverallPositionForm)
// 			{
// 				$thisTitleEn = $eReportCard['Template']['FormPositionEn'];
// 				$thisTitleCh = $eReportCard['Template']['FormPositionCh'];
//				
// 				// 2012-1120-1441-30156: request to disable the customization
// //				if($FormNumOfStudent!=0 && $FormNumOfStudent!='' && $FormNumOfStudent!=$this->EmptySymbol)
// //				{
// //					$FormPositionPercent = ($FormPosition/$FormNumOfStudent)*100;
// //				}
// //	
// //				if($FormPositionPercent<=50)
// //				{
// //					
// //				}
// //				else
// //				{
// //					$FormPosition = $this->EmptySymbol;
// //				}
//
// 				if ($OverallPositionRangeForm > 0 && $FormPosition > $OverallPositionRangeForm) {
// 					$FormPosition = $this->EmptySymbol;
// 				}
// 				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormPos, $FormPosition, $first, $OverallPositionRangeForm);
//
// 				$first = 0;
// 			}
//		
// 			# Number of Students in Class
// 			if($ShowNumOfStudentClass)
// 			{
// 				$thisTitleEn = $eReportCard['Template']['ClassNumOfStudentEn'];
// 				$thisTitleCh = $eReportCard['Template']['ClassNumOfStudentCh'];
//				
// 				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassNumOfStudent, $ClassNumOfStudent, $first);
//
// 				$first = 0;
// 			}	
//			
// 			# Position in Class
// 			if($ShowOverallPositionClass)
// 			{
// 				$thisTitleEn = $eReportCard['Template']['ClassPositionEn'];
// 				$thisTitleCh = $eReportCard['Template']['ClassPositionCh'];
//				
// 				if ($OverallPositionRangeClass > 0 && $ClassPosition > $OverallPositionRangeClass) {
// 					$ClassPosition = $this->EmptySymbol;
// 				}
// 				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassPos, $ClassPosition, $first, $OverallPositionRangeClass);
//
// 				$first = 0;
// 			}

			##################################################################################
			# CSV related
			##################################################################################

			$border_top = $first ? "border_top" : "";
			
			# Conduct
			$thisTitleEn = $eReportCard['Template']['ConductEn'];
			$thisTitleCh = $eReportCard['Template']['ConductCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Conduct", $ary, 1, $conductAry);
			
// 			# Late Attendance
// 			$thisTitleEn = $eReportCard['Template']['TimesLateEn'];
// 			$thisTitleCh = $eReportCard['Template']['TimesLateCh'];
// 			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "TimesLate", $ary);
//
// 			# Excused Absence (Periods)
// 			$thisTitleEn = $eReportCard['Template']['DaysAbsentEn'];
// 			$thisTitleCh = $eReportCard['Template']['DaysAbsentCh'];
// 			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "ExcusedAbsencePeriods", $ary);
//
// 			# Unexcused Absence (Periods)
// 			$thisTitleEn = $eReportCard['Template']['UnexcusedDaysAbsentEn'];
// 			$thisTitleCh = $eReportCard['Template']['UnexcusedDaysAbsentCh'];
// 			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "UnexcusedAbsencePeriods", $ary);
		}
		
// 		if ($ReportType == 'W')
// 		{
// 			# Comments & Final Comments
// 			$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
// 			$ClassTeacherComment = trim($CommentAry[0]);
// 			$ClassTeacherComment = ($ClassTeacherComment=='')? $this->EmptySymbol : $ClassTeacherComment;
// //			$ClassTeacherComment = convert2unicode($ClassTeacherComment, 1, 1);
//			
// 			$FinalComment = nl2br(trim($ary[0]['FinalComments']));
// 			if ($FinalComment != '') {
// 				$FinalCommentDisplay = $FinalComment;
// 			}
// 			else {
// 				// do not default show generated comment. otherwise need to cust master report also 
// 				//$FinalCommentArr = $this->Get_Student_Final_Comment($ReportID, $StudentID);
// 				//$FinalCommentDisplay = implode('<br />', (array)$FinalCommentArr[$StudentID]);
// 			}
//			
// 			$ExtraActivity_Str = $ary[0]['ExtraActivities'];
// 			$ExtraActivity_Str = ($ExtraActivity_Str=='')? $this->EmptySymbol:$ExtraActivity_Str;
// 			$Services_Str = $ary[0]['Services'];
// 			$Services_Str = ($Services_Str=='')? $this->EmptySymbol:$Services_Str;
// 			$AwardPunishment_Str = $ary[0]['AwardPunishment'];
// 			$AwardPunishment_Str = ($AwardPunishment_Str=='')? $this->EmptySymbol:$AwardPunishment_Str;
//		
// 			$FinalCommentDisplay = ($FinalCommentDisplay=='')? $this->EmptySymbol:$FinalCommentDisplay;
//			
// 			$thisTitleEn = $eReportCard['Template']['CommentEn'];
// 			$thisTitleCh = $eReportCard['Template']['CommentCh'];
//			
// 			$x .='<tr height="40px">
// 					   <td class="font_8pt border_top" style="text-align:left;padding-left:8px;">'.$thisTitleCh.'</td>
// 					   <td class="font_8pt border_top" colspan="2" style="text-align:left;padding-left:8px;">'.$thisTitleEn.'</td>	
// 					   <td class="font_8pt border_top border_left" colspan="5" style="text-align:left;padding-left:8px;vertical-align:top;">'.$ClassTeacherComment.'</td>
// 				  </tr>';
//			
// 			$x .= '<tr>';
// 				$x .= '<td colspan="8" class="border_top" style="padding:0px;">';
// 					# Comment
// 					$x .= '<table width="100%" height="100%" cellspacing="0" cellpadding="2" class="font_8pt">';
// 						$x .='<col width="25%"/>';
// 						$x .='<col width="25%"/>';
// 						$x .='<col width="25%"/>';
// 						$x .='<col width="25%"/>';
//					
// 						$x .= '<tr>';
// 							$x .= '<td class="border_right" align="center">'.$eReportCard['Template']['ExtraActivityCh'].' '.$eReportCard['Template']['ExtraActivityEn'].'</td>';
// 							$x .= '<td class="border_right" align="center">'.$eReportCard['Template']['ServiceCh'].' '.$eReportCard['Template']['ServiceEn'].'</td>';
// 							$x .= '<td class="border_right" align="center">'.$eReportCard['Template']['AwardPunishCh'].' '.$eReportCard['Template']['AwardPunishEn'].'</td>';
// 							$x .= '<td class="" align="center">'.$eReportCard['Template']['FinalCommentCh'].' '.$eReportCard['Template']['FinalCommentEn'].'</td>';
// 						$x .='</tr>';
//						
// 						$x .= '<tr style="height:50px; text-align:left;">';
// 							$x .= '<td class="border_right border_top">'.$ExtraActivity_Str.'</td>';
// 							$x .= '<td class="border_right border_top">'.$Services_Str.'</td>';
// 							$x .= '<td class="border_right border_top">'.$AwardPunishment_Str.'</td>';
// 							$x .= '<td class="border_top">'.$FinalCommentDisplay.'</td>';
// 						$x .= '</tr>';
// 					$x .= '</table>';
// 				$x .= '</td>';
//				
// 			$x .= '</tr>';
// 		}

		return $x;
	}

	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting;
        
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
		$SemNum = intval($this->Get_Semester_Seq_Number($SemID));
 		$ReportType = $SemID == "F" ? "W" : "T";
 		$ClassLevelID = $ReportSetting['ClassLevelID'];
        $FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
 		$SchoolType = $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
        $SectionType = $this->GET_SECTION_TYPE_BY_CLASSLEVEL_NAME($ClassLevelID);

 		$isPrimarySchool = $SchoolType == 'P';
        $isSecondarySchool = $SchoolType == 'S';
        $isSecondaryJuniorLevel = $isSecondarySchool && $FormNumber <= 3;
        $isEnglishSection = $SectionType == 'ES';
 		
//  	$LineHeight = $ReportSetting['LineHeight'];
// 		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
// 		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
// 		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
// 		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
// 		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
// 		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
// 		$ShowRightestColumn = $ShowSubjectOverall;
        
		# Retrieve Calculation Settings
// 		$CalSetting = $this->LOAD_SETTING("Calculation");
// 		$UseWeightedMark = $CalSetting['UseWeightedMark'];
        
		# Retrieve Storage Settings
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectScoreDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
        
		# Retrieve Subject
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID);
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID);
		if(sizeof($MainSubjectArray) > 0) {
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		}
		
		# Retrieve Subject Weight
		$WeightAssoArr = array();
		if($ReportType == 'T')
		{
		    $ConsolidatedReportArr = $this->Get_Consolidated_Report_List($ClassLevelID, $ReportID);
		    if(!empty($ConsolidatedReportArr))
		    {
		        $ConsolidatedReportID = $ConsolidatedReportArr[0];
		        
		        // for 146 testing
		        //$ConsolidatedReportID = $ConsolidatedReportArr[1];
		        
		        $WeightArr = $this->returnReportTemplateSubjectWeightData($ConsolidatedReportID, $other_condition="", $MainSubjectIDArray, $ReportColumnID='null', $convertEmptyToZero=true);
		    }
		}
		else
		{
    		$WeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $other_condition="", $MainSubjectIDArray, $ReportColumnID='null', $convertEmptyToZero=true);
		}
		if(!empty($WeightArr))
		{
		    $WeightAssoArr = BuildMultiKeyAssoc($WeightArr, 'SubjectID', array('Weight'), $SingleValue=1);
		}

		# Retrieve Subejct Code Mapping
        $SubjectCodeMapAry = $this->GET_SUBJECTS_CODEID_MAP($withComponent=0, $mapByCode=0);

        # Get Term Reports
		$TermReportIDArr = $this->GET_ALL_RELATED_TERM_REPORT_LIST($ReportID);

		# Get Term Subject Group
        if($StudentID && $ReportType == 'W' && $isSecondarySchool && !empty($TermReportIDArr)) {
            $lastTermReportID = $TermReportIDArr[count($TermReportIDArr) - 1];
            $lastTermSubjectGroupList = $this->Get_Subject_Group_List_Of_Report($lastTermReportID, '', '', $IncludeSubject=true);
        }
		
		$n = 0;
		$x = array();
		
		// loop Subjects
		foreach((array)$SubjectArray as $SubjectID => $SubjectName)
		{
		    // SKIP > Component Subject
		    if(in_array($SubjectID, (array)$MainSubjectIDArray) != true) {
		        continue;
		    }
            $SubjectCode = $SubjectCodeMapAry[$SubjectID];

            // 帶*的科目不顯示排名 (中文部高一、二、三級、英文部Form 4-6)
            // [2020-0608-1140-10164] Form 4AB、5AB、6AB（注意：不要影響到高一至高三級）這6班的全年成績表，帶＂*＂的科目需要顯示科目級排名
            $hideClassOrderDisplay = false;
            if($isSecondarySchool && !$isEnglishSection && !$isSecondaryJuniorLevel) {
                $SubjectNameCh = $this->GET_SUBJECT_NAME_LANG($SubjectID, "CH");
                $hideClassOrderDisplay = strpos($SubjectNameCh, '*') !== false;
            }

            $isAllNA = true;
		    $isAllDroped = true;
		    
		    // loop Term Reports
		    $isFirst = 1;
    		foreach((array)$TermReportIDArr as $thisTermReportID)
    		{
    		    # No need to display next term report
    		    if($ReportType == 'T' && $thisTermReportID == '')
    		    {
    		    	if(in_array($SubjectCode, (array)$eRCTemplateSetting['SubjectDisplayOverallResult'])){
    		    		$x[$SubjectID] .= "<td colspan='2' class='border_right'>".$this->EmptySymbol."</td>";
    		    	} else {
	    		        for($i=0; $i<2; $i++)
	    		        {
	    		            $css_border_left = ($i > 0)? 'border_right' : '';
	    		            $x[$SubjectID] .= "<td class='{$css_border_left}'>".$this->EmptySymbol."</td>";
	    		        }
    		    	}
    		    	continue;
    		    }
    		    
//     			# Retrieve Term Report Display Settings
//     			$TermReportSetting = $this->returnReportTemplateBasicInfo($thisTermReportID);
//     			$thisSemNum = intval($this->Get_Semester_Seq_Number($TermReportSetting['Semester']));
    			
    		    # Retrieve Term Report Columns
    			$ColumnID = array();
    			$ColumnTitle = array();
    			$ColumnTitleArr = $this->returnReportColoumnTitle($thisTermReportID);
    			if(sizeof((array)$ColumnTitleArr) > 0) {
    			    foreach ((array)$ColumnTitleArr as $ColumnID[] => $ColumnTitle[]);
    			}
//     			$numOfColumn = count($ColumnID);
				$numOfColumn = 2;
    			
    			# Retrieve Term Report Marks
    			$thisReportMarksAry = $this->getMarks($thisTermReportID, $StudentID);
				
				# Retrieve Subject Grading Scheme & Settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, $withGrandResult=0, $returnAsso=0, $thisTermReportID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				# Subject Weight (Unit)
				if($isFirst)
				{
    				$thisSubjectWeight = $WeightAssoArr[$SubjectID];
    				if ($thisSubjectWeight != '') {
    				    $thisDisplay = my_round($thisSubjectWeight, 1);
    				} else {
    					$thisDisplay = $this->EmptySymbol;
    				}

                    # [2019-0715-0926-10066] for special subject "閱讀"
                    if(in_array($SubjectCode, (array)$eRCTemplateSetting['SubjectDisplayOverallResult'])) {
                        $thisDisplay = "- -";
                    }
                    
                    if($SubjectID == '112'){
                    	$thisDisplay = '&nbsp;';
                    }

    				$x[$SubjectID] .= "<td align='center' class='border_right'>".$thisDisplay."</td>";
				}
				if(in_array($SubjectCode, (array)$eRCTemplateSetting['SubjectDisplayOverallResult'])) {
					$thisGrade = $thisReportMarksAry[$SubjectID][0]['Grade'];
					$thisMarkDisplay = $thisGrade ? $thisGrade : $this->EmptySymbol;
					$thisMark = $thisMarkDisplay;
					
					if ($thisMark != '' && $thisMark != "N.A.") {
						$isAllNA = false;
					}
					if ($thisMark != "*") {
						$isAllDroped = false;
					}
					
					# Check special case
					list($thisMark, $needStyle) = $this->checkSpCase($thisTermReportID, $SubjectID, $thisMark, $thisReportMarksAry[$SubjectID][$thisColumnID]['Grade']);
					if($needStyle) {
						if ($thisSubjectWeight > 0 && $ScaleDisplay=="M") {
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight != 0)? $thisMark / $thisSubjectWeight : $thisMark;
						} else {
							$thisMarkTemp = $thisMark;
						}
						$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $thisTermReportID, $ClassLevelID, $SubjectID, $thisMarkTemp, '', '', '', '', 0, $SubjectScoreDecimal);
					}
					else {
						$thisMarkDisplay = $thisMark;
					}
					$x[$SubjectID] .= "<td colspan='2' class='border_right'>$thisMarkDisplay</td>";
					$isAllNA = false;
					$isAllDroped = false;
				} else {
					# Assessment Marks
					for($i=0; $i<$numOfColumn; $i++)
					{
						$thisColumnID = $ColumnID[$i];
						$thisMSGrade = $thisReportMarksAry[$SubjectID][$thisColumnID]['Grade'];
						$thisMSMark = $thisReportMarksAry[$SubjectID][$thisColumnID]['Mark'];
						
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='')? $thisMSGrade : "";
						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='')? $thisMSMark : "";
						
						# for preview purpose
						if(!$StudentID) {
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark))? $thisMark : $thisGrade;
						
						if ($thisMark != '' && $thisMark != "N.A.") {
							$isAllNA = false;
						}
						if ($thisMark != "*") {
						    $isAllDroped = false;
						}
						
						# Check special case
						list($thisMark, $needStyle) = $this->checkSpCase($thisTermReportID, $SubjectID, $thisMark, $thisReportMarksAry[$SubjectID][$thisColumnID]['Grade']);
						if($needStyle) {
							if ($thisSubjectWeight > 0 && $ScaleDisplay=="M") {
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight != 0)? $thisMark / $thisSubjectWeight : $thisMark;
							} else {
								$thisMarkTemp = $thisMark;
							}
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $thisTermReportID, $ClassLevelID, $SubjectID, $thisMarkTemp, '', '', '', '', 0, $SubjectScoreDecimal);
						}
						else {
							$thisMarkDisplay = $thisMark;
						}
						
						$css_border_left = ($i > 0)? 'border_right' : '';
						$x[$SubjectID] .= "<td class='{$css_border_left}'>";
							$x[$SubjectID] .= $thisMarkDisplay;
						$x[$SubjectID] .= "</td>";
					}
					
					if($numOfColumn < 2)
					{
					    for($j=0; $j<(2 - $numOfColumn); $j++)
					    {
					        $css_border_left = ($i == 1 || $j > 0)? 'border_right' : '';
					        $x[$SubjectID] .= "<td class='{$css_border_left}'>&nbsp;</td>";
					    }
					}
				}
				
				$isFirst = 0;
    		}
			
			# Overall Marks & Ranking
			if($ReportType == 'T')
			{
			    /*if($isPrimarySchool)
			    {
			        // do nothing
			    }
			    else
			    {*/
			        $x[$SubjectID] .= "<td class='border_right'>".$this->EmptySymbol."</td>";
			    /*}*/

                $x[$SubjectID] .= "<td>".$this->EmptySymbol.' / '.$this->EmptySymbol."</td>";
			}
			else
			{
			    $MarksAry = $this->getMarks($ReportID, $StudentID);
			    $thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
			    $thisMSMark = $MarksAry[$SubjectID][0]['Mark'];
			    
			    $thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='')? $thisMSGrade : "";
			    $thisMark = ($ScaleDisplay=="M" && $thisGrade=='')? $thisMSMark : "";
			    
			    # for preview purpose
			    if(!$StudentID)
			    {
			        $thisMark 	= $ScaleDisplay=="M" ? "S" : "";
			        $thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
			    }
			    $thisMark = ($ScaleDisplay=="M" && strlen($thisMark))? $thisMark : $thisGrade;
			    
			    if ($thisMark != '' && $thisMark != "N.A.") {
			        $isAllNA = false;
			    }
			    if ($thisMark != "*") {
			        $isAllDroped = false;
			    }
			    
// 			    # Failed Subject Handling
// 			    $thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
// 			    if($thisNature == 'Fail')
// 			    {
//     			    // Get Mark-up Exam Result
// 			        $SupplementaryExamMark = $this->GET_EXTRA_SUBJECT_INFO(array($StudentID), $SubjectID, $ReportID);
// 			        if(!empty($SupplementaryExamMark)) {
// 			            $thisMark = $SupplementaryExamMark[$StudentID]['Info'];
// 			        }
// 			    }
			    
			    # Check special case
			    list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
			    if($needStyle) {
			        $thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, '', '', '', '', '', 0, $SubjectTotalDecimal);
			    } else {
			        $thisMarkDisplay = $thisMark;
			    }
			    
// 			    $border_right = $isPrimarySchool? '' : ' border_right ';
			    $border_right = ' border_right ';
			    
			    $x[$SubjectID] .= "<td class='$border_right'>";
			        $x[$SubjectID] .= $thisMarkDisplay;
			    $x[$SubjectID] .= "</td>";

			    // [2020-0525-1451-12066]
                $thisFormOrder = $MarksAry[$SubjectID][0]['OrderMeritForm'];
                $thisFormTotal = $MarksAry[$SubjectID][0]['FormNoOfStudent'];

                $displayOrderLimit = 3;
			    if($SchoolType == 'P')
			    {
			        /*
                    $thisClassOrder = $MarksAry[$SubjectID][0]['OrderMeritClass'];
                    $thisClassTotal = $MarksAry[$SubjectID][0]['ClassNoOfStudent'];
                    if($thisClassOrder > 0 && $thisClassOrder <= 10) {
			    	    $thisMarkDisplay = $thisClassOrder.' / '.$thisClassTotal;
                    } else {
                        $thisMarkDisplay = $this->EmptySymbol.' / '.$thisClassTotal;
                    }
                    */

                    //if ($thisFormOrder > 0 && $thisFormOrder <= 3) {
                    if ($thisFormOrder > 0 && $thisFormOrder <= $displayOrderLimit) {
			    		$thisMarkDisplay = $thisFormOrder.' / '.$thisFormTotal;
			    	} else {
			    		$thisMarkDisplay = $this->EmptySymbol.' / '.$thisFormTotal;
			    	}
			    }
			    else
			    {
			        // Display Form Ordering
			        //if($isSecondaryJuniorLevel)
                    //{
                    	//$thisFormOrder = $MarksAry[$SubjectID][0]['OrderMeritForm'];
                    	//$thisFormTotal = $MarksAry[$SubjectID][0]['FormNoOfStudent'];
                    	//if($thisFormOrder > 0) {
                    	//	$thisMarkDisplay = $thisFormOrder.' / '.$thisFormTotal;
                    	//} else {
                    	//	$thisMarkDisplay = $this->EmptySymbol.' / '.$thisFormTotal;
                    	//}
                    //}
                    // Display Class Ordering
                    //else
                    {
                        /*
                        // [2020-0525-1451-12066] Request to use subject group total for ranking
                        $isShowSubjectGroupOrder = false;
                        if($StudentID && $isSecondarySchool && isset($lastTermSubjectGroupList) && isset($lastTermSubjectGroupList[$SubjectID])) {
                            $studentClassLevelInfo = $this->Get_Student_Class_ClassLevel_Info($StudentID);
                            if(!empty($studentClassLevelInfo)) {
                                $classStreamSettings = $this->GET_CLASSCOUNT_SUBJECT_STREAM($studentClassLevelInfo[0]['ClassID']);
                                if(!empty($classStreamSettings) && ($classStreamSettings[0]['ArtCount'] > 0 || $classStreamSettings[0]['ScienceCount'] > 0)) {
                                    $isShowSubjectGroupOrder = count($lastTermSubjectGroupList[$SubjectID]) == 1;
                                }
                            }
                        }

                        $thisClassOrder = $MarksAry[$SubjectID][0]['OrderMeritClass'];
                        $thisClassTotal = $MarksAry[$SubjectID][0]['ClassNoOfStudent'];
                        if($isShowSubjectGroupOrder) {
                            $thisClassOrder = $MarksAry[$SubjectID][0]['OrderMeritForm'];
                            $thisClassTotal = $MarksAry[$SubjectID][0]['FormNoOfStudent'];
                        }
                        */

                        ## 20191230 (Philips) [2019-0704-1858-08289] - Use Subject Stream Settings
                        if ($eRCTemplateSetting['Settings']['StudentSettings_SubjectStream'] && in_array($SubjectID, $eRCTemplateSetting['Settings']['SubjectSettings_SubjectStream'])) {
                        	$thisStudentSubjectStream = $this->GET_STUDENT_SUBJECT_STREAM(array($StudentID));
                        	if ($thisStudentSubjectStream[0]['SubjectStream'] != '') {
                                $thisFormOrder = $MarksAry[$SubjectID][0]['OrderMeritStream'];
                                $thisFormTotal = $MarksAry[$SubjectID][0]['StreamNoOfStudent'];
                            }
                        }

                        /*
                        if ($hideClassOrderDisplay) {
                            $thisMarkDisplay = $this->EmptySymbol.' / '.$this->EmptySymbol;
                        } else if($thisClassOrder > 0 && $thisClassOrder <= 3) {
                            $thisMarkDisplay = $thisClassOrder.' / '.$thisClassTotal;
                        } else {
                            $thisMarkDisplay = $this->EmptySymbol.' / '.$thisClassTotal;
                        }
                        */

                        //$displayOrderLimit = $isSecondaryJuniorLevel ? 10 : 3;
                        if ($thisFormOrder > 0 && $thisFormOrder <= $displayOrderLimit) {
                            $thisMarkDisplay = $thisFormOrder.' / '.$thisFormTotal;
                        } else {
                            $thisMarkDisplay = $this->EmptySymbol.' / '.$thisFormTotal;
                        }
                    }
			    }
			    
			    # for preview purpose
			    if(!$StudentID)
			    {
			    	$thisMarkDisplay = "# / #";
			    }

                if($hideClassOrderDisplay)
                {
                    $thisMarkDisplay = $this->EmptySymbol.' / '.$this->EmptySymbol;
                }
			    
			    # [2019-0715-0926-10066] for special subject "閱讀"
			    if(in_array($SubjectCode, (array)$eRCTemplateSetting['SubjectDisplayOverallResult']))
			    {
			    	// $thisMarkDisplay = "- -";
			    	$thisMarkDisplay = $this->EmptySymbol;
			    }
			    
			    $x[$SubjectID] .= "<td>";
			    $x[$SubjectID] .= $thisMarkDisplay;
			    $x[$SubjectID] .= "</td>";
			}
			
			# Construct an array
			$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
			$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			$returnArr['isAllDroped'][$SubjectID] = $isAllDroped;
			$returnArr['isAllColumnZeroWeight'][$SubjectID] = $thisAllColumnWeightZero;
		}
        
		return $returnArr;
	}
    
	function getMiscTable($ReportID, $StudentID='')
	{
	    global $eReportCard, $eRCTemplateSetting;
	    
	    # Retrieve Display Settings
	    $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
	    $SemID = $ReportSetting['Semester'];
	    $SemNum = intval($this->Get_Semester_Seq_Number($SemID));
	    $ReportType = $SemID == "F" ? "W" : "T";
	    $ClassLevelID = $ReportSetting['ClassLevelID'];
	    $SchoolType = $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
	    $isPrimarySchool = $SchoolType == 'P';
	    
	    # Get Term Reports
	    $TermReportIDArr = $this->GET_ALL_RELATED_TERM_REPORT_LIST($ReportID);
	    
	    // default value
	    $defaultVal = 'XXX';
	    
	    # Get Misc related data
	    $ary = array();
	    $TeacherComment = '';
        $isApplyManualInputComment = false;
	    $meritDataAry = array();
	    $enrolDataAry = array();
	    $attendanceDataAry = array();
	    if($StudentID)
	    {
	        # Other Info data
            $ary = $this->getReportOtherInfoData($ReportID, $StudentID);
            if(!empty($ary))
            {
                if($ReportType == 'T') {
                    $ary = $ary[$StudentID][$SemID];
                } else {
                    //$lastSemID = $this->Get_Last_Semester_Of_Report($ReportID);
                    //$ary = $ary[$StudentID][$lastSemID];
                    $ary = $ary[$StudentID][0];
                }
            }

            # Teacher's Comment
            //$TeacherComment = $this->returnSubjectTeacherComment($ReportID, $StudentID);
            //$TeacherComment = $TeacherComment[0];
            $TeacherComment = $this->GET_TEACHER_COMMENT(array($StudentID), 0, $ReportID);
            if(isset($TeacherComment[$StudentID]))
            {
                if($TeacherComment[$StudentID]['AdditionalComment'] != '') {
                    $TeacherComment = $TeacherComment[$StudentID]['AdditionalComment'];
                    $isApplyManualInputComment = true;
                }
                else if($TeacherComment[$StudentID]['Comment'] != '') {
                    // [2020-0708-1015-43164]
                    //$TeacherComment = $TeacherComment[$StudentID]['Comment'];
                    $TeacherComment = str_replace('
', '', $TeacherComment[$StudentID]['Comment']);
                }
                else {
                    $TeacherComment = '';
                }
            }
            else
            {
                $TeacherComment = '';
            }

            # eDiscipline data
            $meritDataAry = $this->loadingMeritFromDiscipline($ReportID, $StudentID);
            
            # eEnrolment data
            $enrolDataAry = $this->loadingDataFromEnrolment($ReportID, $StudentID);
            
            # eAttendance data
            $attendanceDataAry = $this->retrieveAttandanceMonthData($ReportID, $StudentID);

            # Promotion Remarks
            if($ReportType == 'W')
            {
                $promotionRemarksArr = $this->Get_Promotion_Status_Remarks();
                $promotionRemarksArr = BuildMultiKeyAssoc($promotionRemarksArr, array('ClassLevelID', 'PromotionType'), 'Remarks', 1, 0);

                $promotionTypeArr = $eRCTemplateSetting['ReportGeneration']['PromoteInputMapping'];
                $promotionTypeArr = array_flip($promotionTypeArr);
            }
	    }
	    
	    $MiscTable = "<table width='100%' border='0' cellspacing='0' cellpadding='2' class='misc_content'>";
    	    $MiscTable .= "<colgroup>
                                <col width='11%'/>
                                <col width='11%'/>
                                <col width='1%'/>
                                <col width='21%'/>
                                <col width='6%'/>
                                <col width='11%'/>
                                <col width='39%'/>
                            </colgroup>";
    	    
    	    # Comment
    	    $TeacherCommentDisplay = $defaultVal;
    	    if($StudentID)
    	    {
    	        $TeacherCommentDisplay = '&nbsp;';
    	        if($TeacherComment != '')
    	        {
                    if($isApplyManualInputComment)
                    {
                        $TeacherCommentDisplay = nl2br($TeacherComment);
                    }
                    else
                    {
                        $TeacherComment = explode('<br />', nl2br($TeacherComment));

                        $TeacherCommentArr = array();
                        foreach($TeacherComment as $thisTeacherComment) {
                            //$TeacherCommentArr[] = trim($thisTeacherComment);
                            $TeacherCommentArr[] = $thisTeacherComment;
                        }

                        // $TeacherCommentDisplay = implode('，', $TeacherCommentArr);
                        $TeacherCommentArr = array_filter($TeacherCommentArr);
                        $TeacherCommentDisplay = implode('', $TeacherCommentArr);
                    }
                }
    	    }
    	    
    	    $MiscTable .= "<tr>";
                $MiscTable .= "<td class='border_top border_right' height='50px'>".$eReportCard['Template']['CommentCh']."<br/>".$eReportCard['Template']['CommentEn']."</td>";
                $MiscTable .= "<td colspan='6' align='left' class='border_top' style='padding-left: 8px; '>";
                    $MiscTable .= $TeacherCommentDisplay;
                $MiscTable .= "</td>";
    	    $MiscTable .= "</tr>";
    	    
    	    # Extra-Curricular Activities
    	    $ECADisplay = $StudentID? '&nbsp;' : $defaultVal;
    	    $PerformanceDisplay = $StudentID? '&nbsp;' : 'X';
    	    
    	    if($StudentID){
	    	    $TermData = $enrolDataAry['ECA'];
	    	    $Term1Data = $TermData[0][0];
	    	    if($Term1Data){
	    	    	$ECADisplay = $Term1Data['ClubTitleB5'].'<br/>'.$Term1Data['ClubTitleEN'];
	    	    } else {
	    	    	$ECADisplay = $this->EmptySymbol;
	    	    }
	    	    $PerformanceDisplay = $Term1Data['Performance'] ? $Term1Data['Performance'] : $this->EmptySymbol;
	    	    
	    	    if($SemNum == '2' || $ReportType == 'W'){
		    	    $Term2Data = $TermData[1][0];
		    	    if($Term2Data){
		    	    	$ECA2Display = $Term2Data['ClubTitleB5'].'<br/>'.$Term2Data['ClubTitleEN'];
		    	    } else {
		    	    	$ECA2Display = $this->EmptySymbol;
		    	    }
		    	    $Performance2Display = $Term2Data['Performance'] ? $Term2Data['Performance'] : $this->EmptySymbol;
	    	    } else {
	    	    	$ECA2Display = $this->EmptySymbol;
	    	    	$Performance2Display = $this->EmptySymbol;
	    	    }
    	    }
    	    /*
    	    $Term1Data = $enrolDataAry[0]['ECA'];
    	    $Term2Data = $enrolDataAry[1]['ECA'];
     	    //if(!empty($Term1Data))
			if($ReportType=='T')
    	    {
    	        $Term1Data = $Term1Data[0];
    	        $ECADisplay = $Term1Data['ClubTitleB5'].'<br/>'.$Term1Data['ClubTitleEN'];
    	        $PerformanceDisplay = $Term1Data['Performance'];
    	    }
     	    //else if($isPrimarySchool && empty($Term1Data) && !empty($Term2Data))
			else if($ReportType=='W')
    	    {
    	        $Term2Data = $Term2Data[0];
    	        $ECADisplay = $Term2Data['ClubTitleB5'].'<br/>'.$Term2Data['ClubTitleEN'];
    	        $PerformanceDisplay = $Term2Data['Performance'];
    	    }
    	    */

    	    # School Team / Club
    	    $SchoolTeamClubDisplay = $StudentID? '&nbsp;' : $defaultVal;
    	    if($ReportType == 'W' && !empty($enrolDataAry['SchoolTeamClub'])) {
    	        $SchoolTeamClubDisplay = implode('<br/>', (array)$enrolDataAry['SchoolTeamClub']);
    	    } else {
    	    	$SchoolTeamClubDisplay = $this->EmptySymbol;
    	    }
    	    
    	    $MiscTable .= "<tr>";
        	    $MiscTable .= "<td rowspan='2' class='border_top border_right' height='65px'>".$eReportCard['Template']['ExtraCurricularActivitiesCh']."<br/>".$eReportCard['Template']['ExtraCurricularActivitiesEn']."</td>";
        	    # Hide Term Display
        	    /*
        	    $this->Get_Term_Name_Display($eReportCard['Template']['Term'.$i.'En']);
        	    if($isPrimarySchool) {
        	        $MiscTable .= "<td align='left' class='border_top' style='padding-left: 8px;'>".$eReportCard['Template']['Term1n2Ch']."<br/>".$this->Get_Term_Name_Display($eReportCard['Template']['Term1n2En'])."</td>";
        	    }
        	    else {
        	        $MiscTable .= "<td align='left' class='border_top' style='padding-left: 8px;'>".$eReportCard['Template']['Term1Ch']."<br/>".$this->Get_Term_Name_Display($eReportCard['Template']['Term1En'])."</td>";
        	    }*/
        	    $MiscTable .= "<td class='border_top' align='left' style='padding-left: 8px;'>".$eReportCard['Template']['Term1Ch']."<br/>".$eReportCard['Template']['Term1En']."</td>";
        	    $MiscTable .= "<td colspan='2'  class='border_top' align='left' style='padding-left: 8px;'>".$ECADisplay."</td>";
        	    $MiscTable .= "<td class='border_top border_right'>".$PerformanceDisplay."</td>";
        	    
        	    # Removed rowspan='2'
        	    $MiscTable .= "<td rowspan='2' class='border_top border_right'>".$eReportCard['Template']['SchoolTeamClubCh']."<br/>".$eReportCard['Template']['SchoolTeamClubEn']."</td>";
        	    $MiscTable .= "<td rowspan='2' align='left' class='border_top' style='padding-left: 8px;'>".$SchoolTeamClubDisplay."</td>";
    	    $MiscTable .= "</tr>";
    	    $MiscTable .= "<tr>";
    	    	$MiscTable .= "<td class='' align='left' style='padding-left: 8px;'>".$eReportCard['Template']['Term2Ch']."<br/>".$eReportCard['Template']['Term2En']."</td>";
    	    	$MiscTable .= "<td colspan='2' class='' align='left' style='padding-left: 8px;'>".$ECA2Display."</td>";
    	    	$MiscTable .= "<td class='border_right'>".$Performance2Display."</td>";
    	    $MiscTable .= "</tr>";
    	    
    	    # Extra-Curricular Activities
    	    /*$ECADisplay = $StudentID? '&nbsp;' : $defaultVal;
    	    $PerformanceDisplay = $StudentID? '&nbsp;' : 'X';
    	    
    	    if($SchoolType == 'P')
    	    {
    	        $Term3Data = $enrolDataAry[2]['ECA'];
    	        $Term4Data = $enrolDataAry[3]['ECA'];
    	        if(!empty($Term3Data))
    	        {
    	            $Term3Data = $Term3Data[0];
    	            $ECADisplay = $Term3Data['ClubTitleB5'].'<br/>'.$Term3Data['ClubTitleEN'];
    	            $PerformanceDisplay = $Term3Data['Performance'];
    	        }
    	        else if(empty($Term3Data) && !empty($Term4Data))
    	        {
    	            $Term4Data = $Term4Data[0];
    	            $ECADisplay = $Term4Data['ClubTitleB5'].'<br/>'.$Term4Data['ClubTitleEN'];
    	            $PerformanceDisplay = $Term4Data['Performance'];
    	        }
    	    }
    	    else
    	    {
    	        $Term2Data = $enrolDataAry[1]['ECA'];
    	        if(!empty($Term2Data)) {
    	            $Term2Data = $Term2Data[0];
    	            $ECADisplay = $Term2Data['ClubTitleB5'].'<br/>'.$Term2Data['ClubTitleEN'];
    	            $PerformanceDisplay = $Term2Data['Performance'];
    	        }
    	    }
    	    
    	    $MiscTable .= "<tr>";
        	    if($SchoolType == 'P') {
        	        $MiscTable .= "<td align='left' style='padding-left: 8px;'>".$eReportCard['Template']['Term3n4Ch']."<br/>".$this->Get_Term_Name_Display($eReportCard['Template']['Term3n4En'])."</td>";
        	    }
        	    else {
        	        $MiscTable .= "<td align='left' style='padding-left: 8px;'>".$eReportCard['Template']['Term2Ch']."<br/>".$this->Get_Term_Name_Display($eReportCard['Template']['Term2En'])."</td>";
        	    }
        	    $MiscTable .= "<td>：</td>";
        	    
        	    $MiscTable .= "<td align='left'>".$ECADisplay."</td>";
        	    $MiscTable .= "<td class='border_right'>".$PerformanceDisplay."</td>";
    	    $MiscTable .= "</tr>"; */
    	    
    	    # Awards
            $displayAwardArr = array();
            $displayAwardArr[0] = $StudentID ? '&nbsp;' : $defaultVal;
            $displayAwardArr[1] = $StudentID ? '&nbsp;' : $defaultVal;
    	    if($StudentID)
            {
                $awardIndex = 0;
                if($ReportType == 'W')
                {
                    $studentMeritAwards = $this->loadingMeritFromDiscipline($ReportID, $StudentID, false, true);
                    if(!empty($studentMeritAwards))
                    {
                        if(isset($studentMeritAwards['AcademicRewards']) && $studentMeritAwards['AcademicRewards'] > 0) {
                            $displayAwardArr[$awardIndex] = '';
                            $displayAwardArr[$awardIndex] .= str_replace('<!--award_count-->', $studentMeritAwards['AcademicRewards'], $eReportCard['Template']['AcademicAwardNumDisplay']);
                            $displayAwardArr[$awardIndex] .= '<br/>';
                            $displayAwardArr[$awardIndex] .= str_replace('<!--award_count-->', $studentMeritAwards['AcademicRewards'], $eReportCard['Template']['AcademicAwardNumENDisplay']);
                            $awardIndex++;
                        }
                        if(isset($studentMeritAwards['ServiceRewards']) && $studentMeritAwards['ServiceRewards'] > 0) {
                            $displayAwardArr[$awardIndex] = '';
                            $displayAwardArr[$awardIndex] .= str_replace('<!--award_count-->', $studentMeritAwards['ServiceRewards'], $eReportCard['Template']['ServiceAwardNumDisplay']);
                            $displayAwardArr[$awardIndex] .= '<br/>';
                            $displayAwardArr[$awardIndex] .= str_replace('<!--award_count-->', $studentMeritAwards['ServiceRewards'], $eReportCard['Template']['ServiceAwardNumENDisplay']);
                            $awardIndex++;
                        }
                    }
                }

                if(!empty($ary['Award']))
                {
                    /*
                    foreach((array)$ary['Award'] as $thisAward) {
                        if(trim($thisAward) != '') {
                            if($displayAwards != '&nbsp;') {
                                $displayAwards .= '<br/>&nbsp;';
                            } else {
                                $displayAwards = '';
                            }

                            $displayAwards .= trim($thisAward);
                        }
                    }
                    */

                    $displayAwardArr[0] = '&nbsp;';
                    $displayAwardArr[1] = '&nbsp;';
                    $awardIndex = 0;

                    $otherInfoAwards = $ary['Award'];
                    $otherInfoAwardCount = count($otherInfoAwards);
                    for($i=0; $i<$otherInfoAwardCount; $i++) {
                        $displayAwardArr[$awardIndex] = '';
                        $displayAwardArr[$awardIndex] .= $otherInfoAwards[$i];
                        $displayAwardArr[$awardIndex] .= '<br/>';
                        $displayAwardArr[$awardIndex] .= $otherInfoAwards[($i+1)];
                        $awardIndex++;
                        $i++;
                    }
                }
            }
    	    
    	    $MiscTable .= "<tr>";
        	    $MiscTable .= "<td class='border_top border_right' height='50px'>".$eReportCard['Template']['AwardsCh']."<br/>".$eReportCard['Template']['AwardsEn']."</td>";
                //$MiscTable .= "<td align='left' colspan='6' class='border_top' style='padding-left: 8px;'>".$displayAwards."</td>";
                //$MiscTable .= "<td align='left' colspan='6' class='border_top' style='padding-left: 8px;'>".$displayAwards."<br/>".$displayENAwards."</td>";
                //$MiscTable .= "<td align='left' colspan='4' class='border_top border_right' style='padding-left: 8px;'>".$displayAwardArr[0]."</td>";
                $MiscTable .= "<td align='left' colspan='4' class='border_top' style='padding-left: 8px;'>".$displayAwardArr[0]."</td>";
                $MiscTable .= "<td align='left' colspan='2' class='border_top' style='padding-left: 8px;'>".$displayAwardArr[1]."</td>";
    	    $MiscTable .= "</tr>";
	    $MiscTable .= "</table>";
	    
	    $MiscTable .= "<table width='100%' border='0' cellspacing='0' cellpadding='2' class='merit_attend_content'>";
	    if($SchoolType == 'P')
	    {
	        /*$MiscTable .= '<colgroup>';
    	        $MiscTable .= '<col style="width: 26%">';
    	        $MiscTable .= '<col style="width: 5.5%">';
    	        $MiscTable .= '<col style="width: 6.5%">';
    	        $MiscTable .= '<col style="width: 6.5%">';
    	        $MiscTable .= '<col style="width: 5.5%">';
    	        $MiscTable .= '<col style="width: 6.5%">';
    	        $MiscTable .= '<col style="width: 6.5%">';
    	        $MiscTable .= '<col style="width: 5.5%">';
    	        $MiscTable .= '<col style="width: 6.5%">';
    	        $MiscTable .= '<col style="width: 6.5%">';
    	        $MiscTable .= '<col style="width: 5.5%">';
    	        $MiscTable .= '<col style="width: 6.5%">';
    	        $MiscTable .= '<col style="width: 6.5%">';
	        $MiscTable .= '</colgroup>';*/
	        $MiscTable .= '<colgroup>';
		        $MiscTable .= '<col style="width: 25%">';
		        $MiscTable .= '<col style="width: 12.5%">';
		        $MiscTable .= '<col style="width: 12.5%">';
		        $MiscTable .= '<col style="width: 12.5%">';
		        $MiscTable .= '<col style="width: 12.5%">';
		        $MiscTable .= '<col style="width: 12.5%">';
		        $MiscTable .= '<col style="width: 12.5%">';
	        $MiscTable .= '</colgroup>';
	    }
	    else
	    {
	        $MiscTable .= '<colgroup>';
    	        $MiscTable .= '<col style="width: 25%">';
    	        $MiscTable .= '<col style="width: 12.5%">';
    	        $MiscTable .= '<col style="width: 12.5%">';
    	        $MiscTable .= '<col style="width: 12.5%">';
    	        $MiscTable .= '<col style="width: 12.5%">';
    	        $MiscTable .= '<col style="width: 12.5%">';
    	        $MiscTable .= '<col style="width: 12.5%">';
	        $MiscTable .= '</colgroup>';
	    }
	    
	    # Attendance
	    $MiscTable .= "<tr>";
    	    $MiscTable .= "<td class='border_top border_right'>".$eReportCard['Template']['AttendanceCh']." ".$eReportCard['Template']['AttendanceEn']."</td>";
	        /*if($SchoolType == 'P')
	        {
	            $MiscTable .= "<td colspan='3' class='border_top border_right'>".$eReportCard['Template']['Term1Ch']." ".$this->Get_Term_Name_Display($eReportCard['Template']['Term1En'])."</td>";
	            $MiscTable .= "<td colspan='3' class='border_top border_right'>".$eReportCard['Template']['Term2Ch']." ".$this->Get_Term_Name_Display($eReportCard['Template']['Term2En'])."</td>";
	            $MiscTable .= "<td colspan='3' class='border_top border_right'>".$eReportCard['Template']['Term3Ch']." ".$this->Get_Term_Name_Display($eReportCard['Template']['Term3En'])."</td>";
	            $MiscTable .= "<td colspan='3' class='border_top'>".$eReportCard['Template']['Term4Ch']." ".$this->Get_Term_Name_Display($eReportCard['Template']['Term4En'])."</td>";
	        }
	        else
	        {*/
	            $MiscTable .= "<td colspan='3' class='border_top border_right'>".$eReportCard['Template']['Term1Ch']." ".$this->Get_Term_Name_Display($eReportCard['Template']['Term1En'])."</td>";
	            $MiscTable .= "<td colspan='3' class='border_top'>".$eReportCard['Template']['Term2Ch']." ".$this->Get_Term_Name_Display($eReportCard['Template']['Term2En'])."</td>";
	        /*}*/
	    $MiscTable .= "</tr>";
	    
	    # Lateness (Times)
	    $MiscTable .= "<tr>";
    	    $MiscTable .= "<td align='left' class='border_top border_right'>".$eReportCard['Template']['LatenessCh']." ".$eReportCard['Template']['LatenessEn']."</td>";
    	    /*if($SchoolType == 'P')
    	    {
    	        $MiscTable .= "<td colspan='3' class='border_top border_right'>".($attendanceDataAry[0]['Lateness']? $attendanceDataAry[0]['Lateness'] : 0)."</td>";
    	        $MiscTable .= "<td colspan='3' class='border_top border_right'>".($attendanceDataAry[1]['Lateness']? $attendanceDataAry[1]['Lateness'] : 0)."</td>";
    	        $MiscTable .= "<td colspan='3' class='border_top border_right'>".($attendanceDataAry[2]['Lateness']? $attendanceDataAry[2]['Lateness'] : 0)."</td>";
    	        $MiscTable .= "<td colspan='3' class='border_top'>".($attendanceDataAry[3]['Lateness']? $attendanceDataAry[3]['Lateness'] : 0)."</td>";
    	    }
    	    else
    	    {*/
    	        $MiscTable .= "<td colspan='3' class='border_top border_right'>".($attendanceDataAry[0]['Lateness']? $attendanceDataAry[0]['Lateness'] : 0)."</td>";
                if($SemNum == 1) {
                    $MiscTable .= "<td colspan='3' class='border_top'>".$this->EmptySymbol."</td>";
                } else {
                    $MiscTable .= "<td colspan='3' class='border_top'>".($attendanceDataAry[1]['Lateness'] ? $attendanceDataAry[1]['Lateness'] : 0)."</td>";
                }
    	    /*}*/
	    $MiscTable .= "</tr>";
	    
	    # Unexcused Absent (Periods)
	    $MiscTable .= "<tr>";
    	    $MiscTable .= "<td align='left' class='border_right'>".$eReportCard['Template']['UnexcusedAbsentCh']." ".$eReportCard['Template']['UnexcusedAbsentEn']."</td>";
    	   /* if($SchoolType == 'P')
    	    {
    	        $MiscTable .= "<td colspan='3' class='border_right'>".($attendanceDataAry[0]['UnexcusedAbsent']? $attendanceDataAry[0]['UnexcusedAbsent'] : 0)."</td>";
    	        $MiscTable .= "<td colspan='3' class='border_right'>".($attendanceDataAry[1]['UnexcusedAbsent']? $attendanceDataAry[1]['UnexcusedAbsent'] : 0)."</td>";
    	        $MiscTable .= "<td colspan='3' class='border_right'>".($attendanceDataAry[2]['UnexcusedAbsent']? $attendanceDataAry[2]['UnexcusedAbsent'] : 0)."</td>";
    	        $MiscTable .= "<td colspan='3'>".($attendanceDataAry[3]['UnexcusedAbsent']? $attendanceDataAry[3]['UnexcusedAbsent'] : 0)."</td>";
    	    }
    	    else
    	    {*/
    	        $MiscTable .= "<td colspan='3' class='border_right'>".($attendanceDataAry[0]['UnexcusedAbsent']? $attendanceDataAry[0]['UnexcusedAbsent'] : 0)."</td>";
    	        if($SemNum == 1) {
    	            $MiscTable .= "<td colspan='3'>".$this->EmptySymbol."</td>";
                } else {
    	            $MiscTable .= "<td colspan='3'>".($attendanceDataAry[1]['UnexcusedAbsent'] ? $attendanceDataAry[1]['UnexcusedAbsent'] : 0)."</td>";
                }
    	    /*}*/
	    $MiscTable .= "</tr>";
	    
	    # Personal Leave (Periods)
	    $MiscTable .= "<tr>";
    	    $MiscTable .= "<td align='left' class='border_right'>".$eReportCard['Template']['PersonalLeaveCh']." ".$eReportCard['Template']['PersonalLeaveEn']."</td>";
    	    /*if($SchoolType == 'P')
    	    {
    	        $MiscTable .= "<td colspan='3' class='border_right'>".($attendanceDataAry[0]['PersonalLeave']? $attendanceDataAry[0]['PersonalLeave'] : 0)."</td>";
    	        $MiscTable .= "<td colspan='3' class='border_right'>".($attendanceDataAry[1]['PersonalLeave']? $attendanceDataAry[1]['PersonalLeave'] : 0)."</td>";
    	        $MiscTable .= "<td colspan='3' class='border_right'>".($attendanceDataAry[2]['PersonalLeave']? $attendanceDataAry[2]['PersonalLeave'] : 0)."</td>";
    	        $MiscTable .= "<td colspan='3'>".($attendanceDataAry[3]['PersonalLeave']? $attendanceDataAry[3]['PersonalLeave'] : 0)."</td>";
    	    }
    	    else
    	    {*/
    	        $MiscTable .= "<td colspan='3' class='border_right'>".($attendanceDataAry[0]['PersonalLeave']? $attendanceDataAry[0]['PersonalLeave'] : 0)."</td>";
                if($SemNum == 1) {
                    $MiscTable .= "<td colspan='3'>".$this->EmptySymbol."</td>";
                } else {
                    $MiscTable .= "<td colspan='3'>".($attendanceDataAry[1]['PersonalLeave'] ? $attendanceDataAry[1]['PersonalLeave'] : 0)."</td>";
                }
    	    /*}*/
	    $MiscTable .= "</tr>";
	    
	    # Sick Leave (Periods)
	    $MiscTable .= "<tr>";
    	    $MiscTable .= "<td align='left' class='border_right'>".$eReportCard['Template']['SickLeaveCh']." ".$eReportCard['Template']['SickLeaveEn']."</td>";
    	    /*if($SchoolType == 'P')
    	    {
    	        $MiscTable .= "<td colspan='3' class='border_right'>".($attendanceDataAry[0]['SickLeave']? $attendanceDataAry[0]['SickLeave'] : 0)."</td>";
    	        $MiscTable .= "<td colspan='3' class='border_right'>".($attendanceDataAry[1]['SickLeave']? $attendanceDataAry[1]['SickLeave'] : 0)."</td>";
    	        $MiscTable .= "<td colspan='3' class='border_right'>".($attendanceDataAry[2]['SickLeave']? $attendanceDataAry[2]['SickLeave'] : 0)."</td>";
    	        $MiscTable .= "<td colspan='3'>".($attendanceDataAry[3]['SickLeave']? $attendanceDataAry[3]['SickLeave'] : 0)."</td>";
    	    }
    	    else
    	    {*/
    	        $MiscTable .= "<td colspan='3' class='border_right'>".($attendanceDataAry[0]['SickLeave']? $attendanceDataAry[0]['SickLeave'] : 0)."</td>";
                if($SemNum == 1) {
                    $MiscTable .= "<td colspan='3'>".$this->EmptySymbol."</td>";
                } else {
                    $MiscTable .= "<td colspan='3'>".($attendanceDataAry[1]['SickLeave'] ? $attendanceDataAry[1]['SickLeave'] : 0)."</td>";
                }
    	    /*}*/
	    $MiscTable .= "</tr>";
	    
	    # Rewards
	    $MiscTable .= "<tr>";
    	    $MiscTable .= "<td class='border_top border_right'>".$eReportCard['Template']['RewardsCh']." ".$eReportCard['Template']['RewardsEn']."</td>";
    	    /*if($SchoolType == 'P')
    	    {
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MeritCh']."<br/>".$eReportCard['Template']['MeritEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MinAchCh']."<br/>".$eReportCard['Template']['MinAchEn']."</td>";
    	        $MiscTable .= "<td class='border_top border_right'>".$eReportCard['Template']['MajAchCh']."<br/>".$eReportCard['Template']['MajAchEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MeritCh']."<br/>".$eReportCard['Template']['MeritEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MinAchCh']."<br/>".$eReportCard['Template']['MinAchEn']."</td>";
    	        $MiscTable .= "<td class='border_top border_right'>".$eReportCard['Template']['MajAchCh']."<br/>".$eReportCard['Template']['MajAchEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MeritCh']."<br/>".$eReportCard['Template']['MeritEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MinAchCh']."<br/>".$eReportCard['Template']['MinAchEn']."</td>";
    	        $MiscTable .= "<td class='border_top border_right'>".$eReportCard['Template']['MajAchCh']."<br/>".$eReportCard['Template']['MajAchEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MeritCh']."<br/>".$eReportCard['Template']['MeritEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MinAchCh']."<br/>".$eReportCard['Template']['MinAchEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MajAchCh']."<br/>".$eReportCard['Template']['MajAchEn']."</td>";
    	    }
    	    else
    	    {*/
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MeritCh']."<br/>".$eReportCard['Template']['MeritEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MinAchCh']."<br/>".$eReportCard['Template']['MinAchEn']."</td>";
    	        $MiscTable .= "<td class='border_top border_right'>".$eReportCard['Template']['MajAchCh']."<br/>".$eReportCard['Template']['MajAchEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MeritCh']."<br/>".$eReportCard['Template']['MeritEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MinAchCh']."<br/>".$eReportCard['Template']['MinAchEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MajAchCh']."<br/>".$eReportCard['Template']['MajAchEn']."</td>";
    	    /*}*/
	    $MiscTable .= "</tr>";
	    
	    # Academic Rewards
	    $MiscTable .= "<tr>";
    	    $MiscTable .= "<td align='left' class='border_top border_right'>".$eReportCard['Template']['AcademicRewardsCh']." ".$eReportCard['Template']['AcademicRewardsEn']."</td>";
    	    /*if($SchoolType == 'P')
    	    {
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[0]['AcademicRewards'][1]? $meritDataAry[0]['AcademicRewards'][1] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[0]['AcademicRewards'][2]? $meritDataAry[0]['AcademicRewards'][2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top border_right'>". ($meritDataAry[0]['AcademicRewards'][3]? $meritDataAry[0]['AcademicRewards'][3] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[1]['AcademicRewards'][1]? $meritDataAry[1]['AcademicRewards'][1] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[1]['AcademicRewards'][2]? $meritDataAry[1]['AcademicRewards'][2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top border_right'>". ($meritDataAry[1]['AcademicRewards'][3]? $meritDataAry[1]['AcademicRewards'][3] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[2]['AcademicRewards'][1]? $meritDataAry[2]['AcademicRewards'][1] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[2]['AcademicRewards'][2]? $meritDataAry[2]['AcademicRewards'][2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top border_right'>". ($meritDataAry[2]['AcademicRewards'][3]? $meritDataAry[2]['AcademicRewards'][3] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[3]['AcademicRewards'][1]? $meritDataAry[3]['AcademicRewards'][1] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[3]['AcademicRewards'][2]? $meritDataAry[3]['AcademicRewards'][2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[3]['AcademicRewards'][3]? $meritDataAry[3]['AcademicRewards'][3] : 0) ."</td>";
    	    }
    	    else
    	    {*/
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[0]['AcademicRewards'][1]? $meritDataAry[0]['AcademicRewards'][1] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[0]['AcademicRewards'][2]? $meritDataAry[0]['AcademicRewards'][2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top border_right'>". ($meritDataAry[0]['AcademicRewards'][3]? $meritDataAry[0]['AcademicRewards'][3] : 0) ."</td>";
                if($SemNum == 1) {
                    $MiscTable .= "<td class='border_top'>".$this->EmptySymbol."</td>";
                    $MiscTable .= "<td class='border_top'>".$this->EmptySymbol."</td>";
                    $MiscTable .= "<td class='border_top'>".$this->EmptySymbol."</td>";
                } else {
                    $MiscTable .= "<td class='border_top'>". ($meritDataAry[1]['AcademicRewards'][1] ? $meritDataAry[1]['AcademicRewards'][1] : 0) ."</td>";
                    $MiscTable .= "<td class='border_top'>". ($meritDataAry[1]['AcademicRewards'][2] ? $meritDataAry[1]['AcademicRewards'][2] : 0) ."</td>";
                    $MiscTable .= "<td class='border_top'>". ($meritDataAry[1]['AcademicRewards'][3] ? $meritDataAry[1]['AcademicRewards'][3] : 0) ."</td>";
                }
    	    /*}*/
	    $MiscTable .= "</tr>";
	    
	    # Service and Non-Academic Rewards
	    $MiscTable .= "<tr>";
    	    $MiscTable .= "<td align='left' class='border_right' style='font-size: 10px;'>".$eReportCard['Template']['ServiceRewardsCh']." ".$eReportCard['Template']['ServiceRewardsEn']."</td>";
    	    /*if($SchoolType == 'P')
    	    {
    	        $MiscTable .= "<td>". ($meritDataAry[0]['ServiceRewards'][1]? $meritDataAry[0]['ServiceRewards'][1] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[0]['ServiceRewards'][2]? $meritDataAry[0]['ServiceRewards'][2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_right'>". ($meritDataAry[0]['ServiceRewards'][3]? $meritDataAry[0]['ServiceRewards'][3] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[1]['ServiceRewards'][1]? $meritDataAry[1]['ServiceRewards'][1] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[1]['ServiceRewards'][2]? $meritDataAry[1]['ServiceRewards'][2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_right'>". ($meritDataAry[1]['ServiceRewards'][3]? $meritDataAry[1]['ServiceRewards'][3] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[2]['ServiceRewards'][1]? $meritDataAry[2]['ServiceRewards'][1] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[2]['ServiceRewards'][2]? $meritDataAry[2]['ServiceRewards'][2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_right'>". ($meritDataAry[2]['ServiceRewards'][3]? $meritDataAry[2]['ServiceRewards'][3] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[3]['ServiceRewards'][1]? $meritDataAry[3]['ServiceRewards'][1] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[3]['ServiceRewards'][2]? $meritDataAry[3]['ServiceRewards'][2] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[3]['ServiceRewards'][3]? $meritDataAry[3]['ServiceRewards'][3] : 0) ."</td>";
    	    }
    	    else
    	    {*/
    	        $MiscTable .= "<td>". ($meritDataAry[0]['ServiceRewards'][1]? $meritDataAry[0]['ServiceRewards'][1] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[0]['ServiceRewards'][2]? $meritDataAry[0]['ServiceRewards'][2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_right'>". ($meritDataAry[0]['ServiceRewards'][3]? $meritDataAry[0]['ServiceRewards'][3] : 0) ."</td>";
                if($SemNum == 1) {
                    $MiscTable .= "<td>".$this->EmptySymbol."</td>";
                    $MiscTable .= "<td>".$this->EmptySymbol."</td>";
                    $MiscTable .= "<td>".$this->EmptySymbol."</td>";
                } else {
                    $MiscTable .= "<td>". ($meritDataAry[1]['ServiceRewards'][1] ? $meritDataAry[1]['ServiceRewards'][1] : 0) ."</td>";
                    $MiscTable .= "<td>". ($meritDataAry[1]['ServiceRewards'][2] ? $meritDataAry[1]['ServiceRewards'][2] : 0) ."</td>";
                    $MiscTable .= "<td>". ($meritDataAry[1]['ServiceRewards'][3] ? $meritDataAry[1]['ServiceRewards'][3] : 0) ."</td>";
                }
    	    /*}*/
	    $MiscTable .= "</tr>";
	    
	    # Conduct Rewards
	    $MiscTable .= "<tr>";
    	    $MiscTable .= "<td align='left' class='border_right'>".$eReportCard['Template']['ConductRewardsCh']." ".$eReportCard['Template']['ConductRewardsEn']."</td>";
    	    /*if($SchoolType == 'P')
    	    {
    	        $MiscTable .= "<td>". ($meritDataAry[0]['ConductRewards'][1]? $meritDataAry[0]['ConductRewards'][1] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[0]['ConductRewards'][2]? $meritDataAry[0]['ConductRewards'][2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_right'>". ($meritDataAry[0]['ConductRewards'][3]? $meritDataAry[0]['ConductRewards'][3] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[1]['ConductRewards'][1]? $meritDataAry[1]['ConductRewards'][1] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[1]['ConductRewards'][2]? $meritDataAry[1]['ConductRewards'][2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_right'>". ($meritDataAry[1]['ConductRewards'][3]? $meritDataAry[1]['ConductRewards'][3] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[2]['ConductRewards'][1]? $meritDataAry[2]['ConductRewards'][1] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[2]['ConductRewards'][2]? $meritDataAry[2]['ConductRewards'][2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_right'>". ($meritDataAry[2]['ConductRewards'][3]? $meritDataAry[2]['ConductRewards'][3] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[3]['ConductRewards'][1]? $meritDataAry[3]['ConductRewards'][1] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[3]['ConductRewards'][2]? $meritDataAry[3]['ConductRewards'][2] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[3]['ConductRewards'][3]? $meritDataAry[3]['ConductRewards'][3] : 0) ."</td>";
    	    }
    	    else
    	    {*/
    	        $MiscTable .= "<td>". ($meritDataAry[0]['ConductRewards'][1]? $meritDataAry[0]['ConductRewards'][1] : 0) ."</td>";
    	        $MiscTable .= "<td>". ($meritDataAry[0]['ConductRewards'][2]? $meritDataAry[0]['ConductRewards'][2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_right'>". ($meritDataAry[0]['ConductRewards'][3]? $meritDataAry[0]['ConductRewards'][3] : 0) ."</td>";
                if($SemNum == 1) {
                    $MiscTable .= "<td>".$this->EmptySymbol."</td>";
                    $MiscTable .= "<td>".$this->EmptySymbol."</td>";
                    $MiscTable .= "<td>".$this->EmptySymbol."</td>";
                } else {
                    $MiscTable .= "<td>". ($meritDataAry[1]['ConductRewards'][1] ? $meritDataAry[1]['ConductRewards'][1] : 0) ."</td>";
                    $MiscTable .= "<td>". ($meritDataAry[1]['ConductRewards'][2] ? $meritDataAry[1]['ConductRewards'][2] : 0) ."</td>";
                    $MiscTable .= "<td>". ($meritDataAry[1]['ConductRewards'][3] ? $meritDataAry[1]['ConductRewards'][3] : 0) ."</td>";
                }
    	    /*}*/
	    $MiscTable .= "</tr>";
	    
	    # Penalties
	    $MiscTable .= "<tr>";
    	    $MiscTable .= "<td class='border_top border_right'>".$eReportCard['Template']['PenaltiesCh']." ".$eReportCard['Template']['PenaltiesEn']."</td>";
    	    /*if($SchoolType == 'P')
    	    {
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['DemeritCh']."<br/>".$eReportCard['Template']['DemeritEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MinFitCh']."<br/>".$eReportCard['Template']['MinFitEn']."</td>";
    	        $MiscTable .= "<td class='border_top border_right'>".$eReportCard['Template']['MajFitCh']."<br/>".$eReportCard['Template']['MajFitEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['DemeritCh']."<br/>".$eReportCard['Template']['DemeritEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MinFitCh']."<br/>".$eReportCard['Template']['MinFitEn']."</td>";
    	        $MiscTable .= "<td class='border_top border_right'>".$eReportCard['Template']['MajFitCh']."<br/>".$eReportCard['Template']['MajFitEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['DemeritCh']."<br/>".$eReportCard['Template']['DemeritEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MinFitCh']."<br/>".$eReportCard['Template']['MinFitEn']."</td>";
    	        $MiscTable .= "<td class='border_top border_right'>".$eReportCard['Template']['MajFitCh']."<br/>".$eReportCard['Template']['MajFitEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['DemeritCh']."<br/>".$eReportCard['Template']['DemeritEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MinFitCh']."<br/>".$eReportCard['Template']['MinFitEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MajFitCh']."<br/>".$eReportCard['Template']['MajFitEn']."</td>";
    	    }
    	    else
    	    {*/
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['DemeritCh']."<br/>".$eReportCard['Template']['DemeritEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MinFitCh']."<br/>".$eReportCard['Template']['MinFitEn']."</td>";
    	        $MiscTable .= "<td class='border_top border_right'>".$eReportCard['Template']['MajFitCh']."<br/>".$eReportCard['Template']['MajFitEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['DemeritCh']."<br/>".$eReportCard['Template']['DemeritEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MinFitCh']."<br/>".$eReportCard['Template']['MinFitEn']."</td>";
    	        $MiscTable .= "<td class='border_top'>".$eReportCard['Template']['MajFitCh']."<br/>".$eReportCard['Template']['MajFitEn']."</td>";
    	    /*}*/
	    $MiscTable .= "</tr>";
	    
	    # Conduct Penalties	
	    $MiscTable .= "<tr>";
    	    $MiscTable .= "<td align='left' class='border_top border_right'>".$eReportCard['Template']['ConductPenaltiesCh']." ".$eReportCard['Template']['ConductPenaltiesEn']."</td>";
    	    /*if($SchoolType == 'P')
    	    {
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[0]['ConductPenalties'][-1]? $meritDataAry[0]['ConductPenalties'][-1] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[0]['ConductPenalties'][-2]? $meritDataAry[0]['ConductPenalties'][-2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top border_right'>". ($meritDataAry[0]['ConductPenalties'][-3]? $meritDataAry[0]['ConductPenalties'][-3] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[1]['ConductPenalties'][-1]? $meritDataAry[1]['ConductPenalties'][-1] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[1]['ConductPenalties'][-2]? $meritDataAry[1]['ConductPenalties'][-2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top border_right'>". ($meritDataAry[1]['ConductPenalties'][-3]? $meritDataAry[1]['ConductPenalties'][-3] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[2]['ConductPenalties'][-1]? $meritDataAry[2]['ConductPenalties'][-1] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[2]['ConductPenalties'][-2]? $meritDataAry[2]['ConductPenalties'][-2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top border_right'>". ($meritDataAry[2]['ConductPenalties'][-3]? $meritDataAry[2]['ConductPenalties'][-3] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[3]['ConductPenalties'][-1]? $meritDataAry[3]['ConductPenalties'][-1] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[3]['ConductPenalties'][-2]? $meritDataAry[3]['ConductPenalties'][-2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[3]['ConductPenalties'][-3]? $meritDataAry[3]['ConductPenalties'][-3] : 0) ."</td>";
    	    }
    	    else
    	    {*/
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[0]['ConductPenalties'][-1]? $meritDataAry[0]['ConductPenalties'][-1] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top'>". ($meritDataAry[0]['ConductPenalties'][-2]? $meritDataAry[0]['ConductPenalties'][-2] : 0) ."</td>";
    	        $MiscTable .= "<td class='border_top border_right'>". ($meritDataAry[0]['ConductPenalties'][-3]? $meritDataAry[0]['ConductPenalties'][-3] : 0) ."</td>";
                if($SemNum == 1) {
                    $MiscTable .= "<td class='border_top'>".$this->EmptySymbol."</td>";
                    $MiscTable .= "<td class='border_top'>".$this->EmptySymbol."</td>";
                    $MiscTable .= "<td class='border_top'>".$this->EmptySymbol."</td>";
                } else {
                    $MiscTable .= "<td class='border_top'>". ($meritDataAry[1]['ConductPenalties'][-1] ? $meritDataAry[1]['ConductPenalties'][-1] : 0) ."</td>";
                    $MiscTable .= "<td class='border_top'>". ($meritDataAry[1]['ConductPenalties'][-2] ? $meritDataAry[1]['ConductPenalties'][-2] : 0) ."</td>";
                    $MiscTable .= "<td class='border_top'>". ($meritDataAry[1]['ConductPenalties'][-3] ? $meritDataAry[1]['ConductPenalties'][-3] : 0) ."</td>";
                }
    	    /*}*/
	    $MiscTable .= "</tr>";
	    $MiscTable .= "</table>";
	    
	    # Failed Subject Display (in Remarks)
	    $SupplementaryRemarks = '';
	    if($ReportType == 'W') {
	        $StudentFailedSubjectArr = $this->Get_Student_Failed_Subject($ReportID, array($StudentID));
	        if(!empty($StudentFailedSubjectArr)) {
	            $SupplementaryRemarks .= $eReportCard['Template']['SupplementarySubjectsCh'].' '.$eReportCard['Template']['SupplementarySubjectsEn'].'：';
	            $SupplementaryRemarks .= implode('，', (array)$StudentFailedSubjectArr);
	        }
	    }

	    # Promotion Status
        $PromotionRemarks = '';
        if($ReportType == 'W')
        {
            $PromotionStatusArr = $this->GetPromotionStatusList('', '', $StudentID, $ReportID);
            $PromotionStatusArr = $PromotionStatusArr[$StudentID];

            if(!empty($PromotionStatusArr)) {
                $PromotionType = $promotionTypeArr[$PromotionStatusArr['Promotion']];
                $PromotionRemarks = $promotionRemarksArr[$ClassLevelID][$PromotionType];
            }
        }
	    
	    # Remarks
	    $remarkDisplay = $StudentID ? '' : $defaultVal;
	    $lineIndex = 1;
	    if($SupplementaryRemarks != '')
	    {
	        //$remarkDisplay .= $lineIndex.'.&nbsp;&nbsp;&nbsp;&nbsp;'.trim($SupplementaryRemarks);
            $remarkDisplay .= $lineIndex.'.&nbsp;'.trim($SupplementaryRemarks);
	        $lineIndex++;
	    }
	    if($PromotionRemarks != '')
        {
            if($SupplementaryRemarks != '') {
                if($this->schoolYear == '2019') {
                    $remarkDisplay = '';
                } else {
                    $remarkDisplay .= '<br/>';
                }
            }

            //$remarkDisplay .= $lineIndex.'.&nbsp;&nbsp;&nbsp;&nbsp;'.nl2br(trim($PromotionRemarks));
            //$lineIndex++;

            $PromotionRemarks = str_replace(' ', '&nbsp;', $PromotionRemarks);
            $remarkDisplay .= nl2br($PromotionRemarks);
        }

        /*
	    if(!empty($ary['Remark']))
	    {
	        foreach((array)$ary['Remark'] as $thisRemark) {
	            if(trim($thisRemark) != '') {
	                if($lineIndex > 1) {
	                    $remarkDisplay .= '<br/>&nbsp;';
	                }
	                
	                $remarkDisplay .= $lineIndex.'.&nbsp;&nbsp;&nbsp;&nbsp;'.trim($thisRemark);
	                $lineIndex++;
	            }
	        }
	    }
	    */

        if(!empty($ary['Remark']) && $StudentID)
        {
            /*
            $remarkDisplay = '';
            foreach((array)$ary['Remark'] as $thisRemark)
            {
                if($thisRemark != '')
                {
                    if($remarkDisplay != '') {
                        //$remarkDisplay .= '<br/>&nbsp;';
                        $remarkDisplay .= '<br/>';
                    }
                    //$remarkDisplay .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$thisRemark;

                    $thisRemark = str_replace(' ', '&nbsp;', $thisRemark);
                    $remarkDisplay .= $thisRemark;
                }
            }
            */

            $targetSemID = $SemID;
            if($ReportType == 'W') {
                $targetSemID = 0;
            }

            $studentRemarks = $this->Get_Student_OtherInfo_Data('remark', $targetSemID, '', $StudentID, null, $ClassLevelID);
            $studentRemarks = $studentRemarks[0]['Information'];
            if($studentRemarks != '') {
                $studentRemarks = str_replace(' ', '&nbsp;', $studentRemarks);
                $remarkDisplay = nl2br($studentRemarks);
            }
        }
	    
	    $MiscTable .= "<table width='100%' border='0' cellspacing='0' cellpadding='2' class='misc_content'>";
	    $MiscTable .= "<tr>";
	       $MiscTable .= "<td class='border_top border_bottom border_right' width='16.5%' height='42px'>".$eReportCard['Template']['RemarksCh']."<br/>".$eReportCard['Template']['RemarksEn']."</td>";
	       $MiscTable .= "<td class='border_top border_bottom' align='left' valign='top' style='padding-left: 8px;'>".$remarkDisplay."</td>";
	    $MiscTable .= "</tr>";
	    $MiscTable .= "</table>";
	    
	    return $MiscTable;
	}
	
	function getReportRemarksPage($ReportID)
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$SchoolType = $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		//$rankEn = ($SchoolType == 'P') ? '10' : '3';
		//$rankCh = ($SchoolType == 'P') ? '十' : '三';
        $rankEn = '10';
        $rankCh = '十';
		
	    $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='2' class='report_remarks'>";
	    
	    $x .= "<colgroup>";
    	    $x .= "<col width='6%'>";
    	    $x .= "<col width='30%'>";
    	    $x .= "<col width='14%'>";
    	    $x .= "<col width='50%'>";
	    $x .= "</colgroup>";
	    
	    $x .= "<tr>";
    	    $x .= "<td colspan='4' style='padding: 5px 0px;'><b>Specification</b></td>";
	    $x .= "</tr>";
	    $x .= "<tr>";
    	    $x .= "<td colspan='4' style='padding: 5px 0px 0px 0px;'><b><i>(1)&nbsp;&nbsp;<u>Academic results</u></i></b></td>";
	    $x .= "</tr>";
	    $x .= "<tr>";
    	    $x .= "<td colspan='4'>";
    	        $x .= "<ul class='list_view'>";
            	    $x .= "<li>Academic results are based on 100 as full marks and 60 as minimum passing marks.</li>";
             	    //$x .= "<li>The Secondary academic year is divided into two terms. Each term carries 50% weight.</li>";
             	    //$x .= "<li>The Primary academic year is divided into two terms. Each term carries 50% weight.</li>";
            	    $x .= "<li>The academic year is divided into two terms. Each term carries 50% weight.</li>";
            	    $x .= "<li>Class rank will only be displayed for the students within the top {$rankEn} in academic performance.</li>";
            	    $x .= "<li>‘Reading’ is counted as zero units. (<i>Applicable for Secondary section only</i>)</li>";
            	    $x .= "<li>Electives are denoted by ‘*’.</li>";
            	    //$x .= "<li>Chinese medium subjects are denoted by ‘(c)’ (<i>Applicable for the Primary English Section only</i>)</li>";
            	    //$x .= "<li class='math_equation'>Average of the total average = Total（Average of the subject x（";
                    $x .= "<li class='math_equation'>Total average of average = Total（Average of the subject x（";
            	       $x .= "<div class='equation'>
                                    <span class='numerator'>(Weight unit)</span>
                                    <span class='bar'>/</span>
                                    <span class='denominator'>(Sum of all units)</span>
                                </div>";
                    $x .= "）</li>";
                $x .= "</ul>";
            $x .= "</td>";
	    $x .= "</tr>";
	    $x .= "<tr>";
	       $x .= "<td colspan='4'>&nbsp;</td>";
        $x .= "</tr>";
        
        $x .= "<tr>";
            $x .= "<td colspan='2'><b><i>(2)&nbsp;&nbsp;<u>Grade</u></i><b></td>";
            $x .= "<td colspan='2'><b><i>(3)&nbsp;&nbsp;<u>Abbreviation</u></i><b></td>";
        $x .= "</tr>";
	    $x .= "<tr>";
    	    $x .= "<td valign='top'>";
        	    $x .= "<ul class='list_view'>";
            	    $x .= "<li>A</li>";
            	    $x .= "<li>B</li>";
            	    $x .= "<li>C</li>";
            	    $x .= "<li>D</li>";
        	    $x .= "</ul>";
    	    $x .= "</td>";
    	    $x .= "<td valign='top'>";
            	    $x .= "<ul>";
            	    $x .= "<li>- Excellent</li>";
            	    $x .= "<li>- Good</li>";
            	    $x .= "<li>- Fair</li>";
            	    $x .= "<li>- Poor</li>";
            	    $x .= "</ul>";
        	    $x .= "</td>";
    	    $x .= "<td valign='top'>";
        	    $x .= "<ul class='list_view'>";
            	    $x .= "<li>C.A.</li>";
            	    $x .= "<li>Min. Ach.</li>";
            	    $x .= "<li>Maj. Ach.</li>";
            	    $x .= "<li>Min. Flt.</li>";
            	    $x .= "<li>Maj. Flt.</li>";
        	    $x .= "</ul>";
    	    $x .= "</td>";
    	    $x .= "<td valign='top'>";
        	    $x .= "<ul>";
            	    $x .= "<li>- Continuous Assessment</li>";
            	    $x .= "<li>- Minor Achievement</li>";
            	    $x .= "<li>- Major Achievement</li>";
            	    $x .= "<li>- Minor Fault</li>";
            	    $x .= "<li>- Major Fault</li>";
        	    $x .= "</ul>";
    	    $x .= "</td>";
	    $x .= "</tr>";
	    $x .= "<tr>";
	       $x .= "<td colspan='4'>&nbsp;</td>";
	       $x .= "</tr>";
       $x .= "<tr>";
	       $x .= "<td colspan='4'>&nbsp;</td>";
       $x .= "</tr>";
	    
	    $x .= "<tr>";
	        $x .= "<td colspan='4' style='padding: 5px 0px;'><b>說明</b></td>";
	    $x .= "</tr>";
	    $x .= "<tr>";
	        $x .= "<td colspan='4' style='padding: 5px 0px 0px 0px;'><b><i>（1）<u>學業成績</u></i></b></td>";
	    $x .= "</tr>";
	    $x .= "<tr>";
    	    $x .= "<td colspan='4'>";
        	    $x .= "<ul class='list_view'>";
            	    $x .= "<li>學業成績以100分為滿分，60分為及格。</li>";
//             	    $x .= "<li>中學每學年分兩個學段，每個學段以50%計算。</li>";
//             	    $x .= "<li>小學每學年分兩個學段，每個學段以50%計算。</li>";
            	    $x .= "<li>每學年分兩個學段，每個學段以50%計算。</li>";
            	    $x .= "<li>考列名次只列全班前{$rankCh}名。</li>";
            	    $x .= "<li>‘閱讀’按零單位計算。( <i>只適用於中學</i> )</li>";
            	    $x .= "<li>‘*’標註科目為選修科目。</li>";
            	    //$x .= "<li>‘(c)’標註科目為中文授課科目。( <i>只適用於小學英文部</i> )</li>";
            	    $x .= "<li class='math_equation'>學年平均分的總平均分 = 加總（學科學年平均分 x（";
            	       $x .= " <div class='equation'>
                                    <span class='numerator'>(學科單位)</span>
                                    <span class='bar'>/</span>
                                    <span class='denominator'>(單位總和)</span>
                                </div>";
            	    $x .= "）</li>";
        	    $x .= "</ul>";
    	    $x .= "</td>";
	    $x .= "</tr>";
	    $x .= "<tr>";
	       $x .= "<td colspan='4'>&nbsp;</td>";
	    $x .= "</tr>";
	    
	    $x .= "<tr>";
    	    $x .= "<td colspan='2'><b><i>（2）<u>等第</u></i></b></td>";
    	    $x .= "<td colspan='2'><b><i>（3）<u>簡稱</u></i></b></td>";
    	    $x .= "</tr>";
	    $x .= "<tr class='chinese_remarks'>";
    	    $x .= "<td valign='top'>";
        	    $x .= "<ul class='list_view'>";
            	    $x .= "<li>A</li>";
            	    $x .= "<li>B</li>";
            	    $x .= "<li>C</li>";
            	    $x .= "<li>D</li>";
        	    $x .= "</ul>";
    	    $x .= "</td>";
    	    $x .= "<td valign='top'>";
        	    $x .= "<ul>";
            	    $x .= "<li>- 優</li>";
            	    $x .= "<li>- 良</li>";
            	    $x .= "<li>- 可</li>";
            	    $x .= "<li>- 劣</li>";
    	       $x .= "</ul>";
    	    $x .= "</td>";
    	    $x .= "<td valign='top'>";
        	    $x .= "<ul class='list_view'>";
            	    $x .= "<li>C.A.</li>";
            	    $x .= "<li>Min. Ach.</li>";
            	    $x .= "<li>Maj. Ach.</li>";
            	    $x .= "<li>Min. Flt.</li>";
            	    $x .= "<li>Maj. Flt.</li>";
        	    $x .= "</ul>";
    	    $x .= "</td>";
    	    $x .= "<td valign='top'>";
        	    $x .= "<ul>";
            	    $x .= "<li>- 持續性評估</li>";
            	    $x .= "<li>- 小功</li>";
            	    $x .= "<li>- 大功</li>";
            	    $x .= "<li>- 小過</li>";
            	    $x .= "<li>- 大過</li>";
        	    $x .= "</ul>";
    	    $x .= "</td>";
	    $x .= "</tr>";
	    $x .= "</table>";
	    
	    return $x;
	}
    
	########### END Template Related
    
	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr, $isFirst=0, $extraValueArr=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
 		$SemID = $ReportSetting['Semester'];
 		$SemNum = intval($this->Get_Semester_Seq_Number($SemID));
 		$ReportType = $SemID == "F" ? "W" : "T";
 		$ClassLevelID = $ReportSetting['ClassLevelID'];
 		$SchoolType = $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
 		$isPrimarySchool = $SchoolType == 'P';
 		
//  	$LineHeight 				= $ReportSetting['LineHeight'];
//  	$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
//  	$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
//
// 		# updated on 08 Dec 2008 by Ivan
// 		# if subject overall column is not shown, grand total, grand average... also cannot be shown
// 		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
// 		$ShowRightestColumn = $ShowSubjectOverall;
//
//		# initialization
// 		$border_top = $isFirst ? "border_top" : "border_top_1px";
// 		$font_class = ($ReportType=='T')? 'font16px' : 'font13px';
		
		# Get Term Reports
		$TermReportIDArr = $this->GET_ALL_RELATED_TERM_REPORT_LIST($ReportID);
		
		$x = "";
		$x .= "<tr>";
		    $x .= $this->Generate_Info_Title_td($ReportType, $TitleEn, $TitleCh, $isFirst);
		
		// loop Term Reports
		$isFirst = 1;
		foreach((array)$TermReportIDArr as $thisTermReportID)
		{
		    # No need to display next term report
	        if($ReportType == 'T' && $thisTermReportID == '')
		    {
// 	            $x .= "<td colspan='2' class='border_right'>&nbsp;</td>";
		    	$x .= "<td colspan='2' class='border_right'>".$this->EmptySymbol."</td>";
		        continue;
		    }
		    
		    # Retrieve Term Report Display Settings
		    $TermReportSetting = $this->returnReportTemplateBasicInfo($thisTermReportID);
		    $thisSemID = $TermReportSetting['Semester'];
		    
		    # display Term values
            if(!empty($extraValueArr)) {
                if(!$StudentID) {
                    $thisValue = '#';
                } else if ($ValueArr[$thisSemID][$InfoKey] != '') {
                    $thisValue = $ValueArr[$thisSemID][$InfoKey];
                } else if ($extraValueArr[$thisTermReportID] != '') {
                    $thisValue = $extraValueArr[$thisTermReportID];
                } else {
                    $thisValue = $this->EmptySymbol;
                }
            }
            else {
		        $thisValue = $StudentID ? ($ValueArr[$thisSemID][$InfoKey] != '' ? $ValueArr[$thisSemID][$InfoKey] : $this->EmptySymbol) : '#';
            }
			$x .= "<td colspan='2' height='{$LineHeight}' class='border_right'>". $thisValue ."</td>";
		}
		
		# display Overall values (always show '---')
        $OverallValue = $ReportType == 'T' ? $this->EmptySymbol : ($StudentID ? ($OverallValue ? $OverallValue : $this->EmptySymbol) : '#');

// 		if(!$isPrimarySchool) {
		    $x .= "<td align='center' height='{$LineHeight}' class='border_right'>". $OverallValue."</td>";
		    $x .= "<td align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
		/*} else {
		    $x .= "<td align='center' height='{$LineHeight}'>". $OverallValue."</td>";
		}*/
		
		$x .= "</tr>";
        
		return $x;
	}
    
	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0, $UpperLimit="", $MarkType='')
	{
	    global $eReportCard, $eRCTemplateSetting;
	    
	    # Retrieve Display Settings
	    $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
	    $SemID = $ReportSetting['Semester'];
// 	    $SemNum = intval($this->Get_Semester_Seq_Number($SemID));
	    $ReportType = $SemID == "F" ? "W" : "T";
	    $ClassLevelID = $ReportSetting['ClassLevelID'];
	    $SchoolType = $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
	    $isPrimarySchool = $SchoolType == 'P';
	    
	    # Retrieve Storage Settings
	    $StorageSetting = $this->LOAD_SETTING("Storage&Display");
	    $grandAverageDecimal = $StorageSetting["GrandAverage"];
	    
// 		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
// 		$CalOrder = $this->Get_Calculation_Setting($ReportID);
// 		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
//
// 		$border_top = $isFirst ? "border_top" : "border_top_1px";
// 		$font_class = ($ReportType=='T')? 'font16px' : 'font13px';
// 	    $border_right = $isPrimarySchool? '' : ' border_right ';
	    $border_right = ' border_right ';
	    
		# Get Term Reports
		$TermReportIDArr = $this->GET_ALL_RELATED_TERM_REPORT_LIST($ReportID);
        
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($ReportType, $TitleEn, $TitleCh, $isFirst);
			
			# display Term values
			$isFirst = 1;
			foreach((array)$TermReportIDArr as $thisTermReportID)
			{
			    $thisValue = $ValueArr[$thisTermReportID];
			    
			    # No need to display next term report
			    if($ReportType == 'T' && $thisTermReportID == '') {
// 			        $thisDisplay = "&nbsp;";
					$thisDisplay = $this->EmptySymbol;
			    }
			    else {
			        $thisDisplay = $this->Get_Score_Display_HTML($thisValue, $thisTermReportID, $ClassLevelID, $SubjectID, $thisValue, 'GrandAverage', '', '', '', 0, $grandAverageDecimal);
			    }
			    $x .= "<td colspan='2' height='{$LineHeight}' class='border_right'>". $thisDisplay ."</td>";
			}
			
			# display Overall values
			if($ReportType == 'T') {
// 			    $thisDisplay = "&nbsp;";
				$thisDisplay = $this->EmptySymbol;
			}
			else {
			    $thisDisplay = $this->Get_Score_Display_HTML($OverallValue, $ReportID, $ClassLevelID, $SubjectID, $OverallValue, 'GrandAverage', '', '', '', 0, $grandAverageDecimal);
			}
			
			$x .= "<td align='center' height='{$LineHeight}' class='$border_right'>". $thisDisplay ."</td>";
// 			if(!$isPrimarySchool) {
			$x .= "<td align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
// 			}
			
		$x .= "</tr>";
        
		return $x;
	}
    
	function Generate_Info_Title_td($ReportType, $TitleEn, $TitleCh, $BorderTopThick=0)
	{
// 		$border_top = ($BorderTopThick)? "border_top" : "border_top_1px";
// 		$font_class = ($ReportType=='T')? 'font16px' : 'font_8pt';
	    
	    $x = "";
		$x .= "<td colspan='1' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
// 			    $x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'>$TitleCh &nbsp; $TitleEn</td></tr>";
				$x .= "<tr>";
					$x .= "<td width='1'>&nbsp;&nbsp;</td>";
					$x .= "<td height='{$LineHeight}' style='text-align: left; width: 5%'>&nbsp;</td>";
					if(strlen($TitleCh) > 12){
						$x .= "<td height='{$LineHeight}' style='text-align: left; width: 44%'>$TitleCh</td>";
					} else {
						$x .= "<td height='{$LineHeight}' style='text-align: justify; text-align-last: justify; width: 25%'>$TitleCh</td>";
						$x .= "<td height='{$LineHeight}' style='text-align: left; width: 19%'>&nbsp;</td>";
					}
					$x .= "<td height='{$LineHeight}' style='text-align: left; width: 49%'>$TitleEn</td>";
				$x .= "</tr>";
			$x .= "</table>";
		$x .= "</td>";
		$x .= "<td  class='border_right'></td>";
		
// 		$x .= "<td class='border_top' height='{$LineHeight}' colspan='2' style='text-align:left;'>";
// 			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
// 				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}' class='".$font_class."'  style=\"text-align:left;\">". $TitleEn ."</td></tr>";
// 			$x .= "</table>";
// 		$x .= "</td>";

		return $x;
	}
    
	function Is_Show_Rightest_Column($ReportID)
	{
// 		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
// 		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
// 		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
// 		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
// 		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
// 		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
// 		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
// 		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
// 		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
//
// 		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
// 		$ShowRightestColumn = $ShowSubjectOverall;

		return $ShowRightestColumn;
	}
    
	function Get_Calculation_Setting($ReportID)
	{
// 		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
// 		$SemID 						= $ReportSetting['Semester'];
//  	$ReportType 				= $SemID == "F" ? "W" : "T";
//
// 		$CalSetting = $this->LOAD_SETTING("Calculation");
// 		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];

		return $CalOrder;
	}
	
	function Get_Term_Name_Display($TermNameEN)
	{
	    $numAry = array('1st', '2nd', '3rd', '4th', '5th');
	    $supNumAry = array('1<sup>st</sup>', '2<sup>nd</sup>', '3<sup>rd</sup>', '4<sup>th</sup>', '5<sup>th</sup>');
	    
	    foreach($numAry as $thisIndex => $thisNum) {
	        if(strpos($TermNameEN, $thisNum) !== false) {
	            $TermNameEN = str_replace($thisNum, $supNumAry[$thisIndex], $TermNameEN);
	        }
	    }
	    return $TermNameEN;
	}
	
	function Get_Student_Failed_Subject($ReportID, $StudentIDArr)
	{
		global $lreportcard, $eReportCard;
		
		if (!is_array($StudentIDArr)) {
			$StudentIDArr = array($StudentIDArr);
		}
		$numOfStudent = count($StudentIDArr);
		
		### Get Report Info
		$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		
		### Get Subject
		$SubjectAssoInfoArr = $lreportcard->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=1, $ReportID);
		
		### Get Mark Nature
		//$MarkNatureArr[$StudentID][$SubjectID][$ReportColumnID]['Weight', 'Nature', ...] = data
		$MarkNatureArr = $lreportcard->Get_Student_Subject_Mark_Nature_Arr($ReportID, $ClassLevelID, $lreportcard->getMarks($ReportID), $WithExtraData=false);
		
		### Get failed Subjects
		$failedSubjectArr = array();
		$ReportColumnID = 0;	         // Check overall column only
		for ($i=0; $i<$numOfStudent; $i++)
		{
			$thisStudentID = $StudentIDArr[$i];
			
			// loop $SubjectAssoInfoArr to maintain Subject ordering
			foreach((array)$SubjectAssoInfoArr as $thisSubjectID => $thisSubjectName)
			{
				$thisNature = $MarkNatureArr[$thisStudentID][$thisSubjectID][$ReportColumnID];
				if ($thisNature == 'Fail') {
				    $thisSubjectName = $this->GET_SUBJECT_NAME_LANG($thisSubjectID, "CH");
				    $thisSubjectName .= " ";
				    $thisSubjectName .= $this->GET_SUBJECT_NAME_LANG($thisSubjectID, "EN");
				    $failedSubjectArr[] = $thisSubjectName;
				}
			}
		}
		
		return $failedSubjectArr;
	}
	
	function getStudentStudyType($StudentID)
	{
	    // return 'Arts';
	    // return 'Sci';
	    return '';
	}
	
	function GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID)
	{
	    $PreloadArrKey = 'GET_SCHOOL_TYPE_BY_CLASSLEVEL';
	    $FuncArgArr = get_defined_vars();
	    $returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
	    if ($returnArr !== false) {
	        return $returnArr;
	    }
	    
	    global $PATH_WRT_ROOT;
	    include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
	    
	    $objYear = new Year($ClassLevelID);
	    $FormWebSAMSCode = $objYear->WEBSAMSCode;
	    $SchoolTypeCode = substr($FormWebSAMSCode, 0, 1);
	    // $SchoolTypeCode = 'P';
	    // $SchoolTypeCode = 'S';
	    
	    $this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $SchoolTypeCode);
	    return $SchoolTypeCode;
	}
	
	function GET_SECTION_TYPE_BY_CLASSLEVEL($ClassLevelID)
	{
	    $PreloadArrKey = 'GET_SECTION_TYPE_BY_CLASSLEVEL';
	    $FuncArgArr = get_defined_vars();
	    $returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
	    if ($returnArr !== false) {
	        return $returnArr;
	    }
	    
	    global $PATH_WRT_ROOT;
	    include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
	    $objYearClass = new year_class();
	    
	    $SectionTypeCode = '';
	    $FormClassArr = $this->GET_CLASSES_BY_FORM($ClassLevelID);
	    $FormClassIDArr = Get_Array_By_Key((array)$FormClassArr, 'ClassID');
	    foreach((array)$FormClassIDArr as $thisClassID) {
	        $thisClassGroupInfo = $objYearClass->Get_Class_Group_Info($thisClassID);
	        $thisClassGroupCode = $thisClassGroupInfo[0]['Code'];
	        if($thisClassGroupCode != '') {
	            $SectionTypeCode = substr($thisClassGroupCode, 0, 1);
	        }
	    }
	    // $SectionTypeCode = 'C';
	    // $SectionTypeCode = 'E';
	    
	    $this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $SectionTypeCode);
	    return $SectionTypeCode;
	}

    function GET_SECTION_TYPE_BY_CLASSLEVEL_NAME($ClassLevelID)
    {
        $PreloadArrKey = 'GET_SECTION_TYPE_BY_CLASSLEVEL_NAME';
        $FuncArgArr = get_defined_vars();
        $returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
        if ($returnArr !== false) {
            return $returnArr;
        }

        global $PATH_WRT_ROOT;
        include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');

        $objYear = new Year($ClassLevelID);
        $FormName = $objYear->YearName;
        $SectionTypeCode = (str_lang($FormName) == "ENG") ? "ES" : "CS";
        // $SectionTypeCode = 'C';
        // $SectionTypeCode = 'E';

        $this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $SectionTypeCode);
        return $SectionTypeCode;
    }
	
	function GET_ALL_RELATED_TERM_REPORT_LIST($ReportID)
	{
	    $PreloadArrKey = 'GET_ALL_RELATED_TERM_REPORT_LIST';
	    $FuncArgArr = get_defined_vars();
	    $returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
	    if ($returnArr !== false) {
	        return $returnArr;
	    }
	    
	    global $PATH_WRT_ROOT;
	    
	    # Retrieve Display Settings
	    $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
	    $SemID = $ReportInfoArr['Semester'];
	    $SemNum = $this->Get_Semester_Seq_Number($SemID);
	    $ReportType = ($SemID == 'F') ? 'W' : 'T';
	    $ClassLevelID = $ReportInfoArr['ClassLevelID'];
	    $SchoolType = $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
	    $isPrimarySchool = $SchoolType == 'P';
	    
	    # Get Term Reports
	    $TermReportIDArr = array();
	    if($ReportType == 'T')
	    {
	        $ConsolidatedReportArr = $this->Get_Consolidated_Report_List($ClassLevelID, $ReportID);
	        if(!empty($ConsolidatedReportArr))
	        {
	            $ConsolidatedReportID = $ConsolidatedReportArr[0];
	            
	            // for 146 testing
	            //$ConsolidatedReportID = $ConsolidatedReportArr[1];
	        }
	        
	        // Have related Consolidate Report
	        if($ConsolidatedReportID)
	        {
	            $TermReportArr = $this->Get_Related_TermReport_Of_Consolidated_Report($ConsolidatedReportID, $MainReportOnly=1, $ReportColumnID="ALL");
	            $TermReportIDArr = Get_Array_By_Key($TermReportArr, 'ReportID');
	            $TermReportIDArr = array_values(array_unique(array_filter($TermReportIDArr)));
	        }
	        // Without Consolidate Report
	        else
	        {
	            $allSemesterArr = $this->GET_ALL_SEMESTERS();
	            foreach((array)$allSemesterArr as $thisSemID => $thisSemName)
	            {
	                $thisSemNum = $this->Get_Semester_Seq_Number($thisSemID);
// 	                if(!$isPrimarySchool && ($thisSemNum == 3 || $thisSemNum == 4)) {
	                if($isPrimarySchool && ($thisSemNum == 3 || $thisSemNum == 4)) {
	                    continue;
	                }
	                
	                $thisTermReportInfo = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $thisSemID, $isMainReport=1);
	                $TermReportIDArr[] = $thisTermReportInfo['ReportID'];
	            }
	        }
	        
	        // for Next term report > Clear ReportID for further checking
	        foreach($TermReportIDArr as $thisReportSeq => $thisReportID) {
	            if($thisReportSeq > ($SemNum-1)) {
	                $TermReportIDArr[$thisReportSeq] = '';
	            }
	        }
	    }
	    else
	    {
	        $TermReportArr = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly = 1, $ReportColumnID = "ALL");
	        $TermReportIDArr = Get_Array_By_Key($TermReportArr, 'ReportID');
	        $TermReportIDArr = array_values(array_unique(array_filter($TermReportIDArr)));
	    }
	    if(sizeof($TermReportIDArr) > 2){
	    	$TermReportIDArr = array($TermReportIDArr[0], $TermReportIDArr[1]);
	    }
	    $this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $TermReportIDArr);
	    return $TermReportIDArr;
	}
	
	function loadingMeritFromDiscipline($ReportID, $StudentID, $forConductCalculation=false, $forAwardCalculation=false)
	{
	    global $PATH_WRT_ROOT, $eRCTemplateSetting;
	    include_once ($PATH_WRT_ROOT."includes/form_class_manage.php");
	    
	    # Retrieve Display Settings
	    $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
	    $SemID = $ReportInfoArr['Semester'];
	    $SemNum = $this->Get_Semester_Seq_Number($SemID);
	    $ReportType = ($SemID == 'F') ? 'W' : 'T';
	    $ClassLevelID = $ReportInfoArr['ClassLevelID'];
	    $SchoolType = $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
	    
	    # Get Term Reports
	    $TermReportIDArr = $this->GET_ALL_RELATED_TERM_REPORT_LIST($ReportID);
	    
	    // loop Term Reports
	    $dataAry = array();
	    foreach($TermReportIDArr as $thisIndex => $thisTermReportID)
	    {
	        # No need to display next term report
	        if($ReportType == 'T' && $thisTermReportID == '') {
	            continue;
	        }

			# Term Report > only return current term data for conduct calculation
			if($forConductCalculation && $ReportType == 'T' && $ReportID != $thisTermReportID) {
				continue;
			}

	        # Retrieve Term Report Display Settings
	        $ReportInfoArr = $this->returnReportTemplateBasicInfo($thisTermReportID);
	        $SemID = $ReportInfoArr['Semester'];
	        $TermStartDate = $ReportInfoArr['TermStartDate'];
	        $TermEndDate = $ReportInfoArr['TermEndDate'];
	        
	        # Retrieve Term Date Range
	        if (is_date_empty($TermStartDate) && is_date_empty($TermEndDate))
	        {
                $objYearTerm = new academic_year_term($SemID);
                $TermStartDate = substr($objYearTerm->TermStart, 0, 10);
                $TermEndDate = substr($objYearTerm->TermEnd, 0, 10);
	        }

	        $extraCond = '';
	        if ($forConductCalculation)
	        {
                $extraCond = " AND (mr.MeritType = -1 OR (
                                            mr.MeritType = 1 AND 
                                            mic.CatID = '".$eRCTemplateSetting['DisciplineTargetCategory']['ConductRewards']."' AND 
                                            mi.ItemCode IN ('".implode("', '", $eRCTemplateSetting['DisciplineTargetCategory']['ConductRewardCodes'])."')
                                        )
                                    )";
            }
            if($forAwardCalculation)
            {
                $extraCond = " AND mr.MeritType = 1 AND ((
                                            mic.CatID = '".$eRCTemplateSetting['DisciplineTargetCategory']['AcademicRewards']."' AND 
                                            mi.ItemCode IN ('".implode("', '", $eRCTemplateSetting['DisciplineTargetCategory']['AcademicRewardCodes'])."')
                                        ) OR (
                                            mic.CatID = '".$eRCTemplateSetting['DisciplineTargetCategory']['ServiceRewards']."' AND 
                                            mi.ItemCode IN ('".implode("', '", $eRCTemplateSetting['DisciplineTargetCategory']['ServiceRewardCodes'])."')
                                        )
                                    )";
            }

            $groupBy = 'mr.StudentID, mic.CatID, mr.ProfileMeritType';
            if ($forConductCalculation)
            {
                $groupBy = 'mr.StudentID, mr.ProfileMeritType';
            }
            if($forAwardCalculation)
            {
                $groupBy = 'mr.StudentID, mic.CatID';
            }

    	    # Retrieve approved and released Merit & Demerit
    	    $sql = "SELECT
    					mr.StudentID, 
                        CASE
                           WHEN mic.CatID = ".$eRCTemplateSetting['DisciplineTargetCategory']['AcademicRewards']." THEN 'AcademicRewards'
                           WHEN mic.CatID = ".$eRCTemplateSetting['DisciplineTargetCategory']['ServiceRewards']." THEN 'ServiceRewards'
                           WHEN mic.CatID = ".$eRCTemplateSetting['DisciplineTargetCategory']['ConductRewards']." THEN 'ConductRewards'
                           WHEN mic.CatID = ".$eRCTemplateSetting['DisciplineTargetCategory']['ConductPenalties']." THEN 'ConductPenalties'
                           WHEN mr.MeritType = -1 THEN 'NormalPenalties'
                        END AS recordCategory, 
                        mr.ProfileMeritType,
                        CEILING(SUM(mr.ProfileMeritCount)) as totalMeritTypeNumber,
                        COUNT(mr.RecordID) as totalRecord
    				FROM
                        DISCIPLINE_MERIT_RECORD mr
    					INNER JOIN DISCIPLINE_MERIT_ITEM mi ON (mi.ItemID = mr.ItemID)
    					INNER JOIN DISCIPLINE_MERIT_ITEM_CATEGORY mic ON (mic.CatID = mi.CatID)
    				WHERE
    					mr.AcademicYearID = '".$this->GET_ACTIVE_YEAR_ID()."' AND
    					mr.RecordDate BETWEEN '$TermStartDate' AND '$TermEndDate' AND
    					mr.StudentID = '$StudentID' AND 
                        mr.RecordStatus = 1 AND
                        mr.ReleaseStatus = 1
                        $extraCond
    				GROUP BY 
    				    $groupBy ";
    	    $result = $this->returnArray($sql);

            if ($forConductCalculation) {
                $dataAry[$thisIndex] = BuildMultiKeyAssoc($result, array('ProfileMeritType'), array('totalMeritTypeNumber'), $SingleValue=1);
            }
            else if ($forAwardCalculation) {
                $dataAry[$thisIndex] = BuildMultiKeyAssoc($result, array('recordCategory'), array('totalRecord'), $SingleValue=1);
            }
            else {
                $dataAry[$thisIndex] = BuildMultiKeyAssoc($result, array('recordCategory', 'ProfileMeritType'), array('totalMeritTypeNumber'), $SingleValue=1);
            }
	    }

        if ($forConductCalculation || $forAwardCalculation)
        {
	        $summarizeDataAry = array();
	        foreach($dataAry as $thisDataAry) {
	            foreach((array)$thisDataAry as $thisKey => $thisValue) {
	                if(!isset($summarizeDataAry[$thisKey])) {
                        $summarizeDataAry[$thisKey] = 0;
                    }

                    $summarizeDataAry[$thisKey] += $thisValue;
                }
            }

            $dataAry = $summarizeDataAry;
        }
	    
	    return $dataAry;
	}
	
	function loadingDataFromEnrolment($ReportID, $StudentID)
	{
	    global $PATH_WRT_ROOT, $eRCTemplateSetting;
	    include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
	    $fcm = new form_class_manage();
	    
	    # Retrieve Display Settings
	    $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
	    $SemID = $ReportInfoArr['Semester'];
	    $ReportType = ($SemID == 'F') ? 'W' : 'T';
	    $ClassLevelID = $ReportInfoArr['ClassLevelID'];
	    $SchoolType = $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
	    
	    # Get Term Reports
	    $TermReportIDArr = $this->GET_ALL_RELATED_TERM_REPORT_LIST($ReportID);
	    // loop Term Reports
	    $dataAry = array();
	    $dataAry['ECA'] = array();
	    foreach($TermReportIDArr as $thisIndex => $thisTermReportID)
	    {
	        # Retrieve Term Report Display Settings
	        $ReportInfoArr = $this->returnReportTemplateBasicInfo($thisTermReportID);
	        $SemID = $ReportInfoArr['Semester'];
	        $SemSeq = $this->Get_Semester_Seq_Number($SemID);
	        $ReportType = ($SemID == 'F') ? 'W' : 'T';
	        # No need to display next term report
	        if($ReportType == 'T' && $thisTermReportID == '') {
	        	continue;
	        }
// 	        $SemesterArr = $fcm->Get_Academic_Year_Term_List($this->GET_ACTIVE_YEAR_ID());
// 	        if($SemID == 'F') $SemID = $SemesterArr[1]['YearTermID'];
	        $cond = " iegi.Semester = '$SemID' AND ";
	        
	        // for 146 testing
	        //$cond = "";
	        
	        # Retrieve Extra-Curricular Activities
	        $sql = "SELECT
            	        iu.UserID,
            	        iu.GroupID,
            	        iu.RecordType,
            	        iu.RecordStatus,
            	        iu.Performance,
            	        iu.isActiveMember,
            	        iu.EnrolGroupID,
            	        ig.Title as ClubTitleEN,
            	        ig.TitleChinese as ClubTitleB5,
            	        iu.Achievement,
            	        iegi.GroupCategory as CategoryID
	               FROM
                        INTRANET_USERGROUP as iu
             	        INNER JOIN INTRANET_GROUP as ig ON (iu.GroupID = ig.GroupID)
            	        INNER JOIN INTRANET_ENROL_GROUPINFO as iegi On (iu.EnrolGroupID = iegi.EnrolGroupID)
	               WHERE
	                    iu.UserID = '$StudentID' AND
                        ig.AcademicYearID = '".$this->GET_ACTIVE_YEAR_ID()."' AND
						$cond
					    ig.RecordType = 5 AND
					    iegi.GroupCategory = '".$eRCTemplateSetting['EnrolTargetCategory']['ECA']."'
				   ORDER BY
						ClubTitleEN ";
            $dataAry['ECA'][] = $this->returnArray($sql);
	    }

        # Retrieve School Team / Club
        # 20191216 Philips - Query School Team by Whole year
        //$cond = " iegi.Semester = '0' AND ";
        $order = ( $eRCTemplateSetting['EnrolTargetCategory']['SchoolTeamClub'] < $eRCTemplateSetting['EnrolTargetCategory']['PotentialDevelopment']) ? "ASC" : "DESC";
        $sql = "SELECT
                    CONCAT(ig.TitleChinese, ' ', ig.Title) as ClubTitle
                    /*IF(ig.TitleChinese IS NULL OR ig.TitleChinese = '', ig.Title, ig.TitleChinese) as ClubTitle
                    ig.Title as ClubTitleEN,
                    ig.TitleChinese as ClubTitleB5*/
                FROM
                    INTRANET_USERGROUP as iu
                    INNER JOIN INTRANET_GROUP as ig ON (iu.GroupID = ig.GroupID)
                    INNER JOIN INTRANET_ENROL_GROUPINFO as iegi On (iu.EnrolGroupID = iegi.EnrolGroupID)
                WHERE
                    iu.UserID = '$StudentID' AND
                    ig.AcademicYearID = '".$this->GET_ACTIVE_YEAR_ID()."' AND
                    iegi.Semester = '0' AND
                    ig.RecordType = 5 AND
                    iegi.GroupCategory IN ('".$eRCTemplateSetting['EnrolTargetCategory']['SchoolTeamClub']."', '".$eRCTemplateSetting['EnrolTargetCategory']['PotentialDevelopment']."')
                ORDER BY
                    iegi.GroupCategory $order,
                    ClubTitle ";

        //if(!isset($dataAry['SchoolTeamClub'])) {
        //    $dataAry['SchoolTeamClub'] = array();
        //}
        //$dataAry['SchoolTeamClub'] = array_merge($this->returnVector($sql), $dataAry['SchoolTeamClub']);
        //$dataAry['SchoolTeamClub'] = array_values(array_unique(array_filter($dataAry['SchoolTeamClub'])));
        //asort($dataAry['SchoolTeamClub']);
        $dataAry['SchoolTeamClub'] = $this->returnVector($sql);
	    if(sizeof($dataAry['SchoolTeamClub']) > 3){
	    	$dataAry['SchoolTeamClub'] = array($dataAry['SchoolTeamClub'][0], $dataAry['SchoolTeamClub'][1], $dataAry['SchoolTeamClub'][2]);
	    }
	    
	    return $dataAry;
	}
	
	function retrieveAttandanceMonthData($ReportID, $StudentID)
	{
	    global $PATH_WRT_ROOT, $eRCTemplateSetting;
	    include_once($PATH_WRT_ROOT.'includes/libcardstudentattend2.php');
	    
	    # Retrieve eAttendance Config
	    $lattend = new libcardstudentattend2();
	    $attendance_mode = $lattend->attendance_mode;
	    $record_count_value = $lattend->ProfileAttendCount == 1 ? 0.5 : 1;
	    $absent_reason_ary = $lattend->Get_Reason_Symbol(PROFILE_TYPE_ABSENT);
	    $absent_reason_ary = Get_Array_By_Key($absent_reason_ary, 'Reason');
	    
	    $present_count_value = $record_count_value;
	    $absent_count_value = $record_count_value;
	    $late_count_value = 1;
	    $early_leave_count_value = 1;
	    
	    # Retrieve Display Settings
	    $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
	    $SemID = $ReportInfoArr['Semester'];
	    $ReportType = ($SemID == 'F') ? 'W' : 'T';
	    $ClassLevelID = $ReportInfoArr['ClassLevelID'];
	    $SchoolType = $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
	    
	    # Get Term Reports
	    $TermReportIDArr = $this->GET_ALL_RELATED_TERM_REPORT_LIST($ReportID);
	    
	    // loop Term Reports
	    $dataAry = array();
	    foreach($TermReportIDArr as $thisIndex => $thisTermReportID)
        {
            # No need to display next term report
            if($ReportType == 'T' && $thisTermReportID == '') {
                continue;
            }
            
            # Retrieve Term Report Display Settings
            $ReportInfoArr = $this->returnReportTemplateBasicInfo($thisTermReportID);
            $SemID = $ReportInfoArr['Semester'];
            $TermStartDate = $ReportInfoArr['TermStartDate'];
            $TermEndDate = $ReportInfoArr['TermEndDate'];
            
            # Retrieve Term Date Range
            if (is_date_empty($TermStartDate) && is_date_empty($TermEndDate))
            {
                $objYearTerm = new academic_year_term($SemID);
                $TermStartDate = substr($objYearTerm->TermStart, 0, 10);
                $TermEndDate = substr($objYearTerm->TermEnd, 0, 10);
            }
            
            # Get related db table
            $table = array();
            if (strtotime($TermStartDate) == strtotime($TermEndDate))
            {
                $year = date('Y', strtotime($TermStartDate));
                $month = date('m', strtotime($TermEndDate));
                
                // $lattend->createTable_Card_Student_Daily_Log($year, $month);
                $table[] = "CARD_STUDENT_DAILY_LOG_" . $year . _ . $month;
            }
            else
            {
                $yearMonthArr = $this->Get_Year_Month_By_Date_Range($TermStartDate, $TermEndDate);
                foreach(array_keys((array)$yearMonthArr) as $year) {
                    foreach ((array)$yearMonthArr[$year] as $month) {
                        // $this->createTable_Card_Student_Daily_Log($year, $month);
                        $temp_table = "CARD_STUDENT_DAILY_LOG_" . $year . _ . $month;
                        $table[] = $temp_table;
                    }
                }
            }
            
            # Get Attendance data
            $monthDataAry = $lattend->retrieveCustomizedIndividualMonthDataByDateRange(array($StudentID), $table);
            
            # Init data array
            $reportData = array();
            $reportData['Lateness'] = 0;
            $reportData['UnexcusedAbsent'] = 0;
            $reportData['PersonalLeave'] = 0;
            $reportData['SickLeave'] = 0;
//             foreach((array)$absent_reason_ary as $this_absent_reason) {
//                 $reportData[$this_absent_reason] = 0;
//             }
            
            // loop data records
            $iniVal = strtotime($TermStartDate);
            $target = strtotime($TermEndDate);
            for ($li = $iniVal; $li <= $target; $li += 86400)
            {
                $j = date('Y-m-d', $li);
                
                if (sizeof($monthDataAry[$StudentID][$j]) != 0)
                {
                    list($am, $pm, $leave, $am_late_waive, $pm_late_waive, $am_absent_waive, $pm_absent_waive, $am_early_waive, $pm_early_waive, 
                            $am_late_reason, $pm_late_reason, $am_absent_reason, $pm_absent_reason, $am_early_leave_reason, $pm_early_leave_reason, 
                            $am_late_session, $am_request_leave_session, $am_play_truant_session, $am_offical_leave_session, $am_absent_session, 
                        $pm_late_session, $pm_request_leave_session, $pm_play_truant_session, $pm_offical_leave_session, $pm_absent_session) = $monthDataAry[$StudentID][$j];
                    
                    // $reportData['Lateness'] += ($am_late_session + $pm_late_session);
                    // $reportData['RequestLeave'] += ($am_request_leave_session + $pm_request_leave_session);
                    // $reportData['OfficalLeave'] += ($am_offical_leave_session + $pm_offical_leave_session);
                    // $reportData['Absenteeism'] += $am_absent_session + $pm_absent_session;
                    
                    # AM Data
                    if ($am != "")
                    {
                        # Absent (with Reason)
                        if($am == '1' && $am_absent_waive != 1 && $am_absent_reason != '' && in_array($am_absent_reason, (array)$absent_reason_ary)) {
                            if($am_absent_reason == $eRCTemplateSetting['AttendanceTargetReason']['PersonalLeave']) {
                                $reportData['PersonalLeave'] += $am_absent_session;
                            }
                            else if($am_absent_reason == $eRCTemplateSetting['AttendanceTargetReason']['SickLeave']) {
                                $reportData['SickLeave'] += $am_absent_session;
                            }
                        }
                        
                        # Lateness
                        if ($am == '2' && $am_late_waive != 1) {
                            $reportData['Lateness'] += $late_count_value;
                        }
                        
                        # Unexcused Absent
                        $reportData['UnexcusedAbsent'] += $am_play_truant_session;
                        
                    }
                    
                    # PM Data
                    if ($pm!= "")
                    {
                        # Absent (with Reason)
                        if($pm == '1' && $pm_absent_waive != 1 && $pm_absent_reason != '' && in_array($pm_absent_reason, (array)$absent_reason_ary)) {
                            if($pm_absent_reason == $eRCTemplateSetting['AttendanceTargetReason']['PersonalLeave']) {
                                $reportData['PersonalLeave'] += $pm_absent_session;
                            }
                            else if($pm_absent_reason == $eRCTemplateSetting['AttendanceTargetReason']['SickLeave']) {
                                $reportData['SickLeave'] += $pm_absent_session;
                            }
                        }
                        
                        # Lateness
                        if ($pm == '2' && $pm_late_waive != 1) {
                            $reportData['Lateness'] += $late_count_value;
                        }
                        
                        # Unexcused Absent
                        $reportData['UnexcusedAbsent'] += $pm_play_truant_session;
                    }
                }
            }
            
            $dataAry[$thisIndex] = $reportData;
        }
        
        return $dataAry;
	}

    /*
    function Get_Student_Conduct_Grade($ReportID, $StudentID)
    {
        global $PATH_WRT_ROOT, $eRCTemplateSetting;
        include_once ($PATH_WRT_ROOT."includes/form_class_manage.php");

        $meritDataAry = $this->loadingMeritFromDiscipline($ReportID, $StudentID, true);

        $conductScore = 0;
        foreach($meritDataAry as $thisMeritType => $thisMeritCount) {
            if(isset($eRCTemplateSetting['MeritConductScore'][$thisMeritType])) {
                $conductScore += $eRCTemplateSetting['MeritConductScore'][$thisMeritType] * $thisMeritCount;
            }
        }

        $conductGrade = 'D';
        foreach((array)$eRCTemplateSetting['ConductScoreLowerLimit'] as $thisRangeGrade => $thisRangeLowerLimit) {
            if($conductScore >= $thisRangeLowerLimit) {
                $conductGrade = $thisRangeGrade;
                break;
            }
        }

        return $conductGrade;
    }
    */

    function Calculate_Student_Conduct_Score_Grade($ReportID, $StudentID)
    {
        global $eRCTemplateSetting;

        $meritDataAry = $this->loadingMeritFromDiscipline($ReportID, $StudentID, true);

        // Calculate Conduct Score
        $conductScore = 0;
        foreach($meritDataAry as $thisMeritType => $thisMeritCount) {
            if(isset($eRCTemplateSetting['MeritConductScore'][$thisMeritType])) {
                $conductScore += $eRCTemplateSetting['MeritConductScore'][$thisMeritType] * $thisMeritCount;
            }
        }

        // Calculate Conduct Grade
        $conductGrade = '';
        foreach((array)$eRCTemplateSetting['ConductScoreLowerLimit'] as $thisRangeGrade => $thisRangeLowerLimit) {
            if($conductScore >= $thisRangeLowerLimit) {
                $conductGrade = $thisRangeGrade;
                break;
            }
        }

        return array($conductScore, $conductGrade);
    }
	
	function Get_Year_Month_By_Date_Range($startDate, $endDate)
	{
	    $time1 = strtotime($startDate);
	    $time2 = strtotime($endDate);
	    
	    $months = array();
	    while ($time1 < $time2) {
	        $y = date("Y", $time1);
	        $m = date("m", $time1);
	        
	        if (!isset($months[$y])) {
	            $months[$y] = array();
	        }
	        $months[$y][] = $m;
	        $time1 = mktime(0, 0, 0, intval($m) + 1, 1, intval($y));
	    }
	    
	    return $months;
	}
}
?>