<?php
# Editing by 
/*
 * 20150602 Bill [2015-0512-0955-58164]
 * modified getLayout(), getAwardsTable(), Get_Student_Award_List()
 * 	- add table - Award from School in second page
 * 20150205 Bill [2015-0204-1212-26066]
 * modified libreportcardcustom(), getRemarksTable()
 * 	- update the remarks of all F4 & F5 reports 
 * 20141103 Siuwan [2014-0912-1716-13164]
 * modified function getLayout() 
 * 	- apply $PrintTemplateType of Formal and Draft. Hide header and signature for Draft
 *  - display remark table on next page
 * modified function genMSTableFooter(), and add variable MinGrandAvgToDisplayFormPosition
 *  - display form position if grand avg is higher than MinGrandAvgToDisplayFormPosition
 *  - hide $FormPosition,$ClassPosition,$GrandTotal,$AverageMark if either term got no GrandTotal
 * modified function getReportStudentInfo()
 *  - add Student ID and Admission Day
 * modified function genMSTableMarks()
 *  - hide subject overall mark if either term got no mark
 * 
 * 2012-0229-1105-24073
 * Improved: 	Syn S6 Report with S7 Report and S5 Report Remarks
 * 				S4,5,6 Consolidate 's remark are the same as 上學期's remark
 * 
 * 2012-0222-1111-58073
 * Improved: Added the number before the extra remarks.
 * 
 * 2012-0117-0946-35066
 * change remarks
 * 
 * 2011-0627-0932-58071
 * Hide the Subject Row if the Subject marks are all "" or "--" or "*"
 * 
 * 2011-0224-1046-52073
 * Class Form Position display change:
 * 1) 24 / 40	for showing both Position and Num of Student
 * 2) 24		for showing Position only
 * 3) -- / 40	for showing Num of Student only
 * 4) --		for hiding both Position and Num of Student
 * 
 * 2011-0217-1554-29073
 * 1) F5 use F4 template
 * 2) modification of remarks
 * 3) Hide Not Submit HW info
 */
### Customization library for Buddhist Fat Ho Memorial College ###
include_once($intranet_root."/lang/reportcard_custom/buddhist_fat_ho.$intranet_session_language.php");
// [2015-0512-0955-58164] include library of iPortfolio config
include_once($intranet_root."/includes/portfolio25/iPortfolioConfig.inc.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		//$this->configFilesType = array("summary","attendance", "merit", "remark", "eca");
		$this->configFilesType = array("summary", "merit", "remark", "attendance", "subjectweight");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 0;
		$this->IsEnableMarksheetExtraInfo = 1;
		
		$this->EmptySymbol = "--";
		
		# declare percentage for remarks display
		$this->EngLanguagePer = array();
		// [2019-0214-1429-54066]
// 		$this->EngLanguagePer[1] = array("23%", "23%", "23%", "23%", "8%");
// 		$this->EngLanguagePer[2] = array("23%", "23%", "23%", "23%", "8%");
// 		$this->EngLanguagePer[3] = array("23%", "23%", "23%", "23%", "8%");
		$this->EngLanguagePer[1] = array("25%", "25%", "25%", "25%");
		$this->EngLanguagePer[2] = array("25%", "25%", "25%", "25%");
		$this->EngLanguagePer[3] = array("25%", "25%", "25%", "25%");
		$this->EngLanguagePer[4] = array("20%", "25%", "30%", "25%");
		$this->EngLanguagePer[5] = array("20%", "25%", "30%", "25%");
		//2012-0229-1105-24073
		//$this->EngLanguagePer[6] = array("18%", "18%", "18%", "18%", "28%");
		$this->EngLanguagePer[6] = array("20%", "25%", "30%", "25%");
		$this->EngLanguagePer[7] = array("18%", "18%", "18%", "18%", "28%");
		
		$this->ChiLanguagePer = array();
		//$this->ChiLanguagePer[1] = array("40%", "40%", "10%", "10%");
		//$this->ChiLanguagePer[2] = array("40%", "40%", "10%", "10%");
		//$this->ChiLanguagePer[3] = array("40%", "40%", "10%", "10%");
		//2013-0107-0951-11156
//		$this->ChiLanguagePer[1] = array("25%", "25%", "12%", "18%", "20%");
//		$this->ChiLanguagePer[2] = array("25%", "25%", "12%", "18%", "20%");
//		$this->ChiLanguagePer[3] = array("25%", "25%", "12%", "18%", "20%");
		$this->ChiLanguagePer[1] = array("40%", "30%", "12%", "18%");
		$this->ChiLanguagePer[2] = array("40%", "30%", "12%", "18%");
		$this->ChiLanguagePer[3] = array("40%", "30%", "12%", "18%");
//		$this->ChiLanguagePer[4] = array("25%", "25%", "12%", "18%", "20%");
//		$this->ChiLanguagePer[5] = array("25%", "25%", "12%", "18%", "20%");
		// 2015-0204-1212-26066 - changed F4 & F5 Chinese Language percentage display
		$this->ChiLanguagePer[4] = array("30%", "30%", "22.5%", "17.5%");
		$this->ChiLanguagePer[5] = array("30%", "30%", "22.5%", "17.5%");
		//2012-0229-1105-24073
		//$this->ChiLanguagePer[6] = array("25%", "15%", "30%", "15%", "15%");
		//2018-0529-0951-10206
		//$this->ChiLanguagePer[6] = array("25%", "25%", "12%", "18%", "20%");
		$this->ChiLanguagePer[6] = array("30%", "30%", "22.5%", "17.5%");
		$this->ChiLanguagePer[7] = array("25%", "15%", "30%", "15%", "15%");
		
		$this->ChiLanguageNCSPer[1] = array("25%", "25%", "25%", "25%");
		$this->ChiLanguageNCSPer[2] = array("25%", "25%", "25%", "25%");
		$this->ChiLanguageNCSPer[3] = array("25%", "25%", "25%", "25%");
		$this->ChiLanguageNCSPer[4] = array("25%", "25%", "25%", "25%");
		$this->ChiLanguageNCSPer[5] = array("25%", "25%", "25%", "25%");
		$this->ChiLanguageNCSPer[6] = array("25%", "25%", "25%", "25%");
		
		$this->ChiEngMathPer = array();
		$this->ChiEngMathPer[1] = "300";
		$this->ChiEngMathPer[2] = "300";
		$this->ChiEngMathPer[3] = "300";
		$this->ChiEngMathPer[4] = "300";
		$this->ChiEngMathPer[5] = "300";
		//2012-0229-1105-24073
		$this->ChiEngMathPer[6] = "300";
		
		$this->ISMarks = array();
		$this->ISMarks[1] = "200";
		$this->ISMarks[2] = "200";
		$this->ISMarks[3] = "200";
		$this->ISMarks[4] = "200";
		$this->ISMarks[5] = "200";
		//2012-0229-1105-24073
		$this->ISMarks[6] = "200";
		
		$this->OthersPer = array();
		$this->OthersPer[1] = "100";
		$this->OthersPer[2] = "100";
		$this->OthersPer[3] = "100";
		$this->OthersPer[4] = "100";
		$this->OthersPer[5] = "100";
		//2012-0229-1105-24073
		$this->OthersPer[6] = "100";
		
		$this->PassingMark = array();
		$this->PassingMark[1] = "50";
		$this->PassingMark[2] = "50";
		$this->PassingMark[3] = "50";
		$this->PassingMark[4] = "40";
		$this->PassingMark[5] = "40";
		$this->PassingMark[6] = "40";
		$this->PassingMark[7] = "40";
		
		$this->MinGrandAvgToDisplayFormPosition = 40;
		
		// [2015-0512-0955-58164] Component Code of OLE - "School Awards"
		$this->SchoolAwardCode = "15";
	}
		
	########## START Template Related ##############
	
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='', $PrintTemplateType='', $numOfStudent='', $studentCounter='') {
		global $eReportCard;
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ClassLevelID = $ReportSetting['ClassLevelID'];
 		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode=true);
 		$RemarksTable = $this->getRemarksTable($ReportID, $StudentID);
		// [2015-0512-0955-58164] Get Award Table
 		$AwardTable = $this->getAwardsTable($ReportID, $StudentID);
 		
 		$SignatureTable = $this->getSignatureTable($ReportID, $StudentID);
 		
 		$TableTop = "";
 		$TableTop .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
 		$TableTop .= $PrintTemplateType=="Draft"?"":"<tr><td>".$TitleTable."</td></tr>";
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		# Merits & Demerits and Class Teacjer Comment
		//if ($ReportType == "W" || $FormNumber==5 || $FormNumber==7)
		//2012-0229-1105-24073
		if ($ReportType == "W" || $FormNumber==7 || $FormNumber==6)
		{
			$TableTop .= "<tr><td>$MiscTable</td><tr>";
		}
		//$TableTop .= "<tr><td>$RemarksTable</td><tr>"; //2014-0912-1716-13164
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= $PrintTemplateType=="Draft"?"":"<tr><td>".$SignatureTable."</td></tr>";
		# hide footer row to move the signature to the bottom of the paper
		//$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
 		
		$x = "";
		$x .= "<tr valign='top'><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='900' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		//2014-0912-1716-13164 Start
		$x .= "</table></div>";
		if ($studentCounter == $numOfStudent - 1) {
			$pageBreakStyle = '';
		}else {
			$pageBreakStyle = ' style="page-break-after:always;" ';
		}
		$x .= "<div id=\"container\"  ".$pageBreakStyle.">";
		$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
		// [2015-0512-0955-58164]
		$x .= $AwardTable;
		$x .= "<tr valign='top'><td>".$RemarksTable."</td></tr>";
		//2014-0912-1716-13164 End
		
		return $x; 
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard, $intranet_root, $image_path;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
			# get school badge
			
			$SchoolLogo = "/file/reportcard2008/templates/buddhist_fat_ho_logo.jpg";
				
			# get school name
			//$SchoolName = GET_SCHOOL_NAME();
			$SchoolName = "";	
			$SchoolName .= "Buddhist Fat Ho Memorial College";
			$SchoolName .= $this->Get_Empty_Image_Div(5);
			$SchoolName .= "佛教筏可紀念中學";
			
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
	
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td align='center' valign='top'><img height='80' src='$SchoolLogo'></td>";
			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
					if(!empty($SchoolName))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$SchoolName."</td></tr>\n";
					if(!empty($ReportTitle))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle."</td></tr>\n";
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			}
			$TitleTable .= "<td width='80' align='center'>&nbsp;</td></tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal		= "XXX";
			$data['AcademicYear'] = getCurrentAcademicYear();
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				$thisUserLogin = $StudentInfoArr[0]['UserLogin']; //2014-0912-1716-13164
				$thisAdmissionDate = $this->Get_Student_Admission_Date($StudentID); //2014-0912-1716-13164
				$thisAdmissionDate = $thisAdmissionDate == "0000-00-00"?$this->EmptySymbol:$thisAdmissionDate;
				$data['Name'] 		= $lu->UserName2Lang('ch', 2);
				$data['StudentID'] 		= $thisUserLogin; //2014-0912-1716-13164
				$data['AdmissionDate'] 	= $thisAdmissionDate; //2014-0912-1716-13164
				//$data['ClassNo'] 	= $lu->ClassNumber; 
				//$data['Class'] 		= $lu->ClassName . " (".$lu->ClassNumber.")";
				//$data['StudentNo'] 	= $lu->ClassNumber;
				$data['ClassNo'] 	= $thisClassNumber; 
				$data['Class'] 		= $thisClassName . " (".$thisClassNumber.")";
				$data['StudentNo'] 	= $thisClassNumber;
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] 	= $lu->Gender;
				$data['STRN']       = str_replace("#", "", $lu->WebSamsRegNo);
				
				$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : $this->EmptySymbol;
			}
			
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
				
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if(in_array($SettingID, $SettingStudentInfo)===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
						}
						
						//$colspan = $SettingID=="Name" ? " colspan='10' " : "";
						$colspan = $SettingID=="Name" ? " colspan='2' " : "";
						
						$StudentInfoTable .= "<td class='tabletext' $colspan width='20%' valign='top' height='{$LineHeight}'>".$Title." : ";
						$StudentInfoTable .= ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
							
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						} else {
							//if($SettingID=="Name")
							if($SettingID=="StudentID")
							{
								$count=-1;
								$StudentInfoTable .= "</tr>";
							}
						}
						
						$count++;
					}
				}
				$StudentInfoTable .= "</table>";
			}
		}
		
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2) = $ColHeaderAry;
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID);
		
		# get isAllNAAry array so as to set border line for subject column first
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID);
		//$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		$SubjectColArr	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID, $isAllNAAry);
		$SubjectCol = $SubjectColArr[0];
		$borderTopSubjectID = $SubjectColArr[1];
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $borderTopSubjectID);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='report_border'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		
		# For full marks calculation (ignore the all-NA-subjects)
		$displayedMainSubjectIDArr = array();
		
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) {
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				if (!$isSub)
				{
					$displayedMainSubjectIDArr[] = $thisSubjectID;
				}
				
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				$DetailsTable .= "</tr>";
				$isFirst = 0;
				
			}
		}
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $displayedMainSubjectIDArr);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		#########################################################
		############## Marks START
		$row2 = "";
		if($ReportType=="T")	# Temrs Report Type
		{
			# Retrieve Invloved Assesment
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if (sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
			$e = 0;
			/*
			for($i=0;$i<sizeof($ColumnTitle);$i++)
			{
				# Convert to Big5 display
				# $ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$i], 1, 2));
				$ColumnTitleDisplay = $ColumnTitle[$i];
				$row1 .= "<td valign='middle' height='{$LineHeight}' class='border_left tabletext' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'><b>". $ColumnTitleDisplay . "</b></td>";
				$n++;
 				$e++;
			}
			*/
			
			# Daily
			$ColumnTitleDisplay = $eReportCard['Template']['DailyEn']."<br />".$eReportCard['Template']['DailyCh'];
			$row1 .= "<td valign='middle' height='{$LineHeight}' class='border_left_strong small_title border_bottom' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'><b>";
			$row1 .= $ColumnTitleDisplay;
			$row1 .= "</b></td>";
			$n++;
 			$e++;
 			
 			# Examination
			$ColumnTitleDisplay = $eReportCard['Template']['ExaminationEn']."<br />".$eReportCard['Template']['ExaminationCh'];
			$row1 .= "<td valign='middle' height='{$LineHeight}' class='small_title border_bottom' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'><b>";
			$row1 .= $ColumnTitleDisplay;
			$row1 .= "</b></td>";
			$n++;
 			$e++;
 			
 			$overall_border_left = "";
			
		}
		else					# Whole Year Report Type
		{
			/*
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
 			$needRowspan=0;
			for($i=0;$i<sizeof($ColumnData);$i++)
			{
				$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
				$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
				if($isDetails==1)
				{
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColumnID = array();
					$ColumnTitle = array();
					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
					for($j=0;$j<sizeof($ColumnTitle);$j++)
					{
						# convert to big5 display
						# $ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$j], 1, 2));
						$ColumnTitleDisplay = $ColumnTitle[$j];
						$row2 .= "<td class='border_left reportcard_text' align='center' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $ColumnTitleDisplay . "</td>";
						$n++;
						$e++;
					}
					$colspan = "colspan='". sizeof($ColumnTitle) ."'";
					$Rowspan = "";
					$needRowspan++;
				}
				else
				{
					$colspan = "";
					$Rowspan = "rowspan='2'";
					$e++;
				}
				$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left small_title' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $SemName ."</td>";
			}
			*/

			if($this->schoolYear == '2019') {
			    $term1RangeDisplay = 'From 1-9-2019 to 13-4-2020<br/>由 1-9-2019 至 13-4-2020';
                $term2RangeDisplay = 'From 14-4-2020 to 30-6-2020<br/>由 14-4-2020 至 30-6-2020';
                $row1 .= "<td rowspan='2' colspan='3' height='{$LineHeight}' class='border_left_strong small_title border_bottom' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%' style='font-size: 10px;'>". $term1RangeDisplay ."</td>";
                $row1 .= "<td rowspan='2' colspan='3' height='{$LineHeight}' class='border_left_strong small_title border_bottom' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%' style='font-size: 10px;'>". $term2RangeDisplay ."</td>";
            }
            else {
                $row1 .= "<td rowspan='2' colspan='3' height='{$LineHeight}' class='border_left_strong small_title border_bottom' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $eReportCard['Template']['FirstTerm'] ."</td>";
                $row1 .= "<td rowspan='2' colspan='3' height='{$LineHeight}' class='border_left_strong small_title border_bottom' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $eReportCard['Template']['SecondTerm'] ."</td>";
            }

			$dailyTitle = $eReportCard['Template']['DailyEn']."<br />".$eReportCard['Template']['DailyCh'];
			$examTitle = $eReportCard['Template']['ExaminationEn']."<br />".$eReportCard['Template']['ExaminationCh'];
            if($this->schoolYear == '2019') {
                $examTitle = $eReportCard['Template']['ELearningEn']."<br />".$eReportCard['Template']['ELearningCh'];
            }
			$totalTitle = $eReportCard['Template']['TotalEn']."<br />".$eReportCard['Template']['TotalCh'];
			
			$row2 .= "<td class='border_left_strong border_top reportcard_text border_bottom' align='center' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $dailyTitle . "</td>";
			$row2 .= "<td class='border_top reportcard_text border_bottom' align='center' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $examTitle . "</td>";
			$row2 .= "<td class='border_top reportcard_text border_bottom' align='center' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $totalTitle . "</td>";
			$row2 .= "<td class='border_left_strong border_top reportcard_text border_bottom' align='center' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $dailyTitle . "</td>";
			$row2 .= "<td class='border_top reportcard_text border_bottom' align='center' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $examTitle . "</td>";
			$row2 .= "<td class='border_top reportcard_text border_bottom' align='center' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $totalTitle . "</td>";
			
			$n = 6;
			$e = 6;
						
			$overall_border_left = "border_left_strong";
		}
		############## Marks END
		#########################################################
		$Rowspan = $row2 ? "rowspan='2'" : "";

		if(!$needRowspan)
			$row1 = str_replace("rowspan='2'", "", $row1);
		
		$x = "<tr>";
		# Subject 
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		for($i=0;$i<sizeof($SubjectColAry);$i++)
		{
			$SubjectEng = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'];
			$SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];
			$SubjectTitle = $SubjectColAry[$i];
			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
			$x .= "<td {$Rowspan}  valign='middle' class='title_big border_bottom' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>". $SubjectTitle . "</td>";
			$n++;
		}
		
		# Marks
		$x .= $row1;
		
		# Subject Overall 
		if($ShowSubjectOverall)
		{
			if($ReportType=="T")
				$thisTitle = $eReportCard['Template']['SubjectOverall'];
			else
				$thisTitle = $eReportCard['Template']['AnnualResultEn'].'<br />'.$eReportCard['Template']['AnnualResultCh'];
				
			$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Overall'] ."' class='{$overall_border_left} small_title border_bottom' align='center'>". $thisTitle ."</td>";
			$n++;
		}
		else
		{
			$e--;	
		}
		
		# Learning Attitude
		$x .= "<td {$Rowspan}  valign='middle' width='3%' nowrap class='border_left_strong small_title border_bottom' align='center' >";
		$x .= str_replace(" ", "<br />", $eReportCard['Template']['LearningAttitudeEn']);
		$x .= "<br />";
		$x .= $eReportCard['Template']['LearningAttitudeCh'];
		$x .= "</td>";
		$n++;
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		
		return array($x, $n, $e);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID, $isAllNAAry=array())
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
			
 		$x = array(); 
		$isFirst = 1;
		
		$borderTopSubjectID = array();
		
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		$thisHasCmpSubject = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID, $ParLang='', $ParDisplayType='Desc', $ReportID, $ReturnAsso=0);
		 		
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
			 		}
			 			
			 		if ($isAllNAAry[$SubSubjectID] == 1)
			 		{
				 		$x[] = "";
			 		}
			 		else
			 		{
				 		if ($isFirst)
				 		{
					 		$css_border_top = "";
			 			}
			 			else
			 			{
				 			if ($Prefix == "" && !in_array($lastSubject, $MainSubjectIDArray) || (in_array($SubSubjectID, $MainSubjectIDArray) && $thisHasCmpSubject))
					 		{
						 		$css_border_top = "border_top";
						 		$borderTopSubjectID[$SubSubjectID] = 1;
					 		}
					 		else
					 		{
						 		$css_border_top = "";
					 		}
			 			}
				 		
				 		foreach($SubjectDisplay as $k=>$v)
				 		{
					 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
			 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
			 				
			 				# Language -> Lang. for F6 only
			 				if ($ClassLevelID==13)
			 				{
				 				$SubjectEng = str_replace("Language", "Lang.", $SubjectEng);
				 				
				 				# Special Handling for F6 Chinese, separate the "中國語文及文化" and "(高級補充程度)" into 2 lines
				 				if ($SubjectID==232)
				 				{
					 				$SubjectEng = str_replace("(", "<br />(", $SubjectEng);
					 				$SubjectChn = str_replace("(", "<br />(", $SubjectChn);
				 				}
				 				
				 				# Special Handling for F6 Math & Stat, separate the "數學及統計學" and "(高級補充程度)" into 2 lines for CHINESE title only
				 				if ($SubjectID==247)
				 				{
					 				$SubjectChn = str_replace("(", "<br />(", $SubjectChn);
				 				}
			 				}
			 				
			 				### "Business, Accounting and Financial Studies" to "Business, Accounting and<br>Financial Studies"
			 				if ($SubjectID==1018)
			 				{
				 				$SubjectEng = 'Business, Accounting and<br>Financial Studies';
			 				}
			 				
			 				### "Information and Communication Technology" to "Information and Communication<br>Technology"
			 				if ($SubjectID==1016)
			 				{
				 				$SubjectEng = 'Information and Communication<br>Technology';
			 				}
			 				
			 				$v = str_replace("SubjectEng", $SubjectEng, $v);
			 				$v = str_replace("SubjectChn", $SubjectChn, $v);
			 				
				 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
							$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
							$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='tabletext' nowrap>$v</td>";
							$t .= "</tr></table>";
							$t .= "</td>";
				 		}
						$x[] = $t;
						$lastSubject = $SubSubjectID;
						
						$isFirst = 0;
			 		}
				}
		 	}
		}
 		return array($x, $borderTopSubjectID);
	}
	
	function getSignatureTable($ReportID='', $StudentID='')
	{
 		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
			$ClassLevelID = $ReportSetting['ClassLevelID'];
			$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode=true);
			$SemID = $ReportSetting['Semester'];
			$ReportType = $SemID == "F" ? "W" : "T";
		}
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' valign='bottom'>";
		$SignatureTable .= "<tr>";
		
		$width = ceil(100/sizeof($SignatureTitleArray))."%";
		$prefix = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			$IssueDate = ($SettingID == "IssueDate" && $Issued)	? "".$Issued."" : "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			$SignatureTable .= "<td valign='bottom' align='center' valign='bottom' width='$width'>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0' valign='bottom'>";
			$SignatureTable .= "<tr><td align='center' class='small_title border_bottom' height='80' valign='bottom' style='width:100%'>$prefix". $IssueDate ."$prefix</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}
		
		# Promotion Status
		# Retrieve Display Settings
		//if ($FormNumber != 7 && $FormNumber != 5)
		//if ($ReportType == "W" || $FormNumber==7 || $FormNumber==5)
		//if ($ReportType == "W" || $FormNumber==7)
		//2012-0703-0958-11066
		if ($ReportType == "W" || $FormNumber==7 || $FormNumber==6)
		{
			if($ReportID)
	 		{
	 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
				$SemID 				= $ReportSetting['Semester'];
		 		$ReportType 		= $SemID == "F" ? "W" : "T";		
		 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		 		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode=true);
		 		
		 		# retrieve Whole year CSV
		 		//2012-0229-1105-24073
				if ($FormNumber == 7 || $FormNumber==6) {
		 			$latestTerm = $SemID;
		 		}
		 		else {
		 			$latestTerm = 0;
		 		}
				
				/*
				# retrieve Student Class
				if($StudentID)
				{
					include_once($PATH_WRT_ROOT."includes/libuser.php");
					$lu = new libuser($StudentID);
					$ClassName 		= $lu->ClassName;
					$WebSamsRegNo 	= $lu->WebSamsRegNo;
				}
				
				# build data array
				$ary = array();
				$csvType = $this->getOtherInfoType();
				foreach($csvType as $k=>$Type)
				{
					$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);	
					if(!empty($csvData)) 
					{
						foreach($csvData as $RegNo=>$data)
						{
							if($RegNo == $WebSamsRegNo)
							{
			 					foreach($data as $key=>$val)
				 					$ary[$key] = $val;
							}
						}
					}
				}
				*/
				
				$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
				$ary = $OtherInfoDataAry[$StudentID][$latestTerm];
			}
			
			
			//$promotionStatus = trim($ary['Promotion Status']);
			if (is_array($ary['Promotion Status'])) {
				$promotionStatus = $ary['Promotion Status'][0];
			}
			else {
				$promotionStatus = $ary['Promotion Status'];
			}
			$promotionStatus = trim($promotionStatus);
	 		$promotionArr = explode(" ", $promotionStatus);
	 		
	 		$promotionDisplay = '';
	 		for ($i=0; $i<count($promotionArr); $i++)
	 		{
		 		$thisPromotionText = $promotionArr[$i];
		 		
		 		if ($i == (count($promotionArr)-2))
		 		{
			 		// switch to chinese
			 		$promotionDisplay .= $thisPromotionText."<br />";
		 		}
		 		else if ($i == (count($promotionArr)-1))
		 		{
			 		$promotionDisplay .= $thisPromotionText;
		 		}
		 		else
		 		{
			 		$promotionDisplay .= $thisPromotionText." ";
		 		}
	 		}
	 		
			$SignatureTable .= "<td valign='bottom' align='center' valign='bottom'>";
				$SignatureTable .= "<table cellspacing='0' cellpadding='0' valign='bottom' class='report_border' width='150'>";
					$SignatureTable .= "<tr><td align='center' class='tabletext' height='80' valign='middle'>".$promotionDisplay."</td></tr>";
				$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $FullMarksSubjectIDArr=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";	
 		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
			
 		
 		# Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
		$categorySettings = $this->LOAD_SETTING('Calculation');
		if ($ReportType == "T")
			$TermCalculationType = $categorySettings['OrderTerm'];
		else
			$TermCalculationType = $categorySettings['OrderFullYear'];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
//		$result = $this->getReportResultScore($ReportID, 0, $StudentID);
//		$GrandTotal = $StudentID ? $result['GrandTotal'] : "S";
//		$AverageMark = $StudentID ? $result['GrandAverage'] : "S";
// 		$FormPosition = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? $this->EmptySymbol: $result['OrderMeritForm']) : $this->EmptySymbol) : "#";
//  		$ClassPosition = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? $this->EmptySymbol: $result['OrderMeritClass']) : $this->EmptySymbol) : "#";
//		$NumClassStudent = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? $this->EmptySymbol: $result['ClassNoOfStudent']) : $this->EmptySymbol) : "#";
//		$NumFormStudent = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? $this->EmptySymbol: $result['FormNoOfStudent']) : $this->EmptySymbol) : "#";
		
		$result = $this->getReportResultScore($ReportID, 0, $StudentID,'',0,1);
		$GrandTotal = $StudentID ? $this->Get_Score_Display_HTML($result['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		$AverageMark = $StudentID ? $this->Get_Score_Display_HTML($result['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		//2014-0912-1716-13164
		$ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
  		$ClassPosition = (float)$result['GrandAverage']>=$this->MinGrandAvgToDisplayFormPosition ? $ClassPosition : $this->EmptySymbol;
		//2014-0912-1716-13164
  		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
  		$FormPosition = (float)$result['GrandAverage']>=$this->MinGrandAvgToDisplayFormPosition ? $FormPosition : $this->EmptySymbol;
  		
  		$NumClassStudent = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
  		$NumFormStudent = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
		
		# calculate total subjects full mark 
//		$SubjectsFullMark = $this->returnSubjectFullMark($ClassLevelID, $WithSub=0, $FullMarksSubjectIDArr,0,$ReportID);
//		$totalFullMark = 0;
//  		foreach ($SubjectsFullMark as $SubjectID => $thisFullMark)
//  		{
//	  		if (is_numeric($thisFullMark))
//	  		{
//		  		# Get Personal Weight among Component Subjects of each student
//				if($TermCalculationType == 1){
//					$OtherCondition = "SubjectID = ".$SubjectID." AND ReportColumnID IS NULL ";
//					$thisWeight = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition);
//				} else {
//					$OtherCondition = "SubjectID = ".$SubjectID." ";
//					$thisWeight = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
//				}
//				
//		  		$totalFullMark += ($thisFullMark * $thisWeight[0]['Weight']);
//	  		}
//  		}
  		$totalFullMark = $this->getGrandTotalFullMark($ReportID, $StudentID);
  		
  		# retrieve terms result
  		$termInfo = array();
  		$ColumnData = $this->returnReportTemplateColumnData($ReportID);
  		$ShowGrandTotalAndPosition = true;
		for($i=0;$i<sizeof($ColumnData);$i++)
		{
			$thisReport = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $ColumnData[$i]['SemesterNum']);
			$thisReportID = $thisReport['ReportID'];
											
//			$result = $this->getReportResultScore($thisReportID, 0, $StudentID);
			
//			$termInfo[$i]['GrandTotal'] = $StudentID ? $result['GrandTotal'] : "S";
//			$termInfo[$i]['GrandAverage'] = $StudentID ? $result['GrandAverage'] : "S";			
//	 		$termInfo[$i]['FormPosition'] = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? $this->EmptySymbol: $result['OrderMeritForm']) : $this->EmptySymbol) : "#";
//	  		$termInfo[$i]['ClassPosition'] = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? $this->EmptySymbol: $result['OrderMeritClass']) : $this->EmptySymbol) : "#";
//			$termInfo[$i]['NumClassStudent'] = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? $this->EmptySymbol: $result['ClassNoOfStudent']) : $this->EmptySymbol) : "#";
//			$termInfo[$i]['NumFormStudent'] = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? $this->EmptySymbol: $result['FormNoOfStudent']) : $this->EmptySymbol) : "#";
			
			$result = $this->getReportResultScore($thisReportID, 0, $StudentID, '', 0, 1);
			$termInfo[$i]['GrandTotal'] = $StudentID ? $this->Get_Score_Display_HTML($result['GrandTotal'], $thisReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
			$termInfo[$i]['GrandAverage'] = $StudentID ? $this->Get_Score_Display_HTML($result['GrandAverage'], $thisReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
			//2014-0912-1716-13164
			$termInfo[$i]['ClassPosition'] = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritClass'], $thisReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
			$termInfo[$i]['ClassPosition'] = (float)$result['GrandAverage']>=$this->MinGrandAvgToDisplayFormPosition ? $termInfo[$i]['ClassPosition'] : $this->EmptySymbol;
			//2014-0912-1716-13164
	  		$termInfo[$i]['FormPosition'] = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritForm'], $thisReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
 			$termInfo[$i]['FormPosition'] = (float)$result['GrandAverage']>=$this->MinGrandAvgToDisplayFormPosition ? $termInfo[$i]['FormPosition'] : $this->EmptySymbol;
 			
	  		$termInfo[$i]['NumClassStudent'] = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $thisReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
	  		$termInfo[$i]['NumFormStudent'] = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $thisReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
			
			$termInfo[$i]['GrandTotalFullMark'] = $this->getGrandTotalFullMark($thisReportID, $StudentID);
			$ShowGrandTotalAndPosition = $result['GrandTotal'] == -1?false:$ShowGrandTotalAndPosition;
		}
		//2014-0912-1716-13164
		if(!$ShowGrandTotalAndPosition){
			$FormPosition = $this->EmptySymbol;
			$ClassPosition = $this->EmptySymbol;
			$GrandTotal = $this->EmptySymbol;
			$AverageMark = $this->EmptySymbol;									
		}
		
		$first = 1;
		# Overall Result
		if($ShowGrandTotal)
		{
			$border_top = $first ? "border_top" : "";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['OverallResultEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['OverallResultCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			
			for($i=0;$i<$ColNum2;$i++)
			{
				if ($i==0 || $i==3)
				{
					$border_left = "border_left_strong";
				}
				else
				{
					$border_left = "";
				}
				
				if ($i==2)
				{
					$x .= "<td class='tabletext {$border_top} {$border_left} nowrap' align='center' height='{$LineHeight}' nowrap>". $termInfo[0]['GrandTotal']." / ".$termInfo[0]['GrandTotalFullMark']."</td>";
				}
				else if ($i==5)
				{
					$x .= "<td class='tabletext {$border_top} {$border_left} nowrap' align='center' height='{$LineHeight}' nowrap>". $termInfo[1]['GrandTotal']." / ".$termInfo[1]['GrandTotalFullMark']."</td>";
				}
				else
				{
					$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>{$this->EmptySymbol}</td>";
				}
			}
			
			if ($ReportType == "T")
			{
				$border_left = "";
			}
			else
			{
				$border_left = "border_left_strong";
			}
			$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}' nowrap>". $GrandTotal." / ".$totalFullMark."</td>";
			$x .= "<td class='tabletext {$border_top} border_left_strong' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
			$x .= "</tr>";
		}
		
		# Average Mark 
		if($ShowGrandAvg)
		{
			$border_top = $first ? "border_top" : "";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['AvgMarkEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['AvgMarkCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			for($i=0;$i<$ColNum2;$i++)
			{
				if ($i==0 || $i==3)
				{
					$border_left = "border_left_strong";
				}
				else
				{
					$border_left = "";
				}
				
				if ($i==2)
				{
					$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>". $termInfo[0]['GrandAverage'] ."</td>";
				}
				else if ($i==5)
				{
					$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>". $termInfo[1]['GrandAverage'] ."</td>";
				}
				else
				{
					$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>{$this->EmptySymbol}</td>";
				}
			}
			if ($ReportType == "T")
			{
				$border_left = "";
			}
			else
			{
				$border_left = "border_left_strong";
			}
			//$AverageMark = $this->getDisplayMarkdp("GrandAverage", $AverageMark);
			$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>". $AverageMark ."</td>";
			$x .= "<td class='tabletext {$border_top} border_left_strong' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
			$x .= "</tr>";
		}
		
		# Position in Class 
		if($ShowOverallPositionClass)
		{
			$border_top = $first ? "border_top" : "";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ClassPositionEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ClassPositionCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			for($i=0;$i<$ColNum2;$i++)
			{
				if ($i==0 || $i==3)
				{
					$border_left = "border_left_strong";
				}
				else
				{
					$border_left = "";
				}
				
				if ($i==2)
				{
					//$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>". $termInfo[0]['ClassPosition']." / ".$termInfo[0]['NumClassStudent'] ."</td>";
					$thisDisplay = $this->Get_Ranking_Display($termInfo[0]['ClassPosition'], $termInfo[0]['NumClassStudent'], $ShowOverallPositionClass, $ShowNumOfStudentClass);
					$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>". $thisDisplay ."</td>";
				}
				else if ($i==5)
				{
					//$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>". $termInfo[1]['ClassPosition'] ." / ".$termInfo[1]['NumClassStudent']."</td>";
					$thisDisplay = $this->Get_Ranking_Display($termInfo[1]['ClassPosition'], $termInfo[1]['NumClassStudent'], $ShowOverallPositionClass, $ShowNumOfStudentClass);
					$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>". $thisDisplay ."</td>";
				}
				else
				{
					$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>{$this->EmptySymbol}</td>";
				}
			}
			if ($ReportType == "T")
			{
				$border_left = "";
			}
			else
			{
				$border_left = "border_left_strong";
			}
			
			//$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>". $ClassPosition." / ".$NumClassStudent ."</td>";
			$thisDisplay = $this->Get_Ranking_Display($ClassPosition, $NumClassStudent, $ShowOverallPositionClass, $ShowNumOfStudentClass);
			$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>". $thisDisplay ."</td>";
			
			$x .= "<td class='tabletext {$border_top} border_left_strong' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
			$x .= "</tr>";
		}
		
		# Position in Form 
		if($ShowOverallPositionForm)
		{
			$border_top = $first ? "border_top" : "";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['FormPositionEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['FormPositionCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			for($i=0;$i<$ColNum2;$i++)
			{
				if ($i==0 || $i==3)
				{
					$border_left = "border_left_strong";
				}
				else
				{
					$border_left = "";
				}
				
				if ($i==2)
				{
					//$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>". $termInfo[0]['FormPosition']." / ".$termInfo[0]['NumFormStudent'] ."</td>";
					$thisDisplay = $this->Get_Ranking_Display($termInfo[0]['FormPosition'], $termInfo[0]['NumFormStudent'], $ShowOverallPositionForm, $ShowNumOfStudentForm);
					$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>". $thisDisplay ."</td>";
				}
				else if ($i==5)
				{
					//$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>". $termInfo[1]['FormPosition'] ." / ".$termInfo[1]['NumFormStudent']."</td>";
					$thisDisplay = $this->Get_Ranking_Display($termInfo[1]['FormPosition'], $termInfo[1]['NumFormStudent'], $ShowOverallPositionForm, $ShowNumOfStudentForm);
					$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>". $thisDisplay ."</td>";
				}
				else
				{
					$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>{$this->EmptySymbol}</td>";
				}
			}
			if ($ReportType == "T")
			{
				$border_left = "";
			}
			else
			{
				$border_left = "border_left_strong";
			}
			
			//$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>". $FormPosition ." / ".$NumFormStudent."</td>";
			$thisDisplay = $this->Get_Ranking_Display($FormPosition, $NumFormStudent, $ShowOverallPositionForm, $ShowNumOfStudentForm);
			$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>". $thisDisplay."</td>";
			
			$x .= "<td class='tabletext {$border_top} border_left_strong' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
			$x .= "</tr>";
		}
		
		##################################################################################
		# CSV related
		##################################################################################
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		
		##################################################################################
		# HARDCODE FOR $this->schoolYear = "2007";
		# Due to incorrect usage, the whole year report is created as term report
		# so that the part of CSV data need hard-code checking
		# Form 1, 2, 3, 4, 6 => 4 colums with First Term and Second Term
		# Form 5, 7 => 2 columns with Second Term only
		##################################################################################
		
		##########
		##########
		// temp skip 2007 in dev site
		##########
		##########
		if($this->schoolYear == "2007")
		//if (false)
		{
			
			# Form 1, 2, 3, 4, 6 
			if($ClassLevelID==8 || $ClassLevelID==9 || $ClassLevelID==10 || $ClassLevelID==11 || $ClassLevelID==13)
			{
				$semType = 1;
				for($i=0;$i<=1;$i++)
				{
					$InfoTermID = $i+1;
					if(!empty($csvType))
					{
						foreach($csvType as $k=>$Type)
						{
							$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
							if(!empty($csvData)) 
							{
								foreach($csvData as $RegNo=>$data)
								{
									//if($RegNo == $WebSamsRegNo)
									if($RegNo == $StudentID)
									{
					 					foreach($data as $key=>$val)
						 					$ary[$i][$key] = $val;
									}
								}
							}
						}
					}
				}
			}
			else	# Form 5, 7
			{
				$semType = 2;
				if(!empty($csvType))
				{
					foreach($csvType as $k=>$Type)
					{
						if($ClassLevelID==12)	# Form 5 (second term)
							$csvData = $this->getOtherInfoData($Type, 2, $ClassName);	
						else					# Form 7 (first term)
							$csvData = $this->getOtherInfoData($Type, 1, $ClassName);	
						if(!empty($csvData)) 
						{
							foreach($csvData as $RegNo=>$data)
							{
								if($RegNo == $StudentID)
								{
				 					foreach($data as $key=>$val)
					 					$ary[0][$key] = $val;
								}
							}
						}
					}
				}
			}
				
			# Days Absent 
			$DaysAbsent0 = $StudentID ? ($ary[0]['Days Absent'] ? $ary[0]['Days Absent'] : $this->EmptySymbol) : "#";
			$DaysAbsent1 = $StudentID ? ($ary[1]['Days Absent'] ? $ary[1]['Days Absent'] : $this->EmptySymbol) : "#";
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['DaysAbsentEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['DaysAbsentCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>--</td>";
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $DaysAbsent0 ."</td>";
			if($semType==1)
			{
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>--</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $DaysAbsent1 ."</td>";
			}
			if($ShowSubjectOverall)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>--</td>";
			$x .= "</tr>";
			
			# Times Late 
			$TimesLate0 = $StudentID ? ($ary[0]['Time Late'] ? $ary[0]['Time Late'] : $this->EmptySymbol) : "#";
			$TimesLate1 = $StudentID ? ($ary[1]['Time Late'] ? $ary[1]['Time Late'] : $this->EmptySymbol) : "#";
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['TimesLateEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['TimesLateCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>--</td>";
			$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $TimesLate0 ."</td>";
			if($semType==1)
			{
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>--</td>";
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $TimesLate1 ."</td>";
			}
			if($ShowSubjectOverall)
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>--</td>";
			$x .= "</tr>";

			# Conduct 
			$Conduct0 = $StudentID ? ($ary[0]['Conduct'] ? $ary[0]['Conduct'] : $this->EmptySymbol) : "G";
			$Conduct1 = $StudentID ? ($ary[1]['Conduct'] ? $ary[1]['Conduct'] : $this->EmptySymbol) : "G";
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ConductEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ConductCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>--</td>";
			$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $Conduct0 ."</td>";
			if($semType==1)
			{
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>--</td>";
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $Conduct1 ."</td>";
			}
			if($ShowSubjectOverall)
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>--</td>";
			$x .= "</tr>";
		}
		else
		{
			/*
			if($SemID=="F")
			{
				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
				foreach($ColumnData as $k1=>$d1)
				{
					$TermID = $d1['SemesterNum'];
					$InfoTermID = $TermID+1;
					
					if(!empty($csvType))
					{
						foreach($csvType as $k=>$Type)
						{
							$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
							if(!empty($csvData)) 
							{
								foreach($csvData as $RegNo=>$data)
								{
									if($RegNo == $WebSamsRegNo)
									{
					 					foreach($data as $key=>$val)
						 					$ary[$TermID][$key] = $val;
									}
								}
							}
						}
					}
				}
				
				# calculate sems/assesment col#
				$ColNum2Ary = array();
				foreach($ColumnData as $k1=>$d1)
				{
					$TermID = $d1['SemesterNum'];
					if($d1['IsDetails']==1)
					{
						# check sems/assesment col#
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
			 			$thisReportID = $thisReport['ReportID'];
						$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
						//$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1
						$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry);
					}
					else
						$ColNum2Ary[$TermID] = 0;
				}
			}
			else
			{
				$InfoTermID = $SemID+1;
				if(!empty($csvType))
				{
					foreach($csvType as $k=>$Type)
					{
						$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
						if(!empty($csvData)) 
						{
							foreach($csvData as $RegNo=>$data)
							{
								if($RegNo == $WebSamsRegNo)
								{
				 					foreach($data as $key=>$val)
					 					$ary[$SemID][$key] = $val;
								}
							}
						}
					}
				}
			}
			*/
			
			if($SemID=="F")
			{
				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
				
				# calculate sems/assesment col#
				$ColNum2Ary = array();
				foreach($ColumnData as $k1=>$d1)
				{
					$TermID = $d1['SemesterNum'];
					if($d1['IsDetails']==1)
					{
						# check sems/assesment col#
						$thisReport = $this->returnReportTemplateBasicInfo("","", $ClassLevelID, $TermID);
			 			$thisReportID = $thisReport['ReportID'];
						$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
						//$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1
						$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry);
					}
					else
						$ColNum2Ary[$TermID] = 0;
				}
			}
			
			$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID,$StudentID);
			$ary = $OtherInfoDataAry[$StudentID];
			
			$border_top = $first ? "border_top" : "";
			
			# Days Absent 
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['DaysAbsentEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['DaysAbsentCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			if($ReportType=="W")	# Whole Year Report
			{
				foreach($ColumnData as $k1=>$d1)
				{
					$TermID = $d1['SemesterNum'];
					$DaysAbsent = $StudentID ? ($ary[$TermID]['Days Absent'] ? $ary[$TermID]['Days Absent'] : $this->EmptySymbol) : "#";
					
					// handle multi-input [2015-0204-1124-04066]
					if(is_array($DaysAbsent)){
						$tempDaysAbsent = "";
						for($i = 0; $i < count($DaysAbsent); $i++){
							$tempDaysAbsent = $DaysAbsent[$i];
							if($tempDaysAbsent)
								break;
						}
						if($tempDaysAbsent)
							$DaysAbsent = $tempDaysAbsent;
						else
							$DaysAbsent = $this->EmptySymbol;
					}
					
					for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					{
						if ($i==0 || $i==3)
						{
							$border_left = "border_left_strong";
						}
						else
						{
							$border_left = "";
						}
						$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>--</td>";
					}
					$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>". $DaysAbsent ."</td>";
				}
				
				if($ShowSubjectOverall)
					$x .= "<td class='tabletext {$border_top} border_left_strong' align='center' height='{$LineHeight}'>--</td>";
					
				$x .= "<td class='tabletext {$border_top} border_left_strong' align='center' height='{$LineHeight}'>{$this->EmptySymbol}</td>";
			}
			else				# Term Report
			{
				$DaysAbsent = $StudentID ? ($ary[$SemID]['Days Absent'] ? $ary[$SemID]['Days Absent'] : $this->EmptySymbol) : "#";
					
				// handle multi-input [2015-0204-1124-04066]
				if(is_array($DaysAbsent)){
					$tempDaysAbsent = "";
					for($i = 0; $i < count($DaysAbsent); $i++){
						$tempDaysAbsent = $DaysAbsent[$i];
						if($tempDaysAbsent)
							break;
					}
					if($tempDaysAbsent)
						$DaysAbsent = $tempDaysAbsent;
					else 
						$DaysAbsent = $this->EmptySymbol;
				}
				
				for($i=0;$i<$ColNum2;$i++)
				{
					if ($i==0)
					{
						$border_left = "border_left_strong";
					}
					else
					{
						$border_left = "";
					}
					$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>{$this->EmptySymbol}</td>";
				}
				$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>". $DaysAbsent ."</td>";
				$x .= "<td class='tabletext {$border_top} border_left_strong' align='center' height='{$LineHeight}'>{$this->EmptySymbol}</td>";
			}
			$x .= "</tr>";
			
			# Times Late 
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['TimesLateEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['TimesLateCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			if($ReportType=="W")	# Whole Year Report
			{
				foreach($ColumnData as $k1=>$d1)
				{
					$TermID = $d1['SemesterNum'];
					$TimesLate = $StudentID ? ($ary[$TermID]['Time Late'] ? $ary[$TermID]['Time Late'] : $this->EmptySymbol) : "#";
					
					// handle multi-input [2015-0204-1124-04066]
					if(is_array($TimesLate)){
						$tempTimesLate = "";
						for($i = 0; $i < count($TimesLate); $i++){
							$tempTimesLate = $TimesLate[$i];
							if($tempTimesLate)
								break;
						}
						if($tempTimesLate)
							$TimesLate = $tempTimesLate;
						else 
							$TimesLate = $this->EmptySymbol;
					}
					
					for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					{
						if ($i==0 || $i==3)
						{
							$border_left = "border_left_strong";
						}
						else
						{
							$border_left = "";
						}
						$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>--</td>";
					}
					$x .= "<td class='tabletext {$border_left}' align='center' height='{$LineHeight}'>". $TimesLate ."</td>";
				}
				
				if($ShowSubjectOverall)
					$x .= "<td class='tabletext border_left_strong' align='center' height='{$LineHeight}'>--</td>";
					
				$x .= "<td class='tabletext {$border_top} border_left_strong' align='center' height='{$LineHeight}'>{$this->EmptySymbol}</td>";
			}
			else				# Term Report
			{
				$TimesLate = $StudentID ? ($ary[$SemID]['Time Late'] ? $ary[$SemID]['Time Late'] : $this->EmptySymbol) : "#";
				
				// handle multi-input [2015-0204-1124-04066]
				if(is_array($TimesLate)){
					$tempTimesLate = "";
					for($i = 0; $i < count($TimesLate); $i++){
						$tempTimesLate = $TimesLate[$i];
						if($tempTimesLate)
							break;
					}
					if($tempTimesLate)
						$TimesLate = $tempTimesLate;
					else 
						$TimesLate = $this->EmptySymbol;
				}
				
				for($i=0;$i<$ColNum2;$i++)
				{
					if ($i==0)
					{
						$border_left = "border_left_strong";
					}
					else
					{
						$border_left = "";
					}
					$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>{$this->EmptySymbol}</td>";
				}
				$x .= "<td class='tabletext {$border_left}' align='center' height='{$LineHeight}'>". $TimesLate ."</td>";
				$x .= "<td class='tabletext {$border_top} border_left_strong' align='center' height='{$LineHeight}'>{$this->EmptySymbol}</td>";
			}		
			$x .= "</tr>";
			
			# Time(s) No Submission of Homework
			// 2011-0217-1554-29073: hide
			// 2013-0107-0951-11156: show back
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['TimesNoSubmissionOfHomeworkEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['TimesNoSubmissionOfHomeworkCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			if($ReportType=="W")	# Whole Year Report
			{
				foreach($ColumnData as $k1=>$d1)
				{
					$TermID = $d1['SemesterNum'];
					$TimesNoHomework = $StudentID ? ($ary[$TermID]['Times No Submission of Homework'] ? $ary[$TermID]['Times No Submission of Homework'] : $this->EmptySymbol) : "#";
					for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					{
						if ($i==0 || $i==3)
						{
							$border_left = "border_left_strong";
						}
						else
						{
							$border_left = "";
						}
						$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>--</td>";
					}
					$x .= "<td class='tabletext {$border_left}' align='center' height='{$LineHeight}'>". $TimesNoHomework ."</td>";
				}
				
				if($ShowSubjectOverall)
					$x .= "<td class='tabletext border_left_strong' align='center' height='{$LineHeight}'>--</td>";
					
				$x .= "<td class='tabletext {$border_top} border_left_strong' align='center' height='{$LineHeight}'>{$this->EmptySymbol}</td>";
			}
			else				# Term Report
			{
				$TimesNoHomework = $StudentID ? ($ary[$SemID]['Times No Submission of Homework'] ? $ary[$SemID]['Times No Submission of Homework'] : $this->EmptySymbol) : "#";
				for($i=0;$i<$ColNum2;$i++)
				{
					if ($i==0)
					{
						$border_left = "border_left_strong";
					}
					else
					{
						$border_left = "";
					}
					$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>{$this->EmptySymbol}</td>";
				}
				$x .= "<td class='tabletext {$border_left}' align='center' height='{$LineHeight}'>". $TimesNoHomework ."</td>";
				$x .= "<td class='tabletext {$border_top} border_left_strong' align='center' height='{$LineHeight}'>{$this->EmptySymbol}</td>";
			}		
			$x .= "</tr>";
			
			# Conduct 
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ConductEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ConductCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			if($ReportType=="W")	# Whole Year Report
			{
				foreach($ColumnData as $k1=>$d1)
				{
					$TermID = $d1['SemesterNum'];
					$Conduct = $StudentID ? ($ary[$TermID]['Conduct'] ? $ary[$TermID]['Conduct'] : $this->EmptySymbol) : "G";
					for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					{
						if ($i==0 || $i==3)
						{
							$border_left = "border_left_strong";
						}
						else
						{
							$border_left = "";
						}
						$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>--</td>";
					}
					$x .= "<td class='tabletext {$border_left}' align='center' height='{$LineHeight}'>". $Conduct ."</td>";
				}
				
				if($ShowSubjectOverall)
					$x .= "<td class='tabletext border_left_strong' align='center' height='{$LineHeight}'>--</td>";
					
				$x .= "<td class='tabletext {$border_top} border_left_strong' align='center' height='{$LineHeight}'>{$this->EmptySymbol}</td>";
			}
			else				# Term Report
			{
				$Conduct = $StudentID ? ($ary[$SemID]['Conduct'] ? $ary[$SemID]['Conduct'] : $this->EmptySymbol) : "G";
				for($i=0;$i<$ColNum2;$i++)
				{
					if ($i==0)
					{
						$border_left = "border_left_strong";
					}
					else
					{
						$border_left = "";
					}
					$x .= "<td class='tabletext {$border_top} {$border_left}' align='center' height='{$LineHeight}'>{$this->EmptySymbol}</td>";
				}
				$x .= "<td class='tabletext {$border_left}' align='center' height='{$LineHeight}'>". $Conduct ."</td>";
				$x .= "<td class='tabletext {$border_top} border_left_strong' align='center' height='{$LineHeight}'>{$this->EmptySymbol}</td>";
			}		
			$x .= "</tr>";
		}
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $BorderTopSubjectID=array())
	{
		global $eReportCard, $ReportCardCustomSchoolName, $PATH_WRT_ROOT, $eRCTemplateSetting;
		
		$HideRowMarkArr = array('', '--', '*');
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
				
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation", "UseWeightedMark");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
		
		$SubjectIdCodeMappingAry = $this->GET_SUBJECTS_CODEID_MAP();
		
		$newStudentArr = $eRCTemplateSetting['SpecialDisplayForNewStudent'][$this->schoolYear];
		if (!is_array($newStudentArr))
			$newStudentArr = array();
			
		$ReportMarksAry[$ReportID] = $MarksAry;
		
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		if($ReportType=="T")	# Temrs Report Type
		{
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
							
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				$thisHasCmpSubject = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID, $ParLang='', $ParDisplayType='Desc', $ReportID, $ReturnAsso=0);
				
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				# define css
				if ($BorderTopSubjectID[$SubjectID] == 1)
				{
					$css_border_top = "border_top";
				}
				else
				{
					$css_border_top = "";
				}
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
				
				$isAllNA = true;
					
				# Assessment Marks & Display
				for($i=0;$i<sizeof($ColumnID);$i++)
				{
					if ($i==0)
					{
						$css_border_left = "border_left_strong";
					}
					else
					{
						$css_border_left = "";
					}
					$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID=".$ColumnID[$i] ." and SubjectID=$SubjectID");
					$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
					
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnID[$i]][Mark] : "";
					$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnID[$i]][Grade] : ""; 
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
					
					if($needStyle && $thisMark != $this->EmptySymbol)
					{
						$thisMarkTemp = $UseWeightedMark ? $thisMark/$thisSubjectWeight : $thisMark;
						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp,'','',$ReportID);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
					{
						$thisMarkDisplay = $thisMark;
					}
					
					if ($thisMark == "")
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					
					//if (substr($thisMark, 0, 4) != "N.A.")
					if (!in_array($thisMark, (array)$HideRowMarkArr))
					{
						$isAllNA = false;
					}
						
					$x[$SubjectID] .= "<td class='{$css_border_left} {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					
  					if($ShowSubjectFullMark)
  					{
	  					# check special full mark
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
	  					
						$FullMark = $ScaleDisplay=="M" ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
					}
					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
					
					$lastSubjectID = $SubjectID;
				}
				
				# Subject Overall 
				if($ShowSubjectOverall)
				{
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0][Grade] : ""; 
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					
					if (is_numeric($thisMark))
					{
						$thisMark = $this->getDisplayMarkdp("SubjectTotal", $thisMark);
					}
					
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
 						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$ReportID);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
						
					if ($thisMark == "")
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
						
					//if (substr($thisMark, 0, 4) != "N.A.")
					if (!in_array($thisMark, (array)$HideRowMarkArr))
					{
						$isAllNA = false;
					}
						
					$x[$SubjectID] .= "<td class='{$css_border_left} {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					if($ShowSubjectFullMark)
  					{
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $thisFullMark .")</td>";
					}
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}
				
				# Learning Attitude
				$temp = $this->GET_EXTRA_SUBJECT_INFO(array($StudentID), $SubjectID, $ReportID);
				$attitude = $temp[$StudentID]['Info'];
				$attitude = $attitude ? $attitude : $this->EmptySymbol;
				
				if (trim($attitude) >= "F")
				{
					$attitude = "<u>F</u>";
				}
				
				$x[$SubjectID] .= "<td class='border_left_strong {$css_border_top}'>";
				$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
					$x[$SubjectID] .= "<td align='center' class='tabletext' width='100%'>". $attitude ."</td>";
					
				$x[$SubjectID] .= "</tr></table>";
				$x[$SubjectID] .= "</td>";
				
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			$isFirst = 1;
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SubjectSchemeInfoArr = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$SchemeType = $SubjectSchemeInfoArr['SchemeType'];
				
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				$thisHasCmpSubject = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID, $ParLang='', $ParDisplayType='Desc', $ReportID, $ReturnAsso=0);

				# define css
				if ($BorderTopSubjectID[$SubjectID] == 1)
				{
					$css_border_top = "border_top";
				}
				else
				{
					$css_border_top = "";
				}
				
				$isAllNA = true;
				
				# Terms's Assesment / Terms Result
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					//if($isDetails==1)		# Retrieve assesments' marks
					//{
						
					# retrieve both assessments and overall score for this school
					$thisReport = $this->returnReportTemplateBasicInfo("","".$ClassLevelID, $ClassLevelID, $ColumnData[$i]['SemesterNum']);
					$thisReportID = $thisReport['ReportID'];
					
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColumnID = array();
					$ColumnTitle = array();
					foreach ((array)$ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
					
					if (!isset($ReportMarksAry[$thisReportID]))
						$ReportMarksAry[$thisReportID] = $this->getMarks($thisReportID, $StudentID);		
					
					for($j=0;$j<sizeof($ColumnID);$j++)
					{
						if ($j==0)
						{
							$css_border_left = "border_left_strong";
						}
						else
						{
							$css_border_left = "";
						}
					
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID=".$ColumnID[$j] ." and SubjectID=$SubjectID");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisMark = $ScaleDisplay=="M" ? $ReportMarksAry[$thisReportID][$SubjectID][$ColumnID[$j]][Mark] : "";
						$thisGrade = $ScaleDisplay=="G" ? $ReportMarksAry[$thisReportID][$SubjectID][$ColumnID[$j]][Grade] : ""; 
						
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						$ShowOverallMark = !($this->Check_If_Grade_Is_SpecialCase($thisMark));//2014-0912-1716-13164
						/*
						if (($thisMark != "N.A." && $thisGrade=="N.A.") || ($thisMark!="0" && $thisGrade!="") || ($thisMark!=""))
						{
							$isAllNA = false;
						}
						*/
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $ReportMarksAry[$thisReportID][$SubjectID][$ColumnID[$j]]['Grade']);
						if($needStyle)
						{
							$thisMarkTemp = $UseWeightedMark ? $thisMark/$thisSubjectWeight : $thisMark;
							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp,'','',$thisReportID);
							$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
						
						if ($thisMark == "")
						{
							$thisMarkDisplay = $this->EmptySymbol;
						}
						
						//if (substr($thisMark, 0, 4) != "N.A." && $thisMark != '')
						if (!in_array($thisMark, (array)$HideRowMarkArr))
						{
							$isAllNA = false;
						}
						
						if ($i==0 && in_array($StudentID, $newStudentArr))
							$thisMarkDisplay = $this->EmptySymbol;
							
						$x[$SubjectID] .= "<td class='{$css_border_left} {$css_border_top}'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
	  					
	  					if($ShowSubjectFullMark)
	  					{
		  					$FullMark = $ScaleDisplay=="M" ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
							$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
						}
						
						$x[$SubjectID] .= "</tr></table>";
						$x[$SubjectID] .= "</td>";
					}
						
						
					//}
					//else					# Retrieve Terms Overall marks
					//{
						/*
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
						
					$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID=".$ColumnData[$i]['ReportColumnID'] ." and SubjectID=$SubjectID");
					$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];

					$MarksAry = $this->getMarks($thisReportID, $StudentID);		
					*/
					
					# Retrieve Terms Overall marks
					$thisMark = $ScaleDisplay=="M" ? $ReportMarksAry[$thisReportID][$SubjectID][0][Mark] : "";
					$thisGrade = $ScaleDisplay=="G" ? $ReportMarksAry[$thisReportID][$SubjectID][0][Grade] : ""; 
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					/*
					if ($thisMark != "N.A." && $thisGrade=="N.A.")
					{
						$isAllNA = false;
					}
					else if ($thisMark!="0" && $thisGrade!="")
					{
						$isAllNA = false;
					}
					*/
					
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $ReportMarksAry[$thisReportID][$SubjectID][0]['Grade']);
					if($needStyle)
					{
						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$thisReportID);
							//$thisMark = ($ScaleDisplay=="M" && $StudentID) ? my_round($thisMark*$thisSubjectWeight,2) : $thisMark;
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
					{
						$thisMarkDisplay = $thisMark;
					}
					
					if ($thisMark == "")
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					
					//if (substr($thisMark, 0, 4) != "N.A." && $thisMark != '')
					if (!in_array($thisMark, (array)$HideRowMarkArr))
					{
						$isAllNA = false;
					}
					
					if ($i==0 && in_array($StudentID, $newStudentArr))
						$thisMarkDisplay = $this->EmptySymbol;
					
					$x[$SubjectID] .= "<td class='{$css_border_left} {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					
  					if($ShowSubjectFullMark)
  					{
							$FullMark = $ScaleDisplay=="M" ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID];
						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
					}
					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
					//}
					
					$lastReportID = $thisReportID;
				}
				
				# Subject Overall 
				if($ShowSubjectOverall)
				{
					//$MarksAry = $this->getMarks($ReportID, $StudentID);
					$MarksAry = $ReportMarksAry[$ReportID];
					
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisGrade = $ScaleDisplay=="G" || $SchemeType=="PF" ? $MarksAry[$SubjectID][0][Grade] : ""; 
					
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					
					if (is_numeric($thisMark))
					{
						$thisMark = $this->getDisplayMarkdp("SubjectTotal", $thisMark);
					}
					
					/*
					if ($thisMark != "N.A." && $thisGrade=="N.A.")
					{
						$isAllNA = false;
					}
					else if ($thisMark!="0" && $thisGrade!="")
					{
						$isAllNA = false;
					}
					*/
					
					//if ($SchemeType == 'PF' && substr($thisMark, 0, 4) != "N.A." && $thisMark != '')
					if ($SchemeType == 'PF' && !in_array($thisMark, (array)$HideRowMarkArr))
					{
						$isAllNA = false;
					}
					
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$ReportID);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
						
					if (!$ShowOverallMark){//2014-0912-1716-13164
						$thisMarkDisplay = $this->EmptySymbol;
					}else if ($SchemeType == 'PF') {
						$SchemeMainInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
						$thisMarkDisplay = ($thisGrade=='P')? $this->ReturnTextwithStyle($SchemeMainInfo['Pass'], 'HighLight', 'Pass') : $this->ReturnTextwithStyle($SchemeMainInfo['Fail'], 'HighLight', 'Fail');
					}
					else if ($thisMark == "") {
						$thisMarkDisplay = $this->EmptySymbol;
					}
					
					//2015-0707-1344-02206: disable the customization
//					if ($SubjectIdCodeMappingAry[$SubjectID]=='55') {
//						// 2012-0628-1025-36066
//						// show "--" for Buddhist Studies 佛學 in the annual column 
//						$thisMarkDisplay = $this->EmptySymbol;
//					}
						
					$x[$SubjectID] .= "<td class='border_left_strong {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					if($ShowSubjectFullMark)
  					{
						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $SubjectFullMarkAry[$SubjectID] .")</td>";
					}
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}
				
				# Learning Attitude (Retrieve from Second Term Report)
				if ($ScaleInput=='G' && $ScaleDisplay=='G')
					$targetReportID = $lastReportID;
				else
					$targetReportID = $ReportID;
					
				$temp = $this->GET_EXTRA_SUBJECT_INFO(array($StudentID), $SubjectID, $targetReportID);
				$attitude = $temp[$StudentID]['Info'];
				
				if (trim($attitude) >= "F")
				{
					$attitude = "<u>F</u>";
				}
				
				$attitude = $attitude ? $attitude : $this->EmptySymbol;
				$x[$SubjectID] .= "<td class='border_left_strong {$css_border_top}'>";
				$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
					$x[$SubjectID] .= "<td align='center' class='tabletext' width='100%'>". $attitude ."</td>";
					
				$x[$SubjectID] .= "</tr></table>";
				$x[$SubjectID] .= "</td>";
				
				$isFirst = 0;
				$lastSubjectID = $SubjectID;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		}	# End Whole Year Report Type
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$AllowClassTeacherComment = $ReportSetting['AllowClassTeacherComment'];
		$LineHeight = $ReportSetting['LineHeight'];
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";	
		
		if ($ReportType == "T")
		{
			# retrieve the latest Term
			$latestTerm = "";
			$sems = $this->returnReportInvolvedSem($ReportID);
			foreach($sems as $TermID=>$TermName)
				$latestTerm = $TermID;
			//$latestTerm++;
		}
		else
		{
			# retrieve Whole year CSV
			$latestTerm = 0;
		}
		
		# retrieve Student Class
		/*
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		foreach($csvType as $k=>$Type)
		{
			$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);	
			if(!empty($csvData)) 
			{
				foreach($csvData as $RegNo=>$data)
				{
					if($RegNo == $WebSamsRegNo)
					{
	 					foreach($data as $key=>$val)
		 					$ary[$key] = $val;
					}
				}
			}
		}
		*/
		
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID); 
		$ary = $OtherInfoDataAry[$StudentID][$latestTerm];
		
		$meritsArr = array("Merits","Minor Credit","Major Credit","Super Credit","Ultra Credit","Demerits","Minor Fault","Major Fault","Super Fault","Ultra Fault","Black Marks");
		# retrieve result data
		$merit = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		
		$counter = 0;
		for ($i=0; $i<sizeof($meritsArr); $i++)
		{
			$thisMeritName = $meritsArr[$i];
			
			# need not show zero merit
			if ($ary[$thisMeritName] == 0)
				continue;
				
			# display 3 data per row
			if (($counter % 3)==0)
			{
				# start of new row
				$merit .= "<tr>";
			}
			
			$merit .= "<td class='tabletext'>".$eReportCard['Template'][$thisMeritName] . ": " . $ary[$thisMeritName]."</td>";
			
			if (($counter % 3)==2)
			{
				# end of the row
				$merit .= "</tr>";
			}
			
			$counter++;
				
		}
		
		$merit .= "</table>";
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$classteachercomment = $CommentAry[0];
		$remark = $ary['Remark'];
		$eca = $ary['ECA'];
		
		//$x .= "<br>";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='60' class='report_border'>";
		$x .= "<tr>";
		$x .= "<td width='50%' valign='top'>";
			# Merits & Demerits 
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr><td class='MISC_title'><b>". $eReportCard['Template']['MeritsDemerits']."</b></td></tr>";
			$x .= "<tr><td class='tabletext'>".$merit."</td></tr>";
			$x .= "</table>";	
			$x .= "</td></tr></table>";	
		$x .="</td>";
		
		//$classteachercomment = stripslashes($classteachercomment);
		$commentArr = explode("\n", $classteachercomment);
		$numOfComment = count($commentArr);
		
		$commentDisplay = '';
		for ($i=0; $i<$numOfComment; $i++)
		{
			$thisComment = $commentArr[$i];
			
			if ($thisComment != '')
			{
				//$commentDisplay .= $thisComment."。<br />";
				$commentDisplay .= $thisComment."<br />";
			}
		}
		$x .= "<td class='border_left' width='50%' valign='top'>";
			# Class Teacher Comment 
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr><td class='MISC_title'><b>". $eReportCard['Template']['ClassTeacherComment']."</b></td></tr>";
			$x .= "<tr><td class='tabletext'>".$commentDisplay."</td></tr>";
			$x .= "</table>";	
			$x .= "</td></tr></table>";	
		$x .="</td>";
		$x .= "</tr>";
		$x .= "</table>";
		
		/*
		$x .= "<td width='50%' class=\"border_left\" valign='top'>";
			# Remark 
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr>";
				$x .= "<td class='tabletext'><b>". $eReportCard['Template']['Remark']."</b><br>".$remark."</td>";
			$x .= "</tr>";
			$x .= "</table>";	
			$x .= "</td></tr></table>";	
		$x .="</td>";
		$x .= "</tr>";
		$x .= "</table>";
		
		$x .= "<br>";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='60' class='report_border'>";
		$x .= "<tr>";
		
		if($AllowClassTeacherComment)
		{
			$x .= "<td width='50%' valign='top'>";
				# Class Teacher Comment 
				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
				$x .= "<tr>";
					$x .= "<td class='tabletext'><b>". $eReportCard['Template']['ClassTeacherComment']."</b><br>".$classteachercomment."</td>";
				$x .= "</tr>";
				$x .= "</table>";	
				$x .= "</td></tr></table>";	
			$x .="</td>";
		}
		$x .= "<td width='50%' class=\"border_left\" valign='top'>";
			# ECA 
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr>";
				$x .= "<td class='tabletext'><b>". $eReportCard['Template']['eca']."</b><br>".$eca."</td>";
			$x .= "</tr>";
			$x .= "</table>";	
			$x .= "</td></tr></table>";	
		$x .="</td>";
		$x .= "</tr>";
		$x .= "</table>";
		*/
		
		return $x;
	}
	
	function getRemarksTable($ReportID, $StudentID)
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
 		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode=true);
 		
 		$TwoColumnMode = ($ReportType == "W" && ($FormNumber==1 || $FormNumber==2 || $FormNumber==3))? true : false;

 		# get CSV data
 		if ($ReportType == "T")
		{
			# retrieve the latest Term
			$latestTerm = "";
			$sems = $this->returnReportInvolvedSem($ReportID);
			foreach($sems as $TermID=>$TermName)
				$latestTerm = $TermID;
			//$latestTerm++;
		}
		else
		{
			# retrieve Whole year CSV
			$latestTerm = 0;
		}
		
		/*
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		foreach($csvType as $k=>$Type)
		{
			$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);	
			if(!empty($csvData)) 
			{
				foreach($csvData as $RegNo=>$data)
				{
					if($RegNo == $WebSamsRegNo)
					{
	 					foreach($data as $key=>$val)
		 					$ary[$key] = $val;
					}
				}
			}
		}
 		*/
 		
 		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID); 
 		$ary = $OtherInfoDataAry[$StudentID][$latestTerm];
 		
 		
 		# Construct Subject Weight Array (For manual adjustment of Subject Weight in Remarks)
 		$SubjectArr = (is_array($ary["Subject"])==true)? $ary["Subject"] : array($ary["Subject"]);
 		$Percentage1Arr = (is_array($ary["Percentage1"])==true)? $ary["Percentage1"] : array($ary["Percentage1"]);
 		$Percentage2Arr = (is_array($ary["Percentage2"])==true)? $ary["Percentage2"] : array($ary["Percentage2"]);
 		$Percentage3Arr = (is_array($ary["Percentage3"])==true)? $ary["Percentage3"] : array($ary["Percentage3"]);
 		$Percentage4Arr = (is_array($ary["Percentage4"])==true)? $ary["Percentage4"] : array($ary["Percentage4"]);
 		$Percentage5Arr = (is_array($ary["Percentage5"])==true)? $ary["Percentage5"] : array($ary["Percentage5"]);
 		for($i=0; $i<sizeof($SubjectArr);$i++)
 		{
 			$thisSubject = $SubjectArr[$i];
 			$tmpAry=array();
 			for($j=1;$j<=5;$j++)
 			{
 				$thisPercentage = ${"Percentage".$j."Arr"}[$i];
 				if(trim($thisPercentage)!='')
 					$tmpAry[] = $thisPercentage;
 			}	
 			
 			$Weight[$thisSubject] = implode( ", ",(array)$tmpAry); 
 		}
 		
 		# Construct Remarks Array
 		$RemarksArrColumn1 = array();
 		$RemarksArrColumn2 = array();
 		//if ($ReportType == "W" || $FormNumber==5 || $FormNumber==7)
 		//if ($ReportType == "W" || $FormNumber==7)
 		//{
	 		# Pomotion line
	 		//$RemarksArrColumn1[] = "<b>".$ary['Promotion Status']."</b>";
	 		# Hidden on 15 Jun 2009 佛教筏可紀念中學 - eReport Card -- Template modification [CRM Ref No.: 2009-0612-1530]
	 		//$RemarksArrColumn1[] = $ary['Promotion Status'];
 		//}
 		
 		# Full Mark Passing Mark Line
 		$RemarksArrColumn1[] = str_replace('<!--PassingMark-->', $this->PassingMark[$FormNumber], $eReportCard['Template']['RemarkContent']['FullMarkPassingMark']);
 		
 		# Percentage carried by each paper in language subjects
 		$thisLine = "";
 		$thisLine .= $eReportCard['Template']['RemarkContent']['PercentageCarried'];
 		$thisLine .= "<br />";
 		$thisLine .= $eReportCard['Template']['RemarkContent']['EnglishLanguage'];
 		$thisLine .= $Weight["English"]?$Weight["English"]:implode(", ", (array)$this->EngLanguagePer[$FormNumber]);
 		$thisLine .= "<br />";
 		$thisLine .= $eReportCard['Template']['RemarkContent']['ChineseLanguage'];
 		$thisLine .= $Weight["Chinese"]?$Weight["Chinese"]:implode(", ", (array)$this->ChiLanguagePer[$FormNumber]);
 		if ($FormNumber < 7) {
 			$thisLine .= "<br />";
	 		$thisLine .= $eReportCard['Template']['RemarkContent']['ChineseLanguageNCS'];
	 		$thisLine .= implode(", ", (array)$this->ChiLanguageNCSPer[$FormNumber]);
 		}
	 	$RemarksArrColumn1[] = $thisLine;
 		
 		
 		# Online Learning
 		//if ($FormNumber!=6 && $FormNumber!=7)
 		//2012-0117-0946-35066
// 		if ($FormNumber<4)
// 		{
//	 		$RemarksArrColumn1[] = $eReportCard['Template']['RemarkContent']['OnlineLearning'];
// 		}
 		
 		# Annual Result Weight
        if ($this->schoolYear == '2019' && $ReportType == "W" && ($FormNumber==1 || $FormNumber==2 || $FormNumber==3))
        {
            // do nothing
        }
 		else if ($TwoColumnMode)
 		{
	 		$thisLine = $eReportCard['Template']['RemarkContent']['AnnualResultEn'];
	 		//$thisLine .= "<br />";
	 		$thisLine .= " ";
	 		$thisLine .= $eReportCard['Template']['RemarkContent']['AnnualResultCh'];
	 		$RemarksArrColumn1[] = $thisLine;
 		}
 		
 		# Overall Result - Chi, Eng, Maths and others subject ratio
 		# if term report => in first column
 		# if consolidated report => in second column
 		if ($ReportType == "T" || ($ReportType == "W" && ($FormNumber==4 || $FormNumber==5)))
 		{
	 		//if ($FormNumber!=6 && $FormNumber!=7)
	 		//{
		 		if ($FormNumber==1 || $FormNumber==2)
		 		{
			 		$thisLine = str_replace("<!--ChiEngMathMarks-->", $this->ChiEngMathPer[$FormNumber], $eReportCard['Template']['RemarkContent']['OverallResultForm1&2En']);
			 		$thisLine = str_replace("<!--ISMarks-->", $this->ISMarks[$FormNumber], $thisLine);
			 		$thisLine = str_replace("<!--OthersMarks-->", $this->OthersPer[$FormNumber], $thisLine);
					//$thisLine .= "<br />";
					$thisLine .= "&nbsp;&nbsp;";
			 		$thisLine .= str_replace("<!--ChiEngMathMarks-->", $this->ChiEngMathPer[$FormNumber], $eReportCard['Template']['RemarkContent']['OverallResultForm1&2Ch']);
			 		$thisLine = str_replace("<!--ISMarks-->", $this->ISMarks[$FormNumber], $thisLine);
			 		$thisLine = str_replace("<!--OthersMarks-->", $this->OthersPer[$FormNumber], $thisLine);
		 		}
		 		//else if ($FormNumber==3 || $FormNumber==5)
		 		else if ($FormNumber==3)
		 		{
			 		$thisLine = str_replace("<!--ChiEngMathMarks-->", $this->ChiEngMathPer[$FormNumber], $eReportCard['Template']['RemarkContent']['OverallResultForm3En']);
			 		$thisLine = str_replace("<!--ISMarks-->", $this->ISMarks[$FormNumber], $thisLine);
			 		$thisLine = str_replace("<!--OthersMarks-->", $this->OthersPer[$FormNumber], $thisLine);
					//$thisLine .= "<br />";
					$thisLine .= "&nbsp;&nbsp;";
			 		$thisLine .= str_replace("<!--ChiEngMathMarks-->", $this->ChiEngMathPer[$FormNumber], $eReportCard['Template']['RemarkContent']['OverallResultForm3Ch']);
			 		$thisLine = str_replace("<!--ISMarks-->", $this->ISMarks[$FormNumber], $thisLine);
			 		$thisLine = str_replace("<!--OthersMarks-->", $this->OthersPer[$FormNumber], $thisLine);
		 		}
		 		//2012-0229-1105-24073
		 		//else if ($FormNumber==4 || $FormNumber==5)
		 		else if ($FormNumber==4 || $FormNumber==5 || $FormNumber==6)
		 		{
		 			$OverallResultCHI = $eReportCard['Template']['RemarkContent']['OverallResultCh'];
		 			// 2015-0204-1212-26066 - modified F4 & F5 remarks
		 			if ($FormNumber==4 || $FormNumber==5){
		 				$OverallResultCHI = $eReportCard['Template']['RemarkContent']['OverallResultForm4&5Ch'];
		 			}
		 			
			 		$thisLine = str_replace("<!--ChiEngMathMarks-->", $this->ChiEngMathPer[$FormNumber], $eReportCard['Template']['RemarkContent']['OverallResultEn']);
			 		$thisLine = str_replace("<!--OthersMarks-->", $this->OthersPer[$FormNumber], $thisLine);
					//$thisLine .= "<br />";
					$thisLine .= "&nbsp;&nbsp;";
//			 		$thisLine .= str_replace("<!--ChiEngMathMarks-->", $this->ChiEngMathPer[$FormNumber], $eReportCard['Template']['RemarkContent']['OverallResultCh']);
					$thisLine .= str_replace("<!--ChiEngMathMarks-->", $this->ChiEngMathPer[$FormNumber], $OverallResultCHI);
			 		$thisLine = str_replace("<!--OthersMarks-->", $this->OthersPer[$FormNumber], $thisLine);
		 		}
		 		else if ($FormNumber==6 || $FormNumber==7)
		 		{
		 			$thisLine = $eReportCard['Template']['RemarkContent']['OverallResultSame'];
		 		}
		 		
		 		$RemarksArrColumn1[] = $thisLine;
	 		//}
	 		//else
	 		//{
		 	//	$RemarksArrColumn1[] = $eReportCard['Template']['RemarkContent']['OverallResultSame'];
	 		//}
	 		
 		}
 		else
 		{
	 		//if ($FormNumber!=6 && $FormNumber!=7)
	 		//{
		 		if ($FormNumber==1 || $FormNumber==2)
		 		{
			 		$thisLine = str_replace("<!--ChiEngMathMarks-->", $this->ChiEngMathPer[$FormNumber], $eReportCard['Template']['RemarkContent']['OverallResultForm1&2En']);
			 		$thisLine = str_replace("<!--ISMarks-->", $this->ISMarks[$FormNumber], $thisLine);
			 		$thisLine = str_replace("<!--OthersMarks-->", $this->OthersPer[$FormNumber], $thisLine);
					//$thisLine .= "<br />";
					$thisLine .= "&nbsp;&nbsp;";
			 		$thisLine .= str_replace("<!--ChiEngMathMarks-->", $this->ChiEngMathPer[$FormNumber], $eReportCard['Template']['RemarkContent']['OverallResultForm1&2Ch']);
			 		$thisLine = str_replace("<!--ISMarks-->", $this->ISMarks[$FormNumber], $thisLine);
			 		$thisLine = str_replace("<!--OthersMarks-->", $this->OthersPer[$FormNumber], $thisLine);
		 		}
		 		else if ($FormNumber==3)
		 		{
			 		$thisLine = str_replace("<!--ChiEngMathMarks-->", $this->ChiEngMathPer[$FormNumber], $eReportCard['Template']['RemarkContent']['OverallResultForm3En']);
			 		$thisLine = str_replace("<!--ISMarks-->", $this->ISMarks[$FormNumber], $thisLine);
			 		$thisLine = str_replace("<!--OthersMarks-->", $this->OthersPer[$FormNumber], $thisLine);
					//$thisLine .= "<br />";
					$thisLine .= "&nbsp;&nbsp;";
			 		$thisLine .= str_replace("<!--ChiEngMathMarks-->", $this->ChiEngMathPer[$FormNumber], $eReportCard['Template']['RemarkContent']['OverallResultForm3Ch']);
			 		$thisLine = str_replace("<!--ISMarks-->", $this->ISMarks[$FormNumber], $thisLine);
			 		$thisLine = str_replace("<!--OthersMarks-->", $this->OthersPer[$FormNumber], $thisLine);
		 		}
		 		//else if ($FormNumber==3 || $FormNumber==4 || $FormNumber==5)
		 		else if ($FormNumber==4 || $FormNumber==5)
		 		{
			 		$thisLine = str_replace("<!--ChiEngMathMarks-->", $this->ChiEngMathPer[$FormNumber], $eReportCard['Template']['RemarkContent']['OverallResultEn']);
			 		$thisLine = str_replace("<!--OthersMarks-->", $this->OthersPer[$FormNumber], $thisLine);
					//$thisLine .= "<br />";
					$thisLine .= "&nbsp;&nbsp;";
//			 		$thisLine .= str_replace("<!--ChiEngMathMarks-->", $this->ChiEngMathPer[$FormNumber], $eReportCard['Template']['RemarkContent']['OverallResultCh']);
					// 2015-0204-1212-26066 - modified F4 & F5 remarks
					$thisLine .= str_replace("<!--ChiEngMathMarks-->", $this->ChiEngMathPer[$FormNumber], $eReportCard['Template']['RemarkContent']['OverallResultForm4&5Ch']);
			 		$thisLine = str_replace("<!--OthersMarks-->", $this->OthersPer[$FormNumber], $thisLine);
		 		}
		 		else if ($FormNumber==6 || $FormNumber==7)
		 		{
		 			$thisLine = $eReportCard['Template']['RemarkContent']['OverallResultSame'];
		 		}
		 		
		 		
		 		$RemarksArrColumn2[] = $thisLine;
	 		//}
	 		//else
	 		//{
		 	//	$RemarksArrColumn2[] = $eReportCard['Template']['RemarkContent']['OverallResultSame'];
	 		//}
 		}
 		
 		# ECA
 		$thisLine = $eReportCard['Template']['RemarkContent']['ECAEn'];
 		//$thisLine .= "<br />";
 		$thisLine .= " ";
 		$thisLine .= $eReportCard['Template']['RemarkContent']['ECACh'];
 		if ($TwoColumnMode)
 		{
	 		$RemarksArrColumn2[] = $thisLine;
		}
		//if ($FormNumber==5 || $FormNumber==7)
		if ($FormNumber==7)
 		{
	 		$RemarksArrColumn1[] = $thisLine;
		}
		
		# Extra-remarks
		$extraRemarks = $ary['Remark'];
		//2012-0222-1111-58073
		//if (is_array($extraRemarks))
		//	$extraRemarks = implode('<br />', $extraRemarks);
		if (!is_array($extraRemarks)) {
			$extraRemarks = array($extraRemarks);
		}
		$numOfExtraRemarks = count($extraRemarks);
			
		if ($extraRemarks != "")
		{
			if (sizeof($RemarksArrColumn2)!=0) {
				//$RemarksArrColumn2[] = $extraRemarks;
				
				for ($i=0; $i<$numOfExtraRemarks; $i++) {
					$thisExtraRemarks = trim($extraRemarks[$i]);
					if ($thisExtraRemarks != '') {
						$RemarksArrColumn2[] = $thisExtraRemarks;
					}
				}
			}
			else {
				//$RemarksArrColumn1[] = $extraRemarks;
				
				for ($i=0; $i<$numOfExtraRemarks; $i++) {
					$thisExtraRemarks = trim($extraRemarks[$i]);
					if ($thisExtraRemarks != '') {
						$RemarksArrColumn1[] = $thisExtraRemarks;
					}
				}
			}
		}
 		//2014-0912-1716-13164
 		$TwoColumnMode = false;
 		$RemarksArrColumn1 = array_merge($RemarksArrColumn1,$RemarksArrColumn2);
 		# Numbering the remarks list
 		$thisDisplay1 = $this->GENERATE_REMARKS_LIST($RemarksArrColumn1, $StartingNumber=1);
 		$thisDisplay2 = $this->GENERATE_REMARKS_LIST($RemarksArrColumn2, sizeof($RemarksArrColumn1)+1);
 		
 		if ($ReportType == "T" || $TwoColumnMode)
 		{
	 		$colspan = "";
 		}
 		else
 		{
	 		$colspan = " colspan=\"2\" ";
 		}
 		 		
 		# Construct table
 		$remarksTable = "";
		$remarksTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\" class=\"report_border\">";
		$remarksTable .= "<tr><td class='MISC_title' $colspan>&nbsp;&nbsp;<b>".$eReportCard['Template']['Remark']."</b></td></tr>";
		
		
		if ($TwoColumnMode) {
			$remarksTable .= "<tr><td class='tabletext' width='50%' valign='top'>".$thisDisplay1."</td>";
			$remarksTable .= "<td class='tabletext' width='50%' valign='top'>".$thisDisplay2."</td>";
		}
		else {
			$remarksTable .= "<tr><td class='tabletext' width='100%' valign='top'>".$thisDisplay1."</td>";
		}
		
		$remarksTable .= "</tr>";
		
		$remarksTable .= "</table>";
		
		return $remarksTable;
	}
	
	function getAwardsTable($ReportID, $StudentID=''){
		global $eReportCard, $PATH_WRT_ROOT;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
 		$ReportType = $SemID == "F" ? "W" : "T";
		$term_seq = $this->Get_Semester_Seq_Number($SemID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
 		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode=true);
		
		$x = "";
		
		// Whole Year Report / F.6 2nd Term Reprot
		if($ReportType=="W" || ($FormNumber==6 && $term_seq==2))
		{
			# Retrieve School Awards Records
			$award_lists = $this->Get_Student_Award_List($ReportID, $StudentID);
			
			// Student with School Awards Record
			if(count($award_lists) > 0)
			{
				# Construct table
		 		$awardsTable = "";
				$awardsTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\" class=\"report_border\">";
				$awardsTable .= "<tr><td class='award_title' $colspan><b>".$eReportCard['Template']['AwardFromSchool']."</b></td></tr>";
				$awardsTable .= "<tr><td class='award_subtitle border_top' $colspan><b>".$eReportCard['Template']['AwardName']."</b></td></tr>";
				foreach($award_lists as $award_title){
					$awardsTable .= "<tr><td class='tabletext border_top' width='100%' valign='top'>".$award_title["Title"]."</td></tr>";
				}
				$awardsTable .= "</tr>";
				$awardsTable .= "</table>";
				
				$x = "<tr valign='top'><td>".$awardsTable."</td></tr>";
				// seperate two tables
				$x .= "<tr valign='top'><td>&nbsp;</td></tr>";
			}
		}
		
		return $x;
	}
	
	########### END Template Related

	/* moved to libreportcard2008.php
	function GET_FORM_NUMBER($ClassLevelID)
	{
		$ClassLevelName = $this->returnClassLevel($ClassLevelID);
		$FormNumber = substr($ClassLevelName,strlen($ClassLevelName)-1,1);
		
		return $FormNumber;
	}
	*/
	
	function GENERATE_REMARKS_LIST($RemarksArr, $StartNumber=1)
	{
		$remarksList = "";
		
		/* Do not work with OL tab
		$remarksList .= "<ol >";
		for ($i=0; $i<sizeof($RemarksArr); $i++)
		{
			$remarksList .= "<li>";
			$remarksList .= $RemarksArr[$i];
			$remarksList .= "</li>";
		}
		
		$remarksList .= "</ol>";
		*/
		
		$remarksList = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		$startIndex = $StartNumber;
		$endIndex = sizeof($RemarksArr) + $StartNumber;
				
		for ($i=0; $i<sizeof($RemarksArr); $i++)
		{
			$remarksList .= "<tr>";
				$remarksList .= "<td class='tabletext' width='20' valign='top'>&nbsp;&nbsp;";
				$remarksList .= $i+$startIndex.". ";
				$remarksList .= "</td>";
				$remarksList .= "<td class='tabletext'>";
				$remarksList .= "<div align='justify'>".$RemarksArr[$i]."</div>";
				$remarksList .= "</td>";
			$remarksList .= "</tr>";
		}
		
		$remarksList .= "</table>";
		
		
		return $remarksList;
	}
	
	function getGrandTotalFullMark($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$SemID = $ReportSetting['Semester'];
 		$ReportType = $SemID == "F" ? "W" : "T";
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2) = $ColHeaderAry;
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID);
		
		# get isAllNAAry array so as to set border line for subject column first
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID);
		//$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		$SubjectColArr	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID, $isAllNAAry);
		$SubjectCol = $SubjectColArr[0];
		$borderTopSubjectID = $SubjectColArr[1];
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# For full marks calculation (ignore the all-NA-subjects)
		$displayedMainSubjectIDArr = array();
		
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) {
				$Droped = 0;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da) {
					if($da['Grade']=="*")	$Droped=1;
				}
			}
			
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				if (!$isSub)
				{
					$displayedMainSubjectIDArr[] = $thisSubjectID;
				}
			}
		}
		
		
		# Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
		$categorySettings = $this->LOAD_SETTING('Calculation');
		if ($ReportType == "T")
			$TermCalculationType = $categorySettings['OrderTerm'];
		else
			$TermCalculationType = $categorySettings['OrderFullYear'];
		
		# calculate total subjects full mark 
		
		
		$SubjectsFullMark = $this->returnSubjectFullMark($ClassLevelID, $WithSub=0, $displayedMainSubjectIDArr, 0, $ReportID);
		$totalFullMark = 0;
  		foreach ($SubjectsFullMark as $SubjectID => $thisFullMark)
  		{
	  		if (is_numeric($thisFullMark))
	  		{
		  		# Get Personal Weight among Component Subjects of each student
				if($TermCalculationType == 1){
					$OtherCondition = "SubjectID = ".$SubjectID." AND ReportColumnID IS NULL ";
					$thisWeight = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition);
				} else {
					$OtherCondition = "SubjectID = ".$SubjectID." ";
					$thisWeight = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
				}
				
				$totalFullMark += ($thisFullMark * $thisWeight[0]['Weight']);
	  		}
  		}
  		
  		return $totalFullMark;
	}
	
	function Get_Ranking_Display($Rank, $NumOfStudent, $DisplayRank, $DisplayNumOfStudent)
	{
		$Display = '';
		$DisplayRank = ($Rank != $this->EmptySymbol || $DisplayRank == true);
		
		if ($DisplayRank==true && $DisplayNumOfStudent==true)
		{
			$Display = $Rank.' / '.$NumOfStudent;
		}
		else if ($DisplayRank==true && $DisplayNumOfStudent==false)
		{
			$Display = $Rank;
		}
		else if ($DisplayRank==false && $DisplayNumOfStudent==true)
		{
			$Display = $this->EmptySymbol.' / '.$NumOfStudent;
		}
		else if ($DisplayRank==false && $DisplayNumOfStudent==false)
		{
			$Display = $this->EmptySymbol;
		}
		
		return $Display;
	}
	
	function Get_student_Award_List($ReportID, $StudentID=''){
		global $eclass_db, $intranet_db, $ipf_cfg;
		
		$condApprove = " and os.RecordStatus in (".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]." ,".$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"].")";
		
		$sql = "select
					op.Title
				from
					".$eclass_db.".OLE_STUDENT as os
					inner join ".$eclass_db.".OLE_PROGRAM as op on os.programid = op.programid
					inner join ".$eclass_db.".OLE_CATEGORY as oc on op.Category = oc.RecordID
					inner join ".$intranet_db.".ACADEMIC_YEAR as ay on op.AcademicYearID = ay.AcademicYearID
				where
					os.userid = '$StudentID'
					$condApprove
					AND op.ELE LIKE '%".$this->SchoolAwardCode."%'
					AND op.INTEXT='INT'
					AND op.AcademicYearID = '".$this->GET_ACTIVE_YEAR_ID()."'
				order by
					op.Title
				";
		$result = $this->returnResultSet($sql);

		return $result;
	}
}
?>