<?php
# Editing by 

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/buddhist_hui_yuan.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary","attendance", "remark", "demerit", "award", "OLE");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 0;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "---";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID) {
		global $eReportCard;
		
		$AttendanceConductTable = $this->GET_ATTENDANCE_CONDUCT_TABLE($ReportID, $StudentID);
		
		$TablePage1 = "";
		$TablePage1 .= "<table width='100%' border='0' cellspacing='0' cellpadding='5' align='center' valign='top' style='page-break-after:always'>";
		$TablePage1 .= "<tr><td>".$TitleTable."</td></tr>";
		$TablePage1 .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TablePage1 .= "<tr><td>".$MSTable."</td></tr>";
		$TablePage1 .= "<tr><td>".$AttendanceConductTable."</td></tr>";
		$TablePage1 .= "</table>";
		
		$TablePage2 = "";
		$TablePage2 .= "<table width='100%' border='0' cellspacing='0' cellpadding='5' align='center' valign='top'>";
		$TablePage2 .= "<tr valign='top' height='810'><td>".$MiscTable."</td></tr>";
		$TablePage2 .= "<tr><td valign='bottom'>".$SignatureTable."</td></tr>";
		$TablePage2 .= $FooterRow;
		$TablePage2 .= "</table>";
		
		$x = "";
		$x .= "<tr valign='top'><td>";
		$x .= $TablePage1;
		$x .= "</td></tr>";
		$x .= "<tr valign='top'><td>";
		$x .= $TablePage2;
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
			# get school badge
			$SchoolLogo = GET_SCHOOL_BADGE();
				
			# get school year (eRC active year)
			$SchoolYear = $this->GET_ACTIVE_YEAR("-");
			
			$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
	
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
					$TitleTable .= "<tr><td nowrap='nowrap' class='school_title' align='center'>".$eReportCard['Template']['SchoolNameEn']."</td></tr>\n";
					$TitleTable .= "<tr><td nowrap='nowrap' class='school_title' align='center'>".$eReportCard['Template']['SchoolNameCh']."</td></tr>\n";
					$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$SchoolYear."</td></tr>\n";
					//if(!empty($ReportTitle))
					//	$TitleTable .= "<tr><td nowrap='nowrap' class='school_title' align='center'>".$ReportTitle."</td></tr>\n";
						
					$TitleTable .= "</table>\n";
					$TitleTable .= "</td>";
					
					# Photo border
					$TitleTable .= "<td width='120' height='120' align='center' class='report_border'>&nbsp</td>\n";
					
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
					$TitleTable .= "</td>";
				}
			}
			
			$TitleTable .= "</tr>\n";
			$TitleTable .= "</table>\n";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal = "XXX";
			$data['AcademicYear'] = getCurrentAcademicYear();
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] = $lu->UserName2Lang('ch', 2);
				$data['ClassNo'] = $lu->ClassNumber;
				$data['StudentNo'] = $data['ClassNo'];
				$data['Class'] = $lu->ClassName;
				/*
				if (is_array($SettingStudentInfo))
				{
					if (!in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo))
						$data['Class'] .= " (".$lu->ClassNumber.")";
				}
				else
				{
					$data['Class'] .= " (".$lu->ClassNumber.")";
				}
				*/
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] = $lu->Gender;
				$data['STRN'] = $lu->STRN;
				
				$ClassTeacherAry = $lclass->returnClassTeacher($lu->ClassName);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
			}
			
			# hardcode the 1st 4 items (Name, ClassName, ClassNumber, STRN)
			$defaultInfoArray = array("Name", "Class", "ClassNo", "STRN");
						
			$StudentInfoTable .= "<table width='100%' border='0' cellpadding='3' cellspacing='0' align='center' class='report_border'>";
			
			# Row1 (Name)
			$SettingID = "Name";
			$thisTitle = $eReportCard['Template']['StudentInfo'][$SettingID];
			$StudentInfoTable .= "<tr><td class='tabletext' valign='top' height='{$LineHeight}' colspan='2'>&nbsp;";
			$StudentInfoTable .= "<b>".$thisTitle." : </b>".$data[$SettingID];
			$StudentInfoTable .= "</td></tr>";
			
			# Row2 (Class, Class No.)
			$SettingID = "Class";
			$thisTitle = $eReportCard['Template']['StudentInfo'][$SettingID];
			$StudentInfoTable .= "<tr>";
				$StudentInfoTable .= "<td class='tabletext' valign='top' height='{$LineHeight}' width='40%'>&nbsp;";
					$StudentInfoTable .= "<b>".$thisTitle." : </b>".$data[$SettingID];
				$StudentInfoTable .= "</td>";
				
			$SettingID = "ClassNo";
			$thisTitle = $eReportCard['Template']['StudentInfo'][$SettingID];
				$StudentInfoTable .= "<td class='tabletext' valign='top' height='{$LineHeight}' width='60%'>&nbsp;";
					$StudentInfoTable .= "<b>".$thisTitle." : </b>".$data[$SettingID];
				$StudentInfoTable .= "</td>";
			$StudentInfoTable .= "</td></tr>";
			
			# Row3 (STRN)
			$SettingID = "STRN";
			$thisTitle = $eReportCard['Template']['StudentInfo'][$SettingID];
			$StudentInfoTable .= "<tr><td class='tabletext' valign='top' height='{$LineHeight}' colspan='2'>&nbsp;";
			$StudentInfoTable .= "<b>".$thisTitle." : </b>".$data[$SettingID];
			$StudentInfoTable .= "</td></tr>";
			
			# Display student info selected by the user other than the 1st 4 items
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if(in_array($SettingID, $SettingStudentInfo)===true && (!in_array($SettingID, $defaultInfoArray))===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
					
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
						}
						
						$StudentInfoTable .= "<td class='tabletext' valign='top' height='{$LineHeight}'>&nbsp;";
						$StudentInfoTable .= "<b>".$Title." : </b>".$data[$SettingID];
						$StudentInfoTable .= "</td>";
											
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						} 
						$count++;
					}
				}				
			}
			
			$StudentInfoTable .= "</table>";
			
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
		
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID);
						
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		$counterRow = 0;
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='3' class='report_border' height='600' valign='top'>";
		
		$DetailsTable .= "<tr><td class='tabletext' colspan='10' valign='top'>&nbsp;<b>".$eReportCard['Template']['AcademicPerformance']."</b></td></tr>";
		
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				# Subject Teacher Comment
				if ($AllowSubjectTeacherComment) {
					//$css_border_top = $isSub?"":"border_top";
					$css_border_top = "";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='tabletext $css_border_top'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='tabletext $css_border_top' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol;
					}
					$DetailsTable .= "</span></td>";
				}
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
			$counterRow++;
		}
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr, $counterRow);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		#########################################################
		############## Marks START
		$row2 = "";
		if($ReportType=="T")	# Terms Report Type
		{
			# Retrieve Invloved Assesment
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if (sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
			$e = 0;
			for($i=0;$i<sizeof($ColumnTitle);$i++)
			{
				$ColumnTitleDisplay =  strtoupper(convert2unicode($ColumnTitle[$i], 1, 2));
				$ColumnTitleDisplay = $this->ADD_CHINESE_TITLE($ColumnTitleDisplay);
				$row1 .= "<td valign='middle' height='{$LineHeight}' class='small_title' align='center'>". $ColumnTitleDisplay . "</td>";
				$n++;
 				$e++;
			}
		}
		else					# Whole Year Report Type
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
 			$needRowspan=0;
			for($i=0;$i<sizeof($ColumnData);$i++)
			{
				$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
				$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
				if($isDetails==1)
				{
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColumnID = array();
					$ColumnTitle = array();
					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
					for($j=0;$j<sizeof($ColumnTitle);$j++)
					{
						$ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$j], 1, 2));
						$row2 .= "<td class='small_title' align='center' height='{$LineHeight}' >". $ColumnTitleDisplay . "</td>";
						$n++;
						$e++;
					}
					$colspan = "colspan='". sizeof($ColumnTitle) ."'";
					$Rowspan = "";
					$needRowspan++;
					$t[$i] = sizeof($ColumnTitle);
				}
				else
				{
					$colspan = "";
					$Rowspan = "rowspan='2'";
					$e++;
				}
				$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='small_title' align='center'>". $SemName ."</td>";
			}
		}
		############## Marks END
		#########################################################
		$Rowspan = $row2 ? "rowspan='2'" : "";

		if(!$needRowspan)
			$row1 = str_replace("rowspan='2'", "", $row1);
		
		$x = "<tr>";
		# Subject 
		$SubjectTitle = strtoupper(trim($eReportCard['Template']['SubjectsEn']))." ".$eReportCard['Template']['SubjectsCh'];
		$x .= "<td {$Rowspan}  valign='middle' class='signature_text' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."' colspan='2'>&nbsp;&nbsp;<u><b>". $SubjectTitle . "</b></u></td>";
		$n++;
		
		# Full Mark
		if ($ShowSubjectFullMark)
		{
			$FullMarkTitle = strtoupper($eReportCard['Template']['SchemesFullMarkEn'])."<br />".$eReportCard['Template']['SchemesFullMarkCh'];
			$x .= "<td {$Rowspan} valign='middle' align='center' class='small_title' height='{$LineHeight}' >". $FullMarkTitle . "</td>";
			$n++;
		}
		
		# Marks
		$x .= $row1;
		
		# Subject Overall 
		if($ShowSubjectOverall)
		{
			$x .= "<td {$Rowspan}  valign='middle' class='small_title' align='center'>". $eReportCard['Template']['SubjectOverall'] ."</td>";
			$n++;
		}
		else
		{
			$e--;	
		}
		
		# Subject Teacher Comment
		if ($AllowSubjectTeacherComment) {
			$x .= "<td {$Rowspan} valign='middle' class='small_title' align='center'>".$eReportCard['TeachersComment']."</td>";
		}
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		//$css_border_top = ($Prefix)? "" : "border_top";
			 		$css_border_top = "";
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
			 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='tabletext'>$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
		}
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			$IssueDate = ($SettingID == "IssueDate" && $Issued)	? "<u>".$Issued."</u>" : "__________";
			$SignatureTable .= "<td valign='bottom' align='center'>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td align='center' class='signature_text' height='130' valign='bottom'>_____". $IssueDate ."_____</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='signature_text' valign='bottom'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array(), $counterRow=0)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ShowSubjectFullMark 		= $ReportSetting['ShowSubjectFullMark'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID);
		$GrandTotal = $StudentID ? $result['GrandTotal'] : "S";
		$AverageMark = $StudentID ? $result['GrandAverage'] : "S";
		
 		$FormPosition = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? "--": $result['OrderMeritForm']) : "--") : "#";
  		$ClassPosition = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? "--": $result['OrderMeritClass']) : "--") : "#";
  		$ClassNumOfStudent = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? "--": $result['ClassNoOfStudent']) : "--") : "#";
  		$FormNumOfStudent = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? "--": $result['FormNoOfStudent']) : "--") : "#";
		
		if ($CalOrder == 2) {
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID);
				$columnTotal[$ColumnID] = $StudentID ? $columnResult['GrandTotal'] : "S";
				$columnAverage[$ColumnID] = $StudentID ? $columnResult['GrandAverage'] : "S";
				$columnClassPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritClass'] : "S";
				$columnFormPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritForm'] : "S";
				$columnClassNumOfStudent[$ColumnID] = $StudentID ? $columnResult['ClassNoOfStudent'] : "S";
				$columnFormNumOfStudent[$ColumnID] = $StudentID ? $columnResult['FormNoOfStudent'] : "S";
			}
		}
		
		$first = 1;
		# Overall Result
		if($ShowGrandTotal)
		{
			//$border_top = "";
			$border_top = "";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['OverallResultEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['OverallResultCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			
			if ($ShowSubjectFullMark)
			{
				$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			}
			
			if ($CalOrder == 1) {
				for($i=0;$i<$ColNum2;$i++)
					$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			} else {
				$curColumn = 0;
				foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					for ($i=0; $i<$NumOfAssessment[$curColumn] - 1; $i++)
					{
						$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					}
					$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$columnTotal[$ColumnID]."</td>";
					$curColumn++;
				}
			}
			
			$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>". $GrandTotal ."</td>";
			if ($AllowSubjectTeacherComment) {
				$x .= "<td class=' $border_top' align='center'>";
				$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
				$x .= "</td>";
			}
			$x .= "</tr>";
			
			$counterRow++;
		}
		
		# Average Mark 
		if($ShowGrandAvg)
		{
			$border_top = "";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['AvgMarkEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['AvgMarkCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			
			if ($ShowSubjectFullMark)
			{
				$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			}
			
			if ($CalOrder == 1) {
				for($i=0;$i<$ColNum2;$i++)
					$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			} else {
				$curColumn = 0;
				foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					for ($i=0; $i<$NumOfAssessment[$curColumn] - 1; $i++)
					{
						$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					}
					$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$columnAverage[$ColumnID]."</td>";
					$curColumn++;
				}
			}
			$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>". $AverageMark ."</td>";
			if ($AllowSubjectTeacherComment) {
				$x .= "<td class=' $border_top' align='center'>";
				$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
				$x .= "</td>";
			}
			$x .= "</tr>";
			
			$counterRow++;
		}
		
		# Position in Class 
		if($ShowOverallPositionClass)
		{
			$border_top = "";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ClassPositionEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ClassPositionCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			
			if ($ShowSubjectFullMark)
			{
				$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			}
			
			if ($CalOrder == 1) {
				for($i=0;$i<$ColNum2;$i++)
					$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			} else {
				$curColumn = 0;
				foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					for ($i=0; $i<$NumOfAssessment[$curColumn] - 1; $i++)
					{
						$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					}
					$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$columnClassPos[$ColumnID]."</td>";
					$curColumn++;
				}
			}
			$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>". $ClassPosition ."</td>";
			if ($AllowSubjectTeacherComment) {
				$x .= "<td class=' $border_top' align='center'>";
				$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
				$x .= "</td>";
			}
			$x .= "</tr>";
			
			$counterRow++;
		}
		
		# Number of Students in Class 
		if($ShowNumOfStudentClass)
		{
			$border_top = "";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ClassNumOfStudentEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ClassNumOfStudentCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			
			if ($ShowSubjectFullMark)
			{
				$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			}
			
			if ($CalOrder == 1) {
				for($i=0;$i<$ColNum2;$i++)
					$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			} else {
				$curColumn = 0;
				foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					for ($i=0; $i<$NumOfAssessment[$curColumn] - 1; $i++)
					{
						$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					}
					$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$columnClassNumOfStudent[$ColumnID]."</td>";
					$curColumn++;
				}
			}
			$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>". $ClassNumOfStudent ."</td>";
			if ($AllowSubjectTeacherComment) {
				$x .= "<td class=' $border_top' align='center'>";
				$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
				$x .= "</td>";
			}
			$x .= "</tr>";
			
			$counterRow++;
		}
		
		# Position in Form 
		if($ShowOverallPositionForm)
		{
			$border_top = "";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['FormPositionEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['FormPositionCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			
			if ($ShowSubjectFullMark)
			{
				$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			}
			
			if ($CalOrder == 1) {
				for($i=0;$i<$ColNum2;$i++)
					$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			} else {
				$curColumn = 0;
				foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					for ($i=0; $i<$NumOfAssessment[$curColumn] - 1; $i++)
					{
						$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					}
					$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$columnFormPos[$ColumnID]."</td>";
					$curColumn++;
				}
			}
			$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>". $FormPosition ."</td>";
			if ($AllowSubjectTeacherComment) {
				$x .= "<td class=' $border_top' align='center'>";
				$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
				$x .= "</td>";
			}
			$x .= "</tr>";
			
			$counterRow++;
		}
		
		# Number of Students in Form 
		if($ShowNumOfStudentForm)
		{
			$border_top = "";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['FormNumOfStudentEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['FormNumOfStudentCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			
			if ($ShowSubjectFullMark)
			{
				$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			}
			
			if ($CalOrder == 1) {
				for($i=0;$i<$ColNum2;$i++)
					$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			} else {
				$curColumn = 0;
				foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					for ($i=0; $i<$NumOfAssessment[$curColumn] - 1; $i++)
					{
						$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					}
					$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$columnFormNumOfStudent[$ColumnID]."</td>";
					$curColumn++;
				}
			}
			$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>". $FormNumOfStudent ."</td>";
			if ($AllowSubjectTeacherComment) {
				$x .= "<td class=' $border_top' align='center'>";
				$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
				$x .= "</td>";
			}
			$x .= "</tr>";
			
			$counterRow++;
		}
		
		$MaxNumRow = 23;
		# Empty rows
		for($i=0; $i<($MaxNumRow-$counterRow); $i++)
		{
			$x .= "<tr><td>&nbsp;</td></tr>";
		}
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eReportCard;
				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1);
		if($ReportType=="T")	# Temrs Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
							
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# define css
				//$css_border_top = ($isSub)? "" : "border_top";
				$css_border_top = "";
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				$isAllNA = true;
				
				# Subject Full Mark
				if ($ShowSubjectFullMark)
				{
					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
  					
					$x[$SubjectID] .= "<td width='$FullMarkColWidth' class='{$css_border_top} tabletext' align='center'>". $thisFullMark ."&nbsp;</td>";
				}
									
				# Assessment Marks & Display
				for($i=0;$i<sizeof($ColumnID);$i++)
				{
					$thisColumnID = $ColumnID[$i];
					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
					
					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
					$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);

					if ($isSub && $columnSubjectWeightTemp == 0)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
						$thisMarkDisplay = $this->EmptySymbol;
					} else {
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnID[$i]][Mark] : "";
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][$ColumnID[$i]][Grade] : "";
						
												
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						if ($thisMark != "N.A.")
						{
							$isAllNA = false;
						}
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
						
						if($needStyle)
						{
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
							$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
						
					//	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					//	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
					//	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark; 
					}
						
					$x[$SubjectID] .= "<td class='{$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center'>". $thisMarkDisplay ."</td>";
  					
  					/*
  					if($ShowSubjectFullMark)
  					{
	  					# check special full mark
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
	  					
						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
					}
					*/
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}
								
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall)
				{
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][0][Grade] : ""; 
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
										
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
 						if($thisSubjectWeight > 0)
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
						else
							$thisMarkTemp = $thisMark;
 						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
						
					$x[$SubjectID] .= "<td class='{$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' >". $thisMarkDisplay ."</td>";
  					/*
  					if($ShowSubjectFullMark)
  					{
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $thisFullMark .")</td>";
					}
					*/
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='{$css_border_top}'>".$this->EmptySymbol."</td>";
				}
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
							
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				$isAllNA = true;
				
				if($ShowSubjectFullMark)
				{
  					$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
  					
  					$x[$SubjectID] .= "<td class='{$css_border_top} tabletext' align='center'>". $FullMark ."</td>";
				}
				
				# Terms's Assesment / Terms Result
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
					//$css_border_top = $isSub? "" : "border_top";
					$css_border_top = "";
					
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					if($isDetails==1)		# Retrieve assesments' marks
					{
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum'] ."' and ClassLevelID = '".$ClassLevelID."' ");
						
						# if no term reports, CANNOT retrieve assessment result at all
						if (empty($thisReport)) {
							for($j=0;$j<sizeof($ColumnID);$j++) {
								$x[$SubjectID] .= "<td align='center' class='tabletext {$css_border_top}'>".$this->EmptySymbol."</td>";
							}
						} else {
							$thisReportID = $thisReport['ReportID'];
							$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
							$ColumnID = array();
							$ColumnTitle = array();
							foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
							
							$thisMarksAry = $this->getMarks($thisReportID, $StudentID);		
								 
							for($j=0;$j<sizeof($ColumnID);$j++)
							{
								$thisColumnID = $ColumnID[$i];
								$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
								$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
								$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
								
								$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
								$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
					
								if ($isSub && $columnSubjectWeightTemp == 0)
								{
									$thisMarkDisplay = "---";
								}
								else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
									$thisMarkDisplay = "---";
								} else {
									$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnID[$j] ."' and SubjectID = '$SubjectID' ");
									$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
									
									$thisMark = $ScaleDisplay=="M" ? $thisMarksAry[$SubjectID][$ColumnID[$j]][Mark] : "";
									$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $thisMarksAry[$SubjectID][$ColumnID[$j]][Grade] : ""; 
									
									# for preview purpose
									if(!$StudentID)
									{
										$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
										$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
									}
									$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
													
									if ($thisMark != "N.A.")
									{
										$isAllNA = false;
									}
									
									# check special case
									list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
									if($needStyle)
									{
										$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
										$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
										$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
									}
									else
									{
										$thisMarkDisplay = $thisMark;
									}
								}
									
								$x[$SubjectID] .= "<td class='{$css_border_top}'>";
								$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
			  					$x[$SubjectID] .= "<td class='tabletext' align='center'>". $thisMarkDisplay ."</td>";
			  					/*
			  					if($ShowSubjectFullMark)
			  					{
				  					$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
									$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
								}
								*/
								$x[$SubjectID] .= "</tr></table>";
								$x[$SubjectID] .= "</td>";
							}
						}
					}
					else					# Retrieve Terms Overall marks
					{
						if ($isParentSubject && $CalculationOrder == 1) {
							$thisMarkDisplay = $this->EmptySymbol;
						} else {
							# See if any term reports available
							$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
							
							# if no term reports, the term mark should be entered directly
							if (empty($thisReport)) {
								$thisReportID = $ReportID;
								$MarksAry = $this->getMarks($thisReportID, $StudentID);
								$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
								$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
							} else {
								$thisReportID = $thisReport['ReportID'];
								$MarksAry = $this->getMarks($thisReportID, $StudentID);
								$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
								$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
							}
	 						
							$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
							$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
														
							# for preview purpose
							if(!$StudentID)
							{
								$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
								$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
							}
							# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
							$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
							
							if ($thisMark != "N.A.")
							{
								$isAllNA = false;
							}
						
							# check special case
							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
							if($needStyle)
							{
								$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
	 							//$thisMark = ($ScaleDisplay=="M" && $StudentID) ? my_round($thisMark*$thisSubjectWeight,2) : $thisMark;
								$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
							}
						}
							
						$x[$SubjectID] .= "<td class='{$css_border_top}'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  					$x[$SubjectID] .= "<td class='tabletext' align='center'>". $thisMarkDisplay ."</td>";
	  					/*
	  					if($ShowSubjectFullMark)
	  					{
 							$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1 && $UseWeightedMark) ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID];
							$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
						}
						*/
						$x[$SubjectID] .= "</tr></table>";
						$x[$SubjectID] .= "</td>";
					}
				}
				
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall && $CalculationOrder == 1)
				{
					$MarksAry = $this->getMarks($ReportID, $StudentID);		
					
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][0][Grade] : ""; 
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
						
					$x[$SubjectID] .= "<td class=' {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center'>". $thisMarkDisplay ."</td>";
  					/*
  					if($ShowSubjectFullMark)
  					{
						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $SubjectFullMarkAry[$SubjectID] .")</td>";
					}
					*/
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='{$css_border_top}'>".$this->EmptySymbol."</td>";
				}
				
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		}	# End Whole Year Report Type
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting	= $this->returnReportTemplateBasicInfo($ReportID); 
		$ClassLevelID				= $ReportSetting['ClassLevelID'];
		$SemID						= $ReportSetting['Semester'];
 		$ReportType					= $SemID == "F" ? "W" : "T";
 		$DateOfIssue 				= $ReportSetting['Issued'];
 		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
 		
 		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
 		
 		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve the latest Term
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		foreach($sems as $TermID=>$TermName)
			$latestTerm = $TermID;
		$latestTerm++;
 		
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		foreach($csvType as $k=>$Type)
		{
			$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);	
			if(!empty($csvData)) 
			{
				foreach($csvData as $RegNo=>$data)
				{
					if($RegNo == $WebSamsRegNo)
					{
	 					foreach($data as $key=>$val)
		 					$ary[$key] = $val;
					}
				}
			}
		}
		
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		
		if($SemID=="F")
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$InfoTermID = $TermID+1;
				
				if(!empty($csvType))
				{
					foreach($csvType as $k=>$Type)
					{
						$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
						if(!empty($csvData)) 
						{
							foreach($csvData as $RegNo=>$data)
							{
								if($RegNo == $WebSamsRegNo)
								{
									foreach($data as $key=>$val)
										$ary[$TermID][$key] = $val;
								}
							}
						}
					}
				}
			}
			
			# calculate sems/assesment col#
			$ColNum2Ary = array();
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				if($d1['IsDetails']==1)
				{
					# check sems/assesment col#
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1;
				}
				else
					$ColNum2Ary[$TermID] = 0;
			}
		}
		else
		{
			$InfoTermID = $SemID+1;
			
			if(!empty($csvType))
			{
				foreach($csvType as $k=>$Type)
				{
					$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
					if(!empty($csvData)) 
					{
						foreach($csvData as $RegNo=>$data)
						{
							if($RegNo == $WebSamsRegNo)
							{
								foreach($data as $key=>$val)
									$ary[$SemID][$key] = $val;
							}
						}
					}
				}
			}
		}
	
		$x = "";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\" class='report_border' valgin='top'>";
		
		# OLE
		$MaxNumRow = 8;
		$countDisplayed = 0;
		$thisSetting = "OLE";
		$thisTitle = $eReportCard['Template']['OLECh']." ".$eReportCard['Template']['OLEEn'];
		if ($ReportType == "T")
		{
			$thisValue = ($StudentID)? $ary[0][$thisSetting] : $this->EmptySymbol;
		}
		else
		{
			$thisValue = ($StudentID)? $ary[$latestTerm-1][$thisSetting] : $this->EmptySymbol;
		}		
		$x .= "<tr>";
			$x .= "<td class='tabletext' height='{$LineHeight}' >&nbsp;&nbsp;<b>". $thisTitle ."</b></td>";
		$x .= "</tr>";
				
		if (is_array($thisValue))
		{
			for ($i=0; $i<sizeof($thisValue); $i++)
			{
				$thisDisplay = $thisValue[$i];
				
				if ($thisDisplay == "")
					continue;
				 
				$x .= "<tr><td class='tabletext ' height='{$LineHeight}' >&nbsp;&nbsp;". $thisDisplay ."</td></tr>";
				$countDisplayed++;
			}
		}
		else
		{
			if ($thisValue != "")
			{
				$x .= "<tr><td class='tabletext ' height='{$LineHeight}' >&nbsp;&nbsp;". $thisValue ."</td></tr>";
				$countDisplayed++;
			}
		}
		for ($i=0; $i<($MaxNumRow-$countDisplayed); $i++)
		{
			$x .= "<tr><td class='tabletext ' height='{$LineHeight}' >&nbsp;&nbsp;</td></tr>";
		}
		
		
		# Awards
		$MaxNumRow = 8;
		$countDisplayed = 0;
		$thisSetting = "Award";
		$thisTitle = $eReportCard['Template']['AwardCh']." ".$eReportCard['Template']['AwardEn'];
		if ($ReportType == "T")
		{
			$thisValue = ($StudentID)? $ary[0][$thisSetting] : $this->EmptySymbol;
		}
		else
		{
			$thisValue = ($StudentID)? $ary[$latestTerm-1][$thisSetting] : $this->EmptySymbol;
		}		
		$x .= "<tr>";
			$x .= "<td class='border_top tabletext' height='{$LineHeight}' >&nbsp;&nbsp;<b>". $thisTitle ."</b></td>";
		$x .= "</tr>";
				
		if (is_array($thisValue))
		{
			for ($i=0; $i<sizeof($thisValue); $i++)
			{
				$thisDisplay = $thisValue[$i];
				
				if ($thisDisplay == "")
					continue;
				 
				$x .= "<tr><td class='tabletext ' height='{$LineHeight}' >&nbsp;&nbsp;". $thisDisplay ."</td></tr>";
				$countDisplayed++;
			}
		}
		else
		{
			if ($thisValue != "")
			{
				$x .= "<tr><td class='tabletext ' height='{$LineHeight}' >&nbsp;&nbsp;". $thisValue ."</td></tr>";
				$countDisplayed++;
			}
		}
		for ($i=0; $i<($MaxNumRow-$countDisplayed); $i++)
		{
			$x .= "<tr><td class='tabletext ' height='{$LineHeight}' >&nbsp;&nbsp;</td></tr>";
		}
		
		
		# Punishment
		$MaxNumRow = 4;
		$thisSetting = "Punishment";
		$thisTitle = $eReportCard['Template']['PunishmentCh']." ".$eReportCard['Template']['PunishmentEn'];
		$x .= "<tr>";
			$x .= "<td class='border_top tabletext' height='{$LineHeight}' >&nbsp;&nbsp;<b>". $thisTitle ."</b></td>";
		$x .= "</tr>";
		
		# 1st term
		$countDisplayed = 0;
		$thisTitle = $eReportCard['Template']['1stTerm']." :";
		$x .= "<tr>";
			$x .= "<td class='remark_text' height='{$LineHeight}' >&nbsp;&nbsp;<b>". $thisTitle ."</b></td>";
		$x .= "</tr>";
		if ($ReportType == "T")
		{
			$thisValue = ($StudentID)? $ary[0][$thisSetting] : $this->EmptySymbol;
		}
		else
		{
			$thisValue = ($StudentID)? $ary[$latestTerm-2][$thisSetting] : $this->EmptySymbol;
		}
		if (is_array($thisValue))
		{
			for ($i=0; $i<sizeof($thisValue); $i++)
			{
				$thisDisplay = $thisValue[$i];
				
				if ($thisDisplay == "")
					continue;
				 
				$x .= "<tr><td class='remark_text' height='{$LineHeight}' >&nbsp;&nbsp;". $thisDisplay ."</td></tr>";
				$countDisplayed++;
			}
		}
		else
		{
			if ($thisValue != "")
			{
				$x .= "<tr><td class='remark_text' height='{$LineHeight}' >&nbsp;&nbsp;". $thisValue ."</td></tr>";
				$countDisplayed++;
			}
		}
		for ($i=0; $i<($MaxNumRow-$countDisplayed); $i++)
		{
			$x .= "<tr><td class='tabletext ' height='{$LineHeight}' >&nbsp;&nbsp;</td></tr>";
		}
		# 2nd term
		$countDisplayed = 0;
		$thisTitle = $eReportCard['Template']['2ndTerm']." :";
		$x .= "<tr>";
			$x .= "<td class='remark_text' height='{$LineHeight}' >&nbsp;&nbsp;<b>". $thisTitle ."</b></td>";
		$x .= "</tr>";
		if ($ReportType == "T")
		{
			$thisValue = "";
		}
		else
		{
			$thisValue = ($StudentID)? $ary[$latestTerm-1][$thisSetting] : $this->EmptySymbol;
		}
		if (is_array($thisValue))
		{
			for ($i=0; $i<sizeof($thisValue); $i++)
			{
				$thisDisplay = $thisValue[$i];
				
				if ($thisDisplay == "")
					continue;
				 
				$x .= "<tr><td class='remark_text' height='{$LineHeight}' >&nbsp;&nbsp;". $thisDisplay ."</td></tr>";
				$countDisplayed++;
			}
		}
		else
		{
			if ($thisValue != "")
			{
				$x .= "<tr><td class='remark_text' height='{$LineHeight}' >&nbsp;&nbsp;". $thisValue ."</td></tr>";
				$countDisplayed++;
			}
		}
		for ($i=0; $i<($MaxNumRow-$countDisplayed); $i++)
		{
			$x .= "<tr><td class='tabletext ' height='{$LineHeight}' >&nbsp;&nbsp;</td></tr>";
		}
		
		# Remarks (but the content is class teacher comment)
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$classteachercomment = $CommentAry[0];
		$MaxNumRow = 3;
		$countDisplayed = 0;
		$thisSetting = "Remark";
		$thisTitle = $eReportCard['Template']['RemarkCh']." ".$eReportCard['Template']['RemarkEn'];
		$thisValue = ($StudentID)? $classteachercomment : $this->EmptySymbol;
				
		$x .= "<tr>";
			$x .= "<td class='border_top tabletext' height='{$LineHeight}' >&nbsp;&nbsp;<b>". $thisTitle ."</b></td>";
		$x .= "</tr>";
				
		if($AllowClassTeacherComment)
		{
			if (is_array($thisValue))
			{
				for ($i=0; $i<sizeof($thisValue); $i++)
				{
					$thisDisplay = $thisValue[$i];
					
					if ($thisDisplay == "")
						continue;
					 
					$x .= "<tr><td class='tabletext ' height='{$LineHeight}' >&nbsp;&nbsp;". $thisDisplay ."</td></tr>";
					$countDisplayed++;
				}
			}
			else
			{
				if ($thisValue != "")
				{
					$x .= "<tr><td class='tabletext ' height='{$LineHeight}' >&nbsp;&nbsp;". $thisValue ."</td></tr>";
					$countDisplayed++;
				}
			}
		}
		
		for ($i=0; $i<($MaxNumRow-$countDisplayed); $i++)
		{
			$x .= "<tr><td class='tabletext ' height='{$LineHeight}' >&nbsp;&nbsp;</td></tr>";
		}
		
		if ($ReportType == "W")
		{
			# Promotion
			$x .= "<tr>";
				$x .= "<td class='border_top tabletext' height='{$LineHeight}' >";
					$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" valgin='top'>";
						$x .= "<tr>";
							# Promoted to
							$thisTitle = $eReportCard['Template']['PromotedToCh']." ".$eReportCard['Template']['PromotedToEn']." ";
							$thisValue = "";
							$x .= "<td class='tabletext' height='{$LineHeight}' >";
							$x .= "&nbsp;&nbsp;<b>".$thisTitle."</b>";
							$x .= $thisValue;
							$x .= "</td>";
							# Promoted on trial
							$thisTitle = $eReportCard['Template']['PromotedOnTrialCh']." ".$eReportCard['Template']['PromotedOnTrialEn']." ";
							$thisValue = "";
							$x .= "<td class='tabletext' height='{$LineHeight}' >";
							$x .= "&nbsp;&nbsp;<b>/ ".$thisTitle."</b>";
							$x .= $thisValue;
							$x .= "</td>";
							# To repeat
							$thisTitle = $eReportCard['Template']['ToRepeatCh']." ".$eReportCard['Template']['ToRepeatEn']." ";
							$thisValue = "";
							$x .= "<td class='tabletext' height='{$LineHeight}' >";
							$x .= "&nbsp;&nbsp;<b>/ ".$thisTitle."</b>";
							$x .= $thisValue;
							$x .= "</td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
			$x .= "</tr>";
			
		}
		
		$x .= "</table>";
		
		
		# Date of Issue
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\" valgin='top'>";
		$x .= "<tr><td class='tabletext'><b>&nbsp;&nbsp;";
		$x .= $eReportCard['Template']['DateOfIssueCh']." ".$eReportCard['Template']['DateOfIssueEn'].": ".$DateOfIssue;
		$x .= "</b></td></tr>";
		$x .= "</table>";
		
		return $x;
	}
	
	function GET_ATTENDANCE_CONDUCT_TABLE($ReportID, $StudentID)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting	= $this->returnReportTemplateBasicInfo($ReportID); 
		$ClassLevelID	= $ReportSetting['ClassLevelID'];
		$SemID			= $ReportSetting['Semester'];
 		$ReportType		= $SemID == "F" ? "W" : "T";
 		
 		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
 		
 		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
 		
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		
		if($SemID=="F")
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$InfoTermID = $TermID+1;
				
				if(!empty($csvType))
				{
					foreach($csvType as $k=>$Type)
					{
						$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
						if(!empty($csvData)) 
						{
							foreach($csvData as $RegNo=>$data)
							{
								if($RegNo == $WebSamsRegNo)
								{
									foreach($data as $key=>$val)
										$ary[$TermID][$key] = $val;
								}
							}
						}
					}
				}
			}
			
			# calculate sems/assesment col#
			$ColNum2Ary = array();
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				if($d1['IsDetails']==1)
				{
					# check sems/assesment col#
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1;
				}
				else
					$ColNum2Ary[$TermID] = 0;
			}
		}
		else
		{
			$InfoTermID = $SemID+1;
			
			if(!empty($csvType))
			{
				foreach($csvType as $k=>$Type)
				{
					$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
					if(!empty($csvData)) 
					{
						foreach($csvData as $RegNo=>$data)
						{
							if($RegNo == $WebSamsRegNo)
							{
								foreach($data as $key=>$val)
									$ary[$SemID][$key] = $val;
							}
						}
					}
				}
			}
		}
	
		$x = "";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\" class='report_border'>";
		
		# Title
		$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}' width='60%'>&nbsp;&nbsp;<b>".$eReportCard['Template']['AttendanceAndConduct']."</b></td>";
			$x .= "<td class='tabletext border_left {$border_top}' height='{$LineHeight}' align='center'><b>".$eReportCard['Template']['1stTerm']."</b></td>";
			$x .= "<td class='tabletext border_left {$border_top}' height='{$LineHeight}' align='center'><b>".$eReportCard['Template']['2ndTerm']."</b></td>";
		$x .= "</tr>";
		
		
		# Total Number of School Days
		$thisSetting = "Total Number of School Days";
		$thisTitle = $eReportCard['Template']['NumSchoolDayCh']." ".$eReportCard['Template']['NumSchoolDayEn'];
		$x .= "<tr>";
		$x .= "<td class='tabletext border_top' height='{$LineHeight}' >&nbsp;&nbsp;". $thisTitle ."</td>";
		
		if($ReportType=="W")	# Whole Year Report
		{
			$thisTotal = "--";
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$thisValue = $StudentID ? ($ary[$TermID][$thisSetting] ? $ary[$TermID][$thisSetting] : $this->EmptySymbol) : "#";
				if (is_numeric($thisValue)) {
					if ($thisTotal == "")
						$thisTotal = $thisValue;
					else if (is_numeric($thisTotal))
						$thisTotal += $thisValue;
				}
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext border_top border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				$x .= "<td class='tabletext border_top border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			}
			
			if($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisTotal."</td>";
		}
		else				# Term Report
		{
			$thisValue = $StudentID ? ($ary[$SemID][$thisSetting] ? $ary[$SemID][$thisSetting] : $this->EmptySymbol) : "#";
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext border_top border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			$x .= "<td class='tabletext border_top border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			$x .= "<td class='tabletext border_top border_left' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
		}
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		# Absent
		$thisSetting = "Days Absent";
		$thisTitle = $eReportCard['Template']['DaysAbsentCh']." ".$eReportCard['Template']['DaysAbsentEn'];
		$x .= "<tr>";
		$x .= "<td class='tabletext' height='{$LineHeight}' >&nbsp;&nbsp;". $thisTitle ."</td>";
		
		if($ReportType=="W")	# Whole Year Report
		{
			$thisTotal = "--";
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$thisValue = $StudentID ? ($ary[$TermID][$thisSetting] ? $ary[$TermID][$thisSetting] : $this->EmptySymbol) : "#";
				if (is_numeric($thisValue)) {
					if ($thisTotal == "")
						$thisTotal = $thisValue;
					else if (is_numeric($thisTotal))
						$thisTotal += $thisValue;
				}
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			}
			
			if($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisTotal."</td>";
		}
		else				# Term Report
		{
			$thisValue = $StudentID ? ($ary[$SemID][$thisSetting] ? $ary[$SemID][$thisSetting] : $this->EmptySymbol) : "#";
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
		}
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		# Late
		$thisSetting = "Time Late";
		$thisTitle = $eReportCard['Template']['TimesLateCh']." ".$eReportCard['Template']['TimesLateEn'];
		$x .= "<tr>";
		$x .= "<td class='tabletext' height='{$LineHeight}' >&nbsp;&nbsp;". $thisTitle ."</td>";
		
		if($ReportType=="W")	# Whole Year Report
		{
			$thisTotal = "--";
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$thisValue = $StudentID ? ($ary[$TermID][$thisSetting] ? $ary[$TermID][$thisSetting] : $this->EmptySymbol) : "#";
				if (is_numeric($thisValue)) {
					if ($thisTotal == "")
						$thisTotal = $thisValue;
					else if (is_numeric($thisTotal))
						$thisTotal += $thisValue;
				}
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			}
			
			if($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisTotal."</td>";
		}
		else				# Term Report
		{
			$thisValue = $StudentID ? ($ary[$SemID][$thisSetting] ? $ary[$SemID][$thisSetting] : $this->EmptySymbol) : "#";
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
		}
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		# Early Leave
		$thisSetting = "Early Leave";
		$thisTitle = $eReportCard['Template']['EarlyLeaveCh']." ".$eReportCard['Template']['EarlyLeaveEn'];
		$x .= "<tr>";
		$x .= "<td class='tabletext' height='{$LineHeight}' >&nbsp;&nbsp;". $thisTitle ."</td>";
		
		if($ReportType=="W")	# Whole Year Report
		{
			$thisTotal = "--";
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$thisValue = $StudentID ? ($ary[$TermID][$thisSetting] ? $ary[$TermID][$thisSetting] : $this->EmptySymbol) : "#";
				if (is_numeric($thisValue)) {
					if ($thisTotal == "")
						$thisTotal = $thisValue;
					else if (is_numeric($thisTotal))
						$thisTotal += $thisValue;
				}
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			}
			
			if($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisTotal."</td>";
		}
		else				# Term Report
		{
			$thisValue = $StudentID ? ($ary[$SemID][$thisSetting] ? $ary[$SemID][$thisSetting] : $this->EmptySymbol) : "#";
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
		}
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		# Conduct
		$thisSetting = "Conduct";
		$thisTitle = $eReportCard['Template']['ConductCh']." ".$eReportCard['Template']['ConductEn'];
		$x .= "<tr>";
		$x .= "<td class='tabletext' height='{$LineHeight}' >&nbsp;&nbsp;". $thisTitle ."</td>";
		
		if($ReportType=="W")	# Whole Year Report
		{
			$thisTotal = "--";
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$thisValue = $StudentID ? ($ary[$TermID][$thisSetting] ? $ary[$TermID][$thisSetting] : $this->EmptySymbol) : "#";
				if (is_numeric($thisValue)) {
					if ($thisTotal == "")
						$thisTotal = $thisValue;
					else if (is_numeric($thisTotal))
						$thisTotal += $thisValue;
				}
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			}
			
			if($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisTotal."</td>";
		}
		else				# Term Report
		{
			$thisValue = $StudentID ? ($ary[$SemID][$thisSetting] ? $ary[$SemID][$thisSetting] : $this->EmptySymbol) : "#";
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
		}
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		$x .= "</table>";
		
		return $x;
	}
	
	########### END Template Related

	function ADD_CHINESE_TITLE($Title)
	{
		$thisTitle = strtoupper(trim($Title));
		
		switch ($thisTitle)
		{
			case "HALF-YEAR EXAM.":
				$Title .= "<br>上學期考試";
				break;
			case "FINAL EXAM.":
				$Title .= "<br>學年考試";
				break;
			case "TEST 1":
				$Title .= "<br>測驗1";
				break;
		}
		
		return $Title;
	}
}
?>