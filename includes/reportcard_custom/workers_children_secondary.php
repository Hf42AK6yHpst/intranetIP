<?php
# Editing by 

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");
include_once($intranet_root."/includes/libuser.php");
//include_once($intranet_root."/includes/form_class_manage.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary","attendance","remark");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "--";
		$this->LongSpace = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		$this->ShortSpace = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		$this->OneSpace = "&nbsp;";
		
		
		$this->MeritTypeArr['Praise'] = 1;
		$this->MeritTypeArr['GoodPoint'] = 2;
		$this->MeritTypeArr['MinorMerit'] = 3;
		$this->MeritTypeArr['MajorMerit'] = 4;
		
		$this->MeritTypeArr['Warning'] = -1;
		$this->MeritTypeArr['BlackMark'] = -2;
		$this->MeritTypeArr['MinorDemerit'] = -3;
		$this->MeritTypeArr['MajorDemerit'] = -4;
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='') {
		global $eReportCard;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$IsMainReport 	= $ReportSetting['isMainReport'];
		
		$ImgHeader = "";
		if($IsMainReport)  //if it is main report, don't show the school header image
		{ 
			$ImgHeader = "<tr><td align='center'>".$this->Get_Empty_Image('70px')."</td></tr>";
		}
		else {
			$ImgHeader = "<tr><td align='center'>".$TitleTable."</td></tr>";
		}
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
		$TableTop .= $ImgHeader;
		$TableTop .= "<tr><td align='center'>".$MSTable."</td></tr>";
		$TableTop .= "</table>";
		
		$sign_html='';
		$sign_html .=$this->getSignaturePartTable($StudentID,$ReportID,$ReportSetting);	
		
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'>";
				$x .= "<tr valign='top' height='930px'><td>".$TableTop."</td></tr>"; 
				$x .= "<tr valign='bottom'><td>".$sign_html."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";

		return $x;

	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		
		$imgFile = get_website().'/file/reportcard2008/templates/workers_children_secondary.jpg';
		
		$HeaderImage = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:510px;">' : '&nbsp;';
		
		$x = '';
		$x .='<table class="" width="100%" align="center">';
			$x .= '<tr>';
				$x .= '<td align="right">'."$HeaderImage".'</td>'."\n";
				$x .= '<td width="80px">'."&nbsp".'</td>'."\n";
			$x .= '</tr>'."\n";	
		$x .= '</table>'."\n";	
		return $x;
	}

	
	function getMSTable($ReportID, $StudentID='')
	{
		
		global $eReportCard, $PATH_WRT_ROOT;	
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php'); 
		
		$x='';
		
		$mainReportID = $ReportID;
		
		$mainReportInfoArr = $this->returnReportTemplateBasicInfo($mainReportID);
		$IsMainReport = $mainReportInfoArr['isMainReport'];
		$mainYearTermID = $mainReportInfoArr['Semester'];
		$mainReportType = ($mainYearTermID=='F')? 'W' : 'T';
		
		$OverallPositionRangeClass 	= $mainReportInfoArr['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $mainReportInfoArr['OverallPositionRangeForm'];
		
		### Report Title
		$ReportTitle =  $mainReportInfoArr['ReportTitle'];
		$ReportTitle = str_replace(":_:", "<br>", $ReportTitle); 
			
		### Semester Title
		if(!$IsMainReport)
		{
			$TermTitleCh = $this->returnSemesters($mainYearTermID,'ch');
			$SemTitle = $TermTitleCh.$eReportCard['Template']['semTitleCh'];
			$SemChTitle = '<tr><td class="TextColorRed">'.$SemTitle.'</td></tr>';
		}

		$ClassLevelID = $mainReportInfoArr['ClassLevelID'];
	
		$Top_html ='<table class="font_12pt" style="width:100%">
						'.$SemChTitle.'
						<tr><td class="TextColorRed">'.$ReportTitle.'</td></tr>
					</table>';


	
		$SubjectWeightDataArr = $this->returnReportTemplateSubjectWeightData($mainReportID, $other_condition="", $SubjectIDArr='', $ReportColumnID=0);
		$SubjectWeightDataAssoArr = BuildMultiKeyAssoc($SubjectWeightDataArr, 'SubjectID', array('Weight'), $SingleValue=1, $BuildNumericArray=0);
		$ClassLevelID = $mainReportInfoArr['ClassLevelID'];

		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode=1);
			
		### student Info html
		$StudentInfo_html= $this->getStudentInfo_html($mainReportID, $StudentID,$mainReportInfoArr);
		
		### Get subject full mark
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 1, $mainReportID);	
	
		### Get All CSV Data
		$OtherInfoDataArr = $this->getReportOtherInfoData($mainReportID, $StudentID);	
		
		### Get Subject Grading Scheme Info
		$GradingSchemeInfoArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, '', $withGrandResult=0, $returnAsso=1, $ReportID);

		
		### Get Subject Statistics Info   
		$MainSubjectInfoArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='en', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		$MainSubjectInfoArrChi = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='ch', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		$MainSubjectIDArr = array_keys($MainSubjectInfoArr);
		
		$AllReportInfoArr = array();
		if ($mainReportType == 'T') {
			$AllReportInfoArr[0]['ReportID'] = $mainReportID;
			$AllReportInfoArr[0]['YearTermID'] = $mainYearTermID;
			$SemTwoReportID = $mainReportID;
		}
		else {
			$ColumnData = $this->returnReportTemplateColumnData($mainReportID);
					
			for($i=0,$i_MAX=count($ColumnData);$i<$i_MAX;$i++)
			{
				//	$thisReportColumnID = $ColumnData[$i]["ReportColumnID"];
				$thisReportColumnID = 0;
				
				$thisYearTermID = $ColumnData[$i]['SemesterNum'];
				$thisReport = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $thisYearTermID, $isMainReport=1);
				//$ReportIDArr[$thisYearTermID] = $thisReport['ReportID'];
				
				$AllReportInfoArr[$i]['ReportID'] = $thisReport['ReportID'];
				$AllReportInfoArr[$i]['YearTermID'] = $thisYearTermID;
	
				if($i==1)
				{
					$SemTwoReportID =$thisReport['ReportID'];
				}
				else if($i==0)
				{
					$SemOneReportID =$thisReport['ReportID'];
				}
			}
			
			$numOfReport = count($AllReportInfoArr);
			$AllReportInfoArr[$numOfReport]['ReportID'] = $mainReportID;
			$AllReportInfoArr[$numOfReport]['YearTermID'] = 0;
		}
		$numOfReport = count($AllReportInfoArr);
		
		### Get Data
		$ReportIDArr = Get_Array_By_Key($AllReportInfoArr,'ReportID');
		
		//2012-0207-1434-51132
		//$thisConductArr = $this->Get_eDiscipline_Conduct_Data($ReportIDArr, $StudentID);
		
		
		$thisClassTeacherCommentArr=array();
		for ($i=0,$i_MAX=count($AllReportInfoArr); $i<$i_MAX; $i++) {
			
			$thisReportID = $AllReportInfoArr[$i]['ReportID'];
			$thisYearTermID = $AllReportInfoArr[$i]['YearTermID'];
		
			$thisGrandMarkArr = $this->getReportResultScore($thisReportID, 0, $StudentID);
						
							
			### Get Student Average Marks and Position
			$MarksAry[$thisReportID] = $this->getMarks($thisReportID, $StudentID,"","",1);

			$thisAverage = $thisGrandMarkArr['GrandAverage'];
			$thisClassPosition = $thisGrandMarkArr['OrderMeritClass'];
			$thisFormPosition = $thisGrandMarkArr['OrderMeritForm'];
			$thisFormNumOfStudent = $thisGrandMarkArr['FormNoOfStudent'];
			$thisClassNumOfStudent = $thisGrandMarkArr['ClassNoOfStudent'];
			
			//$thisAverageStr = ($thisAverage=='')? $this->EmptySymbol : $thisAverage;
			$thisAverageStr = ($thisAverage=='')? $this->EmptySymbol : $this->Get_Score_Display_HTML($thisAverage, $thisReportID, $ClassLevelID, '', '', 'GrandTotal');
			
			$thisClassPosition= ($thisClassPosition=='')? $this->EmptySymbol : $thisClassPosition;	
			$thisFormPosition = ($thisFormPosition=='')? $this->EmptySymbol : $thisFormPosition;
			$thisClassNumOfStudent= ($thisClassNumOfStudent=='')? $this->EmptySymbol : $thisClassNumOfStudent;	
			$thisFormNumOfStudent= ($thisFormNumOfStudent=='')? $this->EmptySymbol : $thisFormNumOfStudent;			
			
			$PositionArr[$thisReportID]['Average'] = ($thisAverageStr=='')? $this->EmptySymbol : $thisAverageStr;
		
			$thisClassPosition = ($thisClassPosition=='')? $this->EmptySymbol : $thisClassPosition;
			$PositionArr[$thisReportID]['ClassPosition'][] = $thisClassPosition;
			$PositionArr[$thisReportID]['ClassPosition'][] = $thisClassNumOfStudent;
				
			$thisFormPosition = ($thisFormPosition=='')? $this->EmptySymbol : $thisFormPosition;
			$PositionArr[$thisReportID]['FormPosition'][]= $thisFormPosition;
			$PositionArr[$thisReportID]['FormPosition'][]= $thisFormNumOfStudent;
		
			### Get Student csv data
			$thisOtherInfoArr = $OtherInfoDataArr[$StudentID][$thisYearTermID];	

			$Conduct = $thisOtherInfoArr["Conduct"];
			$PersonalLeave = $thisOtherInfoArr["Personal Leave"];
			$SickLeave = $thisOtherInfoArr["Sick Leave"];
			$Late = $thisOtherInfoArr["Late"];
			$Absence_Leave = $thisOtherInfoArr["Absence w/o Leave"];								
			
			if($thisYearTermID==0)
			{
				$CSV_Promotion = $thisOtherInfoArr["Promotion"];
				$CSV_PromotionStr = ($CSV_Promotion=='')?$this->EmptySymbol:$CSV_Promotion;
				
				if(is_array($CSV_Promotion))
				{
					$CSV_PromotionStr='';
					$break_html = '';
					for($k=0,$k_MAX=count($CSV_Promotion);$k<$k_MAX;$k++)
					{
						$CSV_PromotionStr .=$break_html.$CSV_Promotion[$k];
						$break_html = '<br/>';
					}
				}
				
			}
						
			$CSVInfoArr['PersonalLeave'][$thisReportID] = ($PersonalLeave=='')? 0:$PersonalLeave;
			$CSVInfoArr['SickLeave'][$thisReportID] = ($SickLeave=='')? 0:$SickLeave;
			$CSVInfoArr['Late'][$thisReportID] = ($Late=='')? 0:$Late;
			$CSVInfoArr['AbsenceLeave'][$thisReportID] = ($Absence_Leave=='')? 0:$Absence_Leave;
			
			### Conduct			
			$Conduct_YearTermID = ($thisYearTermID=='F')? 0 : $thisYearTermID;
			//2012-0207-1434-51132
			//$ConductArr[$thisReportID] = $thisConductArr[$StudentID][$Conduct_YearTermID]['Conduct'];
			$ConductArr[$thisReportID] = $Conduct;
			$ConductArr[$thisReportID] = ($ConductArr[$thisReportID]=='')?$this->EmptySymbol:$ConductArr[$thisReportID];
			
			### Get eDiscipline Data							
			$eDisciplineDataArr[$thisReportID] = $this->Get_Student_Profile_Data($thisReportID, $StudentID, $MapByRecordType=true, $CheckRecordStatus=false, $returnTotalUnit=true);
			
			### Get Class Teacher Comment	
			
			if($thisYearTermID!=0)
			{
				$thisCommentArr = $this->returnSubjectTeacherCommentByBatch($thisReportID, $StudentID);
				$thisClassTeacherCommentArr[$thisReportID] = nl2br(trim($thisCommentArr[0][$StudentID]['Comment']));
				$thisClassTeacherCommentArr[$thisReportID] = ($thisClassTeacherCommentArr[$thisReportID]=='')?$this->EmptySymbol:$thisClassTeacherCommentArr[$thisReportID];
			}
					
		}
		
		
		### Reorder the Subject Display Order
		$NewSubjectSequenceArr = array();		
		$PercentailArr = array();
		$SubjectObjArr = array(); 
	
		foreach((array)$MainSubjectInfoArr as $thisParentSubjectID => $thisSubjectInfoArr)
		{

			foreach((array)$thisSubjectInfoArr as $thisSubjectID => $thisSubjectName)
			{ 
				if ($thisSubjectID==0) {
					$thisIsComponentSubject = false;
					$thisSubjectID = $thisParentSubjectID;
				}
				else {
					$thisIsComponentSubject = true;
					// continue;  // if you do not want to show the component subject, just uncomment it.
				}
				
				$thisScaleInput = $GradingSchemeInfoArr[$thisSubjectID]['ScaleInput'];
				$thisScaleDisplay = $GradingSchemeInfoArr[$thisSubjectID]['ScaleDisplay'];
				
				for ($i=0,$i_MAX=count($AllReportInfoArr); $i<$i_MAX; $i++) {
			
					$thisReportID = $AllReportInfoArr[$i]['ReportID'];
					$thisYearTermID = $AllReportInfoArr[$i]['YearTermID'];
			
					$ReportColumnID = 0;					
					$thisMSGrade = $MarksAry[$thisReportID][$thisSubjectID][$ReportColumnID]["Grade"];
					$thisMSMark = $MarksAry[$thisReportID][$thisSubjectID][$ReportColumnID]["Mark"]; 

					$thisGrade = ($thisScaleDisplay=="G" || $thisScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($thisScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";

					$thisMark = ($thisScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					$thisSubjectPosition = $MarksAry[$thisReportID][$thisSubjectID][$ReportColumnID]['OrderMeritForm'];
					$thisSubjectStdTotal = $MarksAry[$thisReportID][$thisSubjectID][$ReportColumnID]['FormNoOfStudent'];

					if ($StudentID) {

				//		if ($thisMark == 'N.A.') {
						if ($thisMark == 'N.A.' || $thisMark=='') {
				
							continue;
						}	

		
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisSubjectID, $thisMark, $thisMSGrade);
						if($needStyle) {
							$thisNatureDetermineScore = ($thisScaleDisplay=='G')? $thisMark : $thisMSMark;
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $thisSubjectID, $thisNatureDetermineScore);
						}
						else {
							$thisMarkDisplay = $thisMark;
						}						
					}
					else {
						$thisMarkDisplay = $this->EmptySymbol;
					}					
		
					$NewSubjectSequenceArr[$thisSubjectID]['thisIsComponentSubject']=$thisIsComponentSubject;
					if($thisIsComponentSubject)
					{		
						if($thisYearTermID==0)
						{
							$PercentailArr[$thisSubjectID]['Percentile'] = $this->OneSpace;	
						}									
						$NewSubjectSequenceArr[$thisSubjectID][$thisReportID]['Mark'] = $this->LongSpace.$thisMarkDisplay;
					}
					else
					{
						if($thisYearTermID==0)
						{
							$thisSubjectFullMark = $SubjectFullMarkAry[$thisSubjectID];
							$thisPercentile= $this->getPercentile($thisSubjectPosition,$thisSubjectStdTotal);
							$PercentailArr[$thisSubjectID]['Percentile'] = ($thisPercentile=='')? $this->OneSpace:$thisPercentile;
						}
						
						
						$NewSubjectSequenceArr[$thisSubjectID][$thisReportID]['Mark']= $thisMarkDisplay;
					}
					
					if($thisIsComponentSubject)
					{
						$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'] = $this->LongSpace.$MainSubjectInfoArr[$thisParentSubjectID][$thisSubjectID];
						$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameCh'] = $this->LongSpace.$MainSubjectInfoArrChi[$thisParentSubjectID][$thisSubjectID];					
					}
					else
					{						
						$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'] = $this->ShortSpace.$MainSubjectInfoArr[$thisSubjectID][0];
						$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameCh'] = $this->ShortSpace.$MainSubjectInfoArrChi[$thisSubjectID][0];
					}
								
					
					
				}

			}
		}
	
		/******************  html template **********************/
		
		$font_size_class = 'font_9pt';
		
		### Column Width		
		$colWidth_html='';
		if($IsMainReport && $mainReportType=='W') //whole report
		{
	//		$font_size_class = 'font_8pt';
			$cellpadding = "";
			
			$colWidth_html .= '<col width="25%" />
							   <col width="38%" />
							   <col width="9%" />
							   <col width="9%" />
							   <col width="9%" />
						       <col width="10%" />'."\n";				
		}
		else  //Term Report and Extra Report
		{
			$colWidth_html .= '<col width="30%" />
							   <col width="45%" />
							   <col width="25%" />'."\n";
		}		

		### Percentile Space
		$PercentSpace_html = '';
		if($IsMainReport && $mainReportType=='W') //whole report
		{
			$PercentSpace_html='<td class=" border_left ">'.$this->OneSpace.'</td>'."\n";
		}
		
		### Term Report Tearcher Comment
		$TermReportComment_html ='';

		if($IsMainReport && $mainReportType=='T') //Term report
		{		
			
			$cellpadding = "1";
			$TermReportComment_html = '<table  cellpadding="3" cellspacing="0" class="font_9pt " style="width:100%;" align="left">
										<col width="17%" />
										<col width="83%" />	
											<tr>
												<td class="" style="text-align:left;vertical-align:text-top;">'.'&nbsp;'.'</td>	
											</tr>	
											<tr>
												<td class="" style="text-align:left; vertical-align:text-top; padding-right:5px;">'.$eReportCard['Template']['CommentCh'].' '.$eReportCard['Template']['CommentEn'].':'.'</td>
												<td class="" style="text-align:left;">'.$thisClassTeacherCommentArr[$mainReportID].'</td>					
					   						</tr>
				   						</table>'."\n";
		}
			   							
		### Whole Year Report Tearcher Comment		
		$WholeYearComment_html ='';
		if($IsMainReport && $mainReportType=='W') //whole report
		{
			$Sem1_Comment = $thisClassTeacherCommentArr[$SemOneReportID];
			$Sem1_Comment = ($Sem1_Comment=='')? $this->EmptySymbol:$Sem1_Comment;
			$Sem2_Comment = $thisClassTeacherCommentArr[$SemTwoReportID];
			$Sem2_Comment = ($Sem2_Comment=='')? $this->EmptySymbol:$Sem2_Comment;
			

		$WholeYearComment_html .= '<table  cellpadding="3" cellspacing="0" class="font_9pt " style="width:100%;" align="left">
										<col width="19%" />
										<col width="81%" />	
													
										<tr style="text-align:center;">
											<td class="" style="text-align:left;vertical-align:text-top;"><br/>'.$eReportCard['Template']['CommentCh'].' '.$eReportCard['Template']['CommentEn'].'</td>
										</tr>
										<tr style="text-align:center;">
											<td class="" style="text-align:left;vertical-align:text-top;">'.$this->ShortSpace.$eReportCard['Template']['FirstSemCh'].' '.$eReportCard['Template']['FirstSemEn'].":".'</td>
											<td class="" style="text-align:left;vertical-align:text-top;">'.$Sem1_Comment.'</td>	
										</tr>
										<tr style="text-align:center;">
											<td class="" style="text-align:left;vertical-align:text-top;">'.$this->ShortSpace.$eReportCard['Template']['SecondSemCh'].' '.$eReportCard['Template']['SecondSemEn'].":".'</td>
											<td class="" style="text-align:left;vertical-align:text-top;">'.$Sem2_Comment.'</td>	
										</tr>								
										<tr style="text-align:center;">
											<td class="" style="text-align:left;vertical-align:text-top;">'.$eReportCard['Template']['PromotionCh'].' '.$eReportCard['Template']['PromotionEn'].":".'</td>			
											<td class="" style="text-align:left;vertical-align:text-top;">'.$CSV_PromotionStr.'</td>				
										</tr>
								  </table>'."\n";	
		}
		
		### Extra Report Remark	
		$ApplicableFormArr = array(1, 2, 3);
		if(in_array($FormNumber,$ApplicableFormArr))
		{
			$remark_line1 = $eReportCard['Template']['RemarkArr']['line1']['S1toS3_Ch'];
		}
		else
		{
			$remark_line1 = $eReportCard['Template']['RemarkArr']['line1']['S4toS6_Ch'];
		}
		$ExtraReportRemark_html='';
		if(!$IsMainReport) //Extra report
		{
			$cellpadding = "3";
			$ExtraReportRemark_html = '<table  cellpadding="3" cellspacing="0" class="font_9pt" style="width:50%;" align="left">
											<tr colspan="2" style="text-align:center;">	
												<td class="" style="text-align:left;">'.$eReportCard['Template']['RemarkCh'].":".'</td>			
											</tr>	
										
											<tr style="text-align:center;">	
												<td class="" style="text-align:left;">'.'(1)'.'</td>	
												<td class="" style="text-align:left;">'.$remark_line1.'</td>			
											</tr>		
										
											<tr style="text-align:center;">	
												<td class="" style="text-align:left;">'.'(2)'.'</td>			
												<td class="" style="text-align:left;">'.$eReportCard['Template']['RemarkArr']['line2_Ch'].'</td>					
											</tr>		
										</table>'."\n";
		}
		
		### Report Template
	
		$x = '';
		$x .=$Top_html;
		$x .=$StudentInfo_html;
		$x .= '<table cellpadding="'.$cellpadding.'" cellspacing="0" class="'.$font_size_class.' report_border_left report_border_right report_border_top report_border_bottom" style="width:100%; text-align:left;">'."\n";
			$x .=$colWidth_html;
			
			$totalSubjects = count($NewSubjectSequenceArr);

			
			### Here are the "Subject", and "Marks" titles
			$x .= '<tr style="text-align:center;">'."\n";
				$x .= '<td colspan="2" class="border_bottom">';
					$x .= "<b class='TextColorRed'>".$eReportCard['Template']['SubjectsCh']."<br/>".$eReportCard['Template']['SubjectsEn']."</b>";
				$x .= '</td>'."\n";	

				$Max_count = count($AllReportInfoArr);
				
				if($IsMainReport && $mainReportType=='W') //whole report
				{
					$Max_count--;
				}
				
				for ($i=0,$i_MAX=$Max_count; $i<$i_MAX; $i++)
				{			
					$thisReportID = $AllReportInfoArr[$i]['ReportID'];
					$thisYearTermID = $AllReportInfoArr[$i]['YearTermID'];
						
					$TermTitleCh = $this->returnSemesters($thisYearTermID,'ch');
					$TermTitleEn = $this->returnSemesters($thisYearTermID,'en');
					
					$x .= '<td class="border_left border_bottom">';
						$x .= "<b class='TextColorRed'>".$TermTitleCh."<br/>".$TermTitleEn."</b>";
					$x .= '</td>'."\n";	
				}
				
				if($IsMainReport && $mainReportType=='W') //whole report
				{
					$x .= '<td class="border_left border_bottom">';
						$x .= "<b class='TextColorRed'>".$eReportCard['Template']['AnnualCh']."<br/>".$eReportCard['Template']['AnnualEn']."</b>";
					$x .= '</td>'."\n";	
					
					$x .= '<td class="border_left border_bottom">';
						$x .= "<b class='TextColorRed'>".$eReportCard['Template']['PercentileCh']."<br/>".$eReportCard['Template']['PercentileEn']."</b>";
					$x .= '</td>'."\n";	
				}
				
			$x .= '</tr>'."\n";
			
			### End the "Subject",  and "Marks" titles
		

			### Here are the "Subject", and "Marks" rows
			
			$IsSetBorderTop=false;
			foreach ((array)$NewSubjectSequenceArr as $thisSubjectID => $thisSubjectInfoArr) {

				$thisScaleInput = $GradingSchemeInfoArr[$thisSubjectID]['ScaleInput'];
								
				if($thisScaleInput=='G')
				{
					if($IsSetBorderTop)
					{	
						$BorderTop = '';
					}
					else
					{
						$BorderTop = 'border_top';
						$IsSetBorderTop=true;
					}					
				}
				else
				{
					$BorderTop='';
				}
		
				$x .= '<tr style="text-align:center;">'."\n";
					$x .= '<td class="'.$BorderTop.'" style="text-align:left; padding-right:5px;">'.$thisSubjectInfoArr['SubjectNameCh'].'</td>'."\n";
					$x .= '<td class="border_left '.$BorderTop.'" style="text-align:left; padding-right:5px;">'.$thisSubjectInfoArr['SubjectNameEn'].'</td>'."\n";

			
					for ($i=0,$i_MAX=count($AllReportInfoArr); $i<$i_MAX; $i++)
					{			
						$thisReportID = $AllReportInfoArr[$i]['ReportID'];
						$thisYearTermID = $AllReportInfoArr[$i]['YearTermID'];
						
						$thisDisplay = $thisSubjectInfoArr[$thisReportID]['Mark'];
						$thisIsComponentSubject = $thisSubjectInfoArr['thisIsComponentSubject'];
						
						$thisLongSpace = '';
						if($thisIsComponentSubject)
						{
							$thisLongSpace = $this->LongSpace;
						}
						
						$thisDisplay = ($thisDisplay=='')? $thisLongSpace.$this->EmptySymbol : $thisDisplay;					
						$x .= '<td class="border_left '.$BorderTop.'">'.$thisDisplay.'</td>'."\n";		
					}
					if($IsMainReport && $mainReportType=='W') //whole report
					{		
						$thisPercentile = $PercentailArr[$thisSubjectID]['Percentile'];
						$thisPercentile = ($thisPercentile=='')? $this->OneSpace:$thisPercentile;
						$x .= '<td class="border_left '.$BorderTop.'">'.$thisPercentile.'</td>'."\n"; 	
					}	
				$x .= '</tr>'."\n";					
				
			}
			### End the "Subject", and "Marks" rows
			
			### Other Info and CSV Data  
			
			if($IsMainReport)
			{ 
				$x .= '<tr style="text-align:center;">'."\n";
					$x .= '<td class="border_top" style="text-align:left; padding-right:5px;">'.$this->ShortSpace.$eReportCard['Template']['AverageCh'].'</td>'."\n";
					$x .= '<td class="border_top border_left" style="text-align:left; padding-right:5px;">'.$this->ShortSpace.$eReportCard['Template']['AverageEn'].'</td>'."\n";
					for ($i=0,$i_MAX=count($AllReportInfoArr); $i<$i_MAX; $i++)
					{			
						$thisReportID = $AllReportInfoArr[$i]['ReportID'];
						$thisYearTermID = $AllReportInfoArr[$i]['YearTermID'];
						
						$thisDisplay = $PositionArr[$thisReportID]['Average'];
						$thisDisplay = ($thisDisplay=='')? $this->EmptySymbol : $thisDisplay;					
						$x .= '<td class="border_top border_left '.$BorderTop.'">'.$thisDisplay.'</td>'."\n";		
					}
					if($IsMainReport && $mainReportType=='W') //whole report
					{
						$x .= '<td class="border_top border_left ">'.$this->OneSpace.'</td>'."\n";	
					}										
				$x .= '</tr>'."\n";	
			
				$x .= '<tr style="text-align:center;">'."\n";
					$x .= '<td class="" style="text-align:left; padding-right:5px;">'.$this->ShortSpace.$eReportCard['Template']['ClassPositionCh'].'</td>'."\n";
					$x .= '<td class="border_left" style="text-align:left; padding-right:5px;">'.$this->ShortSpace.$eReportCard['Template']['ClassPositionEn'].'</td>'."\n";
					for ($i=0,$i_MAX=count($AllReportInfoArr); $i<$i_MAX; $i++)
					{			
						$thisReportID = $AllReportInfoArr[$i]['ReportID'];
						$thisYearTermID = $AllReportInfoArr[$i]['YearTermID'];					
						$thisClassPosition = $PositionArr[$thisReportID]['ClassPosition'][0];
						$thisClassStd = $PositionArr[$thisReportID]['ClassPosition'][1];  

						if($OverallPositionRangeClass!=0)
						{
							$thisDisplay = ($thisClassPosition>=$OverallPositionRangeClass)? $this->EmptySymbol.'/'.$thisClassStd :$thisClassPosition.'/'.$thisClassStd;
						}
						else
						{
							$thisDisplay = $thisClassPosition.'/'.$thisClassStd;
						}
						$thisDisplay = ($thisDisplay=='')? $this->EmptySymbol : $thisDisplay;					
						$x .= '<td class=" border_left ">'.$thisDisplay.'</td>'."\n";		
					}	
					$x .= $PercentSpace_html;			
				$x .= '</tr>'."\n";	
			
				$x .= '<tr style="text-align:center;">'."\n";
					$x .= '<td class="" style="text-align:left; padding-right:5px;">'.$this->ShortSpace.$eReportCard['Template']['FormPositionCh'].'</td>'."\n";
					$x .= '<td class="border_left" style="text-align:left; padding-right:5px;">'.$this->ShortSpace.$eReportCard['Template']['FormPositionEn'].'</td>'."\n";		
					for ($i=0,$i_MAX=count($AllReportInfoArr); $i<$i_MAX; $i++)
					{			
						$thisReportID = $AllReportInfoArr[$i]['ReportID'];
						$thisYearTermID = $AllReportInfoArr[$i]['YearTermID'];			
						$thisFormPosition = $PositionArr[$thisReportID]['FormPosition'][0];
						$thisFormStd = $PositionArr[$thisReportID]['FormPosition'][1];
		
						if($OverallPositionRangeForm!=0)
						{
							$thisDisplay = ($thisFormPosition>=$OverallPositionRangeForm)? $this->EmptySymbol.'/'.$thisFormStd :$thisFormPosition.'/'.$thisFormStd;
						}
						else
						{
							$thisDisplay = $thisFormPosition.'/'.$thisFormStd;
						}
						
						$thisDisplay = ($thisDisplay=='')? $this->EmptySymbol : $thisDisplay;					
						$x .= '<td class=" border_left">'.$thisDisplay.'</td>'."\n";		
					}	
					$x .= $PercentSpace_html;	
				$x .= '</tr>'."\n";	
			
			
				foreach ((array)$CSVInfoArr as $CSV_title=>$CSV_subArr)
				{
					$TitleCh = $CSV_title.'Ch';
					$TitleEn = $CSV_title.'En';
					
					$x .= '<tr style="text-align:center;">'."\n";
						$x .= '<td class="" style="text-align:left; padding-right:5px;">'.$this->ShortSpace.$eReportCard['Template'][$TitleCh].'</td>'."\n";
						$x .= '<td class="border_left" style="text-align:left; padding-right:5px;">'.$this->ShortSpace.$eReportCard['Template'][$TitleEn].'</td>'."\n";	
						for ($i=0,$i_MAX=count($AllReportInfoArr); $i<$i_MAX; $i++)
						{			
							$thisReportID = $AllReportInfoArr[$i]['ReportID'];
							$thisYearTermID = $AllReportInfoArr[$i]['YearTermID'];
							
							$thisDisplay = $CSVInfoArr[$CSV_title][$thisReportID];
							$thisDisplay = ($thisDisplay=='')? 0: $thisDisplay;					
							$x .= '<td class=" border_left ">'.$thisDisplay.'</td>'."\n";		
						}
						$x .= $PercentSpace_html;					
					$x .= '</tr>'."\n";				
				}
		
				### End  Other Info and CSV Data
				
				
				### eDispline
				$x .= '<tr style="text-align:center;">'."\n";
						$x .= '<td class="border_bottom" style="text-align:left; padding-right:5px;">'.$this->ShortSpace.$eReportCard['Template']['ConductCh'].'</td>'."\n";
						$x .= '<td class="border_left border_bottom" style="text-align:left; padding-right:5px;">'.$this->ShortSpace.$eReportCard['Template']['ConductEn'].'</td>'."\n";
						for ($i=0,$i_MAX=count($AllReportInfoArr); $i<$i_MAX; $i++)
						{			
							$thisReportID = $AllReportInfoArr[$i]['ReportID'];
							$thisYearTermID = $AllReportInfoArr[$i]['YearTermID'];
							
						    $thisConduct = $ConductArr[$thisReportID];
							$thisConduct = ($thisConduct=='')?$this->EmptySymbol:$thisConduct;
							$x .= '<td class="border_left border_bottom">'.$thisConduct.'</td>'."\n";				
						}		
						$x .= $PercentSpace_html;			
				$x .= '</tr>'."\n";			
			
				foreach((array)$this->MeritTypeArr as $eDis_title => $thisRecordType )//bbbb
				{
					
					$TitleCh = $eDis_title.'Ch';
					$TitleEn = $eDis_title.'En';
	
					$x .= '<tr style="text-align:center;">'."\n";
							$x .= '<td class="" style="text-align:left; padding-right:5px;">'.$this->ShortSpace.$eReportCard['Template'][$TitleCh].'</td>'."\n";
							$x .= '<td class=" border_left" style="text-align:left; padding-right:5px;">'.$this->ShortSpace.$eReportCard['Template'][$TitleEn].'</td>'."\n";
							
							$overAllNumber = 0;
							for ($i=0,$i_MAX=count($AllReportInfoArr); $i<$i_MAX; $i++)
							{			
								$thisReportID = $AllReportInfoArr[$i]['ReportID'];
								$thisYearTermID = $AllReportInfoArr[$i]['YearTermID'];
											    	
						    	$thisNumber = my_round($eDisciplineDataArr[$thisReportID][$this->MeritTypeArr[$eDis_title]], 0);
								$thisNumber = ($thisNumber=='')? 0:$thisNumber;
														
								if($thisYearTermID==0)
								{
									$x .= '<td class=" border_left">'.$overAllNumber.'</td>'."\n";
								}
								else
						    	{
						    		$x .= '<td class=" border_left">'.$thisNumber.'</td>'."\n";
						    	}
						    	$overAllNumber +=$thisNumber;										
							}		
						$x .= $PercentSpace_html;						
					$x .= '</tr>'."\n";	
				}
			}
			### End eDispline					
					
					
				
		$x .= '</table>'."\n";	
			
		$x .= $TermReportComment_html;
		$x .=$WholeYearComment_html;

		$x .= $this->Get_Empty_Image('30px');
		$x .=$ExtraReportRemark_html;
		
		return $x;
	}
	
	
	
	 /*****************************************************************
      ** Here is the Signature part (Form teacher, Principal, Parent)** 
      *****************************************************************/	
	private function getSignaturePartTable($StudentID='',$ReportID,$ReportSetting)
   {
   		global $eReportCard;
   			
   		### Principal chop
   		
   		$IsMainReport 	= $ReportSetting['isMainReport'];
   		$width = '40%';
   		if(!$IsMainReport) //Extra report
		{
			$width = '40%';
		} 
		$imgFile = get_website().'/file/reportcard2008/templates/workers_children_secondary_chop.jpg';		
		$ChopImage = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:'.$width.';">' : '&nbsp;';
		
		
		### Class Teacher Name
		$ClassTeacherNameDisplay="XXX";
		$ClassTeacherInfoArr = $this->Get_Student_Class_Teacher_Info($StudentID);
		$ClassTeacherNameArr = Get_Array_By_Key($ClassTeacherInfoArr, 'ChineseName');

		$ClassTeacherNameDisplay = implode('<br />', (array)$ClassTeacherNameArr);
		$ClassTeacherNameDisplay = ($ClassTeacherNameDisplay=='')? "XXX" : $ClassTeacherNameDisplay;
		
		
		$PrincipalArr = $this->Get_Principal_Info();
		$PrincipalNameArr = Get_Array_By_Key($PrincipalArr, 'ChineseName');
		

		$PrincipalNameDisplay = implode('<br />', (array)$PrincipalNameArr);
		$PrincipalNameDisplay = ($PrincipalNameDisplay=='')? "XXX" : $PrincipalNameDisplay;
   	
   	
		$x='';
		
		$x .='<table style="width:100%;">'."\n";
			$x .='<col width="30%">';
			$x .='<col width="5%">';
			$x .='<col width="30%">';
			$x .='<col width="5%">';
			$x .='<col width="30%">';
		
			$x .= '<tr>'."\n";
				$x .='<td>'.$ChopImage.'</td>';
				$x .='<td>'."&nbsp;".'</td>';
				$x .='<td>'."&nbsp;".'</td>';
				$x .='<td>'."&nbsp;".'</td>';
				$x .='<td>'."&nbsp;".'</td>';
			$x .= '</tr>'."\n";
			
			$x .= '<tr>'."\n";
				$x .='<td class="border_top">'.$eReportCard['Template']['PrincipalCh']."&nbsp;".$eReportCard['Template']['PrincipalEn'].'</td>';
				$x .='<td>'."&nbsp;".'</td>';
				$x .='<td class="border_top">'.$eReportCard['Template']['FormTeacherCh']."&nbsp;".$eReportCard['Template']['FormTeacherEn'].'</td>';
				$x .='<td>'."&nbsp;".'</td>';
				$x .='<td class="border_top">'.$eReportCard['Template']['ParentSignCh']."&nbsp;".$eReportCard['Template']['ParentSignEn'].'</td>';
			$x .= '</tr>'."\n";
			
			$x .= '<tr >'."\n";
				$x .='<td class="TextAlignTop">'.$PrincipalNameDisplay.'</td>';
				$x .='<td>'."&nbsp;".'</td>';
				$x .='<td class="TextAlignTop">'.$ClassTeacherNameDisplay.'</td>';
				$x .='<td>'."&nbsp;".'</td>';	
				$x .='<td class="TextAlignTop">'."&nbsp;".'</td>';	
			$x .= '</tr>'."\n";
			
		$x .='</table>'."\n";
	
		return $x;
   }  
   
	private function SplitName($ParName)
	{
		$pos = strpos($ParName, '-'); 			
		if($pos==false)
		{				
		}
		else
		{
			$ParNameArr = explode("-",$ParName);
			$ParName = $ParNameArr[1];
		}
		
		return $ParName;
	}
	
	private function getStudentInfo_html($ReportID, $StudentID='',$ReportInfoArr)
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		
		### Date of Issued
		$DateOfIssue = $ReportInfoArr['Issued'];		
		if (date($DateOfIssue)==0) {
			$DateOfIssue = '';
		} else {
			$DateOfIssue = date("Y-m-d", strtotime($DateOfIssue));
		} 

		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode=1);
		
		
		### Get Student Info
		$defaultVal = ($StudentID=='')? "XXX" : $this->EmptySymbol;
		if ($StudentID) {
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			
			$StudentNameEn = $this->SplitName($StudentInfoArr[0]['EnglishName']);						
			$StudentNameCh =  $this->SplitName($StudentInfoArr[0]['ChineseName']);
						
			$STRNNo = $StudentInfoArr[0]['STRN'];		
			$ClassNameCh = $StudentInfoArr[0]['ClassNameCh'];
			$ClassNameEn = $StudentInfoArr[0]['ClassName'];
			$SexEn = $StudentInfoArr[0]['Gender'];	
			
			$ClassID = $StudentInfoArr[0]['ClassID'];	
			$ClassNumber = $StudentInfoArr[0]['ClassNumber'];	
			$ClassNumber = str_pad($ClassNumber, 2, '0', STR_PAD_LEFT);
			
			if(strtoupper($StudentInfoArr[0]['Gender'])=='M')
			{
				$SexCh=$eReportCard['Template']['GenderM_Ch'];
			}
			else if(strtoupper($StudentInfoArr[0]['Gender'])=='F')
			{
				$SexCh=$eReportCard['Template']['GenderF_Ch'];
			}

			$DateOfBirth =  $StudentInfoArr[0]['DateOfBirth']; 
	
			if (date( $DateOfBirth)==0) {
			    $DateOfBirth = '';
			} else {
			    $DateOfBirth = date("Y-m-d", strtotime($DateOfBirth));
			}  
			$WebSAMSRegNo = $StudentInfoArr[0]['WebSAMSRegNo'];
			
			$StudentClassID = $StudentInfoArr[0]['ClassID'];
			
			$Class_of_Form_no = $this->Convert_ClassOfForm_ToInt($StudentID);
			$Class_ID_No = $FormNumber.$Class_of_Form_no.$ClassNumber;
		
		}
		$StudentNameEn = ($StudentNameEn=='')?$defaultVal:strtoupper($StudentNameEn);
		$StudentNameCh = ($StudentNameCh=='')?$defaultVal:$StudentNameCh;
		$Class_ID_No = ($Class_ID_No=='')?$defaultVal:$Class_ID_No;
		$STRNNo = ($STRNNo=='')?$defaultVal:$STRNNo;	
		$ClassNameCh = ($ClassNameCh=='')?$defaultVal:$ClassNameCh;	
		$ClassNameEn = ($ClassNameEn=='')?$defaultVal:$ClassNameEn;	
		$SexEn =($SexEn=='')?$defaultVal:$SexEn;	
		$SexCh = ($SexCh=='')?$defaultVal:$SexCh;			
		$DateOfBirth =  ($DateOfBirth=='')?$defaultVal:$DateOfBirth;	 
		$DateOfIssue =  ($DateOfIssue=='')?$defaultVal:$DateOfIssue;	
		
		### student Info html
		$space='&nbsp;';
		$StudentName_html = "<b class='TextColorRed'>".$eReportCard['Template']['StudentNameCh'].$space.$eReportCard['Template']['StudentNameEn']." : "."</b>".$StudentNameCh.$space.$space.$StudentNameEn.$space."(".$Class_ID_No.")";
		$STRNNo_html = "<b class='TextColorRed'>".$eReportCard['Template']['STRNnoCh'].$space.$eReportCard['Template']['STRNnoEn']." : "."</b>".$STRNNo;
		$ClassName_html = "<b class='TextColorRed'>".$eReportCard['Template']['ClassCh'].$space.$eReportCard['Template']['ClassEn']." : "."</b>".$ClassNameCh.$space.$space.$ClassNameEn;
		$Sex_html ="<b class='TextColorRed'>".$eReportCard['Template']['SexCh'].$space.$eReportCard['Template']['SexEn']." : "."</b>".$SexCh.$space.$space.$SexEn;		
		$DateOfBirth_html =  "<b class='TextColorRed'>".$eReportCard['Template']['BdateCh'].$space.$eReportCard['Template']['BdateEn']." : "."</b>".$DateOfBirth; 
		$DateOfIssue_html =   "<b class='TextColorRed'>".$eReportCard['Template']['IssueDateCh'].$space.$eReportCard['Template']['IssueDateEn']." : "."</b>".$DateOfIssue;
					
		$IsMainReport 	= $ReportInfoArr['isMainReport'];		
		$stdImg_html='';
		
		if($IsMainReport)
		{
			$stdImg = $this->getStudentImage($StudentID);
			$stdImg_html = "<td rowspan='6' align='right'>".$stdImg.'</td>';
		}
	
		$StudentInfo_html='';
		$StudentInfo_html .='<table class="font_10pt"  style="width:100%; text-align:left" >
								<tr>
									<td >'.$StudentName_html.'</td>
									'.$stdImg_html.'
								</tr>
								<tr><td >'.$STRNNo_html.'</td></tr>
								<tr><td >'.$ClassName_html.'</td></tr>
								<tr><td >'.$Sex_html.'</td></tr>
								<tr><td >'.$DateOfBirth_html.'</td></tr>
								<tr><td >'.$DateOfIssue_html.'</td></tr>
							</table>';
							
		return $StudentInfo_html;
		
	}
	
	private function getStudentImage($ParStudentID)
	{
		$luser = new libuser();
		$imgFileArr = $luser->GET_OFFICIAL_PHOTO_BY_USER_ID($ParStudentID);
	
		$thisStdImg = $imgFileArr[1];
		if (file_exists($imgFileArr[0])) {
			$thisImage = ($thisStdImg != '') ? '<img src="'.$thisStdImg.'" style="width:50%;">' : '&nbsp;';
		}
		
		return $thisImage;
	}
	
	private function getPercentile($ParPosition,$ParStdTotal)
	{		
		$returnPercentile='';
	
		$SkipArray = array();
		$SkipArray[] = $ParStdTotal==0;
		$SkipArray[] = $ParStdTotal=='';
		$SkipArray[] = $ParPosition=='' ;
		
		
		if(in_array(true,$SkipArray) )
		{
			return $returnPercentile;
		}
			
		//2012-0302-1644-08132 point 1
		//$thisPercentile = ($ParPosition/$ParStdTotal)*100;
		$thisPercentile = my_round(($ParPosition/$ParStdTotal)*100, 0);
		
//		if($thisPercentile>=0 && $thisPercentile<=10)
//		{
//			$returnPercentile=1;
//		}
//		else if($thisPercentile>=11 && $thisPercentile<=25)
//		{
//			$returnPercentile=2;
//		}
//		else if($thisPercentile>=26 && $thisPercentile<=50)
//		{
//			//$returnPercentile=3;
//		}
//		else if($thisPercentile>=51 && $thisPercentile<=75)
//		{
//			//$returnPercentile=4;
//		}
//		else if($thisPercentile>=76 && $thisPercentile<=100)
//		{
//			//$returnPercentile=5;
//		}

		if(floatcmp((float)$thisPercentile, ">=", (float)0) && floatcmp((float)$thisPercentile, "<=", (float)10))
		{
			$returnPercentile=1;
		}
		if(floatcmp((float)$thisPercentile, ">=", (float)11) && floatcmp((float)$thisPercentile, "<=", (float)25))
		{
			$returnPercentile=2;
		}
		if(floatcmp((float)$thisPercentile, ">=", (float)26) && floatcmp((float)$thisPercentile, "<=", (float)50))
		{
			//$returnPercentile=3;
		}
		if(floatcmp((float)$thisPercentile, ">=", (float)51) && floatcmp((float)$thisPercentile, "<=", (float)75))
		{
			//$returnPercentile=4;
		}
		if(floatcmp((float)$thisPercentile, ">=", (float)76) && floatcmp((float)$thisPercentile, "<=", (float)100))
		{
			//$returnPercentile=5;
		}
		
		return $returnPercentile;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		
	}
	function getSignatureTable($ReportID='')
	{
	}
	
	function genMSTableColHeader($ReportID)
	{
		
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		
	}
	
	########### END Template Related
	
}
?>