<?php
# Editing by ivan

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/carmel_alison.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary","attendance", "merit", "remark", "eca");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "---";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard;
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
		$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='900px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard,$PATH_WRT_ROOT,$title1;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Semester = $ReportSetting["Semester"];
			$SemName = $Semester=="F"?$eReportCard['Template']['WholeYear']:$this->returnSemesters($Semester);
			
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
			# Get AcademicYearName
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			$ObjYear = new academic_year($this->schoolYearID);
			$AcademicYear = $ObjYear->Get_Academic_Year_Name();
						
			# get school badge
			$imgfile = "/file/reportcard2008/templates/carmel_alison.jpg";
			$SchoolLogo = ($imgfile != "") ? "<img src=\"{$imgfile}\" width=95>\n" : "";				
			# get school name
			$SchoolName = $eReportCard['Template']['SchoolInfo']['SchoolNameCh']."<br>".$eReportCard['Template']['SchoolInfo']['SchoolNameEn'];	
			
			$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
	
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='100' align='center'>".$TempLogo."</td>";
			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
					if(!empty($SchoolName))
						$TitleTable .= "<tr><td nowrap='nowrap' class='schoolname' align='left'>".$SchoolName."</td></tr>\n";
					if($this->getSchoolInfo())
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='left' >".$this->getSchoolInfo()."</td></tr>\n";
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			}
			$TitleTable .= "<td align='right' valign=top>";
			$TitleTable .= "<span class='fontsize16'>".$title1."</span><br>";
			$TitleTable .= "<span class='graybg fontsize14'>".$AcademicYear." ".$SemName."</span><br>";
			//$TitleTable .= "<span class='fontsize14'>".$ReportSetting["Description"]."</span><br>";
			$TitleTable .= "</td>";
			$TitleTable .= "</tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal = ($StudentID=='')? "XXX" : '';
			//$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			$ObjYear = new academic_year($this->schoolYearID);
			$data['AcademicYear'] = $ObjYear->Get_Academic_Year_Name();
			
			$data['DateOfIssue'] = $ReportSetting['Issued'];
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] = $lu->UserName2Lang('en', 2);
				//$data['ClassNo'] = $lu->ClassNumber;
				//$data['Class'] = $lu->ClassName;
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				//$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassName']."(".$StudentInfoArr[0]['ClassNumber'].")";
				
				$data['ClassNo'] = $thisClassNumber;
				//$data['Class'] = $thisClassName;
				
				$data['StudentNo'] = $thisClassNumber;
				
				/*if (is_array($SettingStudentInfo))
				{
					if (!in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo) && ($thisClassNumber != ""))
						$data['Class'] .= " (".$thisClassNumber.")";
				}
				else
				{
					if ($thisClassNumber != "")
						$data['Class'] .= " (".$thisClassNumber.")";
				}*/
				
				$tmpdate = $lu->DateOfBirth;
				$tmpdate = explode("-",$tmpdate);
				$tmpdate = implode("/",$tmpdate); 
				$data['DateOfBirth'] = $tmpdate?$tmpdate:"&nbsp;";
				
				$data['Gender'] = $lu->Gender;
				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				$data['StudentAdmNo'] = $data['STRN'];
				
				$ClassTeacherAry = $lclass->returnClassTeacher($StudentInfoArr[0]['ClassName'], $this->schoolYearID);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
			}

			$defaultInfoArray = array("Name", "ClassNo", "DateOfBirth", "Gender");
			$Page3Array = array("Name", "ClassNo");
			if(!empty($defaultInfoArray))
			{
				$count = 0;
				$StudentInfoTable .= "<table width='80%' border='0' cellpadding='0' cellspacing='0' align='left'>";
				
				$StudentInfoTable .= "<thead><th class='tabletext fontsize14'>".$eReportCard['Template']['SubTitle']['StudentInfo']."</th></thead>";
				
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if($forPage3==1 && !in_array($SettingID,$Page3Array)) continue;
					
					if(in_array($SettingID, $defaultInfoArray)===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
						}
						
						$colspan = $SettingID=="Name" ? " colspan='4' " : "";
						$align = $SettingID!="Name" ? " align='center' " : "";
						$YMD = $SettingID=="DateOfBirth" ? "(Y/M/D)" : "&nbsp;";
						$datewidth = $SettingID=="DateOfBirth" ? " width='80px' ":"";
						
						$StudentInfoTable .= "<td class='tabletext fontsize14' valign='top' height='{$LineHeight}' align=right>".$Title." :</td>";
						$StudentInfoTable .= "<td class='tabletext tableline fontsize14' $align $colspan valign='top' height='{$LineHeight}' $datewidth>".($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
						$StudentInfoTable .= "<td class='tabletext fontsize14'>$YMD</td>";
							
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						} else {
							//if($SettingID=="Name")
							//{
								$count++;
							//	$StudentInfoTable .= "</tr>";
							//}
						}
						$count++;
					}
					
				}
				$StudentInfoTable .= "</table>";
				
				
			}
		}
		return $StudentInfoTable;
	}
	
	/*
	 *	$ReportColumnIDArr[$SubjectID] = $ReportColumnID
	 *	$FullMarkArr[$SubjectID] = $FullMark
	 *	$DisplayArr[$SubjectID] = on
	 */
	function getAssessmentMSTable($ReportID, $ReportColumnIDArr, $DisplayArr, $StudentID='' )
	{
		global $eRCTemplateSetting, $eReportCard;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		### Get Subject List to be displayed
		$SubjectIDArr = (array)$DisplayArr;
		$numOfSubject = count($SubjectIDArr);
		
		### Get Subject Column
		$SubjectCol_HTML_Arr = $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		
		### Get Subject SubMS Mark
		# $MarkArr[$SubjectID][$ColumnID] = Mark
		$MarkArr = $this->getMarks($ReportID, $StudentID);
		
		$html = '<br><br>';
		$html .= "<table width='100%' border='0' cellspacing='0' cellpadding='4' class='report_border'>";
//			$html .= "<tr>";
//				$html .= "<td align='center' colspan='2' width='".$eRCTemplateSetting['SubMS']['ColumnWidth']['Subject']."'>".$eReportCard['Template']['SubjectEn']."</td>";
//				$html .= "<td align='center' class='border_left' width='".$eRCTemplateSetting['SubMS']['ColumnWidth']['FullMark']."'>".$eReportCard['Template']['MaximumMarksEn']."</td>";
//				$html .= "<td align='center' class='border_left' width='".$eRCTemplateSetting['SubMS']['ColumnWidth']['Mark']."'>".$eReportCard['Template']['MarksAwardedEn']."</td>";
//			$html .= "</tr>";
			$html .= "<tr>";
				$html .= "<td align='center' width='".$eRCTemplateSetting['SubMS']['ColumnWidth']['Subject']."'>".$eReportCard['Template']['SubjectCh']."</td>";
				$html .= "<td align='center' class='border_left' width='".$eRCTemplateSetting['SubMS']['ColumnWidth']['FullMark']."'>".$eReportCard['Template']['MaximumMarksCh']."</td>";
				$html .= "<td align='center' class='border_left' width='".$eRCTemplateSetting['SubMS']['ColumnWidth']['Mark']."'>".$eReportCard['Template']['MarksAwardedCh']."</td>";
			$html .= "</tr>";

			for ($i=0; $i<$numOfSubject; $i++)
			{
				$thisSubjectID = $SubjectIDArr[$i];
				$thisReportColumnID = $ReportColumnIDArr[$thisSubjectID];
				$thisFullMark = $thisFullMark = $this->GET_SUBJECT_FULL_MARK($thisSubjectID,$ClassLevelID, $ReportID);
				
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisSubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				### Get the corresponding mark of the subject
				$thisMSGrade = $MarkArr[$thisSubjectID][$thisReportColumnID]['Grade'];
				$thisMSMark = $MarkArr[$thisSubjectID][$thisReportColumnID]['Mark'];
				
				$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
				$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
				
				$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
				
				
				# check special case
				list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisSubjectID, $thisMark);
				
				if($needStyle)
					$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $thisSubjectID);
				else
					$thisMarkDisplay = $thisMark;
					
				
				/*if (is_numeric($thisMark) && $thisMark < $thisPassMark)
					$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', 'Fail');
				else
					$thisMarkDisplay = $thisMark;*/
				
				$thisMarkDisplay = ($thisMarkDisplay=='')? $this->EmptySymbol : $thisMarkDisplay;
				
				$css_border_top = ($i==0)? "border_top" : "";
				$html .= "<tr>";
					$html .= $SubjectCol_HTML_Arr[$thisSubjectID];
					$html .= "<td class='tabletext $css_border_top border_left' align='center'>".$thisFullMark."</td>";
					$html .= "<td class='tabletext $css_border_top border_left' align='center'>".$thisMarkDisplay."</td>";
				$html .= "</tr>";
			}
			
		$html .= "</table>";
		
		return $html; 
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID = $SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		//$css_border_top = ($Prefix)? "" : "border_top";
			 		$css_border_top = ($isFirst)? "border_top" : "";
			 		
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
			 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='5' cellspacing='0'>";
						$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='tabletext'>$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[$SubSubjectID] = $t;
					$isFirst = 0;
				}
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID='',$StudentID='',$IssueDate='')
	{
 		global $eReportCard, $eRCTemplateSetting,$PATH_WRT_ROOT;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $IssueDate;
			if($Issued){
				 $Issued = $this->FormatDate($Issued,"Y/M/D");
			}
		}
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$SignatureTable .= "<tr>";
		$tdwidth = "width='".(100/(sizeof($SignatureTitleArray)+1))."%'";
		$emptytd = "<td $tdwidth>&nbsp;</td>";
		
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		$lclass = new libclass();
		$ClassTeacherAry =  $lclass->returnClassTeacher($this->Get_Student_ClassName($StudentID), $this->schoolYearID);
		$ClassTeacherStr = implode("．",Get_Array_By_Key($ClassTeacherAry,"CTeacher"));
		
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			
			switch($SettingID)
			{
				case "Principal": $DisplayName = "(".$eReportCard['Template']['SchoolInfo']['Principal'].")"; break;
				case "ClassTeacher": $DisplayName = "(".$ClassTeacherStr.")"; break;
				default: $DisplayName = '&nbsp;';
			}	
			
			$Title = $eReportCard['Template'][$SettingID];
			$IssueDate = ($SettingID == "IssueDate" && $Issued)	? $Issued : "&nbsp;";
			$underlineWidth = ($SettingID == "IssueDate" )? "width='100%'" : "width='90%'";
			
			$SignatureTable .= "<td valign='bottom' align='center' $tdwidth>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0' $underlineWidth>";
			$SignatureTable .= "<tr><td align='center' class='fontsize14' valign='bottom'>". $IssueDate ."</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='fontsize10 border_top' height='25' valign='top' nowrap>".$DisplayName."</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='fontsize14' valign='bottom'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
			$SignatureTable .= ($SettingID == "IssueDate" && $Issued)?$emptytd:"";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function Get_Address_Table()
	{
		global $eReportCard;
		
		if (is_array($eReportCard['Template']['AddressDisplay']) || $eReportCard['Template']['AddressDisplay'] != '')
		{
			$AddressArr = array();
			if (is_array($eReportCard['Template']['AddressDisplay']) == false)
				$AddressArr[] = $eReportCard['Template']['AddressDisplay'];
			else
				$AddressArr = $eReportCard['Template']['AddressDisplay'];
		}
		
		$numOfRow = count($AddressArr);
		$x = '';
		$x .= '<table cellspacing="0" cellpadding="2" border="0">';
			for ($i=0; $i<$numOfRow; $i++)
			{
				$x .= '<tr><td class="reportcard_text">'.$AddressArr[$i].'</td></tr>';
			}
		$x .= '</table>';
		
		return $x;
	}
	
	function getSchoolInfo()
	{
		global $eReportCard;
		
		$SchoolInfo = "<table cellspacing=0 cellpadding=0 border=0 class='fontsize10'>";
		$SchoolInfo .= "<tr>";
			$SchoolInfo .= "<td>".$eReportCard['Template']['SchoolInfo']['AddressEn'].": ".$eReportCard['Template']['SchoolInfo']['SchoolAddrEn']."</td>";
		$SchoolInfo .= "</tr>";
		$SchoolInfo .= "<tr>";
			$SchoolInfo .= "<td>";
				$SchoolInfo .= $eReportCard['Template']['SchoolInfo']['AddressCh'].": ".$eReportCard['Template']['SchoolInfo']['SchoolAddrCh'];
				$SchoolInfo .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				$SchoolInfo .= $eReportCard['Template']['SchoolInfo']['TelNoCh']." ".$eReportCard['Template']['SchoolInfo']['TelNoEn']." : ".$eReportCard['Template']['SchoolInfo']['SchoolTel'] ;
			$SchoolInfo .= "</td>";
		$SchoolInfo .= "</tr>";
		$SchoolInfo .= "<tr>";
			$SchoolInfo .= "<td>";
				$SchoolInfo .= $eReportCard['Template']['SchoolInfo']['FaxNoCh']." ".$eReportCard['Template']['SchoolInfo']['FaxNoEn']." : ".$eReportCard['Template']['SchoolInfo']['SchoolFax'] ;
				$SchoolInfo .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				$SchoolInfo .= $eReportCard['Template']['SchoolInfo']['EmailCh']." ".$eReportCard['Template']['SchoolInfo']['EmailEn']." : <u>".$eReportCard['Template']['SchoolInfo']['SchoolEmail']."</u>" ;
			$SchoolInfo .="</td>";	
		$SchoolInfo .= "</tr>";
		$SchoolInfo .= "</table>";
		
		return $SchoolInfo;
			
	}
}
?>