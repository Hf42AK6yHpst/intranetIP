<?php
# Editing by Bill

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("attendance", "summary", "demerit", "remark");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 0;
		$this->IsEnableMarksheetFeedback = 0;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "---";
		$this->absentSubjectAry = array();
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='', $PrintTemplateType='') {
		global $eReportCard;
		
		$reportContentHeight = "882";
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$ClassLevelID = $ReportSetting['ClassLevelID'];
			$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
			$SemID = $ReportSetting['Semester'];
			$ReportType  = $SemID == "F" ? "W" : "T";
			$NonFormSixYearReport = $FormNumber > 0 && $FormNumber != 6 && $ReportType == "W";
			$reportContentHeight = $NonFormSixYearReport? "891" : $reportContentHeight;
		}
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
		$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				//$x .= "<tr height='865px' valign='top'><td>".$TableTop."</td></tr>";
				//$x .= "<tr height='899px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr height='".$reportContentHeight."px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID, $StudentID='', $PrintTemplateType='') {
		global $eReportCard;
		
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$isExtraReport = !$ReportSetting['isMainReport'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			$ReportTitle = str_replace(" ", "&nbsp;", $ReportTitle);
			$ClassLevelID = $ReportSetting['ClassLevelID'];
			$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
			$SemID 		 = $ReportSetting['Semester'];
			$ReportType  = $SemID == "F" ? "W" : "T";
			$term_seq 	 = $this->Get_Semester_Seq_Number($SemID);
			
			// Form 1 - 5 Whole Year Report
			// Style for Report Header
			$NonFormSixYearReport = $FormNumber > 0 && $FormNumber != 6 && $ReportType == "W";
			$YearReportHeaderStyleTop = $NonFormSixYearReport? "style='line-height: 8px;'" : "";
			$YearReportHeaderStyleBottom = $NonFormSixYearReport? "style='margin-bottom: 0px;'" : "";
			
			// Increase Photo Size
			$photo_extra = $isExtraReport? "test_photo" : "";
			// Increase line spacing
			$title_class = $isExtraReport? "_extra" : "";
			
			# get school badge
			//$SchoolLogo = GET_SCHOOL_BADGE();
			//$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			//if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
			$SchoolLogo = "/file/reportcard2008/templates/twgh_wu_york_yu_college_logo.jpg";
				
			# get school name
			//$SchoolName = GET_SCHOOL_NAME();	
			$SchoolName = "TWGHs Mrs. Wu York Yu Memorial College";
			$SchoolName .= "<br>";
			$SchoolName .= "東華三院伍若瑜夫人紀念中學";
			
			# get report title
//			$KeyCh = "Term".$term_seq."Ch";
//			$KeyEn = "Term".$term_seq."En";
			$SemCh = $this->returnSemesters($SemID,'b5');
			$SemEn = $this->returnSemesters($SemID,'en');
			if($isExtraReport){
//				$sem_title = $eReportCard['Template']['MSTable'][$KeyCh].$eReportCard['Template']['MSTable']['TermTestCh']."(".$eReportCard['Template']['MSTable'][$KeyEn]." ".$eReportCard['Template']['MSTable']['TermTestEn'].")";
//				$sem_title = $SemCh.$eReportCard['Template']['MSTable']['TermTestCh']."($SemEn ".$eReportCard['Template']['MSTable']['TermTestEn'].")";
			}
			else{
//				$sem_title = $eReportCard['Template']['MSTable'][$KeyCh]."(".$eReportCard['Template']['MSTable'][$KeyEn].")";
				if($ReportType=="W"){
					$sem_title = $eReportCard['Template']['MSTable']['AnnualCh']." (".strtoupper($eReportCard['Template']['MSTable']['AnnualEn']).")";
				}
				else{
					$sem_title = "$SemCh (".strtoupper($SemEn).")";
				}
			}
			
			# build table
			$TitleTable = "<table class='report_header' width='100%' border='0' cellpadding='0' cellspacing='0' align='center' $YearReportHeaderStyleBottom>";
			
			if($isExtraReport){
				// do nothing
			}
			// add extra pending for main report
			else{
				$TitleTable .= "<tr>";
				$TitleTable .= "<td colspan='3'>";
					$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title_2$title_class' align='center' $YearReportHeaderStyleTop>&nbsp;</td></tr>";
					$TitleTable .= "</table>";
				$TitleTable .= "</td>";
				$TitleTable .= "</tr>";
			}
			
			$TitleTable .= "<tr>";
				$TitleTable .= "<td class='header_photo_td' align='center'><img class='$photo_extra' src=\"$SchoolLogo\"></td>";
				
				$TitleTable .= "<td>";
					$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title_1$title_class' align='center'>$SchoolName</td></tr>";
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title_2$title_class' align='center'>$ReportTitle</td></tr>";
						if(!$isExtraReport){
							$TitleTable .= "<tr><td nowrap='nowrap' class='report_title_3' align='center'><u>$sem_title</u></td></tr>";
						}
					$TitleTable .= "</table>";
				$TitleTable .= "</td>";
							
				$TitleTable .= "<td class='header_address'>";
					$TitleTable .= "新界葵涌石蔭安捷街";
					$TitleTable .= "<br>";
					$TitleTable .= "十三至二十一號";
					$TitleTable .= "<br>";
					$TitleTable .= "13-21, ON CHIT STREET";
					$TitleTable .= "<br>";
					$TitleTable .= "SHEK YAM, KWAI CHUNG";
					$TitleTable .= "<br>";
					$TitleTable .= "N.T., HONG KONG";
				$TitleTable .= "</td>";
			$TitleTable .= "</tr>";
			$TitleTable .= "</table>";
		}
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='', $forPage3='', $PageNum=1, $PrintTemplateType='') {
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$isExtraReport = !$ReportSetting['isMainReport'];
			$ClassLevelID = $ReportSetting['ClassLevelID'];
			$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
			$SemID = $ReportSetting['Semester'];
			$ReportType = $SemID == "F" ? "W" : "T";
			$data['DateOfIssue'] = $ReportSetting['Issued'];
			$data['AcademicYear'] = $this->GET_ACTIVE_YEAR_NAME();
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			
			# Retrieve Required Variables
			$defaultVal = ($StudentID=='')? "XXX" : '';
			
			# Retrieve Student Info
			if($StudentID)		
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				$lu = new libuser($StudentID);
				
				$data['Name'] = $lu->UserName2Lang("b5", 1);
				$data['Gender'] = $lu->Gender;
				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$data['Class'] = $StudentInfoArr[0]['ClassName'];
				$data['ClassNo'] = $StudentInfoArr[0]['ClassNumber'];
			}
			
			if($isExtraReport){
				$data['Name'] = $lu->ChineseName." ".$lu->EnglishName;
			}
		
			// Font Size (11pt) for Term Test Report
			$font_style = $isExtraReport? "table_11pt_font" : "";
			// Increase space with Subject Result Box for Term Test Report
			$table_style = $isExtraReport? "style='padding-bottom:8px;'" : "";
			
			// Form 1 - 5 Whole Year Report
			// Style to remove space below Student Info table
			$NonFormSixYearReport = $FormNumber > 0 && $FormNumber != 6 && $ReportType == "W";
			$table_style = $NonFormSixYearReport? "style='margin-bottom:0px;'" : $table_style;
			
			# Build Table
			$StudentInfoTable .= "<table class='info_table $font_style' width='100%' border='0' cellpadding='0' cellspacing='0' align='center' $table_style>";
			$StudentInfoTable .= "<tr><td>";
			
			$count = 0;
			for($i=0; $i<sizeof($StudentTitleArray); $i++)
			{
				$SettingID = trim($StudentTitleArray[$i]);
				$SettingIDChi = $SettingID."Ch";
				$SettingIDEng = $SettingID."En";
				
				// set width for student info width
				if($count%3==0)	$title_width = 9;
				if($count%3==0)	$content_width = 40;
				if($count%3==1)	$title_width = 12;
				if($count%3==1)	$content_width = 10;
				if($count%3==2)	$title_width = 18;
				if($count%3==2)	$content_width = 15;
				// set Student Info width for Term Test Report
				if($isExtraReport){
					if($count%3==0)	$title_width = 9.5;
					if($count%3==0)	$content_width = 40.5;
					if($count%3==1)	$title_width = 12.5;
					if($count%3==1)	$content_width = 7;
					if($count%3==2)	$title_width = 19.5;
					if($count%3==2)	$content_width = 12;
				}
				
				$Title = $eReportCard['Template']['StudentInfo'][$SettingIDChi]."	".$eReportCard['Template']['StudentInfo'][$SettingIDEng];
				
				$studentdata = $data[$SettingID] ? $data[$SettingID] : $defaultVal;
				if($SettingID=="DateOfIssue")
				{
					$studentdata = date("d/m/Y", strtotime($studentdata));	
				}
				
				if($count % $StudentInfoTableCol==0) 
				{
					$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
					$StudentInfoTable .= "<tr>";
				}
				
				if($isExtraReport){
					$StudentInfoTable .= "<td class='tabletext' width='{$title_width}%' valign='top' height='{$LineHeight}' >".$Title."</td>";
					$StudentInfoTable .= "<td class='tabletext' width='{$content_width}%' valign='top' height='{$LineHeight}'>: ".$studentdata."</td>";
				}
				else{
					$StudentInfoTable .= "<td class='tabletext' width='{$title_width}%' valign='top' height='{$LineHeight}' >".$Title."</td>";
					$StudentInfoTable .= "<td class='tabletext' width='{$content_width}%' valign='top' height='{$LineHeight}'>:　".$studentdata."</td>";
				}
						
				if(($count+1) % $StudentInfoTableCol==0) 
				{
					$StudentInfoTable .= "</tr>";
					
//					if($isExtraReport && (($count+1) / $StudentInfoTableCol)==1){
//						$StudentInfoTable .= "<tr>";
//						$StudentInfoTable .= "<td class='tabletext' valign='top' height='{$LineHeight}' >&nbsp;</td>";
//						$StudentInfoTable .= "<td class='tabletext' valign='top' height='{$LineHeight}'>　 ".$lu->EnglishName."</td>";
//						$StudentInfoTable .= "<td class='tabletext' valign='top' height='{$LineHeight}' colspan='4'>&nbsp;</td>";
//						$StudentInfoTable .= "</tr>";
//					}
					
					$StudentInfoTable .= "</table>";
				}
				$count++;
			}
			$StudentInfoTable .= "</td></tr>";
			$StudentInfoTable .= "</table>";
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='', $PrintTemplateType='', $PageNum='') {
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport = $ReportSetting['isMainReport'];
		$isExtraReport = !$isMainReport;
		$SemID = $ReportSetting['Semester'];
		$term_seq = $this->Get_Semester_Seq_Number($SemID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$LineHeight = $ReportSetting['LineHeight'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectComponent = $ReportSetting['ShowSubjectComponent'];
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# Retrieve Table Header
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;
		
		# Retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach((array)$MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach((array)$SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# Retrieve SubjectCode-SubjectID Mapping - for subject code checking
		if($isExtraReport){
			$SubjectCodeMappping = $this->GET_SUBJECT_SUBJECTCODE_MAPPING();
		}
		
		# Retrieve Subject Columns		
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# Retrieve Marks
		$MarksAry = $this->getMarks($ReportID, $StudentID, '', 0, 1);
		
		# Retrieve Marks HTML
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		# Retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		// Font Size (11pt) for Term Test Report
		$font_style = $isExtraReport? "table_11pt_font" : "";
		
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='2' class='report_border report_content $font_style'>";
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		$displaySubjCount = 0;
		for($i=0; $i<$sizeofSubjectCol; $i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof((array)$MarksAry[$thisSubjectID]) > 0) 
			{
				//2014-0918-1608-33164 (Internal) Follow-up by sienasze on 2016-06-07 19:29
//				$Droped = 1;
//				foreach((array)$MarksAry[$thisSubjectID] as $cid=>$da)
//					if($da['Grade']!="*")	$Droped=0;
				$Droped = 0;
				foreach((array)$MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']=="*")	$Droped=1;
			}
			if($Droped)	continue;
			
			// component subject
			if(in_array($thisSubjectID, (array)$MainSubjectIDArray)!=true){
				$isSub=1;
				$parentSubjectID = $this->GET_PARENT_SUBJECT_ID($thisSubjectID);
			}
			
			// component subject display checking
			// hide component - template settings
			if ($ShowSubjectComponent==0 && $isSub)
				continue;
			// hide component - template flag - HideComponentSubject
			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
				continue;
			// hide component - Term Test Report and Parent subjects in ExcludeSubjCompAry
			if($isExtraReport && in_array($SubjectCodeMappping[$parentSubjectID], (array)$eRCTemplateSetting['Report']['ReportGeneration']['ExcludeSubjCompAry']) && $isSub)
				continue;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				$DetailsTable .= "</tr>";
				
				$isFirst = 0;
				$displaySubjCount++;
			}
		}

		// add empty row to subject table
//		// no need to add empty row for main report
//		if($isMainReport && $displaySubjCount < 24)
//		{
//			$SubjEmptyColumn = $ColNum - 2;
//			
//			for($i=0; $i<(24-$displaySubjCount); $i++)
//			{
//				$DetailsTable .= "<tr>";
//				$DetailsTable .= "<td>&nbsp;</td>";
//				$DetailsTable .= "<td>&nbsp;</td>";
//				$DetailsTable .= "<td class='border_left'>&nbsp;</td>";
//				for($j=0; $j<$SubjEmptyColumn; $j++)
//				{
//					$DetailsTable .= "<td class='border_left'>&nbsp;</td>";
//					$DetailsTable .= "<td>&nbsp;</td>";
//				}
//				$DetailsTable .= "</tr>";
//			}
//		}
//		else 
		if($isExtraReport && $displaySubjCount < 16)
		{
			for($i=0; $i<(16-$displaySubjCount); $i++)
			{
				$DetailsTable .= "<tr>";
				$DetailsTable .= "<td>&nbsp;</td>";
				$DetailsTable .= "<td>&nbsp;</td>";
				$DetailsTable .= "<td class='border_left'>&nbsp;</td>";
				$DetailsTable .= "<td class='border_left'>&nbsp;</td>";
				$DetailsTable .= "</tr>";
			}
		}
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
		# Merit and ECA Table
		$DetailsTable .= $this->getMeritAndECATable($ReportID, $StudentID);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID) {
		global $eReportCard, $eRCTemplateSetting;

		# Retrieve Display Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport 				= $ReportSetting['isMainReport'];
		$isExtraReport 				= !$isMainReport;
		$SemID 						= $ReportSetting['Semester'];
        $SemesterNumber				= $this->Get_Semester_Seq_Number($SemID);
		$ReportType 				= $SemID == "F" ? "W" : "T";
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectFullMark 		= $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ShowRightestColumn 		= $this->Is_Show_Rightest_Column($ReportID);
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];

//		always show Form Ranking
		$RankTitle = $eReportCard['Template']['MSTable']['FormRankCh']."<br>".$eReportCard['Template']['MSTable']['FormRankEn'];
//		if($FormNumber > 3 && $isExtraReport)	
//		{
//			$RankTitle = $eReportCard['Template']['MSTable']['ClassRankCh']."<br>".$eReportCard['Template']['MSTable']['ClassRankEn'];
//		}
				
		$n = 0;
		$e = 0;	# column within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		
		$colwidth = $ReportType == "T"? 10 : 8;
		$fullmark_width = $ReportType == "T"? 10 : 8;
		$semester_width = 0;
		
		#########################################################
		# Marks START
		#########################################################
		
		$row2 = "";
		if($ReportType == "T")	# Term Report Type
		{
			# Retrieve Invloved Assesment
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
//			$semester_name = $this->returnSemesters($SemID, "en");
//			
//			$ColumnID = array();
//			$ColumnTitle = array();
//			if (sizeof($ColoumnTitle) > 0)
//				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
			
			if($isExtraReport)
			{
				$colwidth = 12;
				// for subject - Mathematics (Extended Part – Module 1/2)
				//$fullmark_width = 24;
				$fullmark_width = 22;
			}
			$td_width = "width='$colwidth%'";
			
			$e = 0;
			if($isExtraReport)
			{
				$row1 .= "<td class='border_left border_bottom' align='center' $td_width>".$eReportCard['Template']['MSTable']['ResultCh']."<br>".$eReportCard['Template']['MSTable']['ResultEn']."</td>";
				$row1 .= "<td class='border_bottom' align='center' $td_width>".$RankTitle."</td>";
				$n++;
				$e++;
			}
			else
			{
//				if($FormNumber != 6)
//				{
					$row1 .= "<td class='border_left' align='center' width='".($colwidth-1)."%'>".$eReportCard['Template']['MSTable']['TermTestCh']."</td>";
//					$row1 .= "<td align='center' $td_width>".$semester_name."</td>";
					$row1 .= "<td align='center' $td_width>".$eReportCard['Template']['MSTable']['TermTestEn']."</td>";
					$row2 .= "<td class='border_top border_left border_bottom' align='center' width='".($colwidth-1)."%'>".$eReportCard['Template']['MSTable']['ResultCh']."<br>".$eReportCard['Template']['MSTable']['ResultEn']."</td>";
					$row2 .= "<td class='border_top border_bottom' align='center' $td_width>".$RankTitle."</td>";
					$n++;
	 				$e++;
//				}

                // [2020-0603-1559-06235]
				if($FormNumber != 6 && $SemesterNumber == 1)
                {
                    $SemNameB5 = $this->returnSemesters($SemID, "b5");
                    //$SemNameEN = $this->returnSemesters($SemID, "en");
                    $SemNameEN = "1st Term";

                    $row1 .= "<td class='border_left' align='center' width='".($colwidth-1)."%'>".$SemNameB5."</td>";
                    $row1 .= "<td align='center' $td_width>".$SemNameEN."</td>";
                    $row2 .= "<td class='border_top border_left border_bottom' align='center' width='".($colwidth-1)."%'>".$eReportCard['Template']['MSTable']['OverallResultCh']."<br>".$eReportCard['Template']['MSTable']['OverallResultEn']."</td>";
                    $row2 .= "<td class='border_top border_bottom' align='center' $td_width>".$RankTitle."</td>";
                    $n++;
                    $e++;
                }
                else
                {
                    for($i=0; $i<sizeof($ColoumnTitle); $i++)
                    {
                        //$ColumnTitleDisplay = $ColumnTitle[$i];
                        //$row1 .= "<td class='border_left' align='center' $td_width>". $ColumnTitleDisplay ."</td>";
                        //$row1 .= "<td align='center' $td_width>". $semester_name ."</td>";

					    // Form 6 with Test in Report Column
                        //if($FormNumber==6 && $i==0){
                        //    $row1 .= "<td class='border_left' align='center' $td_width>".$eReportCard['Template']['MSTable']['TermTestCh']."</td>";
                        //    $row1 .= "<td align='center' $td_width>".$eReportCard['Template']['MSTable']['TermTestEn']."</td>";
                        //}
                        //else {
						$row1 .= "<td class='border_left' align='center' width='".($colwidth-1)."%'>".$eReportCard['Template']['MSTable']['TestCh']."</td>";
						$row1 .= "<td align='center' $td_width>".$eReportCard['Template']['MSTable']['TestEn']."</td>";
					    //}
                        $row2 .= "<td class='border_top border_left border_bottom' align='center' width='".($colwidth-1)."%'>".$eReportCard['Template']['MSTable']['ResultCh']."<br>".$eReportCard['Template']['MSTable']['ResultEn']."</td>";
                        $row2 .= "<td class='border_top border_bottom' align='center' $td_width>".$RankTitle."</td>";
                        $n++;
                        $e++;
                    }
                }
			}
			
			// updated width for main report
			//$semester_width = $colwidth * 2 * $n;
			$semester_width = $isExtraReport? ($colwidth * 2 * $n) : (($colwidth * 2 - 1) * $n);
			
//			if($isMainReport && $this->Get_Semester_Seq_Number($SemID) == 2)
//			{
//				$semester_width += $colwidth * 2;
//			}
		}
		else	# Whole Year Report Type
		{
			if($FormNumber == 6)
			{
			    global $DisplayColumnNum;

				$termReport = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
//				$semester_name = $this->returnSemesters($termReport[0]["Semester"], "en");
				$ColoumnTitle = $this->returnReportTemplateColumnData($termReport[0]["ReportID"]);
				
//				$ColumnID = array();
//				$ColumnTitle = array();
//				if (sizeof($ColoumnTitle) > 0)
//					foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);

                // 2 cols - echo 23%
                // 3 cols - echo 15%
                $isHideExamColMode = ($DisplayColumnNum == 2);
                if($isHideExamColMode) {
                    $colwidth = 10;
                    $fullmark_width = 10;
                }
				$td_width = "width='$colwidth%'";
				
				$e = 0;
				
				$row1 .= "<td class='border_left' align='center' width='".($colwidth-1)."%'>".$eReportCard['Template']['MSTable']['TermTestCh']."</td>";
//				$row1 .= "<td align='center' $td_width>".$semester_name."</td>";
				$row1 .= "<td align='center' $td_width>".$eReportCard['Template']['MSTable']['TermTestEn']."</td>";
				$row2 .= "<td class='border_top border_left border_bottom' align='center' width='".($colwidth-1)."%'>".$eReportCard['Template']['MSTable']['ResultCh']."<br>".$eReportCard['Template']['MSTable']['ResultEn']."</td>";
				$row2 .= "<td class='border_top border_bottom' align='center' $td_width>".$RankTitle."</td>";
				$n++;
	 			$e++;

	 			if($isHideExamColMode)
                {
                    // Hide exam column
                }
                else
                {
                    for($i=0; $i<sizeof($ColoumnTitle); $i++)
                    {
                        //$ColumnTitleDisplay = $ColumnTitle[$i]["ColumnTitle"];
                        //$row1 .= "<td class='border_left' align='center' $td_width>". $ColumnTitleDisplay ."</td>";
                        //$row1 .= "<td align='center' $td_width>". $semester_name ."</td>";
                        // Form 6 with Test in Report Column
                        //if($i==0){
                        //    $row1 .= "<td class='border_left' align='center' $td_width>". $eReportCard['Template']['MSTable']['TermTestCh'] ."</td>";
                        //    $row1 .= "<td align='center' $td_width>". $eReportCard['Template']['MSTable']['TermTestEn'] ."</td>";
                        //}
                        //else{
						$row1 .= "<td class='border_left' align='center' width='".($colwidth-1)."%'>". $eReportCard['Template']['MSTable']['TestCh'] ."</td>";
						$row1 .= "<td align='center' $td_width>". $eReportCard['Template']['MSTable']['TestEn'] ."</td>";
    					//}
                        $row2 .= "<td class='border_top border_left border_bottom' align='center' width='".($colwidth-1)."%'>".$eReportCard['Template']['MSTable']['ResultCh']."<br>".$eReportCard['Template']['MSTable']['ResultEn']."</td>";
                        $row2 .= "<td class='border_top border_bottom' align='center' $td_width>".$RankTitle."</td>";
                        $n++;
                        $e++;
                    }
                }
			}
			else
			{
				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
	 			$needRowspan=0;
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					$td_width = "width='$colwidth%'";
					$colspan = "colspan='2'";
					
					$SemNameB5 = $this->returnSemesters($ColumnData[$i]['SemesterNum'], "b5");
					$SemNameEN = $this->returnSemesters($ColumnData[$i]['SemesterNum'], "en");
					
					// Display short form for term column name
					if ($i===0) {
						$SemNameEN = "1st Term";
                    } else if($i===1) {
						$SemNameEN = "2nd Term";
                    }

					//$row1 .= "<td {$colspan} width='".($colwidth * 2)."%' height='{$LineHeight}' class='border_left' align='center'>".$SemName."</td>";
					$row1 .= "<td class='border_left' align='center' width='".($colwidth-1)."%'>". $SemNameB5 ."</td>";
					$row1 .= "<td align='center' $td_width>". $SemNameEN ."</td>";

                    // [2020-0603-1559-06235]
					//$row2 .= "<td class='border_top border_left border_bottom' align='center' width='".($colwidth-1)."%'>".$eReportCard['Template']['MSTable']['ResultCh']."<br>".$eReportCard['Template']['MSTable']['ResultEn']."</td>";
                    $row2 .= "<td class='border_top border_left border_bottom' align='center' width='".($colwidth-1)."%'>".$eReportCard['Template']['MSTable']['OverallResultCh']."<br>".$eReportCard['Template']['MSTable']['OverallResultEn']."</td>";
					$row2 .= "<td class='border_top border_bottom' align='center' $td_width>".$RankTitle."</td>";
					$n++;
					$e++;
				}
			}
			
			// updated width for main report
			//$semester_width = $colwidth * 2 * ($n + 1);
			$semester_width = ($colwidth * 2 - 1) * ($n + 1);
		}
		#########################################################
		# Marks END
		#########################################################

		$total_subject_width = (100 - $semester_width - $fullmark_width);
		
		// slightly increase subject width
		//$subject_width = $isExtraReport? ($total_subject_width / 2) : ($total_subject_width * 0.4);
		$subject_width = $isExtraReport? ($total_subject_width / 2) : ($total_subject_width * 0.41);
		
		$td_width = "width='$colwidth%'";
		$Rowspan = $row2 ? "rowspan='2'" : "";
		if(!$needRowspan)
		{
			$row1 = str_replace("rowspan='2'", "", $row1);
		}
		
		$x = "<tr>";;
		
		# Subject
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		for($i=0; $i<sizeof($SubjectColAry); $i++)
		{
			$SubjectEng = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'];
			$SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];
			$SubjectTitle = $SubjectColAry[$i];
			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
			$x .= "<td {$Rowspan}  valign='middle' class='border_bottom' height='{$LineHeight}' width='$subject_width%'>".$SubjectTitle."</td>";
			$n++;
			
			$subject_width = $isExtraReport? $subject_width : ($total_subject_width - $subject_width);
		}
		
		# Full Mark
		$x .= "<td {$Rowspan} valign='middle' class='border_left border_bottom' align='center' width='$fullmark_width%'>". $eReportCard['Template']['MSTable']['FullMarksCh'] ."<br>". $eReportCard['Template']['MSTable']['FullMarksEn'] ."</td>";
		$e++;
		
		# Marks
		$x .= $row1;
		
		# Subject Overall 
		if($ShowRightestColumn)
		{
			$x .= "<td valign='middle' class='border_left' align='center' width='".($colwidth-1)."%'>". $eReportCard['Template']['MSTable']['AnnualCh'] ."</td>";
			$x .= "<td valign='middle' align='center' $td_width>". $eReportCard['Template']['MSTable']['AnnualEn'] ."</td>";

            // [2020-0603-1559-06235]
			//$row2 .= "<td class='border_top border_left border_bottom' align='center' width='".($colwidth-1)."%'>".$eReportCard['Template']['MSTable']['ResultCh']."<br>".$eReportCard['Template']['MSTable']['ResultEn']."</td>";
            $row2 .= "<td class='border_top border_left border_bottom' align='center' width='".($colwidth-1)."%'>".$eReportCard['Template']['MSTable']['OverallResultCh']."<br>".$eReportCard['Template']['MSTable']['OverallResultEn']."</td>";
			$row2 .= "<td class='border_top border_bottom' align='center' $td_width>".$RankTitle."</td>";
			$n++;
		}
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID) {
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		# Retrieve SubjectID Array
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
		$isFirst = 1;
 		$x = array();
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		// Indentation for component subject 
			 		$Prefix = "&nbsp;&nbsp;&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
			 			// remove Indentation for main subject
				 		$Prefix = "";
			 		}
			 		
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
			 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
							$t .= "<tr><td>$Prefix</td><td height='{$LineHeight}'class='tabletext'>$v</td></tr>";
						$t .= "</table>";
						$t .= "</td>";
			 		}
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
 		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array()) {
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport 				= $ReportSetting['isMainReport'];
		$isExtraReport				= !$isMainReport;
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark 		= $ReportSetting['ShowSubjectFullMark'];
		$ShowRightestColumn 		= $this->Is_Show_Rightest_Column($ReportID);
		$ShowSubjectOverall 		= $ShowRightestColumn;
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		
		# Retrieve Display Settings
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		# Retrieve SubjectID Array
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		
		# Retrieve SubjectCode-SubjectID Mapping - for subject code checking
		$SubjectCodeMappping = $this->GET_SUBJECT_SUBJECTCODE_MAPPING();
		
		$isFirst = 1;
		
		$n = 0;
		$x = array();
		if($ReportType=="T")	# Term Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0){
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
			}
							
			if($isMainReport)
			{
				# Retrieve Extra Report Infomation
				$extraReportInfo = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $SemID, 0);
				$extraReportID = $extraReportInfo["ReportID"];
				$extraMarksAry = $this->getMarks($extraReportID, $StudentID,'', 0, 1);
			
				$extraColoumnTitle = $this->returnReportColoumnTitle($extraReportID);
				if(sizeof($extraColoumnTitle) > 0){
					foreach ($extraColoumnTitle as $extraColumnID[] => $extraColumnTitle[]){
						break;
					}
				}
			}
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				// italic for component subject
				$prefix = "<i>";
				$postfix = "</i>";
				$CmpSubjectArr = array();
				$isParentSubject = 0;
				// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					// remove italic for main subject
					$prefix = "";
					$postfix = "";
					$ParentSubjectID = $SubjectID;
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				else {
					$ParentSubjectID = $this->GET_PARENT_SUBJECT_ID($SubjectID);
				}
				
				// check if Subject need to display order of Class
				$displayClassOrder = in_array($SubjectCodeMappping[$ParentSubjectID], (array)$eRCTemplateSetting['Report']['ReportGeneration']['NotSubjGroupOrderList']);
		
				# Retrieve Subject Scheme Settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $ReportID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				$isAllNA = true;
				
				# Retrieve Full Mark
	  			$thisFullMark = $SubjectFullMarkAry[$SubjectID];
//	  			$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
//	  			$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  			$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID IS NULL and SubjectID = '$SubjectID' ");
				$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
				
				$FullMark = $ScaleDisplay=="M"? ($UseWeightedMark ? $thisFullMark * $thisSubjectWeight : $thisFullMark) : $thisFullMark;
				
//				$fullmark_align = "right";
//				if($isExtraReport)
//				{
//					$fullmark_align = "center";
//				}
//				$x[$SubjectID] .= "<td class='border_left' align='$fullmark_align'>". $FullMark .$postfix."&nbsp;</td>";
				$x[$SubjectID] .= "<td class='border_left' align='center'>".$prefix.$FullMark.$postfix."</td>";
				
				# Display Extra Report (Test) Result Column
				if($isMainReport)
				{
					$extraColumn = $extraColumnID[0];
					
					# Retrieve Subject Scheme Settings
					$extraSubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $extraReportID);
					$extraScaleDisplay = $extraSubjectFormGradingSettings['ScaleDisplay'];
					$extraScaleInput = $extraSubjectFormGradingSettings['ScaleInput'];
					
					# Retrieve Subject Weighting
					$thisExtraSubjectWeightData = $this->returnReportTemplateSubjectWeightData($extraReportID, "ReportColumnID='$extraColumn' and SubjectID = '$SubjectID' ");
					$thisExtraSubjectWeight = $thisExtraSubjectWeightData[$extraColumn]['Weight'];
					
					# Retrieve Subject Mark / Grade
					$thisExtraMSGrade = $extraMarksAry[$SubjectID][$extraColumn]['Grade'];
					$thisExtraMSMark = $extraMarksAry[$SubjectID][$extraColumn]['Mark'];
							
					$thisExtraGrade = ($extraScaleDisplay=="G" || $extraScaleDisplay=="" || $thisExtraMSGrade!='' )? $thisExtraMSGrade : "";
					$thisextraMark = ($extraScaleDisplay=="M" && $thisExtraGrade=='') ? $thisExtraMSMark : "";
					
					# Retrieve Order
//					if($FormNumber > 3)
//					{
//						if($displayClassOrder){
//							$thisOrder = $extraMarksAry[$SubjectID][$extraColumn]['OrderMeritClass'];
//							$orderTotalNumber = $extraMarksAry[$SubjectID][$extraColumn]['ClassNoOfStudent'];
//						}
//						else{
//							$thisOrder = $extraMarksAry[$SubjectID][$extraColumn]['OrderMeritSubjectGroup'];
//							$orderTotalNumber = $extraMarksAry[$SubjectID][$extraColumn]['SubjectGroupNoOfStudent'];
//						}
//						$extraOrderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
//						if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeClass > 0 && $thisOrder > $OverallPositionRangeClass))
//						{
//							$extraOrderDisplay = $this->EmptySymbol;
//						}
//					}
//					else 
//					{
					// always show form number
					$thisOrder = $extraMarksAry[$SubjectID][$extraColumn]['OrderMeritForm'];
					$orderTotalNumber = $extraMarksAry[$SubjectID][$extraColumn]['FormNoOfStudent'];
					$extraOrderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
					if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeForm > 0 && $thisOrder > $OverallPositionRangeForm))
					{
						$extraOrderDisplay = $this->EmptySymbol;
					}
//					}
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisextraMark 	= $extraScaleDisplay=="M" ? "S" : "";
						$thisExtraGrade = $extraScaleDisplay=="G" ? "G" : "";
					}
					$thisextraMark = ($extraScaleDisplay=="M" && strlen($thisextraMark)) ? $thisextraMark : $thisExtraGrade;
					
					if ($thisextraMark != "N.A." && $thisextraMark != "")
					{
						$isAllNA = false;
					}
					
					# store Subject ID if absent
					if(in_array($thisextraMark, array("+", "-", "abs")))
					{
						$this->absentSubjectAry[] = $SubjectID;
					}

                    // [2020-0724-1025-03073] display '---' if input grade + special case - absent / N.A.
                    if($extraScaleDisplay == "G" && in_array($thisextraMark, array("+", "-", "/", "abs", "Abs", "N.A."))) {
                        $extraOrderDisplay = $this->EmptySymbol;
                    }
					
					# check special case
					list($thisextraMark, $needStyle) = $this->checkSpCase($extraReportID, $SubjectID, $thisextraMark, $extraMarksAry[$SubjectID][$extraColumn]['Grade']);
					
					if($needStyle)
					{
						if ($extraScaleDisplay=="M" && $thisExtraSubjectWeight>0)
						{
							$thisExtraMarkTemp = ($UseWeightedMark && $thisExtraSubjectWeight!=0) ? $thisextraMark/$thisExtraSubjectWeight : $thisextraMark;
						}
						else
						{
							$thisExtraMarkTemp = $thisextraMark;
						}
						
						$thisextraMarkDisplay = $this->Get_Score_Display_HTML($thisextraMark, $extraReportID, $ClassLevelID, $SubjectID, $thisExtraMarkTemp);
					}
					else
					{
						$thisextraMarkDisplay = $thisextraMark;
					}
					$thisextraMarkDisplay = $thisextraMarkDisplay==$this->EmptySymbol? $thisextraMarkDisplay : $prefix.$thisextraMarkDisplay.$postfix; 
					
  					$x[$SubjectID] .= "<td class='border_left tabletext' align='center'>$thisextraMarkDisplay</td>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center'>$extraOrderDisplay</td>";
				}
				
				# Display Marks 
				for($i=0;$i<sizeof($ColumnID);$i++)
				{
					$thisColumnID = $ColumnID[$i];
					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
					
					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
					$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);

					if ($isSub && $columnSubjectWeightTemp == 0)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					} 
					else
					{
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisMSGrade = $MarksAry[$SubjectID][$ColumnID[$i]]['Grade'];
						$thisMSMark = $MarksAry[$SubjectID][$ColumnID[$i]]['Mark'];
						
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
						
						# Retrieve Order
//						// show class number only for term test report
//						if($FormNumber > 3 && !$isMainReport)
//						{
//							if($displayClassOrder){
//								$thisOrder = $MarksAry[$SubjectID][$ColumnID[$i]]['OrderMeritClass'];
//								$orderTotalNumber = $MarksAry[$SubjectID][$ColumnID[$i]]['ClassNoOfStudent'];
//							}
//							else{
//								$thisOrder = $MarksAry[$SubjectID][$ColumnID[$i]]['OrderMeritSubjectGroup'];
//								$orderTotalNumber = $MarksAry[$SubjectID][$ColumnID[$i]]['SubjectGroupNoOfStudent'];
//							}
//							$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
//							if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeClass > 0 && $thisOrder > $OverallPositionRangeClass))
//							{
//								$orderDisplay = $this->EmptySymbol;
//							}
//						} 
//						else 
//						{
						// always show form number
						$thisOrder = $MarksAry[$SubjectID][$ColumnID[$i]]['OrderMeritForm'];
						$orderTotalNumber = $MarksAry[$SubjectID][$ColumnID[$i]]['FormNoOfStudent'];
						$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
						if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeForm > 0 && $thisOrder > $OverallPositionRangeForm))
						{
							$orderDisplay = $this->EmptySymbol;
						}
//						}
						
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						if ($thisMark != "N.A." && $thisMark != "")
						{
							$isAllNA = false;
						}
						
						# store Subject ID if absent
						if(in_array($thisMark, array("+", "-", "abs")))
						{
							$this->absentSubjectAry[] = $SubjectID;
						}

                        // [2020-0724-1025-03073] display '---' if input grade + special case - absent / N.A.
                        if($ScaleDisplay == "G" && in_array($thisMark, array("+", "-", "/", "abs", "Abs", "N.A."))) {
                            $orderDisplay = $this->EmptySymbol;
                        }
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
						
						if($needStyle)
						{
							if ($ScaleDisplay=="M" && $thisSubjectWeight>0)
							{
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							}
							else
							{
								$thisMarkTemp = $thisMark;
							}
							
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
						
					//	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					//	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
					//	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark; 
					}
					$thisMarkDisplay = $thisMarkDisplay==$this->EmptySymbol? $thisMarkDisplay : $prefix.$thisMarkDisplay.$postfix; 
					
	  					$x[$SubjectID] .= "<td class='border_left tabletext' align='center'>$thisMarkDisplay</td>";
	  					$x[$SubjectID] .= "<td class='tabletext' align='center'>$orderDisplay</td>";
					$x[$SubjectID] .= "</td>";
					
					// only display first column if is term test report
					if($isExtraReport) break;
				}
				
				if($ShowSubjectOverall)
				{
					$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
					$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];

					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# Retrieve Order
//					// show class number only for term test report
//					if($FormNumber > 3 && !$isMainReport)
//					{
//						if($displayClassOrder){
//							$thisOrder = $MarksAry[$SubjectID][0]['OrderMeritClass'];
//							$orderTotalNumber = $MarksAry[$SubjectID][0]['ClassNoOfStudent'];
//						}
//						else{
//							$thisOrder = $MarksAry[$SubjectID][0]['OrderMeritSubjectGroup'];
//							$orderTotalNumber = $MarksAry[$SubjectID][0]['SubjectGroupNoOfStudent'];
//						}
//						$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
//						if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeClass > 0 && $thisOrder > $OverallPositionRangeClass))
//						{
//							$orderDisplay = $this->EmptySymbol;
//						}
//					} 
//					else 
//					{
					// always show form number
					$thisOrder = $MarksAry[$SubjectID][0]['OrderMeritForm'];
					$orderTotalNumber = $MarksAry[$SubjectID][0]['FormNoOfStudent'];
					$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
					if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeForm > 0 && $thisOrder > $OverallPositionRangeForm))
					{
						$orderDisplay = $this->EmptySymbol;
					}
//					}
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
										
					if ($thisMark != "N.A." && $thisMark != "")
					{
						$isAllNA = false;
					}

					if(in_array($thisMark, array("+", "-", "abs")))
					{
						$this->absentSubjectAry[] = $SubjectID;
					}

					// [2020-0724-1025-03073] display '---' if input grade + special case - absent / N.A.
                    if($ScaleDisplay == "G" && in_array($thisMark, array("+", "-", "/", "abs", "Abs", "N.A."))) {
                        $orderDisplay = $this->EmptySymbol;
                    }

					# check special case
//					if ($CalculationMethod==2 && $isSub)
//					{
//						$thisMarkDisplay = $this->EmptySymbol;
//					}
//					else
//					{
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
						if($needStyle)
						{
	 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							else
								$thisMarkTemp = $thisMark;
								
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
						}
						else
							$thisMarkDisplay = $thisMark;
//					}
					$thisMarkDisplay = $thisMarkDisplay==$this->EmptySymbol? $thisMarkDisplay : $prefix.$thisMarkDisplay.$postfix; 
					
  					$x[$SubjectID] .= "<td class='border_left tabletext' align='center'>$thisMarkDisplay</td>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center'>$orderDisplay</td>";
				} 
//				else
//				{
//					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
//				}			
					
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
				
				$isFirst = 0;
			}
		} 		
		# Term Report Type [End]
		# Whole Year Report Type
		else
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Terms
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $ReportID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
							
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				// italic for component subject
				$prefix = "<i>";
				$postfix = "</i>";
				$CmpSubjectArr = array();
				$isParentSubject = 0;
				// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					// remove italic for component subject
					$prefix = "";
					$postfix = "";
					$ParentSubjectID = $SubjectID;
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				else {
					$ParentSubjectID = $this->GET_PARENT_SUBJECT_ID($SubjectID);
				}
				
				// check if Subject need to display order of Class
				$displayClassOrder = in_array($SubjectCodeMappping[$ParentSubjectID], (array)$eRCTemplateSetting['Report']['ReportGeneration']['NotSubjGroupOrderList']);
				
				$isAllNA = true;
				
				// for subject with grade input and display
				$GradeInputDisplay = $ScaleInput=="G" && $ScaleDisplay=="G";
				
	  			$thisFullMark = $SubjectFullMarkAry[$SubjectID];
	  			$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID IS NULL and SubjectID = '$SubjectID' ");
				$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
				$FullMark = $ScaleDisplay=="M"? ($UseWeightedMark ? $thisFullMark * $thisSubjectWeight : $thisFullMark) : $thisFullMark;
				
				$x[$SubjectID] .= "<td class='border_left' align='center'>".$prefix.$FullMark.$postfix."</td>";
				
				# Terms's Assesment / Terms Result
				for($i=0; $i<sizeof($ColumnData); $i++)
				{
					# See if any term reports available
					$thisReport = $this->returnReportTemplateBasicInfo("", "Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
					
					if($FormNumber == 6)
					{
					    global $DisplayColumnNum;

						# Retrieve Extra Report Infomation for F6
						$extraReportInfo = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $ColumnData[$i]['SemesterNum'], 0);
						$extraReportID = $extraReportInfo["ReportID"];
						$extraMarksAry = $this->getMarks($extraReportID, $StudentID,'', 0, 1);
					
						$extraColoumnTitle = $this->returnReportColoumnTitle($extraReportID);
						if(sizeof($extraColoumnTitle) > 0){
							foreach ($extraColoumnTitle as $extraColumnID[] => $extraColumnTitle[]){
								break;
							}
						}
						$extraColumn = $extraColumnID[0];
						
						# Retrieve Subject Scheme Settings
						$extraSubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $extraReportID);
						$extraScaleDisplay = $extraSubjectFormGradingSettings['ScaleDisplay'];
						$extraScaleInput = $extraSubjectFormGradingSettings['ScaleInput'];
						
						# Retrieve Subject Weighting
						$thisExtraSubjectWeightData = $this->returnReportTemplateSubjectWeightData($extraReportID, "ReportColumnID='$extraColumn' and SubjectID = '$SubjectID' ");
						$thisExtraSubjectWeight = $thisExtraSubjectWeightData[$extraColumn]['Weight'];
						
						# Retrieve Subject Mark / Grade
						$thisExtraMSGrade = $extraMarksAry[$SubjectID][$extraColumn]['Grade'];
						$thisExtraMSMark = $extraMarksAry[$SubjectID][$extraColumn]['Mark'];
								
						$thisExtraGrade = ($extraScaleDisplay=="G" || $extraScaleDisplay=="" || $thisExtraMSGrade!='' )? $thisExtraMSGrade : "";
						$thisextraMark = ($extraScaleDisplay=="M" && $thisExtraGrade=='') ? $thisExtraMSMark : "";
						
						# Retrieve Order
//						if($FormNumber > 3)
//						{
//							if($displayClassOrder){
//								$thisOrder = $extraMarksAry[$SubjectID][$extraColumn]['OrderMeritClass'];
//								$orderTotalNumber = $extraMarksAry[$SubjectID][$extraColumn]['ClassNoOfStudent'];
//							}
//							else{
//								$thisOrder = $extraMarksAry[$SubjectID][$extraColumn]['OrderMeritSubjectGroup'];
//								$orderTotalNumber = $extraMarksAry[$SubjectID][$extraColumn]['SubjectGroupNoOfStudent'];
//							}
//							$extraOrderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
//							if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeClass > 0 && $thisOrder > $OverallPositionRangeClass))
//							{
//								$extraOrderDisplay = $this->EmptySymbol;
//							}
//						}
//						else 
//						{
						// always show form number
						$thisOrder = $extraMarksAry[$SubjectID][$extraColumn]['OrderMeritForm'];
						$orderTotalNumber = $extraMarksAry[$SubjectID][$extraColumn]['FormNoOfStudent'];
						$extraOrderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
						if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeForm > 0 && $thisOrder > $OverallPositionRangeForm))
						{
							$extraOrderDisplay = $this->EmptySymbol;
						}
//						}
						
						# for preview purpose
						if(!$StudentID)
						{
							$thisextraMark 	= $extraScaleDisplay=="M" ? "S" : "";
							$thisExtraGrade = $extraScaleDisplay=="G" ? "G" : "";
						}
						$thisextraMark = ($extraScaleDisplay=="M" && strlen($thisextraMark)) ? $thisextraMark : $thisExtraGrade;
						
						if ($thisextraMark != "N.A." && $thisextraMark != "")
						{
							$isAllNA = false;
						}
						
						# store Subject ID if absent
						if(in_array($thisextraMark, array("+", "-", "abs")))
						{
							$this->absentSubjectAry[] = $SubjectID;
						}

                        // [2020-0724-1025-03073] display '---' if input grade + special case - absent / N.A.
                        if($extraScaleDisplay == "G" && in_array($thisextraMark, array("+", "-", "/", "abs", "Abs", "N.A."))) {
                            $extraOrderDisplay = $this->EmptySymbol;
                        }
						
						# check special case
						list($thisextraMark, $needStyle) = $this->checkSpCase($extraReportID, $SubjectID, $thisextraMark, $extraMarksAry[$SubjectID][$extraColumn]['Grade']);
						
						if($needStyle)
						{
							if ($extraScaleDisplay=="M" && $thisExtraSubjectWeight>0)
							{
								$thisExtraMarkTemp = ($UseWeightedMark && $thisExtraSubjectWeight!=0) ? $thisextraMark/$thisExtraSubjectWeight : $thisextraMark;
							}
							else
							{
								$thisExtraMarkTemp = $thisextraMark;
							}
							
							$thisextraMarkDisplay = $this->Get_Score_Display_HTML($thisextraMark, $extraReportID, $ClassLevelID, $SubjectID, $thisExtraMarkTemp);
						}
						else
						{
							$thisextraMarkDisplay = $thisextraMark;
						}
						$thisextraMarkDisplay = $thisextraMarkDisplay==$this->EmptySymbol? $thisextraMarkDisplay : $prefix.$thisextraMarkDisplay.$postfix; 
						
						$x[$SubjectID] .= "<td class='border_left tabletext' align='center'>$thisextraMarkDisplay</td>";
						$x[$SubjectID] .= "<td class='tabletext' align='center'>$extraOrderDisplay</td>";

                        $isHideExamColMode = ($DisplayColumnNum == 2);
						if($isHideExamColMode)
                        {
                            // Hide exam column
                        }
                        else
                        {
                            # Display Semester Report Result Column
                            $thisReportID = $thisReport['ReportID'];
                            $ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);

                            $ColumnID = array();
                            $ColumnTitle = array();
                            foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);

                            $thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);

                            for($j=0; $j<sizeof($ColumnID); $j++)
                            {
                                $thisColumnID = $ColumnID[$j];
                                $columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
                                $columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
                                $columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];

                                $isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
                                $isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);

                                $thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnID[$j] ."' and SubjectID = '$SubjectID' ");
                                $thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];

                                $thisMSGrade = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade'];
                                $thisMSMark = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Mark'];

                                $thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
                                $thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";

							    //# Retrieve Order
                                //if($FormNumber > 3)
                                //{
                                //    if($displayClassOrder){
                                //        $thisOrder = $thisMarksAry[$SubjectID][$ColumnID[$j]]['OrderMeritClass'];
                                //        $orderTotalNumber = $thisMarksAry[$SubjectID][$ColumnID[$j]]['ClassNoOfStudent'];
                                //    }
                                //    else{
                                //        $thisOrder = $thisMarksAry[$SubjectID][$ColumnID[$j]]['OrderMeritSubjectGroup'];
                                //        $orderTotalNumber = $thisMarksAry[$SubjectID][$ColumnID[$j]]['SubjectGroupNoOfStudent'];
                                //    }
                                //    $orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
                                //    if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeClass > 0 && $thisOrder > $OverallPositionRangeClass))
                                //    {
                                //        $orderDisplay = $this->EmptySymbol;
                                //    }
                                //}
                                //else
                                //{
                                // always show form number
                                $thisOrder = $thisMarksAry[$SubjectID][$ColumnID[$j]]['OrderMeritForm'];
                                $orderTotalNumber = $thisMarksAry[$SubjectID][$ColumnID[$j]]['FormNoOfStudent'];
                                $orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
                                if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeForm  > 0 && $thisOrder > $OverallPositionRangeForm))
                                {
                                    $orderDisplay = $this->EmptySymbol;
                                }
							    //}

                                $reportcolumn_cond = "ReportColumnID IS NULL";

                                # for preview purpose
                                if(!$StudentID)
                                {
                                    $thisMark 	= $ScaleDisplay=="M" ? "S" : "";
                                    $thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
                                }
                                $thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;

                                if ($thisMark != "N.A." && $thisMark != "")
                                {
                                    $isAllNA = false;
                                }

                                if(in_array($thisMark, array("+", "-", "abs")))
                                {
                                    $this->absentSubjectAry[] = $SubjectID;
                                }

                                // [2020-0724-1025-03073] display '---' if input grade + special case - absent / N.A.
                                if($ScaleDisplay == "G" && in_array($thisMark, array("+", "-", "/", "abs", "Abs", "N.A."))) {
                                    $orderDisplay = $this->EmptySymbol;
                                }

                                # check special case
                                list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);

                                # check special case
                                list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);

                                if($needStyle)
                                {
                                    $thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
                                }
                                else
                                {
                                    $thisMarkDisplay = $thisMark;
                                }
                                $thisMarkDisplay = $thisMarkDisplay==$this->EmptySymbol? $thisMarkDisplay : $prefix.$thisMarkDisplay.$postfix;

                                $x[$SubjectID] .= "<td class='border_left tabletext' align='center'>$thisMarkDisplay</td>";
                                $x[$SubjectID] .= "<td class='tabletext' align='center'>$orderDisplay</td>";
                            }
                        }
					}
					else 
					{
						# if no term reports, the term mark should be entered directly
						if (empty($thisReport))
						{
							$thisReportID = $ReportID;
							$thisReportColumnID = $ColumnData[$i]["ReportColumnID"];
							$thisMarksAry = $this->getMarks($thisReportID, $StudentID, "", "", 1);
							
							$thisMSGrade = $thisMarksAry[$SubjectID][$thisReportColumnID]["Grade"];
							$thisMSMark = $thisMarksAry[$SubjectID][$thisReportColumnID]["Mark"];
							
							$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
							$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
							
							# Retrieve Order
//							if($FormNumber > 3)
//							{
//								if($displayClassOrder){
//									$thisOrder = $MarksAry[$SubjectID][$thisReportColumnID]['OrderMeritClass'];
//									$orderTotalNumber = $MarksAry[$SubjectID][$thisReportColumnID]['ClassNoOfStudent'];
//								}
//								else{
//									$thisOrder = $MarksAry[$SubjectID][$thisReportColumnID]['OrderMeritSubjectGroup'];
//									$orderTotalNumber = $MarksAry[$SubjectID][$thisReportColumnID]['SubjectGroupNoOfStudent'];
//								}
//								$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
//								if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeClass > 0 && $thisOrder > $OverallPositionRangeClass))
//								{ 
//									$orderDisplay = $this->EmptySymbol;
//								}
//							} 
//							else 
//							{
							// always show form number
							$thisOrder = $MarksAry[$SubjectID][$thisReportColumnID]['OrderMeritForm'];
							$orderTotalNumber = $MarksAry[$SubjectID][$thisReportColumnID]['FormNoOfStudent'];
							$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
							if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeForm  > 0 && $thisOrder > $OverallPositionRangeForm))
							{
								$orderDisplay = $this->EmptySymbol;
							}
//							}
							
							$reportcolumn_cond = "ReportColumnID='$thisReportColumnID'";
						} 
						else 
						{
							$thisReportID = $thisReport['ReportID'];
							$thisReportColumnID = 0;
							$thisMarksAry = $this->getMarks($thisReportID, $StudentID, "", "", 1);
							
							$thisMSGrade = $thisMarksAry[$SubjectID][$thisReportColumnID]["Grade"];
							$thisMSMark = $thisMarksAry[$SubjectID][$thisReportColumnID]["Mark"];
							
							$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
							$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
							
							// for empty Grade Input
							if($GradeInputDisplay && $thisGrade=="")
							{
								// display N.A. if term column weight is zero 
								if($this->IS_ALL_REPORT_COLUMN_WEIGHTS_ZERO($ReportID, array($SubjectID), $ColumnData[$i]["ReportColumnID"])===1)
									$thisGrade = "N.A.";
							}
							
							# Retrieve Order
//							if($FormNumber > 3)
//							{
//								if($displayClassOrder){
//									$thisOrder = $thisMarksAry[$SubjectID][$thisReportColumnID]['OrderMeritClass'];
//									$orderTotalNumber = $thisMarksAry[$SubjectID][$thisReportColumnID]['ClassNoOfStudent'];
//								}
//								else{
//									$thisOrder = $thisMarksAry[$SubjectID][$thisReportColumnID]['OrderMeritSubjectGroup'];
//									$orderTotalNumber = $thisMarksAry[$SubjectID][$thisReportColumnID]['SubjectGroupNoOfStudent'];
//								}
//								$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
//								if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeClass > 0 && $thisOrder > $OverallPositionRangeClass)) 
//								{
//									$orderDisplay = $this->EmptySymbol;
//								}
//							} 
//							else 
//							{
							// always show form number
							$thisOrder = $thisMarksAry[$SubjectID][$thisReportColumnID]['OrderMeritForm'];
							$orderTotalNumber = $thisMarksAry[$SubjectID][$thisReportColumnID]['FormNoOfStudent'];
							$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
							if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeForm  > 0 && $thisOrder > $OverallPositionRangeForm))
							{
								$orderDisplay = $this->EmptySymbol;
							}
							
							// Display N.A. if current subject input and display grade
							$orderDisplay = $GradeInputDisplay? "N.A." : $orderDisplay;
//							}
							
							$reportcolumn_cond = "ReportColumnID IS NULL";
						}
	
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "$reportcolumn_cond and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
													
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						
						# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
						
						if ($thisMark != "N.A." && $thisMark != "")
						{
							$isAllNA = false;
						}
						
						if(in_array($thisMark, array("+", "-", "abs")))
						{
							$this->absentSubjectAry[] = $SubjectID;
						}

                        // [2020-0724-1025-03073] display '---' if input grade + special case - absent / N.A.
                        if($ScaleDisplay == "G" && in_array($thisMark, array("+", "-", "/", "abs", "Abs", "N.A."))) {
                            $orderDisplay = $this->EmptySymbol;
                        }
					
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
						
						if($needStyle)
						{
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
						$thisMarkDisplay = $thisMarkDisplay==$this->EmptySymbol? $thisMarkDisplay : $prefix.$thisMarkDisplay.$postfix; 
	
	  					$x[$SubjectID] .= "<td class='border_left tabletext' align='center'>$thisMarkDisplay</td>";
			  			$x[$SubjectID] .= "<td class='tabletext' align='center'>$orderDisplay</td>";
					}
				}
				
				# Subject Overall
				if($ShowSubjectOverall)
				{
					# Display Semester Report Result Column
					$thisReport = $this->returnReportTemplateBasicInfo("", "Semester='".$ColumnData[(count($ColumnData)-1)]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					
					$ColumnID = array();
					$ColumnTitle = array();
					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
						
					$thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
					
					$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
					$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
					
					# Retrieve Order
//					if($FormNumber > 3)
//					{
//						if($displayClassOrder){
//							$thisOrder = $MarksAry[$SubjectID][0]['OrderMeritClass'];
//							$orderTotalNumber = $MarksAry[$SubjectID][0]['ClassNoOfStudent'];
//						}
//						else{
//							$thisOrder = $thisMarksAry[$SubjectID][0]['OrderMeritSubjectGroup'];
//							$orderTotalNumber = $thisMarksAry[$SubjectID][0]['SubjectGroupNoOfStudent'];
//						}
//						$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
//						if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeClass > 0 && $thisOrder > $OverallPositionRangeClass)) 
//							$orderDisplay = $this->EmptySymbol;
//					}
//					else
//					{
					// always show form number
					$thisOrder = $MarksAry[$SubjectID][0]['OrderMeritForm'];
					$orderTotalNumber = $MarksAry[$SubjectID][0]['FormNoOfStudent'];
					$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
					if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeForm > 0 && $thisOrder > $OverallPositionRangeForm))
						$orderDisplay = $this->EmptySymbol;
//					}

					// Display N.A. if current subject input grade
					$orderDisplay = $GradeInputDisplay? "N.A." : $orderDisplay;
					
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					if ($thisMark != "N.A." && $thisMark != "") 
					{
						$isAllNA = false;
					}
					
					if(in_array($thisMark, array("+", "-", "abs")))
					{
						$this->absentSubjectAry[] = $SubjectID;
					}

                    // [2020-0724-1025-03073] display '---' if input grade + special case - absent / N.A.
                    if($ScaleDisplay == "G" && in_array($thisMark, array("+", "-", "/", "abs", "Abs", "N.A."))) {
                        $orderDisplay = $this->EmptySymbol;
                    }

					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
						$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
					}
					else
					{
						$thisMarkDisplay = $thisMark;
					}
					$thisMarkDisplay = $thisMarkDisplay==$this->EmptySymbol? $thisMarkDisplay : $prefix.$thisMarkDisplay.$postfix; 
					
					$x[$SubjectID] .= "<td class='border_left tabletext' align='center'>$thisMarkDisplay</td>";
					$x[$SubjectID] .= "<td class='tabletext' align='center'>$orderDisplay</td>";					
					
				} 
//				else 
//				{
//					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
//				}
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
				
				$isFirst = 0;
			}
		}	
		# Whole Year Report Type [End]
		
		return $returnArr;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array()) {
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		# Retrieve Report Display Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
 		$isMainReport 				= $ReportSetting['isMainReport'];
		$isExtraReport 				= !$isMainReport;
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ShowRightestColumn 		= $this->Is_Show_Rightest_Column($ReportID);
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];

		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

        $hideDemeritCol = array();
        $colIndex = 0;
		
		# Retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName = $this->Get_Student_ClassName($StudentID);
			$WebSamsRegNo = $lu->WebSamsRegNo;
		}
		
		# Retrieve Result Data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID, '', 0, 1);
		
		$GrandTotal = $StudentID ? $this->Get_Score_Display_HTML($result['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "#";
		$GrandAverage = $StudentID ? $this->Get_Score_Display_HTML($result['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "#";

        // [2020-0724-1025-03073]
        if($this->schoolYear == '2019' && $StudentID)
        {
            $excludedStudentIDArr = $this->GET_EXCLUDE_ORDER_STUDENTS($ReportID);
            $isExcludedStudent = in_array($StudentID, (array)$excludedStudentIDArr);
            if ($isExcludedStudent && $result['OrderMeritForm'] == -1) {
                $GrandTotal = $this->EmptySymbol;
                $GrandAverage = $this->EmptySymbol;
            }
        }
		
		$ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
  		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
  		$ClassNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
  		$FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
		
		if($isMainReport)
		{	
			# Retrieve Extra Report (Test) Display Settings
			$extraReportInfo = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $SemID, 0);
			$extraReportID = $extraReportInfo["ReportID"];
			
			$extraMarksAry = $this->getMarks($extraReportID, $StudentID,'', 0, 1);
			
			$extraColoumnTitle = $this->returnReportColoumnTitle($extraReportID);
			if(sizeof($extraColoumnTitle) > 0){
				foreach ($extraColoumnTitle as $extraColumnID[] => $extraColumnTitle[]){
					break;
				}
			}
				
			$extraColumn = $extraColumnID[0];
			
			# Display Extra Report (Test) Result Column
			$extraColumnResult = $this->getReportResultScore($extraReportID, $extraColumn, $StudentID, '', 1, 1);
			
			$columnTotal[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['GrandTotal'], $extraReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
			$columnAverage[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['GrandAverage'], $extraReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";

			// [2020-0724-1025-03073]
            //$hideDemeritCol[$colIndex] = false;
            if($this->schoolYear == '2019' && $StudentID)
            {
                $extraExcludedStudentIDArr = $this->GET_EXCLUDE_ORDER_STUDENTS($extraReportID);
                $isExtraExcludedStudent = in_array($StudentID, (array)$extraExcludedStudentIDArr);
                if ($isExtraExcludedStudent && $extraColumnResult['OrderMeritForm'] == -1) {
                    $columnTotal[$extraColumn] = $this->EmptySymbol;
                    $columnAverage[$extraColumn] = $this->EmptySymbol;
                    // $hideDemeritCol[$colIndex] = true;
                }
            }
            //$colIndex++;
			
			$columnClassPos[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['OrderMeritClass'], $extraReportID, $ClassLevelID, '', '', 'ClassPosition') : "S";
			$columnFormPos[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['OrderMeritForm'], $extraReportID, $ClassLevelID, '', '', 'FormPosition') : "S";
			$columnClassNumOfStudent[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['ClassNoOfStudent'], $extraReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
			$columnFormNumOfStudent[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['FormNoOfStudent'], $extraReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";	
		}
		
		if($ReportType == "W" && $FormNumber == 6)
		{
			$termReport = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
			$termReportID = $termReport[0]["ReportID"];
			$ColumnTitle = $this->returnReportColoumnTitle($termReportID);
			
			# Retrieve Extra Report (Test) Display Settings
			$extraReportInfo = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $termReport[0]["Semester"], 0);
			$extraReportID = $extraReportInfo["ReportID"];
			
			$extraMarksAry = $this->getMarks($extraReportID, $StudentID,'', 0, 1);
			
			$extraColoumnTitle = $this->returnReportColoumnTitle($extraReportID);
			if(sizeof($extraColoumnTitle) > 0){
				foreach ($extraColoumnTitle as $extraColumnID[] => $extraColumnTitle[]){
					break;
				}
			}
				
			$extraColumn = $extraColumnID[0];
			
			# Display Extra Report (Test) Result Column
			$extraColumnResult = $this->getReportResultScore($extraReportID, $extraColumn, $StudentID, '', 1, 1);
				
			$columnTotal[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['GrandTotal'], $extraReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
			$columnAverage[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['GrandAverage'], $extraReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";

            // [2020-0724-1025-03073]
            $hideDemeritCol[$colIndex] = false;
            if($this->schoolYear == '2019' && $StudentID)
            {
                $extraExcludedStudentIDArr = $this->GET_EXCLUDE_ORDER_STUDENTS($extraReportID);
                $isExtraExcludedStudent = in_array($StudentID, (array)$extraExcludedStudentIDArr);
                if ($isExtraExcludedStudent && $extraColumnResult['OrderMeritForm'] == -1) {
                    $columnTotal[$extraColumn] = $this->EmptySymbol;
                    $columnAverage[$extraColumn] = $this->EmptySymbol;
                    $hideDemeritCol[$colIndex] = true;
                }
            }
            $colIndex++;
			
			$columnClassPos[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['OrderMeritClass'], $extraReportID, $ClassLevelID, '', '', 'ClassPosition') : "S";
			$columnFormPos[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['OrderMeritForm'], $extraReportID, $ClassLevelID, '', '', 'FormPosition') : "S";
			$columnClassNumOfStudent[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['ClassNoOfStudent'], $extraReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
			$columnFormNumOfStudent[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['FormNoOfStudent'], $extraReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";
			
			foreach ((array)$ColumnTitle as $ColumnID => $ColumnName) 
			{
				$columnResult = $this->getReportResultScore($termReportID, $ColumnID, $StudentID, '', 1, 1);
				
				$columnTotal[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandTotal'], $termReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
				$columnAverage[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandAverage'], $termReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";

                // [2020-0724-1025-03073]
                $hideDemeritCol[$colIndex] = false;
                if($this->schoolYear == '2019' && $StudentID)
                {
                    $termExcludedStudentIDArr = $this->GET_EXCLUDE_ORDER_STUDENTS($termReportID);
                    $isTermExcludedStudent = in_array($StudentID, (array)$termExcludedStudentIDArr);
                    if ($isTermExcludedStudent && $columnResult['OrderMeritForm'] == -1) {
                        $columnTotal[$ColumnID] = $this->EmptySymbol;
                        $columnAverage[$ColumnID] = $this->EmptySymbol;
                        $hideDemeritCol[$colIndex] = true;
                    }
                }
                $colIndex++;
				
				$columnClassPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $termReportID, $ClassLevelID, '', '', 'ClassPosition') : "S";
				$columnFormPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $termReportID, $ClassLevelID, '', '', 'FormPosition') : "S";
				$columnClassNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $termReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
				$columnFormNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $termReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";
				
				$GrandTotal = $StudentID ? $columnTotal[$ColumnID] : "#";
				$GrandAverage = $StudentID ? $columnAverage[$ColumnID] : "#";
				
				$ClassPosition = $StudentID ? $columnClassPos[$ColumnID] : "#";
		  		$FormPosition = $StudentID ? $columnFormPos[$ColumnID] : "#";
		  		$ClassNumOfStudent = $StudentID ? $columnClassNumOfStudent[$ColumnID] : "#";
		  		$FormNumOfStudent = $StudentID ? $columnFormNumOfStudent[$ColumnID] : "#";
			}
		}
		else
		{
			foreach ((array)$ColumnTitle as $ColumnID => $ColumnName)
			{
				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID, '', 1,1);
				
				$columnTotal[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
				$columnAverage[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";

                // [2020-0724-1025-03073]
                $hideDemeritCol[$colIndex] = false;
                if($this->schoolYear == '2019' && $StudentID)
                {
                    $ExcludedStudentIDArr = $this->GET_EXCLUDE_ORDER_STUDENTS($ReportID);
                    $isExcludedStudent = in_array($StudentID, (array)$ExcludedStudentIDArr);
                    if ($isExcludedStudent && $columnResult['OrderMeritForm'] == -1) {
                        $columnTotal[$ColumnID] = $this->EmptySymbol;
                        $columnAverage[$ColumnID] = $this->EmptySymbol;
                        $hideDemeritCol[$colIndex] = true;
                    }
                }
                $colIndex++;
				
				$columnClassPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "S";
				$columnFormPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "S";
				$columnClassNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
				$columnFormNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";
			
				// only display first column if is term test report
				if($isExtraReport) break;
			}
		}

		$first = 1;
		
		# Overall Result
		if($ShowGrandTotal)
		{
			$thisTitleEn = $eReportCard['Template']['MSTable']['OverallResultEn'];
			$thisTitleCh = $eReportCard['Template']['MSTable']['OverallResultCh'];
			$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnTotal, $GrandTotal, $first);
			
			$first = 0;
		}
		
		if ($eRCTemplateSetting['HideCSVInfo'] != true)
		{
			# Average Mark 
			if($ShowGrandAvg)
			{
                // [2020-0427-1025-28207]
                if($this->schoolYear == '2019' && $FormNumber == 6 && $ReportType == 'W' && in_array($StudentID, array(3294, 3484, 3548, 3554))) {
                    $GrandAverage = $this->EmptySymbol;
                }

                $thisTitleEn = $eReportCard['Template']['MSTable']['AvgMarkEn'];
				$thisTitleCh = $eReportCard['Template']['MSTable']['AvgMarkCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnAverage, $GrandAverage, $first);
				
				$first = 0;
			}
			
			# Position in Class - Senior
//			if($ShowOverallPositionClass && $FormNumber > 3)
			if($ShowOverallPositionClass)
			{
                // [2020-0427-1025-28207]
                if($this->schoolYear == '2019' && $FormNumber == 6 && $ReportType == 'W' && in_array($StudentID, array(3294, 3484, 3554))) {
                    $ClassPosition = $this->EmptySymbol;
                }

                $thisTitleEn = $eReportCard['Template']['MSTable']['ClassPositionEn'];
				$thisTitleCh = $eReportCard['Template']['MSTable']['ClassPositionCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassPos, $ClassPosition, $first, $columnClassNumOfStudent, $ClassNumOfStudent);
				
				$first = 0;
			}
			
//			# Number of Students in Class 
//			if($ShowNumOfStudentClass)
//			{
//				$thisTitleEn = $eReportCard['Template']['MSTable']['ClassNumOfStudentEn'];
//				$thisTitleCh = $eReportCard['Template']['MSTable']['ClassNumOfStudentCh'];
//				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassNumOfStudent, $ClassNumOfStudent, $first);
//				
//				$first = 0;
//			}
			
			# Position in Form - Junior
//			if($ShowOverallPositionForm && $FormNumber && $FormNumber < 4)
			if($ShowOverallPositionForm)
			{
				$thisTitleEn = $eReportCard['Template']['MSTable']['FormPositionEn'];
				$thisTitleCh = $eReportCard['Template']['MSTable']['FormPositionCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormPos, $FormPosition, $first, $columnFormNumOfStudent, $FormNumOfStudent);
				
				$first = 0;
			}
			
			# Number of Students in Form 
//			if($ShowNumOfStudentForm)
//			{
//				$thisTitleEn = $eReportCard['Template']['MSTable']['FormNumOfStudentEn'];
//				$thisTitleCh = $eReportCard['Template']['MSTable']['FormNumOfStudentCh'];
//				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormNumOfStudent, $FormNumOfStudent, $first);
//				
//				$first = 0;
//			}
			
			##################################################################################
			# CSV related
			##################################################################################
			
			# build data array
			$ary = array();
			
			# Get Other Info from CSV
			if ($StudentID != '')
			{
				$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID,$StudentID);
				$ary = $OtherInfoDataAry[$StudentID];
			}
			
			if($SemID=="F")
			{
				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
				
				# calculate sems/assesment column
				$ColNum2Ary = array();
				foreach($ColumnData as $k1=>$d1)
				{
					$TermID = $d1['SemesterNum'];
//					if($d1['IsDetails']==1)
//					{
//						# check sems/assesment col
//						$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
//						$thisReportID = $thisReport['ReportID'];
//						
//						$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
//						$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1;
//					}
//					else
//					{
						$ColNum2Ary[$TermID] = 0;
//					}
				}
			}

			$border_top = $first ? "border_top" : "";
			
			# Days Absent 
			$thisTitleEn = $eReportCard['Template']['MSTable']['DaysAbsentEn'];
			$thisTitleCh = $eReportCard['Template']['MSTable']['DaysAbsentCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Days Absent", $ary);
			
			# Times Late
			$thisTitleEn = $eReportCard['Template']['MSTable']['TimesLateEn'];
			$thisTitleCh = $eReportCard['Template']['MSTable']['TimesLateCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Times Late", $ary);

            # Early Leave
            $thisTitleEn = $eReportCard['Template']['MSTable']['EarlyLeaveEn'];
            $thisTitleCh = $eReportCard['Template']['MSTable']['EarlyLeaveCh'];
            $x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Early Leave", $ary);
			
			# Conduct - Semester Report
			if($isMainReport)
			{
				$thisTitleEn = $eReportCard['Template']['MSTable']['ConductEn'];
				$thisTitleCh = $eReportCard['Template']['MSTable']['ConductCh'];
				$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Conduct", $ary);
			}
			
			# Demerits
			$thisTitleEn = $eReportCard['Template']['MSTable']['DemeritEn'];
			$thisTitleCh = $eReportCard['Template']['MSTable']['DemeritCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Demerits", $ary, $hideDemeritCol);
		}
		
		return $x;
	}
	
	function getMiscTable($ReportID, $StudentID='', $PrintTemplateType=''){
		
	}
	
	function getMeritAndECATable($ReportID, $StudentID='') {
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting, $DisplayColumnNum;
		
		# Retrieve Report Display Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport 				= $ReportSetting['isMainReport'];
		$isExtraReport				= !$isMainReport;
 		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$LineHeight					= $ReportSetting['LineHeight'];
		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
		
		// Form 1 - 5 Whole Year Report
		$NonFormSixYearReport = $FormNumber > 0 && $FormNumber != 6 && $ReportType == "W";
		// Style for Merit Table
		$MeritTableHeaderCol = $NonFormSixYearReport? "style='font-size: 7.5pt; padding-top: 0px; padding-bottom: 0px;'" : "";
		$MeritTableContentData = $NonFormSixYearReport? "style='font-size: 7.5pt;'" : "";
		$MeritTableContentNAData = $NonFormSixYearReport? "style='font-size: 7pt;'" : "";
		// Style for ECA Table
		$ECATableContentData = $NonFormSixYearReport? "style='font-size: 8px;'" : "";
		
		# Term Test Report
		if (($ReportType == "T" && $isExtraReport) || $eRCTemplateSetting['HideCSVInfo']==true)
		{
			return "";
		}
		
		# Get Other Info from CSV
		$OtherInfoDataAry = array();
		if ($StudentID != '') 
		{
			$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID); 
			$ary_content = $OtherInfoDataAry[$StudentID];
		}
		
		# Get Column Number
		$ColumnNum = $this->Get_Subject_Col_Num($ReportID);
		$columnTermWidth = ($ReportType == "W" && $FormNumber != 6) ? "35%" : "50%";

        $isHideExamColMode = ($DisplayColumnNum == 2);
        if($isHideExamColMode && ($ReportType == "W" && $FormNumber == 6)) {
            $ColumnNum = $ColumnNum - 2;
        }
		
		# Get Last Involved Term 
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		foreach((array)$sems as $TermID => $TermName)
		{
			$latestTerm = $TermID;
		}
		
		# Get Merit from eDiscipline if CSV not set
		$merit_types = array("MP", "MU", "MH", "GCA", "MECA");
		foreach((array)$sems as $TermID => $TermName)
		{
			# Semesters
			if($TermID != 0)
			{
				list($dataMP, $dataMU, $dataMH) = $this->loadingMeritFromDiscipline($StudentID, $TermID);
				$ary_content[$TermID]["MP"] = $ary_content[$TermID]["MP"] != "" ? $ary_content[$TermID]["MP"] : $dataMP;
				$ary_content[$TermID]["MU"] = $ary_content[$TermID]["MU"] != "" ? $ary_content[$TermID]["MU"] : $dataMU;
				$ary_content[$TermID]["MH"] = $ary_content[$TermID]["MH"] != "" ? $ary_content[$TermID]["MH"] : $dataMH;
				
				if($ReportType != "W" || $FormNumber != 6)
				{
					$ary_content[$TermID]["MECA"] = "N.A.";
				}
			}
		}
		
		# Annual
		// not clear merit value for form 6 consolidated report
		//if($ReportType=="W")
		if($FormNumber != 6 && $ReportType == "W")
		{
			$ary_content[0]["MP"] = "N.A.";
			$ary_content[0]["MU"] = "N.A.";
			$ary_content[0]["MH"] = "N.A.";
		}
		
//		global $UserID;
//		if($UserID == 1){
//			debug_pr($ary_content);
//		}
		
		$x = "";
		$merit = "";
		foreach($merit_types as $type)
		{
			// attain value of different merit type 
			if($type == "GCA"){
				$attain_value = "GCA";
			} 
			else if($type == "MECA"){
				$attain_value = "MECA";
			}
			// for MP, MU, MH
			else {
				$attain_value = "0";
			}
			
			$allInvalid = true;
			
			$current_type_chi = $type."Ch";
			$current_type_eng = $type."En";
			
			$merit_row = "";
			$merit_row .= "<tr>";
			//$merit_row .= "<td class='tabletext merit_table_content' colspan='2' style='padding-left:2px; padding-right:2px;'>&nbsp;".$eReportCard['Template']['MeritTable'][$current_type_chi]." ".$eReportCard['Template']['MeritTable'][$current_type_eng]."</td>";
			$merit_row .= "<td class='tabletext merit_table_content' style='padding-left:2px; padding-right:2px;'>&nbsp;".$eReportCard['Template']['MeritTable'][$current_type_chi]."</td>";
			$merit_row .= "<td class='tabletext merit_table_content' style='padding-left:2px; padding-right:2px;'>".$eReportCard['Template']['MeritTable'][$current_type_eng]."</td>";
			$merit_row .= "<td class='tabletext merit_table_content border_left' colspan='$ColumnNum'>";
			$merit_row .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
				$merit_row .= "<tr>";
				
			# Semesters
			// not loop related terms for form 6 consolidated report, only display Annual column
			if($FormNumber==6 && $ReportType=="W"){
				// do nothing
			}
			else {
				foreach((array)$sems as $TermID => $TermName)
				{
					$merit_value = $ary_content[$TermID][$type];
					if($FormNumber == 6 && $type == "MECA")
					{
						$merit_value = "N.A.";
					}
//					$merit_value = $merit_value!="N.A."? ($merit_value=="X"? $this->EmptySymbol : $this->Get_Tick_Symbol()) : "N.A.";
					$merit_value = $merit_value!="N.A."? ($merit_value!==$attain_value? $this->EmptySymbol : $this->Get_Tick_Symbol()) : "N.A.";
					$MeritTableContentStyle = $merit_value=="N.A."? $MeritTableContentNAData : $MeritTableContentData; 
					$merit_row .= "<td class='tabletext merit_table_content' align='center' width='$columnTermWidth' $MeritTableContentStyle>".$merit_value."</td>";
					
//					$allInvalid = ($merit_value=="N.A." || $merit_value=="X" || $merit_value==$this->EmptySymbol) && $allInvalid;
					$allInvalid = ($merit_value=="N.A." || $merit_value==$this->EmptySymbol) && $allInvalid;
					$td_style = "";
				}
			}
			
			# Annual
			if($ReportType=="W")
			{
				$merit_value = $ary_content[0][$type];
				
				// get merit value from last term
//				if($FormNumber == 6 && ($type == "GCA" || $type == "MECA")){
//				if($FormNumber == 6){
//					$merit_value = $ary_content[$latestTerm][$type];
//					if(($type != "GCA" && $type != "MECA")){
//						$merit_value = $ary_content[0][$type];
//					}
//				}
				
				// special handling for Form 6 Consolidate Report
				// get merit value from last term if annual not set
				if($FormNumber==6 && ($type != "GCA" && $type != "MECA")){
					$merit_value = $merit_value!=""? $merit_value : $ary_content[$latestTerm][$type];
				}
				
//				$merit_value = $merit_value!="N.A."? ($merit_value=="X"? $this->EmptySymbol : $this->Get_Tick_Symbol()) : "N.A.";
				$merit_value = $merit_value!="N.A."? ($merit_value!==$attain_value? $this->EmptySymbol : $this->Get_Tick_Symbol()) : "N.A.";
				$MeritTableContentStyle = $merit_value=="N.A."? $MeritTableContentNAData : $MeritTableContentData; 
				$merit_row .= "<td class='tabletext merit_table_content' align='center' $MeritTableContentStyle>".$merit_value."</td>";
				
//				$allInvalid = ($merit_value=="N.A." || $merit_value=="X" || $merit_value==$this->EmptySymbol) && $allInvalid;
				$allInvalid = ($merit_value=="N.A." || $merit_value==$this->EmptySymbol) && $allInvalid;
			}
				$merit_row .= "</tr>";
			$merit_row .= "</table>";
			$merit_row .= "</td>";
			$merit_row .= "</tr>";
			
//			global $UserID;
//			if($UserID==1){
//				$allInvalid = false;
//			}

			// add merit row if valid values 
			if(!$allInvalid)
			{
				$merit .= $merit_row;
			}
		}
		
//		$x .= "<table class='report_border merit_table' width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='20'>";
//		$x .= "<tr>";
//			$x .= "<td width='100%' valign='top'>";
//			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
//			$x .= "<tr>";
//				$x .= "<td width='100%'>";
//				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
				
				// Merit Table with records
				if($merit != "")
				{
					$x .= "<tr>";
//					$x .= "<td class='tabletext merit_table_header border_right' width=\"40%\" style='padding-left:2px;'>&nbsp;". $eReportCard['Template']['MSTable']['MeritCh']."&nbsp;&nbsp;".$eReportCard['Template']['MSTable']['MeritEn']."</td>";
					$x .= "<td class='tabletext merit_table_header border_top border_bottom' colspan='2' style='vertical-align:middle;'>&nbsp;". $eReportCard['Template']['MSTable']['MeritCh']."&nbsp;&nbsp;".$eReportCard['Template']['MSTable']['MeritEn']."</td>";
					$x .= "<td class='tabletext merit_table_header border_top border_bottom border_left' colspan='$ColumnNum'>";
					$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
						$x .= "<tr>";
						
					if($FormNumber==6 && $ReportType=="W"){
						// do nothing
					}
					else{
						foreach((array)$sems as $TermID => $TermName)
						{
							$term_num = $this->Get_Semester_Seq_Number($TermID);
							
							$current_type_chi = "AttainIn".$term_num."Ch";
							$current_type_eng = "AttainIn".$term_num."En";
							$x .= "<td class='tabletext merit_table_header' align='center' width='$columnTermWidth' $MeritTableHeaderCol>".$eReportCard['Template']['MSTable'][$current_type_chi]."<br>".$eReportCard['Template']['MSTable'][$current_type_eng]."</td>";
						}
					}
					
					if($ReportType=="W")
					{
//						$x .= "<td class='tabletext merit_table_header' align='center' width='$col_width'>".$eReportCard['Template']['MSTable']['AnnualEn']."<br>".$eReportCard['Template']['MSTable']['AnnualCh']."</td>";
						$x .= "<td class='tabletext merit_table_header' align='center' $MeritTableHeaderCol>".$eReportCard['Template']['MSTable']['AnnualCh']."<br>".$eReportCard['Template']['MSTable']['AnnualEn']."</td>";
					}
						$x .= "</tr>";
					$x .= "</table>";
					$x .= "</td>";
					$x .= "</tr>";
					$x .= $merit;
				}
//				else
//				{
//					$emptyCol = $ColumnNum + 2;
//					
//					$x .= "<tr>";
//						$x .= "<td class='tabletext merit_table_content border_top' colspan='$emptyCol' style='padding-top:2px'>". $eReportCard['Template']['MSTable']['MeritCh']."&nbsp;&nbsp;".$eReportCard['Template']['MSTable']['MeritEn']."</td>";
//					$x .= "</tr>";
//					$x .= "<tr>";
////						$x .= "<td class='tabletext merit_table_content'>***</td>";
//						$x .= "<td class='tabletext merit_table_content' colspan='$emptyCol'>".$this->EmptySymbol."</td>";
//					$x .= "</tr>";
//				}
//				$x .= "</table>";	
//				$x .= "</td>";
//			$x .= "</tr>";
//			$x .= "</table>";	
//			$x .="</td>";
//		$x .= "</tr>";
//		$x .= "</table>";
		
		# Get ECA from eEnrollment
//		if($UserID==1 || $ReportType == "W")
		if($ReportType == "W")
		{
			// [2017-0306-1058-39207] Get ECA Info from iPortfolio only
			//$ECA_info = $this->loadECAFromEnrol($StudentID);
			//$ECA_info = $this->loadECAFromiPo($StudentID, $FormNumber==6);
			$ECA_info = $this->loadECAFromiPo($StudentID);
			$info_count = (count($ECA_info) > 12)? 12 : count($ECA_info);
			$ColumnNum = $ColumnNum + 2;
			
			if($info_count > 0){
//				$x .= "<table class='report_border eca_table' width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
				$x .= "<tr>";
//					$x .= "<td width='50%' valign='top'>";
//					$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
//					$x .= "<tr>";
						$x .= "<td class='border_top' width='100%' colspan='$ColumnNum'>";
						$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
						$x .= "<tr>";
							$x .= "<td class='tabletext eca_table_header' width=\"50%\" colspan='2'>&nbsp;". $eReportCard['Template']['MSTable']['ECACh']."&nbsp;".$eReportCard['Template']['MSTable']['ECAEn']."</td>";
						$x .= "</tr>";
						// ECA Table
						for($i=0; $i<6; $i++)
						{
							$current_ECA = $ECA_info[$i];
							
//							if($UserID!=1 && $current_ECA == "")
							if($current_ECA == "")
							{
								break;
							}			
							$club_en_title = $current_ECA['ClubTitle'];
							$last_space_locat = strrpos($club_en_title, " ");
							$club_en_title = $last_space_locat? substr($club_en_title, 0, $last_space_locat) : $club_en_title;			
							
							$current_ECA2 = $ECA_info[$i+6];
							$current_ECA2_display = "";
							if($current_ECA2 != "")
							{
								$club_en_title2 = $current_ECA2['ClubTitle'];
								$last_space_locat2 = strrpos($club_en_title2, " ");
								$club_en_title2 = $last_space_locat2? substr($club_en_title2, 0, $last_space_locat2) : $club_en_title2;
								$current_ECA2_display = $club_en_title2." - ".$current_ECA2['RoleTitle'];
								$current_ECA2_display = $current_ECA2_display? $current_ECA2_display : "&nbsp;";
							}

							$x .= "<tr>";
							$x .= "<td class='eca_table_content' align='left' width='50%' $ECATableContentData>$club_en_title - ".$current_ECA['RoleTitle']."</td>";
							$x .= "<td class='eca_table_content' align='left' width='50%' $ECATableContentData>$current_ECA2_display</td>";
							$x .= "</tr>";
							
							// display at most 8 ECA records for F1-F3
//							if($FormNumber && $FormNumber<=3 && $i==3)
//								break;

							// display at most 12 ECA records
							if($i==5)
								break;
						}
						
						$x .= "</table>";	
						$x .= "</td>";
//					$x .= "</tr>";
//					$x .= "</table>";	
//					$x .="</td>";
				$x .= "</tr>";
//				$x .= "</table>";
			}
		}
		
		return $x;
	}
	
	function getSignatureTable($ReportID='', $StudentID='', $PrintTemplateType='') {
 		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Report Display Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport 				= $ReportSetting['isMainReport'];
		$isExtraReport				= !$isMainReport;
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$SemID 						= $ReportSetting['Semester'];
        $SemesterNumber				= $this->Get_Semester_Seq_Number($SemID);
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$LineHeight					= $ReportSetting['LineHeight'];
		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
 		
		// Form 1 - 5 Whole Year Report
		$NonFormSixYearReport = $FormNumber > 0 && $FormNumber != 6 && $ReportType == "W";
		// Style for Signature Table
		$SignatureTableTop = "style='margin-top: 0px;'";
		
		$SignatureTable = "";

		# Get Other Info from CSV
		$OtherInfoDataAry = array();
		if ($StudentID != '') 
		{
			$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID); 
			$ary_content = $OtherInfoDataAry[$StudentID];
		}
 		
 		if($FormNumber==6)
 		{
 			$termReport = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
 			$termReportID = $termReport[0]["ReportID"];
 			$termSemester = $termReport[0]["Semester"];
 		}
 		
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		$lclass = new libclass();

		# Get Class Teacher
		$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
		$thisClassName = $StudentInfoArr[0]['ClassName'];
		$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
		foreach((array)$ClassTeacherAry as $key=>$val)
		{
			$CTeacher[] = $val['ChineseName'].$eReportCard['Template']['SignatureTable']['TeacherTitle'];
		}
		$ClassTeacher = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
		
		# Get Teacher Comment
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		
		// [2017-1107-1157-14206] for Form 6 Final Report only
		//if($FormNumber==6)
		if($FormNumber==6 && $isMainReport)
		{
			$CommentAry = $this->returnSubjectTeacherComment($termReportID, $StudentID);
		}
		$classteachercomment = $CommentAry[0];

		# Absent Subject and Absent Reason
		$SubjectAry = $this->absentSubjectAry;
		// empty $absentSubjectAry
		$this->absentSubjectAry = array();
		
		// Absent Subject
		$SubjectAry = array_unique($SubjectAry);
		$SubjectName = $isExtraReport? $ary_content[$SemID]["Absent Subject (Test)"] : $ary_content[$SemID]["Absent Subject (Exam)"];
		if($ReportType=="W")
		{
			$SubjectName = $ary_content[0]["Absent Subject (Exam)"];
			if($FormNumber==6)
			{
				$SubjectName = $ary_content[$termSemester]["Absent Subject (Exam)"];
			}
		}
		if(!$SubjectName && count($SubjectAry) > 0)
		{
			$delim = "";
			foreach($SubjectAry as $thisSubjectID){
				$SubjectName .= $delim.$this->GET_SUBJECT_NAME_LANG($thisSubjectID, "CH");
				$delim = "、";
			}
		}
		// Absent Reason
		if($SubjectName)
		{
			$absent_reason = $isExtraReport? $ary_content[$SemID]["Absent Reason (Test)"] : $ary_content[$SemID]["Absent Reason (Exam)"];
			if($ReportType=="W"){
				$absent_reason = $ary_content[0]["Absent Reason (Exam)"];
				if($FormNumber==6)
				{
					$absent_reason = $ary_content[$termSemester]["Absent Reason (Exam)"];
				}
			}
		}
		
		// Font Size (11pt) for Term Test Report
		$font_style = $isExtraReport? "table_11pt_font" : "";
		$td_class = $isExtraReport? "class='table_td_11pt'" : "";
		
		$remarks_type = $isExtraReport? $eReportCard['Template']['MSTable']['TermTestCh'] : $eReportCard['Template']['MSTable']['TestCh'];
		
		// only show teacher remarks for term and whole year report (also 2nd term test report)
		if(!$isExtraReport || ($isExtraReport && strlen(trim($classteachercomment)) > 0))
		{
			$SignatureTable .= "<table class='teacher_comment_td' width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
			$SignatureTable .= "<tr>";
                $SignatureTable .= "<td valign='top' align='left' width='15%'>";
                    $SignatureTable .= $eReportCard['Template']['SignatureTable']['ClassTeacherRemakrsCh']."<br>".$eReportCard['Template']['SignatureTable']['ClassTeacherRemakrsEn'];
                $SignatureTable .= "</td>";
			    //$SignatureTable .= "<td valign='top' style='height:90px; padding:5px;'><div style='word-wrap:break-word; width:660px'>".stripslashes(nl2br($classteachercomment))."</div></td>";
			    $SignatureTable .= "<td valign='top' style='height:30px; padding:3px;'><div style='word-wrap:break-word; width:620px'>".stripslashes(nl2br($classteachercomment))."</div></td>";
			$SignatureTable .= "</tr>";
			$SignatureTable .= "</table>\n";
		}
		
//		global $UserID;
//		if($UserID==1 || $SubjectName != ""){

        // [2020-0427-1025-28207]
        if($this->schoolYear == '2019' && $FormNumber == 6 && $ReportType == 'W' && in_array($StudentID, array(3294, 3484, 3548, 3554)))
        {
            if($StudentID == 3548) {
                $remarks_type = '考試';
            }
            else {
                $remarks_type = '測驗和考試';
            }

            $SignatureTable .= "<table class='absent_remark' width='100%' align='center'>";
            $SignatureTable .= "<tr>";
                $SignatureTable .= "<td>備註　：　因".$absent_reason."缺席".$remarks_type."</td>";
            $SignatureTable .= "</tr>";
            $SignatureTable .= "</table>";
        }
        // [2020-0722-1653-18066]
        else if($this->schoolYear == '2019' && $ReportType == 'W' && in_array($StudentID, array(5190, 5216)))
        {
            if($StudentID == 5190) {
                $absent_term = '第一學期';
            }
            else {
                $absent_term = '第二學期';
            }

            $SignatureTable .= "<table class='absent_remark' width='100%' align='center'>";
            $SignatureTable .= "<tr>";
                $SignatureTable .= "<td>備註　：　學生缺席".$absent_term."考試</td>";
            $SignatureTable .= "</tr>";
            $SignatureTable .= "</table>";
        }
		else if($SubjectName != "")
		{
			$SignatureTable .= "<table class='absent_remark' width='100%' align='center'>";
			$SignatureTable .= "<tr>";
                //$SignatureTable .= "<td valign='top' align='left' width='10%'>&nbsp;</td>";
                //$SignatureTable .= "<td valign='top' width='12%'>備註　： </td>";
                //$SignatureTable .= "<td>因 　 　 　 $absent_reason 　 　 　 缺席 　 　 　 $SubjectName 　 　 　科$remarks_type</td>";
                $SignatureTable .= "<td>備註　：　因".$absent_reason."缺席".$SubjectName."科".$remarks_type."</td>";
			$SignatureTable .= "</tr>";
			$SignatureTable .= "</table>";
		}

        // [2020-0603-1559-06235] not display Comments + Remarks
		if($this->schoolYear == '2019' && $FormNumber != 6 && $SemesterNumber == 1 && $isMainReport)
        {
            $SignatureTable = "";
            $SignatureTable .= "<table class='teacher_comment_td' width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
            $SignatureTable .= "<tr>";
                $SignatureTable .= "<td valign='top' align='left' width='15%'>&nbsp;</td>";
                $SignatureTable .= "<td valign='top' style='height:30px; padding:3px;'><div style='word-wrap:break-word; width:620px'>&nbsp;</div></td>";
            $SignatureTable .= "</tr>";
            $SignatureTable .= "</table>\n";

            $SignatureTable .= "<table class='absent_remark' width='100%' align='center'>";
            $SignatureTable .= "<tr>";
                $SignatureTable .= "<td>&nbsp;</td>";
            $SignatureTable .= "</tr>";
            $SignatureTable .= "</table>";
        }
		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
//		$SettingID = trim($SignatureTitleArray[0]);
//		$TitleEn = $eReportCard["Template"]['SignatureTable'][$SettingID."En"];
//		$TitleCh = $eReportCard["Template"]['SignatureTable'][$SettingID."Ch"];
//		
//		$SignatureTable .= "<table class='signature' height='85px' width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
//		$SignatureTable .= "<tr>";
//		$SignatureTable .= "<td valign='bottom' align='center' class='border_top border_left border_bottom border_right'>";
//		$SignatureTable .= "<table class='sign_footer' cellspacing='0' cellpadding='0' border='0'>";
//		$SignatureTable .= "<tr><td align='center' height='30px' valign='bottom'>$TitleCh<br>&nbsp;</td></tr>";
//		$SignatureTable .= "<tr><td align='center' valign='bottom'>$TitleEn</td></tr>";
//		$SignatureTable .= "</table>";
//		$SignatureTable .= "</td>";
		
		$signatureTableStyle = $isMainReport? "style='margin-top:4px;'" : "";
		$signatureTableStyle = $NonFormSixYearReport? $SignatureTableTop : $signatureTableStyle; 
		$SignatureTable .= "<table class='signature' width='100%' border='0' cellpadding='4' cellspacing='0' align='center' $signatureTableStyle>";
		$SignatureTable .= "<tr>";
		
		$border_left_style = "border_left";
		for($k=1; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$TeacherDisplay = ($SettingID=="ClassTeacher")? "<br>($ClassTeacher)" : "<br>&nbsp;";
			$TeacherDisplay = ($SettingID=="Principal")? "<br>(".$eReportCard['Template']['SignatureTable']['PrincipalName'].")" : $TeacherDisplay;
			
			$TitleEn = $eReportCard["Template"]['SignatureTable'][$SettingID."En"];
			$TitleCh = $eReportCard["Template"]['SignatureTable'][$SettingID."Ch"];
			
			$width_style = $k==1? " width='257px'" : " width='237px'";
			//$line = $k==1? "_____________________________________________" : "_________________________________";
			$line = $k==1? "________________________________________" : "___________________________________";
			
			$SignatureTable .= "<td class='border_top $border_left_style border_right border_bottom' valign='bottom' align='center' $width_style>";
			$SignatureTable .= "<table class='sign_footer $font_style' cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td align='center' height='86px' valign='bottom'>$line</td></tr>";
			$SignatureTable .= "<tr><td $td_class align='center' valign='bottom'>$TitleCh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$TitleEn$TeacherDisplay</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
			
			$border_left_style = "";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	########### END Template Related ##############
	
	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr, $hideDemeritCol=array())
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Report Display Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$isMainReport 				= $ReportSetting['isMainReport'];
		$isExtraReport 				= !$isMainReport;
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowRightestColumn 		= $this->Is_Show_Rightest_Column($ReportID);
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		// Test - Get Start Date and End Date to get AP records
		$ReportStartDate = $ReportSetting['TermStartDate'];
		$ReportEndDate = $ReportSetting['TermEndDate'];
		if($isMainReport || ($isExtraReport && ($ReportStartDate=="0000-00-00" || $ReportEndDate=="0000-00-00"))){
			$ReportStartDate = "";
			$ReportEndDate = "";
		}
		
		# initialization
		$border_top = "";
		$x = "";
		
		if($isMainReport)
		{
			# Retrieve Extra Report (Test) Display Settings
			$extraReportInfo = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $SemID, 0);
			$extraReportID = $extraReportInfo["ReportID"];
			$extraMarksAry = $this->getMarks($extraReportID, $StudentID,'', 0, 1);
		
			$extraColoumnTitle = $this->returnReportColoumnTitle($extraReportID);
			if(sizeof($extraColoumnTitle) > 0){
				foreach ($extraColoumnTitle as $extraColumnID[] => $extraColumnTitle[]) {
					break;
				}
			}
		}
		
		$x .= "<tr>";
		$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh);
		
		# Whole Year Report
		if($ReportType == "W")
		{
		    global $DisplayColumnNum;

			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			if($FormNumber == 6)
			{
				$termReport = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
				$ReportID = $termReport[0]["ReportID"];
				
				# Retrieve Extra Report (Test) Display Settings
				$extraReportInfo = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $termReport[0]["Semester"], 0);
				$extraReportID = $extraReportInfo["ReportID"];
				$extraReportStartDate = $extraReportInfo['TermStartDate'];
				$extraReportEndDate = $extraReportInfo['TermEndDate'];

				$ColumnData = array();
				$ColumnData[] = $this->returnReportTemplateColumnData($extraReportID);
				$ColumnData[] = $this->returnReportTemplateColumnData($termReport[0]["ReportID"]);
			}
			
			$isFirst = true;
			$columnCount = 0;		
			
			$thisTotalValue = "";
			foreach($ColumnData as $k1 => $d1)
			{
				$TermID = $d1['SemesterNum'];
				if($FormNumber == 6)
				{
					$TermID = $termReport[0]["Semester"];		
				}
				
				if($isFirst)
				{
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					$isFirst = false;
//					if($FormNumber == 6)
//					{
//						$x .= "<td class='{$border_top} border_left' align='center' height='{$LineHeight}' colspan='2'>".$this->EmptySymbol."</td>";	
//					}
				}
				
				$thisValue = $ValueArr[$TermID][$InfoKey];
				$thisValue = $StudentID ? ($thisValue ? $thisValue : $this->EmptySymbol) : "#";
//				if($FormNumber == 6 && $columnCount == 0)
//				{
//					$thisValue = $StudentID ? $this->EmptySymbol : "#";
//				}

                // if($InfoKey=="Days Absent" || $InfoKey=="Times Late")
				if ($InfoKey == "Days Absent" || $InfoKey == "Times Late" || $InfoKey == "Early Leave")
				{
					if(!$StudentID)
					{
						$thisValue = "#";
					} 
					else 
					{
						if($FormNumber != 6)
						{
							//$targetKeys = $InfoKey." (Test)";
							//$thisValue1 = $ValueArr[$TermID][$targetKeys];
							//$targetKeys = $InfoKey." (Exam)";
							//$thisValue2 = $ValueArr[$TermID][$targetKeys];
							//$thisValue = $thisValue1 + $thisValue2;
							//$thisValue = $thisValue ? $thisValue : $this->EmptySymbol;
							// Term Column - only get value from exam
							$targetKeys = $InfoKey." (Exam)";
							$thisValue = $ValueArr[$TermID][$targetKeys];
							$thisValue = $thisValue ? $thisValue : 0;
							
							// Year Report - get attendance records within defined period
							if($thisValue == 0)
							{
								$attendanceData = $this->Get_Student_Profile_Attendance_Data($TermID, "", $ReportStartDate, $ReportEndDate, "", array($StudentID));
								$attendanceData = $attendanceData[$StudentID];

								// if($InfoKey=="Days Absent")
								if ($InfoKey == "Days Absent" || $InfoKey == "Early Leave") {
									$thisValue = $attendanceData[$InfoKey] ? $attendanceData[$InfoKey] : 0;
                                } else if ($InfoKey == "Times Late") {
									$thisValue = $attendanceData["Time Late"] ? $attendanceData["Time Late"] : 0;
                                }
							}
						} 
						else
						{
							$targetKeys = $columnCount == 0 ? $InfoKey." (Test)" : $InfoKey." (Exam)";
							$thisValue = $ValueArr[$TermID][$targetKeys];
							//$thisValue = $thisValue ? $thisValue : $this->EmptySymbol;
							$thisValue = $thisValue ? $thisValue : 0;
							
							// Year Report - get attendance records within defined period
							if($thisValue == 0)
							{
								if($columnCount == 0) {
									$attendanceData = $this->Get_Student_Profile_Attendance_Data($TermID, "", $extraReportStartDate, $extraReportEndDate, "", array($StudentID));
                                } else {
									$attendanceData = $this->Get_Student_Profile_Attendance_Data($TermID, "", $ReportStartDate, $ReportEndDate, "", array($StudentID));
                                }
                                $attendanceData = $attendanceData[$StudentID];

								// if($InfoKey=="Days Absent")
								if ($InfoKey == "Days Absent" || $InfoKey == "Early Leave") {
									$thisValue = $attendanceData[$InfoKey] ? $attendanceData[$InfoKey] : 0;
                                } else if ($InfoKey == "Times Late") {
									$thisValue = $attendanceData["Time Late"] ? $attendanceData["Time Late"] : 0;
                                }
							}
						}
					}
				}
				
				# Calculate Total Value
				// F.6 Overall Column - ignore value from Test Column
                // if(($InfoKey == "Days Absent" || $InfoKey == "Times Late") && $StudentID && $FormNumber == 6 && $columnCount == 0 && is_numeric($thisValue)){
                if (($InfoKey == "Days Absent" || $InfoKey == "Times Late" || $InfoKey == "Early Leave") && $StudentID && $FormNumber == 6 && $columnCount == 0 && is_numeric($thisValue))
                {
					// do nothing
				}
				else if (is_numeric($thisValue) && $InfoKey != "Demerits")
				{
					if ($thisTotalValue == "")
					{
						$thisTotalValue = $thisValue;
					}
					else if (is_numeric($thisTotalValue))
					{
						$thisTotalValue += $thisValue;
					}
				}
				
//				$col_num = $ColNum2Ary[$TermID];
//				for($i=0; $i<$col_num; $i++)
//				{
//					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' colspan='2'>".$this->EmptySymbol."</td>";
//				}
					
				# Demerit Record Display
				if($InfoKey == "Demerits")
				{
					if($FormNumber != 6 || $columnCount != 0){
						list($dataMP, $dataMU, $dataMH, $dataMerits) = $this->loadingMeritFromDiscipline($StudentID, $TermID);
						$thisValue = $ValueArr[$TermID][$InfoKey] ? $ValueArr[$TermID][$InfoKey] : intval($dataMerits);
					}
					// [removed] F6 - Year Report - Test Column always show "--"
					else{
						list($dataMP, $dataMU, $dataMH, $dataMerits) = $this->loadingMeritFromDiscipline($StudentID, $SemID, $extraReportStartDate, $extraReportEndDate);
						//$thisValue = "";
						$thisValue = intval($dataMerits);
					}
					
//					if($FormNumber == 6 && $columnCount == 0)
//					{
//						$thisValue = "";
//					}
				
					// F.6 Overall Column - ignore value from Test Column
					if (is_numeric($thisValue) && ($FormNumber != 6 || $columnCount != 0))
					{
						if ($thisTotalValue == "")
						{
							$thisTotalValue = $thisValue;
						}
						else if (is_numeric($thisTotalValue))
						{
							$thisTotalValue += $thisValue;
						}
					}
//					$thisValue = $StudentID ? ($thisValue? $this->GET_CHINESE_MERIT_NUM($thisValue) : $this->EmptySymbol) : "#";
					$thisValue = $StudentID ? ($thisValue ? $this->Get_MERIT_NUM_DISPLAY($thisValue) : $this->Get_MERIT_NUM_DISPLAY(0)) : "#";

					// [2020-0724-1025-03073]
					if($StudentID && isset($hideDemeritCol[$k1]) && $hideDemeritCol[$k1]) {
                        $thisValue = $this->EmptySymbol;
                    }
				}

                $isHideExamColMode = ($DisplayColumnNum == 2);
                if($isHideExamColMode && $FormNumber == 6 && $k1 > 0) {
                    // Hide exam column
                }
                else {
                    $x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' colspan='2'>".$thisValue."</td>";
                    $columnCount++;
                }
			}
			
			# Display Overall value
			if($ShowRightestColumn)
			{
				if($thisTotalValue == "")
				{
					$thisTotalValue = $ValueArr[0][$InfoKey];
					$thisTotalValue = $thisTotalValue == "" ? $this->EmptySymbol : $thisTotalValue;
					
					if($InfoKey == "Demerits")
					{
//						$thisTotalValue = $thisTotalValue!=$this->EmptySymbol? $this->GET_CHINESE_MERIT_NUM($thisTotalValue) : $this->EmptySymbol;
						$thisTotalValue = $thisTotalValue != $this->EmptySymbol ? $this->Get_MERIT_NUM_DISPLAY($thisTotalValue) : $this->Get_MERIT_NUM_DISPLAY(0);
					}
				}
				else if($InfoKey == "Demerits")
				{
//					$thisTotalValue = $this->GET_CHINESE_MERIT_NUM($thisTotalValue);
					$thisTotalValue = $this->Get_MERIT_NUM_DISPLAY($thisTotalValue);
				}

				// [2020-0720-1235-01206] removed
				// Always display N.A. for Annual Demerits
				if($InfoKey == "Demerits" && $FormNumber != 6)
				{
					$thisTotalValue = "N.A.";
				}

				// display 0 for Days Absent and Times Late if data is empty
                // if($thisTotalValue == $this->EmptySymbol && ($InfoKey == "Days Absent" || $InfoKey == "Times Late"))
                if ($thisTotalValue == $this->EmptySymbol && ($InfoKey == "Days Absent" || $InfoKey == "Times Late" || $InfoKey == "Early Leave"))
				{
					$thisTotalValue = 0;
				}
				
				if($FormNumber == 6 && $InfoKey == "Conduct")
				{
					$thisTotalValue = $ValueArr[$termReport[0]["Semester"]][$InfoKey];
					$thisTotalValue = $thisTotalValue == "" ? $this->EmptySymbol : $thisTotalValue;
				}
				
				$thisTotalValue = $StudentID ? $thisTotalValue : "#";
				
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' colspan='2'>".$thisTotalValue."</td>";
			}
		}
		# Term Report
		else				
		{
			# Retrieve Extra Report (Test) Display Settings
			$extraReportInfo = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $SemID, 0);
			$extraReportID = $extraReportInfo["ReportID"];
			$extraReportStartDate = $extraReportInfo['TermStartDate'];
			$extraReportEndDate = $extraReportInfo['TermEndDate'];

			for($i=0; $i<$ColNum2; $i++)
			{
				$span_size = $i==0? "colspan='1'" : "colspan='2'";
				
				$targetKeys = $InfoKey." (Exam)";
				$isTestColumn = $isExtraReport || ($isMainReport && $i==1);
				if($isTestColumn)
				{
					$targetKeys = $InfoKey." (Test)";
				}
				if($InfoKey == "Demerits" || $InfoKey == "Conduct")
				{
					$targetKeys = $InfoKey;
				}
				
				$thisValue = $ValueArr[$SemID][$targetKeys];
				$thisValue = $StudentID ? ($thisValue && $i > 0 ? $thisValue : $this->EmptySymbol) : ($i == 0 ? $this->EmptySymbol : "#");
				
				# Display Days Absent / Times Late Records
                // if(($InfoKey=="Days Absent" || $InfoKey=="Times Late") && $StudentID && $i > 0 && $thisValue==$this->EmptySymbol)
                if (($InfoKey == "Days Absent" || $InfoKey == "Times Late" || $InfoKey == "Early Leave") && $StudentID && $i > 0 && $thisValue == $this->EmptySymbol)
				{
					// Semester Report - get attendance records within defined period
					if($isMainReport && $i == 1) {
						$attendanceData = $this->Get_Student_Profile_Attendance_Data($SemID, "", $extraReportStartDate, $extraReportEndDate, "", array($StudentID));
                    } else {
						$attendanceData = $this->Get_Student_Profile_Attendance_Data($SemID, "", $ReportStartDate, $ReportEndDate, "", array($StudentID));
                    }
					$attendanceData = $attendanceData[$StudentID];

					// if($InfoKey=="Days Absent")
                    if($InfoKey == "Days Absent" || $InfoKey == "Early Leave") {
						$thisValue = $attendanceData[$InfoKey] ? $attendanceData[$InfoKey] : 0;
                    } else if($InfoKey == "Times Late") {
						$thisValue = $attendanceData["Time Late"] ? $attendanceData["Time Late"] : 0;
                    }
				}
				
				# Display Demerit Records
				if($targetKeys == "Demerits")
				{
					// Semester Report - Test Column get eDiscipline records within defined period
					if($isMainReport && $i==1){
						list($dataMP, $dataMU, $dataMH, $dataMerits) = $this->loadingMeritFromDiscipline($StudentID, $SemID, $extraReportStartDate, $extraReportEndDate);
					}
					else{
						list($dataMP, $dataMU, $dataMH, $dataMerits) = $this->loadingMeritFromDiscipline($StudentID, $SemID, $ReportStartDate, $ReportEndDate);
					}
					
					$thisValue = $ValueArr[$SemID][$InfoKey]? $ValueArr[$SemID][$InfoKey] : $dataMerits;
					
					// Term Test Report / Semester Report (Test Column) - only display merit from eDiscipline
					if($isExtraReport || $i==1)
					{
						$thisValue = $dataMerits? $dataMerits : 0;
					}
					
//					$thisValue = $StudentID ? ($thisValue && $last_col ? $this->GET_CHINESE_MERIT_NUM($thisValue) : $this->EmptySymbol) : "#";
					$thisValue = $StudentID ? ($thisValue!="" && $i!=0 ? $this->Get_MERIT_NUM_DISPLAY($thisValue) : $this->Get_MERIT_NUM_DISPLAY(0)) : "#";
				}
				
				# Display Empty Records if not Term Column of Semester Report
				//if(($targetKeys == "Demerits" || $targetKeys == "Conduct") && (($isMainReport && !$last_col) || $isExtraReport))
				if(($targetKeys == "Demerits") && $i==0)
				{
					$thisValue = $this->EmptySymbol;
				}
				if(($targetKeys == "Conduct") && ($isMainReport && $i!=($ColNum2 - 1)))
				{
					$thisValue = $this->EmptySymbol;
				}
				
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'  {$span_size}>".$thisValue."</td>";
			}
			
			if($ShowRightestColumn){
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'  {$span_size}>".$this->EmptySymbol."</td>";
//				
//				# get value of this term
//				if($InfoKey == "Conduct" && $isExtraReport)
//				{
//					$thisValue = $StudentID ? $this->EmptySymbol : "#";
//				} 
//				else if($InfoKey == "Demerits")
//				{
//					if($isMainReport)
//					{
//						$thisValue = $ValueArr[0][$InfoKey];
//						$thisValue = $StudentID ? ($thisValue? $this->GET_CHINESE_MERIT_NUM($thisValue) : $this->EmptySymbol) : "#";	
//					}
//					else
//					{
//						$thisValue = $this->EmptySymbol;	
//					}
//				}
//				else 
//				{
//					$thisValue = $StudentID ? ($ValueArr[$SemID][$InfoKey." (Term Test)"] + $ValueArr[$SemID][$InfoKey." (Term)"] ? $ValueArr[$SemID][$InfoKey." (Term Test)"] + $ValueArr[$SemID][$InfoKey." (Term)"] : $this->EmptySymbol) : "#";
//				}
//					
//				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' {$span_size}>". $thisValue ."</td>";
			}
		}

		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0, $OtherArr=array(), $otherOverall="")
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		# Display Demerit Records
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$isMainReport 				= $ReportSetting['isMainReport'];
		$isExtraReport 				= !$isMainReport;
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ColumnTitle 				= $this->returnReportColoumnTitle($ReportID);
		$CalOrder 					= $this->Get_Calculation_Setting($ReportID);
		$ShowRightestColumn 		= $this->Is_Show_Rightest_Column($ReportID);
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		if($isMainReport && $ReportType!="W")
		{
			# Retrieve Extra Report (Test) Display Settings
			$extraReportInfo = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $SemID, 0);
			$extraReportID = $extraReportInfo["ReportID"];
			
			$extraColoumnTitle = $this->returnReportColoumnTitle($extraReportID);
			if(sizeof($extraColoumnTitle) > 0){
				foreach ($extraColoumnTitle as $extraColumnID[] => $extraColumnTitle[]){
					break;
				}
			}
			
			$extraColumn = $extraColumnID[0];
		}
		
		if($ReportType == "W" && $FormNumber == 6)
		{
		    global $DisplayColumnNum;

			$ColumnTitle = array();
			
			$termReport = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
			
			# Retrieve Extra Report (Test) Display Settings
			$extraReportInfo = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $termReport[0]["Semester"], 0);
			$extraReportID = $extraReportInfo["ReportID"];
			
			$extraColoumnTitle = $this->returnReportColoumnTitle($extraReportID);
			$termReportTitle = $this->returnReportColoumnTitle($termReport[0]["ReportID"]);

            $isHideExamColMode = ($DisplayColumnNum == 2);
            if($isHideExamColMode) {
                // Hide exam column
                $ColumnTitle = $extraColoumnTitle;
            }
            else {
			    $ColumnTitle = $extraColoumnTitle + $termReportTitle;
            }
		}
		
		$border_top = $isFirst ? "border_top" : "";
		
		$x .= "<tr>";
		$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, $isFirst);
		
		$curColumn = 0;
		foreach ($ColumnTitle as $ColumnID => $ColumnName) 
		{
			$current_count = $NumOfAssessment[$curColumn] - 1;
			
			if($curColumn == 0)
			{
				$current_count = $NumOfAssessment[$curColumn] ? $NumOfAssessment[$curColumn] : 1;
			}

			for ($i=0; $i<$current_count; $i++)
			{
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			}
			
			# Extra Report (Test) Value Display
			if($isMainReport && $extraColumn && $curColumn == 0)
			{
				$thisValue = $ValueArr[$extraColumn];
				if($TitleEn == $eReportCard['Template']['MSTable']['ClassPositionEn'] && count($OtherArr) > 0)
				{
					$thisInvalid = !($thisValue > 0 && $OtherArr[$extraColumn] > 0) || ($OverallPositionRangeClass > 0 && $thisValue > $OverallPositionRangeClass);
					$thisValue = $thisInvalid? $this->EmptySymbol : $this->Get_Ranking_Display($thisValue, $OtherArr[$extraColumn], 1, $ShowNumOfStudentClass);
				}
				else if($TitleEn == $eReportCard['Template']['MSTable']['FormPositionEn'] && count($OtherArr) > 0)
				{
					$thisInvalid = !($thisValue > 0 && $OtherArr[$extraColumn] > 0) || ($OverallPositionRangeForm > 0 && $thisValue > $OverallPositionRangeForm);
					$thisValue = $thisInvalid? $this->EmptySymbol : $this->Get_Ranking_Display($thisValue, $OtherArr[$extraColumn], 1, $ShowNumOfStudentForm);
				}
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' colspan='2'>".$thisValue."</td>";
			}

			$thisValue = $ValueArr[$ColumnID];
			if($TitleEn == $eReportCard['Template']['MSTable']['ClassPositionEn'] && count($OtherArr) > 0)
			{
				$thisInvalid = !($thisValue > 0 && $OtherArr[$ColumnID] > 0) || ($OverallPositionRangeClass > 0 && $thisValue > $OverallPositionRangeClass);
				$thisValue = $thisInvalid? $this->EmptySymbol : $this->Get_Ranking_Display($thisValue, $OtherArr[$ColumnID], 1, $ShowNumOfStudentClass);
			}
			else if($TitleEn == $eReportCard['Template']['MSTable']['FormPositionEn'] && count($OtherArr) > 0)
			{
				$thisInvalid = !($thisValue > 0 && $OtherArr[$ColumnID] > 0) || ($OverallPositionRangeForm > 0 && $thisValue > $OverallPositionRangeForm);
				$thisValue = $thisInvalid? $this->EmptySymbol : $this->Get_Ranking_Display($thisValue, $OtherArr[$ColumnID], 1, $ShowNumOfStudentForm);
			}
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' colspan='2'>".$thisValue."</td>";
			
			$curColumn++;
			
			// only display first column if is term test report
			if($ReportType=="T" && $isExtraReport) break;
		}
		
		if ($ShowRightestColumn)
		{
			$thisValue = $OverallValue;
			if($TitleEn == $eReportCard['Template']['MSTable']['ClassPositionEn'] && $otherOverall)
			{
			    // $thisInvalid = !($thisValue > 0 && $otherOverall > 0) || ($OverallPositionRangeClass > 0 && $thisValue > $OverallPositionRangeClass);
				$thisInvalid = !($thisValue > 0 && $otherOverall > 0) || ($OverallPositionRangeClass > 0 && $thisValue > $OverallPositionRangeClass) || ($thisValue == $this->EmptySymbol);
				$thisValue = $thisInvalid ? $this->EmptySymbol : $this->Get_Ranking_Display($thisValue, $otherOverall, 1, $ShowNumOfStudentClass);
			}
			else if($TitleEn == $eReportCard['Template']['MSTable']['FormPositionEn'] && $otherOverall)
			{
			    // $thisInvalid = !($thisValue > 0 && $otherOverall > 0) || ($OverallPositionRangeForm > 0 && $thisValue > $OverallPositionRangeForm);
				$thisInvalid = !($thisValue > 0 && $otherOverall > 0) || ($OverallPositionRangeForm > 0 && $thisValue > $OverallPositionRangeForm) || ($thisValue == $this->EmptySymbol);
				$thisValue = $thisInvalid ? $this->EmptySymbol : $this->Get_Ranking_Display($thisValue, $otherOverall, 1, $ShowNumOfStudentForm);
			}
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' colspan='2'>". $thisValue ."&nbsp;</td>";
		}
		
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0)
	{
		$x = "";
		$border_top = ($hasBorderTop)? "border_top" : "";
				
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>$Prefix</td><td height='{$LineHeight}'class='tabletext'>". $TitleCh ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>$Prefix</td><td height='{$LineHeight}'class='tabletext'>". $TitleEn ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		
		return $x;
	}
	
	function Is_Show_Rightest_Column($ReportID)
	{
		# Display Demerit Records
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport 				= $ReportSetting['isMainReport'];
		$SemID 						= $ReportSetting['Semester'];
		$term_num 					= $SemID == "F" ? "W" : $this->Get_Semester_Seq_Number($SemID);
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
//		$ShowRightestColumn = $isMainReport && $term_num!=1 && $ShowSubjectOverall;
		$ShowRightestColumn = $isMainReport && $term_num=="W" && $ShowSubjectOverall;
		
		return $ShowRightestColumn;
	}
	
	function Get_Calculation_Setting($ReportID)
	{
		# Display Demerit Records
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		return $CalOrder;
	}
	
	// $border, e.g. "left,right,top"
	function Get_Double_Line_Td($border="",$width=1)
	{
		global $image_path,$LAYOUT_SKIN;
		
		$borderArr = explode(",",$border);
		$left = $right = $top = $bottom = 0;
		
		if(in_array("left",$borderArr)) $left = $width;
		if(in_array("right",$borderArr)) $right = $width;
		if(in_array("top",$borderArr)) $top = $width;
		if(in_array("bottom",$borderArr)) $bottom = $width;

		$style = " style='border-style:solid; border-color:#000; border-width:{$top}px {$right}px {$bottom}px {$left}px ' ";
		
		$td = "<td width='0%' height='0%' $style><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='$width' height='$width'/></td>";
		
		return $td;
	}		
	
	function Get_Ranking_Display($Rank, $NumOfStudent, $DisplayRank, $DisplayNumOfStudent)
	{
		$Display = '';
		$DisplayRank = ($Rank != $this->EmptySymbol || $DisplayRank == true);
		
		if ($DisplayRank==true && $DisplayNumOfStudent==true)
		{
			$Display = $Rank.' / '.$NumOfStudent;
		}
		else if ($DisplayRank==true && $DisplayNumOfStudent==false)
		{
			$Display = $Rank;
		}
		else if ($DisplayRank==false && $DisplayNumOfStudent==true)
		{
			$Display = $this->EmptySymbol.' / '.$NumOfStudent;
		}
		else if ($DisplayRank==false && $DisplayNumOfStudent==false)
		{
			$Display = $this->EmptySymbol;
		}
		
		return $Display;
	}
	
	function Get_Subject_Col_Num($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		// Retrieve Display Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport 				= $ReportSetting['isMainReport'];
		$isExtraReport 				= !$isMainReport;
		$SemID 						= $ReportSetting['Semester'];
		$ReportType 				= $SemID == "F" ? "W" : "T";
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		
		$n = 0;
		
		# Term Report Type
		if($ReportType=="T"){
			// Term Test Report
			if($isExtraReport){
				$n++;
			}
			// Term Report
			else {
//				if($FormNumber != 6){
					$n++;
//				}
				$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
				for($i=0;$i<sizeof($ColoumnTitle);$i++){
					$n++;
				}
			}
		}
		# Whole Year Report Type
		else{
			if($FormNumber == 6){
				$termReport = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
				$ColoumnTitle = $this->returnReportTemplateColumnData($termReport[0]["ReportID"]);
				for($i=0;$i<sizeof($ColoumnTitle);$i++){
					$n++;
				}
				$n++;
			}
			else{
				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
				for($i=0;$i<sizeof($ColumnData);$i++){
					$n++;
				}
			}
			$n++;
		}
		
		return ($n*2 + 1);
	}
	
//	function Get_Subject_Col_Width($ReportID)
//	{
//		global $eReportCard, $eRCTemplateSetting;
//		
//		// Retrieve Display Settings
//		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
//		$isMainReport 				= $ReportSetting['isMainReport'];
//		$isExtraReport 				= !$isMainReport;
//		$SemID 						= $ReportSetting['Semester'];
//		$ReportType 				= $SemID == "F" ? "W" : "T";
//		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
//		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
//		
//		$n = 0;
//		$colwidth = 8;
//		$fullmark_width = 8;
//		$semester_width = 0;
//		
//		# Term Report Type
//		if($ReportType=="T"){
//			// Term Test Report
//			if($isExtraReport){
//				$colwidth = 12;
//				$fullmark_width = 24;
//				$n++;
//			}
//			// Term Report
//			else {
//				if($FormNumber != 6){
//					$n++;
//				}
//				$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
//				for($i=0;$i<sizeof($ColoumnTitle);$i++){
//					$n++;
//				}
//			}
//			
//			$semester_width = $colwidth * 2 * $n;
////			if($isMainReport && $this->Get_Semester_Seq_Number($SemID) == 2){
////				$semester_width += $colwidth * 2;
////			}
//		}
//		# Whole Year Report Type
//		else{
//			if($FormNumber == 6){
//				$termReport = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
//				$ColoumnTitle = $this->returnReportTemplateColumnData($termReport[0]["ReportID"]);
//				for($i=0;$i<sizeof($ColoumnTitle);$i++){
//					$n++;
//				}
//			}
//			else{
//				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
//				for($i=0;$i<sizeof($ColumnData);$i++){
//					$n++;
//				}
//			}
//			$semester_width = $colwidth * 2 * ($n + 1);
//		}
//		$subject_width = 100 - $semester_width - $fullmark_width;
//		
//		return $subject_width;
//	}
	
	function Get_MERIT_NUM_DISPLAY($num){
		global $eReportCard;
		
		$display = "";
		if($num > 1)
			$display = "$num ".$eReportCard['Template']['MSTable']['DemeritEn'];
		else
			$display = "$num Demerit";
			
		return $display;
	}
	
//	function GET_CHINESE_MERIT_NUM($num){
//		global $eReportCard;
//		
//		$chineseNumberArr = $eReportCard['Template']['MSTable']['MeritNumChi'];
//
//		if($num < 20)
//		{
//			$result = $chineseNumberArr[$num];
//		}
//		else if($num < 100)
//		{
//			$tens   = ((int) ($num / 10)) * 10;
//			$units  = $num % 10;
//			$result = $chineseNumberArr[$tens];
//			if ($units) 
//			{
//				$result .= " and ";
//				$result .= $chineseNumberArr[$units];
//			}
//		}
//		$result .= " ".$eReportCard['Template']['MSTable']['DemeritEn'];
//		
//		return $result;
//	}		
	
	function loadingMeritFromDiscipline($target_studentid, $termid, $start="", $end=""){
		
		$conds = "";
		if($termid!=0 && $termid!="") $conds .= " AND r.YearTermID='$termid'";
		if($start!="" && $end!="") $conds .= " AND r.RecordDate between '$start' AND '$end'";
		
		# Retrieve Approved and Released Demerit Records
		$sql = "SELECT 
					StudentID, TagName, COUNT(RecordID) as totalRecord 
				FROM DISCIPLINE_MERIT_RECORD r 
					INNER JOIN DISCIPLINE_AP_ITEM_TAG ait ON (ait.APItemID=r.ItemID) 
					INNER JOIN DISCIPLINE_ITEM_TAG_SETTING t ON (t.TagID=ait.TagID) 
				WHERE 
					r.AcademicYearID = '".$this->GET_ACTIVE_YEAR_ID()."' $conds AND (r.MeritType=-1 OR r.MeritType IS NULL) AND 
					r.StudentID = '$target_studentid' AND r.RecordStatus=1 AND r.ReleaseStatus=1  
				GROUP BY r.StudentID, t.TagID";
		$result = $this->returnArray($sql);
		
		$totalResult = count($result);
		for($i=0; $i<$totalResult; $i++) 
		{
			list($studentid, $tagname, $totalRecord) = $result[$i];
//			if(!(strlen($tagname)>2))
//			{
//				if((!isset($data[$studentid]['MP']) || $data[$studentid]['MP']=="") && substr($tagname,0,2)=="MP" && $totalRecord>0)
//				{
//					$data_MP = "X";
//				}
//				if((!isset($data[$studentid]['MU']) || $data[$studentid]['MU']=="") && substr($tagname,0,2)=="MU" && $totalRecord>0)
//				{
//					$data_MU = "X";
//				}
//				if((!isset($data[$studentid]['MH']) || $data[$studentid]['MH']=="") && substr($tagname,0,2)=="MH" && $totalRecord>0)
//				{
//					$data_MH = "X";
//				}
//			}
			if(isset($tagname) && strlen($tagname)>0 && $totalRecord>0)
			{
				if(substr($tagname,0,2)=="MP")
				{
					$data_MP = "0";
				}
				else if(substr($tagname,0,2)=="MU")
				{
					$data_MU = "0";
				}
				else if(substr($tagname,0,2)=="MH")
				{
					$data_MH = "0";
				}
			}
		}
	
		# Retrieve Merit Records
		$sql = "SELECT 
					r.StudentID, SUM(r.ConductScoreChange) as TotalDeducted, COUNT(r.RecordID) as TotalRecord 
				FROM 
					DISCIPLINE_MERIT_RECORD r 
				WHERE 
					r.AcademicYearID='".$this->GET_ACTIVE_YEAR_ID()."' $conds AND (r.MeritType=-1 OR r.MeritType IS NULL) 
					AND r.StudentID = '$target_studentid' AND r.RecordStatus=1 AND r.ReleaseStatus=1   
				GROUP BY r.StudentID";
		$result = $this->returnArray($sql);
		
		$totalResult = count($result);
		for($i=0; $i<$totalResult; $i++) 
		{
			list($studentid, $totalDeducted, $totalRecord) = $result[$i];
			$totalDeducted = ($totalDeducted) ? abs($totalDeducted) : "0";
			$data_Demerit = floor($totalDeducted/5);
		}

		return array($data_MP, $data_MU, $data_MH, $data_Demerit);
	}
	
	function loadECAFromiPo($studentid, $getFromEnrol=false){
		global $plugin, $eclass_db;
		
		# Retrieve ECA Records from eEnrolment
		/*
		 * 146 - GroupCategory = 3
		 * 149 - GroupCategory = 38
		 * client site - GroupCategory = 7
		 */
		if($getFromEnrol)
		{
			$sql = "SELECT
							iu.UserGroupID,
							iu.GroupID,
							iu.UserID,
							iu.RoleID,
							iu.RecordType,
							iu.RecordStatus,
							iu.Performance,
							iu.isActiveMember,
							iu.EnrolGroupID,
							ig.Title as ClubTitle,
							ir.Title as RoleTitle,
							iu.Achievement,
							iegi.GroupCategory as CategoryID
					FROM
							INTRANET_USERGROUP as iu
							Inner Join INTRANET_GROUP as ig ON (iu.GroupID = ig.GroupID)
							Inner Join INTRANET_ENROL_GROUPINFO as iegi On (iu.EnrolGroupID = iegi.EnrolGroupID)
							Left Outer Join INTRANET_ROLE as ir On (iu.RoleID = ir.RoleID)
					WHERE
							iu.UserID = '$studentid'
							AND	ig.AcademicYearID = '".$this->GET_ACTIVE_YEAR_ID()."'
							AND	ig.RecordType = 5
							AND iegi.GroupCategory = 7
					Order By 
							ClubTitle
					";
		}
		# Retrieve ECA Records from iPortfolio
		else
		{
			// get available SubCatID
			$sql = "SELECT osc.SubCatID FROM {$eclass_db}.OLE_SUBCATEGORY AS osc
						INNER JOIN {$eclass_db}.OLE_CATEGORY AS oc ON (osc.CatID = oc.RecordID)
					WHERE oc.RecordStatus IN (1, 2) ";
			$SubCatIDAry = $this->returnVector($sql);
			
			// [2017-0209-0929-17066] Show member records in bottom
			$sql = "SELECT
							op.Title AS ClubTitle,
							os.Role AS RoleTitle
					FROM
							{$eclass_db}.OLE_STUDENT AS os
							Inner Join {$eclass_db}.OLE_PROGRAM AS op ON (os.ProgramID = op.ProgramID)
					WHERE
							os.UserID = '$studentid'
							AND	op.AcademicYearID = '".$this->GET_ACTIVE_YEAR_ID()."'
							AND op.IntExt = 'INT'
							AND	op.SubCategoryID IN ('".implode("', '", $SubCatIDAry)."')
							AND os.RecordStatus IN (2,4)
					Order By 
							UPPER(RoleTitle) = 'MEMBER', ClubTitle, RoleTitle
					";
		}
		
		return $this->returnArray($sql);
	}

	# Map Subject Code and Subject ID
	function GET_SUBJECT_SUBJECTCODE_MAPPING(){
		
		$sql = "SELECT 
					CODEID,
					RecordID
				FROM
					ASSESSMENT_SUBJECT 
				WHERE
					EN_DES IS NOT NULL
					AND RecordStatus = 1
					AND (CMP_CODEID IS NULL || CMP_CODEID = '')
				ORDER BY
					DisplayOrder
				";
		$SubjectArr = $this->returnArray($sql, 2);
		
		$ReturnArr = array();
		if (sizeof($SubjectArr) > 0) {
			for($i=0; $i<sizeof($SubjectArr); $i++) {
				$ReturnArr[$SubjectArr[$i]["RecordID"]] = $SubjectArr[$i]["CODEID"];
			}
		}
		return $ReturnArr;
	}
}
?>