<?php
# Editing by 

/*
 * Change Log: 
 *  Date: 2020/07/17	Philips [2020-0717-1514-48066]
 *  		- modified getStudentECAContentDisplay(), Not to display Voluntary Hours on Primary Level
 *  Date: 2020/04/06	Philips [M-1-1-01] [CU-MO-2020-0364]
 *  		- modified ClassTeacherComment(), getStydebtECAContentDisplay()
 *  Date: 2018/09/12    Bill
 *          - revise report display
 * 	Date: 2017/03/14 	Villa
 * 			- Open the File
 */

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		global $eRCTemplateSetting;
		
		$this->libreportcard();
		$this->configFilesType = $eRCTemplateSetting['OtherInfo']['CustRemarks'];
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1; 
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;

		// [2020-0320-1604-53096]
		//$this->HighLightSubjectID = array(1, 8, 38);
        $this->HighLightSubjectID = array();

        // 146
// 		$this->MainSubjectList = array("N165", "080", "280");
		
		// Client Site
		$this->MainSubjectList = array("PL001", "PL003", "PM001", "JL001", "JL005", "JM001", "SL001", "SL003", "SM001");
		$this->MainSubjectNotShowComponent = array("JL001", "JL005", "JL007", "JL008", "SL001", "SL003", "SL004", "SL005");
		
		// $this->customizedPromotionStatus = array("Promoted"=>1, "Retained"=>2, "Dropout"=>3);
		$this->customizedPromotionStatus = array("Promoted"=>1, "Retained"=>2, "Graduated"=>4);
		
		// $this->EmptySymbol = "---";
		$this->EmptySymbol = "--";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='', $PrintTemplateType='') {
		global $eReportCard;
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
    		$TableTop .= "<tr><td height='150px' class='font_8pt'>"."&nbsp;"."</td></tr>";
    		$TableTop .= "<tr><td class='font_8pt'>".$StudentInfoTable."</td></tr>";
    		$TableTop .= "<tr><td class='font_8pt'>".$MSTable."</td></tr>";
    		$TableTop .= "<tr><td class='font_8pt'>".$SignatureTable."</td></tr>";
		$TableTop .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr valign='top'><td>".$TableTop."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID, $StudentID='', $PrintTemplateType='') {
		// do nothing
	}
	
	function getReportStudentInfo($ReportID, $StudentID='', $forPage3='', $PageNum=1, $PrintTemplateType='') {
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			
			# Retrieve required variables
			$defaultVal = ($StudentID=='')? "XXX" : '';
			
			# Retrieve Report Info
			$data['AcademicYear'] = $this->GET_ACTIVE_YEAR_NAME();
			if($ReportSetting['Issued'] != '') {
                $data['DateOfIssue'] = date('d/m/Y', strtotime($ReportSetting['Issued']));
			}
			
			# Retrieve Student Info
			if($StudentID)
			{
			    $StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			    $thisClassName = $StudentInfoArr[0]['ClassName'];
			    $thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
			    
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				$lu = new libuser($StudentID);
				
				$data['Name'] = $lu->UserName2Lang("ch", 2);
				$data['Class_ClassNo'] = $thisClassName."(".$thisClassNumber.")";
				$data['StudentNo'] = str_replace("s", "", $lu->UserLogin);
				$data['STRN'] = $lu->STRN;
				if($lu->DateOfBirth != '') {
				    $data['DateOfBirth'] = date('d/m/Y', strtotime($lu->DateOfBirth));
				}
			}
			
			$count = 0;
			$StudentInfoTable .= "<table class='student_info' width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
			for($i=0; $i<sizeof((array)$StudentTitleArray); $i++)
			{
				$SettingID = trim($StudentTitleArray[$i]);
				$TitleEn = $eReportCard['Template']['StudentInfo'][$SettingID.'En'];
				$TitleCh = $eReportCard['Template']['StudentInfo'][$SettingID.'Ch'];
				
				$DisplayData = $data[$SettingID] ? $data[$SettingID] : $defaultVal;
				
				$isNewInfoTableRow = $SettingID == "Name" || ($count % $StudentInfoTableCol == 0 && $count != '0');
				if($isNewInfoTableRow || $SettingID == "StudentNo") {
					$StudentInfoTable .= "<tr>";
				}
				
				if($SettingID == "Name" || $SettingID == "StudentNo" || $SettingID == "Class_ClassNo") {
				    $StudentInfoTable .= "<td width='70px'>".$TitleEn."</td>";
				} else {
				    $StudentInfoTable .= "<td width='62px'>".$TitleEn."</td>";
				}
			    if($isNewInfoTableRow || $SettingID == "StudentNo") {
					$StudentInfoTable .= "<td width='54px'>".$TitleCh."</td>";
					$StudentInfoTable .= "<td width='8px'>:</td>";
				} else {
				    $StudentInfoTable .= "<td width='68px'>".$TitleCh."&nbsp;:</td>";
				}
				if($SettingID == "Name") {
                    $StudentInfoTable .= "<td width='92px' colspan='9'>";
				} else if ($SettingID == "StudentNo" || $SettingID == "Class_ClassNo"){
				    $StudentInfoTable .= "<td width='92px'>";
				} else {
				    $StudentInfoTable .= "<td width='100px'>";
				}
                $StudentInfoTable .= $DisplayData;
				$StudentInfoTable .= "</td>";
				
				if($SettingID != "Name") {
					$count++;
				}
				if($SettingID == "Name" || ($count % $StudentInfoTableCol == 0 && $count != '0')) {
					$StudentInfoTable .= "</tr>";
				}
			}
			$StudentInfoTable .= "</table>";
		}
		
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='', $PrintTemplateType='', $PageNum='') {
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$SchoolType	= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		$isPrimaryLevel = $SchoolType == "P";
		//$LineHeight = $ReportSetting['LineHeight'];
		//$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		//$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		//$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		//$ShowSubjectComponent = $ReportSetting['ShowSubjectComponent'];
		
		$TotalNumOfDataArr = 0;
		$Total2NumOfDataArr = 0;
		
		# Table Header
		$MSTableColHeader = $this->genMSTableColHeader($ReportID);
		
		# Subject Result
		$SubjectDisplayArr = $this->returnTemplateSubjectCol($ReportID, $ClassLevelID, $StudentID);
		$NumOfSubjectArr = sizeof((array)$SubjectDisplayArr['DisplayRow']) - sizeof((array)$SubjectDisplayArr['HideRow']);
		$TotalNumOfDataArr += $NumOfSubjectArr;
		
		# Overall Result
		$MSTableFooterArr = $this->genMSTableFooter($ReportID, $StudentID, $SubjectDisplayArr['FailUnitArr'], $TotalNumOfDataArr);
		$NumOfMSTableFooterArr = sizeof((array)$MSTableFooterArr);
		$TotalNumOfDataArr += $NumOfMSTableFooterArr;
		
		# Merit
		$MeritsAndDemeritsArr = $this->getMeritsAndDemerits($ReportID, $StudentID, $TotalNumOfDataArr);
		$NumOfMeritsAndDemeritsArr = sizeof((array)$MeritsAndDemeritsArr);
		$TotalNumOfDataArr += $NumOfMeritsAndDemeritsArr;
		
		# Teacher Comment
		$ClassTeacherCommentArr = $this->ClassTeacherComment($ReportID, $StudentID);
		$NumOfClassTeacherCommentArr = sizeof((array)$ClassTeacherCommentArr);
		$Total2NumOfDataArr += $NumOfClassTeacherCommentArr;

		# ECA Content
		$ECAArr = $this->getECATable($ReportID, $StudentID, ($isPrimaryLevel? $Total2NumOfDataArr : $TotalNumOfDataArr), $Total2NumOfDataArr);
		$NumOfECAArr = sizeof((array)$ECAArr);
		if($isPrimaryLevel) {
		    $Total2NumOfDataArr += $NumOfECAArr;
		} else {
		    $TotalNumOfDataArr += $NumOfECAArr;
		}

		$MSTable = "<table width='100%' height='100%' class='border_table'>";
			$MSTable .= "<tr class='header_row'>";
				$MSTable .= $MSTableColHeader;
			$MSTable .= "</tr>";
			
			/*
			foreach((array)$SubjectDisplayArr['DisplayRow'] as $row => $_SubjectArr) {
				$MSTable .= "<tr>";
					$MSTable .= $_SubjectArr;
					$MSTable .= $ClassTeacherCommentArr[$row];
				$MSTable .= "</tr>";
			}
			
			for($i=0; $i<$NumOfMSTableFooterArr; $i++) {
				$MSTable .= "<tr>";
    				$MSTable .= $MSTableFooterArr[$i];
    				$MSTable .= $ClassTeacherCommentArr[($row + $i + 1)];
					if(($NumOfClassTeacherCommentArr - $NumOfSubjectArr - $i) == 0) {
					    $MSTable .= $ECAArr[$ECAPrintCount];
					    $ECAPrintCount++;
					}
				$MSTable .= "</tr>";
			}
			
			for($i=0; $i<$NumOfMeritsAndDemeritsArr; $i++) {
				$MSTable .= "<tr>";
					$MSTable .= $MeritsAndDemeritsArr[$i];
					if(($NumOfClassTeacherCommentArr - $NumOfSubjectArr - $NumOfMSTableFooterArr - $i) <= 0) {
					    $MSTable .= $ECAArr[$ECAPrintCount];
					    $ECAPrintCount++;
					}
				$MSTable .= "</tr>";
			}
			*/
			$td_border_style = "border_top";
			
			$TotalNumOfDataArr = $TotalNumOfDataArr >= $Total2NumOfDataArr? $TotalNumOfDataArr : $Total2NumOfDataArr;
			for($i=0; $i<$TotalNumOfDataArr; $i++) {
			    $MSTable .= "<tr>";
    			    if($isPrimaryLevel)
    			    {
    			        # Left Table
    			        if(isset($SubjectDisplayArr['DisplayRow'][$i])) {
    			            $MSTable .= $SubjectDisplayArr['DisplayRow'][$i];
    			        }
    			        else if(isset($MSTableFooterArr[$i])) {
    			            $MSTable .= $MSTableFooterArr[$i];
    			        }
    			        else if(isset($MeritsAndDemeritsArr[$i])) {
    			            $MSTable .= $MeritsAndDemeritsArr[$i];
    			        }
    			        else {
    			            $MSTable .= '<td colspan="7" class="border_right_double '.$td_border_style.'">&nbsp;</td>';
    			            $td_border_style = '';
    			        }
    			        
    			        # Right Table
    			        if(isset($ClassTeacherCommentArr[$i])) {
    			            $MSTable .= $ClassTeacherCommentArr[$i];
    			        }
    			        else if(isset($ECAArr[$i])) {
    			            $MSTable .= $ECAArr[$i];
    			        }
    			        else {
    			            $MSTable .= '<td>&nbsp;</td>';
    			        }
    			    }
    			    else
    			    {
    			        # Left Table
    			        if(isset($SubjectDisplayArr['DisplayRow'][$i])) {
    			            $MSTable .= $SubjectDisplayArr['DisplayRow'][$i];
    			        }
    			        else if(isset($MSTableFooterArr[$i])) {
    			            $MSTable .= $MSTableFooterArr[$i];
    			        }
    			        else if(isset($MeritsAndDemeritsArr[$i])) {
    			            $MSTable .= $MeritsAndDemeritsArr[$i];
    			        }
    			        else if(isset($ECAArr[$i])) {
    			            $MSTable .= $ECAArr[$i];
    			        }
    			        else {
    			            $MSTable .= '<td colspan="6" class="border_right_double">&nbsp;</td>';
    			        }
    			        
    			        # Right Table
    			        if(isset($ClassTeacherCommentArr[$i])) {
    			            $MSTable .= $ClassTeacherCommentArr[$i];
    			        }
    			        else {
    			            // [2019-0627-1713-22096] remove eca col padding if too many inputs > prevent cannot display all content in same page
    			            if(isset($ECAArr[$i]) && strpos($ECAArr[$i], 'td_padding_removed') !== false) {
    			                $MSTable .= '<td style=\'padding: 0px 3px;\'>&nbsp;</td>';
    			            }
			                else {
                                $MSTable .= '<td>&nbsp;</td>';
			                }
    			        }
    			    }
			    $MSTable .= "</tr>";
			}
			
		$MSTable .= "</table>";
		
		return $MSTable;
	}
	
	function genMSTableColHeader($ReportID) {
	    global $eReportCard;
	    
	    # Retrieve Display Settings
	    $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
	    $ClassLevelID = $ReportSetting['ClassLevelID'];
	    $SchoolType	= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
	    $isPrimaryLevel = $SchoolType == "P";
		
		# Term Reports
		$AllTermAllSem = $this->getAllTermReport($ReportID);
		$NumOfTerm = sizeof((array)$AllTermAllSem);
		
		# Subject
		$x = '<td class="border_right" align="center" colspan="2">'.$eReportCard['Template']['SubjectEn'].'</br>'.$this->handleChineseContentDisplay($eReportCard['Template']['SubjectCh'], '&nbsp;&nbsp;').'</td>';
		
		if($isPrimaryLevel)
		{
		    # Unit
		    $x .= '<td class="border_right" align="center" width="40px">'.$eReportCard['Template']['UnitEn'].'</br>'.$eReportCard['Template']['UnitCh'].'</td>';
		    # Term
		    for($i=0; $i<2; $i++) {
		        $x .= '<td class="border_right" align="center" width="52px">'.$eReportCard['Template']['TermEn'][$i].'</br>'.$eReportCard['Template']['TermCh'][$i].'</td>';
		    }
		    # Final Average
		    $x .= '<td class="border_right_double" align="center" colspan="2">'.$eReportCard['Template']['FinalAverageEn'].'</br>'.$eReportCard['Template']['FinalAverageCh'].'</td>';
		}
		else
		{
    		# Unit
    		$x .= '<td class="border_right" align="center" width="40px">'.$eReportCard['Template']['UnitEn'].'</br>'.$eReportCard['Template']['UnitCh'].'</td>';
    		# Term
    		for($i=0; $i<2; $i++) {
    			$x .= '<td class="border_right" align="center" width="70px">'.$eReportCard['Template']['TermEn'][$i].'</br>'.$eReportCard['Template']['TermCh'][$i].'</td>';						
    		}
    		# Final Average
    		$x .= '<td class="border_right_double" align="center" width="70px">'.$eReportCard['Template']['FinalAverageEn'].'</br>'.$eReportCard['Template']['FinalAverageCh'].'</td>';
		}
		
		# Comment
		$x .= '<td align="center" width="200px">'.$eReportCard['Template']['CommentEn'].'</br>'.$eReportCard['Template']['CommentCh'].'</td>';
		
		return $x;
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID, $StudentID) {
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ReportSemID = $ReportSetting['Semester'];
		$ReportSemNum = $this->Get_Semester_Seq_Number($ReportSemID);
		$isYearReport = $ReportSemID == "F";
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$SchoolType	= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		$isPrimaryLevel = $SchoolType == "P";
		$LineHeight = $ReportSetting['LineHeight'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$ShowSubjectComponent = $ReportSetting['ShowSubjectComponent'];
		
		# Term Reports
		$AllTermReport = $this->getAllTermReport($ReportID);
		$NumOfTerm = sizeof((array)$AllTermReport);
		
		# Main Subjects
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		
		# Subject Code Mapping
		$SubjectCodeMapping = $this->GET_SUBJECTS_CODEID_MAP(0, 0);
		
		### FooterArr => Total Unit(s) Failed / Average / Number of Students in Class / Position in Class / Periods Absent / Times Tardy
		# Term Results
		for($i=0; $i<2; $i++)
		{
			$thisTermReportID = $AllTermReport[$i]['ReportID'];
			if($isYearReport) { 	            // Final Report
				//$CurrentTermOrder = 3;
			    $CurrentTermOrder = 2;
			} else if($thisTermReportID == $ReportID) {
				$CurrentTermOrder = $i;
			}
			
			// Student Scores
			if($StudentID)
			{
				$MarksAry = $this->getMarks($thisTermReportID, $StudentID, '', 0, 1);
				$MSTableReturned[$i] = $this->genMSTableMarks($thisTermReportID, $MarksAry, $StudentID);
			}
			
			// Subject Weighting
			$SubjectWeighting[$i] = $this->getMSTableUnit($thisTermReportID, $ClassLevelID);
			
			// Init Fail Subject Units
			$UnitFail[$i] = 0;
			
			// Init Total Subject Units
			$UnitTotal[$i] = 0;
			
			if(!$isYearReport && $ReportSemNum == ($i + 1)) {
			    break;
			}
		}
		
		# Final Results
		if($isYearReport)
		{
			if($StudentID) {
				$MarksAry = $this->getMarks($ReportID, $StudentID, '', 0, 1);
				$MSTableReturned['Final'] = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID);
			}
			$SubjectWeighting['Final'] = $this->getMSTableUnit($ReportID, $ClassLevelID);
			$UnitFail['Final'] = 0;
			$UnitTotal['Final'] = 0;
		}
		
		# Check if subject all N.A.
		$isAllNAAry = array();
		foreach((array)$MSTableReturned as $thisMSTableContent) {
		    foreach((array)$thisMSTableContent as $thisSubjectID => $thisSubjectMSTableContent) {
		        if(!isset($isAllNAAry[$thisSubjectID])) {
		            $isAllNAAry[$thisSubjectID] = true;
		        }
                if(!$thisSubjectMSTableContent['isAllNA']) {
                    $isAllNAAry[$thisSubjectID] = false;
                }
		    }
		}
		
		# Subject Results + Process
		$t = array();
		$subjectCount = 0;
		foreach ((array)$MainSubjectArray as $MainSubjectID => $_MainSubject)
		{
			$SubjectEn = $this->GET_SUBJECT_NAME_LANG($MainSubjectID, "EN");
			$SubjectCh = $this->GET_SUBJECT_NAME_LANG($MainSubjectID, "CH");
			
			$isNotShowComponent = false;
			$numOfCMP = sizeof((array)$_MainSubject) - 1;
			if($numOfCMP > 0)
			{
    			$SubjectCode = $SubjectCodeMapping[$MainSubjectID];
    			$isNotShowComponent = in_array($SubjectCode, (array)$this->MainSubjectNotShowComponent);
    			if($isNotShowComponent) {
    			    $numOfCMP = 0;
    			}
			}
			
			//20181023 Ivan: request hide wordings in reportcard in con call on 20181023 with Tommy
			$SubjectEn = trim(str_replace(array('Junior', 'Senior', 'Primary'), '', $SubjectEn));
			$SubjectCh = trim(str_replace(array('初中', '高中', '小學'), '', $SubjectCh));
			
			// Handle Component display
			$validSubCMP = 1;
			if($numOfCMP)
			{
			    $validSubCMP = 0;
			    $lastSubjID = '';
			    $thisCMPCount = $subjectCount;
			    foreach ((array)$_MainSubject as $SubID => $Sub) {
			        // SKIP > Parent Subject
			        if(!$SubID) {
			            continue;
			        }
			        // SKIP > all N.A. result
			        if(isset($isAllNAAry[$SubID]) && $isAllNAAry[$SubID]) {
			            continue;
			        }
			        $validSubCMP++;
			        
			        if($thisCMPCount < 14) {
			            $thisCMPCount++;
// 			            if($thisCMPCount == 15) {
//     			            $lastSubjID = $SubID;
//     			            break;
//                         }
			        } else {
			            break;
			        }
			    }
			}
// 			else {
// 			    if(isset($isAllNAAry[$MainSubjectID]) && $isAllNAAry[$MainSubjectID]) {
// 			        continue;
// 			    } else {
//                     $allCount++;
// 			    }
// 			}
			
			$isFirst = true;
			$isFirstCMP = true;              // Decide bold line
			$isFirstForFinalAverage = true;
			foreach ((array)$_MainSubject as $SubID => $Sub)
			{
			    $isDisplaySubjectRow = true;
			    $isCountFailSubjectUnit = true;
			    $targetSubjectID = $SubID? $SubID : $MainSubjectID;
			    
			    // Skip Components for specific Main Subjects
			    if($isNotShowComponent && $SubID) {
			        continue;
			    }
			    
			    // NOT DISPLAY > Parent Subject
			    if($numOfCMP && !$SubID) {
			        $isDisplaySubjectRow = false;
			        $isCountFailSubjectUnit = false;
			    }
			    
			    // NOT DISPLAY > all N.A. result
			    if(isset($isAllNAAry[$targetSubjectID]) && $isAllNAAry[$targetSubjectID]) {
			        $isDisplaySubjectRow = false;
			        $isCountFailSubjectUnit = false;
			    }
			    
			    // NOT DISPLAY > more than 15 subjects [removed - 2018-1105-1140-12066]
// 			    if($subjectCount > 14) {
// 			        $isDisplaySubjectRow = false;
// 			    }
			    
			    // Display Subject Row
			    $x = "";
			    if(!$isDisplaySubjectRow)
			    {
			        // do nothing
			    }
			    else
			    {
    			    if(($isFirstCMP && in_array($MainSubjectID, $this->HighLightSubjectID)) || (in_array($previous_SubjectID, $this->HighLightSubjectID) && $MainSubjectID != $previous_SubjectID)) {
    					$border_top_css_bold = "border_top_bold";
    					$isFirstCMP = false;
    				} else {
    					$border_top_css_bold = "border_top";
    				}
    				
    				$subjectStyle = '';
    				if($isPrimaryLevel) {
    				    $subjectStyle = " style='padding: 3px' ";
    				}
    				
    				### Subject Name
    				if($numOfCMP) {
    					if($isFirst) {
    					    $height = strlen($SubjectEn) * 6.75;
    						$x .= "<td rowspan='".$validSubCMP."' class='$border_top_css_bold border_right rotate' height='$height' style='padding-left: 10px; padding-right: 10px; line-height: 13px;'>";
    							$x .= "<div align='center' valign='top'>";
    							    $x .= $SubjectEn."<br/>".$SubjectCh;
    							$x .= "</div>";
    						$x .= "</td>";
    						
    						if(in_array($MainSubjectID, $this->HighLightSubjectID)){
    							$isFirstCMP = true;
    						}
    					}
    					
    					if($SubID) {
    						$SubjectEn = $this->GET_SUBJECT_NAME_LANG($SubID, "EN");
    						$SubjectCh = $this->GET_SUBJECT_NAME_LANG($SubID, "CH");
    						
    						$x .= "<td class='$border_top_css_bold border_right' $subjectStyle>";
    							$x .= "<table>";
    								$x .= "<td width='131px'>".$SubjectEn."</td>";
    								$x .= "<td>";
    									$x .= $this->handleChineseContentDisplay($SubjectCh);
    								$x .= "</td>";
    							$x .= "</table>";
    						$x .= "</td>";
    					}
    				}
    				else {
    					$x .= "<td colspan='2' class='$border_top_css_bold border_right' $subjectStyle>";
    						$x .= "<table>";
    							$x .= "<td width='170px'>".$SubjectEn."</td>";
    							$x .= "<td>";
                                    $x .= $this->handleChineseContentDisplay($SubjectCh);
    							$x .= "</td>";
    						$x .= "</table>";
    					$x .= "</td>";
    				}
    				
    				### Unit
//     				$targetSubjectID = $SubID? $SubID : $MainSubjectID;
    				$x .= "<td class='$border_top_css_bold border_right' align='center'>";
    				    if($isYearReport) {
    				        $x .= $SubjectWeighting['Final'][$targetSubjectID];
    				    } else {
    				        $x .= $SubjectWeighting[$CurrentTermOrder][$targetSubjectID];
    				    }
				    $x .= "</td>";
    				    
				    ### Term
				    for($i=0; $i<2; $i++) {
				        $x .= "<td class='$border_top_css_bold border_right' align='center'>";
    				        if($StudentID == "") {
    				            $x .= "S";
    				        } else if($i <= $CurrentTermOrder) {
    				            $x .= $MSTableReturned[$i][$targetSubjectID]['Display'];
    				        } else {
    				            $x .= '&nbsp;';
    				        }
				        $x .= "</td>";
				    }
				    
				    ### Final
				    if($isPrimaryLevel && $numOfCMP > 0)
				    {
				        $x .= "<td class='$border_top_css_bold border_right' align='center' width='47px'>";
				        if($StudentID == "") {
				            $x .= "S";
				        } else if($isYearReport) {
			                $x .= $MSTableReturned['Final'][$targetSubjectID]['Display'];
				        } else {
				            $x .= "&nbsp;";
				        }
				        $x .= "</td>";
				    }
				    if($numOfCMP == 0 || ($numOfCMP > 0 && $isFirstForFinalAverage))
				    {
				        $colspan = ($isPrimaryLevel && $numOfCMP == 0)? 2 : 1;
				        $colwidth = ($isPrimaryLevel && ($numOfCMP > 0 && $isFirstForFinalAverage))? " width='52px' " : "";
// 				        $border_top_css = ($numOfCMP > 0 && $isFirstForFinalAverage)? $border_top_css_bold : "border_top";
				        
			            $x .= "<td rowspan='".$validSubCMP."' class='$border_top_css_bold border_right_double' align='center' colspan='$colspan' $colwidth>";
			            if($StudentID == "") {
			                $x .= "S";
			            } else if($isYearReport) {
			                if($numOfCMP > 0 && $isFirstForFinalAverage) {
			                    $x .= $MSTableReturned['Final'][$MainSubjectID]['Display'];
			                } else {
                                $x .= $MSTableReturned['Final'][$targetSubjectID]['Display'];
			                }
			            } else {
			                $x .= "&nbsp;";
			            }
			            $x .= "</td>";
				        
			            if($numOfCMP > 0 && $isFirstForFinalAverage) {
				            $isFirstForFinalAverage = false;
				        }
				    }
				    $t['DisplayRow'][] = $x;
				    
				    $previous_SubjectID = $MainSubjectID;
				    $isFirst = false;
				    $subjectCount++;
			    }
				
				### Fail Unit Calculation
				if($StudentID && $isCountFailSubjectUnit)
				{
    				### Term
    				for($i=0; $i<2; $i++)
    				{
    				    if($MSTableReturned[$i][$targetSubjectID]['Pass'] == 'Fail') {
    				        $UnitFail[$i] += $SubjectWeighting[$i][$targetSubjectID];
    				    }
    				    if($MSTableReturned[$i][$targetSubjectID]['Pass'] != '' && $MSTableReturned[$i][$targetSubjectID]['Pass'] != 'SC') {
                            $UnitTotal[$i] += $SubjectWeighting[$i][$targetSubjectID];
    				    }
    				}
    				
    				### Final
    				if($isYearReport)
    				{
    				    if($MSTableReturned['Final'][$targetSubjectID]['Pass'] == 'Fail') {
        				    $UnitFail['Final'] += $SubjectWeighting['Final'][$targetSubjectID];
    				    }
    				    if($MSTableReturned['Final'][$targetSubjectID]['Pass'] != '' && $MSTableReturned['Final'][$targetSubjectID]['Pass'] != 'SC') {
    				        $UnitTotal['Final'] += $SubjectWeighting['Final'][$targetSubjectID];
    				    }
    				}
				}
			}
		}
		$t['FailUnitArr'] = $UnitFail;
		$t['TotalUnitArr'] = $UnitTotal;
		
		return $t;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array()){
		global $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 						= $ReportSetting['Semester'];
		$ReportType 				= $SemID == "F" ? "W" : "T";
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark 		= $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];

		foreach ((array)$MarksAry as $SubjectID => $_MarksAry)
		{
			# Retrieve Subject Scheme ID & Settings
			$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $ReportID);
			$SchemeID = $SubjectFormGradingSettings['SchemeID'];
			$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
			$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
			$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
			
			$thisMSGrade = $_MarksAry[0]['Grade'];
			$thisMSMark = $this->ROUND_MARK($_MarksAry[0]['Mark'], 'SubjectTotal');
			
			$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
			$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
			
            $thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
			list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $thisMSGrade);
			if($needStyle) {
				$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMark);
			} else {
				$thisMarkDisplay = $thisMark;
			}

			// [2020-0324-1007-53066] alignment for failed subjects
			if(strpos($thisMarkDisplay, '*') !== false) {
                $thisMarkDisplay = '&nbsp;'.$thisMarkDisplay;
            }
			
			// Get Mark Nature
			if($StudentID)
			{
			    $thisNature = 'SC';
			    if(!$this->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisMSGrade) && !$this->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisMSGrade)) {
				    $thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMSMark, '', '', $ReportID);
			    }
			}
			
			// Special Display for '+'
			if($StudentID && $ReportType == 'T' && $thisMSGrade == '+')
			{
			    $thisMarkDisplay = $this->EmptySymbol;
			    
			    // Get 1st Report Column
			    $ReportColumnArr = $this->returnReportTemplateColumnData($ReportID);
			    $targetReportColumn = reset($ReportColumnArr);
			    $targetReportColumnID = $targetReportColumn['ReportColumnID'];
			    
			    // Get Column Mark & Grade
			    $thisColumnMSGrade = $_MarksAry[$targetReportColumnID]['Grade'];
			    $thisColumnMSMark = $this->ROUND_MARK($_MarksAry[$targetReportColumnID]['Mark'], 'SubjectTotal');
			    if((($thisColumnMSGrade == '') || ($thisColumnMSGrade != '' && !$this->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisColumnMSGrade) && !$this->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisColumnMSGrade))) && is_numeric($thisColumnMSMark) && $thisColumnMSMark != -1)
			    {
			        $thisColumnMSWeight = $this->returnReportTemplateSubjectWeightData($ReportID, '', $SubjectID, $targetReportColumnID);    
			        $thisColumnMSWeight = $thisColumnMSWeight[0]['Weight'];
			        
			        $thisColumnMSFullMark = $SchemeInfo['FullMark'];
			        if($thisColumnMSWeight > 0 && $thisColumnMSFullMark > 0) {
			            $thisMarkDisplay = $this->ROUND_MARK(($thisColumnMSMark * $thisColumnMSWeight), 'SubjectTotal')." / ".($thisColumnMSFullMark * $thisColumnMSWeight);
			        }
			    }
			}
			
			$returnArr[$SubjectID]['Display'] = $thisMarkDisplay;
			$returnArr[$SubjectID]['Pass'] = $thisNature;
			$returnArr[$SubjectID]['isAllNA'] = $thisMark == "N.A.";
		}
		
		return $returnArr;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $FailUnitArr=array(), $rowLoc=0) {
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ReportSemID = $ReportSetting['Semester'];
		$ReportSemNum = $this->Get_Semester_Seq_Number($ReportSemID);
		$isYearReport = $ReportSemID == "F";
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$SchoolType	= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		$isPrimaryLevel = $SchoolType == "P";
		
		# Term Reports
		$AllTermReport = $this->getAllTermReport($ReportID);
		
		# Report Footers
		$FooterArr = $eRCTemplateSetting['MSTable']['FooterField'];
		
		### FooterArr => Total Unit(s) Failed / Average / Number of Students in Class / Position in Class / Periods Absent / Times Tardy
		# Term Results
		$resultAry = array();	
		for($i=0; $i<2; $i++)
		{
		    $thisTermID = $AllTermReport[$i]['YearTermID'];
			$thisTermReportID = $AllTermReport[$i]['ReportID'];
			if($isYearReport) {          // Handle Final Report
				//$CurrentTermOrder = 3;
			    $CurrentTermOrder = 2;
			} else if($thisTermReportID == $ReportID) {
				$CurrentTermOrder = $i;
			}
			
			// Get Overall Result & Profile Data
			if($StudentID) {
				$columnResult = $this->getReportResultScore($thisTermReportID, 0, $StudentID);
				$profileData = $this->Get_Student_Profile_Data($thisTermReportID, $StudentID, $MapByRecordType=false, $CheckRecordStatus=true, $returnTotalUnit=false, $YearTermID='', $returnAttendanceRecords=true);
				$otherInfoDataAry = $this->getReportOtherInfoData($thisTermReportID, $StudentID);
			}
			
			$resultAry[$i]['TotalUnitFailed'] = $FailUnitArr[$i]? $FailUnitArr[$i] : 0;
			$resultAry[$i]['Average'] = $StudentID? $this->Get_Score_Display_HTML($this->ROUND_MARK($columnResult['GrandAverage'], 'GrandAverage'), $thisTermReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
			$resultAry[$i]['NumberOfStudentsInClass'] = $columnResult['ClassNoOfStudent'] > 0? $columnResult['ClassNoOfStudent'] : $this->EmptySymbol;
			$resultAry[$i]['PositionInClass'] = $columnResult['OrderMeritClass'] > 0 && $columnResult['OrderMeritClass'] <= 20 && $resultAry[$i]['TotalUnitFailed'] == 0? $columnResult['OrderMeritClass'] : $this->EmptySymbol;
			//$resultAry[$i]['PeriodsAbsent'] = $profileData['AttendanceAry'][$StudentID]['Days Absent'] > 0? $profileData['AttendanceAry'][$StudentID]['Days Absent'] : 0;
			//$resultAry[$i]['PeriodsAbsent'] = $otherInfoDataAry[$StudentID][$thisTermID]['Periods Absent'] != ''? $otherInfoDataAry[$StudentID][$thisTermID]['Periods Absent'] : $resultAry[$i]['PeriodsAbsent'];
			//$resultAry[$i]['TimesTardy'] = $profileData['AttendanceAry'][$StudentID]['Time Late'] > 0? $profileData['AttendanceAry'][$StudentID]['Time Late'] : 0;
			//$resultAry[$i]['TimesTardy'] = $otherInfoDataAry[$StudentID][$thisTermID]['Times Tardy'] != ''? $otherInfoDataAry[$StudentID][$thisTermID]['Times Tardy'] : $resultAry[$i]['TimesTardy'];
			$resultAry[$i]['PeriodsAbsent'] = $otherInfoDataAry[$StudentID][$thisTermID]['Periods Absent'] != ''? $otherInfoDataAry[$StudentID][$thisTermID]['Periods Absent'] : 0;
			$resultAry[$i]['TimesTardy'] = $otherInfoDataAry[$StudentID][$thisTermID]['Times Tardy'] != ''? $otherInfoDataAry[$StudentID][$thisTermID]['Times Tardy'] : 0;
			
			if(!$isYearReport && $ReportSemNum == ($i + 1)) {
			    break;
			}
		}
		
		# Final Results
		if($isYearReport)
		{
		    // Get Overall Result & Profile Data
			if($StudentID) {
				$columnResult = $this->getReportResultScore($ReportID, 0, $StudentID);
				$profileData = $this->Get_Student_Profile_Data($ReportID, $StudentID, $MapByRecordType=false, $CheckRecordStatus=true, $returnTotalUnit=false, $YearTermID='', $returnAttendanceRecords=true);
				$otherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
			}
			
			$resultAry['Final']['TotalUnitFailed'] = $FailUnitArr['Final']? $FailUnitArr['Final'] : 0;
			$resultAry['Final']['Average'] = $StudentID? $this->Get_Score_Display_HTML($this->ROUND_MARK($columnResult['GrandAverage'], 'GrandAverage'), $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
			$resultAry['Final']['NumberOfStudentsInClass'] = $columnResult['ClassNoOfStudent'] > 0? $columnResult['ClassNoOfStudent'] : $this->EmptySymbol;
			$resultAry['Final']['PositionInClass'] = $columnResult['OrderMeritClass'] > 0 && $columnResult['OrderMeritClass'] <= 20 && $resultAry['Final']['TotalUnitFailed'] == 0? $columnResult['OrderMeritClass'] : $this->EmptySymbol;
			// $resultAry['Final']['PeriodsAbsent'] = $profileData['AttendanceAry'][$StudentID]['Days Absent'] > 0? $profileData['AttendanceAry'][$StudentID]['Days Absent'] : 0;
			// $resultAry['Final']['PeriodsAbsent'] = $otherInfoDataAry[$StudentID][0]['Periods Absent'] != ''? $otherInfoDataAry[$StudentID][0]['Periods Absent'] : $resultAry['Final']['PeriodsAbsent'];
			// $resultAry['Final']['TimesTardy'] = $profileData['AttendanceAry'][$StudentID]['Time Late'] > 0? $profileData['AttendanceAry'][$StudentID]['Time Late'] : 0;
			// $resultAry['Final']['TimesTardy'] = $otherInfoDataAry[$StudentID][0]['Times Tardy'] != ''? $otherInfoDataAry[$StudentID][0]['Times Tardy'] : $resultAry['Final']['TimesTardy'];
			$resultAry['Final']['PeriodsAbsent'] = $otherInfoDataAry[$StudentID][0]['Periods Absent'] != ''? $otherInfoDataAry[$StudentID][0]['Periods Absent'] : 0;
			$resultAry['Final']['TimesTardy'] = $otherInfoDataAry[$StudentID][0]['Times Tardy'] != ''? $otherInfoDataAry[$StudentID][0]['Times Tardy'] : 0;
		}
		
		$t = array();
		foreach ((array)$FooterArr as $_FooterArr)
		{
			$TitleCh = $_FooterArr."Ch";
			$TitleEn = $_FooterArr."En";
			
			if($_FooterArr == 'TotalUnitFailed' || $_FooterArr == 'PeriodsAbsent') {
				$border_top_css = 'border_top_bold';
			} else {
				$border_top_css = 'border_top';
			}
			
			$subjectStyle = '';
			if($isPrimaryLevel) {
			    $subjectStyle = " style='padding: 2.5px 3px 2.5px 3px;' ";
			}
			
			$x = "<td colspan='3' class='$border_top_css border_right' $subjectStyle>";
				$x .= "<table>";
					$x .= "<td width='170px'>".$eReportCard['Template'][$TitleEn]."</td>";
					$x .= "<td>".$eReportCard['Template'][$TitleCh]."</td>";
				$x .= "</table>";
			$x .= "</td>";
			
			// Term Loop
			for($i=0; $i<2; $i++) {
			    $x .= "<td class='$border_top_css border_right' align='center'>";
				if($i <= $CurrentTermOrder) {
				    $resultDisplay = $resultAry[$i][$_FooterArr] ? $resultAry[$i][$_FooterArr] : 0;

                    // [2020-0324-1007-53066] alignment fix
                    if(strpos($resultDisplay, '*') !== false) {
                        $resultDisplay = '&nbsp;'.$resultDisplay;
                    }

				   $x .= $resultDisplay;
				} else {
					$x .= "&nbsp;";
				}
				$x .= "</td>";
			}
			
			// Final Average
			$colspan = $isPrimaryLevel? 2 : 1;
			$x .= "<td class='$border_top_css border_right_double' align='center' colspan='$colspan'>";
				if($isYearReport) {
                    $resultDisplay = $resultAry['Final'][$_FooterArr] ? $resultAry['Final'][$_FooterArr] : 0;

                    // [2020-0324-1007-53066] alignment fix
                    if(strpos($resultDisplay, '*') !== false) {
                        $resultDisplay = '&nbsp;'.$resultDisplay;
                    }

                    $x .= $resultDisplay;
				}
				else{
					$x .= "&nbsp;";
				}
			$x .= "</td>";
			
			$t[$rowLoc++] = $x;
		}
		
		return $t;
	}
	
	function getMiscTable($ReportID, $StudentID='', $PrintTemplateType='') {
		// do nothing
	}
	
	function getSignatureTable($ReportID='', $StudentID='', $PrintTemplateType='') {
	    global $eRCTemplateSetting, $eReportCard;
	    
	    # Retrieve Display Settings
	    $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
	    $ReportSemID = $ReportSetting['Semester'];
	    $isYearReport = $ReportSemID == "F";
	    $ClassLevelID = $ReportSetting['ClassLevelID'];
	    $SchoolType	= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
	    $FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
	    $isPrimaryLevel = $SchoolType == "P";
	    $isSecondaryLevel = $SchoolType == "S";
		
	    # Retrieve Final Result Display
		$PromotionStatus = '&nbsp;';
		if($StudentID && $isYearReport)
		{
		    // Get Generated / Modified Promotion Status
		    $StudentPromotionStatus = $this->GetPromotionStatusList('', '', $StudentID, $ReportID);
		    if(!empty($StudentPromotionStatus))
		    {
		        $StudentPromotionStatus = $StudentPromotionStatus[$StudentID]["FinalStatus"];
		        $PromotionStatusValue = array_search($StudentPromotionStatus, $eReportCard["PromotionStatus"]);
		        
		        // Default Display for Promotion Status
		        if($StudentPromotionStatus != '' && isset($eReportCard["PromotionStatusDisplayArr"][$StudentPromotionStatus]) && is_integer($PromotionStatusValue) && $PromotionStatusValue > 0)
		        {
		            $allPromotionStatus = $this->customizedPromotionStatus;
		            
		            // Promoted
		            if($PromotionStatusValue == $allPromotionStatus["Promoted"])
		            {
		                if($isSecondaryLevel && $FormNumber == 6)
		                {
		                    // do nothing
		                }
		                else
		                {
    		                $NextFormNumber = $FormNumber + 1;
    		                if($isPrimaryLevel && $FormNumber == 6) {
    		                    $SchoolType = 'S';
    		                    $NextFormNumber = 1;
    		                }
    		                $NextFormNumber = $this->Get_English_Number_Word($NextFormNumber);
    		                
    		                $PromotionStatus = $eReportCard["PromotionStatusDisplayArr"][$StudentPromotionStatus][$SchoolType];
    		                $PromotionStatus = str_replace('<!--Level-->', $NextFormNumber, $PromotionStatus);
		                }
		            }
		            // Retained
		            else if($PromotionStatusValue == $allPromotionStatus["Retained"])
		            {
		                if($isSecondaryLevel && $FormNumber == 6)
		                {
		                    // do nothing
		                }
		                else
		                {
    		                $FormNumber = $this->Get_English_Number_Word($FormNumber);
    		                
    		                $PromotionStatus = $eReportCard["PromotionStatusDisplayArr"][$StudentPromotionStatus][$SchoolType];
    		                $PromotionStatus = str_replace('<!--Level-->', $FormNumber, $PromotionStatus);
		                }
		            }
		            // Graduated
		            else if($PromotionStatusValue == $allPromotionStatus["Graduated"])
		            {
		                if($isSecondaryLevel && $FormNumber == 6)
		                {
                            $PromotionStatus = $eReportCard["PromotionStatusDisplayArr"][$StudentPromotionStatus];
		                }
		            }
		        }
		    }
		    
		    // Manual Input Display Content (Other Info)
		    $otherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
		    $PromotionDisplayInput = $otherInfoDataAry[$StudentID][0]['Promotion Content'];
		    if($PromotionDisplayInput != '') {
		        $PromotionStatus = $PromotionDisplayInput;
		    }
		}
		
		$x = "<table width='100%' class='signature_table border_left_double border_right_double border_bottom_double'>";
		$x .= "<tr>";
		
			$isFirst = false;
			foreach ((array)$eRCTemplateSetting['Signature'] as $_eRCTemplateSetting)
			{
				$EnTitle = $_eRCTemplateSetting.'En';
				$ChTitle =  $_eRCTemplateSetting.'Ch';
				
				$border_left_css = $isFirst? "border_left" : "";
				$x .= "<td align='center' class='$border_left_css' width='169.333px'>"; 
					$x .= $eReportCard['Template'][$EnTitle].'&nbsp;'.$eReportCard['Template'][$ChTitle];
				$x .= "</td>";
				
				$isFirst = true;
			}
			
			$x .= "<td align='center' width='200px' class='border_left_double'>";
				$x .= $eReportCard['Template']['FinalResultEn']."&nbsp;".$eReportCard['Template']['FinalResultCh'];
			$x .= "</td>";
			
		$x .= "</tr>";
		
		$x .= "<tr>";
		
            $isFirst = false;
			foreach ((array)$eRCTemplateSetting['Signature'] as $_eRCTemplateSetting)
			{
			    $border_left_css = $isFirst? "border_left":"";
				$x .= "<td align='center' height='85px' class='border_top $border_left_css'>&nbsp;</td>";
				
				$isFirst= true;
			}
			$x .= "<td align='center' height='85px' class='border_left_double border_top'>".$PromotionStatus."</td>";
			
		$x .= "</tr>";
		$x .= "</table>";
		
		return $x;
	}
	
	function ClassTeacherComment($ReportID, $StudentID)
	{
		global $eRCTemplateSetting, $eReportCard, $intranet_root;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$isYearReport = $ReportSetting['Semester'] == "F";
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$SchoolType	= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		
		# Term Reports
		$AllReportID = $this->getAllTermReport($ReportID);
		
		# Term Comment rows
// 		$TotalRowCount = $TotalRowCount - count($HideRowArr);
// 		$TermRow[0] = round($TotalRowCount / 2); 
// 		$TermRow[1] = $TotalRowCount - $TermRow[0];
		$TermRow = $eRCTemplateSetting['MSTable']['CommentAreaRowSize'][$SchoolType];
		
		// Term Loop
		for($i=0; $i<2; $i++) {
            // Handle Final Report
			if($isYearReport) {
				//$CurrentTermOrder = 3;
			    $CurrentTermOrder = 2;
			} else if($AllReportID[$i]['ReportID'] == $ReportID) {
				$CurrentTermOrder = $i;
			}
		}
		
		$t = array();
		$rowNumber = 0;
		for($i=0; $i<2; $i++)
		{
		    // Get Class Teacher Comment
		    $ClassTeacherComment = '';
			if($StudentID) {
			    $ClassTeacherComment = $this->returnSubjectTeacherCommentByBatch($AllReportID[$i]['ReportID'], $StudentID);
			    $ClassTeacherComment = $ClassTeacherComment[0][$StudentID]['Comment'];
			}
			
			$t[$rowNumber++] = "<td class='border_top' align='center'>".$eReportCard['Template']['TermEn'][$i]."&nbsp;".$eReportCard['Template']['TermCh'][$i]."</td>";
			
			$thisRow = '';
			$thisRowCount = $TermRow[$i];
			if($CurrentTermOrder >= $i)
            {
			    $thisRow .= "<td rowspan='$thisRowCount' class='border_top' valign='top' width='200px'>";
    			    if($StudentID == '')
    			    {
    			        $thisRow .= '&nbsp;';
    			    }
    			    else
                    {
    			    	if($i == 1)
    			    	{
    			    	    ## eDiscipline data
                            // Get Merit Type Name
                            $merit_demerit_customize_file = "$intranet_root/file/merit.en.customized.txt";
                            if(file_exists($merit_demerit_customize_file) && is_file($merit_demerit_customize_file) && filesize($merit_demerit_customize_file) != 0)
                            {
                                $file_content_merit_demeirt_wordings = get_file_content($merit_demerit_customize_file);
                                $lines = explode("\n",$file_content_merit_demeirt_wordings);

                                $i_Merit_Merit = $lines[0];
                                $i_Merit_MinorCredit = $lines[1];
                                $i_Merit_MajorDemerit = $lines[7];
                            }

                            // Get Merit Type count (from Student Profile)
                            //$studProfile = $this->Get_Student_Profile_Data($ReportID, $StudentID, $MapByRecordType=true);
                            $studProfile = $this->Get_Student_Profile_Data($ReportID, $StudentID, true, false, true);
                            $studProfile[1] = $studProfile[1] ? $studProfile[1] : 0;
                            $studProfile[2] = $studProfile[2] ? $studProfile[2] : 0;
                            $studProfile[-1] = $studProfile[-1] ? $studProfile[-1] : 0;
                            $studProfile[-2] = $studProfile[-2] ? $studProfile[-2] : 0;
                            $studProfile[-3] = $studProfile[-3] ? $studProfile[-3] : 0;

                            // auto convert Merit Type count
                            $disciplineData = array();
                            $disciplineData[1] = $studProfile[1] % 3;
                            $disciplineData[2] = $studProfile[2] + floor(($studProfile[1] / 3));
                            $disciplineData[-1] = $studProfile[-1] % 3;
                            $disciplineData[-2] = ($studProfile[-2] + floor(($studProfile[-1] / 3))) % 3;
                            $disciplineData[-3] = $studProfile[-3] + floor((($studProfile[-2] + floor(($studProfile[-1] / 3))) / 3));

                            /*
                            // Get manual Merit Type count (from Other Info)
                            $otherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID, '', 'merit_demerit');
                            $disciplineOtherInfoData = $otherInfoDataAry[$StudentID][0];
                            if(!empty($disciplineOtherInfoData))
                            {
                                if(isset($disciplineOtherInfoData['Credits'])) {
                                    $disciplineData[1] = $disciplineOtherInfoData['Credits'];
                                }
                                if(isset($disciplineOtherInfoData['Merits'])) {
                                    $disciplineData[2] = $disciplineOtherInfoData['Merits'];
                                }
                                if(isset($disciplineOtherInfoData['Misdemeanors'])) {
                                    $disciplineData[-1] = $disciplineOtherInfoData['Misdemeanors'];
                                }
                                if(isset($disciplineOtherInfoData['Minor Misdeeds'])) {
                                    $disciplineData[-2] = $disciplineOtherInfoData['Minor Misdeeds'];
                                }
                                if(isset($disciplineOtherInfoData['Major Misdeeds'])) {
                                    $disciplineData[-3] = $disciplineOtherInfoData['Major Misdeeds'];
                                }
                            }
                            */
                            
    			    		// Teacher Comment
    			    		if($ClassTeacherComment != '') {
	    			    		$thisRow .= nl2br($ClassTeacherComment);
	    			    		$thisRow .= "<br/>";
    			    		}

    			    		// Credit
    			    		if($disciplineData[1]) {
	    			    		$thisRow .= $i_Merit_Merit . "(s): " . ($disciplineData[1] ? $disciplineData[1] : 0);
	    			    		$thisRow .= "<br/>";
    			    		}
    			    		// Merit
    			    		if($disciplineData[2]) {
	    			    		$thisRow .= $i_Merit_MinorCredit. "(s): " . ($disciplineData[2] ? $disciplineData[2] : 0);
	    			    		$thisRow .= "<br/>";
    			    		}
    			    		// Major Misdeed
    			    		if($disciplineData[-3]) {
    			    			$thisRow .= $i_Merit_MajorDemerit. "(s): " . ($disciplineData[-3] ? $disciplineData[-3] : 0);
    			    		}
    			    	}
    			    	else
                        {
	    			        $thisRow .= nl2br($ClassTeacherComment);
				    	}
    			    }
			    $thisRow .= '</td>';
			}
			else
            {
			    $thisRow = "<td rowspan='$thisRowCount' class='border_top'>&nbsp;</td>";
			}
			$t[$rowNumber++] = $thisRow;
			
			for($j=0; $j<($thisRowCount - 1); $j++) {
			    $t[$rowNumber++] = '';
			}
		}
		
		/*
		$t = array();
		$count = 0;
		
		$numOfrow = sizeof((array)$y);
		for ($i=0; $i<$numOfrow; $i++) {
			$t[$count] = $y[$i];
			$count++;
		}
		*/
		
		return $t;
	}
	
	function getMeritsAndDemerits($ReportID, $StudentID, $rowLoc=0){
	    global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
	    
	    # Retrieve Display Settings
	    $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
	    $isYearReport = $ReportSetting['Semester']=="F";
	    $ClassLevelID = $ReportSetting['ClassLevelID'];
	    $SchoolType	= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
	    $isPrimaryLevel = $SchoolType == "P";
		
		### Merits And Demerits
		$MeritsAndDemeritsField = $eRCTemplateSetting['MSTable']['MeritsAndDemeritsField'];
		
		# Term Reports
		$AllTermReport = $this->getAllTermReport($ReportID);
		
		// Term Loop
		for($i=0; $i<2; $i++) {
			if($isYearReport) {          // Handle Final Report
				//$CurrentTermOrder = 3;
			    $CurrentTermOrder = 2;
			} else if($AllTermReport[$i]['ReportID'] == $ReportID) {
				$CurrentTermOrder = $i;
			}
		}
		
		$t = array();
		
		/*
		foreach ((array)$MeritsAndDemeritsField as $_MeritsAndDemeritsField) {
			$TitleCh = $_MeritsAndDemeritsField."Ch";
			$TitleEn = $_MeritsAndDemeritsField."En";
			
			$x = "<td colspan='3' class='border_top border_right'>";
				$x .= "<table>";
					$x .= "<td width='170px'>".$eReportCard['Template'][$TitleEn]."</td>";
					$x .= "<td>";
                        $x .= $this->handleChineseContentDisplay($eReportCard['Template'][$TitleCh]);
					$x .= "</td>";
				$x .= "</table>";
			$x .= "</td>";
			
			// Term Loop
			for($i=0; $i<2; $i++) {
				if($i <= $CurrentTermOrder) {
				    // Get Other Info data
					if($StudentID) {
					    $OtherInfoDataAry = $this->getReportOtherInfoData($AllTermReport[$i]['ReportID'], $StudentID);
					}
					$MeritInfoData = $OtherInfoDataAry[$StudentID][$AllTermReport[$i]['YearTermID']][$_MeritsAndDemeritsField];
					
					$x .= "<td class='border_top border_right' align='center'>";
					   $x .= $StudentID? ($MeritInfoData? $MeritInfoData: 0) : 0;
					$x .= "</td>";
				} else {
					$x .= "<td class='border_top border_right' align='center'>&nbsp;</td>";
				}
			}
			
			// Final Average
			if($isYearReport) {
			    // Get Other Info data
				if($StudentID) {
					$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
				}
				$MeritInfoData = $OtherInfoDataAry[$StudentID][0][$_MeritsAndDemeritsField];
				
				$x .= "<td class='border_top border_right_double' align='center'>";
				    $x .= $StudentID? ($MeritInfoData? $MeritInfoData: 0) : 0;
				$x .= "</td>";
			} else {
			    $x .= "<td class='border_top border_right_double' align='center'>&nbsp;</td>";
			}
			$t[] = $x;
		}
		*/
		
		$subjectStyle = '';
		if($isPrimaryLevel) {
		    $subjectStyle = " style='padding: 3px 3px 2.5px 3px;' ";
		}
		
		### Overall Conduct
		$ConductField = $eRCTemplateSetting['MSTable']['ConductField'];
		foreach ((array)$ConductField as $_ConductField){
			$TitleCh = $_ConductField."Ch";
			$TitleEn = $_ConductField."En";
			
			$x = "<td colspan='3' class='border_top border_right' $subjectStyle>";
				$x .= "<table>";
					$x .= "<td width='170px'>".$eReportCard['Template'][$TitleEn]."</td>";
					$x .= "<td>";
					   $x .= $this->handleChineseContentDisplay($eReportCard['Template'][$TitleCh]);
					$x .= "</td>";
				$x .= "</table>";
			$x .= "</td>";
			
			// Term Loop
			for($i=0; $i<2; $i++) {
			    if($i <= $CurrentTermOrder){
			        // Get Other Info data
					if($StudentID) {
						$OtherInfoDataAry = $this->getReportOtherInfoData($AllTermReport[$i]['ReportID'], $StudentID);
					}
					$ConductInfoData = $OtherInfoDataAry[$StudentID][$AllTermReport[$i]['YearTermID']][$_ConductField];
					if(is_array($ConductInfoData)) {
					    $ConductInfoData = $ConductInfoData[0];
					}
                    $ConductInfoData = str_replace('＋', '+', $ConductInfoData);
                    $ConductInfoData = str_replace('—', '-', $ConductInfoData);

                    // [2020-0324-1007-53066] alignment fix
                    if($ConductInfoData && (strpos($ConductInfoData, '*') !== false || strpos($ConductInfoData, '+') !== false)) {
                        $ConductInfoData = '<span style="font-size: 10pt; line-height: 9pt;">&nbsp;&nbsp;</span>'.$ConductInfoData;
                    }
                    else if($ConductInfoData && strpos($ConductInfoData, '-') !== false) {
                        $ConductInfoData = '<span style="font-size: 11pt; line-height: 9pt;">&nbsp;&nbsp;</span>'.str_replace('-', '', $ConductInfoData).'<span style="font-size: 11pt;line-height: 9pt;font-family: cursive;">-<span>';
                    }

                    $x .= $resultDisplay;
					
					$x .= "<td class='border_top border_right' align='center' style='word-break: break-all;'>";
					   $x .= $StudentID? ($ConductInfoData ? $ConductInfoData : $this->EmptySymbol) : $this->EmptySymbol;
					$x .= "</td>";
				} else {
					$x .= "<td class='border_top border_right' align='center'>&nbsp;</td>";
				}
			}
			
			// Final Average
			$colspan = $isPrimaryLevel? 2 : 1;
			if($isYearReport)
			{
			    // Get Other Info data
				if($StudentID) {
					$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
				}
				$ConductInfoData = $OtherInfoDataAry[$StudentID][0][$_ConductField];
				if(is_array($ConductInfoData)) {
				    $ConductInfoData = $ConductInfoData[0];
				}

                // [2020-0324-1007-53066] alignment fix
                if($ConductInfoData && (strpos($ConductInfoData, '*') !== false || strpos($ConductInfoData, '+') !== false)) {
                    $ConductInfoData = '<span style="font-size: 10pt; line-height: 9pt;">&nbsp;&nbsp;</span>'.$ConductInfoData;
                }
                else if($ConductInfoData && strpos($ConductInfoData, '-') !== false) {
                    $ConductInfoData = '<span style="font-size: 11pt; line-height: 9pt;">&nbsp;&nbsp;</span>'.str_replace('-', '', $ConductInfoData).'<span style="font-size: 11pt;line-height: 9pt;font-family: cursive;">-<span>';
                }
				
				$x .= "<td class='border_top border_right_double' align='center' style='word-break: break-all;' colspan='$colspan'>";
				    $x .= $StudentID? ($ConductInfoData ? $ConductInfoData : $this->EmptySymbol) : $this->EmptySymbol;
				$x .= "</td>";
			}
			else
			{
			    $x .= "<td class='border_top border_right_double' align='center' colspan='$colspan'>&nbsp;</td>";
			}
			
			$t[$rowLoc++] = $x;
		}
		
		return $t;
	}
	
	function getECATable($ReportID, $StudentID, $rowLoc, $defaultMaxRow) {
	    global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
	    
	    # Retrieve Display Settings
	    $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
	    $isYearReport = $ReportSetting['Semester']=="F";
	    $ClassLevelID = $ReportSetting['ClassLevelID'];
	    $SchoolType	= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
	    $isPrimaryLevel = $SchoolType == "P";
	    
	    # ECA rows
	    $TermRow = $eRCTemplateSetting['MSTable']['ECAAreaRowSize'][$SchoolType];
	    if(!$isPrimaryLevel) {
            $diffRow = $defaultMaxRow - ($rowLoc + 1);
            if($diffRow > $TermRow) {
                $TermRow = $diffRow;
            }
        }

	    # Term Reports
	    $AllTermReport = $this->getAllTermReport($ReportID);
		
		// Term Loop
		for($i=0; $i<2; $i++) {
			if($isYearReport) { 	    // Handle Final Report
				//$CurrentTermOrder = 3;
			    $CurrentTermOrder = 2;
			} else if($AllTermReport[$i]['ReportID'] == $ReportID) {
				$CurrentTermOrder = $i;
			}
		}
        //$targetTermID = $isYearReport? 0 : $AllTermReport[$CurrentTermOrder]['YearTermID'];

        //$ECA1GradingDataAry = array();
        //$ECA2GradingDataAry = array();
        //$OtherInfoDataAry = array();

        $ECAContent = '&nbsp;';
		if($StudentID)
		{
            //$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
            //$OtherInfoDataAry = $OtherInfoDataAry[$StudentID][$targetTermID]['Activities'];
            //list($ECA1GradingDataAry, $ECA2GradingDataAry) = $this->getStudentECAGradingInfo($ReportID, $StudentID);
            $ECAContent = $this->getStudentECAContentDisplay($ReportID, $StudentID);
		}
		
		/*
		$t = array();
		for($i=$NumOfRow; $i>0; $i--) {
			if($i == $NumOfRow) {
				$x = "<td class='border_top' align='center'>".$eReportCard['Template']['eca']."</td>";
			} else if($i == $NumOfRow - 1) {
				$x = "<td rowspan='$i' class='border_top'>";
				if($isYearReport) {	    // Final Report
					foreach ((array)$OtherInfoDataAry[$StudentID][0]['Activities'] as $_eca) {
						$x .= "-"."&nbsp;".$_eca."</br>";
					}
				} else {	           // Term Report
					foreach ((array)$OtherInfoDataAry[$StudentID][$AllTermReport[$CurrentTermOrder]['YearTermID']]['Activities'] as $_eca) {
						$x .= "-"."&nbsp;".$_eca."</br>";
					}
				}
				$x .= "</td>";
			} else {
				$x = "";
			}
			$t[] = $x;
		}
		*/
		
		if($isPrimaryLevel)
		{
    		$t = array();
    		$t[$rowLoc++] = "<td class='border_top' align='center'>".$eReportCard['Template']['eca']."</td>";
    		
    		$thisRow = '';
    		$thisRow .= "<td rowspan='$TermRow' class='border_top' valign='top' width='200px'>";
                $thisRow .= $ECAContent;
    		/*
                foreach ((array)$OtherInfoDataAry as $_eca) {
                    $thisRow .= "&nbsp;".$_eca."</br>";
                }
            */
            $thisRow .= "</td>";
            $t[$rowLoc++] = $thisRow;
            
            for($j=0; $j<($TermRow - 1); $j++) {
                $t[$rowLoc++] = '';
            }
		}
		else
		{
		    $t = array();
		    $t[$rowLoc++] = "<td colspan='6' class='border_top border_right_double' align='center'>".$eReportCard['Template']['eca']."</td>";
		    
		    // [2019-0627-1713-22096] add class and style to td to remove paddings
		    //$ecaSize = count($OtherInfoDataAry);
		    //$remove_padding = $ecaSize > ($TermRow + 1);
		    //$td_class = $remove_padding? " td_padding_removed " : '';
		    //$td_padding_style = $remove_padding? " style='padding: 0px 3px;' " : '';
		    $td_border_style = "border_top";

		    /*
		    foreach ((array)$OtherInfoDataAry as $_eca) {
		        $thisRow = '';
		        $thisRow .= "<td colspan='6' class='$td_border_style border_right_double $td_class' $td_padding_style>";
                $thisRow .= "<td class='$td_border_style border_right_double $td_class' $td_padding_style>";
		            $thisRow .= "&nbsp;".$_eca;
		        $thisRow .= "</td>";
		        $t[$rowLoc++] = $thisRow;

		        $td_border_style = '';
		    }

		    if($TermRow > $ecaSize)
		    {
		        for($j=0; $j<($TermRow - $ecaSize); $j++) {
		            //$thisRow = "<td colspan='6' class='$td_border_style border_right_double'>&nbsp;</td>";
                    $thisRow = "<td class='$td_border_style border_right_double'>&nbsp;</td>";
		            $t[$rowLoc++] = $thisRow;

		            $td_border_style = '';
    		    }
		    }
		    */

            for($j=0; $j<$TermRow; $j++)
            {
                $thisRow = '';
                if($j == 0)
                {
                	$thisHeight = $TermRow * 18;
                    $thisRow .= "<td rowspan='$TermRow' height='{$thisHeight}px' colspan='6' class='$td_border_style $td_class border_right_double' $td_padding_style valign='top'>";
                        $thisRow .= $ECAContent;
                    $thisRow .= "</td>";
                }
                // 2020-04-03 (Philips) [Z180689] Use 2 Column ECADisplay for Senco	dary
                // $thisRow .= "<td class='$td_border_style border_right_double'>&nbsp;</td>";
                $t[$rowLoc++] = $thisRow;

                $td_border_style = '';
            }
		}

		return $t;
	}

	function getStudentECAContentDisplay($ReportID, $StudentID)
    {
        global $eReportCard;

        # Retrieve Display Settings
        $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
        $SemID = $ReportSetting['Semester'];
        $SemNum = $this->Get_Semester_Seq_Number($SemID);
        $isYearReport = $SemID == "F";
        $ClassLevelID = $ReportSetting['ClassLevelID'];
        $SchoolType	= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
        $isPrimaryLevel = $SchoolType == "P";

        # ECA rows
        $TermRow = $eRCTemplateSetting['MSTable']['ECAAreaRowSize'][$SchoolType];

        # Term Reports
        $AllTermReport = $this->getAllTermReport($ReportID);

        // Term Loop
        for($i=0; $i<2; $i++)
        {
            if($isYearReport) {
                $CurrentTermOrder = 2;
            }
            else if($AllTermReport[$i]['ReportID'] == $ReportID) {
                $CurrentTermOrder = $i;
            }
        }
        $targetTermID = $isYearReport ? 0 : $AllTermReport[$CurrentTermOrder]['YearTermID'];

        // Get ECA / Other Info data
        if($StudentID)
        {
            $OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
            list($ECA1GradingDataAry, $ECA2GradingDataAry) = $this->getStudentECAGradingInfo($ReportID, $StudentID);
        }
        $valign = ($isPrimaryLevel) ? '' : 'vertical-align:top';
        $thisContent = '&nbsp;';
        if(!empty($ECA1GradingDataAry) || !empty($ECA2GradingDataAry))
        {
            $thisContent = '';
            $tdIndex = 0;

            // 2020-04-03 (Philips) [Z180689] Use 2 Column ECADisplay for Sencondary
            if(!$isPrimaryLevel)
            {
            	$thisContent .= '<table width="100%" height="100%">';
            		$thisContent .= '<tbody>';
            			$thisContent .= "<tr height='0'>";
            				$thisContent .= '<td width="50%" style="'.$valign.'">';
            }

            // ECA data (Club)
            foreach((array)$ECA1GradingDataAry as $index => $thisGradingData)
            {
                if($index > 0) {
                	if($isPrimaryLevel) {
	                    $thisContent .= '<br/>';
	                    $thisContent .= '<br/>';
                	}
                }

                $thisContent .= $thisGradingData['ClubTitleEN'];
                if(trim($thisGradingData['RoleTitle']) != '') {
                    $thisContent .= ' ('.trim($thisGradingData['RoleTitle']).')';
                }
                $thisContent .= '<br/>';

                for($i=0; $i<2; $i++)
                {
                    if($i > 0) {
                        $thisContent .= '<br/>';
                    }

                    $thisContent .= $eReportCard['Template']['TermEn'][$i];
                    if(trim($thisGradingData['TermBasedPer'.($i + 1)]) != '') {
                        $thisContent .= ' - '.trim($thisGradingData['TermBasedPer'.($i + 1)]);
                    }

                    if($SemNum == 1) {
                        break;
                    }
                }

                if($SemNum != 1 && trim($thisGradingData['CommentStudent']) != '') {
                    $thisContent .= '<br/>';
                    $thisContent .= nl2br(trim($thisGradingData['CommentStudent']));
                }
                
                if(!$isPrimaryLevel)
                {
                	$thisContent .= '</td>';
                	if($tdIndex == 1) {
                		$thisContent .= "</tr>";
                        $thisContent .= "<tr height='0'>";
                	}
                	$thisContent .= '<td width="50%" style="'.$valign.'">';
                	$tdIndex++;
                }

                // display 2 records only
                if($index >= 1) {
                    break;
                }
            }

            // ECA data (House)
            if(!empty($ECA2GradingDataAry))
            {
                if($isPrimaryLevel && !empty($ECA1GradingDataAry)) {
                    $thisContent .= '<br/>';
                    $thisContent .= '<br/>';
                }

                $thisGradingData = $ECA2GradingDataAry[0];
                $thisContent .= $thisGradingData['ClubTitleEN'];
                if(trim($thisGradingData['RoleTitle']) != '') {
                    $thisContent .= ' ('.trim($thisGradingData['RoleTitle']).')';
                }
                $thisContent .= '<br/>';

                for($i=0; $i<2; $i++)
                {
                    if($i > 0) {
                        $thisContent .= '<br/>';
                    }

                    $thisContent .= $eReportCard['Template']['TermEn'][$i];
                    if(trim($thisGradingData['TermBasedPer'.($i + 1)]) != '') {
                        $thisContent .= ' - '.trim($thisGradingData['TermBasedPer'.($i + 1)]);
                    }

                    if($SemNum == 1) {
                        break;
                    }
                }

                if($SemNum != 1 && trim($thisGradingData['CommentStudent']) != '') {
                    $thisContent .= '<br/>';
                    $thisContent .= nl2br(trim($thisGradingData['CommentStudent']));
                }

                if(!$isPrimaryLevel)
                {
                	$thisContent .= '</td>';
                	if($tdIndex == 1) {
                		$thisContent .= "</tr>";
                        $thisContent .= "<tr height='0'>";
                	}
                	$thisContent .= '<td width="50%" style="'.$valign.'">';
                	$tdIndex++;
                }
            }

            if($SemNum != 1) {
            	if($isPrimaryLevel) {
	                $thisContent .= '<br/>';
	                $thisContent .= '<br/>';
            	} else {
            		// 2020-07-16 (Philips) [2020-0717-1514-48066] - Not to display Voluntary Hours on Primary Level
                	$thisContent .= $eReportCard['Template']['VoluntaryHoursEn'].': '.$OtherInfoDataAry[$StudentID][0]['Hours of Voluntary Service'];
            	}
            }

            if(!$isPrimaryLevel) {
            	$thisContent .= '</td></tr></tbody></table>';
            }
        }

        $OtherInfoDataAry = $OtherInfoDataAry[$StudentID][$targetTermID]['Activities'];
        if(!empty($OtherInfoDataAry))
        {
            $thisContent = '';
            foreach ((array)$OtherInfoDataAry as $_eca) {
                $thisContent .= "&nbsp;".$_eca."</br>";
            }
        }

        return $thisContent;
    }

	########### END Template Related
	
	########### Start Useful Function
	
	function getAllTermReport($ReportID)
	{
	    # Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM 
				WHERE AcademicYearID = '".$this->schoolYearID."'
				ORDER BY TermStart asc";
		$YearTermIDArr = $this->returnResultSet($sql);
		$YearTermIDArr = Get_Array_By_Key($YearTermIDArr, 'YearTermID');
		
		$db_table = $this->DBName.".RC_REPORT_TEMPLATE";
		$sql = "SELECT
					ReportID, Semester
				FROM
					$db_table
				WHERE
				    Semester IN (".implode(',', $YearTermIDArr).") AND	
					ClassLevelID = '$ClassLevelID' AND 
					isMainReport = 1
				GROUP BY
					Semester ";
		$ReportID_Arr = $this->returnResultSet($sql);
		
		foreach ((array)$YearTermIDArr as $Order => $YearTermID) {
			$data[$Order]['YearTermID'] = $YearTermID;
			$data[$Order]['Order'] = $Order;
			foreach ((array)$ReportID_Arr as $_ReportID_Arr) {
				if($YearTermID == $_ReportID_Arr['Semester']) {
					$data[$Order]['ReportID'] = $_ReportID_Arr['ReportID'];
					break;
				}
			}
		}
		
		return $data;
	}
	
	function getMSTableUnit($ReportID,$ClassLevelID) {
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
	    
		$ReportWeightInfo_raw = $this->returnReportTemplateSubjectWeightData($ReportID);
		foreach ((array)$ReportWeightInfo_raw as $_ReportWeightInfo_raw) {
			$ReportWeightInfo[$_ReportWeightInfo_raw['SubjectID']] = $_ReportWeightInfo_raw['Weight'];
		}
		return $ReportWeightInfo;
	}
	
	// Customized Logic for Promotion Status Generation
	function getPromotionStatusCustomized($ReportID, $StudentID, $OtherInfoAry=array(), $LastestTermID=0){
		global $PATH_WRT_ROOT, $eRCTemplateSetting;
		
//		// Student List
// 		$thisStudentList = array($StudentID);
		
		// Get Class Level Info
		$ReportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportBasicInfo['ClassLevelID'];
		$SchoolType	= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$isPrimaryLevel = $SchoolType == "P";
		$isSecondaryLevel = $SchoolType == "S";
		
		// Get Main Subject
		$MainSubjectAry = $this->MainSubjectList;
		
		// Get Subject Code Mapping
		$SubjectCodeMapping = $this->GET_SUBJECTS_CODEID_MAP(0, 0);
		
		// Get Subject Unit
		$SubjectUnitAry = $this->getMSTableUnit($ReportID, $ClassLevelID);
		
		// Get Subject Marks
		$Marks = $this->getMarks($ReportID, $StudentID, $cons=" AND a.IsOverall = '1' ", $ParentSubjectOnly=1, $includeAdjustedMarks=1, $OrderBy="", $SubjectID="", $ReportColumnID=0);
		
		// Get Subject Count
		$SubjectIDArr = array_keys((array)$Marks);
		$studentHasSubject = count($SubjectIDArr) > 0;
		
		// Get Grand Average
		$GrandResult = $this->getReportResultScore($ReportID, $ReportColumnID=0, $StudentID);
		$GrandAverage = $GrandResult["GrandAverage"];
		
		// Get Conduct
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID, "", "otherconduct");
		$OverallConduct = $OtherInfoDataAry[$StudentID][0]["Conduct"];
		
// 		// Get Major Misdeeds Count
// 		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID, "", "merit_demerit");
// 		$TotalMajorMisdeed = $OtherInfoDataAry[$StudentID][0]["Major Misdeeds"];
		
		// *Promotion (Rule 2 - Conduct : C- or upper)
		$passConductReq = strpos($OverallConduct, "A") !== false || strpos($OverallConduct, "B") !== false || strpos($OverallConduct, "C") !== false;
		
		// *Retention (Rule 1 - Grand Mark : failed)
		$averagePassingMark = $this->Get_Subject_Passing_Mark(-1, $ReportID);
		$isFailedAverage = $GrandAverage >= 0 && $GrandAverage < $averagePassingMark;
		$needToRetention = $isFailedAverage;
		
// 		// *Dropout (Rule 2 - Major Misdeeds: 3 or more)
// 		$failDisicplineReq = $TotalMajorMisdeed >= 3;
// 		$needToDropout = $failDisicplineReq;
		
		// Empty Promotion Status (Special Case / No Ordering)
		$isEmptyPromotionStatus = $GrandAverage == -1 || $GrandResult["OrderMeritForm"] == -1;
		if($isEmptyPromotionStatus) {
		    return 0;
		}
		
		// Check Promotion or Retention
		if($studentHasSubject && !$needToRetention && !$isEmptyPromotionStatus)
		{
		    // Initial
		    $totalUnit = 0;
			$failUnit = 0;
			$isAnySubjectFailed = false;
// 			$failMainSubject = 0;
			
			/* 
			// loop Subject Marks
			foreach((array)$Marks as $thisSubjectID => $thisSubjectScore)
			{
//				if(empty($SubjectMark)){
//					continue;
//				}
				
				// Get Subject Code
				$thisSubjectCode = $SubjectCodeMapping[$thisSubjectID];
				
				// Get Subject Unit
				$thisSubjectUnit = $SubjectUnitAry[$thisSubjectID];
				
				// Exclude Subject Components
				if($this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID)) {
					continue;
				}
				
				// Check Main Subject
				$isMainSubject = in_array($thisSubjectCode, $MainSubjectAry);
				
				// Get Subject Overall Mark
				$SubjectScore = $thisSubjectScore[0];
				$SubjectMark = $SubjectScore['Mark'];
				$SubjectGrade = $SubjectScore['Grade'];
				
				// Skip Special Case
				// if($this->Check_If_Grade_Is_SpecialCase($SubjectGrade) && $SubjectGrade != "+") {
				if($this->Check_If_Grade_Is_SpecialCase($SubjectGrade)) {
					continue;
				}
				
				// Get Score Display Settings
				$subjectDisplayType = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisSubjectID, 1, 0, $ReportID);
				$ScaleDisplay = $subjectDisplayType['ScaleDisplay'];
				
				// Get Mark Nature
				$SubjectDisplayMark = $ScaleDisplay == "M"? $SubjectMark : $SubjectGrade;
				$SubjectMarkNature = $this->returnMarkNature($ClassLevelID, $thisSubjectID, $SubjectDisplayMark, $ReportColumnID=0, $ConvertBy="", $ReportID);
				
// 				// Special Case "+" : Absent (Zero mark)
// 				if($SubjectGrade == "+") {
// 					$SubjectMarkNature = "Fail";
// 				}
				
				// Fail Subject
				if($SubjectMarkNature == "Fail")
				{
				    $failUnit += $thisSubjectUnit;
				    $failSubjectCount++;
//					
// 					// Main Subject
// 					if($isMainSubject) {
// 						if(($isPrimaryLevel && $SubjectMark < 55) || ($isSecondaryLevel && $SubjectMark < 50)) {
// 							$failMainSubject++;
// 						}
// 					}
				}
				
				$totalUnit += $thisSubjectUnit;
			}
			 */
			
			$studentUnitInfo = $this->returnTemplateSubjectCol($ReportID, $ClassLevelID, $StudentID);
			$failUnit = $studentUnitInfo['FailUnitArr']['Final'];
			$totalUnit = $studentUnitInfo['TotalUnitArr']['Final'];
			$isAnySubjectFailed = $failUnit > 0;
			
			if($totalUnit > 0) {
			    $failedUnitRatio = $failUnit / $totalUnit;
			}
			
			/*
			 * Promotion Rule
			 * 1. Passed all subjects        (!$isAnySubjectFailed)
			 * 2. Conduct C- or above		 ($passConductReq)
			 *       OR
			 * 3. Fail Unit Ratio <= 30%     ($failedUnitRatio <= 0.3)
			 */
			$directPromotion = (!$isAnySubjectFailed && $passConductReq) || (isset($failedUnitRatio) && $failedUnitRatio <= 0.3);
			
			/*
			 * Repeat Rule
			 * 1. Final Average less than 60										($isFailedAverage)
			 * 2. Final Average mark for English / Chinese / Mathematics below 55	($failMainSubject > 0)
			 * 3. Failed in 3 / 4 / 5 units or more									($failUnit >= 3 / 4 / 5)
			 */
			/* 
			if ($isPrimaryLevel) {
				$needToRetention = $isFailedAverage || $failMainSubject > 0 || $failUnit >= 3;
			} else if ($isSecondaryLevel && $FormNumber==3) {
				$needToRetention = $isFailedAverage || $failMainSubject > 0 || $failUnit >= 5;
			} else if ($isSecondaryLevel) {
				$needToRetention = $isFailedAverage || $failMainSubject > 0 || $failUnit >= 4;
			}
			*/
			
			/*
			 * Repeat Rule
			 * 1. Final Average failed		 ($isFailedAverage)
			 * 2. Fail Unit Ratio > 30%      ($failedUnitRatio > 0.3)
			 */
			$needToRetention = $isFailedAverage || (isset($failedUnitRatio) && $failedUnitRatio > 0.3);
		}
		
		// Get Preset Status
		$promotionStatusAry = $this->customizedPromotionStatus;
		
		// Get Promotion Status
// 		if($needToDropout) {
// 			$PromotionStatus = $promotionStatusAry["Dropout"];
// 		} else if($needToRetention) {
// 			$PromotionStatus = $promotionStatusAry["Retained"];
// 		} else if($directPromotion) {
// 			$PromotionStatus = $promotionStatusAry["Promoted"];
// 		}
		$PromotionStatus = 0;
        if($directPromotion) {
            if($isSecondaryLevel && $FormNumber == 6) {
                $PromotionStatus = $promotionStatusAry["Graduated"];
            } else {
                $PromotionStatus = $promotionStatusAry["Promoted"];
            }
		}
		else if($needToRetention) {
			$PromotionStatus = $promotionStatusAry["Retained"];
		}
		
		// Check Dropout
// 		if(!$needToDropout && $needToRetention) {
// 			$needToDropout = $this->IS_STUDENT_NEED_TO_DROPOUT($StudentID, $SchoolType);
// 			$PromotionStatus = $needToDropout? $promotionStatusAry["Dropout"] : $PromotionStatus;
// 		}
		
		return $PromotionStatus;
	}
	
	function IS_STUDENT_NEED_TO_DROPOUT($StudentID, $SchoolType)
	{
		// Initial
		$RetainCount = 1;
		$isDropout = false;
		$promotionStatusAry = $this->customizedPromotionStatus;
		
		// loop Previous Years
		$PreviousYearIDList = $this->Get_Previous_YearID_By_Active_Year(true);
		$YearCount = count((array)$PreviousYearIDList);
		for($i=0; $i<$YearCount; $i++)
		{
			// Initiate libreportcard Object
			$PreviousYearID = $PreviousYearIDList[$i];
			$lreportcardObj = new libreportcard($PreviousYearID);
			
			// Get Previous ReportID
			$table = $lreportcardObj->DBName.".RC_REPORT_RESULT";
			$table_report = $lreportcardObj->DBName.".RC_REPORT_TEMPLATE";
			$sql = "SELECT
						report_result.ReportID
					FROM
						$table report_result
						INNER JOIN $table_report report_template ON (report_result.ReportID = report_template.ReportID)
					WHERE
						report_result.StudentID = '$StudentID' AND report_result.ReportColumnID = '0' AND report_result.Semester = 'F' AND report_template.Semester = 'F'";
			$PreviousReportID = $lreportcardObj->returnVector($sql);
			$PreviousReportID = $PreviousReportID[0];
			
			if($PreviousReportID)
			{
				// Get Student Promotion Status
				$promotionStatus = $lreportcardObj->GetPromotionStatusList("", "", $StudentID, $PreviousReportID);
				$promotionStatus = $promotionStatus[$StudentID]["Promotion"];
				if($promotionStatus == $promotionStatusAry['Retained'])
				{
					// Get School Type
					$ReportBasicInfo = $lreportcardObj->returnReportTemplateBasicInfo($PreviousReportID);
					$ClassLevelID = $ReportBasicInfo['ClassLevelID'];
					$thisSchoolType	= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
					$isThisSameSchoolType = $SchoolType==$thisSchoolType;
					
					// Dropout (Rule 1: Retained more than 2 times)
					if($isThisSameSchoolType) {
						$RetainCount++;
						if($RetainCount > 2) {
							$isDropout = true;
							break;
						}
					}
				}
			}
		}
		
		return $isDropout;
	}
	
	function GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID) {
		$PreloadArrKey = 'GET_SCHOOL_TYPE_BY_CLASSLEVEL';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		
		$objYear = new Year($ClassLevelID);
		$FormWebSAMSCode = $objYear->WEBSAMSCode;
		$SchoolTypeCode = substr($FormWebSAMSCode, 0, 1);
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $SchoolTypeCode);
		return $SchoolTypeCode;
	}

	function getStudentECAGradingInfo($ReportID, $StudentID)
    {
        # Retrieve Display Settings
        $ReportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
        $SemID = $ReportBasicInfo['Semester'];
        $SemNum = $this->Get_Semester_Seq_Number($SemID);
        $isYearReport = $SemID == "F";

        $termBasedPerformanceFieldSql = '';
        $termBasedPerformanceJoinTableSql = '';

        $SemesterList = getSemesters($this->schoolYearID, 0);
        $numOfTerm = count($SemesterList);
        for($i=0; $i<$numOfTerm; $i++)
        {
            $_yearTermId = $SemesterList[$i]['YearTermID'];
            $_semNum = $this->Get_Semester_Seq_Number($_yearTermId);
            if(!$isYearReport && $_semNum > $SemNum) {
                break;
            }

            /*
             *  Build select part of SQL
             *  e.g.
             * 	performance1.Performance as TermBasedPer1,
             *	performance2.Performance as TermBasedPer2,
             *	performance3.Performance as TermBasedPer3
             */
            $termBasedPerformanceFieldSql .= ' , performance'.$_semNum.'.Performance as TermBasedPer'.$_semNum.' ';

            /*
             * Build join table part of the SQL
             * e.g.
             * LEFT OUTER JOIN INTRANET_ENROL_GROUP_PERFORMANCE as performance1 ON (a.EnrolGroupID=performance1.EnrolGroupID AND performance1.YearTermID=1 AND a.UserID=performance1.StudentID)
             * LEFT OUTER JOIN INTRANET_ENROL_GROUP_PERFORMANCE as performance2 ON (a.EnrolGroupID=performance2.EnrolGroupID AND performance2.YearTermID=2 AND a.UserID=performance2.StudentID)
             * LEFT OUTER JOIN INTRANET_ENROL_GROUP_PERFORMANCE as performance3 ON (a.EnrolGroupID=performance3.EnrolGroupID AND performance3.YearTermID=3 AND a.UserID=performance3.StudentID)
             */
            $termBasedPerformanceJoinTableSql .= ' LEFT OUTER JOIN INTRANET_ENROL_GROUP_PERFORMANCE as performance'.$_semNum.' ON (iug.EnrolGroupID = performance'.$_semNum.'.EnrolGroupID AND iug.UserID = performance'.$_semNum.'.StudentID AND performance'.$_semNum.'.YearTermID='.$_yearTermId.') ';
        }

        $sql = "SELECT
                        iug.UserGroupID,
                        iug.GroupID,
                        iug.UserID,
                        iug.RoleID,
                        iug.RecordType,
                        iug.RecordStatus,
                        iug.CommentStudent,
                        iug.Performance,
                        iug.isActiveMember,
                        iug.EnrolGroupID,
                        iug.Achievement,
                        ig.Title as ClubTitleEN,
                        ig.TitleChinese as ClubTitleB5,
                        iegi.GroupCategory,
                        ig.GroupCode,
                        ir.Title as RoleTitle
                        $termBasedPerformanceFieldSql
                FROM
                        INTRANET_USERGROUP as iug
                        INNER JOIN INTRANET_GROUP as ig ON (iug.GroupID = ig.GroupID AND ig.RecordType = 5 AND ig.AcademicYearID = '".$this->schoolYearID."')
                        INNER JOIN INTRANET_ENROL_GROUPINFO as iegi ON (iug.EnrolGroupID = iegi.EnrolGroupID AND (iegi.Semester = 0 OR iegi.Semester IS NULL))
                        LEFT OUTER JOIN INTRANET_ROLE as ir ON (iug.RoleID = ir.RoleID AND ir.RecordType = 5)
                        $termBasedPerformanceJoinTableSql
                WHERE
                        iug.UserID = '$StudentID' AND
                        iegi.GroupCategory IN (1,2,3,4,9)
                Order By 
                        ig.GroupCode";
        $result1 = $this->returnArray($sql);

        $sql = "SELECT
                        iug.UserGroupID,
                        iug.GroupID,
                        iug.UserID,
                        iug.RoleID,
                        iug.RecordType,
                        iug.RecordStatus,
                        iug.Performance,
                        iug.CommentStudent,
                        iug.isActiveMember,
                        iug.EnrolGroupID,
                        iug.Achievement,
                        ig.Title as ClubTitleEN,
                        ig.TitleChinese as ClubTitleB5,
                        iegi.GroupCategory,
                        ig.GroupCode,
                        ir.Title as RoleTitle
                        $termBasedPerformanceFieldSql
                FROM
                        INTRANET_USERGROUP as iug
                        INNER JOIN INTRANET_GROUP as ig ON (iug.GroupID = ig.GroupID AND ig.RecordType = 5 AND ig.AcademicYearID = '".$this->schoolYearID."')
                        INNER JOIN INTRANET_ENROL_GROUPINFO as iegi ON (iug.EnrolGroupID = iegi.EnrolGroupID AND (iegi.Semester = 0 OR iegi.Semester IS NULL))
                        LEFT OUTER JOIN INTRANET_ROLE as ir ON (iug.RoleID = ir.RoleID AND ir.RecordType = 5)
                        $termBasedPerformanceJoinTableSql
                WHERE
                        iug.UserID = '$StudentID' AND
                        iegi.GroupCategory IN (7)
                Order By 
                        ig.GroupCode";
        $result2 = $this->returnArray($sql);

        return array($result1, $result2);
    }
	
	function handleChineseContentDisplay($str, $targetSeperator='') {
	    $chiStrLen = mb_strlen($str, 'UTF-8');
	    if($chiStrLen == 2) {
	        $seperator = $targetSeperator? $targetSeperator : '　　';
	        $str = mb_substr($str, 0, 1, 'UTF-8').$seperator.mb_substr($str, 1, 1, 'UTF-8');
	    } else if($chiStrLen == 3) {
	        $seperator = $targetSeperator? $targetSeperator : '&nbsp;&nbsp;';
	        $str = mb_substr($str, 0, 1, 'UTF-8').$seperator.mb_substr($str, 1, 1, 'UTF-8').$seperator.mb_substr($str, 2, 1, 'UTF-8');
	    } else {
	        // do nothing
	    }
	    return $str;
	}
	
	function Get_English_Number_Word($num) {
	    $numberArr = array();
	    $numberArr[1] = "One";
	    $numberArr[2] = "Two";
	    $numberArr[3] = "Three";
	    $numberArr[4] = "Four";
	    $numberArr[5] = "Five";
	    $numberArr[6] = "Six";
	    $numberArr[7] = "Seven";
	    $numberArr[8] = "Eight";
	    
	    return $numberArr[$num];
	}
	########### END Useful Function
}
?>