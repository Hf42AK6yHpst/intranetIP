<?php
# Editing by

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/chung_hua_ma.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		
		//2012-1024-1057-42156
		//$this->configFilesType = array("summary","attendance", "eca", "award");
		$this->configFilesType = array("summary", "attendance", "award", "merit");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 0;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;

		$this->EmptySymbol = "---";
		
		$this->AnnualConductSchemeArr[80] = 'A';
		$this->AnnualConductSchemeArr[70] = 'B';
		$this->AnnualConductSchemeArr[60] = 'C';
		$this->AnnualConductSchemeArr[0] = 'D';
	}

	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard;

		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
		$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
		//2012-1024-1057-42156
		//$TableTop .= "<tr><td>&nbsp;</td></tr>";
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		//$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";

		/*
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		*/

		$x = "";
		$x .= "<tr><td>";
			$x .= $TableTop;
			/*
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='850px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
			*/
		$x .= "</td></tr>";

		return $x;
	}

	function getReportHeader($ReportID)
	{
		global $eReportCard;
		$TitleTable = "";

		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $ReportTitle);

			# get school badge
			//$SchoolLogo = GET_SCHOOL_BADGE();
			//$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			$SchoolLogo = "/file/reportcard2008/templates/chung_hua_ma.jpg";

			# get school name
			$SchoolNameCh = str_replace(" ", "&nbsp;", $eReportCard['Template']['SchoolInfo']['NameCh']);
			$SchoolNameCh = str_replace("&nbsp;", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $SchoolNameCh);	// add more space
			$SchoolNameEn = str_replace(" ", "&nbsp;", $eReportCard['Template']['SchoolInfo']['NameEn']);
			$SchoolName = $SchoolNameCh."<br /><span class='TitleEn'>".$SchoolNameEn."</span>";
			$SchoolAddress = $eReportCard['Template']['SchoolInfo']['Address'];
			$SchoolTelFax = $eReportCard['Template']['SchoolInfo']['Tel']." ".$eReportCard['Template']['SchoolInfo']['Fax'];

			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			//$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
			$TitleTable .= "<tr><td width='150' align='right'><img height='115' src='$SchoolLogo' vspace='5'></td>";

			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>\n";
					$TitleTable .= "<tr><td nowrap='nowrap' class='report_title_chi' align='center'><b>".$SchoolName."</b></td></tr>\n";
					$TitleTable .= "<tr><td nowrap='nowrap' class='SchoolInfo' align='center'><b>".$SchoolAddress."</b></td></tr>\n";
					//2012-1024-1057-42156
					//$TitleTable .= "<tr><td nowrap='nowrap' class='SchoolInfo' align='center'><b>".$SchoolTelFax."</b></td></tr>\n";
					if(!empty($ReportTitle))
						$TitleTable .= "<tr><td nowrap='nowrap' class='TitleEn' align='center'><b>".$ReportTitle."</b></td></tr>\n";
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			}
			$TitleTable .= "<td width='150' align='center'>&nbsp;</td></tr>";
			$TitleTable .= "</table>";
		}

		return $TitleTable;
	}

	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;

		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];

			# retrieve required variables
			$defaultVal = ($StudentID)? "" : "XXX";
			//2012-1024-1057-42156
			//$data['AcademicYearEn'] = $this->GET_ACTIVE_YEAR();
			$data['AcademicYearCh'] = $this->GET_ACTIVE_YEAR();
			$data['DateOfIssue'] = $ReportSetting['Issued'];
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();

				$data['Name'] = $lu->UserName2Lang('ch', 2);
				$data['NameCh'] = $lu->ChineseName;
				$data['NameEn'] = strtoupper($lu->EnglishName);
				
				//$data['ClassNo'] = $lu->ClassNumber;
				//$data['StudentNo'] = $data['ClassNo'];
				//$data['Class'] = $lu->ClassName;
				//$ClassNameArr = explode("(" , $data['Class']);
				//$data['ClassCh'] = trim($ClassNameArr[0]);
				//$data['ClassEn'] = trim(str_replace(')', '', $ClassNameArr[1]));
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassNameEn = $StudentInfoArr[0]['ClassName'];
				$thisClassNameCh = $StudentInfoArr[0]['ClassNameCh'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				
				$data['ClassEn'] = $thisClassNameEn;
				$data['ClassCh'] = $thisClassNameCh;
				$data['ClassNo'] = $thisClassNumber;
				$data['StudentNo'] = $thisClassNumber;
				
				/*
				if (is_array($SettingStudentInfo))
				{
					if (!in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo) && ($lu->ClassNumber != ""))
						$data['Class'] .= " (".$lu->ClassNumber.")";
				}
				else
				{
					if ($lu->ClassNumber != "")
						$data['Class'] .= " (".$lu->ClassNumber.")";
				}
				*/

				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] = $lu->Gender;
				//$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				//$data['StudentAdmNoEn'] = $data['STRN'];
				
				//2012-1024-1057-42156
				//$data['StudentAdmNoEn'] = str_replace('s', '', $lu->UserLogin);
				$data['StudentAdmNoCh'] = str_replace('s', '', $lu->UserLogin);

				//$ClassTeacherAry = $lclass->returnClassTeacher($lu->ClassName);
				$ClassTeacherAry = $lclass->returnClassTeacher($thisClassNameEn, $this->schoolYearID);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
			}
			
			# hardcode the default items
			$defaultInfoArray = array(	"NameCh", "ClassCh", "StudentAdmNoCh", "AcademicYearCh", 
										"NameEn", "ClassEn", "StudentAdmNoEn", "AcademicYearEn");
						
			$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
			if(!empty($defaultInfoArray))
			{
				$count = 0;
				for($i=0; $i<sizeof($defaultInfoArray); $i++)
				{
					$SettingID = trim($defaultInfoArray[$i]);
					$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
					
					if($count%$StudentInfoTableCol==0) {
						$StudentInfoTable .= "<tr>";
						$dataWidth = '170px';
					}
					else if(($count+1)%$StudentInfoTableCol==0) {
						$dataWidth = '60px';
					}
					else
					{
						$dataWidth = '90px';
					}
					
					$StudentInfoTable .= "<td class='tabletext' valign='top' height='{$LineHeight}'>";
					
					$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								              <tr>
								              	<td width='2'>&nbsp;</td>
								                <td nowrap width='75' class='tabletext' nowrap>".$Title.":</td>
								                <td width='2'>";
					if ($Title != "") $StudentInfoTable .= "&nbsp;";
					$StudentInfoTable .= "</td><td width='$dataWidth' class='tabletext' nowrap>";
					$StudentInfoTable .= $data[$SettingID] ? $data[$SettingID] : $defaultVal ;
					$StudentInfoTable .= "</td></tr></table>\n";
					
					
					$StudentInfoTable .= "</td>";
										
					if(($count+1)%$StudentInfoTableCol==0) {
						$StudentInfoTable .= "</tr>";
					} 
					$count++;
				}
			}
			
			# Display student info selected by the user other than the default items
			// Do not show name and class again
			$defaultInfoArray[] = "Name";
			$defaultInfoArray[] = "Class";
			$defaultInfoArray[] = "StudentAdmNo";
			$defaultInfoArray[] = "AcademicYearEn";
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if(in_array($SettingID, $SettingStudentInfo)===true && (!in_array($SettingID, $defaultInfoArray))===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
						
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
							$dataWidth = '170px';
						}
						else if(($count+1)%$StudentInfoTableCol==0) {
							$dataWidth = '60px';
						}
						else
						{
							$dataWidth = '90px';
						}
					
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
						}
						
						$StudentInfoTable .= "<td class='tabletext' width='33%' valign='top' height='{$LineHeight}' nowrap>";
						
						$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>
									              <tr>
									              	<td width='2'>&nbsp;</td>
									                <td nowrap width='75' class='tabletext' nowrap>".$Title.":</td>
									                <td width='2'>";
						if ($Title != "") $StudentInfoTable .= "&nbsp;";
						$StudentInfoTable .= "</td><td nowrap width='$dataWidth' class='tabletext'>";
						$StudentInfoTable .= $data[$SettingID] ? $data[$SettingID] : $defaultVal ;
						$StudentInfoTable .= "</td></tr></table>\n";
												
						$StudentInfoTable .= "</td>";
											
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						} 
						$count++;
					}
				}				
			}
			$StudentInfoTable .= "</table>";
		}
		return $StudentInfoTable;
	}

	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;

		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];

		# define
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;

		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);

		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID);

		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);

		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];

		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);

		##########################################
		# Start Generate Table
		##########################################
		$maxNumOfSubject = 18;
		
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='report_border'>";
		# ColHeader
		$DetailsTable .= $ColHeader;

		$isFirst = 1;
		$TotalWeight = 0;
		$numOfSubjectDisplayed = 0;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];

			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0)
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;

			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
				continue;

			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				# Subject
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				# Subject Teacher Comment
				if ($AllowSubjectTeacherComment) {
					$css_border_top = $isSub?"":"border_top";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
					}
					$DetailsTable .= "</span></td>";
				}

				$DetailsTable .= "</tr>";
				$isFirst = 0;
				$TotalWeight += $MSTableReturned['SubjectWeight'][$thisSubjectID];
				
				$numOfSubjectDisplayed++;
			}
		}

		# empty row
		$numOfEmptyRow = $maxNumOfSubject - $numOfSubjectDisplayed;
		for ($i=0; $i<$numOfEmptyRow; $i++)
		{
			$DetailsTable .= '<tr style="height:21px;">';
				$DetailsTable .= '<td colspan="2" class="tabletext">&nbsp;</td>';
				for ($j=0; $j<5; $j++)
					$DetailsTable .= '<td class="border_left tabletext">&nbsp;</td>';
			$DetailsTable .= '</tr>';
		}
		
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr, $TotalWeight);

		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################

		return $DetailsTable;
	}

	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;

		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];

		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;

		$n = 0;
		$e = 0;	# column# within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		#########################################################
		############## Marks START
		$row2 = "";
		/*
		if($ReportType=="T")	# Terms Report Type
		{
			# Retrieve Invloved Assesment
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if (sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);

			$e = 0;
			for($i=0;$i<sizeof($ColumnTitle);$i++)
			{
				//$ColumnTitleDisplay = convert2unicode($ColumnTitle[$i], 1, 2);
				$ColumnTitleDisplay = $ColumnTitle[$i];
				$row1 .= "<td valign='middle' height='{$LineHeight}' class='border_left tabletext' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'><b>". $ColumnTitleDisplay . "</b></td>";
				$n++;
 				$e++;
			}
		}
		else					# Whole Year Report Type
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
 			$needRowspan=0;
			for($i=0;$i<sizeof($ColumnData);$i++)
			{
				$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
				$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
				if($isDetails==1)
				{
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColumnID = array();
					$ColumnTitle = array();
					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
					for($j=0;$j<sizeof($ColumnTitle);$j++)
					{
						$ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$j], 1, 2));
						$row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $ColumnTitleDisplay . "</td>";
						$n++;
						$e++;
					}
					$colspan = "colspan='". sizeof($ColumnTitle) ."'";
					$Rowspan = "";
					$needRowspan++;
					$t[$i] = sizeof($ColumnTitle);
				}
				else
				{
					$colspan = "";
					$Rowspan = "rowspan='2'";
					$e++;
				}
				$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left small_title' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $SemName ."</td>";
			}
		}
		
		############## Marks END
		#########################################################
		$Rowspan = $row2 ? "rowspan='2'" : "";

		if(!$needRowspan)
			$row1 = str_replace("rowspan='2'", "", $row1);

		$x = "<tr>";
		# Subject
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		for($i=0;$i<sizeof($SubjectColAry);$i++)
		{
			$SubjectEng = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'];
			$SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];
			$SubjectTitle = $SubjectColAry[$i];
			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
			$x .= "<td {$Rowspan}  valign='middle' class='small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>". $SubjectTitle . "</td>";
			$n++;
		}

		# Marks
		$x .= $row1;

		# Subject Overall
		if($ShowRightestColumn)
		{
			$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Overall'] ."' class='border_left small_title' align='center'>". $eReportCard['Template']['SubjectOverall'] ."</td>";
			$n++;
		}
		else
		{
			//$e--;
		}

		# Subject Teacher Comment
		if ($AllowSubjectTeacherComment) {
			$x .= "<td {$Rowspan} valign='middle' class='border_left small_title' align='center' width='10%'>".$eReportCard['TeachersComment']."</td>";
		}

		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		*/
		
		$SubjectTitle = $eReportCard['Template']['SubjectChn']." ".$eReportCard['Template']['SubjectEng'];
		
		$SemesterDisplay = $eReportCard['Template']['SemesterCh'].' '.$eReportCard['Template']['SemesterEn'];
		$SemesterWidth = str_replace('%', '', $eRCTemplateSetting['ColumnWidth']['Mark']);
		$SemesterWidth = $SemesterWidth * 2;
		$SemesterWidth = $SemesterWidth."%";
		
		$AverageDisplay = $eReportCard['Template']['AverageCh'].'<br />'.$eReportCard['Template']['AverageEn'];
		$PeriodDisplay = $eReportCard['Template']['PeriodCh'].'<br />'.$eReportCard['Template']['PeriodEn'];
		$TotalDisplay = $eReportCard['Template']['TotalCh'].'<br />'.$eReportCard['Template']['TotalEn'];
		
		$x = '';
		$x = '<tr>';
			$x .= "<td rowspan='2' colspan='2' valign='middle' align='center' class='tabletext' height='{$LineHeight}' width='45%'><br />".$SubjectTitle."</td>";
			$x .= "<td colspan='2' valign='middle' align='center' class='border_left border_bottom tabletext' height='{$LineHeight}' >".$SemesterDisplay."</td>";
			$x .= "<td rowspan='2' valign='middle' align='center' class='border_left tabletext' height='{$LineHeight}' width='15%' >".$AverageDisplay."</td>";
			$x .= "<td rowspan='2' valign='middle' align='center' class='border_left tabletext' height='{$LineHeight}' width='10%' >".$PeriodDisplay."</td>";
			$x .= "<td rowspan='2' valign='middle' align='center' class='border_left tabletext' height='{$LineHeight}' width='12%' >".$TotalDisplay."</td>";
		$x .= '</tr>';
		
		$x .= '<tr>';
			$x .= "<td valign='middle' align='center' class='border_left border_top tabletext' height='{$LineHeight}' width='9%' >1</td>";
			$x .= "<td valign='middle' align='center' class='border_left border_top tabletext' height='{$LineHeight}' width='9%' >2</td>";
		$x .= '</tr>';
		
		return array($x, $n, $e, $t);
	}

	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];

		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$SubjectIDCodeMappingArr = $this->GET_SUBJECTS_CODEID_MAP($withComponent=1);

 		$x = array();
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Separator = "&nbsp;&nbsp;&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
			 		}

			 		$css_border_top = ($isFirst)? "border_top" : "";
			 		//$css_border_top = ($Prefix)? "" : "border_top";
			 		//foreach($SubjectDisplay as $k=>$v)
			 		//{
				 		$SubjectCode = $SubjectIDCodeMappingArr[$SubSubjectID]['CODEID'];
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");

		 				$thisDisplay = $SubjectChn.$Separator.$SubjectEng;

			 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle' colspan='2'>";
							$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
								$t .= "<tr>
											<td height='{$LineHeight}' width='45px' class='tabletext'>&nbsp;&nbsp;{$SubjectCode}</td>
											<td height='{$LineHeight}' class='tabletext'>$thisDisplay</td>";
								$t .= "</tr>";
							$t .= "</table>";
						$t .= "</td>";
			 		//}
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}

 		return $x;
	}

	function getSignatureTable($ReportID='')
	{
		/*
 		global $eReportCard, $eRCTemplateSetting;

 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
		}

		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			$IssueDate = ($SettingID == "IssueDate" && $Issued)	? "<u>".$Issued."</u>" : "__________";
			$SignatureTable .= "<td valign='bottom' align='center'>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td align='center' class='small_title' height='130' valign='bottom'>_____". $IssueDate ."_____</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		*/
		return $SignatureTable;
	}

//2012-1024-1057-42156
//	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array(), $TotalSubjectWeight=0)
//	{
//		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
//		$prefix = "&nbsp;&nbsp;";
//
//		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
//		$LineHeight 				= $ReportSetting['LineHeight'];
//		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
//		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
//		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
//		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
//		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
//		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
//		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
//		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
//		$SemID 						= $ReportSetting['Semester'];
// 		$ReportType 				= $SemID == "F" ? "W" : "T";
//		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
//		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
//		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
//
//		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
//
//		# retrieve Student Class
//		// $targetTerm = $this->Get_Last_Semester_Of_Report($ReportID);
//		if ($ReportType=='T') {
//			$targetTerm = $SemID; 
//		}
//		else {
//			// Get the First Term for Consolidated Report
//			$InvolvedTermArr = $this->returnReportInvolvedSem($ReportID);
//			foreach((array)$InvolvedTermArr as $TermID => $TermName) {
//				$targetTerm = $TermID;
//				break;
//			}
//			
//			$ConsolidatedTermIDArr = array_keys($InvolvedTermArr);
//			$secondTermID = $ConsolidatedTermIDArr[count($ConsolidatedTermIDArr)-1];
//		}
//		
//		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
//		$ary = $OtherInfoDataAry[$StudentID];
//		
//		
//		$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
//		$ClassID = $StudentInfoArr[0]['ClassID'];
//		
//		
//		### Conduct Calculation
//		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
//		$objAcademicYear = new academic_year($this->schoolYearID);
//		$YearTermInfoArr = $objAcademicYear->Get_Term_List($returnAsso=0);
//		$YearTermIDArr = Get_Array_By_Key($YearTermInfoArr, 'YearTermID');
//		
//		# retrieve result data
//		$result = $this->getReportResultScore($ReportID, 0, $StudentID);
//		$GrandTotal = $StudentID ? my_round($result['GrandTotal'], 2) : "S";
//		$AverageMark = $StudentID ? my_round($result['GrandAverage'], 2) : "S";
//		
//		$FormPosition = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? $this->EmptySymbol: $result['OrderMeritForm']) : $this->EmptySymbol) : "#";
//		$ClassPosition = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? $this->EmptySymbol: $result['OrderMeritClass']) : $this->EmptySymbol) : "#";
//		$ClassNumOfStudent = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? $this->EmptySymbol: $result['ClassNoOfStudent']) : $this->EmptySymbol) : "#";
//		$FormNumOfStudent = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? $this->EmptySymbol: $result['FormNoOfStudent']) : $this->EmptySymbol) : "#";
//		
//		if ($StudentID && ($OverallPositionRangeClass!=0) && ($ClassPosition > $OverallPositionRangeClass))
//			$ClassPosition = $this->EmptySymbol;
//		if ($StudentID && ($OverallPositionRangeForm!=0) && ($FormPosition > $OverallPositionRangeForm))
//			$FormPosition = $this->EmptySymbol;
//		
//		$x = '';
//		$x .= "<tr>\n";
//			# Actual Attendance
//			$thisTitle = $prefix.$eReportCard['Template']['ActualAttendanceCh']."<br />".$prefix.$eReportCard['Template']['ActualAttendanceEn'];
//			$this_1stTerm_Value = $this->Get_CSV_Value($ary[$targetTerm]['Actual_Attendance']);
//			//$this_2ndTerm_Value = ($ReportType=='T')? '&nbsp;' : $this->Get_CSV_Value($ary[0]['Actual_Attendance']);
//			$this_2ndTerm_Value = ($ReportType=='T')? '&nbsp;' : $this->Get_CSV_Value($ary[$secondTermID]['Actual_Attendance']);
//			$x .= "<td colspan='2' class='tabletext border_top'>".$thisTitle."</td>";
//			$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$this_1stTerm_Value."</td>";
//			$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$this_2ndTerm_Value."</td>";
//			
//			# Grand Total
//			$thisTitle = $prefix.$eReportCard['Template']['OverallResultCh']."<br />".$prefix.$eReportCard['Template']['OverallResultEn'];
//			$x .= "<td class='tabletext border_top border_left'>".$thisTitle."</td>";
//			$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$TotalSubjectWeight."</td>";
//			$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$GrandTotal."</td>";
//		$x .= "</tr>\n";
//		
//		$x .= "<tr>\n";
//			# Total Attendance
//			$thisTitle = $prefix.$eReportCard['Template']['TotalAttendanceCh']."<br />".$prefix.$eReportCard['Template']['TotalAttendanceEn'];
//			$this_1stTerm_Value = $this->Get_CSV_Value($ary[$targetTerm]['Total_Attendance']);
//			//$this_2ndTerm_Value = ($ReportType=='T')? '&nbsp;' : $this->Get_CSV_Value($ary[0]['Total_Attendance']);
//			$this_2ndTerm_Value = ($ReportType=='T')? '&nbsp;' : $this->Get_CSV_Value($ary[$secondTermID]['Total_Attendance']);
//			$x .= "<td colspan='2' class='tabletext border_top'>".$thisTitle."</td>";
//			$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$this_1stTerm_Value."</td>";
//			$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$this_2ndTerm_Value."</td>";
//			
//			# Grand Average
//			$thisTitle = $prefix.$eReportCard['Template']['AvgMarkCh']."<br />".$prefix.$eReportCard['Template']['AvgMarkEn'];
//			$thisValue = $AverageMark;
//			$x .= "<td colspan='2' class='tabletext border_top border_left'>".$thisTitle."</td>";
//			$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$thisValue."</td>";
//		$x .= "</tr>\n";
//		
//		$x .= "<tr>\n";
//			# Medical Leave / Official Leave
//			$thisTitleCh = $eReportCard['Template']['MedicalLeaveCh']." / ".$eReportCard['Template']['OfficialLeaveCh'];
//			$thisTitleEn = $eReportCard['Template']['MedicalLeaveEn']." / ".$eReportCard['Template']['OfficialLeaveEn'];
//			$thisTitle = $prefix.$thisTitleCh."<br />".$prefix.$thisTitleEn;
//			$this_1stTerm_Value = $this->Get_CSV_Value($ary[$targetTerm]['Medical/Offical_Leave']);
//			//$this_2ndTerm_Value = ($ReportType=='T')? '&nbsp;' : $this->Get_CSV_Value($ary[0]['Medical/Offical_Leave']);
//			$this_2ndTerm_Value = ($ReportType=='T')? '&nbsp;' : $this->Get_CSV_Value($ary[$secondTermID]['Medical/Offical_Leave']);
//			$x .= "<td colspan='2' class='tabletext border_top'>".$thisTitle."</td>";
//			$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$this_1stTerm_Value."</td>";
//			$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$this_2ndTerm_Value."</td>";
//			
//			# Position in class
//			$thisTitle = $prefix.$eReportCard['Template']['ClassPositionCh']."<br />".$prefix.$eReportCard['Template']['ClassPositionEn'];
//			$thisValue = $ClassPosition;
//			$x .= "<td colspan='2' class='tabletext border_top border_left'>".$thisTitle."</td>";
//			$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$thisValue."</td>";
//		$x .= "</tr>\n";
//		
//		$x .= "<tr>\n";
//			# Personal Leave
//			$thisTitle = $prefix.$eReportCard['Template']['PersonalLeaveCh']."<br />".$prefix.$eReportCard['Template']['PersonalLeaveEn'];
//			$this_1stTerm_Value = $this->Get_CSV_Value($ary[$targetTerm]['Personal_Leave']);
//			//$this_2ndTerm_Value = ($ReportType=='T')? '&nbsp;' : $this->Get_CSV_Value($ary[0]['Personal_Leave']);
//			$this_2ndTerm_Value = ($ReportType=='T')? '&nbsp;' : $this->Get_CSV_Value($ary[$secondTermID]['Personal_Leave']);
//			$x .= "<td colspan='2' class='tabletext border_top'>".$thisTitle."</td>";
//			$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$this_1stTerm_Value."</td>";
//			$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$this_2ndTerm_Value."</td>";
//			
//			# Class Enrolment
//			$thisTitle = $prefix.$eReportCard['Template']['ClassNumOfStudentCh']."<br />".$prefix.$eReportCard['Template']['ClassNumOfStudentEn'];
//			$thisValue = $ClassNumOfStudent;
//			$x .= "<td colspan='2' class='tabletext border_top border_left'>".$thisTitle."</td>";
//			$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$thisValue."</td>";
//		$x .= "</tr>\n";
//		
//		$x .= "<tr>\n";
//			# Leave without permission
//			$thisTitle = $prefix.$eReportCard['Template']['LeaveWithoutPermissionCh']."<br />".$prefix.$eReportCard['Template']['LeaveWithoutPermissionEn'];
//			$this_1stTerm_Value = $this->Get_CSV_Value($ary[$targetTerm]['Leave_Without_Permission']);
//			//$this_2ndTerm_Value = ($ReportType=='T')? '&nbsp;' : $this->Get_CSV_Value($ary[0]['Leave_Without_Permission']);
//			$this_2ndTerm_Value = ($ReportType=='T')? '&nbsp;' : $this->Get_CSV_Value($ary[$secondTermID]['Leave_Without_Permission']);
//			$x .= "<td colspan='2' class='tabletext border_top'>".$thisTitle."</td>";
//			$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$this_1stTerm_Value."</td>";
//			$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$this_2ndTerm_Value."</td>";
//			
//			# Average Conduct
//			$thisTitle = $prefix.$eReportCard['Template']['AverageConductCh']."<br />".$prefix.$eReportCard['Template']['AverageConductEn'];
//			$thisValue = $ary[0]['Conduct'].' '.$ary[0]['ConductGrade'];
//			$thisValue = ($ReportType=='T' || trim($thisValue)=='')? '&nbsp;' : $thisValue;
//			$x .= "<td rowspan='2' colspan='2' class='tabletext border_top border_left'>".$thisTitle."</td>";
//			$x .= "<td rowspan='2' class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$thisValue."</td>";
//		$x .= "</tr>\n";
//		
//		# Conduct Marks
//		$x .= "<tr>\n";
//			$thisTitle = $prefix.$eReportCard['Template']['ConductCh']."<br />".$prefix.$eReportCard['Template']['ConductEn'];
//			$x .= "<td colspan='2' class='tabletext border_top'>".$thisTitle."</td>";
//			for ($i=0; $i<2; $i++)
//			{
//				$thisYearTermID = $YearTermIDArr[$i];
//				$thisYearTermConduct = ($ary[$thisYearTermID]['Conduct']=='')? '&nbsp;' : $ary[$thisYearTermID]['Conduct'];
//				$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$thisYearTermConduct."</td>";
//			}
//		$x .= "</tr>\n";
//		
//		# Class Teacher Comment
//		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
//		$ClassTeacherComment = $CommentAry[0];
//		$ClassTeacherComment = explode("\n", $ClassTeacherComment);
//		$x .= $this->Get_CSV_Row($eReportCard['Template']['ClassTeacherCommentEn'], $eReportCard['Template']['ClassTeacherCommentCh'], $ClassTeacherComment);
//		
//		# ECA / Services and Duties
//		$thisTitleEn = $eReportCard['Template']['ECAEn'].' / '.$eReportCard['Template']['Services&DutiesEn'];
//		$thisTitleCh = $eReportCard['Template']['ECACh'].' / '.$eReportCard['Template']['Services&DutiesCh'];
//		$thisValue = ($ReportType=='W')? $ary[0]['ECA_Service_Duties'] : $ary[$targetTerm]['ECA_Service_Duties'];
//		$x .= $this->Get_CSV_Row($thisTitleEn, $thisTitleCh, $thisValue);
//		
//		# Award / Punishment
//		$thisTitleEn = $eReportCard['Template']['AwardEn'].' / '.$eReportCard['Template']['PunishmentEn'];
//		$thisTitleCh = $eReportCard['Template']['AwardCh'].' / '.$eReportCard['Template']['PunishmentCh'];
//		$thisValue = ($ReportType=='W')? $ary[0]['Award/Punishment'] : $ary[$targetTerm]['Award/Punishment'];
//		$x .= $this->Get_CSV_Row($thisTitleEn, $thisTitleCh, $thisValue, 'Award');
//		
//		# Signature Row
//		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
//		$x .= "<tr>\n";
//			$x .= "<td colspan='7' class='border_top' style='padding:0px' >\n";
//				$x .= "<table width='100%' height='90px' border='0' cellpadding='0' cellspacing='0' align='center'>";
//				$x .= "<tr>";
//				
//				$width = floor(100/sizeof($SignatureTitleArray));
//				for($k=0; $k<sizeof($SignatureTitleArray); $k++)
//				{
//					$SettingID = trim($SignatureTitleArray[$k]);
//					$Title = $eReportCard['Template'][$SettingID];
//					$border_left = ($k==0)? '' : 'border_left';
//					
//					$x .= "<td width='".$width."%' class='tabletext $border_left'>".$Title."</td>";
//				}
//		
//				$x .= "</tr>";
//				$x .= "</table>";
//			$x .= "</td>";
//		$x .= "</tr>\n";
//		
//		# Class to be enrolled next year
//		if ($ReportType=='W')
//		{
//			$thisTitle = $prefix.$eReportCard['Template']['ClassEnrolledNextYearCh'].'<br />'.$prefix.$eReportCard['Template']['ClassEnrolledNextYearEn'];
//			$thisValue = $this->Get_CSV_Value($ary[$targetTerm]['Class_Next_Year']);
//			$x .= "<tr>\n";
//				$x .= "<td colspan='2' class='tabletext border_top'>".$thisTitle."</td>";
//				$x .= "<td colspan='5' class='tabletext border_top'>".$thisValue."</td>";
//			$x .= "</tr>\n";
//		}
//		
//
//		return $x;
//	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array(), $TotalSubjectWeight=0) {
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$prefix = "";
		
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID =  $reportInfoAry['ClassLevelID'];
		$YearTermID =  $reportInfoAry['Semester'];
		$ReportType = ($YearTermID=='F')? 'W' : 'T'; 
		$OverallPositionRangeClass 	= $reportInfoAry['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $reportInfoAry['OverallPositionRangeForm'];
		
		
		//2013-0405-0845-35156
		$TermStartDate 	= (is_date_empty($reportInfoAry['TermStartDate']))? '' : $reportInfoAry['TermStartDate'];
		$TermEndDate 	= (is_date_empty($reportInfoAry['TermEndDate']))? '' : $reportInfoAry['TermEndDate'];
		
		
		$termReportIdAry = array();
		if ($ReportType == 'W') {
			$YearTermID = 0;
			$termReportIdAry = Get_Array_By_Key($this->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ReportColumnID="ALL"), 'ReportID');
		}
		else {
			$termNumber = $this->Get_Semester_Seq_Number($YearTermID);
			if ($termNumber == 1) {
				// Term 1 report => Term 2 column all cells is blank
				$termReportIdAry[] = $ReportID;
				$termReportIdAry[] = '';
			}
			else {
				// Term 2 report => Term 1 column all cells is blank
				$termReportIdAry[] = '';
				$termReportIdAry[] = $ReportID;
			}
		}
		$numOfTermReport = count($termReportIdAry);
		
		
		### Get Grand Result
		$result = $this->getReportResultScore($ReportID, 0, $StudentID);
		$GrandTotal = $StudentID ? my_round($result['GrandTotal'], 2) : "S";
		$GrandAverage = $StudentID ? my_round($result['GrandAverage'], 2) : "S";
		$ActualAverage = $StudentID ? my_round($result['ActualAverage'], 2) : "S";
		$MarksDeducted = $StudentID ? my_round($GrandAverage - $ActualAverage, 2) : "S";
		
		$FormPosition = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? $this->EmptySymbol: $result['OrderMeritForm']) : $this->EmptySymbol) : "#";
		$ClassPosition = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? $this->EmptySymbol: $result['OrderMeritClass']) : $this->EmptySymbol) : "#";
		$ClassNumOfStudent = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? $this->EmptySymbol: $result['ClassNoOfStudent']) : $this->EmptySymbol) : "#";
		$FormNumOfStudent = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? $this->EmptySymbol: $result['FormNoOfStudent']) : $this->EmptySymbol) : "#";
		if ($StudentID && ($OverallPositionRangeClass!=0) && ($ClassPosition > $OverallPositionRangeClass)) {
			$ClassPosition = $this->EmptySymbol;
		}
		if ($StudentID && ($OverallPositionRangeForm!=0) && ($FormPosition > $OverallPositionRangeForm)) {
			$FormPosition = $this->EmptySymbol;
		}
		
		$grandInfoAry = array();
		for ($i=0; $i<$numOfTermReport; $i++) {
			$_termReportId = $termReportIdAry[$i];
			
			if ($_termReportId != '') {
				$_resultAry = $this->getReportResultScore($_termReportId, 0, $StudentID);
				
				$_classPosition = $StudentID ? ($_resultAry['OrderMeritClass'] ? ($_resultAry['OrderMeritClass']==-1? $this->EmptySymbol: $_resultAry['OrderMeritClass']) : $this->EmptySymbol) : "#";
				$_classNumOfStudent = $StudentID ? ($_resultAry['ClassNoOfStudent'] ? ($_resultAry['ClassNoOfStudent']==-1? $this->EmptySymbol: $_resultAry['ClassNoOfStudent']) : $this->EmptySymbol) : "#";
				
				if ($StudentID && ($OverallPositionRangeClass!=0) && ($_classPosition > $OverallPositionRangeClass)) {
					$_classPosition = $this->EmptySymbol;
				}
				
				$grandInfoAry[$_termReportId]['classPosition'] = $_classPosition;
				$grandInfoAry[$_termReportId]['classNumOfStudent'] = $_classNumOfStudent;
			}
		}
		
		
		### Get Other Info data
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
		$ary = $OtherInfoDataAry[$StudentID];
		$merits = ($ary[$YearTermID]['Merits']=='')? '&nbsp;' : $ary[$YearTermID]['Merits'];
		$minorMerits = ($ary[$YearTermID]['Minor Merits']=='')? '&nbsp;' : $ary[$YearTermID]['Minor Merits'];
		$majorMerits = ($ary[$YearTermID]['Major Merits']=='')? '&nbsp;' : $ary[$YearTermID]['Major Merits'];
		$demerits = ($ary[$YearTermID]['Demerits']=='')? '&nbsp;' : $ary[$YearTermID]['Demerits'];
		$minorDemerits = ($ary[$YearTermID]['Minor Demerits']=='')? '&nbsp;' : $ary[$YearTermID]['Minor Demerits'];
		$majorDemerits = ($ary[$YearTermID]['Major Demerits']=='')? '&nbsp;' : $ary[$YearTermID]['Major Demerits'];
		
		if (!is_array($ary[$YearTermID]['Award'])) {
			$awardAry = array($ary[$YearTermID]['Award']);
		}
		else {
			$awardAry = $ary[$YearTermID]['Award'];
		}
		
		$studentInfoAry = $this->Get_Student_Class_ClassLevel_Info($StudentID);
		$classId = $studentInfoAry[0]['ClassID'];
		
		$attendanceInfoAry = array();
		for ($i=0; $i<$numOfTermReport; $i++) {
			$_termReportId = $termReportIdAry[$i];
			
			if ($_termReportId != '') {
				$_reportInfoAry = $this->returnReportTemplateBasicInfo($_termReportId);
				$_ClassLevelID =  $_reportInfoAry['ClassLevelID'];
				$_YearTermID =  $_reportInfoAry['Semester'];
				$_ReportType = ($_YearTermID=='F')? 'W' : 'T';
				
				//2013-1029-1550-35073
				$_TermStartDate 	= (is_date_empty($_reportInfoAry['TermStartDate']))? '' : $_reportInfoAry['TermStartDate'];
				$_TermEndDate 	= (is_date_empty($_reportInfoAry['TermEndDate']))? '' : $_reportInfoAry['TermEndDate'];
				
				if ($_ReportType == 'W') {
					$_YearTermID = 0;
				}
				
				//2013-0405-0845-35156
				$_officalLeaveInfoAry = $this->Get_Student_Profile_Attendance_Data($_YearTermID, $classId, $_TermStartDate, $_TermEndDate, $ClassName='', $StudentID, '公假');
				$attendanceInfoAry[$_termReportId]['officalLeave'] = $_officalLeaveInfoAry[$StudentID]['Days Absent'];
				$_medicalLeaveInfoAry = $this->Get_Student_Profile_Attendance_Data($_YearTermID, $classId, $_TermStartDate, $_TermEndDate, $ClassName='', $StudentID, '病假');
				$attendanceInfoAry[$_termReportId]['medicalLeave'] = $_medicalLeaveInfoAry[$StudentID]['Days Absent'];
				$_personalLeaveInfoAry = $this->Get_Student_Profile_Attendance_Data($_YearTermID, $classId, $_TermStartDate, $_TermEndDate, $ClassName='', $StudentID, '事假');
				$attendanceInfoAry[$_termReportId]['personalLeave'] = $_personalLeaveInfoAry[$StudentID]['Days Absent'];
				$_truancyInfoAry = $this->Get_Student_Profile_Attendance_Data($_YearTermID, $classId, $_TermStartDate, $_TermEndDate, $ClassName='', $StudentID, '旷课');
				$attendanceInfoAry[$_termReportId]['truancy'] = $_truancyInfoAry[$StudentID]['Days Absent'];
				
				$_attendanceInfoAry = $this->Get_Student_Profile_Attendance_Data($_YearTermID, $classId, $_TermStartDate, $_TermEndDate, $ClassName='', $StudentID, $Reason='');
				$attendanceInfoAry[$_termReportId]['timeLate'] = $_attendanceInfoAry[$StudentID]['Time Late'];
				$attendanceInfoAry[$_termReportId]['earlyLeave'] = $_attendanceInfoAry[$StudentID]['Early Leave'];
			}
		}
		
		
		### Get Class Teacher Comment
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$ClassTeacherComment = $CommentAry[0];
		$ClassTeacherComment = nl2br(trim($ClassTeacherComment));
		if ($ClassTeacherComment == '') {
			$ClassTeacherComment = '&nbsp;';
		}
		
		
		### Get eEnrolment data
		$eEnrolTargetTermAry = array();
		if ($ReportType == 'T') {
			$eEnrolTargetTermAry = array(0, $YearTermID);
		}
		else {
			$eEnrolTargetTermAry = array(0, $this->Get_Last_Semester_Of_Report($ReportID));
		}
		$eEnrolData = $this->Get_eEnrolment_Data($StudentID, $eEnrolTargetTermAry);
				
		
		$x = '';
		
		$x .= "<tr>\n";
			# Actual Attendance
			$thisTitle = $prefix.$eReportCard['Template']['ActualAttendanceCh']." ".$prefix.$eReportCard['Template']['ActualAttendanceEn'];
			$x .= "<td colspan='2' class='tabletext border_top'>".$thisTitle."</td>";
			for ($i=0; $i<$numOfTermReport; $i++) {
				$_termReportId = $termReportIdAry[$i];
			
				if ($_termReportId != '') {
					$_reportInfoAry = $this->returnReportTemplateBasicInfo($_termReportId);
					$_yearTermId =  $_reportInfoAry['Semester'];
					$_value = $ary[$_yearTermId]['Actual_Attendance'];
					$_unit = $eReportCard['Template']['DayCh'];
				}
				else {
					$_value = '';
					$_unit = '';
				}
				$_value = ($_value=='')? '&nbsp;' : $_value;
				
				$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>";
					$x .= "<div style='width:55%; text-align:right; float:left;'>".$_value."</div><div style='width:45%; text-align:left; float:left;'>&nbsp;".$_unit."</div>";
				$x .= "</td>";
			}
			
			# Grand Total
			$thisTitle = $prefix.$eReportCard['Template']['OverallResultCh']." ".$prefix.$eReportCard['Template']['OverallResultEn'];
			$x .= "<td class='tabletext border_top border_left'>".$thisTitle."</td>";
			$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$TotalSubjectWeight."</td>";
			$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>".$GrandTotal."</td>";
		$x .= "</tr>\n";
		
		$x .= "<tr>\n";
			# Total Attendance
			$thisTitle = $prefix.$eReportCard['Template']['TotalAttendanceCh']." ".$prefix.$eReportCard['Template']['TotalAttendanceEn'];
			$x .= "<td colspan='2' class='tabletext border_top'>".$thisTitle."</td>";
			for ($i=0; $i<$numOfTermReport; $i++) {
				$_termReportId = $termReportIdAry[$i];
			
				if ($_termReportId != '') {
					$_reportInfoAry = $this->returnReportTemplateBasicInfo($_termReportId);
					$_yearTermId =  $_reportInfoAry['Semester'];
					$_value = $ary[$_yearTermId]['Total_Attendance'];
					$_unit = $eReportCard['Template']['DayCh'];
				}
				else {
					$_value = '';
					$_unit = '';
				}
				$_value = ($_value=='')? '&nbsp;' : $_value;
				
				$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>";
					$x .= "<div style='width:55%; text-align:right; float:left;'>".$_value."</div><div style='width:45%; text-align:left; float:left;'>&nbsp;".$_unit."</div>";
				$x .= "</td>";
			}
			
			# Grand Average
			$thisTitle = $prefix.$eReportCard['Template']['AvgMarkCh']."<br />".$prefix.$eReportCard['Template']['AvgMarkEn'];
			$x .= "<td class='tabletext border_top border_left' colspan='2' rowspan='2'>".$thisTitle."</td>";
			$x .= "<td class='tabletext border_top border_left' rowspan='2' style='text-align:center; vertical-align:middle'>".$GrandAverage."</td>";
		$x .= "</tr>\n";
		
		$x .= "<tr>\n";
			# Offical Leave
			$thisTitle = $prefix.$eReportCard['Template']['OfficialLeaveCh']." ".$prefix.$eReportCard['Template']['OfficialLeaveEn'];
			$x .= "<td colspan='2' class='tabletext border_top'>".$thisTitle."</td>";
			for ($i=0; $i<$numOfTermReport; $i++) {
				$_termReportId = $termReportIdAry[$i];
			
				if ($_termReportId != '') {
					$_value = $attendanceInfoAry[$_termReportId]['officalLeave'];
					$_unit = $eReportCard['Template']['DayCh'];
				}
				else {
					$_value = '';
					$_unit = '';
				}
				$_value = ($_value=='')? '&nbsp;' : $_value;
				
				$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>";
					$x .= "<div style='width:55%; text-align:right; float:left;'>".$_value."</div><div style='width:45%; text-align:left; float:left;'>&nbsp;".$_unit."</div>";
				$x .= "</td>";
			}
		$x .= "</tr>\n";
		
		$x .= "<tr>\n";
			# Medical Leave
			$thisTitle = $prefix.$eReportCard['Template']['MedicalLeaveCh']." ".$prefix.$eReportCard['Template']['MedicalLeaveEn'];
			$x .= "<td colspan='2' class='tabletext border_top'>".$thisTitle."</td>";
			for ($i=0; $i<$numOfTermReport; $i++) {
				$_termReportId = $termReportIdAry[$i];
			
				if ($_termReportId != '') {
					$_value = $attendanceInfoAry[$_termReportId]['medicalLeave'];
					$_unit = $eReportCard['Template']['DayCh'];
				}
				else {
					$_value = '';
					$_unit = '';
				}
				$_value = ($_value=='')? '&nbsp;' : $_value;
				
				$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>";
					$x .= "<div style='width:55%; text-align:right; float:left;'>".$_value."</div><div style='width:45%; text-align:left; float:left;'>&nbsp;".$_unit."</div>";
				$x .= "</td>";
			}
			
			# Marks Deducted
			$thisTitle = $prefix.$eReportCard['Template']['MarksDeductedCh']."<br />".$prefix.$eReportCard['Template']['MarksDeductedEn'];
			$x .= "<td class='tabletext border_top border_left' colspan='2' rowspan='2'>".$thisTitle."</td>";
			$x .= "<td class='tabletext border_top border_left' rowspan='2' style='text-align:center; vertical-align:middle'>".$MarksDeducted."</td>";
		$x .= "</tr>\n";
		
		$x .= "<tr>\n";
			# Personal Leave
			$thisTitle = $prefix.$eReportCard['Template']['PersonalLeaveCh']." ".$prefix.$eReportCard['Template']['PersonalLeaveEn'];
			$x .= "<td colspan='2' class='tabletext border_top'>".$thisTitle."</td>";
			for ($i=0; $i<$numOfTermReport; $i++) {
				$_termReportId = $termReportIdAry[$i];
			
				if ($_termReportId != '') {
					$_value = $attendanceInfoAry[$_termReportId]['personalLeave'];
					$_unit = $eReportCard['Template']['DayCh'];
				}
				else {
					$_value = '';
					$_unit = '';
				}
				$_value = ($_value=='')? '&nbsp;' : $_value;
				
				$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>";
					$x .= "<div style='width:55%; text-align:right; float:left;'>".$_value."</div><div style='width:45%; text-align:left; float:left;'>&nbsp;".$_unit."</div>";
				$x .= "</td>";
			}
		$x .= "</tr>\n";
		
		$x .= "<tr>\n";
			# Truancy
			//2013-0405-0845-35156
			//$thisTitle = $prefix.$eReportCard['Template']['LeaveWithoutPermissionCh']." ".$prefix.$eReportCard['Template']['LeaveWithoutPermissionEn'];
			$thisTitle = $prefix.$eReportCard['Template']['DaysAbsentCh']." ".$prefix.$eReportCard['Template']['DaysAbsentEn'];
			$x .= "<td colspan='2' class='tabletext border_top'>".$thisTitle."</td>";
			for ($i=0; $i<$numOfTermReport; $i++) {
				$_termReportId = $termReportIdAry[$i];
			
				if ($_termReportId != '') {
					$_value = $attendanceInfoAry[$_termReportId]['truancy'];
					$_unit = $eReportCard['Template']['DayCh'];
				}
				else {
					$_value = '';
					$_unit = '';
				}
				$_value = ($_value=='')? '&nbsp;' : $_value;
				
				$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>";
					$x .= "<div style='width:55%; text-align:right; float:left;'>".$_value."</div><div style='width:45%; text-align:left; float:left;'>&nbsp;".$_unit."</div>";
				$x .= "</td>";
			}
			
			# Actual Average
			$thisTitle = $prefix.$eReportCard['Template']['ActualAverageCh']."<br />".$prefix.$eReportCard['Template']['ActualAverageEn'];
			$x .= "<td class='tabletext border_top border_left' colspan='2' rowspan='2'>".$thisTitle."</td>";
			$x .= "<td class='tabletext border_top border_left' rowspan='2' style='text-align:center; vertical-align:middle'>".$ActualAverage."</td>";
		$x .= "</tr>\n";
		
		$x .= "<tr>\n";
			# Tardiness
			$thisTitle = $prefix.$eReportCard['Template']['TardinessCh']." ".$prefix.$eReportCard['Template']['TardinessEn'];
			$x .= "<td colspan='2' class='tabletext border_top'>".$thisTitle."</td>";
			for ($i=0; $i<$numOfTermReport; $i++) {
				$_termReportId = $termReportIdAry[$i];
			
				if ($_termReportId != '') {
					$_value = $attendanceInfoAry[$_termReportId]['timeLate'];
					$_unit = $eReportCard['Template']['DayCh'];
				}
				else {
					$_value = '';
					$_unit = '';
				}
				$_value = ($_value=='')? '&nbsp;' : $_value;
				
				$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>";
					$x .= "<div style='width:55%; text-align:right; float:left;'>".$_value."</div><div style='width:45%; text-align:left; float:left;'>&nbsp;".$_unit."</div>";
				$x .= "</td>";
			}
		$x .= "</tr>\n";
		
		$x .= "<tr>\n";
			# Truancy
			$thisTitle = $prefix.$eReportCard['Template']['LeavingEarlyCh']." ".$prefix.$eReportCard['Template']['LeavingEarlyEn'];
			$x .= "<td colspan='2' class='tabletext border_top'>".$thisTitle."</td>";
			for ($i=0; $i<$numOfTermReport; $i++) {
				$_termReportId = $termReportIdAry[$i];
			
				if ($_termReportId != '') {
					$_value = $attendanceInfoAry[$_termReportId]['earlyLeave'];
					$_unit = $eReportCard['Template']['DayCh'];
				}
				else {
					$_value = '';
					$_unit = '';
				}
				$_value = ($_value=='')? '&nbsp;' : $_value;
				
				$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>";
					$x .= "<div style='width:55%; text-align:right; float:left;'>".$_value."</div><div style='width:45%; text-align:left; float:left;'>&nbsp;".$_unit."</div>";
				$x .= "</td>";
			}
			
			# Position Overall
			$value = ($ReportType=='T')? '&nbsp;' : $ClassPosition;
			$thisTitle = $prefix.$eReportCard['Template']['PositionOverallCh']."<br />".$prefix.$eReportCard['Template']['PositionOverallEn'];
			$x .= "<td class='tabletext border_top border_left' colspan='2' rowspan='2'>".$thisTitle."</td>";
			$x .= "<td class='tabletext border_top border_left' rowspan='2' style='text-align:center; vertical-align:middle'>".$value."</td>";
		$x .= "</tr>\n";
		
		$x .= "<tr>\n";
			# Position In Class
			$thisTitle = $prefix.$eReportCard['Template']['ClassPositionCh']." ".$prefix.$eReportCard['Template']['ClassPositionEn'];
			$x .= "<td colspan='2' class='tabletext border_top'>".$thisTitle."</td>";
			for ($i=0; $i<$numOfTermReport; $i++) {
				$_termReportId = $termReportIdAry[$i];
			
				if ($_termReportId != '') {
					$_classPosition = $grandInfoAry[$_termReportId]['classPosition'];
					$_classNumOfStudent = $grandInfoAry[$_termReportId]['classNumOfStudent'];
					
					if ($_classPosition == '' || $_classPosition == $this->EmptySymbol) {
						$_value = $this->EmptySymbol;
					}
					else {
						$_value = $grandInfoAry[$_termReportId]['classPosition'].'/'.$grandInfoAry[$_termReportId]['classNumOfStudent'];
					}
				}
				else {
					$_value = '';
				}
				$_value = ($_value=='')? '&nbsp;' : $_value;
				
				$x .= "<td class='tabletext border_top border_left' style='text-align:center; vertical-align:middle'>";
					$x .= $_value;
				$x .= "</td>";
			}
		$x .= "</tr>\n";
		
		$x .= "<tr style='height:21px;'>\n";
			// Co-curricular Activities title
			$thisTitle = $eReportCard['Template']['CoCurricularActivitiesCh'].' '.$eReportCard['Template']['CoCurricularActivitiesEn'];
			$x .= "<td colspan='4' class='tabletext_14px border_top' style='text-align:center;'>".$thisTitle."</td>";
			
			// Form Teacher's Comments title
			$thisTitle = $eReportCard['Template']['FormTeachersCommentsCh'].' '.$eReportCard['Template']['FormTeachersCommentsEn'];
			$x .= "<td colspan='3' class='tabletext_14px border_left border_top' style='text-align:center;'>".$thisTitle."</td>";
		$x .= "</tr>\n";
		
		$x .= "<tr>\n";
			// Society title
			$thisTitle = $eReportCard['Template']['SocietyCh'].' '.$eReportCard['Template']['SocietyEn'];
			$x .= "<td class='tabletext border_top' style='width:34%; text-align:center;'>".$thisTitle."</td>";
			
			// Post title
			$thisTitle = $eReportCard['Template']['PostCh'].' '.$eReportCard['Template']['PostEn'];
			$x .= "<td colspan='3' class='tabletext border_left border_top' style='text-align:center;'>".$thisTitle."</td>";
			
			// Form Teacher's Comments
			$x .= "<td colspan='3' rowspan='4' class='tabletext_14px border_left border_top'>".$ClassTeacherComment."</td>";
		$x .= "</tr>\n";
		
		$numOfClubRow = 3;
		for ($i=0; $i<$numOfClubRow; $i++) {
			$_society = $eEnrolData[$i]['ClubTitle'];
			$_post = $eEnrolData[$i]['RoleTitle'];
			
			$_society = ($_society=='')? '&nbsp;' : $_society;
			$_post = ($_post=='')? '&nbsp;' : $_post;
			
			$_border_top = ($i==0)? 'border_top' : '';
			
			$x .= "<tr>\n";
				$x .= "<td class='tabletext_14px $_border_top'>".$_society."</td>";
				$x .= "<td colspan='3' class='tabletext_14px $_border_top border_left'>".$_post."</td>";
			$x .= "</tr>\n";
		}
		
		$rowHeight = 21;
		$x .= "<tr style='height:".$rowHeight."px;'>\n";
			// Award(s) title
			//2013-0405-0845-35156
			//$thisTitle = $eReportCard['Template']['AwardCh'].' '.$eReportCard['Template']['AwardEn'];
			$thisTitle = $eReportCard['Template']['RemarkCh'].' '.$eReportCard['Template']['RemarkEn'];
			$x .= "<td colspan='4' class='tabletext_14px border_top' style='text-align:center;'>".$thisTitle."</td>";
			
			// Merit and Demerit table
			$x .= "<td colspan='3' rowspan='4' class='tabletext_14px border_top border_left'>";
				$x .= "<table cellpadding='0' cellspacing='0' style='width:100%; height:100%; text-align:center;'>";
					// Merit
					$x .= "<tr style='height:".($rowHeight)."px;'>";
						$x .= "<td style='width:25%;'>".$eReportCard['Template']['MeritCh']."</td>";
						$x .= "<td style='width:25%;' class='border_left'>".$eReportCard['Template']['Merit1Ch']."</td>";
						$x .= "<td style='width:25%;' class='border_left'>".$eReportCard['Template']['Merit2Ch']."</td>";
						$x .= "<td style='width:25%;' class='border_left'>".$eReportCard['Template']['Merit3Ch']."</td>";
					$x .= "</tr>";
					$x .= "<tr style='height:".($rowHeight)."px;'>";
						$x .= "<td>".$eReportCard['Template']['MeritEn']."</td>";
						$x .= "<td class='border_left border_top'>".$merits."</td>";
						$x .= "<td class='border_left border_top'>".$minorMerits."</td>";
						$x .= "<td class='border_left border_top'>".$majorMerits."</td>";
					$x .= "</tr>";
					
					// Demerit
					$x .= "<tr style='height:".($rowHeight)."px;'>";
						$x .= "<td class='border_top'>".$eReportCard['Template']['DemeritCh']."</td>";
						$x .= "<td class='border_left border_top'>".$eReportCard['Template']['Demerit1Ch']."</td>";
						$x .= "<td class='border_left border_top'>".$eReportCard['Template']['Demerit2Ch']."</td>";
						$x .= "<td class='border_left border_top'>".$eReportCard['Template']['Demerit3Ch']."</td>";
					$x .= "</tr>";
					$x .= "<tr style='height:".($rowHeight)."px;'>";
						$x .= "<td>".$eReportCard['Template']['DemeritEn']."</td>";
						$x .= "<td class='border_left border_top'>".$demerits."</td>";
						$x .= "<td class='border_left border_top'>".$minorDemerits."</td>";
						$x .= "<td class='border_left border_top'>".$majorDemerits."</td>";
					$x .= "</tr>";
				$x .= "</table>";
			$x .= "</td>";
		$x .= "</tr>\n";
		
		// Award(s)
		$numOfAwardRow = 3;
		for ($i=0; $i<$numOfAwardRow; $i++) {
			$_award = $awardAry[$i];
			$_award = ($_award=='')? '&nbsp;' : $_award;
			
			$_borderTop = ($i==0)? 'border_top' : '';
			
			$x .= "<tr style='height:".$rowHeight."px;'>\n";
				$x .= "<td colspan='4' class='tabletext_14px $_borderTop'>".$_award."</td>\n";
			$x .= "</tr>\n";
		}
		
		# Signature Row
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$x .= "<tr>\n";
			$x .= "<td class='border_top tabletext'>".$eReportCard['Template']['Principal']."</td>\n";
			$x .= "<td colspan='3' class='border_top border_left tabletext'>".$eReportCard['Template']['ClassTeacher']."</td>\n";
			$x .= "<td colspan='3' class='border_top border_left tabletext'>".$eReportCard['Template']['ParentGuardian']."</td>\n";
			$x .= "</td>";
		$x .= "</tr>\n";
		
		# Class to be enrolled next year
//		if ($ReportType=='W') {
//			$thisTitle = $prefix.$eReportCard['Template']['ClassEnrolledNextYearCh'].'<br />'.$prefix.$eReportCard['Template']['ClassEnrolledNextYearEn'];
//			$thisValue = $this->Get_CSV_Value($ary[$YearTermID]['Class_Next_Year']);
//			$x .= "<tr>\n";
//				$x .= "<td colspan='2' class='tabletext border_top'>".$thisTitle."</td>";
//				$x .= "<td colspan='5' class='tabletext border_top'>".$thisValue."</td>";
//			$x .= "</tr>\n";
//		}
		
		
		return $x;
	}

	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		$libFCM = new form_class_manage();
		$TermArr = $libFCM->Get_Academic_Year_Term_List($this->schoolYearID);
		$FirstTermID = $TermArr[0]['YearTermID'];		

		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		$IsFirstTermReport = ($SemID == $FirstTermID)? true : false;

		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;

		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];

		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];

		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);

		$n = 0;
		$x = array();
		$isFirst = 1;
		$TotalSubjectWeight = 0;
		
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		if($ReportType=="T")	# Temrs Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];

			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);

			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;

				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}

				# define css
				$css_border_top = ($isFirst)? "border_top" : "";

				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0 ,0, $ReportID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				# Subject Weight
				$subjectWeightConds = " (ReportColumnID = '' OR ReportColumnID IS NULL) AND SubjectID = '$SubjectID' " ;
				$subjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $subjectWeightConds);
				$subjectWeight = $subjectWeightArr[0]['Weight'];
				
				$isAllNA = true;

				/*
				# Assessment Marks & Display
				for($i=0;$i<sizeof($ColumnID);$i++)
				{
					$thisColumnID = $ColumnID[$i];
					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];

					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
					$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
					
					if ($isSub && $columnSubjectWeightTemp == 0)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
						$thisMarkDisplay = $this->EmptySymbol;
					} else {
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];

						$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnID[$i]][Mark] : "";
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][$ColumnID[$i]][Grade] : "";

						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}

						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;

						if ($thisMark != "N.A.")
						{
							$isAllNA = false;
						}

						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
						
						if($needStyle)
						{
							if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
							{
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							}
							else
							{
								$thisMarkTemp = $thisMark;
							}
							
							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
							$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
							
							# Formated mark for Average display
							if (is_numeric($thisMark))
								$thisFormatMark = my_round($thisMark, 1);
							$thisFormatMarkDisplay = $this->ReturnTextwithStyle($thisFormatMark, 'HighLight', $thisNature);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
							
							# Formated mark for Average display
							if (is_numeric($thisMark))
								$thisFormatMark = my_round($thisMark, 1);
							$thisFormatMarkDisplay = $thisFormatMark;
						}

					//	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					//	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
					//	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark;
					}
					
					## Grade convertion
					if ($thisGrade=="") {
						$ConvertedGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark);
					} else {
						$ConvertedGrade = $thisGrade;
					}
					
					if($ConvertedGrade !== "")
					{
						if($needStyle)
						{
	  						$thisGradeDisplay = $this->ReturnTextwithStyle($ConvertedGrade, 'HighLight', $thisNature);
  						}
  						else
  						{
	  						$thisGradeDisplay = $ConvertedGrade;
  						}
					}
					
					$thisScoreDisplay = $thisMarkDisplay.' '.$thisGradeDisplay;

					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisScoreDisplay ."</td>";

  					if($ShowSubjectFullMark)
  					{
	  					# check special full mark
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];

						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
					}

					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}
				*/
				
				# Empty Column for 1st term column
				if ($IsFirstTermReport == false)
					$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center' width='100'>&nbsp;</td>";
				
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall)
				{
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][0][Grade] : "";

					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}

					#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					# check special case
					$thisFormatMarkDisplay = '';
					$thisMarkDisplay = '';
					$thisFormatMark = '';
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					
					if (substr($thisMark, 0, 4) != "N.A." && substr($thisMark, 0, 2) != "NA")
					{
						$isAllNA = false;
					}
					
					if($needStyle)
					{
 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
						else
							$thisMarkTemp = $thisMark;
							
 						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp, 0, '', $ReportID);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
						
						# Formated mark for Average display
						if (is_numeric($thisMark))
							$thisFormatMark = my_round($thisMark, 1);
						$thisFormatMark = ($thisFormatMark == '')? "&nbsp;" : $thisFormatMark;
						$thisFormatMarkDisplay = $this->ReturnTextwithStyle($thisFormatMark, 'HighLight', $thisNature);
					}
					else
					{
						$thisMarkDisplay = $thisMark;
						
						# Formated mark for Average display
						if (is_numeric($thisMark))
							$thisFormatMark = my_round($thisMark, 1);
						$thisFormatMarkDisplay = $thisFormatMark;
					}
						
					## Grade convertion
					$ConvertedGrade = '';
					if ($thisGrade=="") {
						$ConvertedGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark, $ReportID);
					} else {
						$ConvertedGrade = $thisGrade;
					}
					
					$thisGradeDisplay = '';
					if($ConvertedGrade !== "")
					{
						if($needStyle)
						{
	  						$thisGradeDisplay = $this->ReturnTextwithStyle($ConvertedGrade, 'HighLight', $thisNature);
  						}
  						else
  						{
	  						$thisGradeDisplay = $ConvertedGrade;
  						}
					}
					$thisScoreDisplay = $thisMarkDisplay.' '.$thisGradeDisplay;

					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisScoreDisplay ."</td>";
  					if($ShowSubjectFullMark)
  					{
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $thisFullMark .")</td>";
					}
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
				}
				
				# Empty Column for 2nd term column
				if ($IsFirstTermReport)
					$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center' width='100'>&nbsp;</td>";
				
				# Average
				$thisFormatMarkDisplay = ($thisFormatMarkDisplay == '')? "&nbsp;" : $thisFormatMarkDisplay;
				$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center' width='100'>". $thisFormatMarkDisplay ."</td>";
				
				# Periods (weight)
				$periodDisplay = ($subjectWeight == '')? "&nbsp;" : $subjectWeight;
				$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center' width='100'>". $periodDisplay ."</td>";
				
				# Total
				$thisTotal = $thisMark * $subjectWeight;
				$thisTotal = my_round($thisTotal, 1);
				$totalDisplay = ($thisTotal == '')? "&nbsp;" : $thisTotal;
				$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center' width='100'>". $totalDisplay ."</td>";
				
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
				$returnArr['SubjectWeight'][$SubjectID] = $subjectWeight;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];

			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);

			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $ReportID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];

				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;

				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# Subject Weight
				$subjectWeightConds = " (ReportColumnID = '' OR ReportColumnID IS NULL) AND SubjectID = '$SubjectID' " ;
				$subjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $subjectWeightConds);
				$subjectWeight = $subjectWeightArr[0]['Weight'];
				
				$isAllNA = true;

				# Terms's Assesment / Terms Result
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
					$css_border_top = $isFirst? "border_top" : "";
					//$css_border_top = $isSub? "" : "border_top";

					/*
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					if($isDetails==1)		# Retrieve assesments' marks
					{
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum'] ."' and ClassLevelID = '".$ClassLevelID."' ");

						# if no term reports, CANNOT retrieve assessment result at all
						if (empty($thisReport)) {
							for($j=0;$j<sizeof($ColumnID);$j++) {
								$x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
							}
						} else {
							$thisReportID = $thisReport['ReportID'];
							$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
							$ColumnID = array();
							$ColumnTitle = array();
							foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);

							$thisMarksAry = $this->getMarks($thisReportID, $StudentID);

							for($j=0;$j<sizeof($ColumnID);$j++)
							{
								$thisColumnID = $ColumnID[$i];
								$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
								$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
								$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];

								$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
								$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);

								if ($isSub && $columnSubjectWeightTemp == 0)
								{
									$thisMarkDisplay = "---";
								}
								else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
									$thisMarkDisplay = "---";
								} else {
									$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnID[$j] ."' and SubjectID = '$SubjectID' ");
									$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];

									$thisMark = $ScaleDisplay=="M" ? $thisMarksAry[$SubjectID][$ColumnID[$j]][Mark] : "";
									$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $thisMarksAry[$SubjectID][$ColumnID[$j]][Grade] : "";

									# for preview purpose
									if(!$StudentID)
									{
										$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
										$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
									}
									$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;

									if ($thisMark != "N.A.")
									{
										$isAllNA = false;
									}

									# check special case
									list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
									if($needStyle)
									{
										if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
										{
											$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
										}
										else
										{
											$thisMarkTemp = $thisMark;
										}
										$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
										$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
									}
									else
									{
										$thisMarkDisplay = $thisMark;
									}
								}

								$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
								$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
			  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";

			  					if($ShowSubjectFullMark)
			  					{
				  					$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
									$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
								}

								$x[$SubjectID] .= "</tr></table>";
								$x[$SubjectID] .= "</td>";
							}
						}
					}
					*/
					//else					# Retrieve Terms Overall marks
					//{
						//if ($isParentSubject && $CalculationOrder == 1) {
						//	$thisMarkDisplay = $this->EmptySymbol;
						//	$thisScoreDisplay = $thisMarkDisplay;
						//} else {
							# See if any term reports available
							$thisReport = $this->returnReportTemplateBasicInfo('', $others='', $ClassLevelID, $ColumnData[$i]['SemesterNum']);

							# if no term reports, the term mark should be entered directly
							if (empty($thisReport)) {
								$thisReportID = $ReportID;
								$MarksAry = $this->getMarks($thisReportID, $StudentID);
								$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : "";
								$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
							} else {
								$thisReportID = $thisReport['ReportID'];
								$MarksAry = $this->getMarks($thisReportID, $StudentID);
								$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : "";
								$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
							}

							$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
							$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];

							# for preview purpose
							if(!$StudentID)
							{
								$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
								$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
							}
							# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
							$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;

							# check special case
							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
							
							if (substr($thisMark, 0, 4) != "N.A." && substr($thisMark, 0, 2) != "NA")
							{
								$isAllNA = false;
							}
							
							if($needStyle)
							{
		 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
									$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
								else
									$thisMarkTemp = $thisMark;

		 						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp, 0, '', $thisReportID);
								$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
								
								# Formated mark for Average display
								if (is_numeric($thisMark))
									$thisFormatMark = my_round($thisMark, 1);
								$thisFormatMark = ($thisFormatMark == '')? "&nbsp;" : $thisFormatMark;
								$thisFormatMarkDisplay = $this->ReturnTextwithStyle($thisFormatMark, 'HighLight', $thisNature);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
								
								# Formated mark for Average display
								if (is_numeric($thisMark))
									$thisFormatMark = my_round($thisMark, 1);
								$thisFormatMarkDisplay = $thisFormatMark;
							}
							
							## Grade convertion
							$thisGradeDisplay = '';
							if ($thisGrade=="") {
								$ConvertedGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark, $thisReportID);
							} else {
								$ConvertedGrade = $thisGrade;
							}
							
							
							if($ConvertedGrade !== "" && $ConvertedGrade != '/')
							{
								if($needStyle)
								{
			  						$thisGradeDisplay = $this->ReturnTextwithStyle($ConvertedGrade, 'HighLight', $thisNature);
		  						}
		  						else
		  						{
			  						$thisGradeDisplay = $ConvertedGrade;
		  						}
							}
							$thisScoreDisplay = $thisMarkDisplay.' '.$thisGradeDisplay;
							
						//}

						$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisScoreDisplay ."</td>";

	  					if($ShowSubjectFullMark)
	  					{
 							$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1 && $UseWeightedMark) ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID];
							$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
						}

						$x[$SubjectID] .= "</tr></table>";
						$x[$SubjectID] .= "</td>";
					//}
				}

				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall && $CalculationOrder == 1)
				{
					$MarksAry = $this->getMarks($ReportID, $StudentID);

					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][0][Grade] : "";

					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}

					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,1) : $thisMark) : $thisGrade;

					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					
					if (substr($thisMark, 0, 4) != "N.A." && substr($thisMark, 0, 2) != "NA")
					{
						$isAllNA = false;
					}
					
					if($needStyle)
					{
						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark, 0, '', $ReportID);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
						
					## Grade convertion
					if ($thisGrade=="") {
						$ConvertedGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark, $ReportID);
					} else {
						$ConvertedGrade = $thisGrade;
					}
					
					if($ConvertedGrade !== "")
					{
						if($needStyle)
						{
	  						$thisGradeDisplay = $this->ReturnTextwithStyle($ConvertedGrade, 'HighLight', $thisNature);
  						}
  						else
  						{
	  						$thisGradeDisplay = $ConvertedGrade;
  						}
					}
					$thisScoreDisplay = $thisMarkDisplay.' '.$thisGradeDisplay;

					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisScoreDisplay ."</td>";
  					if($ShowSubjectFullMark)
  					{
						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $SubjectFullMarkAry[$SubjectID] .")</td>";
					}
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
				}
				
				# Periods (weight)
				$periodDisplay = ($subjectWeight == '')? "&nbsp;" : $subjectWeight;
				$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center' width='100'>". $periodDisplay ."</td>";
				
				# Total
				$thisTotal = $thisMark * $subjectWeight;
				$thisTotal = my_round($thisTotal, 1);
				$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center' width='100'>". $thisTotal ."</td>";

				$isFirst = 0;

				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
				$returnArr['SubjectWeight'][$SubjectID] = $subjectWeight;
			}
		}	# End Whole Year Report Type

		return $returnArr;
	}

	function getMiscTable($ReportID, $StudentID='')
	{
		/*
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;

		// also cater office.broadlearning to display comment box only
		if ($eRCTemplateSetting['HideCSVInfo'] && !$eRCTemplateSetting['JustShowComment'])
		{
			return "";
		}


		# Retrieve Basic Information of Report
		$BasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$AllowClassTeacherComment = $BasicInfo['AllowClassTeacherComment'];

		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];

		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

		# retrieve the latest Term
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		foreach($sems as $TermID=>$TermName)
			$latestTerm = $TermID;
		$latestTerm++;

		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}

		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		foreach($csvType as $k=>$Type)
		{
			$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);
			if(!empty($csvData))
			{
				foreach($csvData as $RegNo=>$data)
				{
					if($RegNo == $WebSamsRegNo)
					{
	 					foreach($data as $key=>$val)
		 					$ary[$key] = $val;
					}
				}
			}
		}

		# retrieve result data
		$Absence = ($ary['Days Absent'])? $ary['Days Absent'] : 0;
		$Lateness = ($ary['Time Late'])? $ary['Time Late'] : 0;
		$EarlyLeave = ($ary['Early Leave'])? $ary['Early Leave'] : 0;
		$Promotion = ($ary['Promotion'])? $ary['Promotion'] : $this->EmptySymbol;
		$Merits = ($ary['Merits'])? $ary['Merits'] : 0;
		$MinorCredit = ($ary['Minor Credit'])? $ary['Minor Credit'] : 0;
		$MajorCredit = ($ary['Major Credit'])? $ary['Major Credit'] : 0;
		$Demerits = ($ary['Merits'])? $ary['Demerits'] : 0;
		$MinorFault = ($ary['Minor Fault'])? $ary['Minor Fault'] : 0;
		$MajorFault = ($ary['Major Fault'])? $ary['Major Fault'] : 0;
		$Remark = ($ary['Remark'])? $ary['Remark'] : $this->EmptySymbol;
		$ECA = ($ary['ECA'])? $ary['ECA'] : $this->EmptySymbol;
		if (is_array($ECA))
			$ecaList = implode("<br>", $ECA);
		else
			$ecaList = $ECA;


		$merit = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		$merit .= "<tr>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['Merits'] . ": " . ($ary['Merits'] ? $ary['Merits'] :0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['Demerits'] . ": " . ($ary['Demerits'] ? $ary['Demerits'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorCredit'] . ": " . ($ary['Minor Credit'] ? $ary['Minor Credit'] :0) ."</td>";
		$merit .= "</tr>";
		$merit .= "<tr>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorCredit'] . ": " . ($ary['Major Credit'] ? $ary['Major Credit'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorFault'] . ": " . ($ary['Minor Fault'] ? $ary['Minor Fault'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorFault'] . ": " . ($ary['Major Fault'] ? $ary['Major Fault'] : 0) ."</td>";
		$merit .= "</tr>";
		$merit .= "</table>";
		$remark = $ary['Remark'];
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$classteachercomment = $CommentAry[0];
		$eca = $ary['ECA'];
		if (is_array($eca))
			$ecaList = implode("<br>", $eca);
		else
			$ecaList = $eca;

		$x .= "<br>";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='60' class='report_border'>";
		$x .= "<tr>";
		$x .= "<td width='50%' valign='top'>";
			# Merits & Demerits
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr>";
				$x .= "<td class='tabletext'><b>". $eReportCard['Template']['MeritsDemerits']."</b><br>".$merit."</td>";
			$x .= "</tr>";
			$x .= "</table>";
			$x .= "</td></tr></table>";
		$x .="</td>";
		$x .= "<td width='50%' class=\"border_left\" valign='top'>";
			# Remark
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr>";
				$x .= "<td class='tabletext'><b>". $eReportCard['Template']['Remark']."</b><br>".$remark."</td>";
			$x .= "</tr>";
			$x .= "</table>";
			$x .= "</td></tr></table>";
		$x .="</td>";
		$x .= "</tr>";
		$x .= "</table>";

		$x .= "<br>";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='60' class='report_border'>";
		$x .= "<tr>";
		if($AllowClassTeacherComment)
		{
			$x .= "<td width='50%' valign='top'>";
				# Class Teacher Comment
				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
				$x .= "<tr>";
					$x .= "<td class='tabletext'><b>". $eReportCard['Template']['ClassTeacherComment']."</b><br>".stripslashes($classteachercomment)."</td>";
				$x .= "</tr>";
				$x .= "</table>";
				$x .= "</td></tr></table>";
			$x .="</td>";

			$x .= "<td width='50%' class='border_left' valign='top'>";

			// for office.broadlearning to display comment box only
			if ($eRCTemplateSetting['JustShowComment'])
			{
				if (trim($classteachercomment)=="")
				{
					$classteachercomment = "--";
				}

				$rx .= "<br /><table width=\"100%\" border=\"0\" cellpadding=\"9\" cellspacing=\"0\" style=\"border:solid #222222 1px;\">";
				$rx .= "<tr>";
				$rx .= "<td class='tabletext' valign='top' nowrap='nowrap'><b>Comment: &nbsp;</b></td><td width='95%' class='tabletext'>".stripslashes($classteachercomment)."</td>";
				$rx .= "</tr>";
				$rx .= "</table>";

				return $rx;
			}
		} else {
			$x .= "<td width='50%' valign='top'>";
		}
			# ECA
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr>";
				$x .= "<td class='tabletext'><b>". $eReportCard['Template']['eca']."</b><br>".$ecaList."</td>";
			$x .= "</tr>";
			$x .= "</table>";
			$x .= "</td></tr></table>";
		$x .="</td>";
		$x .= "</tr>";
		$x .= "</table>";
		*/

		return $x;
	}

	########### END Template Related

	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;

		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];

		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;

		# initialization
		$border_top = "";
		$x = "";

		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh);

		if($ReportType=="W")	# Whole Year Report
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);

			$thisTotalValue = "";
			foreach($ColumnData as $k1=>$d1)
			{
				# get value of this term
				$TermID = $d1['SemesterNum'];
				$thisValue = $StudentID ? ($ValueArr[$TermID][$InfoKey] ? $ValueArr[$TermID][$InfoKey] : $this->EmptySymbol) : "#";

				# calculation the overall value
				if (is_numeric($thisValue)) {
					if ($thisTotalValue == "")
						$thisTotalValue = $thisValue;
					else if (is_numeric($thisTotalValue))
						$thisTotalValue += $thisValue;
				}

				# insert empty cell for assessments
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";

				# display this term value
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			}

			# display overall year value
			if($ShowRightestColumn)
			{
				$thisTotalValue = ($thisTotalValue=="")? $this->EmptySymbol : $thisTotalValue;
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisTotalValue."</td>";
			}
		}
		else				# Term Report
		{
			# get value of this term
			$thisValue = $StudentID ? ($ValueArr[$SemID][$InfoKey] ? $ValueArr[$SemID][$InfoKey] : $this->EmptySymbol) : "#";

			# insert empty cell for assessments
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";

			# display this term value
			if($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
		}

		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";

		return $x;
	}

	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0, $UpperLimit="")
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;

		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight 				= $ReportSetting['LineHeight'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];

		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		$CalOrder = $this->Get_Calculation_Setting($ReportID);
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

		$border_top = $isFirst ? "border_top" : "";

		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, $isFirst);

		if ($CalOrder == 1) {
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
		} else {
			$curColumn = 0;
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				for ($i=0; $i<$NumOfAssessment[$curColumn]-1 ; $i++)
				{
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				}

				$thisValue = $ValueArr[$ColumnID];
				if ($UpperLimit=="" || $UpperLimit==0 || $thisValue <= $UpperLimit)
				{
					$thisDisplay = $thisValue;
				}
				else
				{
					$thisDisplay = $this->EmptySymbol;
				}
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisDisplay."&nbsp;</td>";
				$curColumn++;
			}
		}

		if ($ShowRightestColumn)
		{
			$thisValue = $OverallValue;
			if ($UpperLimit=="" || $UpperLimit==0 || $thisValue <= $UpperLimit)
			{
				$thisDisplay = $thisValue;
			}
			else
			{
				$thisDisplay = $this->EmptySymbol;
			}
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisDisplay ."&nbsp;</td>";
		}

		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";

		return $x;
	}

	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0)
	{
		$x = "";
		$border_top = ($hasBorderTop)? "border_top" : "";

		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleEn ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleCh ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";

		return $x;
	}

	function Is_Show_Rightest_Column($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];

		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;

		return $ShowRightestColumn;
	}

	function Get_Calculation_Setting($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";

		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];

		return $CalOrder;
	}
	
	function Get_CSV_Value($Value, $NullValue='&nbsp;')
	{
		$Value = trim($Value);
		if ($Value == '')
			return $NullValue;
		else
			return $Value;
	}
	
	function Get_CSV_Row($TitleEn, $TitleCh, $Value, $CsvType='')
	{
		$prefix = '&nbsp;&nbsp;';
		$suffix = ($CsvType == 'Award')? "<br />" : "<br /><br />";
		$thisTitle = $prefix.$TitleCh."<br />".$prefix.$TitleEn.$suffix;
		
		if (is_array($Value))
			$Value = implode('<br />', $Value);
		
		$x .= "<tr>\n";
			$x .= "<td colspan='7' class='border_top'>";
				$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
					$x .= "<tr>\n";
						$x .= "<td class='tabletext' width='240px'>".$thisTitle."</td>";
						$x .= "<td class='tabletext' style='vertical-align:middle'>$Value</td>";
					$x .= "</tr>\n";
				$x .= "</table>";
			$x .= "</td>";
		$x .= "</tr>\n";
		
		return $x;
	}
}
?>