<?php
# Editing by : 

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/holy_trinity.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "remark", "attendance");
		$this->OtherInfoInGrandMS = array("Conduct","Remark","Days Absent","Time Late");	
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "---";
		$this->EmptyStarSymbol = "*****";
		$this->EmptyLineStarSymbol = "******************************************";
		
		### For Subject Passing Percentage Report
		$this->HardCodePassMark = 50;
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard, $eRCTemplateSetting;
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
		$TableTop .= "<tr><td height='85'>".$TitleTable."</td></tr>";
		$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='".$eRCTemplateSetting['ReportWidth']."' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				//$x .= "<tr height='850px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='top'><td>".$TableTop."</td><tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	#No Header for Holy Trinity College: Modified by Josephine (2009/11/13)
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			//$ReportTitle =  $ReportSetting['ReportTitle'];
			//$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
			# get school badge
			//$SchoolLogo = GET_SCHOOL_BADGE();
			$SchoolLogo = '';
				
			# get school name
			//$SchoolName = GET_SCHOOL_NAME();	
			$SchoolName = '';
		
			//$ChineseDisplay = str_replace(' ', "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $eReportCard['Template']['SchoolNameCh']);
			//$SchoolName .= '<span >'.strtoupper($eReportCard['Template']['SchoolNameEn']).'</span>';
			//$SchoolName .= '<br />';
			//$SchoolName .= '<span >'.$ChineseDisplay.'</span>';
			
			$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
	
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
					if(!empty($SchoolName))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$SchoolName."</td></tr>\n";
					if(!empty($ReportTitle))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle."</td></tr>\n";
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			}
			$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			$SemID = $ReportSetting['Semester'];
			$ReportType = $SemID == "F" ? "W" : "T";
			
			# retrieve required variables
			$defaultVal = ($StudentID=='')? "XXX" : '';
			//$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			$ObjYear = new academic_year($this->schoolYearID);
			$data['AcademicYear'] = $ObjYear->Get_Academic_Year_Name();
			
			$DateOfIssue = $ReportSetting['Issued'];
			$data['DateOfIssue'] = date("d/m/Y", strtotime($DateOfIssue));
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] = $lu->UserName2Lang('en', 1);
			
				//$data['ClassNo'] = $lu->ClassNumber;
				//$data['Class'] = $lu->ClassName;
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				
				$data['ClassNo'] = $thisClassNumber;
				$data['Class'] = $thisClassName;
				
				$data['StudentNo'] = $thisClassNumber;
				
				if (is_array($SettingStudentInfo))
				{
					if (!in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo) && ($thisClassNumber != ""))
						$data['Class'] .= " (".$thisClassNumber.")";
				}
				else
				{
					if ($thisClassNumber != "")
						$data['Class'] .= " (".$thisClassNumber.")";
				}
				
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] = $lu->Gender;
				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				$data['StudentAdmNo'] = $data['STRN'];
				
				$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
				foreach((array)$ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
			}
			
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
				
					//Student Info: Modified by Josephine (2009/11/18)
					$StudentInfoTable .= "<tr>";
						$StudentInfoTable .= "<td class='tabletext' width='10%' align='right' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= $eReportCard['Template']['StudentInfo']['NameEn']."&nbsp;&nbsp;";
						$StudentInfoTable .= "</td>";
						$StudentInfoTable .= "<td class='tabletext' width='7%' align='left' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= $eReportCard['Template']['StudentInfo']['NameCh'];
						$StudentInfoTable .= "</td>";
						$StudentInfoTable .= "<td class='tabletext' width='2%' align='left' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= "&nbsp;".":"."&nbsp;";
						$StudentInfoTable .= "</td>";
						
						$SettingID = trim($StudentTitleArray[0]);
						$thisWidth = ($ReportType == 'T')? "34%" : "36%";
						$StudentInfoTable .= "<td class='tabletext' width='$thisWidth' align='left' height='{$LineHeight}' nowrap >".($data[$SettingID] ? $data[$SettingID] : $defaultVal )."</td>";
						
						$StudentInfoTable .= "<td class='tabletext' width='6%' height='{$LineHeight}'>&nbsp;</td>";
						
						$StudentInfoTable .= "<td class='tabletext' align='right' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= $eReportCard['Template']['StudentInfo']['AcademicYearEn']."&nbsp;&nbsp;";
						$StudentInfoTable .= "</td>";
						$StudentInfoTable .= "<td class='tabletext' align='left' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= $eReportCard['Template']['StudentInfo']['AcademicYearCh'];
						$StudentInfoTable .= "</td>";
						$StudentInfoTable .= "<td class='tabletext' align='left' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= "&nbsp;".":"."&nbsp;";
						$StudentInfoTable .= "</td>";
						
						$SettingID = trim($StudentTitleArray[5]);
						$StudentInfoTable .= "<td class='tabletext' align='left' height='{$LineHeight}' nowrap>".($data[$SettingID] ? $data[$SettingID] : $defaultVal )."</td>";
						
						
					$StudentInfoTable .= "</tr>";
					
					$StudentInfoTable .= "<tr>";
						$StudentInfoTable .= "<td class='tabletext' align='right' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= $eReportCard['Template']['StudentInfo']['ClassEn']."&nbsp;&nbsp;";
						$StudentInfoTable .= "</td>";
						$StudentInfoTable .= "<td class='tabletext' align='left' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= $eReportCard['Template']['StudentInfo']['ClassCh'];
						$StudentInfoTable .= "</td>";
						$StudentInfoTable .= "<td class='tabletext' align='left' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= "&nbsp;".":"."&nbsp;";
						$StudentInfoTable .= "</td>";
						
						$SettingID = trim($StudentTitleArray[1]);
						$StudentInfoTable .= "<td class='tabletext' align='left' height='{$LineHeight}' nowrap>".($data[$SettingID] ? $data[$SettingID] : $defaultVal )."</td>";
						
						$StudentInfoTable .= "<td class='tabletext' height='{$LineHeight}'>&nbsp;</td>";
	
						$StudentInfoTable .= "<td class='tabletext' align='right' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= $eReportCard['Template']['StudentInfo']['ClassNoEn']."&nbsp;&nbsp;";
						$StudentInfoTable .= "</td>";
						$StudentInfoTable .= "<td class='tabletext' align='left' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= $eReportCard['Template']['StudentInfo']['ClassNoCh'];
						$StudentInfoTable .= "</td>";
						$StudentInfoTable .= "<td class='tabletext' align='left' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= "&nbsp;".":"."&nbsp;";
						$StudentInfoTable .= "</td>";
						
						$SettingID = trim($StudentTitleArray[2]);
						$StudentInfoTable .= "<td class='tabletext' align='left' height='{$LineHeight}'>".($data[$SettingID] ? $data[$SettingID] : $defaultVal )."</td>";
						
					$StudentInfoTable .= "</tr>";
					
					$StudentInfoTable .= "<tr>";
						$StudentInfoTable .= "<td class='tabletext' align='right' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= $eReportCard['Template']['StudentInfo']['STRNEn']."&nbsp;&nbsp;";
						$StudentInfoTable .= "</td>";
						$StudentInfoTable .= "<td class='tabletext' align='left' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= $eReportCard['Template']['StudentInfo']['STRNCh'];
						$StudentInfoTable .= "</td>";
						$StudentInfoTable .= "<td class='tabletext' align='left' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= "&nbsp;".":"."&nbsp;";
						$StudentInfoTable .= "</td>";
						
						$SettingID = trim($StudentTitleArray[9]);
						$StudentInfoTable .= "<td class='tabletext' align='left' height='{$LineHeight}' nowrap>".($data[$SettingID] ? $data[$SettingID] : $defaultVal )."</td>";
						
						$StudentInfoTable .= "<td class='tabletext' height='{$LineHeight}'>&nbsp;</td>";
						
												$StudentInfoTable .= "<td class='tabletext' width='15%' align='right' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= $eReportCard['Template']['StudentInfo']['DateOfIssueEn']."&nbsp;&nbsp;";
						$StudentInfoTable .= "</td>";
						$StudentInfoTable .= "<td class='tabletext' width='7%' align='left' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= $eReportCard['Template']['StudentInfo']['DateOfIssueCh'];
						$StudentInfoTable .= "</td>";
						$StudentInfoTable .= "<td class='tabletext' width='2%' align='left' height='{$LineHeight}' nowrap>";
						$StudentInfoTable .= "&nbsp;".":"."&nbsp;";
						$StudentInfoTable .= "</td>";
						
						$SettingID = trim($StudentTitleArray[6]);
						$StudentInfoTable .= "<td class='tabletext' width='23%' align='left' height='{$LineHeight}' nowrap>".($data[$SettingID] ? $data[$SettingID] : $defaultVal )."</td>";
						
						
					$StudentInfoTable .= "</tr>";
					
					
				
				$StudentInfoTable .= "</table><br>";
			}
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
	
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
  		$result = $this->getReportResultScore($ReportID, 0, $StudentID, '', 0, 1);
  		$FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";

  		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach((array)$MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		
		if (sizeof($SubjectArray) > 0)
			foreach((array)$SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID,'',0,1);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		list($SubjectCol, $borderTopArr) = $this->returnTemplateSubjectCol($ReportID, $ClassLevelID,$isAllNAAry);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr, $borderTopArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='report_border mstable'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach((array)$MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
				continue;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				# Subject Teacher Comment
				if ($AllowSubjectTeacherComment) {
					$css_border_top = $isSub?"":"border_top";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
					}
					$DetailsTable .= "</span></td>";
				}
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$SemesterNum = $this->Get_Semester_Seq_Number($SemID);

		$ShowSubjectFullMark        = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight                 = $ReportSetting['LineHeight'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		#########################################################
		############## Marks START
		$row2 = "";
		if($ReportType == "T")	# Terms Report Type
		{
			$objYearTerm = new academic_year_term($SemID);
				
			# Retrieve Invloved Assesment
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if (sizeof($ColoumnTitle) > 0) {
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
            }
			$e = 0;

			//Add No. of Candidates and Full Mark Columns: Modified by Josephine (2009/11/17)
			$row1 .= "<td valign='middle' height='{$LineHeight}' width='".$eRCTemplateSetting['ColumnWidth']['Term']['Column']."' class='border_left tabletext' align='center'><b>".$eReportCard['Template']['CandidatesNoEn']."<br>".$eReportCard['Template']['CandidatesNoCh']."</b></td>";
			$n++;
			$n++;

			$row1 .= "<td valign='middle' height='{$LineHeight}' width='".$eRCTemplateSetting['ColumnWidth']['Term']['Column']."' class='border_left tabletext' align='center'><b>".$eReportCard['Template']['FullMarksEn']."<br>".$eReportCard['Template']['FullMarksCh']."</b></td>";
			$e++;
			$e++;
		
		}
		else					# Whole Year Report Type
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
 			$needRowspan = 0;

 			//Add No. of Candidates and Full Mark Columns: Modified by Josephine (2009/11/17)
			$row1 .= "<td valign='middle' height='{$LineHeight}' width='".$eRCTemplateSetting['ColumnWidth']['Consolidate']['NoOfCandidate']."' class='border_left tabletext' align='center'><b>".$eReportCard['Template']['CandidatesNoEn']."<br>".$eReportCard['Template']['CandidatesNoCh']."</b></td>";
			$row1 .= "<td valign='middle' height='{$LineHeight}' width='".$eRCTemplateSetting['ColumnWidth']['Consolidate']['FullMark']."' class='border_left tabletext' align='center'><b>".$eReportCard['Template']['FullMarksEn']."<br>".$eReportCard['Template']['FullMarksCh']."</b></td>";

 			//debug_pr(sizeof($ColumnData));
			for($i=0; $i<sizeof($ColumnData); $i++)
			{
				//$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
				$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
				if($isDetails == 1)
				{
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);

					$ColumnID = array();
					$ColumnTitle = array();
					foreach ((array)$ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);

					$colspan = "colspan='". sizeof($ColumnTitle) ."'";
					$Rowspan = "";
					$needRowspan++;
					$t[$i] = sizeof($ColumnTitle);
				}
				else
				{
					$colspan = "";
					$Rowspan = "rowspan='1'";
					$e++;
				}
				
				//if($i==0){
				//	$SemName = $eReportCard['Template']['TermOneEn']."<br>".$eReportCard['Template']['TermOneCh'];
				//}else{
				//	$SemName = $eReportCard['Template']['TermTwoEn']."<br>".$eReportCard['Template']['TermTwoCh'];
				//}
				$objYearTerm = new academic_year_term($ColumnData[$i]['SemesterNum']);
				$SemName = $objYearTerm->YearTermNameEN."<br>".$objYearTerm->YearTermNameB5;

				$row1 .= "<td {$Rowspan} height='{$LineHeight}' class='border_left small_title' align='center' width='".$eRCTemplateSetting['ColumnWidth']['Consolidate']['Term']."'>". $SemName ."</td>";
			}
		}
		############## Marks END
		#########################################################
		//$Rowspan = $row2 ? "rowspan='2'" : "";
		if(!$needRowspan) {
			$row1 = str_replace("rowspan='2'", "", $row1);
        }

		$x = "<tr>";

		# Subject 
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		
		$x .= "<td colspan='2' {$Rowspan} align='center' valign='middle' class='small_title' height='{$LineHeight}' width='".$eRCTemplateSetting['ColumnWidth']['Subject']."'>".$eReportCard['Template']['SubjectEn']."<br>".$eReportCard['Template']['SubjectCh']."</td>";
		
		# Marks
		$x .= $row1;
		
		# Subject Overall 
		if($ShowRightestColumn)
		{
			if($this->GET_FORM_NUMBER($ClassLevelID) == 6)
			{
				$DisplayLastColumnEn = $eReportCard['Template']['OverallResultEn'];
				$DisplayLastColumnCh = $eReportCard['Template']['OverallResultCh'];
				$OverallWidth = $eRCTemplateSetting['ColumnWidth']['Term']['Column'];
			}
			else if($ReportType == "W")
			{
				$DisplayLastColumnEn = $eReportCard['Template']['OverallResultEn'];
				$DisplayLastColumnCh = $eReportCard['Template']['OverallResultCh'];
				$OverallWidth = $eRCTemplateSetting['ColumnWidth']['Consolidate']['Overall'];
			}
			else
            {
				$DisplayLastColumnEn = $objYearTerm->YearTermNameEN;
				$DisplayLastColumnCh = $objYearTerm->YearTermNameB5;
				$OverallWidth = $eRCTemplateSetting['ColumnWidth']['Term']['Column'];

				// [2020-0703-0936-59207]
                if($this->schoolYear == '2019' && $SemesterNum == 2) {
                    $DisplayLastColumnEn = $eReportCard['Template']['OverallResultEn'];
                    $DisplayLastColumnCh = $eReportCard['Template']['OverallResultCh'];
                }
			}

			$x .= "<td {$Rowspan}  valign='middle' class='border_left small_title' align='center' width='$OverallWidth'>".$DisplayLastColumnEn."<br>".$DisplayLastColumnCh."</td>";
			$n++;
		}
		else
		{
			//$e--;	
		}
		
		# Subject Teacher Comment
		/*
		if ($AllowSubjectTeacherComment) {
			$x .= "<td {$Rowspan} valign='middle' class='border_left small_title' align='center' width='10%'>".$eReportCard['TeachersComment']."</td>";
		}
		*/
		
		$x .= "</tr>";
		if($row2) {
		    $x .= "<tr>". $row2 ."</tr>";
        }
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID, $isAllNAAry)
	{
		global $eRCTemplateSetting, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");		
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
 		$borderTopArr = array();
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
			
			$temp_count=-1;
	 		foreach((array)$SubjectArray as $SubjectID=>$Ary)
	 		{	
	 			
	 			//Check category
 				$SubjectObj = new subject ($SubjectID);
				$thisLearningCategoryID = $SubjectObj->LearningCategoryID;
				
				if($isAllNAAry[$SubjectID]==1)
				{
					$boolean_temp = 0;
				}
				else
				{
					if($temp_count==$thisLearningCategoryID){
						$boolean_temp = 0;
					}else{
						$boolean_temp = 1;
					}
					$temp_count=$thisLearningCategoryID;
				}
		 		foreach((array)$Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		if($Prefix){
			 			$css_border_top = "";
			 			$css_component_text = "component_text";
			 			$borderTopArr[$SubSubjectID] = 0;
			 		}else{
			 			$css_border_top = "border_top";
			 			$css_component_text = "tabletext";
			 			if($boolean_temp==1){
			 				$css_border_top = "border_top";
			 				$borderTopArr[$SubSubjectID] = 1;
			 			}else{
			 				$css_border_top = "";
			 			}
			 		}
			 		
			 		foreach((array)$SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
			 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='{$css_component_text}'>$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
		
 		return array($x, $borderTopArr);
	}
	
	#Modified by Josephine (2009/11/16) No Signature Table for Holy Trinity College
	function getSignatureTable($ReportID='')
	{
 		/*global $eReportCard, $eRCTemplateSetting;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
		}
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			$IssueDate = ($SettingID == "IssueDate" && $Issued)	? "<u>".$Issued."</u>" : "__________";
			$SignatureTable .= "<td valign='bottom' align='center'>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td align='center' class='small_title' height='130' valign='bottom'>_____". $IssueDate ."_____</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;*/
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];

		# Get Grand Average Grading Scheme
		$SchemeIDArr = $this->Get_Grand_Mark_Grading_Scheme($ClassLevelID,$ReportID);
		$AvgSchemeID = $SchemeIDArr['-1']['SchemeID'];

		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		$reportColumnAssoAry = BuildMultiKeyAssoc($this->returnReportTemplateColumnData($ReportID), 'ReportColumnID');
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $this->Get_Student_ClassName($StudentID);
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID, '', 0, 1);
		
		$GrandTotal = $StudentID ? $this->Get_Score_Display_HTML($result['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		
		// Handle Average Grade
		$GrandAverageMark = $result['GrandAverage'];
		$adjustedAvgGrade = $result['GrandAverageGrade'];
		$GrandAverage = !empty($adjustedAvgGrade)? $adjustedAvgGrade : $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($AvgSchemeID, $GrandAverageMark, $ReportID, $StudentID, -1, $ClassLevelID, 0);
		$GrandAverage = $StudentID ? $this->Get_Score_Display_HTML($GrandAverage, $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
		
		$ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
  		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
  		$ClassNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
  		
  		$FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
		
		//if ($CalOrder == 2) {
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				$_yearTermId = $reportColumnAssoAry[$ColumnID]['SemesterNum'];
				$_termReportAry = $this->returnReportTemplateBasicInfo('', $others='', $ClassLevelID, $_yearTermId);
				$_termReportId = $_termReportAry['ReportID'];
				
				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID, "", 1, 1);
				$TermColumnResult = $this->getReportResultScore($_termReportId, 0, $StudentID, "", 0, 1);
				
				$columnTotal[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandTotal'], $_termReportId, $ClassLevelID, '', '', 'GrandTotal') : "S";
				
				// Handle Column Average Grade (Adjusted in Term Report)
				//$columnResult['GrandAverage'] = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($AvgSchemeID, $columnResult['GrandAverage'], $_termReportId, $StudentID, -1, $ClassLevelID, 0);
				$columnAvgGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($AvgSchemeID, $columnResult['GrandAverage'], $_termReportId, $StudentID, -1, $ClassLevelID, 0);
				$adjustedAvgGrade = $TermColumnResult['GrandAverageGrade'];
				$columnResult['GrandAverage'] = !empty($adjustedAvgGrade)? $adjustedAvgGrade : $columnAvgGrade;
				$columnAverage[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandAverage'], $_termReportId, $ClassLevelID, '', '', 'GrandAverage') : "S";
				
				$columnClassPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $_termReportId, $ClassLevelID, '', '', 'ClassPosition') : "S";
				$columnFormPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $_termReportId, $ClassLevelID, '', '', 'FormPosition') : "S";
				$columnClassNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $_termReportId, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
				$columnFormNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $_termReportId, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";
			}
		//}
		
		$first = 1;
		
		if ($eRCTemplateSetting['HideCSVInfo'] != true)
		{
			# Average Mark 
			if($ShowGrandAvg)
			{
				$thisTitleEn = $eReportCard['Template']['AvgMarkEn'];
				$thisTitleCh = $eReportCard['Template']['AvgMarkCh'];
				if($ReportType == "W"){$ColNum2=4;}
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnAverage, $GrandAverage, $first);
				$x .= $this->Generate_Footer_Star_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment);
				$first = 0;
			
			}
				
			##################################################################################
			# CSV related
			##################################################################################
			# build data array
			/*
			$ary = array();
			$csvType = $this->getOtherInfoType();
			
			//modified by Marcus 20/8/2009
			$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID,$StudentID);
			$ary=$OtherInfoDataAry[$StudentID];
			
			if($SemID=="F")
			{
				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
				
				# calculate sems/assesment col#
				$ColNum2Ary = array();
				foreach((array)$ColumnData as $k1=>$d1)
				{
					$TermID = $d1['SemesterNum'];
					if($d1['IsDetails']==1)
					{
						# check sems/assesment col#
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
						$thisReportID = $thisReport['ReportID'];
						$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
						$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1;
					}
					else
						$ColNum2Ary[$TermID] = 0;
				}
			}
			*/
		}
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array(), $borderTopArr=array())
	{
		global $eReportCard;				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach((array)$MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 1, $ReportID);
		
		if($ReportType=="T")	# Temrs Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
							
		
			foreach((array)$SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;	
					$align_para = "center";
					$padding_right = '';
				}
				else
				{
					$align_para = "right";
					$padding_right = "padding_right";
				}
				
				# define css
				if($isSub) {
					$css_border_top = "";
					$css_component_text = "component_text";
					
				}else{
					$css_border_top = "border_top";
					$css_component_text = "tabletext";
					if($borderTopArr[$SubjectID]==1){
			 				$css_border_top = "border_top";
			 			}else{
			 				$css_border_top = "";
			 		}
				}
				
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				$FormNumOfStudent = $MarksAry[$SubjectID][0]['FormNoOfStudent'];
				$FormNumOfStudent = ($FormNumOfStudent == '')? $this->EmptySymbol : $FormNumOfStudent;
				
//				if(!$isSub)
				if($ScaleInput == "G")
				{
					$SubjectFullMark = $SubjectFullMarkAry[$SubjectID];
				}
				else
				{
					$SubjectWeight = $this->returnReportTemplateSubjectWeightData($ReportID, " SubjectID='$SubjectID' ");
					foreach($SubjectWeight as $SubjectWeightInfo)
					{
						if($SubjectWeightInfo['ReportColumnID'] == '' ||$SubjectWeightInfo['ReportColumnID'] == 0 )
						{
							$thisSubjectWeight = $SubjectWeightInfo['Weight'];
							break;
						}
						$thisSubjectWeight = $SubjectWeightInfo['Weight'];
					}				
					$SubjectFullMark = 100* $thisSubjectWeight;
				}
				
				if($isSub){
					$DisplayFormNumOfStudent = $this->EmptySymbol;
				}else{
					$DisplayFormNumOfStudent = $FormNumOfStudent;
				}
				
				$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center'>".$DisplayFormNumOfStudent."</td>";
				$x[$SubjectID] .= "<td class='border_left {$css_border_top} {$css_component_text} $padding_right' align='$align_para'>".$SubjectFullMark."</td>";
			
				$isAllNA = true;
													
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall)
				{
					$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
					$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];

					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
										
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					# check special case
					if ($CalculationMethod==2 && $isSub)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else
					{
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
						if($needStyle)
						{
	 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							else
								$thisMarkTemp = $thisMark;
								
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
						}
						else
							$thisMarkDisplay = $thisMark;
					}
					if($this->Check_If_Grade_Is_SpecialCase($thisMark))
						$thisMarkDisplay = $this->EmptySymbol;	
					
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='{$css_component_text} $padding_right' align='$align_para' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					
  					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
				}
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			$temp_count=-1;
			foreach((array)$SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				
				# Check Category
				$SubjectObj = new subject ($SubjectID);
				$thisLearningCategoryID = $SubjectObj->LearningCategoryID;
				
				if($temp_count==$thisLearningCategoryID){
					$boolean_temp = 0;
				}else{
					$boolean_temp = 1;
				}
				$temp_count=$thisLearningCategoryID;
				
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
					$align_para = "center";
					$padding_right = "";
				}
				else{
					$align_para = "right";
					$padding_right = "padding_right2";
				}
				
				//$css_border_top = ($isSub)? "" : "border_top";
				if($isSub) {
					$css_border_top = "";
					$css_component_text = "component_text";
				}else{
					$css_border_top = "border_top";
					$css_component_text = "tabletext";
					if($borderTopArr[$SubjectID]==1){
			 				$css_border_top = "border_top";
			 			}else{
			 				$css_border_top = "";
			 		}
				}
				$FormNumOfStudent = $MarksAry[$SubjectID][0]['FormNoOfStudent'];
				$FormNumOfStudent = ($FormNumOfStudent == '')? $this->EmptySymbol : $FormNumOfStudent;
				
//				if(!$isSub)
				if($ScaleInput == "G")
				{
					$SubjectFullMark = $SubjectFullMarkAry[$SubjectID];
				}
				else
				{
					$SubjectWeight = $this->returnReportTemplateSubjectWeightData($ReportID, " SubjectID='$SubjectID' ");
					foreach($SubjectWeight as $SubjectWeightInfo)
					{
						if($SubjectWeightInfo['ReportColumnID'] == '' ||$SubjectWeightInfo['ReportColumnID'] == 0 )
						{
							$thisSubjectWeight = $SubjectWeightInfo['Weight'];
							break;
						}
						$thisSubjectWeight = $SubjectWeightInfo['Weight'];
					}				
					$SubjectFullMark = $SubjectFullMarkAry[$SubjectID]* $thisSubjectWeight;
				}
				
				if($isSub){
					$DisplayFormNumOfStudent = $this->EmptySymbol;
				}else{
					$DisplayFormNumOfStudent = $FormNumOfStudent;
				}
			
				$x[$SubjectID] .= "<td class='border_left {$css_border_top} {$css_component_text}' align='center'>".$DisplayFormNumOfStudent."</td>";
				$x[$SubjectID] .= "<td class='border_left {$css_border_top} {$css_component_text} $padding_right' align='$align_para'>".$SubjectFullMark."</td>";
			
				
				$isAllNA = true;
				
				# Terms's Assesment / Terms Result
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
					//Define CSS
					//$css_border_top = $isSub? "" : "border_top";
					if($isSub) {
							$css_border_top = "";
					}else{
						$css_border_top = "border_top";
						if($borderTopArr[$SubjectID]==1){
			 				$css_border_top = "border_top";
			 			}else{
			 			$css_border_top = "";
			 			}
					}
					
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					
						//if ($isParentSubject && $CalculationOrder == 1) {
						//	$thisMarkDisplay = $this->EmptySymbol;
						//} else {
							# See if any term reports available
							$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
							
							# if no term reports, the term mark should be entered directly
							
							if (empty($thisReport)) {
								$thisReportID = $ReportID;
								$MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
								//$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
								
								$thisMSGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"];
								$thisMSMark = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"];
								
								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
									
							} else {
								$thisReportID = $thisReport['ReportID'];
								$MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
								//$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
								
								$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
								$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
								
								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
							}
	 						
							$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
							$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
														
							# for preview purpose
							if(!$StudentID)
							{
								$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
								$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
							}
							# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
							
							$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
							
							if ($thisMark != "N.A.")
							{
								$isAllNA = false;
							}
						
							# check special case
							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
							if($needStyle)
							{
								$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
							}
							
							if($this->Check_If_Grade_Is_SpecialCase($thisMark))
								$thisMarkDisplay = $this->EmptySymbol;	
						//}
							
						$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  					$x[$SubjectID] .= "<td class='{$css_component_text} $padding_right' align='$align_para'>". $thisMarkDisplay ."</td>";
	  					
	  					$x[$SubjectID] .= "</tr></table>";
						$x[$SubjectID] .= "</td>";
				}
				
				# Subject Overall
				if($ShowSubjectOverall)
				{
					$MarksAry = $this->getMarks($ReportID, $StudentID,"","",1);		
					
					$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
					$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
					
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					
					if ($ScaleInput == 'G')
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					//else if ($CalculationMethod==2 && $isSub)
					//{
					//	$thisMarkDisplay = $this->EmptySymbol;
					//}
					else
					{
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
						if($needStyle)
						{
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
						}
						else
							$thisMarkDisplay = $thisMark;
					}
					
					if($this->Check_If_Grade_Is_SpecialCase($thisMark))
						$thisMarkDisplay = $this->EmptySymbol;	
					
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='{$css_component_text} $padding_right' align='$align_para' >". $thisMarkDisplay ."</td>";
  					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
					
					
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
				}
				
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		}	# End Whole Year Report Type
		
		return $returnArr;
	}

	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
		
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";	
 			
		if ($eRCTemplateSetting['HideCSVInfo'] == true)
		{
			return "";
		}
			
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight					= $ReportSetting['LineHeight'];
		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

		//modified by marcus 20/8/2009
		//return: $ReturnArr[$StudentID][$YearTermID][$UploadType] = Value
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID); 
		
		# retrieve the latest Term
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		$SemCounter = 0;
		$SemTitle = array();
		foreach((array)$sems as $TermID=>$TermName)
		{
			$latestTerm = $TermID;
			
			$objYearTerm = new academic_year_term($TermID);
			$SemTitle[$SemCounter]['En'] = $objYearTerm->YearTermNameEN;
			$SemTitle[$SemCounter]['Ch'] = $objYearTerm->YearTermNameB5;
			
			$SemCounter++;
		}
		//$latestTerm++;
	
		# retrieve result data
		$ary = $OtherInfoDataAry[$StudentID][$latestTerm];
		
		$Absence = ($ary['Days Absent'])? $ary['Days Absent'] : 0;
		$Lateness = ($ary['Time Late'])? $ary['Time Late'] : 0;
		$EarlyLeave = ($ary['Early Leave'])? $ary['Early Leave'] : 0;
		$Promotion = ($ary['Promotion'])? $ary['Promotion'] : $this->EmptySymbol;
		$Merits = ($ary['Merits'])? $ary['Merits'] : 0;
		$MinorCredit = ($ary['Minor Credit'])? $ary['Minor Credit'] : 0;
		$MajorCredit = ($ary['Major Credit'])? $ary['Major Credit'] : 0;
		$Demerits = ($ary['Merits'])? $ary['Demerits'] : 0;
		$MinorFault = ($ary['Minor Fault'])? $ary['Minor Fault'] : 0;
		$MajorFault = ($ary['Major Fault'])? $ary['Major Fault'] : 0;
		$Remark = ($ary['Remark'])? $ary['Remark'] : $this->EmptySymbol;
		$ECA = ($ary['ECA'])? $ary['ECA'] : $this->EmptySymbol;
		if (is_array($ECA))
			$ecaList = implode("<br>", $ECA);
		else
			$ecaList = $ECA;
		
		
		$merit = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		$merit .= "<tr>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['Merits'] . ": " . ($ary['Merits'] ? $ary['Merits'] :0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['Demerits'] . ": " . ($ary['Demerits'] ? $ary['Demerits'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorCredit'] . ": " . ($ary['Minor Credit'] ? $ary['Minor Credit'] :0) ."</td>";
		$merit .= "</tr>";
		$merit .= "<tr>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorCredit'] . ": " . ($ary['Major Credit'] ? $ary['Major Credit'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorFault'] . ": " . ($ary['Minor Fault'] ? $ary['Minor Fault'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorFault'] . ": " . ($ary['Major Fault'] ? $ary['Major Fault'] : 0) ."</td>";
		$merit .= "</tr>";
		$merit .= "</table>";
		$remark = $ary['Remark'];
		
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$classteachercomment = $CommentAry[0];
		$eca = $ary['ECA'];
		if (is_array($eca))
			$ecaList = implode("<br>", $eca);
		else
			$ecaList = $eca;
		
			//global $ColNum2Ary, $ColNum2;
			//$border_top = $first ? "border_top" : "";
			$x .= "<br>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" height='60'>";
			# Table Header
			# Conduct 
			if($ReportType === "W") {
				$border_right_valid = "";
				$thisWidth = "116";
			}else{
				$border_right_valid = "border_right";
				$thisWidth = "190";
			}
			
			if($this->GET_FORM_NUMBER($ClassLevelID)!=6)
			{
				$thisTitleEn = $SemTitle[0]['En'];
				$thisTitleCh = $SemTitle[0]['Ch'];
				$x .= "<tr>";
				$x .= "<td width='320'>$nbsp</td><td></td><td align='center' class='border_left {$border_right_valid} border_top tabletext' width='$thisWidth'>".$thisTitleEn." ".$thisTitleCh."</td>";
				
				if($ReportType === "W" ){
					$ColumnData = $this->returnReportTemplateColumnData($ReportID);
					$thisTitleEn = $SemTitle[1]['En'];
					$thisTitleCh = $SemTitle[1]['Ch'];
					$x .= "<td align='center' class=' border_left border_right border_top tabletext' width='116'>".$thisTitleEn." ".$thisTitleCh."</td>";	
				}
				$x .= "</tr>";
			}
			else
			{
				$x .= "<col width='360'><col><col width='$thisWidth'>";
			}
				
			#Josephine (2009/11/18)
			#Information array for Table of Conduct, Times Late and Days Absent
			$ary_CTD = array();
			$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID,$StudentID);
			$ary_CTD=$OtherInfoDataAry[$StudentID];
			
			# Conduct 
			$thisTitleEn = $eReportCard['Template']['ConductEn'];
			$thisTitleCh = $eReportCard['Template']['ConductCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Conduct", $ary_CTD, $hasBorderTop=1, $hasBorderBottom=0);
		
			# Times Late 
			$thisTitleEn = $eReportCard['Template']['TimesLateEn'];
			$thisTitleCh = $eReportCard['Template']['TimesLateCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Time Late", $ary_CTD);
			
			# Days Absent 
			$thisTitleEn = $eReportCard['Template']['DaysAbsentEn'].' / '.$eReportCard['Template']['NoOfSchoolDaysEn'];
			$thisTitleCh = $eReportCard['Template']['DaysAbsentCh'].' / '.$eReportCard['Template']['NoOfSchoolDaysCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Days Absent", $ary_CTD, $hasBorderTop=0, $hasBorderBottom=1);
			
			
			$x .= "</table>";
		
		
		if($AllowClassTeacherComment)
		{
			$x .= "<br>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='60' class='report_border'>";
			$x .= "<tr>";
				$x .= "<td width='50%' valign='top'>";
					# Class Teacher Comment 
					$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td width='100%'>";
					$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
					$x .= "<tr>";
						$x .= "<td class='tabletext border_bottom' style='padding:4px;'>"."&nbsp;".$eReportCard['Template']['ClassTeacherCommentRemark']."</td>";	
					$x .= "</tr>";
					
					$x .= "<tr>";
					$x .= "<td><br></td>";
					$x .= "</tr>";
					
					if($ReportType == "W")
					{
						$remark = array();
						$remark = $this->Get_Report_Remarks($ReportID, $StudentID); 
					}
					else
					{
						$x .= "<tr>";
						$x .= "<td class='tabletext' style='padding-left:20px;'>".nl2br($classteachercomment)."<br><td>";
						$x .= "</tr>";
					}
					
					#Remark (term report:csv , consolidate: progress award + first in + promoted / detained)
					if (is_array($remark) == false)
						$remark = array($remark);
					$numOfRemarks = count($remark);
						
					for ($i=0; $i<$numOfRemarks; $i++)
					{
						$x .= "<tr>";
							$x .= "<td class='tabletext' width='100%' style='padding-left:20px;'>".$remark[$i]."</td>";	
						$x .= "</tr>";
					}
					
					$x .= "<td class='tabletext' style='padding-left:20px;'>".$this->EmptyLineStarSymbol."<br><br><br></td>";
					$x .= "</tr>";
					$x .= "</table>";	
					$x .= "</td></tr></table>";	
				$x .="</td>";
			$x .= "</tr>";
			$x .= "</table>";
		}
		
		return $x;
	}
	
	########### END Template Related

	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr, $hasBorderTop=0, $hasBorderBottom=0)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;

		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$NoOfSchoolDay 				= $ReportSetting['AttendanceDays'];
		
		if ($NoOfSchoolDay == '') {
			$NoOfSchoolDay = $this->EmptySymbol;
		}
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# initialization
		if($hasBorderTop==1){
		$border_top = "border_top";
		}else{$border_top = "";}

		if($hasBorderBottom==1){
			$border_bottom = "border_bottom";
		}else{
			$border_bottom = "";
		}
		$x = "";
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop, $hasBorderBottom);
			
		if($ReportType=="W")	# Whole Year Report
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			$numOfColumn = count($ColumnData);
			
			$thisTotalValue = "";
			$counter = 0;
			foreach((array)$ColumnData as $k1=>$d1)
			{
				# get value of this term
				$TermID = $d1['SemesterNum'];
				
				$thisValue = $StudentID ? ($ValueArr[$TermID][$InfoKey]!=NULL? $ValueArr[$TermID][$InfoKey] : $this->EmptySymbol) : "#";
				
				if ($InfoKey == 'Days Absent') {
					$_termReportInfoAry = $this->returnReportTemplateBasicInfo('', $others='', $ClassLevelID, $TermID);
					$_termNoOfSchoolDay = $_termReportInfoAry['AttendanceDays'];
					
					if ($_termNoOfSchoolDay == '') {
						$_termNoOfSchoolDay = $this->EmptySymbol;
					}
					
					$thisValue .= ' / '.$_termNoOfSchoolDay;
				}
					
				# display this term value
				if($counter== $numOfColumn-1){
					$border_right_para = "border_right";
				}else{
					$border_right_para = "";
				}
				$x .= "<td class='tabletext {$border_top} border_left {$border_bottom} {$border_right_para}' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
				$counter ++;			
			}
		
		}
		else				# Term Report
		{
			# get value of this term
			$thisValue = $StudentID ? ($ValueArr[$SemID][$InfoKey]!=NULL ? $ValueArr[$SemID][$InfoKey] : $this->EmptySymbol) : "#";
			
			if ($InfoKey == 'Days Absent') {
				$thisValue .= ' / '.$NoOfSchoolDay;
			}
			
			# display this term value
			//if($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} {$border_bottom} border_left border_right' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
		}
		
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$SemID 						= $ReportSetting['Semester'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		$CalOrder = $this->Get_Calculation_Setting($ReportID);
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# Get Grand Average Grading Scheme
		$SchemeIDArr = $this->Get_Grand_Mark_Grading_Scheme($ClassLevelID,$ReportID);
		$AvgSchemeID = $SchemeIDArr['-1']['SchemeID'];
		
		$border_top = $isFirst ? "border_top" : "";
		
		# Display the Footer (Average) of the MSTable Modified By Josephine(2009/11/17)
		$x .= "<tr>";
			//$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, $isFirst);
		
		$x = "";
		$border_top = ($isFirst)? "border_top" : "";
		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleEn ."</td></tr>";
			$x .= "</table>";
			
		$x .= "</td>";		
		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleCh ."</td></tr>";
			$x .= "</table>";	
		$x .= "</td>";
	
		
		//if ($CalOrder == 1) {
		//	for($i=0;$i<$ColNum2;$i++) {
		//		
		//		#Full Mark in Footer Average: Modified by Josephine(2009/11/17)
		//		if($i == 1 ){
		//			$DisplayFullMark = "A";
		//		}else{
		//			$DisplayFullMark=$this->EmptySymbol;
		//		}
		//
		//		$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$DisplayFullMark."<br></td>";
		//	}
		//} else {
		
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."<br></td>";
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>A<br></td>";
			
			if ($ReportType == 'W')
			{
				$curColumn = 0;
				foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					for ($i=0; $i<$NumOfAssessment[$curColumn]-1 ; $i++)
					{
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."<br></td>";
					}
					
					$thisValue = $ValueArr[$ColumnID];
					if ($thisValue == '')
						$thisValue = '&nbsp;';
						
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisValue."</td>";
					$curColumn++;
				}
			}	
			
			
		//}
		
		if ($ShowRightestColumn)
		{
			$thisValue = $OverallValue;
			if ($thisValue == '')
				$thisValue = '&nbsp;';
				
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
		}
			
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	//Modified by Josephine(17/11/2009) Display footer of the REMARK Table
	function Generate_Footer_Star_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		$CalOrder = $this->Get_Calculation_Setting($ReportID);
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		$border_top = $isFirst ? "border_top" : "";
		
		$x .= "<tr>";
			//$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, $isFirst);
		
		$x = "";
		$border_top = ($isFirst)? "border_top" : "";
		
		$x .= "<td class='tabletext {$border_top}' colspan='2' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>".$this->EmptyLineStarSymbol."<br><br></td></tr>";
			$x .= "</table>";
		$x .= "</td>";
	
		if ($CalOrder == 1) {
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptyStarSymbol."<br><br></td>";
		} else {
			$curColumn = 0;
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptyStarSymbol."<br><br></td>";
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptyStarSymbol."<br><br></td>";
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				for ($i=0; $i<$NumOfAssessment[$curColumn]-1 ; $i++)
				{
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptyStarSymbol."<br><br></td>";
				}
				
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptyStarSymbol."<br><br></td>";
				$curColumn++;
			}
			
		}
		
		if ($ShowRightestColumn)
		{
			
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptyStarSymbol."<br><br></td>";
		}
			
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0, $hasBorderBottom=0)
	{
		$x = "";
		$border_top = ($hasBorderTop)? "border_top" : "";
		$border_bottom = ($hasBorderBottom)? "border_bottom" : "";
		
		$x .= "<td class='tabletext {$border_top } {$border_bottom} border_left' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext '>". $TitleEn ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";		
		$x .= "<td class='tabletext {$border_top} {$border_bottom}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleCh ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		
		return $x;
	}
	
	function Is_Show_Rightest_Column($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		return $ShowRightestColumn;
	}
	
	function Get_Calculation_Setting($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		return $CalOrder;
	}
	
	function Get_Report_Remarks($ReportID, $StudentID, $IncludePromotion=1)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		
		$remark = array();
		global $eReportCard;
		# teacher comment
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$classteachercomment = $CommentAry[0];
		$remark[] = nl2br($classteachercomment);
		
		# progress award
		$GrandField = $this->Get_Grand_Position_Determine_Field();
		$GrandMarkSubjID = $this->Get_Grand_Field_SubjectID($GrandField);
		$ProgressInfo = $this->Get_Academic_Progress($ReportID, array($GrandMarkSubjID),$StudentID);
		if($ProgressInfo[$StudentID][$GrandMarkSubjID]["AcademicProgressPrize"]==1)
		{
			$remark[] = $eReportCard['ProgressAward'];
		}
		
		#first in ...
		if($ReportType == 'W')
		{
			$FirstPlaceListAry = $this->getMarks($ReportID,$StudentID," AND OrderMeritForm = '1' ",1,1,'','',0);
	
			if(count($FirstPlaceListAry)>0)
			{
				
				foreach($FirstPlaceListAry as $FirstPlaceSubjectID => $FirstPlaceSubjectMarks )
				{
					if(!empty($FirstPlaceSubjectMarks))
					{
						$FirstPlaceSubject[]= $this->GET_SUBJECT_NAME_LANG($FirstPlaceSubjectID, "EN");
					}
				}
				$FirstPlaceSubject = implode(", ",(array)$FirstPlaceSubject);
				if($lastCommaPos = strrpos($FirstPlaceSubject,","))
				{
					
					$FirstPlaceSubject = substr_replace($FirstPlaceSubject," and", $lastCommaPos,1);
				}	
				$remark[] = $eReportCard['FirstIn'].$FirstPlaceSubject.'.';
				
			}
		}
		
		# promoted / detained
		if ($IncludePromotion == 1)
		{
			$PromotionInfo = $this->GET_PROMOTION_INFO($ReportID, $StudentID);
			$PromotionStatusArr = array($eReportCard["Retained"].'.',$eReportCard["Promoted"].'.',$eReportCard["PromotedonTrial"].'.',""); // show none if promotion ==3
			$PromotionStatus = $PromotionStatusArr[$PromotionInfo["$StudentID"]["Promotion"]];
			$remark[] = $PromotionStatus;
		}		
		
		return $remark;
	}
}
?>