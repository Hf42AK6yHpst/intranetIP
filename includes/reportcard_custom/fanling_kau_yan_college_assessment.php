<?php
# Editing by Marcus

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/kiansu_chekiang_college.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		//$this->configFilesType = array("summary","attendance", "merit", "remark", "eca");
		$this->configFilesType = array("summary", "attendance", "merit", "remark", "assessment");
		
		$this->EmptySymbol = "---";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard;
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' style='padding:0; margin:0;'>";
		$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
		$TableTop .= "<tr><td>&nbsp;</td></tr>";
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableTop .= "<tr><td style=\"height:20px;\">&nbsp;</td></tr>";
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td valign=middle align=center style='padding:0; margin:0;'>";
			$x .= "<table width='730px' border='0' cellspacing='0' cellpadding='0' align='center'>";
				$x .= "<tr><td valign='top'>";
					$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
						$x .= "<tr valign='top' height='960px'><td>".$TableTop."</td></tr>";
						$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
					$x .= "</table>";
				$x .= "</td></tr>";	
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		$x = "";
		
		if($ReportID) {
			$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
			$YearClassID = $ReportInfoArr['Semester'];
			
			$AcademicYeraName = $this->GET_ACTIVE_YEAR_NAME();
			$TermNameEn = $this->returnSemesters($YearClassID, 'en');
			$TermNameCh = $this->returnSemesters($YearClassID, 'ch');
			
			$x .= '<table class="font_assessment_12pt" style="width:100%; text-align:center;">'."\r\n";
				$x .= '<tr><td style="height:200px;">&nbsp;</td></tr>'."\r\n";
				$x .= '<tr><td style="font-weight:bold;">'.$eReportCard['Template']['JointTestReportCh'].'</td></tr>'."\r\n";
				$x .= '<tr><td style="font-weight:bold;">'.strtoupper($eReportCard['Template']['JointTestReportEn']).'</td></tr>'."\r\n";
				$x .= '<tr><td>'.$AcademicYeraName.' '.$TermNameCh.' '.$TermNameEn.'</td></tr>'."\r\n";
			$x .= '</table>'."\r\n";
			
		}
		
		return $x;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		$x = '';
		if($ReportID) {
			$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
			$IssueDate = date('Y/m/d', strtotime($ReportInfoArr['Issued']));
			
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$StudentNameEn = ($StudentInfoArr[0]['EnglishName']=='')? $this->EmptySymbol : $StudentInfoArr[0]['EnglishName'];
			$StudentNameCh = ($StudentInfoArr[0]['ChineseName']=='')? $this->EmptySymbol : $StudentInfoArr[0]['ChineseName'];
			$ClassName = ($StudentInfoArr[0]['ClassName']=='')? $this->EmptySymbol : $StudentInfoArr[0]['ClassName'];
			$ClassNumber = ($StudentInfoArr[0]['ClassNumber']=='')? $this->EmptySymbol : $StudentInfoArr[0]['ClassNumber'];
			$DateOfBirth = ($StudentInfoArr[0]['DateOfBirth']=='')? $this->EmptySymbol : date('Y/m/d', strtotime($StudentInfoArr[0]['DateOfBirth']));
			
			
			$x .= '<table class="font_assessment_11pt" style="width:100%; text-align:left;" padding="0px">'."\r\n";
				$x .= '<tr style="vertical-align:top;">'."\r\n";
					// Name
					$x .= '<td style="width:15%;">'.$eReportCard['Template']['StudentNameCh'].'<br />'.$eReportCard['Template']['StudentNameEn'].'</td>'."\r\n";
					$x .= '<td style="width:3%;">:</td>'."\r\n";
//					$x .= '<td style="width:32%;">'.$StudentNameEn.'</td>'."\r\n";
//					$x .= '<td style="width:17%;">'.$StudentNameCh.'</td>'."\r\n";
					$x .= '<td style="width:49%;">'.$StudentNameEn.'&nbsp;&nbsp;&nbsp;&nbsp;'.$StudentNameCh.'</td>'."\r\n";
					
					// Class (Class No.)
					$x .= '<td style="width:20%;">'."\r\n";
						$x .= $eReportCard['Template']['ClassCh'].' ('.$eReportCard['Template']['ClassNumberCh'].')';
						$x .= '<br />'."\r\n";
						$x .= $eReportCard['Template']['ClassEn'].' ('.$eReportCard['Template']['ClassNumberEn'].')';
					$x .= '</td>'."\r\n";
					$x .= '<td style="width:3%;">:</td>'."\r\n";
					$x .= '<td style="width:10%;">'.$ClassName.' ('.$ClassNumber.')</td>'."\r\n";
				$x .= '</tr>'."\r\n";
				
				$TopPadding = 8;
				$x .= '<tr style="vertical-align:top;">'."\r\n";
					// Date of Birth
					$x .= '<td style="padding-top:'.$TopPadding.'px;">'.$eReportCard['Template']['DOBCh'].'<br />'.$eReportCard['Template']['DOBEn'].'</td>'."\r\n";
					$x .= '<td style="padding-top:'.$TopPadding.'px;">:</td>'."\r\n";
					$x .= '<td style="padding-top:'.$TopPadding.'px;">'.$DateOfBirth.'</td>'."\r\n";
					//$x .= '<td style="padding-top:'.$TopPadding.'px;">&nbsp;</td>'."\r\n";
					
					// Date of Issue
					$x .= '<td style="padding-top:'.$TopPadding.'px;">'.$eReportCard['Template']['IssueDate_AssessmentCh'].'<br />'.$eReportCard['Template']['IssueDateEn'].'</td>'."\r\n";
					$x .= '<td style="padding-top:'.$TopPadding.'px;">:</td>'."\r\n";
					$x .= '<td style="padding-top:'.$TopPadding.'px;">'.$IssueDate.'</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			$x .= '</table>'."\r\n";
		}
		return $x;
	}
	
	function getAssessmentMSTable($ReportID, $Assessment, $Display, $StudentID, $StudentRankScoreAry)
	{
		global $eRCTemplateSetting,$eReportCard ;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		### Get Marks
		$MarkArr = $this->getMarks($ReportID);
		
		### Get Subject List to be displayed
		$SubjectIDArr = (array)$Display;
		$numOfSubject = count($SubjectIDArr);
		
		### Get Student Study List
		$StudySubjectIDArr = $this->Get_Student_Studying_SubjectID($ReportID,$StudentID);
		$Display = array_values(array_intersect($Display, $StudySubjectIDArr));
		$numOfDisplay = count($Display);
		
		### Get SubjectID and Code Mapping
		$SubjectCodeIDAssoArr = $this->GET_SUBJECTS_CODEID_MAP($withComponent=0, $mapByCode=0);
		
		### Add Comoponent Subject to the array for Combined Science
		$DisplaySubjectIDArr = array();
		$CombinedSciSubjectCode = $this->Get_Subject_WebSAMSCode_From_Config('CombinedScience');
		for($i=0; $i<$numOfDisplay; $i++) {
			$thisSubjectID = $Display[$i];
			$thisSubjectCode = $SubjectCodeIDAssoArr[$thisSubjectID];
			
			$DisplaySubjectIDArr[] = array('SubjectID' => $thisSubjectID, 'IsComponent' => 0);
			
			if ($thisSubjectCode == $CombinedSciSubjectCode) {
				$thisCmpSubjectInfoArr = $this->GET_COMPONENT_SUBJECT($thisSubjectID, $ClassLevelID, $ParLang='', $ParDisplayType='Desc', $ReportID);
				$thisNumOfCmp = count($thisCmpSubjectInfoArr);
				for ($j=0; $j<$thisNumOfCmp; $j++) {
					$thisCmpSubjectID = $thisCmpSubjectInfoArr[$j]['SubjectID'];
					$DisplaySubjectIDArr[] = array('SubjectID' => $thisCmpSubjectID, 'IsComponent' => 1, 'ParentSubjectID' => $thisSubjectID);
				}
			}
		}
		$numOfDisplaySubject = count($DisplaySubjectIDArr);
		
		
		$FirstTdLeftPadding = 25;
		
		$x = '';
		$x .= '<table width="100%" align="center" border="0" cellspacing="0" cellpadding="4" class="report_border font_assessment_11pt">'."\r\n";
			$x .= '<tr>'."\r\n";
				$x .= '<td class="grey_bg" style="width:20%; padding-left:'.$FirstTdLeftPadding.'px; font-weight:bold;">'.$eReportCard['Template']['TestSubjectsCh'].'</td>'."\r\n";
				$x .= '<td class="grey_bg" style="width:35%; font-weight:bold;">'.$eReportCard['Template']['TestSubjectsEn'].'</td>'."\r\n";
				$x .= '<td class="grey_bg border_left" style="width:45%; text-align:center; font-weight:bold;">'.$eReportCard['Template']['ResultsCh'].' '.$eReportCard['Template']['ResultsEn'].'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			
			$IsFirstSubject = true;
			for($i=0; $i<$numOfDisplaySubject; $i++) {
				$thisSubjectID = $DisplaySubjectIDArr[$i]['SubjectID'];
				$thisIsComponent = $DisplaySubjectIDArr[$i]['IsComponent'];
				
				$thisSubjectNameCh = $this->GET_SUBJECT_NAME_LANG($thisSubjectID, 'ch'); 
				$thisSubjectNameEn = $this->GET_SUBJECT_NAME_LANG($thisSubjectID, 'en');
				
				if ($thisIsComponent) {
					$thisParentSubjectID = $DisplaySubjectIDArr[$i]['ParentSubjectID'];
					$thisReportColumnID = $Assessment[$thisParentSubjectID];
					$thisLeftPadding = $FirstTdLeftPadding + 15;
					//$thisSubjectEnAlign = 'right';
					$thisSubjectEnAlign = 'left';
					$thisFontClass = 'font_assessment_9pt';
				}
				else {
					$thisReportColumnID = $Assessment[$thisSubjectID];
					$thisLeftPadding = $FirstTdLeftPadding;
					$thisSubjectEnAlign = 'left';
					$thisFontClass = '';
				}
				
				$thisMark = $MarkArr[$StudentID][$thisSubjectID][$thisReportColumnID]['Mark'];
				$thisGrade = $MarkArr[$StudentID][$thisSubjectID][$thisReportColumnID]['Grade'];
				
				$thisScore = '';
				if ($thisGrade == 'N.A.') {
					continue;
				}
				else if ($thisGrade != '') {
					$thisScore = $thisGrade;
				}
				else {
					$thisScore = $thisMark;
				}
				
				list($thisScore, $needStyle) = $this->checkSpCase($ReportID, $thisSubjectID, $thisScore, $thisGrade);
				if ($needStyle) {
					$thisMarkDisplay = $this->Get_Score_Display_HTML($thisScore, $ReportID, $ClassLevelID, $thisSubjectID, $thisScore);
				}
				else {
					$thisMarkDisplay = $thisScore;
				}
				$thisMarkDisplay = ($thisMarkDisplay=='')? $this->EmptySymbol : $thisMarkDisplay;
				
				$thisBorderTop = ($IsFirstSubject)? 'border_top' : ''; 
				$IsFirstSubject = false;
				
				$x .= '<tr>'."\r\n";
					$x .= '<td class="'.$thisBorderTop.' '.$thisFontClass.'" style="padding-left:'.$thisLeftPadding.'px; padding-top:10px; padding-bottom:10px;">'.$thisSubjectNameCh.'</td>'."\r\n";
					$x .= '<td class="'.$thisBorderTop.' '.$thisFontClass.'" style="text-align:'.$thisSubjectEnAlign.';">'.$thisSubjectNameEn.'</td>'."\r\n";
					$x .= '<td class="'.$thisBorderTop.' '.$thisFontClass.' border_left" style="text-align:center;">'.$thisMarkDisplay.'</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			}
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	function getSignatureTable($ReportID, $StudentID, $IssueDate) {
		global $eReportCard;
		
		$ClassTeacherInfoArr = $this->Get_Student_Class_Teacher_Info($StudentID);
		$numOfClassTeacher = count($ClassTeacherInfoArr);
		
		$x = '';
		$x .= '<table class="font_assessment_10pt" width="95%" align="center" border="0" cellspacing="0" cellpadding="2" style="text-align:center;">'."\r\n";
			$x .= '<tr>'."\r\n";
				// Principal
				$x .= '<td class="border_top_thick" style="width:30%;">'.$eReportCard['Template']['AssessmentReport']['PrincipalSignatureEn'].' '.$eReportCard['Template']['AssessmentReport']['PrincipalSignatureCh'].'</td>'."\r\n";
				$x .= '<td style="width:5%;">&nbsp;</td>'."\r\n";
				
				// Co-tutors
				$x .= '<td class="border_top_thick" style="width:30%;">'.$eReportCard['Template']['AssessmentReport']['ClassTeacherSignatureEn'].' '.$eReportCard['Template']['AssessmentReport']['ClassTeacherSignatureCh'].'</td>'."\r\n";
				$x .= '<td style="width:5%;">&nbsp;</td>'."\r\n";
				
				// Parent / Guardian
				$x .= '<td class="border_top_thick" style="width:30%;">'.$eReportCard['Template']['AssessmentReport']['GuardianSignatureEn'].' '.$eReportCard['Template']['AssessmentReport']['GuardianSignatureCh'].'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			
			$x .= '<tr style="vertical-align:top;">'."\r\n";
				// Principal Name
				$x .= '<td>'."\r\n";
					$x .= '<table style="width:70%;" align="center" class="font_assessment_10pt">'."\r\n";
						$x .= '<tr>'."\r\n";
							$x .= '<td style="width:65%; text-align:left;">'.$eReportCard['Template']['PrincipalEn'].'</td>'."\r\n";
							$x .= '<td style="width:35%; text-align:right;">'.$eReportCard['Template']['PrincipalCh'].'</td>'."\r\n";
						$x .= '</tr>'."\r\n";
					$x .= '</table>'."\r\n";
				$x .= '</td>'."\r\n";
				$x .= '<td style="width:5%;">&nbsp;</td>'."\r\n";
				
				// Class Teacher Name
				$x .= '<td>'."\r\n";
					$x .= '<table style="width:95%;" align="center" class="font_assessment_10pt">'."\r\n";
						for ($i=0; $i<$numOfClassTeacher; $i++) {
							$thisNameEn = $ClassTeacherInfoArr[$i]['EnglishName'];
							$thisNameCh = $ClassTeacherInfoArr[$i]['ChineseName'];
							
							$x .= '<tr style="vertical-align:top;">'."\r\n";
								$x .= '<td style="width:70%; text-align:left;">'.$thisNameEn.'</td>'."\r\n";
								$x .= '<td style="width:30%; text-align:right;">'.$thisNameCh.'</td>'."\r\n";
							$x .= '</tr>'."\r\n";
						}
					$x .= '</table>'."\r\n";
				$x .= '</td>'."\r\n";
				$x .= '<td>&nbsp;</td>'."\r\n";
				$x .= '<td>&nbsp;</td>'."\r\n";
				
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
}
?>