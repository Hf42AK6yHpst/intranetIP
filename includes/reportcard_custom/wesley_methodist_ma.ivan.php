<?php
# Editing by ivan

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "attendance");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "-";
		
		$this->OverallResultLowerLimitArr[38] = 'Excellent';
		$this->OverallResultLowerLimitArr[31] = 'VeryGood';
		$this->OverallResultLowerLimitArr[21] = 'Good';
		$this->OverallResultLowerLimitArr[16] = 'Satisfactory';
		$this->OverallResultLowerLimitArr[12] = 'Fair';
		$this->OverallResultLowerLimitArr[06] = 'Weak';
		
		/*
		 * UserID:
		 * 
		 * Principal						83		Chia Loy Tian
		 * Vice-Principal					84		Pearl Moses
		 * Upper Secondary Supervisor		52		Jamaliah Ali
		 * Lower Secondary Supervisor		59		Amaranathan
		 */
		 $this->CanGenerateReportUserIDArr = array(83, 84, 52, 59);
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='') {
		global $eReportCard;
		
		$AcademicResultPage = '';
		$AcademicResultPage .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
		$AcademicResultPage .= "<tr><td>".$this->Get_Empty_Image('60px')."</td></tr>";
		$AcademicResultPage .= "<tr><td>".$TitleTable."</td></tr>";
		$AcademicResultPage .= "<tr><td>&nbsp;</td></tr>";
		$AcademicResultPage .= "<tr><td>".$MSTable."</td></tr>";
		$AcademicResultPage .= "</table>";
		
		$SubjectCommentPage = $this->Get_Subject_Comment_Page($ReportID, $StudentID);
		
		
		$x = '';
		$x .= '<tr>'."\r\n";
			$x .= '<td style="vertical-align:top;">'."\r\n";
				$x .= $AcademicResultPage;
				$x .= $SubjectCommentPage;
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ReportTitle =  $ReportSetting['ReportTitle'];
		$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
		
		$x = '';
		$x .= '<table class="font_11pt" style="width:100%; text-align:center;" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr><td>'.strtoupper($eReportCard['Template']['SchoolNameEn']).'</td></tr>'."\n";
			$x .= '<tr><td>'.strtoupper($eReportCard['Template']['AcademicProgressReportEn']).'</td></tr>'."\n";
			$x .= '<tr><td>'.$ReportTitle.'</td></tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		$FormLevelCode = $this->Get_Student_Form_Level_Code($ClassLevelID);
		$LastGeneratedBy = $ReportInfoArr['LastGeneratedBy'];
		
		$YearTermID = $ReportInfoArr['Semester'];
		$ReportType = ($YearTermID=='F')? 'W' : 'T';
		$ReportTypeCode = $this->Get_School_Internal_Report_Type_Code($ReportID);
		
		$IssueDate = $ReportInfoArr['Issued'];
		
		
		### Get Student Info
		$defaultVal = ($StudentID=='')? "XXX" : '';
		if ($StudentID) {
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$StudentNameEn = $StudentInfoArr[0]['EnglishName'];
			$ClassName = $StudentInfoArr[0]['ClassName'];
			$STRN = $StudentInfoArr[0]['STRN'];
		}
		$StudentNameEn = ($StudentNameEn=='')? $defaultVal : $StudentNameEn;
		$ClassName = ($ClassName=='')? $defaultVal : $ClassName;
		$STRN = ($STRN=='')? $defaultVal : $STRN;
		
		
		### Get Student Marks
		// $MarksArr[$SubjectID][$ReportColumnID][Key] = Data
		$MarksArr = $this->getMarks($ReportID, $StudentID, '', 0, 1);
		// $GrandMarkArr[Key] = Data
		$GrandMarkArr = $this->getReportResultScore($ReportID, 0, $StudentID);
		
		### Get Subject Grading Scheme Info
		$GradingSchemeInfoArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, '', $withGrandResult=0, $returnAsso=1, $ReportID);
		foreach ((array)$GradingSchemeInfoArr as $thisSubjectID => $thisSchemeRangeInfoArr) {
			$FirstSchemeID = $thisSchemeRangeInfoArr['SchemeID'];
		}
		$SchemeRangeInfoArr = $this->GET_GRADING_SCHEME_RANGE_INFO($FirstSchemeID);
		$SchemeGradeArr = Get_Array_By_Key($SchemeRangeInfoArr, 'Grade');
		$numOfSchemeGrade = count($SchemeGradeArr);
		
		### Get Subject Statistics Info
		$MainSubjectInfoArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='en', $ParDisplayType='Desc', $ExcludeCmpSubject=1, $ReportID);
		$MainSubjectIDArr = array_keys($MainSubjectInfoArr);
		$SubjectStatInfoArr = $this->Get_Mark_Statistics_Info($ReportID, $MainSubjectIDArr, $Average_Rounding=2, $SD_Rounding=2, $Percentage_Rounding=2, $SubjectMarkRounding=0, $IncludeClassStat=1);
		
		
		### Get Student csv data
		// $OtherInfoDataArr[$StudentID][$YearTermID][Key] = Data
		$OtherInfoDataArr = $this->getReportOtherInfoData($ReportID, $StudentID);
		$OtherInfoDataArr = $OtherInfoDataArr[$StudentID][$YearTermID];
		
		
		### Get Class Teacehr Comment
		$CommentArr = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$ClassTeacherComment = nl2br(trim($CommentArr[0]));
		
		
		### Signauture and School Chop
		if (in_array($_SESSION['UserID'], $this->CanGenerateReportUserIDArr)) {
			$imgFile = get_website().'/file/reportcard2008/templates/wesley_methodist_ma/signature_'.$LastGeneratedBy.'.jpg';
			$SignatureImage = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:120px;">' : '&nbsp;';
			
			$imgFile = get_website().'/file/reportcard2008/templates/wesley_methodist_ma/chop_'.$LastGeneratedBy.'.png';
			$ChopImage = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:165px;">' : '&nbsp;';
		}
		else {
			$SignatureImage = '&nbsp;';
			$ChopImage = '&nbsp;';
		}
		
		
		### Reorder the Subject Display Order
		$NewSubjectSequenceArr = array();
		$AchievementAssoArr = array();
		$SubjectObjArr = array();
		foreach((array)$MainSubjectInfoArr as $thisSubjectID => $thisSubjectInfoArr)
		{
			$SubjectObjArr[$thisSubjectID] = new subject($thisSubjectID);
			$thisSubjectObj = $SubjectObjArr[$thisSubjectID];
			$thisLearningCategoryID = $thisSubjectObj->LearningCategoryID;
			
			$thisScaleInput = $GradingSchemeInfoArr[$thisSubjectID]['ScaleInput'];
			$thisScaleDisplay = $GradingSchemeInfoArr[$thisSubjectID]['ScaleDisplay'];
			
			
			if ($StudentID) {
				$ReportColumnID = 0;	// Overall Result
				$thisMark = $MarksArr[$thisSubjectID][$ReportColumnID]['Mark'];
				$thisGrade = $MarksArr[$thisSubjectID][$ReportColumnID]['Grade'];
				
				if ($thisGrade == 'N.A.') {
					continue;
				}
				if ($thisGrade!='' && !$this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
					$AchievementAssoArr[$thisGrade]++; 
				}
				
				list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisSubjectID, $thisMark, $thisGrade);
				if($needStyle) {
					$thisNatureDetermineScore = ($thisScaleDisplay=='G')? $thisGrade : $thisMark;
					$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $thisSubjectID, $thisNatureDetermineScore);
					$thisGradeDisplay = $this->Get_Score_Display_HTML($thisGrade, $ReportID, $ClassLevelID, $thisSubjectID, $thisNatureDetermineScore);
				}
				
				if ($thisScaleInput=='G' || $this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
					$thisMarkDisplay = $this->EmptySymbol;
				}
				if ($thisScaleDisplay=='M') {
					$thisGradeDisplay = $this->EmptySymbol;
				}
			}
			else {
				$thisMarkDisplay = $this->EmptySymbol;
				$thisGradeDisplay = $this->EmptySymbol;
			}
			
			
			// $SubjectStatInfoArr[$SubjectID][$YearClassID=0]['MinMark', 'MaxMark', 'Average_Raw']
			$YearClassID = 0;	// Form Statistics
			$NewSubjectSequenceArr[$thisLearningCategoryID][$thisSubjectID]['Highest'] = ($SubjectStatInfoArr[$thisSubjectID][$YearClassID]['MaxMark']=='')? $this->EmptySymbol : $SubjectStatInfoArr[$thisSubjectID][$YearClassID]['MaxMark'];
			$NewSubjectSequenceArr[$thisLearningCategoryID][$thisSubjectID]['Lowest'] = ($SubjectStatInfoArr[$thisSubjectID][$YearClassID]['MinMark']=='')? $this->EmptySymbol : $SubjectStatInfoArr[$thisSubjectID][$YearClassID]['MinMark'];
			$NewSubjectSequenceArr[$thisLearningCategoryID][$thisSubjectID]['Average'] = ($SubjectStatInfoArr[$thisSubjectID][$YearClassID]['Average_Raw']=='')? $this->EmptySymbol : my_round($SubjectStatInfoArr[$thisSubjectID][$YearClassID]['Average_Raw'], 0);
			$NewSubjectSequenceArr[$thisLearningCategoryID][$thisSubjectID]['Mark'] = $thisMarkDisplay;
			$NewSubjectSequenceArr[$thisLearningCategoryID][$thisSubjectID]['Grade'] = $thisGradeDisplay;
			
			$NewSubjectSequenceArr[$thisLearningCategoryID][$thisSubjectID]['SubjectName'] = $MainSubjectInfoArr[$thisSubjectID][0];
		}
		
		$TotalNumOfColumn = ($FormLevelCode=='L')? 8 : 9;
		
		$x = '';
		$x .= '<table cellpadding="0" cellspacing="0" class="font_10pt report_border_left report_border_right report_border_top" style="width:100%; text-align:left; page-break-after:always;">'."\n";
			if ($FormLevelCode == 'L') {
				$x .= '<col width="4%" />'."\n";
				$x .= '<col width="30%" />'."\n";
				$x .= '<col width="11%" />'."\n";
				$x .= '<col width="11%" />'."\n";
				$x .= '<col width="11%" />'."\n";
				$x .= '<col width="11%" />'."\n";
				$x .= '<col width="12%" />'."\n";
				$x .= '<col width="10%" />'."\n";
			}
			else if ($FormLevelCode == 'H') {
				$x .= '<col width="14%" />'."\n";
				$x .= '<col width="4%" />'."\n";
				$x .= '<col width="28%" />'."\n";
				$x .= '<col width="9%" />'."\n";
				$x .= '<col width="9%" />'."\n";
				$x .= '<col width="9%" />'."\n";
				$x .= '<col width="9%" />'."\n";
				$x .= '<col width="10%" />'."\n";
				$x .= '<col width="8%" />'."\n";
			}
			
			
			### Student Info & Report Card Type
			$ReportTypePaddingLeft = 5;
			$Colspan = ($FormLevelCode=='L')? 3 : 4;
			$thisTickDisplay = ($ReportTypeCode=='FirstTermExam') ? $this->Get_Tick_Symbol() : '&nbsp;';
			$x .= '<tr>'."\n";
				$x .= '<td rowspan="4" colspan="'.$Colspan.'" style="height:100%; padding-left:5px;">'."\n";
					$x .= '<table class="font_10pt" style="width:100%; height:100%; text-align:left;" cellpadding="0" cellspacing="0">'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td colpan="2" style="vertical-align:top; padding-top:10px;">'.$eReportCard['Template']['StudentNameEn'].': '.$StudentNameEn.'</td>'."\n";
						$x .= '<tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td style="width:50%; vertical-align:bottom;">'.$eReportCard['Template']['ClassEn'].': '.$ClassName.'</td>'."\n";
							$x .= '<td style="width:50%; vertical-align:bottom;">'.$eReportCard['Template']['FileNoEn'].': '.$STRN.'</td>'."\n";
						$x .= '<tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
				$x .= '<td colspan="4" class="border_left" style="padding-left:'.$ReportTypePaddingLeft.'px;">'.$eReportCard['Template']['FirstTermExaminationEn'].'</td>'."\n";
				$x .= '<td class="border_left" style="text-align:center; vertical-align:bottom;">'.$thisTickDisplay.'</td>'."\n";
			$x .= '</tr>'."\n";
			
			$thisTickDisplay = ($ReportTypeCode=='SecondTermExam') ? $this->Get_Tick_Symbol() : '&nbsp;';
			$x .= '<tr>'."\n";
				$x .= '<td colspan="4" class="border_left border_top" style="padding-left:'.$ReportTypePaddingLeft.'px;">'.$eReportCard['Template']['MidTermExaminationEn'].'</td>'."\n";
				$x .= '<td class="border_left border_top" style="text-align:center; vertical-align:bottom;">'.$thisTickDisplay.'</td>'."\n";
			$x .= '</tr>'."\n";
			
			$thisTickDisplay = ($ReportTypeCode=='PMR_TrialExam' || $ReportTypeCode=='SMR_TrialExam')? $this->Get_Tick_Symbol() : '&nbsp;';
			$thisExamDisplay = ($FormLevelCode=='L')? $eReportCard['Template']['PMRTrialExaminationEn'] : $eReportCard['Template']['SPMTrialExaminationEn'];
			$x .= '<tr>'."\n";
				$x .= '<td colspan="4" class="border_left border_top" style="padding-left:'.$ReportTypePaddingLeft.'px;">'.$thisExamDisplay.'</td>'."\n";
				$x .= '<td class="border_left border_top" style="text-align:center; vertical-align:bottom;">'.$thisTickDisplay.'</td>'."\n";
			$x .= '</tr>'."\n";
			
			$thisTickDisplay = ($ReportTypeCode=='FinalTermExam')? $this->Get_Tick_Symbol() : '&nbsp;';
			$x .= '<tr>'."\n";
				$x .= '<td colspan="4" class="border_left border_top" style="padding-left:'.$ReportTypePaddingLeft.'px;">'.$eReportCard['Template']['FinalTermExaminationEn'].'</td>'."\n";
				$x .= '<td class="border_left border_top" style="text-align:center; vertical-align:bottom;">'.$thisTickDisplay.'</td>'."\n";
			$x .= '</tr>'."\n";
			
			
			### Mark Info
			$x .= '<tr style="text-align:center;">'."\n";
				if ($FormLevelCode == 'H') {
					$x .= '<td rowspan="2" class="border_top">'.$eReportCard['Template']['GroupingEn'].'</td>'."\n";
				}
				$border_left = ($FormLevelCode=='L')? '' : 'border_left';
				$x .= '<td rowspan="2" class="'.$border_left.' border_top">&nbsp;</td>'."\n";
				$x .= '<td rowspan="2" class="border_left border_top">'.$eReportCard['Template']['SubjectsEn'].'</td>'."\n";
				$x .= '<td colspan="4" class="border_left border_top">'.$eReportCard['Template']['OverallMarksForFormEn'].'</td>'."\n";
				$x .= '<td rowspan="2" class="border_left border_top">'.$eReportCard['Template']['StudentMarkEn'].'</td>'."\n";
				$x .= '<td rowspan="2" class="border_left border_top">'.$eReportCard['Template']['GradeEn'].'</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr style="text-align:center;">'."\n";
				$x .= '<td colspan="2" class="border_left border_top">'.$eReportCard['Template']['HighestEn'].'</td>'."\n";
				$x .= '<td class="border_left border_top">'.$eReportCard['Template']['LowestEn'].'</td>'."\n";
				$x .= '<td class="border_left border_top">'.$eReportCard['Template']['AverageEn'].'</td>'."\n";
			$x .= '</tr>'."\n";
			
			$SubjectDisplayCount = 0;
			foreach ((array)$NewSubjectSequenceArr as $thisLearningCategoryID => $thisLearningCategorySubjectArr) {
				$thisIsFirstSubjectInLearningCategory = true;
				$thisLC_Colspan = count($thisLearningCategorySubjectArr);
				
				$thisLearningCategoryObj = new Learning_Category($thisLearningCategoryID);
				$thisLC_NameEn = $thisLearningCategoryObj->NameEng;
				
				foreach ((array)$thisLearningCategorySubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
					$x .= '<tr style="text-align:center;">'."\n";
						// Show Learning Category Info if this is the first Subject
						if ($FormLevelCode == 'H' && $thisIsFirstSubjectInLearningCategory) {
							$x .= '<td rowspan="'.$thisLC_Colspan.'" class="border_top">'.$thisLC_NameEn.'</td>'."\n";
							$thisIsFirstSubjectInLearningCategory = false;
						}
						
						$border_left = ($FormLevelCode=='L')? '' : 'border_left';
						$x .= '<td class="border_top '.$border_left.'" style="text-align:left; padding-left:5px;">'.(++$SubjectDisplayCount).'</td>'."\n";
						$x .= '<td class="border_top border_left" style="text-align:left; padding-left:5px;">'.$thisSubjectInfoArr['SubjectName'].'</td>'."\n";
						$x .= '<td colspan="2" class="border_top border_left">'.$thisSubjectInfoArr['Highest'].'</td>'."\n";
						$x .= '<td class="border_top border_left">'.$thisSubjectInfoArr['Lowest'].'</td>'."\n";
						$x .= '<td class="border_top border_left">'.$thisSubjectInfoArr['Average'].'</td>'."\n";
						$x .= '<td class="border_top border_left">'.$thisSubjectInfoArr['Mark'].'</td>'."\n";
						$x .= '<td class="border_top border_left">'.$thisSubjectInfoArr['Grade'].'</td>'."\n";
					$x .= '</tr>'."\n";
				}
			}
			
			
			### Footer Info
			// Achievement
			$AchievementDisplayArr = array();
			for ($i=0; $i<$numOfSchemeGrade; $i++) {
				$thisGrade = $SchemeGradeArr[$i];
				$thisGradeCount = $AchievementAssoArr[$thisGrade];
				
				if ($thisGradeCount != '' && $thisGradeCount > 0) {
					$AchievementDisplayArr[] = $thisGradeCount.$thisGrade;
				}
			}
			$AchievementDisplay = Get_Table_Display_Content(implode(' ', (array)$AchievementDisplayArr));
			
			$LeftPadding = 5;
			$x .= '<tr>'."\n";
				$x .= '<td colspan="'.$TotalNumOfColumn.'" class="border_top">'."\n";
					$x .= '<table class="font_10pt" style="width:100%; text-align:left;" cellpadding="0" cellspacing="0">'."\n";
						// Total Mark and Achievement
						$x .= '<tr>'."\n";
							$x .= '<td style="width:60%; padding-left:'.$LeftPadding.'px;">'.$eReportCard['Template']['TotalMarkEn'].': '.$GrandMarkArr['GrandTotal'].'</td>'."\n";
							$x .= '<td>'.$eReportCard['Template']['AchievementEn'].': '.$AchievementDisplay.'</td>'."\n";
						$x .= '</tr>'."\n";
						// Percentage and GPMP
						$x .= '<tr>'."\n";
							$x .= '<td style="padding-left:'.$LeftPadding.'px;">'.$eReportCard['Template']['PercentageEn'].': '.$GrandMarkArr['GrandAverage'].'</td>'."\n";
							$x .= '<td>'.$eReportCard['Template']['GPMPEn'].': '.$GrandMarkArr['GPA'].'</td>'."\n";
						$x .= '</tr>'."\n";
						// No of Subject Taken and Overall Results
						$x .= '<tr>'."\n";
							$x .= '<td style="padding-left:'.$LeftPadding.'px;">'.$eReportCard['Template']['NoOfSubjectTakenEn'].': '.$SubjectDisplayCount.'</td>'."\n";
							if ($FormLevelCode == 'L') {
								$OverallDisplay = $this->Get_Overall_Result_Display($ReportID, $StudentID);
								$OverallDisplay = ($OverallDisplay=='')? '&nbsp;' : $OverallDisplay;
								
								$x .= '<td>'.$eReportCard['Template']['OverallResultsEn'].': '.$OverallDisplay.'</td>'."\n";
							}
							else {
								$x .= '<td>&nbsp;</td>'."\n";
							}
						$x .= '</tr>'."\n";
						// Placement (Class)
						$x .= '<tr>'."\n";
							$thisDisplay = ($GrandMarkArr['OrderMeritClass']=='' || $GrandMarkArr['OrderMeritClass']==-1)? $this->EmptySymbol : $GrandMarkArr['OrderMeritClass'];
							$x .= '<td style="padding-left:'.$LeftPadding.'px;">'.$eReportCard['Template']['PlacementClassEn'].': '.$thisDisplay.'</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			
			### Attendance and Conduct
			$Attendance = ($OtherInfoDataArr['Attendance']=='')? $this->EmptySymbol : $OtherInfoDataArr['Attendance'];
			$TotalNumOfSchoolDay = ($OtherInfoDataArr['Total Number of School Days']=='')? $this->EmptySymbol : $OtherInfoDataArr['Total Number of School Days'];
			$Conduct = ($OtherInfoDataArr['Conduct']=='')? $this->EmptySymbol : $OtherInfoDataArr['Conduct'];
			$x .= '<tr>'."\n";
				$x .= '<td colspan="'.$TotalNumOfColumn.'" class="border_top">'."\n";
					$x .= '<table class="font_10pt" style="width:100%; text-align:left;" cellpadding="0" cellspacing="0">'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td style="width:60%; padding-left:'.$LeftPadding.'px;">'.$eReportCard['Template']['AttendanceEn'].': &nbsp;&nbsp;&nbsp;&nbsp;'.$Attendance.'/'.$TotalNumOfSchoolDay.' '.$eReportCard['Template']['DaysEn'].'</td>'."\n";
							$x .= '<td>'.$eReportCard['Template']['ConductEn'].': '.$Conduct.'</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			
			### Comments, Issue Date and Signature
			$x .= '<tr>'."\n";
				$x .= '<td colspan="'.$TotalNumOfColumn.'" class="border_top border_bottom">'."\n";
					$x .= '<table class="font_10pt" style="width:100%; text-align:left;" cellpadding="0" cellspacing="0">'."\n";
						$x .= '<tr><td colspan="3">&nbsp;</td></tr>'."\n";
						$x .= '<tr>'."\n";
							// Comment and Issue Date
							$x .= '<td style="width:80%; padding-left:'.$LeftPadding.'px;">'."\n";
								$x .= '<table class="font_10pt" style="width:100%; height:276px; text-align:left;" cellpadding="0" cellspacing="0">'."\n";
									$x .= '<tr><td style="height:12px; vertical-align:top;">'.$eReportCard['Template']['CommentsEn'].': </td></tr>'."\n";
									$x .= '<tr><td style="vertical-align:top;">'.$ClassTeacherComment.'</td></tr>'."\n";
									$x .= '<tr><td style="height:110px; vertical-align:top;">'.$eReportCard['Template']['DateEn'].': '.$IssueDate.'</td></tr>'."\n";
								$x .= '</table>'."\n";
							$x .= '</td>'."\n";
							
							// Signature
							$x .= '<td>'."\n";
								$x .= '<table class="font_10pt" style="width:100%; text-align:left;" cellpadding="0" cellspacing="0">'."\n";
									$x .= '<tr><td style="height:100px;">&nbsp;</td></tr>'."\n";
									$x .= '<tr><td class="border_top" style="vertical-align:top; text-align:center;">'.$eReportCard['Template']['HomeroomTeacherEn'].'</td></tr>'."\n";
									$x .= '<tr><td style="height:75px; text-align:center; vertical-align:bottom;">'.$SignatureImage.'</td></tr>'."\n";
									$x .= '<tr><td class="border_top" style="vertical-align:top; text-align:center;">'."\n";
										$x .= $eReportCard['Template']['PrincipalEn'].' /'."\n";
										$x .= '<br />'.$eReportCard['Template']['VicePrincipalEn']."\n";
									$x .= '</td></tr>'."\n";
									$x .= '<tr><td style="text-align:center; vertical-align:top;">'.$ChopImage.'</td></tr>'."\n";
								$x .= '</table>'."\n";
							$x .= '</td>'."\n";
							
							$x .= '<td style="width:4%;">&nbsp;</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	public function Get_Subject_Comment_Page($ReportID, $StudentID) {
		global $Lang, $intranet_httppath;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		
		$MainSubjectInfoArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='en', $ParDisplayType='Desc', $ExcludeCmpSubject=1, $ReportID);
		$MainSubjectIDArr = array_keys($MainSubjectInfoArr);
		$numOfSubject = count($MainSubjectIDArr);
		
		### Get Student Marks
		// $MarksArr[$SubjectID][$ReportColumnID][Key] = Data
		$MarksArr = $this->getMarks($ReportID, $StudentID, '', 0, 1);
		
		### Get Subject Teacehr Comment
		// $CommentArr[$SubjectID] = Data
		$CommentArr = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		
		
		$SubjectCounter = 0;
		$x = '';
		for ($i=0; $i<$numOfSubject; $i++) {
			$thisSubjectID = $MainSubjectIDArr[$i];
			
			$ReportColumnID = 0;	// Overall Result
			$thisMark = $MarksArr[$thisSubjectID][$ReportColumnID]['Mark'];
			$thisGrade = $MarksArr[$thisSubjectID][$ReportColumnID]['Grade'];
			$thisComment = nl2br(trim($CommentArr[$thisSubjectID]));
			
			if ($thisGrade == 'N.A.') {
				continue;
			}
			
			$SubjectCounter++;
			
			if ($SubjectCounter % 2 == 0) {
				// The second Subject of the page
				$thisPageBreakCSS = 'page-break-after:always;';
				$thisTableBorderBottom = '';
			}
			else {
				$thisPageBreakCSS = '';
				$thisTableBorderBottom = 'border_bottom';
			}
			
			$x .= '<table style="width:100%; '.$thisPageBreakCSS.'" class="'.$thisTableBorderBottom.'">'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td style="vertical-align:top;">'."\r\n";
						$x .= $this->Get_Subject_Comment_Page_Header($ReportID, $thisSubjectID);
						$x .= '<br />'."\r\n";
						$x .= $this->Get_Subject_Comment_Page_Content($ReportID, $StudentID, $thisSubjectID, $thisMark, $thisGrade, $thisComment);
						$x .= $this->Get_Subject_Comment_Page_Footer($ReportID, $StudentID, $thisSubjectID);
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			$x .= '</table>'."\r\n";
		}
		
		return $x;
	}
	
	private function Get_Subject_Comment_Page_Header($ReportID, $SubjectID) {
		global $eReportCard;
		
		// Get School Logo
		$imgFile = get_website().'/file/reportcard2008/templates/wesley_methodist_ma.png';
		$SchoolLogoWidth = 80;
		$SchoolLogo = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:'.$SchoolLogoWidth.'px;">' : '&nbsp;';
		
		// Get Subject Name
		$SubjectNameEn = $this->GET_SUBJECT_NAME_LANG($SubjectID, 'en'); 
		
		// Get tick symbol
		$TickSymbol = $this->Get_Tick_Symbol();
		
		// Get Form Type
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$FormLevelCode = $this->Get_Student_Form_Level_Code($ClassLevelID);
		$FormTypeDisplay = ($FormLevelCode=='L')? $eReportCard['Template']['LowerSecondaryEn'] : $eReportCard['Template']['UpperSecondaryEn'];
		
		// Get Report Type
		$ReportTypeCode = $this->Get_School_Internal_Report_Type_Code($ReportID);
		
		$ExamTypeWidth = 45;
		$TickWidth = 5;
		
		$x = '';
		$x .= '<br />'."\r\n";
		$x .= '<br />'."\r\n";
		$x .= '<table style="width:100%;">'."\r\n";
			$x .= '<tr>'."\r\n";
				$x .= '<td style="width:'.($SchoolLogoWidth + 30).'px;">'.$SchoolLogo.'</td>'."\r\n";
				$x .= '<td style="vertical-align:top;">'."\r\n";
					$x .= '<table class="font_12pt" style="width:100%;" cellpadding="0" cellspacing="0">'."\r\n";
						$x .= '<tr>'."\r\n";
							$x .= '<td><b>'.strtoupper($eReportCard['Template']['SchoolNameEn']).'</b></td>'."\r\n";
						$x .= '</tr>'."\r\n";
						$x .= '<tr>'."\r\n";
							$x .= '<td>'.strtoupper($SubjectNameEn.' '.$FormTypeDisplay.' '.$eReportCard['Template']['CommentSheetEn']).'</td>'."\r\n";
						$x .= '</tr>'."\r\n";
						$x .= '<tr>'."\r\n";
							$x .= '<td style="padding-top:8px;">'."\r\n";
								$x .= '<table class="font_11pt border_table" style="width:100%;" cellpadding="0" cellspacing="0">'."\r\n";
									$x .= '<tr>'."\r\n";
										$x .= '<td style="width:'.$ExamTypeWidth.'%; text-align:left; padding-left:5px;">'.$eReportCard['Template']['FirstTermExaminationEn'].'</td>'."\r\n";
										$thisTickDisplay = ($ReportTypeCode=='FirstTermExam')? $TickSymbol : '&nbsp;';
										$x .= '<td style="width:'.$TickWidth.'%;">'.$thisTickDisplay.'</td>'."\r\n";
										
										$thisExamDisplay = ($FormLevelCode=='L')? $eReportCard['Template']['PMRTrialExaminationEn'] : $eReportCard['Template']['SPMTrialExaminationEn'];
										$thisTickDisplay = ($ReportTypeCode=='PMR_TrialExam' || $ReportTypeCode=='SMR_TrialExam')? $TickSymbol : '&nbsp;';
										$x .= '<td style="width:'.$ExamTypeWidth.'%; text-align:left; padding-left:5px;">'.$thisExamDisplay.'</td>'."\r\n";
										$x .= '<td style="width:'.$TickWidth.'%;">'.$thisTickDisplay.'</td>'."\r\n";
									$x .= '</tr>'."\r\n";
									
									$x .= '<tr>'."\r\n";
										$x .= '<td style="text-align:left; padding-left:5px;">'.$eReportCard['Template']['MidTermExaminationEn'].'</td>'."\r\n";
										$thisTickDisplay = ($ReportTypeCode=='SecondTermExam')? $TickSymbol : '&nbsp;';
										$x .= '<td>'.$thisTickDisplay.'</td>'."\r\n";
										
										$x .= '<td style="text-align:left; padding-left:5px;">'.$eReportCard['Template']['FinalTermExaminationEn'].'</td>'."\r\n";
										$thisTickDisplay = ($ReportTypeCode=='FinalTermExam')? $TickSymbol : '&nbsp;';
										$x .= '<td>'.$thisTickDisplay.'</td>'."\r\n";
									$x .= '</tr>'."\r\n";
								$x .= '</table>'."\r\n";
							$x .= '</td>'."\r\n";
						$x .= '</tr>'."\r\n";
					$x .= '</table>'."\r\n";
				$x .= '</td>'."\r\n";
				$x .= '<td style="width:'.($SchoolLogoWidth + 30).'px;">&nbsp;</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_Subject_Comment_Page_Content($ReportID, $StudentID, $SubjectID, $Mark, $Grade, $Comment) {
		global $eReportCard;
		
		if ($StudentID) {
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$StudentNameEn = $StudentInfoArr[0]['EnglishName'];
			$ClassName = $StudentInfoArr[0]['ClassName'];
			$STRN = $StudentInfoArr[0]['STRN'];
		}
		else {
			$defaultVal = 'XXX';
			$StudentNameEn = $defaultVal;
			$ClassName = $defaultVal;
		}
		
		$x = '';
		$x .= '<table class="font_11pt" style="width:100%; height:262px; text-align:left; vertical-align:top;">'."\r\n";
			$x .= '<tr style="vertical-align:top;">'."\r\n";
				$x .= '<td style="width:17%;">'.strtoupper($eReportCard['Template']['StudentNameEn']).' : </td>'."\r\n";
				$x .= '<td style="width:68%;">'.$StudentNameEn.'</td>'."\r\n";
				$x .= '<td style="width:8%;">'.strtoupper($eReportCard['Template']['ClassEn']).' : </td>'."\r\n";
				$x .= '<td style="width:7%;">'.$ClassName.'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$x .= '<tr style="vertical-align:top;">'."\r\n";
				$x .= '<td>'.strtoupper($eReportCard['Template']['MarkEn']).' : </td>'."\r\n";
				$x .= '<td>'.$Mark.'</td>'."\r\n";
				$x .= '<td>'.strtoupper($eReportCard['Template']['GradeEn']).' : </td>'."\r\n";
				$x .= '<td>'.$Grade.'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$x .= '<tr style="vertical-align:top;">'."\r\n";
				$x .= '<td colspan="4">'.strtoupper($eReportCard['Template']['CommentsEn']).' : </td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$x .= '<tr style="vertical-align:top; height:200px; ">'."\r\n";
				$x .= '<td colspan="4">'.$Comment.'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_Subject_Comment_Page_Footer($ReportID, $StudentID, $SubjectID) {
		global $eReportCard;
		
		// Get Issue Date
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$IssueDate = $ReportInfoArr['Issued'];
		
		// Get Subject Teacher
		$SubjectTeacherNameArr = $this->returnSubjectTeacher('', $SubjectID, $ReportID, $StudentID, $FullTeacherInfo=0);
		$SubjectTeacherName = (count((array)$SubjectTeacherNameArr) > 0)? implode('<br />', (array)$SubjectTeacherNameArr) : '&nbsp;';
		
		$x = '';
		$x .= '<table class="font_11pt" style="width:100%; text-align:left; vertical-align:top;">'."\r\n";
			$x .= '<tr style="vertical-align:top;">'."\r\n";
				$x .= '<td style="width:7%;">'.strtoupper($eReportCard['Template']['DateEn']).' : </td>'."\r\n";
				$x .= '<td style="width:50%;">'.$IssueDate.'</td>'."\r\n";
				$x .= '<td style="width:18%;">'.strtoupper($eReportCard['Template']['SubjectTeacherEn']).' : </td>'."\r\n";
				$x .= '<td style="width:25%;">'.$SubjectTeacherName.'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		
	}
	
	function genMSTableColHeader($ReportID)
	{
		
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		
	}
	
	function getSignatureTable($ReportID='')
	{
 		
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		
	}
	
	########### END Template Related
	
	// return "L" for Lower Form, "H" for Higher Form
	private function Get_Student_Form_Level_Code($ClassLevelID) {
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		if ($FormNumber <= 3) {
			return "L";
		}
		else {
			return "H";
		}
	}
	
	private function Get_Overall_Result_Display($ReportID, $StudentID) {
		global $eReportCard, $PATH_WRT_ROOT;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		
		// $MarksArr[$SubjectID][$ReportColumnID][Key] = Data
		$MarksArr = $this->getMarks($ReportID, $StudentID, '', 0, 1);
		
		// $GrandMarkArr[Key] = Data
		$GrandMarkArr = $this->getReportResultScore($ReportID, 0, $StudentID);
		$GPA = $GrandMarkArr['GPA'];
		
		$SubjectWeightDataArr = $this->returnReportTemplateSubjectWeightData($ReportID, $other_condition="", $SubjectIDArr='', $ReportColumnID=0);
		$SubjectWeightDataAssoArr = BuildMultiKeyAssoc($SubjectWeightDataArr, array("SubjectID"), array("Weight"), 1);
		
		$TotalSubjectWeight = 0;
		foreach ((array)$MarksArr as $thisSubjectID => $thisSubjectMarksArr) {
			$thisGrade = $thisSubjectMarksArr[0]['Grade'];
			
			if ($thisGrade!='' && !$this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
				$thisWeight = ($SubjectWeightDataAssoArr[$thisSubjectID])? $SubjectWeightDataAssoArr[$thisSubjectID] : 0;
				$TotalSubjectWeight += $thisWeight;
			}
		}
		
		$OverallResultPoint = $GPA * $TotalSubjectWeight;
		$OverallResultMappingArr = $this->Get_Overall_Result_Lower_Limit_Arr();
		foreach ((array)$OverallResultMappingArr as $thisLowerLimit => $thisOverallResultCode) {
			if ($OverallResultPoint >= $thisLowerLimit) {
				return $this->Get_Overall_Result_Display_By_Code($thisOverallResultCode);
			}
		}
	}
	
	private function Get_Overall_Result_Lower_Limit_Arr() {
		return $this->OverallResultLowerLimitArr;
	}
	
	private static function Get_Overall_Result_Display_By_Code($Code) {
		global $eReportCard;
		return $eReportCard['Template']['OverallResultArr'][$Code];
	}
	
	private function Get_School_Internal_Report_Type_Code($ReportID) {
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$YearTermID = $ReportInfoArr['Semester'];
		$ReportType = ($YearTermID=='F')? 'W' : 'T';
		$TermSequenceNumber = ($YearTermID == 'F')? -1 : $this->Get_Semester_Seq_Number($YearTermID);
		
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		$PMR_ApplicableFormArr = array(3);
		$SMR_ApplicableFormArr = array(5);
		$FinalTerm_ApplicableFormArr = array(1, 2, 4);
		
		if ($TermSequenceNumber==1) {
			$ReportTypeCode = 'FirstTermExam';	// First-Term Examination
		}
		else if ($TermSequenceNumber==2) {
			$ReportTypeCode = 'SecondTermExam';	// Second-Term Examination
		}
		else if ($ReportType=='W' && in_array($FormNumber, $PMR_ApplicableFormArr)) {
			$ReportTypeCode = 'PMR_TrialExam';	// PMR Trial Examination
		}
		else if ($ReportType=='W' && in_array($FormNumber, $SMR_ApplicableFormArr)) {
			$ReportTypeCode = 'SMR_TrialExam';	// SMR Trial Examination
		}
		else if ($ReportType=='W' && in_array($FormNumber, $FinalTerm_ApplicableFormArr)) {
			$ReportTypeCode = 'FinalTermExam';	// Final-Term Examination
		}
		
		return $ReportTypeCode;
	}
}
?>