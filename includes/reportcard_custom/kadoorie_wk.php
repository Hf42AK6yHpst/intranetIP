<?php
# Editing by ivan

include_once($intranet_root."/lang/reportcard_custom/kadoorie_wk.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "merit", "eca", "remark", "attendance");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "--";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard, $PATH_WRT_ROOT;
		
		$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
			$TableTop .= "<tr><td><img src='".$emptyImagePath."' height='90'>&nbsp;</td></tr>";
			$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
			$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
			$TableTop .= "<tr><td>".$MSTable."</td></tr>";
			$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
			$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
			$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='985px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		//return '';
		
		global $eReportCard, $title1, $title2, $intranet_root;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ClassLevelID = $ReportSetting['ClassLevelID'];
			$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
			//$ReportTitle =  $title1."<br>".$title2;
			
			# 嘉道理 (eRC) F1-3 show report title (CRM: 2010-0706-1422)
			//if ($FormNumber==4 || $FormNumber==5)
			//{
				$ReportTitle =  $ReportSetting['ReportTitle'];
				$ReportTitleArr = explode(":_:", $ReportTitle);
				$ReportTitle = $ReportTitleArr[0];
				if ($ReportTitleArr[1] != '')
					$ReportTitle .= $ReportTitleArr[1];			
				
				# get school badge
				$SchoolLogo = GET_SCHOOL_BADGE();
				$imgfile = "/file/reportcard2008/templates/kadoorie_wk.gif";
				$SchoolLogo = ($imgfile != "") ? "<img src=\"{$imgfile}\" width=80 height=80>\n" : "";
					
				# get school name
				//$SchoolName = GET_SCHOOL_NAME();
				$SchoolName = $eReportCard['Template']['SchoolInfo']['SchoolNameEn'].'<br />'.$eReportCard['Template']['SchoolInfo']['SchoolNameCh'];
				
				$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
				if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
		
				$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
				//$TitleTable .= "<tr><td width='200' valign='top'>".$this->Get_School_Extra_Info_Table()."</td>";
				
				if(!empty($ReportTitle) || !empty($SchoolName))
				{
					$TitleTable .= "<td>";
					//if ($HeaderHeight == -1) {
						$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
						//if(!empty($TempLogo))
						//	$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$TempLogo."</td></tr>\n";
						//if(!empty($SchoolName))
						//	$TitleTable .= "<tr><td nowrap='nowrap' class='report_school_title' align='center' height='50' valign='top' style='font-size:14px'><b>".$SchoolName."</b></td></tr>\n";
						if(!empty($ReportTitle))
							$TitleTable .= "<tr><td nowrap='nowrap' class='report_school_title' height='40' align='center' valign='top'>".$ReportTitle."</td></tr>\n";
						$TitleTable .= "</table>\n";
						
					/*} else {
						for ($i = 0; $i < $HeaderHeight; $i++) {
							$TitleTable .= "<br/>";
						}
					}*/
					$TitleTable .= "</td>";
				}
				//$TitleTable .= "<td width='200' align='center'>&nbsp;</td></tr>";
				$TitleTable .= "</table>";
			//}
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo 		= unserialize($ReportSetting['DisplaySettings']);
			$LineHeight 				= $ReportSetting['LineHeight'];
			$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
			$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
			$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
			$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
			$ClassLevelID 				= $ReportSetting['ClassLevelID'];
			
			$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
			
			# retrieve required variables
			$defaultVal = ($StudentID=='')? "XXX" : '&nbsp;';
			//$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			$ObjYear = new academic_year($this->schoolYearID);
			$data['AcademicYear'] = $ObjYear->Get_Academic_Year_Name();
			
			$data['DateOfIssue'] = $ReportSetting['Issued'];
			$data['FormPosition'] = '#';
			$data['ClassPosition'] = '#';
			$data['FormNumOfStudent'] = '#';
			$data['ClassNumOfStudent'] = '#';
			
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] = $lu->UserName2Lang('ch', 2);
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				
				$data['ClassNo'] = $thisClassNumber;
				$data['Class'] = $thisClassName;
				
				$data['StudentNo'] = $thisClassNumber;
				
				/*
				if (is_array($SettingStudentInfo))
				{
					if (!in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo) && ($thisClassNumber != ""))
						$data['Class'] .= " (".$thisClassNumber.")";
				}
				else
				{
					if ($thisClassNumber != "")
						$data['Class'] .= " (".$thisClassNumber.")";
				}
				*/
				
				### change year format from YYYY-mm-dd to dd/mm/YYYY
				$data['DateOfBirth'] = $lu->DateOfBirth;
				if ($data['DateOfBirth'] != '')
				{
					$dateArr = explode('-', $data['DateOfBirth']);
					$data['DateOfBirth'] = $dateArr[2].'/'.$dateArr[1].'/'.$dateArr[0];
				}
				
				$data['Gender'] = $lu->Gender;
				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				$data['StudentAdmNo'] = $data['STRN'];
				
				$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
				
				
				### Position in Class and Level
				$result = $this->getReportResultScore($ReportID, 0, $StudentID, '', 0, 1);
				$ClassPosition 		= $result['OrderMeritClass'];
				$FormPosition 		= $result['OrderMeritForm'];
				$ClassNumOfStudent 	= $result['ClassNoOfStudent'];
				$FormNumOfStudent	= $result['FormNoOfStudent'];
				
				if ( ($ClassPosition == -1) || ($OverallPositionRangeClass != 0 && $ClassPosition > $OverallPositionRangeClass) )
				{
					$ClassPosition = '--';
					$ClassNumOfStudent = '--';
				}
				if ( ($FormPosition == -1) || ($OverallPositionRangeForm != 0 && $FormPosition > $OverallPositionRangeForm) )
				{
					$FormPosition = '--';
					$FormNumOfStudent = '--';
				}
		  		
				$data['ClassPosition'] = $ClassPosition;
				$data['FormPosition'] = $FormPosition;
				$data['ClassNumOfStudent'] = $ClassNumOfStudent;
				$data['FormNumOfStudent'] = $FormNumOfStudent;
			}
			
			
			$defaultInfoArray = array("Name", "STRN", "Class", "ClassNo", "ClassPosition", "FormPosition", "DateOfBirth", "Gender");
			
			$StudentInfoTable .= "";
			if(!empty($defaultInfoArray))
			{
				$count = 0;
				
				for($i=0; $i<sizeof($defaultInfoArray); $i++)
				{
					$SettingID = trim($defaultInfoArray[$i]);
					if(in_array($SettingID, $defaultInfoArray)===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID."En"];
						$Title2 = $eReportCard['Template']['StudentInfo'][$SettingID."Ch"];
						
						if($count % $StudentInfoTableCol == 0) {
							# first cell in the row
							$StudentInfoTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' ><tr>";
							$padding_left = "";
						}
						else
							$padding_left = "padding_left";
							
						if(($count+1) % $StudentInfoTableCol==0)
						{
							# last cell in the row
							$dataWidth = "width='50'";
							$titleWidth = "30";
						}
						else
						{
							$dataWidth = '';
							$titleWidth = '5%';
						}
						
						//$colspan = $SettingID=="Name" ? " colspan='10' " : "";
						$StudentInfoTable .= "<td class='tabletext $padding_left' valign='bottom' height='{$LineHeight}' width='$titleWidth' nowrap>".$Title."</td>";
						
						if($SettingID == "Name") 
						{
							$StudentInfoTable .= "<td class='tabletext solid_bottom_border'  align='center' valign='bottom' >".(($lu->EnglishName)? $lu->EnglishName : $defaultVal ) ."</td>";
							$StudentInfoTable .= "<td class='tabletext' align='center' valign='bottom' height='{$LineHeight}' width='$titleWidth' nowrap>(".$lu->ChineseName.")</td>";
						}
						else if ($SettingID == "ClassPosition")
						{
							$StudentInfoTable .= "<td class='tabletext solid_bottom_border'  align='center' valign='bottom' >";
							
							//if ($FormNumber == 4)
							if ($ShowOverallPositionClass==false)
								$StudentInfoTable .= '&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;&nbsp;&nbsp;&nbsp;';
							else
								$StudentInfoTable .= $data['ClassPosition'].' / '.$data['ClassNumOfStudent'];
								
							$StudentInfoTable .= "</td>";
						}
						else if ($SettingID == "FormPosition")
						{
							$StudentInfoTable .= "<td class='tabletext solid_bottom_border'  align='center' valign='bottom' >";
							
							//if ($FormNumber == 4)
							if ($ShowOverallPositionForm==false)
								$StudentInfoTable .= '&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;&nbsp;&nbsp;&nbsp;';
							else
								$StudentInfoTable .= $data['FormPosition'].' / '.$data['FormNumOfStudent'];
								
							$StudentInfoTable .= "</td>";
						}
						else
						{
							$StudentInfoTable .= "<td class='tabletext solid_bottom_border' align='center' valign='bottom' $dataWidth >".($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
						}
						
						$row2 .= "<td class='tabletext $padding_left' $colspan valign='top' height='{$LineHeight}' width='$titleWidth' nowrap>".$Title2."</td>";	
						$row2 .= "<td>&nbsp;</td>";
						
						if($SettingID=="Name")  
							$row2 .= "<td>&nbsp;</td>";
						
						if(($count+1) % $StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr><tr>".$row2."</tr></table>";
							$row2="";
						}
						$count++;
					}
				}
			}
			//$StudentInfoTable .= "</table>";
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
		
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID,'',0,1);
						
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='4' class='report_border'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
				continue;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				# Subject Teacher Comment
				if ($AllowSubjectTeacherComment) {
					$css_border_top = $isSub?"":"border_top";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
					}
					$DetailsTable .= "</span></td>";
				}
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}
		
		# MS Table Footer
		if ($ReportType == 'T')
			$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		#########################################################
		############## Marks START
		
		if ($ReportType=="T")
		{
			$rowspan = "";
			$border_top = '';
		}
		else
		{
			$rowspan = "rowspan='2'";
			$border_top = 'border_top';
		}
		$colspan = 0;
		$footerColSpan = 0;
		
		# Subject
		$subjectCell = '';
		$subjectCell .= "<td $rowspan colspan='2' valign='middle' align='center' class='small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>";
			$subjectCell .= $eReportCard['Template']['SubjectEn'].'<br />'.$eReportCard['Template']['SubjectCh'];
		$subjectCell .= "</td>";
		
		# Full Mark Column
		$fullMarkCell = '';
		if ($ShowSubjectFullMark)
		{
			$fullMarkCell .= "<td valign='middle' align='center' class='$border_top border_left small_title' height='{$LineHeight}' width='".$eRCTemplateSetting['ColumnWidth']['FullMark']."'>";
				$fullMarkCell .= $eReportCard['Template']['MaximumMarkEn'].'<br />'.$eReportCard['Template']['MaximumMarkCh'];
			$fullMarkCell .= "</td>";
			$colspan++;
		}
		
		# Report Columns
		$columnMarkCell = '';
		if($ReportType=="T")	# Terms Report Type
		{
			# Retrieve Invloved Assesment
			$ColoumnTitleArr = $this->returnReportColoumnTitle($ReportID);
			
			$thisColumnData = $this->returnReportTemplateColumnData($ReportID);
			$thisColumnWeightMap = array();
			for($j=0; $j<sizeof($thisColumnData); $j++) {
				$thisColumnWeightMap[$thisColumnData[$j]["ReportColumnID"]] = $thisColumnData[$j]["DefaultWeight"];
			}
				
			if (count($ColoumnTitleArr) > 0)
			{
				foreach ($ColoumnTitleArr as $thisColumnID => $thisColumnTitle)
				{
					# get column title
					if (strtolower($thisColumnTitle) == 'examination mark')
					{
						$thisTitleDisplay = $eReportCard['Template']['ExaminationMarkEn'].'<br />'.$eReportCard['Template']['ExaminationMarkCh'];
					}
					else if (strtolower($thisColumnTitle) == 'coursework mark')
					{
						$thisTitleDisplay = $eReportCard['Template']['CourseworkMarkEn'].'<br />'.$eReportCard['Template']['CourseworkMarkCh'];
					}
					else
					{
						$thisTitleDisplay = $thisColumnTitle;
					}
					
					# get column weight
					$thisColumnWeight = ($thisColumnWeightMap[$thisColumnID] * 100)."%";
					$thisTitleDisplay .= ' ('.$thisColumnWeight.')';
					
					$columnMarkCell .= "<td valign='middle' align='center' height='{$LineHeight}' class='border_left tabletext' align='center'><b>". $thisTitleDisplay . "</b></td>";
					
					$colspan++;
					$footerColSpan++;
				}
			}
			
		}
		else					# Whole Year Report Type
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
 			$SecondSemIndex = count($ColumnData) - 1;
 			
 			$thisReportArr = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$SecondSemIndex]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID,'','',1);
 			$thisReportID = $thisReportArr['ReportID'];
			$ColoumnTitleArr = $this->returnReportColoumnTitle($thisReportID);
			
			$thisColumnData = $this->returnReportTemplateColumnData($thisReportID);
			$thisColumnWeightMap = array();
			for($j=0; $j<sizeof($thisColumnData); $j++) {
				$thisColumnWeightMap[$thisColumnData[$j]["ReportColumnID"]] = $thisColumnData[$j]["DefaultWeight"];
			}
				
			if (count($ColoumnTitleArr) > 0)
			{
				foreach ($ColoumnTitleArr as $thisColumnID => $thisColumnTitle)
				{
					# get column title
					if (strtolower($thisColumnTitle) == 'examination mark')
					{
						$thisTitleDisplay = $eReportCard['Template']['ExaminationMarkEn'].'<br />'.$eReportCard['Template']['ExaminationMarkCh'];
					}
					else if (strtolower($thisColumnTitle) == 'coursework mark')
					{
						$thisTitleDisplay = $eReportCard['Template']['CourseworkMarkEn'].'<br />'.$eReportCard['Template']['CourseworkMarkCh'];
					}
					else
					{
						$thisTitleDisplay = $thisColumnTitle;
					}
					
					# get column weight
					$thisColumnWeight = ($thisColumnWeightMap[$thisColumnID] * 100)."%";
					$thisTitleDisplay .= ' ('.$thisColumnWeight.')';
					
					$columnMarkCell .= "<td valign='middle' align='center' height='{$LineHeight}' class='$border_top border_left tabletext' align='center'><b>". $thisTitleDisplay . "</b></td>";
					
					$colspan++;
					$footerColSpan++;
				}
			}
		}
		
		# Term Mark
		$termMarkCell = '';
		if ($ShowSubjectOverall)
		{
			$termMarkCell .= "<td valign='middle' align='center' class='$border_top border_left small_title' height='{$LineHeight}' width='".$eRCTemplateSetting['ColumnWidth']['Overall']."'>";
				$termMarkCell .= $eReportCard['Template']['TermMarkEn'].'<br />'.$eReportCard['Template']['TermMarkCh'];
			$termMarkCell .= "</td>";
			
			$colspan++;
			$footerColSpan++;
		}
		
		$x = '';
		$x .= "<tr>";
			$x .= $subjectCell;
			
			if ($ReportType=="W")
			{
					$x .= "<td colspan='$colspan' valign='middle' align='center' class='border_left small_title' height='{$LineHeight}' >";
						$x .= $eReportCard['Template']['SencodTermExaminationEn']."&nbsp;&nbsp;&nbsp;&nbsp;".$eReportCard['Template']['SencodTermExaminationCh'];
					$x .= "</td>";
				$x .= "</tr>";
				$x .= "<tr>";
			}
			
			$x .= $fullMarkCell;
			$x .= $columnMarkCell;
			$x .= $termMarkCell;
			
		$x .= "</tr>";
		
		return array($x, $n, $footerColSpan, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		$css_border_top = ($isFirst)? "border_top" : "";
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = strtoupper($this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN"));
		 				$SubjectChn = strtoupper($this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH"));
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
			 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='tabletext'>$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID='', $StudentID='')
	{
 		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
			$DateOfIssueTimestamp = strtotime($Issued);
			
			$FormatedIssueDate = date('jS M.,Y',$DateOfIssueTimestamp);
		}
		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' valign='top'>\n";
		$SignatureTable .= "<tr>\n";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			$emptyData = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			$IssueDate = ($SettingID == "IssueDate" && $FormatedIssueDate)	? "&nbsp;&nbsp;".$FormatedIssueDate."&nbsp;&nbsp;" : $emptyData;
			$SignatureTable .= "<td valign='top' align='center'>\n";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>\n";
			$SignatureTable .= "<tr><td align='center' class='signature_title border_bottom' valign='top'>". $IssueDate ."</td></tr>\n";
			
			if ($SettingID == "Principal")
			{
				$thisInfoDisplay = '('.$eReportCard['Template']['PrincipalName'].')<br />'.$eReportCard['Template']['Principal'];
			}
			else if ($SettingID == "ClassTeacher")
			{
				if($StudentID)		# retrieve Student Info
				{
					$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
					$thisClassID = $StudentInfoArr[0]['ClassID'];
					
					include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
					$ObjYearClass = new year_class($thisClassID, $GetYearDetail=false, $GetClassTeacherList=true);
					
					# Assuming one class teacher only
					$TeacherArr = $ObjYearClass->ClassTeacherList;
					$thisTeacherID = $TeacherArr[0]['UserID'];
					
					$lu = new libuser($thisTeacherID);
					switch ($lu->Title)
	                {
	                    case 0: $teacherTitle = $eReportCard['Template']['Mr']; break;
	                    case 1: $teacherTitle = $eReportCard['Template']['Miss']; break;
	                    case 2: $teacherTitle = $eReportCard['Template']['Mrs']; break;
	                    case 3: $teacherTitle = $eReportCard['Template']['Ms']; break;
	                    case 4: $teacherTitle = $eReportCard['Template']['Dr']; break;
	                    case 5: $teacherTitle = $eReportCard['Template']['Prof']; break;
	                	default: $teacherTitle = ""; break;
	                }
	                
	                $thisInfoDisplay = '('.$teacherTitle." ".$lu->EnglishName.")<br />".$eReportCard['Template']['ClassTeacher'];
				}
				else
				{
					$thisInfoDisplay = $eReportCard['Template']['ClassTeacherMale'].'<br />'."Mr. XXXXXX";
				}
			}
			else
			{
				$thisInfoDisplay = $Title;
			}
			
			$SignatureTable .= "<tr><td align='center' class='signature_title' valign='top'>".$thisInfoDisplay."&nbsp;</td></tr>\n";
			
			$SignatureTable .= "</table>\n";
			$SignatureTable .= "</td>\n";
		}

		$SignatureTable .= "</tr>\n";
		$SignatureTable .= "</table>\n";
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID, '', 0, 1);
		$GrandTotal = $StudentID ? $result['GrandTotal'] : "S";
		$GrandAverage = $StudentID ? $result['GrandAverage'] : "S";
		
		$GrandTotal = $this->Get_Score_Display_HTML($GrandTotal, $ReportID, $ClassLevelID, '', '', 'GrandTotal');
		$GrandAverage = $this->Get_Score_Display_HTML($GrandAverage, $ReportID, $ClassLevelID, '', '', 'GrandAverage');
		
 		$FormPosition = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? "--": $result['OrderMeritForm']) : "--") : "#";
  		$ClassPosition = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? "--": $result['OrderMeritClass']) : "--") : "#";
  		$ClassNumOfStudent = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? "--": $result['ClassNoOfStudent']) : "--") : "#";
  		$FormNumOfStudent = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? "--": $result['FormNoOfStudent']) : "--") : "#";
		
  		$GrandTotalFullMark = $this->returnGrandTotalFullMark($ReportID, $StudentID, $checkExemption=1, $excludeDisplayGradeSubject=1, $excludeSPFullChecking=1);
  		
		$first = 1;
		
		# Overall Result
		if ($ShowGrandTotal)
		{
			$border_top = ($first)? "border_top" : "";
			
			$thisTitleEn = $eReportCard['Template']['TermTotalEn'];
			$thisTitleCh = $eReportCard['Template']['TermTotalCh'];
			
			$x .= "<tr>";
				$x .= $this->Generate_Info_Title_td($thisTitleEn, $thisTitleCh, $first);
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$GrandTotalFullMark."</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' colspan='$ColNum2'>".$GrandTotal."</td>";
			$x .= "</tr>";
			
			$first = 0;
		}
		
		if ($ShowGrandAvg)
		{
			$border_top = ($first)? "border_top" : "";
			
			$thisTitleEn = strtoupper($eReportCard['Template']['GrandAverageEn']);
			$thisTitleCh = $eReportCard['Template']['GrandAverageCh'];
			
			$x .= "<tr>";
				$x .= $this->Generate_Info_Title_td($thisTitleEn, $thisTitleCh, $first);
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".'100'."</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' colspan='$ColNum2'>".$GrandAverage."</td>";
			$x .= "</tr>";
			
			$first = 0;
		}
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eReportCard;				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		if($ReportType=="T")	# Temrs Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
							
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# define css
				$css_border_top = ($isFirst)? "border_top" : "";
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				$isAllNA = true;
				
				if ($ShowSubjectFullMark)
				{
					//if ($isSub)
					//	$thisDisplay = "&nbsp;";
					//else
						$thisDisplay = $SubjectFullMarkAry[$SubjectID];
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center'>". $thisDisplay ."</td>";
				}
									
				# Assessment Marks & Display
				for($i=0;$i<sizeof($ColumnID);$i++)
				{
					$thisColumnID = $ColumnID[$i];
					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
					
					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
					$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
					
					//if ($isSub && $columnSubjectWeightTemp == 0)
					if ($isSub && $i>0)
					{
						$thisMarkDisplay = '&nbsp;';
					}
					else if ($columnSubjectWeightTemp == 0)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					//else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
					//	$thisMarkDisplay = $this->EmptySymbol;
					//} 
					else {
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisMSGrade = $MarksAry[$SubjectID][$ColumnID[$i]]['Grade'];
						$thisMSMark = $MarksAry[$SubjectID][$ColumnID[$i]]['Mark'];
						
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
						
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						if ($thisMark != "N.A.")
						{
							$isAllNA = false;
						}
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
						
						if ($thisMark == '-1')
						{
							$thisMarkDisplay = $this->EmptySymbol;
						}
						else
						{
							if($needStyle)
							{
//								if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
//								{
//									$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
//								}
//								else
//								{
//									$thisMarkTemp = $thisMark;
//								}
								//$thisMarkTemp = ($ScaleDisplay=="M" && $thisSubjectWeight > 0) ? $thisMark / $thisSubjectWeight : $thisMark;
								$thisMarkTemp = $thisMark;
								$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp, $thisColumnID,'',$ReportID);

								$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
							}
						}
						
					//	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					//	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
					//	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark; 
					}
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center'>". $thisMarkDisplay ."</td>";
  					
  					/*
  					if($ShowSubjectFullMark)
  					{
	  					# check special full mark
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
	  					
						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
					}
					*/
					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}
								
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall)
				{
					if ($isSub)
					{
						$thisMarkDisplay = '&nbsp;';
					}
					else
					{
						$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
						$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];
	
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
						
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						
						#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
											
						if ($thisMark != "N.A.")
						{
							$isAllNA = false;
						}
							
						# check special case
						if ($CalculationMethod==2 && $isSub)
						{
							$thisMarkDisplay = $this->EmptySymbol;
						}
						else
						{
							list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
							if($needStyle)
							{
//		 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
//									$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
//								else
//									$thisMarkTemp = $thisMark;
								//$thisMarkTemp = ($ScaleDisplay=="M" && $thisSubjectWeight > 0) ? $thisMark / $thisSubjectWeight : $thisMark;
								$thisMarkTemp = $thisMark;
		 						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp,'','',$ReportID);
								$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
							}
							else
								$thisMarkDisplay = $thisMark;
						}
					}
					
					
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					
  					/*
  					if($ShowSubjectFullMark)
  					{
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $thisFullMark .")</td>";
					}
					*/
					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
				}
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
							
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				$css_border_top = $isFirst? "border_top" : "";
				$isAllNA = true;
				
				if ($ShowSubjectFullMark)
				{
					$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center'>". $SubjectFullMarkAry[$SubjectID] ."</td>";
				}
				
				# Terms's Assesment / Terms Result
				###Show 2nd term only in this template
				//for($i=0;$i<sizeof($ColumnData);$i++)
				//{
					#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
					$css_border_top = ($isFirst)? "border_top" : "";
					
					$i = count($ColumnData) - 1;
					
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					
					### Must show 2nd term in this template
					//if($isDetails==1)		# Retrieve assesments' marks
					//{
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum'] ."' and ClassLevelID = '".$ClassLevelID."' ");
						
						# if no term reports, CANNOT retrieve assessment result at all
						if (empty($thisReport)) {
							for($j=0;$j<sizeof($ColumnID);$j++) {
								$x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
							}
						} else {
							$thisReportID = $thisReport['ReportID'];
							$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
							$ColumnID = array();
							$ColumnTitle = array();
							foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
							
							$thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);		
								 
							for($j=0;$j<sizeof($ColumnID);$j++)
							{
								$thisColumnID = $ColumnID[$j];
								$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
								$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
								$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
								
								$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
								$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
					
								if ($isSub && $j>0)
								{
									$thisMarkDisplay = '&nbsp;';
								}
								else if ($columnSubjectWeightTemp == 0)
								{
									$thisMarkDisplay = $this->EmptySymbol;
								}
								//else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
								//	$thisMarkDisplay = $this->EmptySymbol;
								//} 
								else {
									$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnID[$j] ."' and SubjectID = '$SubjectID' ");
									$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
									
									$thisMSGrade = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade'];
									$thisMSMark = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Mark'];
									
									$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
									$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
									
									# for preview purpose
									if(!$StudentID)
									{
										$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
										$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
									}
									$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
													
									if ($thisMark != "N.A.")
									{
										$isAllNA = false;
									}
									
									# check special case
									list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
									
									if ($thisMark == '-1')
									{
										$thisMarkDisplay = $this->EmptySymbol;
									}
									else
									{
										if($needStyle)
										{
//											if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
//											{
//												$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
//											}
//											else
//											{
//												$thisMarkTemp = $thisMark;
//											}
											//$thisMarkTemp = ($ScaleDisplay=="M" && $thisSubjectWeight>0) ? $thisMark/$thisSubjectWeight : $thisMark;
											$thisMarkTemp = $thisMark;
											$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp, $thisColumnID,'',$thisReportID);
											$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
										}
										else
										{
											$thisMarkDisplay = $thisMark;
										}
									}
								}
									
								$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
								$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
			  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
			  					
			  					/*
			  					if($ShowSubjectFullMark)
			  					{
				  					$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
									$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
								}
								*/
								
								$x[$SubjectID] .= "</tr></table>";
								$x[$SubjectID] .= "</td>";
							}
						}
					//}
					//else					# Retrieve Terms Overall marks
					//{
						//if ($isParentSubject && $CalculationOrder == 1) {
						//	$thisMarkDisplay = $this->EmptySymbol;
						//} else {
							# See if any term reports available
							$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
							
							# if no term reports, the term mark should be entered directly
							
							if (empty($thisReport)) {
								$thisReportID = $ReportID;
								$MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
								//$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
								
								$thisMSGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"];
								$thisMSMark = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"];
								
								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
									
							} else {
								$thisReportID = $thisReport['ReportID'];
								$MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
								//$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
								
								$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
								$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
								
								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
							}
	 						
							$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
							$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
														
							# for preview purpose
							if(!$StudentID)
							{
								$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
								$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
							}
							# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
							$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
							
							if ($thisMark != "N.A.")
							{
								$isAllNA = false;
							}
						
							# check special case
							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
							
							if ($isSub)
							{
								$thisMarkDisplay = '&nbsp;';
							}
							else
							{
								if($needStyle)
								{
									$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$thisReportID);
		 							//$thisMark = ($ScaleDisplay=="M" && $StudentID) ? my_round($thisMark*$thisSubjectWeight,2) : $thisMark;
									$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
								}
								else
								{
									$thisMarkDisplay = $thisMark;
								}
							}
							
						//}
							
						$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
	  					
	  					/*
	  					if($ShowSubjectFullMark)
	  					{
 							$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1 && $UseWeightedMark) ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID];
							$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
						}
						*/
						
						$x[$SubjectID] .= "</tr></table>";
						$x[$SubjectID] .= "</td>";
					//}
					
				
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		}	# End Whole Year Report Type
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
		
		if ($eRCTemplateSetting['HideCSVInfo'] == true)
		{
			return "";
		}
			
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

		//modified by marcus 20/8/2009
		//return: $ReturnArr[$StudentID][$YearTermID][$UploadType] = Value
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
		
		# retrieve the latest Term
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		foreach($sems as $TermID=>$TermName)
			$latestTerm = $TermID;
			
		# retrieve result data (from 2nd Term csv if consolidated report)
		$ary = $OtherInfoDataAry[$StudentID][$latestTerm];
		
		$Conduct = ($ary['Conduct'])? $ary['Conduct'] : $this->EmptySymbol;
		$ECA_Performance = ($ary['ECA_Performance'])? $ary['ECA_Performance'] : $this->EmptySymbol;
		$Absence = ($ary['Days Absent'])? $ary['Days Absent'] : 0;
		$Lateness = ($ary['Times Late'])? $ary['Times Late'] : 0;
		$MajorMerit = ($ary['Major Merit'])? $ary['Major Merit'] : 0;
		$Merit = ($ary['Merit'])? $ary['Merit'] : 0;
		$Credit = ($ary['Credit'])? $ary['Credit'] : 0;
		$MajorDemerit = ($ary['Major Demerit'])? $ary['Major Demerit'] : 0;
		$Demerit = ($ary['Demerit'])? $ary['Demerit'] : 0;
		$BlackMark = ($ary['Black Mark'])? $ary['Black Mark'] : 0;
		$Promotion = ($ary['Promotion'])? $ary['Promotion'] : $this->EmptySymbol;
		
		
		# retrieve result data (from Whole Year csv if consolidated report)
		//if ($ReportType == 'W')
		//	$latestTerm = 0;
		
		//$ary = $OtherInfoDataAry[$StudentID][$latestTerm];		
				
		
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$classteachercomment = $CommentAry[0];
		
		### Get Highest Average in Class
		if ($StudentID)
		{
			$ClassInfo = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$ClassID = $ClassInfo[0]['ClassID'];
			$HighestAverageClass = $this->Get_Class_Max_Min_Info($ReportID, 0, $ClassID, "GrandAverage", $Order='desc', $includeAdjustedMarks=1);
		}
		else
		{
			$HighestAverageInClass = $this->EmptySymbol;
		}
		
		$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
		
		$x .= "<br />";
		if ($ReportType == 'W')
		{
			### Get Grand Marks
			$result = $this->getReportResultScore($ReportID, 0, $StudentID, '', 0, 1);
			$GrandTotal = $StudentID ? $result['GrandTotal'] : "S";
			$GrandAverage = $StudentID ? $result['GrandAverage'] : "S";
			
			$GrandTotal = $this->Get_Score_Display_HTML($GrandTotal, $ReportID, $ClassLevelID, '', '', 'GrandTotal');
			$GrandAverage = $this->Get_Score_Display_HTML($GrandAverage, $ReportID, $ClassLevelID, '', '', 'GrandAverage');
			
			$columnCounter = 0;
			foreach ($ColumnTitle as $ColumnID => $ColumnName)
			{
				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID, '', 1, 1);
				$columnTotal[$columnCounter] = $StudentID ? $columnResult['GrandTotal'] : "S";
				
				if ($columnTotal[$columnCounter] == '')
				{
					$columnTotal[$columnCounter] = $this->EmptySymbol;
				}
				else
				{
					$columnTotal[$columnCounter] = $this->Get_Score_Display_HTML($columnTotal[$columnCounter], $ReportID, $ClassLevelID, '', '', 'GrandTotal');
				}
				
				$columnCounter++;
			}
		
			# 1st Term Total, 2nd Term Total, Annual Total, Annual Average(%)
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
				$x .= "<tr class='remark_text' valign='bottom'>";
					$x .= $this->Get_CSV_Info_Td($eReportCard['Template']['1stTermTotalEn'], $columnTotal[0], 0);
					$x .= $this->Get_CSV_Info_Td($eReportCard['Template']['2ndTermTotalEn'], $columnTotal[1]);
					$x .= $this->Get_CSV_Info_Td($eReportCard['Template']['AnnualTotalEn'], $GrandTotal);
					$x .= $this->Get_CSV_Info_Td($eReportCard['Template']['AnnualAverageEn'], $GrandAverage);
				$x .= "</tr>";
				$x .= "<tr class='remark_text' valign='top'>";
					$x .= "<td colspan='2'>".$eReportCard['Template']['1stTermTotalCh']."</td>";
					$x .= "<td colspan='2' class='padding_left'>".$eReportCard['Template']['2ndTermTotalCh']."</td>";
					$x .= "<td colspan='2' class='padding_left'>".$eReportCard['Template']['AnnualTotalCh']."</td>";
					$x .= "<td colspan='2' class='padding_left'>".$eReportCard['Template']['AnnualAverageCh']."</td>";
				$x .= "</tr>";
				$x .= "<tr><td colspan='10'><img src='".$emptyImagePath."' height='5'></td></tr>";
			$x .= "</table>";
		}
		
		# Times Late, Days Absent, Conduct, ECA Performance, Highest Average in Class
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr class='remark_text' valign='bottom'>";
				$x .= $this->Get_CSV_Info_Td($eReportCard['Template']['TimesLateEn'], $Lateness, 0);
				$x .= $this->Get_CSV_Info_Td($eReportCard['Template']['DaysAbsentEn'], $Absence);
				$x .= $this->Get_CSV_Info_Td($eReportCard['Template']['ConductEn'], $Conduct);
				//$x .= $this->Get_CSV_Info_Td($eReportCard['Template']['ECAPerformanceEn'], $ECA_Performance);
				$x .= $this->Get_CSV_Info_Td($eReportCard['Template']['HighestAverageInClassEn'], $HighestAverageClass);
			$x .= "</tr>";
			$x .= "<tr class='remark_text' valign='top'>";
				$x .= "<td colspan='2'>".$eReportCard['Template']['TimesLateCh']."</td>";
				$x .= "<td colspan='2' class='padding_left'>".$eReportCard['Template']['DaysAbsentCh']."</td>";
				$x .= "<td colspan='2' class='padding_left'>".$eReportCard['Template']['ConductCh']."</td>";
				//$x .= "<td colspan='2' class='padding_left'>".$eReportCard['Template']['ECAPerformanceCh']."</td>";
				$x .= "<td colspan='2' class='padding_left'>".$eReportCard['Template']['HighestAverageInClassCh']."</td>";
			$x .= "</tr>";
			$x .= "<tr><td colspan='10'><img src='".$emptyImagePath."' height='5'></td></tr>";
		$x .= "</table>";
		
		# Major Merit, Merit, Credit, Major Demerit, Demerit, Black Mark
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr class='remark_text' valign='bottom'>";
				$x .= $this->Get_CSV_Info_Td($eReportCard['Template']['MajorMeritEn'], $MajorMerit, 0);
				$x .= $this->Get_CSV_Info_Td($eReportCard['Template']['MeritEn'], $Merit);
				$x .= $this->Get_CSV_Info_Td($eReportCard['Template']['CreditEn'], $Credit);
				$x .= $this->Get_CSV_Info_Td($eReportCard['Template']['MajorDemeritEn'], $MajorDemerit);
				$x .= $this->Get_CSV_Info_Td($eReportCard['Template']['DemeritEn'], $Demerit);
				$x .= $this->Get_CSV_Info_Td($eReportCard['Template']['BlackMarkEn'], $BlackMark);
			$x .= "</tr>";
			$x .= "<tr class='remark_text' valign='top'>";
				$x .= "<td colspan='2'>".$eReportCard['Template']['MajorMeritCh']."</td>";
				$x .= "<td colspan='2' class='padding_left'>".$eReportCard['Template']['MeritCh']."</td>";
				$x .= "<td colspan='2' class='padding_left'>".$eReportCard['Template']['CreditCh']."</td>";
				$x .= "<td colspan='2' class='padding_left'>".$eReportCard['Template']['MajorDemeritCh']."</td>";
				$x .= "<td colspan='2' class='padding_left'>".$eReportCard['Template']['DemeritCh']."</td>";
				$x .= "<td colspan='2' class='padding_left'>".$eReportCard['Template']['BlackMarkCh']."</td>";
			$x .= "</tr>";
		$x .= "</table>";
		
		# Class Teacher Comment
		$x .= "<br />";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr class='remark_text' valign='top'>";
				$x .= "<td width='21%'>".$eReportCard['Template']['ClassTeacherCommentEn']."<br />".$eReportCard['Template']['ClassTeacherCommentCh']."</td>";
				$x .= "<td width='2%'>:</td>";
				$x .= "<td width='79%'>".nl2br($classteachercomment)."</td>";
			$x .= "</tr>";
		$x .= "</table>";
		
		if ($ReportType == 'W')
		{
			# Remark (Promotion)
			$x .= "<br />";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
				$x .= "<tr class='remark_text' valign='top'>";
					$x .= "<td width='5%'>".$eReportCard['Template']['RemarkEn']."</td>";
					$x .= "<td width='2%'>:</td>";
					$x .= "<td width='94%'>".$Promotion."</td>";
				$x .= "</tr>";
			$x .= "</table>";
		}
		
		
		return $x;
	}
	
	########### END Template Related

	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0)
	{
		$x = "";
		$border_top = ($hasBorderTop)? "border_top" : "";
		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleEn ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleCh ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		
		return $x;
	}
	
	function Is_Show_Rightest_Column($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		return $ShowRightestColumn;
	}
	
	function Get_Calculation_Setting($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		return $CalOrder;
	}
	
	function Get_School_Extra_Info_Table()
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
			
		$table = '';
		$table .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top" class="school_extra_info">';
			$table .= '<tr><td colspan="3">'.$eReportCard['Template']['SchoolInfo']['AddressCh'].'</td></tr>';
			$table .= '<tr><td colspan="3">22 Hoi Fan Road</td></tr>';
			$table .= '<tr><td colspan="3">Tai Kok Tsui, West Kowloon</td></tr>';
			$table .= "<tr><td colspan='3'><img src='".$emptyImagePath."' height='5'></td></tr>";
			$table .= '<tr>';
				$table .= '<td width="10%">Tel No.</td>';
				$table .= '<td width="5%">:</td>';
				$table .= '<td width="85%">2576 1871 (Office)</td>';
			$table .= '</tr>';
			$table .= '<tr>';
				$table .= '<td>&nbsp;</td>';
				$table .= '<td>&nbsp;</td>';
				$table .= '<td>2576 1179 (Staff Rm.)</td>';
			$table .= '</tr>';
			$table .= '<tr>';
				$table .= '<td>Fax No.</td>';
				$table .= '<td>:</td>';
				$table .= '<td>2882 4548</td>';
			$table .= '</tr>';
			$table .= '<tr>';
				$table .= '<td>E-mail</td>';
				$table .= '<td>:</td>';
				$table .= '<td>sekss100@edb.gov.hk</td>';
			$table .= '</tr>';
			$table .= '<tr>';
				$table .= '<td>Website</td>';
				$table .= '<td>:</td>';
				$table .= '<td>http://seksswk.edu.hk</td>';
			$table .= '</tr>';
		$table .= '</table>';
		
		return $table;
	}
	
	function Get_Address_Table()
	{
		global $eReportCard;
		
		if (is_array($eReportCard['Template']['AddressDisplay']) || $eReportCard['Template']['AddressDisplay'] != '')
		{
			$AddressArr = array();
			if (is_array($eReportCard['Template']['AddressDisplay']) == false)
				$AddressArr[] = $eReportCard['Template']['AddressDisplay'];
			else
				$AddressArr = $eReportCard['Template']['AddressDisplay'];
		}
		
		$numOfRow = count($AddressArr);
		$x = '';
		$x .= '<table cellspacing="0" cellpadding="1" border="0">';
			for ($i=0; $i<$numOfRow; $i++)
			{
				$x .= '<tr><td class="reportcard_text">'.$AddressArr[$i].'</td></tr>';
			}
		$x .= '</table>';
		
		return $x;
	}
	
	function Get_CSV_Info_Td($TitleEn, $Value, $needPaddingLeft=1)
	{
		//$titleWidth = "width='5%'";
		$dataWidth = "width='10%'";
		
		if ($needPaddingLeft)
			$class_padding_left = "class='padding_left'";
		
		$x = '';
		$x .= "<td $titleWidth $class_padding_left nowrap>".$TitleEn."</td>";
		$x .= "<td $dataWidth class='border_bottom' align='center'>".$Value."</td>";
		
		return $x;
	}
}
?>