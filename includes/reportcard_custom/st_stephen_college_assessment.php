<?php
# Editing by ivan

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/st_stephen_college.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary","attendance", "merit", "remark", "eca");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "---";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard;
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
		$TableTop .= "<tr><td><div class='school_info_table'>".$TitleTable."</div></td></tr>";
		$TableTop .= "<tr><td><div class='student_info_table'>".$StudentInfoTable."</div></td></tr>";
		$TableTop .= "<tr><td><div class='mstable'>".$MSTable."</div></td></tr>";
		$TableTop .= "<tr><td><div class='remarks_table'>".$eReportCard['ExtraReport']['Remarks']."</div></td></tr>";
		$TableTop .= "</table>";
		
//		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
//		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
//		$TableBottom .= $FooterRow;
//		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td  valign='top'>";
			$x .= "<table class='extra_report' width='622px' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='850px' valign='top'><td>".$TableTop."</td></tr>";
//				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard,$image_path, $LAYOUT_SKIN, $title1,$title2,$title3;
		$TitleTable = "";
		
		if($ReportID)
		{
			
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
//			$ReportTitle =  $ReportSetting['ReportTitle'];
//			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
//			$ReportTitleArr = explode(":_:",$ReportTitle);
			
			# get school badge
//			$SchoolLogo = GET_SCHOOL_BADGE();
			$SchoolLogo = "/file/reportcard2008/templates/st_stephen_logo.jpg";
				
			# get school name
			$SchoolName = GET_SCHOOL_NAME();	
			
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td align='center'><img height='73' src='$SchoolLogo'></td></tr>";
			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<tr>";
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>\n";
						$TitleTable .= "<tr><td nowrap='nowrap' align='center'><div class='school_name_ch'>".$eReportCard['ExtraReport']['SchoolNameCh']."</div><div class='school_name_en'>".$eReportCard['ExtraReport']['SchoolNameEn']."</div><div  class='school_name_extra'>".$eReportCard['ExtraReport']['SchoolNameExtra']."</div></td></tr>\n";
						$TitleTable .= "<tr><td nowrap='nowrap' align='center'>\n";
							$TitleTable .= "<div class='report_title_div'>\n";
								if($title1)
									$TitleTable .= "<div class='report_title_row1'>".$title1."</div>\n";
								if($title2)
									$TitleTable .= "<div class='report_title_row2' >".$title2."</div>\n";
								if($title3)
									$TitleTable .= "<div class='report_title_row2' >".$title3."</div>\n";
								
							$TitleTable .= "</div>\n";
						$TitleTable .= "</td></tr>\n";
						
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
				$TitleTable .= "</tr>";
			}
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting,$image_path,$LAYOUT_SKIN;
		
		if($ReportID)
		{
			# Retrieve Display Settings
//			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentInfoTableCol = 1;
//			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$StudentTitleArray = array("Class", "Name");
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = array("Class", "Name");
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal = ($StudentID=='')? "XXX" : '';
			//$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			$data['AcademicYear'] = $this->GET_ACTIVE_YEAR_NAME();
			
			$data['DateOfIssue'] = $ReportSetting['Issued'];
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] = $lu->UserName2Lang('en', 1);
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				
				$data['ClassNo'] = $thisClassNumber;
				$data['Class'] = $thisClassName;
				
				$data['StudentNo'] = $thisClassNumber;
				
				if (is_array($SettingStudentInfo))
				{
					if (!in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo) && ($thisClassNumber != ""))
						$data['Class'] .= " (".$thisClassNumber.")";
				}
				else
				{
					if ($thisClassNumber != "")
						$data['Class'] .= " (".$thisClassNumber.")";
				}
				
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] = $lu->Gender;
				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				$data['StudentAdmNo'] = $data['STRN'];
				
				$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
			}
			
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if(in_array($SettingID, $SettingStudentInfo)===true)
					{
						$Title = $eReportCard['Template'][$SettingID."En"]."&nbsp;";
						$Title .= $eReportCard['Template'][$SettingID."Ch"];
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
						}
						
						$colspan = $SettingID=="Name" ? " colspan='10' " : "";
						$StudentInfoTable .= "<td class='tabletext student_info' $colspan width='20%' valign='top' height='{$LineHeight}'>".$Title."&nbsp;:&nbsp;";
						$StudentInfoTable .= ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
							
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						} else {
							if($SettingID=="Name")
							{
								$count=-1;
								$StudentInfoTable .= "</tr>";
							}
						}
						$count++;
					}
				}
				$StudentInfoTable .= "</table>";
			}
		}
		return $StudentInfoTable;
	}
	
	/*
	 *	$ReportColumnIDArr[$SubjectID] = $ReportColumnID
	 *	$FullMarkArr[$SubjectID] = $FullMark
	 *	$DisplayArr[$SubjectID] = on
	 */

//	function getAssessmentMSTable2($ReportID, $StudentID='')
//	{
//		global $eRCTemplateSetting, $eReportCard;
//		
//		# Retrieve Display Settings
//		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
//		$LineHeight = $ReportSetting['LineHeight'];
//		$ClassLevelID = $ReportSetting['ClassLevelID'];
////		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
//		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
////		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
//		$ShowSubjectComponent = $ReportSetting['ShowSubjectComponent'];
//		
//		# define 	
//		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
//		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
//		
//		# retrieve SubjectID Array
//		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
//		if (sizeof($MainSubjectArray) > 0)
//			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
//		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
//		if (sizeof($SubjectArray) > 0)
//			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
//		
//		# retrieve marks
//		$MarksAry = $this->getMarks($ReportID, $StudentID,'',0,1);
//						
//		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
//		$sizeofSubjectCol = sizeof($SubjectCol);
//		
//		# retrieve Marks Array
//		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
//		$MarksDisplayAry = $MSTableReturned['HTML'];
//		$isAllNAAry = $MSTableReturned['isAllNA'];
//		
//		# retrieve Subject Teacher's Comment
//		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
//		
//		##########################################
//		# Start Generate Table
//		##########################################
//		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
//		# ColHeader
//		$DetailsTable .= $ColHeader;
//		
//		$isFirst = 1;
//		for($i=0;$i<$sizeofSubjectCol;$i++)
//		{
//			$isSub = 0;
//			$thisSubjectID = $SubjectIDArray[$i];
//			
//			# If all the marks is "*", then don't display
//			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
//			{
//				$Droped = 1;
//				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
//					if($da['Grade']!="*")	$Droped=0;
//			}
//			if($Droped)	continue;
//			
//			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	
//				$isSub=1;
//			if ($ShowSubjectComponent==0 && $isSub)
//				continue;
//			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
//				continue;
//			
//			# check if displaying subject row with all marks equal "N.A."
//			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
//			{
//				$DetailsTable .= "<tr>";
//				# Subject 
//				$DetailsTable .= $SubjectCol[$i];
//				# Marks
//				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
//				# Subject Teacher Comment
//				if ($AllowSubjectTeacherComment) {
//					$css_border_top = $isSub?"":"border_top";
//					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
//						$DetailsTable .= "<td class='tabletext border_left $css_border_top'>";
//						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
//					} else {
//						$DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
//						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
//					}
//					$DetailsTable .= "</span></td>";
//				}
//				
//				$DetailsTable .= "</tr>";
//				$isFirst = 0;
//			}
//		}
//		
//		# MS Table Footer
////		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
//		
//		$DetailsTable .= "</table>";
//		##########################################
//		# End Generate Table
//		##########################################				
//		
//		return $DetailsTable;
//	}
//	
	function getAssessmentMSTable($ReportID, $ReportColumnIDArr, $DisplayArr, $StudentID='', $StudentRankScoreAry='', $DataSource='' )
	{
		global $eRCTemplateSetting, $eReportCard;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		### Get Subject List to be displayed
		$SubjectIDArr = (array)$DisplayArr;
		$numOfSubject = count($SubjectIDArr);
		
		### Get Subject Column
		$SubjectCol_HTML_Arr = $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		
		### Get Subject SubMS Mark
		# $MarkArr[$SubjectID][$ColumnID] = Mark
		
//		if($DataSource=="Report")
//		{
			$MarkArr = $this->getMarks($ReportID, '', '', 0, 1);
			$MarkArr = $MarkArr[$StudentID];
//		}

		$html = '';
		$html .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
			$html .= "<tr>";
				$html .= "<td align='left' class='small_title border_bottom' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['SubjectEn']."'>".$eReportCard['Template']['SubjectEn']."</td>";
				$html .= "<td align='left' class='small_title border_bottom' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['SubjectCh']."'>".$eReportCard['Template']['SubjectCh']."</td>";
				$html .= "<td align='center' class='small_title border_bottom' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['FullMark']."' nowrap>". $eReportCard['Template']['FullMarkEn'] ."&nbsp;". $eReportCard['Template']['FullMarkCh'] ."</td>";
				$html .= "<td align='center' class='small_title border_bottom' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['Mark']."' nowrap>".$eReportCard['Template']['ResultEn']."&nbsp;".$eReportCard['Template']['ResultCh']."</td>";
			$html .= "</tr>";
			
			for ($i=0; $i<$numOfSubject; $i++)
			{
				$thisSubjectID = $SubjectIDArr[$i];
				
//				if($DataSource=="MarkSheet")
//				{
//					$MarkArr = $this->getMarksFromMarksheet($ReportID, $thisSubjectID);
//					$MarkArr = $MarkArr[$StudentID];
//					
//				}	
				
				$thisReportColumnID = $ReportColumnIDArr[$thisSubjectID];
				
//				$thisFullMark = $FullMarkArr[$thisSubjectID];
				$thisFullMark = $this->GET_SUBJECT_FULL_MARK($thisSubjectID,$ClassLevelID,$ReportID);
				
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisSubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				### Get the corresponding mark of the subject
				$thisMSGrade = $MarkArr[$thisSubjectID][$thisReportColumnID]['Grade'];
				$thisMSMark = $MarkArr[$thisSubjectID][$thisReportColumnID]['Mark'];
//				debug_pr($thisSubjectID);
//				debug_pr($MarkArr);
				
				$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
				$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
				
				$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
				
				
				# check special case
				list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisSubjectID, $thisMark);
				
				if($needStyle)
					$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $thisSubjectID);
				else
					$thisMarkDisplay = $thisMark;
					
				
				if (is_numeric($thisMark) && $thisMark < $thisPassMark)
					$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', 'Fail');
				else
					$thisMarkDisplay = $thisMark;
				
				$thisMarkDisplay = ($thisMarkDisplay=='')? $this->EmptySymbol : $thisMarkDisplay;
				
//				$css_border_top = ($i==0)? "border_top" : "";
				$html .= "<tr>";
					$html .= $SubjectCol_HTML_Arr[$thisSubjectID];
					$html .= "<td class='tabletext $css_border_top ' align='center'>".$thisFullMark."</td>";
					$html .= "<td class='tabletext $css_border_top ' align='center'>".$thisMarkDisplay."</td>";
				$html .= "</tr>";
			}
			
		$html .= "</table>";
		
		return $html; 
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID = $SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		//$css_border_top = ($Prefix)? "" : "border_top";
//			 		$css_border_top = ($isFirst)? "border_top" : "";
			 		
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
			 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='5' cellspacing='0'>";
						$t .= "<tr><td height='{$LineHeight}' class='subject_row'>$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[$SubSubjectID] = $t;
					$isFirst = 0;
				}
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID='', $StudentID='', $IssueDate='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			$IssueDateDisplay = ($SettingID == "IssueDate" && $IssueDate)? "<u>".$IssueDate."</u>" : "__________";
			$SignatureTable .= "<td valign='bottom' align='center'>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td align='center' class='small_title' height='130' valign='bottom'>_____". $IssueDateDisplay ."_____</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function Get_Address_Table()
	{
		global $eReportCard;
		
		if (is_array($eReportCard['Template']['AddressDisplay']) || $eReportCard['Template']['AddressDisplay'] != '')
		{
			$AddressArr = array();
			if (is_array($eReportCard['Template']['AddressDisplay']) == false)
				$AddressArr[] = $eReportCard['Template']['AddressDisplay'];
			else
				$AddressArr = $eReportCard['Template']['AddressDisplay'];
		}
		
		$numOfRow = count($AddressArr);
		$x = '';
		$x .= '<table cellspacing="0" cellpadding="2" border="0">';
			for ($i=0; $i<$numOfRow; $i++)
			{
				$x .= '<tr><td class="reportcard_text">'.$AddressArr[$i].'</td></tr>';
			}
		$x .= '</table>';
		
		return $x;
	}

//	function genMSTableColHeader($ReportID)
//	{
//		global $eReportCard, $eRCTemplateSetting;
//		
//		# Retrieve Display Settings
//		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
//		$ClassLevelID = $ReportSetting['ClassLevelID'];
////		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
//		$SemID = $ReportSetting['Semester'];
//		$ReportType = $SemID == "F" ? "W" : "T";
//		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
////		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
//		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
//		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
//		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
//		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
//		$LineHeight = $ReportSetting['LineHeight'];
//		
//		# updated on 08 Dec 2008 by Ivan
//		# if subject overall column is not shown, grand total, grand average... also cannot be shown
//		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
//		$ShowRightestColumn = $ShowSubjectOverall;
//		
//		$n = 0;
//		$e = 0;	# column# within term/assesment
//		$t = array(); # number of assessments of each term (for consolidated report only)
//		#########################################################
//		############## Marks START
//		$row2 = "";
//		# Retrieve Invloved Assesment
//		$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
//		$ColumnID = array();
//		$ColumnTitle = array();
//		if (sizeof($ColoumnTitle) > 0)
//			foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
//			
//		$e = 0;
//		for($i=0;$i<sizeof($ColumnTitle);$i++)
//		{
//			//$ColumnTitleDisplay = convert2unicode($ColumnTitle[$i], 1, 2);
//			$ColumnTitleDisplay = $ColumnTitle[$i];
//			$row1 .= "<td valign='middle' height='{$LineHeight}' class=' small_title border_bottom' align='right'>".$eReportCard['Template']['ResultEn']."&nbsp;".$eReportCard['Template']['ResultCh']."</td>";
//			$n++;
//			$e++;
//		}
//
//		############## Marks END
//		#########################################################
//		$Rowspan = $row2 ? "rowspan='2'" : "";
//
//		if(!$needRowspan)
//			$row1 = str_replace("rowspan='2'", "", $row1);
//		
//		$x = "<tr>";
//		# Subject 
//		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
//		for($i=0;$i<sizeof($SubjectColAry);$i++)
//		{
//			$SubjectEng = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'];
//			$SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];
//			$SubjectTitle = $SubjectColAry[$i];
//			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
//			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
//			$x .= "<td {$Rowspan}  valign='middle' class='small_title border_bottom {$SubjectColAry[$i]}' height='{$LineHeight}' >". $SubjectTitle . "</td>";
//			$n++;
//		}
//		if($ShowSubjectFullMark)
//		{
//			$x .= "<td {$Rowspan}  valign='middle' class='small_title border_bottom' align='center' nowrap>". $eReportCard['Template']['FullMarkEn'] ."&nbsp;". $eReportCard['Template']['FullMarkCh'] ."</td>";
//			$n++;
//		}
//		
//		# Marks
//		$x .= $row1;
//		
//		# Subject Overall 
//		if($ShowRightestColumn)
//		{
//			$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Overall'] ."' class='border_bottom small_title' align='center'>". $eReportCard['Template']['SubjectOverall'] ."</td>";
//			$n++;
//		}
//		else
//		{
//			//$e--;	
//		}
//		
//		# Subject Teacher Comment
//		if ($AllowSubjectTeacherComment) {
//			$x .= "<td {$Rowspan} valign='middle' class='border_left small_title' align='center' width='10%'>".$eReportCard['TeachersComment']."</td>";
//		}
//		
//		$x .= "</tr>";
//		if($row2)	$x .= "<tr>". $row2 ."</tr>";
//		return array($x, $n, $e, $t);
//	}
//	
//	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
//	{
//		global $eReportCard;				
//		# Retrieve Display Settings
//		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
//		$SemID 				= $ReportSetting['Semester'];
// 		$ReportType 		= $SemID == "F" ? "W" : "T";		
// 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
////		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
//		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
//		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
//		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
//		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
//		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
//		
//		# updated on 08 Dec 2008 by Ivan
//		# if subject overall column is not shown, grand total, grand average... also cannot be shown
//		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
//		$ShowRightestColumn = $ShowSubjectOverall;
//		
//		# Retrieve Calculation Settings
//		$CalSetting = $this->LOAD_SETTING("Calculation");
//		$UseWeightedMark = $CalSetting['UseWeightedMark'];
//		$CalculationMethod =  $CalSetting['OrderTerm'] ;
//		
//		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
//		$SubjectDecimal = $StorageSetting["SubjectScore"];
//		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
//		
//		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
//		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
//		if(sizeof($MainSubjectArray) > 0)
//			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
//		
//		$n = 0;
//		$x = array();
//		$isFirst = 1;
//				
//		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
//
//		$CalculationOrder = $CalSetting["OrderTerm"];
//		
//		$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
//		$ColumnID = array();
//		$ColumnTitle = array();
//		if(sizeof($ColoumnTitle) > 0)
//			foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
//						
//		foreach($SubjectArray as $SubjectID => $SubjectName)
//		{
//			$isSub = 0;
//			if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
//			
//			// check if it is a parent subject, if yes find info of its components subjects
//			$CmpSubjectArr = array();
//			$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
//			if (!$isSub) {
//				$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
//				if(!empty($CmpSubjectArr)) $isParentSubject = 1;
//			}
//			
//			# define css
//			$css_border_top = ($isSub)? "" : "border_top";
//	
//			# Retrieve Subject Scheme ID & settings
//			$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
//			$SchemeID = $SubjectFormGradingSettings['SchemeID'];
//			$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
//			$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
//			$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
//			
//			$isAllNA = true;
//								
//			# Assessment Marks & Display
//			for($i=0;$i<sizeof($ColumnID);$i++)
//			{
//				$thisColumnID = $ColumnID[$i];
//				$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
//				$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
//				$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
//				
//				$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
//				$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
//
//				if ($isSub && $columnSubjectWeightTemp == 0)
//				{
//					$thisMarkDisplay = $this->EmptySymbol;
//				}
//				else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
//					$thisMarkDisplay = $this->EmptySymbol;
//				} else {
//					$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
//					$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
//					
//					$thisMSGrade = $MarksAry[$SubjectID][$ColumnID[$i]]['Grade'];
//					$thisMSMark = $MarksAry[$SubjectID][$ColumnID[$i]]['Mark'];
//					
//					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//					
//					# for preview purpose
//					if(!$StudentID)
//					{
//						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
//						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
//					}
//					
//					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
//					
//					if ($thisMark != "N.A.")
//					{
//						$isAllNA = false;
//					}
//					
//					# check special case
//					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
//					
//					if($needStyle)
//					{
//						if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
//						{
//							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
//						}
//						else
//						{
//							$thisMarkTemp = $thisMark;
//						}
//						
//						$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
//					}
//					else
//					{
//						$thisMarkDisplay = $thisMark;
//					}
//					
//				//	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
//				//	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
//				//	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark; 
//				}
//					
//				if($ShowSubjectFullMark)
//				{
//  					# check special full mark
//  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
//  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
//  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
//  					
//					$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
//					$x[$SubjectID] .= "<td class='subject_row' align='center'>". $FullMark ."</td>";
//				}
//				$x[$SubjectID] .= "<td class='subject_row subject_mark' align='right'>". $thisMarkDisplay ."</td>";
//			}
//							
//			# Subject Overall (disable when calculation method is Vertical-Horizontal)
//			if($ShowSubjectOverall)
//			{
//				$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
//				$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];
//
//				$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//				$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//				
//				# for preview purpose
//				if(!$StudentID)
//				{
//					$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
//					$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
//				}
//				
//				#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
//				$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
//									
//				if ($thisMark != "N.A.")
//				{
//					$isAllNA = false;
//				}
//					
//				# check special case
//				if ($CalculationMethod==2 && $isSub)
//				{
//					$thisMarkDisplay = $this->EmptySymbol;
//				}
//				else
//				{
//					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
//					if($needStyle)
//					{
// 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
//							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
//						else
//							$thisMarkTemp = $thisMark;
//							
//						$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
//					}
//					else
//						$thisMarkDisplay = $thisMark;
//				}
//				
//				$x[$SubjectID] .= "<td class=' {$css_border_top}'>";
//				$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
//				if($ShowSubjectFullMark)
//				{
//  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
//  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
//  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
//					$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $thisFullMark .")</td>";
//				}
//				$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
//				$x[$SubjectID] .= "</tr></table>";
//				$x[$SubjectID] .= "</td>";
//			} else if ($ShowRightestColumn) {
//				$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
//			}
//			$isFirst = 0;
//			
//			# construct an array to return
//			$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
//			$returnArr['isAllNA'][$SubjectID] = $isAllNA;
//		}
//		
//		return $returnArr;
//	}		
	
	
}
?>