<?php
# Editing by ivan

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "promotion", "attendance");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "---";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='') {
		global $eReportCard;
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
		$TableTop .= "<tr><td colspan='3'>".$TitleTable."</td></tr>";
		$TableTop .= "<tr><td colspan='3'>".$StudentInfoTable."</td></tr>";
		$TableTop .= "<tr><td width='58%' valign='top'>".$MSTable."</td><td width='1'>&nbsp;</td><td valign='top'>".$MiscTable."</td></tr>";
		$TableTop .= "<tr><td colspan='3'>".$FooterRow."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='950px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		$x = "";
		$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
			$x .= "<tr height='95px' valign='top'><td>&nbsp;</td></tr>";
		$x .= "</table>";
		return $x;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			
			# Retrieve required variables
			$data['AcademicYear'] = $this->GET_ACTIVE_YEAR_NAME("en");
			$data['DateOfIssue'] = $ReportSetting['Issued'];
			if(!is_date_empty($data['DateOfIssue'])) {
				$data['DateOfIssue'] = strtotime($data['DateOfIssue']);
				$data['DateOfIssue'] = date('d/m/Y', $data['DateOfIssue']);
			}
			
			// "Name", "DateOfBirth", "Class", "STRN", "Gender", "DateOfIssue"
			# Retrieve Student Info
			if($StudentID)
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				$lu = new libuser($StudentID);
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				
				$data['Name'] = $lu->UserName2Lang('ch', 2);
				$data['Class'] = $thisClassName." (".$thisClassNumber.")";
				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				$data['Gender'] = $lu->Gender;
				$data['DateOfBirth'] = $lu->DateOfBirth;
				if(!is_date_empty($data['DateOfBirth'])) {
					$data['DateOfBirth'] = strtotime($data['DateOfBirth']);
					$data['DateOfBirth'] = date('d/m/Y', $data['DateOfBirth']);
				}
			}
			
			$StudentInfoTable .= "<table class='student_info_table' width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
				$StudentInfoTable .= "<tr>";
					$StudentInfoTable .= "<td class='tabletext year_row' colspan='4'>".$data['AcademicYear']."</td>";
				$StudentInfoTable .= "</tr>";
				$StudentInfoTable .= "<tr>";
					$StudentInfoTable .= "<td class='tabletext' colspan='4'>&nbsp;</td>";
				$StudentInfoTable .= "</tr>";
				
				$count = 0;
				$defaultVal = ($StudentID=='')? "XXX" : '';
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					$Title = $eReportCard['Template']['StudentInfo'][$SettingID."Ch"]." ".$eReportCard['Template']['StudentInfo'][$SettingID."En"];
					
					$columnIndex = $i * 2;
					$column1Width = $eRCTemplateSetting['ColumnWidth']['StudentInfo'][$columnIndex];
					$column2Width = $eRCTemplateSetting['ColumnWidth']['StudentInfo'][($columnIndex + 1)];
					
					if($count % $StudentInfoTableCol == 0) {
						$StudentInfoTable .= "<tr>";
					}
							$StudentInfoTable .= "<td class='tabletext' width='$column1Width'>".$Title."</td>";
							$StudentInfoTable .= "<td class='tabletext' width='$column2Width'> : ".($data[$SettingID] ? $data[$SettingID] : $defaultVal)."</td>";		
					if(($count + 1) % $StudentInfoTableCol == 0) {
						$StudentInfoTable .= "</tr>";
					}
					
					$count++;
				}
			
			$StudentInfoTable .= "</table>";
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		# Retrieve all Reports
		$ReportList = $this->Get_Report_List($ClassLevelID, '', 1);
		
		# Retrieve Table Header 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		$ColHeader = $ColHeaderAry[0];
		
		# Retrieve Subject Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID="", $SubjectFieldLang="", $ParDisplayType="Desc", $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach((array)$MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach((array)$SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# Retrieve Subject Column
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# Retrieve Marks
		//$MarksAry = $this->getMarks($ReportID, $StudentID, "", 0, 1);
		
		# Retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry="", $StudentID, $NumOfAssessment=array(), $ReportList);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		# Retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		
		$DetailsTable = "<table width='100%' border='1' cellspacing='0' cellpadding='2' class='ms_table'>";
		
		# Report Header
		$DetailsTable .= $ColHeader;
		
		// loop Subjects
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
//			// if all Marks is "*", then don't display
//			if (sizeof((array)$MarksAry[$thisSubjectID]) > 0) {
//				$Droped = 1;
//				foreach((array)$MarksAry[$thisSubjectID] as $cid => $da)
//					if($da['Grade']!="*")	$Droped=0;
//			}
//			if($Droped)	continue;
//			
//			// if hide Components, then don't display
//			if(in_array($thisSubjectID, (array)$MainSubjectIDArray)!=true)	
//				$isSub=1;
//			if ($ShowSubjectComponent==0 && $isSub)
//				continue;
//			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
//				continue;
			
			// Check if Subject Row with all Marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				
				# Subject Column 
				$DetailsTable .= $SubjectCol[$i];
				
				# Subject Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}
		
		# Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2=1, $NumOfAssessment=array(), $ReportList);
		
		$DetailsTable .= "</table>";
		
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
					
		$x = "<tr class='ms_table_header'>";
		
			# Subject 
			$ColHeader = $eReportCard['Template']['SubjectCh']."<br>".$eReportCard['Template']['SubjectEn'];
			$x .= "<td valign='middle' width='".$eRCTemplateSetting['ColumnWidth']['MSHeader'][0]."'>".$ColHeader."</td>";
			$n++;
			
			# Full Mark 
			$ColHeader = $eReportCard['Template']['FullMarkCh']."<br>".$eReportCard['Template']['FullMarkEn'];
			$x .= "<td valign='middle' width='".$eRCTemplateSetting['ColumnWidth']['MSHeader'][1]."'>".$ColHeader."</td>";
			$n++;
			
			# 1st Term Result
			$ColHeader = $eReportCard['Template']['Term1ResultCh']."<br>".$eReportCard['Template']['Term1ResultEn'];
			$x .= "<td valign='middle' width='".$eRCTemplateSetting['ColumnWidth']['MSHeader'][2]."'>".$ColHeader."</td>";
			$n++;
			
			# 2nd Term Result
			$ColHeader = $eReportCard['Template']['Term2ResultCh']."<br>".$eReportCard['Template']['Term2ResultEn'];
			$x .= "<td valign='middle' width='".$eRCTemplateSetting['ColumnWidth']['MSHeader'][3]."'>".$ColHeader."</td>";
			$n++;
			
			# Overall Result
			$ColHeader = $eReportCard['Template']['OverallResultCh']."<br>".$eReportCard['Template']['OverallResultEn'];
			$x .= "<td valign='middle' width='".$eRCTemplateSetting['ColumnWidth']['MSHeader'][4]."'>".$ColHeader."</td>";
			$n++;
		
		$x .= "</tr>";
		
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		# Get Report Info
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$isSeniorFormReport = $FormNumber > 3;
		
		# Get Report Subjects		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		
		# Get Subject Weights
		if($isSeniorFormReport) {
			$SubjectWeight = $this->returnReportTemplateSubjectWeightData($ReportID, " ReportColumnID IS NULL ");
			$SubjectWeight = BuildMultiKeyAssoc($SubjectWeight, array("SubjectID"));
		}
 		
 		// loop Subjects
 		$x = array();
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID => $Ary)
	 		{
	 			$componentRatio = array();
	 			if($isSeniorFormReport) 
	 			{
	 				// Get Component Ratio
		 			$isParentSubject = count((array)$Ary) > 1;
		 			if($isParentSubject)
		 			{
		 				// loop Components
		 				$relatedComponentSubject = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID, "", "", $ReportID);
		 				for($i=0; $i<count((array)$relatedComponentSubject); $i++)
		 				{
		 					$componentSubjectID = $relatedComponentSubject[$i]["SubjectID"];
		 					$componentRatio[$componentSubjectID] = $SubjectWeight[$componentSubjectID]["Weight"];
		 					$componentRatio[0] += $SubjectWeight[$componentSubjectID]["Weight"];
		 				}
		 			}
	 			}
	 			
		 		foreach($Ary as $SubSubjectID => $Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;&nbsp;&nbsp;";
			 		$SubjectPercentage = "";
			 		
			 		// Main Subject
			 		$isMainSubject = $SubSubjectID == 0;
			 		if($isMainSubject) {
				 		$SubSubjectID = $SubjectID;
				 		$Prefix = "";
			 		}
			 		else if ($isSeniorFormReport && $componentRatio[0] > 0) {
			 			// Calculate Component Percentage
			 			$SubjectPercentage = round($componentRatio[$SubSubjectID] / $componentRatio[0] * 100);
			 			if($SubjectPercentage > 0)
			 				$SubjectPercentage = $SubjectPercentage."%";
			 		}
			 		
			 		// Get Subject Name
			 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
	 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
	 				$SubjectName = $SubjectChn." ".$SubjectEng;
	 				
		 			$t .= "<td class='tabletext subject_col' valign='middle'>";
						$t .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
							$t .= "<tr>";
								if($isMainSubject) {
									$t .= "<td width='1%'>&nbsp;{$Prefix}</td>";
									$t .= "<td>$SubjectName</td>";
								}
								else if($isSeniorFormReport) {
									$t .= "<td width='16%'>&nbsp;{$Prefix}</td>";
									$t .= "<td width='62%' align='left'>$SubjectName</td>";
									$t .= "<td width='21%' align='right'>$SubjectPercentage</td>";
									$t .= "<td width='1%' align='right'>&nbsp;</td>";
								}
								else {
									$t .= "<td width='16%'>&nbsp;{$Prefix}</td>";
									$t .= "<td>$SubjectName</td>";
								}
								$t .= "</tr>";
						$t .= "</table>";
					$t .= "</td>";
					$x[] = $t;
				}
		 	}
		}
		
 		return $x;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array(), $relatedReports=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		# Get Report Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$isSeniorFormReport 		= $FormNumber > 3;
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$SemNumber 					= $this->Get_Semester_Seq_Number($SemID);
		
		# Get Footer Display Row
		$ShowGrandTotal 			= true;
		$ShowGrandAvg 				= true;
		$ShowOverallPositionClass 	= true;
		$ShowOverallPositionForm 	= !$isSeniorFormReport;
		
		# Get Average Full Mark
		$FormGradingSettings = $this->Get_Grand_Mark_Grading_Scheme($ClassLevelID, $ReportID);
		$SchemeID = $FormGradingSettings[-1]['SchemeID'];
		$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
		$AverageFullMark = $SchemeInfo['FullMark'];
  		
		# Term Report Result
		for($i=0; $i<2; $i++)
		{
			# Check if display Result
			$hideTermReport = $ReportType=="T" && ($i >= $SemNumber);
			
			# Get Term Report
			$TermReportID = $relatedReports[$i]['ReportID'];
			
			$columnResult = array();
			if(!$hideTermReport) {
				# Get Term Report Result Score
				$columnResult = $this->getReportResultScore($TermReportID, 0, $StudentID, '', 0, 1);
			
				# Build Grand Result array
				$columnTotal[$i] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandTotal'], $TermReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
				$columnAverage[$i] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandAverage'], $TermReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
				$columnClassPos[$i] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $TermReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
				$columnFormPos[$i] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $TermReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
				$columnClassNumOfStudent[$i] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $TermReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
				$columnFormNumOfStudent[$i] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $TermReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
			}
			else {
				# Build Grand Result array
				$columnTotal[$i] = $this->EmptySymbol;
				$columnAverage[$i] = $this->EmptySymbol;
				$columnClassPos[$i] = $this->EmptySymbol;
				$columnFormPos[$i] = $this->EmptySymbol;
				$columnClassNumOfStudent[$i] = $this->EmptySymbol;
				$columnFormNumOfStudent[$i] = $this->EmptySymbol;
			}
		}
		
		# Consolidate Report Result
		# Check if display Result
		$hideYearReport = $ReportType=="T";
		
		$result = array();
		if(!$hideYearReport) {
			# Get Consolidate Report Result Score
			$result = $this->getReportResultScore($ReportID, 0, $StudentID, '', 0, 1);
		
			# Build Grand Result
			$GrandTotal = $StudentID ? $this->Get_Score_Display_HTML($result['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
			$GrandAverage = $StudentID ? $this->Get_Score_Display_HTML($result['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
			$ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
	  		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
	  		$ClassNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
	  		$FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
		}
		else {
			# Build Grand Result array
			$GrandTotal = $this->EmptySymbol;
			$GrandAverage = $this->EmptySymbol;
			$ClassPosition = $this->EmptySymbol;
			$FormPosition = $this->EmptySymbol;
			$ClassNumOfStudent = $this->EmptySymbol;
			$FormNumOfStudent = $this->EmptySymbol;
		}
		
		# Total Mark 
		if($ShowGrandTotal)
		{
			$thisTitleEn = $eReportCard['Template']['TotalMarkEn'];
			$thisTitleCh = $eReportCard['Template']['TotalMarkCh'];
			$GrandTotalFullMark = $this->returnGrandTotalFullMark($ReportID, $StudentID, $checkExemption=1);
			$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnTotal, $GrandTotal, $first, $otherValueArr=array(), $GrandTotalFullMark);
		}
		
		# Average Mark 
		if($ShowGrandAvg)
		{
			$thisTitleEn = $eReportCard['Template']['AvgMarkEn'];
			$thisTitleCh = $eReportCard['Template']['AvgMarkCh'];
			$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnAverage, $GrandAverage, $first, $otherValueArr=array(), $AverageFullMark);
		}
		
		# Position in Class 
		if($ShowOverallPositionClass)
		{
			$thisTitleEn = $eReportCard['Template']['ClassPositionEn'];
			$thisTitleCh = $eReportCard['Template']['ClassPositionCh'];
			$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassPos, $ClassPosition, $first, $columnClassNumOfStudent, $ClassNumOfStudent);
		}
		
		# Position in Form 
		if($ShowOverallPositionForm)
		{
			$thisTitleEn = $eReportCard['Template']['FormPositionEn'];
			$thisTitleCh = $eReportCard['Template']['FormPositionCh'];
			$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormPos, $FormPosition, $first, $columnFormNumOfStudent, $FormNumOfStudent);
		}
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array(), $relatedReports=array())
	{
		global $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting 		= $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";
		$SemNumber 			= $this->Get_Semester_Seq_Number($SemID);
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$FormNumber 		= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$isSeniorFormReport = $FormNumber > 3;
				
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		
		# Get Report Subject
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
			
		# Get Subject Full Mark
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		
		# Get Subject Weights
		if(!$isSeniorFormReport) {
			$SubjectWeight = $this->returnReportTemplateSubjectWeightData($ReportID, " ReportColumnID IS NULL ");
			$SubjectWeight = BuildMultiKeyAssoc($SubjectWeight, array("SubjectID"));
		}
		
		$x = array();
		$isFirst = 1;
		
		// loop Subjects
		foreach($SubjectArray as $SubjectID => $SubjectName)
		{
			$t = "";
			$isAllNA = true;
			
			// Component Subject
			$isSub = 0;
			if(in_array($SubjectID, $MainSubjectIDArray) != true)	$isSub = 1;
			
			// Parent Subject
			$CmpSubjectArr = array();
			$isParentSubject = 0;
			if (!$isSub) {
				$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
				if(!empty($CmpSubjectArr)) $isParentSubject = 1;
			}
			
			// Get Component Ratio
			$SubjectPercentage = "";
			if(!$isSeniorFormReport && $isSub) 
	 		{
	 			// Get Parent Subject & Components
 				$thisParentSubjectID = $this->GET_PARENT_SUBJECT_ID($SubjectID);
				$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($thisParentSubjectID, $ClassLevelID);
				
 				// loop Components
 				$componentRatio = array();
 				for($i=0; $i<count((array)$CmpSubjectArr); $i++)
 				{
 					$componentSubjectID = $CmpSubjectArr[$i]["SubjectID"];
 					$componentRatio[$componentSubjectID] = $SubjectWeight[$componentSubjectID]["Weight"];
 					$componentRatio[0] += $SubjectWeight[$componentSubjectID]["Weight"];
 				}
 				
 				if($componentRatio[0] > 0) {
		 			$SubjectPercentage = round($componentRatio[$SubjectID] / $componentRatio[0] * 100);
		 			if($SubjectPercentage > 0) {
		 				$SubjectPercentage = " (".$SubjectPercentage."%)";
		 			}
 				}
 			}
			
			// Subject Full Mark
			$FullMark = $SubjectFullMarkAry[$SubjectID];
			$x[$SubjectID] .= "<td class='tabletext' align='center'>".$FullMark.$SubjectPercentage."</td>";
			
			# Term Report Result
			for($i=0; $i<2; $i++)
			{
				# Check if display Result
				$hideTermReport = $ReportType=="T" && ($i >= $SemNumber);
				
				# Get Term Report
				$TermReportID = $relatedReports[$i]['ReportID'];
				
				# Get Subject Grading Scheme
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $TermReportID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				# Get Term Report Marks
				$thisMarksAry = $this->getMarks($TermReportID, $StudentID, "", "", 1);
				$thisMSGrade = $thisMarksAry[$SubjectID][0]["Grade"];
				$thisMSMark = $thisMarksAry[$SubjectID][0]["Mark"];
				$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
				$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
				
				// for Report Preview
				if(!$StudentID) {
					$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
					$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
				}
				
				// Mark Display
				$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
				if ($thisMark != "N.A." && $thisMark!=="" && !$hideTermReport) {
					$isAllNA = false;
				}
				list($thisMark, $needStyle) = $this->checkSpCase($TermReportID, $SubjectID, $thisMark, $thisGrade);
				if($hideTermReport) {
					$thisMarkDisplay = $this->EmptySymbol;
				}
				else if($needStyle) {
					$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $TermReportID, $ClassLevelID, $SubjectID);
				}
				else {
					$thisMarkDisplay = $thisMark;
				}
				$x[$SubjectID] .= "<td class='tabletext' align='center'>".$thisMarkDisplay."</td>";
			}
			
			# Consolidate Report
			# Check if display Result
			$hideYearReport = $ReportType=="T";
			
			# Get Consolidate Report
			$YearReportID = $relatedReports[$i]['ReportID'];
			
			# Get Subject Grading Scheme
			$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $YearReportID);
			$SchemeID = $SubjectFormGradingSettings['SchemeID'];
			$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
			$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
			$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
			
			# Get Consolidate Report Result
			$MarksAry = $this->getMarks($YearReportID, $StudentID, "", "", 1);	
			$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
			$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
			$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
			$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
			
			// for Report Preview
			if(!$StudentID) {
				$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
				$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
			}
			
			// Mark Display
			$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;			
			if ($thisMark != "N.A." && $thisMark!=="" && !$hideYearReport) {
				$isAllNA = false;
			}
			list($thisMark, $needStyle) = $this->checkSpCase($YearReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
			if($hideYearReport) {
				$thisMarkDisplay = $this->EmptySymbol;
			}
			else if($needStyle) {
				$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $YearReportID, $ClassLevelID, $SubjectID);
			}
			else {
				$thisMarkDisplay = $thisMark;
			}
			$x[$SubjectID] .= "<td class='tabletext' align='center'>".$thisMarkDisplay."</td>";
			
			$isFirst = 0;
			
			# construct an array to return
			$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
			$returnArr['isAllNA'][$SubjectID] = $isAllNA;
		}
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
		
		# Get Report Basic Info
		$ReportSetting 		= $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$FormNumber 		= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$isSeniorFormReport = $FormNumber > 3;
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";
		$SemNumber 			= $this->Get_Semester_Seq_Number($SemID);
		
		# Get Semesters
		$RelatedSemesterList = $this->GET_ALL_SEMESTERS();
		$RelatedSemesterList = array_keys((array)$RelatedSemesterList);
		$firstSem = $RelatedSemesterList[0];
		$secondSem = $RelatedSemesterList[1];
		
		# Get Year Report
		$OtherInfoReportID = $ReportID;
		if($ReportType=="T") {
			$ReportList = $this->Get_Report_List($ClassLevelID, 'W', 1);
			$OtherInfoReportID = $ReportList[0]['ReportID'];
		}
		
		# Get eDiscipline Data
		$DisciplineDataAry = array();
		if ($OtherInfoReportID != '' && count($RelatedSemesterList) > 0 && $StudentID != '') {
			for($i=0; $i<2; $i++) {
				$YearTermID = $RelatedSemesterList[$i];
				$DisciplineDataAry[$i] = $this->Get_Student_Profile_Data($OtherInfoReportID, $StudentID, $MapByRecordType=true, $CheckRecordStatus=true, $returnTotalUnit=false, $YearTermID);
			}
		}
		
		# Get ECA Data
		$ECADataAry = array();
		if($OtherInfoReportID != '' && count($RelatedSemesterList) > 0 && !$isSeniorFormReport && $StudentID != '') {
			include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
			$libenroll = new libclubsenrol();
			for($i=0; $i<2; $i++) {
				$ECATermData = $libenroll->Get_Student_Club_Info($StudentID, $EnrolGroupID="", $this->GET_ACTIVE_YEAR_ID(), $RelatedSemesterList[$i], $eRCTemplateSetting['EnrolTargetCategory']);
				$ECATermData = Get_Array_By_Key((array)$ECATermData, "ClubTitle");
				$ECADataAry[$i] = implode("、", (array)$ECATermData);
			}
		}
		
		# Get Other Info Data
		$OtherInfoDataAry = array();
		if ($OtherInfoReportID != '' && $StudentID != '') {
			$OtherInfoDataAry = $this->getReportOtherInfoData($OtherInfoReportID, $StudentID); 
			$OtherInfoDataAry = $OtherInfoDataAry[$StudentID];
		}
		
		# Get Class Teacher Comment
		$TeacherComment = "";
		if ($ReportType=="W" && $OtherInfoReportID != '' && $StudentID != '') {
			$CommentAry = $this->returnSubjectTeacherComment($OtherInfoReportID, $StudentID);
			$TeacherComment = str_replace("\n", "<br/>", $CommentAry[0]);
		}
		
		# Get Result Data (1st Term)
		if($StudentID=="") {
			$SenseOfResponsibility_1 = "#";
			$LearningAttitude_1 	 = "#";
			$Courtesy_1 			 = "#";
			$Perseverance_1 		 = "#";
			$OverallConduct_1 	 	 = "#";
			$Lateness_1 			 = "#";
			$Absence_1 				 = "#";
			$CreditMark_1			 = "#";
			$MinorMerit_1			 = "#";
			$MajorMerit_1			 = "#";
			$BlackMark_1			 = "#";
			$MinorDemerit_1			 = "#";
			$MajorDemerit_1			 = "#";
		}
		else {
			$SenseOfResponsibility_1 = ($OtherInfoDataAry[$firstSem]['Sense of Responsibility'])? $OtherInfoDataAry[$firstSem]['Sense of Responsibility'] : $this->EmptySymbol;
			$LearningAttitude_1 	 = ($OtherInfoDataAry[$firstSem]['Learning Attitude'])? $OtherInfoDataAry[$firstSem]['Learning Attitude'] : $this->EmptySymbol;
			$Courtesy_1 			 = ($OtherInfoDataAry[$firstSem]['Courtesy'])? $OtherInfoDataAry[$firstSem]['Courtesy'] : $this->EmptySymbol;
			$Perseverance_1 		 = ($OtherInfoDataAry[$firstSem]['Perseverance'])? $OtherInfoDataAry[$firstSem]['Perseverance'] : $this->EmptySymbol;
			$OverallConduct_1 	 	 = ($OtherInfoDataAry[$firstSem]['Overall'])? $OtherInfoDataAry[$firstSem]['Overall'] : $this->EmptySymbol;
			$Lateness_1 			 = ($OtherInfoDataAry[$firstSem]['Lateness'])? $OtherInfoDataAry[$firstSem]['Lateness'] : 0;
			$Absence_1 				 = ($OtherInfoDataAry[$firstSem]['Absence'])? $OtherInfoDataAry[$firstSem]['Absence'] : 0;
			$CreditMark_1			 = ($DisciplineDataAry[0][1])? $DisciplineDataAry[0][1] : 0;
			$MinorMerit_1			 = ($DisciplineDataAry[0][2])? $DisciplineDataAry[0][2] : 0;
			$MajorMerit_1			 = ($DisciplineDataAry[0][3])? $DisciplineDataAry[0][3] : 0;
			$BlackMark_1			 = ($DisciplineDataAry[0][-1])? $DisciplineDataAry[0][-1] : 0;
			$MinorDemerit_1			 = ($DisciplineDataAry[0][-2])? $DisciplineDataAry[0][-2] : 0;
			$MajorDemerit_1			 = ($DisciplineDataAry[0][-3])? $DisciplineDataAry[0][-3] : 0;
			$activities_1			 = empty($ECADataAry[0])? "" : $ECADataAry[0];
		}
		
		# Get Result Data (2nd Term)
		if($ReportType=="W" || $SemNumber > 1) {
			if($StudentID=="") {
				$SenseOfResponsibility_2 = "#";
				$LearningAttitude_2 	 = "#";
				$Courtesy_2 			 = "#";
				$Perseverance_2 		 = "#";
				$OverallConduct_2 	 	 = "#";
				$Lateness_2 			 = "#";
				$Absence_2 				 = "#";
				$CreditMark_2			 = "#";
				$MinorMerit_2			 = "#";
				$MajorMerit_2			 = "#";
				$BlackMark_2			 = "#";
				$MinorDemerit_2			 = "#";
				$MajorDemerit_2			 = "#";
			}
			else {
				$SenseOfResponsibility_2 = ($OtherInfoDataAry[$secondSem]['Sense of Responsibility'])? $OtherInfoDataAry[$secondSem]['Sense of Responsibility'] : $this->EmptySymbol;
				$LearningAttitude_2 	 = ($OtherInfoDataAry[$secondSem]['Learning Attitude'])? $OtherInfoDataAry[$secondSem]['Learning Attitude'] : $this->EmptySymbol;
				$Courtesy_2 			 = ($OtherInfoDataAry[$secondSem]['Courtesy'])? $OtherInfoDataAry[$secondSem]['Courtesy'] : $this->EmptySymbol;
				$Perseverance_2 		 = ($OtherInfoDataAry[$secondSem]['Perseverance'])? $OtherInfoDataAry[$secondSem]['Perseverance'] : $this->EmptySymbol;
				$OverallConduct_2 	 	 = ($OtherInfoDataAry[$secondSem]['Overall'])? $OtherInfoDataAry[$secondSem]['Overall'] : $this->EmptySymbol;
				$Lateness_2 			 = ($OtherInfoDataAry[$secondSem]['Lateness'])? $OtherInfoDataAry[$secondSem]['Lateness'] : 0;
				$Absence_2 				 = ($OtherInfoDataAry[$secondSem]['Absence'])? $OtherInfoDataAry[$secondSem]['Absence'] : 0;
				$CreditMark_2			 = ($DisciplineDataAry[1][1])? $DisciplineDataAry[1][1] : 0;
				$MinorMerit_2			 = ($DisciplineDataAry[1][2])? $DisciplineDataAry[1][2] : 0;
				$MajorMerit_2			 = ($DisciplineDataAry[1][3])? $DisciplineDataAry[1][3] : 0;
				$BlackMark_2			 = ($DisciplineDataAry[1][-1])? $DisciplineDataAry[1][-1] : 0;
				$MinorDemerit_2			 = ($DisciplineDataAry[1][-2])? $DisciplineDataAry[1][-2] : 0;
				$MajorDemerit_2			 = ($DisciplineDataAry[1][-3])? $DisciplineDataAry[1][-3] : 0;
				$activities_2			 = empty($ECADataAry[1])? "" : $ECADataAry[1];
			}
		}
		else {
			$SenseOfResponsibility_2 = $this->EmptySymbol;
			$LearningAttitude_2 	 = $this->EmptySymbol;
			$Courtesy_2 			 = $this->EmptySymbol;
			$Perseverance_2 		 = $this->EmptySymbol;
			$OverallConduct_2 	 	 = $this->EmptySymbol;
			$Lateness_2 			 = $this->EmptySymbol;
			$Absence_2 				 = $this->EmptySymbol;
			$CreditMark_2			 = $this->EmptySymbol;
			$MinorMerit_2			 = $this->EmptySymbol;
			$MajorMerit_2			 = $this->EmptySymbol;
			$BlackMark_2			 = $this->EmptySymbol;
			$MinorDemerit_2			 = $this->EmptySymbol;
			$MajorDemerit_2			 = $this->EmptySymbol;
		}
		
		# Get Result Data (Whole Year)
		$services = "";
		if($ReportType=="W" && !empty($OtherInfoDataAry[0])) {
			$services = $OtherInfoDataAry[0]['Services & Prizes'];
		}
		
		$MiscTable = "<table width='100%' border='1' cellspacing='0' cellpadding='2' class='ms_table'>";
		
	        $MiscTable .= "<col style='width: 24%;'/>";
	        $MiscTable .= "<col style='width: 8%;'/>";
	        $MiscTable .= "<col style='width: 30%;'/>";
	        $MiscTable .= "<col style='width: 19px;'/>";
	        $MiscTable .= "<col style='width: 19px;'/>";
	        
	        # Conduct - Performance
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext ms_table_header' colspan='5' align='center'>".$eReportCard['Template']['ConductCh']." ".$eReportCard['Template']['ConductEn']."</td>";
			$MiscTable .= "</tr>";
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext ms_table_header' colspan='3' rowspan='2' align='center'>".$eReportCard['Template']['PerformanceCh']." ".$eReportCard['Template']['PerformanceEn']."</td>";
				$MiscTable .= "<td class='tabletext ms_table_header' align='center'>".$eReportCard['Template']['Term1Ch']."</td>";
				$MiscTable .= "<td class='tabletext ms_table_header' align='center'>".$eReportCard['Template']['Term2Ch']."</td>";
			$MiscTable .= "</tr>";
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext ms_table_header' align='center'>".$eReportCard['Template']['Term1En']."</td>";
				$MiscTable .= "<td class='tabletext ms_table_header' align='center'>".$eReportCard['Template']['Term2En']."</td>";
			$MiscTable .= "</tr>";
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext' colspan='3'>".$eReportCard['Template']['SenseOfResponsibilityCh']." ".$eReportCard['Template']['SenseOfResponsibilityEn']."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$SenseOfResponsibility_1."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$SenseOfResponsibility_2."</td>";
			$MiscTable .= "</tr>";
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext' colspan='3'>".$eReportCard['Template']['LearningAttitudeCh']." ".$eReportCard['Template']['LearningAttitudeEn']."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$LearningAttitude_1."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$LearningAttitude_2."</td>";
			$MiscTable .= "</tr>";
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext' colspan='3'>".$eReportCard['Template']['CourtesyCh']." ".$eReportCard['Template']['CourtesyEn']."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$Courtesy_1."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$Courtesy_2."</td>";
			$MiscTable .= "</tr>";
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext' colspan='3'>".$eReportCard['Template']['PerseveranceCh']." ".$eReportCard['Template']['PerseveranceEn']."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$Perseverance_1."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$Perseverance_2."</td>";
			$MiscTable .= "</tr>";
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext' colspan='3'><b>".$eReportCard['Template']['OverallConductCh']." ".$eReportCard['Template']['OverallConductEn']."</b></td>";
				$MiscTable .= "<td class='tabletext' align='center'><b>".$OverallConduct_1."</b></td>";
				$MiscTable .= "<td class='tabletext' align='center'><b>".$OverallConduct_2."</b></td>";
			$MiscTable .= "</tr>";
			
			# Seperator
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext' colspan='5' align='center'> </td>";
			$MiscTable .= "</tr>";
			
			# Record of Merit and Demerit
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext ms_table_header' rowspan='6' align='center'>".$eReportCard['Template']['MeritAndDemeritCh']."<br/>".$eReportCard['Template']['MeritAndDemeritEn']."</td>";
				$MiscTable .= "<td class='tabletext' colspan='2'>".$eReportCard['Template']['CreditMarkCh']." ".$eReportCard['Template']['CreditMarkEn']."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$CreditMark_1."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$CreditMark_2."</td>";
			$MiscTable .= "</tr>";
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext' colspan='2'>".$eReportCard['Template']['MinorMeritCh']." ".$eReportCard['Template']['MinorMeritEn']."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$MinorMerit_1."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$MinorMerit_2."</td>";
			$MiscTable .= "</tr>";
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext' colspan='2'>".$eReportCard['Template']['MajorMeritCh']." ".$eReportCard['Template']['MajorMeritEn']."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$MajorMerit_1."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$MajorMerit_2."</td>";
			$MiscTable .= "</tr>";
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext' colspan='2'>".$eReportCard['Template']['BlackMarkCh']." ".$eReportCard['Template']['BlackMarkEn']."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$BlackMark_1."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$BlackMark_2."</td>";
			$MiscTable .= "</tr>";
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext' colspan='2'>".$eReportCard['Template']['MinorDemeritCh']." ".$eReportCard['Template']['MinorDemeritEn']."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$MinorDemerit_1."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$MinorDemerit_2."</td>";
			$MiscTable .= "</tr>";
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext' colspan='2'>".$eReportCard['Template']['MajorDemeritCh']." ".$eReportCard['Template']['MajorDemeritEn']."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$MajorDemerit_1."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$MajorDemerit_2."</td>";
			$MiscTable .= "</tr>";
			
			# Attendance
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext ms_table_header' rowspan='2' align='center'>".$eReportCard['Template']['AttendanceCh']."<br/>".$eReportCard['Template']['AttendanceEn']."</td>";
				$MiscTable .= "<td class='tabletext' colspan='2'>".$eReportCard['Template']['LatenessCh']." ".$eReportCard['Template']['LatenessEn']."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$Lateness_1."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$Lateness_2."</td>";
			$MiscTable .= "</tr>";
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext' colspan='2'>".$eReportCard['Template']['AbsenceCh']." ".$eReportCard['Template']['AbsenceEn']."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$Absence_1."</td>";
				$MiscTable .= "<td class='tabletext' align='center'>".$Absence_2."</td>";
			$MiscTable .= "</tr>";
			
			# Remarks
			$MiscTable .= "<tr>";
				$MiscTable .= "<td class='tabletext ms_table_header' colspan='2' align='center'>".$eReportCard['Template']['RemarksCh']." ".$eReportCard['Template']['RemarksEn']."</td>";
				$MiscTable .= "<td class='tabletext ms_table_content' colspan='3'>".$TeacherComment."</td>";
			$MiscTable .= "</tr>";
			
			if($isSeniorFormReport) {
				# Seperator
				$MiscTable .= "<tr>";
					$MiscTable .= "<td class='tabletext' colspan='5' align='center'> </td>";
				$MiscTable .= "</tr>";
				
				# Grading Remarks
				$MiscTable .= "<tr>";
					$MiscTable .= "<td class='tabletext ms_table_header' colspan='5' align='center'>".$eReportCard['Template']['GradingSystemCh']." ".$eReportCard['Template']['GradingSystemEn']."</td>";
				$MiscTable .= "</tr>";
				$MiscTable .= "<tr>";
					$MiscTable .= "<td class='tabletext' colspan='3'>".$eReportCard['Template']['ExcellentCh']." (".$eReportCard['Template']['ExcellentEn'].")</td>";
					$MiscTable .= "<td class='tabletext' colspan='2' align='center'>".$eReportCard['Template']['Excellent']."</td>";
				$MiscTable .= "</tr>";
				$MiscTable .= "<tr>";
					$MiscTable .= "<td class='tabletext' colspan='3'>".$eReportCard['Template']['GoodCh']." (".$eReportCard['Template']['GoodEn'].")</td>";
					$MiscTable .= "<td class='tabletext' colspan='2' align='center'>".$eReportCard['Template']['Good']."</td>";
				$MiscTable .= "</tr>";
				$MiscTable .= "<tr>";
					$MiscTable .= "<td class='tabletext' colspan='3'>".$eReportCard['Template']['AverageCh']." (".$eReportCard['Template']['AverageEn'].")</td>";
					$MiscTable .= "<td class='tabletext' colspan='2' align='center'>".$eReportCard['Template']['Average']."</td>";
				$MiscTable .= "</tr>";
				$MiscTable .= "<tr>";
					$MiscTable .= "<td class='tabletext' colspan='3'>".$eReportCard['Template']['FairCh']." (".$eReportCard['Template']['FairEn'].")</td>";
					$MiscTable .= "<td class='tabletext' colspan='2' align='center'>".$eReportCard['Template']['Fair']."</td>";
				$MiscTable .= "</tr>";
				$MiscTable .= "<tr>";
					$MiscTable .= "<td class='tabletext' colspan='3'>".$eReportCard['Template']['PoorCh']." (".$eReportCard['Template']['PoorEn'].")</td>";
					$MiscTable .= "<td class='tabletext' colspan='2' align='center'>".$eReportCard['Template']['Poor']."</td>";
				$MiscTable .= "</tr>";
			}
			else {
				# Extra-Curricular Activities & School Teams
				$MiscTable .= "<tr>";
					$MiscTable .= "<td class='tabletext ms_table_header' colspan='2' rowspan='2' align='center'>".$eReportCard['Template']['ExtraCurricularActivitiesCh']."<br/>".$eReportCard['Template']['ExtraCurricularActivitiesEn']."</td>";
					$MiscTable .= "<td class='tabletext ms_table_content' colspan='3' valign='top'><b>".$eReportCard['Template']['Term1Ch']." ".$eReportCard['Template']['Term1En']."</b><br/>".$activities_1."</td>";
				$MiscTable .= "</tr>";
				$MiscTable .= "<tr>";
					$MiscTable .= "<td class='tabletext ms_table_content' colspan='3' valign='top'><b>".$eReportCard['Template']['Term2Ch']." ".$eReportCard['Template']['Term2En']."</b><br/>".$activities_2."</td>";
				$MiscTable .= "</tr>";
				
				# Services & Prizes
				$MiscTable .= "<tr>";
					$MiscTable .= "<td class='tabletext ms_table_header' colspan='2' align='center'>".$eReportCard['Template']['ExtraCurricularActivitiesCh']."<br/>".$eReportCard['Template']['ServicesAndPrizesEn']."</td>";
					$MiscTable .= "<td class='tabletext ms_table_content' colspan='3'>".$services."</td>";
				$MiscTable .= "</tr>";
			}
		
		$MiscTable .= "</table>";
		
		return $MiscTable;
	}
	
	function getFooter($ReportID='') {
		global $eReportCard;
		
		$FooterRow = "";
		
		if($ReportID) {
			global $StudentID;
			
			# Get Report Basic Info
			$ReportSetting 		= $this->returnReportTemplateBasicInfo($ReportID);
			$ClassLevelID 		= $ReportSetting['ClassLevelID'];
			$FormNumber 		= $this->GET_FORM_NUMBER($ClassLevelID, 1);
			$isSeniorFormReport = $FormNumber > 3;
			$SemID 				= $ReportSetting['Semester'];
	 		$ReportType 		= $SemID == "F" ? "W" : "T";
			$SemNumber 			= $this->Get_Semester_Seq_Number($SemID);
			
			# Get Other Info Data
			$OtherInfoDataAry = array();
			if ($ReportType == "W" && $StudentID > 0) {
				$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID); 
				$OtherInfoDataAry = $OtherInfoDataAry[$StudentID];
			}
			
			# Get Promotion Status
			$promotionContent = "";
			if(!empty($OtherInfoDataAry[0]) && !empty($OtherInfoDataAry[0]['Promotion'])) {
				$promotionContent = implode("<br/>", (array)$OtherInfoDataAry[0]['Promotion']);
			}
		}
		
		$FooterRow = "<table width='100%' border='0' cellspacing='0' cellpadding='2' class='table_footer'>";
			
			if($isSeniorFormReport) {
				# Seperator
				$FooterRow .= "<tr>";
					$FooterRow .= "<td class='tabletext'>&nbsp;</td>";
				$FooterRow .= "</tr>";
			}
				
			# Fail Subject Remarks
			$FooterRow .= "<tr>";
				$FooterRow .= "<td class='tabletext'>".$eReportCard['Template']['ScoreRemarksCh']."<br/>".$eReportCard['Template']['ScoreRemarksEn']."</td>";
			$FooterRow .= "</tr>";
			
			if($isSeniorFormReport) {
				# OLE Report Remarks
				$FooterRow .= "<tr>";
					$FooterRow .= "<td class='tabletext'>".$eReportCard['Template']['ECARemarksCh']."<br/>".$eReportCard['Template']['ECARemarksEn']."</td>";
				$FooterRow .= "</tr>";
			}
			else {
				# Grading Remarks
				$FooterRow .= "<tr>";
					$FooterRow .= "<td class='tabletext align='center'>";
						$FooterRow .= "<table width='100%' border='1' cellspacing='0' cellpadding='2' class='ms_table'>";
							$FooterRow .= "<tr>";
								$FooterRow .= "<td class='tabletext' align='center' colspan='10'>".$eReportCard['Template']['GradingSystemCh']." (".$eReportCard['Template']['GradingSystemEn'].")</td>";
							$FooterRow .= "</tr>";
							$FooterRow .= "<tr>";
								$FooterRow .= "<td class='tabletext' align='center' width='15%'>".$eReportCard['Template']['ExcellentCh']." (".$eReportCard['Template']['ExcellentEn'].")</td>";
								$FooterRow .= "<td class='tabletext' align='center' width='5%'>".$eReportCard['Template']['Excellent']."</td>";
								$FooterRow .= "<td class='tabletext' align='center' width='15%'>".$eReportCard['Template']['GoodCh']." (".$eReportCard['Template']['GoodEn'].")</td>";
								$FooterRow .= "<td class='tabletext' align='center' width='5%'>".$eReportCard['Template']['Good']."</td>";
								$FooterRow .= "<td class='tabletext' align='center' width='15%'>".$eReportCard['Template']['AverageCh']." (".$eReportCard['Template']['AverageEn'].")</td>";
								$FooterRow .= "<td class='tabletext' align='center' width='5%'>".$eReportCard['Template']['Average']."</td>";
								$FooterRow .= "<td class='tabletext' align='center' width='15%'>".$eReportCard['Template']['FairCh']." (".$eReportCard['Template']['FairEn'].")</td>";
								$FooterRow .= "<td class='tabletext' align='center' width='5%'>".$eReportCard['Template']['Fair']."</td>";
								$FooterRow .= "<td class='tabletext' align='center' width='15%'>".$eReportCard['Template']['PoorCh']." (".$eReportCard['Template']['PoorEn'].")</td>";
								$FooterRow .= "<td class='tabletext' align='center' width='5%'>".$eReportCard['Template']['Poor']."</td>";
							$FooterRow .= "</tr>";
					$FooterRow .= "</table>";
					$FooterRow .= "</td>";
				$FooterRow .= "</tr>";
			}
			
			if($ReportType=="W" && $FormNumber > 0 && !empty($promotionContent)) {
				if($isSeniorFormReport) {
					# Seperator
					$FooterRow .= "<tr>";
						$FooterRow .= "<td class='tabletext'>&nbsp;</td>";
					$FooterRow .= "</tr>";
				}
				
				# Promotion Remarks
				//$nextFormNumber = ($FormNumber + 1);
				$FooterRow .= "<tr>";
					//$FooterRow .= "<td class='tabletext'>".$eReportCard['Template']['PromotionRemarksCh'].$eReportCard['Template']['PromotionFormNumber'][$nextFormNumber]."<br/>".$eReportCard['Template']['PromotionRemarksEn'].$nextFormNumber."</td>";
					$FooterRow .= "<td class='tabletext'>".$promotionContent."</td>";
				$FooterRow .= "</tr>";
			}
			
		$FooterRow .= "</table>";
		
		return $FooterRow;
	}
	
	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT, $StudentID;
 		
 		if($ReportID && $StudentID) {
 			# Get Report Basic Info
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			
			# Get Student Info
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$thisClassName = $StudentInfoArr[0]['ClassName'];
 			
 			# Get Student Class Teacher
			include_once($PATH_WRT_ROOT."includes/libclass.php");
			$lclass = new libclass();
			$ClassTeacher = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
			if(!empty($ClassTeacher)) {
				// Get Class Teacher Signature
				$ClassTeacherID = $ClassTeacher[0]['UserID'];
				$ClassTeacherSignature = $this->getRequiredTeacherChop($ClassTeacherID);
				if(!empty($ClassTeacherSignature)) {
					$ClassTeacherSignature = $ClassTeacherSignature[0]['SignaturePath'];
				}
			}
			
			# Get Chancellor & Principal Signature
			$CommonSignature = $this->getRequiredTeacherChop();
			$CommonSignature = BuildMultiKeyAssoc((array)$CommonSignature, array("TeacherTitle"));
			if(!empty($CommonSignature)) {
				$SupervisorSignature = $CommonSignature['Chancellor']['SignaturePath'];
				$PrincipalSignature = $CommonSignature['Principal']['SignaturePath'];
			}
		}
 		
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' class='signature_table'>";
			$SignatureTable .= "<tr>";
		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$TitleEn = $eReportCard['Template'][$SettingID.'En'];
			$TitleCh = $eReportCard['Template'][$SettingID.'Ch'];
			$SettingID = $SettingID=="Chancellor"? "Supervisor" : $SettingID;
			$TitleSignature = ${$SettingID."Signature"};
			$TitleSignature = $TitleSignature? $TitleSignature : "&nbsp;";
			
			$SignatureTable .= "<td valign='bottom' align='center'>";
				$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
					$SignatureTable .= "<tr><td align='center' valign='bottom' style='position: relative; bottom:-9px;'>".$TitleSignature."</td></tr>";
					$SignatureTable .= "<tr><td align='center' height='1' valign='bottom'>_________________________</td></tr>";
					$SignatureTable .= "<tr><td align='center' valign='bottom'>".$TitleEn."</td></tr>";
					$SignatureTable .= "<tr><td align='center' valign='bottom'>".$TitleCh."</td></tr>";
				$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	########### END Template Related

	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# initialization
		$border_top = "";
		$x = "";
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh);
			
		if($ReportType=="W")	# Whole Year Report
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			$thisTotalValue = "";
			foreach($ColumnData as $k1=>$d1)
			{
				# get value of this term
				$TermID = $d1['SemesterNum'];
				
				$thisValue = $StudentID ? ($ValueArr[$TermID][$InfoKey] ? $ValueArr[$TermID][$InfoKey] : $this->EmptySymbol) : "#";
				
				# calculation the overall value
				if (is_numeric($thisValue)) {
					if ($thisTotalValue == "")
						$thisTotalValue = $thisValue;
					else if (is_numeric($thisTotalValue))
						$thisTotalValue += $thisValue;
				}
				
				# insert empty cell for assessments
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					
				# display this term value
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			}
			
			# display overall year value
			if($ShowRightestColumn)
			{
				if($thisTotalValue=="")
					$thisTotalValue = ($ValueArr[0][$InfoKey]=="")? $this->EmptySymbol : $ValueArr[0][$InfoKey];
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisTotalValue."</td>";
			}
		}
		else				# Term Report
		{
			# get value of this term
			$thisValue = $StudentID ? ($ValueArr[$SemID][$InfoKey] ? $ValueArr[$SemID][$InfoKey] : $this->EmptySymbol) : "#";
			
			# insert empty cell for assessments
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				
			# display this term value
			if($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
		}
		
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0, $otherValueArr=array(), $otherOverallValue="")
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		# Get Footer Type
		$isPositionFooter = !empty($otherValueArr) && !empty($otherOverallValue);
		
		# Get Report Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, $isFirst);
			if($isPositionFooter || empty($otherOverallValue)) {
				$x .= "<td class='tabletext' align='center'>".$this->EmptySymbol."</td>";
			}
			else {
				$x .= "<td class='tabletext' align='center'>".$otherOverallValue."</td>";
			}
		
		# Term Report Grand Result
		foreach ($ValueArr as $thisIndex => $thisValue) {
			if($isPositionFooter && !empty($otherValueArr[$thisIndex]) && $otherValueArr[$thisIndex]!=$this->EmptySymbol) {
				$thisValue = $thisValue."/".$otherValueArr[$thisIndex];
			}
			$x .= "<td class='tabletext' align='center'>".$thisValue."</td>";
		}
		
		# Consolidate Report Grand Result
		$thisValue = $OverallValue;
		if($isPositionFooter && $otherOverallValue!=$this->EmptySymbol) {
			$thisValue = $thisValue."/".$otherOverallValue;
		}
		$x .= "<td class='tabletext' align='center'>".$thisValue."</td>";
		
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0)
	{		
		$TitleName = $TitleCh." ".$TitleEn;
		
		$x = "<td class='tabletext subject_col' valign='middle'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr>";
						$x .= "<td>&nbsp;{$Prefix}</td>";
						$x .= "<td>$TitleName</td>";
				$x .= "</tr>";
			$x .= "</table>";
		$x .= "</td>";
		
		return $x;
	}
	
	function Is_Show_Rightest_Column($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		return $ShowRightestColumn;
	}
	
	function Get_Calculation_Setting($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		return $CalOrder;
	}
	
		// $border, e.g. "left,right,top"
	function Get_Double_Line_Td($border="",$width=1)
	{
		global $image_path,$LAYOUT_SKIN;
		
		$borderArr = explode(",",$border);
		$left = $right = $top = $bottom = 0;
		
		if(in_array("left",$borderArr)) $left = $width;
		if(in_array("right",$borderArr)) $right = $width;
		if(in_array("top",$borderArr)) $top = $width;
		if(in_array("bottom",$borderArr)) $bottom = $width;

		$style = " style='border-style:solid; border-color:#000; border-width:{$top}px {$right}px {$bottom}px {$left}px ' ";
		
		$td = "<td width='0%' height='0%' $style><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='$width' height='$width'/></td>";
		return $td;
	}
	
	function getRequiredTeacherChop($ClassTeacherID="")
	{
		// Get Class Teacher
		$condUserID = "";
		if(trim($ClassTeacherID)!='') {
			$condUserID = " iu.UserID = '$ClassTeacherID' ";
		}
		else {
			$condUserID = " tei.TeacherTitle IN ('Chancellor', 'Principal') ";
		}
		
		// Get Signature Chop
		$RC_TEACHER_EXTRA_INFO = $this->DBName.'.RC_TEACHER_EXTRA_INFO';
		$sql = "SELECT
					tei.TeacherTitle,
					IF(tei.SignaturePath IS NULL OR TRIM(tei.SignaturePath) = '',
						'&nbsp;',
						CONCAT('<img src=\"',tei.SignaturePath,'?ts=', CURRENT_TIMESTAMP(), '\" height=\"60px\">') 
					) AS SignaturePath
				FROM
					INTRANET_USER iu
					INNER JOIN $RC_TEACHER_EXTRA_INFO tei ON iu.UserID = tei.UserID
				WHERE
					iu.RecordType = '".USERTYPE_STAFF."' AND 
					iu.RecordStatus = 1 AND
					$condUserID ";
		return $this->returnArray($sql);
	}
}
?>