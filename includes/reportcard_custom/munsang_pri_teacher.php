<?php
# Editing by 

####################################################
# Library for Munsang College (Primary) - Report of Teacher View
####################################################

include_once($intranet_root."/lang/reportcard_custom/munsang_pri.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary","attendance", "schoolservice", "merit", "remark", "eca","");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 0;
		$this->IsEnableMarksheetFeedback = 0;
		$this->IsEnableMarksheetExtraInfo = 0;
	}
	
	function getChineseSubjectNameClass($name) {
		$nameLen = strlen($name)/2;
		if ($nameLen > 4) return "";
		return "chinese_subject_name_".$nameLen."w";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard;
		$x = "<tr><td>$TitleTable</td><tr>";
		$x .= "<tr><td>$StudentInfoTable</td><tr>";
		$x .= "<tr><td>$MSTable</td><tr>";
		$x .= "<tr><td>$MiscTable</td><tr>";
		//$x .= "<tr><td>$SignatureTable</td><tr>";
		$x .= $FooterRow;
		return $x;
	}
		
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
			# get school name
			$SchoolName = GET_SCHOOL_NAME();	
			
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
					/*
					if(!empty($SchoolName))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$SchoolName."</td></tr>\n";
					*/
					if(!empty($ReportTitle))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle."</td></tr>\n";
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			}
			$TitleTable .= "</tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			// Hardcoded to show Class Teacher for teacher view of report card
			$StudentTitleArray[] = "ClassTeacher";
			
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			// Hardcoded to show Class Teacher for teacher view of report card
			$SettingStudentInfo[] = "ClassTeacher";
			
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal = "XXX";
			$data['AcademicYear'] = getCurrentAcademicYear();
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] = $lu->EnglishName."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;( ".$lu->ChineseName." )";
				$data['ClassNo'] = $lu->ClassNumber;
				$data['StudentNo'] = $data['ClassNo'];
				$data['Class'] = $lu->ClassName;
				if (!in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo))
					$data['Class'] .= " (".$lu->ClassNumber.")";
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] = $lu->Gender;
				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				$data['StudentAdmNo'] = $data['STRN'];
				
				$ClassTeacherAry = $lclass->returnClassTeacher($lu->ClassName);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
				$data['ClassTeacher'] .= $eReportCard['Template']['StudentInfo']['Teacher_Chi'];
			
				
			}
			
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if(in_array($SettingID, $SettingStudentInfo)===true)
					{
						$Title = explode(",", $eReportCard['Template']['StudentInfo'][$SettingID.'_Bi']);
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
						}
						
						$StudentInfoTable .= "<td class='tabletext' width='9%' valign='top' height='{$LineHeight}'>".$Title[0]."</td>";
						$StudentInfoTable .= "<td class='tabletext' width='11%' valign='top' height='{$LineHeight}'>".$Title[1]."</td>";
						$StudentInfoTable .= "<td class='tabletext' width='2%' valign='top' height='{$LineHeight}'>:</td>";
						$StudentInfoTable .= "<td class='tabletext' width='' valign='top' height='{$LineHeight}'>";
						$StudentInfoTable .= ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
							
						$StudentInfoTable .= "</tr>";
						$count++;
					}
				}
				$StudentInfoTable .= "</table>";
			}
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2) = $ColHeaderAry;
		
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID);
		
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MarksDisplayAry = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID);
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' height='100%' border='0' cellspacing='0' cellpadding='0' class='report_border'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			$DetailsTable .= "<tr>";
			# Subject 
			$DetailsTable .= $SubjectCol[$i];;
			# Marks
			$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
			# Subject Teacher Comment
			if ($AllowSubjectTeacherComment) {
				$css_border_top = $isFirst?"border_top":"";
				if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
					$DetailsTable .= "<td class='tabletext border_left $css_border_top'>";
					$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
				} else {
					$DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
					$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>---";
				}
				$DetailsTable .= "</span></td>";
			}
			
			$DetailsTable .= "</tr>";
			$isFirst = 0;
		}
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		#########################################################
		############## Marks START
		$row2 = "";
		
		$ColumnData = $this->returnReportTemplateColumnData($ReportID);
		$ColumnWeightMap = array();
		for($i=0; $i<sizeof($ColumnData); $i++) {
			$ColumnWeightMap[$ColumnData[$i]["ReportColumnID"]] = $ColumnData[$i]["DefaultWeight"];
		}
		
		if($ReportType=="T")	# Terms Report Type
		{
			# Retrieve Invloved Assesment
			/*
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$e = 0;
			foreach ($ColoumnTitle as $ColumnID => $ColumnTitle) {
				$ColumnWeight = ($ColumnWeightMap[$ColumnID] * 100)."%";
				$ColumnTitleDisplay =  (convert2unicode($ColumnTitle, 1, 2));
				$row1 .= "<td valign='middle' height='{$LineHeight}' class='border_left tabletext' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'><b>".$ColumnTitleDisplay."<br />".$ColumnWeight."</b></td>";
				$n++;
 				$e++;
			}
			*/
		}
		else					# Whole Year Report Type
		{
 			$needRowspan=0;
			for($i=0;$i<sizeof($ColumnData);$i++)
			{
				# Hardcoded semester titles
				$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
				$SemName = ($i==0)? $eReportCard['Template']['FirstTerm_Teacher'] : $SemName;
				$SemName = ($i==1)? $eReportCard['Template']['SecondTerm_Teacher'] : $SemName;
				$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
				$ColumnWeight = ($ColumnWeightMap[$ColumnData[$i]["ReportColumnID"]] * 100)."%";
				if($isDetails==1)
				{
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					
					$thisColumnData = $this->returnReportTemplateColumnData($thisReportID);
					$thisColumnWeightMap = array();
					for($j=0; $j<sizeof($thisColumnData); $j++) {
						$thisColumnWeightMap[$thisColumnData[$j]["ReportColumnID"]] = $thisColumnData[$j]["DefaultWeight"];
					}
					/*
					$ColumnID = array();
					$ColumnTitle = array();
					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
					for($j=0;$j<sizeof($ColumnTitle);$j++)
					{
						$ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$j], 1, 2));
						$row2 .= "<td class='border_left reportcard_text' align='center' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $ColumnTitleDisplay . "</td>";
						$n++;
						$e++;
					}
					*/
					foreach ($ColumnTitleAry as $ColumnID => $ColumnTitle) {
						$ColumnTitleDisplay =  (convert2unicode($ColumnTitle, 1, 2));
						$thisColumnWeight = ($thisColumnWeightMap[$ColumnID] * 100)."%";
						$row2 .= "<td class='border_left reportcard_text' align='center' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>".$ColumnTitleDisplay."<br />".$thisColumnWeight."</td>";
						$n++;
		 				$e++;
					}
					$colspan = "colspan='". sizeof($ColumnTitleAry) ."'";
					$Rowspan = "";
					$needRowspan++;
				}
				else
				{
					$colspan = "";
					$Rowspan = "rowspan='2'";
					$e++;
				}
				$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left small_title' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>".$SemName."<br />".$ColumnWeight."</td>";
			}
		}
		############## Marks END
		#########################################################
		$Rowspan = $row2 ? "rowspan='2'" : "";

		if(!$needRowspan)
			$row1 = str_replace("rowspan='2'", "", $row1);
		
		$x = "<tr>";
		# Subject 
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		$x .= "<td {$Rowspan} colspan='".sizeof($SubjectColAry)."' valign='middle' align='center' class='small_title' height='{$LineHeight}' width='".$eRCTemplateSetting['ColumnWidth']['Subject']."'>";
		$x .= $eReportCard['Template']['SubjectsChi']."<br/>".$eReportCard['Template']['SubjectsEn']."</td>";
		$n++;
		
		# Marks
		$x .= $row1;
		
		# Subject Overall 
		if($ShowRightestColumn) {
			$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Overall'] ."' class='border_left small_title' align='center'>";
			if($ReportType=="T") {
				# Hardcoded semester titles
				$x .= $eReportCard['Template']['FirstTerm_Teacher']."<br />50%</td>";
				$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Overall'] ."' class='border_left small_title' align='center'>";
				$x .= $eReportCard['Template']['SecondTerm_Teacher']."<br />50%</td>";
				$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Overall'] ."' class='border_left small_title' align='center'>";
				$x .= $eReportCard['Template']['FinalResult_Teacher']."<br />100%</td>";
				//$SemName = $this->returnSemesters($ReportSetting['Semester']);
				//$x .= $SemName."</td>";
				//$x .= $eReportCard['Template']['SubjectOverall']."<br />100%</td>";
			} else {
				$x .= $eReportCard['Template']['FinalResult_Teacher']."<br />100%</td>";
			}
			$n++;
		} else {
			$e--;	
		}
		
		# Subject Teacher Comment
		if ($AllowSubjectTeacherComment) {
			$x .= "<td {$Rowspan} valign='middle' class='border_left small_title' align='center' width='10%'>".$eReportCard['TeachersComment']."</td>";
		}
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		return array($x, $n, $e);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		$css_border_top = ($isFirst)? "border_top" : "";
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				if ($v == "SubjectChn")
							$chineseSubjectNameClass = $this->getChineseSubjectNameClass($SubjectChn);
		 				else 
							$chineseSubjectNameClass = "";
						$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
						
			 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='tabletext $chineseSubjectNameClass'>$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
 		return $x;
	}
	
	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
		}
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			$IssueDate = ($SettingID == "IssueDate" && $Issued)	? "<u>".$Issued."</u>" : "__________";
			$SignatureTable .= "<td valign='bottom' align='center'>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td align='center' class='small_title' height='130' valign='bottom'>_____". $IssueDate ."_____</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID);
		$GrandTotal = $StudentID ? sprintf("%01.2f", $result['GrandTotal']) : "S";
		$AverageMark = $StudentID ? sprintf("%01.2f", $result['GrandAverage']) : "S";
		
 		$FormPosition = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? "---": $result['OrderMeritForm']) : "---") : "#";
  		$ClassPosition = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? "---": $result['OrderMeritClass']) : "---") : "#";
		
		if ($CalOrder == 2) {
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID);
				$columnTotal[$ColumnID] = $StudentID ? sprintf("%01.2f", $columnResult['GrandTotal']) : "S";
				$columnAverage[$ColumnID] = $StudentID ? sprintf("%01.2f", $columnResult['GrandAverage']) : "S";
				$columnClassPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritClass'] : "S";
				$columnFormPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritForm'] : "S";
				$columnClassNum[$ColumnID] = $StudentID ? $columnResult['ClassNoOfStudent'] : "S";
				$columnFormNum[$ColumnID] = $StudentID ? $columnResult['FormNoOfStudent'] : "S";
			}
		}
		
		$columnResult = $this->getReportResultScore($ReportID, 0, $StudentID);
		$columnClassNum[0] =  $StudentID ? $columnResult['ClassNoOfStudent'] : "S";
		$columnFormNum[0] = $StudentID ? $columnResult['FormNoOfStudent'] : "S";
		
		##################################################################################
		# CSV related
		##################################################################################
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		
		if($SemID=="F") {
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			foreach($ColumnData as $k1=>$d1) {
				$TermID = $d1['SemesterNum'];
				$InfoTermID = $TermID+1;
				if(!empty($csvType)) {
					foreach($csvType as $k=>$Type) {
						$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
						if(!empty($csvData)) {
							foreach($csvData as $RegNo=>$data) {
								if($RegNo == $WebSamsRegNo) {
									foreach($data as $key=>$val)
										$ary[$TermID][$key] = $val;
								}
							}
						}
					}
				}
			}
			
			# calculate sems/assesment col#
			$ColNum2Ary = array();
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				if($d1['IsDetails']==1)
				{
					# check sems/assesment col#
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1;
				}
				else
					$ColNum2Ary[$TermID] = 0;
			}
		}
		else
		{
			$InfoTermID = $SemID+1;
			if(!empty($csvType))
			{
				foreach($csvType as $k=>$Type)
				{
					$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
					if(!empty($csvData)) 
					{
						foreach($csvData as $RegNo=>$data)
						{
							if($RegNo == $WebSamsRegNo)
							{
								foreach($data as $key=>$val)
									$ary[$SemID][$key] = $val;
							}
						}
					}
				}
			}
		}
		
		# Summary Info: Use Looping
		$SummaryInfoFields = array("Diligence", "Discipline", "Politeness", "Sociability", "Tidness", "Conduct");
		
		$first = 1;
		for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
			$border_top = $first ? "border_top" : "";
			$first = 0;
			$chineseSubjectNameClass = $this->getChineseSubjectNameClass($eReportCard['Template'][$SummaryInfoFields[$i].'Ch']);
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext $chineseSubjectNameClass'>". $eReportCard['Template'][$SummaryInfoFields[$i].'Ch'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template'][$SummaryInfoFields[$i].'En'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			if($ReportType=="W")	# Whole Year Report
			{
				foreach($ColumnData as $k1=>$d1)
				{
					$TermID = $d1['SemesterNum'];
					$FieldData = $StudentID ? ((isset($ary[$TermID][$SummaryInfoFields[$i]]) && $ary[$TermID][$SummaryInfoFields[$i]] !== "") ? $ary[$TermID][$SummaryInfoFields[$i]] : "---") : "G";
					for($j=0;$j<$ColNum2Ary[$TermID];$j++)
						$x .= "<td class='tabletext border_left $border_top' align='center' height='{$LineHeight}'>---</td>";
					$x .= "<td class='tabletext border_left $border_top' align='center' height='{$LineHeight}'>". $FieldData ."</td>";
				}
				
				if($ShowRightestColumn)
					$x .= "<td class='tabletext border_left $border_top' align='center' height='{$LineHeight}'>---</td>";
			}
			else				# Term Report
			{
				$FieldData = $StudentID ? ($ary[$SemID][$SummaryInfoFields[$i]] ? $ary[$SemID][$SummaryInfoFields[$i]] : "---") : "G";
				/*
				for($j=0;$j<$ColNum2;$j++)
					$x .= "<td class='tabletext border_left $border_top' align='center' height='{$LineHeight}'>---</td>";
				*/
				$x .= "<td class='tabletext border_left $border_top' align='center' height='{$LineHeight}'>". $FieldData ."</td>";
				$x .= "<td class='tabletext border_left $border_top' align='center' height='{$LineHeight}'>---</td>";
				$x .= "<td class='tabletext border_left $border_top' align='center' height='{$LineHeight}'>---</td>";
			}
			if ($AllowSubjectTeacherComment) {
				$x .= "<td class='border_left $border_top' align='center'>";
				$x .= "<span style='padding-left:4px;padding-right:4px;'>---</span>";
				$x .= "</td>";
			}
			$x .= "</tr>";
		}
		
		
		$first = 1;
		# Overall Result
		if($ShowGrandTotal)
		{
			$border_top = $first ? "border_top" : "";
			$chineseSubjectNameClass = $this->getChineseSubjectNameClass($eReportCard['Template']['OverallResultCh']);
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext $chineseSubjectNameClass'>". $eReportCard['Template']['OverallResultCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['OverallResultEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			
			# Do not show assessments for munsang term report
			if ($ReportType=="W")
			{
				if ($CalOrder == 1) {
					for($i=0;$i<$ColNum2;$i++)
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
				} else {
					foreach ($ColumnTitle as $ColumnID => $ColumnName) {
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnTotal[$ColumnID]."</td>";
					}
				}
			}
			
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $GrandTotal ."</td>";
			
			if($ReportType=="T") {
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
			}
			if ($AllowSubjectTeacherComment) {
				$x .= "<td class='border_left $border_top' align='center'>";
				$x .= "<span style='padding-left:4px;padding-right:4px;'>---</span>";
				$x .= "</td>";
			}
			$x .= "</tr>";
		}
		
		# Average Mark 
		if($ShowGrandAvg)
		{
			$border_top = $first ? "border_top" : "";
			$chineseSubjectNameClass = $this->getChineseSubjectNameClass($eReportCard['Template']['AvgMarkCh']);
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext $chineseSubjectNameClass'>". $eReportCard['Template']['AvgMarkCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['AvgMarkEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			
			# Do not show assessments for munsang term report
			if ($ReportType=="W")
			{
				if ($CalOrder == 1) {
					for($i=0;$i<$ColNum2;$i++)
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
				} else {
					foreach ($ColumnTitle as $ColumnID => $ColumnName) {
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnAverage[$ColumnID]."</td>";
					}
				}
			}
			
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $AverageMark ."</td>";
			
			if($ReportType=="T") {
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
			}
			if ($AllowSubjectTeacherComment) {
				$x .= "<td class='border_left $border_top' align='center'>";
				$x .= "<span style='padding-left:4px;padding-right:4px;'>---</span>";
				$x .= "</td>";
			}
			$x .= "</tr>";
		}
		
		
		
		# Times Late 
		$border_top = $first ? "border_top" : "";
		$chineseSubjectNameClass = $this->getChineseSubjectNameClass($eReportCard['Template']['TimesLateCh']);
		$first = 0;
		$x .= "<tr>";
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
		$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
		$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext $chineseSubjectNameClass'>". $eReportCard['Template']['TimesLateCh'] ."</td>";
		$x .= "</tr></table>";
		$x .= "</td>";		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
		$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
		$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['TimesLateEn'] ."</td>";
		$x .= "</tr></table>";
		$x .= "</td>";
		
		if($ReportType=="W")	# Whole Year Report
		{
			$TotalTimesLate = "--";
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$TimesLate = $StudentID ? ($ary[$TermID]['Times Late'] !== "" ? $ary[$TermID]['Times Late'] : "---") : "#";
				if (is_numeric($TimesLate)) {
					if ($TotalTimesLate == "--")
						$TotalTimesLate = $TimesLate;
					else if (is_numeric($TotalTimesLate))
						$TotalTimesLate += $TimesLate;
				}
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext border_left {$border_top}' align='center' height='{$LineHeight}'>---</td>";
				$x .= "<td class='tabletext border_left {$border_top}' align='center' height='{$LineHeight}'>". $TimesLate ."&nbsp;</td>";
			}
			
			if($ShowRightestColumn)
				$x .= "<td class='tabletext border_left {$border_top}' align='center' height='{$LineHeight}'>$TotalTimesLate</td>";
		}
		else				# Term Report
		{
			$TimesLate = $StudentID ? (trim($ary[$SemID]['Times Late']) !== "" ? $ary[$SemID]['Times Late'] : "---") : "#";
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext border_left {$border_top}' align='center' height='{$LineHeight}'>---</td>";
			$x .= "<td class='tabletext border_left {$border_top}' align='center' height='{$LineHeight}'>". $TimesLate ."</td>";
		}
		if($ReportType=="T") {
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
		}
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>---</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		# Early Leave
		$chineseSubjectNameClass = $this->getChineseSubjectNameClass($eReportCard['Template']['EarlyLeaveCh']);
		$border_top = $first ? "border_top" : "";
		$first = 0;
		$x .= "<tr>";
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
		$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
		$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext $chineseSubjectNameClass'>". $eReportCard['Template']['EarlyLeaveCh'] ."</td>";
		$x .= "</tr></table>";
		$x .= "</td>";		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
		$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
		$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['EarlyLeaveEn'] ."</td>";
		$x .= "</tr></table>";
		$x .= "</td>";
		
		if($ReportType=="W")	# Whole Year Report
		{
			$TotalEarlyLeave = "--";
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$EarlyLeave = $StudentID ? (trim($ary[$TermID]['Early Leave']) !== "" ? $ary[$TermID]['Early Leave'] : "---") : "#";
				if (is_numeric($EarlyLeave)) {
					if ($TotalEarlyLeave == "--")
						$TotalEarlyLeave = $EarlyLeave;
					else if (is_numeric($TotalEarlyLeave))
						$TotalEarlyLeave += $EarlyLeave;
				}
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>---</td>";
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $EarlyLeave ."</td>";
			}
			
			if($ShowRightestColumn)
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>$TotalEarlyLeave</td>";
		}
		else				# Term Report
		{
			$EarlyLeave = $StudentID ? (trim($ary[$SemID]['Early Leave']) !== "" ? $ary[$SemID]['Early Leave'] : "---") : "#";
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>---</td>";
			$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $EarlyLeave ."</td>";
		}
		if($ReportType=="T") {
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
		}
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>---</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		# Days Absent 
		$chineseSubjectNameClass = $this->getChineseSubjectNameClass($eReportCard['Template']['DaysAbsentCh']);
		$border_top = $first ? "border_top" : "";
		$first = 0;
		$x .= "<tr>";
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
		$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
		$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext $chineseSubjectNameClass'>". $eReportCard['Template']['DaysAbsentCh'] ."</td>";
		$x .= "</tr></table>";
		$x .= "</td>";		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
		$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
		$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['DaysAbsentEn'] ."</td>";
		$x .= "</tr></table>";
		$x .= "</td>";
		
		if($ReportType=="W")	# Whole Year Report
		{
			$TotalDayAbsent = "--";
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$DaysAbsent = $StudentID ? (trim($ary[$TermID]['Days Absent']) !== "" ? $ary[$TermID]['Days Absent'] : "---") : "#";
				if (is_numeric($DaysAbsent)) {
					if ($TotalDayAbsent == "--")
						$TotalDayAbsent = $DaysAbsent;
					else if (is_numeric($TotalDayAbsent))
						$TotalDayAbsent += $DaysAbsent;
				}
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $DaysAbsent ."</td>";
			}
			
			if($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>$TotalDayAbsent</td>";
		}
		else				# Term Report
		{
			$DaysAbsent = $StudentID ? (trim($ary[$SemID]['Days Absent']) !== "" ? $ary[$SemID]['Days Absent'] : "---") : "#";
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $DaysAbsent ."</td>";
		}
		if($ReportType=="T") {
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
		}
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>---</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		# Position in Class 
		if($ShowOverallPositionClass)
		{
			$border_top = $first ? "border_top" : "";
			$chineseSubjectNameClass = $this->getChineseSubjectNameClass($eReportCard['Template']['ClassPositionCh']);
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext $chineseSubjectNameClass'>". $eReportCard['Template']['ClassPositionCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ClassPositionEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			
			# Do not show assessments for munsang term report
			if ($ReportType=="W")
			{
				if ($CalOrder == 1) {
					for($i=0;$i<$ColNum2;$i++)
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
				} else {
					foreach ($ColumnTitle as $ColumnID => $ColumnName) {
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnClassPos[$ColumnID]." / ".$columnClassNum[$ColumnID]."</td>";
					}
				}
			}
			
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>$ClassPosition / ".$columnClassNum[0]."</td>";
			
			if($ReportType=="T") {
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
			}
			if ($AllowSubjectTeacherComment) {
				$x .= "<td class='border_left $border_top' align='center'>";
				$x .= "<span style='padding-left:4px;padding-right:4px;'>---</span>";
				$x .= "</td>";
			}
			$x .= "</tr>";
		}
		
		# Position in Form 
		if($ShowOverallPositionForm)
		{
			$border_top = $first ? "border_top" : "";
			$chineseSubjectNameClass = $this->getChineseSubjectNameClass($eReportCard['Template']['FormPositionCh']);
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext $chineseSubjectNameClass'>". $eReportCard['Template']['FormPositionCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['FormPositionEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			# Do not show assessments for munsang term report
			if ($ReportType=="W")
			{
				if ($CalOrder == 1) {
					for($i=0;$i<$ColNum2;$i++)
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
				} else {
					foreach ($ColumnTitle as $ColumnID => $ColumnName) {
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnFormPos[$ColumnID]." / ".$columnFormNum[$ColumnID]."</td>";
					}
				}
			}
			
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>$FormPosition / ".$columnFormNum[0]."</td>";
			
			if($ReportType=="T") {
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>---</td>";
			}
			if ($AllowSubjectTeacherComment) {
				$x .= "<td class='border_left $border_top' align='center'>";
				$x .= "<span style='padding-left:4px;padding-right:4px;'>---</span>";
				$x .= "</td>";
			}
			$x .= "</tr>";
		}
	
		$border_top = $first ? "border_top" : "";
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='')
	{
		global $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1);
		if($ReportType=="T")	# Temrs Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
							
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# define css
				$css_border_top = ($isFirst)? "border_top" : "";
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
				$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
					
				# Assessment Marks & Display
				for($i=0;$i<sizeof($ColumnID);$i++)
				{
					/* Don't show assessment result
					if ($isParentSubject && $CalculationOrder == 1) {
						$thisMarkDisplay = "---";
					} else {
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID=".$ColumnID[$i] ." and SubjectID=$SubjectID");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnID[$i]][Mark] : "";
						$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnID[$i]][Grade] : ""; 
						
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
						if($needStyle)
						{
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							$thisMarkTemp = sprintf("%01.2f", $thisMarkTemp);
							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
							$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
					}
					
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='70%'>". $thisMarkDisplay ."</td>";
  					
					$ConvertedGrade = "";
					if (is_numeric($thisMark)) {
						$ConvertedGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark);
					} else {
						$ConvertedGrade = "-";
					}
					
					$x[$SubjectID] .= "<td class='tabletext' align='center' width='30%'>";
					if($ShowSubjectFullMark)
  					{
	  					# check special full mark
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
	  					
						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
						$x[$SubjectID] .= "(". $FullMark .")";
					}
					if($ConvertedGrade !== "")
					{
						$x[$SubjectID] .= $ConvertedGrade;
					}
					$x[$SubjectID] .= "</td>";
					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
					*/
				}
				
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall)
				{
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][0][Grade] : ""; 
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					//$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					if ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "")
					{
						$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
						$thisMarkTemp = sprintf("%01.2f", $thisMarkTemp);
						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);	
						$thisMark = $thisMark;
						
					}
					else
					{
						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisGrade);
						$thisMark = sprintf("%01.2f", $thisGrade);
					}
					
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
 						if($thisSubjectWeight > 0)
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
						else
							$thisMarkTemp = $thisMark;
						$thisMarkTemp = sprintf("%01.2f", $thisMarkTemp);
 						//$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='70%'>". $thisMarkDisplay ."</td>";
  					
					$ConvertedGrade = "";
					if ($thisGrade=="") {
						$ConvertedGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark);
					} else {
						$ConvertedGrade = $thisGrade;
					}
					
					if($needStyle)
					{
  						$ConvertedGrade = $this->ReturnTextwithStyle($ConvertedGrade, 'HighLight', $thisNature);
					} 	
					
					$x[$SubjectID] .= "<td class=' tabletext' align='center' width='30%'>";
					if($ShowSubjectFullMark)
  					{
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
						$x[$SubjectID] .= "(". $thisFullMark .")";
					}
					if($ConvertedGrade !== "")
					{
						if($needStyle)
						{
	  						$thisMarkDisplay = $this->ReturnTextwithStyle($ConvertedGrade, 'HighLight', $thisNature);
  						}
  						else
  						{
	  						$thisMarkDisplay = $ConvertedGrade;
  						}
						$x[$SubjectID] .= $thisMarkDisplay;
					}
					$x[$SubjectID] .= "</td>";
					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
					
					$x[$SubjectID] .= "<td align='center' class='{$css_border_top} border_left'>---</td><td align='center' class='{$css_border_top} border_left'>---</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>---</td>";
				}
				$isFirst = 0;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
				$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
				
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# Terms's Assesment / Terms Result
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					$css_border_top = ($isFirst)? "border_top" : "";
					
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					if($isDetails==1)		# Retrieve assesments' marks
					{
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
						
						# if no term reports, CANNOT retrieve assessment result at all
						if (empty($thisReport)) {
							for($j=0;$j<sizeof($ColumnID);$j++) {
								$x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>---</td>";
							}
						} else {
							$thisReportID = $thisReport['ReportID'];
							$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
							$ColumnID = array();
							$ColumnTitle = array();
							foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
							
							$thisMarksAry = $this->getMarks($thisReportID, $StudentID);		
								 
							for($j=0;$j<sizeof($ColumnID);$j++)
							{
								if ($isParentSubject && $CalculationOrder == 1) {
									$thisMarkDisplay = "---";
								} else {
									$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID=".$ColumnID[$j] ." and SubjectID=$SubjectID");
									$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
									
									$thisMark = $ScaleDisplay=="M" ? $thisMarksAry[$SubjectID][$ColumnID[$j]][Mark] : "";
									$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $thisMarksAry[$SubjectID][$ColumnID[$j]][Grade] : ""; 
									
									# for preview purpose
									if(!$StudentID)
									{
										$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
										$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
									}
									$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
									
									# check special case
									list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
									if($needStyle)
									{
										$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
										$thisMarkTemp = sprintf("%01.2f", $thisMarkTemp);
										$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
										$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
									}
									else
									{
										$thisMarkDisplay = $thisMark;
									}
								}
									
								$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
								$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
			  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='70%'>". $thisMarkDisplay ."</td>";
			  					
								$ConvertedGrade = "";
								if ($thisGrade=="") {
									$ConvertedGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark);
								} else {
									$ConvertedGrade = $thisGrade;
								}
								
								$x[$SubjectID] .= "<td class=' tabletext' align='center' width='30%'>";
			  					if($ShowSubjectFullMark)
			  					{
		 							$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
									$x[$SubjectID] .= "(". $FullMark .")";
								}
								if($ConvertedGrade !== "")
			  					{
									if($needStyle)
									{
				  						$thisMarkDisplay = $this->ReturnTextwithStyle($ConvertedGrade, 'HighLight', $thisNature);
			  						}
			  						else
			  						{
				  						$thisMarkDisplay = $ConvertedGrade;
			  						}
									$x[$SubjectID] .= $thisMarkDisplay;
								}
								$x[$SubjectID] .= "</td>";
								
								
								$x[$SubjectID] .= "</tr></table>";
								$x[$SubjectID] .= "</td>";
							}
						}
					}
					else					# Retrieve Terms Overall marks
					{
						if ($isParentSubject && $CalculationOrder == 1) {
							$thisMarkDisplay = "---";
						} else {
							# See if any term reports available
							$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
							
							# if no term reports, the term mark should be entered directly
							if (empty($thisReport)) {
								$thisReportID = $ReportID;
								$MarksAry = $this->getMarks($thisReportID, $StudentID);
								$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
								$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
							} else {
								$thisReportID = $thisReport['ReportID'];
								$MarksAry = $this->getMarks($thisReportID, $StudentID);
								$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
								$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
							}
							$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID=".$ColumnData[$i]['ReportColumnID'] ." and SubjectID=$SubjectID");
							$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
							
							# for preview purpose
							if(!$StudentID)
							{
								$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
								$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
							}
							# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
							//$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
							if ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "")
							{
								$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);	
								$thisMark = $thisMark;
							}
							else
							{
								$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisGrade);	
								$thisMark = $thisGrade;
							}
							
							# check special case
							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
							
							if($needStyle)
							{
								//$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
	 							//$thisMark = ($ScaleDisplay=="M" && $StudentID) ? my_round($thisMark*$thisSubjectWeight,2) : $thisMark;
								$thisMark = sprintf("%01.2f", $thisMark);
								$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
							}
						}
												
						$ConvertedGrade = "";
						
						if ($thisGrade=="") {
							$ConvertedGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark);
						} else {
							$ConvertedGrade = $thisGrade;
						}
						
						$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='70%'>". $thisMarkDisplay ."</td>";
	  					
						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='30%'>";
						
	  					if($ShowSubjectFullMark)
	  					{
 							$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1 && $UseWeightedMark) ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID];
							$x[$SubjectID] .= "(". $FullMark .")";
						}
						if($ConvertedGrade !== "")
	  					{
		  					if($needStyle)
							{
		  						$thisMarkDisplay = $this->ReturnTextwithStyle($ConvertedGrade, 'HighLight', $thisNature);
	  						}
	  						else
	  						{
		  						$thisMarkDisplay = $ConvertedGrade;
	  						}
							$x[$SubjectID] .= $thisMarkDisplay;
						}
						$x[$SubjectID] .= "</td>";
						
						$x[$SubjectID] .= "</tr></table>";
						$x[$SubjectID] .= "</td>";
					}
				}
				
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall)
				{
					$MarksAry = $this->getMarks($ReportID, $StudentID);		
					
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][0][Grade] : ""; 
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
						$thisMark = sprintf("%01.2f", $thisMark);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					} else {
						$thisMarkDisplay = $thisMark;
					}
					
					$ConvertedGrade = "";
					if ($thisGrade=="") {
						$ConvertedGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark);
					} else {
						$ConvertedGrade = $thisGrade;
					}
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='70%'>". $thisMarkDisplay ."</td>";
  					
					$x[$SubjectID] .= "<td class=' tabletext' align='center' width='30%'>";
					if($ShowSubjectFullMark)
					{
						$x[$SubjectID] .= "(". $SubjectFullMarkAry[$SubjectID] .")";
					}
					if($ConvertedGrade !== "")
					{
						if($needStyle)
						{
	  						$thisMarkDisplay = $this->ReturnTextwithStyle($ConvertedGrade, 'HighLight', $thisNature);
  						}
  						else
  						{
	  						$thisMarkDisplay = $ConvertedGrade;
  						}
						$x[$SubjectID] .= $thisMarkDisplay;
					}
					$x[$SubjectID] .= "</td>";
					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>---</td>";
				}
				$isFirst = 0;
			}
		}	# End Whole Year Report Type
		
		return $x;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		# Retrieve Basic Information of Report
		$BasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$AllowClassTeacherComment = $BasicInfo['AllowClassTeacherComment'];
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
 		$ReportType = $SemID == "F" ? "W" : "T";	
		$LineHeight = $ReportSetting['LineHeight'];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

		# retrieve the latest Term
		if ($ReportType == "W") {
			# Use other info from Whole Year CSV
			$latestTerm = "0";
		} else {
			$latestTerm = "";
			$sems = $this->returnReportInvolvedSem($ReportID);
			foreach($sems as $TermID=>$TermName)
				$latestTerm = $TermID;
			$latestTerm++;
		}
		
		
		# retrieve Student Class
		if($StudentID) {
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		foreach($csvType as $k=>$Type) {
			$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);;
			if(!empty($csvData)) {
				foreach($csvData as $RegNo=>$data) {
					if($RegNo == $WebSamsRegNo) {
	 					foreach($data as $key=>$val) {
							if (!is_numeric($key) && trim($key) !== "")
								$ary[$key] = $val;
						}
					}
				}
			}
		}
		
		# retrieve result data
		$service = $ary['Position of Services'] ? $ary['Position of Services'] : "NIL";
		$eca = $ary['ECA'] ? $ary['ECA'] : "NIL";
		$merit = $ary['Merits & Demerit'] ? $ary['Merits & Demerit'] : "NIL";
		$reason = $ary['Reasons'] ? $ary['Reasons'] : "NIL";
		$remark = $ary['Remarks'] ? $ary['Remarks'] : "NIL";
		
		$x .= "<br />";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='report_border'>";
		# Table Header
		$x .= "<tr>";
		$x .= "<td width='25%' align='center' class='tabletext'>".$eReportCard['Template']['PositionOfServicesEn']."<br />".$eReportCard['Template']['PositionOfServicesCh']."</td>";
		$x .= "<td width='25%' align='center' class='border_left tabletext'>".$eReportCard['Template']['EcaEn']."<br />".$eReportCard['Template']['EcaCh']."</td>";
		$x .= "<td width='25%' align='center' class='border_left tabletext'>".$eReportCard['Template']['Merit&DemeritEn']."<br />".$eReportCard['Template']['Merit&DemeritCh']."</td>";
		$x .= "<td width='25%' align='center' class='border_left tabletext'>".$eReportCard['Template']['RemarksEn']."<br />".$eReportCard['Template']['RemarksCh']."</td>";
		$x .= "</tr>";
		
		$x .= "<tr height='120'>";
		# Position of Services
		$x .= "<td class='tabletext border_top' valign='top'>";
		$x .= "<div style='padding:5px;'>";
		if (is_array($service)) {
			for($i=0; $i<sizeof($service); $i++) {
				$x .= $service[$i];
				$x .= ($i != sizeof($service)-1) ? ", ":"";
			}
		} else {
			$x .= $service;
		}
		$x .= "</div>";
		$x .= "</td>";
		
		# ECA
		$x .= "<td class='tabletext border_left border_top' valign='top'>";
		$x .= "<div style='padding:5px;'>";
		if (is_array($eca)) {
			for($i=0; $i<sizeof($eca); $i++) {
				$x .= $eca[$i];
				$x .= ($i != sizeof($service)-1) ? ", ":"";
			}
		} else {
			$x .= $eca;
		}
		$x .= "</div>";
		$x .= "</td>";
		
		# Merits & Demerit
		$x .= "<td class='tabletext border_left border_top' valign='top'>";
		$x .= "<div style='padding:5px;'>";
		if (is_array($merit)) {
			for($i=0; $i<sizeof($merit); $i++) {
				$x .= $merit[$i]."(".$reason[$i].")<br />";
			}
		} else {
			if ($merit == "NIL")
				$x .= $merit;
			else
				$x .= $merit."(".$reason.")";
		}
		$x .= "</div>";
		$x .= "</td>";
		
		# Remarks
		$x .= "<td class='tabletext border_left border_top' valign='top'>";
		$x .= "<div style='padding:5px;'>";
		if (is_array($remark)) {
			for($i=0; $i<sizeof($remark); $i++) {
				$x .= $remark[$i]."<br />";
			}
		} else {
			$x .= $remark;
		}
		$x .= "</div>";
		$x .= "</td>";
		
		$x .= "</tr>";
		$x .= "</table>";
		
		return $x;
	}
	
	########### END Template Related

}
?>