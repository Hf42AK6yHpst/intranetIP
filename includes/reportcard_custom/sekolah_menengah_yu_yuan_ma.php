<?php
# Editing by ivan

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "attendance", "award", "foreignStudent");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "-----";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='') {
		global $eReportCard;
		
		$footerTable = $this->getFooterTable();
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
		$TableTop .= "<tr><td>".$this->Get_Empty_Image('50px')."</td></tr>";
		$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$footerTable."</td></tr>";
		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='1000px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID) {
		$imgFile = get_website().'/file/reportcard2008/templates/sekolah_menengah_yu_yuan_ma.jpg';
		$HeaderImage = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:550px;">' : '&nbsp;';
		
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$reportTitle =  $reportInfoAry['ReportTitle'];
		$reportTitleAry = explode(":_:", $reportTitle);
		
		$x = '';
		$x .='<table class="font_17pt" style="width:100%;">';
			$x .= '<tr>';
				$x .= '<td align="right">'.$HeaderImage.'</td>'."\n";
				$x .= '<td width="140px">'."&nbsp".'</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr><td colspan="2" class="border_bottom">'.$this->Get_Empty_Image('8px').'</td></tr>'."\n";			
			$x .= '<tr><td colspan="2">'.$this->Get_Empty_Image('15px').'</td></tr>'."\n";
			// Chinese title
			$x .= '<tr><td colspan="2" class="report_title" style="font-family:\'新細明體\';"><b>'.$reportTitleAry[0].'</b></td></tr>'."\n";
			// English title
			if ($reportTitleAry[1]) {
				$x .= '<tr><td colspan="2" class="report_title" style="padding-top:0px;"><b>'.$reportTitleAry[1].'</b></td></tr>'."\n";
			}
		$x .= '</table>'."\n";
		return $x;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='') {
		
	}
	
	function getMSTable($ReportID, $StudentID='', $StdReportType='') {
		global $PATH_WRT_ROOT, $eReportCard;
		
		$isNormalStudentReport = ($StdReportType=='normal')? true : false;
		$isForeignStudentReport = ($StdReportType=='foreign')? true : false;
		
		
		### Get report info
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$classLevelId = $reportInfoAry['ClassLevelID'];
		$semester = $reportInfoAry['Semester'];
		$reportType = ($semester=='F')? 'W' : 'T';
		
		### Get report column info
		$reportColumnInfoAry = $this->returnReportTemplateColumnData($ReportID);
		$numOfReportColumn = count($reportColumnInfoAry);
		
		### Get student info
		$defaultVal = 'XXX';
		$studentDataAry = array();
		if ($StudentID) {
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$ClassID = $StudentInfoArr[0]['ClassID'];
			$ClassName = $StudentInfoArr[0]['ClassName'];
			$ClassNumber = $StudentInfoArr[0]['ClassNumber'];
			
			$studentDataAry['EnglishName'] = $lu->EnglishName;
			$studentDataAry['ChineseName'] = $lu->ChineseName;
			$studentDataAry['ClassName'] = $ClassName;
			$studentDataAry['AdmissionNo'] = str_replace('#', '', $lu->WebSamsRegNo);
			
			$classTeacherInfoAry = $this->Get_Student_Class_Teacher_Info($StudentID);
			//$studentDataAry['ClassTeacher'] = $classTeacherInfoAry[0]['TitleEnglish'].' '.$classTeacherInfoAry[0]['EnglishName'].' '.$classTeacherInfoAry[0]['ChineseName'].$classTeacherInfoAry[0]['TitleChinese'];
			//2013-0429-0951-54073
			$studentDataAry['ClassTeacher'] = $classTeacherInfoAry[0]['EnglishName'].' '.$classTeacherInfoAry[0]['ChineseName'];
		}
		else {
			$studentDataAry['EnglishName'] = $defaultVal;
			$studentDataAry['ChineseName'] = $defaultVal;
			$studentDataAry['ClassName'] = $defaultVal;
			$studentDataAry['AdmissionNo'] = $defaultVal;
			$studentDataAry['ClassTeacher'] = $defaultVal;
		}
		
		### Get the Report Info to be displayed
		$reportIdAry = array();
		if ($reportType == 'T') {
			$reportIdAry[] = $ReportID;
		}
		else if ($reportType == 'W') {
			$termReportInfoAry = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
			$reportIdAry = Get_Array_By_Key($termReportInfoAry, 'ReportID');
			$reportIdAry[] = $ReportID;
		}
		$numOfReport = count($reportIdAry);
		
		
		### Get data for each report
		$reportColumnAssoAry = array();
		$reportInfoAssoAry = array();
		$numOfTotalReportColumnCount = 0;
		for ($i=0; $i<$numOfReport; $i++) {
			$_reportId = $reportIdAry[$i];
			
			// Show overall column only for this client
			$reportColumnAssoAry[$_reportId][] = 0;
			$numOfTotalReportColumnCount++;
			
			// Get Report Info
			$reportInfoAssoAry[$_reportId]['basicInfoAry'] = $this->returnReportTemplateBasicInfo($_reportId);
			
			// Get Grading Schemes of all Subjects
			$reportInfoAssoAry[$_reportId]['gradingSchemeAry'] = $this->GET_SUBJECT_FORM_GRADING($classLevelId, $SubjectID='', $withGrandResult=0, $returnAsso=1, $_reportId);
			
			// Get Student Marks
			// $MarksArr[$SubjectID][$ReportColumnID][Key] = Data
			$reportInfoAssoAry[$_reportId]['markAry'] = $this->getMarks($_reportId, $StudentID, '', 0, 1, '', '', 0, $CheckPositionDisplay=1);		// Get the overall column only
			
			// Get Student Grand Marks
			$reportInfoAssoAry[$_reportId]['grandMarkAry'] = $this->getReportResultScore($_reportId, 0, $StudentID);
			
			// Get Student Other Info Data
			$reportInfoAssoAry[$_reportId]['otherInfoAry'] = $this->getReportOtherInfoData($_reportId, $StudentID);
		}
		
		### Get all Subjects
		$subjectInOrderArr = $this->returnSubjectwOrder($classLevelId, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		
		### Get Subject Weight
		$subjectWeightAry = $this->returnReportTemplateSubjectWeightData($ReportID, $other_condition="", $SubjectIDArr='', $ReportColumnID=0, $convertEmptyToZero=true);
		$subjectWeightAssoAry = BuildMultiKeyAssoc($subjectWeightAry, 'SubjectID', $IncludedDBField=array('Weight'), $SingleValue=1);
		unset($subjectWeightAry);
		
		### Analyze the Marks array to get the colspan of the Subject Teacher Comments cell
		$subjectDisplayInfoAssoAry = array();
		foreach ((array)$subjectInOrderArr as $_parentSubjectId => $_cmpSubjectArr) {
			$_isParentSubject = (count($_cmpSubjectArr) > 1)? true : false;
			
			foreach ((array)$_cmpSubjectArr as $__cmpSubjectID => $__cmpSubjectName) {
				$isAllNA = true;
				
				$__isComponentSubject = ($__cmpSubjectID == 0)? false : true;
				$__subjectId = ($__isComponentSubject)? $__cmpSubjectID : $_parentSubjectId;
				
				if ($_isParentSubject && !$__isComponentSubject) {
					continue;
				}
				
				$__isAllNA = true;
				$__subjectDisplayInfo = array();
				foreach ((array)$reportColumnAssoAry as $___reportId => $___reportColumnIdAry) {
					$___numOfReportColumn = count($___reportColumnIdAry);
					$___markAry = $reportInfoAssoAry[$___reportId]['markAry'];
					$___scaleDisplay = $reportInfoAssoAry[$___reportId]['gradingSchemeAry'][$__subjectId]['ScaleDisplay'];
					
					for ($i=0; $i<$___numOfReportColumn; $i++) {
						$____reportColumnId = $___reportColumnIdAry[$i];
						
						$___msGrade = $___markAry[$__subjectId][$____reportColumnId]['Grade'];
						$___msMark = $___markAry[$__subjectId][$____reportColumnId]['Mark'];
						$___classRanking = $___markAry[$__subjectId][$____reportColumnId]['OrderMeritClass'];
						$___classNumOfStudent = $___markAry[$__subjectId][$____reportColumnId]['ClassNoOfStudent'];
						$___formRanking = $___markAry[$__subjectId][$____reportColumnId]['OrderMeritForm'];
						$___formNumOfStudent = $___markAry[$__subjectId][$____reportColumnId]['FormNoOfStudent'];
						
						$___grade = ($___scaleDisplay=="G" || $___scaleDisplay=="" || $___msGrade!='' )? $___msGrade : "";
						$___mark = ($___scaleDisplay=="M" && $___grade=='') ? $___msMark : "";
						
						# for preview purpose
						if(!$StudentID) {
							$___mark 	= $___scaleDisplay=="M" ? "S" : "";
							$___grade 	= $___scaleDisplay=="G" ? "G" : "";
						}
						
						$___mark = ($___scaleDisplay=="M" && strlen($___mark)) ? $___mark : $___grade;
						
						if ($___mark != '' && $___mark != 'N.A.') {
							$__isAllNA = false;
						}
						
						# check special case
						list($___mark, $___needStyle) = $this->checkSpCase($___reportId, $__subjectId, $___mark, $___msGrade);
						if($___needStyle) {
							if ($isNormalStudentReport) {
								$___markDisplay = $this->Get_Score_Display_HTML($___mark, $___reportId, $classLevelId, $__subjectId, $___mark);
							}
							else if ($isForeignStudentReport) {
								$___markDisplay = $this->Get_Score_Display_HTML($___grade, $___reportId, $classLevelId, $__subjectId, $___mark);
								if (!$this->Check_If_Grade_Is_SpecialCase($___msGrade)) {
									$___markDisplay .= '('.$this->Get_Score_Display_HTML($___msMark, $___reportId, $classLevelId, $__subjectId, $___mark).')';
									
								}
							}
							
						}
						else {
							$___markDisplay = $___mark;
						}
						$___markDisplay = ($___markDisplay == '')? $this->EmptySymbol : $___markDisplay;
						$__subjectDisplayInfo[$___reportId][$____reportColumnId]['markDisplay'] = $___markDisplay;
					}
				}
				
				if ($StudentID=='' || !$__isAllNA) {
					$subjectWeight = $subjectWeightAssoAry[$__subjectId];
					$subjectDisplayInfoAssoAry[$__subjectId]['weight'] = ($subjectWeight=='')? $this->EmptySymbol : $subjectWeightAssoAry[$__subjectId];
				
					$subjectDisplayInfoAssoAry[$__subjectId]['chineseName'] = $this->GET_SUBJECT_NAME_LANG($__subjectId, 'b5');
					$subjectDisplayInfoAssoAry[$__subjectId]['englishName'] = $this->GET_SUBJECT_NAME_LANG($__subjectId, 'en');
					$subjectDisplayInfoAssoAry[$__subjectId]['markInfoAry'] = $__subjectDisplayInfo;
				}
			}
		}		
		
		
		### Calculate column width
		$subjectNameChColWidth = 24;
		$pColWidth = 8;
		if ($reportType == 'T') {
			$numOfMarkCol = 1;
			$totalMarkColWidth = $markColWidth = 18;
			$studentInfoTitleColWidth = 38;
			$overallColTitle = $eReportCard['Template']['MarksEn'];
		}
		else if ($reportType == 'W') {
			$numOfMarkCol = $numOfReportColumn + 1;
			$markColWidth = 13;
			$totalMarkColWidth = $markColWidth * $numOfMarkCol;
			$studentInfoTitleColWidth = 30;
			$overallColTitle = $eReportCard['Template']['YearlyResultsEn'];
		}
		$subjectNameEnColTotalWidth = 100 - $subjectNameChColWidth - $pColWidth - $totalMarkColWidth;
		$subjectNameEnColRemainingWidth = $subjectNameEnColTotalWidth - $studentInfoTitleColWidth;
		
		$numOfTotalCol = $numOfMarkCol + 4;
		$studentInfoColspan = $numOfTotalCol - 1;
		$studentInfoLeftPadding = 15;
		$leftRightTdWidth = 4;
		
		$x = '';
		$x .= '<table align="center" style="width:100%;">'."\r\n";
			$x .= '<tr>'."\r\n";
				$x .= '<td style="width:'.$leftRightTdWidth.'%">&nbsp;</td>'."\r\n";
				$x .= '<td style="width:68%; vertical-align:top;">'."\r\n";
					$x .= '<table class="border_only_table font_10pt" style="width:100%; text-align:left;">'."\r\n";
						$x .= '<col width="'.$studentInfoTitleColWidth.'%">'."\r\n";
						$x .= '<col width="'.$subjectNameEnColRemainingWidth.'%">'."\r\n";
						$x .= '<col width="'.$subjectNameChColWidth.'%">'."\r\n";
						$x .= '<col width="'.$pColWidth.'%">'."\r\n";
						for ($i=0; $i<$numOfMarkCol; $i++) {
							$x .= '<col width="'.$markColWidth.'%">'."\r\n";
						}
						
						### Student Name
						$x .= '<tr>'."\r\n";
							$x .= '<td>'.strtoupper($eReportCard['Template']['StudentNameEn']).' '.$eReportCard['Template']['StudentNameCh'].'</td>'."\r\n";
							$x .= '<td colspan="'.$studentInfoColspan.'" class="border_left" style="padding-left:'.$studentInfoLeftPadding.'px;">'.$studentDataAry['EnglishName'].' '.$studentDataAry['ChineseName'].'</td>'."\r\n";
						$x .= '</tr>'."\r\n";
						
						### Admission No
						$x .= '<tr>'."\r\n";
							$x .= '<td class="border_top">'.strtoupper($eReportCard['Template']['AdmisNoEn']).' '.$eReportCard['Template']['AdmisNoCh'].'</td>'."\r\n";
							$x .= '<td colspan="'.$studentInfoColspan.'" class="border_top border_left" style="padding-left:'.$studentInfoLeftPadding.'px;">'.$studentDataAry['AdmissionNo'].'</td>'."\r\n";
						$x .= '</tr>'."\r\n";
						
						### Class
						$x .= '<tr>'."\r\n";
							$x .= '<td class="border_top">'.strtoupper($eReportCard['Template']['ClassEn']).' '.$eReportCard['Template']['ClassCh'].'</td>'."\r\n";
							$x .= '<td colspan="'.$studentInfoColspan.'" class="border_top border_left" style="padding-left:'.$studentInfoLeftPadding.'px;">'.$studentDataAry['ClassName'].'</td>'."\r\n";
						$x .= '</tr>'."\r\n";
						
						### Form Teacher
						$x .= '<tr>'."\r\n";
							$x .= '<td class="border_top">'.strtoupper($eReportCard['Template']['FormTeacherEn']).' '.$eReportCard['Template']['FormTeacherCh'].'</td>'."\r\n";
							$x .= '<td colspan="'.$studentInfoColspan.'" class="border_top border_left" style="padding-left:'.$studentInfoLeftPadding.'px;">'.$studentDataAry['ClassTeacher'].'</td>'."\r\n";
						$x .= '</tr>'."\r\n";
						
						### Subject Info Title
						$x .= '<tr style="text-align:center;">'."\r\n";
							$x .= '<td colspan="3" class="border_top_double"><b>'.strtoupper($eReportCard['Template']['SubjectsEn']).'</b> '.$eReportCard['Template']['SubjectsCh'].'</td>'."\r\n";
							
							if ($isNormalStudentReport) {
								$thisWeightTitle = $eReportCard['Template']['P'];
								$thisFirstMarkColBorderLeft = 'border_left';
								$thisMarkTitleFont = ($reportType=='W')? 'font_6pt' : '';
								$thisOverallMarkColBorderLeft = ($reportType=='W')? 'border_left_double' : 'border_left';
							}
							else if ($isForeignStudentReport) {
								$thisWeightTitle = '&nbsp;';
								$thisFirstMarkColBorderLeft = '';
								$thisMarkTitleFont = '';
								$thisOverallMarkColBorderLeft = ($reportType=='W')? 'border_left_double' : '';
							}
							
							$x .= '<td class="border_top_double border_left"><b>'.$thisWeightTitle.'</b></td>'."\r\n";
							if ($reportType == 'W') {
								for ($i=0; $i<$numOfReportColumn; $i++) {
									//2013-1030-1724-06177
//									$_termId = $reportColumnInfoAry[$i]['SemesterNum'];
//									$_termName = $this->returnSemesters($_termId, 'en');
									
									if ($i == 0) {
										$_borderLeft = $thisFirstMarkColBorderLeft;
										$_termName = '1st Term';
									}
									else {
										$_borderLeft = 'border_left';
										$_termName = '2nd Term';
									}
									$x .= '<td class="border_top_double '.$_borderLeft.' '.$thisMarkTitleFont.'"><b>'.$_termName.'</b></td>'."\r\n";
								}
							}
							$x .= '<td class="border_top_double '.$thisOverallMarkColBorderLeft.' '.$thisMarkTitleFont.'"><b>'.$overallColTitle.'</b></td>'."\r\n";
						$x .= '</tr>'."\r\n";
						
						### Subject Info Content
						foreach ((array)$subjectDisplayInfoAssoAry as $_subjectId => $_subjectInfoAry) {
							$_subjectNameEn = $_subjectInfoAry['englishName'];
							$_subjectNameCh = $_subjectInfoAry['chineseName'];
							$_weight = $_subjectInfoAry['weight'];
							$_markInfoAry = $_subjectInfoAry['markInfoAry'];
							
							if ($isNormalStudentReport) {
								$_weightDisplay = $_weight;
							}
							else if ($isForeignStudentReport) {
								$_weightDisplay = '&nbsp;';
							}
							
							$x .= '<tr>'."\r\n";
								$x .= '<td colspan="2" class="border_top">'.$_subjectNameEn.'</td>'."\r\n";
								$x .= '<td class="border_top" style="text-align:center;">'.$_subjectNameCh.'</td>'."\r\n";
								$x .= '<td class="border_top border_left" style="text-align:center;">'.$_weightDisplay.'</td>'."\r\n";
								
								$_columnCount = 0;
								foreach ((array)$_markInfoAry as $__reportId => $__reportMarkAry) {
									foreach ((array)$__reportMarkAry as $___reportColumnId => $___reportColumnMarkAry) {
										$_columnCount++;
										
										$___markDisplay = $___reportColumnMarkAry['markDisplay'];
										
										if ($_columnCount == 1) {
											$___borderLeft = $thisFirstMarkColBorderLeft;
										}
										else if ($_columnCount==$numOfTotalReportColumnCount) {
											$___borderLeft = $thisOverallMarkColBorderLeft;
										}
										else {
											$___borderLeft = 'border_left';
										}
										$x .= '<td class="border_top '.$___borderLeft.'" style="text-align:center;">'.$___markDisplay.'</td>'."\r\n";
									}
								}
							$x .= '</tr>'."\r\n";
						}
						
						### Average
						if ($isNormalStudentReport) {
							$x .= '<tr>'."\r\n";
								$x .= '<td colspan="2" class="border_top">'.$eReportCard['Template']['AverageEn'].'</td>'."\r\n";
								$x .= '<td class="border_top" style="text-align:center;">'.$eReportCard['Template']['AverageCh'].'</td>'."\r\n";
								$x .= '<td class="border_top border_left" style="text-align:center;">&nbsp;</td>'."\r\n";
								
								$_columnCount = 0;
								foreach ((array)$reportColumnAssoAry as $_reportId => $_reportColumnIdAry) {
									$_numOfReportColumn = count($_reportColumnIdAry);
									
									for ($i=0; $i<$_numOfReportColumn; $i++) {
										$_columnCount++;
										$__reportColumnId = $_reportColumnIdAry[$i];
										
										if ($StudentID) {
											$__value = $this->Get_Score_Display_HTML($reportInfoAssoAry[$_reportId]['grandMarkAry']['GrandAverage'], $_reportId, $classLevelId, '', '', 'GrandAverage');
										}
										else {
											$__value = '#';
										}
										
										if ($_columnCount == 1) {
											$__borderLeft = '';
										}
										else if ($reportType=='W' && $_columnCount==$numOfTotalReportColumnCount) {
											$__borderLeft = $thisOverallMarkColBorderLeft;
										}
										else {
											$__borderLeft = 'border_left';
										}
										
										$__value = ($__value=='')? '&nbsp;' : '<b>'.$__value.'</b>';
										$x .= '<td class="border_top '.$__borderLeft.'" style="text-align:center;">'.$__value.'</td>'."\n";
									}
								}
							$x .= '</tr>'."\r\n";
						}
						
						### Position
						if ($isNormalStudentReport) {
							$x .= '<tr>'."\r\n";
								$x .= '<td colspan="2" class="border_top">'.$eReportCard['Template']['PositionEn'].'</td>'."\r\n";
								$x .= '<td class="border_top" style="text-align:center;">'.$eReportCard['Template']['PositionCh'].'</td>'."\r\n";
								$x .= '<td class="border_top border_left" style="text-align:center;">&nbsp;</td>'."\r\n";
								
								$_columnCount = 0;
								foreach ((array)$reportColumnAssoAry as $_reportId => $_reportColumnIdAry) {
									$_numOfReportColumn = count($_reportColumnIdAry);
									
									for ($i=0; $i<$_numOfReportColumn; $i++) {
										$_columnCount++;
										$__reportColumnId = $_reportColumnIdAry[$i];
										
										if ($StudentID) {
											$__value = $reportInfoAssoAry[$_reportId]['grandMarkAry']['OrderMeritClass'];
										}
										else {
											$__value = '#';
										}
										
										if ($_columnCount == 1) {
											$__borderLeft = '';
										}
										else if ($reportType=='W' && $_columnCount==$numOfTotalReportColumnCount) {
											$__borderLeft = $thisOverallMarkColBorderLeft;
										}
										else {
											$__borderLeft = 'border_left';
										}
										
										$__value = ($__value==-1)? $this->EmptySymbol : $__value;
										$__value = ($__value=='')? '&nbsp;' : '<b>'.$__value.'</b>';
										$x .= '<td class="border_top '.$__borderLeft.'" style="text-align:center;">'.$__value.'</td>'."\n";
									}
								}
							$x .= '</tr>'."\r\n";
						}
						
						### No of Students
						//2012-0725-1525-52071 point 1d
						//if ($isNormalStudentReport) {
						//2013-0830-0941-14071 - (Internal) Follow-up by chloeng on 2013-12-24 13:01
						if ($isNormalStudentReport) {
							$x .= '<tr>'."\r\n";
								$x .= '<td colspan="2" class="border_top">'.$eReportCard['Template']['NoOfStudentEn'].'</td>'."\r\n";
								$x .= '<td class="border_top" style="text-align:center;">'.$eReportCard['Template']['NoOfStudentCh'].'</td>'."\r\n";
								$x .= '<td class="border_top border_left" style="text-align:center;">&nbsp;</td>'."\r\n";
								
								$_columnCount = 0;
								foreach ((array)$reportColumnAssoAry as $_reportId => $_reportColumnIdAry) {
									$_numOfReportColumn = count($_reportColumnIdAry);
									
									for ($i=0; $i<$_numOfReportColumn; $i++) {
										$_columnCount++;
										$__reportColumnId = $_reportColumnIdAry[$i];
										
										if ($StudentID) {
											$__value = $reportInfoAssoAry[$_reportId]['grandMarkAry']['ClassNoOfStudent'];
										}
										else {
											$__value = '#';
										}
										
										$__borderLeft = '';
										$__bgClass = '';
										if ($_columnCount == 1) {
											// same as default value
										}
										else if ($reportType=='W' && $_columnCount==$numOfTotalReportColumnCount) {
											// overall column of the consolidated report
											$__borderLeft = 'border_left_double';
											$__bgClass = 'grey_bg';
											$__value = '';
										}
										else {
											// term columns of the consolidated report
											$__borderLeft = 'border_left';
										}
										
										$__value = ($__value=='')? '&nbsp;' : $__value;
										$x .= '<td class="border_top '.$__borderLeft.' '.$__bgClass.'" style="text-align:center;">'.$__value.'</td>'."\n";
									}
								}
							$x .= '</tr>'."\r\n";
						}
						
						### Conduct
						$x .= '<tr>'."\r\n";
							$x .= '<td colspan="2" class="border_top">'.$eReportCard['Template']['ConductEn'].'</td>'."\r\n";
							$x .= '<td class="border_top" style="text-align:center;">'.$eReportCard['Template']['ConductCh'].'</td>'."\r\n";
							$x .= '<td class="border_top border_left" style="text-align:center;">&nbsp;</td>'."\r\n";
							
							$_columnCount = 0;
							foreach ((array)$reportColumnAssoAry as $_reportId => $_reportColumnIdAry) {
								$_numOfReportColumn = count($_reportColumnIdAry);
								
								for ($i=0; $i<$_numOfReportColumn; $i++) {
									$_columnCount++;
									$__reportColumnId = $_reportColumnIdAry[$i];
									
									if ($StudentID) {
										$__semester = $reportInfoAssoAry[$_reportId]['basicInfoAry']['Semester'];
										if ($__semester == 'F') {
											$__semester = 0;
										}
										$__value = $reportInfoAssoAry[$_reportId]['otherInfoAry'][$StudentID][$__semester]['Conduct'];
										$__value = ($__value=='')? $this->EmptySymbol : $__value;
									}
									else {
										$__value = '#';
									}
									
									$__borderTop = 'border_top';
									$__borderLeft = '';
									$__bgClass = '';
									if ($_columnCount == 1) {
										// same as default value
									}
									else if ($reportType=='W' && $_columnCount==$numOfTotalReportColumnCount) {
										// overall column of the consolidated report
										$__borderTop = '';
										$__borderLeft = 'border_left_double';
										$__bgClass = 'grey_bg';
										$__value = '';
									}
									else {
										// term columns of the consolidated report
										$__borderLeft = 'border_left';
									}
									
									$__value = ($__value=='')? '&nbsp;' : $__value;
									$x .= '<td class="'.$__borderTop.' '.$__borderLeft.' '.$__bgClass.'" style="text-align:center;">'.$__value.'</td>'."\n";
								}
							}
						$x .= '</tr>'."\r\n";
						
						### School Terms
						//2013-1030-1724-06177
//						if ($reportType == 'T') {
							$titleEn = $eReportCard['Template']['SchoolTermEn']; 
							$titleCh = $eReportCard['Template']['SchoolTermCh'];
//						}
//						else {
//							$titleEn = $eReportCard['Template']['SchoolTermsEn'];
//							$titleCh = $eReportCard['Template']['SchoolTermsCh'];
//						}
						$x .= '<tr>'."\r\n";
							$x .= '<td colspan="2" class="border_top">'.$titleEn.'</td>'."\r\n";
							$x .= '<td class="border_top" style="text-align:center;">'.$titleCh.'</td>'."\r\n";
							$x .= '<td class="border_top border_left font_8pt" style="text-align:right;">'.$eReportCard['Template']['FromEn'].'</td>'."\r\n";
							
							$_columnCount = 0;
							foreach ((array)$reportColumnAssoAry as $_reportId => $_reportColumnIdAry) {
								$_numOfReportColumn = count($_reportColumnIdAry);
								
								for ($i=0; $i<$_numOfReportColumn; $i++) {
									$_columnCount++;
									$__reportColumnId = $_reportColumnIdAry[$i];
									$__termStart = $reportInfoAssoAry[$_reportId]['basicInfoAry']['TermStartDate'];
									$__dateDisplay = (is_date_empty($__termStart))? $this->EmptySymbol : strtoupper(date("j-M-Y", strtotime($__termStart)));
									
									if ($reportType=='W' && $_columnCount > 2) {
										continue;
									}
									else if ($reportType=='W' && $_columnCount > 1 && ($_columnCount % 2 == 0)) {
										$x .= '<td class="border_top border_left font_8pt" style="text-align:right;">'.$eReportCard['Template']['FromEn'].'</td>'."\r\n";
									}
									$x .= '<td class="border_top font_8pt" style="text-align:center;">'.$__dateDisplay.'</td>'."\n";
								}
							}
						$x .= '</tr>'."\r\n";
						$x .= '<tr>'."\r\n";
							$x .= '<td colspan="2">&nbsp;</td>'."\r\n";
							$x .= '<td style="text-align:center;">&nbsp;</td>'."\r\n";
							$x .= '<td class="border_left font_8pt" style="text-align:right;">'.$eReportCard['Template']['ToEn'].'</td>'."\r\n";
							
							$_columnCount = 0;
							foreach ((array)$reportColumnAssoAry as $_reportId => $_reportColumnIdAry) {
								$_numOfReportColumn = count($_reportColumnIdAry);
								
								for ($i=0; $i<$_numOfReportColumn; $i++) {
									$_columnCount++;
									$__reportColumnId = $_reportColumnIdAry[$i];
									$__termEnd = $reportInfoAssoAry[$_reportId]['basicInfoAry']['TermEndDate'];
									$__dateDisplay = (is_date_empty($__termEnd))? $this->EmptySymbol : strtoupper(date("j-M-Y", strtotime($__termEnd)));
									
									if ($reportType=='W' && $_columnCount > 2) {
										continue;
									}
									else if ($reportType=='W' && $_columnCount > 1 && ($_columnCount % 2 == 0)) {
										$x .= '<td class="border_left font_8pt" style="text-align:right;">'.$eReportCard['Template']['ToEn'].'</td>'."\r\n";
									}
									$x .= '<td class="font_8pt" style="text-align:center;">'.$__dateDisplay.'</td>'."\n";
								}
							}
						$x .= '</tr>'."\r\n";
						
						### Periods of Approved Leave
						$x .= '<tr>'."\r\n";
							$x .= '<td colspan="2" class="border_top">'.$eReportCard['Template']['PerOfApprovedLeaveEn'].'</td>'."\r\n";
							$x .= '<td class="border_top" style="text-align:center;">'.$eReportCard['Template']['PerOfApprovedLeaveCh'].'</td>'."\r\n";
							$x .= '<td class="border_top border_left" style="text-align:center;">&nbsp;</td>'."\r\n";
							
							$_columnCount = 0;
							foreach ((array)$reportColumnAssoAry as $_reportId => $_reportColumnIdAry) {
								$_numOfReportColumn = count($_reportColumnIdAry);
								
								for ($i=0; $i<$_numOfReportColumn; $i++) {
									$_columnCount++;
									$__reportColumnId = $_reportColumnIdAry[$i];
									if ($StudentID) {
										$__semester = $reportInfoAssoAry[$_reportId]['basicInfoAry']['Semester'];
										if ($__semester == 'F') {
											$__semester = 0;
										}
										$__value = $reportInfoAssoAry[$_reportId]['otherInfoAry'][$StudentID][$__semester]['Periods of Approved Leave'];
										$__value = ($__value=='')? 0 : $__value;
									}
									else {
										$__value = '#';
									}
									
									$__borderLeft = '';
									$__bgClass = '';
									if ($_columnCount == 1) {
										// same as default value
									}
									else if ($reportType=='W' && $_columnCount==$numOfTotalReportColumnCount) {
										// overall column of the consolidated report
										$__borderLeft = 'border_left_double';
										$__bgClass = 'grey_bg';
										$__value = '';
									}
									else {
										// term columns of the consolidated report
										$__borderLeft = 'border_left';
									}
									
									$__value = ($__value=='')? '&nbsp;' : $__value;
									$x .= '<td class="border_top '.$__borderLeft.' '.$__bgClass.'" style="text-align:center;">'.$__value.'</td>'."\n";
								}
							}
						$x .= '</tr>'."\r\n";
						
						### Periods of Absence
						$x .= '<tr>'."\r\n";
							$x .= '<td colspan="2" class="border_top">'.$eReportCard['Template']['PerOfAbsenceEn'].'</td>'."\r\n";
							$x .= '<td class="border_top" style="text-align:center;">'.$eReportCard['Template']['PerOfAbsenceCh'].'</td>'."\r\n";
							$x .= '<td class="border_top border_left" style="text-align:center;">&nbsp;</td>'."\r\n";
							
							$_columnCount = 0;
							foreach ((array)$reportColumnAssoAry as $_reportId => $_reportColumnIdAry) {
								$_numOfReportColumn = count($_reportColumnIdAry);
								
								for ($i=0; $i<$_numOfReportColumn; $i++) {
									$_columnCount++;
									$__reportColumnId = $_reportColumnIdAry[$i];
									
									if ($StudentID) {
										$__semester = $reportInfoAssoAry[$_reportId]['basicInfoAry']['Semester'];
										if ($__semester == 'F') {
											$__semester = 0;
										}
										$__value = $reportInfoAssoAry[$_reportId]['otherInfoAry'][$StudentID][$__semester]['Periods of Absence'];
										$__value = ($__value=='')? 0 : $__value;
									}
									else {
										$__value = '#';
									}
									
									$__borderTop = 'border_top';
									$__borderLeft = '';
									$__bgClass = '';
									if ($_columnCount == 1) {
										// same as default value
									}
									else if ($reportType=='W' && $_columnCount==$numOfTotalReportColumnCount) {
										// overall column of the consolidated report
										$__borderTop = '';
										$__borderLeft = 'border_left_double';
										$__bgClass = 'grey_bg';
										$__value = '';
									}
									else {
										// term columns of the consolidated report
										$__borderLeft = 'border_left';
									}
									
									$__value = ($__value=='')? '&nbsp;' : $__value;
									$x .= '<td class="'.$__borderTop.' '.$__borderLeft.' '.$__bgClass.'" style="text-align:center;">'.$__value.'</td>'."\n";
								}
							}
						$x .= '</tr>'."\r\n";
						
						// 2012-0725-1525-52071
						// Please also take note that 'Awards & Punishments‘ column and 'Form Teacher's Comments' column no longer required. 
//						if ($reportType == 'W') {
//							### Awards & Punishments
//							$otherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
//							$otherInfoDataAry = $otherInfoDataAry[$StudentID];
//							$awardAndPunishment = implode('<br />', (array)$otherInfoDataAry[0]['Awards & Punishments']);
//							
//							$x .= '<tr>'."\r\n";
//								$x .= '<td colspan="'.$numOfTotalCol.'" class="border_top" style="padding-top:3px;"><b>'.strtoupper($eReportCard['Template']['AwardsAndPunishmentsEn']).' '.$eReportCard['Template']['AwardsAndPunishmentsCh'].'</b></td>'."\r\n";
//							$x .= '</tr>'."\r\n";
//							$x .= '<tr>'."\r\n";
//								$x .= '<td colspan="'.$numOfTotalCol.'" style="height:32px; vertical-align:top;">'.$awardAndPunishment.'</td>'."\r\n";
//							$x .= '</tr>'."\r\n";
//							
//							### Form Teacher's Comments
//							$commentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
//							$classTeacherComment = $commentAry[0];
//							$x .= '<tr>'."\r\n";
//								$x .= '<td colspan="'.$numOfTotalCol.'" class="border_top" style="padding-top:3px;"><b>'.strtoupper($eReportCard['Template']['FormTeacherCommentsEn']).' '.$eReportCard['Template']['FormTeacherCommentsCh'].'</b></td>'."\r\n";
//							$x .= '</tr>'."\r\n";
//							$x .= '<tr>'."\r\n";
//								$x .= '<td colspan="'.$numOfTotalCol.'" style="height:32px; vertical-align:top;">'.$classTeacherComment.'</td>'."\r\n";
//							$x .= '</tr>'."\r\n";
//						}
						
					$x .= '</table>'."\r\n";
				$x .= '</td>'."\r\n";
				$x .= '<td style="width:1%;">&nbsp;</td>'."\r\n";
				$x .= '<td style="vertical-align:top;">'."\r\n";
					$x .= $this->getSignatureTable();
				$x .= '</td>'."\r\n";
				$x .= '<td style="width:'.$leftRightTdWidth.'%">&nbsp;</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	function genMSTableColHeader($ReportID) {
		
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID) {
		
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array()) {
		
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array()) {
		
	}
	
	function getMiscTable($ReportID, $StudentID='') {
		
	}
	
	function getSignatureTable($ReportID='') {
 		global $eReportCard;
   		
   		$x = '';
   		$x .= $this->Get_Empty_Image_Div('82px')."\n";
		$x .= $this->generateSignatureTableHtml($eReportCard['Template']['FormTeacherSignEn'], $eReportCard['Template']['FormTeacherSignCh']);
		$x .= '<br />'."\n";
		$x .= '<br />'."\n";
		$x .= $this->generateSignatureTableHtml($eReportCard['Template']['PrincipalSignEn'], $eReportCard['Template']['PrincipalSignCh']);
		$x .= '<br />'."\n";
		$x .= '<br />'."\n";
		$x .= $this->generateSignatureTableHtml($eReportCard['Template']['ParentSignEn'], $eReportCard['Template']['ParentSignCh']);
		
		return $x;
	}
	
	function generateSignatureTableHtml($titleEn, $titleCh) {
		global $eReportCard;
		
		$tableWidth = 100;
   		$titleTopPadding = 10;
   		$contentHeight = 50;
   		
		$x = '';
		$x .= '<table class="border_only_table font_8pt signature_text" style="width:'.$tableWidth.'%; text-align:left;">'."\r\n";
			$x .= '<tr><td style="padding-top:'.$titleTopPadding.'px;">'.strtoupper($titleEn).'</td></tr>'."\r\n";
			$x .= '<tr><td>'.$titleCh.'</td></tr>'."\r\n";
			$x .= '<tr><td>'.$this->Get_Empty_Image($contentHeight.'px').'</td></tr>'."\r\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function getFooterTable() {
		global $eReportCard;
		
		$x .= '<table class="border_top font_12pt" style="width:100%; text-align:center;">'."\r\n";
			$x .= '<tr><td style="padding-top:10px;">'.$eReportCard['Template']['FooterLine1'].'</td></tr>'."\r\n";
			$x .= '<tr><td>'.$eReportCard['Template']['FooterLine2'].'</td></tr>'."\r\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	########### END Template Related
}
?>