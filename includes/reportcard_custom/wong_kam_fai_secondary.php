<?php
# Editing by 

/*
 * 2019-0723-1739-00277
 * - Added OLE in config files type, modified Get_OLE_Table()
 * 2011-0622-0920-15069
 * - Subject Teacher Comment of the Consolidated Report is now retrieved from the last Term included in the Consolidated Report (Term 2 for this client)
 */

include_once($intranet_root."/lang/reportcard_custom/wong_kam_fai_secondary.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom($academicYearId='') {
		$this->libreportcard($academicYearId);
		//$this->configFilesType = array("summary", "attendance", "skills", "OLE", "award", "awardproject", "projectinfo", "merit", "remark");
		$this->configFilesType = array("summary", "skills", "OLE", "award", "awardproject", "projectinfo", "merit", "remark");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "---";
		
		// Moved to config file
		//$this->textAreaMaxChar = 540;
		//$this->textAreaMaxChar = 460;
		//$this->textAreaMaxChar_SubjectTeacherComment = 460;
		
		//$this->MathsCode = '280';
		
		//$this->PersonalCharGradeArr[Index] = LangKey
		$this->PersonalCharGradeArr['10'] = 'NotApplicable';
		$this->PersonalCharGradeArr['20'] = 'NeedsImprovement';
		$this->PersonalCharGradeArr['30'] = 'Satisfactory';
		$this->PersonalCharGradeArr['40'] = 'Good';
		$this->PersonalCharGradeArr['50'] = 'Excellent';
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID) {
		global $eReportCard;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$IsMainReport = $ReportSetting['isMainReport'];
		
		$OtherInfoArr = array();
		if ($StudentID != '')
		{
			$OtherInfoArr = $this->getReportOtherInfoData($ReportID, $StudentID);
			$OtherInfoArr = $OtherInfoArr[$StudentID];
		}
			
		if ($IsMainReport)
		{
			$Page1 = '';
			$Page1 .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' style='page-break-after:always;'>";
				$Page1 .= $this->Get_Empty_Row('130px', $NumOfRow=1);
				$Page1 .= "<tr><td>".$TitleTable."</td></tr>";
				$Page1 .= "<tr><td>".$StudentInfoTable."</td></tr>";
				$Page1 .= $this->Get_Empty_Row('10px', $NumOfRow=1);
				$Page1 .= "<tr><td>".$MSTable."</td></tr>";
				//$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
			$Page1 .= "</table>";
			
			$Page2 = '';
			$Page2 .= $this->Get_Empty_Image_Div('70px');
			$Page2 .= $this->getReportStudentInfo($ReportID, $StudentID, $forPage3='', $PageNum=2);
			$Page2 .= $this->Get_Empty_Image_Div('10px');
			$Page2 .= $this->Get_Generic_Skills_Table($ReportID, $StudentID, $OtherInfoArr);
			$Page2 .= $this->Get_Empty_Image_Div('4px');
			$Page2 .= $this->Get_OLE_Table($ReportID, $StudentID, $OtherInfoArr);
			$Page2 .= $this->Get_Empty_Image_Div('10px');
			$Page2 .= $this->Get_Awards_Table($ReportID, $StudentID, $OtherInfoArr);
			$Page2 .= $this->Get_Empty_Image_Div('10px');
			$Page2 .= $this->Get_Conduct_Table($ReportID, $StudentID, $OtherInfoArr);
			$Page2 .= $this->Get_Empty_Image_Div('4px');
			$Page2 .= $this->Get_Comment_And_Signature_Table($ReportID, $StudentID);
		}
		else
		{
			$Page1 = '';
			$Page1 .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' style='page-break-after:always;'>";
				$Page1 .= $this->Get_Empty_Row('130px', $NumOfRow=1);
				$Page1 .= "<tr><td>".$TitleTable."</td></tr>";
				$Page1 .= "<tr><td>".$StudentInfoTable."</td></tr>";
				$Page1 .= $this->Get_Empty_Row('10px', $NumOfRow=1);
				$Page1 .= "<tr><td>".$this->Get_Project_Info_Table($ReportID, $StudentID, $OtherInfoArr)."</td></tr>";
				$Page1 .= $this->Get_Empty_Row('10px', $NumOfRow=1);
				$Page1 .= "<tr><td>".$MSTable."</td></tr>";
				//$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
			$Page1 .= "</table>";
			
			$Page2 = '';
			$Page2 .= $this->Get_Empty_Image_Div('70px');
			$Page2 .= $this->getReportStudentInfo($ReportID, $StudentID, $forPage3='', $PageNum=2);
			$Page2 .= $this->Get_Empty_Image_Div('10px');
			$Page2 .= $this->getMSTable($ReportID, $StudentID, $PrintTemplateType='', $PageNum=2);
			$Page2 .= $this->Get_Empty_Image_Div('10px');
			$Page2 .= $this->Get_Overall_Performance_Table($ReportID, $StudentID);
			$Page2 .= $this->Get_Empty_Image_Div('10px');
			$Page2 .= $this->Get_Awards_Table($ReportID, $StudentID, $OtherInfoArr);
			$Page2 .= $this->Get_Empty_Image_Div('10px');
			$Page2 .= $this->Get_Comment_And_Signature_Table($ReportID, $StudentID);
			
		}
		
		
		
		$x = "";
		$x .= "<tr><td valign='top'>";
			$x .= $Page1;
			$x .= $Page2;
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
			# get school badge
			$SchoolLogo = GET_SCHOOL_BADGE();
				
			# get school name
			$SchoolName = GET_SCHOOL_NAME();	
			
			$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
	
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
					//if(!empty($SchoolName))
					//	$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$SchoolName."</td></tr>\n";
					if(!empty($ReportTitle))
						$TitleTable .= "<tr><td nowrap='nowrap' class='font_14px_bold' align='center'>".$ReportTitle."<br />".$eReportCard['Template']['SecondarySchool']."</td></tr>\n";
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			}
			$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='', $forPage3='', $PageNum=1)
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			$SemID = $ReportSetting['Semester'];
			$ReportType = $SemID == "F" ? "W" : "T";
			$IsMainReport = $ReportSetting['isMainReport'];
			$TermStartDate = $ReportSetting['TermStartDate'];
			$TermEndDate = $ReportSetting['TermEndDate'];
			
			# retrieve required variables
			$defaultVal = ($StudentID=='')? "XXX" : '';
			//$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			//$data['AcademicYear'] = $this->GET_ACTIVE_YEAR_NAME();
			
			$data['DateOfIssue'] = date('F j, Y', strtotime($ReportSetting['Issued']));
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				//$data['Name'] = $lu->UserName2Lang('en', 2);
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				$thisClassID = $StudentInfoArr[0]['ClassID'];
				
				//2013-0205-1006-15073
				//$data['Name'] = $StudentInfoArr[0]['EnglishName'].' <span style="font-family:細明體_HKSCS;">'.$StudentInfoArr[0]['ChineseName'].'</span>';
				//2014-0307-1538-54073
				$data['Name'] = str_replace('*', '', $StudentInfoArr[0]['EnglishName']).' <span style="font-family:細明體_HKSCS;">'.str_replace('*', '', $StudentInfoArr[0]['ChineseName']).'</span>';
				
				$data['ClassNo'] = $thisClassNumber;
				$data['Class'] = $thisClassName;
				
				$data['StudentID'] = str_replace('s', '', $lu->UserLogin);
				
				//$data['DateOfBirth'] = $lu->DateOfBirth;
				//$data['Gender'] = $lu->Gender;
				//$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				//$data['StudentAdmNo'] = $data['STRN'];
				
				//$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
				//foreach($ClassTeacherAry as $key=>$val)
				//{
				//	$CTeacher[] = $val['CTeacher'];
				//}
				//$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
				
				
				### Get Absent and Late data
				// 2012-0206-1620-05069
//				$OtherInfoDataArr = $this->getReportOtherInfoData($ReportID, $StudentID);
//				$OtherInfoDataArr = $OtherInfoDataArr[$StudentID];
//				$TargetSem = ($ReportType=='T')? $SemID : 0;
//				
//				$Absent = $OtherInfoDataArr[$TargetSem]['Day(s) of Absence'];
//				$Late = $OtherInfoDataArr[$TargetSem]['Late'];

				// 2012-0206-1620-05069, 2012-0703-1601-23099
//				$TargetSemAry = array();
//				if ($ReportType=='T') {
//					$TargetSemAry[] = $SemID;
//				}
//				else {
//					$TargetSemAry = Get_Array_By_Key($this->Get_Related_TermReport_Of_Consolidated_Report($ReportID), 'Semester');
//				}
//				$numOfSem = count($TargetSemAry);
//				
//				$attandanceInfoAry = array();
//				for ($i=0; $i<$numOfSem; $i++) {
//					$_yearTermId = $TargetSemAry[$i];
//					$_attandanceInfoAry = $this->Get_Student_Profile_Attendance_Data($_yearTermId, '', '', '', $lu->ClassName);
//					$attandanceInfoAry[$StudentID]["Days Absent"] += $_attandanceInfoAry[$StudentID]["Days Absent"];
//					$attandanceInfoAry[$StudentID]["Time Late"] += $_attandanceInfoAry[$StudentID]["Time Late"];
//				}
				$TargetTermID = ($ReportType=='W')? '' : $SemID;
				//$attandanceInfoAry = $this->Get_Student_Profile_Attendance_Data($TargetTermID, '', $TermStartDate, $TermEndDate, $lu->ClassName);
				$attandanceInfoAry = $this->Get_Student_Profile_Attendance_Data($TargetTermID, $thisClassID, $TermStartDate, $TermEndDate, $lu->ClassName, $StudentID);
				$Absent = ($attandanceInfoAry[$StudentID]["Days Absent"]=='')? 0 : $attandanceInfoAry[$StudentID]["Days Absent"];
				$Late = ($attandanceInfoAry[$StudentID]["Time Late"]=='')? 0 : $attandanceInfoAry[$StudentID]["Time Late"]; 
			}
			
			### Issue Date
			$x = '';
			$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">'."\n";
				$x .= '<tr><td class="font_12px" align="right">'.$eReportCard['Template']['StudentInfo']['Date'].': '.$data['DateOfIssue'].'</td></tr>'."\n";
			$x .= '</table>'."\n";
			
			$thisHeight = ($PageNum == 1)? '5px' : '2px';
				 $x .= $this->Get_Empty_Image_Div($thisHeight,$thisHeight); //20130704 Siuwan [2013-0704-1025-43073]
			
			### Name, StudentID, Absence, Class Late
			$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" class="double_border_table">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="font_12px" width="52%">'.$eReportCard['Template']['StudentInfo']['NameEn'].': &nbsp;&nbsp;<b>'.$data['Name'].'</b></td>'."\n";
					if ($PageNum == 1)
					{
						$x .= '<td class="font_12px" width="25%">'.$eReportCard['Template']['StudentInfo']['StudentIDEn'].': &nbsp;&nbsp;<b>'.$data['StudentID'].'</b></td>'."\n";
						
						// Hide Absent Info in Project-based Report
						$thisDisplay = ($IsMainReport)? $eReportCard['Template']['DaysAbsentEn'].': &nbsp;&nbsp;<b>'.$Absent.'</b>' : '&nbsp;';
						$x .= '<td class="font_12px">'.$thisDisplay.'</td>'."\n";
					}
				$x .= '</tr>'."\n";
				
				if ($PageNum == 1)
				{
					$x .= '<tr>'."\n";
						$x .= '<td class="font_12px">'.$eReportCard['Template']['StudentInfo']['ClassEn'].': &nbsp;&nbsp;<b>'.$data['Class'].'</b>&nbsp;&nbsp;( <b>'.$data['ClassNo'].'</b> )</td>'."\n";
						$x .= '<td class="font_12px">&nbsp;</td>'."\n";
						
						// Hide Late Info in Project-based Report
						$thisDisplay = ($IsMainReport)? $eReportCard['Template']['TimesLateEn'].': &nbsp;&nbsp;<b>'.$Late.'</b>' : '&nbsp;';
						$x .= '<td class="font_12px">'.$thisDisplay.'</td>'."\n";
					$x .= '</tr>'."\n";
				}
			$x .= '</table>'."\n";
		}
		return $x;
	}
	
	function getMSTable($ReportID, $StudentID='', $PrintTemplateType='', $PageNum=1)
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$IsMainReport = $ReportSetting['isMainReport'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		
		
		### Get Report Columns
		$ReportColumnInfoArr = array();
		if ($ReportType == 'W')
			$ReportColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
		
		$ReportColumnInfoArr[] = array("ReportColumnID" => 0);
		$numOfReportColumn = count($ReportColumnInfoArr);
		
		### Get Round Info
		$SubjectScoreRounding = $this->LOAD_SETTING('Storage&Display', 'SubjectScore');
		$SubjectTotalRounding = $this->LOAD_SETTING('Storage&Display', 'SubjectTotal');
		
		
		### Get all Subjects
		$SubjectInOrderArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		
		### Get Subject Mapping
		$SubjectIDCodeArr = $this->GET_SUBJECTS_CODEID_MAP($withComponent=0, $mapByCode=0);
		
		### Get Grading Schemes of all Subjects
		$GradingSchemeArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID='', $withGrandResult=0, $returnAsso=1, $ReportID);
		
		### Get Student Marks
		// $MarksArr[$SubjectID][$ReportColumnID][Key] = Data
		if ($ReportType == 'T')
			$MarksArr = $this->getMarks($ReportID, $StudentID, '', 0, 1, '', '', 0, 0);		// Get the overall column only
		else
			$MarksArr = $this->getMarks($ReportID, $StudentID, '', 0, 1);
			
		### Get Subject Full Marks
		$SubjectFullMarkAssoArr = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
				
		### Get Student Subject Comment
		if ($ReportType == 'T') {
			$SubjectTeacherCommentReportID = $ReportID;
		}
		else if ($ReportType == 'W') {
			$LastTermID = $this->Get_Last_Semester_Of_Report($ReportID);
			$LastTermReportInfoArr = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $LastTermID);
			$SubjectTeacherCommentReportID = $LastTermReportInfoArr['ReportID'];
		}
		$SubjectTeacherCommentArr = $this->returnSubjectTeacherComment($SubjectTeacherCommentReportID, $StudentID);
		
				
		
		### Analyze the Marks array to get the colspan of the Subject Teacher Comments cell
		$SubjectDisplayInfoAssoArr = array();
		$numOfDisplayParentSubject = 0;		// for Subject Row Height calculation
		$numOfDisplayOtherSubject = 0;		// for Subject Row Height calculation
		foreach ((array)$SubjectInOrderArr as $thisParentSubjectID => $thisCmpSubjectArr)
		{
			$numOfCmpSubject = count($thisCmpSubjectArr) - 1;
			$thisIsParentSubject = ($numOfCmpSubject==0)? false : true;
			
			### Check Row Span
			// count number of Subject which is not all N.A. marks
			$thisSubjectRowSpan = 0;
			foreach ((array)$thisCmpSubjectArr as $thisCmpSubjectID => $thisCmpSubjectName)
			{
				$isAllNA = true;
				
				$thisIsComponentSubject = ($thisCmpSubjectID == 0)? false : true;
				$thisSubjectID = ($thisIsComponentSubject)? $thisCmpSubjectID : $thisParentSubjectID;
				$thisScaleDisplay = $GradingSchemeArr[$thisSubjectID]['ScaleDisplay'];
				
				for ($i=0; $i<$numOfReportColumn; $i++)
				{
					# Consolidated Report shows 2nd Term and Overall Result only
					if ($ReportType == 'W' && $i==0)
						continue;
						
					$thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];
					
					$thisMSGrade = $MarksArr[$thisSubjectID][$thisReportColumnID]['Grade'];
					$thisMSMark = $MarksArr[$thisSubjectID][$thisReportColumnID]['Mark'];
					
					$thisGrade = ($thisScaleDisplay=="G" || $thisScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($thisScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";	
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $thisScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $thisScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($thisScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					if ($thisMark != "" && $thisMark != "N.A.")
						$isAllNA = false;
				}
				
				
				if ($IsMainReport == 1)
				{
					### Show Component Subject even if the Component is all N.A. (except Maths (Code=280), Component of Maths is hidden if all marks are N.A.)
					if ($StudentID == '' || $isAllNA == false)
					{
						$SubjectDisplayInfoAssoArr[$thisSubjectID]['DisplaySubject'] = true;
						$SubjectDisplayInfoAssoArr[$thisParentSubjectID]['SubjectTeacherCommentRowSpan']++;
						
						if ($thisIsParentSubject && $thisIsComponentSubject)
							$SubjectDisplayInfoAssoArr[$thisParentSubjectID]['NumOfDisplayCmpSubject']++;
					}
				}
				else
				{
					### Extra Term Report => Count all Subjects first. Determine the display after looping all Cmp Subjects.
					### But display all Component Subjects if the Subject is shown
					$SubjectDisplayInfoAssoArr[$thisSubjectID]['IsAllNA'] = $isAllNA;
					$SubjectDisplayInfoAssoArr[$thisParentSubjectID]['SubjectTeacherCommentRowSpan']++;
						
					if ($thisIsParentSubject && $thisIsComponentSubject)
					{
						$SubjectDisplayInfoAssoArr[$thisParentSubjectID]['NumOfDisplayCmpSubject']++;
						$SubjectDisplayInfoAssoArr[$thisSubjectID]['DisplaySubject'] = true;
						
						if ($isAllNA == false)
							$SubjectDisplayInfoAssoArr[$thisParentSubjectID]['NumOfNotAllNACmpSubject']++;
					}
				}
			}
			
			if ($IsMainReport == 1)
			{
				if ($SubjectDisplayInfoAssoArr[$thisParentSubjectID]['DisplaySubject'])
				{
					if ($thisIsParentSubject)
						$numOfDisplayParentSubject++;
					else
						$numOfDisplayOtherSubject++;
				}
			}
			else
			{
				# Extra Term Report
				# Display Subject if	1) Subject is a Parent Subject and
				#							1.1) Parent Subject has grade
				#							1.2) At least one of the Cmp Subject has grade
				if ($thisIsParentSubject && ($SubjectDisplayInfoAssoArr[$thisParentSubjectID]['IsAllNA'] == false || $SubjectDisplayInfoAssoArr[$thisParentSubjectID]['NumOfNotAllNACmpSubject'] > 0))
				{
					$numOfDisplayParentSubject++;
					$SubjectDisplayInfoAssoArr[$thisParentSubjectID]['DisplaySubject'] = true;
				}
				else
				{
					$SubjectDisplayInfoAssoArr[$thisParentSubjectID]['DisplaySubject'] = false;
				}
			}
		}
		
		
		
		### Check if need to display Project Based Learning Row
		$DisplayProjectBasedRow = false;
		// 2014-0918-1013-51073
		//if ($ReportType == 'W')
		// 2015-0625-1201-49206: hide row until further notice
//		if ($ReportType == 'W' && in_array($this->GET_ACTIVE_YEAR(), (array)$eRCTemplateSetting['HideProjectBaseLearningRowInConsolidatedReportYearAry']))
//		{
//			$ExtraTermReportInfoArr = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, '', $isMainReport=0);
//			$ExtraReportID = $ExtraTermReportInfoArr['ReportID'];
//			//$ExtraReportID = 101;
//			
//			if ($ExtraReportID != '')
//			{
//				$DisplayProjectBasedRow = true;
//				$numOfDisplayOtherSubject++;
//			}
//		}
		
		### Table and Row Height Calculation
		if ($IsMainReport)
			$MarkTableHeight = 725;
		else
			$MarkTableHeight = ($PageNum == 1)? 646 : 454;
		$HeightOfTitleRow = ($IsMainReport)? 42 : 34;		
		$HeightOfParentSubjectRow = ($IsMainReport)? 125 : 204;
		$TotalHeightOfParentSubjectRow = $HeightOfParentSubjectRow * $numOfDisplayParentSubject;
		
		if ($numOfDisplayOtherSubject > 0)
			$HeightOfOtherSubjectRow = floor(($MarkTableHeight - $HeightOfTitleRow - $TotalHeightOfParentSubjectRow) / $numOfDisplayOtherSubject);
			
			
		
		### Build Mark Table
		$x = '';
		$x .= '<table width="100%" height="'.$MarkTableHeight.'px" border="0" cellspacing="0" cellpadding="0" class="double_border_table">'."\n";
			### Header
			$x .= '<tr>'."\n";
				
				# Subject / Area
				$thisWidth = ($ReportType == 'T')? (($IsMainReport)? '25%' : '32%') : '25%';
				$thisTitle = ($IsMainReport)? $eReportCard['Template']['SubjectEn'] : $eReportCard['Template']['AreaEn'];
				$x .= '<td class="font_11px_bold" style="text-align:center;width:'.$thisWidth.';">'.$thisTitle.'</td>'."\n";
				
				# Highest Score
				if ($IsMainReport)
				{
					$thisWidth = ($ReportType == 'T')? '8%' : '8%';
					$x .= '<td class="font_11px_bold border_left" style="text-align:center;width:'.$thisWidth.';">'.$eReportCard['Template']['HighestScoreEn'].'<br />/'.$eReportCard['Template']['LevelEn'].'*</td>'."\n";
				}
				
				# Score / Level
				$thisWidth = ($ReportType == 'T')? (($IsMainReport)? '8%' : '10%') : '8%';
				$thisTitle = ($IsMainReport)? $eReportCard['Template']['ScoreEn'].'<br />/'.$eReportCard['Template']['LevelEn'] : $eReportCard['Template']['LevelEn'];
				$x .= '<td class="font_11px_bold border_left" style="text-align:center;width:'.$thisWidth.';">'.$thisTitle.'*</td>'."\n";
				
				# Annual Total
				if ($ReportType == 'W') {
					$x .= '<td class="font_11px_bold border_left" style="text-align:center;width:8%;">'.$eReportCard['Template']['AnnualTotalEn'].'</td>'."\n";
				}
				
				# Remarks
				$thisWidth = ($ReportType == 'T')? (($IsMainReport)? '59%' : '58%') : '51%';
				$thisTitle = ($IsMainReport)? $eReportCard['Template']['RemarksEn'] : $eReportCard['Template']['IndicatorDescriptionEn'];
				$x .= '<td class="font_11px_bold border_left" style="text-align:center;width:'.$thisWidth.';">'.$thisTitle.'</td>'."\n";
			$x .= '</tr>'."\n";
			
			
			### Subject Marks
			$DisplaySubjectCount = 0;
			foreach ((array)$SubjectInOrderArr as $thisParentSubjectID => $thisCmpSubjectArr)
			{
				$numOfCmpSubject = count($thisCmpSubjectArr) - 1;
				$thisIsParentSubject = ($numOfCmpSubject==0)? false : true;
				
				$thisDisplayLang = ($GradingSchemeArr[$thisParentSubjectID]['LangDisplay']=='')? 'en' : $GradingSchemeArr[$thisParentSubjectID]['LangDisplay']; 
				$thisNumOfDisplayCmpSubject = $SubjectDisplayInfoAssoArr[$thisParentSubjectID]['NumOfDisplayCmpSubject'];
				$thisNumOfNotAllNACmpSubject = $SubjectDisplayInfoAssoArr[$thisParentSubjectID]['NumOfNotAllNACmpSubject'];
				$thisSubjectTeacherCommentRowSpan = $SubjectDisplayInfoAssoArr[$thisParentSubjectID]['SubjectTeacherCommentRowSpan'];
				$thisDisplayParentSubject = $SubjectDisplayInfoAssoArr[$thisParentSubjectID]['DisplaySubject'];
				
				if ($thisDisplayParentSubject == false)
					continue;
					
				if ($IsMainReport == 0)
				{
					// For Extra Term Report, Page 1 => 3 Subjects, Page 2 => 2 Subjects
					$DisplaySubjectCount++;
					
					if ($PageNum == 1 && $DisplaySubjectCount > 3)
						continue;
					if ($PageNum == 2 && ($DisplaySubjectCount < 4 || $DisplaySubjectCount > 5))
						continue;
				}
				
				
				### Show "Module 1 /Module 2" if no Modules has been studied
				if ($SubjectIDCodeArr[$thisParentSubjectID] == $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Maths'] && $SubjectDisplayInfoAssoArr[$thisParentSubjectID]['NumOfDisplayCmpSubject'] == 1)
				{
					$thisNumOfDisplayCmpSubject++;
					$thisSubjectTeacherCommentRowSpan++;
				}
				
				
				### Get Subject Row Height
				if ($thisIsParentSubject)
				{
					$thisParentRowHeight = ($IsMainReport)? 23 : 34;
					
					# Evenly distributed the height to the Parent Subject and Components
					$thisCmpRowHeight = ($thisNumOfDisplayCmpSubject > 0)? floor(($HeightOfParentSubjectRow - $thisParentRowHeight) / $thisNumOfDisplayCmpSubject) : 0;
				}
				else
				{
					$thisParentRowHeight = $HeightOfOtherSubjectRow;
				}
					
				
				// count number of Subject which is not all N.A. marks
				$thisCmpDisplayCount = 0;
				foreach ((array)$thisCmpSubjectArr as $thisCmpSubjectID => $thisCmpSubjectName)
				{
					$thisIsComponentSubject = ($thisCmpSubjectID == 0)? false : true;
					$thisSubjectID = ($thisIsComponentSubject)? $thisCmpSubjectID : $thisParentSubjectID;
					
					if ($SubjectDisplayInfoAssoArr[$thisSubjectID]['DisplaySubject'] == false)
						continue;
						
					// Hide Maths Mark for Upper Form if Maths is a Parent Subject
					$thisHideMark = false;
					if ($SubjectIDCodeArr[$thisSubjectID] == $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Maths'] && $SubjectDisplayInfoAssoArr[$thisParentSubjectID]['NumOfDisplayCmpSubject'] > 0)
					{
						$thisHideMark = true;
					}
						
					if ($thisIsComponentSubject)
						$thisCmpDisplayCount++;
					
					
					### Get Subject Grading Scheme Info 
					$thisScaleInput = $GradingSchemeArr[$thisSubjectID]['ScaleInput'];
					$thisScaleDisplay = $GradingSchemeArr[$thisSubjectID]['ScaleDisplay'];
					
					### Get Subject Name Display
					if ($thisDisplayLang == 'en' || $thisDisplayLang == 'ch')
					{
						$thisSubjectName = $this->GET_SUBJECT_NAME_LANG($thisSubjectID, $thisDisplayLang);
					}
					else
					{
						// both Chinese and English
						$thisSubjectNameEn = $this->GET_SUBJECT_NAME_LANG($thisSubjectID, 'en');
						$thisSubjectNameCh = $this->GET_SUBJECT_NAME_LANG($thisSubjectID, 'b5');
						$thisSubjectName = $thisSubjectNameEn.' '.$thisSubjectNameCh;
					}
					$thisSubjectName = ($thisScaleDisplay == 'G' && $IsMainReport)? $thisSubjectName.'*' : $thisSubjectName;
					$thisPadding = ($thisIsComponentSubject)? (($IsMainReport)? '10px' : '15px') : '3px';
					
					### Get Subject Full Mark
					$thisFullMark = $SubjectFullMarkAssoArr[$thisSubjectID];
					
					
					$thisRowHeight = ($thisIsComponentSubject)? $thisCmpRowHeight : $thisParentRowHeight;
					$thisBorderLeft = ($thisIsParentSubject && !$thisIsComponentSubject)? '' : 'border_left';
					
					
					$thisSubjectHTML = '';
					$thisSubjectHTML .= '<tr height="'.$thisRowHeight.'px">'."\n";
						
						# Subject Name
						if ($thisDisplayLang == 'ch')
							$thisClass = ($thisIsComponentSubject)? 'font_12px_chi' : 'font_14px_bold_chi';
						else
							$thisClass = ($thisIsComponentSubject)? 'font_10px' : 'font_12px_bold';
						$thisSubjectHTML .= '<td class="'.$thisClass.' border_top" style="padding-left:'.$thisPadding.';padding-right:'.$thisPadding.';">'.$thisSubjectName.'</td>'."\n";
									
						# Subject Full Mark
						if ($IsMainReport)
						{
							$thisClass = ($thisIsComponentSubject)? 'font_12px' : (($thisScaleDisplay=='G')? 'font_12px' : 'font_14px');
							$thisFullMark = ($thisHideMark)? '&nbsp;' : $thisFullMark;
							$thisSubjectHTML .= '<td class="'.$thisClass.' '.$thisBorderLeft.' border_top" style="text-align:center;">'.$thisFullMark.'</td>'."\n";
						}
						
						# Subject Marks
						for ($i=0; $i<$numOfReportColumn; $i++)
						{
							# Consolidated Report shows 2nd Term and Overall Result only
							if ($ReportType == 'W' && $i==0)
								continue;
								
							
							$thisRowspan = 1;
							if (($IsMainReport == 0 && !$thisIsComponentSubject) || $thisHideMark == true)
							{
								$thisMarkDisplay = '&nbsp;';
							}
							else
							{
								$thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];
																
								if ($IsMainReport == 0 && $thisIsComponentSubject && $thisNumOfNotAllNACmpSubject == 0)
								{
									// display the Parent Subject Grade for the first Component Subject
									if ($thisCmpDisplayCount == 1)
									{
										$thisSubjectID = $thisParentSubjectID;
										$thisRowspan = $thisNumOfDisplayCmpSubject;
									}
									else
										continue;
								}
								
								$thisMSGrade = $MarksArr[$thisSubjectID][$thisReportColumnID]['Grade'];
								$thisMSMark = $MarksArr[$thisSubjectID][$thisReportColumnID]['Mark'];
																
								$thisGrade = ($thisScaleDisplay=="G" || $thisScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($thisScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";	
								
								# for preview purpose
								if(!$StudentID)
								{
									$thisMark 	= $thisScaleDisplay=="M" ? "S" : "";
									$thisGrade 	= $thisScaleDisplay=="G" ? "G" : "";
								}
								
								$thisMark = ($thisScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
								
								# check special case
								list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisSubjectID, $thisMark, $thisMSGrade);
								
								if($needStyle)
								{
									if ($thisReportColumnID != 0 && $thisSubjectColumnWeight > 0 && $thisScaleDisplay=="M")
										$thisMarkTemp = ($UseWeightedMark && $thisSubjectColumnWeight != 0) ? $thisMark / $thisSubjectColumnWeight : $thisMark;
									else
										$thisMarkTemp = $thisMark;
									
									$thisRounding = ($thisReportColumnID == 0)? $SubjectTotalRounding : $SubjectScoreRounding;
									$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $thisSubjectID, $thisMarkTemp, '', '', '', '', 0, $thisRounding, $thisScaleDisplay);
								}
								else
									$thisMarkDisplay = $thisMark;
								$thisMarkDisplay = ($thisMarkDisplay == '')? $this->EmptySymbol : $thisMarkDisplay;
							}
							
							$thisClass = ($thisIsComponentSubject)? 'font_12px' : 'font_14px';
							$thisSubjectHTML .= '<td class="'.$thisClass.' '.$thisBorderLeft.' border_top" rowspan="'.$thisRowspan.'" style="text-align:center;">'.$thisMarkDisplay.'</td>'."\n";
						}
						
						# Subject Teacher Comment
						if (!$thisIsComponentSubject)
						{
							$thisComment = nl2br($SubjectTeacherCommentArr[$thisSubjectID]);
							$thisComment = ($thisComment=='')? '&nbsp;' : $thisComment;
							$thisSubjectHTML .= '<td class="font_11px border_left border_top" rowspan="'.$thisSubjectTeacherCommentRowSpan.'" style="vertical-align:top;padding-left:3px;padding-top:8px;">'.$thisComment.'</td>'."\n";
						}
					$thisSubjectHTML .= '</tr>'."\n";
					
					$x .= $thisSubjectHTML;
				}
				
				### Show "Module 1 / Module 2" for Maths if the Student did not studied any Modules
				if ($SubjectIDCodeArr[$thisParentSubjectID] == $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Maths'] && $SubjectDisplayInfoAssoArr[$thisParentSubjectID]['NumOfDisplayCmpSubject'] == 1)
				{
					$thisRowHeight = $thisCmpRowHeight;
					
					$x .= '<tr height="'.$thisRowHeight.'px">'."\n";
					
						# Subject Name
						$thisClass = 'font_10px';
						$x .= '<td class="'.$thisClass.' border_top" style="padding-left:'.$thisPadding.';padding-right:'.$thisPadding.';">'.$eReportCard['Template']['Module1Module2En'].'</td>'."\n";
						
						# Subject Full Mark
						$thisClass = 'font_12px';
						$x .= '<td class="'.$thisClass.' '.$thisBorderLeft.' border_top" style="text-align:center;">50</td>'."\n";
						
						# Subject Marks
						$thisClass = 'font_12px';
						$x .= '<td class="'.$thisClass.' '.$thisBorderLeft.' border_top" style="text-align:center;">'.$eReportCard['RemarkNotAssessed'].'</td>'."\n";
						
						# Annual Total
						if ($ReportType == 'W') {
							$thisClass = 'font_12px';
							$x .= '<td class="'.$thisClass.' '.$thisBorderLeft.' border_top" style="text-align:center;">'.$eReportCard['RemarkNotAssessed'].'</td>'."\n";
						}
					
					$x .= '</tr>'."\n";
				}
				
			}	// end loop Subject Mark
			
			### Show Project Based Mark if have the Extra Term Report
			if ($DisplayProjectBasedRow)
			{
				$ProjectBasedGrade = $this->Get_Project_Based_Report_Overall_Grade($ExtraReportID, $StudentID);
				$ProjectBasedGrade = ($ProjectBasedGrade=='')? '&nbsp;' : $ProjectBasedGrade;
				
				$thisRowHeight = $HeightOfOtherSubjectRow;
				$thisBorderLeft = 'border_left';
				
				$x .= '<tr height="'.$thisRowHeight.'px">'."\n";
					
					# Subject Name
					$thisClass = 'font_12px_bold';
					$x .= '<td class="'.$thisClass.' border_top" style="padding-left:'.$thisPadding.';padding-right:'.$thisPadding.';">'.$eReportCard['Template']['ProjectBasedLearningEn'].'*</td>'."\n";

					# Full Mark
					$thisClass = 'font_12px';
					$x .= '<td class="'.$thisClass.' '.$thisBorderLeft.' border_top" style="text-align:center;">'.$eReportCard['Template']['Level5En'].'</td>'."\n";

					# Score
					$x .= '<td class="'.$thisClass.' '.$thisBorderLeft.' border_top" rowspan="'.$thisRowspan.'" style="text-align:center;">'.$ProjectBasedGrade.'</td>'."\n";
					
					# Annual Total
					$x .= '<td class="'.$thisClass.' '.$thisBorderLeft.' border_top" rowspan="'.$thisRowspan.'" style="text-align:center;">'.$ProjectBasedGrade.'</td>'."\n";

					# Subject Teacher Comment
					$x .= '<td class="font_11px border_left border_top" style="vertical-align:top;padding-left:3px;padding-top:8px;">'.$eReportCard['Template']['ReferToProjectReportEn'].'</td>'."\n";

				$x .= '</tr>'."\n";
			}
			
		$x .= "</table>";
		
		if ($PageNum == 1) {	
			$x .= '<div>'.$this->Get_Empty_Image('3px').'</div>';
			$x .= $this->genMSTableFooter($ReportID, $StudentID, $ReportType);
		}
				
		return $x;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		#########################################################
		############## Marks START
		$row2 = "";
		if($ReportType=="T")	# Terms Report Type
		{
			# Retrieve Invloved Assesment
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if (sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
				
			$e = 0;
			for($i=0;$i<sizeof($ColumnTitle);$i++)
			{
				//$ColumnTitleDisplay = convert2unicode($ColumnTitle[$i], 1, 2);
				$ColumnTitleDisplay = $ColumnTitle[$i];
				$row1 .= "<td valign='middle' height='{$LineHeight}' class='border_left tabletext' align='center'><b>". $ColumnTitleDisplay . "</b></td>";
				$n++;
 				$e++;
			}
		}
		else					# Whole Year Report Type
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
 			$needRowspan=0;
			for($i=0;$i<sizeof($ColumnData);$i++)
			{
				$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
				$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
				if($isDetails==1)
				{
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColumnID = array();
					$ColumnTitle = array();
					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
					for($j=0;$j<sizeof($ColumnTitle);$j++)
					{
						//$ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$j], 1, 2));
						$ColumnTitleDisplay =  $ColumnTitle[$j];
						$row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $ColumnTitleDisplay . "</td>";
						$n++;
						$e++;
					}
					$colspan = "colspan='". sizeof($ColumnTitle) ."'";
					$Rowspan = "";
					$needRowspan++;
					$t[$i] = sizeof($ColumnTitle);
				}
				else
				{
					$colspan = "";
					$Rowspan = "rowspan='2'";
					$e++;
				}
				$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left small_title' align='center'>". $SemName ."</td>";
			}
		}
		############## Marks END
		#########################################################
		$Rowspan = $row2 ? "rowspan='2'" : "";

		if(!$needRowspan)
			$row1 = str_replace("rowspan='2'", "", $row1);
		
		$x = "<tr>";
		# Subject 
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		for($i=0;$i<sizeof($SubjectColAry);$i++)
		{
			$SubjectEng = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'];
			$SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];
			$SubjectTitle = $SubjectColAry[$i];
			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
			$x .= "<td {$Rowspan}  valign='middle' class='small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>". $SubjectTitle . "</td>";
			$n++;
		}
		
		# Marks
		$x .= $row1;
		
		# Subject Overall 
		if($ShowRightestColumn)
		{
			$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Overall'] ."' class='border_left small_title' align='center'>". $eReportCard['Template']['SubjectOverall'] ."</td>";
			$n++;
		}
		else
		{
			//$e--;	
		}
		
		# Subject Teacher Comment
		if ($AllowSubjectTeacherComment) {
			$x .= "<td {$Rowspan} valign='middle' class='border_left small_title' align='center' width='10%'>".$eReportCard['TeachersComment']."</td>";
		}
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		$css_border_top = ($Prefix)? "" : "border_top";
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
			 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='tabletext'>$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
		}
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			$IssueDate = ($SettingID == "IssueDate" && $Issued)	? "<u>".$Issued."</u>" : "__________";
			$SignatureTable .= "<td valign='bottom' align='center'>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td align='center' class='small_title' height='130' valign='bottom'>_____". $IssueDate ."_____</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID, $ReportType)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		if ($ReportType == 'W')
		{
			if ($StudentID == '')
			{
				$FormName = 'xxx';
				$GradeCategory = 'xxx';
			}
			else
			{
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$FormName = $StudentInfoArr[0]['ClassLevelName'];
				$GradeCategory = $this->Get_Student_Grade_Category($ReportID, $StudentID);
			}
		}
		
		
		$x = '';
		
		if ($ReportType == 'W')
		{
		    $term1Ratio = 40;
            $term2Ratio = 60;
            if($this->schoolYear == '2019') {
                $term1Ratio = 50;
                $term2Ratio = 50;
            }

			### Term Ratio and Academic Performance in Grade
			$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">'."\n";
				$x .= '<tr>'."\n";
					//$x .= '<td class="font_11px" style="width:15%;">'.$eReportCard['Template']['Term1En'].' '.$eReportCard['Template']['GradeSeparator'].' 40%</td>'."\n";
					//2014-0710-1548-29073 point 3
					//$x .= '<td class="font_11px">'.$eReportCard['Template']['TermP+2En'].' '.$eReportCard['Template']['GradeSeparator'].' 60%</td>'."\n";
					//$x .= '<td class="font_11px">'.$eReportCard['Template']['Term2En'].' '.$eReportCard['Template']['GradeSeparator'].' 60%</td>'."\n";
                    // [2020-0608-0931-27235]
                    $x .= '<td class="font_11px" style="width:15%;">'.$eReportCard['Template']['Term1En'].' '.$eReportCard['Template']['GradeSeparator'].' '.$term1Ratio.'%</td>'."\n";
                    $x .= '<td class="font_11px">'.$eReportCard['Template']['Term2En'].' '.$eReportCard['Template']['GradeSeparator'].' '.$term2Ratio.'%</td>'."\n";
					$x .= '<td class="font_11px_bold" style="text-align:right;">'.$eReportCard['Template']['AcademicPerformanceInEn'].$FormName.': '.$GradeCategory.'</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
		}
		
		### Grade Remarks and Category Remarks
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">'."\n";
			$x .= '<tr>'."\n";
				foreach ((array)$eReportCard['Template']['GradeRemarksArr'] as $thisKey => $thisDescription)
				{
					$x .= '<td class="font_9px" style="width:1%;white-space:nowrap;">'.$thisKey.'-'.$thisDescription.'</td>'."\n";
					
					$thisWidth = ($ReportType == 'T')? 10 : 5;
					$x .= '<td style="width:'.$thisWidth.'%;">&nbsp;</td>'."\n";
				}
				
				$thisRemarks = ($ReportType == 'T')? '&nbsp;' : $eReportCard['Template']['CategoryRemarksEn'];
				$x .= '<td class="font_9px" style="width:35%;text-align:right;">'.$thisRemarks.'</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		return $x;
	}
	
	function Get_Student_Grade_Category($ReportID, $StudentID)
	{
		global $eReportCard;
		
		$ResultArr = $this->getReportResultScore($ReportID, 0, $StudentID, '', 0, 1);
		
		//$FormRanking = $ResultArr['OrderMeritForm'];
		//$FormNumOfStudent = $ResultArr['FormNoOfStudent'];
		$FormRanking = $ResultArr['OrderMeritStream'];
		$FormNumOfStudent = $ResultArr['StreamNoOfStudent'];
		
		$CategoryGrade = '';
		if ($FormNumOfStudent > 0)
		{
			$FormPercentage = ($FormRanking / $FormNumOfStudent) * 100;
		
			foreach ((array)$eReportCard['Template']['GradeCategoryArr'] as $thisLowerLimit => $thisCategoryDisplay)
			{
				if ($FormPercentage <= $thisLowerLimit)
				{
					$CategoryGrade = $thisCategoryDisplay;
					break;
				}
			}
		}
		
		return $CategoryGrade;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
//		global $eReportCard;				
//		# Retrieve Display Settings
//		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
//		$SemID 				= $ReportSetting['Semester'];
// 		$ReportType 		= $SemID == "F" ? "W" : "T";		
// 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
//		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
//		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
//		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
//		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
//		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
//		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
//		
//		# updated on 08 Dec 2008 by Ivan
//		# if subject overall column is not shown, grand total, grand average... also cannot be shown
//		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
//		$ShowRightestColumn = $ShowSubjectOverall;
//		
//		# Retrieve Calculation Settings
//		$CalSetting = $this->LOAD_SETTING("Calculation");
//		$UseWeightedMark = $CalSetting['UseWeightedMark'];
//		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
//		
//		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
//		$SubjectDecimal = $StorageSetting["SubjectScore"];
//		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
//		
//		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
//		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
//		if(sizeof($MainSubjectArray) > 0)
//			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
//		
//		$n = 0;
//		$x = array();
//		$isFirst = 1;
//				
//		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
//		if($ReportType=="T")	# Temrs Report Type
//		{
//			$CalculationOrder = $CalSetting["OrderTerm"];
//			
//			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
//			$ColumnID = array();
//			$ColumnTitle = array();
//			if(sizeof($ColoumnTitle) > 0)
//				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
//							
//			foreach($SubjectArray as $SubjectID => $SubjectName)
//			{
//				$isSub = 0;
//				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
//				
//				// check if it is a parent subject, if yes find info of its components subjects
//				$CmpSubjectArr = array();
//				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
//				if (!$isSub) {
//					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
//					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
//				}
//				
//				# define css
//				$css_border_top = ($isSub)? "" : "border_top";
//		
//				# Retrieve Subject Scheme ID & settings
//				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
//				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
//				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
//				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
//				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
//				
//				$isAllNA = true;
//									
//				# Assessment Marks & Display
//				for($i=0;$i<sizeof($ColumnID);$i++)
//				{
//					$thisColumnID = $ColumnID[$i];
//					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
//					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
//					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
//					
//					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
//					$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
//
//					if ($isSub && $columnSubjectWeightTemp == 0)
//					{
//						$thisMarkDisplay = $this->EmptySymbol;
//					}
//					else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
//						$thisMarkDisplay = $this->EmptySymbol;
//					} else {
//						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
//						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
//						
//						$thisMSGrade = $MarksAry[$SubjectID][$ColumnID[$i]]['Grade'];
//						$thisMSMark = $MarksAry[$SubjectID][$ColumnID[$i]]['Mark'];
//						
//						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//						
//						# for preview purpose
//						if(!$StudentID)
//						{
//							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
//							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
//						}
//						
//						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
//						
//						if ($thisMark != "N.A.")
//						{
//							$isAllNA = false;
//						}
//						
//						# check special case
//						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
//						
//						if($needStyle)
//						{
//							if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
//							{
//								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
//							}
//							else
//							{
//								$thisMarkTemp = $thisMark;
//							}
//							
//							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
//						}
//						else
//						{
//							$thisMarkDisplay = $thisMark;
//						}
//						
//					//	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
//					//	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
//					//	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark; 
//					}
//						
//					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
//					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
//  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
//  					
//  					if($ShowSubjectFullMark)
//  					{
//	  					# check special full mark
//	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
//	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
//	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
//	  					
//						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
//						$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
//					}
//					
//					$x[$SubjectID] .= "</tr></table>";
//					$x[$SubjectID] .= "</td>";
//				}
//								
//				# Subject Overall (disable when calculation method is Vertical-Horizontal)
//				if($ShowSubjectOverall)
//				{
//					$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
//					$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];
//
//					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//					
//					# for preview purpose
//					if(!$StudentID)
//					{
//						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
//						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
//					}
//					
//					#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
//					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
//										
//					if ($thisMark != "N.A.")
//					{
//						$isAllNA = false;
//					}
//						
//					# check special case
//					if ($CalculationMethod==2 && $isSub)
//					{
//						$thisMarkDisplay = $this->EmptySymbol;
//					}
//					else
//					{
//						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
//						if($needStyle)
//						{
//	 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
//								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
//							else
//								$thisMarkTemp = $thisMark;
//								
//							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
//						}
//						else
//							$thisMarkDisplay = $thisMark;
//					}
//					
//					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
//					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
//  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
//  					if($ShowSubjectFullMark)
//  					{
//	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
//	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
//	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
//						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $thisFullMark .")</td>";
//					}
//					$x[$SubjectID] .= "</tr></table>";
//					$x[$SubjectID] .= "</td>";
//				} else if ($ShowRightestColumn) {
//					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
//				}
//				$isFirst = 0;
//				
//				# construct an array to return
//				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
//				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
//			}
//		} # End if($ReportType=="T")
//		else					# Whole Year Report Type
//		{
//			$CalculationOrder = $CalSetting["OrderFullYear"];
//			
//			# Retrieve Invloved Temrs
//			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
//			
//			foreach($SubjectArray as $SubjectID => $SubjectName)
//			{
//				# Retrieve Subject Scheme ID & settings
//				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
//				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
//				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
//				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
//				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
//							
//				$t = "";
//				$isSub = 0;
//				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
//				
//				// check if it is a parent subject, if yes find info of its components subjects
//				$CmpSubjectArr = array();
//				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
//				if (!$isSub) {
//					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
//					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
//				}
//				
//				$isAllNA = true;
//				
//				# Terms's Assesment / Terms Result
//				for($i=0;$i<sizeof($ColumnData);$i++)
//				{
//					#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
//					$css_border_top = $isSub? "" : "border_top";
//					
//					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
//					if($isDetails==1)		# Retrieve assesments' marks
//					{
//						# See if any term reports available
//						$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum'] ."' and ClassLevelID = '".$ClassLevelID."' ");
//						
//						# if no term reports, CANNOT retrieve assessment result at all
//						if (empty($thisReport)) {
//							for($j=0;$j<sizeof($ColumnID);$j++) {
//								$x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
//							}
//						} else {
//							$thisReportID = $thisReport['ReportID'];
//							
//							$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
//							$ColumnID = array();
//							$ColumnTitle = array();
//							foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
//							
//							$thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
//							
//							for($j=0;$j<sizeof($ColumnID);$j++)
//							{
//								$thisColumnID = $ColumnID[$j];
//								$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
//								$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
//								$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
//								
//								$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
//								$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
//					
//								if ($isSub && $columnSubjectWeightTemp == 0)
//								{
//									$thisMarkDisplay = $this->EmptySymbol;
//								}
//								else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
//									$thisMarkDisplay = $this->EmptySymbol;
//								} else {
//									$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnID[$j] ."' and SubjectID = '$SubjectID' ");
//									$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
//									
//									$thisMSGrade = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade'];
//									$thisMSMark = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Mark'];
//									
//									$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//									$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//									
//									# for preview purpose
//									if(!$StudentID)
//									{
//										$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
//										$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
//									}
//									$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
//													
//									if ($thisMark != "N.A.")
//									{
//										$isAllNA = false;
//									}
//									
//									# check special case
//									list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
//									if($needStyle)
//									{
//										if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
//										{
//											$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
//										}
//										else
//										{
//											$thisMarkTemp = $thisMark;
//										}
//										
//										$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
//									}
//									else
//									{
//										$thisMarkDisplay = $thisMark;
//									}
//								}
//									
//								$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
//								$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
//			  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
//			  					
//			  					if($ShowSubjectFullMark)
//			  					{
//				  					$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
//									$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
//								}
//								
//								$x[$SubjectID] .= "</tr></table>";
//								$x[$SubjectID] .= "</td>";
//							}
//						}
//					}
//					else					# Retrieve Terms Overall marks
//					{
//						if ($isParentSubject && $CalculationOrder == 1) {
//							$thisMarkDisplay = $this->EmptySymbol;
//						} else {
//							# See if any term reports available
//							$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
//							
//							# if no term reports, the term mark should be entered directly
//							
//							if (empty($thisReport)) {
//								$thisReportID = $ReportID;
//								$thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
//								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
//								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
//								//$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
//								
//								$thisMSGrade = $thisMarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"];
//								$thisMSMark = $thisMarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"];
//								
//								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//									
//							} else {
//								$thisReportID = $thisReport['ReportID'];
//								$thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
//								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
//								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
//								//$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
//								
//								$thisMSGrade = $thisMarksAry[$SubjectID][0]["Grade"];
//								$thisMSMark = $thisMarksAry[$SubjectID][0]["Mark"];
//								
//								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//							}
//							
//							$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
//							$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
//														
//							# for preview purpose
//							if(!$StudentID)
//							{
//								$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
//								$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
//							}
//							# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
//							$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
//							
//							if ($thisMark != "N.A.")
//							{
//								$isAllNA = false;
//							}
//						
//							# check special case
//							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
//							if($needStyle)
//							{
//								$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
//							}
//							else
//							{
//								$thisMarkDisplay = $thisMark;
//							}
//						}
//							
//						$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
//						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
//	  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
//	  					
//	  					if($ShowSubjectFullMark)
//	  					{
// 							$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1 && $UseWeightedMark) ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID];
//							$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
//						}
//						
//						$x[$SubjectID] .= "</tr></table>";
//						$x[$SubjectID] .= "</td>";
//					}
//				}
//				
//				# Subject Overall
//				if($ShowSubjectOverall)
//				{
//					$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
//					$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
//					
//					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//					
//					# for preview purpose
//					if(!$StudentID)
//					{
//						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
//						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
//					}
//					
//					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
//					
//					if ($thisMark != "N.A.") 
//					{
//						$isAllNA = false;
//					}
//						
//					if ($CalculationMethod==2 && $isSub)
//					{
//						$thisMarkDisplay = $this->EmptySymbol;
//					}
//					else
//					{
//						# check special case
//						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
//						if($needStyle)
//						{
//							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
//						}
//						else
//							$thisMarkDisplay = $thisMark;
//					}
//						
//					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
//					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
//  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
//  					
//  					if($ShowSubjectFullMark)
//  					{
//	  					# check special full mark
//	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
//	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
//	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
//	  					
//						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark * $thisSubjectWeight : $thisFullMark) : $thisFullMark;
//						$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
//					}
//  					
//					$x[$SubjectID] .= "</tr></table>";
//					$x[$SubjectID] .= "</td>";
//					
//					
//				} else if ($ShowRightestColumn) {
//					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
//				}
//				
//				$isFirst = 0;
//				
//				# construct an array to return
//				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
//				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
//			}
//		}	# End Whole Year Report Type
//		
//		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
//		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
//		
//		if ($eRCTemplateSetting['HideCSVInfo'] == true)
//		{
//			return "";
//		}
//			
//		
//		# Retrieve Basic Information of Report
//		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
//		$LineHeight					= $ReportSetting['LineHeight'];
//		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
//		
//		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
//
//		//modified by marcus 20/8/2009
//		//return: $ReturnArr[$StudentID][$YearTermID][$UploadType] = Value
//		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID); 
//		
//		# retrieve the latest Term
//		$latestTerm = "";
//		$sems = $this->returnReportInvolvedSem($ReportID);
//		foreach($sems as $TermID=>$TermName)
//			$latestTerm = $TermID;
//		//$latestTerm++;
//		
//		/*
//		# retrieve Student Class
//		if($StudentID)
//		{
//			include_once($PATH_WRT_ROOT."includes/libuser.php");
//			$lu = new libuser($StudentID);
//			$ClassName 		= $this->Get_Student_ClassName($StudentID);
//			$WebSamsRegNo 	= $lu->WebSamsRegNo;
//		}
//		
//		# build data array
//		$ary = array();
//		$csvType = $this->getOtherInfoType();
//		foreach($csvType as $k=>$Type)
//		{
//			$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);	
//			if(!empty($csvData)) 
//			{
//				foreach($csvData as $RegNo=>$data)
//				{
//					if($RegNo == $WebSamsRegNo)
//					{
//	 					foreach($data as $key=>$val)
//		 					$ary[$key] = $val;
//					}
//				}
//			}
//		}
//		*/
//		
//		# retrieve result data
//		$ary = $OtherInfoDataAry[$StudentID][$latestTerm];
//		
//		$Absence = ($ary['Days Absent'])? $ary['Days Absent'] : 0;
//		$Lateness = ($ary['Time Late'])? $ary['Time Late'] : 0;
//		$EarlyLeave = ($ary['Early Leave'])? $ary['Early Leave'] : 0;
//		$Promotion = ($ary['Promotion'])? $ary['Promotion'] : $this->EmptySymbol;
//		$Merits = ($ary['Merits'])? $ary['Merits'] : 0;
//		$MinorCredit = ($ary['Minor Credit'])? $ary['Minor Credit'] : 0;
//		$MajorCredit = ($ary['Major Credit'])? $ary['Major Credit'] : 0;
//		$Demerits = ($ary['Merits'])? $ary['Demerits'] : 0;
//		$MinorFault = ($ary['Minor Fault'])? $ary['Minor Fault'] : 0;
//		$MajorFault = ($ary['Major Fault'])? $ary['Major Fault'] : 0;
//		$Remark = ($ary['Remark'])? $ary['Remark'] : $this->EmptySymbol;
//		$ECA = ($ary['ECA'])? $ary['ECA'] : $this->EmptySymbol;
//		if (is_array($ECA))
//			$ecaList = implode("<br>", $ECA);
//		else
//			$ecaList = $ECA;
//		
//		
//		$merit = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
//		$merit .= "<tr>";
//		$merit .= "<td class='tabletext'>".$eReportCard['Template']['Merits'] . ": " . ($ary['Merits'] ? $ary['Merits'] :0) ."</td>";
//		$merit .= "<td class='tabletext'>".$eReportCard['Template']['Demerits'] . ": " . ($ary['Demerits'] ? $ary['Demerits'] : 0) ."</td>";
//		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorCredit'] . ": " . ($ary['Minor Credit'] ? $ary['Minor Credit'] :0) ."</td>";
//		$merit .= "</tr>";
//		$merit .= "<tr>";
//		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorCredit'] . ": " . ($ary['Major Credit'] ? $ary['Major Credit'] : 0) ."</td>";
//		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorFault'] . ": " . ($ary['Minor Fault'] ? $ary['Minor Fault'] : 0) ."</td>";
//		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorFault'] . ": " . ($ary['Major Fault'] ? $ary['Major Fault'] : 0) ."</td>";
//		$merit .= "</tr>";
//		$merit .= "</table>";
//		$remark = $ary['Remark'];
//		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
//		$classteachercomment = $CommentAry[0];
//		$eca = $ary['ECA'];
//		if (is_array($eca))
//			$ecaList = implode("<br>", $eca);
//		else
//			$ecaList = $eca;
//		
//		$x .= "<br>";
//		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='60' class='report_border'>";
//		$x .= "<tr>";
//		$x .= "<td width='50%' valign='top'>";
//			# Merits & Demerits 
//			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
//			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
//			$x .= "<tr>";
//				$x .= "<td class='tabletext'><b>". $eReportCard['Template']['MeritsDemerits']."</b><br>".$merit."</td>";
//			$x .= "</tr>";
//			$x .= "</table>";	
//			$x .= "</td></tr></table>";	
//		$x .="</td>";
//		$x .= "<td width='50%' class=\"border_left\" valign='top'>";
//			# Remark 
//			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
//			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
//			$x .= "<tr>";
//				$x .= "<td class='tabletext'><b>". $eReportCard['Template']['Remark']."</b><br>".$remark."</td>";
//			$x .= "</tr>";
//			$x .= "</table>";	
//			$x .= "</td></tr></table>";	
//		$x .="</td>";
//		$x .= "</tr>";
//		$x .= "</table>";
//		
//		$x .= "<br>";
//		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='60' class='report_border'>";
//		$x .= "<tr>";
//		if($AllowClassTeacherComment)
//		{
//			$x .= "<td width='50%' valign='top'>";
//				# Class Teacher Comment 
//				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
//				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
//				$x .= "<tr>";
//					$x .= "<td class='tabletext'><b>". $eReportCard['Template']['ClassTeacherComment']."</b><br>".stripslashes(nl2br($classteachercomment))."</td>";
//				$x .= "</tr>";
//				$x .= "</table>";	
//				$x .= "</td></tr></table>";	
//			$x .="</td>";
//			
//			$x .= "<td width='50%' class='border_left' valign='top'>";
//		} else {
//			$x .= "<td width='50%' valign='top'>";
//		}
//			# ECA 
//			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
//			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
//			$x .= "<tr>";
//				$x .= "<td class='tabletext'><b>". $eReportCard['Template']['eca']."</b><br>".$ecaList."</td>";
//			$x .= "</tr>";
//			$x .= "</table>";	
//			$x .= "</td></tr></table>";	
//		$x .="</td>";
//		$x .= "</tr>";
//		$x .= "</table>";
//		
//		return $x;
	}
	
	
	function Get_Generic_Skills_Table($ReportID, $StudentID, $OtherInfoArr)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID); 
		$ClassLevelID 	= $ReportSetting['ClassLevelID'];
		$SemID 			= $ReportSetting['Semester'];
 		$ReportType 	= $SemID == "F" ? "W" : "T";
 		$TargetTerm 	= ($ReportType == 'T')? $SemID : 0;
 		
// 		$CooperationGrade = ($OtherInfoArr[$TargetTerm]['Cooperation with Others Grade']=='')? '&nbsp;' : $OtherInfoArr[$TargetTerm]['Cooperation with Others Grade'];
// 		$HomeworkGrade = ($OtherInfoArr[$TargetTerm]['Homework Completion Grade']=='')? '&nbsp;' : $OtherInfoArr[$TargetTerm]['Homework Completion Grade'];
// 		$ConflictGrade = ($OtherInfoArr[$TargetTerm]['Conflict Resolution Grade']=='')? '&nbsp;' : $OtherInfoArr[$TargetTerm]['Conflict Resolution Grade'];
// 		$ParticipationGrade = ($OtherInfoArr[$TargetTerm]['Class Participation Grade']=='')? '&nbsp;' : $OtherInfoArr[$TargetTerm]['Class Participation Grade'];
 		$InitiativeGrade = ($OtherInfoArr[$TargetTerm]['Initiative Grade']=='')? '&nbsp;' : $OtherInfoArr[$TargetTerm]['Initiative Grade'];
		$CollaborationGrade = ($OtherInfoArr[$TargetTerm]['Collaboration with Others Grade']=='')? '&nbsp;' : $OtherInfoArr[$TargetTerm]['Collaboration with Others Grade'];
		$ResponsibilityGrade = ($OtherInfoArr[$TargetTerm]['Sense of Responsibility Grade']=='')? '&nbsp;' : $OtherInfoArr[$TargetTerm]['Sense of Responsibility Grade'];
		$ManagementGrade = ($OtherInfoArr[$TargetTerm]['Self-management Grade']=='')? '&nbsp;' : $OtherInfoArr[$TargetTerm]['Self-management Grade'];
		$RespectGrade = ($OtherInfoArr[$TargetTerm]['Respect for Others Grade']=='')? '&nbsp;' : $OtherInfoArr[$TargetTerm]['Respect for Others Grade'];
 		$ProblemSolvingGrade = ($OtherInfoArr[$TargetTerm]['Problem Solving Grade']=='')? '&nbsp;' : $OtherInfoArr[$TargetTerm]['Problem Solving Grade'];
 		
 		$SkillsCommentArr = array();
// 		if ($OtherInfoArr[$TargetTerm]['Cooperation with Others Comment'] != '')
// 			$SkillsCommentArr[] = $OtherInfoArr[$TargetTerm]['Cooperation with Others Comment'];
// 		if ($OtherInfoArr[$TargetTerm]['Homework Completion Comment'] != '')
// 			$SkillsCommentArr[] = $OtherInfoArr[$TargetTerm]['Homework Completion Comment'];
// 		if ($OtherInfoArr[$TargetTerm]['Conflict Resolution Comment'] != '')
// 			$SkillsCommentArr[] = $OtherInfoArr[$TargetTerm]['Conflict Resolution Comment'];
// 		if ($OtherInfoArr[$TargetTerm]['Class Participation Comment'] != '')
// 			$SkillsCommentArr[] = $OtherInfoArr[$TargetTerm]['Class Participation Comment'];
 		if ($OtherInfoArr[$TargetTerm]['Initiative Comment'] != '')
 			$SkillsCommentArr[] = $OtherInfoArr[$TargetTerm]['Initiative Comment'];
 		if ($OtherInfoArr[$TargetTerm]['Collaboration with Others Comment'] != '')
 			$SkillsCommentArr[] = $OtherInfoArr[$TargetTerm]['Collaboration with Others Comment'];
 		if ($OtherInfoArr[$TargetTerm]['Sense of Responsibility Comment'] != '')
 			$SkillsCommentArr[] = $OtherInfoArr[$TargetTerm]['Sense of Responsibility Comment'];
 		if ($OtherInfoArr[$TargetTerm]['Self-management Comment'] != '')
 			$SkillsCommentArr[] = $OtherInfoArr[$TargetTerm]['Self-management Comment'];
 		if ($OtherInfoArr[$TargetTerm]['Respect for Others Comment'] != '')
 			$SkillsCommentArr[] = $OtherInfoArr[$TargetTerm]['Respect for Others Comment'];
 		if ($OtherInfoArr[$TargetTerm]['Problem Solving Comment'] != '')
 			$SkillsCommentArr[] = $OtherInfoArr[$TargetTerm]['Problem Solving Comment'];
 		$numOfSkillsComment = count($SkillsCommentArr);
 		
 		$TitleWidth = '17';
 		$GradeWidth = '7';
 		$CommentWidth = 100 - ($TitleWidth * 2) - ($GradeWidth * 2);
 		
 		$TableHeight = 148;
 		$TitleHeight = 23;
 		$RowHeight = floor(($TableHeight - $TitleHeight) / 3);
 		 		
 		$x = '';
		$x .= '<table width="100%" height="'.$TableHeight.'px" border="0" cellspacing="0" cellpadding="0" class="double_border_table">'."\n";
			$x .= '<tr><td colspan="5" class="font_13px_bold" style="padding-left:5px;height:'.$TitleHeight.'px;">'.$eReportCard['Template']['GenericSkillsEn'].' / '.$eReportCard['Template']['ValuesAndAttitudesEn'].' **</td></tr>'."\n";
			$x .= '<tr style="height:'.$RowHeight.'px;">'."\n";
				$x .= '<td class="font_11px border_top" style="padding-left:5px;width:'.$TitleWidth.'%;">'.$eReportCard['Template']['InitiativeEn'].'</td>'."\n";
				$x .= '<td class="font_11px_bold border_top border_left" style="text-align:center;width:'.$GradeWidth.'%;">'.$InitiativeGrade.'</td>'."\n";
				$x .= '<td class="font_11px border_top border_left" style="padding-left:5px;width:'.$TitleWidth.'%;">'.$eReportCard['Template']['CollaborationWithOthersEn'].'</td>'."\n";
				$x .= '<td class="font_11px_bold border_top border_left" style="text-align:center;width:'.$GradeWidth.'%;">'.$CollaborationGrade.'</td>'."\n";
				$x .= '<td rowspan="3" class="border_top border_left" style="padding-left:5px;padding-top:5px;width:'.$CommentWidth.'%;height:80%;">'."\n"; // 20130704 Siuwan [2013-0704-1025-43073] Add height:80%
					# Comments
					if ($numOfSkillsComment == 0)
						$x .= '&nbsp;';
					else
					{
						$x .= '<table border="0" cellspacing="0" cellpadding="0" style="width:100%;height:100%;">'."\n";
							for ($i=0; $i<$numOfSkillsComment; $i++)
							{
								$thisDisplay = $SkillsCommentArr[$i];
								$x .= '<tr><td class="font_11px" style="vertical-align:top;">'.$thisDisplay.'</td></tr>'."\n";
							}
						$x .= '</table>'."\n";
					}
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr style="height:'.$RowHeight.'px;">'."\n";
				$x .= '<td class="font_11px border_top" style="padding-left:5px;">'.$eReportCard['Template']['SenseOfResponsibilityEn'].'</td>'."\n";
				$x .= '<td class="font_11px_bold border_top border_left" style="text-align:center;">'.$ResponsibilityGrade.'</td>'."\n";
				$x .= '<td class="font_11px border_top border_left" style="padding-left:5px;">'.$eReportCard['Template']['SelfManagementEn'].'</td>'."\n";
				$x .= '<td class="font_11px_bold border_top border_left" style="text-align:center;">'.$ManagementGrade.'</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr style="height:'.$RowHeight.'px;">'."\n";
				$x .= '<td class="font_11px border_top" style="padding-left:5px;">'.$eReportCard['Template']['RespectForOthersEn'].'</td>'."\n";
				$x .= '<td class="font_11px_bold border_top border_left" style="text-align:center;">'.$RespectGrade.'</td>'."\n";
				$x .= '<td class="font_11px border_top border_left" style="padding-left:5px;">'.$eReportCard['Template']['ProblemSolvingEn'].'</td>'."\n";
				$x .= '<td class="font_11px_bold border_top border_left" style="text-align:center;">'.$ProblemSolvingGrade.'</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		$x .= '<div>'.$this->Get_Empty_Image('5px').'</div>';
		
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td class="font_11px" style="width:2%;">**</td>'."\n";
				foreach ((array)$eReportCard['Template']['SkillsGradeRemarksArr'] as $thisKey => $thisDescription)
					$x .= '<td class="font_11px">'.$thisKey.' '.$eReportCard['Template']['GradeSeparator'].' '.$thisDescription.'</td>'."\n";
				$x .= '<td style="width:25%;">&nbsp;</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_OLE_Table($ReportID, $StudentID, $OtherInfoArr)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID); 
		$ClassLevelID 	= $ReportSetting['ClassLevelID'];
		$SemID 			= $ReportSetting['Semester'];
 		$ReportType 	= $SemID == "F" ? "W" : "T";
 		$TargetTerm 	= ($ReportType == 'T')? $SemID : 0;
		
		$TableHeight = 235;
		$TitleHeight = 18;
		$SubTitleHeight = 15;
		
		//2012-0206-1620-05069
//		$ActivityArr = (array)$OtherInfoArr[$TargetTerm]['Activity / Society / Club / Team'];
//		$numOfActivity = count($ActivityArr);
//		$ServiceArr = (array)$OtherInfoArr[$TargetTerm]['School and Community Services'];
//		$numOfService = count($ServiceArr);
		$YearTermIDArr = array();
		if ($ReportType=='T') {
			$YearTermIDArr[] = $SemID;
		}
		else {
			//2012-1012-0955-50073
			//$YearTermIDArr = Get_Array_By_Key($this->Get_Related_TermReport_Of_Consolidated_Report($ReportID), 'Semester');
			$YearTermIDArr[] = $this->Get_Last_Semester_Of_Report($ReportID);
		}
		
		include_once($PATH_WRT_ROOT.'includes/libclass.php');
		$yearID = $this->GET_ACTIVE_YEAR_ID();
		$studentInfo = $this->Get_Student_Class_ClassLevel_Info($StudentID);
		$classID = $studentInfo['ClassID'];
// 		$sql = "SELECT yc.YearClassID FROM YEAR_CLASS yc INNER JOIN YEAR_CLASS_USER ycu ON yc.YearClassID = ycu.YearClassID WHERE ycu.UserID = '$StudentID' AND yc.AcademicYearID = '$yearID' ";
// 		$classAry = $this->returnVector($sql);
// 		$classID = $classAry[0];
// 		$lclass = new libclass();
// 		$className = $lclass->getClassByStudentID($StudentID);
// 		$classID = $lclass->getClassID($className);
		$csvData = $this->getOtherInfoData('OLE', $TargetTerm, $classID);
		$thisOLEData = $csvData[$StudentID]; // Activity / Society / Club / Team
		
		$enrollmentInfoAry = $this->Get_eEnrolment_Data($StudentID, $YearTermIDArr);
		$numOfClub = count($enrollmentInfoAry);
		$ActivityArr = array();
		for ($i=0; $i<$numOfClub; $i++) {
			$_clubTitle = $enrollmentInfoAry[$i]['ClubTitle'];
			$_roleTitle = $enrollmentInfoAry[$i]['RoleTitle'];
			$_attendance = $enrollmentInfoAry[$i]['AttendancePercentageRounded'];
			$_numOfMeetingDate = $enrollmentInfoAry[$i]['NumOfTotalMeetingDate'];
			
			$_ecaDisplay = '';
			$_ecaDisplay .= $_clubTitle;
			
			//2012-1119-1417-39073: If no meeting date settings for Club => don't show attendance remarks
			if ($_numOfMeetingDate > 0) {
				// 3.1	After the club name need to show the club attendance in eEnrolment
				if (floatcmp($_attendance, '==', (float)100)) {
					// If 100% attendance, show "- 100% attendance"
					$_ecaDisplay .= ' - 100% attendance';
				}
				else if (floatcmp($_attendance, '>=', (float)50) && floatcmp($_attendance, '<', (float)100)) {
					// If 50 – 99.9% attendance, no need to show
				}
				else if (floatcmp($_attendance, '>=', (float)10) && floatcmp($_attendance, '<', (float)50)) {
					// If 10 – 49.9% attendance, show "- Less than 50% attendance"
					$_ecaDisplay .= ' - Less than 50% attendance';
				}
				else if (floatcmp($_attendance, '<', (float)10)) {
					// If less than 10% attendance, show "- Less than 10% attendance"
					$_ecaDisplay .= ' - Less than 10% attendance';
				}
			}
			
			// 3.2	After the club attendance information, show the title of the club except “Member”
			// [S93081] [2016-0223-1547-03073] - added Chinese 會員 case
			if (strtolower($_roleTitle) == 'member' || $_roleTitle == '會員') {
				// do nth
			}
			else {
				$_ecaDisplay .= ' - '.$_roleTitle;
			}
			
			$ActivityArr[] = $_ecaDisplay;
		}
		$numOfActivity = count($ActivityArr);
		
		$studentProfileAry = $this->Get_Student_Profile_Data($ReportID, $StudentID, $MapByRecordType=false, $CheckRecordStatus=true, $returnTotalUnit=false, $this->Get_Last_Semester_Of_Report($ReportID));
		$ServiceArr = $studentProfileAry['ServiceAry'];
		$numOfService = count($ServiceArr);
		
		
		$x = '';
		$x .= '<table width="100%" height="'.$TableHeight.'px" border="0" cellspacing="0" cellpadding="0" class="double_border_table">'."\n";
			$x .= '<tr><td class="font_13px_bold" style="padding-left:5px;height:'.$TitleHeight.'px;">'.$eReportCard['Template']['OtherLearningExperiencesEn'].'</td></tr>'."\n";
			
			# Activity / Society / Club / Team
			$x .= '<tr><td class="font_11px border_top" style="padding-left:5px;height:'.$SubTitleHeight.'px;">'.$eReportCard['Template']['ActivitySocietyClubTeamEn'].'</td></tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td class="border_top" style="padding-left:3px;vertical-align:top;">'."\n";
					$x .= '<table border="0" cellspacing="0" cellpadding="2" style="width:100%;">'."\n";
						if($thisOLEData['Activity / Society / Club / Team']){ // Show Other Info First
							$x .= '<tr><td class="font_11px" style="vertical-align:top;">'.$thisOLEData['Activity / Society / Club / Team'].'</td></tr>'."\n";
						}
						for ($i=0; $i<$numOfActivity; $i++)
						{
							$thisDisplay = $ActivityArr[$i];
							$x .= '<tr><td class="font_11px" style="vertical-align:top;">'.$thisDisplay.'</td></tr>'."\n";
						}
						$x .= '<tr><td class="font_11px" style="vertical-align:top;">'.$this->EmptySymbol.'</td></tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# School and Community Services
			$x .= '<tr><td class="font_11px border_top" style="padding-left:5px;height:'.$SubTitleHeight.'px;">'.$eReportCard['Template']['SchoolAndCommunityServicesEn'].'</td></tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td class="border_top" style="padding-left:3px;vertical-align:top;">'."\n";
					$x .= '<table border="0" cellspacing="0" cellpadding="2" style="width:100%;">'."\n";
						for ($i=0; $i<$numOfService; $i++)
						{
							//$thisDisplay = $ServiceArr[$i];
							$thisDisplay = $ServiceArr[$i]['serviceName'];
							
							$x .= '<tr><td class="font_11px" style="vertical-align:top;">'.$thisDisplay.'</td></tr>'."\n";
						}
						$x .= '<tr><td class="font_11px" style="vertical-align:top;">'.$this->EmptySymbol.'</td></tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";		
		return $x;
	}
	
	function Get_Awards_Table($ReportID, $StudentID, $OtherInfoArr)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID); 
		$ClassLevelID 	= $ReportSetting['ClassLevelID'];
		$IsMainReport 	= $ReportSetting['isMainReport'];
		$SemID 			= $ReportSetting['Semester'];
 		$ReportType 	= $SemID == "F" ? "W" : "T";
 		$TargetTerm 	= ($ReportType == 'T')? $SemID : 0;
		
		$TableHeight = ($IsMainReport)? 151 : 106;
		$TitleHeight = 30;
		$ContentHeight = $TableHeight - $TitleHeight;
		
		if ($IsMainReport)
			$AwardArr = (array)$OtherInfoArr[$TargetTerm]['Awards and Achievement'];
		else
			$AwardArr = (array)$OtherInfoArr[$TargetTerm]['Awards (Project)'];
		$numOfAward = count($AwardArr);
		
		$x = '';
		$x .= '<table width="100%" height="'.$TableHeight.'px" border="0" cellspacing="0" cellpadding="0" class="double_border_table">'."\n";
			$x .= '<tr><td class="font_13px_bold" style="padding-left:5px;height:'.$TitleHeight.'px;">'.$eReportCard['Template']['AwardsAndAchievementEn'].':</td></tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td style="padding-left:3px;vertical-align:top;height:'.$ContentHeight.'px">'."\n";
					$x .= '<table border="0" cellspacing="0" cellpadding="2" style="width:100%;">'."\n";
						for ($i=0; $i<$numOfAward; $i++)
						{
							$thisDisplay = $AwardArr[$i];
							$x .= '<tr><td class="font_11px" style="vertical-align:top;">'.$thisDisplay.'</td></tr>'."\n";
						}
						$x .= '<tr><td class="font_11px" style="vertical-align:top;">'.$this->EmptySymbol.'</td></tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		return $x;
	}
	
	function Get_Conduct_Table($ReportID, $StudentID, $OtherInfoArr)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID); 
		$ClassLevelID 	= $ReportSetting['ClassLevelID'];
		$SemID 			= $ReportSetting['Semester'];
 		$ReportType 	= $SemID == "F" ? "W" : "T";
 		$TargetTerm 	= ($ReportType == 'T')? $SemID : 0;
		
		$TableHeight = ($ReportType == 'T')? 45 : 71;
		$ConductWidth = ($ReportType == 'T')? 20 : 17;
		$MeritWidth = ($ReportType == 'T')? 40 : 33;
		$DemeritWidth = ($ReportType == 'T')? 40 : 33;
		$PromotionWidth = ($ReportType == 'T')? 0 : 17;
		
		//$Rowspan = ($ReportType == 'T')? '' : 'rowspan="2"';
		$Rowspan = 'rowspan="2"';
		
//		debug_pr($OtherInfoArr);
		
		$Conduct = ($OtherInfoArr[$TargetTerm]['Conduct']=='')? '&nbsp;' : $OtherInfoArr[$TargetTerm]['Conduct'];
		$MeritArr = ($OtherInfoArr[$TargetTerm]['Merits']=='')? '&nbsp;' : $OtherInfoArr[$TargetTerm]['Merits'];
		$DemeritsArr = ($OtherInfoArr[$TargetTerm]['Demerits']=='')? '&nbsp;' : $OtherInfoArr[$TargetTerm]['Demerits'];
		$Promotion = ($OtherInfoArr[$TargetTerm]['Promotion']=='')? '&nbsp;' : $OtherInfoArr[$TargetTerm]['Promotion'];
		
		if (is_array($Conduct))
			$Conduct = $Conduct[0];		// get the first Conduct if there are more than one
		if (is_array($Promotion))
			$Promotion = $Promotion[0];		// get the first Promotion if there are more than one
		
		if (!is_array($MeritArr))
			$MeritArr = array($MeritArr);
		if (!is_array($DemeritsArr))
			$DemeritsArr = array($DemeritsArr);
		
		$x = '';
		$x .= '<table width="100%" height="'.$TableHeight.'px" border="0" cellspacing="0" cellpadding="0" class="double_border_table">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td class="font_13px_bold" style="padding-left:5px; width:'.$ConductWidth.'%;" '.$Rowspan.'>'."\n";
					$x .= $eReportCard['Template']['ConductEn'].'*:'.'&nbsp;&nbsp;&nbsp;'.$Conduct;
				$x .= '</td>'."\n";
				
				//2012-0103-1227-24132
				//if ($ReportType == 'W')
				//{
					$x .= '<td class="font_10px_bold border_left" style="text-align:center;height:15px;width:'.$MeritWidth.'%;">'.$eReportCard['Template']['MeritsEn'].'</td>'."\n";
					$x .= '<td class="font_10px_bold border_left" style="text-align:center;height:15px;width:'.$DemeritWidth.'%;">'.$eReportCard['Template']['DemeritsEn'].'</td>'."\n";
				if ($ReportType == 'W')
				{
					$x .= '<td class="font_13px border_left" style="text-align:center;width:'.$PromotionWidth.'%;" '.$Rowspan.'>'.$Promotion.'</td>'."\n";
				}
			$x .= '</tr>'."\n";
			
			//2012-0103-1227-24132
			//if ($ReportType == 'W')
			//{
				$x .= '<tr>'."\n";
					$x .= '<td class="font_11px border_top_double border_left" style="text-align:center;">'.implode('<br />', $MeritArr).'</td>'."\n";
					$x .= '<td class="font_11px border_top_double border_left" style="text-align:center;">'.implode('<br />', $DemeritsArr).'</td>'."\n";
				$x .= '</tr>'."\n";
		//	}
		$x .= '</table>';
		
		$x .= '<div>'.$this->Get_Empty_Image('5px').'</div>';
		
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">'."\n";
			
			$x .= '<tr>'."\n";
				$x .= '<td class="font_11px" style="width:2%;">*</td>'."\n";
				foreach ((array)$eReportCard['Template']['ConductGradeRemarksArr'] as $thisKey => $thisDescription)
					$x .= '<td class="font_11px">'.$thisKey.' '.$eReportCard['Template']['GradeSeparator'].' '.$thisDescription.'</td>'."\n";
				$x .= '<td style="width:10%;">&nbsp;</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>';
		
		return $x;
	}
	
	function Get_Comment_And_Signature_Table($ReportID, $StudentID)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID); 
		$ClassLevelID 	= $ReportSetting['ClassLevelID'];
		$IsMainReport 	= $ReportSetting['isMainReport'];
		$SemID 			= $ReportSetting['Semester'];
 		$ReportType 	= $SemID == "F" ? "W" : "T";
		
		$TableHeight = ($ReportType == 'T')? (($IsMainReport)? 246 : 200) : 204;
		$TitleHeight = 30;
		$SignatureHeight = 57;
		$ContentHeight = $TableHeight - $TitleHeight - $SignatureHeight;
		
		### Get Class Teacher Comment
		$CommentArr = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$ClassTeacherComment = nl2br($CommentArr[0]);
		
		### Get Class Teacher for Signature
		$ClassTeacherArr = $this->Get_Student_Class_Teacher_Info($StudentID);
		$ClassTeacherArr = Get_Array_By_Key($ClassTeacherArr, 'EnglishName');
		$ClassTeacherDisplay = implode(' /<br />', $ClassTeacherArr);	// Change format Request: 2011-0128-1407-01069
		
		$SignatureArr = $eRCTemplateSetting['Signature'];
		$numOfSignature = count($SignatureArr);
		
		$x = '';
		$x .= '<table width="100%" height="'.$TableHeight.'px" border="0" cellspacing="0" cellpadding="0" class="double_border_table">'."\n";
			if ($IsMainReport)
			{
				### Class Teacher Comment
				$x .= '<tr><td class="font_13px_bold" style="padding-left:5px;height:'.$TitleHeight.'px;">'.$eReportCard['Template']['ClassTeacherCommentEn'].':</td></tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td style="padding-left:3px;vertical-align:top;height:'.$ContentHeight.'px">'."\n";
						$x .= '<table border="0" cellspacing="0" cellpadding="2" style="width:100%;">'."\n";
							$x .= '<tr><td class="font_12px" style="vertical-align:top;">'.$ClassTeacherComment.'</td></tr>'."\n";
							$x .= '<tr><td class="font_12px" style="vertical-align:top;">'.$this->EmptySymbol.'</td></tr>'."\n";
						$x .= '</table>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			}
			
			### Signature
			$x .= '<tr>'."\n";
				$x .= '<td style="padding-left:2px;padding-right:2px;vertical-align:bottom;height:'.$SignatureHeight.'px">'."\n";
					$x .= '<table border="0" cellspacing="0" cellpadding="5" style="width:100%;">'."\n";
						$x .= '<tr>'."\n";
							for ($i=0; $i<$numOfSignature; $i++)
							{
								$thisKey = $SignatureArr[$i];
								
								if ($thisKey == 'ClassTeacher')
									$thisName = $ClassTeacherDisplay;
								else if ($thisKey == 'Principal')
									$thisName = $eReportCard['Template']['PrincipalNameEn'];
								else
									$thisName = '';
									
								$x .= '<td class="font_10px_bold border_top_bold" style="text-align:center;vertical-align:top;width:24%;">'."\n";
									$x .= $eReportCard['Template'][$thisKey];
									$x .= '<br />';
									$x .= '<div>'.$this->Get_Empty_Image('5px').'</div>';
									$x .= $thisName;
									$x .= '<br />';
									$x .= '<div>'.$this->Get_Empty_Image('5px').'</div>';
								$x .= '</td>'."\n";
								
								if ($i < $numOfSignature - 1)
									$x .= '<td style="width:1%;">&nbsp;</td>'."\n";
							}
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Project_Info_Table($ReportID, $StudentID, $OtherInfoArr)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID); 
		$ClassLevelID 	= $ReportSetting['ClassLevelID'];
		$SemID 			= $ReportSetting['Semester'];
 		$ReportType 	= $SemID == "F" ? "W" : "T";
 		$TargetTerm 	= ($ReportType == 'T')? $SemID : 0;
		
		$ProjectTitle = ($OtherInfoArr[$TargetTerm]['Project Title']=='')? '&nbsp;' : $OtherInfoArr[$TargetTerm]['Project Title'];
		$TeacherName = ($OtherInfoArr[$TargetTerm]['Supervising Teacher']=='')? '&nbsp;' : $OtherInfoArr[$TargetTerm]['Supervising Teacher'];
		
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" class="double_border_table">'."\n";
			# Project Title
			$x .= '<tr>'."\n";
				$x .= '<td class="font_12px">'.$eReportCard['Template']['ProjectTitleEn'].': &nbsp;&nbsp;<b>'.$ProjectTitle.'</b></td>'."\n";
			$x .= '</tr>'."\n";
			
			# Supervising Teacher
			$x .= '<tr>'."\n";
				$x .= '<td class="font_12px">'.$eReportCard['Template']['NameOfSupervisingTeacherEn'].': &nbsp;&nbsp;<b>'.$TeacherName.'</b></td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Overall_Performance_Table($ReportID, $StudentID)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$thisGrandGrade = $this->Get_Project_Based_Report_Overall_Grade($ReportID, $StudentID);
		
		$TableHeight = 45;
		$x = '';
		$x .= '<table width="100%" height="'.$TableHeight.'px" border="0" cellpadding="5" cellspacing="0" class="double_border_table">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td class="font_13px_bold">'.$eReportCard['Template']['OverallPerformanceEn'].'*: &nbsp;&nbsp;'.$thisGrandGrade.'</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		$x .= '<div>'.$this->Get_Empty_Image('5px').'</div>';
		
		### Grade Remarks and Category Remarks
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">'."\n";
			$x .= '<tr>'."\n";
				foreach ((array)$eReportCard['Template']['GradeRemarksArr'] as $thisKey => $thisDescription)
				{
					$x .= '<td class="font_9px" style="width:1%;white-space:nowrap;">'.$thisKey.'-'.$thisDescription.'</td>'."\n";
					$x .= '<td style="width:10%;">&nbsp;</td>'."\n";
				}
				$x .= '<td class="font_9px" style="width:35%;text-align:right;">&nbsp;</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Project_Based_Report_Overall_Grade($ReportID, $StudentID)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID); 
		$ClassLevelID 	= $ReportSetting['ClassLevelID'];
		
		$GrandField = $this->Get_Grand_Position_Determine_Field();
		$GrandFieldSubjectID = $this->Get_Grand_Field_SubjectID($GrandField);
		
		$ManualAdjustedArr = $this->Get_Manual_Adjustment($ReportID);
		if ($ManualAdjustedArr[$StudentID][0][0][$GrandField] != '') {
			// From Manual Adjustment => display the adjusted value directly
			$thisGrandGrade = $ManualAdjustedArr[$StudentID][0][0][$GrandField];
		}
		else {
			// Not From Manual Adjustment => Convert from Grading Schemem
			$ResultArr = $this->getReportResultScore($ReportID, 0, $StudentID, '', 0, 1);
			$GrandFieldValue = $ResultArr[$GrandField];
			
			$GrandFieldSchemeInfoArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $GrandFieldSubjectID, $withGrandResult=1, $returnAsso=0, $ReportID);
			$SchemeID = $GrandFieldSchemeInfoArr['SchemeID'];
			
			$thisGrandGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $GrandFieldValue, $ReportID, $StudentID, $GrandFieldSubjectID, $ClassLevelID, $ReportColumnID=0, $Grade="", $returnGradePoint=0);
			
		}
		
		return $thisGrandGrade;
	}
	########### END Template Related

	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;

		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# initialization
		$border_top = "";
		$x = "";
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh);
			
		if($ReportType=="W")	# Whole Year Report
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			$thisTotalValue = "";
			foreach($ColumnData as $k1=>$d1)
			{
				# get value of this term
				$TermID = $d1['SemesterNum'];
				
				$thisValue = $StudentID ? ($ValueArr[$TermID][$InfoKey] ? $ValueArr[$TermID][$InfoKey] : $this->EmptySymbol) : "#";
				
				# calculation the overall value
				if (is_numeric($thisValue)) {
					if ($thisTotalValue == "")
						$thisTotalValue = $thisValue;
					else if (is_numeric($thisTotalValue))
						$thisTotalValue += $thisValue;
				}
				
				# insert empty cell for assessments
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					
				# display this term value
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			}
			
			# display overall year value
			if($ShowRightestColumn)
			{
				if($thisTotalValue=="")
					$thisTotalValue = ($ValueArr[0][$InfoKey]=="")? $this->EmptySymbol : $ValueArr[0][$InfoKey];
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisTotalValue."</td>";
			}
		}
		else				# Term Report
		{
			# get value of this term
			$thisValue = $StudentID ? ($ValueArr[$SemID][$InfoKey] ? $ValueArr[$SemID][$InfoKey] : $this->EmptySymbol) : "#";
			
			# insert empty cell for assessments
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				
			# display this term value
			if($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
		}
		
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		$CalOrder = $this->Get_Calculation_Setting($ReportID);
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		$border_top = $isFirst ? "border_top" : "";
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, $isFirst);
		
		if ($CalOrder == 1) {
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
		} else {
			$curColumn = 0;
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				for ($i=0; $i<$NumOfAssessment[$curColumn]-1 ; $i++)
				{
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				}
				
				$thisValue = $ValueArr[$ColumnID];
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisValue."&nbsp;</td>";
				$curColumn++;
			}
		}
		
		if ($ShowRightestColumn)
		{
			$thisValue = $OverallValue;
			/*
			if ($UpperLimit=="" || $UpperLimit==0 || $thisValue <= $UpperLimit)
			{
				$thisDisplay = $thisValue;
			}
			else
			{
				$thisDisplay = $this->EmptySymbol;
			}
			*/
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."&nbsp;</td>";
		}
			
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0)
	{
		$x = "";
		$border_top = ($hasBorderTop)? "border_top" : "";
		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleEn ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleCh ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		
		return $x;
	}
	
	function Is_Show_Rightest_Column($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		return $ShowRightestColumn;
	}
	
	function Get_Calculation_Setting($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		return $CalOrder;
	}
	
		// $border, e.g. "left,right,top"
	function Get_Double_Line_Td($border="",$width=1)
	{
		global $image_path,$LAYOUT_SKIN;
		
		$borderArr = explode(",",$border);
		$left = $right = $top = $bottom = 0;
		
		if(in_array("left",$borderArr)) $left = $width;
		if(in_array("right",$borderArr)) $right = $width;
		if(in_array("top",$borderArr)) $top = $width;
		if(in_array("bottom",$borderArr)) $bottom = $width;

		$style = " style='border-style:solid; border-color:#000; border-width:{$top}px {$right}px {$bottom}px {$left}px ' ";
		
		$td = "<td width='0%' height='0%' $style><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='$width' height='$width'/></td>";
		return $td;
	}
	
	function getFormGradeNumber($yearId) {
		$PreloadArrKey = 'getFormGradeNumber';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
		$FormNumberDetermineText = $this->returnClassLevel($yearId);
		$FormNumber = str_replace('G', '', $FormNumberDetermineText);
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $FormNumber);
		return $FormNumber;
	}
}
?>