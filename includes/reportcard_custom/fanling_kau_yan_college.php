<?php
# Editing by 

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");
//include_once($intranet_root."/includes/portfolio25/iPortfolioConfig.inc.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "attendance", "post", "award");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "---";
		
		//$this->PersonalCharGradeArr[Index] = LangKey
		$this->PersonalCharGradeArr['A*'] = 'A*';
		$this->PersonalCharGradeArr['A'] = 'A';
		$this->PersonalCharGradeArr['B'] = 'B';
		$this->PersonalCharGradeArr['C'] = 'C';
		$this->PersonalCharGradeArr['D'] = 'D';
		$this->PersonalCharGradeArr['E'] = 'E';
		$this->DisplayCharItemInReverseOrder = false;
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='', $PrintTemplateType='', $numOfStudent='', $s='') {
		global $eReportCard;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$Semester = $ReportInfoArr['Semester'];
		$ReportType = ($Semester=='F')? 'W' : 'T';
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		$termSequence = $this->Get_Semester_Seq_Number($Semester);
		
		$PageInfoArr = array();
		
		
//		$PageInfoArr[] = $this->Get_First_Page_Table($ReportID, $StudentID);
//		$PageInfoArr[] = $this->Get_Second_Page_Table($ReportID, $StudentID);
//		if ($ReportType == 'W' || $FormNumber==6) {
//			$PageInfoArr[] = $this->Get_Third_Page_Table($ReportID, $StudentID);
//			
//			//2012-0402-0943-13099
//			//$PageInfoArr[] = $this->Get_Fourth_Page_Table($ReportID, $StudentID);
//		}

		// 2013-0605-1122-09140
		if ($ReportType == 'T' && $termSequence == 1) {
			$PageInfoArr[] = $this->Get_Term1_Report_Table($ReportID, $StudentID);
			
		} else {
			$PageInfoArr[] = $this->Get_First_Page_Table($ReportID, $StudentID);
			$PageInfoArr[] = $this->Get_Second_Page_Table($ReportID, $StudentID);
			if ($ReportType == 'W' || $FormNumber==6) {
				$PageInfoArr[] = $this->Get_Third_Page_Table($ReportID, $StudentID);
			}
		}
		
				
		return $this->Get_Report_Layout_By_Pages($PageInfoArr, $termSequence, $numOfStudent, $s);
	}
	
	private function Get_Report_Layout_By_Pages($PageInfoArr, $termSequence='', $numOfStudent='', $s='') {
		
		//2014-0314-1743-36140
		if ($termSequence == 1) {
			$marginTopStyle = ' margin-top:85px; ';
		}
		else {
			$marginTopStyle = '';
		}
		
		$numOfPage = count($PageInfoArr);
		$x = '';
		
		for ($i=0; $i<$numOfPage; $i++) {
			$thisPageHTML = $PageInfoArr[$i];
		
			$thisStyle = '';
			if ($i < ($numOfPage - 1)) {
				$pageBreak = 'page-break-after:always;';
				$thisStyle = '';
			}
			
			if ($i == 0) {
				// do nth
			}
			else {
				$x .= "	<div id=\"container\" style=\"".$pageBreak."\">
							<table width=\"100%\" border=\"0\" cellpadding=\"02\" cellspacing=\"0\" valign=\"top\">
						";
			}
			
			$x .= '<tr><td>'."\r\n";
				$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top" style="margin-left:2px; '.$marginTopStyle.'">'."\r\n";
					$x .= '<tr valign="top"><td>'.$thisPageHTML.'</td></tr>'."\r\n";
				$x .= '</table>'."\r\n";
			$x .= '</td></tr>'."\r\n";
			
			$x .= "</table></div>"."\r\n";
			
			if ($i==0 && ($s == $numOfStudent - 1)) {
				$x .= "<div style='page-break-after:always;'>".$this->Get_Empty_Image(1)."</div>";
			}
		}
		
		return $x;
	}
	
	// 2013-0605-1122-09140
	private function Get_Term1_Report_Table($ReportID, $StudentID) {
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$Semester = $ReportInfoArr['Semester'];
		$ReportType = ($Semester=='F')? 'W' : 'T';
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		$sectionSeparateHeight = 10;
		
		$x = '';
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">'."\r\n";
			$x .= '<tr valign="top" style="height:850px;">'."\r\n";
				$x .= '<td>'."\r\n";
					$x .= $this->Get_Report_Title_Table($ReportID, "", "", $hasSchoolTitle=false);
					$x .= $this->Get_Empty_Image_Div($sectionSeparateHeight);
					$x .= $this->Get_Student_Info_Table($ReportID, $StudentID);
					$x .= $this->Get_Empty_Image_Div($sectionSeparateHeight);
					$x .= $this->Get_ClassTeacherComment_And_Attendance_Table($ReportID, $StudentID);
					$x .= $this->Get_Empty_Image_Div($sectionSeparateHeight);
					$x .= $this->Get_Academic_Result_Table($ReportID, $StudentID);
					$x .= $this->Get_Empty_Image_Div($sectionSeparateHeight);
					
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$x .= '<tr valign="bottom">'."\r\n";
				$x .= '<td>'."\r\n";
					$x .= $this->Get_Signature_Table($ReportID, $StudentID);
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
		
	}	
	
//	private function Get_First_Page_Table($ReportID, $StudentID='') {
//		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
//		$Semester = $ReportInfoArr['Semester'];
//		$ReportType = ($Semester=='F')? 'W' : 'T';
//		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
//		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
//		
//		$sectionSeparateHeight = 10;
//		
//		$x = '';
//		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">'."\r\n";
//			$x .= '<tr valign="top" style="height:950px;">'."\r\n";
//				$x .= '<td>'."\r\n";
//					$x .= $this->Get_Report_Title_Table($ReportID);
//					$x .= $this->Get_Empty_Image_Div($sectionSeparateHeight);
//					$x .= $this->Get_Student_Info_Table($ReportID, $StudentID);
//					$x .= $this->Get_Empty_Image_Div($sectionSeparateHeight);
//					$x .= $this->Get_ClassTeacherComment_And_Attendance_Table($ReportID, $StudentID);
//					
//					if ($ReportType=='W' && $FormNumber == 6) {
//						$x .= $this->Get_Subject_Teacher_Comment_Table($ReportID, $StudentID);
//						$x .= $this->Get_Post_Table($ReportID, $StudentID);
//						$x .= $this->Get_Empty_Image_Div($sectionSeparateHeight);
//					}
//					else if ($ReportType=='W' && $FormNumber < 6) {
//						$x .= $this->Get_Empty_Image_Div($sectionSeparateHeight);
//						$x .= $this->Get_Post_Table($ReportID, $StudentID);
//					}
//					else {
//						$x .= $this->Get_Subject_Teacher_Comment_Table($ReportID, $StudentID);
//						$x .= $this->Get_Empty_Image_Div($sectionSeparateHeight);
//					}
//					
//				$x .= '</td>'."\r\n";
//			$x .= '</tr>'."\r\n";
//			$x .= '<tr valign="bottom">'."\r\n";
//				$x .= '<td>'."\r\n";
//					$x .= $this->Get_Signature_Table($ReportID, $StudentID);
//				$x .= '</td>'."\r\n";
//			$x .= '</tr>'."\r\n";
//		$x .= '</table>'."\r\n";
//		
//		return $x;
//	}
	
	// 2013-0605-1122-09140
	private function Get_First_Page_Table($ReportID, $StudentID='') {
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$Semester = $ReportInfoArr['Semester'];
		$ReportType = ($Semester=='F')? 'W' : 'T';
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		$sectionSeparateHeight = 10;
		
		$x = '';
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">'."\r\n";
			$x .= '<tr valign="top" style="height:850px;">'."\r\n";
				$x .= '<td>'."\r\n";
					$x .= $this->Get_Report_Title_Table($ReportID);
					$x .= $this->Get_Empty_Image_Div($sectionSeparateHeight);
					$x .= $this->Get_Student_Info_Table($ReportID, $StudentID);
					$x .= $this->Get_Empty_Image_Div($sectionSeparateHeight);
					$x .= $this->Get_ClassTeacherComment_And_Attendance_Table($ReportID, $StudentID);
					$x .= $this->Get_Empty_Image_Div($sectionSeparateHeight);
					$x .= $this->Get_Subject_Teacher_Comment_Table($ReportID, $StudentID);
					$x .= $this->Get_Empty_Image_Div($sectionSeparateHeight);
					if ($ReportType == 'W' || $FormNumber==6) {
						$x .= $this->Get_Post_Table($ReportID, $StudentID);
					}
					
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$x .= '<tr valign="bottom">'."\r\n";
				$x .= '<td>'."\r\n";
					$x .= $this->Get_Signature_Table($ReportID, $StudentID);
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_Second_Page_Table($ReportID, $StudentID='') {
		
		$sectionSeparateHeight = 10;
		
		$x = '';
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">'."\r\n";
			$x .= '<tr valign="top" style="height:850px;">'."\r\n";
				$x .= '<td>'."\r\n";
					$x .= $this->Get_Report_Title_Table($ReportID);
					$x .= $this->Get_Empty_Image_Div($sectionSeparateHeight);
					$x .= $this->Get_Student_Info_Table($ReportID, $StudentID, $ForFirstPage=0);
					$x .= $this->Get_Empty_Image_Div($sectionSeparateHeight);
					$x .= $this->Get_Academic_Result_Table($ReportID, $StudentID);
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
//			$x .= '<tr valign="bottom">'."\r\n";
//				$x .= '<td>'."\r\n";
//					$x .= $this->Get_Grading_Scheme_Remarks_Table($ReportID, $StudentID);
//				$x .= '</td>'."\r\n";
//			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_Third_Page_Table($ReportID, $StudentID='') {
		
		$sectionSeparateHeight = 10;
		
		$x = '';
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">'."\r\n";
			$x .= '<tr valign="top" style="height:850px;">'."\r\n";
				$x .= '<td>'."\r\n";
					$x .= $this->Get_Report_Title_Table($ReportID,$hasOLETitle=true);
					$x .= $this->Get_Empty_Image_Div($sectionSeparateHeight);
					$x .= $this->Get_Student_Info_Table($ReportID, $StudentID, $ForFirstPage=0);
					$x .= $this->Get_Empty_Image_Div($sectionSeparateHeight);
					$x .= $this->Get_OLE_Info_Table($ReportID, $StudentID);
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_Fourth_Page_Table($ReportID, $StudentID='') {
		$x = '';
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">'."\r\n";
			$x .= '<tr valign="top" style="height:850px;">'."\r\n";
				$x .= '<td>'."\r\n";
					$x .= $this->Get_Report_Title_Table($ReportID,$hasOLETitle=false,$hasSelfAccTitle=true);
					$x .= '<br />'."\r\n";
					$x .= $this->Get_Student_Info_Table($ReportID, $StudentID, $ForFirstPage=0);
					$x .= '<br />'."\r\n";
					$x .= $this->Get_SELF_ACC_Table($ReportID, $StudentID);
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_Report_Title_Table($ReportID,$hasOLETitle=false,$hasSelfAccTitle=false, $hasSchoolTitle=true) {
		
		global $eReportCard;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ReportTitle = $ReportInfoArr['ReportTitle'];
		//$ReportTitle = str_replace(":_:", " ", $ReportTitle);
		
		$ReportTitleArr = explode(":_:",$ReportTitle);
		
		$Semester = $ReportInfoArr['Semester'];
		$ReportType = ($Semester=='F')? 'W' : 'T';
		
		if($hasOLETitle)
		{
			$ReportTitle .='<br>'.$eReportCard['Template']['RecordOfOLECh'].' '.$eReportCard['Template']['RecordOfOLEEn'];		
		}
			
		if($hasSelfAccTitle)
		{
			$ReportTitle .='<br>'.$eReportCard['Template']['SelfAccCh'].' '.$eReportCard['Template']['SelfAccEn'];		
		}
						
		$x = '';
		$x .= $this->Get_Empty_Image_Div('14')."\r\n";
		$x .= '<table class="report_border grey_bg font_13pt" border="0" cellspacing="0" cellpadding="2" align="center" style="width:100%; text-align:center;">'."\r\n";
			if ($hasSchoolTitle) {
				$x .= '<tr>'."\r\n";
					$x .= '<td>'."\r\n";
						$x .= $this->Get_Text_Span($eReportCard['Template']['SchoolTitleCh'], 'chi');
						$x .= '&nbsp;&nbsp;';
						$x .= $this->Get_Text_Span($eReportCard['Template']['SchoolTitleEn'], 'eng');
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			}
			$x .= '<tr>'."\r\n";
				$x .= '<td>'."\r\n";
					$x .= $this->Get_Text_Span($ReportTitleArr[0], 'chi');
					$x .= ' ';
					$x .= $this->Get_Text_Span($ReportTitleArr[1], 'eng');
				$x .= '</td>'."\r\n";			
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_Student_Info_Table($ReportID, $StudentID='', $ForFirstPage=1) {
		global $eReportCard;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$IssueDate = $ReportInfoArr['Issued'];
		
		
		$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
//debug_r($StudentInfoArr);
		$StudentNameCh = ($StudentInfoArr[0]['ChineseName']=='')? $this->EmptySymbol : $StudentInfoArr[0]['ChineseName'];
		$StudentNameEn = ($StudentInfoArr[0]['EnglishName']=='')? $this->EmptySymbol : $StudentInfoArr[0]['EnglishName'];
		$ClassName = ($StudentInfoArr[0]['ClassName']=='')? $this->EmptySymbol : $StudentInfoArr[0]['ClassName'];
		$ClassNumber = ($StudentInfoArr[0]['ClassNumber']=='')? $this->EmptySymbol : $StudentInfoArr[0]['ClassNumber'];
		$Gender = ($StudentInfoArr[0]['Gender']=='')? $this->EmptySymbol : $StudentInfoArr[0]['Gender'];
		$WebSAMSRegNo = ($StudentInfoArr[0]['WebSAMSRegNo']=='')? $this->EmptySymbol : $StudentInfoArr[0]['WebSAMSRegNo'];
		$DateOfBirth = ($StudentInfoArr[0]['DateOfBirth']=='')? $this->EmptySymbol : substr($StudentInfoArr[0]['DateOfBirth'], 0, 10);
		
		$WebSAMSRegNo = str_replace('#', '', $WebSAMSRegNo);
		
		$paddingLeft = 3;
		
		$nameTitle = str_replace(' ', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', trim(chunk_split($eReportCard['Template']['NameCh'], 3, ' ')));
		$genderTitle = str_replace(' ', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', trim(chunk_split($eReportCard['Template']['GenderCh'], 3, ' ')));
		
		$x = '';
		$x .= '<table class=" font_11pt" border="0" cellspacing="0" cellpadding="0" style="width:100%;">'."\r\n";
			$x .= '<col style="width:14%" />'."\r\n";
			$x .= '<col style="width:3%" />'."\r\n";
			$x .= '<col style="width:35%" />'."\r\n";
			$x .= '<col style="width:19%" />'."\r\n";
			$x .= '<col style="width:3%" />'."\r\n";
			$x .= '<col style="width:26%" />'."\r\n";
			
			$x .= '<tr >'."\r\n";
				### Student Name
				$x .= '<td class="border_top border_bottom border_left" style="padding-left:'.$paddingLeft.'px;">'."\r\n";
					//$x .= '<div class="justify" style="padding-left:5px;">'.$this->Get_Text_Span($eReportCard['Template']['NameCh'], 'chi').'</div>'."\r\n";
					$x .= '<div style="padding-left:5px;">'.$this->Get_Text_Span($nameTitle, 'chi').'</div>'."\r\n";
					$x .= '<div style="padding-left:5px;">'.$this->Get_Text_Span($eReportCard['Template']['NameEn'], 'eng').'</div>'."\r\n";
				$x .= '</td>'."\r\n";
				$x .= '<td class="border_top border_bottom" style="vertical-align:top;text-align:left;">:</td>'."\r\n";
				$x .= '<td class="border_top border_left border_bottom" style="padding-left:15px;">'."\r\n";
					$x .= $this->Get_Text_Span($StudentNameCh, 'chi').' '.$this->Get_Text_Span($StudentNameEn, 'eng');
				$x .= '</td>'."\r\n";
				
				### Class (Class Number)
				$x .= '<td class="border_top border_bottom border_left" style="padding-left:'.$paddingLeft.'px;">'."\r\n";
					$x .= '<div style="padding-left:5px;">'.$this->Get_Text_Span($eReportCard['Template']['ClassCh'].' ('.$eReportCard['Template']['ClassNumberCh'].')', 'chi').'</div>'."\r\n";
					$x .= '<div style="padding-left:5px;">'.$this->Get_Text_Span($eReportCard['Template']['ClassEn'].' ('.$eReportCard['Template']['NoEn'].')', 'eng').'</div>'."\r\n";
				$x .= '</td>'."\r\n";
				$x .= '<td class="border_top border_bottom" style="vertical-align:top;text-align:left;">:</td>';
				$x .= '<td class="border_top border_left border_right border_bottom" style="padding-left:15px;">'.$this->Get_Text_Span($ClassName.' ('.$ClassNumber.')', 'eng').'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			
			
			if ($ForFirstPage) {
				$x .= '<tr>'."\r\n";
					### Gender
					$x .= '<td class=" border_left" style="padding-left:'.$paddingLeft.'px;">'."\r\n";
						//$x .= '<div class="justify" style="padding-left:5px;">'.$this->Get_Text_Span($eReportCard['Template']['GenderCh'], 'chi').'</div>'."\r\n";
						$x .= '<div style="padding-left:5px;">'.$this->Get_Text_Span($genderTitle, 'chi').'</div>'."\r\n";
						$x .= '<div style="padding-left:5px;">'.$this->Get_Text_Span($eReportCard['Template']['GenderEn'], 'eng').'</div>'."\r\n";
					$x .= '</td>'."\r\n";
					$x .= '<td style="vertical-align:top">:</td>';
					$x .= '<td class="border_left" style="padding-left:15px;">'.$this->Get_Text_Span($Gender, 'eng').'</td>'."\r\n";
					### FKYC Student No.
					$x .= '<td class=" border_left" style="padding-left:'.$paddingLeft.'px;">'."\r\n";
						$x .= '<div style="padding-left:5px;">'.$this->Get_Text_Span('FKYC', 'eng').' '.$this->Get_Text_Span($eReportCard['Template']['StudentNoCh'], 'chi').'</div>'."\r\n";
						$x .= '<div style="padding-left:5px;">'.$this->Get_Text_Span('FKYC', 'eng').' '.$this->Get_Text_Span($eReportCard['Template']['StudentNoEn'], 'eng').'</div>'."\r\n";
					$x .= '</td>'."\r\n";
					$x .= '<td style="vertical-align:top;text-align:left;">:</td>';
					$x .= '<td class="border_left border_right" style="padding-left:15px;">'.$this->Get_Text_Span($WebSAMSRegNo, 'eng').'</td>'."\r\n";
				$x .= '</tr>'."\r\n";
				
				
				
				$x .= '<tr>'."\r\n";
					### DOB
					$x .= '<td class="border_top border_left border_bottom" style="padding-left:'.$paddingLeft.'px;">'."\r\n";
						//$x .= '<div class="justify" style="padding-left:5px;">'.$this->Get_Text_Span($eReportCard['Template']['DOBCh'], 'chi').'</div>'."\r\n";
						$x .= '<div class="justify" style="padding-left:5px; width:82px;">'.$this->Get_Text_Span($eReportCard['Template']['DOBCh'], 'chi').'</div>'."\r\n";
						$x .= '<div style="padding-left:5px;">'.$this->Get_Text_Span($eReportCard['Template']['DOBEn'], 'eng').'</div>'."\r\n";
					$x .= '</td>'."\r\n";
								
					$x .= '<td class="border_top border_bottom" style="vertical-align:top;">:</td>';
					$x .= '<td class="border_top border_left border_bottom" style="padding-left:15px;">'.$this->Get_Text_Span($DateOfBirth, 'eng').'</td>'."\r\n";
					### Issue Date
					$x .= '<td class="border_top border_left border_bottom" style="padding-left:'.$paddingLeft.'px;">'."\r\n";
						$x .= '<div class="justify" style="padding-left:5px;">'.$this->Get_Text_Span($eReportCard['Template']['IssueDateCh'], 'chi').'</div>'."\r\n";
						$x .= '<div style="padding-left:5px;">'.$this->Get_Text_Span($eReportCard['Template']['IssueDateEn'], 'eng').'</div>'."\r\n";
					$x .= '</td>'."\r\n";
					$x .= '<td class="border_top border_bottom" style="vertical-align:top;text-align:left;">:</td>';
					$x .= '<td class="border_top border_left border_bottom border_right" style="padding-left:15px;">'.$this->Get_Text_Span($IssueDate, 'eng').'</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			}
		$x .= '</table>'."\r\n";
		
		return $x;
		
	}

//	private function Get_ClassTeacherComment_And_Attendance_Table($ReportID, $StudentID='') {
//		global $eReportCard;
//		
//		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
//		$Semester = $ReportInfoArr['Semester'];
//		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
//		$ReportType = ($Semester=='F')? 'W' : 'T';
//		
//		### Class Teacher Comment
//		if ($ReportType == 'T') {
//			$CommentSourceReportID = $ReportID;
//		}
//		else {
//			$TargetTerm = $this->Get_Last_Semester_Of_Report($ReportID);
//			$TargetTermReportInfoArr = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $TargetTerm, $isMainReport=1);
//			$CommentSourceReportID = $TargetTermReportInfoArr['ReportID'];
//		}
//		$CommentArr = $this->returnSubjectTeacherComment($CommentSourceReportID, $StudentID);
//		
//		//2012-0213-1034-50069
//		//$ClassTeacherComment = Get_Table_Display_Content(stripslashes($CommentArr[0]));
//		if (str_lang($CommentArr[0])=='ENG') {
//			$ClassTeacherComment = $this->Get_Text_Span(Get_Table_Display_Content(stripslashes($CommentArr[0])), 'eng');
//		}
//		else {
//			$ClassTeacherComment = $this->Get_Text_Span(Get_Table_Display_Content(stripslashes($CommentArr[0])), 'chi');
//		}
//		
//		### Other Info
//		//$TargetTerm = ($ReportType=='T')? $Semester : 0;
//		//2012-0611-0931-51099
//		//$TargetTerm = $this->Get_Last_Semester_Of_Report($ReportID);
//		$TargetTerm = ($ReportType=='T')? $Semester : 0;
//		$OtherInfoDataArr = $this->getReportOtherInfoData($ReportID, $StudentID); 
//
//		$OtherInfoDataArr = $OtherInfoDataArr[$StudentID][$TargetTerm];
//		$Conduct = ($OtherInfoDataArr['Conduct'])? $OtherInfoDataArr['Conduct'] : '&nbsp;';
//		$Punctuality = ($OtherInfoDataArr['Punctuality'])? $OtherInfoDataArr['Punctuality'] : '&nbsp;';
//		$Appearance = ($OtherInfoDataArr['Appearance'])? $OtherInfoDataArr['Appearance'] : '&nbsp;';
//		$Late = ($OtherInfoDataArr['Late'])? $OtherInfoDataArr['Late'] : 0;
//		$SickLeave = ($OtherInfoDataArr['Sick Leave'])? $OtherInfoDataArr['Sick Leave'] : 0;
//		$LeaveWithReason = ($OtherInfoDataArr['Leave with Reason'])? $OtherInfoDataArr['Leave with Reason'] : 0;
//		$Truant = ($OtherInfoDataArr['Truant'])? $OtherInfoDataArr['Truant'] : 0;
//		
//		$x = '';
//		$x .= '<table class="font_11pt" border="0" cellspacing="0" cellpadding="0" style="width:100%;">'."\r\n";
//			
//			$x .= '<tr>';
//				$x .= '<td style="width:52%;border:0px;" ></td>'."\r\n";
//				$x .= '<td style="width:12%;border:0px;" ></td>'."\r\n";
//				$x .= '<td style="width:4%;border:0px;" ></td>'."\r\n";
//				$x .= '<td style="width:8%;border:0px;" ></td>'."\r\n";
//				$x .= '<td style="width:8%;border:0px;" ></td>'."\r\n";
//				$x .= '<td style="width:4%;border:0px;" ></td>'."\r\n";
//				$x .= '<td style="width:12%;border:0px;" ></td>'."\r\n";
//			$x .= '</tr>';
//			
//			$titlePaddingTop = 3;
//			$titlePaddingBottom = 3;
//			$csvPaddingLeft = 4;
//			$csvPaddingRight = 4;
//			$csvPaddingTop = 0;
//			$csvPaddingBottom = 0;
//			$x .= '<tr class="grey_bg">'."\r\n";
//				$x .= '<td class="border_top border_left" style="text-align:center; padding-top:'.$titlePaddingTop.'px; padding-bottom:'.$titlePaddingBottom.'px;">'."\r\n";
//					$x .= $this->Get_Text_Span($eReportCard['Template']['ClassTeacherCommentCh'], 'chi')." ".$this->Get_Text_Span($eReportCard['Template']['ClassTeacherCommentEn'], 'eng')."\r\n";
//				$x .= '</td>'."\r\n";
//				$x .= '<td class="border_top border_left border_right" style="text-align:center; padding-top:'.$titlePaddingTop.'px; padding-bottom:'.$titlePaddingBottom.'px;" colspan="6">'."\r\n";
//					$x .= $this->Get_Text_Span($eReportCard['Template']['AttendanceCh'], 'chi')." ".$this->Get_Text_Span($eReportCard['Template']['AttendanceEn'], 'eng')."\r\n";
//				$x .= '</td>'."\r\n";
//			$x .= '</tr>'."\r\n";
//			$x .= '<tr>'."\r\n";
//				### Class Teacher Comment
//				$x .= '<td rowspan="5" class="border_top border_left border_bottom" style="vertical-align:top; padding:8px;"><div style="text-align:justify;">'.$ClassTeacherComment.'</div></td>'."\r\n";
//				### Late & Sick Leave
//				$x .= '<td class="border_top border_left" style="text-align:center; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['LateCh'], 'chi').' <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['LateEn'], 'eng').'</span></td>'."\r\n";
//				$x .= '<td class="border_top border_left" colspan="2">'.$this->Get_Day_Div($Late).'</td>'."\r\n";
//				$x .= '<td class="border_top border_left" colspan="2" style="text-align:center; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['SickLeaveCh'], 'chi').' <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['SickLeaveEn'], 'eng').'</span></td>'."\r\n";
//				$x .= '<td class="border_top border_left border_right">'.$this->Get_Day_Div($SickLeave).'</td>'."\r\n";
//			$x .= '</tr>'."\r\n";
//			$x .= '<tr>'."\r\n";
//				### Leave with Reason & Truant
//				$x .= '<td class="border_top border_left" style="text-align:center; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['LeaveWithReasonCh'], 'chi').' <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['LeaveWithReasonEn'], 'eng').'</span></td>'."\r\n";
//				$x .= '<td class="border_top border_left" colspan="2">'.$this->Get_Day_Div($LeaveWithReason).'</td>'."\r\n";
//				$x .= '<td class="border_top border_left" colspan="2" style="text-align:center; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['TruantCh'], 'chi').' <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['TruantEn'], 'eng').'</span></td>'."\r\n";
//				$x .= '<td class="border_top border_left border_right">'.$this->Get_Day_Div($Truant).'</td>'."\r\n";
//			$x .= '</tr>'."\r\n";
//			
//			$x .= '<tr class="grey_bg">'."\r\n";
//			$x .= '<td class="border_top border_left border_right" style="text-align:center; padding-top:'.$titlePaddingTop.'px; padding-bottom:'.$titlePaddingBottom.'px;" colspan="6">'.$this->Get_Text_Span($eReportCard['Template']['ConductGradesCh'], 'chi')." ".$this->Get_Text_Span($eReportCard['Template']['ConductGradesEn'], 'eng').'</td>'."\r\n";
//			$x .= '</tr>'."\r\n";
//			
//			$x .= '<tr>'."\r\n";
//				### Punctuality, Appearance, Conduct Title
//				$x .= '<td class="border_top border_left" colspan="2" style="text-align:center; padding-left:5px; padding-right:0px;">'.$this->Get_Text_Span($eReportCard['Template']['PunctualityCh'], 'chi').'<br/><span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['PunctualityEn'], 'eng').'</span></td>'."\r\n";
//				$x .= '<td class="border_top border_left" colspan="2" style="text-align:center; padding-left:5px; padding-right:0px;">'.$this->Get_Text_Span($eReportCard['Template']['AppearanceCh'], 'chi').'<br/><span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['AppearanceEn'], 'eng').'</span></td>'."\r\n";
//				$x .= '<td class="border_top border_left border_right" colspan="2" style="text-align:center; padding-left:5px; padding-right:0px;">'.$this->Get_Text_Span($eReportCard['Template']['ConductCh'], 'chi').'<br/><span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['ConductEn'], 'eng').'</span></td>'."\r\n";
//			$x .= '</tr>'."\r\n";
//			$x .= '<tr>'."\r\n";
//				### Punctuality, Appearance, Conduct Data
//				$x .= '<td class="border_top border_left border_bottom" colspan="2" style="text-align:center; padding-left:5px; padding-right:0px; padding-top:1px; padding-bottom:1px;">'.$this->Get_Text_Span($Punctuality, 'eng').'</td>'."\r\n";
//				$x .= '<td class="border_top border_left border_bottom" colspan="2" style="text-align:center; padding-left:5px; padding-right:0px; padding-top:1px; padding-bottom:1px;">'.$this->Get_Text_Span($Appearance, 'eng').'</td>'."\r\n";
//				$x .= '<td class="border_top border_left border_bottom border_right" colspan="2" style="text-align:center; padding-left:5px; padding-right:0px; padding-top:1px; padding-bottom:1px;">'.$this->Get_Text_Span($Conduct, 'eng').'</td>'."\r\n";
//			$x .= '</tr>'."\r\n";
//		$x .= '</table>'."\r\n";
//		
//		return $x;
//	}

	// 2013-0605-1122-09140
	private function Get_ClassTeacherComment_And_Attendance_Table($ReportID, $StudentID='') {
		global $eReportCard;
		
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$Semester = $ReportInfoArr['Semester'];
		$ReportType = ($Semester=='F')? 'W' : 'T';
		
		if ($ReportType == 'T') {
			$termSequence = $this->Get_Semester_Seq_Number($Semester);
		}
		else {
			$termSequence = 0;
		}
			
		
		### Class Teacher Comment
		if ($ReportType == 'T') {
			$CommentSourceReportID = $ReportID;
		}
		else {
			$TargetTerm = $this->Get_Last_Semester_Of_Report($ReportID);
			$TargetTermReportInfoArr = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $TargetTerm, $isMainReport=1);
			$CommentSourceReportID = $TargetTermReportInfoArr['ReportID'];
		}
		$CommentArr = $this->returnSubjectTeacherComment($CommentSourceReportID, $StudentID);
		
		//2012-0213-1034-50069
		//$ClassTeacherComment = Get_Table_Display_Content(stripslashes($CommentArr[0]));
		if (str_lang($CommentArr[0])=='ENG') {
			$ClassTeacherComment = $this->Get_Text_Span(Get_Table_Display_Content(stripslashes($CommentArr[0])), 'eng');
		}
		else {
			$ClassTeacherComment = $this->Get_Text_Span(Get_Table_Display_Content(stripslashes($CommentArr[0])), 'chi');
		}
		
		### Other Info
		// if ($ReportType == 'T') {
			// $consolidatedReportAry = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, 'F');
			// $otherInfoReportId = $consolidatedReportAry['ReportID'];
		// }
		// else {
			// $otherInfoReportId = $ReportID;
		// }
		
		// $OtherInfoDataArr = $this->getReportOtherInfoData($otherInfoReportId, $StudentID);
		// hdebug_pr($otherInfoReportId);
		
		### cumulative other info
		if ($termSequence == 2 || $termSequence == 3 || $termSequence == 0) {
			$SemesterAry = $this->returnSemesters();
			$TermOtherInfoData = array();
			$counter = 0;
			$targetTermSequence = ($termSequence == 0) ? 3 : $termSequence;
			// add first 2 term onter info
			
			foreach ((array)$SemesterAry as $Term => $TermName) {
				
				if ($counter < $targetTermSequence) {
					
					// hdebug_pr($Term);
					$reportArray = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $Term);
					$otherInfoReportId = $reportArray['ReportID'];
					$OtherInfoDataArr = $this->getReportOtherInfoData($otherInfoReportId, $StudentID);
				
//					$CumulativeConduct += $OtherInfoDataArr[$StudentID][$Term]['Conduct'];
//					$CumulativePunctuality += $OtherInfoDataArr[$StudentID][$Term]['Punctuality'];
//					$CumulativeAppearance += $OtherInfoDataArr[$StudentID][$Term]['Appearance'];
					$CumulativeLate += $OtherInfoDataArr[$StudentID][$Term]['Late'];
					$CumulativeSickLeave += $OtherInfoDataArr[$StudentID][$Term]['Sick Leave'];
					$CumulativeLeaveWithReason += $OtherInfoDataArr[$StudentID][$Term]['Leave with Reason'];
					$CumulativeTruant += $OtherInfoDataArr[$StudentID][$Term]['Truant'];
					$CumulativeEarlyLeave += $OtherInfoDataArr[$StudentID][$Term]['Early Leave'];
					$CumulativeDisapprovedLeave += $OtherInfoDataArr[$StudentID][$Term]['Disapproved Leave'];
					$counter++;
				} else {
					continue;
				}
			}
		} 
		// else {
			// $CumulativeConduct = $OtherInfoDataArr[$StudentID][0]['Conduct'];
			// $CumulativePunctuality = $OtherInfoDataArr[$StudentID][0]['Punctuality'];
			// $CumulativeAppearance = $OtherInfoDataArr[$StudentID][0]['Appearance'];
			// $CumulativeLate = $OtherInfoDataArr[$StudentID][0]['Late'];
			// $CumulativeSickLeave = $OtherInfoDataArr[$StudentID][0]['Sick Leave'];
			// $CumulativeLeaveWithReason = $OtherInfoDataArr[$StudentID][0]['Leave with Reason'];
			// $CumulativeTruant = $OtherInfoDataArr[$StudentID][0]['Truant'];
			// $CumulativeEarlyLeave = $OtherInfoDataArr[$StudentID][0]['Early Leave'];
			// $CumulativeDisapprovedLeave = $OtherInfoDataArr[$StudentID][0]['Disapproved Leave'];
		// }
		
//		$CumulativeConduct = ($CumulativeConduct) ? $CumulativeConduct : '&nbsp';
//		$CumulativePunctuality = ($CumulativePunctuality) ? $CumulativePunctuality : '&nbsp';
//		$CumulativeAppearance = ($CumulativeAppearance) ? $CumulativeAppearance : '&nbsp';
		$CumulativeLate = ($CumulativeLate) ? $CumulativeLate : 0;
		$CumulativeSickLeave = ($CumulativeSickLeave) ? $CumulativeSickLeave : 0;
		$CumulativeLeaveWithReason = ($CumulativeLeaveWithReason) ? $CumulativeLeaveWithReason : 0;
		$CumulativeTruant = ($CumulativeTruant) ? $CumulativeTruant : 0;
		$CumulativeEarlyLeave = ($CumulativeEarlyLeave) ? $CumulativeEarlyLeave : 0;
		$CumulativeDisapprovedLeave = ($CumulativeDisapprovedLeave) ? $CumulativeDisapprovedLeave : 0;
		
		
		### this semester other info
		$TargetTerm = $this->Get_Last_Semester_Of_Report($ReportID);
		//$ThisTermOtherInfoDataArr = $OtherInfoDataArr[$StudentID][$TargetTerm];
		$TargetReportAry = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $TargetTerm);
		$TargetReportId = $TargetReportAry['ReportID'];
		$ThisTermOtherInfoDataArr = $this->getReportOtherInfoData($TargetReportId, $StudentID);
		$ThisTermOtherInfoDataArr = $ThisTermOtherInfoDataArr[$StudentID][$TargetTerm];
		
		if ($ReportType == 'T') {
			$Conduct = ($ThisTermOtherInfoDataArr['Conduct'])? $ThisTermOtherInfoDataArr['Conduct'] : '&nbsp;';
			$Punctuality = ($ThisTermOtherInfoDataArr['Punctuality'])? $ThisTermOtherInfoDataArr['Punctuality'] : '&nbsp;';
			$Appearance = ($ThisTermOtherInfoDataArr['Appearance'])? $ThisTermOtherInfoDataArr['Appearance'] : '&nbsp;';
		}
		else {
			$consolidatedOtherInfoAry = $this->getReportOtherInfoData($ReportID, $StudentID);
			$consolidatedOtherInfoAry = $consolidatedOtherInfoAry[$StudentID][0];
			
			$Conduct = ($consolidatedOtherInfoAry['Conduct'])? $consolidatedOtherInfoAry['Conduct'] : '&nbsp;';
			$Punctuality = ($consolidatedOtherInfoAry['Punctuality'])? $consolidatedOtherInfoAry['Punctuality'] : '&nbsp;';
			$Appearance = ($consolidatedOtherInfoAry['Appearance'])? $consolidatedOtherInfoAry['Appearance'] : '&nbsp;';
		}
		
		$Late = ($ThisTermOtherInfoDataArr['Late'])? $ThisTermOtherInfoDataArr['Late'] : 0;
		$SickLeave = ($ThisTermOtherInfoDataArr['Sick Leave'])? $ThisTermOtherInfoDataArr['Sick Leave'] : 0;
		$LeaveWithReason = ($ThisTermOtherInfoDataArr['Leave with Reason'])? $ThisTermOtherInfoDataArr['Leave with Reason'] : 0;
		$Truant = ($ThisTermOtherInfoDataArr['Truant'])? $ThisTermOtherInfoDataArr['Truant'] : 0;
		$EarlyLeave = ($ThisTermOtherInfoDataArr['Early Leave'])? $ThisTermOtherInfoDataArr['Early Leave'] : 0;
		$DisapprovedLeave = ($ThisTermOtherInfoDataArr['Disapproved Leave'])? $ThisTermOtherInfoDataArr['Disapproved Leave'] : 0;
		
		
		$titlePaddingTop = 3;
		$titlePaddingBottom = 3;
		$csvPaddingLeft = 4;
		$csvPaddingRight = 4;
		
		$x = '';
		switch ($termSequence) {
			case 1:
				$x .= '<table class="font_11pt" border="0" cellspacing="0" cellpadding="0" align="center" style="width:100%;">'."\r\n";
					$x .= '<colgroup>'."\r\n";
						$x .= '<col style="width:17%;"/>'."\r\n";
						$x .= '<col style="width:17%;"/>'."\r\n";
						$x .= '<col style="width:17%;"/>'."\r\n";
						$x .= '<col style="width:17%;"/>'."\r\n";
						$x .= '<col style="width:16%;"/>'."\r\n";
						$x .= '<col style="width:16%;"/>'."\r\n";
					$x .= '</colgroup>'."\r\n";
					
					$x .= '<tr class="grey_bg">'."\r\n";
						$x .= '<td class="border_top border_left" style="text-align:center; padding-top:'.$titlePaddingTop.'px; padding-bottom:'.$titlePaddingBottom.'px;" colspan=4>'.$this->Get_Text_Span($eReportCard['Template']['AttendanceCh'], 'chi')." ".$this->Get_Text_Span($eReportCard['Template']['AttendanceEn'], 'eng').'</td>'."\r\n";
						$x .= '<td class="border_top border_left border_right" style="text-align:center; padding-top:'.$titlePaddingTop.'px; padding-bottom:'.$titlePaddingBottom.'px;" colspan=2>'.$this->Get_Text_Span($eReportCard['Template']['ConductGradesCh'], 'chi')." ".$this->Get_Text_Span($eReportCard['Template']['ConductGradesEn'], 'eng').'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
					
					$x .= '<tr>'."\r\n";
						$x .= '<td class="border_top border_left" style="text-align:left; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['LateCh'], 'chi').'<br/> <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['LateEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left" >'.$this->Get_Day_Div($Late).'</td>'."\r\n";
						$x .= '<td class="border_top border_left" style="text-align:left; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['EarlyLeaveCh'], 'chi').'<br/> <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['EarlyLeaveEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left">'.$this->Get_Day_Div($EarlyLeave).'</td>'."\r\n";
						$x .= '<td class="border_top border_left" style="text-align:left; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['PunctualityCh'], 'chi').'<br/> <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['PunctualityEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left border_right" style="text-align:center;">'.$this->Get_Text_Span($Punctuality, 'eng').'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
					
					$x .= '<tr>'."\r\n";
						$x .= '<td class="border_top border_left" style="text-align:left; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['SickLeaveCh'], 'chi').'<br/> <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['SickLeaveEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left">'.$this->Get_Day_Div($SickLeave).'</td>'."\r\n";
						$x .= '<td class="border_top border_left" style="text-align:left; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['LeaveWithReasonCh'], 'chi').'<br/> <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['LeaveWithReasonEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left">'.$this->Get_Day_Div($LeaveWithReason).'</td>'."\r\n";
						$x .= '<td class="border_top border_left" style="text-align:left; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['AppearanceCh'], 'chi').'<br/> <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['AppearanceEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left border_right" style="text-align:center;">'.$this->Get_Text_Span($Appearance, 'eng').'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
					
					$x .= '<tr>'."\r\n";
						$x .= '<td class="border_top border_left border_bottom" style="text-align:left; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['DisapprovedLeaveCh'], 'chi').'<br/> <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['DisapprovedLeaveEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left border_bottom">'.$this->Get_Day_Div($DisapprovedLeave).'</td>'."\r\n";
						$x .= '<td class="border_top border_left border_bottom" style="text-align:left; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['TruantCh'], 'chi').'<br/> <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['TruantEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left border_bottom">'.$this->Get_Day_Div($Truant).'</td>'."\r\n";
						$x .= '<td class="border_top border_left border_bottom" style="text-align:left; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['ConductCh'], 'chi').'<br/> <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['ConductEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left border_bottom border_right" style="text-align:center;">'.$this->Get_Text_Span($Conduct, 'eng').'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
				$x .= '</table>'."\r\n";
				break;
				
			case 2:
			case 3:
			case 0:
				$x .= '<table class="font_11pt" border="0" cellspacing="0" cellpadding="0" style="width:100%;">'."\r\n";
					$x .= '<tr>';
						$x .= '<td style="width:40%;border:0px;" ></td>'."\r\n";
						$x .= '<td style="width:10%;border:0px;" ></td>'."\r\n";
						$x .= '<td style="width:10%;border:0px;" ></td>'."\r\n";
						$x .= '<td style="width:10%;border:0px;" ></td>'."\r\n";
						$x .= '<td style="width:10%;border:0px;" ></td>'."\r\n";
						$x .= '<td style="width:10%;border:0px;" ></td>'."\r\n";
						$x .= '<td style="width:10%;border:0px;" ></td>'."\r\n";
					$x .= '</tr>';
					
					//2014-0425-0927-57140
//					$titlePaddingTop = 3;
//					$titlePaddingBottom = 3;
					$titlePaddingTop = 1;
					$titlePaddingBottom = 1;
					$csvPaddingLeft = 4;
					$csvPaddingRight = 4;
					$csvPaddingTop = 0;
					$csvPaddingBottom = 0;
					$x .= '<tr class="grey_bg">'."\r\n";
						$x .= '<td class="border_top border_left" style="text-align:center; padding-top:'.$titlePaddingTop.'px; padding-bottom:'.$titlePaddingBottom.'px;">'."\r\n";
							$x .= $this->Get_Text_Span($eReportCard['Template']['ClassTeacherCommentCh'], 'chi')." ".$this->Get_Text_Span($eReportCard['Template']['ClassTeacherCommentEn'], 'eng')."\r\n";
						$x .= '</td>'."\r\n";
						$x .= '<td class="border_top border_left border_right" style="text-align:center; padding-top:'.$titlePaddingTop.'px; padding-bottom:'.$titlePaddingBottom.'px;" colspan="6">'."\r\n";
							$x .= $this->Get_Text_Span($eReportCard['Template']['AttendanceCh'], 'chi')." ".$this->Get_Text_Span($eReportCard['Template']['AttendanceEn'], 'eng')."\r\n";
						$x .= '</td>'."\r\n";
					$x .= '</tr>'."\r\n";
					$x .= '<tr>'."\r\n";
						### Class Teacher Comment
						//2014-0425-0927-57140
						//$x .= '<td rowspan="5" class="border_top border_left border_bottom" style="vertical-align:top; padding:8px;"><div style="text-align:justify;">'.$ClassTeacherComment.'</div></td>'."\r\n";
						$x .= '<td rowspan="5" class="border_top border_left border_bottom" style="vertical-align:top; padding-left:8px; padding-right:8px; padding-top:2px; padding-bottom:2px;"><div style="text-align:justify;">'.$ClassTeacherComment.'</div></td>'."\r\n";
						### Late & Early Leave
						$x .= '<td class="border_top border_left" colspan="2" style="text-align:left; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['LateCh'], 'chi').'<br/> <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['LateEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left" >'.$this->Get_Day_Div($Late, $CumulativeLate).'</td>'."\r\n";
						$x .= '<td class="border_top border_left" colspan="2" style="text-align:left; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['EarlyLeaveCh'], 'chi').'<br/> <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['EarlyLeaveEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left border_right">'.$this->Get_Day_Div($EarlyLeave, $CumulativeEarlyLeave).'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
					$x .= '<tr>'."\r\n";
						### Sick Leave & Leave with Reason
						$x .= '<td class="border_top border_left" colspan="2" style="text-align:left; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['SickLeaveCh'], 'chi').'<br/> <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['SickLeaveEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left">'.$this->Get_Day_Div($SickLeave, $CumulativeSickLeave).'</td>'."\r\n";
						$x .= '<td class="border_top border_left" colspan="2" style="text-align:left; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['LeaveWithReasonCh'], 'chi').'<br/> <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['LeaveWithReasonEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left border_right">'.$this->Get_Day_Div($LeaveWithReason, $CumulativeLeaveWithReason).'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
					$x .= '<tr>'."\r\n";
						### Disapproved Leave & Truant
						$x .= '<td class="border_top border_left" colspan="2" style="text-align:left; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['DisapprovedLeaveCh'], 'chi').'<br/> <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['DisapprovedLeaveEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left">'.$this->Get_Day_Div($DisapprovedLeave, $CumulativeDisapprovedLeave).'</td>'."\r\n";
						$x .= '<td class="border_top border_left" colspan="2" style="text-align:left; padding-left:'.$csvPaddingLeft.'px; padding-right:'.$csvPaddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['TruantCh'], 'chi').'<br/> <span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['TruantEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left border_right">'.$this->Get_Day_Div($Truant, $CumulativeTruant).'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
					$x .= '<tr class="grey_bg">'."\r\n";
					$x .= '<td class="border_top border_left border_right" style="text-align:center; padding-top:'.$titlePaddingTop.'px; padding-bottom:'.$titlePaddingBottom.'px;" colspan="6">'.$this->Get_Text_Span($eReportCard['Template']['ConductGradesCh'], 'chi')." ".$this->Get_Text_Span($eReportCard['Template']['ConductGradesEn'], 'eng').'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
					
					$x .= '<tr>'."\r\n";
						### Punctuality, Appearance, Conduct Title
						$x .= '<td class="border_top border_left border_bottom" style="text-align:center; ">'.$this->Get_Text_Span($eReportCard['Template']['PunctualityCh'], 'chi').'<br/><span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['PunctualityEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left border_bottom" style="text-align:center;padding-top:1px; padding-bottom:1px;">'.$this->Get_Text_Span($Punctuality, 'eng').'</td>'."\r\n";
						$x .= '<td class="border_top border_left border_bottom" style="text-align:center;">'.$this->Get_Text_Span($eReportCard['Template']['AppearanceCh'], 'chi').'<br/><span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['AppearanceEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left border_bottom" style="text-align:center; padding-top:1px; padding-bottom:1px;">'.$this->Get_Text_Span($Appearance, 'eng').'</td>'."\r\n";
						$x .= '<td class="border_top border_left border_bottom"  style="text-align:center;">'.$this->Get_Text_Span($eReportCard['Template']['ConductCh'], 'chi').'<br/><span class="font_10pt">'.$this->Get_Text_Span($eReportCard['Template']['ConductEn'], 'eng').'</span></td>'."\r\n";
						$x .= '<td class="border_top border_left border_bottom border_right" style="text-align:center; padding-top:1px; padding-bottom:1px;">'.$this->Get_Text_Span($Conduct, 'eng').'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
				$x .= '</table>'."\r\n";
				break;
		}
		return $x;
	}

	private function Get_Post_Table($ReportID, $StudentID='') {
		global $eReportCard;

        # Load the highlight setting data
        $data = $this->LOAD_SETTING('PostTableHeader');
        $data["Column1Ch"] = $data["Column1Ch"] ? $data["Column1Ch"] : $eReportCard['Template']['SchoolManagementPostCh'];
        $data["Column1En"] = $data["Column1En"] ? $data["Column1En"] : $eReportCard['Template']['SchoolManagementPostEn'];
        $data["Column1Width"] = $data["Column1Width"] ? $data["Column1Width"] : 50;
        $data["Column2Ch"] = $data["Column2Ch"] ? $data["Column2Ch"] : $eReportCard['Template']['OthersPostCh'];
        $data["Column2En"] = $data["Column2En"] ? $data["Column2En"] : $eReportCard['Template']['OthersPostEn'];
        $data["Column2Width"] = $data["Column2Width"] ? $data["Column2Width"] : 50;

		### Other Info
		$TargetTerm = 0;	// retrieve data from whole year csv
		$OtherInfoDataArr = $this->getReportOtherInfoData($ReportID, $StudentID);
		$OtherInfoDataArr = $OtherInfoDataArr[$StudentID][$TargetTerm];
        // 2019-0924-1219-32289
		//$SchoolManagementPostArr = ($OtherInfoDataArr['School Management Post'])? $OtherInfoDataArr['School Management Post'] : '&nbsp;';
		//$OthersPostArr = ($OtherInfoDataArr['Others Post'])? $OtherInfoDataArr['Others Post'] : '&nbsp;';
		//if (!is_array($SchoolManagementPostArr)) {
		//	$SchoolManagementPostArr = array($SchoolManagementPostArr);
		//}
		//$SchoolManagementPostDisplay = implode('<br />', (array)$SchoolManagementPostArr);
		//if (!is_array($OthersPostArr)) {
		//	$OthersPostArr = array($OthersPostArr);
		//}
		//$OthersPostDisplay = implode('<br />', (array)$OthersPostArr);
        $Column1DataArr = ($OtherInfoDataArr[$data["Column1En"]])? $OtherInfoDataArr[$data["Column1En"]] : (($OtherInfoDataArr['School Management Post'])? $OtherInfoDataArr['School Management Post'] : '&nbsp;');
        $Column2DataArr = ($OtherInfoDataArr[$data["Column2En"]])? $OtherInfoDataArr[$data["Column2En"]] : (($OtherInfoDataArr['Others Post'])? $OtherInfoDataArr['Others Post'] : '&nbsp;');
        if (!is_array($Column1DataArr)) {
            $Column1DataArr = array($Column1DataArr);
        }
        $Column1DataDisplay = implode('<br />', (array)$Column1DataArr);
        if (!is_array($Column2DataArr)) {
            $Column2DataArr = array($Column2DataArr);
        }
        $Column2DataDisplay = implode('<br />', (array)$Column2DataArr);
		
		//2014-0425-0927-57140
//		$TitleHeight = 45;
//		$ContentHeight = 58;
		$ContentHeight = 30;
		$x = '';
		$x .= '<table class="border_table font_11pt" border="0" cellspacing="0" cellpadding="8" align="center" style="width:100%;">'."\r\n";
			//$x .= '<col style="width:50%" />'."\r\n";
			//$x .= '<col style="width:50%" />'."\r\n";
            $x .= '<col style="width:'.$data["Column1Width"].'%" />'."\r\n";
            $x .= '<col style="width:'.$data["Column2Width"].'%" />'."\r\n";
			$x .= '<tr class="grey_bg">'."\r\n";
				//2014-0425-0927-57140
				//$x .= '<td style="text-align:center; height:'.$TitleHeight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['SchoolManagementPostCh'], 'chi').' '.$this->Get_Text_Span($eReportCard['Template']['SchoolManagementPostEn'], 'eng').'</td>'."\r\n";
                // 2019-0924-1219-32289
				//$x .= '<td style="text-align:center; padding-top:2px; padding-bottom:2px;">'.$this->Get_Text_Span($eReportCard['Template']['SchoolManagementPostCh'], 'chi').' '.$this->Get_Text_Span($eReportCard['Template']['SchoolManagementPostEn'], 'eng').'</td>'."\r\n";
				//$x .= '<td style="text-align:center; padding-top:2px; padding-bottom:2px;">'.$this->Get_Text_Span($eReportCard['Template']['OthersPostCh'], 'chi').' '.$this->Get_Text_Span($eReportCard['Template']['OthersPostEn'], 'eng').'</td>'."\r\n";
                $x .= '<td style="text-align:center; padding-top:2px; padding-bottom:2px;">'.$this->Get_Text_Span($data["Column1Ch"], 'chi').'<br/>'.$this->Get_Text_Span($data["Column1En"], 'eng').'</td>'."\r\n";
                $x .= '<td style="text-align:center; padding-top:2px; padding-bottom:2px;">'.$this->Get_Text_Span($data["Column2Ch"], 'chi').'<br/>'.$this->Get_Text_Span($data["Column2En"], 'eng').'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$x .= '<tr>'."\r\n";
                // 2019-0924-1219-32289
				//$x .= '<td style="vertical-align:top; height:'.$ContentHeight.'px;">'.$this->Get_Text_Span($SchoolManagementPostDisplay, 'chi').'</td>'."\r\n";
				//$x .= '<td style="vertical-align:top;">'.$this->Get_Text_Span($OthersPostDisplay, 'chi').'</td>'."\r\n";
                $x .= '<td style="vertical-align:top; height:'.$ContentHeight.'px;">'.$this->Get_Text_Span($Column1DataDisplay, 'chi').'</td>'."\r\n";
                $x .= '<td style="vertical-align:top;">'.$this->Get_Text_Span($Column2DataDisplay, 'chi').'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_Other_Info_Data_Div($Value) {
		global $eReportCard;
		return '<span style="float:left; width:30%; text-align:right;">'.$Value.'</span>'."\r\n";
	}
	
	private function Get_Day_Div($Value, $Value2="") {
		global $eReportCard;
		
		$x = '';
//		$x .= '<table class="font_10pt" padding="0px" style="width:100%; cell-padding:0px;">'."\r\n";
//			$x .= '<tr>'."\r\n";
//				$x .= '<td style="text-align:right">'.$this->Get_Text_Span($Value, 'eng').'</td>'."\r\n";
//				$x .= '<td style="text-align:right">'.$this->Get_Text_Span($eReportCard['Template']['DayCh'], 'chi').'</td>'."\r\n";
//			$x .= '</tr>'."\r\n";
//					  
//			$x .= '<tr>'."\r\n";
//				$x .= '<td>&nbsp;</td>'."\r\n";
//				$x .= '<td style="text-align:right" class="font_eng">'.$this->Get_Text_Span($eReportCard['Template']['DayEn'], 'eng').'</td>'."\r\n";
//			$x .= '</tr>'."\r\n";
//		$x .= '</table>'."\r\n";
		
		
//		$paddingLeft = 5;
//		$paddingRight = 5;
//		$x .= '<div style="float:left; padding-left:'.$paddingLeft.'px;">'.$this->Get_Text_Span($Value, 'eng', 'font_10pt').'</div>'."\r\n";
//		$x .= '<div style="float:right; padding-right:'.$paddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['DayCh'], 'chi', 'font_10pt').'</div>'."\r\n";
//		$x .= '<br style="clear:both;" />'."\r\n";
//		$x .= '<div style="float:right; padding-right:'.$paddingRight.'px;">'.$this->Get_Text_Span($eReportCard['Template']['DayEn'], 'eng', 'font_10pt').'</div>'."\r\n";
		
		// 2013-0605-1122-09140
		$paddingRight = 5;
		$x .= '<div align="right">'."\r\n";
			$x .= '<table width="100%" border=0 cellspacing="0" cellpadding="0">';
				$x .= '<tr>';
					$x .= '<td style="text-align:center; width:100%;">';
						$x .= $this->Get_Text_Span($Value, 'eng', 'font_10pt');
						$x .= ($Value2) ? $this->Get_Text_Span(' '.'('.$Value2.')', 'eng', 'font_10pt') : '';
					$x .= '</td>';
					$x .= '<td width="10px" style="text-align:right;">';
						$x .= '<span style="padding-right:'.$paddingRight.';">';
							$x .= $this->Get_Text_Span($eReportCard['Template']['DayCh'], 'chi', 'font_10pt');
						$x .= '</span>';
					$x .= '</td>';
				$x .= '</tr>';
				$x .= '<tr>';
					$x .= '<td style="text-align:right;" colspan = 2>';
						$x .= '<span style="padding-right:'.$paddingRight.';">';
							$x .= $this->Get_Text_Span($eReportCard['Template']['DayEn'], 'eng', 'font_10pt');
						$x .= '</span>';
					$x .= '</td>';
				$x .= '</tr>';
			$x .= '</table>';
		$x .= '</div>'."\r\n";
		// $x .= '<div align="right" style="padding-right:'.$paddingRight.';">'.$this->Get_Text_Span($eReportCard['Template']['DayEn'], 'eng', 'font_10pt').'</div>'."\r\n";
		
		
		return $x;
	}
	
	private function Get_Subject_Teacher_Comment_Table($ReportID, $StudentID='') {
		global $eReportCard;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$Semester = $ReportInfoArr['Semester'];
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$ReportType = ($Semester=='F')? 'W' : 'T';
			
		if ($ReportType == 'T') {
			$CommentSourceReportID = $ReportID;
		}
		else {
			$TargetTerm = $this->Get_Last_Semester_Of_Report($ReportID);
			$TargetTermReportInfoArr = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $TargetTerm, $isMainReport=1);
			$CommentSourceReportID = $TargetTermReportInfoArr['ReportID'];
		}
		
		// $CommentArr[$SubjectID] = Value
		$CommentArr = $this->returnSubjectTeacherComment($CommentSourceReportID, $StudentID);
		
		// $MarksArr[$thisStudentID][$thisSubjectID][$thisReportColumnID][DBField] = Value
		$MarksArr = $this->getMarks($ReportID);
		
		$MainSubjectArrCh = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='ch', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		$MainSubjectArrEn = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='en', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		
		$SubjectIdToCodeMappingArr = $this->GET_SUBJECTS_CODEID_MAP();
		$SubjectGradingSchemeInfoArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, '', $withGrandResult=0, $returnAsso=1, $ReportID);
		
		$DisplayDataArr = array();
		$Counter = 0;
		foreach ((array)$MainSubjectArrCh as $thisSubjectID => $thisSubjectInfoArr) {
			$thisGrade = $MarksArr[$StudentID][$thisSubjectID][0]['Grade'];
			$thisComment = trim($CommentArr[$thisSubjectID]);

			if (!$this->Check_If_Grade_Is_SpecialCase($thisGrade) && $thisComment != '') {
				$DisplayDataArr[$Counter]['SubjectNameCh'] = $this->Get_Text_Span($thisSubjectInfoArr[0], 'chi');
				$DisplayDataArr[$Counter]['SubjectNameEn'] = $this->Get_Text_Span($MainSubjectArrEn[$thisSubjectID][0], 'eng');
				$DisplayDataArr[$Counter]['Comment'] = Get_Table_Display_Content(stripslashes($thisComment));
				
				//2012-0213-1034-50069
				//if ($SubjectGradingSchemeInfoArr[$thisSubjectID]['LangDisplay'] == 'en') {
				if (str_lang($thisComment)=='ENG') {
					$DisplayDataArr[$Counter]['Comment'] = $this->Get_Text_Span($DisplayDataArr[$Counter]['Comment'], 'eng');
				}
				else {
					$DisplayDataArr[$Counter]['Comment'] = $this->Get_Text_Span($DisplayDataArr[$Counter]['Comment'], 'chi');
				}
				
				// [2018-0510-1530-37164] reduce font size for subject name display (ICT / BAFS)
				if($SubjectIdToCodeMappingArr[$thisSubjectID] == '81N' || $SubjectIdToCodeMappingArr[$thisSubjectID] == '11N'){
				    $DisplayDataArr[$Counter]['SmallerFontStyle'] = true;
				}
				
				$Counter++;
			}
		}
		
		$numOfData = count($DisplayDataArr);
		//2014-0425-0927-57140
		//$RowHeight = 51;
		$RowHeight = 41;
		
		$x = '';
		if ($Counter > 0) {
			$x .= '<table class="border_table font_11pt" border="0" cellspacing="0" cellpadding="10" align="center" style="width:100%;">'."\r\n";
				$x .= '<col style="width:25%" />'."\r\n";
				$x .= '<col style="width:75%" />'."\r\n";
				
				$x .= '<tr class="grey_bg">'."\r\n";
					$x .= '<td colspan="2" style="text-align:center; padding-top:2px; padding-bottom:2px;">'.$this->Get_Text_Span($eReportCard['Template']['SubjectTeacherCommentCh'], 'chi').' '.$this->Get_Text_Span($eReportCard['Template']['SubjectTeacherCommentEn'], 'eng').'</td>'."\r\n";
				$x .= '</tr>'."\r\n";
				
				for ($i=0; $i<$numOfData; $i++) {
					$thisSubjectName = $DisplayDataArr[$i]['SubjectNameCh'].'<br/>'.$DisplayDataArr[$i]['SubjectNameEn'];
					$thisComment = $DisplayDataArr[$i]['Comment'];
					
					$subject_font_style = $DisplayDataArr[$i]['SmallerFontStyle'] ? ' font-size: 13px;' : '';
					
					$x .= '<tr style="height:'.$RowHeight.'px;">'."\r\n";
						//2014-0425-0927-57140
//						$x .= '<td style="vertical-align:top;">'.$thisSubjectName.'</td>'."\r\n";
//						$x .= '<td style="vertical-align:top;"><div style="text-align:justify;">'.$thisComment.'</div></td>'."\r\n";
					    $x .= '<td style="vertical-align:top; padding-top:5px; padding-bottom:5px;'.$subject_font_style.'">'.$thisSubjectName.'</td>'."\r\n";
						$x .= '<td style="vertical-align:top; padding-top:5px; padding-bottom:5px;"><div style="text-align:justify;">'.$thisComment.'</div></td>'."\r\n";
					$x .= '</tr>'."\r\n";
				}
			$x .= '</table>'."\r\n";
		}
		
		return $x;
	}
	
	private function Get_Signature_Table($ReportID, $StudentID='') {
		global $eReportCard;
		
		$ClassTeacherInfoArr = $this->Get_Student_Class_Teacher_Info($StudentID);
		$ClassTeacherNameChArr = Get_Array_By_Key($ClassTeacherInfoArr, 'ChineseName');
		$ClassTeacherNameCh = implode('、', $ClassTeacherNameChArr);
		
		$WidthTitle = 17;
		$WidthColon = 1;
		$WidthContent = 32;
		
		$x = '';
		### Class Teacher Signature
		$x .= '<table class="report_border font_11pt" border="0" cellspacing="0" cellpadding="10" align="center" style="width:100%; height:54px;">'."\r\n";
			$x .= '<col style="width:'.$WidthTitle.'%" />'."\r\n";
			$x .= '<col style="width:'.$WidthColon.'%" />'."\r\n";
			$x .= '<col style="width:'.$WidthContent.'%" />'."\r\n";
			$x .= '<col style="width:'.$WidthTitle.'%" />'."\r\n";
			$x .= '<col style="width:'.$WidthColon.'%" />'."\r\n";
			$x .= '<col style="width:'.$WidthContent.'%" />'."\r\n";
			
			$x .= '<tr>'."\r\n";
				$x .= '<td>'.$this->Get_Text_Span($eReportCard['Template']['ClassTeacherNameCh'], 'chi').'<br />'.$this->Get_Text_Span($eReportCard['Template']['ClassTeacherNameEn'], 'eng').'</td>'."\r\n";
				$x .= '<td>'.$eReportCard['Template']['Colon'].'</td>'."\r\n";
				$x .= '<td>'.$this->Get_Text_Span($ClassTeacherNameCh, 'chi').'</td>'."\r\n";
				$x .= '<td class="border_left">'.$this->Get_Text_Span($eReportCard['Template']['ClassTeacherSignatureCh'], 'chi').'<br />'.$this->Get_Text_Span($eReportCard['Template']['ClassTeacherSignatureEn'], 'eng').'</td>'."\r\n";
				$x .= '<td>'.$eReportCard['Template']['Colon'].'</td>'."\r\n";
				$x .= '<td>&nbsp;</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		$x .= $this->Get_Empty_Image_Div(8);
		
		### Principal Signature
		$x .= '<table class="report_border font_11pt" border="0" cellspacing="0" cellpadding="10" align="center" style="width:100%; height:36px;">'."\r\n";
			$x .= '<col style="width:'.$WidthTitle.'%" />'."\r\n";
			$x .= '<col style="width:'.$WidthColon.'%" />'."\r\n";
			$x .= '<col style="width:'.$WidthContent.'%" />'."\r\n";
			$x .= '<col style="width:'.$WidthTitle.'%" />'."\r\n";
			$x .= '<col style="width:'.$WidthColon.'%" />'."\r\n";
			$x .= '<col style="width:'.$WidthContent.'%" />'."\r\n";
			
			$x .= '<tr>'."\r\n";
				$x .= '<td>'.$this->Get_Text_Span($eReportCard['Template']['PrincipalNameCh'], 'chi').'<br />'.$this->Get_Text_Span($eReportCard['Template']['PrincipalNameEn'], 'eng').'</td>'."\r\n";
				$x .= '<td>'.$eReportCard['Template']['Colon'].'</td>'."\r\n";
				$x .= '<td>'.$this->Get_Text_Span($eReportCard['Template']['PrincipalCh'], 'chi').'</td>'."\r\n";
				$x .= '<td class="border_left">'.$this->Get_Text_Span($eReportCard['Template']['PrincipalSignatureCh'], 'chi').'<br />'.$this->Get_Text_Span($eReportCard['Template']['PrincipalSignatureEn'], 'eng').'</td>'."\r\n";
				$x .= '<td>'.$eReportCard['Template']['Colon'].'</td>'."\r\n";
				$x .= '<td>&nbsp;</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_Academic_Result_Table($ReportID, $StudentID) {
		global $eRCTemplateSetting, $eReportCard, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");
		$lreportcard_award = new libreportcard_award();
		
		### Get Report Info
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$Semester = $ReportInfoArr['Semester'];
		$ReportType = ($Semester=='F')? 'W' : 'T';
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		$termSequence = $this->Get_Semester_Seq_Number($Semester);
		
		// $SbaSubjectAssoArr[$DisplaySubjectID] = $SbaSubjectID
		$SbaSubjectAssoArr = $this->Get_Sba_Subject_Mapping();
		$SbaSubjectIDArr = array_values((array)$SbaSubjectAssoArr);
		
		$SubjectIdToCodeMappingArr = $this->GET_SUBJECTS_CODEID_MAP($withComponent=1);
		$SubjectCodeToIdMappingArr = $this->GET_SUBJECTS_CODEID_MAP($withComponent=1, $mapByCode=1);
		
		
		### Get Report Column Info
		if ($ReportType == 'T') {
			$termSequence = $this->Get_Semester_Seq_Number($Semester);
			
			$ReportColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
			$numOfReportColumn = count($ReportColumnInfoArr);
			$PersonalCharSourceReportID = $ReportID;
			$AwardSourceReportID = $ReportID;
			
			if ($FormNumber == 6 && $termSequence != 1) {
				$ReportColumnInfoArr[$numOfReportColumn]['ReportColumnID'] = 0;
				$ReportColumnInfoArr[$numOfReportColumn]['ColumnTitle'] = $eReportCard['Template']['AnnualOverallCh'].'  '.$eReportCard['Template']['AnnualOverallEn'];
				$numOfReportColumn = count($ReportColumnInfoArr);
			}
		}
		else if ($ReportType == 'W') {
			$TermReportInfoArr = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ReportColumnID="ALL");
			$TermReportIDArr = Get_Array_By_Key($TermReportInfoArr, 'ReportID');
			$numOfTermReport = count($TermReportInfoArr);
			
			$FirstTermReportID = $TermReportIDArr[0];
			$FirstTermReportColumnInfoArr = $this->returnReportTemplateColumnData($FirstTermReportID);
			
			$LastTermReportID = $TermReportIDArr[$numOfTermReport-1];
			$ReportColumnInfoArr = $this->returnReportTemplateColumnData($LastTermReportID);

			$numOfReportColumn = count($ReportColumnInfoArr);
			
			$ReportColumnInfoArr[$numOfReportColumn]['ReportColumnID'] = 0;
			$ReportColumnInfoArr[$numOfReportColumn]['ColumnTitle'] = $eReportCard['Template']['AnnualOverallCh'].'  '.$eReportCard['Template']['AnnualOverallEn'];
			$numOfReportColumn = count($ReportColumnInfoArr);
			
			$PersonalCharSourceReportID = $LastTermReportID;
			$AwardSourceReportID = $LastTermReportID;
		}
		
		### Get Personal Characteristics Info
		$PersonalCharInfoArr = $this->returnPersonalCharSettingData($ClassLevelID, $SubjectID='', $GroupByCharID=1);
		$numOfPersonalChar = count($PersonalCharInfoArr);
		// $StudentPersonalCharInfoArr[$ReportID][$SubjectID][$StudentID][$CharID]
		$StudentPersonalCharInfoArr = $this->getPersonalCharacteristicsProcessedDataByBatch($StudentID, $ParReportID='', $SubjectID='', $ReturnGradeCode=0, $ReturnCharID=1, $ReturnComment=0);
		
		### Subject Grading Scheme Info
		// $GradingSchemeInfoArr[$SubjectID] = SchemeInfoArr
		$GradingSchemeInfoArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID='', $withGrandResult=0, $returnAsso=1, $ReportID);
				
		### Subject Weight Info
		$AllSubjectWeightInfoArr = $this->returnReportTemplateSubjectWeightData($ReportID, '', '', '', $convertEmptyToZero=true);
    	$AllSubjectWeightInfoAssoArr[$ReportID] = BuildMultiKeyAssoc($AllSubjectWeightInfoArr, array("ReportColumnID", "SubjectID"));
		if ($ReportType == 'W') {
			$AllSubjectWeightInfoArr = $this->returnReportTemplateSubjectWeightData($LastTermReportID, '', '', '', $convertEmptyToZero=true);
    		$AllSubjectWeightInfoAssoArr[$LastTermReportID] = BuildMultiKeyAssoc($AllSubjectWeightInfoArr, array("ReportColumnID", "SubjectID"));
		}
		
		### Mark Array
		// $MarksArr[$thisReportID][$thisStudentID][$thisSubjectID][$thisReportColumnID][DBField] = Value
		if ($ReportType == 'T') {
			$MarksArr[$ReportID] = $this->getMarks($ReportID);
		}
		else if ($ReportType == 'W') {
			$MarksArr[$FirstTermReportID] = $this->getMarks($FirstTermReportID);
			$MarksArr[$LastTermReportID] = $this->getMarks($LastTermReportID);
			$MarksArr[$ReportID] = $this->getMarks($ReportID, '', '', 0, 1, '', '', 0);
		}
		
		### Subject Teacher Comment
		//$CommentArr = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		
		### Student Award Info
		$StudentAwardArr = $lreportcard_award->Get_Award_Generated_Student_Record($AwardSourceReportID, $StudentIDArr='', $ReturnAsso=0, $AwardNameWithSubject=0, $AwardIDArr='', $SubjectIDArr='', $ShowInReportCardOnly=1);
		$StudentAwardAssoArr = BuildMultiKeyAssoc($StudentAwardArr, array('StudentID', 'SubjectID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		
		
		### Subject Array
		$MainSubjectChArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='ch', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		$MainSubjectEnArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='en', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		
		### Special Handling Array
		$TermElectiveSubjectIDArr = $this->Get_Term_Elective_Subject($FormNumber, $StudentID);
				
		### Table Width
		// $SubjectWidth = 10;
		// $ReportColumnWidth = 30;
		// $PersonalCharWidth = 30;
		// $RemarksWidth = 10;
		
		// if ($numOfReportColumn == 0) {
			// $ReportColumnWidth = 0;
		// }
		// else {
			// $ReportColumnWidth = floor($ReportColumnWidth / $numOfReportColumn);
		// }
		
		// if ($numOfPersonalChar == 0) {
			// $PersonalCharColumnWidth = 0;
		// }
		// else {
			// $PersonalCharColumnWidth = floor($PersonalCharWidth / $numOfPersonalChar);
		// }
		if ($termSequence == 1) {
			# term 1 report
			$SubjectWidth = 17;
			$ReportColumnWidth = 28;
//			$LearningAttitudeWidth = 28;
//			$LearningHabitWidth = 28;
			$RemarksWidth = 0;
		} else {
			# other report
			$SubjectWidth = 14;
			$ReportColumnWidth = 26;
//			$LearningAttitudeWidth = 26;
//			$LearningHabitWidth = 26;
			$RemarksWidth = 8;
		}
		
		$numOfLearningAttitude = 3;
		$numOfLearningHabit = $numOfPersonalChar - $numOfLearningAttitude;
		
		$PersonalCharColumnWidth = floor((100 - $SubjectWidth - $ReportColumnWidth - $RemarksWidth) / $numOfPersonalChar);
		$LearningAttitudeWidth = $PersonalCharColumnWidth * $numOfLearningAttitude;
		$LearningHabitWidth = $PersonalCharColumnWidth * $numOfLearningHabit;
		
		### Font size
		$engTitleCss = 'font_8pt';
				
		$x = '';
		$x .= '<table class="border_table_outerLine font_10pt" border="0" cellspacing="0" cellpadding="2" align="center" style="table-layout:fixed; width:100%; text-align:center;">'."\r\n";
			// $x .= '<col style="width:'.$SubjectWidth.'%" />'."\r\n";
			// for ($i=0; $i<$numOfReportColumn; $i++) {
				// $x .= '<col style="width:'.$ReportColumnWidth.'%" />'."\r\n";
			// }
			// for ($i=0; $i<$numOfPersonalChar; $i++) {
				// $x .= '<col style="width:'.$PersonalCharColumnWidth.'%" />'."\r\n";
			// }
			// $x .= '<col style="width:'.$RemarksWidth.'%" />'."\r\n";
			
			
			$x .= '<tr>'."\r\n";
				$x .= '<td rowspan="2" style="width:'.$SubjectWidth.'%;">'.$this->Get_Text_Span($eReportCard['Template']['SubjectCh'], 'chi').'<br />'.$this->Get_Text_Span($eReportCard['Template']['SubjectEn'], 'eng', $engTitleCss).'</td>'."\r\n";
				$x .= '<td class="border_left" colspan="'.$numOfReportColumn.'" style="width:'.$ReportColumnWidth.'%;">'.$this->Get_Text_Span($eReportCard['Template']['AcademicResultCh'], 'chi').'<br />'.$this->Get_Text_Span($eReportCard['Template']['AcademicResultEn'], 'eng', $engTitleCss).'</td>'."\r\n";
				$x .= '<td class="border_left" colspan="'.$numOfLearningAttitude.'" style="width:'.$LearningAttitudeWidth.'%;">'.$this->Get_Text_Span($eReportCard['Template']['LearningAttitudeCh'], 'chi').'<br />'.$this->Get_Text_Span($eReportCard['Template']['LearningAttitudeEn'], 'eng', $engTitleCss).'</td>'."\r\n";
				$x .= '<td class="border_left" colspan="'.$numOfLearningHabit.'" style="width:'.$LearningHabitWidth.'%;">'.$this->Get_Text_Span($eReportCard['Template']['LearningHabitCh'], 'chi').'<br />'.$this->Get_Text_Span($eReportCard['Template']['LearningHabitEn'], 'eng', $engTitleCss).'</td>'."\r\n";
				if ($termSequence != 1) {
					$x .= '<td rowspan="2" class="border_left" style="width:'.$RemarksWidth.'%;">'.$this->Get_Text_Span($eReportCard['Template']['RemarksCh'], 'chi').'<br />'.$this->Get_Text_Span($eReportCard['Template']['RemarksEn'], 'eng', $engTitleCss).'</td>'."\r\n";
				}
			$x .= '</tr>'."\r\n";
			
			$x .= '<tr>'."\r\n";
				for ($i=0; $i<$numOfReportColumn; $i++) {
					$thisColumnTitle = $ReportColumnInfoArr[$i]['ColumnTitle'];
					$hasDoubleSpace = strpos($thisColumnTitle, "  ");
					if($hasDoubleSpace==false) {
						$thisColumnTitle = $this->Get_Text_Span($thisColumnTitle, 'chi');
					}
					else {
						$_thisColumnTitleArr = explode("  ",$thisColumnTitle);
						if(count($_thisColumnTitleArr) >= 2) {
							$_ColumnTitleCh = $this->Get_Text_Span($_thisColumnTitleArr[0], 'chi');
							
							if ($FormNumber==6 && $ReportType=='W' && $i==0) {
								$targetEngCss = 'font_5pt';
							}
							else {
								$targetEngCss = $engTitleCss;
							}
							//$_ColumnTitleEn = $this->Get_Text_Span($_thisColumnTitleArr[1], 'eng', $engTitleCss);
							$_ColumnTitleEn = $this->Get_Text_Span($_thisColumnTitleArr[1], 'eng', $targetEngCss);
							$thisColumnTitle = $_ColumnTitleCh.'<br />'.$_ColumnTitleEn;
						}						
					}
					// if ($termSequence == 1) {
						# hide column name for 1st term report
						// $thisColumnTitle = '&nbsp;';
					// }
					
					$thisClass = ($i==0)? 'border_left' : 'border_left_dot';
					$x .= '<td class="border_top '.$thisClass.'" style="padding-left:0px; padding-right:0px;">'.$thisColumnTitle.'</td>'."\r\n";
				}
				
				for ($i=0; $i<$numOfPersonalChar; $i++) {
					$thisPersonalCharTitleCh = $this->Get_Text_Span(wordwrap($PersonalCharInfoArr[$i]['Title_CH'], 12, "<br/>", true), 'chi');	// add line-break every 4 chinese characters
					$thisPersonalCharTitleEn = $this->Get_Text_Span($PersonalCharInfoArr[$i]['Title_EN'], 'eng', $engTitleCss);
					$thisPersonalCharTitle = $thisPersonalCharTitleCh.'<br />'.$thisPersonalCharTitleEn;
					
					$thisClass = ($i==0 || $i==$numOfLearningAttitude)? 'border_left' : 'border_left_dot';
					$x .= '<td class="border_top '.$thisClass.'" style="padding-left:0px; padding-right:0px;">'.$thisPersonalCharTitle.'</td>'."\r\n";
				}
			//	$x .= '<td class="border_top border_left_thick">&nbsp;</td>'."\r\n";
			$x .= '</tr>'."\r\n";


	
			$count_subject = 0;
			foreach ((array)$MainSubjectChArr as $thisParentSubjectID => $thisParentSubjectArr) {
				foreach ((array)$thisParentSubjectArr as $thisSubjectID => $thisSubjectName) {
					$SubjectNameCh = $thisSubjectName;
					$SubjectNameEn = $MainSubjectEnArr[$thisParentSubjectID][$thisSubjectID];
					$thisPersonalCharSourceReportID = $PersonalCharSourceReportID;
					
					if ($thisSubjectID==0) {
						$thisIsComponentSubject = false;
						$thisSubjectID = $thisParentSubjectID;
						$BorderTopDot = 'border_top_dot ';
						$thisBorderTop = 'border_top_dot  ';					
						$thisTextAlign = 'left';
						$thisCmpWebSAMSCode = '';
					}
					else {
						$thisIsComponentSubject = true;
						$BorderTopDot='';
						$thisBorderTop = '';
						$thisTextAlign = 'right';
						$thisCmpWebSAMSCode = $SubjectIdToCodeMappingArr[$thisSubjectID]['CMP_CODEID'];
					}
					$thisParentSubjectWebSAMSCode = $SubjectIdToCodeMappingArr[$thisParentSubjectID]['CODEID'];
					
					### skip component subject for 1st term report
					if ($termSequence == 1 && $thisIsComponentSubject) {
						continue;
					}
					
					### skip exam subject for 1st term report
					$thisIsExamSubject = $this->Is_Exam_Subject($ReportID, $thisSubjectID);
					if ($termSequence == 1 && !$thisIsExamSubject) {
						continue;
					}
					
					if (in_array($thisCmpWebSAMSCode, (array)$SbaSubjectIDArr)) {
						// do not display SBA subject in the report card
						continue;
					}
					
					if($count_subject==0) {
						$thisBorderTop = 'border_top';
						$BorderTopDot = 'border_top';
					}
					
					# Grading Scheme Info
					$thisScaleInput = $GradingSchemeInfoArr[$thisSubjectID]['ScaleInput'];
					$thisScaleDisplay = $GradingSchemeInfoArr[$thisSubjectID]['ScaleDisplay'];
					
					# Report Column Marks
					$thisIsAllNA = true;
					$thisIsAllAssessmentNA = true;
					$thisMarkDisplayArr = array();
					for ($i=0; $i<$numOfReportColumn; $i++) {
						$thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];
						$thisColumnTitle = $ReportColumnInfoArr[$i]['ColumnTitle'];
						
						$thisTargetReportID = '';
						if ($ReportType == 'T') {
							$thisTargetReportID = $ReportID;
						}
						else if ($ReportType == 'W') {
							$thisTargetReportID = ($thisReportColumnID == 0)? $ReportID : $LastTermReportID;
						}
						
						### SBA Handling for S5, S6
						$thisOriginalSubjectID = '';
						//if ($thisIsComponentSubject && $this->Is_SBA_Column($thisColumnTitle) && ($FormNumber == 5 || $FormNumber == 6)) {
						if ($thisIsComponentSubject && $this->Is_SBA_Column($thisColumnTitle)) {
							$thisCorrespondingSbaSubjectWebSAMSCode = $SbaSubjectAssoArr[$thisCmpWebSAMSCode];
							if ($thisCorrespondingSbaSubjectWebSAMSCode != '') {
								// display the SBA subject mark here
								$thisSbaSubjectID = $SubjectCodeToIdMappingArr[$thisParentSubjectWebSAMSCode.'_'.$thisCorrespondingSbaSubjectWebSAMSCode];
								$thisOriginalSubjectID = $thisSubjectID;
								$thisSubjectID = $thisSbaSubjectID;								
							}
						}
						
						$thisMSMark = $MarksArr[$thisTargetReportID][$StudentID][$thisSubjectID][$thisReportColumnID]['Mark'];
						$thisMSGrade = $MarksArr[$thisTargetReportID][$StudentID][$thisSubjectID][$thisReportColumnID]['Grade'];
						
						$thisGrade = ($thisScaleDisplay=="G" || $thisScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($thisScaleDisplay=="M" && !$this->Check_If_Grade_Is_SpecialCase($thisGrade)) ? $thisMSMark : "";
						
						# for preview purpose
						if(!$StudentID) {
							$thisMark 	= $thisScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $thisScaleDisplay=="G" ? "G" : "";
						}
						
						$thisMark = ($thisScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						if ($thisMark != "N.A.") {
							$thisIsAllNA = false;
						}
						
						if ($thisMark != "N.A." && $thisReportColumnID != 0) {
							$thisIsAllAssessmentNA = false;
						}
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($thisTargetReportID, $thisSubjectID, $thisMark, $thisMSGrade);
						
						if ($needStyle) {
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $thisTargetReportID, $ClassLevelID, $thisSubjectID, $thisMark);
						}
						else {
							$thisMarkDisplay = $thisMark;
						}
						
						$thisMarkDisplayArr[$thisReportColumnID] = $thisMarkDisplay;
						
						// restore the display for the original subject for SBA
						if ($thisIsComponentSubject && $this->Is_SBA_Column($thisColumnTitle)) {
							$thisCorrespondingSbaSubjectWebSAMSCode = $SbaSubjectAssoArr[$thisCmpWebSAMSCode];
							if ($thisCorrespondingSbaSubjectWebSAMSCode != '' && $thisOriginalSubjectID != '') {
								$thisSubjectID = $thisOriginalSubjectID;
							}
						}
					}
					
					if (in_array($thisSubjectID, (array)$TermElectiveSubjectIDArr)) {
						// always show the term elective subjects
						$thisIsAllNA = false;
						
						// for consolidated report, if all assessments are N.A. in the second term, then show data from 1st term report
						if ($ReportType == 'W' && $thisIsAllAssessmentNA) {
							// show personal char data of first term 
							$thisPersonalCharSourceReportID = $FirstTermReportID;
							
							// show mark of first term
							$thisTargetReportID = $FirstTermReportID;
							
							for ($i=0; $i<$numOfReportColumn; $i++) {
								$thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];
								$thisFirstTermReportColumnID = $FirstTermReportColumnInfoArr[$i]['ReportColumnID'];
								
								if ($thisReportColumnID == 0) {
									continue;
								}
																
								$thisMSMark = $MarksArr[$thisTargetReportID][$StudentID][$thisSubjectID][$thisFirstTermReportColumnID]['Mark'];
								$thisMSGrade = $MarksArr[$thisTargetReportID][$StudentID][$thisSubjectID][$thisFirstTermReportColumnID]['Grade'];
								
								$thisGrade = ($thisScaleDisplay=="G" || $thisScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($thisScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
								
								$thisMark = ($thisScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
								
								# check special case
								list($thisMark, $needStyle) = $this->checkSpCase($thisTargetReportID, $thisSubjectID, $thisMark, $thisMSGrade);
								if ($needStyle) {
									$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $thisTargetReportID, $ClassLevelID, $thisSubjectID, $thisMark);
								}
								else {
									$thisMarkDisplay = $thisMark;
								}
								
								$thisMarkDisplayArr[$thisReportColumnID] = $thisMarkDisplay;
							}
						}
					}

					// [2019-1113-1652-40073] Term 1 Report - check if always display LA Grade
					$isLA_GradeSubject = false;
					if($termSequence == 1)
                    {
                        $SubjectCodeAssoArr = $this->GET_SUBJECTS_CODEID_MAP($withComponent=0, $mapByCode=0);
                        $thisSubjectCode = $SubjectCodeAssoArr[$thisSubjectID];
                        if(isset($eRCTemplateSetting['Report']['LA_GradeSubject'][$thisSubjectCode]) && $this->Is_Student_In_Subject_Group_Of_Subject($ReportID, $StudentID, $thisSubjectID, $Semester))
                        {
                            $SubjectCodeApplicableForms = $eRCTemplateSetting['Report']['LA_GradeSubject'][$thisSubjectCode];
                            if(!empty($SubjectCodeApplicableForms) && is_array($SubjectCodeApplicableForms) && in_array($FormNumber, $SubjectCodeApplicableForms)) {
                                $isLA_GradeSubject = true;
                            }
                            else if (!is_array($SubjectCodeApplicableForms) && $SubjectCodeApplicableForms === true) {
                                $isLA_GradeSubject = true;
                            }
                        }
                    }

                    // [2019-1113-1652-40073] always display LA Grade
                    if($isLA_GradeSubject) {
					    // do nothing
                    }
					// [2018-1120-0926-05073] F.1 - always display LA Grade for 4 subjects
					// 20151008: Email from Stephen "FW: FKYC: 中一級宗教科CA分ㄔ峊X, 只出LA Grades_Fanling Kau Yan College"
					else if ($FormNumber==1 && ($thisSubjectID==$this->Get_SubjectID_From_Config('Religion') ||
                    					    $thisSubjectID==$this->Get_SubjectID_From_Config('LS') || $thisSubjectID==$this->Get_SubjectID_From_Config('ChineseHist') ||
                    					    $thisSubjectID==$this->Get_SubjectID_From_Config('EconBusiness') || $thisSubjectID==$this->Get_SubjectID_From_Config('Geog'))
					    ) {
						// do nothing
					}
					else if ($thisIsAllNA) {
                        continue;
					}
					
					### Subject Remarks (changed to retrieve the award info) 
					//$thisRemarks = nl2br(stripslashes($CommentArr[$thisSubjectID]));
					//$thisRemarks = ($thisRemarks=='')? '&nbsp;' : $thisRemarks;

					$x .= '<tr>'."\r\n";
						# Subject Name
						$x .= '<td class="'.$thisBorderTop.'" style="text-align:'.$thisTextAlign.';">'.$this->Get_Text_Span($SubjectNameCh, 'chi').'<br />'.$this->Get_Text_Span($SubjectNameEn, 'eng', 'font_9pt').'</td>'."\r\n";
						
						# Mark
						for ($i=0; $i<$numOfReportColumn; $i++) {
							$thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];
							$thisColumnTitle = $ReportColumnInfoArr[$i]['ColumnTitle'];
							
							// Display SBA Subject info if available
							if ($thisIsComponentSubject && $this->Is_SBA_Column($thisColumnTitle)) {
								$thisCorrespondingSbaSubjectWebSAMSCode = $SbaSubjectAssoArr[$thisCmpWebSAMSCode];
								if ($thisCorrespondingSbaSubjectWebSAMSCode != '') {
									$thisSbaSubjectID = $SubjectCodeToIdMappingArr[$thisParentSubjectWebSAMSCode.'_'.$thisCorrespondingSbaSubjectWebSAMSCode];
									$thisOriginalSubjectID = $thisSubjectID;
									$thisSubjectID = $thisSbaSubjectID;
								}
							}
							
							if ($ReportType=='T') {
								$thisTargetReportID = $ReportID;
							}
							else if ($ReportType == 'W') {
								$thisTargetReportID = ($thisReportColumnID == 0)? $ReportID : $LastTermReportID;
							}
							
							$thisColumnSubjectWeight = $AllSubjectWeightInfoAssoArr[$thisTargetReportID][$thisReportColumnID][$thisSubjectID]['Weight'];
							
							if ($thisMarkDisplayArr[$thisReportColumnID] == 'N.A.') {
								$thisColumnMark = '&nbsp;';
								$thisGreyBg = 'grey_bg';
							}
							else {
								//$thisColumnMark = ($thisIsComponentSubject && $thisColumnSubjectWeight==0)? '&nbsp;' : $thisMarkDisplayArr[$thisReportColumnID];
								//$thisGreyBg = ($thisIsComponentSubject && $thisColumnSubjectWeight==0)? 'grey_bg' : '';
								
								//2012-0503-1054-27099
								//$thisColumnMark = ($thisColumnSubjectWeight==0)? '&nbsp;' : $thisMarkDisplayArr[$thisReportColumnID];
								//$thisGreyBg = ($thisColumnSubjectWeight==0)? 'grey_bg' : '';
								//2012-0521-1554-34099
								//$thisColumnMark = ($ReportType=='T' && $thisColumnSubjectWeight==0)? '&nbsp;' : $thisMarkDisplayArr[$thisReportColumnID];
								//$thisGreyBg = ($ReportType=='T' && $thisColumnSubjectWeight==0)? 'grey_bg' : '';
								if ($ReportType=='W' && $thisReportColumnID==0) {
									$thisColumnMark = $thisMarkDisplayArr[$thisReportColumnID];
									$thisGreyBg = '';
								}
								else {
									$thisColumnMark = ($thisColumnSubjectWeight==0)? '&nbsp;' : $thisMarkDisplayArr[$thisReportColumnID];
								 	$thisGreyBg = ($thisColumnSubjectWeight==0)? 'grey_bg' : '';
								}
							}
							$thisBorderLeft = ($i==0)? 'border_left' : 'border_left_dot';
							
							$x .= '<td class="'.$thisBorderTop.' '.$thisBorderLeft.' '.$thisGreyBg.'">'.$this->Get_Text_Span($thisColumnMark, 'eng').'</td>'."\r\n";
							
							// Restore the Subject display to the original Subject
							if ($thisIsComponentSubject && $this->Is_SBA_Column($thisColumnTitle)) {
								$thisCorrespondingSbaSubjectWebSAMSCode = $SbaSubjectAssoArr[$thisCmpWebSAMSCode];
								if ($thisCorrespondingSbaSubjectWebSAMSCode != '' && $thisOriginalSubjectID != '') {
									$thisSubjectID = $thisOriginalSubjectID;
								}
							}
						}
						
						# Personal Characteristics
						for ($i=0; $i<$numOfPersonalChar; $i++) {
							$thisPersonalCharID = $PersonalCharInfoArr[$i]['CharID'];
							$thisPersonalCharGrade = $StudentPersonalCharInfoArr[$thisPersonalCharSourceReportID][$thisSubjectID][$StudentID][$thisPersonalCharID];
							
							$thisBorderLeft = ($i==0 || $i==$numOfLearningAttitude)? 'border_left' : 'border_left_dot';
							$thisGreyBg = ($thisIsComponentSubject || $thisPersonalCharGrade=='')? 'grey_bg' : '';
							$thisPersonalCharGrade = ($thisPersonalCharGrade=='')? '&nbsp;':$thisPersonalCharGrade;
							
							$x .= '<td class="'.$BorderTopDot.' '.$thisBorderLeft.' '.$thisGreyBg.'">'.$this->Get_Text_Span($thisPersonalCharGrade, 'eng').'</td>'."\r\n";
						}
						
						# Remarks
						if ($termSequence != 1) {
							$RemarksDisplay = '';
							$thisStudentSubjectAwardArr = $StudentAwardAssoArr[$StudentID][$thisSubjectID];
							$thisNumAward = count($thisStudentSubjectAwardArr);
							$thisGreyBg = ($thisIsComponentSubject) ? 'grey_bg' : '';
							if ($thisNumAward == 0) {
								$RemarksDisplay = '&nbsp;';
							}
							else {
								$RemarksDisplay = $this->Get_Text_Span($thisStudentSubjectAwardArr[0]['AwardNameCh'], 'chi').'<br /><span class="font_8pt">'.$this->Get_Text_Span($thisStudentSubjectAwardArr[0]['AwardNameEn'], 'eng').'</span>';
							}
							$x .= '<td class="'.$thisBorderTop.' border_left '.$thisGreyBg.'">'.$RemarksDisplay.'</td>'."\r\n";
						}
						
					$x .= '</tr>'."\r\n";
					
					$count_subject++;
				}
		
			}
				
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_Grading_Scheme_Remarks_Table($ReportID, $StudentID) {
		global $eReportCard;
		
		$AcademicResultTdArr = array();
		foreach ((array)$eReportCard['GradeDescriptionArr'] as $thisGrade => $thisDescription) {
			$AcademicResultTdArr[] = '<td>'.$thisGrade.'</td><td style="text-align:left;">'.$thisDescription.'</td>';
		}
		$numOfRow = count($AcademicResultTdArr);
		
		$PersonalCharTdArr = array();
		foreach ((array)$eReportCard['PersonalCharDescriptionArr'] as $thisGrade => $thisDescription) {
			$PersonalCharTdArr[] = '<td>'.$thisGrade.'</td><td style="text-align:left;">'.$thisDescription.'</td>';
		}
		
		$GradeWidth = 12;
		$DescriptionWidth = 38;
		
		$x = '';
		$x .= '<table class="border_table font_10pt" border="0" cellspacing="0" cellpadding="5" align="center" style="width:100%; text-align:center;">'."\r\n";
			$x .= '<col style="width:'.$GradeWidth.'%" class="grey_bg" />'."\r\n";
			$x .= '<col style="width:'.$DescriptionWidth.'%" class="grey_bg" />'."\r\n";
			$x .= '<col style="width:'.$GradeWidth.'%" />'."\r\n";
			$x .= '<col style="width:'.$DescriptionWidth.'%" />'."\r\n";
			
			$x .= '<tr>'."\r\n";
				$x .= '<td>'.$eReportCard['Template']['AcademicResultCh'].'</td>'."\r\n";
				$x .= '<td>'.$eReportCard['Template']['GradeDescriptionCh'].'</td>'."\r\n";
				$x .= '<td>'.$eReportCard['Template']['LearningAttitudeCh'].'</td>'."\r\n";
				$x .= '<td>'.$eReportCard['Template']['GradeDescriptionCh'].'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			
			for ($i=0; $i<$numOfRow; $i++) {
				$x .= '<tr>'."\r\n";
					$x .= $AcademicResultTdArr[$i]."\r\n";
					$x .= $PersonalCharTdArr[$i]."\r\n";
				$x .= '</tr>'."\r\n";
			}
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_OLE_Info_Table($ReportID, $StudentID='') {
		
		global $eReportCard, $PATH_WRT_ROOT,$ipf_cfg;
		include_once($PATH_WRT_ROOT."/includes/portfolio25/iPortfolioConfig.inc.php");
		include_once($PATH_WRT_ROOT."/includes/portfolio25/slp/libpf-ole.php");
		//include_once($PATH_WRT_ROOT."/includes/portfolio25/libpf-slp.php");
		
		$li_ole = new libpf_ole();
		//$li_slp = new libpf_award();
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$Semester = $ReportInfoArr['Semester'];
		$ReportType = ($Semester=='F')? 'W' : 'T';
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		// F1-5 Show OLE ELE, F6 show Organisers
		if ($FormNumber <= 5) {
			$showOrganizer = false;
			$showOleEle = true;
		}
		else {
			$showOrganizer = true;
			$showOleEle = false;
		}
		
		// Get Student OLE data
		$OLEInfoArray = array();
		if ($StudentID != '') {
			if ($FormNumber <= 5) {
				$OleCategoryArr = array(7, 14);		// 7:聯課活動(第一期); 14:聯課活動(第二期);
				$TargetAcademicYearID = $this->GET_ACTIVE_YEAR_ID();
				$TargetIntExt = strtoupper($ipf_cfg["OLE_TYPE_STR"]["INT"]);
				$forSlpOnly = true;
				$forSlpOnly = '';
				$OrderByField = 'StartDate';
			}
			else {
				$OleCategoryArr = '';
				$TargetAcademicYearID = '';
				$TargetIntExt = '';
				$forSlpOnly = true;
				$OrderByField = '';
			}
			
			$ELEAssoArray = BuildMultiKeyAssoc($li_ole->Get_ELE_Info(), 'ELECode');
			$OLEInfoArray = $li_ole->Get_Student_OLE_Info($StudentID, $RecordIDArr='', $forSlpOnly, $OleCategoryArr, $TargetAcademicYearID, $TargetIntExt, $OrderByField);
			
			// 2012-0620-1200-30073
			if ($FormNumber <= 5) {
				$numOfOle = count($OLEInfoArray);
				$tempOleInfoAry = array();
				for ($i=0; $i<$numOfOle; $i++) {
					$_oleProgramID = $OLEInfoArray[$i]['ProgramID'];
					$_oleTitle = trim($OLEInfoArray[$i]['ProgramTitle']);
					$_oleHours = $OLEInfoArray[$i]['Hours'];
					$_oleRole = trim($OLEInfoArray[$i]['Role']);
					$_oleEle = trim($OLEInfoArray[$i]['ELE']);
					
					$_oleFound = false;
					$numOfTempOle = count($tempOleInfoAry);
					for ($j=0; $j<$numOfTempOle; $j++) {
						$_tempOleProgramID = $OLEInfoArray[$j]['ProgramID'];
						$_tempOleTitle = trim($tempOleInfoAry[$j]['ProgramTitle']);
						$_tempOleHours = $tempOleInfoAry[$j]['Hours'];
						$_tempOleRole = trim($OLEInfoArray[$j]['Role']);
						$_tempOleEle = trim($OLEInfoArray[$j]['ELE']);
						
						if ($_oleProgramID == $_tempOleProgramID) {
							continue;
						}
						
						if ($_oleTitle == $_tempOleTitle) {
							// found in temp array => merge ole info
							$_oleFound = true;
							
							// Sum all related OLE hours
							$tempOleInfoAry[$j]['Hours'] = $_oleHours + $_tempOleHours;
							
							// merge roles data if not the same
							if ($_oleRole != $_tempOleRole) {
								$OLEInfoArray[$i]['Role'] = $_tempOleRole.$eReportCard['Template']['ComponentOfOleSeparator'].$_oleRole;
							}
							
							// merge roles data if not the same
							if ($_oleRole != $_tempOleRole) {
								$OLEInfoArray[$i]['Role'] = $_tempOleRole.$eReportCard['Template']['ComponentOfOleSeparator'].$_oleRole;
							}
						}
					}
					
					if (!$_oleFound) {
						$tempOleInfoAry[] = $OLEInfoArray[$i];
					}
				}
				
				$OLEInfoArray = $tempOleInfoAry;
			}
		}		

		// Get Student Award data
		//$AwardListArr = $li_slp->getAwardStudentList($StudentID);
		$TargetTerm = ($ReportType=='W')? 0 : $Semester;
		$OtherInfoDataArr = $this->getReportOtherInfoData($ReportID, $StudentID);
		$OtherInfoDataArr = $OtherInfoDataArr[$StudentID][$TargetTerm];
		
		if (!is_array($OtherInfoDataArr['Award'])) {
			$OtherInfoDataArr['Award'] = array($OtherInfoDataArr['Award']);
		}
		if (!is_array($OtherInfoDataArr['Organiser'])) {
			$OtherInfoDataArr['Organiser'] = array($OtherInfoDataArr['Organiser']);
		}
		$numOfAward = count($OtherInfoDataArr['Award']);
		
		$AwardListArr = array();
		for ($i=0; $i<$numOfAward; $i++) {
			$_awardName = $OtherInfoDataArr['Award'][$i];
			$_organization = $OtherInfoDataArr['Organiser'][$i];
			
			if ($_awardName == '' && $_organization == '') {
				continue;
			}
			
			$AwardListArr[] = array('AwardName' => $OtherInfoDataArr['Award'][$i], 'Organization' => $OtherInfoDataArr['Organiser'][$i]);
		}
				
		$GradeWidth = 12;
		$DescriptionWidth = 38;
		
		//2014-0425-1430-36140
//		$MaxNumOfActivity = 20;
//		$MaxNumOfAward = 8;
		//2014-0425-1430-36140 (Internal) Follow-up by karsonyam on 2014-04-25 17:42
//		$MaxNumOfActivity = 18;
//		$MaxNumOfAward = 12;
		$MaxNumOfActivity = 13;
		$MaxNumOfAward = 12;

		$x = '';
		$x .= '<table class="font_10pt" align="center" style="width:100%;">';
//			$x .='<tr><td>';
//				$x .= '<table class="font_11pt" border="0" cellspacing="0" cellpadding="5" style="width:100%; text-align:left;">'."\r\n";
//					$x .='<tr>';
//						$x .='<td><b><u>'.$this->Get_Text_Span($eReportCard['Template']['ActivitiesCh'], 'chi').' '.$this->Get_Text_Span($eReportCard['Template']['ActivitiesEn'], 'eng').'</u></b></td>';
//					$x .='</tr>';
//				$x .= '</table>';		
//			$x .='</td></tr>';

			### Activities
			if ($showOrganizer) {
				$thisTitleCh = $eReportCard['Template']['ActivitiesCh'];
				$thisTitleEn = $eReportCard['Template']['ActivitiesEn'];
			}
			else if ($showOleEle) {
				$thisTitleCh = $eReportCard['Template']['CoCurricularActivitiesCh'];
				$thisTitleEn = $eReportCard['Template']['CoCurricularActivitiesEn'];
			}
			$x .='<tr>';
				$x .='<td class="font_11pt">';
					$x .='<b><u>'.$this->Get_Text_Span($thisTitleCh, 'chi').' '.$this->Get_Text_Span($thisTitleEn, 'eng').'</u></b>';
				$x .='</td>';		
			$x .='</tr>';
			
			$x .='<tr><td>';	
				$x .= '<table border="0" cellspacing="0" cellpadding="5" align="center" style="width:100%; text-align:center; height:360px;">'."\r\n";
					$x .= '<col style="width:5%" />';
					$x .= '<col style="width:30%" />';
					$x .= '<col style="width:30%" />';
					$x .= '<col style="width:20%" />';
					$x .= '<col style="width:15%" />';
								
					$x .= '<tr>'."\r\n";
						$x .= '<td class="grey_bg border_bottom border_top border_left">'.'&nbsp;'.'</span></td>'."\r\n";
						$x .= '<td class="grey_bg border_bottom border_top border_left">'.$this->Get_Text_Span($eReportCard['Template']['EventsCh'], 'chi').'<br />'.$this->Get_Text_Span($eReportCard['Template']['EventsEn'], 'eng').'</td>'."\r\n";
						
						if ($showOrganizer) {
							$titleCh = $eReportCard['Template']['PartnerOrganizationsCh'];
							$titleEn = $eReportCard['Template']['PartnerOrganizationsEn'];
						}
						else if ($showOleEle) {
							$titleCh = $eReportCard['Template']['ComponentOfOleCh'];
							$titleEn = $eReportCard['Template']['ComponentOfOleEn'];
						}
						$x .= '<td class="grey_bg border_bottom border_top border_left">'.$this->Get_Text_Span($titleCh, 'chi').'<br />'.$this->Get_Text_Span($titleEn, 'eng').'</td>'."\r\n";
						
						$x .= '<td class="grey_bg border_bottom border_top border_left">'.$this->Get_Text_Span($eReportCard['Template']['RolesCh'], 'chi').'<br />'.$this->Get_Text_Span($eReportCard['Template']['RolesEn'], 'eng').'</td>'."\r\n";
						$x .= '<td class="grey_bg border_bottom border_top border_left border_right">'.$this->Get_Text_Span($eReportCard['Template']['HoursCh'], 'chi').'<br />'.$this->Get_Text_Span($eReportCard['Template']['HoursEn'], 'eng').'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
					
					$numOfRow = min($MaxNumOfActivity, count($OLEInfoArray));
					for ($i=0; $i<$numOfRow; $i++) {
						$_programTitle = $OLEInfoArray[$i]['ProgramTitle'];
						$_organization = $OLEInfoArray[$i]['Organization'];
						$_role = $OLEInfoArray[$i]['Role'];
						$_hours = $OLEInfoArray[$i]['Hours'];
						$_intExt = $OLEInfoArray[$i]['IntExt'];
						$_ele = $OLEInfoArray[$i]['ELE'];
						
						if($i==($MaxNumOfActivity-1)) {
							$border_bottom = 'border_bottom';
						}
						else {
							$border_bottom = '';
						}
						
						$_programTitle = ($_programTitle=='')? '&nbsp;' : $_programTitle;
						
						if ($showOrganizer) {
							$_organization = ($_organization=='')? '&nbsp;' : $_organization;
						}
						else if ($showOleEle) {
							$_eleDisplayAry = array();
							if ($_ele != '') {
								$_eleAry = explode(',', $_ele);
								$_countELE = sizeof($_eleAry);
								for($j=0; $j<$_countELE; $j++) {
									$__ele = trim($_eleAry[$j]);						
									$_eleDisplayAry[] = $ELEAssoArray[$__ele]['ChiTitle'];
								}
							}
							$_eleDisplayText = implode($eReportCard['Template']['ComponentOfOleSeparator'], $_eleDisplayAry);
							$_eleDisplayText = ($_eleDisplayText=='')? '&nbsp;' : $_eleDisplayText;
							
							$_organization = $_eleDisplayText; 
						}
						
						$_role = ($_role=='')? '&nbsp;' : $_role;
						$_hours = ($_hours=='' || (strtolower($_intExt)==strtolower($ipf_cfg["OLE_TYPE_STR"]["EXT"])))? '&nbsp;' : $_hours;
						
						
						$x .= '<tr>'."\r\n";
							$x .= '<td style="text-align:left;vertical-align:top;padding-bottom:1px;" class="border_left '.$border_bottom.'">'.$this->Get_Text_Span(($i+1).'.', 'eng').'</td>';
							$x .= '<td style="text-align:left;vertical-align:top;padding-bottom:1px;" class="border_left '.$border_bottom.'">'.$this->Get_Text_Span($_programTitle, 'chi').'</td>';
							$x .= '<td style="text-align:left;vertical-align:top;padding-bottom:1px;" class="border_left '.$border_bottom.'">'.$this->Get_Text_Span($_organization, 'chi').'</td>';
							$x .= '<td style="text-align:left;vertical-align:top;padding-bottom:1px;" class="border_left '.$border_bottom.'">'.$this->Get_Text_Span($_role, 'chi').'</td>';
							$x .= '<td style="text-align:left;vertical-align:top;padding-bottom:1px;" class="border_left border_right '.$border_bottom.'">'.$this->Get_Text_Span($_hours, 'eng').'</td>';
						$x .= '</tr>'."\r\n";				
					}
					
					for ($i=0,$i_MAX=($MaxNumOfActivity-$numOfRow); $i<$i_MAX; $i++) 
					{			
						if($i==$i_MAX-1) {
							$border_bottom = 'border_bottom';
						}
						else {
							$border_bottom = '';
						}
						
						$x .= '<tr>'."\r\n";
							$x .= '<td style="text-align:left;vertical-align:top;padding-bottom:1px;" class="border_left '.$border_bottom.'">&nbsp;</td>';
							$x .= '<td style="text-align:left;vertical-align:top;padding-bottom:1px;" class="border_left '.$border_bottom.'">&nbsp;</td>';
							$x .= '<td style="text-align:left;vertical-align:top;padding-bottom:1px;" class="border_left '.$border_bottom.'">&nbsp;</td>';
							$x .= '<td style="text-align:left;vertical-align:top;padding-bottom:1px;" class="border_left '.$border_bottom.'">&nbsp;</td>';
							$x .= '<td style="text-align:left;vertical-align:top;padding-bottom:1px;" class="border_left border_right '.$border_bottom.'">&nbsp;</td>';
						$x .= '</tr>'."\r\n";	
					}
				$x .= '</table>'."\r\n";
			$x .='</td></tr>';
			
			//2014-0425-1430-36140
//			$x .= '<tr>'."\r\n";
//				$x .= '<td style="height:20px">&nbsp;</td>'."\r\n";
//			$x .= '</tr>'."\r\n";
			
			
			### Awards
//			$x .='<tr><td>';			
//				$x .= '<table class="font_11pt" border="0" cellspacing="0" cellpadding="5" style="width:100%; text-align:left;">'."\r\n";
//					$x .='<tr>';
//						$x .='<td><b><u>'.$eReportCard['Template']['AwardsTitleCh'].' <span class="font_eng">'.$eReportCard['Template']['AwardsTitleEn'].'</span></u></b></td>';
//					$x .='</tr>';
//				$x .= '</table>';
//			$x .='</td></tr>';
			$x .='<tr>';
				//2014-0425-1430-36140
				//$x .='<td class="font_11pt">';
				$x .='<td class="font_11pt" style="padding-top:10px;">';
					$x .='<b><u>'.$this->Get_Text_Span($eReportCard['Template']['AwardsTitleCh'], 'chi').' '.$this->Get_Text_Span($eReportCard['Template']['AwardsTitleEn'], 'eng').'</u></b>';
				$x .='</td>';		
			$x .='</tr>';
			
			$x .='<tr><td>';
				$x .= '<table class="font_10pt" border="0" cellspacing="0" cellpadding="5" style="width:100%; text-align:center; height:400px;">'."\r\n";
					$x .= '<col style="width:5%" />';
					$x .= '<col style="width:48%" />';
					$x .= '<col style="width:47%" />';
								
					$x .= '<tr>'."\r\n";
						$x .= '<td class="grey_bg border_bottom border_top border_left">'.'&nbsp;'.'</span></td>'."\r\n";
						$x .= '<td class="grey_bg border_bottom border_top border_left">'.$this->Get_Text_Span($eReportCard['Template']['AwardsCh'], 'chi').'<br />'.$this->Get_Text_Span($eReportCard['Template']['AwardsEn'], 'eng').'</td>'."\r\n";
						$x .= '<td class="grey_bg border_bottom border_top border_left border_right">'.$this->Get_Text_Span($eReportCard['Template']['OrganisersCh'], 'chi').'<br />'.$this->Get_Text_Span($eReportCard['Template']['OrganisersEn'], 'eng').'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
					
					$numOfRow = min($MaxNumOfAward, count($AwardListArr));
					for ($i=0; $i<$numOfRow; $i++) {
						
						if($i==($MaxNumOfAward-1)) {
							$border_bottom='border_bottom';
						}
						else {
							$border_bottom='';
						}
						
						$AwardName = ($AwardListArr[$i]['AwardName']=='')? '&nbsp;':$AwardListArr[$i]['AwardName'] ;
						$Organization = ($AwardListArr[$i]['Organization']=='')? '&nbsp;':$AwardListArr[$i]['Organization'] ;
						
						$x .= '<tr>'."\r\n";
							$x .= '<td style="text-align:left;vertical-align:top;padding-bottom:1px;" class="border_left '.$border_bottom.'">'.$this->Get_Text_Span(($i+1).'.', 'eng').'</td>';
							$x .= '<td style="text-align:left;vertical-align:top;padding-bottom:1px;" class="border_left '.$border_bottom.'">'.$this->Get_Text_Span($AwardName, 'chi').'</td>';
							$x .= '<td style="text-align:left;vertical-align:top;padding-bottom:1px;" class="border_left border_right '.$border_bottom.'">'.$this->Get_Text_Span($Organization, 'chi').'</td>';
						$x .= '</tr>'."\r\n";				
					}
					
					for ($i=0,$i_MAX=($MaxNumOfAward-$numOfRow); $i<$i_MAX; $i++) 
					{			
						if($i==$i_MAX-1)
						{
							$border_bottom='border_bottom';
						}
						else
						{
							$border_bottom='';
						}
						
						$x .= '<tr>'."\r\n";
							$x .= '<td style="text-align:left;vertical-align:top;padding-bottom:1px;" class="border_left '.$border_bottom.'">&nbsp;</td>';
							$x .= '<td style="text-align:left;vertical-align:top;padding-bottom:1px;" class="border_left '.$border_bottom.'">&nbsp;</td>';
							$x .= '<td style="text-align:left;vertical-align:top;padding-bottom:1px;" class="border_left border_right '.$border_bottom.'">&nbsp;</td>';
						$x .= '</tr>'."\r\n";	
					}
				$x .= '</table>'."\r\n";
			
			$x .='</td></tr>';
		
		$x .= '</table>';	
		
		return $x;
		
	}
	
	function Get_SELF_ACC_Table($ReportID, $StudentID='')
	{
		global $eReportCard, $intranet_root;
		
		include_once($intranet_root."/includes/portfolio25/libpf-slp.php");
		$libpf_self_acc = new libpf_self_account();
		$self_accounts_Arr = $libpf_self_acc->getSelfAccountsByUserIds($StudentID,$IsDefault=true);
		
		$Content = $self_accounts_Arr[$StudentID]['ELEMENT']['DETAILS'];
		$Contect = strip_tags($Content);
	//	debug_r($self_accounts_Arr);
		
		$x ='';
		$x .= '<table class="border_table font_11pt" cellpadding="5" align="center" style="width:100%; height:800px; text-align:center;">'."\r\n";
						
			$x .= '<tr>';
				$x .= '<td style="text-align:center;height:20px" class="grey_bg ">'.$this->Get_Text_Span($eReportCard['Template']['SelfAccCh'], 'chi').' '.$this->Get_Text_Span($eReportCard['Template']['SelfAccEn'], 'eng').'</td>';
			$x .= '</tr>';
			
			$x .= '<tr>';
				$x .= '<td style="text-align:left;vertical-align:top" class="">'.$this->Get_Text_Span($Content, 'chi').'</td>';
			$x .= '</tr>';
		$x .='</table>';
		
		return $x;
	}
	
	function getReportHeader($ReportID) {
		
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')	{
		
	}
	
	function getMSTable($ReportID, $StudentID='') {
		
	}
	
	function genMSTableColHeader($ReportID) {
		
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)	{
		
	}
	
	function getSignatureTable($ReportID='') {
 		
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array()) {
		
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())	{
		
	}
	
	function getMiscTable($ReportID, $StudentID='') {
		
	}
	
	########### END Template Related
	
	
//	public function Generate_Report_Award_Customized($ReportID) {
//		global $eRCTemplateSetting;
//		
//		### Get Report Data
//		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
//		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
//		$Semester = $ReportInfoArr['Semester'];
//		$ReportType = $this->Get_Award_Generate_Report_Type($ReportID);
//		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
//		
//		### Get Report Column Info
//		$ReportColumnInfoArr = $this->returnReportTemplateColumnData($ReportID, $other="");
//		$numOfReportColumn = count($ReportColumnInfoArr);
//				
//		### Get Form Students
//		$StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ParClassID="", $ParStudentID="", $ReturnAsso=0, $isShowStyle=0);
//		$numOfStudent = count($StudentInfoArr);
//		
//		### Get Subjects
//		$MainSubjectArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=false, $ReportID);
//		
//		### Get Subject Id-Code mapping
//		$SubjectIdCodeMappingAry = $this->GET_SUBJECTS_CODEID_MAP();
//		
//		### Get Subject Grading Scheme Info
//		$GradingSchemeInfoArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, '', $withGrandResult=0, $returnAsso=1, $ReportID);
//		
//		### Get Subject Subject Marks
//		// $MarksArr[$StudentID][$SubjectID][$ReportColumnID][DBField] = Value
//		$MarksArr = $this->getMarks($ReportID, $StudentID='', $cons='', $ParentSubjectOnly=1, $includeAdjustedMarks=0);
//		
//		### Get Subject Personal Characteristics
//		$SubjectPersonalCharInfoArr = $this->returnPersonalCharSettingData($ClassLevelID, $SubjectID='', $GroupByCharID=0);
//		$SubjectPersonalCharAssoArr = BuildMultiKeyAssoc($SubjectPersonalCharInfoArr, array('SubjectID', 'CharID'));
//		
//		### Get Student Subject Personal Characteristics
//		// $returnAry[$ReportID][$SubjectID][$StudentID][$CharID] = Value;
//		$StudentPersonalCharInfoArr = $this->getPersonalCharacteristicsProcessedDataByBatch($StudentID='', $ReportID, $SubjectID='', $ReturnScaleID=0, $ReturnCharID=1, $ReturnComment=0);
//		
//		### Get Related Award Config
//		$PersonalCharConfigArr = $this->Get_Award_Generate_Personal_Char_Grade_Config();
//				
//		$AwardStudentInfoArr = array();
//		if ($ReportType == 'T') {
//			foreach ((array)$MainSubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
//				$thisSubjectType = ($this->Is_Exam_Subject($ReportID, $thisSubjectID))? 'examSubject' : 'nonExamSubject';
//				$thisIsParentSubject = (count($thisSubjectInfoArr) > 1)? true : false;
//				
//				for ($i=0; $i<$numOfStudent; $i++) {
//					$thisStudentID = $StudentInfoArr[$i]['UserID'];
//					
//					// All Personal Characteristics are A or A*
//					$thisSatisfyPersonalChar = true;
//					if (count((array)$SubjectPersonalCharAssoArr[$thisSubjectID]) == 0) {
//						$thisSatisfyPersonalChar = false;
//					}
//					else {
//						foreach((array)$SubjectPersonalCharAssoArr[$thisSubjectID] as $thisCharID => $thisCharInfoArr) {
//							$thisStudentPersonalChar = $StudentPersonalCharInfoArr[$ReportID][$thisSubjectID][$thisStudentID][$thisCharID];
//							if (!in_array($thisStudentPersonalChar, (array)$PersonalCharConfigArr)) {
//								$thisSatisfyPersonalChar = false;
//								break;
//							}
//						}
//					}
//					if (!$thisSatisfyPersonalChar) {
//						continue;
//					}
//					
//					
//					// Get Award Ranking Determine Value
//					$thisScaleInput = $GradingSchemeInfoArr[$thisSubjectID]['ScaleInput'];
//					$thisMark = $MarksArr[$thisStudentID][$thisSubjectID][0]['Mark'];
//					$thisGrade = $MarksArr[$thisStudentID][$thisSubjectID][0]['Grade'];
//					$thisRankDetermineValue = '';
//					if ($thisScaleInput == 'G' || $this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
//						$thisRankDetermineValue = $thisGrade;
//					}
//					if ($thisScaleInput == 'M') {
//						$thisRankDetermineValue = $thisMark;
//					}
//					
//					
//					// If all Assessments have 5 or 5* or 5** => Excellent
//					$thisAwardCode = 'excellent';
//					$thisSatisfyExcellent = true;
//					for ($j=0; $j<$numOfReportColumn; $j++) {
//						$thisReportColumnID = $ReportColumnInfoArr[$j]['ReportColumnID'];
//						$thisReportColumnTitle = $ReportColumnInfoArr[$j]['ColumnTitle'];
//						$thisReportColumnInternalCode = $this->Get_Report_Column_Internal_Code($thisReportColumnTitle);
//						
//						$thisMark = $MarksArr[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Mark'];
//						$thisGrade = $MarksArr[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Grade'];
//						
//						//2013-0204-1054-07140
////						if ($thisIsParentSubject && $this->Is_SBA_Column($thisReportColumnTitle)) {
////							continue;
////						}
//						
//						if ($this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
//							//$thisSatisfyExcellent = false;
//							//break;
//							continue;
//						}
//						
//						if (!in_array($thisGrade, (array)$this->Get_AwardGradeArray_By_AwardCode($thisAwardCode, $thisSubjectType, $thisReportColumnInternalCode))) {
//							$thisSatisfyExcellent = false;
//							break;
//						}
//					}
//					
//					if ($thisSatisfyExcellent) {
//						$thisAwardID = $this->Get_AwardID_By_AwardCode($thisAwardCode);
//						
//						$thisStudentPersonalChar = implode('###', (array)$StudentPersonalCharInfoArr[$ReportID][$thisSubjectID][$thisStudentID]);
//						$thisDetermineText = $thisStudentPersonalChar.'|||'.$thisGrade;
//						$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['DetermineText'] = $thisDetermineText;
//						$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['DetermineValue'] = $thisRankDetermineValue;
//						
//						continue;
//					}
//					
//					
//					// For Exam Subject only, if all Aessessments have 4 or upper => Good
//					$thisAwardCode = 'good';
//					$thisSatisfyGood = true;
//					//if ($this->Is_Exam_Subject($ReportID, $thisSubjectID)) {
//						for ($j=0; $j<$numOfReportColumn; $j++) {
//							$thisReportColumnID = $ReportColumnInfoArr[$j]['ReportColumnID'];
//							$thisReportColumnTitle = $ReportColumnInfoArr[$j]['ColumnTitle'];
//							$thisReportColumnInternalCode = $this->Get_Report_Column_Internal_Code($thisReportColumnTitle);
//							
//							$thisMark = $MarksArr[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Mark'];
//							$thisGrade = $MarksArr[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Grade'];
//							
//							//2013-0204-1054-07140
////							if ($thisIsParentSubject && $this->Is_SBA_Column($thisReportColumnTitle)) {
////								continue;
////							}
//							
//							if ($this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
//								//$thisSatisfyGood = false;
//								//break;
//								continue;
//							}
//							
//							if (!in_array($thisGrade, (array)$this->Get_AwardGradeArray_By_AwardCode($thisAwardCode, $thisSubjectType, $thisReportColumnInternalCode))) {
//								$thisSatisfyGood = false;
//								break;
//							}
//						}
//					//}
//					//else {
//					//	$thisSatisfyGood = false;
//					//}
//					
//					if ($thisSatisfyGood) {
//						$thisAwardID = $this->Get_AwardID_By_AwardCode($thisAwardCode);
//						
//						$thisStudentPersonalChar = implode('###', (array)$StudentPersonalCharInfoArr[$ReportID][$thisSubjectID][$thisStudentID]);
//						$thisDetermineText = $thisStudentPersonalChar.'|||'.$thisGrade;
//						$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['DetermineText'] = $thisDetermineText;
//						$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['DetermineValue'] = $thisRankDetermineValue;
//						
//						continue;
//					}
//				}	// End loop Student
//			}	// End loop Subject
//		}
//		else if ($ReportType == 'W') {
//			$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
//			
//			// $GrandMarkArr[$StudentID][$ReportColumnID][DBField] = Value
//			$GrandMarkArr = $this->getReportResultScore($ReportID, $ReportColumnID=0, $StudentID='', $other_conds='', $debug=0, $includeAdjustedMarks=0, $CheckPositionDisplay=0, $FilterNoRanking=1, $OrderFieldArr='');
//			
//			### Scholarship Award Criteria Array
//			$ScholarshipRequiredAwardIDArr = array();
//			$ScholarshipRequiredAwardIDArr[] = $this->Get_AwardID_By_AwardCode('excellent');
//			$ScholarshipRequiredAwardIDArr[] = $this->Get_AwardID_By_AwardCode('good');
//			$numOfScholarshipRequiredAward = count((array)$ScholarshipRequiredAwardIDArr);
//			
//			### Get Term Reports
//			$TermReportInfoArr = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ReportColumnID="ALL");
//			$TermReportIDArr = Get_Array_By_Key($TermReportInfoArr, 'ReportID');
//			$numOfTermReport = count($TermReportInfoArr);
//			
//			
//			### Get Term Reports' Mark
//			// $ReportColumnInfoArr[$ReportID] = InfoArr
//			$ReportColumnInfoArr = array();
//			// $TermReportMarksheetArr[$ReportID][$ReportColumnID][$SubjectID][$StudentID] = InfoArr
//			$TermReportMarksheetArr = array();
//			// $TermReportMarkArr[$ReportID][$StudentID][$SubjectID][$ReportColumnID][DBField] = Value
//			$TermReportMarkArr = array();
//			// $TermReportAwardArr[$ReportID][$StudentID][$AwardID][$SubjectID][Field] = Value
//			$TermReportAwardArr = array();
//			
//			for ($i=0; $i<$numOfTermReport; $i++) {
//				$thisTermReportID = $TermReportInfoArr[$i]['ReportID'];
//				
//				# Get Term Report Mark
//				$TermReportMarkArr[$thisTermReportID] = $this->getMarks($thisTermReportID, '', $cons='', $ParentSubjectOnly=1, $includeAdjustedMarks=0);
//				
//				# Get Term Report Marksheet
//				$ReportColumnInfoArr[$thisTermReportID] = $this->returnReportTemplateColumnData($thisTermReportID);
//				$thisNumOfReportColumn = count($ReportColumnInfoArr[$thisTermReportID]);
//				for ($j=0; $j<$thisNumOfReportColumn; $j++) {
//					$thisReportColumnID = $ReportColumnInfoArr[$thisTermReportID][$j]['ReportColumnID'];
//					
//					foreach ((array)$MainSubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
//						$TermReportMarksheetArr[$thisTermReportID][$thisReportColumnID][$thisSubjectID] = $this->GET_MARKSHEET_SCORE($StudentIDArr, $thisSubjectID, $thisReportColumnID);
//					}
//				}
//				
//				# Get Term Report Award
//				$TermReportAwardArr[$thisTermReportID] = $this->Get_Award_Generated_Student_Record($thisTermReportID, $StudentIDArr, $ReturnAsso=1, $AwardNameWithSubject=1, $AwardIDArr='', $SubjectIDArr='');
//			}
//			
//			
//			### Get Student Subject Personal Characteristics
//			// $returnAry[$ReportID][$SubjectID][$StudentID][$CharID] = Value;
//			$StudentPersonalCharInfoArr = $this->getPersonalCharacteristicsProcessedDataByBatch($StudentIDArr, $TermReportIDArr, $SubjectID='', $ReturnScaleID=false, $ReturnCharID=true, $ReturnComment=false);
//			
//			for ($i=0; $i<$numOfStudent; $i++) {
//				$thisStudentID = $StudentInfoArr[$i]['UserID'];
//				
//				//$thisNumOfEstimatedScore = 0;
//				$thisSatisfyPersonalChar = ($numOfTermReport==0)? false : true;
//				$thisSatisfySubjectGrade = ($numOfTermReport==0)? false : true;
//				$thisSubjectAwardedReportIDArr = array();
//				
//				for ($j=0; $j<$numOfTermReport; $j++) {
//					$thisTermReportID = $TermReportInfoArr[$j]['ReportID'];
//					
//					$thisReportColumnInfoArr = $ReportColumnInfoArr[$thisTermReportID];
//					$thisNumOfReportColumn = count($thisReportColumnInfoArr);
//					
//					foreach ((array)$MainSubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
//						$thisGrade = $TermReportMarkArr[$thisTermReportID][$thisStudentID][$thisSubjectID][0]['Grade'];
//						
//						if ($this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
//							// If the student has not studied this Subject => ignore all checking
//							continue;
//						}
//						
//						# Check Estimated Score
////						for ($k=0; $k<$thisNumOfReportColumn; $k++) {
////							$thisReportColumnID = $thisReportColumnInfoArr[$k]['ReportColumnID'];
////						
////							$thisIsEstimatedScore = $TermReportMarksheetArr[$thisTermReportID][$thisReportColumnID][$thisSubjectID][$thisStudentID]['IsEstimated'];
////							if ($thisIsEstimatedScore) {
////								$thisNumOfEstimatedScore++;
////							}
////						}
//						
//						# Check Personal Characteristics
//						if (count((array)$SubjectPersonalCharAssoArr[$thisSubjectID]) == 0) {
//							$thisSatisfyPersonalChar = false;
//						}
//						else {
//							foreach((array)$SubjectPersonalCharAssoArr[$thisSubjectID] as $thisCharID => $thisCharInfoArr) {
//								$thisStudentPersonalChar = $StudentPersonalCharInfoArr[$thisTermReportID][$thisSubjectID][$thisStudentID][$thisCharID];
//								
//								if ($thisStudentPersonalChar=='' || !in_array($thisStudentPersonalChar, (array)$PersonalCharConfigArr)) {
//									$thisSatisfyPersonalChar = false;
//									break;
//								}
//							}
//						}
//						
//						# Check Subject Award
//						for ($k=0; $k<$numOfScholarshipRequiredAward; $k++) {
//							$thisAwardID = $ScholarshipRequiredAwardIDArr[$k];
//							
//							if (isset($TermReportAwardArr[$thisTermReportID][$thisStudentID][$thisAwardID][$thisSubjectID])) {
//								$thisSubjectAwardedReportIDArr[$thisSubjectID][] = $thisTermReportID;
//								break;
//							}
//						}
//					}
//				}	// End loop Term Report
//				
//				# Check Overall Subject Grade has "3" or above
//				foreach ((array)$MainSubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
//					$thisGrade = $MarksArr[$thisStudentID][$thisSubjectID][0]['Grade'];
//					
//					if ($thisGrade == '' || $this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
//						continue;
//					}
//					if (!in_array($thisGrade, (array)$this->Get_AwardGradeArray_By_AwardCode('annual_academic_result'))) {
//						$thisSatisfySubjectGrade = false;
//						break;
//					}
//				}
//				
//				
//				### 全年總成績
//				$thisAwardID = $this->Get_AwardID_By_AwardCode('annual_academic_result');
//				
//				if ($thisSatisfyPersonalChar && $thisSatisfySubjectGrade) {
//					// Record Satisfy Students
//					$thisSubjectID = 0;
//					$thisGrandTotal = $GrandMarkArr[$thisStudentID][0]['GrandTotal'];
//					
//					$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['DetermineValue'] = $thisGrandTotal;
//					$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['RankField'] = 'GrandTotal';
//					//$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['NumOfEstimatedScore'] = $thisNumOfEstimatedScore;
//				}
//				
//				### 各科獎學金
//				$thisAwardID = $this->Get_AwardID_By_AwardCode('annual_subject_scholarship');
//				foreach ((array)$MainSubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
//					$thisNumOfAwardedReport = count((array)$thisSubjectAwardedReportIDArr[$thisSubjectID]);
//					
//					//2012-0622-1619-01099
//					/*
//					 * 	在某些科的首三名名單中, 亦有另一個問題, eclass無法顯示S1,S2的VA及MU, S3的
//					 *	BI/PH/CM/GE, 因為這些科都只會上一個學期, 故同學無法上下學期均得獎.
//					 *	
//					 *	我的建議是, 上述的科目跟以下條件:
//					 *	次序一：以上學期或下學期計，先選該科學習態度及習慣等弟全A或以上
//					 *	次序二：按該科科目總分排龍
//					 *	次序三：上或下學期取得「良好」或「優異」獎
//					 *	次序四：找出第一、二、三名獎學金名單，但不須於備註顯示
//					 *
//					 *	Updated on 2012-09-03
//					 *	1)  獎學金計算
//					 *		-  S1,S2: Visual Arts(VA), Music(MU) (上下學期也有該兩科, 我們會set 40% and 60%)
//					 *		-  S3: Biology(BI) / Physics(PH) / Chemistry(CM) / Geography(GE) (只有一個學期有)
//					 */
//					$thisSubjectWebSAMSCode = $SubjectIdCodeMappingAry[$thisSubjectID];
//					$thisTargetNumOfAwardReport = $numOfTermReport;
//					$studyOneTermSubjectAry = array();
////					if ($FormNumber==1 || $FormNumber==2) {
////						$studyOneTermSubjectAry[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['VisualArt'];
////						$studyOneTermSubjectAry[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Music'];
////					}
////					else if ($FormNumber==3) {
//					if ($FormNumber==3) {
//						$studyOneTermSubjectAry[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Geog'];
//						$studyOneTermSubjectAry[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Bio'];
//						$studyOneTermSubjectAry[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Phy'];
//						$studyOneTermSubjectAry[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Chem'];
//					}
//					if (in_array($thisSubjectWebSAMSCode, $studyOneTermSubjectAry)) {
//						$thisTargetNumOfAwardReport = 1;
//					}
//					
//					if ($thisNumOfAwardedReport == $thisTargetNumOfAwardReport) {
//						// 上、下學期均取得「良好」或「優異」獎
//						$thisSubjectMark = $MarksArr[$thisStudentID][$thisSubjectID][0]['Mark'];
//						$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['DetermineValue'] = $thisSubjectMark;
//						$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['RankField'] = 'Mark';
//					}
//				}
//			}	// End loop Student
//		}	// End else if ($ReportType == 'W')
//		
//		
//		### Convert to Update function format and Save to DB
//		$SuccessArr['Delete_Old_Report_Award'] = $this->Delete_Award_Generated_Student_Record($ReportID);
//		foreach ((array)$AwardStudentInfoArr as $thisAwardID => $thisAwardStudentInfoArr) {
//			$thisAwardStudentInfoArr = $this->Sort_And_Add_AwardRank_In_InfoArr($thisAwardStudentInfoArr, 'DetermineValue', 'desc');
//			
//			$AwardUpdateDBInfoArr = array();
//			foreach ((array)$thisAwardStudentInfoArr as $thisSubjectID => $thisAwardSubjectInfoArr) {
//				foreach ((array)$thisAwardSubjectInfoArr as $thisStudentID => $thisAwardSubjectStudentInfoArr) {
//					$thisUpdateDBInfoArr = array();
//					$thisUpdateDBInfoArr = $thisAwardSubjectStudentInfoArr;
//					$thisUpdateDBInfoArr['SubjectID'] = $thisSubjectID;
//					$thisUpdateDBInfoArr['StudentID'] = $thisStudentID;
//					$AwardUpdateDBInfoArr[] = $thisUpdateDBInfoArr;
//					
//					unset($thisUpdateDBInfoArr);
//				}
//			}
//			
//			if (count((array)$AwardUpdateDBInfoArr) > 0) {
//				$SuccessArr['Update_Award_Generated_Student_Record'][$thisAwardID] = $this->Update_Award_Generated_Student_Record($thisAwardID, $ReportID, $AwardUpdateDBInfoArr);
//			}
//			unset($AwardUpdateDBInfoArr);
//		}
//		
//		
//		### Save the Award Text
//		$SuccessArr['Delete_Old_Report_Award_Text'] = $this->Delete_Award_Student_Record($ReportID);
//		//$SuccessArr['Generate_Report_Award_Text'] = $this->Generate_Report_Award_Text($ReportID);
//		
//		
//		### Update Last Generated Date 
//		$SuccessArr['Update_LastGeneratedAward_Date'] = $this->UPDATE_REPORT_LAST_DATE($ReportID, 'LastGeneratedAward');
//		
//		if (in_multi_array(false, $SuccessArr))
//		{
//			$this->RollBack_Trans();
//			return 0;
//		}
//		else
//		{
//			$this->Commit_Trans();
//			return 1;
//		}
//	}


	public function Generate_Report_Award_Customized($ReportID) {
		global $eRCTemplateSetting;
		
		### Get Report Data
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$Semester = $ReportInfoArr['Semester'];
		$SemesterSequence = $this->Get_Semester_Seq_Number($Semester);
		$ReportType = $this->Get_Award_Generate_Report_Type($ReportID);
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode==true);
		
		### Get Report Column Info
		$ReportColumnInfoArr = $this->returnReportTemplateColumnData($ReportID, $other="");
		$numOfReportColumn = count($ReportColumnInfoArr);
				
		### Get Form Students
		$StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ParClassID="", $ParStudentID="", $ReturnAsso=0, $isShowStyle=0);
		$numOfStudent = count($StudentInfoArr);
		
		### Get Subjects
		$MainSubjectArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=false, $ReportID);
		
		### Get Subject Id-Code mapping
		$SubjectIdCodeMappingAry = $this->GET_SUBJECTS_CODEID_MAP();
		
		### Get Subject Grading Scheme Info
		$GradingSchemeInfoArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, '', $withGrandResult=0, $returnAsso=1, $ReportID);
		
		### Get Subject Subject Marks
		// $MarksArr[$StudentID][$SubjectID][$ReportColumnID][DBField] = Value
		$MarksArr = $this->getMarks($ReportID, $StudentID='', $cons='', $ParentSubjectOnly=1, $includeAdjustedMarks=0);
		
		### Get Subject Column Weight
		$AllSubjectWeightInfoArr = $this->returnReportTemplateSubjectWeightData($ReportID, '', '', '', $convertEmptyToZero=true);
    	$AllSubjectWeightInfoAssoArr[$ReportID] = BuildMultiKeyAssoc($AllSubjectWeightInfoArr, array("ReportColumnID", "SubjectID"));
//		if ($ReportType == 'W') {
//			$AllSubjectWeightInfoArr = $this->returnReportTemplateSubjectWeightData($LastTermReportID, '', '', '', $convertEmptyToZero=true);
//    		$AllSubjectWeightInfoAssoArr[$LastTermReportID] = BuildMultiKeyAssoc($AllSubjectWeightInfoArr, array("ReportColumnID", "SubjectID"));
//		}
		
		### Get Subject Personal Characteristics
		$SubjectPersonalCharInfoArr = $this->returnPersonalCharSettingData($ClassLevelID, $SubjectID='', $GroupByCharID=0);
		$SubjectPersonalCharAssoArr = BuildMultiKeyAssoc($SubjectPersonalCharInfoArr, array('SubjectID', 'CharID'));
		
		### Get Student Subject Personal Characteristics
		// $returnAry[$ReportID][$SubjectID][$StudentID][$CharID] = Value;
		$StudentPersonalCharInfoArr = $this->getPersonalCharacteristicsProcessedDataByBatch($StudentID='', $ParReportID='', $SubjectID='', $ReturnScaleID=0, $ReturnCharID=1, $ReturnComment=0);
		
		### Get Related Award Config
		$PersonalCharConfigArr = $this->Get_Award_Generate_Personal_Char_Grade_Config();
		
		### Get SubjectID of M2
		$m2SubjectId = $this->Get_SubjectID_From_Config('M2');
		
		### Get exclude ordering student
		$excludeRankingStudentIdAry = $this->GET_EXCLUDE_ORDER_STUDENTS($ReportID);
		
		$AwardStudentInfoArr = array();
		if ($ReportType == 'T') {
			//2013-0605-1122-09140 - 粉嶺救恩書院 - eRC - Modification
			//eclass-requirement-11_130618.doc
			//新14.(2013/05/22)
			$personalCharReportIdAry = array();
			$termIdAry = array_keys($this->returnSemesters()); 
			if ($SemesterSequence == 2) {
				// 第二學期︰先選該科第一學期、學二學期的學習態度及習慣等第全A或以上
				$termOneTermId = $termIdAry[0];
				$termOneReportInfoAry = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $termOneTermId);
				$termOneReportId = $termOneReportInfoAry['ReportID'];
				
				$personalCharReportIdAry[] = $termOneReportId;
				$personalCharReportIdAry[] = $ReportID;
			}
			else if ($SemesterSequence == 3) {
				// 第三學期︰先選該科第三學期的學習態度及習慣等第全A或以上
				$personalCharReportIdAry[] = $ReportID;
			}
			$numOfPersonalCharReportId = count($personalCharReportIdAry);
			
			foreach ((array)$MainSubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
				$thisSubjectType = ($this->Is_Exam_Subject($ReportID, $thisSubjectID))? 'examSubject' : 'nonExamSubject';
				$thisIsParentSubject = (count($thisSubjectInfoArr) > 1)? true : false;
				
				for ($i=0; $i<$numOfStudent; $i++) {
					$thisStudentID = $StudentInfoArr[$i]['UserID'];
					
					// All Personal Characteristics are A or A*
					$thisSatisfyPersonalChar = true;
					if (count((array)$SubjectPersonalCharAssoArr[$thisSubjectID]) == 0) {
						$thisSatisfyPersonalChar = false;
					}
					else {
						foreach((array)$SubjectPersonalCharAssoArr[$thisSubjectID] as $thisCharID => $thisCharInfoArr) {
							if ($numOfPersonalCharReportId == 0) {
								$thisSatisfyPersonalChar = false;
								break;
							}
							else {
								for ($j=0; $j<$numOfPersonalCharReportId; $j++) {
									$____reportId = $personalCharReportIdAry[$j];
									
									//2014-0317-0911-47140: no need to input LA in Term 1 for non-exam subjects and M2 for Form 4
									//if (($thisSubjectID==$m2SubjectId || $thisSubjectType == 'nonExamSubject') && $j==0) {
									//2014-0317-1637-32140: M2 for Form 4 only
									if ($SemesterSequence == 2) {
										if (( ($FormNumber==4 && $thisSubjectID==$m2SubjectId) || $thisSubjectType == 'nonExamSubject') && $j==0) {
											continue;
										}
									}
									
									$____studentPersonalChar = $StudentPersonalCharInfoArr[$____reportId][$thisSubjectID][$thisStudentID][$thisCharID];
									if (!in_array($____studentPersonalChar, (array)$PersonalCharConfigArr)) {
										$thisSatisfyPersonalChar = false;
										break;
									}
								}
							}
						}
					}
					
					if (!$thisSatisfyPersonalChar) {
						continue;
					}
					
					
					// Get Award Ranking Determine Value
					$thisScaleInput = $GradingSchemeInfoArr[$thisSubjectID]['ScaleInput'];
					$thisMark = $MarksArr[$thisStudentID][$thisSubjectID][0]['Mark'];
					$thisGrade = $MarksArr[$thisStudentID][$thisSubjectID][0]['Grade'];
					$thisRankDetermineValue = '';
					if ($thisScaleInput == 'G' || $this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
						$thisRankDetermineValue = $thisGrade;
					}
					if ($thisScaleInput == 'M') {
						$thisRankDetermineValue = $thisMark;
					}
					
					// If all Assessments have 5 or 5* or 5** => Excellent
					$thisAwardCode = 'excellent';
					$thisSatisfyExcellent = true;
					$thisNumOfCountScore = 0;
					$thisNumOfSpecalCase = 0;
					for ($j=0; $j<$numOfReportColumn; $j++) {
						$thisReportColumnID = $ReportColumnInfoArr[$j]['ReportColumnID'];
						$thisReportColumnTitle = $ReportColumnInfoArr[$j]['ColumnTitle'];
						$thisReportColumnInternalCode = $this->Get_Report_Column_Internal_Code($thisReportColumnTitle);
						
						//2013-0605-1122-09140
						if ($thisSubjectType=='nonExamSubject' && $thisReportColumnInternalCode!='continuous_assessment') {
							// non-exam subject checks report column "continuous assessment" only now
							continue;
						}
						
						//2014-0317-0911-47140: M2 does not have 1st term exam for Form 4
						//if ($thisSubjectID==$m2SubjectId && $thisReportColumnInternalCode=='1st_term_exam') {
						//2014-0317-1637-32140: for Form 4 only
						if ($FormNumber==4 && $thisSubjectID==$m2SubjectId && $thisReportColumnInternalCode=='1st_term_exam') {
							continue;
						}
						
						$thisMark = $MarksArr[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Mark'];
						$thisGrade = $MarksArr[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Grade'];
						
						//2013-0204-1054-07140
//						if ($thisIsParentSubject && $this->Is_SBA_Column($thisReportColumnTitle)) {
//							continue;
//						}
						
						//2014-0520-1540-13140
//						if ($this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
//							//$thisSatisfyExcellent = false;
//							//break;
//							continue;
//						}

						$thisNumOfCountScore++;
						if ($this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
							//$thisSatisfyExcellent = false;
							//break;
							
							// [2016-0411-1003-22066] exclude special case (N.A.) if subject column weight is empty
							//$thisNumOfSpecalCase++;
							$currentSubjectColumnWeight = $AllSubjectWeightInfoAssoArr[$ReportID][$thisReportColumnID][$thisSubjectID]['Weight'];
							if($currentSubjectColumnWeight > 0)
							{
								$thisNumOfSpecalCase++;
							}
							continue;
						}
						
						if (!in_array($thisGrade, (array)$this->Get_AwardGradeArray_By_AwardCode($thisAwardCode, $thisSubjectType, $thisReportColumnInternalCode))) {
							$thisSatisfyExcellent = false;
							break;
						}
					}
					
					//2014-0520-1540-13140
//					if ($thisNumOfSpecalCase == $thisNumOfCountScore) {
					//2016-0408-1009-52206
					if ($thisNumOfSpecalCase > 0) {
						$thisSatisfyExcellent = false;
					}
					
					if ($thisSatisfyExcellent) {
						$thisAwardID = $this->Get_AwardID_By_AwardCode($thisAwardCode);
						
						$thisStudentPersonalChar = implode('###', (array)$StudentPersonalCharInfoArr[$ReportID][$thisSubjectID][$thisStudentID]);
						$thisDetermineText = $thisStudentPersonalChar.'|||'.$thisGrade;
						$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['DetermineText'] = $thisDetermineText;
						$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['DetermineValue'] = $thisRankDetermineValue;
						
						continue;
					}
					
					
					// For Exam Subject only, if all Aessessments have 4 or upper => Good
					$thisAwardCode = 'good';
					$thisSatisfyGood = true;
					$thisNumOfCountScore = 0;
					$thisNumOfSpecalCase = 0;
					//if ($this->Is_Exam_Subject($ReportID, $thisSubjectID)) {
						for ($j=0; $j<$numOfReportColumn; $j++) {
							$thisReportColumnID = $ReportColumnInfoArr[$j]['ReportColumnID'];
							$thisReportColumnTitle = $ReportColumnInfoArr[$j]['ColumnTitle'];
							$thisReportColumnInternalCode = $this->Get_Report_Column_Internal_Code($thisReportColumnTitle);
							
							//2013-0605-1122-09140
							if ($thisSubjectType=='nonExamSubject' && $thisReportColumnInternalCode!='continuous_assessment') {
								// non-exam subject checks report column "continuous assessment" only now
								continue;
							}
							
							//2014-0317-0911-47140: M2 does not have 1st term exam
							//if ($thisSubjectID==$m2SubjectId && $thisReportColumnInternalCode=='1st_term_exam') {
							//2014-0317-1637-32140: for Form 4 only
							if ($FormNumber==4 && $thisSubjectID==$m2SubjectId && $thisReportColumnInternalCode=='1st_term_exam') {
								continue;
							}
							
							$thisMark = $MarksArr[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Mark'];
							$thisGrade = $MarksArr[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Grade'];
							
							//2013-0204-1054-07140
//							if ($thisIsParentSubject && $this->Is_SBA_Column($thisReportColumnTitle)) {
//								continue;
//							}
							
							//2014-0520-1540-13140
//							if ($this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
//								//$thisSatisfyGood = false;
//								//break;
//								continue;
//							}
							$thisNumOfCountScore++;
							if ($this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
								//$thisSatisfyGood = false;
								//break;
								
								// [2016-0411-1003-22066] exclude special case (N.A.) if subject column weight is empty
								//$thisNumOfSpecalCase++;
								$currentSubjectColumnWeight = $AllSubjectWeightInfoAssoArr[$ReportID][$thisReportColumnID][$thisSubjectID]['Weight'];
								if($currentSubjectColumnWeight > 0)
								{
									$thisNumOfSpecalCase++;
								}
								continue;
							}
							
							if (!in_array($thisGrade, (array)$this->Get_AwardGradeArray_By_AwardCode($thisAwardCode, $thisSubjectType, $thisReportColumnInternalCode))) {
								$thisSatisfyGood = false;
								break;
							}
						}
					//}
					//else {
					//	$thisSatisfyGood = false;
					//}
					
					//2014-0520-1540-13140
//					if ($thisNumOfSpecalCase == $thisNumOfCountScore) {
					//2016-0408-1009-52206
					if ($thisNumOfSpecalCase > 0) {
						$thisSatisfyGood = false;
					}
					
					if ($thisSatisfyGood) {
						$thisAwardID = $this->Get_AwardID_By_AwardCode($thisAwardCode);
						
						$thisStudentPersonalChar = implode('###', (array)$StudentPersonalCharInfoArr[$ReportID][$thisSubjectID][$thisStudentID]);
						$thisDetermineText = $thisStudentPersonalChar.'|||'.$thisGrade;
						$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['DetermineText'] = $thisDetermineText;
						$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['DetermineValue'] = $thisRankDetermineValue;
						
						continue;
					}
				}	// End loop Student
			}	// End loop Subject
		}
		else if ($ReportType == 'W') {
			$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
			
			// $GrandMarkArr[$StudentID][$ReportColumnID][DBField] = Value
			$GrandMarkArr = $this->getReportResultScore($ReportID, $ReportColumnID=0, $StudentID='', $other_conds='', $debug=0, $includeAdjustedMarks=0, $CheckPositionDisplay=0, $FilterNoRanking=1, $OrderFieldArr='');
			
			### Scholarship Award Criteria Array
			$ScholarshipRequiredAwardIDArr = array();
			$ScholarshipRequiredAwardIDArr[] = $this->Get_AwardID_By_AwardCode('excellent');
			$ScholarshipRequiredAwardIDArr[] = $this->Get_AwardID_By_AwardCode('good');
			$numOfScholarshipRequiredAward = count((array)$ScholarshipRequiredAwardIDArr);
			
			### Get Term Reports
			$TermReportInfoArr = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ReportColumnID="ALL");
			$TermReportIDArr = Get_Array_By_Key($TermReportInfoArr, 'ReportID');
			$numOfTermReport = count($TermReportInfoArr);
			
			$personalCharReportIdAry = array();
			$termIdAry = array_keys($this->returnSemesters()); 
			// 第二學期︰先選該科第一學期、學二學期的學習態度及習慣等第全A或以上
			$termOneTermId = $termIdAry[0];
			$termOneReportInfoAry = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $termOneTermId);
			$termOneReportId = $termOneReportInfoAry['ReportID'];
			$personalCharReportIdAry[] = $termOneReportId;
			for ($i=0; $i<$numOfTermReport; $i++) {
				$_termReportId = $TermReportIDArr[$i];
				$personalCharReportIdAry[] = $_termReportId; 
			}
			$numOfPersonalCharReportId = count((array)$personalCharReportIdAry);
			
			
			### Get Term Reports' Mark
			// $ReportColumnInfoArr[$ReportID] = InfoArr
			$ReportColumnInfoArr = array();
			// $TermReportMarksheetArr[$ReportID][$ReportColumnID][$SubjectID][$StudentID] = InfoArr
			$TermReportMarksheetArr = array();
			// $TermReportMarkArr[$ReportID][$StudentID][$SubjectID][$ReportColumnID][DBField] = Value
			$TermReportMarkArr = array();
			// $TermReportAwardArr[$ReportID][$StudentID][$AwardID][$SubjectID][Field] = Value
			$TermReportAwardArr = array();
			
			//for ($i=0; $i<$numOfTermReport; $i++) {
			for ($i=0; $i<$numOfPersonalCharReportId; $i++) {
				//$thisTermReportID = $TermReportInfoArr[$i]['ReportID'];
				$thisTermReportID = $personalCharReportIdAry[$i];
				
				# Get Term Report Mark
				$TermReportMarkArr[$thisTermReportID] = $this->getMarks($thisTermReportID, '', $cons='', $ParentSubjectOnly=1, $includeAdjustedMarks=0);
				
				# Get Term Report Marksheet
				$ReportColumnInfoArr[$thisTermReportID] = $this->returnReportTemplateColumnData($thisTermReportID);
				$thisNumOfReportColumn = count($ReportColumnInfoArr[$thisTermReportID]);
				for ($j=0; $j<$thisNumOfReportColumn; $j++) {
					$thisReportColumnID = $ReportColumnInfoArr[$thisTermReportID][$j]['ReportColumnID'];
					
					foreach ((array)$MainSubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
						$TermReportMarksheetArr[$thisTermReportID][$thisReportColumnID][$thisSubjectID] = $this->GET_MARKSHEET_SCORE($StudentIDArr, $thisSubjectID, $thisReportColumnID);
					}
				}
				
				# Get Term Report Award
				$TermReportAwardArr[$thisTermReportID] = $this->Get_Award_Generated_Student_Record($thisTermReportID, $StudentIDArr, $ReturnAsso=1, $AwardNameWithSubject=1, $AwardIDArr='', $SubjectIDArr='');
			}
			
			
			### Get Student Subject Personal Characteristics
			// $returnAry[$ReportID][$SubjectID][$StudentID][$CharID] = Value;
			//$StudentPersonalCharInfoArr = $this->getPersonalCharacteristicsProcessedDataByBatch($StudentIDArr, $TermReportIDArr, $SubjectID='', $ReturnScaleID=false, $ReturnCharID=true, $ReturnComment=false);
			$StudentPersonalCharInfoArr = $this->getPersonalCharacteristicsProcessedDataByBatch($StudentIDArr, $personalCharReportIdAry, $SubjectID='', $ReturnScaleID=false, $ReturnCharID=true, $ReturnComment=false);
			
			for ($i=0; $i<$numOfStudent; $i++) {
				$thisStudentID = $StudentInfoArr[$i]['UserID'];
				
				if (in_array($thisStudentID, (array)$excludeRankingStudentIdAry)) {
					continue;
				}
				
				//$thisNumOfEstimatedScore = 0;
				$thisSatisfyPersonalChar = ($numOfPersonalCharReportId==0)? false : true;
				$thisSatisfySubjectGrade = ($numOfTermReport==0)? false : true;
				$thisSubjectAwardedReportIDArr = array();
				
				foreach ((array)$MainSubjectArr as $__subjectID => $__subjectInfoArr) {
					if (count((array)$SubjectPersonalCharAssoArr[$__subjectID]) == 0) {
						$thisSatisfyPersonalChar = false;
					}
					else {
						foreach((array)$SubjectPersonalCharAssoArr[$__subjectID] as $___charID => $___charInfoArr) {
							if ($numOfPersonalCharReportId == 0) {
								$thisSatisfyPersonalChar = false;
								break;
							}
							else {
								for ($j=0; $j<$numOfPersonalCharReportId; $j++) {
									$____reportId = $personalCharReportIdAry[$j];
									
									//2013-0605-1122-09140
									//新次序二：	考試科目︰考慮三個學期的學習態度及習慣等第全A或以上
									//非考試科目︰只考慮第二、第三學期的學習態度及習慣等第全A或以上
									$____isExamSubject = $this->Is_Exam_Subject($____reportId, $__subjectID);
									if (!$____isExamSubject && $j==0) {
										continue;
									}
									
									$____grade = $TermReportMarkArr[$____reportId][$thisStudentID][$__subjectID][0]['Grade'];
									if ($this->Check_If_Grade_Is_SpecialCase($____grade)) {
										// If the student has not studied this Subject => ignore all checking
										continue;
									}
									
									$____studentPersonalChar = $StudentPersonalCharInfoArr[$____reportId][$__subjectID][$thisStudentID][$___charID];
									if ($____studentPersonalChar=='' || !in_array($____studentPersonalChar, (array)$PersonalCharConfigArr)) {
										$thisSatisfyPersonalChar = false;
										break;
									}
								}
							}
						}
					}
				}
				
				for ($j=0; $j<$numOfTermReport; $j++) {
					$thisTermReportID = $TermReportInfoArr[$j]['ReportID'];
					
					$thisReportColumnInfoArr = $ReportColumnInfoArr[$thisTermReportID];
					$thisNumOfReportColumn = count($thisReportColumnInfoArr);
					
					foreach ((array)$MainSubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
						$thisGrade = $TermReportMarkArr[$thisTermReportID][$thisStudentID][$thisSubjectID][0]['Grade'];
						
						if ($this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
							// If the student has not studied this Subject => ignore all checking
							continue;
						}
						
						# Check Estimated Score
//						for ($k=0; $k<$thisNumOfReportColumn; $k++) {
//							$thisReportColumnID = $thisReportColumnInfoArr[$k]['ReportColumnID'];
//						
//							$thisIsEstimatedScore = $TermReportMarksheetArr[$thisTermReportID][$thisReportColumnID][$thisSubjectID][$thisStudentID]['IsEstimated'];
//							if ($thisIsEstimatedScore) {
//								$thisNumOfEstimatedScore++;
//							}
//						}
						
						# Check Personal Characteristics
//						if (count((array)$SubjectPersonalCharAssoArr[$thisSubjectID]) == 0) {
//							$thisSatisfyPersonalChar = false;
//						}
//						else {
//							foreach((array)$SubjectPersonalCharAssoArr[$thisSubjectID] as $thisCharID => $thisCharInfoArr) {
//								$thisStudentPersonalChar = $StudentPersonalCharInfoArr[$thisTermReportID][$thisSubjectID][$thisStudentID][$thisCharID];
//								
//								if ($thisStudentPersonalChar=='' || !in_array($thisStudentPersonalChar, (array)$PersonalCharConfigArr)) {
//									$thisSatisfyPersonalChar = false;
//									break;
//								}
//							}
//						}
						
						# Check Subject Award
						for ($k=0; $k<$numOfScholarshipRequiredAward; $k++) {
							$thisAwardID = $ScholarshipRequiredAwardIDArr[$k];
							
							if (isset($TermReportAwardArr[$thisTermReportID][$thisStudentID][$thisAwardID][$thisSubjectID])) {
								$thisSubjectAwardedReportIDArr[$thisSubjectID][] = $thisTermReportID;
								break;
							}
						}
					}
				}	// End loop Term Report
				
				# Check Overall Subject Grade has "3" or above
				foreach ((array)$MainSubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
					$thisGrade = $MarksArr[$thisStudentID][$thisSubjectID][0]['Grade'];
					
					if ($thisGrade == '' || $this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
						continue;
					}
					if (!in_array($thisGrade, (array)$this->Get_AwardGradeArray_By_AwardCode('annual_academic_result'))) {
						$thisSatisfySubjectGrade = false;
						break;
					}
				}
				
				
				### 全年總成績
				$thisAwardID = $this->Get_AwardID_By_AwardCode('annual_academic_result');
				if ($thisSatisfyPersonalChar && $thisSatisfySubjectGrade) {
					// Record Satisfy Students
					$thisSubjectID = 0;
					$thisGrandTotal = $GrandMarkArr[$thisStudentID][0]['GrandTotal'];
					
					$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['DetermineValue'] = $thisGrandTotal;
					$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['RankField'] = 'GrandTotal';
					//$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['NumOfEstimatedScore'] = $thisNumOfEstimatedScore;
				}
				
				### 各科獎學金
				$thisAwardID = $this->Get_AwardID_By_AwardCode('annual_subject_scholarship');
				foreach ((array)$MainSubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
					$thisNumOfAwardedReport = count((array)$thisSubjectAwardedReportIDArr[$thisSubjectID]);
					
					//2012-0622-1619-01099
					/*
					 * 	在某些科的首三名名單中, 亦有另一個問題, eclass無法顯示S1,S2的VA及MU, S3的
					 *	BI/PH/CM/GE, 因為這些科都只會上一個學期, 故同學無法上下學期均得獎.
					 *	
					 *	我的建議是, 上述的科目跟以下條件:
					 *	次序一：以上學期或下學期計，先選該科學習態度及習慣等弟全A或以上
					 *	次序二：按該科科目總分排龍
					 *	次序三：上或下學期取得「良好」或「優異」獎
					 *	次序四：找出第一、二、三名獎學金名單，但不須於備註顯示
					 *
					 *	Updated on 2012-09-03
					 *	1)  獎學金計算
					 *		-  S1,S2: Visual Arts(VA), Music(MU) (上下學期也有該兩科, 我們會set 40% and 60%)
					 *		-  S3: Biology(BI) / Physics(PH) / Chemistry(CM) / Geography(GE) (只有一個學期有)
					 */
					 
					//2013-0605-1122-09140
					//新次序一：	考試科目︰先選該科三個學期的學習態度及習慣等第全A或以上
	    			//			非考試科目︰先選該科第二、第三學期的學習態度及習慣等第全A或以上
					//次序二：按該科科目全年總分排龍
					$__satisfyPersonalChar = true;
					if (count((array)$SubjectPersonalCharAssoArr[$thisSubjectID]) == 0) {
						$__satisfyPersonalChar = false;
					}
					else {
						foreach((array)$SubjectPersonalCharAssoArr[$thisSubjectID] as $___charID => $___charInfoArr) {
							if ($numOfPersonalCharReportId == 0) {
								$__satisfyPersonalChar = false;
								break;
							}
							else {
								for ($j=0; $j<$numOfPersonalCharReportId; $j++) {
									$____reportId = $personalCharReportIdAry[$j];
									
									$____isExamSubject = $this->Is_Exam_Subject($____reportId, $thisSubjectID);
									if (!$____isExamSubject && $j==0) {
										continue;
									}
									
									$____studentPersonalChar = $StudentPersonalCharInfoArr[$____reportId][$thisSubjectID][$thisStudentID][$___charID];
									if ($____studentPersonalChar=='' || !in_array($____studentPersonalChar, (array)$PersonalCharConfigArr)) {
										$__satisfyPersonalChar = false;
										break;
									}
								}
							}
						}
					}
					 
					
					//2013-0605-1122-09140
					//新次序三：第二學期、第三學期均取得「良好」或「優異」獎
					$thisSubjectWebSAMSCode = $SubjectIdCodeMappingAry[$thisSubjectID];
					$thisTargetNumOfAwardReport = $numOfTermReport;
					$studyOneTermSubjectAry = array();
//					if ($FormNumber==1 || $FormNumber==2) {
//						$studyOneTermSubjectAry[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['VisualArt'];
//						$studyOneTermSubjectAry[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Music'];
//					}
//					else if ($FormNumber==3) {

					//2014-0620-0920-16140
//					if ($FormNumber==3) {
//						$studyOneTermSubjectAry[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Geog'];
//						$studyOneTermSubjectAry[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Bio'];
//						$studyOneTermSubjectAry[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Phy'];
//						$studyOneTermSubjectAry[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Chem'];
//					}
//					if (in_array($thisSubjectWebSAMSCode, $studyOneTermSubjectAry)) {
//						$thisTargetNumOfAwardReport = 1;
//					}
					
					if ($__satisfyPersonalChar && $thisNumOfAwardedReport == $thisTargetNumOfAwardReport) {
						// 上、下學期均取得「良好」或「優異」獎
						$thisSubjectMark = $MarksArr[$thisStudentID][$thisSubjectID][0]['Mark'];
						$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['DetermineValue'] = $thisSubjectMark;
						$AwardStudentInfoArr[$thisAwardID][$thisSubjectID][$thisStudentID]['RankField'] = 'Mark';
					}
				}
			}	// End loop Student
		}	// End else if ($ReportType == 'W')
		
		
		### Convert to Update function format and Save to DB
		$SuccessArr['Delete_Old_Report_Award'] = $this->Delete_Award_Generated_Student_Record($ReportID);
		foreach ((array)$AwardStudentInfoArr as $thisAwardID => $thisAwardStudentInfoArr) {
			$thisAwardStudentInfoArr = $this->Sort_And_Add_AwardRank_In_InfoArr($thisAwardStudentInfoArr, 'DetermineValue', 'desc');
			
			$AwardUpdateDBInfoArr = array();
			foreach ((array)$thisAwardStudentInfoArr as $thisSubjectID => $thisAwardSubjectInfoArr) {
				foreach ((array)$thisAwardSubjectInfoArr as $thisStudentID => $thisAwardSubjectStudentInfoArr) {
					$thisUpdateDBInfoArr = array();
					$thisUpdateDBInfoArr = $thisAwardSubjectStudentInfoArr;
					$thisUpdateDBInfoArr['SubjectID'] = $thisSubjectID;
					$thisUpdateDBInfoArr['StudentID'] = $thisStudentID;
					$AwardUpdateDBInfoArr[] = $thisUpdateDBInfoArr;
					
					unset($thisUpdateDBInfoArr);
				}
			}
			
			if (count((array)$AwardUpdateDBInfoArr) > 0) {
				$SuccessArr['Update_Award_Generated_Student_Record'][$thisAwardID] = $this->Update_Award_Generated_Student_Record($thisAwardID, $ReportID, $AwardUpdateDBInfoArr);
			}
			unset($AwardUpdateDBInfoArr);
		}
		
		
		### Save the Award Text
		$SuccessArr['Delete_Old_Report_Award_Text'] = $this->Delete_Award_Student_Record($ReportID);
		//$SuccessArr['Generate_Report_Award_Text'] = $this->Generate_Report_Award_Text($ReportID);
		
		
		### Update Last Generated Date 
		$SuccessArr['Update_LastGeneratedAward_Date'] = $this->UPDATE_REPORT_LAST_DATE($ReportID, 'LastGeneratedAward');
		
		if (in_multi_array(false, $SuccessArr))
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$this->Commit_Trans();
			return 1;
		}
	}
	
	private function Get_Award_Generate_Personal_Char_Grade_Config() {
		global $eRCTemplateSetting;
		return $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['PersonalCharArr'];
	}
	
	private function Get_Award_Generate_Award_Grade_Config() {
		global $eRCTemplateSetting;
		return $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['AwardArr'];
	}
	
	private function Is_Exam_Subject($ReportID, $SubjectID) {
		// If the Subject Weight of the last Report Column is zero => NOT an Exam Subject
		
		### Get Report Column
		$ReportColumnInfoArr = $this->returnReportTemplateColumnData($ReportID, $other="");
		$numOfReportColumn = count($ReportColumnInfoArr);
		$LastReportColumnID = $ReportColumnInfoArr[$numOfReportColumn-1]['ReportColumnID'];
		
		### Get Subject Weight Info
		$AllSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID);
		$SubjectWeightAssoArr = BuildMultiKeyAssoc($AllSubjectWeightData, array("ReportColumnID", "SubjectID"));
		
		$LastColumnWeight = $SubjectWeightAssoArr[$LastReportColumnID][$SubjectID]['Weight'];
		if ($LastColumnWeight == '' || $LastColumnWeight == 0) {
			$IsExamSubject = false;
		}
		else {
			$IsExamSubject = true;
		}
		
		return $IsExamSubject;
	}
	
	private function Get_AwardID_By_AwardCode($AwardCode) {
		global $eRCTemplateSetting;
		return $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['AwardArr'][$AwardCode]['AwardID'];
	}
	
	private function Get_AwardGradeArray_By_AwardCode($AwardCode, $SubjectType='', $ReportColumnInternalCode='') {
		global $eRCTemplateSetting;
		
		if ($SubjectType == '') {
			return $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['AwardArr'][$AwardCode]['GradeArr'];
		}
		else if ($SubjectType != '' && $ReportColumnInternalCode != '') {
			return $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['AwardArr'][$AwardCode]['GradeArr'][$SubjectType][$ReportColumnInternalCode];
		}
	}
	
	private function Get_Text_Span($text, $parLang, $parExtraClass='') {
		$langClass = '';
		switch ($parLang) {
			case "chi":
				$langClass = 'font_chi';
				break;
			case "eng":
				$langClass = 'font_eng';
				break;
		}
		
		return '<span class="'.$langClass.' '.$parExtraClass.'">'.$text.'</span>';
	}
	
	private function Get_Sba_Subject_Mapping() {
		global $eRCTemplateSetting;
		return $eRCTemplateSetting['Report']['SbaSubjectMappingArr'];
	}
	
	private function Is_SBA_Column($ReportColumnTitle) {
		//return (strtoupper(substr(trim($ReportColumnTitle), -3, 3)) == 'SBA')? true : false;
		return ($this->Get_Report_Column_Internal_Code($ReportColumnTitle) == 'sba')? true : false;
	}
	
	private function Get_Report_Column_Internal_Code($ReportColumnTitle) {
		return str_replace(" ", "_", strtolower(trim(substr_lang($ReportColumnTitle, $Language="ENG"))));
	}
	
	
	private function Get_Term_Elective_Subject($FormNumber, $StudentID = '') {
		$SubjectCodeToIDAssoArr = $this->GET_SUBJECTS_CODEID_MAP($withComponent=0, $mapByCode=1);
		
		$SubjectIDArr = array();
		if ($FormNumber == 1 || $FormNumber == 2)
		{
		    // [2018-0329-0956-45207] exclude specific student from Visual Art
		    if($StudentID == 6165) {
		        // do nothing
		    }
            else {
                $SubjectIDArr[] = $SubjectCodeToIDAssoArr[$this->Get_Subject_WebSAMSCode_From_Config('VisualArt')];
            }
			$SubjectIDArr[] = $SubjectCodeToIDAssoArr[$this->Get_Subject_WebSAMSCode_From_Config('Music')];
		}
		else if ($FormNumber == 3)
		{
		    // [2018-1210-1601-09066] exclude specific student from the hardcode display
		    if($StudentID == 6165) {
		        // do nothing
		    }
		    else {
    			$SubjectIDArr[] = $SubjectCodeToIDAssoArr[$this->Get_Subject_WebSAMSCode_From_Config('Geog')];
    			$SubjectIDArr[] = $SubjectCodeToIDAssoArr[$this->Get_Subject_WebSAMSCode_From_Config('Bio')];
    			$SubjectIDArr[] = $SubjectCodeToIDAssoArr[$this->Get_Subject_WebSAMSCode_From_Config('Phy')];
    			$SubjectIDArr[] = $SubjectCodeToIDAssoArr[$this->Get_Subject_WebSAMSCode_From_Config('Chem')];
		    }
		}
		
		return $SubjectIDArr;
		
		// for dev
		//return array(11);
	}
}
?>