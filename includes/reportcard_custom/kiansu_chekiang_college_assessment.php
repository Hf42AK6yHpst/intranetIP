<?php
# Editing by 

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/kiansu_chekiang_college.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		//$this->configFilesType = array("summary","attendance", "merit", "remark", "eca");
		$this->configFilesType = array("summary", "attendance", "merit", "remark", "assessment");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "---";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard;
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' style='padding:0; margin:0;'>";
		$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td valign=middle align=center style='padding:0; margin:0;' height='1068px' width='755px'>";
			$x .= "<table height='1009px' width='755px' border='0' cellspacing='0' cellpadding='0' align='center'>";
				$x .= "<tr><td valign='top'>";
			//$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' style='margin-top:80px; margin-bottom:85px'>";
					$x .= "<table height='997px' width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
						$x .= "<tr valign='top'><td>".$TableTop."</td></tr>";
						$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
					$x .= "</table>";
				$x .= "</td></tr>";	
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			//$ReportTitle =  $ReportSetting['ReportTitle'];
			//$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
			# get school badge
			//$SchoolLogo = GET_SCHOOL_BADGE();
				
			# get school name
			//$SchoolName = GET_SCHOOL_NAME();	
			
			
			$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
	
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='120' align='center' valign=top>".$TempLogo."</td>";

			//if(!empty($ReportTitle) || !empty($SchoolName))
			//{
				$TitleTable .= "<td valign=top>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' >\n";
					$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center' style='padding-bottom:8'>".$eReportCard['Template']['SchoolNameCh']."</td></tr>\n";
					$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center' style='padding-bottom:8'>".$eReportCard['Template']['SchoolNameEn']."</td></tr>\n";
					//if(!empty($ReportTitle))
					//	$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle."</td></tr>\n";
					
						for($i=1;$i<=3;$i++)
						{
							if(trim($_POST["title".$i])=='') 
								break;
							$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center' style='padding-bottom:4'>";
								$TitleTable .= $_POST["title".$i];
							$TitleTable .= "</td></tr>\n";
						}
					//$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$_POST["title2"]."</td></tr>\n";
					
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			//}
			$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['AssessmentReport']['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			//$LineHeight = $ReportSetting['LineHeight'];
			$LineHeight = $eRCTemplateSetting['AssessmentReport']['StudentInfo']['LineHeight'];
			
			# retrieve required variables
			$defaultVal = ($StudentID=='')? "XXX" : '';
			//$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			$ObjYear = new academic_year($this->schoolYearID);
			$data['AcademicYear'] = $ObjYear->Get_Academic_Year_Name();
			
			$data['DateOfIssue'] = $ReportSetting['IssueDate'];
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] = $lu->EnglishName." ( $lu->ChineseName )";
								
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				
				$data['ClassNo'] = $thisClassNumber;
				$data['Class'] = $thisClassName." ($thisClassNumber)";
				
				$data['StudentNo'] = str_replace("#","",$lu->WebSamsRegNo);
				$data['STRN'] = $lu->STRN;
				
				
				if (is_array($SettingStudentInfo))
				{
					if (!in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo) && ($thisClassNumber != ""))
						$data['Class'] .= " (".$thisClassNumber.")";
				}
				else
				{
					if ($thisClassNumber != "")
						$data['Class'] .= " (".$thisClassNumber.")";
				}
				
				$data['DateOfIssue'] = $_POST["IssueDate"];
				
				$data['Gender'] = $lu->Gender;
			}
			
			$SettingStudentInfo = array('Name', 'Class', 'StudentNo','STRN','Gender','DateOfIssue');
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				$StudentInfoTable .= "<br><br><table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
				for($i=0; $i<sizeof($SettingStudentInfo); $i++)
				{
					$SettingID = trim($SettingStudentInfo[$i]);
					if(in_array($SettingID, $SettingStudentInfo)===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
							$StudentInfoTable .= "<td class='tabletext' $colspan width='20%' valign='top' height='{$LineHeight}' nowrap>".$Title." : ";
							$StudentInfoTable .= ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";	
						}
						else
						{
							$StudentInfoTable .= "<td class='tabletext' align='right' width='15%' valign='top' height='{$LineHeight}' nowrap>".$Title." :&nbsp;</td>";
							$StudentInfoTable .= "<td class='tabletext' align='left' width='10%' valign='top' height='{$LineHeight}' nowrap>".($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
						}
						
							
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						} 
						
						$count++;
					}
				}
				$StudentInfoTable .= "</table>";
			}
		}
		return $StudentInfoTable;
	}
	
	function getAssessmentMSTable($ReportID, $Assessment, $Display, $StudentID, $StudentRankScoreAry)
	{
		global $eRCTemplateSetting,$eReportCard ;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		//$LineHeight = $ReportSetting['LineHeight'];
		$LineHeight = $eRCTemplateSetting['AssessmentReport']['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		### Get Subject List to be displayed
		$SubjectIDArr = (array)$Display;
		$numOfSubject = count($SubjectIDArr);
		
		### Get Subject Column
		$SubjectCol_HTML_Arr = $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		
		$html = '';
		$html .= "<table width='100%' align='center' border='0' cellspacing='0' cellpadding='4' class='report_border lite_grey_bg' style='margin-top:10px'>";
			/*$html .= "<tr>";
				$html .= "<td align='center' rowspan='2' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['Subject']."'>".$eReportCard['Template']['SubjectEn']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				$html .= $eReportCard['Template']['SubjectCh']."</td>";
				$html .= "<td align='center' class='border_left ' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['FullMark']."'>".$eReportCard['Template']['FullMarkEn']."</td>";
				$html .= "<td align='center' class='border_left' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['Mark']."'>".$eReportCard['Template']['TestMarkEn']."</td>";
				$html .= "<td align='center' class='border_left' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['FormPosition']."'>".$eReportCard['Template']['FormPositionEn']."</td>";
			$html .= "</tr>";
			$html .= "<tr>";
				$html .= "<td align='center' class='border_left' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['FullMark']."'>".$eReportCard['Template']['FullMarkCh']."</td>";
				$html .= "<td align='center' class='border_left' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['Mark']."'>".$eReportCard['Template']['TestMarkCh']."</td>";
				$html .= "<td align='center' class='border_left' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['FormPosition']."'>".$eReportCard['Template']['FormPositionCh']."</td>";
			$html .= "</tr>";*/
		$html .= "<table width='100%' align='center' border='0' cellspacing='0' cellpadding='0' class='report_border lite_grey_bg' style='margin-top:10px'>";
			$html .= "<tr>";
				$html .= "<td align='center' style='padding:8px' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['Subject']."'>".$eReportCard['Template']['SubjectEn']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				$html .= $eReportCard['Template']['SubjectCh']."</td>";
				$html .= "<td align='center' style='padding:8px' class='border_left ' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['FullMark']."'>".$eReportCard['Template']['FullMarkEn']."<br>".$eReportCard['Template']['FullMarkCh']."</td>";
				$html .= $this->Get_Double_Line_Td("left",1);
				$html .= "<td align='center' style='padding:8px' class='border_left' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['Mark']."'>".$eReportCard['Template']['TestMarkEn']."<br>".$eReportCard['Template']['TestMarkCh']."</td>";
				$html .= "<td align='center' style='padding:8px' class='border_left' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['FormPosition']."'>".$eReportCard['Template']['FormPositionEn']."<br>".$eReportCard['Template']['FormPositionCh']."</td>";
			$html .= "</tr>";
			$html .= "<tr>";
				$html .= $this->Get_Double_Line_Td("top");
				$html .= $this->Get_Double_Line_Td("top");
				$html .= $this->Get_Double_Line_Td();
				$html .= $this->Get_Double_Line_Td("top");
				$html .= $this->Get_Double_Line_Td("top");
			$html .= "</tr>";
			/*$html .= "<tr>";
				$html .= "<td align='center' class='border_left' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['FullMark']."'>".$eReportCard['Template']['FullMarkCh']."</td>";
				$html .= "<td align='center' class='border_left' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['Mark']."'>".$eReportCard['Template']['TestMarkCh']."</td>";
				$html .= "<td align='center' class='border_left' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['FormPosition']."'>".$eReportCard['Template']['FormPositionCh']."</td>";
			$html .= "</tr>";*/
		### Get Student Study List
		$thisStudyList = $this->Get_Student_Studying_SubjectID($ReportID,$StudentID);
		$Display = array_values(array_intersect($Display,$thisStudyList));

		# must print 12 row	
		for($i=0; $i<(12+$emptyrow); $i++)
		{
			$thisSubjectID = $Display[$i];
			### Get Full Mark
			$thisFullMark = $this->GET_SUBJECT_FULL_MARK($thisSubjectID,$ClassLevelID,$ReportID);
			
			### Get Mark and Rank
			$thisMarkDisplay = $StudentRankScoreAry[$thisSubjectID][$Assessment[$thisSubjectID]]["Score"];
			$thisRankDisplay = $StudentRankScoreAry[$thisSubjectID][$Assessment[$thisSubjectID]]["Rank"];
			
						//!in_array($thisSubjectID, $thisStudyList)
			if (substr($thisMarkDisplay, 0, 3) == 'DRP' )
			{
				$emptyrow++;
				continue;
			}
			
			$css_border_top = "border_top";//($i!=14)? "border_top" : "";
			if(!empty($thisSubjectID))
			{
				$html .= "<tr>";
					$html .= $SubjectCol_HTML_Arr[$thisSubjectID];
					$html .= "<td class='tabletext $css_border_top border_left' height='{$LineHeight}' align='center'>".$thisFullMark."</td>";
					$html .= $this->Get_Double_Line_Td("left",1);
					$html .= "<td class='tabletext $css_border_top border_left' height='{$LineHeight}' align='center'>".($thisMarkDisplay!=''?$thisMarkDisplay:"&nbsp;")."</td>";
					$html .= "<td class='tabletext $css_border_top border_left' height='{$LineHeight}' align='center'>".($thisRankDisplay!=''?$thisRankDisplay:"&nbsp;")."</td>";
				$html .= "</tr>";
			}
			else
			{
				$html .= "<tr>";
					$html .= "<td class='tabletext $css_border_top' height='{$LineHeight}' align='center'>&nbsp;</td>";
					$html .= "<td class='tabletext $css_border_top border_left' height='{$LineHeight}' align='center'>&nbsp;</td>";
					$html .= $this->Get_Double_Line_Td("left",1);
					$html .= "<td class='tabletext $css_border_top border_left' height='{$LineHeight}' align='center'>&nbsp;</td>";
					$html .= "<td class='tabletext $css_border_top border_left' height='{$LineHeight}' align='center'>&nbsp;</td>";
				$html .= "</tr>";
			}
		}
			
			
		$html .= '</table>';
		
		return $html;
		

	}
	
	function getAssessmentRemarkTable($ReportID, $StudentID,$Assessment)
	{
		global $eReportCard,$eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		//$LineHeight = $ReportSetting['LineHeight'];
		$LineHeight = $eRCTemplateSetting['AssessmentReport']['LineHeight'];
		
		$ReportTitleArr = $this->returnReportColoumnTitle($ReportID);

		# Get ColumnID Order ($ColumnIDOrder[$ReportColumnID] = $Order)
		# The $Order is the Order of ColumnID order by Display Order 
		$Order=0;
		foreach((array)$ReportTitleArr as $ColumnID => $Title)
			$ColumnIDOrder[$ColumnID] = ++$Order;
		
		$ColumnIDs = array_values(array_unique($Assessment));
		for($i=0;$i<sizeof($ColumnIDs);$i++) 
			$InvolvedAssessmentNumber[] = $ColumnIDOrder[$ColumnIDs[$i]];
		//$InvolvedAssessmentNumber is used to check which assessments are involved in the report
		
		$Remarks = $this->getReportOtherInfoData($ReportID, $StudentID, '', 'assessment');
			
		$AssessmentRemarkArr = array_shift($Remarks[$StudentID]); //The return ary must hv only 1 record
		for($i=0 ; $i<sizeof($AssessmentRemarkArr['AssessmentNumber']); $i++) {
			$thisAssessmentNumber = trim($AssessmentRemarkArr['AssessmentNumber'][$i]);
			//$thisRemark = $AssessmentRemarkArr['AssessmentRemark'][$i];
			$thisRemark = $AssessmentRemarkArr['AssessmentRemark'];
			//2013-0417-0948-31073
			if (is_array($thisRemark)) {
				$thisRemark = implode('<br />', (array)$thisRemark);
			}
			
			if(in_array($thisAssessmentNumber,$InvolvedAssessmentNumber)) {
				$RemarksArr[] = $thisRemark;
			}
		}
		
		$html .= "<table width='100%' align='left' border='0' cellspacing='0' cellpadding='4' class='report_border lite_grey_bg' style='margin-top:15px'>";
			$html .= "<tr>";
				$html .= "<td class='tabletext $css_border_top' height='{$LineHeight}'>".$eReportCard['Template']['AssessmentReport']['RemarksEn']."  ".$eReportCard['Template']['AssessmentReport']['RemarksCh'].": </td>";
			$html .= "</tr>";
			
		for($i=0; $i<4; $i++)
		{
			$css_border_top = '';
			//if($i==0) $css_border_top = "border_top" ;
			$html .= "<tr>";
				$html .= "<td class='tabletext $css_border_top' height='{$LineHeight}'>".($RemarksArr[$i]?$RemarksArr[$i]:"&nbsp;")."</td>";
			$html .= "</tr>";
		}
			
		$html .= "</table>";
		
		return $html;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
		
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID,'',0,1);
						
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='2' class='report_border'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
				continue;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				# Subject Teacher Comment
				if ($AllowSubjectTeacherComment) {
					$css_border_top = $isSub?"":"border_top";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
					}
					$DetailsTable .= "</span></td>";
				}
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		#########################################################
		############## Marks START
		$row2 = "";
		if($ReportType=="T")	# Terms Report Type
		{
			# Retrieve Invloved Assesment
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if (sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
				
			$e = 0;
			for($i=0;$i<sizeof($ColumnTitle);$i++)
			{
				//$ColumnTitleDisplay = convert2unicode($ColumnTitle[$i], 1, 2);
				$ColumnTitleDisplay = $ColumnTitle[$i];
				$row1 .= "<td valign='middle' height='{$LineHeight}' class='border_left tabletext' align='center'><b>". $ColumnTitleDisplay . "</b></td>";
				$n++;
 				$e++;
			}
		}
		else					# Whole Year Report Type
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
 			$needRowspan=0;
			for($i=0;$i<sizeof($ColumnData);$i++)
			{
				$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
				$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
				if($isDetails==1)
				{
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColumnID = array();
					$ColumnTitle = array();
					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
					for($j=0;$j<sizeof($ColumnTitle);$j++)
					{
						//$ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$j], 1, 2));
						$ColumnTitleDisplay =  $ColumnTitle[$j];
						$row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $ColumnTitleDisplay . "</td>";
						$n++;
						$e++;
					}
					$colspan = "colspan='". sizeof($ColumnTitle) ."'";
					$Rowspan = "";
					$needRowspan++;
					$t[$i] = sizeof($ColumnTitle);
				}
				else
				{
					$colspan = "";
					$Rowspan = "rowspan='2'";
					$e++;
				}
				$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left small_title' align='center'>". $SemName ."</td>";
			}
		}
		############## Marks END
		#########################################################
		$Rowspan = $row2 ? "rowspan='2'" : "";

		if(!$needRowspan)
			$row1 = str_replace("rowspan='2'", "", $row1);
		
		$x = "<tr>";
		# Subject 
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		for($i=0;$i<sizeof($SubjectColAry);$i++)
		{
			$SubjectEng = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'];
			$SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];
			$SubjectTitle = $SubjectColAry[$i];
			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
			$x .= "<td {$Rowspan}  valign='middle' class='small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>". $SubjectTitle . "</td>";
			$n++;
		}
		
		# Marks
		$x .= $row1;
		
		# Subject Overall 
		if($ShowRightestColumn)
		{
			$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Overall'] ."' class='border_left small_title' align='center'>". $eReportCard['Template']['SubjectOverall'] ."</td>";
			$n++;
		}
		else
		{
			//$e--;	
		}
		
		# Subject Teacher Comment
		if ($AllowSubjectTeacherComment) {
			$x .= "<td {$Rowspan} valign='middle' class='border_left small_title' align='center' width='10%'>".$eReportCard['TeachersComment']."</td>";
		}
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['AssessmentReport']['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID = $SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		//$css_border_top = ($Prefix)? "" : "border_top";
			 		$css_border_top =  "border_top" ;
			 		
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
		 				$v = strtoupper($v);
		 				
			 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='tabletext' nowrap>$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[$SubSubjectID] = $t;
					$isFirst = 0;
				}
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
		}
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' >";
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			$IssueDate = ($SettingID == "IssueDate" && $Issued)	? $Issued : "&nbsp;";
			$SignatureTable .= "<td valign='bottom' align='center'>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0' width='155px'>";
			$SignatureTable .= "<tr><td align='center' class='small_title'   valign='bottom'>". $IssueDate ."</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='small_title' valign='bottom' height='32px' style='font-size:12px; border-top:1px solid #000'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $this->Get_Student_ClassName($StudentID);
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID,'',0,1);
		$GrandTotal = $StudentID ? $this->Get_Score_Display_HTML($result['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		$GrandAverage = $StudentID ? $this->Get_Score_Display_HTML($result['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		
		$ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
  		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
  		$StreamPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritStream'], $ReportID, $ClassLevelID, '', '', 'StreamPosition') : "#";
  		$ClassNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
  		$FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
		
		if ($CalOrder == 2) {
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				
				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID, '', 1,1);
				
				$columnTotal[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
				$columnAverage[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
				
				$columnClassPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "S";
				$columnFormPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "S";
				$columnStreamPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritStream'], $ReportID, $ClassLevelID, '', '', 'StreamPosition') : "S";
				$columnClassNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
				$columnFormNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";
			}
		}
		
		$first = 1;
		# Overall Result
		if($ShowGrandTotal)
		{
			$thisTitleEn = $eReportCard['Template']['OverallResultEn'];
			$thisTitleCh = $eReportCard['Template']['OverallResultCh'];
			$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnTotal, $GrandTotal, $first);
			
			$first = 0;
		}
		
		if ($eRCTemplateSetting['HideCSVInfo'] != true)
		{
			# Average Mark 
			if($ShowGrandAvg)
			{
				$thisTitleEn = $eReportCard['Template']['AvgMarkEn'];
				$thisTitleCh = $eReportCard['Template']['AvgMarkCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnAverage, $GrandAverage, $first);
				
				$first = 0;
			}
			
			# Position in Class 
			if($ShowOverallPositionClass)
			{
				$thisTitleEn = $eReportCard['Template']['ClassPositionEn'];
				$thisTitleCh = $eReportCard['Template']['ClassPositionCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassPos, $ClassPosition, $first);
				
				$first = 0;
			}
			
			# Number of Students in Class 
			if($ShowNumOfStudentClass)
			{
				$thisTitleEn = $eReportCard['Template']['ClassNumOfStudentEn'];
				$thisTitleCh = $eReportCard['Template']['ClassNumOfStudentCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassNumOfStudent, $ClassNumOfStudent, $first);
				
				$first = 0;
			}
			
			# Position in Form 
			if($ShowOverallPositionForm)
			{
				$thisTitleEn = $eReportCard['Template']['FormPositionEn'];
				$thisTitleCh = $eReportCard['Template']['FormPositionCh'];
				if(!empty($StreamPosition))
				{
					$FormPosition = $StreamPosition;
					$columnFormPos = $columnStreamPos;
				}
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormPos, $FormPosition, $first);
				
				$first = 0;
			}
			
			# Number of Students in Form 
			if($ShowNumOfStudentForm)
			{
				$thisTitleEn = $eReportCard['Template']['FormNumOfStudentEn'];
				$thisTitleCh = $eReportCard['Template']['FormNumOfStudentCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormNumOfStudent, $FormNumOfStudent, $first);
				
				$first = 0;
			}
			
			##################################################################################
			# CSV related
			##################################################################################
			# build data array
			$ary = array();
			$csvType = $this->getOtherInfoType();
			
			//modified by Marcus 20/8/2009
			$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID,$StudentID);
			$ary=$OtherInfoDataAry[$StudentID];
			
			if($SemID=="F")
			{
				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
				
				/*
				foreach($ColumnData as $k1=>$d1)
				{
					$TermID = $d1['SemesterNum'];
					$InfoTermID = $TermID;
					
					if(!empty($csvType))
					{
						foreach($csvType as $k=>$Type)
						{
							$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);
							if(!empty($csvData)) 
							{
								foreach($csvData as $RegNo=>$data)
								{
									if($RegNo == $WebSamsRegNo)
									{
										foreach($data as $key=>$val)
											$ary[$TermID][$key] = $val;
									}
								}
							}
						}
					}
				}
				*/
				
				# calculate sems/assesment col#
				$ColNum2Ary = array();
				foreach($ColumnData as $k1=>$d1)
				{
					$TermID = $d1['SemesterNum'];
					if($d1['IsDetails']==1)
					{
						# check sems/assesment col#
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
						$thisReportID = $thisReport['ReportID'];
						$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
						$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1;
					}
					else
						$ColNum2Ary[$TermID] = 0;
				}
			}
			/*
			else
			{
				$InfoTermID = $SemID;
				
				if(!empty($csvType))
				{
					foreach($csvType as $k=>$Type)
					{
						$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);
						
						if(!empty($csvData)) 
						{
							foreach($csvData as $RegNo=>$data)
							{
								if($RegNo == $WebSamsRegNo)
								{
									foreach($data as $key=>$val)
										$ary[$SemID][$key] = $val;
								}
							}
						}
					}
				}
			}
			*/

			$border_top = $first ? "border_top" : "";
			
			# Days Absent 
			$thisTitleEn = $eReportCard['Template']['DaysAbsentEn'];
			$thisTitleCh = $eReportCard['Template']['DaysAbsentCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Days Absent", $ary);
			
			# Times Late 
			$thisTitleEn = $eReportCard['Template']['TimesLateEn'];
			$thisTitleCh = $eReportCard['Template']['TimesLateCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Time Late", $ary);
			
			# Conduct 
			$thisTitleEn = $eReportCard['Template']['ConductEn'];
			$thisTitleCh = $eReportCard['Template']['ConductCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Conduct", $ary);
		}
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eReportCard;				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		if($ReportType=="T")	# Temrs Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
							
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# define css
				$css_border_top = ($isSub)? "" : "border_top";
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				$isAllNA = true;
									
				# Assessment Marks & Display
				for($i=0;$i<sizeof($ColumnID);$i++)
				{
					$thisColumnID = $ColumnID[$i];
					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
					
					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
					$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);

					if ($isSub && $columnSubjectWeightTemp == 0)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
						$thisMarkDisplay = $this->EmptySymbol;
					} else {
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisMSGrade = $MarksAry[$SubjectID][$ColumnID[$i]]['Grade'];
						$thisMSMark = $MarksAry[$SubjectID][$ColumnID[$i]]['Mark'];
						
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
						
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						if ($thisMark != "N.A.")
						{
							$isAllNA = false;
						}
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
						
						if($needStyle)
						{
							if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
							{
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							}
							else
							{
								$thisMarkTemp = $thisMark;
							}
							
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
						
					//	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					//	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
					//	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark; 
					}
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					
  					if($ShowSubjectFullMark)
  					{
	  					# check special full mark
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
	  					
						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
					}
					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}
								
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall)
				{
					$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
					$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];

					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
										
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					# check special case
					if ($CalculationMethod==2 && $isSub)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else
					{
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
						if($needStyle)
						{
	 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							else
								$thisMarkTemp = $thisMark;
								
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
						}
						else
							$thisMarkDisplay = $thisMark;
					}
					
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					if($ShowSubjectFullMark)
  					{
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $thisFullMark .")</td>";
					}
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
				}
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
							
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				$isAllNA = true;
				
				# Terms's Assesment / Terms Result
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
					$css_border_top = $isSub? "" : "border_top";
					
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					if($isDetails==1)		# Retrieve assesments' marks
					{
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum'] ."' and ClassLevelID = '".$ClassLevelID."' ");
						
						# if no term reports, CANNOT retrieve assessment result at all
						if (empty($thisReport)) {
							for($j=0;$j<sizeof($ColumnID);$j++) {
								$x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
							}
						} else {
							$thisReportID = $thisReport['ReportID'];
							$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
							$ColumnID = array();
							$ColumnTitle = array();
							foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
							
							$thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);		
								 
							for($j=0;$j<sizeof($ColumnID);$j++)
							{
								$thisColumnID = $ColumnID[$i];
								$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
								$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
								$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
								
								$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
								$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
					
								if ($isSub && $columnSubjectWeightTemp == 0)
								{
									$thisMarkDisplay = $this->EmptySymbol;
								}
								else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
									$thisMarkDisplay = $this->EmptySymbol;
								} else {
									$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnID[$j] ."' and SubjectID = '$SubjectID' ");
									$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
									
									$thisMSGrade = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade'];
									$thisMSMark = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Mark'];
									
									$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
									$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
									
									# for preview purpose
									if(!$StudentID)
									{
										$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
										$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
									}
									$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
													
									if ($thisMark != "N.A.")
									{
										$isAllNA = false;
									}
									
									# check special case
									list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
									if($needStyle)
									{
										if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
										{
											$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
										}
										else
										{
											$thisMarkTemp = $thisMark;
										}
										
										$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
									}
									else
									{
										$thisMarkDisplay = $thisMark;
									}
								}
									
								$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
								$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
			  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
			  					
			  					if($ShowSubjectFullMark)
			  					{
				  					$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
									$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
								}
								
								$x[$SubjectID] .= "</tr></table>";
								$x[$SubjectID] .= "</td>";
							}
						}
					}
					else					# Retrieve Terms Overall marks
					{
						if ($isParentSubject && $CalculationOrder == 1) {
							$thisMarkDisplay = $this->EmptySymbol;
						} else {
							# See if any term reports available
							$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
							
							# if no term reports, the term mark should be entered directly
							
							if (empty($thisReport)) {
								$thisReportID = $ReportID;
								$MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
								//$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
								
								$thisMSGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"];
								$thisMSMark = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"];
								
								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
									
							} else {
								$thisReportID = $thisReport['ReportID'];
								$MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
								//$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
								
								$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
								$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
								
								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
							}
	 						
							$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
							$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
														
							# for preview purpose
							if(!$StudentID)
							{
								$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
								$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
							}
							# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
							$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
							
							if ($thisMark != "N.A.")
							{
								$isAllNA = false;
							}
						
							# check special case
							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
							if($needStyle)
							{
								$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
							}
						}
							
						$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
	  					
	  					if($ShowSubjectFullMark)
	  					{
 							$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1 && $UseWeightedMark) ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID];
							$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
						}
						
						$x[$SubjectID] .= "</tr></table>";
						$x[$SubjectID] .= "</td>";
					}
				}
				
				# Subject Overall
				if($ShowSubjectOverall)
				{
					$MarksAry = $this->getMarks($ReportID, $StudentID,"","",1);		
					
					$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
					$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
					
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					if ($CalculationMethod==2 && $isSub)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else
					{
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
						if($needStyle)
						{
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
						}
						else
							$thisMarkDisplay = $thisMark;
					}
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					
  					if($ShowSubjectFullMark)
  					{
	  					# check special full mark
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
	  					
						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark * $thisSubjectWeight : $thisFullMark) : $thisFullMark;
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
					}
  					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
					
					
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
				}
				
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		}	# End Whole Year Report Type
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
		
		if ($eRCTemplateSetting['HideCSVInfo'] == true)
		{
			return "";
		}
			
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight					= $ReportSetting['LineHeight'];
		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

		//modified by marcus 20/8/2009
		//return: $ReturnArr[$StudentID][$YearTermID][$UploadType] = Value
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID); 
		
		# retrieve the latest Term
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		foreach($sems as $TermID=>$TermName)
			$latestTerm = $TermID;
		//$latestTerm++;
		
		/*
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $this->Get_Student_ClassName($StudentID);
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		foreach($csvType as $k=>$Type)
		{
			$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);	
			if(!empty($csvData)) 
			{
				foreach($csvData as $RegNo=>$data)
				{
					if($RegNo == $WebSamsRegNo)
					{
	 					foreach($data as $key=>$val)
		 					$ary[$key] = $val;
					}
				}
			}
		}
		*/
		
		# retrieve result data
		$ary = $OtherInfoDataAry[$StudentID][$latestTerm];
		
		$Absence = ($ary['Days Absent'])? $ary['Days Absent'] : 0;
		$Lateness = ($ary['Time Late'])? $ary['Time Late'] : 0;
		$EarlyLeave = ($ary['Early Leave'])? $ary['Early Leave'] : 0;
		$Promotion = ($ary['Promotion'])? $ary['Promotion'] : $this->EmptySymbol;
		$Merits = ($ary['Merits'])? $ary['Merits'] : 0;
		$MinorCredit = ($ary['Minor Credit'])? $ary['Minor Credit'] : 0;
		$MajorCredit = ($ary['Major Credit'])? $ary['Major Credit'] : 0;
		$Demerits = ($ary['Merits'])? $ary['Demerits'] : 0;
		$MinorFault = ($ary['Minor Fault'])? $ary['Minor Fault'] : 0;
		$MajorFault = ($ary['Major Fault'])? $ary['Major Fault'] : 0;
		$Remark = ($ary['Remark'])? $ary['Remark'] : $this->EmptySymbol;
		$ECA = ($ary['ECA'])? $ary['ECA'] : $this->EmptySymbol;
		if (is_array($ECA))
			$ecaList = implode("<br>", $ECA);
		else
			$ecaList = $ECA;
		
		
		$merit = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		$merit .= "<tr>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['Merits'] . ": " . ($ary['Merits'] ? $ary['Merits'] :0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['Demerits'] . ": " . ($ary['Demerits'] ? $ary['Demerits'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorCredit'] . ": " . ($ary['Minor Credit'] ? $ary['Minor Credit'] :0) ."</td>";
		$merit .= "</tr>";
		$merit .= "<tr>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorCredit'] . ": " . ($ary['Major Credit'] ? $ary['Major Credit'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorFault'] . ": " . ($ary['Minor Fault'] ? $ary['Minor Fault'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorFault'] . ": " . ($ary['Major Fault'] ? $ary['Major Fault'] : 0) ."</td>";
		$merit .= "</tr>";
		$merit .= "</table>";
		$remark = $ary['Remark'];
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$classteachercomment = $CommentAry[0];
		$eca = $ary['ECA'];
		if (is_array($eca))
			$ecaList = implode("<br>", $eca);
		else
			$ecaList = $eca;
		
		$x .= "<br>";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='60' class='report_border'>";
		$x .= "<tr>";
		$x .= "<td width='50%' valign='top'>";
			# Merits & Demerits 
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr>";
				$x .= "<td class='tabletext'><b>". $eReportCard['Template']['MeritsDemerits']."</b><br>".$merit."</td>";
			$x .= "</tr>";
			$x .= "</table>";	
			$x .= "</td></tr></table>";	
		$x .="</td>";
		$x .= "<td width='50%' class=\"border_left\" valign='top'>";
			# Remark 
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr>";
				$x .= "<td class='tabletext'><b>". $eReportCard['Template']['Remark']."</b><br>".$remark."</td>";
			$x .= "</tr>";
			$x .= "</table>";	
			$x .= "</td></tr></table>";	
		$x .="</td>";
		$x .= "</tr>";
		$x .= "</table>";
		
		$x .= "<br>";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='60' class='report_border'>";
		$x .= "<tr>";
		if($AllowClassTeacherComment)
		{
			$x .= "<td width='50%' valign='top'>";
				# Class Teacher Comment 
				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
				$x .= "<tr>";
					$x .= "<td class='tabletext'><b>". $eReportCard['Template']['ClassTeacherComment']."</b><br>".stripslashes(nl2br($classteachercomment))."</td>";
				$x .= "</tr>";
				$x .= "</table>";	
				$x .= "</td></tr></table>";	
			$x .="</td>";
			
			$x .= "<td width='50%' class='border_left' valign='top'>";
		} else {
			$x .= "<td width='50%' valign='top'>";
		}
			# ECA 
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr>";
				$x .= "<td class='tabletext'><b>". $eReportCard['Template']['eca']."</b><br>".$ecaList."</td>";
			$x .= "</tr>";
			$x .= "</table>";	
			$x .= "</td></tr></table>";	
		$x .="</td>";
		$x .= "</tr>";
		$x .= "</table>";
		
		return $x;
	}
	
	########### END Template Related

	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;

		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# initialization
		$border_top = "";
		$x = "";
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh);
			
		if($ReportType=="W")	# Whole Year Report
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			$thisTotalValue = "";
			foreach($ColumnData as $k1=>$d1)
			{
				# get value of this term
				$TermID = $d1['SemesterNum'];
				
				$thisValue = $StudentID ? ($ValueArr[$TermID][$InfoKey] ? $ValueArr[$TermID][$InfoKey] : $this->EmptySymbol) : "#";
				
				# calculation the overall value
				if (is_numeric($thisValue)) {
					if ($thisTotalValue == "")
						$thisTotalValue = $thisValue;
					else if (is_numeric($thisTotalValue))
						$thisTotalValue += $thisValue;
				}
				
				# insert empty cell for assessments
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					
				# display this term value
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			}
			
			# display overall year value
			if($ShowRightestColumn)
			{
				if($thisTotalValue=="")
					$thisTotalValue = ($ValueArr[0][$InfoKey]=="")? $this->EmptySymbol : $ValueArr[0][$InfoKey];
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisTotalValue."</td>";
			}
		}
		else				# Term Report
		{
			# get value of this term
			$thisValue = $StudentID ? ($ValueArr[$SemID][$InfoKey] ? $ValueArr[$SemID][$InfoKey] : $this->EmptySymbol) : "#";
			
			# insert empty cell for assessments
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				
			# display this term value
			if($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
		}
		
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		$CalOrder = $this->Get_Calculation_Setting($ReportID);
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		$border_top = $isFirst ? "border_top" : "";
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, $isFirst);
		
		if ($CalOrder == 1) {
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
		} else {
			$curColumn = 0;
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				for ($i=0; $i<$NumOfAssessment[$curColumn]-1 ; $i++)
				{
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				}
				
				$thisValue = $ValueArr[$ColumnID];
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisValue."&nbsp;</td>";
				$curColumn++;
			}
		}
		
		if ($ShowRightestColumn)
		{
			$thisValue = $OverallValue;
			/*
			if ($UpperLimit=="" || $UpperLimit==0 || $thisValue <= $UpperLimit)
			{
				$thisDisplay = $thisValue;
			}
			else
			{
				$thisDisplay = $this->EmptySymbol;
			}
			*/
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."&nbsp;</td>";
		}
			
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0)
	{
		$x = "";
		$border_top = ($hasBorderTop)? "border_top" : "";
		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleEn ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleCh ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		
		return $x;
	}
	
	function Is_Show_Rightest_Column($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		return $ShowRightestColumn;
	}
	
	function Get_Calculation_Setting($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		return $CalOrder;
	}
	
	function Get_Assessment_Report_Rank_Arr($ReportID,$ClassLevelID,$DataSource,$StudentIDArr)
	{
		if($DataSource=='Report')
		{
			$MarksArr = $this->getMarks($ReportID, '', '', 0, 1, '', '', '', 1);
			
			foreach((array) $MarksArr as $thisStudentID => $SubjectData)
			{
				$isAllNA = true;
				foreach((array) $SubjectData as $thisSubjectID => $ReportColumnAry)
				{
					$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisSubjectID,0 ,0, $ReportID );
					$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
					
					foreach((array) $ReportColumnAry as $thisReportColumnID => $ReportColumnData)
					{
						$thisMSGrade = $ReportColumnData['Grade'];
						$thisMSMark = $ReportColumnData['Mark'];
						
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
						
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						if ($thisMark != "N.A.")
						{
							$isAllNA = false;
						}
										 
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisSubjectID, $thisMark, $ReportColumnData['Grade']);
								
						if($needStyle)
						{
							$thisMarkTemp = $thisMark;
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $thisSubjectID, $thisMarkTemp);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
						
						$StudentRankScoreAry[$thisStudentID][$thisSubjectID][$thisReportColumnID]["Score"] = $thisMarkDisplay;
						
						if (is_numeric($thisMark))
						{
							if ($ReportColumnData['OrderMeritStream'] != '')
								$StudentRankScoreAry[$thisStudentID][$thisSubjectID][$thisReportColumnID]["Rank"] = $ReportColumnData['OrderMeritStream'];
							else
								$StudentRankScoreAry[$thisStudentID][$thisSubjectID][$thisReportColumnID]["Rank"] = $ReportColumnData['OrderMeritForm'];
						}
						else
						{
							$StudentRankScoreAry[$thisStudentID][$thisSubjectID][$thisReportColumnID]["Rank"] = $this->EmptySymbol;
						}
						//$StudentRankScoreAry[$thisStudentID][$thisSubjectID][$thisReportColumnID]["Rank"] = is_numeric($thisMark)?$ReportColumnData['OrderMeritStream']:$this->EmptySymbol;
						
						if ($StudentRankScoreAry[$thisStudentID][$thisSubjectID][$thisReportColumnID]["Rank"] == '')
							$StudentRankScoreAry[$thisStudentID][$thisSubjectID][$thisReportColumnID]["Rank"] = $this->EmptySymbol;
						
					}
				}
				if($isAllNA)
				{
					$StudentRankScoreAry[$thisStudentID] = null;
					$StudentRankScoreAry[$thisStudentID]["isAllNA"] = $isAllNA;
				}
			}
		}
		else
		{
			$ReportColumnDataArr = $this->returnReportTemplateColumnData($ReportID);
			
			foreach((array)$ReportColumnDataArr as $ReportColumnData)
			{
				$thisReportColumnID =  $ReportColumnData['ReportColumnID'];
				$MarksArr = $this->GET_MARKSHEET_COLUMN_SCORE($StudentIDArr,$thisReportColumnID);
//				debug_pr($MarksArr);
				foreach((array)$MarksArr as $thisStudentID => $SubjectDataArr)
				{
					foreach((array) $SubjectDataArr as $thisSubjectID => $SubjectData)
					{
						$tmpScoreArr[$thisReportColumnID][$thisSubjectID][$thisStudentID] = trim($SubjectData['MarkNonNum'])==''?($SubjectData['MarkRaw']==-1?$this->EmptySymbol:$SubjectData['MarkRaw']):$SubjectData['MarkNonNum'];
					}
				}
			}
			
			//generate rank 
			foreach((array) $tmpScoreArr as $thisReportColumnID => $SubjectDataArr)
			{
				foreach((array) $SubjectDataArr as $thisSubjectID => $StudentScoreArr)
				{
					//Get Student Rank - return $returnArr[$StudentID] = $Rank;
					$StudentRankArr = $this->Generate_Rank($StudentScoreArr); 
					
					foreach((array) $StudentScoreArr as $thisStudentID => $thisMark)
					{
						if(!isset($StudentRankScoreAry[$thisStudentID]["isAllNA"])) 
							$StudentRankScoreAry[$thisStudentID]["isAllNA"]=true;
						
						if ($thisMark != "N.A.")
						{
							$StudentRankScoreAry[$thisStudentID]["isAllNA"] = false;
						 
							list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisSubjectID, $thisMark, $thisMark);
									
							if($needStyle && $thisMark != $this->EmptySymbol)
							{
								$thisMarkTemp = $thisMark;
								$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $thisSubjectID, $thisMarkTemp);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
							}
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
						
						$thisRank = $StudentRankArr[$thisStudentID];
						$StudentRankScoreAry[$thisStudentID][$thisSubjectID][$thisReportColumnID]["Score"] = $thisMarkDisplay;
						$StudentRankScoreAry[$thisStudentID][$thisSubjectID][$thisReportColumnID]["Rank"] = $thisRank;
					}
				}
			}
			
		}
		
		return $StudentRankScoreAry;
	}
	
	function Generate_Rank($ParStudentScoreArr)
	{
		global $eRCTemplateSetting;
		if(!is_array($ParStudentScoreArr)) return '';
		
		arsort($ParStudentScoreArr);
		
		foreach($ParStudentScoreArr as $studentID => $mark) 
		{
			
			$numberCount++;
			
			if ($eRCTemplateSetting['RankingMethod'] == '1223')
			{
				# 1,2,2,3
				if(!is_numeric($mark))
					$rank = $this->EmptySymbol;
				else if($mark != $lastMark)
					$rank = ++$orderCount;
			}
			else
			{
				# 1,2,2,4
				if(!is_numeric($mark))
				{
					$rank = $this->EmptySymbol;
					$numberCount--;
				}
				else if ($mark == $lastMark)
				{
					$rank = $lastOrderCount;
				}
				else
				{
					$rank = $numberCount;
				}
				$lastOrderCount = $rank;
			}
			
			$returnArr[$studentID] = $rank;
			$lastMark = $mark;
		}
		
		return $returnArr;
	}
	
	// $border, e.g. "left,right,top"
	function Get_Double_Line_Td($border="",$width=1)
	{
		global $image_path,$LAYOUT_SKIN;
		
		$borderArr = explode(",",$border);
		$left = $right = $top = $bottom = 0;
		
		if(in_array("left",$borderArr)) $left = $width;
		if(in_array("right",$borderArr)) $right = $width;
		if(in_array("top",$borderArr)) $top = $width;
		if(in_array("bottom",$borderArr)) $bottom = $width;

		$style = " style='border-style:solid; border-color:#000; border-width:{$top}px {$right}px {$bottom}px {$left}px ' ";
		
		$td = "<td width='0%' height='0%' $style><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='$width' height='$width'/></td>";
		return $td;
	}
}
?>