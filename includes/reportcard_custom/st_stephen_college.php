<?php
# Editing by 

/*
 *	2012-03-27 Ivan [2012-0321-1414-21066]
 *	- ECA display format changed to "[Role] of [Name of the activity] – [Performance]" 
 */

### Customization library for St. Stephen's College ###
include_once($intranet_root."/lang/reportcard_custom/st_stephen_college.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		//$this->configFilesType = array("merit","demerit","award", "additional_comments","summary");
		//$this->configFilesType = array("attendance","merit","demerit","award", "eca","additional_comments", "conduct");
		//$this->configFilesType = array("attendance","merit","demerit","award", "eca");
		$this->configFilesType = array("attendance","merit","demerit","award");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 0;
		$this->IsEnableMarksheetExtraInfo = 0;
		
		// Moved to config file
		// For standardlize form validation
		//$this->textAreaMaxChar = 500;
		
		//$this->PersonalCharGradeArr[Index] = LangKey
		$this->DefaultPersonalCharGrade = 10;
		$this->PersonalCharGradeArr['10'] = 'NotApplicable';
		$this->PersonalCharGradeArr['20'] = 'NeedsImprovement';
		$this->PersonalCharGradeArr['30'] = 'Satisfactory';
		$this->PersonalCharGradeArr['40'] = 'Good';
		$this->PersonalCharGradeArr['50'] = 'Excellent';
	}
	
	# [OVERRIDE]
	# Create the $TAGS_OBJ for display tabs on the Other Info upload page
	# New condition must be added for more categories of Other Info
	# 	$UploadType = currently selected tab
	# 	ref: /home/admin/reportcard2008/management/other_info/index.php
	function getOtherInfoTabObjArr($UploadType) {
		global $eReportCard, $PATH_WRT_ROOT;
		$otherInfoTypes = $this->getOtherInfoType();
		
		$TAGS_OBJ = array();
		# tag information
		if (in_array("attendance", $otherInfoTypes))
			$TAGS_OBJ[] = array($eReportCard['AttendanceInfoUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=attendance", ($UploadType=="attendance" || !isset($UploadType))?1:0);
		if (in_array("merit", $otherInfoTypes))
			$TAGS_OBJ[] = array($eReportCard['MeritRecordUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=merit", $UploadType=="merit"?1:0);
		if (in_array("demerit", $otherInfoTypes))
			$TAGS_OBJ[] = array($eReportCard['AnnualDemeritRecordUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=demerit", $UploadType=="demerit"?1:0);
		if (in_array("award", $otherInfoTypes))
			$TAGS_OBJ[] = array($eReportCard['AwardRecordUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=award", $UploadType=="award"?1:0);
		if (in_array("eca", $otherInfoTypes))
			$TAGS_OBJ[] = array($eReportCard['ECAUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=eca", $UploadType=="eca"?1:0);
		if (in_array("additional_comments", $otherInfoTypes))
			$TAGS_OBJ[] = array($eReportCard['AdditionalComment'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=additional_comments", $UploadType=="additional_comments"?1:0);
		if (in_array("summary", $otherInfoTypes))
			$TAGS_OBJ[] = array($eReportCard['OtherStudentInfoValue_Conduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=summary", $UploadType=="summary"?1:0);
			
		return $TAGS_OBJ;
	}
	
	/* moved to libreportcard2008.php in IP25
	function returnPersonalCharData($id='')
	{
		$table = $this->DBName.".RC_PERSONAL_CHARACTERISTICS";
		
		$sql = "select * from {$table} ";
		if($id) $sql .= " where CharID=$id ";
		$sql .= " order by Title_EN";
		
		$result = $this->returnArray($sql);
		return $result;
	}
	
	
	function returnPersonalCharSettingData($ClassLevelID, $SubjectID='')
	{
		global $intranet_session_language;
		
		$table = $this->DBName.".RC_LEVEL_SUBJECT_PERSONAL_CHARACTERISTICS";
		$table2 = $this->DBName .".RC_PERSONAL_CHARACTERISTICS";
		$sql = "
			select 
				a.ClassLevelID, 
				a.SubjectID,
				b.CharID,
				b.Title_EN,
				b.Title_CH
			from 
				{$table} as a 
				left join {$table2} as b on (a.CharID=b.CharID)
			where 
				ClassLevelID=$ClassLevelID 
		";
		if($SubjectID) $sql .= " and SubjectID=$SubjectID";
		$sql .= " order by Title_". ($intranet_session_language=="en" ? "EN" : "CH");
		
		$result = $this->returnArray($sql);
		return $result;
	}
	*/
	
	########## START Template Related ##############
	function FirstPage($ReportID='', $StudentID='')
	{
		$x = "";
		
		# Header
		$x .= $this->ReportHeader($ReportID);
		
		# Personal Particulars
		$x .= $this->PersonalParticularsWithPhoto($StudentID);

		# Overall Result
		$x .= $this->OverallAcademicResult($ReportID, $StudentID);
		
		# CSV Data
		$x .= $this->CSVData($ReportID, $StudentID);
		
		# Class Teacher Comment
		$x .= $this->ClassTeacherComment($ReportID, $StudentID, "", 1);
		
		# Class Teacher / Principal 
		$x .= $this->ClassTeacherPrincipal($StudentID);
		
		# Explanatory Notes
		$x .= $this->ExplanatoryNotes($ReportID);
		
		# Build the whole page
		$r = "
			<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" valign=\"top\" style=\"page-break-after: always;\">
			<tr><td>". $x ."</td><tr>
			</table>
		";
		
		return $r;
	}
	
	function getSubjectPages($ReportID, $StudentID='') {
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
 		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID);
 		$MainSubjectIDArr = array_keys($MainSubjectArray);
 		
 		$SubjectPersonalCharInfoArr = $this->returnPersonalCharSettingData($ClassLevelID);
 		$SubjectPersonalCharAssoArr = BuildMultiKeyAssoc($SubjectPersonalCharInfoArr, 'SubjectID');
 		
 		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		$MarksAry = $this->getMarks($ReportID, $StudentID);
 		
 		$displaySubjectIDArr = array();
 		$pageCount = 0;
 		$pageSubjectIDArr = array();
 		$curPageSubjectCount = 0;
 		$curPageSubjectIDArr = array();
 		$x = '';
 		foreach($MainSubjectArray as $SubjectID => $SubejctData) {
 			# Hide the subject page if all columns are exempted
			$absArr = array("abs", "+", "-");
			
			$skipFlag = true;
			foreach ($ColumnTitle as $thisColumnID => $thisTitle) {
				$thisMark = $MarksAry[$SubjectID][$thisColumnID]["Mark"];
				$thisGrade = $MarksAry[$SubjectID][$thisColumnID]["Grade"];
				if ( (is_numeric($thisMark) && $thisMark!=-1) ) {
					if ($thisMark != 0) {
						$skipFlag = false;
						break;
					}
					else {
						if (in_array($thisGrade, $absArr)) {
							$skipFlag = false;
							break;
						}
					}
				}
			}
			
			if ($skipFlag) {
				continue;
			}
			else {
				$displaySubjectIDArr[] = $SubjectID;
			}
			
			//$x .= $this->SubjectPage($ReportID, $SubjectID, $StudentID);
 		}
 		
 		// 2011-0504-1459-44125
 		$numOfDisplaySubject = count($displaySubjectIDArr);
 		$LastDisplaySubjectID = $displaySubjectIDArr[$numOfDisplaySubject-1];
 		for ($i=0; $i<$numOfDisplaySubject; $i++) {
 			$thisSubjectID = $displaySubjectIDArr[$i];
 			
 			// If the subject has no personal characteristics, display 2 subjects in a page
 			if (isset($SubjectPersonalCharAssoArr[$thisSubjectID])) {
 				// have personal char for this subject => individual page
 				$pageSubjectIDArr[$pageCount++][] = $thisSubjectID;
 				
 				$curPageSubjectCount = 0;
 				$curPageSubjectIDArr = array();
 			}
 			else {
 				// don't have personal char for this subject => 2 subjects a page
 				if ($curPageSubjectCount == 1) {
 					// add to subject array
 					$curPageSubjectIDArr[] = $thisSubjectID;
 					$pageSubjectIDArr[$pageCount++] = $curPageSubjectIDArr;
 					
 					$curPageSubjectCount = 0;
 					$curPageSubjectIDArr = array();
 				}
 				else {
 					if ($thisSubjectID == $LastDisplaySubjectID) {
 						// last page => add to subject array
 						$pageSubjectIDArr[$pageCount++][] = $thisSubjectID;
 					}
 					else {
 						// wait next subject
	 					$curPageSubjectIDArr[] = $thisSubjectID;
	 					$curPageSubjectCount++;
 					}
 				}
 			}
 		}
 		 		
 		$numOfSubjectPage = count($pageSubjectIDArr);
 		for ($i=0; $i<$numOfSubjectPage; $i++) {
 			$thisSubjectIDArr = $pageSubjectIDArr[$i];
 			$x .= $this->SubjectPage($ReportID, $thisSubjectIDArr, $StudentID);
 		}
 		
 		return $x;
	}
	
	function SubjectPage($ReportID='', $SubjectIDArr='', $StudentID='')
	{
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
//		# Hide the subject page if all columns are exempted
//		if ($ReportID && $SubjectID)
//		{
//			$absArr = array("abs", "+", "-");
//			
//			$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
//			$MarksAry = $this->getMarks($ReportID, $StudentID, '', 0, 1, '', $SubjectID);
//			
//			$skipFlag = true;
//			foreach ($ColumnTitle as $thisColumnID => $thisTitle)
//			{
//				$thisMark = $MarksAry[$SubjectID][$thisColumnID]["Mark"];
//				$thisGrade = $MarksAry[$SubjectID][$thisColumnID]["Grade"];
//				if ( (is_numeric($thisMark) && $thisMark!=-1) )
//				{
//					if ($thisMark != 0)
//					{
//						$skipFlag = false;
//						break;
//					}
//					else
//					{
//						if (in_array($thisGrade, $absArr))
//						{
//							$skipFlag = false;
//							break;
//						}
//					}
//				}
//			}
//			
//			if ($skipFlag)
//				return "";
//		}
		
		$x = "";
		
		# Header
		$x .= $this->ReportHeader($ReportID);
		
		$numOfPageSubject = count((array)$SubjectIDArr);
		for ($i=0; $i<$numOfPageSubject; $i++) {
			$thisSubjectID = $SubjectIDArr[$i];
			
			# Student Info
			$thisHideStudentInfo = ($i==0)? false : true;
			$x .= $this->SubjectPageStudentInfo($thisSubjectID, $StudentID, $thisHideStudentInfo);
			
			# Curriculum Expectation
	 		$x .= $this->CurriculumExpectationInfo($ClassLevelID, $thisSubjectID);
	 		
			# Result
	 		$x .= $this->ResultInfo($ReportID, $thisSubjectID, $StudentID);
			
			# Personal Characteristics
			$x .= $this->PersonalCharacteristicsInfo($ClassLevelID, $thisSubjectID, $StudentID, $ReportID);
			
			# Subject Teacher Comment
			$x .= $this->ClassTeacherComment($ReportID, $StudentID, $thisSubjectID);
			
			# Teacher Sugnature
			$x .= $this->TeacherSignature($StudentID, $thisSubjectID, $ReportID);
			
			if ($i == 0) {
				$x .= '<br /><br />';
			}
		}
		
		
		# Build the whole page
		$r = "
			<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"  style=\"page-break-after: always;\">
			<tr><td>". $x ."</td><tr>
			</table>
		";
		
		return $r;
	}
	
	function ReportHeader($ReportID, $needBreak='')
	{
		global $eReportCard, $image_path, $LAYOUT_SKIN;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ReportTitle =  str_replace(":_:", "<br>", $ReportSetting['ReportTitle']);
			
		//$AcademicYear = getCurrentAcademicYear();
		$AcademicYear = $this->GET_ACTIVE_YEAR_NAME();
		//$SchoolLogo = GET_SCHOOL_BADGE();
		$SchoolLogo = "/file/reportcard2008/templates/st_stephen_logo.jpg";
		
		$x = '';
		//$x .= $needBreak ? "<P CLASS='breakhere'>&nbsp;" : "";
		$x .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' valign='top' >";
		$x .= "<tr>";
		$x .= "<td width='100' style='border-bottom: 4px solid #000000;'><img height='60' src='$SchoolLogo'></td>";
		$x .= "<td class='text19px' valign='bottom' style='border-bottom: 4px solid #000000;'>".$eReportCard['SchoolNameEn']."<br>".$eReportCard['SchoolNameCh']."</td>";
		$x .= "<td align='right' valign='bottom' style='border-bottom: 4px solid #000000;'>";
		//$x .= $AcademicYear ."<br>" . $ReportTitle;
		$x .= $ReportTitle;
		$x .= "</td>";
		$x .= "</tr>";
		//$x .= "<tr><td height='1' bgcolor='#000000' colspan='3'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='100%' height='4'></td></tr>";
		$x .= "</table>";
		
		return $x;
	}
	
	function PersonalParticularsWithPhoto($StudentID='')
	{
		global $PATH_WRT_ROOT, $eRCTemplateSetting, $eReportCard, $image_path, $LAYOUT_SKIN;
		$StudentInfoTable = "";
		
		# retrieve required variables
		$defaultVal		= "XXX";
		if($StudentID)		# retrieve Student Info
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			include_once($PATH_WRT_ROOT."includes/libclass.php");
			$lu = new libuser($StudentID);
			$lclass = new libclass();
			
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$thisClassName = $StudentInfoArr[0]['ClassName'];
			$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
			
			$data['NameEn'] 		= $lu->EnglishName;
			$data['NameCh'] 		= $lu->ChineseName;
			$data['ClassNo'] 		= $thisClassNumber; 
			$data['ClassIndexNo'] 	= $thisClassNumber; 
			$data['Class'] 			= $thisClassName;
			$data['StudentNo'] 		= $thisClassNumber;
			$data['DateOfBirth'] 	= $lu->DateOfBirth;
			$displayDate = date("dS F, Y", strtotime($data['DateOfBirth']));
			
			$data['Gender'] 		= $lu->Gender;
// 			$data['StudentAdmNo'] 	= $lu->AdmissionNo;
			$data['PhotoLink'] 		= $lu->PhotoLink;
			$data['HKID'] 			= $lu->HKID;
			$data['WebSamsRegNo'] 	= $lu->WebSamsRegNo;
			$data['STRN'] 			= $lu->STRN;
			
			

			$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
			foreach($ClassTeacherAry as $key=>$val)
			{
				$CTeacher[] = $val['CTeacher'];
			}
			$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
		}
		
		//$StudentInfoTable .= "<br>";
		$StudentInfoTable .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>";
		$StudentInfoTable .= "<tr><td>";
			$StudentInfoTable .= "<table border='0' cellpadding='0' cellspacing='0' align='left'>";
			$StudentInfoTable .= "<tr>";
			$StudentInfoTable .= "<td class='section_title' style='border-bottom: 2px solid #000000;'>".$eReportCard['Template']['PersonalParticularsEn']." ".$eReportCard['Template']['PersonalParticularsCh']."</td>";
			$StudentInfoTable .= "</tr>";
			//$StudentInfoTable .= "<tr><td height='1' bgcolor='#000000'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='100%' height='2'></td></tr>";
			$StudentInfoTable .= "</table>";
		$StudentInfoTable .= "</td></tr>";
		$StudentInfoTable .= "<tr><td >";
			$StudentInfoTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center' class='border_table'>";
			$StudentInfoTable .= "<tr>";
					$StudentInfoTable .= "<td class='report_text' colspan='2' width='60%'>".strtoupper($eReportCard['Template']['NameEn'])." ".$eReportCard['Template']['NameCh'].": ". ($StudentID ? $data['NameEn']." ".$data['NameCh'] : $defaultVal)  ."</td>";
					$StudentInfoTable .= "<td class='report_text' >".$eReportCard['Template']['SexEn']." ".$eReportCard['Template']['SexCh'].": ". ($StudentID ? $data['Gender'] : $defaultVal) ."</td>";
			/* remove the photo box on 14 Nov 2008 */
			/*
			$Photo = $data['PhotoLink'] ? "<img src='". $data['PhotoLink'] ."' style='height:130px'>" : "Photo";
			$StudentInfoTable .= "<td class='report_text' rowspan='5' bgcolor='#FFFFFF' align='center' width='130'>". $Photo ."</td>";
			*/
			$StudentInfoTable .= "</tr>";
			$StudentInfoTable .= "<tr>";
				$StudentInfoTable .= "<td class='report_text' >".strtoupper($eReportCard['Template']['ClassEn'])." ".$eReportCard['Template']['ClassCh'].": ". ($StudentID ? $data['Class'] : $defaultVal) ."</td>";
				$StudentInfoTable .= "<td class='report_text' >".$eReportCard['Template']['IDNOEn']." ".$eReportCard['Template']['IDNOCh'].": ". ($StudentID ? $data['HKID'] : $defaultVal) ."</td>";
				$StudentInfoTable .= "<td class='report_text' >".$eReportCard['Template']['STRNEn']." ".$eReportCard['Template']['STRNCh'].": ". ($StudentID ? $data['STRN'] : $defaultVal) ."</td>";
			$StudentInfoTable .= "</tr>";
			
			$StudentInfoTable .= "<tr>";
				$StudentInfoTable .= "<td class='report_text' colspan='3' >".$eReportCard['Template']['DOBEn']." ".$eReportCard['Template']['DOBCh'].": ". ($StudentID ? $displayDate : $defaultVal) ."</td>";
			$StudentInfoTable .= "</tr>";
			
			$StudentInfoTable .= "</table>";
		$StudentInfoTable .= "</td></tr>";
		$StudentInfoTable .= "</table>";
		
		return $StudentInfoTable;
	}
	
	function ClassTeacherPrincipal($StudentID='')
	{
		global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $eReportCard;
		
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		$lu = new libuser($StudentID);
		$lclass = new libclass();
		
		$ClassTeacherAry = $lclass->returnClassTeacher($lu->ClassName);
		foreach($ClassTeacherAry as $key=>$val)
		{
			$CTeacher[] = $val['CTeacher'];
		}
		//$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
		
		if ($CTeacher == NULL) 
			$CTeacher[] = "--";
		
		$x .= "<br>";
		$x .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
			$x .= "<tr>";
				$x .= "<td class='section_title' width='10%' nowrap>".$eReportCard['Template']['ClassTeacherEn']." ".$eReportCard['Template']['ClassTeacherCh'].": &nbsp;</td>";
				$x .= "<td class='section_title' align='left'>". $CTeacher[0] ."</td>";
				$x .= "<td class='section_title' align='right'>".$eReportCard['Template']['PrincipalEn']." ".$eReportCard['Template']['PrincipalCh'].": ".$eReportCard['PrincipalName']."</td>";
			$x .= "</tr>";
			if (sizeof($CTeacher) > 1)
			{
				for ($i=1; $i<sizeof($CTeacher); $i++)
				{
					$x .= "<tr>";
						$x .= "<td>&nbsp;</td>";
						$x .= "<td class='section_title' align='left'>". $CTeacher[$i] ."</td>";
						$x .= "<td>&nbsp;</td>";
					$x .= "</tr>";	
				}
			}
			//$x .= "<tr><td height='1' colspan='3' bgcolor='#000000'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='100%' height='2'></td></tr>";
			$x .= "<tr><td height='1' colspan='3' style='border-bottom: 2px solid #000000;'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='100%' height='0'></td></tr>";
		$x .= "</table>";
		
		return $x;
	}
	
	function ExplanatoryNotes($ReportID='')
	{
		global $eReportCard;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$Semester = $ReportSetting['Semester'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		$TermSequence = $this->Get_Semester_Seq_Number($Semester);
		
		// for testing
//		$Semester = 1;
//		$FormNumber = 1;
		
		$x .= "<br>";
		$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center' class='border_table'>";
		$x .= "<tr>";
		$x .= "<td width='60%' align='center'>".$eReportCard['Template']['ExplanatoryNotesEn']."</td>";
		$x .= "<td width='40%' align='center'>".$eReportCard['Template']['ExplanatoryNotesCh']."</td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width='60%'>";
			$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' class='explanatory_text no_border_table'>";
			$x .= "<tr>";
			$x .= "<td nowrap valign='top'>Pass Mark</td>";
			$x .= "<td width='3' valign='top'>:</td>";
			$x .= "<td width='100%'>It is 50% of the full mark.</td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td nowrap valign='top'>Term Average</td>";
			$x .= "<td width='3' valign='top'>:</td>";
			
			//2013-1018-1506-12066
			//$x .= "<td width='100%'>It is a weighted average of three assessments, namely, 25% from Mid-Term Test Mark, 25% from Class Mark and 50% from Exam Mark.</td>";
			if ($FormNumber <= 3 && $TermSequence == 1) {
				$x .= "<td width='100%'>It is a weighted average of two assessments, namely, 25% from Class Mark and 75% from Exam Mark.</td>";
			}
			else {
				$x .= "<td width='100%'>It is a weighted average of three assessments, namely, 25% from Mid-Term Test Mark, 25% from Class Mark and 50% from Exam Mark.</td>";
			}
			
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td nowrap valign='top'>Rank</td>";
			$x .= "<td width='3' valign='top'>:</td>";
			$x .= "<td width='100%'>There are six ranks: A*, A, B, C, D, E. The highest rank is A*. A* and A consist of 10% of all candidates in the form/class, while each of the other ranks 20% of all candidates in the form/class.</td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td nowrap valign='top'>Conduct</td>";
			$x .= "<td width='3' valign='top'>:</td>";
			$x .= "<td width='100%'>There are four grades: “Excellent”, “Good”, “Satisfactory” and “Needs Improvement”. It is assessed on the basis of the College’s core values which include personal integrity, cooperativeness, perseverance, sense of responsibility, etc.</td>";
			$x .= "</tr>";
			$x .= "</table>";
		$x .= "</td>";
		$x .= "<td width='40%'>";
			$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' class='explanatory_text no_border_table'>";
			$x .= "<tr>";
			$x .= "<td nowrap valign='top'>及格分</td>";
			$x .= "<td width='3' valign='top'>:</td>";
			$x .= "<td width='100%'>及格成績為滿分之50%</td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td nowrap valign='top'>平均分</td>";
			$x .= "<td width='3' valign='top'>:</td>";
			
			//2013-1018-1506-12066
			//$x .= "<td width='100%'>平均分由三項考績組成，期中測驗分佔25%，平時分佔25%，考試分佔50%。</td>";
			if ($FormNumber <= 3 && $TermSequence == 1) {
				$x .= "<td width='100%'>平均分由兩項考績組成，平時分佔25%，考試分佔75%。</td>";
			}
			else {
				$x .= "<td width='100%'>平均分由三項考績組成，期中測驗分佔25%，平時分佔25%，考試分佔50%。</td>";
			}
			
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td nowrap valign='top'>等第</td>";
			$x .= "<td width='3' valign='top'>:</td>";
			$x .= "<td width='100%'>共分六等，由高至低依次為A*, A, B, C, D 及 E。 A*及A兩等各佔考生人數10%，其餘每等佔20%。</td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td nowrap valign='top'>操行</td>";
			$x .= "<td width='3' valign='top'>:</td>";
			$x .= "<td width='100%'>共分“優”、 “良”、 “尚可”及“有待改進”四等。評核以本校核心價值為基礎，包括誠信、合作精神、堅毅、責任感等。</td>";
			$x .= "</tr>";
			$x .= "</table>";
		$x .= "</td>";
		$x .= "</tr>";
		
		$x .= "</table>";
		
		$x .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='text9px'>";
		$x .= "<tr>";
		$x .= "<td>22 Tung Tau Wan Road</td>";
		$x .= "<td>香港　　赤柱</td>";
		$x .= "<td>Telephone 電話:(852)2813-0360</td>";
		
		//2013-1018-1506-12066
		//$x .= "<td align='right'>This report system has been in use since September, 2009.</td>";
		if ($FormNumber <= 3) {
			$x .= "<td align='right'>This report system has been in use since September, 2013.</td>";
		}
		else {
			$x .= "<td align='right'>This report system has been in use since September, 2009 .</td>";
		}
		$x .= "</tr>"; 
		$x .= "<tr>";
		$x .= "<td>Stanley Hong Kong</td>";
		$x .= "<td>東頭灣道22號</td>";
		$x .= "<td>Facsimile 傅真:(852)2813-7311</td>";
		
		//2013-1018-1506-12066
		//$x .= "<td align='right'>此成績表系統自二零零九年九月起用。</td>";
		if ($FormNumber <= 3) {
			$x .= "<td align='right'>此成績表系統自二零一三年九月起用。</td>";
		}
		else {
			$x .= "<td align='right'>此成績表系統自二零零九年九月起用。</td>";
		}
		
		$x .= "</tr>";
		$x .= "</table>";
		
		return $x;
	}	
	
	function OverallAcademicResult($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $image_path, $LAYOUT_SKIN;		
		$defaultVal		= "XXX";
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		# Retrieve Subject Scheme ID
		$GrandMarkSubjectID = -1;
		$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $GrandMarkSubjectID, 1 ,0, $ReportID );
 		$SchemeID = $SubjectFormGradingSettings[SchemeID];
 		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID);
		$GrandTotal = $StudentID ? $result['GrandTotal'] : "S";
		$AverageMark = $StudentID ? $result['GrandAverage'] : "S";
		
 		$FormPosition = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? "--": $result['OrderMeritForm']) : "--") : "#";
  		$ClassPosition = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? "--": $result['OrderMeritClass']) : "--") : "#";
  		$ClassNumOfStudent = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? "--": $result['ClassNoOfStudent']) : "--") : "#";
  		$FormNumOfStudent = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? "--": $result['FormNoOfStudent']) : "--") : "#";
  		
//  		$RankingGrade = $this->Get_GrandRanking_Grade($ReportID, $StudentID);
  		$RankingGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $AverageMark, $ReportID, $StudentID, $GrandMarkSubjectID, $ClassLevelID, $ReportColumnID=0);
  		
  		# Academic Results
		$x = "<br>";
		$x .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>";
		$x .= "<tr><td>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0' align='left'>";
			$x .= "<tr>";
			$x .= "<td class='section_title' style='border-bottom: 2px solid #000000;'>".$eReportCard['Template']['AcademicResultsEn']." ".$eReportCard['Template']['AcademicResultsCh']."</td>";
			$x .= "</tr>";
			//$x .= "<tr><td height='1' bgcolor='#000000'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='100%' height='2'></td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center' class='border_table'>";
			$x .= "<tr>";
				$x .= "<td >";
					$x .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='no_border_table'>";
					$x .= "<tr><td class='report_text'>".$eReportCard['Template']['StudentAverageEn']."</td><td class='report_text' align='right'>".$eReportCard['Template']['StudentAverageCh']."</td></tr>";
					$x .= "</table>";
				$x .= "</td>";
				$x .= "<td class='report_text' width='130' align='center'>". ($StudentID ? $AverageMark : $defaultVal) ."</td>";
			$x .= "</tr>";	
			$x .= "<tr>";
				$x .= "<td >";
					$x .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='no_border_table'>";
					$x .= "<tr><td class='report_text'>".$eReportCard['Template']['RankInFormEn']."</td><td class='report_text' align='right'>".$eReportCard['Template']['RankInFormCh']."</td></tr>";
					$x .= "</table>";
				$x .= "</td>";
				$x .= "<td class='report_text' width='130' align='center'>". ($StudentID ? $RankingGrade : $defaultVal) ."</td>";
			$x .= "</tr>";	
			$x .= "</table>";
		$x .= "</td></tr>";
		$x .= "</table>";
		
		return $x;
	}
	
	function CSVData($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $image_path, $LAYOUT_SKIN;
		
		include_once($PATH_WRT_ROOT.'includes/libreportcard2008_extrainfo.php');
		$lreportcard_extrainfo = new libreportcard_extrainfo();
		
		$defaultVal		= "XXX";
		// getOtherInfoData($UploadType, $Term, $ClassName) 
			
		# Retrieve Semester
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$latestTerm = $ReportSetting['Semester'];
		$SchoolDays = $ReportSetting['AttendanceDays'];
		//$latestTerm++;
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
			
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$ClassID = $StudentInfoArr[0]['ClassID'];
		}
		
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		
		foreach($csvType as $k=>$Type)
		{
			if ($ReportSetting['Semester'] == "F") {
				$csvData = $this->getOtherInfoData($Type, 0, $ClassID);
				if(empty($csvData)) {
					$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassID);
				}
			} else {
				$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassID);
			}
			
			if(!empty($csvData)) 
			{
				foreach($csvData as $RegNo=>$data)
				{
					//if($RegNo == $WebSamsRegNo)
					if($RegNo == $StudentID)
					{
	 					foreach($data as $key=>$val)
		 					$ary[$Type][$key] = $val;
					}
				}
			}
		}
		
		# Data
		//$SchoolDays = $ary['attendance']['Total Number of School Days'];
		//$DaysAbsent = $ary['attendance']['Days Absent'];
		//$TimeLate = $ary['attendance']['Time Late'];
		$Merits = $ary['merit']['Merits'];
		$MeritList = $ary["merit"]["On Principal's Merit List"];
		$MinorBreaches = $ary['demerit']['Minor Breaches'];
		$MajorBreaches = $ary['demerit']['Major Breaches'];
		$comment2 = $ary['additional_comments']['Comment'];
		//$Conduct = $ary['summary']['Conduct'];
		
		$ExtraInfoArr = $lreportcard_extrainfo->Get_Student_Extra_Info($ReportID, $StudentID);
		$ConductID = $ExtraInfoArr[0]['ConductID'];
		$ConductInfoArr = $lreportcard_extrainfo->Get_Conduct($ConductID);
		$Conduct = $ConductInfoArr[0]['Conduct'];
		
		$AwardNameAry = $ary['award']['Award Name'];
		$AwardAry = $ary['award']['Award'];
		
		$AwardTable = "";
		if (is_array($AwardNameAry))
		{
			for($i=0;$i<sizeof($AwardNameAry);$i++)
			{
				$AwardTable .= $AwardNameAry[$i];
				if(trim($AwardAry[$i]))
					$AwardTable .= " - " .$AwardAry[$i];
				$AwardTable .= "<br>";
			}
		}
		else
		{
			$AwardTable .= $AwardNameAry;
			if(trim($AwardAry))
				$AwardTable .= " - " .$AwardAry;
		}
		
		$AwardTable = $AwardTable ? $AwardTable : "---";
		/*
		
		if(sizeof($AwardNameAry))
		{
			$AwardTable = "<table border=0 width='100%' cellpadding='2' cellspacing='0'>";
			for($i=0;$i<sizeof($AwardNameAry);$i++)
			{
				$AwardTable .= "<tr>";
				$AwardTable .= "<td class='report_text'>". $AwardNameAry[$i] ."</td>";
				$AwardTable .= "<td class='report_text' width='200'>". $AwardAry[$i] ."</td>";
				$AwardTable .= "</tr>";
			}
			$AwardTable .= "</table>";
		}
		*/
		
		# build eca data
//		$ecaPositionAry = $ary['eca']['Position'];
//		$ecaAry = $ary['eca']['ECA'];
//		$ecaCommentAry = $ary['eca']['Comment'];
//		
//		$ecaTable = "---";
//		if(sizeof($ecaPositionAry))
//		{
//			$ecaTable = "<table border=0 width='100%' cellpadding='2' cellspacing='0'>";
//			if(sizeof($ecaAry)==1)
//			{
//				$ecaTable .= "<tr>";
//				$ecaTable .= "<td class='report_text'>". $ecaPositionAry ." of " . $ecaAry;
//				if(trim($ecaCommentAry))
//					$ecaTable .= " - " . $ecaCommentAry;
//				$ecaTable .= "</td>";
//				$ecaTable .= "</tr>";
//			}
//			else
//			{
//				for($i=0;$i<sizeof($ecaAry);$i++)
//				{
//					$ecaTable .= "<tr>";
//					$ecaTable .= "<td class='report_text'>". $ecaPositionAry[$i] ." of " . $ecaAry[$i];
//					if(trim($ecaCommentAry[$i]))
//						$ecaTable .= " - " . $ecaCommentAry[$i];
//					$ecaTable .= "</td>";
//					$ecaTable .= "</tr>";
//				}
//			}
//			$ecaTable .= "</table>";
//		}

		// retrieve ECA data from eEnrolment
		$EcaInfoArr = $this->Get_eEnrolment_Data($StudentID, '', $includeActivity=true);
		$numOfEca = count($EcaInfoArr);
		$ecaTable = '';
		if ($numOfEca == 0) {
			$ecaTable = '---';
		}
		else {
			$ecaTable .= "<table border=0 width='100%' cellpadding='2' cellspacing='0'>";
				for ($i=0; $i<$numOfEca; $i++) {
					$thisEcaTitle = trim($EcaInfoArr[$i]['ClubTitle']);
					$thisPerformance = trim($EcaInfoArr[$i]['Performance']);
					$thisRoleTitle = trim($EcaInfoArr[$i]['RoleTitle']);
					
					$thisDisplay = '';
					// 2012-0321-1414-21066
					// [Role] of [Name of the activity] – [Performance]
					$thisDisplay .= $thisRoleTitle.' of ';
					$thisDisplay .= $thisEcaTitle;
					if ($thisPerformance != '') {
						$thisDisplay .= ' - '.$thisPerformance;
					}
					
					$ecaTable .= "<tr>";
						$ecaTable .= "<td class='report_text'>".$thisDisplay."</td>";
					$ecaTable .= "</tr>";
				}
			$ecaTable .= "</table>";
		}
		

		
		# set to 0 if empty value
		// retrieve attendance data from Student Profile
		$AttendanceInfoArr = $this->Get_Student_Profile_Attendance_Data($ReportSetting['Semester'], $ClassID);
		$DaysAbsent = $AttendanceInfoArr[$StudentID]["Days Absent"];
		$TimeLate = $AttendanceInfoArr[$StudentID]["Time Late"]; 
		
		$SchoolDays = $SchoolDays ? $SchoolDays : 0;
		$DaysAbsent = $DaysAbsent ? $DaysAbsent : 0;
		$TimeLate = $TimeLate ? $TimeLate : 0;
		$Merits = $Merits ? $Merits : "---";
		$MeritList = $MeritList ? $MeritList : "---";
		$MinorBreaches = $MinorBreaches ? $MinorBreaches : "---";
		$MajorBreaches = $MajorBreaches ? $MajorBreaches : "---";
		$Conduct = $Conduct ? $Conduct : "---";
		
		
		# Attendance
		$x = "<br>";
		$x .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>";
		$x .= "<tr><td>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0' align='left'>";
			$x .= "<tr>";
			$x .= "<td class='section_title' style='border-bottom: 2px solid #000000;'>".$eReportCard['Template']['AttendanceEn']." ".$eReportCard['Template']['AttendanceCh']."</td>";
			$x .= "</tr>";
			//$x .= "<tr><td height='1' style='border-bottom: 2px solid #000000;'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='100%' height='2'></td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'  class='border_table'>";
			$x .= "<tr>";
				$x .= "<td>";
					$x .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='no_border_table'>";
					$x .= "<tr><td class='report_text'>".$eReportCard['Template']['DaysAbsentEn']." / ".$eReportCard['Template']['TotalNumberOfSchoolDaysEn']."</td><td class='report_text' align='right'>".$eReportCard['Template']['DaysAbsentCh']."/".$eReportCard['Template']['TotalNumberOfSchoolDaysCh']."</td></tr>";
					$x .= "</table>";
				$x .= "</td>";
				$x .= "<td class='report_text' width='130' align='center'>". ($StudentID ? $DaysAbsent."/".$SchoolDays : $defaultVal) ."</td>";
			$x .= "</tr>";	
			$x .= "<tr>";
				$x .= "<td >";
					$x .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='no_border_table'>";
					$x .= "<tr><td class='report_text'>".$eReportCard['Template']['TimesLateEn']."</td><td class='report_text' align='right'>".$eReportCard['Template']['TimesLateCh']."</td></tr>";
					$x .= "</table>";
				$x .= "</td>";
				$x .= "<td class='report_text' width='130' align='center'>". ($StudentID ? $TimeLate : $defaultVal) ."</td>";
			$x .= "</tr>";	
			$x .= "</table>";
		$x .= "</td></tr>";
		$x .= "</table>";
		
		# Merits
		$meritTable .= "<table width='100%' height='100%' border='0' cellpadding='1' cellspacing='0' align='center' valign='top'>";
		$meritTable .= "<tr><td>";
			$meritTable .= "<table border='0' cellpadding='0' cellspacing='0' align='left'>";
			$meritTable .= "<tr>";
			$meritTable .= "<td class='section_title' style='border-bottom: 2px solid #000000;'>".$eReportCard['Template']['MeritEn']." ".$eReportCard['Template']['MeritCh']."</td>";
			$meritTable .= "</tr>";
			//$meritTable .= "<tr><td height='1' style='border-bottom: 2px solid #000000;'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='100%' height='2'></td></tr>";
			$meritTable .= "</table>";
		$meritTable .= "</td></tr>";
		$meritTable .= "<tr><td>";
			$meritTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center' class='border_table'>";
			$meritTable .= "<tr>";
				$meritTable .= "<td >";
					$meritTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='no_border_table'>";
					$meritTable .= "<tr><td class='report_text'>".$eReportCard['Template']['MeritCountEn']."</td><td class='report_text' align='right'>".$eReportCard['Template']['MeritCountCh']."</td></tr>";
					$meritTable .= "</table>";
				$meritTable .= "</td>";
				$thisDisplay = ($StudentID)? $Merits : $defaultVal;
				$thisDisplay = ($thisDisplay == "")? "---" : $thisDisplay;
				$meritTable .= "<td class='report_text' width='130' align='center'>". $thisDisplay ."</td>";
			$meritTable .= "</tr>";	
			$meritTable .= "<tr>";
				$meritTable .= "<td >";
					$meritTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='no_border_table'>";
					$meritTable .= "<tr><td class='report_text'>".$eReportCard['Template']['PrincipalMeritListEn']."</td><td class='report_text' align='right'>".$eReportCard['Template']['PrincipalMeritListCh']."</td></tr>";
					$meritTable .= "</table>";
				$meritTable .= "</td>";
				$thisDisplay = ($StudentID)? $MeritList : $defaultVal;
				$thisDisplay = ($thisDisplay == "")? "---" : $thisDisplay;
				$meritTable .= "<td class='report_text' width='130' align='center'>". $thisDisplay ."</td>";
			$meritTable .= "</tr>";	
			$meritTable .= "</table>";
		$meritTable .= "</td></tr>";
		$meritTable .= "</table>";
		
		# Annual Demerits
		$demeritTable .= "<table width='100%' height='100%' border='0' cellpadding='1' cellspacing='0' align='center' valign='top'>";
		$demeritTable .= "<tr><td>";
			$demeritTable .= "<table border='0' cellpadding='0' cellspacing='0' align='left'>";
			$demeritTable .= "<tr>";
			$demeritTable .= "<td class='section_title' style='border-bottom: 2px solid #000000;'>".$eReportCard['Template']['DemeritsEn']." ".$eReportCard['Template']['DemeritsCh']."</td>";
			$demeritTable .= "</tr>";
			//$demeritTable .= "<tr><td height='1' style='border-bottom: 2px solid #000000;'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='100%' height='2'></td></tr>";
			$demeritTable .= "</table>";
		$demeritTable .= "</td></tr>";
		$demeritTable .= "<tr><td>";
			$demeritTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center' class='border_table'>";
			$demeritTable .= "<tr>";
				$demeritTable .= "<td >";
					$demeritTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='no_border_table'>";
					$demeritTable .= "<tr><td class='report_text'>".$eReportCard['Template']['MinorBreachesEn']."</td><td class='report_text' align='right'>".$eReportCard['Template']['MinorBreachesCh']."</td></tr>";
					$demeritTable .= "</table>";
				$demeritTable .= "</td>";
				$thisDisplay = ($StudentID)? $MinorBreaches : $defaultVal;
				$thisDisplay = ($thisDisplay == "")? "---" : $thisDisplay;
				$demeritTable .= "<td class='report_text' width='130' align='center'>". $thisDisplay ."</td>";
			$demeritTable .= "</tr>";	
			$demeritTable .= "<tr>";
				$demeritTable .= "<td >";
					$demeritTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='no_border_table'>";
					$demeritTable .= "<tr><td class='report_text'>".$eReportCard['Template']['MajorBreachesEn']."</td><td class='report_text' align='right'>".$eReportCard['Template']['MajorBreachesCh']."</td></tr>";
					$demeritTable .= "</table>";
				$demeritTable .= "</td>";
				$thisDisplay = ($StudentID)? $MajorBreaches : $defaultVal;
				$thisDisplay = ($thisDisplay == "")? "---" : $thisDisplay;
				$demeritTable .= "<td class='report_text' width='130' align='center'>". $thisDisplay ."</td>";
			$demeritTable .= "</tr>";	
			$demeritTable .= "</table>";
		$demeritTable .= "</td></tr>";
		$demeritTable .= "</table>";
		
		$x .= "<br>";
		$x .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>";
			$x .= "<tr>";
				$x .= "<td width='48%'>".$meritTable."</td>";
				$x .= "<td>&nbsp;</td>";
				$x .= "<td width='48%'>".$demeritTable."</td>";
			$x .= "</tr>";
		$x .= "</table>";
		
		# Conduct
		if(!$StudentID)
			$conduct = "XXXXX";
			
		$x .= "<br>";
			$x .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>";
			$x .= "<tr><td>";
				$x .= "<table border='0' cellpadding='0' cellspacing='0' align='left'>";
				$x .= "<tr>";
				$x .= "<td class='section_title' style='border-bottom: 2px solid #000000;'>".$eReportCard['Template']['ConductEn']." ".$eReportCard['Template']['ConductCh']."</td>";
				$x .= "</tr>";
				//$x .= "<tr><td height='1' style='border-bottom: 2px solid #000000;'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='100%' height='2'></td></tr>";
				$x .= "</table>";
			$x .= "</td></tr>";
			$x .= "<tr><td class='report_text'>". ($StudentID ? $Conduct : $conduct) ."</td></tr>";
			$x .= "</table>";
				
		# Awards & Major Achievements
		if(!$StudentID)
			$awards = "XXXXX XXXXX XXXXX<br>XXXXX XXXXX XXXXX";
			
		$x .= "<br>";
			$x .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>";
			$x .= "<tr><td>";
				$x .= "<table border='0' cellpadding='0' cellspacing='0' align='left'>";
				$x .= "<tr>";
				$x .= "<td class='section_title' style='border-bottom: 2px solid #000000;'>".$eReportCard['Template']['Awards&MajorAchievementsEn']." ".$eReportCard['Template']['Awards&MajorAchievementsCh']."</td>";
				$x .= "</tr>";
				//$x .= "<tr><td height='1' style='border-bottom: 2px solid #000000;'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='100%' height='2'></td></tr>";
				$x .= "</table>";
			$x .= "</td></tr>";
			$x .= "<tr><td class='report_text'>". ($StudentID ? $AwardTable : nl2br($awards)) ."</td></tr>";
			$x .= "</table>";
		
		# ECA
		if(!$StudentID)
			$eca = "XXXXX XXXXX XXXXX<br>XXXXX XXXXX XXXXX";
			
		$x .= "<br>";
			$x .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>";
			$x .= "<tr><td>";
				$x .= "<table border='0' cellpadding='0' cellspacing='0' align='left'>";
				$x .= "<tr>";
				$x .= "<td class='section_title' style='border-bottom: 2px solid #000000;'>".$eReportCard['Template']['EcaEn']." ".$eReportCard['Template']['EcaCh']."</td>";
				$x .= "</tr>";
				//$x .= "<tr><td height='1' style='border-bottom: 2px solid #000000;'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='100%' height='2'></td></tr>";
				$x .= "</table>";
			$x .= "</td></tr>";
			$x .= "<tr><td class='report_text'>". ($StudentID ? $ecaTable : nl2br($eca)) ."</td></tr>";
			$x .= "</table>";
	
		return $x;
	}
	
	function ClassTeacherComment($ReportID='', $StudentID='', $SubjectID='', $ShowAdditionalComment=0)
	{
		global $PATH_WRT_ROOT, $eReportCard, $image_path, $LAYOUT_SKIN;
		
		# retrieve data
		# student name
		if ($StudentID)
			$lu = new libuser($StudentID);
		
		$defaultVal = "XXX";
			
		$StudentName = ($StudentID)? $lu->EnglishName : $defaultVal;
			
		# teacher's comment
		$TComment = "";
		
		if(!$StudentID)
			$TComment = "XXXXX XXXXX XXXXX<br>XXXXX XXXXX XXXXX";
		else
		{
			$TeacherCommentAry = $this->GET_TEACHER_COMMENT(array($StudentID), $SubjectID, $ReportID);
			foreach($TeacherCommentAry as $k => $ary)
				$TComment .= $ary[Comment];
		}
		
		$TComment = $TComment ? stripslashes($TComment) : "---";	
		$TCommentArr = explode("\n", $TComment);
		
			$x = "<br>";
			$x .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>";
			$x .= "<tr><td>";
				$x .= "<table border='0' cellpadding='0' cellspacing='0' align='left'>";
				$x .= "<tr>";
				$x .= "<td class='section_title' style='border-bottom: 2px solid #000000;'>". ($SubjectID ? "" : "Class "). $eReportCard['Template']['TeacherCommentsEn']." ".$eReportCard['Template']['TeacherCommentsCh']."</td>";
				$x .= "</tr>";
				//$x .= "<tr><td height='1' style='border-bottom: 2px solid #000000;'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='100%' height='2'></td></tr>";
				$x .= "</table>";
			$x .= "</td></tr>";
			$x .= "<tr><td class='report_text'>".$StudentName."</td></tr>";
			$x .= "<tr><td class='report_text'>";
			foreach ($TCommentArr as $key => $comment)
			{
				$comment = trim($comment);
				if ($comment != "")
				{
					// convert the 1st letter to lower case
					$convertedComment = strtolower( substr($comment,0,1) ) . substr($comment,1); 
					$x .= "<li>".$convertedComment."\n";
				}
			}
			$x .= "</td></tr>";
			$x .= "</table>";
			
		if ($ShowAdditionalComment)
		{
			# Additional Comments
			$defaultVal		= "XXX";
				
			# Retrieve Semester
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			if ($ReportSetting['Semester'] == "F")
			{
				$latestTerm = 0;
			}
			else
			{
				$latestTerm = $ReportSetting['Semester'];
				//$latestTerm++;
			}
			
			# retrieve Student Class
			/*
			if($StudentID)
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				$lu = new libuser($StudentID);
				$ClassName 		= $lu->ClassName;
				$WebSamsRegNo 	= $lu->WebSamsRegNo;
			}
			
			# build data array
			$ary = array();
			$csvType = $this->getOtherInfoType();
			
			foreach($csvType as $k=>$Type)
			{
				if ($BasicInfo['Semester'] == "F") {
					$csvData = $this->getOtherInfoData($Type, 0, $ClassName);
					if(empty($csvData)) {
						$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);
					}
				} else {
					$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);
				}
				
				if(!empty($csvData)) 
				{
					foreach($csvData as $RegNo=>$data)
					{
						if($RegNo == $WebSamsRegNo)
						{
		 					foreach($data as $key=>$val)
			 					$ary[$Type][$key] = $val;
						}
					}
				}
			}
			*/
			
			// 2012-0717-0955-58066
			//$OtherInfoArr = $this->getReportOtherInfoData($ReportID, $StudentID, $ClassID='', $UploadType='additional_comments', $ArrayWithType=1);
			//$ary = $OtherInfoArr[$StudentID][$latestTerm];
			
			# Data
			//$comment2 = $ary['additional_comments']['Comment'];
			$comment2 = $ary['AdditionalComment'];
			
			if($StudentID && $comment2)
			{			
				$x .= "<br>";
				$x .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>";
				$x .= "<tr><td>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0' align='left'>";
					$x .= "<tr>";
					$x .= "<td class='section_title' style='border-bottom: 2px solid #000000;'>".$eReportCard['Template']['AdditionalCommentsEn']." ".$eReportCard['Template']['AdditionalCommentsCh']."</td>";
					$x .= "</tr>";
					//$x .= "<tr><td height='1' style='border-bottom: 2px solid #000000;'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='100%' height='2'></td></tr>";
					$x .= "</table>";
				$x .= "</td></tr>";
				$x .= "<tr><td class='report_text'>". nl2br($comment2) ."</td></tr>";
				$x .= "</table>";
			}
		}
			
		return $x;
		
	}
	
	function SubjectPageStudentInfo($SubjectID='', $StudentID='', $HideStudentInfo=false)
	{
		global $PATH_WRT_ROOT, $eReportCard;
		
		if($StudentID)		# retrieve Student Info
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			
			$data['NameEn']	= $lu->EnglishName;
			$data['NameCh']	= $lu->ChineseName;
			$data['Class'] 	= $lu->ClassName;
			
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$data['Class'] = $StudentInfoArr[0]['ClassName'];
		}
		
		$defaultVal		= "XXX";
		
		$x .= "<br>";
		$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'  class='border_table'>";
			if (!$HideStudentInfo) {
				$x .= "<tr>";
					$x .= "<td class='report_text' width='100'>".ucfirst($eReportCard['Template']['NameEn'])." ".$eReportCard['Template']['NameCh']."</td>";
					$x .= "<td class='report_text'>". ($StudentID ? $data['NameEn']  : $defaultVal) ."</td>";
					$x .= "<td class='report_text' width='100'>".ucfirst($eReportCard['Template']['ClassEn'])." ".$eReportCard['Template']['ClassCh']."</td>";
					$x .= "<td class='report_text' width='150'>". ($StudentID ? $data['Class'] : $defaultVal) ."</td>";
				$x .= "</tr>";
			}
		
			$x .= "<tr>";
				$x .= "<td class='report_text' >".$eReportCard['Template']['SubjectEn']." ".$eReportCard['Template']['SubjectCh']."</td>";
				$x .= "<td class='report_text' colspan='3'>". $this->GET_SUBJECT_NAME_LANG($SubjectID) ."</td>";
			$x .= "</tr>";
		$x .= "</table>";
		
		return $x;
	}
	
	function CurriculumExpectationInfo($ClassLevelID, $SubjectID)
	{
		global $eReportCard, $image_path, $LAYOUT_SKIN;
		
		$ce = $this->returnCurriculumExpectation($ClassLevelID, $SubjectID);
		
		$expectation = $ce[$SubjectID] ? nl2br($ce[$SubjectID]) : "---";
		
		$x .= "<br />";
		$x .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>";
		$x .= "<tr><td>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0' align='left'>";
			$x .= "<tr>";
			$x .= "<td class='section_title' style='border-bottom: 2px solid #000000;'>".$eReportCard['Template']['CourseDescriptionEn']." ".$eReportCard['Template']['CourseDescriptionCh']."</td>";
			$x .= "</tr>";
			//$x .= "<tr><td height='1' style='border-bottom: 2px solid #000000;'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='100%' height='2'></td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		$x .= "<tr><td class='report_text'>". $expectation ."</td></tr>";
		$x .= "</table>";
		
		return $x;
	}
	
	function TeacherSignature($StudentID, $SubjectID, $ReportID)
	{
		global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $eReportCard;
		
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		//$lu = new libuser($StudentID);
		//$lclass = new libclass();
		//$ClassName 		= $lu->ClassName;
		//$ClassID = $lclass->getClassID($ClassName);
		
		$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
		$ClassID = $StudentInfoArr[0]['ClassID'];
		
		$ary = $this->returnSubjectTeacher($ClassID, $SubjectID, $ReportID, $StudentID);
		$subject_teacher_list = implode("<br>",array_remove_empty($ary));
		
		//$x .= "<br/><br/><br/><br/><br/>";
		$x .= "<br/><br/>";
		$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>";
		$x .= "<tr>";
		$x .= "<td class='section_title' align='right' valign='top'>".$eReportCard['Template']['SubjectTeacherEn']." ".$eReportCard['Template']['SubjectTeacherCh'].": </td>";
		$x .= "<td class='section_title' align='left'  valign='top' width='300'>". $subject_teacher_list ."</td>";
		$x .= "</tr>";
		
		$x .= "</table>";
		
		return $x;
	}
	
	function PersonalCharacteristicsInfo($ClassLevelID, $SubjectID, $StudentID, $ReportID)
	{
		global $eReportCard, $image_path, $LAYOUT_SKIN;
		
		$pcData = $this->returnPersonalCharSettingData($ClassLevelID, $SubjectID);
		$ScalePoint = $this->returnPersonalCharOptions(1, 1);
		$TempResult = $this->getPersonalCharacteristicsData($StudentID, $ReportID, $SubjectID);
		
		# Build data array
		$result = $TempResult[0][0];
		$resultRow = split(",", $result);
		$resultAry = array();
		foreach($resultRow as $key=>$data)
		{
			list($PCID, $PCPoint) = split(":", $data);
			$resultAry[$PCID] = $PCPoint;
		}
		
		if (count($pcData) == 0)
			return "";
			
			$x = "<br>";
			$x .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>";
			$x .= "<tr><td>";
				$x .= "<table border='0' cellpadding='0' cellspacing='0' align='left'>";
				$x .= "<tr>";
				$x .= "<td class='section_title' style='border-bottom: 2px solid #000000;'>".$eReportCard['Template']['PersonalCharacteristicsEn']." ".$eReportCard['Template']['PersonalCharacteristicsCh']."</td>";
				$x .= "</tr>";
				//$x .= "<tr><td height='1' style='border-bottom: 2px solid #000000;'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='100%' height='2'></td></tr>";
				$x .= "</table>";
			$x .= "</td></tr>";
			$x .= "<tr><td>";
				$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0'>";
				$x .= "<tr>";
				$x .= "<td class='table_right_bottom'>&nbsp;</td>";
				
				foreach($ScalePoint as $k=>$d)
					$x .= "<td width='80' align='center' valign='top' class='table_right_bottom table_top report_text'>". $d ."</td>";
				$x .= "</tr>";
				
				foreach($pcData as $k1=>$d1)
				{
					list($thisClassLevelID, $thisSubjectID, $thisCharID, $thisTitle_EN, $thisTitle_CH) = $d1;
					$x .= "<tr>";
					$x .= "<td class='table_right_bottom table_left'>";
						$x .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr>
							<td class='report_text'>". $thisTitle_EN ."</td>
							<td class='report_text' align='right'>". $thisTitle_CH ."</td>
							</tr>";
						$x .= "</table>";
					$x .= "</td>";
					
					foreach($ScalePoint as $k2=>$d2)
					{
						$thisData = $resultAry[$thisCharID]==$k2 ? "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_tick_black.gif'>" : "&nbsp;";
						$x .= "<td align='center' class='table_right_bottom report_text'>$thisData</td>";
					}
					
					$x .= "</tr>";
				}
				$x .= "</table>";
			$x .= "</td></tr>";
			$x .= "</table>";
		
		return $x;
	}
	
	function ResultInfo($ReportID, $SubjectID, $StudentID='')
	{
		global $eReportCard, $image_path, $LAYOUT_SKIN;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$SemID = $ReportSetting['Semester'];
 		$ReportType = $SemID == "F" ? "W" : "T";
 		
		# Retrieve Subject Scheme ID
		$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
 		$SchemeID = $SubjectFormGradingSettings[SchemeID];
		$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
		
		$SubjectsAry = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		$thisSubject = $SubjectsAry[$SubjectID];
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		
		//$MarksAry = $this->getMarks($ReportID, $StudentID, ' and SubjectID='.$SubjectID);
		$Grand = $this->getReportResultScore($ReportID, $ReportColumnID=0, $StudentID);

		# Subject weight
		// get subject weight info of the column
  		if($ReportType=="T")
			$cond = "ReportColumnID IS NULL";
		$subjectWeightTemp = $this->returnReportTemplateSubjectWeightData($ReportID, $cond);
		
		for($i=0; $i<sizeof($subjectWeightTemp); $i++) 
		{
			$subjectWeight[$subjectWeightTemp[$i]["SubjectID"]] = $subjectWeightTemp[$i]["Weight"];
		}
				
		$x .= "<br>";
		$x .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>";
		$x .= "<tr><td>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0' align='left'>";
			$x .= "<tr>";
			$x .= "<td class='section_title' style='border-bottom: 2px solid #000000;'>".$eReportCard['Template']['ResultEn']." ".$eReportCard['Template']['ResultCh']."</td>";
			$x .= "</tr>";
			//$x .= "<tr><td height='1' style='border-bottom: 2px solid #000000;'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='100%' height='2'></td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='2'>";
			$x .= "<tr>";
				$x .= "<td class='table_bottom'>&nbsp;</td>";
				$x .= "<td align='center' valign='bottom' width='75' class='text12px table_left table_bottom table_top'>".$eReportCard['Template']['FullMarkEn']."<br>".$eReportCard['Template']['FullMarkCh']."</td>";
				
				$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
				
				foreach($ColumnTitle as $cid=>$ct)
				{
					# convert to big5 display
					if($ReportType=="T")
					{
						$ColumnTitleDisplay =  $ct;
						//$ColumnTitleDisplay =  (convert2unicode($ct, 1, 2));
					}
					else
						$ColumnTitleDisplay = $ct;
					$x .= "<td align='center' valign='bottom' width='75' class='report_text table_left table_bottom table_top'>$ColumnTitleDisplay</td>";
				}
				$x .= "<td align='center' valign='bottom' width='75' class='report_text table_left table_bottom table_top'>".$eReportCard['Template']['TermAverageEn']."<br>".$eReportCard['Template']['TermAverageCh']."</td>";
				$x .= "<td align='center' valign='bottom' width='75' class='report_text table_left table_bottom table_top'>".$eReportCard['Template']['NoOfCandidatesEn']."<br>".$eReportCard['Template']['NoOfCandidatesCh']."</td>";
				$x .= "<td align='center' valign='bottom' width='75' class='report_text table_left table_bottom table_top table_right'>".$eReportCard['Template']['RankEn']."<br>".$eReportCard['Template']['RankCh']."</td>";
			$x .= "</tr>";
			
			$si = 1;
			foreach($thisSubject as $thisSubjectID => $data)
			{
				$bottom_css = $si==sizeof($thisSubject) ? "table_bottom" : "";
				
				$thisID = $thisSubjectID ? $thisSubjectID : $SubjectID;
				$MarksAry = $this->getMarks($ReportID, $StudentID, ' and SubjectID='.$thisID);
				$thisSubjectWeight = $subjectWeight[$thisID];
								
				$x .= "<tr>";
					# Subject 
					$x .= "<td class='table_left $bottom_css'>";
						$x .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr>";
							$x .= "<td class='report_text' nowrap>". ($thisSubjectID ? "&nbsp;&nbsp;":"") .$this->GET_SUBJECT_NAME_LANG($thisID, "EN") ."</td>";
							$x .= "<td class='report_text' align='right' nowrap>". $this->GET_SUBJECT_NAME_LANG($thisID, "CH") ."</td>";
						$x .= "</tr>";
						$x .= "</table>";
					$x .= "</td>";
				 		
					# Full mark 
					//$thisFullMark = $ScaleDisplay=="M" ? $SubjectFullMarkAry[$thisID] * $thisSubjectWeight : $SubjectFullMarkAry[$thisID];
					$thisFullMark = $SubjectFullMarkAry[$thisID];
					$x .= "<td align='center' class='table_left $bottom_css report_text'>". $thisFullMark ."</td>";
					
					foreach($ColumnTitle as $cid=>$ct)
					{
						$columnWeightConds = " ReportColumnID = $cid AND SubjectID = $thisID " ;
						$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
						$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
						
						$thisMark = $ScaleDisplay=="M" ? $MarksAry[$thisID][$cid][Mark] : "";
						$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$thisID][$cid][Grade] : ""; 
						
						# Mark
						//$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? my_round(($thisMark * $thisSubjectWeight),0) : $thisGrade;
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? my_round(($thisMark),0) : $thisGrade;
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisID, $thisMark, $MarksAry[$thisID][$cid]['Grade']);
						if($needStyle)
						{
							$thisMarkTemp = ($ScaleDisplay=="M" && strlen($thisMark) & $thisSubjectWeight) ? ($thisMark / $thisSubjectWeight) : $thisMark;
							$thisNature = $this->returnMarkNature($ClassLevelID, $thisID, $thisMarkTemp,'','',$ReportID);
							$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
						}
						else
							$thisMarkDisplay = $thisMark;
						//$thisMarkDisplay = $ScaleDisplay=="M" ? $thisMarkDisplay : $thisGrade;
						
						if(!$StudentID)
							$thisMarkDisplay = "XX";
						
						if ($columnSubjectWeightTemp==0)
							$thisMarkDisplay = "--";
							
						$x .= "<td align='center' class='table_left $bottom_css report_text'>". $thisMarkDisplay ."</td>";
					}
					
					if($SubjectID == $thisID)
					{
						$CandidateNo = $this->CountSubjectCandidatesNum($ReportID, $SubjectID);
						$thisMark = $ScaleDisplay=="M" ? $MarksAry[$thisID][0][Mark] : "";
						$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$thisID][0][Grade] : ""; 
						
						# Mark
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? my_round($thisMark,0) : $thisGrade;
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisID, $thisMark, $MarksAry[$thisID][0]['Grade']);
						if($needStyle)
						{
							$thisMarkTemp = ($ScaleDisplay=="M" && strlen($thisMark) & $thisSubjectWeight) ? ($thisMark / $thisSubjectWeight) : $thisMark;
							
							$thisNature = $this->returnMarkNature($ClassLevelID, $thisID, $thisMarkTemp,'','',$ReportID);
							$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
						}
						else
							$thisMarkDisplay = $thisMark;
						//$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMarkDisplay : $thisGrade;
						
						if ($columnSubjectWeightTemp==0)
							$thisMarkDisplay = "--";
						
						# Term Average
						$x .= "<td align='center' class='table_left $bottom_css report_text'>". ($StudentID ? $thisMarkDisplay : "XX")."</td>";
						
						# No. of Candidates
						$x .= "<td align='center' class='table_left $bottom_css report_text'>". ($StudentID ? $CandidateNo : "XX")."</td>";
											
						# Rank
						if ($thisMark == $eReportCard['RemarkExempted'])
						{
							$Rank = "--"; 
						}
						else
						{
							$Rank = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID, $ReportColumnID=0) : $thisMark; 
						}
						
						$x .= "<td align='center' class='table_left table_right $bottom_css report_text'>". ($StudentID ? $Rank : "XX")."</td>";
					}
					else
					{
						$x .= "<td align='center' class='table_left $bottom_css report_text'>&nbsp;</td>";
						$x .= "<td align='center' class='table_left $bottom_css report_text'>&nbsp;</td>";
						$x .= "<td align='center' class='table_left table_right $bottom_css report_text'>&nbsp;</td>";
					}
					
				$x .= "</tr>";
				$si++;
			}
					
			$x .= "</table>";
		$x .= "</td></tr>";
		$x .= "</table>";
		
		return $x;
	}
	
	# order: 0 - asc, 1-desc
//	function returnPersonalCharOptions($br=0, $order='')
//	{
//		global $intranet_session_language, $eReportCard;
//		
//		if($br)
//		{
//			$str10 = $eReportCard['NotApplicable_en']."<br>".$eReportCard['NotApplicable_b5'];
//			$str20 = $eReportCard['NeedsImprovement_en']."<br>".$eReportCard['NeedsImprovement_b5'];
//			$str30 = $eReportCard['Satisfactory_en']."<br>".$eReportCard['Satisfactory_b5'];
//			$str40 = $eReportCard['Good_en']."<br>".$eReportCard['Good_b5'];
//			$str50 = $eReportCard['Excellent_en']."<br>".$eReportCard['Excellent_b5'];
//		}
//		else
//		{
//			$str10 = $eReportCard['NotApplicable_'.$intranet_session_language];
//			$str20 = $eReportCard['NeedsImprovement_'.$intranet_session_language];
//			$str30 = $eReportCard['Satisfactory_'.$intranet_session_language];
//			$str40 = $eReportCard['Good_'.$intranet_session_language];
//			$str50 = $eReportCard['Excellent_'.$intranet_session_language];
//		}
//		
//		$x = array(
//				'10'=>$str10,
//				'20'=>$str20,
//				'30'=>$str30,
//				'40'=>$str40,
//				'50'=>$str50,
//					);
//		
//		if($order)
//			krsort($x);
//						
//		return $x;
//		
//	} 
//	
//	function getPersonalCharacteristicsData($StudentID='', $ReportID='', $SubjectID='')
//	{
//		$table = $this->DBName.".RC_PERSONAL_CHARACTERISTICS_DATA";
//		
//		$sql = "select 
//					CharData
//				from 
//					{$table} 
//				where 
//					StudentID = $StudentID and
//					ReportID = $ReportID and 
//					SubjectID = $SubjectID
//				";
//		
//		$result = $this->returnArray($sql);
//		return $result;
//	}
	
	function CountSubjectCandidatesNum($ReportID='', $SubjectID='')
	{
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		
		# found out the all special cases
		$spCaseAry = array();
		$spCaseAry = array_unique(array_merge($this->specialCasesSet1, $this->specialCasesSet2));
		$spCaseStr = "and (Grade Is Null Or not (";
		for($i=0;$i<sizeof($spCaseAry);$i++)
		{
			$spCaseStr .= "Grade = '". $spCaseAry[$i]."' ";
			$spCaseStr .= ($i < sizeof($spCaseAry)-1) ? " or " : "";
		}
		$spCaseStr .= "))";
		$sql = "select 
					count(*)
				from 
					{$table} 
				where 
					ReportID = '$ReportID' and 
					SubjectID = '$SubjectID' and
					ReportColumnID = 0 
					$spCaseStr
				";
		$result = $this->returnVector($sql);		
		return $result[0];
	}
	
	function Get_GrandRanking_Grade($ReportID, $StudentID)
	{
		$UpperLimitArr = array(10, 10, 20, 20, 20, 20);
		$GradeArr = array("A*", "A", "B", "C", "D", "E");
		
		# get the poistion in Form and total number of students in the Form
		$table = $this->DBName.".RC_REPORT_RESULT";
		$fieldToSelect = "OrderMeritForm, FormNoOfStudent ";
		
		$sql = "SELECT $fieldToSelect FROM $table WHERE ";
		$sql .= "ReportID = '$ReportID' ";
		$sql .= "AND StudentID = '$StudentID' ";
		$tempArr = $this->returnArray($sql,2);
		$FormPosition = $tempArr[0][0];
		$FormTotalStudent = $tempArr[0][1];
																	
		# if position is less than zero, return the original marks
		if ($FormPosition <= 0) 
		{
			return $Mark;
		}
		
		# calculate Form percentage of student
		$FormPercentage = ($FormPosition / $FormTotalStudent) * 100;
						
		for($i=0 ; $i<count($UpperLimitArr) ; $i++)
		{
			if ($FormPercentage <= $UpperLimitArr[$i])
			{
				/* Need not style in displaying rank
				switch ($RangeInfo[$i]['Nature'])
				{
					case "D":
						$thisNature = "Distinction";
					  	break;
					case "P":
					  	$thisNature = "Pass";
					  	break;
					case "F":
					  	$thisNature = "Fail";
					  	break;
			  	}

				$thisMarkDisplay = $this->ReturnTextwithStyle($RangeInfo[$i]['Grade'], 'HighLight', $thisNature);
				*/
				$returnVal = $GradeArr[$i];
				break;
			}
			else
			{
				$FormPercentage -= $UpperLimitArr[$i];
			}
		}
		
		return $returnVal;
	}
	
	########### END Template Related
	
	###################### Extra Report (Mid-Term Report) ###########################
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard;
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
		$TableTop .= "<tr><td><div class='school_info_table'>".$TitleTable."</div></td></tr>";
		$TableTop .= "<tr><td><div class='student_info_table'>".$StudentInfoTable."</div></td></tr>";
		$TableTop .= "<tr><td><div class='mstable'>".$MSTable."</div></td></tr>";
		$TableTop .= "<tr><td><div class='remarks_table'>".$eReportCard['ExtraReport']['Remarks']."</div></td></tr>";
		$TableTop .= "</table>";
		
//		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
//		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
//		$TableBottom .= $FooterRow;
//		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table class='extra_report' width='622px' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='850px' valign='top'><td>".$TableTop."</td></tr>";
//				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard,$image_path, $LAYOUT_SKIN;
		$TitleTable = "";
		
		if($ReportID)
		{
			
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
//			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			$ReportTitleArr = explode(":_:",$ReportTitle);
			
			# get school badge
//			$SchoolLogo = GET_SCHOOL_BADGE();
			$SchoolLogo = "/file/reportcard2008/templates/st_stephen_logo.jpg";
				
			# get school name
			$SchoolName = GET_SCHOOL_NAME();	
			
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td align='center'><img height='73' src='$SchoolLogo'></td></tr>";
			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<tr>";
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>\n";
						$TitleTable .= "<tr><td nowrap='nowrap' align='center'><div class='school_name_ch'>".$eReportCard['ExtraReport']['SchoolNameCh']."</div><div class='school_name_en'>".$eReportCard['ExtraReport']['SchoolNameEn']."</div><div  class='school_name_extra'>".$eReportCard['ExtraReport']['SchoolNameExtra']."</div></td></tr>\n";
						$TitleTable .= "<tr><td nowrap='nowrap' align='center'>\n";
							$TitleTable .= "<div class='report_title_div'>\n";
								$TitleTable .= "<div class='report_title_row1'>".$ReportTitleArr[0]."</div>\n";
								$TitleTable .= "<div class='report_title_row2' >".$ReportTitleArr[1]."</div>\n";
							$TitleTable .= "</div>\n";
						$TitleTable .= "</td></tr>\n";
						
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
				$TitleTable .= "</tr>";
			}
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting,$image_path,$LAYOUT_SKIN;
		
		if($ReportID)
		{
			# Retrieve Display Settings
//			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentInfoTableCol = 1;
//			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$StudentTitleArray = array("Class", "Name");
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal = ($StudentID=='')? "XXX" : '';
			//$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			$data['AcademicYear'] = $this->GET_ACTIVE_YEAR_NAME();
			
			$data['DateOfIssue'] = $ReportSetting['Issued'];
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] = $lu->UserName2Lang('en', 1);
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				
				$data['ClassNo'] = $thisClassNumber;
				$data['Class'] = $thisClassName;
				
				$data['StudentNo'] = $thisClassNumber;
				
				if (is_array($SettingStudentInfo))
				{
					if (!in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo) && ($thisClassNumber != ""))
						$data['Class'] .= " (".$thisClassNumber.")";
				}
				else
				{
					if ($thisClassNumber != "")
						$data['Class'] .= " (".$thisClassNumber.")";
				}
				
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] = $lu->Gender;
				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				$data['StudentAdmNo'] = $data['STRN'];
				
				$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
			}
			
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if(in_array($SettingID, $SettingStudentInfo)===true)
					{
						$Title = $eReportCard['Template'][$SettingID."En"]."&nbsp;";
						$Title .= $eReportCard['Template'][$SettingID."Ch"];
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
						}
						
						$colspan = $SettingID=="Name" ? " colspan='10' " : "";
						$StudentInfoTable .= "<td class='tabletext student_info' $colspan width='20%' valign='top' height='{$LineHeight}'>".$Title."&nbsp;:&nbsp;";
						$StudentInfoTable .= ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
							
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						} else {
							if($SettingID=="Name")
							{
								$count=-1;
								$StudentInfoTable .= "</tr>";
							}
						}
						$count++;
					}
				}
				$StudentInfoTable .= "</table>";
			}
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
//		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
//		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$ShowSubjectComponent = $ReportSetting['ShowSubjectComponent'];
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
		
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID,'',0,1);
						
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	
				$isSub=1;
			if ($ShowSubjectComponent==0 && $isSub)
				continue;
			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
				continue;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				# Subject Teacher Comment
				if ($AllowSubjectTeacherComment) {
					$css_border_top = $isSub?"":"border_top";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
					}
					$DetailsTable .= "</span></td>";
				}
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}
		
		# MS Table Footer
//		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
//		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
//		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		#########################################################
		############## Marks START
		$row2 = "";
		# Retrieve Invloved Assesment
		$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
		$ColumnID = array();
		$ColumnTitle = array();
		if (sizeof($ColoumnTitle) > 0)
			foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
			
		$e = 0;
		for($i=0;$i<sizeof($ColumnTitle);$i++)
		{
			//$ColumnTitleDisplay = convert2unicode($ColumnTitle[$i], 1, 2);
			$ColumnTitleDisplay = $ColumnTitle[$i];
			$row1 .= "<td valign='middle' height='{$LineHeight}' class=' small_title border_bottom' align='right'>".$eReportCard['Template']['ResultEn']."&nbsp;".$eReportCard['Template']['ResultCh']."</td>";
			$n++;
			$e++;
		}

		############## Marks END
		#########################################################
		$Rowspan = $row2 ? "rowspan='2'" : "";

		if(!$needRowspan)
			$row1 = str_replace("rowspan='2'", "", $row1);
		
		$x = "<tr>";
		# Subject 
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		for($i=0;$i<sizeof($SubjectColAry);$i++)
		{
			$SubjectEng = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'];
			$SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];
			$SubjectTitle = $SubjectColAry[$i];
			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
			$x .= "<td {$Rowspan}  valign='middle' class='small_title border_bottom {$SubjectColAry[$i]}' height='{$LineHeight}' >". $SubjectTitle . "</td>";
			$n++;
		}
		if($ShowSubjectFullMark)
		{
			$x .= "<td {$Rowspan}  valign='middle' class='small_title border_bottom' align='center' nowrap>". $eReportCard['Template']['FullMarkEn'] ."&nbsp;". $eReportCard['Template']['FullMarkCh'] ."</td>";
			$n++;
		}
		
		# Marks
		$x .= $row1;
		
		# Subject Overall 
		if($ShowRightestColumn)
		{
			$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Overall'] ."' class='border_bottom small_title' align='center'>". $eReportCard['Template']['SubjectOverall'] ."</td>";
			$n++;
		}
		else
		{
			//$e--;	
		}
		
		# Subject Teacher Comment
		if ($AllowSubjectTeacherComment) {
			$x .= "<td {$Rowspan} valign='middle' class='border_left small_title' align='center' width='10%'>".$eReportCard['TeachersComment']."</td>";
		}
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
			 			$t .= "<td class='tabletext' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='subject_row'>$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
		
 		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eReportCard;				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
//		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		$CalculationMethod =  $CalSetting['OrderTerm'] ;
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);

		$CalculationOrder = $CalSetting["OrderTerm"];
		
		$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
		$ColumnID = array();
		$ColumnTitle = array();
		if(sizeof($ColoumnTitle) > 0)
			foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
						
		foreach($SubjectArray as $SubjectID => $SubjectName)
		{
			$isSub = 0;
			if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			// check if it is a parent subject, if yes find info of its components subjects
			$CmpSubjectArr = array();
			$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
			if (!$isSub) {
				$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
				if(!empty($CmpSubjectArr)) $isParentSubject = 1;
			}
			
			# define css
			$css_border_top = ($isSub)? "" : "border_top";
	
			# Retrieve Subject Scheme ID & settings
			$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
			$SchemeID = $SubjectFormGradingSettings['SchemeID'];
			$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
			$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
			$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
			
			$isAllNA = true;
								
			# Assessment Marks & Display
			for($i=0;$i<sizeof($ColumnID);$i++)
			{
				$thisColumnID = $ColumnID[$i];
				$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
				$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
				$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
				
				$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
				$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);

				if ($isSub && $columnSubjectWeightTemp == 0)
				{
					$thisMarkDisplay = $this->EmptySymbol;
				}
				else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
					$thisMarkDisplay = $this->EmptySymbol;
				} else {
					$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
					$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
					
					$thisMSGrade = $MarksAry[$SubjectID][$ColumnID[$i]]['Grade'];
					$thisMSMark = $MarksAry[$SubjectID][$ColumnID[$i]]['Mark'];
					
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
					
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
					
					if($needStyle)
					{
						if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
						{
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
						}
						else
						{
							$thisMarkTemp = $thisMark;
						}
						
						$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
					}
					else
					{
						$thisMarkDisplay = $thisMark;
					}
					
				//	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				//	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
				//	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark; 
				}
					
				if($ShowSubjectFullMark)
				{
  					# check special full mark
  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
  					
					$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
					$x[$SubjectID] .= "<td class='subject_row' align='center'>". $FullMark ."</td>";
				}
				$x[$SubjectID] .= "<td class='subject_row subject_mark' align='right'>". $thisMarkDisplay ."</td>";
			}
							
			# Subject Overall (disable when calculation method is Vertical-Horizontal)
			if($ShowSubjectOverall)
			{
				$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
				$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];

				$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
				$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
				
				# for preview purpose
				if(!$StudentID)
				{
					$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
					$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
				}
				
				#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
				$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
									
				if ($thisMark != "N.A.")
				{
					$isAllNA = false;
				}
					
				# check special case
				if ($CalculationMethod==2 && $isSub)
				{
					$thisMarkDisplay = $this->EmptySymbol;
				}
				else
				{
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
						else
							$thisMarkTemp = $thisMark;
							
						$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
					}
					else
						$thisMarkDisplay = $thisMark;
				}
				
				$x[$SubjectID] .= "<td class=' {$css_border_top}'>";
				$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
				if($ShowSubjectFullMark)
				{
  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
					$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $thisFullMark .")</td>";
				}
				$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
				$x[$SubjectID] .= "</tr></table>";
				$x[$SubjectID] .= "</td>";
			} else if ($ShowRightestColumn) {
				$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
			}
			$isFirst = 0;
			
			# construct an array to return
			$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
			$returnArr['isAllNA'][$SubjectID] = $isAllNA;
		}
		
		return $returnArr;
	}		
	
	########### Personal Characteristics Functions ###########
	/* moved to libreportcard2008.php
	function IS_STUDENT_CHARACTERISTICS_RECORD_EXIST($ParDataArr)
	{
		$table = $this->DBName .".RC_PERSONAL_CHARACTERISTICS_DATA ";
		
		$sql = "SELECT 
						COUNT(CharRecordID) 
				FROM 
						$table
				WHERE 
						StudentID = '".$ParDataArr['StudentID']."'
						AND
						SubjectID = '".$ParDataArr['SubjectID']."'
						AND
						ReportID = '".$ParDataArr['ReportID']."'
				";
		$result = $this->returnVector($sql);
		
		return ($result[0] != 0);
	}
	
	function UPDATE_STUDENT_CHARACTERISTICS_RECORD($ParDataArr)
	{
		global $UserID;
		$table = $this->DBName .".RC_PERSONAL_CHARACTERISTICS_DATA ";
		$sql = "UPDATE 
						$table
				SET 
						CharData = '".$ParDataArr['CharData']."',
						TeacherID = $UserID,
						DateModified = NOW()
				WHERE 
						StudentID = '".$ParDataArr['StudentID']."'
						AND
						SubjectID = '".$ParDataArr['SubjectID']."'
						AND
						ReportID = '".$ParDataArr['ReportID']."'
				";
			
		$result = $this->db_db_query($sql);
		return $result;
	}
	
	function INSERT_STUDENT_CHARACTERISTICS_RECORD($ParDataArr)
	{
		global $UserID;
		$table = $this->DBName .".RC_PERSONAL_CHARACTERISTICS_DATA ";
		$sql = "INSERT INTO ".$table." 
				(StudentID, SubjectID, ReportID, CharData, TeacherID, DateInput, DateModified)
				VALUES ( 
						 '".$ParDataArr['StudentID']."', 
						 '".$ParDataArr['SubjectID']."', 
						 '".$ParDataArr['ReportID']."', 
						 '".$ParDataArr['CharData']."', 
						 '".$UserID."', NOW(), NOW()
						)
				";
		$result = $this->db_db_query($sql);
		return $result;
	}
	
	function GET_CLASSLEVEL_CHARACTERISTICS($ParData)
	{
		global $intranet_session_language;
		
		$sql =  " SELECT a.CharID, a.Title_". ($intranet_session_language=="en" ? "EN" : "CH");
		$sql .= " FROM ". $this->DBName .".RC_PERSONAL_CHARACTERISTICS as a ";
		$sql .= " LEFT JOIN ". $this->DBName .".RC_LEVEL_SUBJECT_PERSONAL_CHARACTERISTICS as b ";
		$sql .= " ON a.CharID = b.CharID ";
		$sql .= " WHERE b.ClassLevelID = ". $ParData['ClassLevelID'] ." AND SubjectID = ". $ParData['SubjectID'];
		$sql .= " ORDER BY a.CharID ";
			
		$result = $this->returnArray($sql,2);
		
		return $result;
	}
	*/
	########### End of Personal Characteristics Functions ###########
}
?>