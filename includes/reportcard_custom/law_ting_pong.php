<?php
# Editing by ivan

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard
{
	function libreportcardcustom()
	{
		$this->libreportcard();
		$this->configFilesType = array("summary", "award", "promotion");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 1;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "--";
	}
	
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='')
	{
		global $eReportCard, $_pageBreak;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting["ClassLevelID"];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$isMockExamReport = $FormNumber == 6 && !$ReportSetting["isMainReport"];
		
		# Report - Page 1
		$Page1TableTop = "";
		$Page1TableTop .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
			$Page1TableTop .= "<tr><td>".$TitleTable."</td></tr>";
			$Page1TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
			$Page1TableTop .= "<tr><td>".$MSTable."</td></tr>";
			$Page1TableTop .= "<tr><td>".$MiscTable[0]."</td></tr>";
		$Page1TableTop .= "</table>";
		
		$Page1TableBottom = "";
		$Page1TableBottom .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
			$Page1TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
			$Page1TableBottom .= $FooterRow;
		$Page1TableBottom .= "</table>";
		
		$ReportPage1 = "";
		$ReportPage1 .= "<tr><td>";
			$ReportPage1 .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$ReportPage1 .= "<tr height='1025px' valign='top'><td>".$Page1TableTop."</td></tr>";
				$ReportPage1 .= "<tr valign='bottom'><td>".$Page1TableBottom."</td></tr>";
			$ReportPage1 .= "</table>";
		$ReportPage1 .= "</td></tr>";
		
		if($StudentID != "") {
			$ReportPage1 .= "</table></div>";
		}
		
		$remarkTableType = $isMockExamReport ? "MockRemarksContent" : "RemarksContent";
		$levelTableType = $isMockExamReport ? "MockLevelTable" : "LevelTable";
		
		# Report - Page 2
		$Page2Table = "";
		$Page2Table .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
			$Page2Table .= "<tr><td colspan='2' class='remark_title font_10pt'><b>".$eReportCard["Template"]["Remarks"]."</b></td></tr>";
			foreach($eReportCard["Template"][$remarkTableType] as $thisIndex => $thisRemarks)
			{
				$Page2Table .= "<tr class='remark_content'>";
					$Page2Table .= "<td width='3%' valign='top'>(".($thisIndex + 1).")</td>";
					$Page2Table .= "<td width='97%'>".$thisRemarks."</td>";
				$Page2Table .= "</tr>";
			}
			
			$Page2Table .= "<tr><td colspan='2' class='attitude_table_td'>";
				$Page2Table .= "<table width='100%' border='0' cellspacing='0' cellpadding='1' align='center' valign='top' class='report_border attitude_table'>";
					$Page2Table .= "<tr>";
						$Page2Table .= "<td width='12%' class='border_left'>".$eReportCard["Template"]["AttitudeLevel"]["Attitude"]."</td>";
						$Page2Table .= "<td width='8%' class='border_left'>".$eReportCard["Template"]["AttitudeLevel"]["Level"]."</td>";
						$Page2Table .= "<td width='80%' class='border_left'>".$eReportCard["Template"]["AttitudeLevel"]["Descriptor"]."</td>";
					$Page2Table .= "</tr>";
					foreach($eReportCard["Template"]["AttitudeLevel"][$levelTableType] as $thisLevel => $thisDescriptor)
					{
						$Page2Table .= "<tr>";
							if($thisLevel == "6") {
								$Page2Table .= "<td class='border_top' rowspan='6' style='vertical-align: middle'><img src='/file/reportcard2008/templates/law_ting_pong_level.PNG' width='68px'></td>";
							}
							$Page2Table .= "<td class='border_left border_top'>".$thisLevel."</td>";
							$Page2Table .= "<td class='border_left border_top' style='text-align: left'>".$thisDescriptor."</td>";
						$Page2Table .= "</tr>";
					}
				$Page2Table .= "</table>";
			$Page2Table .= "</td></tr>";
		$Page2Table .= "</table>";
		$Page2Table .= 
		
		$ReportPage2 = "";
		if($StudentID != "") {
			$ReportPage2 .= "<div id='container' ".$_pageBreak.">";
				$ReportPage2 .= "<table width='100%' border='0' cellpadding='02' cellspacing='0' valign='top'>";
		}
		
			$ReportPage2 .= "<tr><td style='padding-top: 54px'>";
				$ReportPage2 .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
					$ReportPage2 .= "<tr valign='top'><td>".$Page2Table."</td></tr>";
					$ReportPage2 .= "<tr valign='top'><td>".$MiscTable[1]."</td></tr>";
				$ReportPage2 .= "</table>";
			$ReportPage2 .= "</td></tr>";
		
		if($StudentID != "") {
				$ReportPage2 .= "</table>";
			$ReportPage2 .= "</div>";
		}
		
		$x = "";
		$x .= $ReportPage1;
		if($_pageBreak == "") {
			$x .= "<div style='page-break-after:always;'>".$this->Get_Empty_Image(1)."</div>";
		}
		$x .= $ReportPage2;
		
		return $x;
	}
	
	function getReportHeader($ReportID, $StudentID="")
	{
		global $PATH_WRT_ROOT, $eReportCard;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$ClassLevelID = $ReportSetting["ClassLevelID"];
			$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
			$isMockExamReport = $FormNumber == 6 && !$ReportSetting["isMainReport"];
			$ReportTitle = $ReportSetting['ReportTitle'];
			$ReportTitle = explode(":_:", $ReportTitle);
			//$YearName = $this->GET_ACTIVE_YEAR_NAME();
			
			# Report Title
			// Mock Report
// 			if ($isMockExamReport)
// 			{
// 				$ReportTitle1 = $YearName." S6 ".$eReportCard["Template"]["MockReportTitle"];
// 				$ReportTitle2 = $eReportCard["Template"]["AcademicPerformanceEn"]." ".$eReportCard["Template"]["AcademicPerformanceCh"];
// 			}
// 			// Nomral Report
// 			else
// 			{
// 				$ReportTitle1 = $YearName." ".$eReportCard["Template"]["ReportTitleEn"];
// 				$ReportTitle2 = $eReportCard["Template"]["ReportTitleCh"];
// 			}
			
			# Student Photo
			$thisStudentPhoto = "&nbsp;";
			if($StudentID)
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				$lu = new libuser($StudentID);
				
				$thisUserLogin = $lu->UserLogin;
				$thisStudentPhoto = $this->Get_Student_Display_Photo($thisUserLogin);
			}
			
			$TitleTable = "";
			$TitleTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr>";
				$TitleTable .= "<td width='15%' align='center'>&nbsp;</td>";
				$TitleTable .= "<td width='69%' align='center' valign='bottom'>";
					$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' class='title_table'>\n";
						$TitleTable .= "<tr><td nowrap='nowrap' class='font_14pt' align='center'>".$ReportTitle[0]."</td></tr>\n";
						$TitleTable .= "<tr><td nowrap='nowrap' class='font_14pt' align='center'>".$ReportTitle[1]."</td></tr>\n";
					$TitleTable .= "</table>\n";
				$TitleTable .= "</td>";
			$TitleTable .= "<td width='16%' align='left' class='student_image'>".$thisStudentPhoto."</td></tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID="")
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Report Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$StudentTitleArray = $eRCTemplateSetting["StudentInfo"]["Selection"];
			$StudentInfoTableCol = $eRCTemplateSetting["StudentInfo"]["Col"];
			$StudentInfoTableColWidth = $eRCTemplateSetting["ColumnWidth"]["StudentInfo"];
			
			# Retrieve required variables
			$defaultVal = $StudentID == "" ? "XXX" : "";
			
			//$data["AcademicYear"] = $this->GET_ACTIVE_YEAR_NAME();
			if($ReportSetting["Issued"] != "" && $ReportSetting["Issued"] != "0000-00-00") {
				$data["DateIssue"] = date("j<\s\u\p>S</\s\u\p> F Y", strtotime($ReportSetting["Issued"]));
			}
			if($StudentID)
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				$lu = new libuser($StudentID);
				
				$data["Name"] = $lu->UserName2Lang("en", 2);
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]["ClassName"];
				$thisClassNumber = $StudentInfoArr[0]["ClassNumber"];
				$data["ClassClassNo"] = $thisClassName." (".$thisClassNumber.")";
				
				$data["StudentNo"] = str_replace("#", "", $lu->WebSamsRegNo);
			}
			
			$count = 0;
			$StudentInfoTable = "";
			$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='student_info_table'>";
			for($i=0; $i<sizeof($StudentTitleArray); $i++)
			{
				$SettingID = trim($StudentTitleArray[$i]);
				
				$Title = $eReportCard["Template"]["StudentInfo"][$SettingID."En"]." ".$eReportCard["Template"]["StudentInfo"][$SettingID."Ch"];
				$titleColWidth = $StudentInfoTableColWidth[($count % $StudentInfoTableCol)];
				
				$TitleData = $data[$SettingID];
				$TitleData = $TitleData ? $TitleData : $defaultVal;
				
				if($count % $StudentInfoTableCol == 0) {
					$StudentInfoTable .= "<tr>";
				}
				
				$StudentInfoTable .= "<td class='tabletext font_11pt' width='".$titleColWidth."' valign='top'>";
					$StudentInfoTable .= "<b>".$Title.": </b>".$TitleData;
				$StudentInfoTable .= "</td>";
					
				if(($count + 1) % $StudentInfoTableCol == 0) {
					$StudentInfoTable .= "</tr>";
				}
				$count++;
			}
			$StudentInfoTable .= "</table>";
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting["ClassLevelID"];
		$isPersonalizedReport = $_POST["PrintTemplateType"] == "Personalized";
		
		# Retrieve Column Header	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
		
		# Retrieve Subject
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID="", $SubjectFieldLang="", $ParDisplayType="Desc", $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0) {
			foreach((array)$MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		}
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0) {
			foreach((array)$SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		}
		
		# Retrieve Subject Marks
		$MarksAry = $this->getMarks($ReportID, $StudentID, "", 0, 1);
		
		# Retrieve Subject Column
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# Retrieve Mark Columns
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned["HTML"];
		$isAllNAAry = $MSTableReturned["isAllNA"];
		
		# Retrieve Teacher's Comment
		//$TeacherCommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='2' class='report_border ms_table'>";
		
		# Column Header
		$DetailsTable .= $ColHeader;
		
		# Subject rows
		$isFirst = 1;
		for($i=0; $i<$sizeofSubjectCol; $i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			// if all marks is "*", then don't display
			if (sizeof((array)$MarksAry[$thisSubjectID]) > 0) 
			{	
				$Droped = 1;
				foreach((array)$MarksAry[$thisSubjectID] as $cid => $da) {
					if($da["Grade"] != "*")		$Droped=0;
				}
			}
			if($Droped) {
				continue;
			}
			
			if(in_array($thisSubjectID, (array)$MainSubjectIDArray) != true) {	
				$isSub = 1;
			}
			if ($eRCTemplateSetting["HideComponentSubject"] && $isSub) {
				continue;
			}
			
			// Check if display subject row with all marks equal "--"
			if ($eRCTemplateSetting["DisplayNA"] || $isAllNAAry[$thisSubjectID] == false)
			{
				$DetailsTable .= "<tr class='subject_row'>";
				
				# Subject Column
				$DetailsTable .= $SubjectCol[$i];
				
				# Comment Column
				if($isPersonalizedReport)
				{
					$DetailsTable .= "<td class='border_left'>";
						$DetailsTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
							$DetailsTable .= "<td class='tabletext' align='left' width='100%'>".$MarksDisplayAry[$thisSubjectID]."</td>";
						$DetailsTable .= "</tr></table>";
					$DetailsTable .= "</td>";
				}
				
				# Mark Columns
				if(!$isPersonalizedReport)
				{
					$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				}
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}
		
//		# MS Table Footer
//		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
		$DetailsTable .= "</table>";
		
		##########################################
		# End Generate Table
		##########################################
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting["ClassLevelID"];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$SemID = $ReportSetting["Semester"];
		$ReportType = $SemID == "F" ? "W" : "T";
		$isMockExamReport = $FormNumber == 6 && !$ReportSetting["isMainReport"];
		$isPersonalizedReport = $_POST["PrintTemplateType"] == "Personalized";
		
		$x = "";
		$row1 = "";
		$row2 = "";
		
		$n = 0;
		$e = 0;				# Columns within Term / Assesment
		$t = array(); 		# Number of Assessments of each term (for consolidated report only)
		
		############## Mark Columns [START]
		
		// Personalized Report
		if($isPersonalizedReport)
		{
			$MSTableColArray = $eRCTemplateSetting["MSTable"]["Selection"];
			
			$row1 .= "<td valign='middle' class='border_left border_bottom tabletext font_13pt ms_table_header' align='center'>";
				$row1 .= $eReportCard["Template"]["CommentsEn"]."　　".$eReportCard["Template"]["CommentsCh"];
			$row1 .= "</td>";
			$n++;
		}
		// Noraml Report
		else
		{
			$MSTableType = $isMockExamReport ? "MockSelection" : "Selection";
			$MSTableColArray = $eRCTemplateSetting["MSTable"][$MSTableType];
			$MSTableColWidth = $eRCTemplateSetting["ColumnWidth"]["MSTableCol"];
			
			$e = 0;
			$titleSperator = $MSTableType == "MockSelection" ? "<br/>" : "<br/>";
			for($i=0; $i<sizeof($MSTableColArray); $i++)
			{
				$SettingID = trim($MSTableColArray[$i]);
				if($MSTableType == "Selection" && $SettingID == "Grade") {
					$SettingID = $ReportType.$SettingID;
				}
				if($FormNumber == 6 && $SettingID == "SummatiAccess") {
					$SettingID = "MockExam";
				}
				if($FormNumber == 6 && $SettingID == "Grade") {
				    $SettingID = "Level";
				}
				
				$ColumnTitleDisplay = $eReportCard["Template"][$SettingID."En"].$titleSperator.$eReportCard["Template"][$SettingID."Ch"];
				if(isset($eReportCard["Template"][$SettingID."Remark"])) {
					$ColumnTitleDisplay .= $titleSperator.$eReportCard["Template"][$SettingID."Remark"];
				}
				$titleColWidth = $MSTableColWidth[$i];
				
				$row1 .= "<td class='border_left border_bottom tabletext ms_table_header' width='".$titleColWidth."' valign='middle' align='center'>".$ColumnTitleDisplay."</td>";
				$n++;
			}
			
			// F1 - F5 Report
			if($MSTableType == "Selection")
			{
				$row2 = $row1;
				$row1 = "";
				$e = $n;
				$n = 0;
				
				$ColumnTitleDisplay = $eReportCard["Template"]["AssessScoreEn"]."<br/>".$eReportCard["Template"]["AssessScoreCh"];
				$row1 .= "<td class='border_left border_bottom tabletext ms_table_header' valign='middle' align='center' colspan='3'>".$ColumnTitleDisplay."</td>";
				$n++;
			}
		}
		$rowspan = $MSTableType == "Selection" && $row2 != "" ? "rowspan='2'" : "";
		$i = sizeof($MSTableColArray);
		
		############## Mark Columns [END]
		
		$x .= "<tr>";
		
		# Subject (Eng) Column
		$ColumnTitleDisplay = $eReportCard["Template"]["SubjectEn"];
		$titleColWidth = $eRCTemplateSetting["ColumnWidth"]["MSTableCol"][$i];
		$x .= "<td class='tabletext border_bottom' width='".$titleColWidth."' ".$rowspan." valign='middle' style='height: 88px;'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'><tr>";
				$x .= "<td></td>";
				$x .= "<td class='tabletext font_13pt ms_table_header'>".$ColumnTitleDisplay."</td>";
			$x .= "</tr></table>";
		$x .= "</td>";
		$n++;
		$i++;
		
		# Subject (Chi) Column
		$ColumnTitleDisplay = $eReportCard["Template"]["SubjectCh"];
		$titleColWidth = $eRCTemplateSetting["ColumnWidth"]["MSTableCol"][$i];
		$x .= "<td class='tabletext border_bottom' width='".$titleColWidth."' ".$rowspan." valign='middle' style='height: 88px;'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'><tr>";
				$x .= "<td></td>";
				$x .= "<td class='tabletext font_13pt ms_table_header'>".$ColumnTitleDisplay."</td>";
			$x .= "</tr></table>";
		$x .= "</td>";
		$n++;
		$i++;
		
		# Mark Columns
		$x .= $row1;
		
		# Attitude Column
		if(!$isPersonalizedReport)
		{
			$ColumnTitleDisplay = $eReportCard["Template"]["AttitudeEn"]."<br/>".$eReportCard["Template"]["AttitudeCh"];
			$titleColWidth = $eRCTemplateSetting["ColumnWidth"]["MSTableCol"][$i];
			$x .= "<td class='border_left border_bottom tabletext ms_table_header' width='".$titleColWidth."' ".$rowspan." valign='middle' align='center'>".$ColumnTitleDisplay."</td>";
			$n++;
		}
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ReportClassLevelID = $ReportSetting["ClassLevelID"];
		$FormNumber = $this->GET_FORM_NUMBER($ReportClassLevelID, 1);
		$isSeniorFormLevel = $FormNumber == 4 || $FormNumber == 5 || $FormNumber == 6;
		$isPersonalizedReport = $_POST["PrintTemplateType"] == "Personalized";
		
 		$x = array();
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID="", $SubjectFieldLang="", $ParDisplayType="Desc", $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
		{
		    $subjectCodeMapping = $this->GET_SUBJECTS_CODEID_MAP($withComponent = 1, $mapByCode = 0);
	 		foreach($SubjectArray as $SubjectID => $SubSubjectAry)
	 		{
		 		foreach($SubSubjectAry as $SubSubjectID => $SubSubjectName)
		 		{
		 		    $isSubjectComponent = $SubSubjectID > 0;
// 			 		if($isPersonalizedReport && $SubSubjectID > 0) {
// 			 			continue;
// 			 		}
			 		
		 		    $Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID == 0)
			 		{
				 		$SubSubjectID = $SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		$SubjectEn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
	 				$SubjectCh = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 			
	 				// Special Handling for component "Listening 聆聽" in S4, S5, S6 > Display "Listening & Integrated Skills 聆聽及綜合"
	 				if($isSeniorFormLevel && $isSubjectComponent)
	 				{
	 				    $SubjectCodeInfo = $subjectCodeMapping[$SubSubjectID];
	 				    if($SubjectCodeInfo['CODEID'] == '165' && $SubjectCodeInfo['CMP_CODEID'] == '165-P3')
	 				    {
	 				        $SubjectEn = 'Listening & Integrated Skills';
	 				        $SubjectCh = '聆聽及綜合';
	 				    }
	 				}
	 				
		 			$col_style = $isPersonalizedReport ? "top" : "middle";
		 			
			 		$t = "";
		 			$t .= "<td class='tabletext subject_col' valign='".$col_style."'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'><tr>";
							$t .= "<td>".$Prefix."</td>";
							$t .= "<td class='tabletext'>".$SubjectEn."</td>";
						$t .= "</tr></table>";
					$t .= "</td>";
		 			$t .= "<td class='tabletext subject_col' height='{$LineHeight}' valign='".$col_style."'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'><tr>";
							$t .= "<td>".$Prefix."</td>";
							$t .= "<td class='tabletext'>".$SubjectCh."</td>";
						$t .= "</tr></table>";
					$t .= "</td>";
					
					$x[] = $t;
				}
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID="", $StudentID="")
	{
 		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		$dataAry = array();
		
		// Principal
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		$lu = new libuser($eRCTemplateSetting["PrincipalUserID"]);
		
		$principalName = $lu->EnglishName;
		$principalTitle = $lu->TitleEnglish;
		$dataAry["Principal"] = $principalName." (".$principalTitle.")";
		
		// Class Teacher
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		$lclass = new libclass();
		
		$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
		$thisClassName = $StudentInfoArr[0]["ClassName"];
		$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
		foreach((array)$ClassTeacherAry as $key => $val) {
			$CTeacher[] = $val["EnglishName"]." (".$val["TitleEnglish"].")";
		}
		$dataAry["ClassTeacher"] = $StudentID ? (!empty($CTeacher) ? implode("<br/>", $CTeacher) : "&nbsp;") : "xxx";
 		
		$SignatureTitleArray = $eRCTemplateSetting["Signature"]["Selection"];
		$SignatureTitleColWidth = $eRCTemplateSetting["ColumnWidth"]["Signature"];
 		
		$SignatureTable = "";
		$SignatureTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' class='signature_table'>";
		$SignatureTable .= "<tr>";
		
		for($i=0; $i<sizeof($SignatureTitleArray); $i++)
		{
			$SettingID = trim($SignatureTitleArray[$i]);
			$TitleColWidth = $SignatureTitleColWidth[$i];
// 			$SignatureTitle1 = $SettingID != "ParentGuardian" ? $dataAry[$SettingID] : $eReportCard["Template"][$SettingID."En"];
// 			$SignatureTitle2 = $SettingID != "ParentGuardian" ? $eReportCard["Template"][$SettingID."En"]." ".$eReportCard["Template"][$SettingID."Ch"] : $eReportCard["Template"][$SettingID."Ch"];
			$SignatureTitle1 = $SettingID != "SchoolChop"? $dataAry[$SettingID] : $eReportCard["Template"][$SettingID."En"];
			$SignatureTitle2 = $SettingID != "SchoolChop"? $eReportCard["Template"][$SettingID."En"]." ".$eReportCard["Template"][$SettingID."Ch"] : $eReportCard["Template"][$SettingID."Ch"];
			
			$SignatureTable .= "<td valign='top' align='center' width='".$TitleColWidth."'>";
				$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0' width='92%'>";
					$SignatureTable .= "<tr><td align='center' class='border_top' valign='bottom' style='padding-top: 5px; border-color: black'>".$SignatureTitle1."</td></tr>";
					$SignatureTable .= "<tr><td align='center' class='' valign='bottom'>".$SignatureTitle2."</td></tr>";
				$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID 				= $ReportSetting["ClassLevelID"];
		$SemID 						= $ReportSetting["Semester"];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $this->Get_Student_ClassName($StudentID);
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID,'',0,1);
		$GrandTotal = $StudentID ? $this->Get_Score_Display_HTML($result['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		$GrandAverage = $StudentID ? $this->Get_Score_Display_HTML($result['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		
		$ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
  		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
  		$ClassNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
  		$FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
  		
  		if ($CalOrder == 2)
  		{
			foreach ((array)$ColumnTitle as $ColumnID => $ColumnName) {
				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID, '', 1,1);
				
				$columnTotal[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
				$columnAverage[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
				
				$columnClassPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "S";
				$columnFormPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "S";
				$columnClassNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
				$columnFormNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";
			}
		}
		
		
		$first = 1;
		# Overall Result
		if($ShowGrandTotal)
		{
			$thisTitleEn = $eReportCard['Template']['OverallResultEn'];
			$thisTitleCh = $eReportCard['Template']['OverallResultCh'];
			$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnTotal, $GrandTotal, $first);
			
			$first = 0;
		}
		
		
		if ($eRCTemplateSetting['HideCSVInfo'] != true)
		{
			# Average Mark 
			if($ShowGrandAvg)
			{
				$thisTitleEn = $eReportCard['Template']['AvgMarkEn'];
				$thisTitleCh = $eReportCard['Template']['AvgMarkCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnAverage, $GrandAverage, $first);
				
				$first = 0;
			}
			
			# Position in Class 
			if($ShowOverallPositionClass)
			{
				$thisTitleEn = $eReportCard['Template']['ClassPositionEn'];
				$thisTitleCh = $eReportCard['Template']['ClassPositionCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassPos, $ClassPosition, $first);
				
				$first = 0;
			}
			
			# Number of Students in Class 
			if($ShowNumOfStudentClass)
			{
				$thisTitleEn = $eReportCard['Template']['ClassNumOfStudentEn'];
				$thisTitleCh = $eReportCard['Template']['ClassNumOfStudentCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassNumOfStudent, $ClassNumOfStudent, $first);
				
				$first = 0;
			}
			
			# Position in Form 
			if($ShowOverallPositionForm)
			{
				$thisTitleEn = $eReportCard['Template']['FormPositionEn'];
				$thisTitleCh = $eReportCard['Template']['FormPositionCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormPos, $FormPosition, $first);
				
				$first = 0;
			}
			
			# Number of Students in Form 
			if($ShowNumOfStudentForm)
			{
				$thisTitleEn = $eReportCard['Template']['FormNumOfStudentEn'];
				$thisTitleCh = $eReportCard['Template']['FormNumOfStudentCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormNumOfStudent, $FormNumOfStudent, $first);
				
				$first = 0;
			}
			
			##################################################################################
			# CSV related
			##################################################################################
			# build data array
			$ary = array();
			//$csvType = $this->getOtherInfoType();
			
			//modified by Marcus 20/8/2009
			$ary = array();
			if ($StudentID != '') {
				$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID,$StudentID);
				$ary = $OtherInfoDataAry[$StudentID];
			}
			
			if($SemID=="F")
			{
				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
				
				/*
				foreach($ColumnData as $k1=>$d1)
				{
					$TermID = $d1['SemesterNum'];
					$InfoTermID = $TermID;
					
					if(!empty($csvType))
					{
						foreach($csvType as $k=>$Type)
						{
							$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);
							if(!empty($csvData)) 
							{
								foreach($csvData as $RegNo=>$data)
								{
									if($RegNo == $WebSamsRegNo)
									{
										foreach($data as $key=>$val)
											$ary[$TermID][$key] = $val;
									}
								}
							}
						}
					}
				}
				*/
				
				# calculate sems/assesment col#
				$ColNum2Ary = array();
				foreach($ColumnData as $k1=>$d1)
				{
					$TermID = $d1['SemesterNum'];
					if($d1['IsDetails']==1)
					{
						# check sems/assesment col#
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
						$thisReportID = $thisReport['ReportID'];
						$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
						$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1;
					}
					else
						$ColNum2Ary[$TermID] = 0;
				}
			}
			/*
			else
			{
				$InfoTermID = $SemID;
				
				if(!empty($csvType))
				{
					foreach($csvType as $k=>$Type)
					{
						$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);
						
						if(!empty($csvData)) 
						{
							foreach($csvData as $RegNo=>$data)
							{
								if($RegNo == $WebSamsRegNo)
								{
									foreach($data as $key=>$val)
										$ary[$SemID][$key] = $val;
								}
							}
						}
					}
				}
			}
			*/
			$border_top = $first ? "border_top" : "";
			
			# Days Absent 
			$thisTitleEn = $eReportCard['Template']['DaysAbsentEn'];
			$thisTitleCh = $eReportCard['Template']['DaysAbsentCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Days Absent", $ary);
			
			# Times Late 
			$thisTitleEn = $eReportCard['Template']['TimesLateEn'];
			$thisTitleCh = $eReportCard['Template']['TimesLateCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Time Late", $ary);
			
			# Conduct 
			$thisTitleEn = $eReportCard['Template']['ConductEn'];
			$thisTitleCh = $eReportCard['Template']['ConductCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Conduct", $ary);
		}
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID="", $NumOfAssessment=array())
	{
		global $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting["ClassLevelID"];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$SemID = $ReportSetting["Semester"];
		$ReportType = $SemID == "F" ? "W" : "T";
		$isMockExamReport = $FormNumber == 6 && !$ReportSetting["isMainReport"];
		$isPersonalizedReport = $_POST["PrintTemplateType"] == "Personalized";
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting["UseWeightedMark"];
		$CalculationMethod = $ReportType == "T" ? $CalSetting["OrderTerm"] : $CalSetting["OrderFullYear"];
		
		# Retrieve Storage Settings
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		# Retrieve Subjects
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		
		# Retrieve Main Subjects
		$MainSubjectIDArray = array();
		$MainSubjectNameArray = array();
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID="", $SubjectFieldLang="", $ParDisplayType="Desc", $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0) {
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		}
		
		# Retrieve last term report data
		$SubjectAttitudeAssoAry = $this->GET_EXTRA_SUBJECT_INFO($StudentID, "", $ReportID);
		$TeacherCommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		if($ReportType == "W")
		{
			$termReportAry = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
			$lastReportId = end($termReportAry);
			$lastReportId = $lastReportId["ReportID"];
			
			// Build last term report column mapping
			$lastReportColumnIds = array();
			$lastReportColumnAry = $this->returnReportTemplateColumnData($lastReportId);
			$lastReportColumnIds[-1] = $lastReportColumnAry[0]['ReportColumnID'];
			$lastReportColumnIds[-2] = $lastReportColumnAry[1]['ReportColumnID'];
			
			// Report marks
			$TermReportMarksAry = $this->getMarks($lastReportId, $StudentID, "", 0, 1);
			
			// Subject attitude
			$SubjectAttitudeAssoAry = $this->GET_EXTRA_SUBJECT_INFO($StudentID, "", $lastReportId);
			
			// Subject teacher comment
			$TeacherCommentAry = $this->returnSubjectTeacherComment($lastReportId, $StudentID);
		}
		else if($isMockExamReport)
		{
			$mainTermReport = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $SemID, 1);
			if(!empty($mainTermReport))
			{
				$mainTermReportId = $mainTermReport["ReportID"];
				$SubjectAttitudeAssoAry = $this->GET_EXTRA_SUBJECT_INFO($StudentID, "", $mainTermReportId);
				$TeacherCommentAry = $this->returnSubjectTeacherComment($mainTermReportId, $StudentID);
			}
		}
		
		$n = 0;
		$x = array();
		
		# Personalized Report
		if($isPersonalizedReport)
		{
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				if(in_array($SubjectID, $MainSubjectIDArray) != true)
				{
				    $returnArr["isAllNA"][$SubjectID] = true;
				    continue;
				}
				
				$SubjectComment = $TeacherCommentAry[$SubjectID];
				$SubjectComment = trim($SubjectComment) != "" ? nl2br($SubjectComment) : "&nbsp;";
				
				// Construct an array to return
				$returnArr["HTML"][$SubjectID] = $SubjectComment;
				$returnArr["isAllNA"][$SubjectID] = ($SubjectComment == "&nbsp;");
			}
		}
		# Mock Report
		else if($isMockExamReport)
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			// Report Columns
			$ColumnID = array();
			$ColumnTitle = array();
			$ReportColumnAry = $this->returnReportColoumnTitle($ReportID);
			if(sizeof($ReportColumnAry) > 0) {
				foreach ($ReportColumnAry as $ColumnID[] => $ColumnTitle[]);
			}
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray) != true)	$isSub = 1;
				
				// Check if parent subject, if yes find its components
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when Scale Input of components Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# Retrieve Components Weight
				$thisSubjectWeightDisplay = "&nbsp;";
				if (!$isParentSubject || $isSub)
				{
					$columnWeightConds = " ReportColumnID = '".$ColumnID[0]."' AND SubjectID = '".$SubjectID."' " ;
					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
					$thisSubjectWeight = $columnSubjectWeightArr[0]["Weight"] * 100;
					$thisSubjectWeightDisplay = "[".$thisSubjectWeight."]";
				}
				
				$x[$SubjectID] .= "<td class='border_left'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='100%'>".$thisSubjectWeightDisplay."</td>";
					$x[$SubjectID] .= "</tr></table>";
				$x[$SubjectID] .= "</td>";
				
				# Retrieve Grading Scheme
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $ReportID);
				$SchemeID = $SubjectFormGradingSettings["SchemeID"];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings["ScaleDisplay"];
				$ScaleInput = $SubjectFormGradingSettings["ScaleInput"];
				
				# Subject Overall Score and Grade
				$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
				$thisMark = $MarksAry[$SubjectID][0]["Mark"];
				
				// for preview
				if(!$StudentID)
				{
					$thisGrade = "G";
					$thisMark = "S";
				}
				
				$isAllNA = true;
				//if ($thisGrade != "" && ($thisGrade == "/" || !in_array($thisGrade, $this->specialCasesSetExcludeRanking)))
                if ($thisGrade != "" && !in_array($thisGrade, $this->specialCasesSetExcludeRanking))
				{
					$isAllNA = false;
				}
				
				// Check special case
				if($isParentSubject)
				{
					$thisMarkDisplay = "&nbsp;";
				}
				else if($StudentID != "")
				{
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]["Grade"]);
					if($needStyle)
					{
						if($thisSubjectWeight > 0 && $ScaleDisplay=="M") {
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark / $thisSubjectWeight : $thisMark;
						}
						else {
							$thisMarkTemp = $thisMark;
						}
						$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
					}
					else {
						$thisMarkDisplay = $thisMark;
					}
				}
				else
				{
					$thisMarkDisplay = $thisMark;
				}
				
				$x[$SubjectID] .= "<td class='border_left'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='100%'>".$thisMarkDisplay."</td>";
					$x[$SubjectID] .= "</tr></table>";
				$x[$SubjectID] .= "</td>";
				
				// Check special case
				if($isSub)
				{
					$thisGradeDisplay = "&nbsp;";
				}
				else if($StudentID != "")
				{
					list($thisGrade, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisGrade, $MarksAry[$SubjectID][0]["Grade"]);
					if($needStyle)
					{
						if($thisSubjectWeight > 0 && $ScaleDisplay=="M") {
							$thisGradeTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark / $thisSubjectWeight : $thisMark;
						}
						else {
							$thisGradeTemp = $thisGrade;
						}
							
						$thisGradeDisplay = $this->Get_Score_Display_HTML($thisGradeTemp, $ReportID, $ClassLevelID, $SubjectID, $thisGradeTemp);
					}
					else {
						$thisGradeDisplay = $thisGrade;
					}
				}
				else
				{
					$thisGradeDisplay = $thisGrade;
				}
				
				$x[$SubjectID] .= "<td class='border_left'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='100%'>".$thisGradeDisplay."</td>";
					$x[$SubjectID] .= "</tr></table>";
				$x[$SubjectID] .= "</td>";
				
				# Subject Attitude
				$thisAttitudeDisplay = $StudentID || $isSub ? "&nbsp;" : "G";
				$thisAttitudeGrade = $SubjectAttitudeAssoAry[$StudentID][$SubjectID]["Info"];
				if (!$isSub && !empty($thisAttitudeGrade)) {
					$thisAttitudeDisplay = $thisAttitudeGrade;
				}
				
				$x[$SubjectID] .= "<td class='border_left'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='100%'>".$thisAttitudeDisplay."</td>";
					$x[$SubjectID] .= "</tr></table>";
				$x[$SubjectID] .= "</td>";
				
				// Construct an array to return
				$returnArr["HTML"][$SubjectID] = $x[$SubjectID];
				$returnArr["isAllNA"][$SubjectID] = $isAllNA;
			}
		}
		# Normal Term Report
		else if($ReportType == "T")
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			// Report Columns
			$ColumnID = array();
			$ColumnTitle = array();
			$ReportColumnAry = $this->returnReportColoumnTitle($ReportID);
			if(sizeof($ReportColumnAry) > 0) {
				foreach ($ReportColumnAry as $ColumnID[] => $ColumnTitle[]);
			}
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray) != true)	$isSub = 1;
				
				// Check if parent subject, if yes find its components
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when Scale Input of components Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# Retrieve Grading Scheme
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $ReportID);
				$SchemeID = $SubjectFormGradingSettings["SchemeID"];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings["ScaleDisplay"];
				$ScaleInput = $SubjectFormGradingSettings["ScaleInput"];
				
				# Assessment Marks & Display
				$isAllNA = true;
				for($i=0; $i<sizeof($ColumnID); $i++)
				{
					$thisColumnID = $ColumnID[$i];
					
					$columnWeightConds = " ReportColumnID = '".$thisColumnID."' AND SubjectID = '".$SubjectID."' " ;
					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]["Weight"];
					
					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
					$isAllCmpZeroWeight = !in_array(false, $isAllCmpZeroWeightArr);
					
//					if ($isSub && $columnSubjectWeightTemp == 0)
//					{
//						$thisMarkDisplay = $this->EmptySymbol;
//					}
//					else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight)
//					{
//						$thisMarkDisplay = $this->EmptySymbol;
//					}
//					else
//					{
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, " ReportColumnID='".$thisColumnID."' and SubjectID = '".$SubjectID."' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]["Weight"];
						
//						$thisMSGrade = $MarksAry[$SubjectID][$thisColumnID]["Grade"];
//						$thisMSMark = $MarksAry[$SubjectID][$thisColumnID]["Mark"];
//						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!="") ? $thisMSGrade : "";
//						$thisMark = ($ScaleDisplay=="M" && $thisGrade=="") ? $thisMSMark : "";
						
						// Default Display - Mark (M)
						$thisGrade = $MarksAry[$SubjectID][$thisColumnID]["Grade"];
						$thisMark = $MarksAry[$SubjectID][$thisColumnID]["Mark"];
						
						// for preview
						if(!$StudentID)
						{
							$thisGrade = "G";
							$thisMark = "S";
						}
						
						$thisMark = ($ScaleInput=="G" && $ScaleDisplay=="G") ? $thisGrade : $thisMark;
						//if ($thisGrade != "" && ($thisGrade == "/" || !in_array($thisGrade, $this->specialCasesSetExcludeRanking)))
                        if ($thisGrade != "" && !in_array($thisGrade, $this->specialCasesSetExcludeRanking))
						{
							$isAllNA = false;
						}
						
						# Check special case
						if($StudentID != "")
						{
							list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$thisColumnID]["Grade"]);
							if($needStyle)
							{
							    if ($thisSubjectWeight > 0 && !($ScaleInput=="G" && $ScaleDisplay=="G"))
								{
									$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight != 0) ? $thisMark / $thisSubjectWeight : $thisMark;
								}
								else
								{
									$thisMarkTemp = $thisMark;
								}
							
								$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
							}
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
//					}
					
					$x[$SubjectID] .= "<td class='border_left'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  						$x[$SubjectID] .= "<td class='tabletext' align='center' width='100%'>". $thisMarkDisplay ."</td>";
						$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
					
					if($i == 1) {
						break;
					}
				}
				
				# Subject Overall
				$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
				$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
				$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!="" ) ? $thisMSGrade : "";
				$thisMark = ($ScaleDisplay=="M" && $thisGrade=="") ? $thisMSMark : "";
				
				// for preview
				if(!$StudentID)
				{
					$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
					$thisGrade 	= $ScaleDisplay == "" || $ScaleDisplay=="G" ? "G" : "";
				}
				
				$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
				//if (!$isSub && $thisMark != "" && ($thisMark == "/" || !in_array($thisMark, $this->specialCasesSetExcludeRanking)))
                if (!$isSub && $thisMark != "" && !in_array($thisMark, $this->specialCasesSetExcludeRanking))
				{
				    $isAllNA = false;
				}
				
				// Check special case
				if ($isSub)
				{
					$thisMarkDisplay = "&nbsp;";
				}
				else if($StudentID != "")
				{
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]["Grade"]);
					if($needStyle)
					{
 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M") {
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark / $thisSubjectWeight : $thisMark;
 						}
						else {
							$thisMarkTemp = $thisMark;
						}
							
						$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
					}
					else {
						$thisMarkDisplay = $thisMark;
					}
				}
				else
				{
					$thisMarkDisplay = $thisMark;
				}
				
				$x[$SubjectID] .= "<td class='border_left'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='100%'>".$thisMarkDisplay."</td>";
					$x[$SubjectID] .= "</tr></table>";
				$x[$SubjectID] .= "</td>";
				
				# Subject Attitude
				$thisAttitudeDisplay = $StudentID || $isSub ? "&nbsp;" : "G";
				$thisAttitudeGrade = $SubjectAttitudeAssoAry[$StudentID][$SubjectID]["Info"];
				if (!$isSub && !empty($thisAttitudeGrade)) {
					$thisAttitudeDisplay = $thisAttitudeGrade;
				}
				
				$x[$SubjectID] .= "<td class='border_left'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='100%'>".$thisAttitudeDisplay."</td>";
					$x[$SubjectID] .= "</tr></table>";
				$x[$SubjectID] .= "</td>";
				
				// Construct an array to return
				$returnArr["HTML"][$SubjectID] = $x[$SubjectID];
				$returnArr["isAllNA"][$SubjectID] = $isAllNA;
			}
		}
		# Whole Year Report
		else
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Terms
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			$ColumnData = array("-1", "-2");
//			$ColumnData = $this->returnReportInvolvedSem($ReportID);
//			$ColumnData = array_keys($ColumnData);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray) != true)	$isSub=1;
				
				// Check if parent subject, if yes find its components
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when Scale Input of components Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# Retrieve Grading Scheme
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0 , 0, $ReportID);
				$SchemeID = $SubjectFormGradingSettings["SchemeID"];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings["ScaleDisplay"];
				$ScaleInput = $SubjectFormGradingSettings["ScaleInput"];
				
				# Term Marks
				$isAllNA = true;
				foreach($ColumnData as $thisColumnType)
				{
//					$termReportInfo = $this->returnReportTemplateBasicInfo("", "Semester='".$thisColumnType."' AND ClassLevelID='".$ClassLevelID."'");
//					$termReportID = $termReportInfo["ReportID"];
//					$termMarksAry = $this->getMarks($termReportID, $StudentID, "", "", 1);
//					
//					// Default Display - Mark (M)
//					$thisGrade = $termMarksAry[$SubjectID][0]["Grade"];
//					$thisMark = $termMarksAry[$SubjectID][0]["Mark"];
					
					// Default Display - Mark (M)
					$thisGrade = $MarksAry[$SubjectID][$thisColumnType]["Grade"];
					$thisMark = $MarksAry[$SubjectID][$thisColumnType]["Mark"];
					
					// for preview
					if(!$StudentID)
					{
						$thisGrade = "G";
						$thisMark = "S";
					}
					
					// Use last term report grade (if G > G)
					$lastReportColumnId = $lastReportColumnIds[$thisColumnType];
					$useLastReportColumnGrade = $ScaleInput=="G" && $ScaleDisplay=="G" && $lastReportColumnId && isset($TermReportMarksAry[$SubjectID][$lastReportColumnId]['Grade']);
					$thisGrade = $useLastReportColumnGrade? $TermReportMarksAry[$SubjectID][$lastReportColumnId]['Grade']: $thisGrade;
					$thisMark = $useLastReportColumnGrade? $TermReportMarksAry[$SubjectID][$lastReportColumnId]['Grade']: $thisMark;
					
					//if ($thisGrade != "" && ($thisGrade == "/" || !in_array($thisGrade, $this->specialCasesSetExcludeRanking)))
                    if ($thisGrade != "" && !in_array($thisGrade, $this->specialCasesSetExcludeRanking))
					{
						$isAllNA = false;
					}
					
					# Check special case
					if($StudentID != "")
					{
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $thisGrade);
						if($needStyle)
						{
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
					}
					else
					{
						$thisMarkDisplay = $thisMark;
					}
					
					$x[$SubjectID] .= "<td class='border_left'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
							$x[$SubjectID] .= "<td class='tabletext' align='center' width='100%'>".$thisMarkDisplay."</td>";
						$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}
				
				# Subject Overall
				$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
				$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
				$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!="")? $thisMSGrade : "";
				$thisMark = ($ScaleDisplay=="M" && $thisGrade=="") ? $thisMSMark : "";
					
				// for preview
				if(!$StudentID)
				{
					$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
					$thisGrade 	= $ScaleDisplay == "" || $ScaleDisplay=="G" ? "G" : "";
				}
					
				$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
				//if (!$isSub && $thisMark != "" && ($thisMark == "/" || !in_array($thisMark, $this->specialCasesSetExcludeRanking)))
                if (!$isSub && $thisMark != "" && !in_array($thisMark, $this->specialCasesSetExcludeRanking))
				{
					$isAllNA = false;
				}
				
				// Check special case
				if ($isSub)
				{
					$thisMarkDisplay = "&nbsp;";
				}
				else if($StudentID != "")
				{
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]["Grade"]);
					if($needStyle) {
						$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
					}
					else {
						$thisMarkDisplay = $thisMark;
					}
				}
				else
				{
					$thisMarkDisplay = $thisMark;
				}
				
				$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='100%'>".$thisMarkDisplay."</td>";
					$x[$SubjectID] .= "</tr></table>";
				$x[$SubjectID] .= "</td>";
				
				# Subject Attitude
				$thisAttitudeDisplay = $StudentID || $isSub ? "&nbsp;" : "G";
				$thisAttitudeGrade = $SubjectAttitudeAssoAry[$StudentID][$SubjectID]["Info"];
				if (!$isSub && !empty($thisAttitudeGrade)) {
					$thisAttitudeDisplay = $thisAttitudeGrade;
				}
				
				$x[$SubjectID] .= "<td class='border_left'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='100%'>".$thisAttitudeDisplay."</td>";
					$x[$SubjectID] .= "</tr></table>";
				$x[$SubjectID] .= "</td>";
				
				// Construct an array to return
				$returnArr["HTML"][$SubjectID] = $x[$SubjectID];
				$returnArr["isAllNA"][$SubjectID] = $isAllNA;
			}
		}
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID="")
	{
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting["ClassLevelID"];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$SemID = $ReportSetting["Semester"];
		$SemNumber = $this->Get_Semester_Seq_Number($SemID);
		$ReportType = $SemID == "F" ? "W" : "T";
		$isMockExamReport = $FormNumber == 6 && !$ReportSetting["isMainReport"];
		$isConsolidateReport = $ReportType == "W" || ($FormNumber == 6 && $ReportSetting["isMainReport"]);
		
		$DisplayCommentSection = $ReportSetting["DisplayCommentSection"];
		$DisplayAwardSection = $ReportSetting["DisplayAwardSection"];
		$MaxActivityCount = $ReportSetting["MaxActivityCount"];
		$DisplayActivitySection = $ReportSetting["DisplayActivitySection"]; 
		$isPersonalizedReport = $_POST["PrintTemplateType"] == "Personalized";
		
		# Retrieve last Term
		$TermIdAry = array($SemID);
		if($ReportType == "W")
		{
			$SemID = $this->Get_Last_Semester_Of_Report($ReportID);
			
			$TermIdAry = $this->returnReportInvolvedSem($ReportID);
			$TermIdAry = array_keys($TermIdAry);
			$TermIdAry[] = "0";
			
			$termReportAry = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
			$lastReportId = end($termReportAry);
			$lastReportId = $lastReportId["ReportID"];
		}
		else if($isMockExamReport)
		{
// 			$mainTermReport = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $SemID, 1);
// 			if(!empty($mainTermReport))
// 			{
// 				$lastReportId = $mainTermReport["ReportID"];
// 			}
            return array('', '');
		}
		
		$dataAry = array();
		
		# Retrieve Average Summary
		$averageFullMark = $StudentID ? $this->returnGrandAverageFullMark($ReportID, $ClassLevelID) : "S";
		$bestStudentResult = $this->getReportResultScore($ReportID, 0, "", $other_conds = " AND OrderMeritForm = 1 ");
		$bestStudentResult = (array)$bestStudentResult;
		$bestStudentResult = reset($bestStudentResult);
		$dataAry["HighestAverage"] = $StudentID ? $this->Get_Score_Display_HTML($bestStudentResult[0]["GrandAverage"], $ReportID, $ClassLevelID, "", "", "GrandTotal") : "S";
		$dataAry["HighestAverage"] = $dataAry["HighestAverage"]."/".$averageFullMark;
		
		# Retrieve Result Score
		$result = $this->getReportResultScore($ReportID, 0, $StudentID, "", 0, 1);
		$dataAry["YearAverage"] = $StudentID ? $this->Get_Score_Display_HTML($result["GrandAverage"], $ReportID, $ClassLevelID, "", "", "GrandTotal") : "S";
		$dataAry["YearAverage"] = $dataAry["YearAverage"]."/".$averageFullMark;
		$dataAry["LevelPosition"] = $StudentID ? ($result["OrderMeritForm"] > 0 && $result["OrderMeritForm"] <= 30 ? $result["OrderMeritForm"] : "-") : "#";
		if($dataAry["LevelPosition"] != "-" && $dataAry["LevelPosition"] != "#")
		{
			$dataAry["LevelPosition"] = $this->Get_Score_Display_HTML($dataAry["LevelPosition"], $ReportID, $ClassLevelID, "", "", "FormPosition")."/".$result["FormNoOfStudent"];
		}
		
		# Retrieve Other Info
		$OtherInfoDataAry = array();
		$allOtherInfoDataAry = array();
		if ($StudentID != "")
		{
			$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
			$allOtherInfoDataAry = $OtherInfoDataAry[$StudentID];
			$OtherInfoDataAry = $allOtherInfoDataAry[$SemID];
		}
		$dataAry["Points"] = $OtherInfoDataAry["LTPSS Points"] ? $OtherInfoDataAry["LTPSS Points"] : 0;
		$dataAry["Conduct"] = $OtherInfoDataAry["Conduct"] ? $OtherInfoDataAry["Conduct"] : $this->EmptySymbol;
		$dataAry["Promotion"] = $OtherInfoDataAry["Promotion"] ? $OtherInfoDataAry["Promotion"] : "&nbsp;";
		
		# Retrieve eAttendance records
		$attendanceDataAry = array();
		$absentDataCountAry = array();
		if($StudentID != "")
		{
			$profileDataAry = $this->Get_Student_Profile_Data($ReportID, $StudentID, $MapByRecordType=false, $CheckRecordStatus=true, $returnTotalUnit=true, "", $returnAttendanceRecords=true);
			$attendanceDataAry = $profileDataAry["AttendanceAry"][$StudentID];
			$absentDataCountAry= $this->GetStudentAbsentDataCount($ReportID, $StudentID);
		}
// 		$dataAry["DaysAbsent"] = $attendanceDataAry["Days Absent"] ? $attendanceDataAry["Days Absent"] : 0;
// 		$dataAry["EarlyLeave"] = $attendanceDataAry["Early Leave"] ? $attendanceDataAry["Early Leave"] : 0;
		$dataAry["DaysAbsentwReason"] = $absentDataCountAry["wReason"] ? $absentDataCountAry["wReason"] : 0;
		$dataAry["DaysAbsentwoReason"] = $absentDataCountAry["woReason"] ? $absentDataCountAry["woReason"] : 0;
		$dataAry["DaysAbsent"] = $dataAry["DaysAbsentwReason"] + $dataAry["DaysAbsentwoReason"];
		$dataAry["TimesLate"] = $attendanceDataAry["Time Late"] ? $attendanceDataAry["Time Late"] : 0;
		
		# Retrieve eEnrolment records
		$dataAry["EnrolmentClub"] = array();
		if($StudentID != "")
		{
			//$enrolmentDataAry = $this->Get_eEnrolment_Data($StudentID, $TermIdAry);
			$enrolmentDataAry = $this->Get_eEnrolment_Data($StudentID);
			$dataAry["EnrolmentClub"] = Get_Array_By_Key((array)$enrolmentDataAry, "ClubTitle");
			$dataAry["EnrolmentClubRole"] = Get_Array_By_Key((array)$enrolmentDataAry, "RoleTitle");
			//$dataAry["EnrolmentClub"] = asorti((array)$dataAry["EnrolmentClub"]);
		}
		
		# Retrieve OLE Award records
		$OLEAwardAry = array();
// 		if($StudentID != "") {
// 			$OLEAwardAry = $this->Get_iPortfolio_OLE_Records($ReportID, $StudentID);
// 		}
		
		// Merge all Award records
		$dataAry["AwardTitle"] = array();
		$dataAry["AwardReceived"] = array();
		if(!empty($allOtherInfoDataAry))
		{
			foreach((array)$allOtherInfoDataAry as $thisTermOtherInfoDataAry)
			{
				if(!empty($thisTermOtherInfoDataAry["Title"]) || !empty($thisTermOtherInfoDataAry["Award"])) {
    				$dataAry["AwardTitle"] = array_merge($dataAry["AwardTitle"], (array)$thisTermOtherInfoDataAry["Title"]);
    				$dataAry["AwardReceived"] = array_merge($dataAry["AwardReceived"], (array)$thisTermOtherInfoDataAry["Award"]);
				}
			}
		}
		if(!empty($OLEAwardAry))
		{
			$OLEAwardTitleAry = Get_Array_By_Key((array)$OLEAwardAry, "Title");
			$OLEAwardReceivedAry = Get_Array_By_Key((array)$OLEAwardAry, "Award");
			
			$dataAry["AwardTitle"] = array_merge($dataAry["AwardTitle"], (array)$OLEAwardTitleAry);
			$dataAry["AwardTitle"] = asorti($dataAry["AwardTitle"]);
			$dataAry["AwardReceived"] = array_merge($dataAry["AwardReceived"], (array)$OLEAwardReceivedAry);
		}
		
		# Retrieve Class Teacher's Comment
		$CommentAry = $this->returnSubjectTeacherComment(($lastReportId ? $lastReportId : $ReportID), $StudentID);
		$ClassTeacherComment = $CommentAry[0];
		
		$MiscTableColArray = $eRCTemplateSetting["MiscTable"]["Selection"];
		$MiscTableColWidth = $eRCTemplateSetting["ColumnWidth"]["MiscTable"];

		/*
		// [2020-0630-1003-36073] 2019 Yearly Report - Hide "Position in Form" column
		if($this->schoolYear == '2019' && $ReportType == 'W' && !$isPersonalizedReport)
		{
            $key = array_search('LevelPosition', $MiscTableColArray);
            if($key !== false) {
                unset($MiscTableColArray[$key]);
                $MiscTableColArray = array_values($MiscTableColArray);
            }
        }
        */

		# Misc Table
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='60' class='misc_table'>";
		for($i=0; $i<count($MiscTableColArray); $i++)
		{
			$SettingID1 = trim($MiscTableColArray[$i]);
			$SettingID2 = trim($MiscTableColArray[($i + 1)]);
			
			// Term Report - not display Conduct & Promotion
			if(!$isConsolidateReport && $SettingID1 == "Conduct")
			{
			    break;
			}
			
			//$isShowDateUnit = ($i == 0 || $i == 2 || $i == 4);
			$isShowDateUnit = $i == 0;
			$isShowTimeUnit = $i == 2;
			$isShowPointUnit = $i == 4;
			$unitDisplay = $isShowDateUnit ? $eReportCard["Template"]["DaysUnit"] : "&nbsp;";
			$unitDisplay = $isShowTimeUnit ? $eReportCard["Template"]["TimesUnit"] : $unitDisplay;
			$unitDisplay = $isShowPointUnit ? $eReportCard["Template"]["PointsUnit"] : $unitDisplay;
			
			$x .= "<tr>";
//                 if($SettingID1== "DaysAbsentwReason" || $SettingID1== "DaysAbsentwoReason")
//                 {
//                     //$seperator = $SettingID1 == "DaysAbsentwReason" ? " 　" : " ";
//     			    $x .= "<td class='tabletext' valign='middle' colspan='2'>";
//         			    $x .= "<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
//                             $x .= "<tr>";
//                                 $x .= "<td class='tabletext' width='68%'><b>".$eReportCard["Template"][$SettingID1."En"]."</b></td>";
//                                 $x .= "<td class='tabletext' width='32%'><b>".$eReportCard["Template"][$SettingID1."Ch"]."</b></td>";
//         			        $x .= "</tr>";
//     			        $x .= "</table>";
//     			    $x .= "</td>";
//     			}
//     			else
//     			{

                /*
                // [2020-0630-1003-36073]
                if($this->schoolYear == '2019' && $ReportType == 'W' && !$isPersonalizedReport && $SettingID1 == "Promotion")
                {
                    // if without promotion status > hide this row
                    if($dataAry["Promotion"] == "&nbsp;") {
                        break;
                    }

                    $x .= "<td class='tabletext' valign='middle' colspan='3' style='padding-left: 5px;'>";
                        $x .= "<table border='0' cellpadding='0' cellspacing='0'><tr>";
                            $x .= "<td class='tabletext'><b>".$dataAry["Promotion"]."</b></td>";
                        $x .= "</tr></table>";
                    $x .= "</td>";
                }
	 			else
                */

                {
                    $x .= "<td class='tabletext' valign='middle' width='".$MiscTableColWidth[0]."'>";
                        $x .= "<table border='0' cellpadding='0' cellspacing='0'><tr>";
                            $x .= "<td class='tabletext'><b>".$eReportCard["Template"][$SettingID1."En"]."</b></td>";
                        $x .= "</tr></table>";
                    $x .= "</td>";
                    $x .= "<td class='tabletext' valign='middle' width='".$MiscTableColWidth[1]."'>";
                        $x .= "<table border='0' cellpadding='0' cellspacing='0'><tr>";
                            $x .= "<td class='tabletext'><b>".$eReportCard["Template"][$SettingID1."Ch"]."</b></td>";
                        $x .= "</tr></table>";
                    $x .= "</td>";
                    $x .= "<td class='tabletext' valign='middle' width='".$MiscTableColWidth[2]."' align='right' style='padding-right: 5px;'>";
                        $x .= "<table border='0' cellpadding='0' cellspacing='0'><tr>";
                            $x .= "<td class='tabletext'>".$dataAry[$SettingID1]."&nbsp;&nbsp;".$unitDisplay."</td>";
                        $x .= "</tr></table>";
                    $x .= "</td>";
                }
				
// 				if($isConsolidateReport && !$isPersonalizedReport && $SettingID2 == "Promotion")
// 				{
// 		 			$x .= "<td class='tabletext' valign='middle' colspan='3' style='padding-left: 5px;'>";
// 						$x .= "<table border='0' cellpadding='0' cellspacing='0'><tr>";
// 							$x .= "<td class='tabletext'><b>".$dataAry[$SettingID2]."</b></td>";
// 						$x .= "</tr></table>";
// 					$x .= "</td>";
// 				}
// 				else if($isConsolidateReport && $isPersonalizedReport && $i == 0)
// 				{
// 					$x .= "<td class='tabletext' valign='middle' colspan='3' style='padding-left: 5px;'>";
// 						$x .= "<table border='0' cellpadding='0' cellspacing='0'><tr>";
// 							$x .= "<td class='tabletext'><b>".$dataAry["Promotion"]."</b></td>";
// 						$x .= "</tr></table>";
// 					$x .= "</td>";
// 				}
// 				else if($isPersonalizedReport || (!$isConsolidateReport && $SettingID2 == "Promotion"))
// 				{
// 					for($j=3; $j<6; $j++)
// 					{
// 			 			$x .= "<td class='tabletext' valign='middle' width='".$MiscTableColWidth[$j]."'>";
// 							$x .= "<table border='0' cellpadding='0' cellspacing='0'><tr>";
// 								$x .= "<td class='tabletext'>&nbsp;</td>";
// 							$x .= "</tr></table>";
// 						$x .= "</td>";
// 					}
// 				}
				
                // Year Report - Promotion display
				if($isConsolidateReport && ((!$isPersonalizedReport && $SettingID2 == "Promotion") || ($isPersonalizedReport && $i == 0)))
				{
				    $x .= "<td class='tabletext' valign='middle' colspan='3' style='padding-left: 5px;'>";
    				    $x .= "<table border='0' cellpadding='0' cellspacing='0'><tr>";
    				        $x .= "<td class='tabletext'><b>".$dataAry["Promotion"]."</b></td>";
    				    $x .= "</tr></table>";
				    $x .= "</td>";
				}
				// Personalized Report - display empty row
				// Academic Performance Report (Term 2 Report) - display empty row
				// else if($isPersonalizedReport || (!$isConsolidateReport && $SemNumber == 2 && $ReportSetting["isMainReport"]))
                else if($isPersonalizedReport || (!$isConsolidateReport && $SemNumber == 2 && $ReportSetting["isMainReport"]) || $SettingID2 == '')
				{
				    for($j=3; $j<6; $j++)
				    {
				        $x .= "<td class='tabletext' valign='middle' width='".$MiscTableColWidth[$j]."'>";
    				        $x .= "<table border='0' cellpadding='0' cellspacing='0'><tr>";
    				            $x .= "<td class='tabletext'>&nbsp;</td>";
    				        $x .= "</tr></table>";
				        $x .= "</td>";
				    }
				}
	 			else
	 			{
		 			$x .= "<td class='tabletext' valign='middle' width='".$MiscTableColWidth[3]."' style='padding-left: 5px;'>";
						$x .= "<table border='0' cellpadding='0' cellspacing='0'><tr>";
							$x .= "<td class='tabletext'><b>".$eReportCard["Template"][$SettingID2."En"]."</b></td>";
						$x .= "</tr></table>";
					$x .= "</td>";
		 			$x .= "<td class='tabletext' valign='middle' width='".$MiscTableColWidth[4]."'>";
						$x .= "<table border='0' cellpadding='0' cellspacing='0'><tr>";
							$x .= "<td class='tabletext'><b>".$eReportCard["Template"][$SettingID2."Ch"]."</b></td>";
						$x .= "</tr></table>";
					$x .= "</td>";
		 			$x .= "<td class='tabletext' valign='middle' width='".$MiscTableColWidth[5]."'>";
						$x .= "<table border='0' cellpadding='0' cellspacing='0'><tr>";
							$x .= "<td class='tabletext'>".$dataAry[$SettingID2]."</td>";
						$x .= "</tr></table>";
					$x .= "</td>";
	 			}
			$x .= "</tr>";
			
// 			if(!$isConsolidateReport && $SettingID2 == "Promotion")
// 			{
// 				break;
// 			}
			
			$i++;
		}
		$x .= "</table>";
		
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='60' class=''>";
		
		# Class Teacher Comment
		if($DisplayCommentSection && ($StudentID == "" || trim($ClassTeacherComment) != ""))
		{
			// for preview
			if($StudentID == "")
			{
				$ClassTeacherComment = "XXX";
			}
			
			$x .= "<tr>";
	 			$x .= "<td class='tabletext' valign='middle' width='100%'>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0' width='100%' class='other_info_table'>";
						$x .= "<tr>"; 
							$x .= "<td class='tabletext other_info_title' width='100%'>";
                                $x .= "<b>".$eReportCard["Template"]["CommentEn"]." 　".$eReportCard["Template"]["CommentCh"]."</b>";
                            $x .= "</td>";
						$x .= "</tr>";
						$x .= "<tr>"; 
							$x .= "<td class='tabletext'>".stripslashes(nl2br($ClassTeacherComment))."</td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
			$x .= "</tr>";
		}
		
		# Awards and Punishments
		if($DisplayAwardSection)
		{
			// for preview
			if($StudentID == "")
			{
				$dataAry["AwardTitle"] = array("XXX", "XXX");
				$dataAry["AwardReceived"] = array("XXX", "XXX");
			}
			
			$awardTableCotent .= "<tr>";
	 			$awardTableCotent .= "<td class='tabletext' valign='middle' width='100%'>";
					$awardTableCotent .= "<table border='0' cellpadding='0' cellspacing='0' width='100%' class='other_info_table'>";
						$awardTableCotent .= "<tr>"; 
							$awardTableCotent .= "<td class='tabletext other_info_title' colspan='2'>";
                                $awardTableCotent .= "<b>".$eReportCard["Template"]["AwardsEn"]."　　".$eReportCard["Template"]["AwardsCh"]."</b>";
                            $awardTableCotent .= "</td>";
						$awardTableCotent .= "</tr>";
					
					$awardCount = 0;
					if(!empty($dataAry["AwardReceived"]))
					{
    					foreach((array)$dataAry["AwardReceived"] as $thisAwardIndex => $thisAwardReceived)
    					{
    					    $thisAwardTitle = $dataAry["AwardTitle"][$thisAwardIndex];
    					    //$thisAwardTitle = $thisAwardTitle ? "- ".$thisAwardTitle : "&nbsp;";
    					    $thisAwardTitle = $thisAwardTitle ? $thisAwardTitle : "&nbsp;";
    						
    						$awardTableCotent .= "<tr>";
    							$awardTableCotent .= "<td class='tabletext' width='31%'>".$thisAwardReceived."</td>";
    							$awardTableCotent .= "<td class='tabletext' width='69%'>".$thisAwardTitle."</td>";
    						$awardTableCotent .= "</tr>";
    						
    						$awardCount++;
    					}
					}
					else
					{
					    $awardTableCotent .= "<tr>";
    					    $awardTableCotent .= "<td class='tabletext' colspan='2'>--</td>";
					    $awardTableCotent .= "</tr>";
					    
					    $awardCount++;
					}
					$awardTableCotent .= "</table>";
				$awardTableCotent .= "</td>";
			$awardTableCotent .= "</tr>";
			
			if($MaxActivityCount == 0 || ($MaxActivityCount > 0 && $awardCount > 0 && $awardCount <= $MaxActivityCount))
			{
				$x .= $awardTableCotent;
				
				$x2 .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=''>";
					$x2 .= "<tr>";
			 			$x2 .= "<td class='tabletext' align='center' width='100%'>***</td>";
					$x2 .= "</tr>";
				$x2 .= "</table>";
			}
			else
			{
				$x2 .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=''>";
					$x2 .= $awardTableCotent;
				$x2 .= "</table>";
			}
		}
		
		# Activities and Positions of Responsibility
		if($DisplayActivitySection && ($StudentID == "" || !empty($dataAry["EnrolmentClub"])))
		{
			// for preview
			if($StudentID == "")
			{
				$dataAry["EnrolmentClub"] = array("XXX", "XXX", "XXX");
				$dataAry["EnrolmentClubRole"] = array("XXX", "", "XXX");
			}
			
			$x .= "<tr>";
	 			$x .= "<td class='tabletext' valign='middle' width='100%'>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0' width='100%' class='other_info_table'>";
						$x .= "<tr>"; 
							$x .= "<td class='tabletext other_info_title'>";
                                $x .= "<b>".$eReportCard["Template"]["COAEn"]."　　".$eReportCard["Template"]["COACh"]."</b>";
                            $x .= "</td>";
						$x .= "</tr>";
						
						$count = 0;
						$clubCount = count((array)$dataAry["EnrolmentClub"]);
						foreach((array)$dataAry["EnrolmentClub"] as $thisIndex => $thisEnrolClubName)
						{
							$thisEnrolDisplay = $thisEnrolClubName;
							if($dataAry["EnrolmentClubRole"][$thisIndex] != "")
							{
								$thisEnrolDisplay.= " (".$dataAry["EnrolmentClubRole"][$thisIndex].")";
							}
							
							if($count % 2 == 0)
							{
								$x .= "<tr>";
									$x .= "<td class='tabletext' width='51%'>".$thisEnrolDisplay."</td>";
								if($count == $clubCount - 1) {
									$x .= "</tr>";
								}
							}
							else
							{
									$x .= "<td class='tabletext' width='49%'>".$thisEnrolDisplay."</td>";
								$x .= "</tr>";
							}
							
							$count++;
						}
					$x .= "</table>";
				$x .= "</td>";
			$x .= "</tr>";
		}
		$x .= "</table>";
		
		return array($x, $x2);
	}
	
	########### END Template Related

	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# initialization
		$border_top = "";
		$x = "";
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh);
			
		if($ReportType=="W")	# Whole Year Report
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			$thisTotalValue = "";
			foreach($ColumnData as $k1=>$d1)
			{
				# get value of this term
				$TermID = $d1['SemesterNum'];
				
				$thisValue = $StudentID ? ($ValueArr[$TermID][$InfoKey] ? $ValueArr[$TermID][$InfoKey] : $this->EmptySymbol) : "#";
				
				# calculation the overall value
				if (is_numeric($thisValue)) {
					if ($thisTotalValue == "")
						$thisTotalValue = $thisValue;
					else if (is_numeric($thisTotalValue))
						$thisTotalValue += $thisValue;
				}
				
				# insert empty cell for assessments
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					
				# display this term value
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			}
			
			# display overall year value
			if($ShowRightestColumn)
			{
				if($thisTotalValue=="")
					$thisTotalValue = ($ValueArr[0][$InfoKey]=="")? $this->EmptySymbol : $ValueArr[0][$InfoKey];
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisTotalValue."</td>";
			}
		}
		else				# Term Report
		{
			# get value of this term
			$thisValue = $StudentID ? ($ValueArr[$SemID][$InfoKey] ? $ValueArr[$SemID][$InfoKey] : $this->EmptySymbol) : "#";
			
			# insert empty cell for assessments
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				
			# display this term value
			if($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
		}
		
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		$CalOrder = $this->Get_Calculation_Setting($ReportID);
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		$border_top = $isFirst ? "border_top" : "";
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, $isFirst);
		
		if ($CalOrder == 1) {
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
		} else {
			$curColumn = 0;
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				for ($i=0; $i<$NumOfAssessment[$curColumn]-1 ; $i++)
				{
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				}
				
				$thisValue = $ValueArr[$ColumnID];
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisValue."&nbsp;</td>";
				$curColumn++;
			}
		}
		
		if ($ShowRightestColumn)
		{
			$thisValue = $OverallValue;
			/*
			if ($UpperLimit=="" || $UpperLimit==0 || $thisValue <= $UpperLimit)
			{
				$thisDisplay = $thisValue;
			}
			else
			{
				$thisDisplay = $this->EmptySymbol;
			}
			*/
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."&nbsp;</td>";
		}
			
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0)
	{
		$x = "";
		$border_top = ($hasBorderTop)? "border_top" : "";
		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleEn ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleCh ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		
		return $x;
	}
	
	function Is_Show_Rightest_Column($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		return $ShowRightestColumn;
	}
	
	function Get_Calculation_Setting($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		return $CalOrder;
	}
	
		// $border, e.g. "left,right,top"
	function Get_Double_Line_Td($border="",$width=1)
	{
		global $image_path,$LAYOUT_SKIN;
		
		$borderArr = explode(",",$border);
		$left = $right = $top = $bottom = 0;
		
		if(in_array("left",$borderArr)) $left = $width;
		if(in_array("right",$borderArr)) $right = $width;
		if(in_array("top",$borderArr)) $top = $width;
		if(in_array("bottom",$borderArr)) $bottom = $width;

		$style = " style='border-style:solid; border-color:#000; border-width:{$top}px {$right}px {$bottom}px {$left}px ' ";
		
		$td = "<td width='0%' height='0%' $style><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='$width' height='$width'/></td>";
		return $td;
	}
	
	function Get_Student_Display_Photo($thisUserLogin)
	{
		global $intranet_root;
		
		// Default Photo
		$thisStudentPhoto = "<img src='/images/myaccount_personalinfo/samplephoto.gif' width='82px'>";
		
		// Student Photo (UserLogin)
		$user_login_path = $intranet_root."/file/user_photo/".$thisUserLogin.".jpg";
		if(file_exists($user_login_path)) {
			$thisStudentPhoto = "<img src='/file/user_photo/".$thisUserLogin.".jpg' width='82px'>";
		}
		
		return $thisStudentPhoto;
	}
	
	function returnGrandAverageFullMark($ReportID, $ClassLevelID)
    {
        $RC_SUBJECT_FORM_GRADING = $this->DBName.".RC_SUBJECT_FORM_GRADING";
        $RC_GRADING_SCHEME = $this->DBName.".RC_GRADING_SCHEME";
            
        // Retrieve Grading Scheme
        $sql = "SELECT SchemeID FROM $RC_SUBJECT_FORM_GRADING WHERE ReportID = '$ReportID' AND SubjectID = '-1'";
        $result = $this->returnArray($sql);
        $SchemeID = $result[0]["SchemeID"];
        
        // Retrieve Full Mark
        $sql = "SELECT FullMark FROM $RC_GRADING_SCHEME WHERE SchemeID = '$SchemeID'";
        $result = $this->returnArray($sql);
        
       return $result[0]["FullMark"];
    }
    
    function GetStudentAbsentDataCount($ReportID, $StudentID)
    {
        global $PATH_WRT_ROOT;
        include_once ($PATH_WRT_ROOT."includes/form_class_manage.php");
        
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $SemID = $ReportInfoArr['Semester'];
        $ReportType = ($SemID == 'F') ? 'W' : 'T';
        
        // Date range
        $TermStartDate = $ReportInfoArr['TermStartDate'];
        $TermEndDate = $ReportInfoArr['TermEndDate'];
        if (is_date_empty($TermStartDate) && is_date_empty($TermEndDate))
        {
            if ($ReportType == 'T')
            {
                $objYearTerm = new academic_year_term($SemID);
                $TermStartDate = substr($objYearTerm->TermStart, 0, 10);
                $TermEndDate = substr($objYearTerm->TermEnd, 0, 10);
            }
            else if ($ReportType == 'W')
            {
                $YearTermIDArr = Get_Array_By_Key($this->Get_Related_TermReport_Of_Consolidated_Report($ReportID), 'Semester');
                $numOfTerm = count($YearTermIDArr);
                for ($i = 0; $i < $numOfTerm; $i ++)
                {
                    $_yearTermId = $YearTermIDArr[$i];
                    $_objYearTerm = new academic_year_term($_yearTermId);
                    $_termStartDate = substr($_objYearTerm->TermStart, 0, 10);
                    $_termEndDate = substr($_objYearTerm->TermEnd, 0, 10);
                    
                    if ($TermStartDate == '' || $_termStartDate < $TermStartDate) {
                        $TermStartDate = $_termStartDate;
                    }
                    if ($TermEndDate == '' || $_termEndDate > $TermEndDate) {
                        $TermEndDate = $_termEndDate;
                    }
                }
            }
        }
        
        // Class Name
        // $StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
        // $thisClassName = $StudentInfoArr[0]["ClassName"];
        
        // Get Absent Data
        $attendance_data = array();
        if($StudentID)
        {
            include_once($PATH_WRT_ROOT."includes/libuser.php");
            $lu = new libuser($StudentID);
            
            include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
            $lattend = new libcardstudentattend2();
            $attendance_data = $lattend->Get_Search_Report_Data($TermStartDate, $TermEndDate, '', CARD_STATUS_ABSENT, '', '', 2, array($lu->ClassName), array($StudentID));
            $attendance_data = $attendance_data[0];
        }
        
        // Get Absent Statistics
        $attendanceSummaryAry = array();
        if(!empty($attendance_data))
        {
            foreach($attendance_data as $this_data)
            {
                if($this_data['AttendanceType'] == CARD_STATUS_ABSENT)
                {
                    $attendanceSummaryAry['all'] += 0.5;
                    if($this_data['DocumentStatus'] == 1)
                    {
                        $attendanceSummaryAry['wReason'] += 0.5;
                    }
                    else
                    {
                        $attendanceSummaryAry['woReason'] += 0.5;
                    }
                }
            }
        }
        
//         $sumttedDocumentSql = $lattend->getNotSumttedDocumentStudentDataSQL($UserIdAry=array($StudentID), $TermStartDate, $TermEndDate, $Get_document_status='1', $record_type=array(CARD_STATUS_ABSENT), $processedDisplay=false, $dateDelimeter='');
//         $studentAttendanceData = $lattend->returnArray($sumttedDocumentSql);
//         if(!empty($studentAttendanceData))
//         {
//             $attendanceSummaryAry['all'] = $studentAttendanceData[0]['countall'];
//             $attendanceSummaryAry['woReason'] = $studentAttendanceData[0]['countnot'];
//             $attendanceSummaryAry['wReason'] = $attendanceSummaryAry['all'] - $attendanceSummaryAry['woReason'];
//         }
        
        return $attendanceSummaryAry;
    }
}
?>