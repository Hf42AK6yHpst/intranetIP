<?php
# Editing by 
/*
 * Modifications:
 *  2020-02-07  Philips [2019-0201-1139-57235]
 *  			- use other info -> summary -> class teacher instead of Homeroom Teacher
 * 
 * 	2016-03-18	Bill [2016-0308-1439-53071]
 * 				- modify getSignatureTable(), hide parent signature for MS4 Mock Report
 * 
 *  2015-03-20	Bill [2014-1218-1621-37164]
 * 				- modify getMSTable(): skip subject display if current student excluded from subject group 
 * 
 *	2013-04-08	Siuwan 
 *				- modify getMSTable():
 *					1. component subject marks change to italics 
 *					2. add overall column
 *					3. move effort column to the front (IB)
 *				- modify getSignatureTable() getSubjectTeacherCommentPageTable(), 
 *					date "4th" <-- superscript the "th"
 * 		
 */

include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "attendance", "promotion");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 1;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "---";
		$this->leftPadding = 10;
		$this->headerHeight = 170;
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='', $PrintTemplateType='') {
		global $eReportCard, $PATH_WRT_ROOT, $intranet_root;
		
		include_once($PATH_WRT_ROOT."includes/libreportcard2008_file.php");
		$lreportcard_file = new libreportcard_file();
		
		if ($PrintTemplateType == '' || $PrintTemplateType == 'normal') {
			$PrintTemplateType = 'DSE';
		}
		
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$classLevelId = $reportInfoAry['ClassLevelID'];
		$yearTermId = $reportInfoAry['Semester'];
		$reportType = $yearTermId == "F" ? "W" : "T";
		
		$formNumber = $this->GET_FORM_NUMBER($classLevelId, $ByWebSAMSCode=true);
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
		$TableTop .= "<tr><td align='center'>".$TitleTable."</td></tr>";
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		$TableTop .= "</table>";
		
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
//		$pageBreakStyle = '';
//		if ($PrintTemplateType == 'IB') {
//			$pageBreakStyle = "style='page-break-after:always;'";
//		}

		$formNumber = 1;
		// consolidated report and F6 term report have awards part => less height for mark table
		if ($reportType=='W') {
			//2013-0225-1227-23140: (Internal) Follow-up by karsonyam on 2013-07-23 09:16
			$tableTopHeight = 670;
			$miscTableHeight = 280;
		}
		else if ($reportType=='T' && $formNumber==6) {
			//2013-1017-1557-19140
//			$tableTopHeight = 780;
//			$miscTableHeight = 180;
			$tableTopHeight = 720;
			$miscTableHeight = 240;
		}
		else {
			// normal term report
			//2013-1017-1557-19140
//			$tableTopHeight = 830;
//			$miscTableHeight = 117;
			$tableTopHeight = 750;
			$miscTableHeight = 197;
		}
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' $pageBreakStyle>";
				$x .= "<tr height='".$tableTopHeight."px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr height='".$miscTableHeight."	px' valign='top'><td>".$MiscTable."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		//$x .= "</td></tr>";
		$x .= "</td></tr></table></div>";
		
		// back page of grade description
		$imageInfoAry = $lreportcard_file->returnFileInfoAry($ReportID, 'GradeDesc_'.$PrintTemplateType);
		$imagePath = $imageInfoAry[0]['FilePath'];
		$x .= '<div id="container" style="page-break-after:always;">'."\r\n";
			$x .= '<table width="100%" border="0" cellpadding="02" cellspacing="0" valign="top">'."\r\n";
				$x .= '<tr><td style="text-align:left;">'."\r\n";
					$x .= '<br />'."\r\n";
					if ($imagePath != '' && file_exists($intranet_root.$imagePath)) {
						//2013-0225-1227-23140: (Internal) Follow-up by karsonyam on 2013-07-23 09:16
						//$x .= '<img src="'.get_website().$imagePath.'" style="max-width:100%;" />'."\r\n";
						
						if ($PrintTemplateType=='IB') {
							$divMarginAttr = 'padding-top:30px; padding-left:11px;';
							$imageWidthAttr = 'width:673px;';
						}
						else {
							$divMarginAttr = 'padding-top:147px; padding-left:79px;';
							$imageWidthAttr = 'width:400px;';
						}
						$x .= '<div style="'.$divMarginAttr.'"><img src="'.get_website().$imagePath.'" style="'.$imageWidthAttr.'" /></div>'."\r\n";
					} 
					else {
						$x .= '&nbsp;'."\r\n";
					}
				$x .= '</td></tr>'."\r\n";
			$x .= '</table>'."\r\n";
		$x .= '</div>'."\r\n";
		
		
		if ($PrintTemplateType == 'IB') {
			// 1 blank page for pre-printed side
//			$x .= "<tr><td>";
//				$x .= "<table style='page-break-after:always;'><tr><td>&nbsp;</td></tr></table>";
//			$x .= "</td></tr>";
			
			$pageAry = $this->getSubjectTeacherCommentPages($ReportID, $StudentID);
			$numOfPage = count($pageAry);
			for ($i=0; $i<$numOfPage; $i++) {
//				$x .= "<tr><td>";
//					$x .= $pageAry[$i];
//				$x .= "</td></tr>";
				$x .= '<div id="container" style="page-break-after:always;">'."\r\n";
					$x .= '<table width="100%" border="0" cellpadding="02" cellspacing="0" valign="top">'."\r\n";
						$x .= '<tr><td>'."\r\n";
							$x .= $pageAry[$i]."\r\n";
						$x .= '</td></tr>'."\r\n";
					$x .= '</table>'."\r\n";
				$x .= '</div>'."\r\n";
			}
		}
		
		return $x;
	}
	
	function getReportHeader($ReportID, $StudentID='', $PrintTemplateType='') {
		global $PATH_WRT_ROOT, $intranet_root;
		
		if ($PrintTemplateType == '' || $PrintTemplateType == 'normal') {
			$PrintTemplateType = 'DSE';
		}
		
		include_once($PATH_WRT_ROOT."includes/libreportcard2008_file.php");
		$lreportcard_file = new libreportcard_file();
		
		$imageInfoAry = $lreportcard_file->returnFileInfoAry($ReportID, 'ReportHeader');
		$imagePath = $imageInfoAry[0]['FilePath'];
		
		$x = '';
		if ($imagePath != '' && file_exists($intranet_root.$imagePath)) {
			//2013-0225-1227-23140: (Internal) Follow-up by karsonyam on 2013-07-23 09:16
			//$x .= '<img src="'.get_website().$imagePath.'" style="max-width:100%; max-height:'.$this->headerHeight.'px;" />'."\r\n";
			$x .= '<div style="width:100%; padding-top:22px;"><img src="'.get_website().$imagePath.'" style="max-width:100%; height:151px;" /></div>'."\r\n";
		} 
		else {
			$x .= $this->Get_Empty_Image_Div($this->headerHeight)."\r\n";
		}
		
		return $x;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='', $forPage3='', $PageNum=1, $PrintTemplateType='') {
		global $PATH_WRT_ROOT, $eReportCard;
		
		if ($PrintTemplateType == '' || $PrintTemplateType == 'normal') {
			$PrintTemplateType = 'DSE';
		}
		
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$yearTermId = $reportInfoAry['Semester'];
		$reportType = $yearTermId == "F" ? "W" : "T";
		$otherInfoTermId = ($reportType == 'T')? $yearTermId : 0;
		$termStartDate = $reportInfoAry['TermStartDate'];
		$termEndDate = $reportInfoAry['TermEndDate'];
		
		$defaultVal = ($StudentID=='')? "XXX" : '';
		
		// Get Student Info
		if($StudentID)		# retrieve Student Info
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$thisClassName = $StudentInfoArr[0]['ClassName'];
			$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
			$thisClassID = $StudentInfoArr[0]['ClassID'];
			
			$data['StudentName'] = '<span class="text_eng font_10pt">'.$lu->EnglishName.'</span> <span class="text_chi font_10pt">'.$lu->ChineseName.'</span>';
			$data['ClassNo'] = $thisClassNumber;
			$data['Class'] = $thisClassName;
			$data['Gender'] = $lu->Gender;
			
			$curAcademicYearClassName = $lu->ClassName;
		}
		else {
			$data['StudentName'] = $defaultVal;
			$data['ClassNo'] = $defaultVal;
			$data['Class'] = $defaultVal;
			$data['Gender'] = $defaultVal;
			
			$curAcademicYearClassName = '';
		}
		
		// Get Student Other Info Data
		//2013-1017-1557-19140
//		$otherInfoAry = $this->getReportOtherInfoData($ReportID, $StudentID);
//		$otherInfoAry = $otherInfoAry[$StudentID];
		//2014-0825-0929-55194
		//$attendanceInfoAry = $this->Get_Student_Profile_Attendance_Data($yearTermId, $thisClassID, $termStartDate, $termEndDate, $curAcademicYearClassName);
		$attendanceInfoAry = $this->Get_Student_Profile_Attendance_Data($yearTermId, $thisClassID, $termStartDate, $termEndDate, $curAcademicYearClassName, $StudentID);
		$attendanceInfoAry = $attendanceInfoAry[$StudentID];
				
		$x = '';
		$x .= '<table style="width:100%;">'."\r\n";
			$x .= '<tr>'."\r\n";
				$x .= '<td style="width:55%; text-align:center;">'."\r\n";
					$x .= '<span class="title_eng font_10pt"><b>'.$this->splitEnglishWord($eReportCard['Template']['StudentParticularsEn']).'</b></span>'."\r\n";
					$x .= '<span>&nbsp;&nbsp;</span>'."\r\n";
					$x .= '<span class="title_chi font_9pt">'.$this->splitChineseWord($eReportCard['Template']['StudentParticularsCh']).'</span>'."\r\n";
				$x .= '</td>'."\r\n";
				$x .= '<td style="text-align:center;">'."\r\n";
					$x .= '<span class="title_eng font_10pt"><b>'.$this->splitEnglishWord($eReportCard['Template']['AttendanceRecordEn']).'</b></span>'."\r\n";
					$x .= '<span>&nbsp;&nbsp;</span>'."\r\n";
					$x .= '<span class="title_chi font_9pt">'.$this->splitChineseWord($eReportCard['Template']['AttendanceRecordCh']).'</span>'."\r\n";
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$x .= '<tr>'."\r\n";
				// Student Particulars
				$x .= '<td>'."\r\n";
					$x .= '<table style="width:100%;">'."\r\n";
						$x .= $this->getStudentParticularsTr($eReportCard['Template']['NameEn'], $eReportCard['Template']['NameCh'], $data['StudentName']);
						$x .= $this->getStudentParticularsTr($eReportCard['Template']['GenderEn'], $eReportCard['Template']['GenderCh'], $data['Gender']);
						$x .= $this->getStudentParticularsTr($eReportCard['Template']['ClassEn'], $eReportCard['Template']['ClassCh'], $data['Class']);
						$x .= $this->getStudentParticularsTr($eReportCard['Template']['ClassNoEn'], $eReportCard['Template']['ClassNoCh'], $data['ClassNo']);
					$x .= '</table>'."\r\n";
				$x .= '</td>'."\r\n";
				
				// Attendance Record
				$x .= '<td style="vertical-align:top;">'."\r\n";
					$x .= '<table style="width:100%;">'."\r\n";
//						$x .= $this->getAttendanceRecordTr($eReportCard['Template']['NumOfDaysAbsentEn'], $eReportCard['Template']['NumOfDaysAbsentCh'], $otherInfoAry[$otherInfoTermId]['Number of Days Absent']);
//						$x .= $this->getAttendanceRecordTr($eReportCard['Template']['EarlyLeaveEn'], $eReportCard['Template']['EarlyLeaveCh'], $otherInfoAry[$otherInfoTermId]['Early Leave']);
//						$x .= $this->getAttendanceRecordTr($eReportCard['Template']['LatenessEn'], $eReportCard['Template']['LatenessCh'], $otherInfoAry[$otherInfoTermId]['Lateness']);
						$x .= $this->getAttendanceRecordTr($eReportCard['Template']['NumOfDaysAbsentEn'], $eReportCard['Template']['NumOfDaysAbsentCh'], $attendanceInfoAry['Days Absent']);
						$x .= $this->getAttendanceRecordTr($eReportCard['Template']['EarlyLeaveEn'], $eReportCard['Template']['EarlyLeaveCh'], $attendanceInfoAry['Early Leave']);
						$x .= $this->getAttendanceRecordTr($eReportCard['Template']['LatenessEn'], $eReportCard['Template']['LatenessCh'], $attendanceInfoAry['Time Late']);
					$x .= '</table>'."\r\n";
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	function getStudentParticularsTr($TitleEn, $TitleCh, $Data) {
		$x = '';
		$x .= '<tr>'."\r\n";
			$x .= '<td class="title_eng font_10pt" style="width:20%; text-align:right;">'.$TitleEn.'</td>'."\r\n";
			$x .= '<td class="text_chi font_10pt" style="width:20%; text-align:left; padding-left:10px;">'.$TitleCh.' | </td>'."\r\n";
			$x .= '<td class="text_eng font_10pt" style="width:60%; text-align:left;">'.$Data.'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		return $x;
	}
	function getAttendanceRecordTr($TitleEn, $TitleCh, $Data) {
		$x = '';
		$x .= '<tr>'."\r\n";
			$x .= '<td class="title_eng font_10pt" style="width:63%; text-align:right;">'.$TitleEn.'</td>'."\r\n";
			$x .= '<td class="text_chi font_10pt" style="width:27%; text-align:left; padding-left:10px;">'.$TitleCh.' | </td>'."\r\n";
			$x .= '<td class="text_eng font_10pt" style="width:10%; text-align:left;">'.$Data.'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		return $x;
	}
	
	function getMSTable($ReportID, $StudentID='', $PrintTemplateType='', $PageNum='') {
		global $eRCTemplateSetting, $eReportCard;
		
		if ($PrintTemplateType == '' || $PrintTemplateType == 'normal') {
			$PrintTemplateType = 'DSE';
		}
		
		### Retrieve Display Settings
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$classLevelId = $reportInfoAry['ClassLevelID'];
		$formNumber = $this->GET_FORM_NUMBER($classLevelId, $ByWebSAMSCode=true);
		$semID = $reportInfoAry['Semester'];
		$reportType = $semID == "F" ? "W" : "T";
		$showOverallPositionClass = $reportInfoAry['ShowOverallPositionClass'];
		$showOverallPositionForm = $reportInfoAry['ShowOverallPositionForm'];
		$showSubjectComponent = $reportInfoAry['ShowSubjectComponent'];
		
		### Get the Report Info to be displayed
		$reportIdAry = array();
		if ($reportType == 'T') {
			$reportIdAry[] = $ReportID;
		}
		else if ($reportType == 'W') {
			$termReportInfoAry = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
			$reportIdAry = array_values(array_unique(Get_Array_By_Key($termReportInfoAry, 'ReportID')));
			$reportIdAry[] = $ReportID;
			
			// [2014-1218-1621-37164] - get last semester id for checking
			$lastTermID = $this->Get_Last_Semester_Of_Report($ReportID);
		}
		$numOfReport = count($reportIdAry);
		
		### Get data for each report
		$reportColumnAssoAry = array();
		$reportInfoAssoAry = array();
		$numOfTotalReportColumnCount = 0;
		for ($i=0; $i<$numOfReport; $i++)
		{
			$_reportId = $reportIdAry[$i];
			
			// Get Report Info
			$reportInfoAssoAry[$_reportId]['basicInfoAry'] = $this->returnReportTemplateBasicInfo($_reportId);
			
			// Get Report Column Info
			$_specificMarkCalculation = false; 
			if ($reportType == 'T') {
				$reportColumnAssoAry[$_reportId] = Get_Array_By_Key($this->returnReportTemplateColumnData($_reportId), 'ReportColumnID');
				$reportColumnAssoAry[$_reportId][] = 0;
//				$reportColumnAssoAry[$_reportId][2] = 0;		// for 146 dev
			}
			else {
				$_reportYearTermId = $reportInfoAssoAry[$_reportId]['basicInfoAry']['Semester'];
				if ($_reportYearTermId == 'F') {
					// show overall mark for consolidated report
					$reportColumnAssoAry[$_reportId][] = 0;
				}
				else {
					// show assessment mark for 2nd term report
					$_termSequenceNum = $this->Get_Semester_Seq_Number($_reportYearTermId);
//						if ($_termSequenceNum > 1) {	// dev
					if ($_termSequenceNum == 4) {	// client
						$reportColumnAssoAry[$_reportId] = Get_Array_By_Key($this->returnReportTemplateColumnData($_reportId), 'ReportColumnID');
						$_specificMarkCalculation = true;
					}
				}
			}
			
			// Get Grading Schemes of all Subjects
			$reportInfoAssoAry[$_reportId]['gradingSchemeAry'] = $this->GET_SUBJECT_FORM_GRADING($classLevelId, $SubjectID='', $withGrandResult=0, $returnAsso=1, $_reportId);
			
			// Get Student Marks
			// $MarksArr[$SubjectID][$ReportColumnID][Key] = Data
			if ($_specificMarkCalculation) {
				// store special calcualted CA and SA score as the 2nd Term Report score
				$reportInfoAssoAry[$_reportId]['markAry'] = $this->getMarks_consolidateSpecial($ReportID, $StudentID, $reportColumnAssoAry[$_reportId]);
			}
			else {
				$reportInfoAssoAry[$_reportId]['markAry'] = $this->getMarks($_reportId, '', '', 0, 1, '', '', '', $CheckPositionDisplay=0);
			}
			
			// Get Student Grand Marks
			$reportInfoAssoAry[$_reportId]['grandMarkAry'] = $this->getReportResultScore($_reportId, 0, $StudentID);
			
			// Get Student Subject Effort
			$reportInfoAssoAry[$_reportId]['effortAry'] = $this->GET_EXTRA_SUBJECT_INFO(array($StudentID), $SubjectID='', $_reportId);
		}

		// Get Student Other Info Data
		$reportInfoAssoAry[$ReportID]['otherInfoAry'] = $this->getReportOtherInfoData($ReportID, $StudentID);

		### Get all Subjects
		$subjectInOrderArr = $this->returnSubjectwOrder($classLevelId, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);

		### Get Subject Full Marks
		$subjectFullMarkAssoArr = $this->returnSubjectFullMark($classLevelId, 1, array(), 0, $ReportID);

		### Analyze the Marks array to get the colspan of the Subject Teacher Comments cell
		$subjectDisplayInfoAssoAry = array();
		foreach ((array)$subjectInOrderArr as $_parentSubjectId => $_cmpSubjectArr)
		{
			foreach ((array)$_cmpSubjectArr as $__cmpSubjectID => $__cmpSubjectName)
			{
				$isAllNA = true;
				
				$__isComponentSubject = ($__cmpSubjectID == 0) ? false : true;
				$__subjectId = ($__isComponentSubject) ? $__cmpSubjectID : $_parentSubjectId;
				
				if (!$showSubjectComponent && $__isComponentSubject) {
					continue;
				}
				
				// [2014-1218-1621-37164] - skip if current student excluded from subject group 
				// Term Report
				if($semID != 'F' && $this->Get_Student_Studying_Subject_Group($semID, $StudentID, $_parentSubjectId) == null){
					continue;
				} 
				// Consolidated Report
				else if ($semID == 'F' && isset($lastTermID) && $this->Get_Student_Studying_Subject_Group($lastTermID, $StudentID, $_parentSubjectId) == null){
					continue;
				}
				
				$__isAllNA = true;
				$__subjectDisplayInfo = array();
				foreach ((array)$reportColumnAssoAry as $___reportId => $___reportColumnIdAry)
				{
					$___numOfReportColumn = count($___reportColumnIdAry);
					$___markAry = $reportInfoAssoAry[$___reportId]['markAry'];
					$___scaleDisplay = $reportInfoAssoAry[$___reportId]['gradingSchemeAry'][$__subjectId]['ScaleDisplay'];
					
					### Effort
					//2013-0225-1227-23140 (Internal) Follow-up by winniewong on 2013-03-07 15:41
					$__effortDisplayInfo[$___reportId] = ($__isComponentSubject)? $eReportCard['RemarkNotAssessed'] : (!empty($reportInfoAssoAry[$___reportId]['effortAry'][$StudentID][$__subjectId]['Info'])?$reportInfoAssoAry[$___reportId]['effortAry'][$StudentID][$__subjectId]['Info']:$eReportCard['RemarkNotAssessed']);
					
					for ($i=0; $i<$___numOfReportColumn; $i++)
					{
						$____reportColumnId = $___reportColumnIdAry[$i];
						
						if ($reportType == 'T' && $formNumber != 6 && $____reportColumnId == 0) {
							// Only MS4 shows Overall column for Term Report
							continue;
						}
						
						$___msGrade = $___markAry[$StudentID][$__subjectId][$____reportColumnId]['Grade'];
						$___msMark = $___markAry[$StudentID][$__subjectId][$____reportColumnId]['Mark'];
						
						$___grade = ($___scaleDisplay == "G" || $___scaleDisplay == "" || $___msGrade != '') ? $___msGrade : "";
						$___mark = ($___scaleDisplay == "M" && $___grade == '') ? $___msMark : "";
						
						# for preview purpose
						if(!$StudentID) {
							$___mark = $___scaleDisplay == "M" ? "S" : "";
							$___grade = $___scaleDisplay == "G" ? "G" : "";
						}
						$___mark = ($___scaleDisplay == "M" && strlen($___mark)) ? $___mark : $___grade;

						// [2020-0617-1457-37206] Show enrolled subjects on T1T2, T3T4 and Yearly report cards even though both CA and SA marks are N/A.
                        //if ($___mark != '' && $___mark != 'N.A.' && $___mark != $eReportCard['RemarkNotAssessed']) {
                        if ($___mark != '') {
							$__isAllNA = false;
						}
						
						# check special case
						list($___mark, $___needStyle) = $this->checkSpCase($___reportId, $__subjectId, $___mark, $___msGrade);
						if($___needStyle) {
							$___markDisplay = $this->Get_Score_Display_HTML($___mark, $___reportId, $classLevelId, $__subjectId, $___mark);
						}
						else {
							$___markDisplay = $___mark;
						}
						$___markDisplay = ($___markDisplay == '')? $this->EmptySymbol : $___markDisplay;
						
						// 2013-0225-1227-23140: (Internal) Follow-up by karsonyam on 2013-04-25 15:57
						// component subject overall score show N/A for
						// 		1) all consolidated report
						//		2) MS4 term report
						if ($__isComponentSubject && $____reportColumnId==0 && ($reportType=='W' || ($reportType=='T' && $formNumber==6))) {
							$___markDisplay = $eReportCard['RemarkNotAssessed'];
						}
						$__subjectDisplayInfo[$___reportId][$____reportColumnId]['markDisplay'] = $___markDisplay;

						### Effort
						//2013-0225-1227-23140 (Internal) Follow-up by winniewong on 2013-03-07 15:41
						/* if ($____reportColumnId == 0) {
							$__subjectDisplayInfo[$___reportId][$____reportColumnId]['effortDisplay'] = ($__isComponentSubject)? $eReportCard['RemarkNotAssessed'] : $reportInfoAssoAry[$___reportId]['effortAry'][$StudentID][$__subjectId]['Info'];
						} */
						
					}
				}
				
				if ($StudentID == '' || !$__isAllNA)
				{
					$subjectDisplayInfoAssoAry[$__subjectId]['isComponentSubject'] = $__isComponentSubject;
					$subjectDisplayInfoAssoAry[$__subjectId]['subjectNameEn'] = $this->GET_SUBJECT_NAME_LANG($__subjectId, 'en');	
					$subjectDisplayInfoAssoAry[$__subjectId]['subjectNameCh'] = $this->GET_SUBJECT_NAME_LANG($__subjectId, 'b5');
					$subjectDisplayInfoAssoAry[$__subjectId]['markInfoAry'] = $__subjectDisplayInfo;

					//2013-0225-1227-23140 (Internal) Follow-up by winniewong on 2013-03-07 15:41
					### Effort
					$subjectDisplayInfoAssoAry[$__subjectId]['effortDisplay'] = $__effortDisplayInfo;
				}
			}
		}

		$x = '';
		
		// Student Performance
		$x .= $this->getStudentPerformanceTitleTable();

		if ($PrintTemplateType == 'DSE') {
			$assessmentColWidth = 18;
			$numOfCol = 3;
		}
		else if ($PrintTemplateType == 'IB') {
			$assessmentColWidth = 15;
			$numOfCol = 4;
		}
		$subjectColWidth = 100 - ($assessmentColWidth * $numOfCol);
		
		// Mark Table
		$x .= '<table style="width:100%; text-align:center;" cellpadding="1px" cellspacing="0px">'."\r\n";
			$x .= '<tr>'."\r\n";
				// Subject
				$x .= '<td class="border_bottom" style="width:'.$subjectColWidth.'%;">'."\r\n";
					$x .= '<span class="text_eng font_10pt">'.strtoupper($eReportCard['Template']['SubjectEn']).'</span>'."\r\n";
					$x .= '<br />'."\r\n";
					$x .= '<span class="text_chi font_9pt">'.$this->splitChineseWord($eReportCard['Template']['SubjectCh']).'</span>'."\r\n";
				$x .= '</td>'."\r\n";

				//2013-0225-1227-23140 (Internal) Follow-up by winniewong on 2013-03-07 15:41
				// Effort Grade
				//2013-0225-1227-23140: (Internal) Follow-up by karsonyam on 2013-07-30 12:25
				//if ($PrintTemplateType=='IB') {
				if ($reportType == 'T' && $PrintTemplateType == 'IB') {
					$x .= '<td class="border_bottom" style="width:'.$assessmentColWidth.'%;">'."\r\n";
						$x .= '<span class="text_eng font_10pt">'.str_replace(' ', '<br />', strtoupper($eReportCard['Template']['EffortGradeEn'])).'</span>'."\r\n";
						$x .= '<br />'."\r\n";
						$x .= '<span class="text_chi font_9pt">'.$this->splitChineseWord($eReportCard['Template']['EffortGradeCh']).'</span>'."\r\n";
					$x .= '</td>'."\r\n";
				}
				
				// Continuous Assessment
				$x .= '<td class="border_bottom" style="width:'.$assessmentColWidth.'%;">'."\r\n";
					$x .= '<span class="text_eng font_10pt">'.str_replace(' ', '<br />', strtoupper($eReportCard['Template']['ContinuousAssessmentEn'])).'</span>'."\r\n";
					$x .= '<br />'."\r\n";
					$x .= '<span class="text_chi font_9pt">'.$this->splitChineseWord($eReportCard['Template']['ContinuousAssessmentCh']).'</span>'."\r\n";
				$x .= '</td>'."\r\n";
				
				// Summative Assessment
				$x .= '<td class="border_bottom" style="width:'.$assessmentColWidth.'%;">'."\r\n";
					$x .= '<span class="text_eng font_10pt">'.str_replace(' ', '<br />', strtoupper($eReportCard['Template']['SummativeAssessmentEn'])).'</span>'."\r\n";
					$x .= '<br />'."\r\n";
					$x .= '<span class="text_chi font_9pt">'.$this->splitChineseWord($eReportCard['Template']['SummativeAssessmentCh']).'</span>'."\r\n";
				$x .= '</td>'."\r\n";

				// 2013-0225-1227-23140: (Internal) Follow-up by karsonyam on 2013-04-25 15:57
				// show overall column for
				// 		1) all consolidated report
				//		2) MS4 term report
				if ($reportType == 'W' || ($reportType == 'T' && $formNumber == 6)) {
					$x .= '<td class="border_bottom" style="width:'.$assessmentColWidth.'%;">'."\r\n";
						$x .= '<span class="text_eng font_10pt">'.str_replace(' ', '<br />', strtoupper($eReportCard['Template']['OverallEn'])).'</span>'."\r\n";
						$x .= '<br /><br />'."\r\n";
						$x .= '<span class="text_chi font_9pt">'.$this->splitChineseWord($eReportCard['Template']['OverallCh']).'</span>'."\r\n";
					$x .= '</td>'."\r\n";
				}
			$x .= '</tr>'."\r\n";
			
			// Grade Title Row
			$x .= '<tr>'."\r\n";
				$x .= '<td>&nbsp;</td>'."\r\n";

				//2013-0225-1227-23140: (Internal) Follow-up by karsonyam on 2013-07-30 12:25
				//if ($PrintTemplateType=='IB') {
				if ($reportType == 'T' && $PrintTemplateType == 'IB') {
					$x .= '<td>'."\r\n";
						$x .= $eReportCard['Template']['GradePointSymbol'];
						$x .= '<span class="title_eng font_10pt">'.strtoupper($eReportCard['Template']['GradeEn']).'</span>';
						$x .= '<span>&nbsp;</span>';
						$x .= '<span class="text_chi font_9pt">'.$eReportCard['Template']['GradeCh'].'</span>';
						$x .= $eReportCard['Template']['GradePointSymbol'];
					$x .= '</td>'."\r\n";
				}
				$x .= '<td>'."\r\n";
					$x .= $eReportCard['Template']['GradePointSymbol'];
					$x .= '<span class="title_eng font_10pt">'.strtoupper($eReportCard['Template']['GradeEn']).'</span>';
					$x .= '<span>&nbsp;</span>';
					$x .= '<span class="text_chi font_9pt">'.$eReportCard['Template']['GradeCh'].'</span>';
					$x .= $eReportCard['Template']['GradePointSymbol'];
				$x .= '</td>'."\r\n";
				$x .= '<td>'."\r\n";
					$x .= $eReportCard['Template']['GradePointSymbol'];
					$x .= '<span class="title_eng font_10pt">'.strtoupper($eReportCard['Template']['GradeEn']).'</span>';
					$x .= '<span>&nbsp;</span>';
					$x .= '<span class="text_chi font_9pt">'.$eReportCard['Template']['GradeCh'].'</span>';
					$x .= $eReportCard['Template']['GradePointSymbol'];
				$x .= '</td>'."\r\n";

				// 2013-0225-1227-23140: (Internal) Follow-up by karsonyam on 2013-04-25 15:57
				// show overall column for
				// 		1) all consolidated report
				//		2) MS4 term report
				if ($reportType == 'W' || ($reportType == 'T' && $formNumber == 6)) {
					$x .= '<td>'."\r\n";
						$x .= $eReportCard['Template']['GradePointSymbol'];
						$x .= '<span class="title_eng font_10pt">'.strtoupper($eReportCard['Template']['GradeEn']).'</span>';
						$x .= '<span>&nbsp;</span>';
						$x .= '<span class="text_chi font_9pt">'.$eReportCard['Template']['GradeCh'].'</span>';
						$x .= $eReportCard['Template']['GradePointSymbol'];
					$x .= '</td>'."\r\n";
				}
			$x .= '</tr>'."\r\n";
			
			// Subject Rows
			foreach ((array)$subjectDisplayInfoAssoAry as $_subjectId => $_subjectInfoAry)
			{
				$_isSubjectComponent = $_subjectInfoAry['isComponentSubject'];
				$_subjectNameEn = $_subjectInfoAry['subjectNameEn'];
				$_subjectNameCh = $_subjectInfoAry['subjectNameCh'];
				$_markAry = $_subjectInfoAry['markInfoAry'];
				$_effortDisplay = $_subjectInfoAry['effortDisplay'];
			
				//2012-1122-1706-22140
				//(Internal) Follow-up by karsonyam on 2013-01-22 16:04
//				if ($PrintTemplateType == 'DSE' && $_isSubjectComponent) {
//					continue;
//				}

				//2013-1017-1557-19140
				$_subjectNameStyleEn = '';
				$_subjectNameStyleCh = '';
				$_scoreFontClass = 'font_10pt';
				$_scoreStyle = '';
				$_effortStyle = '';
				if ($_isSubjectComponent) {
					$_subjectNameStyleEn = 'font-style:italic; padding-left:20px;';
					$_subjectNameStyleCh = 'font-style:italic;';
					$_scoreFontClass = 'font_9pt';
					
					if ($reportType == 'T') {
						$_paddingLeft = ($PrintTemplateType=='IB')? 19 : 32;
					}
					else {
						$_paddingLeft = 15;
					}
					$_scoreStyle = 'font-style:italic; text-align:left; padding-left:'.$_paddingLeft.'px;';
					
					$_effortStyle = 'font-style:italic; text-align:left;';
				}
				
				// Subject Name
				$_subjectNameDisplayEn = '<span class="title_eng font_10pt" style="'.$_subjectNameStyleEn.'">'.$_subjectNameEn.'</span>';
				$_subjectNameDisplayCh = '<span class="text_chi font_9pt" style="'.$_subjectNameStyleCh.'">'.$_subjectNameCh.'</span>';
				if ($PrintTemplateType == 'DSE') {
					$_subjectNameDisplay = $_subjectNameDisplayEn.' '.$_subjectNameDisplayCh;
				}
				else if ($PrintTemplateType == 'IB') {
					$_subjectNameDisplay = $_subjectNameDisplayEn;
				}
				//if ($_isSubjectComponent) {
				//	$_subjectNameDisplay = '&nbsp;&nbsp;<i>'.$_subjectNameDisplay.'</i>';
				//}

				$x .= '<tr>'."\r\n";
					// Subject Name
					$x .= '<td style="text-align:left; padding-left:'.$this->leftPadding.'px;">'.$_subjectNameDisplay.'</td>'."\r\n";

					// Subject Mark and Effort
					foreach ((array)$_markAry as $__reportId => $__reportMarkAry)
					{
						//2013-0225-1227-23140 - (Internal) Follow-up by winniewong on 2013-03-07 15:41
						//2013-0225-1227-23140: (Internal) Follow-up by karsonyam on 2013-07-30 12:25
						//if ($PrintTemplateType=='IB') {
						if ($reportType == 'T' && $PrintTemplateType == 'IB') {
							//show effort
							$x .= '<td class="title_eng '.$_scoreFontClass.'" style="'.$_scoreStyle.'">'.$_effortDisplay[$__reportId].'</td>'."\r\n";
						}

						foreach ((array)$__reportMarkAry as $___reportColumnId => $___reportColumnMarkAry)
						{
							//2013-0225-1227-23140 (Internal) Follow-up by winniewong on 2013-03-07 15:41
						    /* 	if ($PrintTemplateType == 'DSE' && $___reportColumnId == 0) {
								continue;
							}
							 if ($___reportColumnId == 0) {
								// show effort
								$___markDisplay = $___reportColumnMarkAry['effortDisplay'];
							}else {
						    */
								// show mark
								$___markDisplay = $___reportColumnMarkAry['markDisplay'];
							//}
							//2013-0225-1227-23140 - (Internal) Follow-up by winniewong on 2013-03-07 15:41
							$x .= '<td class="title_eng '.$_scoreFontClass.'" style="'.$_scoreStyle.'">'.$___markDisplay.'</td>'."\r\n";
						}
					}					
				$x .= '</tr>'."\r\n";
			}
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	function genMSTableColHeader($ReportID) {
		
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID) {
		
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array()) {
		
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array()) {
		
	}
	
	function getMiscTable($ReportID, $StudentID='', $PrintTemplateType='') {
		global $eReportCard;
		
		if ($PrintTemplateType == '' || $PrintTemplateType == 'normal') {
			$PrintTemplateType = 'DSE';
		}
		
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$classLevelId = $reportInfoAry['ClassLevelID'];
		$yearTermId = $reportInfoAry['Semester'];
		$reportType = $yearTermId == "F" ? "W" : "T";
		$otherInfoTermId = ($reportType == 'T')? $yearTermId : $this->Get_Last_Semester_Of_Report($ReportID);
		
		$formNumber = $this->GET_FORM_NUMBER($classLevelId, $ByWebSAMSCode=true);
		
		// Get Class Teacher Comment
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$classTeacherComment = nl2br(trim($CommentAry[0]));
		
		// Get Award and Services
//		$maxNumOfAwardDisplay = 5;
		// 2013-0225-1227-23140: (Internal) Follow-up by karsonyam on 2013-04-25 15:57
//		$otherInfoAry = $this->getReportOtherInfoData($ReportID, $StudentID);
//		$awardsAry = $otherInfoAry[$StudentID][$otherInfoTermId]['Awards'];
//		if (!is_array($awardsAry)) {
//			$awardsAry = array($awardsAry);
//		}
//		$servicesAry = $otherInfoAry[$StudentID][$otherInfoTermId]['Services & Activities'];
//		if (!is_array($servicesAry)) {
//			$servicesAry = array($servicesAry);
//		}

		$consolidatedAwardAry = array();
		if ($reportType=='W' || ($reportType=='T' && $formNumber==6)) {
			$studentProfileDataAry = $this->Get_Student_Profile_Data($ReportID, $StudentID, $MapByRecordType=false, $CheckRecordStatus=true, $returnTotalUnit=false, $YearTermID=0);	// $YearTermID = 0 menas retrieve "whole year" data only
			$awardsAry = (array)$studentProfileDataAry['AwardAry'];
			$numOfAward = count($awardsAry);
			$servicesAry = (array)$studentProfileDataAry['ServiceAry'];
			$numOfService = count($servicesAry);
			$activitiesAry = (array)$studentProfileDataAry['ActivityAry'];
			$numOfActivities = count($activitiesAry);
			
			//2013-1017-1557-19140
//			$serviceAndActivitiesAry = array();
//			$counter = 0;
//			for ($i=0; $i<$numOfService; $i++) {
//				$serviceAndActivitiesAry[$counter]['dataName'] = $servicesAry[$i]['serviceName'].' '.$servicesAry[$i]['role'];
//				$counter++;
//			}
//			for ($i=0; $i<$numOfActivities; $i++) {
//				$serviceAndActivitiesAry[$counter]['dataName'] = $activitiesAry[$i]['activityName'].' '.$activitiesAry[$i]['role'];
//				$counter++;
//			}
			
			/*
			 * 1) Awards		Academic Award
			 * 					Subject Prize
			 * 					Conduct Award
			 * 					Excellent Service Award
			 * 					Others awards in alphabetic order
			 * 
			 * 2) Service
			 * 
			 * 3) Activities
			 */
			$tmpAwardAssoAry = array();
			$awardTypePriorityAry = array('Academic Award', 'Subject Prize', 'Conduct Award', 'Excellent Service Award');
			$numOfAwardType = count($awardTypePriorityAry);
			for ($i=0; $i<$numOfAward; $i++) {
				$_awardName = trim($awardsAry[$i]['awardName']);
				
				$_noType = true;
				for ($j=0; $j<$numOfAwardType; $j++) {
					$__awardType = $awardTypePriorityAry[$j];
					
					if (strpos($_awardName, $__awardType) !== false) {
						$tmpAwardAssoAry[$__awardType][] = $_awardName;
						$_noType = false;
						break;
					}
				}
				
				if ($_noType) {
					$tmpAwardAssoAry['Others'][] = $_awardName;
				}
			}
			
			for ($i=0; $i<$numOfAwardType; $i++) {
				$_awardType = $awardTypePriorityAry[$i];
				
				$consolidatedAwardAry = array_merge($consolidatedAwardAry, (array)$tmpAwardAssoAry[$_awardType]);
			}
			$consolidatedAwardAry = array_merge($consolidatedAwardAry, (array)$tmpAwardAssoAry['Others']);
			
			
			for ($i=0; $i<$numOfService; $i++) {
				$consolidatedAwardAry[] = $servicesAry[$i]['serviceName'].' '.$servicesAry[$i]['role'];
			}
			for ($i=0; $i<$numOfActivities; $i++) {
				$consolidatedAwardAry[] = $activitiesAry[$i]['activityName'].' '.$activitiesAry[$i]['role'];
			}
		}
		$numOfConsolidatedAward = count($consolidatedAwardAry);
		
		
		$otherInfoAry = $this->getReportOtherInfoData($ReportID, $StudentID);
		//2013-0225-1227-23140: (Internal) Follow-up by karsonyam on 2013-07-23 13:34
//		$promotionStatusEn = $otherInfoAry[$StudentID][$otherInfoTermId]['Promotion(Eng)'];
//		$promotionStatusCh = $otherInfoAry[$StudentID][$otherInfoTermId]['Promotion(Chi)'];
		$promotionStatusEn = $otherInfoAry[$StudentID][0]['Promotion(Eng)'];
		$promotionStatusCh = $otherInfoAry[$StudentID][0]['Promotion(Chi)'];
		if (is_array($promotionStatusEn)) {
			$promotionStatusEn = $promotionStatusEn[0];
		}
		if (is_array($promotionStatusCh)) {
			$promotionStatusCh = $promotionStatusCh[0];
		}
		
		
		$commentLang = str_lang($classTeacherComment);
		if ($commentLang == 'ENG') {
			$textClass = 'title_eng';
		}
		else {
			$textClass = 'text_chi';
		}
		
		$x = '';
		$x .= '<table style="width:100%; text-align:center;" cellpadding="0px" cellspacing="0px">'."\r\n";
			// Homeroom Teacher's Remarks
			if ($reportType=='T') {
				$x .= '<tr>'."\r\n";
					$x .= '<td>'."\r\n";
						$x .= '<span class="title_eng font_10pt"><b>'.$this->splitEnglishWord($eReportCard['Template']['HomeroomTeachersRemarksEn']).'</b></span>';
						$x .= '&nbsp;&nbsp;';
						$x .= '<span class="text_chi font_10pt">'.$this->splitChineseWord($eReportCard['Template']['HomeroomTeachersRemarksCh']).'</span>';
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
				$x .= '<tr style="height:50px; vertical-align:top;">'."\r\n";
					$x .= '<td class="'.$textClass.'" style="padding-left:'.$this->leftPadding.'px; text-align:left;">'."\r\n";
						$x .= $classTeacherComment;
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			}
			
			// Awards, Services & Activities 
			if ($reportType=='W' || ($reportType=='T' && $formNumber==6)) {
				if ($reportType == 'T') {
					$awardWidth = 60;
					$serviceWidth = 40;
					$promotionWidth = 0;
				}
				else {
					$awardWidth = 51;
					$serviceWidth = 32;
					$promotionWidth = 17;
				}
				 
				
				$x .= '<tr>'."\r\n";
					$x .= '<td>'."\r\n";
						$x .= '<span class="title_eng font_10pt"><b>'.$this->splitEnglishWord($eReportCard['Template']['AwardsServicesAndActivitiesEn']).'</b></span>';
						$x .= '&nbsp;&nbsp;';
						$x .= '<span class="text_chi font_10pt">'.$this->splitChineseWord($eReportCard['Template']['AwardsServicesAndActivitiesCh']).'</span>';
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
				
				$x .= '<tr>'."\r\n";
					$x .= '<td>'."\r\n";
					
					// 2013-0225-1227-23140: (Internal) Follow-up by ivanko on 2013-08-07 17:51
//						$x .= '<table style="width:100%; text-align:left;" cellpadding="0px" cellspacing="0px">'."\r\n";
//							for ($i=0; $i<$maxNumOfAwardDisplay; $i++) {
//								$_award = $awardsAry[$i]['awardName'];
//								$_service = $serviceAndActivitiesAry[$i]['dataName'];
//								
//								$_award = ($_award=='')? '&nbsp;' : $_award;
//								$_service = ($_service=='')? '&nbsp;' : $_service;
//								
//								$_awardLang = str_lang($_award);
//								if ($_awardLang == 'ENG') {
//									$_awardTextClass = 'title_eng';
//								}
//								else {
//									$_awardTextClass = 'text_chi';
//								}
//								
//								$_serviceLang = str_lang($_service);
//								if ($_serviceLang == 'ENG') {
//									$_serviceTextClass = 'title_eng';
//								}
//								else {
//									$_serviceTextClass = 'text_chi';
//								}
//								
//								$x .= '<tr>'."\r\n";
//									$x .= '<td class="'.$_awardTextClass.'" style="width:'.$awardWidth.'%; padding-left:'.$this->leftPadding.'px;">'.$_award.'</td>'."\r\n";
//									$x .= '<td class="'.$_serviceTextClass.'" style="width:'.$serviceWidth.'%;">'.$_service.'</td>'."\r\n";
//									
//									if ($reportType=='W' && $i==0) {
//										$x .= '<td class="title_eng" style="width:'.$promotionWidth.'%; text-align: right;">'.$promotionStatusEn.'</td>'."\r\n";
//									}
//									else if ($reportType=='W' && $i==2) {
//										$x .= '<td class="title_chi" style="width:'.$promotionWidth.'%; text-align: right;">'.$promotionStatusCh.'</td>'."\r\n";
//									}
//									
//								$x .= '</tr>'."\r\n";
//							}
//						$x .= '</table>'."\r\n";
						
						$x .= '<table style="width:100%; text-align:left;" cellpadding="0px" cellspacing="0px">'."\r\n";
							for ($i=0; $i<$numOfConsolidatedAward; $i++) {
								$_award = $consolidatedAwardAry[$i];
								
								$_award = ($_award=='')? '&nbsp;' : $_award;
								$_awardLang = str_lang($_award);
								if ($_awardLang == 'ENG') {
									$_awardTextClass = 'title_eng';
								}
								else {
									$_awardTextClass = 'text_chi';
								}
								
								$x .= '<tr>'."\r\n";
									$x .= '<td class="'.$_awardTextClass.'" style="width:'.$awardWidth.'%; padding-left:'.$this->leftPadding.'px;">'.$_award.'</td>'."\r\n";
								$x .= '</tr>'."\r\n";
							}
						$x .= '</table>'."\r\n";

						$x .= '<table style="width:100%;">'."\r\n";
							$x .= '<tr>'."\r\n";
								//2013-1017-1557-19140
//								$x .= '<td style="width:'.$awardWidth.'%; vertical-align:top;">'."\r\n";
//									$numOfAward = count((array)$awardsAry);
//									$x .= '<table style="width:100%; text-align:left;" cellpadding="0px" cellspacing="0px" valign="top">'."\r\n";
//										for ($i=0; $i<$numOfAward; $i++) {
//											$_award = $awardsAry[$i]['awardName'];
//											$_award = ($_award=='')? '&nbsp;' : $_award;
//											$_awardLang = str_lang($_award);
//											if ($_awardLang == 'ENG') {
//												$_awardTextClass = 'title_eng';
//											}
//											else {
//												$_awardTextClass = 'text_chi';
//											}
//											
//											$x .= '<tr>'."\r\n";
//												$x .= '<td class="'.$_awardTextClass.'" style="width:100%; padding-left:'.$this->leftPadding.'px; padding-right:5px; vertical-align:top;">'.$_award.'</td>'."\r\n";
//											$x .= '</tr>'."\r\n";
//										}
//									$x .= '</table>'."\r\n";
//								$x .= '</td>'."\r\n";
//								
//								$x .= '<td style="width:'.$serviceWidth.'%; vertical-align:top;">'."\r\n";
//									$numOfService = count((array)$serviceAndActivitiesAry);
//									$x .= '<table style="width:100%; text-align:left;" cellpadding="0px" cellspacing="0px" valign="top">'."\r\n";
//										for ($i=0; $i<$numOfService; $i++) {
//											$_service = $serviceAndActivitiesAry[$i]['dataName'];
//											$_service = ($_service=='')? '&nbsp;' : $_service;
//											$_serviceLang = str_lang($_service);
//											if ($_serviceLang == 'ENG') {
//												$_serviceTextClass = 'title_eng';
//											}
//											else {
//												$_serviceTextClass = 'text_chi';
//											}
//											
//											$x .= '<tr>'."\r\n";
//												$x .= '<td class="'.$_serviceTextClass.'" style="width:100%; vertical-align:top;">'.$_service.'</td>'."\r\n";
//											$x .= '</tr>'."\r\n";
//										}
//									$x .= '</table>'."\r\n";
//								$x .= '</td>'."\r\n";
//								
//								if ($reportType=='W') {
//									$x .= '<td style="width:'.$promotionWidth.'%; vertical-align:top;">'."\r\n";
//										$x .= '<table style="width:100%;" cellpadding="0px" cellspacing="0px" valign="top">'."\r\n";
//											$x .= '<tr>'."\r\n";
//												$x .= '<td class="title_eng" style="width:'.$promotionWidth.'%; text-align: right; vertical-align:top;">'.$promotionStatusEn.'</td>'."\r\n";
//											$x .= '</tr>'."\r\n";
//											$x .= '<tr>'."\r\n";
//												$x .= '<td class="title_chi" style="width:'.$promotionWidth.'%; text-align: right; vertical-align:top;">'.$promotionStatusCh.'</td>'."\r\n";
//											$x .= '</tr>'."\r\n";
//										$x .= '</table>'."\r\n";
//									$x .= '</td>'."\r\n";
//								}
							$x .= '</tr>'."\r\n";
						$x .= '</table>'."\r\n";
						
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			}
			
			// Project-based Learning (PBL)
			if($reportType == 'W') {
			    $_title = $otherInfoAry[$StudentID][0]['PBL Title'];
			    $_title = (trim($_title)=='')? '&nbsp;' : $_title;
			    $_titleLang = str_lang($_title);
			    if ($_titleLang == 'ENG') {
			        $_titleTextClass = 'title_eng';
			    } else {
			        $_titleTextClass = 'text_chi';
			    }
			    
			    $_attainment = $otherInfoAry[$StudentID][0]['Attainment'];
			    $_attainment = (trim($_attainment)=='')? '&nbsp;' : $_attainment;
			    $_attainmentLang = str_lang($_attainment);
			    if ($_attainmentLang == 'ENG') {
			        $_attainmentTextClass = 'title_eng';
			    } else {
			        $_attainmentTextClass = 'text_chi';
			    }
			    
			    if($_title == '&nbsp;' && $_attainment == '&nbsp;') {
			        // not display this section
			    }
			    else {
    			    $x .= '<tr>'."\r\n";
                        $x .= '<td>&nbsp;</td>'."\r\n";
    			    $x .= '</tr>'."\r\n";
    			    
    			    if($numOfConsolidatedAward == 0) {
    			        $x .= '<tr>'."\r\n";
                            $x .= '<td>&nbsp;</td>'."\r\n";
    			        $x .= '</tr>'."\r\n";
    			    }
    			    
    			    $x .= '<tr>'."\r\n";
        			    $x .= '<td>'."\r\n";
            			    $x .= '<span class="title_eng font_10pt"><b>'.$this->splitEnglishWord($eReportCard['Template']['PBLEn']).'</b></span>';
            			    $x .= '&nbsp;&nbsp;';
            			    $x .= '<span class="text_chi font_10pt">'.$this->splitChineseWord($eReportCard['Template']['PBLChi']).'</span>';
        			    $x .= '</td>'."\r\n";
    			    $x .= '</tr>'."\r\n";
    			    
    			    $x .= '<tr>';
                        $x .= '<td>';
                            $x .= '<table style="width:100%; text-align:left; cellpadding="0px" cellspacing="0px">'."\r\n";
                                $x .= '<tr>'."\r\n";
                                    $x .= '<td style="padding-left:'.$this->leftPadding.'px;">';
                                        $x .= '<span class="title_eng font_10pt"><b>' . $eReportCard['Template']['PBLTitleEn'] . '</b></span>&nbsp;';
                                        $x .= '<span class="text_chi font_10pt">' . $eReportCard['Template']['PBLTitleCh'] . '</span>&nbsp;:&nbsp;';
                                        $x .= '<span class="'.$_titleTextClass.' font_10pt">' . $_title. '</span>';
                                    $x .= '</td>';
                                $x .= '</tr>';
                                $x .= '<tr>';
                                    $x .= '<td style="padding-left:'.$this->leftPadding.'px;">';
                                        $x .= '<span class="title_eng font_10pt"><b>' . $eReportCard['Template']['AttainmentEn'] . '</b></span>&nbsp;';
                                        $x .= '<span class="text_chi font_10pt">'.$eReportCard['Template']['AttainmentCh'] . '</span>&nbsp;:&nbsp;';
                                        $x .= '<span class="'.$_attainmentTextClass.' font_10pt">' . $_attainment. '</span>';
                                    $x .= '</td>';
                                $x .= '</tr>';
                            $x .= '</table>';
                        $x .= '</td>';
                    $x .= '</tr>';
			    }
			}
			
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	function getSignatureTable($ReportID='', $StudentID='', $PrintTemplateType='') {
		global $eReportCard;
		
		if ($PrintTemplateType == '' || $PrintTemplateType == 'normal') {
			$PrintTemplateType = 'DSE';
		}
		
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$yearTermId = $reportInfoAry['Semester'];
		$reportType = $yearTermId == "F" ? "W" : "T";
		$otherInfoTermId = ($reportType == 'T')? $yearTermId : $this->Get_Last_Semester_Of_Report($ReportID);
		//2012-1122-1706-22140 (Internal) Follow-up by karsonyam on 2013-01-29 15:13
		//$dateOfIssue = date('jS, F, Y', strtotime($reportInfoAry['Issued']));
		//2013-0225-1227-23140 (Internal) Follow-up by winniewong on 2013-03-11 15:05
		//$dateOfIssue = date('jS F Y', strtotime($reportInfoAry['Issued']));
		$dateOfIssue = date('j<\sup>S</\sup> F Y', strtotime($reportInfoAry['Issued']));
		
		// [2016-0308-1439-53071] hide parent signature for MS4 Mock (WebSAMS Code: S6 & Term 4 Report)
		$hiddenParentSignature = false;
		if($reportType == "T"){
			$classLevelID = $reportInfoAry['ClassLevelID'];
			$classLevelNum = $this->GET_FORM_NUMBER($classLevelID,1);
			$semesterSeq = $this->Get_Semester_Seq_Number($yearTermId);
			$hiddenParentSignature = $classLevelNum==6 && $semesterSeq==4;
		}
		
		$classTeacherAry = $this->Get_Student_Class_Teacher_Info($StudentID);
		$classTeacherNameEn = $classTeacherAry[0]['EnglishName'];
		$classTeacherNameCh = $classTeacherAry[0]['ChineseName'];
		
//		$principalAry = $this->Get_Principal_Info();
//		$principalNameEn = $principalAry[0]['EnglishName'];
//		$principalNameCh = $principalAry[0]['ChineseName'];
		
		$imgFile = get_website().'/file/reportcard2008/templates/hkcccu_logos_academy_principal.png';
 		$principalSignature = '<img src="'.$imgFile.'" style="width:170px;" />';	// 4.5cm
		$otherInfoAry = $this->getReportOtherInfoData($ReportID, $StudentID);
		if ($reportType=='T') {
			$conduct = $otherInfoAry[$StudentID][$otherInfoTermId]['Conduct'];
		}
		else {
			$conduct = '&nbsp;';
			
			$promotionStatusEn = $otherInfoAry[$StudentID][0]['Promotion(Eng)'];
			$promotionStatusCh = $otherInfoAry[$StudentID][0]['Promotion(Chi)'];
			if (is_array($promotionStatusEn)) {
				$promotionStatusEn = $promotionStatusEn[0];
			}
			if (is_array($promotionStatusCh)) {
				$promotionStatusCh = $promotionStatusCh[0];
			}
		}
		
		$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
		$referenceNumber = strtoupper($StudentInfoArr[0]['UserLogin']);
		
		$spaceColWidth = 3;
		$schoolChopWidth = 11;
		$conductWidth = 11;
		$parentSignatureWidth = 19;
		$promotionWidth = $schoolChopWidth + $conductWidth + $parentSignatureWidth;
		$classTeacherEng = $otherInfoAry[$StudentID][$otherInfoTermId]['Class Teacher English'];
		$classTeacherChi = $otherInfoAry[$StudentID][$otherInfoTermId]['Class Teacher Chinese'];
 		$x = '';
		$x .= '<table style="width:100%; text-align:center;" cellpadding="0px" cellspacing="0px">'."\r\n";
			$x .= '<tr class="title_eng font_7pt">'."\r\n";
				if ($reportType=='T') {
					// School Chop
					$x .= '<td style="width:'.$schoolChopWidth.'%;">&nbsp;</td>'."\r\n";
					$x .= '<td style="width:'.$spaceColWidth.'%;">&nbsp;</td>'."\r\n";
					
					// Conduct
					$x .= '<td class="text_conduct font_13pt" style="width:'.$conductWidth.'%;"><b>'.$conduct.'</b></td>'."\r\n";
					$x .= '<td style="width:'.$spaceColWidth.'%;">&nbsp;</td>'."\r\n";
					
					// Parent / Guardian Signature
					$x .= '<td style="width:'.$parentSignatureWidth.'%;">&nbsp;</td>'."\r\n";
					$x .= '<td style="width:'.$spaceColWidth.'%;">&nbsp;</td>'."\r\n";
				}
				else {
					//2013-1017-1557-19140
					// promotion status
					$x .= '<td class="font_9pt" style="width:'.$promotionWidth.'%; text-align:left; vertical-align:bottom; padding-left:'.$this->leftPadding.'px;">'.$promotionStatusEn.'<br />'.$promotionStatusCh.'</td>'."\r\n";
					
				}
				
				// Homeroom Teacher
				$x .= '<td style="width:17%;">&nbsp;</td>'."\r\n";
				$x .= '<td style="width:'.$spaceColWidth.'%;">&nbsp;</td>'."\r\n";
				
				// Principal
				$x .= '<td style="width:30%;">'.$principalSignature.'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			
			$x .= '<tr class="title_eng font_7pt">'."\r\n";
				//2013-1017-1557-19140
//				// School Chop
//				if ($reportType=='T') {
//					$border_top = 'border_top_signature';
//					$title = $eReportCard['Template']['SchoolChopEn'];
//				}
//				else {
//					$border_top = '';
//					$title = '&nbsp;';
//				}
//				$x .= '<td class="'.$border_top.'">'.$title.'</td>'."\r\n";
//				$x .= '<td>&nbsp;</td>'."\r\n";
//				
//				// Conduct
//				if ($reportType=='T') {
//					$border_top = 'border_top_signature';
//					$title = $eReportCard['Template']['ConductEn'];
//				}
//				else {
//					$border_top = '';
//					$title = '&nbsp;';
//				}
//				$x .= '<td class="'.$border_top.'">'.$title.'</td>'."\r\n";
//				$x .= '<td>&nbsp;</td>'."\r\n";	
//				
//				// Parent / Guardian Signature
//				if ($reportType=='T') {
//					$border_top = 'border_top_signature';
//					$title = $eReportCard['Template']['ParentGuardianSignatureEn'];
//				}
//				else {
//					$border_top = '';
//					$title = '&nbsp;';
//				}
//				$x .= '<td class="'.$border_top.'">'.$title.'</td>'."\r\n";
//				$x .= '<td>&nbsp;</td>'."\r\n";
				
				if ($reportType=='T') {
					// School Chop
					$x .= '<td class="border_top_signature">'.$eReportCard['Template']['SchoolChopEn'].'</td>'."\r\n";
					$x .= '<td>&nbsp;</td>'."\r\n";
					
					// Conduct
					$x .= '<td class="border_top_signature">'.$eReportCard['Template']['ConductEn'].'</td>'."\r\n";
					$x .= '<td>&nbsp;</td>'."\r\n";	
					
					// Parent / Guardian Signature
					// [2016-0308-1439-53071]
					if($hiddenParentSignature)
					{
						$x .= '<td>&nbsp;</td>'."\r\n";
						$x .= '<td>&nbsp;</td>'."\r\n";
					}
					else
					{
						$x .= '<td class="border_top_signature">'.$eReportCard['Template']['ParentGuardianSignatureEn'].'</td>'."\r\n";
						$x .= '<td>&nbsp;</td>'."\r\n";
					}
				}
				else {
					$x .= '<td>&nbsp;</td>'."\r\n";
				}
				
				
				// Homeroom Teacher for term report, School Chop for consolidated report
				if ($reportType=='T') {
					$title = $eReportCard['Template']['HomeroomTeacherEn'];
				}
				else {
					$title = $eReportCard['Template']['SchoolChopEn'];
				}
				$x .= '<td class="border_top_signature">'.$title.'</td>'."\r\n";
				$x .= '<td>&nbsp;</td>'."\r\n";
				
				// Principal
				$x .= '<td class="border_top_signature">'.$eReportCard['Template']['PrincipalEn'].'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			
			$x .= '<tr class="text_chi font_7pt">'."\r\n";
				//2013-1017-1557-19140
				// School Chop
//				if ($reportType=='T') {
//					$title = $this->splitChineseWord($eReportCard['Template']['SchoolChopCh']);
//				}
//				else {
//					$title = '&nbsp;';
//				}
//				$x .= '<td>'.$title.'</td>'."\r\n";
//				$x .= '<td>&nbsp;</td>'."\r\n";
//				
//				// Conduct
//				if ($reportType=='T') {
//					$title = $this->splitChineseWord($eReportCard['Template']['ConductCh']);
//				}
//				else {
//					$title = '&nbsp;';
//				}
//				$x .= '<td>'.$title.'</td>'."\r\n";
//				$x .= '<td>&nbsp;</td>'."\r\n";
//				
//				// Parent / Guardian Signature
//				if ($reportType=='T') {
//					$title = $this->splitChineseWord($eReportCard['Template']['ParentGuardianSignatureCh']);
//				}
//				else {
//					$title = '&nbsp;';
//				}
//				$x .= '<td>'.$title.'</td>'."\r\n";
//				$x .= '<td>&nbsp;</td>'."\r\n";
				
				if ($reportType=='T') {
					// School Chop
					$title = $this->splitChineseWord($eReportCard['Template']['SchoolChopCh']);
					$x .= '<td>'.$title.'</td>'."\r\n";
					$x .= '<td>&nbsp;</td>'."\r\n";
					
					// Conduct
					$title = $this->splitChineseWord($eReportCard['Template']['ConductCh']);
					$x .= '<td>'.$title.'</td>'."\r\n";
					$x .= '<td>&nbsp;</td>'."\r\n";
					
					// Parent / Guardian Signature
					// [2016-0308-1439-53071]
					if($hiddenParentSignature)
					{
						$x .= '<td>&nbsp;</td>'."\r\n";
						$x .= '<td>&nbsp;</td>'."\r\n";
					}
					else
					{
						$title = $this->splitChineseWord($eReportCard['Template']['ParentGuardianSignatureCh']);
						$x .= '<td>'.$title.'</td>'."\r\n";
						$x .= '<td>&nbsp;</td>'."\r\n";
					}
				}
				else {
					$x .= '<td>&nbsp;</td>'."\r\n";
				}
				
				// Homeroom Teacher for term report, School Chop for consolidated report
				if ($reportType=='T') {
					$class = 'title_eng';
// 					$title = $classTeacherNameEn;
					$title = $classTeacherEng;
				}
				else {
					$class = 'title_chi';
					$title = $this->splitChineseWord($eReportCard['Template']['SchoolChopCh']);
				}
				$x .= '<td class="'.$class.'">'.$title.'</td>'."\r\n";
				$x .= '<td>&nbsp;</td>'."\r\n";
				
				// Principal
				$x .= '<td class="title_eng">'.$eReportCard['Template']['PrincipalNameEn'].'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			
			$x .= '<tr class="text_chi font_7pt">'."\r\n";
				//2013-1017-1557-19140
//				// School Chop
//				$x .= '<td>&nbsp;</td>'."\r\n";
//				$x .= '<td>&nbsp;</td>'."\r\n";
//				
//				// Conduct
//				$x .= '<td>&nbsp;</td>'."\r\n";
//				$x .= '<td>&nbsp;</td>'."\r\n";
//				
//				// Parent / Guardian Signature
//				$x .= '<td>&nbsp;</td>'."\r\n";
//				$x .= '<td>&nbsp;</td>'."\r\n";

				if ($reportType=='T') {
					// School Chop
					$x .= '<td>&nbsp;</td>'."\r\n";
					$x .= '<td>&nbsp;</td>'."\r\n";
					
					// Conduct
					$x .= '<td>&nbsp;</td>'."\r\n";
					$x .= '<td>&nbsp;</td>'."\r\n";
					
					// Parent / Guardian Signature
					$x .= '<td>&nbsp;</td>'."\r\n";
					$x .= '<td>&nbsp;</td>'."\r\n";
				}
				else {
					$x .= '<td>&nbsp;</td>'."\r\n";
				}
				
				// Homeroom Teacher
				if ($reportType=='T') {
					// 					$title = $this->splitChineseWord($eReportCard['Template']['HomeroomTeacherCh']).'&nbsp;&nbsp;'.$this->splitChineseWord($classTeacherNameCh);
					$title = $this->splitChineseWord($eReportCard['Template']['HomeroomTeacherCh']).'&nbsp;&nbsp;'.$this->splitChineseWord($classTeacherChi);
				}
				else {
					$title = '&nbsp;';
				}
				$x .= '<td>'.$title.'</td>'."\r\n";
				$x .= '<td>&nbsp;</td>'."\r\n";
				
				// Principal
				$x .= '<td>'.$this->splitChineseWord($eReportCard['Template']['PrincipalCh']).'&nbsp;&nbsp;'.$this->splitChineseWord($eReportCard['Template']['PrincipalNameCh']).'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		$x .= '<br />'."\r\n";
		
		$x .= '<table class="title_eng font_7pt" style="width:100%; text-align:left;" cellpadding="0px" cellspacing="0px">'."\r\n";
			$x .= '<tr>'."\r\n";
				$x .= '<td>'."\r\n";
					$x .= $eReportCard['Template']['DateOfIssue'].': '.$dateOfIssue;
				$x .= '</td>'."\r\n";
				$x .= '<td style="text-align:right;">'."\r\n";
					$x .= $eReportCard['Template']['Ref'].': '.$referenceNumber;
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	function getSubjectTeacherCommentPages($ReportID, $StudentID='') {
		$numOfSubjectPerPage = 2;
		
		### Retrieve Display Settings
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$classLevelID = $reportInfoAry['ClassLevelID'];
		$semID = $reportInfoAry['Semester'];
		$reportType = $semID == "F" ? "W" : "T";
		$yearTermId = ($reportType=='W')? 0 : $semID;
		
		### Get Subject
		$subjectInOrderArr = $this->returnSubjectwOrder($classLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		
		### Get Subject Description
		$subjectDescAry = $this->returnCurriculumExpectation($classLevelID, $SubjectID='', $yearTermId);
		
		### Get Subject Teacher Comment
		$commentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		
		### Analyze the Marks array to get the colspan of the Subject Teacher Comments cell
		$pageSubjectIdAry = array();
		$curPageNum = 1;
		foreach ((array)$subjectInOrderArr as $_parentSubjectId => $_cmpSubjectArr) {
			foreach ((array)$_cmpSubjectArr as $__cmpSubjectID => $__cmpSubjectName) {
				$__isComponentSubject = ($__cmpSubjectID == 0)? false : true;
				$__subjectId = ($__isComponentSubject)? $__cmpSubjectID : $_parentSubjectId;
				$_subjectComment = $commentAry[$__subjectId];
				
				if ($__isComponentSubject) {
					continue;
				}
				if ($_subjectComment=='') {
					continue;
				}
				
				$numOfSubjectInPage = count($pageSubjectIdAry[$curPageNum]);
				
				if ($numOfSubjectInPage == $numOfSubjectPerPage) {
					$curPageNum++;
				}
				
				$pageSubjectIdAry[$curPageNum][] = $__subjectId;
			}
		}
		
		$totalNumOfPage = count($pageSubjectIdAry);
		
		$pagesAry = array();
		foreach((array)$pageSubjectIdAry as $_pageNum => $_pageSubjectIdAry) {
			$pagesAry[] = $this->getSubjectTeacherCommentPageTable($ReportID, $StudentID, $totalNumOfPage, $_pageNum, $_pageSubjectIdAry, $subjectDescAry, $commentAry);
		}
		
		return $pagesAry;
	}
	
	function getSubjectTeacherCommentPageTable($ReportID, $StudentID, $TotalNumOfPage, $CurPageNum, $SubjectIdAry, $SubjectDescAry, $CommentAry) {
		global $PATH_WRT_ROOT, $eReportCard;
		
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$yearTermId = $reportInfoAry['Semester'];
		$reportType = $yearTermId == "F" ? "W" : "T";
		//2012-1122-1706-22140 (Internal) Follow-up by karsonyam on 2013-01-29 15:13
		$dateOfIssue = date('jS, F, Y', strtotime($reportInfoAry['Issued']));
		//2013-0225-1227-23140 (Internal) Follow-up by winniewong on 2013-03-11 15:05
		//$dateOfIssue = date('jS F Y', strtotime($reportInfoAry['Issued']));
		$dateOfIssue = date('j<\sup>S</\sup> F Y', strtotime($reportInfoAry['Issued']));
		$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
		$referenceNumber = strtoupper($StudentInfoArr[0]['UserLogin']);
		
		$pageDisplay = $eReportCard['Template']['PageOf'];
		$pageDisplay = str_replace('<!--curPageNum-->', $CurPageNum, $pageDisplay);
		$pageDisplay = str_replace('<!--totalPageNum-->', $TotalNumOfPage, $pageDisplay);
		
//		if ($CurPageNum == $TotalNumOfPage) {
//			$pageBreakStyle = '';
//		}
//		else {
//			$pageBreakStyle = 'page-break-after:always;';
//		}
				
		$x = '';
		//$x .= '<table style="width:100%; height:1052px;" cellpadding="0px" cellspacing="0px">'."\r\n";
		$x .= '<table style="width:100%; height:1070px;" cellpadding="0px" cellspacing="0px">'."\r\n";
			//2014-0129-1013-01140
			//$x .= '<tr><td style="vertical-align:top;">';
			$x .= '<tr><td style="vertical-align:top; text-align:center;">';
				$x .= $this->getReportHeader($ReportID, $StudentID);
				$x .= $this->getSubjectTeacherCommentPageStudentInfoTable($ReportID, $StudentID);
				$x .= $this->getStudentPerformanceTitleTable();
				
				$numOfSubject = count($SubjectIdAry);
				for ($i=0; $i<$numOfSubject; $i++) {
					$_subjectId = $SubjectIdAry[$i];
					$_subjectNameEn = $this->GET_SUBJECT_NAME_LANG($_subjectId, 'en');
					$_subjectDesc = nl2br(trim($SubjectDescAry[$_subjectId]));
					$_subjectComment = nl2br(trim($CommentAry[$_subjectId]));
					
					$_subjectTeacherInfoAry = $this->Get_Student_Subject_Teacher($yearTermId, $StudentID, $_subjectId);
					$_subjectTeacherName = $_subjectTeacherInfoAry[0]['EnglishName'];
					
					$x .= '<table style="width:90%;" align="center">'."\r\n";
						$x .= '<tr><td class="text_eng font_12pt" style="text-align:center"><b><u>'.$_subjectNameEn.'</u></b></td></tr>'."\r\n";
						$x .= '<tr><td>&nbsp;</td></tr>'."\r\n";
						$x .= '<tr><td class="text_eng font_11pt"><div style="text-align:justify;">'.$_subjectDesc.'</div></td></tr>'."\r\n";
						$x .= '<tr><td>&nbsp;</td></tr>'."\r\n";
						$x .= '<tr><td class="text_eng font_11pt"><i><div style="text-align:justify;">'.$_subjectComment.'</div></i></td></tr>'."\r\n";
						$x .= '<tr><td class="text_eng font_11pt" style="text-align:right;"><i><b>'.$_subjectTeacherName.'</b></i></td></tr>'."\r\n";
					$x .= '</table>'."\r\n";
					$x .= '<br />'."\r\n";
				}
				
			$x .= '</td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		$x .= '<table class="title_eng font_7pt" style="width:100%; text-align:left; '.$pageBreakStyle.'" cellpadding="0px" cellspacing="0px">'."\r\n";
			$x .= '<tr>'."\r\n";
				$x .= '<td style="width:70%;">'."\r\n";
					$x .= $eReportCard['Template']['DateOfIssue'].': '.$dateOfIssue;
				$x .= '</td>'."\r\n";
				$x .= '<td style="width:17%; text-align:right;">'."\r\n";
					$x .= $eReportCard['Template']['Ref'].': '.$referenceNumber;
				$x .= '</td>'."\r\n";
				$x .= '<td style="width:13%; text-align:right;">'."\r\n";
					$x .= $pageDisplay;
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	function getStudentPerformanceTitleTable() {
		global $eReportCard;
		
		$x = '';
		$x .= '<table style="width:100%; text-align:center;" class="border_top border_bottom">'."\r\n";
			$x .= '<tr><td>'."\r\n";
				$x .= '<span class="title_eng font_10pt"><b>'.$this->splitEnglishWord($eReportCard['Template']['StudentPerformanceEn']).'</b></span>';
				$x .= '<span>&nbsp;&nbsp;</span>'."\r\n";
				$x .= '<span class="title_chi font_9pt">'.$this->splitChineseWord($eReportCard['Template']['StudentPerformanceCh']).'</span>'."\r\n";
			$x .= '</td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	function getSubjectTeacherCommentPageStudentInfoTable($ReportID, $StudentID) {
		global $PATH_WRT_ROOT, $eReportCard;
		
		$defaultVal = ($StudentID=='')? "XXX" : '';
		
		// Get Student Info
		if($StudentID)		# retrieve Student Info
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$thisClassName = $StudentInfoArr[0]['ClassName'];
			$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
			
			$data['StudentName'] = '<span class="text_eng font_10pt">'.$lu->EnglishName.'</span> <span class="text_chi font_10pt">'.$lu->ChineseName.'</span>';
			$data['ClassNo'] = $thisClassNumber;
			$data['Class'] = $thisClassName;
		}
		else {
			$data['StudentName'] = $defaultVal;
			$data['ClassNo'] = $defaultVal;
			$data['Class'] = $defaultVal;
		}
		
		$x = '';
		$x .= '<table style="width:100%;">'."\r\n";
			$x .= '<tr>'."\r\n";
				// Student Name
				$x .= '<td class="title_eng font_10pt" style="width:10%; text-align:right;">'.$eReportCard['Template']['NameEn'].'</td>'."\r\n";
				$x .= '<td class="text_chi font_10pt" style="width:9%; text-align:left; padding-left:10px;">'.$eReportCard['Template']['NameCh'].' | </td>'."\r\n";
				$x .= '<td class="text_eng font_10pt" style="width:32%; text-align:left;">'.$data['StudentName'].'</td>'."\r\n";
				
				// Class Name
				$x .= '<td class="title_eng font_10pt" style="width:8%; text-align:right;">'.$eReportCard['Template']['ClassEn'].'</td>'."\r\n";
				$x .= '<td class="text_chi font_10pt" style="width:9%; text-align:left; padding-left:10px;">'.$eReportCard['Template']['ClassCh'].' | </td>'."\r\n";
				$x .= '<td class="text_eng font_10pt" style="width:6%; text-align:left;">'.$data['Class'].'</td>'."\r\n";
				
				// Class Number
				$x .= '<td class="title_eng font_10pt" style="width:12%; text-align:right;">'.$eReportCard['Template']['ClassNoEn'].'</td>'."\r\n";
				$x .= '<td class="text_chi font_10pt" style="width:9%; text-align:left; padding-left:10px;">'.$eReportCard['Template']['ClassNoCh'].' | </td>'."\r\n";
				$x .= '<td class="text_eng font_10pt" style="width:5%; text-align:left;">'.$data['ClassNo'].'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
			
		return $x;
	}
	########### END Template Related
	
	
	function splitEnglishWord($text) {
		return str_replace(' ', '&nbsp;', trim(chunk_split(strtoupper($text), 1, ' ')));
	}
	function splitChineseWord($text) {
		return str_replace(' ', '&nbsp;', trim(chunk_split($text, 3, ' ')));
	}
	
	function getMarks_consolidateSpecial($consolidatedReportId, $studentId, $termReportColumnAry) {
		$consolidatedReportInfoAry = $this->returnReportTemplateBasicInfo($consolidatedReportId);
		$classLevelId = $consolidatedReportInfoAry['ClassLevelID'];
		
		$consolidatedReportColumnAry = $this->returnReportTemplateColumnData($consolidatedReportId);
		$numOfConsolidatedReportColumn = count((array)$consolidatedReportColumnAry);
		
		$consolidatedReportWeightAry = $this->returnReportTemplateSubjectWeightData($consolidatedReportId, $other_condition="", $SubjectIDArr='', $ReportColumnID='', $convertEmptyToZero=true);
		$consolidatedReportWeightAssoAry = BuildMultiKeyAssoc($consolidatedReportWeightAry, array('SubjectID', 'ReportColumnID'), 'Weight', 1);
		
		$subjectAry = $this->returnSubjectwOrder($classLevelId, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $consolidatedReportId);
		$gradingSchemeAry = $this->GET_SUBJECT_FORM_GRADING($classLevelId, $SubjectID='', $withGrandResult=0, $returnAsso=1, $consolidatedReportId);
		
		$consolidatedReportMarkAry = $this->getMarks($consolidatedReportId, '', '', 0, 1, '', '', '', $CheckPositionDisplay=0);
		$consolidatedReportManualAdjustedMarkAry = $this->Get_Manual_Adjustment($consolidatedReportId, $studentId);
		
		$assessmentTypeAry = array('CA', 'SA');
		$numOfAssessmentType = count((array)$assessmentTypeAry);
		
		//$markAry[$SubjectID][$ReportColumnID][Key] = Data
		$markAry = array();
		foreach ((array)$subjectAry as $_parentSubjectId => $_cmpSubjectArr) {
			foreach ((array)$_cmpSubjectArr as $__cmpSubjectID => $__cmpSubjectName) {
				$isAllNA = true;
				
				$__isComponentSubject = ($__cmpSubjectID == 0)? false : true;
				$__subjectId = ($__isComponentSubject)? $__cmpSubjectID : $_parentSubjectId;
				
				$__schemeId = $gradingSchemeAry[$__subjectId]['SchemeID'];
				
				$__subjectTotalWeightAry = array();
				$__subjectExcludeWeightAry = array();
				$__subjectMarkAry = array();
				for ($i=0; $i<$numOfConsolidatedReportColumn; $i++) {
					if ($i%2 == 0) {
						$___assessmentType = 'CA';
					}
					else {
						$___assessmentType = 'SA';
					}
					
					$___consolidatedReportColumnId = $consolidatedReportColumnAry[$i]['ReportColumnID'];
					$___subjectWeight = $consolidatedReportWeightAssoAry[$__subjectId][$___consolidatedReportColumnId];
					$___rawMark = $consolidatedReportMarkAry[$studentId][$__subjectId][$___consolidatedReportColumnId]['RawMark'];
					$___grade = $consolidatedReportMarkAry[$studentId][$__subjectId][$___consolidatedReportColumnId]['Grade'];
					
					$__subjectTotalWeightAry[$___assessmentType] += $___subjectWeight;
					
					//2014-0925-0910-22202
					//if ($this->Check_If_Grade_Is_SpecialCase($___grade)) {
					if ($this->Check_If_Grade_Is_SpecialCase($___grade) || $___grade == '') {
						$__subjectExcludeWeightAry[$___assessmentType] += $___subjectWeight;
					}
					else {
						$__subjectMarkAry[$___assessmentType] += $___rawMark * $___subjectWeight;
					}
				}
				
				for ($i=0; $i<$numOfAssessmentType; $i++) {
					$___assessmentType = $assessmentTypeAry[$i];
					
					$___termReportColumnId = '';
					if ($___assessmentType == 'CA') {
						$___termReportColumnId = $termReportColumnAry[0];
						$___assessmentCode = '-1';
					}
					else if ($___assessmentType == 'SA') {
						$___termReportColumnId = $termReportColumnAry[1];
						$___assessmentCode = '-2';
					}
					
					if (isset($consolidatedReportManualAdjustedMarkAry[$studentId][$___assessmentCode][$__subjectId]['Score'])) {
						$___totalMark = $consolidatedReportManualAdjustedMarkAry[$studentId][$___assessmentCode][$__subjectId]['Score'];
						$___grade = $consolidatedReportManualAdjustedMarkAry[$studentId][$___assessmentCode][$__subjectId]['Score'];
					}
					else {
						if (!isset($__subjectMarkAry[$___assessmentType])) {
							$___totalMark = 0;
							$___grade = 'N.A.';
						}
						else {
							$___totalWeight = ($__subjectTotalWeightAry[$___assessmentType])? $__subjectTotalWeightAry[$___assessmentType] : 0; 
							$___excludeWeight = ($__subjectExcludeWeightAry[$___assessmentType])? $__subjectExcludeWeightAry[$___assessmentType] : 0;
							$___totalMark = ($__subjectMarkAry[$___assessmentType])? $__subjectMarkAry[$___assessmentType] : 0;
							
							$___remainWeight = $___totalWeight - $___excludeWeight;
							if (($___remainWeight) > 0) {
								$___totalMark = ($___totalMark / ($___remainWeight)) * $___totalWeight;
							}
							
							if ($___totalWeight > 0) {
								$___totalMark = ($___totalMark / $___totalWeight) * 1;
							}
							else {
								$___totalMark = 0;
							}
							
							if ($___excludeWeight > 0 && $___totalWeight == $___excludeWeight) {
								$___grade = 'N.A.';
							}
							else {
								$___grade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($__schemeId, $___totalMark, $___consolidatedReportColumnId, $studentId, $__subjectId, $classLevelId);
							}
						}
					}
					
					$markAry[$studentId][$__subjectId][$___termReportColumnId]['Mark'] = $___totalMark;
					$markAry[$studentId][$__subjectId][$___termReportColumnId]['Grade'] = $___grade;
				}
			}
		}
		
		return $markAry;
	}
}
?>