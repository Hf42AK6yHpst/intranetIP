<?php
# Editing by ivan

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

if ($ReportCardCustomSchoolName=='') {
	$ReportCardCustomSchoolName = 'general';
}
include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "attendance", "remark");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "---";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='') {
		global $eReportCard;
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
		$TableTop .= $this->Get_Empty_Row(30);
		$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
		$TableTop .= $this->Get_Empty_Row(12);
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableTop .= $this->Get_Empty_Row(12);
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		$TableTop .= $this->Get_Empty_Row(12);
		$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='990px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID, $StudentID='')
	{
		global $eReportCard;
		
		$schoolLogoWidth = 170;
		$studentPhotoWidth = 170;
		
		// $imgFile = get_website().'/file/reportcard2008/templates/shing_tak_centre_school.png';
		$imgFile = '/file/reportcard2008/templates/shing_tak_centre_school.png';
		$schoolLogo = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:'.$schoolLogoWidth.'px;">' : '&nbsp;';
		$studentPhoto = $this->Get_Student_Photo_Image($StudentID, $photoHeight=146);
		
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$reportTitle =  $reportInfoAry['ReportTitle'];
		$reportTitle = str_replace(":_:", "<br />", $reportTitle);
		
		$x = '';
		$x .= '<table align="center" style="border:0px; width:100%;">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td style="width:'.$schoolLogoWidth.'px; text-align:center; vertical-align: middle;">'.$schoolLogo.'</td>'."\n";
				$x .= '<td style="text-align:center; vertical-align: top;">'."\n";
					$x .= '<span class="font_14pt">';
						$x .= '<span style="font-weight:bold;">'.strtoupper($eReportCard['Template']['SchoolNameEn']).'</span>';
						$x .= '<br />';
						$x .= $eReportCard['Template']['SchoolNameCh'];
					$x .= '</span>'."\n";
					$x .= '<br />';
					$x .= '<br />';
					$x .= '<span class="font_12pt">';
						$x .= strtoupper($eReportCard['Template']['AcademicReportEn']);
						$x .= '<br />';
						$x .= $eReportCard['Template']['AcademicReportCh'];
						$x .= '<br />';
						$x .= $reportTitle;
					$x .= '</span>'."\n";
				$x .= '</td>'."\n";
				$x .= '<td style="width:'.$studentPhotoWidth.'px; text-align:center; vertical-align: middle;">'.$studentPhoto.'</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		
		# retrieve required variables
		$defaultVal = ($StudentID=='')? "XXX" : '';
		$data['AcademicYear'] = $this->GET_ACTIVE_YEAR_NAME();
		
		$data['DateOfIssue'] = $reportInfoAry['Issued'];
		if ($StudentID)		# retrieve Student Info
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$thisClassName = $StudentInfoArr[0]['ClassName'];
			$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
			
			$data['EnglishName'] = $lu->EnglishName;
			$data['ChineseName'] = $lu->ChineseName;
			$data['ClassNo'] = $thisClassNumber;
			$data['Class'] = $thisClassName;
			$data['STRN'] = $lu->STRN;
			
			if ($lu->Gender == 'M') {
				$data['GenderEn'] = $eReportCard['Template']['MaleEn'];
				$data['GenderCh'] = $eReportCard['Template']['MaleCh'];
			}
			else if ($lu->Gender == 'F') {
				$data['GenderEn'] = $eReportCard['Template']['FemaleEn'];
				$data['GenderCh'] = $eReportCard['Template']['FemaleCh'];
			}
			else {
				$data['GenderEn'] = $this->EmptySymbol;
				$data['GenderCh'] = $this->EmptySymbol;
			}
			
			$data['DateOfBirth'] = $lu->DateOfBirth;
		}
		else {
			$data['EnglishName'] = $defaultVal;
			$data['ChineseName'] = $defaultVal;
			$data['ClassNo'] = $defaultVal;
			$data['Class'] = $defaultVal;
			$data['STRN'] = $defaultVal;
			$data['GenderEn'] = $defaultVal;
			$data['GenderCh'] = $defaultVal;
			$data['DateOfBirth'] = $defaultVal;
		}
		
		$x = '';
		$x .= '<table style="border:0px; width:100%;" class="font_8pt">'."\n";
			/* 20130703 Siuwan [2013-0702-1213-11167]
 			$x .= '<col width="10%" />'."\n";					// Chinese Title
			$x .= '<col width="10%" />'."\n";					// English Title
			$x .= '<col width="8%" />'."\n";					// Colon
			$x .= '<col width="13%" />'."\n";					// Chinese Data
			$x .= '<col width="23%" />'."\n";					// English Data
			$x .= '<col width="10%" />'."\n";					// Chinese Title
			$x .= '<col width="10%" />'."\n";					// English Title
			$x .= '<col width="8%" />'."\n";					// Colon
			$x .= '<col width="8%" />'."\n";					// Data	 */	
			
			$x .= '<col width="10%" />'."\n";					// Chinese Title
			$x .= '<col width="10%" />'."\n";					// English Title
			$x .= '<col width="4%" />'."\n";					// Colon
			$x .= '<col width="8%" />'."\n";					// Chinese Data
			$x .= '<col width="5%" />'."\n";					// Space
			$x .= '<col width="30%" />'."\n";					// English Data
			$x .= '<col width="10%" />'."\n";					// Chinese Title
			$x .= '<col width="10%" />'."\n";					// English Title
			$x .= '<col width="4%" />'."\n";					// Colon
			$x .= '<col width="9%" />'."\n";					// Data
			
			$x .= '<tr>'."\n";
				### Name
				$x .= '<td>'.$eReportCard['Template']['StudentNameCh'].'</td>';
				$x .= '<td>'.$eReportCard['Template']['StudentNameEn'].'</td>';
				$x .= '<td>:</td>';
				$x .= '<td>'.$data['ChineseName'].'</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>('.$data['EnglishName'].')</td>';
				### STRN
				$x .= '<td>'.$eReportCard['Template']['STRNCh'].'</td>';
				$x .= '<td>'.$eReportCard['Template']['STRNEn'].'</td>';
				$x .= '<td>:</td>';
				$x .= '<td>'.$data['STRN'].'</td>';
			$x .= '</tr>'."\n";
			
			$x .= '<tr>'."\n";
				### Sex
				$x .= '<td>'.$eReportCard['Template']['SexCh'].'</td>';
				$x .= '<td>'.$eReportCard['Template']['SexEn'].'</td>';
				$x .= '<td>:</td>';
				$x .= '<td>'.$data['GenderCh'].'</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>('.$data['GenderEn'].')</td>';
				### Class Name
				$x .= '<td>'.$eReportCard['Template']['ClassNameCh'].'</td>';
				$x .= '<td>'.$eReportCard['Template']['ClassNameEn'].'</td>';
				$x .= '<td>:</td>';
				$x .= '<td>'.$data['Class'].'</td>';
			$x .= '</tr>'."\n";
			
			$x .= '<tr>'."\n";
				### Date of Birth
				$x .= '<td>'.$eReportCard['Template']['DateOfBirthCh'].'</td>';
				$x .= '<td>'.$eReportCard['Template']['DateOfBirthEn'].'</td>';
				$x .= '<td>:</td>';
				$x .= '<td colspan="2">'.$data['DateOfBirth'].'</td>';
				//$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				### Class No
				$x .= '<td>'.$eReportCard['Template']['ClassNoCh'].'</td>';
				$x .= '<td>'.$eReportCard['Template']['ClassNoEn'].'</td>';
				$x .= '<td>:</td>';
				$x .= '<td>'.$data['ClassNo'].'</td>';
			$x .= '</tr>'."\n";
			
			$x .= '<tr>'."\n";
				### Blank
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				### Date of Issue
				$x .= '<td>'.$eReportCard['Template']['DateOfIssueCh'].'</td>';
				$x .= '<td>'.$eReportCard['Template']['DateOfIssueEn'].'</td>';
				$x .= '<td>:</td>';
				$x .= '<td>'.$data['DateOfIssue'].'</td>';
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $reportInfoAry['ClassLevelID'];
		$SemID = $reportInfoAry['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode=true);
				
		
		### Get Report Columns
		$ReportColumnInfoArr = array();
		$ReportColumnInfoArr[] = array("ReportColumnID" => 0);
		$numOfReportColumn = count($ReportColumnInfoArr);
		
		### Get all Subjects
		$SubjectInOrderArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		
		### Get Grading Schemes of all Subjects
		$GradingSchemeArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID='', $withGrandResult=0, $returnAsso=1, $ReportID);
		
		### Get Student Marks
		// $MarksArr[$SubjectID][$ReportColumnID][Key] = Data
		$MarksArr = $this->getMarks($ReportID, $StudentID, '', 0, 1, '', '', 0, 0);		// Get the overall column only
		
		### Get Student Grand Marks
		$GrandMarksArr = $this->getReportResultScore($ReportID, 0, $StudentID);
		$GrandAverage = $StudentID ? $this->Get_Score_Display_HTML($GrandMarksArr['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		$ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($GrandMarksArr['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
  		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($GrandMarksArr['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
  			
		### Get Subject Full Marks
		$SubjectFullMarkAssoArr = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		
		### Get Other Info Data
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
		$TimesLate = trim($OtherInfoDataAry[$StudentID][$SemID]['Times Late']);
		$TimesLate = ($TimesLate=='')? 0 : $TimesLate;
		$DaysLeave = trim($OtherInfoDataAry[$StudentID][$SemID]['Days Leave']);
		$DaysLeave = ($DaysLeave=='')? 0 : $DaysLeave;
		$DaysAbsent = trim($OtherInfoDataAry[$StudentID][$SemID]['Days Absent']);
		$DaysAbsent = ($DaysAbsent=='')? 0 : $DaysAbsent;
				
		
		### Analyze the Marks array to get the colspan of the Subject Teacher Comments cell
		$subjectDisplayInfoAssoAry = array();
		foreach ((array)$SubjectInOrderArr as $_parentSubjectId => $_cmpSubjectArr) {
			foreach ((array)$_cmpSubjectArr as $__cmpSubjectID => $__cmpSubjectName) {
				$isAllNA = true;
				
				$__isComponentSubject = ($__cmpSubjectID == 0)? false : true;
				$__subjectId = ($__isComponentSubject)? $__cmpSubjectID : $_parentSubjectId;
				$__scaleDisplay = $GradingSchemeArr[$__subjectId]['ScaleDisplay'];
				
				if ($__isComponentSubject) {
					// hide component subjects
					continue;
				}
				
				$__isAllNA = true;
				$__subjectDisplayInfo = array();
				for ($i=0; $i<$numOfReportColumn; $i++) {
					$___reportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];
					
					$___msGrade = $MarksArr[$__subjectId][$___reportColumnID]['Grade'];
					$___msMark = $MarksArr[$__subjectId][$___reportColumnID]['Mark'];
					
					$___grade = ($__scaleDisplay=="G" || $__scaleDisplay=="" || $___msGrade!='' )? $___msGrade : "";
					$___mark = ($__scaleDisplay=="M" && $___grade=='') ? $___msMark : "";	
					
					# for preview purpose
					if(!$StudentID) {
						$thisMark 	= $__scaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $__scaleDisplay=="G" ? "G" : "";
					}
					
					$___mark = ($__scaleDisplay=="M" && strlen($___mark)) ? $___mark : $___grade;
					
					if ($___mark != '' && $___mark != 'N.A.') {
						$__isAllNA = false;
					}
					
					# check special case
					list($___mark, $___needStyle) = $this->checkSpCase($ReportID, $__subjectId, $___mark, $___msGrade);
					if($___needStyle) {
						$___markDisplay = $this->Get_Score_Display_HTML($___mark, $ReportID, $ClassLevelID, $__subjectId, $___mark);
					}
					else {
						$___markDisplay = $thisMark;
					}
					$___markDisplay = ($___markDisplay == '')? $this->EmptySymbol : $___markDisplay;
					
					$__subjectDisplayInfo[$___reportColumnID]['markDisplay'] = $___markDisplay;
				}
				
				if ($StudentID=='' || !$__isAllNA) {
					$subjectDisplayInfoAssoAry[$__subjectId]['chineseName'] = $this->GET_SUBJECT_NAME_LANG($__subjectId, 'b5');
					$subjectDisplayInfoAssoAry[$__subjectId]['englishName'] = $this->GET_SUBJECT_NAME_LANG($__subjectId, 'en');
					$subjectDisplayInfoAssoAry[$__subjectId]['fullMark'] = $SubjectFullMarkAssoArr[$__subjectId];
					$subjectDisplayInfoAssoAry[$__subjectId]['markInfoAry'] = $__subjectDisplayInfo;
				}
			}
		}
		
		
		$maxNumOfRow = 15;
		$leftPadding = 6;
		
		$x = '';
		$x .= '<table class="border_table font_8pt" style="width:100%;">'."\n";
			$x .= '<tr class="header_grey_bg">'."\n";
				$x .= '<td class="border_top_thick border_left_thick" style="width:150px; padding-left:'.$leftPadding.';">'.$eReportCard['Template']['SubjectCh'].'</td>'."\n";
				$x .= '<td class="border_top_thick">'.$eReportCard['Template']['SubjectEn'].'</td>'."\n";
				$x .= '<td class="border_top_thick border_left_thick" style="width:90px; text-align:center;">'.$eReportCard['Template']['FullMarkCh'].'<br />'.$eReportCard['Template']['FullMarkEn'].'</td>'."\n";	// 2.5cm
				$x .= '<td class="border_top_thick border_left_thick border_right_thick" style="width:90px; text-align:center;">'.$eReportCard['Template']['ResultsCh'].'<br />'.$eReportCard['Template']['ResultsEn'].'</td>'."\n";		// 2.5cm
			$x .= '</tr>'."\n";
			
			### Subject Mark Info
			$rowCount = 0;
			foreach ((array)$subjectDisplayInfoAssoAry as $_subjectId => $_subjectDisplayInfoAry) {
				$_subjectNameCh = $_subjectDisplayInfoAry['chineseName'];
				$_subjectNameEn = $_subjectDisplayInfoAry['englishName'];
				$_subjectFullMark = $_subjectDisplayInfoAry['fullMark'];
				
				if ($rowCount >= $maxNumOfRow) {
					break;
				}
				
				$border_top = ($rowCount==0)? 'border_top_thick' : '';
				$bg_class = ($rowCount % 2 == 1)? 'row_grey_bg' : '';
				
				$x .= '<tr class="'.$bg_class.'">'."\n";
					$x .= '<td class="border_left_thick '.$border_top.'" style="padding-left:'.$leftPadding.';">'.$_subjectNameCh.'</td>'."\n";
					$x .= '<td class="'.$border_top.'">'.$_subjectNameEn.'</td>'."\n";
					$x .= '<td class="border_left_thick '.$border_top.'" style="text-align:center;">'.$_subjectFullMark.'</td>'."\n";
					
					foreach ((array)$_subjectDisplayInfoAry['markInfoAry'] as $__reportColumnId => $__columnMarkAry) {
						$__markDisplay = ($__columnMarkAry['markDisplay']=='')? '&nbsp;' : $__columnMarkAry['markDisplay']; 
						$x .= '<td class="border_left_thick border_right_thick '.$border_top.'" style="text-align:center;">'.$__markDisplay.'</td>'."\n";
					}
				$x .= '</tr>'."\n";
				
				$rowCount++;
			}
			
			$numOfEmptyRow = $maxNumOfRow - $rowCount;
			for ($i=0; $i<$numOfEmptyRow; $i++) {
				$bg_class = ($rowCount % 2 == 1)? 'row_grey_bg' : '';
				
				$x .= '<tr>'."\n";
					$x .= '<td class="border_left_thick '.$bg_class.'">&nbsp;</td>'."\n";
					$x .= '<td class="'.$bg_class.'">&nbsp;</td>'."\n";
					$x .= '<td class="border_left_thick '.$bg_class.'">&nbsp;</td>'."\n";
					for ($j=0; $j<$numOfReportColumn; $j++) {
						$x .= '<td class="border_left_thick border_right_thick '.$bg_class.'">&nbsp;</td>'."\n";
					}
				$x .= '</tr>'."\n";
				
				$rowCount++;
			}
			
			### Grand Average
			$x .= '<tr>'."\n";
				$x .= '<td class="border_left_thick border_top_thick" style="padding-left:'.$leftPadding.';">'.$eReportCard['Template']['AverageCh'].'</td>'."\n";
				$x .= '<td class="border_top_thick">'.$eReportCard['Template']['AverageEn'].'</td>'."\n";
				$x .= '<td class="border_top_thick border_left_thick" style="text-align:center;">100</td>'."\n";
				$x .= '<td class="border_top_thick border_left_thick border_right_thick" style="text-align:center;">'.$GrandAverage.'</td>'."\n";
			$x .= '</tr>'."\n";
			
			### Position in Class
			$x .= '<tr class="row_grey_bg">'."\n";
				$x .= '<td class="border_left_thick" style="padding-left:'.$leftPadding.';">'.$eReportCard['Template']['PositionInClassCh'].'</td>'."\n";
				$x .= '<td>'.$eReportCard['Template']['PositionInClassEn'].'</td>'."\n";
				$x .= '<td class="border_left_thick" style="text-align:center;">'.$this->EmptySymbol.'</td>'."\n";
				$x .= '<td class="border_left_thick border_right_thick" style="text-align:center;">'.$ClassPosition.'</td>'."\n";
			$x .= '</tr>'."\n";
			
			### Position in Form
			$x .= '<tr>'."\n";
				$x .= '<td class="border_left_thick" style="padding-left:'.$leftPadding.';">'.$eReportCard['Template']['PositionInFormCh'].'</td>'."\n";
				$x .= '<td>'.$eReportCard['Template']['PositionInFormEn'].'</td>'."\n";
				$x .= '<td class="border_left_thick" style="text-align:center;">'.$this->EmptySymbol.'</td>'."\n";
				$x .= '<td class="border_left_thick border_right_thick" style="text-align:center;">'.$FormPosition.'</td>'."\n";
			$x .= '</tr>'."\n";
			
			### Times Late
			$x .= '<tr class="row_grey_bg">'."\n";
				$x .= '<td class="border_left_thick" style="padding-left:'.$leftPadding.';">'.$eReportCard['Template']['TimesLateCh'].'</td>'."\n";
				$x .= '<td>'.$eReportCard['Template']['TimesLateEn'].'</td>'."\n";
				$x .= '<td class="border_left_thick" style="text-align:center;">'.$this->EmptySymbol.'</td>'."\n";
				$x .= '<td class="border_left_thick border_right_thick" style="text-align:center;">'.$TimesLate.'</td>'."\n";
			$x .= '</tr>'."\n";
			
			### Days Leave
			$x .= '<tr>'."\n";
				$x .= '<td class="border_left_thick" style="padding-left:'.$leftPadding.';">'.$eReportCard['Template']['DaysLeaveCh'].'</td>'."\n";
				$x .= '<td>'.$eReportCard['Template']['DaysLeaveEn'].'</td>'."\n";
				$x .= '<td class="border_left_thick" style="text-align:center;">'.$this->EmptySymbol.'</td>'."\n";
				$x .= '<td class="border_left_thick border_right_thick" style="text-align:center;">'.$DaysLeave.'</td>'."\n";
			$x .= '</tr>'."\n";
			
			### Days Absent
			$x .= '<tr class="row_grey_bg">'."\n";
				$x .= '<td class="border_left_thick border_bottom_thick" style="padding-left:'.$leftPadding.';">'.$eReportCard['Template']['DaysAbsentCh'].'</td>'."\n";
				$x .= '<td class="border_bottom_thick">'.$eReportCard['Template']['DaysAbsentEn'].'</td>'."\n";
				$x .= '<td class="border_left_thick border_bottom_thick" style="text-align:center;">'.$this->EmptySymbol.'</td>'."\n";
				$x .= '<td class="border_left_thick border_right_thick border_bottom_thick" style="text-align:center;">'.$DaysAbsent.'</td>'."\n";
			$x .= '</tr>'."\n";
			
			### Elective
			if ($FormNumber >= 4) {
				$x .= '<tr>'."\n";
					$x .= '<td style="padding-left:'.($leftPadding-2).';">&nbsp;*&nbsp;&nbsp;'.$eReportCard['Template']['ElectiveCh'].'</td>'."\n";
					$x .= '<td>*&nbsp;'.$eReportCard['Template']['ElectiveEn'].'</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
				$x .= '</tr>'."\n";
			}
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function genMSTableColHeader($ReportID)
	{
		
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		
	}
	
	function getSignatureTable($ReportID='', $StudentID='')
	{
		global $eReportCard;
		
		$principalInfoAry = $this->Get_Principal_Info();
		$principalNameAry = Get_Array_By_Key($principalInfoAry, 'ChineseName');
		$principalNameDisplay = implode(' / ', $principalNameAry);
		
		$classTeacherInfoAry = $this->Get_Student_Class_Teacher_Info($StudentID);
		$numOfClassTeacher = count($classTeacherInfoAry);
		
		$classTeacherNameAry = Get_Array_By_Key($classTeacherInfoAry, 'ChineseName');
		$classTeacherNameDisplay = implode(' / ', $classTeacherNameAry);
		
		
		if ($numOfClassTeacher > 1) {
			$classTeacherEn = $eReportCard['Template']['ClassTeacherEn'].'s';
		}
		else {
			$classTeacherEn = $eReportCard['Template']['ClassTeacherEn'];
		}
				
 		$x = '';
 		$x .= '<table class="font_8pt" style="width:100%;">'."\n";
 			$x .= '<tr>'."\n";
 				$x .= '<td class="border_top" style="width:30%; text-align:center; font-weight:bold;">'.strtoupper($eReportCard['Template']['PrincipalEn']).'</td>'."\n";
 				$x .= '<td style="width:5%;">&nbsp;</td>';
 				$x .= '<td class="border_top" style="width:30%; text-align:center; font-weight:bold;">'.strtoupper($classTeacherEn).'</td>'."\n";
 				$x .= '<td style="width:5%;">&nbsp;</td>';
 				$x .= '<td class="border_top" style="width:30%; text-align:center; font-weight:bold;">'.strtoupper($eReportCard['Template']['ParentEn'].' / '.$eReportCard['Template']['GuardianEn']).'</td>'."\n";
 			$x .= '</tr>'."\n";
 			
 			$x .= '<tr>'."\n";
 				$x .= '<td style="text-align:center;">'.$eReportCard['Template']['PrincipalCh'].'</td>'."\n";
 				$x .= '<td>&nbsp;</td>';
 				$x .= '<td style="text-align:center;">'.$eReportCard['Template']['ClassTeacherCh'].'</td>'."\n";
 				$x .= '<td>&nbsp;</td>';
 				$x .= '<td style="text-align:center;">'.$eReportCard['Template']['ParentCh'].' / '.$eReportCard['Template']['GuardianCh'].'</td>'."\n";
 			$x .= '</tr>'."\n";
 			
 			$x .= '<tr>'."\n";
 				$x .= '<td style="text-align:center;">'.$principalNameDisplay.'</td>'."\n";
 				$x .= '<td>&nbsp;</td>';
 				$x .= '<td style="text-align:center;">'.$classTeacherNameDisplay.'</td>'."\n";
 				$x .= '<td>&nbsp;</td>';
 				$x .= '<td>&nbsp;</td>'."\n";
 			$x .= '</tr>'."\n";
 		$x .= '</table>'."\n";
		//20130704 Siuwan [2013-0704-0916-05066]
		$x .= $this->Get_Empty_Image_Div('12px');
 		$x .= '<table class="font_8pt" style="width:100%;">'."\n";
 			$x .= '<tr>'."\n";
 				$x .= '<td>'.$eReportCard['Template']['FailMarkRemark'].'</td>'."\n";
 				
 				$x .= '<td>&nbsp;</td>'."\n";
 			$x .= '</tr>'."\n";
 		$x .= '</table>'."\n";
 		return $x;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{

	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $reportInfoAry['ClassLevelID'];
		$SemID = $reportInfoAry['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		
		### Get Other Info Data
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
		$Conduct = trim($OtherInfoDataAry[$StudentID][$SemID]['Conduct']);
		$Conduct = ($Conduct=='')? $this->EmptySymbol : $Conduct;
/* 		20130704 Siuwan [2013-0704-1034-16073] 
		$AcademicPerformance = trim($OtherInfoDataAry[$StudentID][$SemID]['Academic Performance']);
		$AcademicPerformance = ($AcademicPerformance=='')? $this->EmptySymbol : $AcademicPerformance;
		$PersonalDevelopment = trim($OtherInfoDataAry[$StudentID][$SemID]['Personal Development']);
		$PersonalDevelopment = ($PersonalDevelopment=='')? $this->EmptySymbol : $PersonalDevelopment; */ 
		$AcademicPerformanceArr = (array)$OtherInfoDataAry[$StudentID][$SemID]['Academic Performance'];
		$AcademicPerformance = '';
		if(!empty($AcademicPerformanceArr[0])){
			for($i=0;$i<2;$i++){
				$AcademicPerformance .= ($i>0)?'<br/>':'';
				$AcademicPerformance .= trim($AcademicPerformanceArr[$i]);
			}
		}
		$AcademicPerformance = ($AcademicPerformance=='')? $this->EmptySymbol.'<br/>' : $AcademicPerformance;	
		
		$PersonalDevelopmentArr = (array)$OtherInfoDataAry[$StudentID][$SemID]['Personal Development'];
		$PersonalDevelopment = '';
		if(!empty($PersonalDevelopmentArr[0])){
			for($i=0;$i<2;$i++){
				$PersonalDevelopment .= ($i>0)?'<br/>':'';
				$PersonalDevelopment .= trim($PersonalDevelopmentArr[$i]);
			}
		}
		$PersonalDevelopment = ($PersonalDevelopment=='')? $this->EmptySymbol.'<br/>' : $PersonalDevelopment;		
	//	$OtherInformation = trim($OtherInfoDataAry[$StudentID][$SemID]['Other Information']);
		$OtherInformationArr = (array)$OtherInfoDataAry[$StudentID][$SemID]['Other Information'];
		$OtherInformation = '';
		if(!empty($OtherInformationArr[0])){
			for($i=0;$i<5;$i++){
				$OtherInformation .= ($i>0)?'<br/>':'';
				$OtherInformation .= trim($OtherInformationArr[$i]);
			}
		}
		$OtherInformation = ($OtherInformation=='')? $this->EmptySymbol.'<br/><br/><br/><br/><br/>' : $OtherInformation;
		$PromoteStatus = trim($OtherInfoDataAry[$StudentID][$SemID]['Promote to / Repeat']);
		$PromoteStatus = ($PromoteStatus=='')? $this->EmptySymbol : $PromoteStatus;
		
		$x = '';
		$x .= '<table class="font_8pt" style="width:100%;">';
			### Conduct
			$x .= '<tr>';
				$x .= '<td style="width:3%;">(1)</td>';
				$x .= '<td style="width:15%;">'.$eReportCard['Template']['ConductCh'].'</td>';
				$x .= '<td style="width:20%;">'.$eReportCard['Template']['ConductEn'].'</td>';
				$x .= '<td style="width:5%;">:</td>'; //20130704 Siuwan [2013-0704-0916-05066] 10%
				$x .= '<td style="width:57%;">'.$Conduct.'</td>'; //20130704 Siuwan [2013-0704-0916-05066] 52%
			$x .= '</tr>';
			$x .= $this->Get_Empty_Row(12);
			
			### Teacher's Comment
			$x .= '<tr>';
				$x .= '<td>(2)</td>';
				$x .= '<td>'.$eReportCard['Template']['TeachersCommentCh'].'</td>';
				$x .= '<td>'.$eReportCard['Template']['TeachersCommentEn'].'</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
			$x .= '</tr>';
			// Academic Performance
			$x .= '<tr style="vertical-align:top;">';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>(a) '.$eReportCard['Template']['AcademicPerformanceCh'].'</td>';
				$x .= '<td>&nbsp;&nbsp;&nbsp;&nbsp;'.$eReportCard['Template']['AcademicPerformanceEn'].'</td>';
				$x .= '<td>:</td>';
				$x .= '<td>'.$AcademicPerformance.'</td>';
			$x .= '</tr>';
			$x .= $this->Get_Empty_Row(12);
			// Personal Development
			$x .= '<tr style="vertical-align:top;">';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>(b) '.$eReportCard['Template']['PersonalDevelopmentCh'].'</td>';
				$x .= '<td>&nbsp;&nbsp;&nbsp;&nbsp;'.$eReportCard['Template']['PersonalDevelopmentEn'].'</td>';
				$x .= '<td>:</td>';
				$x .= '<td>'.$PersonalDevelopment.'</td>';
			$x .= '</tr>';
			$x .= $this->Get_Empty_Row(12);
			
			### Other Information
			$x .= '<tr style="vertical-align:top;">';
				$x .= '<td>(3)</td>';
				$x .= '<td>'.$eReportCard['Template']['OtherInformationCh'].'</td>';
				$x .= '<td>'.$eReportCard['Template']['OtherInformationEn'].'</td>';
				$x .= '<td>:</td>';
				$x .= '<td>'.$OtherInformation.'</td>';
			$x .= '</tr>';
			$x .= $this->Get_Empty_Row(12);
			
			if ($this->Get_Semester_Seq_Number($SemID) > 1) {
				### Promote to / Repeat => show in second term report only
				$x .= '<tr>';
					$x .= '<td colspan="2">'.$eReportCard['Template']['PromoteToCh'].'/'.$eReportCard['Template']['RepeatCh'].'</td>';
					$x .= '<td>'.$eReportCard['Template']['PromoteToEn'].'/'.$eReportCard['Template']['RepeatEn'].'</td>';
					$x .= '<td>:</td>';
					$x .= '<td>'.$PromoteStatus.'</td>';
				$x .= '</tr>';
			}
		$x .= '</table>';
		
		return $x;
	}
	
	########### END Template Related

	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr)
	{
		
	}
	
	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0)
	{
		
	}
	
	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0)
	{
		
	}
	
	function Is_Show_Rightest_Column($ReportID)
	{
		
	}
}
?>