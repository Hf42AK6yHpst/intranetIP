<?php
# Editing by 
/*
 * Modification Log:
 * 2011-06-29 Ivan [2011-0629-1033-04067]
 * - Effort changed back from drop down list to textbox
 */

####################################################
# Library for United Christian College (Kowloon East)
####################################################

//include_once($intranet_root."/lang/reportcard_custom/general.$intranet_session_language.php");
include_once($intranet_root."/lang/reportcard_custom/ucc_ke.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 0;
		$this->IsEnableMarksheetFeedback = 0;
		$this->IsEnableMarksheetExtraInfo = 1;
		$this->IsEnableManualAdjustmentPosition = 1;
		
		$this->EmptySymbol = "----";
		$this->MaxPosition = 10;
		
		$this->ConductAry = array("EX","VG","GD","FG","AV","NI","UN","PR");
		
		# Show the following otherinfo in grandmarksheet
		$this->OtherInfoInGrandMS = array("Conduct","Reading");
		
		# Show the following otherinfo in class honour report
		$this->OtherInfoInHonour = array("Conduct","Reading");
		
		# Effort Selection
//		$this->ExtraInfoOptionArr['Effort'][5] = 5;
//		$this->ExtraInfoOptionArr['Effort'][4] = 4;
//		$this->ExtraInfoOptionArr['Effort'][3] = 3;
//		$this->ExtraInfoOptionArr['Effort'][2] = 2;
//		$this->ExtraInfoOptionArr['Effort'][1] = 1;
		
		# Specified Passing Percentage (for reporting)
		$this->SpecifiedPassingPercentage = 50;
	}
	
	########## START Template Related ##############
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
			# Get school badge
			$SchoolLogo = GET_SCHOOL_BADGE();
			$TempLogo = ($SchoolLogo == "") ? "&nbsp;" : $SchoolLogo;
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
			
			# Get school name
			$SchoolName = GET_SCHOOL_NAME();

            $TitleTable = "";
			$TitleTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
			$TitleTable .= "<td nowrap='nowrap' class='school_name' align='left'>";
			if ($HeaderHeight == -1) {
				if(!empty($SchoolName)) {
					$TitleTable .= $SchoolName;
				} else {
					$TitleTable .= "&nbsp;";
				}
			} else {
				for ($i = 0; $i < $HeaderHeight; $i++) {
					$TitleTable .= "<br/>";
				}
			}
			$TitleTable .= "</td></tr>";
			
			if(!empty($ReportTitle)) {
				$TitleTable .= "<tr><td colspan='2' nowrap='nowrap' class='report_title' align='center'>".$ReportTitle."</td></tr>\n";
			}
			$TitleTable .= "</table><br>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			
			# Retrieve required variables
			$defaultVal = "XXX";
			$data['AcademicYear'] = getCurrentAcademicYear();

			# Retrieve Student Info
			if($StudentID)
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				
				$data['Name'] = $lu->UserName2Lang('en', 1);
				$data['ClassNo'] = $thisClassNumber;
				$data['StudentNo'] = $thisClassNumber;
				$data['Class'] = $thisClassName;
				if (is_array($SettingStudentInfo) && !in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo)) {
					$data['Class'] .= " (".$thisClassNumber.")";
				}
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] = $lu->Gender;
				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				$data['StudentAdmNo'] = $data['STRN'];
				
				$ClassTeacherAry = $lclass->returnClassTeacher($lu->ClassName);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : $this->EmptySymbol;
			}
			
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if(in_array($SettingID, $SettingStudentInfo) === true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
						if($count%$StudentInfoTableCol == 0) {
							$StudentInfoTable .= "<tr>";
						}
						
						if ($SettingID=="ClassNo")
						{
							$align = "align='right'";
						}
						elseif ($SettingID=="Class")
						{
							$align = "align='center'";
						}
						else
						{
							$align = "";
						}
						
						$colspan = $SettingID == "Name" ? " colspan='10' " : "";
						$StudentInfoTable .= "<td class='student_info_text' $colspan width='20%' valign='top' height='{$LineHeight}' $align>".$Title." : ";
						$StudentInfoTable .= ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
							
						if(($count+1) % $StudentInfoTableCol == 0) {
							$StudentInfoTable .= "</tr>";
						} else {
							if($SettingID == "Name")
							{
								$count = -1;
								$StudentInfoTable .= "</tr>";
							}
						}
						$count++;
					}
				}
				$StudentInfoTable .= "</table>";
			}
		}
		
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
        $FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode = 1);
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'] && $this->IsEnableSubjectTeacherComment;
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectComponent = $ReportSetting['ShowSubjectComponent'];
		
		# Define table header
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2) = $ColHeaderAry;
		
		# Retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0) {
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		}
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0) {
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		}
		
		# Retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID);
		
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# Retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		# Retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
        $cellpadding = ($FormNumber == 6 || $ReportType == "W") ? 5 : 0;
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='$cellpadding' class='report_border2'>";

		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid => $da) {
					if($da['Grade'] != "*")	$Droped = 0;
				}
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray) != true)	$isSub = 1;
			
			if ($ShowSubjectComponent == false && $isSub == 1) {
				continue;
			}
			
			# Check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID] == false)
			{
				$DetailsTable .= "<tr>";

				# Subject 
				$DetailsTable .= $SubjectCol[$thisSubjectID];

				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];

				# Subject Teacher Comment
				if ($AllowSubjectTeacherComment) {
					$css_border_top = $isSub?"":"border_top";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
					}
					$DetailsTable .= "</span></td>";
				}
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}
		
		if($ReportType == "W")
		{
			# MS Table Footer
			$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2);
		}
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode = 1);
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'] && $this->IsEnableSubjectTeacherComment;
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark        = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight                 = $ReportSetting['LineHeight'];
		$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		#########################################################
		############## Marks START
		$row2 = "";
		if($FormNumber == 6)
        {
            if($ReportType=="T")	# Terms Report Type
            {
                # Retrieve Invloved Assesment
                $ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
                $ColumnID = array();
                $ColumnTitle = array();
                if (sizeof($ColoumnTitle) > 0)
                    foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
                $e = 0;
                for($i=0;$i<sizeof($ColumnTitle);$i++)
                {
                    $ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$i], 1, 2));
                    $row1 .= "<td valign='middle' height='{$LineHeight}' class='report_text border_left border_bottom' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'>". $ColumnTitleDisplay . "</td>";
                    $n++;
                    $e++;
                }
            }
            else					# Whole Year Report Type
            {
                $ColumnData = $this->returnReportTemplateColumnData($ReportID);
                $needRowspan=0;
                for($i=0;$i<sizeof($ColumnData);$i++)
                {
                    $SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
                    $isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
                    if($isDetails==1)
                    {
                        $thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
                        $thisReportID = $thisReport['ReportID'];
                        $ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
                        $ColumnID = array();
                        $ColumnTitle = array();
                        foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
                        for($j=0;$j<sizeof($ColumnTitle);$j++)
                        {
                            $ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$j], 1, 2));
                            $row2 .= "<td class='border_left report_text border_bottom' align='center' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'>". $ColumnTitleDisplay . "</td>";
                            $n++;
                            $e++;
                        }
                        $colspan = "colspan='". sizeof($ColumnTitle) ."'";
                        $Rowspan = "";
                        $needRowspan++;
                    }
                    else
                    {
                        $colspan = "";
                        $Rowspan = "rowspan='2'";
                        $e++;
                    }
                    $css_border_left = $i ? "border_left2" : "";
                    $row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='{$css_border_left} border_left report_text border_bottom' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'>". $SemName ."</td>";
                }
            }
        }
        else
        {
            $ShowRightestColumn = false;
            $needRowspan = 0;

            if($ReportType=="T")	# Terms Report Type
            {
                $row1 .= "<td valign='middle' align='center' height='{$LineHeight}' class='report_text border_left border_right border_bottom' width='".$eRCTemplateSetting['ColumnWidth']['Mark']."'><b>".$eReportCard['Template']['ProgressPerformance']."</b></td>";
                $n++;
                $e++;

                $rangeRow = "";
                $rangeRow .= "<table class=\"range_row\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin-top: 4px;\"><tr>";
                $rangeRow .= "<td align='left' class='report_text' width='25%'>0</td>";
                $rangeRow .= "<td align='left' class='report_text' width='25%'>25</td>";
                $rangeRow .= "<td align='left' class='report_text' width='25%'>50</td>";
                $rangeRow .= "<td align='left' class='report_text' width='12.5%'>75</td>";
                $rangeRow .= "<td align='right' class='report_text' width='12.5%'>100</td>";
                $rangeRow .= "</tr></table>";

                $row1 .= "<td valign='middle' align='center' height='{$LineHeight}' class='report_text border_left border_bottom' width='".$eRCTemplateSetting['ColumnWidth']['ProgressChart']."' style='padding: 0px 3px 0px 3px;'><b>".$eReportCard['Template']['ProgressChart']."</b><br/>".$rangeRow."</td>";
                $n++;
                $e++;
            }
            else					# Whole Year Report Type
            {
                $ColumnData = $this->returnReportTemplateColumnData($ReportID);
                $ColumnData = $ColumnData[sizeof($ColumnData) - 1];

                $SemName = $this->returnSemesters($ColumnData['SemesterNum']);
                $thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
                $thisReportID = $thisReport['ReportID'];

                $ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
                $ColumnID = array();
                $ColumnTitle = array();
                foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
                for($j=0;$j<sizeof($ColumnTitle);$j++)
                {
                    $ColumnTitleDisplay = (convert2unicode($ColumnTitle[$j], 1, 2));
                    $row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' align='center' class='{$css_border_left} border_left report_text border_bottom' width='".$eRCTemplateSetting['ColumnWidth']['Mark']."'>".$ColumnTitleDisplay."</td>";
                    $n++;
                    $e++;
                }

                $row1 .= "<td {$Rowspan} valign='middle' align='center' class='report_text border_left2 border_bottom' width='".$eRCTemplateSetting['ColumnWidth']['Mark']."'>".$eReportCard['Template']['SubjectOverall']."</td>";
                $n++;

                //$colspan = "colspan='". sizeof($ColumnTitle) ."'";
                //$Rowspan = "";
                //$needRowspan++;
            }
        }
		############## Marks END
		#########################################################
		$Rowspan = $row2 ? "rowspan='2'" : "";

		if(!$needRowspan)
			$row1 = str_replace("rowspan='2'", "", $row1);
		
		$x = "<tr>";

		# Subject 
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		for($i=0;$i<sizeof($SubjectColAry);$i++)
		{
			$SubjectEng = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'];
			$SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];

			$SubjectTitle = $SubjectColAry[$i];
			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);

			$text_align = '';
            if($ReportType == "T" && $FormNumber != 6) {
                $SubjectTitle = '<b>'.$SubjectTitle.'</b>';
                $text_align = " align='center' ";
            }

			$x .= "<td {$Rowspan} valign='middle' ".$text_align." class='report_text border_bottom border_right' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject']."'>".$SubjectTitle."</td>";
			$n++;
		}
		
		# Marks
		$x .= $row1;
		
		# Subject Overall 
		if($ShowRightestColumn)
		{
			$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='report_text border_left2 border_bottom'  align='center'>". ($ReportType=="T" ? $eReportCard['Template']['SubjectOverall'] : $eReportCard['Template']['Annual']) ."</td>";
			$n++;
		}
		else
		{
			$e--;	
		}
		
		# UCCKE
		if($FormNumber == 6 && $ReportType == "T" || $FormNumber != 6 && $ReportType == "W")
		{
			# Position
			$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='report_text border_left2 border_bottom'  align='center'>". $eReportCard['Template']['Position'] ."</td>";
			$n++;

			# Effort
			$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='report_text border_left2 border_bottom'  align='center'>". $eReportCard['Template']['Effort'] ."</td>";
			$n++;
		}

		# Subject Teacher Comment
		if ($AllowSubjectTeacherComment) {
			$x .= "<td {$Rowspan} valign='middle' class='border_left' align='center' width='10%'>".$eReportCard['TeachersComment']."</td>";
		}
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		return array($x, $n, $e);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";

 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID => $Ary)
	 		{
		 		foreach($Ary as $SubSubjectID => $Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;";
			 		if($SubSubjectID == 0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		//if($ReportType=="W" && $Prefix != "")
				 	//	continue;
			 		
			 		foreach($SubjectDisplay as $k => $v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
			 			$t .= "<td class='border_top border_right' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td height='{$LineHeight}' class='report_text' valign='middle'>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}' class='report_text' valign='middle'>$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		} 
					$x[$SubSubjectID] = $t;
					$isFirst = 0;
				}
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting;

 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
			$SemID 	= $ReportSetting['Semester'];
	 		$ReportType = $SemID == "F" ? "W" : "T";
            $ClassLevelID = $ReportSetting['ClassLevelID'];
            $FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode = 1);
		}

		//if($ReportType=="T")
		//{
		//	$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		//	$line = "_____________";
		//}
		//else
		//{
		//	$SignatureTitleArray = $eRCTemplateSetting['Signature2'];
		//	$line = "______________________";
		//}

			$SignatureTable = "";
			$SignatureTable = "<br><table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
			if($ReportType == "T") {
				$SignatureTable .= "<tr><td valign='bottom' class='report_text' align='left'>Signature:</td></tr>";
			}
			$SignatureTable .= "<tr>";

			//B56890
//			$numOfSignature = count($SignatureTitleArray);
//			for($k=0; $k<$numOfSignature; $k++)
//			{
//				$SettingID = trim($SignatureTitleArray[$k]);
//				$Title = $eReportCard['Template'][$SettingID];
//				$IssueDate = ($SettingID == "IssueDate" && $Issued)	? "<u>".$Issued."</u>" : $line;
//				$SignatureTable .= "<td valign='bottom' align='center'>";
//				$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
//				$SignatureTable .= "<tr><td align='center' height='80' valign='bottom' class='report_text'>_______". $IssueDate ."_______</td></tr>";
//				$SignatureTable .= "<tr><td align='center' valign='bottom' class='report_text'>".$Title."</td></tr>";
//				$SignatureTable .= "</table>";
//				$SignatureTable .= "</td>";
//			}

        if($FormNumber == 6)
        {
			if ($ReportType=="T")
			{
				// Principal
				$SignatureTable .= "<td valign='bottom' align='center' style='width:25%;'>";
					$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0' style='width:100%;'>";
						$SignatureTable .= "<tr><td align='center' height='80' valign='bottom' class='report_text' style='border-bottom: 1px solid #000000;'>&nbsp;</td></tr>";
						$SignatureTable .= "<tr><td align='center' valign='bottom' class='report_text'>Principal</td></tr>";
					$SignatureTable .= "</table>";
				$SignatureTable .= "</td>";

				// Form Teacher
				$SignatureTable .= "<td valign='bottom' align='center' style='width:25%; padding-left:10px;'>";
					$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0' style='width:100%;'>";
						$SignatureTable .= "<tr><td align='center' height='80' valign='bottom' class='report_text' style='border-bottom: 1px solid #000000;'>&nbsp;</td></tr>";
						$SignatureTable .= "<tr><td align='center' valign='bottom' class='report_text'>Form Teacher</td></tr>";
					$SignatureTable .= "</table>";
				$SignatureTable .= "</td>";

				// Parent / Guardian
				$SignatureTable .= "<td valign='bottom' align='center' style='width:25%; padding-left:10px;'>";
					$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0' style='width:100%;'>";
						$SignatureTable .= "<tr><td align='center' height='80' valign='bottom' class='report_text' style='border-bottom: 1px solid #000000;'>&nbsp;</td></tr>";
						$SignatureTable .= "<tr><td align='center' valign='bottom' class='report_text'>Parent / Guardian</td></tr>";
					$SignatureTable .= "</table>";
				$SignatureTable .= "</td>";

				// School Chop
				$SignatureTable .= "<td valign='bottom' align='center' style='width:25%; padding-left:10px;'>";
					$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0' style='width:100%;'>";
						$SignatureTable .= "<tr><td align='center' height='80' valign='bottom' class='report_text' style='border-bottom: 1px solid #000000;'>&nbsp;</td></tr>";
						$SignatureTable .= "<tr><td align='center' valign='bottom' class='report_text'>School Chop</td></tr>";
					$SignatureTable .= "</table>";
				$SignatureTable .= "</td>";
			}
			else
            {
				$SignatureTable .= "<td valign='bottom' align='center' style='width:9%;'>&nbsp;</td>";

				// Princial
				$SignatureTable .= "<td valign='bottom' align='center' style='width:32%;'>";
					$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0' style='width:100%;'>";
						$SignatureTable .= "<tr><td align='center' height='80' valign='bottom' class='report_text' style='border-bottom: 1px solid #000000;'>&nbsp;</td></tr>";
						$SignatureTable .= "<tr><td align='center' valign='bottom' class='report_text'>Principal</td></tr>";
					$SignatureTable .= "</table>";
				$SignatureTable .= "</td>";

				$SignatureTable .= "<td valign='bottom' align='center' style='width:18%;'>&nbsp;</td>";

				// School Chop
				$SignatureTable .= "<td valign='bottom' align='center' style='width:32%;'>";
					$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0' style='width:100%;'>";
						$SignatureTable .= "<tr><td align='center' height='80' valign='bottom' class='report_text' style='border-bottom: 1px solid #000000;'>&nbsp;</td></tr>";
						$SignatureTable .= "<tr><td align='center' valign='bottom' class='report_text'>School Chop</td></tr>";
					$SignatureTable .= "</table>";
				$SignatureTable .= "</td>";

				$SignatureTable .= "<td valign='bottom' align='center' style='width:9%;'>&nbsp;</td>";
			}
        }
        else
        {
            // F1 - F5 Term Report - no signature display
            if ($ReportType == "T") {
                return '';
            }
            
            // Principal's Signature
            $SignatureTable .= "<td valign='bottom' align='center' style='width:25%;'>";
                $SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0' style='width:100%;'>";
                    $SignatureTable .= "<tr><td align='center' height='80' valign='bottom' class='report_text' style='border-bottom: 1px solid #000000;'>&nbsp;</td></tr>";
                    $SignatureTable .= "<tr><td align='center' valign='bottom' class='report_text'>Principal's Signature</td></tr>";
                $SignatureTable .= "</table>";
            $SignatureTable .= "</td>";

            // Form Teacher's Signature
            $SignatureTable .= "<td valign='bottom' align='center' style='width:25%; padding-left:10px;'>";
                $SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0' style='width:100%;'>";
                    $SignatureTable .= "<tr><td align='center' height='80' valign='bottom' class='report_text' style='border-bottom: 1px solid #000000;'>&nbsp;</td></tr>";
                    $SignatureTable .= "<tr><td align='center' valign='bottom' class='report_text'>Form Teacher's Signature</td></tr>";
                $SignatureTable .= "</table>";
            $SignatureTable .= "</td>";

            // Parent's Signature
            $SignatureTable .= "<td valign='bottom' align='center' style='width:25%; padding-left:10px;'>";
                $SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0' style='width:100%;'>";
                    $SignatureTable .= "<tr><td align='center' height='80' valign='bottom' class='report_text' style='border-bottom: 1px solid #000000;'>&nbsp;</td></tr>";
                    $SignatureTable .= "<tr><td align='center' valign='bottom' class='report_text'>Parent's Signature</td></tr>";
                $SignatureTable .= "</table>";
            $SignatureTable .= "</td>";

            // School Chop
            $SignatureTable .= "<td valign='bottom' align='center' style='width:25%; padding-left:10px;'>";
                $SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0' style='width:100%;'>";
                    $SignatureTable .= "<tr><td align='center' height='80' valign='bottom' class='report_text' style='border-bottom: 1px solid #000000;'>&nbsp;</td></tr>";
                    $SignatureTable .= "<tr><td align='center' valign='bottom' class='report_text'>School Chop</td></tr>";
                $SignatureTable .= "</table>";
            $SignatureTable .= "</td>";
        }
    
		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		if($ReportType == "W")
		{
			if($Issued)
			{
				$issue_date = date("F, Y", strtotime($Issued));
                
                // [2020-0629-1407-02164] remove issue date in Yearly Report
				$SignatureTable .= "<br><table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
				//$SignatureTable .= "<tr><td valign='bottom' class='report_text' align='left'>{$issue_date}</td></tr>";
                $SignatureTable .= "<tr><td valign='bottom' class='report_text' align='left'>&nbsp;</td></tr>";
				$SignatureTable .= "</table>";
			}
		}
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1)
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		# Retrieve Basic Information of Report
		$BasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $BasicInfo['ClassLevelID'];
        $FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode = 1);
		$LineHeight = $BasicInfo['LineHeight'];
		$SemID = $BasicInfo['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		//$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		# Retrieve Student Class
        if($StudentID)
        {
            include_once($PATH_WRT_ROOT."includes/libuser.php");
            $lu = new libuser($StudentID);
            //$ClassName 	= $lu->ClassName;
            $WebSamsRegNo 	= $lu->WebSamsRegNo;

            $StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
            $ClassID 		= $StudentInfoArr[0]['ClassID'];
            $ClassName 		= $StudentInfoArr[0]['ClassName'];
        }

		if($FormNumber == 6 && $ReportType == "W")
		{
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			$SemesterAry = array();
			foreach($ColumnData as $key=>$data) {
                $SemesterAry[] = $data[SemesterNum];
            }

			$csvType = $this->getOtherInfoType();
			//$temp_i = 0;
			$ary = array();
			foreach($SemesterAry as $key=>$semid)
			{
				//$thisSemID = $semid+1;
				$thisSemID = $semid;

				# build csv data array
				foreach($csvType as $k=>$Type)
				{
					$csvData = $this->getOtherInfoData($Type, $thisSemID, $ClassID);
					if(!empty($csvData))
					{
						foreach($csvData as $RegNo=>$data)
						{
							//if($RegNo == $WebSamsRegNo)
							if($RegNo == $StudentID)
							{
			 					foreach($data as $key=>$val)
			 					{
				 					if(is_int($key)) continue;
				 					$ary[$key][$semid] = $val;
			 					}
							}
						}
					}
				}

				# build GPA data aray
				$table = $this->DBName.".RC_REPORT_TEMPLATE";
				$sql = "select ReportID from {$table} where ClassLevelID='$ClassLevelID' and Semester='$semid'";
				$row = $this->returnVector($sql);
				$thisReportID = $row[0];

                $StudentResultAry = array();
                if($StudentID) {
                    $StudentResultAry = $this->getReportResultScore($thisReportID, 0, $StudentID);
                }
				//2013-0704-1646-14140
				//$GPA = $StudentResultAry['GPA'] ? $StudentResultAry['GPA'] : "&nbsp;";
				$GPA = $StudentResultAry['GPA'] ? $StudentResultAry['GPA'] : $this->EmptySymbol;
				$GPA = ($GPA == -1 || $GPA === null)? $this->EmptySymbol : $GPA;
				if (is_numeric($GPA)) {
					$GPA = my_round($GPA, 2);
				}

				$ary['GPA'][$semid] = $GPA;

				if (is_numeric($GPA))
				{
					$GPA_Total_Temp += $GPA;
					$GPA_Total_count++;
				}

				//$temp_i++;
			}

			$i=0;
			foreach($ary as $dname=>$dval)
			{
				if ($FormNumber >= 4 && $dname=='Reading') {
					$i++;
					continue;
				}
				
				$t = "<table border='0' cellpadding='0' cellspacing='0'>";
				$t .= "<tr><td height='{$LineHeight}' class='report_text' valign='middle'>&nbsp;&nbsp;</td><td height='{$LineHeight}' class='report_text' valign='middle'>$dname</td>";
				$t .= "</tr></table>";
				
				$x .= "<tr>";
				$x .= "<td class='border_top border_right' height='{$LineHeight}' valign='middle'>". $t ."</td>";
				
				$tempi=0;
				foreach($SemesterAry as $key=>$semid)
				{
					//$border_left = ($tempi%2) ? "border_left2" : "border_left";
					$border_left = ($tempi==0)? 'border_left' : 'border_left2';
					
					//2013-0704-1646-14140
					//$data = $ary[$dname][$semid] ? $ary[$dname][$semid] : "&nbsp;";
					$data = $ary[$dname][$semid] ? $ary[$dname][$semid] : $this->EmptySymbol;
					
					$x .= "<td class='report_text bold_text border_top {$border_left}' height='{$LineHeight}' align='center'>". $data."</td>";
					$tempi++;
				}
				
				# Annual Column
				// if($i<sizeof($ary)-1)		# (----)
                if($dname != 'GPA')
				{
					$x .= "<td class='report_text border_top border_left2' height='{$LineHeight}' valign='middle' align='center'>----</td>";
				}
				else
				{
					//$GPA_Total = my_round($GPA_Total_Temp / sizeof($SemesterAry), 2);
					//$GPA_Total = my_round($GPA_Total_Temp / $GPA_Total_count, 2);

                    $StudentResultAry = array();
                    if($StudentID) {
                        $StudentResultAry = $this->getReportResultScore($ReportID, 0, $StudentID);
                    }
					$GPA = $StudentResultAry['GPA'] ? $StudentResultAry['GPA'] : "&nbsp;";
					$GPA = ($GPA == -1)? $this->EmptySymbol : $GPA;
					$GPA = (is_numeric($GPA))? my_round($GPA, 2) : $GPA;
					
					$x .= "<td class='report_text border_top border_left2' height='{$LineHeight}' valign='middle' align='center'>". $GPA ."</td>";
				}
			
				$x .= "</tr>";
				$i++;
			}
			
			return $x;
		}
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='')
	{
		global $eReportCard;
				
		# Retrieve Display Settings
		$ReportSetting  = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 		    = $ReportSetting['Semester'];
        $SemSeqNumber   = $this->Get_Semester_Seq_Number($SemID);
 		$ReportType 	= $SemID == "F" ? "W" : "T";
 		$ClassLevelID 	= $ReportSetting['ClassLevelID'];
        $FormNumber     = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode = 1);
		$ShowSubjectOverall         = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark        = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0) {
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		}
		
		$n = 0;
		$x = array();
		$isFirst = 1;
						
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);

		if($FormNumber == 6)
        {
            if($ReportType == "T")	# Temrs Report Type
            {
                $CalculationOrder = $CalSetting["OrderTerm"];

                $ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
                $ColumnID = array();
                $ColumnTitle = array();
                if(sizeof($ColoumnTitle) > 0) {
                    foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
                }

                // [2020-0219-1057-57066]
                $SpecialHandlingForAbsDisplay = $this->schoolYearID == 18;
                if($SpecialHandlingForAbsDisplay)
                {
                    $SubjectCodeMapping = $this->GET_SUBJECTS_CODEID_MAP(1, 0);
                    $SpecialHandlingAbsSubjectArr = array();
                    $SpecialHandlingAbsSubjectArr = array_merge($SpecialHandlingAbsSubjectArr, array('080A_', '080A_140A', '080A_141A', '090_')); // Chinese Lanuage & Literature
                    $SpecialHandlingAbsSubjectArr = array_merge($SpecialHandlingAbsSubjectArr, array('165_', '165_165-P4'));                      // English Language
                    $SpecialHandlingAbsSubjectArr = array_merge($SpecialHandlingAbsSubjectArr, array('210_', '315_', '045_', '81N_'));            // Geo & Phy & Bio & ICT
                    $SpecialHandlingAbsSubjectArr = array_merge($SpecialHandlingAbsSubjectArr, array('135_', '12N_', '13N_'));                    // Econ & BAFS
                    $SpecialHandlingAbsSubjectArr = array_merge($SpecialHandlingAbsSubjectArr, array('71S_', '83S_', '41S_'));                    // Practical
                }

                foreach($SubjectArray as $SubjectID => $SubjectName)
                {
                    $isSub = 0;
                    if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;

                    // check if it is a parent subject, if yes find info of its components subjects
                    $CmpSubjectArr = array();
                    $isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
                    if (!$isSub) {
                        $CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
                        if(!empty($CmpSubjectArr)) $isParentSubject = 1;
                    }

                    // [2020-0219-1057-57066]
                    if($SpecialHandlingForAbsDisplay)
                    {
                        $thisSubjectCode = $SubjectCodeMapping[$SubjectID];
                        $thisSubjectCode = $thisSubjectCode["CODEID"].'_'.$thisSubjectCode["CMP_CODEID"];
                        $isSpecialHandlingAbsSubject = in_array($thisSubjectCode, (array)$SpecialHandlingAbsSubjectArr);
                    }

                    # define css
                    $css_border_top = "border_top";

                    # Retrieve Subject Scheme ID & settings
                    $SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
                    $ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];

                    $isAllNA = true;

                    # Assessment Marks & Display
                    for($i=0; $i<sizeof($ColumnID); $i++)
                    {
                        if ($isParentSubject && $CalculationOrder == 1)
                        {
                            $thisMarkDisplay = $this->EmptySymbol;
                        }
                        else
                        {
                            $thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID=".$ColumnID[$i] ." and SubjectID=$SubjectID");
                            $thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];

                            $thisMark = $ScaleDisplay == "M" ? $MarksAry[$SubjectID][$ColumnID[$i]]['Mark'] : "";
                            $thisGrade = $MarksAry[$SubjectID][$ColumnID[$i]]['Grade'];

                            # for preview purpose
                            if(!$StudentID)
                            {
                                $thisMark = $ScaleDisplay == "M" ? "S" : "";
                                $thisGrade = $ScaleDisplay == "G" ? "G" : "";
                            }

                            $thisMark = ($ScaleDisplay == "M" && strlen($thisMark) && $thisGrade == '') ? $thisMark : $thisGrade;
                            if ($thisMark != "N.A.")
                            {
                                $isAllNA = false;
                            }

                            if ($i == 0 && ($thisMark == "" || $thisMark == "N.A." || $thisMark == "-" || strtolower($thisMark) == "abs" || $thisMark == "/"))
                            {
                                // show abs as '---' for Daily column
                                $thisMarkDisplay = $this->EmptySymbol;
                            }
                            else if ($i > 0 && ($thisMark == "" || $thisMark == "N.A." || $thisMark == "/"))
                            {
                                // show abs as 'ABS' for others column
                                $thisMarkDisplay = $this->EmptySymbol;
                            }
                            else if ($i > 0 && $isSpecialHandlingAbsSubject && ($thisMark == "-" || strtolower($thisMark) == "abs"))
                            {
                                // [2020-0219-1057-57066]
                                // show abs as '---' for Exam column (Special handling for 2019-2020 only)
                                $thisMarkDisplay = $this->EmptySymbol;
                            }
                            else
                            {
                                # check special case
                                list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
                                if($needStyle)
                                {
                                    $thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
                                    $thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp,'','',$ReportID);
                                    $thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
                                }
                                else
                                {
                                    $thisMarkDisplay = $thisMark;
                                }

                                if ($thisMarkDisplay == "N.A. : Not Assessed")
                                {
                                    $thisMarkDisplay = $this->EmptySymbol;
                                }
                            }
                        }

                        $x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
                        $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                        $x[$SubjectID] .= "<td align='center' class='report_text' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";

                        if($ShowSubjectFullMark)
                        {
                            # check special full mark
                            $SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
                            $SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
                            $thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];

                            $FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
                            $x[$SubjectID] .= "<td align='center' class='report_text' width='50%'>(". $FullMark .")</td>";
                        }

                        $x[$SubjectID] .= "</tr></table>";
                        $x[$SubjectID] .= "</td>";
                    }

                    # Subject Overall (disable when calculation method is Vertical-Horizontal)
                    if($ShowSubjectOverall)
                    {
                        $thisMark = $ScaleDisplay == "M" ? $MarksAry[$SubjectID][0]['Mark'] : "";
                        //$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0][Grade] : "";
                        $thisGrade = $MarksAry[$SubjectID][0]['Grade'];

                        # for preview purpose
                        if(!$StudentID)
                        {
                            $thisMark = $ScaleDisplay == "M" ? "S" : "";
                            $thisGrade	= $ScaleDisplay == "G" ? "G" : "";
                        }

                        # $thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
                        $thisMark = ($ScaleDisplay == "M" && strlen($thisMark) && $thisGrade == '') ? $thisMark : $thisGrade;
                        if ($thisMark != "N.A.")
                        {
                            $isAllNA = false;
                        }

                        //if ($thisMark == "" || $thisMark == "N.A.")
                        if ($thisMark == "" || $thisMark == "N.A." || $thisMark == "-" || strtolower($thisMark) == "abs" || $thisMark == "/")
                        {
                            $thisMarkDisplay = $this->EmptySymbol;
                        }
                        else
                        {
                            # check special case
                            list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
                            if($needStyle)
                            {
                                //if($thisSubjectWeight > 0)
                                //	$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
                                //else
                                    $thisMarkTemp = $thisMark;
                                $thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp,'','',$ReportID);
                                $thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
                            }
                            else
                                $thisMarkDisplay = $thisMark;

                            if ($thisMarkDisplay == "N.A. : Not Assessed")
                            {
                                $thisMarkDisplay = $this->EmptySymbol;
                            }
                        }

                        $x[$SubjectID] .= "<td class='border_left2 {$css_border_top}'>";
                        $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                        $x[$SubjectID] .= "<td align='center' class='report_text' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
                        if($ShowSubjectFullMark)
                        {
                            $SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
                            $SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
                            $thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
                            $x[$SubjectID] .= "<td align='center' class='report_text' width='50%'>(". $thisFullMark .")</td>";
                        }
                        $x[$SubjectID] .= "</tr></table>";
                        $x[$SubjectID] .= "</td>";
                    }
                    else if ($ShowRightestColumn)
                    {
                        $x[$SubjectID] .= "<td align='center' class='report_text border_left {$css_border_top}'>--</td>";
                    }

                    # UCCKE
                    # Position - get from manual adjustment first. If no adjustment found, get from the generated position
                    # Hide position for subject components
                    if (!$isSub && $thisNature != 'Fail')
                    {
                        //$ClassInfo = $this->Get_Student_Class_ClassLevel_Info($StudentID);
                        //$ClassID = $ClassInfo[0]['ClassID'];
                        $FormStudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
                        $numOfStudent = count($FormStudentInfoArr);
                        $FormStudentIDArr = array();
                        for ($j=0; $j<$numOfStudent; $j++)
                        {
                            $FormStudentIDArr[] = $FormStudentInfoArr[$j]['UserID'];
                        }

                        $FormManualPositionArr = $this->GET_MANUAL_ADJUSTED_POSITION($FormStudentIDArr, $SubjectID, $ReportID);
                        //$manualPositionArr =  $this->GET_MANUAL_ADJUSTED_POSITION(array($StudentID), $SubjectID, $ReportID);

                        ### Requested by Lau Sir (uccke #157)
                        ### If all manual positions are empty => display system generated position
                        ### Else display manual adjusted position
                        $isAllEmpty = true;
                        $numOfAdjustedMark = count($FormManualPositionArr);
                        if ($numOfAdjustedMark > 0)
                        {
                            foreach ($FormManualPositionArr as $thisStudentID => $MarkInfoArr)
                            {
                                if (trim($MarkInfoArr["Info"]) != '')
                                {
                                    $isAllEmpty = false;
                                    break;
                                }
                            }
                        }

                        $manualPosition = $FormManualPositionArr[$StudentID]['Info'];
                        if ($isAllEmpty)
                        {
                            if ($manualPosition == NULL || $manualPosition == 0 || $manualPosition == "")
                            {
                                $position = $MarksAry[$SubjectID][0]['OrderMeritSubjectGroup'] ? $MarksAry[$SubjectID][0]['OrderMeritSubjectGroup'] : $this->EmptySymbol;
                            }
                            else
                            {
                                $position = $manualPosition;
                            }
                        }
                        else
                        {
                            if ($manualPosition == '')
                                $position = $this->EmptySymbol;
                            else
                                $position = $manualPosition;
                        }

                        if ($position > $this->MaxPosition || $position==-1)
                            $position = $this->EmptySymbol;

                        $x[$SubjectID] .= "<td class='border_left2 {$css_border_top}'>";
                        $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                            $x[$SubjectID] .= "<td align='center' class='report_text' width='100%'>". $position ."</td>";

                        $x[$SubjectID] .= "</tr></table>";
                        $x[$SubjectID] .= "</td>";
                    }
                    else
                    {
                        $x[$SubjectID] .= "<td class='border_left2 {$css_border_top} report_text' align='center'>";
                        $x[$SubjectID] .= $this->EmptySymbol;
                        $x[$SubjectID] .= "</td>";
                    }

                    # UCCKE
                    # Effot
                    $temp = $this->GET_EXTRA_SUBJECT_INFO(array($StudentID), $SubjectID, $ReportID);
                    $effort = $temp[$StudentID][Info];
                    $effort = $effort ? $effort : $this->EmptySymbol;
                    $x[$SubjectID] .= "<td class='border_left2 {$css_border_top}'>";
                    $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                        $x[$SubjectID] .= "<td align='center' class='report_text' width='100%'>". $effort ."</td>";

                    $x[$SubjectID] .= "</tr></table>";
                    $x[$SubjectID] .= "</td>";

                    $isFirst = 0;

                    # construct an array to return
                    $returnArr['HTML'][$SubjectID] = $x[$SubjectID];
                    $returnArr['isAllNA'][$SubjectID] = ($StudentID=='')? false : $isAllNA;	// show all subjects for preview
                }
            } # End if($ReportType=="T")
            else					# Whole Year Report Type
            {
                $CalculationOrder = $CalSetting["OrderFullYear"];

                # Retrieve Invloved Temrs
                $ColumnData = $this->returnReportTemplateColumnData($ReportID);

                foreach($SubjectArray as $SubjectID => $SubjectName)
                {
                    # Retrieve Subject Scheme ID & settings
                    $SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
                    $ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];

                    $t = "";
                    $isSub = 0;
                    if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;

                    // check if it is a parent subject, if yes find info of its components subjects
                    $CmpSubjectArr = array();
                    $isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
                    if (!$isSub) {
                        $CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
                        if(!empty($CmpSubjectArr)) $isParentSubject = 1;
                    }

                    $isAllNA = true;

                    # Terms's Assesment / Terms Result
                    for($i=0; $i<sizeof($ColumnData); $i++)
                    {
                        $css_border_top = "border_top";

                        $isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
                        if($isDetails == 1)		# Retrieve assesments' marks
                        {
                            # See if any term reports available
                            $thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);

                            # if no term reports, CANNOT retrieve assessment result at all
                            if (empty($thisReport))
                            {
                                for($j=0; $j<sizeof($ColumnID); $j++) {
                                    $x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>--</td>";
                                }
                            }
                            else
                            {
                                $thisReportID = $thisReport['ReportID'];
                                $ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
                                $ColumnID = array();
                                $ColumnTitle = array();
                                foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);

                                $thisMarksAry = $this->getMarks($thisReportID, $StudentID);

                                for($j=0; $j<sizeof($ColumnID); $j++)
                                {
                                    if ($isParentSubject && $CalculationOrder == 1)
                                    {
                                        $thisMarkDisplay = $this->EmptySymbol;
                                    }
                                    else
                                    {
                                        $thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID=".$ColumnID[$j] ." and SubjectID=$SubjectID");
                                        $thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];

                                        $thisMark = $ScaleDisplay == "M" ? $thisMarksAry[$SubjectID][$ColumnID[$j]]['Mark'] : "";
                                        $thisGrade = $ScaleDisplay == "G" ? $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade'] : "";

                                        # for preview purpose
                                        if(!$StudentID)
                                        {
                                            $thisMark = $ScaleDisplay == "M" ? "S" : "";
                                            $thisGrade	= $ScaleDisplay == "G" ? "G" : "";
                                        }
                                        $thisMark = ($ScaleDisplay == "M" && strlen($thisMark)) ? $thisMark : $thisGrade;
                                        if ($thisMark != "N.A.")
                                        {
                                            $isAllNA = false;
                                        }

                                        //if ($thisMark == "" || $thisMark == "N.A.")
                                        if ($thisMark == "" || $thisMark == "N.A." || $thisMark == "-" || strtolower($thisMark) == "abs" || $thisMark == "/")
                                        {
                                            $thisMarkDisplay = $this->EmptySymbol;
                                        }
                                        else
                                        {
                                            # check special case
                                            list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
                                            if($needStyle)
                                            {
                                                $thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
                                                $thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp,'','',$thisReportID);
                                                $thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
                                            }
                                            else
                                            {
                                                $thisMarkDisplay = $thisMark;
                                            }

                                            if ($thisMarkDisplay == "N.A. : Not Assessed")
                                            {
                                                $thisMarkDisplay = $this->EmptySymbol;
                                            }
                                        }
                                    }

                                    $x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
                                    $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                                    $x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";

                                    if($ShowSubjectFullMark)
                                    {
                                        $FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
                                        $x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
                                    }

                                    $x[$SubjectID] .= "</tr></table>";
                                    $x[$SubjectID] .= "</td>";
                                }
                            }
                        }
                        else					# Retrieve Terms Overall marks
                        {
                                # See if any term reports available
                                $thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");

                                # if no term reports, the term mark should be entered directly
                                if (empty($thisReport))
                                {
                                    $thisReportID = $ReportID;
                                    $MarksAry = $this->getMarks($thisReportID, $StudentID);
                                    $thisMark = $ScaleDisplay == "M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
                                    #$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : "";
                                    $thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
                                }
                                else
                                {
                                    $thisReportID = $thisReport['ReportID'];
                                    $MarksAry = $this->getMarks($thisReportID, $StudentID);
                                    $thisMark = $ScaleDisplay == "M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
                                    #$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : "";
                                    $thisGrade = $MarksAry[$SubjectID][0]["Grade"];
                                }

                                $thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID=".$ColumnData[$i]['ReportColumnID'] ." and SubjectID=$SubjectID");
                                $thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];

                                # for preview purpose
                                if(!$StudentID)
                                {
                                    $thisMark = $ScaleDisplay == "M" ? "S" : "";
                                    $thisGrade = $ScaleDisplay == "G" ? "G" : "";
                                }

                                # If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
                                $thisMark = ($ScaleDisplay == "M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
                                //2012-0706-1451-01140 5B-26
                                //if ($thisMark != "N.A.") {
                                if ($thisMark != "N.A." && $thisMark != "") {
                                    $isAllNA = false;
                                }

                                //if ($thisMark == "" || $thisMark == "N.A.")
                                if ($thisMark == "" || $thisMark == "N.A." || $thisMark == "-" || strtolower($thisMark) == "abs" || $thisMark == "/")
                                {
                                    $thisMarkDisplay = $this->EmptySymbol;
                                }
                                else
                                {
                                    # check special case
                                    list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
                                    if($needStyle)
                                    {
                                        $thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$thisReportID);
                                        //$thisMark = ($ScaleDisplay=="M" && $StudentID) ? my_round($thisMark*$thisSubjectWeight,2) : $thisMark;
                                        $thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
                                    }
                                    else
                                    {
                                        $thisMarkDisplay = $thisMark;
                                    }

                                    if ($thisMarkDisplay == "N.A. : Not Assessed")
                                    {
                                        $thisMarkDisplay = $this->EmptySymbol;
                                    }
                                }

                            $css_border_left = $i ? "border_left2" : "";
                            $x[$SubjectID] .= "<td class='border_left {$css_border_left} {$css_border_top}'>";
                            $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                            $x[$SubjectID] .= "<td class='report_text' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";

                            if($ShowSubjectFullMark)
                            {
                                $FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1 && $UseWeightedMark) ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID];
                                $x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
                            }

                            $x[$SubjectID] .= "</tr></table>";
                            $x[$SubjectID] .= "</td>";
                        }
                    }

                    # Subject Overall (disable when calculation method is Vertical-Horizontal)
                    # Retrieve Annual result
                    $MarksAry = $this->getMarks($ReportID, $StudentID);
                    $thisMark = $ScaleDisplay == "M" ? $MarksAry[$SubjectID][0]['Mark'] : "";
                    $thisGrade = $ScaleDisplay == "G" ? $MarksAry[$SubjectID][0]['Grade'] : "";

                    # for preview purpose
                    if(!$StudentID)
                    {
                        $thisMark = $ScaleDisplay=="M" ? "S" : "";
                        $thisGrade = $ScaleDisplay=="G" ? "G" : "";
                    }

                    $thisMark = ($ScaleDisplay == "M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
                    if ($thisMark != "N.A." && $thisMark != "*")
                    {
                        $isAllNA = false;
                    }

                    if ($thisMark == "" || $thisMark == "N.A." || $thisMark == "-" || strtolower($thisMark) == "abs" || $thisMark == "*" || $thisMark == "/")
                    {
                        $thisMark = $this->EmptySymbol;
                    }

                    # check special case
                    if ($thisMark == $this->EmptySymbol)
                    {
                        $thisMarkDisplay = $thisMark;
                    }
                    else
                    {
                        list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
                        if($needStyle) {
                            $thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$ReportID);
                            $thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
                        }
                        else {
                            $thisMarkDisplay = $thisMark;
                        }
                    }

                    if ($thisMarkDisplay == "N.A. : Not Assessed") {
                        $thisMarkDisplay = $this->EmptySymbol;
                    }

                    $x[$SubjectID] .= "<td class='border_left2 {$css_border_top}'>";
                    $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                    $x[$SubjectID] .= "<td class='report_text' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
                    if($ShowSubjectFullMark)
                    {
                        $x[$SubjectID] .= "<td class=' report_text' align='center' width='50%'>(". $SubjectFullMarkAry[$SubjectID] .")</td>";
                    }
                    $x[$SubjectID] .= "</tr></table>";
                    $x[$SubjectID] .= "</td>";

                    $isFirst = 0;

                    # construct an array to return
                    $returnArr['HTML'][$SubjectID] = $x[$SubjectID];
                    $returnArr['isAllNA'][$SubjectID] = ($StudentID=='')? false : $isAllNA;
                }
            }	# End Whole Year Report Type
        }
        else
        {
            if($ReportType == "T")	# Temrs Report Type
            {
                $CalculationOrder = $CalSetting["OrderTerm"];

                # Retrieve Term Reports
                $TermReportList = $this->Get_Report_List($ClassLevelID, 'T', 1);

                // loop subjects
                foreach($SubjectArray as $SubjectID => $SubjectName)
                {
                    $isSub = 0;
                    if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;

                    // check if it is a parent subject, if yes find info of its components subjects
                    $CmpSubjectArr = array();
                    $isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
                    if (!$isSub) {
                        $CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
                        if(!empty($CmpSubjectArr)) $isParentSubject = 1;
                    }

                    # define css
                    $css_border_top = "border_top";

                    $isAllNA = true;
                    $isDisplayABS = false;
                    $isDisplayExempt = false;
                    $accumulateMarks = 0;
                    $accumulateTotalMarks = 0;
                    $excludedTotalMarks = 0;

                    // loop term reports
                    foreach((array)$TermReportList as $thisTermReportInfo)
                    {
                        # Retrieve Term Report Data
                        $thisTermReportID = $thisTermReportInfo['ReportID'];
                        $thisTermReportSetting = $this->returnReportTemplateBasicInfo($thisTermReportID);
                        $reportSemID = $thisTermReportSetting['Semester'];
                        $reportSemSeqNumber = $this->Get_Semester_Seq_Number($reportSemID);

                        $ColoumnTitle = $this->returnReportColoumnTitle($thisTermReportID);
                        $ColumnID = array();
                        $ColumnTitle = array();
                        if(sizeof($ColoumnTitle) > 0)
                            foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);

                        # Retrieve Subject Scheme ID & settings
                        $SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $thisTermReportID);
                        $ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
                        $ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
                        $SchemeID = $SubjectFormGradingSettings['SchemeID'];
                        $SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
                        $schemeFullMark = $SchemeInfo['FullMark'];

                        // skip if Input Grade
                        if($ScaleInput == 'G') {
                            $returnArr['HTML'][$SubjectID] = '';
                            $returnArr['isAllNA'][$SubjectID] = true;
                            continue;
                        }

                        # Retrieve Term Report Marks
                        $thisMarksAry = $this->getMarks($thisTermReportID, $StudentID);

                        $preLowerLimit = 100;
                        $defaultRange = array(100, 75, 50, 25);

                        $gradingRangeData = array();
                        if($schemeFullMark > 0)
                        {
                            $SchemeGradingRangeArr = $this->GET_GRADING_SCHEME_RANGE_INFO($SchemeID, $Nature="ALL");
                            foreach((array)$SchemeGradingRangeArr as $thisSchemeRangeInfo)
                            {
                                $gradeLowerLimit = $thisSchemeRangeInfo['LowerLimit'];
                                $gradeLowerLimit = $gradeLowerLimit / $schemeFullMark * 100;

                                foreach($defaultRange as $thisDefaultRange) {
                                    if(($gradeLowerLimit != 100 && $thisDefaultRange == 100 && empty($gradingRangeData)) || ($preLowerLimit > $thisDefaultRange && $gradeLowerLimit < $thisDefaultRange)) {
                                        $gradingRangeData[] = array('&nbsp;', $thisDefaultRange, true);
                                    }
                                }

                                if($thisSchemeRangeInfo['Nature'] == 'F') {
                                    $gradingRangeData[] = array('&nbsp;', $gradeLowerLimit, in_array($gradeLowerLimit, $defaultRange));
                                } else {
                                    $gradingRangeData[] = array($thisSchemeRangeInfo['Grade'], $gradeLowerLimit, in_array($gradeLowerLimit, $defaultRange));
                                }

                                $preLowerLimit = $gradeLowerLimit;
                            }
                        }
                        else
                        {
                            foreach($defaultRange as $thisDefaultRange) {
                                $gradingRangeData[] = array('&nbsp;', $thisDefaultRange, true);
                            }
                        }
                        $gradingRangeData = array_reverse($gradingRangeData);

                        $i = 0;
                        $thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisTermReportID, "ReportColumnID=".$ColumnID[$i] ." and SubjectID=$SubjectID");
                        $thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];

                        $thisMark = $ScaleInput=="M" ? $thisMarksAry[$SubjectID][$ColumnID[$i]]['Mark'] : "";
                        $thisGrade = $thisMarksAry[$SubjectID][$ColumnID[$i]]['Grade'];

                        if($SemSeqNumber >= $reportSemSeqNumber) {
                            // special handling for elective subjects
                            //if($thisSubjectWeight == 1 && $StudentID && !$this->Is_Student_In_Subject_Group_Of_Subject($thisTermReportID, $StudentID, $SubjectID)) {

                            // [2020-0408-1536-14164] exclude if student not in subject group
                            if($StudentID && !$this->Is_Student_In_Subject_Group_Of_Subject($thisTermReportID, $StudentID, $SubjectID)) {
                                if($thisSubjectWeight != 1) {
                                    $excludedTotalMarks += $schemeFullMark * $thisSubjectWeight;
                                }
                                continue;
                            }

                            // [2020-0414-1054-54164] exclude for special case
                            if ($thisGrade == "N.A.") {
                                $excludedTotalMarks += $schemeFullMark * $thisSubjectWeight;
                            }
                            else if ($thisGrade == "-" || strtolower($thisGrade) == "abs") {
                                $isDisplayABS = true;
                            }
                            else if ($thisGrade == "/") {
                                $isDisplayExempt = true;
                            }
                            else {
                                $accumulateMarks += $thisMark * $thisSubjectWeight;
                                $accumulateTotalMarks += $schemeFullMark * $thisSubjectWeight;
                            }

                            //$usePercentage = $thisTermReportInfo['PercentageOnColumnWeight'];
                            //if($usePercentage) {
                            //    $accumulateMarks = $accumulateMarks / 100;
                            //    $accumulateTotalMarks = $accumulateTotalMarks / 100;
                            //}
                        }

                        if ($thisTermReportID == $ReportID)
                        {
                            $thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade=='') ? $thisMark : $thisGrade;
                            if ($thisMark != "N.A.") {
                                $isAllNA = false;
                            }

                            if($thisMark == "-" || strtolower($thisMark) == "abs") {
                                // show abs as 'N.A.' for Daily column
                                $isDisplayABS = true;
                            }

                            if($thisMark == "/") {
                                // show abs as 'N.A.' for Daily column
                                $isDisplayExempt = true;
                            }
                        }
                    }

                    // skip if all N.A.
                    if($isAllNA) {
                        $returnArr['HTML'][$SubjectID] = '';
                        $returnArr['isAllNA'][$SubjectID] = true;
                        continue;
                    }

                    # Retrieve Subject Scheme ID & settings
                    $SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID);
                    $ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
                    $ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
                    $SchemeID = $SubjectFormGradingSettings['SchemeID'];
                    $SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
                    $schemeFullMark = $SchemeInfo['FullMark'];

                    // Handling for ABS
                    if ($isDisplayABS) {
                        $thisMarkDisplay = 'N.A.';
                        $accumulateMarks = 0;
                        $accumulateTotalMarks = 0;
                    }
                    // Handling for Exempt
                    else if ($isDisplayExempt) {
                        $thisMarkDisplay = $this->EmptySymbol;
                        $accumulateMarks = 0;
                        $accumulateTotalMarks = 0;
                    }
                    else {
                        //$thisMarkGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $accumulateMarks, $ReportID, $StudentID, $SubjectID, $ClassLevelID, 0);
                        //if(round($accumulateTotalMarks) == $schemeFullMark) {
                        //    $thisMarkDisplay = $StudentID ? $thisMarkGrade : 'G';
                        //}
                        //else {
                        //    $thisGradeDecriptor = $this->getGradeDescriptor($thisMarkGrade);
                        //    $thisMarkDisplay = $StudentID ? (!empty($thisGradeDecriptor) ? $thisGradeDecriptor[0]['Descriptor_en'] : $this->emptySymbol) : 'DES';
                        //}

                        if($excludedTotalMarks > 0) {
                            $schemeFullMark = $schemeFullMark - $excludedTotalMarks;
                        }
                        if($schemeFullMark > 0) {
                            $accumulateMarks = $this->ROUND_MARK(($accumulateMarks / $schemeFullMark * 100), 'SubjectTotal');
                            $accumulateTotalMarks = $this->ROUND_MARK(($accumulateTotalMarks / $schemeFullMark * 100), 'SubjectTotal');
                        } else {
                            $accumulateMarks = 0;
                            $accumulateTotalMarks = 0;
                        }

                        ### according to template progress percentage to calculate grade
                        $accumulateMarkCalculated = $StudentID && $accumulateTotalMarks > 0 ? $accumulateMarks / $accumulateTotalMarks * 100 : 0;
                        $thisMarkGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $accumulateMarkCalculated, $ReportID, $StudentID, $SubjectID, $ClassLevelID, 0);
                        if($this->ROUND_MARK($accumulateTotalMarks, 'SubjectTotal') >= $SchemeInfo['FullMark']) {
                            $thisMarkDisplay = $StudentID ? $thisMarkGrade : 'G';
                        }
                        else {
                            $thisGradeDecriptor = $this->getGradeDescriptor($thisMarkGrade);
                            $thisMarkDisplay = $StudentID ? (!empty($thisGradeDecriptor) ? $thisGradeDecriptor[0]['Descriptor_en'] : $this->emptySymbol) : 'DES';
                        }
                    }

                    $x[$SubjectID] .= "<td class='border_left border_right border_top '>";
                    $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                    $x[$SubjectID] .= "<td align='center' class='report_text' width='100%'>". $thisMarkDisplay ."</td>";
                    $x[$SubjectID] .= "</tr></table>";
                    $x[$SubjectID] .= "</td>";

                    $x[$SubjectID] .= "<td class='border_left border_top' style='padding: 0px 3px 0px 3px;' >";
                    if (!$isSub)
                    {
                        $x[$SubjectID] .= "<table class=\"range_row\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style='table-layout: fixed; width: 100%; border-collapse: collapse;'><tr>";

                        $currentRange = 0;
                        foreach((array)$gradingRangeData as $this_index => $thisGrandingRange)
                        {
                            $currentGradingRange = $thisGrandingRange[1];
                            if($currentGradingRange == 0) {
                                continue;
                            }

                            $diffRange = $currentGradingRange - $currentRange;

                            $displayGradeName = '&nbsp;';
                            if($gradingRangeData[$this_index - 1][0] != '&nbsp;') {
                                $displayGradeName = $gradingRangeData[$this_index - 1][0];
                            }

                            $display_border_left = $currentRange == 0 ? ' border_left ' : '';
                            $display_border_right = $thisGrandingRange[2] || $currentGradingRange == 100 ? ' border_right ' : '';
                            $x[$SubjectID] .= "<td align='left' class='report_text $display_border_left $display_border_right' width='".$diffRange."%'><i>".$displayGradeName."</i></td>";

                            $currentRange = $currentGradingRange;
                        }

                        $x[$SubjectID] .= "</tr></table>";
                    }

                    $preLowerLimit = 100;
                    $backgroundColor = 'white';
                    $defaultRange = array(100, 75, 50, 25);

                    $gradingRangeData = array();
                    $displayRangeAry = array();
                    if($accumulateTotalMarks != $defaultRange[0]) {
                        $displayRangeAry[] = array($defaultRange[0], 'white');
                    } else {
                        $backgroundColor = 'lightgrey';
                    }
                    if($accumulateMarks != $accumulateTotalMarks) {
                        $displayRangeAry[] = array($accumulateTotalMarks, 'lightgrey');
                    }
                    if(empty($displayRangeAry)) {
                        $backgroundColor = 'black';
                    }
                    $displayRangeAry[] = array($accumulateMarks, 'black');
                    if($accumulateMarks > 0 || $accumulateTotalMarks > 0)
                    {
                        foreach((array)$displayRangeAry as $thisDisplayRangeInfo)
                        {
                            $thisDisplayRange = $thisDisplayRangeInfo[0];
                            foreach($defaultRange as $thisDefaultRange) {
                                if($preLowerLimit > $thisDefaultRange && $thisDisplayRange < $thisDefaultRange) {
                                    $gradingRangeData[] = array($thisDefaultRange, true, $backgroundColor);
                                }
                            }
                            $gradingRangeData[] = array($thisDisplayRange, in_array($thisDisplayRange, $defaultRange), $thisDisplayRangeInfo[1]);

                            $preLowerLimit = $thisDisplayRange;
                            $backgroundColor = $thisDisplayRangeInfo[1];
                        }
                    }
                    else
                    {
                        foreach($defaultRange as $thisDefaultRange) {
                            $gradingRangeData[] = array($thisDefaultRange, true, $backgroundColor);
                        }
                    }
                    $gradingRangeData = array_reverse($gradingRangeData);

                    $x[$SubjectID] .= "<table class=\"range_row\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style='table-layout: fixed; width: 100%; border-collapse: collapse;'><tr>";

                    $currentRange = 0;
                    foreach((array)$gradingRangeData as $this_index => $thisGrandingRange)
                    {
                        $currentGradingRange = $thisGrandingRange[0];
                        if ($currentGradingRange == 0) {
                            continue;
                        }

                        $diffRange = $currentGradingRange - $currentRange;
                        $display_border_left = $currentRange == 0 ? ' border_left ' : '';
                        $display_border_right = $thisGrandingRange[1] ? ' border_right ' : '';

                        $x[$SubjectID] .= "<td align='center' class='report_text $display_border_left $display_border_right' width='".$diffRange."%' height=\"14\" style='background-color:".$thisGrandingRange[2]."; border-top: 2px solid black;'>&nbsp;</td>";

                        $currentRange = $currentGradingRange;
                    }
                    $x[$SubjectID] .= "</tr></table>";

                    if (!$isSub)
                    {
                        $x[$SubjectID] .= "<table class=\"range_row\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"table-layout: fixed; width: 100%; border-collapse: collapse;\"><tr>";
                        $x[$SubjectID] .= "<td align='center' class='report_text border_left border_right' width='25%'  height=\"9\">&nbsp;</td>";
                        $x[$SubjectID] .= "<td align='center' class='report_text border_right' width='25%'  height=\"9\">&nbsp;</td>";
                        $x[$SubjectID] .= "<td align='center' class='report_text border_right' width='25%'  height=\"9\">&nbsp;</td>";
                        $x[$SubjectID] .= "<td align='center' class='report_text border_right' width='25%'  height=\"9\">&nbsp;</td>";
                        $x[$SubjectID] .= "</tr></table>";
                    }

                    $x[$SubjectID] .= "</td>";

                    $isFirst = 0;

                    # construct an array to return
                    $returnArr['HTML'][$SubjectID] = $x[$SubjectID];
                    $returnArr['isAllNA'][$SubjectID] = ($StudentID=='')? false : $isAllNA;	// show all subjects for preview
                }
            } # End if($ReportType=="T")
            else					# Whole Year Report Type
            {
                global $eRCTemplateSetting;
                
                $CalculationOrder = $CalSetting["OrderFullYear"];

                # Retrieve Invloved Temrs
                $ColumnData = $this->returnReportTemplateColumnData($ReportID);
                
                # Retrieve Year Report (Daily)
                $DailyYearReport = $this->returnReportTemplateBasicInfo('', 'ReportTitle LIKE \'%'.$eRCTemplateSetting['Report']['ReportGeneration']['SpecialYearReport_ReportIdentifier'].'%\'', $ClassLevelID, 'F', 1);
                $thisDailyReportID = $DailyYearReport['ReportID'];

                foreach($SubjectArray as $SubjectID => $SubjectName)
                {
                    # Retrieve Subject Scheme ID & Settings
                    $SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0 ,0, $ReportID);
                    $ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];

                    // [2020-0702-1026-53164] Skip if Input Grade
                    $ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
                    if($ScaleInput == 'G') {
                        $returnArr['HTML'][$SubjectID] = '';
                        $returnArr['isAllNA'][$SubjectID] = true;
                        continue;
                    }

                    $t = "";
                    $isSub = 0;
                    if(in_array($SubjectID, $MainSubjectIDArray) != true)	$isSub = 1;

                    // Check if it is a parent subject, if yes find info of its components subjects
                    $CmpSubjectArr = array();
                    $isParentSubject = 0;		// Set to "1" when one ScaleInput of component subjects is Mark (M)
                    if (!$isSub) {
                        $CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
                        if(!empty($CmpSubjectArr))  $isParentSubject = 1;
                    }

                    $displayPosition = '';
                    $displayEffort = '';

                    $isAllNA = true;
                    $isHideSubject = false;
                    $columnDataSize = sizeof($ColumnData);

                    // loop columns
                    // for($i=$columnDataSize-1; $i<$columnDataSize; $i++)
                    for($i=0; $i<$columnDataSize; $i++)
                    {
                        $css_border_top = "border_top";

                        # See if any term reports available
                        $thisReport = $this->returnReportTemplateBasicInfo("", "Semester=".$ColumnData[$i]['SemesterNum'] ." AND ClassLevelID=".$ClassLevelID);

                        # Terms's Assesment / Terms Result (3rd Term only)
                        if($i == $columnDataSize - 1)
                        {
                            # if no term reports, CANNOT retrieve assessment result at all
                            if (empty($thisReport))
                            {
                                for($j=0; $j<sizeof($ColumnID); $j++) {
                                    $x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>--</td>";
                                }
                            }
                            else
                            {
                                $thisReportID = $thisReport['ReportID'];
                                $thisMarksAry = $this->getMarks($thisReportID, $StudentID);

                                $ColumnID = array();
                                $ColumnTitle = array();
                                $ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
                                foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);

                                // loop term report columns
                                for($j=0; $j<sizeof($ColumnID); $j++)
                                {
                                    // always display EmptySymbol for parent subject
                                    if ($isParentSubject)
                                    {
                                        $thisMarkDisplay = $this->EmptySymbol;
                                    }
                                    // Daily Column display
                                    else if($j == 0)
                                    {
                                        if($thisDailyReportID)
                                        {
                                            # Daily Report - Subject Overall
                                            $DailyMarksAry = $this->getMarks($thisDailyReportID, $StudentID);
                                            $thisMark = $ScaleDisplay == "M" ? $DailyMarksAry[$SubjectID][0][Mark] : "";
                                            $thisGrade = $ScaleDisplay == "G" ? $DailyMarksAry[$SubjectID][0][Grade] : "";
                                            
                                            # for preview purpose
                                            if(!$StudentID)
                                            {
                                                $thisMark = $ScaleDisplay == "M" ? "S" : "";
                                                $thisGrade = $ScaleDisplay == "G" ? "G" : "";
                                            }
                                            
                                            $thisMark = ($ScaleDisplay == "M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark, 2) : $thisMark) : $thisGrade;
                                            if ($thisMark != "N.A." && $thisMark != "*") {
                                                $isAllNA = false;
                                            }
                                            if ($thisMark == "*") {
                                                $isHideSubject = true;
                                            }
                                            
                                            //if ($thisMark == "" || $thisMark == "N.A." || $thisMark == "-" || strtolower($thisMark) == "abs" || $thisMark == "/")
                                            if ($thisMark == "" || $thisMark == "N.A." || $thisMark == "/" || $thisMark == "*")
                                            {
                                                //$thisMarkDisplay = $this->EmptySymbol;
                                                $thisMark = $this->EmptySymbol;
                                            }
                                            /* 
                                            else if ($thisMark == "-" || strtolower($thisMark) == "abs")
                                            {
                                                $thisMarkDisplay = $eReportCard['RemarkAbsentNotConsidered'];
                                            }
                                             */
                                            
                                            # Check special case
                                            $thisNature = '';
                                            if ($thisMark == $this->EmptySymbol) {
                                                $thisMarkDisplay = $thisMark;
                                            }
                                            else {
                                                list($thisMark, $needStyle) = $this->checkSpCase($thisDailyReportID, $SubjectID, $thisMark, $DailyMarksAry[$SubjectID][0]['Grade']);
                                                if($needStyle) {
                                                    $thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark, '', '', $thisDailyReportID);
                                                    $thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
                                                }
                                                else {
                                                    $thisMarkDisplay = $thisMark;
                                                }
                                            }
                                            
                                            if ($thisMarkDisplay == "N.A. : Not Assessed") {
                                                $thisMarkDisplay = $this->EmptySymbol;
                                            }
                                        }
                                        else 
                                        {
                                            $thisMarkDisplay = $this->EmptySymbol;
                                        }
                                    }
                                    else
                                    {
                                        $thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID=".$ColumnID[$j] ." and SubjectID=$SubjectID");
                                        $thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];

                                        $thisMark = $ScaleDisplay == "M" ? $thisMarksAry[$SubjectID][$ColumnID[$j]][Mark] : "";
                                        $thisGrade = $ScaleDisplay == "G" ? $thisMarksAry[$SubjectID][$ColumnID[$j]][Grade] : "";

                                        # for preview purpose
                                        if(!$StudentID)
                                        {
                                            $thisMark = $ScaleDisplay == "M" ? "S" : "";
                                            $thisGrade = $ScaleDisplay == "G" ? "G" : "";
                                        }

                                        $thisMark = ($ScaleDisplay == "M" && strlen($thisMark)) ? $thisMark : $thisGrade;
                                        if ($thisMark != "N.A." && $thisMark != "*") {
                                            $isAllNA = false;
                                        }
                                        if ($thisMark == "*") {
                                            $isHideSubject = true;
                                        }
                                        
                                        //if ($thisMark == "" || $thisMark == "N.A." || $thisMark == "-" || strtolower($thisMark) == "abs" || $thisMark == "/")
                                        if ($thisMark == "" || $thisMark == "N.A." || $thisMark == "/" || $thisMark == "*")
                                        {
                                            $thisMarkDisplay = $this->EmptySymbol;
                                        }
                                        /* else if ($thisMark == "-" || strtolower($thisMark) == "abs")
                                        {
                                            $thisMarkDisplay = $eReportCard['RemarkAbsentNotConsidered'];
                                        } */
                                        else
                                        {
                                            # Check special case
                                            list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
                                            if($needStyle)
                                            {
                                                $thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight != 0) ? $thisMark / $thisSubjectWeight : $thisMark;
                                                $thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp,'','',$thisReportID);
                                                $thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
                                            }
                                            else
                                            {
                                                $thisMarkDisplay = $thisMark;
                                            }

                                            if ($thisMarkDisplay == "N.A. : Not Assessed")
                                            {
                                                $thisMarkDisplay = $this->EmptySymbol;
                                            }
                                        }
                                    }

                                    $x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
                                    $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                                    $x[$SubjectID] .= "<td class='report_text' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
                                    $x[$SubjectID] .= "</tr></table>";
                                    $x[$SubjectID] .= "</td>";
                                }
                            }
                        }

                        /* 
                        # Term Position
                        # Position - get from manual adjustment first. If no adjustment found, get from the generated position
                        # Hide position for subject components
                        if ($StudentID && !empty($thisReport))
                        {
                            $thisReportID = $thisReport['ReportID'];
                            $thisMarksAry = $this->getMarks($thisReportID, $StudentID);

                            $thisMark = $ScaleDisplay == "M" ? $thisMarksAry[$SubjectID][0]['Mark'] : "";
                            $thisGrade = $ScaleDisplay == "G" ? $thisMarksAry[$SubjectID][0]['Grade'] : "";

                            $thisMark = ($ScaleDisplay == "M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
                            if ($thisMark == "" || $thisMark == "N.A." || $thisMark == "-" || strtolower($thisMark) == "abs" || $thisMark == "*" || $thisMark == "/") {
                                $thisMark = $this->EmptySymbol;
                            }

                            $thisNature = '';
                            if ($thisMark != $this->EmptySymbol)
                            {
                                list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][0]['Grade']);
                                if($needStyle) {
                                    $thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$thisReportID);
                                }
                            }

                            if(!$isSub && $thisNature != 'Fail')
                            {
                                $FormStudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($thisReportID, $ClassLevelID);
                                $numOfStudent = count($FormStudentInfoArr);

                                $FormStudentIDArr = array();
                                for ($j=0; $j<$numOfStudent; $j++) {
                                    $FormStudentIDArr[] = $FormStudentInfoArr[$j]['UserID'];
                                }
                                $FormManualPositionArr = $this->GET_MANUAL_ADJUSTED_POSITION($FormStudentIDArr, $SubjectID, $thisReportID);

                                ### Requested by Lau Sir (uccke #157)
                                ### If all manual positions are empty => display system generated position
                                ### Else display manual adjusted position
                                $isAllEmpty = true;
                                $numOfAdjustedMark = count($FormManualPositionArr);
                                if ($numOfAdjustedMark > 0)
                                {
                                    foreach ($FormManualPositionArr as $thisStudentID => $MarkInfoArr)
                                    {
                                        if (trim($MarkInfoArr["Info"]) != '')
                                        {
                                            $isAllEmpty = false;
                                            break;
                                        }
                                    }
                                }

                                $manualPosition = $FormManualPositionArr[$StudentID]['Info'];
                                if ($isAllEmpty)
                                {
                                    if ($manualPosition == NULL || $manualPosition == 0 || $manualPosition == "") {
                                        $position = $thisMarksAry[$SubjectID][0]['OrderMeritSubjectGroup'] ? $thisMarksAry[$SubjectID][0]['OrderMeritSubjectGroup'] : $this->EmptySymbol;
                                    }
                                    else {
                                        $position = $manualPosition;
                                    }
                                }
                                else
                                {
                                    if ($manualPosition == '') {
                                        $position = $this->EmptySymbol;
                                    }
                                    else {
                                        $position = $manualPosition;
                                    }
                                }
                                if ($position > $this->MaxPosition || $position == -1)
                                {
                                    $position = $this->EmptySymbol;
                                }

                                # Store Term Position if valid
                                if($position != $this->EmptySymbol && is_numeric($position))
                                {
                                    $displayPosition = $position;
                                }
                            }
                        }
                        */

                        # Term Effort
                        if ($StudentID && !empty($thisReport))
                        {
                            $thisReportID = $thisReport['ReportID'];
                            $temp = $this->GET_EXTRA_SUBJECT_INFO(array($StudentID), $SubjectID, $thisReportID);
                            $effort = $temp[$StudentID]['Info'];
                            $effort = $effort ? $effort : $this->EmptySymbol;

                            # Store Term Effort if valid
                            if($effort != $this->EmptySymbol && $effort != '')
                            {
                                $displayEffort = $effort;
                            }
                        } 
                    }

                    # Subject Overall (disable when calculation method is Vertical-Horizontal)
                    # Retrieve Annual result
                    $MarksAry = $this->getMarks($ReportID, $StudentID);
                    $thisMark = $ScaleDisplay == "M" ? $MarksAry[$SubjectID][0][Mark] : "";
                    $thisGrade = $ScaleDisplay == "G" ? $MarksAry[$SubjectID][0][Grade] : "";

                    # for preview purpose
                    if(!$StudentID)
                    {
                        $thisMark = $ScaleDisplay == "M" ? "S" : "";
                        $thisGrade = $ScaleDisplay == "G" ? "G" : "";
                    }

                    $thisMark = ($ScaleDisplay == "M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
                    if ($thisMark != "N.A." && $thisMark != "*") {
                        $isAllNA = false;
                    }
                    if ($thisMark == "*") {
                        $isHideSubject = true;
                    }
                    
                    //if ($thisMark == "" || $thisMark == "N.A." || $thisMark == "-" || strtolower($thisMark) == "abs" || $thisMark == "/")
                    if ($thisMark == "" || $thisMark == "N.A." || $thisMark == "/" || $thisMark == "*")
                    {
                        //$thisMarkDisplay = $this->EmptySymbol;
                        $thisMark = $this->EmptySymbol;
                    }
                    /* 
                    else if ($thisMark == "-" || strtolower($thisMark) == "abs")
                    {
                        $thisMarkDisplay = $eReportCard['RemarkAbsentNotConsidered'];
                    }

                    if ($thisMark == "" || $thisMark == "N.A." || $thisMark == "-" || strtolower($thisMark) == "abs" || $thisMark == "*" || $thisMark == "/")
                    {
                        $thisMark = $this->EmptySymbol;
                    }
                    */
                    
                    # Check special case
                    $thisNature = '';
                    if ($thisMark == $this->EmptySymbol) {
                        $thisMarkDisplay = $thisMark;
                    }
                    else {
                        list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
                        if($needStyle) {
                            $thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$ReportID);
                            $thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
                        }
                        else {
                            $thisMarkDisplay = $thisMark;
                        }
                    }

                    if ($thisMarkDisplay == "N.A. : Not Assessed") {
                        $thisMarkDisplay = $this->EmptySymbol;
                    }

                    $x[$SubjectID] .= "<td class='border_left2 {$css_border_top}'>";
                    $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                    $x[$SubjectID] .= "<td class='report_text' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
                    $x[$SubjectID] .= "</tr></table>";
                    $x[$SubjectID] .= "</td>";

                    # UCCKE
                    # Position - get from manual adjustment first. If no adjustment found, get from the generated position
                    # Hide position for subject components
                    if (!$isSub && $thisNature != 'Fail')
                    {
                        $FormStudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
                        $numOfStudent = count($FormStudentInfoArr);

                        $FormStudentIDArr = array();
                        for ($j=0; $j<$numOfStudent; $j++) {
                            $FormStudentIDArr[] = $FormStudentInfoArr[$j]['UserID'];
                        }
                        $FormManualPositionArr =  $this->GET_MANUAL_ADJUSTED_POSITION($FormStudentIDArr, $SubjectID, $ReportID);

                        ### Requested by Lau Sir (uccke #157)
                        ### If all manual positions are empty => display system generated position
                        ### Else display manual adjusted position
                        $isAllEmpty = true;
                        $numOfAdjustedMark = count($FormManualPositionArr);
                        if ($numOfAdjustedMark > 0)
                        {
                            foreach ($FormManualPositionArr as $thisStudentID => $MarkInfoArr)
                            {
                                if (trim($MarkInfoArr["Info"]) != '')
                                {
                                    $isAllEmpty = false;
                                    break;
                                }
                            }
                        }
                        
                        $manualPosition = $FormManualPositionArr[$StudentID]['Info'];
                        if ($isAllEmpty)
                        {
                            if ($manualPosition == NULL || $manualPosition == 0 || $manualPosition == "")
                            {
                                $position = $MarksAry[$SubjectID][0]['OrderMeritSubjectGroup'] ? $MarksAry[$SubjectID][0]['OrderMeritSubjectGroup'] : $this->EmptySymbol;
                            }
                            else
                            {
                                $position = $manualPosition;
                            }
                        }
                        else
                        {
                            if ($manualPosition == '')
                            {
                                $position = $this->EmptySymbol;
                            }
                            else
                            {
                                $position = $manualPosition;
                            }
                        }
                        if ($position > $this->MaxPosition || $position == -1)
                        {
                            $position = $this->EmptySymbol;
                        }

                        // [2020-0707-1607-52164] Hardcode S1-3 PE Position to "----" in Yearly Report
                        if($FormNumber > 0 && $FormNumber <= 3)
                        {
                            $SubjectCodeMapping = $this->GET_SUBJECTS_CODEID_MAP();
                            $thisSubjectCode = $SubjectCodeMapping[$SubjectID];
                            if($thisSubjectCode == '310') {
                                $position = $this->EmptySymbol;
                            }
                        }
                        
                        /* 
                        # Apply Term Position if no Overall Position
                        if ($position == $this->EmptySymbol && $displayPosition != '')
                        {
                            $position = $displayPosition;
                        }
                         */
                        
                        $x[$SubjectID] .= "<td class='border_left2 {$css_border_top}'>";
                        $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                        $x[$SubjectID] .= "<td align='center' class='report_text' width='100%'>". $position ."</td>";
                        $x[$SubjectID] .= "</tr></table>";
                        $x[$SubjectID] .= "</td>";
                    }
                    else
                    {
                        $x[$SubjectID] .= "<td class='border_left2 {$css_border_top} report_text' align='center'>";
                        $x[$SubjectID] .= $this->EmptySymbol;
                        $x[$SubjectID] .= "</td>";
                    }

                    # UCCKE
                    # Effot
                    $temp = $this->GET_EXTRA_SUBJECT_INFO(array($StudentID), $SubjectID, $ReportID);
                    $effort = $temp[$StudentID]['Info'];
                    $effort = $effort ? $effort : $this->EmptySymbol;
                  
                    # Apply Term Effort if no Overall Effort
                    if ($effort == $this->EmptySymbol && $displayEffort != '')
                    {
                        $effort = $displayEffort;
                    }
                    
                    $x[$SubjectID] .= "<td class='border_left2 {$css_border_top}'>";
                    $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                    $x[$SubjectID] .= "<td align='center' class='report_text' width='100%'>". $effort ."</td>";
                    $x[$SubjectID] .= "</tr></table>";
                    $x[$SubjectID] .= "</td>";

                    $isFirst = 0;

                    # Construct an array to return
                    $returnArr['HTML'][$SubjectID] = $x[$SubjectID];
                    $returnArr['isAllNA'][$SubjectID] = ($StudentID == '') ? false : ($isAllNA || $isHideSubject);
                }
            }	# End Whole Year Report Type
        }

		return $returnArr;
	}

	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
		
		# Retrieve Basic Information of Report
		$BasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$AllowClassTeacherComment = $BasicInfo['AllowClassTeacherComment'];
		$ClassLevelID = $BasicInfo['ClassLevelID'];
        $FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode = 1);
		$LineHeight = $BasicInfo['LineHeight'];
		$SemID = $BasicInfo['Semester'];
        $SemSeqNumber = $this->Get_Semester_Seq_Number($SemID);
		$ReportType = $SemID == "F" ? "W" : "T";

		if($FormNumber == 6)
        {
            if($ReportType=="T")
            {
                $ColumnTitle = $this->returnReportColoumnTitle($ReportID);

                # retrieve the latest Term
                $latestTerm = "";
                $sems = $this->returnReportInvolvedSem($ReportID);
                foreach($sems as $TermID=>$TermName)
                    $latestTerm = $TermID;
                //$latestTerm++;

                # retrieve Student Class
                if($StudentID)
                {
                    include_once($PATH_WRT_ROOT."includes/libuser.php");
                    $lu = new libuser($StudentID);
                    $ClassInfo  = $this->Get_Student_Class_ClassLevel_Info($StudentID);
                    $ClassID    = $ClassInfo[0]['ClassID'];
                    $ClassName 	= $ClassInfo[0]['ClassName'];
                    //$ClassName = $lu->ClassName;
                    $WebSamsRegNo = $lu->WebSamsRegNo;
                }

                # build csv data array
                $ary = array();
                $csvType = $this->getOtherInfoType();
                foreach($csvType as $k=>$Type)
                {
                    $csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassID);
                    if(!empty($csvData))
                    {
                        foreach($csvData as $RegNo=>$data)
                        {
                            //if($RegNo == $WebSamsRegNo)
                            if($RegNo == $StudentID)
                            {
                                foreach($data as $key=>$val)
                                    $ary[$key] = $val;
                            }
                        }
                    }
                }

                # retrieve student result
                $StudentResultAry = $this->getReportResultScore($ReportID, 0, $StudentID);

                # build result data
                $Conduct = $ary['Conduct'] ? $ary['Conduct'] : $this->EmptySymbol;
                $ECA = $ary['ECA'] ? $ary['ECA'] : $this->EmptySymbol;
                $Reading = $ary['Reading'] ? $ary['Reading'] : $this->EmptySymbol;
                // cannot use the code below because GPA=0 will be treated as missing GPA
                //$GPA = $ary['GPA'] ? $ary['GPA'] : $this->EmptySymbol;
                $GPA = $StudentResultAry['GPA'];
                if (is_numeric($GPA) || $GPA=='')
                    $GPA = ($GPA=="" || $GPA==-1)? $this->EmptySymbol : number_format($GPA, 2);

                $ClassPosition = ($StudentResultAry['OrderMeritClass'] && $StudentResultAry['OrderMeritClass'] > 0) ? $StudentResultAry['OrderMeritClass'] : $this->EmptySymbol;
                if ($ClassPosition > $this->MaxPosition) $ClassPosition = $this->EmptySymbol;

                $x .= "<br>";
                $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='report_border'>";
                $x .= "<tr>";
                $x .= "<td valign='top' width='20%'>";
                    $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" class='report_border'>";
                    $x .= "<tr><td valign='top' align='center' class='report_text'>Conduct</td></tr>";
                    $x .= "<tr><td valign='top' align='center' class='border_top report_text'>". $Conduct ."</td></tr>";
                    $x .= "</table>";
                $x .= "</td>";
                /*
                $x .= "<td valign='top' width='20%'>";
                    $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='report_border'>";
                    $x .= "<tr><td valign='top' align='center' class='report_text'>ECA</td></tr>";
                    $x .= "<tr><td valign='top' align='center' class='border_top report_text'>". $ECA ."</td></tr>";
                    $x .= "</table>";
                $x .= "</td>";
                */

                # Hide Reading for F.4 - F.7
                // [2018-1210-1427-43206] for F.4 (I.S.)
                $FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode = 1);
                if ($FormNumber < 4)
                {
                    $x .= "<td valign='top' width='20%'>";
                    $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" class='report_border'>";
                    $x .= "<tr><td valign='top' align='center' class='report_text'>Reading</td></tr>";
                    $x .= "<tr><td valign='top' align='center' class='border_top report_text'>". $Reading ."</td></tr>";
                    $x .= "</table>";
                    $x .= "</td>";
                }

                $x .= "<td valign='top' width='20%'>";
                    $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" class='report_border'>";
                    $x .= "<tr><td valign='top' align='center' class='report_text'>Grade Point Average</td></tr>";
                    $x .= "<tr><td valign='top' align='center' class='border_top report_text'>". $GPA ."</td></tr>";
                    $x .= "</table>";
                $x .= "</td>";

                //2012-1211-1805-16140
                if ($FormNumber < 4) {
                    $x .= "<td valign='top' width='20%'>";
                        $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" class='report_border'>";
                        $x .= "<tr><td valign='top' align='center' class='report_text'>Position in Class</td></tr>";
                        $x .= "<tr><td valign='top' align='center' class='border_top report_text'>". $ClassPosition ."</td></tr>";
                        $x .= "</table>";
                    $x .= "</td>";
                }

                $x .= "</tr>";
                $x .= "</table>";
                return $x;
            }
        }
        else
        {
            $SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
            $MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
            if(sizeof($MainSubjectArray) > 0)
                foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
            $SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];

            $n = 0;
            $x = array();
            $isFirst = 1;

            $CalculationOrder = $CalSetting["OrderTerm"];
            $TermReportList = $this->Get_Report_List($ClassLevelID, 'T', 1);

            foreach((array)$SubjectArray as $SubjectID => $SubjectName)
            {
                $isSub = 0;
                if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;

                // check if it is a parent subject, if yes find info of its components subjects
                $CmpSubjectArr = array();
                $isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
                if (!$isSub) {
                    $CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
                    if(!empty($CmpSubjectArr)) $isParentSubject = 1;
                }

                # define css
                $css_border_top = "border_top";

                $isAllNA = true;
                $isHideSubject = false;
                foreach((array)$TermReportList as $thisTermReportInfo)
                {
                    $thisTermReportID = $thisTermReportInfo['ReportID'];
                    $thisTermReportSetting = $this->returnReportTemplateBasicInfo($thisTermReportID);
                    $reportSemID = $thisTermReportSetting['Semester'];
                    $reportSemSeqNumber = $this->Get_Semester_Seq_Number($reportSemID);
                    if($ReportType == 'T' && $SemSeqNumber < $reportSemSeqNumber) {
                        continue;
                    }

                    // [2020-0629-1444-17164] Check if all subject weight = 0
                    $thisTermSubjectWeight = $this->returnReportTemplateSubjectWeightData($thisTermReportID, "ReportColumnID IS NULL AND Weight IS NOT NULL AND Weight > 0");
                    if(empty($thisTermSubjectWeight)) {
                        continue;
                    }

                    # Retrieve Subject Scheme ID & settings
                    $SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $thisTermReportID);
                    $ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
                    $ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];

                    // for Input Grade only
                    if($ScaleInput != 'G') {
                        continue;
                    }

                    $MarksAry = $this->getMarks($thisTermReportID, $StudentID);
                    $thisGrade = $MarksAry[$SubjectID][0]['Grade'];

                    # for preview purpose
                    if(!$StudentID)
                    {
                        $thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
                    }

                    $thisMark = $thisGrade;
                    if ($thisMark != "N.A." && $thisMark != "*") {
                        $isAllNA = false;
                    }
                    if ($thisMark == "*") {
                        $isHideSubject = true;
                    }
                    
                    //if ($thisMark == "" || $thisMark == "N.A." || $thisMark == "-" || strtolower($thisMark) == "abs" || $thisMark == "/")
                    if ($thisMark == "" || $thisMark == "N.A." || $thisMark == "/" || $thisMark == "*")
                    {
                        $thisMark = $this->EmptySymbol;
                    }

                    # check special case
                    if ($thisMark == $this->EmptySymbol) {
                        $thisMarkDisplay = $thisMark;
                    }
                    else {
                        list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
                        if($needStyle) {
                            $thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$ReportID);
                            $thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
                        }
                        else {
                            $thisMarkDisplay = $thisMark;
                        }
                    }

                    if ($thisMarkDisplay == "N.A. : Not Assessed") {
                        $thisMarkDisplay = $this->EmptySymbol;
                    }

                    $x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
                    $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                    $x[$SubjectID] .= "<td class='report_text' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
                    $x[$SubjectID] .= "</tr></table>";
                    $x[$SubjectID] .= "</td>";
                }

                if($isAllNA || $isHideSubject) {
                    unset($x[$SubjectID]);
                }
            }

            # retrieve Student Class
            if($StudentID)
            {
                include_once($PATH_WRT_ROOT."includes/libuser.php");
                $lu = new libuser($StudentID);
                $ClassInfo  = $this->Get_Student_Class_ClassLevel_Info($StudentID);
                $ClassID    = $ClassInfo[0]['ClassID'];
                $ClassName 	= $ClassInfo[0]['ClassName'];
                //$ClassName = $lu->ClassName;
                $WebSamsRegNo = $lu->WebSamsRegNo;
            }

            # build csv data array
            $ary = array();
            foreach($TermReportList as $thisTermReportInfo)
            {
                $thisTermReportID  = $thisTermReportInfo['ReportID'];
                $thisTermReportSetting = $this->returnReportTemplateBasicInfo($thisTermReportID);
                $reportSemID = $thisTermReportSetting['Semester'];
                $reportSemSeqNumber = $this->Get_Semester_Seq_Number($reportSemID);
                if($ReportType == 'T' && $SemSeqNumber < $reportSemSeqNumber) {
                    continue;
                }

                // [2020-0629-1444-17164] Check if all subject weight = 0
                $thisTermSubjectWeight = $this->returnReportTemplateSubjectWeightData($thisTermReportID, "ReportColumnID IS NULL AND Weight IS NOT NULL AND Weight > 0");
                if(empty($thisTermSubjectWeight)) {
                    continue;
                }

                $csvType = $this->getOtherInfoType();
                foreach($csvType as $k=>$Type)
                {
                    $csvData = $this->getOtherInfoData($Type, $reportSemID, $ClassID);
                    if(!empty($csvData))
                    {
                        foreach($csvData as $RegNo=>$data)
                        {
                            //if($RegNo == $WebSamsRegNo)
                            if($RegNo == $StudentID)
                            {
                                foreach($data as $key => $val) {
                                    $ary[$reportSemID][$key] = $val;
                                }
                            }
                        }
                    }
                }
            }

            $table_width = $ReportType == "W" ? 100 : ($SemSeqNumber + 1)  * 25 ;

            $misc .= "<br>";
            $misc .= "<table width=\"".$table_width."%\" border='0' cellspacing='0' cellpadding='5' class='report_border2'>";
            $misc .= "<tr>";
            $misc .= "<td valign='top' width='33%' class=' border_right {$css_border_top}'>";
            $misc .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
            $misc .= "<td class='tabletext ' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>&nbsp;</td>";
            $misc .= "</tr></table>";
            $misc .= "</td>";

            foreach($TermReportList as $thisTermReportInfo)
            {
                $thisTermReportID = $thisTermReportInfo['ReportID'];
                $thisTermReportSetting = $this->returnReportTemplateBasicInfo($thisTermReportID);
                $reportSemID = $thisTermReportSetting['Semester'];
                $reportSemSeqNumber = $this->Get_Semester_Seq_Number($reportSemID);
                if($ReportType == 'T' && $SemSeqNumber < $reportSemSeqNumber) {
                    continue;
                }

                // [2020-0629-1444-17164] Check if all subject weight = 0
                $thisTermSubjectWeight = $this->returnReportTemplateSubjectWeightData($thisTermReportID, "ReportColumnID IS NULL AND Weight IS NOT NULL AND Weight > 0");
                if(empty($thisTermSubjectWeight)) {
                    continue;
                }

                $SemName = $this->returnSemesters($reportSemID);
                $misc .= "<td valign='top' width='".$eRCTemplateSetting['ColumnWidth']['Mark']."%' class='border_left {$css_border_top}'>";
                $misc .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                $misc .= "<td class='report_text' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>$SemName</td>";
                $misc .= "</tr></table>";
                $misc .= "</td>";
            }
            $misc .= "</tr>";

            foreach($x as $xSubjectID => $xData)
            {
                $SubjectEng = $this->GET_SUBJECT_NAME_LANG($xSubjectID, "EN");
                $SubjectChn = $this->GET_SUBJECT_NAME_LANG($xSubjectID, "CH");

                $misc .= "<tr>";
                foreach((array)$SubjectDisplay as $k => $v)
                {
                    $v = str_replace("SubjectEng", $SubjectEng, $v);
                    $v = str_replace("SubjectChn", $SubjectChn, $v);

                    $misc .= "<td class='border_top border_right border_right' height='{$LineHeight}' valign='middle'>";
                    $misc .= "<table border='0' cellpadding='0' cellspacing='0'>";
                    $misc .= "<tr><td height='{$LineHeight}' class='report_text' valign='middle'>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}' class='report_text' valign='middle'>$v</td>";
                    $misc .= "</tr></table>";
                    $misc .= "</td>";
                }
                $misc .= $xData;
                $misc .= "</tr>";
            }

            $misc .= "<tr>";
            $misc .= "<td class='border_top border_right' height='{$LineHeight}' valign='middle'>";
            $misc .= "<table border='0' cellpadding='0' cellspacing='0'>";
            $misc .= "<tr><td height='{$LineHeight}' class='report_text' valign='middle'>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}' class='report_text' valign='middle'>Conduct</td>";
            $misc .= "</tr></table>";
            $misc .= "</td>";

            foreach($TermReportList as $thisTermReportInfo)
            {
                $thisTermReportID = $thisTermReportInfo['ReportID'];
                $thisTermReportSetting = $this->returnReportTemplateBasicInfo($thisTermReportID);
                $reportSemID = $thisTermReportSetting['Semester'];
                $reportSemSeqNumber = $this->Get_Semester_Seq_Number($reportSemID);
                if($ReportType == 'T' && $SemSeqNumber < $reportSemSeqNumber) {
                    continue;
                }

                // [2020-0629-1444-17164] Check if all subject weight = 0
                $thisTermSubjectWeight = $this->returnReportTemplateSubjectWeightData($thisTermReportID, "ReportColumnID IS NULL AND Weight IS NOT NULL AND Weight > 0");
                if(empty($thisTermSubjectWeight)) {
                    continue;
                }

                $Conduct = $StudentID && isset($ary[$reportSemID]['Conduct']) ? $ary[$reportSemID]['Conduct'] : $this->EmptySymbol;

                $misc .= "<td class='border_left {$css_border_top}'>";
                $misc .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                $misc .= "<td class='report_text' align='center' width='". ($ShowSubjectFullMark ? "50%" : "100%") ."'>". $Conduct ."</td>";
                $misc .= "</tr></table>";
                $misc .= "</td>";
            }
            $misc .= "</tr>";
            $misc .= "</table>";

            if($ReportType == "W")
            {
                # retrieve student result
                $StudentResultAry = $this->getReportResultScore($ReportID, 0, $StudentID);

                # build result data
                $GPA = $StudentResultAry['GPA'];
                if (is_numeric($GPA) || $GPA=='')
                    $GPA = ($GPA=="" || $GPA==-1)? $this->EmptySymbol : number_format($GPA, 2);

                $ClassPosition = ($StudentResultAry['OrderMeritClass'] && $StudentResultAry['OrderMeritClass'] > 0) ? $StudentResultAry['OrderMeritClass'] : $this->EmptySymbol;
                if ($FormNumber == 4 || $FormNumber == 5 || $ClassPosition > $this->MaxPosition) {
                    $ClassPosition = $this->EmptySymbol;
                }

                $misc .= "<br>";
                $misc .= "<table width='100%' border='0' cellspacing='0' cellpadding='5' class='report_border2'>";
                $misc .= "<tr>";
                $misc .= "<td class='border_top border_right' height='{$LineHeight}' valign='middle' width='50%' align='center'>";
                $misc .= "<table border='0' cellpadding='0' cellspacing='0'>";
                $misc .= "<tr><td height='{$LineHeight}' class='report_text' valign='middle'>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}' class='report_text' valign='middle'>Grade Point Average</td>";
                $misc .= "</tr></table>";
                $misc .= "</td>";
                $misc .= "<td class='border_top border_right' height='{$LineHeight}' valign='middle' width='50%' align='center'>";
                $misc .= "<table border='0' cellpadding='0' cellspacing='0'>";
                $misc .= "<tr><td height='{$LineHeight}' class='report_text' valign='middle'>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}' class='report_text' valign='middle'>Position in Class</td>";
                $misc .= "</tr></table>";
                $misc .= "</td>";
                $misc .= "</tr>";
                $misc .= "<tr>";
                $misc .= "<td class='border_top border_right' height='{$LineHeight}' valign='middle' width='50%' align='center'>";
                $misc .= "<table border='0' cellpadding='0' cellspacing='0'>";
                $misc .= "<tr><td height='{$LineHeight}' class='report_text' valign='middle'>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}' class='report_text' valign='middle'>$GPA</td>";
                $misc .= "</tr></table>";
                $misc .= "</td>";
                $misc .= "<td class='border_top border_right' height='{$LineHeight}' valign='middle' width='50%' align='center'>";
                $misc .= "<table border='0' cellpadding='0' cellspacing='0'>";
                $misc .= "<tr><td height='{$LineHeight}' class='report_text' valign='middle'>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}' class='report_text' valign='middle'>$ClassPosition</td>";
                $misc .= "</tr></table>";
                $misc .= "</td>";
                $misc .= "</tr>";
                $misc .= "</table>";
            }

            $x = $misc;
        }

        if($ReportType=="W")
        {
            if($StudentID) {
                $lu = new libuser($StudentID);
            }

            include_once($PATH_WRT_ROOT."includes/libclass.php");
            $lclass = new libclass();
            $temp = $lclass->getLevelArray();
            for($i=0;$i<sizeof($temp);$i++)
            {
                if($ClassLevelID==$temp[$i]['ClassLevelID'])
                {
                    $currentClass = $temp[$i]['LevelName'];
                    $current = $i;
                    break;
                }
            }

            if($current<sizeof($temp)-1)
            {
                // [2019-0708-1055-53206] Retrieve Next Class Level
                $nextClass = '';
                $curFormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode = 1);
                for($i=$current;$i<sizeof($temp);$i++)
                {
                    $tempFormNumber = $this->GET_FORM_NUMBER($temp[$i]['ClassLevelID'], $ByWebSAMSCode = 1);
                    if(($curFormNumber+1)==$tempFormNumber)
                    {
                        //$currentClass = $temp[$i]['LevelName'];
                        //$current = $i;
                        $nextClass = $temp[$i]['LevelName'];
                        break;
                    }
                }
                // $nextClass = $temp[$current+1]['LevelName'];

                $x .= "<br>";
                $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
                $x .= "<tr><td class='report_text'>";
                $x .= "<b>".$eReportCard['Template']['Promotion']  . " / ". $eReportCard['Template']['Retention'] .":</b> ";
                $x .= "<u>" . $eReportCard['Template']['ToBePromotedTo'] . $nextClass . " / " ;
                $x .=         $eReportCard['Template']['ToBePromotedTo'] . $nextClass . $eReportCard['Template']['ByConcession'] . " / " ;
                $x .=         $eReportCard['Template']['ToBeRetainedIn'] . $currentClass . "</u>";

                $x .= "</td></tr>";
                $x .= "</table>";
            }
        }
        return $x;
	}
	
	########### END Template Related
	
// 	function GET_FORM_NUMBER($ClassLevelID)
// 	{
// 		$ClassLevelName = $this->returnClassLevel($ClassLevelID);
// 		$FormNumber = substr($ClassLevelName,strlen($ClassLevelName)-1,1);
		
// 		return $FormNumber;
// 	}
	
	function initializeCertificatePdfObject(){
		// margin (in mm)
		$report_format = 'A4';
		$margin_top = 0;
		$margin_bottom = 0;
		$margin_left = 0;
		$margin_right = 0;
		$margin_header = 0;
		$margin_footer = 0;
		
		// Create mPDF object
		$this->pdfObj = new mPDF('', $report_format, 0, '', $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer);
		
		//pdf metadata setting
		$this->pdfObj->SetTitle('UCCKE certificates');
		$this->pdfObj->SetAuthor('BroadLearning Education (Asia) Ltd.');
		$this->pdfObj->SetCreator('BroadLearning Education (Asia) Ltd.');
		$this->pdfObj->SetSubject('UCCKE certificates');
		$this->pdfObj->SetKeywords('Certificates');
		
		// Chinese use mingliu 
		$this->pdfObj->backupSubsFont = array('mingliu');
		$this->pdfObj->useSubstitutions = true;
	}
	
	function generateCertificatePdfHtml($parReportId, $parAwardAssoAry, $parIssueDate) {
		global $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");
		$lreportcard_award = new libreportcard_award();
		
		// get academic year info
		// email "From Lau Sir (UCCKE)" Fri, Dec 18, 2015 at 9:52 AM
//		$academicYearInfoAry = $this->Get_Academic_Year_By_AcademicYearId($this->schoolYearID);
//		$academicYearName = $academicYearInfoAry['YearNameEN'];
		$schoolYear = $this->schoolYear;
		$academicYearName = $schoolYear.' - '.($schoolYear + 1);
		
		// get report info
		$reportInfoAry = $this->returnReportTemplateBasicInfo($parReportId);
		$termId = $reportInfoAry['Semester'];
				
		// get term info
		$termName = $this->returnSemesters($termId);
		$termName = strtolower($termName);
		$termName = str_replace('1st ', '1<sup>st</sup> ', $termName);
		$termName = str_replace('2nd ', '2<sup>nd</sup> ', $termName);
		$termName = str_replace('3rd ', '3<sup>rd</sup> ', $termName);
		$termName = str_replace('4th ', '4<sup>th</sup> ', $termName);
		$termName = str_replace('5th ', '5<sup>th</sup> ', $termName);
		
		
		// get award info
		$awardInfoAry = $lreportcard_award->Get_Report_Award_Info($parReportId);
		$awardIdAry = array_keys($parAwardAssoAry);
		
		// get students with award
		$awardStudentAry = $lreportcard_award->Get_Award_Generated_Student_Record($parReportId, $StudentIDArr='', $ReturnAsso=false, $AwardNameWithSubject=1, $awardIdAry);
		$awardStudentAssoAry = BuildMultiKeyAssoc($awardStudentAry, array('AwardID', 'SubjectID', 'StudentID'));
		unset($awardStudentAry);
		
		
		// build date display
		$issueDateTs = strtotime($parIssueDate);
		$issueDateDisplay = 'The '.$this->stringifyNumber(date('j', $issueDateTs)).' Day of '.date('F Y', $issueDateTs);
		
		
		// pdf general elements definition
		$normal_pagebreak = '<pagebreak />';
		$this->pdfObj->DefHTMLHeaderByName('NoHeader','');
		$this->pdfObj->SetHTMLHeaderByName('NoHeader');
		$this->pdfObj->WriteHTML($this->returnStyleSheetForPdf());
				
		// build pdf content 
		$isFirst = true;
		foreach ((array)$parAwardAssoAry as $_awardId => $_awardAry) {
			$_numOfSubject = count($_awardAry);
			
			list($_certTitleEn, $_certTitleCh) = $this->getCertificateTitle($_awardId);
			
			for ($i=0; $i<$_numOfSubject; $i++) {
				$__subjectId = $_awardAry[$i];
				
				foreach ((array)$awardStudentAssoAry[$_awardId][$__subjectId] as $___studentId => $___awardStudentAry) {
					$___awardName = $___awardStudentAry['AwardName'];
					$___awardCode = $___awardStudentAry['AwardCode'];
					$___studentInfoAry = $this->Get_Student_Class_ClassLevel_Info($___studentId);
					$___studentName = strtoupper($___studentInfoAry[0]['EnglishName']);
					$___className = 'S.'.$___studentInfoAry[0]['ClassName'];
					
					$_awardCertTitle = $this->getCertificateAwardTitle($_awardId, $___awardName);
					$___awardVerb = $this->getAwardVerb($___awardCode);
					$___awardPreposition = $this->getAwardPreposition($___awardCode);
					
					if ($isFirst) {
						$isFirst = false;
					}
					else {
						$this->pdfObj->WriteHTML($normal_pagebreak);
					}
					
					
					$___x = '';
					$___x .= '<div>';
						$___x .= '<table align="center" style="width:160mm; text-align:center;">';
							$___x .= '<tr><td style="height:87mm;">&nbsp;</td></tr>';
							$___x .= '<tr><td class="certTitleCh">'.$this->splitChineseWord($_certTitleCh).'</td></tr>';
							$___x .= '<tr><td style="height:6mm;">&nbsp;</td></tr>';
							$___x .= '<tr><td class="certTitleEn">'.strtoupper($_certTitleEn).'</td></tr>';
							$___x .= '<tr><td style="height:25mm;">&nbsp;</td></tr>';
							$___x .= '<tr><td class="content" style="word-spacing:1.5mm;">This is to certify that</td></tr>';
							$___x .= '<tr><td style="height:14mm;">&nbsp;</td></tr>';
							$___x .= '<tr><td class="studentName"><i>'.$___studentName.'</i></td></tr>';
							$___x .= '<tr><td style="height:12mm;">&nbsp;</td></tr>';
							$___x .= '<tr><td class="content" style="line-height:1.5; word-spacing:1.5mm;">of '.$___className.' '.$___awardVerb.'</td></tr>';
							$___x .= '<tr><td class="content" style="line-height:1.5; word-spacing:1.5mm;">'.$_awardCertTitle.'</td></tr>';
							
							switch ($___awardCode) {
								case 'conduct_award':
								case 'service_award':
								case 'student_of_arts':
								case 'student_of_sports':
								case 'student_of_the_form':
								//2018-0709-1307-46206
								case 'disciple_award':
									// no term display
									$___x .= '<tr><td class="content" style="line-height:1.5;">'.$___awardPreposition.' the school year of '.$academicYearName.'.</td></tr>';
								break;
								default:
									$___x .= '<tr><td class="content" style="line-height:1.5; word-spacing:1.5mm;">'.$___awardPreposition.' the '.$termName.' of the school year</td></tr>';
									$___x .= '<tr><td class="content" style="line-height:1.5; word-spacing:1.5mm;">of '.$academicYearName.'.</td></tr>';
							}
							
						$___x .= '</table>';
						$___x .= '<div style="height:34mm;">&nbsp;</div>';
						$___x .= '<table style="border:0px; width:100%; text-align:center;">';
							$___x .= '<tr>';
								$___x .= '<td style="width:105mm;">&nbsp;</td>';
								$___x .= '<td  class="signature" style="border-top:1px solid black; padding-top:2mm;">Principal</td>';
								$___x .= '<td style="width:35mm;">&nbsp;</td>';
							$___x .= '</tr>';
							$___x .= '<tr><td colspan="3" style="height:6mm;">&nbsp;</td></tr>';
							$___x .= '<tr>';
								$___x .= '<td>&nbsp;</td>';
								$___x .= '<td class="signature" style="word-spacing:0.5mm;">'.$issueDateDisplay.'</td>';
								$___x .= '<td>&nbsp;</td>';
							$___x .= '</tr>';
						$___x .= '</table>';
					$___x .= '</div>';
					$this->pdfObj->WriteHTML($___x);
					
				}
			}
		}
	}
	
	function getCertificateTitle($parAwardId) {
		global $PATH_WRT_ROOT, $eReportCard;
		
		include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");
		$lreportcard_award = new libreportcard_award();
		
		$awardInfoAry = $lreportcard_award->Get_Award_Info($parAwardId);
		$awardNameEn = $awardInfoAry['AwardNameEn'];
		$awardNameCh = $awardInfoAry['AwardNameCh'];
		$awardCode = $awardInfoAry['AwardCode'];
		
		$titleEn = '';
		$titleCh = '';
		switch ($awardCode) {
			case 'first_class_honour':
			case 'second_class_honour_1':
			case 'second_class_honour_2':
				$titleEn = $awardNameEn.' '.$eReportCard['Template']['StudentCertficateEn2'];
				$titleEn = str_replace('Honour', $eReportCard['Template']['StudentCertficateEn1'], $titleEn);
				$titleEn = str_replace('1', 'ONE', $titleEn);
				$titleEn = str_replace('2', 'TWO', $titleEn);
				$titleCh = $awardNameCh.$eReportCard['Template']['StudentCertficateCh'];
			break;
			case 'form_subject_prize_1':
			case 'form_subject_prize_2':
			case 'form_subject_prize_3':
			case 'class_subject_prize':
			case 'position_in_class_1':
			case 'position_in_class_2':
			case 'position_in_class_3':
			case 'academic_progress':
				$titleEn = $eReportCard['Template']['AcademicCertficateEn'];
				$titleCh = $eReportCard['Template']['AcademicCertficateCh'];
			break;
			case 'top_book_borrower':
			case 'conduct_progress_award':
			case 'pta_progress':
			case 'reading_scheme_silver':
			case 'disciple_award':
			case 'conduct_award':
			case 'service_award':
			case 'student_of_arts':
			case 'student_of_sports':
			case 'student_of_the_form':
				$titleEn = $eReportCard['Template']['StudentCertficateEn2'];
				$titleCh = $eReportCard['Template']['StudentCertficateCh'];
			break;
			case 'highest_distinction_in_academic':
				$titleEn = 'HIGHEST DISTINCTION CERTIFICATE';
				$titleCh = '最高榮譽狀';
			break;
			default:
				$titleEn = $awardNameEn;
				$titleCh = $awardNameCh;
		}
		
		return array($titleEn, $titleCh);
	}
	function getCertificateAwardTitle($parAwardId, $parAwardNameWithSubject='') {
		global $PATH_WRT_ROOT, $eReportCard;
		
		include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");
		$lreportcard_award = new libreportcard_award();
		
		$awardInfoAry = $lreportcard_award->Get_Award_Info($parAwardId);
		$awardNameEn = $awardInfoAry['AwardNameEn'];
		$awardCode = $awardInfoAry['AwardCode'];
		
		$awardTitle = '';
		switch ($awardCode) {
			case 'first_class_honour':
			case 'second_class_honour_1':
			case 'second_class_honour_2':
				$awardTitle = 'the Roll of '.$awardNameEn;
			break;
			case 'form_subject_prize_1':
			case 'form_subject_prize_2':
			case 'form_subject_prize_3':
				$awardTitle = str_replace(') in', ') <br>in', 'the '.$parAwardNameWithSubject);
			break;
			case 'class_subject_prize':
				$awardTitle = 'the '.$parAwardNameWithSubject;
			break;
			case 'top_book_borrower':
			case 'conduct_progress_award':
			case 'pta_progress':
			case 'reading_scheme_silver':
			case 'disciple_award':
			case 'conduct_award':
			case 'service_award':
			case 'highest_distinction_in_academic':
				$awardTitle = 'the '.$awardNameEn;
			break;
			default:
				$awardTitle = $awardNameEn;
		}
		
		return $awardTitle;
	}
	function getAwardVerb($awardCode) {
		$verb = '';
		switch ($awardCode) {
			case 'first_class_honour':
			case 'second_class_honour_1':
			case 'second_class_honour_2':
			case 'academic_progress':
			case 'student_of_arts':
			case 'student_of_sports':
			case 'student_of_the_form':
				$verb = 'has been elected';
			break;
			case 'highest_distinction_in_academic':
				$verb = 'has attained';
			break;
			default:
				$verb = 'has obtained';
		}
		
		return $verb;
	}
	
	function getAwardPreposition($awardCode) {
		$preposition = '';
		switch ($awardCode) {
			case 'top_book_borrower':
			case 'conduct_progress_award':
			case 'pta_progress':
			case 'reading_scheme_silver':
			case 'disciple_award':
			case 'conduct_award':
			case 'service_award':
			case 'highest_distinction_in_academic':
				$preposition = 'in';
			break;
			default:
				$preposition = 'for';
		}
		
		return $preposition;
	}
	
	function splitChineseWord($text) {
		return str_replace(' ', '&nbsp;&nbsp;&nbsp;', trim(chunk_split($text, 3, ' ')));
	}
	
	// reference: http://stackoverflow.com/questions/20425771/how-to-replace-1-with-first-2-with-second-3-with-third-etc
	function stringifyNumber($number) {
		$specialAry = array('zeroth','first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'ninth', 'tenth', 'eleventh', 'twelvth', 'thirteenth', 'fourteenth', 'fifteenth', 'sixteenth', 'seventeenth', 'eighteenth', 'nineteenth');
		$decaAry = array('twent', 'thirt', 'fourt', 'fift', 'sixt', 'sevent', 'eight', 'ninet');
		
		$returnNumber = '';
		if ($number < 20) {
			$returnNumber = $specialAry[$number]; 
		}
		else if ($number%10 == 0) {
			$returnNumber = $decaAry[floor($number/10)-2] . 'ieth';
		}
		else {
			// 20160422 email: Updates on Certificate Printing
			//$returnNumber = $decaAry[floor($number/10)-2] . 'y-' . $specialAry[$number%10]; 
			$returnNumber = $decaAry[floor($number/10)-2] . 'y ' . ucfirst($specialAry[$number%10]);
		}
		
		return ucfirst($returnNumber); 
	}
	
	function returnStyleSheetForPdf(){
		$style = '	<style>
					td { padding: 0; }
					.certTitleCh { font-family:"msjh"; font-size:22pt; font-weight:bold; }
					.certTitleEn { font-family:"georgia"; font-size:14pt; font-weight:bold; }
					.content { font-family:"mtcorsva"; font-size:16pt; }
					.studentName { font-family:"timesnewroman"; font-size:16pt; }
					.signature { font-family:"timesnewroman"; font-size:10pt; }
					</style>';
				
		return $style;
	}	
	
	function outputPdfFile($parFilename='') {
		$this->pdfObj->Output($parFilename, 'I');
	}
	########## END PDF Generation #############
}
?>