<?php
# Editing by ivan

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/wah_yan_kowloon.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom()
	{
		$this->libreportcard();
		$this->configFilesType = array("summary", "position", "schoolservice", "attendance", "remark");
		$this->CoreSubjectCodeArr = array('80', '165', '280', '265');	// Chi, Eng, Math, Liberal Studies
		$this->OthersSubjectCodeArr = array('186', '310');	// Ethics, PE
		$this->CheckComponentNAArr = array('280');	// Maths
		$this->SpecialCoreSubjectCodeArr = array('280', '265');   // Maths, Liberal Studies
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "-";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID)
	{
		global $eReportCard;
		global $PATH_WRT_ROOT, $intranet_root, $pageStyle, $_pageBreak;
		
		include_once($PATH_WRT_ROOT."includes/libreportcard2008_file.php");
		$lreportcard_file = new libreportcard_file();
		
		// Get Report Info
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting["ClassLevelID"];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		// Get Cover Page Image 
		$imageInfoAry = $lreportcard_file->returnFileInfoAry($ReportID, "CoverPage");
		$imagePath = $imageInfoAry[0]["FilePath"];
		
		$divMarginAttr = "padding-top:30px; padding-left:11px;";
		$imageWidthAttr = "max-height: 700px; max-width: 560px;";
		
		// Build Cover Page
		$x = "";
		$x .= "<tr><td style='text-align:center;'>";
			$x .= "<table width='100%' height='720' border='0' cellspacing='0' cellpadding='0' align='center'>";
				$x .= "<tr valign='bottom'>";
					$x .= "<td width='60%'>";
						if ($imagePath != "" && file_exists($intranet_root.$imagePath)) {
							$x .= "<div style='".$divMarginAttr."'><img src='".get_website().$imagePath."' style='".$imageWidthAttr."'/></div>"."\r\n";
						}
						else {
							$x .= "&nbsp;";
						}
					$x .= "</td>";
					$x .= "<td align='left'>".$StudentInfoTable."</td>";
				$x .= "</tr>";
			$x .= "</table>";
		$x .= "</td></tr>"."\r\n";
		$x .= "</table>";
		$x .= "</div>";
		
		// Open New Page for Report Content
		if($_pageBreak == "") {
			$x .= "<div style=\"page-break-after:always; height: 0;\">&nbsp;</div>";
		}
		$x .= "<div id='container".$pageStyle."' '".$_pageBreak."'>";
		$x .= "<table width='100%' border='0' cellpadding='02' cellspacing='0' valign='top'>";
		
		// Build Report Content
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
			$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
			//$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
			$TableTop .= $this->Get_Empty_Row('8px', $NumOfRow=1);
			
			# Fixed MS Table height for F4-F7
			$thisStyle = ($FormNumber < 4)? '' : "height='450px' valign='top'";
			$TableTop .= "<tr $thisStyle><td>".$MSTable."</td></tr>";
			
			$TableTop .= $this->Get_Empty_Row('10px', $NumOfRow=1);
			$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
			$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
			$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='700px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID, $StudentID='')
	{
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitleArr = explode(':_:', $ReportSetting['ReportTitle']);
			
			$IssueDate = $ReportSetting['Issued'];
			if ($IssueDate == '' || $IssueDate == '0000-00-00')
				$IssueDate = '&nbsp;';
			else
			{
				$IssueDate = date('d F Y', strtotime($IssueDate));
			}
			
			# Get Student Info
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$StudentNameEn = $StudentInfoArr[0]['EnglishName'];
			$StudentNameCh = $StudentInfoArr[0]['ChineseName'];
			$ClassName = $StudentInfoArr[0]['ClassName'];
			$ClassNumber = $StudentInfoArr[0]['ClassNumber'];			
			
			$x = '';
			$x .= '<table class="no_border_table">'."\n";
				$x .= '<tr>'."\n";
					# Empty
					$x .= '<td class="font_12px" style="width:3%;">&nbsp;</td>'."\n";
					$x .= '<td class="font_12px" style="width:3%;">&nbsp;</td>'."\n";
					$x .= '<td class="font_12px" style="width:1%;">&nbsp;</td>'."\n";
					$x .= '<td class="font_12px" style="width:20%;">&nbsp;</td>'."\n";
					
					# Report Title Line 1
					$x .= '<td class="font_16px text_bold" style="text-align:center;vertical-align:middle;white-space:nowrap;">'.$eReportCard['Template']['SchoolNameEn'].'</td>'."\n";
					
					# Empty
					$x .= '<td class="font_12px" style="width:15%;">&nbsp;</td>'."\n";
					$x .= '<td class="font_12px" style="width:3%;">&nbsp;</td>'."\n";
					$x .= '<td class="font_12px" style="width:1%;">&nbsp;</td>'."\n";
					$x .= '<td class="font_12px" style="width:8%;">&nbsp;</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					# Empty
					$x .= '<td class="font_12px">&nbsp;</td>'."\n";
					$x .= '<td class="font_12px">&nbsp;</td>'."\n";
					$x .= '<td class="font_12px">&nbsp;</td>'."\n";
					$x .= '<td class="font_12px">&nbsp;</td>'."\n";
					
					# Report Title Line 1
					$x .= '<td class="font_15px text_bold" style="text-align:center;vertical-align:middle;white-space:nowrap;">'.$eReportCard['Template']['SchoolNameCh'].'</td>'."\n";
					
					# Empty
					$x .= '<td class="font_12px">&nbsp;</td>'."\n";
					$x .= '<td class="font_12px">&nbsp;</td>'."\n";
					$x .= '<td class="font_12px">&nbsp;</td>'."\n";
					$x .= '<td class="font_12px">&nbsp;</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					# Class
					$x .= '<td class="font_12px" style="white-space:nowrap;">'.$eReportCard['Template']['StudentInfo']['ClassEn'].'</td>'."\n";
					$x .= '<td class="font_12px" style="white-space:nowrap;">'.$eReportCard['Template']['StudentInfo']['ClassCh'].'</td>'."\n";
					$x .= '<td class="font_12px"> : </td>'."\n";
					$x .= '<td class="font_12px" style="white-space:nowrap;">'.$ClassName.'</td>'."\n";
					
					# Report Title Line 1
					$x .= '<td class="font_14px" style="text-align:center;vertical-align:middle;white-space:nowrap;">'.$ReportTitleArr[0].'</td>'."\n";
					
					# Empty
					$x .= '<td class="font_12px">&nbsp;</td>'."\n";
					$x .= '<td class="font_12px">&nbsp;</td>'."\n";
					$x .= '<td class="font_12px">&nbsp;</td>'."\n";
					$x .= '<td class="font_12px">&nbsp;</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					# Name
					$x .= '<td class="font_12px" style="white-space:nowrap;">'.$eReportCard['Template']['StudentInfo']['NameEn'].'</td>'."\n";
					$x .= '<td class="font_12px" style="white-space:nowrap;">'.$eReportCard['Template']['StudentInfo']['NameCh'].'</td>'."\n";
					$x .= '<td class="font_12px"> : </td>'."\n";
					$x .= '<td class="font_12px" style="white-space:nowrap;">'.$StudentNameEn.' '.$StudentNameCh.'</td>'."\n";
					
					# Report Title Line 2
					$x .= '<td class="font_14px" style="text-align:center;vertical-align:middle;white-space:nowrap;">'.$ReportTitleArr[1].'</td>'."\n";
					
					# Issue Date
					$x .= '<td class="font_12px" style="text-align:right;white-space:nowrap;">'.$eReportCard['Template']['StudentInfo']['IssueDateEn'].'</td>'."\n";
					$x .= '<td class="font_12px" style="white-space:nowrap;">'.$eReportCard['Template']['StudentInfo']['IssueDateCh'].'</td>'."\n";
					$x .= '<td class="font_12px"> : </td>'."\n";
					$x .= '<td class="font_12px" style="white-space:nowrap;">'.$IssueDate.'</td>'."\n";
				$x .= '</tr>'."\n";
					
			$x .= '</table>'."\n";
		}
		return $x;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
//			# Retrieve Display Settings
//			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
//			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
//			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
//			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
//			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal = $StudentID == "" ? "XXX" : "";
			
			$data["AcademicYear"] = $this->GET_ACTIVE_YEAR_NAME();
			$data["Name"] = $defaultVal;
			$data["Class"] = $defaultVal;
			//$data['DateOfIssue'] = $ReportSetting['Issued'];
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				
				$lu = new libuser($StudentID);
				$data["Name"] = $lu->EnglishName;
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]["ClassName"];
				$data["Class"] = $thisClassName;
			}
			
			$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='left' class='student_info'>";
				$StudentInfoTable .= "<tr>";
					$StudentInfoTable .= "<td class='font_24px' colspan='3' align='center' style='font-family: \"Arial\", \"Lucida Console\";'>".$data["AcademicYear"]."</td>";
				$StudentInfoTable .= "</tr>";
				$StudentInfoTable .= "<tr>";
					$StudentInfoTable .= "<td class='font_16px' colspan='3'>&nbsp;</td>";
				$StudentInfoTable .= "</tr>";
				$StudentInfoTable .= "<tr>";
					$StudentInfoTable .= "<td class='font_18px' width='12%'><b>".$eReportCard["Template"]["StudentInfo"]["NameEn"]."</b>:&nbsp;&nbsp;&nbsp;</td>";
					$StudentInfoTable .= "<td class='font_18px' width='73%' style='border-bottom: 1px solid black;' align='center'>".$data["Name"]."</td>";
					$StudentInfoTable .= "<td width='15%'>&nbsp;</td>";
				$StudentInfoTable .= "</tr>";
				$StudentInfoTable .= "<tr>";
					$StudentInfoTable .= "<td class='font_18px'><b>".$eReportCard["Template"]["StudentInfo"]["ClassEn"]."</b>:&nbsp;&nbsp;&nbsp;</td>";
					$StudentInfoTable .= "<td class='font_18px' style='border-bottom: 1px solid black' align='center'>".$data["Class"]."</td>";
					$StudentInfoTable .= "<td>&nbsp;</td>";
				$StudentInfoTable .= "</tr>";
			$StudentInfoTable .= "</table>";
		}
		
//				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
//				$data['ClassNo'] = $thisClassNumber;
//				$data['StudentNo'] = $thisClassNumber;
//				
//				if (is_array($SettingStudentInfo))
//				{
//					if (!in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo) && ($thisClassNumber != ""))
//						$data['Class'] .= " (".$thisClassNumber.")";
//				}
//				else
//				{
//					if ($thisClassNumber != "")
//						$data['Class'] .= " (".$thisClassNumber.")";
//				}
//				
//				$data['DateOfBirth'] = $lu->DateOfBirth;
//				$data['Gender'] = $lu->Gender;
//				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
//				$data['StudentAdmNo'] = $data['STRN'];
//				
//				$lclass = new libclass();
//				$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
//				foreach($ClassTeacherAry as $key=>$val)
//				{
//					$CTeacher[] = $val['CTeacher'];
//				}
//				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
//			}
//			
//			if(!empty($SettingStudentInfo))
//			{
//				$count = 0;
//				$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
//				for($i=0; $i<sizeof($StudentTitleArray); $i++)
//				{
//					$SettingID = trim($StudentTitleArray[$i]);
//					if(in_array($SettingID, $SettingStudentInfo)===true)
//					{
//						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
//						if($count%$StudentInfoTableCol==0) {
//							$StudentInfoTable .= "<tr>";
//						}
//						
//						$colspan = $SettingID=="Name" ? " colspan='10' " : "";
//						$StudentInfoTable .= "<td class='tabletext' $colspan width='20%' valign='top' height='{$LineHeight}'>".$Title." : ";
//						$StudentInfoTable .= ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
//							
//						if(($count+1)%$StudentInfoTableCol==0) {
//							$StudentInfoTable .= "</tr>";
//						} else {
//							if($SettingID=="Name")
//							{
//								$count=-1;
//								$StudentInfoTable .= "</tr>";
//							}
//						}
//						$count++;
//					}
//				}
//				$StudentInfoTable .= "</table>";
//			}
		
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		//$FormNumber = 6;
		
		$x = '';
		$x .= '<table class="border_table" cellpadding="1" cellspacing="0">'."\n";
			### Title Row
			$x .= '<tr>'."\n";
				# Key Learning Areas (F1-3) / Categories (F4-7)
				$thisTitleEn = ($FormNumber <= 3)? $eReportCard['Template']['KeyLearningAreasEn'] : $eReportCard['Template']['CategoriesEn'];
				$thisTitleCh = ($FormNumber <= 3)? $eReportCard['Template']['KeyLearningAreasCh'] : $eReportCard['Template']['CategoriesCh'];
				$thisWidth = ($FormNumber <= 3)? '10%' : '7%';
				$thisColSpan = 1;
				$x .= $this->Get_MSTable_Title_Cell($thisTitleEn, $thisTitleCh, $thisWidth, $thisColSpan, 1);
				
				# Subjects and Components
				$thisWidth = ($FormNumber <= 3)? '24%' : '28%';
				$thisColSpan = 2;
				$x .= $this->Get_MSTable_Title_Cell($eReportCard['Template']['SubjectsAndComponentsEn'], $eReportCard['Template']['SubjectsAndComponentsCh'], $thisWidth, $thisColSpan);
								
				# Weightings (F1-3 only)
				if ($FormNumber <= 3)
				{
					$thisTitle = '';
					$thisTitle .= '<span class="font_11px">'.$eReportCard['Template']['WeightingsEn'].'</span><span class="font_9px"> '.$eReportCard['Template']['WeightingsCh'].'</span>';
					$thisTitle .= '<br />';
					$thisTitle .= '<span class="font_11px">'.$eReportCard['Template']['CW|ExamEn'].'</span>';
					$x .= '<td colspan="3" class="border_left text_bold" style="text-align:center;width:9%;">'.$thisTitle.'</td>'."\n";
				}
				
				# Coursework (CW)
				//$thisWidth = ($FormNumber <= 3)? '10%' : '11%';
				$thisWidth = ($FormNumber <= 3)? '10%' : '14%';
				$thisColSpan = 1;
				// X86217 - point 5 - F6 show "CA" instead of "CW"
				if ($FormNumber == 6) {
					$x .= $this->Get_MSTable_Title_Cell($eReportCard['Template']['ContinueAssessmentEn'], $eReportCard['Template']['ContinueAssessmentCh'], $thisWidth, $thisColSpan);
				}
				else {
					$x .= $this->Get_MSTable_Title_Cell($eReportCard['Template']['CourseworkEn'], $eReportCard['Template']['CourseworkCh'], $thisWidth, $thisColSpan);
				}
				
				# Examination
				$thisWidth = ($FormNumber <= 3)? '8%' : '18%';
				$thisColSpan = 1;
				$x .= $this->Get_MSTable_Title_Cell($eReportCard['Template']['ExaminationEn'], $eReportCard['Template']['ExaminationCh'], $thisWidth, $thisColSpan);
				
				# Overall Results
//				$thisWidth = ($FormNumber <= 3)? '9%' : '10%';
//				$thisColSpan = 1;
//				$x .= $this->Get_MSTable_Title_Cell($eReportCard['Template']['OverallResultsEn'], $eReportCard['Template']['OverallResultsCh'], $thisWidth, $thisColSpan);
				// X86217 - point 3.2 - hide overall column for senior form template
				if ($FormNumber <= 3) {
					$thisWidth = '9%';
					$thisColSpan = 1;
					$x .= $this->Get_MSTable_Title_Cell($eReportCard['Template']['OverallResultsEn'], $eReportCard['Template']['OverallResultsCh'], $thisWidth, $thisColSpan);
				}
				
				# Subject Teachers
				$thisWidth = ($FormNumber <= 3)? '10%' : '10%';
				$thisColSpan = 1;
				$x .= $this->Get_MSTable_Title_Cell($eReportCard['Template']['SubjectTeachersEn'], $eReportCard['Template']['SubjectTeachersCh'], $thisWidth, $thisColSpan);
				
				# Comments
				$thisWidth = ($FormNumber <= 3)? '18%' : '23%';
				$thisColSpan = 1;
				$x .= $this->Get_MSTable_Title_Cell($eReportCard['Template']['CommentsEn'], $eReportCard['Template']['CommentsCh'], $thisWidth, $thisColSpan);				
			$x .= '</tr>'."\n";
			
			if ($FormNumber <= 3)
				$x .= $this->Get_Lower_Form_Subject_Rows($ReportID, $StudentID);
			else
				$x .= $this->Get_Higher_Form_Subject_Rows($ReportID, $StudentID);
			
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_MSTable_Title_Cell($TitleEn, $TitleCh, $Width, $Colspan, $isFirst=0)
	{
		if ($isFirst==1)
		{
			$border_left = '';
			$title_class = 'mstable_first_title_en';
			$title_font = 'font_9px';
		}
		else
		{
			$border_left = 'border_left';
			$title_class = '';
			$title_font = 'font_11px';
		}
			
		$x = '';
		$x .= '<td colspan="'.$Colspan.'" class="'.$border_left.' text_bold" style="text-align:center;width:'.$Width.'">'."\n";
			$x .= '<span class="'.$title_font.' '.$title_class.'">'.$TitleEn.'</span>'."\n";
			$x .= '<br />'."\n";
			$x .= '<span class="font_9px">'.$TitleCh.'</span>'."\n";
		$x .= '</td>'."\n";
		
		return $x;
	}
	
	function Get_Lower_Form_Subject_Rows($ReportID, $StudentID)
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		
		### Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		
		### Get Report Columns
		$ReportColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
		$numOfReportColumn = count($ReportColumnInfoArr);
		$firstReportColumnId = $ReportColumnInfoArr[0]['ReportColumnID'];
		
		### Get Grading Schemes of all Subjects
		$GradingSchemeArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID='', $withGrandResult=0, $returnAsso=1, $ReportID);
		
		### Get Weight Info of all Subjects
		$SubjectWeightInfoArr = $this->returnReportTemplateSubjectWeightData($ReportID);
		$numOfSubjectWeight = count($SubjectWeightInfoArr);
		$SubjectWeightAssoArr = array();
		for ($i=0; $i<$numOfSubjectWeight; $i++)
		{
			$thisSubjectID = $SubjectWeightInfoArr[$i]['SubjectID'];
			
			if ($thisSubjectID == '')
				continue;
			
			$thisReportColumnID = ($SubjectWeightInfoArr[$i]['ReportColumnID']=='')? 0 : $SubjectWeightInfoArr[$i]['ReportColumnID'];
			$thisWeight = $SubjectWeightInfoArr[$i]['Weight'];
			
			$SubjectWeightAssoArr[$thisSubjectID][$thisReportColumnID] = $thisWeight;
		}
		
		
		### Get Student Marks
		// $MarksArr[$SubjectID][$ReportColumnID][Key] = Data
		$MarksArr = $this->getMarks($ReportID, $StudentID, '', 0, 1);
		$extraInfoAssoAry = $this->GET_EXTRA_SUBJECT_INFO($StudentID, '', $ReportID);
		
		### Get Student Subject Comment
		$SubjectTeacherCommentArr = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		
		### Reorder the Subject Display Order
		$OriginalSubjectSequenceArr = $this->returnSubjectwOrder($ClassLevelID, 0, '', '', 'Desc', 0, $ReportID);
		$NewSubjectSequenceArr = array();
		$SubjectObjArr = array();
		foreach((array)$OriginalSubjectSequenceArr as $thisSubjectID => $thisSubjectInfoArr)
		{
			$SubjectObjArr[$thisSubjectID] = new subject($thisSubjectID);
			$thisSubjectObj = $SubjectObjArr[$thisSubjectID];
			$thisLearningCategoryID = $thisSubjectObj->LearningCategoryID;
			
			$numOfCmpSubject = count($thisSubjectInfoArr) - 1;
			if ($numOfCmpSubject == 0)
			{
				// no component
				$NewSubjectSequenceArr[$thisLearningCategoryID][$thisSubjectID] = $OriginalSubjectSequenceArr[$thisSubjectID];
			}
			else
			{
				// parent subject
				$CmpCount = 0;
				$IncludedParentSubjectFlag = false;
				foreach((array)$thisSubjectInfoArr as $thisCmpSubjectID => $thisCmpSubjectInfoArr)
				{
					if ($thisCmpSubjectID == 0)
						continue;
					
					$CmpCount++;
					
					// Place the parent subject before the first Grade Input Subject
					if ($IncludedParentSubjectFlag == false && $GradingSchemeArr[$thisCmpSubjectID]['ScaleInput'] == 'G')
					{
						$NewSubjectSequenceArr[$thisLearningCategoryID][$thisSubjectID][0] = $OriginalSubjectSequenceArr[$thisSubjectID][0];
						$IncludedParentSubjectFlag = true;
					}
					
					// Add the componenet subject					
					$NewSubjectSequenceArr[$thisLearningCategoryID][$thisSubjectID][$thisCmpSubjectID] = $OriginalSubjectSequenceArr[$thisSubjectID][$thisCmpSubjectID];
					
					// Place the parent subject if there are no Grade input Subject
					if ($IncludedParentSubjectFlag == false && $CmpCount == $numOfCmpSubject)
					{
						$NewSubjectSequenceArr[$thisLearningCategoryID][$thisSubjectID][0] = $OriginalSubjectSequenceArr[$thisSubjectID][0];
						$IncludedParentSubjectFlag = true;
					}
				}
			}
		}
		
		
		### Display Subject Score
		$LastLearningCategoryID = '';
		$ScienceRow = 3;
		$x = '';
		
		foreach((array)$NewSubjectSequenceArr as $thisLearningCategoryID => $thisSubjectArr)
		{
			$numOfSubjectInLC = count($thisSubjectArr);
			$LC_SubjectCount = 0;
			
			foreach ((array)$thisSubjectArr as $thisParentSubjectID => $thisCmpSubjectArr)
			{
				$numOfCmpSubject = count($thisCmpSubjectArr) - 1;
				$thisIsParentSubject = ($numOfCmpSubject==0)? false : true;
				
				# Learning Category Display
				if ($thisIsParentSubject || $LastLearningCategoryID == '' || $thisLearningCategoryID != $LastLearningCategoryID)
				{
					$thisLearningCategoryObj = new Learning_Category($thisLearningCategoryID);
					$thisLC_Code = $thisLearningCategoryObj->Code;
					$thisLC_NameEn = $thisLearningCategoryObj->NameEng;
					$thisLC_NameCh = $thisLearningCategoryObj->NameChi;
					
					### Check Row Span
					if (strtoupper($thisLC_Code) == 'SCI')
					{
						$thisRowSpan = $ScienceRow;
					}
					else if ($thisIsParentSubject)
					{
						$thisRowSpan = 0;
						
						// count number of Component (which is not all N.A. marks)
						foreach ((array)$thisCmpSubjectArr as $thisCmpSubjectID => $thisCmpSubjectName)
						{
							$isAllNA = true;
							
							$thisIsComponentSubject = ($thisCmpSubjectID == 0)? false : true;
							$thisSubjectID = ($thisIsComponentSubject)? $thisCmpSubjectID : $thisParentSubjectID;
							$thisScaleDisplay = $GradingSchemeArr[$thisSubjectID]['ScaleDisplay'];
							
							for ($i=0; $i<$numOfReportColumn; $i++)
							{
								$thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];
								
								$thisMSGrade = $MarksArr[$thisSubjectID][$thisReportColumnID]['Grade'];
								$thisMSMark = $MarksArr[$thisSubjectID][$thisReportColumnID]['Mark'];
								
								$thisGrade = ($thisScaleDisplay=="G" || $thisScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($thisScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";	
								
								# for preview purpose
								if(!$StudentID)
								{
									$thisMark 	= $thisScaleDisplay=="M" ? "S" : "";
									$thisGrade 	= $thisScaleDisplay=="G" ? "G" : "";
								}
								
								$thisMark = ($thisScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
								
								if ($thisMark != "N.A.")
									$isAllNA = false;
							}
							
							if ($StudentID=='' || $isAllNA == false)
								$thisRowSpan++;
						}
					}
					else
					{
						$thisRowSpan = 0;
						
						// count number of Subject in the Learning Category (which is not all N.A. marks)
						foreach ((array)$thisSubjectArr as $thisSubjectID => $thisCmpSubjectArr)
						{
							$isAllNA = true;
							$thisScaleDisplay = $GradingSchemeArr[$thisSubjectID]['ScaleDisplay'];
							
							for ($i=0; $i<$numOfReportColumn; $i++)
							{
								$thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];
								
								$thisMSGrade = $MarksArr[$thisSubjectID][$thisReportColumnID]['Grade'];
								$thisMSMark = $MarksArr[$thisSubjectID][$thisReportColumnID]['Mark'];
								
								$thisGrade = ($thisScaleDisplay=="G" || $thisScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($thisScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";	
								
								# for preview purpose
								if(!$StudentID)
								{
									$thisMark 	= $thisScaleDisplay=="M" ? "S" : "";
									$thisGrade 	= $thisScaleDisplay=="G" ? "G" : "";
								}
								
								$thisMark = ($thisScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
								
								if ($thisMark != "N.A." && $thisMark != "")
									$isAllNA = false;
							}
							
							if ($StudentID=='' || $isAllNA == false)
								$thisRowSpan++;
						}
					}
					
					$x .= '<tr>'."\n";
						$x .= '<td class="border_top" rowspan="'.$thisRowSpan.'" style="vertical-align:middle;text-align:center;">'."\n";
							$x .= '<span class="font_11px">'.$thisLC_NameEn.'</span>'."\n";
							$x .= '<br />'."\n";
							$x .= '<span class="font_9px">'.$thisLC_NameCh.'</span>'."\n";
						$x .= '</td>'."\n";
				}
				
				
				# Subject Display
				$thisCmpSubjectCount = 0;
				foreach ((array)$thisCmpSubjectArr as $thisCmpSubjectID => $thisCmpSubjectName)
				{
					$isAllNA = true;
					$thisSubjectHTML = '';
					
					$thisIsComponentSubject = ($thisCmpSubjectID == 0)? false : true;
					$thisSubjectID = ($thisIsComponentSubject)? $thisCmpSubjectID : $thisParentSubjectID;
					$thisSubjectNameEn = $this->GET_SUBJECT_NAME_LANG($thisSubjectID, 'en');
					$thisSubjectNameCh = $this->GET_SUBJECT_NAME_LANG($thisSubjectID, 'b5');
					$thisScaleInput = $GradingSchemeArr[$thisSubjectID]['ScaleInput'];
					$thisScaleDisplay = $GradingSchemeArr[$thisSubjectID]['ScaleDisplay'];
					
					### Show "SCORE IN XXX" for Chinese, English and Mathematics
					if (!isset($SubjectObjArr[$thisSubjectID]))
						$SubjectObjArr[$thisSubjectID] = new subject($thisSubjectID);
					$thisSubjectObj = $SubjectObjArr[$thisSubjectID];
					$thisSubjectCode = ($thisIsComponentSubject)? $thisSubjectObj->CMP_CODEID : $thisSubjectObj->CODEID;
					
					if ($eReportCard['Template']['HardCodedSubjectNameEn'][$thisSubjectCode] != '')
						$thisSubjectNameEn = $eReportCard['Template']['HardCodedSubjectNameEn'][$thisSubjectCode];
					if ($eReportCard['Template']['HardCodedSubjectNameCh'][$thisSubjectCode] != '')
						$thisSubjectNameCh = $eReportCard['Template']['HardCodedSubjectNameCh'][$thisSubjectCode];
						
						
					# Normal Subject => First Subject of the Learning Category need border_top
					# Parent Subject => First Component Subject of the Parent Subject need border_top
					if (($thisIsComponentSubject == false && $LC_SubjectCount == 0) || ($thisIsComponentSubject && $thisCmpSubjectCount==0))
						$thisBorderTop = 'border_top';
					else if ($LC_SubjectCount > 0 && $thisIsParentSubject == false && $thisIsComponentSubject == false)
						$thisBorderTop = 'border_top_lite';
					else
						$thisBorderTop = '';
						
					$thisCmpWeightDisplay = '';
					if ($thisIsComponentSubject && $thisScaleInput == 'M')
					{
						$thisCmpWeight = $SubjectWeightAssoArr[$thisSubjectID][0] * 100;
						$thisCmpWeightDisplay = ' ('.$thisCmpWeight.'%)';
					}
					
					
					if ($thisCmpSubjectCount > 0)
						$thisSubjectHTML .= '<tr>'."\n";
						
						
						$thisBoldClass = ($thisIsComponentSubject==false && $thisScaleDisplay=='M')? 'text_bold' : '';
						
						# Subject English Name
						$thisSubjectHTML .= '<td class="font_11px border_left '.$thisBorderTop.' '.$thisBoldClass.'" style="vertical-align:top;">'.$thisSubjectNameEn.$thisCmpWeightDisplay.'</td>'."\n";
						
						# Subject Chinese Name
						$thisSubjectHTML .= '<td class="font_9px '.$thisBorderTop.' '.$thisBoldClass.'" style="vertical-align:top;">'.$thisSubjectNameCh.'</td>'."\n";
						
						# Subject Columns Weight
						$thisWeight = array();
						$thisWeight[0] = '&nbsp;';
						$thisWeight[1] = '&nbsp;';
						if (!$thisIsParentSubject || $thisIsComponentSubject)
						{
							for ($i=0; $i<$numOfReportColumn; $i++)
							{
								$thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];
								$thisWeight[$i] = $SubjectWeightAssoArr[$thisSubjectID][$thisReportColumnID] * 100;
							}
						}
						$thisSubjectHTML .= '<td class="font_11px border_left '.$thisBorderTop.'" style="width:4%;text-align:right;vertical-align:top;">'.$thisWeight[0].'</td>'."\n";
						$thisSubjectHTML .= '<td class="font_11px '.$thisBorderTop.'" style="width:2%;text-align:center;vertical-align:top;">|</td>'."\n";
						$thisSubjectHTML .= '<td class="font_11px '.$thisBorderTop.'" style="width:4%;vertical-align:top;">'.$thisWeight[1].'</td>'."\n";
						
						# Subject Column Mark
						if ($StudentID == '')
						{
							### preview
							$isAllNA = false;
							
							# Columns Empty Cells & Overall Empty Cell
							for ($i=0; $i<$numOfReportColumn+1; $i++)
								$thisSubjectHTML .= '<td class="font_11px border_left '.$thisBorderTop.'" style="vertical-align:top;text-align:center;">'.$this->EmptySymbol.'</td>'."\n";
						}
						else
						{
							foreach ((array)$MarksArr[$thisSubjectID] as $thisReportColumnID => $thisMarkInfoArr)
							{
								$thisSubjectColumnWeight = $SubjectWeightAssoArr[$thisSubjectID][$thisReportColumnID];
								
								if ($thisIsParentSubject && !$thisIsComponentSubject && $thisReportColumnID != 0)
								{
									// Assessments of the Parent Subject => Show ''
									$thisSubjectHTML .= '<td class="font_11px border_left '.$thisBorderTop.'" style="vertical-align:top;text-align:center;">&nbsp;</td>'."\n";
								}
								else if ($thisSubjectColumnWeight == 0 && $thisReportColumnID != 0)
								{
									// Weight = 0 Subject => Show '-'
									$thisSubjectHTML .= '<td class="font_11px border_left '.$thisBorderTop.'" style="vertical-align:top;text-align:center;">'.$this->EmptySymbol.'</td>'."\n";
								}
								else
								{
									$thisMSGrade = $MarksArr[$thisSubjectID][$thisReportColumnID]['Grade'];
									$thisMSMark = $MarksArr[$thisSubjectID][$thisReportColumnID]['Mark'];
									
									$thisGrade = ($thisScaleDisplay=="G" || $thisScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
									$thisMark = ($thisScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";	
									
									# for preview purpose
									if(!$StudentID)
									{
										$thisMark 	= $thisScaleDisplay=="M" ? "S" : "";
										$thisGrade 	= $thisScaleDisplay=="G" ? "G" : "";
									}
									
									$thisMark = ($thisScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
									
									if ($thisMark != "N.A.")
										$isAllNA = false;
										
									# check special case
									list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisSubjectID, $thisMark, $thisMSGrade);
									
									if($needStyle)
									{
										if ($thisReportColumnID != 0 && $thisSubjectColumnWeight > 0 && $thisScaleDisplay=="M")
											$thisMarkTemp = ($UseWeightedMark && $thisSubjectColumnWeight != 0) ? $thisMark / $thisSubjectColumnWeight : $thisMark;
										else
											$thisMarkTemp = $thisMark;
										
										$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $thisSubjectID, $thisMarkTemp);
									}
									else
										$thisMarkDisplay = $thisMark;
									
									// X86217 - point 8 - if first column has manual input grade => show manual input grade
									if ($firstReportColumnId==$thisReportColumnID && !empty($extraInfoAssoAry[$StudentID][$thisSubjectID]['Info'])) {
										$thisMarkDisplay = $extraInfoAssoAry[$StudentID][$thisSubjectID]['Info'];
									}
									
									$thisMarkDisplay = ($thisMarkDisplay == '')? $this->EmptySymbol : $thisMarkDisplay;
										
									$thisBoldClass = ($thisIsComponentSubject==false && $thisScaleDisplay=='M' && $thisReportColumnID==0)? 'text_bold' : '';
									$thisSubjectHTML .= '<td class="font_11px border_left '.$thisBorderTop.' '.$thisBoldClass.'" style="vertical-align:top;text-align:center;">'.$thisMarkDisplay.'</td>'."\n";
								}
							}
						}
						
						# Subject Teachers
						$SubjectTeacherInfoArr = $this->returnSubjectTeacher('', $thisSubjectID, $ReportID, $StudentID, $FullTeacherInfo=1);
						$numOfSubjectTeacher = count($SubjectTeacherInfoArr);
						
						$thisSubjectTeacherArr = array();
						for ($i=0; $i<$numOfSubjectTeacher; $i++)
						{
							$thisTitle = Get_Lang_Selection($SubjectTeacherInfoArr[$i]['TitleChinese'], $SubjectTeacherInfoArr[$i]['TitleEnglish']);
							$thisNickname = $SubjectTeacherInfoArr[$i]['NickName'];
							
							$thisSubjectTeacherArr[] = $thisTitle.' '.$thisNickname;
						}
						$thisSubjectTeacherDisplay = implode('<br />', $thisSubjectTeacherArr);
						$thisSubjectTeacherDisplay = ($thisSubjectTeacherDisplay == '')? '&nbsp;' : $thisSubjectTeacherDisplay;
						
						// X86217 - point 1 - hide subject teacher name for Chinese and English row
						if ($thisSubjectCode=='80' || $thisSubjectCode=='165') {
							$thisSubjectTeacherDisplay = '&nbsp;';
						}
						
						$thisSubjectHTML .= '<td class="font_11px border_left '.$thisBorderTop.'" style="vertical-align:top;text-align:left;padding-left:5px;">'.$thisSubjectTeacherDisplay.'</td>'."\n";
						
						
						# Subject Teacher Comment
						if ($thisCmpSubjectCount == 0)
						{
							$thisComment = $SubjectTeacherCommentArr[$thisParentSubjectID];
							$thisRowSpan = $numOfCmpSubject + 1;
							if ($thisComment == '')
							{
								$thisAlign = 'center';
								$thisDisplay = $this->EmptySymbol;
							}
							else
							{
								$thisAlign = 'left';
								$thisDisplay = $thisComment;
							}
							$thisSubjectHTML .= '<td rowspan="'.$thisRowSpan.'" class="font_11px border_left '.$thisBorderTop.'" style="vertical-align:middle;text-align:'.$thisAlign.';padding-left:5px;">'.$thisDisplay.'</td>'."\n";
						}
					$thisSubjectHTML .= '</tr>'."\n";
					
					if ($isAllNA == false)
					{
						$LC_SubjectCount++;
						$thisCmpSubjectCount++;
						$x .= $thisSubjectHTML;
					}	
				}
				
				$LastLearningCategoryID = $thisLearningCategoryID;
				
			}	// End of each Subject Display
			
			if (strtoupper($thisLC_Code) == 'SCI')
			{
				### Field Empty Rows
				for ($i=$LC_SubjectCount; $i<$ScienceRow; $i++)
				{
					$thisBorderTop = 'border_top_lite';
						
					if ($thisCmpSubjectCount > 0)
						$x .= '<tr>'."\n";
					
						# Subject English Name
						$x .= '<td class="font_11px border_left '.$thisBorderTop.'" style="vertical-align:top;">&nbsp;</td>'."\n";
						
						# Subject Chinese Name
						$x .= '<td class="font_9px '.$thisBorderTop.'" style="vertical-align:top;">&nbsp;</td>'."\n";
						
						# Subject Columns Weight
						$thisWeight = array();
						$thisWeight[0] = '&nbsp;';
						$thisWeight[1] = '&nbsp;';
						$x .= '<td class="font_11px border_left '.$thisBorderTop.'" style="text-align:right;vertical-align:top;">'.$thisWeight[0].'</td>'."\n";
						$x .= '<td class="font_11px '.$thisBorderTop.'" style="text-align:center;vertical-align:top;">|</td>'."\n";
						$x .= '<td class="font_11px '.$thisBorderTop.'" style="vertical-align:top;">'.$thisWeight[1].'</td>'."\n";
						
						# Marks Columns
						for ($j=0; $j<$numOfReportColumn+1; $j++)
							$x .= '<td class="font_11px border_left '.$thisBorderTop.'" style="vertical-align:top;">&nbsp;</td>'."\n";
							
						# Subject Teachers
						$x .= '<td class="font_11px border_left '.$thisBorderTop.'" style="vertical-align:top;text-align:left;padding-left:5px;">&nbsp;</td>'."\n";
						
						# Subject Teacher Comment
						$thisAlign = 'center';
						$thisDisplay = $this->EmptySymbol;
						$x .= '<td class="font_11px border_left '.$thisBorderTop.'" style="vertical-align:middle;text-align:'.$thisAlign.';padding-left:5px;">'.$thisDisplay.'</td>'."\n";
						
					$x .= '</tr>'."\n";
				}
			}
		}
		
		return $x;
	}
	
	
	function Get_Higher_Form_Subject_Rows($ReportID, $StudentID)
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		//$FormNumber = 1;
		$SemesterSeq = $this->Get_Semester_Seq_Number($SemID);
		
		### Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		
		### Get Report Columns
		$ReportColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
		$numOfReportColumn = count($ReportColumnInfoArr);
		$firstReportColumnId = $ReportColumnInfoArr[0]['ReportColumnID'];
		$secondReportColumnId = $ReportColumnInfoArr[1]['ReportColumnID'];
		
		### Get Grading Schemes of all Subjects
		$GradingSchemeArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID='', $withGrandResult=0, $returnAsso=1, $ReportID);
		
		### Get Weight Info of all Subjects
		$SubjectWeightInfoArr = $this->returnReportTemplateSubjectWeightData($ReportID);
		$numOfSubjectWeight = count($SubjectWeightInfoArr);
		$SubjectWeightAssoArr = array();
		for ($i=0; $i<$numOfSubjectWeight; $i++)
		{
			$thisSubjectID = $SubjectWeightInfoArr[$i]['SubjectID'];
			
			if ($thisSubjectID == '')
				continue;
			
			$thisReportColumnID = ($SubjectWeightInfoArr[$i]['ReportColumnID']=='')? 0 : $SubjectWeightInfoArr[$i]['ReportColumnID'];
			$thisWeight = $SubjectWeightInfoArr[$i]['Weight'];
			
			$SubjectWeightAssoArr[$thisSubjectID][$thisReportColumnID] = $thisWeight;
		}
		
		### Get Student Marks
		// $MarksArr[$SubjectID][$ReportColumnID][Key] = Data
		$MarksArr = $this->getMarks($ReportID, $StudentID, '', 0, 1);
		$extraInfoAssoAry = $this->GET_EXTRA_SUBJECT_INFO($StudentID, '', $ReportID);
		
		### Get Student Subject Comment
		$SubjectTeacherCommentArr = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		
		### Reorder the Subject Display Order
		$OriginalSubjectSequenceArr = $this->returnSubjectwOrder($ClassLevelID, 0, '', '', 'Desc', 0, $ReportID);
		
		### Get 1st term report data
		$needToGetfirstReportResult = $FormNumber == 6 && $SemesterSeq == 2;
		if ($needToGetfirstReportResult)
		{
		    ### Get 1st term report
		    $firstTermReportSetting = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, '', $isMainReport=1, array($SemID, 'F'));
		    $firstTermReportID = $firstTermReportSetting['ReportID'];
		    
		    ### Get 1st term report data
		    $firstTermGradingSchemeArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID='', $withGrandResult=0, $returnAsso=1, $firstTermReportID);
		    $firstTermMarksArr = $this->getMarks($firstTermReportID, $StudentID, '', 0, 1);
		    //$firstTermExtraInfoAssoAry = $this->GET_EXTRA_SUBJECT_INFO($StudentID, '', $firstTermReportID);
		    
		    ### Set 1st report column
		    foreach($MarksArr as $thisSubjectID => $thisSubjectMarksArr) {
		        $MarksArr[$thisSubjectID][$firstReportColumnId] = array();
		        if(isset($firstTermMarksArr[$thisSubjectID][0])) {
		            $MarksArr[$thisSubjectID][$firstReportColumnId] = $firstTermMarksArr[$thisSubjectID][0];
		        }
		    }
		}
        
		// Initilize the array so as to display the Category in order Core > Elective > Others
		$SubjectDisplayArr['Core'] = array();
		$SubjectDisplayArr['Elective'] = array();
		$SubjectDisplayArr['Others'] = array();
		
		// array for Mathematics display handling
		$MathHandlingArr = array();
		
		$SubjectObjArr = array();
		foreach((array)$OriginalSubjectSequenceArr as $thisSubjectID => $thisSubjectInfoArr)
		{
			$SubjectObjArr[$thisSubjectID] = new subject($thisSubjectID);
			$thisSubjectObj = $SubjectObjArr[$thisSubjectID];
			$thisSubjectCode = $thisSubjectObj->CODEID;
			
			if (in_array($thisSubjectCode, $this->CoreSubjectCodeArr)) {
				$SubjectDisplayArr['Core'][$thisSubjectID] = $thisSubjectInfoArr;
			}
			else if (in_array($thisSubjectCode, $this->OthersSubjectCodeArr)) {
				$SubjectDisplayArr['Others'][$thisSubjectID] = $thisSubjectInfoArr;
			}
			else {
				$SubjectDisplayArr['Elective'][$thisSubjectID] = $thisSubjectInfoArr;
			}
			
			// Point 4 - Mathematics & Extended Part Module
			if($thisSubjectCode=="280" || $thisSubjectCode=="282") {
				$MathHandlingArr[$thisSubjectCode] = $thisSubjectID;
			}
		}
			
		// Add components of Extended Part Module to Mathematics
		$MathSubjectID = $MathHandlingArr["280"];
		$MathExtendedPartID = $MathHandlingArr["282"];
		$MathExtendedPart = $SubjectDisplayArr['Elective'][$MathExtendedPartID];
		if(!empty($MathExtendedPart)){
			foreach((array)$MathExtendedPart as $extendSubjectID => $extendSubjectName){
				if($extendSubjectID==0){
					continue;
				}
				$SubjectDisplayArr['Core'][$MathSubjectID][$extendSubjectID] = $extendSubjectName;
			}
		}
		unset($SubjectDisplayArr['Elective'][$MathExtendedPartID]);
		
		### Display Subject Score
		$ParnetSubjectRowSpanArr = array();
		$SubjectComponentCountArr = array();
		$x = '';
		foreach((array)$SubjectDisplayArr as $thisCategory => $thisSubjectArr)
		{
			$thisCategoryTitleEn = $eReportCard['Template'][$thisCategory.'En'];
			$thisCategoryTitleCh = $eReportCard['Template'][$thisCategory.'Ch'];
			
			$thisLCRowSpan = 0;
			foreach ((array)$thisSubjectArr as $thisParentSubjectID => $thisCmpSubjectArr)
			{
				$numOfCmpSubject = count($thisCmpSubjectArr) - 1;
				$thisIsParentSubject = ($numOfCmpSubject==0)? false : true;
				
				$thisParentSubjectObj = $SubjectObjArr[$thisParentSubjectID];
				$thisParentSubjectCode = $thisParentSubjectObj->CODEID;
				
				### Check Row Span
				// count number of Subject which is not all N.A. marks
				$thisSubjectRowSpan = 0;
				foreach ((array)$thisCmpSubjectArr as $thisCmpSubjectID => $thisCmpSubjectName)
				{
					$isAllNA = true;
					
					$thisIsComponentSubject = ($thisCmpSubjectID == 0)? false : true;
					$thisSubjectID = ($thisIsComponentSubject)? $thisCmpSubjectID : $thisParentSubjectID;
					$thisScaleDisplay = $GradingSchemeArr[$thisSubjectID]['ScaleDisplay'];
					if($needToGetfirstReportResult) {
					    $firstTermScaleDisplay = $firstTermGradingSchemeArr[$thisSubjectID]['ScaleDisplay'];
					}
					
					//2016-0205-1514-34164 [X92593]
					if ($FormNumber==6 && $thisCategory=='Elective' && $thisIsComponentSubject) {
						continue;
					}
					
					//X90476 - point 7 - do not show SBA score
					$thisSubjectNameEn = $this->GET_SUBJECT_NAME_LANG($thisSubjectID, 'en');
					$thisIsSBA = false;
					if (stripos($thisSubjectNameEn, 'School-Based Assessment') !== false) {
						$thisIsSBA = true;
					}
					if ($thisIsSBA) {
						continue;
					}
					
					// [2018-1204-0949-01164] point 2 - display only one overall mark (except Extended Part Module to Mathematics)
					$thisIsExtendedModule = false;
					if ($thisParentSubjectCode == '280') {
					    if (stripos($thisSubjectNameEn, 'extended') !== false) {
					        $thisIsExtendedModule = true;
					    }
					}
					if (in_array($thisParentSubjectCode, $this->SpecialCoreSubjectCodeArr) && $thisIsComponentSubject && !$thisIsExtendedModule) {
					    continue;
					}
					
					for ($i=0; $i<$numOfReportColumn; $i++)
					{
					    $thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];
					    $applyFirstReportResult = $needToGetfirstReportResult && $thisReportColumnID == $firstReportColumnId;
					    if($applyFirstReportResult) {
					        $thisScaleDisplay = $firstTermScaleDisplay;
					    }
						
						$thisMSGrade = $MarksArr[$thisSubjectID][$thisReportColumnID]['Grade'];
						$thisMSMark = $MarksArr[$thisSubjectID][$thisReportColumnID]['Mark'];
						
						$thisGrade = ($thisScaleDisplay=="G" || $thisScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($thisScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";	
						
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $thisScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $thisScaleDisplay=="G" ? "G" : "";
						}
						
						$thisMark = ($thisScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						if ($thisMark != "N.A.") {
							$isAllNA = false;
                        }
                        
                        if($applyFirstReportResult) {
                            $thisScaleDisplay = $GradingSchemeArr[$thisSubjectID]['ScaleDisplay'];
                        }
					}
					
					### Show Component Subject even if the Component is all N.A. (except Maths (Code=280), Component of Maths is hidden if all marks are N.A.)
					if ($StudentID == '' || ($thisIsComponentSubject && !in_array($thisParentSubjectCode, $this->CheckComponentNAArr)) || $isAllNA == false)
					{
						$thisLCRowSpan++;
						$thisSubjectRowSpan++;
						
						// X86217 - point 3.3 show parent subject overall result as a component subject
						// [2018-1204-0949-01164] point 2 - display only one overall mark   ($this->SpecialCoreSubjectCodeArr)
						if ($thisIsParentSubject && $thisCmpSubjectID==0 && in_array($thisParentSubjectCode, $this->CoreSubjectCodeArr) && !in_array($thisParentSubjectCode, $this->SpecialCoreSubjectCodeArr)) {
							$thisLCRowSpan++;
							$thisSubjectRowSpan++;
						}
						
						if ($thisIsComponentSubject) {
							$SubjectComponentCountArr[$thisParentSubjectID]++;
						}
					}
				}
				
				$ParnetSubjectRowSpanArr[$thisParentSubjectID] = $thisSubjectRowSpan;
			}
			
			
			### Category Display
			if ($thisLCRowSpan == 0) {
				continue;
			}
			
			$x .= '<tr>'."\n";
				$x .= '<td class="border_top" rowspan="'.$thisLCRowSpan.'" style="vertical-align:middle;text-align:center;">'."\n";
					$x .= '<span class="font_11px">'.$thisCategoryTitleEn.'</span>'."\n";
					$x .= '<br />'."\n";
					$x .= '<span class="font_9px">'.$thisCategoryTitleCh.'</span>'."\n";
				$x .= '</td>'."\n";
			
				
			# Subject Display
			$thisCategorySubjectCount = 0;
			foreach ((array)$thisSubjectArr as $thisParentSubjectID => $thisCmpSubjectArr)
			{
				$numOfCmpSubject = count($thisCmpSubjectArr) - 1;
				$thisIsParentSubject = ($numOfCmpSubject==0)? false : true;
				
				$thisParentSubjectObj = $SubjectObjArr[$thisParentSubjectID];
				$thisParentSubjectCode = $thisParentSubjectObj->CODEID;
				
				$thisOverallResultTr = '';
				$showOverallTr = false;
				$thisShowedOverallTr = false;
				
				// [2018-1204-0949-01164] point 2 - display only one overall mark  ($this->SpecialCoreSubjectCodeArr)
				if ($thisIsParentSubject && in_array($thisParentSubjectCode, $this->CoreSubjectCodeArr) && !in_array($thisParentSubjectCode, $this->SpecialCoreSubjectCodeArr)) {
					$showOverallTr = true;
				}
				
				$thisCmpSubjectCount = 0;
				foreach ((array)$thisCmpSubjectArr as $thisCmpSubjectID => $thisCmpSubjectName)
				{
					$isAllNA = true;
					$thisSubjectHTML = '';
					$thisExtendedModuleTr = '';
					
					$thisIsComponentSubject = ($thisCmpSubjectID == 0)? false : true;
					$thisSubjectID = ($thisIsComponentSubject)? $thisCmpSubjectID : $thisParentSubjectID;
					$thisSubjectNameEn = $this->GET_SUBJECT_NAME_LANG($thisSubjectID, 'en');
					$thisSubjectNameCh = $this->GET_SUBJECT_NAME_LANG($thisSubjectID, 'b5');
					$thisScaleInput = $GradingSchemeArr[$thisSubjectID]['ScaleInput'];
					$thisScaleDisplay = $GradingSchemeArr[$thisSubjectID]['ScaleDisplay'];
					if($needToGetfirstReportResult) {
					    $firstTermScaleInput = $firstTermGradingSchemeArr[$thisSubjectID]['ScaleInput'];
					    $firstTermScaleDisplay = $firstTermGradingSchemeArr[$thisSubjectID]['ScaleDisplay'];
					}
					
					//2016-0205-1514-34164 [X92593]
					if ($FormNumber==6 && $thisCategory=='Elective' && $thisIsComponentSubject) {
						continue;
					}
					
					//X90476 - point 4 - special handling for Maths extend module
					$thisIsExtendedModule = false;
					if ($thisParentSubjectCode=='280') {
						if (stripos($thisSubjectNameEn, 'extended') !== false) {
							$thisIsExtendedModule = true;
						}
					}
					
					//X90476 - point 7 - do not show SBA score
					$thisIsSBA = false;
					if (stripos($thisSubjectNameEn, 'School-Based Assessment') !== false) {
						$thisIsSBA = true;
					}
					if ($thisIsSBA) {
						continue;
					}
					
					// [2018-1204-0949-01164] point 2 - display only one overall mark (except Extended Part Module to Mathematics)
					if (in_array($thisParentSubjectCode, $this->SpecialCoreSubjectCodeArr) && $thisIsComponentSubject && !$thisIsExtendedModule) {
				        continue;
				    }
				    
					### Add some space before the English Name if it is a component subject
					if ($thisIsComponentSubject) {
						 $thisSubjectNameEn = '&nbsp;&nbsp;&nbsp;'.$thisSubjectNameEn;
					}
					
					# Border Top determination
					//X90476 - LS is not parent subject now
					//if (($thisIsParentSubject && !$thisIsComponentSubject) || $thisCategorySubjectCount == 0)
					if (($thisIsParentSubject && !$thisIsComponentSubject) || $thisCategorySubjectCount == 0 || ($thisParentSubjectCode==265 && !$thisIsComponentSubject)) {
						$thisBorderTop = 'border_top';
					}
					//2016-0205-1514-34164 [X92593]
					else if ($FormNumber==6 && $thisCategory=='Elective') {
						$thisBorderTop = 'border_top';
					} 
					else if ($thisCategory == 'Elective' || $thisCategory == 'Others') {
						$thisBorderTop = 'border_top_lite';
					}
					else {
						$thisBorderTop = '';
					}
					
					if ($thisIsExtendedModule) {
						$thisBorderTop = 'border_top_dotted';
					}
					
					# Subject which is in the Others Category or Subject Component => not bolded 
					$thisBoldClass = ($thisCategory=='Others' || $thisIsComponentSubject)? '' : 'text_bold';
					
					if ($thisCategorySubjectCount > 0) {
						$thisSubjectHTML .= '<tr>'."\n";
					}
						
						# Subject English Name
						$thisSubjectHTML .= '<td class="font_11px border_left '.$thisBorderTop.' '.$thisBoldClass.'" style="vertical-align:top;">'.$thisSubjectNameEn.'</td>'."\n";
						if ($showOverallTr && $thisCmpSubjectID == 0) {
							$thisOverallResultTr .= '<tr>'."\n";
							$thisOverallResultTr .= '<td class="font_11px border_left" style="vertical-align:top;">&nbsp;&nbsp;&nbsp;'.$eReportCard['Template']['OverallResult2En'].'</td>'."\n";
						}
						
						# Subject Chinese Name
						$thisSubjectHTML .= '<td class="font_9px '.$thisBorderTop.' '.$thisBoldClass.'" style="vertical-align:top;">'.$thisSubjectNameCh.'</td>'."\n";
						if ($showOverallTr && $thisCmpSubjectID == 0) {
							$thisOverallResultTr .= '<td class="font_9px" style="vertical-align:top;">'.$eReportCard['Template']['OverallResult2Ch'].'</td>'."\n";
						}
						
						# Subject Column Mark
						if ($StudentID == '')
						{
							### preview
							$isAllNA = false;
							
							# Columns Empty Cells & Overall Empty Cell
							//X86217 - point 3.3 hide overall column
							//for ($i=0; $i<$numOfReportColumn+1; $i++) {
							for ($i=0; $i<$numOfReportColumn; $i++) {
								$thisSubjectHTML .= '<td class="font_11px border_left '.$thisBorderTop.'" style="vertical-align:top;text-align:center;">'.$this->EmptySymbol.'</td>'."\n";
								
								if ($showOverallTr && $thisCmpSubjectID == 0) {
									$thisOverallResultTr .= '<td class="font_11px border_left" style="vertical-align:top;text-align:center;">'.$this->EmptySymbol.'</td>'."\n";
								}
							}
						}
						else
						{
							foreach ((array)$MarksArr[$thisSubjectID] as $thisReportColumnID => $thisMarkInfoArr)
							{
								/* 
								 * 	Display Manual Input Grade when
								 * 		1. 1st Report Column
								 * 		2. Settings > Display Result - Grade
								 * 		3. Manual Input Grade not empty
								 */
								$showInputGrade = $firstReportColumnId==$thisReportColumnID && $thisScaleDisplay=="G" && !empty($extraInfoAssoAry[$StudentID][$thisSubjectID]['Info']);
								
								$thisSubjectColumnWeight = $SubjectWeightAssoArr[$thisSubjectID][$thisReportColumnID];
								if ($thisSubjectColumnWeight == 0 && $thisReportColumnID != 0 && !$showInputGrade)
								{
									$thisSubjectHTML .= '<td class="font_11px border_left '.$thisBorderTop.'" style="vertical-align:top;text-align:center;">'.$this->EmptySymbol.'</td>'."\n";
								}
								else
								{
								    $isSpecialCoreGetOverallResult = $thisIsParentSubject && in_array($thisParentSubjectCode, $this->SpecialCoreSubjectCodeArr) && !$thisIsExtendedModule && $thisReportColumnID == $secondReportColumnId;
								    if (($showOverallTr || $isSpecialCoreGetOverallResult) && $thisParentSubjectID==$thisSubjectID) {
										// parent subject show overall column result only
										$thisTargetReportColumnID = 0;
									}
									else {
										$thisTargetReportColumnID = $thisReportColumnID;
									}
									$applyFirstReportResult = $needToGetfirstReportResult && $thisTargetReportColumnID == $firstReportColumnId;
									if($applyFirstReportResult) {
									    $thisScaleInput = $firstTermScaleInput;
									    $thisScaleDisplay = $firstTermScaleDisplay;
									}
									
									$thisMSGrade = $MarksArr[$thisSubjectID][$thisTargetReportColumnID]['Grade'];
									$thisMSMark = $MarksArr[$thisSubjectID][$thisTargetReportColumnID]['Mark'];
									
									$thisGrade = ($thisScaleDisplay=="G" || $thisScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
									$thisMark = ($thisScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";	
									
									# for preview purpose
									if(!$StudentID)
									{
										$thisMark 	= $thisScaleDisplay=="M" ? "S" : "";
										$thisGrade 	= $thisScaleDisplay=="G" ? "G" : "";
									}
									
									$thisMark = ($thisScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
									if ($thisMark != "N.A.") {
										$isAllNA = false;
									}
									
									### only Form 6 displays Overall Results
//									if (($thisIsParentSubject && !$thisIsComponentSubject && $thisReportColumnID != 0) || ($thisReportColumnID == 0 && $FormNumber != 6))
//									{
//										$thisMarkDisplay = (!$thisIsParentSubject || ($thisIsComponentSubject && $thisCmpSubjectCount==$SubjectComponentCountArr[$thisParentSubjectID]))? $this->EmptySymbol : '&nbsp;';
//										$thisBoldClass = '';
//									}
//									else
//									{
                                        # check special case
                                        if($applyFirstReportResult) {
                                            list($thisMark, $needStyle) = $this->checkSpCase($firstTermReportID, $thisSubjectID, $thisMark, $thisMSGrade);
                                        }
                                        else {
                                            list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisSubjectID, $thisMark, $thisMSGrade);
                                        }
										
										if($needStyle)
										{
											if ($thisTargetReportColumnID != 0 && $thisSubjectColumnWeight > 0 && $thisScaleDisplay=="M")
												$thisMarkTemp = ($UseWeightedMark && $thisSubjectColumnWeight != 0) ? $thisMark / $thisSubjectColumnWeight : $thisMark;
											else
												$thisMarkTemp = $thisMark;
												
											if($applyFirstReportResult) {
											    $thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $firstTermReportID, $ClassLevelID, $thisSubjectID, $thisMarkTemp);
											}
											else {
											    $thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $thisSubjectID, $thisMarkTemp);
											}
										}
										else
										{
											$thisMarkDisplay = $thisMark;
										}
										
										// X86217 - point 8 - show manual input grade
										if ($showInputGrade) {
											$thisMarkDisplay = $extraInfoAssoAry[$StudentID][$thisSubjectID]['Info'];
										}
										
										$thisMarkDisplay = ($thisMarkDisplay == '')? $this->EmptySymbol : $thisMarkDisplay;
										$thisBoldClass = ($thisIsComponentSubject==false && $thisScaleDisplay=='M' && $thisReportColumnID==0)? 'text_bold' : '';
//									}
									
									if ($showOverallTr && $thisParentSubjectID==$thisSubjectID) {
										$thisOverallMarkDisplay = $thisMarkDisplay;
									}
									
									if ($FormNumber==6 && $thisCategory=='Elective') {
										//2016-0205-1514-34164 [X92593] => do nth
									}
									else {
										if ($thisIsParentSubject && !$thisIsComponentSubject)
										{
										    // [2018-1204-0949-01164] point 2 - display only one overall mark
										    if(in_array($thisParentSubjectCode, $this->SpecialCoreSubjectCodeArr) && $thisParentSubjectID == $thisSubjectID) {
// 										        if($thisReportColumnID == $secondReportColumnId) {
// 										            // do nothing
// 										        } else {
//                                                     $thisMarkDisplay = $this->EmptySymbol;
// 										        }
										    }
										    else {
                                                $thisMarkDisplay = '&nbsp;';
										    }
										}
									}
									
									// X86217 - point 3.2 - hide overall column for senior form template
//									$thisSubjectHTML .= '<td class="font_11px border_left '.$thisBorderTop.' '.$thisBoldClass.'" style="vertical-align:top;text-align:center;">'.$thisMarkDisplay.'</td>'."\n";
									if ($thisReportColumnID != 0) {
										$thisSubjectHTML .= '<td class="font_11px border_left '.$thisBorderTop.' '.$thisBoldClass.'" style="vertical-align:top;text-align:center;">'.$thisMarkDisplay.'</td>'."\n";
										
										if ($showOverallTr && $thisCmpSubjectID == 0) {
											$thisOverallResultTr .= '<td class="font_11px border_left '.$thisBoldClass.'" style="vertical-align:top;text-align:center;">'."\n";
												if ($thisReportColumnID==$secondReportColumnId) {
													$thisOverallResultTr .= '<div style="width:30%; float:left;">&nbsp;</div>'."\n";
													$thisOverallResultTr .= '<div class="border_top" style="width:40%; float:left;">'.$thisOverallMarkDisplay.'</div>'."\n";
													$thisOverallResultTr .= '<div style="width:30%; float:left;">&nbsp;</div>'."\n";
												}
												else {
													$thisOverallResultTr .= $this->EmptySymbol."\n";
												}
											$thisOverallResultTr .= '</td>'."\n";
										}
									}
									
									if($applyFirstReportResult) {
									    $thisScaleInput = $GradingSchemeArr[$thisSubjectID]['ScaleInput'];
									    $thisScaleDisplay = $GradingSchemeArr[$thisSubjectID]['ScaleDisplay'];
									}
								}
							}
						}
						
						# Subject Teachers
						if (!$thisIsComponentSubject)
						{
							$SubjectTeacherInfoArr = $this->returnSubjectTeacher('', $thisSubjectID, $ReportID, $StudentID, $FullTeacherInfo=1);
							$numOfSubjectTeacher = count($SubjectTeacherInfoArr);
							
							$thisSubjectTeacherArr = array();
							for ($i=0; $i<$numOfSubjectTeacher; $i++)
							{
								$thisTitle = Get_Lang_Selection($SubjectTeacherInfoArr[$i]['TitleChinese'], $SubjectTeacherInfoArr[$i]['TitleEnglish']);
								$thisNickname = $SubjectTeacherInfoArr[$i]['NickName'];
								
								$thisSubjectTeacherArr[] = $thisTitle.' '.$thisNickname;
							}
							$thisSubjectTeacherDisplay = implode('<br />', $thisSubjectTeacherArr);
							$thisSubjectTeacherDisplay = ($thisSubjectTeacherDisplay == '')? '&nbsp;' : $thisSubjectTeacherDisplay;
							
							$thisRowSpan = $ParnetSubjectRowSpanArr[$thisParentSubjectID];
							$thisSubjectHTML .= '<td rowspan="'.$thisRowSpan.'" class="font_11px border_left '.$thisBorderTop.'" style="vertical-align:middle;text-align:left;padding-left:5px;">'.$thisSubjectTeacherDisplay.'</td>'."\n";
						}
						
						# Subject Teacher Comment
						if ($thisCmpSubjectCount == 0)
						{
							$thisComment = $SubjectTeacherCommentArr[$thisParentSubjectID];
							$thisRowSpan = $ParnetSubjectRowSpanArr[$thisParentSubjectID];
							if ($thisComment == '')
							{
								$thisAlign = 'left';
								$thisDisplay = $this->EmptySymbol;
							}
							else
							{
								$thisAlign = 'left';
								$thisDisplay = $thisComment;
							}
							$thisSubjectHTML .= '<td rowspan="'.$thisRowSpan.'" class="font_11px border_left '.$thisBorderTop.'" style="vertical-align:middle;text-align:'.$thisAlign.';padding-left:5px;">'.$thisDisplay.'</td>'."\n";
						}
					
					$thisSubjectHTML .= '</tr>'."\n";
					if ($showOverallTr && $thisCmpSubjectID == 0) {
						$thisOverallResultTr .= '</tr>'."\n";
					}
					
					### Show Component Subject even if the Component is all N.A.
					if (($thisIsComponentSubject && !in_array($thisParentSubjectCode, $this->CheckComponentNAArr)) || $isAllNA == false)
					{
						$thisCategorySubjectCount++;
						$thisCmpSubjectCount++;
						
						// X86217 - point 3.3 show parent subject overall result as a component subject (Maths show overall before extended module)
						// [2018-1204-0949-01164] point 2 - display only one overall mark
// 						if ($thisParentSubjectCode=='280' && $thisIsExtendedModule && $showOverallTr && !$thisShowedOverallTr) {
// 							$x .= $thisOverallResultTr;
// 							$thisShowedOverallTr = true;
// 						}
						
						$x .= $thisSubjectHTML;
					}
				}
				
				// X86217 - point 3.3 show parent subject overall result as a component subject
				// [2018-1204-0949-01164] point 2 - display only one overall mark
// 				if ($thisParentSubjectCode=='280') {
// 					if ($thisShowedOverallTr) {
// 						// overall already show before extended module
// 						$showOverallTr = false;
// 					}
// 					else {
// 						// if no extended module => show overall row
// 						$showOverallTr = true;
// 					}
// 				}
				
				if ($showOverallTr) {
					$x .= $thisOverallResultTr;
				}
			}
		}
		
		return $x;
	}
	
	
	function genMSTableColHeader($ReportID)
	{
//		global $eReportCard, $eRCTemplateSetting;
//		
//		# Retrieve Display Settings
//		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
//		$ClassLevelID = $ReportSetting['ClassLevelID'];
//		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
//		$SemID = $ReportSetting['Semester'];
//		$ReportType = $SemID == "F" ? "W" : "T";
//		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
//		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
//		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
//		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
//		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
//		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
//		$LineHeight = $ReportSetting['LineHeight'];
//		
//		# updated on 08 Dec 2008 by Ivan
//		# if subject overall column is not shown, grand total, grand average... also cannot be shown
//		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
//		$ShowRightestColumn = $ShowSubjectOverall;
//		
//		$n = 0;
//		$e = 0;	# column# within term/assesment
//		$t = array(); # number of assessments of each term (for consolidated report only)
//		#########################################################
//		############## Marks START
//		$row2 = "";
//		if($ReportType=="T")	# Terms Report Type
//		{
//			# Retrieve Invloved Assesment
//			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
//			$ColumnID = array();
//			$ColumnTitle = array();
//			if (sizeof($ColoumnTitle) > 0)
//				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
//				
//			$e = 0;
//			for($i=0;$i<sizeof($ColumnTitle);$i++)
//			{
//				//$ColumnTitleDisplay = convert2unicode($ColumnTitle[$i], 1, 2);
//				$ColumnTitleDisplay = $ColumnTitle[$i];
//				$row1 .= "<td valign='middle' height='{$LineHeight}' class='border_left tabletext' align='center'><b>". $ColumnTitleDisplay . "</b></td>";
//				$n++;
// 				$e++;
//			}
//		}
//		else					# Whole Year Report Type
//		{
//			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
// 			$needRowspan=0;
//			for($i=0;$i<sizeof($ColumnData);$i++)
//			{
//				$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
//				$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
//				if($isDetails==1)
//				{
//					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
//					$thisReportID = $thisReport['ReportID'];
//					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
//					$ColumnID = array();
//					$ColumnTitle = array();
//					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
//					for($j=0;$j<sizeof($ColumnTitle);$j++)
//					{
//						//$ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$j], 1, 2));
//						$ColumnTitleDisplay =  $ColumnTitle[$j];
//						$row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $ColumnTitleDisplay . "</td>";
//						$n++;
//						$e++;
//					}
//					$colspan = "colspan='". sizeof($ColumnTitle) ."'";
//					$Rowspan = "";
//					$needRowspan++;
//					$t[$i] = sizeof($ColumnTitle);
//				}
//				else
//				{
//					$colspan = "";
//					$Rowspan = "rowspan='2'";
//					$e++;
//				}
//				$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left small_title' align='center'>". $SemName ."</td>";
//			}
//		}
//		############## Marks END
//		#########################################################
//		$Rowspan = $row2 ? "rowspan='2'" : "";
//
//		if(!$needRowspan)
//			$row1 = str_replace("rowspan='2'", "", $row1);
//		
//		$x = "<tr>";
//		# Subject 
//		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
//		for($i=0;$i<sizeof($SubjectColAry);$i++)
//		{
//			$SubjectEng = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'];
//			$SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];
//			$SubjectTitle = $SubjectColAry[$i];
//			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
//			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
//			$x .= "<td {$Rowspan}  valign='middle' class='small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>". $SubjectTitle . "</td>";
//			$n++;
//		}
//		
//		# Marks
//		$x .= $row1;
//		
//		# Subject Overall 
//		if($ShowRightestColumn)
//		{
//			$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Overall'] ."' class='border_left small_title' align='center'>". $eReportCard['Template']['SubjectOverall'] ."</td>";
//			$n++;
//		}
//		else
//		{
//			//$e--;	
//		}
//		
//		# Subject Teacher Comment
//		if ($AllowSubjectTeacherComment) {
//			$x .= "<td {$Rowspan} valign='middle' class='border_left small_title' align='center' width='10%'>".$eReportCard['TeachersComment']."</td>";
//		}
//		
//		$x .= "</tr>";
//		if($row2)	$x .= "<tr>". $row2 ."</tr>";
//		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
//		global $eRCTemplateSetting;
//		
//		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
//		$LineHeight = $ReportSetting['LineHeight'];
//		
//		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
// 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
// 		
// 		$x = array(); 
//		$isFirst = 1;
//		if (sizeof($SubjectArray) > 0) {
//	 		foreach($SubjectArray as $SubjectID=>$Ary)
//	 		{
//		 		foreach($Ary as $SubSubjectID=>$Subjs)
//		 		{
//			 		$t = "";
//			 		$Prefix = "&nbsp;&nbsp;";
//			 		if($SubSubjectID==0)		# Main Subject
//			 		{
//				 		$SubSubjectID=$SubjectID;
//				 		$Prefix = "";
//			 		}
//			 		
//			 		$css_border_top = ($Prefix)? "" : "border_top";
//			 		foreach($SubjectDisplay as $k=>$v)
//			 		{
//				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
//		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
//		 				
//		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
//		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
//		 				
//			 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
//						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
//						$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='tabletext'>$v</td>";
//						$t .= "</tr></table>";
//						$t .= "</td>";
//			 		}
//					$x[] = $t;
//					$isFirst = 0;
//				}
//		 	}
//		}
//		
// 		return $x;
	}
	
	function getSignatureTable($ReportID='', $StudentID='')
	{
 		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
 		
 		$SignatureTitleArr = $eRCTemplateSetting['Signature'];
 		$numOfSignature = count($SignatureTitleArr);
 		
		$EmptySpaceWidth = 5;
		$TotalEmptySpaceWidth = $EmptySpaceWidth * $numOfSignature;
		$SignatureWidth = floor((100 - $TotalEmptySpaceWidth) / $numOfSignature);
		
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		$lclass = new libclass();
		$ClassTeacherArr =  $lclass->returnClassTeacher($this->Get_Student_ClassName($StudentID), $this->schoolYearID);
		$numOfClassTeacher = count($ClassTeacherArr);
		
		$ClassTeacherDisplayArr = array();
		for ($i=0; $i<$numOfClassTeacher; $i++)
		{
			$thisTitle = Get_Lang_Selection($ClassTeacherArr[$i]['TitleChinese'], $ClassTeacherArr[$i]['TitleEnglish']);
			$thisNickname = $ClassTeacherArr[$i]['NickName'];
			
			$ClassTeacherDisplayArr[] = $thisTitle.' '.$thisNickname;
		}
		$ClassTeacherDisplayText = implode(' / ', $ClassTeacherDisplayArr);
		
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="4" cellspacing="0" align="center">'."\n";
			$x .= "<tr>";
				for($k=0; $k<$numOfSignature; $k++)
				{
					$thisSettingID = trim($SignatureTitleArr[$k]);
					$thisTitle = $eReportCard['Template'][$thisSettingID."En"].'&nbsp;&nbsp;&nbsp;<span class="font_11px">'.$eReportCard['Template'][$thisSettingID."Ch"].'</span>';
					
					$ContentOnTop = '';
					$DisplayName = '';
					switch($thisSettingID)
					{
						case "Principal": 
							$DisplayName .= $eReportCard['Template']['PrincipalNameEn'];
							break;
						case "ClassTeacher": 
							$DisplayName .= $ClassTeacherDisplayText; 
							break;
						case "ParentGuardian": 
							$DisplayName = '&nbsp;';
							break;
						default: 
							$DisplayName = '&nbsp;';
					}	
					
					$x .= "<td valign='bottom' align='center' width='".$SignatureWidth."%'>";
						$x .= "<table cellspacing='0' cellpadding='1' border='0' width='100%'>";
							$x .= "<tr><td align='center' class='font_13px border_top' valign='top' nowrap>".$DisplayName."</td></tr>";
							$x .= "<tr><td align='center' class='font_13px' valign='bottom'><b>".$thisTitle."</b></td></tr>";
						$x .= "</table>";
					$x .= "</td>";
					
					if ($k != $numOfSignature - 1)
						$x .= "<td valign='bottom' align='center' width='".$EmptySpaceWidth."%'>&nbsp;</td>";
				}
			$x .= "</tr>";
		$x .= "</table>";
 		
 		return $x;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
//		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
//		
//		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
//		$LineHeight 				= $ReportSetting['LineHeight'];
//		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
//		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
//		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
//		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
//		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
//		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
//		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
//		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
//		$SemID 						= $ReportSetting['Semester'];
// 		$ReportType 				= $SemID == "F" ? "W" : "T";
//		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
//		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
//		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
//		
//		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
//		
//		$CalSetting = $this->LOAD_SETTING("Calculation");
//		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
//		
//		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
//		
//		# retrieve Student Class
//		if($StudentID)
//		{
//			include_once($PATH_WRT_ROOT."includes/libuser.php");
//			$lu = new libuser($StudentID);
//			$ClassName 		= $this->Get_Student_ClassName($StudentID);
//			$WebSamsRegNo 	= $lu->WebSamsRegNo;
//		}
//		
//		# retrieve result data
//		$result = $this->getReportResultScore($ReportID, 0, $StudentID,'',0,1);
//		$GrandTotal = $StudentID ? $this->Get_Score_Display_HTML($result['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
//		$GrandAverage = $StudentID ? $this->Get_Score_Display_HTML($result['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
//		
//		$ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
//  		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
//  		$ClassNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
//  		$FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
//		
//		if ($CalOrder == 2) {
//			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
//				
//				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID, '', 1,1);
//				
//				$columnTotal[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
//				$columnAverage[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
//				
//				$columnClassPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "S";
//				$columnFormPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "S";
//				$columnClassNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
//				$columnFormNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";
//			}
//		}
//		
//		$first = 1;
//		# Overall Result
//		if($ShowGrandTotal)
//		{
//			$thisTitleEn = $eReportCard['Template']['OverallResultEn'];
//			$thisTitleCh = $eReportCard['Template']['OverallResultCh'];
//			$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnTotal, $GrandTotal, $first);
//			
//			$first = 0;
//		}
//		
//		if ($eRCTemplateSetting['HideCSVInfo'] != true)
//		{
//			# Average Mark 
//			if($ShowGrandAvg)
//			{
//				$thisTitleEn = $eReportCard['Template']['AvgMarkEn'];
//				$thisTitleCh = $eReportCard['Template']['AvgMarkCh'];
//				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnAverage, $GrandAverage, $first);
//				
//				$first = 0;
//			}
//			
//			# Position in Class 
//			if($ShowOverallPositionClass)
//			{
//				$thisTitleEn = $eReportCard['Template']['ClassPositionEn'];
//				$thisTitleCh = $eReportCard['Template']['ClassPositionCh'];
//				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassPos, $ClassPosition, $first);
//				
//				$first = 0;
//			}
//			
//			# Number of Students in Class 
//			if($ShowNumOfStudentClass)
//			{
//				$thisTitleEn = $eReportCard['Template']['ClassNumOfStudentEn'];
//				$thisTitleCh = $eReportCard['Template']['ClassNumOfStudentCh'];
//				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassNumOfStudent, $ClassNumOfStudent, $first);
//				
//				$first = 0;
//			}
//			
//			# Position in Form 
//			if($ShowOverallPositionForm)
//			{
//				$thisTitleEn = $eReportCard['Template']['FormPositionEn'];
//				$thisTitleCh = $eReportCard['Template']['FormPositionCh'];
//				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormPos, $FormPosition, $first);
//				
//				$first = 0;
//			}
//			
//			# Number of Students in Form 
//			if($ShowNumOfStudentForm)
//			{
//				$thisTitleEn = $eReportCard['Template']['FormNumOfStudentEn'];
//				$thisTitleCh = $eReportCard['Template']['FormNumOfStudentCh'];
//				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormNumOfStudent, $FormNumOfStudent, $first);
//				
//				$first = 0;
//			}
//			
//			##################################################################################
//			# CSV related
//			##################################################################################
//			# build data array
//			$ary = array();
//			//$csvType = $this->getOtherInfoType();
//			
//			//modified by Marcus 20/8/2009
//			$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID,$StudentID);
//			$ary = $OtherInfoDataAry[$StudentID];
//			
//			if($SemID=="F")
//			{
//				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
//				
//				/*
//				foreach($ColumnData as $k1=>$d1)
//				{
//					$TermID = $d1['SemesterNum'];
//					$InfoTermID = $TermID;
//					
//					if(!empty($csvType))
//					{
//						foreach($csvType as $k=>$Type)
//						{
//							$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);
//							if(!empty($csvData)) 
//							{
//								foreach($csvData as $RegNo=>$data)
//								{
//									if($RegNo == $WebSamsRegNo)
//									{
//										foreach($data as $key=>$val)
//											$ary[$TermID][$key] = $val;
//									}
//								}
//							}
//						}
//					}
//				}
//				*/
//				
//				# calculate sems/assesment col#
//				$ColNum2Ary = array();
//				foreach($ColumnData as $k1=>$d1)
//				{
//					$TermID = $d1['SemesterNum'];
//					if($d1['IsDetails']==1)
//					{
//						# check sems/assesment col#
//						$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
//						$thisReportID = $thisReport['ReportID'];
//						$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
//						$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1;
//					}
//					else
//						$ColNum2Ary[$TermID] = 0;
//				}
//			}
//			/*
//			else
//			{
//				$InfoTermID = $SemID;
//				
//				if(!empty($csvType))
//				{
//					foreach($csvType as $k=>$Type)
//					{
//						$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);
//						
//						if(!empty($csvData)) 
//						{
//							foreach($csvData as $RegNo=>$data)
//							{
//								if($RegNo == $WebSamsRegNo)
//								{
//									foreach($data as $key=>$val)
//										$ary[$SemID][$key] = $val;
//								}
//							}
//						}
//					}
//				}
//			}
//			*/
//
//			$border_top = $first ? "border_top" : "";
//			
//			# Days Absent 
//			$thisTitleEn = $eReportCard['Template']['DaysAbsentEn'];
//			$thisTitleCh = $eReportCard['Template']['DaysAbsentCh'];
//			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Days Absent", $ary);
//			
//			# Times Late 
//			$thisTitleEn = $eReportCard['Template']['TimesLateEn'];
//			$thisTitleCh = $eReportCard['Template']['TimesLateCh'];
//			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Time Late", $ary);
//			
//			# Conduct 
//			$thisTitleEn = $eReportCard['Template']['ConductEn'];
//			$thisTitleCh = $eReportCard['Template']['ConductCh'];
//			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Conduct", $ary);
//		}
//		
//		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
//		global $eReportCard;				
//		# Retrieve Display Settings
//		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
//		$SemID 				= $ReportSetting['Semester'];
// 		$ReportType 		= $SemID == "F" ? "W" : "T";		
// 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
//		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
//		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
//		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
//		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
//		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
//		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
//		
//		# updated on 08 Dec 2008 by Ivan
//		# if subject overall column is not shown, grand total, grand average... also cannot be shown
//		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
//		$ShowRightestColumn = $ShowSubjectOverall;
//		
//		# Retrieve Calculation Settings
//		$CalSetting = $this->LOAD_SETTING("Calculation");
//		$UseWeightedMark = $CalSetting['UseWeightedMark'];
//		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
//		
//		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
//		$SubjectDecimal = $StorageSetting["SubjectScore"];
//		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
//		
//		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
//		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
//		if(sizeof($MainSubjectArray) > 0)
//			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
//		
//		$n = 0;
//		$x = array();
//		$isFirst = 1;
//				
//		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
//		if($ReportType=="T")	# Temrs Report Type
//		{
//			$CalculationOrder = $CalSetting["OrderTerm"];
//			
//			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
//			$ColumnID = array();
//			$ColumnTitle = array();
//			if(sizeof($ColoumnTitle) > 0)
//				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
//							
//			foreach($SubjectArray as $SubjectID => $SubjectName)
//			{
//				$isSub = 0;
//				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
//				
//				// check if it is a parent subject, if yes find info of its components subjects
//				$CmpSubjectArr = array();
//				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
//				if (!$isSub) {
//					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
//					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
//				}
//				
//				# define css
//				$css_border_top = ($isSub)? "" : "border_top";
//		
//				# Retrieve Subject Scheme ID & settings
//				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
//				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
//				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
//				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
//				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
//				
//				$isAllNA = true;
//									
//				# Assessment Marks & Display
//				for($i=0;$i<sizeof($ColumnID);$i++)
//				{
//					$thisColumnID = $ColumnID[$i];
//					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
//					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
//					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
//					
//					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
//					$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
//
//					if ($isSub && $columnSubjectWeightTemp == 0)
//					{
//						$thisMarkDisplay = $this->EmptySymbol;
//					}
//					else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
//						$thisMarkDisplay = $this->EmptySymbol;
//					} else {
//						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
//						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
//						
//						$thisMSGrade = $MarksAry[$SubjectID][$ColumnID[$i]]['Grade'];
//						$thisMSMark = $MarksAry[$SubjectID][$ColumnID[$i]]['Mark'];
//						
//						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//						
//						# for preview purpose
//						if(!$StudentID)
//						{
//							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
//							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
//						}
//						
//						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
//						
//						if ($thisMark != "N.A.")
//						{
//							$isAllNA = false;
//						}
//						
//						# check special case
//						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
//						
//						if($needStyle)
//						{
//							if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
//							{
//								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
//							}
//							else
//							{
//								$thisMarkTemp = $thisMark;
//							}
//							
//							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
//						}
//						else
//						{
//							$thisMarkDisplay = $thisMark;
//						}
//						
//					//	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
//					//	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
//					//	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark; 
//					}
//						
//					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
//					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
//  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
//  					
//  					if($ShowSubjectFullMark)
//  					{
//	  					# check special full mark
//	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
//	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
//	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
//	  					
//						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
//						$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
//					}
//					
//					$x[$SubjectID] .= "</tr></table>";
//					$x[$SubjectID] .= "</td>";
//				}
//								
//				# Subject Overall (disable when calculation method is Vertical-Horizontal)
//				if($ShowSubjectOverall)
//				{
//					$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
//					$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];
//
//					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//					
//					# for preview purpose
//					if(!$StudentID)
//					{
//						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
//						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
//					}
//					
//					#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
//					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
//										
//					if ($thisMark != "N.A.")
//					{
//						$isAllNA = false;
//					}
//						
//					# check special case
//					if ($CalculationMethod==2 && $isSub)
//					{
//						$thisMarkDisplay = $this->EmptySymbol;
//					}
//					else
//					{
//						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
//						if($needStyle)
//						{
//	 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
//								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
//							else
//								$thisMarkTemp = $thisMark;
//								
//							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
//						}
//						else
//							$thisMarkDisplay = $thisMark;
//					}
//					
//					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
//					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
//  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
//  					if($ShowSubjectFullMark)
//  					{
//	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
//	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
//	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
//						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $thisFullMark .")</td>";
//					}
//					$x[$SubjectID] .= "</tr></table>";
//					$x[$SubjectID] .= "</td>";
//				} else if ($ShowRightestColumn) {
//					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
//				}
//				$isFirst = 0;
//				
//				# construct an array to return
//				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
//				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
//			}
//		} # End if($ReportType=="T")
//		else					# Whole Year Report Type
//		{
//			$CalculationOrder = $CalSetting["OrderFullYear"];
//			
//			# Retrieve Invloved Temrs
//			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
//			
//			foreach($SubjectArray as $SubjectID => $SubjectName)
//			{
//				# Retrieve Subject Scheme ID & settings
//				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
//				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
//				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
//				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
//				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
//							
//				$t = "";
//				$isSub = 0;
//				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
//				
//				// check if it is a parent subject, if yes find info of its components subjects
//				$CmpSubjectArr = array();
//				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
//				if (!$isSub) {
//					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
//					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
//				}
//				
//				$isAllNA = true;
//				
//				# Terms's Assesment / Terms Result
//				for($i=0;$i<sizeof($ColumnData);$i++)
//				{
//					#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
//					$css_border_top = $isSub? "" : "border_top";
//					
//					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
//					if($isDetails==1)		# Retrieve assesments' marks
//					{
//						# See if any term reports available
//						$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum'] ."' and ClassLevelID = '".$ClassLevelID."' ");
//						
//						# if no term reports, CANNOT retrieve assessment result at all
//						if (empty($thisReport)) {
//							for($j=0;$j<sizeof($ColumnID);$j++) {
//								$x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
//							}
//						} else {
//							$thisReportID = $thisReport['ReportID'];
//							
//							$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
//							$ColumnID = array();
//							$ColumnTitle = array();
//							foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
//							
//							$thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
//							
//							for($j=0;$j<sizeof($ColumnID);$j++)
//							{
//								$thisColumnID = $ColumnID[$j];
//								$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
//								$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
//								$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
//								
//								$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
//								$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
//					
//								if ($isSub && $columnSubjectWeightTemp == 0)
//								{
//									$thisMarkDisplay = $this->EmptySymbol;
//								}
//								else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
//									$thisMarkDisplay = $this->EmptySymbol;
//								} else {
//									$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnID[$j] ."' and SubjectID = '$SubjectID' ");
//									$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
//									
//									$thisMSGrade = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade'];
//									$thisMSMark = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Mark'];
//									
//									$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//									$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//									
//									# for preview purpose
//									if(!$StudentID)
//									{
//										$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
//										$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
//									}
//									$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
//													
//									if ($thisMark != "N.A.")
//									{
//										$isAllNA = false;
//									}
//									
//									# check special case
//									list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
//									if($needStyle)
//									{
//										if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
//										{
//											$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
//										}
//										else
//										{
//											$thisMarkTemp = $thisMark;
//										}
//										
//										$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
//									}
//									else
//									{
//										$thisMarkDisplay = $thisMark;
//									}
//								}
//									
//								$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
//								$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
//			  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
//			  					
//			  					if($ShowSubjectFullMark)
//			  					{
//				  					$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
//									$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
//								}
//								
//								$x[$SubjectID] .= "</tr></table>";
//								$x[$SubjectID] .= "</td>";
//							}
//						}
//					}
//					else					# Retrieve Terms Overall marks
//					{
//						if ($isParentSubject && $CalculationOrder == 1) {
//							$thisMarkDisplay = $this->EmptySymbol;
//						} else {
//							# See if any term reports available
//							$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
//							
//							# if no term reports, the term mark should be entered directly
//							
//							if (empty($thisReport)) {
//								$thisReportID = $ReportID;
//								$thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
//								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
//								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
//								//$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
//								
//								$thisMSGrade = $thisMarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"];
//								$thisMSMark = $thisMarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"];
//								
//								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//									
//							} else {
//								$thisReportID = $thisReport['ReportID'];
//								$thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
//								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
//								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
//								//$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
//								
//								$thisMSGrade = $thisMarksAry[$SubjectID][0]["Grade"];
//								$thisMSMark = $thisMarksAry[$SubjectID][0]["Mark"];
//								
//								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//							}
//							
//							$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
//							$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
//														
//							# for preview purpose
//							if(!$StudentID)
//							{
//								$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
//								$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
//							}
//							# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
//							$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
//							
//							if ($thisMark != "N.A.")
//							{
//								$isAllNA = false;
//							}
//						
//							# check special case
//							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
//							if($needStyle)
//							{
//								$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
//							}
//							else
//							{
//								$thisMarkDisplay = $thisMark;
//							}
//						}
//							
//						$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
//						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
//	  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
//	  					
//	  					if($ShowSubjectFullMark)
//	  					{
// 							$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1 && $UseWeightedMark) ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID];
//							$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
//						}
//						
//						$x[$SubjectID] .= "</tr></table>";
//						$x[$SubjectID] .= "</td>";
//					}
//				}
//				
//				# Subject Overall
//				if($ShowSubjectOverall)
//				{
//					$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
//					$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
//					
//					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//					
//					# for preview purpose
//					if(!$StudentID)
//					{
//						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
//						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
//					}
//					
//					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
//					
//					if ($thisMark != "N.A.") 
//					{
//						$isAllNA = false;
//					}
//						
//					if ($CalculationMethod==2 && $isSub)
//					{
//						$thisMarkDisplay = $this->EmptySymbol;
//					}
//					else
//					{
//						# check special case
//						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
//						if($needStyle)
//						{
//							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
//						}
//						else
//							$thisMarkDisplay = $thisMark;
//					}
//						
//					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
//					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
//  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
//  					
//  					if($ShowSubjectFullMark)
//  					{
//	  					# check special full mark
//	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
//	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
//	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
//	  					
//						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark * $thisSubjectWeight : $thisFullMark) : $thisFullMark;
//						$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
//					}
//  					
//					$x[$SubjectID] .= "</tr></table>";
//					$x[$SubjectID] .= "</td>";
//					
//					
//				} else if ($ShowRightestColumn) {
//					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
//				}
//				
//				$isFirst = 0;
//				
//				# construct an array to return
//				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
//				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
//			}
//		}	# End Whole Year Report Type
//		
//		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
		
		if ($eRCTemplateSetting['HideCSVInfo'] == true)
		{
			return "";
		}
			
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
 		$AttendanceDays		= $ReportSetting['AttendanceDays'];
 		
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		//$FormNumber = 1;
		
		### Get CSV Info
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID,$StudentID);
		$StudentOtherInfoArr = $OtherInfoDataAry[$StudentID][$SemID];
		
		### Get Overall Result
		$result = $this->getReportResultScore($ReportID, 0, $StudentID, '', 0, 1);
		$GrandTotal = $StudentID ? $this->Get_Score_Display_HTML($result['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		$ClassNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
		
		## Get Class Teacher Comment
		$SubjectTeacherCommentArr = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$ClassTeacherComment = nl2br($SubjectTeacherCommentArr[0]);
		
		$RowHeight = ($FormNumber <= 3)? '28px' : '28px';
		$BorderLeft = ($FormNumber <= 3)? 'border_left' : '';
		
		$x = '';
		$x .= '<table class="border_table" cellpadding="2" cellspacing="0">'."\n";
			$x .= '<tr>'."\n";
				if ($FormNumber <= 3)
				{
					# Grand Total
					$x .= '<td class="font_13px" style="width:14%;">'.$eReportCard['Template']['OverallResultEn'].'</td>'."\n";
					$x .= '<td class="font_12px" style="width:6%;">'.$eReportCard['Template']['OverallResultCh'].'</td>'."\n";
					$x .= '<td class="font_13px" style="width:2%;"> : </td>'."\n";
					$x .= '<td class="font_13px" style="width:4%;">'.$GrandTotal.'</td>'."\n";
				}
				
				
				# Number of times late
				$thisDisplay = $StudentOtherInfoArr['Times Late'];
				$thisDisplay = ($thisDisplay == '')? 0 : $thisDisplay;
				$thisWidth = ($FormNumber <= 3)? '13%' : '13%';
				$x .= '<td class="font_13px '.$BorderLeft.'" style="height:'.$RowHeight.';width:'.$thisWidth.';">'.$eReportCard['Template']['TimesLateEn'].'</td>'."\n";
				$thisWidth = ($FormNumber <= 3)? '6%' : '6%';
				$x .= '<td class="font_12px" style="width:'.$thisWidth.';">'.$eReportCard['Template']['TimesLateCh'].'</td>'."\n";
				$thisWidth = ($FormNumber <= 3)? '2%' : '4%';
				$x .= '<td class="font_13px" style="width:'.$thisWidth.';"> : </td>'."\n";
				$thisWidth = ($FormNumber <= 3)? '5%' : '10%';
				$x .= '<td class="font_13px" style="width:'.$thisWidth.';">'.$thisDisplay.'</td>'."\n";
				
				# Remarks
				$thisDisplay = implode('<br />', (array)$StudentOtherInfoArr['Remark']);
				$thisDisplay = ($thisDisplay == '')? '&nbsp;' : $thisDisplay;
				$thisWidth = ($FormNumber <= 3)? '30%' : '34%';
				$x .= '<td class="font_13px border_left" rowspan="3" style="vertical-align:top;width:'.$thisWidth.';">'."\n";
					$x .= '<b>'.$eReportCard['Template']['RemarkEn'].' '.$eReportCard['Template']['RemarkCh'].'</b>'."\n";
					$x .= '<br />'."\n";
					$x .= $thisDisplay;
				$x .= '</td>'."\n";
				
				# Form Teacher's Comments
				$thisDisplay = $ClassTeacherComment;
				$thisDisplay = ($thisDisplay == '')? '&nbsp;' : $thisDisplay;
				$thisWidth = ($FormNumber <= 3)? '19%' : '35%';
				$thisSeparator = ($FormNumber <= 3)? '<br />' : '&nbsp;&nbsp;&nbsp;';
				$x .= '<td class="font_13px border_left" rowspan="3" style="vertical-align:top;width:'.$thisWidth.';">'."\n";
					$x .= '<b>'.$eReportCard['Template']['ClassTeacherCommentEn'].$thisSeparator.$eReportCard['Template']['ClassTeacherCommentCh'].'</b>'."\n";
					$x .= '<br />'."\n";
					$x .= $thisDisplay;
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			$x .= '<tr>'."\n";
				if ($FormNumber <= 3)
				{
					# Position in Class
					$thisDisplay = $StudentOtherInfoArr['Class Position'];
					// 2011-0221-1500-26069
//					$thisDisplay = ($thisDisplay == '')? $this->EmptySymbol : $thisDisplay;
//					$thisDisplay .= ' / ';
//					$thisDisplay .= $ClassNumOfStudent;

					$thisDisplay = ($thisDisplay == '')? $this->EmptySymbol : $thisDisplay.' / '.$ClassNumOfStudent;
					$x .= '<td class="font_13px border_top">'.$eReportCard['Template']['ClassPositionEn'].'</td>'."\n";
					$x .= '<td class="font_12px border_top">'.$eReportCard['Template']['ClassPositionCh'].'</td>'."\n";
					$x .= '<td class="font_13px border_top"> : </td>'."\n";
					$x .= '<td class="font_13px border_top">'.$thisDisplay.'</td>'."\n";
				}
				
				# Attendance (days)
				$thisDisplay = $StudentOtherInfoArr['Attendance Days'];
				$thisDisplay = ($thisDisplay == '')? 0 : $thisDisplay;
				$thisDisplay .= ' / ';
				$thisDisplay .= '<b>'.$AttendanceDays.'</b>';				
				$x .= '<td class="font_13px border_top '.$BorderLeft.'" style="height:'.$RowHeight.';">'.$eReportCard['Template']['AttendanceDaysEn'].'</td>'."\n";
				$x .= '<td class="font_12px border_top">'.$eReportCard['Template']['AttendanceDaysCh'].'</td>'."\n";
				$x .= '<td class="font_13px border_top"> : </td>'."\n";
				$x .= '<td class="font_13px border_top">'.$thisDisplay.'</td>'."\n";
			$x .= '</tr>'."\n";
			
			$x .= '<tr>'."\n";
				if ($FormNumber <= 3)
				{
					# Number of service hours
					$thisDisplay = $StudentOtherInfoArr['Service Hours'];
					$thisDisplay = ($thisDisplay == '')? 0 : $thisDisplay;
					$x .= '<td class="font_13px border_top">'.$eReportCard['Template']['NumberOfServiceHoursEn'].'</td>'."\n";
					$x .= '<td class="font_12px border_top">'.$eReportCard['Template']['NumberOfServiceHoursCh'].'</td>'."\n";
					$x .= '<td class="font_13px border_top"> : </td>'."\n";
					$x .= '<td class="font_13px border_top">'.$thisDisplay.'</td>'."\n";
				}
				
				# Conduct
				$thisDisplay = $StudentOtherInfoArr['Conduct'];
				$thisDisplay = ($thisDisplay == '')? 0 : $thisDisplay;
				$x .= '<td class="font_13px border_top '.$BorderLeft.'" style="height:'.$RowHeight.';">'.$eReportCard['Template']['ConductEn'].'</td>'."\n";
				$x .= '<td class="font_12px border_top">'.$eReportCard['Template']['ConductCh'].'</td>'."\n";
				$x .= '<td class="font_13px border_top"> : </td>'."\n";
				$x .= '<td class="font_13px border_top">'.$thisDisplay.'</td>'."\n";
			$x .= '</tr>'."\n";
			
		$x .= '</table>'."\n";
		return $x;
	}
	
	########### END Template Related

	
	function Is_Show_Rightest_Column($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		return $ShowRightestColumn;
	}
	
	function Get_Calculation_Setting($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		return $CalOrder;
	}
	
	// $border, e.g. "left,right,top"
	function Get_Double_Line_Td($border="",$width=1)
	{
		global $image_path,$LAYOUT_SKIN;
		
		$borderArr = explode(",",$border);
		$left = $right = $top = $bottom = 0;
		
		if(in_array("left",$borderArr)) $left = $width;
		if(in_array("right",$borderArr)) $right = $width;
		if(in_array("top",$borderArr)) $top = $width;
		if(in_array("bottom",$borderArr)) $bottom = $width;

		$style = " style='border-style:solid; border-color:#000; border-width:{$top}px {$right}px {$bottom}px {$left}px ' ";
		
		$td = "<td width='0%' height='0%' $style><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='$width' height='$width'/></td>";
		return $td;
	}
}
?>