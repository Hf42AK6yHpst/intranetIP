<?php
# Editing by 

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/cheung_chuk_shan.$intranet_session_language.php");

# In case of not setting $ReportCardCustomSchoolName in includes/settings.php, set it to "cheung_chuk_shan" by default
if (!isset($ReportCardCustomSchoolName)) $ReportCardCustomSchoolName = "cheung_chuk_shan";

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "eca", "remark", "attendance");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 0;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "--";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard;
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
		$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
		$TableTop .= $this->Get_Empty_Row('10px');
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableTop .= $this->Get_Empty_Row('5px');
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='950px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitleArr = explode(":_:", $ReportTitle);
			$ReportTitleEn = $ReportTitleArr[0];
			$ReportTitleCh = $ReportTitleArr[1];
			
			$ReportTitle = "<b>".$ReportTitleArr[0]."</b><br />".$ReportTitleArr[1];
			
			# get school badge
			//$SchoolLogo = GET_SCHOOL_BADGE();
				
			# get school name
			//$SchoolName = GET_SCHOOL_NAME();	
			$separator = "&nbsp;&nbsp;&nbsp;&nbsp;";
			$SchoolInfoEn = $eReportCard['Template']['SchoolAddressEn'].$separator.$eReportCard['Template']['SchoolTelEn'];
			$SchoolInfoCh = $eReportCard['Template']['SchoolAddressCh'].$separator.$eReportCard['Template']['SchoolTelCh'];
			
			$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
	
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='3' cellspacing='0' align='center'>\n";
					$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'><b>".$eReportCard['Template']['SchoolNameEn']."</b></td></tr>\n";
					$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$eReportCard['Template']['SchoolNameCh']."</td></tr>\n";
						
					$TitleTable .= "<tr><td nowrap='nowrap' class='address_text' align='center'>".$SchoolInfoEn."</td></tr>\n";
					$TitleTable .= "<tr><td nowrap='nowrap' class='address_text' align='center'>".$SchoolInfoCh."</td></tr>\n";
					
					if(!empty($ReportTitle))
					{
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'><b>".$ReportTitleEn."</b></td></tr>\n";
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitleCh."</td></tr>\n";
					}
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			}
			$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal = "XXX";
			$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			$data['DateOfIssue'] = $ReportSetting['Issued'];
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] = $lu->UserName2Lang('ch', 2);
				# change the format to "EnglishName (ChineseName)"
				//$data['Name'] = str_replace(" ", " (", $data['Name']);
				//$data['Name'] = $data['Name'].")";
				
				$data['ClassNo'] = $lu->ClassNumber;
				$data['StudentNo'] = $data['ClassNo'];
				$data['Class'] = $lu->ClassName;
				if (is_array($SettingStudentInfo))
				{
					if (!in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo) && ($lu->ClassNumber != ""))
						$data['Class'] .= " (".$lu->ClassNumber.")";
				}
				else
				{
					if ($lu->ClassNumber != "")
						$data['Class'] .= " (".$lu->ClassNumber.")";
				}
				
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Age'] = $this->Get_Student_Age($data['DateOfBirth'], 1);
				
				$data['Gender'] = $lu->Gender;
				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				$data['RegNo'] = $data['STRN'];
				
				$ClassTeacherAry = $lclass->returnClassTeacher($lu->ClassName);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
			}
			
			# hardcode the 1st six items
			$defaultInfoArray = array("Name", "Gender", "Age", "RegNo", "Class", "ClassNo");
						
			$StudentInfoTable .= "<table width='100%' border='0' cellpadding='3' cellspacing='0' align='center'>";
			if(!empty($defaultInfoArray))
			{
				$count = 0;
				for($i=0; $i<sizeof($defaultInfoArray); $i++)
				{
					$SettingID = trim($defaultInfoArray[$i]);
					$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
					
					if($count % $StudentInfoTableCol==0) 
					{
						$StudentInfoTable .= "<tr>";
						$titleWidth = "110px";
						$contentWidth = "180px";
						$titleAlign = "left";
					}
					else
					{
						$titleWidth = "110px";
						$contentWidth = "100px";
						$titleAlign = "right";
					}
					
					$StudentInfoTable .= "<td class='tabletext' valign='top' height='{$LineHeight}'>";
					
					$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
						$StudentInfoTable .= "<tr>";
						if ($count%$StudentInfoTableCol==0)
						{
							$StudentInfoTable .= "<td width='2'>&nbsp;</td>";
						}
						else
						{
							$StudentInfoTable .= "<td width='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
						}
							$StudentInfoTable .= "<td nowrap width='".$titleWidth."' class='tabletext' align='$titleAlign'>".$Title."</td>";
							$StudentInfoTable .= "<td width='2'>";
					if ($Title != "") $StudentInfoTable .= "&nbsp;";
					$StudentInfoTable .= "</td>";
					
					if ($SettingID == "Name")
					{
						$StudentInfoTable .= "<td width='".$contentWidth."' nowrap class='tabletext' align='center'>";
							$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
								$StudentInfoTable .= "<tr>";
									$StudentInfoTable .= "<td class='tabletext border_bottom' align='center' width='80%' nowrap>";
										$StudentInfoTable .= $lu->EnglishName;
									$StudentInfoTable .= "</td>";
									$StudentInfoTable .= "<td class='tabletext' align='left' width='20%' nowrap>";
										$StudentInfoTable .= "( ".$lu->ChineseName." )";
									$StudentInfoTable .= "</td>";
								$StudentInfoTable .= "</tr>";
							$StudentInfoTable .= "</table>";
					}
					else
					{
						$StudentInfoTable .= "<td width='".$contentWidth."' nowrap class='tabletext border_bottom' align='center'>";
							$StudentInfoTable .= $data[$SettingID] ? $data[$SettingID] : $defaultVal ;
					}
					
					$StudentInfoTable .= "</td></tr></table>\n";
					
					
					$StudentInfoTable .= "</td>";
										
					if(($count+1)%$StudentInfoTableCol==0) {
						$StudentInfoTable .= "</tr>";
					} 
					$count++;
				}
			}
			
			# Display student info selected by the user other than the 1st six items
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if ($SettingID != "RegNo" || $SettingID != "ClassNo")
					{
						$titleWidth = "110px";
					}
					else
					{
						$titleWidth = "90px";
					}
					
					if(in_array($SettingID, $SettingStudentInfo)===true && (!in_array($SettingID, $defaultInfoArray))===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
					
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
						}
						
						$StudentInfoTable .= "<td class='tabletext' width='33%' valign='top' height='{$LineHeight}'>";
						
						$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>
									              <tr>
									              	<td width='2'>&nbsp;</td>
									                <td nowrap width='".$titleWidth."' class='tabletext'>".$Title."</td>
									                <td width='2'>";
						if ($Title != "") $StudentInfoTable .= "&nbsp;";
						$StudentInfoTable .= "</td><td nowrap width='".$contentWidth."' class='tabletext border_bottom'  align='center'>";
						$StudentInfoTable .= $data[$SettingID] ? $data[$SettingID] : $defaultVal ;
						$StudentInfoTable .= "</td></tr></table>\n";
												
						$StudentInfoTable .= "</td>";
											
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						} 
						$count++;
					}
				}				
			}
			
			# Class Position and Class Number of Students
			$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
			$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
			$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
			$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
			$ClassLevelID 				= $ReportSetting['ClassLevelID'];
			$SemID 						= $ReportSetting['Semester'];
	 		$ReportType 				= $SemID == "F" ? "W" : "T";
			$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
			$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
			$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
			# if subject overall column is not shown, grand total, grand average... also cannot be shown
			$ShowRightestColumn = $AllowSubjectTeacherComment;
			
			$CalSetting = $this->LOAD_SETTING("Calculation");
			$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
			
			# retrieve Student Class
			if($StudentID)
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				$lu = new libuser($StudentID);
				$ClassName 		= $lu->ClassName;
				$WebSamsRegNo 	= $lu->WebSamsRegNo;
			}
			
			# retrieve result data
			$result = $this->getReportResultScore($ReportID, 0, $StudentID);
			$GrandTotal = $StudentID ? $result['GrandTotal'] : "S";
			$AverageMark = $StudentID ? $result['GrandAverage'] : "S";
			
			
	 		$FormPosition = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? "--": $result['OrderMeritForm']) : "--") : "#";
	  		$ClassPosition = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? "--": $result['OrderMeritClass']) : "--") : "#";
	  		$ClassNumOfStudent = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? "--": $result['ClassNoOfStudent']) : "--") : "#";
	  		$FormNumOfStudent = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? "--": $result['FormNoOfStudent']) : "--") : "#";
			
			$spacer = "&nbsp;&nbsp;&nbsp;&nbsp;";
	  		$StudentInfoTable .= "<tr>\n";
	  			$StudentInfoTable .= "<td colspan='3'>\n";
	  				$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
	  					if ($ShowOverallPositionClass || $ShowNumOfStudentClass)
	  					{
		  					$StudentInfoTable .= "<tr>\n";
		  						# Class Position
		  						if ($ShowOverallPositionClass)
		  						{
			  						$thisTitle = $eReportCard['Template']['ClassPositionEn']." ".$eReportCard['Template']['ClassPositionCh'];
			  						$StudentInfoTable .= "<td class='tabletext' width='33%' valign='top' height='{$LineHeight}'>";
						
									$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>
												              <tr>
												              	<td width='2'>&nbsp;</td>
												                <td nowrap width='".$titleWidth."' class='tabletext'>".$thisTitle."</td>
												                <td width='2'>";
									if ($thisTitle != "") $StudentInfoTable .= "&nbsp;";
									$StudentInfoTable .= "</td><td nowrap width='180px' class='tabletext border_bottom'  align='center'>";
									$StudentInfoTable .= $ClassPosition;
									$StudentInfoTable .= "</td></tr></table>\n";
															
									$StudentInfoTable .= "</td>";
		  						}
		  						else
		  						{
			  						$StudentInfoTable .= "<td class='tabletext'>&nbsp;</td>\n";
		  						}
		  						$StudentInfoTable .= "<td class='tabletext' width='33%' valign='top' height='{$LineHeight}'>&nbsp;</td>";
		  						# Class number of student
		  						if ($ShowNumOfStudentClass)
		  						{
			  						$thisTitle = $eReportCard['Template']['ClassNumOfStudentEn']." ".$eReportCard['Template']['ClassNumOfStudentCh'];
			  						$StudentInfoTable .= "<td class='tabletext' width='33%' valign='top' height='{$LineHeight}'>";
						
									$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>
												              <tr>
												              	<td width='2'>&nbsp;</td>
												                <td nowrap width='100%' class='tabletext' align='right'>".$thisTitle."</td>
												                <td width='2'>";
									if ($thisTitle != "") $StudentInfoTable .= "&nbsp;";
									$StudentInfoTable .= "</td><td nowrap width='".$contentWidth."' class='tabletext border_bottom'  align='center'>";
									$StudentInfoTable .= $ClassNumOfStudent;
									$StudentInfoTable .= "</td></tr></table>\n";
															
									$StudentInfoTable .= "</td>";
		  						}
		  						else
		  						{
			  						$StudentInfoTable .= "<td class='tabletext'>&nbsp;</td>\n";
		  						}
		  						
	  						$StudentInfoTable .= "</tr>\n";
	  					}
	  					
	  					if ($ShowOverallPositionForm || $ShowNumOfStudentForm)
	  					{
		  					$StudentInfoTable .= "<tr>\n";
		  						# Form Position
		  						if ($ShowOverallPositionForm)
		  						{
			  						$thisTitle = $eReportCard['Template']['FormPositionEn']." ".$eReportCard['Template']['FormPositionCh'];
			  						$StudentInfoTable .= "<td class='tabletext' width='33%' valign='top' height='{$LineHeight}'>";
						
									$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>
												              <tr>
												              	<td width='2'>&nbsp;</td>
												                <td nowrap width='".$titleWidth."' class='tabletext'>".$thisTitle."</td>
												                <td width='2'>";
									if ($thisTitle != "") $StudentInfoTable .= "&nbsp;";
									$StudentInfoTable .= "</td><td nowrap width='180px' class='tabletext border_bottom'  align='center'>";
									$StudentInfoTable .= $FormPosition;
									$StudentInfoTable .= "</td></tr></table>\n";
															
									$StudentInfoTable .= "</td>";
		  						}
		  						else
		  						{
			  						$StudentInfoTable .= "<td class='tabletext'>&nbsp;</td>\n";
		  						}
		  						$StudentInfoTable .= "<td class='tabletext' width='20%' valign='top' height='{$LineHeight}'>&nbsp;</td>";
		  						# Form number of student
		  						if ($ShowNumOfStudentForm)
		  						{
			  						$thisTitle = $eReportCard['Template']['FormNumOfStudentEn']." ".$eReportCard['Template']['FormNumOfStudentCh'];
			  						$StudentInfoTable .= "<td class='tabletext' width='46%' valign='top' height='{$LineHeight}'>";
						
									$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>
												              <tr>
												              	<td width='2'>&nbsp;</td>
												                <td nowrap width='280px' class='tabletext' align='right'>".$thisTitle."</td>
												                <td width='2'>";
									if ($thisTitle != "") $StudentInfoTable .= "&nbsp;";
									$StudentInfoTable .= "</td><td nowrap width='100%' class='tabletext border_bottom'  align='center'>";
									$StudentInfoTable .= $FormNumOfStudent;
									$StudentInfoTable .= "</td></tr></table>\n";
															
									$StudentInfoTable .= "</td>";
		  						}
		  						else
		  						{
			  						$StudentInfoTable .= "<td class='tabletext'>&nbsp;</td>\n";
		  						}
		  						
	  						$StudentInfoTable .= "</tr>\n";
	  					}
	  					
	  				$StudentInfoTable .= "</table>";
	  			$StudentInfoTable .= "</td>\n";
	  		$StudentInfoTable .= "</tr>\n";
	  		
	  		$StudentInfoTable .= $this->Get_Empty_Row($Height=2, $NumOfRow=1);
			
			$StudentInfoTable .= "</table>";
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
		
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID);
						
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='report_border'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
				continue;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				# Subject Teacher Comment
				if ($AllowSubjectTeacherComment) {
					//$css_border_top = $isSub?"":"border_top";
					$css_border_top = "border_top";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>";
					}
					$DetailsTable .= "</span></td>";
				}
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];

		$ColumnData = $this->returnReportTemplateColumnData($ReportID);
		$ColumnWeightMap = array();
		for($i=0; $i<sizeof($ColumnData); $i++) {
			$thisReportColumnID = $ColumnData[$i]["ReportColumnID"];
			
			$OtherCondition = "SubjectID IS NULL AND ReportColumnID = $thisReportColumnID";
			
			$resultArr = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
			$ColumnWeightMap[$thisReportColumnID] = $resultArr[0]["Weight"];
		}
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		
		#########################################################
		############## Marks START
		$row2 = "";
		if($ReportType=="T")	# Terms Report Type
		{
			# Retrieve Invloved Assesment
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if (sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
				
			$e = 0;
			for($i=0;$i<sizeof($ColumnTitle);$i++)
			{
				$thisWeight = $ColumnWeightMap[$ColumnID[$i]] * 100;
				//$ColumnTitleDisplay = convert2unicode($ColumnTitle[$i], 1, 2);
				//$ColumnTitleDisplay = convert2unicode($ColumnTitle[$i], 1, 2)."<br />".$thisWeight."%";
				$ColumnTitleDisplay = $ColumnTitle[$i]."<br />".$thisWeight."%";
				$row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $ColumnTitleDisplay . "</td>";
				$n++;
				$e++;
			}
			$colspan = "colspan='". (sizeof($ColumnTitle)+1) ."'";
			$Rowspan = "";
			$needRowspan++;
			
			$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
			
			if ($FormNumber==7)
				$FirstTermTitle = $eReportCard['Template']['AnnualEn']." ".$eReportCard['Template']['AnnualCh'];
			else
				$FirstTermTitle = $eReportCard['Template']['1stTermEn']." ".$eReportCard['Template']['1stTermCh'];
			$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left small_title' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $FirstTermTitle ."</td>";
		}
		else					# Whole Year Report Type
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
 			$needRowspan=0;
			for($i=0;$i<sizeof($ColumnData);$i++)
			{
				# only retrieve 2nd Term result
				if ($i!=1)
					continue;
					
				$SemName = "<b>".$eReportCard['Template']['2ndTermEn']."</b>"." ".$eReportCard['Template']['2ndTermCh'];
				$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
				if($isDetails==1)
				{
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColumnID = array();
					$ColumnTitle = array();
					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
					
					$ColumnData = $this->returnReportTemplateColumnData($thisReportID);
					$ColumnWeightMap = array();
					for($i=0; $i<sizeof($ColumnData); $i++) {
						$thisReportColumnID = $ColumnData[$i]["ReportColumnID"];
						
						$OtherCondition = "SubjectID IS NULL AND ReportColumnID = $thisReportColumnID";
						
						$resultArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $OtherCondition); 
						$ColumnWeightMap[$thisReportColumnID] = $resultArr[0]["Weight"];
					}
					
					for($j=0;$j<sizeof($ColumnTitle);$j++)
					{
						$thisWeight = $ColumnWeightMap[$ColumnID[$j]] * 100;
						//$ColumnTitleDisplay = convert2unicode($ColumnTitle[$j], 1, 2);
						//$ColumnTitleDisplay = convert2unicode($ColumnTitle[$j], 1, 2)."<br />".$thisWeight."%";
						$ColumnTitleDisplay = $ColumnTitle[$j]."<br />".$thisWeight."%";
						$row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $ColumnTitleDisplay . "</td>";
						$n++;
						$e++;
					}
					$colspan = "colspan='". (sizeof($ColumnTitle)+1) ."'";
					$Rowspan = "";
					$needRowspan++;
					$t[$i] = sizeof($ColumnTitle);
				}
				else
				{
					$colspan = "";
					$Rowspan = "rowspan='2'";
					$e++;
				}
				$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left tabletext' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $SemName ."</td>";
			}
			
			# Annual Year row
			$thisSemTitle = "<b>".$eReportCard['Template']['AnnualEn']."</b> ".$eReportCard['Template']['AnnualCh'];
			$row1 .= "<td {$Rowspan} height='{$LineHeight}' class='border_left tabletext' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%' nowrap>". $thisSemTitle ."</td>";
			
			$thisAssessmentTitle = "<b>".$eReportCard['Template']['AnnualTotalEn']."</b><br />".$eReportCard['Template']['AnnualTotalCh']."<br />100%";
			$row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $thisAssessmentTitle . "</td>";
			$n++;
			$e++;
		}
		############## Marks END
		#########################################################
		$Rowspan = $row2 ? "rowspan='2'" : "";

		if(!$needRowspan)
			$row1 = str_replace("rowspan='2'", "", $row1);
		
		$x = "<tr>";
		# Subject 
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		for($i=0;$i<sizeof($SubjectColAry);$i++)
		{
			$SubjectEng = "&nbsp;&nbsp;<b>".$eReportCard['Template']['SubjectEng']."</b>";
			$SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];
			$SubjectTitle = $SubjectColAry[$i];
			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
			$x .= "<td {$Rowspan}  valign='middle' class='tabletext' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>". $SubjectTitle . "</td>";
			$n++;
		}
		
		if ($ShowSubjectFullMark)
		{
			$FullMarkTitle = "<b>".$eReportCard['Template']['SchemesFullMarkEn']."</b><br />".$eReportCard['Template']['SchemesFullMarkCh'];
			$x .= "<td {$Rowspan} valign='middle' align='center' class='tabletext border_left' height='{$LineHeight}' width='3%'>". $FullMarkTitle . "</td>";
			$n++;
		}
		
		# Marks
		$x .= $row1;
		
		# Subject Overall 
		if ($ShowRightestColumn)
		{
			if ($ReportType=="T")
			{
				$thisTitle = "<b>".$eReportCard['Template']['OverallResultEn']."</b><br />".$eReportCard['Template']['OverallResultCh'];
				$thisTitle .= "<br />"."100%";
			}
			else
			{
				$thisTitle = "<b>".$eReportCard['Template']['AnnualTotalEn']."</b><br />".$eReportCard['Template']['AnnualTotalCh'];
			}
			$row2 .= "<td valign='top' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='border_left reportcard_text border_top' align='center'>". $thisTitle ."</td>";
			$n++;
		}
		else
		{
			//$e--;	
		}
		
		# Subject Teacher Comment
		if ($AllowSubjectTeacherComment) {
			$thisTitle = $eReportCard['Template']['TeachersCommentEn']."<br />".$eReportCard['Template']['TeachersCommentCh'];
			$x .= "<td {$Rowspan} valign='middle' class='border_left small_title' align='center' width='". $eRCTemplateSetting['ColumnWidth']['SubjectTeacherComment'] ."'>".$thisTitle."</td>";
		}
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		//$css_border_top = ($Prefix)? "" : "border_top";
			 		$css_border_top = "border_top";
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
			 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='tabletext'>$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
		}
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			$IssueDate = ($SettingID == "IssueDate" && $Issued)	? "<u>".$Issued."</u>" : "______________";
			$SignatureTable .= "<td valign='bottom' align='center'>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td align='center' class='tabletext' height='130' valign='bottom'>_____". $IssueDate ."_____</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='tabletext' valign='bottom'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		$ShowSubjectFullMark 		= $ReportSetting['ShowSubjectFullMark'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID);
		$GrandTotal = $StudentID ? $result['GrandTotal'] : "S";
		$AverageMark = $StudentID ? $result['GrandAverage'] : "S";
		
 		$FormPosition = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? "--": $result['OrderMeritForm']) : "--") : "#";
  		$ClassPosition = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? "--": $result['OrderMeritClass']) : "--") : "#";
  		$ClassNumOfStudent = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? "--": $result['ClassNoOfStudent']) : "--") : "#";
  		$FormNumOfStudent = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? "--": $result['FormNoOfStudent']) : "--") : "#";
		
		if ($CalOrder == 2) {
			if ($ReportType=="W")
			{
				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
				$numOfColumn = count($ColumnData);
				
				$ColumnCounter = 0;
				foreach($ColumnData as $key=>$columnInfoArr)
				{
					$ColumnCounter++;
					
					# Get the last semester
					if ($ColumnCounter != $numOfColumn)
						continue;
						
					$TermID = $columnInfoArr['SemesterNum'];
					
					# check sems/assesment col#
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					
					$ColumnTitle = $this->returnReportColoumnTitle($thisReportID);
					$ColumnTitle[0] = "Overall";
					foreach ($ColumnTitle as $thisColumnID => $thisColumnName) {
						$thisColumnResult = $this->getReportResultScore($thisReportID, $thisColumnID, $StudentID);
						$columnTotal[$thisColumnID] = $StudentID ? $thisColumnResult['GrandTotal'] : "S";
						$columnAverage[$thisColumnID] = $StudentID ? $thisColumnResult['GrandAverage'] : "S";
						$columnClassPos[$thisColumnID] = $StudentID ? $thisColumnResult['OrderMeritClass'] : "S";
						$columnFormPos[$thisColumnID] = $StudentID ? $thisColumnResult['OrderMeritForm'] : "S";
						$columnClassNumOfStudent[$thisColumnID] = $StudentID ? $thisColumnResult['ClassNoOfStudent'] : "S";
						$columnFormNumOfStudent[$thisColumnID] = $StudentID ? $thisColumnResult['FormNoOfStudent'] : "S";
					}
				}
			}
			else
			{
				foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID);
					$columnTotal[$ColumnID] = $StudentID ? $columnResult['GrandTotal'] : "S";
					$columnAverage[$ColumnID] = $StudentID ? $columnResult['GrandAverage'] : "S";
					$columnClassPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritClass'] : "S";
					$columnFormPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritForm'] : "S";
					$columnClassNumOfStudent[$ColumnID] = $StudentID ? $columnResult['ClassNoOfStudent'] : "S";
					$columnFormNumOfStudent[$ColumnID] = $StudentID ? $columnResult['FormNoOfStudent'] : "S";
				}
			}
		}
		
				
		##################################################################################
		# CSV related
		##################################################################################
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		
		if($SemID=="F")
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$InfoTermID = $TermID;
				
				if(!empty($csvType))
				{
					foreach($csvType as $k=>$Type)
					{
						$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
						if(!empty($csvData)) 
						{
							foreach($csvData as $RegNo=>$data)
							{
								if($RegNo == $WebSamsRegNo)
								{
									foreach($data as $key=>$val)
										$ary[$TermID][$key] = $val;
								}
							}
						}
					}
				}
			}
			
			# calculate sems/assesment col#
			$ColNum2Ary = array();
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				if($d1['IsDetails']==1)
				{
					# check sems/assesment col#
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1;
				}
				else
					$ColNum2Ary[$TermID] = 0;
			}
		}
		else
		{
			$InfoTermID = $SemID;
			if(!empty($csvType))
			{
				foreach($csvType as $k=>$Type)
				{
					$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
					if(!empty($csvData)) 
					{
						foreach($csvData as $RegNo=>$data)
						{
							if($RegNo == $WebSamsRegNo)
							{
								foreach($data as $key=>$val)
									$ary[$SemID][$key] = $val;
							}
						}
					}
				}
			}
		}
		
		# Separator and 2 empty rows
		$x .= $this->Generate_Empty_Row($ReportID, $CalOrder, $ColNum2, $ColumnTitle, $NumOfAssessment, 1);
		$x .= $this->Generate_Empty_Row($ReportID, $CalOrder, $ColNum2, $ColumnTitle, $NumOfAssessment);
		$x .= $this->Generate_Empty_Row($ReportID, $CalOrder, $ColNum2, $ColumnTitle, $NumOfAssessment); 
		
		
		$first = 1;
		# Overall Result
		if($ShowGrandTotal)
		{
			//$border_top = $first ? "border_top" : "";
			
			if ($FormNumber==7)
			{
				$thisTitleEn = $eReportCard['Template']['AnnualTotalEn'];
				$thisTitleCh = $eReportCard['Template']['AnnualTotalCh'];
			}
			else
			{
				$thisTitleEn = $eReportCard['Template']['OverallResultEn'];
				$thisTitleCh = $eReportCard['Template']['OverallResultCh'];
			}
			
			$border_top = "border_top";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'><b>". $thisTitleEn ."</b></td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $thisTitleCh ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			
			if ($ShowSubjectFullMark)
			{
				$thisFullMark = $this->returnGrandTotalFullMark($ReportID, $StudentID, $checkExemption=1, $excludeDisplayGradeSubject=1, $excludeSPFullChecking=1);
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisFullMark."</td>";
			}
			
			if ($CalOrder == 1) {
				for($i=0;$i<$ColNum2;$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			} else {
				$curColumn = 0;
				foreach ($ColumnTitle as $ColumnID => $ColumnName) 
				{
					//for ($i=0; $i<$NumOfAssessment[$curColumn]-1 ; $i++)
					//{
					//	$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					//}
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnTotal[$ColumnID]."&nbsp;</td>";
					$curColumn++;
				}
				
				//if ($ReportType=="W")
				//	$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnTotal[0]."&nbsp;</td>";
			}
			
			//if ($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $GrandTotal ."&nbsp;</td>";
				
			if ($AllowSubjectTeacherComment) {
				# Days Absent
				$thisDisplay = ($ReportType=="W")? $ary[$TermID]["Days Absent"] : $ary[$SemID]["Days Absent"];
				$x .= "<td class='border_left $border_top' valign='top' height='{$LineHeight}'>";
					$x .= "<table border='0' cellpadding='2' cellspacing='0'>";
						$x .= $this->Get_Empty_Row(1);
						$x .= $this->Generate_CSV_Info_Row($eReportCard['Template']['DaysAbsentEn'], $eReportCard['Template']['DaysAbsentCh'], $thisDisplay);
					$x .= "</table>";
				$x .= "</td>";
			}
			$x .= "</tr>";
		}
		
		if ($eRCTemplateSetting['HideCSVInfo'] != true)
		{
			# Average Mark 
			if($ShowGrandAvg)
			{
				//$border_top = $first ? "border_top" : "";
				$border_top = "border_top";
				$first = 0;
				$x .= "<tr>";
				$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
				$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'><b>". $eReportCard['Template']['AvgMarkEn'] ."</b></td>";
				$x .= "</tr></table>";
				$x .= "</td>";		
				$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
				$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['AvgMarkCh'] ."</td>";
				$x .= "</tr></table>";
				$x .= "</td>";
				
				if ($ShowSubjectFullMark)
				{
					$thisFullMark = 100;
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisFullMark."</td>";
				}
			
				if ($CalOrder == 1) {
					for($i=0;$i<$ColNum2;$i++)
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				} else {
					$curColumn = 0;
					foreach ($ColumnTitle as $ColumnID => $ColumnName) {
						//for ($i=0; $i<$NumOfAssessment[$curColumn] - 1; $i++)
						//{
						//	$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
						//}
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnAverage[$ColumnID]."&nbsp;</td>";
						$curColumn++;
					}
					//if ($ReportType=="W")
					//	$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnAverage[0]."&nbsp;</td>";
				}
				
				if ($ShowRightestColumn)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $AverageMark ."&nbsp;</td>";
					
				if ($AllowSubjectTeacherComment) {
					# Times Late
					$thisDisplay = ($ReportType=="W")? $ary[$TermID]["Time Late"] : $ary[$SemID]["Time Late"];
					$x .= "<td class='border_left' valign='top' height='{$LineHeight}'>";
						$x .= "<table border='0' cellpadding='2' cellspacing='0'>";
							$x .= $this->Generate_CSV_Info_Row($eReportCard['Template']['TimesLateEn'], $eReportCard['Template']['TimesLateCh'], $thisDisplay);
							$x .= $this->Get_Empty_Row(1);
						$x .= "</table>";
					$x .= "</td>";
				}
				$x .= "</tr>";
			}
		}
		
		# ECA / Services and Awards
		$NumOfColspan = 3;
		if ($ShowSubjectFullMark) $NumOfColspan++;
		if ($CalOrder == 1) 
		{
			$NumOfColspan += $ColNum2 - 1;
		}
		else
		{
			$curColumn = 0;
			foreach ($ColumnTitle as $ColumnID => $ColumnName) 
			{
				$NumOfColspan += $NumOfAssessment[$curColumn];
				$curColumn++;
			}
			$NumOfColspan += 1;
		}
		if ($ShowRightestColumn) $NumOfColspan++;
		//if ($ReportType=="W") $NumOfColspan++;	// Overall Column of 2nd Term
		
		$x .= "<tr>";
			# ECA / Services and Awards
			$x .= "<td class='tabletext border_top' colspan='$NumOfColspan' valign='top' height='180px'>";
				$x .= "<table border='0' cellpadding='7' cellspacing='0' valign='top' height='100%'>";
					$x .= "<tr>";
						$x .= "<td class='tabletext' valign='top'>";
							$x .= "<table border='0' cellpadding='1' cellspacing='0' valign='top'>";
								$x .= "<tr>";
									$x .= "<td class='tabletext' valign='top'>";
										$x .= "<b>".$eReportCard['Template']['ECA/ServicesAndAwardsEn']."</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$eReportCard['Template']['ECA/ServicesAndAwardsCh'];
									$x .= "</td>";
								$x .= "</tr>";
								$x .= "<tr>";
									$x .= "<td class='tabletext'>";
										$x .= ($ReportType=="W")? $ary[$TermID]["ECA"] : $ary[$SemID]["ECA"];
									$x .= "</td>";
								$x .= "</tr>";
							$x .= "</table>";
						$x .= "</td>";
					$x .= "</tr>";
				$x .= "</table>";
			$x .= "</td>";
			
			if ($AllowSubjectTeacherComment) {
				# Conduct, Responsibility, Application, Neatness
				$x .= "<td class='border_left border_top' valign='top'>";
					$x .= "<table border='0' cellpadding='1' cellspacing='1'>";
					
						if ($ReportType=="T")
						{
							$x .= $this->Get_Empty_Row(5);
						}
						else
						{
							$x .= $this->Get_Empty_Row(2);
						}
						
						# Conduct
						$thisDisplay = ($ReportType=="W")? $ary[$TermID]["Conduct"] : $ary[$SemID]["Conduct"];
						$x .= $this->Generate_CSV_Info_Row($eReportCard['Template']['ConductEn'], $eReportCard['Template']['ConductCh'], $thisDisplay);
						# Responsibility
						$thisDisplay = ($ReportType=="W")? $ary[$TermID]["Responsibility"] : $ary[$SemID]["Responsibility"];
						$x .= $this->Generate_CSV_Info_Row($eReportCard['Template']['ResponsibilityEn'], $eReportCard['Template']['ResponsibilityCh'], $thisDisplay);
						# Application
						$thisDisplay = ($ReportType=="W")? $ary[$TermID]["Application"] : $ary[$SemID]["Application"];
						$x .= $this->Generate_CSV_Info_Row($eReportCard['Template']['ApplicationEn'], $eReportCard['Template']['ApplicationCh'], $thisDisplay);
						# Neatness
						$thisDisplay = ($ReportType=="W")? $ary[$TermID]["Neatness"] : $ary[$SemID]["Neatness"];
						$x .= $this->Generate_CSV_Info_Row($eReportCard['Template']['NeatnessEn'], $eReportCard['Template']['NeatnessCh'], $thisDisplay);
						
						$x .= $this->Generate_CSV_Info_Empty_Row();
						
						if ($ReportType=="W")
						{
							# Promotion Status
							$promotionStatus = $ary[$TermID]["Promoted To"];
							$retainStatus = $ary[$TermID]["Retained In"];
							
							if ($promotionStatus)
							{
								$thisTitleEn = $eReportCard['Template']['PromotionToEn'];
								$thisTitleCh = $eReportCard['Template']['PromotionToCh'];
								$thisDisplay = $promotionStatus;
							}
							else
							{
								$thisTitleEn = $eReportCard['Template']['RetainedInEn'];
								$thisTitleCh = $eReportCard['Template']['RetainedInCh'];
								$thisDisplay = $retainStatus;
							}
							
							$x .= $this->Generate_CSV_Info_Row($thisTitleEn, $thisTitleCh, $thisDisplay);
							$x .= $this->Get_Empty_Row(1);
						}
						
					$x .= "</table>";
				$x .= "</td>";
			}
			$x .= "</tr>";
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eReportCard;
				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		//$ShowRightestColumn = $ShowSubjectOverall;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		if($ReportType=="T")	# Temrs Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
							
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# define css
				//$css_border_top = ($isSub)? "" : "border_top";
				$css_border_top = "border_top";
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				$isAllNA = true;
				
				# Subject Full Mark
				if($ShowSubjectFullMark)
				{
  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
  					//$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
  					$thisFullMark = $SubjectFullMarkAry[$SubjectID];
					$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center'>". $thisFullMark ."&nbsp;</td>";
				}
				
				# Assessment Marks & Display
				for($i=0;$i<sizeof($ColumnID);$i++)
				{
					$thisColumnID = $ColumnID[$i];
					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
					
					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
					$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);

					if ($isSub && $columnSubjectWeightTemp == 0)
					{
						$thisMarkDisplay = $this->EmptySymbol;
						$colspan = "colspan='2'";
					}
					//else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
					//	$thisMarkDisplay = $this->EmptySymbol;
					//	$colspan = "colspan='2'";
					//} 
					else 
					{
						
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnID[$i]][Mark] : "";
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][$ColumnID[$i]][Grade] : "";
						
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
						
						if (substr($thisMark, 0, 4) != "N.A.")
						{
							$isAllNA = false;
						}
						
						if (in_array($thisMark, $this->specialCasesSet1) || in_array($thisMark, $this->specialCasesSet2))
						{
							$colspan = "colspan='2'";
							$thisMark = $this->EmptySymbol;
						}
						else if ($ScaleDisplay=="G")
						{
							$colspan = "colspan='2'";
						}
						else
						{
							$colspan = "";
						}
						
						if($needStyle)
						{
							if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
							{
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							}
							else
							{
								$thisMarkTemp = $thisMark;
							}
							
							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp,'','',$ReportID);
							$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
						
					//	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					//	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
					//	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark; 
					}
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' $colspan align='center' width='50%'>". $thisMarkDisplay ."</td>";
  					
  					/*
  					if($ShowSubjectFullMark)
  					{
	  					# check special full mark
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
	  					
						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
					}
					*/
					
					if ($colspan == "")
					{
						$ConvertedGrade = "";
						if ($thisGrade=="") {
							$ConvertedGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark);
						} else {
							$ConvertedGrade = $thisGrade;
						}
						
						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='30%'>";
	  					
						if($ConvertedGrade !== "")
	  					{
							if($needStyle)
							{
		  						$thisMarkDisplay = $this->ReturnTextwithStyle($ConvertedGrade, 'HighLight', $thisNature);
	  						}
	  						else
	  						{
		  						$thisMarkDisplay = $ConvertedGrade;
	  						}
							$x[$SubjectID] .= $thisMarkDisplay;
						}
						
						$x[$SubjectID] .= "</td>";
					}
					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}
								
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall)
				{
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][0][Grade] : ""; 
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
										
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					
					if (substr($thisMark, 0, 4) != "N.A.")
					{
						$isAllNA = false;
					}
					
					if (in_array($thisMark, $this->specialCasesSet1) || in_array($thisMark, $this->specialCasesSet2))
					{
						$colspan = "colspan='2'";
						$thisMark = $this->EmptySymbol;
					}
					else if ($ScaleDisplay=="G")
					{
						$colspan = "colspan='2'";
					}
					else
					{
						$colspan = "";
					}
					
					if($needStyle)
					{
 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
						else
							$thisMarkTemp = $thisMark;
 						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp,'','',$ReportID);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>". $thisMarkDisplay ."</td>";
  					
  					/*
  					if($ShowSubjectFullMark)
  					{
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $thisFullMark .")</td>";
					}
					*/
					
					if ($colspan == "")
					{
						$ConvertedGrade = "";
						if ($thisGrade=="") {
							$ConvertedGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark);
						} else {
							$ConvertedGrade = $thisGrade;
						}
						
						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='30%'>";
	  					
						if($ConvertedGrade !== "" && $ScaleDisplay=='M')
	  					{
							if($needStyle)
							{
		  						$thisMarkDisplay = $this->ReturnTextwithStyle($ConvertedGrade, 'HighLight', $thisNature);
	  						}
	  						else
	  						{
		  						$thisMarkDisplay = $ConvertedGrade;
	  						}
							$x[$SubjectID] .= $thisMarkDisplay;
						}
					}
					
					$x[$SubjectID] .= "</td>";
								
					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
				}
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
							
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				$isAllNA = true;
				
				# Subject Full Mark
				if($ShowSubjectFullMark)
				{
  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
  					//$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
  					$thisFullMark = $SubjectFullMarkAry[$SubjectID];
					$x[$SubjectID] .= "<td class='border_left border_top tabletext' align='center'>". $thisFullMark ."&nbsp;</td>";
				}
				
				# Terms's Assesment / Terms Result
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					# show 2nd term result only
					if ($i!=1)
						continue;
					
					#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
					//$css_border_top = $isSub? "" : "border_top";
					$css_border_top = "border_top";
					
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					if($isDetails==1)		# Retrieve assesments' marks
					{
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum'] ."' and ClassLevelID = '".$ClassLevelID."' ");
						
						# if no term reports, CANNOT retrieve assessment result at all
						if (empty($thisReport)) {
							for($j=0;$j<sizeof($ColumnID);$j++) {
								$x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
							}
						} else {
							$thisReportID = $thisReport['ReportID'];
							$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
							$ColumnID = array();
							$ColumnTitle = array();
							foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
							
							$thisMarksAry = $this->getMarks($thisReportID, $StudentID);		
								 
							for($j=0;$j<sizeof($ColumnID);$j++)
							{
								$thisColumnID = $ColumnID[$i];
								$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
								$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
								$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
								
								$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
								$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
					
								if ($isSub && $columnSubjectWeightTemp == 0)
								{
									$colspan = "colspan='2'";
									$thisMarkDisplay = "---";
								}
								//else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
								//	$colspan = "colspan='2'";
								//	$thisMarkDisplay = "---";
								//} 
								else {
									$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnID[$j] ."' and SubjectID = '$SubjectID' ");
									$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
									
									$thisMark = $ScaleDisplay=="M" ? $thisMarksAry[$SubjectID][$ColumnID[$j]][Mark] : "";
									$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $thisMarksAry[$SubjectID][$ColumnID[$j]][Grade] : ""; 
									
									# for preview purpose
									if(!$StudentID)
									{
										$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
										$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
									}
									$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
													
									if ($thisMark != "N.A.")
									{
										$isAllNA = false;
									}
									
									# check special case
									list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
									
									if (in_array($thisMark, $this->specialCasesSet1) || in_array($thisMark, $this->specialCasesSet2))
									{
										$colspan = "colspan='2'";
										$thisMark = $this->EmptySymbol;
									}
									else if ($ScaleDisplay=="G")
									{
										$colspan = "colspan='2'";
									}
									else
									{
										$colspan = "";
									}
									
									if($needStyle)
									{
										if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
										{
											$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
										}
										else
										{
											$thisMarkTemp = $thisMark;
										}
										$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp,'','',$thisReportID);
										$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
									}
									else
									{
										$thisMarkDisplay = $thisMark;
									}
								}
									
								$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
								$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
			  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
			  					
			  					# Display Grade
			  					if ($colspan == "")
			  					{
				  					$ConvertedGrade = "";
									if ($thisGrade=="") {
										$ConvertedGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark);
									} else {
										$ConvertedGrade = $thisGrade;
									}
									
									$x[$SubjectID] .= "<td class=' tabletext' align='center' width='30%'>";
				  					
									if($ConvertedGrade !== "")
				  					{
										if($needStyle)
										{
					  						$thisMarkDisplay = $this->ReturnTextwithStyle($ConvertedGrade, 'HighLight', $thisNature);
				  						}
				  						else
				  						{
					  						$thisMarkDisplay = $ConvertedGrade;
				  						}
										$x[$SubjectID] .= $thisMarkDisplay;
									}
									
									$x[$SubjectID] .= "</td>";
			  					}
								
								$x[$SubjectID] .= "</tr></table>";
								$x[$SubjectID] .= "</td>";
							}
							
							# Retrieve Terms Overall marks
							if ($isParentSubject && $CalculationOrder == 1) {
								$thisMarkDisplay = $this->EmptySymbol;
							} else {
								# See if any term reports available
								$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
								
								# if no term reports, the term mark should be entered directly
								if (empty($thisReport)) {
									$thisReportID = $ReportID;
									$MarksAry = $this->getMarks($thisReportID, $StudentID);
									$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
									#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
									$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
								} else {
									$thisReportID = $thisReport['ReportID'];
									$MarksAry = $this->getMarks($thisReportID, $StudentID);
									$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
									#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
									$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
								}
		 						
								$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
								$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
															
								# for preview purpose
								if(!$StudentID)
								{
									$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
									$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
								}
								# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
								$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
								
								if ($thisMark != "N.A.")
								{
									$isAllNA = false;
								}
							
								# check special case
								list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
								
								if (in_array($thisMark, $this->specialCasesSet1) || in_array($thisMark, $this->specialCasesSet2))
								{
									$colspan = "colspan='2'";
									$thisMark = $this->EmptySymbol;
								}
								else if ($ScaleDisplay=="G")
								{
									$colspan = "colspan='2'";
								}
								else
								{
									$colspan = "";
								}
									
								if($needStyle)
								{
									$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$thisReportID);
		 							//$thisMark = ($ScaleDisplay=="M" && $StudentID) ? my_round($thisMark*$thisSubjectWeight,2) : $thisMark;
									$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
								}
								else
								{
									$thisMarkDisplay = $thisMark;
								}
							}
								
							$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
							$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
		  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
		  					
		  					if ($colspan=="")
		  					{
			  					$ConvertedGrade = "";
								if ($thisGrade=="") {
									$ConvertedGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark);
								} else {
									$ConvertedGrade = $thisGrade;
								}
								
								$x[$SubjectID] .= "<td class=' tabletext' align='center' width='30%'>";
			  					
								if($ConvertedGrade !== "")
			  					{
									if($needStyle)
									{
				  						$thisMarkDisplay = $this->ReturnTextwithStyle($ConvertedGrade, 'HighLight', $thisNature);
			  						}
			  						else
			  						{
				  						$thisMarkDisplay = $ConvertedGrade;
			  						}
									$x[$SubjectID] .= $thisMarkDisplay;
								}
								
								$x[$SubjectID] .= "</td>";
		  					}
							
							$x[$SubjectID] .= "</tr></table>";
							$x[$SubjectID] .= "</td>";
						}
					}
					else					# Retrieve Terms Overall marks
					{
						if ($isParentSubject && $CalculationOrder == 1) {
							$thisMarkDisplay = $this->EmptySymbol;
						} else {
							# See if any term reports available
							$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
							
							# if no term reports, the term mark should be entered directly
							if (empty($thisReport)) {
								$thisReportID = $ReportID;
								$MarksAry = $this->getMarks($thisReportID, $StudentID);
								$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
								$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
							} else {
								$thisReportID = $thisReport['ReportID'];
								$MarksAry = $this->getMarks($thisReportID, $StudentID);
								$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
								$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
							}
	 						
							$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
							$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
														
							# for preview purpose
							if(!$StudentID)
							{
								$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
								$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
							}
							# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
							$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
							
							if ($thisMark != "N.A.")
							{
								$isAllNA = false;
							}
						
							# check special case
							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
							
							if (in_array($thisMark, $this->specialCasesSet1) || in_array($thisMark, $this->specialCasesSet2))
							{
								$colspan = "colspan='2'";
								$thisMark = $this->EmptySymbol;
							}
							else if ($ScaleDisplay=="G")
							{
								$colspan = "colspan='2'";
							}
							else
							{
								$colspan = "";
							}
								
							if($needStyle)
							{
								$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$thisReportID);
	 							//$thisMark = ($ScaleDisplay=="M" && $StudentID) ? my_round($thisMark*$thisSubjectWeight,2) : $thisMark;
								$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
							}
						}
							
						$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
	  					
	  					if ($colspan=="")
	  					{
		  					$ConvertedGrade = "";
							if ($thisGrade=="") {
								$ConvertedGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark);
							} else {
								$ConvertedGrade = $thisGrade;
							}
							
							$x[$SubjectID] .= "<td class=' tabletext' align='center' width='30%'>";
		  					
							if($ConvertedGrade !== "")
		  					{
								if($needStyle)
								{
			  						$thisMarkDisplay = $this->ReturnTextwithStyle($ConvertedGrade, 'HighLight', $thisNature);
		  						}
		  						else
		  						{
			  						$thisMarkDisplay = $ConvertedGrade;
		  						}
								$x[$SubjectID] .= $thisMarkDisplay;
							}
							
							$x[$SubjectID] .= "</td>";
	  					}
						
						$x[$SubjectID] .= "</tr></table>";
						$x[$SubjectID] .= "</td>";
					}
				}
				
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall)
				{
					$MarksAry = $this->getMarks($ReportID, $StudentID);		
					
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][0][Grade] : ""; 
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$ReportID);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					
  					$ConvertedGrade = "";
					if ($thisGrade=="") {
						$ConvertedGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark);
					} else {
						$ConvertedGrade = $thisGrade;
					}
					
					$x[$SubjectID] .= "<td class=' tabletext' align='center' width='30%'>";
  					
					if($ConvertedGrade !== "")
  					{
						if($needStyle)
						{
	  						$thisMarkDisplay = $this->ReturnTextwithStyle($ConvertedGrade, 'HighLight', $thisNature);
  						}
  						else
  						{
	  						$thisMarkDisplay = $ConvertedGrade;
  						}
						$x[$SubjectID] .= $thisMarkDisplay;
					}
					
					$x[$SubjectID] .= "</td>";
  					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}
				
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		}	# End Whole Year Report Type
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
		
		if ($eRCTemplateSetting['HideCSVInfo'] == true)
		{
			return "";
		}
			
		
		# Retrieve Basic Information of Report
		$BasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$AllowClassTeacherComment = $BasicInfo['AllowClassTeacherComment'];
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

		/*
		# retrieve the latest Term
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		foreach($sems as $TermID=>$TermName)
			$latestTerm = $TermID;
		$latestTerm++;
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		foreach($csvType as $k=>$Type)
		{
			$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);	
			if(!empty($csvData)) 
			{
				foreach($csvData as $RegNo=>$data)
				{
					if($RegNo == $WebSamsRegNo)
					{
	 					foreach($data as $key=>$val)
		 					$ary[$key] = $val;
					}
				}
			}
		}
		
		# retrieve result data
		$Absence = ($ary['Days Absent'])? $ary['Days Absent'] : 0;
		$Lateness = ($ary['Time Late'])? $ary['Time Late'] : 0;
		$EarlyLeave = ($ary['Early Leave'])? $ary['Early Leave'] : 0;
		$Promotion = ($ary['Promotion'])? $ary['Promotion'] : $this->EmptySymbol;
		$Merits = ($ary['Merits'])? $ary['Merits'] : 0;
		$MinorCredit = ($ary['Minor Credit'])? $ary['Minor Credit'] : 0;
		$MajorCredit = ($ary['Major Credit'])? $ary['Major Credit'] : 0;
		$Demerits = ($ary['Merits'])? $ary['Demerits'] : 0;
		$MinorFault = ($ary['Minor Fault'])? $ary['Minor Fault'] : 0;
		$MajorFault = ($ary['Major Fault'])? $ary['Major Fault'] : 0;
		$Remark = ($ary['Remark'])? $ary['Remark'] : $this->EmptySymbol;
		$ECA = ($ary['ECA'])? $ary['ECA'] : $this->EmptySymbol;
		if (is_array($ECA))
			$ecaList = implode("<br>", $ECA);
		else
			$ecaList = $ECA;
		*/
		
		$merit = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		$merit .= "<tr>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['Merits'] . ": " . ($ary['Merits'] ? $ary['Merits'] :0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['Demerits'] . ": " . ($ary['Demerits'] ? $ary['Demerits'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorCredit'] . ": " . ($ary['Minor Credit'] ? $ary['Minor Credit'] :0) ."</td>";
		$merit .= "</tr>";
		$merit .= "<tr>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorCredit'] . ": " . ($ary['Major Credit'] ? $ary['Major Credit'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorFault'] . ": " . ($ary['Minor Fault'] ? $ary['Minor Fault'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorFault'] . ": " . ($ary['Major Fault'] ? $ary['Major Fault'] : 0) ."</td>";
		$merit .= "</tr>";
		$merit .= "</table>";
		
		$remark = $ary['Remark'];
		
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$classteachercomment = $CommentAry[0];
		
		$eca = $ary['ECA'];
		if (is_array($eca))
			$ecaList = implode("<br>", $eca);
		else
			$ecaList = $eca;
		
		$x = "";
		if($AllowClassTeacherComment)
		{
			# Class Teacher Comment 
			$x .= $this->Get_Empty_Row($Height=5, $NumOfRow=1);
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
				$x .= "<tr>";
					$x .= "<td class='tabletext'>";
						$x .= "<b>".$eReportCard['Template']['ClassTeacherCommentEn']."</b> &nbsp;&nbsp;&nbsp;&nbsp;".$eReportCard['Template']['ClassTeacherCommentCh'];
						$x .= "<br>";
						$x .= str_replace("\n", "<br />", stripslashes($classteachercomment));
					$x .= "</td>";
				$x .= "</tr>";
			$x .= "</table>";
		}
		
		return $x;
	}
	
	########### END Template Related
	
	function Generate_Empty_Row($ReportID, $CalOrder, $ColNum2, $ColumnTitle, $NumOfAssessment, $withStar=0)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$ShowSubjectFullMark 		= $ReportSetting['ShowSubjectFullMark'];
		
		//$ShowRightestColumn = $ShowSubjectOverall;
		
		$border_top = "border_top";
		
		$x = "";
		
		$x .= "<tr>";
		$thisContent = ($withStar)? "&nbsp;&nbsp;* * * * * * * * * * * * * * * * * * * * * * *" : "&nbsp";
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}' colspan='2'>".$thisContent."</td>";
		
		if ($ShowSubjectFullMark)
		{
			$thisContent = ($withStar)? "* * * *" : "&nbsp";
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisContent."</td>";
		}
		
		if ($CalOrder == 1) 
		{
			$thisContent = ($withStar)? "* * * *" : "&nbsp";
			for($i=0;$i<$ColNum2;$i++)
			{
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisContent."</td>";
			}
		} else {
			$curColumn = 0;
			$thisContent = ($withStar)? "* * * *" : "&nbsp";
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				
				//for ($i=0; $i<$NumOfAssessment[$curColumn]-1 ; $i++)
				//{
				//	$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisContent."</td>";
				//}
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisContent."</td>";
				$curColumn++;
			}
			
			//if ($ReportType == "W")
			//	$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisContent."</td>";
		}
		
		if ($ShowSubjectOverall)
		{
			$thisContent = ($withStar)? "* * * *" : "&nbsp";
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisContent."</td>";
		}
			
		if ($AllowSubjectTeacherComment) {
			$thisContent = ($withStar)? "* * * *" : "&nbsp";
			$x .= "<td class='tabletext border_left $border_top' align='center'>".$thisContent."</td>";
		}
		
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_CSV_Info_Row($TitleEn, $TitleCh, $Value)
	{
		$x = "";
		
		if ($Value == '')
			$Value = '&nbsp;';
		
		$x .= "<tr>";
			$x .= "<td class='tabletext' width='1px'>&nbsp;</td>";
			$x .= "<td class='font_11px' width='45%' valign='top'><b>".$TitleEn."</b></td>";
			$x .= "<td class='tabletext' width='1px'>&nbsp;</td>";
			$x .= "<td class='tabletext' width='25%' valign='top'>".$TitleCh."</td>";
			$x .= "<td class='tabletext' width='1px'>&nbsp;</td>";
			$x .= "<td class='tabletext border_bottom' width='150px' valign='top' align='right'>".$Value."</td>";
			$x .= "<td class='tabletext' width='5px'>&nbsp;</td>"; 
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_CSV_Info_Empty_Row()
	{
		$x = "";
		
		$x .= "<tr>";
			$x .= "<td colspan='7'>&nbsp;</td>";
		$x .= "</tr>";
		
		return $x;
	}

}
?>