<?php
# Editing by 

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/li_sing_tai_hang.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary","attendance", "merit","schoolservice");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 0;
		$this->IsEnableMarksheetExtraInfo = 1;
		
		$this->EmptySymbol = "-";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard;
		
		$AddressTable = $this->genAddressTable();
		
		$x = "";
		$x .= "<tr><td>";
		$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='bg' align='center'>";
		$x .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$x .= "<tr><td height='1'>&nbsp;</td></tr>";
		$x .= "<tr><td>".$MSTable."</td></tr>";
		$x .= "<tr><td>".$MiscTable."</td></tr>";
		$x .= "<tr><td>".$SignatureTable."</td></tr>";
		$x .= "<tr><td width='90%' align='center'>".$AddressTable."</td></tr>";
		$x .= $FooterRow;
		$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		# Report Header is included in getReportStudentInfo() for this school
		/* 
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
			# get school badge
			$SchoolLogo = GET_SCHOOL_BADGE();
				
			# get school name
			$SchoolName = GET_SCHOOL_NAME();	
			
			$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
	
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
					if(!empty($SchoolName))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$SchoolName."</td></tr>\n";
					if(!empty($ReportTitle))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle."</td></tr>\n";
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			}
			$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
		*/
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			$SemID = $ReportSetting['Semester'];
			
			
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
			# get school badge
			//$SchoolLogo = GET_SCHOOL_BADGE();
			$SchoolLogo = $image_path."/file/reportcard2008/templates/li_sang_tai_hang_logo.jpg";
			
			# retrieve required variables
			$defaultVal = "XXX";
			$data['Year'] = $this->GET_ACTIVE_YEAR("-");
			$data['Term'] = $this->returnSemesters($SemID);
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['EnglishName'] = $lu->EnglishName;
				$data['ChineseName'] = $lu->ChineseName;

				$data['ClassNo'] = $lu->ClassNumber+0;
				$data['StudentNo'] = $data['ClassNo']+0;
				$data['Class'] = $lu->ClassName;
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] = $lu->Gender;
				$data['STRN'] = $lu->STRN;
				
				$ClassTeacherAry = $lclass->returnClassTeacher($lu->ClassName);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
			}			
			$data['DateOfIssue'] = $ReportSetting['Issued'];
			
			# hardcode the 1st 3 rows items
			$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
			$StudentInfoTable .= "<tr><td height='1' colspan='3'>&nbsp;</td></tr>";
			
			# 1st row (Year, Term)
			$StudentInfoTable .= "<tr>";
			# Year
			$SettingID = "Year";
			$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
			$StudentInfoTable .= "<td class='student_info_title' width='33%' valign='top' height='{$LineHeight}'>".$Title." : ";
			$StudentInfoTable .= "<u>".($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</u></td>";
			
			$StudentInfoTable .= "<td>&nbsp;</td>";
			
			# Term
			$SettingID = "Term";
			$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
			$StudentInfoTable .= "<td class='student_info_title' width='34%' valign='top' height='{$LineHeight}' align='right'>".$Title." : ";
			$StudentInfoTable .= "<u>".($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</u></td>";
			$StudentInfoTable .= "</tr>\n";
			
			$StudentInfoTable .= "<tr><td height='1' colspan='3'>&nbsp;</td></tr>";
			
			# 2nd Row (Name)
			$StudentInfoTable .= "<tr><td colspan='3'>";
			
			$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
			$StudentInfoTable .= "<tr>";
			$SettingID = "Name";
			$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
			$StudentInfoTable .= "<td class='student_info_title' width='75' valign='top' height='{$LineHeight}' nowrap>".$Title." : </td>";
			$StudentInfoTable .= "<td class='tableline student_info_title' width=\"99%\" height='{$LineHeight}'>&nbsp;".($data['EnglishName'] ? $data['EnglishName'] : $defaultVal )."</td>";
			$StudentInfoTable .= "<td class='student_info_title' width='10%' valign='top' height='{$LineHeight}' nowrap>( ".($data['ChineseName'] ? $data['ChineseName'] : $defaultVal )." )</td>";
			$StudentInfoTable .= "</tr>\n";
			$StudentInfoTable .= "</table>";
			$StudentInfoTable .= "</td></tr>";
			
			$StudentInfoTable .= "<tr><td height='1' colspan='3'>&nbsp;</td></tr>";
			
			# 3rd Row (Class, Class No., STRN)
			$StudentInfoTable .= "<tr>";
			# Class
			$SettingID = "Class";
			$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
			$StudentInfoTable .= "<td class='student_info_title' width='33%' valign='top' height='{$LineHeight}'>".$Title." : ";
			$StudentInfoTable .= "<u>".($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</u></td>";
			
			# Class No.
			$SettingID = "ClassNo";
			$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
			$StudentInfoTable .= "<td class='student_info_title' width='33%' valign='top' height='{$LineHeight}'>".$Title." : ";
			$StudentInfoTable .= "<u>".($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</u></td>";
			
			# STRN
			$SettingID = "STRN";
			$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
			$StudentInfoTable .= "<td class='student_info_title' width='34%' valign='top' height='{$LineHeight}' align='right'>".$Title." : ";
			$StudentInfoTable .= "<u>".($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</u></td>";
			$StudentInfoTable .= "</tr>\n";
			
			$defaultInfoArray = array("Name", "Class", "ClassNo", "STRN");
			
				
			# Display student info selected by the user other than the 1st 3 rows
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if(in_array($SettingID, $SettingStudentInfo)===true && (!in_array($SettingID, $defaultInfoArray))===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
						
						$align = "";
						if(($count+1)%$StudentInfoTableCol==0){
							$align = "align='right'";
						}
					
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr><td height='1' colspan='3'>&nbsp;</td></tr>";
							$StudentInfoTable .= "<tr>";
						}
						
						$StudentInfoTable .= "<td class='student_info_title' width='33%' valign='top' height='{$LineHeight}' {$align}>".$Title." : ";
						$StudentInfoTable .= "<u>".($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</u></td>";
											
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>\n";
						} 
						$count++;
					}
				}				
			}
			
			$StudentInfoTable .= "</table>\n";
		}
		
		$HeaderTable = "";
		$HeaderTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
		$HeaderTable .= "<tr>";
		$HeaderTable .= "<td rowspan='2' width='200'><img height='140' src='$SchoolLogo'></td>";
		$HeaderTable .= "<td class='report_title'>".$ReportTitle."</td>";
		$HeaderTable .= "</tr>";
		$HeaderTable .= "<tr>";
		$HeaderTable .= "<td align='center'>".$StudentInfoTable."</td>";
		$HeaderTable .= "</tr>";
		$HeaderTable .= "</table>";
		
		return $HeaderTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
				
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID);
						
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='report_border'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				# Subject Teacher Comment
				if ($AllowSubjectTeacherComment) {
					$css_border_top = $isSub?"":"border_top";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
					}
					$DetailsTable .= "</span></td>";
				}
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
		$DetailsTable .= "</table>";
		
		# Footer Remarks
		$width = $eRCTemplateSetting['ColumnWidth']['Subject'] * 2;
		$width .= "%";
		
		# 李陞大坑學校 - ERC -- Conduct Mark [CRM Ref No.: 2009-0304-1225]
		# separate calculation of P1-4 and P5-6
		/*
		if ($this->GET_FORM_NUMBER($ClassLevelID) < 5)
		{
			# P1-4
			$spacer = "&nbsp;&nbsp;";
			
			$marksDisplay = "A: 100-93";
			$marksDisplay .= $spacer;
			$marksDisplay .= "A-: 92-85";
			$marksDisplay .= $spacer;
			$marksDisplay .= "B+: 84-82";
			$marksDisplay .= $spacer;
			$marksDisplay .= "B: 81-79";
			$marksDisplay .= $spacer;
			$marksDisplay .= "B-: 78-75";
			$marksDisplay .= $spacer;
			$marksDisplay .= "C+: 74-70";
			$marksDisplay .= $spacer;
			$marksDisplay .= "C: 69-65";
			$marksDisplay .= $spacer;
			$marksDisplay .= "C-: 64-60";
			$marksDisplay .= "<br />";
			$marksDisplay .= "D+: 59-54";
			$marksDisplay .= $spacer;
			$marksDisplay .= "D: 53-47";
			$marksDisplay .= $spacer;
			$marksDisplay .= "D-: 46-40";
			$marksDisplay .= $spacer;
			$marksDisplay .= "E: 39-0";
		}
		else
		{
			# P5-6
			$spacer = "&nbsp;&nbsp;&nbsp;&nbsp;";
			
			$marksDisplay = "A: 100-85";
			$marksDisplay .= $spacer;
			$marksDisplay .= "B: 84-75";
			$marksDisplay .= $spacer;
			$marksDisplay .= "C: 74-60";
			$marksDisplay .= $spacer;
			$marksDisplay .= "D: 59-40";
			$marksDisplay .= $spacer;
			$marksDisplay .= "E: 39-0";
		}
		*/
		
		# 李陞大坑學校 - eRc --- correct conduct mark remarks [CRM Ref No.: 2009-0306-1201]
		# Correct back to uniform conduct mark calculation
		$spacer = "&nbsp;&nbsp;&nbsp;&nbsp;";
		$marksDisplay = "A: 100-85";
		$marksDisplay .= $spacer;
		$marksDisplay .= "B: 84-75";
		$marksDisplay .= $spacer;
		$marksDisplay .= "C: 74-60";
		$marksDisplay .= $spacer;
		$marksDisplay .= "D: 59-30";
		$marksDisplay .= $spacer;
		$marksDisplay .= "E: 29-0";
		
		$DetailsTable .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
		$DetailsTable .= "<tr>";
			$DetailsTable .= "<td class='ms_remarks_text' width='$width' valign='top'>".$eReportCard['Template']['Remark'].": </td>";
			$DetailsTable .= "<td class='ms_remarks_text' width='80px' valign='top'>".$eReportCard['Template']['GradeRemarks']."</td>";
			$DetailsTable .= "<td class='ms_remarks_text' valign='top'>".$marksDisplay."</td>";
		$DetailsTable .= "</tr>";
		$DetailsTable .= "<tr>";
			$DetailsTable .= "<td class='ms_remarks_text'>"."&nbsp;"."</td>";
			$DetailsTable .= "<td class='ms_remarks_text' colspan='2'>".$eReportCard['Template']['PassingMarkRemarks']."</td>";
		$DetailsTable .= "</tr>";
		$DetailsTable .= "</table>";
		
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		#########################################################
		############## Marks START
		$row2 = "";
		if($ReportType=="T")	# Terms Report Type
		{
			# Retrieve Invloved Assesment
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if (sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
			$e = 0;
			for($i=0;$i<sizeof($ColumnTitle);$i++)
			{
				$ColumnTitleDisplay = (convert2unicode($ColumnTitle[$i], 1, 2));
				$ColumnTitleDisplay = $this->ADD_CHINESE_TITLE($ColumnTitleDisplay);
				$row1 .= "<td valign='middle' height='{$LineHeight}' class='border_left tabletext' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $ColumnTitleDisplay . "</td>";
				$n++;
 				$e++;
			}
		}
		else					# Whole Year Report Type
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
 			$needRowspan=0;
			for($i=0;$i<sizeof($ColumnData);$i++)
			{
				$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
				$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
				if($isDetails==1)
				{
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColumnID = array();
					$ColumnTitle = array();
					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
					for($j=0;$j<sizeof($ColumnTitle);$j++)
					{
						$ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$j], 1, 2));
						$ColumnTitleDisplay = $this->ADD_CHINESE_TITLE($ColumnTitleDisplay);
						$row2 .= "<td class='border_left border_top tabletext' align='center' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $ColumnTitleDisplay . "</td>";
						$n++;
						$e++;
					}
					$colspan = "colspan='". sizeof($ColumnTitle) ."'";
					$Rowspan = "";
					$needRowspan++;
					$t[$i] = sizeof($ColumnTitle);
				}
				else
				{
					$colspan = "";
					$Rowspan = "rowspan='2'";
					$e++;
				}
				$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left tabletext' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $SemName ."</td>";
			}
		}
		############## Marks END
		#########################################################
		$Rowspan = $row2 ? "rowspan='2'" : "";

		if(!$needRowspan)
			$row1 = str_replace("rowspan='2'", "", $row1);
		
		$x = "<tr>";
		# Subject 
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		for($i=0;$i<sizeof($SubjectColAry);$i++)
		{
			$SubjectEng = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'];
			$SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];
			$SubjectTitle = $SubjectColAry[$i];
			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
			$x .= "<td {$Rowspan}  valign='middle' class='tabletext' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>". $SubjectTitle . "</td>";
			$n++;
		}
		
		# Full Mark
		if ($ShowSubjectFullMark)
		{
			$FullMarkTitle = $eReportCard['Template']['SchemesFullMarkEn']."<br />".$eReportCard['Template']['SchemesFullMarkCh'];
			$x .= "<td {$Rowspan} valign='middle' align='center' class='border_left tabletext' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['FullMark'] ."'>". $FullMarkTitle . "</td>";
		}
		
		# Marks
		$x .= $row1;
		
		# Subject Overall 
		if($ShowRightestColumn)
		{
			$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Overall'] ."' class='border_left tabletext' align='center'>". $eReportCard['Template']['SubjectOverall'] ."</td>";
			$n++;
		}
		else
		{
			$e--;	
		}
		
		# Subject Teacher Comment
		if ($AllowSubjectTeacherComment) {
			$x .= "<td {$Rowspan} valign='middle' class='border_left tabletext' align='center' width='10%'>".$eReportCard['TeachersComment']."</td>";
		}
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
				 		$isItalic = 0;
			 		}
			 		else
			 		{
				 		$isItalic = 1;
			 		}
			 		
			 		$css_border_top = ($Prefix)? "" : "border_top";
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
			 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='tabletext'>";
						if($isItalic)	# Sub Subject
						{
							$t .= "<i>";
						}
						$t .= $v;
						if($isItalic)
						{
							$t .= "</i>";
						}
						$t .= "</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
 		return $x;
	}
	
	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
		}
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable .= "<br />";
		$SignatureTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' class='report_border'>";
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			
			if ($k==0)
			{
				$border_left = "";
			}
			else
			{
				$border_left = "border_left";
			}
			
			if ($k == (sizeof($SignatureTitleArray)-1))
			{
				$width = "";
			}
			else
			{
				$width = "width='30%'";
			}
			
			$SignatureTable .= "<td {$width} class='{$border_left} tabletext'>";
			$SignatureTable .= $Title;
			$SignatureTable .= "<br />";
			$SignatureTable .= "<br />";
			$SignatureTable .= "<br />";
			$SignatureTable .= "</td>";
			
			/*
			$IssueDate = ($SettingID == "IssueDate" && $Issued)	? "<u>".$Issued."</u>" : "__________";
			$SignatureTable .= "<td valign='bottom' align='center'>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td align='center' class='small_title' height='130' valign='bottom'>_____". $IssueDate ."_____</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
			*/
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$ShowSubjectFullMark 		= $ReportSetting['ShowSubjectFullMark'];
		
		$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# calculate total number of columns
		$totalNumColumn = 0;
		$totalNumColumn = $totalNumColumn + $ColNum2;
		if ($ShowRightestColumn) $totalNumColumn++;
		if ($AllowSubjectTeacherComment) $totalNumColumn++;
		$curColumn = 0;
		foreach ($ColumnTitle as $ColumnID => $ColumnName) {
			$totalNumColumn += sizeof($NumOfAssessment[$curColumn]);
			$curColumn++;
		}
		$colspan = "colspan='$totalNumColumn'";
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID);
		$GrandTotal = $StudentID ? $result['GrandTotal'] : "S";
		$AverageMark = $StudentID ? $result['GrandAverage'] : "S";
		
		$GrandSchemeInfoArr = $this->Get_Grand_Mark_Grading_Scheme($ClassLevelID);
		$GrandAverageSchemeData = $GrandSchemeInfoArr['-1'];
		$GrandAverageSchemeID = $GrandAverageSchemeData['SchemeID'];
		$thisGrandAverageGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandAverageSchemeID, $AverageMark);
		
		$AverageMarkDisplay = ($GrandAverageSchemeData['ScaleDisplay']=="G")? $thisGrandAverageGrade : $AverageMark;
		
		# 李陞大坑學校 - ERC -- Conduct Mark [CRM Ref No.: 2009-0304-1225]
		# If one of subjects is ABS, average must show "-"
		if ($this->isSubjectsAbs($ReportID, $StudentID))
			$AverageMarkDisplay = "-";
			
		$AverageMarkDisplay = ($AverageMarkDisplay=="")? "&nbsp;" : $AverageMarkDisplay;
		
 		$FormPosition = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? "--": $result['OrderMeritForm']) : "--") : "#";
  		$ClassPosition = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? "--": $result['OrderMeritClass']) : "--") : "#";
  		$ClassNumOfStudent = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? "--": $result['ClassNoOfStudent']) : "--") : "#";
  		$FormNumOfStudent = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? "--": $result['FormNoOfStudent']) : "--") : "#";
		
		if ($CalOrder == 2) {
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID);
				$columnTotal[$ColumnID] = $StudentID ? $columnResult['GrandTotal'] : "S";
				$columnAverage[$ColumnID] = $StudentID ? $columnResult['GrandAverage'] : "S";
				$columnClassPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritClass'] : "S";
				$columnFormPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritForm'] : "S";
				$columnClassNumOfStudent[$ColumnID] = $StudentID ? $columnResult['ClassNoOfStudent'] : "S";
				$columnFormNumOfStudent[$ColumnID] = $StudentID ? $columnResult['FormNoOfStudent'] : "S";
			}
		}
		
		# calculate total subjects full mark and average mark
		$SubjectsFullMark = $this->returnSubjectFullMark($ClassLevelID, $WithSub=0);
		$totalFullMark = 0;
		$subjectCount = 0;
  		foreach ($SubjectsFullMark as $SubjectID => $thisFullMark)
  		{
	  		if (is_numeric($thisFullMark))
	  		{
		  		$subjectCount++;
		  		$totalFullMark += $thisFullMark;
	  		}
  		}
  		/* Total Average Marks must be 100
  		if ($subjectCount == 0)
  		{
	  		$averageFullMark = $this->EmptySymbol;
  		}
  		else
  		{
	  		$averageFullMark = round($totalFullMark/$subjectCount);
  		}
  		*/
  		$averageFullMark = 100;
		
		$first = 1;
		# Overall Result
		if($ShowGrandTotal)
		{
			$border_top = "border_top";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['OverallResultEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['OverallResultCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			
			if ($ShowSubjectFullMark)
			{
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $totalFullMark ."</td>";
			}
			
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' {$colspan}>". $GrandTotal ."</td>";
			
			$x .= "</tr>";
		}
		
		# Average Mark 
		if($ShowGrandAvg)
		{
			$border_top = "border_top";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['AvgMarkEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['AvgMarkCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			
			if ($ShowSubjectFullMark)
			{
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $averageFullMark ."</td>";
			}
			
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' {$colspan}>". $AverageMarkDisplay ."</td>";
			
			if ($AllowSubjectTeacherComment) {
				$x .= "<td class='border_left $border_top' align='center'>";
				$x .= "<span style='padding-left:4px;padding-right:4px;'>-</span>";
				$x .= "</td>";
			}
			$x .= "</tr>";
		}
		
		# Position in Class 
		if($ShowOverallPositionClass)
		{
			$border_top = "border_top";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ClassPositionEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ClassPositionCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			
			if ($ShowSubjectFullMark)
			{
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>&nbsp;</td>";
			}
			
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' {$colspan}>". $ClassPosition ."</td>";
			
			$x .= "</tr>";
		}
		
		# Number of Students in Class 
		if($ShowNumOfStudentClass)
		{
			$border_top = "border_top";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ClassNumOfStudentEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ClassNumOfStudentCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			if ($ShowSubjectFullMark)
			{
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>&nbsp;</td>";
			}
			
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' {$colspan}>". $ClassNumOfStudent ."</td>";
			$x .= "</tr>";
		}
		
		# Position in Form 
		if($ShowOverallPositionForm)
		{
			$border_top = "border_top";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['FormPositionEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['FormPositionCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			if ($ShowSubjectFullMark)
			{
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>&nbsp;</td>";
			}
			
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' {$colspan}>". $FormPosition ."</td>";
			$x .= "</tr>";
		}
		
		# Number of Students in Form 
		if($ShowNumOfStudentForm)
		{
			$border_top = "border_top";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['FormNumOfStudentEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['FormNumOfStudentCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			if ($ShowSubjectFullMark)
			{
				$x .= "<td class='tabletext border_top border_left' align='center' height='{$LineHeight}'>&nbsp;</td>";
			}
			
			$x .= "<td class='tabletext border_top border_left' align='center' height='{$LineHeight}' {$colspan}>". $FormNumOfStudent ."</td>";
			$x .= "</tr>";
		}
		
		# Conduct
  		$conductMark = $this->GET_CONDUCT_MARK($ReportID, $StudentID);
		$border_top = "border_top";
		$first = 0;
		$x .= "<tr>";
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
		$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
		$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ConductEn'] ."</td>";
		$x .= "</tr></table>";
		$x .= "</td>";		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
		$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
		$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ConductCh'] ."</td>";
		$x .= "</tr></table>";
		$x .= "</td>";
		if ($ShowSubjectFullMark)
		{
			$x .= "<td class='tabletext border_top border_left' align='center' height='{$LineHeight}'>"."Grade"."</td>";
		}
		
		$x .= "<td class='tabletext border_top border_left' align='center' height='{$LineHeight}' {$colspan}>". $conductMark ."</td>";
		$x .= "</tr>";
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting;
		
				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$FullMarkColWidth = $eRCTemplateSetting['ColumnWidth']['FullMark'];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1);
		if($ReportType=="T")	# Temrs Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
							
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# define css
				$css_border_top = ($isSub)? "" : "border_top";
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				# Subject Full Mark
				if ($ShowSubjectFullMark)
				{
					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
  					
  					if ($ScaleDisplay == "G")
  						$thisFullMark = "Grade";
  						
					$x[$SubjectID] .= "<td width='$FullMarkColWidth' class='border_left {$css_border_top} tabletext' align='center'>". $thisFullMark ."&nbsp;</td>";
				}
				
				$isAllNA = true;
				$hasABS = false;
									
				# Assessment Marks & Display
				for($i=0;$i<sizeof($ColumnID);$i++)
				{
					$thisColumnID = $ColumnID[$i];
					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
					
					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
					$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
					
					if ($isSub && $columnSubjectWeightTemp == 0)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) 
					{
						$thisMarkDisplay = "&nbsp;";
					} 
					else 
					{
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID=".$ColumnID[$i] ." and SubjectID=$SubjectID");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnID[$i]][Mark] : "";
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][$ColumnID[$i]][Grade] : "";
						
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						if ($thisMark != "N.A.")
						{
							$isAllNA = false;
						}
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
						if($needStyle)
						{
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
							$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
						
						if ($thisMark == "ABS")
							$hasABS = true;
					}
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}
								
				# Subject Overall 
				if($ShowSubjectOverall)
				{
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][0][Grade] : ""; 
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
										
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
 						if($thisSubjectWeight > 0)
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
						else
							$thisMarkTemp = $thisMark;
 						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
						
					if ($hasABS)
						$thisMarkDisplay = "-";
					
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>--</td>";
				}
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
							
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
				$css_border_top = $isSub? "" : "border_top";
				
				$isAllNA = true;
				$hasABS = false;
				
				if($ShowSubjectFullMark)
				{
  					$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
  					if ($ScaleDisplay == "G")
						$FullMark = "Grade";
						
  					$x[$SubjectID] .= "<td class='{$css_border_top} border_left tabletext' align='center'>". $FullMark ."</td>";
				}
				
				# Terms's Assesment / Terms Result
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					if($isDetails==1)		# Retrieve assesments' marks
					{
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
						
						# if no term reports, CANNOT retrieve assessment result at all
						if (empty($thisReport)) {
							for($j=0;$j<sizeof($ColumnID);$j++) {
								$x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>--</td>";
							}
						} else {
							$thisReportID = $thisReport['ReportID'];
							$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
							$ColumnID = array();
							$ColumnTitle = array();
							foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
							
							$thisMarksAry = $this->getMarks($thisReportID, $StudentID);		
								 
							for($j=0;$j<sizeof($ColumnID);$j++)
							{
								$thisColumnID = $ColumnID[$i];
								$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
								$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
								$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
								
								$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($thisReportID, $SubjectID);
								$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);

								if ($isSub && $columnSubjectWeightTemp == 0)
								{
									$thisMarkDisplay = $this->EmptySymbol;
								}
								else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
									$thisMarkDisplay = "&nbsp;";
								} else {
									$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID=".$ColumnID[$j] ." and SubjectID=$SubjectID");
									$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
									
									$thisMark = $ScaleDisplay=="M" ? $thisMarksAry[$SubjectID][$ColumnID[$j]][Mark] : "";
									$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $thisMarksAry[$SubjectID][$ColumnID[$j]][Grade] : ""; 
									
									# for preview purpose
									if(!$StudentID)
									{
										$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
										$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
									}
									$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
													
									if ($thisMark != "N.A.")
									{
										$isAllNA = false;
									}
									
									# check special case
									list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
									if($needStyle)
									{
										$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
										$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
										$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
									}
									else
									{
										$thisMarkDisplay = $thisMark;
									}
									
									if ($thisMark == "ABS")
										$hasABS = true;
								}
									
								$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
								$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
			  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
			  					
								$x[$SubjectID] .= "</tr></table>";
								$x[$SubjectID] .= "</td>";
							}
						}
					}
					else					# Retrieve Terms Overall marks
					{
						if ($isParentSubject && $CalculationOrder == 1) {
							$thisMarkDisplay = "&nbsp;";
						} else {
							# See if any term reports available
							$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
							
							# if no term reports, the term mark should be entered directly
							if (empty($thisReport)) {
								$thisReportID = $ReportID;
								$MarksAry = $this->getMarks($thisReportID, $StudentID);
								$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
								$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
							} else {
								$thisReportID = $thisReport['ReportID'];
								$MarksAry = $this->getMarks($thisReportID, $StudentID);
								$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
								$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
							}
	 						
							$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID=".$ColumnData[$i]['ReportColumnID'] ." and SubjectID=$SubjectID");
							$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
														
							# for preview purpose
							if(!$StudentID)
							{
								$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
								$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
							}
							# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
							$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
							
							if ($thisMark != "N.A.")
							{
								$isAllNA = false;
							}
						
							# check special case
							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
							if($needStyle)
							{
								$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
	 							//$thisMark = ($ScaleDisplay=="M" && $StudentID) ? my_round($thisMark*$thisSubjectWeight,2) : $thisMark;
								$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
							}
							
							if ($thisMark == "ABS")
								$hasABS = true;
						}
							
						$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
	  					
						$x[$SubjectID] .= "</tr></table>";
						$x[$SubjectID] .= "</td>";
					}
				}
				
				# Subject Overall
				//if($ShowSubjectOverall && $CalculationOrder == 1)
				if($ShowSubjectOverall)
				{
					$MarksAry = $this->getMarks($ReportID, $StudentID);		
					
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][0][Grade] : ""; 
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
						
					if ($hasABS)
						$thisMarkDisplay = "-";
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>--</td>";
				}
				
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		}	# End Whole Year Report Type
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		# Retrieve Basic Information of Report
		$BasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$AllowClassTeacherComment = $BasicInfo['AllowClassTeacherComment'];
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

		# retrieve the latest Term
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		foreach($sems as $TermID=>$TermName)
			$latestTerm = $TermID;
		$latestTerm++;
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		foreach($csvType as $k=>$Type)
		{
			$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);	
			if(!empty($csvData)) 
			{
				foreach($csvData as $RegNo=>$data)
				{
					if($RegNo == $WebSamsRegNo)
					{
	 					foreach($data as $key=>$val)
		 					$ary[$key] = $val;
					}
				}
			}
		}
		
		# retrieve result data
		$Absence = ($ary['Days Absent'])? $ary['Days Absent'] : 0;
		$Lateness = ($ary['Time Late'])? $ary['Time Late'] : 0;
		$EarlyLeave = ($ary['Early Leave'])? $ary['Early Leave'] : 0;
		$Promotion = ($ary['Promotion'])? $ary['Promotion'] : $this->EmptySymbol;
		$Merits = ($ary['Merits'])? $ary['Merits'] : 0;
		$MinorCredit = ($ary['Minor Credit'])? $ary['Minor Credit'] : 0;
		$MajorCredit = ($ary['Major Credit'])? $ary['Major Credit'] : 0;
		$Demerits = ($ary['Merits'])? $ary['Demerits'] : 0;
		$MinorFault = ($ary['Minor Fault'])? $ary['Minor Fault'] : 0;
		$MajorFault = ($ary['Major Fault'])? $ary['Major Fault'] : 0;
		$Remark = ($ary['Remark'])? $ary['Remark'] : $this->EmptySymbol;
		$ECA = ($ary['ECA'])? $ary['ECA'] : $this->EmptySymbol;
		$Service = ($ary['Service'])? $ary['Service'] : $this->EmptySymbol;
		$MeritRecord = ($ary['MeritRecord'])? $ary['MeritRecord'] : "";
		
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$ClassTeacherComment = $CommentAry[0];
		$ClassTeacherComment = ($ClassTeacherComment)? $ClassTeacherComment : $this->EmptySymbol;
		
		$colspan = 4;
		
		$x .= "<br>";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\" class='report_border'>";
		# Absence, Lateness, Early Leave, Promotion Title
		$x .= "<tr>";
			$thisDisplay = $eReportCard['Template']['DaysAbsentEn']."<br />".$eReportCard['Template']['DaysAbsentCh'];
			$x .= "<td class='tabletext' align='center'>".$thisDisplay."</td>";
			$thisDisplay = $eReportCard['Template']['TimesLateEn']."<br />".$eReportCard['Template']['TimesLateCh'];
			$x .= "<td class='border_left tabletext' align='center'>".$thisDisplay."</td>";
			$thisDisplay = $eReportCard['Template']['EarlyLeaveEn']."<br />".$eReportCard['Template']['EarlyLeaveCh'];
			$x .= "<td class='border_left tabletext' align='center'>".$thisDisplay."</td>";
			$thisDisplay = $eReportCard['Template']['PromotionEn']."<br />".$eReportCard['Template']['PromotionCh'];
			$x .= "<td class='border_left tabletext' align='center'>".$thisDisplay."</td>";
		$x .= "</tr>";
		# Absence, Lateness, Early Leave, Promotion Result
		$x .= "<tr>";
			$x .= "<td class='tabletext' align='center'>".$Absence."</td>";
			$x .= "<td class='border_left tabletext' align='center'>".$Lateness."</td>";
			$x .= "<td class='border_left tabletext' align='center'>".$EarlyLeave."</td>";
			$x .= "<td class='border_left tabletext' align='center'>".$Promotion."</td>";
		$x .= "</tr>";
		
		# Merit/Demerit (to be confirmed how to display)
		$merit = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		
		if (is_array($MeritRecord))
		{
			for ($i=0; $i<count($MeritRecord); $i++)
			{
				$thisDisplay = $MeritRecord[$i];
				$merit .= "<tr>";
					$merit .= "<td class='tabletext'>". $thisDisplay ."</td>";
				$merit .= "</tr>";
			}
		}
		else
		{
			$merit .= "<tr>";
				$merit .= "<td class='tabletext'>". $MeritRecord ."</td>";
			$merit .= "</tr>";
		}
		
		
		/*
		$typeArray = array("Merits", "Minor Credit", "Major Credit", "Demerits", "Minor Fault", "Major Fault");
		$numPerRow = 3;
		$counter = 0;
		
		for ($i=0; $i<sizeof($typeArray); $i++)
		{
			$thisType = $typeArray[$i];
			$thisLang = str_replace(" ", "", $thisType);
			$thisTitle = $eReportCard['Template'][$thisLang];
			$thisValue = $ary[$thisType];
			
			if ($thisValue == 0)
			{
				continue;
			}
			
			$counter++;
			
			if ( ($counter % $numPerRow) == 1 )
			{
				# start of a row
				$merit .= "<tr>";
			}
			
			$merit .= "<td class='tabletext'>". $thisTitle . ": " . $thisValue ."</td>";
			
			if ( ($counter % $numPerRow) == 0 )
			{
				# end of a row
				$merit .= "</tr>";
			}
		}
		
		# add back the empty cells and empty rows
		if ( ($counter % $numPerRow) == 0  && $counter!=6 )
		{
			$numEmptyCell = $numPerRow - ($counter % $numPerRow);
			for ($i=0; $i<$numEmptyCell; $i++)
			{
				$counter++;
				
				if ($counter == 4)
				{
					# start of a row
					$merit .= "<tr>";
				}
				
				$merit .= "<td class='tabletext'>&nbsp;</td>";
				
				if ( ($counter % $numPerRow) == 0 )
				{
					# end of a row
					$merit .= "</tr>";
				}
			}
		}
		*/
		/*
		$merit .= "<tr>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['Merits'] . ": " . ($ary['Merits'] ? $ary['Merits'] :0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorCredit'] . ": " . ($ary['Minor Credit'] ? $ary['Minor Credit'] :0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorCredit'] . ": " . ($ary['Major Credit'] ? $ary['Major Credit'] : 0) ."</td>";
		$merit .= "</tr>";
		$merit .= "<tr>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['Demerits'] . ": " . ($ary['Demerits'] ? $ary['Demerits'] : 0) ."</td>"; 
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorFault'] . ": " . ($ary['Minor Fault'] ? $ary['Minor Fault'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorFault'] . ": " . ($ary['Major Fault'] ? $ary['Major Fault'] : 0) ."</td>";
		$merit .= "</tr>";
		*/
		
		$merit .= "</table>";
		
		$x .= "<tr>";
			$x .= "<td class='border_top tabletext' colspan='$colspan'>".$eReportCard['Template']['MeritDemerit']."</td>";
		$x .= "</tr>";
		$x .= "<tr>";
			$x .= "<td class='tabletext' colspan='$colspan'>".$merit."</td>";
		$x .= "</tr>";
		
		# Activity and Service
		/*
		if (is_array($Service))
		{
			$ServiceDisplay = "";
			for ($i=0; $i<sizeof($Service); $i++)
			{
				$thisService = $Service[$i];
				$ServiceDisplay .= "<li>".$thisService."</li>";
			}
		}
		else
		{
			$ServiceDisplay = $Service;
		}
		*/
		$x .= "<tr>";
			$x .= "<td class='border_top tabletext' colspan='$colspan'>";
				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
					$x .= "<tr>";
						$x .= "<td class='tabletext'>".$eReportCard['Template']['Activity&Service']."</td>";
					$x .= "</tr>";
					# set the height to 3-row-height for all reports
					$x .= "<tr>";
						$x .= "<td class='tabletext' height='55px' valign='center'>".$Service."</td>";
					$x .= "</tr>";
				$x .= "</table>";
			$x .= "</td>";
		$x .= "</tr>";
		
		# Comments
		if ($AllowClassTeacherComment)
		{
			$x .= "<tr>";
				$x .= "<td class='border_top tabletext' colspan='$colspan'>".$eReportCard['Template']['ClassTeacherComment']."</td>";
			$x .= "</tr>";
			$x .= "<tr>";
				$x .= "<td class='tabletext' colspan='$colspan'>".stripslashes($ClassTeacherComment)."</td>";
			$x .= "</tr>";
		}
		
		$x .= "</table>";
		
		return $x;
	}
	
	function genAddressTable()
	{
		global $eReportCard;
		
		$x = "";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		$x .= "<tr>";
			$x .= "<td class='address_text'>".$eReportCard['Template']['Address']."</td>";
			$x .= "<td class='address_text'>".$eReportCard['Template']['Telephone']."</td>";
			$x .= "<td class='address_text'>".$eReportCard['Template']['Fax']."</td>";
		$x .= "</tr>";		
		$x .= "</table>";
		
		return $x;
	}
	
	########### END Template Related
	
	function ADD_CHINESE_TITLE($Title)
	{
		$thisTitle = trim($Title);
		
		switch ($thisTitle)
		{
			case "Formative Assessment":
				$Title .= "<br>進展性評估";
				break;
			case "Summative Assessment":
				$Title .= "<br>總結性評估";
				break;
			case "Test 1":
				$Title .= "<br>測驗1";
				break;
		}
		
		return $Title;
	}
	
	function GET_CONDUCT_MARK($ReportID, $StudentID)
	{
		global $eReportCard, $eRCTemplateSetting;
				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
 		
 		# if the conduct is being saved before, get the saved conduct mark
 		# otherwise, calculate the conduct mark now
 		$thisArr = $this->Get_Student_Final_Conduct_Grade(array($StudentID), $ReportID);
 		$savedConduct = $thisArr[$StudentID]['Conduct'];
 		
 		if ($savedConduct)
 		{
	 		$ConductGrade = $savedConduct;
 		}
 		else
 		{
	 		$SubjectIDArr = $this->returnSubjectwOrder($ClassLevelID);
		
			$SubjectCount = 0;
			$ConductTotal = 0;		
			foreach($SubjectIDArr as $SubjectID=>$CmpSubjectArr)
			{
				$temp = $this->GET_EXTRA_SUBJECT_INFO(array($StudentID), $SubjectID, $ReportID);
				$thisConduct = $temp[$StudentID]['Info'];
				if ($thisConduct!="" && is_numeric($thisConduct))
				{
					$SubjectCount++;
					$ConductTotal += $thisConduct;
				}
			}
			
			if ($SubjectCount != 0)
			{
				$ConductAverage = round($ConductTotal/$SubjectCount);
			}
			else
			{
				$ConductAverage = 0;
			}
			
			$ConductGrade = $this->convertConductMarkToGrade($ConductAverage, $ClassLevelID);
 		}
 		
		return $ConductGrade;
	}
	
	function convertConductMarkToGrade($Mark, $ClassLevelID)
	{
		# 李陞大坑學校 - eReport Card -- template remark [CRM Ref No.: 2009-0121-1002]
		# updated the grading scheme of conduct mark
		# $lowerLimitArr = array(90, 75, 60, 30, 0);
		
		# 李陞大坑學校 - ERC -- Conduct Mark [CRM Ref No.: 2009-0304-1225]
		# separate calculation of P1-4 and P5-6
		/*
		$lowerLimitArr = array(85, 75, 60, 30, 0);
		$gradeArr = array(A, B, C, D, E);
		*/
		
		# 李陞大坑學校 - eRC -- Conduct Grade [CRM Ref No.: 2009-0317-1034]
		if ($this->GET_FORM_NUMBER($ClassLevelID) < 5)
		{
			# P1-4
			$lowerLimitArr = array(93, 85, 82, 79, 75, 70, 65, 60, 54, 47, 40, 0);
			$gradeArr = array("A", "A-", "B+", "B", "B-", "C+", "C", "C-", "D+", "D", "D-", "E");
		}
		else
		{
			# P5-6
			$lowerLimitArr = array(85, 75, 60, 40, 0);
			$gradeArr = array("A", "B", "C", "D", "E");
		}
		
		/*
		# 李陞大坑學校 - eRc --- correct conduct mark remarks [CRM Ref No.: 2009-0306-1201]
		# Change back to uniform conduct mark calculation
		$lowerLimitArr = array(85, 75, 60, 30, 0);
		$gradeArr = array("A", "B", "C", "D", "E");
		*/
		for ($i=0; $i<sizeof($lowerLimitArr); $i++)
		{
			$thisLowerLimit = $lowerLimitArr[$i];
			$thisGrade = $gradeArr[$i];
			if ($Mark > $thisLowerLimit)
			{
				return $thisGrade;
			}
		}
		
		return $gradeArr[sizeof($gradeArr)-1];
	}
	
	function isSubjectsAbs($ReportID, $StudentID)
	{
		$MarksAry = $this->getMarks($ReportID, $StudentID);
		
		$hasABS = false;
		foreach ($MarksAry as $subjectID => $columnMarkArr)
		{
			foreach ($columnMarkArr as $reportColumnID => $markGradeArr)
			{
				list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $subjectID, $markGradeArr['Mark'], $markGradeArr['Grade']);
				if ($thisMark == "ABS")
				{
					$hasABS = true;
					return $hasABS;
				}
			}
		}
		
		return $hasABS;
	}
	
	function Get_Student_Final_Conduct_Grade($StudentIDList, $ReportID) {
		$returnArr = array();
		if (sizeof($StudentIDList) < 1) {
			return $returnArr;
		}
		
		$StudentIDList = implode(",", $StudentIDList);
		$table = $this->DBName.".RC_OTHER_STUDENT_INFO";
		$sql = "SELECT 
					StudentID, Conduct, DateModified
				FROM 
					$table 
				WHERE 
					ReportID = '$ReportID' AND 
					StudentID IN ($StudentIDList)";

		$result = $this->returnArray($sql);
		
		if(sizeof($result) > 0) {
			for($i=0; $i<sizeof($result); $i++) {
				$thisStudentID = $result[$i]["StudentID"];
				
				$returnArr[$thisStudentID]["Conduct"] = $result[$i]["Conduct"];
				$returnArr[$thisStudentID]["DateModified"] = $result[$i]["DateModified"];
			}
		}
		return $returnArr;
	}
	
	/*
	 *	InfoArr[$i]	['StudentID']
	 *				['ReportID']
	 *				['Conduct']
	 */
	function Insert_Student_Final_Conduct_Grade($InfoArr=array())
	{
		if (count($InfoArr) == 0)
			return 0;
			
		$field = " (StudentID, ReportID, Conduct, DateInput, DateModified) ";
		
		$valueTextArr = array();
		$numOfInfo = count($InfoArr);
		for ($i=0; $i<$numOfInfo; $i++)
		{
			$thisInfoArr = $InfoArr[$i];
			$thisValueText = "";
			
			$thisValueText .= " (";
			$thisValueText .= "'".$thisInfoArr['StudentID']."', ";
			$thisValueText .= "'".$thisInfoArr['ReportID']."', ";
			$thisValueText .= "'".$thisInfoArr['Conduct']."', ";
			$thisValueText .= "NOW(), NOW() ";
			$thisValueText .= " )";
			
			$valueTextArr[$i] = $thisValueText;
		}
		
		$valueText = implode(", ", $valueTextArr);
		
		$table = $this->DBName.".RC_OTHER_STUDENT_INFO";
		$sql = "INSERT INTO $table
					$field
				VALUES
					$valueText
				";
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	/*
	 *	InfoArr[$i]	['StudentID']
	 *				['ReportID']
	 *				['Conduct']
	 */
	function Update_Student_Final_Conduct_Grade($InfoArr=array())
	{
		if (count($InfoArr) == 0)
			return 0;
			
		$table = $this->DBName.".RC_OTHER_STUDENT_INFO";
		
		$numOfInfo = count($InfoArr);
		for ($i=0; $i<$numOfInfo; $i++)
		{
			$thisInfoArr = $InfoArr[$i];
			$thisReportID = $thisInfoArr['ReportID'];
			$thisStudentID = $thisInfoArr['StudentID'];
			$thisConduct = $thisInfoArr['Conduct'];
			
			$sql = 	"UPDATE $table SET 
						Conduct = '".$thisConduct."' ,
						DateModified = NOW()
					WHERE 
						ReportID = '".$thisReportID."' 
						AND
						StudentID = '".$thisStudentID."'
					";
			$success[$thisStudentID] = $this->db_db_query($sql);
		}
		
		if (in_array(false, $success))
			return 0;
		else
			return 1;
	}
	
}
?>