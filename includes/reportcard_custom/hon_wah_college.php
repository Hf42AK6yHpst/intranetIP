<?php
# Editing by 

/******************************************************
 *  20151203 Bill:	[2015-1008-1356-44164]
 * 		- modified getMiscTable(), genActivityTable(), for report layout modification
 *  20110111 Marcus:
 * 		- override GET_FORM_NUMBER, force return 4 if form number is 5, so that F5 template was replaced by F4 templates
 * *******************************************************/

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/hon_wah_college.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "award", "remark", "attendance");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 0;
		$this->IsEnableMarksheetExtraInfo = 0;
		
		$this->EmptySymbol = "------";
		$this->prefix = "&nbsp;&nbsp;";
		
// 		$this->ConductItemArr = array("Conduct", "Appearance", "Attitude", "Aspiration", "Competence", "Discipline");
// 		$this->ConductFullMarkArr = array("A", "5", "10", "5", "10", "20");
		$this->ConductItemArr = array("Conduct", "Courtesy Requirements", "Appearance Requirements", "Manners and Social Skills", "Learning Attitude", "Discipline Requirements");
		$this->ConductFullMarkArr = array("A", "8", "6", "8", "8", "20");
		
		$this->MaxPostDisplay = 7;
		$this->MaxActivitiesDisplay = 12;
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID) {
		global $eReportCard;
		
		$ConductTable = $this->getConductTable($ReportID, $StudentID);
		$PostTable = $this->getPostTable($ReportID, $StudentID);
		$ActivitiesTable = $this->getActivitiesTable($ReportID, $StudentID);
		
		$TableLeftTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
		$TableLeftTop .= "<tr><td>".$TitleTable."</td></tr>";
		$TableLeftTop .= "<tr><td height='1'>&nbsp;</td></tr>";
		$TableLeftTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableLeftTop .= "<tr><td height='1'>&nbsp;</td></tr>";
		$TableLeftTop .= "<tr><td>".$MSTable."</td></tr>";
		$TableLeftTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableLeftTop .= "</table>";
		
		$TableLeftBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
		$TableLeftBottom .= "<tr><td>".$SignatureTable."</td></tr>";
		$TableLeftBottom .= $FooterRow;
		$TableLeftBottom .= "</table>";
		
		$TableLeft = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
		$TableLeft .= "<tr height='700' valign='top'><td>".$TableLeftTop."</td></tr>";
		$TableLeft .= "<tr valign='bottom'><td>".$TableLeftBottom."</td></tr>";
		$TableLeft .= "</table>";
		
		$TableRight = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
		$TableRight .= "<tr><td>".$ConductTable."</td></tr>";
		$TableRight .= "<tr><td height='1'>&nbsp;</td></tr>";
		$TableRight .= "<tr><td>".$PostTable."</td></tr>";
		$TableRight .= "<tr><td height='1'>&nbsp;</td></tr>";
		$TableRight .= "<tr><td>".$ActivitiesTable."</td></tr>";
		$TableRight .= "</table>";
				
		$x = "";
		$x .= "<tr valign='top'><td>";
		$x .= "<table width='100%' height='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
		$x .= "<tr valign='top'>";
		$x .= "<td width='48%'>".$TableLeft."</td>";
		$x .= "<td>&nbsp;</td>";
		$x .= "<td width='48%'>".$TableRight."</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
			# get school badge
			//$SchoolLogo = GET_SCHOOL_BADGE();
				
			# get school name
			$SchoolName = "漢華中學 Hon Wah College";	
			
			$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
	
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			# no school badge for this school
			//$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
			
			$TitleTable .= "<tr>";			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
					if(!empty($SchoolName))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$SchoolName."</td></tr>\n";
					if(!empty($ReportTitle))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle."</td></tr>\n";
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			}
			//$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
			$TitleTable .= "</tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal = "XXX";
			$data['AcademicYear'] = getCurrentAcademicYear();
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				
				$data['Name'] = $lu->UserName2Lang('ch', 2);
				$data['ClassNo'] = $thisClassNumber;
				
				$data['StudentNo'] = $data['ClassNo'];
				$data['Class'] = $thisClassName;
				
				$data['DateOfBirth'] = $this->convertDateFormat($lu->DateOfBirth);
				
				$thisGender = $lu->Gender;
				$data['Gender'] = ($thisGender=="M")? $eReportCard['Template']['StudentInfo']['Male'] : $eReportCard['Template']['StudentInfo']['Female'];
				
				$data['STRN'] = $lu->STRN;
				$data['RegNo'] = str_replace("#", "", $lu->WebSamsRegNo);
				
				$ClassTeacherAry = $lclass->returnClassTeacher($lu->ClassName);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : $this->EmptySymbol;
			}
			$data['DateOfIssue'] = $this->convertDateFormat($ReportSetting['Issued']);
			
			# hardcode the 1st 3 rows items
			$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
			# 1st row (Name, Class)
			$numFirstRow = 2;
			$StudentInfoTable .= "<tr>";
			# Name
			$SettingID = "Name";
			$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
			$StudentInfoTable .= "<td class='tabletext' colspan='2' width='20%' valign='top' height='{$LineHeight}'>".$Title." : ";
			$StudentInfoTable .= ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
			# Class
			$SettingID = "Class";
			$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
			$StudentInfoTable .= "<td class='tabletext' $colspan width='20%' valign='top' height='{$LineHeight}'>".$Title." : ";
			$StudentInfoTable .= ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
			$StudentInfoTable .= "</tr>\n";
			
			# 2nd and third row
			$defaultInfoArray = array("Name", "Class", "Gender", "STRN", "ClassNo", "DateOfBirth", "RegNo", "DateOfIssue");
			if(!empty($defaultInfoArray))
			{
				$count = 0;
				for($i=$numFirstRow; $i<sizeof($defaultInfoArray); $i++)
				{
					$SettingID = trim($defaultInfoArray[$i]);
					$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
					
					if($count%$StudentInfoTableCol==0) {
						$StudentInfoTable .= "<tr>";
					}
					
					$StudentInfoTable .= "<td class='tabletext' $colspan width='20%' valign='top' height='{$LineHeight}'>".$Title." : ";
					$StudentInfoTable .= ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
										
					if(($count+1)%$StudentInfoTableCol==0) {
						$StudentInfoTable .= "</tr>\n";
					} 
					$count++;
				}
			}
				
			# Display student info selected by the user other than the 1st 3 rows
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if(in_array($SettingID, $SettingStudentInfo)===true && (!in_array($SettingID, $defaultInfoArray))===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
					
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
						}
						
						$StudentInfoTable .= "<td class='tabletext' $colspan width='20%' valign='top' height='{$LineHeight}'>".$Title." : ";
						$StudentInfoTable .= ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
											
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>\n";
						} 
						$count++;
					}
				}				
			}
			
			$StudentInfoTable .= "</table>\n";
			
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
				
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID);
						
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];

		//$MSTableReturned2 = $this->genMSTableMarks('', $MarksAry, $StudentID, $NumOfAssessmentArr);
		//$MarksDisplayAry2 = $MSTableReturned2['HTML'];
		//$isAllNAAry2 = $MSTableReturned2['isAllNA'];
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='report_border'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				//$DetailsTable .= $MarksDisplayAry2[$thisSubjectID];
				# Subject Teacher Comment
				if ($AllowSubjectTeacherComment) {
					$css_border_top = ($isFirst)? "" : "";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='tabletext border_left border_bottom $css_border_top'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
					}
					$DetailsTable .= "</span></td>";
				}
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}
		
		# MS Table Footer (No Footer for this school)
		//$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		#########################################################
		############## Marks START
		$row2 = "";
		/*
		if($ReportType=="T")	# Terms Report Type
		{
			# Retrieve Invloved Assesment
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if (sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
			$e = 0;
			for($i=0;$i<sizeof($ColumnTitle);$i++)
			{
				$ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$i], 1, 2));
				$row1 .= "<td valign='middle' height='{$LineHeight}' class='border_left tabletext' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'><b>". $ColumnTitleDisplay . "</b></td>";
				$n++;
 				$e++;
			}
		}
		else					# Whole Year Report Type
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
 			$needRowspan=0;
			for($i=0;$i<sizeof($ColumnData);$i++)
			{
				$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
				$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
				if($isDetails==1)
				{
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColumnID = array();
					$ColumnTitle = array();
					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
					for($j=0;$j<sizeof($ColumnTitle);$j++)
					{
						$ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$j], 1, 2));
						$row2 .= "<td class='border_left reportcard_text' align='center' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $ColumnTitleDisplay . "</td>";
						$n++;
						$e++;
					}
					$colspan = "colspan='". sizeof($ColumnTitle) ."'";
					$Rowspan = "";
					$needRowspan++;
					$t[$i] = sizeof($ColumnTitle);
				}
				else
				{
					$colspan = "";
					$Rowspan = "rowspan='2'";
					$e++;
				}
				$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left small_title' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $SemName ."</td>";
			}
		}
		############## Marks END
		#########################################################
		$Rowspan = $row2 ? "rowspan='2'" : "";

		if(!$needRowspan)
			$row1 = str_replace("rowspan='2'", "", $row1);
		
		$x = "<tr>";
		# Subject 
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		for($i=0;$i<sizeof($SubjectColAry);$i++)
		{
			$SubjectEng = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'];
			$SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];
			$SubjectTitle = $SubjectColAry[$i];
			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
			$x .= "<td {$Rowspan}  valign='middle' class='small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>". $SubjectTitle . "</td>";
			$n++;
		}
		
		# Marks
		$x .= $row1;
		
		# Subject Overall 
		if($ShowRightestColumn)
		{
			$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Overall'] ."' class='border_left small_title' align='center'>". $eReportCard['Template']['SubjectOverall'] ."</td>";
			$n++;
		}
		else
		{
			$e--;	
		}
		
		*/
		
		$x = "<tr>";
		
		# Subject 
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		$SubjectTitle = $eReportCard['Template']['SubjectChn']."<br />".$eReportCard['Template']['SubjectEng'];
		$x .= "<td colspan='2' valign='middle' align='center' class='small_title border_bottom' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>". $SubjectTitle . "</td>";
		
		# Full Mark
		if ($ShowSubjectFullMark)
		{
			$FullMarkTitle = $eReportCard['Template']['SchemesFullMarkCh']."<br />".$eReportCard['Template']['SchemesFullMarkEn'];
			$x .= "<td valign='middle' align='center' class='border_left border_bottom small_title' height='{$LineHeight}' width='".$eRCTemplateSetting['ColumnWidth']['Mark']."'>". $FullMarkTitle . "</td>";
		}
		
		if ($this->Is_F6_Graduation_Report($ReportID)) //2011-0927-1127-16069 - 漢華中學 - Change the S6 report card (334 change)
		{
			# Whole Year
		    $x .= "<td valign='middle' align='center' class='border_left border_bottom small_title' height='{$LineHeight}' width='6%'>". $eReportCard['Template']['WholeYear'] . "</td>";
		    $x .= "<td valign='middle' align='center' class='border_left border_bottom small_title' height='{$LineHeight}' width='4%'>". $eReportCard['Template']['Rank'] . "</td>";
		}
		else if ($this->Is_F7_Graduation_Report($ReportID))
		{
			# Matriculation Class Final Examination
			$x .= "<td valign='middle' align='center' class='border_left border_bottom small_title' height='{$LineHeight}' width='6%'>". $eReportCard['Template']['F7_GraduationReport'] . "</td>";
		}
		else if ($this->Is_F5_Graduation_Report($ReportID))
		{
			# Graduate Examination
			$x .= "<td valign='middle' align='center' class='border_left border_bottom small_title' height='{$LineHeight}' width='5%'>". $eReportCard['Template']['F5_GraduationReport'] . "</td>";
		}
		else if ($FormNumber==5 && !$this->Is_F5_Graduation_Report($ReportID))
		{
			# Graduate Examination
			$x .= "<td valign='middle' align='center' class='border_left border_bottom small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'>". $eReportCard['Template']['FirstTerm'] . "</td>";
			$x .= "<td valign='middle' align='center' class='border_left border_bottom small_title' height='{$LineHeight}' width='4%'>". $eReportCard['Template']['F5_GraduationReport'] . "</td>";
		}
		else
		{
			# 1st Term
			$x .= "<td valign='middle' align='center' class='border_left border_bottom small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'>". $eReportCard['Template']['FirstTerm'] . "</td>";

			if($FormNumber < 5 )
				$x .= "<td valign='middle' align='center' class='border_left border_bottom small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Rank'] ."'>". $eReportCard['Template']['Rank'] . "</td>";
			
			# 2nd Term if consolidate report
			//if ($ReportType=="W")
			//{
				$x .= "<td valign='middle' align='center' class='border_left border_bottom small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'>". $eReportCard['Template']['SecondTerm'] . "</td>";
			//}
			
			
			$x .= "<td valign='middle' align='center' class='border_left border_bottom small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'>". $eReportCard['Template']['WholeYear'] . "</td>";
			if($FormNumber < 5 )
				$x .= "<td valign='middle' align='center' class='border_left border_bottom small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Rank'] ."'>". $eReportCard['Template']['Rank'] . "</td>";
		}
		
		
		# Subject Teacher Comment
		if ($AllowSubjectTeacherComment) {
			$x .= "<td {$Rowspan} valign='middle' class='border_left border_bottom small_title' align='center' width='10%'>".$eReportCard['TeachersComment']."</td>";
		}
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		$css_border_top = ($isFirst)? "" : "";
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				
		 				if(strlen($SubjectEng) > 30)
		 					$SubjectEng = str_replace("(","<br>(",$SubjectEng);
		 					
		 				if ($SubjectEng == 'Business Accounting and Financial Studies')
		 					$SubjectEng = 'Business Accounting and <br />Financial Studies';
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
			 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='tabletext' nowrap>$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
 		return $x;
	}
	
	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
		}
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			$IssueDate = ($SettingID == "IssueDate" && $Issued)	? "<u>".$Issued."</u>" : "________________";
			$SignatureTable .= "<td valign='bottom' align='center'>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td align='center' class='tabletext' height='130' valign='bottom'>______". $IssueDate ."______</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='tabletext' valign='bottom'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID);
		$GrandTotal = $StudentID ? $result['GrandTotal'] : "S";
		$AverageMark = $StudentID ? $result['GrandAverage'] : "S";
		
 		$FormPosition = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? $this->EmptySymbol: $result['OrderMeritForm']) : $this->EmptySymbol) : "#";
  		$ClassPosition = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? $this->EmptySymbol: $result['OrderMeritClass']) : $this->EmptySymbol) : "#";
		
		if ($CalOrder == 2) {
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID);
				$columnTotal[$ColumnID] = $StudentID ? $columnResult['GrandTotal'] : "S";
				$columnAverage[$ColumnID] = $StudentID ? $columnResult['GrandAverage'] : "S";
				$columnClassPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritClass'] : "S";
				$columnFormPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritForm'] : "S";
			}
		}
		
		$first = 1;
		# Overall Result
		if($ShowGrandTotal)
		{
			$border_top = $first ? "border_top" : "";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['OverallResultEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['OverallResultCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			
			if ($CalOrder == 1) {
				for($i=0;$i<$ColNum2;$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>--</td>";
			} else {
				$curColumn = 0;
				foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					for ($i=0; $i<$NumOfAssessment[$curColumn] - 1; $i++)
					{
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>--</td>";
					}
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnTotal[$ColumnID]."</td>";
					$curColumn++;
				}
			}
			
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $GrandTotal ."</td>";
			if ($AllowSubjectTeacherComment) {
				$x .= "<td class='border_left $border_top' align='center'>";
				$x .= "<span style='padding-left:4px;padding-right:4px;'>-</span>";
				$x .= "</td>";
			}
			$x .= "</tr>";
		}
		
		# Average Mark 
		if($ShowGrandAvg)
		{
			$border_top = $first ? "border_top" : "";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['AvgMarkEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['AvgMarkCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			if ($CalOrder == 1) {
				for($i=0;$i<$ColNum2;$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>--</td>";
			} else {
				$curColumn = 0;
				foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					for ($i=0; $i<$NumOfAssessment[$curColumn] - 1; $i++)
					{
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>--</td>";
					}
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnAverage[$ColumnID]."</td>";
					$curColumn++;
				}
			}
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $AverageMark ."</td>";
			if ($AllowSubjectTeacherComment) {
				$x .= "<td class='border_left $border_top' align='center'>";
				$x .= "<span style='padding-left:4px;padding-right:4px;'>-</span>";
				$x .= "</td>";
			}
			$x .= "</tr>";
		}
		
		# Position in Class 
		if($ShowOverallPositionClass)
		{
			$border_top = $first ? "border_top" : "";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ClassPositionEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ClassPositionCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			if ($CalOrder == 1) {
				for($i=0;$i<$ColNum2;$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>--</td>";
			} else {
				$curColumn = 0;
				foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					for ($i=0; $i<$NumOfAssessment[$curColumn] - 1; $i++)
					{
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>--</td>";
					}
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnClassPos[$ColumnID]."</td>";
					$curColumn++;
				}
			}
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $ClassPosition ."</td>";
			if ($AllowSubjectTeacherComment) {
				$x .= "<td class='border_left $border_top' align='center'>";
				$x .= "<span style='padding-left:4px;padding-right:4px;'>-</span>";
				$x .= "</td>";
			}
			$x .= "</tr>";
		}
		
		# Position in Form 
		if($ShowOverallPositionForm)
		{
			$border_top = $first ? "border_top" : "";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['FormPositionEn'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";		
			$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['FormPositionCh'] ."</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
			if ($CalOrder == 1) {
				for($i=0;$i<$ColNum2;$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>--</td>";
			} else {
				$curColumn = 0;
				foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					for ($i=0; $i<$NumOfAssessment[$curColumn] - 1; $i++)
					{
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>--</td>";
					}
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnFormPos[$ColumnID]."</td>";
					$curColumn++;
				}
			}
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $FormPosition ."</td>";
			if ($AllowSubjectTeacherComment) {
				$x .= "<td class='border_left $border_top' align='center'>";
				$x .= "<span style='padding-left:4px;padding-right:4px;'>-</span>";
				$x .= "</td>";
			}
			$x .= "</tr>";
		}
		
		##################################################################################
		# CSV related
		##################################################################################
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		
		if($SemID=="F")
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			/*
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$InfoTermID = $TermID+1;
				$InfoTermID = $TermID;
				
				if(!empty($csvType))
				{
					foreach($csvType as $k=>$Type)
					{

						$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
						if(!empty($csvData)) 
						{
							foreach($csvData as $RegNo=>$data)
							{
								if($RegNo == $WebSamsRegNo)
								{
									foreach($data as $key=>$val)
										$ary[$TermID][$key] = $val;
								}
							}
						}
					}
				}
			}
			*/
			
			# calculate sems/assesment col#
			$ColNum2Ary = array();
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				if($d1['IsDetails']==1)
				{
					# check sems/assesment col#
					//$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
					$thisReport = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $TermID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1;
				}
				else
					$ColNum2Ary[$TermID] = 0;
			}
		}
		else
		{
			/*
			$InfoTermID = $SemID+1;
			if(!empty($csvType))
			{
				foreach($csvType as $k=>$Type)
				{
					$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
					if(!empty($csvData)) 
					{
						foreach($csvData as $RegNo=>$data)
						{
							if($RegNo == $WebSamsRegNo)
							{
								foreach($data as $key=>$val)
									$ary[$SemID][$key] = $val;
							}
						}
					}
				}
			}
			*/
		}
		
		$ary = $this->getReportOtherInfoData($ReportID, $StudentID);
		$ary = $ary[$StudentID];
	
		$border_top = $first ? "border_top" : "";
		
		# Days Absent 
		$x .= "<tr>";
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
		$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
		$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['DaysAbsentEn'] ."</td>";
		$x .= "</tr></table>";
		$x .= "</td>";		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
		$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
		$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['DaysAbsentCh'] ."</td>";
		$x .= "</tr></table>";
		$x .= "</td>";
		
		if($ReportType=="W")	# Whole Year Report
		{
			$TotalDayAbsent = $this->EmptySymbol;
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$DaysAbsent = $StudentID ? ($ary[$TermID]['Days Absent'] ? $ary[$TermID]['Days Absent'] : $this->EmptySymbol) : "#";
				if (is_numeric($DaysAbsent)) {
					if ($TotalDayAbsent == "")
						$TotalDayAbsent = $DaysAbsent;
					else if (is_numeric($TotalDayAbsent))
						$TotalDayAbsent += $DaysAbsent;
				}
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>--</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $DaysAbsent ."</td>";
			}
			
			if($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>$TotalDayAbsent</td>";
		}
		else				# Term Report
		{
			$DaysAbsent = $StudentID ? ($ary[$SemID]['Days Absent'] ? $ary[$SemID]['Days Absent'] : $this->EmptySymbol) : "#";
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>--</td>";
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $DaysAbsent ."</td>";
		}
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>-</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		# Times Late 
		$x .= "<tr>";
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
		$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
		$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['TimesLateEn'] ."</td>";
		$x .= "</tr></table>";
		$x .= "</td>";		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
		$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
		$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['TimesLateCh'] ."</td>";
		$x .= "</tr></table>";
		$x .= "</td>";
		
		if($ReportType=="W")	# Whole Year Report
		{
			$TotalTimesLate = $this->EmptySymbol;
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$TimesLate = $StudentID ? ($ary[$TermID]['Time Late'] ? $ary[$TermID]['Time Late'] : $this->EmptySymbol) : "#";
				if (is_numeric($TimesLate)) {
					if ($TotalDayAbsent == "")
						$TotalTimesLate = $TimesLate;
					else if (is_numeric($TotalTimesLate))
						$TotalTimesLate += $TimesLate;
				}
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>--</td>";
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $TimesLate ."</td>";
			}
			
			if($ShowRightestColumn)
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>$TotalTimesLate</td>";
		}
		else				# Term Report
		{
			$TimesLate = $StudentID ? ($ary[$SemID]['Time Late'] ? $ary[$SemID]['Time Late'] : $this->EmptySymbol) : "#";
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>--</td>";
			$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $TimesLate ."</td>";
		}		
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>-</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		# Conduct 
		$x .= "<tr>";
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
		$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
		$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ConductEn'] ."</td>";
		$x .= "</tr></table>";
		$x .= "</td>";		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
		$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
		$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ConductCh'] ."</td>";
		$x .= "</tr></table>";
		$x .= "</td>";
		if($ReportType=="W")	# Whole Year Report
		{
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$Conduct = $StudentID ? ($ary[$TermID]['Conduct'] ? $ary[$TermID]['Conduct'] : $this->EmptySymbol) : "G";
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>--</td>";
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $Conduct ."</td>";
			}
			
			if($ShowRightestColumn)
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>--</td>";
		}
		else				# Term Report
		{
			$Conduct = $StudentID ? ($ary[$SemID]['Conduct'] ? $ary[$SemID]['Conduct'] : $this->EmptySymbol) : "G";
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>--</td>";
			$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'>". $Conduct ."</td>";
		}
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>-</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting;
				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1);
		
		if($ReportType=="T")	# Temrs Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
							
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# define css
				$css_border_top = ($isFirst)? "" : "";
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				$isAllNA = true;
									
				/* Need not show assessment marks for this school
				# Assessment Marks & Display
				for($i=0;$i<sizeof($ColumnID);$i++)
				{
					$thisColumnID = $ColumnID[$i];
					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
					
					if ($isSub && $columnSubjectWeightTemp == 0)
					{
						$thisMarkDisplay = "---";
					}
					else if ($isParentSubject && $CalculationOrder == 1) {
						$thisMarkDisplay = "---";
					} else {
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID=$SubjectID");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnID[$i]][Mark] : "";
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][$ColumnID[$i]][Grade] : "";
						
												
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						
						
						if ($thisMark != "N.A.")
						{
							$isAllNA = false;
						}
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
						if($needStyle)
						{
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
							$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
						
					//	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					//	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
					//	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark; 
					}
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					
  					if($ShowSubjectFullMark)
  					{
	  					# check special full mark
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
	  					
						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
					}
					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}
				*/
				
				# Subject Full Mark
				if($ShowSubjectFullMark)
				{
  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
  					$thisFullMark = ($thisFullMark == '--')? $this->EmptySymbol : $thisFullMark;
					$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center'>". $thisFullMark ."&nbsp;</td>";
				}
				

				# Subject Overall
				$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
				$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][0][Grade] : ""; 
				$thisRank = $isSub?"&nbsp;":$MarksAry[$SubjectID][0]['OrderMeritForm'];
				
				# for preview purpose
				if(!$StudentID)
				{
					$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
					$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					$thisRank 	= "#";
				}
				
				#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
				$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
				
				//2014-0314-1559-21177
				//if ($thisMark != "N.A." && $thisMark != "*" )
				if ($thisMark != "N.A." && $thisMark != "*" && $thisMark != "")
				{
					$isAllNA = false;
				}
					
				# check special case
				list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
				
				if($needStyle)
				{
						if($thisSubjectWeight > 0)
						$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
					else
						$thisMarkTemp = $thisMark;
						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
					$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
				}
				else
					$thisMarkDisplay = $thisMark;
				
				
				
				$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center'>". $thisMarkDisplay ."&nbsp;</td>";
				
				//if (!$this->Is_F7_Graduation_Report($ReportID) && !$this->Is_F5_Graduation_Report($ReportID) && !$this->Is_F6_Graduation_Report($ReportID))
				if (!$this->Is_F7_Graduation_Report($ReportID) && !$this->Is_F5_Graduation_Report($ReportID))
				{
					$SubjectIDCodeIDMap = $this->GET_SUBJECTS_CODEID_MAP();
					
					if(($FormNumber <= 4 || $this->Is_F6_Graduation_Report($ReportID)) && $isSub) {
						$thisRank = '&nbsp;';
					}
						
					if(($FormNumber == 4 || $this->Is_F6_Graduation_Report($ReportID)) && !in_array($SubjectIDCodeIDMap[$SubjectID], $eRCTemplateSetting['HWCoreSubject'])) {
						$thisRank = '&nbsp;';
					}
					
					if($FormNumber < 5 || $this->Is_F6_Graduation_Report($ReportID)) {
						$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top} tabletext'>".(($thisRank > 0 && $thisRank <= 10 && $thisRank != '') ? $thisRank : "&nbsp;")."</td>";
					}
				
					if ($ShowRightestColumn && !$this->Is_F6_Graduation_Report($ReportID)) 
					{
						$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
						if($FormNumber != 5) {
							$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
						}
						if($FormNumber < 5) {
							$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
						}
					}
				}
				
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
							
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				$isAllNA = true;
				
				$css_border_top = ($isFirst)? "" : "";
				
				# Full Mark
				if ($ShowSubjectFullMark)
				{
					if($ShowSubjectFullMark)
  					{
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
	  					$thisFullMark = ($thisFullMark == '--')? $this->EmptySymbol : $thisFullMark;
						$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center'>". $thisFullMark ."&nbsp;</td>";
					}
				}
				
				if (!$this->Is_F5_Graduation_Report($ReportID))
				{
					# Terms's Overall Result (Display Second Term as Whole Year Result)
					for($i=0;$i<sizeof($ColumnData);$i++)
					{
						
						#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
						
						/* Need not show assessment for this school
						$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
						if($isDetails==1)		# Retrieve assesments' marks
						{
							# See if any term reports available
							$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum'] ."' and ClassLevelID=".$ClassLevelID);
							
							# if no term reports, CANNOT retrieve assessment result at all
							if (empty($thisReport)) {
								for($j=0;$j<sizeof($ColumnID);$j++) {
									$x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>--</td>";
								}
							} else {
								$thisReportID = $thisReport['ReportID'];
								$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
								$ColumnID = array();
								$ColumnTitle = array();
								foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
								
								$thisMarksAry = $this->getMarks($thisReportID, $StudentID);		
									 
								for($j=0;$j<sizeof($ColumnID);$j++)
								{
									$thisColumnID = $ColumnID[$i];
									$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
									$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
									$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
									
									if ($isSub && $columnSubjectWeightTemp == 0)
									{
										$thisMarkDisplay = "---";
									}
									else if ($isParentSubject && $CalculationOrder == 1) {
										$thisMarkDisplay = "---";
									} else {
										$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnID[$j] ."' and SubjectID=$SubjectID");
										$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
										
										$thisMark = $ScaleDisplay=="M" ? $thisMarksAry[$SubjectID][$ColumnID[$j]][Mark] : "";
										$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $thisMarksAry[$SubjectID][$ColumnID[$j]][Grade] : ""; 
										
										# for preview purpose
										if(!$StudentID)
										{
											$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
											$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
										}
										$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
														
										if ($thisMark != "N.A.")
										{
											$isAllNA = false;
										}
										
										# check special case
										list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
										if($needStyle)
										{
											$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
											$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
											$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
										}
										else
										{
											$thisMarkDisplay = $thisMark;
										}
									}
										
									$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
									$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
				  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
				  					
				  					if($ShowSubjectFullMark)
				  					{
					  					$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
										$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
									}
									
									$x[$SubjectID] .= "</tr></table>";
									$x[$SubjectID] .= "</td>";
								}
							}
						}
						*/
						//else					
						//{
						# Retrieve Terms Overall marks
						//if ($isParentSubject && $CalculationOrder == 1) {
						//	$thisMarkDisplay = "---";
						//} else {
							
						
						//if ($i > 0)
						//	continue;
							
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
						# if no term reports, the term mark should be entered directly
						if (empty($thisReport)) {
							$thisReportID = $ReportID;
							$MarksAry = $this->getMarks($thisReportID, $StudentID);
							$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
							#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
							$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
							$thisRank = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["OrderMeritForm"] ;
						} else {
							$thisReportID = $thisReport['ReportID'];
							$MarksAry = $this->getMarks($thisReportID, $StudentID);
							$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
							#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
							$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
							$thisRank = $MarksAry[$SubjectID][0]["OrderMeritForm"];
						}
						$thisRank = $isSub?"&nbsp;":$thisRank;
						
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
													
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
							$thisRank 	= '#';
						}
						# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
						
						//2014-0314-1559-21177
						//if ($thisMark != "N.A." && $thisMark != "*" )
						if ($thisMark != "N.A." && $thisMark != "*" && $thisMark != "")
						{
							$isAllNA = false;
						}
					
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
						if($needStyle)
						{
							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
								//$thisMark = ($ScaleDisplay=="M" && $StudentID) ? my_round($thisMark*$thisSubjectWeight,2) : $thisMark;
							$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
						//}
						
						//2014-0707-1021-51066
						if (in_array($StudentID, $this->GET_EXCLUDE_ORDER_STUDENTS($thisReportID))) {
							$thisMarkDisplay = "*";
						}
							
						$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."&nbsp;</td>";
	  					
						
	  					/*
	  					if($ShowSubjectFullMark)
	  					{
							$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1 && $UseWeightedMark) ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID];
							$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
						}
						*/
						
						$x[$SubjectID] .= "</tr></table>";
						$x[$SubjectID] .= "</td>";
						
						//if ($i==0 && !$this->Is_F7_Graduation_Report($ReportID) && !$this->Is_F5_Graduation_Report($ReportID)  && !$this->Is_F6_Graduation_Report($ReportID))
						if ($i==0 && !$this->Is_F7_Graduation_Report($ReportID) && !$this->Is_F5_Graduation_Report($ReportID))
						{
							$SubjectIDCodeIDMap = $this->GET_SUBJECTS_CODEID_MAP();
							if(($FormNumber == 4 || $this->Is_F6_Graduation_Report($ReportID)) && !in_array($SubjectIDCodeIDMap[$SubjectID], $eRCTemplateSetting['HWCoreSubject'])) {
								$thisRank = '&nbsp;';
							}
							
							if($FormNumber < 5 || $this->Is_F6_Graduation_Report($ReportID)) {
								$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top} tabletext'>".(($thisRank <= 10 && $thisRank != '' && $thisRank > 0) ? $thisRank : "&nbsp;")."</td>";
							}
						}
						//}
					}
				}
				
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				
				if($ShowSubjectOverall)
				{
					$MarksAry = $this->getMarks($ReportID, $StudentID);		
					
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][0][Grade] : ""; 
					$thisRank = $isSub?"&nbsp;":$MarksAry[$SubjectID][0]["OrderMeritForm"] ;
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						$thisRank 	= '#';
					}
					
					//$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					//2014-0314-1559-21177
					//if ($thisMark != "N.A." && $thisMark != "*" )
					if ($thisMark != "N.A." && $thisMark != "*" && $thisMark != "")
					{
						$isAllNA = false;
					}
						
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."&nbsp;</td>";
  					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
					
					if (!$this->Is_F7_Graduation_Report($ReportID) && !$this->Is_F5_Graduation_Report($ReportID) && !$this->Is_F6_Graduation_Report($ReportID))
					{
						$SubjectIDCodeIDMap = $this->GET_SUBJECTS_CODEID_MAP();
						if($FormNumber == 4 && !in_array($SubjectIDCodeIDMap[$SubjectID],	$eRCTemplateSetting['HWCoreSubject']))
							$thisRank = '&nbsp;';
						
						if($FormNumber < 5 )
							$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top} tabletext'>".($thisRank>0 && $thisRank<=10 && $thisRank!=''?$thisRank:"&nbsp;")."</td>";
					
					}
				//} else if ($ShowRightestColumn) {
					
					
					//if (!$this->Is_F5_Graduation_Report($ReportID) && $ShowRightestColumn)
					//{
					//	$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
					//}
					
				}
				
				
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		}	# End Whole Year Report Type
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		# Retrieve Basic Information of Report
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 						= $ReportSetting['Semester'];
		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$LineHeight 				= $ReportSetting['LineHeight'];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

		# retrieve the latest Term
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		foreach($sems as $TermID=>$TermName)
			$latestTerm = $TermID;
		//$latestTerm++;
		
		# retrieve Form Number
		$currentFormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID,$StudentID);
		$ary = $OtherInfoDataAry[$StudentID][$latestTerm];
			
		# retrieve Student Class
		/*
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		foreach($csvType as $k=>$Type)
		{
			if(!$csvData = $this->getOtherInfoData($Type, 0, $ClassName))	
				$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);	
				
			if(!empty($csvData)) 
			{
				
				foreach($csvData as $RegNo=>$data)
				{
					if($RegNo == $WebSamsRegNo)
					{
	 					foreach($data as $key=>$val)
		 					$ary[$key] = $val;
					}
				}
			}
		}
		*/
		
		/* Need not merit data for this school
		# retrieve result data
		$merit = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		$merit .= "<tr>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['Merits'] . ": " . ($ary['Merits'] ? $ary['Merits'] :0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['Demerits'] . ": " . ($ary['Demerits'] ? $ary['Demerits'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorCredit'] . ": " . ($ary['Minor Credit'] ? $ary['Minor Credit'] :0) ."</td>";
		$merit .= "</tr>";
		$merit .= "<tr>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorCredit'] . ": " . ($ary['Major Credit'] ? $ary['Major Credit'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorFault'] . ": " . ($ary['Minor Fault'] ? $ary['Minor Fault'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorFault'] . ": " . ($ary['Major Fault'] ? $ary['Major Fault'] : 0) ."</td>";
		$merit .= "</tr>";
		$merit .= "</table>";
		*/
		
		// [2015-1008-1356-44164]
		# Rank Table data
		$dayRank = ($ary['Rank']=="" || $ary['Rank']>90)? "--" : $ary['Rank'];
		$reportResultInfo = $this->getReportResultScore($ReportID, 0, "", '', 0, 1);
		if(is_array($reportResultInfo) && count($reportResultInfo)>0){
			$reportResultInfo = reset($reportResultInfo);
		}
		$formStudentNo = ($reportResultInfo[0]["FormNoOfStudent"]=="")? "--" : $reportResultInfo[0]["FormNoOfStudent"];
		
		# Attendance Table data
		$dayAbsent = ($ary['Days Absent']=="")? 0 : $ary['Days Absent'];
		$timeLate = ($ary['Time Late']=="")? 0 : $ary['Time Late'];
		
		# Class teacher comment Table data
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$classteachercomment = $CommentAry[0];
		
		# Award Table data
		$award = $ary['Award'];
		
		# Remark Table data
		$remark = $ary['Remark'];
		
		$prefix = "&nbsp;&nbsp;";
		
		// [2015-1008-1356-44164]
		if($currentFormNumber && $currentFormNumber < 4){
			$x .= "<br>";
			# Rank Table
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='report_border'>";
				# Rank
				$x .= "<tr>";
				$x .= "<td class='tabletext' width='22%'>".$prefix.$eReportCard['Template']['FormRank'].$prefix.$prefix."</td>";
				//$x .= "<td class='tabletext' width='35%'>".$dayRank."/".$formStudentNo."</td>";
				$x .= "<td class='tabletext' width='78%'>".$dayRank."/".$formStudentNo."</td>";
				$x .= "</tr>";
				
				# Rank Remark
//				$x .= "<tr>";
//				$x .= "<td class='tabletext' colspan='2' nowrap>&nbsp;</td>";
//				$x .= "</tr>";
				$x .= "<tr>";
				$x .= "<td class='tabletext' colspan='2' nowrap>".$prefix.$eReportCard['Template']['FormRankRemarks']."</td>";
				$x .= "</tr>";
			$x .= "</table>";
		}
		
		$x .= "<br>";
		# Attendance Table
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='report_border'>";
			# Absence
			$x .= "<tr>";
			$x .= "<td class='tabletext' nowrap>".$prefix.($ReportType=="W"? $eReportCard['Template']['SecondTermCh'] : "").$eReportCard['Template']['Absence'].$prefix.$prefix."</td>";
			$x .= "<td class='tabletext' width='100%'>".$dayAbsent." ".$eReportCard['Template']['DayUnit']."</td>";
			$x .= "</tr>";
			
			# Lateness
			$x .= "<tr>";
			$x .= "<td class='tabletext' nowrap>".$prefix.($ReportType=="W"? $eReportCard['Template']['SecondTermCh'] : "").$eReportCard['Template']['Lateness'].$prefix.$prefix."</td>";
			$x .= "<td class='tabletext'>".$timeLate." ".$eReportCard['Template']['TimeUnit']."</td>";
			$x .= "</tr>";
		$x .= "</table>";
		
		$TableHeight = $LineHeight * 3;
		
		# Class Teacher Comment Table
		if($AllowClassTeacherComment)
		{
			$x .= "<br>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='report_border' height='$TableHeight'>";
				$x .= "<tr>";
					$x .= "<td class='tabletext' valign='top' width='1' nowrap>".$prefix."</td>";
					$x .= "<td class='tabletext'>".$eReportCard['Template']['ClassTeacherComment']."</td>";
				$x .= "</tr>";
				$x .= "<tr>";
					$x .= "<td class='tabletext' valign='top' nowrap>".$prefix."</td>";
					$x .= "<td class='tabletext'>".nl2br($classteachercomment)."</td>";
				$x .= "</tr>";
			$x .= "</table>";
		} 

		# Awards Table
		if ($award!=NULL || $award!="")
		{
			$x .= "<br>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='report_border' height='$TableHeight'>";
				$x .= "<tr><td class='tabletext' valign='top' nowrap>".$prefix.$eReportCard['Template']['Award'].$prefix.$prefix."</td></tr>";
				
				if (is_array($award))
				{
					for ($i=0; $i<count($award); $i++)
					{
						$thisAward = $award[$i];
						$x .= "<tr><td class='tabletext' valign='top' width='100%'>".$prefix.$thisAward."</td></tr>";
					}
				}
				else
				{
					$x .= "<tr><td class='tabletext' valign='top' width='100%'>".$prefix.$award."</td></tr>";
				}
				$x .= "</tr>";
			$x .= "</table>";
		}
		
		# Remark Table
		if ($remark!=NULL || $remark!="")
		{
			$x .= "<br>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='report_border' height='$TableHeight'>";
				$x .= "<tr>";
					$x .= "<td class='tabletext' valign='top' width='1' nowrap>".$prefix."</td>";
					$x .= "<td class='tabletext'>".$eReportCard['Template']['Remark']."</td>";
				$x .= "</tr>";
				if (is_array($remark))
				{
					for ($i=0; $i<count($remark); $i++)
					{
						$thisRemark = $remark[$i];
						$x .= "<tr>";
							$x .= "<td class='tabletext' valign='top' nowrap>".$prefix."</td>";
							$x .= "<td class='tabletext'>".$thisRemark."</td>";
						$x .= "</tr>";
					}
				}
				else
				{
					$x .= "<tr>";
						$x .= "<td class='tabletext' valign='top' nowrap>".$prefix."</td>";
						$x .= "<td class='tabletext'>".$remark."</td>";
					$x .= "</tr>";
				}
				$x .= "</tr>";
			$x .= "</table>";
		}
		
		return $x;
	}
	
	function getConductTable($ReportID, $StudentID)
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$prefix = $this->prefix;

		# Get CSV data
		# retrieve the latest Term
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		foreach($sems as $TermID=>$TermName)
			$latestTerm = $TermID;
		//$latestTerm++;
		
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID,$StudentID);
		$ary = $OtherInfoDataAry[$StudentID][$latestTerm];
		
		/*
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		foreach($csvType as $k=>$Type)
		{
			$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);	
			if(!empty($csvData)) 
			{
				foreach($csvData as $RegNo=>$data)
				{
					if($RegNo == $WebSamsRegNo)
					{
	 					foreach($data as $key=>$val)
		 					$ary[$key] = $val;
					}
				}
			}
		}
		*/
		
		# Construct Table
		$FullMarkTitle = $eReportCard['Template']['SchemesFullMarkCh']." ".$eReportCard['Template']['SchemesFullMarkEn'];
		$StudentPerformanceTitle = $eReportCard['Template']['StudentPerformanceCh']." ".$eReportCard['Template']['StudentPerformanceEn'];
		
		$conductTable = "";
		
		$conductTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
			# Title
			$conductTable .= "<tr height='$LineHeight'>";
				$conductTable .= "<td class=\"tabletext\">". $eReportCard['Template']['ConductTableTitle'] ."</td>";
			$conductTable .= "</tr>";
		    
			# Table
			$conductTable .= "<tr height='$LineHeight'><td>";
				$conductTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='report_border'>";
					# Full Mark, Student Performance
					$conductTable .= "<tr height='$LineHeight'>";
						$conductTable .= "<td width=\"40%\">&nbsp;</td>";
						$conductTable .= "<td width=\"25%\" class=\"border_left tabletext\" align=\"center\">". $FullMarkTitle ."</td>";
						$conductTable .= "<td width=\"35%\" class=\"border_left tabletext\" align=\"center\">". $StudentPerformanceTitle ."</td>";
					$conductTable .= "</tr>";
					
					# Conduct Grade
					$thisTitle = $eReportCard['Template']['ConductGradeCh']." ".$eReportCard['Template']['ConductGradeEn'];
					$thisFullMark = $this->ConductFullMarkArr[0];
					$thisMark = $ary['Conduct'];
					$conductTable .= "<tr height='$LineHeight'>";
						$conductTable .= "<td width=\"40%\" class=\"border_top tabletext\">".$prefix.$thisTitle."</td>";
						$conductTable .= "<td width=\"25%\" class=\"border_left border_top tabletext\" align=\"center\">". $thisFullMark ."</td>";
						$conductTable .= "<td width=\"35%\" class=\"border_left border_top tabletext\" align=\"center\">". $thisMark ."&nbsp;</td>";
					$conductTable .= "</tr>";
					
					# 品德言行
					//$thisTitle = $eReportCard['Template']['ConductOverall'];
					$thisTitle = $eReportCard['Template']['ConductOverallCh']." ".$eReportCard['Template']['ConductOverallEn']."：";
					$conductTable .= "<tr height='$LineHeight'>";
						$conductTable .= "<td width=\"40%\" class=\"border_top tabletext\">".$prefix.$thisTitle."</td>";
						$conductTable .= "<td width=\"25%\" class=\"border_left border_top tabletext\" align=\"center\">". "&nbsp;" ."</td>";
						$conductTable .= "<td width=\"35%\" class=\"border_left border_top tabletext\" align=\"center\">". "&nbsp;" ."</td>";
					$conductTable .= "</tr>";
					
					for ($i=1; $i<count($this->ConductItemArr)-1; $i++)
					{
					    $thisPrefix = $prefix.$prefix."--";
						$thisItem = $this->ConductItemArr[$i];
						$thisTitleEn = $eReportCard['Template'][$thisItem.'En'];
						$thisTitleCh = $eReportCard['Template'][$thisItem.'Ch'];
						$thisTitle = $thisPrefix." ".$thisTitleCh." ".$thisTitleEn;
						$thisFullMark = $this->ConductFullMarkArr[$i];
						$thisMark = $ary[$thisItem];
					    
						$conductTable .= "<tr height='$LineHeight'>";
						$conductTable .= "<td width=\"40%\" class=\" tabletext\">". $thisTitle ."</td>";
						$conductTable .= "<td width=\"25%\" class=\"border_left tabletext\" align=\"center\">". $thisFullMark ."</td>";
						$conductTable .= "<td width=\"35%\" class=\"border_left tabletext\" align=\"center\">". $thisMark ."&nbsp;</td>";
						$conductTable .= "</tr>";
					}
					
					# Discipline
					# Conduct Grade
					$thisTitle = $eReportCard['Template']['Discipline RequirementsCh']." ".$eReportCard['Template']['Discipline RequirementsEn'];
					$thisFullMark = $this->ConductFullMarkArr[count($this->ConductItemArr)-1];
					$thisMark = $ary['Discipline Requirements'];
					$conductTable .= "<tr height='$LineHeight'>";
						$conductTable .= "<td width=\"40%\" class=\"border_top tabletext\">".$prefix.$thisTitle."</td>";
						$conductTable .= "<td width=\"25%\" class=\"border_left border_top tabletext\" align=\"center\">". $thisFullMark ."</td>";
						$conductTable .= "<td width=\"35%\" class=\"border_left border_top tabletext\" align=\"center\">". $thisMark ."&nbsp;</td>";
					$conductTable .= "</tr>";
					
				$conductTable .= "</table>";
			$conductTable .= "</td></tr>";
			
			# Service Hour
			# show if have service hour only
			if ($ary['Service Hour'])
			{
				$thisDisplay = $eReportCard['Template']['ServiceHourCh']." ".$eReportCard['Template']['ServiceHourEn']." : ".$ary['Service Hour']." ".$eReportCard['Template']['HourUnit'];
				$thisConductBonusDisplay = "";
				if ($ary['ConductBonus'])
				{
					$thisConductBonusDisplay = "&nbsp;&nbsp;&nbsp; ".$eReportCard['Template']['ConductBonus'];
					$thisConductBonusDisplay = str_replace("<!--ConductBonus-->", $ary['ConductBonus'], $thisConductBonusDisplay);
				}
				$conductTable .= "<tr height='$LineHeight'>";
					$conductTable .= "<td class=\"tabletext\">". $thisDisplay . $thisConductBonusDisplay. "</td>";
				$conductTable .= "</tr>";
			}
			else
			{
				# Empty Line
				$conductTable .= "<tr height='$LineHeight'><td class=\"tabletext\">&nbsp;</td></tr>";
			}
			
		$conductTable .= "</table>";
		
		return $conductTable;
	}
	
	
	function getPostTable($ReportID, $StudentID)
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$SemID = $ReportSetting['Semester'];
		$prefix = $this->prefix;
        $thisSchoolYear = intval($this->schoolYear);
		
		# Retrieve Service Data
		//$Year = $this->GET_ACTIVE_YEAR("-");
		//$Semester = $this->returnSemesters($SemID);
		
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		$ObjAcademicYear = new academic_year($this->schoolYearID);
		$Year = $ObjAcademicYear->YearNameEN;
		
		if ($SemID != 'F')
        {
			$Semester = $this->returnSemesters($SemID, 'en');
        }
		else
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			$numOfColumn = count($ColumnData);
			$thisSemID = $ColumnData[$numOfColumn-1]['SemesterNum'];
			$Semester = $this->returnSemesters($thisSemID, 'en');
		}
		
		$sql = "SELECT 
						Organization, Role 
				FROM 
						PROFILE_STUDENT_SERVICE 
				WHERE 
						UserID = '$StudentID' AND 
						Year = '".$this->Get_Safe_Sql_Query($Year)."' AND 
						Semester = '".$this->Get_Safe_Sql_Query($Semester)."' 
				ORDER BY
						Organization, Role
				";
		$ServiceArr = $this->returnArray($sql, 2);
		
//		hdebug_r('sql = '.$sql);
//		hdebug_r($ServiceArr);
		
		# Construct Table
		$PostTable = "";
		$PostTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
			# Title
			$PostTable .= "<tr height='$LineHeight'>";
            // [2020-0811-1542-20206]
			if($thisSchoolYear >= 2020) {
                $PostTable .= "<td class=\"tabletext\">". $eReportCard['Template']['PostTableTitleV2'] ."</td>";
            } else {
				$PostTable .= "<td class=\"tabletext\">". $eReportCard['Template']['PostTableTitle'] ."</td>";
            }
			$PostTable .= "</tr>";
		
			# Table
			$PostTable .= "<tr height='$LineHeight'><td>";
				$PostTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='report_border' height='140'>";
					# Student Organization, Position
					$PostTable .= "<tr height='$LineHeight'>";
					// [2020-0811-1542-20206]
                    if($thisSchoolYear >= 2020) {
                        $PostTable .= "<td width=\"40%\" class='tabletext' valign='top'>". $prefix.$eReportCard['Template']['StudentOrganizationV2'] ."</td>";
                    } else {
                        $PostTable .= "<td width=\"40%\" class='tabletext' valign='top'>". $prefix.$eReportCard['Template']['StudentOrganization'] ."</td>";
                    }
						$PostTable .= "<td width=\"60%\" class='tabletext' valign='top'>". $eReportCard['Template']['Position'] ."</td>";
					$PostTable .= "</tr>";
					
					# Content
					for ($i=0; $i<$this->MaxPostDisplay; $i++)
					{
						$thisOrganization = $ServiceArr[$i]['Organization'];
						$thisPosition = $ServiceArr[$i]['Role'];
						
						$PostTable .= "<tr height='$LineHeight'>";
						$PostTable .= "<td width=\"40%\" class='MISC_text' valign='top'>". $prefix.$thisOrganization ."&nbsp;</td>";
						$PostTable .= "<td width=\"60%\" class='MISC_text' valign='top'>". $thisPosition ."&nbsp;</td>";
						$PostTable .= "</tr>";
					}
					
				$PostTable .= "</table>";
			$PostTable .= "</td></tr>";
		$PostTable .= "</table>";
		
		return $PostTable;
		
	}
	
	function getActivitiesTable($ReportID, $StudentID)
	{
		$InternalActivityTable = $this->genActivityTable($ReportID, $StudentID, "INTERNAL");
		$ExternalActivityTable = $this->genActivityTable($ReportID, $StudentID, "EXTERNAL");
		
		$ActivitiesTable = "";
		$ActivitiesTable .= $InternalActivityTable;
		$ActivitiesTable .= "<tr><td height='1'>&nbsp;</td></tr>";
		$ActivitiesTable .= $ExternalActivityTable;
		
		return $ActivitiesTable;
	}
	
	function genActivityTable($ReportID, $StudentID, $Type)
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$LineHeight = $ReportSetting['LineHeight'];
		$SemID = $ReportSetting['Semester'];
		$prefix = $this->prefix;
		
		# Retrieve Service Data
		//$Year = $this->GET_ACTIVE_YEAR("-");
		//$Semester = $this->returnSemesters($SemID);
		
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		$ObjAcademicYear = new academic_year($this->schoolYearID);
		$Year = $ObjAcademicYear->YearNameEN;
		
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		if ($SemID != 'F')
		{
			$Semester = $this->returnSemesters($SemID, 'en');
		}
		else
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			$numOfColumn = count($ColumnData);
			$thisSemID = $ColumnData[$numOfColumn-1]['SemesterNum'];
			$Semester = $this->returnSemesters($thisSemID, 'en');
		}
		
		// 2011-0511-0922-23073 - 漢華中學 - Change the school term name for activity
		// If the Activity Semester is empty => Show whole year
		// 2012-0418-0922-29066 - 漢華中學 - School Activities Records in eRC template for S.6 & S.7
		// Retrieve whole year activity only for F6 and F7
		//if ($FormNumber == 7) {
		if ($FormNumber == 6 || $FormNumber == 7) {
			//$conds_Semester = " AND (Semester Is Null Or Semester = '' Or Semester = '".$this->Get_Safe_Sql_Query($Semester)."') ";
			$conds_Semester = " AND (Semester Is Null Or Semester = '') ";
		}
		else {
			$conds_Semester = " AND Semester = '".$this->Get_Safe_Sql_Query($Semester)."' ";
		}
		
		
		if ($Type == "INTERNAL")
		{
			$conds_RecordType = " AND RecordType = 1 ";
		}
		else if ($Type == "EXTERNAL")
		{
			$conds_RecordType = " AND RecordType = 2 ";
		}
		
//		$sql = "
//				SELECT 
//						Year as Date, 
//						ActivityName, 
//						Organization, 
//						Performance,
//						if (MONTH(Year) > 8,
//							CONCAT(YEAR(Year),'-',YEAR(Year)+1),
//							CONCAT(YEAR(Year)-1,'-',YEAR(Year))) AS ActivitySchoolYear
//				FROM 
//						PROFILE_STUDENT_ACTIVITY
//				WHERE
//						UserID = '$StudentID'
//						AND
//						Semester = '".$this->Get_Safe_Sql_Query($Semester)."'
//						$conds
//				ORDER BY 
//						Year, ActivityName, Organization
//				";
		$sql = "
				SELECT 
						/* Semester as Date, */
						Semester,
						ActivityDate as Date,
						ActivityName, 
						Organization,
						Performance,
						if (MONTH(Year) > 8,
							CONCAT(YEAR(Year),'-',YEAR(Year)+1),
							CONCAT(YEAR(Year)-1,'-',YEAR(Year))) AS ActivitySchoolYear
				FROM 
						PROFILE_STUDENT_ACTIVITY
				WHERE
						UserID = '$StudentID'
						And Year = '".$this->Get_Safe_Sql_Query($Year)."'
						$conds_Semester
						$conds_RecordType
				ORDER BY 
						Year, ActivityDate, ActivityName, Organization
				";
		$ActivityInfoArr = $this->returnArray($sql, 5);
		
		# Construct Table
		$ActivityTable = "";
		$ActivityTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
			# Title
			$thisTitle = ($Type == "INTERNAL")? $eReportCard['Template']['InternalActivityTitle'] : $eReportCard['Template']['ExternalActivityTitle'];
			$ActivityTable .= "<tr height='$LineHeight'>";
				$ActivityTable .= "<td class=\"tabletext\">". $thisTitle ."</td>";
			$ActivityTable .= "</tr>";
		
			# Table
			$ActivityTable .= "<tr height='$LineHeight'><td>";
				$ActivityTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='report_border' height='160'>";
					
					$ActivityTable .= "<tr height='$LineHeight'>";
						$ActivityTable .= "<td width=\"20%\" class='tabletext' valign='top'>". $prefix.$eReportCard['Template']['ActivitySemester'] ."</td>";
						//$ActivityTable .= "<td width=\"20%\" class='tabletext' valign='top'>". $prefix.$eReportCard['Template']['ActivityDate'] ."</td>";
						$ActivityTable .= "<td class='tabletext' valign='top'>". $eReportCard['Template']['ActivityItem'] ."</td>";
						$ActivityTable .= "<td width=\"25%\" class='tabletext' valign='top' align='center'>". $eReportCard['Template']['ActivityOrganizer'] ."</td>";
						// [2015-1008-1356-44164] show merits for both internal and external activities
//						if ($Type == "INTERNAL")
//						{
//							$ActivityTable .= "<td width=\"20%\" class='tabletext' valign='top'>&nbsp;</td>";
//						}
//						else
//						{
//							$ActivityTable .= "<td width=\"20%\" class='tabletext' valign='top'>". $eReportCard['Template']['ActivityMerit'] ."</td>";
//						}
						$ActivityTable .= "<td width=\"20%\" class='tabletext' valign='top' align='center'>". $eReportCard['Template']['ActivityMerit'] ."</td>";
					$ActivityTable .= "</tr>";
					
					# Content
					$numShown = 0;
					for ($i=0; $i<sizeof($ActivityInfoArr); $i++)
					{
						if ($numShown >= $this->MaxActivitiesDisplay)
							break;
							
						$thisSemester = $ActivityInfoArr[$i]['Semester'];
						//$thisDate = $this->convertDateFormat($ActivityInfoArr[$i]['Date']);
						//$thisSemester = ($thisSemester == '')? $eReportCard['Template']['WholeYearCh'] : $thisSemester;
						$thisSemester = ($thisSemester == '')? $eReportCard['Template']['WholeYearEn'] : $thisSemester;
						$thisItem = $ActivityInfoArr[$i]['ActivityName'];
						$thisOrganizer = $ActivityInfoArr[$i]['Organization'];
						$thisMerit = $ActivityInfoArr[$i]['Performance'];
						$thisActivitySchoolYear = $ActivityInfoArr[$i]['ActivitySchoolYear'];
						
						$thisOrganizer = str_replace('?', '', $thisOrganizer);
						
						/* updated on 18 Dec 2008 by Ivan
						 * Check Semester only, no need check exact date
						# skip activities which are not current year
						if ($thisActivitySchoolYear != $thisSchoolYear)
						{
							continue;
						}
						*/
						
						$ActivityTable .= "<tr height='$LineHeight'>";
						$ActivityTable .= "<td class='MISC_text' valign='top'>". $prefix.$thisSemester ."&nbsp;</td>";
						//$ActivityTable .= "<td class='MISC_text' valign='top'>". $prefix.$thisDate ."&nbsp;</td>";
						$ActivityTable .= "<td class='MISC_text' valign='top'>". $thisItem ."&nbsp;</td>";
						$ActivityTable .= "<td class='MISC_text' valign='top' align='center'>". $thisOrganizer ."&nbsp;</td>";
						// [2015-1008-1356-44164] show merits for both internal and external activities
//						if ($Type == "INTERNAL")
//						{
//							$ActivityTable .= "<td class='MISC_text' valign='top'>&nbsp;</td>";
//						}
//						else
//						{
//							$ActivityTable .= "<td class='MISC_text' valign='top'>". $thisMerit ."&nbsp;</td>";
//						}
						$ActivityTable .= "<td class='MISC_text' valign='top' align='center'>". $thisMerit ."&nbsp;</td>";
						
						$ActivityTable .= "</tr>";
						
						$numShown++;
					}
					
					$remain = $this->MaxActivitiesDisplay - $numShown;
					for ($i=0; $i<$remain; $i++)
					{
						$ActivityTable .= "<tr height='$LineHeight'>";
						$ActivityTable .= "<td class='MISC_text' valign='top'>&nbsp;</td>";
						$ActivityTable .= "<td class='MISC_text' valign='top'>&nbsp;</td>";
						$ActivityTable .= "<td class='MISC_text' valign='top'>&nbsp;</td>";
						$ActivityTable .= "<td class='MISC_text' valign='top'>&nbsp;</td>";
						$ActivityTable .= "</tr>";
					}
					
				$ActivityTable .= "</table>";
			$ActivityTable .= "</td></tr>";
		$ActivityTable .= "</table>";
		
		return $ActivityTable;
	}
	
	########### END Template Related
	
	/*
	 *	Convert Date formate from YYYY-MM-DD to DD/MM/YYYY
	 */
	function convertDateFormat($ParDate)
	{
		$DateArr = explode("-", $ParDate);
		$DateArr = array_reverse($DateArr);
		$convertedDate = implode("/", $DateArr);
		
		return $convertedDate;
	}

	function Is_F6_Graduation_Report($ReportID)
	{
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
 		
		if ($this->GET_FORM_NUMBER($ClassLevelID) == 6)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
		
	function Is_F7_Graduation_Report($ReportID)
	{
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
 		
		if ($this->GET_FORM_NUMBER($ClassLevelID) == 7)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	function Is_F5_Graduation_Report($ReportID)
	{
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
 		
 		if ($this->GET_FORM_NUMBER($ClassLevelID) == 5)
		{
			$table = $this->DBName.".RC_REPORT_TEMPLATE";
			$sql = "	
				SELECT 
						a.ReportID,
						a.Semester
				FROM 
						$table as a
						left join YEAR as b on a.ClassLevelID = b.YearID
				WHERE 
						a.ClassLevelID = '$ClassLevelID'
						And
						a.isMainReport = 1
				ORDER BY
						a.Semester
			";
			$allReportIDArr = $this->returnArray($sql);
			$targetReportID = $allReportIDArr[1]["ReportID"];
			
			if ($ReportID == $targetReportID)
				return 1;
			else
				return 0;
		}
		else
		{
			return 0;
		}
	}
	
	//for 2011-0103-1551-08073 - 漢華中學 - Replace the S4 template to S5
	function GET_FORM_NUMBER($ClassLevelID)
	{
		$FormNumber = parent::GET_FORM_NUMBER($ClassLevelID);
		if($FormNumber==5)
			$FormNumber = 4;
			
		return $FormNumber;
	}
}
?>