<?php
// Editing by 
/*
 * !!!!! This script is called by crontab in certain interval. !!!!!
 * 2016-08-10 (Carlos): Synchronize attendance data from MyIT School attendance data source(SQL Server iMS db).
 */

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbmssql.php");

if(!$sys_custom['StudentAttendance']['SyncAttendance']){
	exit;
}

$log_start_time = time();

intranet_opendb();

$lang = isset($intranet_session_language) && $intranet_session_language != '' ? $intranet_session_language : 'en';

$log_path = $intranet_root.'/file/sync_attendance.log';
$log_path_tmp = $intranet_root.'/file/sync_attendance_tmp.log';
$num_line_to_keep = 200000;
$log_content = '';

$host = isset($SQL_SERVER_CONFIG['host'])? $SQL_SERVER_CONFIG['host'] : '192.168.1.8:1138';
$dbname = isset($SQL_SERVER_CONFIG['db'])? $SQL_SERVER_CONFIG['db'] : 'iMS';
$username = isset($SQL_SERVER_CONFIG['username'])? $SQL_SERVER_CONFIG['username'] : 'eclass';
$password = isset($SQL_SERVER_CONFIG['password'])? $SQL_SERVER_CONFIG['password'] : 'yzklhnl3z';
$is_debug = isset($SQL_SERVER_CONFIG['debug']) && $SQL_SERVER_CONFIG['debug']? $SQL_SERVER_CONFIG['debug'] : false;

$GeneralSetting = new libgeneralsettings();
$mssql = new libdbmssql($dbname, $host, $username, $password);

if($mssql->opendb()){
	
	//$start_date = getStartDateOfAcademicYear(Get_Current_Academic_Year_ID());
	//if($start_date == 0){
	$start_date = date("Y-m-d")." 00:00:00";
	//}
	//$start_date = date("Y-m-d", strtotime($start_date))." 00:00:00";
	
	$settings = $GeneralSetting->Get_General_Setting('StudentAttendance',array("'LastRecordTime'"));
	
	$last_sync_time = $settings['LastRecordTime'] != ''? $settings['LastRecordTime'] : $start_date;
	
	if($is_debug){
		$log_content .= "Synchronizing on ".date("Y-m-d H:i:s")." with records after $last_sync_time.\n";
	}
	
	$sql = "SELECT TOP 50 convert(varchar(19), t.TransactionDate, 120) as TransactionDate, t.TransactionTypeID, t.CardID 
			FROM tbTransaction as t 
			WHERE t.TransactionDate > '$last_sync_time' AND t.CardID<>'' 
			ORDER BY t.TransactionDate ASC";
	$records = $mssql->returnResultSet($sql);
	$records_size = count($records);
	
	// workaround to avoid missing same time records after the 50th record
	if($records_size > 0)
	{
		$sql = "SELECT convert(varchar(19), t.TransactionDate, 120) as TransactionDate, t.TransactionTypeID, t.CardID 
				FROM tbTransaction as t 
				WHERE convert(varchar(19), t.TransactionDate, 120)='".$records[$records_size-1]['TransactionDate']."' AND t.CardID<>'' AND t.CardID<>'".$records[$records_size-1]['CardID']."'
				ORDER BY t.TransactionDate ASC";
		$records2 = $mssql->returnResultSet($sql);
		if(count($records2)>0){
			$records = array_merge($records, $records2);
			$records_size = count($records);
		}
		
		$sql = "SELECT convert(varchar(19), t.TransactionDate, 120) as TransactionDate, t.TransactionTypeID, t.CardID 
				FROM tbTransaction as t 
				WHERE convert(varchar(19), t.TransactionDate, 120)='$last_sync_time' AND t.CardID<>'' 
				ORDER BY t.TransactionDate ASC";
		$records3 = $mssql->returnResultSet($sql);
		if(count($records3)>0){
			$records = array_merge($records3, $records);
			$records_size = count($records);
		}
	}
	
	if($records_size > 0)
	{	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$url = "http://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"")."/cardapi/attendance/receiver_csharp.php";
		
		$last_record_time = $records[$records_size-1]['TransactionDate'];
		
		for($i=0;$i<$records_size;$i++){
			//debug_pr($records);
			$card_id = FormatSmartCardID(hexdec(substr($records[$i]['CardID'], -6)));
			$ts = strtotime($records[$i]['TransactionDate']);
			$external_date = date("Y-m-d",$ts);
			$external_time = date("H:i:s",$ts);
			$tapcard_url = $url."?CardID=".$card_id."&sitename=&external_date=".$external_date."&external_time=".$external_time."&dlang=".$lang;
			curl_setopt($ch, CURLOPT_URL, $tapcard_url);
			$response = curl_exec($ch);
			
			if($is_debug){
				$log_content .= $tapcard_url."\n".$response."\n";
			}
			
			usleep(100);
		}
		
		
		$setting_values['LastRecordTime'] = $last_record_time;
		$GeneralSetting->Save_General_Setting('StudentAttendance',$setting_values);
		
		curl_close($ch);
	}
	
	$mssql->closedb();
	
	$log_end_time = time();
	if($is_debug){
		$log_content .= "Elapsed ".($log_end_time - $log_start_time)." second(s).\n";
		
		$fh = fopen($log_path, "a");
		if($fh){
			fwrite($fh, $log_content);
			fclose($fh);
		}
		
		$line_count = intval(shell_exec("cat '".OsCommandSafe($log_path)."' | wc -l"));
		if($line_count > $num_line_to_keep){
			shell_exec("tail -n ".($num_line_to_keep/2)." '".OsCommandSafe($log_path)."' > '".OsCommandSafe($log_path_tmp)."'"); // keep the last $num_line_to_keep/2 lines
			shell_exec("rm '".OsCommandSafe($log_path)."'");
			shell_exec("mv '".OsCommandSafe($log_path_tmp)."' '".OsCommandSafe($log_path)."'");
		}
	}
	
}else{
	$x = 'Cannot connect to SQL Server '.$host.'.'."\n";
	echo $x;
}

intranet_closedb();
?>