<?
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgeneralsettings.php");

# Customized
if ($sys_custom['QualiEd_StudentAttendance'])
{
    header("Location: receiver_q.php?CardID=$CardID&sitename=$sitename&dlang=$dlang");
    exit();
}

intranet_opendb();

// get all general setting for student attendance
$GeneralSetting = new libgeneralsettings();
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance');

include_once("attend_functions.php");
if ($dlang != "b5") $intranet_session_language = "en";
else $intranet_session_language = "b5";

include_once("attend_lang.php");
include_once("attend_const.php");

if ($CardID == "")
{
    # Card not registered
    echo output_env_str(CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered']);
    intranet_closedb();
    exit();
}

######################################################
# Param : (From QueryString)
#    CardID
#    sitename
######################################################

# $db_engine (from functions.php)

# Check IP
if (!isIPAllowed())
{
     echo output_env_str(CARD_RESP_INVALID_IP."###".$attend_lang['InvalidIP']);
     intranet_closedb();
     exit();
}



################################################
$directProfileInput = false;
$getremind = true;
$reminder_null_text = "No Reminder";
################################################


# 1. Get UserInfo
$namefield = getNameFieldByLang();
$sql = "SELECT UserID, $namefield, RecordType, ClassName, ClassNumber, UserLogin FROM INTRANET_USER WHERE CardID = '$CardID'";
echo "sql [".$sql."]<br><br><br>";
$temp = $db_engine->returnArray($sql,6);
list($targetUserID , $targetName, $targetType, $targetClass, $targetClassNumber, $targetUserLogin) = $temp[0];

#GET USER BELONG TO WHICH GROUP, GROUP IS CLASS TYPE (3)
$groupIDString = getUserGroupID($targetUserID);
$aryGroupIDList = getUserGroupIDArray($targetUserID);
echo "<hr>";
for($i = 0; $i< sizeof($aryGroupIDList); $i++)
{
	list($aa, $bb, $cc,$dd) = $aryGroupIDList[$i];
	echo "xxxx >$aa, $bb, $cc ,$dd<br>";
}
# Check Card
if ($targetUserID == 0)
{
    # Card not registered
    echo output_env_str(CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered']);
    intranet_closedb();
    exit();
}
else if ($targetType == 2)
{

     $null_record_variable = "NULL";

     # Get ClassID
     $sql = "SELECT ClassID FROM INTRANET_CLASS WHERE ClassName = '$targetClass'";
     $temp = $db_engine->returnVector($sql);
     $targetClassID = $temp[0];

     # Check need to take attendance or not
     $sql = "SELECT Mode FROM CARD_STUDENT_CLASS_SPECIFIC_MODE WHERE ClassID = '$targetClassID'";
     $temp = $db_engine->returnVector($sql);
     $mode = $temp[0];
     if ($mode == 2)
     {
         echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###".$attend_lang['NoNeedToTakeAttendance']."###$targetUserLogin");
         intranet_closedb();
         exit();
     }


     # Get Record
     # Create Monthly data table
     $current_time = time();
     if ($external_time != "")
     {
         $current_time = strtotime(date('Y-m-d') . " " . $external_time);
     }
     $month = date('m',$current_time);
     $year = date('Y',$current_time);
     $day = date('d',$current_time);
     $today = date('Y-m-d',$current_time);
     $time_string = date('H:i:s',$current_time);
     $ts_now = $current_time - strtotime($today);
     
	if($bug_tracing['smartcard_student_attend_status']){
		include_once("../../includes/libfilesystem.php");
		if(!is_dir($file_path."/file/log_student_attend_status")){
			$lf = new libfilesystem();
			$lf->folder_new($file_path."/file/log_student_attend_status");
		}
		$log_filepath = "$file_path/file/log_student_attend_status/log_student_attend_status_".($year.$month.$day).".txt";
	}
           
     buildMonthTable($year, $month);
     createEntry_Card_Student_Record_Storage($year,$month);

     //$time_table = trim(get_file_content("$intranet_root/file/time_table_mode.txt"));
     $time_table = $Settings['TimeTableMode'];
     $time_table_mode = $time_table;         ### 0 - Time Slot, 1 - Time Session
//echo "time_table_mode [".$time_table_mode."]<br>";
		######################################
//dump
         if (($time_table_mode == 0)||($time_table_mode == ''))
         {

             # Change the name display
             $targetName = "$targetName ($targetClass-$targetClassNumber)";

             # Get Time Boundaries
             # Get Cycle Day
             $sql = "SELECT TextShort FROM INTRANET_CYCLE_DAYS WHERE RecordDate = '$today'";
             $temp = $db_engine->returnVector($sql);
             $cycleDay = $temp[0];


             # Get Week Day
             $weekDay = date('w');

             # Get Class-specific time boundaries

             # Get Special Day Settings
             $sql = "SELECT RecordID, TIME_TO_SEC(MorningTime), TIME_TO_SEC(LunchStart),
                            TIME_TO_SEC(LunchEnd), TIME_TO_SEC(LeaveSchoolTime),
                            NonSchoolDay
                            FROM CARD_STUDENT_SPECIFIC_DATE_TIME
                            WHERE RecordDate = '$today'
                                  AND ClassID = '$targetClassID'
                            ";
             $temp = $db_engine->returnArray($sql,6);
             list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];

             if (sizeof($temp)==0 || $ts_recordID == "")
             {
                 if ($cycleDay != "")
                 {
                     $conds = " OR (a.DayType = 2 AND a.DayValue = '$cycleDay')";
                 }

                 $sql = "SELECT a.RecordID, TIME_TO_SEC(a.MorningTime), TIME_TO_SEC(a.LunchStart),
                                TIME_TO_SEC(a.LunchEnd), TIME_TO_SEC(a.LeaveSchoolTime),
                                a.NonSchoolDay
                                FROM CARD_STUDENT_CLASS_PERIOD_TIME as a
                                     LEFT OUTER JOIN INTRANET_CLASS as b ON a.ClassID = b.ClassID
                                WHERE b.ClassName = '$targetClass' AND
                                  (
                                    (a.DayType = 1 AND a.DayValue = '$weekDay') $conds
                                      OR
                                    (a.DayType = 0 AND a.DayValue = 0)
                                  )
                                  ORDER BY a.DayType DESC
                                  ";
                 $temp = $db_engine->returnArray($sql,6);
                 list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
                 if (sizeof($temp)==0 || $ts_recordID == "")
                 {
                     # Get Special Day based on School
                     $sql = "SELECT RecordID, TIME_TO_SEC(MorningTime), TIME_TO_SEC(LunchStart),
                                    TIME_TO_SEC(LunchEnd), TIME_TO_SEC(LeaveSchoolTime),
                                    NonSchoolDay
                             FROM CARD_STUDENT_SPECIFIC_DATE_TIME
                             WHERE RecordDate = '$today'
                                   AND ClassID = 0
                                   ";
	                     $temp = $db_engine->returnArray($sql,6);
                     list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];

                     if (sizeof($temp)==0 || $ts_recordID == "")
                     {
                         # Get School Settings
                         $sql = "SELECT a.SlotID, TIME_TO_SEC(a.MorningTime), TIME_TO_SEC(a.LunchStart),
                                        TIME_TO_SEC(a.LunchEnd), TIME_TO_SEC(a.LeaveSchoolTime),
                                        a.NonSchoolDay
                                 FROM CARD_STUDENT_PERIOD_TIME as a
                                      WHERE (a.DayType = 1 AND a.DayValue = '$weekDay') $conds OR (a.DayType = 0 AND a.DayValue = 0)
                                      ORDER BY a.DayType DESC
                                 ";
                         $temp = $db_engine->returnArray($sql,6);
echo "<b>Time Slot Whole School CYCLE, WEEKDAY , NORMAL SQL</b> [".$sql."]<br><br>";
                         list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
                     }
                 }
             }
		$tempArray = array($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay);
		$classSchool_nonSchoolDay = $ts_nonSchoolDay;
		$classSchoolSlotTimeTable = array($tempArray);
		

/** START GET GROUP TIME TABLE 20080104 **/
//Start new
//echo "groupIDString [".$groupIDString."]<br>";
//fai
		$groupID_str = "";
		$groupID_TimeTable = array();
		$groupID_FindSpecificTimeTable = array();
		$compareTimeTable = array();
		if(sizeof($aryGroupIDList) != 0)
		{
			for($i=0; $i< sizeof($aryGroupIDList); $i++)
			{
				list($groupId, $title, $desc,$mode) = $aryGroupIDList[$i];

				$groupID_str .= $groupId.",";

				#MODE = NULL ==> SCHOOL TIME TABLE
				#MDOE = 0    ==> SCHOOL TIME TABLE (SAME AS NULL)
				#MODE = 1    ==> GROUP SPECIFIC TIME TABLE
				#MODE = 2    ==> NO NEED TO TAKE ATTENDANCE
				if($mode == 1)
				{
					array_push($groupID_FindSpecificTimeTable,$aryGroupIDList[$i]);
echo "push groupid [".$groupId."]<br>";
				}
				else
				{
					array_push($groupID_TimeTable,$aryGroupIDList[$i]);
				}
			}
			echo "<hr>";
			echo "<hr>";
			$groupID_str = substr($groupID_str, 0, -1);
			echo "groupID_str [".$groupID_str."]<br>";		
		}
		
 	
		if(sizeof($groupID_FindSpecificTimeTable) > 0)
		{
			if($groupID_str != "")
			{
				//GET GROUP SPECIAL TIME TABLE
				$sql = getGroupSlotSpecialSql ($groupID_str);
				echo "<font color =red>sql [".$sql."]</font><br>";
				$groupSpecialResultSet = $db_engine->returnArray($sql,7);

				$sql = getGroupSlotCycleWeekNormalSql($groupID_str);
				echo "getGroupSlotCycleWeekNormalSql sql  [".$sql."]<br>";
				$groupCycleWeekNormalResultSet = $db_engine->returnArray($sql,8);
			}

			#CHECK SPECIFIC TIME TABLE FOR GROUP MODE == 1
			for($i=0; $i< sizeof($groupID_FindSpecificTimeTable); $i++)
			{
				$groupTimeTableFind = false;
				list($groupId, $title, $desc,$mode) = $groupID_FindSpecificTimeTable[$i];
				echo "groupid ==> $groupId, $title, $desc,$mode<br>";
				for($y = 0; $y<sizeof($groupSpecialResultSet); $y++)
				{
					//CEHCK SPECIAL TIME TABLE
					list($s_groupid, $s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay) = $groupSpecialResultSet[$y];
					if($groupId == $s_groupid)
					{
						echo "get in [".$groupId."] special<br>";
						$aryTemp = array($s_groupid, $title, $desc,$mode,$s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay);
						array_push($groupID_TimeTable, $aryTemp);
						array_push($compareTimeTable, array($s_RecordID, timeToSec($s_MorningTime), timeToSec($s_LunchStart),timeToSec($s_LunchEnd), timeToSec($s_LeaveSchoolTime),$s_NonSchoolDay));
						$groupTimeTableFind = true;
						break;
					}
				}
				if($groupTimeTableFind == false)
				{	//GROUP NOT FIND IN THE SPECIAL TIME TABLE
					//FIND IN THE CYCLE, WEEKDAY, NORMAL TIME TABLE
					for($z = 0; $z< sizeof($groupCycleWeekNormalResultSet);$z++)
					{
						list($s_groupid, $s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay) = $groupCycleWeekNormalResultSet[$z];
						if($groupId == $s_groupid)
						{
							echo "get in [".$groupId."] normal<br>";
							$aryTemp = array($s_groupid,  $title, $desc,$mode,$s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay);
							array_push($groupID_TimeTable, $aryTemp);
							array_push($compareTimeTable, array($s_RecordID, timeToSec($s_MorningTime), timeToSec($s_LunchStart),timeToSec($s_LunchEnd), timeToSec($s_LeaveSchoolTime),$s_NonSchoolDay));
							$groupTimeTableFind = true;
							break;
						}
					}
					//GROUP NOT FIND IN CYCLE, WEEKDAY , NORMAL TIME TABLE, SET TO SCHOOL TIME TABLE (MODE = NULL)
					if($groupTimeTableFind == false)
					{
						$aryTemp = array($groupId, $title, $desc,"");
						array_push($groupID_TimeTable, $aryTemp);
					}
				}
			}		
		}

		
		echo "<table border = \"1\">";
		echo "<tr><td>Title</td><td>Desc</td><td>AM Lesson </td><td>Time Lunch Time </td><td>PM Lesson Time </td><td>School End Time </td></tr>";
		for($i = 0; $i < sizeof($groupID_TimeTable); $i++)
		{

			list($s_groupid,  $title, $desc,$mode,$s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay) = $groupID_TimeTable[$i];
			echo "====> [$s_groupid,  $title, $desc,$mode,$s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay]<br>";
			echo "<tr>";	
			echo "<td>".$title."&nbsp</td>";
			echo "<td>".$desc."&nbsp</td>";

			switch($mode){
				case 1:
						if($s_NonSchoolDay == 1)
						{	
							echo "<td colspan=\"4\">Non school day</td>";
						}
						else
						{
							echo "<td>".$s_MorningTime."</td>";
							echo "<td>".$s_LunchStart."</td>";
							echo "<td>".$s_LunchEnd."</td>";
							echo "<td>".$s_LeaveSchoolTime."</td>";
						}
					break;
				case 2:
						echo "<td colspan=\"4\">No need to take attendnce</td>";
					break;
				default:
						echo "<td colspan=\"4\">Take school attendance</td>";
			}		

			echo "</tr>";

		}
		echo "</table>";		


		$finalTimeTable = mergeTimeTable($classSchoolSlotTimeTable,$compareTimeTable);


		echo "<br><br><table border = \"1\">";
		echo "<tr><td>AM Lesson </td><td>Time Lunch Time </td><td>PM Lesson Time </td><td>School End Time </td></tr>";
		
			list($s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay) = $finalTimeTable[0];
			echo "====> [$s_groupid,  $title, $desc,$mode,$s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay]<br>";
			echo "<tr>";	

			if($s_NonSchoolDay == 1)
			{	
				echo "<td colspan=\"4\">Non school day</td>";
			}
			else
			{
				echo "<td>".secToTime($s_MorningTime)."</td>";
				echo "<td>".secToTime($s_LunchStart)."</td>";
				echo "<td>".secToTime($s_LunchEnd)."</td>";
				echo "<td>".secToTime($s_LeaveSchoolTime)."</td>";
			}
			echo "</tr>";		
		echo "</table>";		

print "<hr><hr><hr>";
////end new
         }
         if($time_table_mode == 1)
         {
                 //echo "Now Using Time Session Mode<BR><BR>";
                 # Get Record From table
             # Change the name display
             $targetName = "$targetName ($targetClass-$targetClassNumber)";


             # Get Time Boundaries
             # Get Cycle Day
             $sql = "SELECT TextShort FROM INTRANET_CYCLE_DAYS WHERE RecordDate = '$today'";
             $temp = $db_engine->returnVector($sql);
             $cycleDay = $temp[0];


             # Get Week Day
             $weekDay = date('w');

             # Get Class-specific time boundaries

             # Get Special Day Settings
             $sql = "SELECT
                                             b.RecordID, TIME_TO_SEC(a.MorningTime), TIME_TO_SEC(a.LunchStart),
                                             TIME_TO_SEC(a.LunchEnd), TIME_TO_SEC(a.LeaveSchoolTime),
                                             a.NonSchoolDay
                              FROM
                                              CARD_STUDENT_TIME_SESSION AS a INNER JOIN
                                              CARD_STUDENT_TIME_SESSION_DATE AS b ON (a.SessionID = b.SessionID)
                              WHERE
                                              RecordDate = '$today' AND ClassID = '$targetClassID'";

             $temp = $db_engine->returnArray($sql,6);
             list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];

             if (sizeof($temp)==0 || $ts_recordID == "")
             {
                 if ($cycleDay != "")
                 {
                     $conds = " OR (a.DayType = 2 AND a.DayValue = '$cycleDay')";
                 }

                 /*
                 $sql = "SELECT
                                                 a.RecordID, TIME_TO_SEC(a.MorningTime), TIME_TO_SEC(a.LunchStart),
                                                 TIME_TO_SEC(a.LunchEnd), TIME_TO_SEC(a.LeaveSchoolTime),
                                                 a.NonSchoolDay
                                  FROM
                                                  CARD_STUDENT_TIME_SESSION_REGULAR AS a LEFT OUTER JOIN
                                                        INTRANET_CLASS as b ON (a.ClassID = b.ClassID)
                                         WHERE
                                                         b.ClassName = '$targetClass' AND
                                  (
                                    (a.DayType = 1 AND a.DayValue = '$weekDay') $conds
                                      OR
                                    (a.DayType = 0 AND a.DayValue = 0)
                                  )
                                  ORDER BY a.DayType DESC";
                                  */
                 $sql = "SELECT
                                                 a.RecordID, TIME_TO_SEC(b.MorningTime), TIME_TO_SEC(b.LunchStart),
                                                 TIME_TO_SEC(b.LunchEnd), TIME_TO_SEC(b.LeaveSchoolTime),
                                                 b.NonSchoolDay
                                  FROM
                                                  CARD_STUDENT_TIME_SESSION_REGULAR AS a LEFT OUTER JOIN
                                                  CARD_STUDENT_TIME_SESSION AS b ON (a.SessionID = b.SessionID)
                                         WHERE
                                                         a.ClassID = '$targetClassID' AND
                                  (
                                    (a.DayType = 1 AND a.DayValue = '$weekDay') $conds
                                      OR
                                    (a.DayType = 0 AND a.DayValue = 0)
                                  )
                                  ORDER BY a.DayType DESC";
                 $temp = $db_engine->returnArray($sql,6);
                 list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
                 if (sizeof($temp)==0 || $ts_recordID == "")
                 {
                     # Get Special Day based on School
                                 $sql = "SELECT
                                                                 a.RecordID, TIME_TO_SEC(b.MorningTime), TIME_TO_SEC(b.LunchStart),
                                                                 TIME_TO_SEC(b.LunchEnd), TIME_TO_SEC(b.LeaveSchoolTime),
                                                                 b.NonSchoolDay
                                                  FROM
                                                                  CARD_STUDENT_TIME_SESSION_DATE AS a INNER JOIN
                                                                  CARD_STUDENT_TIME_SESSION AS b ON (a.SessionID = b.SessionID)
                                                  WHERE
                                                                  a.RecordDate = '$today' AND a.ClassID = 0";

                     $temp = $db_engine->returnArray($sql,6);
                     list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];

                     if (sizeof($temp)==0 || $ts_recordID == "")
                     {
                         # Get School Settings
                         $sql = "SELECT
                                                                 a.RecordID, TIME_TO_SEC(b.MorningTime), TIME_TO_SEC(b.LunchStart),
                                                TIME_TO_SEC(b.LunchEnd), TIME_TO_SEC(b.LeaveSchoolTime),
                                                b.NonSchoolDay
                                 FROM
                                                         CARD_STUDENT_TIME_SESSION_REGULAR AS a INNER JOIN
                                                         CARD_STUDENT_TIME_SESSION as b ON (a.SessionID = b.SessionID AND a.ClassID = 0)
                                 WHERE
                                                         (a.DayType = 1 AND a.DayValue = '$weekDay') $conds OR (a.DayType = 0 AND a.DayValue = 0)
                                 ORDER BY
                                                         a.DayType DESC";

                         $temp = $db_engine->returnArray($sql,6);
                         list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
                     }
                 }
             }

		$tempArray = array($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay);
		$classSchool_nonSchoolDay = $ts_nonSchoolDay;
		$classSchoolSessionTimeTable = array($tempArray);
		$final_timeTable = $classSchoolSessionTimeTable;

/** START GET GROUP TIME TABLE 20080104 **/

		if($groupIDString != "")
		{			
			$sql = "SELECT RecordID, TIME_TO_SEC(MorningTime), TIME_TO_SEC(LunchStart),
								TIME_TO_SEC(LunchEnd), TIME_TO_SEC(LeaveSchoolTime),
								NonSchoolDay
	                            FROM
										CARD_STUDENT_TIME_SESSION AS a INNER JOIN
										CARD_STUDENT_TIME_SESSION_DATE_GROUP AS b ON (a.SessionID = b.SessionID)
								WHERE 
										RecordDate = '$today'
										AND GroupId in ($groupIDString) 
								ORDER BY
										RecordID desc
					   ";
//echo "<b>session group special day</b> sql [  ".$sql."  ]<br><br>";
			$groupSpecialTimeTable = $db_engine->returnArray($sql,6);

			list($tmp_recordID, $tmp_morningTime, $tmp_lunchStart, $tmp_lunchEnd, $tmp_leaveSchool, $tmp_nonSchoolDay) = $groupSpecialTimeTable[0];
			
			//$groupSessionTimeTable = array($groupSpecialTimeTable[0]);
			$groupSessionTimeTable = $groupSpecialTimeTable;
			//NO RECORD OF GROUP SPECIAL TIME TABLE
			if(sizeof($groupSpecialTimeTable) == 0 || $tmp_recordID == "")
			{
					$sql = "SELECT a.RecordID, TIME_TO_SEC(b.MorningTime), TIME_TO_SEC(b.LunchStart),
									TIME_TO_SEC(b.LunchEnd), TIME_TO_SEC(b.LeaveSchoolTime),
									b.NonSchoolDay,a.DayType
							FROM 
									CARD_STUDENT_TIME_SESSION_REGULAR_GROUP AS a LEFT OUTER JOIN
                                    CARD_STUDENT_TIME_SESSION as b ON (a.SessionID = b.SessionID)					
							WHERE a.GroupId in ($groupIDString) AND
									  (
										(a.DayType = 1 AND a.DayValue = '$weekDay') $conds
										  OR
										(a.DayType = 0 AND a.DayValue = 0)
									  )
									  ORDER BY a.DayType DESC, a.recordId desc
							";
//echo "<br><br><b>session group cycle week</b> sql [  ".$sql."  ]<br><br>";
					$groupCycleWeekNormalTimeTable = $db_engine->returnArray($sql,7);

					if(sizeof($groupCycleWeekNormalTimeTable) > 0)
					{
						$emptyArray = array();
						$groupSessionTimeTable = mergeTimeTable($emptyArray,$groupCycleWeekNormalTimeTable);
					}
			}			
			if(sizeof($groupSpecialTimeTable) >0 || sizeof($groupCycleWeekNormalTimeTable) > 0)
			{
				$final_timeTable = mergeTimeTable($classSchoolSessionTimeTable,$groupSessionTimeTable);
			}
		}
		
	list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $final_timeTable[0];
/** END GET GROUP TIME TABLE **/
//			 checkTimeResult($ts_recordID,$ts_morningTime,$ts_lunchStart,$ts_lunchEnd,$ts_leaveSchool,$ts_nonSchoolDay);
             #if ($ts_morningTime == "")

             if (sizeof($temp)==0 || $ts_recordID == "")
             {
                 echo output_env_str(CARD_RESP_SYSTEM_NOT_INIT."###".$attend_lang['SystemNotInit']);
                 intranet_closedb();
                 exit();
             }
             # Check what meaning for this time
             //$content_basic = trim(get_file_content("$intranet_root/file/stattend_basic.txt"));
             $content_basic = $Settings['AttendanceMode'];
             $attendance_mode = $content_basic;
             //echo $attendance_mode."<BR><BR>";
         }



}
else     # Card Not Registered
{
    echo output_env_str(CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered']);
    intranet_closedb();
    exit();
}

intranet_closedb();

function getUserGroupIDArray($userID)
{
	global $db_engine;

	#GET USER BELONGS TO WHICH GROUP, GROUP IS CLASS TYPE (3)
	$groupIDString = "";
	$sql = "select c.GroupID, c.Title,c.Description, e.mode
		from 
			INTRANET_USER a, INTRANET_USERGROUP b, INTRANET_GROUP c 
			left outer join INTRANET_CLASS d on d.groupid = c.groupid 
			left outer join CARD_STUDENT_ATTENDANCE_GROUP e on e.groupid = c.groupid 
		where
			a.userid = b.userid		and 
			b.groupid = c.groupid   and 
			a.userid = '$userID' and 
			c.recordtype = 3 and 
			d.groupid is null";
		$result = $db_engine->returnArray($sql,4);
echo "sql [".$sql."] size of result [".sizeof($result)."]<br>";
	for($i=0; $i<sizeOf($result); $i++)
	{
		print_r($result[$i]);
	}

	return $result;
}

function getUserGroupID($userID)
{
	global $db_engine;
	#GET USER BELONGS TO WHICH GROUP, GROUP IS CLASS TYPE (3)
	$groupIDString = "";
	$sql = "select a.UserID, a.CardID, a.EnglishName, a.ChineseName , c.GroupID, c.Title,c.Description, c.recordtype 
		from 
			INTRANET_USER a, INTRANET_USERGROUP b, INTRANET_GROUP c 
			left outer join INTRANET_CLASS d on d.groupid = c.groupid 
		where
			a.userid = b.userid		and 
			b.groupid = c.groupid   and 
			a.userid = '$userID' and 
			c.recordtype = 3 and 
			d.groupid is null";
		$result = $db_engine->returnArray($sql,8);

	for($i=0; $i<sizeOf($result); $i++)
	{
		list($aa, $bb, $cc, $dd, $groupid, $ff,$gg,$hh) = $result[$i];
		$groupIDString.= $groupid.",";
	}
	$groupIDString = substr($groupIDString, 0, -1);
	return $groupIDString;
}
function checkTimeResult($ts_recordID,$ts_morningTime,$ts_lunchStart,$ts_lunchEnd,$ts_leaveSchool,$ts_nonSchoolDay)
{
	global $db_engine;

	$sql = "select sec_to_time(".$ts_morningTime."), sec_to_time(".$ts_lunchStart."), sec_to_time(".$ts_lunchEnd."), sec_to_time(".$ts_leaveSchool.")";
    $aaaa = $db_engine->returnArray($sql,4);
	list($bb, $cc, $dd, $ee) = $aaaa[0];
	echo "<br>ts_recordID---><b><font color = \"red\">".$ts_recordID."</font></b>";
	echo "<br>ts_morningTime--->".$ts_morningTime." <b><font color = \"red\">[".$bb."]</font></b>";
	echo "<br>ts_lunchStart--->".$ts_lunchStart."<b><font color = \"red\"> [".$cc."]</font></b>";
	echo "<br>ts_lunchEnd--->".$ts_lunchEnd." <b><font color = \"red\">[".$dd."]</font></b>";
	echo "<br>ts_leaveSchool--->".$ts_leaveSchool." <b><font color = \"red\">[".$ee."]</font></b>";
	echo "<br>ts_nonSchoolDay---><b><font color = \"red\">".$ts_nonSchoolDay."</font></b><br><br><br>";

}


#SHOULD RECEIVE CLASS/SCHOOL TIME TABLE FIRST, THEN IS GROUP TIME TABLE
function mergeTimeTable($classTimeTableArray, $groupTimeTableArray)
{
	/*
	for($i=0; $i<sizeof($classTimeTableArray); $i++)
    {
		list($aa, $bb, $cc, $dd, $ee, $ff,$gg) = $classTimeTableArray[$i];
		echo "array 1[".$aa."] [".$bb."] [".$cc."][".$dd."][".$ee."][".$ff."][".$gg."]<br>";               
    }	
	for($i=0; $i<sizeof($groupTimeTableArray); $i++)
    {
		list($aa, $bb, $cc, $dd, $ee, $ff,$gg) = $groupTimeTableArray[$i];
		echo "array 2[".$aa."] [".$bb."] [".$gg."]<br>";               
    }
*/
	$tempArray = array_merge ($classTimeTableArray, $groupTimeTableArray);


	if(sizeof($tempArray) != 0)
	{
		#$tempArray[0] MUST BE THE INFORMATION ABOUT CLASS/ SCHOOL TIME SETTING
		list($prev_RecordID, $prev_MorningTime, $prev_LunchStart, $prev_LunchEnd, $prev_LeaveSchoolTime, $prev_NonSchoolDay,$prev_DayType) = $tempArray[0];
	}

	for($i=0; $i<sizeof($tempArray); $i++)
    {
		list($_RecordID, $_MorningTime, $_LunchStart, $_LunchEnd, $_LeaveSchoolTime, $_NonSchoolDay,$_DayType) = $tempArray[$i];

echo  "<br>xxx -->$_RecordID, $_MorningTime, $_LunchStart, $_LunchEnd, $_LeaveSchoolTime, $_NonSchoolDay,$_DayType<br>";

			if($_DayType > $prev_DayType )
			{
				$prev_RecordID			= $_RecordID;			$prev_MorningTime		= $_MorningTime;
				$prev_LunchStart		= $_LunchStart;			$prev_LunchEnd			= $_LunchEnd;
				$prev_LeaveSchoolTime	= $_LeaveSchoolTime;	$prev_DayType			= $_DayType;
				$prev_NonSchoolDay		= $_NonSchoolDay;
			}
			else if ($_DayType == $prev_DayType)
			{
				#IF EQUAL DAY TYPE, NON SCHOOL DAY RECORD WILL NOT COUNT
				if($_NonSchoolDay != 1)
				{
					#EQUAL DAY TYPE , IF PREVIOUS RECORD IS NONSCHOOL DAY, SHOULD RESET ALL THE RECORD TO THE CURRENT RECORD
					if($prev_NonSchoolDay == 1)
					{
						$prev_RecordID			= $_RecordID;			$prev_MorningTime		= $_MorningTime;
						$prev_LunchStart		= $_LunchStart;			$prev_LunchEnd			= $_LunchEnd;
						$prev_LeaveSchoolTime	= $_LeaveSchoolTime;	$prev_DayType			= $_DayType;
						$prev_NonSchoolDay		= $_NonSchoolDay;
					}
					else
					{
						#EQUAL DAY TYPE, PREVIOUS IS SCHOOL DAY (NOT NON SCHOOL DAY), COMPARE THE TIME
						#_RecordID			TAKE ANY MIN/MAX		#_MorningTime		TAKE MIN
						#_LunchStart		TAKE MAX				#_LunchEnd			TAKE MIN
						#_LeaveSchoolTime	TAKE MAX				#_NonSchoolDay		TAKE MIN (0,1) ==> 1 IS NON SCHOOL DAY

						$prev_RecordID			= ($_RecordID		 > $prev_RecordID)		 ? $_RecordID			:$prev_RecordID;
						$prev_MorningTime		= ($_MorningTime	 > $prev_MorningTime)	 ? $prev_MorningTime	:$_MorningTime;
						$prev_LunchStart		= ($_LunchStart		 > $prev_LunchStart)	 ? $_LunchStart			:$prev_LunchStart;
						$prev_LunchEnd			= ($_LunchEnd		 > $prev_LunchEnd)		 ? $prev_LunchEnd		:$_LunchEnd;
						$prev_LeaveSchoolTime	= ($_LeaveSchoolTime > $prev_LeaveSchoolTime)? $_LeaveSchoolTime    :$prev_LeaveSchoolTime;
				//		$prev_NonSchoolDay	    = ($_NonSchoolDay    > $prev_NonSchoolDay)   ? $_NonSchoolDay	:$prev_NonSchoolDay;
				//		$prev_DayType			= $_DayType;
					}
				}
			}
			//else ($_DayType < $prev_DayType){ //skip}
		


	}

	#PRESERVE THE DATA STURTURE IN ARRAY OF ARRAY
	$arrayElement = array($prev_RecordID,$prev_MorningTime,$prev_LunchStart,$prev_LunchEnd,$prev_LeaveSchoolTime,$prev_NonSchoolDay); 

//	echo "return $prev_RecordID,$prev_MorningTime,$prev_LunchStart,$prev_LunchEnd,$prev_LeaveSchoolTime,$prev_NonSchoolDay<br>";
//checkTimeResult($prev_RecordID,$prev_MorningTime,$prev_LunchStart,$prev_LunchEnd,$prev_LeaveSchoolTime,$prev_NonSchoolDay);

	$returnArray = array($arrayElement);

	return $returnArray;
}
function getGroupSlotSpecialSql ($groupID_str)
{
	global $today;
	$sql =	"SELECT 
				groupid, RecordID, MorningTime, LunchStart,
				LunchEnd, LeaveSchoolTime,NonSchoolDay
			 FROM 
				CARD_STUDENT_SPECIFIC_DATE_TIME_GROUP
			 WHERE 
				RecordDate = '$today' AND 
				GroupId in ($groupID_str)
			 ORDER BY groupid
			";
	return $sql;
}
function getGroupSlotCycleWeekNormalSql($groupID_str)
{
	global $conds;
	global $weekDay;

	$sql =  "SELECT 
				a.groupid, a.RecordID, a.MorningTime, a.LunchStart,
				a.LunchEnd, a.LeaveSchoolTime,a.NonSchoolDay,a.DayType
			 FROM 
				CARD_STUDENT_GROUP_PERIOD_TIME as a
			 WHERE 
				a.GroupId in ($groupID_str) AND
				(
					(a.DayType = 1 AND a.DayValue = '$weekDay') $conds
					OR
					(a.DayType = 0 AND a.DayValue = 0)
				)
			 ORDER BY a.groupid, a.DayType DESC
			";
	return $sql;
}
function timeToSec($_time)
{
	$timeAry = split(":",$_time);

	$returnValue = $timeAry[0] * 60 *60 + $timeAry[1] *60;
	return $returnValue;
}

function secToTime($_sec)
{
	$hours = floor ($_sec / 60/60);
	$mins = $hours % 60;

	$hours = ($hours <= 9)? "0".$hours:$hours;
	$mins = ($mins <= 9)? "0".$mins:$mins;
	$returnValue = $hours.":".$mins;
	return $returnValue;

}
?>


