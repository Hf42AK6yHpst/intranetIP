<?
// Editing by 
/* 
 * 2019-06-03 (Carlos): Apply checkValidAccess() to verify valid access.
 * 2015-05-27 (Carlos): Only record bad card record when it is a student user and need to take attendance on that day.									
 * 2014-12-11 (Bill): $sys_custom['SupplementarySmartCard'] - Added CardID4
 * 						Priority of getting SmartCardID is CardID > CardID2 > CardID3 > CardID4
 * 2013-11-13 (Carlos): $sys_custom['SupplementarySmartCard'] - Added CardID2 and CardID3
 * 						Priority of getting SmartCardID is CardID > CardID2 > CardID3
 */
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libcardstudentattend2.php");

intranet_opendb();

include_once("attend_functions.php");

if(!checkValidAccess()){ // defined in attend_functions.php
	echo "0 ";
	intranet_closedb();
	exit();
}

$lcard = new libcardstudentattend2();

$UserLogin = trim($_REQUEST['UserLogin']);
$StaffClassNum = trim($_REQUEST['StaffClassNum']);
$ClassName = trim($_REQUEST['ClassName']);
$ClassNum = trim($_REQUEST['ClassNum']);

if($sys_custom['SupplementarySmartCard']){
	$MoreCardFields = ", CardID2, CardID3, CardID4 ";
}

if($ClassName != "" && $ClassNum != "") {	
	### Use ClassName & ClassNumber to get CardID, for Students use only ###
	$sql = "SELECT CardID, UserID, RecordType $MoreCardFields FROM INTRANET_USER WHERE ClassName = '$ClassName' AND ClassNumber = '$ClassNum' AND RecordType = 2 AND RecordStatus IN (0,1,2)";
	$temp = $lcard->returnArray($sql);
} 
else if ($UserLogin != "") {
	### Use UserLogin to get CardID, for stud/ staff use
	$sql = "SELECT CardID, UserID, RecordType $MoreCardFields FROM INTRANET_USER WHERE UserLogin = '$UserLogin' AND RecordStatus IN (0,1,2)";
	$temp = $lcard->returnArray($sql);
}
else if ($StaffClassNum != "") {
	
	### Use User Login to get CardID, for Staff use only ###
	$sql = "SELECT CardID, UserID, RecordType $MoreCardFields FROM INTRANET_USER WHERE ClassNumber = '$StaffClassNum' AND RecordType = 1 AND RecordStatus IN (0,1,2)";
	$temp = $lcard->returnArray($sql);
	
}

$return_cardid = $temp[0]['CardID'];
if($sys_custom['SupplementarySmartCard']){
	if($return_cardid == "" && $temp[0]['CardID2'] != ""){
		$return_cardid = $temp[0]['CardID2'];
	}
	if($return_cardid == "" && $temp[0]['CardID3'] != ""){
		$return_cardid = $temp[0]['CardID3'];
	}
	if($return_cardid == "" && $temp[0]['CardID4'] != ""){
		$return_cardid = $temp[0]['CardID4'];
	}
}
if (sizeof($temp)!=0 && $return_cardid != "")
{
	if($temp[0]['RecordType'] == USERTYPE_STUDENT)
	{
		$today = date("Y-m-d");
		$FinalSetting = $lcard->Get_Student_Attend_Time_Setting($today,$temp[0]['UserID']);
		if ($FinalSetting === "NonSchoolDay")
		{
			# No need to take attendance, not count as bad card record
		}else{
    		# Add bad card records
    		$lcard->addBadActionNoCardEntrance($temp[0]['UserID']);
		}
	}
    echo $return_cardid;
}
else echo "0 ";

intranet_closedb();
?>