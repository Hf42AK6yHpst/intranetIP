<?
####################################################################
# Created by : Kenneth Wong
# Creation Date : 20051207
####################################################################
# Version updates
# 20051207: Kenneth Wong
# 20061108: Kenneth Wong
#           Check whether last modified is card read or confirmation
# 20070503: Kenneth Wong
#           Add checking of "0" and "" in InSchoolTime to prevent accidentally 00:00:00 record written
# 20070503: Peter Ho
#                        Staff attendance - Add removing the previous records when the new status and old status are not the same
# 2007 : Ronald Yeung

# 20070711 : Kenneth Wong
#            Add raw log to staff attendance : buildStaffRawLogMonthTable()
# 20070720 : Kenneth Wong
#            Modify reminder retrival : Get more than 1 reminder message
# 20070822 : Peter Ho
#                         Add createEntry_Card_Student_Record_Storage() after buildMonthTable()
# 20070903 : Ronald Yeung
#                         Show Student Leave Option When Student Is Normal Leave School Or Early Leave School
# 20070927 : Kenneth Wong
#            Change Staff output from $attend_lang['InSchool_late'] to $attend_lang['InSchool'] in late situation
# 20071029 : Peter Ho
#                                                Add log of Student Attendance Status ( for St. Mary : Time Slot Mode, WD w/ Lunch )
#                                                $bug_tracing['smartcard_student_attend_status']
# 20080104 : Fai Khoe
#                                                Add check group attendance
# 20090728 : Kenneth Chung 
#		change to get all setting from libgeneralsettings class
####################################################################
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

# Customized
if ($sys_custom['QualiEd_StudentAttendance'])
{
    header("Location: receiver_q.php?CardID=$CardID&sitename=$sitename&dlang=$dlang");
    exit();
}

intranet_opendb();

// get all general setting for student attendance
$GeneralSetting = new libgeneralsettings();
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance');

include_once("attend_functions.php");
if ($dlang != "b5" && $dlang!= "gb") $intranet_session_language = "en";
else $intranet_session_language = $dlang;

include_once("attend_lang.php");
include_once("attend_const.php");

header("Content-type: html/text; charset=big5");

if ($CardID == "")
{
    # Card not registered
    echo mb_convert_encoding(output_env_str(CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered']),"BIG5","UTF-8");
    intranet_closedb();
    exit();
}

######################################################
# Param : (From QueryString)
#    CardID
#    sitename
######################################################

# $db_engine (from functions.php)

################################################
$directProfileInput = false;
$getremind = true;
$reminder_null_text = "No Reminder";
################################################


# 1. Get UserInfo
$namefield = getNameFieldByLang();
$sql = "SELECT UserID, $namefield, RecordType, ClassName, ClassNumber, UserLogin FROM INTRANET_USER WHERE CardID = '$CardID'";
$temp = $db_engine->returnArray($sql,6);
list($targetUserID , $targetName, $targetType, $targetClass, $targetClassNumber, $targetUserLogin) = $temp[0];

# Check Card
if ($targetUserID == 0)
{
    # Card not registered
    echo mb_convert_encoding(output_env_str(CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered']),"BIG5","UTF-8");
    intranet_closedb();
    exit();
}
else if ($targetType == 1)    # Staff, redirection
{
	// get all general setting for student attendance
	$GeneralSetting = new libgeneralsettings();
	$Settings = $GeneralSetting->Get_General_Setting('StaffAttendance');
	
	# Check IP
	if (!isIPAllowed('StaffAttendance'))
	{
	     echo output_env_str(CARD_RESP_INVALID_IP."###".$attend_lang['InvalidIP']);
	     intranet_closedb();
	     exit();
	}
	
    $sitename = intranet_htmlspecialchars($sitename);
    $datatype = intranet_htmlspecialchars($datatype);

    if ($module_version['StaffAttendance'] == 2.0)
    {
        /* User information */
        /* $targetUserID - UserID, $targetName - ChineseName or EnglishName */

        /* Get which group belongs to */
        $sql = "SELECT GroupID FROM CARD_STAFF_ATTENDANCE_USERGROUP WHERE UserID = '$targetUserID'";
        $temp = $db_engine->returnVector($sql);
        $staffGroupID = $temp[0];
        if (sizeof($temp)==0 || $staffGroupID == 0)
        {
            /* if this staff no needs to take attendance - Not belongs to any group (Print ignoring message) */
            echo mb_convert_encoding(output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###".$attend_lang['NoNeedToTakeAttendance']."###$targetUserLogin"),"BIG5","UTF-8");
            intranet_closedb();
            exit();
        }

     $current_time = time();

     if ($external_time != "")
     {
         $current_time = strtotime(date('Y-m-d') . " " . $external_time);
     }

        $month = date('m',$current_time);
        $year = date('Y',$current_time);
        $day = date('d',$current_time);
        $today = date('Y-m-d',$current_time);
        $weekday = date('w', $current_time);
        $time_string = date('H:i:s',$current_time);
        $ts_now = $current_time - strtotime($today);

        /* Add Raw Log to Staff Attendance */
        $tablename = buildStaffRawLogMonthTable($year, $month);
        $sql = "INSERT IGNORE INTO $tablename
                       (UserID, DayNumber, RecordTime, RecordStation, DateInput, DateModified)
                       VALUES
                       ('$targetUserID','$day',NOW(),'$sitename', NOW(),NOW())";
        $db_engine->db_db_query($sql);


        /****************************************/
        /* Get Duty (Exact Day on user > Exact Day on Group > Group Duty)    */
        /*
           $dutyOn -
           $dutyStart -
           $dutyEnd -
           $dutyInWaived -
           $dutyOutWaived -
        */
        $roster_retrieved = false;


        // Get Leave Record
        $sql = "SELECT RecordID, RecordType, OutgoingType, ReasonType
                                FROM CARD_STAFF_ATTENDANCE2_LEAVE_RECORD
                                WHERE StaffID = '$targetUserID' AND RecordDate = '$today'";
        $temp = $db_engine->returnArray($sql,4);
        list ($t_LeaveRecordID, $t_LeaveRecordType, $t_LeaveOutgoingType, $t_LeaveReasonType) = $temp[0];
        if ($t_LeaveRecordID != "")
        {
            if ($t_LeaveRecordType == STAFF_LEAVE_TYPE_HOLIDAY)
            {
                $dutyOn = false;
                $roster_retrieved = true;
            }
            else if ($t_LeaveRecordType == STAFF_LEAVE_TYPE_OUTGOING)
            {
                 $dutyInWaived = ($t_LeaveOutgoingType == STAFF_LEAVE_BOTH_WAIVED || $t_LeaveOutgoingType == STAFF_LEAVE_IN_WAIVED);
                 $dutyOutWaived = ($t_LeaveOutgoingType == STAFF_LEAVE_BOTH_WAIVED || $t_LeaveOutgoingType == STAFF_LEAVE_OUT_WAIVED);
            }
        }

        if (!$roster_retrieved)
        {
             // Exact Day on User
             $sql = "SELECT RecordID, Duty, TIME_TO_SEC(DutyStart), TIME_TO_SEC(DutyEnd)
                            FROM CARD_STAFF_ATTENDANCE2_USER_DATE_DUTY
                            WHERE UserID = $targetUserID AND DutyDate = '$today'";
             $temp = $db_engine->returnArray($sql,4);
             if (sizeof($temp)!=0)
             {
                 list ($t_id, $t_duty, $t_duty_start, $t_duty_end) = $temp[0];
                 if ($t_id > 0)
                 {
                     $dutyOn = $t_duty;
                     $dutyStart = $t_duty_start;
                     $dutyEnd = $t_duty_end;
                     $roster_retrieved = true;
                 }
             }
        }

        if (!$roster_retrieved)
        {
             // Exact Day on Group
             $sql = "SELECT RecordID, Duty, TIME_TO_SEC(DutyStart), TIME_TO_SEC(DutyEnd)
                            FROM CARD_STAFF_ATTENDANCE2_GROUP_DATE_DUTY
                            WHERE GroupID = $staffGroupID AND DutyDate = '$today'";
             $temp = $db_engine->returnArray($sql,4);

             if (sizeof($temp)!=0)
             {
                 list ($t_id, $t_duty, $t_duty_start, $t_duty_end) = $temp[0];
                 if ($t_id > 0)
                 {
                     $dutyOn = $t_duty;
                     $dutyStart = $t_duty_start;
                     $dutyEnd = $t_duty_end;
                     $roster_retrieved = true;
                 }

             }
        }

        if (!$roster_retrieved)
        {
             // Group Roster
             switch ($weekday)
             {
                     case 0: $field_ext = "Sun"; break;
                     case 1: $field_ext = "Mon"; break;
                     case 2: $field_ext = "Tue"; break;
                     case 3: $field_ext = "Wed"; break;
                     case 4: $field_ext = "Thur"; break;
                     case 5: $field_ext = "Fri"; break;
                     case 6: $field_ext = "Sat"; break;
                     default: $field_ext = "Mon";
             }
             $field_duty = "Duty".$field_ext;
             $field_duty_start = "TIME_TO_SEC(DutyStart".$field_ext.")";
             $field_duty_end = "TIME_TO_SEC(DutyEnd".$field_ext.")";
             $sql = "SELECT GroupID, $field_duty, $field_duty_start, $field_duty_end
                            FROM CARD_STAFF_ATTENDANCE2_GROUP
                            WHERE GroupID = $staffGroupID";
             $temp = $db_engine->returnArray($sql,4);
             if (sizeof($temp)!=0)
             {
                 list ($t_id, $t_duty, $t_duty_start, $t_duty_end) = $temp[0];
                 if ($t_id > 0)
                 {
                     $dutyOn = $t_duty;
                     $dutyStart = $t_duty_start;
                     $dutyEnd = $t_duty_end;
                     $roster_retrieved = true;
                 }
             }
        }
        if (!$roster_retrieved)
        {
             echo mb_convert_encoding(output_env_str(CARD_RESP_SYSTEM_NOT_INIT."###".$attend_lang['SystemNotInit']),"BIG5","UTF-8");
             intranet_closedb();
             exit();
        }

        /****************************************/
        // Retrieve Daily Record
        // Get Original Record
        $table_name = buildStaffAttendance2MonthTable($year,$month);

        $sql = "SELECT RecordID, Duty, DutyStart, DutyEnd, StaffPresent,
                       InTime, InSchoolStatus, InWaived, InAttendanceRecordID,
                       OutTime, OutSchoolStatus, OutWaived, OutAttendanceRecordID,
                       MinLate, MinEarlyLeave, RecordType, RecordStatus, DateModified
                       FROM $table_name
                       WHERE StaffID = '$targetUserID' AND DayNumber = '$day'";
        $temp = $db_engine->returnArray($sql, 18);
        list($r_RecordID, $r_Duty, $r_DutyStart, $r_DutyEnd, $r_StaffPresent,
             $r_InTime, $r_InSchoolStatus, $r_InWaived, $r_InAttendID,
             $r_OutTime, $r_OutSchoolStatus, $r_OutWaived, $r_OutAttendID,
             $r_MinLate, $r_MinEarly, $r_RecordType, $r_RecordStatus, $r_DateModified
             ) = $temp[0];
        $noDailyRecord = ($r_RecordID == "");
        if ($noDailyRecord)
        {
            $sql = "INSERT INTO $table_name
                           (StaffID, DayNumber, Duty, DutyStart, DutyEnd, DateInput, DateModified)
                    VALUES
                    ('$targetUserID','$day', '$dutyOn', SEC_TO_TIME('$dutyStart'), SEC_TO_TIME('$dutyEnd') , now(), now())";
            $db_engine->db_db_query($sql);
            $r_RecordID = $db_engine->db_insert_id();
        }
        else
        {
        }

        if ($dutyOn!=1)
        {
            /* if no duty, process OT Record */
            // Check this is the first record or not
            $sql = "SELECT RecordID, TIME_TO_SEC(StartTime)
                                    FROM CARD_STAFF_ATTENDANCE2_OT_RECORD
                                    WHERE StaffID = '$targetUserID' AND RecordDate = '$today'";
            $temp = $db_engine->returnArray($sql,2);
            if (!is_array($temp) || sizeof($temp)==0 || $temp[0][0] == "")
            {
                    // First Record
                    $sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_OT_RECORD
                                            (StaffID, RecordDate, StartTime, DateInput, DateModified)
                                    VALUES
                                            ('$targetUserID', '$today', '$time_string', NOW(), NOW())";
                    $db_engine->db_db_query($sql);
            }
            else
            {
                    // Finishing
                    $t_OTRecordID = $temp[0][0];
                    $t_Start = $temp[0][1];
                    $time_diff = $ts_now - $t_Start;
                    $OTmins = round($time_diff/60);
                    $sql = "UPDATE CARD_STAFF_ATTENDANCE2_OT_RECORD
                                            SET EndTime = '$time_string', OTmins = '$OTmins',
                                            DateModified = NOW()
                                            WHERE RecordID = $t_OTRecordID";
                    $db_engine->db_db_query($sql);
            }



            /* Response */
            echo mb_convert_encoding(output_env_str(CARD_RESP_STAFF_TIME_RECORDED."###".$targetName.$attend_lang['RecordSuccessful'].$time_string."###$targetUserLogin"),"BIG5","UTF-8");
            intranet_closedb();
            exit();
        }
        else  /* else Go to Mark Attendance */
        {

            /* Mark Attendance */
            // Classify Time Zone
            if ($ts_now <= $dutyStart)
            {
                // On Time
                $sql = "UPDATE $table_name
                               SET StaffPresent = 1, InTime = '$time_string',
                                   InSchoolStatus = '".CARD_STATUS_PRESENT."',
                                   InSchoolStation = '$sitename',
                                   MinLate = 0, DateModified = now()
                       WHERE RecordID = $r_RecordID
                               ";
                $db_engine->db_db_query($sql);

                // Remove Profile Record (Late / Absent)
                $sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_PROFILE
                               WHERE StaffID = '$targetUserID' AND RecordDate = '$today'
                                     AND (RecordType = '".PROFILE_TYPE_ABSENT."' OR RecordType='".PROFILE_TYPE_LATE."')
                                     ";
                $db_engine->db_db_query($sql);



                // Response
                echo mb_convert_encoding(output_env_str(CARD_RESP_STAFF_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin"),"BIG5","UTF-8");
                intranet_closedb();
                exit();

            }
            else if ($ts_now >= $dutyEnd)
            {
                // After Working hour
                // Update Daily Record
                $sql = "UPDATE $table_name
                               SET OutTime = '$time_string',
                                   OutSchoolStatus = '".CARD_LEAVE_NORMAL."',
                                   OutSchoolStation = '$sitename',
                                   MinEarlyLeave = 0, DateModified = now()
                       WHERE RecordID = $r_RecordID
                                   ";
                $db_engine->db_db_query($sql);

                // Remove Profile Record (Early Leave)
                $sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_PROFILE
                               WHERE StaffID = '$targetUserID' AND RecordDate = '$today'
                                     AND RecordType = '".PROFILE_TYPE_EARLY."'
                                     ";
                $db_engine->db_db_query($sql);


                // Process OT Record
                // Check whether record created
                $sql = "SELECT RecordID, TIME_TO_SEC(StartTime)
                                            FROM CARD_STAFF_ATTENDANCE2_OT_RECORD
                                            WHERE StaffID = '$targetUserID' AND RecordDate = '$today'";
                    $temp = $db_engine->returnArray($sql,2);
                    if (!is_array($temp) || sizeof($temp)==0 || $temp[0][0] == "")
                    {
                            // Create Record

                            // Get Threshold
                            #$OT_threshold = 0;
                            $mins_ignored = trim(get_file_content("$intranet_root/file/staffattend_ot_ignore.txt"));
                            $OT_threshold = $mins_ignored+0;


                            // OT Start time
                            $t_OTStart = $dutyEnd + $OT_threshold*60;
                            //$string_OTStart = date('Y-m-d',$t_OTStart);
                            $string_OTStart = date('H:i:s',$t_OTStart+strtotime($today));



                            // OT mins
                            $time_diff = $ts_now - $t_OTStart;
                            $OTmins = round($time_diff/60);

                            if ($OTmins > 0)
                            {
                                $sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_OT_RECORD
                                              (StaffID, RecordDate, StartTime, EndTime,
                                               OTmins, DateInput, DateModified)
                                            VALUES
                                              ('$targetUserID', '$today', '$string_OTStart', '$time_string',
                                               '$OTmins', NOW(), NOW())";
                                $db_engine->db_db_query($sql);
                            }
                            else  # Nothing to do if not pass threshold
                            {}

                    }
                    else
                    {
                            // Finishing
                            $t_OTRecordID = $temp[0][0];
                            $t_Start = $temp[0][1];
                            $time_diff = $ts_now - $t_Start;
                            $OTmins = round($time_diff/60);
                            $sql = "UPDATE CARD_STAFF_ATTENDANCE2_OT_RECORD
                                                    SET EndTime = '$time_string', OTmins = '$OTmins',
                                                    DateModified = NOW()
                                                    WHERE RecordID = $t_OTRecordID";
                            $db_engine->db_db_query($sql);
                    }


                // Response
                echo mb_convert_encoding(output_env_str(CARD_RESP_STAFF_LEAVE_SCHOOL_NORMAL."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"),"BIG5","UTF-8");
                intranet_closedb();
                exit();


            }
            else
            {
                // Within

                if ($noDailyRecord || $r_InSchoolStatus == CARD_STATUS_ABSENT)     # First Record or confirmed absent
                {
                    // Late
                    $time_diff = $ts_now - $dutyStart;
                    $minLate = round($time_diff/60);
                    $sql = "UPDATE $table_name
                               SET StaffPresent = 1, InTime = '$time_string',
                                   InSchoolStatus = '".CARD_STATUS_LATE."',
                                   InSchoolStation = '$sitename',
                                   MinLate = '$minLate', DateModified = now()
                             WHERE RecordID = $r_RecordID
                               ";
                    $db_engine->db_db_query($sql);

                     // Remove Profile Record ( Absent)
                        $sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_PROFILE
                               WHERE StaffID = '$targetUserID' AND RecordDate = '$today'
                                     AND RecordType = '".PROFILE_TYPE_ABSENT."'
                                     ";

                $db_engine->db_db_query($sql);

                    if ($dutyInWaived)
                    {
                        // Mark as waived and response as on time
                        $sql = "UPDATE $table_name
                                       SET InWaived = 1
                                       WHERE RecordID = $r_RecordID
                               ";
                        $db_engine->db_db_query($sql);
                        // Response
                        echo mb_convert_encoding(output_env_str(CARD_RESP_STAFF_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin"),"BIG5","UTF-8");
                        intranet_closedb();
                        exit();
                    }
                    else
                    {
                        // Insert Profile Record
                        if ($directProfileInput)
                        {

                            $sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
                                   (StaffID, RecordDate, RecordType, ReasonType, DateInput, DateModified)
                                   VALUES
                                   ('$targetUserID', '$today', '".PROFILE_TYPE_LATE."',
                                   '$t_LeaveReasonType',
                                   NOW(), NOW()
                                   )";
                            $db_engine->db_db_query($sql);

                        }
                        // Response
                        echo mb_convert_encoding(output_env_str(CARD_RESP_STAFF_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin"),"BIG5","UTF-8");
                        intranet_closedb();
                        exit();
                    }
                }
                else
                {
                    // Intermediate Records
                    $inter_table_name = buildStaffAttendance2InterLog($year, $month);
                    $sql = "INSERT INTO $inter_table_name
                                   (StaffID, DayNumber, RecordTime, DateInput, DateModified)
                                   VALUES
                                   ('$targetUserID', '$day', '$time_string', now(), now())";
                    $db_engine->db_db_query($sql);


                    if ($directProfileInput)
                    {

                        // Early Leave Record
                       $sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
                                   (StaffID, RecordDate, RecordType, ReasonType, DateInput, DateModified)
                                   VALUES
                                   ('$targetUserID', '$today', '".PROFILE_TYPE_EARLY."',
                                   '$t_LeaveReasonType',
                                   NOW(), NOW()
                                   )";
                           $db_engine->db_db_query($sql);
                    }
                    // Response
                    echo mb_convert_encoding(output_env_str(CARD_RESP_STAFF_LEAVE_SCHOOL_EARLY."###$targetName".$attend_lang['StaffEarlyLeave']."$time_string"."###$targetUserLogin"),"BIG5","UTF-8");
                    intranet_closedb();
                    exit();

                }


            }

        }


    }
    else if ($module_version['StaffAttendance'] == 3.0)
    {
    	include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
    	
    	$StaffAttend3 = new libstaffattend3();
    	
    	$current_time = time();
			if ($external_time != "") {
				$current_time = strtotime(date('Y-m-d') . " " . $external_time);
			}
			$time_string = date('H:i:s',$current_time);
			
    	$Result = $StaffAttend3->Record_Tap_Card($targetUserID,$current_time,$sitename);
			
			switch ($Result[1]) {
				case CARD_RESP_NO_NEED_TO_TAKE: 
					$ResponseMsg = $attend_lang['NonSchoolDay'];
					break;
				case CARD_RESP_STAFF_IN_SCHOOL_ONTIME: 
					$SlotInfo = $Result[0].': ';
					$ResponseMsg = $attend_lang['InSchool'];
					break;
				case CARD_RESP_STAFF_IN_SCHOOL_LATE: 
					$SlotInfo = $Result[0].': ';
					$ResponseMsg = $attend_lang['InSchool'];
					break;
				case CARD_RESP_STAFF_LEAVE_SCHOOL_NORMAL: 
					$SlotInfo = $Result[0].': ';
					$ResponseMsg = $attend_lang['LeaveSchool'];
					break;
				case CARD_RESP_STAFF_LEAVE_SCHOOL_EARLY: 
					$SlotInfo = $Result[0].': ';
					$ResponseMsg = $attend_lang['StaffEarlyLeave'];
					break;
				case STAFF_LEAVE_TYPE_HOLIDAY: 
					$SlotInfo = $Result[0].': ';
					$ResponseMsg = $attend_lang['RecordSuccessful'];
					break;
				case STAFF_LEAVE_TYPE_OUTGOING: 
					$SlotInfo = $Result[0].': ';
					$ResponseMsg = $attend_lang['RecordSuccessful'];
					break;
				case CARD_RESP_IGNORE_WITHIN_PERIOD:
					$ResponseMsg = $attend_lang['WithinIgnorePeriod'];
					break;
				default:
					break;
			}

			echo $Result[1]."###".$SlotInfo." ".$targetName.$ResponseMsg.$time_string."###$targetUserLogin";
      intranet_closedb();
      exit();
    }
    else # Old version (1.0)
    {

             $sql = "INSERT INTO CARD_STAFF_ATTENDANCE_LOG (CardID,SiteName,RecordedTime,Type)
                            VALUES ('$CardID','$sitename',now(),'$datatype')";
             $db_engine->db_db_query($sql);
             $record = $db_engine->db_insert_id();

             $sql = "SELECT DATE_FORMAT(RecordedTime,'%H:%i:%s') FROM CARD_STAFF_ATTENDANCE_LOG WHERE LogID = '$record'";
             $loggedTime = $db_engine->returnVector($sql);

             $user_field = getNameFieldByLang();
             $sql = "SELECT UserID, CardID, $user_field, UserLogin FROM INTRANET_USER WHERE CardID = '$CardID'";
             $user = $db_engine->returnArray($sql,4);
             list($id,$card,$name,$login) = $user[0];
             if ($dlang == "b5")
             {
                 $str = "�w����.\r\n �ɶ���: ";
                 $fail = "��������Ʀ��~, �Э��թ��p���t�κ޲z��.";
             }
             else
             {
                 $str = "is recorded.\r\n Time Recorded: ";
                 $fail = "Card Error. Please retry or contact System Administrator.";
             }
             if ($id!="")
             {
                 echo mb_convert_encoding(output_env_str(CARD_RESP_AM_IN_SCHOOL_ONTIME."###$name $str ".$loggedTime[0]."###$targetUserLogin"),"BIG5","UTF-8");
             }
             else
             {
                 echo mb_convert_encoding(output_env_str(CARD_RESP_CARD_NOT_REGISTER."###$fail"),"BIG5","UTF-8");
             }
    }

}
else if ($targetType == 2)
{
	// get all general setting for student attendance
	$GeneralSetting = new libgeneralsettings();
	$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance');
	
	# Check IP
	if (!isIPAllowed('StudentAttendance'))
	{
	     echo output_env_str(CARD_RESP_INVALID_IP."###".$attend_lang['InvalidIP']);
	     intranet_closedb();
	     exit();
	}
	
	$lc = new libcardstudentattend2();
	$targetName = "$targetName ($targetClass-$targetClassNumber)";
	
	##### Retrive the Time table mode setting #####
	//$time_table = trim(get_file_content("$intranet_root/file/time_table_mode.txt"));
	$time_table = $Settings['TimeTableMode'];
  $time_table_mode = $time_table;         ### 0 - Time Slot, 1 - Time Session
	
	##### Get Ignore period for card tapping #####
	//$ignore_period = get_file_content("$intranet_root/file/stattend_ignore.txt");
	$ignore_period = $Settings['IgnorePeriod'];
	$ignore_period += 0;
	
	##### Get Attendance Mode Setting #####
	//$content_basic = trim(get_file_content("$intranet_root/file/stattend_basic.txt"));
	$content_basic = $Settings['AttendanceMode'];
	$attendance_mode = $content_basic;
	
	// convert big5 chinese character from VB client terminal
	$CurrentEncoding = mb_detect_encoding($_REQUEST['sitename'],'BIG-5,UTF-8');
	if ($CurrentEncoding != "UTF-8") {
		$sitename = mb_convert_encoding($_REQUEST['sitename'],"UTF-8",$CurrentEncoding);
	}
		
	$null_record_variable = "NULL";
	
	# Get ClassID
	$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE ClassTitleEN = '$targetClass' and AcademicYearID = '".Get_Current_Academic_Year_ID()."'";
	$temp = $db_engine->returnVector($sql);
	$targetClassID = $temp[0];
	
	# Get Record
	# Create Monthly data table
	$current_time = time();
	if ($external_time != "")
	{
	   $current_time = strtotime(date('Y-m-d') . " " . $external_time);
	}
	$month = date('m',$current_time);
	$year = date('Y',$current_time);
	$day = date('d',$current_time);
	$today = date('Y-m-d',$current_time);
	$time_string = date('H:i:s',$current_time);
	$ts_now = $current_time - strtotime($today);
	
	# Check need to take attendance or not
	$FinalSetting = $lc->Get_Student_Attend_Time_Setting($today,$targetUserID);
	if ($FinalSetting === "NonSchoolDay")
	{
	   echo mb_convert_encoding(output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###".$attend_lang['NoNeedToTakeAttendance']."###$targetUserLogin"),"BIG5","UTF-8");
	   intranet_closedb();
	   exit();
	}
	
	$ts_morningTime = $lc->timeToSec($FinalSetting['MorningTime']);
	$ts_lunchStart = $lc->timeToSec($FinalSetting['LunchStart']);
	$ts_lunchEnd = $lc->timeToSec($FinalSetting['LunchEnd']);
	$ts_leaveSchool = $lc->timeToSec($FinalSetting['LeaveSchoolTime']);
	
	/*echo $today.'<br>';
	echo $FinalSetting['MorningTime'].','.$FinalSetting['LunchStart'].','.$FinalSetting['LunchEnd'].','.$FinalSetting['LeaveSchoolTime'];
	echo "$ts_morningTime,$ts_lunchStart,$ts_lunchStart,$ts_leaveSchool";*/
	
	if($bug_tracing['smartcard_student_attend_status']){
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		if(!is_dir($file_path."/file/log_student_attend_status")){
		        $lf = new libfilesystem();
		        $lf->folder_new($file_path."/file/log_student_attend_status");
		}
		$log_filepath = "$file_path/file/log_student_attend_status/log_student_attend_status_".($year.$month.$day).".txt";
	}
	
	buildMonthTable($year, $month);
	createEntry_Card_Student_Record_Storage($year,$month);

	/*  Get Reminder Message */
	if ($getremind)
	{
	   $sql = "SELECT ReminderID, Reason
	                  FROM CARD_STUDENT_REMINDER
	                  WHERE StudentID = $targetUserID AND DateOfReminder = '$today'";
	   $temp = $db_engine->returnArray($sql,2);
	
	   if (sizeof($temp)==0 || $temp[0][0] == "")
	   {
	       $t_remind_id = 0;
	       $t_remind_msg = $reminder_null_text;
	   }
	   else
	   {
	       $t_remind_id = 0;
	       $t_remind_msg = "";
	       $delim = "";
	       for($i=0; $i<sizeof($temp); $i++)
	       {
	           list($s_remind_id, $s_remind_msg) = $temp[$i];
	           $t_remind_msg .= $delim.$s_remind_msg;
	           $delim = "\n";
	       }
	       $t_remind_id = $s_remind_id+0;
	   }
	}
	else
	{
	    $t_remind_id = 0;
	    $t_remind_msg = $reminder_null_text;
	}

  ### Created By Ronald On 3 Sep 2007 ###
  ###  Get The Student Leave Option  ###
  if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
  {
		$current_weekday = date("w");
		$sql = "SELECT Weekday".$current_weekday." FROM CARD_STUDENT_LEAVE_OPTION WHERE StudentID = $targetUserID";
		$result = $db_engine->returnArray($sql,1);
		for($i=0; $i<sizeof($result); $i++)
		{
      list($leave_option) = $result[$i];
      $t_leave_option = $leave_option;
		}
  }
  ######################################

	# Get Record From table
	$dailylog_tablename = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
	$sql = "SELECT RecordID, TIME_TO_SEC(InSchoolTime),
	              IFNULL(AMStatus,'$null_record_variable'), TIME_TO_SEC(LunchOutTime), TIME_TO_SEC(LunchBackTime),
	              IFNULL(PMStatus,'$null_record_variable'), TIME_TO_SEC(LeaveSchoolTime), LeaveStatus, UNIX_TIMESTAMP(DateModified)
	              FROM $dailylog_tablename
	              WHERE UserID = '$targetUserID' AND DayNumber = '$day'";
	$temp = $db_engine->returnArray($sql,9);
	list($DayRecordID, $curr_InSchoolTime, $curr_AMStatus, $curr_LunchOutTime, $curr_LunchBackTime, $curr_PMStatus, $curr_LeaveSchoolTime, $curr_LeaveStatus, $curr_tslastMod) = $temp[0];

	/*if (sizeof($temp)==0 || $ts_recordID == "")
	{
	   echo mb_convert_encoding(output_env_str(CARD_RESP_SYSTEM_NOT_INIT."###".$attend_lang['SystemNotInit']),"BIG5","UTF-8");
	   intranet_closedb();
	   exit();
	}*/

	if ($FinalSetting === "NonSchoolDay")
	{
		echo mb_convert_encoding(output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['NonSchoolDay']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
		intranet_closedb();
		exit();
	}
	
	# Check Last Card Read Time for ignore period
	if ($curr_tslastMod == "")
	{
	   # Insert Record
	   $sql = "INSERT INTO $dailylog_tablename (UserID, DayNumber, DateInput, DateModified)
	                  VALUES ('$targetUserID','$day',now(),now())";
	   $db_engine->db_db_query($sql);
	   $curr_AMStatus = $null_record_variable;
	   $curr_PMStatus = $null_record_variable;
	
	}
	else
	{
	 if ($ignore_period > 0)
	 {
	   $ts_ignore = $ignore_period * 60;
	   # Check whether it is w/i last n mins
	   if ($current_time - $curr_tslastMod < $ts_ignore)
	   {
	     $tslastMod_timeonly = $curr_tslastMod - strtotime($today);
	     if ($tslastMod_timeonly == $curr_InSchoolTime
	         || $tslastMod_timeonly == $curr_LunchOutTime
	         || $tslastMod_timeonly == $curr_LunchBackTime
	         || $tslastMod_timeonly == $curr_LeaveSchoolTime
	         )
	     {
	       echo mb_convert_encoding(output_env_str(CARD_RESP_IGNORE_WITHIN_PERIOD."###".$attend_lang['WithinIgnorePeriod']."###$targetUserLogin"),"BIG5","UTF-8");
	       intranet_closedb();
	       exit();
	     }
	   }
	 }
	}
	
	if ($attendance_mode == 0) # 0 - AM Only
	{
		if ($ts_now <= $ts_morningTime)
		{
		   if ($curr_InSchoolTime=="" || $curr_InSchoolTime=="0" || $ts_now <= $curr_InSchoolTime )
		   {
		       # Before school starts
		       # On Time
		       # Mark AM Status, InSchool Time
		       $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', AMStatus = '".CARD_STATUS_PRESENT."',
		                      InSchoolStation = '$sitename', DateModified = now()
		                      WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
		       $db_engine->db_db_query($sql);
		       echo mb_convert_encoding(output_env_str(CARD_RESP_AM_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
		       send_sms(1);
		       intranet_closedb();
		       exit();
		   }
		   else
		   {
		       # Before school starts
		       # Already marked on time, no need to handle
		       echo mb_convert_encoding(output_env_str(CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
		       intranet_closedb();
		       exit();
		   }
		}
		else if ($ts_now >= $ts_leaveSchool)
		{
			# After School
			# Mark Leave Status, Leave Time
			send_sms(2, $dailylog_tablename);
			$sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_NORMAL."',
			              LeaveSchoolStation = '$sitename', DateModified = now()
			              WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			$db_engine->db_db_query($sql);
			
			# Remove Early Leave Record
			$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
			              WHERE RecordType = '".PROFILE_TYPE_EARLY."' AND UserID = '$targetUserID'
			                    AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
			$db_engine->db_db_query($sql);
			
			# Remove Reason Record of Early Leave
			$sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
			              WHERE RecordDate = '$today' AND StudentID = '$targetUserID'
			                    AND RecordType = '".PROFILE_TYPE_EARLY."' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
			$db_engine->db_db_query($sql);
			
			if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
			{
			       echo mb_convert_encoding(output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option"),"BIG5","UTF-8");
			}
			else
			{
			       echo mb_convert_encoding(output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
			}
			intranet_closedb();
			exit();
		}
		# Lesson Time
		else if ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT)
		{
	    # Lesson Time and AMStatus is NULL or set Absent
	    # Mark as Late
	    # Mark AM Status, InSchool Time
	    $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', AMStatus = '".CARD_STATUS_LATE."',
	                   InSchoolStation = '$sitename', DateModified = now()
	                   WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
	    $db_engine->db_db_query($sql);
	
	    # Remove Previous Absent Record
	    $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
	                   WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$targetUserID'
	                         AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
	    $db_engine->db_db_query($sql);
	
	    $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
	                   WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$targetUserID'
	                         AND RecordDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
	    $db_engine->db_db_query($sql);
	
	    if ($directProfileInput)
	    {
	        # Add to student profile
	        $year = getCurrentAcademicYear();
	        $semester = getCurrentSemester();
	        $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
	        $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
	        $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
	        $db_engine->db_db_query($sql);
	        $insert_id = $db_engine->db_insert_id();
	        # Update to reason table
	        $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
	        $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
	        $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
	                       VALUES ($fieldsvalues)";
	        $db_engine->db_db_query($sql);
	        if ($plugin['Discipline'])
	        {
	            # For Discipline System upgrade
	            include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
	            $ldiscipline = new libdiscipline();
	            $ldiscipline->calculateUpgradeLateToDemerit($targetUserID);
	            $ldiscipline->calculateUpgradeLateToDetention($targetUserID);
	        }
	    }
	    else    # Not insert to profile nor reason record
	    {
	    }
	
	
	    # Response
	    echo mb_convert_encoding(output_env_str(CARD_RESP_AM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
	    intranet_closedb();
	    exit();
		}/*
		else if ($curr_AMStatus == CARD_STATUS_OUTING)
		{
		}*/
		else # Early Leave (as already Present, Late or Outing)
		{
	    # Mark Leave Status - AM early leave, Leave Time
	    $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_AM."',
	                   LeaveSchoolStation = '$sitename', DateModified = now()
	                   WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
	    $db_engine->db_db_query($sql);
	
	    if ($directProfileInput)
	    {
	        # Add to student profile
	        $year = getCurrentAcademicYear();
	        $semester = getCurrentSemester();
	        $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
	        $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
	        $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
	        $db_engine->db_db_query($sql);
	        $insert_id = $db_engine->db_insert_id();
	        # Update to reason table
	        $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
	        $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
	        $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
	                       VALUES ($fieldsvalues)";
	        $db_engine->db_db_query($sql);
	    }
	    else
	    {
	    }
	    if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
	    {
	            echo mb_convert_encoding(output_env_str(CARD_RESP_AM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option"),"BIG5","UTF-8");
	    }
	    else
	    {
	            echo mb_convert_encoding(output_env_str(CARD_RESP_AM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
	    }
	    intranet_closedb();
	    exit();
		}
	}
	else if ($attendance_mode == 1) # 1 - PM Only
	{
		if ($ts_now <= $ts_lunchEnd ) # $ts_morningTime )
		{
			if ($curr_InSchoolTime=="" || $curr_InSchoolTime=="0" || $ts_now <= $curr_InSchoolTime )
			{
			   # On Time
			   # Mark PM Status, InSchool Time
			   $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', PMStatus = '".CARD_STATUS_PRESENT."',
			                  InSchoolStation = '$sitename', DateModified = now()
			                  WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			   $db_engine->db_db_query($sql);
			   echo mb_convert_encoding(output_env_str(CARD_RESP_PM_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
			   send_sms(1);
			   intranet_closedb();
			   exit();
			}
			else
			{
			   echo mb_convert_encoding(output_env_str(CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
			   intranet_closedb();
			   exit();
			}
		}
		else if ($ts_now >= $ts_leaveSchool)
		{
			# After School
			# Mark Leave Status, Leave Time
			send_sms(2, $dailylog_tablename);
			$sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_NORMAL."',
			              LeaveSchoolStation = '$sitename', DateModified = now()
			              WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			$db_engine->db_db_query($sql);
			
			# Remove Early Leave Record
			$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
			              WHERE RecordType = '".PROFILE_TYPE_EARLY."' AND UserID = '$targetUserID'
			                    AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
			$db_engine->db_db_query($sql);
			
			# Remove Reason Record of Early Leave
			$sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
			              WHERE RecordDate = '$today' AND StudentID = '$targetUserID'
			                    AND RecordType = '".PROFILE_TYPE_EARLY."' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
			$db_engine->db_db_query($sql);
			
			if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
			{
			       echo mb_convert_encoding(output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option"),"BIG5","UTF-8");
			}
			else
			{
			       echo mb_convert_encoding(output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
			}
			
			intranet_closedb();
			exit();
		}
		else if ($curr_PMStatus == $null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT)
		{
			# Lesson Time and PMStatus is NULL or set Absent
			# Mark as Late
			# Mark PMStatus, InSchool Time
			$sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', PMStatus = '".CARD_STATUS_LATE."',
			               InSchoolStation = '$sitename', DateModified = now()
			               WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			$db_engine->db_db_query($sql);
			
			# Remove Previous Absent Record
			$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
			               WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$targetUserID'
			                     AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
			$db_engine->db_db_query($sql);
			
			# Remove Previous Absent Record
			$sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
			               WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$targetUserID'
			                     AND RecordDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
			$db_engine->db_db_query($sql);
			
			if ($directProfileInput)
			{
			    # Add to student profile
			    $year = getCurrentAcademicYear();
			    $semester = getCurrentSemester();
			    $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
			    $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
			    $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
			    $db_engine->db_db_query($sql);
			    $insert_id = $db_engine->db_insert_id();
			    # Update to reason table
			    $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
			    $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
			    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
			                   VALUES ($fieldsvalues)";
			    $db_engine->db_db_query($sql);
			
			    if ($plugin['Discipline'])
			    {
			        # For Discipline System upgrade
			        include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
			        $ldiscipline = new libdiscipline();
			        $ldiscipline->calculateUpgradeLateToDemerit($targetUserID);
			        $ldiscipline->calculateUpgradeLateToDetention($targetUserID);
			    }
			}
			else
			{
			}
		
	    # Response
	    echo mb_convert_encoding(output_env_str(CARD_RESP_PM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
	    intranet_closedb();
	    exit();
		}/*
		else if ($curr_PMStatus == CARD_STATUS_OUTING)
		{
		}*/
		else # Early Leave (PM= Present, Late, Outing)
		{
		    # Mark Leave Status -> PM Early, Leave Time
		    $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_PM."',
		                   LeaveSchoolStation = '$sitename', DateModified = now()
		                   WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
		    $db_engine->db_db_query($sql);
		
		    if ($directProfileInput)
		    {
		        # Add to PM Early leave student profile
		        $year = getCurrentAcademicYear();
		        $semester = getCurrentSemester();
		        $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
		        $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
		        $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
		        $db_engine->db_db_query($sql);
		        $insert_id = $db_engine->db_insert_id();
		        # Update to reason table
		        $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
		        $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
		        $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
		                       VALUES ($fieldsvalues)";
		        $db_engine->db_db_query($sql);
		    }
		    else
		    {
		    }
		
		    if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
		    {
		            echo mb_convert_encoding(output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option"),"BIG5","UTF-8");
		    }
		    else
		    {
		            echo mb_convert_encoding(output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
		    }
		    intranet_closedb();
		    exit();
		}
	}
	else if ($attendance_mode == 2) # 2 - WD w/ Lunch
	{
		if ($ts_now <= $ts_morningTime)
		{
	    if ($curr_InSchoolTime=="" || $curr_InSchoolTime=="0" || $ts_now <= $curr_InSchoolTime )
	    {
	      # On Time
	      # Mark AM Status, InSchool Time
	      $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', AMStatus = '".CARD_STATUS_PRESENT."',
	                     InSchoolStation = '$sitename', DateModified = now()
	                     WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
	      $db_engine->db_db_query($sql);
	
	      ### Bug Tracing
        if($bug_tracing['smartcard_student_attend_status']){
          $log_date = date('Y-m-d H:i:s');
          $log_target_date = date('Y-m-d H:i:s');
          $log_student_id = $targetUserID;
          $log_old_status = $curr_AMStatus;
          $log_new_status = CARD_STATUS_PRESENT;
          $log_sql = $sql;
          $log_admin_user = "terminal";
          $log_time = "id=1,ts_now = $ts_now,ts_morningTime = $ts_morningTime";

          $log_page = 'receiver.php';
          $log_content = get_file_content($log_filepath);
          $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
          $log_content .= $log_entry;
          write_file_content($log_content, $log_filepath);
        }
	
	
	      echo mb_convert_encoding(output_env_str(CARD_RESP_AM_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
	      send_sms(1);
	      intranet_closedb();
	      exit();
	    }
	    else
	    {
	        echo mb_convert_encoding(output_env_str(CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
	        intranet_closedb();
	        exit();
	    }
		}
		else if ($ts_now < $ts_lunchStart) # In AM Lesson Time
		{
			if ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT)
			{
				# Late
				# Mark AM Status, InSchool Time
				$sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', AMStatus = '".CARD_STATUS_LATE."',
				              InSchoolStation = '$sitename', DateModified = now()
				              WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
				$db_engine->db_db_query($sql);
				
				### Bug Tracing
				if($bug_tracing['smartcard_student_attend_status']){
					$log_date = date('Y-m-d H:i:s');
				  $log_target_date = date('Y-m-d H:i:s');
				  $log_student_id = $targetUserID;
				  $log_old_status = $curr_AMStatus;
				  $log_new_status = CARD_STATUS_LATE;
				  $log_sql = $sql;
				  $log_admin_user = "terminal";
				  $log_time = "id=2,ts_now = $ts_now,ts_lunchStart = $ts_lunchStart";
				
          $log_page = 'receiver.php';
          $log_content = get_file_content($log_filepath);
          $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
          $log_content .= $log_entry;
          write_file_content($log_content, $log_filepath);
				}
				
				# Remove Previous Absent Record
				$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
				              WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$targetUserID'
				                    AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
				$db_engine->db_db_query($sql);
				
				# Remove Previous Absent Reason Record
				$sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
				         WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$targetUserID'
				               AND RecordDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
				          $db_engine->db_db_query($sql);
			
				if ($directProfileInput)
				{
					# Add late record to student profile
					$year = getCurrentAcademicYear();
					$semester = getCurrentSemester();
					$fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
					$fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
					$sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
					$db_engine->db_db_query($sql);
					$insert_id = $db_engine->db_insert_id();
					# Update to reason table
					$fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
					$fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
					$sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
					              VALUES ($fieldsvalues)";
					$db_engine->db_db_query($sql);
					if ($plugin['Discipline'])
					{
						# For Discipline System upgrade
						include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
						$ldiscipline = new libdiscipline();
						$ldiscipline->calculateUpgradeLateToDemerit($targetUserID);
						$ldiscipline->calculateUpgradeLateToDetention($targetUserID);
					}
				}
				else
				{}
			
				# Response
				echo mb_convert_encoding(output_env_str(CARD_RESP_AM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
				intranet_closedb();
				exit();
			}/*
			else if ($curr_AMStatus == CARD_STATUS_OUTING)
			{
			}*/
			else     # Present/Late/Outing - AM early leave
			{
			 # AM Early Leave
			 # Mark Leave Status, Leave Time
			 $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_AM."',
			                LeaveSchoolStation = '$sitename', DateModified = now()
			                WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			 $db_engine->db_db_query($sql);
			
			### Bug Tracing
			      if($bug_tracing['smartcard_student_attend_status']){
			              $log_date = date('Y-m-d H:i:s');
			              $log_target_date = date('Y-m-d H:i:s');
			              $log_student_id = $targetUserID;
			              $log_old_status = $curr_LeaveStatus;
			              $log_new_status = CARD_LEAVE_AM;
			              $log_sql = $sql;
			              $log_admin_user = "terminal";
			              $log_time = "id=3,ts_now = $ts_now,ts_lunchStart = $ts_lunchStart";
			
			                           $log_page = 'receiver.php';
			                          $log_content = get_file_content($log_filepath);
			                          $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
			                          $log_content .= $log_entry;
			                          write_file_content($log_content, $log_filepath);
			          }
			
			
			 if ($directProfileInput)
			 {
			     # Add Early Leave record to student profile
			     $year = getCurrentAcademicYear();
			     $semester = getCurrentSemester();
			     $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
			     $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
			     $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
			     $db_engine->db_db_query($sql);
			     $insert_id = $db_engine->db_insert_id();
			     # Update to reason table
			     $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
			     $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
			     $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
			                    VALUES ($fieldsvalues)";
			     $db_engine->db_db_query($sql);
			 }
			 else
			 {
			 }
			
			 if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
			 {
			         echo mb_convert_encoding(output_env_str(CARD_RESP_AM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option"),"BIG5","UTF-8");
			 }
			 else
			 {
			         echo mb_convert_encoding(output_env_str(CARD_RESP_AM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
			 }
			 intranet_closedb();
			 exit();
			}
		}
		else if ($ts_now <= $ts_lunchEnd) # In Lunch Time
		{
		     if ($curr_AMStatus == CARD_STATUS_ABSENT || $curr_AMStatus==CARD_STATUS_OUTING)  # PM Present (AM Absent/Outing) - Mark Incoming Time
		     {
		         if ($curr_PMStatus == $null_record_variable && ($curr_InSchoolTime=="" || $curr_InSchoolTime > $ts_now))
		         {
		             # Mark PM Status, InSchool Time
		             $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', PMStatus = '".CARD_STATUS_PRESENT."',
		                            InSchoolStation = '$sitename',
		                            LunchBackTime = '$time_string' , LunchBackStation = '$sitename',
		                            DateModified = now()
		                            WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
		             $db_engine->db_db_query($sql);
		
		          ### Bug Tracing
		              if($bug_tracing['smartcard_student_attend_status']){
		                      $log_date = date('Y-m-d H:i:s');
		                      $log_target_date = date('Y-m-d H:i:s');
		                      $log_student_id = $targetUserID;
		                      $log_old_status = $curr_PMStatus;
		                      $log_new_status = CARD_STATUS_PRESENT;
		                      $log_sql = $sql;
		                      $log_admin_user = "terminal";
		                      $log_time = "id=4,ts_now = $ts_now,ts_lunchEnd = $ts_lunchEnd";
		
		                                   $log_page = 'receiver.php';
		                                  $log_content = get_file_content($log_filepath);
		                                  $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
		                                  $log_content .= $log_entry;
		                                  write_file_content($log_content, $log_filepath);
		                  }
		
		
		
		             echo mb_convert_encoding(output_env_str(CARD_RESP_PM_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
		             intranet_closedb();
		             exit();
		         }
		         else
		         {
		             # Print already back
		             echo mb_convert_encoding(output_env_str(CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
		             intranet_closedb();
		             exit();
		         }
		     }
		     else # AM Present/Late, Lunch checking
		     {
		         # Get Lunch Settings
		         /*$content_lunch_misc = trim(get_file_content("$intranet_root/file/stattend_lunch_misc.txt"));
		         $lunch_misc_settings = explode("\n",$content_lunch_misc);
		         list($lunch_misc_no_need_record,$lunch_misc_once_only,$lunch_misc_all_allowed) = $lunch_misc_settings;*/
		         $lunch_misc_no_need_record = $Settings['NoRecordLunchOut'];
		         $lunch_misc_once_only = $Settings['LunchOutOnce'];
		         $lunch_misc_all_allowed = $Settings['AllAllowGoOut'];
		
		         if ($lunch_misc_no_need_record)
		         {
		             # Lunch Back
		             # Mark PM Status, Lunch Back Time
		             $sql = "UPDATE $dailylog_tablename SET LunchBackTime = '$time_string', PMStatus = '".CARD_STATUS_PRESENT."',
		                     LunchBackStation = '$sitename', DateModified = now()
		                     WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
		             $db_engine->db_db_query($sql);
		
		                                         ### Bug Tracing
		              if($bug_tracing['smartcard_student_attend_status']){
		                      $log_date = date('Y-m-d H:i:s');
		                      $log_target_date = date('Y-m-d H:i:s');
		                      $log_student_id = $targetUserID;
		                      $log_old_status = $curr_PMStatus;
		                      $log_new_status = CARD_STATUS_PRESENT;
		                      $log_sql = $sql;
		                      $log_admin_user = "terminal";
		                      $log_time = "id=5,ts_now = $ts_now,ts_lunchEnd = $ts_lunchEnd";
		
		                                   $log_page = 'receiver.php';
		                                  $log_content = get_file_content($log_filepath);
		                                  $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
		                                  $log_content .= $log_entry;
		                                  write_file_content($log_content, $log_filepath);
		                  }
		
		
		             echo mb_convert_encoding(output_env_str(CARD_RESP_FROM_LUNCH_ONTIME."###$targetName".$attend_lang['BackFromLunch']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
		             intranet_closedb();
		             exit();
		         }
		         else
		         {
		             if ($curr_LunchOutTime == "")   # Not yet out
		             {
		                 # Check Lunch Out allow
		                 if (!$lunch_misc_all_allowed)
		                 {
		                      # Check in Database
		                      $sql = "SELECT StudentID FROM CARD_STUDENT_LUNCH_ALLOW_LIST
		                                     WHERE StudentID = '$targetUserID'";
		                      $temp = $db_engine->returnVector($sql);
		                      if ($temp[0] == "") # Lunch Out NOT allowed
		                      {
		                          # Store Bad action : Lunch out failed
		                          $sql = "INSERT INTO CARD_STUDENT_BAD_ACTION (StudentID, RecordDate, RecordTime, RecordType, DateInput, DateModified)
		                                         VALUES ('$targetUserID', '$today','$today $time_string' , '".CARD_BADACTION_LUNCH_NOTINLIST."', now(), now())";
		                          $db_engine->db_db_query($sql);
		
		                          echo mb_convert_encoding(output_env_str(CARD_RESP_FAILED_OUT_LUNCH_NOTINLIST."###".$attend_lang['NotAllowToOutLunch']." - ".$targetName."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
		                          intranet_closedb();
		                          exit();
		                      }
		                 }
		
		                 # Allow to Go out
		                 # Mark Lunch Out Time, Clear Leave Status
		                 $sql = "UPDATE $dailylog_tablename SET LunchOutTime = '$time_string', LeaveStatus = NULL,
		                                LunchOutStation = '$sitename', DateModified = now()
		                                WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
		                 $db_engine->db_db_query($sql);
		                 echo mb_convert_encoding(output_env_str(CARD_RESP_GO_OUT_LUNCH."###$targetName".$attend_lang['OutLunch']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
		                 intranet_closedb();
		                 exit();
		
		             }
		             else if ($curr_LunchBackTime == "")    # Not Yet back from Lunch
		             {
		                  # Mark Lunch Back time, PM Status
		                  $sql = "UPDATE $dailylog_tablename SET LunchBackTime = '$time_string', PMStatus = '".CARD_STATUS_PRESENT."',
		                                 LunchBackStation = '$sitename', DateModified = now()
		                                 WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
		                  $db_engine->db_db_query($sql);
		
		                  ### Bug Tracing
		                              if($bug_tracing['smartcard_student_attend_status']){
		                                      $log_date = date('Y-m-d H:i:s');
		                                      $log_target_date = date('Y-m-d H:i:s');
		                                      $log_student_id = $targetUserID;
		                                      $log_old_status = $curr_PMStatus;
		                                      $log_new_status = CARD_STATUS_PRESENT;
		                                      $log_sql = $sql;
		                                      $log_admin_user = "terminal";
		                                      $log_time = "id=6,ts_now = $ts_now,ts_lunchEnd = $ts_lunchEnd";
		
		                                                   $log_page = 'receiver.php';
		                                                  $log_content = get_file_content($log_filepath);
		                                                  $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
		                                                  $log_content .= $log_entry;
		                                                  write_file_content($log_content, $log_filepath);
		                                  }
		
		                  echo mb_convert_encoding(output_env_str(CARD_RESP_FROM_LUNCH_ONTIME."###$targetName".$attend_lang['BackFromLunch']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
		                  intranet_closedb();
		                  exit();
		             }
		             else # Already back , and try to go out again
		             {
		                 if ($lunch_misc_once_only)
		                 {
		                     # Not allowed to go out 2nd time
		                     # Store bad action : Lunch trial 2nd time
		                     $sql = "INSERT INTO CARD_STUDENT_BAD_ACTION (StudentID, RecordDate, RecordTime, RecordType, DateInput, DateModified)
		                                    VALUES ('$targetUserID', '$today', '$today $time_string', '".CARD_BADACTION_LUNCH_BACKALREADY."', now(), now())";
		                     $db_engine->db_db_query($sql);
		                     echo mb_convert_encoding(output_env_str(CARD_RESP_FAILED_OUT_LUNCH_ALREADYBACK."###".$attend_lang['NotAllowToOutLunchAgain']." - ".$targetName."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
		                     intranet_closedb();
		                     exit();
		                 }
		                 else
		                 {
		                     # Allow to go out again
		                     # Mark Lunch Out Time, Clear Lunch Back Time, Clear PM Status
		                     $sql = "UPDATE $dailylog_tablename SET LunchOutTime = '$time_string', LeaveStatus = NULL,
		                                    LunchOutStation = '$sitename', LunchBackTime = NULL, LunchBackStation = NULL,
		                                    PMStatus = NULL, DateModified = now()
		                                    WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
		                     $db_engine->db_db_query($sql);
		
		                     ### Bug Tracing
		                                      if($bug_tracing['smartcard_student_attend_status']){
		                                              $log_date = date('Y-m-d H:i:s');
		                                              $log_target_date = date('Y-m-d H:i:s');
		                                              $log_student_id = $targetUserID;
		                                              $log_old_status = $curr_PMStatus;
		                                              $log_new_status = "NULL";
		                                              $log_sql = $sql;
		                                              $log_admin_user = "terminal";
		                                              $log_time = "id=7,ts_now = $ts_now,ts_lunchEnd = $ts_lunchEnd";
		
		                                                           $log_page = 'receiver.php';
		                                                          $log_content = get_file_content($log_filepath);
		                                                          $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
		                                                          $log_content .= $log_entry;
		                                                          write_file_content($log_content, $log_filepath);
		                                          }
		
		                     echo mb_convert_encoding(output_env_str(CARD_RESP_GO_OUT_LUNCH."###$targetName".$attend_lang['OutLunch']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
		                     intranet_closedb();
		                     exit();
		                 }
		
		             }
		         }
		     }
		}
		else if ($ts_now < $ts_leaveSchool) # PM Lesson Time
		{
		     if ($curr_AMStatus == CARD_STATUS_ABSENT || $curr_AMStatus==CARD_STATUS_OUTING)  # PM Present (AM Absent/Outing)
		     {
		         if ($curr_PMStatus==$null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT)
		         {
		             # Mark InSchool Time, PM status late
		             $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', PMStatus = '".CARD_STATUS_LATE."',
		                            InSchoolStation = '$sitename',
		                            LunchBackTime = '$time_string', LunchBackStation = '$sitename',
		                            DateModified = now()
		                            WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
		             $db_engine->db_db_query($sql);
		
		          ### Bug Tracing
		              if($bug_tracing['smartcard_student_attend_status']){
		                      $log_date = date('Y-m-d H:i:s');
		                      $log_target_date = date('Y-m-d H:i:s');
		                      $log_student_id = $targetUserID;
		                      $log_old_status = $curr_PMStatus;
		                      $log_new_status = CARD_STATUS_LATE;
		                      $log_sql = $sql;
		                      $log_admin_user = "terminal";
		                      $log_time = "id=8,ts_now = $ts_now,ts_leaveSchool = $ts_leaveSchool";
		
		                                   $log_page = 'receiver.php';
		                                  $log_content = get_file_content($log_filepath);
		                                  $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
		                                  $log_content .= $log_entry;
		                                  write_file_content($log_content, $log_filepath);
		                  }
		
		
		             # Remove Previous Absent Record
		             $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
		                            WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$targetUserID'
		                                  AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
		             $db_engine->db_db_query($sql);
		                                      # Remove Previous Absent Reason Record
		                           $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
		                                           WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$targetUserID'
		                                                 AND RecordDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
		                            $db_engine->db_db_query($sql);
		
		             if ($directProfileInput)
		             {
		                 # Add PM late to student profile
		                 $year = getCurrentAcademicYear();
		                 $semester = getCurrentSemester();
		                 $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
		                 $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
		                 $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
		                 $db_engine->db_db_query($sql);
		                 $insert_id = $db_engine->db_insert_id();
		                 # Update to reason table
		                 $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
		                 $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
		                 $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
		                                VALUES ($fieldsvalues)";
		                 $db_engine->db_db_query($sql);
		                 if ($plugin['Discipline'])
		                 {
		                     # For Discipline System upgrade
		                     include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
		                     $ldiscipline = new libdiscipline();
		                     $ldiscipline->calculateUpgradeLateToDemerit($targetUserID);
		                     $ldiscipline->calculateUpgradeLateToDetention($targetUserID);
		                 }
		             }
		             else
		             {}
		
		             # Response
		             echo mb_convert_encoding(output_env_str(CARD_RESP_PM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
		             intranet_closedb();
		             exit();
		         }
		         else
		         {
		             # Mark Leave time, Leave Status as PM Early leave
		             $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_PM."',
		                            LeaveSchoolStation = '$sitename', DateModified = now()
		                            WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
		             $db_engine->db_db_query($sql);
		
		          ### Bug Tracing
		              if($bug_tracing['smartcard_student_attend_status']){
		                      $log_date = date('Y-m-d H:i:s');
		                      $log_target_date = date('Y-m-d H:i:s');
		                      $log_student_id = $targetUserID;
		                      $log_old_status = $curr_LeaveStatus;
		                      $log_new_status = CARD_LEAVE_PM;
		                      $log_sql = $sql;
		                      $log_admin_user = "terminal";
		                      $log_time = "id=9,ts_now = $ts_now,ts_leaveSchool = $ts_leaveSchool";
		
		                                   $log_page = 'receiver.php';
		                                  $log_content = get_file_content($log_filepath);
		                                  $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
		                                  $log_content .= $log_entry;
		                                  write_file_content($log_content, $log_filepath);
		                  }
		
		             if ($directProfileInput)
		             {
		                 # Add PM Early Leave to student profile
		                 $year = getCurrentAcademicYear();
		                 $semester = getCurrentSemester();
		                 $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
		                 $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
		                 $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
		                 $db_engine->db_db_query($sql);
		                 $insert_id = $db_engine->db_insert_id();
		                 # Update to reason table
		                 $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
		                 $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
		                 $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
		                                VALUES ($fieldsvalues)";
		                 $db_engine->db_db_query($sql);
		             }
		             else
		             {}
		
		             if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
		             {
		                     echo mb_convert_encoding(output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option"),"BIG5","UTF-8");
		             }
		             else
		             {
		                     echo mb_convert_encoding(output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
		             }
		             intranet_closedb();
		             exit();
		
		         }
		     }
		     else # AM Present/Late
		     {
		         if ($curr_PMStatus == $null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT)
		#                   if ($curr_LunchOutTime != "" && $curr_LunchBackTime == "")      # Gone out lunch and back late
		         {
		             # Mark Lunch Back Time, PM status late
		             $sql = "UPDATE $dailylog_tablename SET LunchBackTime = '$time_string', PMStatus = '".CARD_STATUS_LATE."',
		                            LunchBackStation = '$sitename', DateModified = now()
		                            WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
		             $db_engine->db_db_query($sql);
		
		          ### Bug Tracing
		              if($bug_tracing['smartcard_student_attend_status']){
		                      $log_date = date('Y-m-d H:i:s');
		                      $log_target_date = date('Y-m-d H:i:s');
		                      $log_student_id = $targetUserID;
		                      $log_old_status = $curr_PMStatus;
		                      $log_new_status = CARD_STATUS_LATE;
		                      $log_sql = $sql;
		                      $log_admin_user = "terminal";
		                      $log_time = "id=10,ts_now = $ts_now,ts_leaveSchool = $ts_leaveSchool";
		
		                                   $log_page = 'receiver.php';
		                                  $log_content = get_file_content($log_filepath);
		                                  $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
		                                  $log_content .= $log_entry;
		                                  write_file_content($log_content, $log_filepath);
		                  }
		             # Remove Previous Absent Record
		             $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
		                            WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$targetUserID'
		                                  AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
		             $db_engine->db_db_query($sql);
		
		             # Remove Previous Absent Reason Record
		             $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
		                   WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$targetUserID'
		                         AND RecordDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
		                            $db_engine->db_db_query($sql);
		
		
		             if ($directProfileInput)
		             {
		                 # Add PM Late to student profile
		                 $year = getCurrentAcademicYear();
		                 $semester = getCurrentSemester();
		                 $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
		                 $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
		                 $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
		                 $db_engine->db_db_query($sql);
		                 $insert_id = $db_engine->db_insert_id();
		                 # Update to reason table
		                 $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
		                 $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
		                 $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
		                                VALUES ($fieldsvalues)";
		                 $db_engine->db_db_query($sql);
		             }
		             else
		             {}
		
		             echo mb_convert_encoding(output_env_str(CARD_RESP_BACK_FROM_LUNCH_LATE."###$targetName".$attend_lang['BackFromLunch_late']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
		             intranet_closedb();
		             exit();
		         }/*
		         else if ($curr_PMStatus == CARD_STATUS_OUTING)
		         {
		         }*/
		         else # AM Present, not go out lunch or already back from lunch => Early leave
		         {
		             # Mark Leave time, Leave Status as PM Early leave
		             $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_PM."',
		                            LeaveSchoolStation = '$sitename', DateModified = now()
		                            WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
		             $db_engine->db_db_query($sql);
		
		          ### Bug Tracing
		              if($bug_tracing['smartcard_student_attend_status']){
		                      $log_date = date('Y-m-d H:i:s');
		                      $log_target_date = date('Y-m-d H:i:s');
		                      $log_student_id = $targetUserID;
		                      $log_old_status = $curr_LeaveStatus;
		                      $log_new_status = CARD_LEAVE_PM;
		                      $log_sql = $sql;
		                      $log_admin_user = "terminal";
		                      $log_time = "id=11,ts_now = $ts_now,ts_leaveSchool = $ts_leaveSchool";
		
		                                   $log_page = 'receiver.php';
		                                  $log_content = get_file_content($log_filepath);
		                                  $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
		                                  $log_content .= $log_entry;
		                                  write_file_content($log_content, $log_filepath);
		                  }
		
		             if ($directProfileInput)
		             {
		                 # Add PM Early Leave to student profile
		                 $year = getCurrentAcademicYear();
		                 $semester = getCurrentSemester();
		                 $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
		                 $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
		                 $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
		                 $db_engine->db_db_query($sql);
		                 $insert_id = $db_engine->db_insert_id();
		                 # Update to reason table
		                 $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
		                 $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
		                 $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
		                                VALUES ($fieldsvalues)";
		                 $db_engine->db_db_query($sql);
		             }
		             else
		             {}
		
		             if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
		             {
		                     echo mb_convert_encoding(output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option"),"BIG5","UTF-8");
		             }
		             else
		             {
		                     echo mb_convert_encoding(output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
		             }
		             intranet_closedb();
		             exit();
		         }
		     }
		}
		else
		{
		    # Normal School Leave
		    # Mark Leave Status, Time
		                      send_sms(2, $dailylog_tablename);
		   $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_NORMAL."',
		                  LeaveSchoolStation = '$sitename', DateModified = now()
		                  WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
		   $db_engine->db_db_query($sql);
		
		          ### Bug Tracing
		              if($bug_tracing['smartcard_student_attend_status']){
		                      $log_date = date('Y-m-d H:i:s');
		                      $log_target_date = date('Y-m-d H:i:s');
		                      $log_student_id = $targetUserID;
		                      $log_old_status = $curr_LeaveStatus;
		                      $log_new_status = CARD_LEAVE_NORMAL;
		                      $log_sql = $sql;
		                      $log_admin_user = "terminal";
		                      $log_time = "id=12,ts_now = $ts_now,ts_leaveSchool = $ts_leaveSchool";
		
		                                   $log_page = 'receiver.php';
		                                  $log_content = get_file_content($log_filepath);
		                                  $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
		                                  $log_content .= $log_entry;
		                                  write_file_content($log_content, $log_filepath);
		                  }
		
		   # Remove Early Leave Record (AM/PM)
		   $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
		                  WHERE RecordType = '".PROFILE_TYPE_EARLY."' AND UserID = '$targetUserID'
		                        AND AttendanceDate = '$today' AND (DayType = '".PROFILE_DAY_TYPE_PM."' OR DayType='".PROFILE_DAY_TYPE_AM."')";
		   $db_engine->db_db_query($sql);
		
		   # Remove Reason Record of Early Leave (AM/PM)
		   $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
		                  WHERE RecordDate = '$today' AND StudentID = '$targetUserID'
		                        AND RecordType = '".PROFILE_TYPE_EARLY."' AND (DayType = '".PROFILE_DAY_TYPE_PM."' OR DayType='".PROFILE_DAY_TYPE_AM."')";
		   $db_engine->db_db_query($sql);
		
		   if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
		   {
		           echo mb_convert_encoding(output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option"),"BIG5","UTF-8");
		   }
		   else
		   {
		           echo mb_convert_encoding(output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
		   }
		
		   intranet_closedb();
		   exit();
		}
	}
	else if ($attendance_mode == 3) # 3 - WD w/o Lunch
	{
	    if ($ts_now <= $ts_morningTime)
	    {
	        if ($curr_InSchoolTime=="" || $curr_InSchoolTime=="0" || $ts_now <= $curr_InSchoolTime )
	        {
	            # On Time
	            # Mark AM Status, Time
	            $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', AMStatus = '".CARD_STATUS_PRESENT."',
	                           InSchoolStation = '$sitename', DateModified = now()
	                           WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
	            $db_engine->db_db_query($sql);
	            echo mb_convert_encoding(output_env_str(CARD_RESP_AM_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
	            send_sms(1);
	            intranet_closedb();
	            exit();
	        }
	        else
	        {
	            echo mb_convert_encoding(output_env_str(CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
	            intranet_closedb();
	            exit();
	        }
	    }
	    else if ($ts_now < $ts_lunchStart)  # In AM Lesson Time
	    {
	         if ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT)
	         {
	             # Late
	             # Mark AM Status, Time
	             $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', AMStatus = '".CARD_STATUS_LATE."',
	                            InSchoolStation = '$sitename', DateModified = now()
	                            WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
	             $db_engine->db_db_query($sql);
	
	             # Remove Previous Absent Record
	             $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
	                            WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$targetUserID'
	                                  AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
	             $db_engine->db_db_query($sql);
	
	             # Remove Previous Absent Record Reason
	             $sql =" DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
	                                             WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$targetUserID'
	                                  AND RecordDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
	
	             $db_engine->db_db_query($sql);
	
	             if ($directProfileInput)
	             {
	                 # Add late record to student profile
	                 $year = getCurrentAcademicYear();
	                 $semester = getCurrentSemester();
	                 $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
	                 $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
	                 $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
	                 $db_engine->db_db_query($sql);
	                 $insert_id = $db_engine->db_insert_id();
	                 # Update to reason table
	                 $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
	                 $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
	                 $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
	                                VALUES ($fieldsvalues)";
	                 $db_engine->db_db_query($sql);
	                 if ($plugin['Discipline'])
	                 {
	                     # For Discipline System upgrade
	                     include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
	                     $ldiscipline = new libdiscipline();
	                     $ldiscipline->calculateUpgradeLateToDemerit($targetUserID);
	                     $ldiscipline->calculateUpgradeLateToDetention($targetUserID);
	                 }
	             }
	             else
	             {}
	
	             # Response
	             echo mb_convert_encoding(output_env_str(CARD_RESP_AM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
	             intranet_closedb();
	             exit();
	         }
	         else
	         {
	             # Early Leave
	             # Mark Leave Status, Time
	             $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_AM."',
	                            LeaveSchoolStation = '$sitename', DateModified = now()
	                            WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
	             $db_engine->db_db_query($sql);
	
	             if ($directProfileInput)
	             {
	                 # Add Early Leave record to student profile
	                 $year = getCurrentAcademicYear();
	                 $semester = getCurrentSemester();
	                 $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
	                 $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
	                 $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
	                 $db_engine->db_db_query($sql);
	                 $insert_id = $db_engine->db_insert_id();
	                 # Update to reason table
	                 $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
	                 $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
	                 $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
	                                VALUES ($fieldsvalues)";
	                 $db_engine->db_db_query($sql);
	             }
	             else
	             {}
	
	             if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
	             {
	                     echo mb_convert_encoding(output_env_str(CARD_RESP_AM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option"),"BIG5","UTF-8");
	             }
	             else
	             {
	                     echo mb_convert_encoding(output_env_str(CARD_RESP_AM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
	             }
	             intranet_closedb();
	             exit();
	         }
	    }
	    else if ($ts_now <= $ts_lunchEnd) # In Lunch Time
	    {
	         if ($curr_AMStatus == CARD_STATUS_ABSENT || $curr_AMStatus==CARD_STATUS_OUTING)  # PM Present (AM Absent/Outing)
	         {
	             if (($curr_PMStatus==$null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT) && ($curr_InSchoolTime=="" || $curr_InSchoolTime > $ts_now))
	             {
	                  # Mark PM Status, InSchool Time
	                  $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', PMStatus = '".CARD_STATUS_PRESENT."',
	                                 InSchoolStation = '$sitename',
	                                 LunchBackTime = '$time_string', LunchBackStation = '$sitename',
	                                 DateModified = now()
	                                 WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
	                  $db_engine->db_db_query($sql);
	                  echo mb_convert_encoding(output_env_str(CARD_RESP_PM_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
	                  intranet_closedb();
	                  exit();
	             }
	             else
	             {
	                  echo mb_convert_encoding(output_env_str(CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
	                  intranet_closedb();
	                  exit();
	             }
	         }
	         else
	         {
	             # Normal Leave (Let PM absent to handle)
	             $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_NORMAL."',
	                            LeaveSchoolStation = '$sitename', DateModified = now()
	                            WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
	             $db_engine->db_db_query($sql);
	
	             if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
	             {
	                     echo mb_convert_encoding(output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option"),"BIG5","UTF-8");
	             }
	             else
	             {
	                     echo mb_convert_encoding(output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
	             }
	             intranet_closedb();
	             exit();
	         }
	    }
	    else if ($ts_now < $ts_leaveSchool) # PM Lesson Time
	    {
	         if ($curr_AMStatus == CARD_STATUS_ABSENT || $curr_AMStatus==CARD_STATUS_OUTING)  # PM Present (AM Absent/Outing)
	         {
	             if (($curr_PMStatus==$null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT))
	             {
	                  # Mark InSchool Time, PM status late
	                  $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', PMStatus = '".CARD_STATUS_LATE."',
	                                 InSchoolStation = '$sitename',
	                                 LunchBackTime = '$time_string', LunchBackStation = '$sitename',
	                                 DateModified = now()
	                                 WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
	                  $db_engine->db_db_query($sql);
	
	                  # Remove Previous Absent Record
	                  $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
	                                 WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$targetUserID'
	                                       AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
	                  $db_engine->db_db_query($sql);
	
	                 # Remove Previous Absent Reason Record
	                 $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
	                       WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$targetUserID'
	                             AND RecordDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
	                                $db_engine->db_db_query($sql);
	
	
	
	                  if ($directProfileInput)
	                  {
	                      # Add PM late to student profile
	                      $year = getCurrentAcademicYear();
	                      $semester = getCurrentSemester();
	                      $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
	                      $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
	                      $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
	                      $db_engine->db_db_query($sql);
	                      $insert_id = $db_engine->db_insert_id();
	                      # Update to reason table
	                      $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
	                      $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
	                      $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
	                                     VALUES ($fieldsvalues)";
	                      $db_engine->db_db_query($sql);
	
	                      if ($plugin['Discipline'])
	                      {
	                          # For Discipline System upgrade
	                          include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
	                          $ldiscipline = new libdiscipline();
	                          $ldiscipline->calculateUpgradeLateToDemerit($targetUserID);
	                          $ldiscipline->calculateUpgradeLateToDetention($targetUserID);
	                      }
	                  }
	                  else
	                  {}
	
	                  # Response
	                  echo mb_convert_encoding(output_env_str(CARD_RESP_PM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
	                  intranet_closedb();
	                  exit();
	             }
	             else
	             {
	                 # Mark Leave time, Leave Status as PM Early leave
	                 $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_PM."',
	                                LeaveSchoolStation = '$sitename', DateModified = now()
	                                WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
	                 $db_engine->db_db_query($sql);
	
	                 if ($directProfileInput)
	                 {
	                     # Add PM Early Leave to student profile
	                     $year = getCurrentAcademicYear();
	                     $semester = getCurrentSemester();
	                     $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
	                     $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
	                     $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
	                     $db_engine->db_db_query($sql);
	                     $insert_id = $db_engine->db_insert_id();
	                     # Update to reason table
	                     $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
	                     $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
	                     $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
	                                    VALUES ($fieldsvalues)";
	                     $db_engine->db_db_query($sql);
	                 }
	                 else
	                 {}
	
	                 if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
	                 {
	                         echo mb_convert_encoding(output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option"),"BIG5","UTF-8");
	                 }
	                 else
	                 {
	                         echo mb_convert_encoding(output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
	                 }
	                 intranet_closedb();
	                 exit();
	
	             }
	         }
	         else
	         {
	             if ($curr_PMStatus == $null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT)
	             {
	                  # Mark InSchool Time, PM status late
	                  $sql = "UPDATE $dailylog_tablename SET PMStatus = '".CARD_STATUS_LATE."',
	                                 LunchBackTime = '$time_string', LunchBackStation = '$sitename',
	                                 DateModified = now()
	                                 WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
	                  $db_engine->db_db_query($sql);
	
	                  # Remove Previous Absent Record
	                  $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
	                                 WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$targetUserID'
	                                       AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
	                  $db_engine->db_db_query($sql);
	
	                 # Remove Previous Absent Reason Record
	                 $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
	                       WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$targetUserID'
	                             AND RecordDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
	                                $db_engine->db_db_query($sql);
	
	
	
	                  if ($directProfileInput)
	                  {
	                      # Add PM late to student profile
	                      $year = getCurrentAcademicYear();
	                      $semester = getCurrentSemester();
	                      $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
	                      $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
	                      $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
	                      $db_engine->db_db_query($sql);
	                      $insert_id = $db_engine->db_insert_id();
	                      # Update to reason table
	                      $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
	                      $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
	                      $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
	                                     VALUES ($fieldsvalues)";
	                      $db_engine->db_db_query($sql);
	
	                      if ($plugin['Discipline'])
	                      {
	                          # For Discipline System upgrade
	                          include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
	                          $ldiscipline = new libdiscipline();
	                          $ldiscipline->calculateUpgradeLateToDemerit($targetUserID);
	                          $ldiscipline->calculateUpgradeLateToDetention($targetUserID);
	                      }
	                  }
	                  else
	                  {}
	
	                  # Response
	                  echo mb_convert_encoding(output_env_str(CARD_RESP_PM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
	                  intranet_closedb();
	                  exit();
	
	             }
	             else
	             {
	                 # Mark Leave time, Leave Status as PM Early leave
	                 $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_PM."',
	                                LeaveSchoolStation = '$sitename', DateModified = now()
	                                WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
	                 $db_engine->db_db_query($sql);
	
	                 if ($directProfileInput)
	                 {
	                     # Add PM Early Leave to student profile
	                     $year = getCurrentAcademicYear();
	                     $semester = getCurrentSemester();
	                     $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
	                     $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
	                     $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
	                     $db_engine->db_db_query($sql);
	                     $insert_id = $db_engine->db_insert_id();
	                     # Update to reason table
	                     $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
	                     $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
	                     $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
	                                    VALUES ($fieldsvalues)";
	                     $db_engine->db_db_query($sql);
	                 }
	                 else
	                 {}
	
	                 if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
	                 {
	                         echo mb_convert_encoding(output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option"),"BIG5","UTF-8");
	                 }
	                 else
	                 {
	                         echo mb_convert_encoding(output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
	                 }
	                 intranet_closedb();
	                 exit();
	
	
	             }
	         }
	
	    }
	    else
	    {
	        # Normal School Leave
	        # Mark Leave Status, Time
	        send_sms(2, $dailylog_tablename);
	       $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_NORMAL."',
	                      LeaveSchoolStation = '$sitename', DateModified = now()
	                      WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
	       $db_engine->db_db_query($sql);
	
	       # Remove Early Leave Record (AM/PM)
	       $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
	                      WHERE RecordType = '".PROFILE_TYPE_EARLY."' AND UserID = '$targetUserID'
	                            AND AttendanceDate = '$today' AND (DayType = '".PROFILE_DAY_TYPE_PM."' OR DayType='".PROFILE_DAY_TYPE_AM."')";
	       $db_engine->db_db_query($sql);
	
	       # Remove Reason Record of Early Leave (AM/PM)
	       $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
	                      WHERE RecordDate = '$today' AND StudentID = '$targetUserID'
	                            AND RecordType = '".PROFILE_TYPE_EARLY."' AND (DayType = '".PROFILE_DAY_TYPE_PM."' OR DayType='".PROFILE_DAY_TYPE_AM."')";
	       $db_engine->db_db_query($sql);
	
	                           if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
	                           {
	                                   echo mb_convert_encoding(output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option"),"BIG5","UTF-8");
	                           }
	                           else
	                           {
	                                   echo mb_convert_encoding(output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg"),"BIG5","UTF-8");
	                           }
	       intranet_closedb();
	       exit();
	    }
	}
	else
	{
	   echo mb_convert_encoding(output_env_str(CARD_RESP_SYSTEM_NOT_INIT."###".$attend_lang['SystemNotInit']),"BIG5","UTF-8");
	   intranet_closedb();
	   exit();
	}
}
else     # Card Not Registered
{
  echo mb_convert_encoding(output_env_str(CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered']),"BIG5","UTF-8");
  intranet_closedb();
  exit();
}


intranet_closedb();

function checkTimeResult($ts_recordID,$ts_morningTime,$ts_lunchStart,$ts_lunchEnd,$ts_leaveSchool,$ts_nonSchoolDay)
{
	global $db_engine;
	
	$sql = "select sec_to_time(".$ts_morningTime."), sec_to_time(".$ts_lunchStart."), sec_to_time(".$ts_lunchEnd."), sec_to_time(".$ts_leaveSchool.")";
	$aaaa = $db_engine->returnArray($sql,4);
	list($bb, $cc, $dd, $ee) = $aaaa[0];
	echo "<br>ts_recordID---><b><font color = \"red\">".$ts_recordID."</font></b>";
	echo "<br>ts_morningTime--->".$ts_morningTime." <b><font color = \"red\">[".$bb."]</font></b>";
	echo "<br>ts_lunchStart--->".$ts_lunchStart."<b><font color = \"red\"> [".$cc."]</font></b>";
	echo "<br>ts_lunchEnd--->".$ts_lunchEnd." <b><font color = \"red\">[".$dd."]</font></b>";
	echo "<br>ts_leaveSchool--->".$ts_leaveSchool." <b><font color = \"red\">[".$ee."]</font></b>";
	echo "<br>ts_nonSchoolDay---><b><font color = \"red\">".$ts_nonSchoolDay."</font></b><br><br><br>";

}
?>