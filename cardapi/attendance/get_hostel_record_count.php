<?php

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");


intranet_opendb();

if(!$sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
	intranet_closedb();
	exit();
}



$lc = new libcardstudentattend2();

$libgrouping = new libgrouping();

$groups = $libgrouping->returnCategoryGroups($lc->HostelAttendanceGroupCategory);
$groups = Get_Array_By_Key($groups, 'GroupID');

if(!isset($ts) || $ts == '') {
	$TargetDate = date("Y-m-d");
	$ts = strtotime($TargetDate);
} else {
	$TargetDate = date("Y-m-d", $ts);
}

$year = date("Y", $ts);
$month = date("m", $ts);

$params = array('StartDate'=>$year.'-'.$month.'-01','EndDate'=>$TargetDate,'GroupID'=>$groups);
$attendance_records = $lc->getStudentLastHostelAttendanceRecords($year,$month,$params);


$month = $month - 1;
if($month == 0) {
	$month = 12;
	$year = $year - 1;
}
$EndDate = date("Y-m-t", strtotime($year.'-'.$month.'-01'));
$params = array('StartDate'=>date("Y-m-d", strtotime($year.'-'.$month.'-01')),'EndDate'=>$EndDate,'GroupID'=>$groups);
$previous_records = $lc->getStudentLastHostelAttendanceRecords($year,$month,$params);
$previous_records = BuildMultiKeyAssoc($previous_records, 'UserID');


$present_records = array();
$absent_records = array();
foreach($attendance_records as $temp) {
	if($temp['InStatus'] == '') {
		if(isset($previous_records[$temp['UserID']])) {
			$temp = $previous_records[$temp['UserID']];
		}
	}

	if($temp['InStatus'] == CARD_STATUS_PRESENT) {
		$present_records[] = $temp;
	} else {
		$absent_records[] = $temp;
	}
}


echo count($present_records).'###'.count($absent_records);

intranet_closedb();
