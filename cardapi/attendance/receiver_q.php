<?
####################################################################
# Created by : Kenneth Wong
# Creation Date : 20060518
# Customized version for QualiEd Collge
####################################################################
# Version updates
# 20060518 : Kenneth Wong
####################################################################
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();

include_once("attend_functions.php");
if ($dlang != "b5") $intranet_session_language = "en";
else $intranet_session_language = "b5";

include_once("attend_lang.php");
include_once("attend_const.php");


######################################################
# Param : (From QueryString)
#    CardID
######################################################

# $db_engine (from functions.php)

# Check IP
if (!isIPAllowed())
{
     echo CARD_RESP_INVALID_IP."###".$attend_lang['InvalidIP'];
     intranet_closedb();
     exit();
}

# 1. Get UserInfo
$namefield = getNameFieldByLang();
$sql = "SELECT UserID, $namefield, RecordType, ClassName, ClassNumber FROM INTRANET_USER WHERE CardID = '$CardID'";
$temp = $db_engine->returnArray($sql,5);
list($targetUserID , $targetName, $targetType, $targetClass, $targetClassNumber) = $temp[0];



# Check Card
if ($targetUserID == 0)
{
    # Card not registered
    echo CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered'];
    intranet_closedb();
    exit();
}
else if ($targetType == 1)    # Staff, redirection
{
     # Neglect Staff

}
else if ($targetType == 2)
{

     # Customization starts

     $null_record_variable = "NULL";
     # No special timetable, whole school the same

     # Get Record
     # Create Monthly data table
     $current_time = time();
     $month = date('m',$current_time);
     $year = date('Y',$current_time);
     $day = date('d',$current_time);
     $today = date('Y-m-d',$current_time);
     $time_string = date('H:i:s',$current_time);
     $ts_now = $current_time - strtotime($today);

     buildMonthTable($year, $month);

     # Get Record From table
     $dailylog_tablename = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
     $sql = "SELECT RecordID, TIME_TO_SEC(InSchoolTime),
                    IFNULL(AMStatus,'$null_record_variable'), TIME_TO_SEC(LunchOutTime), TIME_TO_SEC(LunchBackTime),
                    IFNULL(PMStatus,'$null_record_variable'), TIME_TO_SEC(LeaveSchoolTime), LeaveStatus, UNIX_TIMESTAMP(DateModified)
                    FROM $dailylog_tablename
                    WHERE UserID = '$targetUserID' AND DayNumber = '$day'";
     $temp = $db_engine->returnArray($sql,9);
     list($DayRecordID, $curr_InSchoolTime, $curr_AMStatus, $curr_LunchOutTime, $curr_LunchBackTime, $curr_PMStatus, $curr_LeaveSchoolTime, $curr_LeaveStatus, $curr_tslastMod) = $temp[0];

     # Change the name display
     $targetName = "$targetName ($targetClass-$targetClassNumber)";

     # Check Last Card Read Time for ignore period
     $is_first_record = false;
     if ($curr_tslastMod == "")
     {
         # Insert Record
         $sql = "INSERT INTO $dailylog_tablename (UserID, DayNumber, DateInput, DateModified)
                        VALUES ('$targetUserID','$day',now(),now())";
         $db_engine->db_db_query($sql);
         $curr_AMStatus = $null_record_variable;
         $curr_PMStatus = $null_record_variable;
         $is_first_record = true;

     }
     else
     {
         # Check whether within last n mins
         $ignore_period = get_file_content("$intranet_root/file/stattend_ignore.txt");
         $ignore_period += 0;
         if ($ignore_period > 0)
         {
             $ts_ignore = $ignore_period * 60;
             # Check whether it is w/i last n mins
             if ($current_time - $curr_tslastMod < $ts_ignore)
             {
                 echo CARD_RESP_IGNORE_WITHIN_PERIOD."###".$attend_lang['WithinIgnorePeriod'];
                 intranet_closedb();
                 exit();
             }
         }
     }

     # Only 1 time boundaries
     $sql = "SELECT SlotID, DayType, TIME_TO_SEC(SlotStart), TIME_TO_SEC(SlotEnd)
               FROM CARD_STUDENT_ATTENDANCE_CUSTOMIZED_1_TIMESLOT
               ORDER BY DayType, SlotID";
     $temp = $db_engine->returnArray($sql,4);

     for ($i=0; $i<sizeof($temp); $i++)
     {
          list ($t_slotID, $t_dayType, $t_start, $t_end) = $temp[$i];
          if ($t_dayType == 2) # AM
          {
              switch ($t_slotID)
              {
                      case 1: $time_slot_1 = array($t_start,$t_end); break;
                      case 2: $time_slot_2 = array($t_start,$t_end); break;
                      case 3: $time_slot_3 = array($t_start,$t_end); break;
                      case 4: $time_slot_4 = array($t_start,$t_end); break;
              }
          }
          else if ($t_dayType == 3) # PM
          {
              switch ($t_slotID)
              {
                      case 1: $time_slot_5 = array($t_start,$t_end); break;
                      case 2: $time_slot_6 = array($t_start,$t_end); break;
                      case 3: $time_slot_7 = array($t_start,$t_end); break;
                      case 4: $time_slot_8 = array($t_start,$t_end); break;
              }
          }
     }

     if (!is_array($time_slot_1) || !is_array($time_slot_2) ||!is_array($time_slot_3) ||
     !is_array($time_slot_4) ||!is_array($time_slot_5) ||!is_array($time_slot_6) ||
     !is_array($time_slot_7) ||!is_array($time_slot_8) )
     {
         echo CARD_RESP_SYSTEM_NOT_INIT."###".$attend_lang['SystemNotInit'];
         intranet_closedb();
         exit();
     }


     # Different action in different time zones

     if ($ts_now >= $time_slot_1[0] && $ts_now <= $time_slot_1[1])
     {
         # Zone 1
         # Mark AMStatus = Present, InSchoolTime = ts_now
                $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', AMStatus = '".CARD_STATUS_PRESENT."',
                                        InSchoolStation = '$sitename', DateModified = now()
                                   WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
                $db_engine->db_db_query($sql);
                echo CARD_RESP_AM_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string";
                intranet_closedb();
                exit();
     }
     else if ($ts_now >= $time_slot_2[0] && $ts_now <= $time_slot_2[1])
     {
         # Zone 2 (Late Type 1)
         # if AMStatus is NULL or Absent, AMStatus = Late, InSchoolTime = ts_now,
         #   insert profile record, reason record
         #   check 3 times penalty

         # Lesson Time and AMStatus is NULL or set Absent
         if ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT)
         {
              # Mark as Late
              # Mark AM Status, InSchool Time
              $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', AMStatus = '".CARD_STATUS_LATE."',
                             InSchoolStation = '$sitename', DateModified = now(),
                             LateType = 1
                             WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
              $db_engine->db_db_query($sql);

              # Remove Previous Absent Record
              $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                             WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$targetUserID'
                                   AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
              $db_engine->db_db_query($sql);

              # Add to student profile
              $year = getCurrentAcademicYear();
              $semester = getCurrentSemester();
              $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
              $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
              $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
              $db_engine->db_db_query($sql);
              $insert_id = $db_engine->db_insert_id();
              # Update to reason table
              $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
              $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
              $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                             VALUES ($fieldsvalues)";
              $db_engine->db_db_query($sql);

              if ($plugin['Discipline'])  # Check 3 times lates
              {
	              # Check 3 times
	              $sql = "SELECT RecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE StudentID = '$StudentID' AND DemeritWaived = 0 AND DemeritUpgrade = 0";
	            	$temp = $db_engine->returnVector($sql);
	            	$DemeritUpgrade = (sizeof($temp) == 3) ? 1 : 0;
	            	
	            	if ($DemeritUpgrade)
	            	{
		            	# Add Demerit Record
		            	$sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$StudentID'";
                  $temp = $db_engine->returnArray($sql,2);
                	list ($user_classname, $user_classnum) = $temp[0];
                	
                	# Insert Record to PROFILE_STUDENT_MERIT
                	$values = "now(),'$year','$semester','$StudentID', '".$_SESSION['UserID']."','1', '$user_classname', '$user_classnum', now(),now()";
                	$sql = "INSERT INTO PROFILE_STUDENT_MERIT (MeritDate,Year, Semester, UserID, PersonInCharge, RecordStatus, ClassName, ClassNumber, DateInput, DateModified) VALUES ($values)";
                  $db_engine->db_db_query($sql);
                  $profileRecordID = $db_engine->db_insert_id();
                  
                  # Update previous records in CARD_STUDENT_PROFILE_RECORD_REASON
                  $RecordIDs = implode(",", $temp[0]);
                  $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DemeritUpgrade = 1 WHERE RecordID IN ($RecordIDs)";
                  $db_engine->db_db_query($sql);
	            	}
	            	
	            	$sql = "SELECT COUNT(*) FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE StudentID = '$StudentID' AND DetentionWaived = 0 AND DetentionUpgrade <> 1";
	            	$temp = $db_engine->returnVector($sql);
	            	$DetentionUpgrade = (sizeof($temp) == 3) ? 1 : 0;
	            	
	            	if ($DetentionUpgrade)
	            	{
		            	$fields = "StudentID, RecordDate, PICID, Minutes";
		            	$values = "'$StudentID', now(), '".$_SESSION['UserID']."', 30";
		            	
		            	$sql = "INSERT INTO DISCIPLINE_DETENTION_RECORD ($fields) VALUES ($values)";
		            	$db_engine->db_db_query($sql);
		            	$DetentionID = $db_engine->db_insert_id();
		            	
		            	# Update the UpgradedToDetentionID field in PROFILE_STUDENT_ATTENDANCE
		            	$sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET UpgradedToDetentionID = '$DetentionID' WHERE StudentAttendanceID = '$attendance_id'";
                  $db_engine->db_db_query($sql);
                  
                  # Update DetentionRecordID in CARD_STUDENT_PROFILE_RECORD_REASON
                  $RecordIDs = implode(",", $temp[0]);
                  $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DetentionRecordID = '$DetentionID' WHERE RecordID IN ($RecordIDs)";
                  $db_engine->db_db_query($sql);
	            	}
	            	
              }

              echo CARD_RESP_AM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string";
         }
         else # if AMStatus is Present/Late, skip this
         {
                 # Skip
                 echo CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent'];

         }
         intranet_closedb();
         exit();
     }
     else if ($ts_now >= $time_slot_3[0] && $ts_now <= $time_slot_3[1])
     {
         # Zone 3 (Late Type 2)
         # if AMStatus is NULL or Absent, AMStatus = Late, InSchoolTime = ts_now,
         #   insert profile record, reason record
         #   add penalty
         # Lesson Time and AMStatus is NULL or set Absent
         if ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT)
         {
              # Mark as Late
              # Mark AM Status, InSchool Time
              $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', AMStatus = '".CARD_STATUS_LATE."',
                             InSchoolStation = '$sitename', DateModified = now(),
                             LateType = 2
                             WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
              $db_engine->db_db_query($sql);

              # Remove Previous Absent Record
              $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                             WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$targetUserID'
                                   AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
              $db_engine->db_db_query($sql);

              # Add to student profile
              $year = getCurrentAcademicYear();
              $semester = getCurrentSemester();
              $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
              $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
              $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
              $db_engine->db_db_query($sql);
              $insert_id = $db_engine->db_insert_id();
              # Update to reason table
              $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
              $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
              $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                             VALUES ($fieldsvalues)";
              $db_engine->db_db_query($sql);

              if ($plugin['Discipline'])  # add penalty
              {
	              # Add Demerit Record
	              $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$StudentID'";
	              $temp = $db_engine->returnArray($sql,2);
	              list ($user_classname, $user_classnum) = $temp[0];
	              
	              # Insert record to PROFILE_STUDENT_MERIT
	              $fields = "MeritDate, Year, Semester, UserID, PersonInCharge, RecordStauts, ClassName, ClassNumber, DateInput, DateModified";
	              $values = "now(), '$year', '$semester', '".$_SESSION['UserID']."', 1, '$user_classname', '$user_classnum', now(), now()";
	              $sql = "INSERT INTO PROFILE_STUDENT_MERIT ($fields) VALUES ($values)";
	              $db_engine->db_db_query($sql);
	              $profileRecordID = $lcardattend->db_insert_id();
	              
	              # Add Detention Record
	              $fields = "StudentID, RecordDate, PICID, Minutes";
	              $values = "'$StudentID', now(), '".$_SESSION['UserID']."', 30";
	              
	              $sql = "INSERT INTO DISCIPLINE_DETENTION_RECORD ($fields) VALUES ($values)";
	              $db_engine->db_db_query($sql);
	              $DetentionID = $db_engine->db_insert_id();
	              
	              # Update UpgradedToDetentionID in PROFILE_STUDENT_ATTENDANCE
	              $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET UpgradedToDetentionID = '$DetentionID' WHERE StudentAttendanceID = '$attendance_id'";
	              $db_engine->db_db_query($sql);	              
              }

              echo CARD_RESP_AM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string";
         }
         else # if AMStatus is Present/Late, skip this
         {
                 # Skip
                 echo CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent'];

         }
         intranet_closedb();
         exit();

     }
     else if ($ts_now >= $time_slot_4[0] && $ts_now <= $time_slot_4[1])
     {
         # Zone 4
         # if AMStatus is NULL or Absent, AMStatus = Absent, InSchoolTime = ts_now
         #    insert profile record, reason record
         if ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT)
         {
              # Mark as Absent
              # Mark AM Status, InSchool Time
              $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', AMStatus = '".CARD_STATUS_ABSENT."',
                             InSchoolStation = '$sitename', DateModified = now()
                             WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
              $db_engine->db_db_query($sql);


              # Add to student profile
              $year = getCurrentAcademicYear();
              $semester = getCurrentSemester();
              $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
              $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_ABSENT."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
              $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
              $db_engine->db_db_query($sql);
              $insert_id = $db_engine->db_insert_id();
              # Update to reason table
              $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
              $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_ABSENT."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
              $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                             VALUES ($fieldsvalues)";
              $db_engine->db_db_query($sql);
         }
         else  # if AMStatus is Present/Late, skip this
         {
                 # Skip
                 echo CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent'];
         }
         intranet_closedb();
         exit();


     }
     else if ($ts_now >= $time_slot_5[0] && $ts_now <= $time_slot_5[1])
     {
         # Zone 5
         # if AMStatus is NULL or Absent and InSchoolTime is NULL, InSchoolTime = ts_now
         # set PMStatus = Present, LunchBackTime = ts_now
         if ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT)
         {
             $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', InSchoolStation = '$sitename'
                            WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
             $db_engine->db_db_query($sql);
         }

         $sql = "UPDATE $dailylog_tablename SET LunchBackTime = '$time_string', PMStatus = '".CARD_STATUS_PRESENT."',
                        DateModified = now()
                        WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
         $db_engine->db_db_query($sql);
         echo CARD_RESP_PM_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string";
         intranet_closedb();
         exit();
     }
     else if ($ts_now >= $time_slot_6[0] && $ts_now <= $time_slot_6[1])
     {
         # Zone 6 (Late Type 1)
         # if AMStatus is NULL or Absent and InSchoolTime is NULL, InSchoolTime = ts_now
         if ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT)
         {
             $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', InSchoolStation = '$sitename'
                            WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
             $db_engine->db_db_query($sql);
         }

         # if PMStatus is NULL or Absent, PMStatus = Late, LunchBackTime = ts_now,
         #   insert profile record, reason record
         #   check 3 times penalty
         if ($curr_PMStatus == $null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT)
         {
              # Mark as Late
              # Mark PM Status, LunchBack Time
              $sql = "UPDATE $dailylog_tablename SET LunchBackTime = '$time_string', PMStatus = '".CARD_STATUS_LATE."',
                             LunchBackStation = '$sitename', DateModified = now(),
                             LateType = 1
                             WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
              $db_engine->db_db_query($sql);

              # Remove Previous Absent Record
              $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                             WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$targetUserID'
                                   AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
              $db_engine->db_db_query($sql);

              # Add to student profile
              $year = getCurrentAcademicYear();
              $semester = getCurrentSemester();
              $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
              $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
              $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
              $db_engine->db_db_query($sql);
              $insert_id = $db_engine->db_insert_id();
              # Update to reason table
              $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
              $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
              $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                             VALUES ($fieldsvalues)";
              $db_engine->db_db_query($sql);

              if ($plugin['Discipline'])  # Check 3 times lates
              {
	              $sql = "SELECT RecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE StudentID = '$StudentID' AND DemeritWaived = 0 AND DemeritUpgrade = 0";
	            	$temp = $db_engine->returnVector($sql);
	            	$DemeritUpgrade = (sizeof($temp) == 3) ? 1 : 0;
	              
	              if ($DemeritUpgrade)
	              {
		              # Add Demerit Record
		              $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$StudentID'";
					    		$user_array = $db_engine->returnArray($sql, 2);
					    		list ($user_classname, $user_classnum) = $user_array[0];
					    		
					    		# Insert Record to PROFILE_STUDENT_MERIT
					    		$values = "now(),'$year','$semester','$StudentID', '".$_SESSION['UserID']."','1', '$user_classname', '$user_classnum', now(),now()";
                	$sql = "INSERT INTO PROFILE_STUDENT_MERIT (MeritDate,Year, Semester, UserID, PersonInCharge, RecordStatus, ClassName, ClassNumber, DateInput, DateModified) VALUES ($values)";
                  $db_engine->db_db_query($sql);
                  $profileRecordID = $db_engine->db_insert_id();
                  
                  # Update previous records in CARD_STUDENT_PROFILE_RECORD_REASON
		            	$RecordIDs = implode(",", $temp[0]);
		            	$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DemeritUpgrade = 1 WHERE RecordID IN ($RecordIDs)";
		            	$db_engine->db_db_query($sql);							    
	              }
	              
	              # Detention Record
	              $sql = "SELECT COUNT(*) FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE StudentID = '$StudentID' AND DetentionWaived = 0 AND DetentionUpgrade <> 1";
						    $temp = $db_engine->returnVector($sql);
						    $DetentionUpgrade = (sizeof($temp) == 3) ? 1 : 0;
						    
						    if ($DetentionUpgrade)
						    {
							    $fields = "StudentID, RecordDate, PICID, Minutes";
							    $values = "'$StudentID', now(), '".$_SESSION['UserID']."', 30";	
							    $sql = "INSERT INTO DISCIPLINE_DETENTION_RECORD ($fields) VALUES ($values)";
							    $db_engine->db_db_query($sql);
							    $DetentionID = $db_engine->db_insert_id();
							    
							    # Update UpgradedToDetention
							    $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET UpgradedToDententionID = '$DetentionID' WHERE StudentAttendanceID = '$attendance_id'";
							    $db_engine->db_db_query($sql);
							    
							    # Update DetentionRecordID AND DetentionUpgrade
							    $RecordIDs = implode(",", $temp);
							    $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DetentionRecordID = '$DetentionID' AND DetentionUpgrade = 1 WHERE RecordID IN ($RecordIDs)";
							    $db_engine->db_db_query($sql);
						    }
              }

              echo CARD_RESP_PM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string";
         }
         else # if PMStatus is Present/Late, skip this
         {
                 # Skip
                 echo CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent'];

         }
         intranet_closedb();
         exit();
     }
     else if ($ts_now >= $time_slot_7[0] && $ts_now <= $time_slot_7[1])
     {
         # Zone 7 (Late Type 2)
         # if AMStatus is NULL or Absent and InSchoolTime is NULL, InSchoolTime = ts_now
         if ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT)
         {
             $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', InSchoolStation = '$sitename'
                            WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
             $db_engine->db_db_query($sql);
         }

         # if PMStatus is NULL or Absent, PMStatus = Late, LunchBackTime = ts_now,
         #   insert profile record, reason record
         #   check 3 times penalty
         if ($curr_PMStatus == $null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT)
         {
              # Mark as Late
              # Mark PM Status, LunchBack Time
              $sql = "UPDATE $dailylog_tablename SET LunchBackTime = '$time_string', PMStatus = '".CARD_STATUS_LATE."',
                             LunchBackStation = '$sitename', DateModified = now(),
                             LateType = 2
                             WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
              $db_engine->db_db_query($sql);

              # Remove Previous Absent Record
              $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                             WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$targetUserID'
                                   AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
              $db_engine->db_db_query($sql);

              # Add to student profile
              $year = getCurrentAcademicYear();
              $semester = getCurrentSemester();
              $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
              $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
              $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
              $db_engine->db_db_query($sql);
              $insert_id = $db_engine->db_insert_id();
              # Update to reason table
              $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
              $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
              $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                             VALUES ($fieldsvalues)";
              $db_engine->db_db_query($sql);

              if ($plugin['Discipline'])  # add penalty
              {
	              # Add Demerit Record
	              $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$StudentID'";
                $temp = $db_engine->returnArray($sql,2);
            		list ($user_classname, $user_classnum) = $temp[0];	              
            		
            		# Insert record to PROFILE_STUDENT_MERIT
            		$fields = "MeritDate, Year, Semester, UserID, PersonInCharge, RecordStauts, ClassName, ClassNumber, DateInput, DateModified";
		            $values = "now(), '$year', '$semester', '".$_SESSION['UserID']."', 1, '$user_classname', '$user_classnum', now(), now()";
		            $sql = "INSERT INTO PROFILE_STUDENT_MERIT ($fields) VALUES ($values)";
		            $db_engine->db_db_query($sql);
		            $profileRecordID = $db_engine->db_insert_id();
		            
		            # Add Detention Record
		            $fields = "StudentID, RecordDate, PICID, Minutes";
		            $values = "'$StudentID', now(), '".$_SESSION['UserID']."', 30";
		            
		            $sql = "INSERT INTO DISCIPLINE_DETENTION_RECORD ($fields) VALUES ($values)";
		            $db_engine->db_db_query($sql);
		            $DetentionID = $db_engine->db_insert_id();
		            
		            # Update UpgradedToDetentionID in PROFILE_STUDENT_ATTENDANCE
		            $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET UpgradedToDetentionID = '$DetentionID' WHERE StudentAttendanceID = '$attendance_id'";
		            $db_engine->db_db_query($sql);
              }

              echo CARD_RESP_PM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string";
         }
         else # if PMStatus is Present/Late, skip this
         {
                 # Skip
                 echo CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent'];

         }
         intranet_closedb();
         exit();

     }
     else if ($ts_now >= $time_slot_8[0] && $ts_now <= $time_slot_8[1])
     {
         # Zone 8
         # if AMStatus is NULL or Absent and InSchoolTime is NULL, InSchoolTime = ts_now
         if ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT)
         {
             $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', InSchoolStation = '$sitename'
                            WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
             $db_engine->db_db_query($sql);
         }

         # if PMStatus is NULL or Absent, PMStatus = Absent, LunchBackTime = ts_now
         #    insert profile record, reason record
         if ($curr_PMStatus == $null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT)
         {
              # Mark as Absent
              # Mark PM Status, InSchool Time
              $sql = "UPDATE $dailylog_tablename SET LunchBackTime = '$time_string', PMStatus = '".CARD_STATUS_ABSENT."',
                             LunchBackStation = '$sitename', DateModified = now()
                             WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
              $db_engine->db_db_query($sql);


              # Add to student profile
              $year = getCurrentAcademicYear();
              $semester = getCurrentSemester();
              $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
              $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_ABSENT."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
              $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
              $db_engine->db_db_query($sql);
              $insert_id = $db_engine->db_insert_id();
              # Update to reason table
              $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
              $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_ABSENT."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
              $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                             VALUES ($fieldsvalues)";
              $db_engine->db_db_query($sql);
         }
         else  # if PMStatus is Present/Late, skip this
         {
                 # Skip
                 echo CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent'];
         }
         intranet_closedb();
         exit();
     }


#########################################


}
else     # Card Not Registered
{
    echo CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered'];
    intranet_closedb();
    exit();
}




intranet_closedb();
?>

