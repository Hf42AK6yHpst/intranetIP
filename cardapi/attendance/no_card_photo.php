<?php
/*
 * 2019-06-03 (Carlos): Apply checkValidAccess() to verify valid access.
 * 2017-05-17 (Carlos): Created for receiving no bring card student manual input taken photo.
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_opendb();

include_once("attend_functions.php");

$json = new JSON_obj();

header("Content-Type: application/json;charset=utf-8");

$responseAry = array();
# check flag, check IP
if (!$sys_custom['StudentAttendance']['NoCardPhoto'] || !isIPAllowed('StudentAttendance') || !checkValidAccess())
{
	//header('HTTP/1.0 401 Unauthorized', true, 401);
	$responseAry = array('Error'=>'401 Unauthorized access.');
}

if(strtoupper($_SERVER['REQUEST_METHOD']) != 'POST')
{
	$responseAry = array('Error'=>'Invalid usage. Must use POST method.');
}

//debug_pr($_POST);
//debug_pr($_FILES);
if($_POST['CardID']=='' || count($_FILES)==0)
{
	$responseAry = array('Error'=>'Invalid parameters.');
}

if(count($responseAry)>0)
{
	intranet_closedb();
	$output = $json->encode($responseAry);
	echo $output;
    exit();
}

$lf = new libfilesystem();
$lc = new libcardstudentattend2();

function getNextFreeFolder($base_dir,$return_full_path=false)
{	
	$LIMIT = 30000;
	//$LIMIT = 5; // test value
	$absolute_target_folder = $base_dir;
	
	$subfolder_number = 0;
	$file_count = 0;
	$folder_to_find = $absolute_target_folder."/".$subfolder_number;
	$rel_folder_to_find = $subfolder_number;
	
	if(!is_dir($folder_to_find)){
		mkdir($folder_to_find,0777);
	}
	$number_of_attempt = 0;
	do
	{
		$number_of_attempt+=1;
		$cmd = "find $folder_to_find -maxdepth 1 | wc -l";
		$file_count = intval(shell_exec($cmd));
		if($file_count >= $LIMIT)
		{
			$subfolder_number += 1;
			$folder_to_find = $absolute_target_folder."/".$subfolder_number;
			$rel_folder_to_find = $subfolder_number;
			if(!is_dir($folder_to_find)){
				mkdir($folder_to_find,0777);
			}
		}else{
			break;
		}
		if($number_of_attempt > $LIMIT) break; // avoid forever loop
	}while($file_count >= $LIMIT);
	
	if($return_full_path){
		return $folder_to_find;
	}else{
		return $rel_folder_to_find;
	}
}

$CardID = trim($_POST['CardID']);
$SiteName = trim($_POST['SiteName']);

$sql = "SELECT UserID,RecordStatus,RecordType FROM INTRANET_USER WHERE CardID='".$lc->Get_Safe_Sql_Query($CardID)."' ";
$student_records = $lc->returnResultSet($sql);

if(count($student_records)==0){
	$responseAry = array('Error'=>'User with card number '.$CardID.' does not exist.');
	intranet_closedb();
	$output = $json->encode($responseAry);
	echo $output;
    exit();
}

$user_id = $student_records[0]['UserID'];
$user_status = $student_records[0]['RecordStatus'];
$user_type = $student_records[0]['RecordType'];

if($user_status != 1){
	$responseAry = array('Error'=>'User with card number '.$CardID.' is not an active user.');
}

if($user_type != USERTYPE_STUDENT){
	$responseAry = array('Error'=>'User with card number '.$CardID.' is not a student user.');
}

if(count($responseAry)>0)
{
	intranet_closedb();
	$output = $json->encode($responseAry);
	echo $output;
    exit();
}

$date = date("Y-m-d");
$cur_datetime = date("Y-m-d H:i:s");
$file_field_name = 'PhotoFile';

$target_dir = $file_path.'/file/student_attendance';
if(!file_exists($target_dir) || !is_dir($target_dir)){
	$lf->folder_new($target_dir);
}else{
	chmod($target_dir,0777);
}

$target_dir .= '/no_card_photo';
if(!file_exists($target_dir) || !is_dir($target_dir)){
	$lf->folder_new($target_dir);
}else{
	chmod($target_dir,0777);
}

$subfolder = getNextFreeFolder($target_dir);
$target_dir .= '/'.$subfolder;

$base_name = $lf->get_file_basename($_FILES[$file_field_name]["name"]);
$save_photo_path = $subfolder.'/'.$base_name;
$target_path = $target_dir.'/'.$base_name;

$copy_success = move_uploaded_file($_FILES[$file_field_name]["tmp_name"], $target_path);
$insert_success = false;
if($copy_success)
{
	$sql = "INSERT INTO CARD_STUDENT_NO_CARD_PHOTO (RecordDate,StudentID,PhotoPath,TakePhotoTime,SiteName) VALUES ('$date','$user_id','$save_photo_path','$cur_datetime','".$lc->Get_Safe_Sql_Query($SiteName)."')";
	$insert_success = $lc->db_db_query($sql);
}

$responseAry = array('Result'=>($copy_success && $insert_success)?1:0);
$output = $json->encode($responseAry);
intranet_closedb();
echo $output;
exit;
?>