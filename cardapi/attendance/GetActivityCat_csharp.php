<?
// using by 
####################################################################
# Created by : Kenneth Wong
# Creation Date : 20051207
####################################################################
# Version updates
# 20051207: Kenneth Wong
# 20061108: Kenneth Wong
#           Check whether last modified is card read or confirmation
# 20070503: Kenneth Wong
#           Add checking of "0" and "" in InSchoolTime to prevent accidentally 00:00:00 record written
# 20070503: Peter Ho
#			Staff attendance - Add removing the previous records when the new status and old status are not the same
# 20140717:	Bill
#			For eEnrolment IP Checking
####################################################################
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_opendb();
/*
include_once("attend_functions.php");
if ($dlang != "b5") $intranet_session_language = "en";
else $intranet_session_language = "b5";

include_once("attend_lang.php");
include_once("attend_const.php");
*/
include_once("attend_functions.php");
if ($dlang != "b5" && $dlang!= "gb") $intranet_session_language = "en";
else $intranet_session_language = $dlang;

include_once("attend_lang.".$intranet_session_language.".php");
include_once("attend_const.php");

header("Content-type: text/plain; charset=utf-8");
######################################################
# Param : (From QueryString)
#    username, passwd
######################################################

# $db_engine (from functions.php)

# Check IP
if (!isIPAllowed('eEnrolment'))
{
     echo CARD_RESP_INVALID_IP."###".$attend_lang['InvalidIP'];
     intranet_closedb();
     exit();
}

######################################################
#
# get list of categroy
#
######################################################

$le = new libclubsenrol();

$Sql = "
			SELECT
						CategoryID, CategoryName
			FROM
						INTRANET_ENROL_CATEGORY
			ORDER BY
						CategoryID
		";
			
$ReturnArr = $db_engine->returnArray($Sql, 2);

for ($i = 0; $i < sizeof($ReturnArr); $i++) {
	//$ReturnStr .= $ReturnArr[$i][0]."###".conv_utf8_string($ReturnArr[$i][1])."\n";
	$ReturnStr .= $ReturnArr[$i][0]."###".($ReturnArr[$i][1])."\n";
}

echo $ReturnStr;
intranet_closedb();
exit();

?>

