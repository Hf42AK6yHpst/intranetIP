<?php
// Editing by 
/*
 * @Usage: for eAttendance client program to pre-fetch users attend school times.
 * 2019-06-03 (Carlos): Apply checkValidAccess() to verify valid access.
 * 2015-12-09 (Carlos): Cater Staff Attendance v2 time table, do not call v3's get duty function, it would generate wrong v2 duty records. 
 */
set_time_limit(0); 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_api.php");

intranet_opendb();

include_once("attend_functions.php");
if ($dlang != "b5" && $dlang!= "gb") $intranet_session_language = "en";
else $intranet_session_language = $dlang;

include_once("attend_lang.".$intranet_session_language.".php");
include_once("attend_const.php");

header("Content-type: text/plain; charset=utf-8");

if(!checkValidAccess()){ // defined in attend_functions.php
	intranet_closedb();
	exit();
}

$GeneralSetting = new libgeneralsettings();
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance');
$StudentAttendanceAllowed = isIPAllowed('StudentAttendance');

$Settings = $GeneralSetting->Get_General_Setting('StaffAttendance');
$StaffAttendanceAllowed = isIPAllowed('StaffAttendance');

# Check IP
if($StudentAttendanceAllowed || $StaffAttendanceAllowed)
{
	// has either one module is ok
}else
{
     //echo output_env_str(CARD_RESP_INVALID_IP."###".$attend_lang['InvalidIP']);
     intranet_closedb();
     exit();
}

$today = date("Y-m-d");
$weekday = date("w");
$year = date("Y");
$month = date("m");
$day = date("d");

$lc = new libcardstudentattend2();
$ls = new libstaffattend3_api();

$attendance_mode = $lc->attendance_mode;

$record_type = array();
if($plugin['attendancestudent'])
{
	$record_type[] = USERTYPE_STUDENT;
}
if($plugin['attendancestaff'])
{
	$record_type[] = USERTYPE_STAFF;
}

if(count($record_type)==0)
{
	intranet_closedb();
	exit();
}

// Get all active staff and students
$sql = "SELECT RecordType,UserID,UserLogin,CardID FROM INTRANET_USER WHERE RecordStatus='1' AND RecordType IN ('".implode("','",$record_type)."') AND TRIM(CardID)!='' ORDER BY RecordType";
$userIdAry = $lc->returnResultSet($sql);
$user_count = count($userIdAry);

if($plugin['attendancestaff'] && $module_version['StaffAttendance'] == 2.0)
{
	$name_field = getNameFieldByLang2('b.');
	// get user group info for Staff Attendance v2
	$sql = "SELECT UserID,GroupID FROM CARD_STAFF_ATTENDANCE_USERGROUP";
	$user_groups = $ls->returnResultSet($sql);
	$user_groups_count = count($user_groups);
	$userIdToGroupIdAry = array();
	for($i=0;$i<$user_groups_count;$i++){
		$userIdToGroupIdAry[$user_groups[$i]['UserID']] = $user_groups[$i]['GroupID'];
	}
}

$x = '';
$col_delim = "\t";
$row_delim = "";
$time_delim = ",";

for($i=0;$i<$user_count;$i++){
	if($userIdAry[$i]['RecordType'] == USERTYPE_STUDENT)
	{ 
		$time_setting = $lc->Get_Student_Attend_Time_Setting($today,$userIdAry[$i]['UserID']);
		
		if($time_setting != "NonSchoolDay")
		{
			$x .= $row_delim.$userIdAry[$i]['RecordType'].$col_delim.$userIdAry[$i]['UserID'].$col_delim.$userIdAry[$i]['UserLogin'].$col_delim.$userIdAry[$i]['CardID'];
			if($attendance_mode == 0) // AM
			{
				$x .= $col_delim.$time_setting['MorningTime'].$time_delim.$time_setting['LeaveSchoolTime'];
			}else if($attendance_mode == 1){ // PM
				$x .= $col_delim.$time_setting['LunchEnd'].$time_delim.$time_setting['LeaveSchoolTime'];
			}else{ // WD
				$x .= $col_delim.$time_setting['MorningTime'].$time_delim.$time_setting['LunchStart'].$time_delim.$time_setting['LunchEnd'].$time_delim.$time_setting['LeaveSchoolTime'];
			}
			$row_delim = "\n";
		}
	}else if($userIdAry[$i]['RecordType'] == USERTYPE_STAFF){
		if($module_version['StaffAttendance'] == 1.0)
		{
			// Not applicable to Staff Attendance v1.0
		}else if($module_version['StaffAttendance'] == 2.0)
		{
			if(isset($userIdToGroupIdAry[$userIdAry[$i]['UserID']]))
			{
				$duty = $ls->Get_Attendenance_Data($name_field, $userIdToGroupIdAry[$userIdAry[$i]['UserID']], $today, $weekday, $year, $month, $day);
				if(isset($duty[$userIdAry[$i]['UserID']]) && $duty[$userIdAry[$i]['UserID']]["DutyStart"]!='' && $duty[$userIdAry[$i]['UserID']]["DutyEnd"]!='')
				{
					$x .= $row_delim.$userIdAry[$i]['RecordType'].$col_delim.$userIdAry[$i]['UserID'].$col_delim.$userIdAry[$i]['UserLogin'].$col_delim.$userIdAry[$i]['CardID'].$col_delim;
					$x .= $duty[$userIdAry[$i]['UserID']]["DutyStart"].$time_delim.$duty[$userIdAry[$i]['UserID']]["DutyEnd"];
					$row_delim = "\n";
				}
			}
		}else if($module_version['StaffAttendance'] >= 3.0)
		{
			$duty = $ls->Get_Duty_By_Date($userIdAry[$i]['UserID'],$today); 
			
			if(count($duty["CardUserToDuty"][$userIdAry[$i]['UserID']])>0){
				$slots = $duty["CardUserToDuty"][$userIdAry[$i]['UserID']];
				$slot_count = count($slots);
				$x .= $row_delim.$userIdAry[$i]['RecordType'].$col_delim.$userIdAry[$i]['UserID'].$col_delim.$userIdAry[$i]['UserLogin'].$col_delim.$userIdAry[$i]['CardID'].$col_delim;
				$tmp_time_delim = "";
				for($j=0;$j<$slot_count;$j++){
					$x .= $tmp_time_delim.$slots[$j]['DutyStart'].$time_delim.$slots[$j]['DutyEnd'];
					$tmp_time_delim = $time_delim;
				}
				$row_delim = "\n";
			}
		}
	}
}

echo $x; 

intranet_closedb();
?>