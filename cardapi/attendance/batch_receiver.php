<?php
// Editing by 
/*
 * 2019-06-03 (Carlos): added parameters AccessTime and AccessCode to request url.
 * 2019-01-11 (Carlos): Cater https.
 * 2014-10-15 (Carlos): added flag $sys_custom['StudentAttendance']['BatchReceiverAPI'] to control the availability of this script
 * 2014-06-20 (Carlos): Created for batch processing tap cards
 * @param array CardID : array of SmartCardID
 * @param array Timestamp : array of tap card timestamps
 * @param array SiteName : array of tap card locations
 * @param string lang : which language for displaying output, accept "en" or "b5" or "gb"
 * @param bool GetResponseDetail : 1 to output detailed response separated with <br />, otherwise output is separated with comma
 * @return string of response : If GetResponseDetail!=1, return comma separated codes of 1 or 0, "1" - success; "0" - failure. 
 * 								If GetResponseDetail==1, detailed messages are separated with <br />
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
if (!in_array($lang,array("b5","gb"))) $intranet_session_language = "en";
else $intranet_session_language = $lang;

include_once("attend_lang.".$intranet_session_language.".php");
include_once("attend_const.php");

intranet_opendb();

$CardID = $_REQUEST['CardID']; // array
$Timestamp = $_REQUEST['Timestamp']; // array
$SiteName = $_REQUEST['SiteName']; // optional array
$FromClientProgram = $_REQUEST['FromClientProgram'];
$GetResponseDetail = $_REQUEST['GetResponseDetail']; // 1 will output response in detail, code + response message

$record_count = count($CardID);

if(($FromClientProgram!=1 && !$sys_custom['StudentAttendance']['BatchReceiverAPI']) || $record_count == 0){
	intranet_closedb();
	exit;
}

$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$url = (checkHttpsWebProtocol()?"https":"http")."://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"")."/cardapi/attendance/receiver_csharp.php";

$result = array();

$failure_codes = array("0","14","15","16","17","19"); // see attend_const.php

for($i=0;$i<$record_count;$i++)
{
	$ts = trim($Timestamp[$i]);
	
	$site_name = $SiteName[$i];
	$card_id = trim($CardID[$i]);
	if($card_id == "" || $ts == ""){
		$response = CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered'];
	}else{
		$external_date = date("Y-m-d",$ts);
		$external_time = date("H:i:s",$ts);
		$access_time = time();
		$access_code = generateCardApiAccessCode($access_time);
		$tapcard_url = $url."?CardID=".$card_id."&sitename=".urlencode($site_name)."&external_date=".$external_date."&external_time=".$external_time."&dlang=".$intranet_session_language."&AccessTime=".$access_time."&AccessCode=".$access_code;
		curl_setopt($ch, CURLOPT_URL, $tapcard_url);
		$response = curl_exec($ch);
		usleep(1);
	}
	
	if($GetResponseDetail == 1){
		$result[] = $response;
	}else{
		$response_code = substr($response,0,strpos($response,"###",0));
		$result[] = in_array($response_code,$failure_codes)? "0" : "1";
	}
}

curl_close($ch);

header("Content-type: text/plain; charset=utf-8");
if($GetResponseDetail == 1){
	echo implode("<br />",$result);
}else{
	echo implode(",",$result);
}

intranet_closedb();
?>