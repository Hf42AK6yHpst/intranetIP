<?php
// editing by 
##
# 20190603 (Carlos) : added checkValidAccess() for Card APIs to verify valid access. (Flag: $sys_custom['CardApiUseSecurityCode'])
#
# 20160830 (Carlos) : modified buildEntryLogMonthTable(), added RecordStation .
#
# 20070711 (Kenneth Wong):
# add function buildStaffRawLogMonthTable()
#
# 20070822 (Peter Ho):
# add function createEntry_Card_Student_Record_Storage()

$clientIP = getRemoteIpAddress();
$db_engine = new libdb();
#$dlang = "en";


function isIPAllowed($Module="StudentAttendance")
{
         global $clientIP;
         global $intranet_root;
         global $dlang, $Settings;

         # Check IP
         //$ips = get_file_content("$intranet_root/file/stattend_ip.txt");
         if (is_array($Settings))
         	$ips = $Settings['TerminalIP'];
         else {
         	include_once("../../includes/libgeneralsettings.php");
         	
					$GeneralSetting = new libgeneralsettings();
					$Settings = $GeneralSetting->Get_General_Setting($Module,array("'TerminalIP'"));
         	$ips = $Settings['TerminalIP'];
        }
         $ipa = explode("\n", $ips);
         $ip_ok = false;
         foreach ($ipa AS $key => $ip)
         {
              $ip = trim($ip);
              if ($ip == "0.0.0.0")
              {
                  $ip_ok = true;
              }
              else if (testip($ip,$clientIP))
              {
                   $ip_ok = true;
              }
         }
         if (!$ip_ok)
         {
              return false;
         }
         return true;
}

# Skip this function first
function checkSession($key)
{
         global $attendst_keyfile_temp;
         global $clientIP, $TerminalID;
         global $dlang;
         global $intranet_root,$attendst_expiry;

         # Check session key
         $keyfilepath = "$attendst_keyfile_temp/attendst_ip_$clientIP"."_t$TerminalID";
         if (!is_file($keyfilepath))
         {
              $msg = ($dlang=="b5"?"作業過時, 請重新開啟本程式":"Registration Expired. Pls restart the terminal program.");
              echo $msg;
              return false;
         }
         include_once("../../includes/libgeneralsettings.php");
         	
					$GeneralSetting = new libgeneralsettings();
					$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',array("'ConnectionTimeout'"));
         $temp = $Settings['ConnectionTimeout'];
         if ($temp!="" && is_numeric($temp))
         {
             $attendst_expiry = $temp;
         }
         $content = get_file_content($keyfilepath);
         $now = time();
         $data = explode("\n",$content);
         list ($ts,$oldkey) = $data;

         if ($oldkey != $key ||  ($now - $ts)> $attendst_expiry*60 )
         {
              $msg = ($dlang=="b5"?"作業過時, 請重新開啟本程式":"Registration Expired. Pls restart the terminal program.");
              echo $msg;
              return false;
         }
         return true;
}

function buildMonthTable($year, $month)
{
         global $db_engine;
         global $sys_custom;
         $tablename = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
         $sql = "CREATE TABLE IF NOT EXISTS $tablename (
          RecordID int(11) NOT NULL auto_increment,
          UserID int(11) NOT NULL,
          DayNumber int(11) NOT NULL,
          InSchoolTime time,
          InSchoolStation varchar(255),
          AMStatus int,
          LunchOutTime time,
          LunchOutStation varchar(255),
          LunchBackTime time,
          LunchBackStation varchar(255),
          PMStatus int,
          LeaveSchoolTime time,
          LeaveSchoolStation varchar(255),
          LeaveStatus int,
          ConfirmedUserID int,
          IsConfirmed int,
          PMConfirmedUserID int(11) default NULL,
        	PMIsConfirmed int default NULL,
          RecordType int,
          RecordStatus int,
          DateInput datetime,
          InputBy int(11) default NULL,
          DateModified datetime,
          ModifyBy int(11) default NULL,
          LastTapCardTime time default NULL,
		  PMDateModified datetime,
		  PMModifyBy int(11),
          PRIMARY KEY (RecordID),
          UNIQUE UserDay (UserID, DayNumber)
         ) ENGINE=InnoDB charset=utf8";
        $db_engine->db_db_query($sql);
       
}

function buildEntryLogMonthTable($year, $month)
{
         global $db_engine;
         $tablename = "CARD_STUDENT_ENTRY_LOG_".$year."_".$month;
         $sql = "SHOW TABLES LIKE '$tablename'";
         $records = $db_engine->returnResultSet($sql);
         if(count($records)>0)
         {
         	$sql = "ALTER TABLE $tablename ADD COLUMN RecordStation varchar(100) DEFAULT NULL AFTER RecordTime";
         	$db_engine->db_db_query($sql);
         }else{
	         $sql = "CREATE TABLE IF NOT EXISTS $tablename (
			          RecordID int(11) NOT NULL auto_increment,
			          UserID int(11) NOT NULL,
			          DayNumber int(11) NOT NULL,
			          RecordTime time,
					  RecordStation varchar(100) DEFAULT NULL,
			          RecordType int,
			          RecordStatus int,
			          DateInput datetime,
			          DateModified datetime,
			          PRIMARY KEY (RecordID),
			          INDEX DayNumber (DayNumber),
			          INDEX UserID (UserID) 
			         ) ENGINE=InnoDB charset=utf8";
	        $db_engine->db_db_query($sql);
         }
        return $tablename;
}

function buildStaffAttendance2MonthTable($year="", $month="")
{
         global $db_engine;
         $year = ($year == "") ? date("Y") : $year;
         $month = ($month == "") ? date("m") : $month;

         $card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$year."_".$month;
         $sql = "CREATE TABLE IF NOT EXISTS $card_log_table_name (
                        RecordID int(11) NOT NULL auto_increment,
                        StaffID int(11) NOT NULL,
                        DayNumber int(11) NOT NULL,
                        Duty int,
                        DutyStart time,
                        DutyEnd time,
                        StaffPresent int,
                        InTime time,
                        InSchoolStatus int,
                        InSchoolStation varchar(100),
                        InWaived int,
                        InAttendanceRecordID int,
                        OutTime time,
                        OutSchoolStatus int,
                        OutSchoolStation varchar(100),
                        OutWaived int,
                        OutAttendanceRecordID int,
                        MinLate int,
                        MinEarlyLeave int,
                        RecordType int,
                        RecordStatus int,
                        DateInput datetime,
                        DateModified datetime,
                        PRIMARY KEY (RecordID),
                        UNIQUE StaffDay (StaffID, DayNumber),
                        INDEX DayNumber (DayNumber),
                        INDEX StaffID (StaffID)
                   ) ENGINE=InnoDB charset=utf8";
         $db_engine->db_db_query($sql);
         return $card_log_table_name;
}
function buildStaffAttendance2InterLog($year="", $month="")
{
         global $db_engine;
         $year = ($year == "") ? date("Y") : $year;
         $month = ($month == "") ? date("m") : $month;

         $card_log_table_name = "CARD_STAFF_ATTENDANCE2_INTER_LOG_".$year."_".$month;
         $sql = "CREATE TABLE IF NOT EXISTS $card_log_table_name (
                        RecordID int(11) NOT NULL auto_increment,
                        StaffID int(11) NOT NULL,
                        DayNumber int(11) NOT NULL,
                        RecordTime time,
                        RecordType int,
                        RecordStatus int,
                        DateInput datetime,
                        DateModified datetime,
                        PRIMARY KEY (RecordID),
                        INDEX StaffDay (StaffID, DayNumber),
                        INDEX DayNumber (DayNumber),
                        INDEX StaffID (StaffID)
                  ) ENGINE=InnoDB charset=utf8";
         $db_engine->db_db_query($sql);
         return $card_log_table_name;
}

function buildStaffRawLogMonthTable($year, $month)
{
         global $db_engine;
         $tablename = "CARD_STAFF_ATTENDANCE2_RAW_LOG_".$year."_".$month;
         $sql = "CREATE TABLE IF NOT EXISTS $tablename (
          RecordID int(11) NOT NULL auto_increment,
          UserID int(11) NOT NULL,
          DayNumber int(11) NOT NULL,
          RecordTime time,
          RecordStation varchar(100),
          RecordType int,
          RecordStatus int,
          DateInput datetime,
          DateModified datetime,
          PRIMARY KEY (RecordID),
          INDEX DayNumber (DayNumber),
          INDEX UserID (UserID)
         ) ENGINE=InnoDB charset=utf8";
        $db_engine->db_db_query($sql);
        return $tablename;
}


function retrieveStudentReminder($targetDate, $targetStudentID)
{
         # Deprecated
}





# conver utf-8 octets (eg. &#12345) to the represented chars in UTF-8
function octets2char( $input ) {
	if(!function_exists("octets2char_callback")){
         function octets2char_callback($octets) {
                   $dec = $octets[1];
		           if ($dec < 128) {
		             $utf8Substring = chr($dec);
		           } else if ($dec < 2048) {
		             $utf8Substring = chr(192 + (($dec - ($dec % 64)) / 64));
		             $utf8Substring .= chr(128 + ($dec % 64));
		           } else {
		             $utf8Substring = chr(224 + (($dec - ($dec % 4096)) / 4096));
		             $utf8Substring .= chr(128 + ((($dec % 4096) - ($dec % 64)) / 64));
		             $utf8Substring .= chr(128 + ($dec % 64));
		           }
		           return $utf8Substring;
        }
    }
  return preg_replace_callback('|&#([0-9]{1,});|', 'octets2char_callback', $input );                                
  
}

# convert the string with octets to represted chars in UTF-8 form
function utf8Encode2($source) {
   $utf8Str = '';
   $entityArray = explode ("&#", $source);
   $size = count ($entityArray);
   for ($i = 0; $i < $size; $i++) {
       $subStr = $entityArray[$i];
       $nonEntity = strpos ($subStr, ';');
       if ($nonEntity !== false) {
			$str = substr($subStr,0,$nonEntity);
			$str = "&#".$str.";";
			$str_end=substr($subStr,$nonEntity+1);

			$convertedStr=rawurlencode(octets2char($str));
			//$convertedStr=octets2char($str);
			
			$utf8Str.=$convertedStr.$str_end;
       }
       else {
           $utf8Str .= $subStr;
       }
   }
   //return preg_replace('/[:\\x5c\\/*?"<>|]/', '_', $utf8Str);
   return $utf8Str;

}

function createEntry_Card_Student_Record_Storage($year="", $month="")
{
	global $db_engine;
         $year=($year=="")?date("Y"):$year;
         $month=($month=="")?date("m"):$month;

         $sql = "INSERT INTO CARD_STUDENT_RECORD_DATE_STORAGE (Year, Month)
                        VALUES ($year, $month)";
         $db_engine->db_db_query($sql);
}

//callback function for the regex
function utf8_entity_decode($entity){
	$convmap = array(0x0, 0x10000, 0, 0xfffff);
	return mb_decode_numericentity($entity, $convmap, 'UTF-8');
}

//decode decimal HTML entities added by web browser
function conv_utf8_string($ParString) {
	global $g_encoding_unicode;
	//global $intranet_default_lang;
	//$intranet_default_lang = 'b5';
	if (!$g_encoding_unicode){
		if ($intranet_default_lang == "b5") $ChangeType = "BIG5";
		if ($intranet_default_lang == "gb") $ChangeType = "GB2312";
		$ParString = iconv($ChangeType, 'utf-8', $ParString);
	}
	$OutString = preg_replace('/&#\d{2,5};/ue', "utf8_entity_decode('$0')", $ParString);
	return $OutString;
}

function send_sms($sms_type, $dailylog_tablename="")
{
	global $bl_sms_version, $targetUserID, $sms_vendor, $intranet_root, $plugin;
	
	if($bl_sms_version==2)
	{
		include_once("../../includes/libsmsv2.php");
		$lsms 			= new libsmsv2();
		
		$student_id 	= $targetUserID;
		$template_code 	= ($sms_type==1) ? "STUDENT_ATTEND_ARRIVAL" : "STUDENT_ATTEND_LEAVE";
		
		//check the system template status and content
		$template_status = 0;
		if($lsms->returnSystemMsg($template_code) and $lsms->returnTemplateStatus("",$template_code))	$template_status = 1;
		if($template_status and $plugin['attendancestudent'])
		{
			$send_flag 		= 1;
			# leave school > check is the first record or not, skip duplicate sms
			if($sms_type==2)
			{
				$sql = "select LeaveSchoolTime from $dailylog_tablename where UserID=$targetUserID and DayNumber=". date("d");
				$temp = $lsms->returnArray($sql, 1);
				$LeaveSchoolTime = $temp[0][0];
				if($LeaveSchoolTime!="")	$send_flag = 0;
			}
			
			if($send_flag)
				$lsms->sendSMS_student($student_id, $template_code, $sms_type);
		}
		
	}
	
}

// Please note that this function directly check on the super global $_REQUEST['AccessTime'] and $_REQUEST['AccessCode']
function checkValidAccess()
{
	global $sys_custom;
	
	$valid_access = true;
	if($sys_custom['CardApiUseSecurityCode']){
		
		if(!isset($_REQUEST['AccessTime']) || $_REQUEST['AccessTime']=='' || !isset($_REQUEST['AccessCode']) || $_REQUEST['AccessCode']==''){
			$valid_access = false;
		}
		if($valid_access)
		{
			$now_ts = time();
			$access_ts = intval($_REQUEST['AccessTime']);
			$threshold_period = 2 * 60; // seconds
			if(($now_ts - $access_ts) > $threshold_period){
				$valid_access = false;
			}
			if($valid_access)
			{
				$hashed_access_code = generateCardApiAccessCode($access_ts); // generateCardApiAccessCode() is defined in lib.php
				if($_REQUEST['AccessCode'] != $hashed_access_code){
					$valid_access = false;
				}
			}
		}
	}
	return $valid_access;
}

?>