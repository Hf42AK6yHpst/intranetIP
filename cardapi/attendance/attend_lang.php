<?
// kenenth chung
if ($intranet_session_language == "en")
{
   $attend_lang['CardNotRegistered'] = "This Card has not been registered. Please contact System Administrator.";
   $attend_lang['APINoAccess'] = "This system does not have this function. Please contact System Administrator.";
   $attend_lang['WithinIgnorePeriod'] = "This Card has been processed.";
   $attend_lang['SystemNotInit'] = "System has not been initialized.";
   $attend_lang['NotAllowToOutLunch'] = "Not allowed to go out for lunch.";
   $attend_lang['NotAllowToOutLunchAgain'] = "Not allowed to go out for lunch again.";
   $attend_lang['InSchool'] = " has arrived. Time recorded: ";
   $attend_lang['LeaveSchool'] = " has left. Time recorded: ";
   $attend_lang['InSchool_late'] = " has arrived (LATE). Time recorded: ";
   $attend_lang['LeaveSchool_early'] = " has left (EARLY LEAVE). Time recorded: ";
   $attend_lang['OutLunch'] = " has gone out for lunch. Time recorded: ";
   $attend_lang['BackFromLunch'] = " has arrived school from lunch. Time recorded: ";
   $attend_lang['BackFromLunch_late'] = " has arrived school from lunch (LATE). Time recorded: ";
   $attend_lang['InvalidIP'] = "This machine is not allowed to take Attendance Record";
   $attend_lang['NoNeedToTakeAttendance'] = " does not need to use smart card. Record is ignored.";
   $attend_lang['AlreadyPresent'] = " has arrived already.";
   $attend_lang['LateCount'] = "Number of late(s): ";
   $attend_lang['NoSuchFunctionForStaff'] = "This function is not available for Staff";
   $attend_lang['RecordSuccessful'] = " has been recorded. Time recorded: ";
   $attend_lang['StaffEarlyLeave'] = " has been recorded. Please place your card again when you leave. Time recorded: ";
   $attend_lang['NonSchoolDay'] = " does not need to go to school today.";
   $attend_lang['QualiEd_LaterThanType2'] = " has arrived. Time recorded:";
   $attend_lang['PresetOut1'] = " have outing activity (";
	 $attend_lang['PresetOut2'] = "). Please contact teacher(";
	 $attend_lang['PresetOut3'] = ") for your situation.";
	 $attend_lang['On'] = " on ";
	 $attend_lang['To'] = " to ";
   
   # eEnrollment
   $attend_lang['not_in_time_range'] = "Not in activity time range.";
   $attend_lang['have_attendance_record'] = "Attendance record exists.";
   $attend_lang['not_enroll'] = "Not enrolled to this activity.";
   $attend_lang['meeting'] = "Meeting";
   $attend_lang['attend'] = " has arrived. Time recorded: ";
}
elseif ($intranet_session_language == "gb")
{
   $attend_lang['CardNotRegistered'] = "此智能卡并未登记. 请联络系统管理员.";
   $attend_lang['APINoAccess'] = "此系统并没有此项功能. 请联络系统管理员.";
   $attend_lang['WithinIgnorePeriod'] = "此卡已被处理.";
   $attend_lang['SystemNotInit'] = "系统并未进行初始化.";
   $attend_lang['NotAllowToOutLunch'] = "不能外出用膳.";
   $attend_lang['NotAllowToOutLunchAgain'] = "不能再外出.";
   $attend_lang['InSchool'] = "已经回校. 时间为: ";
   $attend_lang['LeaveSchool'] = "已经离开. 时间为: ";
   $attend_lang['InSchool_late'] = "已经回校(迟到). 时间为:";
   $attend_lang['LeaveSchool_early'] = "已经离开(早退). 时间为:";
   $attend_lang['OutLunch'] = "已经外出用膳. 时间为:";
   $attend_lang['BackFromLunch'] = "已经由外出用膳回校. 时间为:";
   $attend_lang['BackFromLunch_late'] = "已经由外出用膳回校(迟到). 时间为:";
   $attend_lang['InvalidIP'] = "此终端机没有权限进行点名.";
   $attend_lang['NoNeedToTakeAttendance'] = "无须拍卡点名.";
   $attend_lang['AlreadyPresent'] = "已经回校.";
   $attend_lang['LateCount'] = "迟到次数:";
   $attend_lang['NoSuchFunctionForStaff'] = "此项功能只用作纪录学生出入";
   $attend_lang['RecordSuccessful'] = "已被纪录. 时间为: ";
   $attend_lang['StaffEarlyLeave'] = "已被纪录. 请于离校时再次拍卡. 时间为: ";
   $attend_lang['NonSchoolDay'] = "今天无需上课.";
   $attend_lang['QualiEd_LaterThanType2'] = "已回校. 时间为:";
   $attend_lang['PresetOut1'] = " 已设定外出活动 (";
	 $attend_lang['PresetOut2'] = "). 請聯絡相關老師(";
	 $attend_lang['PresetOut3'] = ").";
	 $attend_lang['On'] = "於";
	 $attend_lang['To'] = "至";
   

   # eEnrollment
   $attend_lang['not_in_time_range'] = "不在活动时间范围。";
   $attend_lang['have_attendance_record'] = "已有出席纪录。";
   $attend_lang['not_enroll'] = "没有成功参加此活动。";
   $attend_lang['meeting'] = "聚会";
   $attend_lang['attend'] = "已经到达. 时间:";

   # Patrol
   $attend_lang['OnlyAvaliableForStaff'] = "职员专用.";
}
else
{
   $attend_lang['CardNotRegistered'] = "此智能卡並未登記. 請聯絡系統管理員.";
   $attend_lang['APINoAccess'] = "此系統並沒有此項功能. 請聯絡系統管理員.";
   $attend_lang['WithinIgnorePeriod'] = "此卡已被處理.";
   $attend_lang['SystemNotInit'] = "系統並未進行初始化.";
   $attend_lang['NotAllowToOutLunch'] = "不能外出用膳.";
   $attend_lang['NotAllowToOutLunchAgain'] = "不能再外出.";
   $attend_lang['InSchool'] = " 已經回校. 時間為: ";
   $attend_lang['LeaveSchool'] = " 已經離開. 時間為: ";
   $attend_lang['InSchool_late'] = " 已經回校(遲到). 時間為: ";
   $attend_lang['LeaveSchool_early'] = " 已經離開(早退). 時間為: ";
   $attend_lang['OutLunch'] = " 已經外出用膳. 時間為: ";
   $attend_lang['BackFromLunch'] = " 已經由外出用膳回校. 時間為: ";
   $attend_lang['BackFromLunch_late'] = " 已經由外出用膳回校(遲到). 時間為: ";
   $attend_lang['InvalidIP'] = "此終端機沒有權限進行點名.";
   $attend_lang['NoNeedToTakeAttendance'] = " 無須拍卡點名.";
   $attend_lang['AlreadyPresent'] = " 已經回校.";
   $attend_lang['LateCount'] = "遲到次數: ";
   $attend_lang['NoSuchFunctionForStaff'] = "此項功能只用作紀錄學生出入";
   $attend_lang['RecordSuccessful'] = "已被紀錄. 時間為: ";
   $attend_lang['StaffEarlyLeave'] = "已被紀錄. 請於離校時再次拍卡. 時間為: ";
   $attend_lang['NonSchoolDay'] = "今天無需上課.";
   $attend_lang['QualiEd_LaterThanType2'] = "已回校. 時間為: ";
   $attend_lang['PresetOut1'] = " 已設定外出活動 (";
	 $attend_lang['PresetOut2'] = "). 请联络相关老师(";
	 $attend_lang['PresetOut3'] = ").";
	 $attend_lang['On'] = "于";
	 $attend_lang['To'] = "至";

   # eEnrollment
   $attend_lang['not_in_time_range'] = "不在活動時間範圍。";
   $attend_lang['have_attendance_record'] = "已有出席紀錄。";
   $attend_lang['not_enroll'] = "沒有成功參加此活動。";
   $attend_lang['meeting'] = "聚會";
   $attend_lang['attend'] = "已經到達. 時間:";
}

?>