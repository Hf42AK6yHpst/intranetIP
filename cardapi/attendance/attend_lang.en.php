<?php
// Editing by 
$attend_lang['CardNotRegistered'] = "This Card has not been registered. Please contact System Administrator.";
$attend_lang['APINoAccess'] = "This system does not have this function. Please contact System Administrator.";
$attend_lang['WithinIgnorePeriod'] = "This Card has been processed.";
$attend_lang['SystemNotInit'] = "System has not been initialized.";
$attend_lang['NotAllowToOutLunch'] = "Not allowed to go out for lunch.";
$attend_lang['NotAllowToOutLunchAgain'] = "Not allowed to go out for lunch again.";
$attend_lang['InSchool'] = " has arrived. Time recorded: ";
$attend_lang['LeaveSchool'] = " has left. Time recorded: ";
$attend_lang['InSchool_late'] = " has arrived (LATE). Time recorded: ";
$attend_lang['LeaveSchool_early'] = " has left (EARLY LEAVE). Time recorded: ";
$attend_lang['OutLunch'] = " has gone out for lunch. Time recorded: ";
$attend_lang['BackFromLunch'] = " has arrived school from lunch. Time recorded: ";
$attend_lang['BackFromLunch_late'] = " has arrived school from lunch (LATE). Time recorded: ";
$attend_lang['InvalidIP'] = "This machine is not allowed to take Attendance Record";
$attend_lang['NoNeedToTakeAttendance'] = " does not need to use smart card. Record is ignored.";
$attend_lang['AlreadyPresent'] = " has arrived already.";
$attend_lang['LateCount'] = "Number of late(s): ";
$attend_lang['NoSuchFunctionForStaff'] = "This function is not available for Staff";
$attend_lang['RecordSuccessful'] = " has been recorded. Time recorded: ";
$attend_lang['StaffEarlyLeave'] = " has been recorded. Please place your card again when you leave. Time recorded: ";
$attend_lang['NonSchoolDay'] = " does not need to go to school today.";
$attend_lang['QualiEd_LaterThanType2'] = " has arrived. Time recorded:";
$attend_lang['PresetOut1'] = " have outing activity (";
$attend_lang['PresetOut2'] = "). Please contact teacher (";
$attend_lang['PresetOut3'] = ") for your situation.";
$attend_lang['PresetAbsence'] = " have preset absence because of ";
$attend_lang['PresetAbsence1'] = ". Please contact related teacher for arrangement. ";
$attend_lang['On'] = " on ";
$attend_lang['To'] = " to ";
$attend_lang['OverLateMinWarning'] = " have been late for more than ";
$attend_lang['OverLateMinWarning1'] = " minutes. Please contact your class teacher for follow-up.";
$attend_lang['EntryLogRecorded'] = " has been recorded in Entry Log. Time recorded: ";
$attend_lang['HasTappedCard'] = " has tapped card. Time recorded: ";
$attend_lang['IgnoreWithinLunchPeriod'] = "PM session has not started yet, please tap card after <!--TIME-->.";

# eEnrollment
$attend_lang['not_in_time_range'] = " Not in activity time range.";
$attend_lang['have_attendance_record'] = " Attendance record exists.";
$attend_lang['not_enroll'] = " Not enrolled to this activity.";
$attend_lang['meeting'] = "Meeting";
$attend_lang['attend'] = " has arrived. Time recorded: ";
$attend_lang['leave'] = " has left. Time recorded: ";

?>