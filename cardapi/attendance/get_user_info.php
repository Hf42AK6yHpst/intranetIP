<?php
// Editing by 
/*
 * 2019-06-03 (Carlos): Apply checkValidAccess() to verify valid access.
 * 2018-11-01 (Carlos): Cater $sys_custom['SupplementarySmartCard'] multiple card numbers.
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_opendb();

include_once("attend_functions.php");
if ($dlang != "b5" && $dlang!= "gb") $intranet_session_language = "en";
else $intranet_session_language = $dlang;

include_once("attend_lang.".$intranet_session_language.".php");
include_once("attend_const.php");

header("Content-type: text/plain; charset=utf-8");

if(!checkValidAccess()){ // defined in attend_functions.php
	intranet_closedb();
	exit();
}

$GeneralSetting = new libgeneralsettings();
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance');
$StudentAttendanceAllowed = isIPAllowed('StudentAttendance');

$Settings = $GeneralSetting->Get_General_Setting('StaffAttendance');
$StaffAttendanceAllowed = isIPAllowed('StaffAttendance');

# Check IP
if($StudentAttendanceAllowed || $StaffAttendanceAllowed)
{
	// has either one module is ok
}else
{
     //echo output_env_str(CARD_RESP_INVALID_IP."###".$attend_lang['InvalidIP']);
     intranet_closedb();
     exit();
}

$li = new libdb();

$sql = "SELECT CardID, EnglishName, ChineseName, ClassName, ClassNumber, UserLogin, RecordType 
        FROM INTRANET_USER WHERE RecordType IN (1,2) AND RecordStatus IN (0,1,2)
                ORDER BY ClassName, ClassNumber, EnglishName";

$result = $li->returnArray($sql);

if($sys_custom['SupplementarySmartCard']){
	for($i=2;$i<=4;$i++){
		$sql = "SELECT CardID".$i.", EnglishName, ChineseName, ClassName, ClassNumber, UserLogin, RecordType 
        		FROM INTRANET_USER WHERE RecordType IN (1,2) AND RecordStatus IN (0,1,2) AND CardID".$i." IS NOT NULL AND CardID".$i." <> '' 
                ORDER BY ClassName, ClassNumber, EnglishName";
        $temp_records = $li->returnArray($sql);
        if(count($temp_records)>0){
        	$result = array_merge($result,$temp_records);
        }
	}
}

$result_size = count($result);
$final_result = array();
for($i=0;$i<$result_size;$i++){
	$row = array();
	foreach($result[$i] as $key => $val){
		if(is_numeric($key)){
			$row[] = $val;
		}
	}
	$final_result[] = $row;
}

$lexport = new libexporttext();

$exportColumn = array("CardID", "EnglishName", "ChineseName", "ClassName", "Class Number", "LoginID", "User Type");

$export_content = $lexport->GET_EXPORT_TXT($final_result, $exportColumn, "###", "\r\n", ",", 0);

echo $export_content;

intranet_closedb();
?>