<?php
/*
 * 2014-10-21 (Carlos) [ip2.5.5.10.1]: Created
 */
/*
 * @Params
 * $TargetUserID
 * $AccessDateTime
 * $Status
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

if(!isset($TargetUserID) || !isset($AccessDateTime) || !isset($Status)){
	exit;
}

intranet_opendb();

$lc = new libcardstudentattend2();

if($plugin['ASLParentApp'] || $plugin['eClassApp']){
	$lc->Notify_Student_Status(array('UserID'=>$TargetUserID,'AccessDateTime'=>$AccessDateTime,'Status'=>$Status));
}

intranet_closedb();
exit();
?>