<?php
// Editing by 
####################################################################
# Only for C# Client Programs
####################################################################
# 2020-05-20 (Ray): add HostelAttendance_Reason for staff attendance
# 2019-06-03 (Carlos): Apply checkValidAccess() to verify valid access.
# 2016-08-30 (Carlos): Added tap card location.
#
# Created by : Kenneth Wong
# Creation Date : 20060614
####################################################################
# Data Handler for Entry log ONLY
# Client Terminal Program is required to change!
####################################################################
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgeneralsettings.php");
include_once("../../includes/libcardstudentattend2.php");
intranet_opendb();

// get all general setting for student attendance
$GeneralSetting = new libgeneralsettings();
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance');
$lc = new libcardstudentattend2();
/*
include_once("attend_functions.php");
if ($dlang != "b5") $intranet_session_language = "en";
else $intranet_session_language = "b5";

include_once("attend_lang.php");
include_once("attend_const.php");
*/
include_once("attend_functions.php");
if ($dlang != "b5" && $dlang!= "gb") $intranet_session_language = "en";
else $intranet_session_language = $dlang;

include_once("attend_lang.".$intranet_session_language.".php");
include_once("attend_const.php");

header("Content-type: text/plain; charset=utf-8");

######################################################
# Param : (From QueryString)
#    CardID
######################################################

# $db_engine (from functions.php)

if(!checkValidAccess()){ // defined in attend_functions.php
	echo output_env_str(CARD_RESP_INVALID_IP."###".$attend_lang['InvalidIP']);
	intranet_closedb();
	exit();
}

# Check IP
if (!isIPAllowed())
{
     echo CARD_RESP_INVALID_IP."###".$attend_lang['InvalidIP'];
     intranet_closedb();
     exit();
}

if ($CardID == '')
{
    # Card not registered
    echo CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered'];
    intranet_closedb();
    exit();
}

# 1. Get UserInfo
$namefield = getNameFieldByLang();
$sql = "SELECT UserID, $namefield, RecordType, ClassName, ClassNumber, UserLogin FROM INTRANET_USER WHERE CardID = '$CardID'";
$temp = $db_engine->returnArray($sql,5);
list($targetUserID , $targetName, $targetType, $targetClass, $targetClassNumber, $TargetUserLogin) = $temp[0];



# Check Card
if ($targetUserID == 0)
{
    # Card not registered
    echo CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered'];
    intranet_closedb();
    exit();
}
else if ($targetType == 1)    # Staff, redirection
{
	if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
		intranet_closedb();
		include('receiver_csharp.php');
		exit();
	} else {
		echo CARD_RESP_NO_SUCH_FUNCTION_FOR_STAFF."###".$attend_lang['NoSuchFunctionForStaff'];
		intranet_closedb();
		exit();
	}
}
else if ($targetType == 2)
{

	 $sitename = trim(urldecode($sitename));

     # Create Monthly data table
     $current_time = time();
     if ($external_time != "")
     {
         $current_time = strtotime(date('Y-m-d') . " " . $external_time);
     }
     if ($external_date != "" && $external_time)
	 {
		$current_time = strtotime($external_date. " " . $external_time);
	 }
     $month = date('m',$current_time);
     $year = date('Y',$current_time);
     $day = date('d',$current_time);
     $today = date('Y-m-d',$current_time);
     $time_string = date('H:i:s',$current_time);
     $ts_now = $current_time - strtotime($today);

     $tablename = buildEntryLogMonthTable($year, $month);

     # Change the name display
     $targetName = "$targetName ($targetClass-$targetClassNumber)";
	 
     # Check Last Card Read Time for ignore period
     $sql = "SELECT TIME_TO_SEC(MAX(RecordTime)) FROM $tablename WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
     $temp = $db_engine->returnVector($sql);
     $curr_tslastMod = $temp[0];

     if ($curr_tslastMod == "")
     {
     }
     else
     {
         # Check whether within last n mins
         //$ignore_period = get_file_content("$intranet_root/file/stattend_ignore.txt");
         $ignore_period = $Settings['IgnorePeriod'];
         $ignore_period += 0;
         if ($ignore_period > 0)
         {
             $ts_ignore = $ignore_period * 60;
             # Check whether it is w/i last n mins
             if ($ts_now - $curr_tslastMod < $ts_ignore)
             {
                 echo CARD_RESP_IGNORE_WITHIN_PERIOD."###".$attend_lang['WithinIgnorePeriod'];
                 intranet_closedb();
                 exit();
             }
         }
     }

     # Insert Record
     /*
     $sql = "INSERT INTO $tablename (UserID, DayNumber, RecordTime, RecordStation, DateInput ,DateModified)
                    VALUES ('$targetUserID', '$day', '$time_string', '".$db_engine->Get_Safe_Sql_Query($sitename)."', now(), now())";
     $db_engine->db_db_query($sql);
     */
     $lc->Record_Raw_Log($targetUserID,$current_time,$sitename);

     $hostel_status = '';
	 if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
		 $SettingList = array();
		 $SettingList[] = "'HostelAttendanceLocationIn'";
		 $SettingList[] = "'HostelAttendanceLocationOut'";
		 $Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
		 $location_in = explode(',', $Settings['HostelAttendanceLocationIn']);
		 $location_out = explode(',', $Settings['HostelAttendanceLocationOut']);

		 $lc->createTableHostelAttendanceDailyLog($year,$month);
		 $daily_log_table = sprintf("CARD_STUDENT_HOSTEL_ATTENDANCE_DAILY_LOG_%4d_%02d",$year,$month);
		 $sql = "SELECT RecordID
		 FROM
		 $daily_log_table
		 WHERE UserID='".$targetUserID."' AND DayNumber='$day'";
		 $records = $lc->returnResultSet($sql);
		 $time_field = '';
		 if(in_array($sitename, $location_in)) {
			 $time_field = 'InTime';
			 $hostel_status = 'HA';
			 $hostel_in_status = CARD_STATUS_PRESENT;
		 } else if(in_array($sitename, $location_out)) {
			 $time_field = 'OutTime';
			 $hostel_status = 'HL';
			 $hostel_in_status = CARD_STATUS_ABSENT;
		 }

		 if($time_field != '') {
			 $data = array();
			 $data[$time_field] = date('Y-m-d H:i:s',$current_time);
			 $data['InStatus'] = $hostel_in_status;
			 $data['Reason'] = '';
			 $_SESSION['UserID'] = $targetUserID;
			 if(count($records) > 0) {
				 $RecordID = $records[0]['RecordID'];
				 $data['RecordID'] = $RecordID;
				 $lc->upsertHostelAttendanceRecord($year,$month,$data);
			 } else {
				 $data['UserID'] = $targetUserID;
				 $data['DayNumber'] = $day;
				 $lc->upsertHostelAttendanceRecord($year,$month,$data);
			 }
		 }
	 }

     echo CARD_RESP_RECORD_SUCCESSFUL."###".$targetName.$attend_lang['RecordSuccessful'].$time_string."###".$TargetUserLogin;

	 if($hostel_status != '') {
		 Notify_Student_Status_entry($targetUserID, $current_time, $hostel_status);
	 }
	 intranet_closedb();
     exit();

}
else     # Card Not Registered
{
    echo CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered'];
    intranet_closedb();
    exit();
}




intranet_closedb();

function Notify_Student_Status_entry($TargetUserID,$AccessDateTime,$Status)
{
	global $sys_custom;
	if(isset($sys_custom['cardapi_http_protocol']) && in_array($sys_custom['cardapi_http_protocol'],array('http','https'))){
		$http = $sys_custom['cardapi_http_protocol'];
	}else{
		$http = checkHttpsWebProtocol()? "https" : "http";
		//$http = "http"; // must use http as https requires to bypass cert and cater different IP addresses for cloud clients
	}
	$is_https = $http == "https";
	$server_name = (isset($sys_custom['cardapi_host_ip']) && $sys_custom['cardapi_host_ip']!='')? $sys_custom['cardapi_host_ip'] : $_SERVER['SERVER_NAME'];
	$http_host = $http."://".$server_name.(!in_array($_SERVER["SERVER_PORT"],array(80,443))?":".$_SERVER["SERVER_PORT"]:"");
	$cmd = "/usr/bin/nohup wget".($is_https?" --no-check-certificate":"")." -q '$http_host/cardapi/attendance/notify_student_status.php?TargetUserID=".OsCommandSafe($TargetUserID)."&AccessDateTime=".OsCommandSafe($AccessDateTime)."&Status=".OsCommandSafe($Status)."' > /dev/null &";
	shell_exec($cmd);
}
?>