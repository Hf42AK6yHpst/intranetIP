<?
// Editing by 
/*
 * !!! Please note that this file encoding is 中文Big5碼 !!!
 */
/************************************************************* Change log ******************************************************************************
 * 2014-11-05 (Carlos): Modified checkSession($key), changed key file directory from /tmp to /file/ePaymentTerminal catering cloud server sharing hosts
 *******************************************************************************************************************************************************/
$clientIP = getRemoteIpAddress();
//$payment_authfile = get_file_content("$intranet_root/file/payment_auth.txt");
//$payment_auth = $Settings['PaymentNoAuth'];  // explode("\n",$payment_authfile);
//$payment_st_expiry = $Settings['ConnectionTimeout']; //trim(get_file_content("$intranet_root/file/payment_expiry.txt"));


function isIPAllowed()
{
         return true;
         global $clientIP;
         global $intranet_root;
         global $dlang;
					
					include_once('../includes/libgeneralsettings.php');
				$GeneralSetting = new libgeneralsettings();
				$Settings = $GeneralSetting->Get_General_Setting('ePayment');	
				
         # Check IP
         $ips = $Settings['TerminalIPList'];// get_file_content("$intranet_root/file/payment_ip.txt");
         $ipa = explode("\n", $ips);
         $ip_ok = false;
         foreach ($ipa AS $ip)
         {
              $ip = trim($ip);
              if ($ip == "0.0.0.0")
              {
                  $ip_ok = true;
              }
              else if (testip($ip,$clientIP))
              {
                   $ip_ok = true;
              }
         }

         if (!$ip_ok)
         {
              $msg = ($dlang=="b5"?"此終端機之 IP 位址並不可以登入系統":"Invalid IP address. Action cancelled.");
              #echo "$msg";
              echo "-1###invalid IP";
              return false;
         }
         return true;
}

function checkSession($key)
{
         global $payment_keyfile_temp;
         global $clientIP, $TerminalID;
         global $dlang;
         global $payment_expiry, $intranet_root;
         
         include_once('../includes/libgeneralsettings.php');
         $GeneralSetting = new libgeneralsettings();
				 $Settings = $GeneralSetting->Get_General_Setting('ePayment');	
				 $payment_st_expiry = $Settings['ConnectionTimeout']; //trim(get_file_content("$intranet_root/file/payment_expiry.txt"));
         # Check session key
         //$keyfilepath = "$payment_keyfile_temp/payment_ip_$clientIP"."_t$TerminalID";
         $dir = $intranet_root."/file/ePaymentTerminal";
         if(!file_exists($dir) || !is_dir($dir)){
         	mkdir($dir);
         }
         $keyfilepath = $dir."/payment_ip_$clientIP"."_t$TerminalID";
         
         $msg = ($dlang=="b5"?"作業過時, 請重新開啟本程式":"Registration Expired. Pls restart the terminal program.");
         if (!is_file($keyfilepath))
         {
              echo "-1###No Key file";
              return false;
         }

         $content = get_file_content($keyfilepath);
         $now = time();
         $data = explode("\n",$content);
         list ($ts,$oldkey) = $data;
         if ($payment_st_expiry=="")
         {
             $payment_st_expiry = $payment_expiry;
         }

         if ($oldkey != $key || ($now - $ts) > $payment_st_expiry*60 )
         {
              echo "-1###Key expired";
              if ($oldkey != $key)
              {
                  echo "unmatch";

              }
              else
              {
                  echo "now: $now|ts: $ts|exp: $payment_st_expiry";
              }
              return false;
         }
         return true;
}

function isPaymentNoLogin()
{
	include_once('../includes/libgeneralsettings.php');
	$GeneralSetting = new libgeneralsettings();
	$Settings = $GeneralSetting->Get_General_Setting('ePayment');	
	
  return ($Settings['PaymentNoAuth']==1);
}
function isPurchaseNoLogin()
{
	include_once('../includes/libgeneralsettings.php');
  $GeneralSetting = new libgeneralsettings();
	$Settings = $GeneralSetting->Get_General_Setting('ePayment');	

	//global $payment_auth;
	return ($Settings['PurchaseNoAuth']==1);
}

function checkAccess($key,$type)
{
         global $clientIP;

         if ($type==1)        # Payment
         {
             if (isPaymentNoLogin()) return true;
         }
         else if ($type==2)
         {
             if (isPurchaseNoLogin()) return true;
         }
         else return false;

         # Get Username from session log
         $a = new libdb();
         /*
         $sql = "SELECT Username FROM PAYMENT_TERMINAL_LOG WHERE IP = '$clientIP' AND SessionKey = '$key' ORDER BY LoginTime DESC LIMIT 0,1";
         $temp = $a->returnVector($sql);
         $username = $temp[0];
         */
         $username = getUsername($key);

         if ($type==1)
         {
             $sql = "SELECT PaymentAllowed FROM PAYMENT_TERMINAL_USER WHERE Username = '$username'";
             $temp = $a->returnVector($sql);
             return ($temp[0]==1);
         }
         else if ($type==2)
         {
             $sql = "SELECT PurchaseAllowed FROM PAYMENT_TERMINAL_USER WHERE Username = '$username'";
             $temp = $a->returnVector($sql);
             return ($temp[0]==1);
         }
         else return false;
}
function getUsername($key)
{
         global $clientIP, $TerminalID;

         # Get Username from session log
         $a = new libdb();
         $sql = "SELECT Username FROM PAYMENT_TERMINAL_LOG WHERE IP = '$clientIP' AND SessionKey = '$key' AND TerminalID = '$TerminalID' ORDER BY LoginTime DESC LIMIT 0,1";
         $temp = $a->returnVector($sql);
         $username = $temp[0];
         return $username;
}
?>