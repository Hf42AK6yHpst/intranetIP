<?
include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("functions.php");
intranet_opendb();

/*
#####
# return:
0###Failed
or
1###tmchan
where tmchan is intranet login name
######
*/


/*
Param:
$CardID - Card ID of the student
*/

# Check Arguments
if ($CardID == "")
{
    echo "0###Failed";
    exit();
}

$li = new libdb();
$sql = "SELECT UserLogin FROM INTRANET_USER WHERE CardID = '$CardID'";
$temp = $li->returnVector($sql);
$t_login = $temp[0];

if ($t_login == "")
{
    echo "0###Failed";
}
else
{
    echo "1###$t_login";
}

intranet_closedb();
?>
