<?
if ($intranet_session_language == "en")
{
   $response_lang['CardNotRegistered'] = "This Card has not been registered. Please contact System Administrator.";
   $response_lang['APINoAccess'] = "This system does not have this function. Please contact System Administrator.";
   $response_lang['NoLunchboxTicket'] = " does not have today lunch ticket.";
   $response_lang['InvalidIP'] = "This machine is not allowed to take Attendance Record";
   $response_lang['SystemNotInit'] = "System has not been initialized.";
   $response_lang['UnknownError'] = "System Error. Please contact System Administrator.";
   $response_lang['TicketUsedAlready'] = " has already taken lunch box at ";
   $response_lang['TicketOK'] = (isset($plugin['Lunchbox_HCLMS']) && $plugin['Lunchbox_HCLMS']) ? " has been record to not take lunch box." : " can take lunch box.";
   $response_lang['TicketError'] = " has error occurred. Please retry.";

   $response_lang['WithinIgnorePeriod'] = "This Card has been processed.";
   $response_lang['NotAllowToOutLunch'] = "Not allowed to go out for lunch.";
   $response_lang['NotAllowToOutLunchAgain'] = "Not allowed to go out for lunch again.";
   $response_lang['InSchool'] = " has arrived. Time recorded: ";
   $response_lang['LeaveSchool'] = " has left. Time recorded: ";
   $response_lang['InSchool_late'] = " has arrived (LATE). Time recorded: ";
   $response_lang['LeaveSchool_early'] = " has left (EARLY LEAVE). Time recorded: ";
   $response_lang['OutLunch'] = " has gone out for lunch. Time recorded: ";
   $response_lang['BackFromLunch'] = " has arrived school from lunch. Time recorded: ";
   $response_lang['BackFromLunch_late'] = " has arrived school from lunch (LATE). Time recorded: ";
   $response_lang['NoNeedToTakeAttendance'] = " does not need to use smart card. Record is ignored.";
   $response_lang['AlreadyPresent'] = " has arrived already.";
   $response_lang['LateCount'] = "Number of late(s): ";

}
else
{
   $response_lang['CardNotRegistered'] = "此智能咭並未登記. 請聯絡系統管理員.";
   $response_lang['APINoAccess'] = "此系統並沒有此項功能. 請聯絡系統管理員.";
   $response_lang['NoLunchboxTicket'] = "沒有預先買飯.";
   $response_lang['InvalidIP'] = "此終端機沒有權限進行點名.";
   $response_lang['SystemNotInit'] = "系統並未進行初始化.";
   $response_lang['UnknownError'] = "系統問題. 請聯絡系統管理員.";
   $response_lang['TicketUsedAlready'] = " 已經領取飯盒. 領取時間: ";
   $response_lang['TicketOK'] = (isset($plugin['Lunchbox_HCLMS']) && $plugin['Lunchbox_HCLMS']) ? " 己被紀錄為不領取飯盒." : " 可領取飯盒.";
   $response_lang['TicketError'] = " 紀錄有問題. 請重試.";


   $response_lang['WithinIgnorePeriod'] = "此咭已被處理.";
   $response_lang['NotAllowToOutLunch'] = "不能外出用膳.";
   $response_lang['NotAllowToOutLunchAgain'] = "不能再外出.";
   $response_lang['InSchool'] = " 已經回校. 時間為: ";
   $response_lang['LeaveSchool'] = " 已經離開. 時間為: ";
   $response_lang['InSchool_late'] = " 已經回校(遲到). 時間為: ";
   $response_lang['LeaveSchool_early'] = " 已經離開(早退). 時間為: ";
   $response_lang['OutLunch'] = " 已經外出用膳. 時間為: ";
   $response_lang['BackFromLunch'] = " 已經由外出用膳回校. 時間為: ";
   $response_lang['BackFromLunch_late'] = " 已經由外出用膳回校(遲到). 時間為: ";
   $response_lang['NoNeedToTakeAttendance'] = " 無須拍咭點名.";
   $response_lang['AlreadyPresent'] = " 已經回校.";
   $response_lang['LateCount'] = "遲到次數: ";

}
?>
