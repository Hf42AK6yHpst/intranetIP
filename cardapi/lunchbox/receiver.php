<?
####################################################################
# Created by : Kenneth Wong
# Created at : 20051207
####################################################################
# Version updates
# 20051207 : Kenneth Wong
# - Creation
#
####################################################################
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/liblunchbox.php");
intranet_opendb();

include_once("functions.php");
if ($dlang != "b5") $intranet_session_language = "en";
else $intranet_session_language = "b5";

include_once("lang.php");
include_once("const.php");


######################################################
# Param :
#    CardID
#    dlang
######################################################

# $db_engine
/*
if (!$lunchbox_api_open_attendance)
{
     echo mb_convert_encoding(mb_convert_encoding(CARD_RESP_API_NO_ACCESS."###".$response_lang['APINoAccess'];
     intranet_closedb();
     exit();
}
*/

# Check IP
if (!isIPAllowed())
{
     echo mb_convert_encoding(CARD_RESP_INVALID_IP."###".$response_lang['InvalidIP'],"BIG5","UTF-8");
     intranet_closedb();
     exit();
}

# 1. Get UserInfo
$namefield = getNameFieldByLang();
$sql = "SELECT UserID, $namefield, RecordType, ClassName, ClassNumber FROM INTRANET_USER WHERE CardID = '$CardID'";
$temp = $db_engine->returnArray($sql,5);

if (sizeof($temp)==0 || $temp[0][0] == "")
{
    echo mb_convert_encoding(CARD_RESP_CARD_NOT_REGISTER."###".$response_lang['CardNotRegistered'],"BIG5","UTF-8");
    intranet_closedb();
    exit();
}

list($targetUserID , $targetName, $targetType, $targetClass, $targetClassNumber) = $temp[0];
$name_string = "$targetName ($targetClass - $targetClassNumber)";

$curr_year = date('Y');
$curr_month = date('m');
$curr_day = date('d');

$ticket_info = $db_engine->getDayTicketInfo($targetUserID);
if ($ticket_info == FLAG_NO_LUNCH_TICKET)
{
    $db_engine->addBadAction($targetUserID, BAD_ACTION_NO_TICKET);
    echo mb_convert_encoding(CARD_RESP_NO_LUNCHBOX_TICKET."###$name_string".$response_lang['NoLunchboxTicket'],"BIG5","UTF-8");
    intranet_closedb();
    exit();
}

if ($ticket_info == FLAG_DATE_ERROR || !is_array($ticket_info))
{
    echo mb_convert_encoding(CARD_RESP_UNKNOWN_ERROR."###".$response_lang['UnknownError'],"BIG5","UTF-8");
    intranet_closedb();
    exit();
}


list($t_record_id, $t_type, $t_status, $t_taken_time) = $ticket_info;

if ($t_status == STATUS_LUNCH_TICKET_AVAILABLE)
{
    # Update ticket
    if (isset($plugin['Lunchbox_HCLMS']) && $plugin['Lunchbox_HCLMS'])
    {
    	# Hong Chi Lions Morninghill School customization
    	$success = $db_engine->setTicketNotUsed($t_record_id);
    } else
    {
    	$success = $db_engine->setTicketUsed($t_record_id);
    }
    if ($success)
    {
        echo mb_convert_encoding(CARD_RESP_TICKET_SET_OK."###$name_string".$response_lang['TicketOK'],"BIG5","UTF-8");
        intranet_closedb();
        exit();
    }
    else
    {
        echo mb_convert_encoding(CARD_RESP_TICKET_ERROR."###$name_string".$response_lang['TicketError'],"BIG5","UTF-8");
        intranet_closedb();
        exit();
    }
}
else # Used ticket
{
	if (isset($plugin['Lunchbox_HCLMS']) && $plugin['Lunchbox_HCLMS'])
    {
    	echo mb_convert_encoding(CARD_RESP_TICKET_USED_ALREADY."###$name_string".$response_lang['WithinIgnorePeriod'].$t_taken_time,"BIG5","UTF-8");
    } else
    {
	    $db_engine->addBadAction($targetUserID, BAD_ACTION_TICKET_USED_ALREADY);
	    echo mb_convert_encoding(CARD_RESP_TICKET_USED_ALREADY."###$name_string".$response_lang['TicketUsedAlready'].$t_taken_time,"BIG5","UTF-8");
	}
    intranet_closedb();
    exit();
}

intranet_closedb();
?>
