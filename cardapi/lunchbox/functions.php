<?
$clientIP = getRemoteIpAddress();
$db_engine = new liblunchbox();
#$dlang = "en";


function isIPAllowed()
{
         global $clientIP;
         global $intranet_root;
         global $dlang;

         # Check IP
         $ips = get_file_content("$intranet_root/file/stlunch_ip.txt");
         $ipa = explode("\n", $ips);
         $ip_ok = false;
         foreach ($ipa AS $key => $ip)
         {
              $ip = trim($ip);
              if ($ip == "0.0.0.0")
              {
                  $ip_ok = true;
              }
              else if (testip($ip,$clientIP))
              {
                   $ip_ok = true;
              }
         }
         if (!$ip_ok)
         {
              return false;
         }
         return true;
}

?>