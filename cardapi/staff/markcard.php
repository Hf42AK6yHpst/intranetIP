<?
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("functions.php");

# Check IP
if (!isIPAllowed())
{
     $msg = ($dlang=="b5"?"此終端機之 IP 位址並不可以登入系統":"Invalid IP address. Action cancelled.");
     echo "$msg";
     exit();
}
if (!checkSession($key))
{
     $msg = ($dlang=="b5"?"作業過時, 請重新開啟本程式":"Registration Expired. Pls restart the terminal program.");
}

intranet_opendb();
$sitename = intranet_htmlspecialchars($sitename);
$datatype = intranet_htmlspecialchars($datatype);

$sql = "INSERT INTO CARD_STAFF_ATTENDANCE_LOG (CardID,SiteName,RecordedTime,Type)
        VALUES ('$cardid','$sitename',now(),'$datatype')";
$li = new libdb();
$li->db_db_query($sql);
$record = $li->db_insert_id();

$sql = "SELECT DATE_FORMAT(RecordedTime,'%H:%i:%s') FROM CARD_STAFF_ATTENDANCE_LOG WHERE LogID = $record";
$loggedTime = $li->returnVector($sql);

if ($dlang != "b5") $intranet_session_language = "en";
else $intranet_session_language = "b5";

include_once("../../lang/lang.$intranet_session_language.php");
$user_field = getNameFieldByLang();
$sql = "SELECT UserID, CardID, $user_field, UserLogin FROM INTRANET_USER WHERE CardID = '$cardid'";
$user = $li->returnArray($sql,4);
list($id,$card,$name,$login) = $user[0];

if ($dlang == "b5")
{
    $str = "已紀錄.\r\n 時間為: ";
    $fail = "智能咭資料有誤, 請重試或聯絡系統管理員.";
}
else
{
    $str = "is recorded.\r\n Time Recorded: ";
    $fail = "Card Error. Please retry or contact System Administrator.";
}
if ($id!="")
{
    echo "$name $str ".$loggedTime[0];
}
else
{
    echo "$fail";
}

intranet_closedb();
#header("Location: index.php");
?>
