<?
include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("attend_functions.php");

if (!$attendst_api_open_attendance)
{
     echo "This system does not have this function. Please contact System Administrator.";
     exit();
}

# Check IP
if (!isIPAllowed()) exit();


# Check Session key
if (!checkSession($key)) exit();


intranet_opendb();
$sitename = intranet_htmlspecialchars($sitename);
$datatype = intranet_htmlspecialchars($datatype);

$sql = "INSERT INTO CARD_STUDENT_LOG (CardID,SiteName,RecordedTime,Type)
        VALUES ('$cardid','$sitename',now(),'$datatype')";
$li = new libdb();
$li->db_db_query($sql);
$record = $li->db_insert_id();

$sql = "SELECT DATE_FORMAT(RecordedTime,'%H:%i:%s') FROM CARD_STUDENT_LOG WHERE LogID = '".IntegerSafe($record)."'";
$loggedTime = $li->returnVector($sql);

$user_field = ($dlang == "b5")? 'ChineseName':'EnglishName';
$sql = "SELECT UserID, CardID, $user_field, UserLogin, ClassName, ClassNumber FROM INTRANET_USER WHERE CardID = '$cardid'";
$user = $li->returnArray($sql,6);
list($id,$card,$name,$login,$ClassName,$ClassNumber) = $user[0];

if ($dlang == "b5")
{
    $str = "已紀錄.\r\n 時間為: ";
    $fail = "智能咭資料有誤, 請重試或聯絡系統管理員.";
}
else
{
    $str = "is recorded.\r\n Time Recorded: ";
    $fail = "Card Error. Please retry or contact System Administrator.";
}
if ($id!="")
{
    if ($ClassName != "" || $ClassNumber != "")
    {
        $class_str = " ($ClassName - $ClassNumber)";
    }
    else $class_str = "";
    echo "$name$class_str $str ".$loggedTime[0];
}
else
{
    echo "$fail";
}

intranet_closedb();
#header("Location: index.php");
?>
