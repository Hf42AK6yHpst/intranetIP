<?php
// Editing by 
/*
 * 2014-04-10 (Carlos): Added UsedQuota and TotalQuota to PAYMENT_PRINTING_QUOTA_CHANGE_LOG 
 */
include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("../includes/libpayment.php");
include_once("functions.php");
intranet_opendb();
$lpayment = new libpayment();

/*
#####
# return:
-3 : Invalid key
-2 : Argument Error
-1 : No this student / function
0 : Not enough balance
1 : Successful transaction
######
*/

/*	commented by henry chow on 20110825
if ($plugin['payment_printing_quota']==TRUE) // if staff print by "payment", then exit
{
     echo -1;
     exit();
}
*/

/*
Param:
$key - Secret Key
$quota - number of pages to be deducted
$CardID - Card ID of the student
$refund - 1 (yes), else no
$rand - random string
$LicenseMode = 1
*/

# Check Arguments
$string_quota = $quota;
$usedQuota = $quota;
$usedQuota = (int)$usedQuota ; 
$quota = (int)$quota;


if ($CardID == "" || ($quota <= 0 && $refund == 0))
{
    echo -2;
    exit();
}


# Check secret key
if ($LicenseMode != 1) {
	$keysalt = array("1sxvga12");
}
else {
	$client_ip = getRemoteIpAddress();
	if ($PrinterIPLincense[$client_ip] != "") {
		$keysalt = array($PrinterIPLincense[$client_ip]);
	}
	else {
		echo "-3";
		exit();
	}
}
$delimiter = "###";
$valid = false;

foreach ($keysalt as $t_key => $t_value)
{
    $string = $t_value.$delimiter.$string_quota.$delimiter.$CardID.$delimiter.$refund.$delimiter.$rand;
    $hashed_key = md5($string);
    if ($key==$hashed_key)
    {
        $valid = true;
    }
    else
    {
    }
}

if (!$valid)
{
     echo "-3";
     exit();
}

$changeLogType_ByAPI = 3; # Deduction by API
$transaction_detail_recordtype_quota = 2;

# Lock tables
$sql = "LOCK TABLES
             PAYMENT_PRINTING_QUOTA WRITE
             , INTRANET_USER READ
             , PAYMENT_PRINTING_QUOTA WRITE
             , PAYMENT_COPIER_TRANSACTION_DETAIL WRITE
             ";
$lpayment->db_db_query($sql);

# Retrieve UserID
$sql = "SELECT UserID, EnglishName, ChineseName, ClassName, ClassNumber FROM INTRANET_USER WHERE CardID = '$CardID' AND (RecordType = 1 OR RecordType = 2) AND RecordStatus = 1";
$temp = $lpayment->returnArray($sql,5);
list($uid, $t_engName, $t_chiName, $t_className, $t_classNum) = $temp[0];
if ($uid == "" || $uid == 0)
{
    echo -1;
}
else
{
    if ($refund == 1)
    {
        # Retrieve original log record
        $sql = "SELECT RecordID, RecordType, QuotaChange, QuotaAfter FROM PAYMENT_PRINTING_QUOTA_CHANGE_LOG
                       WHERE UserID = '$uid' AND RecordType = '$changeLogType_ByAPI' ORDER BY DateInput DESC LIMIT 0,1";
        $temp = $lpayment->returnArray($sql,4);
        list($t_targetLogID, $t_recordType, $t_targetQuotaChange, $t_targetQuotaAfter) = $temp[0];
        if ($t_targetLogID > 0)
        {
            # Check refund quota not exceed previous transaction
            if ($quota > $t_targetQuotaChange)
            {
                $quota = $t_targetQuotaChange;
            }
        }

        # Retrieve Current Balance
        $sql = "SELECT UsedQuota FROM PAYMENT_PRINTING_QUOTA WHERE UserID = '".IntegerSafe($uid)."'";
        $temp = $lpayment->returnVector($sql);
        $t_used = $temp[0]+0;
        if ($t_used < $quota)
        {
            $quota = $t_used;
        }

        $sql = "UPDATE PAYMENT_PRINTING_QUOTA SET UsedQuota = UsedQuota - '$quota', DateModified = NOW() WHERE UserID = '".IntegerSafe($uid)."'";
		if($usedQuota > 0 )
		{
			$lpayment->db_db_query($sql);
		}
		$sql = "SELECT TotalQuota - UsedQuota FROM PAYMENT_PRINTING_QUOTA WHERE UserID = '".IntegerSafe($uid)."'";
        $temp = $lpayment->returnVector($sql);
        $quota_after = ($temp[0] < 0? 0:$temp[0]);

        # Update change log
        if ($t_targetLogID > 0)
        {
            # Check refund
            if ($quota > $t_targetQuotaChange)
            {
                $quota = $t_targetQuotaChange;
            }

            $sql = "UPDATE PAYMENT_PRINTING_QUOTA_CHANGE_LOG SET QuotaChange = QuotaChange - '$quota', QuotaAfter = QuotaAfter + '$quota'  
                           WHERE RecordID = '$t_targetLogID'";
			if($usedQuota > 0)
			{
				$lpayment->db_db_query($sql);
			}
            # Insert details
            #
            $delim = "";
            $values = "";
            if ($a4_bw != "" && $a4_bw > 0)
            {
                $values .= "$delim ($t_targetLogID, 'a4_bw', '$a4_bw', '$transaction_detail_recordtype_quota', now(), now())";
                $delim = ",";
            }
            if ($a4_color != "" && $a4_color > 0)
            {
                $values .= "$delim ($t_targetLogID, 'a4_color', '$a4_color', '$transaction_detail_recordtype_quota', now(), now())";
                $delim = ",";
            }
            if ($nona4_bw != "" && $nona4_bw > 0)
            {
                $values .= "$delim ($t_targetLogID, 'nona4_bw', '$nona4_bw', '$transaction_detail_recordtype_quota', now(), now())";
                $delim = ",";
            }
            if ($nona4_color != "" && $nona4_color > 0)
            {
                $values .= "$delim ($t_targetLogID, 'nona4_color', '$nona4_color', '$transaction_detail_recordtype_quota', now(), now())";
                $delim = ",";
            }

            if ($values != "")
            {
                $sql = "INSERT IGNORE INTO PAYMENT_COPIER_TRANSACTION_DETAIL
                               (TransactionLogID, PaperType, NumberOfPaper, RecordType, DateInput, DateModified)
                               VALUES $values";
                $lpayment->db_db_query($sql);
            }
        }

        #############################


        echo "1###".$quota_after;
        echo "###".date('Y-m-d H:i:s');
    }
    else
    {
        # Check quota enough
        $sql = "SELECT UsedQuota, TotalQuota FROM PAYMENT_PRINTING_QUOTA WHERE UserID = $uid";
        $temp = $lpayment->returnArray($sql,2);
        list($t_used, $t_total) = $temp[0];
        $t_available = $t_total - $t_used;
		
        if ($t_total == '' || $t_total <= 0 || $t_available < $quota) # Not enough quota
        {
            echo "0###".$t_available;
            echo "###".date('Y-m-d H:i:s');
        }
        else
        {
            $sql = "UPDATE PAYMENT_PRINTING_QUOTA SET UsedQuota = UsedQuota + '$quota', DateModified = NOW() WHERE UserID = '".IntegerSafe($uid)."'";
            $lpayment->db_db_query($sql);
            $quota_after = $t_available - $quota;

            # Insert into change log
            $sql = "INSERT INTO PAYMENT_PRINTING_QUOTA_CHANGE_LOG (UserID, QuotaChange, QuotaAfter, UsedQuota, TotalQuota, RecordType, DateInput, DateModified)
                           VALUES ('$uid','$quota','$quota_after','$t_used','$t_total','$changeLogType_ByAPI',now(),now())";
            $lpayment->db_db_query($sql);


            echo "1###".$quota_after;
            echo "###".date('Y-m-d H:i:s');
        }
    }



}

$sql = "UNLOCK TABLES";
$lpayment->db_db_query($sql);

# Student information
echo mb_convert_encoding("###$t_engName###$t_chiName###$t_className###$t_classNum","BIG5","UTF-8");



intranet_closedb();
?>