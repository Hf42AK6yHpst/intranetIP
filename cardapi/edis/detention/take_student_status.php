<?
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_api.php");
intranet_opendb();

header("Content-type: text/plain; charset=utf-8");

# Check IP
//if (!isIPAllowed()) exit();

$li = new libdb();
$DisAPI = new libdisciplinev12_api();

$SessionID = $_REQUEST['SessionID'];
$StudentID = $_REQUEST['StudentID'];
$TeacherID = $_REQUEST['TeacherID'];

if ($StudentID != "") {
	$ParDetentionArr ['AttendanceStatus'] = "'PRE'"; 
	$ParDetentionArr ['AttendanceTakenBy'] = $TeacherID;
	$ParDetentionArr ['AttendanceTakenDate'] = "NOW()";

	$DisAPI->updateDetentionBySessionIDAndStudentID($ParDetentionArr, $SessionID, $StudentID);
}

$StudentInfo = $DisAPI->getStudentInDetentionSessionByID($SessionID);
$ReturnStr = '<root>';
for ($i=0; $i< sizeof($StudentInfo); $i++) {
	list($StudentID,$AttendanceStatus,$CardID,$EnglishName,$ChineseName,$ClassName,$ClassNumber) = $StudentInfo[$i];
	
	$ReturnStr .= '<student id="'.$StudentID.'" Status="'.$AttendanceStatus.'" CardID="'.$CardID.'" EnglishName="'.urlencode($EnglishName).'" ChineseName="'.urlencode($ChineseName).'" ClassName="'.urlencode($ClassName).'" ClassNumber="'.$ClassNumber.'"></student>';
}
$ReturnStr .= '</root>';

echo $ReturnStr;
intranet_closedb();
die;
?>