<?
// Editing by 
/*
 * 2014-04-14 (Carlos): Use libauth->validate() to validate login user. 
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_api.php");
intranet_opendb();

header("Content-type: text/plain; charset=utf-8");

# Check IP
//if (!isIPAllowed()) exit();

$li = new libdb();
$lauth = new libauth();
$DisAPI = new libdisciplinev12_api();

$LoginName = urldecode($_REQUEST['LoginName']);
$Password = urldecode($_REQUEST['UserPassword']);

$auth_success = $lauth->validate($LoginName, $Password, '', 1);

$sql = "select 
					UserID 
				from 
					INTRANET_USER 
				where 
					UserLogin = '".$li->Get_Safe_Sql_Query($LoginName)."' 
					AND 
					RecordType = '1' 
					AND 
					RecordStatus = '1'";
//debug_r($sql);
$Temp = $li->returnVector($sql);
if ($auth_success && sizeof($Temp) > 0) {
	$SessionDetail = $DisAPI->getDetentionDetailByDetentionDate(date('Y-m-d'));
	
	$ReturnStr = '<root LoginedUserID="'.$Temp[0].'">';
	for ($i=0; $i< sizeof($SessionDetail); $i++) {
		list($DetentionID,$DetentionDate,$StartTime,$EndTime,$Location) = $SessionDetail[$i];
		
		$PICs = $DisAPI->getDetentionSessionPIC($DetentionID);
		
		$ReturnStr .= '<detentionsession id="'.$DetentionID.'" Date="'.urlencode($DetentionDate).'" StartTime="'.urlencode($StartTime).'" EndTime="'.urlencode($EndTime).'" Location="'.urlencode($Location).'">';
		for ($j=0; $j< sizeof($PICs); $j++) {
			$ReturnStr .= '<pic EnglishName="'.urlencode($PICs[$j]['EnglishName']).'" ChineseName="'.urlencode($PICs[$j]['ChineseName']).'"></pic>';
		}
		$ReturnStr .= '</detentionsession>';
	}
	$ReturnStr .= '</root>';
	echo $ReturnStr;
}
else { // login failed
	echo "loginfail";
}


intranet_closedb();
die;
?>