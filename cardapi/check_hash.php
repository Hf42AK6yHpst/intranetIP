<?
include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("../includes/libpayment.php");
include_once("functions.php");
intranet_opendb();

/*
#####
# return:
-4 : Refund amount larger than original
-3 : Invalid key
-2 : Argument Error
-1 : No this student
0 : Not enough balance
1 : Successful transaction
######
*/

/*
# Check IP
if (!isIPAllowed()) exit();

# Check session key
if (!checkSession($key)) exit();

# Check Access Control
if (!checkAccess($key,2))
{
     if ($dlang=="b5")
     {
         $msg = "你不可以進入繳費系統, 請與管理員聯絡.";
     }
     else $msg = "You cannot enter Payment System. Please contact Administrator.";
     echo "-1";
     exit();
}

$username = getUsername($key);
*/


/*
Param:
$key - Secret Key
$amount - Amount to be paid
$item - Item name to be on the transaction record
$CardID - Card ID of the student
$type - 1 (credit/refund), -1 (debit)
$refund - 1 (yes), else no
$rand - random string
*/

$keysalt = array("1sxvga12");
$delimiter = "###";

if ($apitype==1)
{
    $string = $keysalt[0].$delimiter.$quota.$delimiter.$CardID.$delimiter.$refund.$delimiter.$rand;
}
else
{
    $string = $keysalt[0].$delimiter.$amount.$delimiter.$item.$delimiter.$CardID.$delimiter.$type.$delimiter.$refund.$delimiter.$rand;
}

$hashed_key = md5($string);
echo $hashed_key;
#echo "403 Forbidden";


?>
