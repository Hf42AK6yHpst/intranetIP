<?//using by Carlos
include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("../includes/libpayment.php");
include_once("functions.php");
intranet_opendb();
$lpayment = new libpayment();

#####
# return:
# 0 if CardID no match, no need to pay
#
# Row 1: 1 - successful, 0 - not successful
# Row 2: ChineseName
# Row 3: EnglishName
# Row 4: ClassName
# Row 5: ClassNumber
# Row 6: Balance
######

# Check IP
if (!isIPAllowed()) exit();

# Check session key
if (!checkSession($key)) exit();

# Check Access Control
if (!checkAccess($key,1))
{
     if ($dlang=="b5")
     {
         $msg = "你不可以進入繳費系統, 請與管理員聯絡.";
     }
     else $msg = "You cannot enter Payment System. Please contact Administrator.";
     echo "-1";
     exit();
}

if ($CardID == "")
{
    # Card not registered
    #echo CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered'];
    echo "0";
    intranet_closedb();
    exit();
}

$sql = "SELECT UserID,ChineseName,EnglishName,ClassName,ClassNumber FROM INTRANET_USER WHERE CardID = '$CardID' AND RecordType = '2' AND RecordStatus = '1'";
$temp = $lpayment->returnArray($sql,5);
list($uid,$chiName,$engName,$classname,$classnum) = $temp[0];
if (trim($uid) == "" || trim($uid) == 0 || is_null($uid) || sizeof($temp) == 0)  # No such user
{
    echo "0";
}
else
{
    $sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '".IntegerSafe($uid)."'";
    $temp = $lpayment->returnVector($sql);
    $balance = $temp[0];
    echo mb_convert_encoding("1\n$chiName\n$engName\n$classname\n$classnum\n$balance","BIG5","UTF-8");
}
intranet_closedb();
exit();
?>
