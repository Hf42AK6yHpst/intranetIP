<?php
// Editing by 
/*
 * !!! Please note that this file encoding is 中文Big5碼 !!!
 */
/************************************************************* Change log ********************************************************************************
 * 2014-11-05 (Carlos): Modified checkSession($key), changed key file directory from /tmp to /file/eAttendanceTerminal catering cloud server sharing hosts
 *********************************************************************************************************************************************************/
$clientIP = getRemoteIpAddress();

function isIPAllowed()
{
         global $clientIP;
         global $intranet_root;
         global $dlang, $Settings;

         # Check IP
         //$ips = get_file_content("$intranet_root/file/stattend_ip.txt");
         if (is_array($Settings))
         	$ips = $Settings['TerminalIP'];
         else {
         	include_once("../includes/libgeneralsettings.php");
         	
					$GeneralSetting = new libgeneralsettings();
					$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',array("'TerminalIP'"));
         	$ips = $Settings['TerminalIP'];
        }
         $ipa = explode("\n", $ips);
         $ip_ok = false;
         foreach ($ipa AS $key => $ip)
         {
              $ip = trim($ip);
              if ($ip == "0.0.0.0")
              {
                  $ip_ok = true;
              }
              else if (testip($ip,$clientIP))
              {
                   $ip_ok = true;
              }
         }
         if (!$ip_ok)
         {
              return false;
         }
         return true;
}

# Skip this function first
function checkSession($key)
{
         global $attendst_keyfile_temp;
         global $clientIP, $TerminalID;
         global $dlang;
         global $intranet_root,$attendst_expiry;

         # Check session key
         $dir = $intranet_root."/file/eAttendanceTerminal";
         if(!file_exists($dir) || !is_dir($dir)){
         	mkdir($dir);
         }
         //$keyfilepath = "$attendst_keyfile_temp/attendst_ip_$clientIP"."_t$TerminalID";
         $keyfilepath = "$dir/attendst_ip_$clientIP"."_t$TerminalID";
         if (!is_file($keyfilepath))
         {
              $msg = ($dlang=="b5"?"作業過時, 請重新開啟本程式":"Registration Expired. Pls restart the terminal program.");
              echo $msg;
              return false;
         }
         include_once("../includes/libgeneralsettings.php");
         	
					$GeneralSetting = new libgeneralsettings();
					$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',array("'ConnectionTimeout'"));
         $temp = $Settings['ConnectionTimeout'];
         if ($temp!="" && is_numeric($temp))
         {
             $attendst_expiry = $temp;
         }
         $content = get_file_content($keyfilepath);
         $now = time();
         $data = explode("\n",$content);
         list ($ts,$oldkey) = $data;

         if ($oldkey != $key ||  ($now - $ts)> $attendst_expiry*60 )
         {
              $msg = ($dlang=="b5"?"作業過時, 請重新開啟本程式":"Registration Expired. Pls restart the terminal program.");
              echo $msg;
              return false;
         }
         return true;
}

?>
