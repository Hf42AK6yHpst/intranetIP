<?
####################################################################
# Created by : Kenneth Wong
# Creation Date : 20070709
####################################################################
# Version updates
####################################################################
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();
include_once("../attendance/attend_functions.php");
######################################################
# Param : (From QueryString)
#    Action
#          Empty / 0 : Init checking
#          1 : Enter school in morning
#          2 : Leave school
#    CardID
#    dlang
######################################################


if ($dlang != "b5") $intranet_session_language = "en";
else $intranet_session_language = "b5";

include_once("../attendance/attend_lang.php");
include_once("../attendance/attend_const.php");

# Check IP
if (!isIPAllowed())
{
     echo CARD_RESP_INVALID_IP."###".$attend_lang['InvalidIP'];
     intranet_closedb();
     exit();
}


/*
1. Select query from user table
2. Select ClassID from ClassName
3. Select from Time table mode
*/
/*
4. Create a table if not exists
*/

$year = date('Y');
$month = date('m');

$tablename = "CARD_SIMULATION_".$year."_".$month;
$sql = "CREATE TABLE IF NOT EXISTS $tablename (
 RecordID int(11) NOT NULL auto_increment,
 UserID int,
 DayNumber int,
 InSchoolTime time,
 LeaveSchoolTime time,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE UserDay (UserID, DayNumber)
)";
$db_engine->db_db_query($sql);

/*
5. Select query from Daily log table
6. Select query from timetable
7. if Action==1, insert query; if Action==2, update query

*/


echo 1;


intranet_closedb();
?>

