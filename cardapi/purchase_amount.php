<?php
/*
 * 2015-01-22 (Carlos): [ej5.0.5.2.1] Use [System Administrator] as user name if no need to login terminal.
 */
include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("../includes/libpayment.php");
include_once("functions.php");

$CurrentEncoding = mb_detect_encoding($_REQUEST['item'],'BIG-5,UTF-8');

//echo $CurrentEncoding;

if ($CurrentEncoding != "UTF-8") {
	$item = mb_convert_encoding($_REQUEST['item'],"UTF-8",$CurrentEncoding);
}

//echo mb_detect_encoding($item,$DetectOrder);
//echo $item;
//die;

intranet_opendb();
$lpayment = new libpayment();

#####
# return:
# 0 if CardID no match, no need to pay, amount not enough
# 1 if successful
######
if (!$payment_api_open_purchase)
{
     echo -1;
     exit();
}
# Check IP
if (!isIPAllowed()) exit();

# Check session key
if (!checkSession($key)) exit();

# Check Access Control
if (!checkAccess($key,2))
{
     if ($dlang=="b5")
     {
         $msg = "�A���i�H�i�Jú�O�t��, �лP�޲z���p��.";
     }
     else $msg = "You cannot enter Payment System. Please contact Administrator.";
     echo "-1###Key expired";
     exit();
}

$username = trim(getUsername($key));
if($username == ''){
	$username = $Lang['ePayment']['SystemAdmin'];
}

/*
Param:
$key - Secret Key
$amount - Amount to be paid
$item - Item name to be on the transaction record
$CardID - Card ID of the student
*/
$item = urldecode($item);

# Check Arguments
$amount = (float)$amount;
if ($CardID == "" || $amount <= 0 || $item == "")
{
    echo -2;
    exit();
}

# Lock tables
$sql = "LOCK TABLES
             PAYMENT_ACCOUNT WRITE
             , INTRANET_USER READ
             , PAYMENT_OVERALL_TRANSACTION_LOG WRITE
             ";
$lpayment->db_db_query($sql);

# Retrieve UserID
$sql = "SELECT UserID FROM INTRANET_USER WHERE CardID = '$CardID' AND (RecordType = 1 OR  RecordType = 2) AND RecordStatus = 1";
$temp = $lpayment->returnVector($sql);
$uid = $temp[0];
if ($uid == "" || $uid == 0)
{
    echo -1;
}
else
{
    # Check Amount enough
    $sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '".IntegerSafe($uid)."'";
    $temp = $lpayment->returnVector($sql);
    $balance = $temp[0]+0;

    if ($balance < $amount)         # Not enough balance
    {
        echo "0###".number_format($balance,2);
        exit();
    }
    else
    {
        # Deduct balance
        $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance - $amount
        ,LastUpdateByAdmin = NULL,LastUpdateByTerminal='$username',LastUpdated = NOW()
        WHERE StudentID = '".IntegerSafe($uid)."'";
        $lpayment->db_db_query($sql);
        # Insert Transaction Record
        $balanceAfter = $balance - $amount;
        $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
               (StudentID, TransactionType, Amount, RelatedTransactionID, BalanceAfter, TransactionTime, Details)
               VALUES
               ('".IntegerSafe($uid)."', 3,'$amount',NULL,'$balanceAfter',NOW(),'".$lpayment->Get_Safe_Sql_Query($item)."')";
        $lpayment->db_db_query($sql);
        echo "1###".number_format($balanceAfter,2);
        echo "###".date('Y-m-d H:i:s');
    }

}

$sql = "UNLOCK TABLES";
$lpayment->db_db_query($sql);



intranet_closedb();
?>
