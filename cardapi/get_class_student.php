<?php
// Editing by 
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libxml.php");

intranet_opendb();

$lang = $_REQUEST['lang'] == 'B5' || $_REQUEST['lang'] == 'GB' ? $_REQUEST['lang'] : 'EN';

$intranet_session_language = strtolower($_REQUEST['lang']);

$AcademicYearID = Get_Current_Academic_Year_ID();

$libxml = new libxml();
$fcm = new form_class_manage();

$class_list = $fcm->Get_All_Year_Class($AcademicYearID);
$class_count = count($class_list);

header("Content-type: text/xml;charset=UTF-8");

$xml = '<?xml version="1.0" encoding="UTF-8" ?>'."\n";

$xml .= '<classes>';
for($i=0;$i<$class_count;$i++) {
	
	list($class_id, $class_name) = $class_list[$i];
	
	$xml .= '<class>'."\n";
		$xml .= '<classid>'.$class_id.'</classid>'."\n";
		$xml .= '<classname>'.$libxml->HandledCharacterForXML($class_name).'</classname>'."\n";
		$xml .= '<students>'."\n";
	$student_list = $fcm->Get_Active_Student_List('',$class_id,$AcademicYearID);
	$student_count = count($student_list);
	
	for($j=0;$j<$student_count;$j++) {
		$student_name = $student_list[$j]['NameField'].($student_list[$j]['ClassNumber'] != ''? ' ('.$student_list[$j]['ClassNumber'].')':'');
		
		$xml .= '<student>'."\n";
			$xml .= '<userid>'.$student_list[$j]['UserID'].'</userid>'."\n";
			$xml .= '<studentname>'.$libxml->HandledCharacterForXML($student_name).'</studentname>'."\n";
		$xml .= '</student>'."\n";
	}
		$xml .= '</students>'."\n";
	$xml .= '</class>'."\n";
}
$xml .= '</classes>'."\n";

echo $xml;

intranet_closedb();
?>