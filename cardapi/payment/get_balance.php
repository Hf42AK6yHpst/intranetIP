<?php
// Editing by 
/*
 * Created on 2013-09-10
 * @param string $q : encrypted string with data CardID=XXXXXXXXXX
 * @return float balance : -1 - Error (i.e. CardID not found, invalid query, etc.); 
 * 						   >=0 - Balance (precise to 2 floating point number)
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_opendb();

$q = trim($_REQUEST['q']);
if($q == ""){
	echo "-1";
	intranet_closedb();
	exit;
}

$key = "0df6a4fa47dd96e3";
$query = AES_128_Decrypt($q, $key);

if($query == ""){
	echo "-1";
	intranet_closedb();
	exit;
}

parse_str($query,$args); // CardID=XXXXXXXXXX
$CardID = $args['CardID'];

$lib = new libdb();

$sql = "SELECT 
			a.Balance 
		FROM INTRANET_USER as u 
		INNER JOIN PAYMENT_ACCOUNT as a ON a.StudentID=u.UserID
		WHERE u.CardID='$CardID'";
$record = $lib->returnVector($sql);

if(count($record) == 0){
	echo "-1";
	intranet_closedb();
	exit;
}

echo sprintf("%.2f", $record[0]+0);

intranet_closedb();
?>