<?php
// Editing by 
/*
 * Created on 2013-09-10
 * @param string $q : encrypted string with data CardID=XXXXXXXXXX&Amount=-10&Token=[timestamp]&Rand=XXXXXXXXXX
 * @Return
 * 	 1 : success
 *	-1 : Invalid query
 *	-2 : CardID not found
 *	-3 : Process failure, e.g. not enough balance to deduct
 *	-4 : Request has been processed before
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_opendb();

$q = trim($_REQUEST['q']);
if($q == ""){
	echo "-1";
	intranet_closedb();
	exit;
}

$key = "0df6a4fa47dd96e3";
$query = AES_128_Decrypt($q, $key);

if($query == ""){
	echo "-1"; // Invalid query
	intranet_closedb();
	exit;
}

parse_str($query,$args);
$CardID = $args['CardID'];
$Amount = $args['Amount'];
$Token = $args['Token'];
$Rand = $args['Rand'];

if($CardID == "" || !is_numeric($Amount) || $Token == "" || $Rand == ""){
	echo "-1"; // Invalid query
	intranet_closedb();
	exit;
}

$lib = new libdb();

$sql = "SELECT 
			u.UserID, a.Balance 
		FROM INTRANET_USER as u 
		INNER JOIN PAYMENT_ACCOUNT as a ON a.StudentID=u.UserID
		WHERE u.CardID='$CardID'";
$record = $lib->returnResultSet($sql);

if(count($record)==0){
	echo "-2"; // CardID not found
	intranet_closedb();
	exit;
}

$userid = $record[0]['UserID'];
$balance = $record[0]['Balance'] + 0;

$token = $Token.$Rand;
$sql = "SELECT COUNT(*) FROM PAYMENT_API_REQUEST_LOG WHERE CardID='$CardID' AND Token='$token'";
$log_count = $lib->returnVector($sql);
if($log_count[0] > 0){
	echo "-4";
	intranet_closedb();
	exit;
}

if($Amount < 0 && ($balance + $Amount) < 0) {
	echo "-3";
	intranet_closedb();
	exit;
}

$request_detail = "process_change.php?CardID=".$CardID."&Amount=".$Amount."&Token=".$Token."&Rand=".$Rand;

# Lock tables
$sql = "LOCK TABLES
             PAYMENT_ACCOUNT WRITE
             , INTRANET_USER READ 
			 , PAYMENT_CREDIT_TRANSACTION WRITE 
             , PAYMENT_OVERALL_TRANSACTION_LOG WRITE
			 , PAYMENT_API_REQUEST_LOG WRITE 
             ";
$lib->db_db_query($sql);

$return_code = "-3";

$process_datetime = date("Y-m-d H:j:s");

if($Amount < 0) {
 	# Deduct balance
    $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $Amount,
				LastUpdateByAdmin=NULL,LastUpdateByTerminal='Admin',LastUpdated = NOW() 
			WHERE StudentID = '$userid'";
    $update_balance_success = $lib->db_db_query($sql);
    $updated_row = $lib->db_affected_rows();
    if($update_balance_success && $updated_row > 0) {
	    # Insert Transaction Record
	    $balanceAfter = $balance + $Amount;
	    $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
	                   (StudentID, TransactionType, Amount, RelatedTransactionID, BalanceAfter, TransactionTime, Details)
	                   VALUES
	                   ($userid, 3,'$Amount',NULL,'$balanceAfter','$process_datetime','Print Quota Balance Deduct')";
	    $log_success = $lib->db_db_query($sql);
    }
    
    if($update_balance_success && $log_success){
    	$return_code = "1"; // success
    }
}

if($Amount > 0) {
	# Deposit
	$sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $Amount,
				LastUpdateByAdmin=NULL,LastUpdateByTerminal='Admin',LastUpdated = NOW() 
			WHERE StudentID = '$userid'";
	
    $update_balance_success = $lib->db_db_query($sql);
    $updated_row = $lib->db_affected_rows();
    if($update_balance_success && $updated_row > 0) {
    	# Insert Credit Transaction Record
    	$sql = "INSERT INTO PAYMENT_CREDIT_TRANSACTION 
				(StudentID, Amount, RecordType, RecordStatus, TransactionTime, RefCode, DateInput) 
				VALUES ('$userid','$Amount', 2,1, '$process_datetime', 'Print Quota Balance Deposit($process_datetime)', '$process_datetime')";
    	$credit_success = $lib->db_db_query($sql);
    	$credit_transaction_id = $lib->db_insert_id();
    	
    	# Insert Transaction Record
	    $balanceAfter = $balance + $Amount;
	    $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
	                   (StudentID, TransactionType, Amount, RelatedTransactionID, BalanceAfter, TransactionTime, Details)
	                   VALUES
	                   ($userid, 1,'$Amount',NULL,'$balanceAfter','$process_datetime','Print Quota Balance Deposit($credit_transaction_id)')";
	    $log_success = $lib->db_db_query($sql);
    }
    if($update_balance_success && $credit_success && $log_success){
    	$return_code = "1"; // success
    }
}


$sql = "INSERT INTO PAYMENT_API_REQUEST_LOG (CardID,Request,Token,DateInput) VALUES ('$CardID','$request_detail','$token','$process_datetime')";
$lib->db_db_query($sql);

$sql = "UNLOCK TABLES";
$lib->db_db_query($sql);

echo $return_code;

intranet_closedb();
?>