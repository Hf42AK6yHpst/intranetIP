<?php
/*
 * Created on 2013-09-10
 * return the global maximum quota setting
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");

if(!isset($sys_custom['ePayment_API_MaxQuota']))
{
	$sys_custom['ePayment_API_MaxQuota'] = 50;
}
echo $sys_custom['ePayment_API_MaxQuota'];

?>