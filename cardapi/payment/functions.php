<?
include_once('../../includes/libgeneralsettings.php');

$clientIP = getRemoteIpAddress();

/*$payment_authfile = get_file_content("$intranet_root/file/payment_auth.txt");
$payment_auth = explode("\n",$payment_authfile);
#$payment_st_expiry = trim(get_file_content("$intranet_root/file/payment_expiry.txt"));*/

function isIPAllowed()
{
	global $clientIP;
	global $intranet_root;
	global $dlang;
	
	# Check IP
	//$ips = get_file_content("$intranet_root/file/payment_ip.txt");
	$GeneralSetting = new libgeneralsettings();
	
	$Setting = $GeneralSetting->Get_General_Setting('ePayment',array("'TerminalIPList'"));
	$ipa = explode("\n", $Setting['TerminalIPList']);
	$ip_ok = false;
	foreach ($ipa AS $ip)
	{
	    $ip = trim($ip);
	    if ($ip == "0.0.0.0")
	    {
	        $ip_ok = true;
	    }
	    else if (testip($ip,$clientIP))
	    {
	         $ip_ok = true;
	    }
	}
	
	if (!$ip_ok)
	{
	    $msg = ($dlang=="b5"?"此終端機之 IP 位址並不可以登入系統":"Invalid IP address. Action cancelled.");
	    #echo "$msg";
	    echo "-1";
	    return false;
	}
	return true;
}

function isPaymentNoLogin()
{
	$GeneralSetting = new libgeneralsettings();
	
	$Setting = $GeneralSetting->Get_General_Setting('ePayment',array("'PaymentNoAuth'"));
	//global $payment_auth;
	return ($Setting['PaymentNoAuth']==1);
}
function isPurchaseNoLogin()
{
  $GeneralSetting = new libgeneralsettings();
	
	$Setting = $GeneralSetting->Get_General_Setting('ePayment',array("'PurchaseNoAuth'"));
	//global $payment_auth;
	return ($Setting['PurchaseNoAuth']==1);
}

?>