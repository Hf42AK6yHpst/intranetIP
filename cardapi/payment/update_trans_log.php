<?php
/*
 * 	Log
 * 	Date:	2014-01-08 [Cameron] Remove encryption
 * 	Date:	2013-11-20 [Cameron] Force to update PAYMENT_OVERALL_TRANSACTION_LOG and PAYMENT_ACCOUNT for each api call
 * 	Date:	2013-11-15 [Cameron] Add calling function: Process_Printer_Transactions()
 * 	Date:	2013-11-11 [Cameron] File to update transacton log from Ricoh to eClass regarding Payment Module
 */
 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
 
intranet_opendb();

$q = trim($_POST["q"]);
if($q == ""){
	echo "-1";
	intranet_closedb();
	exit;
}

//error_log("\n\nget q -->".print_r($q,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
 
//$decryptedData = AES_128_Decrypt($q, $ricoh2_aes_key);
$decryptedData = $q;		// No enctyption
if($decryptedData == ""){
	echo "-1";
	intranet_closedb();
	exit;
}

$jsonContent = stripslashes($decryptedData);
$objJson = new JSON_obj();
$jsonArray = $objJson->decode($jsonContent);
error_log("\n\ngetRicoh api array -->".print_r($jsonContent,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
 //error_log("\n\ngetRicoh api array -->".print_r($jsonArray,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
 
$error = array();
$nrRecord = 0;	// number of record in this transaction 
$lib = new libdb();
$lib->Start_Trans();
		
if (count($jsonArray) > 0)
{
	$batchID = "";

	// 1. Get BatchID
	$sql = "SELECT IFNULL(MAX(BatchID)+1,1) as BatchID FROM RICOH_TRANS_DETAILS";
	$rs = $lib->returnArray($sql);
//error_log("\n\nlast trans sql -->". $sql."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");		
	if (count($rs) == 1)
	{
		$batchID = $rs[0]["BatchID"];	
	}
	else
	{
		$error[] = "Get Batch ID error";
	}
	
	if ($batchID)
	{
		// 2. Insert Trans Log Details
		foreach($jsonArray as $k => $v)
		{
			$sql = "INSERT INTO RICOH_TRANS_DETAILS (BatchID, LogDate, SeqNo, LogType, BWA3Cost, BWA4Cost, ColorA3Cost, ColorA4Cost, MonoSides, ColorSides, RicohUserID, PaperSizeCode, PaperSize, CardNo, Amount, ApiDate) 
					VALUES ($batchID,'" . $lib->Get_Safe_Sql_Query($v["LogDate"]) . "',
							" . $v["SeqNo"] . ",
							'" . $lib->Get_Safe_Sql_Query($v["LogType"]) . "',
							" . $v["BWA3Cost"] . ",
							" . $v["BWA4Cost"] . ",
							" . $v["ColorA3Cost"] . ",
							" . $v["ColorA4Cost"] . ",
							" . $v["MonoSides"] . ",
							" . $v["ColorSides"] . ",
							'" . $lib->Get_Safe_Sql_Query($v["UserID"]) . "',
							'" . $lib->Get_Safe_Sql_Query($v["PaperSizeCode"]) . "',
							'" . $lib->Get_Safe_Sql_Query($v["PaperSize"]) . "',
							'" . $lib->Get_Safe_Sql_Query($v["CardNo"]) . "',
							" . $v["Amount"] . ",NOW())";
			$result = $lib->db_db_query($sql);
error_log("\n\ntrans details sql -->". $sql."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");			
			if (!$result)
			{
				$error[] = array("Insert Trans Log Details Error",$v);
				error_log("\n\nInsert Trans Log Details Error -->".var_export($v,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ricoh2_error.txt");			
			}
			else
			{
				$nrRecord++;
			}
		}
		
		// 3. Check if CardNo in IP25.INTRANET_USER table
		$sql = "SELECT r.CardNo FROM RICOH_TRANS_DETAILS r LEFT JOIN INTRANET_USER u ON r.CardNo=u.CardID 
					WHERE u.CardID IS NULL";
		$rs = $lib->returnArray($sql);
		if (count($rs) > 0)
		{
			// Note!!!
			// Don't set error here so the the program will ignore those record which CardID exists in Ricoh but not in IP 
//			$error[] = "CardNo not found in eClass error " . var_export($rs,true);
			error_log("\n\nCardNo not found in eClass Error -->".var_export($rs,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ricoh2_error.txt");	
		}
		
		// 4. Insert summary table for each user of Trans Log Details for a batch
		// Note: CardNo may not be unique for a User in case his/her card is lost and need replacement
		// Group by CardNo to ignore those records which CardID exists in Ricoh but not in IP
		$sql = "INSERT INTO RICOH_TRANS_SUMMARY (BatchID, RicohUserID, CardNo, Amount, ApiDate, IPUserID)
					SELECT $batchID, MAX(r.RicohUserID), r.CardNo, SUM(Amount), NOW(), u.UserID FROM RICOH_TRANS_DETAILS r
						LEFT JOIN INTRANET_USER u ON r.CardNo=u.CardID 
						WHERE BatchID=$batchID GROUP BY r.CardNo";	
		$result = $lib->db_db_query($sql);
//error_log("\n\ntrans summary sql -->". $sql."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");		
		if (!$result)
		{
			$error[] = array("Insert Trans Log Summary Error",$batchID);
			error_log("\n\nInsert Trans Log Summary Error: $sql -->".var_export($batchID,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ricoh2_error.txt");			
		}
		
	}	// $batchID not empty
		

}	// count($jsonArray) > 0
//else
//{
//	echo "-1";
//	intranet_closedb();
//	exit;
//} 
	// Force to update PAYMENT_OVERALL_TRANSACTION_LOG and PAYMENT_ACCOUNT for each api call		
	// 5. Update Payment on eClass
	$payment = new libpayment();
	$payment_result = $payment->Process_Printer_Transactions();
	if (!$payment_result)
	{
		$error[] = array("Update eClass Payment Error",$batchID);
		error_log("\n\nUpdate eClass Payment Error -->".var_export($batchID,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ricoh2_error.txt");			
	}

	if (count($error) == 0 )
	{		
		$lib->Commit_Trans();
		$result = "Result >>> Transaction Date = " . date("Y-m-d H:i:s") . ", BatchID = " . $batchID . ", Number of record added in this transaction = " . $nrRecord;
//error_log("\n\nCommit trans-->".$result."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
		echo sprintf("%s", $result);
	}
	else
	{
		$lib->RollBack_Trans();
		error_log("\n\nRollback trans -->".var_export($error,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ricoh2_error.txt");		
		echo sprintf("%s", var_export($error) );		
	}
 
intranet_closedb(); 
?>