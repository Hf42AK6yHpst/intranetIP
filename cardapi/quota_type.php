<? 
/*
#modified by Simon
Payment: 1
Quota: 2
# Development: Gary
# Modified Date: 7 May 2010
#########
# Return:
-3 : No privilege to access this page
-2 : Argument Error
-1 : No this student
0 : Invalid record type
1 : Quota
2 : Payment
########
ChangeLog:
*/


// Import libraries
include_once("../includes/global.php");
include_once("../includes/settings.php");
include_once("../includes/libdb.php");


include("../rifolder/global.php"); // Import RICOH system variables
#include("functions.php"); // Import RICOH functions libraries

// Define variables
$cardID = $_REQUEST["cardID"];
$MFP_incomingIP = getRemoteIpAddress();
$MFP_incomingString = $_REQUEST["key"];
$Random_String = $_REQUEST["rand"];

//echo $rand;

/*###########################################################*/
/* 
 * Remark on $plugin['payment_printing_quota'] & $plugin['student_payment_printing_quota']
 * 
 * TRUE : print by PAYMENT
 * FALSE : print by QUOTA
 * 
 * */
 
//$plugin['payment_printing_quota'] = false; 
//$plugin['student_payment_printing_quota'] = true;
/*###########################################################*/


// Check input parameters format
if ( $cardID == "" || $MFP_incomingString == "") {
    echo -2;	//Argument Error
    exit();
}

//echo "--" .  $CardID . "--" . $MFP_incomingString . "--";
//echo $MFP_incomingString;
//echo md5($MFP_incomingString) . "<BR>";

//$s = "Ke4D7ahd8h4###0009554592###20111800";

// Check MFP can use API or not
$Allow_MFP = FALSE;
$delimiter = "###";

  

foreach (array_keys($PrinterIPLincense) as $v) {
	if (trim($v) == trim($MFP_incomingIP)) {
		$string = $PrinterIPLincense[$MFP_incomingIP].$delimiter.$cardID.$delimiter.$Random_String;
//		echo $string."<BR>";
//		echo "<BR>" . md5($string) ." == " . trim($MFP_incomingString);
                if (md5($string) == trim($MFP_incomingString))
                	$Allow_MFP = TRUE;
	}
}



if ( $Allow_MFP == TRUE ) {
	//echo "MFP Connection [pass] <BR>";

	// Connect Database and retrive iFolder location
	intranet_opendb();
	$li = new libdb();
	$sql = "SELECT UserID, UserLogin, CardID, RecordType FROM INTRANET_USER WHERE CardID = '$cardID'";
	$SubjectArray = $li->returnArray($sql,6);

	if (sizeof($SubjectArray)>0) {

		list($client_u_id, $client_login, $CardID, $RecordType )=$SubjectArray[0];
		//echo "Records: $client_u_id, $client_login, $CardID, $RecordType <BR>";

		if ( $RecordType == 1 ) {	// Teacher Type
			if ( $plugin['payment_printing_quota'] == TRUE ) {
				//echo "Teacher -> Payment";
				echo 1;	// Payment
			} else {
				//echo "Teacher -> Quota";
				echo 2; //Quota
			}
		} else if ( $RecordType == 2 ) {	// Student Type RecordType == 2
        	        if ( $plugin['student_payment_printing_quota'] == TRUE ) {
				//echo "Student -> Payment";
				echo 1;	//Payment
	                } else {
				//echo "Student -> Quota";
				echo 2; //Quota
                	}
	 	} else {
                                echo 0; // Invalid record type
				exit();
		}
	} else {
		//echo "No record found!!";
		echo -1; //No this student
		exit();
	}
	intranet_closedb();
//	include_once("/home/sites/office/ricoh.broadlearner.com/intranetIP/templates/filefooter.php");

} else {
	//echo "No privilege to access this page";
	echo -3;    //No privilege to access this page
	exit();
}


?>