<?php
# using: 
/*
 * 2019-06-04 (Carlos) : Added flag $sys_custom['LoginForceCsrfTokenChecking'] to force check CSRF token even client site is using customized login page.
 * 2017-06-22 (Carlos) : $sys_custom['DHL'] set email error msg code for DHL.
 * 2017-01-04 (Carlos) : Determine $url_success and $url_fail in this page, do not take the values passed by the login page which may changed to perform malicious attack.
 * 2016-08-08 (Carlos) : Ensure redirected url must point to local website, external url is not allowed. 
 * 2015-09-02 (Carlos) : [ip2.5.6.10.1.0]: exclude checking login token if use customized login page /templates/login.customized.php.
 * 2015-07-16 (Carlos) : [ip2.5.6.7.1.0]: added secure token checking to prevent CSRF.
 */
include("includes/global.php");
include("includes/libdb.php");
include("includes/libauth.php");
include("includes/libemail.php");
include("includes/libsendmail.php");
include("includes/libldap.php");
include("includes/libeclass.php");
include("includes/libwebmail.php");
include("includes/libftp.php");
include("includes/libuser.php");
include("includes/SecureToken.php");
// include("lang/email.php");
intranet_opendb();

$is_from_different_domain = !strpos($_SERVER['HTTP_REFERER'], $_SERVER['SERVER_NAME']);
$is_customized_login = is_file($intranet_root.'/templates/login.customized.php') || strpos($_SERVER['HTTP_REFERER'],'login.customized.php') || $is_from_different_domain;
if($sys_custom['LoginForceCsrfTokenChecking']){
	$is_customized_login = false;
}
if($is_customized_login===false || ($is_customized_login!==false && isset($_REQUEST['securetoken'])))
{
	$SecureToken = new SecureToken();
	if(!$SecureToken->CheckToken($securetoken)){
		intranet_closedb();
		session_destroy();
		header("Location: /");
		exit();
	}
}

$url_success = $is_customized_login && !$is_from_different_domain? '/templates/login.customized.php?msg=1' : '/templates/index.php?msg=1';
$url_fail = $is_customized_login && !$is_from_different_domain? '/templates/login.customized.php?err=2' : '/templates/index.php?err=2';

$UserLogin = htmlspecialchars(trim($UserLogin));
$li = new libauth();
$UserInfo = $li->forgetPassword2($UserLogin);

if($UserInfo==0){
     $url = ($url_fail=="") ? "/" : $url_fail;
}else{
	
     # Send Email
     $t_UserID = $UserInfo[0];
     $UserLogin = $UserInfo[1];
     $UserPassword = $UserInfo[2];
     $UserEmail = $UserInfo[3];
     $EnglishName = $UserInfo[4];
     $ChineseName = $UserInfo[5];

     # Hash function
     if ($intranet_authentication_method == "HASH" )
     {
//         # Re-generate Password
//         $t_new_password = intranet_random_passwd(8);
//         $sql = "UPDATE INTRANET_USER SET HashedPass = MD5('$UserLogin$t_new_password$intranet_password_salt') WHERE UserID = $t_UserID";
//         $li->db_db_query($sql);
//         $UserPassword = $t_new_password;
//
//         # Password changed
//         # Reset other system password
//         #$lc = new libeclass();
//         #$lc->eClassUserUpdatePassword($UserEmail,$UserPassword);
//		 
//		 # Update two-way hashed/encrypted password
//		 $li->UpdateEncryptedPassword($t_UserID, $UserPassword);
//		 
//         $lwebmail = new libwebmail();
//         if ($lwebmail->has_webmail)
//             $lwebmail->change_password($UserLogin,$UserPassword);
//
//         # FTP management
//         if ($plugin['personalfile'])
//         {
//             $lftp = new libftp();
//             if ($lftp->isFTP)
//             {
//                 $lftp->changePassword($UserLogin,$UserPassword);
//             }
//         }

	     # Call sendmail function
		$luser = new libuser();
		$Key = $li->CreateResetPasswordKey($UserLogin);
		list($mailSubject,$mailBody) = $luser->returnEmailNotificationData_HashedPw($UserEmail, $UserLogin, $EnglishName, $Key);
	     $mailTo = $UserEmail;
	     $webmaster = get_webmaster();
	     $lu = new libsendmail();
	
	     $result = $lu->send_email($mailTo, $mailSubject, $mailBody,"");
	     
//	     # Send ACK Email
//		list($ackMailSubject,$ackMailBody) = $luser->returnEmailNotificationData_HashedPw_ACK($UserEmail, $UserLogin);
//		
//		$result2 = $lu->send_email($webmaster, $ackMailSubject,$ackMailBody,"");
     }
	else
	{
	     # Call sendmail function
		$luser = new libuser();
		list($mailSubject,$mailBody) = $luser->returnEmailNotificationData($UserEmail, $UserLogin, $UserPassword, $EnglishName);
	     $mailTo = $UserEmail;
	     $webmaster = get_webmaster();
	     $lu = new libsendmail();
	
	     $result = $lu->send_email($mailTo, $mailSubject, $mailBody,"");
	     
	     # Send ACK Email
		list($ackMailSubject,$ackMailBody) = $luser->returnEmailNotificationData_ACK($UserEmail, $UserLogin, $UserPassword, $EnglishName);
		
		$result2 = $lu->send_email($webmaster, $ackMailSubject,$ackMailBody,"");
	}

	if($sys_custom['DHL']){
		if($UserEmail != ''){
			$email = substr($UserEmail,strpos($UserEmail,'@')+1);
			$url_success .= "&email=".$email;
		}else{
			$url_success = str_replace('msg=1','msg=3',$url_success);
		}
	}
	
    $url = $url_success;
	
}

# if redirect back to customized login page, do not restrict it to local relative link because some customized page use http:// absolute link
if(strpos($url,$_SERVER['SERVER_NAME']) === false)
{
	if($url[0] != '/') $url = '/'.$url;
}

intranet_closedb();
$UserID = "";
$t_UserID = "";
session_destroy();
header("Location: $url");
?>