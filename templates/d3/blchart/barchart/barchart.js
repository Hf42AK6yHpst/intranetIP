//Using: Pun
/**
 * ****** Settings ****** 
 * container (string): The barchart's container for d3.select(). Default: 'body'
 * data (object): The data to display. Default: {}
 * chartTitle (string): The title of the chart . Default: ''
 * 
 * width (int): The width of the graph. Default: $(container).width() 
 * height (int): The height of the graph. Default: $(container).height()
 * margin (object): The margin of the graph. Default: {top: 30, right: 60, bottom: 30, left: 60}
 * 
 * xAxis (object): The x-axis settings. Default: {}
 * 		xAxis.items (array): The array of items display in xAxis. Default: []
 * 		xAxis.itemsGroup (array): The array of items for each group. Default: []
 * 
 * yAxis (object): The x-axis settings. Default: {}
 * 		yAxis.min (int): The min value of yAxis. Default: 0
 * 		yAxis.max (int): The max value of yAxis. Default: 100
 * 		yAxis.title (string): The title of yAxis. Default: ''
 * 
 * bar (object): The bar settings. Default: {}
 * 		bar.color (Ordinal Scales): The color of the bar. Default: d3.scale.category20()
 * 
 * legend (object): The legend of the chart. Default: {}
 * 		legend.show (bool): The visibility of legend for the chart. Default: true 
 */

/**
 * Dependency:
 * 	d3.js, jQuery
 * Usage:
	<script src="<?=$PATH_WRT_ROOT?>/templates/d3/jquery/jquery-1.3.2.min.js"></script>
	<script src="<?=$PATH_WRT_ROOT?>/templates/d3/d3_v3_5_16.min.js"></script>
	<script src="<?=$PATH_WRT_ROOT?>/templates/d3/blchart/barchart/barchart.js"></script>
	<link href="<?=$PATH_WRT_ROOT?>/templates/d3/blchart/barchart/barchart_basic.css" rel="stylesheet" type="text/css">
 */

/**
 * Change Log:
 * 2016-02-26 Pun:
 *  - File Create
 */


(function( window, undefined ) {
	var barchart = {};
	var chart;
	

barchart.init = function(settings){
	var _settings = {};
	
	_settings.container = settings.container || 'body';
	_settings.data = settings.data || {};
	_settings.margin = settings.margin || {'top': 30, 'right': 60, 'bottom': 30, 'left': 60};
	_settings.chartTitle = settings.chartTitle || '';
	
	_settings.width = 
		( settings.width || $(_settings.container).width() ) - 
		_settings.margin.left - _settings.margin.right;
	_settings.height = 
		( settings.height || $(_settings.container).height() ) - 
		_settings.margin.top - _settings.margin.bottom;


	_settings.xAxis = {
		'items': [],
		'itemGroups': []
	};
	$.extend( true, _settings.xAxis, settings.xAxis );
	
	
	_settings.yAxis = {
		'min': 0,
		'max': 100,
		'title': ''
	};
	$.extend( true, _settings.yAxis, settings.yAxis );

	
	_settings.bar = {
		'color': d3.scale.category20() // d3.scale.ordinal().range(['#cccccc'])
	};
	$.extend( true, _settings.bar, settings.bar );
	
	
	_settings.legend = {
		'show': true
	};
	$.extend( true, _settings.legend, settings.legend );
	
	
	barchart.settings = _settings;
	return this;
} // End barchart()


barchart.create = function(){
	var container = barchart.settings.container;
	var width = barchart.settings.width;
	var height = barchart.settings.height;
	var margin = barchart.settings.margin;
	var chartTitle = barchart.settings.chartTitle;

	//// Graph Basic Setup START ////
	if(typeof(barchart.chart) == 'undefined'){
		barchart.chart = d3.select(container).append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + (margin.left+10) + "," + margin.top + ")");
	}
	//// Graph Basic Setup END ////
	
	barchart.update();
	
	return this;
} // End barchart.draw()


barchart.update = function(){
	var width = barchart.settings.width;
	var height = barchart.settings.height;
	var margin = barchart.settings.margin;
	var chartTitle = barchart.settings.chartTitle;

	var svg = barchart.chart;

	//// Data Setup START ////
	var data = barchart.settings.data;
	//// Data Setup END ////
	
	
	//// Axis Setup START ////
	// X-Axis START //
	var x0 = d3.scale.ordinal()
		.rangeRoundBands([0, width], .1);
	var x1 = d3.scale.ordinal();
	
	var items = barchart.settings.xAxis.items;
	var itemGroups = barchart.settings.xAxis.itemGroups;
	
	var xAxis = d3.svg.axis()
		.scale(x0)
		.orient("bottom")
		.tickFormat(function(d,i){
			return items[i];
		});

	var itemIds = [];
	d3.map(data).forEach(function(i,d){
		itemIds.push(d.item);
	});
	
	x0.domain(itemIds);
	x1.domain(itemGroups).rangeRoundBands([0, x0.rangeBand()]);
	// X-Axis END //

	// Y-Axis START //
	var y = d3.scale.linear()
		.range([height, 0]);
	var yAxis = d3.svg.axis()
		.scale(y)
		.orient("left")
		/*.tickSize(-width,0)*/;
	
	var yTitle = barchart.settings.yAxis.title;
	
	y.domain([barchart.settings.yAxis.min, barchart.settings.yAxis.max]);
	// Y-Axis END //
	//// Axis Setup END ////

	
	//// Bar Color Setup START ////
	var color = barchart.settings.bar.color;
	//// Bar Color Setup END ////


	//// Chart START ////
	// Title START //
	svg.selectAll('.chartTitle').remove();
	svg.append("text")
		.attr("class", "chartTitle")
		.attr("x", width/2)
		.text(chartTitle)
	    .style("text-anchor", "end");
	// Title END //
	
	
	// Axis START //
	svg.selectAll('.x.axis').remove();
	svg.append("g")
		.attr("class", "x axis")
		.attr("transform", "translate(0," + height + ")") // Move the axis to the bottom
		.call(xAxis)
	    .selectAll(".tick text")
	      .call(wrap, x0.rangeBand());
	
	svg.select('.xLine').remove();
	svg.select('.x.axis')
		.append('line')
		.attr('class', 'xLine')
		.attr('x1', 0)
		.attr('y1', 0)
		.attr('x2', width)
		.attr('y2', 0);

	svg.selectAll('.y.axis').remove();
	svg.append("g")
		.attr("class", "y axis")
		.call(yAxis);
	svg.select(".y.axis") // Y-axis text
		.append("text")
		.attr("transform", "rotate(-90)")
		.attr("y", -50)
		.attr("x", -height/2+20)
		.attr("dy", "1em")
		.style("text-anchor", "end")
		.text(yTitle);
	// Axis END //
	
	// Bar START //
	var bargroup = svg.selectAll(".bargroup")
		.data(data)
		.enter().append("g")
		.attr("class", "bargroup")
		.attr("transform", function(d) { return "translate(" + x0(d.item) + ",0)"; });
	
	bargroup.selectAll("rect")
		.data(function(d) { return d.values; })
		.enter().append("rect")
		.attr("width", x1.rangeBand())
		.attr("x", function(d) { return x1(d.itemGroup); })
		.attr("y", function(d) { return y(d.value); })
		.attr("height", function(d) { return height - y(d.value); })
		.style("fill", function(d,i) { return color(i);	});
	
	bargroup.selectAll("text")
		.data(function(d) { return d.values; })
		.enter()
		.append("text")
		.text(function(d) { return d.value; })
		.attr("x", function(d) { return x1(d.itemGroup) + (x1.rangeBand() / 2); })
		.attr("y", function(d) { return y(d.value)-5; })
		.style("text-anchor", "middle");
	// Bar End //
	
	// Legend START //
	legend = svg.selectAll(".legend").remove();
	if(barchart.settings.legend.show){
		var legend = svg.selectAll(".legend")
		    .data(itemGroups)
			.enter().append("g")
		    .attr("class", "legend")
		    .attr("transform", function(d, i) { return "translate(40," + i * 20 + ")"; });
		
		legend.append("rect")
		    .attr("x", width)
		    .attr("width", 18)
		    .attr("height", 18)
		    .style("fill", color);
		
		legend.append("text")
		    .attr("x", width - 18)
		    .attr("y", 9)
		    .attr("dy", ".35em")
		    .style("text-anchor", "end")
		    .text(function(d) { return d; });
	}
	//Legend END //
	//// Chart END ////

	function wrap(text, width) {
		text.each(function() {
			var text = d3.select(this),
				words = text.text().split(/\s+/  /* /<br\s*\/?>/ */).reverse(),
				word,
				line = [],
				lineNumber = 0,
				lineHeight = 1.1, // ems
				y = text.attr("y"),
				dy = parseFloat(text.attr("dy")),
				tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
			while (word = words.pop()) {
				line.push(word);
				tspan.text(line.join(" "));
				if (tspan.node().getComputedTextLength() > width) {
					line.pop();
					tspan.text(line.join(" "));
					line = [word];
					tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
				}
			}
		});
	}
	
	this.chart = svg;
	return this;
} // End barchart.update()

    window.blchart = window.blchart || {};
    window.blchart.barchart = barchart;
})( window );