//modifying by:
/**
 * Log:
/*	2017-10-30 (Pun) [ip.2.5.9.1.1] [129499]
 * 		- Added chrome use <audio> for the player
 * 	2010-03-05 Sandy:
 * 		TTS_PlayButton() - add "playerType" and "showAllButton", if  playerType = 3, display "Play All" button ONLY
 *		TTS_Player()	 - add "playerType" to switch between the types of Player to use and Where to append the SWF player.
 *		CommitChange() 	 - Re-generate (Play) player after applying setting changes 
 */

var isFromFrame = false;
var fromFrameName = "";
var relative_path = "";
var player_type_int = 1;


var browserIsChrome = (/Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor));
var browserIsSafari = (/Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor));
var browserIsEdge = (/Edge/i.test(navigator.userAgent));


//query server to store the wordings, layout etc.
function TTS_Settings()
{	
	this.settingsString = "";
	this.layout = "2009a";
	this.txtSpeechSetting = "Speech Setting";
	this.txtFullText = "Full Text";
	this.txtFullPage = "Full Page";
	this.txtHighlighted = "Highlighted";
	this.txtHighlightedText = "HighlightedText";
	this.txtVoice = "Voice";
	this.txtMale = "Male";
	this.txtFemale = "Female";
	this.txtSpeed = "Speed";
	this.txtNormal = "Normal";
	this.txtSlow = "Slow";
	this.txtSlowest = "Slowest";
	this.txtApply = "Apply";
	this.txtCancel = "Cancel";
	this.txtNoHighlightError = "Please select text.";
	this.txtNoChineseMaleVoice = "Male voice is not available in Chinese. Female voice will be used.";
	this.excludeChinese = new Array();
	this.isFromFrame = false;
	this.fromFrameName = "";
	this.fromPage="IP";
	this.txtExpired = "The subscription of your PowerSpeech add-on has been expired. Please contact your system administrator to re-new the license.";
	
	
	
	this.IsInitialized = function(){
		return this.settingsString != "";
	}
	
	//assign the value from the ajax call in this.Initialize()
	this.CopyValue = function(t){		
		this.settingsString = t.settingsString; 
		this.layout = t.layout
		this.txtSpeechSetting = t.txtSpeechSetting; 
		this.txtFullText = t.txtFullText;
		this.txtFullPage = t.txtFullPage;
		this.txtHighlighted = t.txtHighlighted;
		this.txtHighlightedText = t.txtHighlightedText;
		this.txtVoice = t.txtVoice;
		this.txtMale = t.txtMale;
		this.txtFemale = t.txtFemale;
		this.txtSpeed = t.txtSpeed;
		this.txtNormal = t.txtNormal;
		this.txtSlow = t.txtSlow;
		this.txtSlowest = t.txtSlowest;
		this.txtApply = t.txtApply;
		this.txtCancel = t.txtCancel;
		this.txtNoHighlightError = t.txtNoHighlightError;
		this.txtNoChineseMaleVoice = t.txtNoChineseMaleVoice;
		this.excludeChinese = t.excludeChinese;
		this.txtExpired = t.txtExpired;
	}
	
	this.Initialize = function(){
		
		$.ajax({
		    type: "GET",
		    url: TTS_urlRelativePath()+"includes/texttospeech/text_to_speech_settings.php",
		    dataType: "json",
		    success: function(data) {
				TTS_tempSettingsString = data.TTSSettings;
			},			
		    async: false
		});

		this.CopyValue(TTS_tempSettingsString);		
	}
}

function stripHTML(str)
{
	var regexp = /<("[^"]*"|'[^']*'|[^'">])*>/gi; 
	var regexpBr = /<\/p>|<\/div>/gi;	 
	//return str.replace(/&nbsp;/g,'').replace(/\r|\n|<(BR|\s\/)>/g,"  .   ").replace(regexp,"");
	
	return str.replace(/&nbsp;/g,'').replace(/\r|<(BR|\s\/)>/gi,"  .   ").replace(regexp,"");
}

//TTS_Player class to hold the player settings
function TTS_Player(t, d, l, pt,e )
{
	if(TTS_Settings.isFromFrame){
		this.fullText = parent.frames[TTS_Settings.fromFrameName].document.body.innerHTML;
	
	}else{	
		//this.fullText = $("#"+t).text();  	//full text of the div to be read
		this.fullText = document.getElementById(t).innerHTML;  	//full text of the div to be read		
	}
	
	
	
	this.playText = '';					//the text to be read, may be full text or highlight text
	this.target = t;					//id of the target div, uses as the id in many places
	this.divPlayer = d;					//id of the player's div 
	this.lang = l;						//the language, c=Chinese, e=English
	this.voice = 2;						//voice, 1=male, 2=female
	this.speed = 3;						//speed, 1=normal, 2=slower, 3=slowest
	this.selectedVoice = 2;				//temp storage of the selected voice in the settings layer
	this.selectedSpeed = 3;				//temp storage of the selected speed in the settings layer
	this.defaultEngVoice = 1;			//the default english voice
	this.setSettings = false;			//record if the user has set the settings
	this.player_type = (!isNaN(pt))? pt : 1;
	this.expired = e;
	
	this.ResetSelect = function(){
		this.selectedVoice = this.voice;
		this.selectedSpeed = this.speed;
	}
	this.CommitChange = function(){
		this.setSettings = true;
		this.voice = this.selectedVoice;
		this.speed = this.selectedSpeed;
				
		//2010-03-05 : Re-generate Player after applied settings
		if(this.playText == ""){			
			//this.playText = document.getElementById(this.target).innerHTML;  	//Default : full text of the div to be read
			if(TTS_Settings.isFromFrame){
				this.playText = parent.frames[TTS_Settings.fromFrameName].document.body.innerHTML;			
			}else{	
				//this.fullText = $("#"+t).text();  	//full text of the div to be read
				this.playText = document.getElementById(this.target).innerHTML;  	//full text of the div to be read		
			}
		}
		
		TTS_Play(this.target,2);
	}
	this.CheckChinese = function(settings){
		var foundChinese = -1

		for (var i = 0; i < this.playText.length && (foundChinese == -1); ++i)
		{
			//foundChinese = (this.playText.charCodeAt(i) > 255 && settings.excludeChinese.indexOf(this.playText.charAt(i)) < 0)? i : foundChinese;
			foundChinese = (this.playText.charCodeAt(i) > 255 )? i : foundChinese;
		}
		
		return (foundChinese > -1);
	}
	this.Play = function(highlight){
	
		if(this.expired > 0){
			alert(TTS_Settings.txtExpired);			
		}
			
		
		if (highlight == 1) //find the highlighted text
		{
			
			if(TTS_Settings.isFromFrame){
				// Get highlighted text from iframe
				var frameObj = parent.frames[TTS_Settings.fromFrameName];
				
				if (frameObj.getSelection)
			   	{
			        this.playText = frameObj.getSelection().toString();
				}
				else if (frameObj.document.getSelection)
			   	{					
			       	this.playText = frameObj.document.getSelection();
				}
			   	else if (frameObj.document.selection)
			   	{
			       	this.playText = frameObj.document.selection.createRange().text;
				}
			}else{
				//Get highlighted text from same page
				if (window.getSelection)
			   	{
			        this.playText = window.getSelection().toString();
				}
				else if (document.getSelection)
			   	{
			       	this.playText = document.getSelection();
				}
			   	else if (document.selection)
			   	{
			       	this.playText = document.selection.createRange().text;
				}
			}
		}
		else if(highlight == 0)
		{			
			this.playText = this.fullText; 
		}else{
			//Do nothing
		}
		
		// Put text in tmp container to strip away Equation editor text
		if((this.playText).indexOf('E0Q0U0A0T0I0O0N')>0){
			
			$('body').append("<div id='tmp_tts' style='display:none;'></div>");
			$('#tmp_tts').html(this.playText);
			$("table#E0Q0U0A0T0I0O0N").remove();		
			this.playText = $('#tmp_tts').html();
			$('#tmp_tts').remove();
		}
			
		this.playText = (this.playText).replace(/’/gi, "'").replace(/”/gi, '\\"').replace(/—/gi,"-").replace(/「/gi," . ").replace(/」/gi, " . ").replace(/–/gi, "-").replace(/<\/td>/gi, " . ");
		
		// Edited: Strip away HTML + add fullstop so that the reader will know the "pause" points
		
		this.playText = stripHTML(this.playText);
	

		if ((this.playText).replace(/\./gi, "") == "")
		{
			alert(TTS_Settings.txtNoHighlightError);
			return; //do nothing
		}
		else //post the request to the TTS server
		{
			//check if contain Chinese character and use Male voice
			
			if (this.CheckChinese(TTS_Settings) && this.voice == 1)
			{
				alert(TTS_Settings.txtNoChineseMaleVoice);
				this.voice = 2;
			}
			
			var divPlayer = this.divPlayer;
			
			// Default SWF settings - Default ttsPlayerType : 1
			var swf_link = "includes/texttospeech/powervoice_player_gowell.swf";
			var swf_width= "395";
			var swf_height = "28"
			
		
			//Get SWF settings according to the player type selected
			player_type_int = parseInt(this.player_type);
			switch(player_type_int){
			
				//Default - Blue slim + long player
				case 1: swf_link = "includes/texttospeech/powervoice_player_gowell.swf"; 
						swf_width= "395";
						swf_height = "28";
						break;
				//Small Player - Shorter width gray player
				case 2: swf_link = "includes/texttospeech/powervoice_player_gowell_ver2.swf"; 
						swf_width= "205";
						swf_height = "48";
						break;
						
				//Button Player - Play + Stop button only
				case 3: 
					
						swf_link = "includes/texttospeech/powervoice_player_gowell_simple.swf";
						swf_width= "55";
						swf_height = "30";
						
						
												
						/* Adjust DIVs so that position of the player does not shift too much*/
						
						$("#PS_Container_"+t).html('<div id="replace_container_'+t+'" style="height:30px;">&nbsp;</div>');
						$("#PS_Container_"+t).css("padding", "5px 0 0 5px").css("height", "30px");
						divPlayer = "replace_container_"+t;						
						
						$("#divPlayer_"+t).prev("div").remove();
						$("#divPlayer_"+t).remove();
						
						
						ps_container_height = (/MSIE (\d+\.\d+);/.test(navigator.userAgent))? "41px" : "35px";
						
						$("#PS_Container_"+t).css("height", ps_container_height);
							
						break;
			}

			//$.post(TTS_urlRelativePath()+"includes/texttospeech/text_to_speech.php", {inputTextLang: this.lang, inputText: this.playText, speed: this.speed, voice: this.voice },
			$.post(TTS_urlRelativePath()+"includes/texttospeech/text_to_speech.php", {inputTextLang: this.lang, inputText: this.playText, speed: this.speed, voice: (this.setSettings ? this.voice : (this.CheckChinese(TTS_Settings) ? this.voice : this.defaultEngVoice)) },
			
	 		function(data){
				if(data=="not_logged_in"){
					//window.location = TTS_urlRelativePath();
					window.location = TTS_urlRelativePath();
					
				}else if(data.charAt(0) == "0"){
		  			alert(data.substring(4));
				}else {
	   				if (richTxt_IsCompatibleBrowser() && !navigator.userAgent.match(/Android/i) && !browserIsChrome)
					{
					
						var areaFlash = document.getElementById(divPlayer);
						while(areaFlash.hasChildNodes()){
							areaFlash.removeChild(areaFlash.firstChild);
						}
			
						var flashvars = {
							SoundFile: data.substring(4),
							SoundFile2: "",
							autoStart: "1",
							playMaxTime: "999"
						};  
						var params = {
							play: "true",
							loop: "true",
							menu: "true",
							quality: "high",
							scale: "noscale",
							wmode: "transparent", 
							bgcolor: "#FFFFFF",
							allowscriptaccess: "sameDomain",
							allowfullscreen: "false"
						};
						var attributes = {
							id: divPlayer,
							name: divPlayer,
							align: "middle"
						};
						
						swfobject.embedSWF(TTS_urlRelativePath()+swf_link, divPlayer, swf_width, swf_height, "9.0.0", "", flashvars, params, attributes,					
								function(data){
							
						});
					} else
					{
						// iPad
			 			//$('#'+divPlayer).html("<audio name=\"audio_player\" id=\"audio_player\" controls=\"controls\" autoplay=\"autoplay\">\n<source src=\""+data.substring(4)+"\" type=\"audio/mpeg\" />\n</audio><script>\nsetTimeout(\"document.getElementById('audio_player').load()\",2000);setTimeout(\"document.getElementById('audio_player').play()\",4000);\n</script><input type='button' onclick=\"document.getElementById('audio_player').play()\" value='play' />");
//player_type_int = 3;
			 			switch(player_type_int){
			
							//Default - long player
							case 0:
							case 1: 
						 			//$('#'+divPlayer).html("<audio name=\""+divPlayer+"audio_player\" id=\""+divPlayer+"audio_player\" controls=\"controls\" autoplay=\"autoplay\">\n<source src=\""+data.substring(4)+"\" type=\"audio/mpeg\" />\n</audio><input type='button' name=\""+divPlayer+"btn_play\" id=\""+divPlayer+"btn_play\" onclick=\"document.getElementById('"+divPlayer+"audio_player').play();alert('played');\" value='&gt;' /><script>\nsetTimeout(\"$('#"+divPlayer+"btn_play').click()\", 1500);\n</script>");
						 			$('#'+divPlayer).html("<audio name=\""+divPlayer+"audio_player\" id=\""+divPlayer+"audio_player\" controls=\"controls\" autoplay=\"autoplay\" controlsList=\"nodownload\">\n<source src=\""+data.substring(4)+"\" type=\"audio/mpeg\" />\n</audio>\n");
						 			break;
						 	
							//Small Player - Shorter width gray player
							case 2:
									$('#'+divPlayer).html("<audio name=\""+divPlayer+"audio_player\" id=\""+divPlayer+"audio_player\" controls=\"controls\" autoplay=\"autoplay\" controlsList=\"nodownload\">\n<source src=\""+data.substring(4)+"\" type=\"audio/mpeg\" />\n</audio>");
									break;
									
							//Button Player - Play + Stop button only
							case 3:
									//$('#'+divPlayer).html("<audio name=\""+divPlayer+"audio_player\" id=\""+divPlayer+"audio_player\" autoplay=\"autoplay\">\n<source src=\""+data.substring(4)+"\" type=\"audio/mpeg\" />\n</audio><input type='button' name=\""+divPlayer+"btn_play\" id=\""+divPlayer+"btn_play\" onclick=\"document.getElementById('"+divPlayer+"audio_player').play();$('#"+divPlayer+"btn_play').hide();$('#"+divPlayer+"btn_pause').show();\" value=' &gt; ' /><input style='display:none' type='button' name=\""+divPlayer+"btn_pause\" id=\""+divPlayer+"btn_pause\" onclick=\"document.getElementById('"+divPlayer+"audio_player').pause();$('#"+divPlayer+"btn_pause').hide();$('#"+divPlayer+"btn_play').show();\" value=' || ' />");
									//$('#'+divPlayer).html("<audio name=\""+divPlayer+"audio_player\" id=\""+divPlayer+"audio_player\" autoplay=\"autoplay\">\n<source src=\""+data.substring(4)+"\" type=\"audio/mpeg\" />\n</audio><img src='/images/icon/btn_play_on.gif' border='0' name=\""+divPlayer+"btn_play\" id=\""+divPlayer+"btn_play\" onclick=\"document.getElementById('"+divPlayer+"audio_player').play();$('#"+divPlayer+"btn_play').hide();$('#"+divPlayer+"btn_pause').show();\" value=' &gt; ' /><img src='/images/icon/btn_pause_on.gif' border='0' style='display:none' name=\""+divPlayer+"btn_pause\" id=\""+divPlayer+"btn_pause\" onclick=\"document.getElementById('"+divPlayer+"audio_player').pause();$('#"+divPlayer+"btn_pause').hide();$('#"+divPlayer+"btn_play').show();\" value=' || ' />");
									$('#'+divPlayer).html("<audio name=\""+divPlayer+"audio_player\" id=\""+divPlayer+"audio_player\" autoplay=\"autoplay\" controlsList=\"nodownload\">\n<source src=\""+data.substring(4)+"\" type=\"audio/mpeg\" />\n</audio><img src='/images/icon/btn_play.png' border='0' name=\""+divPlayer+"btn_play\" id=\""+divPlayer+"btn_play\" onclick=\"document.getElementById('"+divPlayer+"audio_player').play();\" /><img src='/images/icon/btn_pause.png' border='0' name=\""+divPlayer+"btn_pause\" id=\""+divPlayer+"btn_pause\" onclick=\"document.getElementById('"+divPlayer+"audio_player').pause();\" />\n");
									break;
					
						 }
					}
	  			}
		  	});
		}
	};
}

var TTS_tempSettingsString;
var TTS_Settings = new TTS_Settings();
var TTS_Players = new Array();

/* UI related functions */
/* -------------------- */
function TTS_ShowSettingsLayer(target)
{
	TTS_Players[target].ResetSelect();
	var settingLayerID = "speech_setting_layer_" + target;
	TTS_HighlightVoice(target, TTS_Players[target].voice);
	TTS_HighlightSpeed(target, TTS_Players[target].speed);
	MM_showHideLayers(settingLayerID,'','show');
}

function TTS_SelectVoice(target, voice)
{
	TTS_Players[target].selectedVoice = voice;
	TTS_HighlightVoice(target, voice);
}

function TTS_HighlightVoice(target, voice)
{
	var voiceLayerID = "speech_setting_layer_voice_" + target;
	if (voice == 1)
	{
		document.getElementById(voiceLayerID+"_m").className = 'selected_setting';
		document.getElementById(voiceLayerID+"_f").className = '';
	}
	else
	{
		document.getElementById(voiceLayerID+"_m").className = '';
		document.getElementById(voiceLayerID+"_f").className = 'selected_setting';
	}
}

function TTS_SelectSpeed(target, speed)
{
	TTS_Players[target].selectedSpeed = speed;
	TTS_HighlightSpeed(target, speed);
}

function TTS_HighlightSpeed(target, speed)
{
	var speedLayerID = "speech_setting_layer_speed_" + target;
	if (speed == 1)
	{
		document.getElementById(speedLayerID+"_1").className = 'selected_setting';
		document.getElementById(speedLayerID+"_2").className = '';
		document.getElementById(speedLayerID+"_3").className = '';
	}
	else if (speed == 2)
	{
		document.getElementById(speedLayerID+"_1").className = '';
		document.getElementById(speedLayerID+"_2").className = 'selected_setting';
		document.getElementById(speedLayerID+"_3").className = '';
	}
	else
	{
		document.getElementById(speedLayerID+"_1").className = '';
		document.getElementById(speedLayerID+"_2").className = '';
		document.getElementById(speedLayerID+"_3").className = 'selected_setting';
	}
}

function TTS_ApplyUpdate(target, commit)
{
	if (commit) TTS_Players[target].CommitChange();
	var settingLayerID = "speech_setting_layer_" + target;
	MM_showHideLayers(settingLayerID,'','hide');
}

function TTS_CancelUpdate(target)
{
	var settingLayerID = "speech_setting_layer_" + target;
	TTS_Players[target].CommitChange();
	MM_showHideLayers(settingLayerID,'','hide');
}
/* -------------------- */
/* UI related functions */

function check_expiration(expire, roleType){
	
	if(expire == "")
		return false;
	
	var today = new Date();	
	var aryExpire = expire.split("-");	
	var expire  = new Date(aryExpire[0], aryExpire[1], aryExpire[2]);	
	var expired = (today >= expire )? true : false;
	
	/* If Expired & is student, Powerspeech has expired */
	if(roleType == "S" && expired===true)
		return true;	
	
	return false;
}

//Generate the play button
function TTS_PlayButton(target, player, lang, playerType, buttonFull, buttonHighLight, buttonSetting, expired)
{
		
	if (!TTS_Settings.IsInitialized()) TTS_Settings.Initialize();
	
	var showAllButton = playerType==3? false: true;
	
	var layout = TTS_Settings.layout;
	var txtSpeechSetting = TTS_Settings.txtSpeechSetting;
	var txtFullText = TTS_Settings.txtFullText;
	var txtFullPage = TTS_Settings.txtFullPage;
	var txtHighlighted = TTS_Settings.txtHighlighted;
	var txtHighlightedText = TTS_Settings.txtHighlightedText;
	var txtVoice = TTS_Settings.txtVoice;
	var txtMale = TTS_Settings.txtMale;
	var txtFemale  = TTS_Settings.txtFemale;
	var txtSpeed = TTS_Settings.txtSpeed;
	var txtNormal = TTS_Settings.txtNormal;
	var txtSlow = TTS_Settings.txtSlow;
	var txtSlowest = TTS_Settings.txtSlowest;
	var txtApply = TTS_Settings.txtApply;
	var txtCancel = TTS_Settings.txtCancel;
	
	TTS_Players[target] = new TTS_Player(target, player, lang, playerType, expired);
	
	
	var settingLayerID = "speech_setting_layer_" + target;
	var voiceLayerID = "speech_setting_layer_voice_" + target;
	var speedLayerID = "speech_setting_layer_speed_" + target;
	var buttonLayerID = "speech_button_layer_" + target;
	
    var returnButton = '';
    
    returnButton += '<div class="textToSpeech" id="PS_Play_Layer_'+target+'">';
       
	returnButton += '<div id="'+settingLayerID+'" class="speech_setting_layer" style="visibility: hidden; z-index:9;">';	
	//returnButton += '<div class="speech_setting_layer_top_left"><div class="speech_setting_layer_top_right"><span> </span></div></div>';
	
	returnButton += '<div class="speech_setting_layer_top_right"><span > </span></div>';
	
	
	returnButton += '<div class="speech_setting_layer_content" >';
	/*
	returnButton += '<div class="speech_option" style="width: 40%;"> <h1>- '+txtVoice+' - </h1>';
	returnButton += '<h2><a id="'+voiceLayerID+'_m" title="'+txtMale+'" class="selected_setting" href="#" onclick="TTS_SelectVoice(\''+target+'\', 1);return false"><span class="voice_male">  </span></a>';
	returnButton += '<a id="'+voiceLayerID+'_f"  title="'+txtFemale+'" href="#" onclick="TTS_SelectVoice(\''+target+'\', 2);return false"><span class="voice_female"> </span></a></h2>';
	returnButton += '</div>';*/
	
	//returnButton += '<div class="speech_option" style="width: 60%;">';
	returnButton += '<div class="speech_option" style="width: 100%;">';
	returnButton += '<table>';	
	returnButton += '<tr><td>';
	returnButton += '<h1>- '+txtVoice+' - </h1>';
	returnButton += '<a id="'+voiceLayerID+'_m" title="'+txtMale+'" class="selected_setting" href="#"onclick="TTS_SelectVoice(\''+target+'\', 1);return false"><span class="voice_male"> </span></a>';
	returnButton += '<a id="'+voiceLayerID+'_f" title="'+txtFemale+'" href="#" onclick="TTS_SelectVoice(\''+target+'\', 2);return false"><span class="voice_female"> </span></a>';	
	returnButton += '</td><td>';	
	returnButton += '<h1>- '+txtSpeed+' - </h1>';
	returnButton += '<a id="'+speedLayerID+'_1" title="'+txtNormal+'" class="selected_setting" href="#"onclick="TTS_SelectSpeed(\''+target+'\', 1);return false"><span class="speed_normal"> </span></a>';
	returnButton += '<a id="'+speedLayerID+'_2" title="'+txtSlow+'" href="#" onclick="TTS_SelectSpeed(\''+target+'\', 2);return false"><span class="speed_slow"> </span></a>';
	returnButton += '<a id="'+speedLayerID+'_3" title="'+txtSlowest+'" href="#" onclick="TTS_SelectSpeed(\''+target+'\', 3);return false"><span class="speed_slowest"> </span></a>';
	returnButton += '</td></tr></table>';
	returnButton += '</div>';

	
	returnButton += '<div class="speech_setting_layer_btn">';
	//returnButton += '<input type="button" onclick="TTS_ApplyUpdate(\''+target+'\', 1);return false" onmouseout="this.className=\'btn_off\'" onmouseover="this.className=\'btn_on\'" class="btn_off" value="'+txtApply+'"/>';
	//returnButton += '&nbsp;<input type="button" onclick="TTS_ApplyUpdate(\''+target+'\', 0);return false" onmouseout="this.className=\'btn_off\'" onmouseover="this.className=\'btn_on\'" class="btn_off" value="'+txtCancel+'"/>';
	
	returnButton += '<input type="button" onclick="TTS_Tool.ApplyUpdate(\''+target+'\', 1);return false" onmouseout="this.className=\'btn_off\'" onmouseover="this.className=\'btn_on\'" class="btn_off" value="'+txtApply+'"/>';
	returnButton += '&nbsp;<input type="button" onclick="TTS_Tool.ApplyUpdate(\''+target+'\', 0);return false" onmouseout="this.className=\'btn_off\'" onmouseover="this.className=\'btn_on\'" class="btn_off" value="'+txtCancel+'"/>';
	
	returnButton += '</div>';
	returnButton += '<p class="spacer"/>';
	returnButton += '</div>';
	returnButton += '<div class="speech_setting_layer_bottom_left"><div class="speech_setting_layer_bottom_right"><span> </span></div></div>';
	returnButton += '</div>';       

	returnButton += '<div class="speech_button_layer" id="'+buttonLayerID+'" style="width:100px;">';
	//returnButton += '<a class="tts_fullpage" id="tts_fullpage" href="#" onclick="TTS_Play(\''+target+'\', 0);return false" title="'+txtFullPage+'" ttsTarget="'+target+'" title="'+txtFullText+'"> </a>';
	
	
	if(buttonFull==1){
		//Edited: so that the onClick action can be overridden in external HTML/JS file
		if(buttonHighLight==1 || buttonSetting==1){
			returnButton += '<a class="tts_fullpage" id="tts_fullpage" href="#" onclick="TTS_Tool.play_content(\''+target+'\');return false" title="'+txtFullPage+'" ttsTarget="'+target+'" title="'+txtFullText+'"> </a>';
		}else{			
			returnButton += '<span class="tts_mini_fullpage" id="tts_fullpage" href="#" onclick="TTS_Tool.play_content(\''+target+'\');return false" ttsTarget="'+target+'" ><img src="'+TTS_urlRelativePath()+'images/tts_mini_player.gif" border="0" /></span>';
		}
	}
	
	if(buttonHighLight==1){
		returnButton += '<a class="tts_highlight" href="#"onclick="TTS_Play(\''+target+'\', 1);return false" title="'+txtHighlightedText+'" class="tts_highlight" ttsTarget="'+target+'" title="'+txtHighlighted+'"> </a>';
	}
	
	if(buttonSetting==1){
		//	Setting Icon (Spanner - click to open setting layer)	
		returnButton += '<a onclick="TTS_ShowSettingsLayer(\''+target+'\');return false" class="speech_setting" title="'+txtSpeechSetting+'" href="#"/>';
	}
	
	
	returnButton += '</div>';
	returnButton += '</div>';
	
	return returnButton;
}

function TTS_Play(target, highlight)
{
	if(TTS_Players[target]){
		TTS_Players[target].Play(highlight);
	}
}

function TTS_urlRelativePath(){
	// Modify from 2 -> 4
	var pathN = window.location.pathname.split("/").length-2;
	var url_rel_path = "";
		
	for(i=0;i<pathN;i++){
		url_rel_path += "../";
	}

	return url_rel_path;		
}

//Edited: so that the onclick action can be overriden in external HTML/JS file
var TTS_Tool = {		
		play_content: function(target){
			TTS_Play(target, 0);
		},
		ApplyUpdate: function(target, commit){
			TTS_ApplyUpdate(target, commit);
		}
};
