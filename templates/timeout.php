<?
/*
 *  Using: 
 *  
 *  2018-08-31 Cameron
 *      - hide relogin for HKPF
 *  
 */
@session_start();
if (is_file("../servermaintenance.php") && !$iServerMaintenance)
{
    header("Location: ../servermaintenance.php");
    exit();
}

include_once("../includes/global.php");
$lg = $lg ? $lg : "en";
include_once("../lang/lang.$lg.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>eClass Integrated Platform 2.5</title>


<style type="text/css">
<!--
body { padding: 0; margin: 0 auto;background:#e2f3fd url(../images/2020a/session_timeout/timeout_bg.jpg) repeat-x; }/*20200810*/

.timeoutWindow { width: 100%; height: 600px; vertical-align: middle;}
.timeoutWindow .contentContainer { width: 579px; height: 321px; position: relative; display: block; background: url(../../../images/2020a/session_timeout/timeout_bg.png) center; margin: 0 auto; padding: 0;}
.timeoutWindow .textTitle { top: 96px; left: 156px; height: 30px; position: absolute; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; color:#000; width: 380px }
.timeoutWindow .textTitle .timeoutat { color: #FE0000; font-size: 20px; }
.timeoutWindow .textContent { top: 140px; left: 156px; position: absolute; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; color:#000; width: 380px; }
.textContent input {
    font-family: "Century Gothic", "Microsoft JhengHei", Verdana, Arial, Helvetica, sans-serif, 新細明體, 細明體_HKSCS, mingliu ;
    font-weight:bold;
    font-size:15px;
    cursor: pointer;
    color: #ffffff;
    background: #4ab6a2;
    margin-top: 5px;
    border-radius: 2px;
    padding: 3px 10px;
    border: 1px solid #3ba08e;
}/*20200803*/
.textContent input:hover{ opacity: 0.7; }/*20200803*/
-->
</style>

</head>

<body class="timeoutBody">
<div class="timeoutWindowContainer">

<table class="timeoutWindow">
<tr><td>
<div class="contentContainer">
	<div class="textContent"><?=$Lang['SessionTimeout']['Timeout_str1']?>
<?php if (!$sys_custom['project']['HKPF']):?>
	<input type="button" value="<?=$Lang['SessionTimeout']['ReLogin']?>" onClick="self.location='/'" /> <?=$Lang['SessionTimeout']['Timeout_str2']?>
<?php endif;?>	
	</div>
</div>
</td></tr></table>
</div>
</body>
</html>
