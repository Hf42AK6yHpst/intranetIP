<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=GB2312">
<title>PowerConcept 快速键</title>

<link href="../../includes/css/powerconcept_hot_key.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF">

<center>
<h3><img src="../../../images/logo_powerconcept.gif" width="292" height="40"> 快速键</h3>
<table width="580" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td><p><span class="textcontent">为使更方便快捷地制作概念图，用户可利用快速键以加快完成概念图的效率，以下是一些快速键的使用方法：</span><br>
    </p>
    </td>
  </tr>
  <tr>
    <td><table width="100%" border=0 cellpadding=0 cellspacing=0 class="helptable">
      <tr class="helptableheader">
        <th width=167 valign=top><strong>动作</strong></th>
        <th width="208" valign=top><strong>说明</strong></th>
        <th width=181 valign=top><strong>快速键</strong></th>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top ><strong>插入组件</strong></td>
        <td valign=top >在已选择的组件下插入一个组件</td>
        <td width=181 valign=top >按任何字母或数字<br>
      [a-z, A-Z, 0-9] </td>
      </tr>
      <tr >
        <td width=167 valign=top><strong>删除组件/线段</strong></td>
        <td  valign=top>删除已选择的组件/线段</td>
        <td  width=181 valign=top>按 “Delete” 键 (不能删除标题这个组件)</td>
      </tr>
      <tr class="tablerow" >
        <td width=167 valign=top ><strong>修改组件/线段<br>
          名称/格式</strong></td>
        <td  valign=top>修改已选择的组件/线段</td>
        <td  width=181 valign=top>连按组件/线段两下</td>
      </tr>
      <tr>
        <td width=167 valign=top><strong>移动组件</strong></td>
        <td valign=top>&nbsp;</td>
        <td width=181 valign=top>&nbsp;</td>
      </tr>
      <tr >
        <td  width=167 valign=top><li>一个组件</td>
        <td  valign=top>拖拉一个组件到适当的位置</td>
        <td  width=181 valign=top>鼠标拖拉</td>
      </tr>
      <tr>
        <td width=167 valign=top><li>组件及它以下的组件</td>
        <td valign=top>拖拉一组组件到适当的位置</td>
        <td width=181 valign=top>按着 “Ctrl”键 + 鼠标拖拉</td>
      </tr>
      <tr>
        <td width=167 valign=top><li>所有组件</td>
        <td valign=top>拖拉所有组件到适当的位置</td>
        <td width=181 valign=top>按着 “Space Bar” + 鼠标拖拉</td>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top><strong>检视</strong></td>
        <td valign=top>&nbsp;</td>
        <td width=181 valign=top>&nbsp;</td>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top><li>向上检视</td>
        <td valign=top>把概念图向上移动</td>
        <td width=181 valign=top>按 “上” 键</td>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top><li>向下检视</td>
        <td valign=top>把概念图向下移动</td>
        <td width=181 valign=top>按 “下” 键</td>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top><li>向左检视</td>
        <td valign=top>把概念图向左移动</td>
        <td width=181 valign=top>按 “左” 键</td>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top><li>向右检视</td>
        <td valign=top>把概念图向右移动</td>
        <td width=181 valign=top>按 “右” 键</td>
      </tr>
      <tr>
        <td width=167 valign=top><strong>变焦</strong></td>
        <td valign=top>&nbsp;</td>
        <td width=181 valign=top>&nbsp;</td>
      </tr>
      <tr>
        <td width=167 valign=top><li>放大</td>
        <td valign=top>放大概念图</td>
        <td width=181 valign=top>按 “Page Up” 键</td>
      </tr>
      <tr>
        <td width=167 valign=top><li>缩小</td>
        <td valign=top>缩小概念图</td>
        <td width=181 valign=top>按 “Page Down” 键</td>
      </tr>
      <tr class="tablerow">
        <td  width=167 valign=top><strong>复原</strong></td>
        <td  valign=top>复原动作包括︰插入、删除、排列、修改、移动、联机等…</td>
        <td width=181 valign=top>按着 “Ctrl”键 + “z” 键</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center"><a href="javascript:window.close();">关闭</a></td>
  </tr>
</table>

</center>

</body>
</html>
