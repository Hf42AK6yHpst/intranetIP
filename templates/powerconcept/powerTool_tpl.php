<?php
/*
 * using by
 * 
 * Modification Log:
 * 2011-06-26 (Jason)
 * 		- use stripslashes() in the student username info
 * 2011-06-24 (Jason)
 * 		- add $is_marking into the param_send to set into the flashvars to handle teacher marking view of PowerTool Activity
 * 2010-11-30 (Jason): 
 * 		- handle the problem of the failure to display HELP note in PowerConcept, add the HELP case in JS Action
 */ 

include_once('../../../system/settings/global.php');
include_once("../../includes/php/lib-db.php");


//////adam debug//////////////
//$param_send = $param_send."&InterfaceLang=chigb";
//$url_swf = "/junior20/src/tool/powerconcept/PowerConcept.2.0.Secondary.chigb.swf";
//////adam debug//////////////


$param_send = rawurldecode($param_send);
$url_swf = rawurldecode($url_swf);
$username = rawurldecode($username);

$param_send .= (isset($is_marking)) ? '&is_marking='.$is_marking : '';

$system_encode = returnCharset();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML xmlns="http://www.w3.org/1999/xhtml">
<HEAD>
<TITLE></TITLE>
<META http-equiv="content-type" content="text/html; charset=<?=$system_encode?>" />
<META HTTP-EQUIV="EXPIRES" CONTENT="Mon, 22 Jul 2002 11:12:01 GMT" />
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE" />
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<!--
<script src="AC_RunActiveContent.js" type="text/javascript"></script>
<script src="AC_ActiveX.js" type="text/javascript"></script>
<script language="JavaScript" src="../../includes/js/script.js" type="text/javascript"> </script>
-->
<script language="JavaScript" src="../../../js/script.js" type="text/javascript"> </script>

<script language="JavaScript">
var isInternetExplorer = navigator.appName.indexOf("Microsoft") != -1;
var section_end = 0;

// Handle all the FSCommand messages in a Flash movie.
function PowerConcept_DoFSCommand(command, args) {	
	var PowerConceptObj = isInternetExplorer ? document.all.PowerConcept : document.PowerConcept;	
	switch (command) {				
		case "ATTACH_FILE":	
			browseFileLink("", args);			
			break; 		
		case "VIEW_FILE":			
			viewFile(args);			
			break;		
		case "HELP":
			//viewFile(args);
			newWindow("hot_key_<?=$ck_default_lang?>.php", 13);
			break;
		case "EXIT":			
			//document.form1.target = "_parent";			
			document.form1.submit();			
			break;	
		}
}// Hook for Internet Explorer.
if (navigator.appName && navigator.appName.indexOf("Microsoft") != -1 && navigator.userAgent.indexOf("Windows") != -1 && navigator.userAgent.indexOf("Windows 3.1") == -1) {	
	document.write('<script language=\"VBScript\"\>\n');	
	document.write('On Error Resume Next\n');	
	document.write('Sub PowerConcept_FSCommand(ByVal command, ByVal args)\n');	
	document.write('	Call PowerConcept_DoFSCommand(command, args)\n');	document.write('End Sub\n');	
	document.write('</script\>\n');
}
// Select and Attach File from eClass
function browseFileLink(myAtt, args){	
	var tmp = args.split(":");	
	var dObj = tmp[0];	var myAtt = tmp[1]; 	
	document.form1.pc_file_obj.value = dObj;	
	var url = "<?=$eclass_url_path?>/src/resource/eclass_files/files/index.php?category=0&attach=1&fieldname=pc_file_path&attachment=" + myAtt;	
	newWindow(url,30);
}
function getFilePath(myFilePath){
	document.form1.pc_file_path.value = myFilePath;
	var PowerConceptObj = isInternetExplorer ? document.all.PowerConcept : document.PowerConcept;
	PowerConceptObj.SetVariable(document.form1.pc_file_obj.value, myFilePath);
	//viewFile(myFilePath);
} // Open File from eClass

function viewFile(myFilePath){	
	var url = "<?=$eclass_url_path?>/src/resource/eclass_files/files/open.php?categoryID=0&fpath=" + myFilePath;	
	newWindow(url,9);
}

function SavePowerTool(){
	var PowerConceptObj = isInternetExplorer ? document.all.PowerConcept : document.PowerConcept;		
	PowerConceptObj.JS2Save();
}

function PowerConcept_SaveCallBack(success){
	try{
		if (section_end){
			top.notify_teacher(function(refreshFrame){
				top.$('#powerFrame').replaceWith(refreshFrame);
			});
		}
		else{
			top.notify_teacher();
		}
	}catch (ex){}
}


</script>
<style>
html,body{
	height: 100%;
	padding:0px;
	margin:0px;
}
#container{
	min-height: 100%;
}
</style>
</head>
<BODY>
<?php if (!empty($username)) { ?>
<div style="width:100%;padding-left:5px;padding-top:3px;padding-bottom:3px;background-color:#cde2ec;font-weight:bold"><?=stripslashes($username)?></div>
<?php } ?>
<!--<script language="JavaScript" type="text/javascript" src="../../includes/js/swfobject_1_5.js"></script>-->
<script language="JavaScript" type="text/javascript" src="<?=$eclass_http_root?>/js/swfobject_1_5.js"></script>
<div id="flashcontent" style="width:100%;height:600px;">
  This text is replaced by the Flash movie.
</div>

<script type="text/javascript">

	//////adam debug//////////////
	//alert("<?=$url_swf?>");
	//alert("<?=$param_send?>");
	//////adam debug//////////////

   //swf, id, width, height, version, background color
   var so = new SWFObject("<?=$url_swf?>", "PowerConcept", "100%", "100%", "8", "#FFFFFF");
   //so.addParam("allowScriptAccess", "sameDomain");
   so.addParam("loop", "false");
   so.addParam("menu", "false");
   so.addParam("salign", "lt");
   so.addParam("movie", "<?=$url_swf?>");
   so.addParam("quality", "high");
   so.addParam("scale", "showall");
   so.addParam("flashvars", "<?=$param_send?>");
   so.addParam("name", "demo_index");
   so.addParam("align", "middle");
   so.addParam("src", "<?=$url_swf?>");
   so.addParam("allowScriptAccess", "sameDomain");
   so.write("flashcontent");

	/*
	var flashvars = {
		flashvars: "<?=$param_send?>"
	};	
	var params = {
		play: "true",
		loop: "false",
		menu: "false",
		quality: "high",
		scale: "showall",
		wmode: "window", 
		bgcolor: "#FFFFFF",
		allowscriptaccess: "sameDomain",
		allowfullscreen: "false",
		salign: "lt"
	};
	var attributes = {
		id: "PowerConcept",
		name: "PowerConcept",
		align: "middle"
	};
	//alert("name: "+swfPath+" / areaID: "+areaFlashID+" / width: "+flashWidth+" / height: "+flashHeight+" / version: "+flashVersion+" / vars: "+flashvars+" / params: "+params+" / attributes: "+attributes);
	swfobject.embedSWF("<?=$url_swf?>", "flashcontent", "100%", "100%", "7", "", flashvars, params, attributes);
	*/
</script>
	
	<form name="form1" action="../../student/work_student/handin_pc.php" method="post">
	<input type="hidden" name="PowerconceptID" value="<?=$PowerconceptID?>">
	<input type="hidden" name="assignment_id" value="<?=$assignment_id?>">
	<input type="hidden" name="group_id" value="<?=$group_id?>">
	<input type="hidden" name="handin_id" value="<?=$handin_id?>">
	<input type="hidden" name="worktype" value="<?=$worktype?>">
	<input type="hidden" name="fileID" value="<?=$fileID?>">
	<input type="hidden" name="fileName" value="<?=$fileName?>">
	<input type="hidden" name="fromCC" value="<?=$fromCC?>">
	<input type="hidden" name="isFileNew" value="<?=$isFileNew?>">
	<input type="hidden" name="categoryID" value="<?=$categoryID?>">
	<input type="hidden" name="courseID" value="<?=$courseID?>">
	<input type="hidden" name="pc_file_obj" value="">
	<input type="hidden" name="pc_file_path" value="">
	<input type="hidden" name="openPCFromCC" value="<?=$openPCFromCC?>">
	<input type="hidden" name="pcid" value="<?=$pcid?>">
	
</BODY>
</HTML>