<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=big5">
<title>PowerConcept Hot Key</title>

<link href="../../includes/css/powerconcept_hot_key.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF">

<center>
<h3><img src="../../../images/logo_powerconcept.gif" width="292" height="40"> Hot Key</h3>
<table width="580" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td><p><span class="textcontent">To facilitate students to create a concept map quickly, some hot keys are listed below for reference: </span><br>
    </p>
    </td>
  </tr>
  <tr>
    <td><table width="100%" border=0 cellpadding=0 cellspacing=0 class="helptable">
      <tr class="helptableheader">
        <th width=167 valign=top><strong>Action</strong></th>
        <th width="208" valign=top><strong>Description</strong></th>
        <th width=181 valign=top><strong>Hot Key</strong></th>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top ><strong>Insert Node</strong></td>
        <td valign=top >Insert a Sub-node under a Selected Node</td>
        <td width=181 valign=top >Press either numeric or alphabetic key<br>
      [a-z, A-Z, 0-9] </td>
      </tr>
      <tr >
        <td width=167 valign=top><strong>Delete Node/Connection</strong></td>
        <td  valign=top>Delete selected Node/Connection</td>
        <td  width=181 valign=top>Press "Delete" (Title Node cannot be deleted)</td>
      </tr>
      <tr class="tablerow" >
        <td width=167 valign=top ><strong>Edit Node/Connection Properties
          </strong></td>
        <td  valign=top>Edit Selected Node/Connection</td>
        <td  width=181 valign=top>Double Click the Node/Connection</td>
      </tr>
      <tr>
        <td width=167 valign=top><strong>Move Node</strong></td>
        <td valign=top>&nbsp;</td>
        <td width=181 valign=top>&nbsp;</td>
      </tr>
      <tr >
        <td  width=167 valign=top><li>Single Node</td>
        <td  valign=top>Drag a Single Node</td>
        <td  width=181 valign=top>Drag and Drop</td>
      </tr>
      <tr>
        <td width=167 valign=top><li>A Node with its Sub-Node</td>
        <td valign=top>Drag a set of Node and sub-nodes</td>
        <td width=181 valign=top>Press 'CTRL' + Drag and Drop</td>
      </tr>
      <tr>
        <td width=167 valign=top><p> <li>All Nodes</p></td>
        <td valign=top>Drag all Nodes</td>
        <td width=181 valign=top>Press 'SPACE' + Drag and Drop</td>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top><strong>View</strong></td>
        <td valign=top>&nbsp;</td>
        <td width=181 valign=top>&nbsp;</td>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top><li>Upwards</td>
        <td valign=top>Move Concept Map Upwards</td>
        <td width=181 valign=top>Up Arrow</td>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top><li>Downwards</td>
        <td valign=top>Move Concept Downwards</td>
        <td width=181 valign=top>Down Arrow</td>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top><li>Leftwards</td>
        <td valign=top>Move Concept Map Leftwards</td>
        <td width=181 valign=top>Left Arrow</td>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top><li>Rightwards</td>
        <td valign=top>Move Concept Map Rightwards</td>
        <td width=181 valign=top>Right Arrow</td>
      </tr>
      <tr>
        <td width=167 valign=top><strong>Zoom</strong></td>
        <td valign=top>&nbsp;</td>
        <td width=181 valign=top>&nbsp;</td>
      </tr>
      <tr>
        <td width=167 valign=top><li>Magnify</td>
        <td valign=top>Magnify the Concept Map</td>
        <td width=181 valign=top>Page Up</td>
      </tr>
      <tr>
        <td width=167 valign=top><li>Minify</td>
        <td valign=top>Minify the Concept Map</td>
        <td width=181 valign=top>Page Down</td>
      </tr>
      <tr class="tablerow">
        <td  width=167 valign=top><strong>Undo</strong></td>
        <td  valign=top>Undo action: Insert, Delete, Arrange, Edit, Move, Connection, etc.</td>
        <td width=181 valign=top>Press 'CTRL' + 'Z'</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center"><a href="javascript:window.close();">Close</a></td>
  </tr>
</table>
</center>

</body>
</html>
