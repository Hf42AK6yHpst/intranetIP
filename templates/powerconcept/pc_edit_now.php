<?php
# using : Paul

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$PowerConceptID = isset($PowerConceptID) && $PowerConceptID!=''? $PowerConceptID : '';

# Initialize Object
$libdb = new libdb();

# Die if no PowerConceptID 
if(!$PowerConceptID){
	echo "No PowerConceptID";
	die();
}

# Define the swf file use
switch($intranet_session_language){
	case 'en': $url_swf = "PowerConcept.2.0.Secondary.eng.swf"; break;
	case 'b5': $url_swf = "PowerConcept.2.0.Secondary.chib5.swf"; break;
}

# Generate Gateway Path
$src_folder = "/tool/powerconcept/";
$src_folder_referer = "/tool/powerconcept/";
$ecGatewayPath = getAMFphpGateWay($src_folder_referer, $src_folder);

# Define UserKey
$ecUserKey = "{$ck_login_session_id}SeP{$ck_user_id}SeP".$row[0]."SeP".$PowerconceptID."SeP".$pc_type."SeP".$fileID."SeP".$isFileNew."SeP".$fileName."SeP".$categoryID;

# Define ItemID
$ecItemID = "{$intranet_db}SeP{$ck_user_id}SeP{$PowerConceptID}SeP{$fileID}SeP{$isFileNew}SeP{$r_comeFrom}";

# Define the Flash Parameter
$param_send .= "ecGatewayPath=$ecGatewayPath";
$param_send .= "&ecUserKey=$ecUserKey";
$param_send .= "&ecItemID=$ecItemID";

?>
<html>
	<head>
		<title>eClass</title>
		<meta http-equiv='content-type' content='text/html; charset=UTF-8'>
		<script language="JavaScript" type="text/javascript" src="../../eclass40/src/includes/js/script.js"></script>
		<!--<script language="JavaScript" type="text/javascript" src="http://<?=$eclass40_httppath?>src/includes/js/script.js"></script>-->
		<script language="JavaScript">
		<!--
		var isInternetExplorer = navigator.appName.indexOf("Microsoft") != -1;
		// Handle all the FSCommand messages in a Flash movie.
		function PowerConcept_DoFSCommand(command, args) {
			var PowerConceptObj = isInternetExplorer ? document.all.PowerConcept : document.PowerConcept;
			switch (command) {
				case "ATTACH_FILE":
					browseFileLink("", args);
					break;
		
				case "VIEW_FILE":
					viewFile(args);
					break;
		
				case "HELP":
					//viewFile(args);
					newWindow("hot_key_eng.php", 13);
					break;
		
				case "EXIT":
					top.window.close();
					break;
			}
		}
		
		// Hook for Internet Explorer.
		if (navigator.appName && navigator.appName.indexOf("Microsoft") != -1 && navigator.userAgent.indexOf("Windows") != -1 && navigator.userAgent.indexOf("Windows 3.1") == -1) {
			document.write('<script language=\"VBScript\"\>\n');
			document.write('On Error Resume Next\n');
			document.write('Sub PowerConcept_FSCommand(ByVal command, ByVal args)\n');
			document.write('	Call PowerConcept_DoFSCommand(command, args)\n');
			document.write('End Sub\n');
			document.write('</script\>\n');
		}
		// Select and Attach File from eClass
		function browseFileLink(myAtt, args){
			var tmp = args.split(":");
			var dObj = tmp[0];
			var myAtt = tmp[1];
		
			document.form1.pc_file_obj.value = dObj;
			var url = "/eclass40/src/resource/eclass_files/files/index.php?category=0&attach=1&fieldname=pc_file_path&attachment=" + myAtt;
			newWindow(url,1);
		}
		function getFilePath(myFilePath){
			document.form1.pc_file_path.value = myFilePath;
			var PowerConceptObj = isInternetExplorer ? document.all.PowerConcept : document.PowerConcept;
			PowerConceptObj.SetVariable(document.form1.pc_file_obj.value, myFilePath);
			//viewFile(myFilePath);
		}
		// Open File from eClass
		function viewFile(myFilePath){
			var url = "/eclass40/src/resource/eclass_files/files/open.php?categoryID=0&fpath=" + myFilePath;
			newWindow(url,9);
		}
		//-->
		</script>
	</head>
	<BODY bgcolor="#86C7F7" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
	
		<div width="100%" height="100%">
			<div id="flashcontent" width="100%" height="100%">
				This text is replaced by the Flash movie.
			</div>
		</div>
		
		<script language="JavaScript" type="text/javascript" src="<?=$intranet_httppath?>/templates/swfobject_1_5.js"></script>
		<script type="text/javascript">
		function SavePowerTool(){
			var PowerConceptObj = isInternetExplorer ? document.all.PowerConcept : document.PowerConcept;		
			PowerConceptObj.JS2Save();
		}
		
		   //swf, id, width, height, version, background color
		   var so = new SWFObject("<?=$url_swf?>", "PowerConcept", "100%", "100%", "7", "#FFFFFF");
		   //so.addParam("allowScriptAccess", "sameDomain");
		   so.addParam("loop", "false");
		   so.addParam("menu", "false");
		   so.addParam("salign", "lt");
		   so.addParam("movie", "<?=$url_swf?>");
		   so.addParam("quality", "high");
		   so.addParam("scale", "showall");
		   so.addParam("flashvars", "<?=$param_send?>");
		   so.addParam("name", "demo_index");
		   so.addParam("align", "middle");
		   so.addParam("src", "<?=$url_swf?>");
		   so.addParam("allowScriptAccess", "sameDomain");
		   so.write("flashcontent");
		</script>
	</BODY>
</html>