var ie4 = (document.all) ? true : false;
var ns4 = (document.layers) ? true : false;
var ns6 = (document.getElementById && !document.all) ? true : false;
var pa = "parent";
var is_ie5_5up = false;
var sliding = {c1:false, c2:false, c3:false, c4:false}
var hide = "hidden";
var show = "visible";
var ie_move_h = null;
var ie_move_v = null;
var top_max = null;
var top_min = null;
var nextAction = null;
var opacity_it = null;
if(ie4){
	Ex = "event.x";
	Ey = "event.y";
} else if(ns4){
	Ex = "e.pageX";
	Ey = "e.pageY";
	window.captureEvents(Event.MOUSEMOVE);
	window.onmousemove=overhere;
} else if (ns6) {
	Ex = "e.clientX";
	Ey = "e.clientY";
	document.addEventListener("mousemove",overhere,false);
}

function hidelayer(lay) {
	if (ie4) {document.all[lay].style.visibility = "hidden";}
	if (ns4) {document.layers[pa].document.layers[lay].visibility = "hide";}
	if (ns6) {document.getElementById(lay).style.display = "none";}
}


function showlayer(lay) {
	if (ie4) {document.all[lay].style.visibility = "visible";}
	if (ns4) {document.layers[pa].document.layers[lay].visibility = "show";}
	if (ns6) {document.getElementById(lay).style.display = "block";}

}


function writetolayer(lay,txt) {
	showlayer(lay);

	if (ie4)
	{
		document.all[lay].innerHTML = txt;
	}

	if (ns4)
	{
		var lyr = document.layers[pa].document[lay].document;
		lyr.open();
		lyr.write(txt);
		lyr.close();
	}

	if (ns6)
	{
		over = document.getElementById(lay);
		range = document.createRange();
		range.setStartBefore(over);
		domfrag = range.createContextualFragment(txt);
		while (over.hasChildNodes()) {
			over.removeChild(over.lastChild);
		}
		over.appendChild(domfrag);
	}
}

function switchLayer(layer_name, imgObj, alt_s, alt_h){
    var srcImg = imgObj.src;
    var tn = srcImg.split("/");
    var imgName = tn[tn.length-1];

	if (!document.all)
		return;

	//var co=ns6? document.getElementById(layer_name).style : document.all[layer_name].style;
	var co=document.all[layer_name].style;

	if (co.visibility==show)
	{
		co.visibility = hide;
		co.position  = "absolute";
	} else
	{
		co.visibility = show;
		co.position  = "relative";
	}

}

function switchAll(layer_pre, total, objself, txt_show, txt_hide) {
	if (!document.all)
		return;

	var isShow = (objself.value == txt_show) ? true : false;

	for (var i=0; i<total; i++)
	{
		lay = layer_pre + i;
		var co=document.all[lay].style;
		if (!isShow)
		{
			co.visibility = hide;
			co.position  = "absolute";
			objself.value = txt_show;
		} else
		{
			co.visibility = show;
			co.position  = "relative";
			objself.value = txt_hide;
		}
	}

}

function movelayer(lay,h){
	if (ie4) {heig=document.all[lay].style.top;}
	if (ns4) {heig=document.layers[pa].document.layers[lay].top;}
	if (ns6) {heig=document.getElementById(lay).style.top;}

	ih = parseInt(heig) + h;
	if (ie4) {document.all[lay].style.top=ih+'px';}
	if (ns4) {document.layers[pa].document.layers[lay].top = ih;}
	if (ns6) {document.getElementById(lay).style.top=ih+'px';}
}


function slide(lay, h, speed, up) {
	if (ie4)
	{
		heig=document.all[lay].style.top; it=4;
		if (ie_move_h)
		{
			it = ie_move_h;
		} else
		{
			if (is_ie5_5up)
			{
				it = 4;
			}
		}
	}
	if (ns4) {heig=document.layers[pa].document.layers[lay].top; it=6;}
	if (ns6) {heig=document.getElementById(lay).style.top; it=10;}
	ih = parseInt(heig);

	if (up == 0)
	{
		var margin_max = (top_max)? top_max : h;
		if (ih != margin_max)
		{
			it = (it + ih > margin_max) ? margin_max-ih : it;
			movelayer(lay, it);
		}
		if (ih < margin_max)
		{
			setSliding(lay, 1);
			setTimeout("slide('"+lay+"',"+h+","+speed+",0)",speed);
		} else
			setSliding(lay, 0);
	} else
	{
		var margin_min = (top_min)? top_min : 0;
		if (ih != margin_min)
		{
			it = (ih - it < margin_min) ? ih-margin_min : it;
			movelayer(lay, -it);
		}
		if (ih > margin_min)
		{
			setSliding(lay, 1);
			setTimeout("slide('"+lay+"',"+h+","+speed+",1)",speed);
		} else
			setSliding(lay, 0);
	}
}


function setSliding(lay, on_off) {
	is_sliding = "sliding." + lay;
	is_sliding += (on_off==0) ? "=false" : "=true";
	eval (is_sliding);
	if (nextAction && on_off==0)
	{
		eval(nextAction);
		nextAction = null;
	}
}


function getSliding(lay) {
	is_sliding = "sliding." + lay;
	return eval(is_sliding);
}


function move(lay,h){
	if (getSliding(lay))
		return;
	if (ie4) {
		heig=document.all[lay].style.top; speed=1;
		if (is_ie5_5up) {
			speed=1;
			document.all[pa].focus();
		}
	}
	if (ns4) {heig=document.layers[pa].document.layers[lay].top; speed=1;}
	if (ns6) {heig=document.getElementById(lay).style.top;speed=1;}
	ih = parseInt(heig);
	var margin_min = (top_min)? top_min : 0;
	var margin_max = (top_max)? top_max : 0;
	if (ih<=margin_min)
	{
		setTimeout("slide('"+lay+"',"+h+","+speed+",0)",1);
	} else if (ih>=margin_max)
	{
		setTimeout("slide('"+lay+"',"+h+","+speed+",1)",1);
	}
}


function init(max_options){
	ie4 = (document.all) ? true : false;
	ns4 = (document.layers) ? true : false;
	ns6 = (document.getElementById && !document.all) ? true : false;
	var is_major = parseInt(navigator.appVersion);
	var is_minor = parseFloat(navigator.appVersion);
	var agt=navigator.userAgent.toLowerCase();
	var is_ie     = ((agt.indexOf("msie") != -1) && (agt.indexOf("opera") == -1));
    var is_ie3    = (is_ie && (is_major < 4));
    var is_ie4    = (is_ie && (is_major == 4) && (agt.indexOf("msie 4")!=-1) );
    var is_ie5    = (is_ie && (is_major == 4) && (agt.indexOf("msie 5.0")!=-1) );
    is_ie5_5up =(is_ie && !is_ie3 && !is_ie4 && !is_ie5);

	var thin="padding-left:0.1cm; border-color:grey; border-style:solid; border-top-width:0px; border-left-width:1px; border-right-width:1px; border-bottom-width:0px; font-size: 8pt";
	var thick="padding-left: 0.1cm; border-style: solid; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px; border-bottom-width: 0px; font-size: 10pt";

	max_options += 1;
	var i = 0;

	if (ns4){

		for (y=0; y<4; y++){
			if (isEmpty(y, max_options)) {
				i++;
				//lef=y*130+190;
				str='<table width="130" border="0" cellspacing="0" cellpadding="0"><tr><td><img src="../../images/space.gif" width="2" height="1" alt="" border="0"></td></tr>\n';
				str+='<tr><td background="../../images/bar_bg0.gif" height="19"><img src="../../images/space.gif" width="2" height="1" alt="" border="0"><font face="Verdana" size="2"><a style="text-decoration: none" href="javascript:move(\'c'+i+'\',40)"><b>'+menu[y][0]+'</b></a></font></td></tr>\n';
				ii = 0;
				for (x=1; x<max_options; x++){
					if (menu[y][x] != "") {
						str+='<tr><td background="../../images/menu_bg.gif"><img src="../../images/space.gif" width="5" height="1" alt="" border="0"><font face="Verdana" size="1"><a style="text-decoration: none" href="'+url[y][x]+'" target=eClassMain>'+menu[y][x]+'</a></font></td></tr>\n';
						ii ++;
					}
				}
				for (x=ii; x<max_options; x++) {
			    	str+='<tr><td background="../../images/menu_bg.gif">&nbsp;</td></tr>\n';
				}
				str+='<tr><td background="../../images/menu_bg.gif">&nbsp;</td></tr>\n';
				str+='</table>';

				cmd='writetolayer("c'+i+'",str)';
				eval(cmd);
			}
		}

	}else {

		for (y=0; y<4; y++){
			if (isEmpty(y, max_options)) {
				i++;
				//lef=y*130+190;
				str='<table width="130" border="0" cellspacing="0" cellpadding="0"><tr><td colspan=3><img src="../images/space.gif" width="2" height="1" alt="" border="0"></td></tr>\n';
				str+='<tr><td colspan=3 style="'+thick+'" background="../images/bar_bg.gif" height="19"><a class="menuHead" href="javascript:move(\'c'+i+'\',40)"><b>'+menu[y][0]+'</b></font></a></td></tr>\n';
				ii = 0;
				for (x=1; x<max_options; x++){
					if (menu[y][x] != "") {
						str+='<tr><td><img src="../images/space.gif" width="2" height="1" alt="" border="0"></td><td style="'+thin+'" bgcolor=white><a href="'+url[y][x]+'" target=eClassMain>'+menu[y][x]+'</a></td><td><img src="../images/space.gif" width="1" height="1" alt="" border="0"></td></tr>\n';
						ii ++;
					}
				}
				for (x=ii; x<max_options; x++) {
			    	str+='<tr><td><img src="../images/space.gif" width="2" height="1" alt="" border="0"></td><td style="'+thin+'" bgcolor=white>&nbsp;</td><td><img src="../images/space.gif" width="1" height="1" alt="" border="0"></td></tr>\n';
				}
				str+='<tr><td><img src="../images/space.gif" width="2" height="1" alt="" border="0"></td><td style="'+thin+'" bgcolor=white>&nbsp;</td><td><img src="../images/space.gif" width="1" height="1" alt="" border="0"></td></tr>\n';
				str+='</table>';

				cmd='writetolayer("c'+i+'",str)';
				eval(cmd);
			}
		}

	}

}

function isEmpty(y, max_options){
	for (x=1; x<max_options; x++)
	{
		if (menu[y][x]!="")
		return true;
	}
	return false;
}



// horizontal
function movelayerHori(lay,h){
	if (ie4) {heig=document.all[lay].style.left;}
	if (ns4) {heig=document.layers[pa].document.layers[lay].left;}
	if (ns6) {heig=document.getElementById(lay).style.left;}
	ih = parseInt(heig) + h;
	if (ie4) {document.all[lay].style.left=ih+'px';}
	if (ns4) {document.layers[pa].document.layers[lay].left = ih;}
	if (ns6) {document.getElementById(lay).style.left=ih+'px';}
}


function slideHori(lay, h, speed, up) {
	if (ie4) {
		heig=document.all[lay].style.left;
		it = (ie_move_v) ? ie_move_v : 4;
	}
	if (ns4) {heig=document.layers[pa].document.layers[lay].left; it=6;}
	if (ns6) {heig=document.getElementById(lay).style.left; it=10;}
	ih = parseInt(heig);

	if (up == 0){
		if (ih != h)
			movelayerHori(lay, it);
		if (ih < h) {
			setSliding(lay, 1);
			setTimeout("slideHori('"+lay+"',"+h+","+speed+",0)",speed);
		} else {
			setSliding(lay, 0);
		}
	} else {
		if (ih != 25)
			movelayerHori(lay, -it);
		if (ih > 25) {
			setSliding(lay, 1);
			setTimeout("slideHori('"+lay+"',"+h+","+speed+",1)",speed);
		} else {
			setSliding(lay, 0);
		}
	}
}


function moveHori(lay,h){
	if (getSliding(lay))
		return;
	if (ie4)
	{
		heig=document.all[lay].style.left;
		speed=1;
	}
	if (ns4) {heig=document.layers[pa].document.layers[lay].left; speed=1;}
	if (ns6) {heig=document.getElementById(lay).style.left; speed=1;}
	ih = parseInt(heig);

	if (ih<=25)
		slideHori(lay,h,speed,0);
	else
		slideHori(lay,h,speed,1);
}
// end of horizontal


function fadeInTool(lay, max, isToShow){
	var oldValue = document.all[lay].style.filter; // = "Alpha(Opacity=30)";
	var pos_opacity = oldValue.indexOf("pacity=");
	var isContinue = false;

	var newValue = parseInt(oldValue.substr(pos_opacity+7));
	if (newValue<max && isToShow)
	{
		newValue += (opacity_it) ? opacity_it : 2;
		isContinue = true;

	} else if (newValue>max && !isToShow)
	{
		newValue -= (opacity_it) ? opacity_it : 2;
		isContinue = true;
	} else
	{
		newValue = max;
	}

	if ((newValue>max && isToShow) || (newValue<max && !isToShow))
	{
		newValue = max;
	}
	if (newValue==max && nextAction)
	{
		eval(nextAction);
		nextAction = null;
	}

	document.all[lay].style.filter = "Alpha(Opacity="+ newValue +")";
	if (isContinue)
	{
		setTimeout("fadeInTool('"+lay+"', "+max+", " + isToShow + ")", 10);
	}
}


function startup_animate(){
	ie_move_v = 4;
	ie_move_h = 4;

	//lyrMenu : left:285px; top:60px;
	move("lyrMenu", 0);

	//lyrLogo : left:5px; top:15px;
	setTimeout("moveHori('lyrLogo',15)", 200);

	//lyrTools
	nextAction = "isLoaded = true;";
	setTimeout("fadeInTool('lyrTools', 100, 1)", 800);

}


function showSubMenu(mName){
	if (!isLoaded)
	{
		return ;
	}
	var lay = "lyrSM" + mName;
	var sub_menu_pre = document.form1.sub_menu_now.value;
	top_max = 80;
	top_min = 60;
	ie_move_v = 1;

	updateLayerPos(mName, 1, 0);
	if (mName!=sub_menu_pre)
	{
		// hide previous menu
		if (sub_menu_pre!="")
		{
			ie_move_v = 2;
			var preLay = "lyrSM" + sub_menu_pre;
			move(preLay, top_max);
			nextAction = "continueMenu('"+preLay+"', '"+lay+"')";
		} else
		{
			// show selected
			if (typeof(document.all[lay])!="undefined")
			{
				continueMenu("", lay);
			}
		}
		if (typeof(document.all[lay])!="undefined")
		{
			document.form1.sub_menu_now.value = mName;
		} else
		{
			document.form1.sub_menu_now.value = "";
		}
	} else
	{
		move(lay, top_max);
		//nextAction = "hidelayer('"+lay+"')";
		document.form1.sub_menu_now.value = "";
	}
	if (mName=="CONTENTS")
	{
		parent.eClassMain.location = "course/contents/contentframe.php";
	} else if (mName=="MESSAGEBOX")
	{
		parent.eClassMain.location = "student/information/message_box/contentframe.php";
	} else if (mName=="FORUM")
	{
		parent.eClassMain.location = "social_corner/bulletin/index.php";
	} else if (mName=="SHARING")
	{
		parent.eClassMain.location = "social_corner/directory/index.php";
	}
	// Live eClass
	else if (mName=="LIVECLASS")
	{
		window.open('liveclass/index.php','LiveClass','width=1024,height=768,fullscreen=yes');
		return;		// Logo layer is not needed to retain the focus
	}

	setToBlur();
}


function continueMenu(preLay, lay){
	if (preLay!="")
	{
		//hidelayer(preLay);
		displayTable("table_"+lay, "none");
	}

	var layer_str;
	if (ie4) {layer_str = "all[lay]";}
	if (ns4) {layer_str = "layers[lay]";}
	if (ns6) {layer_str = "getElementById(lay)";}
	if (typeof(eval("document."+layer_str))!="undefined")
	{
		ie_move_v = 1;
		displayTable("table_"+lay, "");
		//showlayer(lay);
		move(lay, top_min);
	}
}


function showMainMenu(mName){
	if (!isLoaded)
	{
		return ;
	}

	var lay = "lyrMenu" + mName;
	var main_menu_pre = document.form1.main_menu_now.value;
	if (mName!=main_menu_pre)
	{
		// hide previous menu
		var preLay = "lyrMenu" + main_menu_pre;
		displayTable(preLay, "none");
		//hidelayer(preLay);
		//document.all[preLay].style.position  = "absolute";
		document.form1.main_menu_now.value = mName;

		var sub_menu_pre = document.form1.sub_menu_now.value;
		top_max = 80;
		top_min = 60;
		ie_move_v = 2;
		// hide previous sub menu
		if (sub_menu_pre!="")
		{
			var pLay = "lyrSM" + sub_menu_pre;
			move(pLay, top_max);
			document.form1.sub_menu_now.value = "";
			//nextAction = "hidelayer('"+pLay+"')";
		}
		//document.all[lay].style.position  = "relative";
		displayTable(lay, "");
		//showlayer(lay);

		MM_swapImage("imgCOURSE","","../images/header31_"+lang_used+"/btn_blinking_off.gif",1);
		MM_swapImage("imgSTUDENT","","../images/header31_"+lang_used+"/btn_blinking_off.gif",1);
		MM_swapImage("imgCOMMUNITY","","../images/header31_"+lang_used+"/btn_blinking_off.gif",1);
		MM_swapImage("imgCOURSE1","","../images/header31_"+lang_used+"/btn_course.gif",1);
		MM_swapImage("imgSTUDENT1","","../images/header31_"+lang_used+"/btn_student.gif",1);
		MM_swapImage("imgCOMMUNITY1","","../images/header31_"+lang_used+"/btn_community.gif",1);

		var nameLower = mName.toLowerCase();
		MM_swapImage("img"+mName,"","../images/header31_"+lang_used+"/btn_blinking_"+nameLower+".gif",1);
		MM_swapImage("img"+mName+"1","","../images/header31_"+lang_used+"/btn_"+nameLower+"_h.gif",1);
	}
	setToBlur();
}


function continueMainMenu(preLay, lay){
	document.all[preLay].style.position  = "absolute";
	//hidelayer(preLay);

	document.all[lay].style.position  = "relative";
	document.all[lay].style.filter = "Alpha(Opacity=0)";
	showlayer(lay);
	opacity_it = 20;
	fadeInTool(lay, 100, 1);
}


function showRoleChange(){
	if (!isLoaded)
	{
		return ;
	}

	top_max = 39;
	top_min = 10;
	ie_move_v = 1;
	move("lyrRole", top_max);
	setToBlur();
}

function updateLayerPos(lay, isLeft, isTop){
	if (typeof(document.all["img"+lay])=="undefined")
	{
		return;
	}

	var obj_pos = getObjectPosition(document.all["img"+lay]);
	lay = "lyrSM" + lay;

	var isUpdate = false;
	if (typeof(document.all[lay])!="undefined")
	{
		if (ie4) { isUpdate = (document.all[lay].style.visibility!="visible"); }
		if (ns4) { isUpdate = (document.layers[pa].document.layers[lay].visibility!="show"); }
		if (ns6) { isUpdate = (document.getElementById(lay).style.display!="block"); }
	}

	if (typeof(obj_pos)=="object" && isUpdate)
	{
		// ensure not show outside screen
		if (obj_pos.pos_left + document.all[lay].clientWidth > document.body.clientWidth)
		{
			obj_pos.pos_left = document.body.clientWidth - document.all[lay].clientWidth;
		}

		if (isLeft)
		{
			if (ie4) {document.all[lay].style.left = obj_pos.pos_left+'px';}
			if (ns4) {document.layers[pa].document.layers[lay].left = obj_pos.pos_left;}
			if (ns6) {document.getElementById(lay).style.left = obj_pos.pos_left+'px';}
		}
		if (isTop)
		{
			if (ie4) {document.all[lay].style.top = obj_pos.pos_top+'px';}
			if (ns4) {document.layers[pa].document.layers[lay].top = obj_pos.pos_top;}
			if (ns6) {document.getElementById(lay).style.top = obj_pos.pos_top +'px';}
		}
	}
}


function getMousePos(E){
	return Array(eval(Ex), eval(Ey));
}

function setToBlur(){
	if (ie4) { document.all["lyrLogo"].focus(); }
}

/* ########  TEXT VERSION  #############

Browser					Transparent Menu
---------------------------------------
IE 5 or above			Yes
NS 6					Yes
NS 4					Partial

##################################### */