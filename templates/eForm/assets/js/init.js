/*
var dynamicTableIDs = {
	'2': 'Dynamic Table 2',
	'1': 'Dynamic Table 1'
};
var fixedTableIDs = {
		'2': 'Fixed Table 1',
		'1': 'Fixed Table 1'
	};
*/
var fixedTableIDs = {};
var dynamicTableIDs = {};
var fields = [];
var actionButtons = [];
var templates = [];
var inputSets = [];
var custControlOrder = [ 'presetInputsets', "header", "paragraph", "select", "checkbox-group", "radio-group", "text", "textarea", "date", "number", "time" ];
var typeUserDisabledAttrs = {
		autocomplete: [ 'access' ],
		paragraph: ['subtype'],
		dateFieldElm: [ 'subtype' ],
		fixedTableElm: [ 'subtype', 'required' ],
		dynamicTableElm: [ 'subtype', 'required' ],
		timeFieldElm: [ 'subtype' ],
		subjectGroupElm: [ 'subtype', 'multiple' ],
		teacherElm: [ 'subtype', 'multiple' ],
		studentElm: [ 'subtype', 'multiple' ]
	};
var disabledAttrs = [ 'placeholder', 'description', 'access','className', 'step', 'toggle', 'name', 'value', 'inline' ];
var typeUserEvents = {
	"checkbox-group": {
		onadd: function(fld) {
			var $patternField = $('.field-options', fld);
			$patternField.append("<div style='padding:10px;text-align:right;'>" + langtxt["OptionRemark"] + "</div>");
		}
	},
	"select": {
		onadd: function(fld) {
			var $patternField = $('.field-options', fld);
			$patternField.append("<div style='padding:10px;text-align:right;'>" + langtxt["OptionRemark"] + "</div>");
		}
	},
	"radio-group": {
		onadd: function(fld) {
			var $patternField = $('.field-options', fld);
			$patternField.append("<div style='padding:10px;text-align:right;'>" + langtxt["OptionRemark"] + "</div>");
		}
	}
};