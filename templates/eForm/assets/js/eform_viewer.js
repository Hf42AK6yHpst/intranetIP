/************************************************/
/* debugMsgJS controller
/************************************************/
var debugMsgJS = {
	vrs: {
		allowConsole: false
	},
	ltr: {},
	cfn: {
		dConosle: function(msg, type) {
			if (!debugMsgJS.vrs.allowConsole) {
				window.console.log = function(){
				    window.console.log = function() {
				        return false;
				    }
				}
			} else {
				if (typeof msg == "undefined") msg = "";
				if (typeof type == "undefined") type = "info";
				var colorStyle = "background: #fff; color: #000";
				if (type.toLowerCase() == "error") {
					console.error(msg);
				} else if (type.toLowerCase() == "data") {
					console.log('%c DATA :: ' + msg + " ", colorStyle);
				} else {
					switch (type.toLowerCase()) {
						case "listener":
							colorStyle = "background: Gold; color: #000";
							break;
						case "function":
							colorStyle = "background: DodgerBlue; color: #fff";
							break;
						case "danger":
							colorStyle = "background: Crimson; color: #000";
							break;
						case "warning":
							colorStyle = "background: HotPink; color: #000";
							break;
						case "success":
							colorStyle = "background: LightGreen; color: #000";
							break;
					}
					if (colorStyle != "") {
						if (typeof msg == "string") {
							msg = "[" + msg;
							msg = msg.replace("JS.", "].");
							msg = msg.replace(".cfn.", " Function > ");
							msg = msg.replace(".ltr.", " Listener > ");
						}
						console.log('%c > ' + msg + " ", colorStyle);
					} else {
						console.log(" > " + msg + " ");
					}
				}
			}
		}
	}
};

var optionsJS = {
	vrs: {
		datepickerOpt: { 
			time: false,
			lang: (langtxt['picker_lang'] || 'en'),
			format : 'YYYY-MM-DD',
			nowButton: true,
			cancelText: (langtxt['picker_cancel'] || 'Cancel'),
			okText: (langtxt['picker_ok'] || 'OK'),
			nowText: (langtxt['picker_now'] || 'Today')
		},
		timepickerOpt: { 
			date: false,
			lang: (langtxt['picker_lang'] || 'en'),
			format : 'HH:mm',
			cancelText: (langtxt['picker_cancel'] || 'Cancel'),
			okText: (langtxt['picker_ok'] || 'OK')
		}
	}	
};

var dynamicTableJS = {
	vrs: {
		container: ".dynamicTable ",
		addBtn: "div input.addRowBtn",
		delBtn: "div input.delRowBtn",
		trTemplate: 'tr.TABLE_TPL',
		currCheck: false,
		fromTop: false
	},
	ltr: {
		checkboxClickHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("dynamicTableJS.ltr.checkboxClickHandler > Click", "listener");
			var isChecked = true;
			var thisTable = $(this).closest(dynamicTableJS.vrs.container);
			thisTable.addClass('fromTop');
			dynamicTableJS.vrs.fromTop = true;
			if ($(this).prop('checked')) {
				thisTable.addClass('onChecked');
				dynamicTableJS.vrs.currCheck = true;
			} else {
				thisTable.removeClass('onChecked');
				dynamicTableJS.vrs.currCheck = false;
			}
			
			if (thisTable.find("tbody tr:not(.TABLE_TPL)").length > 0) {
				thisTable.find("tbody tr:not(.TABLE_TPL)").each(function() {
					$(this).find('td:last input[type="checkbox"]').eq(0).trigger('click');
				});
			}
			dynamicTableJS.vrs.fromTop = false;
			thisTable.removeClass('fromTop');
		},
		focusDisableHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("dynamicTableJS.ltr.focusDisableHandler > Focus", "warning");
		    this.blur();
		},
		addColumnHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("dynamicTableJS.ltr.addColumnHandler", "listener");
			var langtxt = eformJS.vrs.langtxt;
			var mainParent = $(this).parent().parent().find(dynamicTableJS.vrs.container);
			if (typeof mainParent != "undefined") {
				var myTRObj = mainParent.find(dynamicTableJS.vrs.trTemplate).eq(0);
				if (typeof myTRObj != "undefined") {
					var strHTML = myTRObj.html();
					var TBBody = mainParent.find('tbody').eq(0);
					
					var maxRow = TBBody.closest(".elm-body").eq(0).find('span.dynno.max').eq(0).html();
					var totalRow = TBBody.find('tr').length;
					if (typeof maxRow == "undefined" || maxRow <= 0 || maxRow >= totalRow) {
						if (TBBody.find('tr').length > 0) {
							var nextID = TBBody.find('tr').length;
						}
						var rel = TBBody.find('tr:last').eq(0).attr('rel');
						if (typeof rel != "undefined") {
							nextID = parseInt(rel) + 1;
						}
						if (typeof strHTML != "undedined") {
							strHTML = "<tr rel='" + nextID + "'>" + strHTML.replace(/_TPL_/g, nextID ) + "</tr>";
						} else {
							strHTML = "";
						}
						TBBody.append(strHTML);
						$('.number-field').unbind('keydown', eformJS.ltr.onlyNumberHandler);
						$('.number-field').bind('keydown', eformJS.ltr.onlyNumberHandler);
						
//						$('[data-toggle="datepicker"]').datepicker(optionsJS.vrs.datepickerOpt); 
//						$('[data-toggle="datepicker"]').unbind('keydown', eformJS.ltr.datepickerKeydownHandler)
//						$('[data-toggle="datepicker"]').bind('keydown', eformJS.ltr.datepickerKeydownHandler)
//						
//						if ($('[data-toggle="timepicker"]').length > 0) {
//							$('[data-toggle="timepicker"]').each(function() {
//								var myOpt = optionsJS.vrs.timepickerOpt;
//								myOpt.now = $(this).val();
//								if (myOpt.now == "") {
//									myOpt.now = "00 : 00";
//								}
//								$(this).wickedpicker(myOpt);
//							});
//							$('.input-time').mask('00 : 00');
//						}
						
						if ($('[data-toggle="datepicker"]').length > 0) {
							$('[data-toggle="datepicker"]').bootstrapMaterialDatePicker(optionsJS.vrs.datepickerOpt);
						}
						
						if ($('[data-toggle="timepicker"]').length > 0) {
							$('[data-toggle="timepicker"]').bootstrapMaterialDatePicker(optionsJS.vrs.timepickerOpt);
						}
						
						if (mainParent.find("tbody tr").length > 2) {
							mainParent.find("th > input[type='checkbox']").eq(0).show();
						} else {
							mainParent.find("th > input[type='checkbox']").eq(0).hide();
						}
						if (mainParent.find("th > input[type='checkbox']").eq(0).prop("checked")) {
							mainParent.find("th > input[type='checkbox']").eq(0).trigger('click');
						};
						
						var thisForm = $(eformJS.vrs.container).eq(0);
						if (thisForm.find('input[required], textarea[required], select[required]').length > 0) {
							thisForm.find('input[required], textarea[required], select[required]').unbind('change keyup blur', eformJS.ltr.requireFieldHandler);
							thisForm.find('input[required], textarea[required], select[required]').bind('change keyup blur', eformJS.ltr.requireFieldHandler);
						}
						if (thisForm.find('input[value="other"]').length > 0) {
							thisForm.find('input[value="other"]').unbind('click', eformJS.ltr.otherOptionHandler);
							thisForm.find('input[value="other"]').bind('click', eformJS.ltr.otherOptionHandler);
						}
						if (thisForm.find('.input-field-other').length > 0) {
							thisForm.find('.input-field-other').unbind('focus change keyup', eformJS.ltr.otherFocusHandler);
							thisForm.find('.input-field-other').bind('focus change keyup', eformJS.ltr.otherFocusHandler);
						}
						if (mainParent.find("td > input[type='checkbox']").length > 0) {
							mainParent.find("td > input[type='checkbox']").unbind('click', dynamicTableJS.ltr.tbClickHandler);
							mainParent.find("td > input[type='checkbox']").bind('click', dynamicTableJS.ltr.tbClickHandler);
						}
					} else {
						if (typeof langtxt["AlertDialog"] == "undefined") {
							langtxt["AlertDialog"] = "Alert";
						}
						if (typeof langtxt["MaxRowLimit"] == "undefined") {
							langtxt["MaxRowLimit"] = "Reached the upper limit of the number of rows";
						}
						jAlert(langtxt["MaxRowLimit"], langtxt["AlertDialog"]);
					}
				}
			} 
			e.preventDefault();
		},
		tbClickHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("dynamicTableJS.ltr.tbClickHandler", "listener");
			var thisItem = $(this);
			var thisTable = $(this).closest(dynamicTableJS.vrs.container);
			if (thisTable.hasClass('fromTop')) {
				if (thisTable.hasClass('onChecked')) {
					$(this).prop('checked', true);
				} else {
					$(this).prop('checked', false);
				}
			} else {
				if (thisTable.hasClass('onChecked')) {
					thisTable.find("th > input[type='checkbox']").eq(0).prop("checked", false);
					thisTable.removeClass('onChecked');
				}
			}
			if (thisTable.find("td > input[type='checkbox']:checked").length > 0) {
				thisTable.parent().find(dynamicTableJS.vrs.delBtn).show();
			} else {
				thisTable.parent().find(dynamicTableJS.vrs.delBtn).hide();
			}
		},
		delColumnHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("dynamicTableJS.ltr.delColumnHandler", "listener");
			var langtxt = eformJS.vrs.langtxt;
			var thisButton = $(this);
			var mainParent = $(this).parent().parent().find(dynamicTableJS.vrs.container);
			if (mainParent.find("td > input[type='checkbox']:checked").length > 0) {
				jConfirm('Can you confirm this?', 'Confirmation Dialog', function(r) {
					if (r) {
						if (mainParent.find("tbody tr:not(.TABLE_TPL)").length > 1) {
							mainParent.find("tbody tr:not(.TABLE_TPL)").each(function() {
								if ($(this).find('td:last input[type="checkbox"]').eq(0).prop('checked')) {
									$(this).remove();
								}
							});
							mainParent.find("th > input[type='checkbox']").eq(0).prop("checked", false);
							mainParent.removeClass('onChecked');
							if (mainParent.find("td > input[type='checkbox']:checked").length == 0) {
								// thisButton.hide();
							}
						}
					}
				});
			} else {
				jAlert(langtxt["RowSelectAlert"], langtxt["AlertDialog"]);
			}
			e.preventDefault();
		}
	},
	cfn: {
		init: function() {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("dynamicTableJS.cfn.init", "info");
			if ($(dynamicTableJS.vrs.container).length > 0) {
				if ($(dynamicTableJS.vrs.container).parent().find(dynamicTableJS.vrs.addBtn).length > 0) { 
					$(dynamicTableJS.vrs.container).parent().find(dynamicTableJS.vrs.addBtn).bind('click', dynamicTableJS.ltr.addColumnHandler);
				}
				if ($(dynamicTableJS.vrs.container).parent().find(dynamicTableJS.vrs.delBtn).length > 0) { 
					$(dynamicTableJS.vrs.container).parent().find(dynamicTableJS.vrs.delBtn).bind('click', dynamicTableJS.ltr.delColumnHandler);
				}
				if ($(dynamicTableJS.vrs.container).find("th > input[type='checkbox']").length > 0) {
					$(dynamicTableJS.vrs.container).find("th > input[type='checkbox']").bind('click', dynamicTableJS.ltr.checkboxClickHandler);
				}
				if ($(dynamicTableJS.vrs.container).find("tbody tr").length > 2) {
					$(dynamicTableJS.vrs.container).find("th > input[type='checkbox']").eq(0).show();
				} else {
					$(dynamicTableJS.vrs.container).find("th > input[type='checkbox']").eq(0).hide();
				}
			}
			$('input[readonly], textarea[readonly]').bind('focus', dynamicTableJS.ltr.focusDisableHandler);
		}
	}
};
var eformJS = {
	vrs: {
		container: "form#userform",
		langtxt: []
	},
	ltr: {
		requireFieldHandler: function() {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("eformJS.ltr.requireFieldHandler", "listener");
			eformJS.cfn.checkForm($(this), this.type);
		},
		otherOptionHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("eformJS.ltr.otherOptionHandler", "listener");
			var thisForm = $(eformJS.vrs.container).eq(0);
			var hasOther = false;
			$(this).parent().parent().find('input:checked').each(function() {
				if ($(this).val() == "other") {
					hasOther = true;
				}
			});
			if (hasOther) {
				if (thisForm.find('.input-field-other').length > 0) {
					thisForm.find('.input-field-other').unbind('focus change keyup', eformJS.ltr.otherFocusHandler);
				}
				$(this).parent().find(".input-field-other").eq(0).focus();
				if (thisForm.find('.input-field-other').length > 0) {
					thisForm.find('.input-field-other').bind('focus change keyup', eformJS.ltr.otherFocusHandler);
				}
			} else {
				$(this).parent().find(".input-field-other").eq(0).val('');
			}
			// 
			// $(this).parent().find(".input-field-other").eq(0).bind('blur', eformJS.ltr.otherOptionBlurHandler);
		},
		otherOptionBlurHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("eformJS.ltr.otherOptionBlurHandler", "listener");
			var tmp = $(this).attr('id');
			id = tmp.replace("_other", "");
			if ($('#' + id).attr('type') == "checkbox") {
				eformJS.cfn.checkCheckboxElm($('#' + id));
			} else if ($('#' + id).attr('type') == "radio") {
				eformJS.cfn.checkRadioElm($('#' + id));
			}
		},
		otherFocusHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("eformJS.ltr.otherFocusHandler", "listener");
			var tmp = $(this).attr('id');
			id = tmp.replace("_other", "");
			if ($('#' + id).attr('type') == "checkbox") {
				eformJS.cfn.checkCheckboxElm($('#' + id));
			} else if ($('#' + id).attr('type') == "radio") {
				eformJS.cfn.checkRadioElm($('#' + id));
			}
			if (!$(this).parent().find('input[value="other"]').eq(0).prop('checked')) $(this).parent().find('input[value="other"]').eq(0).trigger('click');
		},
		onlyNumberHandler: function (e) {
	        // Allow: backspace, delete, tab, escape, enter and .
	        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	             // Allow: Ctrl+A, Command+A
	            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
	             // Allow: home, end, left, right, down, up
	            (e.keyCode >= 35 && e.keyCode <= 40)) {
	                 // let it happen, don't do anything
	                 return;
	        }
	        // Ensure that it is a number and stop the keypress
	        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	            e.preventDefault();
	        }
	    },
	    datepickerKeydownHandler: function(e) {
	    	e.preventDefault();
	    	return false;
	    }
	},
	cfn: {
		setLangTxt: function(txtArr) {
			eformJS.vrs.langtxt = txtArr; 
		},
		isValidEmailAddress: function (emailAddress) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("eformJS.cfn.isValidEmailAddress", "function");
			var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		    if (filter.test(emailAddress)) {
		        return true;
		    } else {
		        return false;
		    }
		},
		checkSelectElm: function(thisObj) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("eformJS.cfn.checkSelectElm", "function");
			var thisValid = true;
			var elmParent = thisObj.closest(".elm")
			var elmlabel = elmParent.find(".elmLabel.elmRequired").eq(0);
			if (elmParent.find('option:selected').length == 0) {
				thisValid = false;
				if (!thisObj.hasClass('error')) thisObj.addClass('error');
				if (typeof elmlabel != "undefined" && !elmlabel.hasClass('elmError')) { 
					elmlabel.addClass('elmError');
				}
			} else {
				if (thisObj.hasClass('error')) thisObj.removeClass('error');
				if (typeof elmlabel != "undefined" && elmlabel.hasClass('elmError')) { 
					elmlabel.removeClass('elmError');
				}
			}
			return thisValid;
		},
		checkValueElm: function(thisObj, thisType) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("eformJS.cfn.checkValueElm", "function");
			var thisValid = true;
			var elmlabel = thisObj.closest(".elm").find(".elmLabel.elmRequired").eq(0);
			
			if (thisObj.val() == "") {
				thisValid = false;
			} else {
				switch (thisType) {
					case "email":
						if (!eformJS.cfn.isValidEmailAddress(thisObj.val())) {
							thisValid = false;
						}
						break;
					case "text":
						if (thisObj.attr('data-toggle') == "timepicker" && thisObj.val() == "00 : 00") {
							thisValid = false;
						}
						break;
				}
			}
			if (thisType == "hidden") {
				if (thisValid) {
					if (thisObj.hasClass('error')) thisObj.removeClass('error');
					if (typeof thisObj.parent() != "undefined" && thisObj.parent().hasClass('elmError')) { 
						thisObj.parent().removeClass('elmError');
					}
				} else {
					if (!thisObj.hasClass('error')) thisObj.addClass('error');
					if (typeof thisObj.parent() != "undefined" && !thisObj.parent().hasClass('elmError')) { 
						thisObj.parent().addClass('elmError');
					}
				}
			} else {
				if (thisValid) {
					if (thisObj.hasClass('error')) thisObj.removeClass('error');
					if (typeof elmlabel != "undefined" && elmlabel.hasClass('elmError')) { 
						elmlabel.removeClass('elmError');
					}
				} else {
					if (!thisObj.hasClass('error')) thisObj.addClass('error');
					if (typeof elmlabel != "undefined" && !elmlabel.hasClass('elmError')) { 
						elmlabel.addClass('elmError');
					}
				}
			}
			return thisValid;
		},
		checkCheckboxElm: function(thisObj) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("eformJS.cfn.checkCheckboxElm", "function");
			var thisValid = true;
			var elmlabel = thisObj.closest(".elm").find(".elmLabel.elmRequired").eq(0);
			var checkboxElm = thisObj.attr('name');
			var hasOther = false;
			if ($("input[name='" + checkboxElm + "']:checked").length > 0) {
				$("input[name='" + checkboxElm + "']:checked").each(function() {
					if($(this).val() == "other") {
						hasOther = true;
					}
				});
				if (hasOther) {
					if (!eformJS.cfn.checkOtherElm(thisObj)) {
						thisValid = false;
					}
				} else {
					var otherObj = thisObj.parent().parent().find(".input-field-other").eq(0);
					if (typeof otherObj != "undefined") {
						if (otherObj.hasClass('error')) otherObj.removeClass('error');
						otherObj.val('');
					}
				}
			} else {
				thisValid = false;
			}
			if (thisValid) {
				if ($("input[name='" + checkboxElm + "']").hasClass('error')) $("input[name='" + checkboxElm + "']").removeClass('error');
				if ($("input[name='" + checkboxElm + "']").parent().hasClass('error')) $("input[name='" + checkboxElm + "']").parent().removeClass('error');
				if (typeof elmlabel != "undefined" && elmlabel.hasClass('elmError')) { 
					elmlabel.removeClass('elmError');
				}
			} else {
				if (!$("input[name='" + checkboxElm + "']").hasClass('error')) $("input[name='" + checkboxElm + "']").addClass('error');
				if (!$("input[name='" + checkboxElm + "']").parent().hasClass('error')) $("input[name='" + checkboxElm + "']").parent().addClass('error');
				if (typeof elmlabel != "undefined" && !elmlabel.hasClass('elmError')) { 
					elmlabel.addClass('elmError');
				}
			}
			return thisValid;
		},
		checkRadioElm: function(thisObj) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("eformJS.cfn.checkRadioElm", "function");
			var radioElm = thisObj.attr('name');
			var thisValid = true;
			var hasOther = false;
			$("input[name='" + radioElm + "']:checked").each(function() {
				if($(this).val() == "other") {
					hasOther = true;
				}
			});
			if (hasOther) {
				if (!eformJS.cfn.checkOtherElm(thisObj)) {
					thisValid = false;
				}
			} else {
				var otherObj = thisObj.parent().parent().find(".input-field-other").eq(0);
				if (typeof otherObj != "undefined") {
					if (otherObj.hasClass('error')) otherObj.removeClass('error');
					otherObj.val('');
				}
			}
			return thisValid;
		},
		checkOtherElm: function(thisObj) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("eformJS.cfn.checkOtherElm", "function");
			var otherObj = thisObj.parent().parent().find(".input-field-other").eq(0);
			var thisValid = true
			if (typeof otherObj != "undefined") {
				if (otherObj.val() == "") {
					if (!otherObj.hasClass('error')) otherObj.addClass('error');
					thisValid = false;
				} else {
					if (otherObj.hasClass('error')) otherObj.removeClass('error');
				}
			}
			return thisValid;
		},
		checkForm: function(obj, objtype) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("eformJS.cfn.checkForm", "function");
			var isValided = true;
			if (typeof obj == "undefined") {
				if (typeof $(eformJS.vrs.container).eq(0) != "undefined") {
					var thisForm = $(eformJS.vrs.container).eq(0);
					if (thisForm.find('input[required], textarea[required], select[required]').length > 0) {
						thisForm.find('input[required], textarea[required], select[required]').each(function() {
							switch (this.type) {
							case "select-multiple":
							case "select":
								if (!eformJS.cfn.checkSelectElm($(this))) isValided = false; break;
							case "tel":
							case "number":
							case "email":
							case "text":
							case "hidden":
							case "textarea":
								if (/_TPL_/i.test($(this).attr('name'))) {
									// isValided = true;
								} else if (!eformJS.cfn.checkValueElm($(this), this.type)) isValided = false;
								break;
							case "checkbox":
								if (/_TPL_/i.test($(this).attr('name'))) {
									// isValided = true;
								} else if (!eformJS.cfn.checkCheckboxElm($(this))) isValided = false;
								break;
							case "radio":
								if (/_TPL_/i.test($(this).attr('name'))) {
									// isValided = true;
								} else if (!eformJS.cfn.checkRadioElm($(this))) isValided = false;
								break;
							default:
								break;
							}
						});
					}
					if (!isValided) {
						var langtxt = eformJS.vrs.langtxt;
						if (typeof langtxt["AlertDialog"] == "undefined" || langtxt["AlertDialog"] == "") {
							langtxt["AlertDialog"] = 'Alert Dialog';
						}
						if (typeof langtxt["PromptRequiredField"] == "undefined" || langtxt["PromptRequiredField"] == "") {
							langtxt["PromptRequiredField"] = 'Please make sure all required fields are filled out correctly!';
						}
						jAlert(langtxt["PromptRequiredField"], langtxt["AlertDialog"]);
						$('#popup_ok').prop('disabled', false);
						if (typeof $('.elmError:first') != "undefined" && $('.elmError:first').length > 0) {
							$('html, body').animate({
							    scrollTop: $('.elmError:first').offset().top - 100
							}, 800);
						} else if (typeof $('.error:first') != "undefined" && $('.error:first').length > 0) {
							$('html, body').animate({
							    scrollTop: $('.error:first').offset().top - 100
							}, 800);
						}
					}
				}
			} else {
				switch (objtype) {
				case "select":
				case "select-multiple":
					isValided = eformJS.cfn.checkSelectElm(obj);
					break;
				case "checkbox":
					isValided = eformJS.cfn.checkCheckboxElm(obj);
					break;
				case "tel":
				case "email":
				case "number":
				case "text":
				case "textarea":
				case "hidden":
					isValided = eformJS.cfn.checkValueElm(obj, objtype);
					break;
				case "radio":
					isValided = eformJS.cfn.checkRadioElm(obj);
					break;
				}
			}
			return isValided; 
		},
		init: function() {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("eformJS.cfn.init", "info");
			var thisForm = $(eformJS.vrs.container).eq(0);
			if (typeof thisForm != "undefined") {
				/*
				$('.form-wrap .elm').each(function(index) {
					$(this).delay(index * 100).fadeIn(200);
				}); */
				if (thisForm.find('input[value="other"]').length > 0) {
					thisForm.find('input[value="other"]').bind('click', eformJS.ltr.otherOptionHandler);
				}
				if (thisForm.find('.input-field-other').length > 0) {
					thisForm.find('.input-field-other').bind('focus change keyup', eformJS.ltr.otherFocusHandler);
				}
				
				$('.number-field').bind('keydown', eformJS.ltr.onlyNumberHandler);
				
//				if ($('[data-toggle="datepicker"]').length > 0) {
//					$('[data-toggle="datepicker"]').datepicker(optionsJS.vrs.datepickerOpt);
//					$('[data-toggle="datepicker"]').bind('keydown', eformJS.ltr.datepickerKeydownHandler)
//				}
//				if ($('[data-toggle="timepicker"]').length > 0) {
//					$('[data-toggle="timepicker"]').each(function() {
//						var myOpt = optionsJS.vrs.timepickerOpt;
//						myOpt.now = $(this).val();
//						if (myOpt.now == "") {
//							myOpt.now = "00 : 00";
//						}
//						$(this).wickedpicker(myOpt);
//					});
//					$('.input-time').mask('00 : 00');
//				}
				
				if ($('[data-toggle="datepicker"]').length > 0) {
					$('[data-toggle="datepicker"]').bootstrapMaterialDatePicker(optionsJS.vrs.datepickerOpt)
				}
				
				if ($('[data-toggle="timepicker"]').length > 0) {
					$('[data-toggle="timepicker"]').bootstrapMaterialDatePicker(optionsJS.vrs.timepickerOpt)
				}
				
				
				if (thisForm.find('input[required], textarea[required], select[required]').length > 0) {
					thisForm.find('input[required], textarea[required], select[required]').bind('change keyup blur', eformJS.ltr.requireFieldHandler);
				}
			}
		}
	}
};

var tagsJS = {
	vrs: {
		elm : 'a.tags'
	},
	ltr: {
		removeTagHandler: function(e) {
			var thisElm = $(this);
			var thisElmID = $(this).attr('rel');
			var elmJsonObj = thisElm.parent().parent().find('input[type="hidden"]').eq(0);
			var elmTagsObj = thisElm.parent().parent().find('.tagsbox').eq(0);
			var elmJson = JSON.parse(elmJsonObj.val());
			
			if (typeof elmJson.selectOpt != "undefined" && elmJson.selectOpt.length > 0) {
				var newSelectOpt = [];
				$.each(elmJson.selectOpt, function(key, value) {
					if (value != thisElmID) {
						newSelectOpt.push(value);
					}
				});
				if (newSelectOpt.length > 0) {
					elmJson.selectOpt = newSelectOpt;
					elmJsonObj.val(JSON.stringify(elmJson));
				} else {
					elmTagsObj.html('');
					elmJsonObj.val('');
				}
			} else {
				elmTagsObj.html('');
				elmJsonObj.val('');
			}
			thisElm.remove();
			e.preventDefault();
		}
	},
	cfn : {
		bindings: function() {
			$(tagsJS.vrs.elm).unbind('click', tagsJS.ltr.removeTagHandler);
			$(tagsJS.vrs.elm).bind('click', tagsJS.ltr.removeTagHandler);
		},
		init: function() {
			if ($(tagsJS.vrs.elm).length > 0) {
				tagsJS.cfn.bindings();				
			}
		}
	}
};

$(document).ready(function() {
	if (typeof langtxt != "undefined") {
		eformJS.cfn.setLangTxt(langtxt);
	}
	eformJS.cfn.init();
	dynamicTableJS.cfn.init();
	tagsJS.cfn.init();
});
