<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
if (file_exists($PATH_WRT_ROOT."lang/eForm/eform.lang.$intranet_session_language.php")) include_once($PATH_WRT_ROOT."lang/eForm/eform.lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/eForm/eformConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/eForm/libeform.php");
include_once($PATH_WRT_ROOT."includes/eForm/libeform_ui.php");

if (!class_exists("FormCommon")) include_once($PATH_WRT_ROOT."includes/eForm/class.formcommon.php");
if (!class_exists("FormTemplate")) include_once($PATH_WRT_ROOT."includes/eForm/class.formtemplate.php");

intranet_auth();
intranet_opendb();

$libeform = new libeform();
$FormTemplate = new FormTemplate();

$libeform->checkAccessRight();

$thistoken = md5($_POST["tplID"] . $_SESSION["UserID"]);

if (!$plugin['eForm']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
if ($thistoken == $_POST["token"]) {
	$templateID = $_POST["tplID"];
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="theme-color" content="#999999" />
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#4285f4">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#4285f4">

	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<link rel="stylesheet" type="text/css" media="screen" href="/templates/eForm/assets/bootstrap-3.3.7/css/bootstrap.min.css">
<?php if ($_GET["type"] == "table") { ?>
	<link rel="stylesheet" type="text/css" href="/templates/eForm/assets/css/tablebuilder.css?v=<?php echo time(); ?>">
<?php } else { ?>
	<link rel="stylesheet" type="text/css" href="/templates/eForm/assets/css/formbuilder.css">
<?php } ?>
	<title><?php echo $Lang["FormBuilder"]["Title"]; ?></title>
</head>
<body>
<div class="content"><br>
	<div id="stage1" class="build-wrap"></div>
	<form class="render-wrap"></form>
	<button id="edit-form">Edit Form</button>
	<div class="action-buttons">
		<button id="getJSON" type="button"><?php echo $Lang["eForm"]["Save"]; ?></button>
		<button id="btnclose" type="button"><?php echo $Lang["eForm"]["CancelAndClose"]; ?></button>
	</div>
</div><br><br>

<script src="/templates/eForm/assets/js/vendor.js"></script>
<script src="/templates/eForm/assets/js/form-builder.min.js"></script>
<script src="/templates/eForm/assets/js/form-render.min.js"></script>
<script src="/templates/eForm/assets/js/control_plugins/datefield.js?v=<?php echo time(); ?>"></script>
<script src="/templates/eForm/assets/js/control_plugins/datetimefield.min.js?v=<?php echo time(); ?>"></script>
<script src="/templates/eForm/assets/js/control_plugins/datetimefield.min.js?v=<?php echo time(); ?>"></script>
<script src="/templates/eForm/assets/js/control_plugins/daulselectpicker.js?v=<?php echo time(); ?>"></script>
<?php if ($_GET["type"] == "table") { ?>
<?php } else { ?>
	<script src="/templates/eForm/assets/js/control_plugins/fixedtable.js?v=<?php echo time(); ?>"></script>
	<script src="/templates/eForm/assets/js/control_plugins/dynamictable.js?v=<?php echo time(); ?>"></script>
<?php } ?>
<script src="/templates/eForm/assets/bootstrap-3.3.7/js/bootstrap-datepicker.min.js"></script>
<script src="/templates/eForm/assets/js/init.js?v=<?php echo time(); ?>"></script>
<script type='text/javascript'>
	var dynamicTableIDs = {};
	var fixedTableIDs = {};

	var langtxt = [];
<?php if (count($Lang["eForm"]["jsMsg"]) > 0) {
	foreach ($Lang["eForm"]["jsMsg"] as $kk => $vv) {
?>
	langtxt["<?php echo $kk; ?>"] = "<?php echo $vv; ?>";
<?php
	}
}
?>
	$(document).ready(function() {
		if (typeof window.top.parent.getTableID == "function") {
			var tableID = window.top.parent.getTableID();
			if (typeof tableID["fixedTable"] != "undefined") {
				fixedTableIDs = tableID["fixedTable"]; 
			}
			if (typeof tableID["dynamicTable"] != "undefined") {
				dynamicTableIDs = tableID["dynamicTable"];
			}
		}
		var initField = window.top.parent.formJSONinfo;
		var typeUserAttrs = {
				text: {
				maxlength: {
					label: "<?php echo $Lang["eForm"]["FB_MaxLength"]; ?>",
					type: "number"},
					subtype: {
						label: "<?php echo $Lang["eForm"]["FB_Type"]; ?>",
						options:{
							"text": "<?php echo $Lang["eForm"]["FB_Text"]; ?>",
							"email" : "<?php echo $Lang["eForm"]["FB_Email"]; ?>",
							"tel": "<?php echo $Lang["eForm"]["FB_Tel"]; ?>"
						}
					}
				},
				textarea: {
					subtype: {
						label: "<?php echo $Lang["eForm"]["FB_Type"]; ?>",
						options: {
							"textarea": "<?php echo $Lang["eForm"]["FB_Textarea"]; ?>" /*,"quill": "HTML Editor"*/
						}
					}
				},
				checkbox_group: {
					toggle: {
						style: "display:none"
					}
				},
				fixedTableElm: {
					value: {
						label: '<?php echo $Lang["eForm"]["FB_TableID"]; ?>',
						options: fixedTableIDs
					}
				},
				dynamicTableElm: {
					value: {
						label: '<?php echo $Lang["eForm"]["FB_TableID"]; ?>',
						options: dynamicTableIDs
					}
				},
				subjectGroupElm: {
					subtype: {
						label: "<?php echo $Lang["eForm"]["FB_PickerOption"]; ?>",
						options:{
							"single": "<?php echo $Lang["eForm"]["FB_SingleValue"]; ?>",
							"multiple" : "<?php echo $Lang["eForm"]["FB_MultipleValue"]; ?>"
						}
					},
					max: {
						label: "<?php echo $Lang["eForm"]["FB_SelectMAX"]; ?>",
						type: 'number',
						maxlength: '4',
						min: '0'
					}
				},
				teacherElm: {
					subtype: {
						label: "<?php echo $Lang["eForm"]["FB_PickerOption"]; ?>",
						options:{
							"single": "<?php echo $Lang["eForm"]["FB_SingleValue"]; ?>",
							"multiple" : "<?php echo $Lang["eForm"]["FB_MultipleValue"]; ?>"
						}
					},
					max: {
						label: "<?php echo $Lang["eForm"]["FB_SelectMAX"]; ?>",
						type: 'number',
						maxlength: '4',
						min: '0'
					}
				},
				studentElm: {
					subtype: {
						label: "<?php echo $Lang["eForm"]["FB_PickerOption"]; ?>",
						options:{
							"single": "<?php echo $Lang["eForm"]["FB_SingleValue"]; ?>",
							"multiple" : "<?php echo $Lang["eForm"]["FB_MultipleValue"]; ?>"
						}
					},
					max: {
						label: "<?php echo $Lang["eForm"]["FB_SelectMAX"]; ?>",
						type: 'number',
						maxlength: '4',
						min: '0'
					}
				}
		};
		var fbOptions = {
			onSave: function(e, formData) {
				toggleEdit();
				$('.render-wrap').formRender({
					formData: formData,
					templates: templates
				});
				window.sessionStorage.setItem('formData', JSON.stringify(formData));
			},
			stickyControls: {
				enable: true
			},
			sortableControls: true,
			fields: fields,
			editOnAdd: true,
			defaultFields: initField, // init data
			templates: templates,
			disabledAttrs: disabledAttrs,
			typeUserDisabledAttrs: typeUserDisabledAttrs,
			typeUserAttrs: typeUserAttrs,
			typeUserEvents: typeUserEvents,
			disableInjectedStyle: false,
			disableFields: ['autocomplete', 'file', 'button', 'hidden'],
			controlPosition: 'right',
			showActionButtons: false,
			controlOrder: custControlOrder,
			i18n: {
	    		locale: '<?php
	    		if ($_GET["type"] == "table") {
	    			echo "tbl_" . $intranet_session_language;
	    		} else {
	    			echo $intranet_session_language;
	    		}
	    		?>',
				location: "assets/js/i18n/",
				extension: '.lang'
			}
		};
		var formData = window.sessionStorage.getItem('formData');
		var editing = true;
		if (formData) {
			fbOptions.formData = JSON.parse(formData);
		}
		function toggleEdit() {
			document.body.classList.toggle('form-rendered', editing);
			return editing = !editing;
		}
		// fbOptions.formData = setFormData;
	  	var formBuilder = $('#stage1').formBuilder(fbOptions);
		var fbPromise = formBuilder.promise;

		fbPromise.then(function(fb) {
			document.getElementById('getJSON').addEventListener('click', function() {
				// alert(formBuilder.actions.getData('json', true));
				window.top.parent.updateFromThickbox(formBuilder.actions.getData('json', true));
			});
			
			document.getElementById('btnclose').addEventListener('click', function() {
				window.top.parent.tb_remove();				
			});
		});
		document.getElementById('edit-form').onclick = function() {
			toggleEdit();
		};
	});
</script>
</body>
</html>