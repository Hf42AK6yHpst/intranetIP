<?php
// using: 

###################### Change Log [Start]
#
#	Date:	2012-01-18	Yuen
#			i.display trademark - authorized to school 
#			ii. make sure "/images/2020a/logo_eclass_footer.gif" is identical and not replaced by other GIF
#
#	Date:	2011-12-08	Yuen
#			report the slow page/script to eClass
#
#	Date:	2010-10-12	Yuen
#			display the number of DB queries executed
#
#	Date:	2010-07-26	YatWoon
#			update the eclass logo display layout
#
###################### Change Log [End]


global $PATH_WRT_ROOT, $EchoQuery, $DebugMode, $eclass_school_copright, $eclass_school_copright_type, $Lang;


if ($eclass_school_copright!="")
{
	$school_name_dec = DECRYPT_PASSWORD($eclass_school_copright);
	if ($eclass_school_copright_type=="TRIAL")
	{
		$wording_to_use = $Lang['General']['sys_copyright_trial'];
	} else
	{
		$wording_to_use = $Lang['General']['sys_copyright_purchased'];		
	}
	$eclass_school_copright_footer = str_replace("[=schoolname=]", $school_name_dec, $wording_to_use);

	if (md5(get_file_content($intranet_root."/images/2020a/logo_eclass_footer.gif"))!="980442a2a7b4cb596f011830d5afd907")
	{
		echo "<script>\nalert('Warning! eClass logo cannot be found. Please contact support@broadlearning.com.');\n</script>\n";
		echo md5(get_file_content($intranet_root."/images/2020a/logo_eclass_footer.gif")); //$intranet_root."/images/2020a/logo_eclass_footer.gif";
	}
}

# to verify if $UserID, $UserType are changed by mistake
if ($DebugMode)
{
	function DebugVerifySessions($SYS_DEBUG_SESSION, $SYS_SESSION, $KEY_ARRAY)
	{
		global $_SERVER;
		
		if (is_array($SYS_DEBUG_SESSION) && sizeof($SYS_DEBUG_SESSION)>0)
		{
			foreach ($SYS_DEBUG_SESSION AS $SYS_Key => $SYS_Key_Value)
			{
				if (!is_array($SYS_Key_Value))
				{
					if ($SYS_DEBUG_SESSION[$SYS_Key] != $SYS_SESSION[$SYS_Key])
					{
						$Keys_Str = "";		
						$Keys_StrLog = "";				
						for ($i=0; $i<sizeof($KEY_ARRAY); $i++)
						{
							$Keys_Str .= "['".$KEY_ARRAY[$i]."']";
							$Keys_StrLog .= $KEY_ARRAY[$i]."_";
						}
						$Keys_Str .= "['".$SYS_Key."']";
						$Keys_StrLog .= "_".$SYS_Key;
						
						# record the change once only (in order to locate the first change!)
						if (!isset($_SESSION["DEBUG_ARRAY_ERROR"][$Keys_StrLog]) || $_SESSION["DEBUG_ARRAY_ERROR"][$Keys_StrLog]=="")
						{						
							$urlSrc = (getenv("SCRIPT_NAME")!="") ? getenv("SCRIPT_NAME") : $_SERVER["SCRIPT_NAME"];
							$_SESSION["DEBUG_ARRAY_ERROR"][$Keys_StrLog] = "WARNING: <font color='red'>\$_SESSION".$Keys_Str."</font> changed from \"".$SYS_DEBUG_SESSION[$SYS_Key] . "\" to \"".$SYS_SESSION[$SYS_Key]."\"! <p>This change occurs in <font color='red'>$urlSrc or previous page</font>. Please trace and do not use reserved PHP variable nor update this session value.</p>\n\r";
						} else
						{
							//debug($Keys_Str, $Keys_StrLog);
						}
					}
				} else
				{
					$NEW_KEY_ARRAY = $KEY_ARRAY;
					$NEW_KEY_ARRAY[] = $SYS_Key;
					DebugVerifySessions($SYS_DEBUG_SESSION[$SYS_Key], $SYS_SESSION[$SYS_Key], $NEW_KEY_ARRAY);
				}		
			}	
		}
	}
	
	DebugVerifySessions($_SESSION["DEBUG_ARRAY"], $_SESSION, $KEY_ARRAY=null);
	
	$SessionValueChangedWanring = "";
	
	if (is_array($_SESSION["DEBUG_ARRAY_ERROR"]) && sizeof($_SESSION["DEBUG_ARRAY_ERROR"])>0)
	{
		foreach ($_SESSION["DEBUG_ARRAY_ERROR"] AS $i_key => $i_value)
		{
			$SessionValueChangedWanring .= $i_value . "<br />";
		}
	} 
	
	if ($SessionValueChangedWanring!="")
	{
		$SessionValueChangedWanringHTML = "<div style='border:5px dashed yellow; position:absolute; left:70px; top:70px; z-index:999; width: 800px; background: black;'><p><font size='4'><font color='white'><b>".$SessionValueChangedWanring."</b></font></font></p></div>";
	}
}


if ($DebugMode && $EchoQuery)
{
	$lu = new libdb();
	$lu->db_show_debug_log();
	
}


unset($li, $la, $lb, $lu, $lh, $linterface, $lcycleperiods, $lportal);
unset($libreportcard, $header_lu, $ls, $libenroll, $lrepairsystem, $header_lc, $lwebmail, $header_lsysacc, $header_lftp, $header_lc);

global $extra_setting, $userBrowser, $special_feature;


$ProcessTime = round(StopTimer(2, true, "GLB_StartTime"), 4);
//debug(time());
if (isset($special_feature['EnablePerformanceReport']) && $special_feature['EnablePerformanceReport']) 
{
	$ExeTimeLimit = ($special_feature['EnablePerformanceReportByExceedSecond']!="" && $special_feature['EnablePerformanceReportByExceedSecond']>0) ? $special_feature['EnablePerformanceReportByExceedSecond'] : 12; // default = 12 seconds
	
	# report to eClass if necessary
	if ($ProcessTime!="" && $ProcessTime>$ExeTimeLimit && $ProcessTime<1200)
	{
		if (trim($_SESSION['ChineseName'])!="")
		{
			$UserName = trim($_SESSION['ChineseName']);
		}
		if (trim($_SESSION['EnglishName'])!="")
		{
			$UserName .= (($UserName!="") ? ", " : "") . trim($_SESSION['EnglishName']);
		}
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$CurrentScript = urlencode($_SERVER["REQUEST_URI"]);
		@curl_setopt($ch, CURLOPT_URL, "http://ip25-tc-demo2.eclass.com.hk/api/school_report.php?ProcessTime=".$ProcessTime."&TS=".time()."&uid=".$_SESSION['UserID']."&SchoolURL=".urlencode($_SERVER['HTTP_HOST'])."&Script=".$CurrentScript."&Browser=".urlencode($userBrowser->browsertype)."&UserName=".urlencode($UserName));
		
		// grab URL and pass it to the browser
		$data_from_checking = curl_exec($ch); 
		if (trim($data_from_checking)!="" && trim($data_from_checking)!="1")
		{
			# show error (not including duplicated report by same user) occurs
			hdebug($data_from_checking);
		}
	}
}

if ($extra_setting['show_process_time'])
{
	$memory_benchmrk = "Total memory used in this page: <font color='yellow' size='3'>".number_format((memory_get_usage())/1024)."KB (".number_format((memory_get_usage())/1024/1024, 1)."MB)";
	$ProcessTimeShow = ($ProcessTime>1) ? "<font color='red' size='4'>".$ProcessTime."</font>" : "<font color='yellow' size='3'>".$ProcessTime."</font>";
	$QueryExecuted = "QUERIES : ". (($GLOBALS[debug][db_query_count]>10) ? "<font color='red' size='4'>" : "<font color='yellow' size='3'>").$GLOBALS[debug][db_query_count] . "</font>";
	$ProcessTimeShowHTML = "<table width='100%' style='border:1px dotted #BBBBBB' border='0' cellpadding='0' cellspacing='0' id='ProcessTimeTable'><tr><td align='center' bgcolor='#000000'><font color='white' size='2'>Page generated in {$ProcessTimeShow} seconds.  $memory_benchmrk.</font> $QueryExecuted </td></tr></table>\n";
	$ProcessTimeShowHTML .= "<script language='javascript'>\nsetTimeout(\"displayTable('ProcessTimeTable', 'none')\", 9000);\n</script>\n";
}


?>
<?php if ($eclass_school_copright_footer!="") {?>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
<tr><td class="footer"><?=$eclass_school_copright_footer?></td></tr>
</table>
<?php } ?>
<?=$ProcessTimeShowHTML?>
<?=$SessionValueChangedWanringHTML?>
<? /*
<img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/10x10.gif" id="hidden_image" name="hidden_image" border="0" width="0" height="0"/>
<div id="divStayTopLeft" style="position:absolute; z-index:-1;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#999999" id="eclassfooter">
<tr>
  <td><?=$ProcessTimeShowHTML?></td>
  <td align="right"><span class="footer">Powered by</span><a href="http://www.eclass.com.hk" target="_blank"><img src="<?=$image_path?>/2020a/logo_eclass_footer.gif" border="0" align="absmiddle"></a></td>
  <td width="20"><img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/10x10.gif" width="20" height="20" /></td>
</tr>
</table>
</div>

<script type="text/javascript">
//Enter "frombottom" or "fromtop"
var verticalpos="frombottom";
<?=$float_js?>
</script>

<? */ ?>