<?php

global $home_header_no_EmulateIE7, $special_feature, $favicon_image, $customCSS, $module_custom;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- meta to tell IE8 to run backward compatible mode -->
<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"  /> 
<META Http-Equiv="Cache-Control" Content="no-cache">

<? if ($home_header_no_EmulateIE7){ ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<? } 
 elseif (!$home_header_no_EmulateIE7){ ?>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<? } ?>
<link href="/templates/<?=$LAYOUT_SKIN?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="/templates/<?=$LAYOUT_SKIN?>/css/content_30.css" rel="stylesheet" type="text/css">
<link href="/templates/<?=$LAYOUT_SKIN?>/css/content_25_catholic.css" rel="stylesheet" type="text/css">
<?php if($customCSS){?>
<link href="/templates/<?=$LAYOUT_SKIN?>/css/<?=$customCSS?>" rel="stylesheet" type="text/css">
<?php } ?>

<script language="JavaScript" src="/templates/brwsniff.js"></script>
<?php
// code to include newer jquery library if is going to staff attendance 3 pages for case: 2010-1216-0911-56073
if (stristr($_SERVER['REQUEST_URI'],'StaffMgmt/attendance') !== false ) {?>
<script language="JavaScript" src="/templates/jquery/jquery-1.4.4.min.js"></script>
<?
}elseif($module_custom['jQuery']['version'] == '1.12.4'){?>
<script language="JavaScript" src="/templates/jquery/jquery-1.12.4.min.js"></script>
<script language="JavaScript" src="/templates/jquery/jquery-migrate-1.4.1.min.js"></script>
<script language="JavaScript" src="/templates/jquery/jquery.PrintArea.js"></script>
<script language="JavaScript" src="/templates/jquery/jquery.blockUI-2.7.js"></script>
<?}else {?>
<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
<?}?>
<script language="JavaScript" src="/templates/script.js"></script>
<script language="JavaScript" src="/templates/2007script.js"></script>
<script language="JavaScript" src="/templates/ajax.js"></script>
<script language="JavaScript" src="/templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="/templates/ajax_connection.js"></script>
<script language="JavaScript" src="/templates/swf_object/swfobject.js"></script>
<script language="JavaScript" src="/lang/script.<?= $intranet_session_language?>.js"></script>
<?php if($module_custom['jQuery']['version'] != '1.12.4'){?>
<script language="JavaScript" src="/templates/jquery/jquery.blockUI.js"></script>
<?php }?>
<script language="JavaScript" src="/templates/jquery/jquery.scrollTo-min.js"></script>
<script language="JavaScript" src="/templates/jquery/jqueryslidemenu.js"></script>
<script language="JavaScript" src="/templates/<?=$LAYOUT_SKIN?>/js/SpryValidationTextField.js"></script>


<link id="page_favicon" href="/images/<?=$favicon_image?>" rel="icon" type="image/x-icon" />
<title><?=$Lang['Header']['HomeTitle']." ".$KISSchoolName?></title>
<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->
</head>



<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<?=$bubbleMsgBlock?>

<?=$bubbleMsgBlockSysAbout?>

<?=$bubbleMsgBlockSysTimeout?>


<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" >
	<tr>
		<td valign="top">