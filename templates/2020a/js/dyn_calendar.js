/**
* Filename.......: calendar.js
* Project........: Popup Calendar
* Last Modified..: $Date: 2002/07/22 18:17:05 $
* CVS Revision...: $Revision: 1.2 $
* Copyright......: 2001, 2002 Richard Heyes
*/

/**
* Global variables
*/
	dynCalendar_layers          = new Array();
	dynCalendar_mouseoverStatus = false;
	dynCalendar_mouseX          = 0;
	dynCalendar_mouseY          = 0;

/**
* The calendar constructor
*
* @access public
* @param string objName      Name of the object that you create
* @param string callbackFunc Name of the callback function
* @param string OPTIONAL     Optional layer name
* @param string OPTIONAL     Optional images path
*/
	function dynCalendar(objName, callbackFunc)
	{
		/**
        * Properties
        */
		// Todays date
		this.today          = new Date();
		this.date           = this.today.getDate();
		this.month          = this.today.getMonth();
		this.year           = this.today.getFullYear();

		this.objName        = objName;
		this.callbackFunc   = callbackFunc;
		this.imagesPath     = arguments[2] ? arguments[2] : 'images/';
		this.layerID        = arguments[3] ? arguments[3] : 'dynCalendar_layer_' + dynCalendar_layers.length;

		this.offsetX        = 5;
		this.offsetY        = 5;

		// [eCorp]
		this.pos_top	  = null;
		this.pos_left	  = null;

		this.useMonthCombo  = true;
		this.useYearCombo   = true;
		this.yearComboRange = 5;

		this.currentMonth   = this.month;
		this.currentYear    = this.year;

		/**
        * Public Methods
        */
		this.show              = dynCalendar_show;
		this.writeHTML         = dynCalendar_writeHTML;

		// Accessor methods
		this.setOffset         = dynCalendar_setOffset;
		this.setOffsetX        = dynCalendar_setOffsetX;
		this.setOffsetY        = dynCalendar_setOffsetY;
		this.setImagesPath     = dynCalendar_setImagesPath;
		this.setMonthCombo     = dynCalendar_setMonthCombo;
		this.setYearCombo      = dynCalendar_setYearCombo;
		this.setCurrentMonth   = dynCalendar_setCurrentMonth;
		this.setCurrentYear    = dynCalendar_setCurrentYear;
		this.setYearComboRange = dynCalendar_setYearComboRange;
		this.setDate	     = dynCalendar_setDate;

		// [eCorp]
		this.setPosition	     = dynCalendar_setPosition;


		/**
        * Private methods
        */
		// Layer manipulation
		this._getLayer         = dynCalendar_getLayer;
		this._hideLayer        = dynCalendar_hideLayer;
		this._showLayer        = dynCalendar_showLayer;
		this._setLayerPosition = dynCalendar_setLayerPosition;
		this._setHTML          = dynCalendar_setHTML;

		// Miscellaneous
		this._getDaysInMonth   = dynCalendar_getDaysInMonth;
		this._mouseover        = dynCalendar_mouseover;

		/**
        * Constructor type code
        */
		dynCalendar_layers[dynCalendar_layers.length] = this;
		this.writeHTML();
	}


	function dynCalendar_setDate(myValue)
	{
		var tmpArr = myValue.split("-");
		var date = Number(tmpArr[2]);
		var month = Number(tmpArr[1]);
		var year = Number(tmpArr[0]);
		if (date>0 && month>0 && year>0)
		{
			this.date           = date;
			this.month          = month-1;
			this.year           = year;
			this.currentMonth   = this.month;
			this.currentYear    = this.year;
		}

		return;
	}

/**
* Shows the calendar, or updates the layer if
* already visible.
*
* @access public
* @param integer month Optional month number (0-11)
* @param integer year  Optional year (YYYY format)
*/
	function dynCalendar_show()
	{
		// Variable declarations to prevent globalisation
		var month, year, monthnames, numdays, thisMonth, firstOfMonth;
		var ret, row, i, cssClass, linkHTML, previousMonth, previousYear;
		var nextMonth, nextYear, prevImgHTML, prevLinkHTML, nextImgHTML, nextLinkHTML;
		var monthComboOptions, monthCombo, yearComboOptions, yearCombo, html;

		this.currentMonth = month = arguments[0] != null ? arguments[0] : this.currentMonth;
		this.currentYear  = year  = arguments[1] != null ? arguments[1] : this.currentYear;

		monthnames = new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec');
		numdays    = this._getDaysInMonth(month, year);

		thisMonth    = new Date(year, month, 1);
		firstOfMonth = thisMonth.getDay();

		// First few blanks up to first day
		ret = new Array(new Array());
		for(i=0; i<firstOfMonth; i++){
			ret[0][ret[0].length] = '<td>&nbsp;</td>';
		}

		// Main body of calendar
		row = 0;
		i   = 1;
		while(i <= numdays)
		{
			if (ret[row].length == 7)
			{
				ret[++row] = new Array();
			}

			/**
            * Generate this cells' HTML
            */
			cssClassBG = (i == this.date && month == this.month && year == this.year) ? "calendartoday" : "";
			cssClass = (ret[row].length==0) ? "calendarholiday" : "calendarday";
			linkHTML = '<a href="javascript:' + this.callbackFunc + '(' + i + ',' + (Number(month) + 1) + ',' + year + ',0)" class="'+ cssClass +'">' + (i++) + '</a>';
			ret[row][ret[row].length] = '<td class="' + cssClassBG + '" onMouseOver="this.className=\'calendarselect\'" onMouseOut="this.className=\'' + cssClassBG + '\'">' + linkHTML + '</td>';

		}

		// Format the HTML
		for (i=0; i<ret.length; i++)
		{
			ret[i] = ret[i].join('\n') + '\n';
		}

		previousYear  = thisMonth.getFullYear();
		previousMonth = thisMonth.getMonth() - 1;
		if (previousMonth < 0)
		{
			previousMonth = 11;
			previousYear--;
		}

		nextYear  = thisMonth.getFullYear();
		nextMonth = thisMonth.getMonth() + 1;
		if(nextMonth > 11){
			nextMonth = 0;
			nextYear++;
		}

		prevImgHTML  = '<img src="' + this.imagesPath + 'icon_can_prev.gif" hspace="8" align="absmiddle" border="0" />';
		prevLinkHTML = '<a href="javascript: ' + this.objName + '.show(' + previousMonth + ', ' + previousYear + ')">' + prevImgHTML + '</a>';
		nextImgHTML  = '<img src="' + this.imagesPath + 'icon_can_next.gif" hspace="8" align="absmiddle" border="0" />';
		nextLinkHTML = '<a href="javascript: ' + this.objName + '.show(' + nextMonth + ', ' + nextYear + ')">' + nextImgHTML + '</a>';

		/**
        * Build month combo
        */
		if (this.useMonthCombo) {
			monthComboOptions = '';
			for (i=0; i<12; i++) {
				selected = (i == thisMonth.getMonth() ? 'selected="selected"' : '');
				monthComboOptions += '<option value="' + i + '" ' + selected + '>' + monthnames[i] + '</option>';
			}
			monthCombo = '<select name="months" onchange="' + this.objName + '.show(this.options[this.selectedIndex].value, ' + this.objName + '.currentYear)">' + monthComboOptions + '</select>';
		} else {
			monthCombo = monthnames[thisMonth.getMonth()];
		}

		/**
        * Build year combo
        */
		if (this.useYearCombo) {
			yearComboOptions = '';
			for (i = thisMonth.getFullYear() - this.yearComboRange; i <= (thisMonth.getFullYear() + this.yearComboRange); i++) {
				selected = (i == thisMonth.getFullYear() ? 'selected="selected"' : '');
				yearComboOptions += '<option value="' + i + '" ' + selected + '>' + i + '</option>';
			}
			yearCombo = '<select style="border: 1px groove" name="years" onchange="' + this.objName + '.show(' + this.objName + '.currentMonth, this.options[this.selectedIndex].value)">' + yearComboOptions + '</select>';
		} else {
			yearCombo = thisMonth.getFullYear();
		}

		calendarType = (row>4) ? 6 : 5;

		htmlCal = '<table width="130" border="0" cellpadding="0" cellspacing="0">';
		htmlCal += '<tr>';
		htmlCal += '<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">';
		htmlCal += '<tr>';
		htmlCal += '<td width="5" height="19"><img src="' + this.imagesPath + 'can_board_01.gif" width="5" height="19"></td>';
		htmlCal += '<td height="19" background="' + this.imagesPath + 'can_board_02.gif">';
		htmlCal += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
		htmlCal += '<tr>';
		htmlCal += '<td width="12">'+prevLinkHTML+'</td>';
		htmlCal += '<td align="center" nowrap="nowrap"><span class="calendartitle">' + monthCombo + ' ' + yearCombo + '</span></td>';
		htmlCal += '<td width="12">'+nextLinkHTML+'</td>';
		htmlCal += '</tr>';
		htmlCal += '</table></td>';
		htmlCal += '<td width="19" height="19"><a href="javascript:' + this.objName + '._hideLayer()"><img src="' + this.imagesPath + 'can_board_close_off.gif" name="can_close" width="19" height="19" border="0" id="can_close" onMouseOver="MM_swapImage(\'can_close\',\'\',\'' + this.imagesPath + 'can_board_close_on.gif\',1)" onMouseOut="MM_swapImgRestore()" /></a></td>';
		htmlCal += '</tr>';
		htmlCal += '</table></td>';
		htmlCal += '</tr>';

		htmlCal += '<tr>';
		htmlCal += '<td><table width="100%" border="0" cellspacing="0" cellpadding="0">';
		htmlCal += '<tr>';
		htmlCal += '<td width="5" background="' + this.imagesPath + 'can_board_04.gif"><img src="' + this.imagesPath + 'can_board_04.gif" width="5" height="19"></td>';
		htmlCal += '<td bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="1">';
		htmlCal += '<tr align="center" class="calendartop">';

		var is_eng = true;
		if (typeof(is_big5)!="undefined")
		{
			is_eng = (!is_big5);
		}
		if (is_eng)
		{
			htmlCal += '<td class="calendarholiday">S</td>';
			htmlCal += '<td class="calendarday">M</td>';
			htmlCal += '<td class="calendarday">T</td>';
			htmlCal += '<td class="calendarday">W</td>';
			htmlCal += '<td class="calendarday">T</td>';
			htmlCal += '<td class="calendarday">F</td>';
			htmlCal += '<td class="calendarday">S</td></tr>';
		} else
		{
			htmlCal += '<td class="calendarday"><font color="#EE6C40">日</font></td>';
			htmlCal += '<td class="calendarday">一</td>';
			htmlCal += '<td class="calendarday">二</td>';
			htmlCal += '<td class="calendarday">三</td>';
			htmlCal += '<td class="calendarday">四</td>';
			htmlCal += '<td class="calendarday">五</td>';
			htmlCal += '<td class="calendarday">六</td></tr>';
		}
		htmlCal += '</tr>';

		htmlCal += '<tr align="center">' + ret.join('</tr>\n<tr>') + '</tr>';

		htmlCal += '</table></td>';
		htmlCal += '<td width="6" background="' + this.imagesPath + 'can_board_06.gif"><img src="' + this.imagesPath + 'can_board_06.gif" width="6" height="6"></td>';
		htmlCal += '</tr>';
		htmlCal += '<tr>';
		htmlCal += '<td width="5" height="6"><img src="' + this.imagesPath + 'can_board_07.gif" width="5" height="6"></td>';
		htmlCal += '<td height="6" background="' + this.imagesPath + 'can_board_08.gif"><img src="' + this.imagesPath + 'can_board_08.gif" width="5" height="6"></td>';
		htmlCal += '<td width="6" height="6"><img src="' + this.imagesPath + 'can_board_09.gif" width="6" height="6"></td>';
		htmlCal += '</tr>';
		htmlCal += '</table></td>';
		htmlCal += '</tr>';
		htmlCal += '</table>';

		html = '<table border="0" cellpadding="0" cellspacing="0" >';
		html += '<tr>';
		html += '<td width="13" height="16" ><img src="' + this.imagesPath + 'calender_TopL.gif" width="13" height="20"></td>';
		html += '<td background="' + this.imagesPath + 'calender_TopBG.gif" align="right"><a href="javascript:' + this.objName + '._hideLayer()"><img src="' + this.imagesPath + 'calender_close.gif" width="9" height="9" border="0"></a></td>';
		html += '<td width="10"><img src="' + this.imagesPath + 'calender_TopR.gif" width="10" height="20"></td>';
		html += '</tr>';
		html += '<tr>';
		html += '<td background="' + this.imagesPath + 'calender_MiddleL.gif">&nbsp;</td>';
		html += '<td bgcolor="white" valign="top">' + htmlCal + '</td>';
		html += '<td background="' + this.imagesPath + 'calender_middleR.gif">&nbsp;</td>';
		html += '</tr>';
		html += '<tr>';
		html += '<td height="23" ><img src="' + this.imagesPath + 'calender_BottomL.gif" width="13" height="23"></td>';
		html += '<td background="' + this.imagesPath + 'calender_BottomBG.gif">&nbsp;</td>';
		html += '<td><img src="' + this.imagesPath + 'calender_BottomR.gif" width="10" height="23"></td>';
		html += '</tr>';
		html += '</table>';

		this._setHTML(htmlCal);
		if (!arguments[0] && !arguments[1])
		{
			this._showLayer();
			this._setLayerPosition();
			setDivVisible(true, this.layerID, "lyrShim");
		}
	}


/**
* Writes HTML to document for layer
*
* @access public
*/
	function dynCalendar_writeHTML()
	{
		if (is_ie5up || is_nav6up || is_gecko) {
			document.write('<div class="calendarlayer" id="' + this.layerID + '" onmouseover="' + this.objName + '._mouseover(true)" onmouseout="' + this.objName + '._mouseover(false)" style="display:none;z-index:100;"></div>');
		}
	}

/**
* Sets the offset to the mouse position
* that the calendar appears at.
*
* @access public
* @param integer Xoffset Number of pixels for vertical
*                        offset from mouse position
* @param integer Yoffset Number of pixels for horizontal
*                        offset from mouse position
*/
	function dynCalendar_setOffset(Xoffset, Yoffset)
	{
		this.setOffsetX(Xoffset);
		this.setOffsetY(Yoffset);
	}

/**
* Sets the X offset to the mouse position
* that the calendar appears at.
*
* @access public
* @param integer Xoffset Number of pixels for horizontal
*                        offset from mouse position
*/
	function dynCalendar_setOffsetX(Xoffset)
	{
		this.offsetX = Xoffset;
	}

/**
* Sets the Y offset to the mouse position
* that the calendar appears at.
*
* @access public
* @param integer Yoffset Number of pixels for vertical
*                        offset from mouse position
*/
	function dynCalendar_setOffsetY(Yoffset)
	{
		this.offsetY = Yoffset;
	}

/**
* Sets the images path
*
* @access public
* @param string path Path to use for images
*/
	function dynCalendar_setImagesPath(path)
	{
		this.imagesPath = path;
	}

/**
* Turns on/off the month dropdown
*
* @access public
* @param boolean useMonthCombo Whether to use month dropdown or not
*/
	function dynCalendar_setMonthCombo(useMonthCombo)
	{
		this.useMonthCombo = useMonthCombo;
	}

/**
* Turns on/off the year dropdown
*
* @access public
* @param boolean useYearCombo Whether to use year dropdown or not
*/
	function dynCalendar_setYearCombo(useYearCombo)
	{
		this.useYearCombo = useYearCombo;
	}

/**
* Sets the current month being displayed
*
* @access public
* @param boolean month The month to set the current month to
*/
	function dynCalendar_setCurrentMonth(month)
	{
		this.currentMonth = month;
	}

/**
* Sets the current month being displayed
*
* @access public
* @param boolean year The year to set the current year to
*/
	function dynCalendar_setCurrentYear(year)
	{
		this.currentYear = year;
	}

/**
* Sets the range of the year combo. Displays this number of
* years either side of the year being displayed.
*
* @access public
* @param integer range The range to set
*/
	function dynCalendar_setYearComboRange(range)
	{
		this.yearComboRange = range;
	}


	// [eCorp]
	function dynCalendar_setPosition(left, top)
	{
		this.pos_top = top;
		this.pos_left = left;
	}

/**
* Returns the layer object
*
* @access private
*/
	function dynCalendar_getLayer()
	{
		var layerID = this.layerID;

		if (document.getElementById(layerID)) {

			return document.getElementById(layerID);

		} else if (document.all(layerID)) {
			return document.all(layerID);
		}
	}

/**
* Hides the calendar layer
*
* @access private
*/
	function dynCalendar_hideLayer()
	{
		this._getLayer().style.visibility = 'hidden';
		setDivVisible(false, this.layerID, "lyrShim");
	}

/**
* Shows the calendar layer
*
* @access private
*/
	function dynCalendar_showLayer()
	{
		this._getLayer().style.visibility = 'visible';
	}

/**
* Sets the layers position
*
* @access private
*/
	function dynCalendar_setLayerPosition()
	{
		this._getLayer().style.top  = (this.pos_top!=null) ? this.pos_top + 'px' : (dynCalendar_mouseY + this.offsetY) + 'px';
		this._getLayer().style.left = (this.pos_left!=null) ? this.pos_left + 'px' : (dynCalendar_mouseX + this.offsetX) + 'px';
	}

/**
* Sets the innerHTML attribute of the layer
*
* @access private
*/
	function dynCalendar_setHTML(html)
	{
		this._getLayer().innerHTML = html;
	}

/**
* Returns number of days in the supplied month
*
* @access private
* @param integer month The month to get number of days in
* @param integer year  The year of the month in question
*/
	function dynCalendar_getDaysInMonth(month, year)
	{
		monthdays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
		if (month != 1) {
			return monthdays[month];
		} else {
			return ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0 ? 29 : 28);
		}
	}

/**
* onMouse(Over|Out) event handler
*
* @access private
* @param boolean status Whether the mouse is over the
*                       calendar or not
*/
	function dynCalendar_mouseover(status)
	{
		dynCalendar_mouseoverStatus = status;
		return true;
	}

/**
* onMouseMove event handler
*/
	dynCalendar_oldOnmousemove = document.onmousemove ? document.onmousemove : new Function;

	document.onmousemove = function ()
	{
		if (is_ie5up || is_nav6up || is_gecko) {
			if (arguments[0]) {
				dynCalendar_mouseX = arguments[0].pageX;
				dynCalendar_mouseY = arguments[0].pageY;
			} else {
				dynCalendar_mouseX = event.clientX + document.body.scrollLeft;
				dynCalendar_mouseY = event.clientY + document.body.scrollTop;
				arguments[0] = null;
			}

			dynCalendar_oldOnmousemove();
		}
	}

/**
* Callbacks for document.onclick
*/
	dynCalendar_oldOnclick = document.onclick ? document.onclick : new Function;

	document.onclick = function ()
	{
		if (is_ie5up || is_nav6up || is_gecko) {
			if(!dynCalendar_mouseoverStatus){
				for(i=0; i<dynCalendar_layers.length; ++i){
					dynCalendar_layers[i]._hideLayer();
				}
			}

			dynCalendar_oldOnclick(arguments[0] ? arguments[0] : null);
		}
	}
