function js_Show_Topic_Remarks_Layer(jsTopicID, jsTopicNameDivID, jsRemarksLayerDivID)
{
	$('div#' + jsRemarksLayerDivID).html('<img src="/images/2020a/indicator.gif"> <span class="tabletextremark">' + JSLang['Loading'] + '</span>');
	js_Change_Layer_Position(jsTopicNameDivID, jsRemarksLayerDivID);
	MM_showHideLayers(jsRemarksLayerDivID, '', 'show');
	
	$('div#' + jsRemarksLayerDivID).load(
		"../../ajax_reload.php", 
		{ 
			Action: 'Topic_Remarks_Layer',
			TopicID: jsTopicID
		},
		function(returnString)
		{
			
		}
	);
}

function js_Hide_Topic_Remarks_Layer(jsRemarksLayerDivID)
{
	MM_showHideLayers(jsRemarksLayerDivID, '', 'hide');
}

function js_Change_Layer_Position(jsClickedObjID, jsLayerDivID) {
	
	var jsOffsetLeft, jsOffsetTop;
	
	jsOffsetLeft = $('div#' + jsClickedObjID).width();
	jsOffsetTop = 0;
	//jsOffsetTop = $('div#' + jsClickedObjID).height();
		
	var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') + jsOffsetLeft;
	var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') + jsOffsetTop;
	
	document.getElementById(jsLayerDivID).style.left = posleft + "px";
	document.getElementById(jsLayerDivID).style.top = postop + "px";
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function js_Get_Filtering_Info_Text()
{
	var jsFilterTopicInfoText = '';		// jsFilterTopicInfoText = 'Level,TopicID::Level,TopicID::'
	var jsTmpTopicID = '';
	var jsTmpInfoText = '';
	for (i=1; i<jsMaxTopicLevel; i++) {
		jsTmpTopicID = $('select#Level' + i + '_TopicID').val();
		
		if (jsTmpTopicID != '' && jsTmpTopicID != null && jsTmpTopicID != 'undefined') {
			jsTmpInfoText = i + ',' + jsTmpTopicID + '::';
			jsFilterTopicInfoText += jsTmpInfoText;
		}
		else {
			break;
		}
	}
	
	return jsFilterTopicInfoText;
}