// Ajax Function
/* 
 * 2019-01-14 (Vito) Modified Show_Class_Form(), add focus into SubjectComponentID
 */

{
function Get_Class_Student_List() {
	var YearClassID = document.getElementById('YearClassSelect').options[document.getElementById('YearClassSelect').selectedIndex].value;
	var YearTermID = document.getElementById('YearTermID').value;
	var SubjectID = document.getElementById('SubjectID').value;
	//2014-0404-1546-25066
	//var SubjectGroupID = document.getElementById('SubjectGroupID').value;
	var SubjectGroupID = $('input#SubjectGroupID').val();
	var SelectedStudentID = Get_Selected_Value('StudentSelected[]','String');
	StudListAjax = GetXmlHttpObject();
   
  if (StudListAjax == null)
  {
    alert (errAjax);
    return;
  } 
    
  var url = 'ajax_get_class_student_list.php';
  var postContent = SelectedStudentID;
  postContent += '&YearClassID='+YearClassID;
  postContent += '&YearTermID='+YearTermID;
  postContent += '&SubjectID='+SubjectID;
  postContent += '&SubjectGroupID='+SubjectGroupID;
  
	StudListAjax.onreadystatechange = function() {
		if (StudListAjax.readyState == 4) {
			ResponseText = Trim(StudListAjax.responseText);
		  document.getElementById('ClassStudentList').innerHTML = ResponseText;
		}
	};
  StudListAjax.open("POST", url, true);
	StudListAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	StudListAjax.send(postContent);
}
	
function Get_Class_Form(YearID,SubjectID,SubjectGroupID) {
	var AcademicYearID = Get_Selection_Value("AcademicYearID","String");
	var YearTermID = Get_Selection_Value("YearTerm","String");
	var SubjectGroupID = SubjectGroupID || "";
	wordXmlHttp = GetXmlHttpObject();
   
  if (wordXmlHttp == null)
  {
    alert (errAjax);
    return;
  } 
  
    
  var url = 'ajax_get_class_form.php';
  var postContent = 'YearID='+YearID;
  postContent += '&YearTermID='+YearTermID;
  postContent += '&SubjectID='+SubjectID;
  postContent += '&SubjectGroupID='+SubjectGroupID;
  postContent += '&AcademicYearID='+AcademicYearID;
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
		  document.getElementById('TB_ajaxContent').innerHTML = wordXmlHttp.responseText;
		  Init_JQuery_AutoComplete('StudentSearch');
		  
		  if (SubjectGroupID != "") 
		  	Check_Selected_Student();
		  else
		  	Update_Class_List(); 
		}
		//Add focus on the 1st item of the form
		$('#SubjectComponentID').focus();
	};
  wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}
}

// jquery autocomplete function 
{
var AutoCompleteObj;
function Init_JQuery_AutoComplete(InputID) {
	AutoCompleteObj = $("#"+InputID).autocomplete("ajax_search_student.php",
		{  			
			onItemSelect: function(li) {
				Add_Selected_Student(li.extra[1],li.selectValue);
				Get_Class_Student_List();
			},
			formatItem: function(row) {
				return row[0] + " (email: " + row[1] + ")";
			},
			maxItemsToShow: 5,
			minChars: 1,
			delay: 0,
			width: 300,
			scrollLayer: 'EditLayer'
		}
	);
	
	Update_Auto_Complete_Extra_Para();
}

function Update_Auto_Complete_Extra_Para() {
	ExtraPara = Get_Selected_Value('StudentSelected[]');
	ExtraPara['YearTermID'] = document.getElementById('YearTermID').value;
	ExtraPara['SubjectID'] = document.getElementById('SubjectID').value;
	ExtraPara['SubjectGroupID'] = document.getElementById('SubjectGroupID').value;
	ExtraPara['YearID'] = Get_Check_Box_Value("YearID[]","Array");
	AutoCompleteObj[0].autocompleter.setExtraParams(ExtraPara);
}
}

// UI function for add/ remove class teacher/ student in add/edit class
{
function Add_Selected_Student(UserID,StudentName) {
	var UserID = UserID || "";
	var StudentName = StudentName || "";
	var StudentSelected = document.getElementById('StudentSelected[]');

	StudentSelected.options[StudentSelected.length] = new Option(StudentName,UserID);
	
	Update_Auto_Complete_Extra_Para();
}

function Remove_Selected_Student() {
	var StudentSelected = document.getElementById('StudentSelected[]');
	
	for (var i = (StudentSelected.length -1); i >= 0 ; i--) {
		if (StudentSelected.options[i].selected) 
			StudentSelected.options[i] = null;	
	}
	
	Get_Class_Student_List();
	
	Update_Auto_Complete_Extra_Para();
}

function Remove_All_Student() {
	var StudentSelected = document.getElementById('StudentSelected[]');
	
	for (var i = (StudentSelected.length -1); i >= 0 ; i--) {
			StudentSelected.options[i] = null;	
	}
	
	Get_Class_Student_List();
	
	Update_Auto_Complete_Extra_Para();
}

function Add_Selected_Class_Student() {
	var ClassStudent = document.getElementById('ClassStudent');
	var StudentSelected = document.getElementById('StudentSelected[]');
	
	for (var i = (ClassStudent.length -1); i >= 0 ; i--) {
		if (ClassStudent.options[i].selected) {
			Student = ClassStudent.options[i];
			StudentSelected.options[StudentSelected.length] = new Option(Student.text,Student.value);
			ClassStudent.options[i] = null;	
		}
	}
	
	//Reorder_Selection_List('StudentSelected[]');
	
	Update_Auto_Complete_Extra_Para();
	Check_Selected_Student();
}

function Add_All_Student() {
	var ClassStudent = document.getElementById('ClassStudent');
	var StudentSelected = document.getElementById('StudentSelected[]');
	var NumOfStudentSelected = StudentSelected.length;
	
	for (var i = (ClassStudent.length -1); i >= 0 ; i--) {
		Student = ClassStudent.options[i];
		//StudentSelected.options[StudentSelected.length] = new Option(Student.text,Student.value);
		StudentSelected.options[i + NumOfStudentSelected] = new Option(Student.text,Student.value);
		ClassStudent.options[i] = null;
	}
	
	//Reorder_Selection_List('StudentSelected[]');
	
	Update_Auto_Complete_Extra_Para();
	Check_Selected_Student();
}

function Get_Selected_Value(SelectID,ReturnType) {
	var ReturnType = ReturnType || "JQuery";
	var str = "";
	var isFirst = true;
	var Obj = document.getElementById(SelectID);
	
	if (ReturnType == "JQuery") {
		for (var i=0; i< Obj.length; i++) {
			if (!isFirst) {
				str += "&";
			}
			str += Obj.options[i].value;
			isFirst = false;
		}
		
		var resultArray = new Array();
		resultArray[SelectID] = str.split('&');
		return resultArray;
	}
	else {
		for (var i=0; i< Obj.length; i++) {
			if (!isFirst) {
				str += "&";
			}
			str += Obj.name + '=' +Obj.options[i].value;
			isFirst = false;
		} 
		
		return str;
	}
}
	
function Add_Teaching_Teacher(TeacherID) {
	var TeacherList = document.getElementById('TeacherList');
	var Teacher = TeacherList.options[TeacherList.selectedIndex];
	var TeachingStaffList = document.getElementById('SelectedClassTeacher[]');
	
	if (Teacher.value != "") {
		TeachingStaffList.options[TeachingStaffList.length] = new Option(Teacher.text,Teacher.value);
		
		TeacherList.options[TeacherList.selectedIndex] = null;
		
		Reorder_Selection_List('SelectedClassTeacher[]');
	}
}

function Reorder_Selection_List(selectId) {
	var selectList = document.getElementById(selectId);
	for (var i = 0; i < selectList.length; i++) {
		for (var j=i; j < selectList.length; j++) {
			if (selectList.options[i].text.toLowerCase() > selectList.options[j].text.toLowerCase()) {
				var tempOption1 = new Option(selectList.options[i].text,selectList.options[i].value);
				var tempOption2 = new Option(selectList.options[j].text,selectList.options[j].value);
				selectList.options[i] = tempOption2;
				selectList.options[j] = tempOption1;
			}
		}
		//alert(selectList.options[i].text);
		/*if (opt.selected) {
			selectList.removeChild(opt);
			selectList.insertBefore(opt, selectOptions[i - 1]);
		}*/
  }
}

function Remove_Teaching_Teacher() {
	var TeachingStaffList = document.getElementById('SelectedClassTeacher[]');
	var TeacherList = document.getElementById('TeacherList');
	
	for (var i = (TeachingStaffList.length -1); i >= 0 ; i--) {
		if (TeachingStaffList.options[i].selected) {
			Teacher = TeachingStaffList.options[i];
			TeacherList.options[TeacherList.length] = new Option(Teacher.text,Teacher.value);
			TeachingStaffList.options[i] = null;	
		}
	}

	TeacherList.options[0].selected = true;
	
	//Reorder_Selection_List('TeacherList');
	Reorder_Teacher_List();
	
}

function Reorder_Teacher_List() {
	wordXmlHttp = GetXmlHttpObject();
   
  if (wordXmlHttp == null)
  {
    alert (errAjax);
    return;
  } 
	checkOptionAll(document.ClassForm.elements['SelectedClassTeacher[]']);
  
  var PostVar = Get_Form_Values(document.getElementById("ClassForm"));
  var url = 'ajax_get_teacher_selection.php';
  var postContent = PostVar;
  
  
  
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
		  document.getElementById('teacherListDiv').innerHTML = wordXmlHttp.responseText;
		  
		 
		}
	};
  wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}

}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init(){   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}

// misc js function
{
function Select_Selection_Element_By_Array(SelectionID,ArrayObj) {
	if (document.getElementById(SelectionID)) {
		var SelectionObj = document.getElementById(SelectionID);
		
		for (var i=0; i< SelectionObj.length; i++) {
			if (jIN_ARRAY(ArrayObj,SelectionObj.options[i].value)) 
				SelectionObj.options[i].selected = true;
			else 
				SelectionObj.options[i].selected = false;
		}
	}
}
}