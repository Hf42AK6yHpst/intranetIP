// Ajax Function
/* 
 * 2019-01-14 (Vito) Modified Show_Class_Form(), add focus into ClassTitleEN
 * 2018-01-31 (Carlos) Modified Get_Class_Student_List(), pass YearClassID to ajax_get_student_without_class_list.php for finding removed students in this class.
 * 2016-09-20 (Ronald) Modify Add_Selected_Class_Student, correct deleting when moving studnet to selected box
 */
{
function Get_Class_Student_List() {
	
	var jsStudentSource = $('select#StudentSourceSelect').val();
	var AcademicYearID = document.getElementById('AcademicYearID').value;
	
	if (jsStudentSource == 1)
	{
		var SelectedStudentID = Get_Selection_Value('StudentSelected[]', 'String', true);
		$('div#ClassStudentList').load(
			"ajax_get_student_without_class_list.php", 
			{ 
				ExcludeStudentList: SelectedStudentID,
				AcademicYearID: AcademicYearID,
				YearClassID: document.getElementById('YearClassID') != null? document.getElementById('YearClassID').value: '' 
			},
			function(ReturnData)
			{
				
			}
		);
	}
	else
	{
		var PrevYearClassID = document.getElementById('YearClassSelect').options[document.getElementById('YearClassSelect').selectedIndex].value;
		var SelectedStudentID = Get_Selected_Value('StudentSelected[]','String');
		var YearClassID = document.getElementById('YearClassID').value;
		
		StudListAjax = GetXmlHttpObject();
	   
		if (StudListAjax == null)
		{
			alert (errAjax);
			return;
		} 
    
		var url = 'ajax_get_class_student_list.php';
		var postContent = SelectedStudentID;
		postContent += '&YearClassID='+YearClassID;
		postContent += '&PrevYearClassID='+PrevYearClassID;
		postContent += '&AcademicYearID='+AcademicYearID;
		  
		StudListAjax.onreadystatechange = function() {
			if (StudListAjax.readyState == 4) {
				ResponseText = Trim(StudListAjax.responseText);
				document.getElementById('ClassStudentList').innerHTML = ResponseText;
			}
		};
			StudListAjax.open("POST", url, true);
			StudListAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			StudListAjax.send(postContent);
	}	
}

function Get_Selection_Value(SelectionID,ReturnType,IgnoreSelectedStatus) {
	var ReturnType = ReturnType || "Array";
	var IgnoreSelectedStatus = IgnoreSelectedStatus || false;
	var ValueArray = Array();
	var str = '';
	
	if (document.getElementById(SelectionID)) {
		var SelectionObj = document.getElementById(SelectionID);
		switch(SelectionObj.type) {
    	case "select-one":
    		str += SelectionObj.options[SelectionObj.selectedIndex].value; 
	      //str += fobj.elements[i].name +
	      //"=" + SelectionObj.options[SelectionObj.selectedIndex].value + "&";
	      break;
      case "select-multiple":
				for (var j=0; j< SelectionObj.length; j++) {
					if (SelectionObj.options[j].selected || IgnoreSelectedStatus) {
	 					str += SelectionObj.options[j].value + ",";
	 				}
	 			}
	 			break;
  	}
  	
  	if (str.lastIndexOf(',') != -1) 
  		str = str.substring(0,str.lastIndexOf(','));
  		
  	if (str != "") 
  		ValueArray = str.split(',');
	}
	
	if (ReturnType == "Array") {
		return ValueArray;
	}
	// normal string -- onlu for select-one element
	else if (ReturnType == "String") {
		return str;
	}
	// form/ ajax post query string
	else {
		str = "";
		for (var i=0; i< ValueArray.length; i++) {
			str += SelectionID + "=" + ValueArray[i] + '&';
		}
		
		return str;
	}
}

function Show_Class_Form(ClassID,FormID,AcademicYearID) {
	var ClassID = ClassID || '';
	var FormID = FormID || '';
	var AcademicYearID = AcademicYearID || '';
	
	wordXmlHttp = GetXmlHttpObject();
  if (wordXmlHttp == null)
  {
    alert (errAjax);
    return;
  } 
    
  var url = 'ajax_get_class_form.php';
  var postContent = 'YearClassID='+ClassID;
  postContent += '&YearID='+FormID;
  postContent += '&AcademicYearID='+AcademicYearID;
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
		  document.getElementById('TB_ajaxContent').innerHTML = wordXmlHttp.responseText;
		  Init_JQuery_AutoComplete('StudentSearch',AcademicYearID,ClassID);
		  $("input#ClassTitleEN").focus();
		}
	};	
	wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}	

function Check_Name_Quote() {
	if ($('input#ClassTitleEN').val().indexOf('"') >= 0 || $('input#ClassTitleEN').val().indexOf("'") >= 0) {
		document.getElementById("ClassTitleENWarningLayer").innerHTML = Lang['ClassTitleQuoteWarning'];
		document.getElementById("ClassTitleENWarningRow").style.display = '';
	}
	
	if ($('input#ClassTitleB5').val().indexOf('"') >= 0 || $('input#ClassTitleB5').val().indexOf("'") >= 0) {
		document.getElementById("ClassTitleB5WarningLayer").innerHTML = Lang['ClassTitleQuoteWarning'];
		document.getElementById("ClassTitleB5WarningRow").style.display = '';
	}
}

function Check_Name_Duplicate(Language,Title,YearClassID,AcademicYearID) {
	var YearClassID = YearClassID || "";
	var AcademicYearID = AcademicYearID || "";
	
	if (Trim(Title) != "") {
		CheckNameDuplicateAjax = GetXmlHttpObject();
		
	  if (CheckNameDuplicateAjax == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'ajax_check_class_title.php';
	  var postContent = 'YearClassID='+YearClassID;
	  postContent += '&ClassTitle='+encodeURIComponent(Title);
	  postContent += '&Language='+Language;
	  postContent += '&AcademicYearID='+AcademicYearID;
		CheckNameDuplicateAjax.onreadystatechange = function() {
			if (CheckNameDuplicateAjax.readyState == 4) {
				TitleIsGood = Trim(CheckNameDuplicateAjax.responseText);
			  
			  if (TitleIsGood == "1") {
			  	document.getElementById("ClassTitle"+Language+"WarningLayer").innerHTML = "";
					document.getElementById("ClassTitle"+Language+"WarningRow").style.display = 'none';
			  }
			  else {		  	
			  	document.getElementById("ClassTitle"+Language+"WarningLayer").innerHTML = Lang['ClassTitleDuplicateWarning'];
					document.getElementById("ClassTitle"+Language+"WarningRow").style.display = "";
			  }
			}
		};
	  CheckNameDuplicateAjax.open("POST", url, false);
		CheckNameDuplicateAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		CheckNameDuplicateAjax.send(postContent);	
	}
	else {
		document.getElementById("ClassTitle"+Language+"WarningLayer").innerHTML = "";
		document.getElementById("ClassTitle"+Language+"WarningRow").style.display = "none";
	}
}

function Check_Class_Title_Empty(ClassTitleEN) {	
	if (Trim(ClassTitleEN) == "") {
		document.getElementById("ClassTitleENWarningLayer").innerHTML = Lang['ClassTitleEmptyWarning'];
		document.getElementById("ClassTitleENWarningRow").style.display = "";
	}	
}
}

// jquery autocomplete function 
{
var AutoCompleteObj;
function Init_JQuery_AutoComplete(InputID,AcademicYearID,YearClassID) {
	AutoCompleteObj = $("#"+InputID).autocomplete("ajax_search_student.php",
		{  			
			onItemSelect: function(li) {
				Add_Selected_Student(li.extra[1],li.selectValue);
			},
			formatItem: function(row) {
				return row[0] + " (email: " + row[1] + ")";
			},
			maxItemsToShow: 5,
			minChars: 1,
			delay: 0,
			width: 200,
			scrollLayer: 'EditLayer'
		}
	);
	
	Update_Auto_Complete_Extra_Para();
}

function Update_Auto_Complete_Extra_Para() {
	ExtraPara = Get_Selected_Value('StudentSelected[]');
	ExtraPara['AcademicYearID'] = document.getElementById('AcademicYearID').value;
	ExtraPara['YearClassID'] = document.getElementById('YearClassID').value;
	AutoCompleteObj[0].autocompleter.setExtraParams(ExtraPara);
}
}

// UI function for add/ remove class teacher/ student in add/edit class
{
function Add_Selected_Student(UserID,StudentName) {
	var UserID = UserID || "";
	var StudentName = StudentName || "";
	var StudentSelected = document.getElementById('StudentSelected[]');

	StudentSelected.options[StudentSelected.length] = new Option(StudentName,UserID);
	
	Update_Auto_Complete_Extra_Para();
}

function Remove_Selected_Student() {
	var StudentSelected = document.getElementById('StudentSelected[]');
	
	for (var i = (StudentSelected.length -1); i >= 0 ; i--) {
		if (StudentSelected.options[i].selected) 
			StudentSelected.options[i] = null;	
	}
	Get_Class_Student_List();
	Update_Auto_Complete_Extra_Para();
}

function Remove_All_Student() {
	var StudentSelected = document.getElementById('StudentSelected[]');
	
	for (var i = (StudentSelected.length -1); i >= 0 ; i--) {
			StudentSelected.options[i] = null;	
	}
	Get_Class_Student_List();
	Update_Auto_Complete_Extra_Para();
}

function Add_Selected_Class_Student() {
	var ClassStudent = document.getElementById('ClassStudent');
	var StudentSelected = document.getElementById('StudentSelected[]');
	
	var toRemoveOptionIndices = [];
	for(var i=0;i<ClassStudent.length;i++){
		if (ClassStudent.options[i].selected) {
			Student = ClassStudent.options[i];
			StudentSelected.options[StudentSelected.length] = new Option(Student.text,Student.value);
			//ClassStudent.options[i] = null;
			toRemoveOptionIndices.push(i);
		}
	}
	for(var i=toRemoveOptionIndices.length-1;i>=0;i--){
		ClassStudent.options[toRemoveOptionIndices[i]] = null;
	}
	
	//Reorder_Selection_List('StudentSelected[]');
	
	Update_Auto_Complete_Extra_Para();
}

function Add_All_Student() {
	var ClassStudent = document.getElementById('ClassStudent');
	var StudentSelected = document.getElementById('StudentSelected[]');
	
	var toRemoveOptionIndices = [];
	for(var i=0;i<ClassStudent.length;i++){
		Student = ClassStudent.options[i];
		StudentSelected.options[StudentSelected.length] = new Option(Student.text,Student.value);
		//ClassStudent.options[i] = null;
		toRemoveOptionIndices.push(i);
	}
	for(var i=toRemoveOptionIndices.length-1;i>=0;i--){
		ClassStudent.options[i] = null;
	}
	
	//Reorder_Selection_List('StudentSelected[]');
	
	Update_Auto_Complete_Extra_Para();
}

function Get_Selected_Value(SelectID,ReturnType) {
	var ReturnType = ReturnType || "JQuery";
	var str = "";
	var isFirst = true;
	var Obj = document.getElementById(SelectID);
	
	if (ReturnType == "JQuery") {
		for (var i=0; i< Obj.length; i++) {
			if (!isFirst) {
				str += "&";
			}
			str += Obj.options[i].value;
			isFirst = false;
		}
		
		var resultArray = new Array();
		resultArray[SelectID] = str.split('&');
		return resultArray;
	}
	else {
		for (var i=0; i< Obj.length; i++) {
			if (!isFirst) {
				str += "&";
			}
			str += Obj.name + '=' +Obj.options[i].value;
			isFirst = false;
		} 
		
		return str;
	}
}
	
function Add_Teaching_Teacher(TeacherID) {
	var TeacherList = document.getElementById('TeacherList');
	var Teacher = TeacherList.options[TeacherList.selectedIndex];
	var TeachingStaffList = document.getElementById('SelectedClassTeacher[]');
	
	if (Teacher.value != "") {
		TeachingStaffList.options[TeachingStaffList.length] = new Option(Teacher.text,Teacher.value);
		
		TeacherList.options[TeacherList.selectedIndex] = null;
		
		Reorder_Selection_List('SelectedClassTeacher[]');
	}
}

function Reorder_Selection_List(selectId) {
	var selectList = document.getElementById(selectId);
	for (var i = 0; i < selectList.length; i++) {
		for (var j=i; j < selectList.length; j++) {
			if (selectList.options[i].text.toLowerCase() > selectList.options[j].text.toLowerCase()) {
				var tempOption1 = new Option(selectList.options[i].text,selectList.options[i].value);
				var tempOption2 = new Option(selectList.options[j].text,selectList.options[j].value);
				selectList.options[i] = tempOption2;
				selectList.options[j] = tempOption1;
			}
		}
		//alert(selectList.options[i].text);
		/*if (opt.selected) {
			selectList.removeChild(opt);
			selectList.insertBefore(opt, selectOptions[i - 1]);
		}*/
  }
}

function Remove_Teaching_Teacher() {
	var TeachingStaffList = document.getElementById('SelectedClassTeacher[]');
	var TeacherList = document.getElementById('TeacherList');
	
	for (var i = (TeachingStaffList.length -1); i >= 0 ; i--) {
		if (TeachingStaffList.options[i].selected) {
			Teacher = TeachingStaffList.options[i];
			TeacherList.options[TeacherList.length] = new Option(Teacher.text,Teacher.value);
			TeachingStaffList.options[i] = null;	
		}
	}

	TeacherList.options[0].selected = true;
	
	Reorder_Selection_List('TeacherList');
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init(){   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}

function js_Change_Student_Source(jsSourceCode)
{
	if (jsSourceCode == 1)
	{
		// from student without classes
		$('div#PreviousYearClassSelectionDiv').hide();
	}
	else if (jsSourceCode == 2)
	{
		// from classes of last year
		$('div#PreviousYearClassSelectionDiv').show();
	}
	
	$('div#ClassStudentList').html('');
	Get_Class_Student_List();
}