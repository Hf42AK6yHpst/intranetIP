
$(document).ready(function() {

  $(window).scroll(function() {
      var scroll = $(window).scrollTop();

      if (scroll >= 200) {
          $("#blkTopBar").addClass("show");
      }else{
          $("#blkTopBar").removeClass("show");
      }
  });

  var $blkForm = $("#blkForm")
  var $row = $("#blkForm > div.student");
  var $showWhenChecked = $(".showWhenChecked");
  $row.addClass("unchecked pointer");

  $blkForm.on('click', 'div.student.unchecked', function()
  {
    var $cbxUnchecked = $(this).find('input[type="checkbox"]');
    $cbxUnchecked.attr('checked', true);
    $(this).removeClass("unchecked").addClass("checked");
    $(this).find(".addAccount_button").show();
    var $radioUnchecked = $(this).find('input[type="radio"]');
    $radioUnchecked.attr('checked', true);
    $(this).removeClass("unchecked").addClass("checked");
    $(this).find(".addAccount_button").show();

    if($("#blkForm > div.student.checked").length > 0)
    {
      $showWhenChecked.show();
      $blkForm.addClass("checked").removeClass("unchecked");
    }
  });

  $blkForm.on('click', 'div.student.checked', function()
  {
    var $cbxChecked = $(this).find('input[type="checkbox"]');
    $cbxChecked.attr('checked', false);
    $(this).removeClass("checked").addClass("unchecked");
    $(this).find(".addAccount_button").hide();
    $(this).find(".extraField").hide();
    $(this).find("div.order-item .order-extra").removeClass("addExtra");
    var $radioChecked = $(this).find('input[type="radio"]');
    $radioChecked.attr('checked', false);
    $(this).removeClass("checked").addClass("unchecked");
    $(this).find(".addAccount_button").hide();
    $(this).find(".extraField").hide();
    $(this).find("div.order-item .order-extra").removeClass("addExtra");

    if($("#blkForm > div.student.unchecked").length == $row.length)
    {
      $showWhenChecked.hide();
      $blkForm.removeClass("checked").addClass("unchecked");
    }

  });

  $("#tbxTel").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
  });

  $blkForm.on('click', '.addAccount_button', function(e){
    $(this).siblings(".extraField").show();
    $(this).parent(2).addClass("addExtra");
    $(this).hide();
    e.stopPropagation();
  });

  $blkForm.on('click', '.cancel_addAccount_button', function(e){
    $(this).closest(".extraField").hide();
    $(this).closest(".extraField").siblings(".addAccount_button").show();
    $(this).closest(".order-extra").removeClass("addExtra");
    e.stopPropagation();
  });

  $blkForm.on('click', '.order-extra', function(e){
    e.stopPropagation();
  });

  $blkForm.find("input").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
  });

  $("#blkTopBar #btnMenu").click(function () {
    $(this).toggleClass("showTopMenu");
    $(this).siblings("a").toggleClass("showTopMenu");
    $("#blkTopBar > img").toggleClass("showTopMenu");
  });

  $("#blkTopBar > img").on('click', function()
  {
       window.location.href="index.html";
  });

  return false;
});
