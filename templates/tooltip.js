// kenneth chung
/* change log
	2010-05-13 (Kenneth Chung): 
		Changed to approach of clarifying client's browse type by getting the value set from 
		javascript library "brwsniff.js" which included in home_header.php
*/
/*==========================================================================

  Crosss-browser Tooltip & Menu 26/09/2002



  Copyright (C) 2002 Broadlearning.com

  This script is written by and copyrighted to YuEn at www.broadlearning.com

============================================================================*/




var tooltip_ie4 = (document.all) ? 1 : 0;

var tooltip_ns4 = (document.layers) ? 1 : 0;

var tooltip_ns6 = (document.getElementById && !document.all) ? 1 : 0;



if (typeof(isToolTip)=="undefined")
{
	var isToolTip = false;
}

if (typeof(isMenu)=="undefined")
{
	var isMenu = false;
}

if(tooltip_ns4){doc = "document."; sty = ""}

if(tooltip_ie4){doc = "document.all."; sty = ".style"}


var isCheckHeight = true;  //new
var initialize = 0;

var Ex, Ey, ContentInfo

var lyrTT = "ToolTip";

var lyrMM = "ToolMenu";

var mouse = new Object;

mouse.x = 0;

mouse.y = 0;



if(tooltip_ie4){

     Ex = "event.x";

     Ey = "event.y";

} else if(tooltip_ns4){

     Ex = "e.pageX";

     Ey = "e.pageY";

     window.captureEvents(Event.MOUSEMOVE);

     window.onmousemove=overhere;

} else if (tooltip_ns6) {

     Ex = "e.clientX";

     Ey = "e.clientY";

     document.addEventListener("mousemove",overhere,false);

}

if (is_ie) {
	Ex = "event.x + document.documentElement.scrollLeft + document.body.scrollLeft";
  Ey = "event.y + document.documentElement.scrollTop + document.body.scrollTop";
}
else if (is_ff) {
	Ex = "e.clientX + window.pageXOffset";
  Ey = "e.clientY + window.pageYOffset";
}




/* ######################## Core Function ########################## */



function moveToolTip(lay, FromTop, FromLeft){

     if (tooltip_ns6) {

         var myElement = document.getElementById(lay);

          myElement.style.left = (FromLeft + 10) + "px";

          myElement.style.top = (FromTop + document.body.scrollTop) + "px";

     }

     else if(tooltip_ie4) {
          difHeight = document.body.clientHeight - FromTop;
          divHeight = parseInt(eval(doc + lay + ".offsetHeight"));
          if (difHeight<divHeight && isCheckHeight) {
              FromTop -= (divHeight-difHeight);
          }
          if (FromTop<0)
          {
          	FromTop = 0;
			}
          eval(doc + lay + sty + ".top = "  + (FromTop + document.body.scrollTop))

     }
 else if(tooltip_ns4){eval(doc + lay + sty + ".top = "  +  FromTop)}

     if (!tooltip_ns6) eval(doc + lay + sty + ".left = " + (FromLeft + 10));

     // resize layer if it is out of window document area
     if (tooltip_ie4) {
          wdif = document.body.clientWidth - parseInt(eval(doc + lay + sty + ".left"));
          mywid = (lay==lyrTT) ? 280 : 200;
          mywid = (wdif > mywid) ? mywid : wdif;
          eval(doc + lay + sty + ".width= mywid+'px';");

          eval(doc + lay + sty + ".left = " + (FromLeft - 80));
     }
	
	document.getElementById(lay).style.top = FromTop;
	document.getElementById(lay).style.left = FromLeft;
}



function writeToLayer(lay, txt){

     if(tooltip_ie4){document.all[lay].innerHTML = txt}

     else if(tooltip_ns4){

          with(document.layers[lay].document)

          {

             open();

             write(txt);

             close();

          }

     }

     else if (tooltip_ns6) {

          over = document.getElementById([lay]);

          range = document.createRange();

          range.setStartBefore(over);

          domfrag = range.createContextualFragment(txt);

          while (over.hasChildNodes()) {

               over.removeChild(over.lastChild);

          }

          over.appendChild(domfrag);

     }

}



function Activate(){initialize=1;}



function deActivate(){initialize=0;}



function overhere(e){
	if (!e)
		var e = window.event||window.Event;
		
	//alert(document.body.scrollLeft + ':' + document.body.scrollTop);
     if (isMenu || initialize) { mouse.x = eval(Ex); mouse.y = eval(Ey); }
     if(initialize){
          if (isToolTip) { moveToolTip(lyrTT, mouse.y, mouse.x); if (isMenu) hideLayer(lyrMM); showLayer(lyrTT); }
     } else {
          if (isToolTip) { moveToolTip(lyrTT, 0, 0); hideLayer(lyrTT); }
     }

}

function hideLayer(lay) {

     if (tooltip_ie4) {document.all[lay].style.visibility = "hidden";}

     else if (tooltip_ns4) {document.layers[lay].visibility = "hide";}

     else if (tooltip_ns6) {document.getElementById([lay]).style.display = "none";}

}

function showLayer(lay) {

     if (tooltip_ie4) {document.all[lay].style.visibility = "visible";}

     else if (tooltip_ns4) {document.layers[lay].visibility = "show";}

     else if (tooltip_ns6) {document.getElementById([lay]).style.display = "block";}

}





/* ######################## API ########################## */

function showMenu() {

     var lay = arguments[0];

     var txt = arguments[1];

     lyrMM = lay;

     writeToLayer(lyrMM, txt);

     var ll = mouse.x;

     var tt = mouse.y;

     if (arguments.length>=3)

          if (!isNaN(arguments[2]))

               ll = arguments[2];

     if (arguments.length>=4)

          if (!isNaN(arguments[3]))

               tt = arguments[3];

     moveToolTip(lyrMM, tt, ll);

     showLayer(lyrMM);

}

/* ### added by Ronald on 15-11-2007 ### */
function showMenu2() {

     var lay = arguments[0];

     var txt = arguments[1];

     lyrMM = lay;

     writeToLayer(lyrMM, txt);

     var ll = screen.width/2;

     var tt = screen.height/2;

     if (arguments.length>=3)

          if (!isNaN(arguments[2]))

               ll = arguments[2];

     if (arguments.length>=4)

          if (!isNaN(arguments[3]))

               tt = arguments[3];

     moveToolTip(lyrMM, tt, ll);

     showLayer(lyrMM);
}



function hideMenu(lay) { hideLayer(lay); }



function showTip(lay, txt) {

     lyrTT = lay;

     writeToLayer(lyrTT, txt);

     Activate();

}

function hideTip(){ initialize=0; hideLayer(lyrTT); }


function makeTip(txt)
{
	
     tt= '<table border=0 cellspacing=0 cellpadding=1>';
     tt += '<tr><td class=tipborder><table width=100% border=0 cellspacing=0 cellpadding=3>';
     tt += '<tr><td class=tipbg valign=top><font size=-2>'+unescape(txt)+'</font></td></tr>';
     tt += '</table></td></tr></table>';

     return tt;

}

function showTipsHelp(myText){

	if (document.readyState=="complete")
	{
		tt= "<table border='0' cellspacing='0' cellpadding='1'>\n";
		tt += "<tr><td class='tipborder'><table width='100%' border='0' cellspacing='0' cellpadding='5'>\n";
		tt += "<tr><td class='tipbg' valign='top'>" + myText + "</td></tr>";
		tt += "</table></td></tr></table>\n";
		showTip('ToolTip', tt);
	}
}