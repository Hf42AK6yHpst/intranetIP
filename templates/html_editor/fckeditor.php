<?php
/*
 * FCKeditor - The text editor for Internet - http://www.fckeditor.net
 * Copyright (C) 2003-2009 Frederico Caldeira Knabben
 *
 * == BEGIN LICENSE ==
 *
 * Licensed under the terms of any of the following licenses at your
 * choice:
 *
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 *
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 *
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 *
 * == END LICENSE ==
 *
 * This is the integration file for PHP (All versions).
 *
 * It loads the correct integration file based on the PHP version (avoiding
 * strict error messages with PHP 5).
 * 
 * 
#   Date:   2019-09-23  Bill    [2019-0923-1111-44235]
#           modified FCKeditor(), add parm $is_scrollable
#	Date:	2011-08-25	Yuen
#			support iPad/Andriod with plain text editor
 */

global $userBrowser;
if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
{
	# plain text editor specially for iPad and Andriod
	class FCKeditor
	{
		var $Value;
		var $OtherIframeTagInfo;
		var $editor_name;
		var $editor_height;
		var $editor_width;
		var $first_run;
		
		function FCKeditor($editor_name, $editor_width,$editor_height, $toolbar_skin='noUse', $toolbar_set='noUse', $editor_preload_msg='', $is_scrollable=false)
		{
			$this->editor_name = $editor_name;
			$this->editor_width = ($editor_width=="100%" || $editor_width>100) ? "80%" : $editor_width;
			$this->editor_height = ($editor_height>20) ? 10 : $editor_height;
			$this->Value = $editor_preload_msg;
			$this->first_run = true;
		}
		
		function CreateHtml()
		{
			global $PATH_WRT_ROOT;
				
			$text_value = $this->Value;
			$text_value = str_replace("<br />", "", trim(strip_tags($text_value)));
			$rx = "<textarea name=\"".$this->editor_name."\" id=\"".$this->editor_name."\" cols=\"".$this->editor_width."\" rows=\"".$this->editor_height."\" ".$this->OtherIframeTagInfo." >".$text_value."</textarea>";;
			if ($this->first_run)
			{
				$rx .= "\n<script type=\"text/javascript\" src=\"{$PATH_WRT_ROOT}templates/jquery/jquery.autogrowtextarea.js\"></script>\n";
                $rx .= "<script language=\"javascript\" >\n";
				$rx .= "$(document).ready(function (){ $('#".$this->editor_name."').autoGrow(); });\n";
				$rx .= "</script>\n";
				$this->first_run = false;
			}
			return $rx;
		}
		
		function create()
		{
			echo $this->CreateHtml();
		}
		
		function create2()
		{
			return $this->CreateHtml();
		}
	}
}
else if (!function_exists('version_compare') || version_compare(phpversion(), '5', '<'))
{
	include_once('fckeditor_php4.php');
}
else
{
	include_once('fckeditor_php5.php');
}