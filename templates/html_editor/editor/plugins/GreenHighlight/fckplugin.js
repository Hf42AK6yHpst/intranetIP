function GreenHighlight()
{
}
GreenHighlight.prototype.Execute = function(){ 
	// Take an undo snapshot before changing the document
	FCKUndo.SaveUndoStep() ;
	
	var oFCKeditor = FCK;
	
	parent.GetSelectedText_Highlight(oFCKeditor, 'green');
	
	FCKUndo.SaveUndoStep() ;
	
	FCK.Focus() ;
	FCK.Events.FireEvent( 'OnSelectionChange' ) ;
}
GreenHighlight.prototype.GetState = function(){ 
      return FCK_TRISTATE_OFF;
}

// Register the command.
FCKCommands.RegisterCommand('GreenHighlight', new GreenHighlight());

// Add the button.
var item = new FCKToolbarButton('GreenHighlight', 'Green Highlight');
item.IconPath = FCKConfig.PluginsPath + 'GreenHighlight/remark_highlight_icon_05_on.png';
FCKToolbarItems.RegisterItem('GreenHighlight', item);
