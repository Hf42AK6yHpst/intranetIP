function OrangeHighlight()
{
}
OrangeHighlight.prototype.Execute = function(){ 
	// Take an undo snapshot before changing the document
	FCKUndo.SaveUndoStep() ;
	
	var oFCKeditor = FCK;
	
	parent.GetSelectedText_Highlight(oFCKeditor, 'orange');
	
	FCKUndo.SaveUndoStep() ;
	
	FCK.Focus() ;
	FCK.Events.FireEvent( 'OnSelectionChange' ) ;
}
OrangeHighlight.prototype.GetState = function(){ 
      return FCK_TRISTATE_OFF;
}

// Register the command.
FCKCommands.RegisterCommand('OrangeHighlight', new OrangeHighlight());

// Add the button.
var item = new FCKToolbarButton('OrangeHighlight', 'Orange Highlight');
item.IconPath = FCKConfig.PluginsPath + 'OrangeHighlight/remark_highlight_icon_02_on.png';
FCKToolbarItems.RegisterItem('OrangeHighlight', item);
