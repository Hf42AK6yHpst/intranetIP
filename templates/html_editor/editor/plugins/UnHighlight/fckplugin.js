function UnHighlight()
{
}
UnHighlight.prototype.Execute = function(){ 
	// Take an undo snapshot before changing the document
	FCKUndo.SaveUndoStep() ;
	
	var oFCKeditor = FCK;
	parent.GetSelectedText_UnHighlight(oFCKeditor, 'red');
	
	FCKUndo.SaveUndoStep() ;
	
	FCK.Focus() ;
	FCK.Events.FireEvent( 'OnSelectionChange' ) ;

}
UnHighlight.prototype.GetState = function(){ 
      return FCK_TRISTATE_OFF;
}

// Register the command.
FCKCommands.RegisterCommand('UnHighlight', new UnHighlight());

// Add the button.
var item = new FCKToolbarButton('UnHighlight', 'Undo Highlight');
item.IconPath = FCKConfig.PluginsPath + 'UnHighlight/remark_highlight_icon_del_on.png';
FCKToolbarItems.RegisterItem('UnHighlight', item);
