function LightGreenHighlight()
{
}
LightGreenHighlight.prototype.Execute = function(){ 
	// Take an undo snapshot before changing the document
	FCKUndo.SaveUndoStep() ;
	
	var oFCKeditor = FCK;
	
	parent.GetSelectedText_Highlight(oFCKeditor, 'lightgreen');
	
	FCKUndo.SaveUndoStep() ;
	
	FCK.Focus() ;
	FCK.Events.FireEvent( 'OnSelectionChange' ) ;
}
LightGreenHighlight.prototype.GetState = function(){ 
      return FCK_TRISTATE_OFF;
}

// Register the command.
FCKCommands.RegisterCommand('LightGreenHighlight', new LightGreenHighlight());

// Add the button.
var item = new FCKToolbarButton('LightGreenHighlight', 'Light Green Highlight');
item.IconPath = FCKConfig.PluginsPath + 'LightGreenHighlight/remark_highlight_icon_04_on.png';
FCKToolbarItems.RegisterItem('LightGreenHighlight', item);
