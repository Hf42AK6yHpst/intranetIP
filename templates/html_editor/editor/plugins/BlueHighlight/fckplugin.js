function BlueHighlight()
{
}
BlueHighlight.prototype.Execute = function(){ 
	// Take an undo snapshot before changing the document
	FCKUndo.SaveUndoStep() ;
	
	var oFCKeditor = FCK;
	
	parent.GetSelectedText_Highlight(oFCKeditor, 'blue');
	
	FCKUndo.SaveUndoStep() ;
	
	FCK.Focus() ;
	FCK.Events.FireEvent( 'OnSelectionChange' ) ;
}
BlueHighlight.prototype.GetState = function(){ 
      return FCK_TRISTATE_OFF;
}

// Register the command.
FCKCommands.RegisterCommand('BlueHighlight', new BlueHighlight());

// Add the button.
var item = new FCKToolbarButton('BlueHighlight', 'Blue Highlight');
item.IconPath = FCKConfig.PluginsPath + 'BlueHighlight/remark_highlight_icon_07_on.png';
FCKToolbarItems.RegisterItem('BlueHighlight', item);
