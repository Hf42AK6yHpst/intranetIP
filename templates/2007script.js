// Modifying by: 

var isShow = 1;
var hidetemp = 1;
var showtemp = 1;
var isShowAll = 0;

var showLinkText, hideLinkText;
var contentLeftPos = 150;
var MenuWidth = 170;

showLinkText = "<a href=\"javascript:void(0)\" class=\"hidemenu\" onMouseOver=\"MM_swapImage('arrow_show','','/images/2020a/leftmenu/icon_show_arrow_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" ";
showLinkText += " onClick=\"(isShow) ? HideLeftMenu(0, -"+contentLeftPos+"):ShowLeftMenu(-"+contentLeftPos+" ,0); \"";
showLinkText += "><img src=\"/images/2020a/leftmenu/icon_show_arrow_off.gif\" name=\"arrow_show\" width=\"14\" height=\"11\" border=\"0\" id=\"arrow_show\"><br>\n";
//showLinkText += "S<br>H<br>O<br>W<br></a>";
showLinkText += "&nbsp;<br />&nbsp;<br /></a>";


hideLinkText = "<a href=\"javascript:void(0)\" class=\"hidemenu\" onMouseOver=\"MM_swapImage('arrow_hide','','/images/2020a/leftmenu/icon_hide_arrow_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" ";
hideLinkText += " onClick=\"(isShow) ? HideLeftMenu(0, -"+contentLeftPos+"):ShowLeftMenu(-"+contentLeftPos+" ,0); \"";
hideLinkText += "><img src=\"/images/2020a/leftmenu/icon_hide_arrow_off.gif\" name=\"arrow_hide\" width=\"14\" height=\"11\" border=\"0\" id=\"arrow_hide\"><br>\n";
//hideLinkText += "H<br>I<br>D<br>E<br></a>";
hideLinkText += "&nbsp;<br />&nbsp;<br /></a>";


function HideLeftMenu_old(jNewPos, jTargetPost) {

	showtemp = 1;

	if (jTargetPost <= jNewPos)
	{
		document.getElementById('leftmenu').style.left = jNewPos + "px";
		document.getElementById('content').style.paddingLeft = (MenuWidth + jNewPos) + "px";
		document.getElementById('titlebar').style.paddingLeft = (MenuWidth + jNewPos) + "px";
		jNewPos = jNewPos - (15 * hidetemp);
		hidetemp ++;
		setTimeout("HideLeftMenu("+ jNewPos + ", " + jTargetPost + ")", 1);
	} else
	{
		document.getElementById('leftmenu').style.left = jTargetPost + "px";
		document.getElementById('content').style.paddingLeft = (MenuWidth + jTargetPost) + "px";
		document.getElementById('titlebar').style.paddingLeft = (MenuWidth + jTargetPost) + "px";
	}
	document.getElementById('showhideLinkText').innerHTML = showLinkText;
	isShow = 0;

}


function ShowLeftMenu_old(jNewPos, jTargetPost) {
	hidetemp = 1;
	if (jTargetPost >= jNewPos) {
		document.getElementById('leftmenu').style.left = jNewPos + "px";
		document.getElementById('content').style.paddingLeft = (contentLeftPos + jNewPos) + "px";
		document.getElementById('titlebar').style.paddingLeft = (MenuWidth + jNewPos) + "px";
		jNewPos = jNewPos + (15 * showtemp);
		showtemp ++;
		setTimeout("ShowLeftMenu("+ jNewPos + ", " + jTargetPost + ")", 1);
	} else {
		document.getElementById('leftmenu').style.left = jTargetPost + "px";
		document.getElementById('content').style.paddingLeft = (contentLeftPos + jTargetPost) + "px";
		document.getElementById('titlebar').style.paddingLeft = MenuWidth + "px";
	}
	document.getElementById('showhideLinkText').innerHTML = hideLinkText;
	isShow = 1;
}

function HideLeftMenu(jNewPos, jTargetPost) {
// changed getElementById('titlebar') to getElementById('module_title')
	showtemp = 1;

	if (jTargetPost <= jNewPos)
	{
		document.getElementById('leftmenu').style.left = jNewPos + "px";
		document.getElementById('content').style.paddingLeft = (MenuWidth + jNewPos) + "px";
		jNewPos = jNewPos - (15 * hidetemp);
		hidetemp ++;
		setTimeout("HideLeftMenu("+ jNewPos + ", " + jTargetPost + ")", 1);
		document.getElementById('module_title').className = 'menu_closed';
		/*document.getElementById('module_title').style.paddingLeft = 0 + "px";*/
	} else
	{
		document.getElementById('leftmenu').style.left = jTargetPost + "px";
		document.getElementById('content').style.paddingLeft = (MenuWidth + jTargetPost) + "px";
		document.getElementById('module_title').className = 'menu_closed';
		/*document.getElementById('module_title').style.paddingLeft = 0 + "px";*/
	}
	document.getElementById('showhideLinkText').innerHTML = showLinkText;
	isShow = 0;

}


function ShowLeftMenu(jNewPos, jTargetPost) {
// changed getElementById('titlebar') to getElementById('module_title')
	hidetemp = 1;
	if (jTargetPost >= jNewPos) {
		document.getElementById('leftmenu').style.left = jNewPos + "px";
		document.getElementById('content').style.paddingLeft = (contentLeftPos + jNewPos) + "px";
		jNewPos = jNewPos + (15 * showtemp);
		showtemp ++;
		setTimeout("ShowLeftMenu("+ jNewPos + ", " + jTargetPost + ")", 1);
		document.getElementById('module_title').className = 'menu_opened';
		/*document.getElementById('module_title').style.paddingLeft = 0 + "px";*/
		
	} else {
		document.getElementById('leftmenu').style.left = jTargetPost + "px";
		document.getElementById('content').style.paddingLeft = (contentLeftPos + jTargetPost) + "px";
		document.getElementById('module_title').className = 'menu_opened';
		/*document.getElementById('module_title').style.paddingLeft = 0 + "px";*/
	}
	document.getElementById('showhideLinkText').innerHTML = hideLinkText;
	isShow = 1;
}

/*
* Display or hide contents
*/
function jDISPLAY_ITEM(jParItemID, jStyle){
	//block, none, inline
	//var jCurrentStyle = document.all[jParItemID].style.display;
	var jCurrentStyle = document.getElementById(jParItemID).style.display;
	var jStyleNew = "none";
	if (typeof(jStyle)!="undefined")
	{
		jStyleNew = jStyle;
	} else
	{
		jStyleNew = (jCurrentStyle=="none") ? "block" : "none";
	}

	//document.all[jParItemID].style.display = jStyleNew;
	document.getElementById(jParItemID).style.display = jStyleNew;

	return;
}


/*
* Hide/Show sub-menus
*/
function jDISPLAY_MENU_ITEM(jKey){
	
	jDISPLAY_ITEM("ItemList"+jKey);
	jDISPLAY_ITEM("Table"+jKey+"Expand");
	jDISPLAY_ITEM("Table"+jKey+"Close");
	
	var jClosed = (document.getElementById("ItemList"+jKey).style.display=="none") ? "CLOSED" : "OPENED";
	var jCookieName = MenuID + "_" + jKey;

	var jExpDate = new Date();
	jExpDate.setTime(jExpDate.getTime() + ( 3600 * 1000 ));
	jExpDate = null;
	setCookie(jCookieName, jClosed, jExpDate, "/home");

}

/*
* Hide/Show all sub-menus
*/

function jDISPLAY_ALL_MENU_ITEM(jKeyArray){
	
	var jKey = "";
	var jStyle, jExpandStyle, jCloseStyle;
		
	var jDate = new Date();
	var isShowAll = getCookie("isShowAll");
	if(isShowAll==1)
	{
		jStyle = "none";
		jExpandStyle = "none";
		jCloseStyle = "block";

		var jDate = new Date();
		jDate.setTime(jDate.getTime() + ( 3600 * 1000 ));
		jDate = null;
		setCookie("isShowAll", 0, jDate, "/home");
	}
	else
	{
		jStyle = "block";
		jExpandStyle = "block";
		jCloseStyle = "none";

		var jDate = new Date();
		jDate.setTime(jDate.getTime() + ( 3600 * 1000 ));
		jDate = null;
		setCookie("isShowAll", 1, jDate, "/home");
	}

	for(var i=0; i<jKeyArray.length; i++)
	{
		jKey = jKeyArray[i];
		jDISPLAY_ITEM("ItemList"+jKey, jStyle);
		jDISPLAY_ITEM("Table"+jKey+"Expand", jExpandStyle);
		jDISPLAY_ITEM("Table"+jKey+"Close", jCloseStyle);

		var jClosed = (document.getElementById("ItemList"+jKey).style.display=="none") ? "CLOSED" : "OPENED";
		var jCookieName = MenuID + "_" + jKey;

		var jExpDate = new Date();
		jExpDate.setTime(jExpDate.getTime() + ( 3600 * 1000 ));
		jExpDate = null;
		setCookie(jCookieName, jClosed, jExpDate, "/home");
	}
}

/*
* Initialize menu when starts
*/
function jINITIAL_MENU_OPEN(){

	var jCookieName;

	if (typeof(MenuID)!="undefined" && typeof(MenuKeyArray)!="undefined")
	{
		for (var i=0; i<MenuKeyArray.length; i++)
		{
			jKey = MenuKeyArray[i];
			jCookieName = MenuID + "_" + jKey;
			if (getCookie(jCookieName)=="CLOSED")
			{
				jDISPLAY_MENU_ITEM(jKey);
			}
		}
	}

}



function jFindPos(obj) {
	// use jquery to get element position if jquery is available
	if($ && typeof($)=='function'){
		var pos = $(obj).position();
		return [pos.left,pos.top];
	}
	
	var curleft = curtop = 0;
	if(window.self !== window.top || typeof isNCS != undefined){
		curleft = obj.offsetParent.offsetLeft + obj.offsetLeft;
		curtop = obj.offsetParent.offsetTop + obj.offsetTop;
	}else{
		if (obj.offsetParent)
		{
			curleft = obj.offsetLeft;
			curtop = obj.offsetTop;
			while (obj = obj.offsetParent)
			{
				curleft += obj.offsetLeft;
				curtop += obj.offsetTop;
			}
		}
	}
	return [curleft,curtop];
}


// generate options from jArrData and bold selected option
function jSelectPresetText(jArrData, jTarget, jMenu, jPos){
	var listStr = "";
	var fieldValue;
	var count = 0;

	var btn_hide = (typeof(js_btn_hide)!="undefined") ? js_btn_hide : "";
	var box_height = (jArrData.length>10) ? "200" : "105";

	listStr = "<table width='160' height='100' border='0' cellpadding='0' cellspacing='0'>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' height='19'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' width='5' height='19'><img src='"+jImagePath+"/can_board_01.gif' width='5' height='19'></td>";
	listStr += "<td style='border:0px; padding:0px;' height='19' valign='middle' background='"+jImagePath+"/can_board_02.gif'></td>";
	listStr += "<td style='border:0px; padding:0px;' width='19' height='19'><a href='javascript:void(0)' onClick=\"document.getElementById('" + jMenu + "').style.display='none';setDivVisible(false, '"+jMenu+"', 'lyrShim')\"><img src='"+jImagePath+"/can_board_close_off.gif' name='pre_close' width='19' height='19' border='0' id='pre_close' onMouseOver=\"MM_swapImage('pre_close','','"+jImagePath+"/can_board_close_on.gif',1)\" onMouseOut='MM_swapImgRestore()' /></a></td>";
	listStr += "</tr>";
	listStr += "</table></td>";
	listStr += "</tr>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' ><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	listStr += "<tr style='border:0px; padding:0px;' >";
	listStr += "<td style='border:0px; padding:0px;' width='5' height='50' background='"+jImagePath+"/can_board_04.gif'><img src='"+jImagePath+"/can_board_04.gif' width='5' height='19'></td>";
	
	// 20090907 Ivan
	// Problem: Scroll bar disappeared if the user open the layer more than one times
	// Temp Solution: Always show the scroll bar - changed style from overflow:auto; to overflow: scroll;
	listStr += "<td style='border:0px; padding:0px;' bgcolor='#FFFFF7'><div style=\"overflow-X: scroll; overflow-Y: scroll; height: " + box_height + "px; width: 160px; \">";
	
	listStr += "<table width='180' border='0' cellspacing='0' cellpadding='2' class='tbclip'>";
	if (typeof(jArrData)=="undefined" || jArrData.length==0)
	{
		if (typeof(js_preset_no_record)!="undefined")
		{
			listStr += "<tr><td style='border:0px; padding:0px;' class='guide' align='center'>"+js_preset_no_record+"</td></tr>";
		}
	} else
	{
		for (var i = 0; i < jArrData.length; i++)
		{
			for (var j = 0; j < jArrData[i].length; j++)
			{
				fieldValue = document.getElementById(jTarget).value.replace(/&/g, "&amp;").replace(/'/g, "&#96;").replace(/`/g, "&#96;").replace(/"/g, "&quot;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
				tmpValue = jArrData[i][j].replace(/&#039;/g, "\\'");
				tmpShowValue = jArrData[i][j].replace(/</g, "&lt;").replace(/>/g, "&gt;");

				listStr += "<tr><td style=\"border-bottom:1px #EEEEEE solid; background-color: ";
				if (fieldValue == jArrData[i][j])
				{
					listStr += "#FFF2C3";
				} else
				{
					listStr += "#FFFFFF";
				}
				listStr += "\">";

				if (fieldValue == jArrData[i][j])
				{
					listStr += "<b>";
				}
				listStr += "<a class='presetlist' style=\"color:#4288E8\" href=\"javascript:document.getElementById('" + jTarget + "').value = '" + tmpValue + "' ;document.getElementById('" + jMenu + "').style.display= 'none';setDivVisible(false, '"+jMenu+"', 'lyrShim')\" title=\"" + jArrData[i][j] + "\">" + tmpShowValue + "</a>";
				
				if (fieldValue == jArrData[i][j])
				{
					listStr += "</b>";
				}

				listStr += "</td></tr>";
				count ++;
			}
		}
	}
	listStr += "</table></div></td>";
	listStr += "<td style='border:0px; padding:0px;' width='6' background='"+jImagePath+"/can_board_06.gif'><img src='"+jImagePath+"/can_board_06.gif' width='6' height='6' border='0' /></td>";
	listStr += "</tr>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' width='5' height='6'><img src='"+jImagePath+"/can_board_07.gif' width='5' height='6'></td>";
	listStr += "<td style='border:0px; padding:0px;' height='6' background='"+jImagePath+"/can_board_08.gif'><img src='"+jImagePath+"/can_board_08.gif' width='100%' height='6' border='0' /></td>";
	listStr += "<td style='border:0px; padding:0px;' width='6' height='6'><img src='"+jImagePath+"/can_board_09.gif' width='6' height='6'></td>";
	listStr += "</tr>";
	listStr += "</table></td>";
	listStr += "</tr>";
	listStr += "</table>";

	document.getElementById(jMenu).innerHTML = listStr;
	document.getElementById(jMenu).style.visibility = 'visible';

	var pos = jFindPos(document.getElementById("posimg" + jPos));

	document.getElementById(jMenu).style.left = parseInt(pos[0]) + "px";
	document.getElementById(jMenu).style.top = parseInt(pos[1]) + "px";

	setDivVisible(true, jMenu, "lyrShim");

}


// generate options from jData for creating hyperlink
function jSelectPresetTextForLink(jData, jMenu, jRowTotal, jPos){
	var listStr = "";
	var box_height = (jRowTotal>4) ? "200" : "150";

	listStr = "<table width='160' height='100' border='0' cellpadding='0' cellspacing='0'>";
	listStr += "<tr>";
	listStr += "<td height='19'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	listStr += "<tr>";
	listStr += "<td width='5' height='19'><img src='"+jImagePath+"/can_board_01.gif' width='5' height='19'></td>";
	listStr += "<td height='19' valign='middle' background='"+jImagePath+"/can_board_02.gif'></td>";
	listStr += "<td width='19' height='19'><a href='javascript:void(0)' onClick=\"document.getElementById('" + jMenu + "').style.display='none';setDivVisible(false, '"+jMenu+"', 'lyrShim')\"><img src='"+jImagePath+"/can_board_close_off.gif' name='pre_close' width='19' height='19' border='0' id='pre_close' onMouseOver=\"MM_swapImage('pre_close','','"+jImagePath+"/can_board_close_on.gif',1)\" onMouseOut='MM_swapImgRestore()' /></a></td>";
	listStr += "</tr>";
	listStr += "</table></td>";
	listStr += "</tr>";
	listStr += "<tr>";
	listStr += "<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	listStr += "<tr>";
	listStr += "<td width='5' height='50' background='"+jImagePath+"/can_board_04.gif'><img src='"+jImagePath+"/can_board_04.gif' width='5' height='19'></td>";
	
	// 20090907 Ivan
	// Problem: Scroll bar disappeared if the user open the layer more than one times
	// Temp Solution: Always show the scroll bar - changed style from overflow:auto; to overflow: scroll;
	listStr += "<td bgcolor='#FFFFF7'><div style=\"overflow: scroll; height: " + box_height + "px;\">";
	if (typeof(jData)=="undefined" || jData=="")
	{
		if (typeof(js_preset_no_record)!="undefined")
		{
			listStr += "<table width='100%' border='0' cellspacing='0' cellpadding='2' class='tbclip'>";
			listStr += "<tr><td class='guide' align='center'>"+js_preset_no_record+"</td></tr>";
			listStr += "</table>";
		}
	} else
	{
		listStr += jData.toString().replace(/&#039;/g, "\'");
	}
	listStr += "</div></td>";
	listStr += "<td width='6' background='"+jImagePath+"/can_board_06.gif'><img src='"+jImagePath+"/can_board_06.gif' width='6' height='6' border='0' /></td>";
	listStr += "</tr>";
	listStr += "<tr>";
	listStr += "<td width='5' height='6'><img src='"+jImagePath+"/can_board_07.gif' width='5' height='6'></td>";
	listStr += "<td height='6' background='"+jImagePath+"/can_board_08.gif'><img src='"+jImagePath+"/can_board_08.gif' width='100%' height='6' border='0' /></td>";
	listStr += "<td width='6' height='6'><img src='"+jImagePath+"/can_board_09.gif' width='6' height='6'></td>";
	listStr += "</tr>";
	listStr += "</table></td>";
	listStr += "</tr>";
	listStr += "</table>";

	document.getElementById(jMenu).innerHTML = listStr;
	document.getElementById(jMenu).style.visibility = 'visible';

	var pos = jFindPos(document.getElementById("poslink" + jPos));

	document.getElementById(jMenu).style.left = parseInt(pos[0]) + "px";
	document.getElementById(jMenu).style.top = parseInt(pos[1]) + 19 + "px";

	setDivVisible(true, jMenu, "lyrShim");

}

//Array prototype function, for IE
if(!Array.indexOf){
	    Array.prototype.indexOf = function(obj){
	        for(var i=0; i<this.length; i++){
	            if(this[i]==obj){
	                return i;
	            }
	        }
	        return -1;
	    }
	}
