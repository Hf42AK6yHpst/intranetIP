( function() {

	CKEDITOR.plugins.add( 'MarginalComment', {
		lang: 'en,zh',
		icons: 'ed_marginalcomment', // %REMOVE_LINE_CORE%
		hidpi: false, 
	    init: function( editor ) {
	    	this.Name = editor.lang.MarginalComment.title;
	        editor.addCommand( 'MarginalComment', {
	            exec: function( editor ) {	
						var oFCKeditor = editor;
						window.oFCKeditor = oFCKeditor;
						window.IsIE = parent.IsIE;
						var eclassVersion = window.parent.location.pathname.split('/')[1];
						window.open(window.parent.location.origin+"/"+eclassVersion+"/src/tool/misc/html_editor/popups/insert_marginal_comment.php?objname="+editor.name+"&editor=CKEditor","MarginalComment", "resizable=no,scrollbars=no,modal=yes,width=410,height=200");					
	            },
	            GetState: function(){
	            	return CKEDITOR.TRISTATE_OFF;
	            }
	        });
	        editor.ui.addButton( 'MarginalComment', {
	            label: editor.lang.MarginalComment.title,
	            command: 'MarginalComment',
	            toolbar: 'MarginalComment,40'
	        });
	    }
	});

})();