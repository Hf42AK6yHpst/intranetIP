( function() {
	CKEDITOR.plugins.add( 'InsertSticker', {
		lang: 'en,zh,zh-cn',
		icons: 'edsticker', // %REMOVE_LINE_CORE%
		hidpi: true, // %REMOVE_LINE_CORE%
	    init: function( editor ) {
	    	this.Name = editor.lang.InsertSticker.title;
	        editor.addCommand( 'InsertSticker', {
	            exec: function( editor ) {
	                 	var oFCKeditor = editor;
						window.oFCKeditor = oFCKeditor;
						window.IsIE = parent.IsIE||CKEDITOR.env.ie;
						var eclassVersion = parent.location.pathname.split("/")[1];
						var eclassPath = parent.location.origin + "/"+ eclassVersion +"/";
						newWindow(eclassPath+"src/tool/misc/html_editor/popups/insert_sticker.php?objname=htmlEditorFrame");
	            },
	            GetState: function(){
	            	return CKEDITOR.TRISTATE_OFF;
	            }
	        });
	        editor.ui.addButton( 'InsertSticker', {
	            label: editor.lang.InsertSticker.title,
	            command: 'InsertSticker',
	            toolbar: 'InsertSticker,40'
	        });
	    }
	});
	
	// open window functions
	var objWin;
	
	function newWindow(url){
		var win_name = "ec_popup";
	
		var win_size = "resizable,scrollbars,status=0,top=140,left=140,width=400,height=400";
	
		//for HTML editor popup : insert sticker
		win_name = "ec_popup20";
		win_size = "resizable,scrollbars,status=0,top=50,left=50,width=720,height=350";
		
		if (newWindow.arguments.length>3)
		{
		    win_name = "GivenSizeWin";
			var is_scroll = 0;
			var is_resizable = 0;
			if (newWindow.arguments.length>4)
			{
			    is_scroll = newWindow.arguments[4];
			}
			if (newWindow.arguments.length>5)
			{
			    is_resizable = (newWindow.arguments[5]==1) ? "1" : "0";
			}
			if (newWindow.arguments.length>6)
			{
			    win_name = (newWindow.arguments[6]);
			}
	
			win_size = "resizable="+is_resizable+",scrollbars="+is_scroll+",status=0,top=40,left=40,width="+newWindow.arguments[2]+",height="+newWindow.arguments[3]+",toolbars=0";
		}
		if (navigator.appName=="Microsoft Internet Explorer" && navigator.appVersion >="4")
	
		if (objWin)
		{
			objWin.close();
		}
	
		objWin = window.open (url, win_name, win_size);
		if (navigator.appName=="Netscape" && navigator.appVersion >= "3")
		{
			objWin.focus();
		}
	
		return;
	}
} )();
