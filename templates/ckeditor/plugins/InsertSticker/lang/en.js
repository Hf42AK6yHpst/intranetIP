/*
 * For CKeditor 4.6
 * 
 * 
 * File Name: en.js
 * 	English language file for the Insert Image Flash plugin.
 * 
 * File Authors:
 * 		Broadlearning Education (Asia) Limited
 */

CKEDITOR.plugins.setLang('InsertSticker','en', {
    title: 'Insert Sticker'
} );