<?php
// Cross-Origin Resource Sharing Header
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: X-Requested-With, Content-Type, Accept');
foreach($_REQUEST as $k=>$r){
	${$k} = $r;
} 
?>
<?php
//$deptId = 'System';
$deptId = 'CS';
$dir = str_replace("dialogs","",getcwd())."templates/".$deptId;
$fileList = scandir($dir);
$content = "";
$templatesJS = " var templateArr = {};";
$selectionBox = "<select onchange='changeTemplate(this)'>";
if(count($fileList) > 2){
	for($i=2; $i < count($fileList); $i++){
		$templateThis = preg_replace("/\r\n|\n\r|\r|\n/","",nl2br(file_get_contents($dir.'/'.$fileList[$i])));
		if($i==2){
			$content = $templateThis;
			$isSelected = "selected";
		}else{
			$isSelected = "";	
		}		
		$selectionBox .= "<option value='".($i-2)."' $isSelected>$fileList[$i]</option>";
		$templatesJS .= "templateArr[".($i-2)."] = '".$templateThis."';";
	}
}else{
	$selectionBox .= "<option value=''>No Templates are found</option>";
}
$selectionBox .= "</select>";
?>
<html>
	<head>
		<script language="javascript" >
			var objname     = "<?=$editorName?>";
			var oCKeditor = window.opener.CKEDITOR.instances[objname];
			<?=$templatesJS?>
			function insertContent(){
				opener.addTemplatesToEditor(objname, document.getElementById('template').innerHTML);
				window.close();
			}
			function changeTemplate(obj){
				document.getElementById('template').innerHTML = templateArr[obj.value];
			}
		</script>
	</head>
	<body>
		<?=$selectionBox?>
		<br/>
		<b>Preview:</b><br/>
		<div id='template' style="border: 2px solid grey;  border-radius: 8px; min-height:400px;">
			<?=htmlspecialchars_decode($content)?>
		</div>
		<br/>
		<div style="margin-left:auto;margin-right:auto; width:120px;">
		<button type="button" onclick='javascript:insertContent()'>Insert</button>
		<button type="button" onclick='window.close()'>Close</button>
		</div>
	</body>
</html>