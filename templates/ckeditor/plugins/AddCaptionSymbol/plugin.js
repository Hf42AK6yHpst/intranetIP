( function() {

	CKEDITOR.plugins.add( 'AddCaptionSymbol', {
		lang: 'en,zh',
		icons: 'AC', // %REMOVE_LINE_CORE%
		hidpi: false, 
	    init: function( editor ) {
	    	this.Name = editor.lang.AddCaptionSymbol.title;
	        editor.addCommand( 'AddCaptionSymbol', {
	            exec: function( editor ) {
	            		this.Name = editor.lang.AddCaptionSymbol.title;
	            		var oFCKeditor = editor;
						var SelectedText = editor.getSelectedHtml(editor);
					
						parent.HighlightSelectedText(SelectedText,'blue');	
					
						var html = "<font color='blue'>[AC]</font>";
						if (window.navigator.userAgent.indexOf("MSIE") > 0  || !!navigator.userAgent.match(/Trident.*rv\:11\./)){
							if (window.getSelection) {
								editor.InsertHtml(parent.Trim(html));
							}else{
								SelectedText[0].pasteHTML(parent.Trim(html));
							}	
						} else {
							editor.InsertHtml(parent.Trim(html));
						}		
	            },
	            GetState: function(){
	            	return CKEDITOR.TRISTATE_OFF;
	            }
	        });
	        editor.ui.addButton( 'AddCaptionSymbol', {
	            label: editor.lang.AddCaptionSymbol.title,
	            command: 'AddCaptionSymbol',
	            toolbar: 'AddCaptionSymbol,40'
	        });
	    }
	});

})();