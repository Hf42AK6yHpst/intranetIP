( function() {

	CKEDITOR.plugins.add( 'NeedExplanationSymbol', {
		lang: 'en',
		icons: 'ed_needexplanationsymbol', // %REMOVE_LINE_CORE%
		hidpi: false, 
	    init: function( editor ) {
	    	this.Name = 'Need Explanation';
	        editor.addCommand( 'NeedExplanationSymbol', {
	            exec: function( editor ) {
						var oFCKeditor = editor;
						var SelectedText = editor.getSelectedHtml(editor);
					
						parent.HighlightSelectedText(SelectedText,'green');	
					
						var html = "<font color='green'>[NE]</font>";
						if (window.navigator.userAgent.indexOf("MSIE") > 0  || !!navigator.userAgent.match(/Trident.*rv\:11\./)){
							if (window.getSelection) {
								editor.InsertHtml(parent.Trim(html));
							}else{
								SelectedText[0].pasteHTML(parent.Trim(html));
							}	
						} else {
							editor.InsertHtml(parent.Trim(html));
						}				
	            },
	            GetState: function(){
	            	return CKEDITOR.TRISTATE_OFF;
	            }
	        });
	        editor.ui.addButton( 'NeedExplanationSymbol', {
	            label: 'Need Explanation',
	            command: 'NeedExplanationSymbol',
	            toolbar: 'NeedExplanationSymbol,40'
	        });
	    }
	});

})();