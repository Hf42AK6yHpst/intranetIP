CKEDITOR.plugins.add( 'pluploadimage', {
	lang: 'en,zh',
    icons: 'pluploadimage',
	hidpi: true,
    init: function( editor ) {
    	var allowedExt = ['jpg', 'jpeg', 'gif', 'png'];
    	
    	$('#'+editor.name).after('<div id="'+editor.name+'_plupload_container" name="'+editor.name+'_plupload_container"><div id="'+editor.name+'_plupload" name="'+editor.name+'_plupload" style="display:none">[File Upload]</div></div>');

    	var uploader = new plupload.Uploader({
			runtimes 			: 'html5,html4,flash,silverlight,gears,browserplus',
			container 			: editor.name+'_plupload_container',
			browse_button 		: editor.name+'_plupload',
			file_data_name 		: 'Filedata',
			multi_selection		: false,
			flash_swf_url 		: '/eclass40/js/plupload2.1.2/Moxie.swf',
			silverlight_xap_url : '/eclass40/js/plupload2.1.2/Moxie.xap',
			url 				: '/eclass40/src/tool/plupload/uploadImage.php',
			multipart_params 	: { 
				toPath 	: editor.config.pluploadimageUploadDir,
				isFrom	: window.location.href									
			},
			filters				: {
				mime_types : [{ title : "Image files", extensions : allowedExt.join() }]
			},
			init				: {
				FilesAdded : function(up, files){
					var invalidFile    = false;
					var invalidFileMsg = ''; 
					var thisObj		   = this;
					plupload.each(files, function(file){
						if(!invalidFile){
							var ext = /[^.]+$/.exec(file.name);
							if(allowedExt.indexOf(ext[0].toLowerCase())==-1){
								invalidFile    = true;
								invalidFileMsg = editor.lang.pluploadimage.uploadextensionwarning+'\n'+allowedExt.join();
							}
						}
						
						if(invalidFile){
							thisObj.removeFile(file);
						}
					});
					
					if(invalidFile){
						alert(invalidFileMsg);
					}
					else{
						this.disableBrowse(true);
						this.start();
					}
				},
				FileUploaded : function(up, file, response){
					var uploaderResult = JSON.parse(response.response);
					
					if(uploaderResult.result){
						var imgElement = editor.document.createElement('img');
						imgElement.setAttribute('src', uploaderResult.path);
						
						if(uploaderResult.imageProp.width!=null && uploaderResult.imageProp.width>400){
							imgElement.setAttribute('width', '400px');
						}
						
						editor.insertElement(imgElement);
					}
					else{
						alert(editor.lang.pluploadimage.uploadfail);
					}
				},
				UploadComplete : function(up, files){
					this.disableBrowse(false);
				},
				Error : function(up, err){
					this.disableBrowse(false);
					alert(editor.lang.pluploadimage.uploadfail+'\n'+err.message+' (#:'+err.code+')');
				}
			}
		});
    	uploader.init();
    	
    	editor.addCommand( 'insertpluploadimage', {
            exec: function( editor ) {
            	$('#'+editor.name+'_plupload').click();
            }
        });
    	
        editor.ui.addButton( 'pluploadimage', {
            label: editor.lang.pluploadimage.uploadimage,
            command: 'insertpluploadimage',
            toolbar: 'insert,1'
        });
    }
});