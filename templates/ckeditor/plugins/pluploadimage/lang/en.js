CKEDITOR.plugins.setLang( 'pluploadimage', 'en', {
	uploadimage : 'Direct Upload',
	uploadextensionwarning : 'Only files with following extensions are allowed.',
	uploadfail : 'Fail to upload image'
} );