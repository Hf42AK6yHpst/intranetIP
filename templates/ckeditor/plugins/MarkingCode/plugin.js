var isMarkingCodeShow=false;
( function() {

	CKEDITOR.plugins.add( 'MarkingCode', {
		lang: 'en,zh',
		icons: 'ed_markingcode', // %REMOVE_LINE_CORE%
		hidpi: false, 
	    init: function( editor ) {
	    	this.Name = editor.lang.MarkingCode.title;
	        editor.addCommand( 'MarkingCode', {
	            exec: function( editor ) {
	            		isMarkingCodeShow = (!isMarkingCodeShow);
						if(isMarkingCodeShow)
							updateMarkingCodeSet(1);
						else
							hideMarkingCodeSet();					
	            },
	            GetState: function(){
	            	return CKEDITOR.TRISTATE_OFF;
	            }
	        });
	        editor.ui.addButton( 'MarkingCode', {
	            label: editor.lang.MarkingCode.title,
	            command: 'MarkingCode',
	            toolbar: 'MarkingCode,40'
	        });
	    }
	});

})();

function hideMarkingCodeSet(){
	var ToolBarNode = document.getElementById('xToolbar');
	var ChildNode = document.getElementById('marking_code_table');
	var BRNode = document.getElementById('marking_code_br');
	ToolBarNode.removeChild(ChildNode);
	ToolBarNode.removeChild(BRNode);
	return;
}
function updateMarkingCodeSet(is_inital){
	if(!is_inital)
		var marking_code_set = document.getElementById('marking_code_set').value;
	else
		var marking_code_set = 0;
	parent.changeMarkingCode(marking_code_set);
	var code_type = parent.JSMarkingCodeType; 	 
	///////////////Select//////////////
	var s = document.createElement("select");
	s.id = 'marking_code_set';
	s.style.width = "130px";
	s.setAttribute('onchange','updateMarkingCodeSet(0);');
	for(var i=0;i<code_type.length;i++){
		s.options.add(new Option(code_type[i], i));
	}
	s.options[marking_code_set].setAttribute("selected", "selected");
	///////////////Select//////////////
	
	var ToolBarNode = document.getElementById('xToolbar');
	var br = document.createElement("br");
	br.style.clear = "both";	
	br.id = "marking_code_br";	
	if(ToolBarNode.lastChild.id=='marking_code_table'){
		var NewNode = ToolBarNode.lastChild;
	}else{
		var NewNode = ToolBarNode.lastChild.cloneNode(true);
		NewNode.id = 'marking_code_table';		
	}	
	
	NewNode.removeChild(NewNode.childNodes[0]);
	var c1,c2,r;
	r = NewNode.insertRow(0); 
	c1 = r.insertCell(0);
	c1.align = 'center';
	if(FCKBrowserInfo.IsIE){
		c1.style.width = "140px";
		c1.style.verticalAlign = "top";
	}else{
		c1.setAttribute("style", "width:140px;");
		c1.setAttribute("valign","top");
	}
	c1.appendChild(s);
	c2 = r.insertCell(1);
	var code = parent.JSMarkingCode;

	for(var i=0;i<code.length;i++){
	 	var div = document.createElement("div");
			if(FCKBrowserInfo.IsIE)
				div.style.cssText = "float:left; width:30px; text-align:center; padding:3px;border:1px #B2CBFF solid;margin:1px;background-color:#f7f8fd;"; 
			else
				div.setAttribute("style", "float:left; min-width:30px; text-align:center; padding:3px;border:1px #B2CBFF solid;margin:1px;background-color:#f7f8fd;"); 

		
		var onclick = div.getAttribute("onclick"); 
		if(typeof(onclick) != "function" && !FCKBrowserInfo.IsIE) { 
			div.setAttribute('onclick','InsertMarkingCode('+i+');'); // for FF,IE8,Chrome
			div.setAttribute('onmouseover','this.style.cursor="pointer";this.style.border="1px #316ac5  solid";this.style.backgroundColor="#dff1ff";'); 
			div.setAttribute('onmouseout','this.style.border="1px #B2CBFF solid";this.style.backgroundColor="#f7f8fd";'); 
		} else {
			div.onclick = (function() {
				var currentI = i;
				return function() { 
				  InsertMarkingCode(currentI + '');
				}
			})(); // for IE
			div.onmouseover = function() { 
				this.style.cursor="pointer";
				this.style.border="1px #316ac5 solid";
				this.style.backgroundColor="#dff1ff";
			};
			div.onmouseout = function() { 
				this.style.border="1px #B2CBFF solid";
				this.style.backgroundColor="#f7f8fd";
			};			
		}
		div.innerHTML = code[i];
		c2.appendChild(div);
	}	
	if(ToolBarNode.lastChild.id!='marking_code_table'){
		ToolBarNode.appendChild(br);
	}
	ToolBarNode.appendChild(NewNode); 
}
function InsertMarkingCode(i){
	var code = parent.JSMarkingCode;
	var html = "<font color='red'>[" + code[i] + "]</font>";
	editor.InsertHtml(parent.Trim(html));
}