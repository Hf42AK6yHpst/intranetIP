( function() {

	CKEDITOR.plugins.add( 'MarkingScheme', {
		lang: 'en,zh',
		icons: 'ed_markingscheme', // %REMOVE_LINE_CORE%
		hidpi: false, 
	    init: function( editor ) {
	    	this.Name = editor.lang.MarkingScheme.title;
	        editor.addCommand( 'MarkingScheme', {
	            exec: function( editor ) {
	            		var eclassVersion = window.parent.location.pathname.split('/')[1];			
						var data = parent.document.getElementById('marking_scheme_data').value;	
						var markingS = '<p><hr><br>';
						markingS += '<table width="400" border="0" cellspacing="0" cellpadding="0" align="center">';
						markingS += '<tr><td bgcolor="#A87743" width="180" height="25" style="BORDER-LEFT: #DDAC3F 1px solid; BORDER-RIGHT: #DDAC3F 1px solid; BORDER-TOP: #DDAC3F 1px solid; margin-left:5px;">&nbsp; &nbsp;<font face="tahoma, arial" size="2" color="white"><b>Marking Scheme</b></font></td><td width="220">&nbsp;</td></tr>';
						markingS += '<tr><td bgcolor="#A87743" style="BORDER-LEFT: #DDAC3F 1px solid;"><img src="'+window.parent.location.origin+"/"+eclassVersion+'images/space.gif" border="0" width="180" height="7"></td><td bgcolor="#A87743" style="BORDER-RIGHT: #DDAC3F 1px solid; BORDER-TOP: #DDAC3F 1px solid;"><img src="'+window.parent.location.origin+"/"+eclassVersion+'images/space.gif" border="0" width="220" height="7"></td></tr>';
						markingS += '<tr><td colspan="2">';
						markingS += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
					  	markingS += '<tr><td bgcolor="#FFF6E8" style="BORDER-LEFT: #DDAC3F 1px solid; BORDER-RIGHT: #DDAC3F 1px solid; BORDER-BOTTOM: #DDAC3F 1px solid;"><table width="90%" border="0" align="center"><tr><td><font color="#894802" size="2">'+data+'</font></td></tr></table>';
						markingS += '</td></tr></table>';
						markingS += '</td></tr></table></p>'; 
					
						editor.EditorDocument.body.innerHTML += markingS;				
	            },
	            GetState: function(){
	            	return CKEDITOR.TRISTATE_OFF;
	            }
	        });
	        editor.ui.addButton( 'MarkingScheme', {
	            label: editor.lang.MarkingScheme.title,
	            command: 'MarkingScheme',
	            toolbar: 'MarkingScheme,40'
	        });
	    }
	});

})();