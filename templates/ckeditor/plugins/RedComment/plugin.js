( function() {

	CKEDITOR.plugins.add( 'RedComment', {
		lang: 'en,zh',
		icons: 'ed_insertmissing', // %REMOVE_LINE_CORE%
		hidpi: false, 
	    init: function( editor ) {
	    	this.Name = editor.lang.RedComment.title;
	        editor.addCommand( 'RedComment', {
	            exec: function( editor ) {
	            		var html = "<font color='red'> { } </font>&nbsp;";
						editor.InsertHtml(parent.Trim(html));
	            },
	            GetState: function(){
	            	return CKEDITOR.TRISTATE_OFF;
	            }
	        });
	        editor.ui.addButton( 'RedComment', {
	            label: editor.lang.RedComment.title,
	            command: 'RedComment',
	            toolbar: 'RedComment,40'
	        });
	    }
	});

})();

