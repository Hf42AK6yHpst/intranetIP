/*
 * For FCKeditor 2.3
 * 
 * 
 * File Name: zh-cn.js
 *	Simiplified Chinese language file for plugin "FlippedChannels".
 * 
 * File Authors:
 * 		Broadlearning Education (Asia) Limited
 */
CKEDITOR.plugins.setLang('InsertFlippedChannels','zh-cn', {
    title: '插入翻转频道'
} );
