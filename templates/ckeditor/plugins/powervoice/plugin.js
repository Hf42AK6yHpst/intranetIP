// Modifying by : Paul
// Modification Log:
// 2019-05-17 (Paul) [ip.2.5.10.6.1]
//	- update to ensure categoryId is always an integer
// 2019-05-15 (Paul) [ip.2.5.10.6.1]
//	- Using ckeditor config of moduleCategory to be the category ID first and then parent category, as parent.category is not reliable 
( function() {
	CKEDITOR.plugins.add( 'powervoice', {
		lang: 'en,zh',
	    icons: 'powervoice.gif',
		hidpi: false,
	    init: function( editor ) {
	    	
	    	editor.addCommand( 'insertpowervoice', {
	            exec: function( editor ) {
	            	var categoryId = editor.config.moduleCategory;
	            	window.open('/eclass40/src/tool/powervoice/index.php?isFileNew=1&editorName='+editor.name+'&categoryID='+categoryId+'&mode=t&toDIY=1', 'powervice',"menubar,toolbar,top=40,left=40,width=580,height=310");
	
	            }
	        });
	    	
	        editor.ui.addButton( 'powervoice', {
	            label: editor.lang.powervoice.powervoice,
	            command: 'insertpowervoice',
	            toolbar: 'insert'
	        });
	        
	    },
	    onLoad: function() {
	        CKEDITOR.addCss(
	            '.pv_image{' + 
	        	'   background-image: url('+this.path+'images/pv_logo.png);' + 
	        	'   background-position: center center;' + 
			    '   background-repeat: no-repeat;' + 
			    '   border: 1px solid #a9a9a9;' + 
			    '   width: 80px;' + 
			    '   height: 80px;' + 
			    '}'
	        );
	    }
	});
})();

function savePvInCkEditor(editorName,fileId,fileName){
	var oEditor = CKEDITOR.instances[editorName];
	var filePath = "/DIY_NOTES/powervoice/"+fileName;
	// Update UI
	var categoryId = oEditor.config.moduleCategory;
	var pvIndex = new Date().getTime();
	var x="";
	
	// Build outout UI
	x += '<div id="fck_pv_container_'+pvIndex+'" style="border:2px orange dotted;padding:10px;width:300px;float:left;">'; 
	x += '<div id="pv_name_'+pvIndex+'">'+fileName+'</div>';
	x += '<div type="fck_pv" id="tmp_pv_'+pvIndex+'" fck_filepath="'+filePath+'" file_id="'+fileId+'" fck_pvcat="'+categoryId+'"></div>';	
	x += '</div>';
	x += '<br style="clear:both;" /> ';
	var newElement = CKEDITOR.dom.element.createFromHtml( x, oEditor.document );
	oEditor.insertElement( newElement );
	

}
if(typeof file_exists != 'function'){
	function file_exists(editorName){
		var oEditor = CKEDITOR.instances[editorName];
		alert(oEditor.lang.powervoice.alertfilenameexist);
	}
}