/*	Created on : 20170608
*	This file is to provide map functions in fckeditor to the correpsonding functions in ckeditor.
*	
*	Using: Paul
*/

CKEDITOR.editor.prototype.InsertHtml = CKEDITOR.editor.prototype.insertHtml;

CKEDITOR.editor.prototype.SetHTML = CKEDITOR.editor.prototype.setData;

CKEDITOR.editor.prototype.GetHTML = CKEDITOR.editor.prototype.getData;

CKEDITOR.editor.prototype.GetXHTML = CKEDITOR.editor.prototype.getData;

CKEDITOR.editor.prototype.UpdateLinkedField = CKEDITOR.editor.prototype.updateElement;

CKEDITOR.editor.prototype.GetParentForm = function(){
	return this.element.$.form;
}
CKEDITOR.config.allowedContent = true; 
CKEDITOR.config.extraAllowedContent = 'img[src,alt,width,height]';

CKEDITOR.config.pasteFromWordPromptCleanup = true;

CKEDITOR.config.pluploadimageUploadDir = '/file/images/';

CKEDITOR.config.baseFloatZIndex = 100001;

function Trim(tempText){
	//var tempText = myText;

	// cut space in the beginning
	var firstChar = tempText.charAt(0);
	while (firstChar == " "){
		tempText = tempText.substr(1, tempText.length-1);
		firstChar = tempText.charAt(0);
	}

	// cut "\n" in the beginning
	var firstTwo = parseInt(tempText.charCodeAt(0));
	while (firstTwo == 13){
		tempText = tempText.substr(2, tempText.length-1);
		firstTwo = parseInt(tempText.charCodeAt(0, 2));
	}

	// cut space in the end
	var lastChar = tempText.charAt(tempText.length-1);
	while (lastChar == " "){
		tempText = tempText.substr(0, tempText.length-1);
		lastChar = tempText.charAt(tempText.length-1);
	}

	return tempText;
}
function GetSelectedText(oFCKeditor){
	if(oFCKeditor.EditorWindow.getSelection){
		// work in Chrome or FF
		var selection = oFCKeditor.EditorWindow.getSelection();
	} else {
		// work in IE
		var selection = oFCKeditor.EditorDocument.selection;	
	}
	
	if(selection.createRange != undefined){
		var range = selection.createRange();
		var html = range.htmlText;
	}else {
        var range = selection.getRangeAt(selection.rangeCount - 1).cloneRange();
        var clonedSelection = range.cloneContents();
        var div = document.createElement('div');
        div.appendChild(clonedSelection);
        var html = div.innerHTML;
    }
	var SelectedText=new Array(range,html);
	return SelectedText;
}

function AddSymbol(text,symbol){
		text = text + symbol;
	return text;
}
function CheckTag(text,tag){
	var ptag = /<p>/gi;
	var brtag = /<br>/i;
	if(tag == 'paragraph')	
		return ptag.test(text);
	else if(tag == 'linebreak')
	{
		var last4char = text.substr(text.length-4,text.length);		
		return last4char.match(brtag);	
	}	
}
function HighlightSelectedText(tempText,symbol,color){
	var text = Trim(tempText);
	var HasParagraph = CheckTag(text,'paragraph');
	var HasLineBreak = CheckTag(text,'linebreak');

	if(HasParagraph || HasLineBreak)
	{	
		var ptag = /<p>/gi;
		var pclosetag = /<\/p>/gi;
		var brtag = /<br>/i;
		var first3char = text.substr(0,3);	
		var last4char = text.substr(text.length-4,text.length);	
		text = text.substr(0,text.length-4); // remove </p> or <br>
		text = AddSymbol(text,symbol); //add symbol
		if(HasLineBreak)
		{
			text = "<font color='"+color+"'>"+text+"</font><br>";			
		}		
		if(HasParagraph)
		{
			if(last4char.match(pclosetag))
				text = text+"</font>";
			if(first3char.match(ptag))
				text = "<font color='"+color+"'>"+text.substr(3);
			if(CheckTag(text,'paragraph')) //any more paragraph
			{
				text = text.replace( ptag, "<p><font color='"+color+"'>");
				text = text.replace( pclosetag, "</font></p>");					
			}
			else
				text = "<p>"+text+"</p>";				
		}
		
	}
	else
	{
		text = "<font color='"+color+"'>"+AddSymbol(text,symbol)+"</font>";
	}
	return text;	
}