<?php
//If new plugin is installed, please update this line

//intranet List of Extra Plugins
$defaultExtraTool = "indentblock,dialogui,dialog,wordcount,notification,eqneditor,smiley,symbol,horizontalrule,table,panel,panelbutton,floatpanel,menu,contextmenu,tabletools,lineheight,widgetselection,filetools,notificationaggregator,lineutils,widget,uploadwidget,font,justify,panelbutton,colorbutton";

//eClass40 List of Extra Plugins
$defaulteClassExtraTool = "indentblock,dialogui,dialog,youtube,image,imageresize,image2,wordcount,notification,eqneditor,smiley,symbol,horizontalrule,table,panel,panelbutton,floatpanel,menu,contextmenu,tabletools,lineheight,widgetselection,filetools,notificationaggregator,lineutils,widget,uploadwidget,uploadimage,font,justify,panelbutton,colorbutton";

//Full list of Toolbar tools, please update if new plugins are added in 
$toolbarGroups['all'] ="{ name: 'document', items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
						{ name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
						{ name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
						{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
						'/',
						{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
						{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
						{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
						{ name: 'insert', items: [ 'Image', 'image2','pluploadimage','eclassimage','imageresize', 'Flash', 'Table', 'TableTools', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
						'/',
						{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
						{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
						{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
						{ name: 'about', items: [ 'About' ] },
						{ name: 'others', items: ['others', 'sba', 'powervoice','mp3upload','EqnEditor','Youtube','InsertFlippedChannels'] },
						'/',
						{ name: 'experiment', items:['ReplyTemplate']}";
$extraTool['all'] = 	array("sba",'powervoice','mp3upload','InsertFlippedChannels','ReplyTemplate','pluploadimage','eclassimage');	

//Default toolbar tools used
$toolbarGroups['default'] ="{ name: 'basicstyles', items: [ 'Undo','Redo', '-', 'Bold','Italic', 'Underline', 'Strike','Subscript','Superscript'] },
						{ name: 'lineheight', items:['lineheight'] },
						{ name: 'colorbutton', items:['TextColor', 'BGColor'] },
						{ name: 'fontStyle' , items: ['Font', 'FontSize']},
						{ name: 'justify', items:['JustifyLeft','JustifyCenter','JustifyRight','NumberedList','BulletedList','Outdent','Indent']},
						{ name: 'links', items:['Link', 'Unlink'] },
						{ name: 'clipboard',   items: [ 'Table', 'HorizontalRule', 'Symbol', 'PasteFromWord' ] },
						{ name: 'others', items: ['others'] }";
$extraTool['default'] = 	array();

//Toolbar tools used in SBA_LS						
$toolbarGroups['sba'] ="{ name: 'basicstyles', items: [ 'Undo','Redo', '-', 'Bold','Italic', 'Underline', 'Strike','Subscript','Superscript'] },
						{ name: 'lineheight', items:['lineheight'] },
						{ name: 'colorbutton', items:['TextColor', 'BGColor'] },
						{ name: 'fontStyle' , items: ['Font', 'FontSize']},
						{ name: 'justify', items:['JustifyLeft','JustifyCenter','JustifyRight','NumberedList','BulletedList','Outdent','Indent']},
						{ name: 'links', items:['Link', 'Unlink'] },
						{ name: 'clipboard',   items: [ 'HorizontalRule', 'Symbol', 'PasteFromWord' ] },
						{ name: 'others', items: ['others', 'sba'] }";
$extraTool['sba'] = 	array("sba");

//Toolbar tools used of Basic						
$toolbarGroups['Basic'] ="{ name: 'basicstyles', items: ['Undo','Redo','-','Bold','Italic','-','NumberedList','BulletedList','-','Link','Unlink','-','About'] }";
$extraTool['Basic'] = 	array("");

//Toolbar tools used of StudentWritingWithEquation						
$toolbarGroups['StudentWritingWithEquation'] ="{ name: 'Font', items: ['Font','FontSize','Styles'] },
						 { name: 'Justify', items: ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','Outdent','Indent','-','EqnEditor']}";
$extraTool['StudentWritingWithEquation'] = 	array("");

//Toolbar tools used of StudentWriting						
$toolbarGroups['StudentWriting'] ="{ name: 'Font', items: ['Font','FontSize','Styles'] },
						 { name: 'Justify', items: ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','Outdent','Indent']}";
$extraTool['StudentWriting'] = 	array("");

//Toolbar tools used of EssayMarkingTool_Test						
$toolbarGroups['EssayMarkingTool_Test'] ="{ name: 'Basic', items: ['Undo','Redo','-','Bold','Italic','Underline','Strike','-','Outdent','Indent', 'NumberedList','BulletedList', '-','Link','Unlink','-','About'], },
						 { name: 'Font', items: ['Font','FontSize'] },
						 { name: 'Color', items: ['Smiley','SpecialChar','TextColor','BGColor']},'/',
						 { name: 'Writing', items: ['NeedExplanationSymbol','AddCaptionSymbol','Missing','MarginalComment','RedComment','MarkCorrect','MarkPartRight','MarkWrong','Appreciate','InsertSticker' ,'MarkingScheme','MarkingCode','-','WordCount','TopicSentence','ErrorSummary' ]	}";
$extraTool['EssayMarkingTool_Test'] = 	array('NeedExplanationSymbol',"AddCaptionSymbol", 'Missing', "MarginalComment",'RedComment','MarkCorrect','MarkPartRight','MarkWrong','Appreciate',"InsertSticker",'MarkingScheme','MarkingCode','TopicSentence','ErrorSummary' );


//Toolbar tools used of EssayMarkingTool						
$toolbarGroups['EssayMarkingTool'] ="{ name: 'Basic', items: ['Undo','Redo','-','Bold','Italic','Underline','Strike','-','Outdent','Indent', 'NumberedList','BulletedList', '-','Link','Unlink','-','About'], },
						 { name: 'Font', items: ['Font','FontSize'] },
						 { name: 'Color', items: ['Smiley','SpecialChar','TextColor','BGColor']},'/',
						 { name: 'Writing', items: ['Missing','MarginalComment','RedComment','MarkCorrect','MarkPartRight','MarkWrong','Appreciate','InsertSticker' ,'MarkingScheme','MarkingCode','-','WordCount','TopicSentence','ErrorSummary' ]	}";
$extraTool['EssayMarkingTool'] = 	array('Missing', "MarginalComment",'RedComment','MarkCorrect','MarkPartRight','MarkWrong','Appreciate',"InsertSticker",'MarkingScheme','MarkingCode','TopicSentence','ErrorSummary' );

//Toolbar tools used of EssayMarkingTool_W2						
$toolbarGroups['EssayMarkingTool_W2'] ="{ name: 'Basic', items: ['Undo','Redo','-','Bold','Italic','Underline','Strike','-','Outdent','Indent', 'NumberedList','BulletedList', '-','Link','Unlink','-','About'] },
						 { name: 'Font', items: ['Font','FontSize'] },
						 { name: 'Color', items: ['Smiley','SpecialChar','TextColor','BGColor']},'/',
						 { name: 'Writing', items: ['Missing','RedComment','MarkCorrect','MarkPartRight','MarkWrong','Appreciate','InsertSticker' ]	}";
$extraTool['EssayMarkingTool_W2'] = 	array('Missing', "MarginalComment",'RedComment','MarkCorrect','MarkPartRight','MarkWrong','Appreciate',"InsertSticker");

//Toolbar tools used of W2_Writing						
$toolbarGroups['W2_Writing'] ="{ name: 'Basic', items: ['Undo','Redo','-','Bold','Italic','Underline','Strike','-', 'Subscript', 'Superscript','lineheight','TextColor','BGColor','Font','FontSize'] },
						 { name: 'Writing', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'NumberedList','BulletedList', 'Outdent','Indent','Link','Unlink','Table','HorizontalRule','SpecialChar','PasteFromWord']	}";
$extraTool['W2_Writing'] = 	array("");

//Toolbar tools used of W2_Writing_Correction						
$toolbarGroups['W2_Writing_Correction'] ="{ name: 'Basic', items: ['Undo','Redo','-','Bold','Italic','Underline','Strike','-', 'Subscript', 'Superscript','lineheight','TextColor','BGColor'] },
						 { name: 'Font', items: ['Font','FontSize','-','JustifyLeft', 'JustifyCenter', 'JustifyRight']	},
						 { name: 'Writing', items: ['NumberedList','BulletedList', 'Outdent','Indent','Link','Unlink','Table','HorizontalRule','SpecialChar','PasteFromWord']	}";
$extraTool['W2_Writing_Correction'] = 	array("");


//Toolbar tools used of ReplyTemplate						
$toolbarGroups['ReplyTemplate'] ="{ name: 'basicstyles', items: ['Undo','Redo','-','Bold','Italic','-','NumberedList','BulletedList','-','Link','Unlink','PasteFromWord','-','About']}, '/',
									{ name: 'experiment', items:['ReplyTemplate']}";
$extraTool['ReplyTemplate'] = 	array("ReplyTemplate");	

//Toolbar tools used of Learning_Portfolio						
$toolbarGroups['Learning_Portfolio'] ="{ name: 'Basic', items: ['Undo','Redo','Bold','Italic','Underline','Strike', 'Subscript', 'Superscript','TextColor','BGColor','Font','FontSize','-','JustifyLeft', 'JustifyCenter', 'JustifyRight','NumberedList','BulletedList', 'Outdent','Indent'] },
									 '/',
						 			{ name: 'Writing', items: ['Link','Unlink','Anchor','HorizontalRule','pluploadimage','eclassimage','imageresize','Table','SpecialChar','PasteFromWord','PasteText','-','powervoice','mp3upload','Youtube']	}";
$extraTool['Learning_Portfolio'] = 	array('powervoice','mp3upload','pluploadimage','eclassimage');				

//Toolbar tools used of Learning_Portfolio on STEM site
$toolbarGroups['Learning_Portfolio_STEM'] ="{ name: 'Basic', items: ['Undo','Redo','Bold','Italic','Underline','Strike', 'Subscript', 'Superscript','TextColor','BGColor','Font','FontSize','-','JustifyLeft', 'JustifyCenter', 'JustifyRight','NumberedList','BulletedList', 'Outdent','Indent'] },
									 '/',
						 			{ name: 'Writing', items: ['Link','Unlink','Anchor','HorizontalRule','pluploadimage','eclassimage','imageresize','Table','SpecialChar','PasteFromWord','-','Youtube']	}";
$extraTool['Learning_Portfolio_STEM'] = 	array('mp3upload','pluploadimage','eclassimage');

//Toolbar tools used of eContent note section type Text
$toolbarGroups['eContent_text'] ="{ name: 'Basic', items: ['Undo','Redo','Bold','Italic','Underline','Strike', 'Subscript', 'Superscript','TextColor','BGColor','Font','FontSize','-','JustifyLeft', 'JustifyCenter', 'JustifyRight','NumberedList','BulletedList', 'Outdent','Indent'] },
									 '/',
						 			{ name: 'Writing', items: ['Link','Unlink','Anchor','HorizontalRule','pluploadimage','eclassimage','imageresize','Table','SpecialChar','PasteFromWord','-','powervoice','mp3upload','EqnEditor','Youtube', 'InsertFlippedChannels']	}";
$extraTool['eContent_text'] = 	array('powervoice','mp3upload','InsertFlippedChannels','pluploadimage','eclassimage');

//Toolbar tools used of eContent note section type Text
$toolbarGroups['eclass_general'] ="{ name: 'Basic', items: ['Undo','Redo','Bold','Italic','Underline','Strike', 'Subscript', 'Superscript','TextColor','BGColor','Font','FontSize','-','JustifyLeft', 'JustifyCenter', 'JustifyRight','NumberedList','BulletedList', 'Outdent','Indent'] },
									 '/',
						 			{ name: 'Writing', items: ['Link','Unlink','Anchor','HorizontalRule','pluploadimage','eclassimage','imageresize','Table','SpecialChar','PasteFromWord','-','powervoice','mp3upload','EqnEditor','Youtube', 'InsertFlippedChannels']	}";
$extraTool['eclass_general'] = 	array('powervoice','mp3upload','InsertFlippedChannels','pluploadimage','eclassimage');

//Toolbar tools used of AnswerSheet Rich Text
$toolbarGroups['AnswerSheet'] ="{ name: 'Basic', items: ['Undo','Redo','Bold','Italic','Underline','Strike', 'Subscript', 'Superscript','TextColor','BGColor','Font','FontSize','JustifyLeft', 'JustifyCenter', 'JustifyRight','-','NumberedList','BulletedList', 'Outdent','Indent', 'HorizontalRule','Link','Unlink','pluploadimage','eclassimage','imageresize','Table','PasteFromWord','-','EqnEditor','Youtube'] }";
$extraTool['AnswerSheet'] = 	array('pluploadimage','eclassimage');

//Toolbar tools used of eClass Files Rich Text
$toolbarGroups['eclass_files'] ="{ name: 'Basic', items: ['Undo','Redo','Bold','Italic','Underline','Strike', 'Subscript', 'Superscript','TextColor','BGColor','Font','FontSize','JustifyLeft', 'JustifyCenter', 'JustifyRight','-','NumberedList','BulletedList', 'Outdent','Indent', 'HorizontalRule','Link','Unlink','pluploadimage','eclassimage','imageresize','Table','SpecialChar','PasteFromWord','-','powervoice','mp3upload','EqnEditor','Youtube'] }";
$extraTool['eclass_files'] = 	array('powervoice','mp3upload','pluploadimage','eclassimage');	

//Toolbar tools used for NCS Porject Forum only
$toolbarGroups['eclass_ncs_forum'] ="{ name: 'Basic', items: ['Undo','Redo','Bold','Italic','Underline','Strike', 'Subscript', 'Superscript','TextColor','BGColor','Font','FontSize','JustifyLeft', 'JustifyCenter', 'JustifyRight','-','NumberedList','BulletedList', 'Outdent','Indent', 'HorizontalRule','Link','Unlink','pluploadimage','imageresize','Table','SpecialChar','PasteFromWord','-','Youtube'] }";
$extraTool['eclass_ncs_forum'] = 	array('pluploadimage');

//Toolbar tools used of eContent note section type Text
$toolbarGroups['eclass_questionBank_itextbook'] ="{ name: 'Basic', items: ['Source','Undo','Redo','Bold','Italic','Underline','Strike', 'Subscript', 'Superscript','TextColor','BGColor','Font','FontSize','-','JustifyLeft', 'JustifyCenter', 'JustifyRight','NumberedList','BulletedList', 'Outdent','Indent'] },
									 '/',
						 			{ name: 'Writing', items: ['Link','Unlink','Anchor','HorizontalRule','pluploadimage','eclassimage','imageresize','Table','SpecialChar','PasteFromWord','-','powervoice','mp3upload','EqnEditor','Youtube']	}";
$extraTool['eclass_questionBank_itextbook'] = 	array('powervoice','mp3upload','pluploadimage','eclassimage');

//Toolbar tools used of iMail App webview
$toolbarGroups['imail_app_webview'] = "
    {name: 'basicstyles', items: ['Undo','Redo', '-', 'Bold','Italic', 'Underline', 'Strike']},
    {name: 'justify', items: ['JustifyLeft','JustifyCenter','JustifyRight']},
    {name: 'clipboard', items: [ 'HorizontalRule']},
    {name: 'others', items: ['others']}
";
?>