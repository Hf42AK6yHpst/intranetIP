<?php
// Editing by 
/*
 * 2019-12-27 (Philips): Use Issus Date
 * 2015-01-21 (Carlos): Added disclaimer remark for KIS
 */
include_once("$intranet_root/includes/global.php");
$Signature = $_REQUEST['Signature'];
$SignatureText = $_REQUEST['SignatureText'];
$date_issue = $date_issue ? $date_issue : date('Y-m-d');
$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];
?>
<br><br><br><br>
<table width=100% border=0 cellpadding=1 cellspacing=0>
<?
	if ($Signature == 1) {
?>
		<tr>
		<td align=left></td>
			<td align=right><?=$SignatureText?>: <U><? for ($i=0; $i<35; $i++) { ?>&nbsp;<? } ?></U><br><br></td>
		</tr>
<?
	}
?>
<tr>
<td width="50%" <?=($sys_custom['ePayment']['ReceiptByCategoryDesc'] /* && stristr($_SERVER['REQUEST_URI'],'/payment/settings/payment_item') !== false */)?'style="visibility:hidden;"':''?>><br><br>
<U><? for ($i=0; $i<46; $i++) { ?>&nbsp;<? } ?></U><br>
<? for ($i=0; $i<20; $i++) { ?>&nbsp;<? } ?><?=$i_Payment_Receipt_SchoolChop?>
</td>
<td align=right>
<?=$i_Payment_Receipt_Date?>: <U><? for ($i=0; $i<8; $i++) { ?>&nbsp;<? } ?><?=$date_issue?><? for ($i=0; $i<9; $i++) { ?>&nbsp;<? } ?></U>
</td>
</tr>
</table>
<br><br>
<div align=left><?=$i_Payment_Receipt_ThisIsComputerReceipt?></div>
<br><br>
<?php if(!$isKIS){ ?>
<b><?=$i_Payment_Receipt_Remark?>: <?=$i_Payment_Receipt_UpTo?> <?=date('Y-m-d')?> <?=$i_Payment_Receipt_AccountBalance?>: <?=$lpayment->getWebDisplayAmountFormat($curr_balance)?></b>
<?php } ?>
<?php 
if($_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['PaymentMethod']){
	echo '<div><b>'.$Lang['ePayment']['RemarkReceiptDisclaimer'].'</b></div>';
}
?>
