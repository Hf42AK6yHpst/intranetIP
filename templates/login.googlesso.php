<?php
//error_reporting(E_ALL);
//ini_set('error_reporting', E_ALL);
############################################################

include_once("../includes/global.php");
require_once($ssoservice["Google"]["SimpleSAML_Root"].'/lib/_autoload.php');


## Get Data
$srv 	= (isset($_GET['srv']) && $_GET['srv'] != '') ? $_GET['srv'] : '';
$gmail 	= (isset($_GET['gmail']) && $_GET['gmail'] != '') ? $_GET['gmail'] : '';
//debug_r($_SESSION);die();


## Init
$extraParam = '';


## Main

if($gmail == ''){
	$gmail = $_SESSION['SignInWithGoogle_UserEmail'];
}

if($ssoservice["Google"]['Enable_SubDomain']['blers'] == true){
	$extraParam .= '&gmail='.$gmail;
}


$SP_name = Get_GoogleSSO_Service_Provider_Name($gmail);
//debug_r($SP_name);die();

### double checking on SP ##################### 
if($SP_name == ''){
	die('You are not allowed to access.');
}
###############################################

$as = new SimpleSAML_Auth_Simple($SP_name);
$as->requireAuth();
//$as->requireAuth(array(
//    'ReturnTo' => 'https://saml-sp-dev.broadlearning.com/templates/login.google-idp.php',
//    'KeepPost' => FALSE,
//));

# permission checking 
if(!$as->isAuthenticated()){
	die('You are not allowed to access.');
} else {
	header("Location: ../sso_login.php?srv=".$srv.$extraParam);
	die();
}

/*
echo '<pre>'; var_dump($_REQUEST); echo '</pre>';
echo '<hr>';

# get user attributes
$attributes = $as->getAttributes();
print_r($attributes);
echo '<hr>';

$authDataArr = $as->getAuthDataArray();

echo '<pre>'; var_dump($authDataArr); echo '</pre>'; 

# expiry date
$expiry_date = date("Y-m-d H:i:s", $authDataArr['Expire']);
echo '<pre>'; var_dump($expiry_date); echo '</pre>';
*/

?>