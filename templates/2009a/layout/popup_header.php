<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv='pragma' content='no-cache' charset=utf-8" />
<?= returnHtmlMETA() ?>

<title><?=$Lang['Header']['HomeTitle']?></title>
<? if(!$home_header_no_EmulateIE7){?>
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<? }?>

<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/content_30.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax_connection.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>lang/script.<?= $intranet_session_language?>.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/2007script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<!--

<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/2007a/js/SpryValidationTextField.js"></script>
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/content.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/topbar.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/leftmenu.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/footer.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/barchar.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/misc.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/iportfolio/iportfolio.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/iportfolio/iportfolio_icon_css.php" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/iportfolio/content_css.php" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/ereportcard.css" rel="stylesheet" type="text/css" />

//-->
<link href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" rel="stylesheet" type="text/css" />
</head>
<style type='text/css' media='print'>
 .print_hide {display:none;}
</style>
<body background="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/bg.gif" style="background-repeat: no-repeat" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">