/**** 
	Copy & Paste Function 
	Modified by Jason Lam
	Workable in IE7 & FF2 or above
	
	Remark:
	- element id input text box is mark[x][y]
	
	Use?
	1. set the xno & yno for the table size
	2. add a textarea with hidden style in the page calling this function
		i.e. <textarea id="text1" name="text1" style="display:none" cols="10" rows="4"></textarea>
****/



/* Start of Copy & Paste function */
var rowx = 0, coly = 0;			//init	- rowx : 
var xno = 2; yno = 4;		// set table size 
var jsDefaultPasteMethod = "selectedIndex";		// "selectedIndex" or "value" or "text"

var startContent = "";
var setStart = 1;

function setxy(jParX, jParY){
	rowx = jParX;
	coly = jParY;
	//alert(rowx + ' = ' + coly);
	//document.getElementById('mark['+jParX+']['+jParY+']').blur();
	
	var jsInputID = 'mark\\[' + jParX + '\\]\\[' + jParY + '\\]';
	$('#' + jsInputID).blur();
}

/* Handling the key press Ctrl+V for Mozilla */
var pasteI, pasteJ;
function isPasteContent(e, i, j){
	var code = (document.all) ? e.keyCode : e.which;
	//var ctrl = (document.all) ? e.ctrlKey : Event.CONTROL_MASK;
	var ctrl = e.ctrlKey;
	
	// Mozilla: V-e.DOM_VK_V
	var isPaste = false;
	if (ctrl && (code==86 || code == 118)){ //CTRL+V
		isPaste = true;
		setxy(i, j);
		paste(j);
	}
	else if (e.type.toUpperCase() == 'PASTE') {
		isPaste = true;
		pasteI = i;
		pasteJ = j;
		setTimeout(executePaste, 1) // wait original paste action executed and then perform our own paste action
	}
	
	return false;
}

function executePaste() {
	setxy(pasteI, pasteJ);
	paste(pasteJ);
}

/* End of Handling the key press Ctrl+V for Mozilla */
	
/* Handling the key press Ctrl+V for IE */
function pasteContent(jPari, jParj){	
	setxy(jPari, jParj);
	
	var temp1;
	if(window.netscape){	// Mozilla.Fixfox 
		try {  
			netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");  
		} catch (e) { 
		 	alert(JSLang['WarningFirefoxNotAllowPaste']); 
		 	return false;   
		}  
	/*
		// create a clipboard object that refers to the system clipboard
		var transobj = Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable); 
		if (!transobj) return;      
		var transid = Components.interfaces.nsITransferable;  
		var trans = Components.classes["@mozilla.org/widget/transferable;1"].getService(transid);         
		
		var clipobj = Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard); 
		if (!clipobj)  return;  
		var clipid = Components.interfaces.nsIClipboard;
		var clip = Components.classes['@mozilla.org/widget/clipboard;1'].getService(clipid);
		
		clip.setData(trans, null, clipid.kGlobalClipboard);	// copy data to the system clipboard of the browser
	*/
		paste();	// copy content from system clipboard to a text area
	} else if(document.all){	// Internet Explorer
		temp1 = document.getElementById('text1').createTextRange();
		temp1.execCommand('Paste');
	}
	
	paste_marksheet(jParj);
}
/* Handling the key press Ctrl+V for IE */
	
/* Copy Marksheet Column Data */
function getCopyContent(col_index){
	//var rowNo = xno;	// number of columns
	var rowNo = col_index;
	var colNo = yno;	// number of students
	var tmp= '';
	var jsThisInputID;
	if(rowNo != '' && colNo > 0){
		for(var j=0 ; j<colNo ; j++){
			
			//tmp += document.getElementById('mark['+j+']['+rowNo+']').value;
			jsThisInputID = 'mark\\[' + j + '\\]\\[' + rowNo + '\\]';
			tmp += $('#' + jsThisInputID).val();
			tmp += (j != colNo-1) ? '\n' : '';
		}
	}
	return tmp;
}

/* Copy Data into clipboard */
function copy(col_index, isclear) {
	var null_txt = isclear || 0;
	var txt = (null_txt == 1) ? "" : getCopyContent(col_index);
	
	if(window.clipboardData) {						// IE
		window.clipboardData.clearData();  
		window.clipboardData.setData("Text", txt); 
	} else if (window.netscape) {					// FF
		try {  
			netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");  
		} catch (e) { 
		 	alert(JSLang['WarningFirefoxNotAllowPaste']);   
			return;
		}  
		
		var clip = Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard); 
		if (!clip)  return;  
		var trans = Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable); 
		if (!trans) return;  
		trans.addDataFlavor('text/unicode');  
		
		var str = new Object();  
		var len = new Object();  
		var str = Components.classes["@mozilla.org/supports-string;1"].createInstance(Components.interfaces.nsISupportsString); 
		
		var copytext = txt;  
		str.data = copytext;  
		trans.setTransferData("text/unicode",str,copytext.length*2); 
		
		var clipid = Components.interfaces.nsIClipboard;  
		if (!clip) return;  
		clip.setData(trans,null,clipid.kGlobalClipboard);  
		
	}  
	return ;  
}

// This function is used to paste the content to temporarily container for later use
function paste(col_index){
	// FF or Chrome
	if (window.netscape || navigator.userAgent.indexOf("Safari") !== -1) {
		try {  
			netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");  
		} catch (e) { 
		 	alert(JSLang['WarningFirefoxNotAllowPaste']);  
			return;
		}  
		
		var clip = Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard); 
		if (!clip)  return;  
		var trans = Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable); 
		if (!trans) return;  
		trans.addDataFlavor('text/unicode');  
		
		
		clip.getData(trans,clip.kGlobalClipboard);
		var str = new Object();
		var strLength = new Object();
		trans.getTransferData("text/unicode",str,strLength);
		if(str){
			str = str.value.QueryInterface(Components.interfaces.nsISupportsString);
		}
		if(str){
			pastetxt = str.data.substring(0, strLength.value/2);
			//alert(pastetxt);
		}
		document.getElementById('text1').value = pastetxt;
	} else {
		document.getElementById('text1').value = window.clipboardData.getData('Text');
	}
	
	paste_marksheet(col_index);
}

function paste_marksheet(col_index){
	var temp1 = null;
	var text1 = null;
	var s = "";

	// you must use a 'text' field
	// (a 'hidden' text field will not show anything)
	s = document.getElementById('text1').value; // clipboard is pasted to a string here
	setStart = 0;
	
	var row_array = s.split("\n");
	var row_num = 0;
	
	while (row_num < row_array.length)
	{
		var col_array = row_array[row_num].split("\t");
		var col_num = 0;
		var xPos=0, yPos=0;
		
		var jsThisInputID;
		var jsThisInputType;
		
		while (col_num < col_array.length)
		{
			if ((col_num == 0) && (row_num == 0)) {
				startContent = col_array[0];
			}
			col_array[col_num] = Trim(col_array[col_num]);
			
			if ((row_array[row_num] != "")&&(col_array[col_num] != ""))
			{
				//var temp = document.getElementById('mark['+ (row_num + rowx) +']['+ (col_num + coly) +']');
				xPos = parseInt(row_num) + parseInt(rowx);
				yPos = parseInt(col_num) + parseInt(coly);
				
				jsThisInputID = 'mark\\[' + xPos + '\\]\\[' + yPos + '\\]';
				jsThisInputType = $('#' + jsThisInputID).attr('type');
				if (jsThisInputType == "select-one") {	// selection
					if (jsDefaultPasteMethod == 'selectedIndex') {
						$('#' + jsThisInputID).attr('selectedIndex', col_array[col_num]);
					}
					else if (jsDefaultPasteMethod == 'value') {
						$('#' + jsThisInputID).val(col_array[col_num]);
					}
					else if (jsDefaultPasteMethod == 'text') {
						var jsNotMatchedNum = 0;
						$('#' + jsThisInputID + ' option').each( function() {
							if ($(this).text() == col_array[col_num]) {
								$(this).attr('selected', 'selected');
							}
							else {
								$(this).attr('selected', '');
								jsNotMatchedNum++;
							}
						});
						
						// mainly for IE
						if (jsNotMatchedNum == parseInt($('#' + jsThisInputID + ' option').length)) {
							$('#' + jsThisInputID).val('');
						}
					}
				}
				else if (jsThisInputType == "text") {	// textbox
					$('#' + jsThisInputID).val(col_array[col_num]);
				}
				
//				if (document.getElementById('mark['+ xPos +']['+ yPos +']') != null)
//				{
//					document.getElementById('mark['+ xPos +']['+ yPos +']').selectedIndex = col_array[col_num];
//				}
			}
			col_num+=1;
		}
		row_num+=1;
	}
	rowx = 0;
	
	followup_after_copy_paste();
}

function followup_after_copy_paste(){
	/*
		copy this function to your own js page &
		add the your own customized code here
	*/
}
/**** End of Copy & Paste Function ****/
