/*
	Excel Pasting Function library for IP Created By Evan Hui
	
	when document ready, call initialize()
	whenever ajax operation finished, recall initialize()

	initialize(){
		if (browser = IE){
			if(using Ctrl+V){
				Paste data to invisible textarea; 
			}else if(using right click pasting){
				Data retrieve from Clipboard;
			} 
			Call paste();
		}else{
			if(input field clicked){
				Visible textarea covering the original input field;
			}else if(dropdown list clicked){
				Paste data to invisible textarea; 
			} 
			Call paste();
		}
		
		excel(e, orifield){handle shift, enter and tab key actions};
		createBox(field, type){ Create a textarea };
		getCoor(field){ Get the coordination of the field };
		paste(orifield, source){ Paste data to the fields according to their position };
		checkIndex(field, data){ Check if the page is in selectedIndex mode }
		
		checkIndex - Select drop down options by index
		(e.g. Options: [A] [B] [C] [D] [E] [N.A.] > Input "3" > Option [D] selected
	}

*/

$(function(){

	initializeCopyPaste();

	$(document).ajaxStop(function(){
        initializeCopyPaste();
    });

	function initializeCopyPaste(){	
		
		var ua = window.navigator.userAgent;
		var msie = ua.indexOf("MSIE ");
		var ctrl = false, ctrlKey = 17, sft = false, sftKey = 16, vKey = 86, aKey = 65, entKey = 13, tabKey = 9;
		var visibleTa = "#visibleTA", invisibleTa = "#invisibleTA", allTa = "#visibleTA, #invisibleTA";
		var input = "input[id^='mark']", select = "select[id^='mark']", allField = "input[id^='mark'], select[id^='mark']";
		
		//Delete all old function inputn the page	
		$(allField).removeAttr('onpaste').removeAttr('onkeyup').removeAttr('onkeydown').removeAttr('onkeypress').removeAttr('onchange').removeAttr('onfocusout');

		if (msie > 0){ //If Current browser is IE
			$(allField).bind('focus', function(){

	      		removeBox();	//Remove any old textarea
	    		orifield = $(this); //Record the original field
				createBox(orifield, "invisible"); //An invisible textearea is created behind the dropdown list

			    $(invisibleTa).keyup(function(e){
			        if (e.keyCode == ctrlKey){	
			        	setTimeout(function(){	//The original field will be focused when Ctrl is released
				        	ctrl = false;
				          	orifield.focus();
				          	$(invisibleTa).val('');
				        }, 10);
			        }
			    }).keydown(function (e){  //If Ctrl+V are pressed, execute paste() function
			      	box = $(this);
			      	field = orifield;
			        if (ctrl === true && e.keyCode == vKey){
				        setTimeout(function(){
			            source = box.val();
				        paste(field, source);
				        }, 0);
			      	}else if(e.keyCode == aKey){
			        	orifield.select();
			    	} 
			    });
			});

			$(allField).keydown(function(e){ //The textarea will be focused when Ctrl is being pressed
		       	orifield = $(this);

		        if (e.keyCode == ctrlKey){
		          $(invisibleTa).val(orifield.val());	//Data from the orginal field will be copied to the textarea
		          $(invisibleTa).select();				//The textarea will be focused when Ctrl is being pressed
		          ctrl = true;
		      	}
		    }).keydown(function(e){	//Detect shift, enter and tab key actions
		    	excel(e, orifield);
		    }).keyup(function(e){
	    		if (e.keyCode == sftKey){
	    		sft = false;
	    		}
	    	});

			$(input).bind('paste', function (e){  //Detect pasting function, mainly from right click menu action
			    field = $(this);
			    field.val('');
			    setTimeout(function(){
			        source = window.clipboardData.getData('Text');
			        paste(field, source);
			    }, 0);
			});

		}else{
			$(input).bind('focus', function(e){ //When input field with type text are being focused
				e.preventDefault();
			    removeBox();
				orifield = $(this);
				getCoor(orifield);
				createBox(orifield, "visible"); // A visible textarea#visibleTA will be generated to cover the original input field

				$( window ).resize(function() { //The textarea will be regenerated whenever resize action is occured
					if ($(visibleTa).length > 0){
						$(visibleTa).attr("id","oldclip");
		  				createBox(orifield, "visible");
						$(visibleTa).val($('#oldclip').val());
						$('#oldclip').remove();
						$(visibleTa).select();
					}
				});

				$(visibleTa).val(orifield.val()).select().bind('focusout', function(){ //The visible textarea will be removed when focusout
					orifield.val($(visibleTa).val());
					removeBox();
				}).keydown(function(e){
			    	excel(e, orifield);
			    }).keyup(function(e){
		    		if (e.keyCode == sftKey){
		    		sft = false;
					}
		    	}).bind('paste',function (e){ //Detect pasting action, trigger paste()
					box = $(this);
					field = orifield;
				    setTimeout(function(){
					    source = box.val();
				        paste(field, source);
						box.val(orifield.val());
	    				box.trigger('focusout');
				    }, 0);
				});
			});

			$(select).bind('focus', function(){ //When dropdown list are being focused
				removeBox();
	    		orifield = $(this);
	    		createBox(orifield, "invisible"); //Invisible textarea created

			    $(select).keydown(function(e){ //When ctrl is pressed, focus to the invisible textarea#sclip
			        if (e.keyCode == ctrlKey){
			        	e.preventDefault();
				        $(invisibleTa).val(orifield.val());
				        $(invisibleTa).select();
				        ctrl = true;
			        }
			    });
				  
			    $(invisibleTa).keyup(function(e){ //When ctrl key is released, focus back to the original field
			        if (e.keyCode == ctrlKey){
			        	setTimeout(function(){
			        		ctrl = false;
			        		orifield.focus();
							$(invisibleTa).val('');
				        }, 100);
			        }
			    }).keydown(function (e){ //When ctrl+V, trigger paste()
			      	box = $(this);
			      	field = orifield;
			        if (ctrl === true && e.keyCode == vKey){
				        setTimeout(function(){
				            source = box.val();
					        paste(field, source);
				        }, 0);
			      	}
			    });
			});
			
			$(select).keydown(function(e){ //Detect shift, enter and tab key actions
		    	excel(e, $(this));
		    }).keyup(function(e){
	    		if (e.keyCode == sftKey){
	    		sft = false;
	    		}
	    	});
		}

		function excel(e, orifield){  //Function to handle Enter and tab key actions
			if (e.keyCode == sftKey){
				sft = true;
			}else if (e.keyCode == entKey){ 
	        	e.preventDefault();
	        	e.stopPropagation();
	        	/*Crashed with the excel plugin in the system
	        	the action will be doubled in select field and for all fields in IE*/
	        	if (orifield.attr("tagName") != 'SELECT' && msie < 0){ //Should be deleted if original plugin is removed
			  	 	orifield.val($(visibleTa).val());
			  	 	 if (sft == true){//Jump to previous row when shift+enter key is pressed
			  		 	xaxis = parseInt(getCoor(orifield)[0])-1;
		  	 		 	nextfield = $("#mark\\["+xaxis+"\\]\\["+getCoor(orifield)[1]+"\\]" );
		  	 		}else{			//Jump to next row when enter key is pressed
		  				xaxis = parseInt(getCoor(orifield)[0])+1;
		  			  	nextfield = $("#mark\\["+xaxis+"\\]\\["+getCoor(orifield)[1]+"\\]" );
		  			}		  	 		
					nextfield.focus();
				}
		  	}else if(e.keyCode == tabKey){ 
		  		e.preventDefault();
		  		orifield.val($(visibleTa).val());
		  		if (sft == true){//Jump to previous field when shift+tab key is pressed
		  	 		nextfield = orifield.closest("td").prev().find(allField);
	  	 		}else{			//Juml to next field when tab is pressed
	  	 			nextfield = orifield.closest("td").next().find(allField);
	  	 		}		  	 		
				nextfield.focus();
		  	}
		}

		function createBox(field, type){ //Function creating textarea
			if (type == "visible"){ //Create visible textarea#visibleTA for input field except in IE
				id = "visibleTA"; width = field.width()-2; height = field.height(); z = 1;
			}else if(type == "invisible"){ //Create invisible textarea#invisibleTA for dropdown list and all field in IE
				id = "invisibleTA"; width = 0; height = 0; z = -1;
			}
			field.closest('body').append("<textarea id=\""+id+"\" style=\"top:"+orifield.position().top+"px; left:"+orifield.position().left+"px;resize: none; width: "+width+"px; height: "+height+"px;opacity: 1;overflow:hidden;outline:0px;position: absolute;z-index: "+z+";\"></textarea>");
		}

		function removeBox(){ //Function to remove the textarea
			$(allTa).remove();
		}

		function getCoor(field){ //Get the coordination of field from its ID
			return field.attr('id').substr(4).replace(/\[/g,"").replace("]",",").replace("]","").split(",");
		}

		function paste (orifield, source){ //Paste function
			field = orifield;
			detabs = $.trim(source).split(/\t/);  //seperate the data with tabs
			coor = getCoor(orifield);
			if (msie > 0){
				crit = /(\r\n|\n|\r)/;
			}else{
				crit = /\n/;
			}

			for(i=0; i < detabs.length; i++){
	            if (detabs[i].search( /(\r\n|\n|\r)/ ) >= 0){	//Check if there are any line breakers in the data
	                var TempArr = detabs[i].split(crit);

	                for(k=0; k < TempArr.length-1 ; k++){  	//For the records except the last one
	                  	checkIndex(field, TempArr[k]);		//////Check if in selectedIndex mode
	                  	coor[0] = parseInt(coor[0])+1;		
	                  	field = $("#mark\\["+coor[0]+"\\]\\["+coor[1]+"\\]" );	//when pasted to the field, Jump to next row
	              	}

	            	checkIndex(field, TempArr[TempArr.length-1]); //For the last record with line breaker
					field = field.closest("td").next().find(allField); //Jump to next column
	            }else{

	              	checkIndex(field, detabs[i]); //For records without line breaker
	              	field = field.closest("td").next().find(allField); //Jump to next column
	            }
	        }
		}

		function checkIndex(field, data){ //Handle selectedIndex mode
	    	if (field.attr("tagName") == 'SELECT' && jsDefaultPasteMethod == "selectedIndex"){
	    		//If the field selected now is dropdown list and the mode is "selectedIndex"
	    		field.val(field.find("option").eq(data).val());
	    	}else{
	    		//Other cases
	    		field.val(data);
	    	}
		}
	}
});