﻿// using:
/*
 * Modification Log: 
 * 2013-04-19 (Jason)
 *		- add init_tag_label(), .ready() for improving the display of tag bubbles 
 */ 
function loadThickbox(task, filePath, headerHtml, extraParam, onLoadFunc, targetFormId) {
	filePath = filePath || '';
	headerHtml = headerHtml || '';
	extraParam = extraParam || '';
	onLoadFunc = onLoadFunc || '';
	targetFormId = targetFormId || 'handinForm';

	$('#mod').val("ajax");
	$('#task').val(task); 
	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#" + targetFormId).serialize() + '&r_filePath=' + filePath + '&r_headerHtml=' + headerHtml + '&r_extraParam=' + extraParam,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
					$('div#TB_iFrameDiv').html(xml);
					$('.textbox').focus();
					
					if (onLoadFunc != '') {
						eval(onLoadFunc);
					}
				  }
	});
}

function loadContentByThickbox(filePath, headerHtml, extraParam) {
	loadThickbox('getThickboxHtml_content', filePath, headerHtml, extraParam);
}

function loadVocabByThickbox(filePath, headerHtml, extraParam) {
	loadThickbox('getThickboxHtml_vocab', filePath, headerHtml, extraParam);
}

function loadEditRefCategoryByThickbox(headerHtml, extraParam) {
	loadThickbox('getThickboxHtml_editRefGroup', '', headerHtml, extraParam, 'initEditRefCategoryThickbox();');
}

function initEditRefCategoryThickbox() {
	$("input#r_refCategoryTitleTb").focus();
	$("input").keypress(function(event) { return event.keyCode != 13; });	// avoid press "enter" and then go to the index page
}

function dynSizeThickboxHeightAdjustment() {
	$('#TB_iFrameDiv').css('height', ip_dyn_tb_h);
	$('#thickbox_content_outer_div').css('height', ip_dyn_tb_h);
	$('#thickbox_content_div').css('height', $('#thickbox_content_outer_div').height() - $('#TB_title').height() - $('h1').height() - $('#system_message_box').height() - $('#edit_content_pop').height());
}

function saveOtherInfo(successMsg, failedMsg) {
	$('#mod').val("ajax");
	$('#task').val("saveOtherInfo");

	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#handinForm").serialize() + '&' + $("#thickboxForm").serialize(),
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
					  var returnMsg = (xml=='1')? successMsg : failedMsg;
					  Get_Return_Message(returnMsg);
				  }
	});
}

function saveComment(successMsg, failedMsg) {
	$('#mod').val("ajax");
	$('#task').val("saveStepStudentComment");
	
	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#handinForm").serialize(),
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
					  var returnedCommentId = parseInt(xml);
					  var returnMsg;
					  
					  if (returnedCommentId > 0) {
						  returnMsg = successMsg;
						  
						  $('#r_commentId').val(returnedCommentId);
						  reloadCommentLayer();
					  }
					  else {
						  returnMsg = failedMsg;
					  }
					  
					  Get_Return_Message(returnMsg);
				  }
	});
}

function reloadCommentLayer() {
	$('#mod').val("ajax");
	$('#task').val("reloadStepStudentCommentLayer");
	
	$('div#layer_comment01_content').html('');
	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#handinForm").serialize(),
		success:  function(xml){
					  $('div#layer_comment01_content').html(xml);
				  }
	});
}

function savePeerComment(successMsg, failedMsg) {
	$('#mod').val("ajax");
	$('#task').val("savePeerMarkingData");
	
	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#handinForm").serialize(),
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
					  var returnedRecordId = parseInt(xml);
					  if (returnedRecordId > 0) {
						  returnMsg = successMsg;
						  reloadPeerCommentLayer(returnedRecordId);
					  }
					  else {
						  returnMsg = failedMsg;
					  }
					  
					  Get_Return_Message(returnMsg);
				  }
	});
}

function reloadPeerCommentLayer(returnedRecordId) {
	$('#mod').val("ajax");
	$('#task').val("reloadPeerCommentLayer");
	
	$('div#layer_peer_comment01_content').html(getAjaxLoadingMsg());
	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#handinForm").serialize() + '&r_peerMarkingDataRecordId=' + returnedRecordId,
		success:  function(xml){
					  $('div#layer_peer_comment01_content').html(xml);
				  }
	});
}

function reloadCommentLayerInThickbox() {
	$('#mod').val("ajax");
	$('#task').val("reloadStepStudentCommentLayer");
	
	$('div#commentDivInThickbox').html('');
	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#handinForm").serialize() + '&r_forThickbox=1',
		success:  function(xml){
					  $('div#commentDivInThickbox').html(xml);
				  }
	});
}

function triggerCommentLayerInThickbox() {
	if ($('div#commentDivInThickbox').is(':visible')) {
		$('div#commentDivInThickbox').hide();
	}
	else {
		reloadCommentLayerInThickbox();
		$('div#commentDivInThickbox').show();
		$('div#thickbox_content_div').scrollTo('div#commentDivInThickbox', 800, {queue:true});
	}
}

function loadWritingIntroFloatLayer(writingId, clickedObjId, floatLayerId) {
	var leftAdjustment = (is_ie7)? 120 : 0;
	var topAdjustment = (is_ie7)? 3 : 20;
	changeLayerPosition(clickedObjId, floatLayerId, leftAdjustment, topAdjustment);
	showFloatLayer(getAjaxLoadingMsg());
	
	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     'mod=ajax&task=getWritingIntro&r_writingId=' + writingId,
		success:  function(xml){
					  showFloatLayer(xml);
				  }
	});
}

function showFloatLayer(contentHtml) {
	$('div#layer_ex_brief_content_div').html(contentHtml);
	MM_showHideLayers('layer_ex_brief', '', 'show');
}

function hideFloatLayer() {
	MM_showHideLayers('layer_ex_brief', '', 'hide');
}

function resetPowerConcept(assignmentId,lang){

	lang = lang.toLowerCase();
	strConfirm = (lang == 'chi' || lang =='ls') ? "確定重置腦圖?": "Are you sure you want to reset to the default brainstrorming picture?";
	strSuccess = (lang == 'chi' || lang =='ls') ? "重置成功": "Reset Success";

	var answer = confirm(strConfirm);

	if(answer){
		//do nothing
	}else{
//		alert("Reset Abord!");
		return;
	}

	$('#mod').val("ajax");
	$('#task').val("resetPowerConcept");

	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#handinForm").serialize()+'&r_assignmentId='+assignmentId,
		error:	function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
					  //have not handle
					  /*
					  date = $(xml).find("date").text();
					  $("#lastModifiedDate").html(date);
					  return 1;
						*/
					Scroll_To_Top();
					//$Lang['General']['ReturnMessage']['AddSuccess'] = '1|=|Record Added.';
///					Get_Return_Message('1|=|Reset Success');
					Get_Return_Message('1|=|'+strSuccess);
				  }
	});

}
function resetPowerBoard(assignmentId,lang){

	lang = lang.toLowerCase();
	strConfirm = (lang == 'chi' || lang =='ls') ? "確定重置腦圖?": "Are you sure you want to reset to the default brainstrorming picture?";
	strSuccess = (lang == 'chi' || lang =='ls') ? "重置成功": "Reset Success";

	var answer = confirm(strConfirm);

	if(answer){
		//do nothing
	}else{
//		alert("Reset Abord!");
		return;
	}

	$('#mod').val("ajax");
	$('#task').val("resetPowerBoard");

	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#handinForm").serialize()+'&r_assignmentId='+assignmentId,
		error:	function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
					  //have not handle
					  /*
					  date = $(xml).find("date").text();
					  $("#lastModifiedDate").html(date);
					  return 1;
						*/
					Scroll_To_Top();
					//$Lang['General']['ReturnMessage']['AddSuccess'] = '1|=|Record Added.';
///					Get_Return_Message('1|=|Reset Success');
					Get_Return_Message('1|=|'+strSuccess);
				  }
	});

}
// view the infoxbox as reference => hide unnecessary info and change the model answer from red to orange
function viewReferenceHandling() {
	$('span.w2_doneSpan, span.w2_notDoneSpan, div.w2_ansInputDiv, input.w2_ansInput, textarea.w2_ansInput, tr.w2_ansInputTr, div.w2_infoboxBtnDiv').hide();
	$('span.w2_modelAnsSpan').removeClass('modelans').addClass('modelans_orange');
}

function updateRefCategoryCssSetHiddenField(cssSet, clickedObj) {
	// changed the UI of the selected css set
	$('a.w2_highlight').each( function() {
		$(this).removeClass('highlight_select');
	});
	$(clickedObj).addClass('highlight_select');
	
	// update the hidden field
	$('input#r_refCategoryCssSet').val(cssSet);
}

function saveRefCategory(successMsg, failedMsg) {
	$('#mod').val("ajax");
	$('#task').val("saveRefCategory");
	
	var canSave = true;
	if (Trim($('input#r_refCategoryTitleTb').val()) == '') {
		alert('Please enter Group Name.');
		$('input#r_refCategoryTitleTb').focus();
		canSave = false;
	}
	
	if (canSave) {
		$.ajax({
			url:      "/home/eLearning/w2/index.php",
			type:     "POST",
			data:     $("#handinForm").serialize() + '&' + $("#thickboxForm").serialize(),
			success:  function(xml){
							var returnedRefCategoryId = parseInt(xml);
							var returnMsg;
						  
							if (returnedRefCategoryId > 0) {
								var infoboxCode = $('input#r_infoboxCode').val();
								reloadRefDisplay(infoboxCode);
								
								js_Hide_ThickBox();
								returnMsg = successMsg;
							}
							else {
								returnMsg = failedMsg;
							}
							//Scroll_To_Top();
							Get_Return_Message(returnMsg);
					  }
		});
	}
}

function deleteRefCategory(confirmMsg, successMsg, failedMsg, refCategoryId, infoboxCode) {
	if (confirm(confirmMsg)) {
		$('#mod').val("ajax");
		$('#task').val("deleteRefCategory");
		
		$.ajax({
			url:      "/home/eLearning/w2/index.php",
			type:     "POST",
			data:     $("#handinForm").serialize() + '&r_refCategoryId=' + refCategoryId,
			success:  function(xml){
							if (xml == '1') {
								reloadRefDisplay(infoboxCode);
								returnMsg = successMsg;
							}
							else {
								returnMsg = failedMsg;
							}
							
							Scroll_To_Top();
							Get_Return_Message(returnMsg);
					  }
		});
	}
}

function reloadRefDisplay(infoboxCode) {
	$('#mod').val("ajax");
	$('#task').val("reloadRefDisplay");
	
	var refDivId = 'refDiv_' + infoboxCode;
	//$('div#' + refDivId).html(getAjaxLoadingMsg());
	
	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#handinForm").serialize() + '&r_infoboxCode=' + infoboxCode,
		success:  function(xml){
						highlightEssay(infoboxCode);
						$('div#' + refDivId).html(xml);
						initThickBox();
				  }
	});
}

function triggerAddItemDiv(refCategoryCode) {
	var divId = 'addRefItemDiv_' + refCategoryCode;
	var textboxId = 'newRefItemTb_' + refCategoryCode;
	
	if ($('div#' + divId).is(':visible')) {
		$('div#' + divId).hide();
	}
	else {
		$('div#' + divId).show();
		$('input#' + textboxId).focus();
	}
}

function saveRefItem(newRefDivId, textboxId, refCategoryType, refCategoryCode, infoboxCode, refItemId) {
	refItemId = refItemId || '';
	
	$('#mod').val("ajax");
	$('#task').val("saveRefItem");
	
	var refItemTitle = Trim($('input#' + textboxId).val());
	
	$('div#' + newRefDivId).html(getAjaxLoadingMsg());
	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#handinForm").serialize() + '&r_refItemTitle=' + refItemTitle + '&r_refCategoryType=' + refCategoryType + '&r_refCategoryCode=' + refCategoryCode + '&r_infoboxCode=' + infoboxCode + '&r_refItemId=' + refItemId,
		success:  function(xml){
						//$('div#' + newRefDivId).hide();
						//$('input#' + textboxId).val('');
						//initThickBox();
						reloadRefDisplay(infoboxCode);
				  }
	});
}

function deleteRefItem(confirmMsg, refItemId, infoboxCode) {
	$('#mod').val("ajax");
	$('#task').val("deleteRefItem");
	
	if (confirm(confirmMsg)) {
		$.ajax({
			url:      "/home/eLearning/w2/index.php",
			type:     "POST",
			data:     $("#handinForm").serialize() + '&r_refItemId=' + refItemId,
			success:  function(xml){
							reloadRefDisplay(infoboxCode);
					  }
		});
	}
}

function triggerEditRefItemUi(refItemId) {
	var titleDisplayDivId = 'itemTitleDisplayDiv_' + refItemId;
	var titleEditDivId = 'itemTitleEditDiv_' + refItemId;
	var editTitleTbId = 'itemTitleEditTb_' + refItemId;
	
	$('div#' + titleEditDivId).show();
	$('div#' + titleDisplayDivId).hide();
	$('input#' + editTitleTbId).data('originalValue', $('input#' + editTitleTbId).val()).focus();
}

function cancelEditRefItem(displayDivId, editTitleDivId, editTitleTbId) {
	$('div#' + displayDivId).show();
	$('div#' + editTitleDivId).hide();
	$('input#' + editTitleTbId).val($('input#' + editTitleTbId).data('originalValue'));
}

function highlightEssay(infoboxCode) {
	infoboxCode = infoboxCode || '';
	
	$('#mod').val("ajax");
	$('#task').val("getEssayHighlightJson");
	
	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#handinForm").serialize() + '&r_infoboxCode=' + infoboxCode,
		success:  function(json){
					//var highlightObj = JSON.parse(json); 		// IE does not have JSON by default
					if (typeof (JSON) == 'undefined') {
					    eval("var highlightObj = " + json);
					}
					else {
					    var highlightObj = JSON.parse(json);
					}
										
					$.each(highlightObj, function(_infoboxCode, _hightlightAssoAry) {
						if (infoboxCode != '' && _infoboxCode != infoboxCode) {
							return;
						}
						
						var essayDivId = 'w2_infoboxEssayDiv_' + _infoboxCode;
						
						// unhighlight all items first
						$.each(_hightlightAssoAry, function(__css, __itemTitleAry) {
							$("div#" + essayDivId).unhighlight({ className: __css, element: 'span' }); 
						});
						
						// highlight the items now
						$.each(_hightlightAssoAry, function(__css, __itemTitleAry) {
							$("div#" + essayDivId).highlight(__itemTitleAry, { wordsOnly: true, className: __css, element: 'span', caseSensitive: false }); 
						});
					});
				  }
	});
}

function handleVocabMeaningDisplay(handleDivPrefix){
	var contentDiv = '#' + handleDivPrefix + '_content';
	var liDiv = '#' + handleDivPrefix + '_li';

	var displayStatus = $(contentDiv).css("display");

	if(displayStatus == 'none'){
		$(contentDiv).show();
		$(liDiv).addClass('open');
	}else{
		$(contentDiv).hide();
		$(liDiv).removeClass('open');
	}
}
function uploadFile(){

	$('#mod').val("common");
	$('#task').val("uploadFile");

	$('#uploadFileIFrame').attr('src', "/home/eLearning/w2/index.php");

	$('#handinForm').attr('target', 'uploadFileIFrame').submit();
	$('#handinForm').attr('target', 'uploadFileIFrame').submit().attr('target', '_self');
	//alert('last line of upload file');
	
}
function refreshUploadFilePanel(){
	$('#mod').val("ajax");
	$('#task').val("reloadStudentUploadFilePanel");

		$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#handinForm").serialize(),
		error:	function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
					$("#w2_studentUploadFilePanel").html(xml);
				  }
	});

}

$(document).ready(function(){
	$(window).resize(function() {
	    if(this.resizeTO) clearTimeout(this.resizeTO);
	    this.resizeTO = setTimeout(function() {
	        $(this).trigger('resizeEnd');
	    }, 100);
	});
	
	$(window).bind('resizeEnd', function() {
	    init_tag_label();
	});
	
	$('.new_tag').bind('mouseover', function(){
		$(this).css('z-index', 20);
	});
	
	$('.new_tag').bind('mouseout', function(){
		$(this).css('z-index', 10);
	});

	init_tag_label();
});

function init_tag_label(){
	var tag_obj = new Array($('.new_tag').length);
	$('.new_tag').each(function(key){
		tag_obj[key] = $(this);
	});
	
	$('.tag_label').each(function(key){
		var idx = parseInt(key) + 1;
		
		var pos = $(this).offset();
		
		var leftPos = 18;
		var topPos = parseFloat(pos.top) - parseInt(9);
		
		tag_obj[key].css({'left':(leftPos)+'px','top':(topPos)+'px','z-index':10}).show();
		idx++;
	});
}