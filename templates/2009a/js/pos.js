// ePOS common functions
// using: Ivan

var jsClientConnectionAjaxFile = "/home/eAdmin/GeneralMgmt/pos/ajax_client_connection.php";

function js_Change_Client_Connection()
{
	var TargetStatus;
	if ($('a#AllowClientProgramConnectIcon').attr('class') == 'active_item')
	{
		// active -> inactive
		$('a#AllowClientProgramConnectIcon').attr('class', 'inactive_item').attr('title', JSLang['Enabled']);
		TargetStatus = 0;
	}
	else
	{
		// inactive -> active
		$('a#AllowClientProgramConnectIcon').attr('class', 'active_item').attr('title', JSLang['Disabled']);
		TargetStatus = 1;
	}
	
	js_Update_Client_Connection(TargetStatus);
	js_Update_Client_Connection_Text_Color();
}

function js_Check_Client_Connection()
{
	$.post(
		jsClientConnectionAjaxFile, 
		{ Action: "CheckSettings" },
		function(ReturnData) {}
	);
}

function js_Update_Client_Connection(jsValue)
{
	$.post(
		jsClientConnectionAjaxFile,
		{ 
			Action: "UpdateSettings",
			Value: jsValue
		},
		function(ReturnData) {}
	);
}

function js_Update_Client_Connection_Text_Color()
{
	if ($('a#AllowClientProgramConnectIcon').attr('class') == 'active_item')
	{
		// active => black
		$('div#AllowClientProgramConnectDiv').css('color', '#000000');
	}
	else
	{
		// inactive => red
		$('div#AllowClientProgramConnectDiv').css('color', 'red');
	}
}

function js_Is_Barcode_Format_Valid(jsBarCode)
{
	var barcode_max_length = 100;	// according to Database format
	var ValidChars = new RegExp("[0-9A-Z\ \.\/\|\$\-]");
	var tmp_length = jsBarCode.length;
	var ErrorCode = '';
	var Char;
	
	if (jsBarCode != "")
	{
		if(tmp_length > barcode_max_length)
		{
			ErrorCode = 'GreaterThanMaxLength';
		}
		for(i=0; i<tmp_length; i++)
		{
			if (ValidChars.test(Char = jsBarCode.charAt(i)) == false)
			{
				ErrorCode = 'InvalidFormat';
				break;
			}
		}
	}
	else
	{
		ErrorCode = 'EmptyBarcode';
	}
	
	if (ErrorCode != '')
		return ErrorCode;
	else
		return 1;
}