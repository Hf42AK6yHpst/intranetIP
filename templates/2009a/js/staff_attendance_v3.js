function DOM_Check_Slot_Name(SlotName,WarningLayer,WarningRow) {
	ElementObj = $('div#'+WarningLayer);
	SlotNameValidate = new RegExp(/[\\\^\@\"\'\:\<\>\,\.\/\?\[\]\~\!\#\$\*\(\)\{\}\|\=\+\-&\;\%]/);
	
	if (Trim(SlotName) != "" && !SlotNameValidate.test(SlotName)) {
		ElementObj.html('');
		document.getElementById(WarningRow).style.display = 'none';
		return true;
	}
	else {
		if (SlotNameValidate.test(SlotName)) 
			ElementObj.html(JSLang['SlotSpecialCharacterCheckWarning']);
		else
			ElementObj.html(JSLang['SlotNameWarning']);
		document.getElementById(WarningRow).style.display = '';
		
		return false;
	}
}

function Check_Day_Count() {
	var DayCount = $('input#DutyCount').val();
	var ElementRow = document.getElementById('DutyCountWarningRow');
	var ElementLayer = $('div#DutyCountWarningLayer');
	 
	var isNumber = !isNaN(new Number(DayCount));
	if(isNumber && DayCount <= 1 && DayCount > 0) {
		if(DayCount.indexOf('.') != -1 || DayCount == 1) {
			ElementLayer.html('');
			ElementRow.style.display = 'none';
		} else {
			ElementLayer.html(JSLang['DayCountFormatWarning']);
			ElementRow.style.display = '';
		}
	} else {
		ElementLayer.html(JSLang['DayCountFormatWarning']);
		ElementRow.style.display = '';
	}
}

function dayOfWeek(day,month,year) {
    var dateObj = new Date();
    dateObj.setFullYear(year,month,day);
    return dateObj.getDay();
}

function isLeapYear(year) {
  return new Date(year,2-1,29).getDate()==29;
}

function NthDay(nth,weekday,month,year) {
	var daysofmonth   = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	var daysofmonthLY = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    if (nth > 0)
    	return (nth-1)*7 + 1 + (7+weekday-dayOfWeek((nth-1)*7+1, month, year))%7;
    if (isLeapYear(year))
    	var days = daysofmonthLY[month]; 
    else
    	var days = daysofmonth[month];
    return days - (dayOfWeek(days,month,year) - weekday + 7)%7;
}

if(!window.ShowPanel)
{
	window.ShowPanel = function(thisObj, PanelID)
	{
		var $PanelObj = $('#'+PanelID);
		var $ClickOnObj = $(thisObj);
		var $pos = $ClickOnObj.position();
		var offsetY = $ClickOnObj.height();
		$PanelObj.css('position', 'absolute');
		$PanelObj.css('left',$pos.left+'px');
		$PanelObj.css('top',($pos.top+offsetY)+'px');
		$PanelObj.css('visibility','visible');
		$PanelObj.slideDown();
	}
}
