$(document).ready(function()
{
	$("#btnUserMenu").bind("click", function()
	{
		$("#blkUserMenu").toggleClass("expand");
	});

  $(".menu").not("#btnUserMenu").bind("click", function()
	{
		$("#blkUserMenu").removeClass("expand");
	});


  var animateTime = 300,
      $accordion = $('.accordion');

  $accordion.each(function()
  {
    $(this).on("click", function()
    {
      var $accordionPage = $(this).next('.accordion-page');
      $(this).toggleClass("collapse");
      if($accordionPage.height() === 0)
      {
      autoHeightAnimate($accordionPage, animateTime);
      } else {
        $accordionPage.stop().animate({ height: '0' }, animateTime);
      }
    });
  });

  /* Function to animate height: auto */
  function autoHeightAnimate(element, time)
  {
  	var curHeight = element.height(), // Get Default Height
        autoHeight = element.css('height', 'auto').height(); // Get Auto Height
    element.height(curHeight); // Reset to Default Height
    element.stop().animate({ height: autoHeight }, time); // Animate to Auto Height
  }
});