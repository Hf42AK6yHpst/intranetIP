<?php 
## Using By :

/********************** Change Log ***********************/
/*
 *  Date:       2019-01-22 Isaac
 *              created this as the common template file
 *
 */

/********************** Change Log ***********************/

	session_start();
	$_SESSION['PowerClass_PAGE'] = "app_settings";

 include_once('iframeHead.php');	
?>
<body>
	<div class="CustCommon CustMainContent PowerCLassPortal">
		<div class="pageHeader" id="header">
			<div class="header_title">
				<span id="module_title" class="menu_opened "><?php echo $Lang["PowerClass"]["Settings"]; ?></span>
			</div>
		</div>
		<div id="blkModuleContainer">
			<table id="tblAppSettings">
				<?php 
				foreach($app_details as $key=>$app){
					if(!is_array($app))continue;
				?>
				<tr>
					<td>
						<span class="portalIcon portalIcon-<?=$app['icon_class']?>">
							<div class="portalIcon_img">
							</div>
							<div class="portalIcon_text">
								<span>
									<div><a href="<?=$app['home_href']?>"><nobr><?=$app['Title']?></nobr></a></div>
								</span>
							</div>
						</span>
					</td>
					<td class="accordion">
						<span class="portalIcon portalIcon-<?=$app['icon_class']?>">
							<div class="portalIcon_img">
							</div>
							<div class="portalIcon_text">
								<span>
									<div><a href="<?=$app['home_href']?>"><nobr><?=$app['Title']?></nobr></a></div>
								</span>
							</div>
						</span>
					</td>
					<td class="accordion-page">
						<div>
							<?php
							foreach($app['Setting'] as $setting){
							?>
							<a class="appSettings-buttons" href="<?=$setting['Link']?>"><nobr><?=$setting['Lang']?></nobr></a>
							<?php
							}
							?>
						</div>
					</td>
				</tr>
				<?php	
				}				
				?>
			</table>
		</div>
		<?php echo $footerHtml?>
	</div>
</body>
</html>