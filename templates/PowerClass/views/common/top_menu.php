<?php
## Using By : 

/********************** Change Log ***********************/
/*
 *  Date:       2020-03-10 Tommy
 *              modified permissoin checking of school setting button
 * 
 *  Date:       2019-01-22 Isaac
 *              created this as the common template file
 *
 */

/********************** Change Log ***********************/

$topMenuCustomization = "";
if ($withTableTD) {
	$topMenuCustomization .= '<tr>
			<td>';
}

if($isCustTemplateTitle){
    $templateTitle = '<a href="' . $dhl_menu["leftMenu"]["home"]["link"] . '"><span class="headerSchName">'.$Lang["PageTitle"].'</span></a>';
}

if( ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"] || $_SESSION['SSV_USER_ACCESS']['eAdmin-AccountMgmt'] || $_SESSION['SSV_USER_ACCESS']["eLearning-eClass"]) && $_SESSION['UserType']==USERTYPE_STAFF){
    $systemSetupbtn = '<li id="btnSetup" class="menu" onclick="$(\'#m_setup\').click();">
    <a href="/home/PowerClass/setup.php" id="m_setup" rel="/home/PowerClass/setup.php" class="framelink"><span>
    ' . $Lang['Header']['Menu']['SystemSetting'] . '
        </span></a></li>';
}

$topMenuCustomization .= '<div id="blkTop" class="textColor-1  CustCommon CustTopHeader">
			<a href="' . $dhl_menu["leftMenu"]["home"]["link"] . '"><img src="' . $source_path . 'images/logo_header.png" style="height:100%""></a>
			'.$templateTitle.'
			<div id="blkRightMenu">
				<ul>
                    '.$systemSetupbtn.' 
					<li class="divider"></li>
					<li id="btnUserMenu" class="menu">
						<div class="lblInfo">
							<div id="lblRole">' . $UserIdentity . '</div>
							<div id="lblUsername">
								' . $UserName . '
							</div>
						</div>
						<div id="blkUserSubMenu">
						<ul>';
if (count($dhl_menu["rightMenu"]) > 0) {
	$topMenuCustomization .= ' <li>';
	foreach ($dhl_menu["rightMenu"] as $rightIndex => $rightVal) {
		if ($rightIndex == "language") {
			if (count($rightVal["child"]) > 0) {
				foreach ($rightVal["child"] as $subIndex => $subVal) {
					$class = $subVal["class"];
					if ($subVal["selected"]) {
						if (!empty($class)) {
							$class .= " ";
						}
						$class .= "lang_button_current";
					}
					$topMenuCustomization .= '<a href="' . $subVal["link"] . '" class="' . $subVal["class"] . ' ' . $class . '">' . $subVal["title"] . '</a>';
				}
			}
		}
		if ($rightIndex == "userIdentify") {
			if ($rightVal["status"]) {
				if (count($rightVal["child"]) > 0) {
					foreach ($rightVal["child"] as $subIndex => $subVal) {
						$class = $subVal["class"];
						if ($subVal["selected"]) {
							if (!empty($class)) {
								$class .= " ";
							}
							$class .= "active";
						}
						$topMenuCustomization .= '	<li class="' . $class . '"><a href="' . $subVal["link"] . '" rel="' . $subVal["link"] . '" id="m_' . $subIndex . '" class="framelink ignoreback">' . $subVal["title"] . '</a></li>';
					}
				}
			}
		}
	}
	$topMenuCustomization .= '</li>';
}
$topMenuCustomization .= '
						</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<!--<div id="blkTab" class="font-light">-->';
/*
if (count($dhl_menu["leftMenu"]) > 0) {
	$topMenuCustomization .= '<ul>';
	foreach ($dhl_menu["leftMenu"] as $leftIndex => $leftVal) {
		if ($leftVal["status"]) {
			$class = $leftVal["class"];
			if ($leftVal["selected"]) {
				if (!empty($class)) {
					$class .= " ";
				}
				$class .= "tab_current";
			}
			
			if (strpos($leftVal["class"], "newtab") !== false) {
				$topMenuCustomization .= '<li class="newtab">';
				$topMenuCustomization .= '<a href="' . $leftVal["link"] . '" target="newtab" class="' . $leftVal["class"]. '"><span>' . $leftVal["title"] . '</span></a>';
			} else {
				$topMenuCustomization .= '<li class="' . $class . '" id="m_' . $leftIndex . '" rel="' . $leftVal["link"] . '">';
				$topMenuCustomization .= '<a href="#" class="' . $leftVal["class"]. '"><span>' . $leftVal["title"] . '</span></a>';
			}
		}
	}
	$topMenuCustomization .= '</ul>';
}
if (isset($PowerLession2)) {
	$topMenuCustomization .= $PowerLession2;
}
$topMenuCustomization .= '</div>';*/
if ($withTableTD) {
	$topMenuCustomization .= '</td>
		</tr>';
}
?>