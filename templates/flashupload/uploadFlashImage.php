<?php
/*
 * Editing by 
 * 
 * Modification Log:
 * 2020-02-18 (Henry)
 * 		- added intranet_auth(), log img path, path checking and file image type checking for security issue fix
 * 2011-04-20 (Jason)
 * 		- fix the problem of the failure of uploading image if client is using prozy server by 
 * 		  add global path & change the value of $imgdir to /home/eclass/intranetIP/ + /file/...
 */
include_once("../../includes/global.php"); 

intranet_auth();

$PATH_WRT_ROOT = "../../../";

############################################################################
# UPLOAD
############################################################################
$imgdir = $PATH_WRT_ROOT."src/tool/flashupload/files/";	

if(!is_null($_GET['imgDir'])){
	$log_path = $file_path . '/file/flashupload_uploadFlashImage_path_log';
	shell_exec('echo \'' . date("Y-m-d H:i:s") . ': ' . OsCommandSafe($_GET['imgDir']) . '\' >> \'' . $log_path . '\'');
	
	if(strpos($_GET['imgDir'], '/file/') === 0 || strpos($_GET['imgDir'], 'file/') === 0 || strpos($_GET['imgDir'], $imgdir) === 0){
		//$imgdir = $_GET['imgDir'];
		$imgdir = $intranet_root.'/'.$_GET['imgDir'];
	}
	else{
		die("Path incorrect.");
	}
}

if (!is_null($_FILES['Filedata'])){
	
	$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
	$detectedType = exif_imagetype($_FILES['Filedata']['tmp_name']);
	$error = !in_array($detectedType, $allowedTypes);
	if($error){
		die("File is not an image.");
	}
		
	$old_umask = umask(0);	
	if (!is_dir($imgdir)){		 
		mkdir($imgdir, 0777, true);
	}
	umask($old_umask);
	$file_name = $_FILES['Filedata']['name'];
	$file_name = get_magic_quotes_gpc() ? stripcslashes($file_name) : $file_name;
	move_uploaded_file($_FILES['Filedata']['tmp_name'], $imgdir.$file_name);  
	chmod($imgdir.$_FILES['Filedata']['name'], 0777); 

	if($imgdir.$_FILES['Filedata']['name']){
		echo "success"; //let actionscript knows
	}

}else{
	//error - don't echo anything, if not, means success
}


?>