// using: 

////////////////////// Change Log ////
//	Date:	2020-06-29 Henry 
//			modified analysisData() for fix #F188372
//
//	Date:	2018-01-16 Isaac 
//			modified getStats(), changed percentage of answers rand off to 2 decimal places. 
//
//	Date:	2015-12-16 Pun 
//			modified viewForm(), added number of answer for ordering. 	
//
//	Date:	2015-11-23 Omas 
//			modified existPos() for fix #B89432 	
//
//	Date:	2015-11-13 Bill
//		modified viewForm(), analysisData(), to remove Question settings from options
//
//	Date:	2011-02-24 YatWoon
//		add DisplayQuestionNumber checking, if DisplayQuestionNumber==1 display question number before the question
//
////////////////////////////////////

function viewForm(txtStr, txtAns){
	var txtArr=txtStr.split("#QUE#");
	ansArr=txtAns.split("#ANS#");
	var tmpArr = null;
	var txtStr = null;

	txtStr='<Form name="answersheet">';
        txtStr+='<table width="100%" border="0" cellpadding="0" cellspacing="5">';
	for (var x=1; x<txtArr.length; x++){
		j = x - 1;
		tmpArr = txtArr[x].split("||");
		type_no = tmpArr[0].split(",");			//0:type, 1:number of options, 2: (For Ordering type)number of answer
		question = tmpArr[1];
		opts = tmpArr[2];
		// remove Question settings
		if(opts.indexOf('#MSUB#')!==-1){
			opts = opts.split("#MSUB#");
			opts = opts[0];
		}
		
		tmpArr = opts.split("#OPT#");

        txtStr+='<tr><td align="center" bgcolor="#EFEFEF">';
		txtStr+='<table border="0" width="95%" align="center" bgcolor="#EFEFEF"><tr><td bgcolor="#EFEFEF" colspan="3" class="tabletext">\n';
		txtStr+='</td></tr>\n';
                
		txtStr+='<table border="0" width="100%"><tr><td class="tabletext" colspan="2">\n';
		if(typeof(DisplayQuestionNumber)!='undefined')
		{
			if(DisplayQuestionNumber==1)
			{
				txtStr += (x) + '. ';
			}
		}
							
		txtStr+=(type_no[0] != 7) ? question : '';
		txtStr+='</td></tr>\n';
        txtStr+='<td class="tabletext"></td>';
		txtStr+='<td width="95%" class="tabletext">';

		switch (type_no[0]){
			case "1":
			case "2":
				txtStr += (isNaN(parseInt(ansArr[x]))) ? "--" : tmpArr[parseInt(ansArr[x])+1];
				break;
			case "3":
				checkAns = ansArr[x].split(",");
				c_i_i = 0
				for (var m=0; m<checkAns.length; m++) {

					if (isNaN(parseInt(checkAns[m]))) {
						txtStr += "--";
					}
					else
					{
						txtStr += (c_i_i>0) ? "<br><br>"+tmpArr[parseInt(checkAns[m])+1] : tmpArr[parseInt(checkAns[m])+1];					    
						c_i_i = 1;
					}
				}
				break;
			case "4":
			case "5":
				txtStr += ansArr[x];
				break;
			case "6":
				break;
			case "7": // Ordering
				var num_of_answer = type_no[2] || type_no[1];
			//(isNaN(parseInt(ansArr[x]))) ? "--" : tmpArr[parseInt(ansArr[x])+1];
				var head1 = (typeof order_lang != undefined) ? order_lang : 'Order';
				var head2 = question;
				if(question.indexOf("|") > -1){
					var tmpQuestionArr = question.split("|");
					head1 = tmpQuestionArr[0];
					head2 = tmpQuestionArr[1];
				}
				var stdAnsArr = ansArr[x].split(",");
				var ans_empty = "" == ansArr[x].replace(/,/gi,"");    
				
				var txtOrderingStr = '';
				txtOrderingStr += '<table border="0" cellspacing="0" cellpadding="2" style="border: 1px solid black; border-collapse: collapse; ">';
				txtOrderingStr += '<tr>';
				txtOrderingStr += '<td align="center" width="80px" style="border: 1px solid black; "><b>'+head1+'</b></td>';
				txtOrderingStr += '<td align="center" style="width:300px; border: 1px solid black;"><b>'+head2+'</b><div style="margin: 0px 0px 0px 0px;height: 1px;width: 300px;"></div></td>';
				txtOrderingStr += '</tr>';
				
				for(var h=0 ; h<num_of_answer ; h++){
					txtOrderingStr += '<tr>';
					txtOrderingStr += '<td align="center" style="border: 1px solid black;vertical-align: top;">'+(parseInt(h)+1)+'.</td>';
					if(ans_empty){						
						txtOrderingStr += '<td align="center" style="border: 1px solid black;width:300px;">'+ (typeof no_answer_lang == 'undefined' ? "" : no_answer_lang)+'</td>';
					}else{
						txtOrderingStr += '<td align="center" style="border: 1px solid black;width:300px;">'+tmpArr[parseInt(stdAnsArr[h])+1]+'</td>';
					}
					txtOrderingStr += '</tr>';
				}
				txtOrderingStr += '</table>';
				
				txtStr += txtOrderingStr;
				break;
		}
		txtStr+=(type_no[0]=="6") ? '</td>' : '</td>';
		txtStr+='</table>\n';
                txtStr+='</td></tr>';
                
	}

        txtStr+='</table>';
	txtStr+='</Form>';

	return txtStr;
}


function switchLayer(layer_name, j){
	var hide = "hidden";
	var show = "visible";

	/*
	if (!document.all)
		return;
		*/
	var co=(document.getElementById && !document.all)? document.getElementById(layer_name).style : document.all[layer_name].style;

	if (co.visibility==show){
		co.visibility = hide;
		co.position  = "absolute";
        eval("document.all.sb"+j+".innerHTML='" + w_show_details+"';");
        //co.top = -50000;
	}
	else {
		co.visibility = show;
		co.position  = "relative";
        eval("document.all.sb"+j+".innerHTML='" + w_hide_details+"';");
        //co.top = 0;
	}
}



/*################################## STATISTICS ####################################*/
function Statistics()
{
	if (typeof(_statistics_prototype_called) == 'undefined')
	{
		 _statistics_prototype_called = true;
		 Statistics.prototype.qString = null;
		 Statistics.prototype.total = 0;
		 Statistics.prototype.answer = new Array();
		 Statistics.prototype.data = new Array();
		 Statistics.prototype.analysisData = analysisData;
		 Statistics.prototype.getStats = getStats;
		 Statistics.prototype.existPos = existPos;
	}

	function getStats() {
		var resultArr = new Array();
		var txtArr = this.qString.split("#QUE#");
		var dataArr = this.data;
		var tmpArr = null;
		var txtStr = null;

		txtStr='<Form name="answersheet">';
        txtStr+='<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">\n';
        txtStr+='<tr><td align="left" class="eNoticereplytitle">'+w_total_no+' : '+this.total+'</td></tr>\n';
        txtStr+='</table>';        
        txtStr+='<table width="96%" border="0" cellpadding="0" cellspacing="5" align="center">\n';
        
		for (var x=1; x<txtArr.length; x++){
			j = x - 1;
			tmpArr = txtArr[x].split("||");
			type_no = tmpArr[0].split(",");			//0:type, 1:number of options
			question = tmpArr[1];
		
            txtStr+='<tr><td align="center" bgcolor="#EFEFEF">\n';
			txtStr+='<table width="100%" border="0" cellspacing="2" cellpadding="2">';
                        
                        txtStr+='<tr>';
                        txtStr+='<td class="tabletext">';
		if(typeof(DisplayQuestionNumber)!='undefined')
		{				
			if(DisplayQuestionNumber==1)
			{
				txtStr += (x) + '. ';
			}
		}
		txtStr+= question+'</td>';
                        
            if (type_no[0]==4 || type_no[0]==5) {
            		txtStr+='<td width="15%" nowrap>';
                            txtStr+="<a class=\"tablelink\" href=\"javaScript:switchLayer('l_g"+j+"', "+ j +");\">"+w_view_details_img+ "<span id=\"sb"+ j +"\">"+ w_show_details + "</span></a>";
                            txtStr+='</td>';
			}
                        
            txtStr+='</tr>\n';

			if (type_no[0]!=6 && type_no[0]!=7) {
				txtStr+='<tr>';
				txtStr+='<td align="center" class="tabletext" colspan="2">';

                if (type_no[0]==4 || type_no[0]==5) {
					//txtStr+="<div id='l_g"+j+"' style='position:absolute; left:0px; top:-50000px; width:100%; z-index:1; visibility: hidden'>";
					//txtStr+="<div id='l_g"+j+"' style='position:absolute; z-index:1;  visibility: hidden'>";
					txtStr+="<div id='l_g"+j+"' style='position:absolute; width:100%; z-index:1; visibility: hidden'>";
				}
                
				txtStr+='<table width="100%" border="0" cellspacing="2" cellpadding="2">';
				for (var i=0; i<dataArr[j].length; i++) {
					percent = (dataArr[j][i][1]==0) ? 0 : Math.round((dataArr[j][i][1]*100/this.total)*100)/100;
//					percent = (dataArr[j][i][1]==0) ? 0 : Math.round(dataArr[j][i][1]*100/this.total);
					txtStr+='<tr><td width="80%" bgcolor="#FFFFFF" class="tabletext">'+dataArr[j][i][0] + '</td>';
                    txtStr+='<td nowrap bgcolor="#FFFFFF" class="tabletext">'+dataArr[j][i][1]+' ('+percent+'%)';
					txtStr+=(type_no[0]==4 && i==dataArr[j].length-1) ? '</td></tr>' : '</td></tr>';
				}
				txtStr+='</table>';
				if (type_no[0]==4 || type_no[0]==5)
					txtStr+="</div>";
				txtStr+='</td></tr>';		    
			} else if(type_no[0]==6) {
			    txtStr+='<tr><td colspan=3>&nbsp;</td></tr>';
			} else if(type_no[0]==7){
			    txtStr+='<tr><td nowrap bgcolor="#FFFFFF" colspan=3>('+noapprostat_lang+')</td></tr>';
			} else{
			    txtStr+='<tr><td colspan=3>&nbsp;</td></tr>';
			}
			
			txtStr+='</table></td></tr>\n';
		}
                
		txtStr+='</table>\n';
		txtStr+='</Form>';

		return txtStr;
	}


	function analysisData() {
		var resultArr = new Array();
		var txtArr=this.qString.split("#QUE#");

		// each application
		var ii=0;
		for (var i=0; i<this.answer.length; i++) {
			ansArr=this.answer[i].split("#ANS#");
			if(this.answer[i]=="")	
			{
				continue;
			}
			var tmpArr = null;

			// each question
			for (var x=1; x<txtArr.length; x++){
				j = x - 1;
				tmpArr = txtArr[x].split("||");
				type_no = tmpArr[0].split(",");			//0:type, 1:number of options
				question = tmpArr[1];
				opts = tmpArr[2];
				// remove Question settings
				if(opts.indexOf('#MSUB#')!==-1){
					opts = opts.split("#MSUB#");
					opts = opts[0];
				}

				tmpArr = opts.split("#OPT#");
				if (i==0 || ii==0) {
				    resultArr[j] = new Array();
				}
				
				switch (type_no[0]){
					case "1":
					case "2":
						if (i==0) {
							opt_no = (type_no[0]=="1") ? 2 : type_no[1];
							for (var k=0; k<opt_no; k++) {
								resultArr[j][k] = new Array();
								resultArr[j][k][0] = tmpArr[k+1];
								resultArr[j][k][1] = 0;
							}
						}
						if (!isNaN(parseInt(ansArr[x]))) {
	                        if(parseInt(ansArr[x]) >= resultArr[j].length)
	                        {
								resultArr[j][(resultArr[j].length-1)][1] ++;
	                        }
	                        else
	                        {
								resultArr[j][parseInt(ansArr[x])][1] ++;
	                        }
						}						
						break;
						
					case "3":
						if (ii==0 || i==0) {
							opt_no = type_no[1];
							for (var k=0; k<opt_no; k++) {
 								resultArr[j][k] = new Array();
								resultArr[j][k][0] = tmpArr[k+1];
								resultArr[j][k][1] = 0;
							}
						}
						if(ansArr[x]){
							checkAns = ansArr[x].split(",");
							for (var m=0; m<checkAns.length; m++) {
								if (!isNaN(parseInt(checkAns[m]))) {
									if(parseInt(checkAns[m])>=resultArr[j].length)		continue;
								    resultArr[j][parseInt(checkAns[m])][1] ++;
								}
							}
						}
						break;
						
					case "4":
					case "5":
						var answer_pos = this.existPos(resultArr[j], ansArr[x]);
						isArrExpand = (answer_pos==resultArr[j].length);

						if (isArrExpand) {
							resultArr[j][answer_pos] = new Array;
							resultArr[j][answer_pos][0] = ansArr[x];
							resultArr[j][answer_pos][1] = 0;
						}
						resultArr[j][answer_pos][1] ++ ;
						break;
					case "6":
						break;
				}
				ii++;
			}
		}

		this.total = this.answer.length;
		this.data = resultArr;
	}

	function existPos(reArr, value) {
		for (var i=0; i<reArr.length; i++) {
			//#B89432
			if (typeof reArr[i][0] != 'undefined' && typeof value != 'undefined') {
				if (reArr[i][0].toUpperCase()==value.toUpperCase())
					return i;
			}
		}
		return reArr.length;
	}

}
