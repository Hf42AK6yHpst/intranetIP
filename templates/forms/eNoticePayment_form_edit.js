// using by : Carlos

////////////////////// Change Log ////
//
//	Date: 2018-09-28 (Bill)
//	support new payment question type - Must Pay
//
//	Date: 2018-01-24 Carlos
//	added data-amount attribute to question options for counting and display the total that need to pay.
//
//	Date: 2017-11-16 Bill
//	improved seperate all options with line break 
//
//	Date: 2017-11-09 Bill
//	improved checking when add question to payment notice
//
//	Date: 2014-02-17 Kin
//	add label for checkedbox and radio button
//
//	Date: 2011-03-09	YatWoon
//	update changeTemplate()
//
//	Date:	2011-02-24	YatWoon
//		add MaxReplySlipOption
//
//	Date:	2011-02-16 YatWoon
//		add DisplayQuestionNumber checking, if DisplayQuestionNumber==1 display question number before the question
//
////////////////////////////////////

var formAllFilled = true;
// define a Answer Sheet class
function Answersheet()
{
  if (typeof(_answersheet_prototype_called) == 'undefined')
  {
        _answersheet_prototype_called = true;
        Answersheet.prototype.qString = null;
        Answersheet.prototype.aString = null;
        Answersheet.prototype.counter = 0;
        //Answersheet.prototype.changeCategory = "";
        Answersheet.prototype.answer = new Array();
        Answersheet.prototype.valueTmp = new Array();
        Answersheet.prototype.catIDTemp = new Array();
        Answersheet.prototype.amtTemp = new Array();
        Answersheet.prototype.itemIDTemp = new Array();
        Answersheet.prototype.nonPayItemTemp = new Array();
        Answersheet.prototype.templates = new Array();
        Answersheet.prototype.selects = new Array();
        Answersheet.prototype.sheetArr = sheetArr;
        Answersheet.prototype.writeSheet = writeSheet;
        Answersheet.prototype.stuffAns = stuffAns;
        Answersheet.prototype.move = move;
        Answersheet.prototype.chgNum = chgNum;
        Answersheet.prototype.secDes = secDes;
        Answersheet.prototype.mode = 0;                                	// 0:create; 1:for fill-in
        Answersheet.prototype.templateNo = 0;                        	// template list menu
     	//Answersheet.prototype.convertType = convertType;
     	if(typeof(MaxOptionNo)!='undefined' && MaxOptionNo>0)
			Answersheet.prototype.ansQty = MaxOptionNo;
		else
			Answersheet.prototype.ansQty = 50;							// default answer quantity [20090310 yat] (default = 15)
  }

        function writeSheet(withSpace){
				withSpace = withSpace || '1';
                var txtArr = this.answer;
                var valueArr = this.valueTmp;
                var valueCatArr = this.catIDTemp;
                var valueAmtArr = this.amtTemp;
                var valueItemIDArr = this.itemIDTemp;
                //var valueNonPayItemArr = this.nonPayItemTemp;

                var selArr = this.selects;
                var arr=txtArr;
                var strTowrite='';
                var assignScript = "";
                

                if (this.mode==0) {

                	// Display Order
                        strTowrite+='<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">\n'
						+'<tr><td valign=top>'+(typeof(Part2)!='undefined'?Part2:'&nbsp;')+'</td>'
						+'<td nowrap><form name="editForm">&nbsp; <select name="sList">';
                        for (x=0; x<arr.length; x++){
                                temTxt=arr[x][0];
                                strTowrite+='<option value="">'+cutStrLen(temTxt, 35);
                        }
                        strTowrite+='</select><br>'
                        +'&nbsp; <input type=button value="&uarr; '+ MoveUpBtn +'" onclick="sheet.move(this.form.sList.selectedIndex, \'up\');writetolayer(\'blockInput\',sheet.writeSheet()); ">'
                        +'<input type=button value="&darr; '+ MoveDownBtn +'" onclick="sheet.move(this.form.sList.selectedIndex, \'down\'); writetolayer(\'blockInput\',sheet.writeSheet()); ">'
                        +'<input type=button value="&Chi; '+ DeleteBtn +'" onclick="sheet.move(this.form.sList.selectedIndex, \'out\'); writetolayer(\'blockInput\',sheet.writeSheet()); ">'
                        +'<input type=button value="&harr; '+ EditBtn +'" onclick="sheet.secDes(this.form.sList.selectedIndex, \'out\'); "> </form>'
						+'</td></tr>\n'
						
						
						/*
						                        +'<tr><td>'
                     
                        +'<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">'
                        +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+(typeof(order_name)!="undefined"?order_name:"&nbsp;")+"</span></td>"
                        +'<td nowrap><form name="editForm">&nbsp; <select name="sList">';
                        for (x=0; x<arr.length; x++){
                                temTxt=arr[x][0];
                                strTowrite+='<option value="">'+cutStrLen(temTxt, 35);
                        }
                        strTowrite+='</select>'
                        +'&nbsp; <input type=button value="&uarr; '+ MoveUpBtn +'" onclick="sheet.move(this.form.sList.selectedIndex, \'up\');writetolayer(\'blockInput\',sheet.writeSheet()); ">'
                        +'<input type=button value="&darr; '+ MoveDownBtn +'" onclick="sheet.move(this.form.sList.selectedIndex, \'down\'); writetolayer(\'blockInput\',sheet.writeSheet()); ">'
                        +'<input type=button value="&Chi; '+ DeleteBtn +'" onclick="sheet.move(this.form.sList.selectedIndex, \'out\'); writetolayer(\'blockInput\',sheet.writeSheet()); ">'
                        +'<input type=button value="&harr; '+ EditBtn +'" onclick="sheet.secDes(this.form.sList.selectedIndex, \'out\'); "> </form></td></tr></table>'
*/
        					+'<tr><td colspan=2>'
                        +'<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center"><tr><td class="dotline" colspan="2">'+(typeof(space10)!='undefined'?space10:'&nbsp;')+'</td></tr></table>'
                        +'</td></tr>'
                        
                 	+'</td></tr></table>'
                }

                
                var ansNum=1;
                //alert(paymentOptAry);
                /*
				txtStr = '<script language="javascript">\n';
				txtStr += 'function updatePaymentType(emt, val) {\n';
				txtStr += 	'if(val=="-999") {\n';
				txtStr += 		'alert(emt);eval(emt).value = 1;\n';
				txtStr += 	'} else {\n';				
				txtStr += 		'alert(emt);eval(emt).value = 0;\n';
				txtStr += 	'}\n';
				txtStr += '}\n';
				txtStr += '</script>\n';
                */
                
                txtStr=strTowrite+'<Form name="answersheet">\n';
				
                txtStr+='<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">'
				+'<tr><td align=center>'+ replyslip +'</td></tr>'
				+'<tr><td>'+(typeof(Part3)!='undefined'?Part3:'&nbsp;')+'</td></tr>'
				+'<tr><td>'
				+'<table width="100%" border="0" cellpadding="0" cellspacing="5">';
                	
                        for (x=0; x<txtArr.length; x++){
						
						tit=recurReplace(">", "&gt;", txtArr[x][0]);
						tit = recurReplace("<", "&lt;", tit);
						if(this.mode==1) {
							tit = recurReplace("\n", "<br>", tit);
						}
						//tit = recurReplace("\n", " ", tit);
                        
                        
                        txtStr+='<tr><td bgcolor="#EFEFEF">';
                        
                        txtStr+='<table border="0" width="95%" align="center" bgcolor="#EFEFEF"><tr><td bgcolor="#EFEFEF" colspan="3" class="tabletext">\n';
                        //txtStr+=tit+'</td></tr>\n';
						if(typeof(DisplayQuestionNumber)!='undefined')
						{
	                        if(DisplayQuestionNumber==1)
							{
								txtStr += (x+1) + '. ';
							}
						}
                        for (y=1; y<txtArr[x].length; y++){
	                        
                                queArr=txtArr[x][y][0].split(",");
                              
                                //txtStr+='<td width="100%" class="tabletext" bgcolor="#EFEFEF">';
                                
                                switch (queArr[0]){
                                        case "1":		// True or False
                                                preValue0 = (ansNum<=this.counter) ? valueArr[ansNum-1][0] : "";
                                                
                                                preValue1 = (ansNum<=this.counter) ? valueArr[ansNum-1][1] : "";
                                                preValueAmt = (ansNum<=this.counter) ? valueAmtArr[ansNum-1][0] : "";
                                                
                                                r_check0 = "";
                                                r_check1 = "";
												tmpAmt = "";
                                                if (selArr.length>0) {

                                                    eval("r_check"+selArr[ansNum-1]+"='checked'");
                                                    tmpAmt = (selArr[ansNum-1]==1) ? "" : preValueAmt;
                                                }
												strLen = (ansNum<=this.counter) ? getStrLen(valueArr[ansNum-1]) : 40; // total str length
						
												if(this.mode==0) {
													txtStr += '<table><tr><td><u>'+paymentTitle+'</u></td><td><u>'+paymentCategory+'</u></td><td><u>'+paymentAmount+'</u></td></tr>';
													txtStr += '<tr><td>';
												}
												
												tmpCatID = (valueCatArr[ansNum-1]!=undefined) ? valueCatArr[ansNum-1][0] : "";
												tmpCatID = Trim(tmpCatID);
												
												if(this.mode==1 && tmpCatID!="-999") txtStr += '<font color="red">*</font>';
												//txtStr += (this.mode==1) ? tit : '<input type="text" name="Q'+ansNum+'" id="Q'+ansNum+'" value="'+tit+'">';
												txtStr += (this.mode==1) ? tit : '<textarea cols=20 rows=2 name="Q'+ansNum+'" id="Q'+ansNum+'">'+tit+'</textarea>';
												
												if(this.mode==0)
												{
													selection = '</td><td><SELECT name="CatID'+ansNum+'_0" id="CatID'+ansNum+'_0" onChange="if(this.value==-999) document.getElementById(\'nonPayItem'+ansNum+'\').value=1; else document.getElementById(\'nonPayItem'+ansNum+'\').value=0;">'+paymentOptions;
													for(k=0; k<paymentOptIDAry.length; k++) {
														//tmpCatID = (valueCatArr[ansNum-1]!=undefined) ? valueCatArr[ansNum-1][0] : "";
														selectFlag = (paymentOptIDAry[k]==tmpCatID) ? " selected" : "";
														selection += '<option value="'+paymentOptIDAry[k]+'" '+selectFlag+'>'+paymentOptNameAry[k]+'</option>';	
													}
													//var nonPaymentSelected = (tmpCatID=="-999") ? " selected" : "";
													//selection += '<option value="-999"'+nonPaymentSelected+'>'+nonPaymentItem+'</option>';
													selection += '</SELECT>';
													txtStr+=selection;
													txtStr += '</td>';
												}
						                        
						                        if(this.mode == 0) {
						                        	txtStr+=' <td>$<input type="text" name="amt'+ansNum+'_0" id="amt'+ansNum+'_0" value="'+((valueAmtArr[ansNum-1]!=undefined)?valueAmtArr[ansNum-1]:"")+'"></td>';
						                        	txtStr += '<tr></table>';
						                        } else {
							                     	txtStr += (tmpCatID!="-999") ? " ($"+((valueAmtArr[ansNum-1]!=undefined)?valueAmtArr[ansNum-1]:"")+")" : "";
						                        }
						                        
						                        txtStr+=' <input type="hidden" name="paymentItemID'+ansNum+'" id="paymentItemID'+ansNum+'" ';
						                        txtStr+=' value="'+valueItemIDArr[ansNum-1]+'">';
						                        txtStr+=' <input type="hidden" name="thisItemAmount'+ansNum+'" id="thisItemAmount'+ansNum+'" value="0">';
						                        txtStr+='</td></tr>\n';
												txtStr+='<td width="100%" class="tabletext" bgcolor="#EFEFEF">';
												
												if(this.mode==0) txtStr += '<u>'+paymentDescritption + '</u> : <br>';
                                                txtStr+='<input type="radio" value="0" '+r_check0+' name="F'+ansNum+'" id="FF0'+ansNum+'"';
                                                if(this.mode==1 && tmpCatID!="-999") txtStr+=' data-amount="'+((valueAmtArr[ansNum-1]!=undefined)?valueAmtArr[ansNum-1]:0)+'" onClick="updateAmountTotal('+ansNum+', document.getElementById(\'Qn'+ansNum+'\').value, '+((valueAmtArr[ansNum-1]!=undefined)?valueAmtArr[ansNum-1]:0)+', 1);document.getElementById(\'Qn'+ansNum+'\').value='+((valueAmtArr[ansNum-1]!=undefined)?valueAmtArr[ansNum-1]:0)+'"';
                                                txtStr+='>';
                                                txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue0+'" name="FD'+ansNum+'_0" size="25" class="tabletext"> ('+paymentPay+')' : '<label for="FF0'+ansNum+'">'+preValue0+'</label>';
												if(withSpace == '1'){
													//txtStr+=(this.mode==0 || strLen>=40) ? '<br>' : ' &nbsp; &nbsp; ';
													txtStr+= '<br>';
												}
                                                txtStr+='<input type="radio" value="1" '+r_check1+' name="F'+ansNum+'" id="FF1'+ansNum+'"';
                                                if(this.mode==1 && tmpCatID!="-999") txtStr+=' data-amount="0" onClick="updateAmountTotal('+ansNum+', document.getElementById(\'Qn'+ansNum+'\').value,0, 1);document.getElementById(\'Qn'+ansNum+'\').value=0"';
                                                txtStr+='>';
                                                txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue1+'" name="FD'+ansNum+'_1" size="25" class="tabletext"> ('+paymentNotPaying+')' : '<label for="FF1'+ansNum+'">'+preValue1+'</label>';
                                                txtStr+= (this.mode==1) ? '<input type="hidden" name="Qn'+ansNum+'" id="Qn'+ansNum+'" value="'+tmpAmt+'">' : '';
                                                var nPi = (tmpCatID=='-999') ? 1 : 0;
                                               	txtStr+= '<input type="hidden" name="nonPayItem'+ansNum+'" id="nonPayItem'+ansNum+'" value="'+nPi+'">';
                                               	
                                                break;
                                        case "2":		// MC (single), i.e. radio button
                                                r_check_i = (selArr.length>0) ? parseInt(selArr[ansNum-1]) : "";

                                                strLen = (ansNum<=this.counter) ? getStrLen(valueArr[ansNum-1]) : 60; // total str length
                                                
                                                tmpCatID = (valueCatArr[ansNum-1]!=undefined) ? valueCatArr[ansNum-1][0] : "";
                                                tmpCatID = Trim(tmpCatID);
                                                
                                                if(this.mode==0) txtStr += '<table><tr><td><u>'+paymentTitle+'</u></td><td><u>'+paymentCategory+'</u></td></tr><tr><td>';

                                                //txtStr+= (this.mode==1 && tmpCatID!="-999") ? '<font color="red">*</font>' + tit : tit;
												if(this.mode==1 && tmpCatID!="-999") txtStr += '<font color="red">*</font>';
												txtStr += (this.mode==1) ? tit : '<input type="text" name="Q'+ansNum+'" id="Q'+ansNum+'" value="'+tit+'">';
                                                
                                                
                                                if(this.mode==0)
												{
													txtStr += '</td>';
													selection = '<SELECT name="CatID'+ansNum+'_0" id="CatID'+ansNum+'_0" onChange="if(this.value==-999) document.getElementById(\'nonPayItem'+ansNum+'\').value=1; else document.getElementById(\'nonPayItem'+ansNum+'\').value=0;">'+paymentOptions;
													for(k=0; k<paymentOptIDAry.length; k++) {
														//tmpCatID = (valueCatArr[ansNum-1]!=undefined) ? valueCatArr[ansNum-1][0] : "";
														selectFlag = (paymentOptIDAry[k]==tmpCatID) ? " selected" : "";
														selection += '<option value="'+paymentOptIDAry[k]+'" '+selectFlag+'>'+paymentOptNameAry[k]+'</option>';	
													}
													//var nonPaymentSelected = (tmpCatID=="-999") ? " selected" : "";
													//selection += '<option value="-999"'+nonPaymentSelected+'>'+nonPaymentItem+'</option>';													
													selection += '</SELECT>';
	
													txtStr+='<td>'+selection+'</td></tr></table>';
													
													txtStr+=' <input type="hidden" name="amt'+ansNum+'_0" id="amt'+ansNum+'_0">';
													if(this.mode==0) txtStr += '<u>'+paymentAmount+'</u> : ';
												}
                                                txtStr+=' <input type="hidden" name="paymentItemID'+ansNum+'" id="paymentItemID'+ansNum+'" value="'+valueItemIDArr[ansNum-1]+'">';
                                                txtStr+=' <input type="hidden" name="thisItemAmount'+ansNum+'" id="thisItemAmount'+ansNum+'" value="0">';
                                                txtStr+='</td></tr>\n';
                                                txtStr+='<td width="100%" class="tabletext" bgcolor="#EFEFEF">';

												selectedValue = "";
												
                                                for (m=0; m<txtArr[x][y][1]; m++){
                                                        preValue = (ansNum<=this.counter) ? valueArr[ansNum-1][m] : "";
														tmpAmt = (r_check_i==m) ? preValue : "";
														
														if(tmpAmt==undefined) tmpAmt = "";
														
                                                        if (r_check_i=='' && r_check_i!='0')
                                                        {
                                                            txtStr+='<input type="radio" value="'+m+'" name="F'+ansNum+'" id="F'+ansNum+'_'+m+'"';
                                                            if(this.mode==1 && tmpCatID!="-999") txtStr+=' data-amount="'+parseFloat(valueArr[ansNum-1][m])+'" onClick="updateAmountTotal('+ansNum+',document.getElementById(\'Qn'+ansNum+'\').value,  '+parseFloat(valueArr[ansNum-1][m])+', 2);document.getElementById(\'Qn'+ansNum+'\').value='+parseFloat(valueArr[ansNum-1][m])+'"';
                                                            else txtStr+='';
                                                            txtStr+='>';
                                                            
                                                        }
                                                        // signed
                                                        else
                                                        {
                                                            if(r_check_i==m) {
	                                                           txtStr += '<input type="radio" value="'+m+'" checked name="F'+ansNum+'" id="F'+ansNum+'_'+m+'"';
	                                                           if(this.mode==1 && tmpCatID!="-999") txtStr+=' data-amount="'+parseFloat(valueArr[ansNum-1][m])+'" onClick="updateAmountTotal('+ansNum+', document.getElementById(\'Qn'+ansNum+'\').value, '+parseFloat(valueArr[ansNum-1][m])+', 2);document.getElementById(\'Qn'+ansNum+'\').value='+parseFloat(valueArr[ansNum-1][m])+'"';
	                                                           else txtStr+='';
	                                                           txtStr += '>';
	                                                           selectedValue = preValue;
	                                                           
                                                            } else {
	                                                            txtStr += '<input type="radio" value="'+m+'" name="F'+ansNum+'" id="F'+ansNum+'_'+m+'"';
	                                                            if(this.mode==1 && tmpCatID!="-999") txtStr+=' data-amount="'+parseFloat(valueArr[ansNum-1][m])+'" onClick="updateAmountTotal('+ansNum+', document.getElementById(\'Qn'+ansNum+'\').value, '+parseFloat(valueArr[ansNum-1][m])+', 2);document.getElementById(\'Qn'+ansNum+'\').value='+parseFloat(valueArr[ansNum-1][m])+'"';
	                                                            else txtStr+='';
	                                                            txtStr += '>';
                                                            }
                                                        }
                                                        txtStr+=(this.mode==0) ? '$<input type="text" value="'+preValue+'" name="FD'+ansNum+'_'+m+'" size="25" class="tabletext"><br>' : (tmpCatID!="-999" ? '<label for="F'+ansNum+'_'+m+'">$'+preValue+'</label>' : '<label for="F'+ansNum+'_'+m+'">'+preValue+'</label>');
														if (withSpace == '1'){
															if (this.mode==1) {
																//txtStr+=(strLen<60) ? ' &nbsp; ' : '<br>';
																//txtStr+=(m==txtArr[x][y][1]-1 && strLen<60) ? '<br>' : '';
																txtStr+= '<br>';
															}
														}
                                                }
                                                txtStr += '<input type=hidden name=Qn'+ansNum+' id=Qn'+ansNum+' value="'+selectedValue+'">';
                                                var nPi = (tmpCatID=='-999') ? 1 : 0;
                                                
                                               	txtStr+= '<input type="hidden" name="nonPayItem'+ansNum+'" id="nonPayItem'+ansNum+'" value="'+nPi+'">';
                                                break;
                                        case "3":		// MC Multiple , i.e. checkbox + payment category + amount field
                                        
                                                s_checkArr = null;
						                       	
                                                strLen = (ansNum<=this.counter) ? getStrLen(valueArr[ansNum-1]) : 60; // total str length
                                               
						                        txtStr+=tit+'</td></tr>\n';
						                        txtStr+='<td width="100%" class="tabletext" bgcolor="#EFEFEF">';
                                                if (selArr.length>0) {
                                                        s_checks = selArr[ansNum-1];
                                                        s_checkArr = s_checks.split(",");
                                                }
                                                if(this.mode==0) txtStr += '<table><tr><td>&nbsp;</td><td><u>'+paymentTitle+'</u></td><td><u>'+paymentCategory+'</u></td><td><u>'+paymentAmount+'</u></td></tr><tr>';
                                                for (m=0; m<txtArr[x][y][1]; m++){
                                                        preValue = (ansNum<=this.counter) ? valueArr[ansNum-1][m] : "";
                                                        preValueAmt = (ansNum<=this.counter) ? valueAmtArr[ansNum-1][m] : "";
                                                        //preNonPayItem = (ansNum<=this.counter) ? valueNonPayItemArr[ansNum-1][m] : "";
                                                        //alert(preNonPayItem);

                                                        c_check = "";
                                                        if (s_checkArr!=null) {
                                                                for (var kk=0; kk<s_checkArr.length; kk++) {
                                                                        if (s_checkArr[kk]==m) {
                                                                                c_check = "checked";
                                                                                break;
                                                                        }
                                                                }
                                                        }
                                                        
                                                        tmpAmt = (valueAmtArr.length>ansNum || valueAmtArr[ansNum-1]!=undefined) ? valueAmtArr[ansNum-1][m] : "";
                                                        if(tmpAmt==undefined) tmpAmt = "";
														
                                                        if(this.mode==0) txtStr += '<td>';
                                                        txtStr+= '<input type="checkbox" '+c_check+' name="F'+ansNum+'" value="'+m+'" id="F' + ansNum + '_' + m + '"';
                                                        if(this.mode==1) {
	                                                        txtStr+=' data-amount="'+tmpAmt+'" onClick="if(this.checked) {updateAmountTotal('+ansNum+', 0, '+tmpAmt+', 3);} else {updateAmountTotal('+ansNum+', 0, -'+tmpAmt+', 3);}"';
                                                        }
                                                        txtStr+= '> ';
                                                        if(this.mode==0) txtStr += '</td><td>';
														txtStr += '<label for="F' + ansNum + '_' + m + '">';
                                                        if(this.mode==1) txtStr += '<font color="red">*</font>';
                                                        txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue+'" name="FD'+ansNum+'_'+m+'" size="25" class="tabletext">' : preValue;
                                                        var tmpCat = "";
                                                        if(this.mode==0)
														{
															selection = '<SELECT name="CatID'+ansNum+'_'+m+'" id="CatID'+ansNum+'_'+m+'" onChange="if(this.value==-999) document.getElementById(\'nonPayItem'+ansNum+'_'+m+'\').value=1; else document.getElementById(\'nonPayItem'+ansNum+'_'+m+'\').value=0;">'+paymentOptions;
															for(k=0; k<paymentOptIDAry.length; k++) {
																tmpCat = (valueCatArr.length>ansNum || valueCatArr[ansNum-1]!=undefined) ? valueCatArr[ansNum-1][m] : "";
																// Change by Ronald
																selectFlag = (paymentOptIDAry[k]==tmpCat) ? " selected" : "";
																selection += '<option value="'+paymentOptIDAry[k]+'" '+selectFlag+'>'+paymentOptNameAry[k]+'</option>';	
															}
															//var nonPaymentSelected = (tmpCat=="-999") ? " selected" : "";
															//selection += '<option value="-999"'+nonPaymentSelected+'>'+nonPaymentItem+'</option>';													
															selection += '</SELECT>';
			
															txtStr+='</td><td>'+selection+'</td>';
														}
                                                       	if(this.mode == 0)
														{
                                                        	// Change by Ronald
                                                        	txtStr+=' <td>$<input type="text" name="amt'+ansNum+'_'+m+'" id="amt'+ansNum+'_'+m+'" value="'+tmpAmt+'" size="10"></td></tr>';
                                                        } else {
	                                                     	txtStr += ' ($'+tmpAmt+')';   
                                                        }
														if(withSpace == '1'){
														    if (this.mode==1) {
																//txtStr+=(strLen<60) ? ' &nbsp; ' : '<br>';
																//txtStr+=(m==txtArr[x][y][1]-1 && strLen<60) ? '<br>' : '';
																txtStr+= '<br>';
															}
														}
                                                        var tmpCatID = "";
                                               			var nPi = (tmpCat=='-999') ? 1 : 0;
                                               			txtStr+= '<input type="hidden" name="nonPayItem'+ansNum+'_'+m+'" id="nonPayItem'+ansNum+'_'+m+'" value="'+nPi+'">';
														txtStr += '</label>';
												}
												
                                                if(this.mode==0) txtStr += '</table>';
                                                tmpPaymentItemID = (valueItemIDArr[ansNum-1]!=undefined) ? valueItemIDArr[ansNum-1] : "";
                                                txtStr+=' <input type="hidden" name="paymentItemID'+ansNum+'" id="paymentItemID'+ansNum+'" value="'+tmpPaymentItemID+'">';
                                                txtStr+=' <input type="hidden" name="thisItemAmount'+ansNum+'" id="thisItemAmount'+ansNum+'" value="0" >';
                                                //var tmpCatID = "";
                                                //var nPi = (tmpCat=='-999') ? 1 : 0;
                                               	//txtStr+= '<input type="hidden" name="nonPayItem'+ansNum+'" id="nonPayItem'+ansNum+'" value="'+nPi+'">';
                                                break;
                                                
												
                                        case "9":		// MC Single, i.e. radio + payment category + amount field
                                        
                                                r_check_i = (selArr.length>0) ? parseInt(selArr[ansNum-1]) : "";
                                                
                                                strLen = (ansNum<=this.counter) ? getStrLen(valueArr[ansNum-1]) : 60; // total str length
                                                
                                                txtStr+=tit+'</td></tr>\n';
                                                txtStr+='<td width="100%" class="tabletext" bgcolor="#EFEFEF">';
					
                                                if(this.mode==0) txtStr += '<table><tr><td>&nbsp;</td><td><u>'+paymentTitle+'</u></td><td><u>'+paymentCategory+'</u></td><td><u>'+paymentAmount+'</u></td></tr>';
                                                
                                                selectedValue = "";
                                                for (m=0; m<txtArr[x][y][1]; m++){
	                                                
                                                        preValue = (ansNum<=this.counter) ? valueArr[ansNum-1][m] : "";
                                                        tmpAmt = (valueAmtArr[ansNum-1]!=undefined) ? valueAmtArr[ansNum-1][m] : "";
                                                        if(tmpAmt==undefined) tmpAmt = "";
                                                        
                                                        if(this.mode==0) txtStr += '<tr><td>';
                                                        if (r_check_i=='' && r_check_i!='0')
                                                        {
                                                            txtStr+='<input type="radio" value="'+m+'" name="F'+ansNum+'" id="F' + ansNum + '_' + m + '" ';
                                                            if(this.mode==1)
                                                            	txtStr+=' data-amount="'+tmpAmt+'" onClick="updateAmountTotal('+ansNum+', document.getElementById(\'Qn'+ansNum+'\').value, \''+tmpAmt+'\', 9);document.getElementById(\'Qn'+ansNum+'\').value='+tmpAmt+'"';
                                                            else 
                                                            	txtStr+='';
                                                            txtStr+='>';
                                                        }
                                                        else
                                                        {
                                                            //txtStr+=(r_check_i==m) ? '<input type="radio" value="'+m+'" checked name="F'+ansNum+'">' : '<input type="radio" value="'+m+'" name="F'+ansNum+'">';
                                                            if(r_check_i==m) {
	                                                            txtStr += '<input type="radio" value="'+m+'" checked name="F'+ansNum+'" id="F' + ansNum + '_' + m + '" ';
	                                                            if(this.mode==1) txtStr+=' data-amount="'+tmpAmt+'" onClick="updateAmountTotal('+ansNum+', document.getElementById(\'Qn'+ansNum+'\').value, '+tmpAmt+', 9);document.getElementById(\'Qn'+ansNum+'\').value='+tmpAmt+'"';
	                                                            else txtStr+='';
	                                                            txtStr += '>';
	                                                            selectedValue = tmpAmt;
                                                            } else {
	                                                            txtStr += '<input type="radio" value="'+m+'" name="F'+ansNum+'" id="F' + ansNum + '_' + m + '" ';
	                                                            if(this.mode==1) txtStr+=' data-amount="'+tmpAmt+'" onClick="updateAmountTotal('+ansNum+', document.getElementById(\'Qn'+ansNum+'\').value, '+tmpAmt+', 9);document.getElementById(\'Qn'+ansNum+'\').value='+tmpAmt+'"';
	                                                            else txtStr+='';
	                                                            txtStr += '>';
                                                            }
                                                            
                                                        }
                                                        if(this.mode==0) txtStr += '</td><td>';
                                                        
														txtStr += '<label for="F' + ansNum + '_' + m + '">';
														
                                                        if(this.mode==1) txtStr += '<font color="red">*</font>';
                                                        txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue+'" name="FD'+ansNum+'_'+m+'" size="25" class="tabletext"></td>' : preValue;
                                                        //txtStr+=' <SELECT name="CatID'+ansNum+'" id="CatID'+ansNum+'_'+m+'" id="CatID'+ansNum+'_'+m+'" id="CatID'+ansNum+'">'+ paymentOptions +'</SELECT>'+valueCatArr[ansNum-1][m];
                                                        
                                                        if(this.mode==0)
														{
															selection = '<SELECT name="CatID'+ansNum+'_'+m+'" id="CatID'+ansNum+'_'+m+'">'+paymentOptions;
															for(k=0; k<paymentOptIDAry.length; k++) {
																tmpCat = (valueCatArr[ansNum-1]!=undefined && valueCatArr[ansNum-1][m]!='') ? valueCatArr[ansNum-1][m] : "";
																selectFlag = (paymentOptIDAry[k]==tmpCat) ? " selected" : "";
																selection += '<option value="'+paymentOptIDAry[k]+'" '+selectFlag+'>'+paymentOptNameAry[k]+'</option>';	
															}
															//var nonPaymentSelected = (tmpCat=="-999") ? " selected" : "";
															//selection += '<option value="-999"'+nonPaymentSelected+'>'+nonPaymentItem+'</option>';													
															selection += '</SELECT>';
															txtStr+='<td>'+selection+'</td>';
														}
														
														if(this.mode == 0) {
                                                        	txtStr+=' <td>$<input type="text" name="amt'+ansNum+'_'+m+'" id="amt'+ansNum+'_'+m+'" value="'+tmpAmt+'" size="10"></td></tr>';
                                                        } else {
	                                                        txtStr += ' ($'+tmpAmt+')';
                                                        }
                                                        if(withSpace =='1'){
															if (this.mode==1) {
																//txtStr+=(strLen<60) ? ' &nbsp; ' : '<br>';
																//txtStr+=(m==txtArr[x][y][1]-1 && strLen<60) ? '<br>' : '';
																txtStr+= '<br>';
															}
														}
														txtStr += '</label>';
                                                }
                                                txtStr += '<input type=hidden name=Qn'+ansNum+' id=Qn'+ansNum+' value="'+selectedValue+'">';
                                                 
                                                if(this.mode==0) txtStr += '</table>';
                                                //txtStr+=' <input type="hidden" name="paymentItemID'+ansNum+'" id="paymentItemID'+ansNum+'" value="'+paymentItemID+'">';
                                                //tmpPaymentItemID = (paymentIDAry[ansNum-1]!=undefined) ? paymentIDAry[ansNum-1] : "";
                                                tmpPaymentItemID = (valueItemIDArr[ansNum-1]!=undefined) ? valueItemIDArr[ansNum-1] : "";
                                                txtStr+=' <input type="hidden" name="paymentItemID'+ansNum+'" id="paymentItemID'+ansNum+'" value="'+tmpPaymentItemID+'">';
                                                txtStr+=' <input type="hidden" name="thisItemAmount'+ansNum+'" id="thisItemAmount'+ansNum+'" value="0" >';
                                                var nPi = (tmpCat=='-999') ? 1 : 0;
                                               	txtStr+= '<input type="hidden" name="nonPayItem'+ansNum+'" id="nonPayItem'+ansNum+'" value="'+nPi+'">';
                                                break;
                                        case "10": // textarea (long)
                                       			t_filled = (selArr.length>0) ? selArr[ansNum-1] : "";
                                                txtStr+=tit+'<br><textarea name="F'+ansNum+'" cols="50" rows="3">'+recurReplace("<br>", "\n", t_filled)+'</textarea>';
                                                break;
                                        case "12":	// Must Pay Item
                                                preValue0 = (ansNum<=this.counter) ? valueArr[ansNum-1][0] : "";
                                                preValueAmt = (ansNum<=this.counter) ? valueAmtArr[ansNum-1][0] : "";
                                                
                                                r_check0 = "";
                                                r_check1 = "";
												tmpAmt = "";
                                                if (selArr.length>0) {
                                                    eval("r_check"+selArr[ansNum-1]+"='checked'");
                                                    tmpAmt = (selArr[ansNum-1]==1) ? "" : preValueAmt;
                                                }
												strLen = (ansNum<=this.counter) ? getStrLen(valueArr[ansNum-1]) : 40; // total str length
												
												if(this.mode==0) {
													txtStr += '<table><tr><td><u>'+paymentTitle+'</u></td><td><u>'+paymentCategory+'</u></td><td><u>'+paymentAmount+'</u></td></tr>';
													txtStr += '<tr><td>';
												}
												
												tmpCatID = (valueCatArr[ansNum-1]!=undefined) ? valueCatArr[ansNum-1][0] : "";
												tmpCatID = Trim(tmpCatID);
												
												if(this.mode==1 && tmpCatID!="-999") txtStr += '<font color="red">*</font>';
												txtStr += (this.mode==1) ? tit : '<textarea cols=20 rows=2 name="Q'+ansNum+'" id="Q'+ansNum+'">'+tit+'</textarea>';
												
												if(this.mode==0)
												{
													selection = '</td><td><SELECT name="CatID'+ansNum+'_0" id="CatID'+ansNum+'_0" onChange="if(this.value==-999) document.getElementById(\'nonPayItem'+ansNum+'\').value=1; else document.getElementById(\'nonPayItem'+ansNum+'\').value=0;">'+paymentOptions;
													for(k=0; k<paymentOptIDAry.length; k++) {
														//tmpCatID = (valueCatArr[ansNum-1]!=undefined) ? valueCatArr[ansNum-1][0] : "";
														selectFlag = (paymentOptIDAry[k]==tmpCatID) ? " selected" : "";
														selection += '<option value="'+paymentOptIDAry[k]+'" '+selectFlag+'>'+paymentOptNameAry[k]+'</option>';	
													}
													selection += '</SELECT>';
													txtStr+=selection;
													txtStr += '</td>';
												}
						                        
						                        if(this.mode == 0) {
						                        	txtStr+=' <td>$<input type="text" name="amt'+ansNum+'_0" id="amt'+ansNum+'_0" value="'+((valueAmtArr[ansNum-1]!=undefined)?valueAmtArr[ansNum-1]:"")+'"></td>';
						                        	txtStr += '<tr></table>';
						                        } else {
							                     	//txtStr += " ($"+((valueAmtArr[ansNum-1]!=undefined)?valueAmtArr[ansNum-1]:"")+")";
							                     	txtStr += (tmpCatID!="-999") ? " ($"+((valueAmtArr[ansNum-1]!=undefined)?valueAmtArr[ansNum-1]:"")+")" : "";
						                        }
						                        txtStr+=' <input type="hidden" name="paymentItemID'+ansNum+'" id="paymentItemID'+ansNum+'" ';
						                        txtStr+=' value="'+valueItemIDArr[ansNum-1]+'">';
						                        txtStr+=' <input type="hidden" name="thisItemAmount'+ansNum+'" id="thisItemAmount'+ansNum+'" value="0">';
						                        txtStr+='</td></tr>\n';
												txtStr+='<td width="100%" class="tabletext" bgcolor="#EFEFEF">';
												
												if(this.mode==0) txtStr += '<u>'+paymentDescritption + '</u> : <br>';
                                                txtStr+='<input type="radio" value="0" '+r_check0+' name="F'+ansNum+'" id="FF0'+ansNum+'"';
                                                if(this.mode==1 && tmpCatID!="-999") txtStr+=' data-amount="'+((valueAmtArr[ansNum-1]!=undefined)?valueAmtArr[ansNum-1]:0)+'" onClick="updateAmountTotal('+ansNum+', document.getElementById(\'Qn'+ansNum+'\').value, '+((valueAmtArr[ansNum-1]!=undefined)?valueAmtArr[ansNum-1]:0)+', 12);document.getElementById(\'Qn'+ansNum+'\').value='+((valueAmtArr[ansNum-1]!=undefined)?valueAmtArr[ansNum-1]:0)+'"';
                                                txtStr+='>';
												txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue0+'" name="FD'+ansNum+'_0" size="25" class="tabletext"> ('+answersheet_mustPay+')' : '<label for="FF0'+ansNum+'">'+preValue0+'</label>';
												
                                                txtStr+= (this.mode==1) ? '<input type="hidden" name="Qn'+ansNum+'" id="Qn'+ansNum+'" value="'+tmpAmt+'">' : '';
                                                var nPi = (tmpCatID=='-999') ? 1 : 0;
                                               	txtStr+= '<input type="hidden" name="nonPayItem'+ansNum+'" id="nonPayItem'+ansNum+'" value="'+nPi+'">';
                                                
                                                break;  
                                }
                                tArr=txtArr[x][y];
                                txtStr+= (queArr[0]=="2" || queArr[0]=="3" || queArr[0]=="6") ? '</td>' : '<br></td>';
                                ansNum++;
                        }
                        txtStr+='</table>\n';
                        txtStr+='</td></tr>\n';
                }
                
                txtStr+='</td></tr></table>';
                if(this.mode==1) txtStr += '<br><span class="tabletextremark"><font color="red">*</font> '+paymentItemName+'</span>';
                if(this.mode==1){
                	if(typeof(totalAmountToPay) !== 'undefined' && totalAmountToPay && totalAmountToPay != ''){
                		txtStr += '<p><span class="tabletext">'+totalAmountToPay+'</span></p>';
                	}
	                txtStr += '<p><span class="tabletext">'+accountBalance+'</span></p>';
	            }
                txtStr+='</td></tr></table>';
                this.counter = ansNum-1;
                txtStr+='</Form>\n';
                
                return txtStr;
        }

        

/* internal function for conversion and operations */
        function pushUp(arr){
                for (x=0; x<arr.length-1; x++){
                        arr[x]=arr[x+1];
                }
                arr.length=arr.length-1;
                return arr;
        }


        //initialization
        function sheetArr(){
                var txtStr = this.qString;
                var txtArr = txtStr.split("#QUE#");
                var tmpArr = null;
                var resultArr = new Array();
                var valueTemp = new Array();
                var catIDTemp = new Array();
                var amtTemp = new Array();
                var itemIDTemp = new Array();
                var nonPayItemTemp = new Array();

                txtArr=pushUp(txtArr);
                this.counter = txtArr.length;
                for (var x=0; x<txtArr.length; x++){
                        tmpArr = txtArr[x].split("||");
	                    //if(tmpArr.length!=0) {
	                        type_no = tmpArr[0].split(",");
	                        question = tmpArr[1];
	                        opts = tmpArr[2];
	                        itemIDs = tmpArr[3];
	                        catIDs = tmpArr[4];
	                        amts = tmpArr[5];
	                        //nonPayItems = tmpArr[6];
	                        
	                    /*} else {
	                        type_no = "";
	                        question = "";
	                        opts = "";
	                        itemIDs = "";
	                        catIDs = "";
	                        amts = "";
	                    }*/
                        
                        var j = x;

                        // question
                        resultArr[j] = new Array();
                        resultArr[j][0] = recurReplace("<br>", "\n", question);
                        resultArr[j][1] = new Array();
                        resultArr[j][1][0] = type_no[0];
                        
                        if(type_no[0]=="10") {
                        	continue;
                        } else {
                        
	                        if (type_no.length>1) {
	                            resultArr[j][1][1] = type_no[1];
	                        }
	
	                        // option values
	                        valueTemp[j] = new Array();
	                        tmpArr = opts.split("#OPT#");
	                        //alert(tmpArr);
	                        if (tmpArr.length>1) {
	                                for (var m=1; m<tmpArr.length; m++){
	                                        valueTemp[j][m-1] = tmpArr[m];
	                                }
	                        }
	                        // payment category ID values
	                        catIDTemp[j] = new Array();
	                        tmpArr = catIDs.split("#CATEGORYID#");
	                        //alert(tmpArr[0]+'/'+tmpArr[1]);
	                        if (tmpArr.length>1) {
	                                for (var m=1; m<tmpArr.length; m++){
	                                        catIDTemp[j][m-1] = tmpArr[m];
	                                        //alert(j+'/'+(m-1)+'/'+tmpArr[m]);
	                                }
	                        }
	                        
							
	                        // amount values
	                        amtTemp[j] = new Array();
	                        if(amts!=undefined) {		// some reply slip format maybe not have "amount" option
		                        tmpArr = amts.split("#AMT#");
		                        if (tmpArr.length>1) {
		                                for (var m=1; m<tmpArr.length; m++){
		                                        amtTemp[j][m-1] = tmpArr[m];
		                                }
		                        }
		                    } else {
			                 	amtTemp[j][0] = "";   
		                    }
		                    
	                        // Payment Item ID values
	                        itemIDTemp[j] = new Array();
	                        //if(amts!=undefined) {		// some reply slip format maybe not have "amount" option
		                        tmpArr = itemIDs.split("#PAYMENTITEMID#");
		                        if (tmpArr.length>1) {
		                                for (var m=1; m<tmpArr.length; m++){
		                                	itemIDTemp[j][m-1] = tmpArr[m];
		                                	//alert(tmpArr[m]);
		                                }
		                        }
		                    //} else {
		                    //	itemIDTemp[j][0] = "";   
		                    //}
		                }

                }

                if (this.mode==1 && this.aString!=null) {
                    this.stuffAns();
                }

                this.valueTmp = valueTemp;
                this.catIDTemp = catIDTemp;
                this.amtTemp = amtTemp;
                this.itemIDTemp = itemIDTemp;
                
                return resultArr;
          }


        function stuffAns(){
                var ansStr=this.aString;
                ansArr=ansStr.split("#ANS#");
                this.selects = pushUp(ansArr);
          }


        function move(i, dir){
                var arr = this.answer;
                var temArr = new Array();

                retainValues();
                var valueArr = this.valueTmp;
                var catIDArr = this.catIDTemp;
                var amtArr = this.amtTemp;
                var itemIDArr = this.itemIDTemp;
                var nonPayItemArr = this.nonPayItemTemp;
                var tvArr = new Array();

                switch (dir){
                        case "up":
                                if (i!=0 && i<arr.length){
                                        //swap question
                                        temArr = arr[i];
                                        arr[i] = arr[i-1];
                                        arr[i-1] = temArr;

                                        //swap current fill-in-value
                                        temArr = valueArr[i];
                                        valueArr[i] = valueArr[i-1];
                                        valueArr[i-1] = temArr;
                                        temArr = catIDArr[i];
                                        catIDArr[i] = catIDArr[i-1];
                                        catIDArr[i-1] = temArr;
                                        temArr = amtArr[i];
                                        amtArr[i] = amtArr[i-1];
                                        amtArr[i-1] = temArr;
                                        temArr = itemIDArr[i];
                                        itemIDArr[i] = itemIDArr[i-1];
                                        itemIDArr[i-1] = temArr;
                                        /*
                                        temArr = nonPayItemArr[i];
                                        nonPayItemArr[i] = nonPayItemArr[i-1];
                                        nonPayItemArr[i-1] = temArr;
                                        */
                                };
                                break;
                        case "down":
                                if (i!=(arr.length-1)&&i>=0){
                                        temArr = arr[i];
                                        arr[i] = arr[i+1];
                                        arr[i+1] = temArr;

                                        temArr = valueArr[i];
                                        valueArr[i] = valueArr[i+1];
                                        valueArr[i+1] = temArr;
                                        temArr = catIDArr[i];
                                        catIDArr[i] = catIDArr[i+1];
                                        catIDArr[i+1] = temArr;
                                        temArr = amtArr[i];
                                        amtArr[i] = amtArr[i+1];
                                        amtArr[i+1] = temArr;
                                        temArr = itemIDArr[i];
                                        itemIDArr[i] = itemIDArr[i+1];
                                        itemIDArr[i+1] = temArr;
                                        /*
                                        temArr = nonPayItemArr[i];
                                        nonPayItemArr[i] = nonPayItemArr[i+1];
                                        nonPayItemArr[i+1] = temArr;  
                                        */
                                };
                                break;
                        case "out":
								if(arr.length>0)
								{
									for (x=i; x<arr.length; x++) {
											arr[x]=arr[x+1];
											valueArr[x]=valueArr[x+1];
											catIDArr[x]=catIDArr[x+1];
											amtArr[x]=amtArr[x+1];
											itemIDArr[x]=itemIDArr[x+1];
									}
									arr.length = arr.length-1;
									valueArr.length = valueArr.length-1;
									catIDArr.length = catIDArr.length-1;
									amtArr.length = amtArr.length-1;
									itemIDArr.length = itemIDArr.length-1;
									//nonPayItemArr.length = nonPayItemArr.length-1;
								}
                                break;
                }
                this.answer = arr;
                this.valueTmp = valueArr;
                this.catIDTemp = catIDArr;
                this.amtTemp = amtArr;
                this.itemIDTemp = itemIDArr;
                //this.nonPayItemTemp = nonPayItemArr;
        }


        function secDes(i){
                var arr=this.answer;

				if(typeof(arr[i])!="undefined")
				{    
					var form_pop_up = window.open("", "form_pop_up", "toolbar=no,location=no,status=no,menubar=no,resizable,width=400,height=200,top=100,left=100");

					var JSfunction = "<script language='javascript'>";
					JSfunction += "function updateDesc(ind, fobj) {";
					JSfunction += "var arrD = window.opener.sheet.answer;";
					JSfunction += "arrD[ind][0] = fobj.new_desc.value;";
					JSfunction += "window.opener.sheet.answer = arrD;";
					JSfunction += "window.opener.retainValues();";
					JSfunction += "window.opener.writetolayer('blockInput',window.opener.sheet.writeSheet());";
					JSfunction += "self.close();";
					JSfunction += "} </script>";
				
					var desc_form = "<HTML><head><title>"+chg_title+"</title></head><body bgcolor='#EFECE7'><form name='form1'>";
					desc_form += "<br>"+JSfunction+"<table border='0' align='center'>";
					desc_form += "<tr><td><textarea name='new_desc' rows='5' cols='40'>"+arr[i][0]+"</textarea></td></tr>";
					desc_form += "<tr><td align='right'><input type='button' onClick='updateDesc("+i+", this.form);' value='"+button_update+"'> <input type='button' onClick='self.close()' value='"+button_cancel+"'></td></tr>";
					desc_form += "</table></form></body></HTML>";
					form_pop_up.document.write(desc_form);
				}

                return;
        }


        function chgNum(i){
                arr=this.answer;
                num=arr[i].length-1;
                var changeNum = prompt ("Change the number of questions from "+num+" to:","");
                if (changeNum){
                    if (isInteger(changeNum) && changeNum>0){
                            if (changeNum>num){
                                    for (n=num; n<changeNum; n++)
                                             arr[i][arr[i].length]=arr[i][arr[i].length-1];
                            }
                            if (changeNum<num){
                                    diff=num-changeNum;
                                    for (n=0; n<diff; n++)
                                        arr[i].length--;
                            }
                     }
                     else
                             alert("your input is not a valid number");
        		}
        }

}
/*
function changeCatItem() {
	return sheet.changeCategory;
}
*/
function editPanel(withSpace){
		withSpace = withSpace || '1';
        answer = "";
        if (sheet.mode==0) {
                answer += '<DIV ID="blockDiv">\n'
                +'<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">\n'
				+'<tr><td>'+(typeof(Part1)!='undefined'?Part1:'&nbsp;')+'</td></tr>\n'
                +'<tr><td><table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">'
                +'<form name="addForm">\n'

                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_template+"</span></td>"
                +'<td class="tabletext">'+getTemplate()+'</td></tr>\n'
                
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_header+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <textarea name="secDesc" class="tabletext" rows="3" cols="30"></textarea></td></tr>\n'
                
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_type+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <select name="qType" onchange="if (this.selectedIndex==3||this.selectedIndex==4||this.selectedIndex==5) this.form.oNum.selectedIndex=1; else this.form.oNum.selectedIndex=0">\n'
                +'                <option>- '+answersheet_type+' -\n'
                +'				  <option value="12">'+answersheet_mustPay+'\n'
                +'                <option value="1">'+answersheet_toPay+'\n'
                +'                <option value="2">'+answersheet_onePayOneCat+'\n'
                +'                <option value="9">'+answersheet_onePayFewCat+'\n'
                +'                <option value="3">'+answersheet_fewPayFewCat+'\n'
                +'                <option value="10">'+answersheet_sq2+'\n'
                +'                </select>\n'
                +'</td></tr>\n'
                
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_NoOfOption+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <select name="oNum" onchange="if (this.form.qType.selectedIndex!=3&&this.form.qType.selectedIndex!=4&&this.form.qType.selectedIndex!=5){alert(\''+no_options_for+'\'); this.selectedIndex=0;}">'
                +'                <option>- '+answersheet_option+' -\n';
                
                // [20090310 yat]
                for(qi=2;qi<=sheet.ansQty;qi++)
                	answer +='                <option value='+qi+'>'+qi+'\n';
                	
                answer +='  </select>\n'
                +'</td></tr></table></td></tr>\n'
                +'<tr><td>'
				+ '<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center"><tr><td class="dotline" colspan="2">'+(typeof(space10)!='undefined'?space10:'&nbsp;')+'</td></tr></table>'
                + '</td></tr>'
				+ '<tr><td colspan="2" align="center">'+(typeof(add_btn)!='undefined'?add_btn:'&nbsp;')+'</td></tr>\n'
                + '        </td></tr>\n'
                +'        </form></table>\n'
                +'</DIV>\n';
        }

        answer += '<DIV ID="blockInput"'
        +'        STYLE="background-color: transparent; '
        +'                width:100%; visibility:visible;">\n'
        +'<script'
        +' language="javascript">\n'
        +' document.write(sheet.writeSheet("'+withSpace+'"));\n'
        +'</script'
        +'>\n'
        +'</DIV>\n';

        return answer;
}
/*
// Added by Kelvin Ho on 10 Feb 09.
function editPaneleDiscipline(){
        answer = "";
        if (sheet.mode==0) {
                answer += '<DIV ID="blockDiv">\n'
                +'<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">\n'
				+'<tr><td>'+(typeof(Part1)!='undefined'?Part1:'&nbsp;')+'</td></tr>\n'
                +'<tr><td><table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">'
                +'<form name="addForm">\n'

                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_template+"</span></td>"
                +'<td class="tabletext">'+getTemplate()+'</td></tr>\n'
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_header+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <textarea name="secDesc" class="tabletext" rows="3" cols="30"></textarea></td></tr>\n'
                
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_type+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <select name="qType" onchange="if (this.selectedIndex==2||this.selectedIndex==3) this.form.oNum.selectedIndex=3; else this.form.oNum.selectedIndex=0">\n'
                +'                <option>- '+answersheet_type_selection+' -\n'
                +'                <option>'+answersheet_tf+'\n'
                +'                <option>'+answersheet_mc+'\n'
                +'                <option>'+answersheet_mo+'\n'
                +'                <option>'+answersheet_mc2+'\n'
                +'                </select>\n'
                +'</td></tr>\n'
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_option+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <select name="oNum" onchange="if (this.form.qType.selectedIndex!=2&&this.form.qType.selectedIndex!=3){alert(\''+no_options_for+'\'); this.selectedIndex=0;}">'
                +'                <option>- '+answersheet_selection+' -\n';
                
                // [20090310 yat]
                for(qi=3;qi<=sheet.ansQty;qi++)
                	answer +='                <option value='+qi+'>'+qi+'\n';
                	
                
                answer +='  </select>\n'
                +'</td></tr></table></td></tr>\n'
                +'<tr><td>'
				+ '<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center"><tr><td class="dotline" colspan="2">'+(typeof(space10)!='undefined'?space10:'&nbsp;')+'</td></tr></table>'
                + '</td></tr>'
				+ '<tr><td colspan="2" align="center">'+(typeof(add_btn)!='undefined'?add_btn:'&nbsp;')+'</td></tr>\n'
                + '        </td></tr>\n'
                +'        </form></table>\n'
                +'</DIV>\n';
        }

        answer += '<DIV ID="blockInput"'
        +'        STYLE="background-color: transparent; '
        +'                width:100%; visibility:visible;">\n'
        +'<script'
        +' language="javascript">\n'
        +' document.write(sheet.writeSheet());\n'
        +'</script'
        +'>\n'
        +'</DIV>\n';

        return answer;
}
*/


function getTemplate() {
        var tmpArr = sheet.templates;
    var xStr = '<select name="fTemplate" onchange="if (confirm(chg_template)) {changeTemplate(this.form.fTemplate.selectedIndex); this.form.secDesc.focus();} else {this.form.fTemplate.selectedIndex=sheet.templateNo;}">';

        xStr += '<option value="">-'+answersheet_template+'-</option>\n';
        for (var i=0; i<tmpArr.length; i++) {
            xStr += '<option value="'+i+'">'+tmpArr[i][0]+'</option>\n';
        }
        xStr += '</select>';

        return xStr;
}


function changeTemplate(index) {
        if (index!=0) {
                sheet.qString = sheet.templates[index-1][1];
                sheet.answer = sheet.sheetArr();
        } else {
            sheet.qString = "";
                //sheet.answer = "";
				 sheet.answer = sheet.sheetArr();
        }
        sheet.templateNo = index;
        writetolayer("blockInput",sheet.writeSheet());
}


function appendTxt(formName){
        var formObj=eval("document."+formName);
        var arr=new Array();
        arr[0]=formObj.secDesc.value;
		
        /* check empty topic / title */
        if(formObj.secDesc.value=='')
        {
	        alert(pls_fill_in_title);
			formObj.secDesc.focus();
			return false;
        }
        
        /* check empty type */
        qtype=String(formObj.qType.selectedIndex);
        if (qtype=="0"){
            alert(pls_specify_type);
            formObj.qType.focus();
            return false;
        }
		
        qtype = String(formObj.qType.value);
        for (x=0; x<1; x++)
        {
            y=x+1;
            arr[y]=new Array();
            arr[y][0]=qtype;
            if (qtype=="2" || qtype=="3" || qtype=="9")
            {
		        /* check empty option number */
            	var optNum=String(formObj.oNum.selectedIndex);
		        if (optNum=="0")
		        {
		            alert(pls_specify_option_num);
		            formObj.oNum.focus();
		            return false;
		        }
            	
            	arr[y][1]=String(formObj.oNum.options[formObj.oNum.selectedIndex].value);
            }
        }
        sheet.answer[sheet.answer.length]=arr;
		
        return true;
}

if (need2checkform==undefined)
{
    var need2checkform = false;
}

function getAns(i){
        var eleObj = eval("document.answersheet.F"+i);
        var strAns = null;
        var tempAns = null;

        if (eleObj==undefined)
        {
            return '';
        }

        if (eleObj.length){
                switch(eleObj[0].type){
                        case "radio":
                                for (p=0; p<eleObj.length; p++){
                                        if (eleObj[p].checked)
                                        {
                                                tempAns += p;
                                        }
                                }
                                if (need2checkform && tempAns == null )
                                {
                                    formAllFilled = false;
                                    return false;
                                }
                                if (tempAns != null)
                                {
                                    strAns += tempAns;
                                }
                                break;

                        case "checkbox":
                                for (p=0; p<eleObj.length; p++) {
                                        if (eleObj[p].checked)
                                        {
                                            if (tempAns != null)
                                            {
                                                tempAns += ","+p;
                                            }
                                            else
                                            {
                                                tempAns = p;
                                            }
                                        }
                                }
                                if (need2checkform && tempAns == null)
                                {
                                    formAllFilled = false;
                                    return false;
                                }
                                strAns = tempAns;
                                break;

                        case "text":
                                for (p=0; p<eleObj.length; p++){
                                        tempAns += recurReplace('"', '&quot;', eleObj.value);
                                }
                                if (need2checkform && (tempAns == null || tempAns==''))
                                {
                                    formAllFilled = false;
                                    return false;
                                }
                                strAns += tempAns;
                                break;
                }
        } else
        {
        	if(eleObj.type!=undefined && eleObj.type=="radio")
        	{
        		if(eleObj.checked) {
        			tempAns = "0";
        			strAns = tempAns;
        		}
        		else if (need2checkform) {
        			formAllFilled = false;
                    return false;
        		}
        	}
        	else
        	{
                tempAns = recurReplace('"', '&quot;', eleObj.value);
                if (need2checkform && (tempAns == null || tempAns==''))
                {
                    formAllFilled = false;
                    return false;
                }
                strAns = tempAns;
            }
        }
        return strAns;
}


function finish(){
        var txtStr="";
        var ansStr="";
        var arr=sheet.answer;
        var ansNum=1;
        var temp = null;
		var result = true;
        formAllFilled = true;
        
        if (sheet.mode==0) {
	        //var arrD = window.sheet.answer;
	        //arrD[ind][0] = fobj.new_desc.value;
                // get questions
                
                for (var x=0; x<arr.length; x++){
                        //txtStr+="#QUE#"+arr[x][0];
                        for (var y=1; y<arr[x].length; y++){
                                //append options
                                queArr = arr[x][y][0].split(",");
                                switch (queArr[0]){
                                        case "1":        myLen=2;break;
                                        case "4":
                                        case "5":
                                        case "6":        myLen=0;break;
                                        case "10":		txtStr += ("#QUE#"+arr[x][1]+"||"+arr[x][0]);ansNum++; continue;
                                        case "12":      myLen=1;break;
                                        default:		myLen=arr[x][y][1]; break;
                                }
                                txtStr+="#QUE#";
                                for (var z=0; z<arr[x][y].length; z++){
                                        txtStr += (z>0) ? ","+arr[x][y][z] : arr[x][y][z];
                                        //alert(arr[x][y][z]);
                                }
                                if(eval("document.getElementById('Q"+(x+1)+"')")!=undefined) {
	                                if(eval("document.getElementById('Q"+(x+1)+"').value")!="")
	                                	arr[x][0] = eval("document.getElementById('Q"+(x+1)+"').value");
	                                else 
	                                	return "Q"+(x+1);
                            	}
                                txtStr+="||"+arr[x][0]+"||";
                                //alert(eval("document.getElementById('Q"+(x+1)+"').value"));
                                
                                for (var m=0; m<myLen; m++){
                                        optDescription = eval("document.answersheet.FD"+ansNum+"_"+m+".value");
                                        //if(eval("document.answersheet.FD"+ansNum+"_"+m+".value")=="") result = false;
                                        if(eval("document.answersheet.FD"+ansNum+"_"+m+".value")=="") {
	                                        result = "FD"+ansNum+"_"+m;
	                                        break;
                                        }
                                        txtStr+="#OPT#"+optDescription;
                                }
								// add payment category ID and amount to the string
								switch(queArr[0]) {
									case "1":
									case "12":
											txtStr += (eval("document.answersheet.paymentItemID"+ansNum+".value")!='undefined') ? "||#PAYMENTITEMID#"+eval("document.answersheet.paymentItemID"+ansNum+".value")+"||" : "||#PAYMENTITEMID#1||";
											txtStr+='#CATEGORYID#'+eval("document.answersheet.CatID"+ansNum+"_0.value");
											txtStr+='||#AMT#'+eval("document.answersheet.amt"+ansNum+"_0.value");
											//txtStr+='||#NonPaymentItem#'+eval("document.answersheet.nonPayItem"+ansNum+".value");
											
											if(eval("document.answersheet.CatID"+ansNum+"_0.value")=="") {
												result = "CatID"+ansNum+"_0";
												break;
											}
											
											if(eval("document.answersheet.amt"+ansNum+"_0.value")=="" || (isNaN(eval("document.answersheet.amt"+ansNum+"_0.value")) || eval("document.answersheet.amt"+ansNum+"_0.value")<0)) {
												result = "amt"+ansNum+"_0";
												break;
											}
											break;
									case "2":
											txtStr += (eval("document.answersheet.paymentItemID"+ansNum+".value")!='undefined') ? "||#PAYMENTITEMID#"+eval("document.answersheet.paymentItemID"+ansNum+".value")+"||" : "||#PAYMENTITEMID#1||";
											txtStr+='#CATEGORYID#'+eval("document.answersheet.CatID"+ansNum+"_0.value");
											//txtStr+='||#NonPaymentItem#'+eval("document.answersheet.nonPayItem"+ansNum+".value");
											//if(eval("document.answersheet.CatID"+ansNum+"_0.value")=="") result = false;
											if(eval("document.answersheet.CatID"+ansNum+"_0.value")=="") {
												result = "CatID"+ansNum+"_0";
												break;
											}
											break;
									case "3":
											var catString = "";
											var amtString = "";
											var nonPayString = "";
											txtStr += (eval("document.answersheet.paymentItemID"+ansNum+".value")!="") ? "||#PAYMENTITEMID#"+eval("document.answersheet.paymentItemID"+ansNum+".value")+"||" : "||#PAYMENTITEMID#1||";
											for (var m=0; m<myLen; m++){
												catString += '#CATEGORYID#'+eval("document.answersheet.CatID"+ansNum+"_"+m+".value");
												amtString += '#AMT#'+eval("document.answersheet.amt"+ansNum+"_"+m+".value");
												//nonPayString += '#NonPaymentItem#'+eval("document.answersheet.nonPayItem"+ansNum+"_"+m+".value");
												
												//if(eval("document.answersheet.CatID"+ansNum+"_"+m+".value")=="") result = false;
												if(eval("document.answersheet.CatID"+ansNum+"_"+m+".value")=="") {
													result = "CatID"+ansNum+"_"+m;
													break;
												}
												//if(eval("document.answersheet.amt"+ansNum+"_"+m+".value")=="" || (isNaN(eval("document.answersheet.amt"+ansNum+"_"+m+".value")) || eval("document.answersheet.amt"+ansNum+"_"+m+".value")<0)) result = false;
												if(eval("document.answersheet.amt"+ansNum+"_"+m+".value")=="" || (isNaN(eval("document.answersheet.amt"+ansNum+"_"+m+".value")) || eval("document.answersheet.amt"+ansNum+"_"+m+".value")<0)) {
													result = "amt"+ansNum+"_"+m;
													break;
												}
											}
											txtStr+=catString+"||"+amtString+"||"+nonPayString;
											break;
									case "9":
											var catString = "";
											var amtString = "";
											txtStr += (eval("document.answersheet.paymentItemID"+ansNum+".value")!="") ? "||#PAYMENTITEMID#"+eval("document.answersheet.paymentItemID"+ansNum+".value")+"||" : "||#PAYMENTITEMID#1||";
											for (var m=0; m<myLen; m++){
												catString += '#CATEGORYID#'+eval("document.answersheet.CatID"+ansNum+"_"+m+".value");
												amtString += '#AMT#'+eval("document.answersheet.amt"+ansNum+"_"+m+".value");
												//if(eval("document.answersheet.CatID"+ansNum+"_"+m+".value")=="") result = false;
												if(eval("document.answersheet.CatID"+ansNum+"_"+m+".value")=="") {
													result = "CatID"+ansNum+"_"+m;
													break;
												}
												//if(eval("document.answersheet.amt"+ansNum+"_"+m+".value")=="" || (isNaN(eval("document.answersheet.amt"+ansNum+"_"+m+".value")) || eval("document.answersheet.amt"+ansNum+"_"+m+".value")<0)) result = false;
												if(eval("document.answersheet.amt"+ansNum+"_"+m+".value")=="" || (isNaN(eval("document.answersheet.amt"+ansNum+"_"+m+".value")) || eval("document.answersheet.amt"+ansNum+"_"+m+".value")<0)) {
													result = "amt"+ansNum+"_"+m;
													break;
												}
											}
											
											txtStr+=catString+"||"+amtString;
											break;
									case "10":
												txtStr += "";
												break;
												
									default	:	break;
												
								}
								
								
                                ansNum++;
                        }
                }
                document.ansForm.qStr.value=txtStr;
        } else if (sheet.mode==1) {

                // get answers
            for (var x=0; x<arr.length; x++){
                        queArr = arr[x][1][0].split(",");
                        temp = getAns(x+1);
                        if (formAllFilled==false)
                        {
                            return false;
                        }
                        txtStr+=(queArr[0]=="6") ? "#ANS#" : "#ANS#"+temp; //getAns(x+1);
                }
                document.ansForm.aStr.value=txtStr;
        }

        return result;

}


function retainValues(){
                var txtStr="";
                var ansStr="";
                var arr=sheet.answer;
                var ansNum=1;
                var valueTemp = new Array();
                var catIDTemp = new Array();
                var amtTemp = new Array();
                var itemIDTemp = new Array();
                var nonPayItemTemp = new Array();

                for (var x=0; x<arr.length; x++){
                        valueTemp[x] = new Array();
                        amtTemp[x] = new Array();
                        catIDTemp[x] = new Array();
                        itemIDTemp[x] = new Array();
                        //nonPayItemTemp[x] = new Array();
                        
                        //txtStr+="#SEC#"+arr[x][0];
                        for (var y=1; y<arr[x].length; y++){
                                //txtStr+="#QUE#";
                                /*
                                for (var z=0; z<arr[x][y].length; z++){
                                        txtStr+=arr[x][y][z]+"||";
                                }
                                */
                                //append options
                                queArr = arr[x][y][0].split(",");
								//alert(arr[x][y][1]);
                                switch (queArr[0]){
                                        case "1":       myLen = 2; myAmtLen = 1; break;
                                        case "2": 		myLen = arr[x][y][1]; myAmtLen = 1; break;
                                        case "3":		myLen = arr[x][y][1]; myAmtLen = arr[x][y][1]; break;
                                        case "4":
                                        case "5":
                                        case "6":       myLen = 0; break;
                                        case "9":		myLen = arr[x][y][1]; myAmtLen = arr[x][y][1]; break;
                                        case "10":		txtStr += ("#QUE#"+arr[x][1]+"||"+arr[x][0]);ansNum++; continue;
                                        case "12":      myLen = 1; myAmtLen = 1; break;
                                        default:        myLen = arr[x][y][1]; myAmtLen = 0; break;
                                }
                                //alert(queArr[0]+'/'+myLen);
                                for (var m=0; m<myLen; m++){
                                        tmpVal = eval("document.answersheet.FD"+ansNum+"_"+m+".value");
                                        valueTemp[x][m] = recurReplace('"', '&quot;', tmpVal);
                                }
                                for(var n=0; n<myAmtLen; n++) {
                                        amtTemp[x][n] = (eval("document.answersheet.amt"+ansNum+"_"+n+".value")!=undefined) ? eval("document.answersheet.amt"+ansNum+"_"+n+".value") : "";
                                        catIDTemp[x][n] = (eval("document.answersheet.CatID"+ansNum+"_"+n+".value")!=undefined) ? eval("document.answersheet.CatID"+ansNum+"_"+n+".value") : "";
                                        //alert(catIDTemp[x][n]);
                                }
                                itemIDTemp[x] = (eval("document.answersheet.paymentItemID"+ansNum+".value")!=undefined) ? eval("document.answersheet.paymentItemID"+ansNum+".value") : "";
                                //nonPayItemTemp[x] = (eval("document.answersheet.nonPayItem"+ansNum+".value")!=undefined) ? eval("document.answersheet.nonPayItem"+ansNum+".value") : "";
                                ansNum++;
                        }
                }
                sheet.valueTmp = valueTemp;
                sheet.catIDTemp = catIDTemp;
                sheet.amtTemp = amtTemp;
                sheet.itemIDTemp = itemIDTemp;
                //sheet.nonPayItemTemp = nonPayItemTemp;
}


function recurReplace(exp, reby, txt) {
    while (txt.search(exp)!=-1) {
        txt = txt.replace(exp, reby);
    }
        return txt;
}




// the following three functions are added for integer validation



function isInteger(s){
        var i;
    if (isEmpty(s))
                if (isInteger.arguments.length == 1) return false;
                else return (isInteger.arguments[1] == true);
    // Search through string's characters one by one
    // until we find a non-numeric character.
    // When we do, return false; if we don't, return true.
    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number.
        var c = s.charAt(i);
        if (!isDigit(c)) return false;
    }

    // All characters are numbers.
    return true;
}



function isEmpty(s)
{   return ((s == null) || (s.length == 0))
}


function isDigit (c)
{   return ((c >= "0") && (c <= "9"))
}

function getStrLen(tmpArr) {
        var tmpV=0;
        if (tmpArr.length<7) {
                for (var i=0; i<tmpArr.length; i++) {
                        for (var j=0; j<tmpArr[i].length; j++) {
                                tmpV += (tmpArr[i].charCodeAt(j)>1000) ? 2 : 1;
                        }
                }
        } else {
                tmpV = 60;
        }

        return tmpV;
}

function cutStrLen(xStr, xLen) {
        var tmpArr = new Array(xStr);
        var tmpStr = "";
        var xCount = 0;

    if (getStrLen(tmpArr)>xLen) {
                for (var j=0; j<xStr.length; j++) {
                        xCount += (xStr.charCodeAt(j)>1000) ? 2 : 1;
                        tmpStr += xStr.charAt(j);
                        if (xCount>xLen-3) {
                            break;
                        }
                }
        xStr = tmpStr+"...";
    }
        return xStr;
}