function viewForm(txtStr, txtAns){
	var txtArr = txtStr.split("#QUE#");
	ansArr = txtAns.split("#ANS#");
	var tmpArr = null;
	var txtStr = null;
	var lastIndexVal = "8";
	
	var table_style = (typeof(print_report)!="undefined") ? ' width="100%" ' : ' style="border-left:2px #CDCDCD solid; border-top:2px #CDCDCD solid; border-right:2px #CDCDCD solid; border-bottom:2px #CDCDCD solid; " width="95%" ';
	
	txtStr='<Form name="answersheet">';
	txtStr += '<table border="0" align="center" bgcolor="white" '+ table_style +' cellpadding="10" cellspacing="0"><tr><td><table width="100%" border="0" cellpadding="5" cellspacing="0">\n';
	if (typeof(form_description)!="undefined")
	{
		description_html = '<p><table border="0" cellpadding="0" cellspacing="0"><tr><td>'+form_description+'</td></tr></table></p>';
	} else
	{
		description_html = '';
	}
	if (typeof(form_reference)!="undefined")
	{
		description_html += '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td align="right">' + form_reference + '</td></tr></table>';
	}
	if (typeof(form_user_photo)!="undefined")
	{
		description_html += '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td align="center">' + form_user_photo + '</td></tr></table>';
	}
	if (typeof(form_title_main)!="undefined")
	{
		if (form_title_main!="" || description_html!="")
		{
			txtStr += '<tr><td align="center" colspan="2" class="form_title">'+form_title_main+'<br>'+description_html;
			//txtStr += '<hr style="border:dashed 3px #999999;" >';
			txtStr += '<br/></td></tr>';
		}
	}
	
	if (displaymode==2)
	{
		txtStr += '<tr><td colspan="2">' + txtArr[0] + '</td></tr>';
	}
	else
	{
		for (var x=1; x<txtArr.length; x++)
		{
			j = x - 1;
			
			tmpArr = txtArr[x].split("||");
			type_no = tmpArr[0].split(",");			//0:type, 1:number of options
			
			tit = tmpArr[1];
			opts = tmpArr[2];
			
			tmpArr = opts.split("#OPT#");

			if (type_no[0]==lastIndexVal)
			{
				if (x>1)
				{
					txtStr += '<tr><td colspan="2">';
					txtStr += '<hr color="#E0F2BB" size=5>';
					txtStr += '</td></tr>';
				}
				tit = '<span class="16Green">' + tit + '</span>';
				title_align = '';
			} 
			else
			{
				title_align = 'align="right"';
				if(type_no[0]==4 || type_no[0]==5)
					var td_style = "style=\"line-height:150%\"";
			}
			
			if (displaymode==0 || type_no[0]==lastIndexVal)
			{
				 txtStr += '<tr><td colspan=2>\n';
				 txtStr += tit+'</td></tr>\n';
				 txtStr += '<tr><td >&nbsp;</td><td width="90%" '+td_style+'>';
			} else if (displaymode==1)
			{
				 txtStr += (tit!='') ? '<tr><td valign="top" '+title_align+' >'+tit+'</td><td width="80%"><table width="100%" border="0" cellpadding="0" cellspaing="0"><tr>' : '<tr><td valign="top" colspan=2><table width="100%" border="0" cellpadding="0" cellspaing="0"><tr>';
				 txtStr += '<td '+td_style+'>';
			}

			switch (type_no[0])
			{
				case "1":
				case "2":
					if (typeof(ansArr[x])!="undefined" && typeof(tmpArr[parseInt(ansArr[x])+1])!="undefined")
					{
						if(isNaN(parseInt(ansArr[x])))
							txtStr += msg_not_answered;
						else
						{
							if(answer_display_mode == 1)
							{
								txtStr += '<table style="padding-left:20px; border-style:none;"><tr>';
								for(y=1; y<tmpArr.length; y++)
								{
									txtStr += '<td>';
									txtStr += (y == parseInt(ansArr[x])+1) ? '<strong>'+tmpArr[y]+'</strong>' : tmpArr[y];
									txtStr += '</td>';
								}
								txtStr += '</tr></table>';
							}
							else
								txtStr += tmpArr[parseInt(ansArr[x])+1];
						}
					} else
					{
						txtStr += msg_not_answered;
					}
					break;
				case "3":
					checkAns = (typeof(ansArr[x])!="undefined") ? ansArr[x].split(",") : null;
					c_i_i = 0
					if (checkAns==null)
					{
						txtStr += msg_not_answered;
					} else
					{
						for (var m=0; m<checkAns.length; m++)
						{
							if (isNaN(parseInt(checkAns[m])))
							{
								txtStr += msg_not_answered;
							} else
							{
								my_value = (typeof(tmpArr[parseInt(checkAns[m])+1])!="undefined") ? tmpArr[parseInt(checkAns[m])+1] : msg_not_answered;
								txtStr += (c_i_i>0) ? "<br><br>"+my_value : my_value;
								c_i_i = 1;
							}
						}
					}
					break;
				case "4":
				case "5":
					my_value = (typeof(ansArr[x])!="undefined") ? ansArr[x] : msg_not_answered;
					txtStr += (my_value!="") ? my_value : msg_not_answered;
					break;
					
				case "6":
				// likert scale
				//alert(ansArr);
				if(typeof(ansArr[x])=="undefined")
				{
					txtStr += msg_not_answered;
					break;
				}
				
				var myAns = ansArr[x];
				
				if(myAns != undefined)
				var myAnsArr = myAns.split(",");
				
				var no_row = type_no[1];
				var no_col = type_no[2];
				
				var quesArr = new Array();
				var scaleArr = new Array();
				var a = 0;
				var b = 0;
				
				for(a = 1; a <= no_row; a++)
				{
					quesArr[a-1] = tmpArr[a];
				}
				for(b = 0; b < no_col; b++)
				{
					scaleArr[b] = tmpArr[a+b];
				}
				
				//alert(quesArr);
				//alert(scaleArr);

				tableName = "TB_" + x;
				txtStr += '<table border="0" align="left" cellpadding="5" cellspacing="5" id="'+tableName+'" name="'+tableName+'">';
				
				// header
				txtStr += '<tr>';
				txtStr += '<td>#</td>';
				txtStr += '<td>'+question_scale+'</td>';
				for(a = 0; a < no_col; a++)
				{
					txtStr += '<td align="center" nowrap>'+ scaleArr[a] + '</td>';
				}
				txtStr += '</tr>';
				
				// question
				for(a = 0; a < no_row; a++)
				{
					txtStr += '<tr>';
					txtStr += '<td>' + (a + 1) + '.</td>';
					txtStr += '<td>' + quesArr[a] + '</td>';
					
					for(b = 0; b < no_col; b++)
					{
						if(myAnsArr != undefined)
						{
							if(myAnsArr[a] == b)
							txtStr += '<td align="center"><input type="radio" name="RS'+ x + '_' + a + '" checked /></td>';	
							else
							txtStr += '<td align="center"><input type="radio" name="RS'+ x + '_' + a + '" /></td>';	
						}
						else
						{
							txtStr += '<td align="center"><input type="radio" name="RS'+ x + '_' + a + '" /></td>';	
						}
					}
					
					txtStr += '</tr>';
				}
				
				txtStr += '</table>';
					break;
					
				case "7":
				// table like format
				//alert(ansArr);
				if(typeof(ansArr[x])=="undefined")
				{
					txtStr += msg_not_answered;
					break;
				}
				
				var myAns = ansArr[x];
				
				if(myAns != undefined)
				var myAnsArr = myAns.split(",");
				
				var no_row = type_no[1];
				var no_col = type_no[2];
				
				if(myAnsArr != undefined)
				{
					if(myAnsArr[0] != undefined)
					var txtAnsArr = myAnsArr[0].split("#MQA#");
				}
				
				var quesArr = new Array();
				var scaleArr = new Array();
				var a = 0;
				var b = 0;
				
				for(a = 1; a <= no_row; a++)
				{
					quesArr[a-1] = tmpArr[a];
				}
				for(b = 0; b < no_col; b++)
				{
					scaleArr[b] = tmpArr[a+b];
				}
				
				//alert(quesArr);
				//alert(scaleArr);
				
				txtStr += '<table border="0" align="left" cellpadding="5" cellspacing="5">';
				// header
				txtStr += '<tr>';
				txtStr += '<td>#</td>';
				txtStr += '<td>'+question_question+'</td>';
				for(a = 0; a < no_col; a++)
				{
					txtStr += '<td nowrap>'+ scaleArr[a] + '</td>';
				}
				txtStr += '</tr>';
				
				// question
				for(a = 0; a < no_row; a++)
				{
					txtStr += '<tr>';
					txtStr += '<td>' + (a + 1) + '.</td>';
					txtStr += '<td>' + quesArr[a] + '</td>';
					
					for(b = 0; b < no_col; b++)
					{
						if(myAnsArr != undefined && myAnsArr != "")
						{									
							ansIndex = a * no_col + b + 1;									
							tmpAnsTxt = txtAnsArr[ansIndex];
							txtStr += '<td align="center"><input type="text" value="'+tmpAnsTxt+'" size="10" readonly="readonly" /></td>';
						}
						else
						{
							txtStr += '<td align="center"><input type="text" value="" size="10" readonly="readonly" /></td>';
						}
					}
					
					txtStr += '</tr>';
				}
				
				txtStr += '</table>';
					break;
					
				case "8":
					break;
					
					
					
			} // end switch
			if (displaymode==0 || type_no[0]==lastIndexVal)
			{
				txtStr += '</td></tr>';
			} else if (displaymode==1)
			{
				txtStr += '</td></tr></table></td></tr>';
			}
		}
	}
    //txtStr+='</td></tr></table><p></p></td></tr></table>\n';
	txtStr+='</table><p></p></td></tr></table>\n';
	txtStr+='</Form>';
    
	//alert(txtStr);
	
	return txtStr;
}

function switchLayer(layer_name){
	var hide = "hidden";
	var show = "visible";

	if (!document.all)
		return;

	var co=(document.getElementById && !document.all)? document.getElementById(layer_name).style : document.all[layer_name].style;

	if (co.visibility==show){
		co.visibility = hide;
		co.position  = "absolute";
	}
	else {
		co.visibility = show;
		co.position  = "relative";
	}
}



/*################################## STATISTICS ####################################*/
function Statistics()
{
	if (typeof(_statistics_prototype_called) == 'undefined')
	{
		 _statistics_prototype_called = true;
		 Statistics.prototype.qString = null;
		 Statistics.prototype.total = 0;
		 Statistics.prototype.answer = new Array();
		 Statistics.prototype.data = new Array();
		 Statistics.prototype.analysisData = analysisData;
		 Statistics.prototype.getStats = getStats;
		 Statistics.prototype.existPos = existPos;
	}

	function getStats() {
		var resultArr = new Array();
		var txtArr = this.qString.split("#QUE#");
		var dataArr = this.data;
		var tmpArr = null;
		var txtStr = null;

		txtStr='<Form name="answersheet">';
		txtStr+='<font color="blue">'+w_total_no+' : '+this.total+'</font><br><br>';
		for (var x=1; x<txtArr.length; x++)
		{
			j = x - 1;
			tmpArr = txtArr[x].split("||");
			type_no = tmpArr[0].split(",");			//0:type, 1:number of options
			question = tmpArr[1];

			txtStr+='<table border="0" width="450"><tr><td colspan=3 class="smallHeaderText">\n';
			txtStr+=question+'</td></tr>\n';

			if (type_no[0]!=6) {
				txtStr+='<tr><td>&nbsp;</td>';
				txtStr+='<td width="95%">';

				if (type_no[0]==4 || type_no[0]==5) {
					txtStr+="<input type='button' value='"+w_show+"' onClick=\"JavaScript:switchLayer('l_g"+j+"');\"><br>";
					txtStr+="<div id='l_g"+j+"' style='position:absolute; left:0px; top:0px; width:100%; z-index:1; visibility: hidden'>";
				}
				txtStr+='<table border="0" width="100%">';
				for (var i=0; i<dataArr[j].length; i++) {
					percent = (dataArr[j][i][1]==0) ? 0 : Math.round(dataArr[j][i][1]*100/this.total);
					txtStr+='<tr><td width="95%">'+dataArr[j][i][0] + '</td><td nowrap><font color="red">'+dataArr[j][i][1]+' ('+percent+'%)</font>';
					txtStr+=(type_no[0]==4 && i==dataArr[j].length-1) ? '<br></td></tr>' : '<br><br></td></tr>';
				}
				txtStr+='</table>';
				if (type_no[0]==4 || type_no[0]==5)
					txtStr+="</div><br>";
				txtStr+='<br></td>';
			} else {
			    txtStr+='<tr><td colspan=3>&nbsp;</td></tr>';
			}

			txtStr+='</table>\n';
		}

		txtStr+='</Form>';

		return txtStr;
	}


	function analysisData() {
		var resultArr = new Array();
		var txtArr=this.qString.split("#QUE#");

		// each application
		for (var i=0; i<this.answer.length; i++)
		{
			ansArr=this.answer[i].split("#ANS#");
			var tmpArr = null;

			// each question
			for (var x=1; x<txtArr.length; x++)
			{
				j = x - 1;
				tmpArr = txtArr[x].split("||");
				type_no = tmpArr[0].split(",");			//0:type, 1:number of options
				question = tmpArr[1];
				opts = tmpArr[2];

				tmpArr = opts.split("#OPT#");

				if (i==0)
				{
				    resultArr[j] = new Array();
				}

				switch (type_no[0])
				{
					case "1":
					case "2":
						if (i==0)
						{
							opt_no = (type_no[0]=="1") ? 2 : type_no[1];
							for (var k=0; k<opt_no; k++)
							{
								resultArr[j][k] = new Array();
								resultArr[j][k][0] = tmpArr[k+1];
								resultArr[j][k][1] = 0;
							}
						}
						if (!isNaN(parseInt(ansArr[x]))) {
						    resultArr[j][parseInt(ansArr[x])][1] ++;
						}
						break;
					case "3":
						if (i==0)
						{
							opt_no = type_no[1];
							for (var k=0; k<opt_no; k++)
							{
								resultArr[j][k] = new Array();
								resultArr[j][k][0] = tmpArr[k+1];
								resultArr[j][k][1] = 0;
							}
						}
						checkAns = ansArr[x].split(",");
						for (var m=0; m<checkAns.length; m++)
						{
							if (!isNaN(parseInt(checkAns[m])))
							{
							    resultArr[j][parseInt(checkAns[m])][1] ++;
							}
						}
						break;
					case "4":
					case "5":
						var answer_pos = this.existPos(resultArr[j], ansArr[x]);
						isArrExpand = (answer_pos==resultArr[j].length);

						if (isArrExpand)
						{
							resultArr[j][answer_pos] = new Array;
							resultArr[j][answer_pos][0] = ansArr[x];
							resultArr[j][answer_pos][1] = 0;
						}
						resultArr[j][answer_pos][1] ++ ;
						break;
					case "6":
					// likert scale
					//resultArr[j][0] = ansArr;
					
					
						break;
						
					case "7":
					// table like format
					
					
						break;
						
						
						case "8":
							break;
				} // end switch
			} // end for each quesiton
		} // end for each application

		this.total = this.answer.length;
		this.data = resultArr;
	}

	function existPos(reArr, value) {
		for (var i=0; i<reArr.length; i++) {
			if (reArr[i][0].toUpperCase()==value.toUpperCase())
				return i;
		}
		return reArr.length;
	}

}
