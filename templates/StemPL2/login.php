<?php 
// Using: Henry

/********************** Change Log ***********************
 * Date: 2018-05-08 [Henry]
 *       Stem Login page
 *********************** Change Log **********************/
 
include_once("../../includes/global.php");
include_once("../../includes/SecureToken.php");

if (is_file("$intranet_root/servermaintenance.php") && !$iServerMaintenance && !$AccessLoginPage)
{
	header("Location: ../servermaintenance.php");
	exit();
}

$PATH_WRT_ROOT_ABS = "../../";

if (!$sys_custom['StemPL2']) {
	header("Location: ../../login.php");
	exit;
}

$customLangVar = $sys_custom['Project_Label'];

if (file_exists($PATH_WRT_ROOT_ABS . "lang/lang.$intranet_session_language.php")) include_once($PATH_WRT_ROOT_ABS . "lang/lang.$intranet_session_language.php");
if (file_exists($PATH_WRT_ROOT_ABS . "lang/" . $customLangVar. "/stem_pl2_lang.$intranet_session_language.php")) include_once($PATH_WRT_ROOT_ABS . "lang/" . $customLangVar. "/stem_pl2_lang.$intranet_session_language.php");

intranet_opendb();
$SecureToken = new SecureToken();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" data-ng-app="powerLesson">
<head>
<title>PowerLesson 2</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/angular-material.min.css"	rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" href="images/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="images/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="images/favicon/manifest.json">
<link rel="mask-icon" href="images/favicon/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery-1.4.4.min.js"></script>
<script language="JavaScript1.2" src="<?=$PATH_WRT_ROOT_ABS?>templates/script.js"></script>
<script language="javascript">
	function checkLoginForm(){
		$("#login_btn").attr("disabled", true);
		$("#login_alert_div").html("");
		$("#pin_alert_div").html("");
		
		var pass = 1;
		if(!checkLoginInput(document.getElementById("UserLogin"), "<?=$Lang[$customLangVar]['PleaseEnterValidUsernameOrPassword']?>", "login_alert_div")){
			pass = 0;
		}
		else if(!checkLoginInput(document.getElementById("UserPassword"), "<?=$Lang[$customLangVar]['PleaseEnterValidUsernameOrPassword']?>", "login_alert_div")){
			pass = 0;
		}
		
		if(pass)	
		{
			$(".page-loading").show();
			return true;
		}
		
		$("#login_btn").attr("disabled", false);
		return false;
	}

	function checkPinForm(){
		$("#pin_btn").attr("disabled", true);
		$("#login_alert_div").html("");
		$("#pin_alert_div").html("");
		
		var pass = 1;
		if(!checkLoginInput(document.getElementById("pin"), "<?=$Lang[$customLangVar]['PleaseEnterValidPin']?>", "pin_alert_div")){
			pass = 0;
		}
		
		if(pass)	
		{
			$(".page-loading").show();
			return true;
		}
		
		$("#pin_btn").attr("disabled", false);
		return false;
	}
	
	function checkLoginInput(f, msg, t){
		if(Trim(f.value) == ""){
		    $('#'+t).html(msg);
		    $('#'+t).css('display','block');
		    f.value = "";
		    f.focus();
		    return false;
		}else{
		    return true;
		}
	}
</script>
</head>
<body>
<div class="page-loading" style="display:none">
	<md-progress-circular md-mode="indeterminate" md-diameter="100px"></md-progress-circular>
</div>
<div class="login">
	<div class="logo-microbit"></div>
	<div class="bg">
		<div class="login-container">
			<div class="logo"><img src="images/logo-pl2.png" width="381" height="87"/></div>
			<div class="box">
				<form name="form_login" action="../../login.php" method="post" onSubmit="return checkLoginForm();">
	    			<?=$SecureToken->WriteFormToken()?>
					<div class="user-box">
			            <input placeholder="<?=$Lang[$customLangVar]['Username']?>" type="text" name="UserLogin" id="UserLogin" />
						<input placeholder="<?=$Lang[$customLangVar]['Password']?>" type="password" name="UserPassword" id="UserPassword" />
			            <button class="btn-login" id="login_btn"><span><?=$Lang[$customLangVar]['Login']?></span></button>
			            <div class="login-alert" id="login_alert_div">
			            <?=$err == 1 ? $Lang[$customLangVar]['PleaseEnterValidUsernameOrPassword'] : '' ?>
						</div>
					</div>
					<input type="hidden" name="url" value="/templates/index.php?err=1&DirectLink=<?=rawurlencode($DirectLink)?>">
					<input type="hidden" name="AccessLoginPage" value="<?=$AccessLoginPage?>">
				</form>
				<div class="separator"><?=$Lang[$customLangVar]['Or']?></div>
				<div class="pin-box">
					<form name="form_pin" action="../../eclass40/pl2/" method="get" onSubmit="return checkPinForm();">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="<?=$Lang[$customLangVar]['Pin']?>" name="pin" id="pin">
							<span class="input-group-btn">
			                	<button class="btn btn-secondary" id="pin_btn"><img src="images/arrow.png" width="17" height="17"/></button>
							</span>
			            </div>
					</form>
		            <div class="login-alert" id="pin_alert_div"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="js/angular.min.js"></script>
<script src="js/angular-animate.js"></script>
<script src="js/angular-aria.min.js"></script>
<script src="js/angular-material.min.js"></script>
<script src="js/app.js"></script>

</body>
</html>