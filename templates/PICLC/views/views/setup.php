<?php 
$order_no = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $Lang["PageTitle"]; ?></title>
	<link href="<?php echo $source_path; ?>css/main.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $source_path; ?>css/portal.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $source_path; ?>css/portalIcon.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $source_path; ?>css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="<?php echo $source_path; ?>js/script.js"></script>
	<link rel="shortcut icon" href="<?php echo $source_path; ?>images/favicon.gif" type="image/x-icon">
</head>
<body>
	<div class=" CustCommon CustMainContent PowerCLassPortal">
	<div class="pageHeader">
						<div class="backButton font-light pointer" onclick="window.history.go(-1); return false;">
							<?php echo $Lang["PoserClass"]["BtnBack"];?>
						</div>
						<div class="pageSubject font-bold">
							<?php echo $Lang['Header']['Menu']['SystemSetting']?>
						</div>
					</div>
		<div id="blkModuleContainer" class="bgColor-2">
			<?php 
			if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]){
				$order_no++;
			?>
			<div class="accordion">
				<div id="lblPart1"><?=$order_no?></div>
				<div class="accordion-subject"><?php echo $Lang["PowerClass"]["School"]; ?></div>
			</div>
			<div class="accordion-page">
				<div class="portalIconsContainer1">
					<span id="btnSchool-SchoolYear" class="portalIcon">
						<a class="portalIcon_img" href='/home/system_settings/school_calendar/academic_year.php'></a>
						<div class="portalIcon_text">
							<span>
								<div><a href='/home/system_settings/school_calendar/academic_year.php'><?php echo $Lang["PowerClass"]["SchoolYearAndTerm"]; ?></a></div>
							</span>
						</div>
					</span><span
					id="btnSchool-Form" class="portalIcon">
						<a class="portalIcon_img" href="/home/system_settings/form_class_management/"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/system_settings/form_class_management/"><?php echo $Lang["PowerClass"]["Class"]; ?></a></div>
							</span>
						</div>
					</span><span
					id="btnSchool-Subject" class="portalIcon">
						<a class="portalIcon_img" href="/home/system_settings/subject_class_mapping/"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/system_settings/subject_class_mapping/"><?php echo $Lang["PowerClass"]["Subject"]; ?></a></div>
							</span>
						</div>
					</span><span
					id="btnSchool-Group" class="portalIcon">
						<a class="portalIcon_img" href="/home/system_settings/group/?clearCoo=1"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/system_settings/group/?clearCoo=1"><?php echo $Lang["PowerClass"]["Group"]; ?></a></div>
							</span>
						</div>
					</span><?php /*<span
					id="btnSchool-Role" class="portalIcon">
						<a class="portalIcon_img" href="/home/system_settings/role_management/"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/system_settings/role_management/"><?php echo $Lang["PowerClass"]["Role"]; ?></a></div>
							</span>
						</div>
					</span> */ ?><span
					id="btnSchool-Events" class="portalIcon">
						<a class="portalIcon_img" href='/home/system_settings/school_calendar/'></a>
						<div class="portalIcon_text">
							<span>
								<div><a href='/home/system_settings/school_calendar/'><?php echo $Lang["PowerClass"]["HolidayAndEvents"]; ?><br>(<?php echo $Lang["PowerClass"]["SchoolCalendar"]; ?>)</a></div>
							</span>
						</div>
					</span><span
					id="btnSchool-Security" class="portalIcon">
						<a class="portalIcon_img" href='/home/system_settings/security/password_policy.php'></a>
						<div class="portalIcon_text">
							<span>
								<div><a href='/home/system_settings/security/password_policy.php'><?php echo $Lang["PowerClass"]["SchoolSecurity"]; ?></a></div>
							</span>
						</div>
					</span><span
					id="btnSchool-Timezone" class="portalIcon">
						<a class="portalIcon_img" href="/home/system_settings/school_calendar/time_zone.php"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/system_settings/school_calendar/time_zone.php"><?php echo $Lang['SysMgr']['CycleDay']['SettingTitle'] ?></a></div>
							</span>
						</div>
					</span><span
					id="btnSchool-Timetable" class="portalIcon">
						<a class="portalIcon_img" href="/home/system_settings/timetable/index.php"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/system_settings/timetable/index.php"><?php echo $Lang['SysMgr']['Timetable']['ModuleTitle']; ?></a></div>
							</span>
						</div>
					</span><span
					id="btnSchool-Role" class="portalIcon">
						<a class="portalIcon_img" href="/home/system_settings/role_management/index.php"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/system_settings/role_management/index.php"><?php echo $Lang['Header']['Menu']['Role']; ?></a></div>
							</span>
						</div>
					</span><span
					id="btnSchool-Campus" class="portalIcon">
						<a class="portalIcon_img" href="/home/system_settings/location/"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/system_settings/location/"><?php echo $Lang['Header']['Menu']['Site']; ?></a></div>
							</span>
						</div>
					</span> 
				</div>
			</div>
			<?php
			}
			?>
			<?php 
			if($_SESSION['SSV_USER_ACCESS']['eAdmin-AccountMgmt']){
				$order_no++;
			?>
			<div class="accordion">
				<div id="lblPart2"><?=$order_no?></div>
				<div class="accordion-subject"><?php echo $Lang["PowerClass"]["Account"]; ?></div>
			</div>
			<div class="accordion-page">
				<div class="portalIconsContainer1">
					<span id="btnAccount-Staff" class="portalIcon">
						<a class="portalIcon_img" href='/home/eAdmin/AccountMgmt/StaffMgmt/?clearCoo=1'></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/eAdmin/AccountMgmt/StaffMgmt/?clearCoo=1"><?php echo $Lang["PowerClass"]["Staff"]; ?></a></div>
								<div id="lblParentNo" class="portalIcon_data"><?php if ($NumberOfStaff > 0) echo $NumberOfStaff; ?></div>
							</span>
						</div>
					</span><span
					id="btnAccount-Student" class="portalIcon">
						<a class="portalIcon_img" href="/home/eAdmin/AccountMgmt/StudentMgmt/?clearCoo=1"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/eAdmin/AccountMgmt/StudentMgmt/?clearCoo=1"><?php echo $Lang["PowerClass"]["Student"]; ?></a></div>
								<div id="lblStudentNo" class="portalIcon_data">
									<?php /* if ($NumberOfStudent > 0) { ?><span id="lblStudentNo" class="portalIcon_data"><?php echo $NumberOfStudentAvailTotal; ?></span><span class="portalIcon_subInfo">&nbsp;<!-- /&nbsp;<span id="lblStudentQuota"><?php echo $NumberOfStudent; ?></span> --></span><?php } */?>
									<?php echo $NumberOfStudentUsed; ?>
								</div>
							</span>
						</div>
					</span><span
					id="btnAccount-StudentPhotos" class="portalIcon">
						<a class="portalIcon_img" href="/home/eAdmin/AccountMgmt/StudentMgmt/photo/"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/eAdmin/AccountMgmt/StudentMgmt/photo/"><?php echo $Lang["PowerClass"]["StudentApp_OfficalPhoto"]; ?></a></div>
							</span>
						</div>
					</span><span
					id="btnAccount-Parent" class="portalIcon">
						<a class="portalIcon_img" href="/home/eAdmin/AccountMgmt/ParentMgmt/?clearCoo=1"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/eAdmin/AccountMgmt/ParentMgmt/?clearCoo=1"><?php echo $Lang["PowerClass"]["Parent"]; ?></a></div>
								<div id="lblStaffNo" class="portalIcon_data"><?php if ($NumberOfParent > 0) echo $NumberOfParent; ?></div>
							</span>
						</div>
					</span>
				</div>
			</div>
			<?php 
			}
			?>
			<?php
			if($_SESSION['SSV_USER_ACCESS']["eLearning-eClass"]){
				$order_no++;
			?>
			<div class="accordion">
				<div id="lblPart3"><?=$order_no?></div>
				<div class="accordion-subject"><?php echo $Lang["PowerClass"]["eClass"]; ?></div>
			</div>
			<div class="accordion-page">
				<div class="portalIconsContainer1">
					<span id="btnClass-eClassroom" class="portalIcon">
						<a class="portalIcon_img" href="/home/eLearning/eclass/organize/"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/eLearning/eclass/organize/"><?php echo $Lang["PowerClass"]["eClassroom"]; ?></a></div>
							</span>
						</div>
					</span>
				</div>
			</div>
			<?php
			}
			?>
		</div>
		<div id="lbleClass"><a href="http://eclass.com.hk" title="PowerClass" target="_blank"><img id="imgPC" src="<?php echo $source_path; ?>images/powerClassLogo_w.png"></a><span>Powered by</span><a href="http://eclass.com.hk" title="eClass" target="_blank"><img src="<?php echo $source_path; ?>images/eClassLogo_w.png"></a></div>
	</div>
</html>