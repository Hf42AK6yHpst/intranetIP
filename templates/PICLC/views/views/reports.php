<?php
/*
 * 	2017-12-20 Cameron
 * 		set overflow:visible for eInventory so that it won't hide partial title name
 */
	session_start();
	$_SESSION['PowerClass_PAGE'] = "reports";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $Lang["PageTitle"]; ?></title>
	<link href="<?php echo $source_path; ?>css/main.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $source_path; ?>css/portal.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $source_path; ?>css/portalIcon.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $source_path; ?>css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="<?php echo $source_path; ?>js/script.js"></script>
	<link rel="shortcut icon" href="<?php echo $source_path; ?>images/favicon.gif" type="image/x-icon">
</head>
<body>
	<div class="CustCommon CustMainContent PowerCLassPortal">
		<div id="blkModuleContainer" class="bgColor-1">
			<div class="portalIconsContainer3">
				<?php
				foreach($app_details as $key=>$app){
					if(!is_array($app))continue;
				?>
				<span id="" class="portalIcon portalIcon-<?=$app['icon_class']?>">
					<div class="portalIcon_img">
					</div>
					<div class="portalIcon_text" <?=($app['icon_class'] == 'eInventory') ? 'style="overflow:visible"' : ''?>>					
						<span>
							<div><a href="<?=$app['href']?>"><?=$app['Title']?></a></div>
						</span>
					</div>
				</span>
				<?php
				}
				?>
			</div>
		</div>
		<div id="lbleClass"><a href="http://eclass.com.hk" title="PowerClass" target="_blank"><img id="imgPC" src="<?php echo $source_path; ?>images/powerClassLogo_w.png"></a><span>Powered by</span><a href="http://eclass.com.hk" title="eClass" target="_blank"><img src="<?php echo $source_path; ?>images/eClassLogo_w.png"></a></div>
	</div>
</body>
</html>