<?php
# using:

#############################################################
#	Date:	2020-11-05 Henry
#			- bug fix for users need to login twice to access eClass [Case#L200676]
#
#   Date:   2020-08-26 Cameron
#           - append timestamp to school badge and background image to prevent cache [case #E193663]
#
#	Date:	2018-08-08 Carlos
#			- Do js checking on XFS(Cross Frame Scripting). Can use $sys_custom['Allow_embed_eClass_in_iframe'] to exclude for those clients that have used iframe approach.
#
#	Date:	2017-08-14 Paul
#			- add mechanism to decide the use of login UI page also on the project sub-theme flag
#
#	Date:	2017-03-29 Carlos
#			- remove including of jquery script as security scanner report it has security risk.
#
#	Date:	2016-10-03 Jason [ip.2.5.7.10.1]
#			- add google button to support login by Google Account, $ssoservice["Google"]["Valid"]
#
#	Date:	2016-05-24 Paul [ip.2.5.7.7.1]
#			- Redirect to PowerFlip login entrance if PowerFlip Lite is using
#
#	Date:	2016-01-13 Jason [ip.2.5.7.1.1]
#			- change the year of copyright from 2009 to 2016
#
#	Date:	2015-10-30 Bill
#			- support API server for HKEdCity EdConnect
#
#	Date:	2015-10-02 Bill
#			- added checking for $ssoservice["HKEdCity"]["Valid"] to display HKEdCity Login button
#
#	Date:	2015-07-16 Carlos
#			- add secure token to prevent CSRF.
#
#	Date:	2015-05-18 Cameron
#			- pass $_GET parameters to customized login page
#
#	Date:	2015-03-20 Cameron
#			- Change login_title css width from 233px to 253px so that full picture can be shown
#
#	Date:	2015-03-11 Cameron
#			- Add flag $sys_custom['eLibraryPlus']['eLibraryPlusOnly'] to control showing IP image or eLib+ image
#
#	Date:	2015-01-16	Omas	[ip.2.5.6.3.1]
#			Disable login btn after submit and show a loading image
#			comment style.css , login_form.png
#
#	Date:	2014-09-29	Carlos [ip.2.5.5.10.1]
#			Set $DirectLink from $_SESSION['REDIRECT_URL'] before session destroyed
#
#	Date:	2013-09-26	YatWoon
#			improved: language will depends on client's default language settings [Case#2013-0909-0940-06071]
#
#	Date:	2013-09-09	Ivan [2013-0906-1440-29177]
#			change wording "forgot password" to "Reset Password"
#
#	Date:	2013-05-08	Siuwan
#			add css "title_en" and checking for login_title css with different $intranet_default_lang
#
#	Date:	2013-05-07	Siuwan
#			Comment out two jquery include files
#
#	Date:	2013-04-26	Siuwan
#			Add meta name="viewport" for handling scale of mobile browser layout
#
#	Date:	2013-03-12	YatWoon
#			Revised ui
#
#	Date:	2013-01-16	Yuen
#			enable PowerLesson login page (to replace standard login to Intranet) and all users access PL by default
#
#	Date:	2012-12-20	YatWoon
#			Add $AccessLoginPage, allow user to still access the login page during the maintenance
#
#	Date:	2012-10-26	YatWoon
#			Add session_destroy(); [Case#2012-0924-1721-00073]
#
#	Date:	2012-10-19	Yuen
#			Show the link of Parent App Online Form according to $sys_custom["show_parentapp_form"]
#
#	Date:	2012-06-21	YatWoon
#			Change the prompt msg for "forgot password"
#
#	Date:	2011-09-08 YatWoon
#			add LDAP checking <== should be no "forgot password" like
#
#	Date:	20110-08-22	YatWoon
#			change the Login button to "button" from "image" [Case: 2011-0819-0955-43073]
#
#############################################################
@session_start();

/*
 * The session variable 'REDIRECT_URL' remembers the attempted request url before login. It is set at /includes/lib.php intranet_auth().
 * Here assign it to $DirectLink for auto redirect.
 */
if(isset($_SESSION['REDIRECT_URL']) && $_SESSION['REDIRECT_URL'] != ''){
	$DirectLink = $_SESSION['REDIRECT_URL'];
}


/*** come from SSO ***/
if (isset($_SESSION["ECLASS_SSO"]["client_credentials"]))
{
    /* remove DSI SSO on this Login Page at 20190103 */
    /*
    // 5 min expired
    $expireAfter = 5;
    $secondsInactive = time() - $_SESSION["ECLASS_SSO"]["client_credentials"]["expiredTime"];
    $expireAfterSeconds = $expireAfter * 60;
    if($secondsInactive <= $expireAfterSeconds)
    {
        $client_credentials = $_SESSION["ECLASS_SSO"]["client_credentials"];
    }
    */
}

session_destroy();
@session_start();
if (is_file("../servermaintenance.php") && !$iServerMaintenance && !$AccessLoginPage)
{
    header("Location: ../servermaintenance.php");
    exit();
} elseif (is_file("login.customized.php"))
{
	if (count($_GET)>0) {
		$para = '?';
		foreach($_GET as $k=>$v) {
			if ($v) {
				$para .= "$k=$v&";
			}
		}
		$para = substr($para,0,-1);	// remove last '&'
	}
	else {
		$para = '';
	}
    header("Location: login.customized.php".$para);
    exit();
}
include_once("../includes/global.php");
include_once("../includes/SecureToken.php");
if ($sys_custom['Project_Custom']) {
	if($sys_custom['Project_Use_Sub']){
		if (file_exists($sys_custom['Project_Sub_Label'] ."/login.php") && $sys_custom[$sys_custom['Project_Sub_Label']])
		{
			if (count($_GET)>0) {
				$para = "?" . $_SERVER['QUERY_STRING'];
			}
			else {
				$para = '';
			}
			header("Location: " . $sys_custom['Project_Sub_Label'] . "/login.php".$para);
			exit();
		}
	}
	if (file_exists($sys_custom['Project_Label'] ."/login.php") && $sys_custom[$sys_custom['Project_Label']])
	{
		if (count($_GET)>0) {
			$para = "?" . $_SERVER['QUERY_STRING'];
		}
		else {
			$para = '';
		}
		header("Location: " . $sys_custom['Project_Label'] . "/login.php".$para);
		exit();
	}
}

intranet_opendb();
$SecureToken = new SecureToken();

if ($plugin["platform"]=="KIS")
{
    header("Location: ../kis/");
    exit();
} elseif (isset($plugin['PowerLesson_Login']) && $plugin['PowerLesson_Login'] && is_file("../pl/index.php"))
{
    header("Location: ../pl/");
    exit();
}elseif (isset($plugin['PowerFlip_lite']) && $plugin['PowerFlip_lite'] && is_file("../pf/index.php"))
{
    header("Location: ../pf/");
    exit();
}
if(isset($intranet_default_lang)){
	$title_lang = $intranet_default_lang;
}else{
	$title_lang = 'b5';
}
include_once("../lang/lang.$intranet_session_language.php");

$title_image = ($sys_custom['eLibraryPlus']['eLibraryPlusOnly']) ? "title_library_b5.png" : "title_b5.png";

if ($sys_custom['eClassApp']['showLoginPageDownloadApkLink']) {
	$eClassAppApkDownloadLink = '<div class="eclass_DL_btn"><a href="http://www.eclass.com.hk/apk/eclass_app_apk.php" target="_epaform">立即下載 eClass App</a></div>';
}
else {
    $eClassAppApkDownloadLink = '';
}

//debug_pr(Get_IP_Version());
$platformVersion = Get_IP_Version();
$platformVersionAry = explode('.', $platformVersion);
$copyRightYear = 2009 + $platformVersionAry[3];

$is_from_SSO = false;
/* remove DSI SSO on this Login Page at 20190103 */
/*
if (isset($client_credentials))
{
    session_start();
    $is_from_SSO = true;
    $_SESSION["ECLASS_SSO"]["client_credentials"] = $client_credentials;
}
*/

$background_image = '';
$organization_logo = '';
if ($sys_custom['SchoolSettings']['OrganizationInfo'] || (float)$intranet_version > 2.5) {
    include_once("../includes/libdb.php");
    include_once("../includes/libgeneralsettings.php");
    include_once("../includes/libfilesystem.php");

    $ls = new libgeneralsettings();
    $settingAry = $ls->Get_General_Setting('OrganizationInfo',array("'BackgroundImage'", "'LoginLogo'"));

    if (count($settingAry)) {
        $backgroundImage = "/file/organization_info/". $settingAry['BackgroundImage'];
        if (is_file($intranet_root.$backgroundImage)) {
            $background_image = $backgroundImage;
        }

//        $loginLogo = "/file/organization_info/". $settingAry['LoginLogo'];
//        if (is_file($intranet_root.$loginLogo)) {
//            $organization_logo = $loginLogo;
//        }
    }

    $lf = new libfilesystem();
    $loginLogo = $lf->file_read($intranet_root."/file/schoolbadge.txt");
    if (is_file($intranet_root."/file/".$loginLogo)) {
        $organization_logo = "/file/" . $loginLogo;
    }
}

$schoolName = GET_SCHOOL_NAME();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,user-scalable=yes">
<link id="page_favicon" href="/images/<?=$favicon_image?>" rel="icon" type="image/x-icon" />
<title>eClass IP</title>
<link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
<script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
<link href="/templates/<?=$LAYOUT_SKIN?>/css/login.css?301" rel="stylesheet" type="text/css">
<? /* ?>

    <script type="text/javascript" src="http://jqueryjs.googlecode.com/files/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="http://flesler-plugins.googlecode.com/files/jquery.scrollTo-1.4.2-min.js"></script>

<? */ ?>
<script language=JavaScript src=/lang/script.<?=$title_lang?>.js></script>
<script language=JavaScript1.2 src=/templates/script.js></script>
<script language="javascript">
function signInWithGoogle(){
	<? if($ssoservice["Google"]['Enable_SubDomain']['blers'] == true){ ?>
	var GoogleSSOMsg = 'Please enter your google email address';
	tmp = prompt(GoogleSSOMsg, ".broadlearning.com");
	if(tmp!=null && Trim(tmp)!=""){
		location.href = "./login.googlesso.php?gmail="+tmp;
	}
	<? } else { ?>
	location.href = "./login.googlesso.php";
	<? } ?>
}

function checkLoginForm(){
	var login_btn = document.getElementById('login_btn');
	login_btn.disabled = true;
	var obj = document.form1;
	var pass=1;

	if(!check_text(document.getElementById('UserLogin'), "Please enter the user login."))
		pass  = 0;
	else if(!check_text(document.getElementById('UserPassword'), "Please enter the password."))
		pass = 0;

	if(pass)
	{
		document.getElementById('loading').style.display = '';
		return true;
	}
	login_btn.disabled = false;
	return false;
}

function checkForgetForm(){
     var obj = document.form2;
     tmp = prompt(ForgotPasswordMsg, "");
     if(tmp!=null && Trim(tmp)!=""){
          obj.UserLogin.value = tmp;
          obj.submit();
     }
}
</script>
</head>

<body><!-- .role-admin / .role-learner / .role-trainer -->
<form name="form1" action="../login.php" method="post" onSubmit="return checkLoginForm();">
<div id="ploginWrapper">
    <div id="blkPhoto" <?php if (!empty($background_image)) echo 'style="background: url('.$background_image.'?'.time().'); background-color: #eee; background-size: cover; background-position: 50% 50%;"'; ?>></div>

    <div id="blkLogin">

        <div id="blkFormWrapper">

            <div id="blkForm">

                <div id="login_title" class="title_<?=$title_lang?>">
						<span class="title_logo">
            	<img src="<?php echo $organization_logo."?".time();?>" border="0" >
					  </span>
                    <span class="title_text"><?php echo $schoolName;?></span>
                </div>
                    <div class="login_form">
                        <span class="text_login"></span>
                        <span class="text_password"></span>


                        <input class="user_loginname" type="text" name="UserLogin" id="UserLogin" placeholder="<?=$Lang['AccountMgmt']['LoginID']?>">
                        <input class="user_password" type="password" name="UserPassword" id="UserPassword" placeholder="<?=$Lang['Gamma']['Password']?>">
                        <div id="loading" style="display:none"><img src="/images/<?=$LAYOUT_SKIN?>/indicator.gif"></div>
                        <input name="submit" type="submit" class="login_btn" id="login_btn" value="<?=$Lang['LoginPage']['Login']?>" />

                        <? if (!($intranet_authentication_method=="LDAP") && !$is_from_SSO) : ?>
                            <a href="javascript:checkForgetForm();" class="forgot_password"><?=$Lang['ForgotHashedPassword']['ResetPassword']?>?</a>
                        <? endif; ?>
                        <!--20200520-->
                        <?php if($ssoservice["Google"]["Valid"] && !$is_from_SSO){ ?>
                        <div class="login-seperate">
                            <span class="line"></span>
                            <span class="text">或</span>
                            <span class="line"></span>
                        </div>
                        <!--20200520-->
                            <a href="#" onclick="signInWithGoogle()" class="google_login" ><img src="/images/google_logo.png">以 Google 帳戶登入</a>
                        <?php } ?>
                        <?php if($err==1) { ?><div class="error_msg"><?=$Lang['LoginPage']['IncorrectLogin']?></div><?php } ?>
                        <?php if($msg==1) { ?><div class="done_msg"><?=$Lang['LoginPage']['RequestSent']?></div><?php } ?>
                        <?php if($err==2) { ?><div class="error_msg"><?=$Lang['LoginPage']['IncorrectLogin']?></div><?php } ?>
                        <?php if($ssoservice["HKEdCity"]["Valid"] && $err==5) { ?><div class="error_msg"><?=$Lang['HKEdCity']['NotLinkToHKEdCityAcct']?></div><?php } ?>
                    </div>
            </div>
            <div id="footer">
                <span> <a href="http://www.eclass.com.hk/" target="_blank"><img src="/images/logo_eclass_footer.png" width="59" height="21" border="0" align="absmiddle"></a></span>
            </div>

            <? if ($sys_custom["show_parentapp_form"]) { ?>
                <div class="eclass_DL_btn"><a href="http://eclass.com.hk/parentapp/" target="_epaform">eClass Parent App Order Form</a></div>
            <? } ?>
            <?php echo $eClassAppApkDownloadLink;?>
        </div>
    </div>
</div>
<input type="hidden" name="url" value="/templates/index.php?err=1&DirectLink=<?=rawurlencode($DirectLink)?>">
<input type="hidden" name="AccessLoginPage" value="<?=htmlspecialchars($AccessLoginPage)?>">
<input type="hidden" name="DirectLink" value="<?=htmlspecialchars($DirectLink)?>">
<?=$SecureToken->WriteFormToken()?>
<?php //<input type="hidden" name="target_url" value="/home/eLearning/eclass/"> ?>
</form>

<form name=form2 action=../forget.php method=post>
    <input type=hidden name=UserLogin>
    <input type=hidden name=url_success value="/templates/index.php?msg=1">
    <input type=hidden name=url_fail value="/templates/index.php?err=2">
    <?=$SecureToken->WriteFormToken()?>
</form>

<?php if($ssoservice["HKEdCity"]["Valid"]){
    include_once("../includes/libhkedcity.php");
    $lhkedcity = new libhkedcity();
    ?>
    <form name="form3" action="<?=$lhkedcity->getCentralServerPath()?>" method="post">
        <input type="hidden" name="HKEdCity" value="<?=$lhkedcity->getSendHKEdCityData()?>">
    </form>
<?php } ?>


</body>

<SCRIPT language=javascript>
function openForgetWin () {
win_size = "resizable,scrollbars,status,top=40,left=40,width=650,height=600";
url = null;
if (url != null && url != "")
{
    var forgetWin = window.open (url, 'forget_password', win_size);
    if (navigator.appName=="Netscape" && navigator.appVersion >= "3") forgetWin.focus();
}
}

document.getElementById('UserLogin').focus();
<?php if($msg==1) { ?>openForgetWin();<?php } ?>

<?php if(!$sys_custom['Allow_embed_eClass_in_iframe']){ ?>
if (top != self){
	top.location = self.location;
}
<?php } ?>
</script>

</html>
<?php
intranet_closedb();
?>