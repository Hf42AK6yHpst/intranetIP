<div class='module_tab'><ul class='main_menu'>
    <? foreach ($tabs as $i=>$tab): ?>
	<li <?=$tab==$current_tab? 'class="selected"':''?>><a href='<?=$i==$default_tab_index? $app_url: $app_url.$tab.'/'?>'>
	    <span class="tab_<?=$tab?>"><?=$kis_lang[$tab]?></span>
	</a></li>
    <? endforeach; ?>
    <?if($app_url == '#/apps/admission/settings/'):?>
	   	<li class="btn_preview_form" ><a href="/kis/admission_form/" target="_blank"><span><?=$kis_lang['form_perview']?></span></a></li>
   	<?endif;?>
</ul>

</div>
<p class="spacer"></p>