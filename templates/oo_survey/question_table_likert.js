// Likert Scale Constructor
function question_table_likert()
{
}

question_table_likert.prototype = new question_table();
question_table_likert.prototype.genQuestionDisplay = genQuestionDisplay;
question_table_likert.prototype.genAnsQuestionDisplay = genAnsQuestionDisplay;
question_table_likert.prototype.checkDuplicateOption = checkDuplicateOption;
question_table_likert.prototype.checkDuplicateQuestion = checkDuplicateQuestion;

/**********************************************************************
 * Methods of this class
 **********************************************************************/
function genQuestionDisplay() {
	var displayQOrder = (this.qOrder == null) ? "" : this.qOrder+". ";
  var x = "";
	  
  x = "<div class=\"ies_q_box\" id=\""+this.qHashStr+"\">"
      + "<div class=\"q_box_manage\">"
      + "<a name=\"moveUpQ\" href=\"javascript:;\" hashID=\""+this.qHashStr+"\" class=\"move_up\">"+Lang["IES"]["MoveUp"]+"</a>"
      + "<a name=\"moveDownQ\" href=\"javascript:;\" hashID=\""+this.qHashStr+"\" class=\"move_down\">"+Lang["IES"]["MoveDown"]+"</a>"
      + "<a name=\"removeQ\" href=\"javascript:;\" hashID=\""+this.qHashStr+"\" class=\"q_cancel\">"+Lang['IES']['Delete']+"</a>"
      + "</div>"
      
      + "<div class=\"\">"
      + "<table class=\"form_table\">"
      + "<tr>"
      + "<td class=\"q_title\">"+displayQOrder+Lang['IES']['QuestionTitle']+" <input name=\"q\" type=\"text\" value=\""+this.q+"\" style=\"width:80%\"/></td>"
      + "</tr>"
      + "<tr><td>";

	  x +=  "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" >"
          + "<tr><td>["+Lang['IES']['Question']+" / "+Lang['IES']['Answer']+"]</td>";
	  for(var i=0; i<this.qOptNum; i++)
	  {
	    var qOpt = (this.qOpt[i] || "");
	  
	    x += "<td><input size=\"11\" type=\"text\" name=\"qOpt\" value=\""+qOpt+"\" /></td>";
	  }
	  
	  x += "<td><a name=\"addOpt\" href=\"javascript:;\" hashID=\""+this.qHashStr+"\">+</a></td>"
	       + "</tr>";
	  for(var i=0; i<this.qQueNum; i++)
	  {
	    var qQue = (this.qQue[i] || "");
	    
	    x += "<tr>";
	    x += "<td><input type=\"text\" name=\"qQue\" value=\""+qQue+"\" /></td>";

	    for(var j=0; j<this.qOptNum; j++)
	    {
	      x += "<td style=\"text-align: center;\"><input type=\"radio\" /></td>";
	    }
	    
      x += "</tr>";
	  }
	  x += "</table>";
	  
    x +=  "</td></tr>"
          + "<tr><td><a name=\"addQue\" href=\"javascript:;\" hashID=\""+this.qHashStr+"\" class=\"add_more\">"+Lang['IES']['AddRow']+"</a></td></tr>"
          + "</table>"
	        + "</div></div>";
	  
	  return x;
}

function genAnsQuestionDisplay(num){
	
	var x = "<div class=\"ies_q_box\"><table class=\"form_table\">";
	x += "<input type=\"hidden\" name=\"ans["+num+"][size]\" value=\""+this.qOptNum+"\">";
	x += "<tr><td class=\"q_title\">"+(num+1)+". "+this.q+"</td></tr><tr><td>";
	
	var colWidthPercent = Math.round(100/(this.qOptNum+1));
	x += "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" >"
	     + "<col style=\"width: "+colWidthPercent+"%\" />";
	for(var i=0; i<this.qOptNum; i++)
  {
    x += "<col style=\"width: "+colWidthPercent+"%;\" />";
  }
	     
  x += "<tr><td>&nbsp;</td>";
	
  for(var i=0; i<this.qOptNum; i++)
  {
    var qOpt = (this.qOpt[i] || "");
  
    x += "<td style=\"text-align: center;\">"+qOpt+"</td>";
  }
  x += "</tr>";
  for(var i=0; i<this.qQueNum; i++)
  {
    var qQue = (this.qQue[i] || "");
    
    x += "<tr>";
    x += "<td>"+qQue+"</td>";

    for(var j=0; j<this.qOptNum; j++)
    {
//    	if(j == 0){
//    		x += "<td align=\"center\"><input type=\"radio\" name=\"ans["+num+"]["+i+"][0]\" value=\""+j+"\" CHECKED/></td>";
//    	}
//    	else{
    		x += "<td style=\"text-align: center;\"><input type=\"radio\" name=\"ans["+num+"]["+i+"][0]\" value=\""+j+"\"/></td>";
//    	}
    }
    x += "</tr>";
  }
  x +=  "</table>";
  
  x +=  "</td></tr>"
        + "</table></div>";
  
  return x;
}

function checkDuplicateOption() {
  var qOpt = this.qOpt;
  
	for(var i=0; i<qOpt.length; i++) {
    for (var j=i+1; j<qOpt.length; j++) { // inner loop only compares items j at i+1 to n
      if (qOpt[i]==qOpt[j])
      {
        return true;
      }
    }
	}
	return false;
}

function checkDuplicateQuestion() {
  var qQue = this.qQue;
  
	for(var i=0; i<qQue.length; i++) {
    for (var j=i+1; j<qQue.length; j++) { // inner loop only compares items j at i+1 to n
      if (qQue[i]==qQue[j])
      {
        return true;
      }
    }
	}
	return false;
}