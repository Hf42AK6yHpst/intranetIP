// Question Base Constructor
function question_base()
{


}

// Prototyping (General attributes/methods applying to all chained objects)
question_base.prototype = {
  q: null,
  qOrder: null,
  qType: null,
  qHashStr: null,
  setQuestion: 
    function(str){
      this.q = htmlspecialchars(str);
    },
  setQuestionType: 
    function(type){
      this.qType = type;
    },
  setQuestionHash:
    function(hashStr){
      this.qHashStr = hashStr;
    },
  setQuestionOrder:
    function(order){
      this.qOrder = order;
    }
}