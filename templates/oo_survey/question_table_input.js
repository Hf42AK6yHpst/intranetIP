// Table-like input question Constructor
function question_table_input()
{
}

question_table_input.prototype = new question_table();
question_table_input.prototype.genQuestionDisplay = genQuestionDisplay;
question_table_input.prototype.genAnsQuestionDisplay = genAnsQuestionDisplay;

/**********************************************************************
 * Methods of this class
 **********************************************************************/
function genQuestionDisplay() {
  var x = "";
  
  x = "<div class=\"ies_q_box\" id=\""+this.qHashStr+"\">"
      + "<div class=\"q_box_manage\">"
      + "<a name=\"moveUpQ\" href=\"javascript:;\" hashID=\""+this.qHashStr+"\" class=\"move_up\">向上移</a>"
      + "<a name=\"moveDownQ\" href=\"javascript:;\" hashID=\""+this.qHashStr+"\" class=\"move_down\">向下移</a>"
      + "<a name=\"removeQ\" href=\"javascript:;\" hashID=\""+this.qHashStr+"\" class=\"q_cancel\">刪除</a>"
      + "</div>"
      
      + "<div class=\"\">"
      + "<table class=\"form_table\">"
      + "<tr>"
      + "<td class=\"q_title\"><input name=\"q\" type=\"text\" value=\""+this.q+"\" style=\"width:90%\"/></td>"
      + "</tr>"
      + "<tr><td>";

	  x +=  "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" >"
	        + "<tr><td>[Que. \\ Ans.]</td>";
	  for(var i=0; i<this.qOptNum; i++)
	  {
	    var qOpt = (this.qOpt[i] || "");
	  
	    x += "<td><input type=\"text\" name=\"qOpt\" value=\""+qOpt+"\" /></td>";
	  }
	  
	  x += "<td><a name=\"addOpt\" href=\"javascript:;\" hashID=\""+this.qHashStr+"\">+</a></td>"
	       + "</tr>";
	  for(var i=0; i<this.qQueNum; i++)
	  {
	    var qQue = (this.qQue[i] || "");
	    
	    x += "<tr>";
	    x += "<td><input type=\"text\" name=\"qQue\" value=\""+qQue+"\" /></td>";

	    for(var j=0; j<this.qOptNum; j++)
	    {
	      x += "<td align=\"center\"><input type=\"text\" /></td>";
	    }
	    
      x += "</tr>";
	  }
	  x +=  "</table>";
	  
	  x +=  "</td></tr>"
          + "<tr><td><a name=\"addQue\" href=\"javascript:;\" hashID=\""+this.qHashStr+"\" class=\"add_more\">增加一行</a></td></tr>"
	        + "</table>"
	        + "</div></div>";
  
  return x;
}

function genAnsQuestionDisplay(num){
	
	var x = "<div class=\"ies_q_box\"><table class=\"form_table\">";
	x += "<input type=\"hidden\" name=\"ans["+num+"][size]\" value=\""+this.qOptNum+"\">";
	x += "<tr><td class=\"q_title\">"+(num+1)+". "+this.q+"</td></tr><tr><td>";
	
	var colWidthPercent = Math.round(100/(this.qOptNum+1));
  x +=  "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" >"
        + "<col style=\"width: "+colWidthPercent+"%\" />";
	for(var i=0; i<this.qOptNum; i++)
  {
    x += "<col style=\"width: "+colWidthPercent+"%\" />";
  }
	       
  x += "<tr><td>&nbsp;</td>";
	
  for(var i=0; i<this.qOptNum; i++)
  {
    var qOpt = (this.qOpt[i] || "");
  
    x += "<td>"+qOpt+"</td>";
  }
  x += "</tr>";
  for(var i=0; i<this.qQueNum; i++)
  {
    var qQue = (this.qQue[i] || "");
    
    x += "<tr>";
    x += "<td>"+qQue+"</td>";

    for(var j=0; j<this.qOptNum; j++)
    {
      x += "<td><input name=\"ans["+num+"]["+i+"]["+j+"]\" type=\"text\" /></td>";
    }
    x += "</tr>";
  }
  x +=  "</table>";
  
  x +=  "</td></tr>"
        + "</table></div>";
  
  return x;
}


