<?php

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");

$this_langFile = '';

$this_langFile = ($_SESSION['sba_SchemeLang']['currentSessionLang'] == '')? $intranet_session_language :$_SESSION['sba_SchemeLang']['currentSessionLang'];
$this_langFile = (trim($this_langFile) == '') ? 'en' : $this_langFile;

include_once($PATH_WRT_ROOT."lang/lang.$this_langFile.php");
include_once($intranet_root."/lang/sba_lang.{$this_langFile}.php");

?>

var Lang = new Array();
Lang["IES"] = new Array();

Lang['IES']['AddRow'] = "<?=$Lang['IES']['AddRow']?>";
Lang['IES']['Answer'] = "<?=$Lang['IES']['Answer']?>";

Lang['IES']['Delete'] = "<?=$Lang['IES']['Delete']?>";
Lang['IES']['DuplicateOptions'] = "<?=$Lang['IES']['DuplicateOptions']?>";
Lang['IES']['DuplicateQuestions'] = "<?=$Lang['IES']['DuplicateQuestions']?>";

Lang['IES']['MoveDown'] = "<?=$Lang['IES']['MoveUp']?>";
Lang['IES']['MoveUp'] = "<?=$Lang['IES']['MoveDown']?>";

Lang['IES']['Question'] = "<?=$Lang['IES']['Question']?>";
Lang['IES']['QuestionTitle'] = "<?=$Lang['IES']['QuestionTitle']?>";