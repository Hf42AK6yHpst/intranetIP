// Question Manager Constructor
function question_manager()
{
  this.basePath = null;
  this.qType = null;
  this.langFile = null;

  // Function to initialize questionnaire
  this.load = function(basePath, qTypeJSon, langFile)
  {
	langFile = langFile || '';
    this.basePath = basePath;
	this.langFile = langFile;
    this.loadLang()
    this.loadQType(qTypeJSon);
    this.loadJS();
  }
  
  this.loadLang = function()
  {
    var basePath = this.basePath;
	var langFile = this.langFile;
  
   langFile = (langFile == '') ? "question_lang.js.php" : langFile;

    // Base class
    $("head script").append(
      $("<script></script>").
        attr("language", "JavaScript").
        attr("src", basePath+"/"+langFile)
    );  
  }
  
  this.loadQType = function(qTypeJSon)
  {
    this.qType = qTypeJSon;
  }
    
  // Function to automatically load question libraries
  this.loadJS = function()
  {
    //var head= document.getElementsByTagName('head')[0];
    var basePath = this.basePath;
   
    // Base class
    $("head script").append(
      $("<script></script>").
        attr("language", "JavaScript").
        attr("src", basePath+"/question_base.js")
    );
    
    // MC base class
    $("head script").append(
      $("<script></script>").
        attr("language", "JavaScript").
        attr("src", basePath+"/question_mc.js")
    );

    // Table base class
    $("head script").append(
      $("<script></script>").
        attr("language", "JavaScript").
        attr("src", basePath+"/question_table.js")
    );

    // Various question type class
    $.each(this.qType, function(key, val){
      var _obj_name = val[0];
      
      $("head script").append(
        $("<script></script>").
          attr("language", "JavaScript").
          attr("src", basePath+"/"+_obj_name+".js")
      );
    });
  
  };
  
  this.randomString = function() {
  	var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
  	var string_length = 8;
  	var randomstring = '';
  	for (var i=0; i<string_length; i++) {
  		var rnum = Math.floor(Math.random() * chars.length);
  		randomstring += chars.substring(rnum,rnum+1);
  	}
  	return randomstring;
  };
  
  this.getQueTypeObjName = function(ParQType){
    var objName = "";
  
    $.each(this.qType, function(key, val){
      if(ParQType == key)
      {
        objName = val[0];
        return false;
      }
    });
    
    return objName;
  }
  
  this.parseQuestionStr = function(qStr)
  {
    // retrieve question array
    var questions = qStr.split("#QUE#");
    var q_arr = new Array();
    
    for(var i=1; i<questions.length; i++){
      var que = questions[i];
      
      if(que == "") continue;
      
      // retrieve question detail
      var q_type = que.split("||")[0].split(",")[0];
      
      q_arr[q_arr.length] = 
        {
          "qType" : q_type,
          "qRawStr" : que
        };
    }

    return q_arr;
  };
}
