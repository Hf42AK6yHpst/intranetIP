<?php

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
// Survey type config by IES (may change later)
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");

$li = new libies();

$json = new JSON_obj();
$json_str = $json->encode($ies_cfg["Questionnaire"]["Question"]);

$dir_path = dirname($_SERVER["PHP_SELF"]);

header("Content-Type: text/xml");
$XML =  $li->generateXML(
          array(
            array("BasePath", $dir_path),
            array("QTypeJson", $json_str)
          ),
          false
        );
echo $XML;

?>