// MC Question Base Constructor
function question_table()
{
}

question_table.prototype = new question_base();
question_table.prototype.qOptNum = 0;
question_table.prototype.qQueNum = 0;
question_table.prototype.qOpt = new Array();
question_table.prototype.qQue = new Array();
question_table.prototype.genOptionNumSelection = genOptionNumSelection;
question_table.prototype.genQuestionNumSelection = genQuestionNumSelection;
question_table.prototype.genQuestionString = genQuestionString;
question_table.prototype.setOptionNum = setOptionNum;
question_table.prototype.setQuestionNum = setQuestionNum;
question_table.prototype.getOptionNum = getOptionNum;
question_table.prototype.getQuestionNum = getQuestionNum;
question_table.prototype.setTableQuestion = setTableQuestion;
question_table.prototype.setOption = setOption;
question_table.prototype.parseQuestionString = parseQuestionString;

/**********************************************************************
 * Methods of this class
 **********************************************************************/
// Number of questions selection (Y-axis)
function genQuestionNumSelection() {
  var x = "";
  
  for(var i=2; i<=10; i++)
  {
    x += "<option value=\""+i+"\">"+i+"</option>";
  }
  
  return x;
}

// Number of choices selection (X-axis)
function genOptionNumSelection() {
  var x = "";
  
  for(var i=2; i<=10; i++)
  {
    x += "<option value=\""+i+"\">"+i+"</option>";
  }
  
  return x;
}

function genQuestionString() {
  var x = "#QUE#";
  x += this.qType+","+this.qQueNum+","+this.qOptNum+"||"+this.q+"||";
  for(var i=0; i<this.qQueNum; i++)
  {
    var _que = this.qQue[i];
  
    x += "#OPT#"+_que;
  }
  for(var i=0; i<this.qOptNum; i++)
  {
    var _opt = this.qOpt[i];
  
    x += "#OPT#"+_opt;
  }
  
  return x;
}

function setOptionNum(num) {
  this.qOptNum = parseInt(num);
}

function setQuestionNum(num) {
  this.qQueNum = parseInt(num);
}

function getOptionNum() {
  return parseInt(this.qOptNum);
}

function getOptionNum() {
  return parseInt(this.qOptNum);
}

function getQuestionNum() {
  return parseInt(this.qQueNum);
}

function parseQuestionString(rawQStr) {
  var qType = rawQStr.split("||")[0].split(",")[0];
  var qQueNum = rawQStr.split("||")[0].split(",")[1];
  var qOptNum = rawQStr.split("||")[0].split(",")[2];
  var q = rawQStr.split("||")[1];
  var qQueOpt = rawQStr.split("||")[2].split("#OPT#");
  qQueOpt.shift();
  var qQue = qQueOpt.slice(0,qQueNum);
  var qOpt = qQueOpt.slice(qQueNum);
    
  this.qType = qType;
  this.qQueNum = parseInt(qQueNum);
  this.qOptNum = parseInt(qOptNum);
  this.q = q;
  this.qQue = qQue;
  this.qOpt = qOpt;
}

function setTableQuestion(que) {
  this.qQue = que;
}

function setOption(opt) {
  this.qOpt = opt;
}