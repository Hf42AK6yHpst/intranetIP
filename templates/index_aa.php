<?
// kenneth chung
@session_start();
if (is_file("../servermaintenance.php") && !$iServerMaintenance)
{
    header("Location: ../servermaintenance.php");
    exit();
}

include_once("../includes/global.php");
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>eClass Integrated Platform 2.5</title>
<style type="text/css">
<!--
body {
	background-image: url(../images/bg.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	margin: 0px;
	padding: 0px;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #000000;
}
table {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #000000;
}
.error_msg {
	font-size: 11px;
	line-height: 20px;
	color: #FF0000;
	font-weight: bold;
}
.Go_link {

	display:block;
	padding-right:75px;
	padding-top:55px;
	text-align:right;
}
.Go_link a{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #003399;
	text-decoration: none;
}
.Go_link a:hover{
	color: #FF0000;
}
a.forgot{
	font-size: 11px;
	color: #3bac24;
	text-decoration: none;
}
a.forgot:hover {
	color: #FF0000;
}
.input_box{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	border: 1px solid #999999;
	padding:2px;
}
.copyright {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #FFFFFF;
	text-align: right;
	padding-right: 65px;
	vertical-align: top;
	padding-top: 5px;
}
.login_btn{
	height:26px;
	width:61px;
	display:block;
	float:right;
	margin-right: 20px;
	background-image:url(../images/btn_bg.gif);
	line-height:26px;
	color:#666666;
	font-weight:bold;
	text-align:center;
	text-decoration:none;
}
.login_btn:hover{
	background-image:url(../images/btn_bg_on.gif);
	color:#FF0000;
}
-->
</style>
<script language=JavaScript src=/lang/script.en.js></script>
<script language=JavaScript1.2 src=/templates/script.js></script>
<script language="javascript">
function checkLoginForm(){
	var obj = document.form1;
	var pass=1;
	if(!check_text(document.getElementById('UserLoginInput'), "Please enter the user login."))
		pass  = 0;
	else if(!check_text(document.getElementById('UserPasswordInput'), "Please enter the password."))
		pass = 0;
		
	if(pass)	{
		obj.UserLogin.value = document.getElementById('UserLoginInput').value;
		obj.UserPassword.value = document.getElementById('UserPasswordInput').value;
		obj.submit();
	}
}

function checkForgetForm(){
     obj = document.form2;
     tmp = prompt("Please type in your LoginID or email", "");
     if(tmp!=null && Trim(tmp)!=""){
          obj.UserLogin.value = tmp;
          obj.submit();
     }
}

function checkKeyEnterLogin(e)
{
	var keynum;
	var keychar;
	
	if(window.event) // IE
	{
		keynum = e.keyCode;
	}
	else if(e.which) // Netscape/Firefox/Opera
	{
		keynum = e.which;
	}
	keychar = String.fromCharCode(keynum);
	
	if (keynum==13)		{//Enter => Submit
		checkLoginForm();
	}
}

</script>
</head>

<body>

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
	<tr>
		<td onMouseOver="MM_preloadImages('../images/btn_bg_on.gif')"><table width="720" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td height="90"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="494"><img src="../images/board_01.jpg" width="494" height="90"/></td>
							<td valign="top" background="../images/board_02.jpg"><span class="Go_link">&nbsp;
							<? if(($plugin['attendancestudent'] && $plugin['ppc_attendance']) || $plugin['ppc_eclass'] || ($plugin['Disciplinev12'] && $plugin['ppc_disciplinev12'])){?>
							<a href="/ppc/">PocketPC Version<img src="../images/icon_palm.gif" width="26" height="22" border="0" align="absmiddle"></a>
							<? } ?>
							</span></td>
						</tr>
					</table></td>
				</tr>
			<tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="279"><img src="../images/board_03.jpg" width="279" height="154" /></td>
						<td valign="top" background="../images/board_04.jpg"><span class="error_msg">&nbsp;
						<?php if($err==1) { ?><font size=1 color=red><b>Incorrect LoginID/Password.</b></font><br><?php } ?>
						<?php if($msg==1) { ?><font size=1 color=green><b>Password has been sent.</b></font><br><?php } ?>
						<?php if($err==2) { ?><font size=1 color=red><b>Invalid LoginID.</b></font><br><?php } ?>
						</span><br>
							<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td width="100">Login ID</td>
								<td width="250"><input name="UserLoginInput" type="text" class="input_box" id="UserLoginInput" size="30" maxlength="100" onkeydown="checkKeyEnterLogin(event);"></td>
							</tr>
							<tr>
								<td>Password</td>
								<td><input name="UserPasswordInput" type="Password" class="input_box" id="UserPasswordInput" size="30" maxlength="20" onkeydown="checkKeyEnterLogin(event);"></td>
							</tr>
							<tr>
								<td colspan="2"><img src="../images/login_line.jpg" width="350" height="12"></td>
								</tr>
							<tr>
								<td valign="top">
								<? if ($intranet_authentication_method!="HASH") {?>
								<a href="javascript:checkForgetForm();" class="forgot">forgot password?</a>
								<? } ?>
								</td>
								<td><a href="javascript:checkLoginForm();" class="login_btn">Login</a>
								</td>
							</tr>
						</table></td>
					</tr>
				</table>
					</td>
				</tr>
			<tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="120"><img src="../images/board_05.jpg" width="120" height="97" /></td>
						<td background="../images/board_06.jpg" class="copyright">Copyright &copy; 2009 BroadLearning Education (Asia) Limited.
							All rights reserved.</td>
					</tr>
				</table></td>
				</tr>
		</table></td>
	</tr>
</table>
<form name="form1" action="../login.php" method="post">
	<input name="UserLogin" type="hidden" id="UserLogin">
	<input name="UserPassword" type="hidden" id="UserPassword">
	<input type="hidden" name="url" value="/templates/index.php?err=1">
</form>


<form name=form2 action=../forget.php method=post>
<input type=hidden name=UserLogin>
<input type=hidden name=url_success value="/templates/index.php?msg=1">
<input type=hidden name=url_fail value="/templates/index.php?err=2">
</form>
</html>
</body>

<SCRIPT language=javascript>
function openForgetWin () {
win_size = "resizable,scrollbars,status,top=40,left=40,width=650,height=600";
url = null;
if (url != null && url != "")
{
    var forgetWin = window.open (url, 'forget_password', win_size);
    if (navigator.appName=="Netscape" && navigator.appVersion >= "3") forgetWin.focus();
}
}

document.getElementById('UserLoginInput').focus();
<?php if($msg==1) { ?>openForgetWin();<?php } ?>


</script>

