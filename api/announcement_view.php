<?php
// using : Bill

/********************** Change Log ***********************/
#
#   Date    :   2020-10-16  Bill
#               hide Listen Now button in webview
#
#   Date    :   2020-08-25  Bill    [2020-0821-1028-21206]
#               fixed cannot access urls with 'https://' due to jquery mobile event listeners
#
#   Date    :   2019-10-14  Bill    [DM#3681]
#               fixed cannot access urls with '#' due to jquery mobile event listeners
#
#	Date    :	2019-05-10 (Bill)
#               - prevent SQL Injection
#               - apply intranet_auth()
#
#   Date    :   2019-03-06  (Bill)
#               for auth checking - get token from returnSimpleTokenByDateTime()     ($special_feature['download_attachment_add_auth_checking'])
#
#   Date     : 2019-01-15 (Tiffany)
#               Support Description for Eng when $sys_custom['SchoolNews']['DisplayEngTitleAndContent'] = true
#
#	Date	:	2014-02-10 [Kin]
#				modified the table to mobile UI
#
/********************** end of Change Log ***********************/

//$NoUTF8 = true;
$PATH_WRT_ROOT = "../";

//set language
@session_start();

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();

$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

//if (file_exists($PATH_WRT_ROOT."file/eclass_app.php"))
//{
//	session_start();
//	include_once($PATH_WRT_ROOT."file/eclass_app.php");
//	$intranet_session_language = $eclass_app_language;
//}
//else {
//	$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
//	if ($parLang != '') {
//		switch (strtolower($parLang)) {
//			case 'en':
//				$intranet_hardcode_lang = 'en';
//				break;
//			default:
//				$intranet_hardcode_lang = 'b5';
//		}
//	}
//}

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libschoolnews.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."lang/DHL/dhl_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");

//$AnnouncementID = 571;
//$StudentID = 1971;
//$CurID = 2981;

### Handle SQL Injection + XSS [START]
$AnnouncementID = IntegerSafe($AnnouncementID);
$StudentID = IntegerSafe($StudentID);
$CurID = IntegerSafe($CurID);
### Handle SQL Injection + XSS [END]

$UserID = $CurID;
$_SESSION['UserID'] = $CurID;

$delimiter = "###";
$keyStr = $special_feature['ParentApp_PrivateKey'].$delimiter.$AnnouncementID.$delimiter.$StudentID.$delimiter.$CurID;
if($token != md5($keyStr)) {
	include_once($PATH_WRT_ROOT."lang/lang.en.php");
	echo $i_general_no_access_right	;
	exit;
}

# Generate token for auth checking
$dl_auth_token = '';
$token_parms = '';
if($special_feature['download_attachment_add_auth_checking'])
{
    $dl_auth_token = returnSimpleTokenByDateTime();
    $token_parms = "&token=".$dl_auth_token;
    
//     unset($_SESSION['dlToken']);
//    
//     $dl_auth_token = md5($keyStr.$delimiter.time());
}

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $ip20_what_is_new;
$linterface = new interface_html("popup6.html");
$linterface->LAYOUT_START();

// retrieve latest settings every time
unset($_SESSION["SSV_PRIVILEGE"]["SchoolNews"]);
$lschoolnews = new libschoolnews();
$libannounce = new libannounce($AnnouncementID);
//echo $libannounce->display2();

if ($libannounce->AnnouncementID == '') {
	$warningMsg = $Lang['SysMgr']['SchoolNews']['SchoolNewsDeleted'];
	echo '<div style="padding:5px;">'.$warningMsg.'</div>';
	die();
}
else if ( !$lschoolnews->allowUserToViewPastNews && $libannounce->EndDate < date('Y-m-d') ) {
	$warningMsg = $Lang['SysMgr']['SchoolNews']['SchoolNewsHasExpired'];
	echo '<div style="padding:5px;">'.$warningMsg.'</div>';
	die();
}
else if (date('Y-m-d H:i:s') < $libannounce->AnnouncementDate) {
	$warningMsg = $Lang['SysMgr']['SchoolNews']['SchoolNewsNotStart'];
	$warningMsg = str_replace('<!--startDate-->', $libannounce->AnnouncementDate, $warningMsg);	
	echo '<div style="padding:5px;">'.$warningMsg.'</div>';
	die();
}

$announcer = $libannounce->returnAnnouncerName();
$ownerGroup = $libannounce->returnOwnerGroup();
$targetGroups = $libannounce->returnTargetGroups();
if ($ownerGroup == "")
{
	$displayName = $announcer;
}
else
{
	$displayName = "$announcer ($ownerGroup)";
}
if (sizeof($targetGroups)==0){
	if($sys_custom['DHL']){
		$target= $Lang['eClassApp']['AllStaffs'];
	}else{
		$target = $i_AnnouncementWholeSchool;
	}
}
else
{
    $target = implode(", ", $targetGroups);
}
	$titlecss = ($colortype==0) ? "indexnewslisttitle" : "indexnewslistgrouptitle";
	
	$x .= "<div data-role='page' id='pageone'>";
	$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">'."\n";
	$x .= '<tr>'."\n";
	$x .= '<td>'."\n";
	$x .= '<table bgcolor="#FFFFFF" width="100%" border="0" cellpadding="0" cellspacing="0">'."\n";
	$x .= '<tr valign="top">'."\n";
    if($sys_custom['SchoolNews']['DisplayEngTitleAndContent']&&$parLang=="en") {
        $x .= '<td style="padding:10px;font-size:22px"><strong>'.$libannounce->TitleEng.'</strong></td>'."\n";
    }else{
        $x .= '<td style="padding:10px;font-size:22px"><strong>'.$libannounce->Title.'</strong></td>'."\n";
    }
	$x .= '</tr>'."\n";
	$x .= '<tr valign="top">'."\n";
	$x .= '<td style="width:50%; text-align:left; vertical-align:bottom; font-size:16px;padding:10px;">'."\n";
	$x .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_date.png" align="absmiddle" style="width:20px; height:20px;" />'."\n";
	$x .= '<span style="vertical-align:bottom;">'.$libannounce->AnnouncementDate.'</span>'."\n";
	$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	$x .= '<tr valign="top">'."\n";
	if($sys_custom['SchoolNews']['DisplayEngTitleAndContent']&&$parLang=="en"){
	    $x .= '<td bgcolor="#FFFFFF" style="font-size:16px; padding:10px;"><div id="descriptionDiv" style="width: 100%; overflow-x: auto; overflow-y: hidden;"><span class="tabletext2">'.HTML_Transform(htmlspecialchars_decode($libannounce->DescriptionEng)).'</span></div></td>'."\n";
    }else{
        $x .= '<td bgcolor="#FFFFFF" style="font-size:16px; padding:10px;"><div id="descriptionDiv" style="width: 100%; overflow-x: auto; overflow-y: hidden;"><span class="tabletext2">'.HTML_Transform(htmlspecialchars_decode($libannounce->Description)).'</span></div></td>'."\n";
    }
	$x .= '</tr>'."\n";
	if ($libannounce->VoiceFile != "")
	{  	$x .= '<tr valign="top">'."\n";
	$x .= '<td bgcolor="#FFFFFF" style="font-size:16px; padding:10px;">'."\n";
	$x .= '<div class="ui-block-a" style="width:100%;">'."\n";
	
	//basename and dirname may work for UTF8.
	//$VoiceFileName = basename($libannounce->VoiceFile);
	//$VoiceDirName1 = dirname($libannounce->VoiceFile);
	//$VoiceDirName2 = basename($VoiceDirName1);
	$VoiceFileName = get_file_basename($libannounce->VoiceFile);
	
	$VoiceDirName1 = substr($libannounce->VoiceFile, 0, strrpos($libannounce->VoiceFile, '/') === false ? strlen($libannounce->VoiceFile) : strrpos($libannounce->VoiceFile, '/'));
	$VoiceDirName2 = get_file_basename($VoiceDirName1);;
	
	$path = "$file_path/file/announcement/".$VoiceDirName2;
	
	$target_filepath = $path."/".$VoiceFileName;
	
	$url = str_replace($file_path, "", $path)."/".$VoiceFileName;
	$url = intranet_handle_url($url);
	
	$VoiceClip = " <img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_voice.gif\" border=\"0\" align=\"absmiddle\" /> ";

	//$displayHtml .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$VoiceFileName."';return true;\" onMouseOut=\"window.status='';return true;\">".$VoiceFileName."</a>";
//	$displayHtml .= "<a class=\"indexpoplink\" href=\"javascript:listenPVoice('$intranet_httppath$url')\" onMouseOver=\"window.status='".$VoiceFileName."';return true;\" onMouseOut=\"window.status='';return true;\">".$VoiceClip.$VoiceFileName."</a>";
//
//
//	$x .= "                        <span><strong>".$iPowerVoice['sound_recording']."</strong></span>";
//	$x .= "                        <span>". $displayHtml."</span>";

	$x .= '</div>'."\n";
	$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	}
	
	if ($libannounce->Attachment != "")
	{  $x .= '<tr valign="top">'."\n";
	$x .= '<td bgcolor="#FFFFFF" style="font-size:16px; padding:10px;">'."\n";
	$x .= '<div class="ui-block-a" style="width:100%;">'."\n";
	
	$hasOther = false;
	$path = "$file_path/file/announcement/".$libannounce->Attachment.$libannounce->AnnouncementID;
	$real_path = $libannounce->Attachment.$libannounce->AnnouncementID;
	$templf = new libfiletable("", $path,0,0,"");
	$files = $templf->files;
	
	while (list($key, $value) = each($files))
	{
		if ($libannounce->VoiceFile != "")
		{
			if (basename($libannounce->VoiceFile)==$files[$key][0])
			{
				continue;
			}
		}
		
		$target_filepath = "$path/".$files[$key][0];
		if (!isImage($target_filepath))
		{
			$url = str_replace($file_path, "", $real_path)."/".$files[$key][0];
			
			//$url = intranet_handle_url($url);
			// 								$url = $url;
			//$url = rawurlencode($file_path."/file/announcement/".$url);
			if (getFileExtention($url)=='MP4') {
				$tmpTargetPath = curPageURL($withQueryString=false, $withPageSuffix=false)."/file/announcement/".$url;
			}
			else {
				$url = $file_path."/file/announcement/".$url;
				$tmpTargetPath = "/home/download_attachment.php?target_e=".getEncryptedText($url).$token_parms;
			}
			
			$displayOther .= "<img src=$image_path/$LAYOUT_SKIN/index/whatnews/icon_attachment.gif hspace=2 vspace=2 border=0 align=absmiddle>";
			//$displayOther .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
			//$displayOther .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"/home/download_attachment.php?target_e=".getEncryptedText($url)."\" >".$files[$key][0]."</a>";
			$displayOther .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"".$tmpTargetPath."\" >".$files[$key][0]."</a>";
			$displayOther .= " (".ceil($files[$key][1]/1000)."Kb)";
			$displayOther .= "</br>\n";

			/*
			 * hide Listen Now button
			$audio_url = str_replace($file_path, "", $path)."/".$files[$key][0];
			$audio_url = intranet_handle_url($audio_url);
			$listen = strpos($audio_url,".mp3");
			if ($listen != FALSE){
				$displayOther .= "
				<INPUT type='button' class='formbutton' valign = 'center' value='Listen Now' onClick=\"window.open('http://$eclass40_httppath/src/tool/powervoice/pvoice_listen_now_intranet.php?from_intranet=1&location=$audio_url','mywindow','width=400,height=200')\">
				";
			}
			*/

			$hasOther = true;
			$attNameDisplay = false;
		}
	}
	
	if ($hasOther)
	{
		$x .= "<br>";
		$x .= $displayAttachment2;
		//$x .= "<br>";
		$x .= $displayOther;
	}
	
	### img file
	$hasImage = false;
	
	$path = "$file_path/file/announcement/".$libannounce->Attachment.$libannounce->AnnouncementID;
	$templf = new libfiletable("", $path,0,0,"");
	$files = $templf->files;
	
	while (list($key, $value) = each($files))
	{
		$target_filepath = "$path/".$files[$key][0];
		if (isImage($target_filepath))
		{
			$url = str_replace($file_path, "", $path)."/".$files[$key][0];
			// 								$url = intranet_handle_url($url);
			
			$size = GetImageSize($target_filepath);
			list($width, $height, $type, $attr) = $size;
			
			$image_html_tag = "";
			$href_start	= "<a class=\"indexpoplink\" href=\"$intranet_httppath$url\" target=_blank onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">";
			$href_end	= "</a>";
			
			if($width > 300)
				$image_html_tag = "style='width: 100%'";
				
				$displayAttachment .= "<span><strong>". ($attNameDisplay ? $i_AnnouncementAttachment : "") ."</strong></span>";
				$displayImage .= "<span>";
				$displayImage .= "<img src=$image_path/$LAYOUT_SKIN/index/whatnews/icon_attachment.gif hspace=2 vspace=2 border=0 align=absmiddle>";
				//                                 $displayImage .= $href_start . $files[$key][0] . "<br \>";
				// 								$url = str_replace($file_path, "", $path)."/".$files[$key][0];
				// 	                        	$url_download = rawurlencode($file_path.$url);
				// 								$displayImage .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"/home/download_attachment.php?target=".$url_download."\" >".$files[$key][0]."</a><br \>";
				$displayImage .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"/home/download_attachment.php?target_e=".getEncryptedText($file_path.$url).$token_parms."\" >".$files[$key][0]."</a><br \>";
				$displayImage .= "<img border=0 src=\"$url\" $image_html_tag>".$href_end;
				$displayImage .= "</span>\n";
				
				$hasImage = true;
				$attNameDisplay = false;
		}
	}
	
	if ($hasImage)
	{
		//	$x .= "<br>";
		$x .= $displayAttachment;
		$x .= "<br>";
		$x .= $displayImage;
	}
	
	$x .= "</span>";
	$x .= '</div>'."\n";
	$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	}
	$x .= '</table>'."\n";
	$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	$x .= '</table>'."\n";
	
	if($ss_intranet_plugin['power_speech'] && trim($libannounce->Description)!="&amp;nbsp;"){
		$x .= generate_texttospeech_ui_IP('announce_msg');
	}
	
	$x .= "<div data-role='collapsible' data-collapsed='true' style='font-size:18px;'>";
	$x .= "<h1>".$Lang['General']['Misc']."</h1>";
	$x .= "<div class='ui-grid-a'>
		 <div class='ui-block-a' style='width:100%;'>
			<span><strong>". $i_AnnouncementOwner ."</strong></span>
			<br>
			<span>". intranet_wordwrap($displayName,30,"\n",1) ."</span>
	     </div>
		 <div class='ui-block-a' style='width:100%;'>
			<br>
    	 	<span><strong>". $i_AnnouncementTargetGroup ."</strong></span>
			<br>
     		<span>". intranet_wordwrap($target,30,"\n",1) ."</span>
		 </div>
	 	 <div class='ui-block-a' style='width:100%;'>
       ";
	$x .="
	 </div>
   </div>
</div>";
	
	if ($CurID != '') {
		$sql = "UPDATE INTRANET_ANNOUNCEMENT SET ReadFlag = CONCAT(ifnull(ReadFlag,''),';$CurID;') WHERE AnnouncementID = $AnnouncementID AND (INSTR(ReadFlag,';$CurID;') = 0 OR ReadFlag IS NULL)";
		$libannounce->db_db_query($sql);
	}
	
// 	if($special_feature['download_attachment_add_auth_checking'] && $dl_auth_token != '')
// 	{
// 	    $_SESSION['dlToken'] = $dl_auth_token;
// 	}
	
	/*$linterface->LAYOUT_STOP();*/
	intranet_closedb();
	
	?>

<SCRIPT LANGUAGE=Javascript>
jQuery(document).ready(function($) {
	$('div#descriptionDiv').css('width', Get_Screen_Width() - 10);
});
</SCRIPT>

<style type="text/css">
.tabletext {
	font-size: 16px;
}
.tabletext2 {
	font-size: 16px;
}
.tabletextremark {
	font-size: 16px;
}
</style>

<div id="spanTemp"></div>

<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">

<script>
    $(function(){
        $('div#descriptionDiv a.ui-link').filter(function() {
            return ($(this).attr('href').indexOf('https://') != -1 || $(this).attr('href').indexOf('#') != -1);
        })
        .attr('rel', 'external');
    });
</script>

	<?=$x?>
</body>
</html>