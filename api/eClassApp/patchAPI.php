<?
// Modifying by: 
$PATH_WRT_ROOT = "../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once ($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_settings.php");

intranet_opendb();


$action = $_REQUEST['a'];
$actionTime = $_REQUEST['at'];
$fakeparam = $_REQUEST['fp'];
$token = $_REQUEST['t'];
$correctToken = hash('md5', $action.'###'.$actionTime.$eclassAppConfig['urlSalt'].$fakeparam);

if ($token != $correctToken) {
	echo 'Invalid parameter 1';
}
else {
	$leClassApp = new libeClassApp();
	
	if ($action == 'addCentralServerUrl') {
		$serverUrl = $_REQUEST['su'];
		$effectiveDate = $_REQUEST['ed'];
		$callingPage = $HTTP_SERVER_VARS["HTTP_REFERER"];
		
		$success = $leClassApp->addCentralServerSettings($serverUrl, $effectiveDate, $callingPage);
		echo ($success)? 'Y' : 'N';
	}
	else if ($action == 'checkCentralServerConnection') {
		$centralServerUrl = $leClassApp->getCurCentralServerUrl();
		$checkConnectionApi = str_replace('index.php', 'checkConnect.php', $centralServerUrl);
		
		session_write_close();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $checkConnectionApi);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		$responseString = curl_exec($ch);
		curl_close($ch);
		
		echo $centralServerUrl.'###'.$responseString;
	}
	else {
		echo 'Invalid parameter 2';
	}
}

intranet_closedb();
?>