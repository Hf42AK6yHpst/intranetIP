<?php
// Editing by 
/*
 * 2017-11-17 (Carlos): Created for CEES.
 */
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclassapiauth.php");
include_once($PATH_WRT_ROOT."includes/cees/libcees_kis_api.php");
include_once($PATH_WRT_ROOT."includes/json.php");

$json = new JSON_obj();
$libapi = new libcees_api();
$lapiAuth = new libeclassapiauth();
		
header("Content-Type: application/json;charset=utf-8");

$basicApiKey = $lapiAuth->GetAPIKeyByProject('CEES');

if(!isset($_REQUEST['AccessTime']) || $_REQUEST['AccessTime']=='' || !isset($_REQUEST['AccessCode']) || $_REQUEST['AccessCode']=='')
{
	$ary = array('Error'=>'Unauthorized access.');
	$output = $json->encode($ary);
	echo $output;
    exit();
}

$now_ts = time();
$access_ts = intval($_REQUEST['AccessTime']);
$threshold_period = 10; // seconds
if($now_ts - $access_ts > $threshold_period){
	$ary = array('Error'=>'Access timeout.');
	$output = $json->encode($ary);
	echo $output;
    exit();
}

$hashed_code = sha1($basicApiKey.'_'.$_REQUEST['AccessTime']);
if( $_REQUEST['AccessCode'] != $hashed_code ){
	$ary = array('Error'=>'Invalid access code.');
	$output = $json->encode($ary);
	echo $output;
    exit();
}

if(!isset($_REQUEST['Method']) || $_REQUEST['Method']=='' || !method_exists($libapi, $_REQUEST['Method']) || !is_callable(array($libapi,$_REQUEST['Method'])) ){
	$ary = array('Error'=>'Invalid usage.');
	$output = $json->encode($ary);
	echo $output;
    exit();
}

intranet_opendb();

function convertDataEncoding($data, $abs, $dir)
{
	if(is_array($data) && count($data)>0){
		foreach($data as $key => $val){
			if(!is_array($val)){
				$data[$key] = convert2unicode($val, $abs, $dir);
			}else if(is_array($val)){
				$data[$key] = convertDataEncoding($val, $abs, $dir);
			}
		}
	}else if(!is_array($data) && is_string($data)){
		$data = convert2unicode($data, $abs, $dir);
	}
	return $data;
}

$ary = array();

$Method = $_REQUEST['Method'];
$args = count($_POST)>0? $_POST : $_GET;
unset($args['Method']);

$ary = call_user_func_array(array($libapi,$Method), array($args));

if(($intranet_version == "2.0" || $eclass_version==3.0) && count($ary)){
	// convert EJ BIG5 or GB encoding to UTF-8
	$ary = convertDataEncoding($ary,1,1);
}

$output = $json->encode($ary);

intranet_closedb();
echo $output;
?>