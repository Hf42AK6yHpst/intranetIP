<?php
// using : 
/*
 * Modification Log:
 * 	Date : 2015-10-02 Bill
 * 		 - copy from eclasslearning
 */
 
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/json.php");

// if state not correct or return empty authorization code, redirect to homepage
if(!$ssoservice["HKEdCity"]["Valid"] || $_GET["code"]=="" || ($_GET["state"]!="bllogin" && $_GET["state"]!="blcheck") || ($_GET["state"]=="blcheck" && $UserID==""))
{
	header("Location: /");
	exit();
}

// Create JSON object
$jsonObj = new JSON_obj();

# User - Get Access Token
// Parms to get Access Token
$token_url = $ssoservice["HKEdCity"]["token_path"];
$token_parms = array();
$token_parms["code"] = $_GET["code"];
$token_parms["client_id"] = $ssoservice["HKEdCity"]["client_id"];
$token_parms["client_secret"] = $ssoservice["HKEdCity"]["client_secret"];
$token_parms["redirect_uri"] = $ssoservice["HKEdCity"]["redirect_uri"];
$token_parms["grant_type"] = "authorization_code";

// Set cURL Option
$c_token = curl_init();
curl_setopt($c_token, CURLOPT_URL, $token_url);
curl_setopt($c_token, CURLOPT_POST, true);
curl_setopt($c_token, CURLOPT_HEADER, false);
curl_setopt($c_token, CURLOPT_HTTPHEADER, array('Expect:'));
curl_setopt($c_token, CURLOPT_RETURNTRANSFER, true);
curl_setopt($c_token, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($c_token, CURLOPT_POSTFIELDS, $token_parms);

// Perform cURL
$token_return = curl_exec($c_token);
$token_return = $jsonObj->decode($token_return);
curl_close($c_token);

// if return empty access token, redirect to homepage
if($token_return["access_token"]=="")
{
	header("Location: /");
	exit();
}

# Get User Info
// Parms to get User Info
$userinfo_url = $ssoservice["HKEdCity"]["info_path"]."?access_token=".$token_return["access_token"];

// Set cURL Option
$c_userinfo = curl_init();
curl_setopt($c_userinfo, CURLOPT_URL, $userinfo_url);
curl_setopt($c_userinfo, CURLOPT_HEADER, false);
curl_setopt($c_userinfo, CURLOPT_HTTPHEADER, array('Expect:'));
curl_setopt($c_userinfo, CURLOPT_RETURNTRANSFER, true);
curl_setopt($c_userinfo, CURLOPT_SSL_VERIFYPEER, false);

// Perform cURL
$user_content = curl_exec($c_userinfo);
$user_content = $jsonObj->decode($user_content);
curl_close($c_userinfo);

// if return empty HKEdCity UserID, redirect to homepage
if($user_content["id"]=="")
{
	header("Location: /");
	exit();
}

// Redirect to correct page according to return state
$targetPath = $ssoservice["HKEdCity"]["dev"]==""? $PATH_WRT_ROOT : $ssoservice["HKEdCity"]["dev"];
if($_GET["state"]=="bllogin")
	header("Location: $targetPath/login.php?HKEdCityID=".getEncryptedText($user_content["id"])."&UsingHKEdCityID=".getEncryptedText("bllogin-success"));
else if($_GET["state"]=="blcheck" && $UserID!="")
	header("Location: $targetPath/home/iaccount/account/hkedcity_edit_update.php?HKEdCityID=".getEncryptedText($user_content["id"])."&UsingHKEdCityID=".getEncryptedText("success-blcheck"));

?>