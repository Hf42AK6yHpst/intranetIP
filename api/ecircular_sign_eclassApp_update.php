<?php
// using :

/********************** Change Log ***********************
 *
 * 20191018 Bill    [2019-0917-1754-39207]
 *  - disable send push message logic after sign    ($sys_custom['eClassTeacherApp']['eCircular']['DisablePushMessageAfterSign'])
 * 20190510 Bill
 *  - prevent SQL Injection
 *  - apply intranet_auth(), token checking
 * 20140827 Ivan
 * 	- copy most logic from /home/web/eclass40/intranetIP25/home/eService/circular/sign_update.php
 */
/********************** end of Change Log ***********************/

$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/user_right_target.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

### Handle SQL Injection + XSS [START]
$CircularID = IntegerSafe($_POST['CircularID']);
$CurUserID = IntegerSafe($_POST['CurUserID']);
$actualTargetUserID = IntegerSafe($_POST['actualTargetUserID']);
### Handle SQL Injection + XSS [END]

$UserID = $CurUserID;
$_SESSION['UserID'] = $CurUserID;

intranet_auth();
intranet_opendb();

$lcircular = new libcircular($CircularID);
$luser = new libuser($CurUserID);

$test = $_POST['test'];
$qStr = $_POST['qStr'];
$aStr = $_POST['aStr'];
// $CircularID = $_POST['CircularID'];
// $actualTargetUserID = $_POST['actualTargetUserID'];
// $CurUserID = $_POST['CurUserID'];
$adminMode = $_POST['adminMode'];

$delimiter = "###";
$keyStr = $special_feature['ParentApp_PrivateKey'].$delimiter.$CircularID.$delimiter.$CurUserID;
if($token != md5($keyStr)) {
    include_once($PATH_WRT_ROOT."lang/lang.en.php");
    echo $i_general_no_access_right	;
    exit;
}

if ($actualTargetUserID == "") {
    include_once($PATH_WRT_ROOT."lang/lang.en.php");
	echo $i_general_no_access_right	;
	exit;
}

$lform = new libform();
$lcircular = new libcircular($CircularID);
$LibAdminJob = new libadminjob($CurUserID);
$UserRightTarget = new user_right_target();

# Check admin level
$isAdmin = false;
$_SESSION["SSV_USER_ACCESS"] = $UserRightTarget->Load_User_Right($CurUserID);
$_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] = $LibAdminJob->isCircularAdmin();
if($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"]) {
	$_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"] = $LibAdminJob->returnAdminLevel($CurUserID, $LibCircular->admin_id_type);
}
if ($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"])
{
    $adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"];
    if ($adminlevel==1 || $lcircular->IssueUserID == $CurUserID)     # Allow if Full admin or issuer
    {
        $isAdmin = true;
    }
}
$isAdmin = $isAdmin && ($adminMode==1);

# Check Reply involved
if (!$lcircular->retrieveReply($actualTargetUserID))
{
    include_once($PATH_WRT_ROOT."lang/lang.en.php");
	echo $i_general_no_access_right	;
	exit;
}

$temp_que = str_replace("&amp;", "&", $lcircular->Question);
$queString = $lform->getConvertedString($temp_que);
$queString = trim($queString)." ";

# Check View/Edit Mode
if ($isAdmin)
{
    $js_edit_mode = ($lcircular->isHelpSignAllow);
}
else
{
    $sign_allowed = false;
    $time_allowed = false;
    if ($lcircular->isReplySigned())
    {
        if ($lcircular->isResignAllow)
        {
            $signed_allowed = true;
        }
    }
    else
    {
        $signed_allowed = true;
    }

    $today = date('Y-m-d');
    if (compareDate($today,$lcircular->DateEnd)>0)
    {
        if ($lcircular->isLateSignAllow)
        {
            $time_allowed = true;
        }
    }
    else
    {
        $time_allowed = true;
    }
    $js_edit_mode = ($signed_allowed && $time_allowed);
}

## $ansString = $lform->getConvertedString($lcircular->answer);
$ansString = $lcircular->answer;
$ansString = str_replace("&amp;", "&", $ansString);
$ansString = $lform->getConvertedString($ansString);
$ansString .= trim($ansString)." ";

$targetType = array("",
                    $i_Circular_RecipientTypeAllStaff,
                    $i_Circular_RecipientTypeAllTeaching,
                    $i_Circular_RecipientTypeAllNonTeaching,
                    $i_Circular_RecipientTypeIndividual);

$lu = new libuser($actualTargetUserID);
$valid = false;

if ($js_edit_mode)
{ 
    if ($actualTargetUserID == "")
    {
        // do nothing
    }
    else
    {
        $valid = true;
        
        $t_ansString = intranet_htmlspecialchars(trim($aStr));
        $type = ($isAdmin? 2: 1);
		
		$star = 0;
		$val = $_POST["test"];
		//Sign and Close
		if ($val <3)
		{
			//Sign and Star
			if($val == 2)
			{	
				$star = 1;
			}
		    # Update
			$sql = "UPDATE INTRANET_CIRCULAR_REPLY SET Answer = '$t_ansString',
                RecordType = '$type',RecordStatus = 2,SignerID='$CurUserID',DateModified=now(),HasStar = '$star'
                WHERE CircularID = '$CircularID' AND UserID = '$actualTargetUserID'";
			$lcircular->db_db_query($sql);
		}
    }

    $lcircular->synchronizeSignedCount();

    $lcircular->retrieveReply($actualTargetUserID);
    $ansString =  str_replace("&amp;", "&", $lcircular->answer);
	$ansString = $lform->getConvertedString($ansString);
	$ansString .= trim($ansString)." ";
}
else
{
	$val = $_POST["test"];
	//Unstar
	if($val == 3)
	{
		$star = 0;
	}
	
	//Add Star
	else if($val == 4)
	{
		$star = 1;
	}
	
	# Update
	$sql = "UPDATE INTRANET_CIRCULAR_REPLY SET HasStar = '$star'
				WHERE CircularID = '$CircularID' AND UserID = '$actualTargetUserID'";
	$lcircular->db_db_query($sql);
	die();
}

if ((!$valid)&&($val < 3))
{
    include_once($PATH_WRT_ROOT."lang/lang.en.php");
	echo $i_general_no_access_right	;
	exit;
}

if ($plugin['eClassTeacherApp'] && !$sys_custom['eClassTeacherApp']['eCircularDisablePushMessageAfterSign'])
{
	if (in_array($CurUserID, $luser->getTeacherWithTeacherUsingTeacherApp())) {
		include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
		$leClassApp = new libeClassApp();
		
		$MsgNotifyDateTime = date("Y-m-d H:i:s");
		$MessageTitle = $Lang['AppNotifyMessage']['eCircularSigned'];
		$MessageTitle = str_replace("[sign_notice_number]", intranet_undo_htmlspecialchars($lcircular->CircularNumber), $MessageTitle);
		$MessageContent = str_replace("[sign_datetime]", $MsgNotifyDateTime, $Lang['AppNotifyMessage']['eCircularSignedContent_eClassApp']);
		$MessageContent = str_replace("[sign_notice_number]", intranet_undo_htmlspecialchars($lcircular->CircularNumber), $MessageContent);
		$MessageContent = str_replace("[sign_notice_title]", intranet_undo_htmlspecialchars($lcircular->Title), $MessageContent);
		
		$messageInfoAry = array();
		$messageInfoAry[0]['relatedUserIdAssoAry'][$CurUserID] = array($CurUserID);
		$notifyMessageId = $leClassApp->sendPushMessage($messageInfoAry, $MessageTitle, $MessageContent, $isPublic='', $recordStatus=0, $appType=$eclassAppConfig['appType']['Teacher']);
	}
}

intranet_closedb();
//$linterface->LAYOUT_STOP();

header("Location: ecircular_sign_eclassApp_complete.php");
?>