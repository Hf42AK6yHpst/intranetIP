<meta name="viewport" content="initial-scale=1.0,maximum-scale=3.0,user-scalable=no,width=device-width,height=device-height"> 
<?php
// using :

/********************** Change Log ***********************/
#	Date	:	2014-01-07 [Ivan]
#				modified commented "$editAllowed" to show sign status for all status now 
#	Date	:	2013-08-31 [Yuen]
#				add meta tag above to control the display scale and avoid display problem in Android 
#	Date	:	2013-05-09 [Yuen]
#				customized to require parent to input valid password in order to sign - control by flag $sys_custom['enotice_sign_with_password']
#	Date	:	2013-03-07 [Yuen]
#				force parent to fill in if the slip is required to
#	Date	:	2011-12-14 [Yuen]
#				change layout from 2 columns to 1 to avoid scrolling 
#
/********************** end of Change Log ***********************/

# set as default
if (!isset($sys_custom['enotice_sign_with_password'])) {
	$sys_custom['enotice_sign_with_password'] = false;
}

$NoUTF8 = true;
$PATH_WRT_ROOT = "../";

# see if there is any custom setting
if (file_exists($PATH_WRT_ROOT."file/eclass_app.php"))
{
	session_start();
	include_once($PATH_WRT_ROOT."file/eclass_app.php");
	$intranet_session_language = $eclass_app_language;
} else
{
	$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
	if ($parLang != '') {
		switch (strtolower($parLang)) {
			case 'en':
				$intranet_hardcode_lang = 'en';
				break;
			default:
				$intranet_hardcode_lang = 'b5';
		}
	}
}

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

$delimiter = "###";
$keyStr = $special_feature['ParentApp_PrivateKey'].$delimiter.$NoticeID.$delimiter.$StudentID.$delimiter.$CurID;
/* testing 
$NoticeID = 7285;
$StudentID = 1668;
$CurID = 1707;
*/
/* testing  
 */
if($token != md5($keyStr)) {
	include_once($PATH_WRT_ROOT."lang/lang.en.php");
	echo $i_general_no_access_right	;
	exit;
}


//intranet_auth();
intranet_opendb();

$lform = new libform();
$lnotice = new libnotice($NoticeID);

$lpayment = new libpayment();

# get user id according to session id
//$current_user_id;
//$sql = "SELECT userid FROM INTRANET_USER where SessionKey=";
$lu = new libuser($CurID);
//debug_pr($lu);
$valid = true;
if ($lu->isStudent())
{
    $valid = false;
}

//$isTeacher = $lu->isTeacherStaff();
//$isParent = $lu->isParent();
$isTeacher = false;
$isParent = true;



if ($StudentID!="") {
    if ($isParent) {
        $children = $lu->getChildren();
        if (!in_array($StudentID,$children)) {
        	// no such children
             $valid = false;
        }
        else {
            $today = time();
            $deadline = strtotime($lnotice->DateEnd);
            if (compareDate($today,$deadline)>0) {
            	// after deadline
                if ($lnotice->isLateSignAllow) {
	            	$valid = true;
	            	$editAllowed = true;
	            }
	            else {
                	$valid = false;
                	$editAllowed = false;
                }
            }
            else {
            	// before deadline
                $editAllowed = true;
            }
        }
    }
    else # Teacher
    {
    	if(strtoupper($lnotice->Module) == "PAYMENT")
    	{
    		$editAllowed = false;    
    	}
    	else
    	{
		    if($lnotice->IsModule)
		    {
				$editAllowed = true;    
		    }
		    else
		    {
	        	$editAllowed = $lnotice->isEditAllowed($StudentID);
	    	}
	    }
    }
}
else
{
    if ($isParent)
    {
        $valid = false;
    }
    else
    {
        $editAllowed = false;
    }
}

if ($editAllowed)
{
    if ($StudentID == "")
    {
        $valid = false;
    }
    
    if (($lnotice->TargetType=="S" && !$lu->isStudent()) || ($lnotice->TargetType=="P" && !$lu->isParent()))
    {
    	$editAllowed = false;
    }
}

$lnotice->retrieveReply($StudentID);
# 2010-06-02 check allow re-sign the notice or not
if($lnotice->signerID && $lnotice->NotAllowReSign && !$lnotice->hasFullRight())
{
    $editAllowed = false;
}



$lnotice->retrieveReply($StudentID);
$temp_ans =  str_replace("&amp;", "&", $lnotice->answer);
$ansString = $lform->getConvertedString($temp_ans);
$ansString.=trim($ansString)." ";


/*
if (!$valid)
{
    header("Location: view.php?StudentID=$StudentID&NoticeID=$NoticeID");
    exit();
}
*/

$targetType = array("",$i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual);
#modified by Kelvin Ho 2008-11-12 for ucc
if($lnotice->Module=='')
{
	$attachment = $lnotice->displayAttachment();
}
else
{
	if(strtoupper($lnotice->Module) == "PAYMENT")
		$attachment = $lnotice->displayAttachment();
	else
		$attachment = $lnotice->displayModuleAttachment();
}

$this_Description = $lnotice->Description;


	
$MODULE_OBJ['title'] = $i_Notice_ElectronicNotice2;
$linterface = new interface_html("popup5.html");
$linterface->LAYOUT_START();

$reqFillAllFields = $lnotice->AllFieldsReq;

?>
<style type="text/css">
.tabletext77, .tabletext277, body77, .tablelink77 {
	font-size: 35px;
}
.eNoticereplytitle77, .formbutton_v3077 {
	font-size: 38px;
}
.formbutton_v3077{ 
	height:54px;
}
.tablelink77:hover {
	font-size: 35px;
}
</style>
<SCRIPT LANGUAGE=Javascript>
jQuery(document).ready(function($) {
	
   textboxes = $("input:text");

	if ($.browser.mozilla) {
		$(textboxes).keypress(checkForEnter);
	} else {
		$(textboxes).keydown(checkForEnter);
	}

	function checkForEnter(event) {
		if (event.keyCode == 13) {
			currentTextboxNumber = textboxes.index(this);
	
			if (textboxes[currentTextboxNumber + 1] != null) {
				nextTextbox = textboxes[currentTextboxNumber + 1];
				nextTextbox.select();
			}
	
			event.preventDefault();
			return false;
		}
	}

});

var need2checkform = <? if($reqFillAllFields){ echo "true"; } else { echo "false"; }  ?>;

function SubmitForm(obj)
{
	if (typeof(obj.user_password)!="undefined")
	{
		$.ajax({
			type: "POST",
		    url: 'ajax_authenticate.php',
		    data: { user_password: obj.user_password.value, user_id: obj.CurID.value },
		    error: function(xhr) {
		      alert('Ajax request error' + xhr);
		    },
		    success: function(response) {
		    //alert(response);
		      if ( response=="1")
		      {
		      	if (checkform(document.form1))
		      	{
		      		document.form1.submit();
		      	}
		      } else
		      {
		      	alert("<?=$Lang['eClassAPI']['WarningMsg']['InvalidPassword']?>");
		      	obj.user_password.focus();
		      }
		    }
	  	});
	} else
	{
		if (checkform(document.form1))
      	{
      		document.form1.submit();
      	}
	}
  
}

function checkform(obj)
{
	var form_all_filled = true;
	if (typeof(formAllFilled)!="undefined")
	{
		var form_all_filled = formAllFilled;
	}
	
	<? if (strtoupper($lnotice->Module) == "PAYMENT") { ?>
		
	<? } else { ?>
		if (!need2checkform || form_all_filled)
		 {
		      return true;
		 }
		 else //return true;
		 {
		      alert("<?=$Lang['Notice']['AnswerAllQuestions']?>");
		      return false;
		 }
	<? } ?>
	
	
		 
	<? if($lnotice->DebitMethod==1) { ?>
	var AccountBalance = document.getElementById('amountBalance').value.replace(',','');
	if(parseInt(document.getElementById('thisAmountTotal').value) > parseInt(AccountBalance)) {
		alert("<?=$Lang['eNotice']['NotEnoughBalance']?>");  
		return false;
	} else {
	<? } ?>
         if (!confirm('<?=$i_Notice_Alert_Sign?>'))
         {
             return false;
             
         }else{
	         <? if(strtoupper($lnotice->Module) == "PAYMENT") { ?>
					document.form1.action = "eNoticePayment_sign_update.php";
				<? } else { ?>
					document.form1.action = "sign_update.php";
				<? } ?>
			 
			 //document.form1.target = "_self";
			 return true;
		 }
	<? if($lnotice->DebitMethod==1) { ?>
	}
	<? } ?>
}

function click_print()
{
	with(document.form1)
	{
        	action = "eNoticePayment_view_print_preview.php";
        	//target = "_blank";
            document.form1.submit();
	}        
}

//function updateAmountTotal(id, amt, type) {
//	if(type!=3) { // not checkbox option 
//		document.getElementById('thisAmountTotal').value = parseInt(document.getElementById('thisAmountTotal').value) + parseInt(amt) - parseInt(document.getElementById('thisItemAmount'+id).value);
//		document.getElementById('thisItemAmount'+id).value = amt;
//	} else {
//		document.getElementById('thisAmountTotal').value = parseInt(document.getElementById('thisAmountTotal').value) + parseInt(amt);
//		document.getElementById('thisItemAmount'+id).value = parseInt(document.getElementById('thisItemAmount'+id).value) + parseInt(eval(amt));
//	}
//	//document.getElementById('spanTemp').innerHTML = document.getElementById('thisAmountTotal').value;
//}
function updateAmountTotal(id, oldAmt, amt, type) {

	if(isNaN(oldAmt)) oldAmt = 0;
//	alert(oldAmt+'//'+amt);
	
	if(type!=3) { // not checkbox option ("3" is checkbox options)
		
		document.getElementById('thisAmountTotal').value = parseInt(document.getElementById('thisAmountTotal').value) + parseInt(amt) - oldAmt;
		document.getElementById('thisItemAmount'+id).value = amt;

	} else {	// checkbox option

		document.getElementById('thisAmountTotal').value = parseInt(document.getElementById('thisAmountTotal').value) + parseInt(amt);
		document.getElementById('thisItemAmount'+id).value = parseInt(document.getElementById('thisItemAmount'+id).value) + parseInt(eval(amt));

	}
	
	//document.getElementById('spanTemp').innerHTML = document.getElementById('thisAmountTotal').value;
}
</SCRIPT>
<div id="spanTemp"></div>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td  >
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					
				<tr valign='top'>
					<td bgcolor='#ffd588'><span class='tabletext'><?=$i_Notice_DateStart?></span></td>
				</tr>
				<tr valign='top'>
					<td bgcolor='#FFFFFF'><span class='tabletext2'><?=$lnotice->DateStart?></span></td>
				</tr>
				
				<tr valign='top'>
					<td bgcolor='#ffd588'><span class='tabletext'><?=$i_Notice_DateEnd?></span></td>
				</tr>
				<tr valign='top'>
					<td bgcolor='#FFFFFF'><span class='tabletext2'><?=$lnotice->DateEnd?></span></td>
				</tr>
				
				<tr valign='top'>
					<td bgcolor='#ffd588'><span class='tabletext'><?=$i_Notice_Title?></span></td>
				</tr>
				<tr valign='top'>
					<td bgcolor='#FFFFFF'><span class='tabletext2'><?=$lnotice->Title?></span></td>
				</tr>



				<tr valign='top'>
					<td bgcolor='#ffd588'><span class='tabletext'><?=$i_Notice_Description?></span></td>
				</tr>
				<tr valign='top'>
					<td bgcolor='#FFFFFF'><span class='tabletext2'><?=str_replace("&nbsp;&nbsp;", "&nbsp; ", htmlspecialchars_decode($this_Description))?></td>
				</tr>
                                

				
				<? if(strtoupper($lnotice->Module) == "PAYMENT") { ?>
								<tr valign='top'>
					<td valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['Title']?></span></td>
					</tr>
					<? if($lnotice->DebitMethod == 1){ ?>
						<tr valign='top'><td bgcolor='#FFFFFF'><span class='tabletext2'><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['DebitDirectly']?></td></tr>
					<? } else { ?>
						<tr valign='top'><td bgcolor='#FFFFFF'><span class='tabletext2'><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['DebitLLater']?></td></tr>
					<? } ?>
				</tr>
				<? } ?>
                                
                                <? if ($attachment != "") { ?>
                                <tr valign='top'>
					<td bgcolor='#ffd588'><span class='tabletext'><?=$i_Notice_Attachment?></span></td>
				</tr>
                                <tr valign='top'>
					<td  bgcolor='#FFFFFF'><span class='tabletext2'><?=$attachment?></td>
				</tr>
                                <? } ?>
                                
                                <? if ($isTeacher) {
        				$issuer = $lnotice->returnIssuerName();
        			?>
                                <tr valign='top'>
									<td valign='top' ><span class='tabletext'><?=$i_Notice_Issuer?></span></td>
									</tr>
									<tr valign='top'><td  bgcolor='#FFFFFF'><span class='tabletext2'><?=$issuer?></td>
								</tr>
                        	<? } ?>
                        	
                        	<? /* if ($editAllowed) { */
                                    if ($lnotice->replyStatus != 2)
                                    {
                                        $statusString = "$i_Notice_Unsigned";
                                    }
                                    else
                                    {
                                        $signer = $lu->getNameWithClassNumber($lnotice->signerID);
                                        $signby = ($lnotice->replyType==1? $i_Notice_Signer:$i_Notice_Editor);
                                        $statusString = "$i_Notice_Signed <I>($signby $signer $i_Notice_At ".$lnotice->signedTime.")</I>";
                                    }
							?>
									<tr valign='top'>
										<td bgcolor='#ffd588'><span class='tabletext'><?=$i_Notice_SignStatus?></span></td>
									</tr>
									<tr valign='top'>
										<td  bgcolor='#FFFFFF'><span class='tabletext2'><?=$statusString?></td>
									</tr>
					                        
									<tr valign='top'>
										<td bgcolor='#ffd588'><span class='tabletext'><?=$i_UserStudentName?></span></td>
									</tr>
									<tr valign='top'>
										<td  bgcolor='#FFFFFF'><span class='tabletext2'><?=$lu->getNameWithClassNumber($StudentID)?></td>
									</tr>
							<? /* } */ ?>
                             
				<tr valign='top'>
					<td  bgcolor='#ffd588'><span class='tabletext'><?=$i_Notice_RecipientType?></span></td>
				</tr>
				<tr valign='top'>
					<td  bgcolor='#FFFFFF'><span class='tabletext2'><?=$targetType[$lnotice->RecordType]?></td>
				</tr>
				
				
	<? if ($reqFillAllFields) { ?>
	<tr>
		<td  bgcolor='#FFFFFF'><span class='tabletext2'>[<?=$i_Survey_AllRequire2Fill?>]</td>
	</tr>
	<? } ?>
	
				</table>
	<td>
</tr>

<? 
### check include reply slip or not
$hasContent = (trim($lnotice->Question) || trim($lnotice->ReplySlipContent)) ? true : false;
?>
<? if($hasContent) {?>
<tr>
	<td  >
					<!-- Break -->
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                        <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/scissors_line.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/icons_scissors.gif"></td>
                                        </tr>
                                        </table>
	</td>
</tr>
<? } ?>
<tr>
	<td  >
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td>                              
                                <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
            <? if($hasContent) {?>
                                <!-- Relpy Slip -->
                                <tr valign='top'>
								<td  class='eNoticereplytitle' align='center'><span class='tabletext2'><?=$i_Notice_ReplySlip?></td>
							</tr>

				
				<? if($lnotice->ReplySlipContent) {?>
				<!-- Content Base Start //-->
					<tr valign='top'>
						<td  >
						<?=$lnotice->ReplySlipContent?>
						</td>
					</tr>
				<? } else {
						$temp_que =  str_replace("&amp;", "&", $lnotice->Question);
						$queString = $lform->getConvertedString($temp_que);
						$queString =trim($queString)." ";
					?>
					<!-- Question Base Start //-->
					<tr valign='top'>
					<td  >
                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                        	<td>
                        	<script language="javascript" src="/templates/forms/layer.js"></script>
                        	<? if(strtoupper($lnotice->Module) == "PAYMENT") { ?>
								<script language="javascript" src="/templates/forms/eNoticePayment_form_edit.js"></script>
							<? } else { ?>
								<script language="javascript" src="/templates/forms/form_edit.js"></script>
							<? } ?>	
                        
                                <form name="ansForm" method="post" action="update.php">
                                        <input type=hidden name="qStr" value="">
                                        <input type=hidden name="aStr" value="">
                                </form>
                                
                                <script language="Javascript">
                                
var replyslip = '<?=$Lang['eNotice']['ReplySlip']?>';

                                <?=$lform->getWordsInJS()?>
                                
                                var paymentOptions = "";
                                var paymentOptIDAry = new Array();
								var paymentOptNameAry = new Array();
								var paymentItemID = ""; 
								var paymentIDAry = new Array();
								var paymentItemName = "<?=$Lang['eNotice']['PaymentItemName']?>"; 
								<?
								$result = ($lpayment->checkBalance($StudentID,1));
								$ac = $lpayment->getWebDisplayAmountFormatDB($result[0]);
								?>
								var accountBalance = "<?=$Lang['eNotice']['AccountBalance']?> : $<?=$result[0]?>";
								
								
                                var sheet= new Answersheet();
                                // attention: MUST replace '"' to '&quot;'
                                sheet.qString="<?=str_replace("&lt;br&gt;","<br>",$queString)?>";
                                sheet.aString="<?=$ansString?>";
                                //edit submitted application
                                sheet.mode=1;
                                sheet.viewOnly = <?= ($ViewOnly ? 1 : 0)?>;
                                sheet.answer=sheet.sheetArr();
                                Part3 = '';
                                document.write(editPanel());
                                </script>
                                
                                <SCRIPT LANGUAGE=javascript>
                                function copyback()
                                {
                                         finish();
                                         document.form1.qStr.value = document.ansForm.qStr.value;
                                         document.form1.aStr.value = document.ansForm.aStr.value;
                                }
                                </SCRIPT>
                                
                        	</td>
					</tr>
                    </table>
                    </td>
				</tr>
				<!-- Question Base End //-->
				<? } ?>
			<? } ?>

				<? if(strtoupper($lnotice->Module) == "PAYMENT") { ?>
					<form name="form1" action="eNoticePayment_sign_update.php" method="post" onSubmit="return false">
				<? } else { ?>
					<form name="form1" action="sign_update.php" method="post" onSubmit="return false">
				<? } ?>
                                <? //if ($editAllowed && !$lnotice->ReplySlipContent) 
                                if ($editAllowed && $hasContent) 
                                { ?>
        							<tr>
                                	<td   align="left" class="tabletext"><span class='tabletext2'><?=($lnotice->replyStatus==2? "":$i_Notice_SignInstruction)?></td>
                					</tr>
                                <? } ?>
                                
                      		<tr>
                                	<td class="dotline"  ><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                                </tr>
                                <tr>
                			<td  >
                                        	<?php
                                        	/* testing */
                                        	if (isset($sys_custom['enotice_sign_with_password']) && $sys_custom['enotice_sign_with_password'])
                                        	{
                                        		$sign_with_password = "<table border='0'><tr><td class='tabletext2'>".$i_UserPassword."<input type='password' name='user_password'  /></td></tr></table>";
                                        	}
                                        	 if ($editAllowed && !$lnotice->ReplySlipContent && $hasContent)
                                        	 {
                                        	 	if (strtoupper($lnotice->Module) != "PAYMENT" || (strtoupper($lnotice->Module) == "PAYMENT" && !$lnotice->isTargetNoticeSigned($NoticeID,$StudentID)))
                                           			echo $sign_with_password.$linterface->GET_ACTION_BTN($i_Notice_Sign, "button", "copyback();SubmitForm(this.form)","submit2");
                                        	 } elseif ($editAllowed && (!$hasContent || $lnotice->ReplySlipContent))
                                        	 {
												echo $sign_with_password.$linterface->GET_ACTION_BTN($i_Notice_Sign, "button", "SubmitForm(this.form)");
											}
                                           ?>
                				
                			</td>
                		</tr>
                                
                                <input type="hidden" name="thisAmountTotal" id="thisAmountTotal" value="<? if($lnotice->TempItemAmount) echo $lnotice->TempItemAmount; else echo 0; ?>">
                                <?
                                $result = ($lpayment->checkBalance($StudentID,1));
                                $accBal = str_replace(",","", $result[0]);
                                ?>
                                <input type="hidden" name="amountBalance" id="amountBalance" value="<?=$accBal?>">
                                <input type="hidden" name="qStr" value="">
                                <input type="hidden" name="aStr" value="">
                                <input type="hidden" name="NoticeID" value="<?=$NoticeID?>">
                                <input type="hidden" name="StudentID" value="<?=$StudentID?>">
                                <input type="hidden" name="CurID" value="<?=$CurID?>">
                                <input type="hidden" name="token" value="<?=$token?>">
                </form>
                                   
				</table>
	</td>
</tr>

</table>  
	</td>
</tr>

</table> 

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();

?>