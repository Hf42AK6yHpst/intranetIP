<?php
// Editing by 
/*
Required parameters:
$token: eClass App token
$email: user full email address
//$access_time: unix timestamp 
//$hashval: sha1 of "$email###$access_time###$salt"

Selection parameters: 
$OptValue (scalar, string) :  First level selection. [By Identity > Teacher | Non-teaching staff | Student | Parent | Alumni], [By Group > group category], [Others > Shared MailBox | Internal Recipient Group]
$ChooseGroupID (array, string) : Second level selection. Sub-Category or Group
$ChooseUserID (array, string) : Third level selection. Usually single user
*/
/*
Sample json output:
[
  {
    "name": "OptValue",
    "optgroup": [
      {
        "label": "By Identity",
        "option": [
          {
            "display": "Teacher",
            "name": "OptValue",
            "value": -1,
            "selected": true
          },
          {
            "display": "Non-teaching Staff",
            "name": "OptValue",
            "value": -2,
            "selected": false
          },
          {
            "display": "Student",
            "name": "OptValue",
            "value": 2,
            "selected": false
          },
          {
            "display": "Parent",
            "name": "OptValue",
            "value": 3,
            "selected": false
          },
          {
            "display": "Alumni",
            "name": "OptValue",
            "value": -4,
            "selected": false
          }
        ]
      },
      {
        "label": "By Group",
        "option": [
          {
            "display": "",
            "name": "OptValue",
            "value": "GROUP_33",
            "selected": false
          },
          {
            "display": "111111",
            "name": "OptValue",
            "value": "GROUP_12",
            "selected": false
          },
          {
            "display": "1112 Teacher by Committee 委員會老師",
            "name": "OptValue",
            "value": "GROUP_19",
            "selected": false
          },
          {
            "display": "123 123",
            "name": "OptValue",
            "value": "GROUP_8",
            "selected": false
          },
          {
            "display": "Academic 行政",
            "name": "OptValue",
            "value": "GROUP_2",
            "selected": false
          },
          {
            "display": "Admin 行政",
            "name": "OptValue",
            "value": "GROUP_1",
            "selected": false
          },
          {
            "display": "Class 班別",
            "name": "OptValue",
            "value": "GROUP_3",
            "selected": false
          },
          {
            "display": "ECA 課外活動",
            "name": "OptValue",
            "value": "GROUP_5",
            "selected": false
          },
          {
            "display": "House 社級",
            "name": "OptValue",
            "value": "GROUP_4",
            "selected": false
          },
          {
            "display": "Marcus Group",
            "name": "OptValue",
            "value": "GROUP_16",
            "selected": false
          },
          {
            "display": "Miscellaneous",
            "name": "OptValue",
            "value": "GROUP_6",
            "selected": false
          },
          {
            "display": "Omas Test 1",
            "name": "OptValue",
            "value": "GROUP_31",
            "selected": false
          },
          {
            "display": "Omas Test 2 edit",
            "name": "OptValue",
            "value": "GROUP_32",
            "selected": false
          },
          {
            "display": "Parent",
            "name": "OptValue",
            "value": "GROUP_38",
            "selected": false
          },
          {
            "display": "Report Group",
            "name": "OptValue",
            "value": "GROUP_18",
            "selected": false
          },
          {
            "display": "sads",
            "name": "OptValue",
            "value": "GROUP_37",
            "selected": false
          },
          {
            "display": "Shared mailbox",
            "name": "OptValue",
            "value": "GROUP_17",
            "selected": false
          },
          {
            "display": "test",
            "name": "OptValue",
            "value": "GROUP_10",
            "selected": false
          },
          {
            "display": "Yat Woon Group Category",
            "name": "OptValue",
            "value": "GROUP_7",
            "selected": false
          },
          {
            "display": "家長教師會",
            "name": "OptValue",
            "value": "GROUP_34",
            "selected": false
          },
          {
            "display": "宿舍部",
            "name": "OptValue",
            "value": "GROUP_42",
            "selected": false
          },
          {
            "display": "宿舍點名",
            "name": "OptValue",
            "value": "GROUP_40",
            "selected": false
          },
          {
            "display": "文武",
            "name": "OptValue",
            "value": "GROUP_35",
            "selected": false
          },
          {
            "display": "許 %&amp;^@&quot;\\&#039;:;&lt;&gt;,./?[]~!#$*(){}|=+-",
            "name": "OptValue",
            "value": "GROUP_30",
            "selected": false
          },
          {
            "display": "護理系統_奬勵計劃",
            "name": "OptValue",
            "value": "GROUP_39",
            "selected": false
          },
          {
            "display": "護理系統_特別需要組",
            "name": "OptValue",
            "value": "GROUP_20",
            "selected": false
          },
          {
            "display": "護理系統_疾病組",
            "name": "OptValue",
            "value": "GROUP_21",
            "selected": false
          },
          {
            "display": "輔導管理",
            "name": "OptValue",
            "value": "GROUP_41",
            "selected": false
          }
        ]
      },
      {
        "label": "Others",
        "option": [
          {
            "display": "Shared MailBox",
            "name": "OptValue",
            "value": "SHARED_MAILBOX",
            "selected": false
          },
          {
            "display": "Internal Recipient Group",
            "name": "OptValue",
            "value": "INTERNAL_GROUP",
            "selected": false
          }
        ]
      }
    ]
  },
  {
    "name": "ChooseGroupID[]",
    "option": [
      {
        "display": "All Teachers",
        "name": "ChooseGroupID[]",
        "value": "1",
        "selected": false,
        "values": "\"0-carlos_t1\" <0-carlos_t1@testmail2.broadlearning.com>; \"8 eInventory\" <invent@testmail2.broadlearning.com>; \"aaaaa\" <aaaaa@testmail2.broadlearning.com>; \"abc\" <winnie_123@testmail2.broadlearning.com>; \"Adam Student\" <adam_S01@testmail2.broadlearning.com>; \"Mr. adam_t1\" <adam_t@testmail2.broadlearning.com>; \"ALAN T\" <alan_t@testmail2.broadlearning.com>; \"Alex_t\" <alex_t@testmail2.broadlearning.com>; \"Amy Kwok\" <amy@testmail2.broadlearning.com>; \"annat2\" <annat2@testmail2.broadlearning.com>; \"anne\" <an@testmail2.broadlearning.com>; \"asd\" <test@testmail2.broadlearning.com>; \"atest3\" <atest_3@testmail2.broadlearning.com>; \"atest_2\" <atest_2@testmail2.broadlearning.com>; \"atf\" <dfdfdf@testmail2.broadlearning.com>; \"Bill\" <bill_t01@testmail2.broadlearning.com>; \"Bill (Basic)\" <bill_basic@testmail2.broadlearning.com>; \"Bill eRC Admin\" <bill_ercadmin@testmail2.broadlearning.com>; \"Bill T03\" <bill_t03@testmail2.broadlearning.com>; \"Bill T04\" <bill_t04@testmail2.broadlearning.com>; \"bill wong\" <billwong@testmail2.broadlearning.com>; \"BL\" <writing2@testmail2.broadlearning.com>; \"bob\" <bob@testmail2.broadlearning.com>; \"bobtest\" <tonytest222@testmail2.broadlearning.com>; \"Broadlearning T\" <broadlearning_t@testmail2.broadlearning.com>; \"Principal Broadlearning-real\" <broadlearning@testmail2.broadlearning.com>; \"broadlearning2 broadlearning2\" <broadlearning2@testmail2.broadlearning.com>; \"btw Eng\" <btw@testmail2.broadlearning.com>; \"C1\" <t20130001@testmail2.broadlearning.com>; \"cameron\" <cameron@testmail2.broadlearning.com>; \"cameron_t1\" <cameron_t1@testmail2.broadlearning.com>; \"cameron_t2\" <cameron_t2@testmail2.broadlearning.com>; \"cameron_t5\" <cameron_t5@testmail2.broadlearning.com>; \"Cara_t\" <cara_t@testmail2.broadlearning.com>; \"Carlos T29\" <carlos_t29@testmail2.broadlearning.com>; \"Mr. Carlos Teacher 5 Eng\" <carlos_t5@testmail2.broadlearning.com>; \"Mr. Carlos testmail2 Teacher1\" <carlos_testmail2_t1@testmail2.broadlearning.com>; \"Mr. carlos_t1 ~!@#$%^&amp;*()_+{}|:\" <carlos_t1@testmail2.broadlearning.com>; \"carlos_t10\" <carlos_t10@testmail2.broadlearning.com>; \"carlos_t11\" <carlos_t11@testmail2.broadlearning.com>; \"carlos_t2 No.2\" <carlos_t2@testmail2.broadlearning.com>; \"carlos_t20\" <carlos_t20_alias2@testmail2.broadlearning.com>; \"carlos_t21\" <carlos_t21@testmail2.broadlearning.com>; \"carlos_t22\" <carlos_t22@testmail2.broadlearning.com>; \"carlos_t23\" <carlos_t23@testmail2.broadlearning.com>; \"carlos_t24\" <carlos_t24@testmail2.broadlearning.com>; \"carlos_t25\" <carlos_t25@testmail2.broadlearning.com>; \"carlos_t26\" <carlos_t26@testmail2.broadlearning.com>; \"carlos_t27\" <carlos_t27@testmail2.broadlearning.com>; \"carlos_t6 Eng\" <carlos_t6@testmail2.broadlearning.com>; \"carlos_t7 &lt;tag1&gt; &lt;fake&gt;\" <carlos_t7@testmail2.broadlearning.com>; \"carlos_t8\" <carlos_t8@testmail2.broadlearning.com>; \"carlos_t9\" <carlos_t9_alias@testmail2.broadlearning.com>; \"cateach2\" <cateacher2@testmail2.broadlearning.com>; \"cateacher_201503191\" <cateacher_201503191@testmail2.broadlearning.com>; \"ca_test1\" <ca_test1@testmail2.broadlearning.com>; \"Mr. Cheuk Tai Man\" <content_t@testmail2.broadlearning.com>; \"Mr. chloe_t\" <chloe_t@testmail2.broadlearning.com>; \"chloe_t1\" <chloe_t1@testmail2.broadlearning.com>; \"Miss creative_t\" <creative_t@testmail2.broadlearning.com>; \"Desmond Pang\" <desmondpang@testmail2.broadlearning.com>; \"Mr. dogbert2 eng\" <dogbert2@testmail2.broadlearning.com>; \"Doraemon\" <dora_t@testmail2.broadlearning.com>; \"Dummy Account (not in any group)\" <dummy@testmail2.broadlearning.com>; \"ebooking_mg_1\" <ebooking_mg_1@testmail2.broadlearning.com>; \"eclass\" <info@testmail2.broadlearning.com>; \"Mr. eng\" <comma@testmail2.broadlearning.com>; \"Mr. eng\" <teachery@testmail2.broadlearning.com>; \"Mr. eng\" <ttt@testmail2.broadlearning.com>; \"Mr. eng\" <inventory@testmail2.broadlearning.com>; \"Miss eng\" <skyso_t@testmail2.broadlearning.com>; \"Miss eng\" <skyso_t2@testmail2.broadlearning.com>; \"Mr. eng\" <lwc014@testmail2.broadlearning.com>; \"Mr. eng\" <lwc015@testmail2.broadlearning.com>; \"Miss eng\" <skyso_st@testmail2.broadlearning.com>; \"Mr. eng\" <ifolder_test@testmail2.broadlearning.com>; \"Mr. eng\" <kenneth_t2@testmail2.broadlearning.com>; \"Mr. eng\" <kekekeke@testmail2.broadlearning.com>; \"eng\" <fffkekekeke@testmail2.broadlearning.com>; \"eng\" <zzzz@testmail2.broadlearning.com>; \"Mr. eng\" <sunny_t@testmail2.broadlearning.com>; \"Mr. eng\" <lwc071@testmail2.broadlearning.com>; \"Mr. eng\" <ifolder2@testmail2.broadlearning.com>; \"Mr. eng\" <action@testmail2.broadlearning.com>; \"Mr. eng\" <ifolder3@testmail2.broadlearning.com>; \"Mr. eng\" <ifolder4@testmail2.broadlearning.com>; \"Mr. eng\" <kenneth_t4@testmail2.broadlearning.com>; \"Mr. eng\" <michael_t1@testmail2.broadlearning.com>; \"Mr. eng\" <henry_2010@testmail2.broadlearning.com>; \"Mr. eng\" <shing_t@testmail2.broadlearning.com>; \"Mr. eng\" <sandy_t@testmail2.broadlearning.com>; \"Mr. eng\" <ftp_01@testmail2.broadlearning.com>; \"Mr. eng\" <connor_t@testmail2.broadlearning.com>; \"Mr. eng\" <connor2_t@testmail2.broadlearning.com>; \"Mr. eng\" <connor3_t@testmail2.broadlearning.com>; \"Mr. eng\" <michaelcheungtest@testmail2.broadlearning.com>; \"Mr. eng\" <avril_t@testmail2.broadlearning.com>; \"Mr. eng\" <kenneth_t5@testmail2.broadlearning.com>; \"dgstaff04 許邨 English title eng\" <dgstaff04@testmail2.broadlearning.com>; \"Mr. eng\" <old_imail_1@testmail2.broadlearning.com>; \"Mr. eng\" <old_imail_2@testmail2.broadlearning.com>; \"Mr. eng\" <eric_t@testmail2.broadlearning.com>; \"Mr. eng\" <fai_3@testmail2.broadlearning.com>; \"Mr. eng\" <import1@testmail2.broadlearning.com>; \"Mr. eng &amp; eng\" <dgstaff01@testmail2.broadlearning.com>; \"ePost Teacher 1 Eng\" <epost_teacher_1@testmail2.broadlearning.com>; \"ePost Teacher 2 Eng\" <epost_teacher_2@testmail2.broadlearning.com>; \"ePost Teacher Eng\" <epost_teacher@testmail2.broadlearning.com>; \"Mr. Eric Yip T001 x\" <ericyip_t001@testmail2.broadlearning.com>; \"Erick\" <erick_t@testmail2.broadlearning.com>; \"Evan_t1\" <evan_t1@testmail2.broadlearning.com>; \"fai\" <fai_t11@testmail2.broadlearning.com>; \"fai 10\" <fai_t10@testmail2.broadlearning.com>; \"Fai 4\" <fai_t4@testmail2.broadlearning.com>; \"fai 5\" <fai_t5@testmail2.broadlearning.com>; \"fai 6\" <fai_t6@testmail2.broadlearning.com>; \"Fai 8\" <fai_t8@testmail2.broadlearning.com>; \"Fai 9\" <fai_t9@testmail2.broadlearning.com>; \"Mr. fai_admin\" <fai_admin@testmail2.broadlearning.com>; \"fai_ms\" <fai_ms@testmail2.broadlearning.com>; \"Mr. fai_t1\" <fai_t1@testmail2.broadlearning.com>; \"fai_t7\" <fai_t7@testmail2.broadlearning.com>; \"fai_taabc\" <fai_taabc@testmail2.broadlearning.com>; \"fai_ttabc\" <fai_ttabc@testmail2.broadlearning.com>; \"fai_t_ies\" <fai_t_ies@testmail2.broadlearning.com>; \"fai_t_sba\" <fai_t_sba@testmail2.broadlearning.com>; \"fiona_t\" <fiona_t@testmail2.broadlearning.com>; \"Frankie T1 ENG\" <frankie_t1@testmail2.broadlearning.com>; \"Frankie T2 ENG\" <frankie_t2@testmail2.broadlearning.com>; \"gamma_t1 en\" <gamma_t1@testmail2.broadlearning.com>; \"gamma_t2 en\" <gamma_t2@testmail2.broadlearning.com>; \"gamma_t3\" <gamma_t3@testmail2.broadlearning.com>; \"gamma_t4\" <gamma_t4@testmail2.broadlearning.com>; \"gamma_t5\" <gamma_t5@testmail2.broadlearning.com>; \"gamma_t6\" <gamma_t6@testmail2.broadlearning.com>; \"gamma_t7\" <gamma_t7@testmail2.broadlearning.com>; \"gamma_t8 ch\" <gamma_t8@testmail2.broadlearning.com>; \"gamma_t9 en\" <gamma_t9@testmail2.broadlearning.com>; \"GG\" <GG@testmail2.broadlearning.com>; \"Mr Henry (NT)\" <henry_nt@testmail2.broadlearning.com>; \"Henry Chan\" <henrychan_t@testmail2.broadlearning.com>; \"Henry Normal Teacher\" <henry_t@testmail2.broadlearning.com>; \"Principal Henry Sir\" <henry@testmail2.broadlearning.com>; \"Ho Chiu Yan\" <superman@testmail2.broadlearning.com>; \"hugo_t\" <hugo_t@testmail2.broadlearning.com>; \"icarus\" <icarus@testmail2.broadlearning.com>; \"IPF821 Admin\" <ipf821_admin@testmail2.broadlearning.com>; \"iPF821 Normal\" <ipf821_normal@testmail2.broadlearning.com>; \"ipf821 nothing\" <ipf821_nothing@testmail2.broadlearning.com>; \"iportfolio standalone\" <ab_iportfolio_cd@testmail2.broadlearning.com>; \"isaacheng\" <ci@testmail2.broadlearning.com>; \"Principal IvanKo\" <ivanko_t@testmail2.broadlearning.com>; \"ivanko_t11\" <ivanko_t11@testmail2.broadlearning.com>; \"ivanko_t12\" <ivanko_t12@testmail2.broadlearning.com>; \"ivanko_t13\" <ivanko_t13@testmail2.broadlearning.com>; \"ivanko_t14\" <ivanko_t14@testmail2.broadlearning.com>; \"ivanko_t15\" <ivanko_t15@testmail2.broadlearning.com>; \"Mr. IvanKo_t2\" <ivanko_t2@testmail2.broadlearning.com>; \"Mr. ivanko_t4\" <ivanko_t4@testmail2.broadlearning.com>; \"ivanko_t5\" <ivanko_t5@testmail2.broadlearning.com>; \"ivanko_t8\" <ivanko_t8@testmail2.broadlearning.com>; \"ivanko_t9\" <ivanko_t9@testmail2.broadlearning.com>; \"ivor\" <ivor@testmail2.broadlearning.com>; \"jadengan\" <jadengan@testmail2.broadlearning.com>; \"Mr. Jason T\" <jason_t@testmail2.broadlearning.com>; \"BL Teacher1A Jason T1\" <jason_t1@testmail2.broadlearning.com>; \"Mr. Jenny Cheung\" <jenny_t@testmail2.broadlearning.com>; \"Jenny Class Teacher\" <jenny_t1@testmail2.broadlearning.com>; \"Jenny Subject Teacher\" <jenny_t2@testmail2.broadlearning.com>; \"Miss Wong JENNY WONG\" <t20130002@testmail2.broadlearning.com>; \"John Lee\" <johnlee@testmail2.broadlearning.com>; \"Karson\" <karson_t@testmail2.broadlearning.com>; \"Mr. kelvin_t\" <kelvin_t@testmail2.broadlearning.com>; \"Ken int\" <ken_int1@testmail2.broadlearning.com>; \"Mr. Ken Teacher\" <ken_t@testmail2.broadlearning.com>; \"Kenneth Yau\" <kennethyau@testmail2.broadlearning.com>; \"kennethyau_t1\" <kennethyau_t1@testmail2.broadlearning.com>; \"kennethyau_t3\" <kennethyau_t3@testmail2.broadlearning.com>; \"Mr. kenneth_t1\" <kenneth_t1@testmail2.broadlearning.com>; \"ken_dis\" <ken_dis@testmail2.broadlearning.com>; \"ken_gt\" <ken_gt@testmail2.broadlearning.com>; \"ken_staff\" <ken_staff@testmail2.broadlearning.com>; \"kindergarten\" <kindergarten@testmail2.broadlearning.com>; \"KIS Carlos T1\" <kis_carlos_t1@testmail2.broadlearning.com>; \"Mr. KIS Creative T\" <kis_creative_t@testmail2.broadlearning.com>; \"Teacher KIS RONALD T2\" <kis_ronald_t2@testmail2.broadlearning.com>; \"kis_chloe_t\" <kis_chloe_t@testmail2.broadlearning.com>; \"kis_chloe_t1\" <kis_chloe_t1@testmail2.broadlearning.com>; \"kis_fiona_t\" <kis_fiona_t@testmail2.broadlearning.com>; \"kis_henrychan_Eng\" <kis_henrychan@testmail2.broadlearning.com>; \"kis_henrychan_nt ENG\" <kis_henrychan_nt@testmail2.broadlearning.com>; \"kis_mingwainp eng\" <kis_mingwainp@testmail2.broadlearning.com>; \"kis_mingwaipe eng\" <kis_mingwaipe@testmail2.broadlearning.com>; \"kis_mosgraceful ENG\" <kis_mosgraceful@testmail2.broadlearning.com>; \"kis_munsang eng\" <kis_munsang@testmail2.broadlearning.com>; \"kis_omas\" <kis_omas@testmail2.broadlearning.com>; \"kis_siuwan_t\" <kis_siuwan_t@testmail2.broadlearning.com>; \"kis_tbck ENG\" <kis_tbck@testmail2.broadlearning.com>; \"kis_winnie\" <kis_winnie@testmail2.broadlearning.com>; \"kis_winnie2\" <kis_winnie2@testmail2.broadlearning.com>; \"kis_ylsyk ENG\" <kis_ylsyk@testmail2.broadlearning.com>; \"ktlmskg\" <kis_ktlmskg@testmail2.broadlearning.com>; \"ktlsks eng\" <kis_ktlsks@testmail2.broadlearning.com>; \"Lee Ho Ho\" <kis_k1t@testmail2.broadlearning.com>; \"listening_t\" <listening_t@testmail2.broadlearning.com>; \"marcus_ct en\" <marcus_ct@testmail2.broadlearning.com>; \"Mr. marcus_t en\" <marcus_t@testmail2.broadlearning.com>; \"Marcus_t1 en\" <marcus_t1@testmail2.broadlearning.com>; \"Mr. marcus_t2 en\" <marcus_t2@testmail2.broadlearning.com>; \"1990-01-24 Mary Chan\" <maryu@testmail2.broadlearning.com>; \"Mary Chan\" <mary@testmail2.broadlearning.com>; \"May Yip\" <mayyip@testmail2.broadlearning.com>; \"Money Tsang\" <money_t@testmail2.broadlearning.com>; \"Monkey D Luffy\" <luffy@testmail2.broadlearning.com>; \"new staff\" <newnew@testmail2.broadlearning.com>; \"Newbie\" <newbie123@testmail2.broadlearning.com>; \"newnew\" <anew@testmail2.broadlearning.com>; \"omas\" <omas_t2@testmail2.broadlearning.com>; \"Omas the 87th\" <omas_t87@testmail2.broadlearning.com>; \"Prof. Omas the 88th\" <omas_t88@testmail2.broadlearning.com>; \"Omas the 999th\" <omas_t999@testmail2.broadlearning.com>; \"omas_classt\" <omas_classt@testmail2.broadlearning.com>; \"omas_ipo\" <omas_ipo@testmail2.broadlearning.com>; \"omas_kis\" <omas_kis@testmail2.broadlearning.com>; \"omas_omas\" <omas_omas@testmail2.broadlearning.com>; \"omas_panelt\" <omas_panelt@testmail2.broadlearning.com>; \"omas_principal\" <omas_principal@testmail2.broadlearning.com>; \"omas_subjectt\" <omas_subjectt@testmail2.broadlearning.com>; \"Mr. omas_t\" <omas_t@testmail2.broadlearning.com>; \"Principal omas_t1\" <omas_t1@testmail2.broadlearning.com>; \"omas_t11\" <omas_t11@testmail2.broadlearning.com>; \"omas_t5\" <omas_t5@testmail2.broadlearning.com>; \"omas_t77\" <omas_t77@testmail2.broadlearning.com>; \"Mr. Oscar\" <oscar@testmail2.broadlearning.com>; \"Parent Teacher Association\" <pta@testmail2.broadlearning.com>; \"Pat Pat\" <pat_t@testmail2.broadlearning.com>; \"paulchyiu\" <paulchyiu@testmail2.broadlearning.com>; \"Peter Wong\" <omas_t66@testmail2.broadlearning.com>; \"Philipsho\" <philipsho@testmail2.broadlearning.com>; \"philipsho_ap1\" <philipsho_ap1@testmail2.broadlearning.com>; \"philipsho_ap2\" <philipsho_ap2@testmail2.broadlearning.com>; \"philipsho_t1\" <philipsho_t1@testmail2.broadlearning.com>; \"Mr plt001\" <plt001@testmail2.broadlearning.com>; \"Pun T 1\" <pun_t1@testmail2.broadlearning.com>; \"Mr. Pun T 2\" <pun_t2@testmail2.broadlearning.com>; \"Pun T4\" <pun_t4@testmail2.broadlearning.com>; \"dgstaff04 許邨 English title qchen_t2\" <qchen_t2@testmail2.broadlearning.com>; \"qqq\" <qqq@testmail2.broadlearning.com>; \"rct\" <rct@testmail2.broadlearning.com>; \"ricky_t1\" <ricky_t1@testmail2.broadlearning.com>; \"Rita Tin\" <ritatin_t@testmail2.broadlearning.com>; \"Rita Tin (T2)\" <ritatin_t2@testmail2.broadlearning.com>; \"Rita Tin (T3)\" <ritatin_t3@testmail2.broadlearning.com>; \"Rita Tin (T4)\" <ritatin_t4@testmail2.broadlearning.com>; \"Rita Tin (T5)\" <ritatin_t5@testmail2.broadlearning.com>; \"Rita Tin (T6)\" <ritatin_t6@testmail2.broadlearning.com>; \"rmkg\" <kis_rmkg@testmail2.broadlearning.com>; \"Teacher Ronald KIS T1\" <kis_ronald_t1@testmail2.broadlearning.com>; \"Mr. Ronald Kwok\" <ronaldkwok_t@testmail2.broadlearning.com>; \"Mr KIS Ronald Ronald Kwok KIS\" <kis_ronald@testmail2.broadlearning.com>; \"Mr. ronald_ebooking_01\" <ronald_ebooking_01@testmail2.broadlearning.com>; \"Mr ronald_import\" <ronald_import@testmail2.broadlearning.com>; \"Mr. ronald_t2\" <ronald_t2@testmail2.broadlearning.com>; \"Mr. ronald_t3\" <ronald_t3@testmail2.broadlearning.com>; \"Captain Roy Law\" <roylaw_t@testmail2.broadlearning.com>; \"Teacher Roy Teacher 1\" <roylaw_t1@testmail2.broadlearning.com>; \"Roy Teacher 2\" <roylaw_t2@testmail2.broadlearning.com>; \"roy teacher 3\" <roylaw_t3@testmail2.broadlearning.com>; \"Roy Teacher 4\" <roylaw_t4@testmail2.broadlearning.com>; \"Mr. CR Ryan Chan\" <rc@testmail2.broadlearning.com>; \"Ryan Chan Class teacher\" <ryanc@testmail2.broadlearning.com>; \"Ryan Subject teacher\" <ryant@testmail2.broadlearning.com>; \"Sales Team\" <sales@testmail2.broadlearning.com>; \"Sally Eng\" <sally_t001@testmail2.broadlearning.com>; \"Sally Wong\" <sallywong@testmail2.broadlearning.com>; \"Ms. SHING Ka Ka\" <kyshing@testmail2.broadlearning.com>; \"Simon Ngai\" <simonhwngai@testmail2.broadlearning.com>; \"simonbl\" <simonbl@testmail2.broadlearning.com>; \"simonysyu\" <simonyu@testmail2.broadlearning.com>; \"siuwan&#039;t\" <siuwan_t@testmail2.broadlearning.com>; \"siuwan_t2\" <siuwan_t2@testmail2.broadlearning.com>; \"so tse kin\" <soso20015@testmail2.broadlearning.com>; \"Student temp\" <student_temp@testmail2.broadlearning.com>; \"System Administrator\" <kis_admin@testmail2.broadlearning.com>; \"Mr. t1\" <t1@testmail2.broadlearning.com>; \"Yeah t123\" <t123@testmail2.broadlearning.com>; \"Mr. t2\" <t2@testmail2.broadlearning.com>; \"Teacher - 1\" <teacher1@testmail2.broadlearning.com>; \"Teacher Eng Name\" <teacher_1@testmail2.broadlearning.com>; \"teacher001\" <teacher001@testmail2.broadlearning.com>; \"teacher_qiao\" <qchen_t@testmail2.broadlearning.com>; \"Teaching Staff A\" <staff_t@testmail2.broadlearning.com>; \"Temp Staff\" <tempstaff@testmail2.broadlearning.com>; \"Mr Testing T1\" <testing_t1@testmail2.broadlearning.com>; \"test_acc_2\" <test_acc_2@testmail2.broadlearning.com>; \"test_archive_t\" <test_archive_t@testmail2.broadlearning.com>; \"test_archive_t1\" <test_archive_t1@testmail2.broadlearning.com>; \"test_t001\" <test_t001@testmail2.broadlearning.com>; \"test_t1\" <test_t1@testmail2.broadlearning.com>; \"test_test1\" <test_test1@testmail2.broadlearning.com>; \"tett\" <tett@testmail2.broadlearning.com>; \"Mr Thomas 3 eng\" <thomas_t3@testmail2.broadlearning.com>; \"thomas 4 eng\" <thomas_t4@testmail2.broadlearning.com>; \"Thomas eng\" <thomas_t@testmail2.broadlearning.com>; \"Mr. thomas_t1 Eng\" <thomas_t1@testmail2.broadlearning.com>; \"Mr. TMAX\" <tmax@testmail2.broadlearning.com>; \"Tommy Li\" <tommyli@testmail2.broadlearning.com>; \"Tony Chan\" <tonychan@testmail2.broadlearning.com>; \"tonytest\" <tonytest@testmail2.broadlearning.com>; \"tsuenwanbckg\" <kis_tsuenwanbckg@testmail2.broadlearning.com>; \"Mr. tt\" <tt@testmail2.broadlearning.com>; \"ttc donate\" <ttcdonate@testmail2.broadlearning.com>; \"ttc donate 2\" <ttcdonate2@testmail2.broadlearning.com>; \"uccke ENG\" <kis_uccke@testmail2.broadlearning.com>; \"Venusx\" <venusxleung@testmail2.broadlearning.com>; \"Victor Chong\" <victor@testmail2.broadlearning.com>; \"villa_ct\" <villa_ct@testmail2.broadlearning.com>; \"villa_sp\" <villa_sp@testmail2.broadlearning.com>; \"villa_st\" <villa_st@testmail2.broadlearning.com>; \"villa_t\" <villa_t@testmail2.broadlearning.com>; \"WINNE10\" <winne10@testmail2.broadlearning.com>; \"BL Teacher winnie\" <winnie@testmail2.broadlearning.com>; \"Miss winnie2\" <winnie2@testmail2.broadlearning.com>; \"winnie_222\" <winnie_222@testmail2.broadlearning.com>; \"winnie_2222\" <winnie_2222@testmail2.broadlearning.com>; \"winnie_ct\" <winnie_ct@testmail2.broadlearning.com>; \"te winnie_s50\" <winnie_s50@testmail2.broadlearning.com>; \"Mr. winnie_t1\" <winnie_t1@testmail2.broadlearning.com>; \"Mr. winnie_t2\" <winnie_t2@testmail2.broadlearning.com>; \"Without iFolder\" <ifolderx@testmail2.broadlearning.com>; \"x123\" <x123@testmail2.broadlearning.com>; \"ykmtest\" <ykmtest@testmail2.broadlearning.com>; \"ykmtest2\" <ykmtest2@testmail2.broadlearning.com>; \"yu_t\" <yu_t@testmail2.broadlearning.com>; \"yu_t2\" <yu_t2@testmail2.broadlearning.com>; \"ZHANG Yan Qi\" <rzigzag@testmail2.broadlearning.com>; \"Mr. zz Functional : ronald_t\" <ronald_t@testmail2.broadlearning.com>; \"陳中文\" <chi_teacher@testmail2.broadlearning.com>; "
      },
      {
        "display": "Teacher(s) of the Same Form",
        "name": "ChooseGroupID[]",
        "value": "2",
        "selected": true,
        "values": "\"Mr. Carlos Teacher 5 Eng\" <carlos_t5@testmail2.broadlearning.com>; \"Mr. carlos_t1 ~!@#$%^&amp;*()_+{}|:\" <carlos_t1@testmail2.broadlearning.com>; "
      },
      {
        "display": "Teacher(s) of the Same Class",
        "name": "ChooseGroupID[]",
        "value": "3",
        "selected": false,
        "values": "\"Mr. Carlos Teacher 5 Eng\" <carlos_t5@testmail2.broadlearning.com>; \"Mr. carlos_t1 ~!@#$%^&amp;*()_+{}|:\" <carlos_t1@testmail2.broadlearning.com>; "
      },
      {
        "display": "Teacher(s) of the Same Subject",
        "name": "ChooseGroupID[]",
        "value": "4",
        "selected": false,
        "values": "\"Bill T04\" <bill_t04@testmail2.broadlearning.com>; \"Mr. Carlos Teacher 5 Eng\" <carlos_t5@testmail2.broadlearning.com>; \"Mr. carlos_t1 ~!@#$%^&amp;*()_+{}|:\" <carlos_t1@testmail2.broadlearning.com>; \"carlos_t2 No.2\" <carlos_t2@testmail2.broadlearning.com>; \"Frankie T1 ENG\" <frankie_t1@testmail2.broadlearning.com>; \"Frankie T2 ENG\" <frankie_t2@testmail2.broadlearning.com>; "
      },
      {
        "display": "Teacher(s) of the Same Subject Group",
        "name": "ChooseGroupID[]",
        "value": "5",
        "selected": false,
        "values": "\"Mr. Carlos Teacher 5 Eng\" <carlos_t5@testmail2.broadlearning.com>; \"Mr. carlos_t1 ~!@#$%^&amp;*()_+{}|:\" <carlos_t1@testmail2.broadlearning.com>; \"carlos_t2 No.2\" <carlos_t2@testmail2.broadlearning.com>; "
      }
    ]
  },
  {
    "name": "ChooseUserID[]",
    "option": [
      {
        "display": "Mr. Carlos Teacher 5 Eng",
        "name": "ChooseUserID[]",
        "value": "\"Mr. Carlos Teacher 5 Eng\" <carlos_t5@testmail2.broadlearning.com>",
        "selected": false
      },
      {
        "display": "Mr. carlos_t1 ~!@#$%^&amp;*()_+{}|:",
        "name": "ChooseUserID[]",
        "value": "\"Mr. carlos_t1 ~!@#$%^&amp;*()_+{}|:\" <carlos_t1@testmail2.broadlearning.com>",
        "selected": false
      }
    ]
  }
] 
*/

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/role_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/user_right_target.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_api.php");
include_once($PATH_WRT_ROOT."includes/json.php");

$json_obj = new JSON_obj();
$return_ary = array();
//$salt = 'E1CuML80Rxiw30y1';

header("Content-Type: application/json;charset=utf-8");
/*
if(!isset($_POST['email']) || $_POST['email']=='' || !isset($_POST['access_time']) || $_POST['access_time']=='' || !isset($_POST['hashval']) || $_POST['hashval']=='')
{
	$return_ary['Error'] = 'Invalid parameters.';
	echo $json_obj->encode($return_ary);
	exit;
}

$now_ts = time();
$ts = intval($_POST['access_time']);
$ts_threshold = 10; // valid within 10 seconds

if($now_ts - $ts > $ts_threshold){
	$return_ary['Error'] = 'Access timeout.';
	echo $json_obj->encode($return_ary);
	exit;
}

$hash_to_check = sha1($_POST['email'].'###'.$_POST['access_time'].'###'.$salt);
if($hash_to_check != $_POST['hashval']){
	$return_ary['Error'] = 'Invalid hash value.';
	echo $json_obj->encode($return_ary);
	exit;
}
*/
if(/*!isset($_POST['email']) || $_POST['email']=='' || */ !isset($_POST['token']) || $_POST['token']=='')
{
	$return_ary['Error'] = 'Invalid parameters.';
	echo $json_obj->encode($return_ary);
	exit;
}

intranet_opendb();

$libdb = new libdb();
$libAppApi = new libeClassAppApi();

$token = trim($_POST['token']);
$apiResult = $libAppApi->retrieveMailUserInfoByToken($token);

if($apiResult == '-999'){
	$return_ary['Error'] = 'Invalid token.';
	echo $json_obj->encode($return_ary);
	intranet_closedb();
	exit;
}

//$email = trim($_POST['email']);
$user_id = $apiResult['userId'];
$sql = "SELECT UserID,UserLogin,ImapUserEmail,RecordType,RecordStatus FROM INTRANET_USER WHERE UserID='".$user_id."'";
$user_records= $libdb->returnResultSet($sql);
if(count($user_records)==0){
	$return_ary['Error'] = 'User does not exist.';
	echo $json_obj->encode($return_ary);
	intranet_closedb();
	exit;
}

if($user_records[0]['RecordStatus']!=1){
	$return_ary['Error'] = 'Inactive user.';
	echo $json_obj->encode($return_ary);
	intranet_closedb();
	exit;
}

$email = $user_records[0]['ImapUserEmail'];
$user_id = $user_records[0]['UserID'];
$user_type = $user_records[0]['RecordType'];
$UserID = $user_id; // some functions implicitly used $UserID as global variable

if(isset($_POST['lang']) && in_array($_POST['lang'],array('en','b5','gb'))){
	$_SESSION['intranet_session_language'] = $_POST['lang'];
	$intranet_session_language = $_SESSION['intranet_session_language'];
}
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if(!isset($_SESSION['SSV_USER_TARGET'])){
	$UserRightTarget = new user_right_target();
	$_SESSION['SSV_USER_TARGET'] = $UserRightTarget->Load_User_Target($user_id);
}

/*
Level 1 selection : $OptValue
Level 2 selection : $ChooseGroupID[]
Level 3 Selection : $ChooseUserID[]
*/


$li = new libuser($user_id);
$lrole = new role_manage();
$fcm = new form_class_manage();
$lgrouping = new libgrouping();
$lwebmail = new libwebmail();
$IMap = new imap_gamma(true);

$name_field = getNameFieldWithClassNumberByLang("a.");
$identity = $lrole->Get_Identity_Type($user_id);
$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];


## get toStaff, toStudent, toParent options - IP25 only ##
$result_to_options['ToTeachingStaffOption'] = 0;
$result_to_options['ToNonTeachingStaffOption'] = 0;
$result_to_options['ToStudentOption'] = 0;
$result_to_options['ToParentOption'] = 0;
$result_to_options['ToAlumniOption'] = 0;

## Newly added by Ronald (20091118) - for �^�خѰ| [CRM Ref No.: 2009-1116-0915]
## Control by flag - $sys_custom['iMail_RecipientCategory_StaffAndTeacher']
$result_to_options['ToTeacherAndStaff'] = 0;
if($sys_custom['iMail_RecipientCategory_StaffAndTeacher'])
{
	if(($identity == "Teaching") || ($identity == "NonTeaching"))
	{
		$result_to_options['ToTeacherAndStaff'] = 1;
	}
}
/*
### Pre-Gen all related group email ###
$js_all_teacher = addslashes(htmlspecialchars_decode($IMap->GetAllTeacherEmailByUserIdentity($UserID),ENT_QUOTES));
$js_all_form_teacher = addslashes(htmlspecialchars_decode($IMap->GetAllSameFormTeacherEmailByUserIdentity($UserID),ENT_QUOTES));
$js_all_class_teacher = addslashes(htmlspecialchars_decode($IMap->GetAllSameClassTeacherEmailByUserIdentity($UserID),ENT_QUOTES));
$js_all_subject_teacher = addslashes(htmlspecialchars_decode($IMap->GetAllSameSubjectTeacherEmailByUserIdentity($UserID),ENT_QUOTES));
$js_all_subject_group_teacher = addslashes(htmlspecialchars_decode($IMap->GetAllSameSubjectGroupTeacherEmailByUserIdentity($UserID),ENT_QUOTES));

$js_all_student = addslashes(htmlspecialchars_decode($IMap->GetAllStudentEmailByUserIdentity($UserID),ENT_QUOTES));
$js_all_form_student = addslashes(htmlspecialchars_decode($IMap->GetAllSameFormStudentEmailByUserIdentity($UserID),ENT_QUOTES));
$js_all_class_student = addslashes(htmlspecialchars_decode($IMap->GetAllSameClassStudentEmailByUserIdentity($UserID),ENT_QUOTES));
$js_all_subject_student = addslashes(htmlspecialchars_decode($IMap->GetAllSameSubjectStudentEmailByUserIdentity($UserID),ENT_QUOTES));
$js_all_subject_group_student = addslashes(htmlspecialchars_decode($IMap->GetAllSameSubjectGroupStudentEmailByUserIdentity($UserID),ENT_QUOTES));

$js_all_parent = addslashes(htmlspecialchars_decode($IMap->GetAllParentEmailByUserIdentity($UserID),ENT_QUOTES));
$js_all_form_parent = addslashes(htmlspecialchars_decode($IMap->GetAllSameFormParentEmailByUserIdentity($UserID),ENT_QUOTES));
$js_all_class_parent = addslashes(htmlspecialchars_decode($IMap->GetAllSameClassParentEmailByUserIdentity($UserID),ENT_QUOTES));
$js_all_subject_parent = addslashes(htmlspecialchars_decode($IMap->GetAllSameSubjectParentEmailByUserIdentity($UserID),ENT_QUOTES));
$js_all_subject_group_parent = addslashes(htmlspecialchars_decode($IMap->GetAllSameSubjectGroupParentEmailByUserIdentity($UserID),ENT_QUOTES));
*/
if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Staff-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup']))
{
	if($sys_custom['iMail_RemoveTeacherCat']) {
		$result_to_options['ToTeachingStaffOption'] = 0;
	} else {
		$result_to_options['ToTeachingStaffOption'] = 1;
	}
}
if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']))
{
	if($sys_custom['iMail_RemoveNonTeachingCat']) {
		$result_to_options['ToNonTeachingStaffOption'] = 0;
	} else {
		$result_to_options['ToNonTeachingStaffOption'] = 1;
	}
}
if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Student-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Student-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup']))
{
	$result_to_options['ToStudentOption'] = 1;
}
if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Parent-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup']))
{
	$result_to_options['ToParentOption'] = 1;
}
if($special_feature['alumni'] && ($_SESSION['SSV_USER_TARGET']['All-Yes'] || $_SESION['SSV_USER_TARGET']['Alumni-All']) ){
	$result_to_options['ToAlumniOption'] = 1;
}

/*if( ($_SESSION['SSV_USER_TARGET']['All-Yes']) || 
	($_SESSION['SSV_USER_TARGET']['Staff-MyGroup']) || 
	($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup']) || 
	($_SESSION['SSV_USER_TARGET']['Student-MyGroup']) || 
	($_SESSION['SSV_USER_TARGET']['Parent-MyGroup']) ||
	($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) ||
	($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) ||
	($_SESSION['SSV_USER_TARGET']['Student-All']) ||
	($_SESSION['SSV_USER_TARGET']['Parent-All']) )*/
if(	($_SESSION['SSV_USER_TARGET']['All-Yes']) || 
	($_SESSION['SSV_USER_TARGET']['Staff-MyGroup']) || 
	($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup']) || 
	($_SESSION['SSV_USER_TARGET']['Student-MyGroup']) || 
	($_SESSION['SSV_USER_TARGET']['Parent-MyGroup']) || 
	($special_feature['alumni'] && $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup']) || 
	(/*$sys_custom['iMailShowAllGroupsWithIdentityAll'] &&*/ $_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || 
	(/*$sys_custom['iMailShowAllGroupsWithIdentityAll'] &&*/ $_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || 
	(/*$sys_custom['iMailShowAllGroupsWithIdentityAll'] &&*/ $_SESSION['SSV_USER_TARGET']['Student-All']) || 
	(/*$sys_custom['iMailShowAllGroupsWithIdentityAll'] &&*/ $_SESSION['SSV_USER_TARGET']['Parent-All'])
)
{
	$result_to_options['ToGroupOption'] = 1;
	
	$result_to_group_options['ToTeacher'] = 0;
	$result_to_group_options['ToStaff'] = 0;
	$result_to_group_options['ToStudent'] = 0;
	$result_to_group_options['ToParent'] = 0;
	$result_to_group_options['ToAlumni'] = 0;
	
	if($_SESSION['SSV_USER_TARGET']['All-Yes']){
		$result_to_group_options['ToTeacher'] = 1;
		$result_to_group_options['ToStaff'] = 1;
		$result_to_group_options['ToStudent'] = 1;
		$result_to_group_options['ToParent'] = 1;
		if($special_feature['alumni']) $result_to_group_options['ToAlumni'] = 1;
	}else{
		if( (/*$sys_custom['iMailShowAllGroupsWithIdentityAll'] &&*/ $_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'])){
			$result_to_group_options['ToTeacher'] = 1;
		}
		if( (/*$sys_custom['iMailShowAllGroupsWithIdentityAll'] &&*/ $_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || ($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'])){
			$result_to_group_options['ToStaff'] = 1;
		}
		if( (/*$sys_custom['iMailShowAllGroupsWithIdentityAll'] &&*/ $_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyGroup'])){
			$result_to_group_options['ToStudent'] = 1;
		}
		if( (/*$sys_custom['iMailShowAllGroupsWithIdentityAll'] &&*/ $_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'])){
			$result_to_group_options['ToParent'] = 1;
		}
		if($special_feature['alumni'] && $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup']){
			$result_to_group_options['ToAlumni'] = 1;
		}
	}
}

$AlumniNoTargetCond = true;
if($special_feature['alumni']) $AlumniNoTargetCond = $_SESSION['SSV_USER_TARGET']['Alumni-All'] == '' && $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup'] == '';
### If no mail targeting is set in the front-end, than will assign some default targeting to user ###
if( ($_SESSION['SSV_USER_TARGET']['All-Yes'] == '') && 
($_SESSION['SSV_USER_TARGET']['All-No'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MyForm'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MyClass'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MySubject'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-All'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MyForm'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MyClass'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MySubject'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MyGroup'] == '') &&
($_SESSION['SSV_USER_TARGET']['Parent-All'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MyForm'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MyClass'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MySubject'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'] == '') && 
$AlumniNoTargetCond 
)
{
	if($result_to_options['ToTeachingStaffOption'] == 0 && $result_to_options['ToNonTeachingStaffOption'] == 0 && $result_to_options['ToStudentOption'] == 0 && $result_to_options['ToParentOption'] == 0 && $result_to_options['ToAlumniOption'] == 0)
	{
		if(($identity == "Teaching") || ($identity == "NonTeaching"))
		{
			### If user is Teacher, then will have the follow targeting :
			###  - to All Teaching Staff
			###  - to All NonTeaching Staff
			###  - to All Student 
			###  - to All Parent 
			$result_to_options['ToTeachingStaffOption'] = 1;
			$result_to_options['ToNonTeachingStaffOption'] = 1;
			$result_to_options['ToStudentOption'] = 1;
			$result_to_options['ToParentOption'] = 1;
			if($special_feature['alumni']) $result_to_options['ToAlumniOption'] = 1;
			$_SESSION['SSV_USER_TARGET']['All-Yes'] = true;
		}
		if($identity == "Student")
		{
			### If user is Student, then will have the follow targeting :
			###  - to Student Own Class Student
			###  - to Student Own Subject Group Student
			$result_to_options['ToStudentOption'] = 1;
			$_SESSION['SSV_USER_TARGET']['Student-MyClass'] = true;	
			$_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'] = true;
		}
		if($identity == "Parent")
		{
			### If user is Parent, then will have the follow targeting :
			###  - to Their Child's Own Class Teacher
			###  - to Their Child's Own Subject Group Teacher
			$result_to_options['ToTeachingStaffOption'] = 1;
			$_SESSION['SSV_USER_TARGET']['Staff-MyClass'] = true;	
			$_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'] = true;
		}
	}
	$result_to_options['ToGroupOption'] = 1;
	
	$result_to_group_options['ToTeacher'] = 0;
	$result_to_group_options['ToStaff'] = 0;
	$result_to_group_options['ToStudent'] = 0;
	$result_to_group_options['ToParent'] = 0;
	$result_to_group_options['ToAlumni'] = 0;
	
	if($_SESSION['SSV_USER_TARGET']['All-Yes']){
		$result_to_group_options['ToTeacher'] = 1;
		$result_to_group_options['ToStaff'] = 1;
		$result_to_group_options['ToStudent'] = 1;
		$result_to_group_options['ToParent'] = 1;
		if($special_feature['alumni']) $result_to_group_options['ToAlumni'] = 1;
	}else{
		if(($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'])){
			$result_to_group_options['ToTeacher'] = 1;
		}
		if(($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || ($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'])){
			$result_to_group_options['ToStaff'] = 1;
		}
		if(($_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyGroup'])){
			$result_to_group_options['ToStudent'] = 1;
		}
		if(($_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'])){
			$result_to_group_options['ToParent'] = 1;
		}
		if($special_feature['alumni'] && ($_SESSION['SSV_USER_TARGET']['Alumni-All'] || $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup'])){
			$result_to_group_options['ToAlumni'] = 1;
		}
	}
}

if($identity != "Student")
{
	$sql = "SELECT COUNT(*) FROM INTRANET_PARENTRELATION WHERE ParentID = '$user_id'";
	$result = $li->returnVector($sql);
	if($result[0]>0)
		$result_to_options['ToMyChildrenOption'] = 1;
}
if($identity == "Student")
{
	$sql = "SELECT COUNT(*) FROM INTRANET_PARENTRELATION WHERE StudentID = '$user_id'";
	$result = $li->returnVector($sql);
	if($result[0]>0)
		$result_to_options['ToMyParentOption'] = 1;
}

if(isset($sys_custom['iMail']['SelectRecipientType']) && is_array($sys_custom['iMail']['SelectRecipientType'])){
	$identity_group_display_order = $sys_custom['iMail']['SelectRecipientType'];
}else{
	$identity_group_display_order = array('identity','group');
}

$return_ary = array();
$level1_ary = array('name'=>'OptValue','optgroup'=>array());
$level2_ary = array('name'=>'ChooseGroupID[]','option'=>array());
$level3_ary = array('name'=>'ChooseUserID[]','option'=>array());

$optgroup_identity = array('label'=>$Lang['iMail']['FieldTitle']['ByIdentity'],'option'=>array());
if ($result_to_options['ToTeacherAndStaff'])
{
	//$x1_identity .= "<option value=-3 ".(($OptValue==-3)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['TeacherAndStaff']."</option>\n";
	$optgroup_identity['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['TeacherAndStaff'],'name'=>'OptValue','value'=>-3,'selected'=>$OptValue==-3);
}
if ($result_to_options['ToTeachingStaffOption'])
{
    //$x1_identity .= "<option value=-1 ".(($OptValue==-1)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['Teacher']."</option>\n";
    $optgroup_identity['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['Teacher'],'name'=>'OptValue','value'=>-1,'selected'=>$OptValue==-1);
}
if ($result_to_options['ToNonTeachingStaffOption'])
{
    //$x1_identity .= "<option value=-2 ".(($OptValue==-2)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['NonTeachingStaff']."</option>\n";
    $optgroup_identity['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['NonTeachingStaff'],'name'=>'OptValue','value'=>-2,'selected'=>$OptValue==-2);
}
if ($result_to_options['ToStudentOption'])
{
    //$x1_identity .= "<option value=2 ".(($OptValue==2)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['Student']."</option>\n";
    $optgroup_identity['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['Student'],'name'=>'OptValue','value'=>2,'selected'=>$OptValue==2);
}
if ($result_to_options['ToParentOption'])
{
    //$x1_identity .= "<option value=3 ".(($OptValue==3)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['Parent']."</option>\n";
    $optgroup_identity['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['Parent'],'name'=>'OptValue','value'=>3,'selected'=>$OptValue==3);
}
if ($special_feature['alumni'] && $result_to_options['ToAlumniOption'])
{
    //$x1_identity .= "<option value=-4 ".(($OptValue==-4)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['Alumni']."</option>\n";
    $optgroup_identity['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['Alumni'],'name'=>'OptValue','value'=>-4,'selected'=>$OptValue==-4);
}
if ($result_to_options['ToMyChildrenOption'])
{
	//$x1_identity .= "<option value=5 ".(($OptValue==5)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['MyChildren']."</option>\n";
	$optgroup_identity['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['MyChildren'],'name'=>'OptValue','value'=>5,'selected'=>$OptValue==5);
}
if ($result_to_options['ToMyParentOption'])
{
	//$x1_identity .= "<option value=6 ".(($OptValue==6)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['MyParent']."</option>\n";
	$optgroup_identity['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['MyParent'],'name'=>'OptValue','value'=>6,'selected'=>$OptValue==6);
}


$optgroup_group = array('label'=>$Lang['iMail']['FieldTitle']['ByGroup'],'option'=>array());

$arrExcludeGroupCatID[] = 0;
$targetGroupCatIDs = implode(",",$arrExcludeGroupCatID);

if($_SESSION['SSV_USER_TARGET']['All-Yes'])
{
	$sql = "SELECT DISTINCT a.GroupCategoryID, a.CategoryName FROM INTRANET_GROUP_CATEGORY as a ";
	if($sys_custom['iMailPlusHideGroupCategoryWithoutGroupMember']){
		$sql.= " LEFT JOIN INTRANET_GROUP AS b ON (a.GroupCategoryID = b.RecordType AND b.AcademicYearID = '$CurrentAcademicYearID') 
				LEFT JOIN INTRANET_USERGROUP AS c ON (b.GroupID = c.GroupID) ";
	}
	$sql.= " WHERE a.GroupCategoryID NOT IN ($targetGroupCatIDs) ";
	if($sys_custom['iMailPlusHideGroupCategoryWithoutGroupMember']){
		$sql.= " GROUP BY a.GroupCategoryID HAVING COUNT(c.UserID)>0 ";
	}
	$sql.= " ORDER BY a.CategoryName ASC";
}
else
{
	if($result_to_group_options['ToTeacher'] || $result_to_group_options['ToStaff'] || $result_to_group_options['ToStudent'] || $result_to_group_options['ToParent'] || ($special_feature['alumni'] && $result_to_group_options['ToAlumni']))
	{
		$sql = "SELECT DISTINCT a.GroupCategoryID, a.CategoryName FROM INTRANET_GROUP_CATEGORY AS a INNER JOIN INTRANET_GROUP AS b ON (a.GroupCategoryID = b.RecordType) ";
		if($sys_custom['iMailPlusHideGroupCategoryWithoutGroupMember']){
			$sql .= " AND b.AcademicYearID = '$CurrentAcademicYearID' ";
		}
		$sql.= " INNER JOIN INTRANET_USERGROUP AS c ON (b.GroupID = c.GroupID)
				 WHERE c.UserID = '$UserID' AND b.RecordType != 0 AND a.GroupCategoryID NOT IN ($targetGroupCatIDs) ";
		if($sys_custom['iMailPlusHideGroupCategoryWithoutGroupMember']){
			$sql.= " GROUP BY a.GroupCategoryID HAVING COUNT(c.UserID)>0 ";
		}
		$sql.= " ORDER BY a.CategoryName ASC";
	}
}
$result = $li->returnArray($sql);

if(sizeof($result) > 0){
	for($i=0; $i<sizeof($result); $i++)
	{
		list($GroupCatID, $GroupCatName) = $result[$i];
		$GroupCatID = "GROUP_".$GroupCatID;
		
		//$x1_group .= "<option value='$GroupCatID' ".(($GroupCatID == $OptValue)?"SELECTED":"").">".$GroupCatName."</option>";
		$optgroup_group['option'][] = array('display'=>$GroupCatName,'name'=>'OptValue','value'=>$GroupCatID,'selected'=>$OptValue==$GroupCatID);
	}
}

$optgroup_others = array('label'=>$Lang['General']['Others'],'option'=>array());
## Others > Shared MailBox
if($_SESSION['SSV_USER_TARGET']['All-Yes']){
	
	$sql = "SELECT s.MailBoxID, s.MailBoxName, IF(p.DisplayName IS NULL OR p.DisplayName = '',s.MailBoxName,p.DisplayName) as DisplayName FROM MAIL_SHARED_MAILBOX as s LEFT JOIN MAIL_PREFERENCE as p ON p.MailBoxName = s.MailBoxName ORDER BY DisplayName ";
	$SharedMailBoxes = $li->returnArray($sql);
	if(sizeof($SharedMailBoxes)>0){
		
		//$x1 .= "<option value='SHARED_MAILBOX' ".($OptValue == "SHARED_MAILBOX"?"SELECTED":"").">".$Lang['SharedMailBox']['SharedMailBox']."</option>";
		$optgroup_others['option'][] = array('display'=>$Lang['SharedMailBox']['SharedMailBox'],'name'=>'OptValue','value'=>'SHARED_MAILBOX','selected'=>$OptValue=='SHARED_MAILBOX');
	}
}

## Internal Group
//$x1 .= "<option value='INTERNAL_GROUP' ".($OptValue == "INTERNAL_GROUP"?"SELECTED":"").">".$Lang['Gamma']['InternalRecipientGroup']."</option>";
$optgroup_others['option'][] = array('display'=>$Lang['Gamma']['InternalRecipientGroup'],'name'=>'OptValue','value'=>'INTERNAL_GROUP','selected'=>$OptValue=='INTERNAL_GROUP');

if($identity_group_display_order[0]=='identity'){
	$level1_ary['optgroup'][] = $optgroup_identity;
	if($identity_group_display_order[1]=='group'){
		$level1_ary['optgroup'][] = $optgroup_group;
	}
}else if($identity_group_display_order[0]=='group'){
	$level1_ary['optgroup'][] = $optgroup_group;
	if($identity_group_display_order[1]=='identity'){
		$level1_ary['optgroup'][] = $optgroup_identity;
	}
}
$level1_ary['optgroup'][] = $optgroup_others;

$return_ary[] = $level1_ary;


if($OptValue != "")
{
	if($OptValue=="SHARED_MAILBOX" || $OptValue == "INTERNAL_GROUP")
	{
		## Others
		$GroupOpt = 3;
		$CatID = $OptValue;
		//$x1 .= "<input type='hidden' name='CatID' value='$CatID'>";
	}
	else if(strpos($OptValue,"GROUP_") !== false)
	{
		## GROUP 
		$GroupOpt = 2;
		$CatID = 4;
		$ChooseGroupCatID = substr($OptValue,6,strlen($OptValue));
		//$x1 .= "<input type='hidden' name='CatID' value=$CatID>";
	}
	else
	{	
		## Identity
		$GroupOpt = 1;
		$CatID = $OptValue;
		//$x1 .= "<input type='hidden' name='CatID' value=$CatID>";
	}
}
else
{
	$GroupOpt = "";
	$CatID = "";
	$ChooseGroupCatID = "";
	//$x1 .= "<input type='hidden' name='CatID' value=''>";
}

if(!isset($ChooseGroupID)){
	$ChooseGroupID = array();
}else if(!is_array($ChooseGroupID)){
	$ChooseGroupID = (array)$ChooseGroupID;
}

# 2nd Level Cat List - IP25 only #
if(($CatID != "") || ($CatID != 0) || ($CatID != 4)){
	if($CatID == -1){
		//$x2 = "<select name='ChooseGroupID[]' id='ChooseGroupID[]' multiple size='10'>";
		## All Teaching Staff ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])){
			
			$num_of_all_teacher = $lwebmail->returnNumOfAllTeachingStaff($identity);
			if($num_of_all_teacher > 0){
				//if(sizeof($ChooseGroupID)>0)
				//	$x2 .= "<option value='1' ".((in_array(1,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToIndividualsTeachingStaff']."</option>";
				//else
				//	$x2 .= "<option value='1' >".$Lang['iMail']['FieldTitle']['ToIndividualsTeachingStaff']."</option>";
				$js_all_teacher = $IMap->GetAllTeacherEmailByUserIdentity($user_id);	
				$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToIndividualsTeachingStaff'],'name'=>'ChooseGroupID[]','value'=>'1','selected'=>in_array(1,$ChooseGroupID),'values'=>$js_all_teacher);
			}
		}
		## Form Teacher ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MyForm'])){
			
			$num_of_form_teacher = $lwebmail->returnNumOfFormTeacher($identity);
			if($num_of_form_teacher > 0)
			{
				//if(sizeof($ChooseGroupID)>0)
				//	$x2 .= "<option value='2' ".((in_array(2,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToFormTeachingStaff']."</option>";
				//else
				//	$x2 .= "<option value='2' >".$Lang['iMail']['FieldTitle']['ToFormTeachingStaff']."</option>";
				$js_all_form_teacher = $IMap->GetAllSameFormTeacherEmailByUserIdentity($user_id);
				$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToFormTeachingStaff'],'name'=>'ChooseGroupID[]','value'=>'2','selected'=>in_array(2,$ChooseGroupID),'values'=>$js_all_form_teacher);
			}
		}
		## Class Teacher ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MyClass'])){
			
			$num_of_class_teacher = $lwebmail->returnNumOfClassTeacher($identity);
			if($num_of_class_teacher > 0)
			{
				//if(sizeof($ChooseGroupID)>0)
				//	$x2 .= "<option value='3' ".((in_array(3,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToClassTeachingStaff']."</option>";
				//else
				//	$x2 .= "<option value='3' >".$Lang['iMail']['FieldTitle']['ToClassTeachingStaff']."</option>";
				$js_all_class_teacher = $IMap->GetAllSameClassTeacherEmailByUserIdentity($user_id);
				$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToClassTeachingStaff'],'name'=>'ChooseGroupID[]','value'=>'3','selected'=>in_array(3,$ChooseGroupID),'values'=>$js_all_class_teacher);
			}
		}
		## Subject Teacher ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MySubject'])){
			
			$num_of_subject_teacher = $lwebmail->returnNumOfSubjectTeacher($identity);
			if($num_of_subject_teacher > 0)
			{
				//if(sizeof($ChooseGroupID)>0)
				//	$x2 .= "<option value='4' ".((in_array(4,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectTeachingStaff']."</option>";
				///else
				//	$x2 .= "<option value='4' >".$Lang['iMail']['FieldTitle']['ToSubjectTeachingStaff']."</option>";
				$js_all_subject_teacher = $IMap->GetAllSameSubjectTeacherEmailByUserIdentity($user_id);
				$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToSubjectTeachingStaff'],'name'=>'ChooseGroupID[]','value'=>'4','selected'=>in_array(4,$ChooseGroupID),'values'=>$js_all_subject_teacher);
			}		
		}
		## Subject Group Teacher ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'])){
			
			$num_of_subject_group_teacher = $lwebmail->returnNumOfSubjectGroupTeacher($identity);
			if($num_of_subject_group_teacher > 0)
			{
				//if(sizeof($ChooseGroupID)>0)
				//	$x2 .= "<option value='5' ".((in_array(5,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectGroupTeachingStaff']."</option>";
				//else
				//	$x2 .= "<option value='5' >".$Lang['iMail']['FieldTitle']['ToSubjectGroupTeachingStaff']."</option>";
				$js_all_subject_group_teacher = $IMap->GetAllSameSubjectGroupTeacherEmailByUserIdentity($user_id);
				$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToSubjectGroupTeachingStaff'],'name'=>'ChooseGroupID[]','value'=>'5','selected'=>in_array(5,$ChooseGroupID),'values'=>$js_all_subject_group_teacher);
			}
		}
		//$x2 .= "</select>";
	}
	if($CatID == -2){
		## Non-teaching Staff ##
		//$result = $fcm->Get_Non_Teaching_Staff_List();
		if($sys_custom['hideTeacherTitle']){
			$NameField = getNameFieldByLang("","",true);
		}else{
			$NameField = getNameFieldByLang();
		}
		
		$sql = "Select UserID, ".$NameField." as Name, IMapUserEmail From INTRANET_USER where RecordStatus = 1 and RecordType = 1 and (Teaching = 0 OR Teaching IS NULL) AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY EnglishName";
		$result = $li->returnArray($sql,3);
		//$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
		if(sizeof($result)>0){
			$tmp_count = 0;
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name, $u_email) = $result[$i];
				if(count($SelectedRecipientsEmails)>0 && in_array($u_email,$SelectedRecipientsEmails)){
					continue;
				}
				$tmp_count++;
				//$final_email = '"'.$u_name.'"'." <".$u_email."> ";
				//$x3 .= "<option value='$final_email'>$u_name</option>";
				$final_email = $IMap->FormatRecipientOption('"'.$u_name.'"'." <".$u_email."> ");
				//$x3 .= '<option value="'.$final_email.'">'.$IMap->FormatRecipientOption($u_name).'</option>';
				$level3_ary['option'][] = array('display'=>$IMap->FormatRecipientOption($u_name),'name'=>'ChooseUserID[]','value'=>$final_email,'selected'=>false);
			}
			//if($tmp_count == 0){
			//	$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
			//}
		}else{
			//$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}
		//$x3 .= "</select>";
	}
	## Newly added by Ronald (20091118) - for �^�خѰ| [CRM Ref No.: 2009-1116-0915]
	## Control by flag - $sys_custom['iMail_RecipientCategory_StaffAndTeacher']
	
	if($CatID == -3){
		## Teachers / Staff ##
		$NameField = getNameFieldByLang();
		$sql = "Select UserID, ".$NameField." as Name, IMapUserEmail From INTRANET_USER WHERE RecordStatus = '1' AND RecordType = '1' AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY EnglishName";
		$result = $fcm->returnArray($sql,3);
		
		//$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
		if(sizeof($result)>0){
			$tmp_count = 0;
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name, $u_email) = $result[$i];
				if(count($SelectedRecipientsEmails)>0 && in_array($u_email,$SelectedRecipientsEmails)){
					continue;
				}
				$tmp_count++;
				//$final_email = '"'.$u_name.'"'." <".$u_email."> ";
				//$x3 .= "<option value='$final_email'>".$u_name."</option>";
				$final_email = $IMap->FormatRecipientOption('"'.$u_name.'"'." <".$u_email."> ");
				//$x3 .= '<option value="'.$final_email.'">'.$IMap->FormatRecipientOption($u_name).'</option>';
				$level3_ary['option'][] = array('display'=>$IMap->FormatRecipientOption($u_name),'name'=>'ChooseUserID[]','value'=>$final_email,'selected'=>false);
			}
			//if($tmp_count == 0){
			//	$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
			//}
		}else{
			//$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}
		//$x3 .= "</select>";
	}
	if($CatID == 2){
		//$x2 = "<select name='ChooseGroupID[]' id='ChooseGroupID[]' multiple size='10'>";
		## All Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])){
			
			$num_of_all_student = $lwebmail->returnNumOfAllStudent($identity);
			if($num_of_all_student > 0)
			{
				//if(sizeof($ChooseGroupID)>0)
				//	$x2 .= "<option value='1' ".((in_array(1,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToIndividualsStudent']."</option>";
				//else
				//	$x2 .= "<option value='1' >".$Lang['iMail']['FieldTitle']['ToIndividualsStudent']."</option>";
				$js_all_student = $IMap->GetAllStudentEmailByUserIdentity($user_id);
				$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToIndividualsStudent'],'name'=>'ChooseGroupID[]','value'=>'1','selected'=>in_array(1,$ChooseGroupID),'values'=>$js_all_student);
			}
		}
		## Form Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MyForm'])){
			
			$num_of_form_subject = $lwebmail->returnNumOfFormStudent($identity);
			if($num_of_form_subject > 0)
			{
				//if(sizeof($ChooseGroupID)>0)
				//	$x2 .= "<option value='2' ".((in_array(2,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToFormStudent']."</option>";
				//else
				//	$x2 .= "<option value='2' >".$Lang['iMail']['FieldTitle']['ToFormStudent']."</option>";
				$js_all_form_student = $IMap->GetAllSameFormStudentEmailByUserIdentity($user_id);
				$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToFormStudent'],'name'=>'ChooseGroupID[]','value'=>'2','selected'=>in_array(2,$ChooseGroupID),'values'=>$js_all_form_student);
			}
		}
		## Class Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MyClass'])){
			
			$num_of_class_student = $lwebmail->returnNumOfClassStudent($identity);
			if($num_of_class_student > 0)
			{
				//if(sizeof($ChooseGroupID)>0)
				//	$x2 .= "<option value='3' ".((in_array(3,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToClassStudent']."</option>";
				//else
				//	$x2 .= "<option value='3' >".$Lang['iMail']['FieldTitle']['ToClassStudent']."</option>";
				$js_all_class_student = $IMap->GetAllSameClassStudentEmailByUserIdentity($user_id);
				$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToClassStudent'],'name'=>'ChooseGroupID[]','value'=>'3','selected'=>in_array(3,$ChooseGroupID),'values'=>$js_all_class_student);
			}
		}
		## Subject Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MySubject'])){
			
			$num_of_subject_student = $lwebmail->returnNumOfSubjectStudent($identity);
			if($num_of_subject_student > 0)
			{
				//if(sizeof($ChooseGroupID)>0)
				//	$x2 .= "<option value='4' ".((in_array(4,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectStudent']."</option>";
				//else
				//	$x2 .= "<option value='4' >".$Lang['iMail']['FieldTitle']['ToSubjectStudent']."</option>";
				$js_all_subject_student = $IMap->GetAllSameSubjectStudentEmailByUserIdentity($user_id);
				$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToSubjectStudent'],'name'=>'ChooseGroupID[]','value'=>'4','selected'=>in_array(4,$ChooseGroupID),'values'=>$js_all_subject_student);
			}
		}
		## Subject Group Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'])){
			
			$num_of_subject_group_student = $lwebmail->returnNumOfSubjectGroupStudent($identity);
			if($num_of_subject_group_student > 0)
			{
				//if(sizeof($ChooseGroupID)>0)
				//	$x2 .= "<option value='5' ".((in_array(5,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectGroupStudent']."</option>";
				//else
				//	$x2 .= "<option value='5' >".$Lang['iMail']['FieldTitle']['ToSubjectGroupStudent']."</option>";
				$js_all_subject_group_student = $IMap->GetAllSameSubjectGroupStudentEmailByUserIdentity($user_id);
				$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToSubjectGroupStudent'],'name'=>'ChooseGroupID[]','value'=>'5','selected'=>in_array(5,$ChooseGroupID),'values'=>$js_all_subject_group_student);
			}
		}
		//$x2 .= "</select>";
	}
	if($CatID == 3){
		$x2 = "<select name='ChooseGroupID[]' id='ChooseGroupID[]' multiple size='10'>";
		## All Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])){
			
			$num_of_all_parent = $lwebmail->returnNumOfAllParent($identity);
			if($num_of_all_parent > 0)
			{
				//if(sizeof($ChooseGroupID)>0)
				//	$x2 .= "<option value='1' ".((in_array(1,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToIndividualsParents']."</option>";
				//else
				//	$x2 .= "<option value='1' >".$Lang['iMail']['FieldTitle']['ToIndividualsParents']."</option>";
				$js_all_parent = $IMap->GetAllParentEmailByUserIdentity($user_id);
				$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToIndividualsParents'],'name'=>'ChooseGroupID[]','value'=>'1','selected'=>in_array(1,$ChooseGroupID),'values'=>$js_all_parent);
			}
		}
		## Form Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MyForm'])){
			
			$num_of_form_parent = $lwebmail->returnNumOfFormParent($identity);
			if($num_of_form_parent > 0)
			{
				//if(sizeof($ChooseGroupID)>0)
				//	$x2 .= "<option value='2' ".((in_array(2,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToFormParents']."</option>";
				//else
				//	$x2 .= "<option value='2' >".$Lang['iMail']['FieldTitle']['ToFormParents']."</option>";
				$js_all_form_parent = $IMap->GetAllSameFormParentEmailByUserIdentity($user_id);
				$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToFormParents'],'name'=>'ChooseGroupID[]','value'=>'2','selected'=>in_array(2,$ChooseGroupID),'values'=>$js_all_form_parent);
			}
		}
		## Class Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MyClass'])){
			
			$num_of_class_parent = $lwebmail->returnNumOfClassParent($identity);
			if($num_of_class_parent > 0)
			{
				//if(sizeof($ChooseGroupID)>0)
				//	$x2 .= "<option value='3' ".((in_array(3,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToClassParents']."</option>";
				//else
				//	$x2 .= "<option value='3' >".$Lang['iMail']['FieldTitle']['ToClassParents']."</option>";
				$js_all_class_parent = $IMap->GetAllSameClassParentEmailByUserIdentity($user_id);
				$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToClassParents'],'name'=>'ChooseGroupID[]','value'=>'3','selected'=>in_array(3,$ChooseGroupID),'values'=>$js_all_class_parent);
			}
		}
		## Subject Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MySubject'])){
			
			$num_of_subject_parent = $lwebmail->returnNumOfSubjectParent($identity);
			if($num_of_subject_parent > 0)
			{
				//if(sizeof($ChooseGroupID)>0)
				//	$x2 .= "<option value='4' ".((in_array(4,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectParents']."</option>";
				//else
				//	$x2 .= "<option value='4' >".$Lang['iMail']['FieldTitle']['ToSubjectParents']."</option>";
				$js_all_subject_parent = $IMap->GetAllSameSubjectParentEmailByUserIdentity($user_id);
				$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToSubjectParents'],'name'=>'ChooseGroupID[]','value'=>'4','selected'=>in_array(4,$ChooseGroupID),'values'=>$js_all_subject_parent);
			}
		}
		## Subject Group Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup'])){
			
			$num_of_subject_group_parent = $lwebmail->returnNumOfSubjectGroupParent($identity);
			if($num_of_subject_group_parent > 0)
			{
				//if(sizeof($ChooseGroupID)>0)
				//	$x2 .= "<option value='5' ".((in_array(5,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectGroupParents']."</option>";
				//else
				//	$x2 .= "<option value='5' >".$Lang['iMail']['FieldTitle']['ToSubjectGroupParents']."</option>";
				$js_all_subject_group_parent = $IMap->GetAllSameSubjectGroupParentEmailByUserIdentity($user_id);
				$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToSubjectGroupParents'],'name'=>'ChooseGroupID[]','value'=>'5','selected'=>in_array(5,$ChooseGroupID),'values'=>$js_all_subject_group_parent);
			}
		}
		//$x2 .= "</select>";
	}
	if($special_feature['alumni'] && $CatID == -4){
		## All Alumni ##
		$NameField = getNameFieldByLang();
		$sql = "Select UserID, ".$NameField." as Name, IMapUserEmail From INTRANET_USER where RecordStatus = 1 and RecordType = 4 AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY EnglishName";
		$result = $li->returnArray($sql,3);
		//$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
		if(sizeof($result)>0){
			$tmp_count = 0;
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name, $u_email) = $result[$i];
				if(count($SelectedRecipientsEmails)>0 && in_array($u_email,$SelectedRecipientsEmails)){
					continue;
				}
				$tmp_count++;
				//$final_email = '"'.$u_name.'"'." <".$u_email."> ";
				//$x3 .= "<option value='$final_email'>$u_name</option>";
				$final_email = $IMap->FormatRecipientOption('"'.$u_name.'"'." <".$u_email."> ");
				//$x3 .= '<option value="'.$final_email.'">'.$IMap->FormatRecipientOption($u_name).'</option>';
				$level3_ary['option'][] = array('display'=>$IMap->FormatRecipientOption($u_name),'name'=>'ChooseUserID[]','value'=>$final_email,'selected'=>false);
			}
			//if($tmp_count==0){
			//	$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
			//}
		}else{
			//$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}
		//$x3 .= "</select>";
	}
	
	if($CatID == 5){
		$sql = "SELECT b.UserID, ".getNameFieldByLang2("b.").", IMapUserEmail FROM INTRANET_PARENTRELATION AS a INNER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) WHERE a.ParentID = '$UserID' AND b.RecordStatus = 1 AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ";
		$result = $li->returnArray($sql,3);
		
		//$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
		if(sizeof($result)>0){
			$tmp_count = 0;
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name, $u_email) = $result[$i];
				if(count($SelectedRecipientsEmails)>0 && in_array($u_email,$SelectedRecipientsEmails)){
					continue;
				}
				$tmp_count++;
				//$x3 .= "<option value='$u_email'>".$u_name."</option>";
				$final_email = $IMap->FormatRecipientOption('"'.$u_name.'" <'.$u_email.'> ');
				//$x3 .= '<option value="'.$final_email.'">'.$IMap->FormatRecipientOption($u_name).'</option>';
				$level3_ary['option'][] = array('display'=>$IMap->FormatRecipientOption($u_name),'name'=>'ChooseUserID[]','value'=>$final_email,'selected'=>false);
			}
			//if($tmp_count==0){
			//	$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
			//}
		}else{
			//$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}
		//$x3 .= "</select>";
	}
	if($CatID == 6){
		$sql = "SELECT b.UserID, ".getNameFieldByLang2("b.").", IMapUserEmail FROM INTRANET_PARENTRELATION AS a INNER JOIN INTRANET_USER AS b ON (a.ParentID = b.UserID) WHERE a.StudentID = '$UserID' AND b.RecordStatus = 1  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ";
		$result = $li->returnArray($sql,3);
		
		//$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
		if(sizeof($result)>0){
			$tmp_count = 0;
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name, $u_email) = $result[$i];
				if(count($SelectedRecipientsEmails)>0 && in_array($u_email,$SelectedRecipientsEmails)){
					continue;
				}
				$tmp_count++;
				//$x3 .= "<option value='$u_email'>".$u_name."</option>";
				$final_email = $IMap->FormatRecipientOption('"'.$u_name.'" <'.$u_email.'> ');
				//$x3 .= '<option value="'.$final_email.'">'.$IMap->FormatRecipientOption($u_name).'</option>';
				$level3_ary['option'][] = array('display'=>$IMap->FormatRecipientOption($u_name),'name'=>'ChooseUserID[]','value'=>$final_email,'selected'=>false);
			}
			//if($tmp_count==0){
			//	$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
			//}
		}else{
			//$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}
		//$x3 .= "</select>";
	}
	
	if($CatID == "SHARED_MAILBOX"){
		$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
		if(sizeof($SharedMailBoxes)>0){
			$tmp_count = 0;
			for($i=0;$i<sizeof($SharedMailBoxes);$i++){
				list($mail_box_id,$mail_box_name,$display_name) = $SharedMailBoxes[$i];
				if(count($SelectedRecipientsEmails)>0 && in_array($mail_box_name,$SelectedRecipientsEmails)){
					continue;
				}
				//$final_email = htmlspecialchars('"'.$display_name.'"'." <".$mail_box_name."> ",ENT_QUOTES);
				//$x3 .= "<option value='".$final_email."'>".htmlspecialchars($display_name,ENT_QUOTES)."</option>";
				//$final_email = str_replace(array('<','>','"'),array('&lt;','&gt;','&quot;'),'"'.$display_name.'"'." <".$mail_box_name."> ");
				//$x3 .= '<option value="'.$final_email.'">'.str_replace(array('<','>','"'),array('&lt;','&gt;','&quot;'),$display_name).'</option>';
				$final_email = $IMap->FormatRecipientOption('"'.$display_name.'"'." <".$mail_box_name."> ");
				//$x3 .= '<option value="'.$final_email.'">'.$IMap->FormatRecipientOption($display_name).'</option>';
				$level3_ary['option'][] = array('display'=>$IMap->FormatRecipientOption($display_name),'name'=>'ChooseUserID[]','value'=>$final_email,'selected'=>false);
				$tmp_count++;
			}
			//if($tmp_count==0){
			//	$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
			//}
		}else{
			//$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}
		//$x3 .= "</select>";
	}
	
	if($CatID == "INTERNAL_GROUP"){
		$sql_int_group = "SELECT 
								AliasID, AliasName 
							FROM 
								INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL 
							WHERE 
								OwnerID = '$UserID' 
							ORDER BY 
								AliasName ";
		$InternalRecipientGroups =  $li->returnArray($sql_int_group);
		$namefield = getNameFieldWithClassNumberByLang("u.");
		$sql = "SELECT 
    				DISTINCT ga.AliasID,ge.TargetID,$namefield as UserName,u.IMapUserEmail 
    			FROM INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL as ga 
				INNER JOIN INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as ge ON ga.AliasID=ge.AliasID AND ga.OwnerID='$UserID'
				INNER JOIN INTRANET_USERGROUP as ug ON ge.TargetID=ug.GroupID 
				INNER JOIN INTRANET_USER as u ON u.UserID=ug.UserID 
				WHERE ga.OwnerID='$UserID' AND ge.RecordType = 'G' AND (u.IMapUserEmail IS NOT NULL AND u.IMapUserEmail<> '') 
				ORDER BY u.EnglishName";
	    $internal_group_groups = $li->returnArray($sql);    	        
	   
    	$sql = "SELECT 
    				DISTINCT ga.AliasID,ge.TargetID,$namefield as UserName,u.IMapUserEmail 
    			FROM INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL as ga  
				INNER JOIN INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as ge ON ga.AliasID=ge.AliasID AND ga.OwnerID='$UserID'
				INNER JOIN INTRANET_USER as u ON ge.TargetID = u.UserID
				WHERE ga.OwnerID='$UserID' AND ge.RecordType = 'U' AND (u.IMapUserEmail IS NOT NULL AND u.IMapUserEmail<> '')
				ORDER BY u.EnglishName ";
	    $internal_group_users = $li->returnArray($sql);
		
		//$js_internal_groups  = 'var js_internal_group_alias={};'."\n"; // store group AliasID to its member users array 
		//$js_internal_groups .= 'var js_internal_group_target_group = {};'."\n"; // store group type TargetID to users array
		//$js_internal_groups .= 'var js_internal_group_target_user = {};'."\n"; // store user type TargetID to user
		
		$targetIdToInternalGroupMembers = array();
		
		for($i=0;$i<sizeof($internal_group_groups);$i++){
			list($t_alias_id, $t_target_id, $t_name, $t_email) = $internal_group_groups[$i];
			//$js_internal_groups .= 'if(!js_internal_group_alias['.$t_alias_id.']){'."\n";
			//$js_internal_groups .= '  js_internal_group_alias['.$t_alias_id.']=[];'."\n";
			//$js_internal_groups .= '}'."\n";
			//$js_internal_groups .= 'js_internal_group_alias['.$t_alias_id.'].push(\''.'"'.str_replace(array('&#039;',"'",'&quot;','"','&amp;'),array("'","\'",'\"','\"','&'),$t_name).'"<'.$t_email.'>'.'\');'."\n";
			
			//$js_internal_groups .= 'if(!js_internal_group_target_group['.$t_target_id.']){'."\n";
			//$js_internal_groups .= '  js_internal_group_target_group['.$t_target_id.']=[];'."\n";
			//$js_internal_groups .= '}'."\n";
			//$js_internal_groups .= 'js_internal_group_target_group['.$t_target_id.'].push(\''.'"'.str_replace(array('&#039;',"'",'&quot;','"','&amp;'),array("'","\'",'\"','\"','&'),$t_name).'"<'.$t_email.'>'.'\');'."\n";
			
			if(!isset($targetIdToInternalGroupMembers[$t_target_id])){
				$targetIdToInternalGroupMembers[$t_target_id] = array();
			}
			$targetIdToInternalGroupMembers[$t_target_id][] = '"'.str_replace(array('&#039;',"'",'&quot;','"','&amp;'),array("'","\'",'\"','\"','&'),$t_name).'"<'.$t_email.'>';
		}
		/*
		for($i=0;$i<sizeof($internal_group_users);$i++){
			list($t_alias_id, $t_target_id, $t_name, $t_email) = $internal_group_users[$i];
			$js_internal_groups .= 'if(!js_internal_group_alias['.$t_alias_id.']){'."\n";
			$js_internal_groups .= '  js_internal_group_alias['.$t_alias_id.']=[];'."\n";
			$js_internal_groups .= '}'."\n";
			$js_internal_groups .= 'js_internal_group_alias['.$t_alias_id.'].push(\''.'"'.str_replace(array('&#039;',"'",'&quot;','"','&amp;'),array("'","\'",'\"','\"','&'),$t_name).'"<'.$t_email.'>'.'\');'."\n";
			
			$js_internal_groups .= 'js_internal_group_target_user['.$t_target_id.']=\''.'"'.str_replace(array('&#039;',"'",'&quot;','"','&amp;'),array("'","\'",'\"','\"','&'),$t_name).'"<'.$t_email.'>'.'\';'."\n";
		}
		*/
		//debug_r(htmlspecialchars($js_internal_groups,ENT_QUOTES));
		//$x2 = "<select name='ChooseGroupID[]' id='ChoooseGroupID[]' multiple='multiple' size='10'>"; // AliasID
		if(count($InternalRecipientGroups)==0){
			//$x2 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}else{
			for($i=0;$i<sizeof($InternalRecipientGroups);$i++){
				$x2 .= "<option value='".$InternalRecipientGroups[$i]['AliasID']."' ".(count($ChooseGroupID)>0 && in_array($InternalRecipientGroups[$i]['AliasID'],$ChooseGroupID)?"selected":"").">".$InternalRecipientGroups[$i]['AliasName']."</option>";
				$level2_ary['option'][] = array('display'=>$InternalRecipientGroups[$i]['AliasName'],'name'=>'ChooseGroupID[]','value'=>$InternalRecipientGroups[$i]['AliasID'],'selected'=>count($ChooseGroupID)>0 && in_array($InternalRecipientGroups[$i]['AliasID'],$ChooseGroupID));
			}
		}
		//$x2.= "</select>";
	}
}

if($GroupOpt == 2 && $CatID == 4 && $ChooseGroupCatID!="")
{
	if($_SESSION['SSV_USER_TARGET']['All-Yes'])
	{
		$title_field = $lgrouping->getGroupTitleByLang();
		$sql = "SELECT GroupID, $title_field as Title FROM INTRANET_GROUP WHERE RecordType = '".$ChooseGroupCatID."' AND RecordType != 0 AND AcademicYearID = '$CurrentAcademicYearID' ORDER BY $title_field";
	}else{
		
		$title_field = $lgrouping->getGroupTitleByLang("a.");
		if($result_to_group_options['ToTeacher'] || $result_to_group_options['ToStaff'] || $result_to_group_options['ToStudent'] || $result_to_group_options['ToParent'] || ($special_feature['alumni'] && $result_to_group_options['ToAlumni']))
		{
			$sql = "SELECT a.GroupID, $title_field as Title FROM INTRANET_GROUP AS a INNER JOIN INTRANET_USERGROUP AS b ON (a.GroupID = b.GroupID) WHERE a.RecordType = '".$ChooseGroupCatID."' AND a.RecordType != 0 AND a.AcademicYearID = '$CurrentAcademicYearID' AND b.UserID = '$UserID' ORDER BY $title_field";
		}
	}
	$GroupArray = $li->returnArray($sql,2);
	//$x2_5 = "<select name='ChooseGroupID[]' id='ChooseGroupID[]' multiple size='10'>";
	if(sizeof($GroupArray) > 0){
		$sql = "SELECT MAX(GroupID) FROM INTRANET_GROUP";
		$MaxGroupID = $li->returnVector($sql);
		// init js array to store group alias email
		//$js_all_group_email .= "var js_group_email = new Array(".$MaxGroupID[0].");\n";
		for($i=0; $i<sizeof($GroupArray); $i++)
		{
			list($GroupID, $GroupName) = $GroupArray[$i];
			//if(sizeof($ChooseGroupID)>0)
			//	$x2_5 .= "<option value='$GroupID' ".((in_array($GroupID,$ChooseGroupID))?"SELECTED":"").">".$GroupName."</option>";
			//else
			//	$x2_5 .= "<option value='$GroupID' >".$GroupName."</option>";
				
			## Get All Related Email in selected Group (for group alias use) ##
			$targetEmail = $IMap->GetAllGroupEmail($GroupID,$result_to_group_options['ToTeacher'],$result_to_group_options['ToStaff'],$result_to_group_options['ToStudent'],$result_to_group_options['ToParent'],$result_to_group_options['ToAlumni']);
			//$js_all_group_email .= "js_group_email[".$GroupID."] = '".str_replace("'","\'",htmlspecialchars_decode($targetEmail,ENT_QUOTES))."';\n";
			
			$level2_ary['option'][] = array('display'=>$GroupName,'name'=>'ChooseGroupID[]','value'=>$GroupID,'selected'=>in_array($GroupID,$ChooseGroupID),'values'=>$targetEmail);
		}
	}else{
		//$x2_5 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
	}
	//$x2_5 .= "</select>";
	$ShowSubGroupSelection = true;
}
else
{
	$ShowSubGroupSelection = false;
}

if($CatID!='' && $CatID != 0 && $CatID!=-2 && $CatID!=5 && $CatID!=6 && $CatID!=-3 && $CatID!=-4)
{
	if(($GroupOpt == 1 && $CatID != 4) || $ShowSubGroupSelection || $CatID == "INTERNAL_GROUP") {
		$return_ary[] = $level2_ary;
	}
}

if($CatID != "" && sizeof($ChooseGroupID)>0)
{
	$sql = "";
	if($CatID == -1) // teaching staff
	{
		for($i=0; $i<sizeof($ChooseGroupID); $i++)
		{			
			if($ChooseGroupID[$i] == 1)
			{
				## All Teaching Staff ##
				if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent") || ($special_feature['alumni'] && $identity == "Alumni"))
				{
					
					if($sys_custom['hideTeacherTitle']){
						//TODO: Kenneth
						$isTitleDisabled=true;
						$prefix = "all_user.";
						$all_sql = "(SELECT all_user.UserID, ".getNameFieldWithClassNumberByLang($prefix,$isTitleDisabled).", IMapUserEmail FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '')  ORDER BY IFNULL(all_user.ClassName,''), IFNULL(all_user.ClassNumber,0), all_user.EnglishName)";
					
					}else{
						$all_sql = "(SELECT all_user.UserID, ".getNameFieldWithClassNumberByLang("all_user.").", IMapUserEmail FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '')  ORDER BY IFNULL(all_user.ClassName,''), IFNULL(all_user.ClassNumber,0), all_user.EnglishName)";
					}
					$sql = $all_sql;
					//debug_pr(getNameFieldWithClassNumberByLang($all_sql));
				}
			}
			if($ChooseGroupID[$i] == 2)
			{
				## Form Teacher ##
				if($identity == "Teaching")
				{
					
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", IMapUserEmail FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID')  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to form teacher
				}
				if($identity == "Student")
				{
					
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", IMapUserEmail FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND  b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID')  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "Parent")
				{
					
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", IMapUserEmail FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID')  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
			}
			if($ChooseGroupID[$i] == 3)
			{		
				## Class Teacher ##
				if($identity == "Teaching")
				{
					
					$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", IMapUserEmail FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID') AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '')  ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to class teacher
				}
				if($identity == "Student")
				{
					
					$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", IMapUserEmail FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID')  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "Parent")
				{
					
					$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", IMapUserEmail FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID')  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
			}
			if($ChooseGroupID[$i] == 4)
			{
				## Subject Teacher ##
				if($identity == "Teaching")
				{					
					
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", IMapUserEmail FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to subject teacher
				}
				if($identity == "Student")
				{					
					
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", IMapUserEmail FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "Parent")
				{					
					
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", IMapUserEmail FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = $UserID AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
			}
			if($ChooseGroupID[$i] == 5)		/* start from here */
			{
				## Subject Group Teacher ##
				if($identity == "Teaching")
				{
					
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", IMapUserEmail FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID')  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to subject group teacher
				}
				if($identity == "Student")
				{
					
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", IMapUserEmail FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID')  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "Parent")
				{
					
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", IMapUserEmail FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID')  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
			}
		}
		$result = $li->returnArray($sql);
	}
	if($CatID == 2) // student
	{
		for($i=0; $i<sizeof($ChooseGroupID); $i++)
		{
			if($ChooseGroupID[$i] == 1)
			{
				## All Student ##
				if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent") || ($special_feature['alumni'] && $identity == "Alumni"))
				{
					
					$all_sql = "(SELECT DISTINCT UserID, ".getNameFieldWithClassNumberByLang().", IMapUserEmail FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(ClassName,''), IFNULL(ClassNumber,0), EnglishName)";
					$sql = $all_sql;
				}
			}
			if($ChooseGroupID[$i] == 2)
			{
				## My Form Student ##
				if($identity == "Teaching")
				{
					
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", IMapUserEmail FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to form student
				}
				if($identity == "Student")
				{
					
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", IMapUserEmail FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '')  ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "Parent")
				{
					
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", IMapUserEmail FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '')  ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
			}
			if($ChooseGroupID[$i] == 3)
			{
				## My Class Student ##
				if($identity == "Teaching")
				{
					
					$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.").", IMapUserEmail FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to class student
				}
				if($identity == "Student")
				{
					
					$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.").", IMapUserEmail FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "Parent")
				{
					
					$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.").", IMapUserEmail FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
			}
			if($ChooseGroupID[$i] == 4)
			{
				## My Subject Student ##
				if($identity == "Teaching")
				{
					
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", IMapUserEmail FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to Subject student
				}
				if($identity == "Student")
				{
					
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", IMapUserEmail FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "Parent")
				{
					
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", IMapUserEmail FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
			}
			if($ChooseGroupID[$i] == 5)
			{		
				## My Subject Group Student ##		
				if($identity == "Teaching")
				{	
					
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", IMapUserEmail FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to subject group student
				}
				if($identity == "Student")
				{	
					
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", IMapUserEmail FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "Parent")
				{	
					
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", IMapUserEmail FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
			}
		}
		$result = $li->returnArray($sql);
	}
	if($CatID == 3) // parent
	{
		for($i=0; $i<sizeof($ChooseGroupID); $i++)
		{
			if($ChooseGroupID[$i] == 1)
			{
				## All Parents ##
				if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent") || ($special_feature['alumni'] && $identity == "Alumni"))
				{					
					
					$NameField = "IF(c.EnglishName != '' AND c.EnglishName IS NOT NULL,
									  IF(c.ClassName != '' AND c.ClassName IS NOT NULL and c.ClassNumber != '' AND c.ClassNumber IS NOT NULL,
											CONCAT('(',c.ClassName,'-',c.ClassNumber,') ', ".getNameFieldByLang2('c.').",'".$Lang['iMail']['FieldTitle']['TargetParent']."', IF(a.EnglishName !='' AND a.EnglishName IS NOT NULL,CONCAT(' (',".getNameFieldByLang2('a.').",')'),'')),
											".getNameFieldByLang2('a.')."),
									  IF(TRIM(a.EnglishName)='' AND TRIM(a.ChineseName)='',a.UserLogin,".getNameFieldByLang2('a.').")
									)";
					$SortField = getParentNameWithStudentInfo2("c.","a.","",0,"0",5);
					
					if ($sys_custom['iMailSendToParent'] == true)
					{
						$all_sql = $IMap->GetSQL_ParentEmail4All($NameField,$SortField);
					}
					else
					{					
						$all_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, a.IMapUserEmail as UserEmail, $SortField as SortField  FROM INTRANET_USER AS a LEFT OUTER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) LEFT OUTER JOIN INTRANET_USER AS c ON (b.StudentID = c.UserID) WHERE a.RecordType = 3 AND a.RecordStatus = 1 AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') ";
					}
					$sql = $all_sql;
				}
			}
			if($ChooseGroupID[$i] == 2)
			{
				## My Form Parents ##
				if($identity == "Teaching")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					
					if ($sys_custom['iMailSendToParent'] == true)
					{
						$form_sql = $IMap->GetSQL_ParentEmail4FormClass($name_field,$sort_field,$UserID,$CurrentAcademicYearID,$identity,$level="form");
					}
					else
					{					
						$form_sql = " SELECT DISTINCT c.UserID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') ";
					}
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to form parent
				}
				if($identity == "Student")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					
					
					$form_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') ";

					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "Parent")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					
					$form_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') ";

					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
			}
			if($ChooseGroupID[$i] == 3)
			{
				## My Class Parents ##
				if($identity == "Teaching")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					
					if ($sys_custom['iMailSendToParent'] == true)
					{
						$class_sql = $IMap->GetSQL_ParentEmail4FormClass($name_field,$sort_field,$UserID,$CurrentAcademicYearID,$identity,$level="class");
					}
					else
					{					
						$class_sql = " SELECT DISTINCT c.UserID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = '$CurrentAcademicYearID')) AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') ";
					}
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to class parent
				}
				if($identity == "Student")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					
					$class_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID')) AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') ";

					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "Parent")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					
					$class_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') ";

					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
			}
			if($ChooseGroupID[$i] == 4)
			{
				## My Subject Parents ##
				if($identity == "Teaching")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					

					if ($sys_custom['iMailSendToParent'] == true)
					{
						$subject_sql = $IMap->GetSQL_ParentEmail4SubjectGroup($name_field,$sort_field,$UserID,$CurrentTermID,$level="subject");
					}
					else
					{					
						$subject_sql = " SELECT DISTINCT c.UserID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') ";
					}
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to subject parent
				}
				if($identity == "Student")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					
					$subject_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') ";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "Parent")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					
					$subject_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') ";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
			}
			if($ChooseGroupID[$i] == 5)
			{
				## My Subject Group Parents ##
				if($identity == "Teaching")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					

					if ($sys_custom['iMailSendToParent'] == true)
					{
						$subject_group_sql = $IMap->GetSQL_ParentEmail4SubjectGroup($name_field,$sort_field,$UserID,$CurrentTermID,$level="subject_group");
					}
					else
					{					
						$subject_group_sql = " SELECT DISTINCT c.UserID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail,$sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') ";
					}
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to subject group parent
				}
				if($identity == "Student")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					
					$subject_group_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID')) AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') ";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "Parent")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					
					$subject_group_sql = " SELECT DISTINCT b.ParentID, $name_field as UserName, c.IMapUserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') ";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
			}			
		}
		if($sql != "") $sql.=" ORDER BY SortField ";
//			$sql = "select 12 , '(S1L-14) luckychan\'s Parent (cabug2)','mfkhoe@broadlearning.com**cameronlau@broadlearning.com'";		
		$result = $li->returnArray($sql);
	}
	if($CatID == 4) // Group
	{
		if(sizeof($ChooseGroupID)>0)
		{			
			$TargetGroupID = implode(",",$ChooseGroupID);
			
			$cond = "";
			if($result_to_group_options['ToTeacher'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 1 AND (a.Teaching = 1)) ";
			}
			if($result_to_group_options['ToStaff'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 1 AND (TEACHING = 0 OR a.Teaching IS NULL)) ";
			}
			if($result_to_group_options['ToStudent'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 2) ";
			}
			if($result_to_group_options['ToParent'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 3) ";
			}
			if($special_feature['alumni'] && $result_to_group_options['ToAlumni'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 4) ";
			}
			
			if($cond != "")
				$final_cond = " AND ( $cond ) ";
			
			if($sys_custom['hideTeacherTitle']){
				$isTitleDisabled = true;
			}else{
				$isTitleDisabled = false;
			}
			
			
			$sql = "SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.",$isTitleDisabled).", IMapUserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_USERGROUP AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.GroupID IN ($TargetGroupID)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') $final_cond ORDER BY a.RecordType, IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName";
			$result = $li->returnArray($sql,2);
		}
	}
	
	if($CatID == "INTERNAL_GROUP"){
		$AliasImplode = "'".implode("','",$ChooseGroupID)."'";
		
		$sql = "SELECT 
    				DISTINCT  CONCAT('G',a.TargetID),b.Title,a.TargetID  
    			FROM 
    				INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as a
				INNER JOIN 
					INTRANET_GROUP as b 
				ON 
					a.TargetID = b.GroupID
				WHERE 
					a.AliasID IN ($AliasImplode) AND a.RecordType = 'G'
				ORDER BY 
					b.Title ";
	    $internal_group_groups = $li->returnArray($sql,3);    	        
		
	    $namefield = getNameFieldWithClassNumberByLang("b.");
	    $sql = "	SELECT 
	    				DISTINCT CONCAT('U',a.TargetID),$namefield 
	    			FROM 
	    				INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as a
					INNER JOIN 
						INTRANET_USER as b 
					ON 
						a.TargetID = b.UserID
					WHERE 
						a.AliasID IN ($AliasImplode) AND a.RecordType = 'U' AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '')
					ORDER BY 
						b.EnglishName ";
	    $internal_group_users = $li->returnArray($sql,2);	
		
		//$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple='multiple' size='10'>";
		for($i=0;$i<sizeof($internal_group_groups);$i++){
			//$x3 .= "<option value='".$internal_group_groups[$i][0]."'>".$internal_group_groups[$i][1]."</option>";
			$group_values = isset($targetIdToInternalGroupMembers[$internal_group_groups[$i][2]]) && count($targetIdToInternalGroupMembers[$internal_group_groups[$i][2]])>0? implode('; ',$targetIdToInternalGroupMembers[$internal_group_groups[$i][2]]).'; ' : '';
			$level3_ary['option'][] = array('display'=>$internal_group_groups[$i][1],'name'=>'ChooseUserID[]','value'=>$internal_group_groups[$i][0],'selected'=>false,'values'=>$group_values);
		}
		for($i=0;$i<sizeof($internal_group_users);$i++){
			//$x3 .= "<option value='".$internal_group_users[$i][0]."'>".$internal_group_users[$i][1]."</option>";
			$level3_ary['option'][] = array('display'=>$internal_group_users[$i][1],'name'=>'ChooseUserID[]','value'=>$internal_group_users[$i][0],'selected'=>false);
		}
		//$x3 .= "</select>";
		
	}else{
		//$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
		if(sizeof($result)>0){
			$tmp_count = 0;
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name, $u_email) = $result[$i];
				if(count($SelectedRecipientsEmails)>0 && in_array($u_email,$SelectedRecipientsEmails)){
					continue;
				}
			//	$final_email = htmlspecialchars('"'.$u_name.'"'." <".$u_email."> ",ENT_QUOTES);
				//$final_email = str_replace(array('<','>','"'),array('&lt;','&gt;','&quot;'),'"'.$u_name.'"'." <".$u_email."> ");
				//$x3 .= "<option value='$u_email'>$u_name</option>";
			//	$x3 .= "<option value='$final_email'>".htmlspecialchars($u_name,ENT_QUOTES)."</option>";
				//$x3 .= '<option value="'.$final_email.'">'.str_replace(array('<','>','"'),array('&lt;','&gt;','&quot;'),$u_name).'</option>';
				//$final_email = $IMap->FormatRecipientOption('"'.$IMap->replaceQuotesForDisplayName($u_name).'"'." <".$u_email."> ");
				$final_email = '"'.$IMap->replaceQuotesForDisplayName($u_name).'"'." <".$u_email.">";
				//$x3 .= '<option value="'.$final_email.'">'.$IMap->FormatRecipientOption($u_name).'';
				$level3_ary['option'][] = array('display'=>$IMap->FormatRecipientOption($u_name),'name'=>'ChooseUserID[]','value'=>$final_email,'selected'=>false);
				$tmp_count++;
			}
			//if($tmp_count == 0){
			//	$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
			//}
		}else{
			//$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}
		//$x3 .= "</select>";
	}
}

$direct_to_step3 = false;
if($CatID != "")
{
	if($GroupOpt == 3 && $CatID == "INTERNAL_GROUP" && sizeof($ChooseGroupID)>0){
		$direct_to_step3 = true;
	}else if($GroupOpt == 3 && $CatID == "SHARED_MAILBOX"){
		$direct_to_step3 = true;
	}else if(($GroupOpt == 1) && (($CatID == -3)||($CatID == -2)||($CatID == 5)||($CatID == 6) || ($CatID == -4))){
		$direct_to_step3 = true;
	}else if(($GroupOpt == 2) && ($CatID == 4) && (sizeof($ChooseGroupID)>0) && (sizeof($ChooseGroupCatID)>0)) {
		$direct_to_step3 = true;
	} else {
		if(($GroupOpt == 1) && ($CatID != 4) && (sizeof($ChooseGroupID)>0)){
			$direct_to_step3 = true;
		}
	}
}

if($direct_to_step3){
	$return_ary[] = $level3_ary;
}

intranet_closedb();

echo $json_obj->encode($return_ary);
die;
?>