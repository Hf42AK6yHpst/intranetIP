<?php
// Editing by 
/*
Required parameters:
$token: eClass App token
$email: user full email address
//$access_time: unix timestamp 
//$hashval: sha1 of "$email###$access_time###$salt"
$keyword: search keyword
*/
/*
Sample json output: 
{
  "Users": [
    "\"Not a group user 1\" <user1@testmail2.broadlearning.com>",
    "\"Not a group user 2\" <user2@testmail2.broadlearning.com>"
  ],
  "InternalGroups": {
    "Discipline Group": [
      "\"teacher_t1\" <teacher_t1@testmail2.broadlearning.com>",
      "\"teacher_t2\" <teacher_t2@testmail2.broadlearning.com>",
      "\"teacher_t3\" <teacher_t3@testmail2.broadlearning.com>"
    ],
    "Group 1A - Parent": [
      "\"Parent 1\" <parent_p1@testmail2.broadlearning.com>",
      "\"Parent 2\" <parent_p2@testmail2.broadlearning.com>"
    ],
    "Group 1A - Student": [
      "\"Student 1 (1A-1)\" <student_s1@testmail2.broadlearning.com>"
    ]
  },
  "ExternalGroups": {
    "External Group 1": [
      "\"Peter\" <peter@gmail.com>",
      "\"May\" <may@hotmail.com>",
      "\"Test\" <test@broadlearning.com>"
    ],
    "External Group 2": [
      "\"John Chan\" <johnchan@gmail.com>"
    ],
    "BroadLearning Group": [
      "\"BroadLearning Support\" <support@broadlearning.com>"
    ]
  }
}
*/

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_api.php");
include_once($PATH_WRT_ROOT."includes/json.php");

$json_obj = new JSON_obj();
$return_ary = array();
//$salt = 'E1CuML80Rxiw30y1';

header("Content-Type: application/json;charset=utf-8");
/*
if(!isset($_POST['email']) || $_POST['email']=='' || !isset($_POST['access_time']) || $_POST['access_time']=='' || !isset($_POST['hashval']) || $_POST['hashval']=='')
{
	$return_ary['Error'] = 'Invalid parameters.';
	echo $json_obj->encode($return_ary);
	exit;
}

$now_ts = time();
$ts = intval($_POST['access_time']);
$ts_threshold = 10; // valid within 10 seconds

if($now_ts - $ts > $ts_threshold){
	$return_ary['Error'] = 'Access timeout.';
	echo $json_obj->encode($return_ary);
	exit;
}

$hash_to_check = sha1($_POST['email'].'###'.$_POST['access_time'].'###'.$salt);
if($hash_to_check != $_POST['hashval']){
	$return_ary['Error'] = 'Invalid hash value.';
	echo $json_obj->encode($return_ary);
	exit;
}
*/

if(/*!isset($_POST['email']) || $_POST['email']=='' || */ !isset($_POST['token']) || $_POST['token']=='')
{
	$return_ary['Error'] = 'Invalid parameters.';
	echo $json_obj->encode($return_ary);
	exit;
}

intranet_opendb();

$libdb = new libdb();
$libAppApi = new libeClassAppApi();

$token = trim($_POST['token']);
$apiResult = $libAppApi->retrieveMailUserInfoByToken($token);

if($apiResult == '-999'){
	$return_ary['Error'] = 'Invalid token.';
	echo $json_obj->encode($return_ary);
	intranet_closedb();
	exit;
}

//$email = trim($_POST['email']);
$user_id = $apiResult['userId'];
$keyword = trim($_POST['keyword']); 
if($libdb->isMagicQuotesOn()){
	$keyword = stripslashes($keyword);
}
if($keyword==''){
	$return_ary['Error'] = 'Invalid search keyword.';
	echo $json_obj->encode($return_ary);
	intranet_closedb();
	exit;
}

$SearchValue = $libdb->Get_Safe_Sql_Like_Query($keyword);

//$sql = "SELECT UserID,UserLogin,ImapUserEmail,RecordType,RecordStatus FROM INTRANET_USER WHERE ImapUserEmail='".$email."'";
$sql = "SELECT UserID,UserLogin,ImapUserEmail,RecordType,RecordStatus FROM INTRANET_USER WHERE UserID='".$user_id."'";
$user_records= $libdb->returnResultSet($sql);
if(count($user_records)==0){
	$return_ary['Error'] = 'User does not exist.';
	echo $json_obj->encode($return_ary);
	intranet_closedb();
	exit;
}

if($user_records[0]['RecordStatus']!=1){
	$return_ary['Error'] = 'Inactive user.';
	echo $json_obj->encode($return_ary);
	intranet_closedb();
	exit;
}

if(isset($_POST['lang']) && in_array($_POST['lang'],array('en','b5','gb'))){
	$_SESSION['intranet_session_language'] = $_POST['lang'];
	$intranet_session_language = $_SESSION['intranet_session_language'];
}
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

$user_id = $user_records[0]['UserID'];
$user_type = $user_records[0]['RecordType'];
$email = $user_records[0]['ImapUserEmail'];

$return_emails = array();

$AddressBookSql = "SELECT 
						CONCAT('\"',TargetName,'\" <',TargetAddress,'>') as Email 
					FROM 
						INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL
					WHERE 
						OwnerID = $user_id
						AND 
						(
							TargetName LIKE '%$SearchValue%'
							OR TargetAddress LIKE '%$SearchValue%'
						)
					";
$AddressBookArr = $libdb->returnVector($AddressBookSql); 
if(count($AddressBookArr)>0){
	$return_emails = array_merge($return_emails, $AddressBookArr);
}

### If the user is a staff/teacher, they can also search the email address by typing the login name of the target user
$InternalArr = array();
if($user_type == USERTYPE_STAFF)
{
	if($sys_custom['iMailPlus']['EmailNameNoNickName'])
	{
		$NameField = getNameFieldByLang();
	}else{
		$NameField = " IF(NickName IS NULL OR TRIM(NickName)='',".getNameFieldByLang().",NickName) ";
	}
	if($sys_custom['iMailPlusEmailNameWithoutClassInfo']) {
		$ClassInfo = "''";
	}else{
		$ClassInfo = "IF(RecordType = 1, '', IF(ClassName = '' OR ClassName IS NULL, '', CONCAT(' (',ClassName,'-',ClassNumber,')')))";
	}
	$InternalSql = "SELECT 
						CONCAT('\"',$NameField, $ClassInfo ,'\" <',ImapUserEmail,'>') as Email 
					FROM 
						INTRANET_USER 
					WHERE 
						RecordStatus = 1
						AND 
						(
							EnglishName LIKE '%$SearchValue%'
							OR ChineseName LIKE '%$SearchValue%' 
							OR ImapUserEmail LIKE '%$SearchValue%'
							OR UserLogin LIKE '%$SearchValue%' 
							OR ClassName LIKE '%$SearchValue%' 
						)
						AND (ImapUserEmail IS NOT NULL AND ImapUserEmail <> '') 
					ORDER BY RecordType, IFNULL(ClassName,''), IFNULL(ClassNumber,0), EnglishName";
	$InternalArr = $libdb->returnVector($InternalSql);
	
	if(count($InternalArr)>0){
		$return_emails = array_merge($return_emails, $InternalArr);
	}
}


# search internal group name
if($sys_custom['iMailPlus']['EmailNameNoNickName'])
{
	$NameField = getNameFieldByLang("c.");
}else{
	$NameField = " IF(c.NickName IS NULL OR TRIM(c.NickName)='',".getNameFieldByLang("c.").",c.NickName) ";
}
if($sys_custom['iMailPlusEmailNameWithoutClassInfo']) {
	$ClassInfo = "''";
}else{
	$ClassInfo = "IF(c.RecordType = 1, '', IF(c.ClassName = '' OR c.ClassName IS NULL, '', CONCAT(' (',c.ClassName,'-',c.ClassNumber,')')))";
}
# get current academic year id
$current_academic_year_id = Get_Current_Academic_Year_ID();
$sql = "
	SELECT 
		a.Title as GroupName, 
		CONCAT('\"',$NameField, IF(c.RecordType = 1, '', $ClassInfo),'\" <',c.ImapUserEmail,'>') as Email
	FROM 
		INTRANET_GROUP a 
		INNER JOIN INTRANET_USERGROUP b on a.GroupID = b.GroupID 
		INNER JOIN INTRANET_USER c ON c.UserID = b.UserID 
	WHERE 
		c.RecordStatus = 1 
		AND (a.AcademicYearID = '$current_academic_year_id' OR a.AcademicYearID IS NULL)
		AND (c.IMapUserEmail <> '' AND c.IMapUserEmail Is NOT NULL)
		AND (a.Title LIKE '%$SearchValue%' OR a.TitleChinese LIKE '%$SearchValue%')
	ORDER BY 
		a.Title
";

$GroupEmailArr = array();
$ResultArr = $libdb->returnArray($sql);
foreach((array)$ResultArr as $EmailArr)
{
	$GroupEmailArr[$EmailArr["GroupName"]][] = $EmailArr["Email"];
}

# Search External Group Name
$sql = "
	SELECT
		c.AliasName as GroupName,
		CONCAT('\"',b.TargetName,'\" <',b.TargetAddress,'>') as Email
  	FROM 
		INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY AS a
        INNER JOIN  INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL AS b ON a.TargetID = b.AddressID
		INNER JOIN INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL c ON a.AliasID = c.AliasID
	WHERE
		c.OwnerID = '$user_id'
		AND c.AliasName LIKE '%$SearchValue%'
";
$ResultArr = $libdb->returnArray($sql);
$ExGroupEmailArr = array();
foreach((array)$ResultArr as $EmailArr)
{
	$ExGroupEmailArr[$EmailArr["GroupName"]][] = $EmailArr["Email"];
}

$return_ary['Users'] = $return_emails;
$return_ary['InternalGroups'] = $GroupEmailArr;
$return_ary['ExternalGroups'] = $ExGroupEmailArr;

intranet_closedb();

echo $json_obj->encode($return_ary);
die;
?>