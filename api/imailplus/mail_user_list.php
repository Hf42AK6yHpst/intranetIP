<?php

/*
 * Required parameters:
 * - access_time : unix timestamp
 * - verify_code : sha1 of "$access_time###$salt_key"
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_api.php");
include_once($PATH_WRT_ROOT."includes/json.php");

$json_obj = new JSON_obj();
$return_ary = array();

header("Content-Type: application/json;charset=utf-8");

if(!isset($_POST['access_time']) || $_POST['access_time']=='' || !isset($_POST['verify_code']) || $_POST['verify_code']=='')
{
	$return_ary['Error'] = 'Invalid parameters.';
	echo $json_obj->encode($return_ary);
	exit;
}

$salt_key = 'LcOgIWpKTxblsY61TsPWs5jPp26K9oL7';
$now_ts = time();
$ts = intval($_POST['access_time']);
$ts_threshold = 10; // valid within 10 seconds

if($now_ts - $ts > $ts_threshold){
	$return_ary['Error'] = 'Access timeout.';
	echo $json_obj->encode($return_ary);
	exit;
}

$hash_to_check = sha1($_POST['access_time'].'###'.$salt_key);
if($hash_to_check != $_POST['verify_code']){
	$return_ary['Error'] = 'Invalid verification code.';
	echo $json_obj->encode($return_ary);
	exit;
}

intranet_opendb();

$api = new libeClassAppApi();

$return_ary = $api->retrieveMailUserList();

intranet_closedb();

echo $json_obj->encode($return_ary);
exit;
?>