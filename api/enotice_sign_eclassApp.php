<?php
// Using :

/********************** Change Log ***********************/
#
#   Date    :   2020-10-27  Ray
#               add alipaycn
#   Date    :   2020-09-25  Bill    [2020-0924-1538-17206]
#               Hide instruction - 'Please fill in the above reply slip and click Sign button to sign this notice.' if all questions' type are 'Not Applicable'
#   Date    :   2020-09-22  Bill    [2020-0922-1006-24206]
#               fixed: cannot display whole notice title if attachment file name too lang > break by chars
#   Date    :   2020-09-15  Bill    [2020-0915-1022-58066]
#               fixed: incorrect Question Relation handling if reply slip has MC questions that not required to be answered
# 	Date    :   2020-09-07  Bill    [2020-0604-1821-16170]
#			    updated - full right checking
#   Date    :   2020-09-03  Bill    [2020-0826-1451-05073]
#               Performance Issue: use JS to handle Question Relation
#   Date    :   2020-08-27  Bill    [2020-0826-1725-41260]
#               fixed cannot display question number
#   Date    :   2020-08-25  Bill    [2020-0821-1028-21206]
#               fixed cannot access urls with 'https://' due to jquery mobile event listeners
#   Date    :   2020-08-11  Ray
#               added wechat
#   Date    :   2020-07-29  Bill    [2020-0721-1316-45073]
#               Parent - can view notice related to own children only
#   Date    :   2020-07-23  Bill    [2020-0722-1649-52073]
#               Student - set role = "s"
#   Date    :   2020-07-21  Bill    [2020-0721-1316-45073]
#               Student - can view related notice only
#   Date    :   2020-07-16  Bill
#               [removed] specific NoticeID handling for UCCKE
#   Date    :   2020-07-15 Ray
#               added visamaster, multi gateway
#	Date	:	2019-11-12  Carlos
#				Check student subsidy identity with both notice start date and end date. 
#	Date	:	2019-10-25	Philips
#				Display $i_Notice_ReplySlip
#	Date	:	2019-10-16  Carlos
#				Hide debit method for eWallet payment.
#	Date	:	2019-09-24  Carlos
#				For payment notice, added CSRF token to prevent fast repeated submit.
#   Date    :   2019-09-05  Bill    [2019-0902-1654-50207]
#               fixed cannot access urls with '#' due to jquery mobile event listeners
#	Date	:	2019-09-04  Carlos
#				Fixed checking of TNG merchant account, allow no merchant account attached the notice, to be backward compatible.
#	Date	:	2019-08-23  Carlos
#				For payment notice, checked the notice selected merchant account service provider to display the available eWallet option.
#	Date	:	2019-08-19  Carlos
#				Cater display [Pay at school] display word if set.
#	Date	:	2019-08-05  Carlos
#				Added Tap and Go payment method.
#	Date	:	2019-06-14  Carlos
#				If use payment gateway and enabled [Pay at school] option, show Payment Method to let signer choose.
#	Date	:	2019-06-06	Carlos
#				For payment notice, added [Pay at school] button to sign the replyslip first but pay later.
#	Date	:	2019-04-08  Carlos
#				Improved reply slip with new js script /templates/forms/eNoticePayment_replyslip.js and php library eNoticePaymentReplySlip.php
#
#   Date    :   2019-03-06  (Bill)
#               for auth checking - get token from returnSimpleTokenByDateTime()     ($special_feature['download_attachment_add_auth_checking'])
#
#   Date    :   2018-12-06  (Bill)  [2018-1205-1037-34207]
#               display warning message if not issued notice
#
#	Date	:	2018-08-31 (Carlos)
#				Added flag $sys_custom['PaymentNoticeDoNotCheckBalance'] to ignore checking balance.
#
#	Date	:	2018-08-22 (Carlos)
#				Cater Alipay to allow resign for failed payments.
#
#   Date    :   2018-07-05 (Bill)   [DM#3447]
#               modified js calculateAmountTotal(), use parseFloat() and toFixed() to handle total amount calculation
#
#   Date    :   2018-03-23  Bill    [2018-0322-1718-26276]
#               Need to check both soft and hard delete
#
#	Date	:	2018-01-26	Carlos - Cater TNG fail to pay condition and let parent re-sign it.
#	Date	:	2018-01-24	Carlos - Count and display total amount for selected items that want to pay for.
#	Date	:	2018-01-17  Carlos
#				Skip checking balance if pay by TNG $sys_custom['ePayment']['TNG']
#
#	Date	:	2017-10-17	Bill	[2017-1009-1757-54256]
#				Hide Reply Slip after Notice Deadline ($sys_custom['eClassApp']['eNoticeHideReplySlipAfterDeadline'])
#
#	Date	:	2017-09-15	Bill	[2017-0206-1342-27225]
#				Improved: modified js checkform(), to perform deadline checking when submit
#				Hide Sign Status ($sys_custom['eClassApp']['eNoticeHideReplySlipSignStatus'])
#				Hide Other Info ($sys_custom['eClassApp']['eNoticeHideOtherInfoDisplay'])
#
#	Date	:	2017-08-16	Bill	[2017-0206-1342-27225]
#				Allow Users to submit new replies without limit ($sys_custom['eNotice']['SubmitNewReplyMulitpleTimes'])
#
#	Date	:	2016-11-14	Tiffany
#				Improve the deadline icon and show the signer name at last.
#				
#	Date	:	2016-09-09	Bill	[2016-0907-1616-07236]
#				fixed: direct use timestamp for deadline checking
#				fixed: not allow late sign for payment notice
#
#	Date	:	2016-09-07	Ivan [R103116] [ip.2.5.7.10.1]
#				Customized: added flag $sys_custom['eClassApp']['eNoticeHideSignButton'] to hide sign button 
#
#	Date	:	2016-08-11	Bill
#				fixed: cannot display error message for some must submit questions if answer is empty
#
#	Date	: 	2016-07-14	Bill	[2016-0113-1514-09066]
#				Support MC questions to set interdependence
#				Added response for questions that no required to submit
#
#	Date	: 	2016-06-10	Bill	(ip.2.5.7.7.1)	[2016-0113-1514-09066]
#				Merge logic from enotice_sign_eclassApp2.php, for interdependent eNotice questions
#				(Parent / Student notice & Module notice use different method to create reply slip content)
#
#	Date	:	2016-06-07	Ivan [B97041] (ip.2.5.7.7.1)
#				added checking of app enabled eNotice or not before displaying the notice
#
#	Date	:	2016-06-07	Kenneth (ip.2.5.7.7.1)
#				date checking: changed to datetime checking
#
#	Date	:	2016-04-10 Tiffany	[90645] (ip.2.5.7.4.1)
#				support student app
#
#	Date	:	2015-12-22 Pun	[90645] (ip.2.5.7.1.1)
#				support windows phone alert and confirm message
#
#	Date	:	2015-11-13	Bill	[2015-0615-1438-48014]
#				display * for Questions must be submitted (sheet.mustSubmit = 1)
#				perform answer checking for Questions must be submitted
#				- Not for payment notice
#
#	Date	:	2015-10-28	Bill	[2015-0416-1040-06164]
#				support merge notice content
#
#	Date	:	2015-04-21	Bill
#				change affected user type from users (without full right) to parents (not allow resign) [2015-0316-1603-27206]
#
#	Date	:	2015-04-02 [Bill]
#				not allow user resign enotice after deadline and resign payment notice [2015-0316-1603-27206]
#				allow PIC to sign if user is eNotice PIC and Settings - "Allow PIC to edit replies for parents." - true [2015-0323-1602-46073]
#
#	Date	:	2015-03-10 [Ivan] (ip.2.5.6.5.1)
#				Added record auth code for reply logic
#	Date	: 	2014-10-14 [Ivan] (ip.2.5.5.10.1)
#				Added auth code logic
#	Date	:	2014-10-13 [Roy]
#				[2014-1008-1215-48071] if it is payment notice, always require fill all fields
#	Date	:	2014-03-05 [Yuen]
#				support custom language (i.e. hardcode for different schools)
#	Date	:	2014-01-07 [Ivan]
#				modified commented "$editAllowed" to show sign status for all status now 
#	Date	:	2013-08-31 [Yuen]
#				add meta tag above to control the display scale and avoid display problem in Android 
#	Date	:	2013-05-09 [Yuen]
#				customized to require parent to input valid password in order to sign - control by flag $sys_custom['enotice_sign_with_password']
#	Date	:	2013-03-07 [Yuen]
#				force parent to fill in if the slip is required to
#	Date	:	2011-12-14 [Yuen]
#				change layout from 2 columns to 1 to avoid scrolling 
#
/********************** end of Change Log ***********************/

# set as default
if (!isset($sys_custom['enotice_sign_with_password'])) {
	$sys_custom['enotice_sign_with_password'] = false;
}

$NoUTF8 = true;
$PATH_WRT_ROOT = "../";

// set language
@session_start();
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();

$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

//# see if there is any custom setting
//if (file_exists($PATH_WRT_ROOT."file/eclass_app.php"))
//{
//	session_start();
//	include_once($PATH_WRT_ROOT."file/eclass_app.php");
//	$intranet_session_language = $eclass_app_language;
//}
//else
//{
//	$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
//	if ($parLang != '') {
//		switch (strtolower($parLang)) {
//			case 'en':
//				$intranet_hardcode_lang = 'en';
//				break;
//			default:
//				$intranet_hardcode_lang = 'b5';
//		}
//	}	
//}

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/eClassAppConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");

$authCode = $_GET['AuthCode'];
$authCode = ($authCode=='(null)')? '' : $authCode;

$delimiter = "###";
$keyStr = $special_feature['ParentApp_PrivateKey'].$delimiter.$NoticeID.$delimiter.$StudentID.$delimiter.$CurID;

# Generate token for auth checking
$dl_auth_token = '';
if($special_feature['download_attachment_add_auth_checking'])
{
    $dl_auth_token = returnSimpleTokenByDateTime();
    
//     unset($_SESSION['dlToken']);
//    
//     $dl_auth_token = md5($keyStr.$delimiter.time());
}

/* testing 
$NoticeID = 7285;
$StudentID = 1668;
$CurID = 1707;
*/
/* testing  
 */
if($token != md5($keyStr)) {
	include_once($PATH_WRT_ROOT."lang/lang.en.php");
	echo $i_general_no_access_right	;
	exit;
}

/*
// [removed] NoticeID related issue no longer exist
if ($ReportCardCustomSchoolName=="ucc_ke" && $NoticeID < 40000) {
	$NoticeID = $NoticeID + 65536;
}
*/

//intranet_auth();
intranet_opendb();

$lform = new libform();
$lnotice = new libnotice($NoticeID);
$lpayment = new libpayment();

# get user id according to session id
//$current_user_id;
//$sql = "SELECT userid FROM INTRANET_USER where SessionKey=";
$lu = new libuser($CurID);
//debug_pr($lu);

$deadlineTimeStamp = "";

$is_payment_notice = strtoupper($lnotice->Module) == "PAYMENT";
$can_pay_at_school = $lpayment->Settings['PaymentNoticeCanPayAtSchool'] == 1;
$pay_at_school_display_word = $Lang['eNotice']['PaymentNoticePayAtSchool'];
if($is_payment_notice && $can_pay_at_school){
	$tmp_pay_at_school_display_word = $lpayment->Settings['PaymentNoticePayAtSchoolDisplayWord'.($_SESSION['intranet_session_language']=='en'?'Eng':'Chi')];
	if($tmp_pay_at_school_display_word != ''){
		$pay_at_school_display_word = $tmp_pay_at_school_display_word;
	}
}

$notice_merchant_sp = array();
if($is_payment_notice){
	$sql = "SELECT a.* FROM INTRANET_NOTICE as n INNER JOIN PAYMENT_MERCHANT_ACCOUNT as a ON a.AccountID=n.MerchantAccountID WHERE n.NoticeID='$NoticeID' AND a.RecordStatus='1'";
	$merchant_accounts = $lnotice->returnResultSet($sql);
	if (count($merchant_accounts) > 0) {
		$notice_merchant_sp[] = strtoupper($merchant_accounts[0]['ServiceProvider']);
	}
    if($sys_custom['ePayment']['MultiPaymentGateway']) {
        $sql = "SELECT a.* FROM INTRANET_NOTICE_PAYMENT_GATEWAY as n INNER JOIN PAYMENT_MERCHANT_ACCOUNT as a ON a.AccountID=n.MerchantAccountID WHERE n.NoticeID='$NoticeID' AND a.RecordStatus='1'";
        $merchant_accounts = $lnotice->returnResultSet($sql);
        foreach($merchant_accounts as $temp) {
            $notice_merchant_sp[] = $temp['ServiceProvider'];
        }
    }
	$notice_merchant_sp = array_unique($notice_merchant_sp);
}

$valid = true;
$IsCurNoticeTarget = true;

// [2020-0722-1649-52073] if student > set role = "s"
if($lu->isStudent()) {
    $role = "s";
}

if($role == "s")
{
	if ($StudentID != "")
	{
		$isStudent = true;
		
		// [2015-0316-1603-27206] check if eNotice after deadline or not
		$today = time();
		$deadline = strtotime($lnotice->DateEnd);
		
		// [2016-0907-1616-07236] direct compare timestamp
		//$afterDeadline = compareDate($today,$deadline)>0;
		$afterDeadline = ($today>$deadline);
		
        if ($afterDeadline) {
        	// after deadline
            if ($lnotice->isLateSignAllow) {
            	$valid = true;
            	$editAllowed = true;
            }
            else {
            	$valid = false;
            	$editAllowed = false;
            }
            
            // [2016-0907-1616-07236] not allow late sign for payment notice
	    	if($is_payment_notice)
	    	{
	    		$editAllowed = false;    
	    	}
        }
        else {
        	// before deadline
            $editAllowed = true;
            $deadlineTimeStamp = (!$lnotice->isLateSignAllow || $is_payment_notice)? $deadline : "";
        }
	}
	else
	{
	     $editAllowed = false;
	}
	
	if ($editAllowed)
	{
	    if ($StudentID == "")
	    {
	        $valid = false;
	    }
	    
	    if (($lnotice->TargetType=="S" && !$lu->isStudent()) || ($lnotice->TargetType=="P" && !$lu->isParent()))
	    {
	    	$editAllowed = false;
	    }
	}

	// [2020-0721-1316-45073] check if user is target of current notice
	$IsCurNoticeTarget = $lnotice->isStudentInNotice($NoticeID, $CurID);
}
else
{
	if ($lu->isStudent())
	{
	    $valid = false;
	}

	//$isTeacher = $lu->isTeacherStaff();
	//$isParent = $lu->isParent();
	$isTeacher = false;
	$isParent = true;
	
	if ($StudentID != "")
	{
		// [2015-0316-1603-27206] check if eNotice after deadline or not
		$today = time();
		$deadline = strtotime($lnotice->DateEnd);
		
		// [2016-0907-1616-07236] direct compare timestamp
		//$afterDeadline = compareDate($today,$deadline)>0;
		$afterDeadline = ($today>$deadline);
		
	    if ($isParent)
	    {
	        $children = $lu->getChildren();
	        if (!in_array($StudentID,$children)) {
	        	// no such children
	             $valid = false;
	        }
	        else {
	            //$today = time();
	            //$deadline = strtotime($lnotice->DateEnd);
	            //if (compareDate($today,$deadline)>0) {
	            if ($afterDeadline) {
	            	// after deadline
	                if ($lnotice->isLateSignAllow) {
		            	$valid = true;
		            	$editAllowed = true;
		            }
		            else {
	                	$valid = false;
	                	$editAllowed = false;
	                }
					
		            // [2016-0907-1616-07236] not allow late sign for payment notice
			    	if($is_payment_notice)
			    	{
			    		$editAllowed = false;    
			    	}
	            }
	            else {
	            	// before deadline
	                $editAllowed = true;
           			$deadlineTimeStamp = (!$lnotice->isLateSignAllow || $is_payment_notice)? $deadline : "";
	            }
	        }
	    }
	    else # Teacher
	    {
	    	if($is_payment_notice)
	    	{
	    		$editAllowed = false;    
	    	}
	    	else
	    	{
			    if($lnotice->IsModule)
			    {
					$editAllowed = true;    
			    }
			    else
			    {
		        	$editAllowed = $lnotice->isEditAllowed($StudentID);
		        	// [2015-0323-1602-46073] Allow PIC to sign if user is eNotice PIC and Settings - "Allow PIC to edit replies for parents." - true
		        	if($StudentID!='undefined' && $lnotice->isNoticePIC($NoticeID) && $lnotice->isPICAllowReply) {
		        		$editAllowed = true;
		        	}
		    	}
		    }
	    }
	}
	else
	{
	    if ($isParent)
	    {
	        $valid = false;
	    }
	    else
	    {
	        $editAllowed = false;
	    }
	}
	
	if ($editAllowed)
	{
	    if ($StudentID == "")
	    {
	        $valid = false;
	    }
	    
	    if (($lnotice->TargetType=="S" && !$lu->isStudent()) || ($lnotice->TargetType=="P" && !$lu->isParent()))
	    {
	    	$editAllowed = false;
	    }
	}

    // [2020-0721-1316-45073] check if children are targets of current notice
    if ($lu->isParent())
    {
        $childrenList = $lu->getChildren();
        $IsCurNoticeTarget = false;

        if ($StudentID != "" && in_array($StudentID, (array)$childrenList)) {
            $IsCurNoticeTarget = $lnotice->isStudentInNotice($NoticeID, $StudentID);
        }
    }
}

if($sys_custom['eNotice']['SubmitNewReplyMulitpleTimes'] && !$is_payment_notice) {
	// [2017-0206-1342-27225] not retrieve reply details => display empty reply slip for mulitple submit
	// do nothing
}
else {
	$lnotice->retrieveReply($StudentID);
}

# 2010-06-02 check allow re-sign the notice or not
//if($lnotice->signerID && $lnotice->NotAllowReSign && !$lnotice->hasFullRight())
// [2015-0316-1603-27206] not allow user to resign enotice after deadline
// 						  not allow user to resign payment notice
// [2015-0316-1603-27206] only parent accounts affected, teachers (with edit right) can also resign after deadline
//if($lnotice->signerID && ($lnotice->NotAllowReSign || (!$lnotice->NotAllowReSign && $afterDeadline) || strtoupper($lnotice->Module) == "PAYMENT") && !$lnotice->hasFullRight())

//And not allow student to resign notice is NotAllowReSign setting is true

// [2020-0604-1821-16170]
//if($lnotice->signerID && ($lnotice->NotAllowReSign || (!$lnotice->NotAllowReSign && $afterDeadline) || strtoupper($lnotice->Module) == "PAYMENT") && $isParent && !$lnotice->hasFullRight())
//if($lnotice->signerID && ($lnotice->NotAllowReSign || (!$lnotice->NotAllowReSign && $afterDeadline) || $is_payment_notice) && ($isParent || $lu->isStudent()) && !$lnotice->hasFullRight())
$hasNoticeFullRight = $is_payment_notice ? $lnotice->hasPaymentNoticeFullRight() : $lnotice->hasSchoolNoticeFullRight();
if($lnotice->signerID && ($lnotice->NotAllowReSign || (!$lnotice->NotAllowReSign && $afterDeadline) || $is_payment_notice) && ($isParent || $lu->isStudent()) && !$hasNoticeFullRight)
{
    $editAllowed = false;
}


$use_tng = $lpayment->isTNGDirectPayEnabled() && (in_array('TNG', $notice_merchant_sp));
$use_alipay = $lpayment->isAlipayDirectPayEnabled() && in_array('ALIPAY', $notice_merchant_sp);
$use_hsbc_fps = $lpayment->isFPSDirectPayEnabled() && in_array('FPS', $notice_merchant_sp);
$use_tapandgo = $lpayment->isTapAndGoDirectPayEnabled() && in_array('TAPANDGO', $notice_merchant_sp);
$use_visamaster = $lpayment->isVisaMasterDirectPayEnabled() && in_array('VISAMASTER', $notice_merchant_sp);
$use_wechat = $lpayment->isWeChatDirectPayEnabled() && in_array('WECHAT', $notice_merchant_sp);
$use_alipaycn = $lpayment->isAlipayCNDirectPayEnabled() && in_array('ALIPAYCN', $notice_merchant_sp);

$use_payment_system = $lpayment->isEWalletDirectPayEnabled();
$use_payment_system_ary = array();
if($use_tng){
	$use_payment_system_ary[] = 'TNG';
}
if($use_alipay){
	$use_payment_system_ary[] = 'AlipayHK';
}
if($use_hsbc_fps){
	$use_payment_system_ary[] = 'FPS';
}
if($use_tapandgo){
	$use_payment_system_ary[] = 'TAPANDGO';
}
if($use_visamaster){
	$use_payment_system_ary[] = 'VISAMASTER';
}
if($use_wechat){
	$use_payment_system_ary[] = 'WECHAT';
}
if($use_alipaycn){
	$use_payment_system_ary[] = 'ALIPAYCN';
}
/*
$use_alipay_topup = $lpayment->isAlipayTopUpEnabled();
$use_tng_topup = $lpayment->isTNGTopUpEnabled();
$use_hsbc_fps_topup = $lpayment->isFPSTopUpEnabled();
$use_tapandgo_topup = $lpayment->isTapAndGoTopUpEnabled();
*/
$use_topup = $lpayment->isEWalletTopUpEnabled();
$do_not_check_balance = $sys_custom['PaymentNoticeDoNotCheckBalance'] || $use_payment_system;

if($use_payment_system && $isParent && $lnotice->signerID && $lnotice->replyStatus != '2'){
	$tng_payment_under_processing = true;
}
// if above conditions evaluated to not allow to sign again, it has paid by payment system such as TNG, Alipay before but failed due to many reasons such as balance not enough, make it allow to sign again
if(!$editAllowed && $tng_payment_under_processing && !$afterDeadline){
	$editAllowed = true;
}

$showAuthCodeRemarks = false;
if($role=="s") {
	// do nothing
}
else {
	if ($sys_custom['eClassApp']['authCode']) {
		$authCodeValid = true;
		if ($authCode=='') {
			$authCodeValid = false;
		}
		else {
			include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp_authCode.php');
			$lauthCode = new libeClassApp_authCode();
			$authCodeAry = $lauthCode->getAuthCodeData($CurID, $useStatus=1, $authCode, '', $eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['active']);
			if (count($authCodeAry) == 0) {
				$authCodeValid = false;
			}
		}
		
		if (!$authCodeValid) {
			$editAllowed = false;
			$showAuthCodeRemarks = true;
			$authCode = '';
		}
	}
}

//$lnotice->retrieveReply($StudentID);
$temp_ans =  str_replace("&amp;", "&", $lnotice->answer);
$ansString = $lform->getConvertedString($temp_ans);
$ansString.=trim($ansString)." ";

/*
if (!$valid)
{
    header("Location: view.php?StudentID=$StudentID&NoticeID=$NoticeID");
    exit();
}
*/

$targetType = array("",$i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual);

# modified by Kelvin Ho 2008-11-12 for ucc
if($lnotice->Module=='')
{
    // $attachment = $lnotice->displayAttachment($targetBlank=true);
    $attachment = $lnotice->displayAttachment_showImage($dl_auth_token);
}
else
{
	if($is_payment_notice) {
	    $attachment = $lnotice->displayAttachment_showImage($dl_auth_token);
	} else {
		$attachment = $lnotice->displayModuleAttachment();
	}
}

// [2015-0416-1040-06164] use getNoticeContent() to get display content
// $this_Description = $lnotice->Description;
$this_Description = $lnotice->getNoticeContent($StudentID);
	
$MODULE_OBJ['title'] = $i_Notice_ElectronicNotice2;
$linterface = new interface_html("popup6.html");
$linterface->LAYOUT_START();

if ($isStudent) {
	$targetUserId = $StudentID;
	$appType = $eclassAppConfig['appType']['Student'];
}
else if ($isTeacher) {
	$targetUserId = $CurID;
	$appType = $eclassAppConfig['appType']['Teacher'];
}
else if ($isParent) {
	$targetUserId = $StudentID;
	$appType = $eclassAppConfig['appType']['Parent'];
}

$leClassApp = new libeClassApp();
$appAccessRightAry = $leClassApp->getUserModuleAccessRight($targetUserId, $appType);
$appCanAccessNotice = $appAccessRightAry[$eclassAppConfig['moduleCode']['eNotice']]['RecordStatus'];
//debug_pr($NoticeID);
// [2018-0322-1718-26276] Need to check both soft and hard delete
//if ($lnotice->NoticeID == '') {
if ($lnotice->NoticeID == '' || $lnotice->isDeletedNotice == 1) {
	$warningMsg = $Lang['eNotice']['NoticeDeleted'];
	
	echo '<div style="padding:5px;">'.$warningMsg.'</div>';
	die();
}
else if (date('Y-m-d H:i:s') < $lnotice->DateStart) {
	$warningMsg = $Lang['eNotice']['NoticeNotIssuedYet'];
	$warningMsg = str_replace('<!--startDate-->', $lnotice->DateStart, $warningMsg);
	
	echo '<div style="padding:5px;">'.$warningMsg.'</div>';
	die();
}
// [2018-1205-1037-34207] for issued notice only
//else if ($lnotice->RecordStatus == 2) {
else if ($lnotice->RecordStatus != 1) {
	$warningMsg = $Lang['eNotice']['NoticeSuspended'];
	
	echo '<div style="padding:5px;">'.$warningMsg.'</div>';
	die();
}
else if (!$appCanAccessNotice) {
	$warningMsg = $Lang['eNotice']['AppNotEnabledNotice'];
	
	echo '<div style="padding:5px;">'.$warningMsg.'</div>';
	die();
}
// [2020-0721-1316-45073]
else if (!$IsCurNoticeTarget) {
    $warningMsg = $Lang['eNotice']['NotCurrentNoticeTarget'];

    echo '<div style="padding:5px;">'.$warningMsg.'</div>';
    die();
}

# Store token to session
// if($special_feature['download_attachment_add_auth_checking'] && $dl_auth_token != '')
// {
//     $_SESSION['dlToken'] = $dl_auth_token;
// }

$reqFillAllFields = $lnotice->AllFieldsReq;
// 2014-1008-1215-48071 if it is payment notice, always require fill all fields
if ($is_payment_notice) {
	$reqFillAllFields = true;
}
$DisplayQNum = $lnotice->DisplayQuestionNumber;
?>

<!--<style type="text/css">
.tabletext77, .tabletext277, body77, .tablelink77 {
	font-size: 35px;
}
.eNoticereplytitle77, .formbutton_v3077 {
	font-size: 38px;
}
.formbutton_v3077{ 
	height:54px;
}
.tablelink77:hover {
	font-size: 35px;
}
.tabletext2 {
	font-size: 18px;
}
</style>-->
<SCRIPT LANGUAGE=Javascript>
jQuery(document).ready(function($) {
    <?php
	if($_SESSION['ENOTICE_PAYMENT_ERROR_RETURN'] != '') {

        if(isset($Lang['ePayment']['PaymentFailOrCanceled_'.$_SESSION['ENOTICE_PAYMENT_ERROR_RETURN']])) {
            echo "alert('".$Lang['ePayment']['PaymentFailOrCanceled_'.$_SESSION['ENOTICE_PAYMENT_ERROR_RETURN']]."');";
		} else {
			echo "alert('".$Lang['ePayment']['PaymentFailOrCanceled_other']." [".$_SESSION['ENOTICE_PAYMENT_ERROR_RETURN']."]');";
		}
	$_SESSION['ENOTICE_PAYMENT_ERROR_RETURN'] = '';
	}
    ?>


   textboxes = $("input:text");

	if ($.browser.mozilla) {
		$(textboxes).keypress(checkForEnter);
	} else {
		$(textboxes).keydown(checkForEnter);
	}

	function checkForEnter(event) {
		if (event.keyCode == 13) {
			currentTextboxNumber = textboxes.index(this);
	
			if (textboxes[currentTextboxNumber + 1] != null) {
				nextTextbox = textboxes[currentTextboxNumber + 1];
				nextTextbox.select();
			}
	
			event.preventDefault();
			return false;
		}
	}
	
	$('div#descriptionDiv').css('width', Get_Screen_Width() - 10);
	$('div#replyslipdiv').css('width', Get_Screen_Width() - 12);

	//calculateAmountTotal();
	
	window.onresize = function(){
		$('div#descriptionDiv').css('width', Get_Screen_Width() - 10);
		$('div#replyslipdiv').css('width', Get_Screen_Width() - 12);
	};
	
<?php if($is_payment_notice) { ?>	
	if($('input[name="NoticePaymentMethod_"]').length > 0 && $('input[name="NoticePaymentMethod_"]:checked').length==0){
		$('input[name="NoticePaymentMethod_"]:first').attr('checked',true);
	}
	if($('input[name="NoticePaymentMethod_"]').length > 0 && $('input[name="NoticePaymentMethod_"]:checked').length > 0 && $('input[name="NoticePaymentMethod"]').length>0){
		$('input[name="NoticePaymentMethod"]').val($('input[name="NoticePaymentMethod_"]:checked').val());
	}
<?php } ?>
});

var need2checkform = <? if($reqFillAllFields){ echo "true"; } else { echo "false"; }  ?>;

<? if($lnotice->Module=='') { ?>
var formAllFilled  = true;

function recurReplace(exp, reby, txt) {
    while (txt.search(exp)!=-1) {
        txt = txt.replace(exp, reby);
    }
	return txt;
}
<? } else { ?>
var need2checkformSQ = false;
<? } ?>

function SubmitForm(obj)
{
	var obj = document.getElementById('form1');
	
	if (document.getElementById('user_password') != null && typeof(document.getElementById('user_password'))!="undefined")
	{
		$.ajax({
			type: "POST",
		    url: 'ajax_authenticate.php',
		    data: { user_password: document.getElementById('user_password').value, user_id: document.getElementById('CurID').value },
		    error: function(xhr) {
		      alert('Ajax request error' + xhr);
		    },
		    success: function(response) {
		    //alert(response);
		      if ( response=="1")
		      {
		      	if (checkform(document.form1))
		      	{
		      		$('input#submit2').attr('disabled', true);
		      		//$('div#submitBtnDiv').html(submittingDivContent);
		      		$('div#submitBtnDiv').hide();
		      		$('div#submittingDiv').show();
		      		
		      		document.form1.submit();
		      	}
		      } else
		      {
		      	alert("<?=$Lang['eClassAPI']['WarningMsg']['InvalidPassword']?>");
		      	document.getElementById('user_password').focus();
		      }
		    }
	  	});
	} else
	{
		if (checkform(document.form1))
      	{
      		$('input#submit2').attr('disabled', true);
      		//$('div#submitBtnDiv').html(submittingDivContent);
      		$('div#submitBtnDiv').hide();
      		$('div#submittingDiv').show();
      		document.form1.submit();
      	}
	}
  
}

function checkform(obj)
{
	<? if ($deadlineTimeStamp != "") { ?>
		var currentTimeStamp = Math.floor(Date.now() / 1000);
		if(currentTimeStamp > <?=$deadlineTimeStamp?>) {
			alert("<?=$Lang['Notice']['NoticeHasExpired']?>");
			if($('#submitBtnDiv')) {
				$('#submitBtnDiv').hide();
			}
			if($('#NoticeSignInstruction')) {
				$('#NoticeSignInstruction').hide();
			}
	      	return false;
		}
	<? } ?>
	
	<? if($lnotice->Module=='') { ?>
		// Form Checking
		if (formAllFilled)
		{
			canSubmit = true;
		}
		else
		{
		      alert("<?=$Lang['Notice']['AnswerSpecificQuestions']?>");
		      return false;
		}
	//	else
	//	{
	//	      alert("<?=$Lang['Notice']['AnswerAllQuestions']?>");
	//	      return false;
	//	}
		
	<? } else { ?>	
		
	var form_all_filled = true;
<?php if($is_payment_notice) { ?>
	var formAllFilled = sheet.validateFillInForm();
<?php } ?>	
	if (typeof(formAllFilled)!="undefined")
	{
		var form_all_filled = formAllFilled;
	}
	
	<? //if (strtoupper($lnotice->Module) == "PAYMENT") { ?>
		
	<? //} else { ?>
		//if (!need2checkform || form_all_filled)
		if ((!need2checkform && !need2checkformSQ) || form_all_filled)
		 {
		      //return true;
		 }
		 else if(!need2checkform && need2checkformSQ)
		 {
		      alert("<?=$Lang['Notice']['AnswerSpecificQuestions']?>");
		      return false;
		 }
		 else
		 {
		      alert("<?=$Lang['Notice']['AnswerAllQuestions']?>");
		      return false;
		 }
	<? //} ?>
	
	<? if($lnotice->DebitMethod==1) { ?>
//	var AccountBalance = document.getElementById('amountBalance').value.replace(',','');
//	
//	if(parseInt(document.getElementById('thisAmountTotal').value) > parseInt(AccountBalance)) {
//		alert("<?=$Lang['eNotice']['NotEnoughBalance']?>");  
//		return false;
//	} else {
	<? } ?>
//         if (!confirm('<?=$i_Notice_Alert_Sign?>'))
//         {
//             return false;
//             
//         }else{
//	         <? if($is_payment_notice) { ?>
//					document.form1.action = "eNoticePayment_sign_update.php";
//				<? } else { ?>
//					document.form1.action = "sign_update.php";
//				<? } ?>
//			 
//			 //document.form1.target = "_self";
//			 return true;
//		 }
	<? if($lnotice->DebitMethod==1) { ?>
//	}
	<? } ?>
	
	var isPaymentNotice = <?= ($is_payment_notice)? 1 : 0 ?>; 
	var debitMethod = "<?=$lnotice->DebitMethod?>";
	var canSubmit = true;
	<?php if(!$do_not_check_balance){ // if not pay by payment system TNG/Alipay, need to check balance ?>
	if (isPaymentNotice) {
		var AccountBalance = document.getElementById('amountBalance').value.replace(',','');
		var payment_method_obj = $('input[name="NoticePaymentMethod_"]:checked');
		if(payment_method_obj.length > 0 && $('#NoticePaymentMethod').length > 0){
			$('#NoticePaymentMethod').val(payment_method_obj.val());
		}
		if(payment_method_obj && payment_method_obj.length > 0 && payment_method_obj.val() == 'PayAtSchool'){ // no need to check balance
			canSubmit = true;
		}else if (parseInt(document.getElementById('thisAmountTotal').value) > parseInt(AccountBalance)) {
			// not enough money
			alert("<?=$Lang['eNotice']['NotEnoughBalance']?>");
			
			if (debitMethod == 1) {
				// debit directly => cannot submit
				canSubmit = false;
			}
			else if (debitMethod == 2) {
				// debit later => warning only, can submit
			}
		}
	}
	<?php } // if not TNG ?>
	<? } ?>
	
	if (canSubmit) {
		if (!confirm('<?=$i_Notice_Alert_Sign?>')) {
			return false;
		} else {
			return submitFormReal();
		 }
	}
}

function submitFormReal(forceSubmit){
	var isPaymentNotice = <?= ($is_payment_notice)? 1 : 0 ?>; 
	if (isPaymentNotice) {
		document.form1.action = "eNoticePayment_sign_update.php";
	} else {
		document.form1.action = "sign_update.php";
	}
	
	if(forceSubmit == true){
		document.form1.submit();
		return false;
	}
	
	return true;
}

function click_print()
{
	with(document.form1)
	{
        	action = "eNoticePayment_view_print_preview.php";
        	//target = "_blank";
            document.form1.submit();
	}        
}

//function updateAmountTotal(id, amt, type) {
//	if(type!=3) { // not checkbox option 
//		document.getElementById('thisAmountTotal').value = parseInt(document.getElementById('thisAmountTotal').value) + parseInt(amt) - parseInt(document.getElementById('thisItemAmount'+id).value);
//		document.getElementById('thisItemAmount'+id).value = amt;
//	} else {
//		document.getElementById('thisAmountTotal').value = parseInt(document.getElementById('thisAmountTotal').value) + parseInt(amt);
//		document.getElementById('thisItemAmount'+id).value = parseInt(document.getElementById('thisItemAmount'+id).value) + parseInt(eval(amt));
//	}
//	//document.getElementById('spanTemp').innerHTML = document.getElementById('thisAmountTotal').value;
//}

function calculateAmountTotal()
{
	var selected_rbs = $(document.answersheet).find('input[type=radio]:checked');
	var selected_cbs = $(document.answersheet).find('input[type=checkbox]:checked');
	var total_to_pay = 0;
	for(var i=0;i<selected_rbs.length;i++){
		var data_amount = $(selected_rbs[i]).attr('data-amount');
		if(data_amount){
			//total_to_pay += parseInt(data_amount);
			total_to_pay += parseFloat(data_amount);
		}
	}
	for(var i=0;i<selected_cbs.length;i++){
		var data_amount = $(selected_cbs[i]).attr('data-amount');
		if(data_amount){
			//total_to_pay += parseInt(data_amount);
			total_to_pay += parseFloat(data_amount);
		}
	}
	var container = $('#TotalAmountToPay');
	if(container.length > 0){
		//container.html(total_to_pay);
		container.html(total_to_pay.toFixed(2));
	}
}

function updateAmountTotal(id, oldAmt, amt, type) {

	if(isNaN(oldAmt)) oldAmt = 0;
//	alert(oldAmt+'//'+amt);
	
	if(type!=3) { // not checkbox option ("3" is checkbox options)
		
		document.getElementById('thisAmountTotal').value = parseInt(document.getElementById('thisAmountTotal').value) + parseInt(amt) - oldAmt;
		document.getElementById('thisItemAmount'+id).value = amt;

	} else {	// checkbox option

		document.getElementById('thisAmountTotal').value = parseInt(document.getElementById('thisAmountTotal').value) + parseInt(amt);
		document.getElementById('thisItemAmount'+id).value = parseInt(document.getElementById('thisItemAmount'+id).value) + parseInt(eval(amt));

	}
	
	//document.getElementById('spanTemp').innerHTML = document.getElementById('thisAmountTotal').value;
	
	calculateAmountTotal();
}
</SCRIPT>
<style type="text/css">
.tabletext {
	font-size: 16px;
}
.tabletext2 {
	font-size: 16px;
}
.tabletextremark {
	font-size: 16px;
}
#rcorners_green {
    border-radius: 5px;
    background: #74DF00;
    padding: 1px; 
    width: 200px;
    height: 150px; 
} 

#rcorners_red {
    border-radius: 5px;
    background: #F78181;
    padding: 1px; 
    width: 200px;
    height: 150px; 
}

#popupConfirmDialog-screen{ /* For Windows Phone */
	opacity: 0;
}

tr.disabled_q, tr.disabled_q td, tr.disabled_q td.tabletext, tr.disabled_q td span.tabletextrequire {
	color: gray;
}

</style>
<div id="spanTemp"></div>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">

<script>
$(function(){
    $('div#descriptionDiv a.ui-link').filter(function() {
        return ($(this).attr('href').indexOf('https://') != -1 || $(this).attr('href').indexOf('#') != -1);
    })
    .attr('rel', 'external');
});
</script>

<!-- -------- For windows phone START -------- -->
<script>
$(function(){
	if(typeof(window.confirm) == 'undefined'){ // Windows Phone does not have confirm() in webview =_=
		function confirmDialog(content, title, callback) {
			title = title || '';
			var btnStr = '';
			if(typeof(callback) != 'undefined'){
				btnStr = '<a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-btn-b optionConfirm" data-rel="back">ok</a>\
                <a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-btn-b optionCancel" data-rel="back">cancel</a>';
			}else{
				btnStr = '<a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-btn-b optionCancel" data-rel="back">ok</a>';
			}
			
			var popupDialogId = 'popupConfirmDialog';
			$('<div data-role="popup" id="' + popupDialogId + '" data-confirmed="no" data-theme="a" data-overlayTheme="b" data-dismissible="false" style="max-width:500px;"> \
			                    <div data-role="header">\
			                        <h1>' + title + '</h1>\
			                    </div>\
			                    <div role="main" class="ui-content">\
			                        <h3 class="ui-title">' + content + '</h3>\
			                        ' + btnStr + '\
			                    </div>\
			                </div>')
			    .appendTo($.mobile.pageContainer);
			var popupDialogObj = $('#' + popupDialogId);
			popupDialogObj.trigger('create');
			popupDialogObj.popup({
			    afterclose: function (event, ui) {
			        popupDialogObj.find(".optionConfirm").first().off('click');
			        var isConfirmed = popupDialogObj.attr('data-confirmed') === 'ok' ? true : false;
			        $(event.target).remove();
			        if (isConfirmed && typeof(callback) != 'undefined') {
			            callback(true);
			        }
			    }
			});
			popupDialogObj.popup('open');
			popupDialogObj.find(".optionConfirm").first().on('click', function () {
			    popupDialogObj.attr('data-confirmed', 'ok');
			});
		}
		window.confirm = function(msg){
			confirmDialog(msg,'', submitFormReal);
		};
	
		window.alert = function(msg){
			confirmDialog(msg,'');
		};
	}
});
</script>
<!-- -------- For windows phone END -------- -->

<div data-role="page" id="pageone">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<? /* if ($editAllowed) { */
			        if ($lnotice->replyStatus != 2) {
			            $statusString = "$i_Notice_Unsigned";
			        }
			        else {
			            $signer = $lu->getNameWithClassNumber($lnotice->signerID);
			            $signby = ($lnotice->replyType==1? $i_Notice_Signer:$i_Notice_Editor);
			            $statusString = "$i_Notice_Signed <I>($signby $signer $i_Notice_At ".$lnotice->signedTime.")</I>";
			        }
				?>
			    <tr valign='top'>					
                    <td bgcolor='#FFFFFF' style="font-size:18px; padding:10px;">
						<span class='tabletext' style="font-size:14px;"><?=$lnotice->NoticeNumber?></span>
				    <br>
					<span class='tabletext' style="font-size:22px;"><strong><?=$lnotice->Title?></strong></span>
					<?php 
					if(!$sys_custom['eClassApp']['eNoticeHideReplySlipSignStatus']) {
						if($lnotice->replyStatus != 2) { ?>
							<span id="rcorners_red" style="color:white;text-shadow: 0px 0px;font-size:16px;vertical-align:text-top;"><?=$i_Notice_Unsigned?></span>						
					<?php }
						else { ?>
							<span id="rcorners_green" style="color:white;text-shadow: 0px 0px;font-size:16px;vertical-align:text-top;"><?=$i_Notice_Signed?></span>
					<?php }
					} 
					?>
					</td>												
				</tr>
				<tr valign='top'>
				    <td bgcolor='#FFFFFF' style="font-size:16px; padding:10px;">
				    <?php if($lnotice->replyStatus != 2){?>
				        <span><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_deadline.png" align="absmiddle" style="width:20px; height:20px;" /><span style="vertical-align:middle;"><font color="red"><?=$i_Notice_DateEnd?>: <?=$lnotice->DateEnd?></font></span></span>				        
				  	<?php }else{?>
				        <span><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_deadline_expired.png" align="absmiddle" style="width:20px; height:20px;" /><span style="vertical-align:middle;"><font color="#b1b1b1"><?=$i_Notice_DateEnd?>: <?=$lnotice->DateEnd?></font></span></span>				        
				  	<?php }?>				  		
				    </td>
				</tr>
				<tr valign='top'>
				    <td bgcolor='#FFFFFF' style="font-size:16px; padding:10px;">
				     	<div id="descriptionDiv" style="width: 100%; overflow-x: auto; overflow-y: hidden;">
				        <span  style="font-size:16px;"><?=str_replace("&nbsp;&nbsp;", "&nbsp; ", htmlspecialchars_decode($this_Description))?></span>
				        </div>
				    </td>
				</tr>
				<? if ($attachment != "") { ?>
				     <tr valign='top'>
				     	<td bgcolor="#FFFFFF" style="font-size:16px; padding:10px; word-break:break-all;">
				            <span><?=$attachment?></span>
				        </td>
				     </tr>
				<? } ?>
			</table>
		</td>
	</tr>
</table>

<? if ($sys_custom['eClassApp']['eNoticeHideOtherInfoDisplay']) {
	// do not show collapsible button
}
else { ?>
<div data-role="collapsible" data-collapsed="true" style="font-size:18px">
   <h1><?=$Lang['General']['Misc']?></h1>
   <div class="ui-grid-a">
		<div class="ui-block-a" style="width:100%;">
			<br>
			<span><strong><?=$i_Notice_DateStart?></strong></span>
			<br>
			<span><?=$lnotice->DateStart?></span>
		</div>
		<? if($is_payment_notice && !$use_payment_system) { ?>
			<div class="ui-block-a" style="width:100%;">
				<br>
		       	<span><strong><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['Title']?></strong></span>
				<br>
			<? if($lnotice->DebitMethod == 1){ ?>
				<span><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['DebitDirectly']?></span>
			<? } else { ?>
				<span><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['DebitLLater']?></span>
			<? } ?>
			</div>
		<? } ?>
		 
		<? if ($isTeacher) {
			$issuer = $lnotice->returnIssuerName();
		?>
		<div class="ui-block-a" style="width:100%;">
			<br>
			<span><strong><?=$i_Notice_Issuer?></strong></span>
			<br>
			<span><?=$issuer?></span>
		</div>
		<? } ?>
		
		<div class="ui-block-a" style="width:100%;">
			<br>
			<span><strong><?=$i_UserStudentName?></strong></span>
			<br>
			<span><?=$lu->getNameWithClassNumber($StudentID)?></span>
		</div>
		 
		<div class="ui-block-a" style="width:100%;">
			<br>
			<span><strong><?=$i_Notice_RecipientType?></strong></span>
			<br>
			<span><?=$targetType[$lnotice->RecordType]?></span>
		</div>
	</div>
	<br>
</div>
<? } ?>

<? 
### check include reply slip or not
$hasContent = (trim($lnotice->Question) || trim($lnotice->ReplySlipContent)) ? true : false;
// [2017-1009-1757-54256] Hide Reply Slip after Notice Deadline
if($sys_custom['eClassApp']['eNoticeHideReplySlipAfterDeadline'] && $afterDeadline && !$editAllowed) {
	$hasContent = false;
}

// [2020-0924-1538-17206]
$isReplySlipTextOnly = true;
?>

<? if($hasContent) { ?>
<!-- Break -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/scissors_line.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/icons_scissors.gif"></td>
	</tr>
</table>
<? } ?>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
            	<tr> 
            		<td>                              
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
        					<? if($hasContent) { ?>
                                <!-- Relpy Slip -->
                                <tr valign='top'>
									<td  class='eNoticereplytitle' align='center'><span class='tabletext2'><?=$i_Notice_ReplySlip?></td>
								</tr>

								<? if($lnotice->ReplySlipContent) {?>
									<!-- Content Base Start //-->
									<tr valign='top'>
										<td  >
										<?=$lnotice->ReplySlipContent?>
										</td>
									</tr>
								<? } else {
										if($lnotice->Module=='')
										{
											// Question and Answer
											$queString = str_replace("&amp;", "&", $lnotice->Question);
											$queString = preg_replace('(\r\n|\n)', "<br>", $queString);
											$queString = trim($queString)." ";
											$qAry = $lnotice->splitQuestion($queString, true);
											$aAry = $lnotice->parseAnswerStr($ansString);

											// Convert Question String for display
											$queString = $lform->getConvertedString($queString);
											
											// init array for js checking
											$optionArray = array();
											$optionArray[0] = array();
											$optionArray[1] = array();
											$mustSubmitArray = array();
                                            $isSkipQuestionArray = array();

											// loop Questions
											for($i=0; $i<count($qAry); $i++)
											{
												$currentQuestion = $qAry[$i];
												$mustSubmitArray[] = $reqFillAllFields || $currentQuestion[3];
												$optionArray[0][] = $currentQuestion[4][0];
												$optionArray[1][] = $currentQuestion[4][1];
                                                $isSkipQuestionArray[] = $mustSubmitArray[$i] && $optionArray[1][$i] != '' && ($optionArray[0][$i] != $optionArray[1][$i]);
												
												// for MC Questions
                                                // [2020-0915-1022-58066]
                                                //if($currentQuestion[0]==2 && $mustSubmitArray[$i])
												if($currentQuestion[0]==2)
												{
													$optionCount = count((array)$currentQuestion[4]);
													for($j=2; $j<$optionCount; $j++)
													{
														$optionArray[$j][$i] = $currentQuestion[4][$j];
													}

                                                    // [2020-0915-1022-58066] MC questions that required to be answered
                                                    $isSkipQuestionArray[$i] = false;
                                                    if($mustSubmitArray[$i])
                                                    {
                                                        if(count(array_unique($currentQuestion[4])) > 1) {
                                                            $mustSubmitArray[$i] = $optionCount;
                                                            $isSkipQuestionArray[$i] = true;
                                                        }
                                                    }
												}

												// [2020-0924-1538-17206] check if all questions are 'Not Applicable'
                                                if ($isReplySlipTextOnly) {
                                                    $isReplySlipTextOnly = $currentQuestion[0] == 6;
                                                }
											}

                                            // [2020-0826-1451-05073] use JS to handle Question Relation
											// $qAryRelation = $lnotice->handleQuestionRelation ($mustSubmitArray, $optionArray);
								?>
										<!-- Question Base Start //-->
										<tr valign='top'>
											<td colspan="2">
					                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
					                        <tr><td>
					                                <form name="ansForm" method="post" action="update.php">
					                                	<table width='100%' border='0' cellspacing='0' cellpadding='5' align='center'>
														<tr><td>
															<?= $lnotice->returnReplySlipContent("sign", $qAry, $aAry, $reqFillAllFields, $DisplayQNum, 1);?>
														</td></tr>
														</table>
				                                        <input type=hidden name="qStr" value="<?=$queString?>">
				                                        <input type=hidden name="aStr" value="">
				                                	</form>
				                                
					                                <SCRIPT LANGUAGE=javascript>
						                                var mustSubmitArr = new Array("", "<?= implode('","',(array)$mustSubmitArray); ?>");
						                                var option0SkipArr = new Array("", "<?= implode('","',(array)$optionArray[0]); ?>");
														var option1SkipArr = new Array("", "<?= implode('","',(array)$optionArray[1]); ?>");
                                                        var isSkipQuestionArr = new Array("", "<?= implode('","',(array)$isSkipQuestionArray); ?>");
														var relateArr = {};
														var totalQNum = <?=count($qAry)?>;
														<?
														    /*
															foreach($qAryRelation as $qNum => $qOptions)
															{
																echo "relateArr[".$qNum."] = {};\n";
																foreach($qOptions as $oNum => $qr)
																{
																	echo "relateArr[".$qNum."][".$oNum."] = new Array(\"".implode("\",\"",(array)$qr)."\");\n";
																}
															}
															*/

                                                            foreach((array)$optionArray as $oNum => $qOptionArray)
                                                            {
                                                                echo "relateArr[".$oNum."] = {};\n";
                                                                foreach((array)$qOptionArray as $qNum => $targetQ)
                                                                {
                                                                    if($targetQ == '') {
                                                                        $targetQ = $optionArray[0][$qNum];
                                                                    }
                                                                    if($targetQ == '') {
                                                                        $targetQ = $qNum + 1;
                                                                    }
                                                                    echo "relateArr[".$oNum."][".$qNum."] = '$targetQ';\n";
                                                                }
                                                            }
														?>
														var skipCheckingToQ = 0;
				                                		
				                                		function checkAvailableOption(i, opt)
				                                		{
				                                			var i = parseInt(i);
                                                            var targetQ = relateArr[opt][i-1];
                                                            if(targetQ != 'end') {
                                                                targetQ = parseInt(targetQ);
                                                            }
                                                            var originTargetQ = targetQ;
                                                            var isJumpQ = false;
                                                            var preTextOnlyQCount = 0;

                                                            //var opt = parseInt(opt);
                                							/*
				                                			var availableAry = relateArr[i-1];
				                                			if(availableAry==undefined)
				                                				return;
				                                				
				                                			availableAry = availableAry[opt];
				                                			if(availableAry==undefined){
				                                				availableAry = new Array("-1");
				                                			}
				                                			*/
				                                			
				                                			for(var v=i+1; v<=totalQNum; v++){
				                                				var eleObj = eval("document.ansForm.F" + v);
				                                				if (eleObj){
					                                				if (eleObj.length){
															       		switch(eleObj[0].type)
															        	{
															        		// Radio Button
															        	    case "radio":	
													                        case "checkbox":
                                                                                if(v >= (targetQ + 1) && targetQ != 'end')
                                                                                {
								                                					$("input[name='F" + v + "']").checkboxradio('enable');
								                                					//$("#msg_"+v).attr('style', 'display:none');
								                                					$("span#msg_" + v).hide();
								                                					$("tr#row" + v).removeClass("disabled_q");

                                                                                    var isPreJumpQ = isJumpQ;
                                                                                    if((targetQ == originTargetQ || !isPreJumpQ) && v == (targetQ + 1 + preTextOnlyQCount)) {
                                                                                        isJumpQ = isSkipQuestionArr[v];
                                                                                        if(!isJumpQ) {
                                                                                            targetQ = relateArr[0][v-1];
                                                                                            if(targetQ != 'end') {
                                                                                                targetQ = parseInt(targetQ);
                                                                                            }
                                                                                        }
                                                                                    }
								                                				}
								                                				else
                                                                                {
								                                					$("input[name='F" + v + "']").checkboxradio('disable');
								                                					$("input[name='F" + v + "']").attr("checked", false).checkboxradio("refresh");
								                                					//$("#msg_"+v).attr('style', '');
								                                					$("span#msg_" + v).show();
								                                					$("tr#row" + v).addClass("disabled_q");
								                                				}
												                                break;
															        	}
					                                				}
					                                				else
					                                				{
                                                                        if(v >= (targetQ + 1) && targetQ != 'end')
                                                                        {
						                                					$("input[name='F" + v + "'], textarea[name='F" + v + "']").each(function(){
						                                						$(this).textinput('enable');
						                                						//$("#msg_"+v).attr('style', 'display:none');
							                                					$("span#msg_" + v).hide();
							                                					$("tr#row" + v).removeClass("disabled_q");
																			});

                                                                            var isPreJumpQ = isJumpQ;
                                                                            if((targetQ == originTargetQ || !isPreJumpQ) && v == (targetQ + 1 + preTextOnlyQCount)) {
                                                                                isJumpQ = isSkipQuestionArr[v];
                                                                                if(!isJumpQ) {
                                                                                    targetQ = relateArr[0][v-1];
                                                                                    if(targetQ != 'end') {
                                                                                        targetQ = parseInt(targetQ);
                                                                                    }
                                                                                }
                                                                            }
						                                				}
						                                				else
                                                                        {
						                                					$("input[name='F" + v + "'], textarea[name='F" + v + "']").each(function(){
																			  	$(this).textinput('disable');
						                                						$(this).val('');
						                                						//$("#msg_"+v).attr('style', '');
							                                					$("span#msg_" + v).show();
							                                					$("tr#row" + v).addClass("disabled_q");
																			});
						                                				}
                                                                    }

                                                                    preTextOnlyQCount = 0;
                                                                } else {
                                                                    if(v >= (targetQ + 1) && targetQ != 'end') {
                                                                        preTextOnlyQCount = preTextOnlyQCount + 1;
                                                                    }
				                                				}
				                                			}
				                                		}
				                                		
						                                function getAns(i)
						                                {
						                                	// get target answer
						       								var eleObj = eval("document.ansForm.F"+i);
				        									
				        									// init
													        var strAns = null;
													        var tempAns = null;
															var canSkipChecking = skipCheckingToQ=='end' || skipCheckingToQ > 0 && i <= skipCheckingToQ;
															var mustSubmitQ = mustSubmitArr[i];
															
															// reutn if required answer not exist
													        if (eleObj==undefined)
													        {
													            return '';
													        }
															
													        if (eleObj.length)
													        {
													        	if(eleObj[0].disabled==false)
													        	{
														        	switch(eleObj[0].type)
														        	{
														        		// Radio Button
														        	    case "radio":
														        			// check is T/F Question
														        			var isTFQ = false;
																        	if(document.getElementById("typeOf"+i))
																        	{
																        		isTFQ = document.getElementById("typeOf"+i).value==1;
																        	}
																        	
											                                for (p=0; p<eleObj.length; p++){
										                                        if (eleObj[p].checked)
										                                        {
									                                                tempAns += p;
									                                                
									                                                // Update skipCheckingToQ for T/F Question 
									                                                if(isTFQ && p=="0" && !canSkipChecking){
									                                                	if (typeof option0SkipArr[i] != "undefined")
									                                                	{
									                                                		skipCheckingToQ = option0SkipArr[i];
									                                                	} 
									                                                }
									                                                else if(isTFQ && p=="1" && !canSkipChecking){
									                                                	if (typeof option1SkipArr[i] != "undefined")
									                                                	{
									                                                		skipCheckingToQ = option1SkipArr[i];
									                                                	}
									                                                }
										                                        }
											                                }
											                                
											                                // check empty input
											                                if ((need2checkform || mustSubmitQ>=1) && tempAns == null && !canSkipChecking)
											                                {
											                                    formAllFilled = false;
											                                    return false;
											                                }
																			
											                                if (tempAns != null)
											                                {
											                                    strAns = tempAns;
											                                }
											                                break;
																		
																		// CheckBox
												                        case "checkbox":
											                                for (p=0; p<eleObj.length; p++) {
										                                        if (eleObj[p].checked)
										                                        {
										                                            if (tempAns != null)
										                                            {
										                                                tempAns += ","+p;
										                                            }
										                                            else
										                                            {
										                                                tempAns = p;
										                                            }
										                                        }
											                                }
											                                
											                                // check empty input
											                                if ((need2checkform || mustSubmitQ==1) && tempAns == null && !canSkipChecking)
											                                {
											                                    formAllFilled = false;
											                                    return false;
											                                }
											                                strAns = tempAns;
											                                break;
																		
												                        case "text":
											                                for (p=0; p<eleObj.length; p++){
											                                	tempAns += recurReplace('"', '&quot;', eleObj.value);
											                                }
											                                
															        		// check empty input
											                                if ((need2checkform || mustSubmitQ==1) && (tempAns == null || tempAns=='') && !canSkipChecking)
											                                {
											                                    formAllFilled = false;
											                                    return false;
											                                }
											                                strAns = tempAns;
											                                break;                  	
													                }
													        	}
													        	else
													        	{
													        		if(eleObj[0].type=='text')
													        		{
										                                strAns = '';
													        		}
													        	}
												        	}
												        	// Text Field
												        	else
												        	{
												        		if(eleObj.disabled==false)
												        		{
													                tempAns = recurReplace('"', '&quot;', eleObj.value);
													                
													        		// check empty input
													                if ((need2checkform || mustSubmitQ==1) && (tempAns == null || tempAns=='') && !canSkipChecking)
													                {
													                    formAllFilled = false;
													                    return false;
													                }
													                strAns = tempAns;
													       		}
													       		else{
													       			strAns = '';
													       		}
												        	}
												        	return strAns;
												  		}
													  	
						                                function copyback()
						                                {
							                                // reset parms
						                                	txtStr = '';
						                                	formAllFilled = true;
						                                	skipCheckingToQ = 0;
						                                	
						                                   	// loop table rows
						                                    var tableArr = $("#ContentTable")[0].tBodies[0].rows;
												            for (var x=0; x<tableArr.length; x++)
												            {
												            	// get answers
												            	temp = getAns(x+1);
												            	
												            	// return false if required question not filled
											                    if (formAllFilled==false)
											                    {
											                        return false;
											                    }
											                    
											                    // append answer to txtStr
																txtStr+="#ANS#"+temp;
												           	}
												           	
												           	// update aStr value
													        document.ansForm.aStr.value=txtStr;
							                                document.form1.qStr.value = document.ansForm.qStr.value;
							                                document.form1.aStr.value = document.ansForm.aStr.value;
						                                }
		                                
						                                function updateAllAvailableOption(){
						                               		// get T/F Question that involve skip action
						                                	var possibleTF = $('input[onclick]:checked');
						                                	if(possibleTF.length){
						                                		for(var TFCount=0; TFCount<possibleTF.length; TFCount++)
						                                		{
						                                			var currentTFQ = possibleTF[TFCount]
						                                			var TFQNum = currentTFQ.name.replace("F", "");
						                                			var TFQOption = currentTFQ.value;
						                                			
							                                		// perform to disable fields not related to answered question
																	if(!currentTFQ.disabled){
						                                				checkAvailableOption(TFQNum, TFQOption);
						                                			}
						                                		}
						                                	}
						                                }
						                                
						                               	$(document).ready(function(){
							                               	// update all options
						                               		updateAllAvailableOption();
						                                });
							                  		</SCRIPT>
				                            </td></tr>
				                            </table>
				                    		</td>
										</tr>
										<!-- Question Base End //-->
								<? 	} 
									else 
									{
										$temp_que =  str_replace("&amp;", "&", $lnotice->Question);
										$queString = $lform->getConvertedString($temp_que);
										$queString = trim($queString)." ";
										
										if($is_payment_notice)
										{
											include_once($PATH_WRT_ROOT."includes/eNoticePaymentReplySlip.php");
											$eNoticePaymentReplySlip = new eNoticePaymentReplySlip();
											
											$result = ($lpayment->checkBalance($StudentID,0));
											$account_balance = $lpayment->getExportAmountFormat($result[0]);
											
											$student_subsidy_identity_id = '';
											$student_subsidy_identity_records = $lpayment->getSubsidyIdentityStudents(array('StudentID'=>$StudentID,'StartDateWithinEffectiveDates'=>$lnotice->DateStart,'EndDateWithinEffectiveDates'=>$lnotice->DateEnd));
											if(count($student_subsidy_identity_records)>0){
												$student_subsidy_identity_id = $student_subsidy_identity_records[0]['IdentityID'];
											}
										}

                                        // [2020-0924-1538-17206] check if all questions are 'Not Applicable'
                                        $qAry = $lnotice->splitQuestion($queString, true);
                                        if(!empty($qAry))
                                        {
                                            $notApplicableQType = $is_payment_notice ? '16' : '6';

                                            $qTypeAry = Get_Array_By_Key((array)$qAry, '0');
                                            $qTypeAry = array_diff($qTypeAry, array($notApplicableQType));
                                            if(!empty($qTypeAry)) {
                                                $isReplySlipTextOnly = false;
                                            }
                                        }
								?>
									
										<!-- Question Base Start //-->
										<tr valign='top'>
											<td>
	                    						<table width="100%" border="0" cellspacing="0" cellpadding="0">
	                    							<tr>
	                    								<td>
	                    									<script language="javascript" src="/templates/forms/layer.js"></script>
								                        	<? if($is_payment_notice) { ?>
																<script type="text/javascript" language="javascript" src="/templates/forms/eNoticePayment_replyslip.js?t=<?=time()?>"></script>
																<script type="text/javascript" language="javascript" src="/templates/jquery/jquery.tablednd_0_5.js"></script>
															<? } else { ?>
																<script language="javascript" src="/templates/forms/form_edit.js"></script>
															<? } ?>	
	                    
							                                <form name="ansForm" method="post" action="update.php">
							                                        <input type=hidden name="qStr" value="">
							                                        <input type=hidden name="aStr" value="">
							                                </form>
	                            							<div id="replyslipdiv" style="width:100%;overflow-x:auto;overflow-y:hidden;"></div>
	                            							<script type="text/javascript" language="Javascript">
															need2checkformSQ = <? if(trim($queString)!="" && strpos($queString, "#MSUB#1")!==false){ echo "true"; } else { echo "false"; }  ?>;
															
														<?php if(!$is_payment_notice) { ?>
	                            							var replyslip = '<?=$Lang['eNotice']['ReplySlip']?>';
							
							                                <?=$lform->getWordsInJS()?>
							                                
							                                var paymentOptions = "";
							                                var paymentOptIDAry = new Array();
															var paymentOptNameAry = new Array();
															var paymentItemID = ""; 
															var paymentIDAry = new Array();
															var paymentItemName = "<?=$Lang['eNotice']['PaymentItemName']?>"; 
															<?
															$result = ($lpayment->checkBalance($StudentID,1));
															$ac = $lpayment->getWebDisplayAmountFormatDB($result[0]);
															?>
															var accountBalance = "<?=$use_payment_system?'':($Lang['eNotice']['AccountBalance'].' : $'.$result[0])?>";
															var totalAmountToPay = '<?=$Lang['General']['Total'].' : $<span id="TotalAmountToPay">0</span>'?>';
															
							                                var sheet= new Answersheet();
							                                // attention: MUST replace '"' to '&quot;'
							                                sheet.qString="<?=str_replace("&lt;br&gt;","<br>",$queString)?>";
							                                sheet.aString="<?=$ansString?>";
							                                //edit submitted application
							                                sheet.mode=1;
							                                <? if(!$is_payment_notice) { ?>
											                // display * and perform answer checking for Questions must be submitted
							                                sheet.mustSubmit=1;
															<? } ?>	
							                                sheet.viewOnly = <?= ($ViewOnly ? 1 : 0)?>;
							                                sheet.answer=sheet.sheetArr();
							                                Part3 = '';
							                                document.write(editPanel('0'));
							                                
							                             <?php }else{ ?>
							                               	var options = {
																"QuestionString":"<?=str_replace("&lt;br&gt;","<br>",$queString)?>",
																"AnswerString":"<?=$ansString?>",
																"AccountBalance":<?=$account_balance?>,
																"DoNotShowBalance":<?=$do_not_check_balance?'1':'0'?>,
																"ExternalAmountTotalFieldID":"thisAmountTotal",
																"PaymentCategoryAry":[],
																"TemplatesAry":[],
																"DisplayQuestionNumber":<?=$lnotice->DisplayQuestionNumber==1?1:0?>,
																"SubsidyStudentID":'<?=IntegerSafe($StudentID)?>',
																"SubsidyIdentityID":'<?=$student_subsidy_identity_id?>',
																<?php
																$words = $eNoticePaymentReplySlip->getRequiredWords();
																foreach($words as $key => $value){
																	echo '"'.$key.'":"'.$value.'",'."\n";
																}
																?>
																"Debug":false
															};
															var sheet = new Answersheet(options);
															//$(document).ready(function(){
																sheet.renderFillInPanel('replyslipdiv');
							                                //});
							                             <?php } ?>
							                                
							                                function copyback()
							                                {
							                                <?php if($is_payment_notice) { ?>
							                                		document.ansForm.aStr.value = sheet.constructAnswerString();
							                                <?php }else{ ?>	
							                                         finish();
							                                <?php } ?>         
							                                         document.form1.qStr.value = document.ansForm.qStr.value;
							                                         document.form1.aStr.value = document.ansForm.aStr.value;
							                                }
							                                </SCRIPT>
	                    								</td>
													</tr>
	                							</table>
	                						</td>
										</tr>
										<!-- Question Base End //-->
								<? } ?>
								<? } ?>
							<? } ?>

			
                            <? //if ($editAllowed && !$lnotice->ReplySlipContent) 
                            if ($editAllowed && $hasContent)
                            { ?>
    							<tr>
                            		<td align="left" class="tabletext"><span class='tabletext2' id='NoticeSignInstruction'><?=(($lnotice->replyStatus==2 || $isReplySlipTextOnly) ? "" : $i_Notice_SignInstruction)?></td>
            					</tr>
	        					<?php
	        					if($tng_payment_under_processing){
	        						echo '<tr>';
	        						echo '<td align="left" class="tabletext"><span class="red">'.$Lang['eNotice']['PaymentResignRemark'].'</span></td>';
	        						echo '</tr>';
	        					}
	        					?>
                            <? } ?>
                            
                            <?php 
			                if($is_payment_notice && !$sys_custom['eClassApp']['eNoticeHideSignButton'] && !$ViewOnly && $editAllowed && ($use_payment_system || $use_topup) && ($can_pay_at_school || count($use_payment_system_ary)>0)){
			                	echo '<tr>';
			                   	echo 	'<td class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td>';
			               		echo '</tr>';
			               		
			                	echo '<tr>';
			                		echo '<td align="left" class="tabletext">'.$Lang['eNotice']['PaymentNoticePaymentMethod'].'</td>';
			                	echo '</tr>';
			                	echo '<tr>';
			                		echo '<td align="left" class="tabletext">';
								    $checked = 1;
			                		if($sys_custom['ePayment']['MultiPaymentGateway']) {
                                        $checked = 0;
									}

			                		if($use_tng){
                            			echo $linterface->Get_Radio_Button("NoticePaymentMethod_TNG", "NoticePaymentMethod_", 'TNG', $checked, "", $Lang['ePayment']['TNG'], "$('#NoticePaymentMethod').val(this.value);");
                            		} 
                            		if($use_alipay){
                            			echo $linterface->Get_Radio_Button("NoticePaymentMethod_AlipayHK", "NoticePaymentMethod_", 'AlipayHK', $checked, "", $Lang['ePayment']['Alipay'], "$('#NoticePaymentMethod').val(this.value);");
                            		}
                                    if($use_alipaycn){
                                        echo $linterface->Get_Radio_Button("NoticePaymentMethod_AlipayCN", "NoticePaymentMethod_", 'AlipayCN', $checked, "", $Lang['ePayment']['AlipayCN'], "$('#NoticePaymentMethod').val(this.value);");
                                    }
                            		if($use_hsbc_fps){
                            			echo $linterface->Get_Radio_Button("NoticePaymentMethod_FPS", "NoticePaymentMethod_", 'FPS', $checked, "", $Lang['ePayment']['FPS'], "$('#NoticePaymentMethod').val(this.value);");
                            		}
                            		if($use_visamaster) {
										echo $linterface->Get_Radio_Button("NoticePaymentMethod_VISAMASTER", "NoticePaymentMethod_", 'VISAMASTER', $checked, "", $Lang['ePayment']['VisaMaster'], "$('#NoticePaymentMethod').val(this.value);");
									}
                                    if($use_wechat) {
                                        echo $linterface->Get_Radio_Button("NoticePaymentMethod_WECHAT", "NoticePaymentMethod_", 'WECHAT', $checked, "", $Lang['ePayment']['WeChat'], "$('#NoticePaymentMethod').val(this.value);");
                                    }
                            		if($use_tapandgo){
                            			echo $linterface->Get_Radio_Button("NoticePaymentMethod_TAPANDGO", "NoticePaymentMethod_", 'TAPANDGO', $checked, "", $Lang['ePayment']['TapAndGo'], "$('#NoticePaymentMethod').val(this.value);");
                            		}
                            		if($use_topup){
                            			echo $linterface->Get_Radio_Button("NoticePaymentMethod_DirectPay", "NoticePaymentMethod_", '', $checked, "", $eEnrollment['debit_directly'], "$('#NoticePaymentMethod').val(this.value);");
                            		}
                            		if($can_pay_at_school){
                            			echo $linterface->Get_Radio_Button("NoticePaymentMethod_PayAtSchool", "NoticePaymentMethod_", 'PayAtSchool', 0, "", $pay_at_school_display_word, "$('#NoticePaymentMethod').val(this.value);");
                            		}
									echo '</td>';
			                	echo '</tr>';
			                }
			                ?>
                            
                  			<tr>
                            	<td class="dotline"  ><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                            </tr>
                          
                            <?  if ($lnotice->replyStatus == 2) { ?>
                            <tr> 
                                <td align="left" class="tabletext">
								    <span><?=$signby?> : <?=$signer ?></span>
							    </td>
            				</tr>
            			    <tr> 
                                <td align="left" class="tabletext">
								    <span><?=$i_Notice_SignedAt?> : <?=$lnotice->signedTime ?></span>
							    </td>
            				</tr>								  
						    <?  } ?>
						    
                            <tr>
            					<td>
                                	<?php
                                	if ($sys_custom['eClassApp']['eNoticeHideSignButton']) {
			                    		// do not show sign button for some schools
			                    	}
			                    	else {
	                                	if (isset($sys_custom['enotice_sign_with_password']) && $sys_custom['enotice_sign_with_password'])
	                                	{
	                                		$sign_with_password = "<table border='0'><tr><td class='tabletext2'>".$i_UserPassword."<input type='password' name='user_password' id='user_password' /></td></tr></table>";
	                                	}
	                                	 if ($editAllowed && !$lnotice->ReplySlipContent && $hasContent)
	                                	 {
	                                	 	if (!$is_payment_notice || ($is_payment_notice && !$lnotice->isTargetNoticeSigned($NoticeID,$StudentID))){
	                                   			echo $sign_with_password.'<div id="submitBtnDiv">'.$linterface->GET_ACTION_BTN($i_Notice_Sign, "button", "copyback();SubmitForm(this.form)","submit2").'</div>';
	                                	 	}
	                                	 } elseif ($editAllowed && (!$hasContent || $lnotice->ReplySlipContent))
	                                	 {
											echo $sign_with_password.'<div id="submitBtnDiv">'.$linterface->GET_ACTION_BTN($i_Notice_Sign, "button", "SubmitForm(this.form)","submit2").'</div>';
										 }
										 echo '<div id="submittingDiv" style="width:100%; text-align:center; display:none;">'.$linterface->Get_Ajax_Loading_Image($noLang=1).' <span style="color:red; font-weight:bold;">'.$Lang['General']['Submitting...'].'</span></div>';
										 
										 if ($showAuthCodeRemarks) {
										 	echo '	<br />
													<div style="color:red; width:100%; text-align:center; font-weight:bold; font-size:20px;">'.$Lang['eClassApp']['Warning']['CannotSignBecauseNoAuthCode'].'</div>
												';
										 }
			                    	}
                                   ?>
								</td>
            				</tr>
            			</table>
            		</td>
            	</tr>
            </table>  
		</td>
	</tr>
</table>

<? if($is_payment_notice) { ?>
	<form id="form1" name="form1" action="eNoticePayment_sign_update.php" method="post" onSubmit="return false">
<? } else { ?>
	<form id="form1" name="form1" action="sign_update.php" method="post" onSubmit="return false">
<? } ?>

	<input type="hidden" name="thisAmountTotal" id="thisAmountTotal" value="0">
	<?
	$result = ($lpayment->checkBalance($StudentID,1));
	$accBal = str_replace(",","", $result[0]);
	?>
    <input type="hidden" name="role" id="role" value="<?=$role?>">
	<input type="hidden" name="amountBalance" id="amountBalance" value="<?=$accBal?>">
	<input type="hidden" name="qStr" value="">
	<input type="hidden" name="aStr" value="">
	<input type="hidden" name="NoticeID" id="NoticeID" value="<?=$NoticeID?>">
	<input type="hidden" name="StudentID" id="StudentID" value="<?=$StudentID?>">
	<input type="hidden" name="CurID" id="CurID" value="<?=$CurID?>">
	<input type="hidden" name="token" id="token" value="<?=$token?>">
	<input type="hidden" name="authCode" id="authCode" value="<?=$authCode?>">
	<input type="hidden" name="NoticePaymentMethod" id="NoticePaymentMethod" value="" />
<?php 
if($is_payment_notice) { 
	echo csrfTokenHtml(generateCsrfToken());
}
?>	
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>