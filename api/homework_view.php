<?php
// using 
/*
 *
 */
/********************** Change Log ***********************/
#	Date	:	2020-04-07 [Ray]
#				added homework status
#	Date	:	2014-02-10 [Kin]
#				modified the table to mobile UI 
#
/********************** end of Change Log ***********************/
//$NoUTF8 = true;
$PATH_WRT_ROOT = "../";

//set language
@session_start();
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

//if (file_exists($PATH_WRT_ROOT."file/eclass_app.php"))
//{
//	session_start();
//	include_once($PATH_WRT_ROOT."file/eclass_app.php");
//	$intranet_session_language = $eclass_app_language;
//}
//else {
//	$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
//	if ($parLang != '') {
//		switch (strtolower($parLang)) {
//			case 'en':
//				$intranet_hardcode_lang = 'en';
//				break;
//			default:
//				$intranet_hardcode_lang = 'b5';
//		}
//	}
//}

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");


$CurID = IntegerSafe($_GET['CurID']);
$StudentID = IntegerSafe($_GET['StudentID']);
$HomeworkID = IntegerSafe($_GET['HomeworkID']);

$UserID = $CurID;
$_SESSION['UserID'] = $CurID;

$delimiter = "###";
$keyStr = $special_feature['ParentApp_PrivateKey'].$delimiter.$HomeworkID.$delimiter.$fromModule.$delimiter.$StudentID.$delimiter.$CurID;

if($token != md5($keyStr)) {
	include_once($PATH_WRT_ROOT."lang/lang.en.php");
	echo $i_general_no_access_right	;
	exit;
}

# Generate token for auth checking
$dl_auth_token = '';
if($special_feature['download_attachment_add_auth_checking'])
{
    $dl_auth_token = returnSimpleTokenByDateTime();
}

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['eHomework'];
$linterface = new interface_html("popup6.html");
$linterface->LAYOUT_START();


$lhomework = new libhomework2007();

if ($fromModule == 'eHomework') {
	$record = $lhomework->returnRecord($HomeworkID);
	
	list($HomeworkID, $subjectGroupID, $subjectID, $start, $due, $loading, $title, $description, $lastModified, $homeworkType, $PosterName, $TypeID, $AttachmentPath,$handinRequired,$collectRequired, $PostUserID, $CreatedBy)=$record; 
	$lu = new libuser($PostUserID);
	$PosterName2 = $lu->UserName();
	
	$lu2 = new libuser($CreatedBy);
	$CreatedByName = $lu2->UserName();
	
	if($lhomework->useHomeworkType) {
		$typeInfo = $lhomework->getHomeworkType($TypeID);
		$typeName = ($typeInfo[0]['TypeName']) ? $typeInfo[0]['TypeName'] : "---";
	}
	
	$subjectName = $lhomework->getSubjectName($subjectID);
	$subjectGroupName = $lhomework->getSubjectGroupName($subjectGroupID);
	
	$description = nl2br($lhomework->convertAllLinks($description,40));
	
	if($AttachmentPath!=NULL){
		//$displayAttachment = $lhomework->displayAttachment($HomeworkID);
		$displayAttachment = $lhomework->displayAttachment_IMG($HomeworkID,$dl_auth_token);
		//$displayAttachment = '<div id="descriptionDiv" style="width: 100%; overflow-x: auto; overflow-y: hidden;">'.$lhomework->displayAttachment_IMG($HomeworkID).'</div>';
		
	}
	
	$sql = "SELECT 
	IF(e.RecordStatus=1, '".$Lang['SysMgr']['Homework']['Submitted']."', IF(e.RecordStatus=6,'".$Lang['SysMgr']['Homework']['Supplementary']."',(IF(e.RecordStatus=-1, IF(CURDATE()<a.DueDate,'".$Lang['SysMgr']['Homework']['NotSubmitted']."','".$Lang['SysMgr']['Homework']['ExpiredWithoutSubmission']."'), IF(e.RecordStatus=2, '".$Lang['SysMgr']['Homework']['LateSubmitted']."','--'))))) as status
	FROM INTRANET_HOMEWORK as a
     LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS d ON d.SubjectGroupID = a.ClassGroupID
     LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.HomeworkID = a.HomeworkID AND e.StudentID = d.UserID)
	 WHERE a.HomeworkID=$HomeworkID AND d.UserID=$StudentID";

	$homework_status = $lhomework->returnArray($sql,1);
	$homework_status = $homework_status[0]['status'];

}
?>
<style type="text/css">
</style>
<SCRIPT LANGUAGE=Javascript>
jQuery(document).ready( function() {
	$('div#descriptionDiv').css('width', Get_Screen_Width() - 10);
});
</SCRIPT>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
<div data-role="page" id="pageone">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr valign='top'>
					<td bgcolor='#FFFFFF' style="padding:10px;"><span class='tabletext' style="font-size:22px;"><strong><?=$title?></strong></span></td>
				</tr>
				<tr valign='top'>
					<td bgcolor='#FFFFFF' style=" padding:10px;font-size:18px;">
						<table border="0" cellpadding="0" cellspacing="0" style="width:100%; font-size:16px;">
							<tr>
								<td style="width:90%; text-align:left; vertical-align:bottom; font-size:16px;">
									<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_date.png" align="absmiddle" style="width:20px; height:20px;" />
									<span style="vertical-align:bottom;"><?=$start?></span>
								</td>
							</tr>
								<tr>
									<td style="text-align:left; vertical-align:bottom; font-size:16px;">
										<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_deadline.png" align="absmiddle" style="width:20px; height:20px;" />
										<span style="vertical-align:bottom;"><font color="red"><?=$due?></font></span>
									</td>
								</tr>
						</table>
					</td>
				</tr>
				<tr valign='top'>
					<td bgcolor='#FFFFFF' style="font-size:16px; padding:10px;"><div id="descriptionDiv" style="width: 100%; overflow-x: auto; overflow-y: hidden;"><span class='tabletext2'><?=$description?></span></div></td>
				</tr>
				<tr valign='top'>
					<td bgcolor='#FFFFFF' style="font-size:16px; padding:10px;">
						<div class="ui-block-a" style="width:100%;">
							<span><?=$displayAttachment?></span>
						</div>
					</td>
				</tr>
						
			</table>
		</td>
	</tr>
</table>
	<div data-role="collapsible" data-collapsed="true" style="font-size:18px">
		<h1><?=$Lang['General']['Misc']?></h1>
		<div class="ui-grid-a">
    		<div class="ui-block-a" style="width:100%;">
    			<span><strong><?=$Lang['SysMgr']['Homework']['Subject']?></strong></span>
       			<br>
       			<span><?=$subjectName?></span>
			</div>
			
	    	<div class="ui-block-a" style="width:100%;">
	    		<br>
	       		<span><strong><?=$Lang['SysMgr']['Homework']['SubjectGroup']?></strong></span>
	       		<br>
	       		<span><?=$subjectGroupName?></span>
			</div>
			
			<? if($lhomework->useHomeworkType) { ?>
			<div class="ui-block-a" style="width:100%;">
				<br>
	       		<span><strong><?=$i_Homework_HomeworkType?></strong></span>
	       		<br>
	       		<span><?=$typeName?></span>
			</div>
			<? } ?>
			
			<div class="ui-block-a" style="width:100%;">
				<br>
				<span><strong><?=$Lang['SysMgr']['Homework']['Workload']?></strong></span>
				<br>
				<span><?php
				if($loading<17){
				   $show_hours = ($loading/2);
				}else{
				   $show_hours = "> 8";
				}
				echo $show_hours." ".$Lang['SysMgr']['Homework']['Hours']			
				?>
				</span>
			</div>
			
			<div class="ui-block-a" style="width:100%;">
				<br>
				<span><strong><?=$Lang['SysMgr']['Homework']['HandinRequired']?></strong></span>
				<br>
				<span><?=($handinRequired=="1"? $i_general_yes: $i_general_no)?></span>
			</div>
			
			<? if($lhomework->useHomeworkCollect){?>
			<!--div class="ui-block-a" style="width:100%;">
				<br>
				<span><strong><?=$i_Homework_Collected_By_Class_Teacher?></strong></span>
				<br>
				<span><?=($collectRequired=="1"? $i_general_yes:$i_general_no)?></span>
			</div-->
			<? } ?>
			
			<div class="ui-block-a" style="width:100%;">
				<br>
	       		<span><strong><?=$Lang['SysMgr']['Homework']['TeacherSubjectLeader']?></strong></span>
	       		<br>
	       		<span><?=($PosterName2==""? "--":$PosterName2)?></span>
			</div>
			
			<!--div class="ui-block-a" style="width:100%;">
				<br>
	       		<span><strong><?=$Lang['SysMgr']['Homework']['CreatedBy']?></strong></span>
	       		<br>
	       		<span><?=($CreatedBy ? $CreatedByName : $PosterName2)?></span>
			</div-->

            <div class="ui-block-a" style="width:100%;">
                <br>
                <span><strong><?=$Lang['SysMgr']['Homework']['Status']?></strong></span>
                <br>
                <span><?=$homework_status?></span>
            </div>
		</div>
	</div>
</div>

<?php
//debug_pr($image_path);
//debug_pr($LAYOUT_SKIN);

intranet_closedb();
/*$linterface->LAYOUT_STOP();*/
?>