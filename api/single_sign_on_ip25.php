<?php


############### Change Log [Start]
#
#	Date:	2015-11-13 Yuen
#			Customization: single-sign-on to PL of IP 2.5
#
#
###############################


include ("../includes/global.php");
include ("../includes/libdb.php");
include ("../includes/libuser.php");

intranet_opendb();

if ($sys_custom['SingleSignOnIP25URL']=="")
{
    echo "Failed to proceed because of missing setting for IP 2.5 URL!";
    die();
}

if ($sys_custom['SingleSignOnIP25eBooking'])
{
    $module = "IP25eBooking";
} elseif ($plugin['CENTRAL_Power_Lesson'])
{
    $module = "IP25PowerLesson";
} else
{
    echo "Failed to proceed because of missing configuration of single-sign-on!";
    die();
}

$header_lu = new libuser($UserID);

# get the URL of this site
$PageURLArr = explode("//", curPageURL());
if (sizeof($PageURLArr)>1)
{
    $strPath = $PageURLArr[1];
    $eClassURL = $PageURLArr[0] . "//" . substr($strPath, 0, strpos($strPath, "/"));
}

intranet_closedb();

header("location: ".$sys_custom['SingleSignOnIP25URL']."/single_sign_on_from_ej.php?eclasskey=".$header_lu->sessionKey."&eClassURL=".$eClassURL."&module=".$module);
?>