<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ePCM_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ePCM/libPCM_cfg.php");
include_once($PATH_WRT_ROOT."includes/ePCM/libPCM.php");
include_once($PATH_WRT_ROOT."includes/ePCM/libPCM_ui.php");
include_once($PATH_WRT_ROOT."includes/ePCM/libPCM_db.php");
include_once($PATH_WRT_ROOT."includes/ePCM/libPCM_db_file.php");
intranet_opendb();

$db = new libPCM_db();
$db_file = new libPCM_db_file();

$token = $_POST['token'];
$password = $_POST['password'];

$isValid = checkPassword($password,$token);

if(!$isValid){
	header('Location: acknowledge.php?t='.$token.'&msg=wpw');
	die();
}

$quotation_id=$db->getQuotationIdByToken($token);
$rows_quotation=$db->getCaseQuotation('',$quotation_id);
$row_quotation=$rows_quotation[0];

$case_id=$row_quotation['CaseID'];

$attachmentAry = getAttachment($case_id);
//UI
$school_name=GET_SCHOOL_NAME();
$MODULE_OBJ['title'] = $school_name;
$linterface = new interface_html("popup_index.html");
$linterface->LAYOUT_START();
echo getFileListUI($attachmentAry[2]);
$linterface->LAYOUT_STOP();

// if(is_dir($attachment_folder)){
// 	$fileNameAry = scandir($attachment_folder);
// }


intranet_closedb();

function getAttachment($case_id){
	global $PATH_WRT_ROOT,$db;
	$return = $db->getCaseStageAttachment($case_id, '2', true,'2');
	return $return;
}

function getFileListUI($fileInfoArr, $displaySize=true, $returnArr=false){
	$x = '';
	if(!empty($fileInfoArr)){
		$count = 0;
		$num = count($fileInfoArr);
		$resultArr = array();
		foreach((array)$fileInfoArr as $_fileInfo){
			$count ++;

			$linkLang = $_fileInfo['FileName'];
			if($displaySize){
				$linkLang .= ' ('.displayFileSize($_fileInfo['FileSize']).')';
			}
			$fileLink = displayFileDownloadLinkSize($_fileInfo['AttachmentID'], $linkLang);
			$x .= $fileLink;
			if($count != $num){
				$x .= '<br />';
			}
			if($returnArr)
				$resultArr[] = $fileLink;
		}
	}
	else{
		$x .= Get_String_Display('');
	}
	if($returnArr){
		return $resultArr;
	}
	else{
		return $x;
	}
}
function displayFileSize($sizeInByte, $recursiveNum=0){
	$arr = array('', 'K', 'M', 'G', 'T');
	$callNum = $recursiveNum;
	if($sizeInByte < 1024){
		return round($sizeInByte, 1).$arr[$callNum].'B';
	}
	else{
		$callNum ++;
		// for safety
		if($callNum > 4){
			return false;
		}else{
			return displayFileSize(($sizeInByte/1024),$callNum);
		}
	}
}
function displayFileDownloadLinkSize($AttachmentID, $LangDisplay){
	global $token;
	//encrypt attachmentID
	$html = '';
	$html .= '<a class="icon_clip tablelink" href="downloadAttachment.php?i='.encryptAttachmentID($AttachmentID).'">';
	$html .= $LangDisplay;
	$html .= '</a>';
	return $html;
}
function encryptAttachmentID($AttachmentID){
	$key = 'ePCM_key_for_encrpt_aid'; //Don't edit
	return AES_128_Encrypt($AttachmentID,$key);
}
function checkPassword($password,$token){
	global $db;
	$tokenInfo = $db->getTokenInfo($token,$quotationID='');
	//for safty reason => if no password => return false
	if(empty($tokenInfo['Pw'])){
		return false;
	}
	if($password==$tokenInfo['Pw']){
		return true;
	}
	return false;
}
?>