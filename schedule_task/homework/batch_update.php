<?php
$WRT_ROOT= "{$_SERVER['DOCUMENT_ROOT']}/";
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_opendb();
$libdb = new libdb();
$lhomework = new libhomework2007();

$json = new JSON_obj();
$nowtime = time();

$tableArr = array(
		"INTRANET_HOMEWORK",
		"INTRANET_HOMEWORK_HANDIN_LIST",
		"DISCIPLINE_ACCU_ADDED_DETENTION_MAPPING",
		"DISCIPLINE_ACCU_RECORD",
		"DISCIPLINE_ACCU_HW_GROUPED_MAPPING",
		"DISCIPLINE_MERIT_RECORD",
		"DISCIPLINE_DETENTION_STUDENT_SESSION"
);

if (count($tableArr) > 0) {
	foreach ($tableArr as $kk => $table) {
		$strSQL = " CREATE TABLE " . $table . "_20170123 SELECT * FROM " . $table;
		$lhomework->db_db_query($strSQL);
	}
}

$academicYear = $lhomework->GetAllAcademicYear();
$yearID = Get_Current_Academic_Year_ID();
$academicYearTerm = $lhomework->getAllAcademicYearTerm($yearID);

$handin_param = array(
		"yearID" => $yearID,
		"isCronjob" => TRUE,
		"sqlType" => "forDataTransferBatchOldDataUpdate",
		"batchDueDate" => "2016-12-31",
		"callBy" => basename(__FILE__)
);

if (!isset($intranet_session_language)) $intranet_session_language = "b5";

/*****************************************************/
if (!function_exists('cust_checkSchoolHoliday')) {
	function cust_checkSchoolHoliday($lhomework) {
	
		$strSQL = "SELECT COUNT(*) AS HDYCount FROM INTRANET_EVENT WHERE DATE_FORMAT(EventDate,'%Y-%m-%d') like '" . date("Y-m-d") . "' and RecordType IN (3,4)";
		$result = $lhomework->returnResultSet($strSQL);
	
		$date = strtotime($date);
		$date = date("l", $date);
		$date = strtolower($date);
		if(in_array($date, array("saturday", "sunday")) || $result[0]["HDYCount"] > 0) {
			return true;
		}
		return false;
	}
}

/*****************************************************/
$currDate = date("Y-m-d");
/*****************************************************/
if (cust_checkSchoolHoliday($lhomework) || !$sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage'] || $sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']) {
	/*intranet_closedb();
	echo '<br>eHomework [Violation & Detention] cron job end (disabled) : ' . date("Y-m-d H:i:s") . '<br>';
	exit;*/
}

$funcData = $lhomework->getStudentsByParamForDiscipline($handin_param);
/************************************************ Batch Update ***************************************/
$_violation = $funcData[0];
$handleArr = $funcData[1];
$discipData = $funcData[2];
	
/**************************/
/* For Debug setting */
/**************************/
$is_debugMode = false;
$allowUpdateAndDiscip = true;
	
if ($is_debugMode) {
	header('Content-type: text/plain; charset=utf-8');
}
$debugID = 1;
/**************************/
/* For Debug setting */
/**************************/

if (count($handleArr) > 0) {
	if (count($discipData) > 0) {
		if (!$is_debugMode && $allowUpdateAndDiscip) {
			foreach ($discipData as $violationDate => $violationData) {
				/* Pass to eDiscipline */
				$disciplinev12 = new libdisciplinev12();
				$disciplinev12->INSERT_HW_MISCONDUCT_RECORD($violationData, $violationDate);
			}
		}
	}
	$_voliationHTML = "";
	$_detentionHTML = "";
	$_supplementHTML = "";
	$_absHTML = "";

	$defaultNow = "NOW()";
	
	foreach ($handleArr as $kk => $formObjs) {
		if (count($formObjs) > 0) {
			foreach ($formObjs as $hw_userID => $hw_userInfo) {
					
				if (empty($hw_userInfo["info"]["StudentName"])) $studentName = $hw_userInfo["info"]["UserLogin"];
				else $studentName = $hw_userInfo["info"]["StudentName"];
					
				$studenInfo = $hw_userInfo["info"]["StudentID"] . "\t" . $hw_userInfo["info"]["ClassTitle"] . "\t" . $studentName . "";
					
				$strSQL = "INSERT INTO INTRANET_HOMEWORK_HANDIN_VIOLATION";
				$strSQL .= " (HWViolationDate, StudentID, UserLogin, StudentName, ClassNumber, WEBSAMSCode, ClassTitle, ViolationRecord, DateInput)";
				$strSQL .= " VALUES ";
				$strSQL .= " ('" . $currDate. "', " . $hw_userID . ", '" . $hw_userInfo["info"]["UserLogin"] . "', '" . $studentName . "', '" . $hw_userInfo["info"]["ClassNumber"] . "', '" . $hw_userInfo["info"]["WEBSAMSCode"] . "', '" . $hw_userInfo["info"]["ClassTitle"] . "', '0', NOW());";
				
				if (!$is_debugMode) {
					if ($allowUpdateAndDiscip) {
						$lhomework->db_db_query($strSQL);
						$HWViolationID = $lhomework->db_insert_id();
					} else {
						$HWViolationID = $debugID;
						$debugID++;
						// echo $strSQL . "<br>";
					}
				} else {
					$HWViolationID = $debugID;
					$debugID++;
				}
				if ($HWViolationID > 0) {
					$pre_strSQL = "INSERT INTO INTRANET_HOMEWORK_HANDIN_VIOLATION_HWLIST ";
					$pre_strSQL .= " (HWViolationID, StudentID, RecordID, HomeworkID, Title, Description, RecordStatus, DueDate, YearClassID, ClassGroupID, SubjectID, Subject, SuppRecordStatus, SuppRecordDate, RecordType) ";
					$pre_strSQL .= " VALUES ";
					if (count($hw_userInfo["_violation"]) > 0) {
						$_voliationHTML .= $studenInfo . "";
						$_voliationHTML .= "\n";
						foreach ($hw_userInfo["_violation"] as $hw_id => $hw_info) {
							$param = array(
									$HWViolationID,
									$hw_userID,
									$hw_info["RecordID"],
									$hw_info["HomeworkID"],
									$hw_info["Title"],
									$hw_info["Description"],
									$hw_info["RecordStatus"],
									$hw_info["DueDate"],
									$hw_info["YearClassID"],
									$hw_info["ClassGroupID"],
									$hw_info["SubjectID"],
									$hw_info["Subject"],
									!empty($hw_info["SuppRecordStatus"]) ? $hw_info["SuppRecordStatus"]: -9999,
									!empty($hw_info["SuppRecordDate"]) ? $hw_info["SuppRecordDate"] : 'NOW()',
									"VIOLATION_BATCH"
							);
							$strSQL = str_replace("'NOW()'", "NOW()", $pre_strSQL . " ('" . implode("', '", $param) . "');");
							if (!$is_debugMode) {
								if ($allowUpdateAndDiscip) {
									$lhomework->db_db_query($strSQL);
								} else {
									// echo $strSQL . "<br>";
								}
							}
							$_voliationHTML .= "\t ----- \t" . $hw_info["RecordID"] . "\t" . $hw_info["HomeworkID"] . "\t" . $hw_info["Subject"] . "\t" . $hw_info["DueDate"] . "\t" . $hw_info["Title"] . "\t" . str_replace("\r", " ", str_replace("\n", " ", $hw_info["Description"])) . "\t欠交\t" . $vv["DueDate"] . "\n";
						}
						/****************************************************/
						$strSQL = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET SuppRecordStatus='-20', SuppRecordDate=NOW() WHERE RecordID in (" . implode(", ", array_keys($hw_userInfo["_violation"])) . ") AND SuppRecordDate IS NULL;";
						if (!$is_debugMode) {
							if ($allowUpdateAndDiscip) {
								$lhomework->db_db_query($strSQL);
							} else {
								echo $strSQL . "<br>";
							}
						} else {
							// echo $strSQL . "\n";
						}
						/****************************************************/
						$strSQL = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET HWViolationID='" . $HWViolationID . "', HWViolationDate=NOW() WHERE RecordID in (" . implode(", ", array_keys($hw_userInfo["_violation"])) . ");";
						if (!$is_debugMode) {
							if ($allowUpdateAndDiscip) {
								$lhomework->db_db_query($strSQL);
							} else {
								echo $strSQL . "<br>";
							}
						} else {
							// echo $strSQL . "\n";
						}
						/****************************************************/
						$strSQL = "UPDATE INTRANET_HOMEWORK_HANDIN_VIOLATION SET ViolationRecord='1' WHERE HWViolationID='" . $HWViolationID . "';";
						if (!$is_debugMode) {
							if ($allowUpdateAndDiscip) {
								$lhomework->db_db_query($strSQL);
							} else {
								echo $strSQL . "<br>";
							}
						}
					}
				}
			}
		}
	}
	if ($is_debugMode) {
		if (!empty($_voliationHTML)) echo "\n" . $currDate . "\t'記欠交'記錄\n" . $_voliationHTML;
	}
} else {
	$strSQL = "INSERT INTO INTRANET_HOMEWORK_HANDIN_VIOLATION";
	$strSQL .= " (HWViolationDate, StudentID, UserLogin, StudentName, ClassNumber, WEBSAMSCode, ClassTitle, ViolationRecord, DateInput)";
	$strSQL .= " VALUES ";
	$strSQL .= " ('" . $currDate. "', 0, '--', '--', '0', '--', '--', '0', NOW());";
	if (!$is_debugMode) {
		if ($allowUpdateAndDiscip) {
			$lhomework->db_db_query($strSQL);
			$HWViolationID = $lhomework->db_insert_id();
		} else {
			echo $strSQL . "<br>";
		}
	} else {
		$HWViolationID = $debugID;
		$debugID++;
	}
}
/************************************************ Batch Update ***************************************/

/*************/
/* For Debug */
/*************/
intranet_closedb();
echo '<br>eHomework [Violation & Detention] cron job end : ' . date("Y-m-d H:i:s") . '<br>';
?>