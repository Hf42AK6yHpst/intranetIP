<?php
##### Change Log [Start] #####
#
#	Date	:	2015-09-24	Omas
# 			Create this page
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
intranet_opendb();

if(!$plugin['eEnrollment'] ){
	exit;
	intranet_closedb();
}

$libenroll = new libclubsenrol();
$lwebmail = new libwebmail();
$GeneralSetting = new libgeneralsettings();

$SettingList[] = "'UseAttendanceReminder'";
$SettingList[] = "'AttendanceReminderSubject'";
$SettingList[] = "'AttendanceReminderContent'";
$SettingList[] = "'AttendanceReminderDaysBefore'";
$SettingList[] = "'AttendanceReminderSendTime'";

$Settings = $GeneralSetting->Get_General_Setting($libenroll->ModuleTitle,$SettingList);


if(!$Settings['UseAttendanceReminder']){
	exit;
	intranet_closedb();
}

if($Settings['AttendanceReminderDaysBefore'] ==0){
	$dateToCheck = date('Y-m-d');
}
else{
	$dateToCheck = date('Y-m-d',(time()-86400 *$Settings['AttendanceReminderDaysBefore']));
}

$startTime = $dateToCheck.' 00:00:00';
$endTime = $dateToCheck.' 23:59:59';

// club
$sql = "SELECT 
							b.EnrolGroupID,b.GroupDateID,count(c.StudentID) as numAttendance
					FROM
							INTRANET_ENROL_GROUP_DATE as b 
							left outer join INTRANET_ENROL_GROUP_ATTENDANCE as c ON (b.EnrolGroupID = c.EnrolGroupID)
					WHERE
							UNIX_TIMESTAMP(b.ActivityDateStart) 
							BETWEEN 
							UNIX_TIMESTAMP('".$startTime."') AND UNIX_TIMESTAMP('".$endTime."')
					Group By
							b.GroupDateID";
$groupAttendanceInfoArr = $libenroll->returnResultSet($sql);

//Select group without attendance record
foreach((array)$groupAttendanceInfoArr as $_attendaceArr){
	if($_attendaceArr['numAttendance'] > 0){
	}
	else{
		$remindGroupIDArr[] = $_attendaceArr['EnrolGroupID'];
	}
}
	
if(count($remindGroupIDArr)>0){
	// get group PIC
	$AcademicYearID = Get_Current_Academic_Year_ID();
				
	$sql = "SELECT
				iegs.EnrolGroupID,
				iegs.UserID
			FROM
				INTRANET_ENROL_GROUPSTAFF as iegs
			WHERE 
				iegs.EnrolGroupID IN ('".implode("','",(array)$remindGroupIDArr)."')
				AND iegs.StaffType = 'PIC' ";
	$groupStaffArr = $libenroll->returnResultSet($sql);
}

// event
$sql = "			SELECT 
							b.EnrolEventID,b.EventDateID,count(c.StudentID) as numAttendance
					FROM
							INTRANET_ENROL_EVENT_DATE as b
							left outer join INTRANET_ENROL_EVENT_ATTENDANCE as c on (b.EnrolEventID = c.EnrolEventID)
					WHERE
							UNIX_TIMESTAMP(b.ActivityDateStart) 
							BETWEEN 
							UNIX_TIMESTAMP('".$startTime."') AND UNIX_TIMESTAMP('".$endTime."')
					Group By
							b.EventDateID";

$eventAttendanceInfoArr = $libenroll->returnResultSet($sql);

// Select event without attendance record
foreach((array)$eventAttendanceInfoArr as $_attendaceArr){
	if($_attendaceArr['numAttendance'] > 0){
	}
	else{
		$remindEventIDArr[] = $_attendaceArr['EnrolEventID'];
	}
}

if(count($remindEventIDArr)>0){
	// get event PIC
	$AcademicYearID = Get_Current_Academic_Year_ID();
				
	$sql = "SELECT
				iees.EnrolEventID,
				iees.UserID
			FROM
				INTRANET_ENROL_EVENTSTAFF as iees
			WHERE 
				iees.EnrolEventID IN ('".implode("','",(array)$remindEventIDArr)."')
				AND iees.StaffType = 'PIC' ";
	$eventStaffArr = $libenroll->returnResultSet($sql);
}

// Get Club & Event Info
$EnrolGroupInfoArr = BuildMultiKeyAssoc( $libenroll->Get_All_Club_Info(Get_Array_By_Key($groupStaffArr,'EnrolGroupID')) , 'EnrolGroupID');
$EnrolEventInfoArr = $libenroll->Get_Activity_Info_By_Id(Get_Array_By_Key($eventStaffArr,'EnrolEventID'));

// Email subject & content
$email_subject_setting = $Settings['AttendanceReminderSubject'];
$email_content_setting = $Settings['AttendanceReminderContent'];

foreach((array)$groupStaffArr as $InfoArr){
	$groupStaffAssoArr[$InfoArr['UserID']][] = $InfoArr['EnrolGroupID'];
}
foreach((array)$eventStaffArr as $InfoArr){
	$eventStaffAssoArr[$InfoArr['UserID']][] = $InfoArr['EnrolEventID'];
}

// loop through each recipient to prepare content and send
$recipientIDArr = array_unique(array_merge(Get_Array_By_Key($eventStaffArr,'UserID'),Get_Array_By_Key($groupStaffArr,'UserID')));
foreach((array)$recipientIDArr as $recipientID){
		
	$recipientEventStrArr[$recipientID] = array();
	foreach((array)$groupStaffAssoArr[$recipientID] as $GroupIDArr){
		$recipientEventStrArr[$recipientID][] = $EnrolGroupInfoArr[$GroupIDArr]['TitleChinese'];
	}
	foreach((array)$eventStaffAssoArr[$recipientID] as $EventIDArr){
		$recipientEventStrArr[$recipientID][] = $EnrolEventInfoArr[$EventIDArr]['EventTitle'];
	}
	
	$clubEventstr = implode(', ',$recipientEventStrArr[$recipientID]);
	
	$_recipentSubject = str_replace(array('[=ActivityName=]','[=AttendanceDate=]'),array($clubEventstr,$dateToCheck),$email_subject_setting);
	$_recipentContent = str_replace(array('[=ActivityName=]','[=AttendanceDate=]'),array($clubEventstr,$dateToCheck),$email_content_setting);
	$result[] = $lwebmail->sendModuleMail((array)$recipientID,$_recipentSubject,$_recipentContent);
}

?>
