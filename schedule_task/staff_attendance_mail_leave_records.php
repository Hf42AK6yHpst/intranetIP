<?php
// Editing by 
/*
 * 2012-0912-1247-40066 - 香港神託會培基書院  Stewards Pooi Kei College - Customization project - Staff Attendance System 
 * Cron-job to send today's staff leave records to group email staff@spkc.edu.hk
 * 
 * For testing, we can feed in parameters $ToEmail and $TargetDate to our own value. 
 */

$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_opendb();

if(!$sys_custom['StaffAttendance']['DailyAttendanceRecord']){
	exit;
}

$weekday = date("w");
if(!in_array($weekday,array(1,2,3,4,5))){ // Monday to Friday
	exit;
}

$StaffAttend3 = new libstaffattend3();
$StaffAttend3UI = new libstaffattend3_ui();

if(!isset($_REQUEST['TargetDate']) || $_REQUEST['TargetDate']==''){
	$TargetDate = date("Y-m-d");
}else{
	$TargetDate = $_REQUEST['TargetDate'];
}

if(!isset($_REQUEST['ToEmail']) || $_REQUEST['ToEmail']=='') {
	//$ToEmail = "staff@spkc.edu.hk";
	$ToEmail = "";
}else{
	$ToEmail = $_REQUEST['ToEmail'];
}

$success = $StaffAttend3UI->Send_Daily_Attendance_Record_Mail($TargetDate,$ToEmail);

if($success){
	echo "1|=|Staff Attendance leave records on $TargetDate were sent successfully.\n";
}else{
	echo "0|=|Failed to send Staff Attendance leave records on $TargetDate.\n";
}

intranet_closedb();
?>