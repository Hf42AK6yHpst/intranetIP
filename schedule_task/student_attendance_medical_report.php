<?php
// Editing by
/*

 */

$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");


intranet_opendb();

if(!$plugin['attendancestudent']){
	intranet_closedb();
	exit;
}

$lc = new libcardstudentattend2();

$csv_data = array();
$csv_data[] = array("date","sch_name","sch_edb","school type","grade","ili","gi","hfmd","other","casual leave","undef","total_stud");

$start_date = strtotime($StartDate);
$end_date = strtotime($EndDate);

if($start_date == false || $end_date == false) {
	intranet_closedb();
	exit;
}

$number_of_days = ($end_date - $start_date) / 86400;

$i_MedicalReasonName = array(); // reset $i_MedicalReasonName array and reload in lang.en.ip20.php
include("$intranet_root/lang/lang.en.ip20.php"); // use English as the report language
include("$intranet_root/lang/lang.en.php"); // use English as the report language

$medical_reason_list = array();
for($i=1;$i<sizeof($i_MedicalReasonName);$i++) {
	$medical_reason_list[] = $i_MedicalReasonName[$i];
}
$medical_reason_list[] = $i_MedicalReasonName[0];

$school_code = '';
for($i=0;$i<=$number_of_days;$i++) {
	$TargetDate = date("Y-m-d", strtotime($StartDate." +$i days"));
	$result = generateMedicalReasonSummary($lc, $TargetDate);
	if($result == -1) {
		continue;
	}
	$csv_data = array_merge($csv_data, $result);
}

$content = '';
foreach($csv_data as $temp) {
	$content .= '"'.implode('","', $temp).'"';
	$content .= "\r\n";
}

header("Content-type:application/csv;charset=UTF-8");
header('Content-Encoding: UTF-8');
header('Content-Description: File Transfer');
header('Content-Disposition: attachment; filename=student_attendance_medical_report_'.$school_code.'_'.$StartDate.'_'.$EndDate.'.csv');
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
//echo "\xEF\xBB\xBF"; // UTF-8 BOM
echo $content;

intranet_closedb();



function generateMedicalReasonSummary($lc, $TargetDate)
{
	global $school_code, $medical_reason_list, $intranet_root, $config_school_type, $BroadlearningClientName,$BroadlearningClientID, $i_MedicalReasonName, $config_school_code;

	$available_level = $lc->returnAvailableWebSamsLevel();

	$ts = strtotime($TargetDate);
	$year = date("Y",$ts);
	$month = date("m",$ts);
	$day = date("d",$ts);

	if ($lc->attendance_mode=="NO") {
		$tlc->retrieveSettings();
	}

	$lfcm = new form_class_manage();
	$AcademicYearID = $lfcm->Get_Academic_Year_And_Year_Term_By_Date($TargetDate);
	if(empty($AcademicYearID)) {
		return -1;
	}

	$AcademicYearID = $AcademicYearID[0]['AcademicYearID'];

	# Retrieve Class Level List
	$sql = "Select 
			 					yc.ClassTitleEN as ClassName, 
			 					y.WEBSAMSCode as WebSAMSLevel 
			 				From 
			 					YEAR as y 
			 					inner join 
			 					YEAR_CLASS as yc 
			 					on y.YearID = yc.YearID and yc.AcademicYearID = '".$AcademicYearID."' 
			       ";
	$classes = $lc->returnArray($sql,2);

	$none_websamslevel = true;

	unset($class_data);
	unset($result_count_total);
	unset($result_data);


	for ($i=0; $i<sizeof($classes); $i++)
	{
		list($t_classname, $t_webSamsClassLevel) = $classes[$i];
		if (in_array($t_webSamsClassLevel, $available_level))
		{
			$none_websamslevel = false;
			$class_data[$t_webSamsClassLevel][] = $t_classname;
		}
		else
		{
			# auto-map websams level if not set by school
			$t_classname_level_int = (int) str_replace("S", "", str_replace("F", "", str_replace("P", "", $t_classname)));
			if ($t_classname_level_int>0)
			{
				$classes[$i][1] = "S".$t_classname_level_int;
				$none_websamslevel = false;
				$class_data[$classes[$i][1]][] = $t_classname;
			}
			$other_class[] = $t_classname;
		}
	}

	if($none_websamslevel){ // no websams level has been set
		return -1;
	}

	foreach ($class_data as $t_webSamsLevel => $t_classArray)
	{
		$t_class_list = "'".implode("','",$t_classArray)."'";
		$sql = "SELECT COUNT(*) FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2) AND ClassName IN ($t_class_list)";

		$temp = $lc->returnVector($sql);
		$result_count_total[$t_webSamsLevel] = $temp[0];
	}

	# Get a list of absence
	$sql = "SELECT 
								user.UserID, 
								level.WEBSAMSCode, 
								day_log.AMStatus, 
								am_reason.MedicalReasonType,
			          day_log.PMStatus, 
			          pm_reason.MedicalReasonType, user.ClassName
			       	FROM 
			       		CARD_STUDENT_DAILY_LOG_".$year."_".$month." as day_log
								LEFT OUTER JOIN 
								INTRANET_USER as user 
								ON day_log.UserID = user.UserID
								LEFT OUTER JOIN 
								SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON as am_reason
						    ON day_log.UserID = am_reason.StudentID
						       AND am_reason.RecordDate = '$TargetDate'
						       AND am_reason.DayType = '".PROFILE_DAY_TYPE_AM."'
								LEFT OUTER JOIN 
								SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON as pm_reason
						    ON day_log.UserID = pm_reason.StudentID
						       AND pm_reason.RecordDate = '$TargetDate'
						       AND pm_reason.DayType = '".PROFILE_DAY_TYPE_PM."'
								LEFT OUTER JOIN 
								YEAR_CLASS as class 
								ON user.ClassName = class.ClassTitleEN and class.AcademicYearID = '".$AcademicYearID."' 
								LEFT OUTER JOIN 
								YEAR as level 
								ON level.YearID = class.YearID
							WHERE day_log.DayNumber = '$day'
								AND (day_log.AMStatus='".CARD_STATUS_ABSENT."' OR day_log.PMStatus='".CARD_STATUS_ABSENT."')
			           ";
	//debug_r($sql);
	$abs_data = $lc->returnArray($sql,7);

	for ($i=0; $i<sizeof($abs_data); $i++)
	{
		# auto-map websams level if not set by school
		list($t_userID, $t_webSamsLevel, $t_amStatus, $t_amReason, $t_pmStatus, $t_pmReason, $t_ClassName) = $abs_data[$i];
		if ($t_webSamsLevel==null or $t_webSamsLevel=="")
		{
			$t_classname_level_int = (int) str_replace("S", "", str_replace("F", "", str_replace("P", "", $t_ClassName)));
			$t_webSamsLevel = "P".$t_classname_level_int;
		}
		if ($lc->attendance_mode==0)
		{
			if ($t_amStatus == CARD_STATUS_ABSENT)
			{
				if($t_amReason > 0) {
					$result_data[$t_webSamsLevel][$t_amReason]++;
				}else{
					$result_data[$t_webSamsLevel][0]++; // Undefined
				}
			}
		}
		else if ($lc->attendance_mode==1)
		{
			if ($t_pmStatus == CARD_STATUS_ABSENT)
			{
				if($t_pmReason > 0) {
					$result_data[$t_webSamsLevel][$t_pmReason]++;
				}else{
					$result_data[$t_webSamsLevel][0]++; // Undefined
				}
			}
		}
		else if ($lc->attendance_mode==2 || $lc->attendance_mode==3)
		{
			if ($t_amStatus == CARD_STATUS_ABSENT && ($t_amReason != 0) && ($t_amReason != "") )  # AM Absent and with medical reason
			{
				if($t_amReason > 0) {
					$result_data[$t_webSamsLevel][$t_amReason]++;
				}else{
					$result_data[$t_webSamsLevel][0]++; // Undefined
				}
			}
			else if ($t_pmStatus==CARD_STATUS_ABSENT)  # PM Absent (As AM has not been set, just use PM medical reason)
			{
				if($t_pmReason > 0) {
					$result_data[$t_webSamsLevel][$t_pmReason]++;
				}else{
					$result_data[$t_webSamsLevel][0]++; // Undefined
				}
			}
			else if ($t_amStatus == CARD_STATUS_ABSENT) # AM Absent only, but no medical reason set
			{
				//echo "Part 3";
				$result_data[$t_webSamsLevel][0]++;
			}
		}
	}

	$sch_edb = $config_school_code;
	if($config_school_code == '' && $BroadlearningClientID != '') {
		$sch_edb = $BroadlearningClientID;
	}

	$school_code = $sch_edb;

	$result = array();
	for ($i=0; $i<sizeof($available_level); $i++)
	{
		$temp = array();
		$temp[] = $TargetDate;
		$temp[] = $BroadlearningClientName;
		$temp[] = $sch_edb;
		$sch_level = substr($available_level[$i], 0,1);
		$form_level = substr($available_level[$i],1,1);
		$temp[] = $sch_level;
		$temp[] = $form_level;

		for($j=0;$j<count($medical_reason_list);$j++) {
			$value = $result_data[$available_level[$i]][$medical_reason_list[$j][0]];
			if($value == "") {
				$value = 0;
			}
			$temp[] = $value;
		}
		$value = $result_count_total[$available_level[$i]];
		if($value == "") {
			$value = 0;
		}
		$temp[] = $value;

		$result[] = $temp;
	}

	return $result;
}
