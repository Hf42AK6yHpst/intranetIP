<?php
// Editing by 
/*
 * Note: 
 * If server host is 192.XXX.XXX.XXX, need to append &is_test=1 to this script url to force send.
 * Append &is_debug=1 to url to fakely send the data and just for previewing the result.  
 */
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_opendb();

$is_internal_site = false;
if(preg_match('/^192\..+$/', $_SERVER['SERVER_NAME']) || $_SERVER['SERVER_NAME'] == '127.0.0.1'){
	$is_internal_site = true;
}

if(!$sys_custom['eClassApp']['HKUSPH'] || ($is_internal_site && !$is_test)){
	intranet_closedb();
	exit;
}

$lc = new libcardstudentattend2();

$today_date = isset($_REQUEST['target_date'])? $_REQUEST['target_date'] : date("Y-m-d");
$today_date_ts = strtotime($today_date);
$today_day = date("j", $today_date_ts);
$send_all = in_array($today_day,array(1,16));

$target_date = isset($_REQUEST['target_date'])? $_REQUEST['target_date'] : date("Y-m-d", time()-86400); // get yesterday's data to submit
//$target_date_ts = strtotime($target_date);
//$day = date("j", $target_date_ts);

/*
$weekday = date("w", $target_date_ts);
if(in_array($weekday,array(0,6))){
	// skip Saturday and Sunday
	intranet_closedb();
	exit;
}
*/
if($send_all)
{
	$send_success = $lc->sendHKUFluData($today_date,$send_all);
	echo $send_success? "Weekly data sent.<br />" : "Weekly data sent failed.<br />";
}
/*
$holiday_count = $lc->returnVector("SELECT COUNT(*) FROM INTRANET_EVENT WHERE DATE_FORMAT(EventDate,'%Y-%m-%d')='$target_date' and RecordType IN (3,4)");
if($holiday_count[0] > 0){
	// skip holidays
	echo "Today is holiday.<br />";
	intranet_closedb();
	exit;
}
*/
/*
$year = date("Y",$target_date_ts);
$month = date("m",$target_date_ts);
$day = date("j",$target_date_ts);
$attendance_count = $lc->returnVector("SELECT COUNT(*) FROM CARD_STUDENT_DAILY_LOG_".$year."_".$month." WHERE DayNumber='$day'");
if($attendance_count[0] == 0){
	// no attendance records
	echo "No attendance record.<br />";
	intranet_closedb();
	exit;
}
*/
/*
$leave_count = $lc->returnVector("SELECT COUNT(*) FROM HKUSPH_SICK_LEAVE WHERE LeaveDate LIKE '".$target_date."%'");
if($leave_count[0] == 0){
	echo "No leave record.<br />";
	intranet_closedb();
	exit;
}
*/

//$is_debug = true;
$send_success = $lc->sendHKUFluData($target_date);
echo $send_success? "Data sent.<br />" : "Data sent failed.<br />";

intranet_closedb();
?>