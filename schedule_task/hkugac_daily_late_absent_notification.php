<?php
// Editing by 
/*
 * crontab script for HKUGAC
 */
$PATH_WRT_ROOT = "../";
include($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

if(!$sys_custom['StudentAttendance']['HKUGAC_LateAbsentEmailNotification']){
	echo 'Customization feature is not enabled.';
	exit;
}

intranet_opendb();

$lc = new libcardstudentattend2();
$lwebmail = new libwebmail();

$today = date("Y-m-d");
$academic_info = getAcademicYearAndYearTermByDate($today); 

$AcademicYearID = $academic_info['AcademicYearID'];	
$YearTermID = $academic_infp['YearTermID'];
$LateCount = 1;
$AbsentCount = 1;
$AndOr = 'OR';
$Keyword = '';
// get today's late/absent
$params = array('StudentIDToRecords'=>1,'StartDate'=>$today,'EndDate'=>$today,'AcademicYearID'=>$AcademicYearID,'YearTermID'=>$YearTermID,'YearClassID'=>'','LateCount'=>$LateCount,'AbsentCount'=>$AbsentCount,'AndOr'=>$AndOr,'Keyword'=>$Keyword);
$studentIdToRecords = $lc->getStudentLateAbsentCountingRecords($params);

// get total count of late/absent in this academic year term
$params = array('StudentIDToRecords'=>1,'AcademicYearID'=>$AcademicYearID,'YearTermID'=>$YearTermID,'YearClassID'=>'','LateCount'=>$LateCount,'AbsentCount'=>$AbsentCount,'AndOr'=>$AndOr,'Keyword'=>$Keyword);
$studentIdToTotalRecords = $lc->getStudentLateAbsentCountingRecords($params);

// find HOY group users which to be sent when number of late or number of absent reach 6 times
$sql = "SELECT 
			u.UserID 
		FROM INTRANET_GROUP as g 
		INNER JOIN INTRANET_USERGROUP as ug ON ug.GroupID=g.GroupID 
		INNER JOIN INTRANET_USER as u ON u.UserID=ug.UserID 
		WHERE u.RecordStatus='1' AND g.AcademicYearID='$AcademicYearID' AND g.Title='HOY'";
$HOY_group_user_ids = $lc->returnVector($sql);

ob_start();
include($intranet_root.'/home/eAdmin/StudentMgmt/attendance/other_features/late_absent_records/email_template.php');
$template = ob_get_clean();
$template = str_replace(array("\r\n","\n"),"",$template);

$email_title = $Lang['StudentAttendance']['HKUGACLateAbsentEmailTitle'];
$place_holders = array('<!--STUDENT_NAME-->','<!--CLASS_NAME-->','<!--CLASS_NUMBER-->','<!--EVENT_DATE-->','<!--LATE_COUNT-->','<!--ABSENT_COUNT-->','<!--RECORD-->');
$event_date = $today;
//debug_pr($studentIdToRecords);
$result = array();
if(count($studentIdToRecords)>0){
	foreach($studentIdToRecords as $student_id => $record){
		$year_class_id = $record[0]['YearClassID'];
		if($record[0]['LateCount'] == 0 && $record[0]['AbsentCount']==0){
			continue;
		}
		$year_class_obj = new year_class($year_class_id,$GetYearDetail=false,$GetClassTeacherList=true,$GetClassStudentList=false,$GetLockedSGArr=false);
		$teacher_list = $year_class_obj->ClassTeacherList;
		//debug_pr($year_class_obj);
		if(count($teacher_list)>0)
		{
			$teacher_user_ids = Get_Array_By_Key($teacher_list,'UserID');
			$to_array = $teacher_user_ids;
			if(($studentIdToTotalRecords[$student_id][0]['LateCount'] >= 6 || $studentIdToTotalRecords[$student_id][0]['AbsentCount'] >= 6) && count($HOY_group_user_ids)>0){
				$to_array = array_merge($to_array, $HOY_group_user_ids);
			}
			$record_detail = '';
			if($record[0]['LateCount'] > 0){
				$params = array('GetDetailRecords'=>1,'DetailRecordType'=>2,'StartDate'=>$today,'EndDate'=>$today,'AcademicYearID'=>$AcademicYearID,'YearTermID'=>$YearTermID,'YearClassID'=>$YearClassID,'LateCount'=>$LateCount,'AbsentCount'=>$AbsentCount,'AndOr'=>$AndOr,'Keyword'=>$Keyword);
				$late_records = $lc->getStudentLateAbsentCountingRecords($params);
				for($i=0;$i<count($late_records);$i++){
					$record_detail .= 'Lateness ('.$late_records[$i]['RecordDate'].' '.$late_records[$i]['LateTime'].')<br />';
				}
			}
			if($record[0]['AbsentCount'] > 0){
				$params = array('GetDetailRecords'=>1,'DetailRecordType'=>1,'StartDate'=>$today,'EndDate'=>$today,'AcademicYearID'=>$AcademicYearID,'YearTermID'=>$YearTermID,'YearClassID'=>$YearClassID,'LateCount'=>$LateCount,'AbsentCount'=>$AbsentCount,'AndOr'=>$AndOr,'Keyword'=>$Keyword);
				$absent_records = $lc->getStudentLateAbsentCountingRecords($params);
				for($i=0;$i<count($absent_records);$i++){
					$record_detail .= 'Absence ('.$absent_records[$i]['RecordDate'].')<br />';
				}
			}
			
			$replace_data = array($record[0]['StudentName'],$record[0]['ClassName'],$record[0]['ClassNumber'],$event_date,sprintf("%02d", $studentIdToTotalRecords[$student_id][0]['LateCount']),sprintf("%02d",$studentIdToTotalRecords[$student_id][0]['AbsentCount']),$record_detail);
			$email_title_student = ' - '.$record[0]['ClassName'].'('.$record[0]['ClassNumber'].') '.$record[0]['StudentName'];
			$email_content = $template;
			$email_content = str_replace($place_holders, $replace_data, $email_content);
			//echo $email_content;
			$result['send_for_'.$student_id.'_to_'.implode('_',$to_array)] = $lwebmail->sendModuleMail($to_array,$email_title.$email_title_student,$email_content);
		}
	}
}

$log_row = date("Y-m-d H:i:s").' '.$_SERVER['REQUEST_URI'].' '.$_SESSION['UserID'].' '.serialize($result);
$lc->log($log_row);

intranet_closedb();
?>