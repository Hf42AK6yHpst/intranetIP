<?php
// Editing by 
/*
 * 2017-04-26 (Carlos): This script is for: (1) auto activate user accounts for suspended and non-terminated users that have reach JoinDate. 
 * 											(2) auto suspend user accounts if reach TerminatedDate.
 */
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");

intranet_opendb();

$libdhl = new libdhl();

$libdhl->auotActivateSuspendUserAccounts();

intranet_closedb();
?>