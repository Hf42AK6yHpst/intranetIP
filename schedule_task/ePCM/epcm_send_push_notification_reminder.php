<?php
$WRT_ROOT= "{$_SERVER['DOCUMENT_ROOT']}/";
include_once($WRT_ROOT."includes/global.php");
include_once($WRT_ROOT."includes/libdb.php");
include_once($WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($WRT_ROOT."/includes/libemail.php");
include_once($WRT_ROOT."/includes/libsendmail.php");
include_once($WRT_ROOT."/includes/libwebmail.php");

intranet_opendb();



$libdb = new libdb();
$libeClassApp = new libeClassApp();

/**
 * Step 1: Check is cron job enable in general settings
 * Step 2: Get notification schedule for today
 * Step 3: Send Push Notification (eClassApp only)
 * Step 4: Send Email
 * Step 5: Update Record Status of the notification schedule in epcm
 */

echo '<br>ePCM cron job start <br>';

#### Step 1: Check is cron job enable in general settings
$sql = "SELECT *
		FROM GENERAL_SETTING WHERE Module = 'ePCM'";
$settingAry = $libdb->returnResultSet($sql);
$settingAry = BuildMultiKeyAssoc($settingAry,'SettingName');

$emailNotiEnabled = $settingAry['emailNotification']['SettingValue'];
$appNotiEnabled = $settingAry['eClassAppNotification']['SettingValue'];
$cronJobEnable = ($emailNotiEnabled||$appNotiEnabled)?1:0;
if($cronJobEnable){

	#### Step 2: Get notification schedule for today
	$sql = "Select * From INTRANET_PCM_NOTIFICATION_SCHEDULE AS ipns
				INNER JOIN INTRANET_PCM_NOTIFICATION_SCHEDULE_USER AS ipnsu 
				ON (ipns.CaseID=ipnsu.CaseID AND ipns.StageID = ipnsu.StageID) 
				WHERE ipns.IsDeleted = '0' AND DATE(ScheduledDate) = DATE(now()) AND RecordStatus = '1'";
	$scheduledPushMsgAry = $libdb->returnResultSet($sql);
	
	$countOfUser = count($scheduledPushMsgAry);
	echo $countOfUser.' user(s) would be notified today. <br>';
	
	
	
	foreach ($scheduledPushMsgAry as $_scheduledPushMsg){
		$caseID = $_scheduledPushMsg['CaseID'];
		$messageTitle = $_scheduledPushMsg['MessageTitle'];
		$messageContent = $_scheduledPushMsg['MessageContent'];
		
		
		$messageTitle=iconv("UTF-8","big5",$messageTitle);
		$messageContent=iconv("UTF-8","big5",$messageContent);
		
		
		if(isset($junior_mck)){
			/**
			 * UFT-8 => Big 5 before send
			 *
			 */
			$scheduledDateTime = $_scheduledPushMsg['ScheduledDate'];
			$receiverUser = $_scheduledPushMsg['UserID'];
		}
		
		#### Step 3: Send Push Notification (eClassApp only)
		if($appNotiEnabled){
			$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = array($receiverUser=>$receiverUser);
			$libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic="", $recordStatus=1, $appType='T', $sendTimeMode='scheduled', $scheduledDateTime, '', '', 'ePCM', $caseID);
			echo 'Push notifications are sent. <br>';
		}
		
		#### Step 4: Send Email
		if($emailNotiEnabled){
			$lwebmail = new libwebmail();
			$messageContent .= '<p>';
			$messageContent .= 'http://'.$_SERVER['HTTP_HOST'].'/home/eAdmin/ResourcesMgmt/eProcurement/index.php?p=mgmt.caseDetials.'.$caseID;
			$lwebmail->sendModuleMail((array) $receiverUser,$messageTitle,$messageContent,1,'', 'User');
			echo 'Email are sent. <br>';
		}
	}
	
	

	#### Step 5: Update Record Status of the notification schedule in epcm
	//Update recordStatus from 1 => 0
	$sql = "UPDATE INTRANET_PCM_NOTIFICATION_SCHEDULE
				SET RecordStatus = '0'
				WHERE IsDeleted = '0' AND DATE(ScheduledDate) = DATE(now()) AND RecordStatus = '1'";
	$libdb->db_db_query($sql);
	
	

}else{
	echo 'Cron Job is disabled, program exit';
}




?>