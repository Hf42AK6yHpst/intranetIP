<?php
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/groupMessage/libeClassApp_groupMessage_group.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/groupMessage/libeClassApp_groupMessage.php');

intranet_opendb();

if($sys_custom['eClassApp']['RemoveOneToOneGroupMessage'] || get_client_region() == 'zh_TW') {
	$leClassApp_groupMessage = new libeClassApp_groupMessage();

	$sql = "select * from INTRANET_APP_MESSAGE_GROUP where GroupType = 1  and RecordStatus = " . $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['RecordStatus']['active'];
	$rs = $leClassApp_groupMessage->returnResultSet($sql);
	foreach ($rs as $temp) {
		$_groupId = $temp['GroupID'];
		$_groupObj = new libeClassApp_groupMessage_group($_groupId);
		$_groupObj->setRecordStatus($eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['RecordStatus']['deleted']);

		$_groupId = $_groupObj->save();
		$saveLocal = ($_groupId > 0) ? true : false;

		if ($saveLocal) {
			$saveCloud = $_groupObj->saveCloud();
		}
	}
}
