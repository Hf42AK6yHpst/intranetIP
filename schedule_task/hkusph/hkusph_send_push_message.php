<?php

/*******************************
 * 2016-08-31 Henry HM
 * - Created file
 * 
 *******************************/

/*******************************
 * The overall sending conditions are:
 * (1) Students have to be agreed to attend HKUSPH; AND
 * (2) Students absent without any HKUSPH records after the first day of previous month; AND
 * (3) Students absent without any HKUSPH records after the agreement sign date; AND
 * (4) Students' parents are using parents app
 * 
 *******************************/

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/eClassAppConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/HKUFlu/HKUFlu.php");
include_once($PATH_WRT_ROOT."includes/HKUFlu/libHKUFlu_db.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_session_language.php");

// localhost job is duplicatedly run
if($_SERVER['SERVER_NAME'] == '127.0.0.1'){
	return;
}

$number_of_month = 2; //[1 .. 12]

//----------------------------------------------------------------------------------------------

intranet_opendb();

$luser = new libuser();
$db = new libdb();
$libeClassApp = new libeClassApp();
$db_hkuFlu = new libHKUFlu_db();
$log = array();

$log[] = '['.date('Y-m-d H:i:s').'] Cron Job Start. <br>';
//----------------------------------------------------------------------------------------------

# get student array with parent using parent app
$studentWithParentUsingParentAppAry = $luser->getStudentWithParentUsingParentApp();

//----------------------------------------------------------------------------------------------


$now_year = intval(date('Y'));
$now_month = intval(date('m'));

$start_year = $now_year;
$start_month = $now_month - ($number_of_month - 1);
if($start_month <= 0){
	$start_year--;
}

$rows_will_be_sent = array();
$y=$start_year;
$rows_will_be_sent[$y] = array();

for($m=$start_month;$m<=$now_month;$m++){
	$m_actual = (($m<=0)?($m+12):($m));
	$rows_will_be_sent[$y][$m] = array();
	
	### build card log table
	$card_log_table_name = 'CARD_STUDENT_DAILY_LOG_'.sprintf('%04d',$y).'_'.sprintf('%02d',$m_actual);
	
	$sql = 'SELECT `RecordID`, `UserID`, `DayNumber` FROM `'.$card_log_table_name.'` as `log`' .
			' WHERE (`log`.`AMStatus` = \''.CARD_STATUS_ABSENT.'\' OR `log`.`PMStatus` = \''.CARD_STATUS_ABSENT.'\')' .
					' AND `log`.`RecordID` NOT IN (SELECT `DailyLogRecordID` FROM `HKUSPH_ABSENT_PUSH_MESSAGE_SENT_QUEUE` `q` WHERE `q`.`DailyLogYear` = \''.$y.'\' AND `DailyLogMonth` = \''.$m_actual.'\')';
//					$log[] = $sql.'<br/>';
	$rows_log = $db->returnResultSet($sql);

	foreach((array)$rows_log as $row_log){
		if(!is_array($rows_will_be_sent[$y][$m][$row_log['DayNumber']])){
			$rows_will_be_sent[$y][$m][$row_log['DayNumber']] = array();
		}
		array_push($rows_will_be_sent[$y][$m][$row_log['DayNumber']], $row_log);
	}
	
	unset($rows_log);
	
	if($m_actual>=12){
		$y++;
	}
}

$sent_count = 0;
foreach((array)$rows_will_be_sent as $y => $rows_m){
	foreach((array)$rows_m as $m => $rows_d){
		$m_actual = (($m<=0)?($m+12):($m));
		foreach((array)$rows_d as $d => $rows_log){
			$message_date = sprintf('%04d-%02d-%02d',$y,$m_actual,$d);
			if($message_date<=date('Y-m-d')){
				$array_agreed_student_id = $db_hkuFlu->getAgreedStudentIDAfterSignDate($message_date);
	
				$array_student_id = array();
				foreach((array)$rows_log as $row_log){
					if(count($db_hkuFlu->getStandAloneSickLeaveByStudentIDAndDate(array($row_log['UserID']), $message_date)) <= 0){
						array_push($array_student_id, $row_log['UserID']);
					}
				}
				$array_student_id_to_be_sent = array_intersect($array_student_id, $studentWithParentUsingParentAppAry, $array_agreed_student_id);
				
				foreach((array)$array_student_id_to_be_sent as $student_id_to_be_sent){
					$log[] = '['.date('Y-m-d H:i:s').'] Parents of Student ID = '.$student_id_to_be_sent.' will be notified for the absent date '.$message_date.'. <br>';
				}
				
				if(count($array_student_id_to_be_sent) > 0){
					/* signal message for all parent
					//send push message
					$messageTitle = $Lang['eClassApp']['HKUFlu']['PushMessageTitle'];
					$messageContent = $Lang['eClassApp']['HKUFlu']['PushMessageContent'].$message_date.'';
					
					$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($array_student_id_to_be_sent), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
					$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
					$notification_id = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic="", $recordStatus=1, $appType=$eclassAppConfig['appType']['Parent'], $sendTimeMode=$eclassAppConfig['pushMessageSendMode']['now'], $scheduledDateTime = '', '', '', $eclassAppConfig['moduleCode']['hkuFlu'], str_replace('-','',$message_date));
					if($notification_id > 0){
						$log[] = '['.date('Y-m-d H:i:s').'] Push notifications of '.$message_date.' are sent. <br>';
						$sent_count++;
					}else{
						$log[] = '['.date('Y-m-d H:i:s').'] Push notifications of '.$message_date.' are FAILED to send. <br>';
					}
					
					if($notification_id > 0){
						foreach((array)$rows_log as $row_log){
							$sql = 'INSERT INTO `HKUSPH_ABSENT_PUSH_MESSAGE_SENT_QUEUE`(`DailyLogYear`,`DailyLogMonth`,`DailyLogRecordID`,`SentDate`) VALUES(\''.$y.'\',\''.$m_actual.'\',\''.$row_log['RecordID'].'\',NOW());';
							$db->db_db_query($sql);
						}
					}
					/**/
					
					//send push message
					$array_parent_student = $luser->getParentStudentMappingInfo($array_student_id_to_be_sent);
					
					$i = 0;
					$_messageTitle = $Lang['eClassApp']['HKUFlu']['PushMessageTitle'];
					$messageContent = 'MULTIPLE MESSAGES';
					$individualMessageInfoAry = array();
					foreach($array_parent_student as $parent_student) {
						$_parentId = $parent_student['ParentID'];
						$_messageTitle = $Lang['eClassApp']['HKUFlu']['PushMessageTitle'];
						$_messageContent = str_replace(array('%NAME%','%DATE%'),array($parent_student['StudentName'],$message_date),$Lang['eClassApp']['HKUFlu']['PushMessageContent']);
						$_relatedToUserId = $parent_student['StudentID'];
						
						$_individualMessageInfoAry = array();
						$_individualMessageInfoAry['relatedUserIdAssoAry'][$_parentId] = array($_relatedToUserId);
						$_individualMessageInfoAry['messageTitle'] = $_messageTitle;
						$_individualMessageInfoAry['messageContent'] = $_messageContent;
						
						$individualMessageInfoAry[$i] = $_individualMessageInfoAry;
						$i++;
					}
					
					$notification_id = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle=$_messageTitle, $messageContent, $isPublic='', $recordStatus=1, $appType=$eclassAppConfig['appType']['Parent'], $sendTimeMode=$eclassAppConfig['pushMessageSendMode']['now'], $sendTimeString = '', '', '', $eclassAppConfig['moduleCode']['hkuFlu'], str_replace('-','',$message_date));
					
					if($notification_id > 0){
						$log[] = '['.date('Y-m-d H:i:s').'] Push notifications of '.$message_date.' are sent. <br>';
						$sent_count++;
					}else{
						$log[] = '['.date('Y-m-d H:i:s').'] Push notifications of '.$message_date.' are FAILED to send. <br>';
					}
					
					if($notification_id > 0){
						foreach((array)$rows_log as $row_log){
							$sql = 'INSERT INTO `HKUSPH_ABSENT_PUSH_MESSAGE_SENT_QUEUE`(`DailyLogYear`,`DailyLogMonth`,`DailyLogRecordID`,`SentDate`) VALUES(\''.$y.'\',\''.$m_actual.'\',\''.$row_log['RecordID'].'\',NOW());';
							$db->db_db_query($sql);
						}
					}
				}
			}
		}
	}
}

if($sent_count <= 0){
	$log[] = '['.date('Y-m-d H:i:s').'] No push notifications have to be sent. <br>';
}
$log[] = '['.date('Y-m-d H:i:s').'] Cron Job End. <br>';

//write log file
$str_log = implode('',$log);
$log_folder = $PATH_WRT_ROOT.'file/hkusph/';
if(!is_dir($log_folder)){
	mkdir($log_folder);
}
file_put_contents ($log_folder.'hkusph_log_'.date('Y_m').'.log',  str_replace('<br>',"\n", $str_log), FILE_APPEND);
echo $str_log;


//----------------------------------------------------------------------------------------------

?>