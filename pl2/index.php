<?php
$BLOCK_LIB_FOR_PL2 = true;
include __DIR__.'/../includes/global.php';

if (isset($web_protocol) && preg_match('/^https?$/', $web_protocol)===1){
    $protocol = $web_protocol;
} else {
    $protocol = 'http';
}

$url = $protocol . '://' . trim($eclass40_httppath, '/') . '/pl2/';

header("Location: $url");
exit;