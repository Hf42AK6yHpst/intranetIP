<?php

include __DIR__.'/../includes/global.php';

include_once(__DIR__.'/../includes/libdb.php');
include_once(__DIR__.'/../includes/libuser.php');

intranet_opendb();

$luser = new libuser($_SESSION['UserID']);

$sessionKey = $luser->sessionKey;
$sessionKeepAliveTime = $session_expiry_time * 60;

$defaultLang = '';
switch($intranet_session_language){
    case 'b5':
        $defaultLang = 'zh-hk';
        break;
    case 'gb':
        $defaultLang = 'zh-cn';
        break;
    default:
        $defaultLang = 'en';
}

$platformType    = 'intranet';
$platformVersion = Get_IP_Version();

if (isset($web_protocol) && preg_match('/^https?$/', $web_protocol)===1){
    $protocol = $web_protocol;
} else {
    $protocol = 'http';
}

$powerLesson2_path = $protocol . '://' . $eclass40_httppath;
$powerLesson2_path = trim($powerLesson2_path, '/')."/src/powerlesson/portal/login.php?enableUrlRedirect=1";

?>
<html>
<body onload="document.powerLessonLogin.submit()">
    <form id="powerLessonLogin" name="powerLessonLogin" style="display:none;" action="<?=$powerLesson2_path ?>" method="POST">
        <input type="hidden" name="sessionKey" value="<?=$sessionKey ?>" />
        <input type="hidden" name="sessionKeepAliveTime" value="<?=$sessionKeepAliveTime ?>" />
        <input type="hidden" name="defaultLang" value="<?=$defaultLang ?>" />
        <input type="hidden" name="platformType" value="<?=$platformType ?>" />
        <input type="hidden" name="platformVersion" value="<?=$platformVersion ?>" />
        <input type="hidden" name="directLogin" value="1" />
    </form>
</body>
</html>