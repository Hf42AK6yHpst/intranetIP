<?php
// Editing by 
/*
 * 2018-11-20 Carlos: Fix checking on student helper and non-teaching staff.
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");


intranet_auth();
intranet_opendb();

if(!isset($TargetDate) || $TargetDate == ''){
	$TargetDate = date("Y-m-d");
}

if(isset($_REQUEST['class_name']))
{
	$class_name = rawurldecode($_REQUEST['class_name']);
}

$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();

$luser = new libuser($_SESSION['UserID']);
$isStudentHelper = false;
$isStudentHelper= $luser->isStudent();

$ladminjob = new libadminjob($_SESSION['UserID']);
$isSmartAttendanceAdmin = $ladminjob->isSmartAttendenceAdmin();

if ($luser->isStudent())
{
	$class_name_that_student_can_take = '';
	if($isSmartAttendanceAdmin){
  		$class_name_that_student_can_take = $luser->ClassName;
	}
}

$class_list = $lcardattend->getClassListToTakeAttendanceByDate($TargetDate,$luser->isTeacherStaff()? $_SESSION['UserID'] : '');

$disallow_non_teaching_staff_take_attendance = $lcardattend->DisallowNonTeachingStaffTakeAttendance && $luser->isTeacherStaff() && $luser->teaching!=1;

### Class select
$class_select = "<SELECT id=\"class_name\" name=\"class_name\" onChange=\"retrivePMTime();\">";
$empty_selected = ($class_name == '')? "SELECTED":"";
$class_select .= "<OPTION value='' $empty_selected> -- $button_select -- </OPTION>\n";
for($i=0; $i< sizeof($class_list); $i++)
{
	if($_SESSION['UserType']==USERTYPE_STUDENT && $class_name_that_student_can_take != $class_list[$i][3]){ 
		// restrict student helper can only take own class
		continue;
	}
	if($disallow_non_teaching_staff_take_attendance){
		continue;
	}
	
	if ($class_list[$i][2] != $class_list[$i-1][2]) {
		if ($i == 0) 
			$class_select .= '<optgroup label="'.$class_list[$i][2].'">';
		else {
			$class_select .= '</optgroup>';
			$class_select .= '<optgroup label="'.$class_list[$i][2].'">';
		}
	}
	list($itr_class_id, $itr_class_name,$YearName,$itr_class_name_en) = $class_list[$i];
	$selected = ($itr_class_name_en==$class_name)?"SELECTED":"";
	$class_select .= "<OPTION value=\"".$itr_class_name_en."\" $selected>".$itr_class_name."</OPTION>\n";
	
	if ($i == (sizeof($class_list)-1)) 
		$class_select .= '</optgroup>';
}
$class_select .= "</SELECT>\n";

echo $class_select;

intranet_closedb();
?>