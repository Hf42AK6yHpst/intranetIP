<?php
// Editing by 
##################################### Change Log #####################################################
# 2019-10-15 Ray: Added session From/To
# 2019-01-21 Carlos: Added [Approval Time] and [Approval User].
# 2019-01-02 Carlos: Added [Last Updated Time].
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

// Only allow teacher to access
$luser = new libuser($_SESSION['UserID']);
if(!$luser->isTeacherStaff()){
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();

$filter = $filter==""?2:$filter;
$today = date('Y-m-d');
switch($filter){
	case 1: $cond_filter = " AND a.RecordDate <='$today' "; break;
	case 2: $cond_filter = " AND a.RecordDate >'$today' "; break;
	case 3: $cond_filter = ""; break;
	default: $cond_filter =" AND a.RecordDate > '$today' ";
}

$has_apply_leave = $plugin['eClassApp']? true : false;

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;
if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
	$field_array = array("a.RecordDate", "a.DayPeriod", "a.SessionFrom", "a.SessionTo", "a.Reason", "Waive", "a.Remark", "a.DateModified");
} else {
	$field_array = array("a.RecordDate", "a.DayPeriod", "a.Reason", "Waive", "a.Remark", "a.DateModified");
}
if($has_apply_leave){
	$field_array[] = "a.ApprovedAt";
	$field_array[] = "ApprovedBy";
}
$order_str = " ORDER BY ".$field_array[$field];
$order_str.= ($order==1)?" ASC " : " DESC ";

if($studentID!=""){
	$lb = new libdb();
	$namefield = getNameFieldWithClassNumberByLang("");
	$sql ="SELECT $namefield FROM INTRANET_USER WHERE UserID='$studentID' AND RecordType=2 AND RecordStatus IN (0,1,2)";
	$temp = $lb->returnVector($sql);
	$student_name = $temp[0];
	
	$more_fields = "";
	$more_joins = "";
	if($has_apply_leave){
		$more_joins .= " LEFT JOIN INTRANET_USER as u ON u.UserID=a.ApprovedBy ";
		$approved_by_namefield = getNameFieldByLang("u.");
		$more_fields .= " ,a.ApprovedAt,$approved_by_namefield as ApprovedBy ";
	}

	$session_fields = "";
	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$session_fields .= " a.SessionFrom, a.SessionTo, ";
	}

	$sql=" SELECT a.RecordDate,
				 IF(a.DayPeriod=2,'$i_DayTypeAM','$i_DayTypePM'),
				 $session_fields
				 a.Reason,
				 IF(a.Waive = '1' AND a.Waive IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as Waive,
				 IF(a.DocumentStatus = '1' AND a.DocumentStatus IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as DocumentStatus,
				 a.Remark,
				 a.DateModified 
					$more_fields 
				FROM CARD_STUDENT_PRESET_LEAVE AS a $more_joins
				 WHERE a.StudentID='$studentID'  $cond_filter AND 
				(a.RecordDate LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				 $order_str ";

	$temp = $lb->returnArray($sql);
	
	$lexport = new libexporttext();

	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$exportColumn = array("$i_Attendance_Date", "$i_Attendance_DayType", $Lang['StudentAttendance']['SessionFrom'], $Lang['StudentAttendance']['SessionTo'], "$i_Attendance_Reason", $Lang['StudentAttendance']['Waived'], $Lang['StudentAttendance']['ProveDocument'], "$i_Attendance_Remark", $Lang['General']['LastUpdatedTime']);
	} else {
		$exportColumn = array("$i_Attendance_Date", "$i_Attendance_DayType", "$i_Attendance_Reason", $Lang['StudentAttendance']['Waived'], $Lang['StudentAttendance']['ProveDocument'], "$i_Attendance_Remark", $Lang['General']['LastUpdatedTime']);
	}

	if($has_apply_leave){
		$exportColumn[] = $Lang['StudentAttendance']['ApprovalTime'];
		$exportColumn[] = $Lang['StudentAttendance']['ApprovalUser'];
	}
	$export_content = $lexport->GET_EXPORT_TXT($temp, $exportColumn);
	
	$export_content_final = "$i_SmartCard_DailyOperation_Preset_Absence - $student_name\n\n";
	$export_content_final .= $export_content;
	
}

intranet_closedb();

$filename = "preset_absence_".$student_name.".csv";
$lexport->EXPORT_FILE($filename, $export_content_final);
?>