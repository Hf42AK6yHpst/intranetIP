<?php
##################################### Change Log #####################################################
#
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

// Only allow teacher to access
$luser = new libuser($_SESSION['UserID']);
if(!$luser->isTeacherStaff()){
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();

$return_url = $return_url==""?"edit.php":$return_url;

if($RecordID=="" || sizeof($RecordID)<=0)
	header("Location: $return_url");

$sql="UPDATE CARD_STUDENT_PRESET_LEAVE SET 
				Reason ='$reason' , 
				Waive = '$Waive', 
				DocumentStatus = '$Handin_Prove',
				Remark = '$remark' 
			WHERE RecordID='$RecordID'";

$lc = new libcardstudentattend2();
$Result = $lc->db_db_query($sql);	

intranet_closedb();

$t = explode("?",$return_url);
$filename = $t[0];
$str = $t[1];
$ary = explode("&",$str);
$found = false;

if ($Result) 
	$Msg = $Lang['StudentAttendance']['PresetAbsenceUpdateSuccess'];
else 
	$Msg = $Lang['StudentAttendance']['PresetAbsenceUpdateFail'];

for($i=0;$i<sizeof($ary);$i++){
	if(strpos($ary[$i],"Msg=") != false) {
		$ary[$i]="Msg=".urlencode($Msg);
		$found = true;
		break;
	}
}

if(!$found){
	array_push($ary,"Msg=".urlencode($Msg));
}

$return_url = $filename."?".implode("&",$ary);

header("Location: $return_url");
?>
