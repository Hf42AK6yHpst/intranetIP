<?php
ini_set("memory_limit", "500M");
// Editing by YatWoon
/*
##################################### Change Log ###############################################
# 2019-11-08 by Ray: Add $sys_custom['SmartCardAttendance_StudentAbsentSessionToggleSessionIgnoreConfirm']
# 2019-09-24 by Carlos: Fixed PM only mode display wrong in time and in station fields for PM session.
# 2019-06-19 by Ray   : Modified teacher_remark_js read from database
# 2019-06-13 by Carlos: Added primary key to temporary table TEMP_STUDENT_PHOTO_LINK to improve query performance.
# 2019-03-27 by Carlos: [ip.2.5.10.4.1] Toggle display of mouse over tooltip image with showHideTooltipPhoto(mouseOverObj,isShow) which use jquery position api.
# 2018-10-22 by Carlos: [ip.2.5.9.11.1] Added cust flag $sys_custom['SmartCardAttendance_SubmittedProveDocumentForEarlyLeave'] for ticking the submitted prove document status of early leave.
# 2018-08-03 by Carlos: [ip.2.5.9.10.1] Apply setting [iSmartCardDisableModifyStatusAfterConfirmed] to disable status change for confirmed attendance records.
# 2018-03-12 by Carlos: Check setting TeacherCanManageProveDocumentStatus to show/hide [Submitted Prove Document] option.
# 2018-01-30 by Carlos: Fine tune group admin checking, if a group hasn't set any admin users, default staff can take it, student can only take is group admin groups.
# 2017-12-15 by Carlos: [ip.2.5.9.1.1] Fixed PM to follow AM Teacher's remark. 
# 2017-10-26 by Carlos: [ip.2.5.9.1.1] Filter attendance groups that can be taken by current user with checking on Group Responsible Users.
# 2017-05-26 by Carlos: [ip.2.5.8.7.1] $sys_custom['StudentAttendance']['RecordBodyTemperature'] display body temperature.
# 2017-05-17 by Carlos: [ip.2.5.8.7.1] $sys_custom['StudentAttendance']['NoCardPhoto'] added no bring card photo.
# 2017-04-12 by Carlos: [ip.2.5.8.4.1] Allow manage [Waived], [Reason] and [Teacher's remark] according to settings, added [Submitted Prove Document], added teacher's remark selection.
# 2016-10-18 by Carlos: [ip2.5.7.10.1] Added checking on [disallow non-teaching staff taking attendance].
# 2016-10-17 by Carlos: [ip2.5.7.10.1] Improved to pre-display outing remark if have preset outing record.
# 2016-09-29 by Carlos: [ip2.5.7.10.1] Improved to display teaching subject groups only if setting [Class teacher take own class(es) attendance at iSmartCard] is on.  
# 2016-07-05 by Carlos: [ip2.5.7.7.1] $sys_custom['StudentAttendance']['AllowEditTime'] - allow edit in time.
# 2016-06-01 by Carlos: [ip2.5.7.7.1] Modified Lesson Attendance status selection to cater more status for customization. And added reason input.
# 2016-03-23 by Carlos: [ip2.5.7.4.1] Added change all status tool button.
# 2016-03-11 by Carlos: [ip2.5.7.4.1] $sys_custom['StudentAttendance']['PMStatusFollowLunchSetting'] - customized to make students that do not go out for lunch 
#										to set PM status to follow AM status because these students do not tap card in lunch time to trigger the Pm in status.
# 2016-03-08 by Carlos: [ip2.5.7.3.1] Modified js PrintPage(AttendOverviewID,LessonDate) to get SubjectGroupID from hidden element value.
# 2015-11-06 by Carlos: [ip2.5.7.1.1] Fixed Outing and Early Leave duplicated records bug.
# 2015-10-22 by Carlos: Display Last Modified info under time slot selection.
# 2015-10-14 by Carlos: Cater official photo to be consistent with teacher app. 
# 2015-10-02 by Carlos: Added background color to the period AM/PM radio buttons for easier recognition. 
# 2015-08-13 by Carlos: Added js reloadClassSelection(), reloadGroupSelection() and onDateValueChanged() to reload class/group selections when target date changed.
# 2015-07-14 by Carlos: Disable all editable fields if disallow to edit n days before records. 
# 2015-03-31 by Carlos: Display preset absence remark to Office remark and do not allow edit, not to Teachers's remark. 
# 2015-03-03 by Carlos: Added js checkTargetDateChanged() which called in view_class() to handle settings of cannot take past/future date records.
# 2015-02-13 by Carlos: Added [Office Remark].
# 2014-09-22 by Carlos: Modified Subject Group no need to follow time table to take attendance. 
#						Use $sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable'] can fallback to original design, i.e. follow time table to take attendance
# 2014-07-11 by Carlos: Modified js view_class(), if setting CannotTakeFutureDateRecord is on, disallow take attendance in future days
# 2014-03-17 by Carlos: Added Absent Session for $sys_custom['SmartCardAttendance_StudentAbsentSession2']
# 2013-11-21 by Carlos: Modified js retrivePMTime(), disable auto change period if $sys_custom['StudentAttendance']['DisableAutoChangePeriod'] on
# 2013-09-03 by Carlos: inner join YEAR_CLASS_USER, YEAR_CLASS to strictly get current year students
# 2013-08-29 by Carlos: Add LessonType option [Own Lessons] or [All Lessons] if allow teachers to take all lessons
# 2013-08-23 by Carlos: Only display lesson attendance if no student attendance
# 2013-08-20 by Carlos: Display "You have no lesson to take. " if no session to select
# 2013-08-13 by Henry : Change to use html version of the lesson attendance form
# 2013-04-16 by Carlos: Add CARD_STUDENT_DAILY_REMARK.DayType
# 2013-02-26 by Carlos: Change [step 1] period selection to radio checkboxes 
# 2013-02-07 by Carlos: If preset absent record's remark is set, display as teacher's remark
# 2012-03-16 by Carlos: PM display PMDateModified and PMModifyBy
# 2011-11-16 by Carlos: Get LeaveStatus and LeaveSchoolTime from sql
# 2011-11-04 by Carlos: Modified to use js show alert and redirect to portal if no need to take attendance on selected date 
# 2011-11-03 by Carlos: Fix class name problem, pls only use english class name as value
# 2011-05-05 by YatWoon: Add checking, if "On time" no need to display reason for selection
#
# 2010-05-10 by Kenneth Chung: Add labels on selecting "school" or "lesson", also, 
															 add few more <br> after the view button of lesson attendance to 
															 avoid calendar being covered by the flash
# 2010-04-23 by Carlos: Allow take attendance of past dates
# 2010-04-22 by Carlos: Allow input late session if student is late
# 2010-03-17 by Kenneth Chung: add condition of EnableEntryLeavePeriod setting
# 2010-03-08 by Kenneth Chung: Move Get Default PM Status Logic to libcardstudentattend2.php
# 2010-02-09 by Carlos: Add setting Default PM status not to follow AM status
################################################################################################
*/
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

if($LessonDate){
	$ToDate = $LessonDate;
}
if(!isset($ToDate) || $ToDate=="")
{
	$ToDate=date('Y-m-d');
}
$ts_record = strtotime($ToDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
//$toDay=date('Y-m-d');

$page_load_time = time()+1;
if(!$plugin['attendancestudent'] && !$plugin['attendancelesson'])
{
		intranet_closedb();
        header("Location: $intranet_httppath/");
        exit();
}

if($sys_custom['StudentAttendance']['HideSmartCardTakeAttendance']) {
	intranet_closedb();
	header("Location: attendance_record/search_report.php");
	exit();
}

$lcardattend = new libcardstudentattend2();

if(method_exists($lcardattend,'getAttendanceGroupRecords')){
	$attendance_groups = $lcardattend->getAttendanceGroupRecords(array());
	$responsible_user_groups = $lcardattend->getAttendanceGroupAdminUsers(array('GroupID'=>Get_Array_By_Key($attendance_groups,'GroupID'),'UserID'=>$_SESSION['UserID']));
	$responsible_user_group_ids = Get_Array_By_Key($responsible_user_groups,'GroupID');
	
	if($_SESSION['UserType'] == USERTYPE_STAFF){
		// get all group admins with group id as key to admin users
		$groupIdToAdminUsers = $lcardattend->getAttendanceGroupAdminUsers(array('GroupID'=>Get_Array_By_Key($attendance_groups,'GroupID'),'GroupIDToUsers'=>1));
		// go through all groups to see any admin users added to the group
		for($i=0;$i<count($attendance_groups);$i++){
			if(!isset($groupIdToAdminUsers[$attendance_groups[$i]['GroupID']]) && !in_array($attendance_groups[$i]['GroupID'],$responsible_user_group_ids)){
				// if the group hasn't set any admin, staff can take it
				$responsible_user_group_ids[] = $attendance_groups[$i]['GroupID'];
			}
		}
	}
	
	$responsible_user_group_id_size = count($responsible_user_group_ids);
}

$ladminjob = new libadminjob($_SESSION['UserID']);
$isSmartAttendanceAdmin = $ladminjob->isSmartAttendenceAdmin();
if (!($isSmartAttendanceAdmin || $responsible_user_group_id_size>0))
{
	intranet_closedb();
    header ("Location: /");
    exit();
}


// get default attend status
// get default status from basic settings

$GeneralSetting = new libgeneralsettings();
/*
$SettingList[] = "'DefaultAttendanceStatus'";
$SettingList[] = "'PMStatusNotFollowAMStatus'";
$SettingList[] = "'CannotTakeFutureDateRecord'";
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
*/
$Settings = $lcardattend->Settings;
//$student_attend_default_status = trim(get_file_content("$intranet_root/file/studentattend_default_status.txt"));
$student_attend_default_status = $Settings['DefaultAttendanceStatus'];//($student_attend_default_status == "")? CARD_STATUS_ABSENT:$student_attend_default_status;
$CannotTakeFutureDateRecord = $Settings['CannotTakeFutureDateRecord'];

$LessonAttendanceSettingList = array("'TeacherCanTakeAllLessons'");
$LessonAttendanceSettings = $GeneralSetting->Get_General_Setting('LessonAttendance',$LessonAttendanceSettingList);

$luser = new libuser($_SESSION['UserID']);
$isStudentHelper = false;
$isStudentHelper= $luser->isStudent();

$lcardattend->retrieveSettings();
$lcardattend->createTable_LogAndConfirm($txt_year,$txt_month);
/*
if ($lcardattend->attendance_mode==1)
{
    header("Location: index.php");
    exit();
}
*/

if($lcardattend->DisallowNonTeachingStaffTakeAttendance && $luser->isTeacherStaff() && $luser->teaching!=1){
	 intranet_closedb();
	 header ("Location: /");
     exit();
}

if($sys_custom['SmartCardAttendance_StudentAbsentSession'] || $sys_custom['SmartCardAttendance_StudentAbsentSession2']){
	$DefaultNumOfSessionSettings = $lcardattend->getDefaultNumOfSessionSettings();
}

$linterface         = new interface_html();
$lsmartcard			= new libsmartcard();
$lteaching = new libteaching($_SESSION['UserID']);
$CurrentPage        = "PageTakeAttendance";



if ($luser->isStudent())
{
	if($isSmartAttendanceAdmin){
  		$class_name_that_student_can_take = $luser->ClassName;
	}
}

$class_list = $lcardattend->getClassListToTakeAttendanceByDate($ToDate,$luser->isTeacherStaff()? $_SESSION['UserID'] : '');
$group_list = $lcardattend->getGroupListToTakeAttendanceByDate($ToDate);
$SubjectGroupSessionList = $lcardattend->Get_Subject_Group_Session($_SESSION['UserID'],$ToDate, "", ($LessonAttendanceSettings['TeacherCanTakeAllLessons']==1 && isset($LessonType))? $LessonType : 0);

if($sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable']){
	$SubjectGroupList = $lcardattend->Get_Subject_Group_Session($_SESSION['UserID'],$ToDate,true);
	//$TeachingClassList = $lteaching->returnTeacherClass($_SESSION['UserID']);
}else{
	$scm_ui = new subject_class_mapping_ui();
	$fcm = new form_class_manage();
	
	$YearTermInfo = $fcm->Get_Academic_Year_And_Year_Term_By_Date($ToDate);
	$YearTermID_ = $YearTermInfo[0]['YearTermID'];
	if($lcardattend->ClassTeacherTakeOwnClassOnly)
	{
		$scm = new subject_class_mapping();
		$TeachingOnly_ = 1;
		if ($YearTermID == '')
		{
			$YearTermArr = getCurrentAcademicYearAndYearTerm();
			$YearTermID = $YearTermArr['YearTermID'];
		}
		$tmp_subject_group_list = $scm->Get_Subject_Group_List($YearTermID, $SubjectID, $returnAssociate=0, $TeachingOnly_);
		$IncludeSubjectIDArr_ = array_values(array_unique( Get_Array_By_Key( $tmp_subject_group_list, 'SubjectID') ));
	}else{
		$TeachingOnly_ = 0;
		$IncludeSubjectIDArr_ = '';
	}
	$SubjectSelect = $scm_ui->Get_Subject_Selection("SubjectID", $SubjectID, 'subjectChanged(this.options[this.selectedIndex].value);', $noFirst_='0', $firstTitle_=$button_select, $YearTermID_='', $OnFocus_='', $FilterSubjectWithoutSG_=1, $IsMultiple_=0, $IncludeSubjectIDArr_);
	$SubjectGroupSelect = $scm_ui->Get_Subject_Group_Selection($SubjectID, "SubjectGroup", $SubjectGroup, $OnChange_='', $YearTermID, $IsMultiple_=0, $AcademicYearID_='', $noFirst_=0, $DisplayTermInfo_=0, $TeachingOnly_);
}

$EditDaysBeforeRecord = $lcardattend->EditDaysBeforeRecord == 1;
$EditDaysBeforeRecordSelect = $lcardattend->EditDaysBeforeRecordSelect;
$IsEditDaysBeforeDisabled = false;
if($EditDaysBeforeRecord)
{
	$ts_target_date = strtotime($ToDate);
	$ts_today = strtotime(date("Y-m-d"));
	$ts_edit_days_before = $ts_today - $EditDaysBeforeRecordSelect * 86400;
	$IsEditDaysBeforeDisabled = $ts_target_date <= $ts_edit_days_before; 
}

/*
if($class_name=="")
{
        list($itr_class_id, $itr_class_name) = $class_list[0];
        $class_name = $itr_class_name;
}
*/

### Period setting
//if($period=="")        $period = "AM";
switch ($period){
  case "AM": 
  	$display_period = $i_DayTypeAM;
  	$DayType = PROFILE_DAY_TYPE_AM;
  	break;
  case "PM": 
  	$display_period = $i_DayTypePM;
  	$DayType = PROFILE_DAY_TYPE_PM;
  	break;
  default : 
  	if ($luser->isStudent()) {  		
  		if ($lcardattend->attendance_mode == 1) {
	  		$display_period = $i_DayTypePM; 
		  	$DayType = PROFILE_DAY_TYPE_PM;
		  	$period = "PM";
	  	}
	  	else if ($lcardattend->attendance_mode == 0) {
	    	$display_period = $i_DayTypeAM; 
		  	$DayType = PROFILE_DAY_TYPE_AM;
		  	$period = "AM";
		  }
		  else {
		  	$ClassID = $lcardattend->getClassID($luser->ClassName);
			  $TimeSetting = $lcardattend->Get_Class_Attend_Time_Setting($ToDate,"",$ClassID);
			  
			  if ($TimeSetting != "NonSchoolDay" && $TimeSetting !== false) {
			  	$current_time = time();
					$today = date('Y-m-d',$current_time);
					$ts_now = $current_time - strtotime($today);
					
					$ts_morningTime = $lcardattend->timeToSec($TimeSetting['MorningTime']);
					$ts_lunchStart = $lcardattend->timeToSec($TimeSetting['LunchStart']);
					$ts_lunchEnd = $lcardattend->timeToSec($TimeSetting['LunchEnd']);
					$ts_leaveSchool = $lcardattend->timeToSec($TimeSetting['LeaveSchoolTime']);
					
					if ($ts_now > $ts_lunchEnd) {
						$display_period = $i_DayTypePM; 
				  	$DayType = PROFILE_DAY_TYPE_PM;
				  	$period = "PM";
					}
					else {
						$display_period = $i_DayTypeAM; 
				  	$DayType = PROFILE_DAY_TYPE_AM;
				  	$period = "AM";
					}
				}
				else {
					$display_period = $i_DayTypeAM; 
			  	$DayType = PROFILE_DAY_TYPE_AM;
			  	$period = "AM";
				}
		  }
  	}
  	else {
	  	if ($lcardattend->attendance_mode == 1) {
	  		$display_period = $i_DayTypePM; 
		  	$DayType = PROFILE_DAY_TYPE_PM;
		  	$period = "PM";
	  	}
	  	else {
	    	$display_period = $i_DayTypeAM; 
		  	$DayType = PROFILE_DAY_TYPE_AM;
		  	$period = "AM";
		  }
		}
  	break;
}

if($class_name)
{
  # Check this class need to take attendance
  if (!$lcardattend->isRequiredToTakeAttendanceByDate($class_name,$ToDate))
  {
		//header ("Location: /");
		//intranet_closedb();
		//exit();
		$js_redirect_hompage = '$(document).ready(function(){
									alert("'.$Lang['StudentAttendance']['Warning']['NoNeedToTakeAttendanceOnSelectedDate'].'");
									window.location.href="/home/smartcard";
								});';
  }
}

### build confirmation info table
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;

# Group Type defaults to "Class"
$groupType = 1;
if($_SESSION['UserType']==USERTYPE_STUDENT && !$isSmartAttendanceAdmin && count($responsible_user_group_ids)>0){
	$groupType = 2; // default to group type
}

### Class select
$class_select = "<SELECT id=\"class_name\" name=\"class_name\" onChange=\"retrivePMTime();\">";
$empty_selected = ($class_name == '')? "SELECTED":"";
$class_select .= "<OPTION value='' $empty_selected> -- $button_select -- </OPTION>\n";
for($i=0; $i< sizeof($class_list); $i++)
{
	if($_SESSION['UserType']==USERTYPE_STUDENT && $class_name_that_student_can_take != $class_list[$i][3]){ 
		// restrict student helper can only take own class
		continue;
	}
	
	if ($class_list[$i][2] != $class_list[$i-1][2]) {
		if ($i == 0) 
			$class_select .= '<optgroup label="'.$class_list[$i][2].'">';
		else {
			$class_select .= '</optgroup>';
			$class_select .= '<optgroup label="'.$class_list[$i][2].'">';
		}
	}
	list($itr_class_id, $itr_class_name,$YearName,$itr_class_name_en) = $class_list[$i];
	$selected = ($itr_class_name_en==$class_name)?"SELECTED":"";
	$class_select .= "<OPTION value=\"".$itr_class_name_en."\" $selected>".$itr_class_name."</OPTION>\n";
	
	if ($i == (sizeof($class_list)-1)) 
		$class_select .= '</optgroup>';
}
$class_select .= "</SELECT>\n";

### Group select
$group_select = "<SELECT id=\"group_id\" name=\"group_id\" onChange=\"retrivePMTime();\">\n";
$empty_selected = ($group_id == '')? "SELECTED":"";
$group_select .= "<OPTION value='' $empty_selected> -- $button_select -- </OPTION>\n";
for($i=0; $i<sizeOf($group_list); $i++)
{
        list($itr_group_id, $itr_group_name) = $group_list[$i];
        if($responsible_user_group_id_size>0 && in_array($itr_group_id,$responsible_user_group_ids))
        {
        	$selected = ($itr_group_id==$group_id)?"SELECTED":"";
        	$group_select .= "<OPTION value=\"".$itr_group_id."\" $selected>".$itr_group_name."</OPTION>\n";
        }
}
$group_select .= "</SELECT>\n";

## subject Group session select
if(count($SubjectGroupSessionList)==0) {
	$SubjectGroupSessionSelect = $Lang['LessonAttendance']['NoLessonToTake'];
}else{
	$SubjectGroupSessionSelect = "<SELECT id=\"SubjectGroupSession\" name=\"SubjectGroupSession\">\n";
	$empty_selected = ($SubjectGroupSession == '')? "SELECTED":"";
	$SubjectGroupSessionSelect .= "<OPTION value='' $empty_selected> -- $button_select -- </OPTION>\n";
	
	for($i=0; $i<sizeOf($SubjectGroupSessionList); $i++)
	{
	  list($SubjectGroupID,$ClassTitleEN,$ClassTitleB5,$RoomAllocationID,$TimeSlotName,$StartTime,$EndTime,$ClassTitleEN,$ClassTitleB5) = $SubjectGroupSessionList[$i];
	  $ClassName = Get_Lang_Selection($ClassTitleB5,$ClassTitleEN);
	  $Value = htmlspecialchars($SubjectGroupID.'|=|'.$ClassName.'|=|'.$ToDate.'|=|'.$RoomAllocationID.'|=|'.$StartTime.'|=|'.$EndTime,ENT_QUOTES);
	
	  $selected = ($Value==$SubjectGroupSession)?"SELECTED":"";
	  if($_REQUEST['SubjectGroupID']){
	  	$Value1 = htmlspecialchars($_REQUEST['SubjectGroupID'].'|=|'.$SubjectGroupName.'|=|'.$_REQUEST["LessonDate"].'|=|'.$_REQUEST["RoomAllocationID"].'|=|'.$_REQUEST["StartTime"].'|=|'.$_REQUEST["EndTime"],ENT_QUOTES);
	  	//debug_pr($Value1);
	  	//debug_pr($Value);
	  	$selected = ($Value==$Value1)?"SELECTED":"";
	  }
	  $SubjectGroupSessionSelect .= "<OPTION value=\"".$Value."\" $selected>".$TimeSlotName.":".Get_Lang_Selection($ClassTitleB5,$ClassTitleEN)." ".$StartTime."-".$EndTime."</OPTION>\n";
	}
	
	$SubjectGroupSessionSelect .= "</SELECT>\n";
}

## subject Group select
if($sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable']){
	$SubjectGroupSelect = "<SELECT id=\"SubjectGroup\" name=\"SubjectGroup\">\n";
	$empty_selected = ($SubjectGroup == '')? "SELECTED":"";
	$SubjectGroupSelect .= "<OPTION value='' $empty_selected> -- $button_select -- </OPTION>\n";
	for($i=0; $i<sizeOf($SubjectGroupList); $i++)
	{
	  list($SubjectGroupID,$ClassTitleEN,$ClassTitleB5) = $SubjectGroupList[$i];
	  $ClassName = Get_Lang_Selection($ClassTitleB5,$ClassTitleEN);
	  
	  $selected = ($SubjectGroupID==$SubjectGroup)?"SELECTED":"";
	  $SubjectGroupSelect .= "<OPTION value=\"".$SubjectGroupID."\" $selected>".$ClassName."</OPTION>\n";
	}
	$SubjectGroupSelect .= "</SELECT>\n";
}else{
	
}

if ($period != "" && $class_name!="")
{
	#  use pull-down menu when taking

	$slot_select = "<SELECT name=period id=period>\n";
	if ($lcardattend->attendance_mode != 1)
	{
		$slot_select .= "<OPTION value=\"AM\" ".($period=="AM"?"SELECTED":"").">$i_DayTypeAM</OPTION>\n";
	}
	if ($lcardattend->attendance_mode != 0)
	{
		$slot_select .= "<OPTION value=\"PM\" ".($period=="PM"?"SELECTED":"").">$i_DayTypePM</OPTION>\n";
	}
	$slot_select .= "</SELECT>\n";

} else
{
	# by default, radio buttons are used
	/*
	$slot_select = "<SELECT name=period id=period>\n";
	if ($lcardattend->attendance_mode != 1)
	{
		$slot_select .= "<OPTION value=\"AM\" ".($period=="AM"?"SELECTED":"").">$i_DayTypeAM</OPTION>\n";
	}
	if ($lcardattend->attendance_mode != 0)
	{
		$slot_select .= "<OPTION value=\"PM\" ".($period=="PM"?"SELECTED":"").">$i_DayTypePM</OPTION>\n";
	}
	$slot_select .= "</SELECT>\n";
	*/
	$slot_select = "";
	if($lcardattend->attendance_mode == 0){
		$slot_select .= $i_DayTypeAM.'<input type="hidden" id="periodAM" name="period" value="AM" />';
	}else if($lcardattend->attendance_mode == 1){
		$slot_select .= $i_DayTypePM.'<input type="hidden" id="periodPM" name="period" value="PM" />';
	}else{
		if ($lcardattend->attendance_mode != 1)
		{
			$slot_select .= '<span class="period-am">'.$linterface->Get_Radio_Button("periodAM","period","AM",$lcardattend->attendance_mode==0,"",$i_DayTypeAM).'</span>';
		}
		if ($lcardattend->attendance_mode != 0)
		{
			$slot_select .= '<span class="period-pm">'.$linterface->Get_Radio_Button("periodPM","period","PM",$lcardattend->attendance_mode==1,"",$i_DayTypePM).'</span>';
		}
	}
}
####



if($class_name && $period)
{
  // Get preset Teacher's Remark from txt file
	/*$fs = new libfilesystem();
	$words_remarks = array();
	$file_path = $intranet_root."/file/student_attendance/preset_absent_reason2.txt";
	if (file_exists($file_path))
	{
		// Get data
		$text_data = trim($fs->file_read($file_path));
		if ($text_data != "")
		{
			$text_data_ary = explode("\n", $text_data);
			for ($i=0; $i<sizeof($text_data_ary); $i++)
			{
				if(trim($text_data_ary[$i])!="")
					$words_remarks[] = trim($text_data_ary[$i]);
			}
		}
	}*/
    
	$teacher_remark_js = "";
	$teacher_remark_words = $lcardattend->GetTeacherRemarkPresetRecords(array());
	$hasTeacherRemarkWord = sizeof($teacher_remark_words)!=0;
	if($hasTeacherRemarkWord)
	{
	    $teacher_remark_words = Get_Array_By_Key($teacher_remark_words, 'ReasonText');
	    foreach($teacher_remark_words as $key=>$word) {
	        $teacher_remark_words_temp[$key]= htmlspecialchars($word);
	    }
	    $teacher_remark_js = $linterface->CONVERT_TO_JS_ARRAY($teacher_remark_words_temp, "RemarksArrayWords", 1);
	}
	

  $CurAcedemicYear = getCurrentAcademicYear();
  $sql = "SELECT
        a.RecordID,
        IF(a.ConfirmedUserID <> -1, ".getNameFieldWithClassNumberByLang("c.").", CONCAT('$i_general_sysadmin')),
        a.DateModified
        FROM
        $card_student_daily_class_confirm as a
        LEFT OUTER JOIN YEAR_CLASS as b ON (a.ClassID=b.YearClassID)
        LEFT OUTER JOIN ACADEMIC_YEAR as d ON (b.AcademicYearID=d.AcademicYearID)
        LEFT OUTER JOIN INTRANET_USER as c ON (a.ConfirmedUserID=c.UserID)
        WHERE b.ClassTitleEN = '".$luser->Get_Safe_Sql_Query($class_name)."' AND DayNumber = '".$luser->Get_Safe_Sql_Query($txt_day)."' AND DayType = '".$luser->Get_Safe_Sql_Query($DayType)."' AND d.YearNameEN = '".$luser->Get_Safe_Sql_Query($CurAcedemicYear)."' " ;
  $result2 = $luser->returnArray($sql,3);
  list($confirmed_id, $confirmed_user_name, $last_confirmed_date) = $result2[0];
  if($confirmed_id==""){
          $update_str = $i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record;
  }
  else{
          //$update_str .= $i_LastModified .": ".$last_confirmed_date." (".$confirmed_user_name.")";
          $update_str = $last_confirmed_date." (".$confirmed_user_name.")";
  }


  $ldb = new libdb();

	if($sys_custom['StudentAttendance']['NoCardPhoto']){
		$studentIdToNoCardPhotoRecords = $lcardattend->getNoCardPhotoRecords(array('RecordDate'=>$ToDate,'StudentIDToRecord'=>1));
	}

  ### build student attend record table
  $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

  $conds = "";
  $conds .= " a.RecordType = 2 AND a.RecordStatus IN (0,1,2)";

  if($class_name<>"" && $period<>"")
          $conds .= "AND a.ClassName = '".$luser->Get_Safe_Sql_Query($class_name)."' ";
  $conds .= "ORDER BY a.ClassNumber ASC ";

	$username_field = getNameFieldByLang("a.");
	if($sys_custom['SmartCardAttendance_Teacher_ShowPhoto']){
		$sql = "SELECT a.UserID,a.WebSAMSRegNo,a.PhotoLink,a.UserLogin FROM INTRANET_USER AS a WHERE a.ClassName='".$luser->Get_Safe_Sql_Query($class_name)."' AND a.RecordType=2 AND a.RecordStatus IN(0,1)";
    $temp = $ldb->returnArray($sql,4);
		
    $link_ary=array();
    for($i=0;$i<sizeof($temp);$i++){
      list($sid,$regno,$p_link, $p_userlogin)=$temp[$i];
      
      $photo_url="";
      
      //$PhotoPath = $luser->GET_OFFICIAL_PHOTO_BY_USER_ID($sid);
      //if (file_exists($PhotoPath[0])) {
      //	$photo_url = $PhotoPath[2];
      //}
      $PhotoAry = $luser->GET_USER_PHOTO($p_userlogin, $sid);
      if(strpos($PhotoAry[1],'samplephoto.gif')===false){
      	$photo_url = $PhotoAry[2]; // url
      }
      
      if($photo_url=="" && $p_link!=""){ # use photo uploaded by student
      	$photo_url = $p_link;
      }
      if($photo_url!="" && file_exists($intranet_root.$photo_url)) {
      	$link_ary[$sid]=$photo_url;
      }
      else $link_ary[$sid]="";
    }

    $sql_insert_temp = "INSERT INTO TEMP_STUDENT_PHOTO_LINK (StudentID,Link) VALUES";
    if(sizeof($link_ary)>0){
      foreach($link_ary AS $student_id => $link){
              $sql_insert_temp.="($student_id,'$link'),";
      }
      $sql_insert_temp = substr($sql_insert_temp,0,strlen($sql_insert_temp)-1);
    }

    $create_temp_table = "CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_STUDENT_PHOTO_LINK(
                                                    StudentID INT(8),Link VARCHAR(255), PRIMARY KEY(StudentID))
                                            ";
    $ldb->db_db_query($create_temp_table);
    $ldb->db_db_query($sql_insert_temp);
		/*
		$NameField = "
							IF((e.Link IS NULL OR e.Link=''),$username_field,
							CONCAT('
								<div onMouseOut=\"setToolTip(false);setMenu(true);hideTip();\" onMouseOver=\"setToolTip(true);setMenu(false);showTip(\'ToolTip2\',makeTip(\'<table border=0 cellpadding=5 cellspacing=0 class=attendancestudentphoto><tr><td><img width=100px src=',e.Link,'></td></tr></table>\'))\"><table style=\"display:inline\" id=photo', a.UserID ,'a cellpadding=0 cellspacing=0><tr><td><img src={$image_path}/{$LAYOUT_SKIN}/icon_photo.gif>&nbsp;</td><td class=tabletext>',$username_field,'</td></tr></table></div><table style=\"display:none\" id=photo', a.UserID ,'b cellpadding=0 cellspacing=0><tr><td><img width=100px src=',e.Link,'>&nbsp;</td><td class=tabletext>',$username_field,'</td></tr></table>')) as name";
		*/
		$NameField = "
							IF((e.Link IS NULL OR e.Link=''),$username_field,
							CONCAT('
								<table style=\"display:inline\" id=photo', a.UserID ,'a cellpadding=0 cellspacing=0><tr><td style=\"border:none\"><img src={$image_path}/{$LAYOUT_SKIN}/icon_photo.gif onMouseOver=\"showHideTooltipPhoto(this,true);\" onMouseOut=\"showHideTooltipPhoto(this,false);\">
								<table style=\"display:none;position:absolute;\" border=0 cellpadding=5 cellspacing=0 class=attendancestudentphoto><tr><td><img width=100px src=',e.Link,'></td></tr></table>
								&nbsp;</td><td class=\"tabletext\" style=\"border:none\">',$username_field,'</td></tr></table></div><table style=\"display:none\" id=photo', a.UserID ,'b cellpadding=0 cellspacing=0><tr><td><img width=100px src=',e.Link,'>&nbsp;</td><td class=tabletext>',$username_field,'</td></tr></table>')) as name";						
		$JoinTable = "LEFT OUTER JOIN TEMP_STUDENT_PHOTO_LINK AS e ON (a.UserID=e.StudentID)";
	}
	else {
		$NameField = $username_field." as name";
		$JoinTable = "";
	}
	
	if ($lcardattend->EnableEntryLeavePeriod) {
		$FilterInActiveStudent .= "
 	        INNER JOIN 
          CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
          on a.UserID = selp.UserID 
          	AND 
          	'$ToDate' between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
          ";
	}
	
	if($period=="AM")
	{
		$ExpectedField = $lcardattend->Get_AM_Expected_Field("b.","c.","f.");
		$AbsentExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_ABSENT,'j.',"","d.","f.");
		$LateExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_LATE,'j.');
		$OutingExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,CARD_STATUS_OUTING,'j.',"","d.","c.",true);
		$ConfirmedUserField = "ConfirmedUserID";
		$IsConfirmedField = "IsConfirmed";
		$StatusField = "AMStatus";
		$BackTimeInfoField = "IF(b.InSchoolTime IS NULL, '-', b.InSchoolTime),
													IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),";
	
		$DateModifiedField = "DateModified";
		$ModifyByField = "ModifyBy";
		
		$daily_remark_field = "d.Remark as TeacherRemark ";
		
		$ConfirmedByAdminField = "b.AMConfirmedByAdmin";
	}
	else
	{
		if($sys_custom['StudentAttendance']['PMStatusFollowLunchSetting']){
			if($lcardattend->attendance_mode == 2 && $lcardattend->PMStatusNotFollowAMStatus){ // whole day with lunch, PM status does not follow AM status
				$LUNCH_ALLOW_LIST_TABLE = "CARD_STUDENT_LUNCH_ALLOW_LIST";
				$join_lunch_setting_table = " LEFT JOIN $LUNCH_ALLOW_LIST_TABLE ON $LUNCH_ALLOW_LIST_TABLE.StudentID=a.UserID ";
			}
		}
		
		if ($lcardattend->attendance_mode==1) // PM only mode
		{
	    	$time_field = "InSchoolTime";
		  	$station_field = "InSchoolStation";
		}
		else
		{
	    	$time_field = "LunchBackTime";
	    	$station_field = "LunchBackStation";
		}
		
		$ExpectedField = $lcardattend->Get_PM_Expected_Field("b.","c.","f.");
		$AbsentExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_ABSENT,'j.','k.','d.','f.');
		$LateExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_LATE,'j.');
		$OutingExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,CARD_STATUS_OUTING,'j.',"","d.","c.",true);
		$ConfirmedUserField = "PMConfirmedUserID";
		$IsConfirmedField = "PMIsConfirmed";
		$StatusField = "PMStatus";
		$AMJoinTable = "LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as k 
										ON 
											k.StudentID = a.UserID 
											AND k.RecordDate = '".$ToDate."' 
											AND k.DayType = '".PROFILE_DAY_TYPE_AM."'
											AND k.RecordType = '".PROFILE_TYPE_ABSENT."' ";
		$BackTimeInfoField = "IF(b.$time_field IS NULL,'-',b.$time_field),
													IF(b.$station_field IS NULL,'-',b.$station_field),";
		
		$DateModifiedField = "PMDateModified";
		$ModifyByField = "PMModifyBy";
		
		$daily_remark_field = "IF(d.RecordID IS NULL AND TRIM(d2.Remark)<>'',d2.Remark,d.Remark) as TeacherRemark ";
		$join_am_daily_remark_table = " LEFT JOIN CARD_STUDENT_DAILY_REMARK as d2 ON d2.StudentID=a.UserID AND d2.RecordDate='".$luser->Get_Safe_Sql_Query($ToDate)."' AND d2.DayType='".PROFILE_DAY_TYPE_AM."' ";
		
		$ConfirmedByAdminField = "b.PMConfirmedByAdmin";
	}
	
  $sql = "SELECT        
						b.RecordID,
						a.UserID,
						".$NameField.",
						a.ClassNumber,
						".$BackTimeInfoField." 
						IF(b.".$StatusField." IS NOT NULL,b.".$StatusField.", ".$ExpectedField."),
						$daily_remark_field,
						IF(j.RecordID IS NOT NULL,j.OfficeRemark,IF(TRIM(f.Remark)!='',f.Remark,c.Detail)) as OfficeRemark,
						IF(f.RecordID IS NOT NULL,1,0),
						f.Reason,
						f.Remark,
						f.Waive as PresetWaive, 
						IF(c.OutingID IS NOT NULL,1,0),
						c.RecordDate,
						c.OutTime,
						c.BackTime,
						c.Location,
						c.Objective,
						c.Detail,
						".$username_field.",
						if (".$ExpectedField." = '".CARD_STATUS_ABSENT."',
							".$AbsentExpectedReasonField.",
							IF(".$ExpectedField." = '".CARD_STATUS_LATE."',
								".$LateExpectedReasonField.",
								IF(".$ExpectedField." = '".CARD_STATUS_OUTING."',
									".$OutingExpectedReasonField.",
									j.Reason))) as Reason,
						j.AbsentSession,
						IFNULL(b.".$DateModifiedField.",'--'),
						IFNULL(".getNameFieldByLang("u.").",'--') as ModifyName, 
						b.".$IsConfirmedField." as IsConfirmed,
						IFNULL(".getNameFieldByLang("u1.").",'--') as ConfirmUserName, 
						j.RecordStatus, 
						ssc.LateSession,
						ssc.RequestLeaveSession,
						ssc.PlayTruantSession,
						ssc.OfficalLeaveSession,
						".($sys_custom['SmartCardAttendance_StudentAbsentSession2']?"ssc.AbsentSession as Absent2Session,":"'' as Absent2Session,")."
						".$AbsentExpectedReasonField." as DefaultLateReason,
						".$LateExpectedReasonField." as DefaultAbsentReason, 
						".$OutingExpectedReasonField." as DefaultOutingReason,
						b.LeaveStatus,
						b.LeaveSchoolTime,
						IF(j.RecordID IS NOT NULL,j.DocumentStatus,f.DocumentStatus) as DocumentStatus,
						$ConfirmedByAdminField as ConfirmedByAdmin     
					FROM
						INTRANET_USER AS a 
						INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=a.UserID 
						INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$lcardattend->CurAcademicYearID."' 
						".$FilterInActiveStudent." 
						LEFT OUTER JOIN $card_log_table_name AS b 
						ON (a.UserID=b.UserID AND b.DayNumber = '".$luser->Get_Safe_Sql_Query($txt_day)."')
						LEFT JOIN INTRANET_USER as u on b.".$ModifyByField." = u.UserID 
						LEFT JOIN INTRANET_USER as u1 on b.".$ConfirmedUserField." = u1.UserID 
						LEFT OUTER JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = '".$luser->Get_Safe_Sql_Query($ToDate)."' AND c.DayType = '".$luser->Get_Safe_Sql_Query($DayType)."')
						LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (a.UserID=d.StudentID AND d.RecordDate='".$luser->Get_Safe_Sql_Query($ToDate)."' AND d.DayType='".$luser->Get_Safe_Sql_Query($DayType)."') 
						$join_am_daily_remark_table 
						".$JoinTable." 
						LEFT OUTER JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID=f.StudentID AND f.RecordDate='".$luser->Get_Safe_Sql_Query($ToDate)."' AND f.DayPeriod='".$luser->Get_Safe_Sql_Query($DayType)."')
						LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as j
						ON j.StudentID = a.UserID 
							AND j.RecordDate = '".$luser->Get_Safe_Sql_Query($ToDate)."' 
							AND j.DayType = '".$luser->Get_Safe_Sql_Query($DayType)."' 
							AND 
							(
								(j.RecordType = b.".$StatusField." AND j.RecordType<>'".PROFILE_TYPE_EARLY."')
								OR  
								(j.RecordType = '".PROFILE_TYPE_ABSENT."' 
								AND b.".$StatusField." = '".CARD_STATUS_OUTING."' 
								)
							)
						".$AMJoinTable." $join_lunch_setting_table 
						LEFT JOIN CARD_STUDENT_STATUS_SESSION_COUNT as ssc
						ON ssc.StudentID = a.UserID 
							AND ssc.RecordDate = '".$luser->Get_Safe_Sql_Query($ToDate)."' 
							AND ssc.DayType = '".$luser->Get_Safe_Sql_Query($DayType)."' 
					WHERE
						".$conds;
  //debug_r($sql);
  $result = $luser->returnArray($sql,37);
//  debug_pr($result);
//  die();
	if($sys_custom['StudentAttendance']['RecordBodyTemperature']){
		$student_id_ary = Get_Array_By_Key($result, 'UserID');
		$bodyTemperatureRecords = $lcardattend->getBodyTemperatureRecords(array('RecordDate'=>$ToDate,'StudentID'=>$student_id_ary,'StudentIDToRecord'=>1));
	}
  	
  	if($sys_custom['SmartCardAttendance_SubmittedProveDocumentForEarlyLeave']){
  		$student_id_ary = count($result)>0 ? Get_Array_By_Key($result, 'UserID') : array(-1);
  		$earlyleave_profile_sql = "SELECT * FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='".$luser->Get_Safe_Sql_Query($ToDate)."' AND RecordType='".PROFILE_TYPE_EARLY."' AND StudentID IN (".implode(",",$student_id_ary).")";
  		$earlyleave_profile_records = $lcardattend->returnResultSet($earlyleave_profile_sql);
  		$earlyleave_profile_records_size = count($earlyleave_profile_records);
  		$studentIdToEarlyLeaveRecords = array();
  		for($i=0;$i<$earlyleave_profile_records_size;$i++){
  			$studentIdToEarlyLeaveRecords[$earlyleave_profile_records[$i]['StudentID']] = $earlyleave_profile_records[$i];
  		}
  	}
  	
	include_once($PATH_WRT_ROOT."includes/libdbtable.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
	
  if($field=="") $field = 0;
  if($order=="") $order = 1;
  $li = new libdbtable2007($field, $order, $pageNo);

  $setAllAttend = "<br />
					<select id=\"drop_down_status_all\" name=\"drop_down_status_all\">
						<option value=\"".CARD_STATUS_PRESENT."\"".($lcardattend->DefaultAttendanceStatus == CARD_STATUS_PRESENT?" selected":"").">".$Lang['StudentAttendance']['Present']."</option>
						<option value=\"".CARD_STATUS_ABSENT."\"".($lcardattend->DefaultAttendanceStatus == CARD_STATUS_ABSENT?" selected":"").">".$Lang['StudentAttendance']['Absent']."</option>						
						<option value=\"".CARD_STATUS_LATE."\">".$Lang['StudentAttendance']['Late']."</option>
						<option value=\"".CARD_STATUS_OUTING."\">".$Lang['StudentAttendance']['Outing']."</option>
					</select><br />
					".$linterface->Get_Apply_All_Icon("javascript:SetAllAttend();");

  // TABLE COLUMN
  $pos = 0;
  $li->column_list .= "<td class='tablegreentop tabletoplink' width='3%'>". $i_UserClassNumber ."</td>\n";
  $li->column_list .= "<td class='tablegreentop tabletoplink' width='10%'>". $i_UserName ."</td>\n";
  $li->column_list .= "<td class='tablegreentop tabletoplink' width='8%'>". $i_SmartCard_Frontend_Take_Attendance_In_School_Time ."</td>\n";
  $li->column_list .= "<td class='tablegreentop tabletoplink' width='6%'>". $i_SmartCard_Frontend_Take_Attendance_In_School_Station ."</td>\n";
  if($sys_custom['StudentAttendance']['RecordBodyTemperature']){
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>". $Lang['StudentAttendance']['BodyTemperature'] ."</td>\n";
  }
  $li->column_list .= "<td class='tablegreentop tabletoplink' width='3%'>". $i_SmartCard_Frontend_Take_Attendance_PreStatus ."</td>\n";
  $li->column_list .= "<td class='tablegreentop tabletoplink' width='1%'>&nbsp;</td>\n";
  $li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>". $i_SmartCard_Frontend_Take_Attendance_Status.$setAllAttend."</td>\n";
  if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
  {
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>".$Lang['StudentAttendance']['Late']."</td>\n";
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>".$Lang['StudentAttendance']['RequestLeave']."</td>\n";
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>".$Lang['StudentAttendance']['PlayTruant']."</td>\n";
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>".$Lang['StudentAttendance']['OfficalLeave']."</td>\n";
  }
  if($sys_custom['SmartCardAttendance_StudentAbsentSession2'])
  {
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>".$Lang['StudentAttendance']['Absent']."</td>\n";
  }
  if (!$isStudentHelper) {
  	if($lcardattend->TeacherCanManageWaiveStatus)
  	{
  		$li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>
  											".$i_SmartCard_Frontend_Take_Attendance_Waived.
  											($IsEditDaysBeforeDisabled?"":"<br /><input type=\"checkbox\" name=\"all_wavied\" onClick=\"$('.WaiveBtn').attr('checked',this.checked);\">")."</td>\n";
  	}
  }
  if($lcardattend->TeacherCanManageProveDocumentStatus){
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>".$Lang['StudentAttendance']['ProveDocument']."<br /><input type=\"checkbox\" name=\"master_Handin_prove\" value=\"1\" onClick=\"(this.checked)?setAllHandin(document.form1,1):setAllHandin(document.form1,0);\"></td>";
  	if($sys_custom['SmartCardAttendance_SubmittedProveDocumentForEarlyLeave']){
  		$li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>".$Lang['StudentAttendance']['ProveDocument'].'('.$Lang['StudentAttendance']['EarlyLeave'].')'."<br /><input type=\"checkbox\" name=\"master_HandinEL_prove\" value=\"1\" onClick=\"(this.checked)?setAllHandinEL(document.form1,1):setAllHandinEL(document.form1,0);\"></td>";
  	}
  }
  if($lcardattend->TeacherCanManageReason)
  {
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='10%'>". $i_Attendance_Reason ."</td>\n";
  }
  if($lcardattend->TeacherCanManageTeacherRemark)
  {
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='10%'>".$Lang['StudentAttendance']['iSmartCardRemark']."<br>
				<input class=\"textboxnum\" type=\"text\" name=\"SetAllRemark\" id=\"SetAllRemark\" maxlength=\"255\" value=\"\">".$linterface->GET_PRESET_LIST("RemarksArrayWords", "SetAllRemarkIcon", "SetAllRemark")."<br>".$linterface->Get_Apply_All_Icon("javascript:SetAllRemarks();")."</td>\n";
  }

  $li->column_list .=  "<script language='javascript'>jImagePath=\"{$image_path}/{$LAYOUT_SKIN}\";\n</script>\n";

  $li->column_list .= "<td class='tablegreentop tabletoplink' width='10%'>". $Lang['StudentAttendance']['OfficeRemark'] ."</td>\n";
	$li->column_list .= "<td class='tablegreentop tabletoplink' width='12%'>".$Lang['General']['LastModified']." (".$Lang['General']['LastModifiedBy'].")</td>\n";
	$li->column_list .= "<td class='tablegreentop tabletoplink' width='15%'>".$Lang['StudentAttendance']['LastConfirmPerson']."</td>\n";
  $li->IsColOff = "SmartCardTakeAttendance";
  $li->title = "";
}
//debug_r($sql);
### Button / Tag
$ViewPastRecord = '<a href="#" onclick="window.open(\'attendance_record/class_monthly_report.php?ClassName='.urlencode($class_name).'&Year='.urlencode($txt_year).'&Month='.urlencode($txt_month).'\', \'attendRefWin\');" class="tabletool">';
$ViewPastRecord .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view_s.gif" width="12" height="12" border="0" align="absmiddle" />'.$i_StudentAttendance_ViewPastRecords.'</a>';
$DisplayPhotoBtn = "<a href='javascript:toggle_photo();' class='tabletool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_photo.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> <span id=\"DisplayAllPhotoTxt\">" . $DisplayAllPhoto . "<span></a>";
$DisplayPhotoBtn = $sys_custom['SmartCardAttendance_Teacher_ShowPhoto'] ? $DisplayPhotoBtn : "";
$setAtoP        = "<a href='javascript:AbsToPre();' class='tabletool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $i_StudentAttendance_Action_SetAbsentToPresent . "</a>";
if($IsEditDaysBeforeDisabled){
	$setAtoP = ''; // remove the edit tools if disable edit mode
}

# check eDis LATE category
if($plugin['Disciplinev12']) {	# check whether eDis "Late" category is set to "Not In Use" , and with any GM Late record in Target Date

	include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
	$ldiscipline = new libdisciplinev12();
	
	$CanPassGmLateChecking = $ldiscipline->Check_Can_Pass_Gm_Late_Checking($ToDate);
	
} else {
	$CanPassGmLateChecking = 1;	
}


### Title ###
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/smartcard/icon_takeattendance.gif' align='absmiddle' />";
$TitleTitle1 = "<span class='contenttitle'>".$TitleImage1. $i_StudentAttendance_Frontend_menu_TakeAttendance ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
$TAGS_OBJ[] = array($TitleTitle, "", 0);

$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

?>
<link href="/templates/<?=$LAYOUT_SKIN?>/css/lesson_attendance.css" rel="stylesheet" type="text/css" />
<script language="Javascript" src='<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/js/lesson_attendance.js'></script>
<script language="Javascript" src='<?=$PATH_WRT_ROOT?>templates/tooltip.js'></script>
<style type="text/css">
	#ToolTip2{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
	
	.period-am {
		background-color: #9EDBF0;
		padding: 6px 10px 5px 3px;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		-ms-border-radius: 4px;
		-o-border-radius: 4px;
		border-radius: 4px;
	}
	
	.period-pm {
		background-color: #F5C99A;
		padding: 6px 10px 5px 3px;
		margin-left: 5px;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		-ms-border-radius: 4px;
		-o-border-radius: 4px;
		border-radius: 4px;
	}
</style>
<div id="ToolTip2"></div>
<div id="ToolTip3" style='position:absolute; left:100px; top:30px; z-index:1; visibility: hidden'></div>
<script language="javascript">
<!--
// AJAX follow-up
var callback = {
  success: function ( o )
  {
  	<? if($lcardattend->attendance_mode > 1) {?>
	    //jChangeContent( "ToolMenu2", o.responseText );
	    //writeToLayer('ToolMenu2',o.responseText);
	    //showMenu2("img2_"+imgObj,"ToolMenu2");
	    //alert(o.responseText);
	//    var obj = document.getElementById('period');
	    if(o.responseText == 'AM'){
			<? if($lcardattend->attendance_mode !=1) {?>
	     	//obj.options[0].selected = true;
	        //obj.options[1].selected = false;
	        $('#periodAM').attr('checked',true);
	        $('#periodPM').attr('checked',false);
	      <? } ?>
	      
	    }
	    if(o.responseText == 'PM'){
	    <? if($lcardattend->attendance_mode !=0) {?>
	      //obj.options[0].selected = false;
	      //obj.options[1].selected = true;
	      $('#periodAM').attr('checked',false);
	      $('#periodPM').attr('checked',true);
	      <? } ?>
	    }
	    if(o.responseText == ''){
	    	$('#periodAM').attr('checked',false);
	      	$('#periodPM').attr('checked',false);
	    }
  	<?}?>
  }
}
// start AJAX
function retrivePMTime()
{
	<? if($lcardattend->attendance_mode > 1 && !$sys_custom['StudentAttendance']['DisableAutoChangePeriod']) {?>
	obj = document.form1;
	
	<? if (!$isStudentHelper) { ?>
		if(document.getElementById('groupTypeClass') && document.getElementById('groupTypeClass').checked){
			var name = obj.class_name.value;
			var path = "retrive_pm_time.php?ClassName=" + encodeURIComponent(name);
			
			YAHOO.util.Connect.setForm(obj);
			var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
		}
		else if(document.getElementById('groupTypeGroup') && document.getElementById('groupTypeGroup').checked){
			var GroupID = obj.group_id.value;
			var path = "retrive_pm_time.php?groupIDString=" + GroupID;
			
			YAHOO.util.Connect.setForm(obj);
			var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
		}
		else {
			
		}
	<?}
		else {?>
			var name = '<?=intranet_htmlspecialchars($class_name)?>';
			var path = "retrive_pm_time.php?ClassName=" + encodeURIComponent(name);
			
			YAHOO.util.Connect.setForm(obj);
			var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
	<?}?>
	
			
	//var myElement = document.getElementById("ToolMenu2");
	//writeToLayer('ToolMenu2','<table border=1 width=300 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td>Loading</td></tr></table>');
	//imgObj = i;
	//showMenu2("img2_"+i,"ToolMenu2");
	<?}?>
}
function showMenu2(objName,menuName)
{
                  hideMenu2('ToolMenu');
                  hideMenu2('ToolMenu2');
           objIMG = document.getElementById(objName);

                        offsetX = (objIMG==null)?0:objIMG.width;
                        offsetY =0;
            var pos_left = getPostion(objIMG,"offsetLeft");
                        var pos_top  = getPostion(objIMG,"offsetTop");
                        objDiv = document.getElementById(menuName);

                        if(objDiv!=null){
                                objDiv.style.visibility='visible';
                                objDiv.style.top = pos_top+offsetY;
                                objDiv.style.left = pos_left+offsetX;
                                setDivVisible(true, menuName, "lyrShim");
                        }
}

function checkTargetDateChanged()
{
	var target_date = $('#ToDate').val();
	var today = getToday();
	<?php if($lcardattend->DisableISmartCardPastDate=='1'){ ?>
	if(target_date != '' && target_date < today){
		alert('<?=$Lang['StudentAttendance']['ISmartCardDisableTakePastRecord']?>');
		return false;
	}
	<?php } ?>
	<?php if($lcardattend->CannotTakeFutureDateRecord=='1'){ ?>
	if(target_date != '' && target_date > today){
		alert('<?=$Lang['StudentAttendance']['WarningCannotTakeFutureDateRecord']?>');
		return false;
	}
	<?php } ?>
	return true;
}
-->
</script>

<script language="JavaScript">
isToolTip = false;

function setToolTip(value)
{
        isToolTip=value;
}

function toggle_photo()
{

        with(document.form1)
        {
                var DisplayAllPhotoTxt=document.getElementById("DisplayAllPhotoTxt");
                <? for($i=0; $i<sizeof($result); $i++) {
                                        ?>
                if(document.getElementById("photo<?=$result[$i][1]?>a") )
                {
                	var photo<?=$result[$i][1]?>a=document.getElementById("photo<?=$result[$i][1]?>a");
                	var photo<?=$result[$i][1]?>b=document.getElementById("photo<?=$result[$i][1]?>b");
                	
                        if(photo<?=$result[$i][1]?>a.style.display=="inline")
                        {
                                photo<?=$result[$i][1]?>a.style.display = "none";
                                photo<?=$result[$i][1]?>b.style.display = "inline";
                                DisplayAllPhotoTxt.innerHTML = "<?=$HiddenAllPhoto?>";
                        }
                        else
                        {
                                photo<?=$result[$i][1]?>a.style.display = "inline";
                                photo<?=$result[$i][1]?>b.style.display = "none";
                                DisplayAllPhotoTxt.innerHTML = "<?=$DisplayAllPhoto?>";
                        }
                }
                <? } ?>
        }
}

function showHideTooltipPhoto(mouseOverObj,isShow)
{
	var obj = $(mouseOverObj);
	var imageObj = obj.next();
	
	var pos = obj.position();
	var offsetX = 18;
	var offsetY = 18;
	imageObj.css({'left':pos.left + offsetX,'top':pos.top+offsetY});
	if(isShow){
		imageObj.show();
	}else{
		imageObj.hide();
	}
}

function Check_Form1() {
	<? if(!$CanPassGmLateChecking) {?>
		alert('<?=$Lang['eAttendance']['eDisciplineCategoryNotInUseWarning']?>');
		return false;
	<?
	} else {?>
		AlertPost(document.form1, 'take_update.php', '<?=$i_SmartCard_Confirm_Update_Attend?>');
	<? }
	?>
}
</script>

<style type="text/css">
        #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>

<script language="JavaScript">
    isMenu =false;
</script>

<iframe id='lyrShim' src='javascript:false;' scrolling='no' frameborder='0' style='position:absolute; top:0px; left:0px; display:none;'></iframe>
<div id="ToolMenu" style="position:absolute; width:200; height:100; visibility:hidden"></div>
<br />
<?php 
if($plugin['attendancelesson'] && $_SESSION['isTeaching'])
{
?>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" onMouseMove="overhere();">
	<tr>
	  <td align="center">
	  	<table width="96%" border="0" cellspacing="0" cellpadding="0">
	    <tr>
	    	<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$Lang['StudentAttendance']['TakeAttendanceFor']?></span></td>
					<td valign="top" class="tabletext">
					<?php if($plugin['attendancestudent']){ ?>
						<input type="radio" id="AttendnaceForSchool" name="AttendanceFor" value="School" checked="checked" onclick="Get_School_Layer(this.checked,'School');" autocomplete="off">
						<label for="AttendnaceForSchool"><?=$Lang['StudentAttendance']['School']?></label> 
					<?php } ?>
					<?php if($plugin['attendancelesson']){ ?>
						<input type="radio" id="AttendnaceForLesson" name="AttendanceFor" value="Lesson" <?if((!$plugin['attendancestudent'] && $plugin['attendancelesson']) || $SessionSelect || $_REQUEST['SubjectGroupID']) echo 'checked="checked"'; ?> onclick="Get_Lesson_Layer(this.checked);" autocomplete="off">
						<label for="AttendnaceForLesson"><?=$Lang['StudentAttendance']['Lesson']?></label>
					<?php } ?>
					</td>
				</tr>
				</table>
				</td>
			</tr>
	    <tr>
	    	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;</td>
	    </tr>
	    <tr>
	      <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	    </tr>
	    <tr>
	      <td colspan="2">
	    	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	      <tr>
          <td align="center">
          </td>
        </tr>
	      </table>
	      </td>
	   	</tr>
	    <tr>
        <td align="center" class="tabletextremark" colspan="2">&nbsp;&nbsp;&nbsp;
        <?
        //DATA HAS BEEN UPDATED BY OTHER BEFORE SUBMIT THIS PAGE, DATA NOT CONSISTENCE
        if($msg == 99)
        {
                echo "<br><font color =\"red\">".$i_StudentAttendance_Warning_Data_Outdated."</font><br><br>";
        }
        ?>

        </td>
	    </tr>
	  	</table>
	  </td>
	</tr>
</table>
<?php
}
?>
<div id="SchoolAttendanceLayer" <? if((!$plugin['attendancestudent']) || $SessionSelect || $_REQUEST['SubjectGroupID']) echo 'style="display:none"';?> >
	<form name="form1" method="get" onsubmit="return false;">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" onMouseMove="overhere();">
	<tr>
	        <td align="center">
	                <table width="96%" border="0" cellspacing="0" cellpadding="0">
	                <tr>
	                        <td align="left" class="tabletext">
	
							<table width="100%" border="0" cellspacing="0" cellpadding="3">
	<?
						if($class_name != "")
						{
	?>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_SmartCard_ClassName?> </span></td>
								<td valign="top" class="tabletext"><?=intranet_htmlspecialchars($class_name)?><input type="hidden" id="class_name" name="class_name" value="<?=escape_double_quotes($class_name)?>"></td>
							</tr>
	<?
						}
						elseif($group_id != "")
						{
	?>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_SmartCard_GroupName?> </span></td>
								<td valign="top" class="tabletext"><?=intranet_htmlspecialchars($groupName)?><input type="hidden" name="group_id" id="group_id" value="<?=escape_double_quotes($group_id)?>"></td>
							</tr>
	<?
						}
						else
						{
	?>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_StudentAttendance_By?></span></td>
								<td>
								<?php if(!($_SESSION['UserType']==USERTYPE_STAFF || ($_SESSION['UserType']==USERTYPE_STUDENT && $isSmartAttendanceAdmin))){
									echo '<span style="display:none;">';
								 }?>
									<input type="radio" name="groupType" value="1"<?=($groupType==1)?" checked":""?> id="groupTypeClass" onClick="toggle(this.value);" autocomplete="off"><label for="groupTypeClass"><?=$i_SmartCard_ClassName?></label>&nbsp;
								<?php if(!($_SESSION['UserType']==USERTYPE_STAFF || ($_SESSION['UserType']==USERTYPE_STUDENT && $isSmartAttendanceAdmin))){
									echo '</span>';
								 }?>
								<?php if(count($responsible_user_group_ids)==0){
									echo '<span style="display:none;">';
								}?>
									<input type="radio" name="groupType" value="2"<?=($groupType==2)?" checked":""?> id="groupTypeGroup" onClick="toggle(this.value);" autocomplete="off"><label for="groupTypeGroup"><?=$i_SmartCard_GroupName?></label>&nbsp;
								<?php if(count($responsible_user_group_ids)==0){
									echo '</span>';
								}?>
<?
	if ($intranet_version == "2.5" || 1) {
?>
								<?php 
								if($_SESSION['UserType']!=USERTYPE_STAFF){
									echo '<span style="display:none;">';
								} 
								?>
									<input type="radio" id="groupTypeSubjectGroup" name="groupType" value="SubjectGroup" <?=($groupType=="SubjectGroup")?" checked":""?> onClick="toggle(this.value);" autocomplete="off"><label for="groupTypeSubjectGroup"><?=$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup']?></label>
								<?php 
								if($_SESSION['UserType']!=USERTYPE_STAFF){
									echo '</span>';
								} 
								?>	
<?
	}
?>
								</td>
							</tr>
					<?php if($sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable'])
						  { ?>						
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext">&nbsp;</span></td>
								<td>
									<?=$class_select?>
									<?=$group_select?>
<?
	if ($intranet_version == "2.5" || 1) {
?>								
									<span id="SubjectGroupSelectLayer" style="display:none;">
										<?=$SubjectGroupSelect?>
									</span>
<?
	}
?>
								</td>
							</tr>
					<?php }else{ ?>
							<tr id="TrClassSelection">
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext">&nbsp;</span></td>
								<td>
									<?=$class_select?>
								</td>
							</tr>
							<tr id="TrGroupSelection" style="display:none;">
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext">&nbsp;</span></td>
								<td>
									<?=$group_select?>
								</td>
							</tr>
							<?php if ($intranet_version == "2.5" || 1){ ?>
							<tr id="TrSubjectSelection" style="display:none;">
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$Lang['SysMgr']['SubjectClassMapping']['Subject']?></span></td>
								<td>
									<span id="SubjectSelectLayer"><?=$SubjectSelect?></span>
								</td>
							</tr>
							<tr id="TrSubjectGroupSelection" style="display:none;">
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup']?></span></td>
								<td>
									<span id="SubjectGroupSelectLayer"><?=$SubjectGroupSelect?></span>
								</td>
							</tr>
							<?php } ?>
					<?php } ?>
	<?
						}
	?>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%">
									<?=$i_StudentAttendance_Field_Date?>
								</td>
								<td class="tabletext" width="70%">
<?
						if ($lcardattend->DisableISmartCardPastDate && $lcardattend->CannotTakeFutureDateRecord) {
?>
							<?=date('Y-m-d')?> <input type="hidden" name="ToDate" id="ToDate" value="<?=date('Y-m-d')?>">
<?
						}
						else {
							if($sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable']){
								echo $linterface->GET_DATE_PICKER("ToDate", $ToDate,' onchange="Get_Subject_Group_List(this.value); onDateValueChanged();" ',"","","","",'Get_Subject_Group_List(this.value); onDateValueChanged();');
							}else{
								echo $linterface->GET_DATE_PICKER("ToDate", $ToDate,' onchange="reloadSubject(this.value);subjectChanged(\'\'); onDateValueChanged();" ',"","","","",'reloadSubject(this.value);subjectChanged(\'\'); onDateValueChanged();');
							}
						}
?>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_BookingTimeSlots?> </span></td>							
								<td valign="top" class="tabletext"><?=($class_name and $period) ? $slot_select ."<input type=\"hidden\" id=\"period\" name=\"period\" value=\"".escape_double_quotes($period)."\">" : $slot_select?></td>
							</tr>
							<?php if($class_name && $period) {?>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_LastModified?> </span></td>
								<td valign="top" class="tabletext"><?=$update_str?></td>
							</tr>
							<?php } ?>
							</table>
	
	                        </td>
	                </tr>
	                <tr>
	                        <td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;</td>
	                </tr>
	                <tr>
	                        <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	                </tr>
	                <tr>
	                        <td colspan="2">
	                                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	                                <tr>
	                                        <td align="center">
	                                        <? if($class_name && $period) {?>
	                                                <?= $linterface->GET_ACTION_BTN($button_view, "button", "view_class();","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	                                                <?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='take_attendance.php'","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	                                        <? } else {?>
	                                                <?= $linterface->GET_ACTION_BTN($button_view, "button", "view_class();","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	                                        <? } ?>
	                                        </td>
	                                </tr>
	                                </table>
	                        </td>
	                </tr>
	
	                <tr>
	                        <td align="center" class="tabletextremark" colspan="2">&nbsp;&nbsp;&nbsp;
	                        <?
	                        //DATA HAS BEEN UPDATED BY OTHER BEFORE SUBMIT THIS PAGE, DATA NOT CONSISTENCE
	                        if($msg == 99)
	                        {
	                                echo "<br><font color =\"red\">".$i_StudentAttendance_Warning_Data_Outdated."</font><br><br>";
	                        }
	                        ?>
	
	                        </td>
	                </tr>
	                <? if($class_name && $period) {?>
	                <tr>
	                        <td align="left" colspan="2">
	
	                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                                        <tr>
	                                                <td valign="bottom" align="left">
	                                                	
																										<table border=0>
																											<tr>
																												<td class="attendance_late" width="15px">
																													&nbsp;
																												</td>
																												<td class="tabletext">
																													<?=$Lang['StudentAttendance']['Late']?>
																												</td>
																												<td class="attendance_norecord" width="15px">
																													&nbsp;
																												</td>
																												<td class="tabletext">
																													<?=$Lang['StudentAttendance']['Absent']?>
																												</td>
																												<td class="attendance_outing" width="15px">
																													&nbsp;
																												</td>
																												<td class="tabletext">
																													<?=$Lang['StudentAttendance']['Outgoing']?>
																												</td>
																												<td class="tablebottom" width="15px">
																													&nbsp;
																												</td>
																												<td class="tabletext">
																													<?=$Lang['StudentAttendance']['UnConfirmed']?>
																												</td>
																											</tr>
																										</table>
	                                                </td>
	                                                <td align="right" valign="bottom">
	                                                        <table border="0" cellspacing="0" cellpadding="0">
	                                                        <tr>
	                                                                <td width="21"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/tablegreen_tool_01.gif" width="21" height="23" /></td>
	                                                                <td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/tablegreen_tool_02.gif">
	                                                                        <table border="0" cellspacing="0" cellpadding="2">
	                                                                        <tr>
	                                                                                <td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="5" /></td>
	                                                                                <td nowrap><?=$ViewPastRecord?></td>
	                                                                                <td nowrap><?=$DisplayPhotoBtn?></td>
	                                                                                <td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="5" /></td>
	                                                                                <td nowrap><?=$setAtoP?></td>
	                                                                        </tr>
	                                                                        </table>
	                                                                </td>
	                                                                <td width="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/tablegreen_tool_03.gif" width="6" height="23" /></td>
	                                                        </tr>
	                                                        </table>
	                                                </td>
	                                        </tr>
	                                </table>
	                        </td>
	                </tr>
	
	                <tr>
	                        <td align="center">
	                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                                <tr>
	                                        <td colspan="2"><?=$li->display($display_period)?></td>
	                                </tr>
	                                </table>
	                        </td>
	                </tr>
					<?php if(!$IsEditDaysBeforeDisabled){ ?>
	                <tr>
	                        <td>
	                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
	                                <tr>
	                                        <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	                                </tr>
	                                <tr>
	                                        <td align="center">
	                                            <?= $linterface->GET_ACTION_BTN($button_submit, "button", "Check_Form1(document.form1,'take_update.php','$i_SmartCard_Confirm_Update_Attend?')","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	                                            <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "resetForm(document.form1);","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	                                        </td>
	                                </tr>
	                        </table>
	                </td>
	                </tr>
	                <?php } // end of if(!$IsEditDaysBeforeDisabled)  ?>
	                <? } ?>
	                </table>
	        </td>
	</tr>
	</table>
	<br />
	<input name="confirmed_user_id" type="hidden" value="<?=$_SESSION['UserID']?>">
	<input name="confirmed_id" type="hidden" value="<?=escape_double_quotes($confirmed_id)?>">
	<input name="PageLoadTime" type="hidden" value="<?=escape_double_quotes($page_load_time)?>">
	<? for ($i=0; $i<sizeof($editAllowed); $i++) { ?>
	<input type="hidden" name="editAllowed<?=$i?>" value="<?=escape_double_quotes($editAllowed[$i])?>">
	<? } ?>
	</form>
</div>

<!-- lesson attendance layer-->
<div id="LessonAttendanceLayer" <? if((!$plugin['attendancestudent'] && $plugin['attendancelesson']) || $SessionSelect || $_REQUEST['SubjectGroupID']) echo 'style="display:block"';else echo 'style="display:none"';?> >
	<!-- lesson attendance  -->
	<form name="form2" method="get">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" onMouseMove="overhere();">
	<tr>
	        <td align="center">
	                <table width="96%" border="0" cellspacing="0" cellpadding="0">
	                <tr>
	                        <td align="left" class="tabletext">
	
							<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_StudentAttendance_Field_Date?> </span></td>
								<td valign="top" class="tabletext">
									<?
									if($LessonDate)
										echo $linterface->GET_DATE_PICKER("TargetDate",$LessonDate,' onchange="Get_Session_List(this.value);" ',"","","","",'Get_Session_List(this.value);');
									else
										echo $linterface->GET_DATE_PICKER("TargetDate",$TargetDate,' onchange="Get_Session_List(this.value);" ',"","","","",'Get_Session_List(this.value);');
										?>
								</td>
							</tr>
					<?php if($LessonAttendanceSettings['TeacherCanTakeAllLessons']==1){ ?>		
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$Lang['LessonAttendance']['LessonType']?> </span></td>
								<td valign="top" class="tabletext">
									<?=$linterface->Get_Radio_Button("LessonTypeOwn", "LessonType", "0", isset($LessonType)?$LessonType!=1:1, '', $Lang['LessonAttendance']['TeachersOwnLessons'], 'Get_Session_List(document.getElementById(\'TargetDate\').value);" autocomplete="off', '')?>
									<?=$linterface->Get_Radio_Button("LessonTypeAll", "LessonType", "1", isset($LessonType)?$LessonType==1:0, '', $Lang['LessonAttendance']['AllLessons'], 'Get_Session_List(document.getElementById(\'TargetDate\').value);" autocomplete="off', '')?>
								</td>
							</tr>
					<?php } ?>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$Lang['StudentAttendance']['Session']?> </span></td>
								<td valign="top" class="tabletext">
									<span id="SessionSelectLayer"><?=$SubjectGroupSessionSelect?></span>
								</td>
							</tr>
							</table>
	
	                        </td>
	                </tr>
	                <tr>
	                        <td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;</td>
	                </tr>
	                <tr>
	                        <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	                </tr>
	                <tr>
                    <td colspan="2">
                      <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                      <tr>
                        <td align="center">
                        <?= $linterface->GET_ACTION_BTN($button_view, "button", "View_Session();","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                        </td>
                      </tr>
                      </table>
                    </td>
	                </tr>
	                </table>
	        </td>
	</tr>
	</table>
	<br />
	</form>
	<br />
	<!--<br />
	<br />
	<br />
	<br />
	<br />
	<br />-->
	<!-- end of lesson attendance -->
	<div id="LoadingLayer" name="LoadingLayer" style="z-index:0;">
		<div id="LessonAttendanceFlashLayer" name="LessonAttendanceFlashLayer" width="100%" style="height=800px;">
		<?
			if($_REQUEST['SubjectGroupID']){
				$LessonAttendUI = new libstudentattendance_ui();
				
				$lesson_attendance_reason_js = '';
				$statusMap = $LessonAttendUI->LessonAttendanceStatusMapping();
				foreach($statusMap as $key => $value)
				{
					$reason_ary = $LessonAttendUI->GetLessonAttendanceReasons('', $value['code']);
					$reason_text_ary = Get_Array_By_Key($reason_ary, 'ReasonText');
					$lesson_attendance_reason_js .= $linterface->CONVERT_TO_JS_ARRAY($reason_text_ary, "jArrayLessonReasons".$value['code'], 1);
				}
				
				$AttendOverviewID = $LessonAttendUI->Check_Record_Exist_Subject_Group_Attend_Overview($_REQUEST['SubjectGroupID'],$LessonDate,$_REQUEST['RoomAllocationID']);
				echo $LessonAttendUI->Get_Lesson_Attendance_Panel_Form($_REQUEST['SubjectGroupID'], "SubjectGroup", $LessonDate, $AttendOverviewID, 'web', 1);
			}
		?>
		</div>
	</div>
	<!--<iframe id='SessionFrame' src="<?=$PATH_WRT_ROOT?>home/smartcard/attendance/lesson_attendance/new_start/main1.html""  scrolling='no' frameborder='0' ></iframe>-->
</div>
<?php
if($plugin['attendancelesson']) echo $lesson_attendance_reason_js;
?>
<SCRIPT LANGUAGE="Javascript">
function view_class()
{
	if(!checkTargetDateChanged()) return false;
	
	if ("<?=$lcardattend->DisableISmartCardPastDate?>" == "1" || $("#DPWL-ToDate").html() == "") {
		var ToDate = document.getElementById('ToDate').value;
		var PeriodValue = '';
		if($('[name=period]').attr('tagName').toUpperCase() == 'SELECT') {
			PeriodValue = $('[name=period]').val();
		}else if($('[name=period]').attr('type').toUpperCase() == 'HIDDEN'){
			PeriodValue = $('[name=period]').val();
		}else{
			PeriodValue = $('[name=period]:checked').val();
		}
		if(!PeriodValue) {
			alert('<?=$Lang['StudentAttendance']['Warning']['SelectPeriodSession']?>');
			$('[name=period]').focus();
			return false;
		}
		if('<?=$CannotTakeFutureDateRecord?>' == '1'){
			var today = getToday();
			if(ToDate != '' && ToDate > today){
				alert('<?=$Lang['StudentAttendance']['WarningCannotTakeFutureDateRecord']?>');
				return false;
			}
		}
	<? if($class_name) {?>
		window.location="?class_name=" + document.getElementById('class_name').value + "&period="+PeriodValue+"&ToDate="+ToDate;
	<? } else {?>
		
		//var PeriodValue = document.getElementById('period')[document.getElementById('period').selectedIndex].value;
		if(document.form1.groupType[0].checked) {// Class
			var ClassValue = document.getElementById('class_name')[document.getElementById('class_name').selectedIndex].value;
			window.location="?class_name=" + ClassValue + "&period="+PeriodValue+"&ToDate="+ToDate;
		}
<?
	if ($intranet_version == "2.5" || 1) {
?>
		else if (document.form1.groupType[2].checked) { // Subject Group
			var SubjectGroupSelection = document.getElementById('SubjectGroup');
			if (SubjectGroupSelection.selectedIndex != 0) {
				var SubjectGroupValue = SubjectGroupSelection[SubjectGroupSelection.selectedIndex].value;
				
				window.location="take_attendance_subject_group.php?SubjectGroupID=" + SubjectGroupValue + "&period="+PeriodValue+"&ToDate="+ToDate;
			}
			else {
				alert('<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['SubjectGroup']?>');
			}
		}
<?}?>
		else { // Group
			var GroupValue = document.getElementById('group_id')[document.getElementById('group_id').selectedIndex].value;
			window.location="take_attendance_group.php?group_id=" + GroupValue + "&period="+PeriodValue+"&ToDate="+ToDate;
		}
	<?}?>
	}
        /*with(document.form1)
        {
                <? if($class_name) {?>
                window.location="?class_name=" + class_name.value + "&period="+period.value;
                <? } else {?>
                        if(document.form1.groupType[0].checked == true)
                        {
							
							if (typeof(document.form1.period[0])!="undefined")
							{
								if(document.form1.period[0].checked == true) {
									window.location="?class_name=" + class_name.value + "&period="+period[0].value;
								}
								if(document.form1.period[1].checked == true) {
									window.location="?class_name=" + class_name.value + "&period="+period[1].value;
								}
							} else
							{
								window.location="?class_name=" + class_name.value + "&period="+period.value;
							}
                        }else if(document.form1.groupType[1].checked == true)
                        {
							if (typeof(document.form1.period[0])!="undefined")
							{
								if(document.form1.period[0].checked == true) {
									window.location="take_attendance_group.php?group_id=" + group_id.value + "&period="+period[0].value;
								}
								if(document.form1.period[1].checked == true) {
									window.location="take_attendance_group.php?group_id=" + group_id.value + "&period="+period[1].value;
								}
							} else
							{
								window.location="take_attendance_group.php?group_id=" + group_id.value + "&period="+period.value;
							}
                        }
                        <? } ?>

        }*/
}

var reasonSelection = Array();
<?=$reason_js_array?>
        var posx = 0;
        var posy = 0;
function calMousePos(e) {

        if (!e) var e = window.event;
        if (e.pageX || e.pageY)         {
                posx = e.pageX;
                posy = e.pageY;
        }
        else if (e.clientX || e.clientY)         {
                posx = e.clientX;
                posy = e.clientY;
        }
}

function AbsToPre()
{
         obj = document.form1;
        len=obj.elements.length;
        var i=0;
        for( i=0 ; i<len ; i++) {
                if (obj.elements[i].name=="drop_down_status[]")
                {
                	if(obj.elements[i].style.display == 'none') continue;
                    obj2 = obj.elements[i];
                    if (obj2.selectedIndex == 1)
                    {
                        obj2.selectedIndex = 0;
                    }
                }
        }
        setEditAllowed();
}

function setMenu(value){
        isMenu=value;
}

function showSelection(i, allowed)
{
         if (allowed == 1)
         {
                 setMenu(true);
                 setToolTip(false);
                 writeToLayer('ToolMenu', reasonSelection[i]);
                 moveToolTip('ToolMenu',posy,posx );
                 showLayer('ToolMenu');
         }
}
/*
function showSelection2(pos)
{
             obj2 = document.getElementById("drop_down_status_"+pos);
             if (obj2.selectedIndex == 1)
             {
                 jArrayTemp[0] = "abc";
                 jArrayTemp[1] = "def";
             }
             else if (obj2.selectedIndex == 2)
             {
                 jArrayTemp[0] = "123";
                 jArrayTemp[1] = "456";
             }

}
*/

<?
# Grab reasons
?>
function getReasons(pos)
{
         var jArrayTemp = new Array();
             obj2 = document.getElementById("drop_down_status_"+pos);
             if (obj2.selectedIndex == 1)
             {
                 return jArrayWordsAbsence;
             }
             else if (obj2.selectedIndex == 2)
             {
                  return jArrayWordsLate;
             }
             else if (obj2.selectedIndex == 3)
             {
                  return jArrayWordsOuting;
             }
             else return jArrayTemp;
}

function putBack(obj, value)
{
         obj.value = value;
         hideMenu('ToolMenu');
}
function clearReason(i){
	if (document.getElementById('input_reason'+i)) {
		var CurStatus = document.getElementById('drop_down_status_'+i).value;
		var PrevStatus = document.getElementById('PrevStatus['+i+']').value;
		
		if (PrevStatus == 1) {
			document.getElementById('AbsentDefaultReason['+i+']').value = document.getElementById('input_reason'+i).value;
		}
		else if (PrevStatus == 2) {
			document.getElementById('LateDefaultReason['+i+']').value = document.getElementById('input_reason'+i).value;
		}
		else if (PrevStatus == 3) {
			document.getElementById('OutingDefaultReason['+i+']').value = document.getElementById('input_reason'+i).value;
		}
		
		if (CurStatus == 1) { // absent
			document.getElementById('input_reason'+i).value = document.getElementById('AbsentDefaultReason['+i+']').value;
		}
		else if (CurStatus == 2) { // late
			document.getElementById('input_reason'+i).value = document.getElementById('LateDefaultReason['+i+']').value;
		}
		else if (CurStatus == 3) { // Outing
			document.getElementById('input_reason'+i).value = document.getElementById('OutingDefaultReason['+i+']').value;
		} 
		
		if (CurStatus == 0)	// On Time
		{
			document.getElementById('div_reason_'+i).style.visibility='hidden';
		}
		else
		{
			document.getElementById('div_reason_'+i).style.visibility='visible';
		}
		
		document.getElementById('PrevStatus['+i+']').value = CurStatus;
	}
}

function setEditAllowed(i){
	var drop_down_list = document.getElementsByName('drop_down_status[]');
	if(drop_down_list==null)return;
	
	var i = i || "";
<? if(!$isStudentHelper){ ?>
	if (i == "") {
		var LoopStart = 0;
		var LoopTo = drop_down_list.length;
	}
	else {
		var LoopStart = i;
		var LoopTo = (i+1);
	}
  for(i=LoopStart;i<LoopTo;i++){
    editAllowedObj = eval('document.form1.editAllowed'+i);
    var statusObj = drop_down_list[i];
    var statusVal = $(statusObj).val();
    var office_remark_obj = $('#OfficeRemark'+i);
	if(office_remark_obj.length>0){
		statusVal != '<?=CARD_STATUS_PRESENT?>'? office_remark_obj.show(): office_remark_obj.hide();
	}
    if(editAllowedObj==null || statusObj==null) return;

		if(statusObj.selectedIndex==1||statusObj.selectedIndex==2||statusObj.selectedIndex==3){
			editAllowedObj.value=1;
		}
		else {
			editAllowedObj.value=0;
		}
		
		if (document.getElementById('Waived'+i)) {
			if(editAllowedObj.value == 1 && statusObj.selectedIndex!=3){
				document.getElementById('Waived'+i).style.display = "";
			}
			else 
				document.getElementById('Waived'+i).style.display = "none";
		}
		/* commented by kenneth on 20100726
		if (document.getElementById('reason'+i)) {
    	reasonObj = document.getElementById('reason'+i);
      reasoniconObj = document.getElementById('posimg'+i);
      if(editAllowedObj.value == 1){
        reasonObj.style.display="";
				reasoniconObj.style.display="";
      }else{
        reasonObj.style.display="none";
				reasoniconObj.style.display="none";
      }
    }
    */
    if (document.getElementById('input_reason'+i))
		{
      inputReasonObj = document.getElementById('input_reason'+i)
      inputReasoniconObj = document.getElementById('posimg_'+i);
      if(editAllowedObj.value == 1){
        inputReasonObj.style.display="";
        if (inputReasoniconObj)
					inputReasoniconObj.style.display="";
      }else{
        inputReasonObj.style.display="none";
        if (inputReasoniconObj)
					inputReasoniconObj.style.display="none";
      }
    }
    if(document.getElementById('HandIn'+i)){
    	document.getElementById('HandIn'+i).style.display = statusVal == '<?=CARD_STATUS_ABSENT?>'?'':'none';
    }
    <?php if($sys_custom['SmartCardAttendance_SubmittedProveDocumentForEarlyLeave']){ ?>
    if(document.getElementById('HandInEL'+i)){
    	document.getElementById('HandInEL'+i).style.display = statusVal == '<?=CARD_STATUS_ABSENT?>'?'none':'';
    }
    <?php } ?>
    //if(editAllowedObj.value==0)
    // All status change hide menu
    // hideMenu('ToolMenu');

    if(document.getElementById('listContent'))
    {
    	obj2 = document.getElementById('listContent').style.display='none';
    	setDivVisible(false, 'listContent', 'lyrShim');
		}
    
    <?if($sys_custom['SmartCardAttendance_StudentAbsentSession']){?>
    	if(statusObj.selectedIndex == 1 || statusObj.selectedIndex == 2)
    	{
    		absentSessionObj = document.getElementById('absent_session'+i);
    		if(absentSessionObj != null)
    		{
    			absentSessionObj.style.display = "inline";
    		}
    	}else
    	{
    		absentSessionObj = document.getElementById('absent_session'+i);
    		if(absentSessionObj != null)
    		{
    			absentSessionObj.style.display = "none";
    		}
    	}
    	
		if(!global_do_not_init_default_absent_session){
    		ChangeDefaultSessionSelect(i);
		}
    <?}?>
  }
<? } ?>
}

/*
function setEditAllowed_single(i){
	<? if(!$isStudentHelper){ ?>
        drop_down_list = document.getElementsByName('drop_down_status[]');
        if(drop_down_list==null)return;

                editAllowedObj = eval('document.form1.editAllowed'+i);
                statusObj = drop_down_list[i];
                if (eval('document.form1.reason'+i)) {
                        reasonObj = eval('document.form1.reason'+i);
                        reasoniconObj = eval('document.form1.posimg'+i);
                      }
                if(editAllowedObj==null || statusObj==null) return;

                //if(statusObj.selectedIndex==1||statusObj.selectedIndex==2){
                if(statusObj.selectedIndex >= 0){
                        editAllowedObj.value=1;
                        if(typeof(reasonObj) != 'undefined'){
                                reasonObj.disabled=false;
                                //reasoniconObj.style.display="inline";
                        }
                       if(typeof(reasoniconObj) != 'undefined'){
                                         reasoniconObj.style.display="inline";
                            }
                            reasonObj.style.background='#FFFFFF';

                }else{
                        editAllowedObj.value=0;
                        if(typeof(reasonObj) != 'undefined'){
                                reasonObj.value="";
                                reasonObj.disabled = true;
                                //reasoniconObj.style.display="none";
                        }
                        if(typeof(reasoniconObj) != 'undefined'){
                                         reasoniconObj.style.display="none";
                            }
                            reasonObj.style.background='#DFDFDF';
                }
                //if(editAllowedObj.value==0)
                // All status change hide menu
                // hideMenu('ToolMenu');
                if(typeof(listContent) != 'undefined')
                {
                	obj2 = document.getElementById('listContent').style.display='none';
                	setDivVisible(false, 'listContent', 'lyrShim');
            	}
            	
              <?if($sys_custom['SmartCardAttendance_StaffInputReason'] == true){?>
              	if(statusObj.selectedIndex==0)
              	{
              		if (eval('document.form1.input_reason'+i))
              		{
                        inputReasonObj = eval('document.form1.input_reason'+i);
                        inputReasoniconObj = document.getElementById('posimg_'+i);
                        packimage = document.getElementById('posimg_pack'+i);
                        if(typeof(inputReasonObj) != 'undefined')
                        {
                        	inputReasonObj.value="";
                            inputReasonObj.disabled = true;
                            inputReasonObj.style.background='#DFDFDF';
                        }
                        if(inputReasoniconObj != null)
                        {
                            inputReasoniconObj.style.display="none";
                        }
                        if(packimage != null)
                        {
                        	packimage.style.display = "inline";
                        }
                    }
              	}else
              	{
              		if (eval('document.form1.input_reason'+i))
              		{
                        inputReasonObj = eval('document.form1.input_reason'+i);
                        inputReasoniconObj = document.getElementById('posimg_'+i);
                        packimage = document.getElementById('posimg_pack'+i);
                        if(typeof(inputReasonObj) != 'undefined')
                        {
                            inputReasonObj.value="";
                            inputReasonObj.disabled = false;
                            inputReasonObj.style.background='#FFFFFF';
                        }
                        if(inputReasoniconObj != null)
                        {
                            inputReasoniconObj.style.display="inline";
                        }
                        if(packimage != null)
                        {
                        	packimage.style.display = "none";
                        }
                    }
              	}
              <?}?>
              
              <?if($sys_custom['SmartCardAttendance_StudentAbsentSession']){?>
              	if(statusObj.selectedIndex == 1 || statusObj.selectedIndex == 2)
              	{
              		absentSessionObj = document.getElementById('absent_session'+i);
              		if(absentSessionObj != null)
              		{
              			absentSessionObj.style.display = "inline";
              		}
              	}else
              	{
              		absentSessionObj = document.getElementById('absent_session'+i);
              		if(absentSessionObj != null)
              		{
              			absentSessionObj.style.display = "none";
              		}
              	}
              <?}?>
	<?}?>

}
*/

function resetForm(formObj){
  formObj.reset();
  setEditAllowed();
}

// for Preset Absence
function showPresetAbs(obj,reason){
	var pos_left = getPostion(obj,"offsetLeft");
	var pos_top  = getPostion(obj,"offsetTop");
	
	offsetX = (obj==null)?0:obj.width;
	offsetY =0;
	objDiv = document.getElementById('ToolTip3');
	if(objDiv!=null){
		objDiv.innerHTML = reason;
		objDiv.style.visibility='visible';
		objDiv.style.top = pos_top+offsetY + 'px';
		objDiv.style.left = pos_left+offsetX + 'px';
		setDivVisible(true, "ToolTip3", "lyrShim");
	}

}

// for preset absence
function hidePresetAbs(){
                obj = document.getElementById('ToolTip3');
                if(obj!=null)
                        obj.style.visibility='hidden';
                setDivVisible(false, "ToolTip3", "lyrShim");
}
<?php if($sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable']){ ?>
function toggle(x)
{
	if(x==1) { // class
		document.getElementById('class_name').style.display="";
		document.getElementById('group_id').style.display="none";
		$('#SubjectGroupSelectLayer').css('display',"none");
	} 
	else if (x == "SubjectGroup") {
		document.getElementById('class_name').style.display="none";
		document.getElementById('group_id').style.display="none";
		$('#SubjectGroupSelectLayer').css('display',"");
	}
	else { // attendance group
		document.getElementById('class_name').style.display="none";
		document.getElementById('group_id').style.display="";
		$('#SubjectGroupSelectLayer').css('display',"none");
	}
}
<?php }else{ ?>
function toggle(x)
{
	if(x==1) { // class
		$('#TrClassSelection').show();
		$('#TrGroupSelection').hide();
		$('#TrSubjectSelection').hide();
		$('#TrSubjectGroupSelection').hide();
	} 
	else if (x == "SubjectGroup") {
		$('#TrClassSelection').hide();
		$('#TrGroupSelection').hide();
		$('#TrSubjectSelection').show();
		if($('#SubjectID').val() == ''){
			$('#TrSubjectGroupSelection').hide();
		}else{
			$('#TrSubjectGroupSelection').show();
		}
	}
	else { // attendance group
		$('#TrClassSelection').hide();
		$('#TrGroupSelection').show();
		$('#TrSubjectSelection').hide();
		$('#TrSubjectGroupSelection').hide();
	}
}
<?php } ?>

function changeSlot(val, class_name)
{
	/*if(val != ""){
		self.location = "take_attendance.php?class_name="+class_name+"&period="+val;
	}*/
}

// lesson attendance
{
var LessonAttendReturnMessage = '<?=$Lang['LessonAttendance']['LessonAttendanceDataSavedSuccess']?>';
	
function Get_School_Layer() {
	document.getElementById('LessonAttendanceLayer').style.display = "none";
	document.getElementById('SchoolAttendanceLayer').style.display = "";
}

function Get_Lesson_Layer() {
	document.getElementById('LessonAttendanceLayer').style.display = "";
	document.getElementById('SchoolAttendanceLayer').style.display = "none";
}

function View_Session() {
	if ($('#DPWL-TargetDate').html() == "" && document.getElementById('SubjectGroupSession')) {
		var OptionIndex = document.getElementById('SubjectGroupSession').selectedIndex;
		if (OptionIndex > 0) {
			//Block_Element("LoadingLayer");
			var SessionSelected = document.getElementById('SubjectGroupSession').options[OptionIndex].value;
			var LessonDetail = SessionSelected.split('|=|');
			//var FlashVars = new Array();
			window.location.href="?SubjectGroupID="+LessonDetail[0]+"&SubjectGroupName="+LessonDetail[1]+"&RoomAllocationID="+LessonDetail[3]+"&StartTime="+LessonDetail[4]+"&EndTime="+LessonDetail[5]+"&LessonDate="+LessonDetail[2]<?=$LessonAttendanceSettings['TeacherCanTakeAllLessons']==1?'+"&LessonType="+$("input[name=LessonType]:checked").val()':''?>;
			
//			LessonAttendFlashVars = {
//				RecordID: LessonDetail[0],
//				Name: LessonDetail[1],
//				LessonDate: LessonDetail[2],
//				RoomAllocationID: LessonDetail[3],
//				StartTime: LessonDetail[4],
//				EndTime: LessonDetail[5], 
//				ServerPath: "<?='http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT']?>", 
//				RecordType:"SubjectGroup",
//				DefaultLang:"<?=$intranet_session_language?>"
//			}
//			
//			$("div#LessonAttendanceFlashLayer").load('ajax_get_lesson_attendance_panel.php',LessonAttendFlashVars,function() {UnBlock_Element('LoadingLayer');});
			
//			/*FlashVars['SubjectGroupID'] = LessonDetail[0];
//			FlashVars['LessonDate'] = LessonDetail[1];
//			FlashVars['RoomAllocationID'] = LessonDetail[2];
//			FlashVars['StartTime'] = LessonDetail[3];
//			FlashVars['EndTime'] = LessonDetail[4];*/
//			embedFlash("LessonAttendanceFlashLayer", "lesson_attendance/main1.swf", LessonAttendFlashVars, "#869ca7", "100%", "900px", "9.0.0", "NO");
//			UnBlock_Element("LoadingLayer");
		}
	}
}
// lesson attendance
function Change_Single_Status(index, obj)
{
	obj.className=obj.options[obj.selectedIndex].className;
	var status = $(obj).val();
	if(status > 0)
	{
		$('#reasonSpan'+index).css('visibility','visible');
	}else{
		$('#reasonSpan'+index).css('visibility','hidden');
		$('#reason'+index).val('');
	}
}
//copied from lesson_attendance_panel.php
function Change_All_Status(status){
	//if(status >= 0){
		/*
		var class1 = '';
		var class2 = '';
		if(status == 0){
			class1 = 'greenText';
			class2 = 'blueText orangeText redText';
		}
		else if(status == 1){
			class1 = 'blueText';
			class2 = 'greenText orangeText redText';
		}
		else if(status == 2){
			class1 = 'orangeText';
			class2 = 'blueText greenText redText';
		}
		else{
			class1 = 'redText';
			class2 = 'blueText orangeText greenText';
		}
		$(".drop_down_status").val(status);
		$("#all_drop_down_status").val('-1');
		$(".drop_down_status").removeClass(class2).addClass(class1);
		*/
		//$("#all_drop_down_status").val('-1');
		var objs = document.getElementsByName('drop_down_status[]');
<?php if($sys_custom['LessonAttendance_LaSalleCollege']){ ?>
		var cbs = document.getElementsByName('checkbox[]');
		for(var i=0;i<objs.length;i++)
		{
			if(cbs[i].checked)
			{
				var obj = $(objs[i]);
				obj.val(status);
				Change_Single_Status(i, objs[i]);
			}
		}
<?php }else{ ?>
		for(var i=0;i<objs.length;i++)
		{
			var obj = $(objs[i]);
			obj.val(status);
			//objs[i].className = objs[i].options[objs[i].selectedIndex].className;
			Change_Single_Status(i, objs[i]);
		}
<?php } ?>
	//}
}

function getLessonAttendanceReasons(pos)
{
	var returnArray = new Array();
	var status = $('#drop_down_status\\['+pos+'\\]').val();
	returnArray = eval('jArrayLessonReasons'+status);
	return returnArray;
}


function Check_Form() {
	//alert("hihi");
// Must be applied the input checking
//	var LateOK = true;
//	$('div.LateTimeWarningLayer').each(function() {
//		if ($(this).html() != "") {
//			LateOK = false;
//		}
//	});
//	
//	if (LateOK) {
<?php if($sys_custom['LessonAttendance_LaSalleCollege']){ ?>
		var selection_objs = document.getElementsByName('drop_down_status[]');
		var all_status_selected = true; 
		for(var i=0;i<selection_objs.length;i++){
			if(selection_objs[i].options[selection_objs[i].selectedIndex].value == '-1'){
				all_status_selected = false;
				break;
			}
		}
		if(!all_status_selected){
			alert('<?=$Lang['LessonAttendance']['RequestSelectAllStatus']?>');
			return;
		}
<?php } ?>
		var qs = "<?=remove_qs_key($_SERVER['QUERY_STRING'], "Msg")?>";
		AlertPost(document.form3,'../../eAdmin/StudentMgmt/attendance/dailyoperation/daily_lesson_attendance/lesson_attendance_panel_update.php?flag=2&'+qs,'<?=$i_SmartCard_Confirm_Update_Attend?>');
//	}
}
function Cancel_Form() {
		var qs = "<?=remove_qs_key($_SERVER['QUERY_STRING'], "Msg")?>";
		//AlertPost(document.form3,'../../eAdmin/StudentMgmt/attendance/dailyoperation/daily_lesson_attendance/lesson_attendance_panel_update.php?flag=2&'+qs,'<?=$i_SmartCard_Confirm_Update_Attend?>');
		window.location.href='take_attendance.php';
}
<?function remove_qs_key($url, $key) {
	$url = preg_replace('/(?:&|(\?))' . $key . '=[^&]*(?(1)&|)?/i', "$1", $url);
	return $url;
}?>
function drop_down_status_onchange() {
	setTimeout(function(){
        $(".drop_down_status").change();
    }, 50);
   
}
function Show_Time_Slot_Layer(LayerID,e) {
	var tempX = 0;
	var tempY = 0;
	
	if (document.all) {
		tempX = event.clientX + document.body.scrollLeft;
		tempY = event.clientY + document.body.scrollTop;
	} else {
		tempX = e.pageX;
		tempY = e.pageY;
	}
	
	if (tempX < 0){tempX = 0} else {tempX = tempX}
	if (tempY < 0){tempY = 0} else {tempY = tempY}
	
	$('span.member_list_group_layer').slideUp(); 
	$('span#'+LayerID).css('left',(tempX-200)+'px');
	//$('span#'+LayerID).css('top',tempY+'px');
	$('span#'+LayerID).slideDown();
}
function PrintPage(AttendOverviewID,LessonDate)
{
    //newWindow("../../eAdmin/StudentMgmt/attendance/dailyoperation/daily_lesson_attendance/lesson_attendance_panel_print.php?SubjectGroupID=<?=urlencode($SubjectGroupID)?>&LessonDate="+LessonDate+"&AttendOverviewID="+AttendOverviewID, 12);
    var subject_group_id = $('input[name="SubjectGroupID"]').val();
    newWindow("../../eAdmin/StudentMgmt/attendance/dailyoperation/daily_lesson_attendance/lesson_attendance_panel_print.php?SubjectGroupID="+subject_group_id+"&LessonDate="+LessonDate+"&AttendOverviewID="+AttendOverviewID, 12);
}
//End of the copied from lesson_attendance_panel.php

function Get_Session_List(TargetDate1) {
	var re = /\b\d{4}[\/-]\d{1,2}[\/-]\d{1,2}\b/;
	if (re.test(TargetDate1)) {
		$('#LoadingLayer').html('<div id="LessonAttendanceFlashLayer"></div>');
		
		var PostVar = {
			TargetDate: TargetDate1
		<?php if($LessonAttendanceSettings['TeacherCanTakeAllLessons']==1){ ?>
			,LessonType: $('input[name=LessonType]:checked').val() 
		<?php } ?>
			}
		
		$('span#SessionSelectLayer').load("ajax_get_lesson_session_select.php",PostVar);
	}
	else {
		return false;
	}
}

function Get_Subject_Group_List(TargetDate) {
	if ($('#DPWL-ToDate').html() == "") {
		var PostVar = {
			"TargetDate": TargetDate
			}
		
		$('span#SubjectGroupSelectLayer').load("ajax_get_subject_group_select.php",PostVar);
	}
	else
		return false;
}
}


// generate options from jArrData and bold selected option
function jSelectPresetReasonText(jArrData, jTarget, jMenu, jPos){
	var listStr = "";
	var fieldValue;
	var count = 0;

	var btn_hide = (typeof(js_btn_hide)!="undefined") ? js_btn_hide : "";
	var box_height = (jArrData.length>10) ? "200" : "105";

	listStr = "<table width='160' height='100' border='0' cellpadding='0' cellspacing='0'>";
	listStr += "<tr>";
	listStr += "<td height='19'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	listStr += "<tr>";
	listStr += "<td width='5' height='19'><img src='"+jImagePath+"/can_board_01.gif' width='5' height='19'></td>";
	listStr += "<td height='19' valign='middle' background='"+jImagePath+"/can_board_02.gif'></td>";
	listStr += "<td width='19' height='19'><a href='javascript:void(0)' onClick=\"document.getElementById('" + jMenu + "').style.display='none';setDivVisible(false, '"+jMenu+"', 'lyrShim')\"><img src='"+jImagePath+"/can_board_close_off.gif' name='pre_close' width='19' height='19' border='0' id='pre_close' onMouseOver=\"MM_swapImage('pre_close','','"+jImagePath+"/can_board_close_on.gif',1)\" onMouseOut='MM_swapImgRestore()' /></a></td>";
	listStr += "</tr>";
	listStr += "</table></td>";
	listStr += "</tr>";
	listStr += "<tr>";
	listStr += "<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	listStr += "<tr>";
	listStr += "<td width='5' height='50' background='"+jImagePath+"/can_board_04.gif'><img src='"+jImagePath+"/can_board_04.gif' width='5' height='19'></td>";
	
	// 20090907 Ivan
	// Problem: Scroll bar disappeared if the user open the layer more than one times
	// Temp Solution: Always show the scroll bar - changed style from overflow:auto; to overflow: scroll;
	listStr += "<td bgcolor='#FFFFF7'><div style=\"overflow-X: scroll; overflow-Y: scroll; height: " + box_height + "px; width: 160px; \">";
	
	listStr += "<table width='180' border='0' cellspacing='0' cellpadding='2' class='tbclip'>";
	if (typeof(jArrData)=="undefined" || jArrData.length==0)
	{
		if (typeof(js_preset_no_record)!="undefined")
		{
			listStr += "<tr><td class='guide' align='center'>"+js_preset_no_record+"</td></tr>";
		}
	} else
	{
		for (var i = 0; i < jArrData.length; i++)
		{
			for (var j = 0; j < jArrData[i].length; j++)
			{
				fieldValue = document.getElementById(jTarget).value.replace(/&/g, "&amp;").replace(/'/g, "&#96;").replace(/`/g, "&#96;").replace(/"/g, "&quot;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
				tmpValue = jArrData[i][j].replace(/&#039;/g, "\\'");
				tmpShowValue = jArrData[i][j].replace(/</g, "&lt;").replace(/>/g, "&gt;");

				listStr += "<tr><td style=\"border-bottom:1px #EEEEEE solid; background-color: ";
				if (fieldValue == jArrData[i][j])
				{
					listStr += "#FFF2C3";
				} else
				{
					listStr += "#FFFFFF";
				}
				listStr += "\">";

				if (fieldValue == jArrData[i][j])
				{
					listStr += "<b>";
				}
				listStr += "<a class='presetlist' href=\"javascript:document.getElementById('" + jTarget + "').value = '" + tmpValue + "' ;document.getElementById('" + jMenu + "').style.display= 'none';setDivVisible(false, '"+jMenu+"', 'lyrShim')\" title=\"" + jArrData[i][j] + "\">" + tmpShowValue + "</a>";
				
				if (fieldValue == jArrData[i][j])
				{
					listStr += "</b>";
				}

				listStr += "</td></tr>";
				count ++;
			}
		}
	}
	listStr += "</table></div></td>";
	listStr += "<td width='6' background='"+jImagePath+"/can_board_06.gif'><img src='"+jImagePath+"/can_board_06.gif' width='6' height='6' border='0' /></td>";
	listStr += "</tr>";
	listStr += "<tr>";
	listStr += "<td width='5' height='6'><img src='"+jImagePath+"/can_board_07.gif' width='5' height='6'></td>";
	listStr += "<td height='6' background='"+jImagePath+"/can_board_08.gif'><img src='"+jImagePath+"/can_board_08.gif' width='100%' height='6' border='0' /></td>";
	listStr += "<td width='6' height='6'><img src='"+jImagePath+"/can_board_09.gif' width='6' height='6'></td>";
	listStr += "</tr>";
	listStr += "</table></td>";
	listStr += "</tr>";
	listStr += "</table>";

	document.getElementById(jMenu).innerHTML = listStr;
	document.getElementById(jMenu).style.visibility = 'visible';

	var pos = jFindPos(document.getElementById("posimg_" + jPos));

	document.getElementById(jMenu).style.left = parseInt(pos[0]) + "px";
	document.getElementById(jMenu).style.top = parseInt(pos[1]) + "px";

	setDivVisible(true, jMenu, "lyrShim");

}

function ChangeDefaultSessionSelect(i) {
	var IsConfirmedAlready = document.getElementById('Confirmed['+i+']');
	var ignore_confirm_check = <?=(($sys_custom['SmartCardAttendance_StudentAbsentSessionToggleSessionIgnoreConfirm'] == true) ? 'true' : 'false')?>;
    if ((IsConfirmedAlready && IsConfirmedAlready.value == "") || ignore_confirm_check == true) {
		var drop_down_list = document.getElementsByName('drop_down_status[]');
		if(drop_down_list==null)return;
		var StatusObj = drop_down_list[i];
		var LateSessionObj = document.getElementById('late_session'+i);
		var OfficalLeaveSessionObj = document.getElementById('offical_leave_session'+i);
		var RequestLeaveSessionObj = document.getElementById('request_leave_session'+i);
		var PlayTruantSessionObj = document.getElementById('play_truant_session'+i);
		if (LateSessionObj) {
			if (StatusObj.selectedIndex == 1) {
				OfficalLeaveSessionObj.selectedIndex = 0;
				LateSessionObj.selectedIndex = 0;
				//RequestLeaveSessionObj.selectedIndex = 2;
				//PlayTruantSessionObj.selectedIndex = 0;
				<?if($sys_custom['SmartCardAttendance_StudentAbsentSession'] && isset($DefaultNumOfSessionSettings['DefaultNumOfSessionForLeave']) && $DefaultNumOfSessionSettings['DefaultNumOfSessionForLeave'] != -1){?>
					RequestLeaveSessionObj.selectedIndex = <?=($DefaultNumOfSessionSettings['DefaultNumOfSessionForLeave']*2)?>;
				<?}else{?>
					RequestLeaveSessionObj.selectedIndex = 2;
				<?}?>
				<?if($sys_custom['SmartCardAttendance_StudentAbsentSession'] && isset($DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsenteesism']) && $DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsenteesism'] != -1){?>
					PlayTruantSessionObj.selectedIndex = <?=($DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsenteesism']*2)?>;
				<?}else{?>
					PlayTruantSessionObj.selectedIndex = 0;
				<?}?>
			}
			else if (StatusObj.selectedIndex == 2) {
				OfficalLeaveSessionObj.selectedIndex = 0;
				//LateSessionObj.selectedIndex = 2;
				<?if($sys_custom['SmartCardAttendance_StudentAbsentSession'] && isset($DefaultNumOfSessionSettings['DefaultNumOfSessionForLate']) && $DefaultNumOfSessionSettings['DefaultNumOfSessionForLate'] != -1){?>
					LateSessionObj.selectedIndex = <?=($DefaultNumOfSessionSettings['DefaultNumOfSessionForLate']*2)?>;
				<?}else{?>
					LateSessionObj.selectedIndex = 2;
				<?}?>
				RequestLeaveSessionObj.selectedIndex = 0;
				PlayTruantSessionObj.selectedIndex = 0;
			}
			else {
				OfficalLeaveSessionObj.selectedIndex = 0;
				LateSessionObj.selectedIndex = 0;
				RequestLeaveSessionObj.selectedIndex = 0;
				PlayTruantSessionObj.selectedIndex = 0;
			}
		}
		<?php if($sys_custom['SmartCardAttendance_StudentAbsentSession2']){ ?>
		var AbsentSessionObj = document.getElementById('absent2_session'+i);
		if(AbsentSessionObj){
			AbsentSessionObj.selectedIndex = 0;
			if(StatusObj.selectedIndex == 1) {
				<?php if(isset($DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsent']) && $DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsent'] != -1){ ?>
				AbsentSessionObj.selectedIndex = <?=($DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsent']*2)?>;
				<?php } ?>
			}
		}
		<?php } ?>
	}
}

<?php if(!$sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable']){ ?>
function reloadSubject(TargetDate)
{
	var PostVar = { 'TargetDate': TargetDate };
	$('span#SubjectSelectLayer').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$('span#SubjectSelectLayer').load("ajax_get_subject_select.php",PostVar);
}
	
function subjectChanged(subjectId)
{
	var PostVar = { "SubjectID": subjectId };
	if ($('#DPWL-ToDate').html() == "") {
		PostVar["TargetDate"] = $('#ToDate').val();
	}
	$('span#SubjectGroupSelectLayer').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$('span#SubjectGroupSelectLayer').load("ajax_get_subject_group_select.php",PostVar);
	if(subjectId == ''){
		$('#SubjectGroup').val('');
		$('#TrSubjectGroupSelection').hide();
	}else{
		$('#TrSubjectGroupSelection').show();
	}
}
<?php } ?>

function reloadClassSelection(callback)
{
	$.post(
		'ajax_get_class_select.php',
		{
			'TargetDate': $('#ToDate').val(),
			'class_name': encodeURIComponent($('select#class_name').val())
		},
		function(html){
			$('select#class_name').replaceWith(html);
			if(typeof(callback) == 'function'){
				callback.call();
			}
			var selectedGroupType = $('input[name="groupType"]:checked').val();
			toggle(selectedGroupType);
		}
	);
}

function reloadGroupSelection(callback)
{
	$.post(
		'ajax_get_group_select.php',
		{
			'TargetDate': $('#ToDate').val(),
			'group_id': $('select#group_id').val() 
		},
		function(html){
			$('select#group_id').replaceWith(html);
			if(typeof(callback) == 'function'){
				callback.call();
			}
			var selectedGroupType = $('input[name="groupType"]:checked').val();
			toggle(selectedGroupType);
		}
	);
}

function onDateValueChanged()
{
	var groupType = $('input[name="groupType"]:checked').val();
	if(groupType == '1'){
		reloadClassSelection(retrivePMTime);
		reloadGroupSelection(null);
	}else if(groupType == '2'){
		reloadClassSelection(null);
		reloadGroupSelection(retrivePMTime);
	}else{
		reloadClassSelection(null);
		reloadGroupSelection(null);
		retrivePMTime();
	}
}

function SetAllAttend()
{
	var apply_all_value = $('#drop_down_status_all').val();
	var status_selections = document.getElementsByName('drop_down_status[]');
	for(var i=0;i<status_selections.length;i++){
		if(status_selections[i].style.display == 'none') continue;
		$(status_selections[i]).val(apply_all_value);
		clearReason(i);
		setEditAllowed(i);
		ChangeDefaultSessionSelect(i);
	}
}

function setAllHandin(formObj,val)
{
	if(formObj==null) return;
	for(i=0;i<formObj.elements.length;i++){
		obj = formObj.elements[i];
		if(typeof(obj)!="undefined"){
			if(obj.name.indexOf("HandIn_prove_")>-1){
				obj.checked = val==1?true:false;
			}
		}
	}
}
<?php if($sys_custom['SmartCardAttendance_SubmittedProveDocumentForEarlyLeave']){ ?>
function setAllHandinEL(formObj,val)
{
	if(formObj==null) return;
	for(i=0;i<formObj.elements.length;i++){
		obj = formObj.elements[i];
		if(typeof(obj)!="undefined"){
			if(obj.name.indexOf("HandInEL_prove_")>-1){
				obj.checked = val==1?true:false;
			}
		}
	}
}
<?php } ?>

function SetAllRemarks() 
{
	var StatusSelection = document.getElementsByName("drop_down_status[]");
	var ApplyRemark = document.getElementById("SetAllRemark").value;
	for (var i=0; i< StatusSelection.length; i++) {
		document.getElementById('reason'+i).value = ApplyRemark;
	}
}

function getRemarks(pos)
{
	return RemarksArrayWords;
}

<?php if($sys_custom['StudentAttendance']['AllowEditTime']){ ?>
function onRevealEditTime(uid, time)
{
	var text_field = '<input type="text" id="InSchoolTime'+uid+'" name="InSchoolTime'+uid+'" value="'+time+'" size="10" onchange="onEditTimeChanged('+uid+')" />';
	
	var text_obj = $('#InSchoolTime'+uid);
	if(text_obj && text_obj.length > 0)
	{
		text_obj.val(time);
	}else{
		$('#cancelTimeBtn'+uid).before(text_field);
	}
	
	$('#InSchoolTime_Display'+uid).hide();
	$('#InSchoolTime_Edit'+uid).show();
}

function onCancelEditTime(uid)
{
	$('#InSchoolTime_Display'+uid).show();
	$('#InSchoolTime_Edit'+uid).hide();
	$('#InSchoolTime'+uid).remove();
}

function onEditTimeChanged(uid)
{
	var obj = $('#InSchoolTime'+uid);
	var val = $.trim(obj.val());
	if(val != ''){
		if(!val.match(/^\d\d:\d\d:\d\d$/g)){
			obj.val('');
			return;
		}
		var parts = val.split(':');
		var hr = Math.max(0, Math.min(24, parseInt(parts[0])));
		var min = Math.max(0, Math.min(60, parseInt(parts[1])));
		var sec = Math.max(0, Math.min(60, parseInt(parts[2])));
		
		var val_str = hr < 10? '0' + hr : '' + hr;
		val_str += ':';
		val_str += min < 10? '0' + min : '' + min;
		val_str += ':';
		val_str += sec < 10? '0' + sec : '' + sec;
		obj.val(val_str);
	}
}
<?php } ?>
</SCRIPT>
<?=$teacher_remark_js?>
<script>
var global_do_not_init_default_absent_session = <?php echo $sys_custom['SmartCardAttendance_StudentAbsentSessionToggleSessionIgnoreConfirm']?'true':'false'?>;
setEditAllowed();
global_do_not_init_default_absent_session = false;
<? if ($class_name == "") { ?>
toggle(<?=$groupType?>);
<? } ?>
<?=$js_redirect_hompage?>
</script>
<style>
.greenText{ color:#339900; font-weight: bold;}
.blueText{ color:#0066FF; font-weight: bold;}
.redText{ color:red; font-weight: bold;}
.orangeText{ color:#FF9900; font-weight: bold;}
</style>
<?php if($sys_custom['StudentAttendance']['NoCardPhoto']){ ?>
<link rel="stylesheet" href="/templates/jquery/fancybox-1.3.4/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/jquery/fancybox-1.3.4/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="/templates/jquery/fancybox-1.3.4/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="/templates/jquery/fancybox-1.3.4/jquery.fancybox-1.3.4.js"></script>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	$('a.fancybox').fancybox();
});
</script>
<?php } ?>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>