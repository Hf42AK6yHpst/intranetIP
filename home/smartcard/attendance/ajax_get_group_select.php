<?php
// Editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if(!isset($TargetDate) || $TargetDate == ''){
	$TargetDate = date("Y-m-d");
}

$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();

$group_list = $lcardattend->getGroupListToTakeAttendanceByDate($ToDate);

### Group select
$group_select = "<SELECT id=\"group_id\" name=\"group_id\" onChange=\"retrivePMTime();\">\n";
$empty_selected = ($group_id == '')? "SELECTED":"";
$group_select .= "<OPTION value='' $empty_selected> -- $button_select -- </OPTION>\n";
for($i=0; $i<sizeOf($group_list); $i++)
{
        list($itr_group_id, $itr_group_name) = $group_list[$i];
        $selected = ($itr_group_id==$group_id)?"SELECTED":"";
        $group_select .= "<OPTION value=\"".$itr_group_id."\" $selected>".$itr_group_name."</OPTION>\n";
}
$group_select .= "</SELECT>\n";

echo $group_select;

intranet_closedb();
?>