<?
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_opendb();

$RecordID = IntegerSafe($_REQUEST['RecordID']);
$RecordType = $_REQUEST['RecordType'];

$li = new libdb();

if ($RecordType == "SubjectGroup") {
	if ($RecordID != "") {
		$sql = 'delete from ATTEND_DEFAULT_PLAN where SubjectGroupID = \''.$RecordID.'\'';
		$li->db_db_query($sql);
		
		$sql = 'select 
							AttendOverviewID 
						From 
							SUBJECT_GROUP_ATTEND_OVERVIEW_'.Get_Current_Academic_Year_ID().' 
						where SubjectGroupID = \''.$RecordID.'\'';
		$AttendOverviewID = $li->returnVector($sql);
		
		$sql = 'delete from SUBJECT_GROUP_ATTEND_OVERVIEW_'.Get_Current_Academic_Year_ID().' where SubjectGroupID = \''.$RecordID.'\'';
		$li->db_db_query($sql);
		
		if (sizeof($AttendOverviewID) > 0) {
			$Cond = implode(',',$AttendOverviewID);
			
			$sql = 'delete from SUBJECT_GROUP_STUDENT_ATTENDANCE_'.Get_Current_Academic_Year_ID().' where AttendOverviewID in ('.$Cond.')';
			$li->db_db_query($sql);
		}
	}
}
else {
	if ($RecordID != "") {
		$sql = 'delete from ATTEND_DEFAULT_PLAN where YearClassID = \''.$RecordID.'\'';
		$li->db_db_query($sql);
	}
}

intranet_closedb();
?>