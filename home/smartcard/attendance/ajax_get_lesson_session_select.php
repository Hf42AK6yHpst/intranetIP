<?
// Editing by 
/*
 * 2013-08-20 (Carlos): Display "You have no lesson to take. " if no session to select
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_opendb();

$GeneralSetting = new libgeneralsettings();
$LessonAttendanceSettingList = array("'TeacherCanTakeAllLessons'");
$LessonAttendanceSettings = $GeneralSetting->Get_General_Setting('LessonAttendance',$LessonAttendanceSettingList);

if(isset($_REQUEST['LessonType']) && $LessonAttendanceSettings['TeacherCanTakeAllLessons']==1) {
	$take_all_lesson = $_REQUEST['LessonType']==1?1:0;
}else{
	$take_all_lesson = 0;
}

$lcardattend = new libcardstudentattend2();
$SubjectGroupSessionList = $lcardattend->Get_Subject_Group_Session($_SESSION['UserID'],$TargetDate,"",$take_all_lesson);

if(count($SubjectGroupSessionList)==0) {
	echo $Lang['LessonAttendance']['NoLessonToTake'];
	intranet_closedb();
	exit;
}
## subject Group session select
$SubjectGroupSelect = "<SELECT id=\"SubjectGroupSession\" name=\"SubjectGroupSession\" onchange=\"\">\n";
$empty_selected = ($SubjectGroupSession == '')? "SELECTED":"";
$SubjectGroupSelect .= "<OPTION value='' $empty_selected> -- $button_select -- </OPTION>\n";
for($i=0; $i<sizeOf($SubjectGroupSessionList); $i++)
{
  list($SubjectGroupID,$ClassTitleEN,$ClassTitleB5,$RoomAllocationID,$TimeSlotName,$StartTime,$EndTime,$ClassTitleEN,$ClassTitleB5) = $SubjectGroupSessionList[$i];
  $ClassName = Get_Lang_Selection($ClassTitleB5,$ClassTitleEN);
  $Value = htmlspecialchars($SubjectGroupID.'|=|'.$ClassName.'|=|'.$TargetDate.'|=|'.$RoomAllocationID.'|=|'.$StartTime.'|=|'.$EndTime,ENT_QUOTES);
  $selected = ($Value==$SubjectGroupSession)?"SELECTED":"";
  $SubjectGroupSelect .= "<OPTION value=\"".$Value."\" $selected>".$TimeSlotName.":".Get_Lang_Selection($ClassTitleB5,$ClassTitleEN)." ".$StartTime."-".$EndTime."</OPTION>\n";
}
$SubjectGroupSelect .= "</SELECT>\n";

echo $SubjectGroupSelect;
intranet_closedb();
die;
?>
