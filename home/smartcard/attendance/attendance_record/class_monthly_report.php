<?php
// editing by 
####################################### Change Log #################################################
# 2019-10-31 by Ray:    Added date range select
# 2018-09-06 by Carlos: Only Allow teacher to access.
# 2012-12-28 by Carlos: Added continuous absent day option
# 2011-12-20 by Carlos: Added option Student status. 
#						Modified to use class id for generating report.
# 2010-06-17 by Carlos: Copied from Student Attendance > Class-Monthly Attendance Report;
#						Post ClassName instead of ClassID.
####################################################################################################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

// Only allow teacher to access
$luser = new libuser($_SESSION['UserID']);
if(!$luser->isTeacherStaff()){
	intranet_closedb();
	header ("Location: /");
	exit();
}

$DefaultClass = $_REQUEST['ClassID'];
$TargetYear = ($_REQUEST['Year'] == "")? date('Y'):$_REQUEST['Year'];
$TargetMonth = ($_REQUEST['Month'] == "")? date('n'):$_REQUEST['Month'];

$linterface = new interface_html();
$lsmartcard = new libsmartcard();
$CurrentPage = "PageAttendance";

$lc = new libcardstudentattend2();
# Get Setting - Class Teacher Can Take its Own Classes Only
$GeneralSetting = new libgeneralsettings();
$SettingList = array("'ClassTeacherTakeOwnClassOnly'");
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
$take_own_class = $Settings['ClassTeacherTakeOwnClassOnly'];

$prev_button_select = $button_select;
$button_select = $i_status_all;

$AcademicYear = Get_Current_Academic_Year_ID();
# Get Classes List
# Teacher can only view its own classes
if($take_own_class==1)
{
	$lteaching = new libteaching();
	$class = $lteaching->returnTeacherClass($UserID);
	$lclass = new libclass();
	$ClassList[] = array("","-- ".$i_status_all." --");
	for ($i=0; $i< sizeof($class); $i++) {
		$ClassList[] = array($class[$i][0],$class[$i][1]);
	}
	$select_class = $lteaching->getSelect($ClassList,' name="ClassID" class="class_list" ',$ClassID,1);
}else # Teachers can view all classes
{
	$select_class = $lc->getSelectClassID("name=\"ClassID\" class=\"class_list\" ",$ClassID);
}
$button_select = $prev_button_select;

# Get Year List
$years = $lc->getRecordYear();
$select_year = getSelectByValue($years, "name=\"Year\"",$TargetYear,0,1);

# Month List
$months = $i_general_MonthShortForm;
$select_month = "<select name=\"Month\">";
$currMon = $TargetMonth-1;
for ($i=0; $i<sizeof($months); $i++)
{
     $month_name = $months[$i];
     $string_selected = ($currMon==$i? "SELECTED":"");
     $select_month .= "<OPTION value=".($i+1)." $string_selected>".$month_name."</OPTION>\n";
}
$select_month .= "</SELECT>\n";


$format_array = array(
                      array(0,"Web"),
                      array(1,"Excel"));
$select_format = getSelectByArray($format_array, "name=\"format\"",0,0,1);

### Title ###
$TAGS_OBJ[] = array($i_StudentAttendance_Report_Search,"search_report.php", 0);
if ($sys_custom['SmartCardAttendance_StudentAbsentSession']) 
	$TAGS_OBJ[] = array($button_find." (".$Lang['StudentAttendance']['AbsentSessions'].")","search_absent_session.php", 0);
$TAGS_OBJ[] = array($StatusRecord,"attendance_list.php", 0);
$TAGS_OBJ[] = array($MonthlyRecord,"attendance_list_monthly.php", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Report_NoCardTab,"nocard.php", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Report_ClassMonth,"class_monthly_report.php", 1);
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<script LANGUAGE="Javascript">
function submitForm(obj){
	obj.action = 'class_monthly_report_result.php';
	// auto correct
	var absent_day = parseInt(document.getElementById('AbsentDay').value.Trim());
	if(!is_positive_int(absent_day)) {
		document.getElementById('AbsentDay').value = "0";
	}else{
		document.getElementById('AbsentDay').value = absent_day;
	}
	
	if(obj.format.value=='0') {
		obj.target='intranet_popup10';
    	newWindow('about:blank', 10);
		obj.submit();
	} else {
		obj.target='_blank';
		obj.submit();
	}
}


function Use_Month_Year(){
    $('tr#RowStartDate').hide();
    $('tr#RowEndDate').hide();
    $('tr#RowSelectYear').show();
    $('tr#RowSelectMonth').show();
}


function Use_Date_Range(){
    $('tr#RowSelectYear').hide();
    $('tr#RowSelectMonth').hide();
    $('tr#RowStartDate').show();
    $('tr#RowEndDate').show();
}
</script>
<br />
<? if($take_own_class==1 && sizeof($class) == 0){ ?>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="96%" border="0" cellspacing="0" cellpadding="0">
	  			<tr>
					<td colspan="2"><?=$i_Profile_NotClassTeacher?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<? }else { ?>
<form name="form1" action="" method="POST">
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?="$button_select $i_ClassName"?></td>
		<td width="70%"class="tabletext">
			<?=$select_class?>
		</td>
	</tr>

    <tr>
        <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['UserDateRange']?></td>
        <td width="70%" class="tabletext">
            <label for="useDateRangeRadio1"><input type="radio" id="useDateRangeRadio1" name="UseDateRange" value="1" onclick="Use_Date_Range()" <?=($UseDateRange==1?"checked":"")?>><?=$i_general_yes?></label>
            &nbsp;&nbsp;
            <label for="useDateRangeRadio2"><input type="radio" id="useDateRangeRadio2" name="UseDateRange" value="0" onclick="Use_Month_Year()" <?=($UseDateRange!=1?"checked":"")?>><?=$i_general_no?></label>
        </td>
    </tr>

    <tr id="RowStartDate" <?=($UseDateRange==1?'':'style="display:none;"')?>>
        <td  id="dateRange1" width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_startdate?></td>
        <td  id="dateRange2" width="70%" class="tabletext">
			<?=$linterface->GET_DATE_PICKER("StartDate", $startDate)?>
        </td>
    </tr>
    <tr id="RowEndDate" <?=($UseDateRange==1?'':'style="display:none;"')?>>
        <td  id="dateRange3" width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_enddate?></td>
        <td  id="dateRange4" width="70%" class="tabletext">
			<?=$linterface->GET_DATE_PICKER("EndDate", $endDate)?>
        </td>
    </tr>

    <tr id="RowSelectYear" <?=($UseDateRange!=1?'':'style="display:none;"')?>>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Year?></td>
		<td width="70%"class="tabletext">
			<?=$select_year?>
		</td>
	</tr>
	<tr id="RowSelectMonth" <?=($UseDateRange!=1?'':'style="display:none;"')?>>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Month?></td>
		<td width="70%"class="tabletext">
			<?=$select_month?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Format?></td>
		<td width="70%"class="tabletext">
			<?=$select_format?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['FilterNoDataDate']?></td>
		<td width="70%"class="tabletext">
			<input type="checkbox" name="HideNoData" id="HideNoData" value="1">
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['StudentStatus']?></td>
		<td width="70%"class="tabletext">
			<input type="radio" name="StudentStatus" value="0,1,2,3" ><?=$Lang['General']['All']?>
			&nbsp;
			<input type="radio" name="StudentStatus" value="0,1,2" checked="checked"><?=$Lang['Status']['Active'].' + '.$Lang['Status']['Suspended']?></td>
	</tr>
	<?php
	echo '<tr>'."\n";
	echo '<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'."\n";
	echo str_replace('<!--NUMBER-->','</td><td width="70%" class="tabletext"><input type="text" class="textboxnum" name="AbsentDay" id="AbsentDay" value="0" maxlength="5">',$Lang['StudentAttendance']['DisplayInRedForContinuousAbsent'])."\n";
	echo '&nbsp;<span class="tabletextremark">('.$Lang['StudentAttendance']['ZeroToDisable'].')</span>'."\n";
	echo '</td>'."\n";
	echo '</tr>'."\n";
	?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['SmartCard']['StaffAttendence']['ShowAllColumns']?></td>
		<td width="70%"class="tabletext">
			<input type="radio" name="ShowAllColumns" value="1" onclick="document.getElementById('OptionsLayer').style.display='none';this.checked=true;" <?if($ShowAllColumns=="" || $ShowAllColumns=="1"){echo "checked";}else{echo "";}?>><?=$i_general_yes?>
			&nbsp;
			<input type="radio" name="ShowAllColumns" value="0" onclick="document.getElementById('OptionsLayer').style.display='block';this.checked=true;" <?if($ShowAllColumns=="0"){echo "checked";}else{echo "";}?>><?=$i_general_no?></td>
	</tr>
	<?
	if($ShowAllColumns == "0")
		$OptionsLayerVisibility = "style='display:block;'";
	else
		$OptionsLayerVisibility = "style='display:none;'";
	?>
	<tr>
		<td width="30%">&nbsp;</td>
		<td width="70%"class="tabletext">
		<span id="OptionsLayer" <?=$OptionsLayerVisibility?>>
			<input type="checkbox" name="ColumnChineseName" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?=$i_UserChineseName?><br />
			<input type="checkbox" name="ColumnEnglishName" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?=$i_UserEnglishName?><br />
			<input type="checkbox" name="ColumnGender" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?=$i_UserGender?><br />
			<input type="checkbox" name="ColumnData" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?=$Lang['StudentAttendance']['DataDistribution']?><br />
			<input type="checkbox" name="ColumnDailyStat" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?=$i_StudentAttendance_Daily_Stat?><br />
			<input type="checkbox" name="ColumnSchoolDays" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?=$Lang['SmartCard']['StudentAttendence']['SchoolDaysStat']?><br />
			<input type="checkbox" name="ColumnMonthlyStat" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?=$i_StudentAttendance_Monthly_Level_Stat?><br />
			<input type="checkbox" name="ColumnRemark" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?=$i_Discipline_System_general_remark?><br />
			<input type="checkbox" name="ColumnReasonStat" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?=$Lang['SmartCard']['StudentAttendance']['CustomizedReasonStat']?><br />
			<?if($sys_custom['SmartCardAttendance_StudentAbsentSession'] ){?>
			<input type="checkbox" name="ColumnSession" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?=$Lang['StudentAttendance']['SessionsStat']?><br />
			<?}?>
		</span>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "button", "submitForm(document.form1);", "submit2") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onclick=\"document.getElementById('OptionsLayer').style.display='none';\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="AcademicYear" value="<?=$AcademicYear?>" />
</form>
<br />
<?
}
$linterface->LAYOUT_STOP();
intranet_closedb();
?>