<?php
// using: 
/**************************************** Changes *****************************************************
 * 2011-12-15 (Carlos): added filter to only display school days record for student and parent
 ******************************************************************************************************/
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");

intranet_auth();
intranet_opendb();

if(!$plugin['attendancestudent'])
{
	header("Location: $intranet_httppath/");
	exit();
}

$attendDate = $_POST['RecordDate'];
if ($attendDate == "" || !intranet_validateDate($attendDate)) {
	$year = date('Y');
	$month = date('m');
	$day = date('d');
	$attendDate	= date('Y-m-d');
} else {
	$year = substr($attendDate, 0, 4);
	$month = substr($attendDate, 5, 2);
	$day = substr($attendDate, 8, 2);
}

$lu		= new libuser($UserID);
$lcard	= new libcardstudentattend2();
$lcard->retrieveSettings();
$linterface = new interface_html();
$lsmartcard = new libsmartcard();
$CurrentPage = "PageAttendance";

$noNeedToRetrieve = false;
$iSmartCardOnlyDisplaySchoolDayAttendanceRecord = false;
// if user is parent, get all of his children
if ($lu->isParent())
{
	$lfamily = new libfamily();
	$students = $lfamily->returnChildrens($UserID);
	if (sizeof($students)==0)
	{
		$noNeedToRetrieve = true;
	}
	$iSmartCardOnlyDisplaySchoolDayAttendanceRecord = $lcard->iSmartCardOnlyDisplaySchoolDayAttendanceRecord == 1;
}
// if user is teacher, get all of his student who he is class tacher
else if ($lu->isTeacherStaff())
{
	$lteaching = new libteaching();
	$class = $lteaching->returnTeacherClass($UserID);
	$lclass = new libclass();
	if (sizeof($class) == 1) {
		$classname = $class[0][1];
	}
	else if (sizeof($class) > 1) {
		if ($classname == "") 
			$classname = $class[0][1];
		
		for ($i=0; $i< sizeof($class); $i++) {
			$ClassList[] = array($class[$i][1],$class[$i][1]);
		}
		$SelectClass = $lteaching->getSelect($ClassList,' name="classname" id="classname"',$classname,1);
	}
	
	if ($classname != "")
	{
		$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2) AND ClassName = '".$lu->Get_Safe_Sql_Query($classname)."'";
		$students = $lu->returnVector($sql);
	}
	if (!$lcard->isRequiredToTakeAttendanceByDate($classname,$attendDate))
	{
		$noNeedToRetrieve = true;
	}
}
else
{
	$students = array($UserID);
	$iSmartCardOnlyDisplaySchoolDayAttendanceRecord = $lcard->iSmartCardOnlyDisplaySchoolDayAttendanceRecord == 1;
}

if($field=="") $field = 2;
if($order=="") $order = 1;
$li = new libdbtable2007($field, $order, $pageNo);

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td class='tablebluetop tabletoplink' width='30%'>". $i_UserName ."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletoplink' width='10%'>". $i_ClassName ."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletoplink' width='10%'>". $i_UserClassNumber ."</td>\n";
if ($lcard->attendance_mode != 1)
$li->column_list .= "<td class='tablebluetop tabletoplink' width='10%'>". $i_StudentAttendance_Slot_AM ."</td>\n";
if ($lcard->attendance_mode != 0)
$li->column_list .= "<td class='tablebluetop tabletoplink' width='10%'>". $i_StudentAttendance_Slot_PM ."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletoplink' width='7.5%'>". $i_StudentAttendance_Field_InSchoolTime ."</td>\n";
if ($lcard->attendance_mode == 2) {
	if (!$lcard->NoRecordLunchOut) 
		$li->column_list .= "<td class='tablebluetop tabletoplink' width='7.5%'>". $i_StudentAttendance_Offline_Import_DataType_LunchOut ."</td>\n";
	$li->column_list .= "<td class='tablebluetop tabletoplink' width='7.5%'>". $i_StudentAttendance_Offline_Import_DataType_LunchIn ."</td>\n";
}
$li->column_list .= "<td class='tablebluetop tabletoplink' width='7.5%'>". $i_StudentAttendance_Field_LeaveTime ."</td>\n";

if(!$noNeedToRetrieve && count($students)>0 && $iSmartCardOnlyDisplaySchoolDayAttendanceRecord)
{
	$sql = "SELECT UserID, ClassName, ClassNumber FROM INTRANET_USER WHERE UserID IN ('".implode("','",$lu->Get_Safe_Sql_Query($students))."') ";	
	$candidates = $lcard->returnArray($sql);
	$filtered_students = array();
	for($j=0;$j<count($candidates);$j++){
		if($lcard->isRequiredToTakeAttendanceByDate($candidates[$j]['ClassName'],$attendDate)){
			$filtered_students[] = $candidates[$j]['UserID'];
		}
	}
}

if (!$noNeedToRetrieve && sizeof($students)!=0)
{
	# Get Data Records
	$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
	$lcard->createTable_Card_Student_Daily_Log($year,$month);
	
	if(isset($filtered_students)){
		$list = count($filtered_students)>0 ? "'".implode("','",$lu->Get_Safe_Sql_Query($filtered_students))."'": "-1";
	}else{
		$list = "'".implode("','",$lu->Get_Safe_Sql_Query($students))."'";
	}
		$namefield = getNameFieldByLang("a.");
		
		$sql = "
				SELECT
				$namefield as stuName,
				a.ClassName,
				a.ClassNumber,
				";

	if ($lcard->attendance_mode != 1){
		//$sql .= "IF(b.AMStatus IS NULL, '"."--"."',b.AMStatus) as AMStatus,";
		//$sql .= "IF(b.LeaveStatus IS NOT NULL, IF(b.LeaveStatus = 1, '-1', '--'), IF(b.AMStatus IS NULL, '"."--"."',b.AMStatus)) as AMStatus,";
		$sql .= "IF(b.LeaveStatus = 1, 
							IF(b.AMStatus = 2,'-3','-1'), 
							IF(b.AMStatus IS NULL, '"."--"."',b.AMStatus)
							) as AMStatus,";
	}
	if ($lcard->attendance_mode != 0){
		//$sql .= "IF(b.PMStatus IS NULL, '"."--"."',b.PMStatus) as PMStatus,";
		//$sql .= "IF(b.LeaveStatus IS NOT NULL, IF(b.LeaveStatus = 2, '-2', '--'), IF(b.PMStatus IS NULL, '"."--"."',b.PMStatus)) as PMStatus,";
		$sql .= "IF(b.LeaveStatus = 2, 
							IF(b.PMStatus = 2,'-4','-2'), 
							IF(b.PMStatus IS NULL, '"."--"."',b.PMStatus)
							) as PMStatus,";
	}
	//$sql .= "b.LeaveStatus as EarlyLeave, ";
	
	$sql .= "
		IF(b.InSchoolTime IS NULL,'--',TIME_FORMAT(b.InSchoolTime,'%H:%i:%s')) as InSchoolTime,";
	if ($lcard->attendance_mode == 2) {
		if (!$lcard->NoRecordLunchOut) 
			$sql .= "IF(b.LunchOutTime IS NULL,'--',TIME_FORMAT(b.LunchOutTime,'%H:%i:%s')) as LunchOutTime, ";
		$sql .= "IF(b.LunchBackTime IS NULL,'--',TIME_FORMAT(b.LunchBackTime,'%H:%i:%s')) as LunchBackTime, ";
	}
	$sql .= "
		IF(b.LeaveSchoolTime IS NULL,'--',TIME_FORMAT(b.LeaveSchoolTime,'%H:%i:%s')) as LeaveSchoolTime, 
		c.RecordStatus as AMWaive,
		d.RecordStatus as PMWaive, 
		e.RecordStatus as AMLeaveWaive, 
		f.RecordStatus as PMLeaveWaive 
		FROM
			INTRANET_USER as a ";
	if ($lcard->EnableEntryLeavePeriod) {
		$sql .= "
			INNER JOIN 
			CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
			on 
				a.UserID = selp.UserID 
				and 
				'".$lu->Get_Safe_Sql_Query($attendDate)."' between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
			";
	}
	$sql .= "
			LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '".$lu->Get_Safe_Sql_Query($day)."'
			LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c 
			ON c.StudentID = a.UserID 
				AND DATE_FORMAT(c.RecordDate,'%Y%m') = '$year$month' 
				AND DAYOFMONTH(c.RecordDate) = b.DayNumber 
				AND c.DayType = 2 
				AND c.RecordType = b.AMStatus 
			LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as d 
			ON d.StudentID = a.UserID 
				AND DATE_FORMAT(d.RecordDate,'%Y%m') = '$year$month' 
				AND DAYOFMONTH(d.RecordDate) = b.DayNumber 
				AND d.DayType = 3 
				AND d.RecordType = b.PMStatus 
			LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as e 
			ON e.StudentID = a.UserID 
				AND DATE_FORMAT(e.RecordDate,'%Y%m') = '$year$month' 
				AND DAYOFMONTH(e.RecordDate) = b.DayNumber 
				AND e.DayType = 2 
				AND e.RecordType = 3 
			LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as f 
			ON f.StudentID = a.UserID 
				AND DATE_FORMAT(f.RecordDate,'%Y%m') = '$year$month' 
				AND DAYOFMONTH(f.RecordDate) = b.DayNumber 
				AND f.DayType = 3 
				AND f.RecordType = 3 
			WHERE
				a.UserID IN ($list)
				AND b.DayNumber = '".$lu->Get_Safe_Sql_Query($day)."' 
				";
}
else
{
	if ($lu->isTeacherStaff())
	{
		if ($classname=="")
			$prompt_msg = $i_Profile_NotClassTeacher;
		else if (sizeof($students)==0)
		{
			$prompt_msg = $i_StudentAttendance_Message_NoStudentInClass;
		}
		else
			$prompt_msg = $i_StudentAttendance_Message_ThisClassNoNeedToTake;
	}
	else     # Parent
	{
		$prompt_msg = $i_Profile_NoFamilyRelation;
	}
}

/*
if ($lcard->attendance_mode != 1 and $lcard->attendance_mode != 0 )
{
	$li->field_array = array("stuName","a.ClassName","a.ClassNumber","AMStatus", "PMStatus", "InSchoolTime", "LeaveSchoolTime");
}
else
{
	if($lcard->attendance_mode != 1) $apmstatus = "AMStatus";
	if($lcard->attendance_mode != 0) $apmstatus = "PMStatus";
	$li->field_array = array("stuName","a.ClassName","a.ClassNumber", $apmstatus, "InSchoolTime", "LeaveSchoolTime");
}
*/
$li->field_array[] = "stuName";
$li->field_array[] = "a.ClassName";
$li->field_array[] = "a.ClassNumber";
if ($lcard->attendance_mode != 1)
	$li->field_array[] = "AMStatus";
if ($lcard->attendance_mode != 0)
	$li->field_array[] = "PMStatus";
$li->field_array[] = "InSchoolTime";
if ($lcard->attendance_mode == 2) {
	if (!$lcard->NoRecordLunchOut) 
		$li->field_array[] = "LunchOutTime";
	$li->field_array[] = "LunchBackTime";
}
$li->field_array[] = "LeaveSchoolTime";
//debug_r($sql);
$li->sql = $sql;
$li->no_col = sizeof($li->field_array);
$li->IsColOff = "SmartCardAttandencdList";
$li->title = "";

### Title ###
if ($lu->isTeacherStaff()) {
	$TAGS_OBJ[] = array($i_StudentAttendance_Report_Search,"search_report.php", 0);
	if ($sys_custom['SmartCardAttendance_StudentAbsentSession']) 
		$TAGS_OBJ[] = array($button_find." (".$Lang['StudentAttendance']['AbsentSessions'].")","search_absent_session.php", 0);
	$TAGS_OBJ[] = array($StatusRecord,"attendance_list.php", 1);
	$TAGS_OBJ[] = array($MonthlyRecord,"attendance_list_monthly.php", 0);
	$TAGS_OBJ[] = array($i_StudentAttendance_Report_NoCardTab,"nocard.php", 0);
	$TAGS_OBJ[] = array($i_StudentAttendance_Report_ClassMonth,"class_monthly_report.php", 0);
}
else {
	$TAGS_OBJ[] = array($StatusRecord,"attendance_list.php", 1);
	$TAGS_OBJ[] = array($MonthlyRecord,"attendance_list_monthly.php", 0);
}

$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>
<script language="JavaScript">
function checkForm(){
	objDate = document.form1.RecordDate;
	if(!check_date(objDate,'<?=$i_StudentAttendance_Warn_Invalid_Date_Format?>')) return false;
	return true;
}
</script>

<br />
<form name="form1" action="attendance_list.php" method="POST">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="10%"><span class="tabletext"><?=$i_StudentAttendance_Field_Date?>:</span></td>
			<td nowrap="nowrap"><?= $linterface->GET_DATE_PICKER("RecordDate", $attendDate)?></td>
			<td><?= $SelectClass?></td>
			<td width="65%"><?= $linterface->GET_SMALL_BTN($button_view, "submit", "return checkForm();", "submitBtn")?></td>
		</tr>
		</table>
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2"><?=$li->display($prompt_msg)?></td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
<br />


<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>