<?php
// Editing by 
/**************************************************** Change Log ********************************************************************
 * 2018-09-06 (Carlos) : Only Allow teacher to access.
 * 2016-02-11 (Carlos) : Add Outing attendance type.
 * 2015-07-09 (Carlos) : Add column option [Prove Document].
 * 2015-05-27 (Carlos) : Add [Report Type] to separate the detail report, sumamry stat and class summary stat. Added display column [Office Remark].
 * 2014-01-28 (Carlos) : Add column option [Remark]
 ************************************************************************************************************************************/
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$linterface = new interface_html();
$lsmartcard = new libsmartcard();
$CurrentPage = "PageAttendance";

$lc = new libcardstudentattend2();

$lclass= new libclass();

// Only allow teacher to access
$luser = new libuser($_SESSION['UserID']);
if(!$luser->isTeacherStaff()){
	intranet_closedb();
	header ("Location: /");
	exit();
}

# class list
if($lc->ClassTeacherTakeOwnClassOnly==1)
{
	include_once($PATH_WRT_ROOT."includes/libteaching.php");
	$lteaching = new libteaching();
	$class = $lteaching->returnTeacherClass($UserID);
	$lclass = new libclass();
	for ($i=0; $i< sizeof($class); $i++) {
		$ClassList[] = array($class[$i][1],$class[$i][1]);
	}
	$select_class = $lteaching->getSelect($ClassList,' name="class_name[]" id="class_name[]" multiple class="class_list" style="min-width:150px; width:200px;" onchange="Get_Student_List()"',$class_name,1);
}
else {
	$select_class = $lc->getSelectClass('name="class_name[]" id="class_name[]" multiple class="class_list" size="10" style="min-width:150px; width:200px;" onchange="Get_Student_List()"',$class_name,1);
}

# attendance type
$select_attend = "<SELECT name=\"attendance_type\">\n";
$select_attend .= "<OPTION value=\"\" SELECTED>$i_status_all</OPTION>";
$select_attend .= "<OPTION value=\"".CARD_STATUS_ABSENT."\">$i_StudentAttendance_Status_Absent</OPTION>";
$select_attend .= "<OPTION value=\"".CARD_STATUS_LATE."\">$i_StudentAttendance_Status_Late</OPTION>";
$select_attend .= "<OPTION value=\"".PROFILE_TYPE_EARLY."\">$i_StudentAttendance_Status_EarlyLeave</OPTION>";
$select_attend .= "<OPTION value=\"".(CARD_STATUS_OUTING+CARD_STATUS_ABSENT)."\">".$i_StudentAttendance_Status_Outing."</OPTION>";
$select_attend .="</SELECT>\n";

# session
$select_session ="<SELECT name=\"session\">\n";
$select_session.= "<OPTION value=\"\" SELECTED>$i_status_all</OPTION>";
$select_session.="<OPTION value=\"".PROFILE_DAY_TYPE_AM."\">$i_StudentAttendance_Slot_AM</OPTION>\n";
$select_session.="<OPTION value=\"".PROFILE_DAY_TYPE_PM."\">$i_StudentAttendance_Slot_PM</OPTION>\n";
$select_session.="</SELECT>\n";

# date range
$current_month=date('n');
$current_year =date('Y');
if($current_month>=9){
	$startDate = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year));
	//$endDate   = date('Y-m-d',mktime(0, 0, 0, 8, 31,$current_year+1));
}else{
	$startDate = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year-1));
	//$endDate   = date('Y-m-d',mktime(0, 0, 0, 8, 31,$current_year));
}
$endDate=date('Y-m-d');

# reason
$lword = new libwordtemplates();
$words = $lword->getWordListAttendance();

# reason
$lword = new libwordtemplates();
$words = $lword->getWordListAttendance();
$select_words = "<SELECT name='words' onChange='changeReason(this.form)'>\n";
$select_words.="<OPTION value=''> -- $i_StudentAttendance_WordTemplates  -- </OPTION>\n";
for ($j=0; $j<sizeof($words); $j++){
     $select_words.="<OPTION value='$j'>".$words[$j]."</OPTION>\n";
}
$select_words.="</SELECT>\n";

# waived
$select_waived = "<SELECT name=\"waived\">\n";
$select_waived.="<OPTION value=\"\" SELECTED>$i_status_all</OPTION>\n";
$select_waived.="<OPTION value=\"1\">$i_general_yes</OPTION>\n";
$select_waived.="<OPTION value=\"2\">$i_general_no</OPTION>\n";
$select_waived.="</SELECT>\n";

# format
$select_format ="<SELECT name=\"format\">\n";
$select_format .="<OPTION value=\"1\">Web</OPTION>\n";
$select_format.="<OPTION VALUE=\"2\">CSV</OPTION>\n";
$select_format.="</SELECT>\n";

### Title ###
$TAGS_OBJ[] = array($i_StudentAttendance_Report_Search,"search_report.php", 1);
if ($sys_custom['SmartCardAttendance_StudentAbsentSession']) 
	$TAGS_OBJ[] = array($button_find." (".$Lang['StudentAttendance']['AbsentSessions'].")","search_absent_session.php", 0);
$TAGS_OBJ[] = array($StatusRecord,"attendance_list.php", 0);
$TAGS_OBJ[] = array($MonthlyRecord,"attendance_list_monthly.php", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Report_NoCardTab,"nocard.php", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Report_ClassMonth,"class_monthly_report.php", 0);
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<style type="text/css">
.class_list{width:200px; height=150px;}
</style>
<script type="text/javascript">
<!--
function isValidDate(obj){
	if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
	return true;
}
function changeReason(formObj){
	if(formObj==null)return;
	if(formObj.words==null || formObj.reason==null) return;
	if(formObj.words.selectedIndex<=0) {
		formObj.reason.value="";
		return;
	}
	formObj.reason.value = formObj.words.options[formObj.words.selectedIndex].text;
}

function checkform(formObj){
	if(formObj==null)return false;
	if(formObj.startStr==null || formObj.endStr==null) return false;
	if(!isValidDate(formObj.startStr) || !isValidDate(formObj.endStr)) return false;
	if(formObj.startStr.value>formObj.endStr.value){
		alert('<?=$i_Booking_EndDateWrong?>');
		return false;
	}
	var ClassSelected = false;
	var ClassSelectObj = document.getElementById('class_name[]');
	for(i=0;i<ClassSelectObj.options.length;i++){
		if(ClassSelectObj.options[i].selected) {
			ClassSelected = true;
			break;
		}
	}
	
	var report_type_elements = document.getElementsByName('ReportType[]');
	var checked_report_types = [];
	for(var i=0;i<report_type_elements.length;i++){
		if(report_type_elements[i].checked) checked_report_types.push(report_type_elements[i].value);
	}
	if(checked_report_types.length == 0){
		report_type_elements[0].checked = true;
		onDetailReportTypeChecked(true);
	}
	
	if(!ClassSelected){
		alert('<?=$i_Discipline_System_alert_PleaseSelectClass?>');
		return false;
	}
	else {
		return true;
	}
}
function submitForm(formObj){
	if(checkform(formObj)){ 
		selected_format = formObj.format.options[formObj.format.selectedIndex].value;
		if(selected_format=='2'){
		  formObj.target="_blank";
		} else {
			formObj.target='intranet_popup10';
			newWindow('about:blank', 10);
		}
	/*
	selected_type = formObj.type.options[formObj.type.selectedIndex].value;
	if(selected_type=='2')
	{
		formObj.action="search_report_summary_result.php";
	}else
	{
		formObj.action="search_report_result.php";
	}
	*/
	formObj.submit();
	}
}

function Get_Student_List() {
	var PersonalSelect = document.getElementById('PersonalSelection').checked;
	
	if (PersonalSelect) {
		document.getElementById('StudentListRow').style.display = '';
		$('#ClassSummaryLayer').hide();
		//document.getElementById('ColumnClassSummaryStat').checked = false;
		var ClassSelected = Get_Selection_Value("class_name[]","Array");
		
		var PostVar = {
			"ClassList[]":ClassSelected
		};
		
		$('#StudentListLayer').load("<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/attendance/report/ajax_get_student_selection.php",PostVar,
			function(data) {
				if (data == "die") {
					window.top.location = "/";
				}
			}
		);
	}
	else {
		document.getElementById('StudentListRow').style.display = 'none';
		$('#StudentListLayer').html('');
		$('#ClassSummaryLayer').show();
	}
}

function onDetailReportTypeChecked(isChecked)
{
	if(isChecked)
		$('.ColumnOptionRow').show();
	else{
		$('.ColumnOptionRow').hide();
	}
}
// -->
</script>
<br />
<? if (sizeof($class) == 0 && $lc->ClassTeacherTakeOwnClassOnly==1) {?>
			<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center">
	        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
              <tr>
								<td colspan="2"><?=$i_Profile_NotClassTeacher?></td>
							</tr>
            </table>
      		</td>
				</tr>
			</table>
<? }
	 else {?>
<form name="form1" action="search_report_result.php" method="GET">
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_startdate?></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER("startStr", $startDate)?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_enddate?></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER("endStr", $endDate)?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?="$button_select $i_ClassName"?></td>
		<td width="70%"class="tabletext">
			<?=$select_class?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['PersonalSelection']?></td>
		<td width="70%"class="tabletext">
			<input type="checkbox" id="PersonalSelection" value="1" onclick="Get_Student_List();">
		</td>
	</tr>
	<tr id="StudentListRow" style="display:none;">
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['Identity']['Student']?></td>
		<td width="70%"class="tabletext">
			<div id="StudentListLayer">
			</div>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Attendance_attendance_type?></td>
		<td width="70%"class="tabletext">
			<?=$select_attend?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Attendance_DayType?></td>
		<td width="70%"class="tabletext">
			<?=$select_session?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Attendance_Reason?></td>
		<td width="70%"class="tabletext">
			<input class="textboxnum" type="text" name="reason">
			<?=$select_words?><br />
			<input type="checkbox" name="match" id="match" value="1">
			<label for="match"><?=$i_StudentAttendance_exactly_matched?></label>
		</td>
	</tr>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SmartCard_Frontend_Take_Attendance_Waived?></td>
		<td width="70%"class="tabletext">
			<?=$select_waived?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Format?></td>
		<td width="70%"class="tabletext">
			<?=$select_format?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['ReportType']?></td>
		<td width="70%"class="tabletext">
			<input type="checkbox" name="ReportType[]" id="ReportType1" value="1" checked="checked" onclick="onDetailReportTypeChecked(this.checked);" /><label for="ReportType1"><?=$Lang['StudentAttendance']['DetailRecords']?></label>
			<input type="checkbox" name="ReportType[]" id="ReportType2" value="2" /><label for="ReportType2"><?=$Lang['StudentAttendance']['SummaryStatistic']?></label>
			<input type="checkbox" name="ReportType[]" id="ReportType3" value="3" /><label for="ReportType3"><?=$Lang['StudentAttendance']['Class'].' '.$Lang['StudentAttendance']['SummaryStatistic']?></label>
		</td>
	</tr>
	<tr class="ColumnOptionRow">
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['SmartCard']['StaffAttendence']['ShowAllColumns']?></td>
		<td width="70%"class="tabletext">
		<input type="radio" name="ShowAllColumns" id="ShowAllColYes" value="1" onclick="document.getElementById('OptionsLayer').style.display='none';this.checked=true;" <?if($ShowAllColumns=="" || $ShowAllColumns=="1"){echo "checked";}else{echo "";}?>><?="<label for=\"ShowAllColYes\">".$i_general_yes."</label>"?>
		&nbsp;
		<input type="radio" name="ShowAllColumns" id="ShowAllColNo" value="0" onclick="document.getElementById('OptionsLayer').style.display='block';this.checked=true;" <?if($ShowAllColumns=="0"){echo "checked";}else{echo "";}?>><?="<label for=\"ShowAllColNo\">".$i_general_no."</label>"?></td>
	</tr>
	<?
	if($ShowAllColumns == "0")
		$OptionsLayerVisibility = "style='display:block;'";
	else
		$OptionsLayerVisibility = "style='display:none;'";
	?>
	<tr class="ColumnOptionRow">
		<td width="30%">&nbsp;</td>
		<td width="70%"class="tabletext">
		<span id="OptionsLayer" <?=$OptionsLayerVisibility?>>
			<input type="checkbox" name="ColumnLoginID" id="ColumnLoginID" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?="<label for=\"ColumnLoginID\">".$i_UserLogin."</label>"?><br />
			<input type="checkbox" name="ColumnStudentName" id="ColumnStudentName" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?="<label for=\"ColumnStudentName\">".$i_UserStudentName."</label>"?><br />
			<input type="checkbox" name="ColumnClass" id="ColumnClass" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?="<label for=\"ColumnClass\">".$i_ClassNameNumber."</label>"?><br />
			<input type="checkbox" name="ColumnReason" id="ColumnReason" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?="<label for=\"ColumnReason\">".$i_Attendance_Reason."</label>"?><br />
			<input type="checkbox" name="ColumnRemark" id="ColumnRemark" value="1"><?="<label for=\"ColumnRemark\">".$Lang['StudentAttendance']['iSmartCardRemark']."</label>"?><br />
			<input type="checkbox" name="ColumnOfficeRemark" id="ColumnOfficeRemark" value="1"><?="<label for=\"ColumnOfficeRemark\">".$Lang['StudentAttendance']['OfficeRemark']."</label>"?><br />
			<input type="checkbox" name="ColumnWaived" id="ColumnWaived" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?="<label for=\"ColumnWaived\">".$i_SmartCard_Frontend_Take_Attendance_Waived."</label>"?><br />
			<input type="checkbox" name="ColumnProveDocument" id="ColumnProveDocument" value="1"><?="<label for=\"ColumnProveDocument\">".$Lang['StudentAttendance']['ProveDocument']."</label>"?><br />
			<!--
			<input type="checkbox" name="ColumnSummaryStat" id="ColumnSummaryStat" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?="<label for=\"ColumnSummaryStat\">".$Lang['StudentAttendance']['SummaryStatistic']."</label>"?>
			<div id="ClassSummaryLayer"><input type="checkbox" name="ColumnClassSummaryStat" id="ColumnClassSummaryStat" value="1"><?="<label for=\"ColumnClassSummaryStat\">".$Lang['StudentAttendance']['Class'].' '.$Lang['StudentAttendance']['SummaryStatistic']."</label>"?></div>
			-->
		</span>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "button", "submitForm(document.form1);") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onclick=\"document.getElementById('OptionsLayer').style.display='none';\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="selected_classes" value="">
</form>
<br />
<?
	 }
$linterface->LAYOUT_STOP();
intranet_closedb();
?>