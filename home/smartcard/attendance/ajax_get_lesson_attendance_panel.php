<?php
# Using:

################################################# Change log ####################################################
# 2013-08-13 by Henry: File Created
#################################################################################################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$SubjectGroupID = IntegerSafe($_REQUEST['RecordID']);
$LessonDate = $_REQUEST['LessonDate'];
$RoomAllocationID = IntegerSafe($_REQUEST['RoomAllocationID']);
//$AttendOverviewID = $_REQUEST['AttendOverviewID'];
//if($StudentID == -1)
//	$StudentID = "";

$linterface = new interface_html();
$LessonAttendUI = new libstudentattendance_ui();

$AttendOverviewID = $LessonAttendUI->Check_Record_Exist_Subject_Group_Attend_Overview($SubjectGroupID,$LessonDate,$RoomAllocationID);
echo $LessonAttendUI->Get_Lesson_Attendance_Panel_Form($SubjectGroupID, "SubjectGroup", $LessonDate, $AttendOverviewID);

intranet_closedb();
?>