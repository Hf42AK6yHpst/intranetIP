<?php

$PATH_WRT_ROOT = "../../../../";

//set language
@session_start();
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");

$uid = IntegerSafe($_GET['uid']);

$UserID = $uid;
$_SESSION['UserID'] = $uid;

intranet_auth();
intranet_opendb();

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);

if(!$isTokenValid) {
    echo $i_general_no_access_right	;
    exit;
}

$libdb = new libdb();
$linterface = new interface_html();
$lcardattend = new libcardstudentattend2();
$lhomework = new libhomework2007();

$yearID = Get_Current_Academic_Year_ID();

$show_date = date("Y/m/d",strtotime($date));
$ToDate = date("Y-m-d",strtotime($date));
$ts_record = strtotime($date);
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
$today = date('m/d/Y');
$today_record = strtotime($today);
$page_load_time = time()+1;

$card_student_daily_log = "CARD_STUDENT_DAILY_LOG_";
$card_student_daily_log .= $txt_year."_";
$card_student_daily_log .= $txt_month;

if($timeslots=="am")
{
    $show_timeslots= $Lang['StudentAttendance']['ApplyLeaveAry']['AM'];
}else{
    $show_timeslots= $Lang['StudentAttendance']['ApplyLeaveAry']['PM'];
}

switch ($timeslots)
{
    case "am": $InSchool = " AND b.AMStatus IN (0,2) ";
        $LeaveStatus = " AND (b.LeaveStatus IS NULL OR b.LeaveStatus = 0) ";
        break;
    case "pm": $InSchool = " AND b.PMStatus IN (0,2) ";
        $LeaveStatus = " AND (b.LeaveStatus IS NULL OR b.LeaveStatus = 0)";
        break;
    default : $InSchool = " AND b.AMStatus IN (0,2) ";
        $LeaveStatus = " AND (b.LeaveStatus IS NULL OR b.LeaveStatus = 0) ";
        break;
}

$classes = array();
$classesArray = $lhomework->getClassInfoByTeacherID(Get_Current_Academic_Year_ID(), $UserID);
foreach ($classesArray as $class) {
    $classes[] = $class['YearClassID']; ;
}
$classes_string = '(-1)';
if(count($classes) > 0) {
	$classes_string = implode(",", $classes);
	$classes_string = "(" . $classes_string . ")";
}

$leaveTimeHDisplay .= $linterface->Get_Time_Selection_Box('sendTime_hour', 'hour',  date('H'));
$leaveTimeDisplay .= " : ";
$leaveTimeIDisplay .= $linterface->Get_Time_Selection_Box('sendTime_minute', 'min',date('i'));

//student select
$namefield = getNameFieldWithClassNumberByLang("a.");
$sql = "SELECT 
					a.UserID,$namefield 
			FROM 
					INTRANET_USER AS a INNER JOIN 
					$card_student_daily_log AS b ON (a.UserID = b.UserID) 
					INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=a.UserID
                    LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
                    LEFT OUTER JOIN YEAR as y ON (yc.YearID = y.YearID)
			WHERE 
					b.DayNumber = $txt_day
					$InSchool
					$LeaveStatus
					AND a.RecordType = 2 
					AND a.RecordStatus IN (0,1,2)
					AND ycu.YearClassID IN ".$classes_string."
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName
			";
//debug_pr($sql);die();

$studentArray = $libdb->returnArray($sql);
$select_student= getSelectByArray($studentArray, "name=studentID id=studentID",$studentID);

echo $libeClassApp->getAppWebPageInitStart();
?>
    <script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.ui.datepicker.js"></script>
    <script id="mobile-datepicker" src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.js"></script>
    <script>

        jQuery(document).ready( function() {

            var msg = "<?=$msg?>";
            if(msg=="add"){
                alert("<?=$Lang['ePost']['ReturnMessage']['AddSuccess']?>");
            }else if(msg=="add_failed"){
                alert("<?=$Lang['ePost']['ReturnMessage']['AddUnsuccess']?>");
            }

            $.datepicker.setDefaults({
                dateFormat: 'yy-mm-dd'
            });
        });

        function back(){
            window.location.href="attendance_list.php?date=<?=$date?>&charactor=<?=$charactor?>&timeslots=<?=$timeslots?>&token=<?=$token?>&uid=<?=$uid?>&ul=<?=$ul?>&is_early_leave=<?=$is_early_leave?>&parLang=<?=$parLang?>";
        }

        function submit(){

            if ($('#studentID').val()=="")
            {
                alert('<?=$i_Discipline_System_alert_PleaseSelectStudent?>');
                return;
            }

            if($('select#sendTime_hour').val()<10){
                leaveTimeHString = "0"+$('select#sendTime_hour').val();
            }   else{
                leaveTimeHString = $('select#sendTime_hour').val();
            }

            if($('select#sendTime_minute').val()<10){
                leaveTimeIString = "0"+$('select#sendTime_minute').val();
            }   else{
                leaveTimeIString = $('select#sendTime_minute').val();
            }

            leaveTimeString = leaveTimeHString + ':' + leaveTimeIString;
            $('input#leaveTime').val(leaveTimeString);

            document.form1.action = "add_early_leave_update.php";
            document.form1.submit();
        }

    </script>
    <link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.css">
    <style type="text/css">
        #body{

            background:#ffffff;
        }

        .form_table tr td{vertical-align:center; line-height:30px; padding-top:5px; padding-bottom:5px; padding-left:2px; padding-right:2px;border-bottom:2px solid #EFEFEF;}

    </style>
    </head>
    <body>

    <div data-role="page" id="body">
        <form id="form1" name="form1" method="post" enctype="multipart/form-data">
            <div data-role="header" data-position="fixed"  style="padding-top: 0px;padding-left: 0px;padding-right: 0px;">
                <table class="form_table" width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr style="background-color: #429DEA;border:0">
                        <td width="10px">
                            <a href="javascript:back();"> <img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/iMail/app_view/white/icon_outbox_white.png" style="width:2em; height:2em;" /></a>
                        </td>
                        <td  align="center">
                            <span style="color:white;text-shadow:none;"><?=$Lang['SysMgr']['Homework']['NewRecord'] ?></span>
                        </td>
                    </tr>
                </table>
            </div>
            <div data-role="content" style="padding-top: 0px;padding-left: 0px;padding-right: 0px;">


                <div id="div_content">
                </div>
                <table class="form_table" width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="padding-left:10px" width="65px">
                            <?=$Lang['StudentAttendance']['RecordDate']?>
                        </td>
                        <td  colspan="2"  style="padding-right:10px">
                            <?=$show_date?>
                        </td>
                    </tr>

                    <tr>
                        <td style="padding-left:10px" width="65px">
                            <?=$Lang['StudentAttendance']['TimeSlot']?>
                        </td>
                        <td  colspan="2"  style="padding-right:10px">
                            <?=$show_timeslots?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:10px">
                            <?=$i_general_students_selected?><span style="color:red">*</span>
                        </td>
                        <td colspan="2"  style="padding-right:10px">
                            <?=$select_student?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:10px">
                            <?=$i_StudentAttendance_Time_Departure?><span style="color:red">*</span>
                        </td>
                        <td style="padding-right:10px">
                            <?=$leaveTimeHDisplay?>
                        </td>
                        <td style="padding-right:10px">
                            <?=$leaveTimeIDisplay?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:10px" >
                            <?=$i_StudentAttendance_Field_CardStation?>
                        </td>
                        <td  colspan="2" style="padding-right:10px">
                            <input type="text" id="leavePlace" name="leavePlace" value="">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:10px" ><?=$i_SmartCard_Frontend_Take_Attendance_Waived?></td>
                        <td colspan="2" style="padding-bottom:10px"><input type="checkbox" name="waived" value=1></td>
                    </tr>

                    <tr>
                        <td style="padding-left:10px" ><?=$Lang['StudentAttendance']['ProveDocument']?></td>
                        <td colspan="2" style="padding-bottom:10px"><input type="checkbox" name="proveDocument" value=1></td>
                    </tr>

                    <tr>
                        <td style="padding-left:10px">
                            <?=$i_Attendance_Reason?>
                        </td>
                        <td colspan="2"  style="padding-right:10px">
                            <input type="text" id="reason" name="reason" value="">
                        </td>
                    </tr>

                    <tr>
                        <td style="padding-left:10px">
                            <?=$Lang['StudentAttendance']['iSmartCardRemark']?>
                        </td>
                        <td colspan="2"  style="padding-right:10px">
                            <input type="text" id="teacherRemark" name="teacherRemark" value="">
                        </td>
                    </tr>

                </table>
                <div id="div1"></div>
                <input type="hidden" value=<?=$token?> name="token">
                <input type="hidden" value=<?=$uid?> name="uid">
                <input type="hidden" value=<?=$ul?> name="ul">
                <input type="hidden" value=<?=$date?> name="date">
                <input type="hidden" value=<?=$timeslots?> name="timeslots">
                <input type="hidden" value=<?=$is_early_leave?> name="is_early_leave" id="is_early_leave">
                <input type="hidden" id="leaveTime" name="leaveTime" value="" />
                <input type="hidden" name="parLang" value="<?=$parLang?>" />
            </div>
            <div data-role="footer" data-position="fixed"  style ="background-color:white;height:70px" align="center">
                <a href="javascript:submit();" class="ui-btn ui-corner-all" style ="background-color:#429DEA;display:block;margin-left:10px;margin-right:10px;
                     padding-top:11px;padding-bottom:11px;font-size:1.3em;font-weight:normal;color:white !important;text-shadow:none;" ><?=$button_submit?></a>

            </div>
        </form>
    </div>

    </body>
    </html>

<?php

intranet_closedb();

?>