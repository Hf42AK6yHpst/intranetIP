<?php

$PATH_WRT_ROOT = "../../../../";

//set language
@session_start();
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");

$uid = IntegerSafe($_GET['uid']);
$early_leave_recordid = IntegerSafe($_GET['early_leave_recordid']);
$UserID = $uid;
$_SESSION['UserID'] = $uid;

intranet_auth();
intranet_opendb();

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);

if(!$isTokenValid) {
	echo $i_general_no_access_right	;
	exit;
}

$libdb = new libdb();
$linterface = new interface_html();
$lcardattend = new libcardstudentattend2();
$lhomework = new libhomework2007();

$yearID = Get_Current_Academic_Year_ID();

$show_date = date("Y/m/d",strtotime($date));
$ToDate = date("Y-m-d",strtotime($date));
$ts_record = strtotime($date);
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
$today = date('m/d/Y');
$today_record = strtotime($today);
$page_load_time = time()+1;

$card_student_daily_log = "CARD_STUDENT_DAILY_LOG_";
$card_student_daily_log .= $txt_year."_";
$card_student_daily_log .= $txt_month;

if($timeslots=="am") {
	$DayType = 2;
	$show_timeslots= $Lang['StudentAttendance']['ApplyLeaveAry']['AM'];
} else {
	$DayType = 3;
	$show_timeslots= $Lang['StudentAttendance']['ApplyLeaveAry']['PM'];
}

$ExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_EARLY,'c.');
$LastModifyUserNameField = getNameFieldByLang2("u.");

$sql  = "SELECT 
					b.RecordID, 
					a.UserID,
	        ".getNameFieldWithClassNumberByLang("a.")."as name,
			a.Gender,
          b.LeaveSchoolTime,
          IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
          c.Reason,
          c.RecordStatus, 
		  c.DocumentStatus, 
          ".$ExpectedReasonField." ExpectedReason,
		  d.Remark,
		  c.OfficeRemark,
		  c.DateModified, 
		  $LastModifyUserNameField as LastModifyUser 
		FROM
			$card_student_daily_log as b 
			LEFT JOIN INTRANET_USER as a 
			ON (b.UserID = a.UserID) 
			LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c 
			ON c.StudentID = a.UserID 
				AND c.RecordDate = '".$lcardattend->Get_Safe_Sql_Query($ToDate)."' 
				AND c.DayType = '".$lcardattend->Get_Safe_Sql_Query($DayType)."' 
				AND c.RecordType = '".PROFILE_TYPE_EARLY."' 
			LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d 
			ON (a.UserID = d.StudentID AND d.RecordDate ='".$lcardattend->Get_Safe_Sql_Query($ToDate)."' AND d.DayType='".$lcardattend->Get_Safe_Sql_Query($DayType)."') 
			LEFT JOIN INTRANET_USER as u ON u.UserID=c.ModifyBy
         WHERE 
         		b.DayNumber = '".$lcardattend->Get_Safe_Sql_Query($txt_day)."' 
         		AND c.RecordDate = '".$lcardattend->Get_Safe_Sql_Query($ToDate)."' 
				AND c.DayType = '".$lcardattend->Get_Safe_Sql_Query($DayType)."' 
				AND c.RecordType = '".PROFILE_TYPE_EARLY."' 
         		AND a.RecordType = 2 
         		AND a.RecordStatus IN (0,1,2)
                AND b.RecordID='".$lcardattend->Get_Safe_Sql_Query($early_leave_recordid)."'
		";
//debug_r($sql);
$result = $lcardattend->returnArray($sql,13);
if($result) {
	list($record_id, $user_id, $name, $gender, $leave_time, $leave_station, $reason, $record_status, $document_status, $ExpectedReason, $remark, $office_remark, $date_modified, $last_modify_user) = $result[0];
	$select_status = "<select id=\"drop_down_status\" name=\"drop_down_status\" onChange=\"setReasonComp(this.value, this.form.reason, this.form.editAllowed)\">\n";
	$select_status .= "<option value=\"0\">".$Lang['StudentAttendance']['Present']."</option>\n";
	$select_status .= "<option value=\"3\" SELECTED>".$Lang['StudentAttendance']['EarlyLeave']."</option>\n";
	$select_status .= "</select>\n";
	$select_status .= "<input name=\"record_id\" type=\"hidden\" value=\"$record_id\">\n";
	$select_status .= "<input name=\"user_id\" type=\"hidden\" value=\"$user_id\">\n";
	$waived_option = "<input type=\"checkbox\" name=\"waived_{$user_id}\"".(($record_status == 1) ? " CHECKED" : " ") .">";
	$reason_comp = "<input type=\"text\" name=\"reason\" id=\"reason\" class=\"textboxnum\" maxlength=\"255\" value=\"".htmlspecialchars($ExpectedReason,ENT_QUOTES)."\" style=\"background:$enable_color\">";
	$remark_input = "<input type=\"text\" name=\"remark\" id=\"remark\" maxlength=\"255\" value=\"".htmlspecialchars($remark, ENT_QUOTES)."\" />";
}
//var_dump($result);
$disable_color="#DFDFDF";  # background color for disabled text field
$enable_color="#FFFFFF";  # background color for enabled text field
echo $libeClassApp->getAppWebPageInitStart();
?>
	<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.ui.datepicker.js"></script>
	<script id="mobile-datepicker" src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.js"></script>
	<script>

        jQuery(document).ready( function() {

            var msg = "<?=$msg?>";
            if(msg=="add"){
                alert("<?=$Lang['ePost']['ReturnMessage']['AddSuccess']?>");
            }else if(msg=="add_failed"){
                alert("<?=$Lang['ePost']['ReturnMessage']['AddUnsuccess']?>");
            }

            $.datepicker.setDefaults({
                dateFormat: 'yy-mm-dd'
            });
        });

        function back(){
            window.location.href="attendance_list.php?date=<?=$date?>&charactor=<?=$charactor?>&timeslots=<?=$timeslots?>&token=<?=$token?>&uid=<?=$uid?>&ul=<?=$ul?>&is_early_leave=<?=$is_early_leave?>&parLang=<?=$parLang?>";
        }

        function submit(){
            if(confirm('<?=$i_SmartCard_Confirm_Update_Attend?>')) {
                document.form1.action = "edit_early_leave_update.php";
                document.form1.submit();
            }
        }

        function setReasonComp(value, txtComp, hiddenFlag)
        {
            if (value==3)
            {
                txtComp.disabled = false;
                txtComp.style.background='<?=$enable_color?>';
                hiddenFlag.value = 1;
            }
            else
            {
                txtComp.disabled = true;
                txtComp.value="";
                txtComp.style.background='<?=$disable_color?>';
                hiddenFlag.value = 0;
            }
        }
	</script>
	<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.css">
	<style type="text/css">
		#body{

			background:#ffffff;
		}

		.form_table tr td{vertical-align:center; line-height:30px; padding-top:5px; padding-bottom:5px; padding-left:2px; padding-right:2px;border-bottom:2px solid #EFEFEF;}

	</style>
	</head>
	<body>

	<div data-role="page" id="body">
		<form id="form1" name="form1" method="post" enctype="multipart/form-data">
			<div data-role="header" data-position="fixed"  style="padding-top: 0px;padding-left: 0px;padding-right: 0px;">
				<table class="form_table" width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr style="background-color: #429DEA;border:0">
						<td width="10px">
							<a href="javascript:back();"> <img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/iMail/app_view/white/icon_outbox_white.png" style="width:2em; height:2em;" /></a>
						</td>
						<td  align="center">
							<span style="color:white;text-shadow:none;"><?=$Lang['SysMgr']['Homework']['EditRecord'] ?></span>
						</td>
					</tr>
				</table>
			</div>
			<div data-role="content" style="padding-top: 0px;padding-left: 0px;padding-right: 0px;">


				<div id="div_content">
				</div>
				<table class="form_table" width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td style="padding-left:10px" width="65px">
							<?=$Lang['StudentAttendance']['RecordDate']?>
						</td>
						<td  colspan="2"  style="padding-right:10px">
							<?=$show_date?>
						</td>
					</tr>

					<tr>
						<td style="padding-left:10px" width="65px">
							<?=$Lang['StudentAttendance']['TimeSlot']?>
						</td>
						<td  colspan="2"  style="padding-right:10px">
							<?=$show_timeslots?>
						</td>
					</tr>
					<tr>
						<td style="padding-left:10px">
                            <?=$Lang['General']['StudentName']?>
						</td>
						<td colspan="2"  style="padding-right:10px">
                            <?=$name?>
						</td>
					</tr>
					<tr>
						<td style="padding-left:10px">
							<?=$i_StudentAttendance_Time_Departure?>
						</td>
						<td colspan="2" style="padding-right:10px">
							<?=$leave_time?>
						</td>
					</tr>
                    <tr>
                        <td style="padding-left:10px" >
							<?=$i_SmartCard_Frontend_Take_Attendance_Status?>
                        </td>
                        <td  colspan="2" style="padding-right:10px">
							<?=$select_status?>
                        </td>
                    </tr>
					<tr>
						<td style="padding-left:10px" >
							<?=$i_StudentAttendance_Field_CardStation?>
						</td>
						<td  colspan="2" style="padding-right:10px">
							<?=$leave_station?>
						</td>
					</tr>
					<tr>
						<td style="padding-left:10px" ><?=$i_SmartCard_Frontend_Take_Attendance_Waived?></td>
						<td colspan="2" style="padding-bottom:10px">
                            <?=$waived_option?></td>
					</tr>

					<tr>
						<td style="padding-left:10px" ><?=$Lang['StudentAttendance']['ProveDocument']?></td>
						<td colspan="2" style="padding-bottom:10px"><?=$linterface->Get_Checkbox("DocumentStatus_".$user_id, "DocumentStatus_".$user_id, "1",$document_status == '1', 'ClassDocumentStatus', '')?></td>
					</tr>

					<tr>
						<td style="padding-left:10px">
							<?=$i_Attendance_Reason?>
						</td>
						<td colspan="2"  style="padding-right:10px">
							<?=$reason_comp?>
						</td>
					</tr>

					<tr>
						<td style="padding-left:10px">
							<?=$Lang['StudentAttendance']['iSmartCardRemark']?>
						</td>
						<td colspan="2"  style="padding-right:10px">
							<?=$remark_input?>
						</td>
					</tr>

				</table>
				<div id="div1"></div>
				<input type="hidden" value="<?=$token?>" name="token">
				<input type="hidden" value="<?=$uid?>" name="uid">
				<input type="hidden" value="<?=$ul?>" name="ul">
				<input type="hidden" value="<?=$date?>" name="date">
				<input type="hidden" value="<?=$timeslots?>" name="timeslots">
				<input type="hidden" value="<?=$is_early_leave?>" name="is_early_leave" id="is_early_leave">
				<input type="hidden" id="leaveTime" name="leaveTime" value="" />
                <input type="hidden" name="editAllowed" value="1">
                <input type="hidden" value="<?=$DayType?>" name="DayType">
                <input type="hidden" value="<?=$early_leave_recordid?>" name="early_leave_recordid">
                <input name=PageLoadTime type="hidden" value="<?=time()+1?>">
                <input type="hidden" name="parLang" value="<?=$parLang?>" />
			</div>
			<div data-role="footer" data-position="fixed"  style ="background-color:white;height:70px" align="center">
				<a href="javascript:submit();" class="ui-btn ui-corner-all" style ="background-color:#429DEA;display:block;margin-left:10px;margin-right:10px;
                     padding-top:11px;padding-bottom:11px;font-size:1.3em;font-weight:normal;color:white !important;text-shadow:none;" ><?=$button_submit?></a>

			</div>
		</form>
	</div>

	</body>
	</html>

<?php

intranet_closedb();

?>