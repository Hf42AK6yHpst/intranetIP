<?php

$PATH_WRT_ROOT = "../../../../";

//set language
@session_start();
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$parLang = isset($_POST['parLang'])? $_POST['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');

$uid = IntegerSafe($_POST['uid']);
$early_leave_recordid = IntegerSafe($_POST['early_leave_recordid']);

$UserID = $uid;
$_SESSION['UserID'] = $uid;

intranet_auth();
intranet_opendb();

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);

if(!$isTokenValid) {
	echo $i_general_no_access_right	;
	exit;
}

$TargetDate = date("Y-m-d",strtotime($date));

$lcardattend = new libcardstudentattend2();
$has_magic_quotes = $lcardattend->is_magic_quotes_active;

$ts_record = strtotime($TargetDate);
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

$return_url = 'attendance_list.php?date='.$TargetDate.'&charactor='.$charactor.'&timeslots='.$timeslots.'&token='.$token.'&uid='.$uid.'&ul='.$ul.'&is_early_leave='.$is_early_leave.'&parLang='.$parLang;

if ($ts_record == -1 || ($DayType!=PROFILE_DAY_TYPE_PM && $DayType!=PROFILE_DAY_TYPE_AM) )
{
	intranet_closedb();
	header("Location: $return_url");
	exit();
}


if ($plugin['eSchoolBus']) {
	include ($PATH_WRT_ROOT. "includes/eSchoolBus/schoolBusConfig.php");
	include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus_db.php");
	$leSchoolBus = new libSchoolBus_db();
}

list($school_year_id, $school_year, $semester_id, $semester) = getAcademicYearInfoAndTermInfoByDate($TargetDate);

### update daily records
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

$log_row = date("Y-m-d H:i:s").' '.$_SERVER['REQUEST_URI'].' '.$_SESSION['UserID'].' ';

$sql = "SELECT * FROM $card_log_table_name WHERE RecordID='$early_leave_recordid'";

$records = $lcardattend->returnResultSet($sql);
if(count($records) != 1) {
	intranet_closedb();
	header("Location: $return_url");
	exit();
}
$record = $records[0];

$record_updated = false;

$pageload_time_verify = true;

$my_user_id = $record['UserID'];
$my_day = $txt_day;
$my_drop_down_status = $drop_down_status;
$my_record_id = $record['RecordID'];

// check and skip outdated record if attendance record last modified time is later than page load time

if ($record['DateModified'] != '') {
	$am_modified_ts = strtotime($record['DateModified']);
	if ($am_modified_ts >= $PageLoadTime) {
		$pageload_time_verify = false;
	}
}
if ($record['PMDateModified'] != '') {
	$pm_modified_ts = strtotime($record['PMDateModified']);
	if ($pm_modified_ts >= $PageLoadTime) {
		$pageload_time_verify = false;
	}
}


if($pageload_time_verify == true) {

	// Retrieve Waived only if early leave
	($my_drop_down_status == 3) ? ($my_record_status = (${"waived_" . $my_user_id} == '') ? 0 : 1) : "";

	// KENNETH CHUNG CODE
	if ($DayType == PROFILE_DAY_TYPE_AM) {
		$InSchoolTimeField = "InSchoolTime";
		$StatusField = "AMStatus";
		$DateModifiedField = "DateModified";
		$ModifyByField = "ModifyBy";
	} else {
		$lcardattend->retrieveSettings();
		if ($lcardattend->attendance_mode == 1)    # retrieve InSchoolTime
		{
			$InSchoolTimeField = "InSchoolTime";
		} else # retrieve LunchBackTime
		{
			$InSchoolTimeField = "LunchBackTime";
		}
		$StatusField = "PMStatus";
		$DateModifiedField = "PMDateModified";
		$ModifyByField = "PMModifyBy";
	}

	if ($my_drop_down_status == 3)       # Early Leave
	{
		# Reason input
		$txtReason = trim(htmlspecialchars_decode(stripslashes(${"reason"}), ENT_QUOTES));

		$Result[$my_user_id . 'SetProfile'] = $lcardattend->Set_Profile($my_user_id, $TargetDate, $DayType, $my_drop_down_status, $txtReason, "|**NULL**|", "|**NULL**|", "|**NULL**|", "", false, $my_record_status, true);

		$document_status = $_POST['DocumentStatus_' . $my_user_id];
		$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DocumentStatus='$document_status' WHERE RecordDate='$TargetDate' AND StudentID='$my_user_id' AND DayType='$DayType' AND RecordType='" . PROFILE_TYPE_EARLY . "'";
		$lcardattend->db_db_query($sql);

		if ($plugin['eSchoolBus']) {
			$isTakingBusStudent = $leSchoolBus->isTakingBusStudent($my_user_id);
			if ($isTakingBusStudent) {
				$teacherRemark = trim($remark);
				if ($has_magic_quotes) {
					$teacherRemark = stripslashes($teacherRemark);
				}
				$input_reason = stripslashes(trim(${"reason"}));
				$dayTypeStr = 'PM';

				// check if apply leave record already exist
				$isAppliedLeave = $leSchoolBus->isTimeSlotAppliedLeave($my_user_id, $TargetDate, $TargetDate, $dayTypeStr, $dayTypeStr);
				if ($isAppliedLeave) {
					// update reason and teacher's remark only
					$Result['UpdateReasonAndRemark_' . $my_record_id] = $leSchoolBus->updateReasonAndRemark($my_user_id, $TargetDate, $my_record_id, $input_reason, $teacherRemark);
				} else {    // add record
					$Result['SyncAbsentRecordToeSchoolBus_' . $my_record_id] = $leSchoolBus->syncAbsentRecordFromAttendance($my_user_id, $TargetDate, $input_reason, $teacherRemark, $dayTypeStr, $my_record_id, $attendanceType = 3);
				}
			}
		}

	} else if ($my_drop_down_status == 0)    # On Time
	{
		# Remove Reason record
		$sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
		       WHERE RecordDate = '$TargetDate'
		             AND StudentID = '$my_user_id'
		             AND DayType = '" . $DayType . "'
		             AND RecordType = '" . PROFILE_TYPE_EARLY . "'";
		$lcardattend->db_db_query($sql);
		# Remove Profile record
		$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
		       WHERE AttendanceDate = '$TargetDate'
		             AND UserID = '$my_user_id'
		             AND DayType = '" . $DayType . "'
		             AND RecordType = '" . PROFILE_TYPE_EARLY . "'";
		$lcardattend->db_db_query($sql);
		# Set to Daily Record Table
		# Set LeaveStatus and InSchoolTime
		$sql = "UPDATE $card_log_table_name
		      SET 
			      LeaveStatus = NULL,
			      " . $DateModifiedField . " = NOW(),
			      " . $ModifyByField . " = '" . $_SESSION['UserID'] . "' 
		      WHERE DayNumber = '$txt_day'
		            AND UserID = $my_user_id";
		//echo "sql [".$sql."]<br>";
		$lcardattend->db_db_query($sql);

		if ($DayType == PROFILE_DAY_TYPE_AM && $lcardattend->attendance_mode == 3 && $lcardattend->PMStatusNotFollowAMStatus != 1) {
			$Result[$my_user_id . 'SetPMStatus'] = $lcardattend->Auto_Set_PM_Status(CARD_STATUS_PRESENT, $my_user_id, $TargetDate);
		}


		// Early Leave -> Present on Time
		if ($plugin['eSchoolBus']) {
			$isTakingBusStudent = $leSchoolBus->isTakingBusStudent($my_user_id);
			if ($isTakingBusStudent) {
				$conditionAry['AttendanceRecordID'] = $my_record_id;
				$conditionAry['StudentID'] = $my_user_id;
				$conditionAry['StartDate'] = $TargetDate;
				$conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
				$Result['ChangeFromAbsentToOnTime_' . $my_record_id] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);
			}
		}

	}

	$remark_value = trim($remark);
	if (!$use_magic_quotes) {
		$remark_value = addslashes($remark_value);
	}
	$lcardattend->updateTeacherRemark($my_user_id, $TargetDate, $DayType, $remark_value);


	$log_row .= '[' . $TargetDate . ' ' . $DayType . ' ' . $my_user_id . ' 3 ' . $my_drop_down_status . ']';

	if ($DayType == PROFILE_DAY_TYPE_AM) {
		$period_type = PROFILE_DAY_TYPE_AM;
	} else if ($DayType == PROFILE_DAY_TYPE_PM) {
		$period_type = PROFILE_DAY_TYPE_PM;
	} else {
		$period_type = "";
	}
	if ($period_type != "") {
		# Update Confirm Record
		$sql = "UPDATE CARD_STUDENT_DAILY_DATA_CONFIRM
         SET EarlyConfirmed = 1, EarlyConfirmTime = now(), EarlyConfirmUserID='$UserID', DateModified = now()
         WHERE RecordDate = '$TargetDate' AND RecordType = '$period_type'";
		$lcardattend->db_db_query($sql);
		if ($lcardattend->db_affected_rows() != 1)         # Not Exists
		{
			# Not exists
			$sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate, EarlyConfirmed, RecordType, EarlyConfirmTime, EarlyConfirmUserID, DateInput, DateModified)
            VALUES ('$TargetDate',1,'$period_type',now(),'$UserID',now(),now())";
			$lcardattend->db_db_query($sql);
		}
	}

	$lcardattend->log($log_row);

	$record_updated = true;
}

intranet_closedb();

if($record_updated) {
	$msg = 1;
} else {
	$msg = 2;
}
header('Location:attendance_list.php?msg='.$msg.'&date='.$date.'&charactor='.$charactor.'&timeslots='.$timeslots.'&token='.$token.'&uid='.$uid.'&ul='.$ul.'&is_early_leave='.$is_early_leave.'&parLang='.$parLang.'');

?>