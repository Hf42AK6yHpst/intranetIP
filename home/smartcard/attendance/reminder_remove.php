<?
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");

intranet_opendb();

if(!$plugin['attendancestudent'])
{
	header("Location: /");
	intranet_closedb();
    exit();
}

$ladminjob = new libadminjob($UserID);
if (!$ladminjob->isSmartAttendenceAdmin())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$li = new libdb();

$list = "'".implode("','",$ReminderID)."'";
if($list!=""){
	$sql = "DELETE FROM CARD_STUDENT_REMINDER WHERE ReminderID IN ($list)";
	$li->db_db_query($sql);
}

header ("Location: reminder.php?xmsg=delete");
?>
