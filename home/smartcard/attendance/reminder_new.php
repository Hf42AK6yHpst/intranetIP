<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$linterface 	= new interface_html();
$lsmartcard	= new libsmartcard();
$CurrentPage	= "PageReminderRecord";

if(!$plugin['attendancestudent'])
{
	header("Location: $intranet_httppath/");
        exit();
}

$ladminjob = new libadminjob($UserID);
if (!$ladminjob->isSmartAttendenceAdmin())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$lclass = new libclass();
if (!isset($ClassName) || $ClassName == "")
{
        $temp = $lclass->getClassList();
        $ClassName= $temp[0][1];
        
}

$lclass = new libclass();
$select_class = $lclass->getSelectClass("name='ClassName' onChange=\"this.form.action='';this.form.submit()\"",$ClassName);
$select_student = $lclass->getStudentSelectByClass($ClassName,"name=StudentID", "", 1);

$lcard = new libcardstudentattend();

$namefield = getNameFieldByLang();
$sql = "SELECT $namefield FROM INTRANET_USER WHERE UserID='$UserID'";
$staff = $lclass->returnVector($sql,1);
$select_teacher = $staff[0];

### Title ###
$TitleTitle1 = "<span class='contenttitle'><img src='{$image_path}/{$LAYOUT_SKIN}/smartcard/icon_reiminderrecord.gif' align='absmiddle' />". $i_StudentAttendance_Menu_OtherFeatures_Reminder  ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" nowrap=nowrap>".$TitleTitle1."</td></tr></table>";
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($TitleTitle, "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_new.($intranet_session_language=="en"?" ":""). $i_StudentAttendance_Reminder, "");

?>
<SCRIPT LANGUAGE="Javascript">
function checkForm(obj){
                sid = obj.StudentID;
                if(sid==null)return false;
                if(sid.selectedIndex<0) {
                        alert('<?=$i_Profile_SelectUser?>');
                        return false;

                }

                if(!check_date(obj.RemindDate, "<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
                if(!check_text(obj.Reason, "<?=$i_alert_pleasefillin.$i_StudentAttendance_Reminder_Reason?>")) return false;

}

function dateValidation(obj){
                        if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
                        return true;
 }
</SCRIPT>

<br />   
<form name="form1" method="post" action="reminder_new_update.php" onSubmit="return checkForm(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$xmsg?></td>
</tr>
<tr>
	<td colspan="2">
                <table width="96%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_SmartCard_ClassName?> <span class='tabletextrequire'>*</span></span></td>
					<td><?=$select_class?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_UserStudentName?> <span class='tabletextrequire'>*</span></span></td>
					<td><?=$select_student?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_StudentAttendance_Reminder_Date?> <span class='tabletextrequire'>*</span></span></td>
					<td><input name="RemindDate" type="text" class="textboxnum" maxlength="255" /><?=$linterface->GET_CALENDAR("form1","RemindDate");?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_StudentAttendance_Reminder_Teacher?> </span></td>
					<td class="tabletext"><?=$select_teacher?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_StudentAttendance_Reminder_Reason?> <span class='tabletextrequire'>*</span></span></td>
		                   	<td><?=$linterface->GET_TEXTAREA('Reason', '', 50, 5)?></td>
				</tr>
			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='reminder.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>                        

</form>

<?
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.ClassName");
$linterface->LAYOUT_STOP();
?>
