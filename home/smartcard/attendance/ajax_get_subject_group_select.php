<?php
// Editing by 
/*
 * 2016-09-29 (Carlos): [ip2.5.7.10.1] Improved to display teaching subject groups only if setting [Class teacher take own class(es) attendance at iSmartCard] is on.  
 * 2015-01-13 (Carlos): added $id_name for the <select> id and name
 * 2014-09-22 (Carlos): if $sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable'], use original design, follow time table to get subject group by teacher and date
 * 						else get subject group by selected subject and target date
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_opendb();

if($sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable']){

	$lcardattend = new libcardstudentattend2();
	$SubjectGroupList = $lcardattend->Get_Subject_Group_Session($_SESSION['UserID'],$TargetDate,true);
	
	$id_name = !isset($id_name)?"SubjectGroup":$id_name;
	
	## subject Group select
	$SubjectGroupSelect = "<SELECT id=\"$id_name\" name=\"$id_name\">\n";
	$empty_selected = ($SubjectGroup == '')? "SELECTED":"";
	$SubjectGroupSelect .= "<OPTION value='' $empty_selected> -- $button_select -- </OPTION>\n";
	for($i=0; $i<sizeOf($SubjectGroupList); $i++)
	{
	  list($SubjectGroupID,$ClassTitleEN,$ClassTitleB5) = $SubjectGroupList[$i];
	  $ClassName = Get_Lang_Selection($ClassTitleB5,$ClassTitleEN);
	  
	  $selected = ($SubjectGroupID==$SubjectGroup)?"SELECTED":"";
	  $SubjectGroupSelect .= "<OPTION value=\"".$SubjectGroupID."\" $selected>".$ClassName."</OPTION>\n";
	}
	$SubjectGroupSelect .= "</SELECT>\n";
	
	echo $SubjectGroupSelect;

}else{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	
	$lcardattend = new libcardstudentattend2();
	$scm_ui = new subject_class_mapping_ui();
	$fcm = new form_class_manage();
	
	if(!isset($TargetDate) || $TargetDate == ''){
		$TargetDate = date("Y-m-d");
	}
	$id_name = !isset($id_name)?"SubjectGroup":$id_name;
	
	$YearTermInfo = $fcm->Get_Academic_Year_And_Year_Term_By_Date($TargetDate);
	$YearTermID = $YearTermInfo[0]['YearTermID'];
	
	$TeachingOnly = $lcardattend->ClassTeacherTakeOwnClassOnly? 1 : 0;
	
	$SubjectGroupSelect = $scm_ui->Get_Subject_Group_Selection($SubjectID, $id_name, $SubjectGroup, $OnChange, $YearTermID, $IsMultiple_=0, $AcademicYearID_='', isset($NoFirst)?$NoFirst:0, $DisplayTermInfo_=0, $TeachingOnly);
	
	echo $SubjectGroupSelect;
}

intranet_closedb();
?>