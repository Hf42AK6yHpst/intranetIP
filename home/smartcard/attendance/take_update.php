<?php
// Editing by 
##################################### Change Log ###############################################
# 2018-10-22 by Carlos: [ip.2.5.9.11.1] Added cust flag $sys_custom['SmartCardAttendance_SubmittedProveDocumentForEarlyLeave'] for updating the submitted prove document status of early leave.
# 2018-03-12 by Carlos: Only update [Submitted Prove Document] when setting TeacherCanManageProveDocumentStatus is on.
# 2018-01-30 by Carlos: Fine tune group admin checking, if a group hasn't set any admin users, default staff can take it, student can only take is group admin groups.
# 2017-11-22 by Carlos: [ip.2.5.9.1.1] added checking to only allow group admin to take attendance. 
# 2017-05-31 by Carlos: [ip.2.5.8.7.1] Check and skip outdated record if attendance record last modified time is later than page load time. 
# 2017-04-12 by Carlos: [ip.2.5.8.4.1] Allow manage [Waived], [Reason] and [Teacher's remark] according to settings. Update DocumentStatus for absent records.
# 2017-03-21 by Carlos: [ip.2.5.8.4.1] use libcardstudentattend2->updateTeacherRemark() to manage teacher remark records.
# 2017-02-24 by Carlos: [ip2.5.8.3.1] added status change log for tracing purpose. required new method libcardstudentattend2->log()
# 2016-07-05 by Carlos: [ip2.5.7.7.1] $sys_custom['StudentAttendance']['AllowEditTime'] - update in time if time is being edited.
# 2015-07-14 by Carlos: Redirect back if disallow to edit n days before records. 
# 2015-04-27 by Carlos: Fix fail to remove eDiscipline late record for status change and setting OnlySynceDisLateRecordWhenConfirm on. 
# 2015-02-13 by Carlos: Added [Office Remark].
# 2014-03-21 by Carlos: When clear profile should always clean eDiscipline late misconduct records together
# 2014-03-17 by Carlos: Added Absent Session for $sys_custom['SmartCardAttendance_StudentAbsentSession2']
# 2013-05-14 by Carlos: Add subject group confirm record
# 2013-04-16 by Carlos: Add CARD_STUDENT_DAILY_REMARK.DayType
# 2012-05-04 by Carlos: Modify to add no card entrance log if and only if no both InSchoolTime and LunchBackTime 
# 2012-03-16 by Carlos: Fix Pm use PMDateModified and PMModifyBy
# 2012-03-22 by Carlos: check setting OnlySynceDisLateRecordWhenConfirm to update eDiscipline late record at Set_Profile()
# 2010-04-23 by Carlos: Allow take attendance of past dates
# 2010-04-22 by Carlos: Allow input late session if student is late
################################################################################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

$lcardattend = new libcardstudentattend2();

if(method_exists($lcardattend,'getAttendanceGroupRecords')){
	$attendance_groups = $lcardattend->getAttendanceGroupRecords(array());
	$responsible_user_groups = $lcardattend->getAttendanceGroupAdminUsers(array('GroupID'=>Get_Array_By_Key($attendance_groups,'GroupID'),'UserID'=>$_SESSION['UserID']));
	$responsible_user_group_ids = Get_Array_By_Key($responsible_user_groups,'GroupID');
	
	if($_SESSION['UserType'] == USERTYPE_STAFF){
		// get all group admins with group id as key to admin users
		$groupIdToAdminUsers = $lcardattend->getAttendanceGroupAdminUsers(array('GroupID'=>Get_Array_By_Key($attendance_groups,'GroupID'),'GroupIDToUsers'=>1));
		// go through all groups to see any admin users added to the group
		for($i=0;$i<count($attendance_groups);$i++){
			if(!isset($groupIdToAdminUsers[$attendance_groups[$i]['GroupID']]) && !in_array($attendance_groups[$i]['GroupID'],$responsible_user_group_ids)){
				// if the group hasn't set any admin, staff can take it
				$responsible_user_group_ids[] = $attendance_groups[$i]['GroupID'];
			}
		}
	}
	
	$responsible_user_group_id_size = count($responsible_user_group_ids);
	$isGroupAdmin = $responsible_user_group_id_size>0;
}

$ladminjob = new libadminjob($UserID);
if (!($ladminjob->isSmartAttendenceAdmin() || $isGroupAdmin))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$lu = new libuser($UserID);
$isStudentHelper = $lu->isStudent();

$LIDB = new libdb();
$LICLASS = new libclass();

if(!isset($ToDate) || $ToDate=="" || strtotime($ToDate)==-1 || !intranet_validateDate($ToDate))
{
	$ToDate=date('Y-m-d');
}
$TargetDate=$ToDate;
$ts_record = strtotime($ToDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

$day_type = $period == "AM"? PROFILE_DAY_TYPE_AM : PROFILE_DAY_TYPE_PM;

$msg = "";
/*
if($user_id != "")
{
	//CHECK RECORD UPDATED AFTER LOAD THE SUBMIT PAGE, CHECK DATA CONSISTENCE
	if($lcardattend->isDataOutdated($user_id,$TargetDate,$PageLoadTime)){
		$msg = 99;  // data outdated

		$return_url = 	($hidGroup_id =="") ? "take_attendance.php?class_name=$class_name" : "take_attendance_group.php?group_id=$group_id";

		$return_url .= "&period=$period&msg=$msg";
		$return_url .= "&ToDate=$TargetDate";
		header("Location: $return_url");
		exit();
	}
}
*/
//$use_magic_quotes = get_magic_quotes_gpc();
$use_magic_quotes = $lcardattend->is_magic_quotes_active;

$upadteeDisLateRecord = $lcardattend->OnlySynceDisLateRecordWhenConfirm != '1';

$EditDaysBeforeRecord = $lcardattend->EditDaysBeforeRecord == 1;
$EditDaysBeforeRecordSelect = $lcardattend->EditDaysBeforeRecordSelect;
$IsEditDaysBeforeDisabled = false;
if($EditDaysBeforeRecord)
{
	$ts_target_date = strtotime($ToDate);
	$ts_today = strtotime(date("Y-m-d"));
	$ts_edit_days_before = $ts_today - $EditDaysBeforeRecordSelect * 86400;
	$IsEditDaysBeforeDisabled = $ts_target_date <= $ts_edit_days_before; 
}
if($IsEditDaysBeforeDisabled){
	$return_url = 	($hidGroup_id =="") ? "take_attendance.php?class_name=".urlencode($class_name) : "take_attendance_group.php?group_id=".urlencode($group_id);
	$return_url .= "&period=".urlencode($period)."&msg=".urlencode($msg);
	$return_url .= "&ToDate=".urlencode($TargetDate);
	header("Location: $return_url");
	exit();
}

$log_row = date("Y-m-d H:i:s").' '.$_SERVER['REQUEST_URI'].' '.$_SESSION['UserID'].' ';

### for student confirm
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
$my_record_date = $TargetDate;

if(sizeof($user_id)>0){
	$user_id = IntegerSafe($user_id);
	$student_list = "'".implode("','",$user_id)."'";
	## Get Remark list
  $sql = "SELECT RecordID,StudentID FROM CARD_STUDENT_DAILY_REMARK WHERE RecordDate='".$LIDB->Get_Safe_Sql_Query($my_record_date)."' AND StudentID IN ( $student_list) AND DayType='".$LIDB->Get_Safe_Sql_Query($day_type)."'";
  $r = $LIDB->returnArray($sql,2);
  for($i=0;$i<sizeof($r);$i++){
    $rid = $r[$i][0];
    $student_id = $r[$i][1];
    $remarkResult[$student_id]['RecordID']= $rid;
  }
  
  ## Get Preset Absence List
	if($_SESSION['client_is_ppc']){ ########### PPC Version #####################
		$my_period = $period=="AM"?2:3;
		$sql="SELECT RecordID,StudentID,Reason FROM CARD_STUDENT_PRESET_LEAVE WHERE StudentID IN($student_list) AND RecordDate='".$LIDB->Get_Safe_Sql_Query($my_record_date)."' AND DayPeriod='".$LIDB->Get_Safe_Sql_Query($my_period)."'";
		$tt = $LIDB->returnArray($sql,3);
		for($i=0;$i<sizeof($tt);$i++){
			list($tt_id,$tt_sid,$tt_reason)=$tt[$i];
			$preset_absence[$tt_sid]['reason']=$tt_reason;
		}
		/*
		for($i=0; $i<sizeOf($user_id); $i++){
	    $my_user_id = $user_id[$i];
	    $my_day = $txt_day;
	    $my_drop_down_status = $drop_down_status[$i];
	    $my_record_id = $record_id[$i];
	
			$remark_record_id = $remarkResult[$my_user_id]['RecordID'];
	
			if( $my_drop_down_status!=CARD_STATUS_ABSENT && $my_drop_down_status!=CARD_STATUS_LATE && $remark_record_id!=""){
	      # remove Remarks if :
	      # 1. not absent/late
	      $remark_list_to_remove[] =$remark_record_id;
			}else if( ($my_drop_down_status==CARD_STATUS_ABSENT) && $remark_record_id==""){
				// Late / Absent AND No Remark
				// 1. Set Preset Absent as Remark ?
				$preset_absence_reason = $preset_absence[$my_user_id]['reason'];
				if($preset_absence_reason!="")
					$remark_list_to_insert[] ="('$my_user_id','$my_record_date','$preset_reason')";
			}
	
	    if( $period == "AM"){
	            # insert if not exist
	            if($my_record_id==""){
	                    $sql = "INSERT INTO $card_log_table_name
	                                   (UserID,DayNumber,AMStatus,ConfirmedUserID,DateInput,DateModified) VALUES
	                                   ('$my_user_id','$my_day','$my_drop_down_status','$UserID',NOW(),NOW())
	                                            ";
											$LIDB->db_db_query($sql);
	
	                    # Check Bad actions
	                    if ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status == CARD_STATUS_LATE)           # empty -> present
	                    {
	                      $lcardattend->removeBadActionFakedCardAM($my_user_id,$my_record_date);
	                        # Forgot to bring card
	                        $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,"");
	                    }
	            }
	            # update if exist
	            else{
	                 # Grab original status
	                 $sql = "SELECT AMStatus, InSchoolTime, RecordID FROM $card_log_table_name WHERE RecordID = '$my_record_id'";
	                 $temp = $LIDB->returnArray($sql,3);
	                 list($old_status, $old_inTime, $old_record_id) = $temp[0];
	                 if ($old_status != $my_drop_down_status)
	                 {
	                     # Check bad actions
	                     if ( ($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_ABSENT)
	                     {
	                       $lcardattend->removeBadActionNoCardEntrance($my_user_id,$my_record_date);
	                        if($old_inTime!=""){ # with time but absent
	                             # Has card record but not present in classroom
	                             $lcardattend->addBadActionFakedCardAM($my_user_id,$my_record_date,$old_inTime);
	                           }
	                     }
	
	                     # Absent -> Late / Present
	                     if ( $old_status==CARD_STATUS_ABSENT && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE))
	                     {
	                           $lcardattend->removeBadActionFakedCardAM($my_user_id,$my_record_date);
	                             # forgot to bring card
	                             $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,$old_inTime);
	                     }
	
	
	                     # Update Daily table
	                    $sql = "UPDATE $card_log_table_name SET AMStatus='$my_drop_down_status', ConfirmedUserID='$UserID', DateModified = now() WHERE RecordID='$my_record_id'";
	                    $LIDB->db_db_query($sql);
	                 }
	
	                 # Try to remove profile records if applicable
	                 if ($my_drop_down_status != CARD_STATUS_ABSENT)
	                 {
	                     # Remove Previous Absent Record
	                     $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
	                                    WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$my_user_id'
	                                          AND AttendanceDate = '$my_record_date' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
	                     $lcardattend->db_db_query($sql);
	                     $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
	                                    WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$my_user_id'
	                                          AND RecordDate = '$my_record_date' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
	                     $lcardattend->db_db_query($sql);
	                 }
	
	            }
	            
	            # Deal with Staff Input Reason for Status - Absent, Late and Outing. No need to record reason for Present. 
	            if($sys_custom['SmartCardAttendance_StaffInputReason'])
	            {
	            	if($my_drop_down_status != CARD_STATUS_PRESENT)
	            	{
	            		$txtInputReason = ${"input_reason$i"};
	    				$txtInputReason = intranet_htmlspecialchars($txtInputReason);
	    				
	    				# Get ProfileRecordID
	    				$sql = "SELECT RecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
	                   			WHERE RecordDate = '$my_record_date'
	                             AND StudentID = '$my_user_id'
	                             AND DayType = '".PROFILE_DAY_TYPE_AM."'";
	                $temp = $lcardattend->returnArray($sql,1);
	                
	                list($reason_record_id) = $temp[0];
	                
	                $record_type = $my_drop_down_status;
	                if($my_drop_down_status == CARD_STATUS_ABSENT || $my_drop_down_status == CARD_STATUS_OUTING)
	                	$record_type = PROFILE_TYPE_ABSENT;
	                else if($my_drop_down_status == CARD_STATUS_LATE)
	                	$record_type = PROFILE_TYPE_LATE;
	                # If Reason record not exists
	    				if($reason_record_id == "")
	    				{
	    					# Insert to Reason table
	                    $fieldname = "RecordDate, StudentID, Reason, DayType, RecordType, DateInput, DateModified";
	                    $fieldsvalues = "'$my_record_date', '$my_user_id','$txtInputReason', '".PROFILE_DAY_TYPE_AM."', '$record_type', now(), now() ";
	                    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
	                                   VALUES ($fieldsvalues)";
	                    $lcardattend->db_db_query($sql);
	    				}else
	    				{
	    					# Update Reason table
	    					$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason = '$txtInputReason', RecordType = '$record_type', DateModified = now() WHERE RecordID = '$reason_record_id' ";
	                  	$lcardattend->db_db_query($sql);
	    				}
	            	}else if($my_drop_down_status == CARD_STATUS_PRESENT)
	            	{
	            		# Try to Delete Reason Record if exists
	            		$sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$my_record_date' AND StudentID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_AM."'";
	        			$lcardattend->db_db_query($sql);
	            	}
	            }
	            
	            # Record number of absent sessions for Status Absent
	            if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
	            {
	            	if($my_drop_down_status == CARD_STATUS_ABSENT || $my_drop_down_status == CARD_STATUS_LATE)
	            	{
	            		$profile_type = ($my_drop_down_status==CARD_STATUS_ABSENT?PROFILE_TYPE_ABSENT:PROFILE_TYPE_LATE);
	            		# Get RecordID
	    				$sql = "SELECT RecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
	                   			WHERE RecordDate = '$my_record_date'
	                             AND StudentID = '$my_user_id'
	                             AND DayType = '".PROFILE_DAY_TYPE_AM."'";
	                $temp = $lcardattend->returnArray($sql,1);
	                
	                list($reason_record_id) = $temp[0];
	                $my_absent_session = $absent_session[$i];
	                
	                if($reason_record_id == "")
	    				{
	    					# Insert to Reason table
	                    $fieldname = "RecordDate, StudentID, DayType, RecordType, AbsentSession, DateInput, DateModified";
	                    $fieldsvalues = "'$my_record_date', '$my_user_id', '".PROFILE_DAY_TYPE_AM."', '".$profile_type."','$my_absent_session', now(), now() ";
	                    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
	                                   VALUES ($fieldsvalues)";
	                    $lcardattend->db_db_query($sql);
	    				}else
	    				{
	    					# Update Reason table
	    					$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET RecordType='".$profile_type."', AbsentSession = '$my_absent_session', DateModified = now() WHERE RecordID = '$reason_record_id' ";
	                  	$lcardattend->db_db_query($sql);
	    				}
	            	}
	            }
	    }
	    else if( $period == "PM"){
	            # insert if not exist
	            if($my_record_id==""){
	                    $sql = "INSERT INTO $card_log_table_name
	                                   (UserID,DayNumber,PMStatus,ConfirmedUserID,DateInput,DateModified) VALUES
	                                   ('$my_user_id','$my_day','$my_drop_down_status','$UserID',NOW(),NOW())
	                                            ";
	                    $LIDB->db_db_query($sql);
	                    # Check Bad actions
	                    if ($my_drop_down_status==CARD_STATUS_PRESENT  || $my_drop_down_status==CARD_STATUS_LATE)           # empty -> present
	                    {
	                      $lcardattend->removeBadActionFakedCardPM($my_user_id,$my_record_date);
	                        # Forgot to bring card
	                        $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,"");
	                    }
	            }
	            # update if exist
	            else{
	                 # Grab original status
	                 $sql = "SELECT PMStatus, InSchoolTime, LunchOutTime, LunchBackTime, RecordID FROM $card_log_table_name WHERE RecordID = '$my_record_id'";
	                 $temp = $LIDB->returnArray($sql,5);
	                 list($old_status, $old_inTime,$old_lunchOutTime, $old_lunchBackTime, $old_record_id) = $temp[0];
	
	                ## get old time
		        if ($lcardattend->attendance_mode==1){ # PM only
					$bad_action_time_field = $old_inTime;
				}else{
					$bad_action_time_field = $old_lunchBackTime;
				}
	
	                 if ($old_status != $my_drop_down_status)
	                 {
	                    if ( ($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_ABSENT)
	                     {
	
	                           $lcardattend->removeBadActionNoCardEntrance($my_user_id,$my_record_date);
							if($bad_action_time_field!=""){ # has time but absent
	                             # Has card record but not present in classroom
	                             $lcardattend->addBadActionFakedCardPM($my_user_id,$my_record_date,$bad_action_time_field);
	                           }
	                     }
	
	                     # Absent -> Late / Present
	                     if ( $old_status==CARD_STATUS_ABSENT && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE))
	                     {
	                           $lcardattend->removeBadActionFakedCardPM($my_user_id,$my_record_date);
	                           # forgot to bring card
	                           $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,$bad_action_time_field);
	                     }
	
	                     # Update Daily table
	                     $sql = "UPDATE $card_log_table_name SET PMStatus='$my_drop_down_status', ConfirmedUserID='$UserID', DateModified=now() WHERE RecordID='$my_record_id'";
	                     $LIDB->db_db_query($sql);
	                 }
	
	                 # Try to remove profile records if applicable
	                 if ($my_drop_down_status != CARD_STATUS_ABSENT)
	                 {
	                     # Remove Previous Absent Record
	                     $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
	                                    WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$my_user_id'
	                                          AND AttendanceDate = '$my_record_date' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
	                     $lcardattend->db_db_query($sql);
	                     $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
	                                    WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$my_user_id'
	                                          AND RecordDate = '$my_record_date' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
	                     $lcardattend->db_db_query($sql);
	                 }
	            }
	            
	            # Deal with Staff Input Reason for Status - Absent, Late and Outing. No need to record reason for Present. 
	            if($sys_custom['SmartCardAttendance_StaffInputReason'])
	            {
	            	if($my_drop_down_status != CARD_STATUS_PRESENT)
	            	{
	            		$txtInputReason = ${"input_reason$i"};
	    				$txtInputReason = intranet_htmlspecialchars($txtInputReason);
	    				
	    				# Get ProfileRecordID
	    				$sql = "SELECT RecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
	                   			WHERE RecordDate = '$my_record_date'
	                             AND StudentID = '$my_user_id'
	                             AND DayType = '".PROFILE_DAY_TYPE_PM."'";
	                $temp = $lcardattend->returnArray($sql,1);
	                
	                list($reason_record_id) = $temp[0];
	                
	                $record_type = $my_drop_down_status;
	                if($my_drop_down_status == CARD_STATUS_ABSENT || $my_drop_down_status == CARD_STATUS_OUTING)
	                	$record_type = PROFILE_TYPE_ABSENT;
	                else if($my_drop_down_status == CARD_STATUS_LATE)
	                	$record_type = PROFILE_TYPE_LATE;
	                # If Reason record not exists
	    				if($reason_record_id == "")
	    				{
	    					# Insert to Reason table
	                    $fieldname = "RecordDate, StudentID, Reason, DayType, RecordType, DateInput, DateModified";
	                    $fieldsvalues = "'$my_record_date', '$my_user_id','$txtInputReason', '".PROFILE_DAY_TYPE_PM."', '$record_type', now(), now() ";
	                    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
	                                   VALUES ($fieldsvalues)";
	                    $lcardattend->db_db_query($sql);
	    				}else
	    				{
	    					# Update Reason table
	    					$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason = '$txtInputReason', RecordType = '$record_type', DateModified = now() WHERE RecordID = '$reason_record_id' ";
	                  	$lcardattend->db_db_query($sql);
	    				}
	            	}else if($my_drop_down_status == CARD_STATUS_PRESENT)
	            	{
	            		# Try to Delete Reason Record if exists
	            		$sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$my_record_date' AND StudentID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_PM."'";
	        			$lcardattend->db_db_query($sql);
	            	}
	            }
	            
	            # Record number of absent sessions for Status Absent
	            if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
	            {
	            	if($my_drop_down_status == CARD_STATUS_ABSENT || $my_drop_down_status == CARD_STATUS_LATE)
	            	{
	            		$profile_type = ($my_drop_down_status==CARD_STATUS_ABSENT?PROFILE_TYPE_ABSENT:PROFILE_TYPE_LATE);
	            		# Get RecordID
	    				$sql = "SELECT RecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
	                   			WHERE RecordDate = '$my_record_date'
	                             AND StudentID = '$my_user_id'
	                             AND DayType = '".PROFILE_DAY_TYPE_PM."'";
	                $temp = $lcardattend->returnArray($sql,1);
	                
	                list($reason_record_id) = $temp[0];
					$my_absent_session = $absent_session[$i];
	                
	                if($reason_record_id == "")
	    				{
	    					# Insert to Reason table
	                    $fieldname = "RecordDate, StudentID, DayType, RecordType, AbsentSession, DateInput, DateModified";
	                    $fieldsvalues = "'$my_record_date', '$my_user_id', '".PROFILE_DAY_TYPE_PM."', '".$profile_type."','$my_absent_session', now(), now() ";
	                    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
	                                   VALUES ($fieldsvalues)";
	                    $lcardattend->db_db_query($sql);
	    				}else
	    				{
	    					# Update Reason table
	    					$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET RecordType='".$profile_type."', AbsentSession = '$my_absent_session', DateModified = now() WHERE RecordID = '$reason_record_id' ";
	                  	$lcardattend->db_db_query($sql);
	    				}
	            	}
	            }
	    }
	
		}
	  ## Remove Remark
	  if(sizeof($remark_list_to_remove)>0){
	    $r_list = implode(",",$remark_list_to_remove);
	    $sql = "DELETE FROM CARD_STUDENT_DAILY_REMARK WHERE RecordID IN ($r_list)";
	    $lcardattend->db_db_query($sql);
	  }
	  ## Insert Remark
	  if(sizeof($remark_list_to_insert)>0){
	    $r_list = implode(",",$remark_list_to_insert);
	    $sql ="INSERT INTO CARD_STUDENT_DAILY_REMARK (StudentID,RecordDate,Remark) VALUES $r_list ";
	    $lcardattend->db_db_query($sql);
	  }
		*/
	} // End of PPC Version
	else{ ##################### PC Version #########################
		if($isStudentHelper){
			$my_period = $period=="AM"?2:3;
			$s_list = "'".implode("','",$user_id)."'";
			$sql="SELECT RecordID,StudentID,Reason FROM CARD_STUDENT_PRESET_LEAVE WHERE StudentID IN($s_list) AND RecordDate='".$LIDB->Get_Safe_Sql_Query($my_record_date)."' AND DayPeriod='".$LIDB->Get_Safe_Sql_Query($my_period)."'";
			$tt = $LIDB->returnArray($sql,3);
			for($i=0;$i<sizeof($tt);$i++){
				list($tt_id,$tt_sid,$tt_reason)=$tt[$i];
				$preset_absence[$tt_sid]['reason']=$tt_reason;
			}
		}
	} // End of PC Version
}

//$insertRemarkSQL="INSERT INTO CARD_STUDENT_DAILY_REMARK (StudentID,RecordDate,DayType,Remark)VALUES";
//$removeRemarkSQL="DELETE FROM CARD_STUDENT_DAILY_REMARK WHERE RecordID IN( ";
//$insertRemark = false;
//$removeRemark = false;

$studentIdToDailyLog = $lcardattend->getDailyLogRecords(array('RecordDate'=>$TargetDate,'StudentID'=>$user_id,'StudentIDToRecord'=>1));

for($i=0; $i<sizeOf($user_id); $i++){
  $my_user_id = $user_id[$i];
  $my_day = $txt_day;
  $my_drop_down_status = $drop_down_status[$i];
  $my_record_id = $record_id[$i];
  $prev_status = $PrevStatus[$i];
  
  	// check and skip outdated record if attendance record last modified time is later than page load time
	if(isset($studentIdToDailyLog[$my_user_id])){
		if($studentIdToDailyLog[$my_user_id]['DateModified'] != '')
		{
			$am_modified_ts = strtotime($studentIdToDailyLog[$my_user_id]['DateModified']);
			if($am_modified_ts >= $PageLoadTime) continue;
		}
		if($studentIdToDailyLog[$my_user_id]['PMDateModified'] != '')
		{
			$pm_modified_ts = strtotime($studentIdToDailyLog[$my_user_id]['PMDateModified']);
			if($pm_modified_ts >= $PageLoadTime) continue;
		}
	}
  
  $remark_record_id = $remarkResult[$my_user_id]['RecordID'];
	//$my_reason = trim(htmlspecialchars_decode(stripslashes(${"reason".$i}), ENT_QUOTES));
	$my_reason = trim($_POST["reason".$i]);
	// Retrieve Waived only if absent/ late
  $my_record_status = (($my_drop_down_status == CARD_STATUS_ABSENT || $my_drop_down_status == CARD_STATUS_LATE || $my_drop_down_status==CARD_STATUS_OUTING) && !$isStudentHelper)? (${"Waived".$i} == ''?0:1) :'|**NULL**|';
  if(!$lcardattend->TeacherCanManageWaiveStatus){
  	$my_record_status = 'NULL';
  }

  # insert / update CARD_STUDENT_DAILY_REMARK
  # if Student Helper, and No Remark, then use Preset Absence reason for the Remark
  if($isStudentHelper && $my_drop_down_status==CARD_STATUS_ABSENT && $remark_record_id==""){
    $preset_reason = $preset_absence[$my_user_id]['reason'];
    if($preset_reason!=""){
    	$my_reason = $preset_reason;
      //$insertRemark = true;
      //$insertRemarkSQL .="('$my_user_id','$my_record_date','$day_type','$preset_reason'),";
  	}
	}
/*
  if($my_reason!=""){
    # insert
    if($remark_record_id==""){
      $insertRemark = true;
      $insertRemarkSQL .="('$my_user_id','$my_record_date','$day_type','".$LIDB->Get_Safe_Sql_Query($my_reason)."'),";
    }
    # update
    else{
      $updateRemarkSQL ="UPDATE CARD_STUDENT_DAILY_REMARK SET Remark='".$LIDB->Get_Safe_Sql_Query($my_reason)."' WHERE RecordID='$remark_record_id'";
      $LIDB->db_db_query($updateRemarkSQL);
    }
  }else if( ( ($my_drop_down_status!=CARD_STATUS_ABSENT && $my_drop_down_status!=CARD_STATUS_LATE)
  			|| !$isStudentHelper) && $remark_record_id!=""){
    # remove Remarks if :
    # 1. not absent/late
    # 2. remark is null , and not student helper
    $removeRemark=true;
    $removeRemarkSQL .=$remark_record_id.",";
  }
*/
  // code added by kenneth
	if ($period == "AM"){
		$DayType = PROFILE_DAY_TYPE_AM;
		$StatusField = "AMStatus";
		$ConfirmField = "IsConfirmed";
		$ConfirmByField = "ConfirmedUserID";
		
		$DateModifiedField = "DateModified";
		$ModifyByField = "ModifyBy";
	}
	else {
		$DayType = PROFILE_DAY_TYPE_PM;
		$StatusField = "PMStatus";
		$ConfirmField = "PMIsConfirmed";
		$ConfirmByField = "PMConfirmedUserID";
		
		$DateModifiedField = "PMDateModified";
		$ModifyByField = "PMModifyBy";
	}
	
	if($my_record_id==""){
		$sql = "INSERT INTO $card_log_table_name
           	(UserID,DayNumber,".$StatusField.",".$ConfirmField.",".$ConfirmByField.",DateInput,InputBy,".$DateModifiedField.",".$ModifyByField.") 
           VALUES
           	('".$LIDB->Get_Safe_Sql_Query($my_user_id)."','".$LIDB->Get_Safe_Sql_Query($my_day)."','".$LIDB->Get_Safe_Sql_Query($my_drop_down_status)."','1','".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."')
           ";
	    $LIDB->db_db_query($sql);
	    
	    # Check Bad actions
	    if ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE)           # empty -> present
	    {
	      $lcardattend->removeBadActionFakedCard($my_user_id,$my_record_date,$DayType);
	      # Forgot to bring card
	      $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,"");
	    }
	    $RealInTime = "";
	    
	    $log_row .= '['.$TargetDate.' '.$DayType.' '.$my_user_id.' NULL '.$my_drop_down_status.']';
	}
	else {
		# Grab original status
		$sql = "SELECT ".$StatusField.", InSchoolTime, LunchOutTime, LunchBackTime, RecordID FROM $card_log_table_name WHERE RecordID = '".$LIDB->Get_Safe_Sql_Query($my_record_id)."'";
		$temp = $LIDB->returnArray($sql,5);
		list($old_status, $old_inTime,$old_lunchOutTime, $old_lunchBackTime, $old_record_id) = $temp[0];

		## get old time
		if ($lcardattend->attendance_mode==1 || $DayType == PROFILE_DAY_TYPE_AM){ # PM only
			$RealInTime = $old_inTime;
		}else{
			$RealInTime = $old_lunchBackTime;
		}

		if ($old_status != $my_drop_down_status)
		{
			if (($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_ABSENT)
			{
				$lcardattend->removeBadActionNoCardEntrance($my_user_id,$my_record_date);
				if($bad_action_time_field!=""){ # has time but absent
			  	# Has card record but not present in classroom
			    $lcardattend->addBadActionFakedCard($my_user_id,$my_record_date,$RealInTime,$DayType);
			  }
			}
		
			# Absent -> Late / Present
			if (($old_status==CARD_STATUS_ABSENT || trim($old_status) == "") && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE))
			{
				$lcardattend->removeBadActionFakedCard($my_user_id,$my_record_date,$DayType);
				# forgot to bring card
				if(trim($old_inTime)=='' && trim($old_lunchBackTime)==''){ // add no card entrance log if and only if no both AM in time and PM in time
					$lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,$RealInTime);
				}
			}
			$LastModifyUpdate = ", ".$DateModifiedField."=now(),
													 ".$ModifyByField."='".$_SESSION['UserID']."' ";
		}
		else {
			$LastModifyUpdate = "";
		}
		# Update Daily table
		$sql = "UPDATE $card_log_table_name SET 
							".$StatusField."='".$LIDB->Get_Safe_Sql_Query($my_drop_down_status)."', 
							".$ConfirmField." = '1',
							".$ConfirmByField."='".$_SESSION['UserID']."' 
							".$LastModifyUpdate." 
						WHERE RecordID='".$LIDB->Get_Safe_Sql_Query($my_record_id)."'";
		$LIDB->db_db_query($sql);
		
		$log_row .= '['.$TargetDate.' '.$DayType.' '.$my_user_id.' '.$old_status.' '.$my_drop_down_status.']';
	}
	
	# Try to remove profile records if applicable
	if ($my_drop_down_status == CARD_STATUS_ABSENT || $my_drop_down_status == CARD_STATUS_LATE || $my_drop_down_status == CARD_STATUS_OUTING) {	
		// cust field
		$txtInputReason = trim(htmlspecialchars_decode(stripslashes(${"input_reason$i"}), ENT_QUOTES));
		if(!$lcardattend->TeacherCanManageReason){
			$txtInputReason = null;
		}
		
		//if($my_drop_down_status != CARD_STATUS_LATE){
		//	$lcardattend->Clear_Profile($my_user_id,$my_record_date,$DayType,true);
		//}
		$lcardattend->Set_Profile($my_user_id,$my_record_date,$DayType,$my_drop_down_status,$txtInputReason,"|**NULL**|","|**NULL**|","|**NULL**|",$RealInTime,false,$my_record_status,false,$upadteeDisLateRecord);
		if($my_drop_down_status == CARD_STATUS_ABSENT && $lcardattend->TeacherCanManageProveDocumentStatus){
			$HandIn_prove_sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DocumentStatus='".$LIDB->Get_Safe_Sql_Query($_POST['HandIn_prove_'.$i])."' WHERE StudentID='".$LIDB->Get_Safe_Sql_Query($my_user_id)."' AND RecordDate='".$LIDB->Get_Safe_Sql_Query($my_record_date)."' AND RecordType='".$LIDB->Get_Safe_Sql_Query($my_drop_down_status)."'";
    		$lcardattend->db_db_query($HandIn_prove_sql);
		}
	}
	else {
		$lcardattend->Clear_Profile($my_user_id,$my_record_date,$DayType,true);
	}
	// end code added by kenneth
	
	if($sys_custom['SmartCardAttendance_StudentAbsentSession'] || $sys_custom['SmartCardAttendance_StudentAbsentSession2']) {
		$sql = "select 
							LateSession,
							RequestLeaveSession,
							PlayTruantSession,
							OfficalLeaveSession 
							".($sys_custom['SmartCardAttendance_StudentAbsentSession2']?",AbsentSession ":"")."  
						from 
							CARD_STUDENT_STATUS_SESSION_COUNT 
						where 
							RecordDate = '".$LIDB->Get_Safe_Sql_Query($my_record_date)."' 
							and 
							StudentID = '".$LIDB->Get_Safe_Sql_Query($my_user_id)."' 
							and 
							DayType = '".$LIDB->Get_Safe_Sql_Query($DayType)."'";
		$OrgRecord = $lcardattend->returnArray($sql);
		
		if ($OrgRecord[0]['LateSession'] != $late_session[$i] 
				|| $OrgRecord[0]['RequestLeaveSession'] != $request_leave_session[$i] 
				|| $OrgRecord[0]['PlayTruantSession'] != $play_truant_session[$i] 
				|| $OrgRecord[0]['OfficalLeaveSession'] != $offical_leave_session[$i]
				|| $OrgRecord[0]['AbsentSession'] != $absent2_session[$i]) {
			# Update Daily table
			$sql = "UPDATE ".$card_log_table_name." SET 
  							".$DateModifiedField."=now(),
								".$ModifyByField."='".$_SESSION['UserID']."' 
						WHERE 
							DayNumber = '".$LIDB->Get_Safe_Sql_Query($my_day)."' 
    					and 
							UserID='".$LIDB->Get_Safe_Sql_Query($my_user_id)."'";
			$Result['UpdateLastModify:'.$my_user_id] = $lcardattend->db_db_query($sql);
		}
		
		// insert/update the number of sessions to db
		$sql = "insert into CARD_STUDENT_STATUS_SESSION_COUNT (
							RecordDate,
							StudentID,
							DayType,
							LateSession,
							RequestLeaveSession,
							PlayTruantSession,
							OfficalLeaveSession,
							".($sys_custom['SmartCardAttendance_StudentAbsentSession2']?"AbsentSession,":"")."
							DateInput,
							InputBy,
							DateModify,
							ModifyBy
							)
						values (
							'".$LIDB->Get_Safe_Sql_Query($my_record_date)."',
							'".$LIDB->Get_Safe_Sql_Query($my_user_id)."',
							'".$LIDB->Get_Safe_Sql_Query($DayType)."',
							'".$LIDB->Get_Safe_Sql_Query($late_session[$i])."',
							'".$LIDB->Get_Safe_Sql_Query($request_leave_session[$i])."',
							'".$LIDB->Get_Safe_Sql_Query($play_truant_session[$i])."',
							'".$LIDB->Get_Safe_Sql_Query($offical_leave_session[$i])."',
							".($sys_custom['SmartCardAttendance_StudentAbsentSession2']?"'".$LIDB->Get_Safe_Sql_Query($absent2_session[$i])."',":"")."
							NOW(),
						  '".$_SESSION['UserID']."',
						  NOW(),
						  '".$_SESSION['UserID']."'
						 ) 
						on duplicate key update 
							LateSession = '".$LIDB->Get_Safe_Sql_Query($late_session[$i])."',
							RequestLeaveSession = '".$LIDB->Get_Safe_Sql_Query($request_leave_session[$i])."',
							PlayTruantSession = '".$LIDB->Get_Safe_Sql_Query($play_truant_session[$i])."',
							OfficalLeaveSession = '".$LIDB->Get_Safe_Sql_Query($offical_leave_session[$i])."',
							".($sys_custom['SmartCardAttendance_StudentAbsentSession2']?"AbsentSession = '".$LIDB->Get_Safe_Sql_Query($absent2_session[$i])."',":"")." 
							DateModify = NOW(),
							ModifyBy = '".$_SESSION['UserID']."'";
		$lcardattend->db_db_query($sql);
	}
	
	if($lcardattend->TeacherCanManageTeacherRemark)
	{
		$remark_value = $my_reason;
		if(!$use_magic_quotes){
			$remark_value = addslashes($remark_value);
		}
		$lcardattend->updateTeacherRemark($my_user_id,$TargetDate,$DayType,$remark_value);
	}
	// handle Office remark
	if($my_drop_down_status != CARD_STATUS_PRESENT){
		$office_remark_value = trim($OfficeRemark[$i]);
		if(!$use_magic_quotes){
			$office_remark_value = addslashes($office_remark_value);
		}
		$RealProfileType = $my_drop_down_status;
		if($my_drop_down_status == CARD_STATUS_OUTING){
			$RealProfileType = CARD_STATUS_ABSENT;
		}
		$lcardattend->updateOfficeRemark($my_user_id,$TargetDate,$DayType,$RealProfileType,$office_remark_value);
	}
	
	if($sys_custom['StudentAttendance']['AllowEditTime'] && isset($_POST['InSchoolTime'.$my_user_id]))
	{
		$lcardattend->updateAttendanceInTime($my_user_id,$TargetDate,$DayType,$_POST['InSchoolTime'.$my_user_id]);
	}
	
	if($sys_custom['SmartCardAttendance_SubmittedProveDocumentForEarlyLeave'] && $my_drop_down_status != CARD_STATUS_ABSENT && $lcardattend->TeacherCanManageProveDocumentStatus){
		$HandInEL_prove_sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DocumentStatus='".$LIDB->Get_Safe_Sql_Query($_POST['HandInEL_prove_'.$i])."' WHERE StudentID='".$LIDB->Get_Safe_Sql_Query($my_user_id)."' AND RecordDate='".$LIDB->Get_Safe_Sql_Query($TargetDate)."' AND RecordType='".PROFILE_TYPE_EARLY."'";
    	$lcardattend->db_db_query($HandInEL_prove_sql);
	}
	
	/*
  if($period == "AM"){
    # insert if not exist
    if($my_record_id==""){
      $sql = "INSERT INTO $card_log_table_name
                     (UserID,DayNumber,AMStatus,ConfirmedUserID,DateInput,DateModified) VALUES
                     ($my_user_id,$my_day,$my_drop_down_status,$UserID,NOW(),NOW())
                              ";
      $LIDB->db_db_query($sql);

      # Check Bad actions
      if ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE)           # empty -> present
      {
      	$lcardattend->removeBadActionFakedCardAM($my_user_id,$my_record_date);
        # Forgot to bring card
        $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,"");
      }
    }
    # update if exist
    else{
			# Grab original status
			$sql = "SELECT AMStatus, InSchoolTime, RecordID FROM $card_log_table_name WHERE RecordID = $my_record_id";
			$temp = $LIDB->returnArray($sql,3);
			list($old_status, $old_inTime, $old_record_id) = $temp[0];
			if ($old_status != $my_drop_down_status)
			{
				# Check bad actions
				if (($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_ABSENT)
				{
					$lcardattend->removeBadActionNoCardEntrance($my_user_id,$my_record_date);
					if($old_inTime!=""){
					 # Has card record but not present in classroom
					 $lcardattend->addBadActionFakedCardAM($my_user_id,$my_record_date,$old_inTime);
					}	
				}
			
				# Absent -> Late / Present
				if ($old_status==CARD_STATUS_ABSENT && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE))
				{
					$lcardattend->removeBadActionFakedCardAM($my_user_id,$my_record_date);
					# forgot to bring card
					$lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,$old_inTime);
				}
				# Update Daily table
			  $sql = "UPDATE $card_log_table_name SET AMStatus=$my_drop_down_status, ConfirmedUserID=$UserID, DateModified = now() WHERE RecordID=$my_record_id";
			  $LIDB->db_db_query($sql);
			}

			# Try to remove profile records if applicable
			if ($my_drop_down_status != CARD_STATUS_ABSENT)
			{
			   # Remove Previous Absent Record
			   $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
			                  WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$my_user_id'
			                        AND AttendanceDate = '$my_record_date' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
			   $lcardattend->db_db_query($sql);
			   $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
			                  WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$my_user_id'
			                        AND RecordDate = '$my_record_date' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
			   $lcardattend->db_db_query($sql);
			}
    }
          
    # Deal with Staff Input Reason for Status - Absent, Late and Outing. No need to record reason for Present. 
    if($sys_custom['SmartCardAttendance_StaffInputReason'])
    {
    	if($my_drop_down_status != CARD_STATUS_PRESENT)
    	{
    		$txtInputReason = ${"input_reason$i"};
				$txtInputReason = intranet_htmlspecialchars($txtInputReason);
				
				# Get ProfileRecordID
				$sql = "SELECT RecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
               			WHERE RecordDate = '$my_record_date'
                       AND StudentID = $my_user_id
                       AND DayType = '".PROFILE_DAY_TYPE_AM."'";
        $temp = $lcardattend->returnArray($sql,1);
        
        list($reason_record_id) = $temp[0];
        
        $record_type = $my_drop_down_status;
        if($my_drop_down_status == CARD_STATUS_ABSENT || $my_drop_down_status == CARD_STATUS_OUTING)
        	$record_type = PROFILE_TYPE_ABSENT;
        else if($my_drop_down_status == CARD_STATUS_LATE)
        	$record_type = PROFILE_TYPE_LATE;
        # If Reason record not exists
				if($reason_record_id == "")
				{
					# Insert to Reason table
            $fieldname = "RecordDate, StudentID, Reason, DayType, RecordType, DateInput, DateModified";
            $fieldsvalues = "'$my_record_date', '$my_user_id','$txtInputReason', '".PROFILE_DAY_TYPE_AM."', '$record_type', now(), now() ";
            $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                           VALUES ($fieldsvalues)";
            $lcardattend->db_db_query($sql);
				}else
				{
					# Update Reason table
					$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason = '$txtInputReason', RecordType='$record_type', DateModified = now() WHERE RecordID = '$reason_record_id' ";
          	$lcardattend->db_db_query($sql);
				}
    	}else if($my_drop_down_status == CARD_STATUS_PRESENT)
    	{
    		# Try to Delete Reason Record if exists
    		$sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$my_record_date' AND StudentID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_AM."'";
    			$lcardattend->db_db_query($sql);
    	}
    }
          
    # Record number of absent sessions for Status Absent
    if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
    {
    	if($my_drop_down_status == CARD_STATUS_ABSENT || $my_drop_down_status == CARD_STATUS_LATE)
    	{
    		$profile_type = ($my_drop_down_status==CARD_STATUS_ABSENT?PROFILE_TYPE_ABSENT:PROFILE_TYPE_LATE);
    		# Get RecordID
				$sql = "SELECT RecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
               			WHERE RecordDate = '$my_record_date'
                       AND StudentID = $my_user_id
                       AND DayType = '".PROFILE_DAY_TYPE_AM."'";
        $temp = $lcardattend->returnArray($sql,1);
        
        list($reason_record_id) = $temp[0];
$my_absent_session = $absent_session[$i];
        
        if($reason_record_id == "")
				{
					# Insert to Reason table
            $fieldname = "RecordDate, StudentID, DayType, RecordType, AbsentSession, DateInput, DateModified";
            $fieldsvalues = "'$my_record_date', '$my_user_id', '".PROFILE_DAY_TYPE_AM."', '".$profile_type."','$my_absent_session', now(), now() ";
            $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                           VALUES ($fieldsvalues)";
            $lcardattend->db_db_query($sql);
				}else
				{
					# Update Reason table
					$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET RecordType='".$profile_type."', AbsentSession = '$my_absent_session', DateModified = now() WHERE RecordID = '$reason_record_id' ";
          	$lcardattend->db_db_query($sql);
				}
    	}
    }
  }
  else if( $period == "PM"){
    # insert if not exist
    if($my_record_id==""){
      $sql = "INSERT INTO $card_log_table_name
                     (UserID,DayNumber,PMStatus,ConfirmedUserID,DateInput,DateModified) VALUES
                     ($my_user_id,$my_day,$my_drop_down_status,$UserID,NOW(),NOW())
                              ";
      $LIDB->db_db_query($sql);
      # Check Bad actions
      if ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE)           # empty -> present
      {
        $lcardattend->removeBadActionFakedCardPM($my_user_id,$my_record_date);
        # Forgot to bring card
        $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,"");
      }
    }
    # update if exist
    else{
			# Grab original status
			$sql = "SELECT PMStatus, InSchoolTime, LunchOutTime, LunchBackTime, RecordID FROM $card_log_table_name WHERE RecordID = $my_record_id";
			$temp = $LIDB->returnArray($sql,5);
			list($old_status, $old_inTime,$old_lunchOutTime, $old_lunchBackTime, $old_record_id) = $temp[0];

			## get old time
			if ($lcardattend->attendance_mode==1){ # PM only
				$bad_action_time_field = $old_inTime;
			}else{
				$bad_action_time_field = $old_lunchBackTime;
			}

			if ($old_status != $my_drop_down_status)
			{
				if ( ($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_ABSENT)
				{
				
				     $lcardattend->removeBadActionNoCardEntrance($my_user_id,$my_record_date);
				if($bad_action_time_field!=""){ # has time but absent
				       # Has card record but not present in classroom
				       $lcardattend->addBadActionFakedCardPM($my_user_id,$my_record_date,$bad_action_time_field);
				     }
				}
			
				# Absent -> Late / Present
				if ( $old_status==CARD_STATUS_ABSENT && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE))
				{
				     $lcardattend->removeBadActionFakedCardPM($my_user_id,$my_record_date);
				     # forgot to bring card
				     $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,$bad_action_time_field);
				}
				# Update Daily table
				$sql = "UPDATE $card_log_table_name SET PMStatus=$my_drop_down_status, ConfirmedUserID=$UserID, DateModified=now() WHERE RecordID=$my_record_id";
				$LIDB->db_db_query($sql);
			}
			
			# Try to remove profile records if applicable
			if ($my_drop_down_status != CARD_STATUS_ABSENT)
			{
				# Remove Previous Absent Record
				$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
				              WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$my_user_id'
				                    AND AttendanceDate = '$my_record_date' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
				$lcardattend->db_db_query($sql);
				$sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
				              WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$my_user_id'
				                    AND RecordDate = '$my_record_date' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
				$lcardattend->db_db_query($sql);
			}
    }
    
    # Deal with Staff Input Reason for Status - Absent, Late and Outing. No need to record reason for Present. 
    if($sys_custom['SmartCardAttendance_StaffInputReason'])
    {
    	if($my_drop_down_status != CARD_STATUS_PRESENT)
    	{
    		$txtInputReason = ${"input_reason$i"};
				$txtInputReason = intranet_htmlspecialchars($txtInputReason);
				
				# Get ProfileRecordID
				$sql = "SELECT RecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
               			WHERE RecordDate = '$my_record_date'
                       AND StudentID = $my_user_id
                       AND DayType = '".PROFILE_DAY_TYPE_PM."'";
        $temp = $lcardattend->returnArray($sql,1);
        
        list($reason_record_id) = $temp[0];
        
        $record_type = $my_drop_down_status;
        if($my_drop_down_status == CARD_STATUS_ABSENT || $my_drop_down_status == CARD_STATUS_OUTING)
        	$record_type = PROFILE_TYPE_ABSENT;
        else if($my_drop_down_status == CARD_STATUS_LATE)
        	$record_type = PROFILE_TYPE_LATE;
        # If Reason record not exists
				if($reason_record_id == "")
				{
					# Insert to Reason table
            $fieldname = "RecordDate, StudentID, Reason, DayType, RecordType, DateInput, DateModified";
            $fieldsvalues = "'$my_record_date', '$my_user_id','$txtInputReason', '".PROFILE_DAY_TYPE_PM."', '$record_type', now(), now() ";
            $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                           VALUES ($fieldsvalues)";
            $lcardattend->db_db_query($sql);
				}else
				{
					# Update Reason table
					$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason = '$txtInputReason', RecordType = '$record_type', DateModified = now() WHERE RecordID = '$reason_record_id' ";
          	$lcardattend->db_db_query($sql);
				}
    	}else if($my_drop_down_status == CARD_STATUS_PRESENT)
    	{
    		# Try to Delete Reason Record if exists
    		$sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$my_record_date' AND StudentID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_PM."'";
    			$lcardattend->db_db_query($sql);
    	}
    }
    
    # Record number of absent sessions for Status Absent
    if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
    {
    	if($my_drop_down_status == CARD_STATUS_ABSENT || $my_drop_down_status == CARD_STATUS_LATE)
    	{
    		$profile_type = ($my_drop_down_status==CARD_STATUS_ABSENT?PROFILE_TYPE_ABSENT:PROFILE_TYPE_LATE);
    		# Get RecordID
				$sql = "SELECT RecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
               			WHERE RecordDate = '$my_record_date'
                       AND StudentID = $my_user_id
                       AND DayType = '".PROFILE_DAY_TYPE_PM."'";
        $temp = $lcardattend->returnArray($sql,1);
        
        list($reason_record_id) = $temp[0];
$my_absent_session = $absent_session[$i];
        
        if($reason_record_id == "")
				{
					# Insert to Reason table
            $fieldname = "RecordDate, StudentID, DayType, RecordType, AbsentSession, DateInput, DateModified";
            $fieldsvalues = "'$my_record_date', '$my_user_id', '".PROFILE_DAY_TYPE_PM."', '".$profile_type."','$my_absent_session', now(), now() ";
            $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                           VALUES ($fieldsvalues)";
            $lcardattend->db_db_query($sql);
				}else
				{
					# Update Reason table
					$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET RecordType='".$profile_type."', AbsentSession = '$my_absent_session', DateModified = now() WHERE RecordID = '$reason_record_id' ";
          	$lcardattend->db_db_query($sql);
				}
    	}
    }
  }*/
}
/*
//debug_r($insertRemarkSQL);
//debug_r($removeRemarkSQL);
if($insertRemark){
  $insertRemarkSQL = substr($insertRemarkSQL,0,strlen($insertRemarkSQL)-1);
  $LIDB->db_db_query($insertRemarkSQL);
}
if($removeRemark){
  $removeRemarkSQL =substr($removeRemarkSQL,0,strlen($removeRemarkSQL)-1).")";
  $LIDB->db_db_query($removeRemarkSQL);
}
*/
### for class / GROUP confirm
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;
$card_student_daily_group_confirm = "CARD_STUDENT_DAILY_GROUP_CONFIRM_".$txt_year."_".$txt_month;
$card_student_daily_subject_group_confirm = "CARD_STUDENT_DAILY_SUBJECT_GROUP_CONFIRM_".$txt_year."_".$txt_month;

if( $confirmed_id <> "" ){
	# update if record exist
	$sql = "UPDATE $card_student_daily_class_confirm SET ConfirmedUserID='".$LIDB->Get_Safe_Sql_Query($UserID)."', DateModified = NOW() WHERE RecordID = '".$LIDB->Get_Safe_Sql_Query($confirmed_id)."'";
	if($hidGroup_id != "")
	{
	$sql = "UPDATE $card_student_daily_group_confirm SET ConfirmedUserID='".$LIDB->Get_Safe_Sql_Query($UserID)."', DateModified = NOW() WHERE RecordID = '".$LIDB->Get_Safe_Sql_Query($confirmed_id)."'";
	}
	if($SubjectGroupID != "")
	{
		$sql = "UPDATE $card_student_daily_subject_group_confirm SET ConfirmedUserID='".$LIDB->Get_Safe_Sql_Query($UserID)."', DateModified=NOW() WHERE RecordID='".$LIDB->Get_Safe_Sql_Query($confirmed_id)."'";
	}
	
	$LIDB->db_db_query($sql);
	$msg = 2;
}
else{
	if ($IsSubjectGroup != 1) {
	  # insert if record not exist
	  $class_id = $LICLASS->getClassID($class_name);
	
		//FAI, CASE FOR TAKE GROUP ATTENDANCE
		$class_id = ($hidGroup_id != "") ?$group_id:$class_id;
	
	  switch($period){
		  case "AM": $DayType = 2; break;
		  case "PM": $DayType = 3; break;
		  default : $DayType = 2; break;
	  }
	
		if($hidGroup_id != "")
		{
			$sql = "INSERT INTO $card_student_daily_group_confirm
					(
						GroupID,
						ConfirmedUserID,
	          DayNumber,
	          DayType,
	          DateInput,
						DateModified
					) VALUES
	                  (
						'".$LIDB->Get_Safe_Sql_Query($group_id)."',
	          '".$LIDB->Get_Safe_Sql_Query($UserID)."',
	          '".$LIDB->Get_Safe_Sql_Query($txt_day)."',
	          '".$LIDB->Get_Safe_Sql_Query($DayType)."',
						NOW(),
						NOW()
					)
					";
		}
		else {
			$sql = "INSERT INTO $card_student_daily_class_confirm
	        (
	          ClassID,
	          ConfirmedUserID,
	          DayNumber,
	          DayType,
	          DateInput,
	          DateModified
	        ) VALUES
	        (
	          '".$LIDB->Get_Safe_Sql_Query($class_id)."',
	          '".$LIDB->Get_Safe_Sql_Query($UserID)."',
	          '".$LIDB->Get_Safe_Sql_Query($txt_day)."',
	          '".$LIDB->Get_Safe_Sql_Query($DayType)."',
	          NOW(),
	          NOW()
	        )
	        ";
		}
	  $LIDB->db_db_query($sql);
	  $msg = 1;
	}else if($SubjectGroupID != ""){
		switch($period){
		  case "AM": $DayType = 2; break;
		  case "PM": $DayType = 3; break;
		  default : $DayType = 2; break;
	  	}
		$sql = "INSERT INTO $card_student_daily_subject_group_confirm
	        (
	          SubjectGroupID,
	          ConfirmedUserID,
	          DayNumber,
	          DayType,
	          DateInput,
	          DateModified
	        ) VALUES
	        (
	          '".$LIDB->Get_Safe_Sql_Query($SubjectGroupID)."',
	          '".$LIDB->Get_Safe_Sql_Query($UserID)."',
	          '".$LIDB->Get_Safe_Sql_Query($txt_day)."',
	          '".$LIDB->Get_Safe_Sql_Query($DayType)."',
	          NOW(),
	          NOW()
	        )
	        ";
	    $LIDB->db_db_query($sql);
	  	$msg = 1;
	}
}

if ($IsSubjectGroup != 1) {
	// remove the confirm cache status
	if($hidGroup_id != "") {
		$sql = "delete from CARD_STUDENT_CACHED_CONFIRM_STATUS where TargetDate = '".$LIDB->Get_Safe_Sql_Query($TargetDate)."' and DayType = '".$LIDB->Get_Safe_Sql_Query($DayType)."' and Type = 'Group'";
	}
	else {
		$sql = "delete from CARD_STUDENT_CACHED_CONFIRM_STATUS where TargetDate = '".$LIDB->Get_Safe_Sql_Query($TargetDate)."' and DayType = '".$LIDB->Get_Safe_Sql_Query($DayType)."' and Type = 'Class'";
	}
	$Result["ClearConfirmCache"] = $lcardattend->db_db_query($sql);
} else if($SubjectGroupID != ""){
	$sql = "delete from CARD_STUDENT_CACHED_CONFIRM_STATUS where TargetDate = '".$LIDB->Get_Safe_Sql_Query($TargetDate)."' and DayType = '".$LIDB->Get_Safe_Sql_Query($DayType)."' and Type = 'Subject_Group'";
	$Result["ClearConfirmCache"] = $lcardattend->db_db_query($sql);
}

if($isPPC){
	$return_url = "/ppc/home/attendance/take/";
	$return_url .= ($period=="AM")?"takeAM.php":"";
	$return_url .= ($period=="PM")?"takePM.php":"";
}else{
	//	$return_url = "take_attendance.php";
	if ($hidGroup_id !="") {
		$return_url = "take_attendance_group.php?group_id=".urlencode($group_id);
		
	}
	else if ($IsSubjectGroup == 1) {
		$return_url = 	"take_attendance_subject_group.php?SubjectGroupID=".urlencode($SubjectGroupID);
	}
	else {
		$return_url = 	"take_attendance.php?class_name=".urlencode($class_name);
	}
}

$lcardattend->log($log_row);
intranet_closedb();

$return_url .= "&period=".urlencode($period)."&msg=".urlencode($msg);
$return_url .= "&ToDate=".urlencode($TargetDate);
header( "Location: $return_url");
?>