<?php
// use by kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");

intranet_auth();
intranet_opendb();

$toDay=date('Y-m-d');
$page_load_time = time();
if(!$plugin['attendancelesson'])
{
        header("Location: $intranet_httppath/");
        exit();
}

$luser = new libuser($UserID);

$linterface         = new interface_html();
$lsmartcard			= new libsmartcard();
$lteaching = new libteaching($_SESSION['UserID']);
$CurrentPage        = "PageTakeAttendance";

$TeachingClassList = $lteaching->returnTeacherClass($_SESSION['UserID']);

## Teaching Class select
$TeachingClassSelect = "<SELECT id=\"TeachingClassSelect\" name=\"TeachingClassSelect\">\n";
$empty_selected = ($TeachingClass == '')? "SELECTED":"";
$TeachingClassSelect .= "<OPTION value='' $empty_selected> -- $button_select -- </OPTION>\n";
for($i=0; $i<sizeOf($TeachingClassList); $i++)
{
  list($TeachingClassID,$TeachingClassName) = $TeachingClassList[$i];
  $Value = htmlspecialchars($TeachingClassID.'|=|'.$TeachingClassName,ENT_QUOTES);
  $selected = ($Value==$TeachingClass)?"SELECTED":"";
  $TeachingClassSelect .= "<OPTION value=\"".$Value."\" $selected>".$TeachingClassName."</OPTION>\n";
}
$TeachingClassSelect .= "</SELECT>\n";

### Title ###
$CurrentPage	= "PageClassFloorPlanSetup";
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/smartcard/icon_takeattendance.gif' align='absmiddle' />";
$TitleTitle1 = "<span class='contenttitle'>".$TitleImage1. $Lang['LessonAttendance']['ClassFloorPlanSetup'] ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
$TAGS_OBJ[] = array($TitleTitle, "", 0);

$linterface->LAYOUT_START();

echo "<script language=\"Javascript\" src='/templates/".$LAYOUT_SKIN."/js/lesson_attendance.js'></script>";
?>
<!-- lesson attendance layer-->
<div id="LessonAttendanceLayer">
	<!-- Class Floor Plan setting -->
	<form name="ClassFloorPlanForm" method="get">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	        <td align="center">
	                <table width="96%" border="0" cellspacing="0" cellpadding="0">
	                <tr>
	                        <td align="left" class="tabletext">
	
							<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_general_class?> </span></td>
								<td valign="top" class="tabletext">
									<span id="SessionSelectLayer"><?=$TeachingClassSelect?></span>
								</td>
							</tr>
							</table>
	
	                        </td>
	                </tr>
	                <tr>
	                        <td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;</td>
	                </tr>
	                <tr>
	                        <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	                </tr>
	                <tr>
                    <td colspan="2">
                      <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                      <tr>
                        <td align="center">
                        <?= $linterface->GET_ACTION_BTN($button_view, "button", "View_Class_Default_Floor_Plan();","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                        </td>
                      </tr>
                      </table>
                    </td>
	                </tr>
	                </table>
	        </td>
	</tr>
	</table>
	<br />
	</form>
	<!-- end of class floor plan setting -->
	<div id="LoadingLayer" name="LoadingLayer" style="z-index:0;">
		<div id="LessonAttendanceFlashLayer" name="LessonAttendanceFlashLayer" width="100%" style="height=800px;">
		</div>
	</div>
	<!--<iframe id='SessionFrame' src="<?=$PATH_WRT_ROOT?>home/smartcard/attendance/lesson_attendance/new_start/main1.html""  scrolling='no' frameborder='0' ></iframe>-->
</div>

<SCRIPT LANGUAGE="Javascript">
// lesson attendance
{
var LessonAttendReturnMessage = '<?=$Lang['LessonAttendance']['ClassSeatingPlanSavedSuccess']?>';
	
function View_Class_Default_Floor_Plan() {
	var TeachingClassSelect = document.getElementById('TeachingClassSelect');
	
	Block_Element("LoadingLayer");
	var SessionSelected = TeachingClassSelect.options[TeachingClassSelect.selectedIndex].value;
	var LessonDetail = SessionSelected.split('|=|');
	//var FlashVars = new Array();
	
	LessonAttendFlashVars = {
		RecordID: LessonDetail[0],
		Name: LessonDetail[1],
		LessonDate: "",
		RoomAllocationID: "",
		StartTime: "",
		EndTime: "", 
		ServerPath: "<?='http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT']?>", 
		RecordType:"YearClass", 
		DefaultLang:"<?=$intranet_session_language?>"
	}

	/*FlashVars['SubjectGroupID'] = LessonDetail[0];
	FlashVars['LessonDate'] = LessonDetail[1];
	FlashVars['RoomAllocationID'] = LessonDetail[2];
	FlashVars['StartTime'] = LessonDetail[3];
	FlashVars['EndTime'] = LessonDetail[4];*/
	embedFlash("LessonAttendanceFlashLayer", "lesson_attendance/main1.swf", LessonAttendFlashVars, "#869ca7", "100%", "900px", "9.0.0", "NO");
	UnBlock_Element("LoadingLayer");
}
}
</SCRIPT>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>