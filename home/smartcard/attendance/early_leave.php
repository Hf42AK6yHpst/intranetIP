<?php
//using by
/************************************************* Changes ***********************************************

 *********************************************************************************************************/
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");

intranet_auth();
intranet_opendb();


if(!$plugin['attendancestudent'] && !$plugin['attendancelesson'])
{
	intranet_closedb();
	header("Location: $intranet_httppath/");
	exit();
}

$lcardattend = new libcardstudentattend2();

if(method_exists($lcardattend,'getAttendanceGroupRecords')){
	$attendance_groups = $lcardattend->getAttendanceGroupRecords(array());
	$responsible_user_groups = $lcardattend->getAttendanceGroupAdminUsers(array('GroupID'=>Get_Array_By_Key($attendance_groups,'GroupID'),'UserID'=>$_SESSION['UserID']));
	$responsible_user_group_ids = Get_Array_By_Key($responsible_user_groups,'GroupID');

	if($_SESSION['UserType'] == USERTYPE_STAFF){
		// get all group admins with group id as key to admin users
		$groupIdToAdminUsers = $lcardattend->getAttendanceGroupAdminUsers(array('GroupID'=>Get_Array_By_Key($attendance_groups,'GroupID'),'GroupIDToUsers'=>1));
		// go through all groups to see any admin users added to the group
		for($i=0;$i<count($attendance_groups);$i++){
			if(!isset($groupIdToAdminUsers[$attendance_groups[$i]['GroupID']]) && !in_array($attendance_groups[$i]['GroupID'],$responsible_user_group_ids)){
				// if the group hasn't set any admin, staff can take it
				$responsible_user_group_ids[] = $attendance_groups[$i]['GroupID'];
			}
		}
	}

	$responsible_user_group_id_size = count($responsible_user_group_ids);
}

$ladminjob = new libadminjob($_SESSION['UserID']);
$isSmartAttendanceAdmin = $ladminjob->isSmartAttendenceAdmin();
if (!($isSmartAttendanceAdmin || $responsible_user_group_id_size>0))
{
	intranet_closedb();
	header ("Location: /");
	exit();
}

$TargetDate = isset($_POST['TargetDate'])? $_POST['TargetDate'] : $_GET['TargetDate'];
$DayType = isset($_POST['DayType'])? $_POST['DayType'] : $_GET['DayType'];

$lc = new libcardstudentattend2();
$CurrentPage = "PageEarlyLeave";

$linterface = new interface_html();
$StudentAttendUI = new libstudentattendance_ui();


### class used
$lword = new libwordtemplates();
$luser = new libuser($_SESSION['UserID']);
$lsmartcard	= new libsmartcard();


$lc->retrieveSettings();
if ($TargetDate == "")
{
	$TargetDate = date('Y-m-d');
}

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
	$TargetDate = date('Y-m-d');
	$ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

###period
switch ($DayType)
{
	case PROFILE_DAY_TYPE_AM:
		$display_period = $i_DayTypeAM;
		$LeaveType = CARD_LEAVE_AM;
		break;
	case PROFILE_DAY_TYPE_PM:
		$display_period = $i_DayTypePM;
		$LeaveType = CARD_LEAVE_PM;
		break;
	default :
		if ($lc->attendance_mode == 1) {
			$display_period = $i_DayTypePM;
			$LeaveType = CARD_LEAVE_PM;
			$DayType = PROFILE_DAY_TYPE_PM;
		}
		else {
			$display_period = $i_DayTypeAM;
			$LeaveType = CARD_LEAVE_AM;
			$DayType = PROFILE_DAY_TYPE_AM;
		}
		break;
}

# order information
$default_order_by = " a.ClassName, a.ClassNumber+0, a.EnglishName";
$order_by = $default_order_by ;


$confirmNameField = getNameFieldByLang2("u.");
# Get Confirmation status
$sql = "SELECT c.RecordID, c.EarlyConfirmed, c.EarlyConfirmTime, $confirmNameField as ConfirmUser FROM CARD_STUDENT_DAILY_DATA_CONFIRM as c 
			LEFT JOIN INTRANET_USER as u ON u.UserID=c.EarlyConfirmUserID 
               WHERE c.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' AND c.RecordType = '".$lc->Get_Safe_Sql_Query($DayType)."'";
$temp = $lc->returnArray($sql,4);
list ($recordID, $confirmed , $confirmTime, $confirmUser) = $temp[0];


$ExpectedReasonField = $lc->Get_Expected_Reason($DayType,PROFILE_TYPE_EARLY,'c.');

### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

$LastModifyUserNameField = getNameFieldByLang2("u.");


$sql  = "SELECT 
					b.RecordID, 
					a.UserID,
	        ".getNameFieldWithClassNumberByLang("a.")."as name,
			a.Gender,
          b.LeaveSchoolTime,
          IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
          c.Reason,
          c.RecordStatus, 
		  c.DocumentStatus, 
          ".$ExpectedReasonField." ExpectedReason,
		  d.Remark,
		  c.OfficeRemark,
		  c.DateModified, 
		  $LastModifyUserNameField as LastModifyUser 
		FROM
			$card_log_table_name as b 
			LEFT JOIN INTRANET_USER as a 
			ON (b.UserID = a.UserID) 
			LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c 
			ON c.StudentID = a.UserID 
				AND c.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' 
				AND c.DayType = '".$lc->Get_Safe_Sql_Query($DayType)."' 
				AND c.RecordType = '".PROFILE_TYPE_EARLY."' 
			LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d 
			ON (a.UserID = d.StudentID AND d.RecordDate ='".$lc->Get_Safe_Sql_Query($TargetDate)."' AND d.DayType='".$lc->Get_Safe_Sql_Query($DayType)."') 
			LEFT JOIN INTRANET_USER as u ON u.UserID=c.ModifyBy
			LEFT JOIN YEAR_CLASS_USER as ycu ON (ycu.UserID = a.UserID)
			LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
    		INNER JOIN YEAR_CLASS_TEACHER yct ON (yc.YearClassID = yct.YearClassID and yct.UserID = ".$UserID.")
         WHERE 
         		b.DayNumber = '".$lc->Get_Safe_Sql_Query($txt_day)."' 
         		AND c.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' 
				AND c.DayType = '".$lc->Get_Safe_Sql_Query($DayType)."' 
				AND c.RecordType = '".PROFILE_TYPE_EARLY."' 
         		AND a.RecordType = 2 
         		AND a.RecordStatus IN (0,1,2)
         		AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
         ORDER BY 
         		$order_by
		";
//debug_r($sql);
$result = $lc->returnArray($sql,13);
$StudentIDArr = Get_Array_By_Key($result,'UserID');
//debug_pr($result);


$table_attend = "";

$words = $lword->getWordListAttendance(3);
$hasWord = sizeof($words)!=0;
$reason_js_array = "";

$colspan = 11;

$x = $linterface->CONVERT_TO_JS_ARRAY($words, "jArrayWords", 1, 1);

# Teacher Remark Preset
$teacher_remark_js = "";
$teacher_remark_words = $lc->GetTeacherRemarkPresetRecords(array());
$hasTeacherRemarkWord = sizeof($teacher_remark_words)!=0;
if($hasTeacherRemarkWord)
{
	$teacher_remark_words = Get_Array_By_Key($teacher_remark_words, 'ReasonText');
	foreach($teacher_remark_words as $key=>$word)
		$teacher_remark_words_temp[$key]= htmlspecialchars($word);
	$x .= $linterface->CONVERT_TO_JS_ARRAY($teacher_remark_words_temp, "jArrayWordsTeacherRemark", 1);
}


$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$x .= "<tr class=\"tabletop\">";
$x .= "<td width=\"2%\" class=\"tabletoplink\">#</td>";
$x .= "<td width=\"10%\" class=\"tabletoplink\">$i_UserName</td>";
$x .= "<td width=\"5%\" class=\"tabletoplink\">".$Lang['StudentAttendance']['Gender']."</td>";
$x .= "<td width=\"5%\" class=\"tabletoplink\">$i_StudentAttendance_LeaveSchoolTime</td>";
$x .= "<td width=\"5%\" class=\"tabletoplink\">$i_StudentAttendance_Field_CardStation</td>";
$x .= "<td width=\"5%\" class=\"tabletoplink\">$i_SmartCard_Frontend_Take_Attendance_Status
					<br>
					<select id=\"drop_down_status_all\" name=\"drop_down_status_all\">
		        		<option value=\"0\">".$Lang['StudentAttendance']['Present']."</option>
       					<option value=\"3\" SELECTED>".$Lang['StudentAttendance']['EarlyLeave']."</option>
	        		</select>
					<br>
					".$linterface->Get_Apply_All_Icon("javascript:SetAllAttend();")."
				</td>";
$x .= "<td width=\"5%\" class=\"tabletoplink\" align=\"center\">$i_SmartCard_Frontend_Take_Attendance_Waived<br /><input type=\"checkbox\" name=\"all_wavied\" onClick=\"(this.checked)?setAllWaived(document.form1,1):setAllWaived(document.form1,0)\"></td>";
$x .= "<td class=\"tabletoplink\" width=\"5%\" align=\"center\">".$Lang['StudentAttendance']['ProveDocument']."<br /><input type=\"checkbox\" name=\"master_document_status\" value=\"1\" onclick=\"SetAllDocumentStatus(this.checked);\"></td>";
$x .= "<td width=\"10%\" class=\"tabletoplink\">
				$i_Attendance_Reason
				<br>
				<input class=\"textboxnum\" type=\"text\" name=\"SetAllReason\" id=\"SetAllReason\" maxlength=\"255\" value=\"\">
				".$linterface->GET_PRESET_LIST("jArrayWords", "SetAllReasonIcon", "SetAllReason")."
				<br>
				".$linterface->Get_Apply_All_Icon("javascript:SetAllReason();")."
			</td>";
$x .= "<td width=\"10%\" class=\"tabletoplink\">".$Lang['StudentAttendance']['iSmartCardRemark']."</td>";



$x .= "<td width=\"10%\" class=\"tabletoplink\">".$Lang['StudentAttendance']['LastModify']."</td>";
$x .= "</tr>\n";

$disable_color="#DFDFDF";  # background color for disabled text field
$enable_color="#FFFFFF";  # background color for enabled text field

$select_word = "";

$gender_word['M'] = $Lang['General']['Male'];
$gender_word['F'] = $Lang['General']['Female'];

for($i=0; $i<sizeOf($result); $i++)
{
	list($record_id, $user_id, $name, $gender, $leave_time, $leave_station, $reason, $record_status, $document_status, $ExpectedReason, $remark, $office_remark, $date_modified, $last_modify_user) = $result[$i];
	$css=($i%2==0)?"tablerow1":"tablerow2";
	$css = (trim($record_status) == "")? $css:"attendance_early";
	$select_status = "<select id=\"drop_down_status[$i]\" name=\"drop_down_status[]\" onChange=\"setReasonComp(this.value, this.form.reason$i, this.form.editAllowed$i,$i)\">\n";
	$select_status .= "<option value=\"0\">".$Lang['StudentAttendance']['Present']."</option>\n";
	$select_status .= "<option value=\"3\" SELECTED>".$Lang['StudentAttendance']['EarlyLeave']."</option>\n";
	$select_status .= "</select>\n";
	$select_status .= "<input name=\"record_id[$i]\" type=\"hidden\" value=\"$record_id\">\n";
	$select_status .= "<input name=\"user_id[$i]\" type=\"hidden\" value=\"$user_id\">\n";

	$reason_comp = "<input type=\"text\" name=\"reason$i\" id=\"reason$i\" class=\"textboxnum\" maxlength=\"255\" value=\"".htmlspecialchars($ExpectedReason,ENT_QUOTES)."\" style=\"background:$enable_color\">";

	$waived_option = "<input type=\"checkbox\" name=\"waived_{$user_id}\"".(($record_status == 1) ? " CHECKED" : " ") .">";

	$x .= "<tr class=\"$css\"><td class=\"tabletext\">".($i+1)."</td>";
	$x .= "<td class=\"tabletext\">$name</td>";
	$x .= "<td class=\"tabletext\">".$gender_word[$gender]."</td>";
	$x .= "<td class=\"tabletext\">$leave_time</td>";
	$x .= "<td class=\"tabletext\">$leave_station</td>";
	$x .= "<td class=\"tabletext\">$select_status</td>";
	$x .= "<td class=\"tabletext\" align=\"center\">$waived_option</td>";
	$x .= "<td class=\"tabletext\" align=\"center\">".$linterface->Get_Checkbox("DocumentStatus_".$user_id, "DocumentStatus_".$user_id, "1",$document_status == '1', 'ClassDocumentStatus', '')."</td>";
	$x .= "<td class=\"tabletext\">$reason_comp";
	$x .= $linterface->GET_PRESET_LIST("jArrayWords", $i, "reason$i");
	$x .= "</td>";

	$remark_input = "<input type=\"text\" name=\"remark[]\" id=\"remark$i\" maxlength=\"255\" value=\"".htmlspecialchars($remark, ENT_QUOTES)."\" />";
	if($hasTeacherRemarkWord)
	{
		$remark_input .= $linterface->GET_PRESET_LIST("getTeacherRemarkReasons($i)", '_teacher_remark_'.$i, 'remark'.$i);
	}

	$x .= "<td class=\"tabletext\">";
	$x .= $remark_input;
	$x .= "</td>";


	$x .= "<td class=\"tabletext\">";
	if($date_modified != '' && $last_modify_user!=''){
		$x .= $last_modify_user.$Lang['General']['On'].$date_modified;
	}else{
		$x .= Get_String_Display('');
	}
	$x .= "</td>";
	$x .= "</tr>\n";
}
if (sizeof($result)==0)
{
	$x .= "<tr class=\"tablerow2\"><td class=\"tabletext\" height=\"40\" colspan=\"$colspan\" align=\"center\">$i_StudentAttendance_NoEarlyStudents</td></tr>\n";
}

$x .= "</table>\n";
$table_attend = $x;



$slot_select = "<SELECT name=\"DayType\">\n";
if ($lcardattend->attendance_mode != 1)
{
	$slot_select .= "<OPTION value=\"2\" ".($DayType=="2"?"SELECTED":"").">$i_DayTypeAM</OPTION>\n";
}
if ($lcardattend->attendance_mode != 0)
{
	$slot_select .= "<OPTION value=\"3\" ".($DayType=="3"?"SELECTED":"").">$i_DayTypePM</OPTION>\n";
}
$slot_select .= "</SELECT>\n";

$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/smartcard/icon_takeattendance.gif' align='absmiddle' />";
$TitleTitle1 = "<span class='contenttitle'>".$TitleImage1. $i_SmartCard_DailyOperation_ViewEarlyLeaveStatus ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
$TAGS_OBJ[] = array($TitleTitle, "", 0);
$linterface->LAYOUT_START(urldecode($Msg));

$PAGE_NAVIGATION[] = array("$TargetDate ($display_period)");

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
    <script language="Javascript" src='/templates/tooltip.js'></script>
    <style type="text/css">
        #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
        #ToolMenu2{position:absolute; top: 0px; left: 0px; z-index:5; visibility:show;  width: 450px;}
    </style>

    <script language="JavaScript">
        isMenu = true;
    </script>
    <div id="ToolMenu" style="position:absolute; width=200; height=100; visibility:hidden"></div>
    <div id="ToolMenu2" style="position:absolute; width=0px; height=0px; visibility:hidden"></div>

    <script language="JavaScript" type="text/javascript">

        function getTeacherRemarkReasons()
        {
            return jArrayWordsTeacherRemark;
        }

        function Check_View_Form() {
            if ($('#DPWL-TargetDate').html() == '') {
                document.getElementById("form2").submit();
            }
        }
        function setAllWaived(formObj,val){
            if(formObj==null) return;
            for(i=0;i<formObj.elements.length;i++){
                obj = formObj.elements[i];
                if(typeof(obj)!="undefined"){
                    if(obj.name.indexOf("waived_")>-1){
                        obj.checked = val==1?true:false;
                    }
                }
            }
        }
        function SetAllDocumentStatus(checked)
        {
            $('input.ClassDocumentStatus').attr('checked',checked);
        }
        function SetAllAttend() {
            var AttendValue = document.getElementById("drop_down_status_all").value;
            var AttendSelection = document.getElementsByName("drop_down_status[]").length;
            for (var i=0; i< AttendSelection; i++) {
                document.getElementById('drop_down_status['+i+']').value = AttendValue;

                var targetReasonEle = document.getElementById('reason'+i);
                var targetEditEle = document.getElementsByName('editAllowed'+i)[0];

                setReasonComp(AttendValue, targetReasonEle, targetEditEle, i);
            }
        }
        function SetAllReason() {
            var StatusSelection = document.getElementsByName("drop_down_status[]");
            var ApplyReason = document.getElementById("SetAllReason").value;
            for (var i=0; i< StatusSelection.length; i++) {
                if (StatusSelection[i].selectedIndex == 1) // early leave
                {
                    document.getElementById('reason'+i).value = ApplyReason;
                }
            }
        }
        function setReasonComp(value, txtComp, hiddenFlag, index)
        {
            if (value==3)
            {
                txtComp.disabled = false;
                txtComp.style.background='<?=$enable_color?>';
                hiddenFlag.value = 1;

                if($('#office_remark'+index).length>0){
                    $('#office_remark'+index).attr('disabled','');
                }
            }
            else
            {
                txtComp.disabled = true;
                txtComp.value="";
                txtComp.style.background='<?=$disable_color?>';
                hiddenFlag.value = 0;

                if($('#office_remark'+index).length>0){
                    $('#office_remark'+index).attr('disabled','disabled');
                }
            }
        }
    </script>
    <br />
    <form name="form2" id="form2" method="post" action="early_leave.php" >
    <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
        </tr>
        <tr>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
                    <tr>
                        <td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%">
							<?=$i_StudentAttendance_Field_Date?>
                        </td>
                        <td class="tabletext" width="70%">
							<?
							if ($lcardattend->DisableISmartCardPastDate && $lcardattend->CannotTakeFutureDateRecord) {
								?>
								<?=date('Y-m-d')?> <input type="hidden" name="TargetDate" id="TargetDate" value="<?=date('Y-m-d')?>">
								<?
							}
							else {
							    echo $linterface->GET_DATE_PICKER("TargetDate", $TargetDate,'',"","","","",'');
							}
							?>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_BookingTimeSlots?> </span></td>
                        <td valign="top" class="tabletext"><?=$slot_select?></td>
                    </tr>

					<?
					if($confirmed==1)
					{
						?>
                        <tr>
                            <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_Field_LastConfirmedTime?>:</td>
                            <td class="tabletext" width="70%"><?=$confirmTime.($confirmUser!=''?'&nbsp;('.$confirmUser.')':'')?></td>
                        </tr>
						<?
					}
					else
					{
						?>
                        <tr>
                            <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_Field_LastConfirmedTime?>:</td>
                            <td class="tabletext" width="70%"><?=$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record?></td>
                        </tr>
						<?
					}
					?>
                    <tr>
                        <td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                                <tr>
                                    <td align="center">
										<?= $linterface->GET_ACTION_BTN($button_view, "button", "Check_View_Form();","submit1"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>

    <form name="form1" id="form1" method="post" action="show_update.php" >
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center" onMouseMove="overhere()">
                        <tr>
                            <td align="right"><?= $SysMsg ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <td valign="bottom" align="left">
                                <table border=0>
                                    <tr>
                                        <td class="attendance_early" width="15px">
                                            &nbsp;
                                        </td>
                                        <td class="tabletext">
											<?=$Lang['StudentAttendance']['Confirmed']?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="bottom" align="right">

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
					<?=$table_attend?>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
                        <tr>
                            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
                        <tr>
                            <td align="center" colspan="2">
								<? if (sizeof($result)!=0) { ?>
									<?= $linterface->GET_ACTION_BTN($button_save, "button", "AlertPost(document.form1,'early_leave_update.php','$i_SmartCard_Confirm_Update_Attend?')") ?>
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
								<? } ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <input id="DayType" name="DayType" type="hidden" value="<?=escape_double_quotes($DayType)?>">
        <input name="TargetDate" type="hidden" value="<?=escape_double_quotes($TargetDate)?>">
        <input name=PageLoadTime type=hidden value="<?=time()+1?>">
		<? for ($i=0; $i<sizeof($result); $i++) { ?>
            <input type="hidden" name="editAllowed<?=$i?>" value="1">
		<? } ?>
        <input type="hidden" name="this_page_title" value="<?=$i_SmartCard_DailyOperation_ViewEarlyLeaveStatus?>">
        <input type="hidden" name="this_page_nav" value="PageDailyOperation_ViewEarlyLeaveStatus">
        <input type="hidden" name="this_page" value="early/show.php?TargetDate=<?=urlencode($TargetDate)?>&DayType=<?=urlencode($DayType)?>">
        <input type="hidden" name="TemplateCode" value="STUDENT_ATTEND_EARLYLEAVE">
    </form>
    <br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>