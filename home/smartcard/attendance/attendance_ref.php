<?
// using: 
############################################# Change Log ##############################################
# 2010-05-06 by Carlos: Add Late Session Statistic and Late session reason statistic
# 2010-04-26 by Carlos : Synced with Class Monthly Report
#######################################################################################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
intranet_opendb();

$lcard = new libcardstudentattend2();
$MODULE_OBJ['title'] = $i_StudentAttendance_ViewPastRecords;
$linterface = new interface_html("popup.html");
$StudAttendUI = new libstudentattendance_ui();
$lcard->retrieveSettings();

$group_id = IntegerSafe($group_id);
if ($group_id != "")
{
	// Get group name / title
	$sql = "select title from INTRANET_GROUP where groupid = '".$group_id."'";
	$resultSet = $lcard->returnArray($sql,1);
	$groupName = $resultSet[0][0];
}

if ($Year == "")
{
    $Year = date('Y');
}
if ($Month == "")
{
    $Month = date('m');
}
if (strlen($Month)==1)
{
    $Month = "0".$Month;
}
$ts_record = mktime(0,0,0,$Month,1,$Year);

### month selection
$month_select = "<select name='Month' onChange='submit();'>";
for($m = 1; $m<=12; $m++) {
	$mm = ($m<10?"0":"").$m;
	$month_select .= "<option value='".$mm."' ". ($mm==$Month?"selected":"") .">". $i_general_MonthShortForm[$m-1] ."</option>";
}
$month_select .= "</select>";

### year selection
$m = date("n",$ts_record);
if($m>=9){
	$year_list = array(date('Y',$ts_record),date('Y',$ts_record)+1);
}else{
	$year_list = array(date('Y',$ts_record)-1,date('Y',$ts_record));
}
$year_select = "<select name='Year' onChange='submit();'>";
for($i=0;$i<sizeof($year_list);$i++){
	$y = $year_list[$i];
	$year_select .= "<option value='$y' ". ($y==$Year?"selected":"") .">". $y ."</option>";
}
$year_select .= "</select>";

$my_select = $month_select . " " . $year_select;

$content = "<form name=\"form1\" method=\"get\">";
if ($ClassName != "") {
	$content .= "<input name=\"ClassName\" type=\"hidden\" value=\"".$ClassName."\">";
} else {
	$content .= "<input name=\"group_id\" type=\"hidden\" value=\"".$group_id."\">";
}
$content .= "<br/>";
$content .= "</form>";

$Columns['ChineseName'] = "1";
$Columns['EnglishName'] = "1";
$Columns['Gender'] = "1";
$Columns['Data'] = "1";
$Columns['DailyStat'] = "1";
$Columns['Schooldays'] = "1";
$Columns['MonthlyStat'] = "1";
$Columns['Remark'] = "1";
$Columns['ReasonStat'] = "1";
if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
{
	$Columns['Session'] = "1";
}

# Get Report Content
$content .= $StudAttendUI->Get_Class_Monthly_Attendance_Report(array(array('ClassID'=>$group_id,'ClassName'=>$groupName)),$Year,$Month,$Columns,"",true);

$linterface->LAYOUT_START();
?>
<style type="text/css">
#contentTable1, #contentTable2 {
	border:1px solid black;
}

#contentTable1 td, #contentTable2 td {
	border:1px solid black;
}

#contentTable3 td, #contentTable4 td {
	border:0px solid black;
}
</style>
<?php
echo "<br />";
echo $content;
$linterface->LAYOUT_STOP();
intranet_closedb();
?>