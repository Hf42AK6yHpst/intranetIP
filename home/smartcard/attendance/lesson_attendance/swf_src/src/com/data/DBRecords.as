package com.data
{
	public class DBRecords
	{
				
		public function DBRecords(){}
		
		// ****************
		// Getter methods
		// ****************
		static public function get_lesson_saved_plan():Array{
			var planLayoutAMF:String;
			var studentPositionAMF:String;
			
			var array:Array = new Array();
			planLayoutAMF = "<xml><style>rigid</style><rowNum>3</rowNum><columeNum>4</columeNum></xml>||<xml><row id='0'><colume id='0'><element type='Seat'></element></colume><colume id='1'><element type='Seat'></element></colume><colume id='2'><element type='Seat'></element></colume><colume id='3'><element type='Table' content='row=0,col=3'></element></colume></row><row id='1'><colume id='0'><element type='Seat'></element></colume><colume id='1'><element type='Table' content='row=1,col=1'></element></colume><colume id='2'><element type='Seat'></element></colume><colume id='3'><element type='Seat'></element></colume></row><row id='2'><colume id='0'><element type='Seat'></element></colume><colume id='1'><element type='Empty'></element></colume><colume id='2'><element type='Seat'></element></colume><colume id='3'><element type='Seat'></element></colume></row></xml>";	
			studentPositionAMF = "<xml>";
			studentPositionAMF += "<seatPlan id='0'>";
			studentPositionAMF += "<position seatPlanId='0' studentIndex='0' row='0' colume='0' xPos='-999' yPos='-999'/>";
			studentPositionAMF += "<position seatPlanId='0' studentIndex='1' row='-1' colume='-1' xPos='-999' yPos='-999'/>";
			studentPositionAMF += "<position seatPlanId='0' studentIndex='2' row='1' colume='3' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='0' studentIndex='3' row='-1' colume='-1' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='0' studentIndex='4' row='-1' colume='-1' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='0' studentIndex='5' row='-1' colume='-1' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='0' studentIndex='6' row='1' colume='2' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='0' studentIndex='7' row='-1' colume='-1' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='0' studentIndex='8' row='2' colume='0' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='0' studentIndex='9' row='0' colume='2' xPos='-999' yPos='-999' />";
			studentPositionAMF += "</seatPlan>";
			studentPositionAMF += "</xml>";
			array.push("0","plan1",studentPositionAMF,planLayoutAMF);
			return array;
		}
		
		static public function get_subject_group_default_plan():Array{
			var planLayoutAMF:String;
			var studentPositionAMF:String;
			
			var array1:Array = new Array();
			planLayoutAMF = "<xml><style>rigid</style><rowNum>3</rowNum><columeNum>4</columeNum></xml>||<xml><row id='0'><colume id='0'><element type='Seat'></element></colume><colume id='1'><element type='Seat'></element></colume><colume id='2'><element type='Seat'></element></colume><colume id='3'><element type='Table' content='row=0,col=3'></element></colume></row><row id='1'><colume id='0'><element type='Seat'></element></colume><colume id='1'><element type='Table' content='row=1,col=1'></element></colume><colume id='2'><element type='Seat'></element></colume><colume id='3'><element type='Seat'></element></colume></row><row id='2'><colume id='0'><element type='Seat'></element></colume><colume id='1'><element type='Empty'></element></colume><colume id='2'><element type='Seat'></element></colume><colume id='3'><element type='Seat'></element></colume></row></xml>";
			studentPositionAMF = "<xml>";
			studentPositionAMF += "<seatPlan id='6'>";
			studentPositionAMF += "<position seatPlanId='6' studentIndex='0' row='0' colume='0' xPos='-999' yPos='-999'/>";
			studentPositionAMF += "<position seatPlanId='6' studentIndex='1' row='-1' colume='-1' xPos='-999' yPos='-999'/>";
			studentPositionAMF += "<position seatPlanId='6' studentIndex='2' row='1' colume='3' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='6' studentIndex='3' row='-1' colume='-1' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='6' studentIndex='4' row='-1' colume='-1' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='6' studentIndex='5' row='-1' colume='-1' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='6' studentIndex='6' row='1' colume='2' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='6' studentIndex='7' row='-1' colume='-1' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='6' studentIndex='8' row='2' colume='0' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='6' studentIndex='9' row='0' colume='2' xPos='-999' yPos='-999' />";
			studentPositionAMF += "</seatPlan>";
			studentPositionAMF += "</xml>";
			array1.push("6","plan1",studentPositionAMF,planLayoutAMF);
			
			var array2:Array = new Array();
			planLayoutAMF = "<xml><style>rigid</style><rowNum>3</rowNum><columeNum>4</columeNum></xml>||<xml><row id='0'><colume id='0'><element type='Seat'></element></colume><colume id='1'><element type='Seat'></element></colume><colume id='2'><element type='Seat'></element></colume><colume id='3'><element type='Table' content='row=0,col=3'></element></colume></row><row id='1'><colume id='0'><element type='Seat'></element></colume><colume id='1'><element type='Table' content='row=1,col=1'></element></colume><colume id='2'><element type='Seat'></element></colume><colume id='3'><element type='Seat'></element></colume></row><row id='2'><colume id='0'><element type='Seat'></element></colume><colume id='1'><element type='Empty'></element></colume><colume id='2'><element type='Seat'></element></colume><colume id='3'><element type='Seat'></element></colume></row></xml>";
			studentPositionAMF = "<xml>";
			studentPositionAMF += "<seatPlan id='10'>";
			studentPositionAMF += "<position seatPlanId='10' studentIndex='0' row='0' colume='0' xPos='-999' yPos='-999'/>";
			studentPositionAMF += "<position seatPlanId='10' studentIndex='1' row='-1' colume='-1' xPos='-999' yPos='-999'/>";
			studentPositionAMF += "<position seatPlanId='10' studentIndex='2' row='1' colume='3' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='10' studentIndex='3' row='-1' colume='-1' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='10' studentIndex='4' row='-1' colume='-1' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='10' studentIndex='5' row='-1' colume='-1' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='10' studentIndex='6' row='1' colume='2' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='10' studentIndex='7' row='-1' colume='-1' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='10' studentIndex='8' row='2' colume='0' xPos='-999' yPos='-999' />";
			studentPositionAMF += "<position seatPlanId='10' studentIndex='9' row='0' colume='1' xPos='-999' yPos='-999' />";
			studentPositionAMF += "</seatPlan>";
			studentPositionAMF += "</xml>";
			array2.push("10","plan2",studentPositionAMF,planLayoutAMF);
			
			var returnArray:Array = new Array();
			returnArray.push(array1,array2);
			return returnArray;
		}
		
		static public function get_subject_group_student_list():Array{
			var studentRecord:Array = new Array();
			var a1:Array = ["0","Mary","郭美莉","2","5C","0",""];
			var a2:Array = ["1","Susan","朱小芯","3","5A","0",""];
			var a3:Array = ["2","Sam","陳第森","11","5A","0",""];
			var a4:Array = ["3","May","劉喜好","12","5A","0",""];
			var a5:Array = ["4","June","陳倩恆","8","5A","0",""];
			var a6:Array = ["5","Tom","黃大文","0","5A","0",""];
			var a7:Array = ["6","Winnie","葉彩玲","7","5A","0",""];
			var a8:Array = ["7","John","王小明","15","5A","0",""];
			var a9:Array = ["8","Jackie","張學生","16","5C","0",""];
			var a10:Array = ["9","Andy","劉華","17","5A","0",""];
			studentRecord.push(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10);
			return studentRecord;
		}

		static public function get_lesson_attendance_record():Array{
			var studentRecord:Array = new Array();
			var a1:Array = ["0","Mary","郭美莉","2","5C","0","hello"];
			var a2:Array = ["1","Susan","朱小芯","3","5A","1",""];
			var a3:Array = ["2","Sam","陳第森","11","5A","0",""];
			var a4:Array = ["3","May","劉喜好","12","5A","0",""];
			var a5:Array = ["4","June","陳倩恆","8","5A","3",""];
			var a6:Array = ["5","Tom","黃大文","0","5A","2",""];
			var a7:Array = ["6","Winnie","葉彩玲","7","5A","2",""];
			var a8:Array = ["7","John","王小明","15","5A","1",""];
			var a9:Array = ["8","Jackie","張學生","16","5C","0","hi"];
			var a10:Array = ["9","Andy","劉華","17","5A","1",""];
			studentRecord.push(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10);
			return studentRecord;	
		}
		
		// *************************
		// Setter / Saving method
		// *************************
		
		static public function save_default_plan(planDetailsArray:Array):void{
			
		}
		
		static public function save_lesson_plab(planID:String, planName:String, studentPositionAMF:String, planLayoutAMF:String):void{
			
		}
		
		static public function save_lesson_attendance_data(studentAttendArray:Array):void{
			
		}
		
		// ********************
		// Checking functions
		// ********************
		static public function check_record_exist_subject_group_attend_overview():String{
			var str:String = "";
			return str;
		}
		
		static public function check_record_exist_subject_group_attend_default_plan():String{
			return "true";
		}

	}
}