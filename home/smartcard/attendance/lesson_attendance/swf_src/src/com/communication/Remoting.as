﻿package com.communication {
	
	import com.hexagonstar.util.debug.Debug;
	
	import flash.events.*;
	import flash.net.NetConnection;
	import flash.net.ObjectEncoding;
	import flash.net.Responder;
	
	public class Remoting {
		
		private var _gateway:String;
		private var _connection:NetConnection;
		private var _responderTest:Responder;
			
		private var _Check_Record_Exist_Subject_Group_Attend_Overview_Respond:Responder;
		private var _Check_Record_Exist_Subject_Group_Attend_Default_Plan_Respond:Responder;
		private var _Get_Subject_Group_Default_Plan_Respond:Responder;
		private var _Get_Subject_Group_Student_List_Respond:Responder;
		private var _Get_Lesson_Saved_Plan_Respond:Responder;
		private var _Get_Lesson_Attendance_Record_Respond:Responder;
		private var _Save_Default_Plan_Respond:Responder;
		private var _Save_Lesson_Plan_Respond:Responder;
		private var _Save_Lesson_Attendance_Data_Respond:Responder;
			
		private var _fv:Object;
		
		public var Check_Record_Exist_Subject_Group_Attend_Overview_Result:Object = null;
		public var Check_Record_Exist_Subject_Group_Attend_Default_Plan_Result:String = null;
		public var Get_Subject_Group_Default_Plan_Result:Object = null;
		public var Get_Subject_Group_Student_List_Result:Object = null;
		public var Get_Lesson_Saved_Plan_Result:Object = null;
		public var Get_Lesson_Attendance_Record_Result:Object = null;
		public var Save_Default_Plan_Result:Object = null;
		public var Save_Lesson_Plan_Result:Object = null;
		public var Save_Lesson_Attendance_Data_Result:Object = null;
		
		
		public var AttendOverviewID:String = null;
		public var org_AttendOverviewID:String = null;
		public var StudentRecord:Array = new Array();
		public var SeatPlanRecord:Array = new Array();
		
		public function Remoting(gwPath:String,fv:Object){
			
			Debug.trace("----------------------start remoting----------------------");
			
			_fv = fv;
			_gateway = gwPath;
			
			// ***********************************************
			// Responder to handle data returned from AMFPHP.
			// ***********************************************
			//_responderTest = new Responder(onResultTest, onFault);
			_Check_Record_Exist_Subject_Group_Attend_Overview_Respond = new Responder(on_Check_Record_Exist_Subject_Group_Attend_Overview_Respond, onFault);
			_Check_Record_Exist_Subject_Group_Attend_Default_Plan_Respond = new Responder(on_Check_Record_Exist_Subject_Group_Attend_Default_Plan_Respond, onFault);
			_Get_Subject_Group_Default_Plan_Respond = new Responder(on_Get_Subject_Group_Default_Plan_Respond,onFault);
			_Get_Subject_Group_Student_List_Respond = new Responder(on_Get_Subject_Group_Student_List_Respond,onFault);
			_Get_Lesson_Saved_Plan_Respond = new Responder(on_Get_Lesson_Saved_Plan_Respond,onFault);
			_Get_Lesson_Attendance_Record_Respond = new Responder(on_Get_Lesson_Attendance_Record_Respond,onFault);
			_Save_Default_Plan_Respond = new Responder(on_Save_Default_Plan_Respond,onFault);
			_Save_Lesson_Plan_Respond = new Responder(on_Save_Lesson_Plan_Respond,onFault);
			_Save_Lesson_Attendance_Data_Respond = new Responder(on_Save_Lesson_Attendance_Data_Respond,onFault);
		
			// ************
			// connection
			// ************
			_connection = new NetConnection();
			_connection.objectEncoding = ObjectEncoding.AMF0;
			_connection.connect(_gateway);
			
			_connection.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
			_connection.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
		}
		
		private function netStatusHandler(evt:NetStatusEvent):void {
			Debug.trace("NetStatusEvent: "+evt.info.code);
        }
		
		private function ioErrorHandler(evt:IOErrorEvent):void {
			Debug.trace("IOErrorEvent: "+evt);
        }		
		
		// **********************************************************************************************************
		// ****************************************** Checking function *********************************************
		// **********************************************************************************************************
		
		public function Check_Record_Exist_Subject_Group_Attend_Overview():void{
			_connection.call("liblessonattendance.Check_Record_Exist_Subject_Group_Attend_Overview", 
			_Check_Record_Exist_Subject_Group_Attend_Overview_Respond, 
			_fv.SubjectGroupID, _fv.LessonDate, _fv.RoomAllocationID);
			/*Debug.trace("Inside Check_Record_Exist_Subject_Group_Attend_Overview");
			Debug.trace("_fv.SubjectGroupID: " + _fv.SubjectGroupID);
			Debug.trace("_fv.LessonDate: " + _fv.LessonDate);
			Debug.trace("_fv.RoomAllocationID: " + _fv.RoomAllocationID);*/
		}
		
		public function Check_Record_Exist_Subject_Group_Attend_Default_Plan():void{
			_connection.call("liblessonattendance.Check_Record_Exist_Subject_Group_Attend_Default_Plan", 
			_Check_Record_Exist_Subject_Group_Attend_Default_Plan_Respond, 
			_fv.SubjectGroupID);
		}
		
		// **********************************************************************************************************
		// ****************************************** Getting function **********************************************
		// **********************************************************************************************************
		
		public function Get_Subject_Group_Default_Plan():void{
			_connection.call("liblessonattendance.Get_Subject_Group_Default_Plan", _Get_Subject_Group_Default_Plan_Respond, 
			_fv.SubjectGroupID);
		}
		
		public function Get_Subject_Group_Student_List():void{
			_connection.call("liblessonattendance.Get_Subject_Group_Student_List", _Get_Subject_Group_Student_List_Respond, 
			_fv.SubjectGroupID);
		}
		
		public function Get_Lesson_Saved_Plan():void{
			_connection.call("liblessonattendance.Get_Lesson_Saved_Plan", _Get_Lesson_Saved_Plan_Respond, 
			_fv.SubjectGroupID, _fv.LessonDate, _fv.RoomAllocationID);
		}
		
		public function Get_Lesson_Attendance_Record():void{
			_connection.call("liblessonattendance.Get_Lesson_Attendance_Record", _Get_Lesson_Attendance_Record_Respond, 
			AttendOverviewID);
		}
		
		// **********************************************************************************************************
		// ****************************************** Saving function ***********************************************
		// **********************************************************************************************************
		
		public function Save_Default_Plan(PlanDetailArray:Array):void{
			Debug.trace("");
			Debug.trace("----------------------Outputting DEFAULT Seat Plan Record----------------------");
			for (var i:int=0; i<PlanDetailArray.length; i++){
				Debug.trace(i + " : " + PlanDetailArray[i]);
			}
			_connection.call("liblessonattendance.Save_Default_Plan", _Save_Default_Plan_Respond, 
			_fv.SubjectGroupID,PlanDetailArray);
		}
		
		public function Save_Lesson_Plan(PlanDetailArray:Array):void{
			Debug.trace("");
			Debug.trace("----------------------Outputting LESSON Seat Plan Record----------------------");
			/*Debug.trace(_fv.SubjectGroupID);
			Debug.trace(_fv.LessonDate);
			Debug.trace(_fv.RoomAllocationID);
			Debug.trace(_fv.StartTime);
			Debug.trace(_fv.EndTime);*/
			Debug.trace(PlanDetailArray);
			_connection.call("liblessonattendance.Save_Lesson_Plan", _Save_Lesson_Plan_Respond, 
			_fv.SubjectGroupID, _fv.LessonDate, _fv.RoomAllocationID, _fv.StartTime, _fv.EndTime,
			PlanDetailArray[0], PlanDetailArray[1], PlanDetailArray[2], PlanDetailArray[3]);
		}
		
		public function Save_Lesson_Attendance_Data(StudentAttendArray:Array):void{
			Debug.trace("");
			Debug.trace("----------------------Outputting Student Record----------------------");
			Debug.trace("AttendOverviewID: " + AttendOverviewID);
			for (var i:int=0; i<StudentAttendArray.length; i++){
				Debug.trace(i + " : " + StudentAttendArray[i]);
			}
			_connection.call("liblessonattendance.Save_Lesson_Attendance_Data", _Save_Lesson_Attendance_Data_Respond, 
			AttendOverviewID, StudentAttendArray);
		}
		
		// **********************************************************************************************************
		// ***************************************** RESULTS ********************************************************
		// **********************************************************************************************************
		
		private function on_Check_Record_Exist_Subject_Group_Attend_Overview_Respond(results:Object):void{
			Check_Record_Exist_Subject_Group_Attend_Overview_Result = results;
			AttendOverviewID = String(results);
			org_AttendOverviewID = String(results);
			//Debug.trace("Inside on_Check_Record_Exist_Subject_Group_Attend_Overview_Respond: " + String(results));
			//Debug.trace("AttendOverviewID: " + AttendOverviewID);
		}
		
		private function on_Check_Record_Exist_Subject_Group_Attend_Default_Plan_Respond(results:Object):void{
			//Debug.trace("Inside on_Check_Record_Exist_Subject_Group_Attend_Default_Plan_Respond: " + String(results));
			//Debug.trace("Typeof: " + typeof(results));
			Check_Record_Exist_Subject_Group_Attend_Default_Plan_Result = String(results);
		}

		private function on_Get_Subject_Group_Default_Plan_Respond(results:Object):void{
			Get_Subject_Group_Default_Plan_Result = String(results);
			// *******************************
			// Results should be a 2D array
			// *******************************
			for (var i:int=0; i<results.length; i++){
				var array:Array = new Array();
				var PlanID:String = String(results[i][0]);
			 	var PlanName:String = String(results[i][1]);
			 	var StudentPositionAMF:String = String(results[i][2]);
			 	var PlanLayoutAMF:String = String(results[i][3]);
			 	array.push(PlanID,PlanName,StudentPositionAMF,PlanLayoutAMF);
			 	SeatPlanRecord.push(array);
			}	
		}
		
		private function on_Get_Subject_Group_Student_List_Respond(results:Object):void{
			Get_Subject_Group_Student_List_Result = String(results);
			if (Get_Subject_Group_Student_List_Result != "-1"){
				//Debug.trace("Result.lengthL: " + results.length);
				for (var i:int=0; i<results.length; i++){
					var array:Array = new Array();
					var StudentID:String = String(results[i][0]);
					var EnglishName:String = String(results[i][1]);
					var ChineseName:String = String(results[i][2]);
					var ClassNumber:String = String(results[i][3]);
					var ClassName:String = String(results[i][4]);
					var AttendStatus:String = String(results[i][5]);
					var Remarks:String = String(results[i][6]);
					var PhotoPath:String; 
					if (results[i][7] == "") PhotoPath = "";//"data_load/assets/emptySeat.jpg";
					else PhotoPath = _fv.ServerPath + String(results[i][7]);
					//Debug.trace("Student " + i + ": " + array);
					array.push(StudentID,EnglishName,ChineseName,ClassNumber,ClassName,AttendStatus,Remarks,PhotoPath);
					StudentRecord.push(array);
				}
			}
		}
		
		private function on_Get_Lesson_Saved_Plan_Respond(results:Object):void{
			Get_Lesson_Saved_Plan_Result = String(results);
			//Debug.trace("Get_Lesson_Saved_Plan_Result: " + Get_Lesson_Saved_Plan_Result);
			// ****************************************************
			// Results should be a 1D array, but convert to 2D!!!
			// ****************************************************
			if (Get_Lesson_Saved_Plan_Result != "-1"){
				var array:Array = new Array();
				var PlanID:String = String(results[0]);
			 	var PlanName:String = String(results[1]);
			 	var StudentPositionAMF:String = String(results[2]);
			 	var PlanLayoutAMF:String = String(results[3]);
			 	/*Debug.trace("length: " + results.length);
			 	Debug.trace("planID: " + PlanID);
			 	Debug.trace("planName: " + PlanName);
			 	Debug.trace("studentposition: " + StudentPositionAMF);
			 	Debug.trace("planLayout: " + PlanLayoutAMF);*/
			 	array.push(PlanID,PlanName,StudentPositionAMF,PlanLayoutAMF);
			 	SeatPlanRecord.push(array);
			}
		}
		
		private function on_Get_Lesson_Attendance_Record_Respond(results:Object):void{
			Get_Lesson_Attendance_Record_Result = String(results);
			if (Get_Lesson_Attendance_Record_Result != "-1"){
				for (var i:int=0; i<results.length; i++){
					var array:Array = new Array();
					var StudentID:String = String(results[i][0]);
					var EnglishName:String = String(results[i][1]);
					var ChineseName:String = String(results[i][2]);
					var ClassNumber:String = String(results[i][3]);
					var ClassName:String = String(results[i][4]);
					var AttendStatus:String = String(results[i][5]);
					var Remarks:String = String(results[i][6]);
					var PhotoPath:String;
					if (results[i][7] == "") PhotoPath = "";//"data_load/assets/emptySeat.jpg";
					else PhotoPath = _fv.ServerPath + String(results[i][7]);
					array.push(StudentID,EnglishName,ChineseName,ClassNumber,ClassName,AttendStatus,Remarks,PhotoPath);
					StudentRecord.push(array); 
				}
			}
		}	
		
		private function on_Save_Default_Plan_Respond(results:Object):void{
			Save_Default_Plan_Result = String(results);
			Debug.trace("Save_Default_Plan_Result: " + Save_Default_Plan_Result);
		}
		
		private function on_Save_Lesson_Plan_Respond(results:Object):void{
			Save_Lesson_Plan_Result = String(results);
			AttendOverviewID = String(results);
			Debug.trace("Save_Lesson_Plan_Result: " + Save_Lesson_Plan_Result);
		}
		
		private function on_Save_Lesson_Attendance_Data_Respond(results:Object):void{
			Save_Lesson_Attendance_Data_Result = String(results);
			Debug.trace("Save_Lesson_Attendance_Data_Result: " + Save_Lesson_Attendance_Data_Result);
		}
		
		// *********************************************************************************
		// Handle an unsuccessfull AMF call. This is method is dedined by the responder. 
		// *********************************************************************************
		private function onFault(fault:Object):void {
			trace("Fault: " + String(fault.description));
		}
		
		/*public function sendData():void {
			
			// *************************************
			// Send the data to the remote server. 
			// *************************************
			//_connection.call("HelloWorld.say", _responderSave, params);
			var params:String = "adam is testing, result is good";
			_connection.call("liblessonattendance.Test_Connection", _responderTest, "no parameter");
			//_connection.call("liblessonattendance.Test_DB_Connection", _responderTest, 1);
		}
		
		private function onResultTest(result:Object):void {
			Debug.trace("successful: " + String(result));
		}*/
	}
}