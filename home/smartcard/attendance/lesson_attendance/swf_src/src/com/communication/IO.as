package com.communication
{
	
	import com.hexagonstar.util.debug.Debug;
	
	import com.elements.AbstractFloor;
	import com.elements.StudentList;
	
	import mx.collections.ArrayCollection;
	import mx.events.CollectionEvent;
	
	public class IO
	{
		private var mainApp:Object = new Object();
		public var studentRecord:ArrayCollection = new ArrayCollection();
		public var seatPlanRecord:ArrayCollection = new ArrayCollection();
		public var studentPositionRecord:ArrayCollection = new ArrayCollection();
		
		public function IO(app:Object):void{
			mainApp = app;
		}
		
		// ***************************************************************************************************************
		// Get students' data from database and convert to an ArrayCollection with student object (NOT DisplayObject)!
		// ***************************************************************************************************************						
		public function getStudentRecord(input:Array):void{
			
			var output:Array = new Array();
			//Debug.trace("Inside IO: ");
			for (var i:int=0; i<input.length; i++){
				var stud:Object = new Object();
				stud.index = String(input[i][0]);
				stud.myEngName = String(input[i][1]);
				stud.myName = String(input[i][2]);
				stud.classNo = int(input[i][3]);
				stud.myClassName = String(input[i][4]);
				stud.fullClassName = String(input[i][4] + "-" + input[i][3]);
				if (String(input[i][5]) == "0") stud.status = "出席";
				else if (String(input[i][5]) == "1") stud.status = "外出";
				else if (String(input[i][5]) == "2") stud.status = "遲到";
				else if (String(input[i][5]) == "3") stud.status = "缺席";
				stud.arrival_time = "";
				stud.sheight = "";
				stud.grade = "";
				stud.remarks = String(input[i][6]);
				stud.photoPath = String(input[i][7]);
				
				/*Debug.trace(i + ": " + stud.index + ", " + stud.myEngName + ", " + stud.myName + ", " + stud.classNo + ", " + 
				stud.myClassName + ", " + stud.fullClassName + ", " + stud.status + ", " + stud.remarks);*/
				 
				output.push(stud);
			}
			studentRecord.source = output;
			studentRecord.addEventListener(CollectionEvent.COLLECTION_CHANGE,onStudentRecordChange);
			studentRecord.source.sortOn("index",Array.NUMERIC);
			
			/*trace("STUDENT RECORD: ");
			for (var j:int=0; j<studentRecord.source.length; j++){
				trace("index: " + studentRecord[j].index + ", eng: " + studentRecord[j].myEngName +
				", chi_name: " + studentRecord[j].myName + ", class_no: " + studentRecord[j].classNo + 
				", myClassName: " + studentRecord[j].myClassName + ", status: " + studentRecord[j].status);
			}*/
		}
		
		// *************************************************************
		// Sychronize data in all floor when ArrayCollection change!!!
		// *************************************************************
		private function onStudentRecordChange(e:CollectionEvent):void{
			for (var i:int=0; i<mainApp.mainTabMenu.numChildren; i++){
				if (mainApp.mainTabMenu.getChildAt(i) is StudentList == false){
					mainApp.mainTabMenu.getChildAt(i).setAllAttendanceDisplay();
					mainApp.mainTabMenu.getChildAt(i).setAllRemarks();
				}
			}
			/*for (var j:int=0; j<studentRecord.source.length; j++){
				trace("Name: " + studentRecord[j].myName + ", status: " + studentRecord[j].status + ", remarks: " + studentRecord[j].remarks);
			}*/
		}
		
		// ******************************************************************************************
		// Get student position data in seat plan from database and convert to an ArrayCollection
		// ******************************************************************************************
		public function getStudentPositionRecord(xmlStr:String):Array{
			
			var xml:XML = new XML(xmlStr);
			var output:Array = new Array();
			var seatPlanId:String = xml.seatPlan.attribute("id");
			var studentNum:int = xml.seatPlan.position.length();
			for (var j:int=0; j<studentNum; j++){
				var studentPosition:Object = new Object();
				studentPosition.seatPlanId = String(seatPlanId);
				studentPosition.studentIndex = String(xml.seatPlan.position[j].attribute("studentIndex"));
				studentPosition.row = int(xml.seatPlan.position[j].attribute("row"));
				studentPosition.colume = int(xml.seatPlan.position[j].attribute("colume"));
				studentPosition.xPos = Number(xml.seatPlan.position[j].attribute("xPos"));
				studentPosition.yPos = Number(xml.seatPlan.position[j].attribute("yPos"));
				output.push(studentPosition);
			}
			return output;
		}
		
		// ********************************************************************
		// Get seat plan data from database and convert to an ArrayCollection
		// ********************************************************************
		public function get_SeatPlan_StudentPosition_Record(input:Array):void{
			
			var output:Array = new Array();
			
			for (var i:int=0; i<input.length; i++){
				var seatPlan:Object = new Object();
				seatPlan.seatPlanId = String(input[i][0]);
				seatPlan.tabName = String(input[i][1]);
			
				studentPositionRecord.source[i] = getStudentPositionRecord(String(input[i][2]));	
			
				var seatPlanStr:String = String(input[i][3]);
				var seatPlanHeaderStr:String = seatPlanStr.split("||")[0];
				var seatPlanLayoutStr:String = seatPlanStr.split("||")[1];
			
				var xml:XML = new XML(seatPlanHeaderStr);
				seatPlan.seatPlanType = String(xml.style);
				seatPlan.rowNum = int(xml.rowNum);
				seatPlan.columeNum = int(xml.columeNum);
				seatPlan.layoutStr = String(seatPlanLayoutStr);
				
				output.push(seatPlan);	
			}
			seatPlanRecord.source = output;
			
			/*trace("SEATPLAN RECORD: ");
			for (var j:int=0; j<seatPlanRecord.source.length; j++){
				trace("Id: " + seatPlanRecord.source[j].seatPlanId + ", name: " + seatPlanRecord.source[j].tabName +
				", type: " + seatPlanRecord.source[j].seatPlanType + ", rowNum: " + seatPlanRecord.source[j].rowNum + 
				", columeNum: " + seatPlanRecord.source[j].columeNum + ", layoutStr: " + seatPlanRecord.source[j].layoutStr);
			}
			trace("POSITION RECORD: ");
			for (var g:int=0; g<studentPositionRecord.source.length; g++){
				for (var k:int=0; k<studentPositionRecord.source[g].length; k++){
					trace("seatPlanId: " + studentPositionRecord.source[g][k].seatPlanId + ", studentIndex: " + studentPositionRecord.source[g][k].studentIndex + 
					", row: " + studentPositionRecord.source[g][k].row + ", col: " + studentPositionRecord[g][k].colume);
				}
			}*/
			
		}
		
	}
}