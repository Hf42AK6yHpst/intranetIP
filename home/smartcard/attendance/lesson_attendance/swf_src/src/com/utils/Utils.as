package com.utils
{
	import flash.net.registerClassAlias;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	
	import mx.utils.ObjectUtil;
	
	public class Utils
	{
		public function Utils(){}
		
		/*public static function copyOverObject(objToCopy:Object, registerAlias:Boolean = false):Object{
			if (registerAlias){
				var className:String = flash.utils.getQualifiedClassName(objToCopy);
				flash.net.registerClassAlias(className,(flash.utils.getDefinitionByName(className) as Class));
			}
			return mx.utils.ObjectUtil.copy(objToCopy);
		}
		
		public static function copyOverArray(arr:Array):Array{
			var newArray:Array = new Array();
			for (var i:int; i<arr.length; i++){
				newArray.push(copyOverObject(arr[i],true));
			}
			return newArray;
		}*/
	
		static public function shuffle(array:Array,startIndex:int=0,endIndex:int=0):Array {
			if (endIndex==0) {
				endIndex=array.length - 1;
			}
			for (var i:int=endIndex; i>startIndex; i--) {
				var randomNumber:int=Math.floor(Math.random()*endIndex)+startIndex;
				var tmp:* = array[i];
				array[i] = array[randomNumber];
				array[randomNumber] = tmp;
			}
			return array;
		}	
		
	}
}