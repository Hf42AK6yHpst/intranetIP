<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$lsmartcard	= new libsmartcard();
$CurrentPage	= "PageReminderRecord";

if(!$plugin['attendancestudent'])
{
	header("Location: $intranet_httppath/");
        exit();
}

$ladminjob = new libadminjob($UserID);
if (!$ladminjob->isSmartAttendenceAdmin())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;

if (isset($UserID) && $UserID != "")
{
    $conds = "AND a.TeacherID= '".IntegerSafe($UserID)."'";
}
$user_field = getNameFieldWithClassNumberByLang("b.");

if ($filter == 1)
{
    $conds .= " AND a.DateOfReminder >= CURDATE()";
}
else if ($filter == 2)
{
     $conds .= " AND a.DateOfReminder < CURDATE()";
}

$escaped_keyword = $lsmartcard->Get_Safe_Sql_Like_Query($keyword);

$sql  = "SELECT
		1, 
               a.DateOfReminder, $user_field, a.Reason,
               CONCAT('<input type=checkbox name=ReminderID[] value=', a.ReminderID ,'>')
         FROM
             CARD_STUDENT_REMINDER as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType = 2 AND b.RecordStatus IN(0,1,2)
         WHERE
              b.UserID IS NOT NULL AND a.TeacherID='".IntegerSafe($UserID)."' AND
              (b.ChineseName like '%$escaped_keyword%'
               OR b.EnglishName like '%$escaped_keyword%'
               OR a.Reason like '%$escaped_keyword%'
               OR $user_field like '%$escaped_keyword%'
               )
               $conds
                ";

# TABLE INFO
if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.DateOfReminder","b.EnglishName","a.Reason");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->IsColOff = "SmartCardReminder";

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class='tabletopnolink'>#</td>\n";
$li->column_list .= "<td width=25% class='tabletoplink'>".$li->column($pos++, $i_StudentAttendance_Reminder_Date)."</td>\n";
$li->column_list .= "<td width=25% class='tabletoplink'>".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width=50% class='tabletoplink'>".$li->column($pos++, $i_StudentAttendance_Reminder_Reason)."</td>\n";
$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("ReminderID[]")."</td>\n";

$select_status = "<SELECT name=filter onChange=this.form.submit()>\n";
$select_status .= "<OPTION value='' ".($filter==""?"SELECTED":"").">$AllRecords</OPTION>\n";
$select_status .= "<OPTION value='1' ".($filter=="1"?"SELECTED":"").">$i_StudentAttendance_Reminder_Status_Coming</OPTION>\n";
$select_status .= "<OPTION value='2' ".($filter=="2"?"SELECTED":"").">$i_StudentAttendance_Reminder_Status_Past</OPTION>\n";
$select_status .= "</SELECT>\n";

### Button
$AddBtn 	= "<a href=\"javascript:checkNew('reminder_new.php')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'ReminderID[]','reminder_remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_delete . "</a>";
$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'ReminderID[]','reminder_edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";
$searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
$searchTag 	.= "<td><input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".escape_double_quotes(stripslashes($keyword))."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
$searchTag 	.= "</tr></table>";

### Title ###
$TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/smartcard/icon_reiminderrecord.gif' align='absmiddle' />";
$TitleTitle1 = "<span class='contenttitle'>".$TitleImage1. $i_StudentAttendance_Menu_OtherFeatures_Reminder  ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($TitleTitle, "", 0);

$linterface->LAYOUT_START();
?>

<br />
<form name="form1" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
                                        	<table border="0" cellspacing="0" cellpadding="2">
                                            	<tr>
                                              		<td><p><?=$AddBtn?></p></td>
                                            	</tr>
                                          	</table>
					</td>
                                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
				</tr>

				<tr>
					<td class="tabletext">
                                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                	<td class="tabletext" nowrap><?=$i_general_EachDisplay?>:&nbsp;<?=$select_status?></td>
                                                        <td width="100%" align="left" class="tabletext"><?=$searchTag?></td>
						</tr>
                                                </table>
					</td>
					<td align="right" valign="bottom">
						<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
							<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
								<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td nowrap><?=$editBtn?></td>
                                                                        <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
                                                                        <td nowrap><?=$delBtn?></td>
								</tr>
        							</table>
							</td>
        						<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
						</tr>
						</table>
					</td>
				</tr>
                                <tr>
                                	<td colspan="2">
						<?php
							echo $li->display();
						?>
					</td>
				</tr>
				</table>
			</td>
		</tr>
                </table>
        </td>
</tr>
</table>
<br />

<input type='hidden' name='pageNo' value="<?php echo $li->pageNo; ?>">
<input type='hidden' name='order' value="<?php echo $li->order; ?>">
<input type='hidden' name='field' value="<?php echo $li->field; ?>">
<input type='hidden' name='page_size_change' value="">
<input type='hidden' name='numPerPage' value="<?=$li->page_size?>">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
