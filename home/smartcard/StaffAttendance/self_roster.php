<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();
$lsmartcard = new libsmartcard();
$CurrentPage	= "PageStaffOwnRoster";

### Title ###
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['StaffAttendance']['RosterReport'], "", 0);
$linterface->LAYOUT_START();

echo $StaffAttend3UI->Get_Report_Roster_Group_Calendar_Index($_SESSION['UserID'],date('m'),date('Y'),"Individual","",true);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
// ajax function
{
function Get_Report_Roster_Group_Calendar_Table(ID, TargetMonth, TargetYear, GroupOrIndividual)
{
	var ID = ID || $('#GroupList').val();
	var TargetMonth = TargetMonth || $('#TargetMonth').val();
	var TargetYear = TargetYear || $('#TargetYear').val();
	var GroupOrIndividual = GroupOrIndividual || 'Individual';
	var PostVar = {
		"ID":ID,
		"GroupOrIndividual":GroupOrIndividual,
		"TargetMonth":TargetMonth,
		"TargetYear":TargetYear
		};
	
	Block_Element("RosterTableLayer");
	$.post('ajax_get_roster_individual_calendar_table.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#RosterTableLayer').html(data);
							UnBlock_Element("RosterTableLayer");
						}
					});
}
}
</script>