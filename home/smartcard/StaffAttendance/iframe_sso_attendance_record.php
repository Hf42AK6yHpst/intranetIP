<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$ParentSite = urldecode($_REQUEST['ParentSite']);

$StaffAttend3UI = new libstaffattend3_ui();
?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/content_30.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/2007script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>home/smartcard/StaffAttendance/sso_script.js"></script>
<?
echo '<div class="main_content" style="height:100%">';
echo $StaffAttend3UI->Get_Self_Report_Attendance_Monthly_Summary_Index();
echo '</div>';
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
var ParentSite = '<?=$ParentSite?>';
// dom function 
{
function Check_Go_Search_Monthly_Details(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Report_Attendance_Monthly_Details_List();
	}
	else
		return false;
}

function SetTimerToHideWarning(WarningLayerId, ticks)
{
	var action = "$('#"+WarningLayerId+"').css('visibility','hidden');";
	setTimeout(action,ticks);
}
}

// ajax function
{
function Get_Report_Attendance_Monthly_Summary()
{
	var StartYear = $('#StartYear').val();
	var EndYear = $('#EndYear').val();
	var PostVar = {
		"StartYear":StartYear,
		"EndYear":EndYear
	}
	
	Block_Element("ReportAttendanceLayer");
	$.post('ajax_get_monthly_summary.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceLayer').html(data);
							Thick_Box_Init();
							Resize_Iframe();
							UnBlock_Element("ReportAttendanceLayer");
						}
					});
}

function Get_Report_Attendance_Monthly_Summary_List()
{
	var StartYear = $('#StartYear').val();
	var EndYear = $('#EndYear').val();
	var PostVar = {
		"StartYear":StartYear,
		"EndYear":EndYear
	}
	
	if(StartYear > EndYear)
	{
		$WarningLayer = $('#DateWarningLayer');
		$WarningLayer.html('<?=$Lang['StaffAttendance']['InvalidDateRange']?>');
		$WarningLayer.css('visibility','visible');
		SetTimerToHideWarning('DateWarningLayer', 3000);
		return;
	}
	
	Block_Element("ReportAttendanceTableLayer");
	$.post('ajax_get_monthly_summary_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceTableLayer').html(data);
							Thick_Box_Init();
							Resize_Iframe();
							UnBlock_Element("ReportAttendanceTableLayer");
						}
					});
}

function Get_Report_Attendance_Monthly_Details(Year, Month)
{
	var TargetYear = Year || $('#SelectYear').val();
	var TargetMonth = Month || $('#SelectMonth').val();
	var Keyword = '';
	if(document.getElementById('Keyword'))
		Keyword = encodeURIComponent($('Input#Keyword').val());
	var PostVar = {
		"SelectMonth":TargetMonth,
		"SelectYear":TargetYear,
		"Keyword": Keyword
	};
	
	Block_Element("ReportAttendanceLayer");
	$.post('ajax_get_monthly_details.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceLayer').html(data);
							Thick_Box_Init();
							Resize_Iframe();
							UnBlock_Element("ReportAttendanceLayer");
						}
					});
}

function Get_Report_Attendance_Monthly_Details_List(Year, Month)
{
	var TargetYear = Year || $('#SelectYear').val();
	var TargetMonth = Month || $('#SelectMonth').val();
	var Keyword = '';
	if(document.getElementById('Keyword'))
		Keyword = encodeURIComponent($('Input#Keyword').val());
	var PostVar = {
		"SelectMonth":TargetMonth,
		"SelectYear":TargetYear,
		"Keyword": Keyword
	};
	
	Block_Element("ReportAttendanceTableLayer");
	$.post('ajax_get_monthly_details_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceTableLayer').html(data);
							Thick_Box_Init();
							Resize_Iframe();
							UnBlock_Element("ReportAttendanceTableLayer");
						}
					});
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}

// onload function to resize containing iframe size
$(document).ready(function() {
	Resize_Iframe();
});
</script>