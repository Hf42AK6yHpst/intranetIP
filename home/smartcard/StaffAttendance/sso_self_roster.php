<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if (!$sys_custom["StaffAttendance"]["SSORedirect"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lu = new libuser($_SESSION["UserID"]);

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();
$lsmartcard = new libsmartcard();
$CurrentPage	= "PageStaffOwnRoster";

### Title ###
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['StaffAttendance']['RosterReport'], "", 0);
$linterface->LAYOUT_START();

# get the URL of this site
$PageURLArr = explode("//", curPageURL());
if (sizeof($PageURLArr)>1)
{
	$strPath = $PageURLArr[1];
	$eClassURL = $PageURLArr[0] . "//" . substr($strPath, 0, strpos($strPath, "/"));
}
?>
<iframe id="SSOFrame" name="SSOFrame" width="100%" scrolling="no" frameborder="0" src="<?=$PageURLArr[0]?>//<?=$sys_custom["StaffAttendance"]["SSOSiteName"]?>/login_sso.php?target_url=<?=urlencode("home/smartcard/StaffAttendance/iframe_sso_self_roster.php?ParentSite=".$eClassURL)?>&eClassKey=<?=$lu->sessionKey?>&UserLogin=<?=$lu->UserLogin?>&FromSite=<?=urlencode($eClassURL)?>"></iframe>
<?
//echo $StaffAttend3UI->Get_Report_Roster_Group_Calendar_Index($_SESSION['UserID'],date('m'),date('Y'),"Individual","",true);
$linterface->LAYOUT_STOP();
intranet_closedb();
?>