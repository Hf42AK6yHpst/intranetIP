<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if (!$sys_custom["StaffAttendance"]["SSORedirect"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lu = new libuser($_SESSION["UserID"]);

$linterface = new interface_html();
$lsmartcard = new libsmartcard();
$CurrentPage = "PageStaffOwnAttendance";

### Title ###
$TAGS_OBJ[] = array($Lang['StaffAttendance']['AttendanceRecord'],"sso_attendance_record.php", 1);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['InOutRecord'],"sso_in_out_record.php", 0);
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

# get the URL of this site
$PageURLArr = explode("//", curPageURL());
if (sizeof($PageURLArr)>1)
{
	$strPath = $PageURLArr[1];
	$eClassURL = $PageURLArr[0] . "//" . substr($strPath, 0, strpos($strPath, "/"));
}
?>
<iframe id="SSOFrame" name="SSOFrame" width="100%" scrolling="no" frameborder="0" src="<?=$PageURLArr[0]?>//<?=$sys_custom["StaffAttendance"]["SSOSiteName"]?>/login_sso.php?target_url=<?=urlencode("home/smartcard/StaffAttendance/iframe_sso_attendance_record.php?ParentSite=".$eClassURL)?>&eClassKey=<?=$lu->sessionKey?>&UserLogin=<?=$lu->UserLogin?>&FromSite=<?=urlencode($eClassURL)?>"></iframe>
<?
//echo $StaffAttend3UI->Get_Self_Report_Attendance_Monthly_Summary_Index();
$linterface->LAYOUT_STOP();
intranet_closedb();
?>