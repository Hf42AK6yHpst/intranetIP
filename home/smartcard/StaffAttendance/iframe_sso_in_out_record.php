<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$ParentSite = urldecode($_REQUEST['ParentSite']);

$StaffAttend3UI = new libstaffattend3_ui();
?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/content_30.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/2007script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>home/smartcard/StaffAttendance/sso_script.js"></script>
<?
echo '<div class="main_content" style="height:100%">';
echo $StaffAttend3UI->Get_Report_DailyLog_Index("","",$_SESSION['UserID'],"","SmartCard");
echo '</div>';
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
var ParentSite = '<?=$ParentSite?>';
// dom function 
{
function Check_Go_Search_DailyLog(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Report_DailyLog_List();
	}
	else
		return false;
}
}

// ajax function
{
function Get_Report_DailyLog_List()
{
	var PostVar = {
			"TargetDate": $('#SelectYear').val()+'-'+$('#SelectMonth').val()+'-01',
			"SelectType": $('#SelectType').val(),
			"SelectStaff": $('#SelectStaff').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val())
		}
	
	Block_Element("ReportDailyLogTableLayer");
	$.post('ajax_get_inout_list.php',PostVar,
					function(data){
						if (data == "die")
							window.top.location = '/';
						else {
							$('div#ReportDailyLogTableLayer').html(data);
							Thick_Box_Init();
							Resize_Iframe();
							UnBlock_Element("ReportDailyLogTableLayer");
						}
					});
}

}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}

// onload function to resize containing iframe size
$(document).ready(function() {
	Resize_Iframe();
});
</script>