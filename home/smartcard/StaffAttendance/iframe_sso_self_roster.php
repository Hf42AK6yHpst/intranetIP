<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$ParentSite = urldecode($_REQUEST['ParentSite']);

$StaffAttend3UI = new libstaffattend3_ui();
?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/content_30.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/2007script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>home/smartcard/StaffAttendance/sso_script.js"></script>
<?
echo '<div class="main_content" style="height:100%">';
echo $StaffAttend3UI->Get_Report_Roster_Group_Calendar_Index($_SESSION['UserID'],date('m'),date('Y'),"Individual","",true);
echo '</div>';
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
var ParentSite = '<?=$ParentSite?>';
// ajax function
{
function Get_Report_Roster_Group_Calendar_Table(ID, TargetMonth, TargetYear, GroupOrIndividual)
{
	var ID = ID || $('#GroupList').val();
	var TargetMonth = TargetMonth || $('#TargetMonth').val();
	var TargetYear = TargetYear || $('#TargetYear').val();
	var GroupOrIndividual = GroupOrIndividual || 'Individual';
	var PostVar = {
		"ID":ID,
		"GroupOrIndividual":GroupOrIndividual,
		"TargetMonth":TargetMonth,
		"TargetYear":TargetYear
		};
	
	Block_Element("RosterTableLayer");
	$.post('ajax_get_roster_individual_calendar_table.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#RosterTableLayer').html(data);
							Resize_Iframe();
							UnBlock_Element("RosterTableLayer");
						}
					});
}
}

// onload function to resize containing iframe size
$(document).ready(function() {
	Resize_Iframe();
});
</script>