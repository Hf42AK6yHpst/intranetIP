<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$lsmartcard = new libsmartcard();
$StaffAttend3UI = new libstaffattend3_ui();
$CurrentPage = "PageStaffOwnAttendance";

### Title ###
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['StaffAttendance']['AttendanceRecord'], "attendance_record.php", 0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['InOutRecord'], "in_out_record.php", 0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['EntryLogReport'], "entry_log.php", 1);
$linterface->LAYOUT_START();

echo $StaffAttend3UI->Get_Report_Entry_Log_Index(true);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
// ajax function
{
function Generate_Report(Mode) {
	var Mode = Mode || "View";

	if(Mode == "Print")
	{
		$("input#Mode").val(Mode);
		var FormObj = document.getElementById('EntryLogForm');
		FormObj.action = "ajax_get_entry_log.php";
		FormObj.target = "_blank";
		FormObj.submit();
	}else if(Mode == "Export")
	{
		$("input#Mode").val(Mode);
		var FormObj = document.getElementById('EntryLogForm');
		FormObj.action = "ajax_get_entry_log.php";
		
		FormObj.submit();
	}else
	{
		var PostVar = {
				"StartDate": $('Input#StartDate').val(),
				"EndDate": $('Input#EndDate').val(),
				"TargetUser[]": $('#TargetUser').val(),
				"GroupBy": Get_Selection_Value("GroupBy","String"), 
				"ShowNoDutyDateOnly": (document.getElementById('ShowNoDutyDateOnly').checked)? "1":"0", 
				"HideWithoutData": (document.getElementById('HideWithoutData').checked)? "1":"0", 
				"Mode":Mode
			}
		
		if($('input#StartDate').val() > $('input#EndDate').val())
		{
			var $WarningLayer = $('#WarningLayer');
			$WarningLayer.html('<?=$Lang['StaffAttendance']['InvalidDateRange']?>');
			$WarningLayer.css('visibility','visible');
			setTimeout("$('#WarningLayer').css('visibility','hidden');",3000);
			return;
		}
		
		Block_Element("ReportLayer");
		$("div#ReportLayer").load('ajax_get_entry_log.php',PostVar,
			function(data){
				if (data == "die") 
					window.top.location = '/';
				else {
					UnBlock_Element("ReportLayer");
				}
			});
	}
}
}

// dom function 
{

}
</script>