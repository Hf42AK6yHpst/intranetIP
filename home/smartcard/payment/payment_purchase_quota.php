<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$plugin['payment']){
    header("Location: $intranet_httppath/");	
    exit();
}

$CurrentPage	= "PagePhotocopierPurchaseQuota";
$linterface 	= new interface_html();
$lsmartcard		= new libsmartcard();
$lpayment		= new libpayment();
$lu 			= new libuser($UserID);

$hasRight = true;

/*
if ($lu->RecordType == 1)  # Teacher
{
    $hasRight = false;
}
else*/
if ($lu->RecordType == 3)  # Parent
{
     $hasRight = false;
}
else # Student / Teacher
{
       $studentid = $UserID;
}

if (!$hasRight)
{
     header("Location: $intranet_httppath/");
     exit();
}

if ($hasRight)
{	
	$studentid = integerSafe($studentid);
	$temp = $lpayment->checkBalance($studentid);
	list($balance, $lastupdated) = $temp;
	if ($balance != "" && $lastupdated != "")
	{
	    $balance_str = "$ ".number_format($balance,2)." ($i_Payment_Field_LastUpdated: $lastupdated)";
	}
	else
	{
	    $balance_str = "$ $balance";
	}
	
	$sql = "SELECT RecordID, NumOfQuota, CONCAT('$', FORMAT(Price, 1)), PurchasingItemName FROM PAYMENT_PRINTING_PACKAGE ORDER BY NumOfQuota, Price";
	$temp = $lpayment->returnArray($sql, 4);

	$table_content = "";
	$table_content .= "<tr class=\"tabletop\">";
	$table_content .= "<td class=\"tabletoplink\" width=\"35%\">$i_Payment_Menu_PhotoCopier_TitleName</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"20%\">$i_Payment_Field_Quota</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"20%\">$i_Payment_Field_Amount</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"20%\">$i_SmartCard_Payment_Photocopier_Quota_Purchase_Quantity</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"5%\"><input type=checkbox onClick=(this.checked)?setChecked(1,this.form,'purchaseItem[]'):setChecked(0,this.form,'purchaseItem[]')></td>";
	$table_content .= "</tr>";
	
	if(sizeof($temp)>0)
	{
		for($i=0; $i<sizeof($temp); $i++)
		{
			list($record_id, $num_of_quota, $price, $item_name) = $temp[$i];
			$table_content .= "<tr><td nowrap=\"nowrap\" class=\"tabletext\">$item_name</td>";
			$table_content .= "<td nowrap=\"nowrap\" class=\"tabletext\">$num_of_quota</td>";
			$table_content .= "<td nowrap=\"nowrap\" class=\"tabletext\">$price</td>";
			$table_content .= "<td><input type=text name=qty_$record_id size=7 disabled=true style='background-color:#CCCCCC'></td>";
			$table_content .= "<td><input type=checkbox name=purchaseItem[] value=$record_id onClick='this.checked? checkClicked(1, $record_id) : checkClicked(0, $record_id)'></td></tr>";
		}
	}
	if(sizeof($temp)==0)
	{
		$table_content .= "<tr><td colspan=\"5\" nowrap=\"nowrap\" class=\"tabletext\">$i_no_record_exists_msg</td></tr>";
	}
	
	
	
	### Title ###
    $MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
    $TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/smartcard/icon_accountbalance.gif' align='absmiddle' />";
    $TitleTitle1 = "<span class='contenttitle'>". $TitleImage1.$i_Payment_Menu_PhotoCopier_Quota_Purchase ."</span>";
    $TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
    $TAGS_OBJ[] = array($TitleTitle, "", 0);
    
    $lpayment = new libpayment();
    $temp = $lpayment->checkBalance($studentid);
    list($balance, $lastupdated) = $temp;
    if ($balance != "" && $lastupdated != "")
    {
        $balance_str = "$ ".number_format($balance,2)." ($i_Payment_Field_LastUpdated: $lastupdated)";
    }
    else
    {
        $balance_str = "$ $balance";
    }
        
	$linterface->LAYOUT_START();

?>
<SCRIPT LANGUAGE="Javascript">
function resetForm(formObj)
{
	formObj.reset();
	<?
	$sql = "SELECT RecordID, NumOfQuota, Price, PurchasingItemName FROM PAYMENT_PRINTING_PACKAGE";
	$temp = $lpayment->returnArray($sql, 4);
	for($i=0; $i<sizeof($temp); $i++)
	{
		list($record_id, $num_of_quota, $price, $item_name) = $temp[$i];
	?>
		document.form1.qty_<?=$record_id?>.disabled=true;
		document.form1.qty_<?=$record_id?>.style.backgroundColor="#CCCCCC";
		document.form1.qty_<?=$record_id?>.value = 0;
	<?
	}
	?>
}

function checkClicked(cnt, id)
{
	var temp;
	temp = eval("document.form1.qty_"+id);
	if (cnt == 1)
	{
		temp.disabled=false; 
		temp.value="1";
		temp.style.backgroundColor="";
	}
	else
	{
		temp.disabled=true; 
		temp.value="";
		temp.style.backgroundColor="#CCCCCC";
	}
}

function setChecked(val, obj, element_name){
        len=obj.elements.length;
        var i=0;
        for( i=0 ; i<len ; i++) {
                if (obj.elements[i].name==element_name)
                {
                	obj.elements[i].checked=val;
				}
        }
        if(val == 1)
        {
	        <?
			$sql = "SELECT RecordID, NumOfQuota, Price, PurchasingItemName FROM PAYMENT_PRINTING_PACKAGE";
			$temp = $lpayment->returnArray($sql, 4);
			for($i=0; $i<sizeof($temp); $i++)
			{
				list($record_id, $num_of_quota, $price, $item_name) = $temp[$i];
			?>
				document.form1.qty_<?=$record_id?>.disabled=false;
				document.form1.qty_<?=$record_id?>.style.backgroundColor="";
				document.form1.qty_<?=$record_id?>.value = 1;
			<?
			}
			?>
		}
		if(val == 0)
		{
			<?
			$sql = "SELECT RecordID, NumOfQuota, Price, PurchasingItemName FROM PAYMENT_PRINTING_PACKAGE";
			$temp = $lpayment->returnArray($sql, 4);
			for($i=0; $i<sizeof($temp); $i++)
			{
				list($record_id, $num_of_quota, $price, $item_name) = $temp[$i];
			?>
				document.form1.qty_<?=$record_id?>.disabled=true;
				document.form1.qty_<?=$record_id?>.style.backgroundColor="#CCCCCC";
				document.form1.qty_<?=$record_id?>.value = "";
			<?
			}
			?>
		}
}

function checkForm()
{
	var total_amount = 0;
	var checking_1 = 0;
	var checking_2 = 0;
	
	var obj = document.form1;
	var element = 'purchaseItem[]';
	
	if(countChecked(obj,element)>0)
	{
		<?
		$sql = "SELECT RecordID, NumOfQuota, Price, PurchasingItemName FROM PAYMENT_PRINTING_PACKAGE";
		$temp = $lpayment->returnArray($sql, 4);
		for($i=0; $i<sizeof($temp); $i++)
		{
			list($record_id, $num_of_quota, $price, $item_name) = $temp[$i];
		?>
			if((document.form1.qty_<?=$record_id?>.disabled == false) && (document.form1.qty_<?=$record_id?>.value > 0) && (!isNaN(document.form1.qty_<?=$record_id?>.value)))
			{
				var str = document.form1.qty_<?=$record_id?>.value.indexOf(".");
				if(str<0)
				{
					var amount = (<?=$price?> * document.form1.qty_<?=$record_id?>.value);
					total_amount = total_amount + amount;
					checking_1 = 1;
				}
			}
			if((document.form1.qty_<?=$record_id?>.disabled == false) && ((document.form1.qty_<?=$record_id?>.value == "") || (isNaN(document.form1.qty_<?=$record_id?>.value))))
			{
				checking_2 = -1;
			}
		<?
		}
		?>
	}
	else
	{
		checking_1 = -1;
	}
		
	if(checking_2 == -1)
	{
		alert("<?=$i_Payment_PhotocopierPackageQtyNotValid_Warning?>");
		return false;
	}
	else
	{
		if(checking_1 == 1)
		{
			if((total_amount > <?=$balance?>) || (total_amount <= 0))
			{
				alert ("<?=$i_Payment_PrintPage_Outstanding_LeftAmount?>");
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			if(checking_1 == -1)
			{
				alert("<?=$i_Payment_PhotocopierQuotaSelectPackage_Warning?>");
				return false;
			}
			if(checking_1 == 0)
			{
				alert("<?=$i_Payment_PhotocopierPackageQtyNotValid_Warning?>");
				return false;
			}
		}
	}
}
</SCRIPT>

<br>

<form name="form1" action="payment_photocopier_quota_update.php" method="POST" onSubmit="return checkForm()">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td valign="top" nowrap="nowrap" class="paymenttitle" width="10%"><?=$i_Payment_Field_Balance?> : </td>
				<td valign="top" class="tabletext"><?=$balance_str?></td>
			</tr>
			<tr>
				<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="center">
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<?=$table_content?>
		</table>
	</td>
</tr>
<tr>
	<td align="center">
	<table width="96%" border="0" cellspacing="0" cellpadding="5">
		<tr><td class="dotline" width="96%"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td></tr>
	</table>
	</td>
</tr>
<tr>
	<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "resetForm(document.form1);","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	</td>
</tr>
</table>

</form>

<br>

<?php
}

$linterface->LAYOUT_STOP();
intranet_closedb();
?>