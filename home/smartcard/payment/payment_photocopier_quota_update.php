<?
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libfamily.php");
include_once("../../../includes/libpayment.php");
include_once("../../../lang/lang.$intranet_session_language.php");

intranet_opendb();
$lpayment = new libpayment();
$lidb = new libdb();

//echo $UserID."<BR><BR>";

### Get The SmartCard Balance Of The User ###
$sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '$UserID'";
$temp_balance = $lpayment->returnVector($sql);
list($balance) = $temp_balance;

if(sizeof($purchaseItem) > 0)
{
	$purchaseItemList = "'".implode("','",$purchaseItem)."'";
}

$sql = "SELECT RecordID, NumOfQuota, Price, PurchasingItemName From PAYMENT_PRINTING_PACKAGE WHERE RecordID IN ($purchaseItemList)";
$temp_purchaseDetail = $lpayment->returnArray($sql, 4);
//print_r($temp_purchaseDetail);

if(sizeof($temp_purchaseDetail) > 0)
{
	for($i=0; $i<sizeof($temp_purchaseDetail); $i++)
	{
		list($record_id, $num_of_quota, $price, $item_name) = $temp_purchaseDetail[$i];
		$unit_price = $price * ${"qty_$record_id"};
		$total_amount = $total_amount + $unit_price;	
	}
}

//echo "<BR><br>Current Balance: ".$balance;
//echo "<BR><BR>Total Amount: ".$total_amount."<BR><BR>";


if($balance >= $total_amount)
{
	# Deduct balance
	$sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance - $total_amount
			,LastUpdateByAdmin = NULL,LastUpdateByTerminal='$username',LastUpdated = NOW()
			WHERE StudentID = '$UserID'";
			//echo $sql."<BR><BR>";
	$result = $lpayment->db_db_query($sql);
	if ($result)
	{
		$cnt_1 = 1;
	}
	
	# Insert Transaction Record
	$balanceAfter = $balance - $total_amount;
	if(sizeof($temp_purchaseDetail) > 0)
	{
		//echo $balance;
		$temp_balanceAfter = $balance;
		
		for($i=0; $i<sizeof($temp_purchaseDetail); $i++)
		{
			list($record_id, $num_of_quota, $price, $item_name) = $temp_purchaseDetail[$i];	
			
			//echo $price * ${"qty_$record_id"};
			$temp_balanceAfter = $temp_balanceAfter - $price * ${"qty_$record_id"};
			//echo $temp_balanceAfter;
			
			$sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
					(StudentID, TransactionType, Amount, RelatedTransactionID, BalanceAfter, TransactionTime, Details)
					VALUES
					('$UserID', 3, $price*".${"qty_$record_id"}.",NULL,'$temp_balanceAfter',NOW(),'$item_name')";
			//echo $sql."<BR><BR>";
			$temp_total_quota = $num_of_quota * ${"qty_$record_id"};
			$total_quota = $total_quota + $temp_total_quota;
			$result = $lpayment->db_db_query($sql);
			if ($result)
			{
				$cnt_2 = 1;
			}
			
			//echo "1###".number_format($balanceAfter,2);
			//echo "###".date('Y-m-d H:i:s');
		}
	}
	//echo $total_quota;
	$sql = "INSERT IGNORE INTO PAYMENT_PRINTING_QUOTA VALUES ('$UserID', 0, 0, '', '', NOW(), NOW())";
	//echo $sql;
	$lpayment->db_db_query($sql);

	### Record PRINTING QUOTA log ##
	$updatePrintingQuotaLogCondition = "where UserID = '".$UserID."' ";
	$lpayment->update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG($total_quota,4,$updatePrintingQuotaLogCondition,$lidb);	

	### Update User Photocopier Quota ###
	$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET TotalQuota = TotalQuota + $total_quota WHERE UserID = '$UserID'";
	//echo $sql;	
	$lpayment->db_db_query($sql);
	if ($result)
	{
		$cnt_3 = 1;
	}	
}
if(($cnt_1 == 1)&&($cnt_2 == 1)&&($cnt_3 == 1))
{
	Header ("Location: paymentinfo.php?msg=1");
	intranet_closedb();
}
else
{
	Header ("Location: paymentinfo.php?msg=2");
	intranet_closedb();
}

?>
