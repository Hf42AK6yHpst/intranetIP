<?php
// Editing by 
/*
 * 2015-09-29 (Carlos): Added word [Status].
 * 2014-02-27 (Carlos): Added record access right checking
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

$invalid_access = false;
$lu = new libuser($_SESSION['UserID']);
$lpayment = new libpayment();

$PaymentID = IntegerSafe($PaymentID);
$sql = "SELECT StudentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID='$PaymentID'";
$studentIdRecord = $lpayment->returnVector($sql);

if($studentIdRecord[0] != $_SESSION['UserID'])
{
	if($lu->isParent()){
		$childrenIdAry = $lu->getChildren();
		if(!in_array($studentIdRecord[0],$childrenIdAry)){
			$invalid_access = true;
		}
	}else if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']){
		$invalid_access = true;
	}
}
if($invalid_access){
	No_Access_Right_Pop_Up();
}

$payment_info = $lpayment->returnPaymentDetailedInfo($PaymentID);
list($itemname, $amount, $studentname, $classname, $classnum, $paidtime, $status) = $payment_info;
$amount = "$ ".number_format($amount,1);
$status_str = ($status==1?"$i_Payment_PaymentStatus_Paid":"$i_Payment_PaymentStatus_Unpaid");
$i_title = "$i_Payment_Title_Receipt_Payment";
?>
<br>
<div align="center" class="tabletext"><?=$i_title?></div>
<br>
<table width="95%" border="0" cellpadding="4" cellspacing="1" align="center" class="tabletext">
<tr><td align="right" nowrap width="50%"><?=$i_Payment_Field_PaymentItem?>:</td><td width="50%"><?=$itemname?></td></tr>
<tr><td align="right" nowrap width="50%"><?=$i_Payment_Field_Amount?>:</td><td width="50%"><?=$amount?></td></tr>
<tr><td align="right" nowrap width="50%"><?=$i_UserStudentName?>:</td><td width="50%"><?=$studentname?></td></tr>
<tr><td align="right" nowrap width="50%"><?=$i_UserClassName?>:</td><td width="50%"><?=$classname?></td></tr>
<tr><td align="right" nowrap width="50%"><?=$i_UserClassNumber?>:</td><td width="50%"><?=$classnum?></td></tr>
<tr><td align="right" nowrap width="50%"><?=$Lang['General']['Status2']?>:</td><td width="50%"><?=$status_str?></td></tr>
<?php if ($status==1) { ?>
<tr><td align="right" nowrap width="50%"><?=$i_Payment_Field_PaidTime?>:</td><td width="50%"><?=$paidtime?></td></tr>
<? } ?>
</table>

<?php
intranet_closedb();
?>
