<?php 
# using: yat

################# Change Log [Start] ############
#
#	Date:	2011-09-15 (YatWoon)
#			add option "Allow student choose option for need receipt or not"
#			change the layout to IP25 standard
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../"; 

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$GeneralSetting = new libgeneralsettings();
$Settings = $GeneralSetting->Get_General_Setting('ePayment');

if(!$plugin['payment'] || !$Settings['AllowStudentChooseReceipt'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libuserpersonalsettings.php");

$linterface = new interface_html();
$CurrentPage	= "PageReceiptSettings"; 
$lsmartcard	= new libsmartcard();
$lps = new libuserpersonalsettings();

# Left menu 
$TAGS_OBJ[] = array($Lang['ePayment']['ReceiptSettings']);
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START($Lang['ePayment'][$msg]);

## Retrieve student option
$dataAry = array("ePayment_NoNeedReceipt","ePayment_NoNeedReceipt_DateModified");
$psettings = $lps->Get_Setting($UserID, $dataAry);	
$NeedReceipt = $psettings['ePayment_NoNeedReceipt'] ? 0 : 1;
?>

<script language="javascript">
<!--
function EditInfo()
{
	$('#EditBtn').attr('style', 'visibility: hidden'); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', '');
	
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
}		

function ResetInfo()
{
	document.form1.reset();
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
}
//-->
</script>
		
<form name="form1" method="get" action="receipt_settings_update.php">

<div class="table_board">
	<div class="table_row_tool row_content_tool">
		<?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
	</div>
	<p class="spacer"></p>

	<table class="form_table_v30">
	
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['ePayment']['NeedReceipt']?></td>
				<td> 
				<span class="Edit_Hide"><?=$NeedReceipt ? $i_general_yes:$i_general_no?></span>
				<span class="Edit_Show" style="display:none">
				<input type="radio" name="NeedReceipt" value="1" <?=$NeedReceipt ? "checked":"" ?> id="NeedReceipt1"> <label for="NeedReceipt1"><?=$i_general_yes?></label> 
				<input type="radio" name="NeedReceipt" value="0" <?=$NeedReceipt ? "":"checked" ?> id="NeedReceipt0"> <label for="NeedReceipt0"><?=$i_general_no?></label>
				</span></td>
			</tr>
			
			
	</table>                        
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
		<p class="spacer"></p>
	</div>
</div>

</form>




<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>