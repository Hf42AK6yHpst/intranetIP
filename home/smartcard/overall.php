<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");


intranet_auth();
intranet_opendb();

if(!$plugin['attendancestudent'] and !$plugin['payment'])
{
	header("Location: $intranet_httppath/");
        exit();
}

$linterface 	= new interface_html();
$lsmartcard	= new libsmartcard();
$CurrentPage	= "PageOverall";

$year	= date('Y');
$month 	= date('m');
$day	= date('d');

$toDay = date('Y-m-d');

$lu 		= new libuser($UserID);
//$students 	= array($UserID);
$studentid = $UserID;

if($plugin['attendancestudent'])
{
    $lcard = new libcardstudentattend2();
    $lcard->retrieveSettings();
    $record = $lcard->retrieveStudentDailyRecord($studentid);

    if ( $record==false || !is_array($record))
    {
	$str_status = "";
    }
    else
    {
        list($in_school_time, $in_school_station, $am_status, $lunch_out_time, $lunch_out_station,
              $lunch_back_time, $lunch_back_station, $pm_status, $leave_school_time, $leave_school_status) = $record;
        # $attendance_detail_table = "";
        # AM
        # $attendance_detail_table .= "<tr><td>$i_StudentAttendance_LeftStatus_Type_InSchool:</td><td>";

        $is_present = false;
        if ($lcard->attendance_mode==0)   # AM Only
        {
            switch ($am_status)
            {
                    case CARD_STATUS_PRESENT:
                    case CARD_STATUS_LATE:
                         $str_status = $i_StudentAttendance_InSchool_BackAlready; break;
                    case CARD_STATUS_OUTING:
                         $str_status = $i_StudentAttendance_Status_Outing; break;
                    case CARD_STATUS_ABSENT:
                    default:
                         $str_status = $i_StudentAttendance_Report_NoRecord; break;

            }
            # $attendance_detail_table .= $str_status;
            $str_in_status = $str_status;
            if ($in_school_time != '-')
            {
                    $str_in_status = "$in_school_time";
                # $attendance_detail_table .= " ($in_school_time)";
            }
            # Leave School Status
            if ($am_status == CARD_STATUS_PRESENT || $am_status == CARD_STATUS_LATE)
            {
                $is_present = true;
            }
        }
        else if ($lcard->attendance_mode == 1) # PM Only
        {
            switch ($pm_status)
            {
                    case CARD_STATUS_PRESENT:
                    case CARD_STATUS_LATE:
                         $str_status = $i_StudentAttendance_InSchool_BackAlready; break;
                    case CARD_STATUS_OUTING:
                         $str_status = $i_StudentAttendance_Status_Outing; break;
                    case CARD_STATUS_ABSENT:
                    default:
                         $str_status = $i_StudentAttendance_Report_NoRecord; break;

            }
            # $attendance_detail_table .= $str_status;
            $str_in_status=$str_status;
            if ($in_school_time != '-')
            {
                # $attendance_detail_table .= " ($in_school_time)";
                $str_in_status = "$in_school_time";

            }
            # Leave School Status
            if ($pm_status == CARD_STATUS_PRESENT || $pm_status == CARD_STATUS_LATE)
            {
                $is_present = true;
            }

        }
        else if ($lcard->attendance_mode == 2 || $lcard->attendance_mode == 3)
        {
             if ($am_status == CARD_STATUS_PRESENT || $am_status == CARD_STATUS_LATE)
             {
                 # $attendance_detail_table .= $i_StudentAttendance_InSchool_BackAlready;
                 $str_in_status = $i_StudentAttendance_InSchool_BackAlready;

                 if ($in_school_time != '-')
                 {
                     # $attendance_detail_table .= " ($in_school_time)";
                     $str_in_status.=" ($in_school_time)";
                 }
                 $is_present = true;
             }
             else
             {
                 if ($pm_status == CARD_STATUS_PRESENT || $pm_status == CARD_STATUS_LATE)
                 {
                     # $attendance_detail_table .= $i_StudentAttendance_InSchool_BackAlready;
                     $str_in_status .= $i_StudentAttendance_InSchool_BackAlready;

                     if ($in_school_time != '-')
                     {
                         # $attendance_detail_table .= " ($in_school_time)";
                         $str_in_status .= " ($in_school_time)";
                     }
                     $is_present = true;
                 }
             }
        }

        # Leave
        if ($is_present)
        {
            $str_out_status = "";
            # $attendance_detail_table .= "<tr><td>$i_StudentAttendance_LeftStatus_Type_AfterSchool:</td><td>";
            if ($leave_school_time != '-')
            {
                # $attendance_detail_table .= "$i_StudentAttendance_AfterSchool_Left ($leave_school_time)";
                $str_out_status .= "$i_StudentAttendance_AfterSchool_Left ($leave_school_time)";
            }
            else
            {
                # $attendance_detail_table .= $i_StudentAttendance_AfterSchool_Stay;
                $str_out_status = $i_StudentAttendance_AfterSchool_Stay;
            }
        }else{
               $str_out_status = "$i_StudentAttendance_Report_NoRecord";

             }
    }      
}

if($plugin['payment']) 
{
### Balance        
	$lpayment 	= new libpayment();
        $temp = $lpayment->checkBalance($studentid);
        list($balance, $lastupdated) = $temp;
        
        if ($balance != "" && $lastupdated != "")
        {
            $balance_str = "$ ".number_format($balance,2)." ($i_Payment_Field_LastUpdated: $lastupdated)";
        }
        else
        {
            $balance_str = "$ $balance";
        }
}     


### Title ###
$TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/smartcard/icon_overall.gif' align='absmiddle' />";
$TitleTitle1 = "<span class='contenttitle'>".$TitleImage1. $OverallRecord ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
$TAGS_OBJ[] = array($TitleTitle,"");
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>

<br />   

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="paymenttitle"><?=$i_BookingDateStart?> </span></td>
					<td class="tabletext"><?=$toDay?></td>
				</tr>
                                <tr>
                                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                                </tr>
                                <tr>
                                	<td colspan="2" align="left">&nbsp;&nbsp;&nbsp;</td>
                		</tr>
                                <? if($plugin['attendancestudent']) { ?>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_StudentAttendance_LeftStatus_Type_InSchool?> </span></td>
					<td class="tabletext"><?=$str_in_status==""?$i_StudentAttendance_Report_NoRecord:$str_in_status?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_StudentAttendance_LeftStatus_Type_AfterSchool?> </span></td>
					<td class="tabletext"><?=$str_out_status==""?$i_StudentAttendance_Report_NoRecord:$str_out_status?></td>
				</tr>
                                <tr>
                                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                                </tr>
                                <tr>
                                	<td colspan="2" align="left">&nbsp;&nbsp;&nbsp;</td>
                		</tr>
                                <? } ?>
                                <? if($plugin['payment']) { ?>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Payment_AccountBalance?> </span></td>
					<td class="tabletext"><?=$balance_str?></td>
				</tr>
                                <tr>
                                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                                </tr>
                                <tr>
                                	<td colspan="2" align="left">&nbsp;&nbsp;&nbsp;</td>
                		</tr>
                                <? } ?>
			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
</table>     
<br />  

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
