<?php
// Editing by 


/********************** Change Log ***********************
* 
*	2016-10-18 (Carlos): Added checking on [disallow non-teaching staff taking attendance].
*	2013-08-23 (Carlos): Redirect to Take Attendance if only lesson attendance is on
*
*  	Date	:	2010-08-27 [Kenneth]
*	Details	:	support integration with external Staff Attendance following $sys_custom["StaffAttendance"]["SSORedirect"]
*
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");

intranet_auth();
intranet_opendb();

if(!$plugin['attendancestudent'] and !$plugin['payment'] and !isset($module_version['StaffAttendance']) and !$plugin['attendancelesson'])
{
	header("Location: $intranet_httppath/");
	exit();
}


$lu = new libuser($UserID);


if ($sys_custom["StaffAttendance"]["SSORedirect"]) {
	header("Location: ".$PATH_WRT_ROOT."home/smartcard/StaffAttendance/sso_attendance_record.php");
	exit();
}

if($lu->isStudent())	
{
	header("Location: overall.php");
        exit;
}

if($lu->isParent())	
{
	if($plugin['attendancestudent'])
  {
	  header("Location: attendance/attendance_record/attendance_list.php");
	  exit;  
	}
	if($plugin['payment'])
  {
  	header("Location: payment/index.php");
		exit();
	}
}

if($lu->isTeacherStaff())
{
	
	if($plugin['attendancestudent'] || $plugin['attendancelesson'])
    {
    		$ladminjob = new libadminjob($_SESSION['UserID']);
        	$lcardattend = new libcardstudentattend2();
            //$lcardattend->retrieveSettings();
            if($lu->teaching == 1 || !($lcardattend->DisallowNonTeachingStaffTakeAttendance && $lu->teaching!=1))
            {
            	 header("Location: attendance/take_attendance.php");
                 exit();
            }
        	      
            /*    
               	$lteaching = new libteaching();
        	$class = $lteaching->returnTeacherClass($UserID);
        	$lclass = new libclass();
        	$classname = $class[0][1];
        	if (!$lcard->isRequiredToTakeAttendanceByDate($classname,$toDay))
        	{
        		$noNeedToRetrieve = true;
        	}
                if (!$noNeedToRetrieve)
                {
                    header("Location: attendance/attendance_list.php");
                    exit();
        	}   
            */    
	}
        
  if($plugin['payment'])
  {
  	header("Location: payment/index.php");
		exit();
	}

  if($module_version['StaffAttendance'] == 2.0)
  {
  	header("Location: StaffAttendance/OwnAttendance.php");
  	exit();
  }
  
  if($module_version['StaffAttendance'] == 3.0)
  {
  		header("Location: ".$PATH_WRT_ROOT."home/smartcard/StaffAttendance/attendance_record.php");
  		exit();
  }
}	

header("Location: $intranet_httppath/");
      
intranet_closedb();

?>