<?
# using: yat

$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

$MODULE_OBJ['title'] = $Lang['SysMgr']['RoleManagement']['UpdateStudentPwdPopUp'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);

//if(!($_SESSION['SSV_USER_ACCESS']["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]))
if(!($_SESSION["CanAccessUpdateStudentPwdPopup"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT($Lang['General']['AccessDenied']);
	$linterface->LAYOUT_STOP();
	exit;
}

intranet_auth();
intranet_opendb();

$li = new libuser();

##### Class List
$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=\"targetClass\" onChange=\"this.form.action='';this.form.submit()\"",$targetClass);
if ($targetClass != "")
{
	$list = $lclass->returnStudentListByClass($targetClass);
	if(sizeof($list))
	{
    	$select_students = $lclass->getStudentSelectByClass($targetClass,"name=\"targetID\"");
	}
    else
    {
	    $select_students= $i_StudentAttendance_Message_NoStudentInClass;
    }	
}
?>

<script language="javascript">
function checkform(obj)
{
	if(!check_text(obj.NewPassword, "<?php echo $i_alert_pleasefillin.$i_frontpage_myinfo_password_new; ?>")) return false;
      
	<?php if ($li->RetrieveUserInfoSetting("Enable", "PasswordPolicy",USERTYPE_STUDENT )) { ?>
	if (obj.NewPassword.value.length<6)
	{
		alert("<?=$Lang['PasswordNotSafe'][-2]?>");
		obj.NewPassword.focus();
		return false;
	}
	var re = /[a-zA-Z]/;
	if (!re.test(obj.NewPassword.value))
	{
		alert("<?=$Lang['PasswordNotSafe'][-3]?>");
		obj.NewPassword.focus();
		return false;
	}
	re = /[0-9]/;
	if (!re.test(obj.NewPassword.value))
	{
		alert("<?=$Lang['PasswordNotSafe'][-4]?>");
		obj.NewPassword.focus();
		return false;
	}
	<?php } ?>
}
</script>

<form name="form1" method="post" action="updatepwd_update.php" onsubmit="return checkform(this)">
<div class="main_form">
<table class="form_table_v30">
<tr>
	<td class="field_title"><?=$i_general_class?></td>
	<td><?=$select_class?></td>
</tr>
<? if($targetClass) {?>
<tr>
	<td class="field_title"><?=$i_identity_student?></td>
	<td><?=$select_students?></td>
</tr>
<? } ?>

<? if($targetClass && sizeof($list)) {?>
<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$i_frontpage_myinfo_password_new?></td>
	<td><input name="NewPassword" type="password" class="textboxnum"/>
		<span class="tabletextremark">
		<?php if ($li->RetrieveUserInfoSetting("Enable", "PasswordPolicy",USERTYPE_STUDENT)) { ?>
			<br>(<?=$Lang['PasswordNotSafe'][-2]?>)
		<? } ?>
		<br><span class="tabletextremark"><?=$Lang['AccountMgmt']['PasswordRemark']?>
		</span>
	</td>
</tr>

<? } ?>
</table>


<div class="edit_bottom_v30">
<p class="spacer"></p>
<? if($targetClass && sizeof($list)) {?>
	<?= $linterface->GET_ACTION_BTN($button_save, "submit") ?>
	<? } ?>
	<?= $linterface->GET_BTN($button_close, "button", "self.close();") ?>
<p class="spacer"></p>
</div>		


</div>
<input type="hidden" name="msg" value="">
</form>


<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>