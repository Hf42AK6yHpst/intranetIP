<?php
//using : 

#######################################
#	
#	Date:	2017-10-16	Carlos
#			add api_key checking to allow public access if generated from eclass api.
#
#	Date:	2012-10-18	Carlos
#			fix Chinese file name monster code problem
#
#	Date:	2012-05-04	YatWoon
#			add $special_feature['download_attachment_add_auth_checking'] 
#
#	Date:	2012-03-16	YatWoon
#			add intranet_auth() ensure download the file within login session. [Case#2012-0316-1554-09132]
#
#######################################

$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$lfs = new libfilesystem();

# decrypt for the variables of $Module, $UserLogin, $UID, $RecordID
DecryptModuleFilePars($key);

//debug($Module, $UserLogin, $UID, $RecordID);
# check access right to view that announcement

if ($Module=="Announcement")
{
	$lo = new libannounce($RecordID);
	if(isset($api_key) && $api_key != ''){
		$decrypted_api_key = getDecryptedText($api_key);
		$call_from_api = $decrypted_api_key == $RecordID;
	}
	if ($lo->hasRightToView($UID) || $call_from_api)
	{
		$attachment_folder = $lo->Attachment;
		$voiceFile = $lo->VoiceFile;
		
		if ($voiceFile!="")
		{
			$AttachmentURL[] = array(basename($voiceFile), $school_domain."/home/download_attachment.php?target_e=".getEncryptedText($voiceFile));
		}
		
		if (trim($attachment_folder)!="")
		{
			 $attachment_path = "$file_path/file/announcement/".$attachment_folder.$RecordID;
			 if (file_exists($attachment_path))
			 {
			 	$attachment_array = $lfs->return_files($attachment_path);
			 	for ($i=0; $i<sizeof($attachment_array); $i++)
			 	{
			 		$AttachmentURL[] = array(basename($attachment_array[$i]), $school_domain."/home/download_attachment.php?target_e=".getEncryptedText($attachment_array[$i]));
			 	}
			 }
		}

		$RecordTitle = $lo->Title;
	} else
	{
		die ("You don't have the right to view the attachment(s) of the announcement!");
	}
}


$linterface = new interface_html("popup_index.html");
$linterface->LAYOUT_START();

echo "<b><font size='5'>".$RecordTitle."</font></b>";
echo "<p><font color='grey'>(".$i_Files_ClickToDownload.")</font></p>";
echo "<font size='+1'><ul>";
for ($i=0; $i<sizeof($AttachmentURL); $i++)
{
	echo "<p><li><a href='".$AttachmentURL[$i][1]."'>".$AttachmentURL[$i][0]."</a></li></p>";
}
echo "</ul></font>";
?>



<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>