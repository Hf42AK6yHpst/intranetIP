<?php
// editing by yat

$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

if(!($_SESSION["CanAccessUpdateStudentPwdPopup"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT($Lang['General']['AccessDenied']);
	$linterface->LAYOUT_STOP();
	exit;
}

intranet_auth();
intranet_opendb();

$li = new libdb();
$lc = new libeclass();
$lu = new libuser($targetID);
$lauth = new libauth();

$NewPassword = intranet_htmlspecialchars(trim($NewPassword));

$fieldname = "";
$result = array();

     $fieldname .= "DateModified = now(), LastModifiedPwd = NOW() ";

     $TargetPassword = $NewPassword;
     if ($intranet_authentication_method == "HASH")
     {
         $fieldname .= ", HashedPass = '".MD5(strtolower($lu->UserLogin).$TargetPassword.$intranet_password_salt)."'";
     }
     else
     {
         $fieldname .= ", UserPassword = '$TargetPassword'";
     }
     $_SESSION['eclass_session_password'] = $TargetPassword;
	
 	 $lauth->UpdateEncryptedPassword($lu->UserID, $TargetPassword);
	
     # Webmail
     $lwebmail = new libwebmail();
     if($lwebmail->has_webmail && !$plugin['imail_gamma']){
     	$result['webmail_changepassword'] = $lwebmail->change_password($lu->UserLogin,$TargetPassword,"iMail");
     }
    # iMail Gamma 
    if($plugin['imail_gamma'] && ($_SESSION['SSV_EMAIL_LOGIN']!=''&&$_SESSION['SSV_EMAIL_PASSWORD']!=''&&$_SESSION['SSV_LOGIN_EMAIL']!=''))
	{
		$IMap = new imap_gamma();
     	$imap_result = $IMap->change_password('',$TargetPassword);
     	if($imap_result)
     		$_SESSION['SSV_EMAIL_PASSWORD'] = $TargetPassword;
     	$result['gamma_changepassword'] = $imap_result;
    }
    
     # FTP management
     if ($plugin['personalfile'])
     {
         if ($personalfile_type == 'FTP' || $personalfile_type == 'LOCAL_FTP' || $personalfile_type == 'REMOTE_FTP')
         {
             include_once($PATH_WRT_ROOT."includes/libftp.php");
             $lftp = new libftp();
             $result['ftp_changepassword'] = $lftp->changePassword($lu->UserLogin,$NewPassword,"iFolder");
         }
     }

     if ($intranet_authentication_method == 'LDAP')
     {
         include_once($PATH_WRT_ROOT."includes/libldap.php");
         $lldap = new libldap();
         if ($lldap->isPasswordChangeNeeded())
         {
				$result['ldap_changepassword'] = $lldap->changePassword($lu->UserLogin,$NewPassword);
         }
     }
     

     $sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = ".$lu->UserID;
     $result['sql_update'] = $li->db_db_query($sql) or die ($sql.mysql_error());
     
     $newmail = "";
     
     if (!in_array(false,$result))
     {
         $lc->eClassUserUpdateInfoIP($lu->UserEmail, $lu->Title, $lu->EnglishName,$lu->ChineseName,$lu->FirstName,$lu->LastName, $lu->NickName, $TargetPassword, $lu->ClassName,$lu->ClassNumber, $newmail,$lu->Gender, $lu->ICQNo,$lu->HomeTelNo,$lu->FaxNo,$lu->DateOfBirth,$lu->Address,$lu->Country,$lu->URL,$lu->Info);
         $signal = "UpdateSuccess";
         
         # Send Ack to admin
         if($FromResetPassword)
         {
         	include_once($PATH_WRT_ROOT."includes/libemail.php");
			include_once($PATH_WRT_ROOT."includes/libsendmail.php");
		    $lsendmail = new libsendmail();
         	
		    $webmaster = get_webmaster();

		    # Send ACK Email
			list($ackMailSubject,$ackMailBody) = $lu->returnEmailNotificationData_HashedPw_ACK($lu->UserEmail, $lu->UserLogin);
			
			$result2 = $lsendmail->send_email($webmaster, $ackMailSubject,$ackMailBody,"");
         }
     }
     else
     {
         $signal = "UpdateUnsuccess";
     }
intranet_closedb();

header("Location: updatepwd.php?msg=$signal");
?>