<?php
## Using by :

################ Change Log [Start] #####################
#
#   Date:       2020-05-12 (Henry)
#               commentted $lclubsenrol->hasAccessRight($UserID) at eEnrolment logo condition	
#
#   Date:       2020-04-14 (Pun)
#               Changed elearning timetable popup width
#
#   Date:       2020-04-06 (Thomas)
#               Added Britannica Image Quest SSO
#
#   Date:       2020-03-30 (Pun)
#               Added elearning timetable
#
#   Date:       2020-03-06 (Thomas)
#               Changed the icons for Britannica SSO
#
#   Date:       2020-02-14 (Thomas)
#               Added mechanism to prevent multiple click on PL2 button
#
#	Date:		2019-10-15 (Philips)
#				Modified $name if ChineseName = "", use EnglishName
#
#   Date:       2019-10-09 (Tommy)
#               fixed: parent account show white screen
#               change checking for $childarr to $Libuser in my group
#
#   Date:       2019-09-25 (Tommy)
#               changed my group to only getting group that is not in Enrolment Result Time
#
#   Date:       2019-09-10 (Thomas)
#               Added Britannica SSO
#
#   Date:       2019-05-28 (Philips)
#               Modified SDAS with adding $libSDAS->isAccessGroupMember() for access right checking
#
#   Date:       2019-05-15 (Anna)
#               change MonthlyReportPIC to isMonthlyReportPIC for handling sectionPIC for CEES monthly report
#
#   Date:       2019-02-27 (Henry)
#               bug fix: eLibrary plus icon will change when mouseover eBooking icon [Case#E157535]
#
#   Date:       2019-02-15 (Isaac)
#               added ePCM admin/user checking to the rules on showing the eProcurement quick btn
#
#   Date:       2019-02-15 (Isaac)
#               fixed some quick btns text overflow issue in english ui by adding class 'indexquickbtnslim';
#
#   Date:       2019-02-14 (Isaac)
#               added convert quick btns' overflow number using convertOverflowNumber();
#
#   Date:       2019-01-02 (Anna)
#               - ADDED $sys_custom['eEnrolment']['HideEnrolForParentView']> hide enrollment button for parent
#
#   Date:       2018-12-20 (Isaac)
#               Added eProcurement quick btn
#
#	Date:		2018-11-23 (Carlos)
#				added checking of $sys_custom['eClassApp']['enableReprintCard'] to display Reprint Card icon.
#
#   Date:       2018-11-22 (Isaac)
#               Fixed eBooking non-admin $facilityCond syntax issue causing return no record.
#
#   Date:       2018-11-12 (Cameron)
#               consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
#
#   Date:       2018-09-11 (Paul) [ip.2.5.9.10.1]
#               add icon for webmail connection for customization $sys_custom['SSO_multi_platform_show_webmail']
#
#   Date:       2018-09-06 (Cameron)
#               temporary disable Block_Document and UnBlock_Document for HKPF
#
#   Date:       2018-09-04 (Cameron) [ip.2.5.9.10.1]
#               comment out extra unused element in getCalendarView for HKPF
#
#   Date:       2018-08-29 (Ivan) [ip.2.5.9.10.1]
#               hide ePost for parent for customization $sys_custom['ePost']['hiddenForParent']
#
#   Date:       2018-08-14 (Cameron) [ip.2.5.9.10.1]
#               don't show marquee for HKPF
#
#   Date:       2018-08-10 (Bill) [ip.2.5.9.10.1]
#               direct access - eReportCard (Kindergarten) - for admin and class teacher only
#
#   Date:       2018-08-06 (Cameron) [ip.2.5.9.10.1]
#               add parameter srcFrom in getCalendarView() for HKPF
#
#   Date:       2018-07-27 (Ivan) [ip.2.5.9.10.1]
#               added HKPF cust portal
#
#   Date:       2018-06-14 (Frankie)
#               added SSO for DSI
#
#   Date:       2018-05-04 (Anna)
#               added enrollment Committee Recruitment button
#
#   Date:       2018-04-24 (Pun) [ip.2.5.9.5.1]
#               added redirect to iPortfolio while is stand_alone
#
#   Date:       2018-03-22 (Bill) [ip.2.5.9.5.1]    [2018-0202-1046-39164]
#               2 quick buttons for direct access - eReportCard (Kindergarten) > Learning Zone / Input Lesson Score
#
#   Date:       2018-03-06 (Cameron)
#               show medical event list under medical student log [case #F133828]
#
#	Date:		2018-01-24 (Cameron)
#				change logo according to cust flag $sys_custom['medical']['CaringSystem'] [case #F121742]
#
#	Date:		2018-01-17 (Anna)
#				added MonthlyReportPIC in StudentDataAnalysisSystem
#
#	Date:		2017-12-18 (Cameron) [ip.2.5.9.1.1]
#				redirect to home/portal.php if $sys_custom['project']['Oaks'] is set
#
#	Date:		2017-10-12 (Bill) [ip.2.5.8.10.1]	[2017-1011-0940-46235]
#				Apply disable checking for students and parents ($sys_custom['eNotice']['eServiceDisableParent'] & $sys_custom['eNotice']['eServiceDisableStudent'])
#
#	Date:		2017-09-21 (Simon) [ip.2.5.8.10.1]
#				eBooking change the sql retrieve records under mgmt group by user login
#
#	Date:		2017-09-20 (Simon) [ip.2.5.8.10.1]
# 				change sql
#
#	Date:		2017-09-19 Henry [ip.2.5.8.10.1]
#				show PowerLesson2 (PL2.0) Logo if current user is teacher and student only
#
#	Date:		2017-09-18 (Bill) [ip.2.5.8.10.1]	[2017-0908-1248-12235]
#				Added apply leave icon to the top right section for Class Teacher
#
#	Date:		2017-08-08 (Paul)
#				Remove Scrabble Fun form to prevent duplicating form
#
#	Date	:	2017-06-27	[Villa]
#				Modicated SDAS icon - add monitoring Group Access right
#
#	Date	:	2017-06-22 [Frankie]
#				Add eForm Reminder
#
#	Date	:	2017-06-13 [Villa]
#				Add ebooking quick btn
#
#   Date    :   2017-05-11 [Paul] [ip.2.5.8.6.1]
#               redirect to ncs/index.php if $sys_custom['project']['NCS'] is set and do not want to display Intranet
#
#   Date    :   2017-04-19 [Paul] [ip.2.5.8.4.1]
#               added PowerPad Lite SSO icon
#
#   Date    :   2017-03-23 [Siuwan] [ip.2.5.8.4.1]
#               updated PowerLesson 2 quick button
#
#   Date    :   2017-02-16 [Thomas] [ip.2.5.8.4.1]
#               Use $web_protocol instead of $_SERVER['HTTPS'] when building $powerLesson2_path for PL2.0
#
#	Date	:	2016-10-18 [Pun] [ip.2.5.7.10.1]
#				fixed PL2.0 hardcoded redirect protocol
#
#	Date	:	2016-10-17 [Cameron] [ip.2.5.7.10.1]
#				add $plugin['eLibraryPlusOne'], link to elibplus2 (new windows tab) if it's not set, else link to elibplus
#
#	Date	: 	2016-10-13 [Pun] [ip.2.5.7.10.1]
#				changed icon for PL2.0
#
#	Date	: 	2016-10-05 [Pun] [ip.2.5.7.10.1]
#				added name attribute for PL2.0 popup
#
#	Date	: 	2016-10-03 [Jason] [ip.2.5.7.10.1]
#				added gmail icon connection by SSO Google App
#
#	Date	: 	2016-07-22 [Pun]
#				added PL2.0 icon
#
#	Date	: 	2016-07-20 [Kenneth]
#				added MDM quick icon
#
#	Date	: 	2016-07-11 [Cameron] [ip.2.5.7.7.1]
#				show write-off approval icon for user who are Resource Management Group Leader ($inventory_access_level == 2)
#				in eInventory module [case #K98549]
#
#	Date	:	2016-07-06 [Ivan] [ip.2.5.7.7.1]
#				added flag $special_feature['hide_iMail'] to hide iMail portal shortcut icon
#
#	Date	:	2016-06-21 [Paul] [ip.2.5.7.7.1]
#				redirect to cc_eap/index.php if $sys_custom['project']['centennialcollege'] is set and do not want to display Intranet
#
#	Date	:	2016-06-03 [Kenneth] [ip.2.5.7.7.1]
#				Added eNotice Logo for approval User if at least 1 notice pending approval
#
#	Date	:	2016-04-07 [Pun] [ip.2.5.7.4.1]
#				Added general skin for Student Analysis Data System
#
#	Date	:	2016-02-29 [Pun] [ip.2.5.7.3.1] [90826]
#				Added flag to control Student Analysis Data System access right block subject panel, class teacher, subject teacher
#
#	Date	:	2016-02-17 [Pun] [ip.2.5.7.3.1] [90826]
#				Change Student Analysis Data System access right
#
#	Date	:	2016-01-19 [Carlos]
#				Modified to display Document Routing icon for staff user type, not only display when have new routings.
#
#	Date	:	2015-12-11 [Cameron]
#				redirect to home/portal.php if $sys_custom['project']['Amway'] is set
#
#	Date	:	2015-08-25 [Bill]	[2015-0728-1228-52073]
#				School News > Group - Display group name language according to UI
#
#	Date	:	2015-08-21 [Siuwan]
#				Add PowerFlip Logo shortcut icon
#
#	Date	:	2015-08-18 [Pun]
#				Added cust icon for Student Analysis Data System
#
#	Date	:	2015-08-13 [Omas]
#				Added $sys_custom['portal']['schoolnews_cust'] for cust school news part(middle part of portal)
#
#	Date	:	2015-08-03 [Bill]	[2015-0728-1228-52073]
#				Display identity group in School News > Group
#
#	Date	:	2015-07-28 [Bill]	[2015-0611-1642-26164]
#				Add Rehabilitation Approval Logo shortcut icon
#
#	Date	:	2015-07-16 [Pun]
#				Add Student Data Analysis System Logo shortcut icon
#
#	Date	:	2015-06-16 [Jason] [ip.2.5.6.7.1]
#				fix session_register method to support php 5.4+
#
#	Date	:	2015-06-04 [Siuwan]
#				Add Flipped Channels Logo shortcut icon
#
#	Date	:	2015-03-19 [Cameron]
#				Add flag $sys_custom['HideeClass'] and $sys_custom['HideNewsInFrontPage'] to hide eClass and news
#
#	Date	:	2014-08-21 [Pun]
#				Add Medical student log details view for teacher
#
#	Date	:	2014-07-04 [YatWoon]	> ip.2.5.5.8.1
#				Improved: add config for default right menu [Case#V61868]
#
#	Date	:	2014-05-25 [Pun]
#				Show Medical Notice Logo shortcut icon if has access right
#
#	Date	:	2014-05-22 [Pun]
#				Modify Medical Notice Logo shortcut icon
#
#	Date	:	2014-05-21 [Siuwan]
#				Add ePost Logo shortcut icon and rename redundant img id of other Logo shortcut icon
#
#	Date	:	2014-04-03 [Pun]
#				Modify Medical Notice Logo shortcut icon
#
#	Date	:	2014-04-02 [Pun]
#				Modify Medical Notice Logo shortcut icon
#
#	Date	:	2014-04-01 [Pun]
#				Modify Medical Notice Logo shortcut icon
#
#	Date	:	2014-03-28 [Pun]
#				Added Medical Notice Logo shortcut icon
#
#	Date	:	2014-02-21 [Ivan]
#				Added apply leave icon to the top right section
#
#	Date	:	2013-12-23 [Henry]
#				Add eLib plus ICON to Big Icon section
#
#	Date	:	2013-12-13 [Henry]
#				Add checking for $warning_day_period. If 0, disable to popup an inventory warranty alert
#
#	Date	:	2013-12-09 [Ivan]
#				Fixed: Rolled back the dynamic size thickbox
#
#	Date	:	2013-12-04 [Carlos]
#				Modified js getGammaNewMailNumber(), call aj_get_mail_quota_alert.php to check quota usage
#
#	Date	:	2013-12-03 [Ivan]
#				improved: risky account thickbox changed to dynamic size
#
#	Date	:	2013-11-11 [YatWoon]
#				Add flag checking $special_feature['eNotice']['QuickIconDisplay'], don't hide eCircular icon [Case#2013-0913-1452-54035]
#
#	Date	:	2013-09-12 [Ivan]
#				modified $showLibraryPeriodicalOrderAlert logic
#
#	Date	:	2013-09-12 [YatWoon]
#				added quick icon for Digital Archive (requested by UCCKE)
#				temporary for UCCKE only $sys_custom['DA_quick_icon']
#
#	Date	:	2013-09-07 [Ivan]
#				added $showLibraryPeriodicalOrderAlert logic
#
#	Date	:	2013-07-03 [YatWoon]
#				add checking for $sys_custom['OnlineRegistry']
#
#	Date	:	2013-04-19 [yuen]
#				correct the wrong path of alumni default folder, now direct to eCommunity
#
#	Date	:	2013-03-08 [yuen]
#				alert system admin if there is any user account using loginID as password
#  				p.s. can be disabled by setting $sys_custom["disable_account_alert"] = true;
#
#	Date	:	2012-12-18 [Ivan] [2012-1217-1602-52156]
#				changed $HeightUsed to height="100%" for Chrome also
#				changed id="EventContentDiv" from <span> to <div>
#
#	Date	:	2012-12-07 [YatWoon]
#				Add flag checking $special_feature['eCircular']['QuickIconDisplay'], don't hide eCircular icon [Case#2012-1206-1651-18073]
#
#	Date	:	2012-12-06 [YatWoon]
#				Add flag checking $special_feature['HideShowAllEventIcon'] [Case#2012-1206-1051-42071]
#
#	Date	:	2012-10-30 [Jason]
#				Added a new form 'scrabble_form' for the access of scrabble fun corner
#
#	Date	:	2012-10-18 [Ivan]
#				Added Document Routing shortcut icon
#
#	Date	:	2012-08-29 [Jason]
#				Add New Classroom Div for the 190 project - wrong wah san by checking 'WWS_eLearningProject'
#				Add JS jAJAX_GO_ECLASS2()
#
#	Date	:	2012-08-28 [YatWoon]
#				Add "UpdateStudentPwdPopUp" shortcut icon
#
#	Date	: 	2012-05-14 [Jason]
#				Improved: block the normal access of some accounts for ip25-tc-demo2 site. It is used in PowerLesson App only
#
#	Date	:	2012-04-05 [Yuen]
#				specially use to block the logins of the user account created
#
#	Date	:	2012-04-03 [YatWoon]
#				Improved: Add shortcut icon for "Swimming Gala"
#
#	Date	:	2012-03-28 [YatWoon]
#				Fixed: popup an empty inventory warranty alert
#
#	Date	:	2011-12-09 [Carlos]
#				modified js getGammaNewMailNumber() added alert user msg if could not connect mail server
#
#	Date	: 	2011-11-17 [Jason]
#				add powerlesson shortcut for the view of active PowerLesson in student view
#
#	Date	: 	2011-04-08 [Carlos]
#				removed $header_onload_js from <body> onLoad event, moved it to be executed in $(document).ready()
#
#	Date	:	2011-04-06 [Carlos]
#				modified the condition checking to display iMail quick icon for UCCKE $sys_custom['uccke_parent_quick_icon']
#
#	Date	:	2011-03-24 [YatWoon]
#				update new bubble, use curl to check url is exists or not
#
#	Date	:	2011-03-23 [Yuen]
#				disabled to call returnNumNewMessage() for counting new mails if iMail Gamma/Plus is active
#
#	Date	:	2011-03-15 [Henry Chow]
#				only display the bubble if $url exists & readable (reachable)
#
#	Date	:	2011-02-24 [ YatWoon]
#				add $special_feature['portal_new_features_bubble'] to on/off the new features bubble
#
#	Date	:	2011-02-09	[Henry Chow]
#				revise the bubble [broken in Firefox]
#
#	Date	:	2011-02-02	[Henry Chow]
#				add bubble message (eClass Update message)
#
#	Date	:	2011-02-02	[YatWoon]
#				update javascript function for popup new window of eInventory warranty expire reminder to the bottom page
#				And update the path of item_warranty_expiry_warning.php page
#
#	Date	:	2010-10-11 [Ronald]
#				remove duplicate Query executed in eInventory (#884,#892)
#				improve the no. of query executed in School Calendar (#1213,#1227,#1582)
#
#	Date	:	2010-10-11 [YatWoon]
#				Re-order quick icon
#
#	Date	:	2010-10-08 [YatWoon]
#				UCCKE customization: parent login, some quick icon MUST display ($sys_custom['uccke_parent_quick_icon'])
#
#	Date	:	2010-10-04 [YatWoon]
#				Add quick icon for SLS  (2010-10-06 cancelled this)
#
#	Date	:	2010-09-20 [Ivan]
#				Added eEnrolment Trial Period Logic
#
#	Date	:	2010-09-13	[Henry Chow]
#				pass AcademicYearID to function awardPunishmentRecordCount() in order to return records of current year
#
#	Date	:	2010-09-02	[Carlos]
#				Changed to ajax way to get number of new mails on iMail Gamma icon;
#				Get iMail Gamma identity access right from lib instead of imap_gamma.
#
#	Date	:	2010-08-19  [Carlos]
#				Add checking on identity and access right to control showing iMail Gamma icon
#
#	Date	:	2010-08-16	[Carlos]
#				Show number of new mails on iMail Gamma icon
#
#	Date	:	2010-08-13  [Carlos]
#				change checking flags to control to show either iMail or iMail Gamma icon
#
#	Date	:	2010-08-04	YatWoon
#				update eSports shortcut link (student mode)
#
#	Date	:	2010-07-22 [YatWoon]
#				update bottom logo display
#
#	Date 	:	2010-06-23 [Yuen]
#	Details	:	Improved to show the div in Chrome which failed before due to use of "height:100%" in div
#
#	Date 	:	2010-06-04 [Marcus]
#	Details	:	Add flag to show hide imail icon
#
#	Date 	:	2010-06-04 [Yuen]
#	Details	:	do not load eClass, eCommunity & eHomework on the right hand side section;
#               instead, load one using AJAX when onload in order to save memory consumption by this page
#
#	Date:	2010-05-05	[YatWoon]
#			Add "School holiday" in small calendar
#
## - 2009-12-22 [YatWoon]
##	 Distinct the Group to prevnet duplicate Group selection display
#
################ Change Log [End] #####################

//$DebugMode = true;
ini_set("memory_limit", "300M");
$PATH_WRT_ROOT = "../";
//$NoLangOld20 = true;
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (isset($_SESSION['SSV_isiPortfolio_standalone']) && $_SESSION['SSV_isiPortfolio_standalone'] && $stand_alone['iPortfolio']){
    header("location: /home/portfolio/");
    exit;
}
if ($_SESSION["platform"]=="KIS")
{
    if ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])
    {
        header("location: /home/eAdmin/AccountMgmt/StaffMgmt/");
    } else
    {
        header("location: /kis/");
    }
    die();
}

if ($sys_custom['project']['CourseTraining']['IsEnable']) {
    header("location: /home/portal.php");
    exit;
}

if ($sys_custom['LivingHomeopathy']) {
    header("location: /templates/" . $sys_custom['Project_Label']. "/portal.php");
    exit;
}
if ($sys_custom['PowerClass']) {
    intranet_opendb();
    $isConsoleAdmin = false;
    if ($UserType == USERTYPE_STAFF) {
        $libdb = new libdb();
        $sql = "select * from GENERAL_SETTING where Module='SchoolSettings'";
        $result = $libdb->returnArray($sql);
        $AdminUser = $result[0]['SettingValue'];
        if (!empty($AdminUser)) {
            $Arr_AdminUser = explode(",", $AdminUser);
            if (in_array($_SESSION["UserID"], $Arr_AdminUser)) {
                $isConsoleAdmin = true;
            }
        }
    }
    if ($isConsoleAdmin) {
        $AcademicYearID = Get_Current_Academic_Year_ID();
        if ($AcademicYearID > 0) {
            $PowerClassLink = "/home/" . $sys_custom['Project_Label']. "/";
        } else {
            $PowerClassLink = "/home/" . $sys_custom['Project_Label']. "/#setup";
        }
    } else {
        $PowerClassLink = "/logout.php";
    }
    intranet_closedb();
    header("location: " . $PowerClassLink);
    exit;
}

if ($sys_custom['project']['centennialcollege']&&$sys_custom['centennialcollege']['preventDisplayIP25']&&$_SESSION['UserID']!='1'){
    header("location: /cc_eap/index.php");
}

if ($sys_custom['project']['NCS']){
    header("location: /ncs/index.php");
}

StartTimer("loginTimer");

# specially use to block the logins of the user account created
if (is_array($special_feature["login_control"]) && in_array($UserID, $special_feature["login_control"]))
{
    header("location: /logout.php");
    die();
}


if (!isset($_SESSION['Is_First_Login']))
{
    //session_register("Is_First_Login");
    $_SESSION['Is_First_Login'] = 1;

    //UPDATE_CONTROL_VARIABLE();
    # log essential session for checking

    if ($DebugMode)
    {
        $SYS_DEBUG_SESSION["UserID"] = null;
        $SYS_DEBUG_SESSION["UserType"] = null;
        /*$SYS_DEBUG_SESSION["inventory"]["role"] = null;
         $SYS_DEBUG_SESSION["inventory"]["admins"]["user"] = null;
         $SYS_DEBUG_SESSION["inventory"]["admins"]["group"] = null;
         $SYS_DEBUG_SESSION["inventory"]["right"] = null;
         $SYS_DEBUG_SESSION["inventory"]["admins"]["groupTest"] = null;
         */
        $SYS_DEBUG_SESSION["isTeaching"] = null;

        function DebugLogSessions($SYS_DEBUG_SESSION, $SYS_SESSION)
        {
            foreach ($SYS_DEBUG_SESSION AS $SYS_Key => $SYS_Key_Value)
            {
                if (!is_array($SYS_Key_Value))
                {
                    //debug ($SYS_Key, $SYS_Key_Value);
                    $SYS_DEBUG_SESSION[$SYS_Key] = $SYS_SESSION[$SYS_Key];
                } else
                {
                    $SYS_DEBUG_SESSION[$SYS_Key] = DebugLogSessions($SYS_DEBUG_SESSION[$SYS_Key], $SYS_SESSION[$SYS_Key]);
                }
            }

            return $SYS_DEBUG_SESSION;
        }

        $_SESSION["DEBUG_ARRAY"] = DebugLogSessions($SYS_DEBUG_SESSION, $_SESSION);
    }
}

if (!(isset($ck_intranet_justlogin) && $ck_intranet_justlogin != 0))
{

    setcookie("ck_intranet_justlogin",1);
    $ck_intranet_justlogin = 1;
}
else
{
    $ck_intranet_justlogin = 0;
}


## hard code to NOT do browser checking now
$sys_cutom['bypass_browser_checking'] = true;

# just for internal use!!!
if ($sys_custom["check_kanhan_sms_quota"])
{
    include_once("../test/check_kanhan.php");
}

intranet_auth();
intranet_opendb();

# Please do not comment below IF case
# it is used to block the access of IP25 via Normal Porcess since those accounts are only allowed to access via iPad App
# For PowerLesson App use only
if( $_SERVER['HTTP_HOST'] == 'ip25-tc-demo2.eclass.com.hk' &&
    in_array($_SESSION['UserID'], array(5389,5390,5391,5392,5393,5394,5395,5396)) )
{
    header("Location: ../logout.php");
    die();
}
//debug_r($_SESSION);

# Bubble Message (display eClass Update message)
# only display the bubble when first login

$client_region = get_client_region();			# get client's region
$permittedRegion = array("zh_HK", "zh_MO");		# region allowed to display the bubble message

//if($_SESSION['Is_First_Login'] && $special_feature['portal_new_features_bubble'])
if($_SESSION['Is_First_Login'] && in_array($client_region, $permittedRegion))
{


    $libdb = new libdb();

    $version = Get_IP_Version();

    $display = displayBubbleMessage($version, $UserID);

    if($display) {			# display the bubble if user did not read it / choose "remind me later" before
        # get admin user
        $sql = "SELECT * FROM GENERAL_SETTING where Module='SchoolSettings'";
        $result = $libdb->returnArray($sql);
        $AdminUser = $result[0]['SettingValue'];
        $AdminUserAry = explode(',',$AdminUser);

        $url = "";
        $bubble = "";

        if(in_array($UserID, $AdminUserAry)) {	# system admin
            //$url = "http://192.168.0.146:31002/home/abc.htm";
            if($intranet_session_language=="en") {
                $url = "http://support.broadlearning.com/doc/help/central/bubble_news/news.html#CurrentNewsAdminEN";
            }
            else {
                $url = "http://support.broadlearning.com/doc/help/central/bubble_news/news.html#CurrentNewsAdminHK";
            }
        }
        # hide teacher role checking, since only display for System Admin [20110310 by Henry Chow]
        /*
         else if($_SESSION['UserType']==USERTYPE_STAFF) {	# teacher
         if($intranet_session_language=="en")
         $url = "http://support.broadlearning.com/doc/help/central/BubbleNews.html#CurrentNewsUserEN";
         else
         $url = "http://support.broadlearning.com/doc/help/central/BubbleNews.html#CurrentNewsUserHK";
         }
         */

         # check url exists or not
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, $url);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
         curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
         $head = curl_exec($ch);
         $conn_error = curl_error($ch);

         //if($url!="" && @fopen($url,"r")) {			# if $url exists & readable
         if($url!="" && empty($conn_error)) {			# if $url exists & readable
             $bubble = '
				<script language="javascript">
                 
					function emptyContent(layername)
					{
						if(document.getElementById(layername)) document.getElementById(layername).innerHTML = "";
					}
                 
					function hideSpan(layername) {
						if(document.getElementById(layername)!=undefined)
							document.getElementById(layername).style.visibility = "hidden";
					}
                 
                 
					function goCancel() {
                 
						cancelAjax = GetXmlHttpObject();
                 
						if (cancelAjax == null)
						{
							alert (errAjax);
							return;
						}
                 
						var url = \'ajax_cancel_bubble.php\';
						var PostValue = "version='.$version.'";
						cancelAjax.onreadystatechange = function() {
							if (cancelAjax.readyState == 4) {
								//alert(cancelAjax.responseText);
							}
						    
						};
						cancelAjax.open("POST", url, true);
						cancelAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
						cancelAjax.send(PostValue);
						    
						hideSpan(\'sub_layer_eclass_update_alert_v30\');
					}
						    
					function GetXmlHttpObject()
					{
						var xmlHttp = null;
						try
						{
							// Firefox, Opera 8.0+, Safari
							xmlHttp = new XMLHttpRequest();
						}
						catch (e)
						{
							// Internet Explorer
							try
							{
								xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
							}
							catch (e)
							{
								xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
							}
						}
						return xmlHttp;
					}
						    
					function DisplayPosition(layername) {
						    
						//var windowWidth = $(window).width();
						if( typeof( window.innerWidth ) == \'number\' ) {
							//Non-IE
							myWidth = window.innerWidth;
						} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
							//IE 6+ in \'standards compliant mode\'
							myWidth = document.documentElement.clientWidth;
						} else if( document.body && ( document.body.clientWidth) ) {
							//IE 4 compatible
							myWidth = document.body.clientWidth;
						}
						    
						var leftLoc = myWidth - 580;
						    
						document.getElementById(layername).style.left = leftLoc;
						    
					}
						    
					function getPosition(obj, direction)
					{
						var objStr = "obj";
						    
						var pos_value = 0;
						while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
						{
							pos_value += eval(objStr + "." + direction);
							objStr += ".offsetParent";
						}
						    
						return pos_value;
					}
						    
					function goAnnounce()
					{
						self.location.href  = "/home/eAdmin/GeneralMgmt/schoolnews/new.php?publishEclassNews=1";
					}
						    
				</script>
			';

             $bubble .= '
				<div class="sub_layer_board_v30" id="sub_layer_eclass_update_alert_v30" style="visibility: visible; z-index:9999999999;top:20px;">
					<span class="bubble_board_01_v30">
						<span class="bubble_board_02_v30">
							<em><img src="/images/'.$LAYOUT_SKIN.'/addon_tools/sub_layer_arrow.png" width="16" height="12"></em>
						</span>
					</span>
					<span class="bubble_board_03_v30">
						<span class="bubble_board_04_v30">
							<iframe width="94%" height="280" frameborder="0" src="'.$url.'"></iframe>
							<p>
							<div class="edit_bottom">
								<input type="button" class="formsmallbutton" onClick="javascript:goCancel();emptyContent(\'newFeatureSpan\')" name="closeBtn" id="closeBtn" value="'.$Lang['General']['Close'].'"   onMouseOver="this.className=\'formsmallbuttonon\'" onMouseOut="this.className=\'formsmallbutton\'"/>
								    
								<input type="button" class="formsmallbutton" onClick="javascript:goAnnounce();javascript:hideSpan(\'sub_layer_eclass_update_alert_v30\');emptyContent(\'newFeatureSpan\')" name="announceBtn" id="announceBtn" value="'.$Lang['General']['Announce'].'"   onMouseOver="this.className=\'formsmallbuttonon\'" onMouseOut="this.className=\'formsmallbutton\'"/>
								    
								<input type="button" class="formsmallbutton" onClick="javascript:hideSpan(\'sub_layer_eclass_update_alert_v30\');emptyContent(\'newFeatureSpan\')" name="remindBtn" id="remindBtn" value="'.$Lang['General']['RemindMeLater'].'"   onMouseOver="this.className=\'formsmallbuttonon\'" onMouseOut="this.className=\'formsmallbutton\'"/>
							</div>
						</span>
					</span>
					<span class="bubble_board_05_v30"><span class="bubble_board_06_v30"></span></span>
				</div>
								    
				<script language="javascript">
					DisplayPosition(\'sub_layer_eclass_update_alert_v30\');
				</script>
			';


             //echo $bubble;
         }
    }


}






include_once($PATH_WRT_ROOT."includes/libuser.php");

include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");

include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."includes/libannounce2007a.php");

//$memory_init2 = memory_get_usage();

if (!$sys_custom["disable_account_alert"] && $_SESSION['SSV_PRIVILEGE']['schoolsettings']["isAdmin"] && $_SESSION['Is_First_Login'])
{

    include_once($PATH_WRT_ROOT."includes/libauth.php");
    $li = new libauth();

    $rows = $li->getProtentialRiskByPassword();

    $alertAdminForAccountPassword = (sizeof($rows)>0);
}
/*
 if (!isset($_SESSION['generated']))
 $_SESSION['generated'] = time();
 else { // if the session is not allowed to live more, regenerate it
 if (time() - $_SESSION['generated'] > ini_get('session.gc_maxlifetime'))
 $_SESSION = array('generated' => time());
 }
 */

if ($sys_cutom['bypass_browser_checking'] == false && $skip_browser_checking != 1) { ?>
<!-- Broswer Checking -->
<script type="text/javascript" src="../templates/brwsniff.js"></script>
<script language="javascript">
/*
alert('is_ie: '+is_ie);
alert('ie7_below: '+ie7_below);
alert('is_ie7: '+is_ie7);
alert('ie7_up: '+ie7_up);

alert('is_ff: '+is_ff);
alert('is_ff3: '+is_ff3);
alert('ff3_or_up: '+ff3_or_up);
alert('ff3_below: '+ff3_below);
alert('other_broswer: '+other_broswer);
alert('has_flash: '+has_flash);
alert('flash_version: '+flash_version);
*/

if (ie7_below==true || ff3_below==true || other_broswer==true || has_flash==false || flash_version < 10)
{
	// Below IE 7, Firefox 3, using other broswer, or flash version older than 10 => Forward to alert page

	var browser_problem = 0;
	var flash_problem = 0;

	if (ie7_below==true || ff3_below==true || other_broswer==true)
		browser_problem = 1;
	if (has_flash==false || flash_version < 10)
		flash_problem = 1;

	location.href = "alert_browser.php?browser_problem=" + browser_problem + "&flash_problem=" + flash_problem;
}
else if (ie7_up==true || (is_ff3==false && ff3_or_up==true))
{
	// Above IE 7 or Firefox 3 => pop up alert
	if ("<?=$_SESSION['Is_First_Login']?>" == "1")
		alert("<?=$Lang['Portal']['BrowserChecking']['BrowserTooNew']?>");
}
</script>
<!-- End of Broswer Checking -->
<? } ?>
<?
$top_menu_mode = TOP_MENU_MODE_eService;
# Standalone System redirection
if ($eclass_standalone_module == "CampusTV")
{
    header("Location: /home/plugin/campustv/standalone.php");
    exit();
}

# Alumni user redirection
if ($_SESSION['UserType'] == USERTYPE_ALUMNI)
{
    include($PATH_WRT_ROOT."plugins/alumni_conf.php");
	$url = "/home/eCommunity/group/index.php?GroupID=$alumni_GroupID";
    header("Location: $url");
    exit();
}

include_once($PATH_WRT_ROOT."includes/libportal.php");

include_once($PATH_WRT_ROOT."includes/libinterface.php");
$linterface = new interface_html();


$lu = new libuser2007($UserID);
$lportal = new libportal();
$li = new libcalevent2007($ts,$v);
$la = new libannounce2007();
$lb = new libfilesystem();
$lcycleperiods = new libcycleperiods();


if ($extra_setting['show_process_time'])
{
	//debug("memory used in creating library instances: <font color='blue' size='3'>".number_format((memory_get_usage()-$memory_init2)/1024)."KB (".number_format((memory_get_usage()-$memory_init2)/1024/1024, 1)."MB)");
}
// debug_r($_SESSION);


# Improved to show the div in Chrome which failed before due to use of "height:100%" in div
//2012-1217-1602-52156
//$HeightUsed = (strstr($_SERVER['HTTP_USER_AGENT'], "MSIE")) ? "height:100%;" : "";
//chrome no need to set height=100% after Version 24.0.1312.52
//$HeightUsed = (strstr($_SERVER['HTTP_USER_AGENT'], "MSIE") || strstr($_SERVER['HTTP_USER_AGENT'], "Chrome")) ? "height:100%;" : "";
$HeightUsed = (strstr($_SERVER['HTTP_USER_AGENT'], "MSIE")) ? "height:100%;" : "";


####################################################################################################
## User Information
####################################################################################################
$name = ($intranet_session_language=="en"? $lu->EnglishName: $lu->ChineseName);
if(trim($name) == ''){
	$name = ($intranet_session_language=="en"? $lu->ChineseName: $lu->EnglishName);
}
$CurDate = date("Y.m.d (D)");
$CurDateForEventList = date("Y-m-d");
$CurCycleDate = $lcycleperiods->getCycleDayStringByDate(date("Y-m-d"));

####################################################################################################
## Message of Day [Marquee]
####################################################################################################
# Marquee
$motd = $lb->convertAllLinks($lb->file_read($intranet_root."/file/motd.txt"));
if ($sys_custom['project']['HKPF']) {
    $motd = '';
}
if($motd<>"")
{
	$scroller = "<iframe src='msg_of_day.php' frameborder='0' marginheight='0' marginwidth='0' height='14' width='100%' scrolling='no'></iframe>";
	$scrollerTable ="
						<tr>
								<td>
								<table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
								<tr>
									<td height=\"5\"><img src=\"/images/2009a/10x10.gif\" width=\"10\" height=\"5\"></td>
								</tr>
								<tr>
									<td height=\"25\" align=\"center\" valign=\"top\">
									<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
									<tr>
										<td width=\"4\" height=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/scrolltext/board_01.gif\" width=\"4\" height=\"4\"></td>
										<td height=\"4\" bgcolor=\"#FFF299\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"4\" height=\"4\"></td>
										<td width=\"4\" height=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/scrolltext/board_03.gif\" width=\"4\" height=\"4\"></td>
									</tr>
									<tr>
										<td width=\"4\" bgcolor=\"#FFF299\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"4\" height=\"4\"></td>
										<td bgcolor=\"#FFF299\" class=\"indexscrolltext\" valign=\"middle\" title=\"{$motd}\" >{$scroller}</td>
										<td width=\"4\" bgcolor=\"#FFF299\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"4\" height=\"4\"></td>
									</tr>
									<tr>
										<td width=\"4\" height=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/scrolltext/board_07.gif\" width=\"4\" height=\"4\"></td>
										<td height=\"4\" bgcolor=\"#FFF299\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"4\" height=\"4\"></td>
										<td width=\"4\" height=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/scrolltext/board_09.gif\" width=\"4\" height=\"4\"></td>
									</tr>
									</table>
									</td>
								</tr>
								</table>
								</td>
							</tr>
							";
}

####################################################################################################
## Build right menu [eClass List (default) / Homework List / Group List]
####################################################################################################
# $ListMenu
//$ListType=2;
$ListType = $special_feature['PortalDefaultRightMenu'] ? $special_feature['PortalDefaultRightMenu'] : 5;

if ($ListType == 1 && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"])
{
	# Homework List
	/* disabled by Yuen on 2010-06-04
	include_once($PATH_WRT_ROOT."includes/libhomework.php");
	include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
	$lh = new libhomework2007();
	$ListContent = $lh->displayIndex($classID,$UserID);
	*/
	$header_onload_js = "jAJAX_GO_HOMEWORK(document.ListForm);"; // load eClass news using AJAX
}
else if ($ListType == 2)
{
	# Groups
	/* disabled by Yuen on 2010-06-04
	$ListContent = $lu->displayGroupPage();
	*/
	$header_onload_js = "jAJAX_GO_COMMUNITY(document.ListForm);"; // load eClass news using AJAX
}
else if ($ListType ==4)
{
	$header_onload_js = "jAJAX_GO_TEXTBOOK(document.ListForm);"; // load iTextbook using AJAX
}
else
{
	# eClass
	/* disabled by Yuen on 2010-06-04
	include_once($PATH_WRT_ROOT."includes/libeclass40.php");
	include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
	$le = new libeclass2007();

	if ((true) || (!$lu->isParent()))
	{
		$ListContent  = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
		//$ListContent .= $le->displayUserEClass12($lu->UserEmail,1);
		$ListContent .= $lportal->displayUserEClass($lu->UserEmail,1);
		$ListContent .= "</table>";
	}
	else
	{
		$ListContent  = "	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
		$ListContent .= "	<tr>
									<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
									<td width=\"90%\" class=\"indextabclassiconoff\" >$i_no_record_exists_msg</td>
									<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
								</tr>
							";
		$ListContent .= "</table>";
	}
	*/
	if ($sys_custom['HideeClass']) {
		$header_onload_js = "";
	}
	else {
		$header_onload_js = "jAJAX_GO_ECLASS(document.ListForm);"; // load eClass news using AJAX
	}
}



####################################################################################################
## Build shortcut icon array
####################################################################################################
$LogoArr = array();
$defultIconDigitLimit = 2;
$intranet_session_language=="en"? $indexquickbtnslimClass="class='indexquickbtnslim'":$indexquickbtnslimClass="";

###################################################################
# iAccount Quick Icon [START]
###################################################################
# UCCKE customization ($sys_custom['uccke_parent_quick_icon'])
if($sys_custom['uccke_parent_quick_icon'] && $UserType==USERTYPE_PARENT)
{
	$iAcc_path = "/home/iaccount/account/";

	$LogoArr["iAccount"] = "
						<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_iaccount','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_iAccount_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$iAcc_path}'\"  > \n
						<tr> \n
							<td width=\"41\" height=\"32\"><a href=\"{$iAcc_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_iAccount_off.gif\" name=\"q3_iaccount\" border=\"0\" id=\"q3_iaccount\" /></a></td> \n
							<td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
						</tr> \n
						<tr> \n
							<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"{$iAcc_path}\" class=\"indexquickbtn\" >{$Lang['Header']['Menu']['iAccount']}</a></td> \n
						</tr> \n
						</table> \n
    					";
}

###################################################################
# iAccount Quick Icon [END]
###################################################################

#####################################################
##### Customization of webMail Link (#144094)
#####################################################
if($sys_custom['SSO_multi_platform_show_webmail'] && isset($sys_custom['SSO_multi_platform_show_webmail_link']) && $sys_custom['SSO_multi_platform_show_webmail_link'] !=""){
	 //$mail_count = $iNewCampusMail;
	 $mail_path = "imail";
	 $mail_href = "javascript:newWindow('".$sys_custom['SSO_multi_platform_show_webmail_link']."',8)";
	 $mail_txt	= "Webmail";
	 $mail_onclick	= "onclick=\"newWindow('".$sys_custom['SSO_multi_platform_show_webmail_link']."', 8)\"";
	 $mail_count = convertOverflowNumber($mail_count, $defultIconDigitLimit);
	 $LogoArr["ssoWebMail"] = "
									<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q1','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_imail_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" {$mail_onclick}  > \n
									<tr> \n
										<td width=\"41\" height=\"32\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_imail_off.gif\" name=\"q1\" border=\"0\" id=\"q1\"></a></td> \n
										<td width=\"29\" align=\"center\"><a href=\"{$mail_href}\" class=\"indexquicklink\">{$mail_count}</a>&nbsp;&nbsp;</td> \n
		
									</tr> \n
									<tr> \n
										<td align=\"center\" colspan=\"2\" valign=\"middle\"><a href=\"{$mail_href}\" class=\"indexquickbtn\" >{$mail_txt}</a></td> \n
									</tr> \n
									</table> \n
			    					";
}
#####################################################
##### Customization of webMail Link (#144094) [END]
#####################################################

########################
##### Campus mail / iMail
########################
# check any new mail for both mail (Campus mail / iMail)
/* commented because the checking will be performed if $access2campusmail is set
if ($_SESSION['special_feature']['imail'])
	$iNewCampusMail = $lportal->returnNumNewMessage_iMail($UserID);
else
	$iNewCampusMail = $lportal->returnNumNewMessage($UserID);
*/
$access2campusmail = $_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"];
$access2webmail = $_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_webmail"];

# Check new mails
//$access2campusmail = $laccess->isAccessCampusmail();

$newmail_icon = "";
//if ($access2campusmail && (!session_is_registered("iNewCampusMail") || strpos($SCRIPT_NAME,"viewfolder.php")!==false  ) )
if ($access2campusmail)
{
    // Check any new message
    include_once("$intranet_root/includes/libcampusmail.php");
    $header_lc = new libcampusmail();


	include_once($PATH_WRT_ROOT."includes/libwebmail.php");
	$lwebmail = new libwebmail();


	# no need to count for iMail Gamma/Plus
	if (!$plugin['imail_gamma'])
	{
	    if ($special_feature['imail'])
	        $iNewCampusMail = $header_lc->returnNumNewMessage_iMail($UserID);
	    else
	    	$iNewCampusMail = $header_lc->returnNumNewMessage($UserID);

    	//session_register("iNewCampusMail");
    	$_SESSION['iNewCampusMail'] = $iNewCampusMail;
	}


    # Check preference of check email
    $sql ="SELECT SkipCheckEmail FROM INTRANET_IMAIL_PREFERENCE WHERE UserID='$UserID'";
    $temp = $header_lc->returnArray($sql,1);
    $optionSkipCheckEmail = $temp[0][0];
    if ($optionSkipCheckEmail || $plugin['imail_gamma'])
    {
        $skipCheck = true;
    }
    else
    {
        $skipCheck = false;
    }


	//if ($ck_intranet_justlogin && $access2webmail && $header_lwebmail->type==3)


	if (!$skipCheck && $access2webmail && $lwebmail->type==3)
	{
		if ($lwebmail->openInbox($lu->UserLogin, $lu->UserPassword))
	    {
	    	$exmail_count = $lwebmail->checkMailCount();
	        $lwebmail->close();
	        $iNewCampusMail += $exmail_count;
	    }
	}

	# added by Ronald on 20080807 #
    //session_unregister("iNewCampusMail");
    session_unregister_intranet("iNewCampusMail");

}


if (!$special_feature['hide_iMail']) {
	# UCCKE customization ($sys_custom['uccke_parent_quick_icon'])
	if (( ($_SESSION["SSV_PRIVILEGE"]["special_feature"]['imail'])  || ($_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"]) || ($access2webmail) || ($access2campusmail) ) && ($plugin['imail_gamma']!==true || ($sys_custom['uccke_parent_quick_icon'] && $UserType==USERTYPE_PARENT && $plugin['imail_gamma']!==true)) )
	{
		$mail_count = $iNewCampusMail;
		$mail_count = convertOverflowNumber($mail_count, $defultIconDigitLimit);
		$mail_path = ($special_feature['imail'] ? "imail":"campusmail");

		$mail_href	= $special_feature['imail'] ? "/home/{$mail_path}/" : "javascript:newWindow('{$PATH_WRT_ROOT}home/{$mail_path}/',8)";
		$mail_txt	= $special_feature['imail'] ? $i_CampusMail_New_iMail : $i_adminmenu_sc_campusmail;
		$mail_onclick	= $special_feature['imail'] ? "onclick=\"self.location='/home/{$mail_path}/'\"" : "onclick=\"newWindow('{$PATH_WRT_ROOT}home/{$mail_path}/',8)\"";

		if($access2campusmail)
		{
			if ($mail_count > 0)
			{
			    $LogoArr["imail"] = "
									<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q1','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_imail_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" {$mail_onclick}  > \n
									<tr> \n
										<td width=\"41\" height=\"32\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_imail_off.gif\" name=\"q1\" border=\"0\" id=\"q1\"></a></td> \n
										<td width=\"29\" align=\"center\"><a href=\"{$mail_href}\" class=\"indexquicklink\">{$mail_count}</a>&nbsp;&nbsp;</td> \n
		
									</tr> \n
									<tr> \n
										<td align=\"center\" colspan=\"2\" valign=\"middle\"><a href=\"{$mail_href}\" class=\"indexquickbtn\" >{$mail_txt}</a></td> \n
									</tr> \n
									</table> \n
			    					";
			}
			else
			{

			    $LogoArr["imail"] = "
									<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q1','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_imail_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" {$mail_onclick}  > \n
									<tr> \n
										<td width=\"41\" height=\"32\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_imail_blur.gif\" name=\"q1\" border=\"0\" id=\"q1\"></a></td> \n
										<td width=\"29\" align=\"center\"><a href=\"{$mail_href}\" class=\"indexquicklink\"></a>&nbsp;&nbsp;</td> \n
		
									</tr> \n
									<tr> \n
										<td align=\"center\" colspan=\"2\" valign=\"middle\"><a href=\"{$mail_href}\" class=\"indexquickbtn\" >{$mail_txt}</a></td> \n
									</tr> \n
									</table> \n
			    					";
			}
		}
	}

	//temp replace imail logo by imail gamma
	if($plugin['imail_gamma'] && !empty($_SESSION['SSV_EMAIL_LOGIN']) && !empty($_SESSION['SSV_LOGIN_EMAIL']) && ($special_feature['forCharles'] || $special_feature['imail']!==true || $plugin['imail_gamma']))
	{
		$mail_path = "imail_gamma";
		$mail_txt = $i_CampusMail_New_iMail;
		$gamma_access_right = Get_Gamma_Identity_Access_Rights();
		if(($lu->isTeacherStaff() && $gamma_access_right[0]==1) || ($lu->isStudent() && $gamma_access_right[1]==1) || ($lu->isParent() && $gamma_access_right[2]==1)){
			$is_imail_gamma = true;
			$LogoArr["imail"] = "
							<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q1','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_imail_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='/home/{$mail_path}/'\"  > \n
							<tr> \n
								<td width=\"41\" height=\"32\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_imail_off.gif\" name=\"q1\" border=\"0\" id=\"q1\"></a></td> \n
								<td width=\"29\" align=\"center\"><a href=\"/home/{$mail_path}/\" class=\"indexquicklink\" id=\"gamma_new_mail_number\"></a>&nbsp;&nbsp;</td> \n
			
							</tr> \n
							<tr> \n
								<td align=\"center\" colspan=\"2\" valign=\"middle\"><a href=\"/home/{$mail_path}/\" class=\"indexquickbtn\" >{$mail_txt}</a></td> \n
							</tr> \n
							</table> \n
							";
		}
	}
}
################################################
##### Campus mail / iMail [END]
################################################

################################################
##### eLearning timetable [START]
################################################
include_once $intranet_root.'/includes/libgeneralsettings.php';
$lgs = new libgeneralsettings();
$settings = $lgs->Get_General_Setting("eLearningTimetable", array("'enableTimetable'"));
if(($lu->isStudent() || $lu->isParent()) && $settings['enableTimetable'] !== '0') {
	$portal_elearningTimetable_icon_off = "icon_elearn_table_off.png";// : "alert_elearn_table_blur.gif";
	$portal_elearningTimetable_icon_on = "icon_elearn_table_on.png";// : "alert_elearn_table_blur.gif";
	$elearningTimetable_html            = <<<HTML
<table width="70" height="68" border="0" cellpadding="0" cellspacing="0" background="{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif" onMouseOver="this.className='eLearningTimetable';MM_swapImage('eLearningTimetable','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/{$portal_elearningTimetable_icon_on}',1)" onMouseOut="MM_swapImgRestore()" onclick="javascript: showELearningTimetable()" class="eLearningTimetable">
    <tr>
        <td width="41" height="32"><a href="#"><img id="eLearningTimetable" src="{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/{$portal_elearningTimetable_icon_off}" /></a></td>
        <td width="29" align="center">&nbsp;&nbsp;<div id='elearningTimeTable' style='display: none;'><iframe id='elearningTimeTableIFrame' src='' style='width:100%;height:100%; border:0;'></iframe></div></td>
    </tr>
    <tr> 
        <td align="center" valign="middle" colspan="2" width="100%"><a href="#" class="indexquickbtn">{$Lang['eLearningTimetable']['eLearningTimetable']}</a></td> 
    </tr>
</table>
HTML;


	$elearningTimetable_html .= <<<SCRIPT
<script>
    function showELearningTimetable(){
        setTimeout(function(){
            tb_show('{$Lang['eLearningTimetable']['eLearningTimetable']}','#TB_inline?width=1000&amp;height=500&amp;inlineId=elearningTimeTable');
            $('#elearningTimeTableIFrame').attr('src', '/home/eLearning/timetable/index.php?isFromPortal=1');
        }, 100);
    }
</script>
SCRIPT;

	if(false && !$_SESSION['hasShowTimetableFirstTime']) {
		$elearningTimetable_html .= "<script>showELearningTimetable();</script>\n";
		$_SESSION['hasShowTimetableFirstTime'] = true;
	}

	$LogoArr['eLearningTimetable'] = $elearningTimetable_html;
}

################################################
##### eLearning timetable [END]
################################################

###################################################################
# iSmartcard Quick Icon [START]
###################################################################
# UCCKE customization ($sys_custom['uccke_parent_quick_icon'])
if($sys_custom['uccke_parent_quick_icon'] && $UserType==USERTYPE_PARENT)
{
	$iSmartcard_path = "/home/smartcard/";

	$LogoArr["iSmartcard"] = "
						<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_ismartcard','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_ismartcard_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$iSmartcard_path}'\"  > \n
						<tr> \n
							<td width=\"41\" height=\"32\"><a href=\"{$iSmartcard_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_ismartcard_off.gif\" name=\"q3_ismartcard\" border=\"0\" id=\"q3_ismartcard\" /></a></td> \n
							<td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
						</tr> \n
						<tr> \n
							<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"{$iSmartcard_path}\" class=\"indexquickbtn\" >{$Lang['Header']['Menu']['ismartcard']}</a></td> \n
						</tr> \n
						</table> \n
    					";
}
###################################################################
# iSmartcard Quick Icon [END]
###################################################################

########################
##### Circular
########################
if($special_feature['circular'])
{
	if($lu->isTeacherStaff())
	{
		$circular_count = $lportal->getUnsignedCircularCount($UserID);
		$circular_count = convertOverflowNumber($circular_count, $defultIconDigitLimit);
		$circular_path 	= "/home/eService/circular/" . (($circular_count !=0) ? "" : "?past=1");
		if ($circular_count != 0 || $special_feature['eCircular']['QuickIconDisplay'])
		{
			$circular_count = $circular_count? $circular_count : "";
			$LogoArr["eCircular"] = "
		                           <table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" OnMouseOver=\"this.className='handCursor';MM_swapImage('q2_circular','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_circular_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$circular_path}'\" > \n
									<tr> \n
										<td height=\"32\" width=\"41\"><a href=\"{$circular_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_circular_off.gif\" name=\"q2_circular\" width=\"41\" height=\"32\" border=\"0\" id=\"q2_circular\"></a></td> \n
										<td width=\"29\" align=\"center\"><a href=\"{$circular_path}\" class=\"indexquicklink\">{$circular_count}</a>&nbsp;&nbsp;</td> \n
									</tr> \n
									<tr> \n
										<td align=\"center\" valign=\"middle\" colspan=\"2\"><a href=\"{$circular_path}\" class=\"indexquickbtn\" >{$Lang['Header']['Menu']['eCircular']}</a></td> \n
									</tr> \n
									</table> \n
									";
		}
	}
}

###################################################################
# Digital Archive Quick Icon [START]
# temporary for UCCKE only
###################################################################
if($sys_custom['DA_quick_icon'] && $UserType==USERTYPE_STAFF)
{
	$da_path = "/home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/";
	$LogoArr["DA"] = "
						<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_da','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_da_on.png',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$da_path}'\"  > \n
						<tr> \n
							<td width=\"41\" height=\"32\"><a href=\"{$da_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_da_off.png\" name=\"q3_da\" border=\"0\" id=\"q_da\" /></a></td> \n
							<td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
						</tr> \n
						<tr> \n
							<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"{$da_path}\" class=\"indexquickbtn\" >{$Lang['Header']['Menu']['DigitalArchive']}</a></td> \n
						</tr> \n
						</table> \n
    					";
}
###################################################################
# Digital Archive Quick Icon [END]
###################################################################

########################
##### Discipline (v12)
########################
if($plugin['Disciplinev12'])
{

	include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
	$header_ldisciplinev12 = new libdisciplinev12();

	if($header_ldisciplinev12->DisplayIconInPortal) {

		# iDiscipline Approval alert icon
		if ($header_ldisciplinev12->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval"))
		{
			$AP_Pending_Record_Number = $header_ldisciplinev12->awardPunishmentRecordCount("RecordStatus",DISCIPLINE_STATUS_PENDING, Get_Current_Academic_Year_ID());
			$AP_Pending_Record_Number = convertOverflowNumber($AP_Pending_Record_Number, $defultIconDigitLimit);
			if($AP_Pending_Record_Number != 0)
			{
				$Disciplinev12_path = "/home/eAdmin/StudentMgmt/disciplinev12/overview/";

				$LogoArr["eDisciplinev12"] = "
									<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_disciplinev12','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_discipline_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$Disciplinev12_path}'\"  > \n
									<tr > \n
										<td width=\"41\" height=\"32\"><a href=\"{$Disciplinev12_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_discipline_off.gif\" name=\"q3_disciplinev12\" border=\"0\" id=\"q3_disciplinev12\" /></a></td> \n
										<td width=\"29\" align=\"center\"><a href=\"{$Disciplinev12_path}\" class=\"indexquicklink\">".$AP_Pending_Record_Number."</a>&nbsp;&nbsp;</td> \n
									</tr> \n
									<tr> \n
										<td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"{$Disciplinev12_path}\" class=\"indexquickbtn\" >{$i_Discipline_System_Approval}</a></td> \n
									</tr> \n
									</table> \n
			    					";
			}

		}
	}

	# UCCKE customization ($sys_custom['uccke_parent_quick_icon'])
	if($sys_custom['uccke_parent_quick_icon'] && $UserType==USERTYPE_PARENT)
	{
		$Disciplinev12_path = "/home/eService/disciplinev12/";

		$LogoArr["eDisciplinev12"] = "
							<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_disciplinev12','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_discipline_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$Disciplinev12_path}'\"  > \n
							<tr > \n
								<td width=\"41\" height=\"32\"><a href=\"{$Disciplinev12_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_discipline_off.gif\" name=\"q3_disciplinev12\" border=\"0\" id=\"q3_disciplinev12\" /></a></td> \n
								<td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
							</tr> \n
							<tr> \n
								<td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"{$Disciplinev12_path}\" class=\"indexquickbtn\" >{$Lang['Header']['Menu']['eDiscipline']}</a></td> \n
							</tr> \n
							</table> \n
	    					";
	}

	# [2015-0611-1642-26164] Po Leung Kuk C W Chu College Customization ($sys_custom['eDiscipline']['CSCProbation'])
	if($sys_custom['eDiscipline']['CSCProbation'] && $UserType==USERTYPE_STAFF){
		// path
		$Disciplinev12_rehabi_path = "/home/eAdmin/StudentMgmt/disciplinev12/management/rehabilitation/";

		// related records
		list($withRecord, $waitingRecords) = $header_ldisciplinev12->checkWithRabiRecord($_SESSION['UserID']);
		$waitingRecords = $waitingRecords>0? $waitingRecords : "&nbsp;";
		$waitingRecords = convertOverflowNumber($waitingRecords, $defultIconDigitLimit);

		if($withRecord){
			$LogoArr["Rehabilitation"] = "
							<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_disciplinev12','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_discipline_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$Disciplinev12_rehabi_path}'\"  > \n
							<tr > \n
								<td width=\"41\" height=\"32\"><a href=\"{$Disciplinev12_rehabi_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_discipline_off.gif\" name=\"q3_disciplinev12\" border=\"0\" id=\"q3_disciplinev12\" /></a></td> \n
								<td width=\"29\" align=\"center\"><a href=\"{$Disciplinev12_rehabi_path}\" class=\"indexquicklink\">".$waitingRecords."</a>&nbsp;&nbsp;</td> \n
							</tr> \n
							<tr > \n
								<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"{$Disciplinev12_rehabi_path}\" class=\"indexquickbtn\" >{$Lang['eDiscipline']['CWCRehabil']['Approval']}</a></td> \n
							</tr> \n
							</table> \n
	    					";
		}
	}
}



########################
##### eEnrolment Enrolment Period
########################

if ($plugin['eEnrollment'] || $plugin['enrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$lclubsenrol = new libclubsenrol();
}

if ($plugin['eEnrollment'] and $lclubsenrol->isUseEnrollment() and !$lu->isTeacherStaff() && !$lclubsenrol->Has_Trial_Period_Past())
{
	$ChildArr = array();
	if ($lu->isStudent())
	{
		$ChildArr[] = array($UserID);
	}
	else		// parent
	{
		$ChildArr = $lu->getChildrenList();
	}
	$temp_disabled = 0;

	# show icon if within club enrolment period
	if ($lclubsenrol->WITHIN_ENROLMENT_STAGE("club"))
	{
		$disabled = "";
	}
	else
	{
		$disabled = "disabled";
	}

	if ( (($lclubsenrol->enrollPersontype == 1)&&($lu->IsStudent())) || (($lclubsenrol->enrollPersontype == 0)&&($lu->IsParent())) )
		$disabled = "disabled";

	if($_SESSION['UserType'] == USERTYPE_PARENT && $sys_custom['eEnrolment']['HideEnrolForParentView']){
	    $disabled = "disabled";
	}


	$defaultMax = $lclubsenrol->defaultMax;

	for($c=0;$c<sizeof($ChildArr);$c++)
	{
		$enrolled_club_number = sizeof($lclubsenrol->STUDENT_ENROLLED_CLUB($ChildArr[$c][0]));
		$enrolled_applied_number = sizeof($lclubsenrol->STUDENT_APPLIED_CLUB($ChildArr[$c][0]));
		$total_club_involved = $enrolled_club_number + $enrolled_applied_number;
		if ($defaultMax==0)
		{
			// no limit => always show icon
			$temp_disabled += 1;
		}
		else
		{
			$temp_disabled += ($defaultMax-$total_club_involved) ? 1 : 0;
		}
	}
	$disabled = $temp_disabled ? $disabled : "disabled";

	$enrol_path = $disabled=="disabled" ? "": "/home/eService/enrollment/index.php";
	$enrol_name = $i_ClubsEnrollment;
	//--- End Club Enrolment Checking


	//--- Start Activity Enrolment Checking
	$in = 0;
	# check activity if club is disabled, otherwise skip activity check so that the icon is linked to the club enrolment page
	if($enrol_path=="" and $disabled=="disabled")
	{
		$disabled = "disabled";
		for($c=0;$c<sizeof($ChildArr);$c++)
		{
			$ApplicantID = $ChildArr[$c][0];
			$temp_disabled = 0;
			$LibUser = new libuser($ApplicantID);

			# check if within enrolment activity period
			if ($lclubsenrol->WITHIN_ENROLMENT_STAGE("activity"))
				$disabled = "";

			# check if student has enrolled in any activities
        	$EventArr = $lclubsenrol->GET_AVAILABLE_EVENTINFO_LIST('', $ApplicantID);
			for ($i = 0; $i < sizeof($EventArr); $i++)
			{
				$DataArr['EnrolEventID'] = $EventArr[$i][0];
				$DataArr['StudentID'] = $ApplicantID;
				$tempMark = false;
				if (
					(
						( ($EventArr[$i][15] == "S")&&($_SESSION['UserType']==USERTYPE_STUDENT) )||
						( ($EventArr[$i][15] == "P")&&($_SESSION['UserType']==USERTYPE_PARENT) )||
						( ($EventArr[$i][15] == "T")&&($lclubsenrol->IS_STUDNET_ENROLLED_EVENT($DataArr)) )||
						( ($EventArr[$i][15] == "P")&&($lclubsenrol->IS_STUDNET_ENROLLED_EVENT($DataArr)) )
					) &&
					(!(!$lclubsenrol->IS_STUDNET_ENROLLED_EVENT($DataArr) && (date("Y-m-d H:i", strtotime($EventArr[$i][14])) < date("Y-m-d H:i"))))
					)
				{

					if ($EventArr[$i][2] == $EventArr[$i][1]) {
						// quota display
						if ($EventArr[$i][1] == 0) {
							// normal for apply
							$temp_disabled++;
						}
					} else {
						$tempMark = true;
					}

					if ($lclubsenrol->IS_STUDNET_ENROLLED_EVENT($DataArr)) {
						// enrolled event
						$applied = true;
					} else if ($lclubsenrol->IS_STUDNET_APPLIED_EVENT($DataArr)) {
						// applied event
						$TempEnrollStatus = $eEnrollment['front']['applied'];
					} else if ($tempMark) {
						if (
							(!((date("Y-m-d H:i", strtotime($EventArr[$i][13])) <= date("Y-m-d H:i"))&&
							  (date("Y-m-d H:i", strtotime($EventArr[$i][14])) > date("Y-m-d H:i")))) &&
							  !($EventArr[$i][13] == $EventArr[$i][14])
							)
						{
							//$EnrollStatus = "---";
						} else {
							// normal for apply
							$temp_disabled++;
						}
					}


				}
			}
			$disabled = $temp_disabled ? "" : $disabled;
			$enrol_path = $disabled ? "": "/home/eService/enrollment/event_index.php";
			$enrol_name = $i_Form_Activity;
		}
	}

	if($_SESSION['UserType'] == USERTYPE_PARENT && $sys_custom['eEnrolment']['HideEnrolForParentView']){
	    $disabled = "disabled";
	}

	//--- End Activity Enrolment Checking
	if(!$lclubsenrol->Has_Trial_Period_Past() && !$disabled and $enrol_path/* and $lclubsenrol->hasAccessRight($UserID)*/)
	{

		$LogoArr["eEnrolment"] = "
						<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enrollment_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$enrol_path}'\"  > \n
						<tr > \n
							<td width=\"41\" height=\"32\"><a href=\"{$enrol_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enrollment_off.gif\" name=\"q3\" border=\"0\" id=\"q3\" /></a></td> \n
							<td width=\"29\" align=\"center\"><a href=\"{$enrol_path}\" class=\"indexquicklink\"></a>&nbsp;&nbsp;</td> \n
						</tr> \n
						<tr > \n
							<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\" ><a href=\"{$enrol_path}\" class=\"indexquickbtn\" >{$enrol_name}</a></td> \n
						</tr> \n
						</table> \n
    					";

	}
	# UCCKE customization ($sys_custom['uccke_parent_quick_icon'])
	else if($sys_custom['uccke_parent_quick_icon'] && $UserType==USERTYPE_PARENT)
	{

		$enrol_path = "/home/eService/enrollment/index.php";
		$LogoArr["eEnrolment"] = "
							<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enrollment_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$enrol_path}'\"  > \n
							<tr > \n
								<td width=\"41\" height=\"32\"><a href=\"{$enrol_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enrollment_off.gif\" name=\"q3\" border=\"0\" id=\"q3\" /></a></td> \n
								<td width=\"29\" align=\"center\"><a href=\"{$enrol_path}\" class=\"indexquicklink\"></a>&nbsp;&nbsp;</td> \n
							</tr> \n
							<tr> \n
								<td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\" ><a href=\"{$enrol_path}\" class=\"indexquickbtn\" >{$Lang['Header']['Menu']['eEnrolment']}</a></td> \n
							</tr> \n
							</table> \n
	    					";
	}


	## Check popup notification
// 	debug_pr("yat testing: ". $lclubsenrol->enable_popup_notification);
// 	debug_pr("yat testing: ". $lclubsenrol->popup_startdate);
// 	debug_pr("yat testing: ". $lclubsenrol->popup_enddate);
	if($_SESSION['Is_First_Login'] && $lclubsenrol->enable_popup_notification && date("Y-m-d H:i:s") >= $lclubsenrol->popup_startdate && date("Y-m-d H:i:s") <= $lclubsenrol->popup_enddate)
	{
		$eEnrolment_Reminder_popup = 1;
	}

	if( $UserType==USERTYPE_STUDENT && $sys_custom['eEnrolment']['CommitteeRecruitment']){

	    $CommitteeStart = $lclubsenrol->Committee_Recruitmen_Start." ".$lclubsenrol->Committee_Recruitmen_Start_Hour.":".$lclubsenrol->Committee_Recruitmen_Start_Min;
	    $CommitteeEnd = $lclubsenrol->Committee_Recruitmen_End." ".$lclubsenrol->Committee_Recruitmen_End_Hour.":".$lclubsenrol->Committee_Recruitmen_End_Min;
	    (date("Y-m-d H:i") >= $CommitteeStart && date("Y-m-d H:i")<=$CommitteeEnd) ? $disabled = "" : $disabled = "1";
	    if($disabled == ''){
	        $enrol_path = "/home/eService/enrollment/committee_club.php";
	        $LogoArr["eEnrolment2"] = "
                	        <table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enrollment_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$enrol_path}'\"  > \n
                    	        <tr > \n
                        	        <td width=\"41\" height=\"32\"><a href=\"{$enrol_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enrollment_off.gif\" name=\"q3\" border=\"0\" id=\"q3\" /></a></td> \n
                        	        <td width=\"29\" align=\"center\"><a href=\"{$enrol_path}\" class=\"indexquicklink\"></a>&nbsp;&nbsp;</td> \n
                    	        </tr> \n
                    	        <tr> \n
                    	           <td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\" ><a href=\"{$enrol_path}\" class=\"indexquickbtn\" >{$eEnrollment['tab']['CommitteeRecruitment']}</a></td> \n
                    	        </tr> \n
                	        </table> \n
	        ";
	    }

	}

}

# UCCKE customization ($sys_custom['uccke_parent_quick_icon'])
else if($sys_custom['uccke_parent_quick_icon'] && $UserType==USERTYPE_PARENT)
{
	$enrol_path = "/home/eService/enrollment/index.php";
	$LogoArr["eEnrolment"] = "
						<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enrollment_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$enrol_path}'\"  > \n
						<tr > \n
							<td width=\"41\" height=\"32\"><a href=\"{$enrol_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enrollment_off.gif\" name=\"q3\" border=\"0\" id=\"q3\" /></a></td> \n
							<td width=\"29\" align=\"center\"><a href=\"{$enrol_path}\" class=\"indexquicklink\"></a>&nbsp;&nbsp;</td> \n
						</tr> \n
						<tr> \n
							<td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\" ><a href=\"{$enrol_path}\" class=\"indexquickbtn\" >{$enrol_name}</a></td> \n
						</tr> \n
						</table> \n
    					";
}

if (!$plugin['eEnrollment'] and $plugin['enrollment'] and $lclubsenrol->isFillForm() and $_SESSION['UserType']==USERTYPE_STUDENT && !$lclubsenrol->Has_Trial_Period_Past())
{

	$enrol_path = "javascript:newWindow('/home/ecaenrol/',8)";
	$LogoArr["eEnrolment"] = "
						<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enrollment_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"newWindow('/home/ecaenrol/',8)\"  > \n
						<tr > \n
							<td width=\"41\" height=\"32\"><a href=\"{$enrol_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enrollment_off.gif\" name=\"q3\" border=\"0\" id=\"q3\" /></a></td> \n
							<td width=\"29\" align=\"center\"><a href=\"{$enrol_path}\" class=\"indexquicklink\"></a>&nbsp;&nbsp;</td> \n
						</tr> \n
						<tr> \n
							<td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\" ><a href=\"{$enrol_path}\" class=\"indexquickbtn\" >{$i_ClubsEnrollment}</a></td> \n
						</tr> \n
						</table> \n
    					";
}

###################################################################
# eHomework Quick Icon [START]
###################################################################
# UCCKE customization ($sys_custom['uccke_parent_quick_icon'])
if($sys_custom['uccke_parent_quick_icon'] && $UserType==USERTYPE_PARENT)
{
	$eHomework_path = "/home/eService/homework/";

	$LogoArr["eHomework"] = "
						<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_eHomework','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_ehomework_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$eHomework_path}'\"  > \n
						<tr > \n
							<td width=\"41\" height=\"32\"><a href=\"{$eHomework_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_ehomework_off.gif\" name=\"q3_eHomework\" border=\"0\" id=\"q3_eHomework\" /></a></td> \n
							<td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
						</tr> \n
						<tr> \n
							<td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"{$eHomework_path}\" class=\"indexquickbtn\" >{$Lang['Header']['Menu']['eHomework']}</a></td> \n
						</tr> \n
						</table> \n
    					";
}
###################################################################
# eHomework Quick Icon [END]
###################################################################

###################################################################
# eProcurement
###################################################################
if($plugin['ePCM'] && ($_SESSION['ePCM']['isAdmin'] != '' || $_SESSION['ePCM']['isUser'] != '')){
    include_once $intranet_root.'/includes/ePCM/libPCM_db_mgmt.php';
    include_once $intranet_root.'/includes/ePCM/libPCM_cfg.php';
    include_once $intranet_root.'/includes/libgeneralsettings.php';
    include_once($intranet_root."/lang/ePCM_lang.$intranet_session_language.php");
    $lpcm_db_mgmt = new libPCM_db_mgmt();
    $libgeneralsettings = new libgeneralsettings();
    $pcmGeneralSetting = $libgeneralsettings->Get_General_Setting($ModuleName = 'ePCM', $SettingList = array());
    $academicYearID = $pcmGeneralSetting['defaultAcademicYearID'];
    $numOfApproval = $lpcm_db_mgmt->getUserNumberOfApproval($academicYearID);
    $numOfApproval = convertOverflowNumber($numOfApproval, $defultIconDigitLimit);
    if($numOfApproval<= 0)$numOfApproval='&nbsp;';

    $pcmPath 	= "/home/eAdmin/ResourcesMgmt/eProcurement/";
    $LogoArr["ePCM"] = "
    <table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" OnMouseOver=\"this.className='handCursor';MM_swapImage('q10_RepairSystem','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_eprocurement_on.png',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$RepariSystem_path}'\" > \n
    <tr > \n
    <td height=\"32\" width=\"41\"><a href=\"{$pcmPath}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_eprocurement_off.png\" name=\"q10_RepairSystem\" width=\"41\" height=\"32\" border=\"0\" id=\"q10_RepairSystem\"></a></td> \n
    <td width=\"29\" align=\"center\"><a href=\"{$pcmPath}\" class=\"indexquicklink\">{$numOfApproval}</a>&nbsp;&nbsp;</td> \n
    </tr> \n
    <tr> \n
    <td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\"><a href=\"{$pcmPath}\" class=\"indexquickbtn\" >{$Lang['Header']['Menu']['ePCMApproval']}</a></td> \n
    </tr> \n
    </table> \n
    ";
}
###################################################################
# eProcurement[END]
###################################################################

###################################################################
# eInventory
###################################################################
if($_SESSION['SSV_PRIVILEGE']['plugin']['Inventory'])
{
	include_once($PATH_WRT_ROOT."includes/libinventory.php");
	$linventory  = new libinventory();

	if($_SESSION['inventory']['role'] != "")
	{
		$inventory_access_level = $linventory->getAccessLevel();

		if(($inventory_access_level == 1) || ($inventory_access_level == 2))
		{
			$warning_day_period = $linventory->getWarrantyExpiryWarningDayPeriod();

			if($warning_day_period > 0){

				if($linventory->IS_ADMIN_USER($UserID))
					$sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
				else
					$sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '$UserID'";

				$tmp_arr_group = $linventory->returnVector($sql);
				if(sizeof($tmp_arr_group)>0)
				{
					$target_admin_group = implode(",",$tmp_arr_group);
				}

				if($target_admin_group != "")
				{
					$cond = " AND c.AdminGroupID IN ($target_admin_group) ";
				}

				$curr_date = date("Y-m-d");
				$date_range = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + $warning_day_period, date("Y")));

				$sql = "SELECT
								count(a.ItemID)
						FROM
								INVENTORY_ITEM AS a INNER JOIN 
								INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID AND b.WarrantyExpiryDate != '') INNER JOIN
								INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID) INNER JOIN
								INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN
								INVENTORY_LOCATION_LEVEL AS floor ON (d.LocationLevelID = floor.LocationLevelID) INNER JOIN
								INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID)
						WHERE
								b.WarrantyExpiryDate BETWEEN '$curr_date' AND '$date_range'
								$cond
						ORDER BY
								b.WarrantyExpiryDate";
				$arr_expiry = $linventory->returnVector($sql);

	// 			debug_pr($sql);
				/*
				2011-02-02	YatWoon, Move to the bottom page (since newWindow not yet defined!)
				### Show Warranty Expiry Warning ###
				if(count($arr_expiry)>0)
				{
					?>
					<script language="javascript">
					<!--
					/home/admin/inventory/management/
					newWindow("admin/inventory/report/item_warranty_expiry_warning.php",4);
					//-->
					</script>
					<?
				}
				*/
			}
		}

		### Check Write-off approve ###
		if(($inventory_access_level == 1) || ($inventory_access_level == 2))
		{
			### Show Write-off Approval Alert ###
			$num_of_write_off_approval = $linventory->retrieveWriteOffApprovalNumber();
			$num_of_write_off_approval = convertOverflowNumber($num_of_write_off_approval, $defultIconDigitLimit);


			if($num_of_write_off_approval > 0)
			{
				$Inventory_path = "eAdmin/ResourcesMgmt/eInventory/management/write_off_approval/write_off_item.php";
				$LogoArr["eInventory"] = "
									<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3_inventory','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_inventory_approval_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$Inventory_path}'\"  > \n
									<tr class=\"indexquickbtnslim\"> \n
										<td width=\"41\" height=\"32\"><a href=\"{$Inventory_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_inventory_approval_off.gif\" name=\"q3_inventory\" border=\"0\" id=\"q3_inventory\" /></a></td> \n
										<td width=\"29\" align=\"center\"><a href=\"{$Inventory_path}\" class=\"indexquicklink\">".$num_of_write_off_approval."</a>&nbsp;&nbsp;</td> \n
									</tr> \n
									<tr> \n
										<td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"{$Inventory_path}\" class=\"indexquickbtn\" >{$i_InventorySystem_Approval}</a></td> \n
									</tr> \n
									</table> \n
			    					";
			}
		}
		### show variance handling notice ###
		$retriveVarianceHandlingNotice_count = $linventory->retriveVarianceHandlingNotice();
		$retriveVarianceHandlingNotice_count = convertOverflowNumber($retriveVarianceHandlingNotice_count, $defultIconDigitLimit);
		if($retriveVarianceHandlingNotice_count > 0)
		{
			$variance_notice_path = "eAdmin/ResourcesMgmt/eInventory/management/variance_handling_notice/index.php";
			$LogoArr["eInventory_Variance_Handling"] = "
														<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3_inventory1','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_inventory_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$variance_notice_path}'\"  > \n
														<tr > \n
															<td width=\"41\" height=\"32\"><a href=\"{$variance_notice_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_inventory_off.gif\" name=\"q3_inventory1\" border=\"0\" id=\"q3_inventory1\" /></a></td> \n
															<td width=\"29\" align=\"center\"><a href=\"{$variance_notice_path}\" class=\"indexquicklink\">".$retriveVarianceHandlingNotice_count."</a>&nbsp;&nbsp;</td> \n
														</tr> \n
														<tr> \n
															<td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"{$variance_notice_path}\" class=\"indexquickbtn\" >{$i_InventorySystem_Notice}</a></td> \n
														</tr> \n
														</table> \n
								    					";
		}
	}
}
###################################################################
# eInventory [END]
###################################################################
###eBooking START
if($plugin['eBooking'] && ($_SESSION["eBooking"]["role"] == "ADMIN" || $_SESSION["eBooking"]["role"] == "MANAGEMENT_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "FOLLOW_UP_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "MANAGEMENT_AND_FOLLOW_UP_GROUP_MEMBER") ){
	include_once($PATH_WRT_ROOT."includes/libebooking.php");
	$lebooking 	= new libebooking();

	$ebooking_path_home = '/home/eAdmin/ResourcesMgmt/eBooking/management/booking_request.php';
	if($lebooking->IS_ADMIN_USER()){
		//Get All the Booking Record
		// change sql where condition compare with date instead of datetime --- simonyu/20170920
		$sql = '
					SELECT
						ier.BookingID, ADDTIME(ier.Date,ier.EndTime) as DateStartTime
					FROM
						INTRANET_EBOOKING_RECORD ier
					INNER JOIN
						INTRANET_EBOOKING_BOOKING_DETAILS iebd
							ON	ier.BookingID = iebd.BookingID
					WHERE
						iebd.BookingStatus = "0"
					AND
						ier.RecordStatus = "1"
					AND
						ier.Date >= date(now())
		';

		$ebooking_path_home = '/home/eAdmin/ResourcesMgmt/eBooking/management/booking_request.php?BookingStatus=0';
	}else{
		//Approve Group
		//$UserID = $_SESSION['UserID'];
		//Step 1 Get All the Item ID, Location ID related to the approval Group user belongs to
		$sql =
		'
				SELECT
					DISTINCT
						iemg.GroupID, ielmgr.LocationID, ieimg.ItemID
				FROM
					INTRANET_EBOOKING_MANAGEMENT_GROUP iemg
				INNER JOIN
					INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER iemgm ON iemg.GroupID = iemgm.GroupID
				LEFT JOIN
					INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP ieimg ON ieimg.GroupID = iemg.GroupID
				LEFT JOIN
					INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION ielmgr ON ielmgr.GroupID = iemg.GroupID
				WHERE
					iemgm.UserID = "'.$_SESSION['UserID'].'"
			';

		$rs = $lebooking->returnResultSet($sql);
		foreach ((array)$rs as $_rs){
			if($_rs['LocationID']){
				$rmID[] = $_rs['LocationID'];

			}if($_rs['ItemID']){
				$itemID[] = $_rs['ItemID'];
			}
		}
		$rmID_sql = implode(',',(array)$rmID);
		$itemID_sql = implode(',',(array)$itemID);
		#room.FacilityType = 1
		if(!empty($_rs['LocationID']) && !empty($rmID_sql)){
			$facilityCondArr[] = "(iebd.FacilityID in ($rmID_sql) and iebd.FacilityType = '1') ";
		}
		#item.FacilityType = 2
		if(!empty($_rs['ItemID']) && !empty($itemID_sql)){
			$facilityCondArr[]= "(iebd.FacilityID in ($itemID_sql) and iebd.FacilityType = '2') ";
		}

		if(!empty($facilityCondArr)){
			$facilityCond = '('.implode(' OR ',$facilityCondArr).') AND ';
		}

		// Step 2 Get count of Booking Record the Item/ Location Selected above related to
		// back up sql
		/*
		$sql = '
					SELECT
						ier.BookingID, ADDTIME(ier.Date,ier.EndTime) as DateStartTime
					FROM
						INTRANET_EBOOKING_RECORD ier
					INNER JOIN
						INTRANET_EBOOKING_BOOKING_DETAILS iebd
							ON	ier.BookingID = iebd.BookingID
					WHERE
						iebd.FacilityID in ('.$ID_sql.')
					AND
						iebd.BookingStatus = "0"
					AND
						ier.RecordStatus = "1"
					AND
						ier.Date >= date(now())
		';
		*/

		$sql = '
					SELECT
						ier.BookingID, ADDTIME(ier.Date,ier.EndTime) as DateStartTime
					FROM
						INTRANET_EBOOKING_RECORD ier
					INNER JOIN
						INTRANET_EBOOKING_BOOKING_DETAILS iebd
							ON	ier.BookingID = iebd.BookingID
					WHERE
						  '.$facilityCond.' 
					
						iebd.BookingStatus = "0"
					AND
						ier.RecordStatus = "1"
					AND
						ier.Date >= date(now())
		';

		$ebooking_path_home = '/home/eAdmin/ResourcesMgmt/eBooking/management/booking_request.php?BookingStatus=0';
	}
	$rs = $lebooking->returnResultSet($sql);

	$NumberOfApprove = sizeof($rs)==0? '': sizeof($rs);
	$NumberOfApprove = convertOverflowNumber($NumberOfApprove, $defultIconDigitLimit);
	$LogoArr["eBooking"] = "
	<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('ebooking','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_ebooking_approval_on.png',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\""."self.location='{$ebooking_path_home}'"."\"  > \n
	<tr> \n
	<td width=\"41\" height=\"32\"><a href=\"{$ebooking_path_home}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_ebooking_approval_off.png\" name=\"ebooking\" border=\"0\" id=\"ebooking\" /></a></td> \n
	<td width=\"29\" align=\"center\"><a href=\"{$ebooking_path_home}\" class=\"indexquicklink\">".$NumberOfApprove."</a>&nbsp;&nbsp;</td> \n
	</tr> \n
	<tr> \n
	<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"{$ebooking_path_home}\" class=\"indexquickbtn\" >".$Lang['Header']['Menu']['eBookingApproval']."</a></td> \n
	</tr> \n
	</table> \n
	";
}
###eBooking END

########################
##### eNews Approval
########################

if($lu->isTeacherStaff() and $_SESSION["SSV_PRIVILEGE"]["adminjob"]["isAnnouncementAdmin"])
{
	$WaitingAnnouncement_count = $lportal->getWaitingAnnouncement($UserID);
	$WaitingAnnouncement_count = convertOverflowNumber($WaitingAnnouncement_count, $defultIconDigitLimit);
// 	if($UserID==1663)
// 	{
//		debug_pr($WaitingAnnouncement_count);
// 	}
	$WaitingAnnouncement_path = "/home/redirect.php?mode=1&url=/home/admin/announcement/";
	if ($WaitingAnnouncement_count != 0)
	{
		$LogoArr["eNewsApproval"] = "
	                           <table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" OnMouseOver=\"this.className='handCursor';MM_swapImage('q_announcement_approve','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_announcement_approve_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$WaitingAnnouncement_path}'\" > \n
								<tr> \n
									<td height=\"32\" width=\"41\"><a href=\"{$WaitingAnnouncement_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_announcement_approve_off.gif\" name=\"q_announcement_approve\" width=\"41\" height=\"32\" border=\"0\" id=\"q_announcement_approve\"></a></td> \n
									<td width=\"29\" align=\"center\"><a href=\"{$WaitingAnnouncement_path}\" class=\"indexquicklink\">{$WaitingAnnouncement_count}</a>&nbsp;&nbsp;</td> \n
								</tr> \n
								<tr> \n
									<td align=\"center\" valign=\"middle\" colspan=\"2\"><a href=\"{$WaitingAnnouncement_path}\" class=\"indexquickbtn\" >{$i_eNews_Approval2}</a></td> \n
								</tr> \n
								</table> \n
								";
	}
}

########################
##### eNotice
########################
if($plugin['notice'])
{
	if((!$lu->isTeacherStaff()))
	{
		// [2017-1011-0940-46235] apply disable checking
		if ($sys_custom['eNotice']['eServiceDisableParent'] && $_SESSION['UserType']==USERTYPE_PARENT) {
			$_SESSION["SSV_PRIVILEGE"]["notice"]["canAccess"] = false;
		}
		if ($sys_custom['eNotice']['eServiceDisableStudent'] && $_SESSION['UserType']==USERTYPE_STUDENT) {
			$_SESSION["SSV_PRIVILEGE"]["notice"]["canAccess"] = false;
		}

		$enotice_location = '/home/eService/notice/';
		$unsigned_notice_icon = "";
		if (!$_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] && $_SESSION["SSV_PRIVILEGE"]["notice"]["canAccess"])
		{
		     if ($lu->isParent())
		     {
		         $header_notices_unsigned = $lportal->getParentNoticeUnsignedCount();
		     }
		     else if ($lu->isStudent())
		     {
		          $header_notices_unsigned = $lportal->getStudentNoticeUnsignedCount();
		     }
		     $header_notices_unsigned = convertOverflowNumber($header_notices_unsigned, $defultIconDigitLimit);

		     # UCCKE customization ($sys_custom['uccke_parent_quick_icon'])
		     if (($header_notices_unsigned != 0 && $header_notices_unsigned != "") || ($sys_custom['uccke_parent_quick_icon'] && $UserType==USERTYPE_PARENT) || $special_feature['eNotice']['QuickIconDisplay'])
		     {
			     $header_notices_unsigned = $header_notices_unsigned ? $header_notices_unsigned : "";
				 $LogoArr["enotice"] = "
											<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q2','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enotice_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='".$enotice_location."'\" > \n
											<tr > \n
												<td width=\"41\" height=\"32\"><a href=\"".$enotice_location."\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enotice_off.gif\" name=\"q2\" border=\"0\" id=\"q2\"></a></td> \n
												<td width=\"29\" align=\"center\"><a href=\"".$enotice_location."\" class=\"indexquicklink\">{$header_notices_unsigned}</a>&nbsp;&nbsp;</td> \n
											</tr> \n
											<tr> \n
												<td align=\"center\" valign=\"middle\" colspan=\"2\" ><a href=\"".$enotice_location."\" class=\"indexquickbtn\" >{$ip20_enotice}</a></td> \n
											</tr> \n
											</table> \n
					    					";
		     }
	 	}
	}
	else
	{
		// Staff and Teacher 	20160603 Kenneth
		include_once $PATH_WRT_ROOT.'includes/libnotice.php';
		$lnotice = new libnotice();
		if($lnotice->hasApprovalRight())
		{
			$countOfPendingNotice_P = $lnotice->getPendingApprovalNotice('P',NULL);
			$countOfPendingNotice_S = $lnotice->getPendingApprovalNotice('S',NULL);
			$countOfPendingNotice_Payment = $lnotice->getPendingApprovalNotice('','Payment');
			$header_notices_unsigned = $countOfPendingNotice_S+$countOfPendingNotice_P+$countOfPendingNotice_Payment;
			if($countOfPendingNotice_P>0)
				$enotice_location = '/home/eAdmin/StudentMgmt/notice/index.php?pendingApproval=1';
			else if($countOfPendingNotice_S>0)
				$enotice_location = '/home/eAdmin/StudentMgmt/notice/student_notice/index.php?pendingApproval=1';
			else
				$enotice_location = '/home/eAdmin/StudentMgmt/notice/payment_notice/paymentNotice.php?pendingApproval=1';
		}

		if($header_notices_unsigned>0)
		{
			$LogoArr["enotice"] = "
											<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q2','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enotice_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='".$enotice_location."'\" > \n
											<tr > \n
												<td width=\"41\" height=\"32\"><a href=\"".$enotice_location."\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enotice_off.gif\" name=\"q2\" border=\"0\" id=\"q2\"></a></td> \n
												<td width=\"29\" align=\"center\"><a href=\"".$enotice_location."\" class=\"indexquicklink\">{$header_notices_unsigned}</a>&nbsp;&nbsp;</td> \n
											</tr> \n
											<tr> \n
												<td align=\"center\" valign=\"middle\" colspan=\"2\" ><a href=\"".$enotice_location."\" class=\"indexquickbtn\" >".$Lang['eNotice']['Logo']['eNoticeApproval']."</a></td> \n
											</tr> \n
											</table> \n
					    					";
		}
	}
}

########################
##### Poll
########################

if(!$lu->isParent())
{
	$poll_list = $lportal->returnUnPollingList($UserID);
	$poll_count = sizeof($poll_list);
	$poll_count = convertOverflowNumber($poll_count, $defultIconDigitLimit);
	if ($poll_count != 0)
	{
		$LogoArr["polling"] = "
	                           <table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" OnMouseOver=\"this.className='handCursor';MM_swapImage('q4','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_polling_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='/home/eService/polling/'\" > \n

								<tr> \n
									<td height=\"32\" width=\"41\"><a href=\"/home/eService/polling/\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_polling_blur.gif\" name=\"q4\" width=\"41\" height=\"32\" border=\"0\" id=\"q4\"></a></td> \n
									<td width=\"29\" align=\"center\"><a href=\"/home/eService/polling/\" class=\"indexquicklink\">{$poll_count}</a>&nbsp;&nbsp;</td> \n
								</tr> \n
								<tr> \n
									<td align=\"center\" valign=\"middle\" colspan=\"2\"><a href=\"/home/eService/polling/\" class=\"indexquickbtn\" >{$i_adminmenu_polling}</a></td> \n
								</tr> \n
								</table> \n
								";
	}
}


########################
##### eReportCard Marksheet Submission Period
########################
if($plugin['ReportCard2008'])
{
	/* eRC1.0 Code
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	$libreportcard = new libreportcard();

	if($lu->isTeacherStaff())
	{
		$PageRight = "TEACHER";
		if($libreportcard->hasAccessRight($UserID))
		{
			if($libreportcard->CHECK_MARKSHEET_SUBMISSION_PERIOD() and $libreportcard->GET_TEACHER_CONFIRMATION_COUNT() != 0)
			{
				$LogoArr["eReportCard"] = "
								<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3_reportcard','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_reportcard_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='/home/admin/reportcard/admin.php'\"  > \n
								<tr> \n
									<td width=\"41\" height=\"32\"><a href=\"/home/admin/reportcard/admin.php\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_reportcard_off.gif\" name=\"q3_reportcard\" border=\"0\" id=\"q3_reportcard\" /></a></td> \n
									<td width=\"29\" align=\"center\"><a href=\"/home/admin/reportcard/admin.php\" class=\"indexquicklink\">". $libreportcard->GET_TEACHER_CONFIRMATION_COUNT() ."</a></td> \n
								</tr> \n
								<tr> \n
									<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"/home/admin/reportcard/admin.php\" class=\"indexquickbtn\" >{$eReportCard['MarksheetSubmission']}</a></td> \n
								</tr> \n
								</table> \n
		    					";
			}
		}
	}

	//if($lu->isStudent() or $lu->isParent())
	if($lu->isStudent())
	{
		$PageRight = $lu->isStudent() ? "STUDENT" : "PARENT";
		if($libreportcard->hasAccessRight($UserID))
		{
			if($libreportcard->GET_MARKSHEET_VERIFICATION() and $libreportcard->GET_STUDENT_VERIFICATION_COUNT() != 0)
			{
				$LogoArr["eReportCard"] = "
								<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3_reportcard','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_reportcard_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='/home/admin/reportcard/admin.php'\"  > \n
								<tr> \n
									<td width=\"41\" height=\"32\"><a href=\"/home/admin/reportcard/admin.php\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_reportcard_off.gif\" name=\"q3_reportcard\" border=\"0\" id=\"q3_reportcard\" /></a></td> \n
									<td width=\"29\" align=\"center\"><a href=\"/home/admin/reportcard/admin.php\" class=\"indexquicklink\">". $libreportcard->GET_STUDENT_VERIFICATION_COUNT() ."</a></td> \n
								</tr> \n
								<tr> \n
									<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"/home/admin/reportcard/admin.php\" class=\"indexquickbtn\" >{$eReportCard['MarksheetSubmission']}</a></td> \n
								</tr> \n
								</table> \n
		    					";
			}
		}
	}
	*/
}

########################
##### Repair System
########################
if($plugin['RepairSystem'] && ($_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] || $lportal->isRepairSystemMgmtGroupMember($UserID)))
{
	$repair_count = $lportal->RepairSystemPendingCount();
	$repair_count = convertOverflowNumber($repair_count, $defultIconDigitLimit);
	$RepariSystem_path 	= "/home/eAdmin/ResourcesMgmt/RepairSystem/";
	if ($repair_count != 0)
	{
		$LogoArr["RepairSystem"] = "
	                           <table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" OnMouseOver=\"this.className='handCursor';MM_swapImage('q10_RepairSystem','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_repairsystem_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$RepariSystem_path}'\" > \n
								<tr> \n
									<td height=\"32\" width=\"41\"><a href=\"{$RepariSystem_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_repairsystem_off.gif\" name=\"q10_RepairSystem\" width=\"41\" height=\"32\" border=\"0\" id=\"q10_RepairSystem\"></a></td> \n
									<td width=\"29\" align=\"center\"><a href=\"{$RepariSystem_path}\" class=\"indexquicklink\">{$repair_count}</a>&nbsp;&nbsp;</td> \n
								</tr> \n
								<tr> \n
									<td align=\"center\" valign=\"middle\" colspan=\"2\"><a href=\"{$RepariSystem_path}\" class=\"indexquickbtn\" >{$Lang['Header']['Menu']['RepairSystem']}</a></td> \n
								</tr> \n
								</table> \n
								";
	}
}

########################
##### Sports enrollment
########################
if ($plugin['Sports'])
{
	if($_SESSION["SSV_PRIVILEGE"]["eSports"]["inEnrolmentPeriod"] && $lu->isStudent())
	{
		$esports_link = "/home/eService/eSports/sports/";
		$LogoArr["eSports"] = "
								<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3_sport','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_sport_enroll_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='$esports_link'\"  > \n
								<tr > \n
									<td width=\"41\" height=\"32\"><a href=\"$esports_link\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_sport_enroll_off.gif\" name=\"q3_sport\" border=\"0\" id=\"q3_sport\" /></a></td> \n
									<td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
								</tr> \n
								<tr> \n
									<td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"$esports_link\" class=\"indexquickbtn\" >{$i_Sports_Day_Enrolment}</a></td> \n
								</tr> \n
								</table> \n
		    					";
	}
}
########################
##### SwimmingGala enrollment
########################
if ($plugin['swimming_gala'])
{
	if($_SESSION["SSV_PRIVILEGE"]["swimminggala"]["inEnrolmentPeriod"] && $lu->isStudent())
	{
		$swimminggala_link = "/home/eService/eSports/swimming_gala/";
		$LogoArr["swimminggala"] = "
								<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3_swimminggala','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_swim_enroll_on.png',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='$swimminggala_link'\"  > \n
								<tr > \n
									<td width=\"41\" height=\"32\"><a href=\"$swimminggala_link\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_swim_enroll_off.png\" name=\"q3_swimminggala\" border=\"0\" id=\"q3_swimminggala\" /></a></td> \n
									<td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
								</tr> \n
								<tr> \n
									<td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"$swimminggala_link\" class=\"indexquickbtn\" >{$i_Swimming_Gala_Enrolment}</a></td> \n
								</tr> \n
								</table> \n
		    					";
	}
}


########################
##### Group Survey
########################
$new_survey_icon = "";
$survey_count = sizeof($lportal->returnSurvey());
$survey_count = convertOverflowNumber($survey_count, $defultIconDigitLimit);
if ($survey_count != 0)
{
   $LogoArr["survey"] = "
						<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_esurvery_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='/home/eService/eSurvey/'\"  > \n
						<tr> \n
							<td width=\"41\" height=\"32\"><a href=\"/home/eService/eSurvey/\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_esurvery_off.gif\" name=\"q3\" border=\"0\" id=\"q3\" /></a></td> \n
							<td width=\"29\" align=\"center\"><a href=\"/home/eService/eSurvey/\" class=\"indexquicklink\">{$survey_count}</a>&nbsp;&nbsp;</td> \n
						</tr> \n
						<tr> \n
							<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\" ><a href=\"/home/eService/eSurvey/\" class=\"indexquickbtn\" >{$Lang['eSurvey']['Survey']}</a></td> \n
						</tr> \n
						</table> \n
    					";
}

########################
##### UpdateStudentPwdPopUp
########################
if ($_SESSION["CanAccessUpdateStudentPwdPopup"])
{
   $LogoArr["UpdateStudentPwdPopup"] = "
						<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q23','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_setting_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"jsUpdateStudentPwdPopUp();\"  > \n
						<tr > \n
							<td width=\"41\" height=\"32\"><a href=\"#\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_setting_off.gif\" name=\"q23\" border=\"0\" id=\"q23\" /></a></td> \n
							<td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
						</tr> \n
						<tr> \n
							<td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\" ><a href=\"#\" class=\"indexquickbtn\" >{$Lang['SysMgr']['RoleManagement']['UpdateStudentPwdPopUp'] }</a></td> \n
						</tr> \n
						</table> \n
    					";
}


/*
#####################################
# SLS Logo [Start]
#####################################

if(isset($_SESSION["SSV_PRIVILEGE"]["library"]["access"]) && $_SESSION["SSV_PRIVILEGE"]["library"]["access"])
{
	$sls_path = "/home/eService/sls_library_index.php";
	$LogoArr["SLS"] = "
						<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3_sls','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_SLS_Library_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$sls_path}'\"  > \n
						<tr> \n
							<td width=\"41\" height=\"32\"><a href=\"{$sls_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_SLS_Library_off.gif\" name=\"q3_sls\" border=\"0\" id=\"q3_sls\" /></a></td> \n
							<td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
						</tr> \n
						<tr> \n
							<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"{$sls_path}\" class=\"indexquickbtn\" >{$Lang['SLSLibrary']}</a></td> \n
						</tr> \n
						</table> \n
    					";
}
#####################################
# SLS Logo [End]
#####################################
*/

#####################################
# PowerLesson Logo [Start]
#####################################
$new_powerlesson_icon = "";
/*
if ($plugin['power_lesson'] == true && $lu->isStudent())	// hide temporarily
{
	$Lang['LessonPlan']['PowerLesson'] = 'Power Lesson';
	$lesson_count = 0;
	$allUserCourse = $lportal->returnEClassUserIDCourseIDStandard($lu->UserEmail);
	if (!empty($allUserCourse)){
		foreach($allUserCourse as $course){
			$lsession = $lportal->get_lesson_session($course[0]);
			$lesson_count += count($lsession);
		}
	}
	$bubble_layer .= '
	<script language="JavaScript" src="/templates/'.$LAYOUT_SKIN.'/js/powerlesson.js"></script>
	<div class="sub_layer_board" id="sub_layer_powerlesson" style="visibility:hidden;z-index:100;width:300px;">
		<div class="bubble_board_01">
			<div class="bubble_board_02">
				<img style="float:right" src="/images/2009a/addon_tools/sub_layer_arrow.gif" width="16" height="12" />
				<br style="clear:both;" />
				<a href="javascript:void(0)" title="'.$Lang['Btn']['Close'].'" onclick="MM_showHideLayers(\'sub_layer_powerlesson\',\'\',\'hide\')"><img src="/images/2009a/addon_tools/close_layer.gif" border="0" style="float:right"/></a>
			</div>
		</div>
		<div class="bubble_board_03">
			<div class="bubble_board_04">
				<div style="position: absolute;width:225px"><span class="tabletext" style="float:right; background-color:red; color:white;display:none; padding:2px" id="ajaxMsgBlock"></span>
				</div>
				<div id="bubble_board_content"></div>
			</div>
		</div>
		<div class="bubble_board_05">
			<div class="bubble_board_06"></div>
		</div>
	</div>';
   	$LogoArr["powerlesson"] = "
			<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_powerlesson_on.png',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"MM_showHideLayers('sub_layer_powerlesson','','show');loadPowerLessonList(event);\"  > \n
			<tr> \n
				<td width=\"41\" height=\"32\"><a href=\"javascript:void(0)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_powerlesson_off.png\" name=\"q3\" border=\"0\" id=\"q3\" /></a></td> \n
				<td width=\"29\" align=\"center\"><a href=\"javascript:void(0)\" class=\"indexquicklink\">{$lesson_count}</a>&nbsp;&nbsp;</td> \n
			</tr> \n
			<tr> \n
			  <td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\" >".$bubble_layer."
				<a href=\"javascript:void(0)\" class=\"indexquickbtn\" >{$Lang['LessonPlan']['PowerLesson']}</a>
			  </td> \n
			</tr> \n
			</table> \n";
}*/
#####################################
# PowerLesson Logo [End]
#####################################


#####################################
# Document Routing Logo [Start]
#####################################
if ($plugin['DocRouting'] && $_SESSION['UserType']==USERTYPE_STAFF) {
	include_once($PATH_WRT_ROOT."includes/DocRouting/docRoutingConfig.inc.php");

	$numOfInvolvedEntry = $lportal->getDrUserCurrentInvolvedEntryCount();
	$numOfInvolvedEntry = convertOverflowNumber($numOfInvolvedEntry, $defultIconDigitLimit);
	//if ($numOfInvolvedEntry > 0) {
		$docRoutingLink = '/'.$docRoutingConfig['eAdminPath'];
		$LogoArr["DocRouting"] = "
	                           <table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" OnMouseOver=\"this.className='handCursor';MM_swapImage('q4_involvedentry','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_dr_on.png',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='".$docRoutingLink."'\" > \n

								<tr> \n
									<td height=\"32\" width=\"41\"><a href=\"".$docRoutingLink."\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_dr_off.png\" name=\"q4_involvedentry\" width=\"41\" height=\"32\" border=\"0\" id=\"q4_involvedentry\"></a></td> \n
									<td width=\"29\" align=\"center\"><a href=\"".$docRoutingLink."\" class=\"indexquicklink\">".($numOfInvolvedEntry>0?$numOfInvolvedEntry:'')."</a>&nbsp;&nbsp;</td> \n
								</tr> \n
								<tr> \n
									<td align=\"center\" valign=\"middle\" colspan=\"2\"><a href=\"".$docRoutingLink."\" class=\"indexquickbtn\" >".$Lang['Header']['Menu']['DocRouting']."</a></td> \n
								</tr> \n
								</table> \n
								";
	//}
}
#####################################
# Document Routing Logo [End]
#####################################




if ($plugin['library_management_system'] && !isset($sys_custom['eLib']['HideShortCut']))
{
	include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");

	$permission = elibrary_plus::getUserPermission($UserID);

	# may hide only for admin to view
	if ($plugin['library_management_system'] && ($permission['elibplus_opened'] || $_SESSION['UserType']==USERTYPE_STAFF))
	{
		$eLib_plus_access = true;
	}

	if ($eLib_plus_access)
	{
		$lelibplus = new elibrary_plus($book_id, $UserID, $ck_eLib_bookType, $range);

		$elib_plus_current_records = $lelibplus->getNotificationCount();
		$elib_plus_current_records = convertOverflowNumber($elib_plus_current_records, $defultIconDigitLimit);
		if ($elib_plus_current_records==0)
		{
			$elib_plus_current_records = "";
		}

		if ($plugin['eLibraryPlusOne']) {
			$elib_path_home = "/home/eLearning/elibplus/";
			$elib_path_record_page = "/home/eLearning/elibplus/?page=record";
		}
		else {
			$elib_path_home = "javascript:newWindow('/home/eLearning/elibplus2/', 8);";
			$elib_path_record_page = "javascript:newWindow('/home/eLearning/elibplus2/myrecord.php', 8);";
		}

		$LogoArr["eLibrary_plus"] = "
			<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3_elibplus','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_elibplus_on.png',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"".($plugin['eLibraryPlusOne']?"self.location='{$elib_path_home}'":$elib_path_home)."\"  > \n
			<tr> \n
				<td width=\"41\" height=\"32\"><a href=\"{$elib_path_home}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_elibplus_off.png\" name=\"q3_elibplus\" border=\"0\" id=\"q3_elibplus\" /></a></td> \n
				<td width=\"29\" align=\"center\"><a href=\"{$elib_path_record_page}\" class=\"indexquicklink\">".$elib_plus_current_records."</a>&nbsp;&nbsp;</td> \n
			</tr> \n
			<tr> \n
				<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"{$elib_path_home}\" class=\"indexquickbtn\" >eLibrary <i>plus</i></a></td> \n
			</tr> \n
			</table> \n
			";
	}
}


#####################################
# Apply Leave (app) Logo [Start]
#####################################
//if ($plugin["eClassApp"] && $_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancestudent"] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"])
	{
		$numOfPendingApplyLeaveRecord = $lportal->getApplyLeaveTodayPendingRecord();
		$numOfPendingApplyLeaveRecord = convertOverflowNumber($numOfPendingApplyLeaveRecord, $defultIconDigitLimit);
		if ($numOfPendingApplyLeaveRecord > 0) {
			$applyLeaveLink = "/home/eAdmin/StudentMgmt/attendance/dailyoperation/apply_leave/";
			$LogoArr["ApplyLeave(App)"] = "
		                           	<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" OnMouseOver=\"this.className='handCursor';MM_swapImage('q4_eattendance','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_eattendance_on.png',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='".$applyLeaveLink."'\" > \n
									<tr > \n
										<td height=\"32\" width=\"41\"><a href=\"".$applyLeaveLink."\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_eattendance_off.png\" name=\"q4_eattendance\" width=\"41\" height=\"32\" border=\"0\" id=\"q4_eattendance\"></a></td> \n
										<td width=\"29\" align=\"center\"><a href=\"".$applyLeaveLink."\" class=\"indexquicklink\">".$numOfPendingApplyLeaveRecord."</a>&nbsp;&nbsp;</td> \n
									</tr> \n
									<tr> \n
										<td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\"><a href=\"".$applyLeaveLink."\" class=\"indexquickbtn\" >".$Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)']."</a></td> \n
									</tr> \n
									</table> \n ";
		}
	}
	// [2017-0908-1248-12235] Class Teacher can approve leave request
	else if ($sys_custom["StudentAttendance"]["Class_Teacher_Approve_Leave_Request"])
	{
		// Get Teaching Classes
		include_once($intranet_root."/includes/libteaching.php");
		$lteaching = new libteaching();
		$TeachingClasses = $lteaching->returnTeacherClass($UserID);
		$TeachingClassCount = count((array)$TeachingClasses);

		// Class Teacher > Show Apply Leave Icon
		$isClassTeacher = $TeachingClassCount > 0;
		if ($isClassTeacher)
		{
			$YearClassList = Get_Array_By_Key($TeachingClasses, "ClassID");
			$ClassPendingApplyLeaveRecordCount = $lportal->getClassTeacherApplyLeaveTodayPendingRecord($YearClassList);
			$ClassPendingApplyLeaveRecordCount = convertOverflowNumber($ClassPendingApplyLeaveRecordCount, $defultIconDigitLimit);
			$ClassPendingApplyLeaveRecordCount = $ClassPendingApplyLeaveRecordCount? $ClassPendingApplyLeaveRecordCount : "";

			$applyLeaveLink = "/home/eAdmin/StudentMgmt/attendance/class_teacher/apply_leave_list.php";
			$LogoArr["ApplyLeave(App)"] = "
		                           	<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" OnMouseOver=\"this.className='handCursor';MM_swapImage('q4_eattendance','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_eattendance_on.png',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='".$applyLeaveLink."'\" > \n
									<tr > \n
										<td height=\"32\" width=\"41\"><a href=\"".$applyLeaveLink."\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_eattendance_off.png\" name=\"q4_eattendance\" width=\"41\" height=\"32\" border=\"0\" id=\"q4_eattendance\"></a></td> \n
										<td width=\"29\" align=\"center\"><a href=\"".$applyLeaveLink."\" class=\"indexquicklink\">".$ClassPendingApplyLeaveRecordCount."</a>&nbsp;&nbsp;</td> \n
									</tr> \n
									<tr> \n
										<td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\"><a href=\"".$applyLeaveLink."\" class=\"indexquickbtn\" >".$Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)']."</a></td> \n
									</tr> \n
									</table> \n ";
		}
	}
}
#####################################
# Apply Leave (app) Logo [End]
#####################################

#####################################
# Reprint Card (app) Logo [Start]
#####################################
if ($plugin["eClassApp"] && $sys_custom['eClassApp']['enableReprintCard'] && $_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancestudent"])
{
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"])
    {
        $numOfPendingReprintCardRecord = $lportal->getReprintCardPendingRecord();
        $numOfPendingReprintCardRecord = convertOverflowNumber($numOfPendingReprintCardRecord, $defultIconDigitLimit);
        $reprintCardLink = "/home/eAdmin/StudentMgmt/attendance/dailyoperation/reprint_card/";
        $LogoArr["ReprintCard(App)"] = "
                                <table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" OnMouseOver=\"this.className='handCursor';MM_swapImage('q_reprintcard','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_reprint_on.png',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='".$reprintCardLink."'\" > \n
                                <tr > \n
                                    <td height=\"32\" width=\"41\"><a href=\"".$reprintCardLink."\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_reprint_off.png\" name=\"q_reprintcard\" width=\"41\" height=\"32\" border=\"0\" id=\"q_reprintcard\"></a></td> \n
                                    <td width=\"29\" align=\"center\"><a href=\"".$reprintCardLink."\" class=\"indexquicklink\">".$numOfPendingReprintCardRecord."</a>&nbsp;&nbsp;</td> \n
                                </tr> \n
                                <tr> \n
                                    <td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\"><a href=\"".$reprintCardLink."\" class=\"indexquickbtn\" >".$Lang['StudentAttendance']['ReprintCard']['ReprintCard(App)']."</a></td> \n
                                </tr> \n
                                </table> \n ";

    }
}
#####################################
# Reprint Card (app) Logo [End]
#####################################

#####################################
# Medical Notice Logo [Start]
#####################################
if( ($plugin['medical']) ){
	include_once($PATH_WRT_ROOT."includes/cust/medical/libMedical.php");
	$objMedical = new libMedical();
	if(($plugin['medical_module']['message']) && ( $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='RECEIVE_NOTICE') ) ){
		include_once($PATH_WRT_ROOT."lang/cust/medical_lang.".$intranet_session_language.".php");
		include_once($PATH_WRT_ROOT."includes/cust/medical/libMedicalMessage.php");
		$objMedicalMessage = new MedicalMessage();
		$numOfUnreadMessage = $objMedicalMessage->getUnreadMessageCount($_SESSION['UserID']);
		$numOfUnreadMessage = convertOverflowNumber($numOfUnreadMessage, $defultIconDigitLimit);

		$medicalNoticeLink = '/home/eAdmin/StudentMgmt/medical/?t=message.readReceiveMessage';
		if ($sys_custom['medical']['CaringSystem']) {
			$small_logo = 'logo_caring_small2.png';
			$small_on_logo = 'logo_caring_small2_on.png';
		}
		else {
			$small_logo = 'logo_caring_small.png';
			$small_on_logo = 'logo_caring_small_on.png';
		}

		$LogoArr["MedicalMessage"] = "
	                           <table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" OnMouseOver=\"this.className='handCursor';MM_swapImage('q_medical','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/{$small_on_logo}',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='".$medicalNoticeLink."'\" > \n
	
								<tr > \n
									<td height=\"32\" width=\"41\"><a href=\"".$medicalNoticeLink."\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/{$small_logo}\" name=\"q_medical\" width=\"41\" height=\"32\" border=\"0\" id=\"q_medical\" style=\"margin-top: 2px;\"></a></td> \n
									<td width=\"29\" align=\"center\"><a href=\"".$medicalNoticeLink."\" class=\"indexquicklink\">".$numOfUnreadMessage."</a>&nbsp;&nbsp;</td> \n
								</tr> \n
								<tr> \n
									<td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\"><a href=\"".$medicalNoticeLink."\" class=\"indexquickbtn\" >".$Lang['medical']['module']."</a></td> \n
								</tr> \n
								</table> \n
								";
	}
}
#####################################
# Medical Notice Logo [End]
#####################################

#####################################
# ePost Logo [Start]
#####################################
if ($plugin['ePost']){
	include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
	$ePost = new libepost();

	if (!$ePost->check_is_expired()){
	    $showePost = true;
	    if ($sys_custom['ePost']['hiddenForParent'] && $_SESSION['UserType']==USERTYPE_PARENT) {
	        $showePost = false;
	    }

	    if ($showePost) {
    		$epost_path = "/home/ePost/";
    		$LogoArr["ePost"] = "
    			<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_epost','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_epost_forIP_on.png',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"newWindow('{$epost_path}', 36);\"  > \n
    			<tr> \n
    				<td width=\"41\" height=\"32\"><a href=\"javascript:newWindow('{$epost_path}', 36);\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_epost_forIP.png\" name=\"q_epost\" border=\"0\" id=\"q_epost\" /></a></td> \n
    				<td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
    			</tr> \n
    			<tr> \n
    				<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"javascript:newWindow('{$epost_path}', 36);\" class=\"indexquickbtn\">".$Lang['ePost']['ePost']."</a></td> \n
    			</tr> \n
    			</table> \n
    			";
	    }
	}
}
#####################################
# ePost Logo [End]
#####################################

#####################################
# FlippedChannels Logo [Start]
#####################################
if ($plugin['FlippedChannels'] && $UserType==USERTYPE_STAFF){
	$flipchan_path = "/home/asp/flipchan.php";
	$LogoArr["FlippedChannels"] = "
		<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_flipchan','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_flipchan_on.png',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"newWindow('{$flipchan_path}', 36);\"  > \n
		<tr> \n
			<td width=\"41\" height=\"32\"><a href=\"javascript:newWindow('{$flipchan_path}', 36);\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_flipchan.png\" name=\"q_flipchan\" border=\"0\" id=\"q_flipchan\" /></a></td> \n
			<td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
		</tr> \n
		<tr> \n
			<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"javascript:newWindow('{$flipchan_path}', 36);\" class=\"indexquickbtn\">".$Lang['General']['FlippedChannels']."</a></td> \n
		</tr> \n
		</table> \n
		";

}
#####################################
# FlippedChannels Logo [End]
#####################################

#####################################
# PowerFlip Logo [Start]
#####################################
if ($plugin['PowerFlip']){
	$powerflip_path = "/home/pl/index.php";
	$LogoArr["PowerFlip"] = "
		<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_powerflip','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_powerFlip_forIP_on.png',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"newWindow('{$powerflip_path}', 8);\"  > \n
		<tr> \n
			<td width=\"41\" height=\"32\"><a href=\"javascript:newWindow('{$powerflip_path}', 8);\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_powerFlip_forIP.png\" name=\"q_powerflip\" border=\"0\" id=\"q_powerflip\" /></a></td> \n
			<td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
		</tr> \n
		<tr> \n
			<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"javascript:newWindow('{$powerflip_path}', 8);\" class=\"indexquickbtn\">".$Lang['General']['PowerFlip']."</a></td> \n
		</tr> \n
		</table> \n
		";

}
#####################################
# PowerFlip Logo [End]
#####################################

#####################################
# PowerLesson2 (PL2.0) Logo [Start]
#####################################
if ($plugin['power_lesson_2'] && ($UserType == USERTYPE_STAFF || $UserType == USERTYPE_STUDENT)){
    $luser = new libuser($_SESSION['UserID']);

    $sessionKeepAliveTime = $session_expiry_time * 60;
    $memberType = ($luser->RecordType == USERTYPE_STAFF)? 'T' : 'S';

    if (isset($web_protocol) && preg_match('/^https?$/', $web_protocol)===1){
        $protocol = $web_protocol;
    } else {
        $protocol = 'http';
    }
    $powerLesson2_path = $protocol . '://' . $eclass40_httppath;
    $powerLesson2_path = trim($powerLesson2_path, '/')."/src/powerlesson/portal/login.php?enableUrlRedirect=1";

    $powerLesson2_icon_on = 'icon_powerlesson2_on.png';
    $powerLesson2_icon_off = 'icon_powerlesson2_off.png';

    $defaultLang = '';
    switch($intranet_session_language){
        case 'b5':
            $defaultLang = 'zh-hk';
            break;
        case 'gb':
            $defaultLang = 'zh-cn';
            break;
        default:
            $defaultLang = 'en';
    }

    $platformType    = 'intranet';
    $platformVersion = Get_IP_Version();

    $LogoArr["PowerLesson2"] = <<<HTML
    <script>
        $(document).ready(function(){
            var loadingPl2 = false;
            $('#pl2login').click(function(){
                if(!loadingPl2){
                    loadingPl2 = true;
                    $('#powerLessonLogin').submit();
                    setTimeout(function(){
                        loadingPl2 = false;
                    }, 5000);
                }
            });
        });
    </script>
    <a id="pl2login" href="javascript:void(0)"
    	onMouseOver="this.className='handCursor';MM_swapImage('q_powerlesson2','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/{$powerLesson2_icon_on}',1)" 
        onMouseOut="MM_swapImgRestore()" >
    	<img src="{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/{$powerLesson2_icon_off}" name="q_powerlesson2" border="0" id="q_powerlesson2" />
    </a>

    <form id="powerLessonLogin" name="powerLessonLogin" style="display:none;" action="{$powerLesson2_path}" target="PowerLesson20" method="POST">
        <input type="hidden" name="sessionKey" value="{$luser->sessionKey}" />
        <input type="hidden" name="sessionKeepAliveTime" value="{$sessionKeepAliveTime}" />
        <input type="hidden" name="defaultLang" value="{$defaultLang}" />
        <input type="hidden" name="platformType" value="{$platformType}" />
        <input type="hidden" name="platformVersion" value="{$platformVersion}" />
    </form>
HTML;
}
#####################################
# PowerLesson2 (PL2.0) Logo [End]
#####################################

#####################################
# STEM x PL2 Logo [Start]
#####################################
if($plugin['stem_x_pl2'] && ($UserType == USERTYPE_STAFF || $UserType == USERTYPE_STUDENT)){
    $LogoArr["stem_x_pl2"] = <<<HTML
    <script>
        $(document).ready(function(){
            $.ajax({
                url : '/home/stem/powerlesson/access.php',
                success : function(response){
                    var icon_off = '{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_pl2stem_off.png';
                    var icon_on  = '{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_pl2stem_on.png';
                
                    $('#q_stem_x_pl2').attr('src', icon_off);
                    $('#stem_x_pl2').click(function(){
                        window.open('/home/stem/powerlesson/sso.php', 'StemPl2');
                    }).hover(
                        function(){
                            $('#q_stem_x_pl2').attr('src', icon_on);
                        }, 
                        function(){
                            $('#q_stem_x_pl2').attr('src', icon_off);
                        }
                    );
                }
            });
        });
    </script>
    <a id="stem_x_pl2" href="javascript:void(0)">
    	<img src="{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_pl2stem_disabled.png" name="q_stem_x_pl2" border="0" id="q_stem_x_pl2" />
    </a>
HTML;
}
#####################################
# STEM x PL2 Logo [End]
#####################################

#####################################
# Student Data Analysis System Logo [Start]
#####################################
if( ($plugin['StudentDataAnalysisSystem']) ){
// 	include_once($PATH_WRT_ROOT."includes/libportfolio.php");
// 	$libpf = new libportfolio();
// 	$sdasAccessRight = $libpf->getAssessmentStatReportAccessRight();
	include_once($PATH_WRT_ROOT."includes/cust/student_data_analysis_system/libSDAS.php");
	$libSDAS= new libSDAS();
	$sdasAccessRight = $libSDAS->getAssessmentStatReportAccessRight();

	$AccessRightChecking = ($_SESSION["SSV_USER_ACCESS"]["other-SDAS"]) ||
	($sdasAccessRight['admin']) ||
	( !$plugin['SDAS_module']['accessRight']['blockSubjectPanel'] && $sdasAccessRight['subjectPanel'] ) ||
	( !$plugin['SDAS_module']['accessRight']['blockClassTeacher'] && $sdasAccessRight['classTeacher'] ) ||
	( !$plugin['SDAS_module']['accessRight']['blockSubjectTeacher'] && $sdasAccessRight['subjectTeacher'] ) ||
	( !$plugin['SDAS_module']['accessRight']['MonitoringGroupPIC'] && $sdasAccessRight['MonitoringGroupPIC'] ) ||
	( $libSDAS->isAccessGroupMember());

	if($sys_custom['SDAS']['OnlyAdminAccess']){
	    $AccessRightChecking = 	($sdasAccessRight['admin']);
    }

	if( $AccessRightChecking){

		$sdasLink = '/home/student_data_analysis_system/';

		if($plugin['StudentDataAnalysisSystem_Style_User'][$lu->UserLogin]){
			$styleFolder = $plugin['StudentDataAnalysisSystem_Style_User'][$lu->UserLogin];
			$iconPath_off = "{$image_path}/{$LAYOUT_SKIN}/StudentAnalysisDataSystem/{$styleFolder}/icon_sdas.png";
			$iconPath_on = "{$image_path}/{$LAYOUT_SKIN}/StudentAnalysisDataSystem/{$styleFolder}/icon_sdas_on.png";
		}else if($plugin['StudentDataAnalysisSystem_Style']){
			$styleFolder = $plugin['StudentDataAnalysisSystem_Style'];
			$iconPath_off = "{$image_path}/{$LAYOUT_SKIN}/StudentAnalysisDataSystem/{$styleFolder}/icon_sdas.png";
			$iconPath_on = "{$image_path}/{$LAYOUT_SKIN}/StudentAnalysisDataSystem/{$styleFolder}/icon_sdas_on.png";
		}else{
			$iconPath_off = "{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_sdas.png";
			$iconPath_on = "{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_sdas_on.png";
		}
		$sdasSkin = ($plugin['StudentDataAnalysisSystem_Style'])?$plugin['StudentDataAnalysisSystem_Style']:'general';

		$LogoArr["SDAS"] = "
			<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_sdas','','{$iconPath_on}',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"newWindow('{$sdasLink}', 31);\"  > \n
			<tr > \n
				<td width=\"41\" height=\"32\"><a href=\"javascript:newWindow('{$sdasLink}', 31);\"><img src=\"{$iconPath_off}\" name=\"q_sdas\" border=\"0\" id=\"q_sdas\" /></a></td> \n
				<td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
			</tr> \n
			<tr> \n
				<td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"javascript:newWindow('{$sdasLink}', 31);\" class=\"indexquickbtn\">".$Lang['Header']['Menu']['StudentDataAnalysisSystem'][$sdasSkin]."</a></td> \n
			</tr> \n
			</table> \n
		";
// 	}else if($sdasAccessRight['MonthlyReportPIC']){
	}else if ($libSDAS->isMonthlyReportPIC($_SESSION['UserID'])){
		$sdasLink = '/home/cees/monthlyreport';
		 if($plugin['StudentDataAnalysisSystem_Style']){
			$styleFolder = $plugin['StudentDataAnalysisSystem_Style'];
			$iconPath_off = "{$image_path}/{$LAYOUT_SKIN}/StudentAnalysisDataSystem/{$styleFolder}/icon_sdas.png";
			$iconPath_on = "{$image_path}/{$LAYOUT_SKIN}/StudentAnalysisDataSystem/{$styleFolder}/icon_sdas_on.png";
		}else{
			$iconPath_off = "{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_sdas.png";
			$iconPath_on = "{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_sdas_on.png";
		}
		$sdasSkin = ($plugin['StudentDataAnalysisSystem_Style'])?$plugin['StudentDataAnalysisSystem_Style']:'general';

		$LogoArr["SDAS"] = "
		<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_sdas','','{$iconPath_on}',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"newWindow('{$sdasLink}', 31);\"  > \n
		<tr > \n
		<td width=\"41\" height=\"32\"><a href=\"javascript:newWindow('{$sdasLink}', 31);\"><img src=\"{$iconPath_off}\" name=\"q_sdas\" border=\"0\" id=\"q_sdas\" /></a></td> \n
		<td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
		</tr> \n
		<tr> \n
		<td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"javascript:newWindow('{$sdasLink}', 31);\" class=\"indexquickbtn\">".$Lang['Header']['Menu']['StudentDataAnalysisSystem'][$sdasSkin]."</a></td> \n
			</tr> \n
			</table> \n
		";
	}
}
#####################################
# Student Data Analysis System Logo [End]
#####################################

###################################################################
# MDM Logo [Start]
###################################################################

if($plugin['eSchoolPad_MDM']){
// 	include_once($PATH_WRT_ROOT."includes/libuser.php");
// 	$luser = new libuser($_SESSION['UserID']);
	if($_SESSION['SSV_USER_ACCESS']['eAdmin-eSchoolPad_MDM']){
		$can_access_mdm = true;
// 	}else if($luser->isTeacherStaff()){
// 		$can_access_mdm = false;
// 	}else if($luser->isStudent()){
// 		$can_access_mdm = false;
	}else{
		$can_access_mdm = false;
	}
	if($can_access_mdm){
		$iconPath_off = "{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_mdm.png";
		$iconPath_on = "{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_mdm_on.png";
		$Link='mdm_sso.php';
		$LogoArr["eSchoolPad_MDM"] = "
		<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_sdas','','{$iconPath_on}',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"newWindow('{$Link}', 31);\"  > \n
		<tr> \n
		<td width=\"41\" height=\"32\"><a href=\"javascript:newWindow('{$Link}', 31);\"><img src=\"{$iconPath_off}\" name=\"q_sdas\" border=\"0\" id=\"q_sdas\" /></a></td> \n
		<td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
		</tr> \n
		<tr> \n
		<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"javascript:newWindow('{$Link}', 31);\" class=\"indexquickbtn\">".$Lang['Header']['Menu']['MDM']."</a></td> \n
			</tr> \n
			</table> \n
		";
	}
}
###################################################################
# MDM Logo [End]
###################################################################

#####################################
# Google Mail Logo [Start]
#####################################
if($ssoservice["Google"]["Valid"] == true && $ssoservice["Google"]['service_icon']['gmail'] == true && in_array($_SESSION['UserType'], array(1,2)) && (!isset($special_feature['hide_GMail_on_SSO']) || $special_feature['hide_GMail_on_SSO']==false)){
	// Google Drive - https://drive.google.com/a/eclass.uccke.edu.hk/#my-drive
	$Lang['General']['GoogleMail'] = "GMail";
	//$googlesso_gmail_path = "https://mail.google.com/a/".$_SESSION['SignInWithGoogle_UserEmailSuffix'];
	//$googlesso_gmail_path = "../templates/login.googlesso.php?srv=gmail";
	/*
	$LogoArr["GMail"] = <<<HTML
    	<img src="{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_googlemail.png" onclick="signInWithGoogle('gmail')" style="cursor: pointer;width:70px; height:68px;" />
HTML;*/
	$LogoArr["GMail"] = "
		<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_googlemail','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_gmail_on.png',1)\" onMouseOut=\"MM_swapImgRestore()\"  > \n
		<tr> \n
			<td width=\"41\" height=\"32\"><a href=\"javascript:newWindow(signInWithGoogle('gmail'),8);\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_gmail_off.png\" name=\"q_googlemail\" border=\"0\" id=\"q_googlemail\" /></a></td> \n
			<td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
		</tr> \n
		<tr> \n
			<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"javascript:newWindow(signInWithGoogle('gmail'),8);\" class=\"indexquickbtn\">".$Lang['General']['GoogleMail']."</a>" .
					"<form id=\"gmail_click_form\" action=\"#\" target=\"_blank\"></form>" .
					"</td> \n
		</tr> \n
		</table> \n
		";

}
#####################################
# Google Mail [End]
#####################################

#####################################
# DSI Logo [Start]
#####################################
if($ssoservice["dsi"]["Valid"] == true && in_array($_SESSION['UserType'], array(1))){
    $Lang['General']['DsiSso'] = "DSI";

    $LogoArr["DSI"] = "
    <table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_dsi','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_dsi_on.png',1)\" onMouseOut=\"MM_swapImgRestore()\"  > \n
    <tr> \n
    <td width=\"70\" height=\"70\"><a href=\"#\" onClick=\"javascript:newWindow(signInWithDsi(),8); return false;\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_dsi_off.png\" name=\"q_dsi\" border=\"0\" id=\"q_dsi\" /></a></td> \n
    </tr> \n
	</table> \n
	";
}
#####################################
# DSI Logo [End]
#####################################

###################################################################
# PowerBoard Lite Logo [Start]
###################################################################
if($plugin['PowerPad_Lite']){
	$libdb = new libdb();
	$access_user_arr = array();
	include_once('../plugins/pb_lite_config.php');
	$sql = "SELECT u.UserLogin FROM POWERPAD_LITE_USER ppl INNER JOIN INTRANET_USER u ON ppl.UserID=u.UserID";
	$UserList = $libdb->returnVector($sql);
	if($plugin['PowerPad_Lite_Quota']==99999){
		$ssoservice["PowerPad_Lite"]["userList"] = "";
	}else if(empty($UserList)){
		$ssoservice["PowerPad_Lite"]["userList"]= "-1";
	}else{
		$ssoservice["PowerPad_Lite"]["userList"] = "'".implode("','",$UserList)."'";
	}
	if(!empty($ssoservice["PowerPad_Lite"]["userList"])){
		$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin IN (".$ssoservice["PowerPad_Lite"]["userList"].")";
		$access_user_arr = $libdb->returnVector($sql);
	}
	if(in_array($_SESSION['UserID'],$access_user_arr) || empty($ssoservice["PowerPad_Lite"]["userList"])){
		$iconPath_off = "{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_powerpad_lite_off.png";
		$iconPath_on = "{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_powerpad_lite_on.png";
		$Link='pb_lite_sso.php';
		$LogoArr["PowerPad_Lite"] = "
			<a href='".$Link."' target='_blank'
	    	onMouseOver='this.className=\"handCursor\";MM_swapImage(\"q_powerpad_lite\",\"\",\"$iconPath_on\",1)'
	        onMouseOut='MM_swapImgRestore()' >
	    	<img src='".$iconPath_off."' name='q_powerpad_lite' border='0' id='q_powerpad_lite' />
	    </a>
		";
	}
}
###################################################################
# PowerBoard Lite Logo [End]
###################################################################


###################################################################
##### eReportCard (Kindergarten) Logo [Start]
###################################################################
if($plugin['ReportCardKindergarten'])
{
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCardKindergarten"] || $_SESSION["SSV_PRIVILEGE"]["reportcard_kindergarten"]["is_class_teacher"])
    {
        include_once($PATH_WRT_ROOT."lang/reportcardkindergarten_lang.$intranet_session_language.php");

        $reportcard_kg_student_link = '/home/eAdmin/StudentMgmt/reportcard_kindergarten/index.php?task=lesson.lesson_student_view.index';
        $LogoArr["eReportCardKG_Student"] = "
        <table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_erc_kg_student','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_reportcard_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='$reportcard_kg_student_link'\" > \n
        <tr > \n
        <td width=\"41\" height=\"32\"><a href=\"$reportcard_kg_student_link\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_reportcard_off.gif\" name=\"q_erc_kg_student\" border=\"0\" id=\"q_erc_kg_student\" /></a></td> \n
        <td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
        </tr> \n
        <tr> \n
        <td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"$reportcard_kg_student_link\" class=\"indexquickbtn\" >{$Lang['eReportCardKG']['Management']['Device']['LearningZone']} {$Lang['eReportCardKG']['Title']}</a></td> \n
        </tr> \n
        </table> \n";

        $reportcard_kg_teacher_link = '/home/eAdmin/StudentMgmt/reportcard_kindergarten/index.php?task=lesson.lesson_teacher_view.index';
        $LogoArr["eReportCardKG_Teacher"] = "
        <table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_erc_kg_teacher','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_reportcard_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='$reportcard_kg_teacher_link'\" > \n
        <tr > \n
        <td width=\"41\" height=\"32\"><a href=\"$reportcard_kg_teacher_link\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_reportcard_off.gif\" name=\"q_erc_kg_teacher\" border=\"0\" id=\"q_erc_kg_teacher\" /></a></td> \n
        <td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
        </tr> \n
        <tr> \n
        <td ".$indexquickbtnslimClass." align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"$reportcard_kg_teacher_link\" class=\"indexquickbtn\" >{$Lang['eReportCardKG']['Management']['Device']['InputScore']} {$Lang['eReportCardKG']['Title']}</a></td> \n
        </tr> \n
        </table> \n";
    }
}
###################################################################
##### eReportCard (Kindergarten) Logo [End]
###################################################################


###################################################################
##### Britannica School Logo [Start]
###################################################################
if($plugin['BritannicaSchool'])
{
    switch($intranet_session_language){
        case 'b5': $locale = 'zh-hk'; break;
        case 'gb': $locale = 'zh-cn'; break;
        default: $locale = 'en';
    }

    $iconPath_off = "{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_britannica_sch_off.png";
    $iconPath_on = "{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_britannica_sch_on.png";
    $Link = "/home/lti/britannica/launch.php?locale={$locale}&module=BritannicaSchool";

    $LogoArr["BritannicaSchool"] = <<<HTML
    <a href="javascript:window.open('{$Link}', 'britannica');" onMouseOver="this.className='handCursor';MM_swapImage('britannicaSch','','{$iconPath_on}',1)" onMouseOut="MM_swapImgRestore()">
        <img src="{$iconPath_off}" name="britannicaSch" border="0" id="britannicaSch" />
	</a>
HTML;
}
###################################################################
##### Britannica School Logo [End]
###################################################################


###################################################################
##### Britannica Launch Packs Science Logo [Start]
###################################################################
if($plugin['BritannicaLaunchPacksScience'])
{
    switch($intranet_session_language){
        case 'b5': $locale = 'zh-hk'; break;
        case 'gb': $locale = 'zh-cn'; break;
        default: $locale = 'en';
    }

    $iconPath_off = "{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_britannica_sci_off.png";
    $iconPath_on = "{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_britannica_sci_on.png";
    $Link = "/home/lti/britannica/launch.php?locale={$locale}&module=BritannicaLaunchPacksScience";

    $LogoArr["BritannicaLaunchPacksScience"] = <<<HTML
    <a href="javascript:window.open('{$Link}', 'britannica');" onMouseOver="this.className='handCursor';MM_swapImage('britannicaLpS','','{$iconPath_on}',1)" onMouseOut="MM_swapImgRestore()">
        <img src="{$iconPath_off}" name="britannicaLpS" border="0" id="britannicaLpS" />
	</a>
HTML;
}
###################################################################
##### Britannica Launch Packs Science Logo [End]
###################################################################


#########################################################################
##### Britannica Launch Packs Humanities and Social Sciences Logo [Start]
#########################################################################
if($plugin['BritannicaLaunchPacksHumanitiesAndSocialSciences'])
{
    switch($intranet_session_language){
        case 'b5': $locale = 'zh-hk'; break;
        case 'gb': $locale = 'zh-cn'; break;
        default: $locale = 'en';
    }

    $iconPath_off = "{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_britannica_hum_off.png";
    $iconPath_on = "{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_britannica_hum_on.png";
    $Link = "/home/lti/britannica/launch.php?locale={$locale}&module=BritannicaLaunchPacksHumanitiesAndSocialSciences";

    $LogoArr["BritannicaLaunchPacksHumanitiesAndSocialSciences"] = <<<HTML
    <a href="javascript:window.open('{$Link}', 'britannica');" onMouseOver="this.className='handCursor';MM_swapImage('britannicaLpHss','','{$iconPath_on}',1)" onMouseOut="MM_swapImgRestore()">
        <img src="{$iconPath_off}" name="britannicaLpHss" border="0" id="britannicaLpHss" />
	</a>
HTML;
}
#########################################################################
##### Britannica Launch Packs Humanities and Social Sciences Logo [End]
#########################################################################


###################################################################
##### Britannica Image Quest [Start]
###################################################################
if($plugin['BritannicaImageQuest'])
{
    switch($intranet_session_language){
        case 'b5': $locale = 'zh-hk'; break;
        case 'gb': $locale = 'zh-cn'; break;
        default: $locale = 'en';
    }

    $iconPath_off = "{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_britannica_iq_off.png";
    $iconPath_on = "{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_britannica_iq_on.png";
    $Link = "/home/lti/britannica/launch.php?locale={$locale}&module=BritannicaImageQuest";

    $LogoArr["BritannicaImageQuest"] = <<<HTML
    <a href="javascript:window.open('{$Link}', 'britannica');" onMouseOver="this.className='handCursor';MM_swapImage('britannicaIQ','','{$iconPath_on}',1)" onMouseOut="MM_swapImgRestore()">
        <img src="{$iconPath_off}" name="britannicaIQ" border="0" id="britannicaIQ" />
	</a>
HTML;
}
###################################################################
##### Britannica Image Quest [End]
###################################################################


####################################################################################################
## Build shortcut icon array [Start]
####################################################################################################
# Build Shourtcut Icons table
$LogoTable = "";
if (is_array($LogoArr) && count($LogoArr)>0)
{
	$CeilCnt = 0;
	$LogoTable .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
	foreach ($LogoArr as $Key => $Value)
	{
		if ($CeilCnt%3 == 0)
		{
			$LogoTable .= "<tr>";
		}
		$LogoTable .= "<td align=\"center\" valign=\"top\" >{$Value}</td>";
		if ($CeilCnt%3 == 2)
		{
			$LogoTable .= "</tr>";
		}
		$CeilCnt++;
	}
	$RemainCell = count($LogoArr) %3;
	if ($RemainCell >0)
	{
		for ($i=0;$i<(3-$RemainCell);$i++)
		{
			$LogoTable .= "<td>&nbsp;</td>";
		}
		$LogoTable .= "</tr>";
	}
	$LogoTable .= "</table>";
}

###################################################################
# Events Menu
###################################################################
if ($EventType == "")
{
	$EventType = 0;
}
// $EventType=1;
if ($EventType==0)
{
	$EventMenu = "
						<table width=\"100%\" height=\"22\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
						<tr>
							<td width=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_on_left.gif\" width=\"4\" height=\"22\" /></td>
							<td align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_on_bg.gif\" class=\"indexeventtitleon\">{$ip20_event_month}</td>
							<td width=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_middle_on_off.gif\" width=\"8\" height=\"22\"></td>
							<td align=\"center\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_off_bg.gif\"><a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_EVENT(document.EventForm,1)\" class=\"indexeventtitleoff\">{$ip20_event_today}</a> </td>
							<td width=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_off_right.gif\" width=\"8\" height=\"22\"></td>
						</tr>
						</table>
						";
	$EventContent =  $li->displayEventType_Portal(1);
	//$EventContent =  $li->displayEventType(1);
} else {
	$EventMenu = "
						<table width=\"100%\" height=\"22\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
						<tr>
							<td width=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_off_left.gif\" width=\"4\" height=\"22\"></td>
							<td align=\"center\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_off_bg.gif\"><a  href=\"javascript:void(0)\" onclick=\"jAJAX_GO_EVENT(document.EventForm,0)\" class=\"indexeventtitleoff\">{$ip20_event_month}</a> </td>
							<td width=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_middle_off_on.gif\" width=\"8\" height=\"22\"></td>
							<td align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_on_bg.gif\" class=\"indexeventtitleon\">{$ip20_event_today}</td>
							<td width=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_on_right.gif\" width=\"8\" height=\"22\"></td>
						</tr>
						</table>
						";
	$EventContent =  $li->displayEventType_Portal(0);
	//$EventContent =  $li->displayEventType(0);
}
###################################################################
# Events Menu [End]
###################################################################


###################################################################
# My Group
###################################################################
# Get all groups selection
// [2015-0728-1228-52073] added condition to display identity group in School News > Group - AcademicYearID IS NULL
// improved: display group name language according to UI
if(!isset($LibUser)){
    $LibUser = new libuser($UserID);
}
$groups = $LibUser->returnGroupsWithFunctionAdmin(Get_Current_Academic_Year_ID(),1);
$availableGroup = array();
for ($i=0; $i<sizeof($groups); $i++)
{
    list ($groupsid, $groupsname, $groupsfunction, $groupsadmin, $groupstype) = $groups[$i];

    $outputResultDateTime = $lclubsenrol->AnnounceEnrolmentResultDate." ".$lclubsenrol->AnnounceEnrolmentResultHour.':'.$lclubsenrol->AnnounceEnrolmentResultMin;
    $currentDateTime = date('Y-m-d H:i');
    $userinEnrolGroup = $LibUser->approvedUserinEnrol($UserID);
    $alreadyExist = $LibUser->groupRecordExist($UserID, $groupsid);

    if($groupstype == 5 && $currentDateTime < $outputResultDateTime && sizeof($userinEnrolGroup) > 0 && sizeof($alreadyExist) == 0){
    }else{
        $availableGroup[] = $groupsid;
    }
}
$outputGroup = implode(", ", $availableGroup);

$title_field = Get_Lang_Selection("IF(a.TitleChinese IS NULL OR a.TitleChinese = '', a.Title, a.TitleChinese)","a.Title");
$sql = "SELECT
			distinct(a.GroupID), ".$title_field." as Title
		FROM
			INTRANET_GROUP as a, INTRANET_USERGROUP as b
		WHERE
			a.GroupID = b.GroupID AND b.UserID = $UserID
			and (AcademicYearID=". Get_Current_Academic_Year_ID() ." OR AcademicYearID IS NULL)
            and b.GroupID IN (".$outputGroup.")
		ORDER BY
			a.RecordType+0, Title
	 ";
$result = $la->returnArray($sql,2);
$select_group = getSelectByArray($result,"name='GroupID' onChange=\"jAJAX_GO_CROUP_ANNOUNCE(document.AnnounceForm,this.value)\" class=\"tabletext\" ",$GroupID,1,0, $i_All_MyGroup);
$GroupForm .= $select_group;

# Get my group list
$row = $GroupID != "" ? array($GroupID) : $lu->returnGroupIDs();
$myGroupList = $la->displayGroupAnnounce($row);
###################################################################
# My Group [End]
###################################################################

###################################################################
# Protal TV
###################################################################
$is_portal_tv = false;
if ($plugin['tv'])
{
	$PortalTVinfo = $lportal->returnTVinfo();
	if($PortalTVinfo['live_show'])
	{
		# retrieve live movie
		$movie_url = $lportal->returnLiveURL();
 		$channelID = 0;

		if (trim($movie_url)!="")
		{
		        $live_width = $PortalTVinfo['live_width'];
		        $live_height = $PortalTVinfo['live_height'];
		        $live_auto_start = "true";
		        $is_portal_tv = true;
		}
	}
}

if ($is_portal_tv)		# may need to change the if condition
{
	$TVContent ="
		<tr>
			<td valign=\"top\">
				<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\">
				<tr>
					<td width=\"7\"><img src=\"/images/2009a/index/campusTV/tv_board_01.gif\" width=\"7\" height=\"20\"></td>
					<td background=\"/images/2009a/index/campusTV/tv_board_02.gif\" height=\"20\">
						<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
						<tr>
							<td class=\"indextvtitle\">eTV</td>
							<td align=\"right\"><a href=\"javascript:void(0)\" onclick=\"displayTable('tvContent')\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/arrow_down_off.gif\" name=\"tv_arrow\" width=\"9\" height=\"6\" border=\"0\" id=\"tv_arrow\" onMouseOver=\"MM_swapImage('tv_arrow','','/images/2009a/index/arrow_down_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a></td>
						</tr>
						</table>
					</td>
					<td width=\"7\"><img src=\"/images/2009a/index/campusTV/tv_board_03.gif\" width=\"7\" height=\"20\"></td>
				</tr>
				<tr>
					<td width=\"7\" background=\"/images/2009a/index/campusTV/tv_board_04.gif\"><img src=\"/images/2009a/10x10.gif\" width=\"7\" height=\"10\"></td>
					<td align=\"center\" background=\"/images/2009a/index/campusTV/tv_board_05.gif\">

						<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" height=\"100%\">
						<tr id=\"tvContent\">
							<td align=\"center\">
								<!-- TV content //-->
								<div id=\"div_tv_movie_area\" style='z-index:1'></div>
								<!-- TV content [End] //-->
							</td>
						</tr>
						</table>

					</td>
					<td width=\"7\" background=\"/images/2009a/index/campusTV/tv_board_06.gif\"><img src=\"/images/2009a/10x10.gif\" width=\"7\" height=\"10\"></td>
				</tr>
				<tr>
					<td width=\"7\" height=\"7\"><img src=\"/images/2009a/index/campusTV/tv_board_07.gif\" width=\"7\" height=\"7\"></td>
					<td background=\"/images/2009a/index/campusTV/tv_board_08.gif\" height=\"7\"><img src=\"/images/2009a/index/campusTV/tv_board_08.gif\" width=\"7\" height=\"7\"></td>
					<td width=\"7\" height=\"7\"><img src=\"/images/2009a/index/campusTV/tv_board_09.gif\" width=\"7\" height=\"7\"></td>
				</tr>
				</table>
			</td>
		</tr>
	";
}
###################################################################
# Protal TV [End]
###################################################################

###################################################################
# eLib+ [Start]
###################################################################
// copied from "/home/library_sys/lib/liblibrarymgmt_periodical" function GET_PERIODICAL_RENEW_ALERT_INFO()
if ($plugin['library_management_system'] && ($_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] || $_SESSION['LIBMS']['admin']['current_right']['admin']['book management'])) {
	$lmsDb = $eclass_prefix . "eClass_LIBMS";
	$sql = "Select
					lpb.PeriodicalBookID, lpb.PeriodicalCode, lpb.BookTitle, lpo.OrderEndDate, lpo.PublishDateLast, lpo.Remarks
			From
					$lmsDb.LIBMS_PERIODICAL_BOOK as lpb
					Inner Join $lmsDb.LIBMS_PERIODICAL_ORDER as lpo On (lpb.PeriodicalBookID = lpo.PeriodicalBookID)
			Where
					lpb.RecordStatus = 1
					And lpo.RecordStatus = 1
					And lpo.AlertDay > 0
					And DATE_SUB(lpo.OrderEndDate, INTERVAL lpo.AlertDay DAY) <= CURDATE()
					And CURDATE() <= lpo.OrderEndDate
			Order By
					lpo.OrderEndDate
			";
	$orderAry = $li->returnResultSet($sql);
	$numOfOrder = count((array)$orderAry);

	$showLibraryPeriodicalOrderAlert = false;
	if ($numOfOrder > 0) {
		$showLibraryPeriodicalOrderAlert = true;
	}
}

###################################################################
# eLib+ [End]
###################################################################


###################################################################
# eForm [Start]
###################################################################
if ($plugin["eForm"] && $_SESSION["UserType"] == USERTYPE_STAFF && !isset($_SESSION["eForm"]["FormExpiredAlert"])) {
	###### eForm Reminder ( Check Form expired) #######
	$eformLibPath = dirname(dirname(__FILE__)) . "/includes/eForm/";
	$eformLangPath = dirname(dirname(__FILE__)) . "/lang/eForm/";
	if (file_exists($eformLibPath . "libeform.php")) include_once($eformLibPath . "libeform.php");
	if (file_exists($eformLibPath . "libeform_ui.php")) include_once($eformLibPath . "libeform_ui.php");
	if (file_exists($eformLibPath . "class.formcommon.php")) include_once($eformLibPath . "class.formcommon.php");
	if (file_exists($eformLibPath . "class.formgrid.php")) include_once($eformLibPath . "class.formgrid.php");

	if (file_exists($eformLangPath. "eform.lang." . $intranet_session_language . ".php")) include_once($eformLangPath. "eform.lang." . $intranet_session_language . ".php");

	if (class_exists("FormGrid")) {
		$libeform_ui = new libeform_ui();
		$libformgrid = new FormGrid("eForm");
		$args = array(
				"UserID" => $_SESSION["UserID"],
				"Status" => "eform_expired"
		);
		$eFormGridInfo = $libformgrid->getTableGridInfo($args, false);
		$eFormAlertDatas = $libformgrid->returnResultSet($eFormGridInfo["sql"]);
		if (count($eFormAlertDatas) > 0) {
			$eFormPromptHTML = $libeform_ui->getPromptHTML($eFormAlertDatas);
		}
	}
	$_SESSION["eForm"]["FormExpiredAlert"] = true;
}
###################################################################
# eForm [End]
###################################################################

?>
<? if(!($sys_custom['OnlineRegistry'] && $_SESSION['ParentFillOnlineReg'] && $_SESSION['ParentFillOnlineRegMode']=="new")) {?>
<script><!-- used to enlarge the layout //--></script>
<? } ?>
<?
if($bubble!="")
	$showNewFeature = 1;


$CurrentPageArr['Home'] = 1;
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/home_header.php");


$htmlAry['hkpfPortal'] = '';
if ($sys_custom['project']['HKPF']) {
    $prev_month_link = "<a href='#' onClick='jsChangeMonthByArrow(-1)'><img height='21' border='0' align='absmiddle' width='21' src='".$image_path."/".$LAYOUT_SKIN."/icalendar/icon_prev_off.gif'></a>";
    $next_month_link = "<a href='#' onClick='jsChangeMonthByArrow(+1)'><img height='21' border='0' align='absmiddle' width='21' src='".$image_path."/".$LAYOUT_SKIN."/icalendar/icon_next_off.gif'></a>";

    $selectDateLayer = '';
    $selectDateLayer .= $prev_month_link;

    # Get Month selection
    $numMonth =count($Lang['General']['month']);
    for($i = 0; $i<$numMonth ; $i++){
        if($i != 0 && $i != 13){
            $monthArr[($i-1)] = $Lang['General']['month'][$i];
        }
    }
    $curMonth = date('m');
    $selectDateLayer .= getSelectByAssoArray($monthArr, 'id="monthSelect" onChange="javascript:isTargetCalendar();"','',0,1);

    # Get Year selection
    $curYear = date('Y');
    $yearArr[$curYear-1] = $curYear-1;
    $yearArr[$curYear] = $curYear;
    $yearArr[$curYear+1] = $curYear+1;
    $selectDateLayer .= getSelectByAssoArray($yearArr, 'id="yearSelect" onChange="javascript:isTargetCalendar();"','',0,1);

    # Go Btn
    $selectDateLayer .= $linterface->GET_SMALL_BTN($Lang['ePost']['Filter']['Go'], "button", "jsChangeMonth();",'goBtn');
    $selectDateLayer .= $next_month_link;

    $htmlAry['hkpfPortal'] .= '<tr>'."\r\n";
        $htmlAry['hkpfPortal'] .= '<td>'."\r\n";
        $htmlAry['hkpfPortal'] .= '<br />'."\r\n";
            $htmlAry['hkpfPortal'] .= '<table border="0" width="95%" cellspacing="0" cellpadding="0" align="center">'."\r\n";
                $htmlAry['hkpfPortal'] .= '<tr>'."\r\n";
                    $htmlAry['hkpfPortal'] .= '<td>'."\r\n";
                        $htmlAry['hkpfPortal'] .= $linterface->GET_NAVIGATION2_IP25('Course Calendar');
                    $htmlAry['hkpfPortal'] .= '</td>'."\r\n";
                $htmlAry['hkpfPortal'] .= '</tr>'."\r\n";
            $htmlAry['hkpfPortal'] .= '</table>'."\r\n";
            $htmlAry['hkpfPortal'] .= '<div style="text-align:center;">'.$selectDateLayer.'</div>'."\r\n";
            $htmlAry['hkpfPortal'] .= '<table border="0" width="95%" cellspacing="0" cellpadding="0" align="center">'."\r\n";
                $htmlAry['hkpfPortal'] .= '<tbody>'."\r\n";
                    $htmlAry['hkpfPortal'] .= '<tr>'."\r\n";
                        $htmlAry['hkpfPortal'] .= '<td style="padding: 5px; background-color:rgb(219,237,255)">'."\r\n";
                            $htmlAry['hkpfPortal'] .= '<div id="CalendarDiv" style="background-color:#FFF;"></div>'."\r\n";
                            $htmlAry['hkpfPortal'] .= $linterface->GET_HIDDEN_INPUT('timeString2', 'timeString2', 'aaa');
                        $htmlAry['hkpfPortal'] .= '</td>'."\r\n";
                    $htmlAry['hkpfPortal'] .= '</tr>'."\r\n";
                $htmlAry['hkpfPortal'] .= '</tbody>'."\r\n";
            $htmlAry['hkpfPortal'] .= '</table>'."\r\n";
            $htmlAry['hkpfPortal'] .= '<br />'."\r\n";
            $htmlAry['hkpfPortal'] .= '<br />'."\r\n";
        $htmlAry['hkpfPortal'] .= '</td>'."\r\n";
    $htmlAry['hkpfPortal'] .= '</tr>'."\r\n";
}

?>
<?php if ($sys_custom['project']['HKPF']) { ?>
<script language="javascript">
var currentDate = new Date();
var currentMM = currentDate.getMonth();
var currentYYYY = currentDate.getFullYear();
// js month is start from 0
var correctMM =  (currentMM+1);
var MonthLang = new Array("<?=implode('","',$Lang['General']['month'])?>");

$(document).ready( function() {
	$('#CalendarDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	getCalendarView();
	isTargetCalendar();
});

function isTargetCalendar(){
	if($('#monthSelect').val() == currentMM && $('#yearSelect').val() == currentYYYY){
		$('#goBtn').attr('disabled','disabled');
	}
	else{
		$('#goBtn').removeAttr('disabled');
	}
}

function changeSelected(id,value){
	var ddl = document.getElementById(id);
	var opts = ddl.options.length;
	for (var i=0; i<opts; i++){
	    if (ddl.options[i].value == value){
	        ddl.options[i].selected = true;
	        break;
	    }
	}
}

function jsChangeMonth(){
	currentDate.setYear($('#yearSelect').val());
	currentDate.setMonth($('#monthSelect').val());
	currentDate.setDate('01');

	currentDD = currentDate.getDate();
	currentMM = currentDate.getMonth();
	correctMM =  (currentMM+1);
	currentYYYY = currentDate.getFullYear();
	getCalendarView();
}

function jsChangeMonthByArrow(change){
	//2016-03-31 Kenneth
	//Set Date First, set month after => cater bug of 03-31 -> 05-01
	currentDate.setDate('01');
	currentDate.setMonth(currentDate.getMonth() + change);
	currentDD = currentDate.getDate();
	currentMM = currentDate.getMonth();
	correctMM =  (currentMM+1);
	currentYYYY = currentDate.getFullYear();
	getCalendarView();
}

function getCalendarView(){
	changeSelected('monthSelect',currentMM);
	changeSelected('yearSelect',currentYYYY);
// 	displayLang = MonthLang[correctMM] + " - " + currentYYYY;
// 	$('#DisplayMonth').html(displayLang);
//	Block_Document();
	$.ajax({
				type: 'POST',
				url: "/home/eAdmin/StudentMgmt/enrollment/reports/calendar_view/ajax_task.php",
				data: {
						"action"		:	"getCalendarView",
						"timeString" 	:	currentYYYY + '-' + correctMM,
						"viewOnly"		:	1,
						"srcFrom"		:	"portal"
						},
				success: function(responseText){
					$('#CalendarDiv').html(responseText);
//					UnBlock_Document();
				}
	});
}
</script>
<?php } ?>
<?
/*
<!--
<script language="Javascript">


top.window.moveTo(0,0);
if (document.all)
{
	top.window.resizeTo(screen.availWidth,screen.availHeight);
}
else if (document.layers||document.getElementById)
{
	if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth)
	{
		top.window.outerHeight = screen.availHeight;
		top.window.outerWidth = screen.availWidth;
	}
}
</script>
-->*/
?>
<? /*
<script type="text/javascript" src="http://<?=$eclass_httppath?>/src/includes/js/ajax_yahoo.js" ></script>
<script type="text/javascript" src="http://<?=$eclass_httppath?>/src/includes/js/ajax_connection.js" ></script>
*/ ?>

<script language="javascript">

<!--
// Today's event row ID
var jsDefaultEventTrID = "Event_" + "<?=$CurDateForEventList?>";

var callback_event =
{
	success: function (o)
	{
		jChangeContent( "EventContentDiv", o.responseText );
		$('#EventInboxDiv').scrollTo($('#'+jsDefaultEventTrID), 800, {queue:true});
	}
}

var callback_announcement =
{
	success: function (o)
	{
		jChangeContent( "groupwhatnews", o.responseText );
	}
}

var callback_list =
{
	success: function (o)
	{
		jChangeContent( "ListContent", o.responseText );
	}
}

var callback_calender =
{
	success: function (o)
	{
		jChangeContent( "CalContent", o.responseText);
	}
}

var callback_wws =
{
	success: function (o)
	{
		jChangeContent( "WWSListContentDiv", o.responseText);
	}
}

function jAJAX_GO_EVENT( jFormObject,jParType, ParTS)
{
	jChangeContent( "EventInboxDiv", "<span class='tabletext' >&nbsp;<?=$ip20_loading?> ...</span>");
	jFormObject.type.value = jParType;
	if (typeof(ParTS)!="undefined" && ParTS>0)
		jFormObject.ts.value = ParTS;
	YAHOO.util.Connect.setForm(jFormObject);
	path = "index20_aj_event.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_event);
}

function jAJAX_GO_CROUP_ANNOUNCE( jFormObject, ParGroupID )
{
	jChangeContent( "groupwhatnews", "<span class='tabletext' ><?=$ip20_loading?> ...</span>");

	jFormObject.GroupID.value=ParGroupID;

	YAHOO.util.Connect.setForm(jFormObject);
	path = "index20_aj_announcement.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_announcement);
}

function jAJAX_GO_HOMEWORK( jFormObject)
{
	jChangeContent( "ListContentDiv", "<span class='tabletext' ><?=$ip20_loading?> ...</span>");

	YAHOO.util.Connect.setForm(jFormObject);
	path = "index20_aj_homework.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_list);
}

function jAJAX_GO_ECLASS( jFormObject)
{
	jChangeContent( "ListContentDiv", "<span class='tabletext' ><?=$ip20_loading?> ...</span>");

	YAHOO.util.Connect.setForm(jFormObject);
	path = "index20_aj_eclass.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_list);
}

function jAJAX_GO_ECLASS_WWS(jFormObject)
{
	jChangeContent( "WWSListContent", "<span class='tabletext' ><?=$ip20_loading?> ...</span>");

	YAHOO.util.Connect.setForm(jFormObject);
	path = "index20_aj_eclass_wws.php?roomType=13";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_wws);
}

function jAJAX_GO_TEXTBOOK( jFormObject)
{
	jChangeContent( "ListContentDiv", "<span class='tabletext' ><?=$ip20_loading?> ...</span>");

	YAHOO.util.Connect.setForm(jFormObject);
	path = "index20_aj_itextbook.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_list);
}

function jAJAX_GO_COMMUNITY( jFormObject)
{
	jChangeContent( "ListContentDiv", "<span class='tabletext' ><?=$ip20_loading?> ...</span>");

	YAHOO.util.Connect.setForm(jFormObject);
	path = "index20_aj_community.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_list);
}

function jAJAX_GO_CALENDER( jFormObject, ParV, ParTS )
{
	jChangeContent( "CalContentDiv", "<span class='tabletext' ><?=$ip20_loading?> ...</span>");

	jFormObject.v.value=ParV;
	jFormObject.ts.value=ParTS;

	YAHOO.util.Connect.setForm(jFormObject);
	path = "index20_aj_calender.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_calender);
}

function moreNews(type)
{
	newWindow('moreannouncement.php?type='+type,1);
}

function moreGroupNews(type, group)
{
	newWindow('moreannouncement.php?type='+type+'&group='+group,1);
}

function displayMovie(moviid)
{
	$('#div_tv_movie_area').load(
		'aj_tv_retrieve_movie.php',
		function (data){
		//alert($('#MediaPlayer1').width() + " x " + $('#MediaPlayer1').height());
		});
}
<? if($is_imail_gamma===true){ ?>
function getGammaNewMailNumber()
{
	$.get(
		"aj_gamma_get_new_mail.php",
		function(data){
			if(data==-1){
<? if(stristr($_SERVER["SCRIPT_NAME"], "/home/index.php")!==FALSE){ ?>
				alert("<?=$Lang['Gamma']['Warning']['CouldNotConnectMailServer']?>");
<? }?>
			}else if(data!=0){
				data = convertOverflowNumber(data, 2);
				$('#gamma_new_mail_number').html(data);
			}
	<?php if($sys_custom['iMailPlus']['QuotaAlert']){ ?>
			if(data!=-1)
			{
				$.get(
					"aj_get_mail_quota_alert.php",
					function(alert_msg){
						if(alert_msg != ''){
							alert(alert_msg);
						}
					}
				);
			}
	<?php } ?>
		}
	);
}
<? } ?>

function jsUpdateStudentPwdPopUp()
{
	newWindowWithSize('updatepwd.php', 1, 0, 500, 420);
}

function jAJAX_GO_CUST( jFormObject)
{
	jChangeContent( "CustDiv", "<span class='tabletext' ><?=$ip20_loading?> ...</span>");

	YAHOO.util.Connect.setForm(jFormObject);
	path = "aj_index_cust.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_cust);
}

var callback_cust =
{
	success: function (o)
	{
		jChangeContent( "CustDiv", o.responseText);
	}
}
//-->
</script>

<script language="Javascript" src='/templates/tooltip.js'></script>
<script language="Javascript">

top.window.moveTo(0,0);
if (document.all)
{
	top.window.resizeTo(screen.availWidth,screen.availHeight);
}
else if (document.layers||document.getElementById)
{
	if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth)
	{
		top.window.outerHeight = screen.availHeight;
		top.window.outerWidth = screen.availWidth;
	}
}

function jMAX_TABLE()
{
	var tableHeight = parseInt(document.body.clientHeight)-100;
	document.write('<table id="outertable1" width="100%" height="'+tableHeight+'" border="0" cellpadding="0" cellspacing="0"  >');
}

function jMAX_TABLE2()
{
	var tableHeight = parseInt(document.body.clientHeight)-109;
	var tableWidth = parseInt(document.body.clientWidth);
	document.all["outertable1"].style.height = tableHeight;
	document.all["outertable1"].style.width = tableWidth;
}
jMAX_TABLE();

window.onresize = function()
{
	jMAX_TABLE2();
	<?php
	if($motd<>"")
	{
	?>
		marqueewidth = (parseInt(document.body.clientWidth)-500);
		//document.getElementById("MovingTextDiv").style.width=marqueewidth;
	<?php
	}
	?>
}

function signInWithDsi() {
	return "/sso/dsi";
}

function signInWithGoogle(srv){
	var srv = srv || '';
	var url = '';
	$.ajax({
		url: "aj_ck_google_sso.php",
		success: function(data){
			if(data == 1){
				url = "../templates/login.googlesso.php?srv="+srv;
				//newWindow("../templates/login.googlesso.php?srv="+srv, 8);
			} else {
				<? if($ssoservice["Google"]['Enable_SubDomain']['blers'] == true){ ?>
					var GoogleSSOMsg = 'Please enter your google email address';
					tmp = prompt(GoogleSSOMsg, ".broadlearning.com");
					if(tmp!=null && Trim(tmp)!=""){
						url = "../templates/login.googlesso.php?srv="+srv+"&gmail="+tmp;
						//newWindow("../templates/login.googlesso.php?srv="+srv+"&gmail="+tmp, 8);
					}
				<? } else { ?>
					url = "../templates/login.googlesso.php?srv="+srv;
					//newWindow("../templates/login.googlesso.php?srv="+srv, 8);
				<? } ?>
			}
		},
		async: false
	});

	return url;

	/*
	<? if($ssoservice["Google"]['Enable_SubDomain']['blers'] == true && $_SESSION['SignInWithGoogle_UserEmail'] == ''){ ?>
	var GoogleSSOMsg = 'Please enter your google email address';
	tmp = prompt(GoogleSSOMsg, ".broadlearning.com");
	if(tmp!=null && Trim(tmp)!=""){
		newWindow("../templates/login.googlesso.php?srv="+srv+"&gmail="+tmp, 8);
	}
	<? } else { ?>
	newWindow("../templates/login.googlesso.php?srv="+srv, 8);
	<? } ?>
	*/
}
</script>

<tr>
<?php if (!$sys_custom['HidePortalLeftMenu']) { ?>
	<td width="210" height="100%" valign="top" style='background:url(<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/leftbg.gif) right'>

	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0"  >
	<tr>
		<td height="16" align="right" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/left_top_bg.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/left_top_corner.gif" width="24" height="16" /></td>
	</tr>
	<tr>
		<td height="100%" valign="top">
		<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td height="100%" align="right" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/left_bg.gif">

			<? ### Calendar & Events ### ?>
			<table height="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="right" height="240">
				<? ### Calendar ### ?>
				<table width="193" height="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td valign="bottom"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_01.gif" width="4" height="10" /></td>
					<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_02.gif" width="183" height="10" /></td>
					<td valign="bottom"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_03.gif" width="6" height="10" /></td>
				</tr>
				<tr>
					<td width="4" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_04.gif">
					<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_04.gif" width="4" height="209" />
					</td>
					<td valign="top" bgcolor="#FFFFFF">

					<span id="CalContent"  >
					<table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td class="indexcalendartoday" align="left"><?=$CurDate?></td>
							<td align="right" class="indexcalendarstoday"><?=$CurCycleDate?></td>
						</tr>
						</table>
						<span id="CalContentDiv"  >
						<?php
							echo $li->displayCalendar();
						?>
						</span>
						</td>
					</tr>
					<tr>
						<td align="center" valign="bottom" height="100%" >
						<table width="90%" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td width="12"><p><?=$li->displayPrevMonth()?></p></td>
							<td align="center" class="indexcalendarmonth"><?=$li->displayMonthText()?></td>
							<td width="12"><p><?=$li->displayNextMonth()?></p></td>
						</tr>
						</table>
						</td>
					</tr>

					<tr>
						<td align="center" valign="bottom">
						<table width="100%" border="0" cellspacing="0" cellpadding="1">

						<tr>
							<? /* Public Holiday */ ?>
							<td align="center" valign="middle">
								<a href="javascript:fe_view_event_by_type(3)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar_2/btn_holiday_off.gif" alt="<?=$i_EventTypeString[2]?>" title="<?=$i_EventTypeString[2]?>" name="ca3" width="25" height="15" border="0" id="ca3" onMouseOver="MM_swapImage('ca3','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar_2/btn_holiday_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a>
							</td>
							<? /* School Holiday */ ?>
							<td align="center" valign="middle">
								<a href="javascript:fe_view_event_by_type(4)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar_2/btn_school_holiday_off.gif" title="<?=$i_EventTypeString[4]?>" name="ca4" width="25" height="15" border="0" id="ca4" onMouseOver="MM_swapImage('ca4','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar_2/btn_school_holiday_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a>
							</td>
							<? /* School Event */ ?>
							<td align="center" valign="middle">
								<a href="javascript:fe_view_event_by_type(0)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar_2/btn_schoolevent_off.gif" alt="<?=$i_EventTypeString[0]?>" title="<?=$i_EventTypeString[0]?>" name="ca2" width="25" height="15" border="0" id="ca2" onMouseOver="MM_swapImage('ca2','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar_2/btn_schoolevent_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a>
							</td>
							<? /* Academic Event */ ?>
							<td align="center" valign="middle">
								<a href="javascript:fe_view_event_by_type(1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar_2/btn_academicevent_off.gif" alt="<?=$i_EventTypeString[1]?>" title="<?=$i_EventTypeString[1]?>" name="ca1" width="25" height="15" border="0" id="ca1" onMouseOver="MM_swapImage('ca1','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar_2/btn_academicevent_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a>
							</td>
							<? /* Group Event */ ?>
							<td align="center" valign="middle">
								<a href="javascript:fe_view_event_by_type(2)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar_2/btn_groupevent_off.gif" alt="<?=$i_EventTypeString[3]?>" title="<?=$i_EventTypeString[3]?>" name="ca5" width="25" height="15" border="0" id="ca5" onMouseOver="MM_swapImage('ca5','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar_2/btn_groupevent_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a>
							</td>
							<? if(!$special_feature['HideShowAllEventIcon']) {?>
							<? /* All event */ ?>
							<td align="center" valign="middle">
								<? $AllEventText = $i_status_all.(($intranet_session_language=="en") ? " ": "" ).$i_Events; ?>
								<a href="javascript:showAllEventList()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar_2/btn_allevent_off.gif" alt="<?=$AllEventText?>" title="<?=$AllEventText?>" name="ca6" width="25" height="15" border="0" id="ca6" onMouseOver="MM_swapImage('ca6','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar_2/btn_allevent_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a>
							</td>
							<? } ?>
						</tr>

						</table>
						</td>
					</tr>
					</table>
					</span>

					<form name="CalForm" >
					<input type="hidden" name="v" value="<?=$v?>" />
					<input type="hidden" name="ts" value="<?=$ts?>" />
					</form>
					</td>
					<td width="6" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_06.gif" width="6" height="10" /></td>
				</tr>
			<tr>
				<td width="4" height="7"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_07.gif" width="4" height="7" /></td>
				<td valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_08.gif" height="7"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_08.gif" width="4" height="7" /></td>
				<td width="6" height="7"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_09.gif" width="6" height="7" /></td>
			</tr>
			</table>
			<? ### Calendar End ### ?>
			</td>
		</tr>
		<tr>
			<td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5" /></td>
		</tr>
		<tr>
			<td align="right" height="100%" valign="top">
			<!--<span id="EventContentDiv" style="height:100%">-->
			<div id="EventContentDiv" style="height:100%">
			<table width="193" height="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td height="22"><?=$EventMenu?></td>
			</tr>
			<tr>
				<td height="100%"  valign="top" >
				<table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%">
				<tr>
					<td width="5" height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_01.gif" width="5" height="5"></td>
					<td height="5" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_02.gif" width="5" height="5"></td>
					<td width="7" height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_03.gif" width="7" height="5"></td>
				</tr>
				<tr>
					<td width="5" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_04.gif" width="5" height="5"></td>
					<td valign="top" bgcolor="#FFFFFF" height="100%" >
					<div id="EventInboxDiv" style="width:100%; <?=$HeightUsed?> z-index:1; overflow: auto;">
					<?=$EventContent?>
					</div>
					</td>
					<td width="7" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_06.gif" width="7" height="5"></td>
				</tr>
				<tr>
					<td width="5" height="8"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_07.gif" width="5" height="8"></td>
					<td height="8" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_08.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_08.gif" width="7" height="8"></td>
					<td width="7" height="8"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_09.gif" width="7" height="8"></td>
				</tr>
				</table>
				</td>
			</tr>
			</table>
			<!--</span>-->
			</div>

			<form name="EventForm" >
			<input type="hidden" name="type" value="<?=$EventType?>" />
			<input type="hidden" name="ts" value="" />
			</form>
			</td>
		</tr>
		<tr>
			<td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5"></td>
		</tr>
		</table>

		</td>
		<td width="12" height="100%" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/left_edge_bg.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/left_edge_corner.gif" width="12" height="11"></td>
		</tr>
		</table>
		</td>
	</tr>
	</table>
	</td>
<?php } ?>
	<td valign="top" bgcolor="#FFFFFF" height="100%" >
	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td height="27">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="9"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/middle_welcome_left.gif" width="9" height="27" /></td>
			<td align="center" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/middle_welcome_bg.gif" class="indexwelcome">
				<?=$ip20_welcome?> , <?php echo (($lu->NickName=="") ? intranet_wordwrap($name,20,"\n",1) : intranet_wordwrap($lu->NickName,20,"\n",1) ); ?>
			</td>
			<td width="9"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/middle_welcome_right.gif" width="9" height="27" /></td>
		</tr>
		</table>
		</td>
	</tr>

	<?=$scrollerTable?>

	<tr>
		<td valign="top" height="100%" >
		<table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%">
		<tr>
			<td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5" /></td>
		</tr>
		<?=$TVContent?>
		<tr>
			<td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5"></td>
		</tr>
<!-- START cust Medical studentLog hint -->
		<?php
		if( ($plugin['medical']) ){
			include_once($PATH_WRT_ROOT."includes/cust/medical/libMedical.php");
			$objMedical = new libMedical();
			if(
				($plugin['medical_module']['studentLog']) &&
				(
					( $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STUDENTLOG_MANAGEMENT') ) ||
					( $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STUDENTLOG_REPORT') )
				)
			){
//				foreach ((array)$plugin['medical_module']['homepageHint'] as $medicalHomeHintModule) {
			    $medicalHomeHintModule = $plugin['medical_module']['homepageHint']['studentLog'];
					if($medicalHomeHintModule){
						$medicalPath = $PATH_WRT_ROOT.'/home/eAdmin/StudentMgmt/medical';
						include($medicalPath.'/homepage/medicalNotice.php');
//						break;
					}
//				}
			}

			if ($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'EVENT_MANAGEMENT') && $plugin['medical_module']['discipline'] && $plugin['medical_module']['homepageHint']['event']) {
			    $medicalPath = $PATH_WRT_ROOT.'/home/eAdmin/StudentMgmt/medical';
			    include($medicalPath.'/homepage/medicalEvent.php');
			}
		}
		?>
<!-- END cust -->

<!-- START show news -->
<?php if (!$sys_custom['HideNewsInFrontPage']):?>
		<tr>
			<td >
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td height="24">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="8"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_01.gif" width="8" height="24"></td>
					<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_02.gif">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="indexnewstitle"><?=$ip20_what_is_new?></td>
						<td align="right">&nbsp;
						</td>
					</tr>
					</table>
					</td>
					<td width="8"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_03.gif" width="8" height="24"></td>
				</tr>
				</table>
				</td>
			</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td height="100%" >
			<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5" height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_04.gif" width="5" height="5"></td>
				<td height="5" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_05.gif" width="6" height="5"></td>
				<td width="5" height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_06.gif" width="5" height="5"></td>
			</tr>

			<tr>
				<td width="5" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_07.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_07.gif" width="5" height="5"></td>
				<td valign="top" bgcolor="#FFFFFF" height="100%"  >
				<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%" align="center" valign="top" height="100%"  >
					<table width="98%" border="0" cellspacing="0" cellpadding="1" height="100%" >
					<tr>
						<td height="2" class="indexnewslisttitle" align='left'>
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/icon_arrow_left.gif" width="11" height="15" align="absmiddle" />
						 <?=$ip20_public?>
						 <img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/icon_arrow_right.gif" align="absmiddle" /></td>
					</tr>
					<tr>
						<td height="2" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="2"></td>
					</tr>
					<tr>
						<td height="100%" >
						<div id="whatnews" style="width:100%; height:100%; z-index:1; overflow: auto;">
						<?=$la->displaySchoolAnnouncement($UserID)?>
						</div>
						</td>
					</tr>
					</table>

					</td>
					<td width="5" height="100%"  ><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="5" height="10"></td>
					<td width="1" height="100%"  bgcolor="#E1E1E1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="1" height="10"></td>
					<td width="5" height="100%"  ><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="5" height="10"></td>
					<td width="50%" align="center" valign="top" height="100%"  >
	  				<form name="form2" method="get" action="index20.php" >
					<table width="98%" border="0" cellspacing="0" cellpadding="1" height="100%"  >
					<tr>
						<td height="2" class="indexnewslistgrouptitle" align="left">
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/icon_arrow_left.gif" width="11" height="15" align="absmiddle">
						<?=$ip20_my_group?>
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/icon_arrow_right.gif" width="11" height="15" align="absmiddle">
						<?=$GroupForm?>
						</td>
					</tr>
					<tr>
						<td height="2" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="2"></td>
					</tr>
					<tr>
						<td height="100%" >
						<div id="groupwhatnews" style="width:100%; <?=$HeightUsed?> z-index:1; overflow: auto;">
						<?=$myGroupList?>
						</div>
						</td>
					</tr>
					</table>
					</form>
					</td>
				</tr>
				</table>
				</td>
				<td width="5" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_09.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_09.gif" width="5" height="5"></td>
			</tr>
			<tr>
				<td width="5" height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_10.gif" width="5" height="6"></td>
				<td height="6" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_11.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_11.gif" width="5" height="6"></td>
				<td width="5" height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_12.gif" width="5" height="6"></td>
			</tr>

			</table>
			<form name="AnnounceForm" >
			<input type="hidden" name="GroupID" value="<?=$GroupID?>" />
			</form>

			</td>
		</tr>
		<tr>
			<td height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5"></td>
		</tr>

<?php endif;?>
<!-- END show news -->
<?if($sys_custom['portal']['schoolnews_cust']){include_once("school_news_cust.php");}?>
<?php echo $htmlAry['hkpfPortal'] ?>
		</table>
		</td>
	</tr>

    </table>
    </td>
<?php if (!$sys_custom['HidePortalRightMenu']) { ?>
    <td width="235" valign="top" height="100%"  style='background:url(<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/rightbg.gif) left'>
	<table width="235" border="0" cellpadding="0" cellspacing="0" height="100%">
	<tr>
		<td height="16" align="left" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/right_top_bg.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/right_top_corner.gif" width="24" height="16"></td>
    </tr>
	<tr>
		<td valign="top" >
		<table width="235" height="100%" border="0" cellpadding="0" cellspacing="0" valign="top">
		<tr>
			<td width="12" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/right_edge_bg.gif"><p><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/right_edge_corner.gif" width="12" height="11"></p> </td>
			<td valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/right_bg.gif">
			<table width="223" height="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td height="0" valign="top">
				<?=$LogoTable?>
				</td>
			</tr>
			<tr>
				<td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5"></td>
			</tr>

		<?
		  if($plugin['WWS_eLearningProject']){
		?>
			<tr>
			  <td width="220" height="180" valign="top">
				<div id="WWSListContentDiv" style="width:220px; height:100%; z-index:1; overflow: hidden;"></div>
			  </td>
			</tr>
			<tr><td>&nbsp;</td></tr>
		<? } ?>

<?php	if (!$sys_custom['HideeClass']):?>
			<tr>
				<td  width="220" height="100%" valign="top" >
				<span id="ListContent" >
				<table width="220" border="0" cellpadding="0" cellspacing="0" height="400" >
				<tr>
					<td height="40" width="220" ><span id="ListMenuDiv"><?=$ListMenu?></span></td>
				</tr>
				<tr>
					<td valign="top" width="220" >
					<table width="220" height="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="5" height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_01.gif" width="5" height="5"></td>
						<td height="5" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_02.gif" width="5" height="5"></td>
						<td width="6" height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_03.gif" width="6" height="5"></td>
					</tr>
					<tr>
						<td width="5" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_04.gif" width="5" height="5"></td>
						<td valign="top" bgcolor="#FFFFFF">
						<div id="ListContentDiv" style="width:209px; height:100%; z-index:1; overflow: auto;">
						<?=$ListContent?>
						</div>
						</td>
						<td width="6" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_06.gif" width="6" height="5"></td>
					</tr>
					<tr>
						<td width="5" height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_07.gif" width="5" height="6" /></td>
						<td height="6" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_08.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_08.gif" width="6" height="6" /></td>
						<td width="6" height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_09.gif" width="6" height="6" /></td>
					</tr>
					</table>
					</span>

					</td>
				</tr>
				</table>
				</td>
			</tr>
<?php endif;?>
			<tr>
				<td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5"></td>
			</tr>
			</table>
			</td>
		</tr>
		</table>
		</td>
	</tr>
	</table>
	</td>
<?php } ?>
</tr>
</table>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
    <td class="footer" style="border-top:1px solid #cccccc;">
<?php if ($sys_custom["showWithBL"]) { ?>
	<?php if ($sys_custom["FooterWithoutLink"]) {?>
	<span>Powered by BroadLearning</span>
	<?php } else { ?>
	<span>Powered by <a href="http://www.eclass.com.hk" target="_blank">BroadLearning</a></span>
	<?php } ?>
<?php } else { ?>
	<span>Powered by <a href="http://www.eclass.com.hk" target="_blank"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/logo_eclass_footer.gif" width="39" height="15" border="0" align="absmiddle"></a></span>
<?php } ?></td>
  </tr>
</table>

<?php
# popup window
if ($ck_intranet_justlogin)
{
    $popup_file = get_file_content($intranet_root."/file/popupdate.txt");
    $dates = explode("\n",$popup_file);

    if ($dates[0]!= "" && $dates[1] != "")
    {
        $start = strtotime($dates[0]);
        $end = strtotime($dates[1]);
        if ($start != -1 && $end != -1)
        {
            $end += 3600*24;
            $now = time();
            if ($now >= $start && $now < $end)
            {
                if ($HTTP_COOKIE_VARS['ck_intranet_notshowhelp_'.$lu->UserLogin]!=1)
                {
?>
                <script language="javascript" >
                win_size = "resizable,status,top=40,left=40,width=500,height=450";
                newWin = window.open ("/help/", '', win_size);
                </script>
<?php
                }
            }
        }
    }

    # Set up AeroDrive Login
    if (isset($plugin['aerodrive']) && $plugin['aerodrive'])
    {
        $is_student = $lu->isStudent();
        if ($is_student || $lu->isTeacherStaff())
        include_once("plugin/aerodrive/goaero.php");
    }

	?>
	<? /* No need to check "Always trasnfer URL in UTF8" in IP2.5  ?>
	<SCRIPT language=Javascript>
		var intWidth = screen.width;
		var intHeight = screen.height;
		win_size = "top="+(intHeight+30)+",left="+intWidth+",width=220,height=170";
		newWin = window.open ("http://<?=$eclass_httppath?>/src/b2.php", 'back', win_size);
    </SCRIPT>
    <? */ ?>
	<?php
}



intranet_closedb();
?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.tablednd_0_5.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.cookies.2.2.0.js"></script>

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" />


<script language="javascript">
<!--

<? if ($is_portal_tv) {?>
displayMovie();
<? } ?>

// Scroll the Month's Event layer to today's event by default
$(document).ready(function() {
	jAJAX_GO_CUST(document.ListForm);

<?php if ($alertAdminForAccountPassword) {?>
	tb_show("<?=$Lang['login']['password_alert']?>", "aj_risky_account.php?KeepThis=true&TB_iframe=true&width=720&height=540");
	//load_dyn_size_thickbox_ip('<?=$Lang['login']['password_alert']?>', 'onloadRiskyAccountTb();', '', '', '', '', 1, 'aj_risky_account.php');
<?php
} else if ($plugin["eForm"] && !empty($eFormPromptHTML)) {
	###### eForm Reminder's Javascript (Thickbox) #######
?>
	if (typeof $("#eForm_PromptWindow") != "undefined") {
		tb_show("<?php echo $Lang['eForm']['Msg_Reminder'];?>", "#TB_inline?width=700&inlineId=myOnPageContent");
		$('#TB_ajaxContent').html($("#eForm_PromptWindow").html());
	}
<?php } ?>
	<? if($plugin['WWS_eLearningProject']){ ?>
	jAJAX_GO_ECLASS_WWS(document.ListForm);
	<? } ?>
	<?=$header_onload_js?>
	$('#EventInboxDiv').scrollTo($('#'+jsDefaultEventTrID), 800, {queue:true});
	<? if($is_imail_gamma===true){ ?>
	getGammaNewMailNumber();
	<? } ?>
});

//-->
</script>
<?php
###### eForm Reminder's HTML #######
if (!$alertAdminForAccountPassword && $plugin["eForm"] && !empty($eFormPromptHTML)) {
	echo $eFormPromptHTML;
}
?>
	<? ##### eInventory warranty expiry warning popup ?>
	<? if($arr_expiry[0]>0) { ?>
	<script language="javascript">
		//newWindow("/home/eAdmin/ResourcesMgmt/eInventory/report/item_warranty_expiry_warning.php",4);
		var eInventory_url = "/home/eAdmin/ResourcesMgmt/eInventory/report/item_warranty_expiry_warning.php";
		var eInventory_win_name = "intranet_popup4_eInventory";
        var eInventory_win_size = "menubar,resizable,scrollbars,status,top=40,left=40,width=600,height=400";
        var eInventory_newWin = window.open (eInventory_url, eInventory_win_name, eInventory_win_size);
		if ((navigator.appName=="Netscape") || (navigator.appName=="Microsoft Internet Explorer" && navigator.appVersion.indexOf("MSIE")>1))
			 eInventory_newWin.focus();
	</script>
	<? } ?>

	<? ##### eEnrolment reminder popup ?>
	<? if($eEnrolment_Reminder_popup) { ?>
	<script language="javascript">
		newWindow("/home/eAdmin/StudentMgmt/enrollment/popup_reminder.php",16);
	</script>
	<? } ?>

	<? ##### eLib+ periodical alert popup ?>
	<? if($showLibraryPeriodicalOrderAlert) { ?>
	<script language="javascript">
		newWindow("/home/library_sys/admin/periodical/orderRenewAlert.php",4);
	</script>
	<? } ?>

<?
$NoSpace = 1;
//$lu->db_show_debug_log();
$_SESSION['Is_First_Login'] = 0;

# display the eClass bubble after resize of window (when login)
echo $bubble;

include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/home_footer.php");

?>
