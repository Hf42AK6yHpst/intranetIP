<?php
# using: yat

#########################################
#
#	Date:	2014-05-23	YatWoon
#			improved: add checking for duplicate record [Case#D62243]
#
#########################################

$PATH_WRT_ROOT = "../../../";
include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
intranet_auth();
intranet_opendb();

## Check access right
if($_SESSION['UserType']==USERTYPE_PARENT)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	 $laccessright = new libaccessright();
	 $laccessright->NO_ACCESS_RIGHT_REDIRECT();
	 exit;
}

$li = new libdb();
$PollingID = IntegerSafe($PollingID);

# check duplicate
$sql = "select count(*) from INTRANET_POLLINGRESULT where PollingID=$PollingID and UserID=$UserID";
$result = $li->returnVector($sql);
if(!$result[0])
{
	$sql = "INSERT INTO INTRANET_POLLINGRESULT (PollingID, UserID, Answer, DateInput, DateModified) VALUES ($PollingID, $UserID, '$Answer', now(), now())";
	$li->db_db_query($sql);
}

intranet_closedb();

header("Location: index.php?xmsg=voted");
?>
