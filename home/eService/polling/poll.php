<?php
# using: yat

#############################
#
#	Date:	2012-07-16 (YatWoon)
#			update download_attachment.php with encrypt logic
#
#	Date:	2011-04-13	YatWoon
#			display choice to Z
#
#	Date:	2010-12-29	YatWoon
#			IP25 standard
#
#############################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpolling.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PagePollingCurrent"; 
$lpolling 	= new libpolling($PollingID[0]);

## Check access right
if($_SESSION['UserType']==USERTYPE_PARENT)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	 $laccessright = new libaccessright();
	 $laccessright->NO_ACCESS_RIGHT_REDIRECT();
	 exit;
}

### check if the poll is voted, then redirect to result page
if($lpolling->UserisVoted($UserID))
{
        header ("Location: result.php?PollingID=$PollingID[0]");
	exit();
}

$now = time();
$today = mktime(0,0,0,date("m",$now),date("d",$now),date("Y",$now));
$end = strtotime($lpolling->DateEnd);
if ($end < $today)
{
    header ("Location: index.php");
    exit();
}

#content
$x = "";

//$x .= "<table width='100%' border='0' cellpadding='04' cellspacing='0'>\n";

for($i=65;$i<=90;$i++)
{
	$x .= ($lpolling->{"Answer".chr($i)}=="") ? "" : "<tr><td >&nbsp;</td><td class='tabletext'><input type='radio' name='Answer' value='". chr($i) ."' id='". chr($i) ."'> <label for='". chr($i) ."'>".$lpolling->{"Answer".chr($i)}."</label></td></tr>\n";
}

$x .= ($lpolling->Reference=="") ? "" : "<tr><td>&nbsp;</td><td><a href=javascript:newWindow('".$lpolling->Reference."',5) class='tablelink'>$i_PollingReference</a></td></tr>\n";
//$x .= "</table>\n";
                

### Title ###
$TAGS_OBJ[] = array($i_adminmenu_im_polling,"");
$MODULE_OBJ = $lpolling->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_Polling_Vote, "");

$pollingAttFolder = $lpolling->Attachment;
if(!empty($pollingAttFolder))
{
	$path = "$file_path/file/polling/$pollingAttFolder";
	$lo = new libfiletable("", $path, 0, 0, "");
	$files = $lo->files;
}
?>

<script language="javascript">
function checkform(obj)
{
     if(countChecked(obj,"Answer")==0){
          alert("<?php echo $i_alert_pleaseselect.$i_QB_Answer; ?>");
          return false;
     }
     
     //document.getElementById('submit2').disabled  = true; 
	//document.getElementById('submit2').className  = "formbutton_disable_v30 print_hide"; 
	//return true;
}
</script>

<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<form name="form1" action="poll_update.php" method="post" onSubmit="return checkform(this);">

<table class="form_table_v30">

<tr valign="top">
	<td class="field_title" nowrap="nowrap"><?=$i_PollingTopic?></td>
	<td ><?=intranet_wordwrap($lpolling->Question,30,"\n",1)?></td>
</tr>
	<tr valign="top">
	<td class="field_title"><?=$i_PollingPeriod?></td>
	<td><?=$lpolling->DateStart?> ~ <?=$lpolling->DateEnd?></td>
</tr>


<? if(!empty($files)) {?>
<tr valign='top'>
	<td class='field_title'><?=$i_Notice_Attachment?></td>
	<td>
	<? 
	foreach($files as $k=>$d) {
		$this_path = $path . "/".$d[0];
		echo "<img src=$image_path/$LAYOUT_SKIN/index/whatnews/icon_attachment.gif hspace=2 vspace=2 border=0 align=absmiddle><a href='/home/download_attachment.php?target_e=". getEncryptedText($this_path) ."' class='indexpoplink'>$d[0]</a><br />";
	
	 } ?>
	
	</td>
</tr>   
<? } ?>
<?=$x;?>
</table>


<div class="edit_bottom_v30">
	<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submit2") ?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancelbtn") ?>
	<p class="spacer"></p>
</div>	

<input type="hidden" name="PollingID" value="<?= $lpolling->PollingID ?>">

</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>