<?php
#Modify : 

#############################
#	Date:	2017-11-15 Anna
#			change returnResult to returnReleaseResult
#
#	Date:	2012-07-16 (YatWoon)
#			update download_attachment.php with encrypt logic
#
#	Date:	2010-12-29	YatWoon
#			IP25 standard
#
#############################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpolling.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
if($fr == 1)
	$CurrentPage	= "PagePollingCurrent";
else	
	$CurrentPage	= "PagePollingPast";
	
$PollingID = IntegerSafe($PollingID);
$lpolling 	= new libpolling($PollingID);

## Check access right
if($_SESSION['UserType']==USERTYPE_PARENT)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	 $laccessright = new libaccessright();
	 $laccessright->NO_ACCESS_RIGHT_REDIRECT();
	 exit;
}
    
### Title ###
$TAGS_OBJ[] = array($i_adminmenu_im_polling,"");
$MODULE_OBJ = $lpolling->GET_MODULE_OBJ_ARR();
//$lpolling->Result = $lpolling->returnResult($lpolling->PollingID);
$lpolling->Result = $lpolling->returnReleaseResult($lpolling->PollingID);

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_view.($intranet_session_language=="en"?" ":""). $i_adminmenu_im_polling);

$pollingAttFolder = $lpolling->Attachment;
if(!empty($pollingAttFolder))
{
	$path = "$file_path/file/polling/$pollingAttFolder";
	$lo = new libfiletable("", $path, 0, 0, "");
	$files = $lo->files;
}

?>

<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<table class="form_table_v30">
<tr valign="top">
	<td class="field_title"><?=$i_PollingTopic?></td>
	<td><?=intranet_wordwrap($lpolling->Question,30,"\n",1)?> (<?=sizeof($lpolling->Result)?>)</td>
</tr>
<tr valign="top">
	<td class="field_title"><?=$i_PollingPeriod?></td>
	<td><?=$lpolling->DateStart?> ~ <?=$lpolling->DateEnd?></td>
</tr>

<? if(!empty($files)) {?>
<tr valign='top'>
	<td class='field_title'><?=$i_Notice_Attachment?></td>
	<td>
	<? 
	foreach($files as $k=>$d) {
		$this_path = $path . "/".$d[0];
		echo "<img src=$image_path/$LAYOUT_SKIN/index/whatnews/icon_attachment.gif hspace=2 vspace=2 border=0 align=absmiddle><a href='/home/download_attachment.php?target_e=". getEncryptedText($this_path) ."' class='indexpoplink'>$d[0]</a><br>";
	
	 } ?>
	
	</td>
</tr>   
<? } 

if ($lpolling->Reference!="")
{
	echo "<tr><td>&nbsp;</td><td ><a href=javascript:newWindow('".$lpolling->Reference."',5) class='tablelink'>$i_PollingReference</a></td></tr>\n";	
}

?>

</table>

<table align="center" width="80%" border="0" cellpadding="0" cellspacing="0"  align="center">
	<tr>
		<td><?=$lpolling->displayResultChart('1')?></td>
	</tr>
</table>
						
<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back();","cancelbtn") ?>
	<p class="spacer"></p>
</div>							
					
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>