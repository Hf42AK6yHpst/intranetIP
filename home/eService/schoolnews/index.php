<?php
# using: 
/*  
 *  show all school news for students
 * 
 *  2019-06-06 Cameron
 *      - create this file
 */

	$PATH_WRT_ROOT = "../../../";
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libschoolnews.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libannounce.php");
	

	intranet_auth();
	intranet_opendb();
	
	if (!$sys_custom['LivingHomeopathy'] || ($_SESSION['UserType'] != USERTYPE_STUDENT)) {
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}



	# Temp Assign memory of this page
	ini_set("memory_limit", "150M");

	# Create a new interface instance
	$linterface = new interface_html();

	# Create a new homework instance
	$lschoolnews = new libschoolnews();

	# Select current page
	$CurrentPage = "eServiceSchoolNews";
	$CurrentPageArr['eServiceSchoolNews'] = 1;

	# Page title
	$PAGE_TITLE = $Lang['SysMgr']['SchoolNews']['SchoolNews'];
	$TAGS_OBJ[] = array($Lang['SysMgr']['SchoolNews']['SchoolNews'], "");
	
	$MODULE_OBJ = $lschoolnews->GET_MODULE_OBJ_ARR();

	# change page size
	if ($page_size_change == 1)
	{
		setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
		$ck_page_size = $numPerPage;
	}

	if (isset($ck_page_size) && $ck_page_size != "") 
		$page_size = $ck_page_size;

	$pageSizeChangeEnabled = true;
	
	# Select sort field
	$sortField = 0;
	
	# Table initialization
	$order = ($order == "") ? 0 : $order;
	$field = ($field == "") ? $sortField : $field;
	$pageNo = ($pageNo == "") ? 1 : $pageNo;

	# Create a new dbtable instance
	$li = new libdbtable2007($field, $order, $pageNo);
	
	# TABLE SQL
	$keyword = intranet_htmlspecialchars(trim($keyword));
	$keyword = addcslashes($keyword, '_');
	
	$user_field = getNameFieldWithClassNumberByLang("b.");

	$title = intranet_wordwrap("', a.Title,'", 20,"\n",1);
	
	
	$name_field = getNameFieldWithClassNumberByLang("b.");
	
	$allStatus = " a.RecordStatus IN (1)";
	$status = ($status == "") ? $allStatus : "a.RecordStatus = '$status'";
	
	$fields = "CONCAT(DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d'),' ~ ', DATE_FORMAT(a.EndDate, '%Y-%m-%d')) AS DisplayDate,
			   CONCAT('<a class=\"tablelink\" href=\"javascript:viewNews(',a.AnnouncementID,')\">$title</a>'),
			   IF(c.Title='' OR c.Title IS NULL,IF(b.ChineseName='' AND b.EnglishName='','$i_AnnouncementSystemAdmin',$name_field),CONCAT($name_field,'<br>',c.Title)) AS AnnouncedBy
                ";
	
	
	$dbtables = "INTRANET_ANNOUNCEMENT AS a 
				LEFT JOIN INTRANET_USER AS b ON (b.UserID = a.UserID)
				LEFT JOIN INTRANET_GROUP AS c ON (c.GroupID = a.OwnerGroupID)
				LEFT JOIN INTRANET_GROUPANNOUNCEMENT AS d on (d.AnnouncementID=a.AnnouncementID)
				 ";
	$conds .= " ((a.Title like '%".$lschoolnews->Get_Safe_Sql_Query($keyword)."%') OR (b.EnglishName like '%".$lschoolnews->Get_Safe_Sql_Query($keyword)."%') OR
				(b.ChineseName like '%".$lschoolnews->Get_Safe_Sql_Query($keyword)."%')) AND $status";
	if ($_SESSION['UserType'] == USERTYPE_STUDENT) {
	    $dbtables .= " LEFT JOIN (SELECT DISTINCT GroupID FROM INTRANET_USERGROUP WHERE UserID LIKE '" . $_SESSION["UserID"] . "') AS e ON e.GroupID=d.GroupID ";
	    $conds .= " AND (d.GroupAnnouncementID IS NULL OR e.GroupID IS NOT NULL)";
	}
	$conds .= " AND a.AnnouncementDate <= CURDATE() ";
	if (!$lschoolnews->allowUserToViewPastNews) {
	    $conds .= " AND a.EndDate >= CURDATE() ";
	}
	
	$groupby = " group by a.AnnouncementID ";
	$sql = "SELECT $fields FROM $dbtables WHERE $conds $groupby";
		
	# TABLE INFO
	$li->field_array = array("a.AnnouncementDate", "a.Title", "AnnouncedBy");	
	$li->fieldorder2 = ",a.AnnouncementDate DESC, a.AnnouncementID DESC";
	
	
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+1;
	$li->IsColOff = "IP25_table";	

	// TABLE COLUMN
	$li->column_list .= "<th class='num_check'>#</td>\n";
	$li->column_list .= "<th>".$li->column(0, $i_AnnouncementDisplayDate)."</th>\n";
	$li->column_list .= "<th>".$li->column(1, $i_AnnouncementTitle)."</th>\n";
	$li->column_list .= "<th>".$li->column(2, $i_AnnouncementOwner)."</th>\n";

	# Start layout
	$linterface->LAYOUT_START($xmsg);
	
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"". stripslashes($keyword) ."\" onkeyup=\"Check_Go_Search(event);\"/>";

?>
<SCRIPT LANGUAGE=Javascript>

	function viewNews(id)
	{
		 newWindow('../../view_announcement.php?ct=0&AnnouncementID='+id,1);
	}

	
	function Check_Go_Search(evt)
	{
		var key = evt.which || evt.charCode || evt.keyCode;
		
		if (key == 13) // enter
		{
			document.form1.submit();
		}
		else {
			return false;
		}
	}
</SCRIPT>

<form name="form1" method="post" action="">

<div class="content_top_tool">
	<?=$toolbar?>
	<div class="Conntent_search"><?=$searchTag?></div>
	<br style="clear:both" />
</div>

<div class="table_board">

<?=$li->display()?>

</div>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>"/>
<input type="hidden" name="order" value="<?php echo $li->order; ?>"/>
<input type="hidden" name="field" value="<?php echo $li->field; ?>"/>
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
<input type="hidden" name="page_size_change" value=""/>
</form>
			
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>