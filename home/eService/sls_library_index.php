<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libslslib2007a.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lslslib 	= new libslslib2007();

if($_SESSION["SSV_PRIVILEGE"]["library"]["access"])
{
	$SpecialArr[] = array("javascript:newWindow('/home/plugin/sls/library.php',8)", $i_sls_library_search);
	$SpecialArr[] = array("javascript:newWindow('/home/plugin/sls/library_info.php',8)", $i_sls_library_info);
}



### Title ###
$title = $ip20TopMenu['SLSLibrary'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ['title'] = $ip20TopMenu['SLSLibrary'];
$CurrentPageArr['SLSLibrary'] = 1;

$linterface->LAYOUT_START();

?>


<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="8">
		<tr><td>
		<?=$lslslib->displayTable($SpecialArr)?>
		</td></tr>
		</table>
	</td>
</tr>
</table>        
<br />

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
