<?php
/*
 *  2019-05-13 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 *      - apply cleanHtmlJavascript() before save Remark to avoid javascript injection
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/librb.php");
intranet_auth();
intranet_opendb();

$Remark = htmlspecialchars(cleanHtmlJavascript(trim($Remark)));
$RecordStatus = 0; # Pending

$lb = new librb();
$lb->retrieveBooking($bid);
$newInserted = 0;
$failed = false;
# Check ownership
if ($UserID != $lb->AppliedUserID)
{
    $failed = true;
    header("Location: index.php");
}
else
{
    $batch_booking = $lb->retrieveBookingBatchIDs();
    $bookingList = implode(",",$batch_booking);
    $sql = "SELECT BookingID,TimeSlot FROM INTRANET_BOOKING_RECORD WHERE BookingID IN ($bookingList)";
    $result = $lb->returnArray($sql,2);

    $newSlotsList = implode(",", $slots);
    if ($lb->RecordStatus == 4)
    {
        # update to cancel status for deselecting records
        $sql = "UPDATE INTRANET_BOOKING_RECORD SET RecordStatus = 1 WHERE BookingID IN ($bookingList) AND TimeSlot NOT IN ($newSlotsList)";
        $lb->db_db_query($sql);

        # update remarks
        $sql = "UPDATE INTRANET_BOOKING_RECORD SET Remark = '$Remark' WHERE BookingID IN ($bookingList) AND RecordStatus NOT IN (1)";
        $lb->db_db_query($sql);
    }
    else if ($lb->RecordStatus == 0)
    {
        $bdate = $lb->BookingDate;
        $rid = $lb->ResourceID;

        $lb->db_db_query("LOCK TABLES INTRANET_BOOKING_RECORD WRITE, INTRANET_RESOURCE READ");

        # Check availability
        $sql = "SELECT count(BookingID) FROM INTRANET_BOOKING_RECORD WHERE ResourceID = '$rid' AND BookingDate = '$bdate' AND TimeSlot IN ($newSlotsList) AND RecordStatus IN (2,3,4)";
        $count = $lb->returnVector($sql);
        if ($count[0]!=0)
        {
            $failed = true;
            header("Location: index.php?signal=2");
        }
        else
        {
            # Remove old records
            $sql = "DELETE FROM INTRANET_BOOKING_RECORD WHERE BookingID IN ($bookingList)";
            $lb->db_db_query($sql);

            # Get default status
            $sql = "SELECT RecordType FROM INTRANET_RESOURCE WHERE ResourceID = '$rid'";
            $result = $lb->returnVector($sql);
            if ($result[0]!="") $RecordStatus = $result[0];

            # Build insert query
            $insert_values = "";
            $delimiter = "";
            for ($i=0; $i<sizeof($slots); $i++)
            {
                 $pos = $slots[$i];
                 $insert_values .= "$delimiter ('$rid', '$UserID', '$bdate', null, '$Remark', '$pos', '$RecordStatus', now(),now())";
                 $delimiter = ",";
            }
            # Re-add the records
            if ($delimiter == ",")
            {
                $fields_name = "ResourceID, UserID, BookingDate, PeriodicBookingID, Remark, TimeSlot, RecordStatus,TimeApplied,LastAction";
                $sql = "INSERT INTO INTRANET_BOOKING_RECORD ($fields_name) VALUES $insert_values";
                $lb->db_db_query($sql);
            }
            $lb->db_db_query("UNLOCK TABLES");
        }
    }
    else
    {
        $failed = true;
        header("Location: index.php?signal=4");
    }
}
intranet_closedb();
if (!$failed)
    header("Location: index.php?signal=3");

?>