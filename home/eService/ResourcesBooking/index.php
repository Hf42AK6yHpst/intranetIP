<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/librb.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();
auth_resource();
?>
<script language="Javascript" src='/templates/brwsniff.js'></script>
<script language="Javascript" src='/templates/tooltip.js'></script>
<style type="text/css">
     #ToolTip{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
     </style>

     <script language="JavaScript">
     //isMenu = true;
     isToolTip = true;
     </script>
     <div id="ToolTip"></div>

<?php
$navigation = $i_frontpage_separator.$i_frontpage_rb;
$body_tags = "onMouseMove=\"overhere()\"";
include_once($PATH_WRT_ROOT."templates/homeheader.php");
$lb = new librb();

echo $lb->pageTitle();

if ($lb->hasRightToReserve($UserID))
{
    $toolbar = "<a href=add.php> <img border=0 src='/images/icon_new.gif'> $i_BookingNew </a>";
}
else
{
    $toolbar = "<img border=0 src=/images/icon_new.gif><span STYLE=\"text-decoration: line-through\">$i_BookingNew</span>";
}


$toolbar .= " | <a href=clear.php> <img border=0 src='/images/icon_clearrejectrecord.gif'> $i_Booking_ClearReject</a>";
if ($signal != "") $xsignal = $i_Booking_Signal[$signal];
?>
<form name=form1 method=get>
<table width=794 border=0 cellpadding=0 cellspacing=0>
  <tr>
    <td>&nbsp;</td>
  </tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
<tr>
          <td width="25">&nbsp;</td>
		<td><?php echo $i_frontpage_rb_title; ?></td>
		<td align="right" valign="bottom">
		
		<br><br><a class="functionlink" href="reserved.php"><?=$i_Community_Community_GoTo?><?=$intranet_session_language=="en"?" ":""?><?=$i_frontpage_menu_resourcebooking_reserved?></a>
		
		</td>
</tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td><?=$xsignal?></td>
          <td>&nbsp;</td>
        </tr>
</table>
<table width=794 border=0 cellpadding=5 cellspacing=0>
<tr>
<td align=center>
<?=$lb->getLargeTitleBar($i_BookingMyRecords, "",$toolbar )?>
<?=$lb->displaySingleBookings()?>
<?=$lb->displayPeriodicBookings()?>
<?=$lb->getLargeEndBoard();?>
</td>
</tr>
</table>
</form>
<?php
include_once($PATH_WRT_ROOT."templates/homefooter.php");
intranet_closedb();
?>