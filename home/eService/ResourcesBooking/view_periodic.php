<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/librb.php");
include_once($PATH_WRT_ROOT."includes/libpb.php");
include_once($PATH_WRT_ROOT."includes/libresource.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
$lr = new librb();
$lp = new libpb();
$lp->retrievePeriodicBooking($pid);
$lresource = new libresource($lp->ResourceID);
$slots = $lr->returnSlotsString($lp->TimeSlots,$lresource->TimeSlotBatchID);
$lcycleperiods = new libcycleperiods();

if ($lp->RecordType != 3)
{
    $type_str = $lr->returnTypeString($lp->RecordType, $lp->TypeValues);
}
else
{
    $type_str = $lcycleperiods->returnCriteriaString($lp->TypeValues,$lp->BookingStartDate,$lp->BookingEndDate);
}
$dates = $lp->retrieveDates();
$assoc_date_cycle = $lcycleperiods->getCycleInfoByDateArray($dates);

$dates_str = "";
$delimiter = "";

$word_status = array($i_BookingPending, $i_status_cancel,$i_status_checkin,$i_status_checkout,$i_status_reserved,$i_status_rejected);

$detailedInfo = $lp->retrieveDetailedInfo();

$dates_str = "<table width=100% border=0 cellspacing=1 cellpadding=1>\n";
for ($i=0; $i<sizeof($detailedInfo); $i++)
{
     list($t_date, $t_slotname, $t_slottime, $t_status) = $detailedInfo[$i];
     $cycle_array = $assoc_date_cycle[$t_date];
     $name="";
     list ($txtEng, $txtChi, $txtShort) = $cycle_array;
     if ($intranet_session_language=="en")
     {
         if ($txtEng != "")
             $name = $txtEng;
         else if ($txtChi != "")
              $name = $txtChi;
     }
     else
     {
         if ($txtChi != "")
             $name = $txtChi;
         else if ($txtEng != "")
              $name = $txtEng;
     }
     $name = ($name==""? $txtShort:$name);
     if ($name != "") $name = "($name)";

     $dates_str .= "<tr><td>$t_date $name<br> <i>$t_slotname $t_slottime </i></td><td>$word_status[$t_status]</td></tr>\n";
#     $dates_str .= $delimiter.$t_date.$assoc_date_cycle[$t_date]." - $t_slotname $t_slottime - <i>".$word_status[$t_status]."</i>";
#     $delimiter = "<br>\n";
}
$dates_str .= "</table>\n";
/*
for ($i=0; $i<sizeof($cycleDays); $i++)
{
     $cycleNum = ($cycleDays[$i]!=-1)? " (".$i_DayType[$cycleDays[$i]].")" :"";
     $dates_str .= $delimiter.$dates[$i].$cycleNum;
     $delimiter = "<br>\n";
}
*/
//$dates_str = implode("<br>",$dates);
echo '<script language="Javascript" src=\'/templates/brwsniff.js\'></script>';
include_once($PATH_WRT_ROOT."templates/fileheader.php");
?>

<table width="400" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="/images/resourcesbooking/popuppapertop.gif" width="400" height="12"></td>
  </tr>
</table>
<table width="400" border="0" cellspacing="0" cellpadding="0" background="/images/resourcesbooking/poptab.gif" class="h1">
  <tr>
    <td width="25">&nbsp;</td>
    <td width="50" align="center"><img src="/images/resourcesbooking/icon_periodic.gif">
    </td>
    <td width="375"><?=$i_BookingPeriodicBooking?></td>
  </tr>
</table>
<table width="400" border="0" cellspacing="0" cellpadding="2" background="/images/resourcesbooking/popupbg.gif" class="body">
  <tr>
    <td width="120" align="right" valign="top">&nbsp;</td>
    <td valign="top" align="left" width="280">&nbsp;</td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top"><?=$i_ResourceCategory?> :</td>
    <td valign="top" align="left" width="280"><?=$lresource->ResourceCategory?><br>
    </td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top">&nbsp;</td>
    <td valign="top" align="left" width="280">&nbsp;</td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top"><?=$i_BookingItem?> :</td>
    <td valign="top" align="left" width="280"><? echo $lresource->Title." (".$lresource->ResourceCode.")"; ?><br>
    </td>
  </tr>
  <tr>
    <td align="right" valign="top">&nbsp;</td>
    <td valign="top" align="left">&nbsp;</td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top"><?=$i_BookingStartDate?> :</td>
    <td valign="top" align="left" width="280"><?=$lp->BookingStartDate?><br>
    </td>
  </tr>
  <tr>
    <td align="right" valign="top">&nbsp;</td>
    <td valign="top" align="left">&nbsp;</td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top"><?=$i_BookingEndDate?> :</td>
    <td valign="top" align="left" width="280"><?=$lp->BookingEndDate?><br>
    </td>
  </tr>
  <tr>
    <td align="right" valign="top">&nbsp;</td>
    <td valign="top" align="left">&nbsp;</td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top"><?=$i_BookingTimeSlots?> :</td>
    <td valign="top" align="left" width="280"><?=$slots?></td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top">&nbsp;</td>
    <td valign="top" align="left" width="280">&nbsp;</td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top"><?=$i_BookingPeriodicType?> :</td>
    <td valign="top" align="left" width="280"><?=$type_str?></td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top">&nbsp;</td>
    <td valign="top" align="left" width="280">&nbsp;</td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top"><?=$i_BookingPeriodicDates?> :</td>
    <td valign="top" align="left" width="280"><?=$dates_str?></td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top">&nbsp;</td>
    <td valign="top" align="left" width="280">&nbsp;</td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top"><?=$i_BookingStatus?> :<br>
    </td>
    <td valign="top" align="left" width="280"><?=$i_BookingStatusArray[$lp->RecordStatus]?>
    </td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top">&nbsp;</td>
    <td valign="top" align="left" width="280">&nbsp;</td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top"><?=$i_BookingNotes?> :</td>
    <td valign="top" align="left" width="280"><?=nl2br($lp->Remark)?></td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top">&nbsp;</td>
    <td valign="top" align="left" width="280">&nbsp;</td>
  </tr>
</table>
<table width="400" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="/images/resourcesbooking/popuppaperbottom.gif"></td>
  </tr>
</table>

<?php
include_once($PATH_WRT_ROOT."templates/filefooter.php");
intranet_closedb();
?>