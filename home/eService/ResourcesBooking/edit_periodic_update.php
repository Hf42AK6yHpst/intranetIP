<?php
/*
 *  2019-05-13 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 *      - apply cleanHtmlJavascript() before save Remark to avoid javascript injection
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/librb.php");
include_once($PATH_WRT_ROOT."includes/libpb.php");
include_once($PATH_WRT_ROOT."includes/libresource.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
intranet_auth();
intranet_opendb();

$status_cancelled = 1;
$status_reserved = 4;
$status_pending = 0;

$Remark = intranet_htmlspecialchars(cleanHtmlJavascript(trim($Remark)));
$start = intranet_htmlspecialchars(trim($start));
$end = intranet_htmlspecialchars(trim($end));
$value1 = intranet_htmlspecialchars(trim($value1));
$timeSlotsList = implode(",", $slots);

$RecordStatus = 0; # Pending

$lb = new librb();
$lp = new libpb();
$lcycleperiods = new libcycleperiods();
$lp->retrievePeriodicBooking($pid);

$newInserted = 0;
$failed = false;
# Check ownership
if ($UserID != $lp->AppliedUserID)
{
    $failed = true;
    header("Location: index.php");
    exit();
}
else
{
    # Check dates validation
    $start_stamp = strtotime($start);
    $end_stamp = strtotime($end);
    $old_start = strtotime($lp->BookingStartDate);
    $msg = 0;

    if ($lp->RecordStatus == $status_pending)
    {
        $lr = new libresource($lp->ResourceID);
        $daysBefore = $lr->DaysBefore;
        $daysBefore = $li->DaysBefore;
        $daysBefore += 0;
        $min_date = mktime(0,0,0,date("m"),(date("d")+$daysBefore),date("Y"));
        if ($min_date > $old_start)
        {
            $min_date = $old_start;
        }
        if (compareDate($start_stamp,$min_date) < 0 || compareDate($end_stamp,$start_stamp)<0)
        {
            $success = 0;
            header("Location: index.php?signal=7");
            exit();
        }



        $sql = "DELETE FROM INTRANET_BOOKING_RECORD WHERE PeriodicBookingID = '$pid' AND BookingDate >= CURDATE()";
        $lp->db_db_query($sql);
        $sql = "DELETE FROM INTRANET_PERIODIC_BOOKING WHERE PeriodicBookingID = '$pid'";
        $lp->db_db_query($sql);

        $target = array(0);
        $temp = $start_stamp;
        $count = 0;

        if ($recordType == 0)
        {
            while ($temp <= $end_stamp)
            {
                   $target[$count] = date('Y-m-d',$temp);
                   $count++;
                   $temp += 86400;
            }
            $typeValue = 0;
        }
        else if ($recordType == 1)
        {
             while ($temp <= $end_stamp)
             {
                    $target[$count] = date('Y-m-d',$temp);
                    $count++;
                    $temp += 86400*$value1;
             }
             $typeValue = $value1;
        }
        else if ($recordType == 2)
        {
             // Weekdays
             while (date("w",$temp) != $value2) $temp += 86400;
             while ($temp <= $end_stamp)
             {
                    $target[$count] = date('Y-m-d',$temp);
                    $count++;
                    $temp += 86400*7;
             }
             $typeValue = $value2;
        }
        else if ($recordType == 3)
        {
             // Cycle Days
        $targetCycle = $value3;
        $dates_array = $lcycleperiods->getDatesForCycleDay($start,$end,$targetCycle);
        $target = $dates_array;
        $typeValue = $value3;
/*
             $lc = new libcycle();
             $lc->DateToday = $start_stamp;
             $cycleID = $lc->CycleID;
             $cycle_no = $cycleID % 3 ;
             $cycle_no = ($cycle_no==0? 8: $cycle_no+5);
             $currentCycle = $lc->getCycleNumber($cycle_no);

             $targetCycle = $value3;

             $conds = "RecordStatus =1
                  AND (UNIX_TIMESTAMP(EventDate) BETWEEN ".$lc->DateStart." AND ".$lc->DateToday.")
                  AND (
                       (isSkipCycle IS NULL AND RecordType = 2)
                       OR
                       (isSkipCycle = 1)
                      )
                  AND (WEEKDAY(EventDate) NOT IN (5,6))
                  ";
             $sql = "SELECT distinct EventDate FROM INTRANET_EVENT WHERE $conds";
             $holiday = $lp->returnVector($sql);
             while ($temp <= $end_stamp)
             {
                    if (date("w",$temp) == 0 || date("w",$temp) == 6 || in_array($temp, $holiday) )
                    {
                        $temp += 86400;
                        continue;
                    }
                    if ($temp != $start_stamp)
                    {
                        $currentCycle++;
                        if ($currentCycle == $cycle_no) $currentCycle = 0;
                    }

                    if ($currentCycle == $targetCycle)
                    {
                        $target[$count] = date('Y-m-d',$temp);
                        $count++;
                    }
                    $temp += 86400;
             }
             $typeValue = $value3;
             */
        }

        if ($target[0] == 0)
        {
            $success = 0;
            $msg = 6;
        }
        else
        {
            $dates_str = "'".implode("','",$target)."'";
            $rid = $lp->ResourceID;

            # Lock tables
            $lp->db_db_query("LOCK TABLES INTRANET_BOOKING_RECORD WRITE, INTRANET_PERIODIC_BOOKING WRITE");

            # Check availability
            $sql = "SELECT CONCAT(BookingDate,TimeSlot) FROM INTRANET_BOOKING_RECORD WHERE ResourceID = '$rid' AND BookingDate IN ($dates_str) AND TimeSlot IN ($timeSlotsList) AND RecordStatus IN (2,3,4)";
            $reserved = $lp->returnVector($sql);

            # insert record in periodic record table
            $sql = "INSERT INTO INTRANET_PERIODIC_BOOKING (ResourceID,UserID,BookingStartDate,BookingEndDate,TimeSlots,Remark,RecordType,TypeValues,RecordStatus,TimeApplied,LastAction)
            values ('$rid','$UserID','$start','$end','$timeSlotsList','$Remark','$recordType','$typeValue','$status_pending',now(),now())";
            $lp->db_db_query($sql);

            $inserted_id = $lp->db_insert_id();

            # Build insert query
            $insert_values = "";
            $delimiter = "";
            for ($i=0; $i<sizeof($target); $i++)
            {
                 $currDate = $target[$i];
                 for ($j=0; $j<sizeof($slots); $j++)
                 {
                      $currSlot = $slots[$j];
                      $needle = "$currDate$currSlot";
                      if (sizeof($reserved)!=0 && in_array($needle,$reserved))
                      {
                          continue;
                      }
                      else
                      {
                          $insert_values .= "$delimiter('$rid','$UserID','$currDate','$inserted_id','$Remark','$currSlot','$status_pending',now(),now())";
                          $delimiter = ",";
                      }
                 }
            }

            if ($delimiter == "")
            {
                $sql = "DELETE FROM INTRANET_PERIODIC_BOOKING WHERE PeriodicBookingID = '$inserted_id'";
                $lp->db_db_query($sql);
            }
            else
            {
                $fields_name = "ResourceID, UserID, BookingDate, PeriodicBookingID, Remark, TimeSlot, RecordStatus,TimeApplied,LastAction";
                $sql = "INSERT INTO INTRANET_BOOKING_RECORD ($fields_name) VALUES $insert_values";
                $lp->db_db_query($sql);
            }
            $lp->db_db_query("UNLOCK TABLES");
        }
    }
    else if ($lp->RecordStatus == 4)
    {
         # Check date validation
         if ($lp->BookingStartDate > $start || $lp->BookingEndDate < $end)
         {
             $failed = true;
             header("Location: index.php?signal=7");
         }
         else
         {
             $sql = "DELETE FROM INTRANET_BOOKING_RECORD WHERE PeriodicBookingID = '$pid' AND BookingDate >= CURDATE() AND (TimeSlot NOT IN ($timeSlotsList) OR BookingDate < '$start' OR BookingDate > '$end')";
             $lp->db_db_query($sql);

             $sql = "UPDATE INTRANET_BOOKING_RECORD SET Remark = '$Remark' WHERE PeriodicBookingID = '$pid'";
             $lp->db_db_query($sql);

             $sql = "UPDATE INTRANET_PERIODIC_BOOKING SET Remark = '$Remark', BookingStartDate = '$start' , BookingEndDate = '$end', TimeSlots = '$timeSlotsList' WHERE PeriodicBookingID = '$pid'";
             $lp->db_db_query($sql);
         }

    }
}
intranet_closedb();
if (!$failed)
    header("Location: index.php?signal=3");

?>