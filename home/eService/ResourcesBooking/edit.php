<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libbooking.php");
include_once($PATH_WRT_ROOT."includes/libresource.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$lo = new libbooking($ts,$v,$BookingID);
$li = new libresource($lo->ResourceID);

$periods = $lo->displayEdit();
if ($periods == "")
    header("index.php");

$navigation = $i_frontpage_separator."<a class=home href=javascript:history.back()>$i_frontpage_rb</a>".$i_frontpage_separator.$button_edit;
echo '<script language="Javascript" src=\'/templates/brwsniff.js\'></script>';
include_once($PATH_WRT_ROOT."templates/homeheader.php");
if($lo->BookingID<>""){
?>

<?php echo $i_frontpage_rb_title_edit; ?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function copy2all (obj, text)
{
         len = obj.elements.length;
         var i=0;
         for (i=0; i<len; i++)
         {
              if (obj.elements[i].name.substring(0,6) == "Remark")
                  obj.elements[i].value = text;
         }
}
</SCRIPT>
<blockquote>
<p>
<table border=0 cellpadding=0 cellspacing=0>
<tr><td colspan=3><?php echo $i_frontpage_rb_title_form; ?></td></tr>
<tr>
<td><img src=../../images/frontpage/myinfo_bg_tl.gif border=0 width=12 height=12></td>
<td class=myinfo_bg_tm><img src=../../images/space.gif border=0 width=13 height=12></td>
<td><img src=../../images/frontpage/myinfo_bg_tr.gif border=0 width=23 height=12></td>
</tr>
<tr>
<td class=myinfo_bg_ml><img src=../../images/space.gif border=0 width=12 height=12></td>
<td class=myinfo_bg_mm>

<form action=edit_update.php method=post>
<?php echo $li->display(); ?>
<table width=400 border=0 cellpadding=2 cellspacing=1>
<tr><td><?php echo $i_frontpage_rb_date; ?>:</td><td><?php echo date("Y-m-d", $lo->DateStart); ?></td></tr>
<tr><td colspan=2><img src=/images/frontpage/myinfo_yellowbar.gif border=0 width=350 height=2 vspace=5></td></tr>
<tr><td><?php echo $i_frontpage_rb_period; ?>:</td><td><?php echo $i_frontpage_rb_note; ?></td></tr>
<?php echo $periods; ?>
<!---
<tr><td><?php echo $i_frontpage_rb_period; ?>:</td><td><?php echo date("H:i", $lo->DateStart); ?> - <?php echo date("H:i", $lo->DateEnd); ?></td></tr>
<tr><td><?php echo $i_frontpage_rb_note; ?>:</td><td><textarea name=Remark cols=30 rows=5><?php echo $lo->Remark; ?></textarea></td></tr>
-->
<tr><td width=30%><br></td><td><input type=image src=<?php echo $i_frontpage_image_save; ?> border=0> <a href=javascript:onClick=history.back()><?php echo $i_frontpage_button_cancel; ?></a></td></tr>
<tr><td width=30%><br></td><td><?php echo $i_frontpage_rb_lastmodified; ?>: <?php echo $lo->DateModified; ?></td></tr>
</table>
<!--
<input type=hidden name=BookingID value="<?php echo $lo->BookingID; ?>">
<input type=hidden name=ResourceID value="<?php echo $lo->ResourceID; ?>">
-->
<input type=hidden name=ts value="<?php echo $lo->ts; ?>">
</form>

</td>
<td class=myinfo_bg_mr><img src=../../images/space.gif border=0 width=23 height=12></td>
</tr>
<tr>
<td><img src=../../images/frontpage/myinfo_bg_bl.gif border=0 width=12 height=23></td>
<td class=myinfo_bg_bm><img src=../../images/space.gif border=0 width=13 height=23></td>
<td><img src=../../images/frontpage/myinfo_bg_br.gif border=0 width=23 height=23></td>
</tr>
</table>
</blockquote>

<?php }
include_once($PATH_WRT_ROOT."templates/homefooter.php");
intranet_closedb();
?>