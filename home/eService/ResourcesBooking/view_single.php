<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/librb.php");
include_once($PATH_WRT_ROOT."includes/libresource.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
$lr = new librb();
$lr->retrieveBooking($bid);
$lresource = new libresource($lr->ResourceID);
$whole_batch = $lr->retrieveBookingBatch();
$list = implode(",",$whole_batch);
$slots = $lr->returnSlotsString($list,$lresource->TimeSlotBatchID);

$lc = new libcycleperiods();
$temp = $lc->getCycleInfoByDate($lr->BookingDate);
$field_pos = ($intranet_session_language=="en"?0:1);
$cycle_day = $temp[$field_pos];
$cycle_string = ($cycle_day!=""? "($cycle_day)" : "");
echo '<script language="Javascript" src=\'/templates/brwsniff.js\'></script>';
include_once($PATH_WRT_ROOT."templates/fileheader.php");
?>

<table width="400" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="/images/resourcesbooking/popuppapertop.gif" width="400" height="12"></td>
  </tr>
</table>
<table width="400" border="0" cellspacing="0" cellpadding="0" background="/images/resourcesbooking/poptab.gif" class="h1">
  <tr>
    <td width="25">&nbsp;</td>
    <td width="50" align="center"><img src="/images/resourcesbooking/icon_single.gif">
    </td>
    <td width="375"><?=$i_BookingSingleBooking?></td>
  </tr>
</table>
<table width="400" border="0" cellspacing="0" cellpadding="2" background="/images/resourcesbooking/popupbg.gif" class="body">
  <tr>
    <td width="120" align="right" valign="top">&nbsp;</td>
    <td valign="top" align="left" width="280">&nbsp;</td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top"><?=$i_ResourceCategory?> :</td>
    <td valign="top" align="left" width="280"><?=$lresource->ResourceCategory?><br>
    </td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top">&nbsp;</td>
    <td valign="top" align="left" width="280">&nbsp;</td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top"><?=$i_BookingItem?> :</td>
    <td valign="top" align="left" width="280"><? echo $lresource->Title." (".$lresource->ResourceCode.")"; ?><br>
    </td>
  </tr>
  <tr>
    <td align="right" valign="top">&nbsp;</td>
    <td valign="top" align="left">&nbsp;</td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top"><?=$i_BookingDate?> :</td>
    <td valign="top" align="left" width="280"><?=$lr->BookingDate?> <?=$cycle_string?><br>
    </td>
  </tr>
  <tr>
    <td align="right" valign="top">&nbsp;</td>
    <td valign="top" align="left">&nbsp;</td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top"><?=$i_BookingTimeSlots?> :</td>
    <td valign="top" align="left" width="280"><?=$slots?></td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top">&nbsp;</td>
    <td valign="top" align="left" width="280">&nbsp;</td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top"><?=$i_BookingStatus?> :<br>
    </td>
    <td valign="top" align="left" width="280"><?=$i_BookingStatusArray[$lr->RecordStatus]?>
    </td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top">&nbsp;</td>
    <td valign="top" align="left" width="280">&nbsp;</td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top"><?=$i_BookingNotes?> :</td>
    <td valign="top" align="left" width="280"><?=nl2br($lr->Remark)?></td>
  </tr>
  <tr>
    <td width="120" align="right" valign="top">&nbsp;</td>
    <td valign="top" align="left" width="280">&nbsp;</td>
  </tr>
</table>
<table width="400" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="/images/resourcesbooking/popuppaperbottom.gif"></td>
  </tr>
</table>

<?php
include_once($PATH_WRT_ROOT."templates/filefooter.php");
intranet_closedb();
?>