<?php
# using: yat

######################################################
#
#	Date:	2011-12-14	YatWoon
#			fixed: filter problem [Case#2011-1214-0937-18066]
#
######################################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libresource.php");
include_once($PATH_WRT_ROOT."includes/librb.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($ts == "") $ts = time();
$bdate = date('Y-m-d',$ts);
$lr = new libresource();
$lrb = new librb();
$lc = new libcycleperiods();
$temp = $lc->getCycleInfoByDate($bdate);
$field_pos = ($intranet_session_language=="en"?0:1);
$cycle_day = $temp[$field_pos];
$cycle_string = ($cycle_day!=""? "($cycle_day)" : "");

$filter_cat = $lr->getSelectCats("name=cat onChange=\"this.form.item.value=''; this.form.submit();\"",$cat,$lbatch->BatchID,false);
$filter_item = $lr->getSelectItems("name=item onChange=\"this.form.submit();\"",$item,$cat,$lbatch->BatchID,false);

//$filter_cat = $lr->getSelectCats("name=cat onChange=\"this.form.item.value='';this.form.submit();\"",$cat);
//$filter_item = $lr->getSelectItems("name=item onChange=\"this.form.cat.value='';this.form.submit();\"",$item);
echo '<script language="Javascript" src=\'/templates/brwsniff.js\'></script>';
include_once($PATH_WRT_ROOT."templates/fileheader.php");
?>
<form name=form1 method=get>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10">&nbsp;</td>
    <td width="*">&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td>
    <?=$i_Booking_filter?> : <?=$filter_cat?>/<?=$filter_item?>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td class=h1><?=$bdate?> <?=$cycle_string?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <?=$lrb->displayDayView($ts,$cat,$item)?>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center"><a href="javascript:document.form1.ts.value=<?=$ts-86400?>; document.form1.submit()"><?=$image_prevday?></a><a href="javascript:document.form1.ts.value=<?=$ts+86400?>; document.form1.submit()"><?=$image_nextday?></a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<input type=hidden name=ts value=<?=$ts?>>
</form>
<?php
echo $lrb->pageTitle();

include_once($PATH_WRT_ROOT."templates/filefooter.php");
intranet_closedb();
?>