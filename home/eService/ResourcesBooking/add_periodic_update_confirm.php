<?php
// Editing by 
/*
 *  2019-06-04 Henry
 * 		- added CSRF protection
 *  2019-05-21 Henry
 *      - fix for cannot insert into INTRANET_BOOKING_RECORD
 *  2019-05-13 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 *      - apply cleanHtmlJavascript() before save Remark to avoid javascript injection
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/librb.php");
intranet_auth();
intranet_opendb();

if(!verifyCsrfToken($_POST['csrf_token'],$_POST['csrf_token_key'])){
	No_Access_Right_Pop_Up();
	exit;
}

$li = new librb();
if (!$li->hasRightToReserve($UserID))
{
     header("Location: index.php");
     exit();
}

$Remark = intranet_htmlspecialchars(cleanHtmlJavascript(trim($Remark)));
for ($i=0; $i<sizeof($dates); $i++)
{
     $dates[$i] = "'".intranet_htmlspecialchars(trim($dates[$i]))."'";
}
$dates_str = implode(",",$dates);
$slots = explode(",",$timeSlotsList);
$rid = IntegerSafe($rid);

# Get default status
$RecordStatus = 0; # Pending
$sql = "SELECT RecordType FROM INTRANET_RESOURCE WHERE ResourceID = '$rid'";
$result = $li->returnVector($sql);
if ($result[0]!="") $RecordStatus = $result[0];

$li->db_db_query("LOCK TABLES INTRANET_BOOKING_RECORD WRITE, INTRANET_PERIODIC_BOOKING WRITE");
# Check availability
$sql = "SELECT CONCAT(BookingDate,TimeSlot) FROM INTRANET_BOOKING_RECORD WHERE ResourceID = '$rid' AND BookingDate IN ($dates_str) AND TimeSlot IN ($timeSlotsList) AND RecordStatus IN (2,3,4)";
$reserved = $li->returnVector($sql);

# insert record in periodic record table
$sql = "INSERT INTO INTRANET_PERIODIC_BOOKING (ResourceID,UserID,BookingStartDate,BookingEndDate,TimeSlots,Remark,RecordType,TypeValues,RecordStatus,TimeApplied,LastAction)
values ('$rid','$UserID','$start','$end','$timeSlotsList','$Remark','$recordType','$typeValue','$RecordStatus',now(),now())";
$li->db_db_query($sql);

$inserted_id = $li->db_insert_id();

    # Build insert query
    $insert_values = "";
    $delimiter = "";
    for ($i=0; $i<sizeof($dates); $i++)
    {
         $currDate = $dates[$i];
         for ($j=0; $j<sizeof($slots); $j++)
         {
              $currSlot = $slots[$j];
              $needle = "$currDate$currSlot";
              if (sizeof($reserved)!=0 && in_array($needle,$reserved))
              {
                  continue;
              }
              else
              {
                  $insert_values .= "$delimiter('$rid','$UserID',$currDate,'$inserted_id','$Remark','$currSlot','$RecordStatus',now(),now())";
                  $delimiter = ",";
              }
         }
    }

    if ($delimiter == "")
    {
        $sql = "DELETE FROM INTRANET_PERIODIC_BOOKING WHERE PeriodicBookingID = '$inserted_id'";
        $li->db_db_query($sql);
    }
    else
    {
        $fields_name = "ResourceID, UserID, BookingDate, PeriodicBookingID, Remark, TimeSlot, RecordStatus,TimeApplied,LastAction";
        $sql = "INSERT INTO INTRANET_BOOKING_RECORD ($fields_name) VALUES $insert_values";
        $li->db_db_query($sql);
    }

$li->db_db_query("UNLOCK TABLES");

intranet_closedb();
header("Location: index.php?signal=1");

?>