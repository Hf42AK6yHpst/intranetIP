<?php
/*
 *  2019-06-04 Henry
 * 		- added CSRF protection
 *  2019-05-13 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 *      - apply IntegerSafe() to $rid
 *      - apply cleanHtmlJavascript() before save Remark to avoid javascript injection
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/librb.php");
intranet_auth();
intranet_opendb();

if(!verifyCsrfToken($_POST['csrf_token'],$_POST['csrf_token_key'])){
	No_Access_Right_Pop_Up();
	exit;
}

$RecordStatus = 0; # Pending

$li = new librb();
if (!$li->hasRightToReserve($UserID))
{
     header("Location: index.php");
     exit();
}
$Remark = htmlspecialchars(cleanHtmlJavascript(trim($Remark)));
$bdate = htmlspecialchars(trim($bdate));
$rid = IntegerSafe($rid);

# Get default status and days before
$sql = "SELECT RecordType, DaysBefore FROM INTRANET_RESOURCE WHERE ResourceID = '$rid'";
$result = $li->returnArray($sql,2);
if ($result[0][0]!="") $RecordStatus = $result[0][0];
$daysBefore = $result[0][1];
$daysBefore += 0;
$min_date = mktime(0,0,0,date("m"),(date("d")+$daysBefore),date("Y"));
$target_stamp = strtotime($bdate);
if ($target_stamp < $min_date)
{
    header("Location: index.php?signal=7");
    exit();
}

$timeSlotsList = implode("','", $slots);
$li->db_db_query("LOCK TABLES INTRANET_BOOKING_RECORD WRITE");
# Check availability
$sql = "SELECT count(BookingID) FROM INTRANET_BOOKING_RECORD WHERE ResourceID = '$rid' AND BookingDate = '$bdate' AND TimeSlot IN ('$timeSlotsList') AND RecordStatus IN (2,3,4)";
$count = $li->returnVector($sql);
if ($count[0]!=0)
{
    header("Location: index.php?signal=2");
}
else
{
    # Build insert query
    $insert_values = "";
    $delimiter = "";
    for ($i=0; $i<sizeof($slots); $i++)
    {
         $pos = $slots[$i];
         $insert_values .= "$delimiter ('$rid', '$UserID', '$bdate', null, '$Remark', '$pos', '$RecordStatus', now(),now())";
         $delimiter = ",";
    }

    $fields_name = "ResourceID, UserID, BookingDate, PeriodicBookingID, Remark, TimeSlot, RecordStatus,TimeApplied,LastAction";
    $sql = "INSERT INTO INTRANET_BOOKING_RECORD ($fields_name) VALUES $insert_values";
    $li->db_db_query($sql);
}
$li->db_db_query("UNLOCK TABLES");
intranet_closedb();
header("Location: index.php?signal=1");

?>