<?php
include_once ("../../../../includes/global.php");
include_once ("../../../../includes/libdb.php");
include_once ("../../../../includes/libuser.php");
include_once ("../../../../includes/libslslib.php");
include_once ("../../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
$lu = new libuser($UserID);
$lsls = new libslslib();
$oid = $lu->UserLogin;
#$oid = "1a01";        // Hardcode for testing
$fine = $lsls->getOutstandingFine($oid);
#$fine += 3;
if ($fine != 0)
{
    #$fine_statement = "$i_sls_library_outstanding_fine:<span style=\"color:red; font-size:18px; font-family: Arial;\"> \$$fine</font>";
    $fine_statement = "<table width=135 border=0 cellspacing=0 cellpadding=0>
              <tr>
                <td><img src=\"images/library/outstandingfine_$intranet_session_language.gif\"></td>
              </tr>
              <tr>
                <td align=\"center\" bgcolor=\"#FAA931\"><strong><font color=\"#FFFFFF\" size=\"5\">\$$fine</font></strong></td>
              </tr>
            </table>";
}
else
{
    $fine_statement = "<table width=135 border=0 cellspacing=0 cellpadding=0>
              <tr>
                <td><img src=\"$image_path/spacer.gif\" width=135></td>
              </tr>
            </table>";
}
include_once ("../../../../templates/homeheader.php");
?>
<p><img src="images/library/title_library_<?=$intranet_session_language?>.gif">
<table width="80%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="20"><img src="images/library/lib_top_l.gif" width="20" height="47"></td>
    <td align="center" STYLE="background-image: url(images/library/lib_cell_t.gif)"><img src="images/library/lib_top_m.gif" width="519" height="47"></td>
    <td width="20"><img src="images/library/lib_top_r.gif" width="20" height="47"></td>
  </tr>
  <tr>
    <td STYLE="background-image: url(images/library/lib_cell_l.gif)">&nbsp;</td>
    <td align="left" bgcolor="#FBFEEE">
      <table width="75%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="5" align="center" valign="top">
          <?=$fine_statement?>
          </td>
          <td width="285">
              <a href='javascript:newWindow("new_additions.php", 1)'><img border=0 src="images/library/bk_new.gif" width="74" height="46"><img border=0 src="images/library/cat_new_<?=$intranet_session_language?>.gif" width="211" height="46"></a>
          </td>
        </tr>
        <tr>
          <td><a href='javascript:newWindow("patron_top_list.php", 1)'><img border=0 src="images/library/bk_top10patrons.gif" width="74" height="46"><img border=0 src="images/library/cat_top10patrons_<?=$intranet_session_language?>.gif" width="211" height="46"></a></td>
        </tr>
        <tr>
          <td><a href='javascript:newWindow("item_top_list.php", 1)'><img border=0 border=0 src="images/library/bk_top10items.gif" width="74" height="46"><img border=0 src="images/library/cat_top10items_<?=$intranet_session_language?>.gif" width="211" height="46"></a></td>
        </tr>
        <tr>
          <td><a href='javascript:newWindow("checkout_list.php", 1)'><img border=0 border=0 src="images/library/bk_checkoutlist.gif" width="74" height="46"><img border=0 src="images/library/cat_checkoutlist_<?=$intranet_session_language?>.gif" width="211" height="46"></a></td>
        </tr>
        <tr>
          <td><a href='javascript:newWindow("hold_list.php", 1)'><img border=0 src="images/library/bk_holdlist.gif" width="74" height="46"><img border=0 src="images/library/cat_holdlist_<?=$intranet_session_language?>.gif" width="211" height="46"></a></td>
        </tr>
      </table></td>
    <td STYLE="background-image: url(images/library/lib_cell_r.gif)">&nbsp;</td>
  </tr>
  <tr>
    <td><img src="images/library/lib_btm_l.gif" width="20" height="16"></td>
    <td STYLE="background-image: url(images/library/lib_cell_b.gif)"><img src="images/library/lib_cell_b.gif" width="10" height="16"></td>
    <td><img src="images/library/lib_btm_r.gif" width="20" height="16"></td>
  </tr>
</table>

<?
include_once ("../../../../templates/homefooter.php");
intranet_closedb();
?>