<?php 
echo $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
?>
<script src="/templates/eForm/assets/libs/jquery.3.1.1.js"></script>
<script src="/templates/eForm/assets/libs/jquery-migrate-1.4.1.js"></script>
<link rel="stylesheet" href="/templates/slrs/css/font-awesome.min.css" type="text/css" />
<link href="/templates/eForm/assets/css/jquery.alerts.css" rel="stylesheet" type="text/css">
<script src="/templates/eForm/assets/libs/jquery.alerts.js"></script>
<link href="/templates/eForm/assets/css/eform_viewer.css?t=<?php echo time(); ?>" rel="stylesheet" type="text/css">
<script src="/templates/eForm/assets/js/eform_viewer.js?t=<?php echo time(); ?>"></script>
<div class='form-preview form-style-2'>
	<div class='form-wrap'>
<?php
if ($formInfo["GridInfo"]["isDeleted"] != "1") {
	echo $htmlAry['formBody'];
} else {
	echo $Lang["eForm"]["FormNotFound"];
}
?>
	</div>
</div>
<div style='text-align:center; margin-top:20px; margin-bottom:20px;'>
<?php echo $indexVar['libeform_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn'); ?>
</div>
<script type="text/javascript">
	function goBack() {
		<?php if ($formInfo["GridInfo"]["Draft"] == "1") { ?>
		window.location.href = "?task=forminfo/list";
		<?php } else { ?>
		window.location.href = "?task=forminfo/submittedlist";
		<?php } ?>
	}
</script>