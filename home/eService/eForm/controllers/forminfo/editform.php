<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (isset($_POST['keyword'])) {
	$keyword = $_POST['keyword'];
}

$formData = array();
// if (isset($_POST["groupIdAry"][0]) && $_POST["groupIdAry"][0] > 0) {
$thisCTK =  $indexVar["FormInfo"]->getToken($_GET["fid"] . "_" . $_GET["gid"]);
if (isset($_GET["fid"]) && !empty($_GET["fid"]) && ($_GET["ctk"] == $thisCTK)) { 
	// $FormID = $_POST["groupIdAry"][0];
	$FormID = $_GET["fid"];
	$GridID = $_GET["gid"];
	$formInfo = $indexVar["FormInfo"]->getFormInfoById($FormID);
	if (isset($formInfo["FormID"]) && isset($formInfo["TemplateID"]) && $formInfo["TemplateID"] > 0) {
		$formInfo["TemplateInfo"] = $indexVar["FormTemplate"]->getFormTemplateInfoById($formInfo["TemplateID"], true);
		/*echo "<Pre>";
		print_r($formInfo["TemplateInfo"]);
		exit;*/
		if (empty($GridID)) {
			$formInfo["GridInfo"] = $indexVar["FormGrid"]->initGridInfo($formInfo);
		} else {
			$formInfo["GridInfo"] = $indexVar["FormGrid"]->getGridInfoByID($GridID, $formInfo);
		}
		if (count($formInfo["GridInfo"]) > 0) {
			if ($formInfo["GridInfo"]["GridStatus"] == "1") {
				header("Location: ?task=forminfo/viewform&fid=" . $_GET["fid"] . "&gid=" . $_GET["gid"] . "&ctk=" . $_GET["ctk"]);
				exit;
			} else {
				$formInfo["GridInfo"]["SuppReqInfo"] =  $indexVar["FormGrid"]->getGridLatestSuppInfo($_GET["gid"]);
				$formInfo["GridInfo"]["GridData"] =  $indexVar["FormGrid"]->getGridDataByID($GridID);
				$htmlAry['formBody'] = $indexVar['libeform_ui']->return_FormBuilderByTemplate($formInfo, $formInfo["GridInfo"]["GridData"], false);
			}
			# Page Title
			$TAGS_OBJ[] = array($Lang['eForm']['FormInfo']);
			$indexVar['libeform_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
			echo $indexVar['libinterface']->Include_AutoComplete_JS_CSS();
			echo $indexVar['libeform_ui']->Include_Thickbox_JS_CSS();
			echo $indexVar['libinterface']->Include_JS_CSS();
			
		} else {
			No_Access_Right_Pop_Up();
		}
	} else {
		No_Access_Right_Pop_Up();
	}
	$PAGE_NAVIGATION[] = array($Lang["eForm"]["FormPreview"]);
} else {
	No_Access_Right_Pop_Up();
}

$htmlAry['backBtn'] = $indexVar['libeform_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
?>