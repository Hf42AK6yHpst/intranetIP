<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
### check access right

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


$trueToken = false;
if ($_POST["saveAsDraft"] == "SD") {
	$thisToken = $indexVar["FormInfo"]->getToken($_POST["fid"] . "_" . $_POST["gid"]. "_TRUE");
	if ($thisToken == $_POST["stk"]) {
		$trueToken = true;
	}
} else {
	$thisToken = $indexVar["FormInfo"]->getToken($_POST["fid"] . "_" . $_POST["gid"]. "_FALSE");
	if ($thisToken == $_POST["ftk"]) {
		$trueToken = true;
	}
}
if ($trueToken) {
	$reponse = array("result" => false);
	$formInfo = $indexVar["FormInfo"]->getFormInfoById($_POST["fid"]);
	if (isset($formInfo["FormID"]) && isset($formInfo["TemplateID"]) && $formInfo["TemplateID"] > 0) {
		$formInfo["TemplateInfo"] = $indexVar["FormTemplate"]->getFormTemplateInfoById($formInfo["TemplateID"], true);
		$formInfo["GridInfo"] = $indexVar["FormGrid"]->getGridInfoByID($_POST["gid"], $formInfo);
		
		if ( $formInfo["GridInfo"]["GridID"] > 0
			&& !in_array($formInfo["GridInfo"]["GridStatus"], array( $indexVar["FormGrid"]->GridStatusArr["COMPLETED"]))
				&& count($_POST) > 0 && is_array($_POST)
		) {
			$saveAsDraft = true;
			if ($_POST["saveAsDraft"] == "FS") {
				$saveAsDraft = false;
			} else {
				$saveAsDraft = true;
			}
			$reponse = $indexVar["FormGrid"]->editGridData($_POST, $saveAsDraft);
		}
	}
	if ($reponse["result"]) {
		sleep(1);
?>
<script language="javascript" type='text/javascript'>
	window.parent.top.saveSuccess();
</script>
<?php
	} else {
?>
<script language="javascript" type='text/javascript'>
	window.parent.top.saveFail();
</script>
<?php
	}
} else {
	No_Access_Right_Pop_Up();
}
?>