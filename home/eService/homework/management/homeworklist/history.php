<?php
	// Modifing by yat
		
	$PATH_WRT_ROOT = "../../../../../";

	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libhomework.php");
	include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
		
	intranet_auth();
	intranet_opendb();

	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION['UserType']==USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"])))
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	# Temp Assign memory of this page
	ini_set("memory_limit", "150M");

	# Create a new interface instance
	$linterface = new interface_html();

	# Create a new homework instance
	$lhomework = new libhomework2007();

	$CurrentPageArr['eServiceHomework'] = 1;

	# Select current page
	$CurrentPage = "Management_HomeworkList";

	# Page title
	$PAGE_TITLE = $Lang['SysMgr']['Homework']['HomeworkList'];

	$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

	# Select sort field
	$sortField = 0;
	
	# change page size
	if ($page_size_change == 1)
	{
		setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
		$ck_page_size = $numPerPage;
	}

	if (isset($ck_page_size) && $ck_page_size != "") 
		$page_size = $ck_page_size;

	$pageSizeChangeEnabled = true;

	# Table initialization
	$order = ($order == "") ? 1 : $order;
	$field = ($field == "") ? $sortField : $field;
	$pageNo = ($pageNo == "") ? 1 : $pageNo;

	# Create a new dbtable instance
	$li = new libdbtable2007($field, $order, $pageNo);
	
	# Settings
	$s = addcslashes($s, '_');
	$searchByTeacher =($lhomework->teacherSearchDisabled == 0) ? "or a.PosterName like '%$s%'" : "" ;
	$searchBySubject =($lhomework->subjectSearchDisabled == 0 && $lhomework->subjectsTaughtSearchDisabled == 0) ? " or IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) like '%$s%'" : "" ;
	$allowExport = $lhomework->exportAllowed;
	
	$handInStatusArr = $lhomework->getAllHandInStatus();
	
	# Student Mode
	if($_SESSION['UserType']==USERTYPE_STUDENT)
	{
		# Academic Year
		$academicYear = $lhomework->GetAllAcademicYear();
		$yearID = ($yearID=="")? Get_Current_Academic_Year_ID():$yearID;
		
		$selectedYear = $lhomework->getCurrentYear("name=\"yearID\" onChange=\"reloadForm()\"", $academicYear, $yearID, "", false);
		
		if($yearID!=""){
		
			if($yid!="" && $yid != $yearID){
				$yearTermID="";
			}
			
			# Current Year Term
			$academicYearTerm = $lhomework->getAllAcademicYearTerm($yearID);
			$selectedYearTerm = $lhomework->getCurrentYearTerm("name=\"yearTermID\" onChange=\"reloadForm()\"", $academicYearTerm, $yearTermID, $Lang['SysMgr']['Homework']['AllYearTerms']);
			
			if($ytid!="" && $ytid != $yearTermID){
				$subjectID="";
			}
			
			# Subject Menu
			$subject = $lhomework->getStudyingSubjectList($UserID, "", $yearID, $yearTermID);
			$selectedSubject = $lhomework->getCurrentStudyingSubjects("name=\"subjectID\" onChange=\"this.form.method='get'; this.form.action=''; this.form.submit();\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);

			$filterbar = "$selectedYear $selectedYearTerm $selectedSubject";
		}
		# SQL statement
		if(sizeof($subject)!=0){
			$allSubjects = " a.SubjectID IN (";
			for ($i=0; $i < sizeof($subject); $i++)
			{	
				list($ID)=$subject[$i];
				$allSubjects .= $ID.",";
			}
			$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
			$allSubjects .= " AND";
		}
		else{
			$allSubjects ="";
		}
		
		# Set conditions
		$date_conds = "AND a.DueDate < CURDATE()";
		$conds = ($subjectID=='')? "$allSubjects" : " a.SubjectID = $subjectID AND";
		$conds .= ($s=='')? "": " (IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) like '%$s%'
									or a.Title like '%$s%'
									$searchByTeacher 
									$searchBySubject
								) AND";

		$desc = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_alt.gif\" title=\"',a.Description,'\" \>')";						
		$attach = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_attachment.gif\" title=\"$i_AnnouncementAttachment\" \>')";
		
		$fields = "IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) AS Subject, 
				   IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroup, 
				   CONCAT(CONCAT(CONCAT(CONCAT('<a class=tablelink href=javascript:viewHmeworkDetail(',a.HomeworkID,')>',a.Title,'</a>'), if (a.StartDate=CURDATE(),' $i_Homework_today','')), IF(a.Description='', '',$desc)), IF(a.AttachmentPath!='NULL', $attach, '')), 
				   a.StartDate, a.DueDate, a.Loading/2, (if (a.HandinRequired=1,'". $i_general_yes ."','". $i_general_no ."')), ";
		if($lhomework->useHomeworkCollect)
		{
			$fields .= "(if (a.CollectRequired=1,'". $i_general_yes."','". $i_general_no."')), ";
		}
		// $fields .= " IF(e.RecordStatus=1, '".$Lang['SysMgr']['Homework']['Submitted']."', IF(e.RecordStatus=6,'".$Lang['SysMgr']['Homework']['Supplementary']."',IF(e.RecordStatus=-1,'".$Lang['SysMgr']['Homework']['ExpiredWithoutSubmission']."', IF(e.RecordStatus=2, '".$Lang['SysMgr']['Homework']['LateSubmitted']."','--'))))";
		$fields .= " IF(e.RecordStatus=1,
						'".$Lang['SysMgr']['Homework']['Submitted']."',
					 	IF(e.RecordStatus=6,
							'".$Lang['SysMgr']['Homework']['Supplementary']."',
							(
								IF(e.RecordStatus=-1 OR e.RecordStatus=-12 OR e.RecordStatus=-13,
									IF(CURDATE()<a.DueDate,
										'".$Lang['SysMgr']['Homework']['NotSubmitted']."',
										IF(e.SuppRecordStatus=7,
											'". Get_Lang_Selection($handInStatusArr["7"]["HandInStatusTitleB5"], $handInStatusArr["7"]["HandInStatusTitleEN"]) ."',
											IF(e.SuppRecordStatus=-11,
												'". Get_Lang_Selection($handInStatusArr["-11"]["HandInStatusTitleB5"], $handInStatusArr["-11"]["HandInStatusTitleEN"]) ."',
												'".$Lang['SysMgr']['Homework']['ExpiredWithoutSubmission']."'
											)
										)
									),
									IF(e.RecordStatus=2, '".$Lang['SysMgr']['Homework']['LateSubmitted']."','--')
								)
							)
						)
					)";
		$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID 
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS d ON d.SubjectGroupID = a.ClassGroupID
					 LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.HomeworkID = a.HomeworkID AND e.StudentID = d.UserID)";
					 
		$conds .= " d.UserID = $UserID AND a.AcademicYearID = $yearID $date_conds";
		$conds .=($yearTermID=='')? "" : " AND a.YearTermID =$yearTermID";
		
		$sql = "SELECT $fields FROM $dbtables WHERE $conds";

		if($lhomework->useHomeworkCollect)
		{
			$li->field_array = array("Subject", "SubjectGroup", "a.Title", "a.StartDate", "a.DueDate", "a.Loading", "a.HandinRequired", "a.CollectRequired", "e.RecordStatus");
		}
		else
		{
			$li->field_array = array("Subject", "SubjectGroup", "a.Title", "a.StartDate", "a.DueDate", "a.Loading", "a.HandinRequired", "e.RecordStatus");
		}
			
		$li->sql = $sql;
		$li->no_col = sizeof($li->field_array)+2;
// 		if($lhomework->useHomeworkCollect)
// 				$li->no_col++;

		$li->IsColOff = 2;

		# TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<td width='5%'>#</td>\n";
		$li->column_list .= "<td width='10%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Subject'])."</td>\n";
		$li->column_list .= "<td width='15%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['SubjectGroup'])."</td>\n";
		$li->column_list .= "<td width='18%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Topic'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['StartDate'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['DueDate'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Workload']." (".$Lang['SysMgr']['Homework']['Hours'].")")."</td>\n";
		$li->column_list .= "<td width='8%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['HandinRequired'])."</td>\n";
		if($lhomework->useHomeworkCollect)
			$li->column_list .= "<td width='12%' class='tabletop tabletoplink' nowrap>".$li->column($pos++, $i_Homework_Collected_By_Class_Teacher)."</td>\n";
		$li->column_list .= "<td width='14%' class='tabletop tabletoplink' nowrap>".$li->column($pos++, $Lang['SysMgr']['Homework']['Status'])."</td>\n";
		$li->column_list .= "<td>&nbsp;</td>";
		
// 		debug_pr($li->built_sql());
// 		exit;
	}
	
	# Parent Mode
	else if($_SESSION['UserType']==USERTYPE_PARENT)
	{
		# Children Menu
		$children = $lhomework->getChildrenList($UserID);
		$childrenID = ($childrenID=="")? $children[0][0]:$childrenID;
		
		$selectedChildren = $lhomework->getCurrentChildren("name=\"childrenID\" onChange=\"reloadForm()\"", $children, $childrenID, "", false);
		if($childrenID!=""){
			# Academic Year
			$academicYear = $lhomework->GetAllAcademicYear();
			$yearID = ($yearID=="")? Get_Current_Academic_Year_ID():$yearID;
			
			$selectedYear = $lhomework->getCurrentYear("name=\"yearID\" onChange=\"reloadForm()\"", $academicYear, $yearID, "", false);
			
			if($yid!="" && $yid != $yearID){
				$yearTermID="";
			}
			
			# Current Year Term
			$academicYearTerm = $lhomework->getAllAcademicYearTerm($yearID);
			$selectedYearTerm = $lhomework->getCurrentYearTerm("name=\"yearTermID\" onChange=\"reloadForm()\"", $academicYearTerm, $yearTermID, $Lang['SysMgr']['Homework']['AllYearTerms']);
			
			if($ytid!="" && $ytid != $yearTermID){
				$subjectID="";
			}
			
			# Subject Menu
			$subject = $lhomework->getStudyingSubjectList($childrenID, "", $yearID, $yearTermID);
			
			if($cid != $childrenID){
				$subjectID ="";
			}
		
			$selectedSubject = $lhomework->getCurrentStudyingSubjects("name=\"subjectID\" onChange=\"reloadForm()\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);
		
			$filterbar = "$selectedChildren $selectedYear $selectedYearTerm $selectedSubject";
		}
		if(sizeof($subject)!=0){
			$allSubjects = " a.SubjectID IN (";
			for ($i=0; $i < sizeof($subject); $i++)
			{	
				list($ID)=$subject[$i];
				$allSubjects .= $ID.",";
			}
			$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
			$allSubjects .= "AND";
		}
		else{
			$allSubjects ="";
		}
			
		# SQL statement
		$date_conds = " a.DueDate < CURDATE()";
		$conds = ($childrenID=='')? "" : " d.UserID = $childrenID AND";
		$conds .= ($subjectID=='')? " $allSubjects" : " a.SubjectID = $subjectID AND";

// 		$conds .= ($s=='')? "": " AND (IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) like '%$s%'
// 									or a.Title like '%$s%'
// 									$searchByTeacher
// 									$searchBySubject
// 								)";
		$conds .= ($s=='')? "": " (IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) like '%$s%'
									or a.Title like '%$s%'
									$searchByTeacher
									$searchBySubject
								) AND";

		$desc = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_alt.gif\" title=\"',a.Description,'\" \>')";						
		$attach = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_attachment.gif\" title=\"$i_AnnouncementAttachment\" \>')";
		
		$fields = "IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) AS Subject, 
				   IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroup, 
				   CONCAT(CONCAT(CONCAT(CONCAT('<a class=tablelink href=javascript:viewHmeworkDetail(',a.HomeworkID,')>',a.Title,'</a>'), if (a.StartDate=CURDATE(),' $i_Homework_today','')), IF(a.Description='', '',$desc)), IF(a.AttachmentPath!='NULL', $attach, '')), 
				   a.StartDate, a.DueDate, a.Loading/2, (if (a.HandinRequired=1,'". $i_general_yes ."','". $i_general_no ."')),"; 
		if($lhomework->useHomeworkCollect)
		{
			$fields .= "(if (a.CollectRequired=1,'". $i_general_yes."','". $i_general_no."')), ";
		}			   
		// $fields .= " IF(e.RecordStatus=1, '".$Lang['SysMgr']['Homework']['Submitted']."', IF(e.RecordStatus=6,'".$Lang['SysMgr']['Homework']['Supplementary']."',IF(e.RecordStatus=-1,'".$Lang['SysMgr']['Homework']['ExpiredWithoutSubmission']."', IF(e.RecordStatus=2, '".$Lang['SysMgr']['Homework']['LateSubmitted']."','--'))))";
		$fields .= " IF(e.RecordStatus=1,
						'".$Lang['SysMgr']['Homework']['Submitted']."',
					 	IF(e.RecordStatus=6,
							'".$Lang['SysMgr']['Homework']['Supplementary']."',
							(
								IF(e.RecordStatus=-1 OR e.RecordStatus=-12 OR e.RecordStatus=-13,
									IF(CURDATE()<a.DueDate,
										'".$Lang['SysMgr']['Homework']['NotSubmitted']."',
										IF(e.SuppRecordStatus=7,
											'". Get_Lang_Selection($handInStatusArr["7"]["HandInStatusTitleB5"], $handInStatusArr["7"]["HandInStatusTitleEN"]) ."',
											IF(e.SuppRecordStatus=-11,
												'". Get_Lang_Selection($handInStatusArr["-11"]["HandInStatusTitleB5"], $handInStatusArr["-11"]["HandInStatusTitleEN"]) ."',
												'".$Lang['SysMgr']['Homework']['ExpiredWithoutSubmission']."'
											)
										)
									),
									IF(e.RecordStatus=2, '".$Lang['SysMgr']['Homework']['LateSubmitted']."','--')
								)
							)
						)
					)";
		
		$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID 
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS d ON d.SubjectGroupID = a.ClassGroupID
					 LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.HomeworkID = a.HomeworkID AND e.StudentID = d.UserID)";
					 
		
		$conds .= " $date_conds AND a.AcademicYearID = $yearID";
		$conds .=($yearTermID=='')? "" : " AND a.YearTermID =$yearTermID";
		
		$sql = "SELECT $fields FROM $dbtables WHERE $conds";

		
		// echo $sql;
		$li->field_array = array("Subject", "SubjectGroup", "a.Title", "a.StartDate", "a.DueDate", "a.Loading", "a.HandinRequired", "e.RecordStatus"); 
		$li->sql = $sql;
		$li->no_col = sizeof($li->field_array)+2;
		if($lhomework->useHomeworkCollect)
				$li->no_col++;
		$li->IsColOff = 2;

		# TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<td width='5%'>#</td>\n";
		$li->column_list .= "<td width='10%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Subject'])."</td>\n";
		$li->column_list .= "<td width='15%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['SubjectGroup'])."</td>\n";
		$li->column_list .= "<td width='18%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Topic'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['StartDate'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['DueDate'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Workload']." (".$Lang['SysMgr']['Homework']['Hours'].")")."</td>\n";
		$li->column_list .= "<td width='8%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['HandinRequired'])."</td>\n";
		if($lhomework->useHomeworkCollect)
			$li->column_list .= "<td width='12%' class='tabletop tabletoplink' nowrap>".$li->column($pos++, $i_Homework_Collected_By_Class_Teacher)."</td>\n";
		$li->column_list .= "<td width='14%' class='tabletop tabletoplink' nowrap>".$li->column($pos++, $Lang['SysMgr']['Homework']['Status'])."</td>\n";
		$li->column_list .= "<td>&nbsp;</td>";
		
	}
	
	# tag information
	if($_SESSION['UserType']==USERTYPE_PARENT){
		$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?childrenID=$childrenID&subjectID=$subjectID&cid=$cid", 0);
		$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?yearID=$yearID&yearTermID=$yearTermID&childrenID=$childrenID&subjectID=$subjectID&cid=$cid", 1);
	}
	else{
		$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?subjectID=$subjectID", 0);
		$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID", 1);
	}
	
	# Start layout
	$linterface->LAYOUT_START();
?>

<script language="javascript">
	function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
	}

	
	function reloadForm() {
		document.form1.s.value = "";
		document.form1.pageNo.value = 1;
		document.form1.submit();
	}

	
	function goSearch() {
		document.form1.s.value = document.form2.text.value;
		document.form1.pageNo.value = 1;
		document.form1.submit();
	}
	
	
	function viewHmeworkDetail(id)
	{
		   newWindow('./view.php?hid='+id,1);
	}

	
	function viewHandinList(id)
	{
		   newWindow('./handin_list.php?hid='+id,10);
	}

</script>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post" onSubmit="return false">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td><?=$toolbar ?></td>
								</tr>
							</table>
						</td>
						<td align="right"><input name="text" type="text" class="formtextbox" value="<?=stripslashes(stripslashes($s))?>">
							<?=$linterface->GET_BTN($button_find, "button","javascript:goSearch()");?>
						</td>
					</tr>
				</table>
			</form>
			<form name="form1" method="post" action="">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right"><?= $linterface->GET_SYS_MSG($msg) ?>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td height="30">
												<?=$filterbar?>
											</td>
											<td align="right" valign="middle" class="thumb_list"><?=$choiceType?></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
											</td>
											<td align="right" valign="bottom">
											<br/>
											<br/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<?
						if($_SESSION['UserType']==USERTYPE_PARENT){
							if($childrenID!="")
								echo "<td>".$li->display()."</td>";
							else{}
						}
						else{
							echo "<td>".$li->display()."</td>";
						}
					?>
				</tr>
			</table><br>
			<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>"/>
			<input type="hidden" name="order" value="<?php echo $li->order; ?>"/>
			<input type="hidden" name="field" value="<?php echo $li->field; ?>"/>
			<input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
			<input type="hidden" name="page_size_change" value=""/>
			<input type="hidden" name="s" value="<?=$s?>"/>
			<input type="hidden" name="cid" value="<?=$childrenID?>"/>
			<input type="hidden" name="ytid" value="<?=$yearTermID?>"/>
			<input type="hidden" name="yid" value="<?=$yearID?>"/>
			</form>
		</td>
	</tr>
</table>
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>