<?php
// Modifing by 

#####################################################
#	Date:	2017-06-30	Carlos
#			$sys_custom['LivingHomeopathy'] Added upload homework document function, display marked document and rating. 
#
#	Date:	2012-09-25	YatWoon
#			Fixed: missing to change the description format (line break, hyper link) [Case#2012-0924-1138-04073]
#
#####################################################
		
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION['UserType']==USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lhomework = new libhomework2007();
$linterface = new interface_html("popup.html");

$CurrentPageArr['eAdminHomework'] = 1;

$MODULE_OBJ['title'] = $Lang['SysMgr']['Homework']['HomeworkList'];


$record= $lhomework->returnRecord($hid);

list($HomeworkID, $subjectGroupID, $subjectID, $start, $due, $loading, $title, $description, $lastModified, $homeworkType, $PosterName, $TypeID, $AttachmentPath,$handinRequired,$collectRequired, $PostUserID)=$record; 

$description = nl2br($lhomework->convertAllLinks($description,40));

$lu = new libuser($PostUserID);
$PosterName2 = $lu->UserName();

if($lhomework->useHomeworkType) {
	$typeInfo = $lhomework->getHomeworkType($TypeID);
	$typeName = ($typeInfo[0]['TypeName']) ? $typeInfo[0]['TypeName'] : "---";
}

$subjectName = $lhomework->getSubjectName($subjectID);
$subjectGroupName = $lhomework->getSubjectGroupName($subjectGroupID);

if($AttachmentPath!=NULL){
	$displayAttachment = $lhomework->displayAttachment($hid);
}

if($sys_custom['LivingHomeopathy'] && ($_SESSION['UserType'] == USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"]) ) ){
	$include_js = '<script type="text/javascript" language="JavaScript" src="/templates/jquery/jquery.blockUI.js"></script>'."\n";
	$include_js.= '<script type="text/javascript" language="JavaScript" src="/templates/jquery/jquery.scrollTo-min.js"></script>'."\n";
	$form_enctype = ' enctype="multipart/form-data" ';
	$document_size_limit = isset($sys_custom['eHomework']['HomeworkDocumentSizeLimit']) && is_numeric($sys_custom['eHomework']['HomeworkDocumentSizeLimit'])? $sys_custom['eHomework']['HomeworkDocumentSizeLimit'] : 100; // default 100MB
	if($_SESSION['UserType']==USERTYPE_PARENT && $childrenID==''){
		$children = $lhomework->getChildrenList($_SESSION['UserID']);
		$childrenID = $children[0][0];
	}
	$student_id = $_SESSION['UserType'] == USERTYPE_STUDENT? $_SESSION['UserID'] : $childrenID;
	$handin_records = $lhomework->getHomeworkHandinListRecords(array('HomeworkID'=>$HomeworkID,'StudentID'=>$student_id,'GetTeacherName'=>1));
	if(count($handin_records)==0){
		// init submission status to under processing (5)
		$sql = "INSERT INTO INTRANET_HOMEWORK_HANDIN_LIST (HomeworkID,StudentID,RecordStatus,MainStatusRecordDate,DateInput,DateModified) VALUES ('".$HomeworkID."','".$student_id."','".(isset($sys_custom['eHoemwork_default_status'])?$sys_custom['eHoemwork_default_status']:5)."',NOW(),NOW(),NOW())";
		$lhomework->db_db_query($sql);
		$handin_records = $lhomework->getHomeworkHandinListRecords(array('HomeworkID'=>$HomeworkID,'StudentID'=>$_SESSION['UserID']));
	}
	$handin_record = $handin_records[0];
	$today = date("Y-m-d");
	$xx = '<tr>
				<td class="tabletext formfieldtitle" width="30%" valign="top">'.$Lang['eHomework']['HandInHomework'].'</td>
				<td id="StudentDocumentContainer" align="left" valign="top">';
					$xx .= '<input type="hidden" name="RecordID" id="RecordID" value="'.$handin_record['RecordID'].'" />';
			if($_SESSION['UserType'] == USERTYPE_STUDENT && $today >= $start){
				$display_uploader = $handin_record['StudentDocument'] == ''? 1: 0;
				$xx .= '<div id="UploadSpinner" style="display:none">'.$linterface->Get_Ajax_Loading_Image(0,$Lang['eHomework']['Uploading']).'</div>';
				$xx .= '<div id="StudentDocumentUpload" '.($display_uploader?'':'style="display:none"').'>
							<input type="file" name="StudentDocument" id="StudentDocument" value="" />'.$linterface->GET_SMALL_BTN($Lang['Btn']['Upload'], 'button', 'uploadStudentDocument();', 'UploadBtn', $ParOtherAttribute="", $OtherClass="", $ParTitle='').'
							<div class="tabletextremark">('.str_replace('<!--SIZE-->',$document_size_limit,$Lang['eHomework']['FileSizeLimitRemark']).')</div>
							<div id="StudentDocumentWarning" class="red warning" style="display:none">'.$Lang['General']['PleaseSelectFiles'].'</div>
							<iframe id="FileUploadFrame" name="FileUploadFrame" style="display:none;"></iframe>
						</div>';
			}
				$xx .= '<div id="StudentDocumentLink">';
				if($handin_record['StudentDocument']!='')
				{
					$file_name = substr($handin_record['StudentDocument'],strrpos($handin_record['StudentDocument'],'/')+1);
					$download_url = getEncryptedText($file_path.'/file/homework_document/'.$handin_record['StudentDocument']);
					$xx .= '<table><tr>';
					$xx .= '<td><a href="/home/download_attachment.php?target_e='.$download_url.'">'.intranet_htmlspecialchars($file_name).'</a></td>';
					// if not marked by teacher and not due yet, student can delete uploaded document and reupload again
					if($_SESSION['UserType'] == USERTYPE_STUDENT && trim($handin_record['TeacherDocument'])=='' && trim($handin_record['TextScore'])=='' && $today <= $due)
					{
						$xx .= '<td><span class="table_row_tool"><a href="javascript:void(0);" class="delete_dim" onclick="deleteStudentDocument('.$handin_record['RecordID'].');" title="'.$Lang['Btn']['Delete'].'"></a></span></td>';
					}
					$xx .= '</tr></table>';
					$xx .= '<div class="tabletextremark">('.str_replace('<!--DATETIME-->',$handin_record['StudentDocumentUploadTime'],$Lang['eHomework']['SubmittedTimeRemark']).')</div>';
				}else if($_SESSION['UserType']==USERTYPE_PARENT){
					$xx .= $Lang['eHomework']['NotSubmitted'];
				}
				$xx .= '</div>';
	if($_SESSION['UserType']==USERTYPE_PARENT){
		$xx .= '<input type="hidden" name="childrenID" value="'.$childrenID.'" />';
	}
	$xx .=			'
				</td>
			</tr>';
	
	$xx .= '<tr>
				<td class="tabletext formfieldtitle" width="30%" valign="top">'.$Lang['eHomework']['Mark'].'</td>
				<td align="left" valign="top">';
				$xx .= '<div>';
			if($handin_record['TeacherDocument']!=''){
				$file_name = substr($handin_record['TeacherDocument'],strrpos($handin_record['TeacherDocument'],'/')+1);
				$download_url = getEncryptedText($file_path.'/file/homework_document/'.$handin_record['TeacherDocument']);
				$xx .= '<a href="/home/download_attachment.php?target_e='.$download_url.'">'.$file_name.'</a>';
				$xx .= '<div class="tabletextremark">('.str_replace('<!--NAME-->',$handin_record['TeacherDocumentUploaderName'],str_replace('<!--DATETIME-->',$handin_record['TeacherDocumentUploadTime'],$Lang['eHomework']['MarkedRemark'])).')</div>';
			}else{
				$xx .= $Lang['eHomework']['NotMarked'];
			}
				$xx .= '</div>';		
	$xx .=		'</td>
			</tr>';
	$xx .= '<tr>
				<td class="tabletext formfieldtitle" width="30%" valign="top">'.$Lang['eHomework']['Rating'].'</td>
				<td>'.($handin_record['TextScore']==''? $Lang['eHomework']['NotRated'] : '<p>'.Get_String_Display(intranet_htmlspecialchars($handin_record['TextScore'])).'</p><div class="tabletextremark">('.str_replace('<!--NAME-->',$handin_record['TextScoreUpdaterName'],str_replace('<!--DATETIME-->',$handin_record['TextScoreUpdateTime'],$Lang['eHomework']['RatedRemark'])).')</div>' ).'</td>
			</tr>';
	
}

$linterface->LAYOUT_START();
?>
<?=$include_js?>
<script type="text/javascript" language="javascript">
function click_print()
{
	with(document.form1)
    {
        submit();
	}
}
<?php if($sys_custom['LivingHomeopathy'] && $_SESSION['UserType'] == USERTYPE_STUDENT){ ?>
function uploadStudentDocument()
{
	$('.warning').hide();
	if($('#StudentDocument').val() == ''){
		$('#StudentDocumentWarning').show();
		return;
	}
	$('#StudentDocumentUpload').hide();
	$('#UploadSpinner').show();
	
	var obj = document.form1;
	var old_action = obj.action;
	var old_target = obj.target;
	var old_encoding = obj.encoding;
	obj.action = "upload_document.php";
	obj.target = "FileUploadFrame";
	obj.encoding = "multipart/form-data";
	obj.method = 'post';
	obj.submit();
	obj.action = old_action;
	obj.target = old_target;
	obj.encoding = old_encoding;
}

function studentDocumentUploaded(success,returnHtml)
{
	$('#UploadSpinner').hide();
	$('#StudentDocumentLink').html(returnHtml);
	$('#StudentDocument').val('');
	if(success){
		$('#StudentDocumentUpload').hide();
		Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
	}else{
		$('#StudentDocumentUpload').show();
		Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>');
	}
	Scroll_To_Top();
	if(window.opener){
		window.opener.location.reload();
	}
}

function deleteStudentDocument(recordId)
{
	if(confirm('<?=$Lang['General']['__Warning']['AreYouSureYouWouldLikeToDeleteThisFile']?>'))
	{
		Block_Document();
		$.post(
			'delete_document.php',
			{
				"RecordID":recordId 
			},
			function(returnCode){
				UnBlock_Document();
				if(returnCode == '1'){ // success
					$('#StudentDocumentLink').html('');
					$('#StudentDocumentUpload').show();
					Get_Return_Message('<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>');
				}else{
					Get_Return_Message('<?=$Lang['General']['ReturnMessage']['DeleteUnsuccess']?>');
				}
				Scroll_To_Top();
				if(window.opener){
					window.opener.location.reload();
				}
			}
		);
	}
}
<?php } ?>
</script>
<br />
<form name="form1" action="print_preview.php" method="post" target = "_blank">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td colspan="2">
				<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
						<td>
							<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
								<!-- subject-->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Subject']?></td>
									<td width="567" align="left" valign="top">
										<?=$subjectName?>
									</td>
								</tr>
								
								<? if($lhomework->useHomeworkType) { ?>
								<!-- Homework Type-->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$i_Homework_HomeworkType?></td>
									<td width="567" align="left" valign="top">
										<?=$typeName?>
									</td>
								</tr>
								<? } ?>
								
								<!-- subject Group-->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['SubjectGroup']?></td>
									<td width="567" align="left" valign="top">
										<?=$subjectGroupName?>
									</td>
								</tr>
								
								<!-- topic -->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Topic']?></td>
									<td width="567" align="left" valign="top">
									  <?=$title?>
									</td>
								</tr>
								
								<!-- description -->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Description']?></td>
									<td width="567" align="left" valign="top">
										<?php
											if($description!="")
												echo $description;
											else
												echo "--";
										?>
									</td>
								</tr>
								
								<!-- attachment -->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Attachment']?></td>
									<td width="567" align="left" valign="top">
										<?php
											if($AttachmentPath!=NULL) 
												echo $displayAttachment; 
											else 
												echo $Lang['SysMgr']['Homework']['NoAttachment'];
										?>
									</td>
								</tr>
								<!-- end of attachment -->

								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Workload']?></td>
									<td width="567" align="left" valign="top">
										<?=($loading/2)." ".$Lang['SysMgr']['Homework']['Hours']?>
									</td>
								</tr>
								  
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['StartDate']?></td>
									<td width="567" align="left" valign="top">
										<?=$start?>
									</td>
								</tr>

								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']["DueDate"]?></td>
									<td width="567" align="left" valign="top">
										<?=$due?>
									</td>
								</tr>
								
								<tr>	                      	
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['HandinRequired']?></td>
									<td width="567" align="left" valign="top">
										<?=($handinRequired=="1"? $i_general_yes: $i_general_no)?>
									</td>						
								</tr>
								
								<? if($lhomework->useHomeworkCollect) {?>
								<tr>	                      	
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$i_Homework_Collected_By_Class_Teacher?></td>
									<td width="567" align="left" valign="top">
										<?=($collectRequired=="1"? $i_general_yes:$i_general_no)?>
									</td>						
								</tr>
								<? } ?>
								
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Poster']?></td>
									<td width="567" align="left" valign="top">
										<?=($PosterName2=="" ? "--":$PosterName2)?>
									</td>
								</tr>
								<?=$xx?>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr>
						<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
														
					<tr>
						<td align="center" colspan="2">
							<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "click_print()") ?>
							<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close()","submit3") ?>
						</td>
					</tr>
				</table>										
							
			</td>
		</tr>
	</table>
	<input type="hidden" name="hid" value="<?=$hid?>">
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>