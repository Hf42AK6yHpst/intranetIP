<?php
// Editing by 
/*
 * 2017-06-27 (Carlos): $sys_custom['LivingHomeopathy'] created.
 */
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if(!$sys_custom['LivingHomeopathy'] || $_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION['UserType']==USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"])))
{
	intranet_closedb();
	echo "0";
	//include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	//$laccessright = new libaccessright();
	//$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lhomework = new libhomework2007();
$lf = new libfilesystem();

if($_POST['RecordID']==''){
	intranet_closedb();
	echo "0";
	exit;
}

$RecordID = $_POST['RecordID'];
$handin_records = $lhomework->getHomeworkHandinListRecords(array('RecordID'=>$RecordID,'StudentID'=>$_SESSION['UserID']));

if(count($handin_records)==0)
{
	intranet_closedb();
	echo "0";
	exit;
}

$handin_record = $handin_records[0];
if($handin_record['StudentDocument'] == ''){
	intranet_closedb();
	echo "0";
	exit;
}

$full_file_path = $file_path.'/file/homework_document/'.$handin_record['StudentDocument'];
if(is_file($full_file_path)){
	$uniq_folder = substr($full_file_path,0,strrpos($full_file_path,'/'));
	$delete_file_success = $lf->file_remove($full_file_path);
	$lf->folder_remove($uniq_folder);
	$sql = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus='5',StudentDocument=NULL,StudentDocumentUploadTime=NOW() WHERE RecordID='".$RecordID."'";
	$db_success = $lhomework->db_db_query($sql);
	echo $delete_file_success && $db_success ? "1" : "0";
}else{
	echo "0";
}

intranet_closedb();
?>