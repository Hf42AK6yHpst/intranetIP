<?php
# Modifing by : 
		
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION['UserType']==USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();
$lhomework = new libhomework2007();

$CurrentPageArr['eServiceHomework'] = 1;
$CurrentPage = "Reports_WeekHomeworkList";
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

$linterface = new interface_html();

$PAGE_TITLE = $Lang['SysMgr']['Homework']['WeeklyHomeworkList'];

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['WeeklyHomeworkList'], "index.php", 0);
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['MonthlyHomeworkList'], "monthlyList.php", 1);

$today = date('Y-m-d');
# Current Year
$yearID = Get_Current_Academic_Year_ID();
# Current Year Term
$currentYearTerm = getAcademicYearAndYearTermByDate($today);
$yearTermID = $currentYearTerm[0];
if($yearTermID=="")
	$yearTermID = 0;
	
# Set TimeStamp
/*
if ($ts == ""){
	$ts = time();
}

$ts = mktime(0,0,0,date('m',$ts),date('d',$ts)-date('w',$ts),date('Y',$ts));
*/
if ($ts == "")
{
    $month = date('m');
    $year = date('Y');
}
else
{
    $month = date('m',$ts);
    $year = date('Y',$ts);
}
$ts = mktime(0,0,0,$month,1,$year);
$month_display = "$month/$year";

$current = time();
$current = mktime(0,0,0,date('m',$current),date('d',$current)-date('w',$current),date('Y',$current));

$prev = mktime(0,0,0,$month-1,1,$year);
$next = mktime(0,0,0,$month+1,1,$year);

$subjectID = ($subjectID=="")? -1 :$subjectID;

if($subjectGroupID && $subjectID==-1)
{
	$TempSubjectInfo = $lhomework->RetrieveSubjectbySubjectGroupID($subjectGroupID);
	$subjectID = $TempSubjectInfo[0]['SubjectID'];
}


//$y = $lh->displayCalendarClass($ts);

# Student
if($_SESSION['UserType']==USERTYPE_STUDENT){

	$subject = $lhomework->getStudyingSubjectList($UserID, "", $yearID, $yearTermID);
	$subjectID = ($subjectID=="")? $subject[0][0]:$subjectID;
	
	$display = (sizeof($subject)==0)? false:true;

	if($display){
		//$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm($ts)\"", $subject, $subjectID, "", false);		
		 $selectedSubject = $lhomework->getCurrentStudyingSubjects("name=\"subjectID\" onChange=\"reloadForm($ts)\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);
		
			if(sizeof($subject)!=0){
				$allSubjects = " a.SubjectID IN (";
				for ($i=0; $i < sizeof($subject); $i++)
				{	
					list($ID)=$subject[$i];
					$allSubjects .= $ID.",";
				}
				$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
				}
			else{
				$allSubjects ="";
			}
			

		$cond = "";
		
		/*if($subjectID=="")
			$cond .= " AND $allSubjects";
			 
		else*/ 
		//debug_pr($_POST);
		if($subjectID!="" && $subjectID!=-1)
			$cond = "a.SubjectID = $subjectID AND ";
			
		$cond .= " a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID AND ";
		
		$cond .= " c.UserID='$UserID' ";
		
		// [DM#2934] Cannot view future homework in reports
		$cond .= " AND a.StartDate<=CURDATE() ";
		
		//echo $cond;
		$y = $lhomework->monthlyHomeworkList($ts, "", $cond,"","","");
		$filterbar = "$selectClass $selectedSubject $selectedSubjectGroup";
	}
}

# Parent
else if($_SESSION['UserType']==USERTYPE_PARENT){

	$children = $lhomework->getChildrenList($UserID);
	$childrenID = ($childrenID=="")? $children[0][0]:$childrenID;
	
	$selectedChildren = $lhomework->getCurrentChildren("name=\"childrenID\" onChange=\"document.getElementById('subjectID').options.selectedIndex=0; reloadForm($ts)\"", $children, $childrenID, "", false);

	$subject = $lhomework->getStudyingSubjectList($childrenID, "", $yearID, $yearTermID);
	//$subjectID = ($subjectID=="")? $subject[0][0]:$subjectID;

	$display = (sizeof($subject)==0)? false:true;
	
	$selectedSubject = $lhomework->getCurrentStudyingSubjects("name=\"subjectID\" id=\"subjectID\" onChange=\"reloadForm($ts)\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);
		
	if($display){

		if(sizeof($subject)!=0){
			$allSubjects = " a.SubjectID IN (";
			for ($i=0; $i < sizeof($subject); $i++)
			{	
				list($ID)=$subject[$i];
				$allSubjects .= $ID.",";
			}
			$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
			}
		else{
			$allSubjects ="";
		}
			
		$cond = "";
		
		if(isset($childrenID) && $childrenID!="") 
			$cond = " c.UserID='$childrenID' AND ";
		else if(sizeof($childAry)>0)
			$cond .= " c.UserID IN (".implode(',', $childAry).") AND ";
		
		if($subjectID!="" && $subjectID!=-1)
			$cond .= " a.SubjectID = $subjectID AND ";
			
		$cond .= " a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID";
		
		// [DM#2934] Cannot view future homework in reports
		$cond .= " AND a.StartDate<=CURDATE() ";

		//}
		$y = $lhomework->monthlyHomeworkList($ts, "", $cond,"","","");
	}
	
	$filterbar = "$selectedChildren $selectClass $selectedSubject $selectedSubjectGroup";	
}


# Print link
$printPreviewLink = "monthly_print_preview.php?childrenID=$childrenID&yearID=$yearID&yearTermID=$yearTermID&classID=$classID&subjectID=$subjectID&subjectGroupID=$subjectGroupID&ts=$ts&year=$year&month=$month";
# Export link
$exportLink = "monthly_export.php?childrenID=$childrenID&yearID=$yearID&yearTermID=$yearTermID&classID=$classID&subjectID=$subjectID&subjectGroupID=$subjectGroupID&ts=$ts&year=$year&month=$month";

# Toolbar: print, export
$toolbar = $linterface->GET_LNK_PRINT("javascript:newWindow('$printPreviewLink', 23)", "", "", "", "", 0 );

$allowExport = $lhomework->exportAllowed;
if($allowExport)
	$toolbar .= $linterface->GET_LNK_EXPORT($exportLink, "", "", "", "", 0 );

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
	function click_previous(prev){
		url = "monthlyList.php?ts=" + prev
		document.form1.action = url;
		document.form1.target = "_self";
		document.form1.submit();
	}
	
	function click_next(next){
		url = "monthlyList.php?ts=" + next
		document.form1.action = url;
		document.form1.target = "_self";
		document.form1.submit();
	}
	
	function click_current(current){
		url = "monthlyList.php?ts=" + current
		document.form1.action = url;
		document.form1.target = "_self";
		document.form1.submit();
	}
	
	function reloadForm(ts) {
		url = "monthlyList.php?ts=" + ts
		document.form1.action = url;
		document.form1.target = "_self";
		document.form1.submit();
	}
	
	function viewHmeworkDetail(id){
		newWindow('../../management/homeworklist/view.php?hid='+id,1);
	}
	
	function viewHandinList(id)
	{
		newWindow('../../management/homeworklist/handin_list.php?hid='+id,10);
	}

</script>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post" onSubmit="return false">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="2" width="300">
								<tr>
									<td>
										<?if($display){
											echo $toolbar;
										}?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</form>
			<form name="form1" method="post">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr>
						<td align="right">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td height="28" align="right" valign="bottom">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td height="30">
													<?=$filterbar?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="2" cellspacing="0">
					<tr>
						<td align="right">
							<!--
							<table width="90%"><tr>
								<th align="center">
								<?
									if($month>=10)
										echo $eEnrollment['month'][$month]."/".$year;
									else 
										echo $eEnrollment['month'][substr($month,-1)]."/".$year;
								?>
								</th>
							</tr></table>
							-->
								<?
									if($month>=10)
										$curMonth = $eEnrollment['month'][$month]." ".$year;
									else 
										$curMonth = $eEnrollment['month'][substr($month,-1)]." ".$year;
								
								if($display){		
								?>
									<a href="javascript:click_previous(<?=$prev?>)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icalendar/icon_prev_off.gif" width="21" height="21" border="0" align="absmiddle" alt="<?=$Lang['Calendar']['PrevMonth']?>"/></a>
									<?=$curMonth?>
									<a href="javascript:click_next(<?=$next?>)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icalendar/icon_next_off.gif" width="21" height="21" border="0" align="absmiddle" alt="<?=$Lang['Calendar']['NextMonth']?>"/></a>
								<? } ?>
						</td>
					</tr>
					<tr>
						<td>
							<?
							if($display){
								echo $y;
							}
							
							?>
						</td>
					</tr>
				</table>

				<br />
				<input type="hidden" name="yearID" value="<?=$yearID?>"/>
				<input type="hidden" name="yearTermID" value="<?=$yearTermID?>"/>
				<input type="hidden" name="sid" value="<?=$subjectID?>"/>
			</form>
		</td>
	</tr>
</table>
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>