<?php

// Modifing by
		
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION['UserType']==USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();
$lhomework = new libhomework2007();

$CurrentPageArr['eServiceHomework'] = 1;

$CurrentPage = "Reports_WeekHomeworkList";
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

$linterface = new interface_html();

$PAGE_TITLE = $Lang['SysMgr']['Homework']['WeeklyHomeworkList'];

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['WeeklyHomeworkList'], "index.php", 1);
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['MonthlyHomeworkList'], "monthlyList.php", 0);


# Set TimeStamp
if ($ts == ""){
	$ts = time();
}

$ts = mktime(0,0,0,date('m',$ts),date('d',$ts)-date('w',$ts),date('Y',$ts));

$current = time();
$current = mktime(0,0,0,date('m',$current),date('d',$current)-date('w',$current),date('Y',$current));

$prev = $ts-604800;
$next = $ts+604800;

$today = date('Y-m-d');
# Current Year
$yearID = Get_Current_Academic_Year_ID();
# Current Year Term
$currentYearTerm = getAcademicYearAndYearTermByDate($today);
$yearTermID = $currentYearTerm[0];
if($yearTermID=="")
	$yearTermID = 0;
	
# Parent Mode
if($_SESSION['UserType']==USERTYPE_PARENT){

	# Children Menu
	$children = $lhomework->getChildrenList($UserID);
	$childrenID = ($childrenID=="")? $children[0][0]:$childrenID;
	
	$selectedChildren = $lhomework->getCurrentChildren("name=\"childrenID\" onChange=\"reloadForm($ts)\"", $children, $childrenID, "", false);
	if($childrenID!=""){
		# Subject Menu
		$subject = $lhomework->getStudyingSubjectList($childrenID, "", $yearID, $yearTermID);
		$display = (sizeof($subject)==0)? false:true;
		
		if($display){
			if($cid != $childrenID){
				$subjectID ="";
			}

			$selectedSubject = $lhomework->getCurrentStudyingSubjects("name=\"subjectID\" onChange=\"reloadForm($ts)\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);
			
			if(sizeof($subject)!=0){
				$allSubjects = " a.SubjectID IN (";
				for ($i=0; $i < sizeof($subject); $i++)
				{	
					list($ID)=$subject[$i];
					$allSubjects .= $ID.",";
				}
				$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
				}
			else{
				$allSubjects ="";
			}
			
			$cond = ($subjectID=='')? "$allSubjects AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : " a.SubjectID = $subjectID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID";
			
			$y = $lhomework->weeklyHomeworkList($ts, $childrenID, $cond);
			
			$filterbar = "$selectedChildren $selectedSubject";
		}
	}
}

# Student Mode	
else{
	
	$subject = $lhomework->getStudyingSubjectList($UserID, "", $yearID, $yearTermID);
	$display = (sizeof($subject)==0)? false:true;
			
	if($display){
		$selectedSubject = $lhomework->getCurrentStudyingSubjects("name=\"subjectID\" onChange=\"reloadForm($ts)\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);
		
		if(sizeof($subject)!=0){
			$allSubjects = " a.SubjectID IN (";
			for ($i=0; $i < sizeof($subject); $i++)
			{	
				list($ID)=$subject[$i];
				$allSubjects .= $ID.",";
			}
			$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
		}
		else{
			$allSubjects ="";
		}
		
		$cond = ($subjectID=='')? "$allSubjects AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : " a.SubjectID = $subjectID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID";
			
		$y = $lhomework->weeklyHomeworkList($ts, "", $cond);
		
		$filterbar = "$selectedSubject";
	}
}

# Print link
$printPreviewLink = "print_preview.php?yearID=$yearID&yearTermID=$yearTermID&childrenID=$childrenID&subjectID=$subjectID&ts=$ts";

# Export link
$exportLink = "export.php?yearID=$yearID&yearTermID=$yearTermID&childrenID=$childrenID&subjectID=$subjectID&ts=$ts";

# Toolbar: print
$toolbar = $linterface->GET_LNK_PRINT("javascript:newWindow('$printPreviewLink', 23)", "", "", "", "", 0 );

$allowExport = $lhomework->exportAllowed;
if($allowExport)
	$toolbar .= $linterface->GET_LNK_EXPORT($exportLink, "", "", "", "", 0 );
# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
	function click_previous(prev){
		url = "index.php?ts=" + prev
		document.form1.action = url;
		document.form1.target = "_self";
		document.form1.submit();
	}
	
	function click_next(next){
		url = "index.php?ts=" + next
		document.form1.action = url;
		document.form1.target = "_self";
		document.form1.submit();
	}
	
	function click_current(current){
		url = "index.php?ts=" + current
		document.form1.action = url;
		document.form1.target = "_self";
		document.form1.submit();
	}
	
	function reloadForm(ts) {
		url = "index.php?ts=" + ts
		document.form1.action = url;
		document.form1.target = "_self";
		document.form1.submit();
	}
	
	function viewHmeworkDetail(id){
		newWindow('../../management/homeworklist/view.php?hid='+id,1);
	}
	
	function viewHandinList(id)
	{
		newWindow('../../management/homeworklist/handin_list.php?hid='+id,10);
	}

</script>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post" onSubmit="return false">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="2" width="300">
								<tr>
									<td>
										<?if($display){
											echo $toolbar;
										}?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</form>
			<form name="form1" method="post">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr>
						<td align="right">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td height="28" align="right" valign="bottom">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td height="30">
													<?=$filterbar?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="right">
						<? if($display){ ?>
							<a href="javascript:click_previous(<?=$prev?>)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icalendar/icon_prev_off.gif" width="21" height="21" border="0" align="absmiddle" alt="<?=$Lang['SysMgr']['Homework']['PreviousWeek']?>"/></a>
							<a href="javascript:click_next(<?=$next?>)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icalendar/icon_next_off.gif" width="21" height="21" border="0" align="absmiddle" alt="<?=$Lang['SysMgr']['Homework']['NextWeek']?>"/></a>
						<? } ?>
						</td>
					</tr>
				</table>
	
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td>
							<?
							if($display){
								echo $y;
							}
							else{
								if($_SESSION['UserType']==USERTYPE_PARENT)
									echo $Lang['SysMgr']['Homework']['WeeklyHomeworkListWarningParent'];
								else
									echo $Lang['SysMgr']['Homework']['WeeklyHomeworkListWarningStudent'];
							}
							?>
						</td>
					</tr>
				</table>
	<!--
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					
					<?if($display){
							echo "<tr><td class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>
								<tr><td align=\"center\">".
									$linterface->GET_ACTION_BTN($Lang['SysMgr']['Homework']['PreviousWeek'], "button", "javascript:click_previous($prev)")." ".
									$linterface->GET_ACTION_BTN($Lang['SysMgr']['Homework']['CurrentWeek'], "button", "javascript:click_current($current)")." ".
									$linterface->GET_ACTION_BTN($Lang['SysMgr']['Homework']['NextWeek'], "button", "javascript:click_next($next)")."
								 </td></tr>";
					}?>
				</table>
	-->
				<input type="hidden" name="cid" value="<?=$childrenID?>"/>
				<input type="hidden" name="sid" value="<?=$subjectID?>"/>
			</form>
		</td>
	</tr>
</table>
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>