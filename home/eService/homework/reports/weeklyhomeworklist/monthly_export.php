<?php
// Modifing by : 
$PATH_WRT_ROOT = "../../../../../";
$CurrentPageArr['eServiceHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION['UserType']==USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lhomework = new libhomework2007();
$lexport = new libexporttext();

$filename = "MonthlyHomeworkList.csv";


# Student
if($_SESSION['UserType']==USERTYPE_STUDENT){

	$subject = $lhomework->getStudyingSubjectList($UserID, "", $yearID, $yearTermID);
	$subjectID = ($subjectID=="")? $subject[0][0]:$subjectID;
	
	$display = (sizeof($subject)==0)? false:true;

	if($display){
		//$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm($ts)\"", $subject, $subjectID, "", false);		
		
			if(sizeof($subject)!=0){
				$allSubjects = " a.SubjectID IN (";
				for ($i=0; $i < sizeof($subject); $i++)
				{	
					list($ID)=$subject[$i];
					$allSubjects .= $ID.",";
				}
				$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
				}
			else{
				$allSubjects ="";
			}
			

		$cond = "";
		
		/*if($subjectID=="")
			$cond .= " AND $allSubjects";
			 
		else*/ 
		//debug_pr($_POST);
		if($subjectID!="" && $subjectID!=-1)
			$cond = "a.SubjectID = $subjectID AND ";
			
		$cond .= " a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID AND ";
		
		$cond .= " c.UserID='$UserID' ";
		
		// [DM#2934] Cannot export future homework
		$cond .= " AND a.StartDate<=CURDATE() ";
		
		//echo $cond;
		$data = $lhomework->exportMonthlyHomeworkList($ts, "", $cond,"","");
		//debug_pr($data);
	}
}

# Parent
else if($_SESSION['UserType']==USERTYPE_PARENT){

	$children = $lhomework->getChildrenList($UserID);
	$childrenID = ($childrenID=="")? $children[0][0]:$childrenID;
	

	$subject = $lhomework->getStudyingSubjectList($childrenID, "", $yearID, $yearTermID);
	//$subjectID = ($subjectID=="")? $subject[0][0]:$subjectID;

	$display = (sizeof($subject)==0)? false:true;
	
		
	if($display){

		if(sizeof($subject)!=0){
			$allSubjects = " a.SubjectID IN (";
			for ($i=0; $i < sizeof($subject); $i++)
			{	
				list($ID)=$subject[$i];
				$allSubjects .= $ID.",";
			}
			$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
			}
		else{
			$allSubjects ="";
		}
			
		$cond = "";
		
		if(isset($childrenID) && $childrenID!="") 
			$cond = " c.UserID='$childrenID' AND ";
		else if(sizeof($childAry)>0)
			$cond .= " c.UserID IN (".implode(',', $childAry).") AND ";
		
		if($subjectID!="" && $subjectID!=-1)
			$cond .= " a.SubjectID = $subjectID AND ";
			
		$cond .= " a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID";
		
		// [DM#2934] Cannot export future homework
		$cond .= " AND a.StartDate<=CURDATE() ";

		//}
		$data = $lhomework->exportMonthlyHomeworkList($ts, "", $cond,"","");

	}
	//debug_pr($data);
	
}

# Set $ts to sunday of the week
$ts = mktime(0,0,0,date('m',$ts),date('d',$ts)-date('w',$ts),date('Y',$ts));
$today = date('Y-m-d');

# Get Cycle days
$start = mktime(0,0,0,$month,1,$year);
$end = mktime(0,0,0,$month+1,1,$year);
$lcal = new libcalevent();
$row = $lcal->returnCalendarArray($month,$year);
$lcycle = new libcycleperiods();
$cycle_days = $lcycle->getCycleInfoByDateRange(date('Y-m-d',$start),date('Y-m-d',$end));


for ($i=0; $i<sizeof($row); $i++)
{
  $week_ts[] = $ts+$i*86400;
}


$exportColumn = array();
		
for ($i=0; $i<7; $i++)
{
	$day = $Lang['SysMgr']['Homework']['WeekDay'][$i];
	array_push($exportColumn, $day);
}

if(sizeof($data)!=0)
{
	$m = 0;
	$new_m = $m;
	$HW_Flag = 0;
	$maxHw = 0;
	$lang = $intranet_session_language=="b5" ? 1 : 0;
	
	for($i=0; $i<sizeof($row); $i++){		# loop for each day
		
		$current = date('Y-m-d',$week_ts[$i]);  #date('Y-m-d',$ts+$i*86400);
		
		$pos = $i%7;
		$HW_Flag = 0;
		
		if($row[$i] !='') {				# with the date
			$recordDate = date('Y-m-d',$row[$i]);
			$cycle = $cycle_days[$recordDate][$lang] ? " (".$cycle_days[$recordDate][$lang].")" : "";
			
			$day = date('j',$row[$i]);
			$ExportArr[$m][$pos] = $day.$cycle;
			
			$new_m = $m+1;
				
			for($j=0; $j<sizeof($data); $j++){
				
				list($hid,$subject,$subjectgroup,$startdate,$duedate,$loading,$title,$description,$subjectGroupID,$subjectID,$AttachmentPath, $TypeID) = $data[$j];
				
				$displayDate = ($lhomework->useStartDateToGenerateList) ? $startdate : $duedate;
				
				if ($current == $displayDate){
					
					if ($loading==0)
					{
						$tload = 0;
					}
					else if ($loading <= 16)
					{
						$tload = $loading/2;
					}
					else
					{
						$tload = "$i_Homework_morethan 8";
					}
					$useHwType = "";
					if($lhomework->useHomeworkType) {
						$homeworkTypeInfo = $lhomework->getHomeworkType($TypeID);
						$typeName = ($homeworkTypeInfo[0]['TypeName']) ? $homeworkTypeInfo[0]['TypeName'] : "---";
						$useHwType = $typeName;					
					}
	
					$text1 = $Lang['SysMgr']['Homework']['Topic'].": ".$title;
					$text2 = $Lang['SysMgr']['Homework']['Subject'].": ".$subject;
					$text3 = $Lang['SysMgr']['Homework']['SubjectGroup'].": ".$subjectgroup;
					$text4 = $Lang['SysMgr']['Homework']['Workload'].": $tload ".$Lang['SysMgr']['Homework']['Hours'];
					$text5 = ($lhomework->useStartDateToGenerateList) ? ($Lang['SysMgr']['Homework']['DueDate'].": ".$duedate) : ($Lang['SysMgr']['Homework']['StartDate'].": ".$startdate);
					$text6 = $i_Homework_HomeworkType.": ".$useHwType;
					
					//echo $text1.'/'.$text2.'/'.$text3.'/'.$text4.'/'.$text5.'/'.$new_m.'<br>';
					$ExportArr[$new_m][$pos] = $text1;
					$new_m++;
					$ExportArr[$new_m][$pos] = $text2;
					$new_m++;
					$ExportArr[$new_m][$pos] = $text3;
					$new_m++;
					if($lhomework->useHomeworkType) {
						$ExportArr[$new_m][$pos] = $text6;
						$new_m++;
					}
					$ExportArr[$new_m][$pos] = $text4;
					$new_m++;
					$ExportArr[$new_m][$pos] = $text5;
					$new_m++;
					$ExportArr[$new_m][$pos] = "";
					$new_m++;

					$HW_Flag++;
				} 
				
			}
			$maxHw = ($HW_Flag>$maxHw) ? $HW_Flag : $maxHw;
			if($pos==6) {						# move to a new row per 7 days
				if($maxHw!=0) 
					$m = $new_m + ($maxHw*6);
				else 
					$m = $new_m;
					
				$ExportArr[$m][] = "\n";	
				$HW_Flag = 0;
				$maxHw = 0;
				$m++;
			}
		}
	}
}

else{
	$ExportArr[0][0] = $i_no_record_exists_msg;
}

$export_content = $lexport->GET_EXPORT_TXT2($ExportArr, $exportColumn, "", "\r\n", "", 0, "00");

# display the month & year on the top
if($month>=10)
	$export_content = $eEnrollment['month'][$month]."/".$year."\n".$export_content;
else 
	$export_content = $eEnrollment['month'][substr($month,-1)]."/".$year."\n".$export_content;


intranet_closedb();

# Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
