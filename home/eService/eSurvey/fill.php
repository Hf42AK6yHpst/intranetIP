<?php

#################################
#
#	Date:   2019-05-14 (Bill)
#           prevent SQL Injection
#
#   Date:   2019-03-19  Isaac
#           Added flag $sys_custom['eSurvey']['studentHideCreaterAndTargetGroup'] to hide creator and target groups
#
#	Date:	2015-03-24 (Jason)
#			- add js order_lang to support Ordering Type
#
#	Date:	2012-05-02	YatWoon
#			- add access right checking
#
#	Date:	2011-06-28	YatWoon
#			- add checking NotAllowReSign to determine show the remark or not
#
#	Date:	2011-06-02	YatWoon
#			- add option allow re-sign the survey or not ($lsurvey->NotAllowReSign)
#			- display original answer
#
#	Date:	2011-02-23	YatWoon
#			Add "Display question number"
#
#################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$SurveyID = IntegerSafe($SurveyID);
$row = IntegerSafe($row);
### Handle SQL Injection + XSS [END]

$lform = new libform();
$lsurvey = new libsurvey($SurveyID);

if($sys_custom['eSurvey']['studentHideCreaterAndTargetGroup'])
{
    include_once($PATH_WRT_ROOT."includes/libuser.php");
    $lu = new libuser($_SESSION["UserID"]);
    $checkIsStudentView = $lu->isStudent();
    if($checkIsStudentView)$hideCreatorAndTargetGroup = true;
}

# check access right
$this_target_group = $lsurvey->returnTargetGroupsArray();
if(sizeof($this_target_group) > 0)	# with target group
{
	include_once($PATH_WRT_ROOT."includes/libgroup.php");
	$lg = new libgroup();
	$inGroup = 0;
	# check is group member or not
	foreach($this_target_group as $k=>$d)
	{
		list($this_group_id, $this_group_title) = $d;
		$inGroup = $inGroup || $lg->isGroupMember($UserID, $this_group_id);
	}
	
	if(!$inGroup)
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}
$this_target_group = $lsurvey->returnTargetSubjectGroupsArray();
if(sizeof($this_target_group) > 0)	# with target subject group
{
	foreach($this_target_group as $k=>$d)
	{
		list($this_group_id, $this_group_title) = $d;
		$sql = "SELECT SubjectGroupID FROM SUBJECT_TERM_CLASS_USER WHERE UserID = '$UserID' AND SubjectGroupID = '$this_group_id'";
		$isGroupMember = $lsurvey->returnVector($sql);
		$inGroup = $inGroup || $isGroupMember;
	}
	
	if(!$inGroup)
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}
$disallowSaveAsDraft = true;
$userAnswer = $lsurvey->returnUserAnswer($UserID);
if ($sys_custom['SurveySaveWithDraft']) {
	$disallowSaveAsDraft = false;
	if (!empty($userAnswer[0]["Answer"]) && $userAnswer[0]["isDraft"] != "1" || $lsurvey->AllowSaveDraft != "1") {
		$disallowSaveAsDraft = true;
	}
}

if(!empty($userAnswer) && $lsurvey->NotAllowReSign)
{
	if ($userAnswer[0]["isDraft"] != "1" || !$sys_custom['SurveySaveWithDraft']) {
		header("Location: fill_update.php?SurveyID=$SurveyID");
		exit;
	}
}

if($userAnswer)
{
	$ansstr = $userAnswer[0]['Answer'];
	$temp_ans = str_replace("&amp;", "&", $ansstr);
	$ansString = $lform->getConvertedString($temp_ans);
	$ansString = trim($ansString)." ";	
}
$MODULE_OBJ['title'] = $i_Survey_FillSurvey;

$home_header_no_EmulateIE7 = true;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$queString = $lsurvey->Question;
$queString = str_replace('"','&quot;',$queString);
$queString = str_replace("\n",'<br>',$queString);
$queString = str_replace("\r",'',$queString);

$poster = $lsurvey->returnPosterName();
$ownerGroup = $lsurvey->returnOwnerGroup();
$targetGroups = $lsurvey->returnTargetGroups();
$targetSubjectGroups = $lsurvey->returnTargetSubjectGroups();
$reqFillAllFields = $lsurvey->reqFillAllFields();

if (sizeof($targetGroups)==0 && sizeof($targetSubjectGroups)==0)
{
    $target = $Lang['eSurvey']['WholeSchool'];
}
else
{
    $target = implode(", ",array_merge($targetGroups,$targetSubjectGroups));
}
?>
<br />

<table border=0 width=95% cellspacing=0 cellpadding=5 align=center>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_startdate?></td>
	<td><?=$lsurvey->DateStart?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_enddate?></td>
	<td><?=$lsurvey->DateEnd?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_title?></td>
	<td><?=$lsurvey->Title?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_description?></td>
	<td><?=nl2br(intranet_convertAllLinks($lsurvey->Description))?></td>
</tr>
<?php if(!$hideCreatorAndTargetGroup){?>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_Survey_Poster?></td>
	<td><?=$poster?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_TargetGroup?></td>
	<td><?=$target?></td>
</tr>
<?php }?>

<? if ($lsurvey->RecordType == 1) { ?>
<tr>
	<td>&nbsp;</td>
	<td>[<?=$i_Survey_Anonymous?>]</td>
</tr>
<? } ?>

<? if ($reqFillAllFields) { ?>
<tr>
	<td>&nbsp;</td>
	<td>[<?=$i_Survey_AllRequire2Fill?>]</td>
</tr>
<? } ?>


<tr>
	<td colspan=2>
		<form name="ansForm" method="post" action="update.php">
		<input type=hidden name=isDraft value="0">
		<input type=hidden name="qStr" value="">
		<input type=hidden name="aStr" value="">
		</form>
		<? if($lsurvey->NotAllowReSign) {?>
		<?=$i_Survey_PleaseFill?>
		<? } ?><br/><?=$Lang['eSurvey']['Hint'] ?>
		<hr>
		
		<script LANGUAGE=Javascript>
		// Set true if need to fill in all fields before submit
		var need2checkform = <? if($reqFillAllFields){ echo "true"; } else { echo "false"; }  ?>;
		</script>
		<script language="javascript" src="/templates/forms/layer.js"></script>
		<script language="javascript" src="/templates/forms/form_edit.js"></script>
		
		<script language="Javascript">
		<?=$lform->getWordsInJS()?>
	
		var replyslip = '<?=$Lang['eSurvey']['ReplySlip']?>';
		var order_lang = '<?=$Lang['eSurvey']['Order']?>';
		var choice_lang = '<?=$Lang['Polling']['Choice']?>';
		var hint_lang = '<?=$Lang['Polling']['Hint']?>';
		var no_answer_lang = '<?=$Lang['eSurvey']['NotAnswered']?>';
		
		var DisplayQuestionNumber = '<?=$lsurvey->DisplayQuestionNumber?>';

		//background_image = "/images/layer_bg.gif";
		
		var sheet= new Answersheet();
		// attention: MUST replace '"' to '&quot;'
		sheet.qString="<?=$queString?>";
		sheet.aString="<?=$ansString?>";
		//edit submitted application
		sheet.mode=1;
		sheet.answer=sheet.sheetArr();
		//sheet.templates=form_templates;
		document.write(editPanel());
		</script>
		<SCRIPT LANGUAGE=javascript>
		function copyback()
		{
			need2checkform = <? if($reqFillAllFields){ echo "true"; } else { echo "false"; }  ?>;
		 	finish();
		 	document.form1.qStr.value = document.ansForm.qStr.value;
		 	document.form1.aStr.value = document.ansForm.aStr.value;
		}

		function saveAsDraft()
		{
			need2checkform = false;
			document.ansForm.isDraft.value = "1";
			finish();
			document.form1.isDraft.value = document.ansForm.isDraft.value;
		 	document.form1.qStr.value = document.ansForm.qStr.value;
		 	document.form1.aStr.value = document.ansForm.aStr.value;
		}
		function validSubmit(){
			if (formAllFilled) //!need2checkform ||
			{
				return true;
			} else if(!need2checkform && !orderingAllFilled){ 
				alert("<?=$Lang['eSurvey']['fillallorderingquestion']?>");
				return false;
		 	} else {
	 		 //return true;
		      alert("<?=$i_Survey_alert_PleaseFillAllAnswer?>!");
		      return false;
		 	}
		}
		$(document).ready(function() {
			$('form input[type="text"]').bind('keyup keypress', function(e) {
				var keyCode = e.keyCode || e.which;
				if (keyCode === 13) {
					e.preventDefault();
					return false;
				}
			});
		});
		</SCRIPT>
		<hr>
	</td>
</tr>
<tr>
	<form name=form1 action="fill_update.php?SurveyID=<?php echo $SurveyID; ?>" method=post onsubmit="return validSubmit()">
	
	<td colspan="2" align="center">
		<?=$linterface->GET_ACTION_BTN($button_submit, "submit", "copyback();"); ?>&nbsp;<?php
if ($sys_custom['SurveySaveWithDraft'] && !$disallowSaveAsDraft ) {
	echo $linterface->GET_ACTION_BTN($Lang["eSurvey"]["SaveAsDraft"], "submit", "saveAsDraft();");
}
?>
	</td>
	<input type=hidden name=isDraft value="0">
	<input type=hidden name=qStr value="">
	<input type=hidden name=aStr value="">
	<input type=hidden name=SurveyID value="<?=$SurveyID?>">
	<input type=hidden name=row value="<?=$row?>">
	</form>
</tr>
</table>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>