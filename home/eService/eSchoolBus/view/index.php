<?php
// Editing by 
$linterface = $indexVar['linterface'];
// $returnMsg = $indexVar['libSchoolBus']->getTempSession('ManagementAttendance');
$linterface->LAYOUT_START($returnMsg);

//Check identity
include_once($PATH_WRT_ROOT."includes/libuser.php");
$user = new libuser($_SESSION['UserID']);
$isTeacherStaff = $user->isTeacherStaff();
$isParent = $user->isParent();
$isStudent = $user->isStudent();
//Get Student Name Selection
/**
 * Logic: 
 * - if Teacher -> get All students????
 * - if Parent -> get his/her children
 * - if Student -> ONLY his/herself
 */
if($isTeacherStaff){
	$htmlAry['studentSelector'] = $linterface->getClassSelection(Get_Current_Academic_Year_ID(), ' id="YearClassID" name="YearClassID" onchange="getStudentSelection();" ', $YearClassID, $all=0, $noFirst=1, $FirstTitle="");
}else if($isParent){
	$htmlAry['childrenSelector'] =  $user->Get_Children_Selection($Id='StudentID', $Name='StudentID', $SelectedChildId='', $ParentId='', $OnChange='');
}else if($isStudent){
	$htmlAry['self'] = Get_Lang_Selection($user->ChineseName,$user->EnglishName);
	$htmlAry['self'] .= $linterface->GET_HIDDEN_INPUT($ID='StudentID', $Name='StudentID', $Value=$user->UserID);
}

?>
<script>
var loading_img = '<?=$indexVar['linterface']->Get_Ajax_Loading_Image()?>';
function getStudentSelection()
{
	$('#StudentSelectionContainer').html(loading_img);
	$.post(
		'index.php?task=view/ajax',
		{
			'ajax_task': 'getStudentSelection',
			'YearClassID':$('#YearClassID').val()
		},function(returnHtml){
			$('#StudentSelectionContainer').html(returnHtml);
		}
	);
}
var canSubmit = true;
function goSearch(){
	canSubmit = true;
	var studentId = $('#StudentID').val();
	if(!studentId){
		canSubmit = false;
	}
	var year = $('#year').val();
	var month = $('#month').val();
	if(canSubmit){
		var formArea = $('#ajax_form');
		formArea.html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
		$.ajax({
			url: "index.php?task=view/ajax/form",
			type: "POST",
			data: {
				action: 'student_view',
				studentId: studentId,
				year:year,
				month:month
			},
			dataType: "html",
			success: function(response){
			  formArea.html(response);
			}
		});
	}
}

</script>
<div id="select_panel">
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center" class="form_table_v30">
		<?php 
		if($isTeacherStaff){
		?>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title" width="30%">
				<?= $Lang['eSchoolBus']['eService']['StudentView']['Class'] ?>
			</td>
			<td>
				<?= $htmlAry['studentSelector'] ?>
			</td>
		</tr>
		<?php 
		}
		?>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title" width="30%"><span class="tabletextrequire">*</span><?=$Lang['Identity']['Student']?></td>
			<td>
				<span id="StudentSelectionContainer">
				<?php 
				if($isParent){
					echo $htmlAry['childrenSelector'];
				}else if ($isStudent){
					echo $htmlAry['self'];
				}
				?>
			
				</span>
				<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("StudentID_Error",$Lang['eSchoolBus']['Settings']['Warning']['RequestInputData'], "WarnMsgDiv")?>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title" width="30%">
				<?= $Lang['eSchoolBus']['eService']['StudentView']['Date'] ?>
			</td>
			<td>
				<?=$Lang['eSchoolBus']['eService']['StudentView']['Year'] ?>:
				<?=$indexVar['linterface']->Get_Number_Selection($ID_Name='year', $MinValue=2000, $MaxValue=2050, $SelectedValue=date('Y'), $Onchange='', $noFirst=1, $isAll=0, $FirstTitle='', $Disabled=0) ?>
				<?= $Lang['eSchoolBus']['eService']['StudentView']['Month'] ?>:
				<?=$indexVar['linterface']->Get_Number_Selection($ID_Name='month', $MinValue=1, $MaxValue=12, $SelectedValue=date('m'), $Onchange='', $noFirst=1, $isAll=0, $FirstTitle='', $Disabled=0) ?>
			</td>
		</tr>
	</table>
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$linterface->Get_Action_Btn($Lang['Btn']['View'], "button", "goSearch()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
		<p class="spacer"></p>
	</div>
	<br>
</div>
<form id="ajax_form" method="post" action="index.php?task=management/attendance/update">


</form>	


<?php
$linterface->LAYOUT_STOP();
 ?>