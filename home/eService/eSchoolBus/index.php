<?php
// using :

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/eSchoolBus_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus.php");
include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus_ui.php");
include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus_db.php");

intranet_auth();
intranet_opendb();
$indexVar['linterface'] = new libSchoolBus_ui();
$indexVar['libSchoolBus'] = new libSchoolBus();
$indexVar['db'] = new libSchoolBus_db();

$task = '';
if ($_POST['task'] != '') {
	$task = $_POST['task'];
}
else if ($_GET['task'] != '') {
	$task = $_GET['task'];
}

if ($task == '') {
	$task = 'view';
}

### Menu Settings
$CurrentPage = $indexVar['libSchoolBus']->getCurrentPage($task);
$CurrentPageArr['eServiceSchoolBus'] = 1;
$MODULE_OBJ = $indexVar['linterface']->GET_MODULE_OBJ_ARR($eService=true);
$TAGS_OBJ = $indexVar['linterface']->GET_TAGS_OBJ_ARR();
// $indexVar['libSchoolBus']->checkAccessRight();
// $indexVar['linterface']->LAYOUT_START($returnMsg);




### Include corresponding php files
$indexVar['taskScript'] = '';
$indexVar['taskScript'] .= $task.'.php';

if (file_exists($indexVar['taskScript'])){
	include_once($indexVar['taskScript']);
}
else {
	$indexVar['taskScript'] = $task.'/index.php';
	if (file_exists($indexVar['taskScript'])){
		include_once($indexVar['taskScript']);
	}else{
		No_Access_Right_Pop_Up();
	}
}

// $indexVar['linterface']->LAYOUT_STOP();
intranet_closedb();
?>