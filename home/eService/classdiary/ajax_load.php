<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary_ui.php");

intranet_auth();
intranet_opendb();

$libclassdiary_ui = new libclassdiary_ui();
$libclassdiary = $libclassdiary_ui->Get_libclassdiary();
if(!$plugin['ClassDiary'] || !$libclassdiary->IS_eService_USER())
{
	intranet_closedb();
	exit;
}

switch($task)
{
	case "Get_Class_Diary_Student_View_Table":
		echo $libclassdiary_ui->Get_Class_Diary_Student_View_Table($_REQUEST['StudentID'],$_REQUEST['FromDate'],$_REQUEST['ToDate']);
	break;
}

intranet_closedb();
?>