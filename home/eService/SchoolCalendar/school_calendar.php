<?php
/*   Using By :
 *
 *  2020-07-30 Cameron
 *      - Create this file
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

# Check access right
if (!(($_SESSION['UserType'] == USERTYPE_STAFF) || ($_SESSION['UserType'] == USERTYPE_STUDENT) || ($_SESSION['UserType'] == USERTYPE_PARENT))) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$linterface = new interface_html();
$lc = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();
$lfcm = new form_class_manage();
$libcalevent = new libcalevent2007();

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['Class']['ClassGroupSettings'];

#############################################
## System Checking
## Step 1 : check finish academic year setting or not
##				IsAcademicYearSet() : 1 = true, 0= false
## Step 2 : check current date is in term or not
##				IsCurrentDateInTerm() : 1 = true, 0 = false
#############################################
if(!$lc->IsAcademicYearSet())
{
    $empty_msg = $Lang['SysMgr']['SchoolCalendar']['FieldTitle']['AcademicYearIsNotSet'];
    $msg = urlencode($empty_msg);
    //header("Location: academic_year.php?msg=$msg");
}
if(!$lc->IsCurrentDateInTerm())
{
    $not_in_term_msg = $Lang['SysMgr']['SchoolCalendar']['FieldTitle']['CurrentDateNotInTerm'];
    $msg = urlencode($not_in_term_msg);
    //header("Location: academic_year.php?msg=$msg");
}
## End checking ##

$CurrentPageArr['SchoolCalendar'] = 1;
### Title ###
$TAGS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['SettingTitle'],"index.php",1);

$MODULE_OBJ['title'] = $Lang['SysMgr']['SchoolCalendar']['ModuleTitle'];

$TAGS_OBJ_RIGHT[] = array();


if($AcademicYearID == "")
{
    $AcademicYearID = Get_Current_Academic_Year_ID();
}

$ymTable = 'ymTable'.(date('Ym'));

$linterface->LAYOUT_START();
?>
<!--[if lte IE 7]>
<style type="text/css">
    html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

<?php echo $lcycleperiods_ui->getProductionCycleDaysCalendarView(1,$AcademicYearID,$class_group_id='',$viewOnly=true); ?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>


<script type="text/javascript">

$(document).ready(function(){
    var obj = $("#<?php echo $ymTable;?>");
    if (obj.length) {
        scrollToElement(obj);
    }
});

function scrollToElement(ele)
{
//    $(window).scrollTop(ele.position().top).scrollLeft(ele.position().left);
    $(window).scrollTop(ele.offset().top).scrollLeft(ele.offset().left);
}

function ChangeSchoolYear(type, schoolYearID)
    {
        Block_Document();
        $.post(
            "../../../home/system_settings/school_calendar/ajax_calendar_view.php",
            {
                "type":type,
                "targetSchoolYearID":schoolYearID,
                "viewOnly":1
                <?php if($isKIS){ ?>
                ,"targetClassGroupID":$('#class_group_id').val()
                <?php } ?>
            },
            function(responseText){
                UnBlock_Document();
                $('#main_body').html(responseText);
                initThickBox();
            }
        );
    }

    function jsShowMoreEvent(val, month)
    {
        LayerClassName = month+"DetailEventRow";

        if(val == 1){
            $("."+LayerClassName).show();
            $("#MoreEventLayer"+month).hide();
            $("#HideEventLayer"+month).show();
        }else{
            $("."+LayerClassName).hide();
            $("#MoreEventLayer"+month).show();
            $("#HideEventLayer"+month).hide();
        }
    }

</script>