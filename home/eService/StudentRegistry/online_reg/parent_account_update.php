<?php
#using: yat

######### Change Log
#
#
##########################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");

intranet_opendb();

$li = new libdb();
$lc = new libeclass();
$lu = new libuser($UserID);

$NickName = intranet_htmlspecialchars(trim($NickName));
$Gender = intranet_htmlspecialchars(trim($Gender));
$UserEmail = intranet_htmlspecialchars(trim($UserEmail));
$HomeTelNo = intranet_htmlspecialchars(trim($HomeTelNo));
$OfficeTelNo = intranet_htmlspecialchars(trim($OfficeTelNo));
$MobileTelNo = intranet_htmlspecialchars(trim($MobileTelNo));
$FaxNo = intranet_htmlspecialchars(trim($FaxNo));
$ICQNo = intranet_htmlspecialchars(trim($ICQNo));
$Address = intranet_htmlspecialchars(trim($Address));
$Country = intranet_htmlspecialchars(trim($Country));
$URL = intranet_htmlspecialchars(trim($URL));
$Info = intranet_htmlspecialchars(trim($Info));

################################################
# Check whether can change the info
################################################
# 1. New email does not exists
$failed = false;
if ($UserEmail != "" && $lu->UserEmail != $UserEmail)
{
    if ($lu->isEmailExists($UserEmail))
    {
        $failed = true;
        $signal = "EmailUsed";
    }
    else
    {
        if ($lc->isEmailExistInSystem($UserEmail))
        {
            $failed = true;
            $signal = "EmailUsed";
        }
    }
}

if(!$failed)
{
	# Check whether can change the info
	################################################
	# Personal Photo
	################################################
	$re_path2 = "/file/photo/personal";
	$path2 = "$intranet_root$re_path2";
	$target = "";
	if($personal_photo=="none" || $personal_photo == "")
	{
	}
	else
	{
		$lf = new libfilesystem();
		if (!is_dir($path2))
		{
			$lf->folder_new($path2);
		}
		$ext = strtolower($lf->file_ext($personal_photo_name));
		
		if ($ext == ".jpg")
		{
			$target2 = $path2 ."/p". $lu->UserID . $ext;
			$re_path2 .= "/p". $lu->UserID . $ext;
			
			$lf->lfs_copy($personal_photo, $target2);
			# check need to resize or not			
			include_once($PATH_WRT_ROOT."includes/SimpleImage.php");
			$im = new SimpleImage();
			if($im->GDLib_ava)
			{
				$im->load($target2);
				$w = $im->getWidth();
				$h = $im->getHeight();
				$default_photo_width = $default_photo_width ? $default_photo_width : 100;
				$default_photo_height = $default_photo_height ? $default_photo_height : 130;
				if($w > $default_photo_width || $h > $default_photo_height)	
				{
					$im->resize($default_photo_width,$default_photo_height);
					$im->save($target2);	
				}
			}
		}
	}
	################################################
	# Personal Photo [End]
	################################################
	
	if($lu->RetrieveUserInfoSetting("CanUpdate", "NickName"))
		$fieldname .= "NickName = '$NickName', ";      
	
	if($lu->RetrieveUserInfoSetting("CanUpdate", "Gender"))
		$fieldname .= "Gender = '$Gender', ";
	
	if ($target2 != "")
		$fieldname .= "PersonalPhotoLink = '$re_path2', ";
	
	if ($lu->RetrieveUserInfoSetting("CanUpdate", "HomeTel"))
		$fieldname .= "HomeTelNo = '$HomeTelNo', ";
	
	if ($lu->RetrieveUserInfoSetting("CanUpdate", "OfficeTel"))
		$fieldname .= "OfficeTelNo = '$OfficeTelNo', ";
	
	if ($lu->RetrieveUserInfoSetting("CanUpdate", "Mobile"))
		$fieldname .= "MobileTelNo = '$MobileTelNo', ";
	
	if ($lu->RetrieveUserInfoSetting("CanUpdate", "Fax"))
		$fieldname .= "FaxNo = '$FaxNo', ";
	
	if($lu->RetrieveUserInfoSetting("CanUpdate", "Address"))
		$fieldname .= "Address = '$Address', ";
	
	if ($lu->RetrieveUserInfoSetting("CanUpdate", "Country"))
		$fieldname .= "Country = '$Country', ";
	
	if ($lu->RetrieveUserInfoSetting("CanUpdate", "URL"))
		$fieldname .= "URL = '$URL', ";		
		
	if ($UserEmail != "")
    {
      $fieldname .= "UserEmail = '$UserEmail', ";
      $newmail = $UserEmail;
    }
    else
      $newmail = "";	
		
	if ($lu->RetrieveUserInfoSetting("CanUpdate", "Message"))
		$fieldname .= "Info = '".$Info."', ";

	$fieldname .= "DateModified = now(), ";
	$fieldname .= "ModifyBy = $UserID";
	
	$sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = $UserID";
	$result = $li->db_db_query($sql);
	
	if ($result)
	{
		$lc->eClassUserUpdateInfoIP($lu->UserEmail, $Title, $lu->EnglishName,addslashes($lu->ChineseName),$lu->FirstName,$lu->LastName, $NickName, $lu->UserPassword, $lu->ClassName,$lu->ClassNumber, $newmail,$Gender, $lu->ICQNo,$lu->HomeTelNo,$lu->FaxNo,$lu->DateOfBirth,$lu->Address,$lu->Country,$lu->URL,$lu->Info);
		$signal = "UpdateSuccess";
	}
	else
	{
		$signal = "UpdateUnsuccess";
	}
}

intranet_closedb();
header("Location: parent_account.php?msg=$signal");

?>
