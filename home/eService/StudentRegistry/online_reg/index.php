<?php
// using : 

############# Change Log [Start] ################
#
#   2019-07-08 Cameron
#       - don't show eNotice and message to parrent in this page for Kentville [case #Y164476]
#
#   2019-03-26 Cameron
#       - customize for Kentville [case #Y140316]
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_kv.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

if ($sys_custom['StudentRegistry']['Kentville']) {
    $lsr = new libstudentregistry_kv();
}
else {
    $lsr = new libstudentregistry();
}
$lu = new libuser($UserID);
$lnotice = new libnotice();

# Check access right
if(!($lsr->checkParentFillOnlineReg()))
{
	header("location: /index.php");
	exit;
}

$linterface = new interface_html();

### Title ###
$MODULE_OBJ['title'] = $Lang['StudentRegistry']['Settings']['OnlineReg'];
$linterface->LAYOUT_START();

# account information last modified by ownself
$last_modified = $lu->ModifyBy==$UserID ? $lu->DateModified : "";

# check children list

$myChildren = $lu->getChildrenList("", false);
$selected_year = trim_array(explode(",",$lsr->YearIDStr));

foreach($myChildren as $k=>$d)
{
	# check the student in the selected form or not
	$thisForm = $lsr->getStudentForm($d['StudentID']);	
	if($_SESSION['ParentFillOnlineRegMode']=="current")
	{
		
		if(!in_array($thisForm, $selected_year))	continue;
	} elseif (in_array($thisForm, $selected_year))
	{
		$HasCurrentStudent = true;
	}
	# new student online reg > display all student at the moment, will improve later
	# not selected parent group, should be improve to select form as well
	
	$child_table .= "<p class='spacer'></p>
						<div class='reg_board'>
							<h1 class='head_student'>
						    	<span>". $Lang['Identity']['Student'] ."</span>
						        <em>". $d['StudentName'] ."</em>
						    </h1>";
	
	$child_table .= "<ul>";						    					    
	# registration information
	if ($sys_custom['StudentRegistry']['Kentville']) {
	    $this_reg_data = $lsr->getStudentInfoBasic($d['StudentID']);
	}
	else {
	    $this_reg_data = $lsr->RETRIEVE_STUDENT_INFO_BASIC_HK($d['StudentID']);
	}
	$last_update_str = $this_reg_data[0]['ModifyBy'] ? $this_reg_data[0]['DateModified']." by ". $this_reg_data[0]['ModifyByName'] : ""; 
	$child_table .= "<li class='btn_info'><a href='javascript:pop_reg(". $d['StudentID'] .")'>". $Lang['Menu']['AccountMgmt']['StudentRegistryInfo'] ."</a><span>". $last_update_str ."</span> <p class='spacer'></p></li>";

    if (!$sys_custom['StudentRegistry']['Kentville']) {
        # retrieve eNotice list
        $NoticeData = $lnotice->getParentNotice($d['StudentID']);
        foreach ($NoticeData as $k => $d) {
            $sign_str = $d['SignerID'] ? $d['DateModified'] . " by " . $lsr->RETRIEVE_MODIFIED_BY_INFO($d['SignerID']) : "";

            $signed_str = $sign_str ? $sign_str : "<img src='{$image_path}/{$LAYOUT_SKIN}/alert_new.gif' />";
            $child_table .= "<li class='btn_notice'><a href='javascript:sign(" . $d['NoticeID'] . "," . $d['StudentID'] . ")'>" . $ip20_enotice . " - " . $d['Title'] . "</a><span>" . $signed_str . "</span> <p class='spacer'></p></li>";
        }
    }
	$child_table .= "<p class='spacer'></p></ul>";
	$child_table .= "</div>";
}


if ($sys_custom['StudentRegistry']['Kentville']) {
    $Text2Display = "";
}
else {
    $Text2Display = ($_SESSION['ParentFillOnlineRegMode'] == "new") ? $lsr->NewStudentText2Display : $lsr->CurrentStudentText2Display;
}
?>

<script language="javascript">
<!--
function sign(id,studentID)
{
	newWindow('/home/eService/notice/sign.php?NoticeID='+id+'&StudentID='+studentID,10);
}

function pop_login()
{
	newWindow('login_password.php?FromLogin=OnlineReg',10);
}

function pop_parent_info()
{
	newWindow('parent_account.php',10);
}

function pop_reg(studentID)
{
	newWindow('/home/eAdmin/AccountMgmt/StudentRegistry/management/hk/information/view.php?studentID='+studentID,10);
}
//-->
</script>


<table align="center" width="600" border="0">
<tr>
	<td>
		<?=$Text2Display?>
	</td>
</tr>
</table>

<? if($_SESSION['ParentFillOnlineRegMode']=="new") {?>
 <div class="reg_board">
	<h1 class="head_parent">
    	<span><?=$Lang['Identity']['Parent']?></span>
        <em><?=$lu->UserName()?></em>
      </h1>
        <ul>
        	<li class="btn_setting"><a href="javascript:pop_login();"><?=$i_UserProfileLogin?></a><span></span> <p class="spacer"></p></li>
        	<? /* ?>
            <li class="btn_setting"><a href="javascript:pop_parent_info();"><?=$Lang['Gamma']['AccountInformation']?></a><span><?=$last_modified?></span> <p class="spacer"></p></li>
            <? */ ?>
             <p class="spacer"></p>
        </ul>
</div>
<? } ?>

<?/* if($Lang['StudentRegistry']['ToParent']) {?>
<center><font color="red" size="4"><?=$Lang['StudentRegistry']['ToParent']?></font></p>
<? } */?>

<?=$child_table?>

<? if($_SESSION['ParentFillOnlineRegMode']=="current" || $HasCurrentStudent) {
	
	$_SESSION['ParentFillOnline_Intranet_Allowed'] = true;
	?>
<?php if (!$sys_custom['StudentRegistry']['Kentville']):?>	
<div align="center">
<br><br><?= $linterface->GET_ACTION_BTN($Lang['eComm']['BackToHome'], "button", "window.location='/'")?>
</div> 
<?php endif;?>
<? } ?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

