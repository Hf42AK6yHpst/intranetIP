<?php
/*
 * Using: 
 * Change Log:
 * Date:    2020-04-22  Tommy
 *          - remove $libenroll->hasAccessRight() because it uses for eAdmin checking
 * Date:	2017-03-21	Villa	#E89842 
 * 			- Modified OLE Display: Using ELE array in stead of lang to get OLE category name
 * Date:	2017-03-20	Villa	#E89842 
 * 			- Add Group Category/ OLE Category Display
 */
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."home/admin/enrollment/templang.php");
intranet_auth();
intranet_opendb();



if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();
	
	$EnrolGroupID = IntegerSafe($EnrolGroupID);
	$applicantId = IntegerSafe($applicantId);
	
	$applicableEnrolGroupIdAry = array();
	$canBeAppliedClubInfoAry = $libenroll->getGroupsAvailableForApply($applicantId, 0);
	$canBeAppliedClubIdAry = Get_Array_By_Key($canBeAppliedClubInfoAry, '0');
	
	$enrolledClubIdAry = Get_Array_By_Key($libenroll->Get_Student_Club_Info($applicantId), 'EnrolGroupID');
	$applicableEnrolGroupIdAry = array_merge($canBeAppliedClubIdAry, $enrolledClubIdAry);
	
	if (!in_array($EnrolGroupID, (array)$applicableEnrolGroupIdAry)) {
		No_Access_Right_Pop_Up();
	}
	
	
	$EnrollGroupArr = $libenroll->GET_GROUPINFO($EnrolGroupID);
	$GroupInfoArr = $libenroll->getGroupInfo($EnrollGroupArr[1]);
	
	if($plugin['iPortfolio']) {
// 		libportfolio
		include_once($PATH_WRT_ROOT."includes/libportfolio.php");
		include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
		$LibPortfolio = new libpf_slp();
		$DefaultELEArray = $LibPortfolio->GET_ELE();
		##Category
		$categoryAry = $libenroll->GET_CATEGORY_LIST();
		$CategoryID = $EnrollGroupArr['GroupCategory'];
		foreach($categoryAry as $_categoryAry){
			if($CategoryID==$_categoryAry['CategoryID']){
				$CategoryName = $_categoryAry['CategoryName'];
			}
		}
		##OLE 
		$OLEAry = $libenroll->Get_Enrolment_Default_OLE_Setting('club',(array)$EnrolGroupID);
		$OLE_Component_Ary = explode(',',$OLEAry[0]['OLE_Component']);
		foreach ($OLE_Component_Ary as $_OLE_Component_Ary){
			$temp = intranet_undo_htmlspecialchars($DefaultELEArray[$_OLE_Component_Ary]);
			if($temp){ //prevent temp is null
				$OLE_Component_Name_Ary[] = $temp;
 			}
		}
		$OLE_Component_Name = implode(', ',(array)$OLE_Component_Name_Ary);
	}
	
	$MODULE_OBJ['title'] = $GroupInfoArr[0][1].$eEnrollment['front']['information'];
//     if ($libenroll->hasAccessRight($_SESSION['UserID']))
    if($libenroll->isUseEnrollment())
    {
	    $linterface = new interface_html("popup.html");

	    ################################################################

// if ($libenroll->hasAccessRight($_SESSION['UserID']))
if($libenroll->isUseEnrollment())
{

}
else
{
    echo "You have no priviledge to access this page.";
    exit();
}

################################################################

$linterface->LAYOUT_START();

$lclass = new libclass();
$ClassLvlArr = $lclass->getLevelArray();
$GroupArr = $libenroll->GET_GROUPCLASSLEVEL($EnrollGroupArr[0]);
$GroupYearID = Get_Array_By_Key($GroupArr,"ClassLevelID");

$ClassLvlChk .= "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">";
for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
	$checked = false;
	for ($j = 0; $j < sizeof($GroupArr); $j++) {	
		if (in_array($ClassLvlArr[$i][0],$GroupYearID)) {	
			 $checked = true;
			 break;
		}
	}
	
	if (($i % 10) == 0) $ClassLvlChk .= "<tr><td class=\"tabletext\">";
	if ($checked) {
		$ClassLvlChk .= $comma.$ClassLvlArr[$i][1];
		$comma = ", ";
	}
	if (($i % 10) == 9) $ClassLvlChk .= "</td></tr>";
}
$ClassLvlChk .= "</table>";

?>
<table id="html_body_frame" width="95%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr>
		<td colspan="2" height="15"></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $eEnrollment['add_activity']['act_name']?></td>
		<td class="tabletext"><?= $GroupInfoArr[0][1]?></td>
	</tr>
<!-- 	Category -->
<?php if($plugin['iPortfolio']) {?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eEnrollment['add_activity']['act_category']?></td>
		<td class="tabletext"><?=Get_String_Display($CategoryName)?></td>
	</tr>
<?php }?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_content']?></td>
		<td class="tabletext"><?= $EnrollGroupArr[4]?></td>
	</tr>
<!-- 	OLE -->
<?php if($plugin['iPortfolio']) {?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eEnrollment['OLE_Components']?></td>
		<td class="tabletext"><?=Get_String_Display($OLE_Component_Name)?></td>
	</tr>
<?php }?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_target']?> </td>
		<td class="tabletext"><?= $ClassLvlChk?>
			
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_age']?></td>
		<td class="tabletext">
			<? if (($EnrollGroupArr[7] > 0)||($EnrollGroupArr[8] > 0)) { ?>
				<?= $EnrollGroupArr[8]?>
				-
				<?= $EnrollGroupArr[7]?>&nbsp;<?= $eEnrollment['add_activity']['age']?>
			<? } else { ?>
				<?= $eEnrollment['no_limit']?>
			<? } ?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_gender']?></td>
		<td class="tabletext">
			<?
				if ($EnrollGroupArr[9] == "M") {
					print $eEnrollment['male'];
				} else if ($EnrollGroupArr[9] == "F") {
					print $eEnrollment['female']; 
				} else {
					print $eEnrollment['all'];
				}
			?>
		</td>
	</tr>
	
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_quota']?></td>
		<td class="tabletext"><? ($EnrollGroupArr[2] == 0) ? print $i_ClubsEnrollment_NoLimit : print $EnrollGroupArr[2]; ?></td>
	</tr>
	
	<?
		if (($EnrollGroupArr[10] != "")||($EnrollGroupArr[11] != "")||($EnrollGroupArr[12] != "")||
			($EnrollGroupArr[13] != "")||($EnrollGroupArr[14] != ""))  {
	?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['attachment']?></td>
		<td class="tabletext">
		<? if ($EnrollGroupArr[10] != "")  { ?>
			<a href="<?= "/file/".$EnrollGroupArr[10]?>" target="_blank" class="tablelink"><?= basename($EnrollGroupArr[10])?></a><br/>
		<? } ?>
		<? if ($EnrollGroupArr[11] != "")  { ?>
			<a href="<?= "/file/".$EnrollGroupArr[11]?>" target="_blank" class="tablelink"><?= basename($EnrollGroupArr[11])?></a><br/>
		<? } ?>
		<? if ($EnrollGroupArr[12] != "")  { ?>
			<a href="<?= "/file/".$EnrollGroupArr[12]?>" target="_blank" class="tablelink"><?= basename($EnrollGroupArr[12])?></a><br/>
		<? } ?>
		<? if ($EnrollGroupArr[13] != "")  { ?>
			<a href="<?= "/file/".$EnrollGroupArr[13]?>" target="_blank" class="tablelink"><?= basename($EnrollGroupArr[13])?></a><br/>
		<? } ?>
		<? if ($EnrollGroupArr[14] != "")  { ?>
			<a href="<?= "/file/".$EnrollGroupArr[14]?>" target="_blank" class="tablelink"><?= basename($EnrollGroupArr[14])?></a><br/>
		<? } ?>
		<? if ($EnrollGroupArr['AttachmentLink6'] != "")  { ?>
			<a href="<?= "/file/".$EnrollGroupArr['AttachmentLink6']?>" target="_blank" class="tablelink"><?= basename($EnrollGroupArr['AttachmentLink6'])?></a><br/>
		<? } ?>
		<? if ($EnrollGroupArr['AttachmentLink7'] != "")  { ?>
			<a href="<?= "/file/".$EnrollGroupArr['AttachmentLink7']?>" target="_blank" class="tablelink"><?= basename($EnrollGroupArr['AttachmentLink7'])?></a><br/>
		<? } ?>
		<? if ($EnrollGroupArr['AttachmentLink8'] != "")  { ?>
			<a href="<?= "/file/".$EnrollGroupArr['AttachmentLink8']?>" target="_blank" class="tablelink"><?= basename($EnrollGroupArr['AttachmentLink8'])?></a><br/>
		<? } ?>
		<? if ($EnrollGroupArr['AttachmentLink9'] != "")  { ?>
			<a href="<?= "/file/".$EnrollGroupArr['AttachmentLink9']?>" target="_blank" class="tablelink"><?= basename($EnrollGroupArr['AttachmentLink9'])?></a><br/>
		<? } ?>
		<? if ($EnrollGroupArr['AttachmentLink10'] != "")  { ?>
			<a href="<?= "/file/".$EnrollGroupArr['AttachmentLink10']?>" target="_blank" class="tablelink"><?= basename($EnrollGroupArr['AttachmentLink10'])?></a><br/>
		<? } ?>
		</td>
	</tr>
	<?
		}
	?>
	
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0">    
    	<tr>
        	<td colspan="3">        
            	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
                	<tr>
                    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                    </tr>
                    <tr>
        			<td align="center">
        				<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","close_btn") ?>
        			</td>
        			</tr> 
        		</table>                                
        	</td>
        </tr>
	</table>
<br>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>