<?php
//Using: Rita

/**************************************************
 * 	Date:		2013-02-08 (Rita)
 * 	Details:	add btn 'Cancel & Close' and 'Submit & Enrol'
 * 	Date:		2012-12-12 (Rita)
 * 	Details:	Copy from eSurvey, amend it to suit for activity survey customization	
 **************************************************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$lform = new libform();
$lsurvey = new libsurvey();
$libenroll = new libclubsenrol($AcademicYearID);
$LibUser = new libuser();

if (!isset($AcademicYearID) || $AcademicYearID==""){
	$AcademicYearID = Get_Current_Academic_Year_ID();
}

$RecordType = $_GET['RecordType'];
$ApplicantID = $_GET['ApplicantID'];
$enrolType = $_GET['enrolType'];

if($RecordType=="A"){
	$eEnrolRecordID = $_GET['EnrolEventID'];
	$enrollEventInfoArr = $libenroll->GET_EVENTINFO($eEnrolRecordID);
	
}elseif($RecordType=="C"){
	
}

# Check Access Right
if (!$libenroll->isUseEnrollment()) {
	die("You have no priviledge to access this page.");
}
elseif ((($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) || ($libenroll->IS_ENROL_MASTER($_SESSION['UserID']))) && !$LibUser->isStudent() && !$LibUser->isParent()){
	header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/");	
}else{
		
	if (!$libenroll->hasAccessRight($_SESSION['UserID']) || !$libenroll->enableActivitySurveyFormRight())
    {
    	
    	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
    	
    }
}

$userAnswer = $libenroll->returnUserSurveyAnswerInfo($ApplicantID, $eEnrolRecordID);

if(!empty($userAnswer) && $lsurvey->NotAllowReSign)
{
	header("Location: fill_update.php?RecordType=$RecordType&eEnrolRecordID=$eEnrolRecordID&ApplicantID=$ApplicantID");
	exit;	
}

if($userAnswer)
{
	$ansstr = $userAnswer[0]['Answer'];
	$temp_ans =  str_replace("&amp;", "&", $ansstr);
	$ansString = $lform->getConvertedString($temp_ans);
	$ansString = trim($ansString)." ";	
}
$MODULE_OBJ['title'] = $i_Survey_FillSurvey;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$queString = $enrollEventInfoArr['Question'];
$queString = str_replace('"','&quot;',$queString);
$queString = str_replace("\n",'<br>',$queString);
$queString = str_replace("\r",'',$queString);

$reqFillAllFields = $enrollEventInfoArr['AllFieldsReq'];

if (sizeof($targetGroups)==0)
{
    $target = $Lang['eSurvey']['WholeSchool'];
}
else
{
    $target = implode(", ",$targetGroups);
}


$formBtn = '';
if($enrolType=='toEnrol'){
	$formBtn .=  $linterface->GET_ACTION_BTN($button_submit . ' & ' . $eEnrollment['front']['enroll'] , "button", "js_Submit_Survey($ApplicantID, $eEnrolRecordID);");
	
}elseif($enrolType=='View'){
	$formBtn =  $linterface->GET_ACTION_BTN($button_submit, "submit", "copyback();");
}

$formBtn .= ' ';
$formBtn .=  $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel']  . ' & ' . $Lang['Btn']['Close'] , "button", "window.close(); return false;","cancelbtn");



?>


<script language="Javascript">
function js_Submit_Survey(userID, EnrolEventID){

	window.opener.$('#form1').attr("action", 'event_add_update.php');
	window.opener.$('#EnrolEventID').val(EnrolEventID);
	window.opener.$('#form1').submit();
	
	copyback();
	$('#surveyForm').submit();
}
</script>

<br />

<table border=0 width=95% cellspacing=0 cellpadding=5 align=center>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$eEnrollment['add_activity']['act_name']?></td>
	<td><?=$enrollEventInfoArr['EventTitle']?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$eEnrollment['add_activity']['app_period']?></td>
	<td><?=$enrollEventInfoArr['ApplyStartTime'] . '<br />'. $Lang['General']['To'] . '<br />' . $enrollEventInfoArr['ApplyEndTime'] ?></td>
</tr>

<? if ($reqFillAllFields) { ?>
<tr>
	<td>&nbsp;</td>
	<td>[<?=$i_Survey_AllRequire2Fill?>]</td>
</tr>
<? } ?>

<tr>
	<td colspan=2>
		<form name="ansForm" method="post" action="update.php">
			<input type=hidden name="qStr" value="">
			<input type=hidden name="aStr" value="">
		</form>
		<? if($lsurvey->NotAllowReSign) {?>
		<?=$i_Survey_PleaseFill?>
		<? } ?>
		<hr>
		
		<script LANGUAGE=Javascript>
			// Set true if need to fill in all fields before submit
			var need2checkform = <? if($reqFillAllFields){ echo "true"; } else { echo "false"; }  ?>;
		</script>
		<script language="javascript" src="/templates/forms/layer.js"></script>
		<script language="javascript" src="/templates/forms/form_edit.js"></script>
		
		<script language="Javascript">
		<?=$lform->getWordsInJS()?>
	
		var replyslip = '<?=$Lang['eSurvey']['ReplySlip']?>';
		var DisplayQuestionNumber = '<?=$enrollEventInfoArr['DisplayQuestionNumber']?>';

		//background_image = "/images/layer_bg.gif";
		
		var sheet= new Answersheet();
		// attention: MUST replace '"' to '&quot;'
		sheet.qString="<?=$queString?>";
		sheet.aString="<?=$ansString?>";
		//edit submitted application
		sheet.mode=1;
		sheet.answer=sheet.sheetArr();
		//sheet.templates=form_templates;
		document.write(editPanel());
		</script>
		<SCRIPT LANGUAGE=javascript>
		function copyback()
		{
		 finish();
		 document.form1.qStr.value = document.ansForm.qStr.value;
		 document.form1.aStr.value = document.ansForm.aStr.value;
		}
		function validSubmit()
		{
		 if (!need2checkform || formAllFilled)
		 {
		      return true;
		 }
		 else //return true;
		 {
		      alert("<?=$i_Survey_alert_PleaseFillAllAnswer?>!");
		      return false;
		 }
		}
		</SCRIPT>
		<hr>
	</td>
</tr>
<tr>
	<form name=form1 id="surveyForm" action=fill_update.php method=post onsubmit="return validSubmit()">
	
		<td colspan="2" align="center">
			<?=$formBtn?>
		</td>
		<input type=hidden name=qStr value="">
		<input type=hidden name=aStr value="">
		
		<input type=hidden name=eEnrolRecordID value="<?=$eEnrolRecordID?>">
		<input type=hidden name=RecordType value="<?=$RecordType?>">
		<input type=hidden name=ApplicantID value="<?=$ApplicantID?>">
		
		<input type=hidden name=row value="<?=$row?>">
	</form>
</tr>
</table>


<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>