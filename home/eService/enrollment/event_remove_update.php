<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$ApplicantID = IntegerSafe($_POST['ApplicantID']);
$EnrolEventID = IntegerSafe($_POST['EnrolEventID']);
$sel_category = IntegerSafe($_POST['sel_category']);

$LibUser = new libuser($_SESSION['UserID']);
$isUserValid = false;
if ($LibUser->IsStudent()) {
	if ($ApplicantID == $_SESSION['UserID']) {
		$isUserValid = true;
	}
}
else if ($LibUser->IsParent()) {
	// is parent, get child ID
	$ChildArr = $LibUser->getChildrenList();
	$ChildrenUserIDAry = Get_Array_By_Key($ChildArr, 'StudentID');
	if (in_array($ApplicantID, $ChildrenUserIDAry)) {
		$isUserValid = true;
	}
}
if (!$isUserValid) {
	No_Access_Right_Pop_Up();
}

$libenroll = new libclubsenrol();

$StudentArr['StudentID'] = $ApplicantID;
$StudentArr['EventEnrolID'] = $EnrolEventID;

$libenroll->DEL_APPLIED_EVENTSTUDENT($StudentArr);

intranet_closedb();
header ("Location: event_index.php?msg=2&ChildID=$ApplicantID&sel_category=$sel_category");
?>