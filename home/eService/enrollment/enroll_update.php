<?php
# Using by:

############## Change Log [start] ####
#	Date:	2016-01-28	omas
#			Fix: If submit application twice will overwrite the rejected record of same choice number -#K92074
#			- now the update sql will only update pending record , and won't update DateInput again
#
#	Date:	2015-09-25	omas
#			Fix: If maxwant = 0 will not delete enrolled record problem
#
#	Date:	2015-01-20	omas
#			Fix: case #J74187
#
#	Date:	2014-10-14	Pun
# 				Add round report for SIS customization
#
############## Change Log [end] ####

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if (!$libenroll->isFillForm())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$ApplicantID = IntegerSafe($ApplicantID);
$OldChildID = IntegerSafe($OldChildID);
$OldCategoryID = IntegerSafe($OldCategoryID);
$max_clubs = IntegerSafe($max_clubs);
$maxwant = IntegerSafe($maxwant);

$LibUser = new libuser($_SESSION['UserID']);
$isUserValid = false;
if ($LibUser->IsStudent()) {
	if ($ApplicantID == $_SESSION['UserID']) {
		$isUserValid = true;
	}
}
else if ($LibUser->IsParent()) {
	// is parent, get child ID
	$ChildArr = $LibUser->getChildrenList();
	$ChildrenUserIDAry = Get_Array_By_Key($ChildArr, 'StudentID');
		
	if (in_array($ApplicantID, $ChildrenUserIDAry)) {
		$isUserValid = true;
	}
}

if (!$isUserValid) {
	No_Access_Right_Pop_Up();
}



$SuccessArr = array();
$libenroll->Start_Trans();

$targetSemester = $libenroll->targetSemester;

$maxwant +=0;
# Table INTRANET_ENROL_STUDENT
$ParInfo['maxwant'] = $maxwant;
$ParInfo['StudentID'] = $ApplicantID;
$ParInfo['Category'] = $sel_category;
$libenroll->Update_Enrol_Student_Max_Club($ParInfo);

$ClubOfCurrentYear = $libenroll->Get_EnrolGroupID_Of_Current_Year();	     
# Table INTRANET_ENROL_GROUPSTUDENT
$filled_count = 0;
for ($i=1; $i<=$max_clubs; $i++)
{
	 $crash = false;
     $choice = IntegerSafe(${"choice$i"});
     
     if ($maxwant == 0) {
     	// if not want to join any club => clear all enrollment records
     	$choice = '';
     }
     
     if (trim($choice) == "")
     {
     	if($sel_category!='')
     		$INNER_JOIN_INTRANET_ENROL_GROUPINFO = " INNER JOIN INTRANET_ENROL_GROUPINFO b ON a.EnrolGroupID = b.EnrolGroupID AND b.GroupCategory='$sel_category' ";
     	else
     		$INNER_JOIN_INTRANET_ENROL_GROUPINFO = "";	
         $sql = "
			DELETE
				a 
			FROM 
				INTRANET_ENROL_GROUPSTUDENT a
				$INNER_JOIN_INTRANET_ENROL_GROUPINFO
			WHERE 
				a.StudentID = '$ApplicantID' 
				AND a.Choice = '$i' 
				AND a.EnrolGroupID IN ('".implode("','",(array)$ClubOfCurrentYear)."')
				AND a.RecordStatus = 0
				
		";         
//		debug_pr($sql);
         $libenroll->db_db_query($sql);
     }
     else
     {	    
	     
	     /*
		# School request, Temp disable for check time crash, due to slow performamce
		# �E�s�u��� - eEnrolment -- Slow performance [CRM Ref No.: 2008-0908-0944]
		# ���Q�խװ|�Ǯ� - Bypass the time crash  --- eEnrollment [CRM Ref No.: 2008-0911-0834]
		# ����E�s�����Ф��ثŹD�|�G�a������ - [Partner] Request disable Time-crash checking for eEnrolment [CRM Ref No.: 2008-1023-1018]
		*/
	    ############ check for time crash ############################################################
	    /*
	     * No need to check time crash here
	     *	- if oneEnrol is 1 => check in the index page when student applying clubs (student cannot submit if time crash occurs)
	     *	- if oneEnrol is 0 => allow enrolment of clubs even time crash occurs
	     
		if ($libenroll->oneEnrol == 1)
		{
		    $StudentClub = $libenroll->STUDENT_APPLIED_CLUB($ApplicantID);            	
			for ($CountCrash = 0; $CountCrash < sizeof($StudentClub); $CountCrash++) {
				if ( ($libenroll->IS_CLUB_CRASH($choice, $StudentClub[$CountCrash][0])) ) {
					$crash = true;
					continue;
				}
			}
			
			// get student's enrolled event list, check for any time crash
			$StudentEvent = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($ApplicantID);
			for ($CountEvent = 0; $CountEvent < sizeof($StudentEvent); $CountEvent++) {
				if ($libenroll->IS_EVENT_GROUP_CRASH($choice, $StudentEvent[$CountEvent][0])) {
					$crash = true; 
					continue;
				}
			}
		}
		*/
		############ check for time crash ############################################################
		
		//$Sql = "SELECT EnrolGroupID FROM INTRANET_ENROL_GROUPSTUDENT WHERE StudentID = '$ApplicantID' AND Choice < $i";
		//$GroupIDArr = $libenroll->returnArray($Sql);
		$sql = "SELECT EnrolGroupID FROM INTRANET_USERGROUP WHERE UserID = '$ApplicantID' AND EnrolGroupID = '$choice'";
		$EnrolGroupIDArr = $libenroll->returnVector($sql);
		//debug_r($sql);
		//debug_r($EnrolGroupIDArr);
		$GroupID = $libenroll->GET_GROUPID($choice);
         
//		$TestArr[0] = $choice;
//		$TestArr['EnrolGroupID'] = $choice;
		//if (in_array($TestArr, $EnrolGroupIDArr)) $AddBefore = true;
		if (in_array($choice, $EnrolGroupIDArr)) $AddBefore = true;
		
         if ((!$crash)&&(!$AddBefore)) {
	     	if($sel_category!='')
	     		$INNER_JOIN_INTRANET_ENROL_GROUPINFO = " INNER JOIN INTRANET_ENROL_GROUPINFO b ON a.EnrolGroupID = b.EnrolGroupID AND b.GroupCategory='$sel_category' ";
	     	else
	     		$INNER_JOIN_INTRANET_ENROL_GROUPINFO = "";	
         		     
	         $sql = "
				SELECT 
					COUNT(*) 
				FROM 
					INTRANET_ENROL_GROUPSTUDENT a
					$INNER_JOIN_INTRANET_ENROL_GROUPINFO 
				WHERE 
					a.StudentID = '$ApplicantID' 
					AND a.Choice = '$i'
					AND a.EnrolGroupID IN ('".implode("','",(array)$ClubOfCurrentYear)."')
					AND a.RecordStatus = 0
			";
         
			if($plugin['SIS_eEnrollment']){
				if($sel_category!=''){
					$categoryFilter = "AND b.GroupCategory='$sel_category'";
				}
				$sql = "
					SELECT 
						COUNT(*) 
					FROM 
						INTRANET_ENROL_GROUPSTUDENT a
					INNER JOIN 
						INTRANET_ENROL_GROUPINFO b 
					ON 
						a.EnrolGroupID = b.EnrolGroupID 
						{$categoryFilter}
					WHERE 
						a.StudentID = '$ApplicantID' 
						AND a.Choice = '$i'
						AND a.EnrolGroupID IN ('".implode("','",(array)$ClubOfCurrentYear)."')
						AND b.RoundNumber = '{$libenroll->targetRound}'
				";
			}
			
	         $count = $libenroll->returnVector($sql);
	         
	         if ($count[0]==0)
	         {	           
	         	 $sql = "INSERT INTO INTRANET_ENROL_GROUPSTUDENT (StudentID, GroupID, EnrolGroupID, EnrolSemester, Choice, RecordStatus,StatusChangedBy,DateInput,DateModified)
	                     VALUES ('$ApplicantID','$GroupID','$choice','$targetSemester','$i',0,'$UserID',now(),now())";
	             $SuccessArr['Insert_INTRANET_ENROL_GROUPSTUDENT'] = $libenroll->db_db_query($sql);
	         }
	         else
	         {
	         	
	         	# update input time if user submit a new club for the same choice
		     	if($sel_category!='')
		     		$INNER_JOIN_INTRANET_ENROL_GROUPINFO = " INNER JOIN INTRANET_ENROL_GROUPINFO b ON a.EnrolGroupID = b.EnrolGroupID AND b.GroupCategory='$sel_category' ";
		     	else
		     		$INNER_JOIN_INTRANET_ENROL_GROUPINFO = "";	
	         	
		         $sql = "
					SELECT 
						a.EnrolGroupID 
					FROM 
						INTRANET_ENROL_GROUPSTUDENT a
						$INNER_JOIN_INTRANET_ENROL_GROUPINFO
					WHERE 
						a.StudentID = '$ApplicantID' 
						AND a.Choice = '$i'
					AND a.EnrolGroupID IN ('".implode("','",(array)$ClubOfCurrentYear)."')
				";
				if($plugin['SIS_eEnrollment']){
					if($sel_category!=''){
						$categoryFilter = "AND b.GroupCategory='$sel_category'";
					}
					$sql = "
						SELECT 
							a.EnrolGroupID 
						FROM 
							INTRANET_ENROL_GROUPSTUDENT a
						INNER JOIN 
							INTRANET_ENROL_GROUPINFO b 
						ON 
							a.EnrolGroupID = b.EnrolGroupID 
							$INNER_JOIN_INTRANET_ENROL_GROUPINFO
						WHERE 
							a.StudentID = '$ApplicantID' 
							AND a.Choice = '$i'
						AND a.EnrolGroupID IN ('".implode("','",(array)$ClubOfCurrentYear)."')
					";
				}
				
	         	 $EnrolGroupID = $libenroll->returnVector($sql);
	         	 $ChoiceGroupID = $EnrolGroupID[0];
		         $DateInputUpdate = ($ChoiceGroupID != $choice) ? ", a.DateInput = now()" : "";
		         
		         # check if the club is applied before
		         $sql = "SELECT RecordStatus, Choice FROM INTRANET_ENROL_GROUPSTUDENT WHERE StudentID = '$ApplicantID' AND EnrolGroupID = '$choice'";
		         $result = $libenroll->returnVector($sql);
		         
		         if ($result == NULL)
		         {
			        # Not applied - set to waiting
			        $status = 0; 
//			      	$sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT SET EnrolGroupID = '$choice', GroupID = '$GroupID', EnrolSemester='$targetSemester', RecordStatus = $status, StatusChangedBy = $UserID, DateModified = now() $DateInputUpdate
//	                     WHERE StudentID = $ApplicantID AND Choice = $i AND RecordStatus != 2";
//	             	$SuccessArr['Update_INTRANET_ENROL_GROUPSTUDENT'] = $libenroll->db_db_query($sql);
		         }
		         else
		         {
			        # Applied - set to the current status and delete the previous record
			      	$sql = "DELETE FROM INTRANET_ENROL_GROUPSTUDENT WHERE EnrolGroupID = '$choice' AND StudentID = '$ApplicantID' AND Choice != '$i'";
			        $SuccessArr['Delete_INTRANET_ENROL_GROUPSTUDENT'] = $libenroll->db_db_query($sql);
			        
			        $status = $result[0][0];			      	
//			      	$sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT SET EnrolGroupID = $choice, GroupID = '$GroupID', EnrolSemester='$targetSemester', RecordStatus = $status, StatusChangedBy = $UserID, DateModified = now() $DateInputUpdate
//	                     WHERE StudentID = $ApplicantID AND Choice = $i AND RecordStatus != 2";
//	            	$SuccessArr['Update_INTRANET_ENROL_GROUPSTUDENT'] = $libenroll->db_db_query($sql); 
		         }

		     	if($sel_category!='')
		     		$INNER_JOIN_INTRANET_ENROL_GROUPINFO = " INNER JOIN INTRANET_ENROL_GROUPINFO b ON a.EnrolGroupID = b.EnrolGroupID  AND b.GroupCategory='$sel_category'";
		     	else
		     		$INNER_JOIN_INTRANET_ENROL_GROUPINFO = "";	
				
		     	//K92074 
		      	$sql = "
					UPDATE 
						INTRANET_ENROL_GROUPSTUDENT a
						$INNER_JOIN_INTRANET_ENROL_GROUPINFO 
					SET 
						a.EnrolGroupID = '$choice', a.GroupID = '$GroupID', a.EnrolSemester='$targetSemester', a.RecordStatus = '$status', a.StatusChangedBy = '$UserID', a.DateModified = now() 
						WHERE 
						a.StudentID = '$ApplicantID' AND a.Choice = '$i' AND a.RecordStatus = 0
						AND a.EnrolGroupID IN ('".implode("','",(array)$ClubOfCurrentYear)."')
				";
				if($plugin['SIS_eEnrollment']){
			     	if($sel_category!='')
			     		$INNER_JOIN_INTRANET_ENROL_GROUPINFO = " AND b.GroupCategory='$sel_category'";

			      	$sql = "
						UPDATE 
							INTRANET_ENROL_GROUPSTUDENT a
						INNER JOIN 
							INTRANET_ENROL_GROUPINFO b 
						ON 
							a.EnrolGroupID = b.EnrolGroupID
						AND
							b.RoundNumber='{$libenroll->targetRound}'
							$INNER_JOIN_INTRANET_ENROL_GROUPINFO 
						SET 
							a.EnrolGroupID = '$choice', a.GroupID = '$GroupID', a.EnrolSemester='$targetSemester', a.RecordStatus = '$status', a.StatusChangedBy = '$UserID', a.DateModified = now() 
					    WHERE 
							a.StudentID = '$ApplicantID' AND a.Choice = '$i' AND a.RecordStatus = 0
							AND a.EnrolGroupID IN ('".implode("','",(array)$ClubOfCurrentYear)."')
					";
				}
            	$SuccessArr['Update_INTRANET_ENROL_GROUPSTUDENT'] = $libenroll->db_db_query($sql);
	         }
	         
	         
	         /*
	         $sql = "SELECT COUNT(*) FROM INTRANET_ENROL_GROUPSTUDENT WHERE StudentID = '$ApplicantID' AND Choice = $i";
	         $count = $libenroll->returnVector($sql);
	         if ($count[0]==0)
	         {
	             $sql = "INSERT INTO INTRANET_ENROL_GROUPSTUDENT (StudentID,GroupID,Choice,RecordStatus,DateInput,DateModified)
	                     VALUES ($ApplicantID,$choice,$i,0,now(),now())";
	         }
	         else
	         {
	             $sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT SET GroupID = $choice, DateModified = now()
	                     WHERE StudentID = $ApplicantID AND Choice = $i AND RecordStatus != 2";
	         }
	         */
	     }
	     else
	     {
	     	// is group member but no enrollment record => add back an enrollment record
	     	$sql = "SELECT COUNT(*) FROM INTRANET_ENROL_GROUPSTUDENT WHERE StudentID = '$ApplicantID' AND EnrolGroupID = '$choice' And RecordStatus = 2";
	     	$count = $libenroll->returnVector($sql);
	         
	         if ($count[0]==0)
	         {
	         	$sql = "INSERT INTO INTRANET_ENROL_GROUPSTUDENT (StudentID, GroupID, EnrolGroupID, EnrolSemester, Choice, RecordStatus,StatusChangedBy,DateInput,DateModified)
		                     VALUES ('$ApplicantID','$GroupID','$choice','$targetSemester','$i',2,'".$_SESSION['UserID']."',now(),now())";
		        $SuccessArr['Insert_INTRANET_ENROL_GROUPSTUDENT'] = $libenroll->db_db_query($sql);
	         }
	         else {
	         	$sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT SET Choice = '".$i."'  WHERE StudentID = '$ApplicantID' AND EnrolGroupID = '$choice' And RecordStatus = 2";
	         	$SuccessArr['UPDATE_INTRANET_ENROL_GROUPSTUDENT'] = $libenroll->db_db_query($sql);
	         }
	     }
	     $AddBefore = false;
     }     
}

if ($libenroll->mode == 1)
{
    //$sql = "UPDATE INTRANET_ENROL_STUDENT SET Max = $filled_count, DateModified = now() WHERE StudentID = $ApplicantID";
    //$SuccessArr['Update_INTRANET_ENROL_STUDENT'] = $libenroll->db_db_query($sql) or die(mysql_error());

}

if (in_array(false, $SuccessArr)) {
	$libenroll->RollBack_Trans();
	$returnMsgKey = 'UpdateUnsuccess'; 
}
else {
	$libenroll->Commit_Trans();
	$returnMsgKey = 'UpdateSuccess';
}


//intranet_closedb();
//header ("Location: index.php?msg=2&ChildID=$ApplicantID&sel_category=$sel_category");

$para = "msg=2&ChildID=$ApplicantID&sel_category=$sel_category&returnMsgKey=$returnMsgKey";
//$lurlparahandler = new liburlparahandler('e', $para, $enrolConfigAry['encryptionKey']);
//$paraEncrypted = $lurlparahandler->getParaEncrypted();
$paraEncrypted = getEncryptedText($para, $enrolConfigAry['encryptionKey']);

intranet_closedb();
header ("Location: index.php?p=$paraEncrypted");
?>