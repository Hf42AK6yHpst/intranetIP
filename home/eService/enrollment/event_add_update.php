<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();


$sel_category = IntegerSafe($_POST['sel_category']);
$maxwant = IntegerSafe($_POST['maxwant']);
$EnrolEventID = IntegerSafe($_POST['EnrolEventID']);
$ApplicantID = IntegerSafe($_POST['ApplicantID']);


$libenroll = new libclubsenrol('',$sel_category);

if($libenroll->Event_UseCategorySetting)
	$Category = $sel_category;

# array for applying activity
$StudentArr['StudentID'] = $ApplicantID;
$StudentArr['EnrolEventID'] = $EnrolEventID;
//$StudentArr['RecordType'] = 0;
$StudentArr['RecordStatus'] = 0;
$StudentArr['maxwant'] = $maxwant + 0;
$StudentArr['Category'] = $Category;


$LibUser = new libuser($UserID);

$isUserValid = false;
if ($LibUser->IsStudent()) {
	if ($ApplicantID == $_SESSION['UserID']) {
		$isUserValid = true;
	}
}
else if ($LibUser->IsParent()) {
	// is parent, get child ID
	$ChildArr = $LibUser->getChildrenList();
	$ChildrenUserIDAry = Get_Array_By_Key($ChildArr, 'StudentID');
	if (in_array($ApplicantID, $ChildrenUserIDAry)) {
		$isUserValid = true;
	}
}
if (!$isUserValid) {
	No_Access_Right_Pop_Up();
}


if ($libenroll->WITHIN_ENROLMENT_STAGE("activity"))
{
	# within enrol period => do checking
	$numApplied = $libenroll->GET_STUDENT_NUMBER_OF_APPLIED_EVENT($ApplicantID, $excludeRejectedActivity=1, $Category);
	if ($numApplied >= $libenroll->Event_defaultMax && $libenroll->Event_defaultMax!=0)
	{
		// number of appplied clubs > Default max. no. of groups to be selected by each student => quota exceeded
		$msg = 3;
	}
	else
	{
		// apply for the activity
		$libenroll->APPLY_ACTIVITY($StudentArr);
		$msg = 2;
	}	
}
else
{
	# out of enrol period => apply for the activity directly without checking
	$libenroll->APPLY_ACTIVITY($StudentArr);
	$msg = 2;
}


intranet_closedb();
header ("Location: event_index.php?msg=$msg&ChildID=$ApplicantID&sel_category=$sel_category");
?>