<?php
// using : 

/***************************
 *  Date:	2017-08-14 (Omas) #P110324 
 *  		Modified ReplySlip Related $sys_custom['eEnrolment']['ReplySlip']
 * 	Date:	2016-01-13	(Omas)
 *  Details: modified $ReloadAction-timeClashDisplay - improve not check time clash between 2 approved club
 * 			 modified Get_Student_Applied_Club_Info() - pass status parm to return record of waiting
 *	Date:	2015-02-10	(Omas)
 *	Details: fix- check different category club time clash 
 *  Date: 2012-12-19 Rita
 *  Details: add action reloadSurverySubmissionStatus
 * 
 ****************************/
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();


# Get data
$ReloadAction = trim(stripslashes($_POST['ReloadAction']));

$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();

if ($ReloadAction == 'studentChosenClubInfoRowData') {
	$enrolGroupId = $_POST['enrolGroupId'];
	$applicantId = $_POST['applicantId'];
	$choiceNumber = $_POST['choiceNumber'];
	
	$displayAry = array();
	$displayAry[] = $libenroll_ui->Get_Student_Application_Meeting_Date_Display($applicantId, $enrolGroupId, $choiceNumber);
	$displayAry[] = $libenroll_ui->Get_Student_Application_Fee_Display($applicantId, $enrolGroupId);
	$displayAry[] = $libenroll_ui->Get_Student_Application_Status_Display($applicantId, $enrolGroupId);
	$displayAry[] = $libenroll_ui->Get_Student_Application_Position_Display($applicantId, $enrolGroupId);
	$displayAry[] = $libenroll_ui->Get_Student_Application_Application_Date_Time_Display($applicantId, $enrolGroupId);
	$displayAry[] = $libenroll_ui->Get_Student_Application_Role_Display($applicantId, $enrolGroupId);
	$displayAry[] = $libenroll_ui->Get_Student_Application_Attendance_Display($applicantId, $enrolGroupId, $choiceNumber);
	
	
	if ($sys_custom['eEnrolment']['ApplicationExtraInfoTextarea']) {
		$displayAry[] = $libenroll_ui->Get_Student_Application_Applicant_Info_Display($applicantId, $enrolGroupId, $choiceNumber);
	}
	if($sys_custom['eEnrolment']['ReplySlip']){
		$ReplySlipRelation = $libenroll->getReplySlipEnrolGroupRelation('',$enrolGroupId);
		if(!empty($ReplySlipRelation)){
			$displayAry[] = '<a href="javasrcipt:void(0);" onclick="OnloadReplySlip('.$enrolGroupId.')">'.'<img src="/images/2009a/eOffice/icon_resultview.gif" border="0" align="absmiddle">'.'</a>';
		}else{
			$displayAry[] = '--';
		}
	}
	
	echo implode($enrolConfigAry['ajaxDataSeparator'], $displayAry);
}
elseif ($ReloadAction == 'timeClashDisplay') {
	$clashesChoiceNumberAry = array();
	if ($sys_custom['eEnrolment']['do_not_check_time_clash']) {
		// do not check time crash
	}
	else {
		$applicantId = $_POST['applicantId'];
		$enrolGroupIdText = $_POST['enrolGroupIdText'];
		$enrolGroupIdAry = explode(',', $enrolGroupIdText);
		$numOfClub = count($enrolGroupIdAry);
		
		$studentEnrolledClubInfoAry = $libenroll->GET_STUDENT_ENROLLED_CLUB_LIST($applicantId);
		$studentEnrolledEnrolGroupIdAry = Get_Array_By_Key($studentEnrolledClubInfoAry, 'EnrolGroupID');
		$numOfEnrolledClub = count($studentEnrolledEnrolGroupIdAry);
		unset($studentEnrolledClubInfoAry);
		
		$studentAppliedClubInfoAry = $libenroll->Get_Student_Applied_Club_Info($applicantId,'',$enrolConfigAry['EnrolmentRecordStatus_Waiting']);
		$studentAppliedEnrolGroupIdAry = Get_Array_By_Key($studentAppliedClubInfoAry, 'EnrolGroupID');
		$numOfAppliedClub = count($studentAppliedEnrolGroupIdAry);
		unset($studentAppliedClubInfoAry);
		
		$studentEnrolledActivityInfoAry = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($applicantId);
		$studentEnrolledActivityIdAry = Get_Array_By_Key($studentEnrolledActivityInfoAry, 'EnrolEventID');
		$numOfEnrolledActivity = count($studentEnrolledActivityIdAry);
		unset($studentEnrolledActivityInfoAry);
		
		for ($i=0; $i<$numOfClub; $i++) {
			$_choiceNumber_i = $i + 1;
			$_enrolGroupId_i = $enrolGroupIdAry[$i];
			
			if ($_enrolGroupId_i == '') {
				// if no selected club => no need checking
				continue;
			}
			
			if ($clashesChoiceNumberAry[$_choiceNumber_i]) {
				// checked clashed with other clubs / activities already => no need to check again
				continue;
			}
			
			for ($j=0; $j<$numOfClub; $j++) {
				$__choiceNumber_j = $j + 1;
				$__enrolGroupId_j = $enrolGroupIdAry[$j];
				if ($_choiceNumber_i == $__choiceNumber_j || $_enrolGroupId_i == $__enrolGroupId_j) {
					// same club => no need to do checking
					continue;
				}
				if(in_array($_enrolGroupId_i,$studentEnrolledEnrolGroupIdAry) && in_array($__enrolGroupId_j,$studentEnrolledEnrolGroupIdAry) ){
					// if 2 club are enrolled , no need to do checking #B91344 
					continue;
				}
				if ($libenroll->IS_CLUB_CRASH($_enrolGroupId_i, $__enrolGroupId_j)) {
					$clashesChoiceNumberAry[$_choiceNumber_i] = true;
					$clashesChoiceNumberAry[$__choiceNumber_j] = true;
					break;
				}
			}
			
			// check clash with enrolled club
			if ($clashesChoiceNumberAry[$_choiceNumber_i]) {
				// checked clashed already => no need to check activity clash
			}
			else {
				for ($j=0; $j<$numOfEnrolledClub; $j++) {
					$_enrolGroupId = $studentEnrolledEnrolGroupIdAry[$j];
					
					if ($_enrolGroupId_i == $_enrolGroupId) {
						// same club => no need to do checking
						continue;
					}
					if(in_array($_enrolGroupId_i,$studentEnrolledEnrolGroupIdAry) && in_array($_enrolGroupId,$studentEnrolledEnrolGroupIdAry) ){
						// if 2 club are enrolled , no need to do checking #B91344 
						continue;
					}
					if ($libenroll->IS_CLUB_CRASH($_enrolGroupId_i, $_enrolGroupId)) {
						$clashesChoiceNumberAry[$_choiceNumber_i] = true;
					}
				}
			}
			
			// check clash with applied club
			if ($clashesChoiceNumberAry[$_choiceNumber_i]) {
				// checked clashed already => no need to check activity clash
			}
			else {
				for ($j=0; $j<$numOfAppliedClub; $j++) {
					$_enrolGroupId = $studentAppliedEnrolGroupIdAry[$j];
					
					if ($_enrolGroupId_i == $_enrolGroupId) {
						// same club => no need to do checking
						continue;
					}
					if(in_array($_enrolGroupId_i,$studentEnrolledEnrolGroupIdAry) && in_array($_enrolGroupId,$studentEnrolledEnrolGroupIdAry) ){
						// if 2 club are enrolled , no need to do checking #B91344 
						continue;
					}
					if ($libenroll->IS_CLUB_CRASH($_enrolGroupId_i, $_enrolGroupId)) {
						$clashesChoiceNumberAry[$_choiceNumber_i] = true;
					}
				}
			}
			
			// check clash with enrolled activity
			if ($clashesChoiceNumberAry[$_choiceNumber_i]) {
				// checked clashed already => no need to check activity clash
			}
			else {
				for ($j=0; $j<$numOfEnrolledActivity; $j++) {
					$_enrolEventId = $studentEnrolledActivityIdAry[$j];
					
					if ($libenroll->IS_EVENT_GROUP_CRASH($_enrolGroupId_i, $_enrolEventId)) {
						$clashesChoiceNumberAry[$_choiceNumber_i] = true;
					}
				}
			}
		}	
	}
	
	echo implode(',', array_keys($clashesChoiceNumberAry));
	
}
elseif($ReloadAction == 'reloadSurverySubmissionStatus') {
	
	$userID = $_POST['userID'];
	$EnrolEventID = $_POST['EnrolEventID'];
		
	$eventInfoArr = $libenroll->GET_EVENTINFO($EnrolEventID);
	
	$surveyQuestionInfoArr = $eventInfoArr['Question'];
	
	if($surveyQuestionInfoArr){
		$userAnswer = $libenroll->returnUserSurveyAnswerInfo($userID, $EnrolEventID);
		if(count($userAnswer)>0){
			$userFilled = 'Filled';
		}else{
			$userFilled = 'NotYetFilled';
		}
	}else{
		$userFilled = 'NotFormForFill';
	}
	echo $userFilled;

}

intranet_closedb();
?>