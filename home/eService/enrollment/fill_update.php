<?php

/**************************************************
 * 	Date:		2012-12-12 (Rita)
 * 	Details:	Copy from eSurvey
 **************************************************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

$aStr = trim($aStr);

intranet_auth();
intranet_opendb();

$lform = new libform();
$lsurvey = new libsurvey();
$libenroll = new libclubsenrol();


# Check Access Right
if (!$libenroll->isUseEnrollment()) {
	die("You have no priviledge to access this page.");
}
elseif ((($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) || ($libenroll->IS_ENROL_MASTER($_SESSION['UserID']))) && !$LibUser->isStudent() && !$LibUser->isParent()){
	header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/");	
}else{
		
	if (!$libenroll->hasAccessRight($_SESSION['UserID']) || !$libenroll->enableActivitySurveyFormRight())
    {	
    	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit; 	
    }
}

$MODULE_OBJ['title'] = $i_Survey_FillSurvey;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

if($_POST['RecordType']){
	$RecordType = trim($_POST['RecordType']);
}else{
	$RecordType = trim($_GET['RecordType']);
}

if($_POST['ApplicantID']){
	$ApplicantID = trim($_POST['ApplicantID']);
}else{
	$ApplicantID = trim($_GET['ApplicantID']);
}


$lu = new libuser($ApplicantID);

if($RecordType=='A'){
	if($_POST['eEnrolRecordID']){
		$eEnrolRecordID = trim($_POST['eEnrolRecordID']);
	}else{
		$eEnrolRecordID = $_GET['eEnrolRecordID'];
	}
$enrollEventInfoArr = $libenroll->GET_EVENTINFO($eEnrolRecordID);
	
}elseif($RecordType=='C'){
	
}



# Update survey
$sql = "SELECT Answer FROM INTRANET_ENROL_SURVEYANSWER WHERE eEnrolRecordID = '$eEnrolRecordID' AND UserID = '$ApplicantID'";
$answer = $libenroll->returnVector($sql);

if ($answer[0]=="" && $aStr!="")
{
    $username = $lu->getNameForRecord();

	$sql = "INSERT INTO INTRANET_ENROL_SURVEYANSWER(eEnrolRecordID, UserID, UserName, Answer, RecordType, DateInput, InputBy, DateModified, ModifiedBy)
			VALUES ('$eEnrolRecordID', '$ApplicantID', '$username', '$aStr', '$RecordType', now(), '$UserID', now(), '$UserID' )
			";

    $libenroll->db_db_query($sql);
}
else
{
	if(!$lsurvey->NotAllowReSign)
	{
		$sql = "UPDATE INTRANET_ENROL_SURVEYANSWER SET Answer='$aStr', DateModified=now(), ModifiedBy='$UserID' where eEnrolRecordID='$eEnrolRecordID' and UserID='$ApplicantID'";
		$libenroll->db_db_query($sql);
	}
	else{
    	$aStr = $answer[0];
	}
}

# Questions
$queString = $enrollEventInfoArr['Question'];
$queString = str_replace('"','&quot;',$queString);
$queString = str_replace("\n",'<br>',$queString);
$queString = str_replace("\r",'',$queString);
$aStr = str_replace('"','&quot;',$aStr);
$aStr = str_replace("\n",'<br>',$aStr);
$aStr = str_replace("\r",'',$aStr);


?>
<br />

<table width=95% cellspacing=0 cellpadding=5 border=0>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$eEnrollment['add_activity']['act_name']?></td>
	<td><?=$enrollEventInfoArr['EventTitle']?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$eEnrollment['add_activity']['app_period']?></td>
	<td><?=$enrollEventInfoArr['ApplyStartTime'] . '<br />'. $Lang['General']['To'] . '<br />' . $enrollEventInfoArr['ApplyEndTime'] ?></td>
</tr>

<tr>
	<td colspan=2>
		<form name="ansForm" method="post" action="update.php">
		        <input type="hidden" name="qStr" value="">
		        <input type="hidden" name="aStr" value="">
		        <input type="hidden" name="eEnrolRecordID" value="<?=$eEnrolRecordID?>">
				<input type="hidden" name="RecordType" value="<?=$RecordType?>">
				<input type="hidden" name="ApplicantID" value="<?=$ApplicantID?>">
		</form>
		
		<?=$i_Survey_Result?>
		<hr>

		
		<? if($lsurvey->NotAllowReSign) {?>
			<script language="javascript" src="/templates/forms/form_view.js"></script>
			
			<script language="Javascript">
			var replyslip = '<?=$i_Notice_ReplySlip?>';
			
			var DisplayQuestionNumber = '<?=$enrollEventInfoArr['DisplayQuestionNumber']?>';
			myQue = "<?=$queString?>";
			myAns = "<?=$aStr?>";
			document.write(viewForm(myQue, myAns));
			</SCRIPT>
		<? } else {
			$userAnswer =  $libenroll->returnUserSurveyAnswerInfo($ApplicantID, $eEnrolRecordID);
			if($userAnswer)
			{
				$ansstr = $userAnswer[0]['Answer'];
				$temp_ans =  str_replace("&amp;", "&", $ansstr);
				$ansString = $lform->getConvertedString($temp_ans);
				$ansString =trim($ansString)." ";	
			}
			?>
			<script language="javascript" src="/templates/forms/form_edit.js"></script>
			
			<script language="Javascript">
			<?=$lform->getWordsInJS()?>
		
			var replyslip = '<?=$Lang['eSurvey']['ReplySlip']?>';
			var DisplayQuestionNumber = '<?=$enrollEventInfoArr['DisplayQuestionNumber']?>';
	
			//background_image = "/images/layer_bg.gif";
			
			var sheet= new Answersheet();
			// attention: MUST replace '"' to '&quot;'
			sheet.qString="<?=$queString?>";
			sheet.aString="<?=$ansString?>";
			//edit submitted application
			sheet.mode=1;
			sheet.answer=sheet.sheetArr();
			//sheet.templates=form_templates;
			document.write(editPanel());
			</script>
			<SCRIPT LANGUAGE=javascript>
			function copyback()
			{
			 finish();
			 document.form1.qStr.value = document.ansForm.qStr.value;
			 document.form1.aStr.value = document.ansForm.aStr.value;
			}
			function validSubmit()
			{
			 if (!need2checkform || formAllFilled)
			 {
			      return true;
			 }
			 else //return true;
			 {
			      alert("<?=$i_Survey_alert_PleaseFillAllAnswer?>!");
			      return false;
			 }
			}
			</SCRIPT>
		<? } ?>
		
		
		<hr>
</td>
</tr>
<tr>
	<td colspan="2" align="center">
	<form name="form1" action="fill_update.php" method="post" onsubmit="return validSubmit()">
	<? if(!$lsurvey->NotAllowReSign) {?>
		<?=$linterface->GET_BTN($button_submit, "submit", "copyback();") ?>
		<input type="hidden" name="qStr" value="">
		<input type="hidden" name="aStr" value="">
		<input type="hidden" name="eEnrolRecordID" value="<?=$eEnrolRecordID?>">
		<input type="hidden" name="RecordType" value="<?=$RecordType?>">
		<input type="hidden" name="ApplicantID" value="<?=$ApplicantID?>">
			
	<? } ?>
		<?=$linterface->GET_BTN($button_print, "button", "window.print();") ?>
		<?=$linterface->GET_BTN($button_close, "button", "window.close();") ?>
	</form>
	</td>
</tr>
</table>


	<? if($row) { ?>
		<script language="javascript">
		<!--
			var x=opener.document.getElementById('ContentTable').rows;
			var y=x[<?=$row?>].cells;
			y[4].innerHTML = "<?=$Lang['eSurvey']['Answered']?>";
		//-->
		</script>
     <? } ?>


<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
