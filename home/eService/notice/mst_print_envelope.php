<?php
# using: yat
###############################################
#  請用utf-8 save file,  function retrieveConductPotentialsReport() >>> 標楷體
###############################################
##### Change Log [Start] #####
#
#	Date:	2014-01-10	YatWoon
#		modified layout (client provide new layout on 20Dec2013)
#
#	Date:	2013-10-03	YatWoon
#			remove school info 

#	Date:	2013-07-18	YatWoon
#			customization [Case#2013-0520-1008-41072] with flag $sys_custom['eDiscipline']['MST_envelope']
#
##### Change Log [End] #####

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php"); 
//include_once($PATH_WRT_ROOT."includes/libform.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lnotice = new libnotice();
$ldiscipline = new libdisciplinev12();
$ldiscipline_cust = new libdisciplinev12_cust();
//$lform = new libform();

if($NoticeIDStr)
{
	$NoticeIDStr = substr($NoticeIDStr, 0,-1);
	$result = explode(",",$NoticeIDStr);
}
else
{
	# retrieve NoticeID
	//$sign_status_cond = $sign_status ? "b.RecordType is not null" : "b.RecordType is NULL";
	
	if($sign_status!=-1 && $sign_status!="")
	{
		$sign_status_cond = " and " . ($sign_status ? "b.RecordType is not null" : "b.RecordType is NULL");
	}
	if($print_status!=-1 && $print_status!="")
	{
		$print_status_cond = " and " . ($print_status ? "b.PrintDate is not null" : "b.PrintDate is NULL");
	}

	if ($status != "")
		$recordstatus_cond = " and a.RecordStatus=$status";

	$sql = "select 
		a.NoticeID
		from 
			INTRANET_NOTICE as a 
			left join INTRANET_NOTICE_REPLY as b on (b.NoticeID = a.NoticeID)
		where
			a.DateStart >= '$StartDate' and a.DateStart <= '$EndDate'
			$recordstatus_cond
			and a.Module = '$thisModule'
			$sign_status_cond
			$print_status_cond
		";
	$result = $lnotice->returnVector($sql);
}

# need check access right
$valid = true;
$editAllowed = true;

?>
 
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?
$count = 0;
foreach($result as $k=>$id)
{
	#### data
	# only print envelope for punishment records only
	$PunishmentRecordData = $ldiscipline_cust->returnPunishmentDataByNoticeID($id);
	if(empty($PunishmentRecordData))	continue;		
	
	$lnotice = new libnotice($id);
	$StudentID = str_replace("U","",$lnotice->RecipientID);
	$lu = new libuser($StudentID);
	$stu_parent_info = $lu->getParent($StudentID);

	//$this_classname = $stu_parent_info[0]['ClassName'];
	//$this_stu_name = $stu_parent_info[0]['StudentName'];
	//$this_classname = $lu->ClassName;
	//$this_stu_name = $lu->StandardDisplayName;
	$this_stu_name = $lu->ChineseName;	# requested by client, display student Chinese name
	$this_parent_name = $stu_parent_info[0]['OutputName'];
	
	# retrieve SchoolYear info 
	$this_record_date = $PunishmentRecordData['RecordDate'];
	$year_info = getAcademicYearAndYearTermByDate($this_record_date);
	$start_year = substr(getStartDateOfAcademicYear($year_info['AcademicYearID']), 2,2);
	$display_year_info = $start_year . ($start_year+1);
	$this_remark = $PunishmentRecordData['RecordNumber'] . "/" . $display_year_info;
	$record_remark = $PunishmentRecordData['Remark'];
	
	# retrieve English Class Name (requested by client, display English Class Name)
	$this_year_id = $PunishmentRecordData['AcademicYearID'];
	$this_classname = $ldiscipline_cust->returnClassNameEn($StudentID, $this_year_id);
	
	########
	
	$count++;
	/*
 	if($count < sizeof($result)-1)
	{
		$breakStyle = 'style="page-break-after:always;"';	
	}
 	else
	{
 		$breakStyle = "";
		}
		*/
	?>
	
	<? /*
	width:214mm;height:90mm
	height="90"
	*/?>
	
	<table border="0" cellpadding="0" cellspacing="0" align="left" style="width:214mm;height:80mm">
		<tr>
		<? /* ?>
			<td width="280" valign="top" >
		
				<span style="font-size:14px; font-family:'標楷體'">聖公會莫壽增會督中學</span><br>
				<span style="font-size:10px; font-family:'Times New Roman'">SHENG KUNG HUI<br>BISHOP MOK SAU TSENG SECONDARY SCHOOL</span><br>
				<span style="font-size:10px; font-family:'Times New Roman'">26 Wan Tau Kok Lane, Tai Po, N.T.</span><br>
				<span style="font-size:10px; font-family:'Times New Roman'">Tel: 26567804-5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fax: 26507265</span><br>
				<span style="font-size:10px; font-family:'Times New Roman'">Website: www.mst.edu.hk</span>
		
			</td>
			<? */ ?>
			<? /* ?>
			<td valign="bottom">
				<span style="font-size:21px; font-family:'標楷體'">送呈</span><br><br>
				<span style="font-size:21px; font-family:'標楷體'"><?=$this_classname?></span><span style="font-size:21px; font-family:'標楷體'"> 班 <?=$this_stu_name?> 家長</span><br><br>
				<span style="font-size:21px; font-family:'標楷體'"><?=$this_parent_name?> 先生/女士 收啟</span><br><br><br>
			</td>
			<? */ ?>
			
			<td valign="middle" height="100%" align="center">
				<br><br><br><br>
				<span style="font-size:19px; font-family:'標楷體'"><?=$this_classname?> 班 <?=$this_stu_name?> 家長收啟</span>
			</td>
			
		</tr>
		<tr><td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" height="50">
			<tr>
			<td width="30">&nbsp;</td>
			<td valign="top">
				<span style="font-size:13px; font-family:'標楷體'"><?=$this_remark?></span><br>
				<span style="font-size:13px; font-family:'標楷體'"><?=$PunishmentRecordData['ItemText']?><?=$record_remark?></span><br>
				<span style="font-size:13px; font-family:'標楷體'"><?=$PunishmentRecordData['ProfileMeritType']?></span>
			</td></tr>
			</table>
			</td>
		</tr>
	</table>
	
	
<? 
	if($count <= sizeof($result)-1)
	{
		if(strpos($HTTP_USER_AGENT, "MSIE"))	
		{
			echo "<p class='breakhere'></p>";		# work in IE10
		}
		else
		{
			echo "<div style='page-break-after:always;'>&nbsp;</div>";			# work in Chrome
		}
	}
}

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
