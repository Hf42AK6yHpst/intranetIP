<?php
// Using :

############ Change Log [Start]
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			updated access right checking
#
#   Date:   2020-07-31  Bill    [2020-0522-1550-16315]
#           show "Student Subsidy Identity Settings" col only for payment notice
#
#   Date:   2020-04-16  Bill    [2020-0310-1051-05235]
#           Special notice > access checking for teacher    ($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'])
#
#	Date:	2020-03-12	YatWoon
#			Add $special_feature['eNotice']['DisableSubsidyIdentityName'] flag checking [#F180463]
#			Hide Student Subsidy Identity Name
#
#   Date:   2019-12-11  Ray
#           Added - Student Subsidy Identity Name
#
# 	Date:	2019-10-29  Carlos
#			Display the late "*" only if status not signed and has signer name (tried sign) and real last modify time later than notice end date.
#
#	Date:	2019-10-11	Philips
#			Fix - back button check $class changed into $_GET['class']
#
#	Date:	2019-10-04	Philips
#			Added back button
#
#	Date:	2019-08-01  Carlos
#			For Payment Notice, added [Last modify answer person] and [Last modify answer time].
#
#	Date:	2019-06-14  Carlos
#			Added [Pay at school] column.
#			Added filter to filter out [Unpaid]/[Pay at school] records.
#
#   Date:   2019-06-06  (Bill)
#           add CSRF token in html form + pass token to ajax page
#
#	Date:	2019-05-29  Carlos
#			Added flag $sys_custom['PaymentNoticeDoNotDisplayFailPaySignerInStat'] to do not display signed but failed to pay signer data.
#			Deprecated the old flag $sys_custom['PaymentNoticeDisplayFailPaySignerInStat'] which is reverse logic of the new flag.
#
#   Date:   2019-05-27  Bill    [2019-0522-1421-59206]
#           fixed cannot view data in stat.php if class name with whitespaces
#
#	Date:	2019-05-17  Carlos
#			Added flag $sys_custom['PaymentNoticeDisplayFailPaySignerInStat'] to display signed but failed to pay students.
#
#   Date:   2019-04-30  Bill
#           prevent SQL Injection + Cross-site Scripting
#
#	Date:	2019-04-10  Carlos
#			Cater more question types for Payment Notice.
#
#	Date:	2019-02-26  Carlos
#			Cater display of signer for Payment Notice that failed to pay but signed, display as unsigned.
#
#	Date:	2017-11-03	Bill	[2017-1102-1332-42207]
#			fixed: All Students > Answer Display - Question Type: One payment from a few categories
#
#	Date:	2017-01-18 	Bill	[DM#3121]
#			Handle display special characters in answer
#
#	Date:	2016-03-10 [Bill]	[2016-0310-0911-58085]
#			Added Export (Unsigned Notice List) button
#
#	Date:	2016-03-10 [Bill]	[2016-0301-1447-14206]
#			for All Students, display Class Name & Class Number
#
#	Date:	2015-11-30 [Kenneth] [2015-1016-1125-38207]
#			Show option name in UI and print mode ($sys_custom['eNotice']['showOptionNumber'] != true)
#
#	Date:	2015-07-20 [Ivan] (ip.2.5.6.7.1)
#			Added sms confirmation page logic
#
#	Date:	2015-06-05	Omas
#			Add Export Mobile Button
#
#	Date:	2015-05-11	Bill
#			Fixed: hide Send Push Message button if notice type is Student-signed Notice
#
#	Date:	2015-04-15	Bill
#			Improved: allow eNotice PIC to access and send push message [2015-0323-1602-46073]
#			Improved: allow class teacher to send push message only if "Allow class teacher to send push message to parents." - true [2015-0303-1237-15073]
#
#	Date:	2015-03-10 [Ivan] (ip.2.5.6.5.1)
#			Added to show auth code logic for admin view
#
#	Date:	2015-03-04 Roy
#			remove checking of not exist variable $lnotice->IssuerUserID
#
#	Date:	2014-12-23 Bill
#			pass $type to stat.php & ensure when $type = 1, only Own Class data can be shown [2014-1217-1446-17207]
#
#	Date:	2014-09-30 Bill
#			allow access when "staffview" is turned on
#
#	Date:	2014-09-08 YatWoon
#			cater with classname is NULL or empty
#
#	Date:	2014-02-26 YatWoon
#			improved: cater 1 staff with multiple class [Case#U58796]
#
#	Date:	2014-01-03 Ivan
#			added $plugin['eClassApp'] logic
#
#	Date:	2013-11-18 Roy
#			 check sms admin $_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"]
#				& flag $sys_custom["notify_sms_disabled"] to control sms options
#
#	Date:	2013-10-08 Roy
#			added thickbox to show push message method options
#
#	Date:	2010-08-04	YatWoon
#			update layout to IP25 standard
#
############ Change Log [End] 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

################################
# Pass in:
#	$NoticeID
#   $type (1: view own class)
################################

### Handle SQL Injection + XSS [START]
$NoticeID = integerSafe($NoticeID);
$all = integerSafe($all);
$type = integerSafe($type);
### Handle SQL Injection + XSS [END]

$MODULE_OBJ['title'] = $i_Notice_ViewResult;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$Message = standardizeFormPostValue($_POST['Message']);
$directSend = standardizeFormPostValue($_POST['directSend']);
$sendMethod = standardizeFormPostValue($_POST['sendMethod']);

$lnotice = new libnotice($NoticeID);
$lu = new libuser($UserID);
$lclass = new libclass();
$lpayment = new libpayment();

// [2020-0310-1051-05235] Special notice > access checking for teacher
$allowTeacherAccess = true;
if($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'] && $lnotice->SpecialNotice && $lu->isTeacherStaff())
{
    $allowTeacherAccess = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $lnotice->isNoticePIC($NoticeID);
}

// if (!$lu->isTeacherStaff())
if (!$lu->isTeacherStaff() || !$allowTeacherAccess)
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT($i_general_no_access_right);
    exit;
}
$stype = $type;

// [2020-0604-1821-16170]
// [2015-0323-1602-46073] - Target Type - not Applicable students -> add checking if user is not current eNotice PIC
//if ($type==1 || (!$lnotice->hasViewRight() && !$lnotice->staffview && $lnotice->IssuerUserID != $UserID))
$isStaffView = strtoupper($lnotice->Module) == "PAYMENT" ? $lnotice->payment_staffview : $lnotice->staffview;
// if ($type==1 || (!$lnotice->hasViewRight() && !$lnotice->staffview && $lnotice->IssuerUserID != $UserID && !$lnotice->isNoticePIC($NoticeID)))
if ($type==1 || (!$lnotice->hasViewRight() && !$isStaffView && $lnotice->IssuerUserID != $UserID && !$lnotice->isNoticePIC($NoticeID)))
{
    $class = $lclass->returnHeadingClass($UserID,1);
    if(is_array($class))
    {
        $class_str = "";
        $dest = "";
        for($i=0;$i<sizeof($class);$i++)
        {
            $class_str .= $dest . $class[$i][0];
            $dest = ",";
        }
    }
    $class = $class_str;
    
    if(empty($class))
    {
        include_once($PATH_WRT_ROOT."includes/libaccessright.php");
        $laccessright = new libaccessright();
        $laccessright->NO_ACCESS_RIGHT_REDIRECT($i_general_no_access_right);
        exit;
    }
}

$is_payment_notice = strtoupper($lnotice->Module) == "PAYMENT";

if($is_payment_notice)
{
	$payment_status_selection_data = array();
	$payment_status_selection_data[] = array('',$Lang['General']['All']);
	$payment_status_selection_data[] = array('-1',$Lang['eNotice']['PaymentNoticeUnpaid']);
	$payment_status_selection_data[] = array('-2',$Lang['eNotice']['PaymentNoticePayAtSchool']);
	
	$payment_status = in_array($payment_status,array('','-1','-2'))? $payment_status : '';
	$only_show_unpaid = $payment_status == '-1';
	$only_show_payatschool = $payment_status == '-2';
	
	$payment_status_selection = $linterface->GET_SELECTION_BOX($payment_status_selection_data, ' id="payment_status" name="payment_status" onchange="refreshForm();" ', '', $payment_status);
	
	$modify_answer_records = $lnotice->getReplySlipModifyAnswerRecords(array('NoticeID'=>$NoticeID,'GetModifierUserInfo'=>true));
	$studentIdToModifyAnswerRecords = BuildMultiKeyAssoc($modify_answer_records,'StudentID');
}

$questions = $lnotice->splitQuestion($lnotice->Question);

$signedCount = 0;
$totalCount = 0;
$singleTable = false;

// Applicable students & Signed/Total
if ($lnotice->RecordType == 4 && $type != 1)
{
    $class = "";
    $all = 1;
    $data = $lnotice->returnTableViewAll();
    $singleTable = true;
}
else
{
    // [2020-0604-1821-16170]
    // Check if Class Name is valid
    // if(($class == '' || $class == 'NULL') || $type == 1 || (!$lnotice->hasViewRight() && !$lnotice->staffview && $lnotice->IssuerUserID != $UserID && !$lnotice->isNoticePIC($NoticeID))) {
    if(($class == '' || $class == 'NULL') || $type == 1 || (!$lnotice->hasViewRight() && !$isStaffView && $lnotice->IssuerUserID != $UserID && !$lnotice->isNoticePIC($NoticeID))) {
        // do nothing
    }
    else {
        $noticeClassList = $lnotice->returnClassList();
        $noticeClassList = array_values(array_filter($noticeClassList));
        if(!in_array($class, (array)$noticeClassList)) {
            include_once($PATH_WRT_ROOT."includes/libaccessright.php");
            $laccessright = new libaccessright();
            $laccessright->NO_ACCESS_RIGHT_REDIRECT($i_general_no_access_right);
            exit;
        }
    }
    
    //if ($class != "")
    if(!empty($class))
    {
    	$data = $lnotice->returnTableViewByClass($class);
        $singleTable = true;
    }
    else 
    {
        $singleTable = false;
    }
}

$unsigned_StudentIDArr = array();
$SelectedClass = $class;
if ($singleTable)
{
    $totalCount = sizeof($data);
    if (sizeof($data)!=0)
    {
        //$x .= "<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center'>";
		$x .= "<table class='common_table_list view_table_list_v30'>";
        $x .= "<tr>"; 
		$x .= "<th>$i_ClassName</th>";
		$x .= "<th>$i_ClassNumber</th>";
        $x .= "<th>$i_UserStudentName</th>";

        // [2020-0522-1550-16315]
		//if(!$special_feature['eNotice']['DisableSubsidyIdentityName'])
        if($is_payment_notice && !$special_feature['eNotice']['DisableSubsidyIdentityName'])
		{
			$x .= "<th>".$Lang['ePayment']['SubsidyIdentityName']."</th>";
		}

        for ($i=0; $i<sizeof($questions); $i++)
        {
        	if (!in_array($questions[$i][0],array(6,16)))
            {
				$t = "<table border='0' cellpadding='5' cellspacing='0' class='attendancestudentphoto'>";
                $t .="<tr>";
                
				$t .="<td class='tabletext'>".$questions[$i][1]."</td>";
				$t .="</tr>";
                $t .="</table>";
                $tooltipQ = makeTooltip($t);
                
                ######################
                #	30-11-2015	Kenneth
                #	Replace Option 1, Option 2.... to question itself
                ######################
                if($sys_custom['eNotice']['showOptionNumber']){
                	$x .= "<th><a href='#' $tooltipQ>$i_Notice_Option ".($i+1)."</a></th>";
                }
                else{
                	$x .= "<th>".$questions[$i][1]."</th>";
                }
             }
        } 
        $x .= "<th>$i_Notice_Signer".($is_payment_notice?"<br />(".$Lang['eNotice']['LastModifyAnswerPerson'].")":"")."</th>";
        $x .= "<th>$i_Notice_SignedAt".($is_payment_notice?"<br />(".$Lang['eNotice']['LastModifyAnswerTime'].")":"")."</th>";
        
        if ($sys_custom['eClassApp']['authCode']) {
        	$x .= "<th>".$Lang['eNotice']['SignedByAuthCode']."</th>";
        }
        
        if(strtoupper($lnotice->Module) == "PAYMENT")
		{
			$x .= "<th>".$Lang['eNotice']['FieldTitle']['NoOfItemNeedToPaid']."</th>";
			$x .= "<th>".$Lang['eNotice']['FieldTitle']['NoOfItemPaidSuccessfully']."</th>";
			$x .= "<th>".$Lang['eNotice']['PaymentNoticePayAtSchool']."</th>";
		}
        $x .= "</tr>\n";
        
        for ($i=0; $i<sizeof($data); $i++)
        {
        	if ($lnotice->RecordType == 4)
			{
				list($sid,$name,$classNo,$type,$status,$answer,$signer,$last,$class_name,$websamsRegNo,$signerAuthCode,$studentUserLogin,$student_status,$notice_payment_method) = $data[$i];
		 	}
		 	else
		 	{
			 	list($sid,$name,$classNo,$type,$status,$answer,$signer,$last,$class_name,$websamsRegNo,$signerAuthCode,$studentUserLogin,$student_status,$notice_payment_method) = $data[$i];
		 	}
		 	$display_signer = $signer;
		 	$last_modify_time = $last;
		 	
		 	if ($status == 2)
            {
                $css = ($type==1 ? "" : "row_avaliable");
                $signedCount++;
            }
            else
            {
                $css = "row_suspend";
                if($sys_custom['PaymentNoticeDoNotDisplayFailPaySignerInStat']){
                	$display_signer = "";
                }
                $last = "$i_Notice_Unsigned";
                $unsigned_StudentIDArr[] = $sid;
            }
            
            $display_current_row = true;
            
            $y = '';
            
            $y .= "<tr class=$css><td class='$css' align = 'left'>$class_name</td>";
			$y .= "<td class='$css' align = 'left'>$classNo</td>";
			if(strtoupper($lnotice->Module) == "PAYMENT")
			{
				$y .= "<td class='$css'><a class='tablebluelink' href='eNoticePayment_sign.php?NoticeID=$NoticeID&StudentID=$sid'>$name</a></td>";
			}
			else
			{
				$y .= "<td class='$css'><a class='tablebluelink' href='sign.php?NoticeID=$NoticeID&StudentID=$sid'>$name</a></td>";
			}

            // [2020-0522-1550-16315]
            //if(!$special_feature['eNotice']['DisableSubsidyIdentityName'])
            if($is_payment_notice && !$special_feature['eNotice']['DisableSubsidyIdentityName'])
			{
				$student_subsidy_name = '';
				$student_subsidy_identity_records = $lpayment->getSubsidyIdentityStudents(array('StudentID'=>$sid,'StartDateWithinEffectiveDates'=>$lnotice->DateStart,'EndDateWithinEffectiveDates'=>$lnotice->DateEnd));
				if(count($student_subsidy_identity_records)>0){
					$student_subsidy_identity_id = $student_subsidy_identity_records[0]['IdentityID'];
					$student_subsidy_identity_records = $lpayment->getSubsidyIdentity(array('IdentityID'=>$student_subsidy_identity_id));
					if(count($student_subsidy_identity_records)>0) {
						$student_subsidy_name = $student_subsidy_identity_records[0]['IdentityName'];
					}
				}
				$y .= "<td class='$css'>$student_subsidy_name</td>";
			}

            for ($j=0; $j<sizeof($questions); $j++)
            {
            	/*if ($questions[$j][0]==9)
            	{
            	 	// [DM#3121] handle special characters 
            	 	//$x .= "<td class='$css'>".$questions[$j][2][$answer[$j]]." &nbsp;</td>";
            	 	$x .= "<td class='$css'>".intranet_undo_htmlspecialchars($questions[$j][2][$answer[$j]])." &nbsp;</td>";
	            }
	            else */ if (!in_array($questions[$j][0],array(6,16)))
	            {
            	 	// [DM#3121] handle special characters 
					//$x .= "<td class='$css'>".$answer[$j]." &nbsp;</td>";
					$y .= "<td class='$css'>".intranet_undo_htmlspecialchars($answer[$j])." &nbsp;</td>";
				}
				else
				{
				 	// do nothing
				}
            }
            
            # check late submit
            $LateRemark = ($status != 2 && $display_signer!="" && $last_modify_time != "" && $last_modify_time > $lnotice->DateEnd . " 23:59:59") ? "*" : "";
            $modify_person = $is_payment_notice && isset($studentIdToModifyAnswerRecords[$sid])? '<br />('.$studentIdToModifyAnswerRecords[$sid]['ModifierUserName'].')' : '';
            $modify_time = $is_payment_notice && isset($studentIdToModifyAnswerRecords[$sid])? '<br />('.$studentIdToModifyAnswerRecords[$sid]['InputDate'].')' : '';
            $y .= "<td class='$css'>$display_signer".$modify_person."&nbsp;</td><td class='$css'>$last".$modify_time." $LateRemark</td>";
            
            if ($sys_custom['eClassApp']['authCode']) {
            	$signerAuthCode = ($signerAuthCode=='')? '&nbsp;' : $signerAuthCode;
            	$y .= "<td class='$css'>$signerAuthCode</td>";
            }
            
            if(strtoupper($lnotice->Module) == "PAYMENT")
     		{
     			$sql = "SELECT ItemID FROM PAYMENT_PAYMENT_ITEM WHERE NoticeID = '$NoticeID'";
     			$arrItemIDs = $lnotice->returnVector($sql);
     			//echo $sql.'<br>';
     			if(sizeof($arrItemIDs)>0){
     				$targetItemID = implode(",",$arrItemIDs);
     			}
     			
            	$sql = "SELECT COUNT(*) FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE STUDENTID = '$sid' AND ItemID IN ($targetItemID)";
            	//echo $sql.'<br>';
            	$NoOfItemNeedToPaid = $lnotice->returnVector($sql);
            	
            	$sql = "SELECT COUNT(*) FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE STUDENTID = '$sid' AND ItemID IN ($targetItemID) AND RecordStatus = 1";
            	$NoOfItemPaidSuccessfully = $lnotice->returnVector($sql);
            	
            	if(sizeof($answer)>0)
            	{
            		if($only_show_unpaid)
            		{ 
            			if($NoOfItemNeedToPaid[0] > 0 && $NoOfItemPaidSuccessfully[0] < $NoOfItemNeedToPaid[0]){
            				$display_current_row = true;
            			}else{
            				$display_current_row = false;
            			}
            		}
            		
            		$y .= "<td class='$css'>".$NoOfItemNeedToPaid[0]."</td>";
            		$y .= "<td class='$css'>".$NoOfItemPaidSuccessfully[0]."</td>";
            		$y .= "<td class='$css'>".($notice_payment_method=='PayAtSchool'?$Lang['General']['Yes']:$Lang['General']['No'])."</td>";
            	}
            	else
            	{
            		if($only_show_unpaid){
            			$display_current_row = false;
            		}
            		
            		$y .= "<td class='$css'> - </td>";
            		$y .= "<td class='$css'> - </td>";
            		$y .= "<td class='$css'> - </td>";
            	}
            	
            	if($only_show_payatschool){
            		$display_current_row = $notice_payment_method == 'PayAtSchool';
            	}
            }
            $y .= "</tr>\n";
            
            if($display_current_row){
            	$x .= $y;
            }
        }
        
        $x .= "</table>\n";
    }
    else
    {
        $NoMsg = ($all==1? $i_Notice_NoStudent:$i_Notice_NotForThisClass);
        $x .= "<table width='100%' border='0' cellpadding='1' cellspacing='1'>\n";
        $x .= "<tr><td align='center' class='tabletext'>$NoMsg</td></tr>\n";
        $x .= "</table>\n";
    }
}
else
{
    $classes = $lnotice->returnTableViewGroupByClass();
    for ($pos=0; $pos<sizeof($classes); $pos++)
    {
		list($classname,$data) = $classes[$pos];
        $totalCount += sizeof($data);
        if (sizeof($data)!=0)
        {
        	$classname_display = $classname == "" || $classname == "NULL" || $classname=="0" ? "--": $classname ;
            $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'>";
            //$x .= "<table class='common_table_list view_table_list'>";
            $x .= "<tr><td>". $linterface->GET_NAVIGATION2($classname_display) ."</td></tr>";
            $x .= "</table>";
            
//          $x .= $linterface->GET_NAVIGATION2($classname);
//          $x .= "<table width='90%' border='01' cellspacing='0' cellpadding='5' align='center'>";
			$x .= "<table class='common_table_list view_table_list_v30'>";
			$x .= "<th>$i_ClassName</th>";
			$x .= "<th>$i_ClassNumber</th>";
            $x .= "<th>$i_UserStudentName</th>";

            // [2020-0522-1550-16315]
            //if(!$special_feature['eNotice']['DisableSubsidyIdentityName'])
            if($is_payment_notice && !$special_feature['eNotice']['DisableSubsidyIdentityName'])
			{
				$x .= "<th>".$Lang['ePayment']['SubsidyIdentityName']."</th>";
			}
			for ($i=0; $i<sizeof($questions); $i++)
            {
            	if (!in_array($questions[$i][0],array(6,16)))
                {
                	$t = "<table border='01' cellpadding='5' cellspacing='0' class='attendancestudentphoto'>";
                    $t .="<tr>";
        				$t .="<td class='tabletext'>".$questions[$i][1]."</td>";
        			$t .="</tr>";
	                $t .="</table>";
	                $tooltipQ = makeTooltip($t);
                
	                ######################
	                #	30-11-2015	Kenneth
	                #	Replace Option 1, Option 2.... to question itself
	                ######################
	                if($sys_custom['eNotice']['showOptionNumber']){
	                	$x .= "<th><a href='#' $tooltipQ>$i_Notice_Option ".($i+1)."</a></th>";
	                }
	                else{
	                	$x .= "<th>".$questions[$i][1]."</th>";
	                }
             	}
            }
            $x .= "<th>$i_Notice_Signer".($is_payment_notice?"<br />(".$Lang['eNotice']['LastModifyAnswerPerson'].")":"")."</th><th>$i_Notice_SignedAt".($is_payment_notice?"<br />(".$Lang['eNotice']['LastModifyAnswerTime'].")":"")."</th>";
            
         	if(strtoupper($lnotice->Module) == "PAYMENT")
			{
				$x .= "<th>".$Lang['eNotice']['FieldTitle']['NoOfItemNeedToPaid']."</th>";
				$x .= "<th>".$Lang['eNotice']['FieldTitle']['NoOfItemPaidSuccessfully']."</th>";
				$x .= "<th>".$Lang['eNotice']['PaymentNoticePayAtSchool']."</th>";
			}
            $x .= "</tr>\n";
            
            for ($i=0; $i<sizeof($data); $i++)
            {
            	//list ($sid,$name,$type,$status,$answer,$signer,$last) = $data[$i];
                list ($sid,$name,$type,$status,$answer,$signer,$last,$class_num, $regno, $signerAuthCode, $studentUserLogin, $student_status, $notice_payment_method) = $data[$i];
                $display_signer = $signer;
                if ($status == 2)
                {
                	$css = ($type==1?"":"row_avaliable");
                    $signedCount++;
                }
                else
                {
                	$css = "row_suspend";
                    $last = "$i_Notice_Unsigned";
                    if($sys_custom['PaymentNoticeDoNotDisplayFailPaySignerInStat']){
                    	$display_signer = "";
                    }
                    $unsigned_StudentIDArr[] = $sid;
                }
				
				$display_current_row = true;
                
                $y = '';
				
	            $y .= "<tr class='$css'>";
	            $y .= "<td class='$css'>$classname_display</td>";
				$y .= "<td class='$css'>$class_num</td>";
                //$x .= "<tr class='$css'><td class='$css'><a href='sign.php?NoticeID=$NoticeID&StudentID=$sid'>$name</a></td>";
                if(strtoupper($lnotice->Module) == "PAYMENT")
				{
					//$x .= "<tr class='$css'><td class='$css'><a class='tablebluelink' href='eNoticePayment_sign.php?NoticeID=$NoticeID&StudentID=$sid'>$name</a></td>";
					$y .= "<td class='$css'><a class='tablebluelink' href='eNoticePayment_sign.php?NoticeID=$NoticeID&StudentID=$sid'>$name</a></td>";
				}
				else
				{
					//$x .= "<tr class='$css'><td class='$css'><a class='tablebluelink' href='sign.php?NoticeID=$NoticeID&StudentID=$sid'>$name</a></td>";
					$y .= "<td class='$css'><a class='tablebluelink' href='sign.php?NoticeID=$NoticeID&StudentID=$sid'>$name</a></td>";
				}

                // [2020-0522-1550-16315]
                //if(!$special_feature['eNotice']['DisableSubsidyIdentityName'])
                if($is_payment_notice && !$special_feature['eNotice']['DisableSubsidyIdentityName'])
				{
					$student_subsidy_name = '';
					$student_subsidy_identity_records = $lpayment->getSubsidyIdentityStudents(array('StudentID'=>$sid,'StartDateWithinEffectiveDates'=>$lnotice->DateStart,'EndDateWithinEffectiveDates'=>$lnotice->DateEnd));
					if(count($student_subsidy_identity_records)>0){
						$student_subsidy_identity_id = $student_subsidy_identity_records[0]['IdentityID'];
						$student_subsidy_identity_records = $lpayment->getSubsidyIdentity(array('IdentityID'=>$student_subsidy_identity_id));
						if(count($student_subsidy_identity_records)>0) {
							$student_subsidy_name = $student_subsidy_identity_records[0]['IdentityName'];
						}
					}
					$y .= "<td class='$css'>$student_subsidy_name</td>";
				}

                for ($j=0; $j<sizeof($questions); $j++)
                {
                	// [2017-1102-1332-42207] Answer Display - Question Type: One payment from a few categories
                	/*if ($questions[$j][0]==9)
            	 	{
	            	 	$x .= "<td class='$css'>".intranet_undo_htmlspecialchars($questions[$j][2][$answer[$j]])." &nbsp;</td>";
		        	}
		            else */ if (!in_array($questions[$j][0],array(6,16)))
                    {
	            	 	// [DM#3121] handle special characters
                   		//$x .= "<td class='$css'>".$answer[$j]." &nbsp;</td>";
                       	$y .= "<td class='$css'>".intranet_undo_htmlspecialchars($answer[$j])." &nbsp;</td>";
                    }
                }
                $modify_person = $is_payment_notice && isset($studentIdToModifyAnswerRecords[$sid])? '<br />('.$studentIdToModifyAnswerRecords[$sid]['ModifierUserName'].')' : '';
            	$modify_time = $is_payment_notice && isset($studentIdToModifyAnswerRecords[$sid])? '<br />('.$studentIdToModifyAnswerRecords[$sid]['InputDate'].')' : '';
                $y .= "<td class='$css'>$display_signer".$modify_person."&nbsp;</td><td class='$css'>$last".$modify_time."</td>";
                
              	if(strtoupper($lnotice->Module) == "PAYMENT")
				{
					$sql = "SELECT ItemID FROM PAYMENT_PAYMENT_ITEM WHERE NoticeID = '$NoticeID'";
					$arrItemIDs = $lnotice->returnVector($sql);
					if(sizeof($arrItemIDs)>0){
						$targetItemID = implode(",",$arrItemIDs);
					}
					
					$sql = "SELECT COUNT(*) FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE STUDENTID = '$sid' AND ItemID IN ($targetItemID)";
					$NoOfItemNeedToPaid = $lnotice->returnVector($sql);
					
					$sql = "SELECT COUNT(*) FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE STUDENTID = '$sid' AND ItemID IN ($targetItemID) AND RecordStatus = 1";
					$NoOfItemPaidSuccessfully = $lnotice->returnVector($sql);
					
					if(sizeof($answer)>0)
					{
						if($only_show_unpaid)
	            		{ 
	            			if($NoOfItemNeedToPaid[0] > 0 && $NoOfItemPaidSuccessfully[0] < $NoOfItemNeedToPaid[0]){
	            				$display_current_row = true;
	            			}else{
	            				$display_current_row = false;
	            			}
	            		}
						
						$y .= "<td class='$css'>".$NoOfItemNeedToPaid[0]."</td>";
						$y .= "<td class='$css'>".$NoOfItemPaidSuccessfully[0]."</td>";
						$y .= "<td class='$css'>".($notice_payment_method=='PayAtSchool'?$Lang['General']['Yes']:$Lang['General']['No'])."</td>";
					}
					else
					{
						if($only_show_unpaid){
            				$display_current_row = false;
            			}
						
						$y .= "<td class='$css'> - </td>";
						$y .= "<td class='$css'> - </td>";
						$y .= "<td class='$css'> - </td>";
					}
					
					if($only_show_payatschool){
	            		$display_current_row = $notice_payment_method == 'PayAtSchool';
	            	}
				}
                
                $y .= "</tr>\n";
                
                if($display_current_row){
            		$x .= $y;
            	}
			}
            $x .= "</table>\n";
		}
        else
        {
        	$NoMsg = ($all==1? $i_Notice_NoStudent:$i_Notice_NotForThisClass);
            $x .= "<br /><table width='100%' border='0' cellpadding='1' cellspacing='1'>\n";
            $x .= "<tr><td align='center' class='tabletext'>$NoMsg</td></tr>\n";
            $x .= "</table>\n";
        }
        $x .= "<br \>\n";
    }
}

# [2015-0323-1602-46073] check if user teach any selected classes
$matchClasses = false;
$currentClassName = explode(',', $class);
$teacherClasses = $lclass->returnHeadingClass($UserID, 1);
foreach((array)$teacherClasses as $teacherClassName){
	if(in_array($teacherClassName[0], (array)$currentClassName)){ 
		$matchClasses = true;
		break;
	}
}

# build thickbox
$thickBoxHeight = 180;
$thickBoxWidth = 600;

$pushMessageButton = $linterface->Get_Thickbox_Link($thickBoxHeight, $thickBoxWidth, "", 
    $Lang['AppNotifyMessage']['MessageToNotSignedParents'], "", 'divPushMessage', $Content='', 'pushMessageButton');

# thickbox layout
$thickboxContent = '';
$thickboxContent .= '<div id="divPushMessage" style="display:none">';
$thickboxContent .= '<form id="messageMethodForm" action="#">';
$thickboxContent .= '<table class="form_table_v30">';
$thickboxContent .= '<tr>';
$thickboxContent .= '<td class="field_title"><span>'.$Lang['AppNotifyMessage']['MessagingMethod'].'</span></td>';
$thickboxContent .= '<td>';
$smsChecked = 1;
if ($plugin['ASLParentApp'] || $plugin['eClassApp']) {
	$smsChecked = 0;
	$thickboxContent .= $linterface->Get_Radio_Button('pushMessage', 'messageMethod', 'pushMessage', $isChecked=1, $Class='', $Lang['AppNotifyMessage']['PushMessage'], $Onclick='', $Disabled='');
	$thickboxContent .= '<br/>';
}
if ($plugin['sms'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"]) {
	$thickboxContent .= $linterface->Get_Radio_Button('SMS', 'messageMethod', 'SMS', $isChecked=$smsChecked, $Class='', $Lang['AppNotifyMessage']['SMS'], $Onclick='', $Disabled='');
	$thickboxContent .= '<br/>';
}
if (($plugin['ASLParentApp'] || $plugin['eClassApp']) && $plugin['sms'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"]) {
	$thickboxContent .= $linterface->Get_Radio_Button('pushMessageThenSMS', 'messageMethod', 'pushMessageThenSMS', $isChecked=0, $Class='', $Lang['AppNotifyMessage']['PushMessageThenSMS'], $Onclick='', $Disabled='');
	$thickboxContent .= '</td>';
}
$thickboxContent .= '</tr>';
$thickboxContent .= '</table>';
$thickboxContent .= '<br/>';
$thickboxContent .= '<div align="center">';
$thickboxContent .= $linterface->GET_ACTION_BTN($Lang['AppNotifyMessage']['MessageToNotSignedParents'], "button", "js_send_message()");
if($plugin['eClassApp']){
	$thickboxContent .= '&nbsp;';
	$thickboxContent .= $linterface->GET_ACTION_BTN($Lang['MessageCenter']['ExportMobile'], "button", "goExportMobile()");
	$thickboxContent .= '<br /><br />';
}
$thickboxContent .= '</div>';
if($plugin['eClassApp']){
	$thickboxContent .= '<div>';
	$thickboxContent .= "<span class='tabletextremark'>".$Lang['MessageCenter']['ExportMobileRemarks']."</span>";
	$thickboxContent .= '</div>';
}	
$thickboxContent .= '</form>';
$thickboxContent .= '</div>';

if ($plugin['eClassApp']) {
	$sendPushMessagePhp = 'notify_parent_via_eClassApp.php';
	$sendPushMessageThenSmsPhp = 'notify_parent_via_eClassApp_then_sms.php';
}
else {
	$sendPushMessagePhp = 'notify_parent_via_app.php';
	$sendPushMessageThenSmsPhp = 'notify_parent_via_app_then_sms.php';
}
?>

<script language="Javascript" src='/templates/brwsniff.js'></script>
<script language="Javascript" src='/templates/tooltip.js'></script>

<script type="text/javascript" src= "<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.tablednd_0_5.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.cookies.2.2.0.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.scrollTo-min.js"></script>

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" />

<style type="text/css">
#ToolTip{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>

<script language="JavaScript">
isToolTip = true;
</script>
<div id="ToolTip"></div>

<script language="JavaScript" type="text/javascript">
$(document).ready(function() {
	if ('<?=$directSend?>' == '1') {
		if ('<?=$sendMethod?>' == 'SMS') {
			notifyParentBySMS_send();
		}
		else if ('<?=$sendMethod?>' == 'pushMessageThenSMS') {
			notifyParentByAppThenSMS_send();
		}
	}
});

function click_print()
{
	with(document.form1)
    {
        submit();
	}
}

function csv_export(export_type)
{
	window.location="export_csv.php?NoticeID=<?php echo $NoticeID ?>&class=<?php echo urlencode($class)?>&all=<?php echo urlencode($all) ?><?php echo $is_payment_notice && $payment_status != ''? '&payment_status='.urlencode($payment_status):''?>&unsignedList="+export_type;
}

function notifyParentByApp()
{
	$('#PreviewStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.ajax({
	    url: '../../eAdmin/StudentMgmt/notice/<?=$sendPushMessagePhp?>?NoticeID=<?=$NoticeID?>&class=<?=$SelectedClass?>&csrf_token=' + $('input#csrf_token').val() + '&csrf_token_key=' + $('input#csrf_token_key').val(),
	    error: function(xhr) {
			alert('request error');
	    },
	    success: function(response) {
	      	$('#PreviewStatus').html(response);
	    }
	});
}

function notifyParentBySMS()
{
//	$('#PreviewStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
//	$.ajax(
//		{
//			url: '../../eAdmin/StudentMgmt/notice/notify_parent_via_sms.php?NoticeID=<?=$NoticeID?>&class=<?=$SelectedClass?>',
//			error: function(xhr) {
//				alert('request error');
//			},
//			success: function(response) {
//				$('#PreviewStatus').html(response);
//			}
//		}
//	);
	
	$('input#sendMethod').val('SMS');
	$('form#form1').attr('target', '_self').attr('action', '../../eAdmin/StudentMgmt/notice/confirm_send_sms.php').submit();
}

function notifyParentBySMS_send()
{
	$('#PreviewStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.post(
		"../../eAdmin/StudentMgmt/notice/notify_parent_via_sms.php", 
		{
			NoticeID: '<?=$NoticeID?>',
			SelectedClass: '<?=$SelectedClass?>',
			Message: $('input#Message').val(),
			csrf_token: $('input#csrf_token').val(),
			csrf_token_key: $('input#csrf_token_key').val()
		},
		function(response) {
			js_Hide_ThickBox();
			$('#PreviewStatus').html(response);
			Scroll_To_Element('PreviewStatus');
		}
	);
}

function notifyParentByAppThenSMS()
{
//	$('#PreviewStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
//	$.ajax(
//		{
//			url: '../../eAdmin/StudentMgmt/notice/<?=$sendPushMessageThenSmsPhp?>?NoticeID=<?=$NoticeID?>&class=<?=$SelectedClass?>',
//			error: function(xhr) {
//				alert('request error');
//			},
//			success: function(response) {
//				$('#PreviewStatus').html(response);
//			}
//		}
//	);
	
	$('input#sendMethod').val('pushMessageThenSMS');
	$('form#form1').attr('target', '_self').attr('action', '../../eAdmin/StudentMgmt/notice/confirm_send_sms.php').submit();
}

function notifyParentByAppThenSMS_send()
{
	$('#PreviewStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.post(
		"../../eAdmin/StudentMgmt/notice/<?=$sendPushMessageThenSmsPhp?>", 
		{ 
			NoticeID: '<?=$NoticeID?>',
			SelectedClass: '<?=$SelectedClass?>',
			Message: $('input#Message').val(),
			csrf_token: $('input#csrf_token').val(),
			csrf_token_key: $('input#csrf_token_key').val()
		},
		function(response) {
			js_Hide_ThickBox();
			$('#PreviewStatus').html(response);
			Scroll_To_Element('PreviewStatus');
		}
	);
}

function js_send_message()
{
	var selectedVal = "";
	selectedVal = $('input:radio[name=messageMethod]:checked').val();
	
	if (selectedVal == 'pushMessage') {
		notifyParentByApp();
		js_Hide_ThickBox();
	} else if (selectedVal == 'SMS') {
		notifyParentBySMS();
	} else if (selectedVal == 'pushMessageThenSMS') {
		notifyParentByAppThenSMS();
	}
}

function js_show_thickbox()
{
	$('a#pushMessageButton').click();
}

function goExportMobile()
{
	var url = '<?=$PATH_WRT_ROOT?>' + '/home/eAdmin/GeneralMgmt/MessageCenter/export_mobile.php';
	$('form#form2').attr('action', url ).submit();
	$('form#form2').attr('action', '' );
}

function refreshForm()
{	
	document.form1.action = '';
	document.form1.target = '';
	document.form1.method = 'POST';
	
	document.form1.submit();
}
</script>

<form name="form1" id="form1" action="tableview_print_preview.php" method="post" target = "_blank">

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
	<td align="right"> 
		<a href="stat.php?type=<?=urlencode($stype)?>&NoticeID=<?=$NoticeID?>&class=<?=urlencode($class)?>&all=<?=urlencode($all)?>" class="contenttool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eOffice/icon_viewstat.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Notice_ViewStat?></a>
	</td>
</tr>
</table>

<table class="form_table_v30" border="0"> 
	<!-- View Slip --> 
	<tr valign='top'>
		<td class="field_title"><?=$i_Notice_NoticeNumber?></td>
		<td><?=$lnotice->NoticeNumber?></td>
	</tr>
	
	<tr valign='top'>
		<td class="field_title"><?=$i_Notice_Title?></td>
		<td><?=$lnotice->Title?></td>
	</tr>
	
	<? if ($all != 1) {?>
    	<tr valign='top'>
    		<td class="field_title"><?=$i_ClassName?></td>
    		<td><?=$class=="NULL"? "--" : $class?></td>
    	</tr>
	<? } ?>
	
	<tr valign='top'>
		<td class="field_title"><?="$i_Notice_Signed/$i_Notice_Total"?></td>
		<td><?="$signedCount/$totalCount"?></td>
	</tr>

	<!--  Class List -->
	
</table>

<br>
<?php 
if($is_payment_notice){
	echo '<div class="table_filter">';
	echo $payment_status_selection;
	echo '</div>'."\n";
}
?>

<?=$x?>

<table width="100%" align="center">
<tr>
	<td> * <?=$Lang['eNotice']['LateSubmission']?></td>
</tr>
</table>

<!-- Button //-->
<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_export_all, "button", "csv_export(0)") ?>
	<?= $linterface->GET_ACTION_BTN($Lang['Notice']['ExportUnsignedNoticeList'], "button", "csv_export(1)") ?>
	<?php
    // [2020-0604-1821-16170]
	// [2015-0303-1237-15073] - allow class teacher to send push message only if "Allow class teacher to send push message to parents." - true
	// [2015-0323-1602-46073] - allow eNotice PIC to send notify message
	// Condition:
	// 1) eClassApp / SMS plugin
	// 2) at least one student has not yet sign enotice
	// 3a) user type - Issuer, Normal control group / Advanced control group, eNotice admin, eNotice PIC
	// 3b) user teach any selected classes AND Settings - "Allow class teacher to send push message to parents." - true
	// 4) notice type not Student-signed Notice 
    // if ((($plugin['ASLParentApp'] || $plugin['eClassApp']) || $plugin['sms']) && sizeof($lnotice->getUnsignList($NoticeID, $SelectedClass)) && ($lnotice->hasViewRight() || $lnotice->IssuerUserID == $UserID))
    // if ((($plugin['ASLParentApp'] || $plugin['eClassApp']) || $plugin['sms']) && sizeof($lnotice->getUnsignList($NoticeID, $SelectedClass)) &&
    //    ($lnotice->hasViewRight() || $lnotice->isNoticePIC($NoticeID) || ($lnotice->isAllowClassTeacherSendeNoticeMessage && $matchClasses)) &&
    //    (trim($lnotice->TargetType)!='S'))
    //    {
    if (($plugin['ASLParentApp'] || $plugin['eClassApp'] || $plugin['sms']) && sizeof($lnotice->getUnsignList($NoticeID, $SelectedClass)) &&
        (
            $lnotice->hasViewRight() || $lnotice->isNoticePIC($NoticeID) || ($matchClasses &&
            (
                (!$is_payment_notice && $lnotice->isAllowClassTeacherSendeNoticeMessage) ||
                ($is_payment_notice && $lnotice->payment_isAllowClassTeacherSendeNoticeMessage)
            ))
        ) && (trim($lnotice->TargetType) != 'S'))
    {
		    echo $linterface->GET_ACTION_BTN($Lang['AppNotifyMessage']['MessageToNotSignedParents'], "button", "js_show_thickbox();");
			echo $pushMessageButton;
    }
	?>	
	<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "click_print()") ?>
	<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close()","submit3") ?>
	<?php  if(!($_GET['class']=="") || $_GET['all'] == '1') {
		//echo $linterface->GET_ACTION_BTN($button_back, "button", "window.location.href='/home/eAdmin/StudentMgmt/notice/result.php?NoticeID=$NoticeID'");
		echo $linterface->GET_ACTION_BTN($button_back, "button", "window.history.back()");
	}?>
	<p class="spacer"></p>
</div>
<p><center><span id='PreviewStatus'></span></center></p>

<input type="hidden" name="NoticeID" id="NoticeID" value="<?=$NoticeID?>">
<input type="hidden" name="class" id="class" value="<?=$class?>">
<input type="hidden" name="all" id="all" value="<?=$all?>">
<input type="hidden" name="type" id="type" value="<?=$stype?>">

<input type="hidden" name="Message" id="Message" value="<?=intranet_htmlspecialchars($Message)?>">
<input type="hidden" name="fromPage" id="fromPage" value="tableview">
<input type="hidden" name="sendMethod" id="sendMethod" value="">

<?php echo csrfTokenHtml(generateCsrfToken()); ?>
</form>

<form id="form2" name="form2" method="post" action="" >
<input type="hidden" name="StudentIDArr" value="<?=rawurlencode(serialize($unsigned_StudentIDArr))?>">
</form>
             
<?=$thickboxContent?>
			 
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>