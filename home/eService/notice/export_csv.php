<?php
// Using :

################### Change Log
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			updated access right checking
#
#   Date:   2020-07-31  Bill    [2020-0522-1550-16315]
#           show "Student Subsidy Identity Settings" col only for payment notice
#
#	Date:	2020-03-12	YatWoon
#			- Add $special_feature['eNotice']['DisableSubsidyIdentityName'] flag checking [#F180463]
#			  Hide Student Subsidy Identity Name
#			- Header title use $Lang instead of hard-code
#
#   Date:   2019-12-11  Ray
#           Added - Student Subsidy Identity Name
#
#	Date:	2019-09-25	Philips [2019-0830-1524-38206]
#			modified returnTableViewGroupByClass() with $withoutClass to collection information of students having no class and class nubmer
#
#   Date:   2019-08-13  Bill    [2019-0813-1222-14207]
#           fixed: cannot export unsigned student list
#
#	Date:	2019-08-01  Carlos
#			For Payment Notice, added [Last modify answer person] and [Last modify answer time].
# 
#	Date:	2019-06-14  Carlos
#			Added [Pay at school] column.
#			Added filter to filter out [Unpaid]/[Pay at school] records.
#
#	Date:	2019-05-29  Carlos
#			Added flag $sys_custom['PaymentNoticeDoNotDisplayFailPaySignerInStat'] to do not display signed but failed to pay signer data.
#			Deprecated the old flag $sys_custom['PaymentNoticeDisplayFailPaySignerInStat'] which is reverse logic of the new flag.
#
#	Date:	2019-05-17  Carlos
#			Added flag $sys_custom['PaymentNoticeDisplayFailPaySignerInStat'] to display signed but failed to pay students.
#
#	Date:	2019-04-10	Carlos
#			Cater more question types for Payment Notice.
#
#	Date:	2019-02-26	Carlos
#			Cater display of signer for Payment Notice that failed to pay but signed, display as unsigned.
#
#	Date:	2017-01-18 	Bill	[DM#3121]
#			Handle export special characters in question and answer
#
#	Date:	2016-08-04	Ivan [ip.2.5.7.9.1]
#			Improved: support export UserLogin if $sys_custom['eNotice']['ExportUserLogin'] is on
#
#	Date:	2016-03-10	Bill	[2016-0310-0911-58085]
#			Improved: allow export student in Unsigned Notice List
#
#	Date:	2016-03-04	Bill	[2016-0301-1704-52207]
#			Fixed: remove tab in answer to prevent inproper alignment
#
#	Date:	2015-04-15	Bill
#			Improved: show details to eNotice PIC if Settings - "Allow PIC to edit replies for parents." - true [2015-0323-1602-46073]
#			Fixed: output file name "notice.csv" only if teacher teach too many classes
#
#	Date:	2015-03-10 [Ivan] (ip.2.5.6.5.1)
#			Added to show auth code logic for admin view	
#
# 	Date:	2014-09-30 Bill
#			improved: allow access when "staffview" is turned on
#
#	Date:	2014-09-06 YatWoon
#			improved: hide classname in filename if class name is NULL
#
#	Date:	2014-02-26 YatWoon
#			improved: cater 1 staff with multiple class [Case#U58796]
#
#	Date:	2013-09-23	YatWoon
#			add WebSAMSRegNo, for customization $sys_custom['eNotice']['ExportWebSAMSRegNo']  [Case#2012-1018-1523-11140]
#
#	Date:	2010-09-10	YatWoon
#			trim line break for question
#
##############################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

################################
# Pass in:
#	$NoticeID
################################
$NoticeID = IntegerSafe($NoticeID);

$lexport = new libexporttext();

$lnotice = new libnotice($NoticeID);
$lu = new libuser($UserID);
$lcalss = new libclass();
$lpayment = new libpayment();

$stype = $type;
$onlyUnsigned = $unsignedList==1;   // [2016-0310-0911-58085]
if (!$lu->isTeacherStaff())
{
     header("Location: /close.php");
     exit();
}

// [2020-0604-1821-16170]
// [2015-0323-1602-46073] - Target Type - not Applicable students -> add checking if user is not current eNotice PIC
$isStaffView = strtoupper($lnotice->Module) == "PAYMENT" ? $lnotice->payment_staffview : $lnotice->staffview;
//if ($type==1 || (!$lnotice->hasViewRight() && $lnotice->IssuerUserID != $UserID && !$lnotice->staffview && !$lnotice->isNoticePIC($NoticeID)))
if ($type==1 || (!$lnotice->hasViewRight() && $lnotice->IssuerUserID != $UserID && !$isStaffView && !$lnotice->isNoticePIC($NoticeID)))
{
     //$class = $lnotice->returnClassTeacher($UserID);
     $class = $lcalss->returnHeadingClass($UserID,1);
     if(is_array($class))
     {
     	$class_str = "";
     	$dest = "";
     	for($i=0;$i<sizeof($class);$i++)
     	{
     		$class_str .= $dest . $class[$i][0];
     		$dest = ",";
     	}
     }
     $class = $class_str;

     if ($class=="")
     {
     	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT($i_general_no_access_right);
		exit;
     }
}

$is_payment_notice = strtoupper($lnotice->Module) == "PAYMENT";

$payment_status = in_array($payment_status,array('','-1','-2'))? $payment_status : '';
$only_show_unpaid = $payment_status == '-1';
$only_show_payatschool = $payment_status == '-2';

if($is_payment_notice){
	$modify_answer_records = $lnotice->getReplySlipModifyAnswerRecords(array('NoticeID'=>$NoticeID,'GetModifierUserInfo'=>true));
	$studentIdToModifyAnswerRecords = BuildMultiKeyAssoc($modify_answer_records,'StudentID');
}

$questions = $lnotice->splitQuestion($lnotice->Question);

if ($all == "1")
{
    $data = array();

    $classes = $lnotice->returnTableViewGroupByClass(true);
    for ($pos=0; $pos<sizeof($classes); $pos++)
    {
		list($classname,$tmp_data) = $classes[$pos];
		
		for($i=0; $i<sizeof($tmp_data); $i++)
		{
			$tmp_arr = array($classname);
			$tmp_data[$i] = array_merge($tmp_arr, $tmp_data[$i]);
		}
		
		$data = array_merge($data, $tmp_data);
    }
}
else if ($class!="")
{
	$data = $lnotice->returnTableViewByClass($class);
}
$x = "";
for($i=0; $i<sizeof($data); $i++)
{
	$display_current_row = true;
	$y = '';
	$utf_y = '';
	
	if($all == "1")
	{
		list ($classname,$sid,$name,$type,$status,$answer,$signer,$last, $classNo, $regno, $signerAuthCode, $studentUserLogin, $student_status, $notice_payment_method) = $data[$i];
		
		// [2016-0310-0911-58085] skip - Unsigned Notice List and notice is signed 
		if($onlyUnsigned && $status==2) {
            continue;
        }
		
		$y .= "\"".$classname."\",";
		$utf_y .= "".$classname."\t";
	}
	else if($class!="")
	{
		list ($sid,$name,$classNo,$type,$status,$answer,$signer,$last, $classname, $regno, $signerAuthCode, $studentUserLogin, $student_status, $notice_payment_method) = $data[$i];
		
		// [2016-0310-0911-58085] skip - Unsigned Notice List and notice is signed 
		if($onlyUnsigned && $status==2) {
            continue;
        }

		$y .= "\"".$classname."\",";
		$utf_y .= "".$classname."\t";
	}
	
	// [2016-0301-1704-52207] replace tab by space
	//$answer = str_replace("\r\n"," ",$answer);
	$answer = str_replace("\r\n"," ",str_replace("\t"," ",$answer));
	
	$display_signer = $signer;
	
	//added by Kelvin Ho 2009-01-14. if not signed by parents, hide the last modify date
	if(trim($signer)==''){
		$last='';
	}
	if($status != "2"){
		if($sys_custom['PaymentNoticeDoNotDisplayFailPaySignerInStat']){
			$display_signer = "";
		}
		$last = "";
	}
	
	$y .= "\"".$classNo."\",";
	$utf_y .= "".$classNo."\t";
	
	$y .= "\"".$name."\",";
	$utf_y .= "".$name."\t";

    // [2020-0522-1550-16315]
	//if(!$special_feature['eNotice']['DisableSubsidyIdentityName'])
    if($is_payment_notice && !$special_feature['eNotice']['DisableSubsidyIdentityName'])
	{
		$student_subsidy_name = '';
		$student_subsidy_identity_records = $lpayment->getSubsidyIdentityStudents(array('StudentID'=>$sid,'StartDateWithinEffectiveDates'=>$lnotice->DateStart,'EndDateWithinEffectiveDates'=>$lnotice->DateEnd));
		if(count($student_subsidy_identity_records)>0){
			$student_subsidy_identity_id = $student_subsidy_identity_records[0]['IdentityID'];
			$student_subsidy_identity_records = $lpayment->getSubsidyIdentity(array('IdentityID'=>$student_subsidy_identity_id));
			if(count($student_subsidy_identity_records)>0) {
				$student_subsidy_name = $student_subsidy_identity_records[0]['IdentityName'];
			}
		}
		$y .= "\"".$student_subsidy_name."\",";
		$utf_y .= "".$student_subsidy_name."\t";
	}

	if($sys_custom['eNotice']['ExportWebSAMSRegNo'])
	{
		$y .= "\"".$regno."\",";
		$utf_y .= "".$regno."\t";
	}
	if($sys_custom['eNotice']['ExportUserLogin'])
	{
		$y .= "\"".$studentUserLogin."\",";
		$utf_y .= "".$studentUserLogin."\t";
	}
	
	// [2016-0310-0911-58085] skip - Unsigned Notice List, no need to export reply details
	if($onlyUnsigned)
	{
		$y .= "\n";
		$utf_y .= "\r\n";

        $x .= $y;
        $utf_x .= $utf_y;
		continue;
	}

	for($j=0; $j<sizeof($questions); $j++)
	{
		/*
		if ($questions[$j][0]!=6) 
      	{
        	$x .= "\"".$answer[$j]."\",";
        	$utf_x .= "".$answer[$j]."\t";
    	}
    	*/
		/*if ($questions[$j][0]==9)
    	{
    		$x .= "\"".$questions[$j][2][$answer[$j]]."\",";
    		
			// [DM#3121] handle special characters
        	//$utf_x .= "".$questions[$j][2][$answer[$j]]."\t";
        	$utf_x .= "".str_replace("\r\n", "", intranet_undo_htmlspecialchars(str_replace("\t", " ", $questions[$j][2][$answer[$j]])))."\t";
        }
        else */
		if (!in_array($questions[$j][0],array(6,16)))
        {
			$y .= "\"".$answer[$j]."\",";
			
			// [DM#3121] handle special characters
        	//$utf_x .= "".$answer[$j]."\t";
        	$utf_y .= "".str_replace("\r\n", "", intranet_undo_htmlspecialchars(str_replace("\t", " ", $answer[$j])))."\t";
		}
		else
		{
		    // do nothing
		}
	}
    
    $modify_person = $is_payment_notice && isset($studentIdToModifyAnswerRecords[$sid])? ' ('.$studentIdToModifyAnswerRecords[$sid]['ModifierUserName'].')' : '';
    $modify_time = $is_payment_notice && isset($studentIdToModifyAnswerRecords[$sid])? ' ('.$studentIdToModifyAnswerRecords[$sid]['InputDate'].')' : '';
        
    $y .= "\"".$display_signer.$modify_person."\",";
    $utf_y .= "".$display_signer.$modify_person."\t";
    $y .= "\"".$last.$modify_time."\"";
    $utf_y .= "".$last.$modify_time."";
      
    if ($sys_custom['eClassApp']['authCode'])
    {
      	$utf_y .= "\t";
    	$y .= "\"".$signerAuthCode."\"";
      	$utf_y .= "".$signerAuthCode."";
    }

    if(strtoupper($lnotice->Module) == "PAYMENT")
    {
		$sql = "SELECT ItemID FROM PAYMENT_PAYMENT_ITEM WHERE NoticeID = '$NoticeID'";
		$arrItemIDs = $lnotice->returnVector($sql);
		if(sizeof($arrItemIDs)>0){
			$targetItemID = implode(",",$arrItemIDs);
		}
		
		$sql = "SELECT COUNT(*) FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE STUDENTID = '$sid' AND ItemID IN ($targetItemID)";
		$NoOfItemNeedToPaid = $lnotice->returnVector($sql);
		
		$sql = "SELECT COUNT(*) FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE STUDENTID = '$sid' AND ItemID IN ($targetItemID) AND RecordStatus = 1";
		$NoOfItemPaidSuccessfully = $lnotice->returnVector($sql);
		
		if(sizeof($answer)>0)
		{
			if($only_show_unpaid)
    		{ 
    			if($NoOfItemNeedToPaid[0] > 0 && $NoOfItemPaidSuccessfully[0] < $NoOfItemNeedToPaid[0]){
    				$display_current_row = true;
    			}else{
    				$display_current_row = false;
    			}
    		}
			
			$y .= ",";
			$y .= "\"".$NoOfItemNeedToPaid[0]."\",";
			$y .= "\"".$NoOfItemPaidSuccessfully[0]."\",";
			$y .= "\"".($notice_payment_method=='PayAtSchool'?$Lang['General']['Yes']:$Lang['General']['No'])."\"";
			$utf_y .= "\t";
  			$utf_y .= "".$NoOfItemNeedToPaid[0]."\t";
  			$utf_y .= "".$NoOfItemPaidSuccessfully[0]."\t";
  			$utf_y .= "".($notice_payment_method=='PayAtSchool'?$Lang['General']['Yes']:$Lang['General']['No'])."";
		}
		else
		{
			if($only_show_unpaid){
    			$display_current_row = false;
    		}
			
			$y .= ",";
			$y .= "\" - \",";
			$y .= "\" - \",";
			$y .= "\" - \"";
			$utf_y .= "\t";
  			$utf_y .= "\" - \"\t";
  			$utf_y .= "\" - \"\t";
  			$utf_y .= "\" - \"";
		}
		
		if($only_show_payatschool){
        	$display_current_row = $notice_payment_method == 'PayAtSchool';
        }
	}

  	$y .= "\n";
  	$utf_y .= "\r\n";
  	
    if($display_current_row){
  		$x .= $y;
  		$utf_x .= $utf_y;
  	}
}
    
    // $heading = "\"Class\",\"Class No.\",\"Student\",";
    $utf_heading = "\"".$i_ClassName ."\"\t"."\"".$i_ClassNumber."\"\t"."\"".$i_UserStudentName."\"\t";

    // [2020-0522-1550-16315]
    //if(!$special_feature['eNotice']['DisableSubsidyIdentityName'])
    if($is_payment_notice && !$special_feature['eNotice']['DisableSubsidyIdentityName'])
	{
		$utf_heading .= "\"". $Lang['ePayment']['SubsidyIdentityName'] ."\"\t";
	}

    if($sys_custom['eNotice']['ExportWebSAMSRegNo']) {
		$utf_heading .= "\"".$i_WebSAMS_Registration_No."\"\t";
	}
	if ($sys_custom['eNotice']['ExportUserLogin']) {
		$utf_heading .= "\"".$Lang['General']['UserLogin']."\"\t";
	}
    
    // [2016-0310-0911-58085] Unsigned Notice List, no need to display headers of reply details
    if($onlyUnsigned)
    {
		// do nothing
	}
	else
	{
	    for($i=0; $i<sizeof($questions); $i++)
	    {
			if (!in_array($questions[$i][0],array(6,16))) 
		    {
//		 		$heading .= "\"". str_replace("\r\n","",intranet_undo_htmlspecialchars($questions[$i][1])) ."\",";
				
				// [DM#3121] handle special characters (for double quote display)
		        //$utf_heading .= "". str_replace("\r\n","",intranet_undo_htmlspecialchars(str_replace("\t"," ",$questions[$i][1])))."\t";
		        $utf_heading .= '"'.str_replace("\r\n", "", intranet_undo_htmlspecialchars(str_replace("\t", " ", str_replace('"', '""', $questions[$i][1])))).'"'."\t";
		    }
	    }
	    //$heading .= "\"Signer\",\"Last Modified\"\n";
	    //$utf_heading .= "Signer"."\t"."Last Modified"."\r\n";

	    if(strtoupper($lnotice->Module) == "PAYMENT")
	    {
//	     	$heading .= "\"Signer\",";
	    	//$utf_heading .= "Signer (Last modify answer person)"."\t";
			$utf_heading .= "\"".$i_Notice_Signer.($is_payment_notice?" (".$Lang['eNotice']['LastModifyAnswerPerson'].")":"")."\"\t";
//	     	
//	     	$heading .= "\"Last Modified\",";
	    	//$utf_heading .= "Last Modified (Last modify answer time)"."\t";
			$utf_heading .= "\"".$i_Notice_SignedAt.($is_payment_notice?" (".$Lang['eNotice']['LastModifyAnswerTime'].")":"")."\"\t";
	    	
	    	if ($sys_custom['eClassApp']['authCode']) {
		    	//$utf_heading .= "Signed By Auth Code\t";
				$utf_heading .= $Lang['eNotice']['SignedByAuthCode']."\t";
		    }
	    	
//	     	$heading .= "\"".$Lang['eNotice']['ExportTitle']['NoOfItemNeedToPaid']."\",";
	    	//$utf_heading .= "".$Lang['eNotice']['ExportTitle']['NoOfItemNeedToPaid']."\t";
			$utf_heading .= "\"". $Lang['eNotice']['FieldTitle']['NoOfItemNeedToPaid'] ."\"\t";
//	     	$heading .= "\"".$Lang['eNotice']['ExportTitle']['NoOfItemPaidSuccessfully']."\"";
	    	//$utf_heading .= "".$Lang['eNotice']['ExportTitle']['NoOfItemPaidSuccessfully']."";
			$utf_heading .= "".$Lang['eNotice']['FieldTitle']['NoOfItemPaidSuccessfully']."";
	    	$heading .= ",\"".$Lang['eNotice']['PaymentNoticePayAtSchool']."\"";
		    $utf_heading .= "\t".$Lang['eNotice']['PaymentNoticePayAtSchool']."";
    	}
    	else
        {
//	     	$heading .= "\"Signer\",";
	    	$utf_heading .= "\"". $i_Notice_Signer. "\"\t";
//	     	$heading .= "\"Last Modified\"";
	    	//$utf_heading .= "Last Modified";
			$utf_heading .= "\"". $i_Notice_SignedAt. "\"";
			
	    	
	    	if ($sys_custom['eClassApp']['authCode']) {
	    		$utf_heading .= "\t";
		    	//$utf_heading .= "Signed By Auth Code";
				$utf_heading .= $Lang['eNotice']['SignedByAuthCode']."";
		    }
	    }
    }
    
    $heading .= "\n";
    $utf_heading .= "\r\n";
    
//  $x = $heading.$x;
    $utf_x = $utf_heading.$utf_x;

$class = $class=="NULL" || $class=='0' ? "": $class;

// prevent file name too long - return "notice.csv"
if($class != "" && strlen($class) > 100){
	$class = "";
}

// [2016-0310-0911-58085] get notice number
$notice_number = "_".$lnotice->NoticeNumber;

// [2016-0310-0911-58085] file name for Unsigned Notice List
$export_type = "";
if($onlyUnsigned){
	$export_type = "_noreply";
}

if($all == "1") {
    $filename = "notice".$notice_number."_all".$export_type.".csv";
} else {
    $filename = "notice".$notice_number.$class.$export_type.".csv";
}
$filename = parseFileName($filename);

/*
if (!$g_encoding_unicode) {
	output2browser($x, $filename);
	//$lexport->EXPORT_FILE($filename, $x);
} else {
	$lexport->EXPORT_FILE($filename, $utf_x);
}
*/
$lexport->EXPORT_FILE($filename, $utf_x);

intranet_closedb();
?>