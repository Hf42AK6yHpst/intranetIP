<?php
// Modifying by : 

############ Change log ################
#	Date:	2019-11-01  (Philips)	[2019-1016-1125-40260]
#			added back button
#
#   Date:   2019-06-05  (Bill)
#           add CSRF token checking
#
#	Date: 	2016-07-15	Bill
#			remove syntax from UI
#
#	Date: 	2016-06-10	Bill	[2016-0113-1514-09066]
#			remove target question syntax from question string
#
#	Date:	2015-10-28	Bill	[2015-0416-1040-06164]
#			support merge notice content
#
#	Date:	2015-07-27	Bill	[2015-0428-1214-11207]
# 			Display PICs to parent only $sys_custom['eNotice']['DisplayPICForParent'] = true
#
#	Date:	2015-05-18	Bill	[2015-0428-1214-11207]
# 			Improved: Add field to display PIC
#
#	Date:	2015-04-15	Bill
#			Improved: show details to eNotice PIC if Settings - "Allow PIC to edit replies for parents." - true [2015-0323-1602-46073]
#
#	Date:	2014-03-28	Henry
#			Fix bug: Filtering Result can't return after signed parent notice
#
#	Date:	2014-02-28	YatWoon
#			Add checking for student, is the student related to this notice? 
#
#	Date:	2011-02-16	YatWoon
#			Add option "Display question number"
#
#	Date :	2010-10-12 [YatWoon]
#			update reload parent javascript
#	
########################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

if(!verifyCsrfToken($_POST['csrf_token'], $_POST['csrf_token_key'])){
    header('Location: /');
    exit;
}

intranet_auth();
intranet_opendb();

################################
# Pass in:
#	$NoticeID
#	$StudentID
################################
$NoticeID = IntegerSafe($NoticeID);
$StudentID = IntegerSafe($StudentID);


$MODULE_OBJ['title'] = $i_Notice_ElectronicNotice;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$lform = new libform();
$lnotice = new libnotice($NoticeID);

$lu = new libuser($UserID);
$valid = true;
if ($lu->isStudent())
{
    $valid = false;
}
$isTeacher = $lu->isTeacherStaff();
$isParent = $lu->isParent();
if ($StudentID!="")
{
    if ($isParent)
    {
        $children = $lu->getChildren();
        if (!in_array($StudentID,$children))
        {
             $valid = false;
        }
        else
        {
            $editAllowed = true;
        }
    }
    else # Teacher
    {
    	
	    if($lnotice->IsModule)
	    {
			$editAllowed = true;    
	    }
	    else
	    {
        	$editAllowed = $lnotice->isEditAllowed($StudentID);
        	// [2015-0323-1602-46073] show details to eNotice PIC if Settings - "Allow PIC to edit replies for parents." - true
        	if($lnotice->isNoticePIC($NoticeID) && $lnotice->isPICAllowReply)
        		$editAllowed = true;
    	}
    }
}
else
{
	$valid = false;
	/*
    if ($isParent)
    {
        $valid = false;
    }
    else
    {
        $editAllowed = false;
    }
    */
}


############################################################
#	Check student belongs to this notice?
############################################################
if(!$lnotice->isStudentInNotice($NoticeID, $StudentID))
{
	$valid = false;
}

if(!$valid)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if ($editAllowed)
{
    $ansString = intranet_htmlspecialchars(trim($aStr));
        
	if(trim($ansString)=="" && trim($lnotice->Question)!="")
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT($Lang['General']['InvalidData'], "sign.php?NoticeID=$NoticeID&StudentID=$UserID");
		exit;
	}

	$type = ($isParent? 1: 2);

	# Update
	$sql = "UPDATE INTRANET_NOTICE_REPLY SET Answer = '$ansString',
			RecordType = '$type',RecordStatus = 2,SignerID='$UserID',DateModified=now()
			WHERE NoticeID = '$NoticeID' AND StudentID = '$StudentID'";
	$lnotice->db_db_query($sql);

    $lnotice->retrieveReply($StudentID);
    $ansString = $lnotice->answer;
    $ansString =  str_replace("&amp;", "&", $ansString);
	$ansString = $lform->getConvertedString($ansString);
	$ansString.=trim($ansString)." ";
}

// [2015-0428-1214-11207] get PICs of eNotice
if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){
	$pics = "";
	$noticePICs = $lnotice->returnNoticePICNames();
	if(count($noticePICs) > 0){
		$pics = "";
		$delim = "";
		for($picCount=0; $picCount<count($noticePICs); $picCount++){
			$pics .= $delim.$noticePICs[$picCount]['UserName'];
			$delim = ", ";
	  	}
	}
}

$targetType = array("",$i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual);
$attachment = $lnotice->displayAttachment();
$reqFillAllFields = $lnotice->AllFieldsReq;
?>
<script language="javascript">
if(window.opener!=null)
	window.opener.form1.submit();

</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td>
		<table class="form_table_v30">
		<tr valign='top'>
			<td class="field_title"><?=$i_Notice_DateStart?></td>
			<td><?=$lnotice->DateStart?></td>
		</tr>
		
		<tr valign='top'>
			<td class="field_title"><?=$i_Notice_DateEnd?></span></td>
			<td><?=$lnotice->DateEnd?></td>
		</tr>
		
		<tr valign='top'>
			<td class="field_title"><?=$i_Notice_Title?></td>
			<td><?=$lnotice->Title?></td>
		</tr>
		
		<tr valign='top'>
			<td class="field_title"><?=$i_Notice_Description?></td>
			<!--<td><?="";//htmlspecialchars_decode($lnotice->Description)?></td>-->
			<td><?=htmlspecialchars_decode($lnotice->getNoticeContent($StudentID))?></td>
		</tr>
		        
		<? if ($attachment != "") { ?>
		<tr valign='top'>
			<td class="field_title"><?=$i_Notice_Attachment?></td>
			<td><?=$attachment?></td>
		</tr>
		<? } ?>
                                
		<? if ($isTeacher) {
			$issuer = $lnotice->returnIssuerName();
		?>
                                <tr valign='top'>
					<td class="field_title"><?=$i_Notice_Issuer?></td>
					<td><?=$issuer?></td>
				</tr>
                        	<? } ?>
             
    	<?php if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){ ?>              	
			<tr valign='top'>
				<td class="field_title"><?=$i_Profile_PersonInCharge?></td>
				<td><?=$pics?></td>
			</tr>
		<?php } ?>
		         	
                        	<? if ($editAllowed) {
                                        if ($lnotice->replyStatus != 2)
                                        {
                                            $statusString = "$i_Notice_Unsigned";
                                        }
                                        else
                                        {
                                            $signer = $lu->getNameWithClassNumber($lnotice->signerID);
                                            $signby = ($lnotice->replyType==1? $i_Notice_Signer:$i_Notice_Editor);
                                            $statusString = "$i_Notice_Signed <I>($signby $signer $i_Notice_At ".$lnotice->signedTime.")</I>";
                                        }
				?>
                        	<tr valign='top'>
					<td class="field_title"'><?=$i_Notice_SignStatus?></td>
					<td><?=$statusString?></td>
				</tr>
                        
                        	<tr valign='top'>
					<td class="field_title"><?=$i_UserStudentName?></td>
					<td><?=$lu->getNameWithClassNumber($StudentID)?></td>
				</tr>
                        
				<? } ?>
                        
                                
				<tr valign='top'>
					<td class="field_title"><?=$i_Notice_RecipientType?></td>
					<td><?=$targetType[$lnotice->RecordType]?></td>
				</tr>
				
				<? if ($reqFillAllFields) { ?>
				<tr>
					<td class="field_title">&nbsp;</td>
					<td>[<?=$i_Survey_AllRequire2Fill?>]</td>
				</tr>
				<? } ?>
         </table>
			<td>
		</tr>   
                             
<? 
### check include reply slip or not
$hasContent = (trim($lnotice->Question) || trim($lnotice->ReplySlipContent)) ? true : false;
?>
<? if($hasContent) {?>
<tr>
	<td colspan="2">      
		<!-- Break -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/scissors_line.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/icons_scissors.gif"></td>
        </tr>
        </table>
	</td>
</tr>
<? } ?>
<tr>
 <!-- Relpy Slip -->
<tr valign='top'>
<td colspan="2" class='eNoticereplytitle' align='center'><?=$i_Notice_ReplySlip?></td>
</tr>

	<tr>
		<td colspan="2">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                        <? if($hasContent) {?>
                                        <tr>
                                        	<td class='tabletext' align='center'><?=$i_Notice_ReplySlip_Signed?></td>
					</tr>
					
					<? if($lnotice->ReplySlipContent) {?>
					<!-- Content Base Start //-->
						<tr valign='top'>
							<td colspan="2">
							<?=$lnotice->ReplySlipContent?>
							</td>
						</tr>
					<? } else { 
						$temp_que =  str_replace("&amp;", "&", $lnotice->Question);
						$queString = $lform->getConvertedString($temp_que);
						?>
                                        <tr>
                                        	<td>
                                                
                                                <script language="javascript" src="/templates/forms/form_view.js"></script>

                                                <form name="ansForm" method="post" action="update.php">
                                                        <input type=hidden name="qStr" value="">
                                                        <input type=hidden name="aStr" value="">
                                                </form>
                
                                                <script language="Javascript">
                                                        myQue="<?=$queString?>";
                                                       	myQue="<?= preg_replace("/#TAR#(([A-Z]|[a-z]|[0-9])+)#/", "", $queString) ?>";
                                                        myAns="<?=$ansString?>";
                                                        
                                                        var DisplayQuestionNumber = '<?=$lnotice->DisplayQuestionNumber?>';
                                                        document.write(viewForm(myQue, myAns));
                                                </SCRIPT>
                                                
                                        	</td>
					</tr>
					<? } ?>
					 <? } ?>

                                        </table>
                                        </td>
				</tr>

				</table>
				<div class="edit_bottom_v30">
					<p class="spacer"></p>
						<?= $linterface->GET_ACTION_BTN($button_print, "button", "window.print();","submit2"); ?>
                		<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close();","cancelbtn"); ?>
                		<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.history.back()");?>
					<p class="spacer"></p>
				</div>
                                
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>