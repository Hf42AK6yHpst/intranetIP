<?php
# modifying by : 

/* Change Log [Start]
 * 
 #	Date:	2015-04-28	Bill
 #			set $notice_content to class "tabletext", add style to ensure table header default align center
 # 
 #	Date:	2011-02-24	YatWoon
 #			Add "Display question number"
 #
 * Date		:	20101102 (Henry Chow)
 * Detail	:	display notice footer
 * 
 * Date		:	20100803 (Henry Chow)
 * Detail	:	allow parent to access this page (because some notices are module basis)
 * 
 * Change Log [End]	
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$NoticeID = IntegerSafe($NoticeID);


$linterface = new interface_html();

$lform = new libform();
$lnotice = new libnotice($NoticeID);

$lu = new libuser($UserID);

if ($lnotice->disabled || (!$lu->isTeacherStaff() && !$lu->isParent()))
{
     echo $i_general_no_access_right;
     intranet_closedb();
     exit();
}

$StudentID = str_replace("U","",$lnotice->RecipientID);

$editAllowed = true;

if ($editAllowed)
{
	$lnotice->retrieveReply($StudentID);
 	$temp_ans =  str_replace("&amp;", "&", $lnotice->answer);
	$ansString = $lform->getConvertedString($temp_ans);
	$ansString.=trim($ansString)." ";
}

$targetType = array("",$i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual);
$attachment = $lnotice->displayAttachment();

?>
<style>
	th { text-align: center; }
</style>

<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<?
$notice_content = $lnotice->Description;
?>
<table width="90%" border="0" cellpadding="3" cellspacing="0" align="center">
<tr><td class="tabletext"><?=$notice_content?></td></tr>
<? if ($attachment != "") { ?>
<tr><td class="eSportprinttitle"><?=$i_Notice_Attachment?> : <?=$attachment?></td></tr>
<? } ?>
<? if ($isTeacher) {
	$issuer = $lnotice->returnIssuerName();
?>
<tr><td class="eSportprinttitle"><?=$i_Notice_Issuer?> : <?=$issuer?></td></tr>
<? } ?>
</table>


<? 
### check include reply slip or not
$hasContent = (trim($lnotice->Question) || trim($lnotice->ReplySlipContent)) ? true : false;
?>

<? if($hasContent) {?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align='center'><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/print_scissors.gif"></td>
</tr>
</table>
<? } ?>

<table align="center" width="90%" border="0" cellpadding="0" cellspacing="0">
<? if($hasContent) {?>
                                <!-- Relpy Slip -->
                                <tr valign='top'>
					<td colspan="2" class='eSportprinttitle' align='center'><b><?=$i_Notice_ReplySlip?></b></td>
				</tr>
                                
				<? if($lnotice->ReplySlipContent) {?>
					<!-- Content Base Start //-->
						<tr valign='top'>
							<td colspan="2">
							<?=$lnotice->ReplySlipContent?>
							</td>
						</tr>
					<? } else { 
						$temp_que =  str_replace("&amp;", "&", $lnotice->Question);
						$queString = $lform->getConvertedString($temp_que);
						$queString =trim($queString)." ";
					?>
                                <tr valign='top'>
					<td colspan="2">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                        <tr>
                                        	<td>
                                        	<script language="javascript" src="/templates/forms/layer.js"></script>
                                        	<script language="javascript" src="/templates/forms/form_edit.js"></script>
                                        
                                                <form name="ansForm" method="post" action="update.php">
                                                        <input type=hidden name="qStr" value="">
                                                        <input type=hidden name="aStr" value="">
                                                </form>
                                                
                                                <script language="Javascript">
                                                var DisplayQuestionNumber = '<?=$lnotice->DisplayQuestionNumber?>';
                                                <?=$lform->getWordsInJS()?>
                                                var replyslip = '<?=$i_Notice_ReplySlip?>';
                                                var sheet= new Answersheet();
                                                // attention: MUST replace '"' to '&quot;'
                                                sheet.qString="<?=$queString?>";
                                                sheet.aString="<?=$ansString?>";
                                                //edit submitted application
                                                sheet.mode=1;
                                                sheet.answer=sheet.sheetArr();
                                                Part3 = '';
                                                document.write(editPanel());
                                                </script>
                                                
                                                <SCRIPT LANGUAGE=javascript>
                                                function copyback()
                                                {
                                                         finish();
                                                         document.form1.qStr.value = document.ansForm.qStr.value;
                                                         document.form1.aStr.value = document.ansForm.aStr.value;
                                                }
                                                </SCRIPT>
                                                
                                        	</td>
					</tr>
                                        </table>
                                        </td>
				</tr>
				<? } ?>
				<? } ?>

				</table>
				<table align="center" width="90%" border="0" cellpadding="0" cellspacing="0">
					<tr><td>
						<?
						if($sys_custom['eDisciplinev12_SunKei_eNoticeSignature'])
							echo $lnotice->footer;
						?>
					</td></tr>
				</table>				
			</td>
                </tr>
                </table>

<?

$update_sql = "update INTRANET_NOTICE_REPLY set PrintDate = now() where NoticeID in ($NoticeID)";
$lnotice->db_db_query($update_sql);

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>