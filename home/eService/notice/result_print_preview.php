<?php
// using : 

############ Change Log Start ###############
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			updated access right checking
#
#	Date:	2015-04-14	Bill	[2015-0323-1602-46073]
#			Improved: allow eNotice PIC to access
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

################################
# Pass in:
#	$NoticeID
################################
$NoticeID = IntegerSafe($NoticeID);


$linterface = new interface_html("");

$lnotice = new libnotice($NoticeID);

$AcademicYearID = Get_Current_Academic_Year_ID();

$lu = new libuser($UserID);
if (!$lu->isTeacherStaff())
{
     header("Location: /close.php");
     exit();
}

// [2020-0604-1821-16170]
// [2015-0323-1602-46073] - allow eNotice PIC to access
$isStaffView = strtoupper($lnotice->Module) == "PAYMENT" ? $lnotice->payment_staffview : $lnotice->staffview;
//if (!$lnotice->hasViewRight() && !$lnotice->staffview && !$lnotice->isNoticePIC($NoticeID))
if (!$lnotice->hasViewRight() && !$isStaffView && !$lnotice->isNoticePIC($NoticeID))
{
     header("Location: /close.php");
     exit();
}

if ($lnotice->RecordType == 4)
{
    header("Location: tableview.php?NoticeID=$NoticeID");
    exit();
}

$array_total = $lnotice->returnTotalCountByClass();
$array_signed = $lnotice->returnSignedCountByClass();
$all_total = 0;
$all_signed = 0;
$classes = $lnotice->returnClassList();

$x .= "<table width='90%' border='0' cellspacing='0' cellpadding='4' class='eSporttableborder'>\n";
$x .= "<tr>";
$x .= "<td class='eSporttdborder eSportprinttabletitle'>$i_ClassName</td>";
$x .= "<td class='eSporttdborder eSportprinttabletitle' width='80'>$i_Notice_Signed</td>";
$x .= "<td class='eSporttdborder eSportprinttabletitle' width='80'>$i_Notice_Total</td>";
if(strtoupper($lnotice->Module) == "PAYMENT")
{
	$x .= "<td class='eSporttdborder eSportprinttabletitle' width='80'>".$Lang['eNotice']['FieldTitle']['NoOfStudentNeedToPaid']."</td>";
	$x .= "<td class='eSporttdborder eSportprinttabletitle' width='80'>".$Lang['eNotice']['FieldTitle']['NoOfStudentPaidSuccessfully']."</td>";
}
$x .= "</tr>\n";

for ($i=0; $i<sizeof($classes); $i++)
{
	 $cnt = 0;
     $name = $classes[$i];
     ## check $class <>""
     if(!$name) continue;
     $signed = $array_signed[$name]+0;
     $total = $array_total[$name]+0;
     $displayName = ($name==""? "--":"$name");
     $x .= "<tr>\n";
     $x .= "<td class='eSporttdborder eSportprinttext'>$displayName</td>";
     $x .= "<td class='eSporttdborder eSportprinttext' width='80'>$signed</td>";
     $x .= "<td class='eSporttdborder eSportprinttext' width='80'>$total</td>";
     
     if(strtoupper($lnotice->Module) == "PAYMENT")
     {
     	 $sql = "SELECT b.UserID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE a.ClassTitleEN = '$name' AND a.AcademicYearID = '$AcademicYearID'";
	     $arrUserIDs = $lnotice->returnVector($sql);
	     
	     $sql = "SELECT ItemID FROM PAYMENT_PAYMENT_ITEM WHERE NoticeID = '$NoticeID'";
	     $arrItemIDs = $lnotice->returnVector($sql);
	     
	     
	     if(sizeof($arrUserIDs)>0){
	     	$targetUserID = implode(",",$arrUserIDs);
	     }
	     if(sizeof($arrItemIDs)>0){
	     	$targetItemID = implode(",",$arrItemIDs);
	     }
	     //$sql = "SELECT StudentID, COUNT(*) as TotalItemNeedToPaid FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE StudentID IN ($targetUserID) AND ItemID IN ($targetItemID) GROUP BY StudentID";
		 $sql = "SELECT a.StudentID, COUNT(*) as TotalItemNeedToPaid FROM PAYMENT_PAYMENT_ITEMSTUDENT a
				LEFT JOIN INTRANET_NOTICE_REPLY b ON a.StudentID=b.StudentID AND b.NoticeID='$NoticeID'
				WHERE a.StudentID IN ($targetUserID)
				AND ItemID IN ($targetItemID) 
				AND b.RecordStatus!=0
				GROUP BY a.StudentID";
	     
	     $arrTotalNoOfStudentNeedToPaid = $lnotice->returnArray($sql,2);
	     if(sizeof($arrTotalNoOfStudentNeedToPaid)>0)
	     {
	     	for($j=0; $j<sizeof($arrTotalNoOfStudentNeedToPaid); $j++)
	     	{
	     		list($student_id, $num_of_item_need_to_paid) = $arrTotalNoOfStudentNeedToPaid[$j];
	     		$arrNumOfItemNeedToPaied[$student_id]['NumOfItemNeedToPaid'] = $num_of_item_need_to_paid;
	     	}
	     }
	     
	     if($signed == 0)
	     {
	     	$total_num_of_class_student_need_to_paid = " - ";
	     }
	     else
	     {
		     if(sizeof($arrTotalNoOfStudentNeedToPaid)>0)
		     	$total_num_of_class_student_need_to_paid = sizeof($arrTotalNoOfStudentNeedToPaid);
		     else 
		     	$total_num_of_class_student_need_to_paid = " - ";
		     	
		     $num_of_whole_school_student_need_to_paid = $num_of_whole_school_student_need_to_paid + $total_num_of_class_student_need_to_paid;
		 }
		 
	     
	     $sql = "SELECT StudentID, COUNT(*) as TotalItemPaid FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE StudentID IN ($targetUserID) AND ItemID IN ($targetItemID) AND RecordStatus = 1 GROUP BY StudentID";
	     $arrStudentPaid = $lnotice->returnArray($sql,2);

	     for($j=0; $j<sizeof($arrStudentPaid); $j++){
	     	list($sid, $NumOfItemPaid) = $arrStudentPaid[$j];
	     	if($NumOfItemPaid == $arrNumOfItemNeedToPaied[$sid]['NumOfItemNeedToPaid'])
	     	{
	     		$cnt++;
	     	}
	     }
	     
	     
	     if($signed == 0)
	     {
	     	$num_of_class_student_paid_successfully = " - ";
	     }
	     else
	     {
	     	$num_of_class_student_paid_successfully = $cnt;
			$num_of_whole_school_student_paid_successfully = $num_of_whole_school_student_paid_successfully + $num_of_class_student_paid_successfully;
	     }
     	 $x .= "<td class='eSporttdborder eSportprinttext' width='80'>$total_num_of_class_student_need_to_paid</td>";
     	 $x .= "<td class='eSporttdborder eSportprinttext' width='80'>$num_of_class_student_paid_successfully</td>";
 	 }
     
     $x .= "</tr>\n";
     $all_total += $total;
     $all_signed += $signed;
}
$x .= "</table>\n";

### All student
$x .= "<br /><table width='90%' border='0' cellspacing='0' cellpadding='4' class='eSporttableborder'>\n";
$x .= "<tr><td class='eSporttdborder eSportprinttext'>$i_Notice_AllStudents</td>";
$x .= "<td class='eSporttdborder eSportprinttext' width='80'>$all_signed</td>";
$x .= "<td class='eSporttdborder eSportprinttext' width='80'>$all_total</td>";
if(strtoupper($lnotice->Module) == "PAYMENT")
{
	if($all_signed == 0){
		$num_of_whole_school_student_paid_successfully = " - ";
		$num_of_whole_school_student_need_to_paid = " - ";
	}
	$x .= "<td class='eSporttdborder eSportprinttext' width='80'>$num_of_whole_school_student_need_to_paid</td>";
	$x .= "<td class='eSporttdborder eSportprinttext' width='80'>$num_of_whole_school_student_paid_successfully</td>";
}
$x .= "</tr>\n";
$x .= "</table>\n";

?>

<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Notice Title -->
<table width='100%' align='center' border=0>
	<tr>
        	<td align='center' class='eSportprinttitle'><b><?=$i_Notice_ResultForEachClass?></b></td>
	</tr>
</table>      


<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="eSportprinttitle"><?=$i_Notice_NoticeNumber?> : <?=$lnotice->NoticeNumber?></td></tr>
<tr><td class="eSportprinttitle"><?=$i_Notice_Title?> : <?=$lnotice->Title?></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="5">

<!--  List -->
<tr>
	<td align="center"><?=$x?></td>
</tr>  

</table>
<br />

<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
