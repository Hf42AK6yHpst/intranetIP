<?php
# using:

##########################
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			updated access right checking
#
#   Date:   2019-09-12 Bill     [2019-0911-1635-34207]
#           prevent display 2 Reply Slip wordings
#
#   Date:   2018-06-21 Isaac
#           expended width of the notice table and reduce the width of the title td
#
# 	Date:	2018-02-01 (Isaac)
#			Fixed ui consistancy.
#
#	Date:	2018-01-29 (Isaac)
#			Added noitice number field.
#
#	Date: 	2016-06-10	Bill	[2016-0113-1514-09066]
#			remove target question syntax from question string
#
#	Date:	2015-10-28	Bill	[2015-0416-1040-06164]
#			support merge notice content
#
#	Date:	2015-07-27	Bill	[2015-0428-1214-11207]
# 			Display PICs to parent only $sys_custom['eNotice']['DisplayPICForParent'] = true
#
#	Date:	2015-05-18	Bill	[2015-0428-1214-11207]
# 			Improved: Add field to display PIC
#
#	Date:	2015-04-15	Bill
#			Improved: show details to eNotice PIC if Settings - "Allow PIC to edit replies for parents." - true [2015-0323-1602-46073]
#
#	Date:	2012-09-27	YatWoon
#			IP25 layout standard
#
#	Date:	2011-02-16	YatWoon
#			Add option "Display question number"
#
##########################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$NoticeID = IntegerSafe($NoticeID);
//$StudentID = IntegerSafe($StudentID);

$linterface = new interface_html();

$lform = new libform();
$lnotice = new libnotice($NoticeID);

$lu = new libuser($UserID);
$valid = true;
if ($lu->isStudent())
{
    $valid = false;
}

$isTeacher = $lu->isTeacherStaff();
$isParent = $lu->isParent();
if ($StudentID!="")
{
    if ($isParent)
    {
		$children = $lu->getChildren();
        if (!in_array($StudentID,$children))
        {
        	$valid = false;
        }
        else
        {
        	$today = time();
            $deadline = strtotime($lnotice->DateEnd);
            if (compareDate($today,$deadline)>0)
            {
            	$editAllowed = false;
                //$valid = false;
            }
            else
            {
                $editAllowed = true;
            }
        }
    }
    else # Teacher
    {
	    if($lnotice->IsModule)
	    {
			$editAllowed = true;    
	    }
	    else
	    {
            // [2020-0604-1821-16170]
        	// [2015-0323-1602-46073] also show details to eNotice PIC if Settings - "Allow PIC to edit replies for parents." - true
//        	$editAllowed = $lnotice->isEditAllowed($StudentID) || $lnotice>staffview;
            $isStaffView = strtoupper($lnotice->Module) == "PAYMENT" ? $lnotice->payment_staffview : $lnotice->staffview;
            // $editAllowed = $lnotice->isEditAllowed($StudentID) || $lnotice->staffview || ($StudentID && $lnotice->isNoticePIC($NoticeID));
            $editAllowed = $lnotice->isEditAllowed($StudentID) || $isStaffView || ($StudentID && $lnotice->isNoticePIC($NoticeID));
    	}
    }
}
else
{
    if ($isParent)
    {
        $valid = false;
    }
    else
    {
        $editAllowed = false;
    }
}

if ($editAllowed)
{
    $lnotice->retrieveReply($StudentID);
 	$temp_ans =  str_replace("&amp;", "&", $lnotice->answer);
	$ansString = $lform->getConvertedString($temp_ans);
	$ansString.=trim($ansString)." ";
}

if (!$valid && !$isTeacher)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","view.php?StudentID=$StudentID&NoticeID=$NoticeID");
	exit;
}

// [2015-0428-1214-11207] get PICs of eNotice
if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){
	$pics = "";
	$noticePICs = $lnotice->returnNoticePICNames();
	if(count($noticePICs) > 0){
		$pics = "";
		$delim = "";
		for($picCount=0; $picCount<count($noticePICs); $picCount++){
			$pics .= $delim.$noticePICs[$picCount]['UserName'];
			$delim = ", ";
	  	}
	}
}

$targetType = array("",$i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual);
$attachment = $lnotice->displayAttachment();
$reqFillAllFields = $lnotice->AllFieldsReq;
?>

<STYLE TYPE="text/css">
<!--
p.text1 {font-size: 60px;font-weight: bolder;}
-->
</STYLE>

<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Notice Title -->
<?/* ?>
<table width='100%' align='center' border=0>
	<tr>
        	<td align='center' class='field_title'><b><?=$i_Notice_ElectronicNotice2?></b></td>
	</tr>
</table>        
<? */ ?>
<?
// $notice_content = nl2br($lnotice->Description);
// [2015-0416-1040-06164] use getNoticeContent() to get display content
//$notice_content = htmlspecialchars_decode($lnotice->Description);
$notice_content = htmlspecialchars_decode($lnotice->getNoticeContent($StudentID));
//nl2br(intranet_convertAllLinks($lnotice->Description))
?>

<table class="form_table_v30"  align="center" style="margin:auto auto; width:100%">
<tr><td class="field_title" style="width:15%"><?=$i_Notice_DateStart?></td>
		<td><?=$lnotice->DateStart?></td></tr>
<tr><td class="field_title" style="width:15%"><?=$i_Notice_DateEnd?></td>
		<td><?=$lnotice->DateEnd?></td></tr>
 <tr><td class="field_title" style="width:15%"><?=$i_Notice_NoticeNumber?></td>
		<td><?=$lnotice->NoticeNumber?></td></tr>
<tr><td class="field_title" style="width:15%"><?=$i_Notice_Title?></td>
		<td><?=$lnotice->Title?></td></tr>
<tr><td class="field_title" style="width:15%"><?=$i_Notice_Description?></td>
		<td><?=$notice_content?></td></tr>

<? if(strtoupper($lnotice->Module) == "PAYMENT"){ ?>
<tr><td class="field_title"><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['Title']?> : <? echo ($lnotice->DebitMethod == 1) ? $Lang['eNotice']['FieldTitle']['DebitMethod']['DebitDirectly'] : $Lang['eNotice']['FieldTitle']['DebitMethod']['DebitLater'];?></td></tr>
<? } ?>

<? if ($attachment != "") { ?>
<tr><td class="field_title"><?=$i_Notice_Attachment?></td>
		<td><?=$attachment?></td></tr>
<? } ?>
<? if ($isTeacher) {
	$issuer = $lnotice->returnIssuerName();
?>
<tr><td class="field_title"><?=$i_Notice_Issuer?></td>
		<td><?=$issuer?></td></tr>
<? } ?>
<?php if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){ ?>
<tr><td class="field_title"><?=$i_Profile_PersonInCharge?></td>
	<td><?=$pics?></td></tr>
<? } ?>
<? if ($editAllowed) {
	if ($lnotice->replyStatus != 2)
	{
		$statusString = "$i_Notice_Unsigned";
	}
	else
	{
		$signer = $lu->getNameWithClassNumber($lnotice->signerID);
		$signby = ($lnotice->replyType==1? $i_Notice_Signer:$i_Notice_Editor);
		$statusString = "$i_Notice_Signed <I>($signby $signer $i_Notice_At ".$lnotice->signedTime.")</I>";
	}
?>
<tr><td class="field_title"><?=$i_Notice_SignStatus?></td>
		<td><?=$statusString?></td></tr>
<tr><td class="field_title"><?=$i_UserStudentName?></td>
		<td><?=$lu->getNameWithClassNumber($StudentID)?></td></tr>
<? } ?>
<tr><td class="field_title"><?=$i_Notice_RecipientType?></td>
		<td><?=$targetType[$lnotice->RecordType]?></td></tr>

<? if ($reqFillAllFields) { ?>
<tr><td class="field_title">[<?=$i_Survey_AllRequire2Fill?>]</td></tr>
<? } ?>

</table>

<? 
### check include reply slip or not
$hasContent = (trim($lnotice->Question) || trim($lnotice->ReplySlipContent)) ? true : false;
?>
<? if($hasContent) {?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align='center'><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/print_scissors.gif"></td>
</tr>
</table>

<table align="center" width="90%" border="0" cellpadding="0" cellspacing="0">

                <!-- Relpy Slip -->
                <tr valign='top'>
					<td colspan="2" class='field_title' align='center'><b><?=$i_Notice_ReplySlip?></b></td>
				</tr>
	
				<? if($lnotice->ReplySlipContent) {?>
				<!-- Content Base Start //-->
					<tr valign='top'>
						<td colspan="2">
						<?=$lnotice->ReplySlipContent?>
						</td>
					</tr>
				<? } else { 
					$temp_que =  str_replace("&amp;", "&", $lnotice->Question);
					$queString = $lform->getConvertedString($temp_que);
					$queString =trim($queString)." ";
				?>
                <tr valign='top'>
                    <td colspan="2">
                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                        	<td>
                        	<script language="javascript" src="/templates/forms/layer.js"></script>
                        	<? if(strtoupper($lnotice->Module) == "PAYMENT") { ?>
                        		<script language="javascript" src="/templates/forms/eNoticePayment_form_edit.js"></script>
                        	<? } else { ?>
                        		<script language="javascript" src="/templates/forms/form_edit.js"></script>
                        	<? } ?>	
                        
                                <form name="ansForm" method="post" action="update.php">
                                        <input type=hidden name="qStr" value="">
                                        <input type=hidden name="aStr" value="">
                                </form>
                                
                                <script language="Javascript">
                                
                                <?=$lform->getWordsInJS()?>
                                //var replyslip = '<?=$i_Notice_ReplySlip?>';
                                var replyslip = "";
                                var paymentOptions = "";
                                var paymentOptIDAry = new Array();
								var paymentOptNameAry = new Array();
								var paymentItemID = ""; 
								var paymentIDAry = new Array();
                                var DisplayQuestionNumber = '<?=$lnotice->DisplayQuestionNumber?>';
                                
                                var sheet= new Answersheet();
                                // attention: MUST replace '"' to '&quot;'
                                sheet.qString="<?=$queString?>";
                                sheet.qString="<?= preg_replace("/#TAR#(([A-Z]|[a-z]|[0-9])+)#/", "", $queString) ?>";
                                sheet.aString="<?=$ansString?>";
                                //edit submitted application
                                sheet.mode=1;
                                sheet.answer=sheet.sheetArr();
                                Part3 = '';
                                document.write(editPanel());
                                </script>
                                
                                <SCRIPT LANGUAGE=javascript>
                                function copyback()
                                {
                                         finish();
                                         document.form1.qStr.value = document.ansForm.qStr.value;
                                         document.form1.aStr.value = document.ansForm.aStr.value;
                                }
                                </SCRIPT>
                                
                        	</td>
						</tr>
                        </table>
                    </td>
                </tr>
				<? } ?>

</table>
<? } ?>
</td>
</tr>
</table>

<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>