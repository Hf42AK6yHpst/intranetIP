<?php
// using :

###############################
#
#   Date:   2020-04-17  Bill    [2020-0310-1051-05235]
#           Special notice > access checking for teacher    ($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'])
#
#	Date: 	2016-07-15	Bill
#			remove syntax from UI
#
#	Date: 	2016-06-10	Bill	[2016-0113-1514-09066]
#			remove target question syntax from question string
#
#	Date:	2015-11-16	Bill	[2015-0416-1040-06164]
#			support merge notice content
#
#	Date:	2015-07-27	Bill	[2015-0428-1214-11207]
# 			Display PICs to parent only $sys_custom['eNotice']['DisplayPICForParent'] = true
#
#	Date:	2015-05-18	Bill	[2015-0428-1214-11207]
# 			Improved: Add field to display PIC
#
#	Date:	2014-09-25	YatWoon
#			Fixed: class teacher failed to "preview notice" in eService [Case#L68335]
#			Deploy: ip.2.5.5.10.1
#
#	Date:	2014-08-29	YatWoon
#			Fixed: staff failed to view student notice in eService [Case#Z66399]
#			Deploy: ip.2.5.5.8.1 [Critical deploy #1]
#
#	Date:	2014-02-28	YatWoon
#			Add checking for student, is the student related to this notice? 
#
#	Date:	2011-02-16	YatWoon
#			Add option "Display question number"
#
###############################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

################################
# Pass in:
#	$NoticeID
#	$StudentID
################################
$NoticeID = IntegerSafe($NoticeID);
$StudentID = IntegerSafe($StudentID);

$lform = new libform();
$lnotice = new libnotice($NoticeID);

/*
if(strtoupper($lnotice->Module) == "PAYMENT"){
	echo "HERE";
	header("location : ePaymentNotice_view.php?NoticeID=$NoticeID&StudentID=$StudentID");
	die;
}
*/

$lu = new libuser($UserID);
$valid = true;
$isTeacher = $lu->isTeacherStaff();
$isParent = $lu->isParent();
$isStudent = $lu->isStudent();

if (!isset($StudentID) || $StudentID == "")
{
     $valid = false;
}
else
{
	if ($isStudent)
    {
        $StudentID = $UserID;
        
        ############################################################
		#	Check student belongs to this notice?
		############################################################
		if(!$lnotice->isStudentInNotice($NoticeID, $StudentID))
		{
			$valid = false;
		}
        
    }
    else if ($isParent)
    {
         $children = $lu->getChildren();
         if (!in_array($StudentID,$children))
         {
              $valid = false;
         }
    }
    else if ($isTeacher)
    {
    	if($StudentID)
    	{
    		$valid = $lnotice->hasViewRightForStudent($StudentID);
    	}
    	else
    	{
    		$valid = true;		// preview notice
    	}
    }
}

// [2020-0310-1051-05235] Special notice > access checking for teacher
$allowTeacherAccess = true;
if($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'] && $lnotice->SpecialNotice && $isTeacher)
{
    $allowTeacherAccess = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $lnotice->isNoticePIC($NoticeID);
}

//if (!$lnotice->showAllEnabled && !$valid)
//if (!$valid)
if (!$valid || !$allowTeacherAccess)
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

// [2015-0428-1214-11207] get PICs of eNotice
if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){
	$pics = "";
	$noticePICs = $lnotice->returnNoticePICNames();
	if(count($noticePICs) > 0){
		$delim = "";
		for($picCount=0; $picCount<count($noticePICs); $picCount++){
			$pics .= $delim.$noticePICs[$picCount]['UserName'];
			$delim = ", ";
	  	}
	}
}

if ($valid)
{
    $lnotice->retrieveReply($StudentID);
    $ansString = str_replace("\r\n", "<br>",$lnotice->answer);
}

$targetType = array("",$i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual);
if($lnotice->ModuleID=='')
{
	$attachment = $lnotice->displayAttachment();
}
else
{
	if(strtoupper($lnotice->Module) == "PAYMENT")
		$attachment = $lnotice->displayAttachment();
	else
		$attachment = $lnotice->displayModuleAttachment();
}

$MODULE_OBJ['title'] = $i_Notice_ElectronicNotice;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

?>

<table class="form_table_v30">
<tr valign='top'>
	<td class='field_title'><?=$i_Notice_DateStart?></td>
	<td><?=$lnotice->DateStart?></td>
</tr>
                
                <tr valign='top'>
	<td class='field_title'><?=$i_Notice_DateEnd?></td>
	<td><?=$lnotice->DateEnd?></td>
</tr>
                
                <tr valign='top'>
	<td class='field_title'><?=$i_Notice_Title?></td>
	<td><?=$lnotice->Title?></td>
</tr>
                
                <tr valign='top'>
	<td class='field_title'><?=$i_Notice_Description?></td>
	<!--<td><?="";//intranet_undo_htmlspecialchars($lnotice->Description)?></td>-->
	<td><?=intranet_undo_htmlspecialchars($lnotice->getNoticeContent($StudentID))?></td>
</tr>
                
                <? if ($attachment != "") { ?>
                <tr valign='top'>
	<td class='field_title'><?=$i_Notice_Attachment?></td>
	<td><?=$attachment?></td>
</tr>
                <? } ?>
                
                <? if ($isTeacher) {
		$issuer = $lnotice->returnIssuerName();
	?>
                <tr valign='top'>
	<td class='field_title'><?=$i_Notice_Issuer?></td>
	<td><?=$issuer?></td>
</tr>
        	<? } ?>

     <?php if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){ ?>
		<tr valign='top'>
			<td class='field_title'><?=$i_Profile_PersonInCharge?></td>
			<td><?=$pics?></td>
		</tr>
	<?php } ?>

                <?
                if ($valid)
                {
                        if ($lnotice->replyStatus != 2)
                        {
                            $statusString = "$i_Notice_Unsigned";
                        }
                        else
                        {
                            $signer = $lu->getNameWithClassNumber($lnotice->signerID);
                            $signby = ($lnotice->replyType==1? $i_Notice_Signer:$i_Notice_Editor);
                            $statusString = "$i_Notice_Signed <I>($signby $signer $i_Notice_At ".$lnotice->signedTime.")</I>";
                        }
                ?>
                	<tr valign='top'>
			<td class='field_title'><?=$i_Notice_SignStatus?></td>
			<td><?=$statusString?></td>
	</tr>
                	<tr valign='top'>
			<td class='field_title'><?=$i_UserStudentName?></td>
			<td><?=$lu->getNameWithClassNumber($StudentID)?></td>
	</tr>
                <? } ?>
                
<tr valign='top'>
	<td class='field_title'><?=$i_Notice_RecipientType?></td>
	<td><?=$targetType[$lnotice->RecordType]?></td>
</tr>
</table>

<? 
### check include reply slip or not
$hasContent = (trim($lnotice->Question) || trim($lnotice->ReplySlipContent)) ? true : false;
?>
<? if($hasContent) {?>
<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
	<td colspan="2">      
		<!-- Break -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/scissors_line.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/icons_scissors.gif"></td>
        </tr>
        </table>
	</td>
</tr>
                <br />                                
                                <table align="center" width="100%" border="0" cellpadding="0" cellspacing="0">
                                <!-- Relpy Slip -->
                                <tr valign='top'>
					<td colspan="2" class='eNoticereplytitle' align='center'><?=$Lang['eNotice']['ReplySlipForm']?></td>
				</tr>
				
				<? if($lnotice->ReplySlipContent) {?>
					<!-- Content Base Start //-->
						<tr valign='top'>
							<td colspan="2">
							<?=$lnotice->ReplySlipContent?>
							</td>
						</tr>
					<? } else { 
						$queString = $lform->getConvertedString($lnotice->Question);
					?>          
                                <tr>
                                	<td colspan="2">
                                        <? if ($lnotice->replyStatus == 2) { ?>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                                  <tr>
        						<td align="center" class="tabletext"><?=$i_Notice_ReplySlip_Signed?></td>
                                                  </tr>
                                                  <tr>
                                                  	<td align="left">
                                                
                                                	<script language="javascript" src="/templates/forms/form_view.js"></script>
                                                        <form name="ansForm" method="post" action="update.php">
                                                                <input type=hidden name="qStr" value="">
                                                                <input type=hidden name="aStr" value="">
                                                        </form>
                                                        
                                                        <script language="Javascript">
                                                        var DisplayQuestionNumber = '<?=$lnotice->DisplayQuestionNumber?>';
                                                        myQue="<?=$queString?>";
                                                       	myQue="<?= preg_replace("/#TAR#(([A-Z]|[a-z]|[0-9])+)#/", "", $queString) ?>";
                                                        myAns="<?=$ansString?>";
                                                        Part3 = '';
                                                        document.write(viewForm(myQue, myAns));
                                                        </SCRIPT>
                                                	</td>
        					</tr>
                                                </table>
                                	<? } else { ?>
                                        
                                                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                                <tr>
                                                	<td align=left>
                                                
                                                        <script language="javascript" src="/templates/forms/layer.js"></script>
                                                        <script language="javascript" src="/templates/forms/form_edit.js"></script>
                                                        
                                                        <form name="ansForm" method="post" action="update.php">
                                                                <input type=hidden name="qStr" value="">
                                                                <input type=hidden name="aStr" value="">
                                                        </form>
                                                        <script language="Javascript">
                                                        
                                                        <?=$lform->getWordsInJS()?>
                                                        var replyslip = '<?=$Lang['eNotice']['ReplySlip']?>';
                                                        var DisplayQuestionNumber = '<?=$lnotice->DisplayQuestionNumber?>';
                                                        var sheet= new Answersheet();
                                                        // attention: MUST replace '"' to '&quot;'
                                                        sheet.qString="<?=$queString?>";
                                                       	sheet.qString="<?= preg_replace("/#TAR#(([A-Z]|[a-z]|[0-9])+)#/", "", $queString) ?>";
                                                        sheet.aString="<?=$ansString?>";
                                                        //edit submitted application
                                                        sheet.mode=1;
                                                        sheet.answer=sheet.sheetArr();
                                                        Part3 = '';
                                                        document.write(editPanel());
                                                        </script>
                                                	</td>
                                                </tr>
                                                </table>
                                        <? } ?>
                                        </td>
                                </tr>
                    <? } ?>
				</table>
<? } ?>				
				
<form name="form1" action="view_print_preview.php" method="post" target="_blank"> 
<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "document.form1.submit()","cancelbtn") ?>
	<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn") ?>
<p class="spacer"></p>
</div>
<input type="hidden" name="NoticeID" value="<?=$NoticeID?>">
<input type="hidden" name="StudentID" value="<?=$StudentID?>">
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>