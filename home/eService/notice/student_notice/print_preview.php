<?php
// Using: 

######################################### !!! IMPORTANT !!! #############################################
###																									  ###
###  						[Case #B77398 - 港大同學會書院 - eClass Customization]						  ###
###   		     if upload this file to client site before ip.2.5.7.1.1, please find Bill first.	  ###
###																									  ###
######################################### !!! IMPORTANT !!! #############################################

#######################################
#
#   Date:   2018-11-12 Cameron
#           consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
#
#   Date:   2018-06-21 Isaac
#           expended width of the notice table
#
#	Date:	2017-12-22  Cameron
#			configure for Oaks refer to Amway
#
#	Date: 	2016-06-10	Bill	[2016-0113-1514-09066]
#			remove target question syntax from question string
#
#	Date:	2015-12-17  Cameron (#P89445)
#			show student name (distributor) only for Amway
#
#	Date:	2015-11-16	Bill	[2015-0416-1040-06164]
#			support merge notice content
#
#	Date:	2015-07-27	Bill	[2015-0428-1214-11207]
# 			Display PICs to parent only $sys_custom['eNotice']['DisplayPICForParent'] = true
#
#	Date:	2015-05-18	Bill	[2015-0428-1214-11207]
# 			Improved: Add field to display PIC
#
#	Date:	2011-02-16	YatWoon
#			Add option "Display question number"
#
#######################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$NoticeID = IntegerSafe($NoticeID);
$StudentID = IntegerSafe($StudentID);

$linterface = new interface_html();

$lform = new libform();
$lnotice = new libnotice($NoticeID);

$lu = new libuser($UserID);
$valid = true;
if ($lu->isStudent())
{
	$today = time();
    $deadline = strtotime($lnotice->DateEnd);
    if (compareDate($today,$deadline)>0)
    {
    	$editAllowed = false;
        //$valid = false;
    }
    else
        $editAllowed = true;
}

$isTeacher = $lu->isTeacherStaff();
$isParent = $lu->isParent();

if ($editAllowed)
{
    if ($StudentID == "")
    {
        $valid = false;
    }
    
	$lnotice->retrieveReply($StudentID);
 	$temp_ans =  str_replace("&amp;", "&", $lnotice->answer);
	$ansString = $lform->getConvertedString($temp_ans);
	$ansString.=trim($ansString)." ";

}

if (!$valid)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","view.php?StudentID=$StudentID&NoticeID=$NoticeID");
	exit;
}

// [2015-0428-1214-11207] get PICs of eNotice
if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){
	$pics = "";
	$noticePICs = $lnotice->returnNoticePICNames();
	if(count($noticePICs) > 0){
		$pics = "";
		$delim = "";
		for($picCount=0; $picCount<count($noticePICs); $picCount++){
			$pics .= $delim.$noticePICs[$picCount]['UserName'];
			$delim = ", ";
	  	}
	}
}

$targetType = array("",$i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual);
$attachment = $lnotice->displayAttachment();
$reqFillAllFields = $lnotice->AllFieldsReq;
?>


<STYLE TYPE="text/css">
<!--
p.text1 {font-size: 60px;font-weight: bolder;}
-->
</STYLE>

<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Notice Title -->
<?
// $notice_content = nl2br($lnotice->Description);
// [2015-0416-1040-06164] use getNoticeContent() to get display content
//$notice_content = htmlspecialchars_decode($lnotice->Description);
$notice_content = htmlspecialchars_decode($lnotice->getNoticeContent($StudentID));
//nl2br(intranet_convertAllLinks($lnotice->Description))
?>
<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
<tr><td class="eSportprinttitle"><?=$i_Notice_DateStart?> : <?=$lnotice->DateStart?></td></tr>
<tr><td class="eSportprinttitle"><?=$i_Notice_DateEnd?> : <?=$lnotice->DateEnd?></td></tr>
<tr><td class="eSportprinttitle"><?=$i_Notice_Title?> : <?=$lnotice->Title?></td></tr>
<tr><td class="eSportprinttitle"><?=$i_Notice_Description?> : <?=$notice_content?></td></tr>

<? if(strtoupper($lnotice->Module) == "PAYMENT"){ ?>
<tr><td class="eSportprinttitle"><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['Title']?> : <? echo ($lnotice->DebitMethod == 1) ? $Lang['eNotice']['FieldTitle']['DebitMethod']['DebitDirectly'] : $Lang['eNotice']['FieldTitle']['DebitMethod']['DebitLater'];?></td></tr>
<? } ?>

<? if ($attachment != "") { ?>
<tr><td class="eSportprinttitle"><?=$i_Notice_Attachment?> : <?=$attachment?></td></tr>
<? } ?>
<? if ($isTeacher) {
	$issuer = $lnotice->returnIssuerName();
?>
<tr><td class="eSportprinttitle"><?=$i_Notice_Issuer?> : <?=$issuer?></td></tr>
<? } ?>
<?php if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){ ?>
<tr><td class="eSportprinttitle"><?=$i_Profile_PersonInCharge?> : <?=$pics?></td></tr>
<?php } ?>
<? if ($editAllowed) {
	if ($lnotice->replyStatus != 2)
	{
		$statusString = "$i_Notice_Unsigned";
	}
	else
	{
		$signer = $lu->getNameWithClassNumber($lnotice->signerID);
		$signby = ($lnotice->replyType==1? $i_Notice_Signer:$i_Notice_Editor);
		$statusString = "$i_Notice_Signed <I>($signby $signer $i_Notice_At ".$lnotice->signedTime.")</I>";
	}
?>
<tr><td class="eSportprinttitle"><?=$i_Notice_SignStatus?> : <?=$statusString?></td></tr>
<tr><td class="eSportprinttitle"><?=$i_UserStudentName?> : 
<?php
    if ($sys_custom['project']['CourseTraining']['IsEnable']) {
		$ls = new libuser($StudentID);
		echo $ls->UserNameLang(0);
	}   
	else {
		echo $lu->getNameWithClassNumber($StudentID);
	}
?></td></tr>
<? } ?>
<tr><td class="eSportprinttitle"><?=$i_Notice_RecipientType?> : <?=$targetType[$lnotice->RecordType]?></td></tr>

<? if ($reqFillAllFields) { ?>
<tr><td class="eSportprinttitle">[<?=$i_Survey_AllRequire2Fill?>]</td></tr>
<? } ?>
				
</table>

<? 
### check include reply slip or not
$hasContent = (trim($lnotice->Question) || trim($lnotice->ReplySlipContent)) ? true : false;
?>
<? if($hasContent) {?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align='center'><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/print_scissors.gif"></td>
</tr>
</table>

<table align="center" width="90%" border="0" cellpadding="0" cellspacing="0">
                                <!-- Relpy Slip -->
                                <tr valign='top'>
					<td colspan="2" class='eSportprinttitle' align='center'><b><?=$i_Notice_ReplySlip?></b></td>
				</tr>
	
				<? if($lnotice->ReplySlipContent) {?>
				<!-- Content Base Start //-->
					<tr valign='top'>
						<td colspan="2">
						<?=$lnotice->ReplySlipContent?>
						</td>
					</tr>
				<? } else { 
					$temp_que =  str_replace("&amp;", "&", $lnotice->Question);
					$queString = $lform->getConvertedString($temp_que);
					$queString =trim($queString)." ";
				?>
                                <tr valign='top'>
					<td colspan="2">
                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                        	<td>
                        	<script language="javascript" src="/templates/forms/layer.js"></script>
                        	<? if(strtoupper($lnotice->Module) == "PAYMENT") { ?>
                        		<script language="javascript" src="/templates/forms/eNoticePayment_form_edit.js"></script>
                        	<? } else { ?>
                        		<script language="javascript" src="/templates/forms/form_edit.js"></script>
                        	<? } ?>	
                        
                                <form name="ansForm" method="post" action="update.php">
                                        <input type=hidden name="qStr" value="">
                                        <input type=hidden name="aStr" value="">
                                </form>
                                
                                <script language="Javascript">
                                
                                <?=$lform->getWordsInJS()?>
                                var replyslip = '<?=$i_Notice_ReplySlip?>';
                                var paymentOptions = "";
                                var paymentOptIDAry = new Array();
								var paymentOptNameAry = new Array();
								var paymentItemID = ""; 
								var paymentIDAry = new Array();
                                var DisplayQuestionNumber = '<?=$lnotice->DisplayQuestionNumber?>';
                                
                                var sheet= new Answersheet();
                                // attention: MUST replace '"' to '&quot;'
                                sheet.qString="<?=$queString?>";
                                sheet.qString="<?= preg_replace("/#TAR#(([A-Z]|[a-z]|[0-9])+)#/", "", $queString) ?>";
                                sheet.aString="<?=$ansString?>";
                                //edit submitted application
                                sheet.mode=1;
                                sheet.answer=sheet.sheetArr();
                                Part3 = '';
                                document.write(editPanel());
                                </script>
                                
                                <SCRIPT LANGUAGE=javascript>
                                function copyback()
                                {
                                         finish();
                                         document.form1.qStr.value = document.ansForm.qStr.value;
                                         document.form1.aStr.value = document.ansForm.aStr.value;
                                }
                                </SCRIPT>
                                
                        	</td>
						</tr>
                        </table>
                    </td>
				</tr>
				<? } ?>

				</table>
				<? } ?>
			</td>
                </tr>
                </table>

<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
