<?php
// using : 

############ Change Log Start ###############
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			updated access right checking
#
#	Date:	2019-04-15 Carlos
#			For Payment Notice, modified to render statistics result with PHP class eNoticePaymentReplySlip.php
#
#	Date:	2017-01-18 	Bill	[DM#3121]
#			Handle display special characters in answer
#
#	Date: 	2016-07-15	Bill	[2016-0113-1514-09066]
#			remove target question syntax from question string
#
#	Date:	2015-04-15	Bill	
#			Improved: allow eNotice PIC to access [2015-0323-1602-46073]
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/eNoticePaymentReplySlip.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$NoticeID = IntegerSafe($NoticeID);

$linterface = new interface_html("");

$lform = new libform();
$lnotice = new libnotice($NoticeID);
$queString = $lform->getConvertedString($lnotice->Question);
$lclass = new libclass();

$lu = new libuser($UserID);
if (!$lu->isTeacherStaff())
{
     include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT($i_general_no_access_right);
		exit;
}

$eNoticePaymentReplySlip = new eNoticePaymentReplySlip();

// [2020-0604-1821-16170]
// [2015-0323-1602-46073] - allow eNotice PIC to access
$isStaffView = strtoupper($lnotice->Module) == "PAYMENT" ? $lnotice->payment_staffview : $lnotice->staffview;
//if (!$lnotice->hasViewRight() && !$lnotice->staffview && $lnotice->IssuerUserID != $UserID && !$lnotice->isNoticePIC($NoticeID))
if (!$lnotice->hasViewRight() && !$isStaffView && $lnotice->IssuerUserID != $UserID && !$lnotice->isNoticePIC($NoticeID))
{
     //$class = $lnotice->returnClassTeacher($UserID);
     $class = $lclass->returnHeadingClass($UserID);
     if ($class=="")
     {
         include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT($i_general_no_access_right);
		exit;
     }
}

$hasEditRight = $lnotice->hasFullRightForClass($class);

// Applicable students & Signed/Total
if ($lnotice->RecordType == 4 && !$class)
{
    $class = "";
    $all = 1;
    $data = $lnotice->returnAllResult();
}
else
{
    if ($class != "")
    {
        $data = $lnotice->returnResultOfClass($class);
    }
    else
    {
        $data = $lnotice->returnAllResult();
        $all = 1;
    }
}

if (sizeof($data)==0)
{
    $x .= "$i_Notice_NoReplyAtThisMoment";
    $viewNeeded = false;
}
else
{
	// [DM#3121] handle special characters in data
	for ($j=0; $j<sizeof($data); $j++)
	{
		if ($data[$j]) {
	    	$data[$j] = intranet_undo_htmlspecialchars($data[$j]);
		}
	}
	
    $x = $lform->returnAnswerInJS($data);
    $viewNeeded = true;
}

$class_display = $class=="NULL" ? "--" : $class;

$is_payment_notice = strtoupper($lnotice->Module) == "PAYMENT";

?>

<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Notice Title -->
<table width='100%' align='center' border=0>
	<tr>
        	<td align='center' class='eSportprinttitle'><b><?=$i_Notice_ElectronicNotice2?><br /><?=$i_Notice_ViewStat?></b></td>
	</tr>
</table>        

<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="eSportprinttitle"><?=$i_Notice_NoticeNumber?> : <?=$lnotice->NoticeNumber?></td></tr>
<tr><td class="eSportprinttitle"><?=$i_Notice_Title?> : <?=$lnotice->Title?></td></tr>
<? if ($attachment != "") { ?>
<tr><td class="eSportprinttitle"><?=$i_Notice_Attachment?> : <?=$attachment?></td></tr>
<? } ?>
<? if ($all != 1) {?>
<tr><td class="eSportprinttitle"><?=$i_ClassName?> : <?=$class_display?></td></tr>
<? } else {?>
	<tr><td class="eSportprinttitle">
	<? switch($lnotice->RecordType)
	{
		case 2:	echo $i_Notice_RecipientTypeLevel; 	break;
		case 3:	echo $i_Notice_RecipientTypeClass; 	break;
		case 4:	echo $i_Notice_RecipientTypeIndividual; 	break;
	}
	?>
	</td></tr>
<? } ?>
</table>

				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                <tr valign='top'>
                                	<td colspan="2">
<?php
if($is_payment_notice){
	if($viewNeeded){
		echo $eNoticePaymentReplySlip->printStatisticsView($queString, $data, $lnotice->DisplayQuestionNumber==1, $isPreview=true);
	}else{
		echo $x;
	}
}else{
?>					
                                        <? if ($viewNeeded) { ?>
                                        <? if(strtoupper($lnotice->Module) == "PAYMENT") { ?>
                                        	<script language="javascript" src="/templates/forms/eNoticePayment_form_view_preview.js"></script>
                                    	<? } else { ?>
                                    		<script language="javascript" src="/templates/forms/form_view_preview.js"></script>
                                    	<? } ?>
                                        	<form name="ansForm" method="post" action="update.php">
                                                        <input type=hidden name="qStr" value="">
                                                        <input type=hidden name="aStr" value="">
                                                </form>
                                        
                                                <script language="Javascript">
                                                
                                                <?=returnFormStatWords()?>
                                                var DisplayQuestionNumber = '<?=$lnotice->DisplayQuestionNumber?>';
                                                w_total_no = "<?=$i_Notice_NumberOfSigned?>";
                                                w_show_details = "<?=$i_Form_ShowDetails?>";
                                                w_hide_details = "<?=$i_Form_HideDetails?>";
                                                w_view_details_img = "<img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_view.gif' border='0'>";
                                                var myQue = "<?=$queString?>";
												myQue="<?= preg_replace("/#TAR#(([A-Z]|[a-z]|[0-9])+)#/", "", $queString) ?>";
                                                var myAns = new Array();
                                        <? } ?>
                                        
                                                <?=$x?>
                                        
                                        <? if ($viewNeeded) { ?>
                                                var stats= new Statistics();
                                                stats.qString = myQue;
                                                stats.answer = myAns;
                                                stats.analysisData();
						document.write(stats.getStats());
                                                </SCRIPT>
                                        <? } ?>

<?php } ?>                                        
                                        </td>
				</tr>
                                
				</table>

                     
<br />

<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>