<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
//include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
//include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
//$lstudentprofile = new libstudentprofile();
//$lclass = new libclass();

# Check Access Right
if($_SESSION['UserType']!=2 && $_SESSION['UserType']!=3){ // only student / parent can view
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

if(!$ldiscipline->CHECK_ACCESS("Discipline-RANKING-Award_Ranking-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$StudentIdAry = Array();
if($_SESSION['UserType']==3)		# parent view
{
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	$luser = new libuser($UserID);

	$ChildrenAry = $luser -> getActiveChildrenList();
	
	if(count($ChildrenAry)>1)		# parent with more than 1 child
	{
		if($student_id=='' || $student_id=='0') {
			for($i=0; $i<count($ChildrenAry); $i++)
				$studentIDAry[$i] = $ChildrenAry[$i][0]; 
		} else {
			$studentIDAry[] = $student_id;	
		}

	}
	else							# parent with only 1 child
	{
		$studentIDAry[] = $ChildrenAry[0][0];
		
	}
}
else		# student view
{
	$studentIDAry[] = $_SESSION['UserID'];
}

foreach($studentIDAry as $sid) {

	#get User Information
	$stdName = $ldiscipline->getStudentNameByID($sid);
	list($userID, $std_name, $className, $classNumber) = $stdName[0];
	$classID = $ldiscipline->getClassIDByClassName($className);
	
	# get student class level
	//$classlevel = $lclass -> getClassLevelByClassName($className);
	$sql = " SELECT 
				b.YearID, 
				b.YearName 
			FROM 
				YEAR_CLASS a 
				LEFT JOIN YEAR b ON a.YearID = b.YearID 
			WHERE 
				a.YearClassID=$classID";
	$classlevel = $ldiscipline->returnArray($sql,2);
	list ($ClassLevelID, $LevelName) = $classlevel[0];
	
		$meritTitle = $i_Discipline_System_Stat_Others_Top_Award_Student;	
		$conds .= " AND a.MeritType=1";
	$semester = 'WholeYear';
	$semesterText =  $i_Discipline_System_Award_Punishment_Whole_Year;
	
			$date = getCurrentAcademicYear()." ".$i_Discipline_System_Award_Punishment_Whole_Year;
			//$conds .= " AND a.Year='".getCurrentAcademicYear()."'";
			$conds .= " AND a.AcademicYearID = ".Get_Current_Academic_Year_ID();
	
		
	$tempConds = "";
	
	$groupBy = "UserID";
	
	$showStudentName = ($_SESSION['UserType']==3) ? "$std_name &gt; " : "";
	
	for($i=0;$i<2;$i++)
	{
	
		//$conds2 = ($j == 0 ? (" AND d.ClassTitleEN = '".$className."' " ):( " AND d.YearID = ".$ClassLevelID ));
		$conds2 = ($i == 0) ? " AND d.YearClassID = $classID" : " AND d.YearID = ".$ClassLevelID;
		
		$name_field = getNameFieldByLang('b.');
		$sql = "SELECT
					$name_field as name,
					COUNT(*) as total,
					d.ClassTitleEN,
					e.YearName as form
				FROM
					DISCIPLINE_MERIT_RECORD as a
					LEFT OUTER JOIN
						INTRANET_USER as b ON (a.StudentID=b.UserID)
					LEFT OUTER JOIN
						YEAR_CLASS_USER as c ON (b.UserID=c.UserID)
					LEFT OUTER JOIN 
						YEAR_CLASS as d ON (c.YearClassID=d.YearClassID)
					LEFT OUTER JOIN
						YEAR as e ON (e.YearID = d.YearID)
				WHERE
					a.DateInput IS NOT NULL
					$conds
					$conds2
					GROUP BY b.UserID
					ORDER BY total DESC
					LIMIT 10
				";
	
		$result = $ldiscipline->returnArray($sql,3);
	
	
		//$tableContent .= "<br>".$i_Discipline_System_Stat_Others_Top_Award_Student.($i == 0? ($eDiscipline["ClassRank"].$className):($eDiscipline["ClassLevelRank"].$LevelName)) ;
		if($intranet_session_language == "en") 
			$tableContent .= "<br>$showStudentName".($i == 0? ($i_Discipline_System_Stat_Others_Top_Award_Student." in ".$className):($i_Discipline_System_Stat_Others_Top_Award_Student." in ".$LevelName)) ;
		else if($intranet_session_language == "b5")
			$tableContent .= "<br>$showStudentName".($i == 0? ($className.$eDiscipline['Class'].$i_Discipline_System_Stat_Others_Top_Award_Student):($LevelName.$eDiscipline['Form'].$i_Discipline_System_Stat_Others_Top_Award_Student)) ;
	
		$tableContent .= "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
		$tableContent .= "<tr class='tablebluetop'>";
		$tableContent .= "<td width='15%'><a class='tabletoplink'>{$iDiscipline['Rank']}</a></td>";
		$tableContent .= $i == 1?"<td><a class='tabletoplink'>{$i_Discipline_Class} </a></span></td>":"";
		$tableContent .= "<td width='30%'><a class='tabletoplink'>{$i_Discipline_Student} </a></span></td>";
		$tableContent .= "<td width='25%'><a class='tabletoplink'>{$i_Discipline_No_of_Records}</a></td>";
		$tableContent .= "</tr>";
	
		if(sizeof($result)==0) {
			$tableContent .= "<tr class='tablebluerow1'>";
			$tableContent .= "<td class='tabletext' colspan='4' height='40' align='center'>{$i_no_record_exists_msg}</td>";
			$tableContent .= "</tr>";
		}
	
		for($j=0;$j<sizeof($result);$j++) {
			$k = $j+1;
			$css = ($j%2)+1;
			$tableContent .= "<tr class='tablebluerow{$css}'>";
			$tableContent .= "<td class='tabletext'>{$k}</td>";
			$tableContent .= $i == 1?"<td>{$result[$i][2]}</td>":"";
			$tableContent .= "<td>{$result[$j][0]}</td>";
			$tableContent .= "<td>{$result[$j][1]}</td>";
			$tableContent .= "</tr>";
		}
		$tableContent .= "</table>";
	}
	$tableContent .= "<br>";
}

$CurrentPageArr['eServiceeDisciplinev12'] = 1;

# menu highlight setting
$TAGS_OBJ[] = array($eDiscipline['AwardRanking'], "");
$CurrentPage = "Ranking_Award";

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>

<form name="form1" method="post" action="ranking_report_view.php" onSubmit="return goCheck(this)">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<br>
			<table width="88%" border="0" cellpadding="5" cellspacing="0" class="result_box">
				<tr>
					<td>
						<table border="0" cellspacing="5" cellpadding="5" align="center">
							<tr>
								<td align="center" valign="middle" class="Chart_title"><?=$date?><?=$meritTitle?><?=$criteria?></td>
							</tr>
						</table>
						<br>
						<?=$tableContent?>

					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" name="showReportOption" value="0">
<input type="hidden" name="studentFlag" id="studentFlag" value="<? if(sizeof($studentID)>0){echo "1";} else { echo "0";} ?>">			
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
