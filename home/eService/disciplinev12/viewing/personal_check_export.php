<?php
# using by : Bill 

###############################################
#
#   Date:   2020-11-24 Bill     [IP30 DM#1014]
#           pass AcademicYearID to getTermIDByTermName() to ensure return correct YearTermID
#
#	Date:	2017-06-28 (Bill)	[2015-0120-1200-33164]
#			show Study Score column if Study Score is enabled
#
#	Date:	2014-03-06 (YatWoon)
#			remove "Year" selection (due to misconfuse with Date range)
#
#	Date:	2011-03-22	YatWoon
#			add flag checking $sys_custom['Discipline_HideRemarkInPersonalReport']
#
###############################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
//include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$TargetStudentID = IntegerSafe($TargetStudentID);
$detailType = IntegerSafe($detailType);
$show_waive_status = IntegerSafe($show_waive_status);
$targetID = IntegerSafe($targetID);

$ldiscipline = new libdisciplinev12();
//$lsp = new libstudentprofile();

if($_SESSION['UserType']==3)	# parent
{
	if(!empty($targetID))
	{
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		$luser = new libuser($UserID);
		$ChildrenAry = $luser -> getChildrenList();
	
		#################################################
		# Check the child is belongs to parent or not
		#################################################
		$valid_child = false;
		foreach($ChildrenAry as $k => $d)
		{
			if($d['StudentID'] == $targetID) $valid_child = true;	
		}
		if(!$valid_child)
		{
			$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
			exit;
		}
	}
}
else if($_SESSION['UserType']==2)	# student
{
	if($targetID!=$UserID)
	{
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

$lexport = new libexporttext();

# Retrieve Student Name
$lu_student = new libuser($targetID);
$student_name = $lu_student->UserName();
$class_name = $lu_student->ClassName;
$class_num = $lu_student->ClassNumber;

$academicYearID = $ldiscipline->getAcademicYearIDByYearName($year);
// [IP30 DM#1014]
//$yearTermID = $ldiscipline->getTermIDByTermName($semester);
$yearTermID = $ldiscipline->getTermIDByTermName($semester, $academicYearID);

$show_remark_in_personal_report = !$sys_custom['Discipline_HideRemarkInPersonalReport'];

/*
$year_info = "\"".$i_Profile_Year."\"\t\"".str_replace('"','""',$year)."\"\n";
$utf_year_info = $i_Profile_Year."\t".$year."\t\r\n";

if($semester=="")
	$str_semester = $i_status_all;
else $str_semester = $semester;
$semester_info = "\"".$i_SettingsSemester."\"\t\"".str_replace('"','""',$str_semester)."\"\n\n";
$utf_semester_info = $i_SettingsSemester."\t".$str_semester."\t\r\n";
*/

$header  = "\"$i_UserStudentName\"\t\"".$student_name."(".str_replace('"','""',$class_name)." - ".str_replace('"','""',$class_num).")\"\n";
$header .= "\"".$i_general_startdate."\"\t\"".str_replace('"','""',$startdate)."\"\n";
$header .= "\"".$i_general_enddate."\"\t\"".str_replace('"','""',$enddate)."\"\n";
$header .= $year_info;
$header .= $semester_info;

$preheader = "Student Name\t".$student_name."(".$class_name." - ".$class_num.")\t\r\n";
$preheader .= $i_general_startdate.":\t".$startdate."\t\r\n";
$preheader .= $i_general_enddate.":\t".$enddate."\t\r\n";
$preheader .= $utf_year_info;
$preheader .= $utf_semester_info."\n";
//$preheader = iconv('BIG5', 'UTF-16LE', $preheader);

$content = "";
$ReleaseStatus = DISCIPLINE_STATUS_RELEASED;

$conds = " AND a.ReleaseStatus=$ReleaseStatus";

switch($detailType)
{
        case 1:	# Punihsment Record
                $string_type["0"] = $i_Merit_Warning;
                $string_type["-1"] = $i_Merit_BlackMark;
                $string_type["-2"] = $i_Merit_MinorDemerit;
                $string_type["-3"] = $i_Merit_MajorDemerit;
                $string_type["-4"] = $i_Merit_SuperDemerit;
                $string_type["-5"] = $i_Merit_UltraDemerit;

                // Retrieve Demerit Record
                $result = $ldiscipline->retrieveStudentDemeritRecord($targetID, $startdate, $enddate, $academicYearID, $yearTermID, $show_waive_status, $conds);

                // Build Table Header
                $header .= "\"".$i_Discipline_System_general_date_demerit."\"\t\"".$i_Discipline_System_general_record."\"\t\"".$i_Discipline_System_Add_Demerit."\"";
                $exportColumn = array($i_Discipline_System_general_date_demerit, $i_Discipline_System_general_record, $i_Discipline_System_Add_Demerit);
                
                if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'])
                {
	                $header .= "\t\"".$i_Discipline_System_Conduct_ScoreDifference."\"";
	                 $exportColumn[] = $i_Discipline_System_Conduct_ScoreDifference;
            	}
                
                if ($ldiscipline->UseSubScore || $ldiscipline->use_sub_score1) {
	                $header .= "\t\"".$i_Discipline_System_Subscore1_ScoreDifference."\""; #.(($sys_custom['DisciplineShowScoreAfter']) ? ",\"$i_Discipline_System_Subscore1_UpdatedScore\"" : "");
	                $exportColumn[] = $i_Discipline_System_Subscore1_ScoreDifference;
                }
                if ($ldiscipline->use_subject) {
	                $header .= "\t\"".$i_Subject_name."\"";
	                $exportColumn[] = $i_Subject_name;
                }
                $header .= "\t\"".$i_Profile_PersonInCharge."\"";
                $exportColumn[] = $i_Profile_PersonInCharge;
				if($show_waive_status==1) {
					$header.="\t\"".$i_Discipline_System_WaiveStatus."\"";
					 $exportColumn[] = $i_Discipline_System_WaiveStatus;
				}
				if($show_remark_in_personal_report)
				{
					$header.="\t\"".$i_Discipline_System_general_remark."\"";
					 $exportColumn[] = $i_Discipline_System_general_remark;
				}
								
				$header.="\n";
				$exportColumn[] = "\n";
				
                for ($i=0; $i<sizeof($result); $i++)
                {
                        list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count, $r_conduct, $r_conduct_after, $t_subscore1, $t_subscore1_after, $t_subject, $r_picid, $r_pic_name,$record_status, $remark) = $result[$i];
                        $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
                        
                        if($r_profile_count!=0) {
	                        $displayMerit = ($intranet_session_language=="en") ? str_replace('"','""',$ldiscipline->TrimDeMeritNumber_00($r_profile_count))." ".str_replace('"','""',$string_type[$r_profile_type]) : str_replace('"','""',$string_type[$r_profile_type]).str_replace('"','""',$ldiscipline->TrimDeMeritNumber_00($r_profile_count)).$Lang['eDiscipline']['Times'];
                        	//$record_data = str_replace('"','""',$r_profile_count)." ".str_replace('"','""',$string_type[$r_profile_type]);
                        	$record_data = $displayMerit;
                    	}
                        else {
                        	//$record_data = "(". $i_general_na .")";
                        	$record_data = "--";
                    	}
                        	
                        $content .= "\"".str_replace('"','""',$r_date)."\"\t\"".str_replace('"','""',$r_item)."\"\t\"".$record_data."\"";                                                
                        //$row = array($r_date, $r_item, $ldiscipline->TrimDeMeritNumber_00($r_profile_count)." ".$string_type[$r_profile_type]);
                        $row = array($r_date, $r_item, $record_data);
                        
                        if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'])
                        {
	                        $content .= "\t\"".str_replace('"','""',$r_conduct)."\"";
	                        $row[] = $r_conduct;
                        }
                        
                        if ($ldiscipline->UseSubScore || $ldiscipline->use_sub_score1) {
	                        $content .= "\t\"".str_replace('"','""',$t_subscore1)."\""; #.(($sys_custom['DisciplineShowScoreAfter']) ? ",\"$t_subscore1_after\"" : "");
	                        $row[] = $t_subscore1;
                        }
                        if ($ldiscipline->use_subject) {
	                        $content .= "\t\"".str_replace('"','""',$t_subject)."\"";
	                        $row[] = $t_subject;
                        }
                        $content .= "\t\"".str_replace('"','""',$r_pic_name)."\"";
                        $row[] = $r_pic_name;
                        if($show_waive_status==1) {
	                        $content.="\t\"".str_replace('"','""',$str_waive_status)."\"";
	                        $row[] = $str_waive_status;
                        }
                        if($show_remark_in_personal_report)
                        {
	                        $content.="\t\"".str_replace('"','""',$remark)."\"";
	                        $row[] = $remark;
                        }
                  		
                        $row_result[] = $row;
                        unset($row);
                        $content.="\n";
                }

                $filename = "punishment_record_".$targetID.".csv";
                break;
        case 2:	# Award Record
                $string_type["1"] = $i_Merit_Merit;
                $string_type["2"] = $i_Merit_MinorCredit;
                $string_type["3"] = $i_Merit_MajorCredit;
                $string_type["4"] = $i_Merit_SuperCredit;
                $string_type["5"] = $i_Merit_UltraCredit;

                // Retrieve Merit Record
                $result = $ldiscipline->retrieveStudentMeritRecord($targetID, $startdate, $enddate, $academicYearID, $yearTermID, $show_waive_status, $conds);

                // Build Table Header
                $header .= "\"".$i_Discipline_System_general_date_merit."\"\t\"".$i_Discipline_System_general_record."\"\t\"".$i_Discipline_System_Add_Merit."\"";
                $exportColumn = array($i_Discipline_System_general_date_merit, $i_Discipline_System_general_record, $i_Discipline_System_Add_Merit);
                
                if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'])
                {
	                $header .= "\t\"".$i_Discipline_System_Conduct_ScoreDifference."\"";
	                 $exportColumn[] = $i_Discipline_System_Conduct_ScoreDifference;
            	}
            	
                if ($ldiscipline->UseSubScore || $ldiscipline->use_sub_score1) {
	                $header .= "\t\"".$i_Discipline_System_Subscore1_ScoreDifference."\""; #.(($sys_custom['DisciplineShowScoreAfter']) ? ",\"$i_Discipline_System_Subscore1_UpdatedScore\"" : "");
	                $exportColumn[] = $i_Discipline_System_Subscore1_ScoreDifference;
                }
                $header .= "\t\"".$i_Profile_PersonInCharge."\"";
                $exportColumn[] = $i_Profile_PersonInCharge;
                
                if($show_waive_status==1) {
					$header.="\t\"".$i_Discipline_System_WaiveStatus."\"";
					 $exportColumn[] = $i_Discipline_System_WaiveStatus;
				}
                
                if($show_remark_in_personal_report)
                {
					$header.="\t\"".$i_Discipline_System_general_remark."\"";
					$exportColumn[] = $i_Discipline_System_general_remark;
                }

                $header.="\n";
                $exportColumn[] = "\n";
                
                for ($i=0; $i<sizeof($result); $i++)
                {
                        list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count, $r_conduct, $r_conduct_after, $t_subscore1, $t_subscore1_after, $r_picid, $r_pic_name, $remark, $record_status) = $result[$i];
                        $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
                        
                        if($r_profile_count!=0) {
	                        $displayMerit = ($intranet_session_language=="en") ? str_replace('"','""',$ldiscipline->TrimDeMeritNumber_00($r_profile_count))." ".str_replace('"','""',$string_type[$r_profile_type]) : str_replace('"','""',$string_type[$r_profile_type]).str_replace('"','""',$ldiscipline->TrimDeMeritNumber_00($r_profile_count)).$Lang['eDiscipline']['Times'];
                        	//$record_data = str_replace('"','""',$r_profile_count)." ".str_replace('"','""',$string_type[$r_profile_type]);
                        	$record_data = $displayMerit;
                    	}
                        else {
                        	//$record_data = "(". $i_general_na .")";
                        	$record_data = "--";
                    	}
                        	
                        $content .= "\"".str_replace('"','""',$r_date)."\"\t\"".str_replace('"','""',$r_item)."\"\t\"". $record_data ."\"";
                        //$row = array($r_date, $r_item, $ldiscipline->TrimDeMeritNumber_00($r_profile_count)." ".$string_type[$r_profile_type]);
                        $row = array($r_date, $r_item, $record_data);
                        
                        if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'])
                        {
	                        $content .= "\t\"".str_replace('"','""',$r_conduct)."\"";
	                        $row[] = $r_conduct;
                        }
                        
                        if ($ldiscipline->UseSubScore || $ldiscipline->use_sub_score1) {
	                        $content .= "\t\"".str_replace('"','""',$t_subscore1)."\""; #.(($sys_custom['DisciplineShowScoreAfter']) ? ",\"$t_subscore1_after\"" : "");
	                        $row[] = $t_subscore1;
                        }
                        $content .= "\t\"".str_replace('"','""',$r_pic_name)."\"";
                        $row[] = $r_pic_name;
                        
                        if($show_waive_status==1) {
	                        $content.="\t\"".str_replace('"','""',$str_waive_status)."\"";
	                        $row[] = $str_waive_status;
                        }
                        
                        if($show_remark_in_personal_report)
                        {
	                        $content.="\t\"".str_replace('"','""',$remark)."\"";
	                        $row[] = $remark;
                        }
                        
                        $content .= "\n";
                        
                        $row_result[] = $row;
                        unset($row);
                }
                $filename = "award_record_".$targetID.".csv";
                break;
        case 4:	# Award/Punihsment Record
                $string_type["0"] = $i_Merit_Warning;
                $string_type["-1"] = $i_Merit_BlackMark;
                $string_type["-2"] = $i_Merit_MinorDemerit;
                $string_type["-3"] = $i_Merit_MajorDemerit;
                $string_type["-4"] = $i_Merit_SuperDemerit;
                $string_type["-5"] = $i_Merit_UltraDemerit;
                $string_type["1"] = $i_Merit_Merit;
                $string_type["2"] = $i_Merit_MinorCredit;
                $string_type["3"] = $i_Merit_MajorCredit;
                $string_type["4"] = $i_Merit_SuperCredit;
                $string_type["5"] = $i_Merit_UltraCredit;


                
                // Retrieve Merit/Demerit Record
                $result = $ldiscipline->retrieveStudentProfileRecord($targetID, $startdate, $enddate, $academicYearID, $yearTermID, $show_waive_status, $conds);

                // Build Table Header
                if ($sys_custom['Discipline_ShowCategoryInPersonalReport']) 
                {
                	$header .= "\"".$i_general_record_date."\"\t\"".$i_Discipline_System_CategoryName."\"\t\"".$i_Discipline_System_general_record."\"\t\"".$i_Discipline_System_Add_Merit_Demerit."\"";
                	$exportColumn = array($i_general_record_date, $i_Discipline_System_CategoryName, $i_Discipline_System_general_record, $i_Discipline_System_Add_Merit_Demerit);
            	}
            	else
            	{
	            	$header .= "\"".$i_general_record_date."\"\t\"".$i_Discipline_System_general_record."\"\t\"".$i_Discipline_System_Add_Merit_Demerit."\"";
                	$exportColumn = array($i_general_record_date, $i_Discipline_System_general_record, $i_Discipline_System_Add_Merit_Demerit);	
            	}
            	
            	if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'])
                {
	                $header .= "\t\"".$i_Discipline_System_Conduct_ScoreDifference."\"";
	                 $exportColumn[] = $i_Discipline_System_Conduct_ScoreDifference;
            	}
            	
                if ($ldiscipline->UseSubScore || $ldiscipline->use_sub_score1) {
	                $header .= "\t\"".$i_Discipline_System_Subscore1_ScoreDifference."\""; #.(($sys_custom['DisciplineShowScoreAfter']) ? ",\"$i_Discipline_System_Subscore1_UpdatedScore\"" : "");
	                $exportColumn[] = $i_Discipline_System_Subscore1_ScoreDifference;
                }
                
                if ($ldiscipline->use_subject) {
	                $header .= "\t\"".$i_Subject_name."\"";
	                $exportColumn[] = $i_Subject_name;
                }
                
                $header .= "\t\"".$i_Profile_PersonInCharge."\"";
                $exportColumn[] = $i_Profile_PersonInCharge;
                
                if($show_waive_status==1) {
                	$header.="\t\"".$i_Discipline_System_WaiveStatus."\"";
                	$exportColumn[] = $i_Discipline_System_WaiveStatus;
            	}
            	if($show_remark_in_personal_report)
				{
					$header.="\t\"".$i_Discipline_System_general_remark."\"";
					$exportColumn[] = $i_Discipline_System_general_remark;
				}
                $header.="\n";
                $exportColumn[] = "\n";

                for ($i=0; $i<sizeof($result); $i++)
                {
                        list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count, $r_conduct, $r_conduct_after, $t_subscore1, $t_subscore1_after, $t_subject, $r_picid, $r_pic_name,$record_status, $MeritType, $remark, $cat_name) = $result[$i];
                       
                        $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
                        
                        $content .= "\"".str_replace('"','""',$r_date)."\",";
                        
                        if($sys_custom['Discipline_ShowCategoryInPersonalReport'])
                        	$content .= "\"".str_replace('"','""',$cat_name)."\",";
                        	
                        if($r_profile_count!=0) {
	                        $displayMerit = ($intranet_session_language=="en") ? str_replace('"','""',$ldiscipline->TrimDeMeritNumber_00($r_profile_count))." ".str_replace('"','""',$string_type[$r_profile_type]) : str_replace('"','""',$string_type[$r_profile_type]).str_replace('"','""',$ldiscipline->TrimDeMeritNumber_00($r_profile_count)).$Lang['eDiscipline']['Times'];
                        	//$record_data = str_replace('"','""',$r_profile_count)." ".str_replace('"','""',$string_type[$r_profile_type]);
                        	$record_data = $displayMerit;
                    	}
                        else {
                        	//$record_data = "(". $i_general_na .")";
                        	$record_data = "--";
                    	}
                        	
                        $content .="\"".str_replace('"','""',$r_item)."\"\t\"".$record_data."\"";
                        //$row = array($r_date, $r_item, $ldiscipline->TrimDeMeritNumber_00($r_profile_count)." ".$string_type[$r_profile_type]);
                        $row = array($r_date, $r_item, $record_data);
                        
                        if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'])
                        {
	                        $content .= "\t\"".str_replace('"','""',$r_conduct)."\"";
	                        $row[] = $r_conduct;
                        }
                        
                        if ($ldiscipline->UseSubScore || $ldiscipline->use_sub_score1) {
	                        $content .= "\t\"".str_replace('"','""',$t_subscore1)."\""; #.(($sys_custom['DisciplineShowScoreAfter']) ? ",\"$t_subscore1_after\"" : "");
	                        $row[] = $t_subscore1;
                        }
                        if ($ldiscipline->use_subject) {
	                        $content .= "\t\"".str_replace('"','""',$t_subject)."\"";
	                        $row[] = $t_subject;
                        }
                        $content .= "\t\"".str_replace('"','""',$r_pic_name)."\"";
                        $row[] = $r_pic_name;
                        if($show_waive_status==1) {
                        	$content.="\t\"".str_replace('"','""',$str_waive_status)."\"";
                        	$row[] = $str_waive_status;
                    	}
                    	if($show_remark_in_personal_report)
                        {
	                        $content.="\t\"".str_replace('"','""',$remark)."\"";
	                        $row[] = $remark;
                        }
                        $row[] = "\n";
                    	$row_result[] = $row;
                    	unset($row);
                        $content.="\n";
                }
                $filename = "award_punishment_record_".$targetID.".csv";
                break;
             case 6: # Conduct Mark Change Log
             		$result = $ldiscipline->retrieveConductScoreChangeLogRecord($targetID,$startdate,$enddate,$academicYearID,$yearTermID);
             		$header .= "\"".$i_general_record_date."\"\t\"".$i_Merit_Type."\"\t\"".$i_Discipline_System_general_record."\"\t\"".$i_Discipline_System_FromScore."\",";
                    $header .= "\"".$i_Discipline_System_ToScore."\"\t\"".$i_Profile_PersonInCharge."\"";
                    
                    if($show_remark_in_personal_report)
                    {
	                    $header .= "\t\"".$i_Discipline_System_general_remark."\"";
                    }
                    $header .= "\n";
                    
                    $exportColumn = array($i_general_record_date, $i_Merit_Type, $i_Discipline_System_general_record, $i_Discipline_System_FromScore, $i_Discipline_System_ToScore, $i_Profile_PersonInCharge);
                    
		             for ($i=0; $i<sizeof($result); $i++)
		             {
		                  list ($r_date, $r_action_type, $r_item, $r_fromscore, $r_toscore,$r_pic_name, $remark) = $result[$i];
		
		                  switch($r_action_type){
			                  case 1 : $action = $i_Discipline_System_New_MeritDemerit; break;
			                  case 2 : $action = $i_Discipline_System_Edit_MeritDemerit; break;
			                  case 3 : $action = $i_Discipline_System_Remove_MeritDemerit; break;
			                  case 4 : $action = $i_Discipline_System_Manual_Adjustment; break;
			                  case 5 : $action = $i_Discipline_System_System_Revised; break;
			              }
			              if($r_item =="--" && $r_action_type!=5){
				              $r_item = "$i_Discipline_System_Record_Already_Removed";
				          }
		                  
				          if(!$show_remark_in_personal_report)
				          {
								$content .= "\"".str_replace('"','""',$r_date)."\"\t\"".str_replace('"','""',$action)."\"\t\"".str_replace('"','""',$r_item)."\"\t\"".str_replace('"','""',$r_fromscore)."\"\t\"".str_replace('"','""',$r_toscore)."\"\t\"".str_replace('"','""',$r_pic_name)."\"\n";
								$row_result[] = array($r_date, $action, $r_item, $r_fromscore, $r_toscore, $r_pic_name);
				          }
				          else
				          {
				          		$content .= "\"".str_replace('"','""',$r_date)."\"\t,\"".str_replace('"','""',$action)."\"\t\"".str_replace('"','""',$r_item)."\"\t\"".str_replace('"','""',$r_fromscore)."\"\t\"".str_replace('"','""',$r_toscore)."\"\t\"".str_replace('"','""',$r_pic_name)."\"\t\"".str_replace('"','""',$remark)."\"\n";
								$row_result[] = array($r_date, $action, $r_item, $r_fromscore, $r_toscore, $r_pic_name, $remark);
					  		}
					  	unset($row);
		             }
                  	$filename = "conduct_score_change_log_".$targetID.".csv";
             		break;
             case 7: # Subscore Change Log
                    $result = $ldiscipline->retrieveSubscore1ChangeLogRecord($targetID,$startdate,$enddate,$academicYearID,$yearTermID);
                    $header .= "\"".$i_general_record_date."\"\t\"".$i_Merit_Type."\"\t\"".$i_Discipline_System_general_record."\"\t\"".$i_Discipline_System_FromScore."\",";
                    $header .="\"".$i_Discipline_System_ToScore."\"\t\"".$i_Profile_PersonInCharge."\"";
                    if($show_remark_in_personal_report)
                    {
	                    $header .= "\t\"".$i_Discipline_System_general_remark."\"";
                    }
                    $header .= "\n";
                    
                    $exportColumn = array($i_general_record_date, $i_Merit_Type, $i_Discipline_System_general_record, $i_Discipline_System_FromScore, $i_Discipline_System_ToScore, $i_Profile_PersonInCharge);
		             for ($i=0; $i<sizeof($result); $i++)
		             {
		                  list ($r_date, $r_action_type, $r_item, $r_fromscore, $r_toscore,$r_pic_name, $remark) = $result[$i];
		
		                  switch($r_action_type){
			                  case 1 : $action = $i_Discipline_System_New_MeritDemerit; break;
			                  case 2 : $action = $i_Discipline_System_Edit_MeritDemerit; break;
			                  case 3 : $action = $i_Discipline_System_Remove_MeritDemerit; break;
			                  case 4 : $action = $i_Discipline_System_Manual_Adjustment; break;
			              }
			              if($r_item =="--"){
				              $r_item = "$i_Discipline_System_Record_Already_Removed";
				          }
				          
				          if(!$show_remark_in_personal_report)
				          {
					        $header .="\"".str_replace('"','""',$r_date)."\"\t\"".str_replace('"','""',$action)."\"\t\"".str_replace('"','""',$r_item)."\"\t\"".str_replace('"','""',$r_fromscore)."\"\t\"".str_replace('"','""',$r_toscore)."\"\t\"".str_replace('"','""',$r_pic_name)."\"\n";
						  	$row_result[] = array($r_date, $action, $r_item, $r_fromscore, $r_toscore, $r_pic_name, $remark);  
				          }
				          else
				          {
				          	$header .="\"".str_replace('"','""',$r_date)."\"\t\"".str_replace('"','""',$action)."\"\t\"".str_replace('"','""',$r_item)."\"\t\"".str_replace('"','""',$r_fromscore)."\"\t\"".str_replace('"','""',$r_toscore)."\"\t\"".str_replace('"','""',$r_pic_name)."\"\t\"".str_replace('"','""',$remark)."\"\n";
						  	$row_result[] = array($r_date, $action, $r_item, $r_fromscore, $r_toscore, $r_pic_name, $remark);
					  	}
		             }
                  	$filename = "study_score_change_log_".$targetID.".csv";             
             		break; 
         case 8:	# Misconduct Record
				$header.="\"".$i_Discipline_System_general_date_demerit."\",\"".$iDiscipline['Accumulative_Category_Name']."\"\t";
				$header.="\"".$iDiscipline['Accumulative_Category_Item_Name']."\"\t";
				$header.="\"". $i_Profile_PersonInCharge ."\"\t";
				$header.="\"".$i_Discipline_System_general_remark."\"";
				
				if($show_waive_status==1) {
					$header.="\t\"".$i_Discipline_System_WaiveStatus."\"";
				}
				
				$header .= "\n";
				
               $exportColumn = array($i_Discipline_System_general_date_demerit,$iDiscipline['Accumulative_Category_Name'], $iDiscipline['Accumulative_Category_Item_Name'], $i_Discipline_System_general_remark);

				$result = $ldiscipline->retrieveStudentConductRecord($targetID,$startdate,$enddate,$academicYearID,$yearTermID, "-1", $show_waive_status);
				for ($i=0; $i<sizeof($result); $i++)
				{
				  list ($record_id,$record_date,$cat_name,$item_name,$remark,$upgrade_record_id,$record_status,$r_pic_name)= $result[$i];
				  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
				                 
				  $content.="\"".str_replace('"','""',$record_date)."\"\t\"".str_replace('"','""',$cat_name)."\"\t\"".str_replace('"','""',$item_name)."\"\t\"".str_replace('"','""',$r_pic_name)."\"\t\"".str_replace('"','""',$remark)."\"";
				  
				  if($show_waive_status==1){
      				    $content .= "\t\"".str_replace('"','""',$str_waive_status)."\"";
	      				$row_result[] = array($record_date, $cat_name, $item_name, $r_pic_name, $remark, $str_waive_status);

			      	 }
			      	 else {
      	 				$row_result[] = array($record_date, $cat_name, $item_name, $r_pic_name, $remark);
				     }
				     $content .= "\n";

				}
				$filename = "misconduct_record".$targetID.".csv";
             break;     
             
		case 9:	# Good Conduct Record
				$header.="\"".$i_Discipline_System_general_date_demerit."\",\"".$iDiscipline['Accumulative_Category_Name']."\"\t";
				$header.="\"".$iDiscipline['Accumulative_Category_Item_Name']."\"\t";
				$header.="\"". $i_Profile_PersonInCharge ."\"\t";
				$header.="\"".$i_Discipline_System_general_remark."\"";
				
				if($show_waive_status==1) {
					$header.="\t\"".$i_Discipline_System_WaiveStatus."\"";
				}
				
				$header .= "\n";
				
               $exportColumn = array($i_Discipline_System_general_date_demerit,$iDiscipline['Accumulative_Category_Name'], $iDiscipline['Accumulative_Category_Item_Name'], $i_Profile_PersonInCharge, $i_Discipline_System_general_remark);

				$result = $ldiscipline->retrieveStudentConductRecord($targetID,$startdate,$enddate,$academicYearID,$yearTermID, "1", $show_waive_status);
				for ($i=0; $i<sizeof($result); $i++)
				{
				  list ($record_id,$record_date,$cat_name,$item_name,$remark,$upgrade_record_id,$record_status,$r_pic_name)= $result[$i];
				  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
				                 
				  $content.="\"".str_replace('"','""',$record_date)."\"\t\"".str_replace('"','""',$cat_name)."\"\t\"".str_replace('"','""',$item_name)."\"\t\"".str_replace('"','""',$r_pic_name)."\"\t\"".str_replace('"','""',$remark)."\"";
				  
				  if($show_waive_status==1){
      				    $content .= "\t\"".str_replace('"','""',$str_waive_status)."\"";
	      				$row_result[] = array($record_date, $cat_name, $item_name, $r_pic_name, $remark, $str_waive_status);

			      	 }
			      	 else {
      	 				$row_result[] = array($record_date, $cat_name, $item_name, $r_pic_name, $remark);
				     }
				     $content .= "\n";

				}
				$filename = "goodconduct_record".$targetID.".csv";
             break;  
		case 10:	# Good /Mic Conduct Record
				$header.="\"".$i_Discipline_System_general_date_demerit."\"\t\"".$iDiscipline['Accumulative_Category_Name']."\"\t";
				$header.="\"".$iDiscipline['Accumulative_Category_Item_Name']."\"\t";
				$header.="\"". $i_Profile_PersonInCharge ."\"\t";
				$header.="\"".$i_Discipline_System_general_remark."\"";
				
				if($show_waive_status==1) {
					$header.="\t\"".$i_Discipline_System_WaiveStatus."\"";
				}
				
				$header .= "\n";
				
               $exportColumn = array($i_Discipline_System_general_date_demerit,$iDiscipline['Accumulative_Category_Name'], $iDiscipline['Accumulative_Category_Item_Name'], $i_Profile_PersonInCharge, $i_Discipline_System_general_remark);

				$result = $ldiscipline->retrieveStudentConductRecord($targetID,$startdate,$enddate,$academicYearID,$yearTermID, "", $show_waive_status);
				
				for ($i=0; $i<sizeof($result); $i++)
				{
					
				  list ($record_id,$record_date,$cat_name,$item_name,$remark,$upgrade_record_id,$record_status,$r_pic_name)= $result[$i];
				  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
				                 
				  $content.="\"".str_replace('"','""',$record_date)."\"\t\"".str_replace('"','""',$cat_name)."\"\t\"".str_replace('"','""',$item_name)."\"\t\"".str_replace('"','""',$r_pic_name)."\"\t\"".str_replace('"','""',$remark)."\"";
				  
				  if($show_waive_status==1){
      				    $content .= "\t\"".str_replace('"','""',$str_waive_status)."\"";
	      				$row_result[] = array($record_date, $cat_name, $item_name, $r_pic_name, $remark, $str_waive_status);

			      	 }
			      	 else {
      	 				$row_result[] = array($record_date, $cat_name, $item_name, $r_pic_name, $remark);
				     }
				     $content .= "\n";

				}
				$filename = "good_misconduct_record".$targetID.".csv";

             break;  
             
}

if ($g_encoding_unicode) {
	$export_content = $header.$content;
	output2browser($export_content,$filename);

} else {
	$export_content = $preheader.$lexport->GET_EXPORT_TXT($row_result, $exportColumn, "\t", "\r\n", "\t", 0, "11");
	$lexport->EXPORT_FILE($filename, $export_content);
}

/*
$export_content = $header.$content;
output2browser($export_content,$filename);
*/

/*
$export_content = $header.$content;

// Export Content
header("Content-type: application/octet-stream");
header("Content-Length: ".strlen($export_content) );
header("Content-Disposition: attachment; filename=\"".$filename."\"");
echo $export_content;
*/

?>
