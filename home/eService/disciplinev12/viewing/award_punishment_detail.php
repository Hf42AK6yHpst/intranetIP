<?php
// Modifying by: 
/*
* 
* 	Date	:	2019-05-24  Bill
* 	Detail	:	apply download_attachment.php
* 
* 	Date	:	2016-04-08 (Bill)	[2016-0224-1423-31073]
* 	Detail	:	added prefix style to teacher name of deleted account
* 
*	Date	:	2016-03-02 (Bill)	[2016-0224-1423-31073]
*	Detail	:	return PIC name even teacher account is inactive or removed
*
* 	Date	:	2015-10-26 (Bill)	[2015-0611-1642-26164]
* 	Detail	:	not allow apply for CWC Rehabilitation Scheme if any punishment recorsd uner probation
* 
* 	Date	:	2015-07-23 (Bill)	[2015-0611-1642-26164]
* 	Detail	:	support CWC Rehabilitation Scheme - Display info and apply button
* 
*	Date	:	2010-06-11 (Henry)
*	Detail 	:	allow Staff (Teacher) can view eService > eDiscipline
*/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

$TargetStudentID = IntegerSafe($TargetStudentID);
$SchoolYear = IntegerSafe($SchoolYear);
$approved = IntegerSafe($approved);
$waived = IntegerSafe($waived);
$MeritType = IntegerSafe($MeritType);
$SchoolYear2 = IntegerSafe($SchoolYear2);
$waived2 = IntegerSafe($waived2);
$approved2 = IntegerSafe($approved2);
$waitApproval2 = IntegerSafe($waitApproval2);
$released2 = IntegerSafe($released2);
$rejected2 = IntegerSafe($rejected2);
$MeritType2 = IntegerSafe($MeritType2);
$id = IntegerSafe($id);

$ldiscipline = new libdisciplinev12();

# Check access right (View page)
if($_SESSION['UserType']!=2 && $_SESSION['UserType']!=3 && $_SESSION['UserType']!=1)
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

if(!$ldiscipline->CHECK_ACCESS("Discipline-VIEWING-Award_Punishment-View") && $_SESSION['UserType']!=1) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($id);
$data = $datainfo[0];

if($_SESSION['UserType']==1) {	# check teacher access right
	if(!$ldiscipline->checkTeacherAccessEserviceRight($UserID, $data['AcademicYearID'])) {	
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();	
	}
} else if($_SESSION['UserType']==2)	# student
{
	if($data['StudentID']!=$UserID)
	{
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();	
		exit;
	}
} else if($_SESSION['UserType']==3)	#parent
{
	$lu = new libuser($UserID);
	$children = $lu->getChildren();
	if (!in_array($data['StudentID'],$children))
    {
         $ldiscipline->NO_ACCESS_RIGHT_REDIRECT();	
         exit;
    }
} 



$linterface = new interface_html();

# menu highlight setting
$CurrentPage = "Viewing_AwardPunishment";
$CurrentPageArr['eServiceeDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);


# navigation bar
$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "award_punishment.php");
$PAGE_NAVIGATION[] = array($eDiscipline["RecordDetails"], "");

if(empty($datainfo))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$data = $datainfo[0];
$modified_by = $data['ModifiedBy'];
$isAcc = $data['fromConductRecords'];

$lu = new libuser($modified_by);

# build html part
if ($intranet_session_language == "en")
{
	$last_update = $i_Discipline_Last_Updated ." : " . $data['DateModified']. " " . $iDiscipline['By'] . " " . $lu->UserName();
}
else
{
	$last_update = $i_Discipline_Last_Updated ." : " . $data['DateModified']. " (" .$iDiscipline['By'] . $lu->UserName() . ")";
}

$stu = new libuser($data['StudentID']);
$stu_class = $stu->ClassName;
$stu_classnumber = $stu->ClassNumber;
$stu_name = $stu_class . ($stu_classnumber ? "-".$stu_classnumber: "") . " " . $stu->UserName();

$PIC = $data['PICID'];
$namefield = getNameFieldWithLoginByLang();

//[2016-0224-1423-31073]
// INTRANET_USER
//$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($PIC) AND RecordStatus = 1 ORDER BY $namefield";
$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($PIC) ORDER BY $namefield";
$array_PIC = $ldiscipline->returnArray($sql);

// INTRANET_ARCHIVE_USER
//$sql = "SELECT UserID, $namefield FROM INTRANET_ARCHIVE_USER WHERE RecordType = 1 AND UserID IN ($PIC) ORDER BY $namefield";
$sql = "SELECT UserID, CONCAT('<span class=\"tabletextrequire\">*</span>', $namefield) FROM INTRANET_ARCHIVE_USER WHERE RecordType = 1 AND UserID IN ($PIC) ORDER BY $namefield";
$array_ArchivePIC = $ldiscipline->returnArray($sql);

// Merge user and archive user
$array_PIC = array_merge($array_PIC, $array_ArchivePIC);

$PICAry = array();
foreach($array_PIC as $k=>$d)
	$PICAry[] = $d[1];
$PICname = empty($PICAry) ? "---" : implode("<br>",$PICAry);

if($data['Attachment']!="") {
	$path = "$file_path/file/disciplinev12/award_punishment/".$data['Attachment']."/";
	$attachment_list = $ldiscipline->getAttachmentArray($path, '');
	if(sizeof($attachment_list) > 0) {
		$attachmentHTML = "";
		for($i=0; $i<sizeof($attachment_list); $i++) {
			list($displayFilename) = $attachment_list[$i];
			// $linkFilename = str_replace(" ","%20", $displayFilename);
			// $displayFilename = "<a href=/file/disciplinev12/award_punishment/".$data['Attachment']."/".$linkFilename." target=\"_blank\">$displayFilename</a>";
			
			$url = $path.$displayFilename;
			$displayFilename = "<a href='/home/download_attachment.php?target_e=".getEncryptedText($url)."' target=\"_blank\">$displayFilename</a>";
			$attachmentHTML .= ($attachmentHTML=="") ? $displayFilename : ", ".$displayFilename;
		}
		$attachmentHTML = "[".$attachmentHTML."]";
	}
} else {
	$attachmentHTML = "---";	
}

$thisMeritType = $ldiscipline->RETURN_MERIT_NAME($data['ProfileMeritType']);
if($data['MeritType']==1)	# Award setting
{
	$thisicon = "icon_merit";
	$AwardPunishment = $i_Merit_Award;
	$IncrementDecrement = $i_Discipline_System_general_increment;
}
else						# Punishment setting
{
	$thisicon = "icon_demerit";
	$AwardPunishment = $i_Merit_Punishment;
	$IncrementDecrement = $i_Discipline_System_general_decrement;
}

if($data['fromConductRecords'])
{
	$conduct_records = $ldiscipline->RETRIEVE_GROUPED_CONDUCT_RECORD($id);
	
	$Reference = "<td class='detail_box'>". $eDiscipline["HISTORY"] ."<br>";
	
	$Reference .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' bgcolor='#CCCCCC' class='layer_table_detail'>";
		$Reference .= "<tr class='tabletop'>
							<td width='15' align='center' valign='middle'>#</td>
							<td width='20' align='left'>&nbsp;</td>
							<td align='left'>". $i_Merit_Reason ."</td>";
		if($sys_custom['eDiscipline_GM_Times']) {
			$Reference .= "<td>{$Lang['eDiscipline']['Times']}</td>";	
		}
		$Reference .= "
							<td>". $i_EventDate ."</td>
							<td>". $i_Discipline_PIC ."</a></td>
						</tr>";

		for($i=0;$i<sizeof($conduct_records);$i++)
		{
			$thisIcon = ($conduct_records[$i]['RecordType']==1) ? "icon_gd_conduct.gif":"icon_misconduct.gif";
			$thisCatID = $conduct_records[$i]['CategoryID'];
			$thisItemID = $conduct_records[$i]['ItemID'];
			$thisRecordDate = substr($conduct_records[$i]['RecordDate'],0,10);
			$record_reason = $ldiscipline->RETURN_CONDUCT_REASON($thisItemID, $thisCatID);
			$thisGmCount = (floor($conduct_records[$i]['GMCount'])==$conduct_records[$i]['GMCount']) ? floor($conduct_records[$i]['GMCount']) : round($conduct_records[$i]['GMCount'],1);
			
			$thisPIC = $ldiscipline->RETRIEVE_CONDUCT_PIC($conduct_records[$i]['RecordID']);
			$thisPIC = $thisPIC ? $thisPIC : "---";
			
			$Reference .= "<tr class='row_approved'>
								<td align='center' valign='top'>". ($i+1)."</td>
								<td align='left' valign='top' class='tabletext'><img src='". $image_path."/".$LAYOUT_SKIN ."/ediscipline/". $thisIcon ."' width='20' height='20'></td>
								<td valign='top' class='row_approved'>". $record_reason ."</td>";
			if($sys_custom['eDiscipline_GM_Times']) {
				$Reference .= "<td>{$thisGmCount}</td>";	
			}
			$Reference .= "
								<td valign='top'>". $thisRecordDate ."</td>
								<td valign='top'>". $thisPIC ."</td>
							</tr>";
		}
		$Reference .= "</table>";
	
	
	$Reference .= "</td>";
}

if($data['CaseID'])
{
	$case_info = $ldiscipline->RETRIEVE_CASE_INFO($data['CaseID']);
	$Reference = "<td>". $i_Discipline_System_Discipline_Case_Record_Case ." ". $case_info[0]['CaseNumber'] ."</td>";
}

$Reference = $Reference ? $Reference :"<td>---</td>";

# Detention
$TempAllForms = $ldiscipline->getAllFormString();
$DetentionList = $ldiscipline->getDetentionListByDemeritID($data['RecordID']);
for ($i=0; $i<sizeof($DetentionList); $i++) {
	list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $DetentionList[$i];
	if ($TmpForm == $TempAllForms) $TmpForm = $i_Discipline_All_Forms;
	$TmpAttendanceStatus = $ldiscipline->getAttendanceStatusByStudentIDDetentionID($data['StudentID'], $TmpDetentionID);
	if ($TmpAttendanceStatus[0] == "PRE") {
		$TmpAttendanceStatus = "<img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_present.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Attended\">";
	} else if ($TmpAttendanceStatus[0] == "ABS") {
		$TmpAttendanceStatus = "<img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_absent.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Absent\">";
	} else {
		$TmpAttendanceStatus = $i_Discipline_Not_Yet;
	}
	$DetentionOutputRow .= "<tr class=\"row_approved\">\n";
	$DetentionOutputRow .= "<td valign=\"top\">".($i+1)."</td>\n";
	if ($TmpDetentionID == "")
	{
		$DetentionOutputRow .= "<td valign=\"top\">".$i_Discipline_System_Not_Yet_Assigned."</td>\n";
	}
	else
	{
		$DetentionOutputRow .= "<td valign=\"top\">$TmpDetentionDate | $TmpLocation | $TmpForm | $TmpPIC</td>\n";
	}
	$DetentionOutputRow .= "<td valign=\"top\"><span class=\"tabletextremark\">$TmpAttendanceStatus</span></td>\n";
	$DetentionOutputRow .= "</tr>\n";
}
if ($DetentionOutputRow != "") {
	$DetentionOutputPre .= "<tr><td>".$eDiscipline['Detention']."</td></tr>\n";
	$DetentionOutputPre .= "<tr><td style=\"border:1px solid #CCCCCC\">\n";
	$DetentionOutputPre .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
	$DetentionOutputPre .= "<tr class=\"tabletop\">\n";
	$DetentionOutputPre .= "<td width=\"15\" align=\"center\">#</td>\n";
	$DetentionOutputPre .= "<td>$i_Discipline_Arranged_Session</td>\n";
	$DetentionOutputPre .= "<td>$i_Discipline_Attendance</td>\n";
	$DetentionOutputPre .= "</tr>\n";
	$DetentionOutputPost = "</table></td></tr>\n";
	$DetentionOutputData = $DetentionOutputPre.$DetentionOutputRow.$DetentionOutputPost;
}
if ($DetentionOutputData) {
	$DetentionButtonLabel = $eDiscipline["EditDetention"];
	$DetentionButtonAction = "document.form1.action='edit_detention.php';document.form1.DetentionAction.value='Edit';document.form1.submit();";
} else {
	$DetentionButtonLabel = $eDiscipline["AddDetention"];
	$DetentionButtonAction = "document.form1.action='edit_detention.php';document.form1.DetentionAction.value='Add';document.form1.submit();";
}

# eNotice
$lc = new libucc($data['NoticeID']);
$result = $lc->getNoticeDetails();
if ($data['NoticeID']) {
	$eNoticePath = "<a href=\"javascript:;\" class=\"tabletext\" onClick=\"newWindow('".$result['url']."&StudentID=".$data['StudentID']."', 1)\">".$eDiscipline["PreviewNotice"]."<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a>";
	$actionContent = $eNoticePath;
} else if ($data['TemplateID']) {
	$actionContent = $eDiscipline["SendNotice"];
} else if ($DetentionOutputData == ""){
	$actionContent = $i_Discipline_No_Action;
}

switch($data['RecordStatus'])
{
	case DISCIPLINE_STATUS_REJECTED:
		$lu = new libuser($data['RejectedBy']);
		$icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif' width='20' height='20' border='0' align='absmiddle'>";
		$status_str = $icon . " " . $eDiscipline["RejectedBy"] . " " . $lu->UserName() . " " . $data['RejectedDate'];
		break;
	case DISCIPLINE_STATUS_APPROVED:
		$lu = new libuser($data['ApprovedBy']);
		$icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif' width='20' height='20' border='0' align='absmiddle'>";
		if ($intranet_session_language == "en") {
			$status_str = $icon . " " . $eDiscipline["ApprovedBy"] . " " . $lu->UserName() . " " . $eDiscipline["ApprovedBy2"] . " (" . $data['ApprovedDate'] . ") " . $eDiscipline["ApprovedBy3"];
		} else {
			$status_str = $icon . " " . $eDiscipline["ApprovedBy"] . " " . $lu->UserName() . " " . $eDiscipline["ApprovedBy2"] . " " . $data['ApprovedDate'] . " " . $eDiscipline["ApprovedBy3"];
		}
		break;
	case DISCIPLINE_STATUS_WAIVED:
		$lu = new libuser($data['WaivedBy']);
		$icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_waived.gif' width='20' height='20' border='0' align='absmiddle'>";
		$status_str = $icon . " " . $eDiscipline["WaivedBy"] . " " . $lu->UserName() . " " . $data['WaivedDate'];

		$waived_info = $ldiscipline->RETRIEVE_WAVIED_RECORD_INFO($id);
		
		# Get other redeem records info if the record is waived by redemption
		$redeemInfoTable = "";
		if ($data['RedeemID'] != NULL)
		{
			$isRedeemed = 1;
			# Get info of the Redeem Group
			$thisRecordID = $data['RecordID'];
			$thisRedeemID = $data['RedeemID'];
			$RecordIDArr = $ldiscipline->getRedeemGroup($thisRedeemID, $thisRecordID);
			
			# Header (Corresponding waived records)
			$redeemInfoTable .= $ldiscipline->generateRedeemInfoTableHeader($thisRedeemID, $thisRecordID);
			
			# Table Content
			$TableColumnArr = array("Record", "Reason", "EventDate", "PIC", "Reference");
			$redeemInfoTable .= $ldiscipline->generateRedeemInfoTable($RecordIDArr, $TableColumnArr);
		}
		
		//$waive_reason = stripslashes(intranet_htmlspecialchars($waived_info[0]['Reason']));
		$waive_reason = $waived_info[0]['Reason'] ? $waived_info[0]['Reason'] : "---";
		
		$thisDisplay = "";
		$thisDisplay .= "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
		$thisDisplay .= "<tr><td class='tabletext'>".$eDiscipline["WaivedReason"] .": ". $waive_reason."</tr></td>";
		$thisDisplay .= "<tr><td class='tabletext'>".$redeemInfoTable."</tr></td>";
		$thisDisplay .= "</table>";
		
		$status_str .= "<tr><td>&nbsp;</td>";
		$status_str .= "<td class='detail_box'>". $thisDisplay ."</td>";
		$status_str .= "</tr>";

		break;
	case DISCIPLINE_STATUS_PENDING:
		$icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif' width='20' height='20' border='0' align='absmiddle'>";
		$status_str = $icon . " " . $i_Discipline_System_Award_Punishment_Pending;
		break;
}

switch($data['ReleaseStatus'])
{
	case DISCIPLINE_STATUS_RELEASED:
		$lu = new libuser($data['ReleasedBy']);
		$icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_to_student.gif' width='20' height='20' border='0' align='absmiddle'>";
		if ($intranet_session_language == "en") {
			$release_status_str = $icon . " " . $eDiscipline["ReleasedBy"] . " " . $lu->UserName() . " " . $eDiscipline["ApprovedBy2"] . " (" . $data['ReleasedDate'] . ")";
		} else {
			$release_status_str = $icon . " " . $eDiscipline["ReleasedBy"] . " " . $lu->UserName() . " " . $eDiscipline["ApprovedBy2"] . " " . $data['ReleasedDate']." ".$eDiscipline["ReleasedBy3"];
		}
		break;
}
if($release_status_str)
{
	$release_status_row = "
		<tr>
			<td valign='top' nowrap='nowrap'>&nbsp;</td>
			<td>". $release_status_str ."</td>
		</tr>
	";
}

# CWC Rehabilitation Scheme	[2015-0611-1642-26164]
/*
 * 1. Student or Teacher
 * 2. Demerit Record
 * 3. Approved or Released Record (removed)
 */
$rehabilDetails = false;
$rehabilSubmit = false;
if($sys_custom['eDiscipline']['CSCProbation'] && ($_SESSION['UserType']==1 || $_SESSION['UserType']==2) && $data['MeritType']==-1){
//		$data['RecordStatus']==DISCIPLINE_STATUS_APPROVED && $data['ReleaseStatus']==DISCIPLINE_STATUS_RELEASED)
	
	// merit record with rehabilitation settings
	if($data['RehabilPIC']){
		// Rehabilitation Teacher PIC
		$lu = new libuser($data['RehabilPIC']);
		
		// Rehabilitation Status
		switch($data['RehabilStatus'])
		{
			case 1:
				$rehabil_status_icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif' width='20' height='20' border='0' align='absmiddle'>";
				$rehabil_status_str = $Lang['eDiscipline']['CWCRehabil']['Status']['Rejected'];
				break;
			case 2:
				$rehabil_status_icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif' width='20' height='20' border='0' align='absmiddle'>";
				$rehabil_status_str = $Lang['eDiscipline']['CWCRehabil']['Status']['Processing'];
				break;
			case 3:
				$rehabil_status_icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif' width='20' height='20' border='0' align='absmiddle'>";
				$rehabil_status_str = $Lang['eDiscipline']['CWCRehabil']['Status']['Complete'];
				break;
			case 4:
				$rehabil_status_icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif' width='20' height='20' border='0' align='absmiddle'>";
				$rehabil_status_str = $Lang['eDiscipline']['CWCRehabil']['Status']['Incomplete'];
				break;
			default:
				$rehabil_status_icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif' width='20' height='20' border='0' align='absmiddle'>";
				$rehabil_status_str = $Lang['eDiscipline']['CWCRehabil']['Status']['WaitingApproval'];
				break;
		}
		
		// display content
		$rehabilContent = "";
		$rehabilContent .= "<tr>
								<td colspan=\"2\" valign=\"middle\" nowrap=\"nowrap\" class=\"form_sep_title\"><i> - ".$Lang['eDiscipline']['CWCRehabil']['ApplicationContent']." -</i></td>
							</tr>
							<tr>
								<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$Lang['eDiscipline']['CWCRehabil']['FollowUpTeacher']."</td>
								<td>".$lu->UserName()."</td>
							</tr>
							<tr>
								<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$Lang['eDiscipline']['CWCRehabil']['RehabilReason']."</td>
								<td>".nl2br($data['RehabilReason'])."</td>
							</tr>";
		
		// Teacher Reply
		if($data['RehabilFeedback']){
			$rehabilContent .= "<tr>
									<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$Lang['eDiscipline']['CWCRehabil']['RehabilResponse']."</td>
									<td>".nl2br($data['RehabilFeedback'])."</td>
								</tr>";
		}
		
		// Apply Status
		$rehabilContent .= "<tr>
								<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$Lang['eDiscipline']['CWCRehabil']['Status']['Title']."</td>
								<td>$rehabil_status_icon $rehabil_status_str";
		// Status Update Details
		if($data['RehabilFeedback'] && $data['RehabilUpdateDate'] && $data['RehabilLastModify']){
			$lu = new libuser($data['RehabilLastModify']);
			
			if ($intranet_session_language == "en"){
				$update_str = $i_Discipline_Last_Updated ." : " . $data['RehabilUpdateDate']. " " . $iDiscipline['By'] . " " . $lu->UserName();
			}
			else {
				$update_str = $i_Discipline_Last_Updated ." : " . $data['RehabilUpdateDate']. " (" .$iDiscipline['By'] . $lu->UserName() . ")";
			}
			$rehabilContent .= " ($update_str)";
		}
		$rehabilContent .= "	</td>
							</tr>";
		
		$rehabilDetails = true;
	}
	// merit record without rehabilitation settings - allow student to submit
	else if($_SESSION['UserType']==2){
		$rehabilSubmit = true;
		list($approve_probation, $probation_records) = $ldiscipline->CHECK_STUDENT_CWC_PROBATION_RECORD($UserID, $data["YearTermID"], 0);
		
		// not allow apply for Rehabilitation if any punishment records under probation
		$rehabilAction = $approve_probation? "alert('".$Lang['eDiscipline']['CWCRehabil']['CannotApply']."');" : "document.form1.action='award_punishment_rehil.php'; document.form1.submit();";
	}
}

# Start layout
$linterface->LAYOUT_START();

# reason item
//$ItemText = stripslashes(intranet_htmlspecialchars($data['ItemText']))
$ItemText = intranet_htmlspecialchars($data['ItemText']);

//$receive = $data['ProfileMeritCount']>0 ? $data['ProfileMeritCount']." ".$thisMeritType: "(".$i_general_na.")";
$receive = $ldiscipline->returnDeMeritStringWithNumber($data['ProfileMeritCount'], $thisMeritType);
?>

<script language="javascript">
<!--

var jsDetention = [];
var jsWaive = [];
var jsHistory = [];

function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
    
}

function changeClickID(id) {
	document.form1.clickID.value = id;
	hideAllOtherLayer();
}

function getObject( obj ) {

	if ( document.getElementById ) {
		obj = document.getElementById( obj );
	
	} else if ( document.all ) {
		obj = document.all.item( obj );
	
	} else {
		obj = null;
	}
	
	return obj;
}

function moveObject( obj, e ) {
	//alert(event.clientX+" "+event.clientY);
	var tempX = 0;
	var tempY = 0;
	var offset = 5;
	var objHolder = obj;
	
	obj = getObject( obj );
	if (obj==null) {return;}
	
	if (document.all) {
		tempX = event.clientX + document.body.scrollLeft;
		tempY = event.clientY + document.body.scrollTop;
	} else {
		tempX = e.pageX;
		tempY = e.pageY;
	}
	
	if (tempX < 0){tempX = 0} else {tempX = tempX - 300}
	if (tempY < 0){tempY = 0} else {tempY = tempY}
	
	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
		
	displayObject( objHolder, true );
}

function displayObject( obj, show ) {

	obj = getObject( obj );
	if (obj==null) return;
	
	obj.style.display = show ? 'block' : 'none';
	obj.style.visibility = show ? 'visible' : 'hidden';
}

function hideAllOtherLayer() {
	var layer = "";
	
	if(jsDetention.length != 0) {
		for(i=0;i<jsDetention.length;i++) {
			layer = "show_detail" + jsDetention[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsWaive.length != 0) {
		for(i=0;i<jsWaive.length;i++) {
			layer = "show_waive" + jsWaive[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsHistory.length != 0) {
		for(i=0;i<jsHistory.length;i++) {
			layer = "show_history2_" + jsHistory[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
}

function go_detail(id)
{
	document.form1.id.value = id;
	document.form1.action="detail.php";
	document.form1.submit();
}

/*
window.onload = initialMove;

function initialMove() {
	if(jsHistory.length != 0)	 {
		for(i=0;i<jsHistory.length;i++) {
			layer = "show_history2_" + jsHistory[i];
			moveObject(layer,true);	
		}
	}
}
*/
//*******************
function showResult(str,flag)
{
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "get_live.php?flag="+flag;
	url = url + "&RecordID=" + str
	url = url + "&sid=" + Math.random()
	
	if(flag=='detention') {
		xmlHttp.onreadystatechange = stateChanged 
	} else if(flag=='waived') {
		xmlHttp.onreadystatechange = stateChanged2 
	} else if(flag=='history') {
		xmlHttp.onreadystatechange = stateChanged3 
	}
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_detail"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_detail"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged2() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_waive"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_waive"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged3() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_history2_"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_history2_"+id).style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function no_view_right()
{
	alert("<?=$i_general_no_access_right?>");	
}
//-->
</script>

<br />
<form name="form1" method="POST" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>

<tr>
	<td align='center'>
		<table align='center' width='90%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td align='right'><?=$linterface->GET_SYS_MSG($msg)?></td>
			</tr>
			<tr>
				<td align='right'><?=$last_update?></td>
			</tr>
		</table>
	</td>
</tr>

<tr>
	<td align='center'>
		<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td valign="top" nowrap="nowrap" class="tablerow2"><?=$i_identity_student?></td>
			<td valign="top" class="tablerow2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_stu_ind.gif" width="20" height="20" border="0" align="absmiddle"><?=$stu_name?></td>
		</tr>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["BasicInfo"]?> -</i></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
			<td width="80%" valign="top"><?=$data['Year']?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Conduct_Semester?></td>
			<td valign="top"><?=$data['Semester']?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline["EventDate"]?></td>
			<td valign="top"><?=$data['RecordDate']?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Contact_PIC?></td>
			<td valign="top"><?=$PICname?></td>
		</tr>
		<? if($data['Subject']) {?>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Homework_subject?></td>
			<td><?=nl2br($data['Subject'])?></td>
		</tr>
		<? } ?>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Case_Record_Attachment?></td>
			<td valign="top"><?=$attachmentHTML?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_general_remark?></td>
			<td><?=nl2br($data['Remark'])?></td>
		</tr>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["RecordDetails"]?> -</i></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline["Type"] ?></td>
			<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/<?=$thisicon?>.gif" width="20" height="20" border="0" align="absmiddle"> <?=$AwardPunishment?></td>
		</tr>
		<tr valign="top">
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline["Category_Item"]?></td>
			<td><?=$ItemText?></td>
		</tr>
		<tr valign="top">
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Discipline_Reason2?></span></td>
			<td><?=$receive?></td>
		</tr>
		
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline['Conduct_Mark']?> (<?=$IncrementDecrement?>)</td>
			<td><label for="grading_passfail" class="tabletext"></label>
					<label for="grading_passfail" class="tabletext"><?=$data['ConductScoreChange']?></label></td>
		</tr>
		<? if($ldiscipline->UseSubScore) {?>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Subscore1?> <?=$IncrementDecrement?></td>
			<td><label for="grading_passfail" class="tabletext"></label>
					<label for="grading_passfail" class="tabletext"><?= (($data['SubScore1Change'])? $data['SubScore1Change'] : 0) ?></label></td>
		</tr>
		<? } ?>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Award_Punishment_Reference?></td>
			<?=$Reference?>
		</tr>
		
		<?php if($sys_custom['eDiscipline']['CSCProbation'] && $rehabilDetails){
			echo $rehabilContent;
		} ?>
		
		<tr>
			<td colspan='2' valign='top' nowrap='nowrap' class='formfieldtitle'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif' width='10' height='1'></td>
		</tr>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap"><em class="form_sep_title"> - <?=$eDiscipline["Actions"]?> -</em></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iDiscipline['Accumulative_Category_Item']?></td>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="3">
					<?=$DetentionOutputData?>
					<tr>
						<td><?=$actionContent?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan='2' valign='top' nowrap='nowrap' class='formfieldtitle'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif' width='10' height='1'></td>
		</tr>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap"><em class="form_sep_title"> - <?=$eDiscipline["Status"]?> -</em></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap">&nbsp;</td>
			<td><?=$status_str?></td>
		</tr>
		<?=$release_status_row?>
		
	</table>

	</td>
</tr>



<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "<?=$image_path?>/<?=$LAYOUT_SKIN?>" ?>/10x10.gif" width="10" height="1"></td></tr>
		<? 
			if ($isRedeemed) { 
		?>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_finish, "button", "document.form1.action='award_punishment.php';document.form1.submit()")?>
			</td>
		</tr>
		<? 
			}
			else {
		?>
		<tr>
			<td align="center">
			<?php if($sys_custom['eDiscipline']['CSCProbation'] && $rehabilSubmit){ ?>
				<?= $linterface->GET_ACTION_BTN($Lang['eDiscipline']['CWCRehabil']['Apply'], "button", $rehabilAction)?>
				<input type="hidden" name="id" value="<?=$id?>" />
				&nbsp;
			<?php } ?>
				<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='award_punishment.php'")?>
			</td>
		</tr>
		<? } ?>
	</table>
	</td>
</tr>
</table>
</form>
<br />


<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
