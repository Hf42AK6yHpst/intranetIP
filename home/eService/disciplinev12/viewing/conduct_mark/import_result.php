<?
# Using by : 

#####################################################
#
#	Date	:	2017-08-09	(Bill)	[2017-0321-1643-50240]
#				Copy from admin folder 
#
#####################################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

# Check CSV File Extension
$lo = new libfilesystem();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));
if($ext != ".CSV" && $ext != ".TXT") {
	header("location: import.php?xmsg=import_failed");
	exit();
}

# Get CSV Data
$limport = new libimporttext();
$data = $limport->GET_IMPORT_TXT($csvfile);

# Get CSV Header
if(is_array($data)) {
	$col_name = array_shift($data);
}

# Check CSV Header
$format_wrong = false;
$file_format = array('Class Name', 'Class Number', 'Year', 'Semester', 'Adjustment', 'PIC', 'Reason');
for($i=0; $i<sizeof($file_format); $i++) {
	if ($col_name[$i] != $file_format[$i]) {
		$format_wrong = true;
		break;
	}
}
if($format_wrong)
{
	header("location: import.php?xmsg=wrong_header");
	exit();
}

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lu = new libuser();

# Access Right Checking
$dataAry = $ldiscipline->ClassTeacherConductAccessRightChecking();

# Get Teaching Class Students
$studentIDAry = $dataAry[1];

# Menu Highlight
$CurrentPageArr['eServiceeDisciplinev12'] = 1;
$CurrentPage = "Management_ConductMark";

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag
$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Conduct_Mark);

# Step
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# Start Layout
$linterface->LAYOUT_START();

# List import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_class</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_identity_student</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_Year</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_SettingsSemester</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Discipline_Conduct_Mark_Adjustment</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Profile_PersonInCharge</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_Reason</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['AccumulativePunishment_Import_Failed_Reason']."</td>";
$x .= "</tr>";

# DROP TABLE [temp_conduct_score_import]
$sql = "DROP TABLE temp_conduct_score_import";
$ldiscipline->db_db_query($sql);

# CREATE TABLE [temp_conduct_score_import]
$sql = "CREATE TABLE temp_conduct_score_import
		(
			 ClassName varchar(10),
			 ClassNumber int(3),
			 StudentID int(11),
			 Year varchar(100),
			 Semester varchar(100),
			 IsAnnual int(11),
			 AdjustMark int(11),
			 Reason varchar(100),
			 PICID int(11),
			 Teacher varchar(255),
			 Student varchar (255)
	    ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$ldiscipline->db_db_query($sql);

# Check CSV Data
$error_occured = 0;
for($i=0; $i<sizeof($data); $i++)
{
	$error = array();
	$PicIDArr = array();
	
	# Get Data
	list($ClassName, $ClassNumber, $Year, $Semester, $Adjustment, $PIC, $Reason) = $data[$i];
	
	# School Year
	if(trim($Year) == "") {
		$error['No_Year'] = $iDiscipline['Award_Punishment_Missing_Year'];
	}
	else {
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE WHERE Year = '$Year'";
		$tmpYear = $ldiscipline->returnVector($sql);
		if(!$tmpYear[0])
		{
			$AcademicYearID = $ldiscipline->getAcademicYearIDByYearName($Year);
			if ($AcademicYearID == "") {
				$error['No_Year'] = $iDiscipline['Award_Punishment_No_Year'];
			}
		}
	}
	
	# Semester
	if(trim($Semester) == "") {
		$error['No_Semester']=$iDiscipline['Award_Punishment_Missing_Semester'];	
	}
	else {
		if ($AcademicYearID != "" && strtolower($Semester) != "whole year") {
			$YearTermID = $ldiscipline->getTermIDByTermName($Semester, $AcademicYearID);
			if($YearTermID == "") {
				$error['Wrong_Semester'] = "No such semester";
			}
		}
	}
	
	# Class & Class Number
	$ClassInfo = $ClassName."-".$ClassNumber;
	$StudentID = $lu->returnUserID($ClassName, $ClassNumber, 2, '0,1,2');
	$lu = new libuser($StudentID);
	$StudentName = ($intranet_session_language=="en")? $lu->EnglishName : $lu->ChineseName;
	if(trim($StudentID) == "") {
		$error['No_Student'] = $iDiscipline['Good_Conduct_No_Student'];
	}
	else if(!in_array($StudentID, (array)$studentIDAry)) {
		$error['No_Student'] = $i_Teaching_NotClassTeacher;
	}
	
	# PIC
	$PicName = array();
	$PicArr = explode(",", $PIC);
	for($ct=0; $ct<sizeof($PicArr); $ct++)
	{
		$sql = "SELECT UserID as PICID, ".getNamefieldByLang()." as Name FROM INTRANET_USER WHERE UserLogin = '".$PicArr[$ct]."' AND RecordType = 1";
		$tmpResult = $lu->returnArray($sql);
		
		$PicID = $tmpResult[0]['PICID'];
		if(trim($PicID) == "") {
			$error['PIC']=$iDiscipline['Good_Conduct_No_Staff'];
		}
		else {
			$PicIDArr[] = $PicID;
			$PicName[] = $tmpResult[0]['Name'];
		}
	}
		
	# Adjustment
	if(!is_numeric($Adjustment)) {	
		$error['Adjustment'] = $i_ServiceMgmt_System_Warning_Numeric;
	}
	else {
		$IsFloat = (strpos($Adjustment, "."));
		if($IsFloat) {
			$error['Adjustment'] = $iDiscipline['Conduct_Mark_Import_Adjustment_OutRange'];
		}
	}
	
	# Table
	$css = (sizeof($error)==0) ? "tabletext":"red";
	$x .= "<tr class=\"tablebluerow".($i%2+1)."\">";
		$x .= "<td class=\"$css\">".($i+1)."</td>";
		$x .= "<td class=\"$css\">". $ClassInfo ."</td>";
		$x .= "<td class=\"$css\">". $StudentName ."</td>";
		$x .= "<td class=\"$css\">". $Year ."</td>";
		$x .= "<td class=\"$css\">". $Semester ."</td>";
		$x .= "<td class=\"$css\">". $Adjustment ."</td>";
		$x .= "<td class=\"$css\">". implode(",",$PicName) ."</td>";
		$x .= "<td class=\"$css\">". $Reason ."</td>";
		$x .= "<td class=\"$css\">";
	if(sizeof($error)>0) {
		foreach($error as $Key => $Value) {
			$x .= $Value.'<br/>';
		}	
	}
	else {	
		$x .= "--";
	}
		$x .= "</td>";
	$x .= "</tr>";
	
	# INSERT INTO [temp_conduct_score_import]
	if($Semester=='') {
		$sql_fields = " (ClassName,ClassNumber,StudentID,Year,IsAnnual,AdjustMark,Reason,PICID,Teacher,Student)";
		$sql_values = " ('$ClassName',$ClassNumber,'$StudentID' ,'$Year','1','$Adjustment','$Reason','".implode(',',$PicIDArr)."','".implode(",",$PicName)."','".addslashes($StudentName)."')";
	}
	else {
		$sql_fields = " (ClassName,ClassNumber,StudentID,Year,Semester,AdjustMark,Reason,PICID,Teacher,Student)";
		$sql_values = " ('$ClassName',$ClassNumber,'$StudentID' ,'$Year','$Semester','$Adjustment','$Reason','".implode(',',$PicIDArr)."','".implode(",",$PicName)."','".addslashes($StudentName)."')";
	}
	$sql = "insert into temp_conduct_score_import $sql_fields values $sql_values ";
	$ldiscipline->db_db_query($sql);

	if($error) {
		$error_occured++;
	}
}
$x .= "</table>";

if($error_occured) {
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php?RedirectYearTermID=$RedirectYearTermID&RedirectClassID=$RedirectClassID'");
}
else {	
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php?RedirectYearTermID=$RedirectYearTermID&RedirectClassID=$RedirectClassID'");
}
?>

<br />
<form name="form1" method="post" action="import_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2"><?=$x?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>

<input type="hidden" name="RedirectYearTermID" id="RedirectYearTermID" value="<?php echo $RedirectYearTermID; ?>" />
<input type="hidden" name="RedirectClassID" id="RedirectClassID" value="<?php echo $RedirectClassID; ?>" />
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>