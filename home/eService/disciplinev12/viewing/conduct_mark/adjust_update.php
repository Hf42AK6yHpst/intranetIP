<?php 
# Using by : 

#####################################################
#
#	Date	:	2017-08-09	(Bill)	[2017-0321-1643-50240]
#				Copy from admin folder 
#
#####################################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldisciplinev12 = new libdisciplinev12();

# Access Right Checking
$dataAry = $ldisciplinev12->ClassTeacherConductAccessRightChecking();

# Get Teaching Class Students
$studentIDAry = $dataAry[1];

//$IsAllowedtoAdjust = $ldisciplinev12->CHECK_ACCESS("Discipline-MGMT-Conduct_Mark-Adjust");
//if($IsAllowedtoAdjust)
{
	# Get Adjusted Info
	if(is_array($_POST)) {
		foreach($_POST as $Key => $Value) {
			if($Key=='UniqueID') {
				for($ct=0; $ct<sizeof($UniqueID); $ct++)
				{
					$Student = explode(",", $UniqueID[$ct]);
					list($StudentArr[$ct]['StudentID'], $StudentArr[$ct]['Year'], $StudentArr[$ct]['Semester']) = $Student;
					
					if($StudentArr[$ct]['Semester']=="IsAnnual" || $StudentArr[$ct]['Semester']==0) {
						$StudentArr[$ct]['Semester'] = "";
						$StudentArr[$ct]['IsAnnual'] = 1;
						$Semester = "";
					}
					else {
						$StudentArr[$ct]['IsAnnual'] = "";
					}
					
					$StudentArr[$ct]['AdjustMark'] = $_POST['tb_'.$UniqueID[$ct]];
					$StudentArr[$ct]['Reason'] = $_POST['select_'.$UniqueID[$ct]];
				}
			}
		}
	}
	
	# Get Year Term
	$AcademicYearID = $YearID;
	$YearTermID = $Semester;
	
	# Insert Adjusted Records
	for($a=0; $a<sizeof($StudentArr); $a++)
	{
		// Skip if not Class Students
		if(!in_array($StudentArr[$a]['StudentID'], (array)$studentIDAry)) {
			continue;
		}
		
		# INSERT INTO [DISCIPLINE_CONDUCT_ADJUSTMENT]
		$fields = "(StudentID, AcademicYearID, YearTermID, IsAnnual, AdjustMark, Reason, PICID, DateInput, DateModified)";
		$values = "(".$StudentArr[$a]['StudentID'].", '".$StudentArr[$a]['Year']."', '".$SemesterStr."', '".$StudentArr[$a]['IsAnnual']."', 
					".$StudentArr[$a]['AdjustMark'].", '".$StudentArr[$a]['Reason']."', $UserID, NOW(), NOW() )";
		$sql = "INSERT INTO DISCIPLINE_CONDUCT_ADJUSTMENT $fields VALUES $values";
		if($ldisciplinev12->db_db_query($sql)) {
			$result = 'add';
			
			# Send Warning Letter if reach Warning Level
			$current_conduct_info = $ldisciplinev12->retrieveConductBalanceGrade(array($StudentArr[$a]['StudentID']), "", "", "", $AcademicYearID, $YearTermID);
			$FromScore = $current_conduct_info[0]['ConductScore'];
			$ldisciplinev12->CHECK_AND_SEND_WARNING_RULE_LETTER($StudentArr[$a]['StudentID'], "", "", $FromScore, $AcademicYearID, $YearTermID, "", $manuallyAdjust=1);	
		}
		else {
			$result = 'failed';
		}
	}
}

# Get Return Page
$classID = $ldisciplinev12->getClassIDByClassName($ClassName);
$ReturnPage = "index.php?Year=$Year&Semester=$SemesterStr&ClassID=$ClassID&searchStr=$searchStr&xmsg=add";

intranet_closedb();
header("Location: $ReturnPage");
?>