<?php
# Using by : 

#####################################################
#
#	Date	:	2017-08-09	(Bill)	[2017-0321-1643-50240]
#				Copy from admin folder 
#
#####################################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

# Access Right Checking
$ldiscipline->ClassTeacherConductAccessRightChecking();

# Get Import Data from temp table
$sql = "SELECT * FROM temp_conduct_score_import";
$recordArr = $ldiscipline->returnArray($sql);

# List import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
	$x .= "<tr>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_class</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_identity_student</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_Year</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_SettingsSemester</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Discipline_Conduct_Mark_Adjustment</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Profile_PersonInCharge</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_Reason</td>";
	$x .= "</tr>";

for($a=0; $a<sizeof($recordArr); $a++)
{
	$x .= "<tr class=\"tablebluerow".($a%2+1)."\">";
	$x .= "<td class=\"tabletext\">".($a+1)."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['ClassName']."-".$recordArr[$a]['ClassNumber']."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['Student'] ."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['Year'] ."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['Semester'] ."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['AdjustMark'] ."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['Teacher'] ."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['Reason'] ."</td>";
	$x .= "</tr>";
	
	# Academic Year & Semester
	$AcademicYearID = $ldiscipline->getAcademicYearIDByYearName($recordArr[$a]['Year']);
	if(strtolower($recordArr[$a]['Semester']) == "whole year") {
		$fields = "(StudentID, AcademicYearID, IsAnnual, AdjustMark, Reason, PICID, DateInput, DateModified)";
		$values = "('".$recordArr[$a]['StudentID']."', '".$AcademicYearID."', 1, '".$recordArr[$a]['AdjustMark']."', '".$recordArr[$a]['Reason']."', '".$recordArr[$a]['PICID']."', NOW(), NOW())";		
	}
	else {
		$YearTermID = $ldiscipline->getTermIDByTermName($recordArr[$a]['Semester'], $AcademicYearID);
		$fields = "(StudentID, AcademicYearID, YearTermID, AdjustMark, Reason, PICID, DateInput, DateModified)";
		$values = "('".$recordArr[$a]['StudentID']."', '".$AcademicYearID."', '".$YearTermID."', '".$recordArr[$a]['AdjustMark']."', '".$recordArr[$a]['Reason']."', '".$recordArr[$a]['PICID']."', NOW(), NOW())";
	}
	
	# INSERT INTO [DISCIPLINE_CONDUCT_ADJUSTMENT]
	$sql = "INSERT INTO DISCIPLINE_CONDUCT_ADJUSTMENT $fields VALUES $values";
	if($ldiscipline->db_db_query($sql)) {
		$import_success++;	
	}
}
if($import_success>0) {
	$xmsg2= "<font color=green>".($import_success)." ".$iDiscipline['Reord_Import_Successfully']."</font>";
}
else {
	$xmsg="import_failed";	
}

$x .= "</table>";

# Import Button
$import_button = $linterface->GET_ACTION_BTN($button_finish, "button", "self.location='index.php?Semester=$RedirectYearTermID&ClassID=$RedirectClassID'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import.php?RedirectYearTermID=$RedirectYearTermID&RedirectClassID=$RedirectClassID'");

# Menu Highlight
$CurrentPageArr['eServiceeDisciplinev12'] = 1;
$CurrentPage = "Management_ConductMark";

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag
$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Conduct_Mark);

# Step
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

# Start Layout
$linterface->LAYOUT_START();
?>

<form name="form1" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
</tr>
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td><?= $x ?></td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2"><?=$import_button?></td>
		</tr>
		</table>
	</td>
</tr>		
</table>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>