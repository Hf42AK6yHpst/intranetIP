<?php
# Using: Bill
 
##############################################################
#
#	Date:	2017-10-31 (Bill)	[2017-0403-1552-54240]
#			Hide options related GM	($sys_custom['eDiscipline']['HideAllGMReport'])
#
#	Date:	2016-07-12	Bill
#			change split() to explode(), for PHP 5.4
#
#	Date:	2014-03-05	YatWoon
#			add checking for student and parent (not allow view others student data)
#
#	Date:	2014-02-28	YatWoon
#			add student checking, is the student selected is belongs to that parent?
#
##############################################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

$perPage = 10;
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if($ck_page_size=='') {
	$ck_page_size = $perPage;	
}

# Preserve Table View
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
}
else if (!isset($pageNo))
{
	$pageNo = 1;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
}
else if (!isset($order) && $ck_right_page_order!="")
{
	$order = $ck_right_page_order;
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
}
else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();

$TargetStudentID = IntegerSafe($TargetStudentID);
$studentID = IntegerSafe($studentID);
$waive_record1 = IntegerSafe($waive_record1);
$award_punish_period = IntegerSafe($award_punish_period);
$SchoolYear1 = IntegerSafe($SchoolYear1);
$waive_record2 = IntegerSafe($waive_record2);
$gdConduct_miscondict_period = IntegerSafe($gdConduct_miscondict_period);
$SchoolYear2 = IntegerSafe($SchoolYear2);
$award_punishment_flag = IntegerSafe($award_punishment_flag);
$gdConduct = IntegerSafe($gdConduct);
$misconduct = IntegerSafe($misconduct);

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();

# Check access right (View page)
if($_SESSION['UserType']!=2 && $_SESSION['UserType']!=3)
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
if(!$ldiscipline->CHECK_ACCESS("Discipline-VIEWING-StudentReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
if($TargetStudentID=='') {
	header("Location: student_report.php");	
}

###### Initial StudentIDAry ######
	#### Gen ChildrenFilter (For Parent Mode)
	$StudentIdAry = Array();
	if($_SESSION['UserType']==3)	# parent
	{
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		$luser = new libuser($UserID);
		$ChildrenAry = $luser->getChildrenList();
		if(!empty($TargetStudentID))
		{
			#################################################
			# Check the child is belongs to parent or not
			#################################################
			$valid_child = false;
			foreach($ChildrenAry as $k => $d)
			{
				if($d['StudentID'] == $TargetStudentID) $valid_child = true;	
			}
			if(!$valid_child)
			{
				$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
				exit;
			}
		}
		
		if(count($ChildrenAry)>1)
		{
			$ChildrenFilter =  getSelectByArray($ChildrenAry, " name='TargetStudentID' id='TargetStudentID' ", $TargetStudentID,0,1);
			for($i=0; $i<count($ChildrenAry); $i++)
				$StudentIDAry[$i] = $ChildrenAry[$i][0];
		}
		else
		{
			$ChildrenFilter = $ChildrenAry[0][1];
			$StudentIDAry[] = $ChildrenAry[0][0];
			$StudentIDStr = $ChildrenAry[0][0];
		}
	}
	else
	{
		$TargetStudentID = $UserID;
		$StudentIDAry[]=$UserID;
		$StudentIDStr = $UserID;
		$ChildrenFilter = "<input type='hidden' name='TargetStudentID' value='$UserID'>";
	}

# Get Student Info , Set $targetClass and $studentID
$Student_Info = $ldiscipline->getStudentNameByID($TargetStudentID);
list($studentID,$StudentName,$targetClass, $ClassNum) = $Student_Info[0];

# Temp assign memory of this page
ini_set("memory_limit", "200M"); 

if (!$lstudentprofile->is_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='1' id='merit1' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==1) ? " checked" : "";
	}
	$meritType .= "><label for='merit1'>".$i_Merit_Merit."</label>";
}
if (!$lstudentprofile->is_min_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='2' id='merit2' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==2) ? " checked" : "";
	}
	$meritType .= "><label for='merit2'>".$i_Merit_MinorCredit."</label>";
}
if (!$lstudentprofile->is_maj_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='3' id='merit3' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==3) ? " checked" : "";
	}
	$meritType .= "><label for='merit3'>".$i_Merit_MajorCredit."</label>";
}
if (!$lstudentprofile->is_sup_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='4' id='merit4' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==4) ? " checked" : "";
	}
	$meritType .= "><label for='merit4'>".$i_Merit_SuperCredit."</label>";
}
if (!$lstudentprofile->is_ult_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='5' id='merit5' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==5) ? " checked" : "";
	}
	$meritType .= "><label for='merit5'>".$i_Merit_UltraCredit."</label>";
}

$demeritType .= "<input name='demerit[]' type='checkbox' value='0' id='demerit0' onClick=\"checkClickBox(this,'all_punishment')\"";
for($i=0;$i<sizeof($demerit);$i++) {
	$demeritType .= ($demerit[$i]==0) ? " checked" : "";
}
$demeritType .= "><label for='demerit0'>".$i_Merit_Warning."</label>";
if (!$lstudentprofile->is_black_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-1' id='demerit1' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-1) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit1'>".$i_Merit_BlackMark."</label>";
}
if (!$lstudentprofile->is_min_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-2' id='demerit2' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-2) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit2'>".$i_Merit_MinorDemerit."</label>";
}
if (!$lstudentprofile->is_maj_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-3' id='demerit3' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-3) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit3'>".$i_Merit_MajorDemerit."</label>";
}
if (!$lstudentprofile->is_sup_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-4' id='demerit4' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-4) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit4'>".$i_Merit_SuperDemerit."</label>";
}
if (!$lstudentprofile->is_ult_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-5' id='demerit5' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-5) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit5'>".$i_Merit_UltraDemerit."</label>";
}

$meritDemerit[5] = $i_Merit_Warning;
$meritDemerit[6] = $i_Merit_Merit;
$meritDemerit[7] = $i_Merit_MinorCredit;
$meritDemerit[8] = $i_Merit_MajorCredit;
$meritDemerit[9] = $i_Merit_SuperCredit;
$meritDemerit[10] = $i_Merit_UltraCredit;
$meritDemerit[4] = $i_Merit_BlackMark;
$meritDemerit[3] = $i_Merit_MinorDemerit;
$meritDemerit[2] = $i_Merit_MajorDemerit;
$meritDemerit[1] = $i_Merit_SuperDemerit;
$meritDemerit[0] = $i_Merit_UltraDemerit;

$SchoolYear = getCurrentAcademicYear();
$selectSchoolYear1 .= "<option value='0'";
$selectSchoolYear1 .= ($SchoolYear1==0) ? " selected" : "";
$selectSchoolYear1 .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
$selectSchoolYear1 .= $ldiscipline->getConductSchoolYear($SchoolYear1);

$selectSchoolYear2 .= "<option value='0'";
$selectSchoolYear2 .= ($SchoolYear2==0) ? " selected" : "";
$selectSchoolYear2 .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
$selectSchoolYear2 .= $ldiscipline->getConductSchoolYear($SchoolYear2);

# Semester Menu #
$semester_data = explode("\n",get_file_content("$intranet_root/file/semester.txt"));
$SemesterMenu1 .= "<option value='WholeYear'";
$SemesterMenu1 .= ($semester1 != 'WholeYear') ? "" : " selected";
$SemesterMenu1 .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for ($i=0; $i<sizeof($semester_data); $i++)
{
	$target_sem = $semester_data[$i];
	$line = explode("::",$target_sem);
	list ($name,$current) = $line;
	$SemesterMenu1 .= "<option value='".$name."'";
	if($name==$semester1) { $SemesterMenu1 .= " selected"; }
	$SemesterMenu1 .= ">".$name."</option>";
}

$SemesterMenu2 .= "<option value='WholeYear'";
$SemesterMenu2.= ($semester2 != 'WholeYear') ? "" : " selected";
$SemesterMenu2 .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for ($i=0; $i<sizeof($semester_data); $i++)
{
	$target_sem = $semester_data[$i];
	$line = explode("::",$target_sem);
	list ($name,$current) = $line;
	$SemesterMenu2 .= "<option value='".$name."'";
	if($name==$semester2) { $SemesterMenu2 .= " selected"; }
	$SemesterMenu2 .= ">".$name."</option>";
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$classAry = array();
$studentAry = array();
$studentAry = $ldiscipline->storeStudent($studentID, $targetClass);

$meritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif width=20 height=20 border=0 align=absmiddle border=0>";
$demeritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif width=20 height=20 border=0 align=absmiddle border=0>";
$waivedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_waived.gif width=20 height=20 align=absmiddle title=Waived border=0>";

$semester1Text = ($semester1=='WholeYear') ? $i_Discipline_System_Award_Punishment_Whole_Year : $semester1;
$semester2Text = ($semester2=='WholeYear') ? $i_Discipline_System_Award_Punishment_Whole_Year : $semester2;

# SQL conditions (A&P) #
if($award_punish_period == 1) 	### Choose "School Year" & "Semester"
{		
	if($SchoolYear1 != '0') 	# Specific School Year
	{
		//$date1 = $SchoolYear1." ".$semester1Text;
		//$conds .= " AND a.Year='$SchoolYear1'";
		
		$SchoolYear1_Name = $ldiscipline->getAcademicYearNameByYearID($SchoolYear1);
		$date1 = $SchoolYear1_Name." ".$semester1Text;
		$conds .= " AND (a.AcademicYearID='$SchoolYear1' or a.Year='$SchoolYear1_Name')";
	} 
	else 						# All School Year
	{
		$date1 = $i_Discipline_System_Award_Punishment_All_School_Year." ".$semester1Text;
		$academicYear = $ldiscipline->generateAllSchoolYear();
	}
	
	if($semester1!='WholeYear')		# Specific Semester
	{
		$conds .= " AND a.Semester='$semester1'";
	}
} 
else 							### Choose "Specific Date Range"
{		
	$date1 = $Section1Fr." To ".$Section1To;
	$conds .= " AND (a.RecordDate BETWEEN '$Section1Fr' AND '$Section1To')";
}

if($waive_record1 == 1) {		# with waived record
	$conds .= " AND ((a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.") OR (a.RecordStatus=".DISCIPLINE_STATUS_WAIVED."))";
}
else { 							# only approved record
	$conds .= " AND (a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.")";	
}

# Only released records are shown to students / parent
$conds .= " AND a.ReleaseStatus = ".DISCIPLINE_STATUS_RELEASED;	

# Award & Punishment option #
if($merit != '') {
	$meritChoice = implode(",",$merit);
}
if($demerit != '') {
	$demeritChoice = implode(",",$demerit);
}

if($all_award != '' && $all_punishment != '') {				# Award + Punishment
	$conds .= " AND (a.MeritType=1 OR a.MeritType=-1)";
}
else if($all_award != '' && $demerit != '') {				# Award + Demerit
	$conds .= " AND (a.MeritType=1 OR a.ProfileMeritType IN ($demeritChoice))";
}
else if($all_punishment != '' && $merit != '') {			# Punish + Merit
	$conds .= " AND (a.MeritType=-1 OR a.ProfileMeritType IN ($meritChoice))";
}
else if($merit != '' && $demerit != '') {					# Merit + Demerit
	$conds .= " AND (a.ProfileMeritType IN ($meritChoice) or a.ProfileMeritType IN ($demeritChoice))";
}
else if($all_award != '')	{								# only Award
	$conds .= " AND a.MeritType=1";
}
else if($all_punishment != '') {							# only Punishment
	$conds .= " AND a.MeritType=-1";
}
else if($merit != '')	 {									# Merit
	$conds .= " AND a.ProfileMeritType IN ($meritChoice)";
}
else if($demerit != '') {									# Demerit
	$conds .= " AND a.ProfileMeritType IN ($demeritChoice)";
}
# end of SQL conditions

# loop of Table Content #
$displayContent = "";
$tempContent = "";

for($i=0; $i<sizeof($studentAry); $i++)
{
	# Student Info
	$stdName = $ldiscipline->getStudentNameByID($studentAry[$i]);
	list($userID, $std_name, $className, $classNumber) = $stdName[0];
	
	$classDisplay = $className;
	$classDisplay .= ($classNumber != '') ? ' - '.$classNumber : $classNumber;

	# --------- Table A&P ---------- 
	if($userID != '')
	{
		if($award_punishment_flag==1) {
			$result = $ldiscipline->awardPunishmentStudentReport($userID, $conds, $waivedImg, $meritImg, $demeritImg);
			
			$tempContent = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
			$tempContent .= "<tr class='tabletop'>";
			$tempContent .= "<td width='1' class='tabletoplink'>#</td>";
			$tempContent .= "<td width='15%'>{$i_RecordDate}</td>";
			$tempContent .= "<td width='1'>&nbsp;</td>";
			$tempContent .= "<td width='1'>&nbsp;</td>";
			$tempContent .= "<td width='15%'>{$i_Discipline_Reason2}</td>";
			$tempContent .= "<td width='40%'>{$eDiscipline['Category_Item2']}</td>";
			$tempContent .= "<td width='30%'>{$i_Profile_PersonInCharge}</td>";
			$tempContent .= "</tr>";
			
			if(sizeof($result)==0) {
				$tempContent .= "<tr class='row_approved'>";
				$tempContent .= "<td class='tabletext' height='40' colspan='7' align='center'>{$i_no_record_exists_msg}</td>";
				$tempContent .= "</tr>";
			}
			for($m=0; $m<sizeof($result); $m++) {
				$k = ($m%2)+1;
				$j = $m+1;
				
				$temp = $result[$m][4] + 5;
				if($result[$m][3]==0) {
					$displayMerit = "--";	
				}
				else if($intranet_session_language=="en") {
					$displayMerit = $result[$m][3]." ".$meritDemerit[$temp];	
				}
				else {
					$displayMerit = $meritDemerit[$temp].$result[$m][3].$Lang['eDiscipline']['Times'];
				}
				
				$tempContent .= "<tr class='row_approved'>";
				$tempContent .= "<td class='tabletext'>{$j}</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$result[$m][0]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$result[$m][1]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$result[$m][2]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>".$ldiscipline->TrimDeMeritNumber_00($displayMerit)."</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$result[$m][5]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>";
				
				$name_field = getNameFieldByLang();
                if($result[$m][6] != '' && $result[$m][6] != 0) {
                	$sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID IN ({$result[$m][6]})";
                	$temp = $ldiscipline->returnArray($sql,1);
                	if(sizeof($temp)!=0) {
                    	for($p=0; $p<sizeof($temp); $p++) {
                        	$tempContent .= $temp[$p][0];
                        	$tempContent .= ($p != (sizeof($temp)-1)) ? ", " : "";
                    	}
            		}
            		else {
                		$tempContent .= "---";
            		}
        		}
        		else {
            		$tempContent .= "---";	
        		}
				$tempContent .= "&nbsp;</td></tr>";
			}
			$tempContent .= "</table>";
		}
		
		# --------- Good Conduct & Misconduct Table --------------- #
		
		# SQL conditions (G&M) #
		if(!$sys_custom['eDiscipline']['HideAllGMReport'])
		{
		if($gdConduct_miscondict_period == 1) 
		{
			//$date2 = ($SchoolYear2 != '0') ? $SchoolYear2." ".$semester2Text : $i_Discipline_System_Award_Punishment_All_School_Year." ".$semester2Text;
			//$dateConds = ($SchoolYear2 != '0') ? " AND b.Year='$SchoolYear2'" : "";
			//$dateConds .= ($semester2 != 'WholeYear') ? " AND b.Semester='$semester2'" : "";
			
			$SchoolYear2_Name = $ldiscipline->getAcademicYearNameByYearID($SchoolYear2);
			$date2 = ($SchoolYear2 != '0') ? $SchoolYear2_Name." ".$semester2Text : $i_Discipline_System_Award_Punishment_All_School_Year." ".$semester2Text;
			$dateConds = ($SchoolYear2 != '0') ? " AND (b.AcademicYearID='$SchoolYear2' or b.Year='$SchoolYear2_Name')" : "";
			$dateConds .= ($semester2 != 'WholeYear') ? " AND b.Semester='$semester2'" : "";
		}
		else {
			$date2 = $Section2Fr." To ".$Section2To;
			$dateConds = " AND (b.RecordDate BETWEEN '$Section2Fr' AND '$Section2To')";
		}
		
		$all_gd_conduct = true;
		$all_misconduct = true;
		
		$gdConductContent = $ldiscipline->getGdConductContent($userID, $waive_record2, $all_gd_conduct, $gdConductChoice, $SchoolYear2, $semester2,$intranet_root, $i_no_record_exists_msg,'row_approved', $dateConds);
		$misConductContent = $ldiscipline->getMisConductContent($userID, $waive_record2, $all_misconduct, $misConductChoice, $SchoolYear2, $semester2,$intranet_root, $i_no_record_exists_msg,'row_approved', $dateConds);
		
		}
		# --------- End of Good Conduct & Misconduct Table --------- #
		
		# Display Content #
		if($i != 0) {
			$displayContent .= "<tr class='row_approved'><td height='1'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td></tr>";	
		}
		
		$displayContent .= "
			<tr>
				<td align='left' class='tablegreenrow2'>
					<table border='0' cellspacing='0' cellpadding='3'>
						<tr>
							<td><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_stu_ind.gif' width='20' height='20' align='absmiddle'>{$i_Discipline_Student} : <span class='sectiontitle'>{$std_name}</span> </td>
							<td>{$i_Discipline_Class} : <span class='sectiontitle'>{$classDisplay}</span></td>
						</tr>
					</table>
				</td>
			</tr>
		";
		
		if($award_punishment_flag==1) {
			$displayContent .= "
				<tr>
					<td>
						<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr>
								<td align='left' class='form_sep_title'><i> - {$eDiscipline['Award_and_Punishment']} -</i></td>
								<td align='right'>{$date1}</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align='left' class='tabletextremark'>
						{$tempContent}								
						<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_waived.gif' width='20' height='20' align='absmiddle'>= {$i_Discipline_System_WaiveStatus_Waived}
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			";
		}
		
		if(!$sys_custom['eDiscipline']['HideAllGMReport'])
		{
		if($gdConduct==1 || $misconduct==1) {
			$displayContent .= "
				<tr>
					<td>
						<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr>
								<td align='left' class='form_sep_title'><i> - {$eDiscipline['Good_Conduct_and_Misconduct']} -</i></td>
								<td align='right'>{$date2}</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align='left'>
						<table width='100%' border='0' cellspacing='0' cellpadding='3'>
							<tr>
			";
			if($gdConduct==1) {
				$displayContent .= "	
					<td width='50%' valign='top'>
						<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr class='tablebottom'>
								<td valign='top'>
									<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_gd_conduct.gif' width='20' height='20' border='0' align='absmiddle'>{$eDiscipline['Setting_GoodConduct']} </td>
								<!--<td align='center' valign='middle'>���~�ֿn����</td>-->
								<td align='center' valign='middle'>{$iDiscipline['Accumulative_Merit_Times']}</td>
							</tr>
							{$gdConductContent}
						</table>
					</td>
				";
			}
			if($misconduct==1) {
				$displayContent .= "
					<td width='50%' valign='top'>
						<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr class='tablebottom'>
								<td valign='top'>
									<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_misconduct.gif' width='20' height='20' border='0' align='absmiddle'>{$eDiscipline['Setting_Misconduct']}</td>
								<!--<td align='center' valign='middle'>���~�ֿn���� </td>-->
								<td align='center' valign='middle'>{$iDiscipline['Accumulative_Demerit_Times']}</td>
							</tr>
							{$misConductContent}
						</table>
					</td>
				";
			}

			$displayContent .= "</tr></table></td></tr>";
		}
		}
	}
	# End of Display Content #
	
	#--------- End Table ------
}

# Report Option #
$reportOptionContent = "<table align='center' width='100%' border='0' cellpadding='5' cellspacing='0'>";
$reportOptionContent .= "<tr>";
$reportOptionContent .= "<td colspan='2' valign='middle' nowrap='nowrap' class='form_sep_title'><i> - {$i_Discipline_Student} -</i></td>";
$reportOptionContent .= "</tr><tr><td  colspan='2'>";
$reportOptionContent .= $ChildrenFilter."</td></tr><tr>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$i_Discipline_Student}</td>";
$reportOptionContent .= "<td class='navigation'>";
$reportOptionContent .= "{$targetClass} - {$StudentName}";
$reportOptionContent .= "<input type='hidden' name='studentID' id='studentID' value='$studentID'/>";
$reportOptionContent .= "<input type='hidden' name='targetClass' id='targetClass' value='$targetClass'/>";
$reportOptionContent .= "</td></tr><tr>";
$reportOptionContent .= "<td colspan='2' valign='middle' nowrap='nowrap' class='form_sep_title'><i>{$i_Discipline_System_Reports_First_Section}</i></td>";
$reportOptionContent .= "</tr><tr>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$i_general_show} <font color='green'>#</font></td>";
$reportOptionContent .= "<td width='80%'>";
$reportOptionContent .= "<table width='100%' border='0' cellspacing='0' cellpadding='3'>";
$reportOptionContent .= "<tr><td valign='top'><input type='checkbox' name='all_award' id='all_award' onClick=\"resetOption(this,'merit[]')\"";
$reportOptionContent .= ($all_award != '') ? " checked" : "";
$reportOptionContent .= "><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif' width='20' height='20' align='absmiddle'> <label for='all_award'>{$i_Discipline_System_Reports_All_Awards}</label> ";
$reportOptionContent .= "</td><td>{$meritType}</td></tr>";
$reportOptionContent .= "<tr><td valign='top'><input type='checkbox' name='all_punishment' id='all_punishment' onClick=\"resetOption(this,'demerit[]')\"";
$reportOptionContent .= ($all_punishment != '') ? " checked" : "";
$reportOptionContent .= "><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif' width='20' height='20' align='absmiddle'><label for='all_punishment'>{$i_Discipline_System_Reports_All_Punishment}</label> </td>";
$reportOptionContent .= "<td>{$demeritType}</td></tr></table><br>";
$reportOptionContent .= "<input type='checkbox' name='waive_record1' id='waive_record1' value='1'";
$reportOptionContent .= ($waive_record1==1) ? " checked" : "";
$reportOptionContent .= "><label for='waive_record1'>{$i_Discipline_System_Reports_Include_Waived_Record}</label>";
$reportOptionContent .= "</td></tr><tr valign='top'>";
$reportOptionContent .= "<td height='57' valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['Period']}</td>";
$reportOptionContent .= "<td>";
$reportOptionContent .= "<table border='0' cellspacing='0' cellpadding='3'><tr>";
$reportOptionContent .= "<td height='30' colspan='6'>";
$reportOptionContent .= "<input name='award_punish_period' type='radio' id='award_punish_period' value='1'";
$reportOptionContent .= ($award_punish_period==1) ? " checked" : "";
$reportOptionContent .= ">{$i_Discipline_School_Year} <select name='SchoolYear1' onFocus=\"form1.award_punish_period[0].checked=true;\"  onChange=\"changeTerm(this.value, 'semester1')\">{$selectSchoolYear1}</select>";
$reportOptionContent .= "{$i_Discipline_Semester} <span id='spanSemester1'><select name='semester1' onFocus=\"form1.award_punish_period[0].checked=true;\">{$SemesterMenu1}</select></span>";
$reportOptionContent .= "</td></tr><tr>";
$reportOptionContent .= "<td><input name='award_punish_period' type='radio' id='award_punish_period' value='2'";
$reportOptionContent .= ($award_punish_period==2) ? " checked" : "";
$reportOptionContent .= ">{$iDiscipline['Period_Start']}</td>";
$reportOptionContent .= "<td><input name='Section1Fr' type='text' class='tabletext' value='{$Section1Fr}' onFocus=\"form1.award_punish_period[1].checked=true;\" /></td>";
$reportOptionContent .= "<td align='center' onClick='form1.award_punish_period[1].checked=true'>".$linterface->GET_CALENDAR('form1', 'Section1Fr')."</td>";
$reportOptionContent .= "<td width='25' align='center'>{$iDiscipline['Period_End']}</td>";
$reportOptionContent .= "<td><input name='Section1To' type='text' class='tabletext' value='{$Section1To}' onFocus=\"form1.award_punish_period[1].checked=true;\"/></td>";
$reportOptionContent .= "<td align='center' onClick='form1.award_punish_period[1].checked=true'>".$linterface->GET_CALENDAR('form1', 'Section1To')."</td>";
$reportOptionContent .= "</tr></table></td></tr>";
if(!$sys_custom['eDiscipline']['HideAllGMReport']) {
$reportOptionContent .= "<tr><td colspan='2' valign='middle' nowrap='nowrap' class='form_sep_title'><i> {$i_Discipline_System_Reports_Second_Section}</i></td>";
$reportOptionContent .= "</tr><tr>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$i_general_show} <font color='green'>#</font></td>";
$reportOptionContent .= "<td>";
$reportOptionContent .= "<input type='checkbox' name='waive_record2' id='waive_record2' value='2'";
$reportOptionContent .= ($waive_record2==2) ? "checked" : "";
$reportOptionContent .= "><label for='waive_record2'>{$i_Discipline_System_Reports_Include_Waived_Record}</label>";
$reportOptionContent .= "</td></tr><tr valign='top'>";
$reportOptionContent .= "<td height='57' valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['Period']}</td>";
$reportOptionContent .= "<td><table border='0' cellspacing='0' cellpadding='3'><tr>";
$reportOptionContent .= "<td height='30' colspan='6'><input name='gdConduct_miscondict_period' type='radio' id='radio' value='1' checked>";
$reportOptionContent .= "{$i_Discipline_School_Year} <select name='SchoolYear2' onFocus=\"form1.gdConduct_miscondict_period[0].checked=true;\">{$selectSchoolYear2}</select>";
$reportOptionContent .= "{$i_Discipline_Semester} <span id='spanSemester2'><select name='semester2' onFocus=\"form1.gdConduct_miscondict_period[0].checked=true;\">{$SemesterMenu2}</select></span>";
$reportOptionContent .= "</td></tr><tr>";
$reportOptionContent .= "<td><input name='gdConduct_miscondict_period' type='radio' id='gdConduct_miscondict_period' value='2'";
$reportOptionContent .= ($gdConduct_miscondict_period==2) ? " checked" : "";
$reportOptionContent .= ">{$iDiscipline['Period_Start']}</td>";
$reportOptionContent .= "<td><input name='Section2Fr' type='text' class='tabletext' value='{$Section2Fr}' onFocus=\"form1.gdConduct_miscondict_period[1].checked=true;\" /></td>";
$reportOptionContent .= "<td align='center' onClick='form1.gdConduct_miscondict_period[1].checked=true'>".$linterface->GET_CALENDAR('form1', 'Section2Fr')."</td>";
$reportOptionContent .= "<td width='25' align='center'>{$iDiscipline['Period_End']}</td>";
$reportOptionContent .= "<td><input name='Section2To' type='text' class='tabletext' value='{$Section2To}' onFocus=\"form1.gdConduct_miscondict_period[1].checked=true;\" /></td>";
$reportOptionContent .= "<td align='center' onClick='form1.gdConduct_miscondict_period[1].checked=true'>".$linterface->GET_CALENDAR('form1', 'Section2To')."</td>";
$reportOptionContent .= "</tr></table></td></tr>";
}
$reportOptionContent .= "<tr>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='tabletextremark'>{$i_general_required_field}</td>";
$reportOptionContent .= "<td>&nbsp;</td></tr><tr><td colspan='2' align='center'>";
$reportOptionContent .= "<input type='hidden' name='gdConduct' value='1'><input type='hidden' name='misconduct' value='1'><input type='hidden' name='award_punishment_flag' value=''>";
$reportOptionContent .= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, 'submit','document.form1.target=\'\';document.form1.action=\'student_report_view.php\'')."</td></tr></table>";
# End of Report Option #

$CurrentPageArr['eServiceeDisciplinev12'] = 1;
$CurrentPage = "Viewing_StudentReport";

# Menu Highlight Setting
$TAGS_OBJ[] = array($eDiscipline['StudentReport'], "");

# Left Menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start Layout
$linterface->LAYOUT_START();

$i = 1;
?>

<script language="javascript">
function showSpan(span){
	document.getElementById(span).style.display="inline";
}

function hideSpan(span){
	document.getElementById(span).style.display="none";
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('divReportOption');
	document.form1.showReportOption.value = '1';
}

function hideOption(){
	hideSpan('divReportOption');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.form1.showReportOption.value = '0';
}

function checkClickBox(obj, ref) {
	var field = "";
	if(obj.checked==false) {
		field = eval("form1." + ref);
		field.checked = false;
	}
}

function resetOption(obj, element_name) {
	var field = "";
	var len = document.form1.elements.length;
	if(obj.checked == true)	 {
		for(i=0;i<len;i++) {
			if(document.form1.elements[i].name==element_name) 
				document.form1.elements[i].checked = true;
		}
	}
	else {
		for(i=0;i<len;i++) {
			if(document.form1.elements[i].name==element_name) 
				document.form1.elements[i].checked = false;
		}
	}
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function goCheck(form1) {
	var flag = 0;
	var temp = "";
	var region1 = 0;
	var region2 = 0;
	var region3 = 0;
	var region4 = 0;
	var choiceSelected = 0;
	var choice = "";
	form1.award_punishment_flag.value = "";
	form1.gdConduct.value = "1";
	form1.misconduct.value = "1";
	
	// region 1
	if(form1.all_award.checked==true || form1.all_punishment.checked==true) {
		region1 = 1;
		form1.award_punishment_flag.value = 1;
	}
	
	var len = form1.elements.length;
	for(i=0;i<len;i++) {
		if(form1.elements[i].name=='merit[]' && form1.elements[i].checked==true) {
			region2 = 1;	
			form1.award_punishment_flag.value = 1;
		}
		if(form1.elements[i].name=='demerit[]' && form1.elements[i].checked==true) {
			region2 = 1;	
			form1.award_punishment_flag.value = 1;
		}
	}
	
	if(region1==0 && region2==0 && region3==0 && region4==0) {
		alert("<?=$i_Discipline_System_Please_Select_Record?>");	
		return false;
	}
	
	if(region1!=0 || region2!=0){
		if(form1.award_punish_period[1].checked==true && (form1.Section1Fr.value=='' && form1.Section1To.value=='')) {
			alert("<?=$i_alert_pleasefillin?><?=$i_Discipline_System_Reports_Report_Period?>");	
			return false;
		}	
		if(form1.award_punish_period[1].checked==true && ((!check_date(form1.Section1Fr,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.Section1To,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
		{
			return false;
		}
		if(form1.award_punish_period[1].checked==true && (form1.Section1Fr.value > form1.Section1To.value)) {
			alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
			return false;	
		}
	}
	
	return true;
}

function showDiv(div)
{
	document.getElementById(div).style.display = "inline";
}

function hideDiv(div)
{
	document.getElementById(div).style.display = "none";
}

var xmlHttp2
var xmlHttp3
function changeTerm(val, field) 
{
	if (val.length==0)
	{
		document.getElementById("spanSemester1").innerHTML = "";
		document.getElementById("spanSemester1").style.border = "0px";
		
		<? if(!$sys_custom['eDiscipline']['HideAllGMReport']) { ?>
			document.getElementById("spanSemester2").innerHTML = "";
			document.getElementById("spanSemester2").style.border = "0px";
		<? } ?>
		return
	}
	
	xmlHttp2 = GetXmlHttpObject()
	xmlHttp3 = GetXmlHttpObject()
	if (xmlHttp2==null || xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 
	
	var url = "";
	url = "ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&field=" + field;
	url += "&term1=<?=$semester1?>";
	url += "&term2=<?=$semester2?>";
		
	if(field=='semester1') {
		xmlHttp2.onreadystatechange = stateChanged2 
		xmlHttp2.open("GET",url,true)
		xmlHttp2.send(null)
	}
	else {
		xmlHttp3.onreadystatechange = stateChanged3 
		xmlHttp3.open("GET",url,true)
		xmlHttp3.send(null)
	}
} 

function stateChanged2() 
{
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{
		document.getElementById("spanSemester1").innerHTML = xmlHttp2.responseText;
		document.getElementById("spanSemester1").style.border = "0px solid #A5ACB2";
	}
}

function stateChanged3() 
{
	<? if(!$sys_custom['eDiscipline']['HideAllGMReport']) { ?>
		if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
		{
			document.getElementById("spanSemester2").innerHTML = xmlHttp3.responseText;
			document.getElementById("spanSemester2").style.border = "0px solid #A5ACB2";
		}
	<? } ?>
}

//window.onunload = goCheck(document.form1);
</script>

<form name="form1" method="get" action="" onSubmit="return goCheck(this)">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td class="report_show_option">
									<!--<a href="javascript:;" class="contenttool" onClick="javascript:if(document.getElementById('showReportOption').value=='0'){document.getElementById('showReportOption').value='1';showDiv('divReportOption');}else{document.getElementById('showReportOption').value='0';hideDiv('divReportOption');}"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_System_Reports_Show_Reports_Option?></a>//-->
									<span id="spanShowOption"><a href="javascript:showOption();" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_Show_Statistics_Option?></a></span>
									<span id="spanHideOption" style="display:none"><a href="javascript:hideOption();" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_Hide_Statistics_Option?></a></span>
									<br>
									<div id='divReportOption' style='display:none'><?=$reportOptionContent?></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
			<table width="88%" border="0" cellpadding="5" cellspacing="0" class="result_box">
				<tr>
					<td align="center">
						<table border="0" cellspacing="5" cellpadding="5">
							<tr>
								<td align="center" valign="middle" class="Chart_title"><?=$i_Discipline_System_Reports_Discipline_report?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<?=$displayContent?>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<?= $linterface->GET_ACTION_BTN($button_export, "submit","document.form1.target='';document.form1.action='export_student.php'")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_print, "submit","document.form1.target='_blank';document.form1.action='print_student.php'")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_back, "button","javascript:self.location.href='student_report.php'")?>&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>

<input type="hidden" name="pageNo" value="<?= $li->pageNo; ?>"/>
<input type="hidden" name="order" value="<?= $li->order; ?>"/>
<input type="hidden" name="field" value="<?= $li->field; ?>"/>
<input type="hidden" name="page_size_change" value=""/>
<input type="hidden" name="numPerPage" value="<?= $li->page_size?>"/>
<input type="hidden" name="showReportOption" value="0">
<input type='hidden' name='gdConduct' value='<?=$gdConduct?>'>
<input type='hidden' name='misconduct' value='<?=$misconduct?>'>
<input type='hidden' name='award_punishment_flag' value='<?=$award_punishment_flag?>'>
</form>

<script>
<!--
function init()
{
	var obj = document.form1;
	
	// Semester
	changeTerm('<?=$SchoolYear1?>', 'semester1');
	setTimeout("changeTerm('<?=$SchoolYear2?>', 'semester2')",1000);
}

init();
//-->
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>