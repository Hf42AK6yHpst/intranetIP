<?php
# modifying by : Bill

###### Change Log [Start] ######
#
#	Date	:	2017-10-31 (Bill)	[2017-0403-1552-54240]
#				Support HKUGA Cust - only allow HOY and Class Teacher to view Conduct Records ($sys_custom['eDiscipline']['HOY_Access_GM'])
#
#	Date	:	2016-07-14 (Bill)
#				fixed incorrect record count due to Group By in sql
#
#	Date	:	2016-07-04 (Bill)	[2016-0526-1018-39096]
#				add Class and PIC drop down list for teacher
#
#	Date	:	2016-04-08 (Bill)	[2016-0224-1423-31073]
#				added remarks for prefix style of deleted teacher account
#
#	Date	:	2015-09-29 (Bill)
#				add exit afer $ldiscipline->NO_ACCESS_RIGHT_REDIRECT()
#
#	Date	:	2015-07-31 (Bill)
#				fixed cannot search ' and "	[DM#2879]
#				fixed incorrect search result if same ItemID but diff CategoryID (homework and others) > $check_cat [Case#P68212]
#
#	Date	:	2014-05-22 (Carlos)
#				$sys_custom['eDiscipline']['PooiToMiddleSchool'] - added a [Score] column
#
#	Date	:	2014-02-27 (YatWoon)
#				add student checking, is the student selected is belongs to that parent?
#
#	Date	:	2010-09-30 (Henry)
#				display "Sign Notice" in action column rather than "Send Notice"
#
#	Date	:	2010-06-11 (Henry)
#	Detail 	:	allow Staff (Teacher) can view eService > eDiscipline
#
####### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

$sortEventDate = 4;			# Event Date is the 4th SQL column
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$sortEventDate += 1;
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve Table View
if ($ck_approval_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_approval_page_number", $pageNo, 0, "", "", 0);
	$ck_approval_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_approval_page_number!="")
{
	$pageNo = $ck_approval_page_number;
}

if ($ck_approval_page_order!=$order && $order!="")
{
	setcookie("ck_approval_page_order", $order, 0, "", "", 0);
	$ck_approval_page_order = $order;
}
else if (!isset($order) && $ck_approval_page_order!="")
{
	$order = $ck_approval_page_order;
}

if ($ck_approval_page_field!=$field && $field!="")
{
	setcookie("ck_approval_page_field", $field, 0, "", "", 0);
	$ck_approval_page_field = $field;
}
else if (!isset($field) || $ck_approval_page_field == "")
{
	//$field = $ck_right_page_field;
	$field = $sortEventDate;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

# Temp assign memory of this page
ini_set("memory_limit", "150M");

$TargetStudentID = IntegerSafe($TargetStudentID);
$SchoolYear = IntegerSafe($SchoolYear);
$approved = IntegerSafe($approved);
$rejected = IntegerSafe($rejected);
$waived = IntegerSafe($waived);
$RecordType = IntegerSafe($RecordType);

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lclass = new libclass();

# Check access right (View page)
if($_SESSION['UserType']!=2&& $_SESSION['UserType']!=3 && $_SESSION['UserType']!=1)
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
if(!$ldiscipline->CHECK_ACCESS("Discipline-VIEWING-GoodConduct_Misconduct-View") && $_SESSION['UserType']!=1) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
/*
if(!$ldiscipline->CHECK_ACCESS("Discipline-VIEWING-Award_Punishment-View") && $_SESSION['UserType']!=1) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
*/

# Check Teacher access right
if($_SESSION['UserType']==1) {
	if(!$ldiscipline->checkTeacherAccessEserviceRight($UserID)) {	
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$currentYearID = ($SchoolYear=="") ? Get_Current_Academic_Year_ID() : $SchoolYear;

###### Initial StudentIDAry ######
	#### Gen ChildrenFilter
	$StudentIdAry = Array();
	if($_SESSION['UserType']==3)		# Parent View
	{
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		$luser = new libuser($UserID);
		$ChildrenAry = $luser->getChildrenList();
		
		if(!empty($TargetStudentID))
		{
			#################################################
			# Check the child is belongs to parent or not
			#################################################
			$valid_child = false;
			foreach($ChildrenAry as $k => $d)
			{
				if($d['StudentID'] == $TargetStudentID) $valid_child = true;	
			}
			if(!$valid_child)
			{
				$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
				exit;
			}
		}
		
		if(count($ChildrenAry)>1)
		{
			$ChildrenFilter =  getSelectByArray($ChildrenAry, " name='TargetStudentID' id='TargetStudentID'  onchange='document.form1.submit()'", $TargetStudentID, 1);
			for($i=0; $i<count($ChildrenAry); $i++)
				$StudentIDAry[$i] = $ChildrenAry[$i][0]; 
			
			if(empty($TargetStudentID))
				$StudentIDStr = implode(",",$StudentIDAry);
			else
				$StudentIDStr = $TargetStudentID;
		}
		else
		{
			$ChildrenFilter = $ChildrenAry[0][1];
			$StudentIDAry[] = $ChildrenAry[0][0];
			$StudentIDStr = $ChildrenAry[0][0];
		}
	}
	else if($_SESSION['UserType']==2)	# Student View
	{
		$StudentIDAry[] = $UserID;
		$StudentIDStr = $UserID;
	}
	else 								# Teacher View
	{
		$studentIDAry = array();
		$studentIDAry2 = array();
		
		$studentIDAry = $ldiscipline->getStudentListByClassTeacherID("",$UserID, $currentYearID);
		$studentIDAry2 = $ldiscipline->getStudentListBySubjectTeacherID("", $UserID, $currentYearID);
		
		$newStudentAry = array_merge($studentIDAry, $studentIDAry2);
		$newStudentAry = array_unique($newStudentAry);
		
		if(sizeof($newStudentAry)>0)
			$StudentIDStr = implode(',', $newStudentAry);
		else 
			$StudentIDStr = "''";
	}

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$choiceType = ($RecordType!="1" && $RecordType != "-1") ? $i_Discipline_System_Award_Punishment_All_Records : "<a href='javascript:reloadForm(0)'>".$i_Discipline_System_Award_Punishment_All_Records."</a>";
$choiceType .= " | ";
$choiceType .= ($RecordType=="1") ? "<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_gd_conduct.gif' width='20' height='20' border='0' align='absmiddle'> {$i_Discipline_GoodConduct}" : "<a href='javascript:reloadForm(1)'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_gd_conduct.gif' width='20' height='20' border='0' align='absmiddle'> {$i_Discipline_GoodConduct}</a>";
$choiceType .= " | ";
$choiceType .= ($RecordType=="-1") ? "<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_misconduct.gif' width='20' height='20' border='0' align='absmiddle'> {$i_Discipline_Misconduct}" : "<a href='javascript:reloadForm(-1)'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_misconduct.gif' width='20' height='20' border='0' align='absmiddle'> {$i_Discipline_Misconduct}</a>";

######### Conditions #########
$conds = "";

// [2016-0526-1018-39096] Teacher only
if($_SESSION['UserType']==1)
{
	# Class Condition #
	if (!$sys_custom['eDisciplinev12_Munsang_NewLeafScheme'] && $targetClass != '' && $targetClass!="0")
	{
	    if(is_numeric($targetClass)) {
	    	$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE YearID='$targetClass'";
	    	$temp = $ldiscipline->returnVector($sql);
	    	$conds .= (sizeof($temp)>0) ? " AND yc.YearClassID IN (".implode(',', $temp).")" : "";
		}
	    else {
			$conds .= " AND yc.YearClassID='".substr($targetClass,2)."'";
	    }
	}
	else if($newleaf != 1 && $targetClass != "" && $targetClass != "0")
	{
	    if(is_numeric($targetClass)) {
	    	$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE YearID='$targetClass'";
	    	$temp = $ldiscipline->returnVector($sql);
	    	$conds .= (sizeof($temp)>0) ? " AND yc.YearClassID IN (".implode(',', $temp).")" : "";
		}
		else {
			$conds .= " AND yc.YearClassID='".substr($targetClass,2)."'";
		}
	}
}

# RecordType #
if ($RecordType != '' && $RecordType != 0) {
    $conds .= " AND a.RecordType = '$RecordType'";
}
else {
	$RecordType = 0;
}

# School Year Menu #
$yearSelected = 0;
$currentYearInfo = Get_Current_Academic_Year_ID();
$SchoolYear = ($SchoolYear == '') ? $currentYearInfo : $SchoolYear;

$yearName = $ldiscipline->getAcademicYearNameByYearID($SchoolYear);
$yearArr = $ldiscipline->getGMSchoolYear($SchoolYear);

$selectSchoolYear = "<select name='SchoolYear' onChange='document.form1.semester.value=\"\";reloadForm($MeritType)'>";
/*
$selectSchoolYear .= "<option value='0'";
$selectSchoolYear .= ($SchoolYear==0) ? " selected" : "";
$selectSchoolYear .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
*/
for($i=0; $i<sizeof($yearArr); $i++) {
	$selectSchoolYear .= "<option value='".$yearArr[$i][0]."'";
	if($SchoolYear==$yearArr[$i][0]) {
		$selectSchoolYear .= " SELECTED";
	}
	if(Get_Current_Academic_Year_ID()==$yearArr[$i][0]) {
		$yearSelected = 1;
	}
	$selectSchoolYear .= ">".$yearArr[$i][1]."</option>";
}
if($yearSelected==0) {
	$selectSchoolYear .= "<option value='".Get_Current_Academic_Year_ID()."'";
	$selectSchoolYear .= (Get_Current_Academic_Year_ID()==$SchoolYear) ? " selected" : "";
	$selectSchoolYear .= ">".$ldiscipline->getAcademicYearNameByYearID(Get_Current_Academic_Year_ID())."</option>";
}
$selectSchoolYear .= "</select>";

# School Year Condition #
if($SchoolYear != '' && $SchoolYear != 0) {
	$conds .= " AND a.AcademicYearID = '$SchoolYear' AND yc.AcademicYearID='$SchoolYear'";	
}
//$extraConds = " AND a.AcademicYearID = yc.AcademicYearID";

# Semester Menu #
$sql = "SELECT YearTermID, YearTermNameEN, YearTermNameB5 FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID='$currentYearID' ORDER BY TermStart";
$semResult = $ldiscipline->returnArray($sql, 3);
//echo $sql;
$SemesterMenu = "<select name='semester' onChange='reloadForm($RecordType)'>";
$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester!='WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for($i=0; $i<sizeof($semResult); $i++) {
	list($id, $nameEN, $nameB5) = $semResult[$i];
	$semName = Get_Lang_Selection($nameB5, $nameEN);
	$SemesterMenu .= "<option value='$id'";
	$SemesterMenu .= ($semester==$id) ? " selected" : "";
	$SemesterMenu .= ">$semName</option>";
}
$SemesterMenu .= "</select>";

# Semester Condition #
if($semester != '' && $semester != 'WholeYear') {
	$conds .= " AND (a.YearTermID = '$semester')";
}

// [2016-0526-1018-39096] Teacher only
if($_SESSION['UserType']==1)
{
	# Class Menu #
	if($ldiscipline->use_newleaf && $sys_custom['eDisciplinev12_Munsang_NewLeafScheme'] && $newleaf==1) {
		if($targetClass=="") $targetClass = 2;
		$select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"reloadForm($RecordType)\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_general_please_select);
		//$stdAry = $ldiscipline->storeStudent('0', $targetClass, $SchoolYear);
	}
	else {
		$select_class = $lclass->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"reloadForm($RecordType)\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes);
	}
	
	# PIC Menu #
	if($semester != '' && $semester != 'WholeYear') $thisSemester = $semester; 
	$PICArray = $ldiscipline->getPICInArray("DISCIPLINE_ACCU_RECORD", $currentYearID, $thisSemester);
	if(sizeof($PICArray)==0) {
		$PICArray = array();
	}
	$picSelection = getSelectByArray($PICArray," name=\"pic\" onChange=\"reloadForm($RecordType)\"",$pic,0,0,"".$i_Discipline_Detention_All_Teachers."");
	
	# PIC Condition #
	if($pic != "") {
		$conds .= " AND concat(',',a.PICID,',') LIKE '%,$pic,%'";
	}
}

$conds2 = "";
# Approved #  	// released record also be approved
if($approved == 1) {
	$approvedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_APPROVED;
}

# Rejected #
if($rejected == 1) {
	$rejectedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_REJECTED;
}

# Waived #
if($waived == 1) {
	$waivedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_WAIVED;
}

$conds2 .= ($conds2 != "") ? ")" : "";

##############

$i_Merit_MinorCredit = addslashes($i_Merit_MinorCredit);
$i_Merit_MajorCredit = addslashes($i_Merit_MajorCredit);
$i_Merit_SuperCredit = addslashes($i_Merit_SuperCredit);
$i_Merit_UltraCredit = addslashes($i_Merit_UltraCredit);

# $access_level = $ldiscipline->access_level;
$li = new libdbtable2007($field, $order, $pageNo);

$const_status_pending = 0;

$gdConductImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_gd_conduct.gif width=20 height=20 border=0 align=absmiddle title=\'".$i_Discipline_GoodConduct."\'>";
$misconductImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_misconduct.gif width=20 height=20 border=0 align=absmiddle title=\'".$i_Discipline_Misconduct."\'>";
$pendingImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif width=20 height=20 align=absmiddle title=\'Waiting For Approval\'>";
$rejectImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif width=20 height=20 align=absmiddle title=Rejected>";
$releasedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_to_student.gif width=20 height=20 align=absmiddle title=\'Released to Student\'>";
$approvedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif width=20 height=20 align=absmiddle title=Approved>";
$waivedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_waived.gif width=20 height=20 align=absmiddle title=Waived>";

$student_namefield = getNamefieldByLang("b.");
$PICID_namefield = getNameFieldWithLoginByLang("d.");
$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
$sbjName = ($intranet_session_language=="en") ? "SUB.EN_DES" : "SUB.CH_DES";

$use_intranet_homework = $ldiscipline->accumulativeUseIntranetSubjectList();

if($fromPPC==1) {
	# no use in student view
	$result = $ldiscipline->retrieveGMRecordFromPPC();
	$GMRecordIDFromPPC = implode(',', $result);
	
		$sql = "SELECT
					CONCAT($clsName,' - ',ycu.ClassNumber) as ClassNameNum,
					$student_namefield as std_name,
					IF(a.RecordType=1,'$gdConductImg','$misconductImg') as conductImg,";
		if($use_intranet_homework){
			$sql .= " IF(a.CategoryID = 2, CONCAT(c.Name,' - ',e.SubjectName), IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name))) as itemName,";
		}
		else {
			$sql .= " IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name)) as itemName,";
		}
		if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
			$sql .= " IF(a.ScoreChange=0,'".$Lang['General']['EmptySymbol']."',a.RecordType*a.ScoreChange) as ScoreChange, ";
		}
		$sql .= "
					LEFT(a.RecordDate,10) as RecordDate,
					PICID,
					a.NoticeID as action,
					f.CategoryID as WaiveDay,
					CONCAT('<font title=\'',a.DateModified,'\'>',LEFT(a.DateModified,10),'</font>') as DateModified,
					CONCAT(
						IF(a.RecordStatus=".DISCIPLINE_STATUS_PENDING.",'$pendingImg',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_REJECTED.",'$rejectImg',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.",'$approvedImg',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.",'$waivedImg','')
					) as status,
					CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>'),
					a.RecordStatus,
					a.RecordID,
					b.UserID,
					a.Remark,
					LEFT(DATE_ADD(a.RecordDate, INTERVAL f.WaiveDay DAY),10) as waivePassDate
				FROM DISCIPLINE_ACCU_RECORD as a
					LEFT OUTER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY as c ON (a.CategoryID = c.CategoryID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID)
					LEFT OUTER JOIN DISCIPLINE_NEW_LEAF_SCHEME as f ON (f.CategoryID = a.CategoryID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_PERIOD as g ON (a.CategoryID=g.CategoryID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_PERIOD as h ON (f.PeriodID=h.PeriodID)
					LEFT OUTER JOIN YEAR_CLASS_USER as ycu ON (b.UserID=ycu.UserID)
					LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID=yc.YearClassID)
				";
	if($use_intranet_homework){
		//$sql .= " LEFT OUTER JOIN INTRANET_SUBJECT as e ON (a.ItemID = e.SubjectID)";
		$sql .= " LEFT OUTER JOIN ASSESSMENT_SUBJECT as SUB ON (a.ItemID = SUB.RecordID)";
	}
	
	$sql .=	($GMRecordIDFromPPC) ? " WHERE a.RecordID IN ($GMRecordIDFromPPC)" : " WHERE a.RecordDate='9999-99-99'";
	$sql .= $conds;
	$sql .= $extraConds;
	$sql .= ($conds2 != "") ? " AND ".$conds2 : "";
	$sql .= " GROUP BY a.RecordID";
}
else if($ldiscipline->use_newleaf && $newleaf==1) {			# Show "new leaf" only
		# no use in student view
		$result = $ldiscipline->GMWaitForNewLeafCount();
		$waitForNewLeafID = implode(',', $result);
		
		$sql = "SELECT
					CONCAT($clsName,' - ',ycu.ClassNumber) as ClassNameNum,
					$student_namefield as std_name,
					IF(a.RecordType=1,'$gdConductImg','$misconductImg') as conductImg,";
		if($use_intranet_homework){
			$sql .= " IF(a.CategoryID = 2, CONCAT(c.Name,' - ',e.SubjectName), IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name))) as itemName,";
		}
		else {
			$sql .= " IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name)) as itemName,";
		}
		if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
			$sql .= " IF(a.ScoreChange=0,'".$Lang['General']['EmptySymbol']."',a.RecordType*a.ScoreChange) as ScoreChange, ";
		}
		$sql .= "
					LEFT(a.RecordDate,10) as RecordDate,
					PICID,
					a.NoticeID as action,
					f.CategoryID as WaiveDay,
					CONCAT('<font title=\'',a.DateModified,'\'>',LEFT(a.DateModified,10),'</font>') as DateModified,
					CONCAT(
						IF(a.RecordStatus=".DISCIPLINE_STATUS_PENDING.",'$pendingImg',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_REJECTED.",'$rejectImg',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.",'$approvedImg',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.",'$waivedImg','')
					) as status,
					CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>'),
					a.RecordStatus,
					a.RecordID,
					b.UserID,
					a.Remark,
					LEFT(DATE_ADD(a.RecordDate, INTERVAL f.WaiveDay DAY),10) as waivePassDate
				FROM DISCIPLINE_ACCU_RECORD as a
					LEFT OUTER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY as c ON (a.CategoryID = c.CategoryID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID)
					LEFT OUTER JOIN DISCIPLINE_NEW_LEAF_SCHEME as f ON (f.CategoryID = a.CategoryID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_PERIOD as g ON (a.CategoryID=g.CategoryID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_PERIOD as h ON (f.PeriodID=h.PeriodID)
					LEFT OUTER JOIN YEAR_CLASS_USER as ycu ON (b.UserID=ycu.UserID)
					LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID=yc.YearClassID)
				";
	if($use_intranet_homework){
		//$sql .= " LEFT OUTER JOIN INTRANET_SUBJECT as e ON (a.ItemID = e.SubjectID)";
		$sql .= " LEFT OUTER JOIN ASSESSMENT_SUBJECT as SUB ON (a.ItemID = SUB.RecordID)";
	}
	
	$sql .=	($waitForNewLeafID) ? " WHERE a.RecordID IN ($waitForNewLeafID)" : " WHERE a.RecordDate='9999-99-99'";
	$sql .= $conds;
	$sql .= $extraConds;
	$sql .= ($conds2 != "") ? " AND ".$conds2 : "";
	$sql .= " GROUP BY a.RecordID";
}
else {
	if($s != "") {		# filter by Searching
		// case 1: quote in string directly stored
		$s = addslashes(trim($s));
		// case 2: quote in string converted to special characters before storage
		$s2 = intranet_htmlspecialchars($s);
		
		$sql = "SELECT	
						CONCAT($clsName,' - ',ycu.ClassNumber) as ClassNameNum,
						$student_namefield,
						IF(a.RecordType=1,'$gdConductImg','$misconductImg') as conductImg,";
		if($use_intranet_homework){
			$sql .= " IF(a.CategoryID = 2, CONCAT(c.Name,' - ',$sbjName), IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name))) as itemName,";
			// [DM#2879] add condition when using intranet default subject
			$check_cat = " and a.CategoryID !=2 ";
		}
		else {
			$sql .= " IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name)) as itemName,";
			// [DM#2879]
			$check_cat = "";
		}
		if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
			$sql .= " IF(a.ScoreChange=0,'".$Lang['General']['EmptySymbol']."',a.RecordType*a.ScoreChange) as ScoreChange, ";
		}
		$sql .= "	LEFT(a.RecordDate,10) as RecordDate,
						PICID,
						a.NoticeID as action,
						f.CategoryID as WaiveDay,
						CONCAT('<font title=\'',a.DateModified,'\'>',LEFT(a.DateModified,10),'</font>') as DateModified,
						CONCAT(
							IF(a.RecordStatus=".DISCIPLINE_STATUS_PENDING.",'$pendingImg',''),
							IF(a.RecordStatus=".DISCIPLINE_STATUS_REJECTED.",'$rejectImg',''),
							IF(a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.",'$approvedImg',''),
							IF(a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.",'$waivedImg','')
						) as status,
						CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>'),
						a.RecordStatus,
						a.RecordID,
						b.UserID,
						a.Remark
					FROM DISCIPLINE_ACCU_RECORD as a
						LEFT OUTER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID)
						LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY as c ON (a.CategoryID = c.CategoryID)
						LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID $check_cat)
						LEFT OUTER JOIN DISCIPLINE_NEW_LEAF_SCHEME as f ON (f.CategoryID = a.CategoryID)
						LEFT OUTER JOIN YEAR_CLASS_USER as ycu ON (b.UserID=ycu.UserID)
						LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID=yc.YearClassID)";
		if($use_intranet_homework){
			$sql .= " LEFT OUTER JOIN ASSESSMENT_SUBJECT as SUB ON (a.ItemID = SUB.RecordID) ";
		}
		$sql .=	"WHERE a.DateInput IS NOT NULL
							AND (
								$clsName like '%$s%'
								OR ycu.ClassNumber like '%$s%'
								OR $student_namefield like '%$s%'
								OR c.Name like '%$s%'
								OR d.Name like '%$s%'
								OR a.RecordDate like '%$s%'
								OR c.Name like '%$s2%'
								OR d.Name like '%$s2%'
							)
					$conds 
					$extraConds
				";
	}
	else {				# filter by option
		$sql = "SELECT
					CONCAT($clsName,' - ',ycu.ClassNumber) as ClassNameNum,
					$student_namefield as std_name,
					IF(a.RecordType=1,'$gdConductImg','$misconductImg') as conductImg,";
					
		if($use_intranet_homework){
			$sql .= " IF(a.CategoryID = 2, CONCAT(c.Name,' - ',$sbjName), IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name))) as itemName,";			
		}
		else {
			$sql .= " IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name)) as itemName,";
		}
		if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
			$sql .= " IF(a.ScoreChange=0,'".$Lang['General']['EmptySymbol']."',a.RecordType*a.ScoreChange) as ScoreChange, ";
		}
		$sql .= "
					LEFT(a.RecordDate,10) as RecordDate,
					PICID,
					a.NoticeID as action,
					f.CategoryID as WaiveDay,
					CONCAT('<font title=\'',a.DateModified,'\'>',LEFT(a.DateModified,10),'</font>') as DateModified,
					CONCAT(
						IF(a.RecordStatus=".DISCIPLINE_STATUS_PENDING.",'$pendingImg',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_REJECTED.",'$rejectImg',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.",'$approvedImg',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.",'$waivedImg','')
					) as status,
					CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>'),
					a.RecordStatus,
					a.RecordID,
					b.UserID,
					a.Remark
				FROM DISCIPLINE_ACCU_RECORD as a
					LEFT OUTER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY as c ON (a.CategoryID = c.CategoryID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID)
					LEFT OUTER JOIN DISCIPLINE_NEW_LEAF_SCHEME as f ON (f.CategoryID = a.CategoryID)
					LEFT OUTER JOIN YEAR_CLASS_USER as ycu ON (b.UserID=ycu.UserID)
					LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID=yc.YearClassID)
					";
		if($use_intranet_homework){
			$sql .= " LEFT OUTER JOIN ASSESSMENT_SUBJECT as SUB ON (SUB.RecordID = a.ItemID) ";
		}
		$sql .=	"WHERE a.DateInput IS NOT NULL
						$conds
						$extraConds
				";
	}
	$sql .= ($conds2 != "") ? " AND ".$conds2 : "";
	
	// [2017-0403-1552-54240] HKUGA Handling
	// 1. HOY Members: View all GM
	// 2. Class Teacher: View students' GM only
	if($sys_custom['eDiscipline']['HOY_Access_GM'])
	{
		if(!$ldiscipline->IS_HOY_GROUP_MEMBER())
		{
			$ClassStudentAry = $ldiscipline->getStudentListByClassTeacherID("", $UserID, $currentYearID);
			$ClassStudentAry = array_unique($ClassStudentAry);
			if(sizeof($ClassStudentAry) > 0) {
				$ClassStudentStr = implode("', '", $ClassStudentAry);
			}
			else {
				$ClassStudentStr = "";
			}
			$sql .= " AND a.StudentID IN ('".$ClassStudentStr."') ";
		}
	}
	else
	{
		$sql .= " AND a.StudentID IN (".$StudentIDStr.") ";
	}

	$sql .= " GROUP BY a.RecordID";
}

//echo $sql;
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$li->field_array = array("concat(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', 1)), IF(INSTR(ClassNameNum, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', -1)), 10, '0')))", "b.EnglishName", "conductImg", "itemName", "ScoreChange", "RecordDate", "PICID", "action", "WaiveDay", "DateModified", "status");
}
else{
	$li->field_array = array("concat(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', 1)), IF(INSTR(ClassNameNum, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', -1)), 10, '0')))", "b.EnglishName", "conductImg", "itemName", "RecordDate", "PICID", "action", "WaiveDay", "DateModified", "status");
}
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $eDiscipline["Record"];;
$li->column_array = array(0,0,0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0,0,0);
$li->IsColOff = 2;

// fixed incorrect record count due to GROUP BY in SQL
$li->count_mode = 1;

// Table Column
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='8%'>".$li->column($pos++, $i_general_class)."</td>\n";
$li->column_list .= "<td>".$li->column($pos++, $i_general_name)."</td>\n";
$li->column_list .= "<td width='1'>".$li->column($pos++, "")."</td>\n";
$li->column_list .= "<td>".$li->column($pos++, $eDiscipline["Category_Item"])."</td>\n";
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$li->column_list .= "<td width='3%'>".$li->column($pos++, $Lang['eDiscipline']['Score'])."</td>\n";
}
$li->column_list .= "<td width='10%' nowrap>".$li->column($pos++, $eDiscipline['EventDate'])."</td>\n";
$li->column_list .= "<td width='15%' nowrap>".$li->column($pos++, $i_Profile_PersonInCharge)."</td>\n";
$li->column_list .= "<td width='10%' nowrap>".$eDiscipline["Action"]."</td>\n";$pos++;
/*
if($ldiscipline->use_newleaf && $newleaf==1)
	$li->column_list .= "<td nowrap>".$i_Discipline_System_GoodConduct_Misconduct_Waive_Date_Count."</td>\n";$pos++;
*/
$li->column_list .= "<td width='10%' nowrap>".$li->column($pos++, $i_Discipline_Last_Updated)."</td>\n";
$li->column_list .= "<td nowrap>".$li->column($pos++, $i_Discipline_System_Discipline_Status)."</td>\n";

$categoryItemLocation = 3;
$PICLocation = 5;
$StatusLocation = 11;
$checkboxLocation = 10;
$recordIDLocation = 12;
$actionLocation = 6;		
$noticeIDLocation = 6;
$waiveDayLocation = 7;
$waivedByLocation = 9;	
$remarkLocation = 14;	
/*
if($ldiscipline->use_newleaf) 
	$newleafFlag = $newleaf;
*/
$historyLocation = 99;		# no use
$caseIDLocation = 99;		# no use

if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$PICLocation += 1;
	$StatusLocation += 1;
	$checkboxLocation += 1;
	$recordIDLocation += 1;
	$actionLocation += 1;		
	$noticeIDLocation += 1;
	$waiveDayLocation += 1;
	$waivedByLocation += 1;	
	$remarkLocation += 1;
}

######## End of Table Content #########
#######################################

# Access Right Checking
$actionLayer = "";
if($ldiscipline->use_newleaf && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf") && $newleaf==1) {
	if($actionLayer != "") {
		$actionLayer .= "<tr>";
		$actionLayer .= "<td align='left' valign='top' class='dotline' height='2'><img src='{$image_path}/{$LAYOUT_SKIN}'/10x10.gif' height='2'></td>";
		$actionLayer .= "</tr>";
	}
	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top'><a href=\"javascript:changeFormAction(document.form1,'RecordID[]','newleaf_update.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_waive.gif' width='12' height='12' border='0' align='absmiddle'> ".$eDiscipline['Waive_By_New_Leaf_Scheme']."</a></td>";
	$actionLayer .= "</tr>";
}
if($fromPPC==1) {
	if($actionLayer != "") {
		$actionLayer .= "<tr>";
		$actionLayer .= "<td align='left' valign='top' class='dotline' height='2'><img src='{$image_path}/{$LAYOUT_SKIN}'/10x10.gif' height='2'></td>";
		$actionLayer .= "</tr>";
	}
	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top'><a href=\"javascript:changeFormAction(document.form1,'RecordID[]','cancelPPC_update.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='12' height='12' border='0' align='absmiddle'> ".$eDiscipline['Setting_Cancel_PPC']."</a></td>";
	$actionLayer .= "</tr>";
}

# Menu Highlight Setting
$CurrentPageArr['eServiceeDisciplinev12'] = 1;
$CurrentPage = "Viewing_GoodConductMisconduct";

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eDiscipline['Good_Conduct_and_Misconduct'], "");

# Start Layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--

var xmlHttp
var jsDetention = [];
var jsWaive = [];
var jsHistory = [];
var jsRemark = [];

function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3)
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function removeCat(obj,element,page){
	var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
    if(countChecked(obj,element)==0) {
        alert(globalAlertMsg2);
    }
    else{
        if(confirm(alertConfirmRemove)){
            obj.action=page;
            obj.method="post";
            obj.submit();
        }
    }
}

function reloadForm(val) {
	document.form1.RecordType.value = val;
	document.form1.s.value = "";
	document.form1.pageNo.value = 1;
	//document.form1.newleaf.value = "";
	document.form1.submit();
}

function goSearch(obj) {
	/*
	if(document.form1.text.value=="") {
		alert("<?=$i_Discipline_System_Award_Punishment_Search_Alert?>");
		document.form1.text.focus();
		return false;
	}
	else {
	*/
	document.form1.s.value = document.form1.text.value;
	document.form1.pageNo.value = 1;
	document.form1.newleaf.value = "";
	document.form1.submit();
	return false;
	//}
}

function changeFormAction(obj,element,url) {
	if(countChecked(obj,element)==0) {
		alert(globalAlertMsg2);
	}
	else {
		document.form1.action = url;
		document.form1.submit();
	}
}

function checkCR(evt) {
	var evt  = (evt) ? evt : ((event) ? event : null);
	var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
	if ((evt.keyCode == 13) && (node.type=="text")) {return false;}
}

//document.onkeypress = checkCR;

/*********************************/
function getObject( obj ) {
	if ( document.getElementById ) {
		obj = document.getElementById( obj );
	}
	else if ( document.all ) {
		obj = document.all.item( obj );
	}
	else {
		obj = null;
	}
	return obj;
}

function moveObject( obj, e, moveLeft ) {
	var tempX = 0;
	var tempY = 0;
	var offset = 8;
	var objHolder = obj;
	var moveDistance = 0;
	
	obj = getObject( obj );
	if (obj==null) {return;}
	
	if (document.all) {
		tempX = event.clientX + document.body.scrollLeft;
		tempY = event.clientY + document.body.scrollTop;
	}
	else {
		tempX = e.pageX;
		tempY = e.pageY;
	}
	
	if(moveLeft == undefined) {
		moveDistance = 300;		
	}
	else {
		moveDistance = moveLeft;
	}
	
	if (tempX < 0){tempX = 0} else {tempX = tempX - moveDistance}
	if (tempY < 0){tempY = 0} else {tempY = tempY}
	
	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
	displayObject( objHolder, true );
}

function displayObject( obj, show ) {
	obj = getObject( obj );
	if (obj==null) return;

	obj.style.display = show ? 'block' : 'none';
	obj.style.visibility = show ? 'visible' : 'hidden';
}

function changeClickID(id) {
	document.form1.clickID.value = id;
	hideAllOtherLayer();
}

function hideAllOtherLayer() {
	var layer = "";
	if(jsDetention.length != 0) {
		for(i=0;i<jsDetention.length;i++) {
			layer = "show_detail" + jsDetention[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsWaive.length != 0) {
		for(i=0;i<jsWaive.length;i++) {
			layer = "show_waive" + jsWaive[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsRemark.length != 0) {
		for(i=0;i<jsRemark.length;i++) {
			layer = "show_remark" + jsRemark[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsHistory.length != 0) {
		for(i=0;i<jsHistory.length;i++) {
			layer = "show_history" + jsHistory[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
}

function stateChanged() 
{
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{
		var id = document.form1.clickID.value;
		document.getElementById("show_detail"+id).style.zIndex = "1";
		document.getElementById("show_detail"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_detail"+id).style.border = "0px solid #A5ACB2";
	}
}

function stateChanged2() 
{
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{
		var id = document.form1.clickID.value;
		document.getElementById("show_waive"+id).style.zIndex = "1";
		document.getElementById("show_waive"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_waive"+id).style.border = "0px solid #A5ACB2";
	}
}

function stateChanged3() 
{
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{
		var id = document.form1.clickID.value;
		document.getElementById("show_remark"+id).style.zIndex = "1";
		document.getElementById("show_remark"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_remark"+id).style.border = "0px solid #A5ACB2";
	}
}

function showResult(str,flag)
{
	xmlHttp = GetXmlHttpObject()
	if (xmlHttp==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	}
	
	var url = "";
	url = "goodconduct_misconduct_get_live.php?flag="+flag;
	url = url + "&RecordID=" + str
	url = url + "&sid=" + Math.random()
	
	if(flag=='detention') {
		xmlHttp.onreadystatechange = stateChanged 
	}
	else if(flag=='waived') {
		xmlHttp.onreadystatechange = stateChanged2 
	}
	else if(flag=='record_remark') {
		xmlHttp.onreadystatechange = stateChanged3 
	}
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
//-->
</script>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form1" method="post" onSubmit="return goSearch('document.form1')">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<?=$ChildrenFilter?>
						</td>
						<td align="right"><input name="text" type="text" class="formtextbox" value="<?=intranet_htmlspecialchars(stripslashes(stripslashes($s)))?>">
							<?=$linterface->GET_BTN($button_find, "submit");?>
						</td>
					</tr>
				</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right"><?= $linterface->GET_SYS_MSG($msg) ?>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td height="30">
												<?=$selectSchoolYear?>
												<?=$SemesterMenu?>
												<?=$select_class?>
												<?=$picSelection?>
											</td>
											<td align="right" valign="middle" class="thumb_list"><?=$choiceType?></td>
										</tr>
										<tr>
											<td colspan="2" align="center">
												<?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $i_Discipline_System_Award_Punishment_Warning) ?>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
												<div class="selectbox_group"> <a href="javascript:;" onClick="MM_showHideLayers('status_option','','show')"><?=$i_Discipline_System_Award_Punishment_Select_Status?></a> </div>
												<br style="clear:both">
												<div id="status_option" class="selectbox_layer" style="width:150px">
													<table width="100%" border="0" cellspacing="0" cellpadding="3">
														<tr>
															<td><!--<input type="checkbox" name="waitApproval" id="waitApproval" value="1" <?=$waitApprovalChecked?>>
																<label for="waitApproval"><?=$i_Discipline_System_Award_Punishment_Pending?></label><br>-->
																<input type="checkbox" name="approved" id="approved" value="1" <?=$approvedChecked?>>
																<label for="approved"><?=$i_Discipline_System_Award_Punishment_Approved?></label><br>
																<input type="checkbox" name="rejected" id="rejected" value="1" <?=$rejectedChecked?>>
																<label for="rejected"><?=$i_Discipline_System_Award_Punishment_Rejected?></label></td>
														</tr>
														<tr>
															<td align="left" valign="top" class="dotline" height="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="2"></td>
														</tr>
														<tr>
															<td><input type="checkbox" name="waived" id="waived" value="1" <?=$waivedChecked?>>
																<label for="waived"><?=$i_Discipline_System_Award_Punishment_Waived?></label></td>
														</tr>
														<tr>
															<td align="left" valign="top" class="dotline" height="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="2"></td>
														</tr>
														<tr>
															<td align="center" class="tabletext">
																<?= $linterface->GET_BTN($button_apply, "button", "javascript:reloadForm($RecordType)")?>
																<?= $linterface->GET_BTN($button_cancel, "button", "javascript: onClick=MM_showHideLayers('status_option','','hide');form.reset();")?>
															</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<?=$li->displayFormat_Split_PIC("GoodConduct_Misconduct",$PICLocation,$StatusLocation,$checkboxLocation,$actionLocation,$recordIDLocation,"{$image_path}/{$LAYOUT_SKIN}", $historyLocation, $caseIDLocation, $waivedByLocation, $noticeIDLocation, $waiveDayLocation, $categoryItemLocation, $remarkLocation, $newleafFlag, "","", $Lang['eDiscipline']['SignNotice'])?>
					</td>
				</tr>
			</table><br>
			<div width="100%" align="left"><?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?></div>
			
			<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>"/>
			<input type="hidden" name="order" value="<?php echo $li->order; ?>"/>
			<input type="hidden" name="field" value="<?php echo $li->field; ?>"/>
			<input type="hidden" name="page_size_change" value=""/>
			<input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
			<input type="hidden" name="RecordType" value="<?=$RecordType ?>">
			<input type="hidden" name="s" value="<?=stripslashes(stripslashes($s))?>"/>
			<input type="hidden" name="newleaf" value="<?=$newleaf?>"/>
			<input type="hidden" name="fromPPC" value="<?=$fromPPC?>"/>
			<input type="hidden" name="clickID" id="clickID" />
			</form>
		</td>
	</tr>
</table>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>