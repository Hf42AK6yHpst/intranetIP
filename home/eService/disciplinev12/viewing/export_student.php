<?php
// using: Bill

########################################################
#
#	Date:	2017-10-31 (Bill)	[2017-0403-1552-54240]
#			Hide GM row	($sys_custom['eDiscipline']['HideAllGMReport'])
#
#	Date:	2014-03-05	YatWoon
#			add checking for student and parent (not allow view others student data)
#
########################################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$TargetStudentID = IntegerSafe($TargetStudentID);
$studentID = IntegerSafe($studentID);
$waive_record1 = IntegerSafe($waive_record1);
$award_punish_period = IntegerSafe($award_punish_period);
$SchoolYear1 = IntegerSafe($SchoolYear1);
$waive_record2 = IntegerSafe($waive_record2);
$gdConduct_miscondict_period = IntegerSafe($gdConduct_miscondict_period);
$SchoolYear2 = IntegerSafe($SchoolYear2);
$award_punishment_flag = IntegerSafe($award_punishment_flag);
$gdConduct = IntegerSafe($gdConduct);
$misconduct = IntegerSafe($misconduct);

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

ini_set("memory_limit", "150M"); 

if($_SESSION['UserType']!=2 && $_SESSION['UserType']!=3)
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
if(!$ldiscipline->CHECK_ACCESS("Discipline-VIEWING-StudentReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

if($_SESSION['UserType']==3)	# Parent
{
	if(!empty($studentID))
	{
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		$luser = new libuser($UserID);
		$ChildrenAry = $luser->getChildrenList();
	
		#################################################
		# Check the child is belongs to parent or not
		#################################################
		$valid_child = false;
		foreach($ChildrenAry as $k => $d)
		{
			if($d['StudentID'] == $studentID) $valid_child = true;	
		}
		if(!$valid_child)
		{
			$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
			exit;
		}
	}
}
else if($_SESSION['UserType']==2)	# Student
{
	if($studentID!=$UserID)
	{
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

$ExportArr = array();

if (!$lstudentprofile->is_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='1' id='merit1' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==1) ? " checked" : "";
	}
	$meritType .= "><label for='merit1'>".$i_Merit_Merit."</label>";
}
if (!$lstudentprofile->is_min_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='2' id='merit2' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==2) ? " checked" : "";
	}
	$meritType .= "><label for='merit2'>".$i_Merit_MinorCredit."</label>";
}
if (!$lstudentprofile->is_maj_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='3' id='merit3' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==3) ? " checked" : "";
	}
	$meritType .= "><label for='merit3'>".$i_Merit_MajorCredit."</label>";
}
if (!$lstudentprofile->is_sup_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='4' id='merit4' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==4) ? " checked" : "";
	}
	$meritType .= "><label for='merit4'>".$i_Merit_SuperCredit."</label>";
}
if (!$lstudentprofile->is_ult_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='5' id='merit5' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==5) ? " checked" : "";
	}
	$meritType .= "><label for='merit5'>".$i_Merit_UltraCredit."</label>";
}

if (!$lstudentprofile->is_black_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-1' id='demerit1' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-1) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit1'>".$i_Merit_BlackMark."</label>";
}
if (!$lstudentprofile->is_min_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-2' id='demerit2' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-2) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit2'>".$i_Merit_MinorDemerit."</label>";
}
if (!$lstudentprofile->is_maj_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-3' id='demerit3' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-3) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit3'>".$i_Merit_MajorDemerit."</label>";
}
if (!$lstudentprofile->is_sup_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-4' id='demerit4' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-4) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit4'>".$i_Merit_SuperDemerit."</label>";
}
if (!$lstudentprofile->is_ult_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-5' id='demerit5' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-5) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit5'>".$i_Merit_UltraDemerit."</label>";
}

$meritDemerit[5] = $i_Merit_Warning;
$meritDemerit[6] = $i_Merit_Merit;
$meritDemerit[7] = $i_Merit_MinorCredit;
$meritDemerit[8] = $i_Merit_MajorCredit;
$meritDemerit[9] = $i_Merit_SuperCredit;
$meritDemerit[10] = $i_Merit_UltraCredit;
$meritDemerit[4] = $i_Merit_BlackMark;
$meritDemerit[3] = $i_Merit_MinorDemerit;
$meritDemerit[2] = $i_Merit_MajorDemerit;
$meritDemerit[1] = $i_Merit_SuperDemerit;
$meritDemerit[0] = $i_Merit_UltraDemerit;

/*
$SchoolYear = getCurrentAcademicYear();
$selectSchoolYear1 .= "<option value='0'";
$selectSchoolYear1 .= ($SchoolYear1==0) ? " selected" : "";
$selectSchoolYear1 .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
$selectSchoolYear1 .= $ldiscipline->getConductSchoolYear($SchoolYear1);

$selectSchoolYear2 .= "<option value='0'";
$selectSchoolYear2 .= ($SchoolYear2==0) ? " selected" : "";
$selectSchoolYear2 .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
$selectSchoolYear2 .= $ldiscipline->getConductSchoolYear($SchoolYear2);

# Semester Menu #
$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));

$SemesterMenu1 .= "<option value='WholeYear'";
$SemesterMenu1 .= ($semester1 != 'WholeYear') ? "" : " selected";
$SemesterMenu1 .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for ($i=0; $i<sizeof($semester_data); $i++)
{
	$target_sem = $semester_data[$i];
	$line = split("::",$target_sem);
	list ($name,$current) = $line;
	$SemesterMenu1 .= "<option value='".$name."'";
	if($name==$semester1) { $SemesterMenu1 .= " selected"; }
	$SemesterMenu1 .= ">".$name."</option>";
}

$SemesterMenu2 .= "<option value='WholeYear'";
$SemesterMenu2.= ($semester2 != 'WholeYear') ? "" : " selected";
$SemesterMenu2 .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for ($i=0; $i<sizeof($semester_data); $i++)
{
	$target_sem = $semester_data[$i];
	$line = split("::",$target_sem);
	list ($name,$current) = $line;
	$SemesterMenu2 .= "<option value='".$name."'";
	if($name==$semester2) { $SemesterMenu2 .= " selected"; }
	$SemesterMenu2 .= ">".$name."</option>";
}
*/

# Class #
//$select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"showResult(this.value,'')\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_alert_pleaseselect);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$classAry = array();
$studentAry = array();
$studentAry = $ldiscipline->storeStudent($studentID, $targetClass);

$meritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif width=20 height=20 border=0 align=absmiddle border=0>";
$demeritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif width=20 height=20 border=0 align=absmiddle border=0>";
$waivedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_waived.gif width=20 height=20 align=absmiddle title=Waived border=0>";

$semester1Text = ($semester1=='WholeYear') ? $i_Discipline_System_Award_Punishment_Whole_Year : $semester1;
$semester2Text = ($semester2=='WholeYear') ? $i_Discipline_System_Award_Punishment_Whole_Year : $semester2;

# SQL conditions (A&P) #
if($award_punish_period == 1) {
	if($SchoolYear1 != '0') {		# Specific School Year
		//$date1 = $SchoolYear1." ".$semester1Text;
		//$conds .= " AND a.Year='$SchoolYear1'";
		
		$SchoolYear1_Name = $ldiscipline->getAcademicYearNameByYearID($SchoolYear1);
		$date1 = $SchoolYear1_Name." ".$semester1Text;
		$conds .= " AND (a.AcademicYearID='$SchoolYear1' or a.Year='$SchoolYear1_Name')";
	}
	else {							# All School Year
		$date1 = $i_Discipline_System_Award_Punishment_All_School_Year." ".$semester1Text;
		$academicYear = $ldiscipline->generateAllSchoolYear();
	}
	
	if($semester1!='WholeYear') {	# Specific Semester
		$conds .= " AND a.Semester='$semester1'";
	}
}
else {								# Specific Date Range
	$date1 = $Section1Fr." To ".$Section1To;
	$conds .= " AND (a.RecordDate BETWEEN '$Section1Fr' AND '$Section1To')";
}

if($waive_record1 == 1) {		# with waived record
//	$conds .= " AND (a.RecordStatus=".DISCIPLINE_STATUS_APPROVED." OR a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.")";
//	$conds .= " AND ((a.RecordStatus=".DISCIPLINE_STATUS_APPROVED." AND (a.ReleaseStatus=".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL)) OR (a.RecordStatus=".DISCIPLINE_STATUS_WAIVED." AND (a.ReleaseStatus=".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL)))";
	$conds .= " AND ((a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.") OR (a.RecordStatus=".DISCIPLINE_STATUS_WAIVED."))";
}
else { 							# only approved record
//	$conds .= " AND a.RecordStatus=".DISCIPLINE_STATUS_APPROVED;	
//	$conds .= " AND (a.RecordStatus=".DISCIPLINE_STATUS_APPROVED." AND (a.ReleaseStatus = ".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL))";	
	$conds .= " AND (a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.")";	
}

# Only released records are shown to students / parent
$conds .= " AND a.ReleaseStatus = ".DISCIPLINE_STATUS_RELEASED;	

# Award & Punishment option #
if($merit != '') {
	$meritChoice = implode(",",$merit);
}
if($demerit != '') {
	$demeritChoice = implode(",",$demerit);
}

if($all_award != '' && $all_punishment != '') {			# Award + Punishment
	$conds .= " AND (a.MeritType=1 OR a.MeritType=-1)";
}
else if($all_award != '' && $demerit != '') {			# Award + Demerit
	$conds .= " AND (a.MeritType=1 OR a.ProfileMeritType IN ($demeritChoice))";
}
else if($all_punishment != '' && $merit != '') {		# Punish + Merit
	$conds .= " AND (a.MeritType=-1 OR a.ProfileMeritType IN ($meritChoice))";
}
else if($merit != '' && $demerit != '') {				# Merit + Demerit
	$conds .= " AND (a.ProfileMeritType IN ($meritChoice) or a.ProfileMeritType IN ($demeritChoice))";
}
else if($all_award != '')	{							# only Award
	$conds .= " AND a.MeritType=1";
}
else if($all_punishment != '') {						# only Punishment
	$conds .= " AND a.MeritType=-1";
}
else if($merit != '')	 {								# Merit
	$conds .= " AND a.ProfileMeritType IN ($meritChoice)";
}
else if($demerit != '') {								# Demerit
	$conds .= " AND a.ProfileMeritType IN ($demeritChoice)";
}

$filename = "student_report.csv";	
$export_content = $i_Discipline_System_Reports_Discipline_report."\n\n";

/*
if($goodconductid != '') {
	$gdConductChoice = implode(",", $goodconductid);	
}
if($misconductid != '') {
	$misConductChoice = implode(",", $misconductid);	
}
*/
$all_gd_conduct = true;
$all_misconduct = true;		

# SQL Statement
$exportColumn = array("#", $i_EventDate, $eDiscipline["Record"], $i_Discipline_Reason, $i_Discipline_PIC, $i_Discipline_System_Award_Punishment_Waived);
$exportColumn1 = array("#", $iDiscipline['Accumulative_Category_Item'], $eDiscipline['Setting_AccumulatedTimes'], $i_Discipline_System_Award_Punishment_Waived);

for($i=0;$i<sizeof($studentAry);$i++)
{
	# Student Info
	$stdName = $ldiscipline->getStudentNameByID($studentAry[$i]);
	list($userID, $std_name, $className, $classNumber) = $stdName[0];
		
	$export_content .= "\n".$i_Discipline_Class." : ".$className."\n";
	$export_content .= $i_Discipline_Student." : ".$std_name."(".$classNumber.")\n\n";
	
	# --------- Table A&P ---------- 
	
	// echo $award_punishment_flag."/";
	if($userID != '')
	{
		if($award_punishment_flag==1) {
			$sql = "
				SELECT 
					a.RecordDate as RecordDate, 
					a.ProfileMeritCount as meritCount, 
					a.ProfileMeritType as meritType,
					CONCAT(c.ItemCode,' - ',c.ItemName) as item,
					PICID,
					IF(a.RecordStatus=2,'Yes','') as waivedRecord
			
				FROM DISCIPLINE_MERIT_RECORD as a
					LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
					LEFT OUTER JOIN DISCIPLINE_MERIT_TYPE_SETTING as d ON a.ProfileMeritType = d.MeritType
					LEFT OUTER JOIN INTRANET_USER as e ON a.PICID = e.UserID
				WHERE a.DateInput IS NOT NULL
					AND b.UserID = $userID
					$conds
					ORDER BY a.RecordDate DESC
			";
			$row = $ldiscipline->returnArray($sql,5);
			
			// Create data array for export
			$ExportArr = array();
			
			if(sizeof($row)==0) {
				$export_content .= "{$eDiscipline['Award_and_Punishment']} ({$date1})\n";
				$export_content .= $i_no_record_exists_msg."\n";
			} 
			else {
				for($j=0; $j<sizeof($row); $j++){
					$temp = $row[$j][2] + 5;
					$ExportArr[$j][0] = $j+1;										// #
					$ExportArr[$j][1] = $row[$j][0];								// Event Date
					$ExportArr[$j][2] = $row[$j][1]." ".$meritDemerit[$temp];		// Record
					$ExportArr[$j][3] = $row[$j][3];								// Reason				
				
					$name_field = getNameFieldByLang();								// PIC
					if($row[$j][4] != '') {
						$pic = $row[$j][4];
						
						$sql2 = "SELECT $name_field FROM INTRANET_USER WHERE UserID IN ($pic)";
						$temp = $ldiscipline->returnArray($sql2,1);
						if(sizeof($temp)!=0) {
					    	for($k=0; $k<sizeof($temp); $k++) {
					        	$ExportArr[$j][4] .= $temp[$k][0];
					        	$ExportArr[$j][4] .= ($k != (sizeof($temp)-1)) ? ", " : "";
					    	}
						}
						else {
							$ExportArr[$j][4] = "---";
						}
					}
					else {
						$ExportArr[$j][4] = "---";	
					}
					$ExportArr[$j][5] = $row[$j][5];								// Reason	
				}
				
				$export_content .= "{$eDiscipline['Award_and_Punishment']} ({$date1})\n";
				$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");
			}
		}
		
		# GoodConduct & Misconduct Option #
		if(!$sys_custom['eDiscipline']['HideAllGMReport'])
		{
		if($gdConduct_miscondict_period == 1) {
			/*
			$date2 = ($SchoolYear2 != '0') ? $SchoolYear2." ".$semester2Text : $i_Discipline_System_Award_Punishment_All_School_Year." ".$semester2Text;
			$dateConds = ($SchoolYear2 != '0') ? " AND b.Year='$SchoolYear2'" : "";
			$dateConds .= ($semester2 != 'WholeYear') ? " AND b.Semester='$semester2'" : "";
			*/
			
			$SchoolYear2_Name = $ldiscipline->getAcademicYearNameByYearID($SchoolYear2);
			$date2 = ($SchoolYear2 != '0') ? $SchoolYear2_Name." ".$semester2Text : $i_Discipline_System_Award_Punishment_All_School_Year." ".$semester2Text;
			$dateConds = ($SchoolYear2 != '0') ? " AND (b.AcademicYearID='$SchoolYear2' or b.Year='$SchoolYear2_Name')" : "";
			$dateConds .= ($semester2 != 'WholeYear') ? " AND b.Semester='$semester2'" : "";
		}
		else {
			$date2 = $Section2Fr." To ".$Section2To;
			$dateConds = " AND (b.RecordDate BETWEEN '$Section2Fr' AND '$Section2To')";
		}
		
		if($gdConduct==1) {
			$gdConductContent = $ldiscipline->getGdConductContent2($userID, $waive_record2, $all_gd_conduct, $gdConductChoice, $SchoolYear2, $semester2, $intranet_root, $i_no_record_exists_msg, $dateConds);
			if(sizeof($gdConductContent) != 0) {
				$export_content .= "\n- {$i_Discipline_GoodConduct} - ({$date2})\n";
				$export_content .= $lexport->GET_EXPORT_TXT($gdConductContent, $exportColumn1, "\t", "\r\n", "\t", 0, "11");
			}
		}
		
		if($misconduct==1) {
			$misConductContent = $ldiscipline->getMisConductContent2($userID, $waive_record2, $all_misconduct, $misConductChoice, $SchoolYear2, $semester2, $intranet_root, $i_no_record_exists_msg, $dateConds);
			if(sizeof($misConductContent) != 0) {
				$export_content .= "\n- {$i_Discipline_Misconduct} - ({$date2})\n";
				$export_content .= $lexport->GET_EXPORT_TXT($misConductContent, $exportColumn1, "\t", "\r\n", "\t", 0, "11");
			}
		}
		}
	}
	
}
		
intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>