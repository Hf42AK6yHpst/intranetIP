<?php
# Using: Bill 

# Personal Report
# - show number of merit, demerit pts

##########################################################################
# Report type:
# 1. Punhishment Record
# 2. Award Record
# 3. Detention Attendance Record		<== not support in v12
# 4. Award/Punhishment Record
# 5. Detention Punishment Record		<== not support in v12
# 6. Conduct Mark Change Log
# 7. Subscore Change Log
# 8. Misconduct Record
# 9. Good conduct Record				<== new in v12
# 10. Good/Misconduct Record			<== new in v12
##########################################################################

###############################################
#
#	Date:	2017-10-31 (Bill)	[2017-0403-1552-54240]
#			hide GM related options ($sys_custom['eDiscipline']['HideAllGMReport'])
#
#	Date:	2017-06-28 (Bill)	[2015-0120-1200-33164]
#			show Study Score column if Study Score is enabled
#
#	Date:	2014-03-06 (YatWoon)
#			remove "Year" selection (due to misconfuse with Date range)
#
#	Date:	2014-02-27 (YatWoon)
#			add student checking, is the student selected is belongs to that parent?
#
#	Date:	2011-03-22	YatWoon
#			add flag checking $sys_custom['Discipline_HideRemarkInPersonalReport']
#
###############################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$TargetStudentID = IntegerSafe($TargetStudentID);
$detailType = IntegerSafe($detailType);
$show_waive_status = IntegerSafe($show_waive_status);
$targetID = IntegerSafe($targetID);

$ldiscipline = new libdisciplinev12();

# Check Access Right (View page)
if($_SESSION['UserType']!=2 && $_SESSION['UserType']!=3) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
if(!$ldiscipline->CHECK_ACCESS("Discipline-VIEWING-PersonalReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$linterface = new interface_html();
$lclass = new libclass();
$lsp = new libstudentprofile();

###### Initial StudentIDAry ######
	#### Gen ChildrenFilter (For Parent Mode)
	$StudentIdAry = Array();
	if($_SESSION['UserType']==3)
	{
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		$luser = new libuser($UserID);
		$ChildrenAry = $luser->getChildrenList();
		
		if(!empty($TargetStudentID))
		{
			#################################################
			# Check the child is belongs to parent or not
			#################################################
			$valid_child = false;
			foreach($ChildrenAry as $k => $d)
			{
				if($d['StudentID'] == $TargetStudentID) $valid_child = true;	
			}
			if(!$valid_child)
			{
				$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
				exit;
			}
		}
		
		if(count($ChildrenAry)>1)
		{
			$ChildrenFilter =  getSelectByArray($ChildrenAry, " name='TargetStudentID' id='TargetStudentID'  onchange='document.form1.submit()'", $TargetStudentID,0,1);
			for($i=0; $i<count($ChildrenAry); $i++)
				$StudentIDAry[$i] = $ChildrenAry[$i][0]; 
			
			if(empty($TargetStudentID))
				$StudentIDStr = implode(",",$StudentIDAry);
			else
				$StudentIDStr = $TargetStudentID;
		}
		else
		{
			$ChildrenFilter = $ChildrenAry[0][1];
			$StudentIDAry[] = $ChildrenAry[0][0];
			$StudentIDStr = $ChildrenAry[0][0];
		}
	}
	else
	{
		$StudentIDAry[]=$UserID;
		$StudentIDStr = $UserID;
	}

$targetID = $TargetStudentID?$TargetStudentID:$StudentIDAry[0];

# Retrieve Student Name
$lu_student = new libuser($targetID);
$student_name = $lu_student->UserName();

$class_name = $lu_student->ClassName;
$class_num = $lu_student->ClassNumber;
if($class_name)
	$targetClass = $class_name;

/*
# get year
if ($year=="")
{
	$year = getCurrentAcademicYear();
}
$sql = "SELECT DISTINCT Year FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE WHERE Year != '' ORDER BY Year";
$years = $ldiscipline->returnVector($sql);
$select_year .= $linterface->CONVERT_TO_JS_ARRAY($years, "jArrayYears", 1);
$select_year .= $linterface->GET_PRESET_LIST("jArrayYears", 0, "year");	
	
# Get semester selection list
$yearID = $ldiscipline->getAcademicYearIDByYearName($year);
$sql = "SELECT YearTermID, YearTermNameEN, YearTermNameB5 FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID='$yearID' ORDER BY TermStart";
$semResult = $ldiscipline->returnArray($sql, 3);

$yearTermName = $ldiscipline->getTermNameByTermID($semester);

$select_sem = "<select name='semester' onChange=\"this.form.action='personal_check.php';this.form.submit()\">";
$select_sem .= "<option value=''";
$select_sem .= ($semester!='') ? "" : " selected";
$select_sem .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for($i=0; $i<sizeof($semResult); $i++) {
	list($id, $nameEN, $nameB5) = $semResult[$i];
	$semName = Get_Lang_Selection($nameB5, $nameEN);
	$select_sem .= "<option value='$id'";
	$select_sem .= ($semester==$id) ? " selected" : "";
	$select_sem .= ">$semName</option>";
}
$select_sem .= "</select>";
*/

# Date Range
$ts = time();
if ($startdate == "") {
    $startdate = date('Y-m-d',getStartOfAcademicYear($ts));
}
if ($enddate == "") {
    $enddate = date('Y-m-d',getEndOfAcademicYear($ts));
}

$label_year = "$i_Profile_Year ($year) ";
$label_semester = "$i_SettingsSemester ($yearTermName) ";

$additionConds = " AND a.ReleaseStatus = ".DISCIPLINE_STATUS_RELEASED;

# Get Data from DB
$date_count = $ldiscipline->retrieveStudentMeritTypeCountByDate($targetID,$startdate,$enddate, $additionConds);
$year_count = $ldiscipline->retrieveStudentMeritTypeByYearSemester($targetID, $yearID, "", $additionConds);
$sem_count = $ldiscipline->retrieveStudentMeritTypeByYearSemester($targetID, $yearID, $semester, $additionConds);

# Page
$CurrentPage = "Viewing_PersonalReport";
$CurrentPageArr['eServiceeDisciplinev12'] = 1;
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag Information
$TAGS_OBJ[] = array($iDiscipline['Reports_Child_PersonalReport']);

$linterface->LAYOUT_START();
?>

<br/>
<form name="form1" action="personal_check.php" method="GET">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td align=left>
		<? if($ClassList[$prev_index]) {?>
			<a href="javascript:nextRecord('<?=$ClassList[$prev_index]?>')" class="tablebottomlink" onMouseOver="MM_swapImage('prevp','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_on.gif',1)" onMouseOut="MM_swapImgRestore()"title="<?=$StudentName[$ClassList[$prev_index]]?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_off.gif" name="prevp" width="11" height="10" border="0" align="absmiddle" id="prevp"> <?=$i_Discipline_System_Previous_Student?></a>
		<? } ?>
		</td>
		<td align=right>
		<? if($ClassList[$next_index]) {?>
			<a href="javascript:nextRecord('<?=$ClassList[$next_index]?>')" class="tablebottomlink" onMouseOver="MM_swapImage('nextp','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_on.gif',1)" onMouseOut="MM_swapImgRestore()"title="<?=$StudentName[$ClassList[$next_index]]?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_off.gif" name="nextp" width="11" height="10" border="0" align="absmiddle" id="nextp"> <?=$i_Discipline_System_Next_Student?></a>
		<? } ?>
		</td>
	</tr>
</table>
<br><br>

<table border="0" cellpadding="2" cellspacing="0" width="90%">
<tr><td colspan=2 ><?=$ChildrenFilter?></td></tr>
<tr>
	<td class="tabletext formfieldtitle" valign="top" width="30%"><?=$i_identity_student?> </td>
	<td class="tabletext"><?=$targetClass?> - <?=$student_name?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" valign="top"><span class="tabletextrequire">*</span> <?=$i_general_startdate?></td>
	<td class="tabletext">
		<?= $linterface->GET_DATE_FIELD("theStartDate", "form1", "startdate", $startdate, 1)?>
	</td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" valign="top"><span class="tabletextrequire">*</span> <?=$i_general_enddate?></td>
	<td class="tabletext">
		<?= $linterface->GET_DATE_FIELD("theEndDate", "form1", "enddate", $enddate, 1)?>
	</td>
</tr>
<? /* ?>
<tr class="tabletext">
		<td width="30%" class="formfieldtitle" valign="top"><?=$i_Profile_Year?> <span class="tabletextrequire">*</span></td>
		<td><input type="text" name="year" maxlength="100" value="<?=$year?>" class="textboxnum"> <?=$select_year?></td>
	</tr>
	<tr class="tabletext">
		<td class="formfieldtitle" valign="top"><?=$i_SettingsSemester ?></td>
		<td><?=$select_sem?></td>
	</tr>
<? */ ?>	
</table>

<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_view, "button", "this.form.action='personal_check.php'; this.form.submit()");?>
		</td>
	</tr>
</table>
<br/>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr><td class="tabletext"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_section.gif" width="20" height="20" align="absmiddle">
	<span class="sectiontitle"><?= $iDiscipline['compare_different_periods']?></span>
</td></tr>
</table>

<table border="0" cellpadding="2" cellspacing="0" width="96%">
<tr><td>
  <!-- Merit Count --->
  <table width="96%" border="0" cellpadding="2" cellspacing="0">
    <tr class="tablebluetop tabletopnolink">
      <td>&nbsp;</td>
    <?
    if (!$lsp->is_ult_merit_disabled)
    {
         echo "<td>".$i_Merit_UltraCredit."</td>";
    }
    if (!$lsp->is_sup_merit_disabled)
    {
         echo "<td>".$i_Merit_SuperCredit."</td>";
    }
    if (!$lsp->is_maj_merit_disabled)
    {
         echo "<td>".$i_Merit_MajorCredit."</td>";
    }
    if (!$lsp->is_min_merit_disabled)
    {
         echo "<td>".$i_Merit_MinorCredit."</td>";
    }
    if (!$lsp->is_merit_disabled)
    {
         echo "<td>".$i_Merit_Merit."</td>";
    }
    if (!$lsp->is_ult_demer_disabled)
    {
         echo "<td>".$i_Merit_UltraDemerit."</td>";
    }
    if (!$lsp->is_sup_demer_disabled)
    {
         echo "<td>".$i_Merit_SuperDemerit."</td>";
    }
    if (!$lsp->is_maj_demer_disabled)
    {
         echo "<td>".$i_Merit_MajorDemerit."</td>";
    }
    if (!$lsp->is_min_demer_disabled)
    {
         echo "<td>".$i_Merit_MinorDemerit."</td>";
    }
    if (!$lsp->is_black_disabled)
    {
         echo "<td>".$i_Merit_BlackMark."</td>";
    }
    if (!$lsp->is_warning_disabled)
    {
         echo "<td>".$i_Merit_Warning."</td>";
    }
    ?>
    </tr>
    <tr class="tabletext tablebluerow1">
      <td class="tabletext formfieldtitle" width="30%" valign="top"><?=$i_Discipline_System_Report_DateSelected?></td>
    <?
    if (!$lsp->is_ult_merit_disabled)
    {
         echo "<td>".($date_count['5']+0)."</td>";
    }
    if (!$lsp->is_sup_merit_disabled)
    {
         echo "<td>".($date_count['4']+0)."</td>";
    }
    if (!$lsp->is_maj_merit_disabled)
    {
         echo "<td>".($date_count['3']+0)."</td>";
    }
    if (!$lsp->is_min_merit_disabled)
    {
         echo "<td>".($date_count['2']+0)."</td>";
    }
    if (!$lsp->is_merit_disabled)
    {
         echo "<td>".($date_count['1']+0)."</td>";
    }
    if (!$lsp->is_ult_demer_disabled)
    {
         echo "<td>".($date_count['-5']+0)."</td>";
    }
    if (!$lsp->is_sup_demer_disabled)
    {
         echo "<td>".($date_count['-4']+0)."</td>";
    }
    if (!$lsp->is_maj_demer_disabled)
    {
         echo "<td>".($date_count['-3']+0)."</td>";
    }
    if (!$lsp->is_min_demer_disabled)
    {
         echo "<td>".($date_count['-2']+0)."</td>";
    }
    if (!$lsp->is_black_disabled)
    {
         echo "<td>".($date_count['-1']+0)."</td>";
    }
    if (!$lsp->is_warning_disabled)
    {
         echo "<td>".($date_count['0']+0)."</td>";
    }
    ?>
    </tr>
    <? /* ?>
    <tr class="tabletext">
      <td class="tabletext formfieldtitle" width="30%" valign="top"><?=$label_year?></td>
          <?
    if (!$lsp->is_ult_merit_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['5']+0)."</td>";
    }
    if (!$lsp->is_sup_merit_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['4']+0)."</td>";
    }
    if (!$lsp->is_maj_merit_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['3']+0)."</td>";
    }
    if (!$lsp->is_min_merit_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['2']+0)."</td>";
    }
    if (!$lsp->is_merit_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['1']+0)."</td>";
    }
    if (!$lsp->is_ult_demer_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['-5']+0)."</td>";
    }
    if (!$lsp->is_sup_demer_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['-4']+0)."</td>";
    }
    if (!$lsp->is_maj_demer_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['-3']+0)."</td>";
    }
    if (!$lsp->is_min_demer_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['-2']+0)."</td>";
    }
    if (!$lsp->is_black_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['-1']+0)."</td>";
    }
    if (!$lsp->is_warning_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['0']+0)."</td>";
    }
    ?>
    </tr>
<?php if($semester!=""){?>
    <tr class="tabletext tablebluerow1">
      <td class="tabletext formfieldtitle" width="30%" valign="top"><?=$label_semester?></td>
           <?
    if (!$lsp->is_ult_merit_disabled)
    {
         echo "<td>".($sem_count['5']+0)."</td>";
    }
    if (!$lsp->is_sup_merit_disabled)
    {
         echo "<td>".($sem_count['4']+0)."</td>";
    }
    if (!$lsp->is_maj_merit_disabled)
    {
         echo "<td>".($sem_count['3']+0)."</td>";
    }
    if (!$lsp->is_min_merit_disabled)
    {
         echo "<td>".($sem_count['2']+0)."</td>";
    }
    if (!$lsp->is_merit_disabled)
    {
         echo "<td>".($sem_count['1']+0)."</td>";
    }
    if (!$lsp->is_ult_demer_disabled)
    {
         echo "<td>".($sem_count['-5']+0)."</td>";
    }
    if (!$lsp->is_sup_demer_disabled)
    {
         echo "<td>".($sem_count['-4']+0)."</td>";
    }
    if (!$lsp->is_maj_demer_disabled)
    {
         echo "<td>".($sem_count['-3']+0)."</td>";
    }
    if (!$lsp->is_min_demer_disabled)
    {
         echo "<td>".($sem_count['-2']+0)."</td>";
    }
    if (!$lsp->is_black_disabled)
    {
         echo "<td>".($sem_count['-1']+0)."</td>";
    }
    if (!$lsp->is_warning_disabled)
    {
         echo "<td>".($sem_count['0']+0)."</td>";
    }
    ?>
    </tr>
<?php } ?>
<? */ ?>
  </table>
</td></tr>
</table>

<br>
<?
if (!isset($detailType)) {
     $detailType = 4;
}
if(!($detailType == 6 || $detailType == 7))
{
	if($show_waive_status==1){
		$show_waive_status_link = "<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" name=\"statusp\"  border=\"0\" align=\"absmiddle\" id=\"statusp\"><a href='javascript:showWaiveStatus(0)'  class='tablebottomlink'>$i_Discipline_System_No_WaiveStatus</a>";
	}
	else{
		$show_waive_status_link = "<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" name=\"statusp\"  border=\"0\" align=\"absmiddle\" id=\"statusp\"><a href='javascript:showWaiveStatus(1)'  class='tablebottomlink'>$i_Discipline_System_Show_WaiveStatus</a>";
	}
}

$detail_table = "";

$edit_image= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" alt=\"$button_edit\" border=\"0\">";
$delete_image="<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" alt=\"$button_remove\" border=\"0\">";

$show_remark_in_personal_report = !$sys_custom['Discipline_HideRemarkInPersonalReport'];
switch ($detailType)
{
        case 1:
             $string_type["0"] = $i_Merit_Warning;
             $string_type["-1"] = $i_Merit_BlackMark;
             $string_type["-2"] = $i_Merit_MinorDemerit;
             $string_type["-3"] = $i_Merit_MajorDemerit;
             $string_type["-4"] = $i_Merit_SuperDemerit;
             $string_type["-5"] = $i_Merit_UltraDemerit;

             # get list of merit record id that is upgraded from ACCU punishment
             $sql="SELECT DISTINCT UpgradedRecordID FROM DISCIPLINE_ACCU_RECORD WHERE UpgradedRecordID>0";
             $upgraded_list = $ldiscipline->returnVector($sql);

             # Demerit
           	 $result = $ldiscipline->retrieveStudentDemeritRecord($targetID, $startdate, $enddate,$yearID,$semester,$show_waive_status, $additionConds);

             $detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
             $detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletopnolink\" width=\"5\">#</td>
                                 <td class=\"tabletopnolink\" width=\"80\">$i_general_record_date</td>
                                 <td class=\"tabletopnolink\">$i_Discipline_System_general_record</td>
                                 <td class=\"tabletopnolink\" width=\"80\">$i_Discipline_System_Add_Demerit</td>";
             
			 //if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'] && !$ldiscipline->Hidden_ConductMark)
             if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'])
				$detail_table .= "<td class=\"tabletopnolink\" width=\"60\">$i_Discipline_System_Conduct_ScoreDifference</td>";
				
			 $detail_table .= ($ldiscipline->UseSubScore || $ldiscipline->use_sub_score1) ? "<td class=\"tabletopnolink\">$i_Discipline_System_Subscore1_ScoreDifference</td>" : "";
			 $detail_table .= ($ldiscipline->use_subject) ? "<td class=\"tabletopnolink\" width=\"10%\">$i_Subject_name</td>" : "";
             $detail_table .= "<td class=\"tabletopnolink\" width=\"10%\">$i_Profile_PersonInCharge</td>";
             $detail_table .= $show_waive_status? "<td class=\"tabletopnolink\" width=\"60\">$i_Discipline_System_WaiveStatus</td>":"";
             
             # Yat Woon, customization for fyk.edu.hk 20080424
             $detail_table .= $show_remark_in_personal_report ? "<td class=\"tabletopnolink\" width=\"150\">$i_Discipline_System_general_remark</td>":"";

             $detail_table .="</tr>";

             for ($i=0; $i<sizeof($result); $i++)
             {
                  list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count,
                        $r_conduct, $r_conduct_after, $t_subscore1, $t_subscore1_after, $t_subject, 
                        $r_picid, $r_pic_name,$record_status,$remark) = $result[$i];
                  $css = ($i%2?"1":"2");

				  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
          
				  if($r_profile_count!=0) {
						$displayMerit = ($intranet_session_language=="en") ? $r_profile_count." ".$string_type[$r_profile_type] : $string_type[$r_profile_type].$r_profile_count.$Lang['eDiscipline']['Times'];
						$record_data = $ldiscipline->TrimDeMeritNumber_00($displayMerit);
				  }
				  else {
						//$record_data = "(".$i_general_na.")";
						$record_data = "--";
				  }
                  $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
                                     <td>$r_date</td>
                                     <td>". intranet_htmlspecialchars($r_item) ."</td>
                                     <td>$record_data</td>";
                  
               	  //if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'] && !$ldiscipline->Hidden_ConductMark)
                  if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'])
						$detail_table .= "<td>$r_conduct</td>";
                  $detail_table .= ($ldiscipline->UseSubScore || $ldiscipline->use_sub_score1) ? "<td>$t_subscore1</td>"  : "";
                  $detail_table .= ($ldiscipline->use_subject) ? "<td>$t_subject</td>" : "";
                  $detail_table .= "<td>".($r_pic_name==""?"--":$r_pic_name)."</td>";
                  if($show_waive_status==1)
                  		$detail_table .="<Td>$str_waive_status</td>";

                  if($show_remark_in_personal_report)
                  		$detail_table .="<Td>". nl2br($remark) ."</td>";

                  $detail_table.="</tr>";
             }
             if (sizeof($result) == 0)
             	$detail_table .= "<tr><td class='tabletext' colspan='7' align='center'>{$i_no_record_exists_msg}</td></tr>";
             $detail_table .= "</table>\n";

             break;
        case 2:
             $string_type["1"] = $i_Merit_Merit;
             $string_type["2"] = $i_Merit_MinorCredit;
             $string_type["3"] = $i_Merit_MajorCredit;
             $string_type["4"] = $i_Merit_SuperCredit;
             $string_type["5"] = $i_Merit_UltraCredit;

             # Merit
             $result = $ldiscipline->retrieveStudentMeritRecord($targetID, $startdate, $enddate,$yearID,$semester,$show_waive_status, $additionConds);
             $detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
             $detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletopnolink\" width=\"5\">#</td>
                                 <td class=\"tabletopnolink\" width=\"80\">$i_general_record_date</td>
                                 <td class=\"tabletopnolink\">$i_Discipline_System_general_record</td>
                                 <td class=\"tabletopnolink\" width=\"80\">$i_Discipline_System_Add_Merit</td>";
            
            //if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'] && !$ldiscipline->Hidden_ConductMark)
            if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'])
				$detail_table .= "<td class=\"tabletopnolink\" width=\"60\">$i_Discipline_System_Conduct_ScoreDifference</td>";
			$detail_table .= ($ldiscipline->UseSubScore || $ldiscipline->use_sub_score1) ? "<td class=\"tabletopnolink\">$i_Discipline_System_Subscore1_ScoreDifference</td>" : "";
            $detail_table .= "<td class=\"tabletopnolink\" width=\"10%\">$i_Profile_PersonInCharge</td>";
            $detail_table .= $show_waive_status? "<td class=\"tabletopnolink\" width=\"60\">$i_Discipline_System_WaiveStatus</td>":"";
            
            # Yat Woon, customization for fyk.edu.hk 20080424
             $detail_table .= $show_remark_in_personal_report? "<td class=\"tabletopnolink\" width=\"150\">$i_Discipline_System_general_remark</td>":"";
            
             $detail_table .= "</tr>";
              
             for ($i=0; $i<sizeof($result); $i++)
             {
                  list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count,
                        $r_conduct, $r_conduct_after, $t_subscore1, $t_subscore1_after, $r_picid, 
                        $r_pic_name, $remark, $record_status) = $result[$i];
                  $css = ($i%2?"2":"");
                  
                  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
                  
                  if($r_profile_count!=0) {
	                  $displayMerit = ($intranet_session_language=="en") ? $r_profile_count." ".$string_type[$r_profile_type] : $string_type[$r_profile_type].$r_profile_count.$Lang['eDiscipline']['Times'];
						$record_data = $ldiscipline->TrimDeMeritNumber_00($displayMerit);
					} else {
						//$record_data = "(".$i_general_na.")";
						$record_data = "--";
					}
						
                  $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
                                     <td>$r_date</td>
                                     <td>". intranet_htmlspecialchars($r_item) ."</td>
                                     <td>$record_data</td>";
                
				//if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'] && !$ldiscipline->Hidden_ConductMark)
            	if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'])
                		$detail_table .= "<td>$r_conduct</td>";
                  $detail_table .= ($ldiscipline->UseSubScore || $ldiscipline->use_sub_score1) ? "<td>$t_subscore1</td>" : "";
                  $detail_table .= "<td>".($r_pic_name==""?"--":$r_pic_name)."</td>";
                  if($show_waive_status==1)		
                  		$detail_table .="<Td>$str_waive_status</td>";
                  if($show_remark_in_personal_report)
                  		$detail_table .="<Td>". nl2br($remark) ."</td>";
                  		
                  $detail_table .= "</tr>";
             }
             if (sizeof($result) == 0)
             	$detail_table .= "<tr><td class='tabletext' colspan='7' align='center'>{$i_no_record_exists_msg}</td></tr>";
             $detail_table .= "</table>\n";
             break;
         case 4:
                         $string_type["0"] = $i_Merit_Warning;
			             $string_type["-1"] = $i_Merit_BlackMark;
			             $string_type["-2"] = $i_Merit_MinorDemerit;
			             $string_type["-3"] = $i_Merit_MajorDemerit;
			             $string_type["-4"] = $i_Merit_SuperDemerit;
			             $string_type["-5"] = $i_Merit_UltraDemerit;
			             $string_type["1"] = $i_Merit_Merit;
			             $string_type["2"] = $i_Merit_MinorCredit;
			             $string_type["3"] = $i_Merit_MajorCredit;
			             $string_type["4"] = $i_Merit_SuperCredit;
			             $string_type["5"] = $i_Merit_UltraCredit;

             # get list of merit record id that is upgraded from ACCU punishment
             $sql="SELECT DISTINCT UpgradedRecordID FROM DISCIPLINE_ACCU_RECORD WHERE UpgradedRecordID>0";
             $upgraded_list = $ldiscipline->returnVector($sql);

             # Merit / Demerit
                          
             $result = $ldiscipline->retrieveStudentProfileRecord($targetID, $startdate, $enddate, $yearID, $semester, $show_waive_status, $additionConds);

             $detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
             $detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletopnolink\">#</td>
                                 <td class=\"tabletopnolink\">$i_general_record_date</td>";
			if($sys_custom['Discipline_ShowCategoryInPersonalReport'])                                 
				$detail_table .= "<td class=\"tabletopnolink\">$i_Discipline_System_CategoryName</td>";
			$detail_table .= "	<td class=\"tabletopnolink\">$i_Discipline_System_general_record</td>
                                 <td class=\"tabletopnolink\">$i_Discipline_System_Add_Merit_Demerit</td>";

			//if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'] && !$ldiscipline->Hidden_ConductMark)
            if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'])
            	$detail_table .= "<td class=\"tabletopnolink\">$i_Discipline_System_Conduct_ScoreDifference</td>";
            $detail_table .= ($ldiscipline->UseSubScore || $ldiscipline->use_sub_score1) ? "<td class=\"tabletopnolink\">$i_Discipline_System_Subscore1_ScoreDifference</td>" : "";
            $detail_table .= ($ldiscipline->use_subject) ? "<td class=\"tabletopnolink\">$i_Subject_name</td>" : "";
            $detail_table .= "<td class=\"tabletopnolink\">$i_Profile_PersonInCharge</td>";
            $detail_table .=$show_waive_status? "<td class=\"tabletopnolink\">$i_Discipline_System_WaiveStatus</td>":"";
            
            # Yat Woon, customization for fyk.edu.hk 20080424
             $detail_table .= $show_remark_in_personal_report? "<td class=\"tabletopnolink\">$i_Discipline_System_general_remark</td>":"";
            
            $detail_table.="</tr>";
             for ($i=0; $i<sizeof($result); $i++)
             {
                  list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count,
                        $r_conduct, $r_conduct_after, $t_subscore1, $t_subscore1_after, $t_subject, 
                        $r_picid, $r_pic_name,$record_status, $MeritType, $remark, $cat_name) = $result[$i];
					$cat_name = intranet_htmlspecialchars($cat_name);
					$r_item = intranet_htmlspecialchars($r_item);
					$remark = intranet_htmlspecialchars($remark);
					
                  $css = ($i%2?"2":"1");

                  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
                  $css = $record_status==DISCIPLINE_STATUS_WAIVED ? 3 : $css;

                  if($r_profile_count!=0) {
	                  $displayMerit = ($intranet_session_language=="en") ? $r_profile_count." ".$string_type[$r_profile_type] : $string_type[$r_profile_type].$r_profile_count.$Lang['eDiscipline']['Times'];
                  	$record_data = $ldiscipline->TrimDeMeritNumber_00($displayMerit);
              		} else {
                  		//$record_data = "(".$i_general_na.")";
                  		$record_data = "--";
              		}
                  $detail_table .= "<tr><td class=\"tablegreenrow$css tabletext\">". ($i+1)."</td>
                                     <td class=\"tablegreenrow$css tabletext\">$r_date</td>";
                  if($sys_custom['Discipline_ShowCategoryInPersonalReport'])
						$detail_table .= "<td class=\"tablegreenrow$css tabletext\">$cat_name</td>";
                  $detail_table .= "<td class=\"tablegreenrow$css tabletext\">$r_item</td>
                                     <td class=\"tablegreenrow$css tabletext\">$record_data</td>";
                  
                  //if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'] && !$ldiscipline->Hidden_ConductMark)
            	  if(!$sys_custom['Discipline_HiddenConductMarkInPersonalReport'])                   
                  	$detail_table .= "<td class=\"tablegreenrow$css tabletext\">$r_conduct</td>";
                  $detail_table .= ($ldiscipline->UseSubScore || $ldiscipline->use_sub_score1) ? "<td class=\"tablegreenrow$css tabletext\">$t_subscore1</td>" : "";
                  $detail_table .= ($ldiscipline->use_subject) ? "<td class=\"tablegreenrow$css tabletext\">$t_subject&nbsp;</td>" : "";
                  $detail_table .= "<td class=\"tablegreenrow$css tabletext\">".($r_pic_name==""?"--":$r_pic_name)."</td>";
                  if($show_waive_status==1)
                	$detail_table .="<Td class=\"tablegreenrow$css tabletext\">$str_waive_status</td>";
                	if($show_remark_in_personal_report)
                  		$detail_table .="<td class=\"tablegreenrow$css tabletext\">". nl2br($remark) ."</td>";
                  		
                   $detail_table.= "</tr>";

             }
             if (sizeof($result) == 0)
             	$detail_table .= "<tr><td class='tabletext' colspan='7' align='center'>{$i_no_record_exists_msg}</td></tr>";
             $detail_table .= "</table>\n";
             break;
       case 6:
             		$result = $ldiscipline->retrieveConductScoreChangeLogRecord($targetID,$startdate,$enddate,$yearID,$semester);
  		            $detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
             		$detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletopnolink\">#</td>
                                 <td class=\"tabletopnolink\">$i_general_record_date</td>
                                 <td class=\"tabletopnolink\">$i_Merit_Type</td>
                                 <td class=\"tabletopnolink\">$i_Discipline_System_general_record</td>
                                 <td class=\"tabletopnolink\">$i_Discipline_System_FromScore</td>
                                 <td class=\"tabletopnolink\">$i_Discipline_System_ToScore</td>
            					 <td class=\"tabletopnolink\">$i_Profile_PersonInCharge</td>";
            		# Yat Woon, customization for fyk.edu.hk 20080424
             $detail_table .= $show_remark_in_personal_report? "<td class=\"tabletopnolink\">$i_Discipline_System_general_remark</td>":"";
             			 
                   $detail_table .= "</tr>";
             for ($i=0; $i<sizeof($result); $i++)
             {
                  list ($r_date, $r_action_type, $r_item, $r_fromscore, $r_toscore,$r_pic_name, $remark) = $result[$i];
                  $css = ($i%2?"2":"1");

                  switch($r_action_type){
	                  case 1 : $action = $i_Discipline_System_New_MeritDemerit; break;
	                  case 2 : $action = $i_Discipline_System_Edit_MeritDemerit; break;
	                  case 3 : $action = $i_Discipline_System_Remove_MeritDemerit; break;
	                  case 4 : $action = $i_Discipline_System_Manual_Adjustment; break;
	                  case 5 : $action = $i_Discipline_System_System_Revised; break;
	              }
	              $r_item = intranet_htmlspecialchars($r_item);
	              if($r_item =="--" && $r_action_type!=5){
		              $r_item = "<i>$i_Discipline_System_Record_Already_Removed</i>";
		          }
                  $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
                                     <td>$r_date</td>
                                     <td>$action</td>
                                     <td>$r_item</td>
                                     <td>$r_fromscore</td>
                                     <td>$r_toscore</td>
                  					 <td>".($r_pic_name==""?"--":$r_pic_name)."</td>";
				if($show_remark_in_personal_report)
                  		$detail_table .="<Td>". nl2br($remark) ."</td>";
                  $detail_table .="</tr>";

             }
             if (sizeof($result) == 0)
             	$detail_table .= "<tr><td class='tabletext' colspan='7' align='center'>{$i_no_record_exists_msg}</td></tr>";
             $detail_table .= "</table>\n";

             		break;
      case 7:
                    $result = $ldiscipline->retrieveSubscore1ChangeLogRecord($targetID,$startdate,$enddate,$year,$semester);
  		            $detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
             		$detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletopnolink\">#</td>
                                 <td class=\"tabletopnolink\">$i_general_record_date</td>
                                 <td class=\"tabletopnolink\">$i_Merit_Type</td>
                                 <td class=\"tabletopnolink\">$i_Discipline_System_general_record</td>
                                 <td class=\"tabletopnolink\">$i_Discipline_System_FromScore</td>
                                 <td class=\"tabletopnolink\">$i_Discipline_System_ToScore</td>
            					 <td class=\"tabletopnolink\">$i_Profile_PersonInCharge</td>";
            		# Yat Woon, customization for fyk.edu.hk 20080424
             		$detail_table .= $show_remark_in_personal_report? "<td class=\"tabletopnolink\">$i_Discipline_System_general_remark</td>":"";
                    $detail_table .= "</tr>";
             for ($i=0; $i<sizeof($result); $i++)
             {
                  list ($r_date, $r_action_type, $r_item, $r_fromscore, $r_toscore,$r_pic_name, $remark) = $result[$i];
                  $css = ($i%2?"2":"1");

                  switch($r_action_type){
	                  case 1 : $action = $i_Discipline_System_New_MeritDemerit; break;
	                  case 2 : $action = $i_Discipline_System_Edit_MeritDemerit; break;
	                  case 3 : $action = $i_Discipline_System_Remove_MeritDemerit; break;
	                  case 4 : $action = $i_Discipline_System_Manual_Adjustment; break;
	              }
	              $r_item = intranet_htmlspecialchars($r_item);
	              if($r_item =="--"){
		              $r_item = "<i>$i_Discipline_System_Record_Already_Removed</i>";
		          }
                  $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
                                     <td>$r_date</td>
                                     <td>$action</td>
                                     <td>$r_item</td>
                                     <td>$r_fromscore</td>
                                     <td>$r_toscore</td>
                  					 <td>".($r_pic_name==""?"--":$r_pic_name)."</td>";
				 if($show_remark_in_personal_report)
                  		$detail_table .="<Td>". nl2br($remark) ."</td>";
                $detail_table .= "</tr>";

             }
             if (sizeof($result) == 0)
             	$detail_table .= "<tr><td class='tabletext' colspan='7' align='center'>{$i_no_record_exists_msg}</td></tr>";
             $detail_table .= "</table>\n";


             	break;

        case 8:
				$detail_table="";
				$detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
				$detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletopnolink\" width=\"5\">#</td>
				                     <td class=\"tabletopnolink\" width=\"80\">$i_general_record_date</td>
				                     <td class=\"tabletopnolink\" width=\"30%\">".$iDiscipline['Accumulative_Category_Name']."</td>
				                     <td class=\"tabletopnolink\" width=\"30%\">".$iDiscipline['Accumulative_Category_Item_Name']."</td>
				                     <td class=\"tabletopnolink\" width=\"10%\">$i_Profile_PersonInCharge</td>
				                     <td class=\"tabletopnolink\" width=\"150\">$i_Discipline_System_general_remark</td>";
				$detail_table .=$show_waive_status? "<td class=\"tabletopnolink\">$i_Discipline_System_WaiveStatus</td>":"";
				 $detail_table .="</tr>";


				$result = $ldiscipline->retrieveStudentConductRecord($targetID,$startdate,$enddate,$yearID,$semester,"-1",$show_waive_status);
				
				for ($i=0; $i<sizeof($result); $i++)
				{
				  list ($record_id,$record_date,$cat_name,$item_name,$remark,$upgrade_record_id,$record_status,$r_pic_name)= $result[$i];
				  $css = ($i%2?"2":"1");
				  
				  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
				 				  
				      $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
				                         <td>$record_date</td>
				                         <td>$cat_name</td>
				                         <Td>$item_name</td>
				                         <td>".($r_pic_name==""?"--":$r_pic_name)."</td>
				                         <Td>". intranet_htmlspecialchars($remark) ."</td>
				                        ";
				      if($show_waive_status==1)
				      	$detail_table .="<Td>$str_waive_status</td>";

				      $detail_table.="</tr>";

				}
				$cols = $show_waive_status==1?8:7;
				if (sizeof($result) == 0)
					$detail_table .= "<tr><td class='tabletext' colspan='$cols' align='center'>{$i_no_record_exists_msg}</td></tr>";
				$detail_table .= "</table>\n";

             break;
		case 9:
				$detail_table="";
				$detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
				$detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletopnolink\" width=\"5\">#</td>
				                     <td class=\"tabletopnolink\" width=\"80\">$i_general_record_date</td>
				                     <td class=\"tabletopnolink\" width=\"30%\">".$iDiscipline['Accumulative_Category_Name']."</td>
				                     <td class=\"tabletopnolink\" width=\"30%\">".$iDiscipline['Accumulative_Category_Item_Name']."</td>
				                     <td class=\"tabletopnolink\" width=\"10%\">$i_Profile_PersonInCharge</td>
				                     <td class=\"tabletopnolink\" width=\"150\">$i_Discipline_System_general_remark</td>";
				$detail_table .=$show_waive_status? "<td class=\"tabletopnolink\">$i_Discipline_System_WaiveStatus</td>":"";
				 $detail_table .="</tr>";

				$result = $ldiscipline->retrieveStudentConductRecord($targetID,$startdate,$enddate,$yearID,$semester,"1",$show_waive_status);
				
				for ($i=0; $i<sizeof($result); $i++)
				{
				  list ($record_id,$record_date,$cat_name,$item_name,$remark,$upgrade_record_id,$record_status,$r_pic_name)= $result[$i];
				  $css = ($i%2?"2":"1");

				  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
				  
				      $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
				                         <td>$record_date</td>
				                         <td>$cat_name</td>
				                         <Td>$item_name</td>
				                         <Td>$r_pic_name</td>
				                         <Td>". intranet_htmlspecialchars($remark) ."</td>
				                        ";
				      if($show_waive_status==1)
				      	$detail_table .="<Td>$str_waive_status</td>";

				      $detail_table.="</tr>";

				}
				$cols = $show_waive_status==1?8:7;
				if (sizeof($result) == 0)
					$detail_table .= "<tr><td class='tabletext' colspan='$cols' align='center'>{$i_no_record_exists_msg}</td></tr>";
				$detail_table .= "</table>\n";
             break;
		case 10:
				$detail_table="";
				$detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
				$detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletopnolink\" width=\"5\">#</td>
				                     <td class=\"tabletopnolink\" width=\"80\">$i_general_record_date</td>
				                     <td class=\"tabletopnolink\" width=\"30%\">".$iDiscipline['Accumulative_Category_Name']."</td>
				                     <td class=\"tabletopnolink\" width=\"30%\">".$iDiscipline['Accumulative_Category_Item_Name']."</td>
				                     <td class=\"tabletopnolink\" width=\"10%\">$i_Profile_PersonInCharge</td>
				                     <td class=\"tabletopnolink\" width=\"150\">$i_Discipline_System_general_remark</td>";
				$detail_table .=$show_waive_status? "<td class=\"tabletopnolink\">$i_Discipline_System_WaiveStatus</td>":"";
				 $detail_table .="</tr>";


				$result = $ldiscipline->retrieveStudentConductRecord($targetID,$startdate,$enddate,$yearID,$semester,"",$show_waive_status);
				
				for ($i=0; $i<sizeof($result); $i++)
				{
				  list ($record_id,$record_date,$cat_name,$item_name,$remark,$upgrade_record_id,$record_status,$r_pic_name)= $result[$i];
				  $css = ($i%2?"2":"1");
				  
				  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
				 				  
				      $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
				                         <td>$record_date</td>
				                         <td>$cat_name</td>
				                         <Td>$item_name</td>
				                         <td>".($r_pic_name==""?"--":$r_pic_name)."</td>
				                         <Td>". intranet_htmlspecialchars($remark) ."</td>
				                        ";
				      if($show_waive_status==1)
				      	$detail_table .="<Td>$str_waive_status</td>";

				      $detail_table.="</tr>";

				}
				$cols = $show_waive_status==1?8:7;
				if (sizeof($result) == 0)
					$detail_table .= "<tr><td class='tabletext' colspan='$cols' align='center'>{$i_no_record_exists_msg}</td></tr>";
				$detail_table .= "</table>\n";

             break;
             


}
?>

<SCRIPT language=Javascript>
function confirmRemoveRecord(detailType, id, targetID)
{
	if (confirm('<?=$i_Usage_RemoveConfirm?>'))
	{
    	if (detailType==1 || detailType==2 || detailType == 4)
        {
        	location.href = 'personal_check_merit_remove.php?RecordID='+id+'&targetID='+targetID+'&detailType='+detailType+'&show_waive_status='+document.form1.show_waive_status.value;
        }
        else if (detailType==3)
        {
        	location.href = 'personal_check_detention_remove.php?RecordID='+id+'&targetID='+targetID+'&detailType='+detailType;
        }
	}
}

function confirmRemoveAccumulativeRecord(detailType,id, targetID)
{
    if (confirm('<?=$i_Usage_RemoveConfirm?>'))
    {
    	location.href = 'accumulative/accu_personal_check_remove.php?RecordID='+id+'&targetID='+targetID+'&detailType='+detailType+'&show_waive_status='+document.form1.show_waive_status.value;
    }
}

function exportRecord()
{
    form = document.form1;
    form.action = "personal_check_export.php";
    form.submit();
}

function printRecord()
{
    document.form1.action = "personal_check_print.php";
    document.form1.target = "_blank";
    document.form1.submit();
    document.form1.target = "_self";
    document.form1.action = "";
}

function nextRecord(id)
{
    form = document.form1;
    form.targetID.value = id;
    form.action = '';
    form.submit();
}

function showWaiveStatus(v){
	obj = document.form1.show_waive_status;
	document.form1.action="personal_check.php";
	obj.value=v;
	document.form1.submit();
}
</SCRIPT>

<table border="0" cellpadding="2" cellspacing="0" width="96%">
<tr><td>
<SELECT name="detailType" onChange="this.form.action='personal_check.php'; this.form.submit()">
<OPTION value="1" <?=($detailType==1?"SELECTED":"")?>><?=$eDiscipline["PunishmentRecord"]?></OPTION>
<OPTION value="2" <?=($detailType==2?"SELECTED":"")?>><?=$eDiscipline["AwardRecord"]?></OPTION>
<OPTION value="4" <?=($detailType==4?"SELECTED":"")?>><?=$eDiscipline["Personal_Report_AwardPunishmentRecord"]?></OPTION>
<? if(!$sys_custom['eDiscipline']['HideAllGMReport']) { ?>
	<OPTION value="8" <?=($detailType==8?"SELECTED":"")?>><?=$eDiscipline['Personal_Report_Misconduct_Record']?></OPTION>
	<OPTION value="9" <?=($detailType==9?"SELECTED":"")?>><?=$eDiscipline['Personal_Report_Goodconduct_Record']?></OPTION>
	<OPTION value="10" <?=($detailType==10?"SELECTED":"")?>><?=$eDiscipline['Personal_Report_Good_Misconduct_Record']?></OPTION>
<? } ?>
<? /* hidden in IP25
<OPTION value="6" <?=($detailType==6?"SELECTED":"")?>><?=$i_Discipline_System_Conduct_Change_Log?></OPTION>
*/ ?>

<?php
if($ldiscipline->UseSubScore || $ldiscipline->use_sub_score1){?>
	<OPTION value="7" <?=($detailType==7?"SELECTED":"")?>><?=$i_Discipline_System_SubScore1_Change_Log?></OPTION>
<?php } ?>
</SELECT>

&nbsp;
<?=$show_waive_status_link?>
</td>
<td></td>
</tr>
</table>
<table border="0" cellpadding="2" cellspacing="0" width="96%">
<tr><td>
<?=$detail_table?>
</td></tr>
</table>

<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_export, "button", "javascript:exportRecord()") ?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_print, "button", "javascript:printRecord()") ?>
		</td>
	</tr>
</table>

<input type='hidden' name='show_waive_status' value='<?=$show_waive_status?>'>
<input type='hidden' name='targetID' value='<?=$targetID?>'>
</form>

<?
$linterface->LAYOUT_STOP();
?>