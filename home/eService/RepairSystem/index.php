<?php
//Modifying by: 

############ Change Log Start ###############
#
#   Date    :   2020-10-28 (Cameron)
#               support showing multiple follow-up person of a repair request record
#
#   Date    :   2020-09-29 (Cameron)
#               change $xmsg to $returnMsgKey for consitency
#
#   Date    :   2020-07-02 (Cameron)
#               Fix: show followup person
#
#	Date	:	2017-10-11  (Cameron)
#				Fix: use left join INTRANET_USER to include deleted user (case #N128259)
#
#	Date	:	2017-05-31  (Cameron)
#				Fix: support special character search like '"<>&	
#
#	Date	:	2016-12-02 (Cameron)
#				Add customized flag $sys_custom['RepairSystem']['GrantedUserIDArr'] to hardcode user for accessing eService -> Repair System
#
#	Date	:	2016-11-11 (Cameron)
#				Add customized flag $sys_custom['RepairSystemBanAccessForStudent'] to ban access for student
#
#	Date	:	2015-03-17	(Omas)
#				Improvement : Search can search by user name 
#
#	Date	:	2015-02-12	(Omas)
#				Improved : Add new field FollowUpPerson
#
#	Date	:	2014-11-06	(Omas)
#				Improved : Add new column Case Number, new status:Pending, able to search by CaseNumber
#
#	Date	:	2011-03-28 (Henry Chow)
#				Improved : select "Location" from "School Setting > Campus"
#
#	Date	:	2011-01-03	YatWoon
#				add rejected status
#
#	Date	:	2010-11-23	YatWoon
#				- IP25 UI standard
#				- add filter "own" / "all"
#				- add search
#
############ Change Log End ###############



$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

/*
# implement later
if (!$plugin['RepairSystem'] || ... )
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-RepairSystem"])
	{
		header("location: ./settings/basic_settings/");	
		exit;
	}
	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
*/
if ( !isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || $_SESSION['UserType'] == USERTYPE_ALUMNI || ($_SESSION['UserType'] == USERTYPE_PARENT && !$sys_custom['RepairSystemAccessForParent']) || ($_SESSION['UserType'] == USERTYPE_STUDENT && $sys_custom['RepairSystemBanAccessForStudent']) || (isset($sys_custom['RepairSystem']['GrantedUserIDArr']) && !in_array($_SESSION['UserID'],(array)$sys_custom['RepairSystem']['GrantedUserIDArr'])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

$arrCookies[] = array("ck_request_summary_keyword", "keyword");
$arrCookies[] = array("ck_request_summary_Status", "Status");
$arrCookies[] = array("ck_request_summary_catid", "CatID");
$arrCookies[] = array("ck_request_summary_display_own", "display_own");
$arrCookies[] = array("ck_request_summary_year", "year");
$arrCookies[] = array("ck_request_summary_month", "month");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	$order = $ck_right_page_order;
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 1 : $field;

intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$CurrentPageArr['eServiceRepairSystem'] = 1;
$CurrentPage = "PageList";
$lrepairsystem = new librepairsystem();

$linterface = new interface_html();

# TABLE INFO
if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);

# Add button
$AddBtn = "<a href=\"new.php?CatID=$CatID&Status=$Status&year=$year&month=$month\" class='new'>" . $button_new . "</a>";

## select Year
$currentyear = date('Y');
$array_year = $lrepairsystem->retrieveRecordYears();
if(empty($array_year))	$array_year[] = $currentyear;

$firstname = $Lang['RepairSystem']['AllYears'];
$select_year = getSelectByValueDiffName($array_year,$array_year,"name='year' id='year' onChange='document.form1.submit();'",$year,1,0, $firstname) . "&nbsp;";

## select Month
for ($i=1; $i<=12; $i++)
{
     $array_month[] = $i;
}
$firstname = $Lang['RepairSystem']['AllMonths'];
$select_month = getSelectByValueDiffName($array_month,$array_month,"name='month' id='month' onChange='document.form1.submit();'",$month,1,0,$firstname) . "&nbsp;";

$archiveCheckBox = "<input type='checkbox' name='withArchive' id='withArchive' value='1'".($withArchive?" checked" : "")." onClick='document.form1.submit()'><label for='withArchive'>".$Lang['RepairSystem']['DisplayArchivedRecord']."</label>";

## Status
//$StatusSelect = "<SELECT name='Status' id='Status' onChange='document.form1.submit()'>\n";
//$StatusSelect.= "<OPTION value='' ".(($Status=='') ? "selected" : "").">".$eDiscipline['Setting_Status_All']."</OPTION>\n";
//$StatusSelect.= "<OPTION value='1' ".(($Status=='1') ? "selected" : "").">".$Lang['RepairSystem']['Processing']."</OPTION>\n";
//$StatusSelect.= "<OPTION value='2' ".(($Status=='2') ? "selected" : "").">".$Lang['General']['Completed']."</OPTION>\n";
//$StatusSelect.= "<OPTION value='0' ".(($Status=='0') ? "selected" : "").">".$i_status_cancel."</OPTION>\n";
//$StatusSelect.= "<OPTION value='3' ".(($Status=='3') ? "selected" : "").">".$i_status_rejected."</OPTION>\n";
//$StatusSelect.= "</SELECT>&nbsp;\n";
$StatusArray[] = array('4', $Lang['RepairSystem']['Pending']);
$StatusArray[] = array('1', $Lang['RepairSystem']['Processing']);
$StatusArray[] = array('2', $Lang['General']['Completed']);
$StatusArray[] = array('0', $i_status_cancel);
$StatusArray[] = array('3', $i_status_rejected);
$StatusSelect = $linterface->GET_SELECTION_BOX($StatusArray, " name='Status' id='Status' onChange='document.form1.submit()' ", $eDiscipline['Setting_Status_All'], $Status);

## Category
$CategorySelect = $lrepairsystem->getCategorySelection($CatID, " onChange='document.form1.submit()'", 1, $Lang['RepairSystem']['AllCategory']);

## Request Summary Selection
$RequestSummarySelection = $lrepairsystem->getRequestSummarySelection($CatID," onChange='document.form1.submit()'","", $Lang['RepairSystem']['AllRequestSummary'],1,$RequestID);

## Filter (own/all)
$IssuedSelect = "<select name='display_own' onChange='document.form1.submit()'>\n";
$IssuedSelect .= "<option value='' ".(($display_own=='') ? "selected" : "").">". $Lang['RepairSystem']['AllRequests'] ."</option>";
$IssuedSelect .= "<option value='1' ".(($display_own=='1') ? "selected" : "").">". $Lang['RepairSystem']['MyRequests'] ."</option>";
$IssuedSelect .= "</SELECT>&nbsp;\n";

$keyword = standardizeFormPostValue($_POST['keyword']);
## search
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"". intranet_htmlspecialchars($keyword) ."\" onkeyup=\"Check_Go_Search(event);\"/>";

# Conditions
$conds = "";

if($CatID!="")
	$conds .= " AND a.CategoryID=$CatID";

if($Status!="")
	$conds .= " AND a.RecordStatus=$Status";

if($year!="")
	$conds .= " AND a.DateInput LIKE '$year-%'";
if($month!="") {
	if($month<=9) $month = '0'.$month;
	$conds .= " AND a.DateInput LIKE '%-$month-%'";
}

if($display_own)
{
	$conds .= " and a.UserID=$UserID";
}

if($keyword != "") {
	$ukw = mysql_real_escape_string(str_replace("\\","\\\\",$keyword));				// A&<>'"\B ==> A&<>\'\"\\\\B
	$ckw = intranet_htmlspecialchars(str_replace("\\","\\\\",$_POST['keyword']));	// A&<>'"\B ==> A&amp;&lt;&gt;\&#039;&quot;\\\\B
	
	$conds .= " and (a.Title LIKE '%$ukw%'";
	$conds .= " OR a.CaseNumber LIKE '%$ukw%'";
	$conds .= " OR b.EnglishName LIKE '%$ukw%'";
	$conds .= " OR b.ChineseName LIKE '%$ukw%'"; 
	$conds .= " OR a.Title LIKE '%$ckw%'";
	$conds .= " OR a.CaseNumber LIKE '%$ckw%'";
	$conds .= " OR b.EnglishName LIKE '%$ckw%'";
	$conds .= " OR b.ChineseName LIKE '%$ckw%')";	
}

$other_from .= $RequestID ? " and e.RecordID=$RequestID " :"";
$keyword = intranet_htmlspecialchars(stripslashes($keyword));
$name_field = getNameFieldByLang("b.");

# SQL Query

$buildingName = Get_Lang_Selection("bu.NameChi", "bu.NameEng");
$levelName = Get_Lang_Selection("lv.NameChi", "lv.NameEng");
$LocationName = Get_Lang_Selection("l.NameChi", "l.NameEng");
$FollowUpPersonName= getNameFieldByLang("u.");

if($withArchive) {
	$withArchiveDisplay = "<span class=\"tabletextrequire\">*</span>";
} else {
	$conds .= " AND a.ArchivedBy IS NULL";	
}

$sql = "
		SELECT 
			a.CaseNumber,
			LEFT(a.DateInput,10) as DateInput,
			IF($name_field IS NULL,'".$Lang['General']['EmptySymbol']."',$name_field) as user_name,
			IF((a.LocationBuildingID!=0),	
				CONCAT(IF($buildingName IS NOT NULL, $buildingName, '".$Lang['RepairSystem']['MainBuilding']."'), IF($levelName!='', CONCAT(' &gt; ', $levelName), ''), IF($LocationName!='', CONCAT(' &gt; ', $LocationName), ''), IF(a.DetailsLocation!='', CONCAT(' &gt; ', a.DetailsLocation), '')), CONCAT(c.LocationName, IF((a.DetailsLocation IS NOT NULL AND a.DetailsLocation!=''), CONCAT(' &gt; ', a.DetailsLocation), ''))) as LocationName,
			d.Name as CategoryName,
			concat('<a href=\'view.php?RecordID=', a.RecordID ,'&CatID=$CatID&Status=$Status&year=$year&month=$month&display_own=$display_own\'>',IF(a.Title IS NULL Or a.Title='','---', a.Title),'</a>') as title,
			CONCAT(
				IF(a.ArchivedBy IS NULL, '', '$withArchiveDisplay'),
				case a.RecordStatus
					when '1' then '". $Lang['RepairSystem']['Processing'] ."'
					when '2' then '". $Lang['General']['Completed'] ."'
					when '3' then '". $i_status_rejected ."'
					when '4' then '".$Lang['RepairSystem']['Pending']."'	
					else '". $i_status_cancel."'
				end
			) as status,
			/*IF(a.FollowUpPerson IS NULL,'".$Lang['General']['EmptySymbol']."',a.FollowUpPerson) as FollowUpPerson*/
			IF (
				p.RecordID is null,
					IF(a.FollowUpPerson IS NULL OR a.FollowUpPerson='','".$Lang['General']['EmptySymbol']."',a.FollowUpPerson),
					p.FollowupPerson
			) as FollowUpPerson
		FROM 
			REPAIR_SYSTEM_RECORDS as a
			LEFT JOIN INTRANET_USER as b on (b.UserID=a.UserID AND b.RecordType IN (0,1,2,3))
			LEFT join INTRANET_USER as u on (u.UserID=a.FollowUpPersonID)
			LEFT JOIN INVENTORY_LOCATION l ON (l.LocationID=a.LocationID)
			LEFT JOIN INVENTORY_LOCATION_LEVEL lv ON (lv.LocationLevelID=a.LocationLevelID)
			LEFT JOIN INVENTORY_LOCATION_BUILDING as bu on (bu.BuildingID=a.LocationBuildingID)
			LEFT JOIN REPAIR_SYSTEM_LOCATION as c on (c.LocationID=a.LocationID)
    		LEFT JOIN (
				SELECT 	p.RecordID, 
						GROUP_CONCAT(DISTINCT ".$FollowUpPersonName." ORDER BY u.EnglishName SEPARATOR ', ') AS FollowupPerson,
						GROUP_CONCAT(DISTINCT u.EnglishName ORDER BY u.EnglishName SEPARATOR ', ') AS SortFollowupPerson
				FROM 
						REPAIR_SYSTEM_FOLLOWUP_PERSON p
				INNER JOIN 
						INTRANET_USER u ON u.UserID=p.PersonID
				GROUP BY p.RecordID
			) AS p ON p.RecordID=a.RecordID			
			inner join REPAIR_SYSTEM_CATEGORY as d on (d.CategoryID=a.CategoryID)
			inner join REPAIR_SYSTEM_REQUEST_SUMMARY as e on (e.Title=a.Title and e.CategoryID=a.CategoryID  $other_from)
		WHERE
			a.RecordStatus != ".REPAIR_SYSTEM_STATUS_DELETED." 
			and e.RecordStatus='".REPAIR_SYSTEM_STATUS_APPROVED."'
			$conds

";

//debug_r($sql);
$li->sql = $sql;
$li->field_array = array("CaseNumber","DateInput", "user_name", "LocationName", "CategoryName", "a.Title", "status","FollowUpPerson");
$li->no_col = sizeof($li->field_array)+1;
$li->IsColOff = "IP25_table";
$li->column_array = array(0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0);

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th width='5%'>".$li->column($pos++,$Lang['RepairSystem']['CaseNumber'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $i_Discipline_Date)."</th>\n";
$li->column_list .= "<th width='12%' >".$li->column($pos++, $Lang['RepairSystem']['Reporter'])."</th>\n";
$li->column_list .= "<th width='18%' >".$li->column($pos++, $Lang['RepairSystem']['Location'])."</th>\n";
$li->column_list .= "<th width='18%' >".$li->column($pos++, $Lang['RepairSystem']['Category'])."</th>\n";
$li->column_list .= "<th width='18%' >".$li->column($pos++, $Lang['RepairSystem']['Request'])."</th>\n";
$li->column_list .= "<th width='8%' >".$li->column($pos++, $i_general_status)."</th>\n";
$li->column_list .= "<th width='8%' >".$li->column($pos++, $Lang['RepairSystem']['FollowUpPerson'])."</th>\n";

### Title ###
$TAGS_OBJ[] = array($Lang['RepairSystem']['List']);
$MODULE_OBJ = $lrepairsystem->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$returnMsgKey]);
?>

<script language="javascript">
<!--
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}
//-->
</script>

   
<form name="form1" method="POST" action="index.php">

<div class="content_top_tool">
       <div class="Conntent_tool"><?=$AddBtn?></div>
        <div class="Conntent_search"><?=$searchTag?></div>     
<br style="clear:both" />
</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="bottom">
	<?=$IssuedSelect?><?=$CategorySelect?><?=$RequestSummarySelection?><?=$StatusSelect?><?=$select_year?><?=$select_month?><br><?=$archiveCheckBox?>
</td>
</tr>
</table>

<?=$li->display();?>

<input type="hidden" name="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<? if($withArchive) {?>
<br>
<span class="tabletextremark"><span class='tabletextrequire'>*</span><?=$Lang['RepairSystem']['Archived']?></span>
<?}?>
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>