<?
/*
 *  2018-07-16 Cameron
 *      Fix: make request details box larger [case #N140845]
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();
$linterface = new interface_html();

$CatInfo = $lrepairsystem->getCategoryInfo($CatID);
$DetailsContent = intranet_undo_htmlspecialchars($CatInfo[0]['DetailsContent']);

echo $linterface->GET_TEXTAREA("content", $DetailsContent, $taCols=70, $taRows=20);
intranet_closedb();

?>