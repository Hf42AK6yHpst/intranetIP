<?

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();
$linterface = new interface_html();

$Info = $lrepairsystem->getRequestSummarySelection($CatID,"","","-- ". $Lang['Btn']['Select'] ." --", 1);
echo $Info;

intranet_closedb();

?>