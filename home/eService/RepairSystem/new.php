<?php
# using:  

##########################################
#
#   Date    :   2020-10-22 (Cameron)
#               move plupload parts to function call: isUsePlupload() and updateTempPluploadInfo()
#
#   Date    :   2020-09-29 (Cameron)
#               add case SrcFrom == 'eAdmin' [case #X190079]
#
#   Date    :   2020-08-02 (Cameron)
#               Modify: reorder item to: Location, Location Details, Category, Request Summary [case #X190080]
#
#   Date    :   2018-07-10 (Cameron)
#               Fix: set default content if CatID is selected, make request details box larger [case #N140845]
#
#	Date	:	2017-05-31 (Cameron)
#				Fix: disable button before submit to avoid adding duplicate record
#
#	Date	:	2017-05-09 (Cameron)
#				Improvement: add upload photo(s) function
#
#	Date	:	2016-12-02 (Cameron)
#				Add customized flag $sys_custom['RepairSystem']['GrantedUserIDArr'] to hardcode user for accessing eService -> Repair System
#
#	Date	:	2016-11-11 (Cameron)
#				Add customized flag $sys_custom['RepairSystemBanAccessForStudent'] to ban access for student
#
#	Date	:	2014-11-06 (Omas)
#				Improved : checking for building,location,sublocation,location detail
#
#	Date	:	2011-03-28 (Henry Chow)
#				Improved : select "Location" from "School Setting > Campus"
#
#	Date:	2011-01-03 [YAtWoon]
#			- set Location Detail is a optional field
#
#	Date:	2010-11-24 [YatWoon]
#			- change "Request Summary" from text field to selection (requested by "Lai Shan")
#
#	Date:	2010-10-27 [YatWoon]
#			set default of "Request Details" (flag: $sys_custom['RepairSystem_DefaultDetailsContent'])
##########################################

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

if ( !isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || $_SESSION['UserType'] == USERTYPE_ALUMNI || ($_SESSION['UserType'] == USERTYPE_PARENT && !$sys_custom['RepairSystemAccessForParent']) || ($_SESSION['UserType'] == USERTYPE_STUDENT && $sys_custom['RepairSystemBanAccessForStudent']) || (isset($sys_custom['RepairSystem']['GrantedUserIDArr']) && !in_array($_SESSION['UserID'],(array)$sys_custom['RepairSystem']['GrantedUserIDArr'])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}



$lrepairsystem = new librepairsystem();
$linterface = new interface_html();

$use_plupload = $lrepairsystem->isUsePlupload();

if($use_plupload) {
    $lrepairsystem->updateTempPluploadInfo();
}


$buildingAry = $lrepairsystem->getInventoryBuildingArray();
$levelAry = $lrepairsystem->getInventoryLevelArray();
$locationAry = $lrepairsystem->getInventoryLocationArray();

$buildingSelect = getSelectByArray($buildingAry, 'name="buildingID" id="building" onChange="changeLevel(this.value)"', $buildingID, 0, 0, "-- ".$Lang['RepairSystem']['Building']." --");

# menu highlight setting
if ($_GET['SrcFrom'] == 'eAdmin') {
    $CurrentPageArr['eAdminRepairSystem'] = 1;
    $TAGS_OBJ[] = array($Lang['RepairSystem']['AllRequests'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/RepairSystem/index.php", 1);
    $TAGS_OBJ[] = array($Lang['RepairSystem']['ArchivedRequests'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/RepairSystem/archive.php", 0);
}
else {
    $CurrentPageArr['eServiceRepairSystem'] = 1;
    $TAGS_OBJ[] = array($Lang['RepairSystem']['List']);
}

$CurrentPage = "PageList";
$MODULE_OBJ = $lrepairsystem->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
$PAGE_NAVIGATION[] = array($Lang['RepairSystem']['NewRequest']);

$categorySelection = $lrepairsystem->getCategorySelection($CatID, "onChange='selectCategory();'","","-- ". $Lang['Btn']['Select'] ." --");

$locationSelection = $lrepairsystem->getLocationSelection("", "");
$RequestSummarySelection = $lrepairsystem->getRequestSummarySelection($CatID,"","","-- ". $Lang['Btn']['Select'] ." --");

if($sys_custom['RepairSystem_DefaultDetailsContent'] && $CatID) {
    $CatInfo = $lrepairsystem->getCategoryInfo($CatID);
    $content = intranet_undo_htmlspecialchars($CatInfo[0]['DetailsContent']);
}
else {
    $content = '';
}

//echo $linterface->Include_Thickbox_JS_CSS();
//echo $lrepairsystem->Include_JS_CSS();
?>
<script language="javascript">
<!--
function check_form() {
	$('input.actionBtn').attr('disabled', 'disabled');
	var obj = document.form1;
	
	var error_no = 0;
	var focus_field = "";
	
	//// Reset div innerHtml
	reset_innerHtml();
	
	if(!check_select_30(obj.CatID, "<?php echo $i_alert_pleaseselect.$Lang['RepairSystem']['Category']; ?>.","","div_CatID_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "CatID";  
	}
	if(obj.buildingID.value==""){
		if(!check_select_30(obj.buildingID,  "<?php echo $i_alert_pleaseselect.$Lang['RepairSystem']['Location']; ?>.","","div_buildingID_err_msg")){
			error_no++
		if(focus_field=="")	focus_field = "buildingID";  
		}	
	}
	<?php if(!$sys_custom['RepairSystem']['DisableSubLocation']){ ?>
	else if(obj.LocationDetail.value=="") {
		if(!check_select_30(obj.locID, "<?php echo $i_alert_pleaseselect.$Lang['RepairSystem']['LocationDetail']; ?>.","","div_Location_err_msg"))
		{

			error_no++;
			if(focus_field=="")	focus_field = "locID";  
		}
	}

	else if(obj.locID.value=="") {
		if(!check_text_30(obj.LocationDetail, "<?php echo $i_alert_pleaseselect.$Lang['RepairSystem']['LocationDetail']; ?>.", "div_LocationDetail_err_msg"))
		{
			error_no++;
			if(focus_field=="")	focus_field = "LocationDetail";
		}
	}
	<?php } ?>

	if(!check_select_30(obj.RequestID, "<?php echo $i_alert_pleaseselect.$Lang['RepairSystem']['RequestSummary']; ?>.","","div_RequestSummary_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "RequestID";  
	}

	if(!check_text_30(obj.content, "<?php echo $i_alert_pleasefillin.$Lang['RepairSystem']['RequestDetails']; ?>.", "div_RequestDetails_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "content";
	}
	
<? if ($use_plupload): ?>	
	if(!jsIsFileUploadCompleted()) {
		error_no++;
		$('#FileWarnDiv').html('<?=$Lang['RepairSystem']['jsWaitAllPhotosUploaded']?>').show();
		setTimeout(hideFileWarningTimerHandler,5000);
	}
<? endif;?>
	
	if(error_no>0)
	{
		if (focus_field != '') {
			eval("obj." + focus_field +".focus();");
		}
		$('input.actionBtn').attr('disabled', '');
		return false;
	}
	else
	{
<? if (!$use_plupload): ?>	
		$('#FileName').val(document.getElementById('File').value);
<? endif;?>		
		obj.submit();
		return true;
	}
}

function reset_innerHtml()
{
 	document.getElementById('div_CatID_err_msg').innerHTML = "";
 	<?php if(!$sys_custom['RepairSystem']['DisableSubLocation']){ ?>
 	document.getElementById('div_Location_err_msg').innerHTML = "";
 	document.getElementById('div_LocationDetail_err_msg').innerHTML = "";
 	<?php } ?>
 	document.getElementById('div_RequestSummary_err_msg').innerHTML = "";
 	document.getElementById('div_RequestDetails_err_msg').innerHTML = "";
 	
 	
}

function goBack() {
	window.location= "<?php echo ($_GET['SrcFrom'] == 'eAdmin') ? $PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/RepairSystem/index.php' : 'index.php';?>";
}


function selectCategory()
{
	var cate_id = document.form1.CatID.value;

	<? if($sys_custom['RepairSystem_DefaultDetailsContent']) {?>
		$('#div_content').load(
			'ajax_loadDefaultContent.php', 
			{CatID: cate_id}, 
			function (data){
			}); 
	<? } ?>
	
	$('#div_request_summary').load(
		'ajax_loadDefaultRequestSummary.php', 
		{CatID: cate_id}, 
		function (data){
		}); 
}

function click_reset()
{
	reset_innerHtml();
	document.form1.reset();
	selectCategory();
}

function changeLevel(value)
{
	changeLocation('');
	var level_select = document.form1.elements["LocationLevelID"];
	
	var selectedCatID = value;
	
	while (level_select.options.length > 0)
	{
	    level_select.options[0] = null;
	}
	
	level_select.options[0] = new Option("-- <?=$i_InventorySystem_Location_Level?> --","");
	
	if (selectedCatID == '')
	{
	<?
		$curr_building_id = "";
		$pos = 1;
	for ($i=0; $i<sizeof($levelAry); $i++)
	{
	     list($r_buildingID, $r_LevelID, $r_LevelName) = $levelAry[$i];
	     $r_LevelName = intranet_undo_htmlspecialchars($r_LevelName);
	     $r_LevelName = str_replace('"', '\"', $r_LevelName);
	     $r_LevelName = str_replace("'", "\'", $r_LevelName);
	     if ($r_buildingID != $curr_building_id)
	     {
	         $pos = 1;
	?>
     }
	else if (selectedCatID == "<?=$r_buildingID?>")
	{
	     <?
	         $curr_building_id = $r_buildingID;
	     }
	     ?>
		level_select.options[<?=$pos++?>] = new Option("<?=$r_LevelName?>",<?=$r_LevelID?>);
		<?	if($r_LevelID == $LevelID) {	?>
			level_select.selectedIndex = <?=$pos-1?>;
		<? } ?>
		<?
	}
	?>
     }
}

function changeLocation(value)
{
	var location_select = document.form1.elements["locID"];
	
	var selectedLevelID = value;
	
	while (location_select.options.length > 0)
	{
	    location_select.options[0] = null;
	}
	
	location_select.options[0] = new Option("-- <?=$i_InventorySystem_Location?> --","");
	
	if (selectedLevelID == '')
	{
	<?
		$curr_level_id = "";
		$pos = 1;
	for ($i=0; $i<sizeof($locationAry); $i++)
	{
	     list($r_levelID, $r_LocationID, $r_LocatinName) = $locationAry[$i];
	     $r_LocatinName = intranet_undo_htmlspecialchars($r_LocatinName);
	     $r_LocatinName = str_replace('"', '\"', $r_LocatinName);
	     $r_LocatinName = str_replace("'", "\'", $r_LocatinName);
	     if ($r_levelID != $curr_level_id)
	     {
	         $pos = 1;
	?>
     }
	else if (selectedLevelID == "<?=$r_levelID?>")
	{
	     <?
	         $curr_level_id = $r_levelID;
	     }
	     ?>
		location_select.options[<?=$pos++?>] = new Option("<?=$r_LocatinName?>",<?=$r_LocationID?>);
		<?	if($r_LocationID == $locID) {	?>
			location_select.selectedIndex = <?=$pos-1?>;
		<? } ?>
		<?
	}
	?>
     }
}

function hideFileWarningTimerHandler()
{
	$('#FileWarnDiv').hide();
}

-->
</script>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>

<form name="form1" method="POST" action="new_update.php" enctype="multipart/form-data">
<table class="form_table_v30">

<tr>
	<td class="field_title"><span class='tabletextrequire'>*</span><?=$Lang['RepairSystem']['Location']?> </td>
	<td>
		<?/*=$locationSelection*/?>
		<div id="div_buildingID"><?=$buildingSelect?> &gt; 
		<select name="LocationLevelID" id="LocationLevelID" onChange="changeLocation(this.value)"><OPTION>-- <?=$i_InventorySystem_Location_Level?> --</select></div><span id='div_buildingID_err_msg'></span>
	</td>
</tr>

<tr>
	<td class="field_title"><span class='tabletextrequire'><?php echo ($sys_custom['RepairSystem']['DisableSubLocation'] == ''? '*' : ''); ?></span><?=$Lang['RepairSystem']['LocationDetail']?> </td>
	<td>
		<select name="locID" id="locID"><OPTION>-- <?=$i_InventorySystem_Location?> --</OPTION></select>
		&gt;
		<input type='text' name='LocationDetail' id='LocationDetail' maxlength='255' value='' class=''><span class="tabletextremark"> (<?=$Lang['General']['EnterBothOrEitherOne'] ?>)</span><br><span id='div_Location_err_msg'></span><span id='div_LocationDetail_err_msg'></span></td>
</tr>

<? /* ?>
<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['RequestSummary']?> <span class='tabletextrequire'>*</span></td>
	<td><input type='text' name='Title' id='Title' maxlength='255' value='' class='textboxtext'></td>
</tr>
<? */ ?>

    <tr>
        <td class="field_title"><span class='tabletextrequire'>*</span><?=$Lang['RepairSystem']['Category']?> </td>
        <td>
            <?=$categorySelection?><br><span id='div_CatID_err_msg'></span>
        </td>
    </tr>

    <tr>
	<td class="field_title"><span class='tabletextrequire'>*</span><?=$Lang['RepairSystem']['RequestSummary']?> </td>
	<td><div id="div_request_summary"><?=$RequestSummarySelection?></div><span id='div_RequestSummary_err_msg'></span></td>
</tr>
<tr>
	<td class="field_title"><span class='tabletextrequire'>*</span><?=$Lang['RepairSystem']['RequestDetails']?> </td>
	<td><div id="div_content"><?=$linterface->GET_TEXTAREA("content", $content, $taCols=70, $taRows=20);?></div><span id='div_RequestDetails_err_msg'></span></td>
</tr>

<!-- upload photoes -->
<tr>
	<td class="field_title">
		<?=$Lang['RepairSystem']['UploadPhotos']?>
	</td>
	<td> 
<? if($use_plupload){ ?>
		<div id="<?=$pluploadContainerId?>"></div>
		<?=$linterface->GET_SMALL_BTN($Lang['RepairSystem']['SelectPhotos'], "button", "", $pluploadButtonId, $ParOtherAttribute="", $OtherClass="", $ParTitle='')?>
		<span class="tabletextremark">(<?=$Lang['RepairSystem']['PhotoRemark']?>)</span>
		<div id="<?=$pluploadDropTargetId?>" style="display:none;" class="DropFileArea"><?=$Lang['RepairSystem']['OrDragAndDropPhotosHere']?></div>
		<div id="<?=$pluploadFileListDivId?>"></div>
			<?=$linterface->Get_Thickbox_Warning_Msg_Div('FileWarnDiv','','FileWarnDiv')?>
<? }else{ ?>
		<input type="file" name="File" id="File" class="textbox"/>
<? } ?>
		<span id="div_err_File"></span>
	</td>

</tr>
					
</table>

<?=$linterface->MandatoryField();?>

<div class="edit_bottom_v30">
<p class="spacer"></p>

	<?= $linterface->GET_ACTION_BTN($button_submit, "button", "check_form()", "btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
	<?= $linterface->GET_ACTION_BTN($button_reset, "button", "click_reset()", "btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
	<?= $linterface->GET_ACTION_BTN($button_back, "button", "goBack()", "btnBack", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
<p class="spacer"></p>
</div>


<input type="hidden" name="FileName" id="FileName" value="">
<? if($use_plupload) { ?>
<input type="hidden" name="TargetFolder" id="TargetFolder" value="<?=$tempFolderPath?>">
<? } ?>
<input type="hidden" name="SrcFrom" id="SrcFrom" value="<?php echo $_GET['SrcFrom'];?>">
</form>

<?
if($use_plupload) {
	include_once("plupload_script.php");
}

// print $linterface->FOCUS_ON_LOAD("form1.RecordDate");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>