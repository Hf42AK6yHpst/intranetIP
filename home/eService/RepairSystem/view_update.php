<?php
# using: 
#
#	Date	:	2016-12-02 (Cameron)
#				Add customized flag $sys_custom['RepairSystem']['GrantedUserIDArr'] to hardcode user for accessing eService -> Repair System
#
#	Date	:	2016-11-11 (Cameron)
#				Add customized flag $sys_custom['RepairSystemBanAccessForStudent'] to ban access for student
#

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

if ( !isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || $_SESSION['UserType'] == USERTYPE_ALUMNI || ($_SESSION['UserType'] == USERTYPE_PARENT && !$sys_custom['RepairSystemAccessForParent']) || ($_SESSION['UserType'] == USERTYPE_STUDENT && $sys_custom['RepairSystemBanAccessForStudent']) || (isset($sys_custom['RepairSystem']['GrantedUserIDArr']) && !in_array($_SESSION['UserID'],(array)$sys_custom['RepairSystem']['GrantedUserIDArr'])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$lrepairsystem = new librepairsystem();

if(sizeof($_POST)==0) {
	header("Location: index.php");	
}

if($flag=="cancel") {			# cancel request
	$lrepairsystem->changeRequestStatus($RecordID, REPAIR_RECORD_STATUS_CANCELLED);
	$lrepairsystem->addRecordRemark($RecordID, intranet_htmlspecialchars($Lang['RepairSystem']['CancelRequest']));
} 
else if($flag=="submit") {		# submit "remark"
	$lrepairsystem->addRecordRemark($RecordID, intranet_htmlspecialchars($additionRemark));
}

intranet_closedb();

header("Location: view.php?xmsg=UpdateSuccess&RecordID=$RecordID");

?>