<?php
#using: yat

################## Change Log [Start] ##############
#
#	Date:	2011-08-04	YatWoon
#			HTML-editor issue (need update display description)
#
#	Date:	2010-11-30	YatWoon
#			If user can edit circular, then display "edit" button and click button redriect to edit page
#
# Date:	2010-03-24	YatWoon
#		Add "All questions are required to be answered" option
#
################## Change Log [End] ##############

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$special_feature['circular'] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lform = new libform();
$lcircular = new libcircular($CircularID);

# Check admin level
$actualTargetUserID = $UserID;
$isAdmin = false;
if ($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"])
{
    $adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"];  # 1 - circular type admin
    $isAdmin = true;
}
$queString = $lform->getConvertedString($lcircular->Question);

$targetType = array("",$i_Circular_RecipientTypeAllStaff,
                    $i_Circular_RecipientTypeAllTeaching,
                    $i_Circular_RecipientTypeAllNonTeaching,
                    $i_Circular_RecipientTypeIndividual);

$attachment = $lcircular->displayAttachment();
$reqFillAllFields = $lcircular->AllFieldsReq;

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['eCircular'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();


# check if user can edit the circular or not
if ($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"])
{
    $adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"];  # 1 - circular type admin
    if ($adminlevel==1 || $lcircular->IssueUserID == $UserID)     # Allow if Full admin or issuer
    {
        if ($lcircular->isEditable())
		{
			$link = "/home/eAdmin/StaffMgmt/circular/edit.php?CircularID=".$CircularID;
			$edit_btn = $linterface->GET_ACTION_BTN($Lang['Btn']['Edit'], "button", "EditCircular('$link');" );
		}
    }
}

?>
<script language="Javascript">
function checkform(obj)
{
	if (!confirm('<?=$i_Circular_Alert_Sign?>'))
		return false;
}

function click_print()
{
	with(document.form1)
	{
		action = "view_print_preview.php";
		target = "_blank";
		submit();
	}
    
}

function EditCircular(url)
{
	window.opener.location=url;
	window.close();
}
</script>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_DateStart?></span>
					</td>
					<td class="tabletext"><?=$lcircular->DateStart?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_DateEnd?></span>
					</td>
					<td class="tabletext"><?=$lcircular->DateEnd?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_Title?></span>
					</td>
					<td class="tabletext"><?=$lcircular->Title?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
					<span class="tabletext"><?=$i_Circular_CircularNumber?></span>
					</td>
					<td class="tabletext"><?=$lcircular->CircularNumber?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_Description?></span>
					</td>
					<td class="tabletext"><?=htmlspecialchars_decode($lcircular->Description)?></td>
				</tr>
				<? if ($attachment != "") { ?>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_Attachment?></span>
					</td>
					<td class="tabletext"><?=$attachment?></td>
				</tr>
				<? } ?>
				<?
					$issuer = $lcircular->returnIssuerName();
				?>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_Issuer?></span>
					</td>
					<td class="tabletext"><?=$issuer?></td>
				</tr>
				<? if ($isAdmin) { ?>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_RecipientType?></span>
					</td>
					<td class="tabletext"><?=$targetType[$lcircular->RecordType]?></td>
				</tr>
				<? } ?>
				
				<? if ($reqFillAllFields) { ?>
				<tr>
					<td>&nbsp;</td>
					<td>[<?=$i_Survey_AllRequire2Fill?>]</td>
				</tr>
				<? } ?>
			</table>
		</td>
	</tr>
	<tr>
		<td>	
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/scissors_line.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/icons_scissors.gif"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr valign="top">
					<td class='eNoticereplytitle' align='center' colspan="2"><?=$Lang['eCircular']['ReplySlipForm']?></td>
				</tr>
				
				<tr valign="top">
					<td class="tabletext" colspan="2">
						<script language="javascript" src="/templates/forms/layer.js"></script>
						<script language="javascript" src="/templates/forms/form_edit.js"></script>
						<script language="Javascript">
						var replyslip = '<?=$Lang['eCircular']['ReplySlip']?>';
						</script>
						<form name="ansForm" method="post" action="update.php">
							<input type=hidden name="qStr" value="">
							<input type=hidden name="aStr" value="">
						</form>
						<script language="Javascript">
						<?=$lform->getWordsInJS()?>
							background_image = "/images/layer_bg.gif";
							var DisplayQuestionNumber = '<?=$lcircular->DisplayQuestionNumber?>';
							
							var sheet= new Answersheet();
							// attention: MUST replace '"' to '&quot;'
							sheet.qString="<?=$queString?>";
							sheet.aString="<?=$ansString?>";	
							
							//edit submitted application
							sheet.mode=1;
							sheet.answer=sheet.sheetArr();
							//sheet.templates=form_templates;
							document.write(editPanel());
							
						</script>
						<script language="javascript">
							function copyback()
							{
								finish();
								document.form1.qStr.value = document.ansForm.qStr.value;
								document.form1.aStr.value = document.ansForm.aStr.value;

							}
							
							function star(option)
							{
								//Unstar
								if (option == 3)
								{
									document.form1.test.value = 3;
								}
								//Add Star
								else if (option == 4)
								{
									document.form1.test.value = 4;
								}
								
								finish();
							}
							
							
						</script>					
					</td>
				</tr>
				<tr>
					<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
				
				<tr>			
				

				<td align = "right">
				<?if ($showStar ==1){?>
					<form name="form1" action="sign_update.php" method="post" onSubmit="return checkform(this)">
					<?$star = $lcircular ->getStar($lcircular -> CircularID);
					if ($star == 1){?>
						<?= $linterface->GET_ACTION_BTN('Remove Star', "submit", "star(3);","submit") ;}
					else{?>
						<?= $linterface->GET_ACTION_BTN('Add Star', "submit", "star(4);","submit") ;}?>
						
					<input type=hidden name=test value="">
					<input type=hidden name=qStr value="">
					<input type=hidden name=aStr value="">
					<input type=hidden name=CircularID value="<?=$CircularID?>">
					<input type=hidden name=actualTargetUserID value="<?=$actualTargetUserID?>">
					<input type=hidden name=adminMode value="<?=($isAdmin? "1":"0")?>">
					</form>
				<?}?>		
				</td>
				
				<form name="form1" action="view_print_preview.php" method="post">

					<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "click_print();","cancelbtn") ?>
						<?=$edit_btn?>
						<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn") ?>
						
						<input type=hidden name=qStr value="">
						<input type=hidden name=aStr value="">
						<input type=hidden name=CircularID value="<?=$CircularID?>">
						<input type=hidden name=actualTargetUserID value="<?=$actualTargetUserID?>">
						<input type=hidden name=adminMode value="<?=($isAdmin? "1":"0")?>">
					</td>			
				</form>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>