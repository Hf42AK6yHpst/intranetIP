<?php
# using: yat

################## Change Log [Start] ##############
#
#   Date:   2018-06-21 Isaac
#           expended width of the notice table
#
#	Date:	2011-08-04	YatWoon
#			HTML-editor issue (need update display description)
#
#	Date:	2011-02-23	YatWoon
#			Add "Display question number"
#
# Date:	2010-04-29	YatWoon
#		Add "All questions are required to be answered" option
#
################## Change Log [End] ##############


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!$special_feature['circular'] || $_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] || $_SESSION['UserType']!=USERTYPE_STAFF)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT($i_general_no_access_right);
	exit;
}

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$lform = new libform();
$lcircular = new libcircular($CircularID);

/*
# Check admin level
$actualTargetUserID = $UserID;
$isAdmin = false;
if ($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
{
    $adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"];
    if ($adminlevel==1 || $lcircular->IssueUserID == $UserID || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])     # Allow if Full admin or issuer
    {
        if ($targetUserID != "")
        {
            $actualTargetUserID = $targetUserID;
            if ($targetUserID != $UserID)
            {
                $isAdmin = true;
            }
        }
        else
        {
            $actualTargetUserID = $UserID;
        }
    }
}
*/

# Check Reply involved
if (!$lcircular->retrieveReply($actualTargetUserID))
{
     header("Location: $intranet_httppath/home/close.php");
     intranet_closedb();
     exit();
}
$lu = new libuser($actualTargetUserID);

$temp_que =  str_replace("&amp;", "&", $lcircular->Question);
$queString = $lform->getConvertedString($temp_que);
$queString=trim($queString)." ";

# Check View/Edit Mode
$pos = 0;
if ($isAdmin)
{
    $js_edit_mode = ($lcircular->isHelpSignAllow);
}
else
{
    $sign_allowed = false;
    $time_allowed = false;
    if ($lcircular->isReplySigned())
    {
        if ($lcircular->isResignAllow)
        {
            $signed_allowed = true;
        }
    }
    else
    {
        $signed_allowed = true;
    }

    $today = time();
    $deadline = strtotime($lcircular->DateEnd);
    if (compareDate($today,$deadline)>0)
    {
        if ($lcircular->isLateSignAllow)
        {
            $time_allowed = true;

        }
    }
    else
    {
        $time_allowed = true;
    }

    $js_edit_mode = ($signed_allowed && $time_allowed);

}

$temp_ans =  str_replace("&amp;", "&", $lcircular->answer);
$ansString = $lform->getConvertedString($temp_ans);
$ansString.=trim($ansString)." ";

// include_once("../../templates/fileheader.php");

$targetType = array("",$i_Circular_RecipientTypeAllStaff,
                    $i_Circular_RecipientTypeAllTeaching,
                    $i_Circular_RecipientTypeAllNonTeaching,
                    $i_Circular_RecipientTypeIndividual);

$attachment = $lcircular->displayAttachment();

//$MODULE_OBJ['title'] = $i_Circular_Circular;
$linterface = new interface_html();
//$linterface->LAYOUT_START();

$reqFillAllFields = $lcircular->AllFieldsReq;

?>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='center' class='eSportprinttitle'><b><?=$i_Circular_Circular?></b></td>
	</tr>
</table>

<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr><td class="eSportprinttitle"><?=$i_Circular_DateStart?> : <?=$lcircular->DateStart?></td></tr>
	<tr><td class="eSportprinttitle"><?=$i_Circular_DateEnd?> : <?=$lcircular->DateEnd?></td></tr>
	<tr><td class="eSportprinttitle"><?=$i_Circular_Title?> : <?=$lcircular->Title?></td></tr>
	<tr><td class="eSportprinttitle"><?=$i_Circular_Description?> : <?=htmlspecialchars_decode($lcircular->Description)?></td></tr>
	<? if ($attachment != "") { ?>
	<tr><td class="eSportprinttitle"><?=$i_Circular_Attachment?> : <?=$attachment?></td></tr>
	<? } ?>
	<?
		$issuer = $lcircular->returnIssuerName();
	?>
	<tr><td class="eSportprinttitle"><?=$i_Circular_Issuer?> : <?=$issuer?></td></tr>
	<?
		if (!$lcircular->isReplySigned())
		{
			$statusString = "$i_Circular_Unsigned";
		}
		else
		{
			$signer = $lu->getNameWithClassNumber($lcircular->signerID);
			$signby = ($lcircular->replyType==1? $i_Circular_Signer:$i_Circular_Editor);
			$statusString = "$i_Circular_Signed <i>($signby $signer $i_Circular_At ".$lcircular->signedTime.")</i>";
		}
	?>
	<tr><td class="eSportprinttitle"><?=$i_Circular_SignStatus?> : <?=$statusString?></td></tr>
	<? if ($isAdmin) { ?>
	<tr><td class="eSportprinttitle"><?=$i_Circular_Recipient?> : <?=$lu->getNameWithClassNumber($actualTargetUserID)?></td></tr>
	<tr><td class="eSportprinttitle"><?=$i_Circular_RecipientType?> : <?=$targetType[$lcircular->RecordType]?></td></tr>
	<? } ?>
	<? if ($reqFillAllFields) { ?>
	<tr>
		<td>[<?=$i_Survey_AllRequire2Fill?>]</td>
	</tr>
	<? } ?>
	
	<tr><td align='center'><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/scissors_line.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/icons_scissors.gif"></td>
				</tr>
			</table>
			</td></tr>
	<tr><td class='eSportprinttitle' align='center'><b><?=$i_Circular_ReplySlip?></b></td></tr>
	<tr><td class="eSportprinttitle">
		<script language="javascript" src="/templates/forms/layer.js"></script>
		<? if ($js_edit_mode) {
		# Fill form mode
		?>
		<script language="javascript" src="/templates/forms/form_edit.js"></script>
		<script language="Javascript">
		var replyslip = '<?=$i_Notice_ReplySlip?>';
		</script>
		<? } else { ?>
		<script language="javascript" src="/templates/forms/form_view.js"></script>
		<? } ?>
		
		<script language="Javascript">
		<?=$lform->getWordsInJS()?>
			background_image = "/images/layer_bg.gif";
			<? if ($js_edit_mode) {
			# Fill form mode
			?>

			var DisplayQuestionNumber = '<?=$lcircular->DisplayQuestionNumber?>';
			
			var sheet= new Answersheet();
			// attention: MUST replace '"' to '&quot;'
			sheet.qString="<?=$queString?>";
			sheet.aString="<?=$ansString?>";

			//edit submitted application
			sheet.mode=1;
			sheet.answer=sheet.sheetArr();
			//sheet.templates=form_templates;
			document.write(editPanel());

			<? } else {
			# View answer only
			?>
			myQue="<?=$queString?>";
			myAns="<?=$ansString?>";
			document.write(viewForm(myQue, myAns));
		<? } ?>
		</script>
		
		
	</td></tr>
</table>
<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>