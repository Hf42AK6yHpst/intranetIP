<?php

################## Change Log [Start] ##############
#
#   Date:   2018-06-21 Isaac
#           expended width of the notice table and reduce the width of the title td
#
#	Date:	2011-08-04	YatWoon
#			HTML-editor issue (need update display description)
#
#	Date:	2011-02-22	YatWoon
#			Add  "Display question number"
#
# Date:	2010-03-24	YatWoon
#		Add "All questions are required to be answered" option
#
################## Change Log [End] ##############

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$special_feature['circular'] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$lform = new libform();
$lcircular = new libcircular($CircularID);

//$actualTargetUserID = $UserID;
# Check admin level
$isAdmin = false;
if($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"])
{
    $adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"];
    $isAdmin = true;
}
$queString = $lform->getConvertedString($lcircular->Question);

$targetType = array("",$i_Circular_RecipientTypeAllStaff,
                    $i_Circular_RecipientTypeAllTeaching,
                    $i_Circular_RecipientTypeAllNonTeaching,
                    $i_Circular_RecipientTypeIndividual);

$attachment = $lcircular->displayAttachment();
$reqFillAllFields = $lcircular->AllFieldsReq;

$linterface = new interface_html();

?>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='center' class='eSportprinttitle'><b><?=$i_Circular_Circular?></b></td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr><td class="eSportprinttitle"><?=$i_Circular_DateStart?> : <?=$lcircular->DateStart?></td></tr>
	<tr><td class="eSportprinttitle"><?=$i_Circular_DateEnd?> : <?=$lcircular->DateEnd?></td></tr>
	<tr><td class="eSportprinttitle"><?=$i_Circular_Title?> : <?=$lcircular->Title?></td></tr>
	<tr><td class="eSportprinttitle"><?=$i_Circular_Description?> : <?=htmlspecialchars_decode($lcircular->Description)?></td></tr>
	<? if ($attachment != "") { ?>
	<tr><td class="eSportprinttitle"><?=$i_Circular_Attachment?> : <?=$attachment?></td></tr>
	<? } ?>
	<?
		$issuer = $lcircular->returnIssuerName();
	?>
	<tr><td class="eSportprinttitle"><?=$i_Circular_Issuer?> : <?=$issuer?></td></tr>
	<? if ($isAdmin) { ?>
	<tr><td class="eSportprinttitle"><?=$i_Circular_RecipientType?> : <?=$targetType[$lcircular->RecordType]?></td></tr>
	<? } ?>
	<? if ($reqFillAllFields) { ?>
	<tr>
		<td>[<?=$i_Survey_AllRequire2Fill?>]</td>
	</tr>
	<? } ?>
				
	<tr><td align='center'><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/print_scissors.gif"></td></tr>
	<tr><td class='eSportprinttitle' align='center'><b><?=$Lang['eCircular']['ReplySlipForm']?></b></td></tr>
	<tr><td class="eSportprinttitle">
		<script language="javascript" src="/templates/forms/layer.js"></script>
		<script language="javascript" src="/templates/forms/form_edit.js"></script>
		<script language="Javascript">
		var replyslip = '<?=$Lang['eCircular']['ReplySlip']?>';
		</script>

		<form name="ansForm" method="post" action="update.php">
			<input type=hidden name="qStr" value="">
			<input type=hidden name="aStr" value="">
		</form>
		<script language="Javascript">
		<?=$lform->getWordsInJS()?>
			background_image = "/images/layer_bg.gif";
	
			var DisplayQuestionNumber = '<?=$lcircular->DisplayQuestionNumber?>';
			
			var sheet= new Answersheet();
			// attention: MUST replace '"' to '&quot;'
			sheet.qString="<?=$queString?>";
			sheet.aString="<?=$ansString?>";	
			
			//edit submitted application
			sheet.mode=1;
			sheet.answer=sheet.sheetArr();
			//sheet.templates=form_templates;
			document.write(editPanel());
			
		</script>
		<script language="javascript">
			function copyback()
			{
				finish();
				document.form1.qStr.value = document.ansForm.qStr.value;
				document.form1.aStr.value = document.ansForm.aStr.value;
			}
		</script>
	</td></tr>
</table>
<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>