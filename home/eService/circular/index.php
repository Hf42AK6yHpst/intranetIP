<?php
# using: 

############ Change Log Start ###############
#   Date:	2019-07-09	Tommy	
#			add $From_eService=1 in $lcircular->GET_MODULE_OBJ_ARR()
#
#	Date:	2017-05-31	Bill	[2017-0529-1652-27206]
#			set cookies after include global.php, PHP 5.4 problem
#
#	Date:	2010-02-03	YatWoon
#			update "Year" select option, only display the year which includes any notice.
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

### Set Cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve Table View
if ($ck_circular_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_circular_page_number", $pageNo, 0, "", "", 0);
	$ck_circular_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_circular_page_number!="")
{
	$pageNo = $ck_circular_page_number;
}
if ($ck_circular_page_order!=$order && $order!="")
{
	setcookie("ck_circular_page_order", $order, 1, "", "", 1);
	$ck_circular_page_order = $order;
}
else if (!isset($order) && $ck_circular_page_order!="")
{
	$order = $ck_circular_page_order;
}
if ($ck_circular_page_field!=$field && $field!="")
{
	setcookie("ck_circular_page_field", $field, 0, "", "", 0);
	$ck_circular_page_field = $field;
}
else if (!isset($field) && $ck_circular_page_field!="")
{
	$field = $ck_circular_page_field;
}

if(!$special_feature['circular'] || $_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] || $_SESSION['UserType']!=USERTYPE_STAFF)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);

$CurrentPageArr['eServiceCircular'] = 1;
$lcircular = new libcircular();

$functionbar = "$changeLink";
                  
## select Year
$filterbar = "";
$currentyear = date('Y');
$array_year = $lcircular->retrieveCircularYears();
if(empty($array_year))	$array_year[] = $currentyear;

/*
for ($i=$currentyear-7; $i<$currentyear+1; $i++)
{
     $array_year[] = $i;
}*/

$firstname = $i_status_all . ($intranet_session_language=="en"?" ":"") . $i_general_Years;
$select_year = getSelectByValueDiffName($array_year,$array_year,"name='year' onChange='document.form1.submit();'",$year,1,0, $firstname);

## select Month
for ($i=1; $i<=12; $i++)
{
     $array_month[] = $i;
}
$firstname = $i_status_all . ($intranet_session_language=="en"?" ":"") . $i_general_Months;
$select_month = getSelectByValueDiffName($array_month,$array_month,"name='month' onChange='document.form1.submit();'",$month,1,0,$firstname);

// if($past!=2)
// {
	## select status
	$firstname_status = "-- $i_status_all --";
	if($special_feature['circular_star'])
	{
		$array_status_name = array($i_Circular_Unsigned, $i_Circular_Signed, $eCircular["StarredCirculars"]);
		$array_status_data = array("0", "2", "4");
	}
	else
	{
		$array_status_name = array($i_Circular_Unsigned, $i_Circular_Signed);
		$array_status_data = array("0", "2");
	}
	$select_status = getSelectByValueDiffName($array_status_data,$array_status_name,"name='status' onChange='document.form1.submit();'",$status,1,0,$firstname_status);
// }

## select current/past
$array_past_name = array($i_Circular_CurrentCircular, $i_Circular_PastCircular);
$array_past_data = array("0", "1");
if($lcircular->showAllEnabled)
{
	array_push($array_past_name, $i_Circular_AllCircular);
	array_push($array_past_data, "2");
}
$select_past = getSelectByValueDiffName($array_past_data,$array_past_name,"name='past' onChange='document.form1.submit();'",$past,0,1);


$filterbar .= "$select_past $select_status";
$filterbar .= "$select_year $select_month";

if ($intranet_session_language == "gb")
{
	$image_suffix = "_gb";
} else {
	$image_suffix = "";
}

$CurrentPageArr['eServiceCircular'] = 1;
$CurrentPage = "PageCircular";
$linterface = new interface_html();

############################################################################################################

# TABLE SQL

$keyword = convertKeyword($keyword);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if (!isset($field)) $field = 0;
if (!isset($order)) $order = 0; 


$li = new libdbtable2007($field, $order, $pageNo);
//$li = new libdbtable2007(0, 1, 1);

if ($year != "")
{
	$conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
}
if ($month != "")
{
	$conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
}
if ($past == 1)
{
	$conds .= " AND a.DateEnd < CURDATE()";
	$order_str = " a.DateStart DESC";
}
else
{
	if($past!=2)
		$conds .= " AND a.DateStart <= CURDATE() AND a.DateEnd >= CURDATE()";
	$order_str = " a.DateEnd ASC";
}
if ($status == "0" || $status == 2)
{
	$conds .= " AND c.RecordStatus = $status";
}

if ($status == 4 && $special_feature['circular_star'])
{
	$conds .= " AND HasStar = 1";
}


if($past!=2)
{
	$j = "sign";
	$conds .= " and c.UserID = $UserID";	
}
else
{
	if(!$lcircular->showAllEnabled)
		$past = 0;
	$j = "view";
}
//$name_field = getNameFieldWithLoginByLang("b.");
$name_field = getNameFieldByLang("b.");

$sql = "SELECT 
			DATE_FORMAT(a.DateStart,'%Y-%m-%d'),
       		DATE_FORMAT(a.DateEnd,'%Y-%m-%d'),";
       		

if($special_feature['circular_star'])
{
	$sql.=" IF(HasStar = 1,
			CONCAT('<img src=\"$image_path/$LAYOUT_SKIN/eLibrary/star_on.gif\"><font>&nbsp;&nbsp;</font>',a.CircularNumber), 
			CONCAT('<FONT>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</FONT>',a.CircularNumber)),";
}
else
{
	$sql.="a.CircularNumber,";	
}
$sql .= "CONCAT('<a href=\"javascript:". $j ."(', a.CircularID, ')\" class=\"tablelink\">', 
       			'<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" alt=\"$button_view\" border=0></a>'),
       		CONCAT('<a href=\"javascript:". $j ."(', a.CircularID, ')\" class=\"tablelink\">', 
       			a.Title, '</a>'),
       		IF(b.UserID IS NULL,CONCAT('<I>',a.IssueUserName,'</I>'),$name_field),
       		c.RecordStatus,
       		CASE (a.RecordType) 
       			WHEN '' THEN ''
       			WHEN 1 THEN '$i_Circular_RecipientTypeAllStaff'
       			WHEN 2 THEN '$i_Circular_RecipientTypeAllTeaching'
       			WHEN 3 THEN '$i_Circular_RecipientTypeAllNonTeaching'
       			WHEN 4 THEN '$i_Circular_RecipientTypeIndividual'
       			ELSE '' 
       		END
       FROM 
			INTRANET_CIRCULAR as a
			LEFT OUTER JOIN  INTRANET_CIRCULAR_REPLY as c ON (a.CircularID = c.CircularID)
			LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = a.IssueUserID
       WHERE 
	   				('%$keyword%' IS NOT NULL) AND (
				((a.CircularNumber like '%$keyword%') OR
				(a.Title like '%$keyword%') OR
				(b.EnglishName like '%$keyword%') OR
				(b.ChineseName like '%$keyword%')) AND 
				(a.RecordStatus = 1 $conds))
		group by a.CircularID
       ";
//and c.UserID = $UserID
       
//        		CONCAT('<input type=checkbox name=CircularID[] value=', a.CircularID ,'>')
//$star = $lcircular ->getStar($lcircular -> CircularID);
$li->field_array = array("a.DateStart", "a.DateEnd", "a.CircularNumber ", "a.Title", "a.IssueUserName", "c.RecordStatus", "a.RecordType");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,1,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0,0);
$li->IsColOff = "displayeCircularView";
//$li->IsColOff = 2;

$pos = 0;
$li->column_list .= "<table width='96%' border='0' cellspacing='0' cellpadding='4'><tr class='tabletop'>";

$li->column_list .= "<td width='1'><FONT COLOR='#FFFFFF'>#</td>\n";              
$li->column_list .= "<td width='12%'>".$li->column(0, $i_Circular_DateStart)."</td>\n";
$li->column_list .= "<td width='12%' >".$li->column(1, $i_Circular_DateEnd)."</td>\n";
$li->column_list .= "<td width='15%' >".$li->column(2, $i_Circular_CircularNumber)."</td>\n";
$li->column_list .= "<td width='1' > </td>\n";
$li->column_list .= "<td width='21%' >".$li->column(3, $i_Circular_Title)."</td>\n";
$li->column_list .= "<td width='15%' >".$li->column(4, $i_Circular_Issuer)."</td>\n";

if($past!=2)
	$li->column_list .= "<td width='10%' >".$li->column(5, $i_Circular_SignStatus)."</td>\n";
$li->column_list .= "<td width='15%' >".$li->column(6, $i_Circular_RecipientType)."</td>\n";
//$li->column_list .= "<td width='1'>".$li->check("CircularID[]")."</td>\n";
$li->column_list .="</tr>";

$searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><td>";
$searchTag 	.= "&nbsp;<input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
$searchTag 	.= "</td></table>";

$TAGS_OBJ[] = array($Lang['Header']['Menu']['eCircular']);
$MODULE_OBJ = $lcircular->GET_MODULE_OBJ_ARR($From_eService=1);
$linterface->LAYOUT_START();

?>
<SCRIPT LANGUAGE="Javascript">
function sign(id)
{
         newWindow('/home/eService/circular/sign.php?CircularID='+id,10);
}
function view(id)
{	
	<? if($special_feature['circular_star']) {?>
         newWindow('/home/eService/circular/view.php?CircularID='+id+'&showStar='+1,10);
	<? } else {?>
		newWindow('/home/eService/circular/view.php?CircularID='+id,10);
	<? } ?>
}
</SCRIPT>

<br />
<form name="form1" method="get">
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">

<td class="tabletext" align="right" ><?=$searchTag?></td>

<tr>
	<td align="left"></td>
	<td align="right"><?=$response_msg?></td>
</tr>
<tr>
	<td align="left"><?= $filterbar; ?></td>
	
	<td align="right">
	<?// $functionbar; ?>
<? /*
	<table border="0" cellspacing="0" cellpadding="0">
	<tr>
	  <td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
	  <td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif"><table border="0" cellspacing="0" cellpadding="2">
	      <tr>
	        <td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'CircularID[]','sign.php')" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" width="12" height="12" border="0" align="absmiddle">
	          <?=$button_edit?></a></td>
	      </tr>
	    </table></td>
	  <td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
	</tr>
	</table>
*/ ?>
	</td>
</tr>

<?php 
echo $li->display($past); 

?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>