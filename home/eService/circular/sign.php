<?php
# using: Bill

################## Change Log [Start] ##############
#	Date:	2016-11-03	Villa	[U107012]
#			Add Notice number to the view page 
#
#	Date:	2015-12-04	Bill	[2015-0611-1642-49207]
#			Improved: not allow staff (except eCircular Admin) to resign after deadline
#
#	Date:	2011-08-04	YatWoon
#			HTML-editor issue (need update display description)
#
#	Date:	2011-02-23	YatWoon
#			Add "Display question number"
#
# Date:	2010-03-24	YatWoon
#		Add "All questions are required to be answered" option
#
################## Change Log [End] ##############

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!$special_feature['circular'] || $_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] || $_SESSION['UserType']!=USERTYPE_STAFF)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT($i_general_no_access_right);
	exit;
}

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lform = new libform();
$lcircular = new libcircular($CircularID);

# Check admin level
$actualTargetUserID = $UserID;
$isAdmin = false;
// [2015-0611-1642-49207]
$isCircularAdmin = false;
if ($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
{
    $adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"];
    if ($adminlevel==1 || $lcircular->IssueUserID == $UserID || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])     # Allow if Full admin or issuer
    {
        if ($targetUserID != "")
        {
        	// [2015-0611-1642-49207]
            $isCircularAdmin = true;
            $actualTargetUserID = $targetUserID;
            if ($targetUserID != $UserID)
            {
                $isAdmin = true;
            }
        }
        else
        {
            $actualTargetUserID = $UserID;
        }
    }
}
# Check Reply involved
if (!$lcircular->retrieveReply($actualTargetUserID))
{
     header("Location: $intranet_httppath/home/close.php");
     intranet_closedb();
     exit();
}
$lu = new libuser($actualTargetUserID);

$temp_que =  str_replace("&amp;", "&", $lcircular->Question);
$queString = $lform->getConvertedString($temp_que);
$queString=trim($queString)." ";

# Check View/Edit Mode
$pos = 0;
if ($isAdmin)
{
    $js_edit_mode = ($lcircular->isHelpSignAllow);
}
else
{
    $sign_allowed = false;
    $time_allowed = false;
    
	// [2015-0611-1642-49207] check if eCircular after deadline or not
	$today = time();
	$deadline = strtotime($lcircular->DateEnd);
	$afterDeadline = compareDate($today,$deadline)>0;
	
    if ($lcircular->isReplySigned())
    {
    	// [2015-0611-1642-49207] before deadline / eCircular Admin
        //if ($lcircular->isResignAllow)
        if ($lcircular->isResignAllow && ($isCircularAdmin || !$afterDeadline))
        {
            $signed_allowed = true;
        }
    }
    else
    {
        $signed_allowed = true;
    }

    //$today = time();
    //$deadline = strtotime($lcircular->DateEnd);
    //if (compareDate($today,$deadline)>0)
    if($afterDeadline)
    {
        if ($lcircular->isLateSignAllow)
        {
            $time_allowed = true;

        }
    }
    else
    {
        $time_allowed = true;
    }

    $js_edit_mode = ($signed_allowed && $time_allowed);

}

   
$temp_ans =  str_replace("&amp;", "&", $lcircular->answer);
$ansString = $lform->getConvertedString($temp_ans);
$ansString.=trim($ansString)." ";

// include_once("../../templates/fileheader.php");

$targetType = array("",$i_Circular_RecipientTypeAllStaff,
                    $i_Circular_RecipientTypeAllTeaching,
                    $i_Circular_RecipientTypeAllNonTeaching,
                    $i_Circular_RecipientTypeIndividual);

$attachment = $lcircular->displayAttachment();

$MODULE_OBJ['title'] = $i_Circular_Circular;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$reqFillAllFields = $lcircular->AllFieldsReq;
?>
<script language="javascript" src="/templates/forms/layer.js"></script>
		
<script language="Javascript">
var replyslip = '<?=$Lang['eCircular']['ReplySlip']?>';

</script>

<? if ($js_edit_mode) {
						# Fill form mode
						?>
						<script language="javascript" src="/templates/forms/form_edit.js"></script>
						<? } else { ?>
						<script language="javascript" src="/templates/forms/form_view.js"></script>
						<? } ?>
						
<script language="javascript">

// Set true if need to fill in all fields before submit
var need2checkform = <? if($reqFillAllFields){ echo "true"; } else { echo "false"; }  ?>;

function checkform(obj)
{
	 if (!need2checkform || formAllFilled)
		 {
		      return true;
		 }
		 else //return true;
		 {
		      alert("<?=$Lang['Notice']['AnswerAllQuestions']?>");
		      return false;
		 }
	
	var alert_msg = "";
	switch(obj.test.value)
	{
		case "1":
			alert_msg = "<?=$i_Circular_Alert_Sign?>";
			break;
		case "2":
			alert_msg = "<?=$i_Circular_Alert_Sign_AddStar?>";
			break;
		case "3":
			alert_msg = "<?=$i_Circular_Alert_UnStar?>";
			break;
		case "4":
			alert_msg = "<?=$i_Circular_Alert_AddStar?>";
			break;
					
	}
	
	if (!confirm(alert_msg))
		return false;
}

function click_print()
{
	with(document.form1)
	{
		action = "print_preview.php";
		target = "_blank";
		submit();
		action = "sign_update.php";
		target = "_self";
	}
    
}
</script>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_DateStart?></span>
					</td>
					<td class="tabletext"><?=$lcircular->DateStart?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_DateEnd?></span>
					</td>
					<td class="tabletext"><?=$lcircular->DateEnd?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_Title?></span>
					</td>
					<td class="tabletext"><?=$lcircular->Title?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
					<span class="tabletext"><?=$i_Circular_CircularNumber?></span>
					</td>
					<td class="tabletext"><?=$lcircular->CircularNumber?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_Description?></span>
					</td>
					<td class="tabletext"><?=htmlspecialchars_decode($lcircular->Description)?></td>
				</tr>
				<? if ($attachment != "") { ?>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_Attachment?></span>
					</td>
					<td class="tabletext"><?=$attachment?></td>
				</tr>
				<? } ?>
				<?
					$issuer = $lcircular->returnIssuerName();
				?>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_Issuer?></span>
					</td>
					<td class="tabletext"><?=$issuer?></td>
				</tr>
				<?
					if (!$lcircular->isReplySigned())
					{
						$statusString = "$i_Circular_Unsigned";
					}
					else
					{
						$signer = $lu->getNameWithClassNumber($lcircular->signerID);
						$signby = ($lcircular->replyType==1? $i_Circular_Signer:$i_Circular_Editor);
						$statusString = "$i_Circular_Signed <i>($signby $signer $i_Circular_At ".$lcircular->signedTime.")</i>";
					}
				?>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_SignStatus?></span>
					</td>
					<td class="tabletext"><?=$statusString?></td>
				</tr>
				<? if ($isAdmin) { ?>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_Recipient?></span>
					</td>
					<td class="tabletext"><?=$lu->getNameWithClassNumber($actualTargetUserID)?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_RecipientType?></span>
					</td>
					<td class="tabletext"><?=$targetType[$lcircular->RecordType]?></td>
				</tr>
				<? } ?>
				
				<? if ($reqFillAllFields) { ?>
				<tr>
					<td>&nbsp;</td>
					<td>[<?=$i_Survey_AllRequire2Fill?>]</td>
				</tr>
				<? } ?>
			</table>
		</td>
	</tr>
	<tr>
		<td>	
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/scissors_line.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/icons_scissors.gif"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr valign="top">
					<td class='eNoticereplytitle' align='center'><?=$Lang['eCircular']['ReplySlipForm']?></td>
				</tr>
				<tr valign="top">
					<td class="tabletext">
						
						<form name="ansForm" method="post" action="update.php">
							<input type=hidden name="qStr" value="">
							<input type=hidden name="aStr" value="">
						</form>
						<script language="Javascript">
						<?=$lform->getWordsInJS()?>
							background_image = "/images/layer_bg.gif";
							<? if ($js_edit_mode) {
							# Fill form mode
							?>
							var DisplayQuestionNumber = '<?=$lcircular->DisplayQuestionNumber?>';
							var sheet= new Answersheet();
							// attention: MUST replace '"' to '&quot;'
							sheet.qString="<?=$queString?>";
							sheet.aString="<?=$ansString?>";	
							
							//edit submitted application
							sheet.mode=1;
							sheet.answer=sheet.sheetArr();
							//sheet.templates=form_templates;
							document.write(editPanel());
							
							<? } else {
							# View answer only
							?>
							myQue="<?=$queString?>";
							myAns="<?=$ansString?>";
							document.write(viewForm(myQue, myAns));
						<? } ?>			
						</script>
						<script language="javascript">
							function copyback(option)
							{
								//Sign & Close
								if (option == 1)
								{
									document.form1.test.value = 1;
								}
								//Sign & Add Star
								else if(option == 2)
								{
									document.form1.test.value = 2;
								}
								//Unstar
								else if (option == 3)
								{
									document.form1.test.value = 3;
								}
								//Add Star
								else if (option == 4)
								{
									document.form1.test.value = 4;
								}
								
								//if(document.form1.qStr.value.length > 0) 
								//{
									finish();
									document.form1.qStr.value = 							   document.ansForm.qStr.value;
									document.form1.aStr.value = 							   document.ansForm.aStr.value;
								//}
								
							}

						</script>
							
							
							
						<? if ($js_edit_mode) {
							echo ($lcircular->replyStatus==2? $i_Circular_SignInstruction2:$i_Circular_SignInstruction);
						} ?>
						
					</td>
				</tr>
				<tr>
					<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
				<form name="form1" action="sign_update.php" method="post" onSubmit="return checkform(this)">
				<tr>
					<td align="center" colspan="2">
						<? if ($js_edit_mode) { ?>
						<!--?= $linterface->GET_ACTION_BTN($i_Circular_Sign, "submit", "copyback();","submit") ?-->

							<?= $linterface->GET_ACTION_BTN($eCircular["SignClose"], "submit", "copyback(1);","submitbtn") ?>
							<? if($special_feature['circular_star']) {?>
							<?= $linterface->GET_ACTION_BTN($eCircular["SignAddStar"], "submit", "copyback(2);","submitbtn") ?>
							<? } ?>

						<? }else{ ?>
								<? if($special_feature['circular_star']) {?>
								<?//Check whether circular is starred
								$star = $lcircular->getStar($lcircular->CircularID);

								if ($star == 1){?>
									<?= $linterface->GET_ACTION_BTN($eCircular["RemoveStar"], "submit", "copyback(3);","submitbtn") ;}
								else{?>
									<?= $linterface->GET_ACTION_BTN($eCircular["AddStar"], "submit", "copyback(4);","submitbtn") ;}}?>
									<? } ?>
							<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "click_print();",							   "cancelbtn") ?>
								
							<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn") ?>

						<input type=hidden name=test value="">
						<input type=hidden name=qStr value="">
						<input type=hidden name=aStr value="">
						<input type=hidden name=CircularID value="<?=$CircularID?>">
						<input type=hidden name=actualTargetUserID value="<?=$actualTargetUserID?>">
						<input type=hidden name=adminMode value="<?=($isAdmin? "1":"0")?>">
					</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>
<?php

intranet_closedb();
$linterface->LAYOUT_STOP();
?>