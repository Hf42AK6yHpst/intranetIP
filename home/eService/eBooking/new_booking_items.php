<?php
// using :
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.marcus.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$linterface 	= new interface_html("popup3.html");
$lebooking_ui 		= new libebooking_ui();
$lebooking_ui->Check_Page_Permission($AdminOnly=0);

$linterface->LAYOUT_START($ReturnMsg);

echo $lebooking_ui->Get_New_Booking_Step1_Items_Thickbox_UI();

?>
<script type="text/javascript" src="../../../templates/jquery/jquery-1.3.2.min.js"></script>
<script>
function refreshSubCat()
{
	ShowHideSubCatSelect();
	$.post(
		"ajax_task.php",
		{
			task:"LoadMultiSubCatTable",
			CategoryID: $("#Category").val()
		},
		function(data)
		{
			$("td#SubCatList").html(data);
			refreshItem();
		}
	);
}

function refreshItem(IsSearch)
{
	var ctr=0;
	var IDList = '';
	var Keyword = IsSearch==1?$("#Keyword").val():'';
	$("input.SubCatCheckBox:checked").each(function(){
		IDList+= ctr++>0?',':'';
		IDList+= $(this).val();
	});
	$.post(
		"ajax_task.php",
		{
			task:"LoadBookingItemsList",
			CategoryID: $("#Category").val(),
			SubCatIDList: IDList,
			Keyword: Keyword
		},
		function(data)
		{
			$("div#ItemShowcase").html(data);
		}
	);
}

var SelectedItemID = new Array();
var SelectedItemName = new Array();
//var SelectedItemID;
//var SelectedItemName;
function SelectItem(obj, SelectedID, ItemName)
{
	
	if(!$("#"+SelectedID).hasClass("booking_table_item_select"))
	{
		$("#"+SelectedID).toggleClass("booking_table_item_select","booking_table_item");
		$("#"+SelectedID).removeClass('booking_table_item').addClass('booking_table_item_select');
		SelectedItemID.push(SelectedID);
		SelectedItemName.push(ItemName);
	}
	else
	{
		$("#"+SelectedID).toggleClass("booking_table_item","booking_table_item_select");
		$("#"+SelectedID).removeClass('booking_table_item_select').addClass('booking_table_item');
		var idx = SelectedItemID.indexOf(SelectedID);
		if(idx!=-1){
			SelectedItemID.splice(idx, 1);
		} 
		var idx2 = SelectedItemName.indexOf(ItemName);
		if(idx2!=-1){
			SelectedItemName.splice(idx2, 1);
		}
	}
	
	/*
	if(!$(obj).hasClass("booking_table_item_select"))
	{
		$("a.booking_table_item_select").toggleClass("booking_table_item_select","booking_table_item");
		$(obj).removeClass('booking_table_item').addClass('booking_table_item_select');
	}
	
	SelectedItemID = SelectedID;
	SelectedItemName = ItemName;
	*/
	
}

function ShowHideSubCatSelect()
{
	$("#Category").val()>0?$("#SubCatSelectTD").show():$("#SubCatSelectTD").hide();
}

function ConfirmItem()
{
	var str_SelectedItemID = SelectedItemID.join(",");
	var str_SelectedItemName = SelectedItemName.join("<br>");
	/*
	if(parent.document.frm1.FacilityID.value!=''&&parent.document.frm1.FacilityID.value!=SelectedItemID)
	{
		if(!parent.ConfirmChange()) 
			return false;
	}
	else
	{
		parent.SkipRemoveDate = true;	
	}
	*/
	
	//parent.refreshItemCalendar(SelectedItemID, SelectedItemName);
	parent.refreshItemCalendar(str_SelectedItemID, str_SelectedItemName);
	parent.tb_remove();
}

$().ready(function() {
	refreshSubCat();
});
</script>


<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
