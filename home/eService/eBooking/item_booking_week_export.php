<?php
// using : 
/*
 *  2019-05-02 Cameron
 *  - fix potential sql injection problem by cast related variables to integer
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lebooking_ui = new libebooking_ui();
$linventory = new libinventory();
$li = new libfilesystem();
$lexport = new libexporttext();

$CurrentPageArr['eBooking'] = 1;

// Empty row
$Rows[] = array('');

$CategoryID = IntegerSafe($CategoryID);
$SubCategoryID = IntegerSafe($SubCategoryID);

$sql = "SELECT CONCAT(".$linventory->getInventoryNameByLang("cat.").",' > ',".$linventory->getInventoryNameByLang("subcat.").") FROM INVENTORY_CATEGORY as cat INNER JOIN INVENTORY_CATEGORY_LEVEL2 as subcat ON (cat.CategoryID = subcat.CategoryID) WHERE cat.CategoryID = '$CategoryID' AND subcat.Category2ID = '$SubCategoryID'";
$arrCategory = $lebooking_ui->returnVector($sql);
if(sizeof($arrCategory) > 0){
	$tempRows = array($Lang['eBooking']['Settings']['FieldTitle']['Category'],
						$arrCategory[0]);
}
$Rows[] = $tempRows;

if($ManagementGroup == ""){
	$tempRows = array($Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'],
						$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllManagementGroup']);
}else{
	if($ManagementGroup != "-999"){
	    $ManagementGroup = IntegerSafe($ManagementGroup);
		$sql = "SELECT GroupName FROM INTRANET_EBOOKING_MANAGEMENT_GROUP WHERE GroupID = '$ManagementGroup'";
		$arrMgmtGroup = $lebooking_ui->returnVector($sql);
		$tempRows = array($Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'],
							$arrMgmtGroup[0]);
	}else{
		$tempRows = array($Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'],
							$Lang['eBooking']['Settings']['FieldTitle']['NoMgmtGroup']);
	}
}
$Rows[] = $tempRows;

if(sizeof($BookStatus) > 0) {
    $BookingStatusList = IntegerSafe($BookingStatusList);
	$BookingStatusArr = explode(",",$BookingStatusList); 
	$show_wait = in_array(0,$BookingStatusArr)?"1":"0";
	$show_approve = in_array(1,$BookingStatusArr)?"1":"0";
	$show_reject = in_array(-1,$BookingStatusArr)?"1":"0";
	$show_tempory = in_array(999,$BookingStatusArr)?"1":"0";

	if($show_wait){
		$booking_status_content[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval'];
	}
	if($show_approve){
		$booking_status_content[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved'];
	}
	$str_booking_status = implode(" , ",$booking_status_content);
	$tempRows = array($Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status'],
						$str_booking_status);
	$Rows[] = $tempRows;
}

// Empty row
$Rows[] = array('');

$FacilityID = IntegerSafe($FacilityID);
if(($FacilityID == "")) {
	$FacilityID = "ALL";
}

$exportColumn = array($Lang['eBooking']['Management']['ItemBooking']['FieldTitle']['ReportTitle']['ItemBookingWeek'],'','','','','','','','','','','','','','');

// csv main content
$csv_content = $lebooking_ui->Show_Item_Booking_Week_Export($WeekStartTimeStamp,$CategoryID,$SubCategoryID,$FacilityID,$ManagementGroup,$BookingStatusList);

// combine additional information and main content
$final_csv_content = array_merge($Rows,$csv_content);

$export_content = $lexport->GET_EXPORT_TXT($final_csv_content, $exportColumn, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "item_booking_week".date('Y-m-d').".csv";
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>