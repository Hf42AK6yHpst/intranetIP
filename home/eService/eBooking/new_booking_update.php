<?php
// using : 

############################################ Change Log ############################################
##
##  Date    : 2019-05-02 (Cameron)
##              fix potential sql injection problem by cast related variables to integer and enclosed them with apostrophe
##
##	Date	:	2010-06-09 (Ronald)
##	Details	:	Add a process to delete the current booking for Edit Booking
##

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentBookingID = IntegerSafe($_REQUEST['CurrentBookingID']);

$FacilityType = IntegerSafe($_REQUEST['FacilityType']);
$ItemID = IntegerSafe($_REQUEST['ItemID']);
$RoomID = $_REQUEST['RoomID'];
$ResponsibleUID = $_REQUEST['ResponsibleUID'];
$SingleItemIDArr = $_REQUEST['SingleItemID'];
$SettingsArr['CancelDayBookingIfOneItemIsNA'] = $_REQUEST['CancelDayBookingIfOneItemIsNA'];
$BookingRemarks = stripslashes(cleanHtmlJavascript($_REQUEST['BookingRemarks']));

$iCalenderEvent = IntegerSafe($_REQUEST['iCalEvent']);

$StartTime_TimeSlotID_Mapping_Arr = unserialize(rawurldecode($_REQUEST['StartTime_TimeSlotID_Mapping_Arr']));

$lebooking = new libebooking();
if ($FacilityType == 2)	// item
{
	if($SelectMethod == "Specific")
		$TimeRangeArr = $_REQUEST['SpecificTimeRangeArr'];
	else
		$TimeRangeArr = $_REQUEST['TimeRangeArr'];
	$DateList = $_REQUEST['DateList'];
	$DateArr = explode(',', $DateList);
	
	list($StartTimeArr, $EndTimeArr, $StartTime_TimeSlotID_Mapping_Arr) = $lebooking->Get_StartTime_EndTime_TimeSlotID_Array_By_TimeRange_Array($TimeRangeArr);

	$TmpTimeArr = $lebooking->Get_Room_Booking_DateTimeArray_And_TimeClashArray($RoomID, $DateArr, $StartTimeArr, $EndTimeArr);
	$DateTimeArr = $TmpTimeArr[0];
}
else if ($FacilityType == 1)	// location
{
	$DateTimeArr = unserialize(rawurldecode($_REQUEST['DateTimeArr']));
}

### Prepare iCalendar Event array
if($iCalenderEvent == 1) {
	include_once($PATH_WRT_ROOT."includes/libinventory.php");
	$linventory = new libinventory();
	
	if($ResponsibleUID == "") {
		$OwnerID = $_SESSION['UserID'];
	} else {
		$OwnerID = $ResponsibleUID;
	}
	
	if($RoomID != "") {
		$sql = "SELECT 
					CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
				FROM
					INVENTORY_LOCATION_BUILDING AS building
					INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
					INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
				WHERE 
					room.LocationID = '$RoomID'";
		$arrLocation = $linventory->returnVector($sql);
		$location = $arrLocation[0];
	} else {
		$location = "";
	}
	
	$iCalEventArr[0]['Owner'] = $OwnerID;
	$iCalEventArr[0]['EventTitle'] = $iCalEventTitle;
	$iCalEventArr[0]['EventDesc'] = $BookingRemarks;
	$iCalEventArr[0]['Location'] = $location;
	//$lebooking->CreatePersonalCalenderEvent($OwnerID,$DateTimeArr,$StartTime_TimeSlotID_Mapping_Arr,$iCalEventTitle,$BookingRemarks,$RoomID);
}

if($CurrentBookingID != "")
{
	### Remove the existing Booking Request
	$sql = "DELETE FROM INTRANET_EBOOKING_RECORD WHERE BookingID = '$CurrentBookingID'";
	$lebooking->db_db_query($sql);
	
	$sql = "SELECT MIN(BookingID) FROM INTRANET_EBOOKING_RECORD WHERE RelatedTo = '$CurrentBookingID'";
	$NewRelatedTo = $lebooking->returnVector($sql);
	
	$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RelatedTo = ".$NewRelatedTo[0]." WHERE RelatedTo = '$CurrentBookingID'";
	$lebooking->db_db_query($sql);
	
	$sql = "SELECT EventID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID = '$CurrentBookingID'";
	$arrExistingEvent = $lebooking->returnVector($sql);
	
	if(sizeof($arrExistingEvent) > 0) {
		$sql = "DELETE FROM CALENDAR_EVENT_ENTRY WHERE EventID = '".$arrExistingEvent[0]."'";
		$lebooking->db_db_query($sql);
		
		$sql = "DELETE FROM INTRANET_EBOOKING_CALENDAR_EVENT_DETAILS WHERE EventID = '".$arrExistingEvent[0]."'";
		$lebooking->db_db_query($sql);
		
		$sql = "DELETE FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID = '".$CurrentBookingID."'";
		$lebooking->db_db_query($sql);
	}
}

### Create Booking Request
if ($FacilityType == 1)	// location
	$SuccessArr['New_Room_Booking'] = $lebooking->New_Room_Booking($RoomID, $DateTimeArr, $StartTime_TimeSlotID_Mapping_Arr, $SingleItemIDArr, $BookingRemarks, $ResponsibleUID, $SettingsArr, $iCalEventArr);
else if ($FacilityType == 2)	// item
	$SuccessArr['New_Item_Booking'] = $lebooking->New_Item_Booking($ItemID, $DateTimeArr, $StartTime_TimeSlotID_Mapping_Arr, $BookingRemarks, $ResponsibleUID, $iCalEventArr);


if (in_array(false, $SuccessArr))
	$ReturnMsgKey = 'NewBookingFailed';
else
	$ReturnMsgKey = 'NewBookingSuccess';
	
intranet_closedb();
header("Location: index_list.php?ReturnMsgKey=$ReturnMsgKey");
?>