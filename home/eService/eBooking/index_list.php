<?php
// Using:
@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);

################################## Change Log ##################################

##  2019-05-02 (Cameron) fix: change $keyword to $Keyword when apply cleanHtmlJavascript
##              - fix potential sql injection problem by cast related variables to integer
##
##	2018-08-15 (Henry) fixed XSS
##
##	2016-12-28 (Villa) #A101078 - modified EditBookingRequest Allowed to edit Remarks & Attachment even if the booking is approved 
##
##  2015-04-21 (Omas) modified to add cookies for booking category
##
##	2015-01-16 (Omas) new Setting - Booking Category enable by $sys_custom['eBooking_BookingCategory']
##
##  2014-01-16 (Tiffany) - add 	function CheckDate() to check the invalid choosed date.
##
##	2011-04-01 (Marcus) - fix show empty list after submit new booking
##
##	2011-02-02 (Carlos) - fix status filter js function UpdateStatusList()
##
##	Date	:	2010-06-09 (Ronald)
##	Details :	add new JS function EditBookingRequest(), handle "Edit request", "Delete request", "Cancel request"
##

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_FacilityType", "FacilityType");
$arrCookies[] = array("ck_BookingStatusList", "BookingStatusList");
$arrCookies[] = array("ck_BookingDateStatus", "BookingDateStatus");
$arrCookies[] = array("ck_eBooking_My_Booking_Record_Keyword", "Keyword");	
$arrCookies[] = array("ck_pageNo", "pageNo");	
$arrCookies[] = array("ck_numPerPage", "numPerPage");	
$arrCookies[] = array("ck_eBooking_My_Booking_Record_SelectedStartDate", "SelectedStartDate");	
$arrCookies[] = array("ck_eBooking_My_Booking_Record_SelectedEndDate", "SelectedEndDate");
$arrCookies[] = array("ck_eBooking_My_Booking_Record_Category", "BookingCategoryID");

if (isset($FacilityType)) {
    $FacilityType = IntegerSafe($FacilityType);
}
if (isset($BookingStatusList)) {
    $BookingStatusList = IntegerSafe($BookingStatusList);
}
if (isset($BookingDateStatus)) {
    $BookingDateStatus = IntegerSafe($BookingDateStatus);
}

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
	updateGetCookies($arrCookies);

intranet_auth();
intranet_opendb();

# control reserve right in booking process
$_SESSION['EBOOKING_BOOKING_FROM_EADMIN'] = 0; 

$CurrentPage	= "eService_MyBookingRecord";
$linterface 	= new interface_html();
$lebooking_ui 	= new libebooking_ui();
$lebooking_ui->Check_Page_Permission($AdminOnly=0);

$TAGS_OBJ[] = array($Lang['eBooking']['eService']['FieldTitle']['MyBookingRecord']);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR($From_eService=1);

$ReturnMsg = $Lang['eBooking']['eService']['ReturnMsg'][$ReturnMsgKey];

$linterface->LAYOUT_START($ReturnMsg);

# get from cookies now
$Keyword = trim($Keyword)!=''? trim(stripslashes($Keyword)) : '';
$FacilityType = trim($FacilityType)!=''? $FacilityType : 0;
$BookingStatusList = trim($BookingStatusList)!=''? $BookingStatusList : '1,0,-1';
$BookingDateStatus = trim($BookingDateStatus)!=''? $BookingDateStatus : 2;
$Keyword = trim($Keyword)!=''? trim(stripslashes($Keyword)) : '';
$SelectedStartDate = trim($SelectedStartDate)!=''? trim(stripslashes($SelectedStartDate)) : '';
$SelectedEndDate = trim($SelectedEndDate)!=''? trim(stripslashes($SelectedEndDate)) : '';
$Keyword = cleanHtmlJavascript($Keyword);

echo $lebooking_ui->initJavaScript();
echo $lebooking_ui->Get_My_Booking_List_UI($Keyword, $FacilityType, $BookingStatusList, $BookingDateStatus, $SelectedStartDate, $SelectedEndDate,$BookingCategoryID);
?>

<script language="javascript">
$(document).ready( function() {
	
});

function js_ShowHideRemark(layer_id, act)
{
	if(act == "show"){
		$("#Remark_"+layer_id).show();
	}else{
		$("#Remark_"+layer_id).hide();
	}
}

function js_Show_Detail_Layer(jsDetailType, jsBookingID, jsClickedObjID, jsFacilityType, jsFacilityID)
{
	js_Hide_Detail_Layer();
	
	var jsAction = '';
	if (jsDetailType == 'Remark')
		jsAction = 'Load_Remark';
	else
		jsAction = 'Load_RejectReason';
	
	$('div#RemarkLayerContentDiv').html('<?=$Lang['General']['Loading']?>');	
	js_Change_Layer_Position(jsClickedObjID);
	MM_showHideLayers('RemarkLayer','','show');
			
	$('div#RemarkLayerContentDiv').load(
		"ajax_task.php", 
		{ 
			task: jsAction,
			BookingID: jsBookingID,
			FacilityType: jsFacilityType,
			FacilityID: jsFacilityID
		},
		function(returnString)
		{
			$('div#RemarkLayerContentDiv').css('z-index', '999');
		}
	);
}

function js_Hide_Detail_Layer()
{
	MM_showHideLayers('RemarkLayer','','hide');
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function js_Change_Layer_Position(jsClickedObjID) {
	
	var jsOffsetLeft, jsOffsetTop;
	
	jsOffsetLeft = 450;
	jsOffsetTop = -15;
		
	var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') - jsOffsetLeft;
	var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') - jsOffsetTop;
	
	document.getElementById('RemarkLayer').style.left = posleft + "px";
	document.getElementById('RemarkLayer').style.top = postop + "px";
	document.getElementById('RemarkLayer').style.visibility = 'visible';
}

function js_Go_New_Booking()
{
	var ObjForm = document.getElementById('form1');
	ObjForm.action = '<?=$PATH_WRT_ROOT?>home/eAdmin/ResourcesMgmt/eBooking/management/reserve_step1.php';
	ObjForm.submit();
}

function refreshBookRecord()
{
	MM_showHideLayers('status_option','','hide');
	document.form1.submit();
}

function UpdateStatusList()
{
	var StatusListAry = new Array();
	var check_box_checked = 0;
	
	$("input.StatusCheckBox:checked").each(function(){
		StatusListAry.push($(this).val());
		check_box_checked++;
	});

	$("#BookingStatusList").val(StatusListAry.join(","));
	if(check_box_checked == 0){
		alert("<?=$Lang['eBooking']['Management']['General']['JSWarning']['PleaseSelectStatus'];?>");
	}else{
		refreshBookRecord();
	}
}

function EditBookingRequest(action, page)
{
	var js_arr_booking_id = new Array();
		
	for(i=0; i<document.getElementsByName("BookingID[]").length; i++)
	{
		if( document.getElementsByName("BookingID[]")[i].checked )
		{
			js_arr_booking_id[js_arr_booking_id.length] = document.getElementsByName("BookingID[]")[i].value;
			
		}
	}
	if(js_arr_booking_id.length>0)
	{
		if(action == "Edit")
		{
			// Edit
			if(js_arr_booking_id.length == 1)
			{
				$.post(
					"ajax_task.php",
					{
						"task"			:	"EditBookingRequest",
						"strBookingID" 	:	js_arr_booking_id.toString()
					},
					function(responseText)
					{
						if(responseText == 1 || responseText == -1){ //Allowed to edit Remarks & Attachment even if the booking is approved 
							document.form1.action=page;
				            document.form1.submit();
						}else{
							alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['BookingRequestCannotEdit'];?>");
						}
					}
				);
			}
			else
			{
				alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['PleaseSelectOneRequestOnly'];?>");
			}
		}
		else if(action == "Cancel")
		{
			// Cancel
			$.post(
				"ajax_task.php",
				{
					"task"			:	"CancelBookingRequest",
					"strBookingID" 	:	js_arr_booking_id.toString()
				},
				function(responseText)
				{
					if(responseText == 1){
						if(confirm("<?=$Lang['eBooking']['eService']['MyBookingRecord']['JSWarning']['ConfirmCancel'];?>")) {
							page = page+'?action=CancelBookingRequest';
							document.form1.action=page;
							document.form1.submit();
						} else {
							return false;
						}
					} else {
						alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['BookingRequestCannotCancel'];?>");
					}
				}
			);
		}
		else
		{
			// Delete
			$.post(
				"ajax_task.php",
				{
					"task"			:	"DeleteBookingRequest",
					"strBookingID" 	:	js_arr_booking_id.toString()
				},
				function(responseText)
				{
					if(responseText == 1){
						if(confirm("<?=$Lang['eBooking']['eService']['MyBookingRecord']['JSWarning']['ConfirmDelete'];?>")) {
							page = page+'?action=DeleteBookingRequest';
							document.form1.action=page;
							document.form1.submit();
						} else {
							return false;
						}
					} else {
						alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['BookingRequestCannotDelete'];?>");
					}
				}
			);
		}
	}
	else
	{
		alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['PleaseSelectAtLeaseOneRequest'];?>");
	}
}

function js_Go_Export() {
	$('form#form1').attr('action', 'export.php').submit().attr('action', 'index_list.php');
}

function CheckDate(obj){
	if (obj.SelectedStartDate.value > obj.SelectedEndDate.value){
		alert("<?=$Lang['eBooking']['jsWarningArr']['InvalidSearchPeriod']?>");
		return false;			
	}
	else{		
		form1.submit();
	}
		
}
</script>



<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>