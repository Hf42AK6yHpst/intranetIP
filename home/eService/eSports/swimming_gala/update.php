<?php
# editing by:
/***************************************************************************
*	modification log:
*	2013-09-02	Roy
*		- add trackCount
*
***************************************************************************/
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");

intranet_auth();
intranet_opendb();
$lswimminggala = new libswimminggala();
if (!$lswimminggala->inEnrolmentPeriod())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$typeArr = explode(",", $types);

$trackCount = $_REQUEST["tc"];

#Clear the old records 
$sql = "DELETE FROM SWIMMINGGALA_STUDENT_ENROL_EVENT WHERE StudentID = '$UserID'";
$lswimminggala->db_db_query($sql);

$face_full_quota = 0;

# Insert Restrict Quota Event Enrolment Record(s)
for($i=0; $i<sizeof($typeArr); $i++)
{
	$t = $typeArr[$i];
	$tempArr = ${RQ_EventGroup.$t};
	$de = "";
	$values = "";
	for($j=0; $j<sizeof($tempArr); $j++)
	{
		$egid = $tempArr[$j];
		
		# check the event is full or not
        $cur_enroled = $lswimminggala->returnEventEnroledNo($egid, $UserID);
        $eventQuota = $lswimminggala->returnEventQuota($egid);
        
        if($cur_enroled < $eventQuota || !$eventQuota)	
        {
//			$values .= $de."(".$UserID.", ".$egid.", now(), ".$UserID.")";
//			$de = ",";
			$values = "(".$UserID.", ".$egid.", now(), ".$UserID.")";
			
			$fields = "(StudentID, EventGroupID, DateModified, InputBy)";
			$sql = "INSERT INTO SWIMMINGGALA_STUDENT_ENROL_EVENT $fields VALUES $values";
			$lswimminggala->db_db_query($sql);
			
//			${totalCount.$t} = sizeof($tempArr);
			${totalCount.$t} = sizeof($tempArr);
        }
        else
		{
			$face_full_quota = 1;
		}
	}
		
	
}

// $totalCount1 = sizeof($EventGroup1);

###############
# Insert Unrestrict Quota Event Enrolment Record(s)
$de = "";
$values = "";
for($i=0; $i<sizeof($UQ_EventGroup); $i++)
{
	$egid = $UQ_EventGroup[$i];
	
	# check the event is full or not
    $cur_enroled = $lswimminggala->returnEventEnroledNo($egid, $UserID);
    $eventQuota = $lswimminggala->returnEventQuota($egid);
    
    if($cur_enroled < $eventQuota || !$eventQuota)	
    {
//		$values .= $de."(".$UserID.", ".$egid.", now(), ".$UserID.")";
//		$de = ",";
		$values = "(".$UserID.", ".$egid.", now(), ".$UserID.")";
		
		$fields = "(StudentID, EventGroupID, DateModified, InputBy)";
		$sql = "INSERT INTO SWIMMINGGALA_STUDENT_ENROL_EVENT $fields VALUES $values";
		$lswimminggala->db_db_query($sql);
    }
    else
	{
		$face_full_quota = 1;
	}
}
#####################

#Check Record Exist Or Not
$sql = "SELECT COUNT(*) FROM SWIMMINGGALA_STUDENT_ENROL_INFO WHERE StudentID = '$UserID'";
$exist_info = $lswimminggala->returnVector($sql);

if($exist_info[0] == 0) 
{
	#Insert New Record To SWIMMINGGALA_STUDENT_ENROL_INFO
	$fields1 = "(StudentID, TrackEnrolCount, DateModified)";
	$values1 = "(".$UserID.", ".$trackCount.", now())";
	$sql = "INSERT INTO SWIMMINGGALA_STUDENT_ENROL_INFO $fields1 VALUES $values1";
	$lswimminggala->db_db_query($sql);
}
else
{
	// #Retrieve Amount of Events that the Student Have Enroled
	// $sql = "SELECT TrackEnrolCount FROM SWIMMINGGALA_STUDENT_ENROL_INFO WHERE StudentID = '$UserID'";
	// $temp = $lswimminggala->returnArray($sql, 1);
	// if(sizeof($temp) != 0)
	// {
		// $trackCount = $temp[0][0];
		// $fieldCount = $temp[0][1];
	// }
	// else
	// {
		// $trackCount = 0;
		// $fieldCount = 0;
	// }

	#Update Existing Record in SWIMMINGGALA_STUDENT_ENROL_INFO
	$sql = "UPDATE SWIMMINGGALA_STUDENT_ENROL_INFO SET TrackEnrolCount = '$trackCount', DateModified = now() WHERE StudentID = '$UserID'";
	$lswimminggala->db_db_query($sql);
}
intranet_closedb();
if($face_full_quota)
	$xmsg = "UpdatePartiallySuccess";
else
	$xmsg = "UpdateSuccess";
header ("Location: index.php?xmsg=$xmsg");
?>