<?php
# Using:

######################################
#
#	Date:   2019-05-14 (Bill)
#           prevent SQL Injection
#
#	Date: 	2017-09-14	Bill	[2017-0906-1708-05236]
# 			-  Support Enrolment Restriction
#
#	Date: 	2013-09-02	Roy
#			- get MaxTotal, MaxTrack, MaxField from age group first, then from rules
#			- POST hidden input tc to update page
#
#	Date:	2012-06-13	YatWoon
#			Improved: add event quota checking
#
#	Date:	2012-04-03	YatWoon
#			Fixed: js error cause cannot check the max enrolment checking [Case#2012-0327-1109-10066] 
#			Improved: change the js alert wordings from $i_general_PerPage to $Lang['eSports']['per']
#			Fixed: broken layout
#
######################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$lswimminggala	= new libswimminggala();
$CurrentPage	= "PageEnroll";

if (!$lswimminggala->inEnrolmentPeriod())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

# Get this student info (Group)
# Get Event available for enrolment
# Show Enrolment details
$names = $lswimminggala->retrieveEventTypeName();
$txt_track = $names[0];
$txt_field = $names[1];

# Retrieve Athletic Number
$sql = "SELECT AthleticNum FROM SWIMMINGGALA_STUDENT_ENROL_INFO WHERE StudentID = '$UserID'";
$athleticNum = $lswimminggala->returnArray($sql,1);

# Retrieve Student Info
$sql = "SELECT DateOfBirth, Gender FROM INTRANET_USER WHERE UserID = '$UserID'";
$temp = $lswimminggala->returnArray($sql,2);
$gender = $temp[0][1];
$namefield = ($intranet_session_language == "b5") ? "ChineseName" : "EnglishName";

# [2017-0906-1708-05236] Check student is allow for enrol or not
$sql = "SELECT Reason FROM SWIMMINGGALA_ENROL_RESTRICTION_LIST WHERE UserID = '$UserID'";
$restrict_reason = $lswimminggala->returnVector($sql);
$can_apply = $restrict_reason[0]? 0 : 1; 

if($temp[0][0]=="0000-00-00 00:00:00")
{
	$have_DOB = 0;
	$targetGroupID = "";
	$targetGroupName = "N/A";
}
else
{
	$have_DOB = 1;
	$birthArr = explode(" ", $temp[0][0]);
	$birth = trim($birthArr[0]);
    
	# Retrieve Group Info
	$sql = "SELECT AgeGroupID, $namefield, DOBLowLimit, DOBUpLimit, EnrolMaxTrack FROM SWIMMINGGALA_AGE_GROUP WHERE Gender = '$gender'";
	$groups = $lswimminggala->returnArray($sql,5);

	# Check which Group the Student Belonged to
	for($i=0; $i<sizeof($groups); $i++)
	{
        list($gid, $gname, $lowDOB, $upDOB, $gMaxTrack) = $groups[$i];
        if($upDOB == NULL)
        {
                if($birth <= $lowDOB)
                {
                        $targetGroupID = $gid;
                        $targetGroupName = $gname;
						$maxTrack = $gMaxTrack;
                        break;
                }
        }
        else if($lowDOB == NULL)
        {
                if($birth >= $upDOB)
                {
                        $targetGroupID = $gid;
                        $targetGroupName = $gname;
						$maxTrack = $gMaxTrack;
                        break;
                }
        }
        else
        {
                if(($birth <= $lowDOB) && ($birth >= $upDOB))
                {
                        $targetGroupID = $gid;
                        $targetGroupName = $gname;
						$maxTrack = $gMaxTrack;
                        break;
                }
        }
	}
}

# Retrieve Enrollment Rules
# 1 - Track
$rules = $lswimminggala->retrieveEnrolmentRules();
list($RuleMaxTrack, $details) = $rules[0];
if (!isset($maxTrack)) {
	$maxTrack = $RuleMaxTrack;
}

# Retrieve Enroled EventGroup IDs
$sql = "SELECT EventGroupID FROM SWIMMINGGALA_STUDENT_ENROL_EVENT WHERE StudentID = '$UserID'";
$enroledID = $lswimminggala->returnArray($sql, 1);

### Title ###
//$TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/polling/icon_currentpolls.gif' align='absmiddle' />";
$TitleTitle1 = "<span class='contenttitle'>". $i_Water_Gala_Enrolment ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleImage1.$TitleTitle1."</td></tr></table>";
$TAGS_OBJ[] = array($TitleTitle,"");
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$CurrentPageArr['eWaterGala'] 			= 0;
$CurrentPageArr['eServiceeWaterGala'] 	= 1;

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<SCRIPT LANGUAGE=Javascript>
var maxTrack = "<?=$maxTrack?>";
var track = '<?=$txt_track?>';
</SCRIPT>

<?php
if($have_DOB==1 && $can_apply)
{
	# Retrieve Restrict Quota Track and Field Events that can be Enrolled
	$RQ_eventgroup = $lswimminggala->retrieveRestrictQuotaEventGroupInfo($targetGroupID, $UserID);

	# Retrieve Unrestrict Quota Track and Field Events that can be Enrolled
	$UQ_eventgroup = $lswimminggala->retrieveUnrestrictQuotaEventGroupInfo($targetGroupID, $UserID);
	
	
			 ###################### Restrict Quota Events #######################

			 $currType = "";
			 $displayHTML = "";
			 for($i=0; $i<sizeof($RQ_eventgroup); $i++)
			 {
					list($eg_id, $event_id, $event_name, $type_id, $type_name, $group_id, $event_quota) = $RQ_eventgroup[$i];

					$ext_exist = $lswimminggala->checkExtInfoExist($type_id, $eg_id);
					if($ext_exist == 1)
					{
							if($currType != $type_id)
							{
									 //if($i>0) $displayHTML .= "</table></td></tr>";
									 if($i>0) $displayHTML .= "</td></tr>";
									 $displayHTML .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">".$type_name."</span></td>";
									 $displayHTML .= "<td valign=\"top\" class=\"tabletext\"><table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
									 $currType = $type_id;
									 $typeArr[] = $type_id;
							}

							$checked = "";
							$enroled_hint = "";
							for($j=0; $j<sizeof($enroledID); $j++)
							{
									 if($eg_id == $enroledID[$j][0])
									 {
											 $checked = "CHECKED";
											 $enroled_hint = "<font color='red'> ($i_Sports_Enrolled)</font>";
									 }
							}

							if($group_id == '-2')
							{
									$event_name = $i_Sports_Event_Girls_Open.$event_name;
							}
							else if($group_id == '-1')
							{
									$event_name = $i_Sports_Event_Boys_Open.$event_name;
							}
							else if($group_id == '-4')
							{
									$event_name = $i_Sports_Event_Mixed_Open.$event_name;
							}

//							$css = ($i%2?"":"2");
							
							# check the event is full or not
                            $cur_enroled = $lswimminggala->returnEventEnroledNo($eg_id);
                            $enroled_hint .= ($event_quota && $cur_enroled>=$event_quota) ? " <font color=red> (". $Lang['eSports']['QuotaFull'] .")</font>" : "";
                            $this_disabled = ($event_quota && $cur_enroled>=$event_quota && !$checked) ? " disabled" : "";
                           
							$displayHTML .= "<tr><td class='tabletext'><input type='checkbox' ". $this_disabled ." name='RQ_EventGroup".$type_id."[]' value='".$eg_id."' ".$checked." id='RQ_EventGroup".$eg_id."'><label for='RQ_EventGroup".$eg_id."'>".$event_name.$enroled_hint."</label></td></tr>";
					}
			 }
			 
			 if(is_array($typeArr))
					$types = implode($typeArr, ",");
			 else
					$types = $typeArr;

			 if($displayHTML == "")
			 {
					$btn_flag = "-1";
					$displayHTML .= "<tr><td colspan='2' align='center' class='tabletext'><br /><br /><br />".$i_no_record_exists_msg."<br /></td></tr>";
			 }
			 else
				$displayHTML .= "</table></td></tr>";


			############## Unrestrict Quota Events ##############################
			
			$recordExist = 0;
			$displayHTML2 = "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">".$i_Sports_Event_UnrestrictQuota_Event."</span></td>";
			$displayHTML2 .= "<td valign=\"top\" class=\"tabletext\"><table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
			for($i=0; $i<sizeof($UQ_eventgroup); $i++)
			{
					list($eg_id, $event_name, $type_id, $group_id, $event_quota) = $UQ_eventgroup[$i];
					
					$ext_exist = $lswimminggala->checkExtInfoExist($type_id, $eg_id);
					if($ext_exist == 1)
					{
							$recordExist = 1;

							$css = ($i%2?"":"2");
							
							$checked = "";
							$enroled_hint = "";
							for($j=0; $j<sizeof($enroledID); $j++)
							{
									if($eg_id == $enroledID[$j][0])
									{
											$checked = "CHECKED";
											$enroled_hint = "<font color='red'> ($i_Sports_Enrolled)</font>";
									}
							}

							if($group_id == '-2')
							{
									$event_name = $i_Sports_Event_Girls_Open.$event_name;
							}
							else if($group_id == '-1')
							{
									$event_name = $i_Sports_Event_Boys_Open.$event_name;
							} 
							else if($group_id == '-4')
							{
									$event_name = $i_Sports_Event_Mixed_Open.$event_name;
							}
							
							# check the event is full or not
                            $cur_enroled = $lswimminggala->returnEventEnroledNo($eg_id);
                            $enroled_hint .= ($event_quota && $cur_enroled>=$event_quota) ? " <font color=red> (". $Lang['eSports']['QuotaFull'] .")</font>" : "";
                            $this_disabled = ($event_quota && $cur_enroled>=$event_quota && !$checked) ? " disabled" : "";
                           
							$displayHTML2 .= "<tr><td class='tabletext'><input type='checkbox' ". $this_disabled ." name='UQ_EventGroup[]' value='".$eg_id."' ".$checked." id='UQ_EventGroup".$eg_id."'><label for='UQ_EventGroup".$eg_id."'>".$event_name.$enroled_hint."</label></td></tr>";
					}
			}
			$displayHTML2 .= "</table></td></tr>";
			
			if($recordExist==1)
			{
				$displayHTML = $displayHTML.$displayHTML2;
				$btn_flag = "1";
			}
}
else
{
	$btn_flag = "-1";
	if(!$can_apply)
    {
    	$displayHTML = "<tr><td colspan='2' align='center' class='tabletext'><br /><br /><br />".$Lang['eSports']['NotAllowForEnrolment'].$restrict_reason[0]."<br /></td></tr>";
    }
    else
    {
    	$displayHTML = "<tr><td colspan='2' align='center' class='tabletext'><br /><br /><br />".$i_Sports_No_DOB."<br /></td></tr>";
    }
}

if ($msg == 2)
{
    $xmsg = "<font color=green>$i_ClubsEnrollment_Form_Updated</font>";
    //$xmsg = "$i_ClubsEnrollment_Form_Updated";
}
?>

<SCRIPT LANGUAGE=Javascript>
function checkform(obj)
{
	var tc = countChecked(obj, "RQ_EventGroup1[]");

//     var totalCount = parseInt(tc) + parseInt(fc);

    if(tc > maxTrack) 
    {
            alert('<?=$i_Sports_Enrolment_Warn_MaxEvent?> ' + maxTrack + ' <?=$Lang['eSports']['per']?>' + track + '<?=$i_ResourceTitle?>');
            return false;
    }
    else
			document.getElementById("tc").value = tc;
            return true;
}
</SCRIPT>

<br />   
<form name="form1" method="post" action="update.php" enctype="multipart/form-data" onSubmit="return checkform(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">

<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                		<? if($can_apply) { ?>
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Enrolment_Detail?> </span></td>
								<td class="tabletext"><?=nl2br($lswimminggala->enrolDetails)?></td>
							</tr>
							
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Group?> </span></td>
								<td class="tabletext"><?=$targetGroupName?></td>
							</tr>
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Participant_Number?> </span></td>
								<td class="tabletext"><?=$athleticNum[0][0]?></td>
							</tr>
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?="$i_Sports_Enrolment_MaxEvent ($txt_track)"?> </span></td>
								<td class="tabletext"><?=$maxTrack?></td>
							</tr>
						<? } ?>
						
						<?=$displayHTML?>
						</table>
					</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <? if($btn_flag<>"-1") {?>
	                <tr>
						<td align="center">
							<?= $linterface->GET_ACTION_BTN($button_save, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
							<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
						</td>
					</tr>
				<? } ?>
                </table>                                
	</td>
</tr>
</table>                        
<br />

<input type="hidden" name="types" value="<?=$types?>" />
<input type="hidden" id="tc" name="tc" value="" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>