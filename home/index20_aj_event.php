<?php

## Using By : 

################ Change Log [Start] #####################
#
#	Date 	:	2010-06-23 [Yuen]
#	Details	:	Improved to show the div in Chrome which failed before due to use of "height:100%" in div
#
################ Change Log [End] #####################


$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$li = new libcalevent2007($ts,$v);

if ($type==0)
{
	$EventMenu = "	
						<table width=\"100%\" height=\"22\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
						<tr>
							<td width=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_on_left.gif\" width=\"4\" height=\"22\" /></td>
							<td align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_on_bg.gif\" class=\"indexeventtitleon\">{$ip20_event_month}</td>
							<td width=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_middle_on_off.gif\" width=\"8\" height=\"22\"></td>
							<td align=\"center\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_off_bg.gif\"><a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_EVENT(document.EventForm,1)\" class=\"indexeventtitleoff\">{$ip20_event_today}</a> </td>
							<td width=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_off_right.gif\" width=\"8\" height=\"22\"></td>
						</tr>
						</table>	
						";
	$EventContent =  $li->displayEventType(1);	
} 
else 
{
	$EventMenu = "	
						<table width=\"100%\" height=\"22\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
						<tr>
							<td width=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_off_left.gif\" width=\"4\" height=\"22\"></td>
							<td align=\"center\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_off_bg.gif\"><a  href=\"javascript:void(0)\" onclick=\"jAJAX_GO_EVENT(document.EventForm,0)\" class=\"indexeventtitleoff\">{$ip20_event_month}</a> </td>
							<td width=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_middle_off_on.gif\" width=\"8\" height=\"22\"></td>
							<td align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_on_bg.gif\" class=\"indexeventtitleon\">{$ip20_event_today}</td>
							<td width=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_on_right.gif\" width=\"8\" height=\"22\"></td>
						</tr>
						</table>	
						";	
	$EventContent =  $li->displayEventType(0);	
}

# Improved to show the div in Chrome which failed before due to use of "height:100%" in div
$HeightUsed = (strstr($_SERVER['HTTP_USER_AGENT'], "MSIE")) ? "height:100%;" : "";

$x = 	"
			<table width=\"193\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
			<tr>
				<td height=\"22\">{$EventMenu}</td>
			</tr>
			<tr>
				<td height=\"100%\"  valign=\"top\" >
				<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\">
				<tr>
					<td width=\"5\" height=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/board_01.gif\" width=\"5\" height=\"5\"></td>
					<td height=\"5\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/board_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/board_02.gif\" width=\"5\" height=\"5\"></td>
					<td width=\"7\" height=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/board_03.gif\" width=\"7\" height=\"5\"></td>
				</tr>
				<tr>
					<td width=\"5\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/board_04.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/board_04.gif\" width=\"5\" height=\"5\"></td>
					<td valign=\"top\" bgcolor=\"#FFFFFF\" height=\"100%\">
					<div  id=\"EventInboxDiv\" style=\"width:100%; {$HeightUsed} z-index:1; overflow: auto;\">
					{$EventContent}					
					</div>
					</td>
					<td width=\"7\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/board_06.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/board_06.gif\" width=\"7\" height=\"5\"></td>
				</tr>
				<tr>
					<td width=\"5\" height=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/board_07.gif\" width=\"5\" height=\"8\"></td>
					<td height=\"8\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/board_08.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/board_08.gif\" width=\"7\" height=\"8\"></td>
					<td width=\"7\" height=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/board_09.gif\" width=\"7\" height=\"8\"></td>
				</tr>
				</table>
				</td>
			</tr>
			</table>
		";
		
echo $x;			

intranet_closedb();

?>