<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT.'includes/global.php');
include_once($PATH_WRT_ROOT.'includes/libdb.php');
include_once($PATH_WRT_ROOT.'includes/tableReplySlip/libTableReplySlipMgr.php');

if(strpos($_SERVER['HTTP_REFERER'],'/home/eAdmin/ResourcesMgmt/DocRouting/index.php?pe=8OaOAb4COQCRJNyp2jeWSB4kDyYanKf')===false){
	intranet_auth();
}
intranet_opendb();

array_walk_recursive($_POST, 'handleFormPost');
$replySlipId = $_POST['tableReplySlipId'];
$replySlipUserId = $_POST['tableReplySlipUserId'];
$replySlipItemIdAry = $_POST['tableReplySlipItemIdList_'.$replySlipId];

$libReplySlipMgr = new libTableReplySlipMgr();
echo ($libReplySlipMgr->saveUserAnswer($replySlipId, $replySlipUserId, $replySlipItemIdAry, $_POST))? '1' : '0';

intranet_closedb();
?>