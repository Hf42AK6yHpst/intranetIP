<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT.'includes/global.php');
include_once($PATH_WRT_ROOT.'includes/libdb.php');
include_once($PATH_WRT_ROOT.'includes/tableReplySlip/libTableReplySlipMgr.php');

if(strpos($_SERVER['HTTP_REFERER'],'/home/eAdmin/ResourcesMgmt/DocRouting/index.php?pe=8OaOAb4COQCRJNyp2jeWSB4kDyYanKf')===false){
	intranet_auth();
}
intranet_opendb();

$replySlipId = $_POST['ReplySlipID'];
$defaultShowDiv = $_POST['DefaultShowDiv'];


$libTableReplySlipMgr = new libTableReplySlipMgr();
$libTableReplySlipMgr->setReplySlipId($replySlipId);
echo $libTableReplySlipMgr->returnReplySlipStatisticsHtml($defaultShowDiv);

intranet_closedb();
?>