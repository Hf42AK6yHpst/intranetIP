<?php
// using : 
/*
 * Change Log:
 * 
 * 	2017-08-29 [Cameron]: add case "Medical"
 * 	
 * 	2017-07-07 [Anna]: create file to handle password checking of different modules -- copy from EJ
 */
 
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

// get module and path
$target_module = $_POST['module'];
$target_path = $_POST['module_path'];

// different modules
switch ($target_module)
{
	
	case "DocRouting":
		include_once($PATH_WRT_ROOT."includes/DocRouting/libDocRouting.php");
		include_once($PATH_WRT_ROOT."includes/DocRouting/docRoutingConfig.inc.php");
		
		//		$lib_obj = new libDocRouting();
		
		# Left menu
		$MODULE_OBJ['title'] = $Lang['Header']['Menu']['DocRouting'];
		$MODULE_OBJ['title_css'] = "menu_opened";
		$MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_doc_routing.png";
		$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT.$docRoutingConfig['eAdminPath'];
		
		# Tag
		$TAGS_OBJ[] = array($Lang['Header']['Menu']['DocRouting'], "");
		
		break;
		
	case "disciplinev12":
		include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
		
//		$lib_obj = new libdisciplinev12();
		
		# Left menu
		$MODULE_OBJ['title'] = $ip20TopMenu['eDisciplinev12'];
		$MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_ediscipline.gif";
	//	$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/admin/disciplinev12/index.php";
		$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."/home/eAdmin/StudentMgmt/disciplinev12/overview/index.php";
		$MODULE_OBJ['title_css'] = "menu_opened";
		
		# Tag
		$TAGS_OBJ[] = array($ip20TopMenu['eDisciplinev12'], "");
		
		break;
	case "eGuidance":
		include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
		include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
		
		# Left menu
		$MODULE_OBJ['title'] =$Lang['eGuidance']['name'];
		$MODULE_OBJ['title_css'] = "menu_opened";
		$MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_eguidance.png";
		$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."/home/eAdmin/StudentMgmt/eGuidance/index.php";
		
		# Tag
		$TAGS_OBJ[] = array($Lang['eGuidance']['name'], "");
		
		break;
		
	case "eReportCard":
		include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
		include_once($PATH_WRT_ROOT."lang/reportcard2008_lang.$intranet_session_language.php");
		
		# Left menu
		$MODULE_OBJ['title'] = $i_ReportCard_System;
	//	$MODULE_OBJ['title_css'] = "menu_opened";
		$MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_reportcardsystem.gif";
		$MODULE_OBJ['root_path'] =  $PATH_WRT_ROOT."/home/eAdmin/StudentMgmt/eReportCard/index.php";
	
		# Tag
		$TAGS_OBJ[] = array($i_ReportCard_System, "");
		
		break;
		
	case "Medical":
		include_once($PATH_WRT_ROOT."includes/cust/medical/libMedical.php");
		include_once($PATH_WRT_ROOT."lang/cust/medical_lang.$intranet_session_language.php");
		
		# Left menu
		$MODULE_OBJ['title'] =$Lang['medical']['module'];
		$MODULE_OBJ['title_css'] = "menu_opened";
		$MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_medical.png";
		$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."/home/eAdmin/StudentMgmt/medical/?t=index.index";
		
		# Tag
		$TAGS_OBJ[] = array($Lang['medical']['module'], "");
		
		break;
		
// 	case "DigitalArchive":
// 		include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
// 		include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");
		
// 		# Left menu
// 		$MODULE_OBJ['title'] = $Lang["DigitalArchive"]["DigitalArchiveName"];
// //		$MODULE_OBJ['title_css'] = "menu_opened";
// 		$MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_digital_archive.png";
// 		$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/?clearCoo=1";
		
// 		# Tag
// 		$TAGS_OBJ[] = array($Lang["DigitalArchive"]["DigitalArchiveName"], "");
		
// 		break;
}

// after submit, check submitted password
if($_POST['isSubmit']==1){
	$password = stripslashes(trim($_POST['password']));
	
	// password matched
	if($_SESSION['eclass_session_password'] == "$password"){
		// set value in session
		$_SESSION['module_session_login']["$target_module"] = 1;
		
		// redirect to original path
		header("Location: $target_path");
		
		intranet_closedb();	
		exit;
	}
	// show error message
	else {
		$msg = "AccessDenied";
	}
}

// error message
$msgDisplay = "";
if($msg!=""){
	$msgDisplay = "0|=|".$Lang['General'][$msg];
}

# Start layout
$linterface = new interface_html();
$linterface->LAYOUT_START($msgDisplay);

?>

<div class="table_board">
<br />
<form id="form1" name="form1" method="post" action="module_access_checking.php">
	<?=$linterface->Get_Warning_Message_Box($Lang['Security']['Authentication'], $Lang['Security']['AuthenticationInstruction'], $others_="");?>
	<table class="form_table_v30" cellpadding="10" cellspacing="0" border="0" align="center">
	<tbody>
		<tr>
			<td class="field_title"><label for="password"><span class="tabletextrequire">*</span><?=$i_UserPassword?></label></td>
			<td><input type="password" name="password" id="password" class="user_password" /><?=$linterface->Get_Form_Warning_Msg('WarningPassword',$Lang['eClassAPI']['WarningMsg']['InputPassword'],'Warning')?></td>
		</tr>
		<tr>
			<td colspan="2"><?=$linterface->MandatoryField()?></td>
		</tr>
	</tbody>
	</table>
	<p class="spacer"></p>
	<div class="edit_bottom_v30">
		<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","checkSubmit();").'&nbsp;'.$linterface->GET_ACTION_BTN($Lang['Btn']['Reset'],"reset","");?>
	</div>
	<input type="hidden" name="module" value="<?=$target_module?>" />
	<input type="hidden" name="module_path" value='<?=$target_path?>' />
	<input type="hidden" name="isSubmit" value='1' />
</form>
</div>

<script type="text/javascript" language="JavaScript">
function checkSubmit()
{
	var pw = document.getElementById('password').value.Trim();
	$('#WarningPassword').hide();
	if(pw == ''){
		$('#WarningPassword').show();
		$('#password').focus();
		return false;
	}
	document.form1.submit();
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();	
?>