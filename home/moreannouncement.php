<?php
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."includes/libannounce_ip30.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lo = new libannounce_ip30();

if ($sys_custom['LivingHomeopathy']) {
	$type_title = $Lang['Header']['Menu']['schoolNews'];
} else {
	$type_title = ($type==0? $i_AnnouncementPublic:$i_AnnouncementGroup);
}

if ($type == 2) {
	$ajax_target = 'all';
	$Groups = explode(",",$group);
	$display = $lo->announcementListHtml($UserID, $Groups, $ajax_target, true);
} elseif ($type == 1){
	$ajax_target = 'group';
	$Groups = explode(",",$group);
	$display = $lo->announcementListHtml($UserID, $Groups, $ajax_target, true);
}else{
	$ajax_target = 'public';
	$display = $lo->announcementListHtml($UserID, $Groups, $ajax_target, true);
}
$MODULE_OBJ['title'] = $type_title;
$linterface = new interface_html("popup_index.html");
$linterface->LAYOUT_START();

?>

<?php echo $display; ?>
<?php

intranet_closedb();
$linterface->LAYOUT_STOP();
?>