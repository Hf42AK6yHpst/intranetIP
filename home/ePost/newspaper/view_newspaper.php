<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_writer.php");

# $r is base64 encoded string which contains NewspaperID
$r_decode = isset($r) && $r!=''? base64_decode($r) : '';
if($r_decode){
	$NewspaperID  = end(explode('=',$r_decode));
	$IsPublicLink = true;
}
else{
	intranet_auth();
	$NewspaperID  = isset($NewspaperID) && $NewspaperID!=''? $NewspaperID : '';
	$IsPublicLink = false;
}

intranet_opendb();

# Initialize Variable
$NewspaperID = isset($NewspaperID) && $NewspaperID!=''? $NewspaperID : '';
$PageID 	 = isset($PageID) && $PageID!=''? $PageID : '';
$PageOrder	 = isset($PageOrder) && $PageOrder!=''? $PageOrder : 0;
$IsZoomOut	 = isset($IsZoomOut) && $IsZoomOut!=''? $IsZoomOut : false;
$Header = 'PopUp';
# Initialize Object
$ePostUI	 	= new libepost_ui();
$ePostNewspaper = new libepost_newspaper($NewspaperID);

# Check if ePost should be accessible
$ePostUI->auth();
if($IsPublicLink && ((!empty($UserID) && !$ePostNewspaper->is_published()) || (empty($UserID) && !$ePostNewspaper->is_open_to_public()))){
	die("This Newspaper is not published yet!");
}

# Update Newspaper No. of View
if($UpdateView){
	$ePostNewspaper->update_NoOfView();
}

# Get the Page Object
$Pages_ary = $ePostNewspaper->get_pages();
if($PageID){
	$PageObj = new libepost_page($PageID);
}
else{
	$PageObj = $Pages_ary[$PageOrder];
}

# Process the Page HTML
if($IsZoomOut){
	$tmp_PageDisplay = $ePostUI->get_page_display($ePostNewspaper, $PageObj->Page_PageOrder);
	
	if($tmp_PageDisplay==1){ // Cover Page
		$LeftPageObj = $PageObj;
		$PageHTML 	 = $ePostUI->gen_small_cover_back_page_layout($LeftPageObj, true);
	}
	else if($tmp_PageDisplay%2==0 && $tmp_PageDisplay==count($ePostNewspaper->Newspaper_PageIDs)){ // Single Bottom Page
		$LeftPageObj = $PageObj;
		$PageHTML 	 = $ePostUI->gen_small_cover_back_page_layout($LeftPageObj, false);
	}
	else{ // Inside Page
		if($tmp_PageDisplay%2!=0){
			$LeftPageObj  = $Pages_ary[$ePostUI->get_prev_next_page_order($ePostNewspaper, $PageOrder, -1)];
			$RightPageObj = $PageObj;
		}
		else{
			$LeftPageObj  = $PageObj;
			$RightPageObj = $Pages_ary[$ePostUI->get_prev_next_page_order($ePostNewspaper, $PageOrder,  1)];
		}
		
		$PageHTML  = $ePostUI->gen_small_inside_page_layout($LeftPageObj, true);
		$PageHTML .= $ePostUI->gen_small_inside_page_layout($RightPageObj, false);
	}
	
	$PageHTML .= $ePostUI->gen_view_newspaper_bottom_bar($ePostNewspaper, $LeftPageObj->Page_PageOrder, true);
}
else{
	$PageHTML  = $ePostUI->gen_big_page_layout($PageObj, false, false);
	$PageHTML .= $ePostUI->gen_view_newspaper_bottom_bar($ePostNewspaper, $PageObj->Page_PageOrder, false);
}

include($intranet_root.'/home/ePost/newspaper/templates/tpl_view_newspaper.php');

intranet_closedb();
?>
