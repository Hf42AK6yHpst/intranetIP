<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$FolderID = isset($FolderID) && $FolderID!=''? $FolderID : '';

# Initialize Object
$ePost = new libepost();

# Check if ePost should be accessible
$ePost->portal_auth();

if($FolderID){
	$ePostFolder = new libepost_folder($FolderID);
	$result = $ePostFolder->delete_folder();
}
$returnMsg = $result?"DeleteSuccess":"DeleteUnsuccess";
header('location:index.php?returnMsg='.$returnMsg);

intranet_closedb();
?>