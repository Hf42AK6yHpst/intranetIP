<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_studenteditor.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$NewspaperID = isset($NewspaperID) && $NewspaperID!=''? $NewspaperID : 0;
$PageID		 = isset($PageID) && $PageID!=''? $PageID : 0;
$IsPopup  = isset($IsPopup) && $IsPopup!=''? $IsPopup : 0;

# Initialize Object and Navigation Array
$ePostUI 			 = new libepost_ui();
$ePostStudentEditors = new libepost_studenteditors();
$ePostNewspaper 	 = new libepost_newspaper($NewspaperID);
$Header=($IsPopup)?'PopUp':'NEW';
$FromPage = 'editor';
# Check if ePost should be accessible
$ePostUI->portal_auth();

$IsReadOnly = $ePostNewspaper->Newspaper_PublishStatus!=$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft'];
$nav_ary[] = array("title"=>$Lang['ePost']['EditorPage'], "link"=>"../editor.php");
$nav_ary[] = array("title"=>$Lang['ePost']['FolderList'], "link"=>"index.php");
$nav_ary[] = array("title"=>($ePostNewspaper->Folder_IsSystemPublic==$cfg_ePost['BigNewspaper']['Folder_IsSystemPublic']['true']? $Lang['ePost']['PublicFolder']:$ePostFolder->Folder_Title), "link"=>"issue_list.php?FolderID=".$ePostNewspaper->FolderID);
$nav_ary[] = array("title"=>($IsReadOnly? $Lang['ePost']['ViewIssueInformation']:$Lang['ePost']['EditIssue']), "link"=>"");

if($IsReadOnly){
	if(is_array($cfg_ePost['BigNewspaper']['skins']) && count($cfg_ePost['BigNewspaper']['skins'])){
		foreach($cfg_ePost['BigNewspaper']['skins'] as $skin_id=>$skin_ary){
			if($skin_id==$ePostNewspaper->Newspaper_Skin){
				$skin_selection_html = "<img height=\"58\" width=\"300\" class=\"issue_layout_img\" src=\"$intranet_httppath/images/ePost/".$skin_ary['thumbnail']."\">\n";
			}
		}
	}
}
else{
	# Process Skin Selection HTML
	$default_skin = $ePostNewspaper->Newspaper_Skin;
	$school_logo = $ePostNewspaper->Newspaper_SchoolLogo; 
	$cover_banner = $ePostNewspaper->Newspaper_CoverBanner; 
	$inside_banner = $ePostNewspaper->Newspaper_InsideBanner; 
	$skin_selection_html = "";
	if(is_array($cfg_ePost['BigNewspaper']['skins']) && count($cfg_ePost['BigNewspaper']['skins'])){
		foreach($cfg_ePost['BigNewspaper']['skins'] as $skin_id=>$skin_ary){
			$default_skin = $default_skin=='' && $skin_ary['isDefault']? $skin_id : $default_skin;
			
			$skin_selection_html .= "<a id=\"$skin_id\" class=\"skin_select select_layout ".($skin_id==$default_skin? "select_layout_selected":"")."\" href=\"javascript:void(0)\" onclick=\"select_skin('$skin_id')\">\n";
			$skin_selection_html .= "	<img height=\"58\" width=\"300\" class=\"issue_layout_img\" src=\"$intranet_httppath/images/ePost/".$skin_ary['thumbnail']."\">\n";
			$skin_selection_html .= "</a>\n";
		}
	}
}

include($intranet_root.'/home/ePost/newspaper/templates/tpl_issue_new_design.php');

intranet_closedb();
?>