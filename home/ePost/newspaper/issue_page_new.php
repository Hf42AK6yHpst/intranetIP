<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_writer.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$NewspaperID = isset($NewspaperID) && $NewspaperID!=''? $NewspaperID : 0;
$PageID		 = isset($PageID) && $PageID!=''? $PageID : 0;
$FromPage	 = isset($FromPage) && $FromPage!=''? $FromPage : '';

# Initialize Object
$ePostUI 	    = new libepost_ui();
$ePostNewspaper = new libepost_newspaper($NewspaperID);
$PublishStatus = $ePostNewspaper->Newspaper_PublishStatus;
if($PublishStatus==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Published'])
	header('Location: index.php');
# Check if ePost should be accessible
$ePostUI->portal_auth();

if($PageID){
	$ePostPage = new libepost_page($PageID);
}
$Type   = $PageID? $ePostPage->Page_Type   : 'I1';
$Layout = $PageID? $ePostPage->Page_Layout : 1;
$Theme	= $PageID? $ePostPage->Page_Theme  : 1;
$Name	= $PageID? $ePostPage->Page_Name   : '';
$PageTotal = count($ePostNewspaper->Newspaper_PageIDs);


include($intranet_root.'/home/ePost/newspaper/templates/tpl_issue_page_new.php');

intranet_closedb();
?>