<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$FolderID = isset($FolderID) && $FolderID!=''? $FolderID : 0;
$Status   = isset($Status) && $Status!=''? $Status : $cfg_ePost['BigNewspaper']['Newspaper_filter']['All'];
$returnMsg = isset($returnMsg) && $returnMsg!=''? $returnMsg : "";

# Initialize Object
$ePost		 = new libepost();
$ePostUI	 = new libepost_ui();
$ePostFolder = new libepost_folder($FolderID);
$Header='NEW';
$FromPage = 'editor';
# Check if ePost should be accessible
$ePost->portal_auth();
$Newspapers_ary = $ePostFolder->get_managing_newspapers();

# Process Table Content HTML
for($i=0;$i<count($Newspapers_ary);$i++){
	$NewspaperObj = $Newspapers_ary[$i];
	
	switch($Status){
		case $cfg_ePost['BigNewspaper']['Newspaper_filter']['All']: 
			$IsSkipped = false; 
			break;
		case $cfg_ePost['BigNewspaper']['Newspaper_filter']['Draft']: 
			$IsSkipped = $NewspaperObj->Newspaper_PublishStatus!=$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft']; 
			break;
		case $cfg_ePost['BigNewspaper']['Newspaper_filter']['WaitApproval']: 
			$IsSkipped = $NewspaperObj->Newspaper_PublishStatus!=$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['WaitApproval']; 
			break;
		case $cfg_ePost['BigNewspaper']['Newspaper_filter']['Published']: 
			$IsSkipped = $NewspaperObj->Newspaper_PublishStatus!=$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Published']; 
			break;
	}
	
	if(!$IsSkipped){
		$NoOfPages = count($NewspaperObj->Newspaper_PageIDs);
		if($NewspaperObj->Newspaper_PublishStatus==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Published'] && !($ePostUI->user_obj->isTeacherStaff() && $ePostUI->is_publish_manager)){
			$param = "disabled";
		}else{
			$param = "onchange='change_publish_status(".$NewspaperObj->NewspaperID.", ".$NewspaperObj->Newspaper_PublishStatus.")'";
		}
		
		$table_html .= "<tr class=\"".($NewspaperObj->Newspaper_PublishStatus==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Published']? "past" : "noraml")."\">";
		$table_html .= "	<td>".($i+1)."</td>";
		$table_html .= "	<td id=\"Newspaper_epost_title_".$NewspaperObj->NewspaperID."\">".$NewspaperObj->Newspaper_Title."</td>";
		$table_html .= "	<td>".($NoOfPages > 0? "<a id=\"Newspaper_title_".$NewspaperObj->NewspaperID."\" href=\"javascript:openNewspaper(".$NewspaperObj->NewspaperID.")\">".$NewspaperObj->Newspaper_Name."</a>":"<span id=\"Newspaper_title_".$NewspaperObj->NewspaperID."\">".$NewspaperObj->Newspaper_Name)."</span></td>";
		$table_html .= "	<td>".$NoOfPages."</td>";
		$table_html .= "	<td>".date('Y-m-d', strtotime($NewspaperObj->Newspaper_IssueDate))."</td>";
		$table_html .= "	<td>".($NewspaperObj->Newspaper_NoOfView? $NewspaperObj->Newspaper_NoOfView : 0)."</td>";
		$table_html .= "	<td>".$ePostUI->gen_publish_status_drop_down_list('PublishStatus_'.$NewspaperObj->NewspaperID, 'PublishStatus_'.$NewspaperObj->NewspaperID, $NewspaperObj->Newspaper_PublishStatus, $param, ($NoOfPages==0))."</td>";
		$table_html .= "	<td>".date('Y-m-d', strtotime($NewspaperObj->Newspaper_ModifiedDate))." (".$NewspaperObj->Newspaper_ModifiedByUser.")</td>";
		$table_html .= "	<td>";
		$table_html .= "		<div class=\"table_row_tool\">";
		if($NewspaperObj->Newspaper_PublishStatus==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft'])
		$table_html .= "			<a href=\"javascript:go_edit(".$NewspaperObj->NewspaperID.")\" class=\"tool_edit\" title=\"".$Lang['ePost']['Edit']."\">&nbsp;</a>";
		
		if($NewspaperObj->Newspaper_PublishStatus==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft'] && $ePost->user_obj->isTeacherStaff() && $ePost->is_editor)
			$table_html .= "		<a href=\"javascript:go_delete(".$NewspaperObj->NewspaperID.")\" class=\"tool_delete\" title=\"".$Lang['ePost']['Delete']."\">&nbsp;</a>";
		
		$table_html .= "		</div>";
		$table_html .= "	</td>";
		$table_html .= "</tr>";
	}
}

if(!$table_html){
	$table_html  = "<tr>";
	$table_html .= "	<td height='80' class='tabletext' colspan='9' style='text-align:center;vertical-align:middle'>$i_no_record_exists_msg</td>";
	$table_html .= "</tr>";
}

$nav_ary[] = array("title"=>$Lang['ePost']['EditorPage'], "link"=>"../editor.php");
$nav_ary[] = array("title"=>$Lang['ePost']['FolderList'], "link"=>"index.php");
$nav_ary[] = array("title"=>($ePostFolder->Folder_IsSystemPublic==$cfg_ePost['BigNewspaper']['Folder_IsSystemPublic']['true']? $Lang['ePost']['PublicFolder']:$ePostFolder->Folder_Title), "link"=>""); 


include($intranet_root.'/home/ePost/newspaper/templates/tpl_issue_list.php');

intranet_closedb();
?>