<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_writer.php");

intranet_opendb();

# Initialize Variable
$ArticleID = isset($ArticleID) && $ArticleID!=''? $ArticleID : ''; 

$ePostUI	= new libepost_ui();
$ArticleObj = new libepost_article($ArticleID);

# Check if ePost should be accessible
$ePostUI->auth();

$article_cfg_ary = $ArticleObj->get_page_TypeLayout_articles_ary();

$para_ary 						= array();
$para_ary['PageID'] 			= $ArticleObj->PageID;
$para_ary['Position'] 			= $ArticleObj->Article_Position;
$para_ary['ArticleObj']			= $ArticleObj;
$para_ary['ArticleCfg'] 		= $article_cfg_ary[$ArticleObj->Article_Position];
$para_ary['IsEditMode']		 	= false;
$para_ary['ShowMoreBtn'] 		= false;
$para_ary['RestrictionNameAry'] = $ArticleObj->get_article_restriction_name_ary($ArticleObj->Article_Position);
				
$article_html = $ePostUI->gen_page_article_display($para_ary);

include($intranet_root.'/home/ePost/newspaper/templates/tpl_view_newspaper_article.php');

intranet_closedb();
?>