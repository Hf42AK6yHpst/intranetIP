<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_writer.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$PageID	  = isset($PageID) && $PageID!=''? $PageID : 0;
$Position = isset($Position) && $Position!=''? $Position : 1;

# Initialize Object
$ePostUI   = new libepost_ui();
$ePostPage = new libepost_page($PageID);

# Check if ePost should be accessible
$ePostUI->portal_auth();
$Header = 'PopUp';
$Articles_ary = $ePostPage->get_articles();
$ePostArticle = $Articles_ary[$Position];

$ArticleShelfID = $ArticleShelfID? $ArticleShelfID:(is_object($ePostArticle)? $ePostArticle->ArticleShelfID:'');
$FontStyle   = $FontStyle? $FontStyle : (is_object($ePostArticle)? $ePostArticle->Article_FontStyle:1); 
$FontSize    = $FontSize? $FontSize : (is_object($ePostArticle)? $ePostArticle->Article_FontSize:2); 
$LineHeight  = $LineHeight? $LineHeight : (is_object($ePostArticle)? $ePostArticle->Article_LineHeight:0); 
$DisplayAuthor  = $DisplayAuthor? $DisplayAuthor : (is_object($ePostArticle)? $ePostArticle->Article_DisplayAuthor:$cfg_ePost['BigNewspaper']['Article_DisplayAuthor']['Yes']); 
$DisplayTitle  	= $DisplayTitle? $DisplayTitle : (is_object($ePostArticle)? $ePostArticle->Article_DisplayTitle:$cfg_ePost['BigNewspaper']['Article_DisplayTitle']['Yes']); 
$DisplayContent = $DisplayContent? $DisplayContent : (is_object($ePostArticle)? $ePostArticle->Article_DisplayContent:$cfg_ePost['BigNewspaper']['Article_DisplayContent']['Yes']); 
$Type			= $Type?  $Type  : (is_object($ePostArticle)? $ePostArticle->Article_Type  : (is_array($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'])? current(array_keys($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'])) : ""));
$Theme			= $Theme? $Theme : (is_object($ePostArticle)? $ePostArticle->Article_Theme : (is_array($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'][$Type]['Themes'])? current(array_keys($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'][$Type]['Themes'])) : ""));
$Color			= $Color? $Color : (is_object($ePostArticle)? $ePostArticle->Article_Color : (is_array($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'][$Type]['Themes'][$Theme]['Color'])? current(array_keys($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'][$Type]['Themes'][$Theme]['Color'])) : ""));
$ePostAttachmentAry = $ePostArticle->Article_Attachment;
$isFromArticleShelf = !empty($_POST);
# Get Article Config
$article_cfg_ary = $ePostPage->get_page_TypeLayout_articles_ary();
$article_cfg 	 = $article_cfg_ary[$Position];
$courseware = $courseware?$courseware:2;
# Process selected Article Shelf Article
if($ArticleShelfID){
	$ePostArticleShelf = new libepost_articleshelf($ArticleShelfID);
	$ePostWriter 	   = new libepost_writer($ePostArticleShelf->WritingID);
	$ePostAttachmentAry = empty($_POST)?$ePostAttachmentAry:$ePostArticleShelf->Attachment;
	$ePostArticleShelf->isFrom = 'issue';
	$maxUploadCnt = $cfg_ePost['max_upload'][$ePostWriter->ModuleCode];
	if($ePostArticleShelf->IsAppendix==$cfg_ePost['BigNewspaper']['ArticleShelf_IsAppendix']['Yes']){
		header('location:issue_page_select_article.php?PageID='.$PageID.'&Position='.$Position);
	}
	
	# Decided Courseware, Type and Sub-Type
	foreach($cfg_ePost['BigNewspaper']['ArticleType'][($ePostWriter->RequestID? "non_courseware":"courseware")]['types'] as $type=>$type_ary){
		if($type_ary['search_attr']['level']==$ePostWriter->LevelCode && $type_ary['search_attr']['content']==$ePostWriter->ModuleCode){
			$tmp_article_type = $type;
			break;
		}
		else{
			if(is_array($type_ary['sub_types']) && count($type_ary['sub_types'])){
				foreach($type_ary['sub_types'] as $sub_type=>$sub_type_ary){
					if($sub_type_ary['search_attr']['level']==$ePostWriter->LevelCode && $sub_type_ary['search_attr']['content']==$ePostWriter->ModuleCode){
						$tmp_article_type 	  = $type;
						$tmp_article_sub_type = $sub_type;
						break;
					}
				}
				
				if($tmp_article_type && $tmp_article_sub_type) break;
			}
		}
	}
		
	$courseware = $ePostWriter->LevelCode=='C'? 1 : 2;

	$article_requestID     = $courseware==2? $ePostWriter->RequestID:'';
	$article_request_topic = '';
	if($article_requestID){
		include_once($PATH_WRT_ROOT."includes/ePost/libepost_request.php");
		$ePostRequest = new libepost_request($article_requestID);
		$article_request_topic = $ePostRequest->Topic;
	}
	
	$article_type 	   = $courseware==1? $tmp_article_type:'';
	$article_sub_type  = $courseware==1? $tmp_article_sub_type:'';
	
	# Format file path of writer template
	if($courseware==2){
		$writer_template = "templates/article_edit/NC/".$ePostWriter->ModuleCode.".php";
	}
	else{
		//$writer_template = "templates/article_edit/".$ePostWriter->LevelCode."/content/".$ePostWriter->ModuleCode."/".$theme_obj->theme_writer_code.".php";
		$writer_template = "templates/article_edit/".$ePostWriter->LevelCode."/".$ePostWriter->ModuleCode.".php";
	}
	
	# Check writer template file is exist or not
	$writer_template = file_exists($writer_template)? $writer_template : "";
	if($writer_template=='') die("Writer template file not exist!");
}
##Font Setting HTML
$fontSettingHtml = "";
$fontSettingHtml .= "<div id=\"Div_DisplayContent_Detail\" class=\"att_detail\">";
$fontSettingHtml .= "<ul>";
$fontSettingHtml .= "<li>";
$fontSettingHtml .= "<span>".$Lang['ePost']['FontStyle']."</span><em>:</em>";
$fontSettingHtml .= $ePostUI->gen_fontsetting_drop_down_list("FontStyle",$FontStyle,"class=\"font_setting\" onchange=\"updateContentFontStyle();\"");
$fontSettingHtml .= "</li>";
$fontSettingHtml .= "<li>";
$fontSettingHtml .= "<span>".$Lang['ePost']['FontSize']."</span><em>:</em>";
$fontSettingHtml .= $ePostUI->gen_fontsetting_drop_down_list("FontSize",$FontSize,"class=\"font_setting\" onchange=\"updateContentFontStyle();\"");
$fontSettingHtml .= "</li>";
$fontSettingHtml .= "<li>";
$fontSettingHtml .= "<span>".$Lang['ePost']['LineHeight']."</span><em>:</em>";
$fontSettingHtml .= $ePostUI->gen_fontsetting_drop_down_list("LineHeight",$LineHeight,"class=\"font_setting\" onchange=\"updateContentFontStyle();\"");
$fontSettingHtml .= "</li>";
$fontSettingHtml .= "</ul>";
$fontSettingHtml .= "</div>";

$fontcss = "font-family:".$cfg_ePost['BigNewspaper']['AttachmentLayout']['FontStyleAry'][$FontStyle].";font-size:".$cfg_ePost['BigNewspaper']['AttachmentLayout']['FontSizeAry'][$FontSize].";line-height:".$cfg_ePost['BigNewspaper']['AttachmentLayout']['LineHeightAry'][$LineHeight].";";

include($intranet_root.'/home/ePost/newspaper/templates/tpl_issue_page_select_article.php');

intranet_closedb();
?>