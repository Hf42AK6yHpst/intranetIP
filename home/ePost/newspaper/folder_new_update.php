<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");

intranet_auth();
intranet_opendb();

# Initialize Object
$ePost = new libepost();

# Check if ePost should be accessible
$ePost->portal_auth();

if($FolderID){
	$sql = "UPDATE
				EPOST_FOLDER
			SET
				Title = '$FolderTitle',
				ModifiedBy = '$UserID',
				ModifiedDate = NOW()
			WHERE
				FolderID = $FolderID";
	$result = $ePost->db_db_query($sql);	
	$returnMsg = $result?"UpdateSuccess":"UpdateUnsuccess";		
}
else{
	$sql = "INSERT INTO
				EPOST_FOLDER
				(
					Title, Status, InputDate, ModifiedBy, ModifiedDate
				)
			VALUES
				(
					'$FolderTitle', '".$cfg_ePost['BigNewspaper']['Folder_status']['exist']."', NOW(), '$UserID', NOW()
				)";
	$result = $ePost->db_db_query($sql);
	$returnMsg = $result?"AddSuccess":"AddUnsuccess";
}



header('location:index.php?returnMsg='.$returnMsg);

intranet_closedb();
?>