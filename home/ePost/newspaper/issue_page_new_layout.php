<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_writer.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$PageID	= isset($PageID) && $PageID!=''? $PageID : 0;

# Initialize Object
$ePostUI   = new libepost_ui();
$ePostPage = new libepost_page($PageID);

# Check if ePost should be accessible
$ePostUI->portal_auth();

$nav_ary[] = array("title"=>$Lang['ePost']['FolderList'], "link"=>"index.php");
$nav_ary[] = array("title"=>($ePostPage->Folder_IsSystemPublic==$cfg_ePost['BigNewspaper']['Folder_IsSystemPublic']['true']? $Lang['ePost']['PublicFolder']:$ePostPage->Folder_Title), "link"=>"issue_list.php?FolderID=".$ePostPage->FolderID); 
$nav_ary[] = array("title"=>$ePostPage->Newspaper_Title." - ".$ePostPage->Newspaper_Name, "link"=>"issue_page_list.php?NewspaperID=".$ePostPage->NewspaperID); 
$nav_ary[] = array("title"=>$ePostPage->Page_Name, "link"=>"");

include($intranet_root.'/home/ePost/newspaper/templates/tpl_issue_page_new_layout.php');

intranet_closedb();
?>