<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$NewspaperID = isset($NewspaperID) && $NewspaperID!=''? $NewspaperID : 0;
$returnMsg = isset($returnMsg) && $returnMsg!=''? $returnMsg : "";

# Initialize Object
$ePostUI	    = new libepost_ui();
$ePostNewspaper = new libepost_newspaper($NewspaperID);

# Check if ePost should be accessible
$ePostUI->portal_auth();

$Pages_ary = $ePostNewspaper->get_Pages();

# Process Table Content HTML
for($i=0;$i<count($Pages_ary);$i++){
	$PageObj = $Pages_ary[$i];
	$Theme_img_path  = $PageObj->return_theme_image_path();
	$Layout_img_path = $PageObj->return_layout_image_path();
	
	$table_html .= "<tr class=\"normal\">";
	$table_html .= "	<td>".($i+1)."</td>";
	$table_html .= "	<td><a id=\"Page_title_".$PageObj->PageID."\" href=\"javascript:openNewspaper(".$PageObj->PageID.")\">".$PageObj->Page_Name."</a></td>";
	$table_html .= "	<td>";
	$table_html .= $Theme_img_path?  "<img src=\"$Theme_img_path\" width=\"90\" height=\"65\" class=\"issue_layout_img\" />" : "--";
	$table_html .= "	</td>";
	$table_html .= "	<td>";
	$table_html .= $Layout_img_path? "<img src=\"$Layout_img_path\" width=\"60\" height=\"80\" class=\"issue_layout_img\" />" : "--";
	$table_html .= "	</td>";
	$table_html .= "	<td>".count($PageObj->Page_ArticleIDs)."/".$PageObj->return_page_max_article()."</td>";
	$table_html .= "	<td>".date('Y-m-d', strtotime($PageObj->Page_ModifiedDate))." (".$PageObj->Page_ModifiedByUser.")</td>";
	
	if($ePostNewspaper->Newspaper_PublishStatus==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft']){
		$table_html .= "<td>";
		$table_html .= "	<div class=\"table_row_tool\">";
		$table_html .= "		<a href=\"javascript:edit_page(".$PageObj->PageID.")\" class=\"tool_edit\" title=\"".$Lang['ePost']['Edit']."\">&nbsp;</a>";
		$table_html .= $PageObj->get_page_format()!=$cfg_ePost['BigNewspaper']['PageType']['Cover']? "<a href=\"javascript:go_delete(".$PageObj->PageID.")\" class=\"tool_delete\" title=\"".$Lang['ePost']['Delete']."\">&nbsp;</a>" : "";
		$table_html .= "	</div>";
		$table_html .= "</td>";
	}
	$table_html .= "</tr>";
}

if(!$table_html){
	$table_html  = "<tr>";
	$table_html .= "	<td height='80' class='tabletext' colspan='".($ePostNewspaper->Newspaper_PublishStatus==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft']? '7':'6')."' style='text-align:center;vertical-align:middle'>$i_no_record_exists_msg</td>";
	$table_html .= "</tr>";
}
$nav_ary[] = array("title"=>$Lang['ePost']['EditorPage'], "link"=>"../editor.php");
$nav_ary[] = array("title"=>$Lang['ePost']['FolderList'], "link"=>"index.php");
$nav_ary[] = array("title"=>($ePostNewspaper->Folder_IsSystemPublic==$cfg_ePost['BigNewspaper']['Folder_IsSystemPublic']['true']? $Lang['ePost']['PublicFolder']:$ePostNewspaper->Folder_Title), "link"=>"issue_list.php?FolderID=".$ePostNewspaper->FolderID); 
$nav_ary[] = array("title"=>$ePostNewspaper->Newspaper_Title." - ".$ePostNewspaper->Newspaper_Name, "link"=>""); 

include($intranet_root.'/home/ePost/newspaper/templates/tpl_issue_page_list.php');

intranet_closedb();
?>