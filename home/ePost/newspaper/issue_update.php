<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$FolderID 	   = isset($FolderID) && $FolderID!=''? $FolderID:'';
$NewspaperID   = isset($NewspaperID) && $NewspaperID!=''? $NewspaperID:'';
$PublishStatus = isset($PublishStatus) && $PublishStatus!=''? $PublishStatus:$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft'];

$Status		   = isset($Status) && $Status!=''? $Status:0; // For issue_list.php use only

# Initialize Object
$ePostNewspaper = new libepost_newspaper($NewspaperID);

# Check if ePost should be accessible
$ePostNewspaper->portal_auth();

# Delete Newspaper
$result = $ePostNewspaper->update_PublishStatus($PublishStatus);
$new_order_arr = array();
for($i=0;$i<count($ePostNewspaper->Newspaper_PageIDs);$i++){
	$page_id = $ePostNewspaper->Newspaper_PageIDs[$i];
	$order = ${'li_order_'.$page_id};
	$new_order_arr[$order-1] = $page_id;
	
}
$ePostNewspaper->Newspaper_PageIDs = $new_order_arr;
$ePostNewspaper->update_pages_PageOrder();
$redirect_location = 'issue_list.php?FolderID='.$FolderID.'&Status='.$Status.'&msg='.$result;


header('location:'.$redirect_location);

intranet_closedb();
?>