<?php

include($intranet_root.'/home/ePost/newspaper/templates/tpl_issue_header.php');
?>
<body class="edit_page_panel">
<script>
	$(document).ready(function() {
		$('#form1').bind("keypress", function(e) {
		  var code = e.keyCode || e.which; 
		  if (code  == 13) {               
		    e.preventDefault();
		    return false;
		  }
		});		
		resizeDiv();
		refreshPage(<?=$PageID?>);
		<?php if($FromPage=='new_design' && $PageTotal==1){?>
		add_new_page(<?=$PageID?>);
		<?php } ?>
		
	});
  window.onresize = function(event) {
      resizeDiv();
  }

  function resizeDiv() {
      newheight = $(window).height() -80; 
      $('#sidebar').css({'height': newheight + 'px'});
  }
	function edit_issue_info(){
		var title = '';
		var url   = 'issue_new.php?FolderID=<?=$ePostNewspaper->FolderID?>&NewspaperID=<?=$NewspaperID?>&IsPopup=1&TB_iframe=true&width=850&height=500';
		
		tb_show(title, url);
	}
	function edit_issue_design(){
		var title = '';
		var url   = 'issue_new_design.php?FolderID=<?=$ePostNewspaper->FolderID?>&NewspaperID=<?=$NewspaperID?>&IsPopup=1&TB_iframe=true&width=850&height=500';
		
		tb_show(title, url); 
	}
	function add_new_page(p){
		var title = '';
		var url   = 'issue_page_new_info.php?PageID='+p+'&NewspaperID=<?=$NewspaperID?>&TB_iframe=true&width=700&height=400';
		
		tb_show(title, url);
	}	
	function refreshPage(p){
		$.post("/home/ePost/newspaper/ajax_gen_page.php", { task:'refresh_page',NewspaperID:<?=$NewspaperID?>,PageID:p},  
			function(xml, textStatus){
				var PageTotal			= $(xml).find("PageTotal").text();
				var li_html 			= $(xml).find("li_html").text();
				var newspaper_body_html = $(xml).find("newspaper_body_html").text();
				var Status     			= $(xml).find("Status").text();	
				var PageID     			= $(xml).find("PageID").text();	

				$('#PublishStatus').val(Status);
				$('#PageID').val(PageID);
				$('#span_page_total').text(PageTotal);
				$('.page_list').html(li_html);
				$('.edit_page_panel_right').html(newspaper_body_html);
				$( "#ul_issue_page_list" ).sortable({ 
					axis: 'y' ,
					items: "li:not(.ui-state-disabled)",
					update: function( event, ui ) {
						var id_str = '';
						$("li.li_page").each(function(i){
							var cur_id = $(this).attr('id').replace('li_page_','');
							if(i>0){
								id_str += '|=|';
							}	
							id_str += cur_id;
						});	
						updateIssueOrder(id_str);		
					}
				});
				$( "#ul_issue_page_list" ).disableSelection();
			}		
		);
	}		
	function updatePage(page_id,theme,type,layout,name,refpage,change){
		var cur_no = parseInt($('#li_page_'+page_id+' .li_page_no').text());
		$.post("/home/ePost/newspaper/ajax_gen_page.php", { task:'update_page',NewspaperID:<?=$NewspaperID?>,PageID:page_id,PageTheme:theme,PageType:type,PageLayout:layout,PageName:name,PageRefPage:refpage,PageLayoutChanged:change},  
			function(xml, textStatus){
				var page_total 			= $(xml).find("page_total").text();
				var li_html 			= $(xml).find("li_html").text();
				var action 				= $(xml).find("action").text();
				var new_page_id 		= $(xml).find("page_id").text();
				var newspaper_body_html = $(xml).find("newspaper_body_html").text();

				$('#span_page_total').text(page_total);
				$('#ul_issue_page_list').html(li_html);
				$('.edit_page_panel_right').html(newspaper_body_html);
				$('#PageID').val(new_page_id);
			}		
		);
	}
	function deletePage(page_id){
		var PageName = $('#a_'+page_id+' h3').text();
		if(confirm('<?=$Lang['ePost']['Confirm']['RemovePage'][0]?>'+ PageName + '<?=$Lang['ePost']['Confirm']['RemovePage'][1]?>')){
			var cur_no = parseInt($('#li_page_'+page_id+' .li_page_no').text());
			if(cur_no>1){ //cannot delete cover page
				$.post("/home/ePost/newspaper/ajax_gen_page.php", { task:'delete_page',NewspaperID:<?=$NewspaperID?>,PageID:page_id},  
					function(xml, textStatus){
						 var newspaper_body_html = $(xml).find("newspaper_body_html").text();
						 var new_page_id = $(xml).find("new_page_id").text();
						$("em.li_page_no").each(function(i){ 
							if(i>=cur_no){
								$(this).text((i)+")");
							}
						});			
						var PageTotal = parseInt($('#span_page_total').html());
						$('#span_page_total').text(PageTotal-1);
						if($('#li_page_'+page_id).hasClass('selected_page')){
							$("li.selected_page").removeClass('selected_page');
							$('#li_page_'+new_page_id).addClass('selected_page');
							$('.edit_page_panel_right').html(newspaper_body_html);
						}
						$('#li_page_'+page_id).remove();
							
					}		
				);
			}
		}
	}	
	function displayPage(page_id){
		$.post("/home/ePost/newspaper/ajax_gen_page.php", { task:'display_page',NewspaperID:<?=$NewspaperID?>,PageID:page_id},  
			function(xml, textStatus){
				var newspaper_body_html = $(xml).find("newspaper_body_html").text();
				$('.edit_page_panel_right').html(newspaper_body_html);
				$("li.selected_page").removeClass('selected_page');
				$('#li_page_'+page_id).addClass('selected_page');					
			}		
		);
	}	
	function updateIssueOrder(id_str){
		$.post("/home/ePost/newspaper/ajax_gen_page.php", { task:'update_issue_order',NewspaperID:<?=$NewspaperID?>,id_str:id_str},  
			function(xml, textStatus){
				var result = $(xml).find("result").text();
				
				if(result){
					$("em.li_page_no").each(function(i){
						$(this).text((i+1)+")");
					});	
				}
			}		
		);
	}	
	function go_delete_article(ArticleID){
		var ArticleTitle = document.getElementById('Article_title_' + ArticleID).innerHTML;
		
		if(confirm('<?=$Lang['ePost']['Confirm']['RemoveArticle'][0]?>' + ArticleTitle + '<?=$Lang['ePost']['Confirm']['RemoveArticle'][1]?>')){
			$.post("/home/ePost/newspaper/ajax_gen_page.php", { task:'delete_page_article',ArticleID:ArticleID},  
				function(xml, textStatus){
					var newspaper_body_html = $(xml).find("newspaper_body_html").text();
					$('.edit_page_panel_right').html(newspaper_body_html);	
				}		
			);
		}
	}
	
	function go_add_appendix(pageID,position){
		var title = '';
		var url   = 'issue_page_add_appendix.php?PageID='+pageID+'&Position='+position+'&NewspaperID=<?=$NewspaperID?>&TB_iframe=true&width=850&height=550';
		tb_show(title, url);
	}	
	function go_select_article(pageID,position){
		var title = '';
		var url   = 'issue_page_select_article.php?PageID='+pageID+'&Position='+position+'&NewspaperID=<?=$NewspaperID?>&TB_iframe=true&width=850&height=550';
		tb_show(title, url);
	}		
	function updateIssue(){
		var page_id = $('#PageID').val();
		$.post("/home/ePost/newspaper/ajax_gen_page.php", { task:'update_issue',NewspaperID:<?=$NewspaperID?>,PageID:page_id},  
			function(xml, textStatus){
				var folder_title		= $(xml).find("folder_title").text();
				var newspaper_title 	= $(xml).find("newspaper_title").text();
				var folder_id			= $(xml).find("folder_id").text();
				var cpage_id			= $(xml).find("cpage_id").text();
				var newspaper_body_html = $(xml).find("newspaper_body_html").text();

				$('#FolderID').val(folder_id);
				$('#div_post_issue_title_1').html(folder_title+' - <span>'+newspaper_title+' </span>');
				$('.edit_page_panel_right').html(newspaper_body_html);	
				$("li.selected_page").removeClass('selected_page');
				$('#li_page_'+cpage_id).addClass('selected_page');	
			}		
		);
	}
	function updateIssueSkin(){
		var page_id = $('#PageID').val();
		$.post("/home/ePost/newspaper/ajax_gen_page.php", { task:'update_issue_skin',NewspaperID:<?=$NewspaperID?>,PageID:page_id},  
			function(xml, textStatus){
				var newspaper_body_html = $(xml).find("newspaper_body_html").text();
				$('.edit_page_panel_right').html(newspaper_body_html);	
			}		
		);
	}	
	function edit_issue(){
		var obj = document.form1;
		obj.action = 'issue_publish_status_update.php';
		obj.submit();
	}
	function show_enjoy_list(PageID, Position, RecordID){
		$('#comment_' + PageID + '_' + Position).css('visibility', 'hidden');
		if($('#enjoy_ppl_' + PageID + '_' + Position).css('visibility')=='visible'){
			$('#enjoy_ppl_' + PageID + '_' + Position).css('visibility', 'hidden');
		}else{
			$('#enjoy_ppl_' + PageID + '_' + Position).css('visibility', 'visible');
		}
	}
	function updateStudentComment(e,rid){
		var charCode = e.which || e.keyCode;
		if (charCode == 13) {
			if(e.preventDefault){ 
				e.preventDefault();
			}else{
				e.returnValue = false;
			}
			var comment = $('#studentcomment_'+rid).val();
			
			if(comment!=''){
			    $.post(
					'ajax_enjoy_article.php',
					{
						'action'  : 'ShowArticleComments',
						'RecordID': rid,
						'Comment' : comment
					},
					function(data){
						var result 	  = data.split('|=|')[0];
						var content = data.split('|=|')[1];
						var commentcnt = content.split('|=@@@=|')[0];
						var comment = content.split('|=@@@=|')[1];
						if(result){
							$('div#comment_list_'+rid+' ul').html(comment);
							$('#studentcomment_'+rid).val('');
							$('#a_studentcomment_'+rid).html('<?=$Lang['ePost']['Comment']?> (' + commentcnt + ')');
						}
						else
							alert('<?=$Lang['ePost']['Warning']['FailedToLoadComment']?>');
					}
				);
			}
		}
		return false;
	}	
	function show_teacher_comment(PageID, Position, ShowHide){
		$('#enjoy_ppl_' + PageID + '_' + Position).css('visibility', 'hidden');
		$('#comment_'   + PageID + '_' + Position).css('visibility', (ShowHide=='hide'? 'hidden': 'visible'));
	}	
	function go_delete_comment(aid,cid){
		<?php if(!empty($UserID)){ ?>
		if(confirm('<?=$Lang['ePost']['Confirm']['RemoveStudentComment']?>')){
			$.post(
				'ajax_enjoy_article.php',
				{
					'action'  : 'DeleteArticleComment',
					'CommentID': cid
				},
				function(data){
					var result 	  = data.split('|=|')[0];
					var content = data.split('|=|')[1];
					if(result){
						$('#li_comment_'+cid).remove();
						$('#a_studentcomment_'+aid).html('<?=$Lang['ePost']['Comment']?> (' + content + ')');
					}
				}
			);
		}
		<?php }?>
		return false;
	}	
	function enjoy_article(RecordID){
		$.post(
				'ajax_enjoy_article.php',
				{
					'action' : 'ShowBoardEnjoyArticle',
					'RecordID' : RecordID
				},
				function(data){
					var result 	  = data.split('|=|')[0];
					var ppl_enjoy_btn = data.split('|=|')[1];
					var ppl_enjoy_list = data.split('|=|')[2];
					if(result){
						$("#div_enjoy_btn_"+RecordID).html(ppl_enjoy_btn);
						$("#div_enjoy_list_"+RecordID).html(ppl_enjoy_list);
					}
					else
						alert('<?=$Lang['ePost']['Warning']['FailtoLoadEnjoyArticle']?>');
				}
		);
	}
</script>
<form id="form1" name="form1" method="post" style="margin:0;padding:0">
	<div class="edit_page_panel_left" id="sidebar">
    	<div class="page_list">
        </div>
    </div>
    <div class="edit_page_panel_right">
    <!-- ################ Start of Newspaper ####################-->
	
    <!-- ################ End of Newspaper ####################-->
    </div>
	
    <div class="edit_page_header">
    	<div class="post_issue_title" id="div_post_issue_title_1"> <?=$ePostNewspaper->Folder_Title?>  - <span><?=$ePostNewspaper->Newspaper_Title?> </span></div>
		<div class="table_row_tool" > 
			<div class="row_tool_group">
				<a href="#" title="Edit" class="tool_edit" onclick="MM_showHideLayers('edit_tool_layer','','show')">&nbsp;</a>
				<div class="row_tool_layer" id="edit_tool_layer" onclick="MM_showHideLayers('edit_tool_layer','','hide')">
					<a href="javascript:edit_issue_info();"><?=$Lang['ePost']['EditInformation']?></a>
					<a href="javascript:edit_issue_design()"><?=$Lang['ePost']['EditSkin']?></a>
				 </div>
			</div> 
			
		</div>
        <div class="post_issue_title"><span><em> | &nbsp;</em> <?=$Lang['ePost']['Status']?> : 

			<select name="PublishStatus" id="PublishStatus">
				<option value="<?=$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft']?>" <?=$PublishStatus==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft']? "selected":""?>><?=$Lang['ePost']['Filter']['Draft']?></option>
				<option value="<?=$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['WaitApproval']?>" <?=$PublishStatus==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['WaitApproval']? "selected":""?>><?=$Lang['ePost']['Filter']['WaitingApproval']?></option>
				<?php if(($NewspaperObj->Newspaper_PublishStatus==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft'])  && $ePostUI->user_obj->isTeacherStaff() && $ePostUI->is_publish_manager){?>
				<option value="<?=$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Published']?>" <?=$PublishStatus==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Published']? "selected":""?>><?=$Lang['ePost']['Filter']['Published']?></option>
				<?php } ?>
			</select>
		</span>  
		</div>    			
        <div class="edit_page_btn"><input name="finish_btn" type="button" class="formbutton" onclick="edit_issue();" value="<?=$Lang['ePost']['Button']['Finish']?>" />  </div>
    </div>
<div class="edit_page_tool"> <?php if($ePostUI->user_obj->isTeacherStaff() || $ePostUI->is_editor){ ?><div class="Content_tool"><a href="javascript:add_new_page('');"><?=$Lang['ePost']['NewPage']?></a> </div><?php } ?><span class="page_total"> <?=$Lang['ePost']['Total']?> : <span id="span_page_total"></span></span>
</div>
		<input type="hidden" id="FolderID" name="FolderID" value="<?=$ePostNewspaper->FolderID?>"/>
		<input type="hidden" id="NewspaperID" name="NewspaperID" value="<?=$ePostNewspaper->NewspaperID?>"/>
		<input type="hidden" id="PageID" name="PageID" value=""/>
    </div>
    <p class="spacer"></p> 

<?php
include($intranet_root.'/home/ePost/newspaper/templates/tpl_issue_footer.php');
?>