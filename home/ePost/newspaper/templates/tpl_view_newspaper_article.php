<?php 
$HeaderOnly = true;
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>

<body class="bg3_green_body newspaper_body_bg">
	<script>
		function show_teacher_comment(PageID, Position, ShowHide){
			$('#enjoy_ppl_' + PageID + '_' + Position).css('visibility', 'hidden');
			$('#comment_'   + PageID + '_' + Position).css('visibility', (ShowHide=='hide'? 'hidden': 'visible'));
		}
		
		function show_enjoy_list(PageID, Position, RecordID, ShowHide){
			$('#comment_' + PageID + '_' + Position).css('visibility', 'hidden');
			
			if(ShowHide == 'show'){
				if($('#enjoy_ppl_' + PageID + '_' + Position).html()==''){
					$.post(
							'ajax_enjoy_article.php',
							{
								'action'   : 'EnjoyList',
								'PageID'   : PageID,
								'RecordID' : RecordID,
								'Position' : Position,
								'ShowAll'  : 1
							},
							function(data){
								var result = data.split('|=|')[0];
								var list   = data.split('|=|')[1];
							
								if(result)
									$('#enjoy_ppl_' + PageID + '_' + Position).html(list);
								else
									alert('Fail to Load Enjoy List!');
							}
					);
				}
				
				$('#enjoy_ppl_' + PageID + '_' + Position).css('visibility', 'visible');
			}
			else{
				$('#enjoy_ppl_' + PageID + '_' + Position).css('visibility', 'hidden');
			}
		}
		
		function enjoy_article(RecordID){
			<?php if(!empty($UserID)){ ?>
			$.post(
					'ajax_enjoy_article.php',
					{
						'action' : 'ShowBoardEnjoyArticle',
						'RecordID' : RecordID
					},
					function(data){
						var result 	  = data.split('|=|')[0];
						var ppl_enjoy = data.split('|=|')[1];
						
						if(result){
							var enjoy_ppl_link_title = ppl_enjoy > 0? (ppl_enjoy + (ppl_enjoy > 1? ' people enjoy':' person enjoys')) : 'No one enjoys';
							
							$('[name="enjoy_ppl_link_'  + RecordID + '"]').attr('title', enjoy_ppl_link_title);
							$('[name="enjoy_ppl_link_'  + RecordID + '"]').html('(' + ppl_enjoy + ')');
							$('[name="enjoy_ppl_board_' + RecordID + '"]').html('');
						}
						else
							alert('Fail to Load Enjoy Article!');
					}
			);
			<?php } else {?>
			alert('Please login in order to enjoy this article.');
			<?php } ?>
		}
	</script>
	<div class="newspaper_big_page" style="background:#EEEEEE; border:2px solid #666666; min-height:383px; padding-bottom: 125px">
		<?=$article_html?>
	</div>
</body>

<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>