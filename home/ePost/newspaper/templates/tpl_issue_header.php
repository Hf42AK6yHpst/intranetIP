<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?=$Lang['ePost']['ePost']?></title>
	
	<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/jquery/jquery.datepick.css">
	<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/jquery/thickbox.css">
	<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/<?=$LAYOUT_SKIN?>/css/ePost_new.css"/>
	<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/<?=$LAYOUT_SKIN?>/css/ePost_newspaper_new.css"/>
	<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/<?=$LAYOUT_SKIN?>/css/ePost_print.css" media="print"/>
	<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/jquery/jquery.fancybox.css" media="screen" />	
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/script.js"></script>
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/swf_object/swfobject.js"></script>
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/jquery.blockUI.js"></script>
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/thickbox.js"></script>
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/jquery.fancybox.pack.js"></script>
	<script language="JavaScript" src="<?=$intranet_httppath?>/templates/jquery/ui/ui.core_dd.js"></script>
	<script language="JavaScript" src="<?=$intranet_httppath?>/templates/jquery/ui/ui.sortable_dd.js"></script>
	<script language="JavaScript" src="<?=$intranet_httppath?>/templates/jquery/ui/ui.draggable_dd.js"></script>
	<script language="JavaScript" src="<?=$intranet_httppath?>/templates/jquery/ui/ui.droppable_dd.js"></script>	
</head>
