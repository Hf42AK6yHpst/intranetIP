<?php
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>

<script>
	function go_issue(){
		var obj = document.form1;
		
		obj.action = 'issue_page_list.php';
		obj.submit();
	}
	
	function create_new_page(){
		var obj = document.form1;
		
		obj.PageID.value = '';
		
		obj.action = 'issue_page_new.php';
		obj.submit();
	}
	
	function change_page_layout_theme(){
		var obj = document.form1;
		
		obj.action = 'issue_page_new.php';
		obj.submit();
	}
	
	function go_delete_article(ArticleID){
		var ArticleTitle = document.getElementById('Article_title_' + ArticleID).innerHTML;
		
		if(confirm('<?=$Lang['ePost']['Confirm']['RemoveArticle'][0]?>' + ArticleTitle + '<?=$Lang['ePost']['Confirm']['RemoveArticle'][1]?>')){
			var obj = document.form1;
			
			obj.ArticleID.value = ArticleID;
			
			obj.action = "issue_page_delete_article.php";	
			obj.submit();	
		}
	}
	
	function show_teacher_comment(PageID, Position, ShowHide){
		$('#enjoy_ppl_' + PageID + '_' + Position).css('visibility', 'hidden');
		$('#comment_'   + PageID + '_' + Position).css('visibility', (ShowHide=='hide'? 'hidden': 'visible'));
	}
	
	function show_enjoy_list(PageID, Position, RecordID, ShowHide){
		$('#comment_' + PageID + '_' + Position).css('visibility', 'hidden');
		
		if(ShowHide == 'show'){
			if($('#enjoy_ppl_' + PageID + '_' + Position).html()==''){
				$.post(
					'ajax_enjoy_article.php',
					{
						'action'   : 'EnjoyList',
						'PageID'   : PageID,
						'RecordID' : RecordID,
						'Position' : Position,
						'ShowAll'  : 1
					},
					function(data){
						var result = data.split('|=|')[0];
						var list   = data.split('|=|')[1];
						
						if(result)
							$('#enjoy_ppl_' + PageID + '_' + Position).html(list);
						else
							alert('Fail to Load Enjoy List!');
					}
				);
			}
			
			$('#enjoy_ppl_' + PageID + '_' + Position).css('visibility', 'visible');
		}
		else{
			$('#enjoy_ppl_' + PageID + '_' + Position).css('visibility', 'hidden');
		}
	}
	
	function enjoy_article(RecordID){
		$.post(
				'ajax_enjoy_article.php',
				{
					'action' : 'ShowBoardEnjoyArticle',
					'RecordID' : RecordID
				},
				function(data){
					var result 	  = data.split('|=|')[0];
					var ppl_enjoy = data.split('|=|')[1];
					
					if(result){
						var enjoy_ppl_link_title = ppl_enjoy > 0? (ppl_enjoy + (ppl_enjoy > 1? ' people enjoy':' person enjoys')) : 'No one enjoys';
						
						$('[name="enjoy_ppl_link_'  + RecordID + '"]').attr('title', enjoy_ppl_link_title);
						$('[name="enjoy_ppl_link_'  + RecordID + '"]').html('(' + ppl_enjoy + ')');
						$('[name="enjoy_ppl_board_' + RecordID + '"]').html('');
					}
					else
						alert('Fail to Load Enjoy Article!');
				}
		);
	}
</script>

<form name="form1" action="" method="post" style="margin:0;padding:0">
	<p class="spacer">&nbsp;</p>
	<?=$ePostUI->gen_admin_newspaper_page_nav(0)?>
	<p class="spacer">&nbsp;</p>
	<?=$ePostUI->gen_portal_page_nav($nav_ary)?>
	<br />
	<p class="spacer">&nbsp;</p>
		<div class="marking_board">
			<br />
			<div class="stepboard stepboard_2s">
				<h1><?=$Lang['ePost']['Step']?>:</h1>
				<ul> 
					<li>
						<a href="javascript:void(0)" onclick="change_page_layout_theme()">
							<div>
								<em>1</em>
								<span><?=$Lang['ePost']['SelectThemeLayout']?></span>
							</div>
						</a>
					</li>
					<li class="stepon">
						<div>
							<em>2</em>
							<span><?=$Lang['ePost']['SelectArticles']?></span>
						</div>
					</li>
				</ul> 
				<br style="clear:both" />
			</div>
			<p class="spacer"></p>
			<?=$ePostUI->gen_big_page_layout($ePostPage, true, true)?>
			<p class="spacer"></p>
		</div>
		<p class="spacer"></p>
		<div class="edit_bottom">
			<p class="spacer"></p><br />
			<input name="submit_btn" type="button" class="formbutton" onclick="create_new_page()" value="<?=$Lang['ePost']['Button']['CreateAnotherPage']?>" />
			<input name="cancel_btn" type="button" class="formsubbutton" onclick="go_issue()" value="<?=$Lang['ePost']['Button']['BackToIssue']?>" />
		</div>
	</div>
	<input type="hidden" id="FolderID" name="FolderID" value="<?=$ePostNewspaper->FolderID?>"/>
	<input type='hidden' id='NewspaperID' name='NewspaperID' value='<?=$ePostPage->NewspaperID?>'/>
	<input type='hidden' id='PageID' name='PageID' value='<?=$ePostPage->PageID?>'/>
	<input type='hidden' id='ArticleID' name='ArticleID' value=''/>
</form>

<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>