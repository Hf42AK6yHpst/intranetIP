<?php 
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>

<body class="<?=$PageObj->get_page_body_class()?> newspaper_body_bg newspaper_print_01">
	<div class="<?=$PageObj->get_page_top_left_class()?>">
		<div class="<?=$PageObj->get_page_top_right_class()?>">
			<div class="<?=$PageObj->get_page_top_bg_class()?>">
				<div class="<?=$PageObj->get_page_bg_class()?>">
					<form name="form1" method="post" style="padding:0; margin:0;">
						<script>
							function updateStudentComment(e,rid){
								var charCode = e.which || e.keyCode;
								if (charCode == 13) {
									if(e.preventDefault){ 
										e.preventDefault();
									}else{
										e.returnValue = false;
									}
									var comment = $('#studentcomment_'+rid).val();
									
									if(comment!=''){
									    $.post(
											'ajax_enjoy_article.php',
											{
												'action'  : 'ShowArticleComments',
												'RecordID': rid,
												'Comment' : comment
											},
											function(data){
												var result 	  = data.split('|=|')[0];
												var content = data.split('|=|')[1];
												var commentcnt = content.split('|=@@@=|')[0];
												var comment = content.split('|=@@@=|')[1];
												if(result){
													$('div#comment_list_'+rid+' ul').html(comment);
													$('#studentcomment_'+rid).val('');
													$('#a_studentcomment_'+rid).html('<?=$Lang['ePost']['Comment']?> (' + commentcnt + ')');
												}
												else
													alert('<?=$Lang['ePost']['Warning']['FailedToLoadComment']?>');
											}
										);
									}
								}
								return false;
							}
							function show_teacher_comment(PageID, Position, ShowHide){
								$('#enjoy_ppl_' + PageID + '_' + Position).css('visibility', 'hidden');
								$('#comment_'   + PageID + '_' + Position).css('visibility', (ShowHide=='hide'? 'hidden': 'visible'));
							}
							
							function show_enjoy_list(PageID, Position, RecordID){
								$('#comment_' + PageID + '_' + Position).css('visibility', 'hidden');
								if($('#enjoy_ppl_' + PageID + '_' + Position).css('visibility')=='visible'){
									$('#enjoy_ppl_' + PageID + '_' + Position).css('visibility', 'hidden');
								}else{
									$('#enjoy_ppl_' + PageID + '_' + Position).css('visibility', 'visible');
								}
							}
							function go_delete_comment(aid,cid){
								<?php if(!empty($UserID)){ ?>
								if(confirm('<?=$Lang['ePost']['Confirm']['RemoveStudentComment']?>')){
									$.post(
										'ajax_enjoy_article.php',
										{
											'action'  : 'DeleteArticleComment',
											'CommentID': cid
										},
										function(data){
											var result 	  = data.split('|=|')[0];
											var content = data.split('|=|')[1];
											if(result==1){
												$('#li_comment_'+cid).remove();
												$('#a_studentcomment_'+aid).html('<?=$Lang['ePost']['Comment']?> (' + content + ')');
											}
										}
									);
								}
								<?php }?>
								return false;
							}
							function enjoy_article(RecordID){
								<?php if(!empty($UserID)){ ?>
								$.post(
										'ajax_enjoy_article.php',
										{
											'action' : 'ShowBoardEnjoyArticle',
											'RecordID' : RecordID
										},
										function(data){
											var result 	  = data.split('|=|')[0];
											var ppl_enjoy_btn = data.split('|=|')[1];
											var ppl_enjoy_list = data.split('|=|')[2];
											if(result){
												$("#div_enjoy_btn_"+RecordID).html(ppl_enjoy_btn);
												$("#div_enjoy_list_"+RecordID).html(ppl_enjoy_list);
											}
											else
												alert('<?=$Lang['ePost']['Warning']['FailtoLoadEnjoyArticle']?>');
										}
								);
								<?php } else {?>
								alert('<?=$Lang['ePost']['Warning']['LogintoLoadEnjoyArticle']?>');
								<?php } ?>
							}
						</script>
						<?=$PageHTML?>
						
						<?php if($IsPublicLink){ ?>
							<input type="hidden" id="r" name="r" value="<?=$r?>"/>
						<?php } else {?>
							<input type="hidden" id="NewspaperID" name="NewspaperID" value="<?=$NewspaperID?>"/>
						<?php } ?>
						
						<input type="hidden" id="PageOrder" name="PageOrder" value="<?=$PageOrder?>"/>
						<input type="hidden" id="IsZoomOut" name="IsZoomOut" value="<?=$IsZoomOut?>"/>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>

<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>