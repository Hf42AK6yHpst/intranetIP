<?php 
# using :Paul
/* 
 * Modification Log:
 * 2019-06-20 (Paul) [ip.2.5.10.6.1]
 * 	- fix the security fix trimming the "NEW" in AttachmentID into 0 causing failure in uploading attachment by changing AttachmentID to AttachmentIDs
 * 
 */
$HeaderOnly = true;
include($intranet_root.'/home/ePost/templates/portal_head.php');

$IsImage = false;
$IsVideo = false;
$attachmentAry = array();
$attachmentType = '';
$i=0;
foreach((array)$ePostAttachmentAry as $_attachmentId => $_attachmentPathAry){
	$_attachment = $_attachmentPathAry['attachment'];
	$ext = strtoupper(substr($_attachment, '-4'));	
	
	if(in_array($ext, array('.JPG','.GIF','.JPE','.PNG','.BMP'))){
		$attachmentType = 'image';
	}elseif(in_array($ext, array('.MOV','.MP4','.FLV'))){
		$attachmentType = 'video';
	}else{
		$attachmentType = '';
	}
	$attachment_file_path = $ePostArticle->format_attachment_path($_attachment,false);
	if(file_exists($attachment_file_path)&&!empty($attachmentType)){ 
		$attachment_http_path = $ePostArticle->format_attachment_path($_attachment,true);
		
		$_caption = $_attachmentPathAry['caption'];
		$_alignment = $_attachmentPathAry['alignment'];
		$_size = $_attachmentPathAry['size'];
		$_status = $_attachmentPathAry['status']!==false?$_attachmentPathAry['status']:$cfg_ePost['BigNewspaper']['Article_DisplayAttachment']['Yes'];
		$attachmentHTML = "<div class=\"tool_attacment_file\"><a href=\"".$attachment_http_path."\" class=\"att_".$attachmentType." attachment_".$attachmentType."\" target=\"_BLANK\">";
		if($attachmentType == 'video'){
			$attachmentHTML .= $ePostUI->gen_video_player($attachment_http_path);
		}else{
			$attachmentHTML .= $_attachment;
		}
		$attachmentHTML .= "</a></div>";
		$attachmentHTML .= "<div class=\"table_row_tool\"><a href=\"javascript:void(0);\" class=\"tool_delete\" onclick=\"removeArticleAttachment(".$i.");\";></a></div>";
		$attachmentHTML .= "<p class=\"spacer\"></p>";
       	$attachmentHTML .= "<div id=\"Div_DisplayAttachment".$i."_Detail\" class=\"att_detail\" ".($_status?"":"style=\"display:none;\"").">";
        $attachmentHTML .= "<ul>";
       	$attachmentHTML .= "<li>";
        $attachmentHTML .= "<span>".$Lang['ePost']['Width']." </span><em>:</em>";
        foreach($cfg_ePost['BigNewspaper']['AttachmentLayout']['SizeToCss'] as $__cssSize => $__css){
        	$isChecked = $__cssSize==$_size?'checked':'';
        	$attachmentHTML .= "<input type=\"radio\" name=\"Attachment_Width".$i."\" id=\"Attachment_Width".$i."_".$__cssSize."\" value=\"".$__cssSize."\" ".$isChecked."/>";
        	$attachmentHTML .= "<label for=\"Attachment_Width".$i."_".$__cssSize."\">".$__cssSize."%</label>";
        }
        $attachmentHTML .= "</li>";
        $attachmentHTML .= "<li><span>".$Lang['ePost']['Alignment']."</span><em>:</em>";
        $attachmentHTML .= "<input type=\"radio\" id=\"Alignment".$i."_L\" name=\"Alignment".$i."\" value=\"".$cfg_ePost['BigNewspaper']['Alignment']['Left']."\" ".($_alignment==$cfg_ePost['BigNewspaper']['Alignment']['Left']? "checked":"")." />";
		$attachmentHTML .= "<label for=\"Alignment".$i."_L\">".$Lang['ePost']['Left']."</label>";
		$attachmentHTML .= "<input type=\"radio\" id=\"Alignment".$i."_R\" name=\"Alignment".$i."\" value=\"".$cfg_ePost['BigNewspaper']['Alignment']['Right']."\" ".($_alignment==$cfg_ePost['BigNewspaper']['Alignment']['Right']? "checked":"")."/>";
		$attachmentHTML .= "<label for=\"Alignment".$i."_R\">".$Lang['ePost']['Right']."</label>";
		$attachmentHTML .= "</li>";
        $attachmentHTML .= "<li><span>".$Lang['ePost']['Caption']."</span><em>:</em>";
        $attachmentHTML .= "<input type=\"text\" class=\"textbox\" name=\"Caption".$i."\" id=\"Caption".$i."\" value=\"".$_caption."\"/>";
        $attachmentHTML .= "</li>";
        $attachmentHTML .= "</ul>";
        $attachmentHTML .= "<input type=\"hidden\" class=\"is_attachment\" name=\"AttachmentIDs[".$i."]\" id=\"AttachmentID_".$i."\" value=\"".$_attachmentId."\"/>";
		$attachmentAry[$i] = array(
						"attachment_name"=>$_attachment,
						"attachment"=>$attachmentHTML,
						"caption"=>$_caption,
						"is_show"=>$_status,
						"lang"=>$attachmentType=='image'?$Lang['ePost']['Image'.($i+1)]:$Lang['ePost']['Video']
					);
		
		$i++;			
	}
}
$attachmentCnt = count($attachmentAry);
if($attachmentType == 'video' && $attachmentCnt>0 || $attachmentType == 'image' && $attachmentCnt>1){
	$displayAddMoreButton = true;
}
# Get Attachment Alignment
$Alignment = is_object($ePostArticle)? $ePostArticle->Article_Alignment : $cfg_ePost['BigNewspaper']['Alignment']['Left'];

# Get Attachment Size
$Size = is_object($ePostArticle) && $ePostArticle->Article_Size? $ePostArticle->Article_Size : 'Large';

# Set $image_para_ary for gen_image_video_size_option()
$image_para_ary = array();
$image_para_ary['Type'] 	   = 'IMG';
$image_para_ary['ArticleCfg']  = $article_cfg;
$image_para_ary['Name']		   = 'ImageSize';
$image_para_ary['DefaultSize'] = $Size;

# Set $video_para_ary for gen_image_video_size_option()
$video_para_ary = array();
$video_para_ary['Type'] 	   = 'FLV';
$video_para_ary['ArticleCfg']  = $article_cfg;
$video_para_ary['Name']		   = 'VideoSize';
$video_para_ary['DefaultSize'] = $Size;
?>

<body id="edit_pop" class="edit_pop_page_select">
	<script>
		var skip_fileupload_checking = <?=$attachment? 1:0;?>;
		var image_lang = ['<?=$Lang['ePost']['Image1']?>','<?=$Lang['ePost']['Image2']?>'];
		var video_lang = ['<?=$Lang['ePost']['Video']?>'];

		function change_article_theme_type(article_theme_type){
			$.post(
					'ajax_gen_filter.php',
					{
						'Type'				 : article_theme_type,
						'FilterType[]'		 : ['article_theme_theme_filter', 'article_theme_color_filter'],
						'otherAttr_ary[]'	 : ['onchange="change_article_theme_theme(this.value)"', 'onchange="preview_article_theme()"'],
						'Separator'		   	 : '|=|'
					},
					function(data){
						if(data!='-1'){
							$('#article_theme_theme_filter').html(data.split('|=|')[0]);
							$('#article_theme_color_filter').html(data.split('|=|')[1]);
						}
						preview_article_theme();
					}
			);
		}
		
		function change_article_theme_theme(article_theme_theme){
			var article_theme_type = $('#Type').val();
			
			$.post(
					'ajax_gen_filter.php',
					{
						'Type'				 : article_theme_type,
						'Theme'				 : article_theme_theme,
						'FilterType[]'		 : ['article_theme_color_filter'],
						'otherAttr_ary[]'	 : ['onchange="preview_article_theme()"'],
						'Separator'		   	 : '|=|'
					},
					function(data){
						if(data!='-1'){
							$('#article_theme_color_filter').html(data.split('|=|')[0]);
						}
						preview_article_theme();
					}
			);
		}
		
		function preview_article_theme(){
			var article_theme_type  = $('#Type').val();
			var article_theme_theme = $('#Theme').val(); 
			var article_theme_color = $('#Color').val();
			
			article_theme_type  = article_theme_type?  '_'+article_theme_type  : '';
			article_theme_theme = article_theme_theme? '_'+article_theme_theme : '';
			article_theme_color = article_theme_color? '_'+article_theme_color : '';
			  
			var classname = 'log_header' + article_theme_type + article_theme_theme + article_theme_color;
			
			$('#article_theme_preview').removeClass();
			$('#article_theme_preview').addClass('height_short');
			$('#article_theme_preview').addClass('log_header' + article_theme_type + '_left');
			$('#article_theme_preview').addClass(classname);
		}
		function ToggleDisplay(obj){
			var is_checked = obj.checked;
			var is_attachment = obj.id.indexOf("DisplayAttachment")!=-1?true:false;	
			if(is_checked){
				$("#Td_"+obj.id+"_Show").removeClass().addClass('news_show');
				$("#Td_"+obj.id+"_Content").removeClass();
				if(is_attachment){
					$("#Div_"+obj.id+"_Detail").show();
				}
			}else{
				$("#Td_"+obj.id+"_Show").removeClass().addClass('news_hide');
				$("#Td_"+obj.id+"_Content").removeClass().addClass('news_hide_content');	
				if(is_attachment){
					$("#Div_"+obj.id+"_Detail").hide();
				}				
			}

		}		
		
		function delete_attachment(){
			if(confirm('<?=$Lang['ePost']['Confirm']['RemoveAttachedImageFile']?>')){
				skip_fileupload_checking = 0;
				document.form1.del_attach.value = 1;
				$('#no_attachment').click();
				showHideAttachmentType(true);
			}
		}

		function go_submit(){
			var obj = document.form1;
			
			if(obj.Title.value==''){
				obj.Title.focus();
				alert('<?=$Lang['ePost']['Warning']['InputTitle']?>');
				return false;
			}
			
			if(obj.Content.value==''){
				obj.Content.focus();
				alert('<?=$Lang['ePost']['Warning']['InputContent']?>');
				return false;
			}
			
			var fileupload,ext,isDisplay,this_id,attachment_type;
			var valid = 1;
			if($('.attachment_image').length>0){//image is chosen
				attachment_type = 'image';
			}else if($('.attachment_video').length>0){
				attachment_type = 'video';				
			}
			$('.is_attachment').each(function(){
				if(valid&&$(this).val()=='NEW'){
					this_id = $(this).attr("id").split("_")[1];
					fileupload  = Get_File_Name($('#fileupload'+this_id).val());
					
					ext = fileupload.substr((fileupload.length-4)).toUpperCase();
					if(attachment_type == 'image'){
						if(fileupload){
							ext = fileupload.substr((fileupload.length-4)).toUpperCase();
							if(ext!='.JPG' && ext!='.GIF' && ext!='.JPE' && ext!='.PNG' && ext!='.BMP'){
								alert('<?=$Lang['ePost']['Warning']['UploadImageFile']?>');
								$('#fileupload'+this_id).val('');
								valid = 0;
								
							}
						}else{
							alert('<?=$Lang['ePost']['Warning']['UploadImageFile']?>');
							$('#fileupload'+this_id).val('');
							valid = 0;
						}
					}else{
						if(fileupload==''||(ext!='.FLV'&& ext!='.MOV'&& ext!='.MP4')){ 
							alert('<?=$Lang['ePost']['Warning']['UploadFLVFiles']?>');
							$(this).val('');
							valid = 0;
						}
					}

					
					isDisplay = $("input[name=DisplayAttachment"+this_id+"]").is(':checked');
					if(valid && isDisplay){
						if(!$("input[name=Attachment_Width"+this_id+"]").is(':checked')){
							alert('<?=$Lang['ePost']['Warning']['SelectWidth']?>');
							valid = 0;
						}else if(!$("input[name=Alignment"+this_id+"]").is(':checked')){
							alert('<?=$Lang['ePost']['Warning']['SelectAlignment']?>');
							valid = 0;
						}
					}
				}						
			});
				
			if(!valid) return false;
			
			obj.action = "issue_page_add_appendix_update.php";
			obj.submit();
		}
		
		function go_cancel(){
			parent.window.tb_remove();
		}
		$(document).ready(function(){
			$('a.att_image').click(function(){
				addArticleAttachment('image');
			});
			$('a.att_video').click(function(){
				addArticleAttachment('video');
			});			
			displayAddAttachmentBtn();
		});		
		function displayAddAttachmentBtn(){
			if($('.attachment_video').length>0||$('.attachment_image').length>1){
				$("#tr_add_attachment").hide();
			}else if($('.attachment_image').length==1){
				$("a.att_video").hide();
				$("#tr_add_attachment").show();
			}else{
				$("a.att_video").show();
				$("#tr_add_attachment").show();
			}
			
		}
		function check_select_file_type(id){
			var fileupload = $('#fileupload'+id).val();
			if(fileupload=='') return false;
			
			fileupload = Get_File_Name(fileupload);
			var ext = fileupload.substr((fileupload.length-4)).toUpperCase();
			
			if($('#fileupload'+id).hasClass('attachment_image')){
				if(ext!='.JPG' && ext!='.GIF' && ext!='.JPE' && ext!='.PNG' && ext!='.BMP'){
					alert('<?=$Lang['ePost']['Warning']['UploadImageFile']?>');
					$('#fileupload'+id).val('');
					return false;
				}
			}else if($('#fileupload'+id).hasClass('attachment_video')){
				if(ext!='.FLV'&&ext!='.MOV'&&ext!='.MP4'){
					alert('<?=$Lang['ePost']['Warning']['UploadFLVFiles']?>');
					$('#fileupload'+id).val('');
					return false;
				}
			}
			return true;
		}			
		function CheckAllowShowAttachment(cur_id){
			if(!check_select_file_type(cur_id)) return false;
			var obj = document.getElementById("DisplayAttachment"+cur_id);
			if($("#fileupload"+cur_id).val()!=''){
				$("#DisplayAttachment"+cur_id).attr("disabled","");
				obj.checked = true;
				ToggleDisplay(obj);
			}else{
				obj.checked = false;
				ToggleDisplay(obj);
				$("#DisplayAttachment"+cur_id).attr("disabled","disabled");
			}
		}	
		function removeAttachmentBox(type,idx){
			$("#tr_attachment_"+idx).remove();
			var attachmentCnt = $('.attachment_'+type).length;
			if(attachmentCnt==0){
				$("a.att_video").show();
			}
			MM_showHideLayers('div_add_attachment_box','','hide');
			$("#tr_add_attachment").show();
		}
		function updateAttachmentTitle(){
			if($('.attachment_video').length>0){
				$('.attachment_video').each(function(i){
					$(this).closest('tr').find('.td_attachment_title_1').html(video_lang[i]);
				});
			}
			if($('.attachment_image').length>0){
				$('.attachment_image').each(function(i){
					$(this).closest('tr').find('.td_attachment_title_1').html(image_lang[i]);
				});
			}			
			
		}
		function removeArticleAttachment(idx){
			$("#tr_attachment_"+idx).remove();
			updateAttachmentTitle();
			displayAddAttachmentBtn();
		}		
		function addArticleAttachment(type){
			if(type!='image' && type!='video') return;
			var idx = parseInt($('#AddAttachmentCnt').val());
			var attachmentCnt = $('.attachment_'+type).length;
			var html = "";
			var attachment_html = "";
			attachment_html += "<div class=\"tool_attacment_file\">";
				attachment_html += "<input type=\"file\" class=\"attachment_"+type+"\" id=\"fileupload"+idx+"\" name=\"fileupload"+idx+"\" onchange=\"CheckAllowShowAttachment("+idx+");\"/>";
			attachment_html += "</div>";
			attachment_html += "<div class=\"table_row_tool\"><a href=\"javascript:void(0);\" class=\"tool_delete\" onclick=\"removeAttachmentBox('"+type+"',"+idx+");\";></a></div>";
			attachment_html += "<p class=\"spacer\"></p>";
           	attachment_html += "<div id=\"Div_DisplayAttachment"+idx+"_Detail\" class=\"att_detail\" style=\"display:none;\">";
            	attachment_html += "<ul>";
           			attachment_html += "<li>";
	       				attachment_html += "<span><?=$Lang['ePost']['Width']?></span><em>:</em>";
					<?foreach($cfg_ePost['BigNewspaper']['AttachmentLayout']['SizeToCss'] as $__cssSize => $__css){?>
        				attachment_html += "<input type=\"radio\" name=\"Attachment_Width"+idx+"\" id=\"Attachment_Width"+idx+"_<?=$__cssSize?>\" value=\"<?=$__cssSize?>\" />";
        				attachment_html += "<label for=\"Attachment_Width"+idx+"_<?=$__cssSize?>\"><?=$__cssSize?>%</label>";
        			<?}?>
    				attachment_html += "</li>";
    				attachment_html += "<li><span><?=$Lang['ePost']['Alignment']?></span><em>:</em>";
    					attachment_html += "<input type=\"radio\" id=\"Alignment"+idx+"_L\" name=\"Alignment"+idx+"\" value=\"<?=$cfg_ePost['BigNewspaper']['Alignment']['Left']?>\" />";
						attachment_html += "<label for=\"Alignment"+idx+"_L\"><?=$Lang['ePost']['Left']?></label>";
						attachment_html += "<input type=\"radio\" id=\"Alignment"+idx+"_R\" name=\"Alignment"+idx+"\" value=\"<?=$cfg_ePost['BigNewspaper']['Alignment']['Right']?>\" />";
						attachment_html += "<label for=\"Alignment"+idx+"_R\"><?=$Lang['ePost']['Right']?></label>";
					attachment_html += "</li>";
			        attachment_html += "<li><span><?=$Lang['ePost']['Caption']?></span><em>:</em>";
			            attachment_html += "<input type=\"text\" class=\"textbox\" name=\"Caption"+idx+"\" id=\"Caption"+idx+"\" />";
			         attachment_html += "</li>";
			    attachment_html += "</ul>";
			    attachment_html += "<input type=\"hidden\" class=\"is_attachment\" name=\"AttachmentIDs["+idx+"]\" id=\"AttachmentID_"+idx+"\" value=\"NEW\"/>";


			if(type=='video'){
				html += "<tr id=\"tr_attachment_"+idx+"\">"
				html += "<td class=\"td_attachment_title_1\"><?=$Lang['ePost']['Attachment']?></td>";
				html += "<td class=\"td_attachment_title_2\">:</td>";
				html += "<td id=\"Td_DisplayAttachment"+idx+"_Content\" class=\"news_hide_content\">"+attachment_html+"</td>";
				html += "<td id=\"Td_DisplayAttachment"+idx+"_Show\" class=\"news_hide\"><label for=\"DisplayAttachment"+idx+"\">";	
				html += "<input type=\"checkbox\" id=\"DisplayAttachment"+idx+"\" name=\"DisplayAttachment"+idx+"\" value=\"1\" onclick=\"ToggleDisplay(this);\" disabled/><?=$Lang['ePost']['Show']?>"; 
				html += "</label></td>";			
			  	html += "</tr>";
				
			}else if(type=='image'){
					html += "<tr id=\"tr_attachment_"+idx+"\">"
				if(attachmentCnt==0){
					html += "<td class=\"td_attachment_title_1\"><?=$Lang['ePost']['Attachment']?></td>";
					html += "<td class=\"td_attachment_title_2\">:</td>";
					MM_showHideLayers('div_add_attachment_box','','hide');
					$(".span_add_attachment").hide();
					$("a.att_video").hide();
				}else{
					html += "<td class=\"td_attachment_title_1\"></td>";
					html += "<td class=\"td_attachment_title_2\">:</td>"	
				}
				html += "<td id=\"Td_DisplayAttachment"+idx+"_Content\" class=\"news_hide_content\">"+attachment_html+"</td>";
				html += "<td id=\"Td_DisplayAttachment"+idx+"_Show\" class=\"news_hide\"><label for=\"DisplayAttachment"+idx+"\">";	
				html += "<input type=\"checkbox\" id=\"DisplayAttachment"+idx+"\" name=\"DisplayAttachment"+idx+"\" value=\"1\" onclick=\"ToggleDisplay(this);\" disabled/><?=$Lang['ePost']['Show']?>"; 
				html += "</label></td>";			
			  	html += "</tr>";				
			}
			$(html).insertBefore($("#tr_add_attachment"));
			updateAttachmentTitle();
			$('#AddAttachmentCnt').val(idx+1);
			displayAddAttachmentBtn();			
		}	
		function updateContentFontStyle(){
			var fontstyle = $("#FontStyle option:selected").text();
			var fontsize = $("#FontSize option:selected").text();
			var lineheight = $("#LineHeight option:selected").text();
			$("#Content").css({"width":"100%","font-family":fontstyle,"font-size":fontsize,"line-height":lineheight});
		}		
	</script>
	<input type="hidden" name="AttachmentCnt" id="AttachmentCnt" value="<?=$i?>">
	<input type="hidden" name="AddAttachmentCnt" id="AddAttachmentCnt" value="<?=$i?>">
	<div class="pop_container">
		<form name="form1" method="post" enctype="multipart/form-data">
			<div class="marking_board">
	        	<span class="sectiontitle"><?=$ePostPage->Newspaper_Title?> - <?=$ePostPage->Newspaper_Name?> - <?=$ePostPage->Page_Name?> - <?=$Lang['ePost']['Article']?></span>
				<p class="spacer"></p>
	          	<table class="form_table form_table_article">
<?/*Author*/?>	          	
	          <?/*		<tr>
						<td><?=$Lang['ePost']['Author']?></td>
						<td>:</td>
						<?$isDisplayAuthor = ($DisplayAuthor==$cfg_ePost['BigNewspaper']['Article_DisplayAuthor']['Yes']);?>
						<td id="Td_DisplayAuthor_Content" class="<?=$isDisplayAuthor? '':'news_hide_content'?>">
							<?=$ePostUI->user_obj->StandardDisplayName?>
						</td>
						<td id="Td_DisplayAuthor_Show" class="<?=$isDisplayAuthor? 'news_show':'news_hide'?>">
						 	<label for="DisplayAuthor">
              					<input type="checkbox" id="DisplayAuthor" name="DisplayAuthor" value="<?=$cfg_ePost['BigNewspaper']['Article_DisplayAuthor']['Yes']?>" <?=$isDisplayAuthor? 'checked':''?> onclick="ToggleDisplay(this);"/><?=$Lang['ePost']['Show']?> 
              				</label>
              			</td>
					</tr>*/?>
<?/*Title*/?>	   					
					<?$isDisplayTitle = ($DisplayTitle==$cfg_ePost['BigNewspaper']['Article_DisplayTitle']['Yes']);?>
					<tr>
       					<td><?=$Lang['ePost']['Title']?></td>
       					<td>:</td>
						<td id="Td_DisplayTitle_Content" class="<?=$isDisplayTitle? '':'news_hide_content'?>">
							<input type="text" id="Title" name="Title" style="width:100%" value="<?=$ePostArticle->ArticleID? $ePostArticle->Article_Title:""?>"/>
						</td>
						<td id="Td_DisplayTitle_Show" class="<?=$isDisplayTitle? 'news_show':'news_hide'?>">
						 	<label for="DisplayTitle">
								<input type="checkbox" class="display_option" id="DisplayTitle" name="DisplayTitle" value="<?=$cfg_ePost['BigNewspaper']['Article_DisplayTitle']['Yes']?>" <?=$isDisplayTitle? 'checked':''?> onclick="ToggleDisplay(this);" /><?=$Lang['ePost']['Show']?> 
							</label>
						</td>						
     				</tr>
<?/*Content*/?>     				
     				<?$isDisplayContent = ($DisplayContent==$cfg_ePost['BigNewspaper']['Article_DisplayContent']['Yes']);?>
     				<tr>
      					<td><?=$Lang['ePost']['Content']?></td>
       					<td>:</td>
       					<td id="Td_DisplayContent_Content" class="<?=$isDisplayContent? '':'news_hide_content'?>">
       						<textarea type="text" id="Content" name="Content" style="width:100%;<?=$fontcss?>" rows="8"><?=$ePostArticle->ArticleID? $ePostArticle->Article_Content:""?></textarea>
       						<?=$fontSettingHtml?>
       					</td>
						<td id="Td_DisplayContent_Show" class="<?=$isDisplayContent? 'news_show':'news_hide'?>">
						 	<label for="DisplayContent">
								<input type="checkbox" class="display_option" id="DisplayContent" name="DisplayContent" value="<?=$cfg_ePost['BigNewspaper']['Article_DisplayContent']['Yes']?>" <?=$isDisplayContent? 'checked':''?> onclick="ToggleDisplay(this);" /><?=$Lang['ePost']['Show']?> 
							</label>
						</td>       					
					</tr>
<?/*Attachment*/?>					
						<?php 
							foreach($attachmentAry as $_idx => $_attachmentPathAry){
								$_isShow = $_attachmentPathAry['is_show'];
								$_attachment = $_attachmentPathAry['attachment'];
								$_lang = $_attachmentPathAry['lang'];
						?>
							<tr id="tr_attachment_<?=$_idx?>">
								<td class="td_attachment_title_1"><?=$_lang?></td>
								<td class="td_attachment_title_2">:</td>
								<td id="Td_DisplayAttachment<?=$_idx?>_Content" class="<?=$_isShow? '':'news_hide_content'?>"><?=$_attachment?></td>
								<td id="Td_DisplayAttachment<?=$_idx?>_Show" class="<?=$_isShow? 'news_show':'news_hide'?>"><label for="DisplayAttachment<?=$_idx?>">
									<input type="checkbox" id="DisplayAttachment<?=$_idx?>" name="DisplayAttachment<?=$_idx?>" value="1" <?=$_isShow? 'checked':''?>  onclick="ToggleDisplay(this);"/><?=$Lang['ePost']['Show']?> 
								</label></td> 
							</tr>
						<?php  } ?>
						<tr id="tr_add_attachment">
						    <td><span class="span_add_attachment"><?=$Lang['ePost']['Attachment']?></span></td>
						    <td><span class="span_add_attachment">:</span></td>
						    <td class="news_hide_content">
						    	<div id="div_add_attachment_btn" class="Content_tool"><a href="javascript:void(0);" class="tool_new" onclick="MM_showHideLayers('div_add_attachment_box','','show');"><?=$Lang['ePost']['Add']?></a></div>   
						        <div id="div_add_attachment_box" class="tool_new_attachment">
						            <a href="javascript:void(0);" class="att_image"><?=$Lang['ePost']['Image']?></a> 
						            <a href="javascript:void(0);" class="att_video"><?=$Lang['ePost']['Video']?></a>            
						        </div>
							</td>
						    <td class="news_hide_content">&nbsp;</td>
						 </tr>
					<tr id="attachment_caption" style='<?=!($IsImage || $IsVideo)? 'display:none':''?>'>
						<td>
							<span id='attachment_caption_image_title' style='<?=$IsImage? '':'display:none'?>'><?=$Lang['ePost']['ImageCaption']?></span>
							<span id='attachment_caption_video_title' style='<?=$IsVideo? '':'display:none'?>'><?=$Lang['ePost']['VideoCaption']?></span>
						</td>
						<td>:</td>
						<td colspan="2">
							<input type="text" id="Caption" name="Caption" style="width:100%" value="<?=$ePostArticle->ArticleID? $ePostArticle->Caption:""?>"/>
						</td>
					</tr>
					<tr id="attachment_alignment" style='<?=!($IsImage || $IsVideo)? 'display:none':''?>'>
						<td>
							<span id='attachment_alignment_image_title' style='<?=$IsImage? '':'display:none'?>'><?=$Lang['ePost']['ImageAlignment']?></span>
							<span id='attachment_alignment_video_title' style='<?=$IsVideo? '':'display:none'?>'><?=$Lang['ePost']['VideoAlignment']?></span>
						</td>
						<td>:</td>
						<td colspan="2">
							<input type="radio" id="Alignment_L" name="Alignment" value="<?=$cfg_ePost['BigNewspaper']['Alignment']['Left']?>"  <?=$Alignment==$cfg_ePost['BigNewspaper']['Alignment']['Left']? "checked":""?>  onclick="changeAttachmentAlignment()"/>
							<label for="Alignment_L"><?=$Lang['ePost']['Left']?></label>
							<input type="radio" id="Alignment_R" name="Alignment" value="<?=$cfg_ePost['BigNewspaper']['Alignment']['Right']?>" <?=$Alignment==$cfg_ePost['BigNewspaper']['Alignment']['Right']? "checked":""?> onclick="changeAttachmentAlignment()"/>
							<label for="Alignment_R"><?=$Lang['ePost']['Right']?></label>
							<input type="radio" id="Alignment_H" name="Alignment" value="<?=$cfg_ePost['BigNewspaper']['Alignment']['Hide']?>"  <?=$Alignment==$cfg_ePost['BigNewspaper']['Alignment']['Hide']? "checked":""?>  onclick="changeAttachmentAlignment()"/>
							<label for="Alignment_H"><?=$Lang['ePost']['Hide']?></label>
						</td>
					</tr>
					<tr id="attachment_size" style='<?=!($IsImage || $IsVideo) || $Alignment==$cfg_ePost['BigNewspaper']['Alignment']['Hide']? 'display:none':''?>'>
						<td>
							<span id='attachment_size_image_title' style='<?=$IsImage? '':'display:none'?>'><?=$Lang['ePost']['ImageSize']?></span>
							<span id='attachment_size_video_title' style='<?=$IsVideo? '':'display:none'?>'><?=$Lang['ePost']['VideoSize']?></span>
						</td>
						<td>:</td>
						<td colspan="2">
							<span id='attachment_size_image_option' style='<?=$IsImage? '':'display:none'?>'><?=$ePostUI->gen_image_video_size_option($image_para_ary)?></span>
							<span id='attachment_size_video_option' style='<?=$IsVideo? '':'display:none'?>'><?=$ePostUI->gen_image_video_size_option($video_para_ary)?></span>
							<input type="hidden" id="Size" name="Size" value="">
						</td>
					</tr>
					<tr>
						<td><?=$Lang['ePost']['Theme']?></td>
						<td>:</td>
						<td colspan="2">
							<span id="article_theme_type_filter" ><?=$ePostUI->gen_article_theme_type_filter('Type', 'Type', $Type, 'onchange="change_article_theme_type(this.value)"')?></span>
							<span id="article_theme_theme_filter"><?=$ePostUI->gen_article_theme_theme_filter($Type, 'Theme', 'Theme', $Theme, 'onchange="change_article_theme_theme(this.value)"')?></span>
							<span id="article_theme_color_filter"><?=$ePostUI->gen_article_theme_color_filter($Type, $Theme, 'Color', 'Color', $Color, 'onchange="preview_article_theme()"')?></span>
         					<div class="newspaper_page select_theme">
         						<div id="article_theme_preview" class="log_header<?=($Type? '_'.$Type:'').($Theme? '_'.$Theme:'').($Color? '_'.$Color:'')?> <?="log_header_".$Type."_left"?> height_short">
          							<div class="log_header">
                        				<div class="log_header_top">
                        					<div class="log_header_top_right">
                        						<div class="log_header_top_bg"></div>
                        					</div>
                        				</div>
										<div class="log_header_body">
											<div class="log_header_body_right">
												<div class="log_header_body_bg">
													<h1><?=$Lang['ePost']['Title']?></h1>
                        						</div>
                        					</div>
                        				</div>
										<div class="log_header_bottom">
											<div class="log_header_bottom_right">
												<div class="log_header_bottom_bg"></div>
											</div>
										</div>
                    				</div>
                    				<div class="log_content_body"></div>
         						</div>
         					</div>
        				</td>
     				</tr>
				</table>
				<p class="spacer"></p>
				<div class="edit_bottom">
	    			<input name="submit2" class="formbutton" onclick="go_submit()" value="<?=$Lang['ePost']['Button']['Submit_1']?>" type="button">
	    			<input name="submit2" class="formsubbutton" value="<?=$Lang['ePost']['Button']['Cancel']?>" type="button" onclick="go_cancel()"/>
	  			</div>
			</div>
	  		<input type="hidden" id="NewspaperID" name="NewspaperID" value="<?=$ePostPage->NewspaperID?>"/>
	  		<input type="hidden" id="PageID" name="PageID" value="<?=$ePostPage->PageID?>"/>
	  		<input type="hidden" id="Position" name="Position" value="<?=$Position?>"/>
	  		<input type="hidden" id="ArticleID" name="ArticleID" value="<?=is_object($ePostArticle)? $ePostArticle->ArticleID : ''?>"/>
	  	</form>
    </div>
</body>

<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>