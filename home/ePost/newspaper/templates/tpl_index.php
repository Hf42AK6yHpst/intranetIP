<?php
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>

<script>
	function go_filter(){
		document.filter_form.submit();
	}
	
	function go_edit(FolderID){
		var obj = document.form1;
		
		obj.action = 'folder_new.php';
		obj.FolderID.value = FolderID;
		obj.submit();
	}
	
	function go_delete(FolderID){
		var obj = document.form1;
		var FolderName = document.getElementById('Folder_title_' + FolderID).innerHTML;
		
		if(confirm('<?=$Lang['ePost']['Confirm']['RemoveFolder'][0]?>'+ FolderName + '<?=$Lang['ePost']['Confirm']['RemoveFolder'][1]?><?=$Lang['ePost']['PublicFolder']?><?=$Lang['ePost']['Confirm']['RemoveFolder'][2]?>')){
			obj.action = 'folder_delete.php';
			obj.FolderID.value = FolderID;
			obj.submit();
		}
	}
	
	function openFolder(FolderID)
	{
		var obj = document.form1;
		
		obj.FolderID.value = FolderID;
		obj.Status.value = '';
		obj.action = 'issue_list.php';
		obj.submit();
	}
	
	function displayCreateNewFolder(){
		var row_no = $('#new_row_no').val();
		$('#div_create_new_folder').hide();
		$('#div_input_new_folder').show();
		$('#tr_create_new_folder').addClass('select_q');
		$('#td_new_row_no').html(row_no);
	}
	
	function add_new_folder(){
		var new_row_no = $('#new_row_no').val();
		var cur_row_no = new_row_no-1;
		var folder_title = $('#FolderTitle').val();
		$.post("/home/ePost/ajax.php", { task:'add_new_issue_folder',new_row_no:new_row_no,folder_title:folder_title},  
			function(data, textStatus){
				var output = data.split('|=|');
				$('#new_row_no').val(output[1]);
				$('#tr_row_'+cur_row_no).after(output[0]);
				$('#div_create_new_folder').show();
				$('#div_input_new_folder').hide();
				$('#FolderTitle').val('');
				$('#td_new_row_no').html('');
				$('#tr_create_new_folder').removeClass('select_q');
			}		
		);
	}
</script>
<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
<p class="spacer">&nbsp; </p>
<p class="spacer">&nbsp; </p>
<?=$ePostUI->gen_portal_page_nav($nav_ary)?>
<p class="spacer">&nbsp; </p>
<form name="filter_form" method="post" action="index.php" style="margin:0;padding:0">
    <div class="content_top_tool">
		<div id="table_filter">
			<select name="Status" id="Status">
				<option value="<?=$cfg_ePost['BigNewspaper']['Folder_filter']['All']?>" <?=$Status==$cfg_ePost['BigNewspaper']['Folder_filter']['All']? "selected":""?>><?=$Lang['ePost']['Filter']['All']?></option>
				<option value="<?=$cfg_ePost['BigNewspaper']['Folder_filter']['FolderWithNewspaper']?>" <?=$Status==$cfg_ePost['BigNewspaper']['Folder_filter']['FolderWithNewspaper']? "selected":""?>><?=$Lang['ePost']['Filter']['FoldersWithNewspaper']?></option>
				<option value="<?=$cfg_ePost['BigNewspaper']['Folder_filter']['EmptyFolder']?>" <?=$Status==$cfg_ePost['BigNewspaper']['Folder_filter']['EmptyFolder']? "selected":""?>><?=$Lang['ePost']['Filter']['EmptyFolders']?></option>
			</select>
			<input name="filter_go_btn" type="button" class="formsmallbutton" value="<?=$Lang['ePost']['Filter']['Go']?>" onclick="go_filter()"/>
	  </div><p class="spacer"></p><br>
    </div>
</form>	
<form name="form1" action="" method="post" style="margin:0;padding:0">
	<?php if(!$ePost->user_obj->isStudent()){ ?>
		<div class="Content_tool">
			<!--a href="folder_new.php" class="new"><?=$Lang['ePost']['CreateNewFolder']?></a-->
			<a href="issue_new.php" class="new"><?=$Lang['ePost']['CreateNewIssue']?></a>
		</div>
	<?php } ?>
	<div id="sys_msg"><?=$Lang['ePost']['ReturnMessage'][$returnMsg]?></div>
    <div class="table_board">
<table class="common_table_list edit_table_list" id="folder_table">
  <col   nowrap="nowrap"/>
  <tr>
	<th width="30">#</th>
	<th><?=$Lang['ePost']['FolderName']?></th>
	<th><?=$Lang['ePost']['NoOfIssue']?></th>
	<!--th><?=$Lang['ePost']['View']?></th-->
	<th><?=$Lang['ePost']['LastModified']?></th>
	<th width="100">&nbsp;</th>
</tr>
<?=$table_html?>
<?php if(!$ePost->user_obj->isStudent()){ ?>
<tr id="tr_create_new_folder">
  <td id="td_new_row_no">&nbsp;</td>
  <td>
	  <div id="div_create_new_folder">
		  <div class="Content_tool">
			<a href="javascript:displayCreateNewFolder();" class="new"><?=$Lang['ePost']['CreateNewFolder']?></a>
		  </div>
	  </div>
	  <div id="div_input_new_folder" style="display:none;">
			<input type="text" name="FolderTitle" id="FolderTitle" />
			<input name="add_btn" type="button" class="formsmallbutton" onclick="add_new_folder();" value="<?=$Lang['Btn']['Add']?>" />
	  </div>
  </td>
  <td>&nbsp;</td>
  <!--td>&nbsp;</td-->
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>
<?php } ?>
</table>
<p class="spacer"></p><p class="spacer"></p><br />
        </div>
		
	<input type="hidden" id="FolderID" name="FolderID" value=""/>
	<input type="hidden" id="Status" name="Status" value="<?=$Status?>"/>
	<?=$new_row_html?>
</form>
</div></div></div>
<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>