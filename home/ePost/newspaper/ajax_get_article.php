<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$article_id = isset($article_id) && $article_id!=''? $article_id : '';
$Separator  = isset($Separator) && $Separator? $Separator : '|=|';

# Initialize Object
$ePostUI = new libepost_ui();
$ePostArticleShelf = new libepost_articleshelf($article_id);

# Check if ePost should be accessible
$ePostUI->auth();

# Initialize $para_ary for $ePostUI->gen_article_display();
$para_ary['Title'] 	 = $ePostArticleShelf->Title;
$para_ary['Content'] = nl2br($ePostArticleShelf->Content);

$para_ary['Attachment'] = array();
$para_ary['Attachment']['Original'] = $ePostArticleShelf->Attachment;
$para_ary['Attachment']['FilePath'] = $ePostArticleShelf->format_attachment_path(false);
$para_ary['Attachment']['HTTPPath'] = $ePostArticleShelf->format_attachment_path(true);
$para_ary['Attachment']['Caption']  = $ePostArticleShelf->Caption;
	
$article_display_ary = $ePostUI->gen_article_display($para_ary);

$result = $article_display_ary['content'].$Separator.$article_display_ary['attachment'].$Separator.$article_display_ary['suggest_height'];

echo $result;
?>