<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$FolderID    = isset($FolderID) && $FolderID!=''? $FolderID : '';
$NewspaperID = isset($NewspaperID) && $NewspaperID!=''? $NewspaperID : '';

# Initialize Object
$ePostNewspaper = new libepost_newspaper($NewspaperID);

# Check if ePost should be accessible
$ePostNewspaper->portal_auth();

# Delete Newspaper
$result = $ePostNewspaper->delete_newspaper();

header('location:issue_list.php?FolderID='.$ePostNewspaper->FolderID.'&msg='.$result);

intranet_closedb();
?>