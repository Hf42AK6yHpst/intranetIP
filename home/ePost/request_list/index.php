<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_request.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$Status = isset($Status) && $Status!=''? $Status : 0;
$returnMsg = isset($returnMsg) && $returnMsg!=''? $returnMsg : "";

# Initialize Object
$ePost   	   = new libepost();
$ePostUI 	   = new libepost_ui();
$ePostRequests = new libepost_requests();

$Header='NEW';
$FromPage = 'editor';
# Check if ePost should be accessible
$ePost->portal_auth();

# Process Table Content HTML
$Requests_ary = $ePostRequests->get_managing_requests();

for($i=0;$i<count($Requests_ary);$i++){
	$RequestObj = $Requests_ary[$i];
	
	$DeadlinePassed = strtotime($RequestObj->Deadline) <= time();
	
	switch($Status){
		case $cfg_ePost['Noncourseware_Request']['Status']['All']: 
			$IsSkipped = false; 
			break;
		case $cfg_ePost['Noncourseware_Request']['Status']['DeadlineNotPassed']: 
			$IsSkipped = $DeadlinePassed; 
			break;
		case $cfg_ePost['Noncourseware_Request']['Status']['DeadlinePassed']: 
			$IsSkipped = !$DeadlinePassed; 
			break;
	}
	
	if(!$IsSkipped){
		$table_html .= "<tr class=\"".($DeadlinePassed? "noraml" : "past")."\">";
		$table_html .= "	<td>".($i+1)."</td>";
		$table_html .= "	<td><a id=\"Request_title_".$RequestObj->RequestID."\" href=\"javascript:openRequest(".$RequestObj->RequestID.", 0)\">".$RequestObj->Topic."</a></td>";
		$table_html .= "	<td>".$cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['types'][$RequestObj->Type]['name']."</td>";
		$table_html .= "	<td>".date('Y-m-d', strtotime($RequestObj->ModifiedDate))." (".$RequestObj->ModifiedByUser.")</td>";
		$table_html .= "	<td>".date('Y-m-d', strtotime($RequestObj->Deadline))."</td>";
		$table_html .= "	<td><a href=\"javascript:openRequest(".$RequestObj->RequestID.", 0)\">".$RequestObj->ArticleSubmitted."</a></td>";
		$table_html .= "	<td><a href=\"javascript:openRequest(".$RequestObj->RequestID.", 1)\">".$RequestObj->ArticleAddedToShelf."</a></td>";
		$table_html .= "	<td>";
		$table_html .= "		<div class=\"table_row_tool\">";
		$table_html .= "			<a href=\"javascript:go_edit(".$RequestObj->RequestID.")\" class=\"tool_edit\" title=\"".$Lang['ePost']['Edit']."\">&nbsp;</a>";
		
		if($ePost->user_obj->isTeacherStaff() && $ePost->is_editor){
			$table_html .= "		<a href=\"javascript:go_delete(".$RequestObj->RequestID.")\" class=\"tool_delete\" title=\"".$Lang['ePost']['Delete']."\">&nbsp;</a>";
		}
		
		$table_html .= "		</div>";
		$table_html .= "	</td>";
		$table_html .= "</tr>";
	}
}

if(!$table_html){
	$table_html  = "<tr>";
	$table_html .= "	<td height='80' class='tabletext' colspan='8' style='text-align:center;vertical-align:middle'>$i_no_record_exists_msg</td>";
	$table_html .= "</tr>";
}
$nav_ary[] = array("title"=>$Lang['ePost']['EditorPage'], "link"=>"../editor.php");
$nav_ary[] = array("title"=>$Lang['ePost']['RequestList'], "link"=>""); 

include($intranet_root.'/home/ePost/request_list/templates/tpl_index.php');

intranet_closedb();
?>