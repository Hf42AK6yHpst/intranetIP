<?php
# using : 

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_request.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_writer.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$RecordID = isset($RecordID) && $RecordID!=''? $RecordID : 0;

# Initialize Object
$ePost		  = new libepost();
$ePostUI	  = new libepost_ui();
$ePostWriter  = new libepost_writer($RecordID);
$ePostRequest = new libepost_request($ePostWriter->RequestID);
$Header = 'NEW';
$FromPage = 'editor';
# Check if ePost should be accessible
$ePost->portal_auth();
# Initialize $para_ary for $ePostUI->gen_article_display();
$para_ary['Title'] 	 = $ePostWriter->Title;
$para_ary['Content'] = nl2br($ePostWriter->Content);

$para_ary['AttachmentAry'] = array();
foreach($ePostWriter->Attachment as $_attachmentId => $_attachmentPathAry){
	$_attachment = $_attachmentPathAry['attachment'];
	$_caption = $_attachmentPathAry['caption'];
	$para_ary['AttachmentAry'][$_attachmentId] = array(
									'Original'	=>	$_attachment,
									'FilePath'	=>	$ePostWriter->format_attachment_path($_attachment,false),
									'HTTPPath'	=>	$ePostWriter->format_attachment_path($_attachment,true),
									'Caption'	=>	$_caption,
									'IsSelectArticlePage' => true
	);
}
	
$article_display_ary = $ePostUI->gen_article_display($para_ary);
# Initialize Navigation Array
$nav_ary[] = array("title"=>$Lang['ePost']['EditorPage'], "link"=>"../editor.php");
$nav_ary[] = array("title"=>$Lang['ePost']['RequestList'], "link"=>"index.php");
$nav_ary[] = array("title"=>$ePostRequest->Topic, "link"=>"request_entries_list.php?RequestID=".$ePostWriter->RequestID); 
$nav_ary[] = array("title"=>$Lang['ePost']['Comment'], "link"=>""); 
include($intranet_root.'/home/ePost/request_list/templates/tpl_comment_record.php');

intranet_closedb();
?>