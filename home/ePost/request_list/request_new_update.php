<?php

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_studenteditor.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$RequestID			   = isset($RequestID) && $RequestID!=''? $RequestID : 0;
$RequestTitle 	 	   = isset($RequestTitle) && $RequestTitle!=''? htmlspecialchars($RequestTitle) : "";
$RequestType 	 	   = isset($RequestType) && $RequestType!=''? $RequestType : "";
$RequestDeadline 	   = isset($RequestDeadline) && $RequestDeadline!=''? ($RequestDeadline." 23:59:59") : "";
$RequestTargetLevel    = isset($RequestTargetLevel) && is_array($RequestTargetLevel)? $RequestTargetLevel : array();
$RequestDesc 	 	   = isset($RequestDesc) && $RequestDesc!=''? htmlspecialchars($RequestDesc) : "";
$StudentEditorSelected = isset($StudentEditorSelected) && $StudentEditorSelected!=''? $StudentEditorSelected : array();

# Initialize Object
$ePost = new libepost();
$ePostStudentEditors = new libepost_studenteditors();

# Check if ePost should be accessible
$ePost->portal_auth();

if($RequestID){
	$sql = "UPDATE
				EPOST_NONCOURSEWARE_REQUEST
			SET
				Topic = '$RequestTitle',
				Type = '$RequestType',
				Deadline = '$RequestDeadline',
				TargetLevel = '".(is_array($RequestTargetLevel)? implode($cfg_ePost['sql_config']['NonCoursewareRequest_TargetLevel_delimiter'], $RequestTargetLevel) : $RequestTargetLevel)."',
				Description = '$RequestDesc',
				ModifiedBy = '$UserID',
				ModifiedDate = NOW()
			WHERE
				RequestID = $RequestID";
	$result = $ePost->db_db_query($sql);
	
	$sql = "SELECT
				DISTINCT ese.UserID
			FROM
				EPOST_STUDENT_EDITOR_MANAGING AS esem INNER JOIN
				EPOST_STUDENT_EDITOR AS ese ON esem.EditorID = ese.EditorID
			WHERE
				ese.Status = ".$cfg_ePost['BigNewspaper']['StudentEditor_status']['exist']." AND
				esem.ManageItemID = $RequestID AND
				esem.ManageType = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Request']."' AND
				esem.Status = ".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist'];
	$CurrentStudentEditor_ary = $ePost->returnVector($sql);
	
	$DeleteStudentEdiotr_ary = array_diff($CurrentStudentEditor_ary, $StudentEditorSelected);
	$InsertStudentEditor_ary = array_diff($StudentEditorSelected, $CurrentStudentEditor_ary);
	
	if((is_array($DeleteStudentEdiotr_ary) && count($DeleteStudentEdiotr_ary)>0) || (is_array($InsertStudentEditor_ary) && count($InsertStudentEditor_ary)>0)){
		for($i=0;$i<count($ePostStudentEditors->StudentEditors);$i++){
			$EditorObj = $ePostStudentEditors->StudentEditors[$i];
			if(in_array($EditorObj->UserID, $DeleteStudentEdiotr_ary)){
				$sql = "UPDATE
							EPOST_STUDENT_EDITOR_MANAGING
						SET
							Status = ".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['deleted'].",
							ModifiedBy = $UserID,
							ModifiedDate = NOW()
						WHERE
							EditorID = ".$EditorObj->EditorID." AND 
							ManageType = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Request']."' AND
							ManageItemID = $RequestID";
				$result = $ePost->db_db_query($sql);
			}
			else if(in_array($EditorObj->UserID, $InsertStudentEditor_ary)){
				$sql = "INSERT INTO
							EPOST_STUDENT_EDITOR_MANAGING
							(
								EditorID, ManageType, ManageItemID, Status, InputDate, ModifiedBy, ModifiedDate
							)
						VALUES
							(
								".$EditorObj->EditorID.", '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Request']."', ".$RequestID.", ".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist'].", NOW(), $UserID, NOW()
							)";
				$result = $ePost->db_db_query($sql);
			}
		}
	}
	$returnMsg = $result?"UpdateSuccess":"UpdateUnsuccess";
}
else{
	$sql = "INSERT INTO
				EPOST_NONCOURSEWARE_REQUEST
				(
					Topic, Type, Deadline, TargetLevel, Description,
					Status, InputDate, ModifiedBy, ModifiedDate
				)
			VALUES
				(
					'$RequestTitle', '$RequestType', '$RequestDeadline', '".(is_array($RequestTargetLevel)? implode($cfg_ePost['sql_config']['NonCoursewareRequest_TargetLevel_delimiter'], $RequestTargetLevel) : $RequestTargetLevel)."', '$RequestDesc',
					".$cfg_ePost['BigNewspaper']['NonCoursewareRequest_status']['exist'].", NOW(), $UserID, NOW()
				)";
	$result = $ePost->db_db_query($sql);
	$RequestID = $ePost->db_insert_id();
	
	if(is_array($StudentEditorSelected) && count($StudentEditorSelected)>0){
		for($i=0;$i<count($ePostStudentEditors->StudentEditors);$i++){
			$EditorObj = $ePostStudentEditors->StudentEditors[$i];
			if(in_array($EditorObj->UserID, $StudentEditorSelected)){
				$sql = "INSERT INTO
							EPOST_STUDENT_EDITOR_MANAGING
							(
								EditorID, ManageType, ManageItemID, Status, InputDate, ModifiedBy, ModifiedDate
							)
						VALUES
							(
								".$EditorObj->EditorID.", '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Request']."', ".$RequestID.", ".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist'].", NOW(), $UserID, NOW()
							)";
				$result = $ePost->db_db_query($sql);
			}
		}
	}
	$returnMsg = $result?"UpdateSuccess":"UpdateUnsuccess";
}

header('location:index.php?returnMsg='.$returnMsg);

intranet_closedb();
?>