<?php
# using : 

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_request.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$RequestID = isset($RequestID) && $RequestID!=''? $RequestID : 0;
$returnMsg = isset($returnMsg) && $returnMsg!=''? $returnMsg : "";

# Initialize Object
$ePost   	  = new libepost();
$ePostUI 	  = new libepost_ui();
$ePostRequest = new libepost_request($RequestID);
$Header='NEW';
$FromPage = 'editor';
# Check if ePost should be accessible
$ePost->portal_auth();
# Prepare Table Content //IF(enw.Attachment IS NOT NULL, '".$Lang['ePost']['Yes']."', '".$Lang['ePost']['No']."') AS Attachment,
$sql = "SELECT
			CONCAT('<a id=\"article_title_', enw.RecordID, '\" href=\"javascript:show_article(', enw.RecordID, ')\">', enw.Title, '</a>') AS Title,
			IF(COUNT(wa.Attachment)>0, '".$Lang['ePost']['Yes']."', '".$Lang['ePost']['No']."') AS Attachment,
			IF(iu.ClassName IS NOT NULL AND iu.ClassName !='', CONCAT(iu.ClassName, IF(iu.ClassNumber IS NOT NULL AND iu.ClassNumber !='', CONCAT('-', iu.ClassNumber), '')), '--') AS Class,
			".getNameFieldByLang('iu.')." AS Student,
			DATE_FORMAT(enw.modified, '%Y-%m-%d') AS Submission,
			IFNULL(GROUP_CONCAT(DISTINCT CONCAT('<a href=\"javascript:openNewspaper(', en.NewspaperID, ', ', ep.PageID, ')\">', en.Title, ' - ', en.Name, '</a>') ORDER BY en.IssueDate ASC SEPARATOR '<br/>'), '&nbsp;') AS Issue,
			CONCAT('<input type=\"button\" value=\"".$Lang['ePost']['Button']['Screen']."\" class=\"',IF(enas.ArticleShelfID IS NOT NULL, 'formsubbutton', 'formbutton'),'\" name=\"Comment_Btn_', enw.RecordID, '\" onclick=\"comment_article(', enw.RecordID, ')\">'),
		".(($ePost->is_editor&&$ePost->user_obj->isTeacherStaff())?" IF(enas.ArticleShelfID IS NOT NULL,'',CONCAT('<div class=\"table_row_tool\"><a href=\"javascript:go_delete(', enw.RecordID, ')\" class=\"tool_delete\" title=\"".$Lang['ePost']['Delete']."\">&nbsp;</a></div>')) ControlButton,":"")."				
			enw.RecordID AS ID,
			IF(enas.ArticleShelfID IS NOT NULL, 1, 0) AS IsClassPast,
			enw.Title as Sort_Title
		FROM
			EPOST_WRITING AS enw INNER JOIN
			INTRANET_USER AS iu ON enw.UserID = iu.UserID LEFT JOIN
			EPOST_ARTICLE_SHELF AS enas ON enas.Status = ".$cfg_ePost['BigNewspaper']['ArticleShelf_status']['exist']." AND enw.RecordID = enas.WritingID LEFT JOIN
			EPOST_NEWSPAPER_PAGE_ARTICLE AS ea ON enas.ArticleShelfID = ea.ArticleShelfID AND ea.Status = ".$cfg_ePost['BigNewspaper']['Article_status']['exist']." LEFT JOIN
			EPOST_NEWSPAPER_PAGE AS ep ON ea.PageID = ep.PageID AND ep.Status = ".$cfg_ePost['BigNewspaper']['Page_status']['exist']." LEFT JOIN
			EPOST_NEWSPAPER AS en ON ep.NewspaperID = en.NewspaperID AND en.Status = ".$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']." LEFT JOIN
			EPOST_WRITING_ATTACHMENT AS wa ON enw.RecordID = wa.WritingID 
		WHERE
			enw.RequestID != 0 AND
			enw.RequestID = $RequestID AND
			enw.Status = ".$cfg_ePost['sql_config']['Status']['submitted']."
			".($Status==1? "AND enas.ArticleShelfID IS NOT NULL":"")."
			".($Status==2? "AND en.NewspaperID IS NOT NULL":"")."
		GROUP BY
			enw.RecordID";

$field  = $field==''? 1:$field;
$order  = $order==''? 1:$order;
$pageNo = $pageNo==''? 1:$pageNo;
$li = new libdbtable2007($field, $order, $pageNo, true);
$li->field_array = array("", "Sort_Title", "Attachment", "Class", "Student", "Submission", "Issue");
$li->sql = $sql;

$li->page_size = ($numPerPage=='')?10:$numPerPage;
$li->IsColOff = "ePost_article_shelf";
$li->no_col = ($ePost->is_editor&&$ePost->user_obj->isTeacherStaff())?sizeof($li->field_array) + 2:sizeof($li->field_array) + 1;
$li->count_mode = 1;
	
$pos = 1;
$li->column_list .= "<th class=\"num_check\">#</th>\n";
$li->column_list .= "<th width=\"30%\">".$li->column($pos++, $Lang['ePost']['Title'])."</th>\n";
$li->column_list .= "<th width=\"10%\">".$li->column($pos++, $Lang['ePost']['ImageVideo'])."</th>\n";
$li->column_list .= "<th width=\"10%\">".$li->column($pos++, $Lang['ePost']['Class'])."</th>\n";
$li->column_list .= "<th width=\"15%\">".$li->column($pos++, $Lang['ePost']['Student'])."</th>\n";
$li->column_list .= "<th width=\"10%\">".$li->column($pos++, $Lang['ePost']['SubmissionDate'])."</th>\n";
$li->column_list .= "<th width=\"15%\">".$li->column($pos++, $Lang['ePost']['PostedOnIssue'])."</th>\n";
if($ePost->is_editor&&$ePost->user_obj->isTeacherStaff()){
	$li->column_list .= "<th width=\"5%\">&nbsp;</th>\n";
	$li->column_list .= "<th width=\"1%\">&nbsp;</th>\n";
}else{
	$li->column_list .= "<th width=\"6%\">&nbsp;</th>\n";
}
$li->db_db_query("SET SESSION group_concat_max_len = 1048576"); // Set the max length of group concat to 1MB

# Initialize Navigation Array
$nav_ary[] = array("title"=>$Lang['ePost']['EditorPage'], "link"=>"../editor.php");
$nav_ary[] = array("title"=>$Lang['ePost']['RequestList'], "link"=>"index.php");
$nav_ary[] = array("title"=>$ePostRequest->Topic, "link"=>""); 

include($intranet_root.'/home/ePost/request_list/templates/tpl_request_entries_list.php');

intranet_closedb();
?>