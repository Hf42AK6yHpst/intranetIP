<?php
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>

<script>
	function go_filter(){
		document.filter_form.submit();
	}
	
	function go_edit(RequestID){
		var obj = document.form1;
		
		obj.action = 'request_new.php';
		obj.RequestID.value = RequestID;
		obj.submit();
	}
	
	function go_delete(RequestID){
		var obj = document.form1;
		var RequestName = document.getElementById('Request_title_' + RequestID).innerHTML;
		
		if(confirm('<?=$Lang['ePost']['Confirm']['RemoveRequest'][0]?>'+ RequestName + '<?=$Lang['ePost']['Confirm']['RemoveRequest'][1]?>')){
			obj.action = 'request_delete.php';
			obj.RequestID.value = RequestID;
			obj.submit();
		}
	}
	
	function openRequest(RequestID, Status)
	{
		var obj = document.form1;
		
		obj.RequestID.value = RequestID;
		obj.Status.value = Status;
		obj.action = 'request_entries_list.php';
		obj.submit();
	}
</script>
<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
	<p class="spacer">&nbsp; </p>
	<?=$ePostUI->gen_portal_page_nav($nav_ary)?>
    <p class="spacer">&nbsp; </p>
	<form name="filter_form" method="post" action="index.php" style="margin:0;padding:0">
	<div class="content_top_tool">
		<div id="table_filter">
			<select name="Status" id="Status">
				<option value="<?=$cfg_ePost['Noncourseware_Request']['Status']['All']?>" <?=$Status==$cfg_ePost['Noncourseware_Request']['Status']['All']? "selected":""?>><?=$Lang['ePost']['Filter']['All']?></option>
				<option value="<?=$cfg_ePost['Noncourseware_Request']['Status']['DeadlinePassed']?>" <?=$Status==$cfg_ePost['Noncourseware_Request']['Status']['DeadlinePassed']? "selected":""?>><?=$Lang['ePost']['Filter']['DeadlinePassed']?></option>
				<option value="<?=$cfg_ePost['Noncourseware_Request']['Status']['DeadlineNotPassed']?>" <?=$Status==$cfg_ePost['Noncourseware_Request']['Status']['DeadlineNotPassed']? "selected":""?>><?=$Lang['ePost']['Filter']['DeadlineNotPassed']?></option>
			</select>
			<input name="filter_go_btn" type="button" class="formsmallbutton" value="<?=$Lang['ePost']['Filter']['Go']?>" onclick="go_filter()"/>
		</div><p class="spacer"></p><br>
	</div>
	</form>
	<form name="form1" action="" method="post" style="margin:0;padding:0">
	<?php if(!$ePost->user_obj->isStudent()){ ?>
		<div class="Content_tool">
			<a href="request_new.php" class="new"><?=$Lang['ePost']['CreateNewRequest']?></a>
		</div>
	<?php } ?>
	
	<div id="sys_msg"><?=$Lang['ePost']['ReturnMessage'][$returnMsg]?></div>
        <div class="table_board">
          <table class="common_table_list edit_table_list">
							<col   nowrap="nowrap"/>
							<thead>
								<tr>
									<th width="30">#</th>
									<th><?=$Lang['ePost']['Topic']?></th>
									<th><?=$Lang['ePost']['Types']?></th>
									<th><?=$Lang['ePost']['LastModified']?></th>
									<th><?=$Lang['ePost']['Deadline']?></th>
									<th><?=$Lang['ePost']['NoOfEntriesReceived']?></th>
									<th><?=$Lang['ePost']['AddedToShelf']?></th>
									<th width="100">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
							<?=$table_html?>
							</tbody>
						</table>
          <p class="spacer"></p><p class="spacer"></p><br />
        </div>
		<em style="float:left;margin-left:5px;width:15px;height:15px;background-color:#f1f1f1;border:1px solid #ccc">&nbsp;</em>&nbsp;=&nbsp;<?=$Lang['ePost']['ActiveRequest']?>
      <input type="hidden" id="RequestID" name="RequestID" value=""/>
	<input type="hidden" id="Status" name="Status" value="<?=$Status?>"/>
    </div></div></div>
</form>

<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>