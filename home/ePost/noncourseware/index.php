<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_request.php");

intranet_auth();
intranet_opendb();

# Initialize Object
$ePost   	   = new libepost();
$ePostUI 	   = new libepost_ui();
$ePostRequests = new libepost_requests();
$Header='Write';
# Check if ePost should be accessible
$ePost->auth();

# Prepare Request List Record HTML
$user_assigned_requests = $ePostRequests->get_user_assigned_requests($cfg_ePost['Noncourseware_Request']['Status']['DeadlineNotPassed']);
$RequestList_HTML='';
if(count($user_assigned_requests)){
	for($i=0; $i<count($user_assigned_requests); $i++){
		$RequestObj = $user_assigned_requests[$i];
		$RequestList_HTML .= '
			<li>
				<div class="request_row_top">
					<h1>'.$RequestObj->Topic.'
						<span class="deadline_date">'.$Lang['ePost']['SubmitOnOrBefore'][0].' <strong>'.date('Y-m-d', strtotime($RequestObj->Deadline)).'</strong> '.$Lang['ePost']['SubmitOnOrBefore'][1].'</span>
					</h1>
				</div>
				<div class="request_row_bottom">
				'.nl2br($RequestObj->Description).'
				<div class="board_editor">
					<div class="btn_tool">
						<a href="writer.php?RequestID='.$RequestObj->RequestID.'" class="btn_write">'.$Lang['ePost']['StartToWrite'].'</a>
					</div>
				  </div>
				</div>
			</li>';
	}
} 

include($intranet_root.'/home/ePost/noncourseware/templates/tpl_index.php');

intranet_closedb();
?>