<?php

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
	<title>ePost</title>
	<link id="page_favicon" href="/images/<?=$favicon_image?>" rel="icon" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/<?=$LAYOUT_SKIN?>/css/ePost_new.css"/>
	<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/<?=$LAYOUT_SKIN?>/css/ePost_newspaper_new.css"/>
	
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/script.js"></script>
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/swf_object/swfobject.js"></script>
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/jquery-1.8.0.min.js"></script>
</head>

<body>
	<div class="bg_left"><div class="bg_right"><div class="bg">
       	<div id="container">
        	<div class="top_header">
            	<a href="<?=$intranet_httppath?>/home/ePost/index.php" class="title_logo_en" title="ePost"></a> <span class="title_usertype"><?=$Lang['ePost']['Postit']?></span>
                <span class="user_name"><?=$ePostUI->user_obj->UserName(1)?></span>
            </div>
        	<div class="content_board" id="post_board">
            	<div class="content_board_top"><div class="content_board_top_right"><div class="content_board_top_bg">
                	<div class="board_tab">
                    	<ul>
                        	<li <?=(($Header=='Write')?'class="current_tab"':'')?>><a href="<?=$intranet_httppath?>/home/ePost/noncourseware/index.php"><span><?=$Lang['ePost']['WriteNow']?></span></a></li>
                            <li <?=(($Header=='Portfolio')?'class="current_tab"':'')?>><a href="<?=$intranet_httppath?>/home/ePost/noncourseware/portfolio.php"><span><?=$Lang['ePost']['MySubmittedRecord']?></span></a></li>
                        </ul>
                    </div>
                </div></div></div>