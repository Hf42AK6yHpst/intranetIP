<?php

?>

<script>
	var IsSkipAttachmentChecking = <?=$ePostWriter->Attachment? 'true':'false'?>;

	function delete_attachment(id){
		if(confirm('<?=$Lang['ePost']['Confirm']['RemoveAttachedVideoFile']?>')){
			IsSkipAttachmentChecking = false;
			$('#del_attach'+id).val(1);
			//document.form1.del_attach.value = 1;
			$('#attachment'+id).css('display', 'none');
			$('#fileupload'+id).css('display', '');
		}
	}
	
	function writer_edit_start(){
		$('#write_title_temp').css('display', 'none');
		$('#write_content_temp').css('display', 'none');
		$('#write_title').css('display', '');
		$('#write_content').css('display', '');
		$('#write_upload').css('visibility', 'visible');
	}
	function check_upload_file_valid(){
		var fileupload  = '';
	<?php for($i=1;$i<=$maxUploadCnt;$i++){?>
		fileupload  = $('#fileupload<?=$i?>').val();
		if(fileupload){
			filename = Get_File_Name(fileupload);
			var ext = fileupload.substr((fileupload.length-4)).toUpperCase();
			
			if(ext!='.FLV'&&ext!='.MOV'&&ext!='.MP4'){
				alert('<?=$Lang['ePost']['Warning']['UploadFLVFiles']?>');
				$('#fileupload<?=$i?>').val('');
				return false;
			}
		}
	<?php }?>
		return true;
	}	

</script>			 
<span id="write_title" class="write_title">
	<input type="text" id="title" name="title" value="<?=$ePostWriter->Title?>"/>
</span>
<span id="write_content" class="write_content">
	<textarea id="content" name="content"><?=$ePostWriter->Content?></textarea>
</span>

<?$i=0;?>
<?if(count($ePostWriter->Attachment)>0){?>
	<?foreach($ePostWriter->Attachment as $_attachmentId => $_attachmentPathAry){?>
		<?$i++;?>
		<span class="write_upload"><?=$Lang['ePost']['AttachFile']?>: 
		<?=$_attachmentPathAry['attachment']? "<span id='attachment".$i."'>".$_attachmentPathAry['attachment']."<a href='javascript:delete_attachment(".$i.")'><img src='$image_path/2009a/icon_cross.gif' style='border:0'></a></span>" : "";?>
		<input type="file" id="fileupload<?=$i?>" name="fileupload<?=$i?>" <?=$_attachmentPathAry['attachment']? "style='display:none'" : ""?>/>
		</span>
		<span id="span_caption<?=$i?>" class="write_upload"><?=$Lang['ePost']['OptionalVideoCaption']?> : 
		    <input type="text" id="caption<?=$i?>" name="caption<?=$i?>" value="<?=$_attachmentPathAry['caption']?>"/>
		</span>
		<input type="hidden" id="del_attach<?=$i?>" name="del_attach<?=$i?>" value="0"/>
	<?}?>
<?}?>
<?
	for($j=$i+1;$j<=$maxUploadCnt;$j++){
?>
	<span class="write_upload"><?=$Lang['ePost']['OptionalAttachVideoFiles']?>: 
	<input type="file" id="fileupload<?=$j?>" name="fileupload<?=$j?>"/>
	</span>
	<span id="span_caption<?=$j?>" class="write_upload" style="display:none;"><?=$Lang['ePost']['OptionalVideoCaption']?> : 
	    <input type="text" id="caption<?=$j?>" name="caption<?=$j?>" value=""/>
	</span>
<?}?>