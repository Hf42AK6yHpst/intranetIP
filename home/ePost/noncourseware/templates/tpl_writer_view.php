<?php
include($intranet_root.'/home/ePost/noncourseware/templates/tpl_head.php');


?>
<style>
table	{width:auto;}
</style>
<script>
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
	
	function toogle_teacher_comment(ShowHide){
		if(ShowHide == 'show'){
			$('#teacher_comment_board').attr('style', 'visibility:visible');
		}
		else{
			$('#teacher_comment_board').attr('style', 'visibility:hidden');
		}
	}
	
	function go_back(){
		var obj = document.form1;
		obj.action = 'portfolio.php';
		obj.submit();
	}
	
	function go_edit(){
		var obj = document.form1;
		obj.action = 'writer.php';
		obj.submit();
	}
	function openNewspaper(NewspaperID, PageID){
		var intWidth  = screen.width;
	 	var intHeight = screen.height-100;
		window.open('/home/ePost/newspaper/view_newspaper.php?NewspaperID='+NewspaperID+'&PageID='+PageID,"ePost_Newspaper","width="+intWidth+",height="+intHeight+",scrollbars,resizable");
	}

	$(document).ready(function(){
		$('.video_attachment').each(function(){
			var that = $(this);
			var path = that.attr("path");
			
			$.post("/home/ePost/ajax.php", { task:'gen_video_player',path:path},  
				function(data, textStatus){
					that.html(data);
					$('table.log_photo').show();
				}		
			);	
		});
	});	
</script>
<form id="form1" name="form1" method="POST" enctype="multipart/form-data" style="padding:0;margin:0">
<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
    <div class="write_content_guide">
		<?=$issue_list[0]?>
		<div id="guide_board">
			<div class="guide_content_board">
				<div class="request_row_top">
					<h1><?=$ePostRequest->Topic?>
						<span class="deadline_date"><?=$Lang['ePost']['SubmitOnOrBefore'][0].' <strong>'.date('Y-m-d', strtotime($ePostRequest->Deadline)).'</strong> '.$Lang['ePost']['SubmitOnOrBefore'][1]?></span>
					</h1>
				</div>
				<div class="request_row_bottom">
					<?=$ePostRequest->Description?>					
					<input name="submit2" type="button" class="formsmallbutton"  onclick="MM_showHideLayers('guide_board','','hide');MM_showHideLayers('btn_guide_layer','','show')" value="Close" />
				</div>
			</div>
		</div>
		<a href="#" title="guide" class="btn_guide" id="btn_guide_layer" onclick="MM_showHideLayers('guide_board','','show');MM_showHideLayers('btn_guide_layer','','hide')">Guide</a>    
	</div>
	<div class="write_content_board">
		<?php include($writer_template); ?>
		<div class="edit_bottom"> 
			<p class="spacer"></p>
		<?php if($ePostWriter->Status==$cfg_ePost['sql_config']['Status']['drafted']){ ?>
				<input type="button" value="<?=$Lang['ePost']['Button']['Edit']?> &gt;" onclick="go_edit()" class="formbutton" name="edit_btn">
		<?php } ?>
			<input type="button" value="&lt; <?=$Lang['ePost']['Button']['Back']?>" onclick="go_back()" class="formbutton" name="back_btn">
			<p class="spacer"></p>
		</div>
	</div>
	<p class="spacer"></p> &nbsp;
</div></div></div>	
	<input type="hidden" id="RecordID" name="RecordID" value="<?=$RecordID?>"/>
	<input type="hidden" id="RequestID" name="RequestID" value="<?=$RequestID?>"/>
</form>

<?php
include($intranet_root.'/home/ePost/noncourseware/templates/tpl_foot.php');
?>