<? if ($offset==0) :?>

<div class="table_board">	   
    <table class="common_table_list">
	<col   nowrap="nowrap"/>
	  <thead>
	      <tr>
		  <th class="num_check">#</th>
		  <th><a
		  <? if ($sortby=='title') :?>
		  href='#' onclick="sortPost('title', '<?=($order=='asc')? 'dec': 'asc'?>'); return false;" class="sort_<?=$order?>"
		  <? else: ?> href='#' onclick="sortPost('title', 'asc'); return false;"
		  <? endif; ?>><?=$Lang['ePost']['IssueTitle']?></a></th>
		  <th><a 
		  
		  <? if ($sortby=='name') :?>
		  href='#' onclick="sortPost('name', '<?=($order=='asc')? 'dec': 'asc'?>'); return false;" class="sort_<?=$order?>"
		  <? else: ?>
		  href='#' onclick="sortPost('name', 'asc'); return false;"
		  <? endif; ?>
		  ><?=$Lang['ePost']['IssueName']?></a></th>
		  <th><a 
		  
		  <? if ($sortby=='date') :?>
		  href='#' onclick="sortPost('date', '<?=($order=='asc')? 'dec': 'asc'?>'); return false;" class="sort_<?=$order?>"
		  <? else: ?>
		  href='#' onclick="sortPost('date', 'asc'); return false;"
		  <? endif; ?>
		  ><?=$Lang['ePost']['IssueDate']?></a></th>
	      </tr>
	  </thead>

	  <tbody>
<? endif; ?>
<?php if(count($NewsArr)>0){ ?>
	    <?for($i=0;$i<count($NewsArr);$i++){?>
	      <tr>
		    <td><?=$i+($offset*$amount)+1?></td>
		    <td><a href="<?=$NewsArr[$i]['link']?>"><?=$NewsArr[$i]['title']?></a></td>
		    <td><?=$NewsArr[$i]['name']?></td>
			<td><?=$NewsArr[$i]['date']?></td>
	      </tr>
	    <?}?>
<?php }else{ ?>
	      <tr>
			<td></td>
			<td colspan="3" align="center"><?=$Lang['ePost']['NoIssuesFound']?></td>
	      </tr>
<?php } ?>

<? if ($offset==0) :?>
	  <tbody>			
      </table>
  </div>
  <input type="hidden" name="display_list_issue_total" id="display_list_issue_total" value="<?=$result_total?>">
<p class="spacer"></p>
<? endif; ?>




