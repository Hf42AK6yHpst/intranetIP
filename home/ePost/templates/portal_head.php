<?php
/*
 * Editing by 
 * 
 * Modification Log: 
 * 2012-10-30 (Jason)
 * 		- remove the meta script to define IE Engine version 
 */
# Initialize Variable
$Header = (isset($Header))?$Header:'NEW';
$FromPage = (isset($FromPage))?$FromPage:'portal';
$SubHeader = (isset($FromPage)&&$FromPage=='editor')?'<span class="title_usertype"><a style="position:relative;" href="'.$intranet_httppath.'/home/ePost/editor.php">'.$Lang['ePost']['Editors'].'</a></span>':'';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>eClass ePost</title>
	<link id="page_favicon" href="/images/<?=$favicon_image?>" rel="icon" type="image/x-icon" />
	
	<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/jquery/jquery.datepick.css">
	<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/jquery/thickbox.css">
	<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/<?=$LAYOUT_SKIN?>/css/ePost_new.css"/>
	<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/<?=$LAYOUT_SKIN?>/css/ePost_newspaper_new.css"/>
	<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/<?=$LAYOUT_SKIN?>/css/ePost_print.css" media="print"/>
	<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/jquery/jquery.fancybox.css" media="screen" />	
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/script.js"></script>
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/swf_object/swfobject.js"></script>
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/jquery.blockUI.js"></script>
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/thickbox.js"></script>
	<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/jquery.fancybox.pack.js"></script>

</head>

<?php if($Header=='OLD'){ ?>
<body>
	<div class="main_board">
    	<div class="main_board_top_left">
    		<div class="main_board_top_right">
        		<div class="home_icon">
        			<a href="<?=$intranet_httppath?>/home/ePost/index.php">&nbsp;</a>
        		</div>
        		<div class="school_name_tab">
        			<div class="top_banner"><?=$Lang['ePost']['ePost']?></div>
        			<?=$ePostUI->gen_portal_page_tab()?>
				</div>
        	</div>
        </div>
		<div class="main_board_left">
			<div class="main_board_right">
<?php }elseif($Header=='NEW'){ 
?>
<body>
	<div class="bg_left"><div class="bg_right"><div class="bg">
       	<div id="container">
        	<div class="top_header">
            	<a href="<?=$intranet_httppath?>/home/ePost/index.php" class="title_logo_en" title="ePost"></a><?=$SubHeader?> 
                <span class="user_name"><?=$ePostUI->user_obj->UserName(1)?></span>
            </div>
        	<div class="content_board" id="<?=$FromPage?>_board">
            	<div class="content_board_top"><div class="content_board_top_right"><div class="content_board_top_bg">
				<?=(($FromPage=='editor'?$ePostUI->gen_editor_page_tab():''))?> 
                </div></div></div>
<?php }elseif($Header=='PUBLIC'){ 
?>
<body>
	<div class="bg_left"><div class="bg_right"><div class="bg">
       	<div id="container">
        	<div class="top_header">
            	<a href="javascript:void(0)" class="title_logo_en" title="ePost"></a>
            </div>
        	<div class="content_board" id="<?=$FromPage?>_board">
            	<div class="content_board_top"><div class="content_board_top_right"><div class="content_board_top_bg">
                </div></div></div>
<?php } ?>