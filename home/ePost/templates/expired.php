<?php
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>
<?php if(!$ePost->check_is_expired() && false){ ?>
	<script>
		window.close();
	</script>
<?php } else { ?>
	<div style="width:100%;text-align:center;padding-top:85px">
		<?=isset($plugin['ePost'])? $Lang['ePost']['ExpiredMessage'] : $Lang['ePost']['NoSubscriptionMessage']?>
	</div>
<?php } ?>
<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>