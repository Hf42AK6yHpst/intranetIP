<?php
# using :

$PATH_WRT_ROOT = "../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");

intranet_opendb();

$ePost   	   = new libepost();
$ePostUI 	   = new libepost_ui();
$ePostFolders  = new libepost_folders();
$Header='PUBLIC';
$FromPage = 'portal';
# Check if ePost should be accessible
$ePost->auth();
$ePostUI->post_ids = $ePostUI->get_post_ids($ePostFolders,true);
$ePostUI->attachment_format = ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")?'mp4':'flv';
include($intranet_root.'/home/ePost/templates/tpl_newspaper_list.php');

intranet_closedb();
?>