<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_publishmanager.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

# Initialize Object
$lclass  = new libclass();
$ePostUI = new libepost_ui();
$ePostPublishManagers = new libepost_publishmanagers();

# Check if ePost should be accessible
$ePostUI->portal_auth();
$Header='NEW';
$FromPage = 'editor';
$IgnoreUserID = array();
for($i=0;$i<count($ePostPublishManagers->PublishManagers);$i++){
	$IgnoreUserID[] = $ePostPublishManagers->PublishManagers[$i]->UserID;
}

$sql = "SELECT
			iu.UserID,
			".getNameFieldWithClassNumberByLang ("iu.")." AS TeacherName
		FROM
			INTRANET_USER iu
		WHERE
			iu.RecordType = 1 AND
			iu.RecordStatus = 1 AND
			".getNameFieldWithClassNumberByLang ("iu.")." != ''
			".(is_array($IgnoreUserID) && count($IgnoreUserID)? "AND iu.UserID NOT IN (".implode(',', $IgnoreUserID).")":"")."
		ORDER BY
			iu.EnglishName";
$teacher_ary = $ePostPublishManagers->returnArray($sql);

# Teacher Selection Filter
$StudentSelectionFilter  = "<select multiple=\"true\" size=\"13\" id=\"ManagerUserIDAry\" name=\"ManagerUserIDAry[]\" style=\"width: 441px;\">";
for($i=0;$i<count($teacher_ary);$i++){
	$StudentSelectionFilter .= "<option value='".$teacher_ary[$i]['UserID']."'>".$teacher_ary[$i]['TeacherName']."</option>";
}
$StudentSelectionFilter .= "</select>";
$nav_ary[] = array("title"=>$Lang['ePost']['EditorPage'], "link"=>"../editor.php");
$nav_ary[] = array("title"=>$Lang['ePost']['TeacherEditorList'], "link"=>"index.php"); 
$nav_ary[] = array("title"=>$Lang['ePost']['AddTeacherEditor'], "link"=>""); 

include($intranet_root.'/home/ePost/publish_manager/templates/tpl_add_manager.php');

intranet_closedb();
?>