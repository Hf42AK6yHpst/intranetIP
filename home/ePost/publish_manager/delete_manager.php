<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_publishmanager.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$ManagerID = isset($ManagerID) && $ManagerID!=''? $ManagerID : 0;

# Initialize Object
$ePostPublishManager = new libepost_publishmanager($ManagerID);

# Check if ePost should be accessible
$ePostPublishManager->portal_auth();

# Delete Student Editor
$result = $ePostPublishManager->delete_publishmanager();
$returnMsg = $result?"DeleteSuccess":"DeleteUnsuccess";
header('location:index.php?returnMsg='.$returnMsg);

intranet_closedb();
?>