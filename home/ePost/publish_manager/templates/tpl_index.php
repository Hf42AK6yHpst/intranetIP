<?php
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>

<script>
	function go_delete(ManagerID, HideClassName){
		var obj = document.form1;
		var PublishManagerClass = document.getElementById('PublishManager_class_' + ManagerID).innerHTML;
		var PublishManagerName  = document.getElementById('PublishManager_name_' + ManagerID).innerHTML;
		
		var confirmStr = '<?=$Lang['ePost']['Confirm']['RemovePublishingManager'][0]?>';
		if(!HideClassName){
			confirmStr += PublishManagerClass + '<?=$Lang['ePost']['Confirm']['RemovePublishingManager'][1]?>';
		}
		confirmStr += PublishManagerName + '<?=$Lang['ePost']['Confirm']['RemovePublishingManager'][2]?>';
		
		if(confirm(confirmStr)){
			obj.action = 'delete_manager.php';
			obj.ManagerID.value = ManagerID;
			obj.submit();
		}
	}
</script>
<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
<p class="spacer">&nbsp;</p>
<?=$ePostUI->gen_admin_publish_manager_page_nav(0)?>              
<p class="spacer">&nbsp;</p>
<?=$ePostUI->gen_portal_page_nav($nav_ary)?>
<p class="spacer">&nbsp;</p>
<div class="content_top_tool" style="min-height:0px"></div>
<form name="form1" action="" method="post" style="margin:0;padding:0">
	<div class="Content_tool">
		<a href="add_manager.php" class="new"><?=$Lang['ePost']['AddTeacherEditor']?></a>
	</div>
	<div id="sys_msg"><?=$Lang['ePost']['ReturnMessage'][$returnMsg]?></div>
	<div class="table_board">
		<table class="common_table_list edit_table_list">
			<colgroup>
				<col nowrap="nowrap">
			</colgroup>
			<tr>
				<th width="30">#</th>
				<th><?=$Lang['ePost']['Class']?></th>
				<th><?=$Lang['ePost']['Teacher']?></th>
				<th width="100">&nbsp;</th>
			</tr>
			<?=$table_html?>
		</table>
		<p class="spacer"></p>
		<p class="spacer"></p>
		<br>
	</div>
	<input type="hidden" id="ManagerID" name="ManagerID" value=""/>
</form>
</div></div></div>
<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>