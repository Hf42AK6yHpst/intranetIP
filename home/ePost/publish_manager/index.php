<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_publishmanager.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$Status = isset($Status) && $Status!=''? $Status : 0;
$returnMsg = isset($returnMsg) && $returnMsg!=''? $returnMsg : "";

# Initialize Object
$ePostUI = new libepost_ui();
$ePostPublishManagers = new libepost_publishmanagers();
$Header='NEW';
$FromPage = 'editor';
# Check if ePost should be accessible
$ePostUI->portal_auth();
if(!$ePostUI->is_super_admin){
	header("Location:../student_editor/index.php");
	exit;
}
# Process Table Content HTML
for($i=0;$i<count($ePostPublishManagers->PublishManagers);$i++){
	$PublishManagerObj = $ePostPublishManagers->PublishManagers[$i];
	
	$table_html .= "<tr>";
	$table_html .= "	<td>".($i+1)."</td>";
	$table_html .= "	<td><span id='PublishManager_class_".$PublishManagerObj->ManagerID."'>".($PublishManagerObj->ClassName? $PublishManagerObj->ClassName.($PublishManagerObj->ClassNumber? ' - '.$PublishManagerObj->ClassNumber:'') : $Lang['ePost']['NA'])."</span></td>";
	$table_html .= "	<td><span id='PublishManager_name_".$PublishManagerObj->ManagerID."'>".$PublishManagerObj->EnglishName."</span></td>";
	$table_html .= "	<td>";
	$table_html .= "		<div class=\"table_row_tool\">";
	$table_html .= "			<a href=\"javascript:go_delete(".$PublishManagerObj->ManagerID.", ".($PublishManagerObj->ClassName? '0':'1').")\" class=\"tool_delete\" title=\"".$Lang['ePost']['Delete']."\">&nbsp;</a>";
	$table_html .= "		</div>";
	$table_html .= "	</td>";
	$table_html .= "</tr>";
}

if(!$table_html){
	$table_html  = "<tr>";
	$table_html .= "	<td height='80' class='tabletext' colspan='4' style='text-align:center;vertical-align:middle'>$i_no_record_exists_msg</td>";
	$table_html .= "</tr>";
}
$nav_ary[] = array("title"=>$Lang['ePost']['EditorPage'], "link"=>"../editor.php");
$nav_ary[] = array("title"=>$Lang['ePost']['TeacherEditorList'], "link"=>""); 

include($intranet_root.'/home/ePost/publish_manager/templates/tpl_index.php');

intranet_closedb();
?>