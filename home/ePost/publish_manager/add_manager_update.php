<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_publishmanager.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$ManagerUserIDAry = isset($ManagerUserIDAry) && $ManagerUserIDAry!=''? $ManagerUserIDAry : array();

# Initialize Object
$ePostPublishManagers = new libepost_publishmanagers();

# Check if ePost should be accessible
$ePostPublishManagers->portal_auth();
if(!$ePostPublishManagers->is_super_admin){
	header("Location:../student_editor/index.php");
	exit;
}
# Found the users that are already editors
$IgnoreUserID = array();
for($i=0;$i<count($ePostPublishManagers->PublishManagers);$i++){
	$IgnoreUserID[] = $ePostPublishManagers->PublishManagers[$i]->UserID;
}

# Assign the users as student editors
for($i=0;$i<count($ManagerUserIDAry);$i++){
	$ManagerUserID = $ManagerUserIDAry[$i];
	
	if(count($IgnoreUserID)==0 || !in_array($ManagerUserID, $IgnoreUserID)){
		$PublishManager_Data_ary['UserID'] = $ManagerUserID;
		$PublishManager_Data_ary['Status'] = $cfg_ePost['BigNewspaper']['StudentEditor_status']['exist'];
		
		$PublishManagerObj = new libepost_publishmanager(0, $PublishManager_Data_ary);
		
		$PublishManagerObj->update_publishmanager_db();
	}
} 

intranet_closedb();
header('Location:index.php');
?>
