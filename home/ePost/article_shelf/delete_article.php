<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$ArticleShelfID = isset($ArticleShelfID) && $ArticleShelfID!=''? $ArticleShelfID : 0;

# Initialize Object
$ePostArticleShelf = new libepost_articleshelf($ArticleShelfID);

# Check if ePost should be accessible
$ePostArticleShelf->portal_auth();
$result = 0;
if($ArticleShelfID){
	$sql = "SELECT COUNT(*) FROM EPOST_ARTICLE_SHELF AS ea 
			INNER JOIN EPOST_NEWSPAPER_PAGE_ARTICLE AS enpa ON ea.ArticleShelfID = enpa.ArticleShelfID AND enpa.Status = ".$cfg_ePost['BigNewspaper']['Article_status']['exist']." 
			INNER JOIN EPOST_NEWSPAPER_PAGE AS enp ON enpa.PageID = enp.PageID AND enp.Status = ".$cfg_ePost['BigNewspaper']['Page_status']['exist']."
			INNER JOIN EPOST_NEWSPAPER AS en ON enp.NewspaperID = en.NewspaperID AND en.Status = ".$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']." 
			WHERE ea.ArticleShelfID = '".$ArticleShelfID."'";
	$cnt = current($ePostArticleShelf->returnVector($sql));
	if(!$cnt) $result = $ePostArticleShelf->delete_article_shelf();
}
$returnMsg = $result?"DeleteSuccess":"DeleteUnsuccess";
intranet_closedb();
header('Location:index.php?courseware='.$courseware."&article_requestID=".$article_requestID."&returnMsg=".$returnMsg);
?>
