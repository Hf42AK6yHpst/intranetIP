<?php 
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>

	<script>
		function go_submit(){
			<?php if($ArticleShelfID){ ?>
				var obj = document.form1;
				
				if(!writer_checkform(obj)) return false;
				
				obj.action = "article_edit_update.php";
				obj.submit();
			<?php } else {?>
				alert('<?=$Lang['ePost']['Warning']['SelectArticle']?>');
			<?php } ?>
		}		
		
		function go_cancel(){
			var obj = document.form1;
			obj.action = "index.php";
			obj.submit();
		}
	</script>
<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
	<form name="form1" method="post" enctype="multipart/form-data" >
		<p class="spacer">&nbsp; </p>
        <p class="spacer">&nbsp; </p>
		<?=$ePostUI->gen_portal_page_nav($nav_ary)?>
        <p class="spacer">&nbsp; </p><br />
       <!---->
       
        <div class="table_board">
        <span class="sectiontitle">&nbsp;&nbsp;&nbsp; <?=$Lang['ePost']['Article']?> - <?=$ePostArticleShelf->Title?></span>
       
        <p class="spacer"></p>
          <table class="form_table">
             <tr>
               <td><?=$Lang['ePost']['SourceType']?></td>
               <td>:</td>
               <td>
			    <?=$Lang['ePost']['From']?>
				<?=$cfg_ePost['BigNewspaper']['ArticleType'][($courseware==1? "courseware":"non_courseware")]['name']?>
				<?=$article_requestID? " > ".$article_request_topic:""?>
				<?=$article_type? " > ".$cfg_ePost['BigNewspaper']['ArticleType'][($courseware==1? "courseware":"non_courseware")]['types'][$article_type]['name']:""?>
				<?=$article_type && $article_sub_type? " > ".$cfg_ePost['BigNewspaper']['ArticleType'][($courseware==1? "courseware":"non_courseware")]['types'][$article_type]['sub_types'][$article_sub_type]['name']:""?>
			   </td>
             </tr>
             <tr>
               <td><?=$Lang['ePost']['Author']?></td>
               <td>:</td>
               <td><?=$ePostWriter->UserName.($ePostWriter->ClassName? " (".$ePostWriter->ClassName.($ePostWriter->ClassNumber? "-".$ePostWriter->ClassNumber:"").")":"")?></td>
             </tr>
            <?php include($writer_template); ?>
           </table>
        <p class="spacer"></p>
        <div class="edit_bottom">
          <p class="spacer"></p><br />
  			<input name="submit2" class="formbutton" onclick="go_submit()" value="<?=$Lang['ePost']['Button']['Submit_1']?>" type="button">
	        <input name="submit2" class="formsubbutton" value="<?=$Lang['ePost']['Button']['Cancel']?>" type="button" onclick="go_cancel()"/>
          </div>
        </div>	  	
		
	  	<input type="hidden" id="ArticleShelfID" name="ArticleShelfID" value="<?=$ArticleShelfID?>"/>
	</form>
</div></div></div>


<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>