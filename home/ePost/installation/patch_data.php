<?php

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");

intranet_opendb();

$debug_mode = false;
$mapAttachmentSizeAry = array();
//$mapAttachmentSizeAry[Type][Layout][Position][Size]
$mapAttachmentSizeAry['C1'][1][1] = array('Large'=>'50','Medium'=>'50','Small'=>'30');
$mapAttachmentSizeAry['C2'][1][1] = array('Large'=>'50','Medium'=>'50','Small'=>'30');
$mapAttachmentSizeAry['C2'][1][2] = array('Large'=>'50','Medium'=>'50','Small'=>'30');

$mapAttachmentSizeAry['C3'][1][1] = array('Large'=>'50','Medium'=>'50','Small'=>'30');
$mapAttachmentSizeAry['C3'][1][2] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['C3'][1][3] = array('Large'=>'70','Small'=>'50');

$mapAttachmentSizeAry['C3'][2][1] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['C3'][2][2] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['C3'][2][3] = array('Large'=>'70','Small'=>'50');

$mapAttachmentSizeAry['I1'][1][1] = array('Large'=>'50','Medium'=>'50','Small'=>'30');
$mapAttachmentSizeAry['I2'][1][1] = array('Large'=>'50','Medium'=>'50','Small'=>'30');
$mapAttachmentSizeAry['I2'][1][2] = array('Large'=>'50','Medium'=>'50','Small'=>'30');

$mapAttachmentSizeAry['I3'][1][1] = array('Large'=>'50','Medium'=>'50','Small'=>'30');
$mapAttachmentSizeAry['I3'][1][2] = array('Large'=>'50','Medium'=>'50','Small'=>'30');
$mapAttachmentSizeAry['I3'][1][3] = array('Large'=>'50','Medium'=>'50','Small'=>'30');

$mapAttachmentSizeAry['I3'][2][1] = array('Large'=>'50','Medium'=>'50','Small'=>'30');
$mapAttachmentSizeAry['I3'][2][2] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I3'][2][3] = array('Large'=>'70','Small'=>'50');

$mapAttachmentSizeAry['I3'][3][1] = array('Large'=>'50','Medium'=>'50','Small'=>'30');
$mapAttachmentSizeAry['I3'][3][2] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I3'][3][3] = array('Large'=>'70','Small'=>'50');

$mapAttachmentSizeAry['I3'][4][1] = array('Large'=>'50','Medium'=>'50','Small'=>'30');
$mapAttachmentSizeAry['I3'][4][2] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I3'][4][3] = array('Large'=>'70','Small'=>'50');

$mapAttachmentSizeAry['I4'][1][1] = array('Large'=>'50','Medium'=>'50','Small'=>'30');
$mapAttachmentSizeAry['I4'][1][2] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I4'][1][3] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I4'][1][4] = array('Large'=>'70','Small'=>'50');

$mapAttachmentSizeAry['I4'][2][1] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I4'][2][2] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I4'][2][3] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I4'][2][4] = array('Large'=>'70','Small'=>'50');

$mapAttachmentSizeAry['I4'][3][1] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I4'][3][2] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I4'][3][3] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I4'][3][4] = array('Large'=>'50','Medium'=>'50','Small'=>'30');

$mapAttachmentSizeAry['I5'][1][1] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I5'][1][2] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I5'][1][3] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I5'][1][4] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I5'][1][5] = array('Large'=>'70','Small'=>'50');

$mapAttachmentSizeAry['I5'][2][1] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I5'][2][2] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I5'][2][3] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I5'][2][4] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I5'][2][5] = array('Large'=>'70','Small'=>'50');

$mapAttachmentSizeAry['I6'][1][1] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I6'][1][2] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I6'][1][3] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I6'][1][4] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I6'][1][5] = array('Large'=>'70','Small'=>'50');
$mapAttachmentSizeAry['I6'][1][6] = array('Large'=>'70','Small'=>'50');

if($plugin['ePost']){
	$li = new libdb();
	$li->db = $intranet_db; 
	$ePost = new libepost();
	
	/* Patch Attachment */
	### DELETE current record in EPOST_WRITING_ATTACHMENT
	$sql = "SELECT COUNT(*) FROM EPOST_WRITING_ATTACHMENT";
	$hasPatched = current($li->returnVector($sql));
	if(!$hasPatched){
		### INSERT Writing Record into EPOST_WRITING_ATTACHMENT
		$sql = "SELECT 
					w.RecordID WritingID,s.ArticleShelfID, w.Attachment, w.Caption, s.Caption as ArticleShelfCaption, w.UserID as inputBy, w.inputdate   
				FROM 
					EPOST_WRITING w   
				LEFT JOIN 
					EPOST_ARTICLE_SHELF s ON w.RecordID = s.WritingID
				WHERE 
					w.Attachment != '' AND w.Attachment IS NOT NULL";
		$writingAry = $li->returnArray($sql);
		$writingRecordCnt = count($writingAry);
		$insertSQL = "INSERT INTO EPOST_WRITING_ATTACHMENT (WritingID,ArticleShelfID,Attachment,Caption,ArticleShelfCaption,InputDate,InputBy,ModifiedBy,ModifiedDate) VALUES ";
		for($i=0;$i<$writingRecordCnt;$i++){
			$_writingId = $writingAry[$i]['WritingID']?$writingAry[$i]['WritingID']:0;
			$_articleShelfId = $writingAry[$i]['ArticleShelfID']?$writingAry[$i]['ArticleShelfID']:0;
			$_articleShelfCaption = $writingAry[$i]['ArticleShelfCaption'];
			$_inputBy = $writingAry[$i]['inputBy'];
			$_attachment = $writingAry[$i]['Attachment'];
			$_caption = $writingAry[$i]['Caption'];
			$_inputdate = $writingAry[$i]['inputdate'];
			$insertSQL .= $i>0?",":"";
			$insertSQL .= "('".$_writingId."','".$_articleShelfId."','".$_attachment."','".addslashes($_caption)."','".$_articleShelfCaption."','".$_inputdate."','".$_inputBy."','".$UserID."',NOW())";	
			
		}
		$li->db_db_query($insertSQL);
		
		
		### INSERT Article Shelf Record into EPOST_WRITING_ATTACHMENT
		$sql = "SELECT 
					pa.ArticleID, 
					a.AttachmentID,
					CASE WHEN pa.Alignment = 0 THEN 1 ELSE pa.Alignment END Alignment,    
					CASE WHEN pa.Alignment = 0 THEN 0 ELSE 1 END ShowStatus,
					pa.InputDate,
					pa.Position, 
					pa.Size, 
					p.Type, 
					p.Layout  
				FROM 
					EPOST_NEWSPAPER_PAGE_ARTICLE pa    
				INNER JOIN EPOST_NEWSPAPER_PAGE p ON pa.PageID = p.PageID 
				INNER JOIN EPOST_WRITING_ATTACHMENT a ON pa.ArticleShelfID = a.ArticleShelfID
			";
		$articleAry = $li->returnArray($sql);
		$articleRecordCnt = count($articleAry);
		### INSERT Writing Record into EPOST_WRITING_ATTACHMENT
		$insertSQL = "INSERT INTO EPOST_NEWSPAPER_PAGE_ARTICLE_ATTACHMENT (ArticleID,AttachmentID,Alignment,Size,Status,InputDate,InputBy,ModifiedBy,ModifiedDate) VALUES ";
		for($i=0;$i<$articleRecordCnt;$i++){
			$_articleId = $articleAry[$i]['ArticleID'];
			$_attachmentId = $articleAry[$i]['AttachmentID'];
			$_alignment = $articleAry[$i]['Alignment'];
			$_status = $articleAry[$i]['ShowStatus'];
			$_inputdate = $articleAry[$i]['InputDate'];
			$_position = $articleAry[$i]['Position'];
			$_type = $articleAry[$i]['Type'];
			$_size = $articleAry[$i]['Size'];
			$_layout = $articleAry[$i]['Layout'];		
			$_newSize = $mapAttachmentSizeAry[$_type][$_layout][$_position][$_size];
			$insertSQL .= $i>0?",":"";
			$insertSQL .= "('".$_articleId."','".$_attachmentId."','".$_alignment."','".$_newSize."','".$_status."','".$_inputdate."','".$UserID."','".$UserID."',NOW())";	
			
		}
		$li->db_db_query($insertSQL);	
		echo "Patch Attachment Done <br/>";
	}
	
	$sql = "SHOW COLUMNS FROM EPOST_WRITING_COMMENT LIKE 'ArticleShelfID'";
	$result = $li->db_db_query($sql);	
	$exists = (mysql_num_rows($result))?TRUE:FALSE;
	if(!$exists){
		/* Patch Comment */
		$sql = "ALTER TABLE EPOST_WRITING_COMMENT ADD COLUMN `ArticleShelfID` int(11) default '0' AFTER WritingID";
		$li->db_db_query($sql);	
		
		$sql = "UPDATE EPOST_WRITING_COMMENT c INNER JOIN EPOST_ARTICLE_SHELF s ON c.WritingID = s.WritingID SET c.ArticleShelfID = s.ArticleShelfID";
		$li->db_db_query($sql);
		
		echo "Patch Comment Done <br/>";
	}else{
		echo "Comment Updated Already <br/>";
	}	
	$sql = "SHOW COLUMNS FROM EPOST_ARTICLE_SHELF LIKE 'Enjoy'";
	$result = $li->db_db_query($sql);	
	$exists = (mysql_num_rows($result))?TRUE:FALSE;
	if(!$exists){	
		$sql = "ALTER TABLE EPOST_ARTICLE_SHELF ADD COLUMN `Enjoy` text AFTER Status";
		$li->db_db_query($sql);
		
		$sql = "UPDATE EPOST_ARTICLE_SHELF s INNER JOIN EPOST_WRITING w ON s.WritingID = w.RecordID SET s.Enjoy = w.ReadFlag WHERE w.ReadFlag != ''";
		$li->db_db_query($sql);
		
		echo "Patch Enjoy Done <br/>";
	}else{
		echo "Enjoy Updated Already <br/>";
	}
	$sql = "SHOW COLUMNS FROM EPOST_WRITING LIKE 'AcademicYearID'";
	$result = $li->db_db_query($sql);	
	$exists = (mysql_num_rows($result))?TRUE:FALSE;
	if(!$exists){	
		$sql = "ALTER TABLE EPOST_WRITING ADD COLUMN `AcademicYearID` INT(8)";
		$li->db_db_query($sql);
		
		$sql = "UPDATE EPOST_WRITING SET AcademicYearID = '".$_SESSION['CurrentSchoolYearID']."'";
		$li->db_db_query($sql);
		echo "Patch SchoolYear Done <br/>";	
	}else{
		echo "SchoolYear Updated Already <br/>";
	}
	$sql = "SELECT column_name FROM information_schema.columns WHERE table_name = 'EPOST_NEWSPAPER_PAGE_ARTICLE' AND TABLE_SCHEMA = '".$li->db."'";		
	$result = $li->returnVector($sql);
	$addFieldAry = array("Title","Content","Enjoy","FontSize","FontStyle","LineHeight");
	$addFieldTypeAry = array("VARCHAR(255)","TEXT","TEXT","TINYINT","TINYINT","TINYINT");
	for($i=0;$i<count($addFieldAry);$i++){
		$_field = $addFieldAry[$i];
		$_fieldType = $addFieldTypeAry[$i]; 
		if(!in_array($_field,$result)){
			$sql = "ALTER TABLE EPOST_NEWSPAPER_PAGE_ARTICLE ADD COLUMN `".$_field."` ".$_fieldType;
			$li->db_db_query($sql);
			if($i<3){
				$sql = "UPDATE EPOST_NEWSPAPER_PAGE_ARTICLE a INNER JOIN EPOST_ARTICLE_SHELF s ON a.ArticleShelfID = s.ArticleShelfID SET a.".$_field." = s.".$_field;
				$li->db_db_query($sql);
			}
			echo "Patch Article ".$_field." Done <br/>";
		}else{
			echo "Article ".$_field." Updated Already <br/>";
		}
	}
	$sql = "SHOW COLUMNS FROM EPOST_NEWSPAPER_PAGE_ARTICLE_ATTACHMENT LIKE 'Caption'";
	$result = $li->db_db_query($sql);	
	$exists = (mysql_num_rows($result))?TRUE:FALSE;
	if(!$exists){	
		$sql = "ALTER TABLE EPOST_NEWSPAPER_PAGE_ARTICLE_ATTACHMENT ADD COLUMN `Caption` text AFTER Size";
		$li->db_db_query($sql);
		
		$sql = "UPDATE EPOST_NEWSPAPER_PAGE_ARTICLE_ATTACHMENT aa INNER JOIN EPOST_WRITING_ATTACHMENT wa ON wa.AttachmentID = aa.AttachmentID SET aa.Caption = wa.ArticleShelfCaption";
		$li->db_db_query($sql);
		
		echo "Patch Article Caption Done <br/>";
	}else{
		echo "Article Caption Updated Already <br/>";
	}	
	$sql    = "SHOW TABLES LIKE 'EPOST_NEWSPAPER_PAGE_ARTICLE_COMMENT'";
	$result = current($li->returnVector($sql));
	if($result=='EPOST_NEWSPAPER_PAGE_ARTICLE_COMMENT'){
		$sql = "SELECT COUNT(*) FROM EPOST_NEWSPAPER_PAGE_ARTICLE_COMMENT";
		$hasPatched = current($li->returnVector($sql));
		if(!$hasPatched){
			$sql = "SELECT WritingID,Content,Status,InputBy,InputDate,ModifiedBy,ModifiedDate,ArticleShelfID FROM EPOST_WRITING_COMMENT";
			$commentAry = $li->returnArray($sql);
			
			if(count($commentAry)>0){	
				$insert_sql = "";	
				foreach((array)$commentAry as $_commentAry){
					if(!empty($_commentAry['Content'])){
						$sql = "SELECT ArticleID FROM EPOST_NEWSPAPER_PAGE_ARTICLE WHERE ArticleShelfID = '".$_commentAry['ArticleShelfID']."'";
						$articleAry = $li->returnVector($sql);
						for($i=0;$i<count($articleAry);$i++){
							$insert_sql .= $insert_sql==''?"":",";
							$insert_sql .= " ('".$articleAry[$i]."', '".addslashes($_commentAry['Content'])."', '".$_commentAry['Status']."', '".$_commentAry['InputBy']."', '".$_commentAry['InputDate']."', '".$_commentAry['ModifiedBy']."', '".$_commentAry['ModifiedDate']."') ";
						}
					}
				}
				$sql = "INSERT INTO EPOST_NEWSPAPER_PAGE_ARTICLE_COMMENT (ArticleID,Content,Status,InputBy,InputDate,ModifiedBy,ModifiedDate) VALUES".$insert_sql;
				$li->db_db_query($sql);
				echo "Patch Article Comment Done <br/>";	
			}
		}else{
			echo "Article Comment Updated Already <br/>";
		}
				
			
	}else{
		echo "EPOST_NEWSPAPER_PAGE_ARTICLE_COMMENT Not Created Yet. <br/>";		
	}		
		
	$sql = "SHOW COLUMNS FROM EPOST_NEWSPAPER_PAGE_ARTICLE_ATTACHMENT LIKE 'Attachment'";
	$result = $li->db_db_query($sql);	
	$exists = (mysql_num_rows($result))?TRUE:FALSE;
	if(!$exists){	
		$sql = "ALTER TABLE EPOST_NEWSPAPER_PAGE_ARTICLE_ATTACHMENT ADD COLUMN `Attachment` text AFTER ArticleID";
		$li->db_db_query($sql);
		
		$sql = "UPDATE EPOST_NEWSPAPER_PAGE_ARTICLE_ATTACHMENT aa INNER JOIN EPOST_WRITING_ATTACHMENT wa ON wa.AttachmentID = aa.AttachmentID SET aa.Attachment = wa.Attachment";
		$li->db_db_query($sql);
		echo "Patch Article Attachment Done <br/>";
	}else{
		echo "Article Attachment Updated Already <br/>";
	}	
	$toDir = $PATH_WRT_ROOT.'file/ePost/article_attachment/';
	
	if(!is_dir($PATH_WRT_ROOT.'file/ePost'))
		 mkdir($PATH_WRT_ROOT.'file/ePost', 0777);
	if(!is_dir($PATH_WRT_ROOT.'file/ePost/article_attachment'))
		 mkdir($PATH_WRT_ROOT.'file/ePost/article_attachment', 0777);
	$sql = "
		SELECT 
			wa.WritingID,wa.ArticleShelfID,aa.ArticleID,wa.Attachment 
		FROM 
			EPOST_NEWSPAPER_PAGE_ARTICLE_ATTACHMENT aa 
		INNER JOIN 
			EPOST_WRITING_ATTACHMENT wa ON wa.AttachmentID = aa.AttachmentID
	";
	$result = $li->returnArray($sql);		
	$aa_cnt = count($result);	
	for($r=0;$r<$aa_cnt;$r++){
		$_writingId = $result[$r]['WritingID'];
		$_articleShelfId = $result[$r]['ArticleShelfID'];		
		$_articleId = $result[$r]['ArticleID'];		
		$_attachment = $result[$r]['Attachment'];	
		if(!empty($_writingId)){
			$_fromDir = $PATH_WRT_ROOT.'file/ePost/writing_attachment/'.$_writingId.'/'; 
		}else if(!empty($_articleShelfId)){
			$_fromDir = $PATH_WRT_ROOT.'file/ePost/appendix_attachment/'.$_articleShelfId.'/'; 					
		}
		$_fileDes = $toDir.$_articleId; 
		if(!is_dir($_fileDes))
			mkdir($_fileDes, 0777);
		$_fileDes .= '/'.$_attachment;	
		if(!file_exists($_fileDes)&&file_exists($_fromDir.$_attachment)){
			$ext = getFileExtention($_attachment);
			copy($_fromDir.$_attachment, $_fileDes);
			if($ext=='FLV'||$ext=='MOV'||$ext=='MP4'){
				$ePost->convertWritingVideo($_fileDes);
			}
		}
	}
	$sql = "SHOW COLUMNS FROM EPOST_NEWSPAPER_PAGE LIKE 'ThemeName'";
	$result = $li->db_db_query($sql);	
	$exists = (mysql_num_rows($result))?TRUE:FALSE;
	if(!$exists){	
		$sql = "ALTER TABLE EPOST_NEWSPAPER_PAGE ADD COLUMN `ThemeName` text AFTER Theme";
		$li->db_db_query($sql);
		
		echo "Patch PAGE ThemeName Done <br/>";
	}else{
		echo "PAGE ThemeName Updated Already <br/>";
	}	

	$sql = "
		SELECT 
			WritingID,ArticleShelfID,Attachment 
		FROM 
			EPOST_WRITING_ATTACHMENT
	";
	$result = $li->returnArray($sql);
	$cnt = count($result);	
	for($r=0;$r<$cnt;$r++){
		$_writingId = $result[$r]['WritingID'];
		$_articleShelfId = $result[$r]['ArticleShelfID'];		
		$_attachment = $result[$r]['Attachment'];	
		if(!empty($_writingId)){
			$_fromDir = $PATH_WRT_ROOT.'file/ePost/writing_attachment/'.$_writingId.'/'; 
			$_fileDes = $_fromDir.$_attachment;	
			if(file_exists($_fileDes)){
				$ext = getFileExtention($_attachment);
				if($ext=='FLV'||$ext=='MOV'||$ext=='MP4'){
					$ePost->convertWritingVideo($_fileDes);
				}
			}
		}else if(!empty($_articleShelfId)){
			$_fromDir = $PATH_WRT_ROOT.'file/ePost/appendix_attachment/'.$_articleShelfId.'/'; 	
			$_fileDes = $_fromDir.$_attachment;	
			if(file_exists($_fileDes)){
				$ext = getFileExtention($_attachment);
				if($ext=='FLV'||$ext=='MOV'||$ext=='MP4'){
					$ePost->convertWritingVideo($_fileDes);
				}
			}			
		}
		

	}	
}


intranet_closedb();
?>