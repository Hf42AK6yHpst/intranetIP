<?php
# using :

$PATH_WRT_ROOT = "../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_request.php");

intranet_auth();
intranet_opendb();

$ePost   	   = new libepost();
$ePostUI 	   = new libepost_ui();
$ePostFolders  = new libepost_folders();
$ePostRequests = new libepost_requests();
$Header='NEW';
$FromPage = 'editor';
$allow_student_comment = $ePost->get_general_setting('DisableStudentComment')?0:1;
# Check if ePost should be accessible
$ePost->auth();

include($intranet_root.'/home/ePost/templates/portal_editor.php');

intranet_closedb();
?>