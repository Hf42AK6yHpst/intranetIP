<?php
# using :

$PATH_WRT_ROOT = "../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");

intranet_auth();
intranet_opendb();

$ePost   = new libepost();
$ePostUI = new libepost_ui();

include($intranet_root.'/home/ePost/templates/expired.php');

intranet_closedb();
?>