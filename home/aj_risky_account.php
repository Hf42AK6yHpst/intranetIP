<?php
# using: 

#################################
#	Date:	2015-07-15	Bill	[2015-0714-1051-41207]
#			use $Lang['login']['password_follow_option_or'] for "OR"
#
#	Date:	2014-10-08 	Carlos
#			Rename var $EnglishName and $ChineseName to $English_Name and $Chinese_Name because it overwrites $_SESSION['ChineseName'] and $_SESSION['EnglishName']			
#
#	Date:	2013-12-03	Ivan
#			- added dynamic size thickbox and the thickbox content div is scrollable now
#
#	Date:	2013-03-07	Yuen
#			first version
#################################

$PATH_WRT_ROOT = "../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

# iMail Plus enabled, assigned user only
# to be changed
if (!$_SESSION['SSV_PRIVILEGE']['schoolsettings']["isAdmin"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");

intranet_auth();
intranet_opendb();

# Create a new interface instance
$linterface = new interface_html("popup_no_layout.html");
$li = new libauth();

$rows = $li->getProtentialRiskByPassword();
$data_size = sizeof($rows);
for ($i=0; $i<$data_size; $i++)
{
	$rowObj = $rows[$i];
	$ClassName = ($rowObj["ClassName"]!="") ? $rowObj["ClassName"] : "--";
	$English_Name = ($rowObj["EnglishName"]!="") ? $rowObj["EnglishName"] : "--";
	$Chinese_Name = ($rowObj["ChineseName"]!="") ? $rowObj["ChineseName"] : "--";
	$ClassNumber = ($rowObj["ClassNumber"]!="" && $rowObj["ClassNumber"]>0) ? str_pad($rowObj["ClassNumber"], 2, "0", STR_PAD_LEFT) : "--";
	$UserLogin = $rowObj["UserLogin"];
	switch($rowObj["RecordType"])
	{
		case 1: $UserTypeShow = $Lang['iMail']['FieldTitle']['TeacherAndStaff']; break;
		case 2: $UserTypeShow = $Lang['Identity']['Student']; break;
		case 3: $UserTypeShow = $Lang['Identity']['Parent'] ; break;
		case 4: $UserTypeShow = $Lang['Identity']['Alumni']; break;
	}
	$LastUsed = ($rowObj["LastUsed"]!="") ? date("Y-m-d", strtotime($rowObj["LastUsed"])) : "--";
	$row_style = ($i%2==0) ? "" : "class='row_avaliable'";
	$ParentIcon = ($rowObj["IsParent"]) ? "(".$Lang['Identity']['Parent'].") " : "";
	$sent_records .= "<tr {$row_style}><td>".($i+1).".</td><td>".$UserTypeShow."</td><td>".$UserLogin."</td><td>".$English_Name."</td><td>".$Chinese_Name."</td><td nowrap='nowrap'>".$ClassName."</td><td nowrap='nowrap'>".$ClassNumber."</td><td nowrap='nowrap'>". $LastUsed."</td></tr>\n";

}

$htmlAry['closeBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Close'], "button", "window.parent.js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

$linterface->LAYOUT_START();
?>
<div id="thickboxContainerDiv" class="edit_pop_board">
	<form id="thickboxForm" name="thickboxForm">
		<div id="thickboxContentDiv" class="edit_pop_board_write">
			<fieldset class="instruction_warning_box_v30">
				<legend><?=$Lang['General']['Warning']?></legend>
				<font size="2"><?=str_replace("[=total_number=]", $data_size, $Lang['login']['password_notes'])?>
				<p>&nbsp; &nbsp; 1. <a href="aj_risky_account_random_assign.php"><?=$Lang['login']['password_follow_option_a'] ?></a><?=$Lang['login']['password_follow_option_or']?></p>
				<p>&nbsp; &nbsp; 2. <?=$Lang['login']['password_follow_option_b'] ?></p></font>
			</fieldset>
			
			<table width="96%" border="0" align="center">
				<tr>
					<td>
					<?=$print_all?>
						<table align="center" class="common_table_list">
						<thead>
							<th width='1%'>#</th>
							<th width='10%'><?=$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['UserType']?></th>
							<th width='15%'><?=$Lang['RepairSystem']['ImportDataCol'][0]?></th>
							<th width='25%'><?=$Lang['General']['EnglishName']?></th>
							<th width='22%'><?=$Lang['General']['ChineseName']?></th>
							<th width='8%'><?=$Lang['SysMgr']['RoleManagement']['Class']?></th>
							<th width='7%'><?=$Lang['SysMgr']['RoleManagement']['ClassNumber']?></th>
							<th width='13%'><?=$Lang['login']['last_used']?></th>
						</thead>
						<tbody>
							<?=$sent_records?>
						</tbody>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<div id="editBottomDiv" class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['closeBtn']?>
			<p class="spacer"></p>
		</div>
	</form>
</div>

<?
	
	intranet_closedb();
$linterface->LAYOUT_STOP();
?>