<form name="form1" action="" method="POST">
	<?= $html["navigation"] ?>
	
	
	<div class="content_top_tool">
		<div class="Conntent_search"><input type="text" name="keyword" value="<?= $html["keyword"] ?>"></div><br style="clear: both;" />
	</div>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom">                                 
				<div class="table_filter"> <?= $html["lesson_selection"] ?> </div>
				<div class="table_filter"> <?= $html["quiz_selection"] ?> </div>
				<?=$html["quiz_script"]?>
				<p class="spacer" />
				<br style="clear:both">
			</td>
			<td valign="bottom">
			<!-- 
				<div class="common_table_tool">
                    <a class="tool_edit" href="javascript:void(0);" onClick="checkEdit(document.form1, 'assessment_id[]', 'manage.php',0);"><?=$button_edit?></a>
                    <a class="tool_copy" href="javascript:void(0);" onClick="checkRemove(document.form1, 'assessment_id[]', 'copy.php');"><?=$button_copy?></a>
                    <a class="tool_delete" href="javascript:void(0);" onClick="checkRemove(document.form1,'assessment_id[]','remove.php');"><?=$button_delete?></a>
				</div> 
			-->
			</td>
		</tr>
	</table>
	<div id="main_table_area">
	<?= $html["display_table"] ?>
	</div>
	<script>
	$(document).ready(function(){
		var selectedQuiz = parseInt($('#quizFilter').val());
		if(isNaN(selectedQuiz)){
			quizResultData = {};
		}else{
			quizResultData = quizResult[selectedQuiz];
		}
		$.post(
			'ajax.php',
			{
				"field": "<?=$field?>",
				"order": "<?=$order?>",
				"pageNo": "<?=$pageNo?>",
				"page_size_change": "<?=$page_size_change?>",
				"numPerPage": "<?=$numPerPage?>",
				"keyword": "<?=$keyword?>",
				"status": "<?=$status?>",
				"course_id": "<?=$course_id?>",
				"quizData": quizResultData
			},
			function(response){
				$('#main_table_area').html(response);
			}
		)
	});
	function updateTable(){
		var selectedQuiz = $('#quizFilter').val();
		$.post(
				'ajax.php',
				{
					"field": "<?=$field?>",
					"order": "<?=$order?>",
					"pageNo": "<?=$pageNo?>",
					"page_size_change": "<?=$page_size_change?>",
					"numPerPage": "<?=$numPerPage?>",
					"keyword": "<?=$keyword?>",
					"status": "<?=$status?>",
					"course_id": "<?=$course_id?>",
					"quizData": quizResult[selectedQuiz]
				},
				function(response){
					$('#main_table_area').html(response);
				}
			)
	}
	function redraw(){
		$('#quizFilter').val('');
		document.form1.submit();
	}
	</script>	
</form>
