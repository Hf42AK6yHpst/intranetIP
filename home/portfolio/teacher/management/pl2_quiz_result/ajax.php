<?php

// Modified by Paul

/********************** Change Log ***********************/

/********************** Change Log ***********************/
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-teacher.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-thirdparty.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/json.php");
iportfolio_auth("T");
intranet_opendb();

$libportfolio_ui = new libportfolio_ui();
$libpf_slp = new libpf_slp();
$libpf_tp = new libpf_third_party();
$json = new JSON_obj();

// Search Parameters
$field = $_POST["field"];
$order = isset($_POST["order"])?$_POST["order"]:1;
$pageNo = $_POST["pageNo"];
$page_size_change = $_POST["page_size_change"];	//	To refresh the page when change the $numPerPage, (without it cannot change the page size)
$numPerPage = !empty($_POST["numPerPage"])?$_POST["numPerPage"]:$page_size;	//	The parameter $page_size seems to be set in global.php, pls use $numPerPage to avoid modified the $page_size

// other parameters
$keyword = trim($_POST["keyword"]);	//	This is the string for searching
$status = isset($_POST["status"])?$_POST["status"]:'all';
$quizData = $json->decode(stripslashes($_POST["quizData"]));
$course_id = ($sys_custom['ncs_dev'])?"881":$_POST["course_id"];
if($quizData==""){
	echo $Lang['General']['NoRecordAtThisMoment'];
}else{
//Pack Quiz Data
$quizResult = array(array('results'=>array()));
foreach($quizData as $q){
	foreach($q['results'] as $uid=>$r){
		if($r!=''){
			$quizResult[0]['results'][$uid] = $r."/".$q['fullMarks'];
		}else{
			$quizResult[0]['results'][$uid] = "--";
		}
	}
}
$quizData = $quizResult;
	
// Table setup
$pfTable = new libpf_dbtable($field, $order, $pageNo);
$pfTable->page_size = $numPerPage;	//	the constructor does not provide setting page size, which is the number of records display per page

// Data(tmp table) setup
$sql = "CREATE TEMPORARY TABLE quiz_result_PL2_report(
			user_id int(11) NOT NULL,
			result varchar(255) default NULL
		)ENGINE = InnoDB  CHARACTER SET=utf8";
$pfTable->db_db_query($sql);
$nameField = ($intranet_session_language=="zh")?"u.firstname":"u.chinesename";


foreach($quizData as $idx=>$content){
	foreach($content["results"] as $uid=>$rs){
		$sql = "INSERT INTO quiz_result_PL2_report VALUES ('".$uid."','".$rs."')";
		$pfTable->db_db_query($sql);
	}
}
$mainSql = "SELECT CONCAT(".$nameField.",IF(u.class_number='','',CONCAT('(', u.class_number,')'))) as Name, re.result AS Result";
$mainSql .= " FROM quiz_result_PL2_report re LEFT JOIN ".classNamingDB($course_id).".usermaster u ON re.user_id=u.user_id";

//~ Table headers			--------------------------||
$columnCount=0;
$pfTable->column_list .= "<th width='1'>#</th>\n";
$pfTable->column_list .= "<th nowrap width='50%'>".$pfTable->column($columnCount++, $Lang['General']['Name'])."</th>\n";	//	column($fieldIndex,$columnTitle)
$pfTable->column_list .= "<th nowrap width='50%'>".$pfTable->column($columnCount++, $Lang['iPortfolio']['ThirdParty']['Result'].$Lang['PL2_Report']['Remark'])."</th>\n";

$pfTable->sql = $mainSql;

//~ Control Display			--------------------------||
$pfTable->IsColOff = "IP25_table";	//	choosing layout
$pfTable->field_array = array("Name", "Result");	//	set the sql field being display
$pfTable->no_col = 1/* row number */+count($pfTable->field_array)/* checkbox */;	//	set the number of fields to be displayed, at least 2, becoz the 1st row is used to display the number of rows
$pfTable->column_array = array(0,0,0);	//	use to control the text style

//~ Get the HTML			--------------------------||
$displayTableHtml = $pfTable->display();
$table_hidden = <<<HTMLEND
<input type="hidden" name="pageNo" value="{$pfTable->pageNo}">
<input type="hidden" name="order" value="{$pfTable->order}">
<input type="hidden" name="field" value="{$pfTable->field}">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="{$pfTable->page_size}">
HTMLEND;
$displayTableHtml .= $table_hidden;
###############################################


###############################################
##	HTML - display_table (containing the hidden form fields for the table settings)
intranet_closedb();
echo $displayTableHtml;
}
?>