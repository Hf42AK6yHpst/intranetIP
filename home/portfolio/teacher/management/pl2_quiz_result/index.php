<?php

// Modified by Paul

/********************** Change Log ***********************/

/********************** Change Log ***********************/
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-teacher.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-thirdparty.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libeclassapiauth.php");
iportfolio_auth("T");
intranet_opendb();

// Search Parameters
$field = $_POST["field"];
$order = isset($_POST["order"])?$_POST["order"]:1;
$pageNo = IntegerSafe($_POST["pageNo"]);
$page_size_change = $_POST["page_size_change"];	//	To refresh the page when change the $numPerPage, (without it cannot change the page size)
$numPerPage = !empty($_POST["numPerPage"])?$_POST["numPerPage"]:$page_size;	//	The parameter $page_size seems to be set in global.php, pls use $numPerPage to avoid modified the $page_size

// other parameters
$keyword = trim($_POST["keyword"]);	//	This is the string for searching
$classId = isset($_POST["YearClassID"])?$_POST["YearClassID"]:'all';
$status = isset($_POST["status"])?$_POST["status"]:'all';

$lpf = new libportfolio();

$libportfolio_ui = new libportfolio_ui();
$libpf_slp = new libpf_slp();
$libpf_tp = new libpf_third_party();
$json = new JSON_obj();
$eclassApiAuth = new libeclassapiauth();

// Get eClass Token & Course ID
$sql = "SELECT c.course_id FROM {$eclass_db}.user_course uc
RIGHT JOIN {$eclass_db}.course c ON uc.course_id=c.course_id AND intranet_user_id='".$_SESSION['UserID']."'
WHERE course_code='NCS'";
$course_id = ($sys_custom['ncs_dev'])?"24":current($lpf->returnVector($sql));

$sql = "SELECT SessionKey FROM INTRANET_USER WHERE UserID='".$UserID."'";
$apiKeys = $eclassApiAuth->GetAPIKeyList();

$getTokenRequestVar = array(
		"eClassRequest"=>array(
				"APIKey"=>$apiKeys[2]["APIKey"],
				"SessionID"=>($sys_custom['ncs_dev'])?"8a092306c043e9d0cdf467e4dde49c6e":current($lpf->returnVector($sql)),
				"RequestMethod"=>"LoginCourses"
		),
		"Request"=>null
);
$postFieldStr = $json->encode($getTokenRequestVar);
$ch = curl_init();
$eclassApiUrl = ($sys_custom['ncs_dev'])?"http://192.168.0.172:8080/":$intranet_rel_path;
curl_setopt($ch, CURLOPT_URL, $eclassApiUrl."webserviceapi/?reqtype=json");
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postFieldStr);
$tokenResponseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($postFieldStr)	
	)
);
$curlData=curl_exec($ch);
curl_close($ch);

$tokenList = $json->decode($curlData);
$AuthToken = $tokenList['Result'][$course_id];

// Get List of Lessons to investigate
$pl2_header = array();
$pl2_header[] = 'Content-Type: application/json';
$pl2_header[] = 'X-CourseId: '.$course_id;
$pl2_header[] = 'X-AuthToken: '.$AuthToken;
$pl2_header[] = 'Accept:  application/json';

//if(!$sys_custom['ncs_dev']){
	$lessonApiUrl = ($sys_custom['ncs_dev'])?"http://192.168.0.172:8080":$website;
	$ch = curl_init();
	$url = $lessonApiUrl."/eclass40/src/powerlesson/api/ncsStatistics/lessons";
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $pl2_header);
	$lpResponseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$lessonData=curl_exec($ch);
	curl_close($ch);
//}else{
//	$lessonData = '{"2":"Henry Test","6":"Thomas testing","22":"SW 1221","39":"SW 1221 - copy","40":"quiz","41":"Test 170120","44":"PL Testing 170309","45":"PL Testing 170309 - copy","46":"Ronald Testing","47":"Testing 20170314","48":"","49":"","50":"Siuwan Test 170317","51":"Henry Test 20170329","52":"","53":"","54":"quiz - copy","55":"","56":"Ronald Testing - copy","57":"","58":"Henry Test 20170329 - copy","59":"Thomas testing - copy","60":"SW 1221 - copy","61":"Siuwan Test 170317 - copy","62":"PL Testing 170309 - copy - copy","63":"PL Testing 170309 - copy","64":"Testing 20170314 - copy","65":"Henry Test - copy","66":"Thomas testing - copy - copy","67":"Henry Test - copy","68":"","69":"Ronald Testing1111","70":"Test 170120 - copy","71":"SW 1221 - copy - copy","72":"SW 1221 - copy","73":"SW 1221 - copy - copy","74":"Henry Test 20170407","75":"Henry FC Test 20170410","76":"Henry Test Power Presenter","77":"","78":"Thomas 201704210950","79":"Henry Test 20170421","80":"Henry Test PowerPresenter & PowerPad","81":"","82":"Henry Test 20170508","83":"","84":"Henry Test 20170516","85":"","86":"Henry Test 20170602","87":"","88":"","89":"Henry Test 20170609","90":"","91":"Thomas","92":"Henry Test 20160619","93":"Henry Copy Test 20160619","95":"Henry Copy Test 20160619 - copy","96":"Henry test powerpad","97":"","98":"Discussion 20170626"}';
//}

$lessonList = $json->decode($lessonData);

//Form lesson selection
$html["lesson_selection"] = "<select name='lesson' onchange='redraw();'>";
//$html["lesson_selection"] .= "<option value=''>".$Lang['PL2_Report']['SelectLesson']."</option>";
$lesson = isset($lesson)?IntegerSafe($lesson):current(array_keys((array)$lessonList));
if(!isset($lessonList['error'])){
	foreach((array)$lessonList as $lid=>$lname){
		$lname = ($lname=="")?$Lang['PL2_Report']['Untitled']:$lname;
		$selected = ($lesson==$lid)?"selected":"";
		$html["lesson_selection"] .= "<option value='".$lid."' $selected>".$lname."</option>";
	}
}
$html["lesson_selection"] .= "</select>";

// Fetch all quizzes within the current lesson
//if(!$sys_custom['ncs_dev']){
$ch = curl_init();
$quizApiUrl = ($sys_custom['ncs_dev'])?"http://192.168.0.172:8080":$website;
$url = $quizApiUrl."/eclass40/src/powerlesson/api/ncsStatistics/lesson/".$lesson."/quizActivities";
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_HTTPHEADER, $pl2_header);
$quizData=curl_exec($ch);
curl_close($ch);
//}else{
//	$quizData = '{"25":{"title":"","quizzes":[{"fullMarks":15,"results":{"2":7.5,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"26":{"title":"","quizzes":[{"fullMarks":1,"results":{"4":null,"6":null,"7":null,"9":null,"11":null,"14":null,"15":null,"19":null,"2":1,"3":1,"5":1,"8":1,"10":1,"12":1,"13":1}}]},"27":{"title":"","quizzes":[{"fullMarks":1,"results":{"2":1,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"42":{"title":"","quizzes":[{"fullMarks":30,"results":{"2":20,"3":5,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"43":{"title":"","quizzes":[{"fullMarks":3,"results":{"2":2,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"46":{"title":"Quiz","quizzes":[{"fullMarks":3,"results":{"2":1.9,"3":1,"4":1,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"48":{"title":"haha","quizzes":[{"fullMarks":2,"results":{"2":0,"3":0.6,"4":0.5,"6":null,"9":null,"11":null,"13":null}},{"fullMarks":1,"results":{"5":0.99,"7":null,"8":null,"10":null,"12":null,"14":null,"15":null,"19":null}}]},"49":{"title":"","quizzes":[{"fullMarks":2,"results":{"2":0,"5":0,"6":0,"7":0,"8":0,"12":0,"13":0,"19":0,"3":0.5,"4":0.5,"9":0.5,"10":0.5,"11":0.5,"14":0.5,"15":0.5}}]},"50":{"title":"","quizzes":[{"fullMarks":1,"results":{"2":0,"3":0,"4":0.5,"5":0,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"51":{"title":"","quizzes":[{"fullMarks":1,"results":{"5":0,"7":0,"8":0,"9":0,"11":0,"12":0,"13":0,"2":0,"3":0,"4":0,"6":0,"10":0,"14":0,"15":0,"19":0}}]},"52":{"title":"","quizzes":[{"fullMarks":1,"results":{"2":0,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"53":{"title":"siuwan here","quizzes":[{"fullMarks":1,"results":{"2":1,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"55":{"title":"","quizzes":[{"fullMarks":2,"results":{"2":0,"3":null,"4":0,"5":null,"6":1,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"56":{"title":"","quizzes":[{"fullMarks":2,"results":{"2":null,"3":null,"4":null,"5":null,"6":1,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"57":{"title":"","quizzes":[{"fullMarks":2,"results":{"2":1,"3":null,"4":null,"5":null,"6":1,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"58":{"title":"","quizzes":[{"fullMarks":2,"results":{"2":2,"3":null,"4":1,"5":1,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"59":{"title":"","quizzes":[{"fullMarks":2,"results":{"2":0,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"61":{"title":"","quizzes":[{"fullMarks":1,"results":{"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"64":{"title":"","quizzes":[{"fullMarks":105,"results":{"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"65":{"title":"","quizzes":[{"fullMarks":3,"results":{"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"66":{"title":"","quizzes":[{"fullMarks":0,"results":{"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"67":{"title":"","quizzes":[{"fullMarks":4,"results":{"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"79":{"title":"","quizzes":[{"fullMarks":2,"results":{"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"87":{"title":"","quizzes":[{"fullMarks":2,"results":{"4":null,"7":0,"9":null,"10":null,"11":null,"13":null,"15":null}},{"fullMarks":0,"results":{"2":0,"3":0,"5":null,"6":null,"8":null,"12":null,"14":null,"19":null}}]},"88":{"title":"different tasks","quizzes":[{"fullMarks":2,"results":{"2":0,"4":null,"6":null,"8":null,"9":null,"10":null,"12":null,"13":null,"15":null}},{"fullMarks":2,"results":{"3":1,"5":null,"7":0,"11":null,"14":null,"19":null}}]},"89":{"title":"","quizzes":[{"fullMarks":1,"results":{"3":0,"6":null,"7":0,"8":null,"10":null,"12":null,"14":null}},{"fullMarks":2,"results":{"2":0.71,"4":null,"5":null,"9":null,"11":null,"13":null,"15":null,"19":null}}]},"90":{"title":"","quizzes":[{"fullMarks":3,"results":{"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"99":{"title":"","quizzes":[{"fullMarks":124,"results":{"2":12.5,"3":33,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"101":{"title":"","quizzes":[{"fullMarks":37,"results":{"2":35,"3":0,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"104":{"title":"","quizzes":[{"fullMarks":3,"results":{"2":null,"3":2,"6":null,"7":null,"10":null,"11":null,"12":null,"15":null}},{"fullMarks":2,"results":{"4":2,"5":null,"8":null,"9":null,"13":null,"14":null,"19":null}}]},"105":{"title":"","quizzes":[{"fullMarks":25,"results":{"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"253":{"title":"","quizzes":[{"fullMarks":1,"results":{"2":1,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"335":{"title":"20170315","quizzes":[{"fullMarks":6,"results":{"2":null,"3":4,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"340":{"title":"20170315 - copy","quizzes":[{"fullMarks":6,"results":{"2":1,"3":1,"4":1,"5":1,"6":1,"7":1,"8":1,"9":1,"11":1,"10":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"349":{"title":"20170315 - copy - copy","quizzes":[{"fullMarks":6,"results":{"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"11":null,"10":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"350":{"title":"20170315 - copy - copy","quizzes":[{"fullMarks":6,"results":{"2":1,"3":1,"4":1,"5":1,"6":1,"7":1,"8":1,"9":1,"11":1,"10":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"878":{"title":"","quizzes":[{"fullMarks":2,"results":{"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"341":{"title":"20170315 - copy - copy","quizzes":[{"fullMarks":6,"results":{"2":2,"3":2,"4":2,"5":2,"6":2,"7":2,"8":2,"9":2,"11":2,"10":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"356":{"title":"20170315 - copy - copy - copy","quizzes":[{"fullMarks":6,"results":{"2":3.5,"3":0,"4":3,"5":null,"6":null,"7":null,"8":null,"9":null,"10":null,"11":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"357":{"title":"20170315 - copy - copy - copy","quizzes":[{"fullMarks":6,"results":{"2":4,"3":4,"4":4,"5":4,"6":4,"7":4,"8":4,"9":4,"11":4,"10":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]},"363":{"title":"20170315 - copy - copy - copy - copy","quizzes":[{"fullMarks":6,"results":{"2":2,"3":2,"4":2,"5":2,"6":2,"7":2,"8":2,"9":2,"11":2,"10":null,"12":null,"13":null,"14":null,"15":null,"19":null}}]}}';
//}
$quizResultList= $json->decode($quizData);

//Form quiz selection
$html["quiz_selection"] = "<select id='quizFilter' name='quizFilter' onchange='updateTable();'>";
//$html["quiz_selection"] .= "<option value=''>".$Lang['PL2_Report']['SelectQuiz']."</option>";
$html["quiz_script"] = "<script> var quizResult = {};";

if(!isset($quizResultList['error'])){
	foreach((array)$quizResultList as $qid=>$quiz){
		$quiz['title'] = ($quiz['title']=="")?$Lang['PL2_Report']['Untitled']:$quiz['title'];
		$selected = ($quizFilter == $qid)?"selected":"";
		$html["quiz_selection"] .= "<option value='".$qid."' $selected>".$quiz['title']."</option>";
		$html["quiz_script"] .= "quizResult[".$qid."]='".$json->encode($quiz['quizzes'])."';";
	}
}
$html["quiz_script"] .= "</script>";

$html["quiz_selection"] .= "</select>";

# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_PLQuizResult";
$CurrentPageName = $iPort['menu']['pl2_quiz_report'];
### Title ###
$TAGS_OBJ[] = array($iPort['menu']['pl2_quiz_report'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

######## Display #########
$PAGE_NAVIGATION[] = array($CurrentPageName);
$html["navigation"] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

if($IS_MANAGE)	//	iportfolio admin
{
	//	$groupIds = $libpf_slp->getUserGroupsInTeacherComment();
} else {
	$groupIds = $libpf_slp->getUserGroupsInTeacherComment($UserID);
}
//debug_r($groupIds);
$userClassInfo = $libpf_slp->getClassInfoByGroupIds($groupIds);
$class_arr = $libpf_slp->refineUserClassInfo($userClassInfo);

$html["keyword"] = $keyword;
$linterface->LAYOUT_START();
include_once("templates/index.tmpl.php");	//	the content besides the new page layout
$linterface->LAYOUT_STOP();
intranet_closedb();
?>