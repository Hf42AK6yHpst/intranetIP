<?php 
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-teacher.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-assessment-report.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
iportfolio_auth("TSP");
intranet_opendb();

$fm = new libfilesystem();
$libpf_ar = new libpf_assessment_report();
$attachment_url = "/file/iportfolio/";

switch($action){
	case 'uploadFile':
		if (isset($_FILES['upload_file'])) {
			$folder = 'assessment/'.$assessment_id.'/'.$student_id;
			$attachment_path = $file_path.$attachment_url.$folder.'/';
			$fm->createFolder($attachment_path);
			$result = move_uploaded_file($_FILES['upload_file']['tmp_name'], $attachment_path.$_FILES['upload_file']['name']);
			if($result){
				$class_id = $libpf_ar->getClassIDByAssessmentID($assessment_id);
				$file_name = $libpf_ar->getSaveFileName($_FILES['upload_file']['name']);
				$libpf_ar->saveStudentAssessment($assessment_id,$class_id,$student_id,$file_name);
				echo 'done@=@'.$_FILES['upload_file']['name'];
			}else{
				echo 'false@=@fail To Upload';
			}
		}else{
			echo 'false@=@no file';	
		}		
		break;
	case 'removeFile':
		$class_id = $libpf_ar->getClassIDByAssessmentID($assessment_id);
		$libpf_ar->removeStudentAssessment($assessment_id,$class_id,$student_id);
		break;
	case 'readFile':
		$AssessmentArr = current($libpf_ar->getStudentAssessmentList($assessmentId,'',array($studentId)));
		$folder = 'assessment/'.$assessmentId.'/'.$studentId;
		$file_name = $AssessmentArr['assessment']['title'];
		$attachment_path = $file_path.$attachment_url.$folder.'/'.$file_name;
		if (file_exists($attachment_path)){
			$libpf_ar->downloadFile($attachment_path, $file_name);
		}
		break;
	default:
		break;
}



intranet_closedb();
?>