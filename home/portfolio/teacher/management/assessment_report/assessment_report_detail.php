<?php

// Modified by Paul

/********************** Change Log ***********************/

/********************** Change Log ***********************/
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-teacher.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-assessment-report.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
iportfolio_auth("T");
intranet_opendb();

// Search Parameters
$field = $_POST["field"];
$order = isset($_POST["order"])?$_POST["order"]:1;
$pageNo = $_POST["pageNo"];
$page_size_change = $_POST["page_size_change"];	//	To refresh the page when change the $numPerPage, (without it cannot change the page size)
$numPerPage = !empty($_POST["numPerPage"])?$_POST["numPerPage"]:$page_size;	//	The parameter $page_size seems to be set in global.php, pls use $numPerPage to avoid modified the $page_size

// other parameters
$keyword = trim($_POST["keyword"]);	//	This is the string for searching
$classId = isset($_POST["YearClassID"])?$_POST["YearClassID"]:'all';
$status = isset($_POST["status"])?$_POST["status"]:'all';
$assessment_id = isset($_GET["assessment_id"])?$_GET["assessment_id"]:'';

if($assessment_id==''){
	header('Location: assessment_report_list.php');
}

$lpf = new libportfolio();
$lpf_ar = new libpf_assessment_report();
$libportfolio_ui = new libportfolio_ui();
$libpf_slp = new libpf_slp();

// Table setup
$pfTable = new libpf_dbtable($field, $order, $pageNo);
$pfTable->page_size = $numPerPage;	//	the constructor does not provide setting page size, which is the number of records display per page

//~ Table headers			--------------------------||
$columnCount=0;
$pfTable->column_list .= "<th width='1'>#</th>\n";
$pfTable->column_list .= "<th nowrap>".$pfTable->column($columnCount++, $Lang['Identity']['Student'])."</th>\n";	//	column($fieldIndex,$columnTitle)
$pfTable->column_list .= "<th>".$pfTable->column($columnCount++, $iPort['Assessment']['AssessmentFile'])."</th>\n";
$pfTable->column_list .= "<th>".$pfTable->column($columnCount++, $iPort['Assessment']['UploadedDate'])."</th>\n";

$pfTable->sql = $lpf_ar->getStudentSubmisstionInfo($assessment_id,$keyword,$status);

//~ Control Display			--------------------------||
$pfTable->IsColOff = "IP25_table";	//	choosing layout
$pfTable->field_array = array("Name","ReportTitle","ModifiedDate");	//	set the sql field being display
$pfTable->no_col = count($pfTable->field_array)+1/* checkbox */;	//	set the number of fields to be displayed, at least 2, becoz the 1st row is used to display the number of rows
$pfTable->column_array = array(0,0,0);	//	use to control the text style

//~ Get the HTML			--------------------------||
$displayTableHtml = $pfTable->display();
$table_hidden = <<<HTMLEND
<input type="hidden" name="pageNo" value="{$pfTable->pageNo}">
<input type="hidden" name="order" value="{$pfTable->order}">
<input type="hidden" name="field" value="{$pfTable->field}">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="{$pfTable->page_size}">
HTMLEND;
$displayTableHtml .= $table_hidden;
###############################################


###############################################
##	HTML - display_table (containing the hidden form fields for the table settings)
$html["display_table"] = $displayTableHtml;

# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_AssessmentReport";
$CurrentPageName = $iPort['menu']['assessment_report'];
### Title ###
$TAGS_OBJ[] = array($iPort['menu']['assessment_report'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

######## Display #########
$PAGE_NAVIGATION[] = array($CurrentPageName, 'assessment_report_list.php');
$PAGE_NAVIGATION[] = array($lpf_ar->getAssessmentReportName($assessment_id),'');
$html["navigation"] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
$html["title"] = $lpf_ar->getAssessmentReportName($assessment_id);
$html['ReleaseDate'] = date('Y-m-d',strtotime($lpf_ar->getAssessmentReportReleaseDate($assessment_id)));

if($IS_MANAGE)	//	iportfolio admin
{
	//	$groupIds = $libpf_slp->getUserGroupsInTeacherComment();
} else {
	$groupIds = $libpf_slp->getUserGroupsInTeacherComment($UserID);
}
//debug_r($groupIds);
$userClassInfo = $libpf_slp->getClassInfoByGroupIds($groupIds);
$class_arr = $libpf_slp->refineUserClassInfo($userClassInfo);
$html["status_selection"] = '
							<select id="status" name="status" class="auto_submit" onchange="document.form1.submit();">
								<option value="all" '.(($status=="all")?"selected":"").'>'.$Lang['Btn']['All'] .'</option>
								<option value="1" '.(($status=="1")?"selected":"").'>'.$iPort['Assessment']['UploadStatus'][1].'</option>
								<option value="2" '.(($status=="2")?"selected":"").'>'.$iPort['Assessment']['UploadStatus'][2].'</option>
							</select>
							';

$linterface->LAYOUT_START();
include_once("templates/detail.tmpl.php");	//	the content besides the new page layout
$linterface->LAYOUT_STOP();
intranet_closedb();
?>