
<form name="form1" action="" method="POST">
	<?= $html["navigation"] ?>
	
	
	<div class="content_top_tool">
		<div class="Conntent_search"><input type="text" name="keyword" value="<?= $html["keyword"] ?>"></div><br style="clear: both;" />
	</div>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td nowrap="">
                <div class="Conntent_tool">
                    <a href="assessment_report_manage.php" class="new"><?=$Lang['Btn']['New']?></a>
                </div>
			</td>
		</tr>
		<tr class="table-action-bar">
			<td valign="bottom">                                 
				<div class="table_filter"><?= $html["class_selection"] ?></div>
				<div class="table_filter"><?= $html["status_selection"] ?></div>
				<p class="spacer" />
				<br style="clear:both">
			</td>
			<td valign="bottom">
				<div class="common_table_tool">
                    <a class="tool_edit" href="javascript:void(0);" onClick="checkEdit(document.form1, 'assessment_id[]', 'assessment_report_manage.php',0);"><?=$button_edit?></a>
                    <a class="tool_copy" href="javascript:void(0);" onClick="checkRemove(document.form1, 'assessment_id[]', 'assessment_report_copy.php');"><?=$button_copy?></a>
                    <a class="tool_delete" href="javascript:void(0);" onClick="checkRemove(document.form1,'assessment_id[]','assessment_report_remove.php');"><?=$button_delete?></a>
				</div> 
			</td>
		</tr>
	</table>
	<?= $html["display_table"] ?>
</form>
