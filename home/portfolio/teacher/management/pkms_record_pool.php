<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");

intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$LibPortfolio = new libpf_slp();
$lpf_ui = new libportfolio_ui();
$lu = new libuser($StudentID);

$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$LibPortfolio->ACCESS_CONTROL("ole");

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_OLE";

########################################################
# Page Config : Start
########################################################
// Default: no effect on processing OLE Record
$IntExt = 0;

$StudentLevel = $LibPortfolio->getClassLevel($lu->ClassName);
$OLEAssignedArr = $LibPortfolio->GET_OLE_RECORD_ASSIGNED($StudentID, $IntExt, 1);

// Check access right
$portfolio__plms_report_config_file = "$eclass_root/files/portfolio_plms_slp_config.txt";
if(!file_exists($portfolio__plms_report_config_file) || get_file_content($portfolio__plms_report_config_file) == null){
	intranet_closedb();
	header("Location: ole_student.php?IntExt=$IntExt&StudentID=$StudentID");
//	exit();
} 
else 
{
	list($r_formAllowed, $startdate, $enddate, $allowStudentPrintPKMSSLP,$issuedate,$r_isPrintIssueDate,$noOfRecord) = explode("\n", trim(get_file_content($portfolio__plms_report_config_file)));
	$formArray = unserialize($r_formAllowed);
//	$allowStudentPrint = unserialize($allowStudentPrintPKMSSLP);
	$new_rec_allow = unserialize($noOfRecord);
	$RecordsAllowed = $new_rec_allow;
	
	if(!(is_array($formArray) && in_array($StudentLevel[0], $formArray))){
		intranet_closedb();
		header("Location: ole_student.php?IntExt=$IntExt&StudentID=$StudentID");
	}

}

// Calculate number of assigned records
if(is_array($OLEAssignedArr)){
	$new_rec_allow = $RecordsAllowed - count($OLEAssignedArr);
	
	if($new_rec_allow < 0){
		$LibPortfolio->RESET_OVERSET_RECORD_SLP($StudentID, $RecordsAllowed, $IntExt, 1);
		$OLEAssignedArr = $LibPortfolio->GET_OLE_RECORD_ASSIGNED($StudentID, $IntExt, 1);
		
		$new_rec_allow = 0;
	}
}

//$OLESettings = $LibPortfolio->GET_OLE_SETTINGS_SLP($IntExt);

//if(is_array($OLESettings))
//{
//	if($OLESettings[$StudentLevel[1]][0] != "")
//	{
//		$RecordsAllowed = $OLESettings[$StudentLevel[1]][0]-count($OLEAssignedArr);
//
//		if($RecordsAllowed < 0)
//    {
//      $LibPortfolio->RESET_OVERSET_RECORD_SLP($StudentID, $OLESettings[$StudentLevel[1]][0], $IntExt);
//      $OLEAssignedArr = $LibPortfolio->GET_OLE_RECORD_ASSIGNED($StudentID, $IntExt);
//    }
//	}
//	else
//		$RecordsAllowed = "inf";
//}
//$new_rec_allow = (strcmp($RecordsAllowed, "inf") == 0 || $RecordsAllowed > 0);

$swap_control =	"
CONCAT('
<table cellpadding=0 border=0 cellspacing=0>
	<tr><td><a href=\"javascript:;\" onClick=\"swap_row(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode,', os.RecordID,', -1)\"><img src=\"/images/{$LAYOUT_SKIN}/icon_sort_a_off.gif\" border=\"0\" width=\"13\" height=\"13\" /></a></td></tr>
	<tr><td><a href=\"javascript:;\" onClick=\"swap_row(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode,', os.RecordID,', 1)\"><img src=\"/images/{$LAYOUT_SKIN}/icon_sort_d_off.gif\" border=\"0\" width=\"13\" height=\"13\" /></a></td></tr>
</table>
')";
########################################################
# Page Config : End
########################################################

########################################################
# Tab Menu : Start
########################################################

//$TabIndex = empty($IntExt) ? IPF_CFG_SLP_MGMT_OLE : IPF_CFG_SLP_MGMT_OUTSIDE;
$TabIndex = IPF_CFG_SLP_MGMT_OLE;

$TabMenuArr = libpf_tabmenu::getSlpMgmtTabs($TabIndex, 1);
### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags($TabIndex);
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

########################################################
# Operations : Start
########################################################
# tab menu
$tab_menu_html = $lpf_ui->GET_TAB_MENU($TabMenuArr);

########################################################
# Operations : End
########################################################

########################################################
# Operation result : Start
########################################################
$op_result = ($msg == "") ? "" : "<td align=\"right\">".$linterface->GET_SYS_MSG($msg)."</td>";
########################################################
# Operation result : End
########################################################

########################################################
# Table content : Start
########################################################
$ELEArray = $LibPortfolio->GET_ELE();
//$ELECount = ($IntExt == 1) ? 0 : count($ELEArray);
$ELECount = count($ELEArray);

$pageSizeChangeEnabled = true;
$checkmaster = true;

# Filter conditions
$cond = (!empty($OLEAssignedArr)) ? " AND os.RecordID IN (".implode(",",$OLEAssignedArr).")" : " AND 1 = 2";
//$cond = " AND os.PKMSSLPOrder IS NOT NULL";

# Main query
$LibTable = new libpf_dbtable(0, 1, $pageNo);

$sql =  "
          SELECT
            op.Title,
            ".Get_Lang_Selection("oc.ChiTitle","oc.EngTitle").",
            IF(ay.AcademicYearID IS NULL, '--', IF(ayt.YearTermID IS NULL, ".Get_Lang_Selection("ay.YearNameB5","ay.YearNameEN").", CONCAT(".Get_Lang_Selection("ay.YearNameB5","ay.YearNameEN").", '<br />', ".Get_Lang_Selection("ayt.YearTermNameB5", "ayt.YearTermNameEN")."))),
            IF(DATE_FORMAT(op.EndDate, '%Y-%m-%d') = '0000-00-00', IF(DATE_FORMAT(op.StartDate, '%Y-%m-%d') = '0000-00-00', '--', DATE_FORMAT(op.StartDate, '%Y-%m-%d')), CONCAT(DATE_FORMAT(op.StartDate, '%Y-%m-%d'), ' {$profiles_to} ', DATE_FORMAT(op.EndDate, '%Y-%m-%d'))) AS period,
        ";

//if ($IntExt != 1)
//{
foreach($ELEArray as $ELECode => $ELETitle)
{
    $sql .= "IF(INSTR(op.ELE, '".$ELECode."') > 0, '<img src=\"/images/2009a/icon_tick_green.gif\" />', ''),";
}
//}
$sql .= "
            ".$swap_control.",
            CONCAT('<input onClick=\"document.form1.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', os.RecordID ,'>')
          FROM
            {$eclass_db}.OLE_STUDENT AS os
          INNER JOIN {$eclass_db}.OLE_PROGRAM AS op
            ON op.ProgramID = os.ProgramID
          LEFT JOIN {$intranet_db}.ACADEMIC_YEAR AS ay
            ON op.AcademicYearID = ay.AcademicYearID
          LEFT JOIN {$intranet_db}.ACADEMIC_YEAR_TERM AS ayt
            ON op.YearTermID = ayt.YearTermID
          LEFT JOIN {$eclass_db}.OLE_CATEGORY AS oc
            ON op.Category = oc.RecordID
          WHERE
            (op.IntExt = 'EXT' || op.IntExt = 'INT') AND
            os.UserID = $StudentID
            $cond
        ";
//hdebug_r(htmlspecialchars($sql));

// TABLE INFO
$LibTable->field_array = array("os.PKMSSLPOrder");
$LibTable->sql = $sql;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
$LibTable->no_col = (7 + $ELECount);
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

// TABLE COLUMN
$LibTable->column_list .= "<tr class='tabletop'>\n";
$LibTable->column_list .= "<td height='25' align='center' rowspan=\"2\" class=\"tabletopnolink\" >#</span></td>\n";
$LibTable->column_list .= "<td rowspan=\"2\" class=\"tabletopnolink\" width=\"150\" >".$ec_iPortfolio['title']."</td>\n";
$LibTable->column_list .= "<td rowspan=\"2\" class=\"tabletopnolink\">".$ec_iPortfolio['category']."</td>";
$LibTable->column_list .= "<td rowspan=\"2\" class=\"tabletopnolink\" nowrap='nowrap'>".$ec_iPortfolio['by_year']."</td>";
$LibTable->column_list .= "<td rowspan=\"2\" >".$ec_iPortfolio['date']."/".$ec_iPortfolio['period']."</td>\n";
//$LibTable->column_list .= ($IntExt != 1) ? "<td colspan=\"".$ELECount."\" class=\"tabletopnolink\" align=\"center\" nowrap='nowrap'>".$ec_iPortfolio['ele']."</td>\n" : "";
$LibTable->column_list .= "<td colspan=\"".$ELECount."\" class=\"tabletopnolink\" align=\"center\" nowrap='nowrap'>".$ec_iPortfolio['ele']."</td>\n";
$LibTable->column_list .= "<td rowspan=\"2\" align='center' nowrap='nowrap'>&nbsp;</td>\n";
$LibTable->column_list .= "<td rowspan=\"2\" >".$LibTable->check("record_id[]")."</td>\n";
$LibTable->column_list .= "</tr>\n";

$LibTable->column_list .= "<tr class=\"tabletop\">";
//if ($IntExt != 1)
//{
//	
foreach($ELEArray as $ELECode => $ELETitle)
{
	$ELEDisplay = str_replace(array("[", "]"), "", $ELECode);
	$ELEDisplay = is_numeric($ELEDisplay) ? $ELETitle : $ELEDisplay;
	$LibTable->column_list .= "<td align='center' class='tabletopnolink'><b><span title='".$ELETitle."'>".$ELEDisplay."</span></b></td>";
}
	
  
 $LibTable->column_array = array_merge(array(0,3,3,3), array_fill(0, $ELECount, 3), array(3,3));				
//}
//else
//{
//  $LibTable->column_array = array(0,3,3,3,3,3);
//}
$LibTable->column_list .= "</tr>";

$table_content = $LibTable->displayPlain();
$table_content .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
$table_content .= ($LibTable->navigationHTML!="") ? "<tr class='tablebottom'><td class=\"tabletext\" align=\"right\">".$LibTable->navigation(1)."</td></tr>" : "";
$table_content .= "</table>";

########################################################
# Table content : End
########################################################

########################################################
# Layout Display
########################################################
$student_info_html = Get_Lang_Selection($lu->ChineseName." (".$lu->ClassName."-".$lu->ClassNumber.")", $lu->EnglishName." (".$lu->ClassName."-".$lu->ClassNumber.")");

$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['student_list'], "ole_studentview.php?IntExt=".$IntExt);
$MenuArr[] = array($student_info_html, "ole_student.php?IntExt=".$IntExt."&StudentID=".$StudentID);
$MenuArr[] = array($ec_iPortfolio['ole_pkms_set_pool_record'], "");
$navigation_html = $linterface->GET_NAVIGATION($MenuArr);

$linterface->LAYOUT_START();
include_once("template/pkms_record_pool.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();

?>