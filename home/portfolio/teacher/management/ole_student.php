<?php
##	Modifying by: 
##
#######################################
##	Modification Log:
##  2018-08-02 Anna [H142673] 
##  - Change approver to input by
##
##	2014-07-15	Bill
##	- Add PKMS SLP Report Setting link
##
##  2013-05-29: Yuen 
##  - Improved: support to export member list to CSV [Case#2013-0110-1235-02054]
##
##  2011-02-16: Ivan 
##  - Improved: Change the Academic Year Selection to IP25 standard selection
##
##  2010-05-24: Fai 
##  - Enhancement : GET OLE PROGRAM TITLE FROM OLE_PROGRAM INSTEAD OF OLE_STUDENT
##
##  2010-05-03: Fai 
##  - Enhancement : support teacher to see the OLE student record with Multiple class teacher
##
##	2010-01-28: Max (201001281018)
##	- Reordered the $TabMenuArr
#######################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-mgmt-group.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

// Initializing classes
$LibPortfolio = new libpf_slp();
$libdb = new libdb();
$lpf_ui = new libportfolio_ui();
$LibUser = new libuser($UserID);
$LibUserStudent = new libuser($StudentID);
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf_mgmt_grp = new libpf_mgmt_group();


//start TEST
function getProgramCanBeApproval($teacherID, $studentID,$programIntExtType,$allowClassTeacherApprove){
	global $eclass_db;
	$lpf_mgmt_grp = new libpf_mgmt_group();
	$LibPortfolio = new libpf_slp();
	$libdb =  new libdb();
	//CHECK user in which group
	$ec_uID = $LibPortfolio->IP_USER_ID_TO_EC_USER_ID($teacherID);
	$group_id_arr = $lpf_mgmt_grp->GET_USER_IN_GROUP($ec_uID);

	//get USER's OLE component
	$mgmt_ele_arr = $lpf_mgmt_grp->getGroupComponent($group_id_arr);
	
	$_com_progamList = array();
	$com_programListStr = "";
	if(!empty($group_id_arr))
	{
		$conds = "";
		for($i=0, $i_max=count($mgmt_ele_arr); $i<$i_max; $i++)
		{
			$mgmt_ele = $mgmt_ele_arr[$i];
		
			$conds .= "OR INSTR(op.ELE, '{$mgmt_ele}') > 0 ";
		}
		

		$sql = "SELECT DISTINCT op.ProgramID FROM {$eclass_db}.OLE_PROGRAM op ";
		$sql .= "WHERE 0 {$conds}";
//error_log("f: ".$sql."\n", 3, "/tmp/aaa.txt");
//debug_r(" component ===>".$sql);	
		$_com_progamList = $libdb->returnVector($sql);	
		
		if(sizeof($_com_progamList) > 0 && is_array($_com_progamList)){
			$com_programListStr = implode(",",$_com_progamList);
			$com_programListStr = " or op.ProgramID in ({$com_programListStr}) ";
		}
	}

	if($allowClassTeacherApprove)
	{
		$sql = "";
		$sql .= "SELECT distinct os.UserId as 'userid' FROM {$eclass_db}.OLE_PROGRAM op ";
		$sql .= "INNER JOIN {$eclass_db}.OLE_STUDENT os ON op.ProgramID = os.ProgramID ";
		$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON os.UserID = ycu.UserID ";
		$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_TEACHER yct ON ycu.YearClassID = yct.YearClassID ";
		$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON yc.YearClassID = yct.YearClassID AND yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." ";
		$sql .= "WHERE yct.UserID = {$teacherID} ";
	//echo "class ".$sql."<Br/>";
//error_log("f: ".$sql."\n", 3, "/tmp/aaa.txt");
		$classStudentRS = $libdb->returnVector($sql);
	
		$classStudentStr = "";
		$classStudentSql = "";
		if(is_array($classStudentRS) && sizeof($classStudentRS) > 0 ){
			$classStudentStr = implode(",",$classStudentRS);
		}
		if($classStudentStr  != ""){
			// $classStudentStr has value
			$classStudentSql = " or os.Userid in({$classStudentStr}) ";
		}
	}

	$sql = "Select distinct op.ProgramID
				from {$eclass_db}.OLE_STUDENT as os
				inner join {$eclass_db}.OLE_PROGRAM as op on os.Programid = op.programid
				where os.USERID = {$studentID}
				and 
				(
					os.RequestApprovedBy = {$teacherID}
						or 
					os.ApprovedBy = {$teacherID}
						or
					op.CreatorID = {$teacherID}
					{$classStudentSql}
					{$com_programListStr}
				)
				and op.IntExt = '{$programIntExtType}'
		   ";
//error_log("f: ".$sql."\n", 3, "/tmp/aaa.txt");
	$programRS = $libdb->returnVector($sql);
//echo "function sql ".$sql."<br/>";
	$programListStr = "";
	if(is_array($programRS) && sizeof($programRS) > 0){
		$programListStr = implode(",",$programRS);
	}
	return $programListStr;
}
//END test

//$LibPortfolio->ACCESS_CONTROL("ole");

########################################################
# Data Preprocessing : Start
########################################################
//GET PASSING VARIABLE
$action = $_SESSION["SESS_OLE_ACTION"];
$isReadOnly = ($action == $ipf_cfg["OLE_ACTION"]["view"]) ? 1 : 0 ;
$no_of_col = ($isReadOnly) ? ($IntExt == 1 ? 15 : 16) : ($IntExt == 1 ? 16 : 17);
define ("STANDARD_NO_OF_COLUMN", $no_of_col);

$IntExtType = ($IntExt == 1) ? "EXT" : "INT";
$approvalSettings  = $LibPortfolio->GET_OLR_APPROVAL_SETTING_DATA2($IntExtType);
$allowClassTeacherApprove = ($approvalSettings["Elements"]["ClassTeacher"] == 1) ? 1 : 0;

$mgmt_ele_arr = array();
if($IntExtType == "INT"){
	// for the component group , it support with INT only
	//CHECK user in which group
	$ec_uID = $LibPortfolio->IP_USER_ID_TO_EC_USER_ID($UserID);
	$group_id_arr = $lpf_mgmt_grp->GET_USER_IN_GROUP($ec_uID);

	//get USER's OLE component
	$mgmt_ele_arr = $lpf_mgmt_grp->getGroupComponent($group_id_arr);
}
$AcademicYearID = $_GET['AcademicYearID'];


//1) the teacher is a OLE ADMIN and
//2a) the teacher is big admin "strstr($ck_function_rights, ":manage:")" OR
//2b) the teacher in a group without ELE component restriction "sizeof($mgmt_ele_arr) == 0"
$isAdmin = $LibPortfolio->isOLEMasterAdmin($mgmt_ele_arr);	
$StudentInfo = Get_Lang_Selection($LibUserStudent->ChineseName." (".$LibUserStudent->ClassName."-".$LibUserStudent->ClassNumber.")", $LibUserStudent->EnglishName." (".$LibUserStudent->ClassName."-".$LibUserStudent->ClassNumber.")");

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = ($isReadOnly) ? "Teacher_OLEView" : "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR("Teacher");

// Tab Menu Settings
if($IntExt == "" || $IntExt  == 0 ||  $IntExt == "0")
{
	$IntExt_0 = 1;
	$IntExt_1 = 0;
}
else
{
	$IntExt_0 = 0;
	$IntExt_1 = 1;
}

//# load approval setting
//$ApprovalSetting = $LibPortfolio->GET_OLR_APPROVAL_SETTING();
$approvalSettings= $LibPortfolio->GET_OLR_APPROVAL_SETTING_DATA2($IntExtType);
if($approvalSettings["Elements"]["ClassTeacher"] == 1){
	$allowClassTeacherApprove = 1;	
}

// load certificate submission period settings
$student_award_config_file = "$eclass_root/files/student_ole_config.txt";
$filecontent = trim(get_file_content($student_award_config_file));	
list($starttime, $sh, $sm, $endtime, $eh, $em) = unserialize($filecontent);

$starttime = (trim($starttime)!="") ? $starttime." $sh:$sm:00" : "";
$endtime = (trim($endtime)!="") ? $endtime." $eh.$em:59" : "";

$ValidPeriod = (($starttime!="" && $starttime!="0000-00-00 00:00:00") || ($endtime!="" && $endtime!="0000-00-00 00:00:00")) ? validatePeriod($starttime, $endtime) : 1;

########################################################
# Data Preprocessing : End
########################################################

########################################################
# Tab Menu : Start
########################################################

$TabIndex = empty($IntExt) ? IPF_CFG_SLP_MGMT_OLE : IPF_CFG_SLP_MGMT_OUTSIDE;

$TabMenuArr = libpf_tabmenu::getSlpMgmtTabs($TabIndex, 1);
### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags($TabIndex);
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################
$currentAcademicYear = Get_Current_Academic_Year_ID();	
$vars = "ClassName=$ClassName&StudentID=$StudentID";
/*
if ($ck_memberType=="S")
{
	list($ClassName, $StudentID) = $LibPortfolio->GET_STUDENT_ID($ck_user_id);

	# define the navigation
	$template_pages = 	Array(
								Array($ec_iPortfolio['ole'], "")
									);
}
else 
{
	header("Location ../index.php");
}
*/
$ValidPeriod = 1;

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

/*
#####################################
if($displayBy=="")
	$displayBy = "Record";

# define the buttons according to the variable $displayBy
$bcolor_record = ($displayBy=="Record") ? "#CFE6FE" : "#FFD49C";
$bcolor_stat = ($displayBy=="Stat") ? "#CFE6FE" : "#FFD49C";
//$bcolor_analysis = ($displayBy=="Analysis") ? "#CFE6FE" : "#FFD49C";

$gif_record = ($displayBy=="Record") ? "l" : "o";
$gif_stat = ($displayBy=="Stat") ? "l" : "o";
//$gif_analysis = ($displayBy=="Analysis") ? "l" : "o";

$link_record = ($displayBy=="Record") ? $ec_iPortfolio['olr_display_record'] : "<a href='index.php?$vars&displayBy=Record' class='link_a'>".$ec_iPortfolio['olr_display_record']."</a>";
$link_stat = ($displayBy=="Stat") ? $ec_iPortfolio['olr_display_stat'] : "<a href='index_stat.php?$vars&displayBy=Stat' class='link_a'>".$ec_iPortfolio['olr_display_stat']."</a>";
//$link_analysis = ($displayBy=="Analysis") ? $ec_iPortfolio['olr_analysis'] : "<a href='index_analysis.php?$vars&displayBy=Analysis' class='link_a'>".$ec_iPortfolio['olr_display_analysis']."</a>";
####################################
*/

########################################################
# Operations : Start
########################################################
// Create a status filter
$status_selection = "<SELECT name='status' onChange='this.form.submit()'>";
$status_selection .= "<OPTION value='' ".($status==0?"SELECTED":"").">".$ec_iPortfolio['all_status']."</OPTION>";
$status_selection .= "<OPTION value='1' ".($status==1?"SELECTED":"").">".$ec_iPortfolio['pending']."</OPTION>";
$status_selection .= "<OPTION value='2' ".($status==2?"SELECTED":"").">".$ec_iPortfolio['approved']."</OPTION>";
$status_selection .= "<OPTION value='3' ".($status==3?"SELECTED":"").">".$ec_iPortfolio['rejected']."</OPTION>";
$status_selection .= "<OPTION value='4' ".($status==4?"SELECTED":"").">".$ec_iPortfolio['teacher_submit_record']."</OPTION>";
$status_selection .= "</SELECT>";
$status_selection_html = $ec_iPortfolio['status']." : ".$status_selection;

# cateogry selection
//$category_selection = $LibPortfolio->GET_OLE_CATEGORY_SELECTION("name='category' onChange='this.form.submit()'", $category, 1, true);
$category_selection_html = $LibPortfolio->GET_OLE_CATEGORY_SELECTION("name='category' onChange='this.form.submit()'", $category, 1);
//$category_selection_html = $ec_iPortfolio['category']." : ".$category_selection;

# component selection
$ELEArray = $LibPortfolio->GET_ELE();
$ELECodeArray = array_keys($ELEArray);
if (count($ELECodeArray)>0)
{
	$ELEImplode = implode(",",$ELECodeArray);
}
$ELECount = count($ELECodeArray);
$ele_selection_html = ($IntExt == 1) ? "" : $LibPortfolio->GET_ELE_SELECTION("name='ELE' onChange='this.form.submit()'", $ELE, 1);

# Searching box
$searching_html = "<input name='search_text' value='".$search_text."' >&nbsp;<input class='button_g' onClick=\"this.form.submit();\" type='button' value=\"".$button_search."\">";

$YearArray = $LibPortfolio->GET_OLE_DISTINCT_YEAR($StudentID);
//$YearSelectionHTML = $linterface->GET_SELECTION_BOX($YearArray,"name='Year' onChange='this.form.submit()'", $i_general_all, $Year);

if($Year!='')
{
	$YearSelectionHTML = getSelectAcademicYear('Year', 'onChange="this.form.submit()"', $noFirst=1, $noPastYear=0, $Year, $displayAll=1);
}


//debug_r($currentAcademicYear);
########################################################
# Operations : End
########################################################

///////////////////////////////////////////////////////
///// TABLE SQL
///////////////////////////////////////////////////////
$pageSizeChangeEnabled = true;
$checkmaster = true; 

if ($order=="") $order=0;
if ($field=="") $field=1;
$LibTable = new libpf_dbtable($field, $order, $pageNo);

$conds = ($status=="") ? "" : " AND a.RecordStatus = '$status'";
$conds .= ($category=="") ? "" : " AND c.Category = '$category'";
$conds .= ($ELE=="") ? "" : " AND c.ELE LIKE '%$ELE%'";

//if($Year!="")
//{
//	$SplitArray = explode("-", $Year);
//	if($SplitArray[0] == "0000")
//	{
//    $LowerDate = 0;
//    $UpperDate = 0;
//  }
//  else
//  {
//  	if(sizeof($SplitArray)>1)
//  	{
//  		$LowerDate = mktime(0, 0, 0, 9, 1, trim($SplitArray[0]));
//  		$UpperDate = mktime(0, 0, 0, 8, 31, trim($SplitArray[1]));
//  	}
//  	else
//  	{
//  		$LowerDate = mktime(0, 0, 0, 9, 1, $Year);
//  		$UpperDate = mktime(0, 0, 0, 8, 31, $Year+1);
//  	}
//  }
//	$conds .= " AND UNIX_TIMESTAMP(c.StartDate) BETWEEN ".$LowerDate." AND ".$UpperDate;
//}



if($Year!="") {
	$conds .= " AND c.AcademicYearID = '".$Year."' ";
}

$namefield = getNameFieldWithClassNumberByLang("b.");

/*
if ($approvalSettings['IsApprovalNeed'] && $approvalSettings['Elements']['ClassTeacher'])	//only class teachers and admin show checkbox
{
	//CHECK WHETHER THE LOGIN USER IS A CLASS TEACHER
	//$sql = "SELECT ClassID FROM INTRANET_CLASSTEACHER WHERE UserID = '$UserID'";
	$sql = "select 
				YearClassTeacherID 
			from 
				{$intranet_db}.YEAR_CLASS_TEACHER as yct 
			inner join 
				{$intranet_db}.YEAR_CLASS as yc on yct.YearClassID = yc.YearClassID and yc.AcademicYearID = {$currentAcademicYear} 
			where 
				yct.UserID = {$UserID}";

	$result = $LibPortfolio->returnArray($sql,1);
	($result==NULL)? $isClassTeacher = false : $isClassTeacher = true;
	
	if ($LibPortfolio->IS_IPF_ADMIN() || $isClassTeacher)
	{
		$edit_field = "CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>')";
	}
	else
	{
		$edit_field = "CONCAT('&nbsp;')";
	}
	
}
else if ($approvalSettings['IsApprovalNeed'] && $approvalSettings['Elements']['Self'])	//only the assigned teacher or admin show checkbox
{
	if ($LibPortfolio->IS_IPF_ADMIN() && strstr($ck_function_rights, "Growth"))
	{
		$edit_field = "CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>')";
	}
	else
	{
		$edit_field = "if((a.RecordStatus = 1 AND a.ApprovedBy = ".$UserID.") , CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>'), '')";
	}
	
}
else if (!$approvalSettings['IsApprovalNeed'])	//show checkbox to all
{
	$edit_field = "CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>')";
}
*/


$programListStr = "";

if($isAdmin || $isReadOnly){
	// IF is a admin login or for view only , no need to get the restrict program
	// do nothing
}else{
	// NOT a ADMIN or NOT read only ,  get the Program that can be approval 
	$programListStr = getProgramCanBeApproval($UserID,$StudentID,$IntExtType,$allowClassTeacherApprove);
//	echo "restrict list == ".$programListStr."<br/><br/>";
	if(trim($programListStr) == ""){
		//since the return is EMPTY , than mean this teacher cannot approve any Program , so set the SQL condition as c.ProgramID is -9999 (this condition should return empty result set
		$restrictProgramListSQL = " and c.ProgramID = (-9999) ";
	}else{
		$restrictProgramListSQL = " and c.ProgramID in ({$programListStr}) ";
	}
}
# get ELE title
$ELEFields = $LibPortfolio->GET_ELE_FIELD_FOR_RECORD("a.");
$SqlELEFields = ($ELEFields == "") ? "'-'," : "$ELEFields,";

# get Category field
$CategoryField = $LibPortfolio->GET_OLR_Category_Field_For_Record("c.", true);
$SqlCategoryField = ($CategoryField == "") ? "'-'," : "$CategoryField,";

if($search_text!="")
{
	$search_cond = " AND (c.Title LIKE '%".$search_text."%' OR c.Details LIKE '%".$search_text."%' OR a.Role LIKE '%".$search_text."%'  OR a.Achievement LIKE '%".$search_text."%')";
}
$int_ext_cond = ($IntExt == 1) ? " AND c.IntExt = 'EXT' " : " AND c.IntExt = 'INT' ";

//$edit_field2 = " , if((a.RecordStatus = 1 AND a.ApprovedBy = ".$UserID.") , CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>'), '')";
if($action == $ipf_cfg["OLE_ACTION"]["view"]){
	$edit_field = "concat('&nbsp;')";
}else{
	$edit_field = "CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>')";
}
//$SqlELEFields

if($IntExt == 1)
{
	$sql = "SELECT
					a.RecordStatus,
					'{$ELECount}',
					'{$ELEImplode}',
					CONCAT('<a class=\"tablelink\" href=\"ole_detail.php?IntExt={$IntExt}&status={$status}&record_id=', a.RecordID, '&StudentID={$StudentID}\">', c.Title, '</a>'),
					IF (IFNULL(DATE_FORMAT(c.StartDate, '%Y-%m-%d'), '0000-00-00') = '0000-00-00', '--', IF(IFNULL(DATE_FORMAT(c.EndDate, '%Y-%m-%d'), '0000-00-00') = '0000-00-00', DATE_FORMAT(c.StartDate, '%Y-%m-%d'), CONCAT(DATE_FORMAT(c.StartDate, '%Y-%m-%d'),'<br /> $profiles_to <br />', DATE_FORMAT(c.EndDate, '%Y-%m-%d')))) as OLEDate,
					c.ELE,
					a.Role,
					a.Achievement,
			  	$SqlCategoryField
			  	IF ((a.Attachment!='' AND a.Attachment IS NOT NULL), CONCAT('<a href=\"attach.php?RecordID=', a.RecordID, '\" target=\"_blank\"><img src=\"$image_path/icon/attachment.gif\" border=0></a>'), '&nbsp;')
				";
}
else
{
	$sql = "SELECT
					a.RecordStatus,
					'{$ELECount}',
					'{$ELEImplode}',
					CONCAT('<a class=\"tablelink\" href=\"ole_detail.php?IntExt={$IntExt}&status={$status}&record_id=', a.RecordID, '&StudentID={$StudentID}\">', c.Title, '</a>'),
					IF(IFNULL(DATE_FORMAT(c.StartDate, '%Y-%m-%d'), '0000-00-00') = '0000-00-00', '--', IF(IFNULL(DATE_FORMAT(c.EndDate, '%Y-%m-%d'), '0000-00-00') = '0000-00-00', DATE_FORMAT(c.StartDate, '%Y-%m-%d'), CONCAT(DATE_FORMAT(c.StartDate, '%Y-%m-%d'),'<br /> $profiles_to <br />', DATE_FORMAT(c.EndDate, '%Y-%m-%d')))) as OLEDate,
					c.ELE,
					a.Role,
					a.Achievement,
			  	$SqlCategoryField			  	
			  	a.Hours,
			  	IF ((a.Attachment!='' AND a.Attachment IS NOT NULL), CONCAT('<a href=\"attach.php?RecordID=', a.RecordID, '\" target=\"_blank\"><img src=\"$image_path/icon/attachment.gif\" border=0></a>'), '&nbsp;')				  	
		";
}
		
$sql .= ", CASE a.RecordStatus
						WHEN 1 THEN '<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title=\"{$ec_iPortfolio['pending']}\">'
						WHEN 2 THEN '<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif\" title=\"{$ec_iPortfolio['approved']}\">'
						WHEN 3 THEN '<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif\" title=\"{$ec_iPortfolio['rejected']}\">'
						WHEN 4 THEN '<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_teacher_input.gif\" title=\"{$ec_iPortfolio['teacher_submit_record']}\">'
					ELSE '--' END,
					IFNULL(DATE_FORMAT(a.ProcessDate,'%Y-%m-%d'),'--') AS ProcessDate,
					IF(a.RecordStatus = 1 OR IFNULL(".getNameFieldByLang("b.").", '') = '', '--', ".getNameFieldByLang("b.").") AS Processor,
					(".getNameFieldByLang("d.").") AS InputBy,
			  	$edit_field  	
			  	, IF((Month(c.StartDate)>8), Year(c.StartDate), Year(c.StartDate)-1) AS StartYear
					FROM
						{$eclass_db}.OLE_STUDENT as a
					INNER JOIN {$eclass_db}.OLE_PROGRAM as c
						ON a.ProgramID = c.ProgramID
					LEFT JOIN {$intranet_db}.INTRANET_USER as b
						ON a.ApprovedBy = b.UserID
					LEFT JOIN {$intranet_db}.INTRANET_USER as d
						ON a.InputBy = d.UserID
					WHERE
						a.UserID = '$StudentID'
						$restrictProgramListSQL
						$conds
						$search_cond
						$int_ext_cond
				";

//echo "<br/><br/>last last ".$sql."<Br/>";
//debug_r(htmlspecialchars($sql));
// TABLE INFO
//$LibTable->field_array = array("a.Title","OLEDate", "a.Role","a.Achievement",$CategoryField, "a.Hours", "a.Attachment", "a.RecordStatus");
if($IntExt == 1)
{
	# use getNameFieldByLang("b.") to sort it by username rather then the whole html link string, i.e. sort bu b.EnglishName
	$LibTable->field_array = array("a.Title","OLEDate", "a.Role","a.Achievement",$CategoryField, "a.RecordStatus", "ProcessDate", "Processor","InputBy");
}
else
{
	$LibTable->field_array = array("a.Title","OLEDate", "a.Role","a.Achievement",$CategoryField, "a.Hours", "a.RecordStatus", "ProcessDate", "Processor","InputBy");
}
////error_log($sql."\n", 3, "/tmp/aaa.txt");
$LibTable->sql = $sql;
$LibTable->db = $intranet_db;
$LibTable->no_msg = $no_record_msg;
if ($IsExport)
{
	$LibTable->page_size = 99999;
	
} else
{
	$LibTable->page_size = ($numPerPage=="") ? $page_size : $numPerPage;
}

$LibTable->no_col = STANDARD_NO_OF_COLUMN;
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1'>";
//$LibTable->row_alt = array("#FFFFFF", "F3F3F3");
$LibTable->row_alt = array("", "");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

if($IntExt == 1)
$LibTable->IsColOff = "iPortfolioStudentOLE1_EXT";
else
$LibTable->IsColOff = "iPortfolioStudentOLE1";

if($IntExt != 1)
$LibTable->additionalCols = count($ELECodeArray);


//$ELEArray = $LibPortfolio->GET_ELE();

$rowStyle = ($IntExt == 1) ? "" : "rowspan=\"2\"";

// TABLE COLUMN

$LibTable->column_list .= "<tr class='tabletop'>\n";

$LibTable->column_list .= "<td height='25' class='tabletoplink' align='center' nowrap='nowrap' $rowStyle >#</span></td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' $rowStyle width='100' nowrap='nowrap' >".$LibTable->column(0,$ec_iPortfolio['title'], 1)."</td>\n";
$LibTable->column_list .= "<td $rowStyle width='100' nowrap='nowrap' >".$LibTable->column(1,$ec_iPortfolio['date']." / ".$ec_iPortfolio['period'], 1)."</td>\n";

if($IntExt != 1)
$LibTable->column_list .= "<td colspan=\"".$ELECount."\" align=\"center\" nowrap='nowrap' class=\"tabletopnolink\">{$ec_iPortfolio['ele']}</td>\n";

$LibTable->column_list .= "<td $rowStyle >".$LibTable->column(2,$ec_iPortfolio['ole_role'], 1)."</td>\n";
$LibTable->column_list .= "<td $rowStyle nowrap>".$LibTable->column(3,$ec_iPortfolio['achievement'], 1)."</td>\n";
$LibTable->column_list .= "<td $rowStyle nowrap='nowrap'>".$LibTable->column(4,$ec_iPortfolio['category'], 1)."</td>\n";

//if($IntExt != 1)
//$LibTable->column_list .= "<td $rowStyle nowrap='nowrap'>".$LibTable->column(5,$ec_iPortfolio['hours'], 1)."</td>\n";

//$LibTable->column_list .= "<td $rowStyle class='tabletoplink' width='5' >".$ec_iPortfolio['attachment_only']."</td>\n";
//$LibTable->column_list .= "<td $rowStyle nowrap='nowrap'>".$LibTable->column(6,$ec_iPortfolio['status']."/<br>".$ec_iPortfolio['process_date'], 1)."</td>\n";
if($IntExt == 1)
{
  $LibTable->column_list .= "<td class='tabletop' width='5' align='center'><span class='tabletoplink'>".$ec_iPortfolio['attachment_only']."</span></td>\n";
  $LibTable->column_list .= "<td $rowStyle class='tabletop' align='center' nowrap='nowrap'>".$LibTable->column(5,$ec_iPortfolio['status'], 1)."</td>\n";
  $LibTable->column_list .= "<td $rowStyle width='100' class='tabletop' align='center' nowrap='nowrap'>".$LibTable->column(6,$Lang['iPortfolio']['approvaldate'], 1)."</td>\n";
  $LibTable->column_list .= "<td $rowStyle class='tabletop' align='center' nowrap='nowrap'>".$LibTable->column(7,$Lang['iPortfolio']['approver'], 1)."</td>\n";
  $LibTable->column_list .= "<td $rowStyle class='tabletop' align='center' nowrap='nowrap'>".$LibTable->column(7,$Lang['iPortfolio']['InputBy'], 1)."</td>\n";
}
else
{
  $LibTable->column_list .= "<td $rowStyle class='tabletop' align='center' nowrap='nowrap'>".$LibTable->column(5,$ec_iPortfolio['hours'], 1)."</td>\n";
  $LibTable->column_list .= "<td $rowStyle class='tabletop' width='5' align='center'><span class='tabletoplink'>".$ec_iPortfolio['attachment_only']."</span></td>\n";
  $LibTable->column_list .= "<td $rowStyle class='tabletop' align='center' nowrap='nowrap'>".$LibTable->column(6,$ec_iPortfolio['status'], 1)."</td>\n";
  $LibTable->column_list .= "<td $rowStyle width='100' class='tabletop' align='center' nowrap='nowrap'>".$LibTable->column(7,$Lang['iPortfolio']['approvaldate'], 1)."</td>\n";
  $LibTable->column_list .= "<td $rowStyle class='tabletop' align='center' nowrap='nowrap'>".$LibTable->column(8,$Lang['iPortfolio']['approver'], 1)."</td>\n";
  $LibTable->column_list .= "<td $rowStyle class='tabletop' align='center' nowrap='nowrap'>".$LibTable->column(8,$Lang['iPortfolio']['InputBy'], 1)."</td>\n";
}

$h_checkAll = ($action == $ipf_cfg["OLE_ACTION"]["view"]) ? "" : "<td $rowStyle >".$LibTable->check("record_id[]")."</td>\n";
$LibTable->column_list .= $h_checkAll;

$LibTable->column_list .= "</tr>\n";


if (count($ELECodeArray) > 0 && $IntExt != 1)
{
	$oletitle_count = 0;
	$LibTable->column_list .= "<tr class=\"tabletop\">";
	foreach($ELEArray as $ELECode => $ELETitle)
	{
//		$CodePos = array_search($ELECode, $ELECodeArray);
//		$TargetField = $CodePos+1;
//		$ELEDisplay = $ELETitle;
//		$LibTable->column_list .= "<td align='center' class='tabletopnolink'><b>".$ELEDisplay."</b></td>";
		$ELEDisplay = str_replace(array("[", "]"), "", $ELECode);
		$ELEDisplay = is_numeric($ELEDisplay) ? $ELETitle : $ELEDisplay;
		$LibTable->column_list .= "<td align='center' class='tabletopnolink'><b><span id='oletitle_".$ELECode."' title='".$ELETitle."'>".$ELEDisplay."</span></b></td>";
	}

	$LibTable->column_list .= "</tr>";				
}


//$LibTable->column_list .= 

if($ck_memberType=="S")
	$LibTable->column_array = array(10,10,0,10,0,0,10,0);
else
	$LibTable->column_array = ($approvalSettings['IsApprovalNeed'] && $approvalSettings['Elements']['Self']) ? array(10,10,0,10,10,0,10,10) : array(10,10,0,10,10,0,10);
	
//$ELEArray = $LibPortfolio->GET_ELE();
//////////////////////////////////////////////



if ($msg == "3")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("delete")."</td></tr>";
} 
else if ($msg == "1")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("add")."</td></tr>";
} 
else if ($msg == "2")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("update")."</td></tr>";
} 
else
{
	$xmsg = "";
}

# Check if teacher has right to set OLE records for SLP report
if($LibPortfolio->IS_CLASS_TEACHER() || $LibPortfolio->IS_IPF_ADMIN())
{
	$StudentLevel = $LibPortfolio->getClassLevel($LibUserStudent->ClassName);
	$OLESettings = $LibPortfolio->GET_OLE_SETTINGS_SLP($IntExt);
	
	if(is_array($OLESettings))
		$SettingAllowed = $OLESettings[$StudentLevel[1]][1];
}
$ole_new_btn_html = $linterface->GET_LNK_NEW("ole_new.php?IntExt=$IntExt&StudentID=$StudentID",$button_new,"","","",0);;
$ole_assign_student_btn_html = $linterface->GET_LNK_EXPORT("javascript:goExport()",$button_export,"","","",0);

# Check if teacher has right to set OLE records for Pui Kiu SLP report
if($sys_custom['iPf']['pkms']['Report']['SLP'] && ($LibPortfolio->IS_CLASS_TEACHER() || $LibPortfolio->IS_IPF_ADMIN()))
{
	
	$PKMSAccess = false;
	
	$portfolio__plms_report_config_file = "$eclass_root/files/portfolio_plms_slp_config.txt";
	if(file_exists($portfolio__plms_report_config_file) && get_file_content($portfolio__plms_report_config_file) != null)
	{	
		list($r_formAllowed, $startdate, $enddate, $allowStudentPrintPKMSSLP,$issuedate,$r_isPrintIssueDate,$noOfPKMSRecord) = explode("\n", trim(get_file_content($portfolio__plms_report_config_file)));
		$formArray = unserialize($r_formAllowed);
//		$allowStudentPrint = unserialize($allowStudentPrintPKMSSLP);
		$StudentLevel = $LibPortfolio->getClassLevel($LibUserStudent->ClassName);
		
		// student is in allowed form
		if(is_array($formArray) && in_array($StudentLevel[0], $formArray) && $IntExt == 0){
			$PKMSAccess = true;
		}
		
//		$selectStartDate = trim(unserialize($startdate));
//		$selectEndDate = trim(unserialize($enddate));
//		$today = date("Y-m-d");
//		
//		if($selectStartDate != null && $selectEndDate != null && !( $today < $selectStartDate || $today > $selectEndDate)){
//			$inDateRange = true;
//		}
	}
}

if ($IsExport)
{
	# export to CSV
	
	$le = new libexporttext();

	$export_header = array($ec_iPortfolio['title'], $ec_iPortfolio['date']." / ".$ec_iPortfolio['period']);
	if($IntExt == 1)
	{
	} else
	{
		//$export_header[] = $ec_iPortfolio['ele'];
		
		if (count($ELECodeArray) > 0 && $IntExt != 1)
		{
			$oletitle_count = 0;
			foreach($ELEArray as $ELECode => $ELETitle)
			{
				//$ELEDisplay = str_replace(array("[", "]"), "", $ELECode);
				//$ELEDisplay = is_numeric($ELEDisplay) ? $ELETitle : $ELEDisplay;		
				$export_header[] = $ELETitle . " " . $ELECode;
			}
		}
	}
	
		$export_header[] = $ec_iPortfolio['ole_role'];
		$export_header[] = $ec_iPortfolio['achievement'];
		$export_header[] = $ec_iPortfolio['category'];
	
	if($IntExt == 1)
	{
	}
	else
	{
			$export_header[] = $ec_iPortfolio['hours'];
	}


	$export_header[] = $ec_iPortfolio['attachment_only'];
	  $export_header[] = $ec_iPortfolio['status'];
	  $export_header[] = $Lang['iPortfolio']['approvaldate'];
	  $export_header[] = $Lang['iPortfolio']['approver'];
	  
	# loop
	$export_ary = $LibTable->exportOLE_StudentRecordList(3, $ELEArray, $IntExt); 
	
	$export_text = $le->GET_EXPORT_TXT($export_ary,$export_header);
	$filename = "OLE_student_".$LibUserStudent->EnglishName." (".$LibUserStudent->ClassName."-".$LibUserStudent->ClassNumber.")".".csv";
	$le->EXPORT_FILE($filename,$export_text); 
	    
	intranet_closedb();
	die();
} else
{
	

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
function checkApprove(obj,element,page,status){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(globalAlertMsg4)){
                obj.action=page;
                obj.approve.value=status;
                obj.submit();
                }
        }
}

function checkRejectNoDel(obj,element,page,status){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(globalAlertMsgRejectNoDel)){
                obj.action=page;
                obj.approve.value=status;
                obj.submit();
                }
        }
}

function goExport() {
	document.form1.IsExport.value = 1;
	document.form1.submit();
	document.form1.IsExport.value = 0;	
}
</SCRIPT>

<FORM name="form1" method="GET">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
			<?=$lpf_ui->GET_TAB_MENU($TabMenuArr) ?>
		
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<?=$xmsg?>	
				<tr>
					<td>
						<!-- CONTENT HERE -->
						<?php if (!$isReadOnly && strstr($ck_function_rights, "Profile:OLR")) { ?>	
			
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="5">
											<tr>
												<td class="navigation">
													<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" align="absmiddle">
													<a href="ole_studentview.php?IntExt=<?=$IntExt?>"><?=$ec_iPortfolio['student_list']?></a>				
													<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" align="absmiddle">
													<?=$StudentInfo?>				
												</td>				 
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="right">		
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>				
												<td>	
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="tabletext">
																<table border="0" cellspacing="0" cellpadding="9">
																	<tr>
																		<td><?= $ole_new_btn_html ?></td>
																		<td>
																			<?= $ole_assign_student_btn_html ?>
																		</td>
																		<?php if ($SettingAllowed) { ?>
																			<td>
																				<a href="ole_record_pool.php?IntExt=<?=$IntExt?>&StudentID=<?=$StudentID?>" class="contenttool">
																				<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_assign.gif" width="12" height="12" border="0" align="absmiddle"> <?=$ec_iPortfolio['ole_set_pool_record']?> </a>
																			</td>
																		<?php } ?>
																		<?php if($sys_custom['iPf']['pkms']['Report']['SLP'] && $PKMSAccess){ ?>
																			<td>
																				<a href="pkms_record_pool.php?IntExt=<?=$IntExt?>&StudentID=<?=$StudentID?>" class="contenttool">
																				<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_assign.gif" width="12" height="12" border="0" align="absmiddle"> <?=$ec_iPortfolio['ole_pkms_set_pool_record']?> </a>
																			</td>
																		<?php } ?>
																	</tr>
																</table>
															</td>
															<td align="right" valign="bottom" class="tabletext">&nbsp;</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						<?php } ?>		
						
						<table border="0" cellpadding="3" cellspacing="0">
							<tr>
								<td><?=$YearSelectionHTML?></td>
								<td><?=$ele_selection_html?></td>
								<td><?=$category_selection_html?></td>						
								<td><?=$status_selection?></td>
							</tr>
						</table>						
					</td>
					</tr>
                    <tr>
					<td align="right" valign="bottom">
						<? if (!$isReadOnly) { ?>
                        <div class="common_table_tool">
                            <a href="javascript:checkApprove(document.form1,'record_id[]','student_ole_status_update2.php', 2)" class="tool_approve"><?php echo $button_approve;?></a>
                            <a href="javascript:checkRejectNoDel(document.form1,'record_id[]','student_ole_status_update2.php', 3)" class="tool_reject"><?php echo $button_reject;?></a>
                            <a href="javascript:checkEdit(document.form1,'record_id[]','ole_edit.php',0)" class="tool_edit"><?php echo $ec_iPortfolio['edit'];?></a>
                            <a href="javascript:checkRemove(document.form1,'record_id[]','ole_student_remove.php')" class="tool_delete"><?php echo $ec_iPortfolio['delete'];?></a>
                        </div>

<!--							<table border="0" cellpadding="0" cellspacing="0">-->
<!--								<tbody>-->
<!--									<tr>-->
<!--										<td width="21"><img src="--><?//=$PATH_WRT_ROOT?><!----><?//=$image_path?><!--/--><?//=$LAYOUT_SKIN?><!--/management/table_tool_01.gif" height="23" width="21"></td>-->
<!--										<td background="--><?//=$PATH_WRT_ROOT?><!----><?//=$image_path?><!--/--><?//=$LAYOUT_SKIN?><!--/management/table_tool_02.gif">-->
<!--											<table border="0" cellpadding="0" cellspacing="2">-->
<!--												<tbody>-->
<!--													<tr>-->
<!--														<td><img src="--><?//=$PATH_WRT_ROOT?><!----><?//=$image_path?><!--/--><?//=$LAYOUT_SKIN?><!--/10x10.gif" width="5"></td>-->
<!--														<td nowrap><a href="javascript:checkApprove(document.form1,'record_id[]','student_ole_status_update2.php', 2)" class="tabletool"><img src="--><?//=$PATH_WRT_ROOT?><!----><?//=$image_path?><!--/--><?//=$LAYOUT_SKIN?><!--/icon_approve.gif" border="0" align="absmiddle">--><?//=$button_approve?><!--</a></td>-->
<!--														<td><img src="--><?//=$PATH_WRT_ROOT?><!----><?//=$image_path?><!--/--><?//=$LAYOUT_SKIN?><!--/10x10.gif" width="5"></td>-->
<!--														<td nowrap><a href="javascript:checkRejectNoDel(document.form1,'record_id[]','student_ole_status_update2.php', 3)" class="tabletool"><img src="--><?//=$PATH_WRT_ROOT?><!----><?//=$image_path?><!--/--><?//=$LAYOUT_SKIN?><!--/icon_reject.gif" border="0" align="absmiddle">--><?//=$button_reject?><!--</a></td>																													-->
<!--														<td><img src="--><?//=$PATH_WRT_ROOT?><!----><?//=$image_path?><!--/--><?//=$LAYOUT_SKIN?><!--/management/10x10.gif" width="5"></td>-->
<!--														<td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'record_id[]','ole_edit.php',0)" class="tabletool"><img src="--><?//=$PATH_WRT_ROOT?><!----><?//=$image_path?><!--/--><?//=$LAYOUT_SKIN?><!--/management/icon_edit.gif" name="imgEdit" align="absmiddle" border="0" >--><?//=$ec_iPortfolio['edit'] ?><!--</a></td>-->
<!--														<td><img src="--><?//=$PATH_WRT_ROOT?><!----><?//=$image_path?><!--/--><?//=$LAYOUT_SKIN?><!--/management/10x10.gif" width="5"></td>-->
<!--														<td nowrap="nowrap"><a href="javascript:checkRemove(document.form1,'record_id[]','ole_student_remove.php')" class="tabletool"><img src="--><?//=$PATH_WRT_ROOT?><!----><?//=$image_path?><!--/--><?//=$LAYOUT_SKIN?><!--/management/icon_delete.gif"  name="imgDelete" align="absmiddle" border="0" >--><?//=$ec_iPortfolio['delete'] ?><!-- </a></td>-->
<!--													</tr>-->
<!--												</tbody>-->
<!--											</table>-->
<!--										</td>-->
<!--										<td width="6"><img src="--><?//=$PATH_WRT_ROOT?><!----><?//=$image_path?><!--/--><?//=$LAYOUT_SKIN?><!--/management/table_tool_03.gif" height="23" width="6"></td>-->
<!--									</tr>-->
<!--								</tbody>-->
<!--							</table>-->
						<? } ?>					
					</td>
				</tr>
			</table>	
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_b">
				<tr>
	        	 	<td align="center" valign="middle">
	          			<table width="100%" border="0" cellpadding="0" cellspacing="0">
	          				<tr>
	          					<td>
					          		<?php 
					          			$LibTable->display() ;
					          		?>
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
										<?php if ($LibTable->navigationHTML!="") { ?>
											<tr class='tablebottom'>
												<td  class="tabletext" align="right"><?=$LibTable->navigationHTML?></td>
											</tr>
										<?php } ?>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>			
		</td>
	</tr>
</table>			
			
		
		<!-- End of CONTENT -->

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?= $order ?>">
<input type="hidden" name="field" value="<?= $field ?>">
<input type="hidden" name="numPerPage" value="<?= $numPerPage ?>">
<input type="hidden" name="StudentID" value="<?=$StudentID?>" />
<input type="hidden" name="approve" />
<input type="hidden" name="page_size_change" />
<input type=hidden name="IntExt" value="<?=$IntExt?>" />
<input type="hidden" name="IsExport" value="0" />

</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();


////start TEST
//function getProgramCanBeApproval($teacherID, $studentID,$programIntExtType,$allowClassTeacherApprove){
//	global $eclass_db;
//	$lpf_mgmt_grp = new libpf_mgmt_group();
//	$LibPortfolio = new libpf_slp();
//	$libdb =  new libdb();
//	//CHECK user in which group
//	$ec_uID = $LibPortfolio->IP_USER_ID_TO_EC_USER_ID($teacherID);
//	$group_id_arr = $lpf_mgmt_grp->GET_USER_IN_GROUP($ec_uID);
//
//	//get USER's OLE component
//	$mgmt_ele_arr = $lpf_mgmt_grp->getGroupComponent($group_id_arr);
//	
//	$_com_progamList = array();
//	$com_programListStr = "";
//	if(!empty($group_id_arr))
//	{
//		$conds = "";
//		for($i=0, $i_max=count($mgmt_ele_arr); $i<$i_max; $i++)
//		{
//			$mgmt_ele = $mgmt_ele_arr[$i];
//		
//			$conds .= "OR INSTR(op.ELE, '{$mgmt_ele}') > 0 ";
//		}
//		
//
//		$sql = "SELECT DISTINCT op.ProgramID FROM {$eclass_db}.OLE_PROGRAM op ";
//		$sql .= "WHERE 0 {$conds}";
////error_log("f: ".$sql."\n", 3, "/tmp/aaa.txt");
////debug_r(" component ===>".$sql);	
//		$_com_progamList = $libdb->returnVector($sql);	
//		
//		if(sizeof($_com_progamList) > 0 && is_array($_com_progamList)){
//			$com_programListStr = implode(",",$_com_progamList);
//			$com_programListStr = " or op.ProgramID in ({$com_programListStr}) ";
//		}
//	}
//
//	if($allowClassTeacherApprove)
//	{
//		$sql = "";
//		$sql .= "SELECT distinct os.UserId as 'userid' FROM {$eclass_db}.OLE_PROGRAM op ";
//		$sql .= "INNER JOIN {$eclass_db}.OLE_STUDENT os ON op.ProgramID = os.ProgramID ";
//		$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON os.UserID = ycu.UserID ";
//		$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_TEACHER yct ON ycu.YearClassID = yct.YearClassID ";
//		$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON yc.YearClassID = yct.YearClassID AND yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." ";
//		$sql .= "WHERE yct.UserID = {$teacherID} ";
//	//echo "class ".$sql."<Br/>";
////error_log("f: ".$sql."\n", 3, "/tmp/aaa.txt");
//		$classStudentRS = $libdb->returnVector($sql);
//	
//		$classStudentStr = "";
//		$classStudentSql = "";
//		if(is_array($classStudentRS) && sizeof($classStudentRS) > 0 ){
//			$classStudentStr = implode(",",$classStudentRS);
//		}
//		if($classStudentStr  != ""){
//			// $classStudentStr has value
//			$classStudentSql = " or os.Userid in({$classStudentStr}) ";
//		}
//	}
//
//	$sql = "Select distinct op.ProgramID
//				from {$eclass_db}.OLE_STUDENT as os
//				inner join {$eclass_db}.OLE_PROGRAM as op on os.Programid = op.programid
//				where os.USERID = {$studentID}
//				and 
//				(
//					os.RequestApprovedBy = {$teacherID}
//						or 
//					os.ApprovedBy = {$teacherID}
//						or
//					op.CreatorID = {$teacherID}
//					{$classStudentSql}
//					{$com_programListStr}
//				)
//				and op.IntExt = '{$programIntExtType}'
//		   ";
////error_log("f: ".$sql."\n", 3, "/tmp/aaa.txt");
//	$programRS = $libdb->returnVector($sql);
////echo "function sql ".$sql."<br/>";
//	$programListStr = "";
//	if(is_array($programRS) && sizeof($programRS) > 0){
//		$programListStr = implode(",",$programRS);
//	}
//	return $programListStr;
//}
////END test

}
?>