<?php
// Modifing by 
/*
 * 	2018-01-30  Anna
 * 	-  added Input by info 
 * 
 *  2016-07-15 Henry HM
 *  - Replaced all split() with explode() for PHP 5.4
 * 
 * 	2015-05-06 Bill
 * 	Fixed: get attachment content from array
 * 
 * 	2015-01-21 Bill
 * 	- Update Details of OLE_STUDENT using input "Details / Name of Activities"
 * 
 *  2015-01-13 Bill
 * 	- Update SAS Category and SAS Credit Point for Ng Wah SAS Cust
 * 
 * 	2011-03-16 Ivan
 * 	- Fixed: Failed to edit info if the student has more than 1 records in OLE_STUDENT for the same program
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");

intranet_auth();
intranet_opendb();

$fromPage = trim($fromPage);
$fromAction = trim($fromAction);

$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$LibPortfolio->ACCESS_CONTROL("ole");

if($SubmitType == "")
{
	if($IntExt == 1)
	{
		$SubmitType = "EXT";
	}
	else
	{
		$SubmitType = "INT";
	}
}

$fs = new libfilesystem();

// $UserID is a sesssion variable
$setApprovalPerson = $UserID;
$studentRecordComeFrom = $ipf_cfg["OLE_STUDENT_COMEFROM"]["teacherInput"];

$data = $LibPortfolio->RETURN_OLE_PROGRAM_BY_PROGRAMID($EventID);
list($startdate, $enddate, $title, $category, $ele, $organization, $details, $remark, $input_date, $modified_date, $period) = $data;

for ($i=0;$i<count($SortStudentID);$i++)
{
	$TmpStudentID = $SortStudentID[$i];	
	$TmpRole = $Role[$i];	
	$TmpAchievement = $Achievement[$i];
	$TmpHours = $Hours[$i];
	$TmpComment = $TeacherComment[$i];
	$TmpStatus = $Status[$i];	
	$ApprovalPerson = ($TmpStatus != 1) ? $setApprovalPerson: "";
	// Added: 2015-01-12
	$TmpSASPoint = $SASPoint[$i];
	$TmpSASPoint = ($TmpSASPoint == '')? "NULL" : "'".$TmpSASPoint."'"; 
	// Added: 2015-01-21 - Default details of program
	$TmpDetails = $details;
	if($sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme']){
		$TmpDetails = $StudentDetails[$i];
	}
	
	$TmpRecordID = $SortOleStudentRecordID[$i];
	if ($TmpRecordID == '') {
		$TmpRecordID = $LibPortfolio->RETURN_RECORDID_BY_EVENT_STUDENT($EventID,$TmpStudentID);
	}
	
	if($TmpRecordID=="")
	{
		if($sys_custom['NgWah_SAS']){
			$SAS_field = ", SASPoint";
			$SAS_field_value = ", $TmpSASPoint";
		}
		
		$fields = "(UserID, Title, Category, ELE, Role, Hours, Achievement, Organization";
		$fields .= ", ApprovedBy";
		$fields .= ", Details";
		$fields .= ", RecordStatus, StartDate, EndDate, ProcessDate, InputDate,InputBy, ModifiedDate, TeacherComment, ProgramID, IntExt, ComeFrom ".$SAS_field.")";

		$values = "('".$TmpStudentID."', '".addslashes($title)."', '".$category."', '".$ele."', '".$TmpRole."', '".$TmpHours."', '".$TmpAchievement."', '".addslashes($organization)."'";
		$values .= ", '".$ApprovalPerson."'";
		$values .= ", '".addslashes($TmpDetails)."'";
		$RecorStatus = ($ApprovalSetting==2) ? 2 : 1;
		$values .= ", '{$TmpStatus}', '".$startdate."', '".$enddate."', now(), now(),'".$_SESSION['UserID']."', now(), '".$TmpComment."', '".$EventID."', '".$SubmitType."', ".$studentRecordComeFrom." ".$SAS_field_value.")";
	
		$sql = "INSERT INTO {$eclass_db}.OLE_STUDENT $fields VALUES $values";
		
		$LibPortfolio->db_db_query($sql);
		$TmpRecordID = $LibPortfolio->db_insert_id();
		$msg = 1;
	} 
	else
	{
		
//		$TmpApprovalPerson = $ApprovedBy[$i];
		
		$fields_values = "Title = '".addslashes($title)."', ";
		$fields_values .= "Category = '$category', ";
		$fields_values .= "ELE = '$ele', ";
		$fields_values .= "Role = '$TmpRole', ";
		$fields_values .= "Hours = '$TmpHours', ";
		$fields_values .= "Achievement = '$TmpAchievement', ";
		$fields_values .= "Organization = '".addslashes($organization)."', ";

		//1) Suppose ApprovedBy  cannot be changed / updated
		//2) If change the ApprovedBy, it may affect the access right to view  OLE Record (since show the ole program may related to approved by)
//		$fields_values .= "ApprovedBy = '$ApprovalPerson', ";

		$fields_values .= "Details = '".addslashes($TmpDetails)."', ";
		$fields_values .= "TeacherComment = '$TmpComment', ";
		// Before record status update
		$fields_values .= "ApprovedBy = IF(IFNULL(ApprovedBy, 0) = 0 OR RecordStatus <> '{$TmpStatus}', '{$setApprovalPerson}', ApprovedBy), ";
		$fields_values .= "RecordStatus = '$TmpStatus', ";
		$fields_values .= "StartDate = '$startdate', ";
		$fields_values .= "EndDate = '$enddate', ";
		$fields_values .= "IntExt = '$SubmitType', ";
//		$fields_values .= "ApprovedBy = '$TmpApprovalPerson', ";
		//$fields_values .= ($TmpStatus == 1) ? "ProcessDate = NULL, " : "ProcessDate = now(), ";
		
		// Added: 2015-01-12
		if($sys_custom['NgWah_SAS']){
			$fields_values .= "SASPoint = $TmpSASPoint, ";
		}
		
		$fields_values .= "ProcessDate = now(), ";
		$fields_values .= "ModifiedDate = now()";
//		$sql = "UPDATE {$eclass_db}.OLE_STUDENT SET $fields_values WHERE RecordID = '$TmpRecordID' AND UserID='$TmpStudentID' AND RecordStatus=1 ";
		# Any record can be modified
		$sql = "UPDATE {$eclass_db}.OLE_STUDENT SET $fields_values WHERE RecordID = '$TmpRecordID' AND UserID='$TmpStudentID' ";
		$LibPortfolio->db_db_query($sql);
		$msg = 2;
	} // end if has record id

		###########################################################################
		############ Update the attachment if exist ###############################
		###########################################################################
		// [2015-0324-1420-39164] get attachment from $_POST array
//		$UserAttachment = ${"UserAttachment_".$TmpStudentID};
//		$UserRemoveAttachment = ${"UserRemoveAttachment_".$TmpStudentID};
		$currentUserAttachment = $UserAttachment[$i];
		$currentUserRemoveAttachment = $UserRemoveAttachment[$i];
		
		$AttachmentArr = array();
		$RemoveAttachmentArr = array();
		
		if($currentUserAttachment != "")
		$AttachmentArr = explode(":", stripslashes($currentUserAttachment));
		
		if($currentUserRemoveAttachment != "")
		$RemoveAttachmentArr = explode(":", stripslashes($currentUserRemoveAttachment));
		
		$attachments = "";
		$file_current = array();
		$is_need = array();

		for($k = 0; $k < count($AttachmentArr); $k++)
		{
			$file_current[$k] = $AttachmentArr[$k];
			$is_need[$k] = 1;
		}
		for($t = $k; $t < $k + count($RemoveAttachmentArr); $t++)
		{
			$file_current[$t] = $RemoveAttachmentArr[$t - $k];
			$is_need[$t] = 0;
		}
		
		$attachment_size_current = count($AttachmentArr) + count($RemoveAttachmentArr);
		$attachment_size = count($AttachmentArr);
		
		$is_attachment = false;
		
		if ($attachment_size > 0)
		{
			$is_attachment = true;
		}
	
		// Update attachment
		// use previous session_id if exists
		if ($attachment_size_current>0)
		{
			$tmp_arr = explode("/", $file_current[0]);
			
			$SessionID = trim($tmp_arr[0]);
		}
		if ($SessionID=="")
		{
			$SessionID = session_id();
		}
		$folder_prefix = $eclass_root."/files/portfolio/ole/r".$TmpRecordID."/".$SessionID;
		
		# remove unwanted files
		for ($a=0; $a<$attachment_size_current; $a++)
		{
			$attach_file = $file_current[$a];
			
			//debug_r("A: ".$attach_file);
			
			if ($is_need[$a])
			{	
				$attachments .= (($attachments=="")?"":":") . $attach_file;
				$fileArr[] = $attach_file;
			} 
			else
			{
				// remove the corresponding file
				if(file_exists($folder_prefix."/".stripslashes(get_file_basename($attach_file))))
				{
					//debug_r("remove: ".$folder_prefix."/".stripslashes(basename($attach_file)));
					$fs->file_remove($folder_prefix."/".stripslashes(get_file_basename($attach_file)));
				}
				
			}
		}
	
		if($is_attachment)
		{
			$fs->folder_new($eclass_root."/files/portfolio/ole/r".$TmpRecordID);
			$fs->folder_new($folder_prefix);

			# copy the files from tmp folder
			for ($r=0; $r < $attachment_size; $r++)
			{
				$loc_folder_prefix = $eclass_root."/files/portfolio/ole/tmp/t_".$UserID."/s_".$TmpStudentID;
				//$loc = ${"ole_file".($i)};
				//$name_hidden = ${"ole_file".$i."_hidden"};
				//$filename = (trim($name_hidden)!="") ? $name_hidden : ${"ole_file".($i)."_name"};
				
				$filename = $AttachmentArr[$r];
				$loc = $loc_folder_prefix."/".$filename;
				
				//debug_r("Copy From: ".$loc);
				
				if ($loc!="none" && file_exists($loc))
				{
					if(strpos($filename, ".")==0)
					{
						// Fail
						$isOk = false;
					} 
					else
					{ 
						// Success
						//debug_r("Copy To: ".stripslashes($folder_prefix."/".basename($filename)));
						$fs->file_copy($loc, stripslashes($folder_prefix."/".get_file_basename($filename)));
						$tmp_attach = $filename;
						$exist_flag=0;
						for($j=0; $j<sizeof($fileArr); $j++)
						{
							if($tmp_attach==$fileArr[$j])
							{
								$exist_flag = 1;
								break;
							}
						}
						if($exist_flag!=1)
						{
							$attachments .= (($attachments=="")?"":":") . $tmp_attach;
							$fileArr[] = $tmp_attach;
						}
					}
				} // end if file exist
			}
		}
	
		$ole_file_sql = (trim($attachments)=="") ? "NULL" : "'$attachments'";
		$sql = "UPDATE
				{$eclass_db}.OLE_STUDENT
			SET
				Attachment=$ole_file_sql
			WHERE 
				RecordID = '$TmpRecordID'
			";
		
		$LibPortfolio->db_db_query($sql);

		###########################################################################
		############ END Update the attachment if exist ###########################
		###########################################################################
} // end for


intranet_closedb();

//header("Location: ole_event_studentview.php?msg=$msg&EventID=$EventID&Year=$Year&FromPage=$FromPage&IntExt=$IntExt");

$redirectURL = "ole_event_studentview.php?msg={$msg}&EventID={$EventID}&Year={$Year}&FromPage={$FromPage}&IntExt={$IntExt}";

switch($fromAction){
	case "approveStudentRecord":
		$redirectURL = "/home/portfolio/teacher/management/oleIndex.php?msg={$msg}";
	break;
}
header("Location: ".$redirectURL);
exit();
?>