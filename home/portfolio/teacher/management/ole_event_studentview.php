<?php
## Modifying by: anna
##########################################################
## Modification Log
##	2017-07-05 Anna
##	- added attachemnt upload 
##
##	2016-09-20 Omas
##	- modified add $isReadOnly condition to edit button
##
##  2016-05-27 Henry HM
##  - Added field of "Default Hours" - #U95004
##
##	2015-01-15 Omas
##	- added $sys_custom['iPf']['displayIneRC'] feature - #U77580
##
##	2015-06-10 Omas
##	- Add If user is not the program creator cannot assign / delete #E78120
##	- change also applied to ole_event_studentview.tmpl.php 
##
##	2015-05-12 Bill		[2015-0324-1420-39164]
##	- Add Field "Teacher-in-charge"
##	- Hide "Allow Participation Reporting", "Map to OEA", "Partner Organizations", "Details / Name of Activities", "Student Remark"
##
##	2015-05-11 Bill		[Defect List #2866]
##	- ensure fields display "--" if value is empty
##
##	2015-01-21 Bill
##	for St. Paul PAS Cust enhancement [Case #F73433]
##	- Program details - "Details / Name of Activities" show [Refer to the students records below]
##	- Student record - add column Details / Name of Activities
##
##	2015-01-12 Omas	
##	Add edit button
##
##	2014-03-25 Ryan
##	Add SIS hide columns cust 

##	2013-05-29	Yuen
##	support to export member list to CSV [Case#2013-0110-1235-02054]

## 2011-03-15 Ivan
## - Hide OLE Component Info Display for External Programs

## 2011-01-14 Thomas
## - Changed $orderby_classno

## 2010-04-30: Fai 
## - Enhancement : support teacher to see the OLE student record with Multiple class teacher

## 2010-04-21: Max (201004211152)
## - Changed comparison of ApprovalSetting from plaintext number to variable in iPortfolioConfig.inc

## 2010-02-23: Max (201002181147)
## - Customize for CNECC

## 2010-01-20: Max (201001201158)  (Fai ??)
## - Remove the link for Assign Student when the program canJoin = false (fai : teacher can assign Student although canJoin = false)

## 2009-12-30: Max (200912281012)
## - add display of sub category
## 
## 2009-12-08: Max (200912081532)
## -Fix Case: retrieving data from function PROGRAM_BY_PROGRAMID_MAX

## 2009-12-14: Fai 
## - add sql condition "$intExt_cond" , otherwise it will display both EXT and INT record
##
##########################################################
/**
 * Type			: Enhancement
 * Date 		: 200911131142
 * Description	: 1C) Enhance the form to New an OLE with chinese title and chinese details
 * C=CurrentIssue 2) The import page is required to support chinese title, chinese details and add school remarks
 * Case Number	: 200911131142MaxWong
 */
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-teacher.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-mgmt-group.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
intranet_auth();
intranet_opendb();

$action = $_SESSION["SESS_OLE_ACTION"];
$isReadOnly = ($action == $ipf_cfg["OLE_ACTION"]["view"]) ? 1 : 0 ;

$ldb = new libdb();
$lpf = new libpf_slp();
$lpf_ui = new libportfolio_ui();
$lpf_mgmt_grp = new libpf_mgmt_group();

$ec_uID = $lpf->IP_USER_ID_TO_EC_USER_ID($UserID);
$group_ids = $lpf_mgmt_grp->GET_USER_IN_GROUP($ec_uID);
	
if($IntExtType == "INT"){
	// for the component group , it support with INT only
	//get USER's OLE component
	$group_id_arr = $group_ids;
	$mgmt_ele_arr = $lpf_mgmt_grp->getGroupComponent($group_id_arr);
}

# Check if any group set - "batch manage self-added records only"
for($i=0; $i<count($group_ids); $i++){
	$lpf_mgmt_grp->setGroupID($group_ids[$i]);
	$lpf_mgmt_grp->setGroupProperty();
	$lpf_mgmt_grp->setGroupFunctionRight();
	
	# Get the fucntion settings
	$gf_arrTemp = $lpf_mgmt_grp->getGroupFunctionRight();
	
	# bmsa (Profile:DelSelfAccountOnly) => change to short form 
	# due to DB field "function_right varchar(255)" character size limitation
	$manageSelfAcc = in_array("bmsa", $gf_arrTemp);
	if($manageSelfAcc){
		break;
	}
}

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$lpf->ACCESS_CONTROL("ole");


/****************************************************************************/

// >>>>>>>>>> libconfig 
/* This shows which columns ARE ABLE TO HIDE, */
/* show/hide option of columns in here can setup in settings.php ['IPF_HIDE_COLUMN'][INT||EXT_STUDENT] per each school */
//$cfg_column_displayConfig['INT_STUDENT'] = array('OLE_ROLE','ACHIEVEMENTS','ATTACHMENT','COMMENT','STATUS','APPROVALDATE','APPROVER');
//$cfg_column_displayConfig['EXT_STUDENT'] = array('OLE_ROLE','ATTACHMENT','COMMENT','STATUS','APPROVALDATE','APPROVER');
// End of libconfig <<<<<<<<<<<<


/*
// >>>>>>>>>>> settings.php  
// cust value for disable student tab  
$sys_custom['IPF_HIDE_COLUMN']['SIS'] = true;
// boolean used for hidden column, [MOUDLE_ACTION][TAB][COLNAME]
$sys_custom['IPF_HIDE_COLUMN']['INT_STUDENT']['OLE_ROLE'] = true; 
$sys_custom['IPF_HIDE_COLUMN']['INT_STUDENT']['ACHIEVEMENTS'] = true;
$sys_custom['IPF_HIDE_COLUMN']['INT_STUDENT']['ATTACHMENT']= true; 
$sys_custom['IPF_HIDE_COLUMN']['INT_STUDENT']['COMMENT'] = true;
$sys_custom['IPF_HIDE_COLUMN']['INT_STUDENT']['STATUS'] = true;
$sys_custom['IPF_HIDE_COLUMN']['INT_STUDENT']['APPROVALDATE']= true; 
$sys_custom['IPF_HIDE_COLUMN']['INT_STUDENT']['APPROVER'] = true;


$sys_custom['IPF_HIDE_COLUMN']['EXT_STUDENT']['OLE_ROLE'] = true; 
$sys_custom['IPF_HIDE_COLUMN']['EXT_STUDENT']['ATTACHMENT']= true; 
$sys_custom['IPF_HIDE_COLUMN']['EXT_STUDENT']['COMMENT'] = true;
$sys_custom['IPF_HIDE_COLUMN']['EXT_STUDENT']['STATUS'] = true;
$sys_custom['IPF_HIDE_COLUMN']['EXT_STUDENT']['APPROVALDATE']= true; 
$sys_custom['IPF_HIDE_COLUMN']['EXT_STUDENT']['APPROVER'] = true;


// boolean used for hidden row for view_tmpl 
$sys_custom['IPF_HIDE_COLUMN']['INT_PROGRAM']['SUBMISSION_TYPE'] =true;
$sys_custom['IPF_HIDE_COLUMN']['INT_PROGRAM']['DATE_PERIOD'] =true;
$sys_custom['IPF_HIDE_COLUMN']['INT_PROGRAM']['SUB_CATEGORY'] =true;
$sys_custom['IPF_HIDE_COLUMN']['INT_PROGRAM']['CREATEDBY'] =true;

$sys_custom['IPF_HIDE_COLUMN']['EXT_PROGRAM']['SUBMISSION_TYPE'] =true;
$sys_custom['IPF_HIDE_COLUMN']['EXT_PROGRAM']['DATE_PERIOD'] =true;
$sys_custom['IPF_HIDE_COLUMN']['EXT_PROGRAM']['SUB_CATEGORY'] =true;
$sys_custom['IPF_HIDE_COLUMN']['EXT_PROGRAM']['ORGANIZATION'] =true;
$sys_custom['IPF_HIDE_COLUMN']['EXT_PROGRAM']['CREATEDBY'] =true;
$sys_custom['IPF_HIDE_COLUMN']['EXT_PROGRAM']['LASTUPDATED']=true;

// End of settings.php <<<<<<<<<<<<<<<<<
****************************************************************************/



########################################################
# HIDE COL FOR SIS 20140319 TEMPLATE SETTINGS 
########################################################

// set current tab name for sys_custom 
/* Please do not change the Value 
 * Unless you have understood how hide column works as well as INT/EXT means */
switch($IntExt){
	case 1 : $TabName = 'EXT_STUDENT';
			 $TmplName = 'EXT_PROGRAM';
				break;
	default : $TabName = 'INT_STUDENT'; 
			  $TmplName = 'INT_PROGRAM';
				break;
}

// >>>>>>>>>>>> Get Settings from settings.php
 
// Getting hidden column settings from settings.php $sys_custom['IPF_HIDE_COLUMN']['TAB']
// Set how many columns should be hidden
$IPF_HIDE_COLUMN_TMPL = array();
if(isset($sys_custom['IPF_HIDE_COLUMN'][$TmplName])){
	foreach($sys_custom['IPF_HIDE_COLUMN'][$TmplName] as $colName=>$hide){
		$IPF_HIDE_COLUMN_TMPL[$colName] = $hide;
	}
	
	$IPF_HIDE_COLUMN = array();
	foreach($sys_custom['IPF_HIDE_COLUMN'][$TabName] as $colName=>$hide){
		$IPF_HIDE_COLUMN[$colName] = $hide;
	}
}
// END of Get Settings from settings.php <<<<<<<<<<<<<<<<<<<<<<<<

 
//>>>>>>>>>>>>>>>>>  SQL Statement Settings 

// This is SQL select statment for each field, 
// add select field here if there is column added in the future 
/*  PLEASE NAMES THE ARRAY KEYS SAME AS settings.php $sys_custom['IPF_HIDE_COLUMN']['$TAB']  */ 
$sqlSelectfield = array();
$sqlSelectfield['OLE_ROLE'] = 'a.Role,';
$sqlSelectfield['ACHIEVEMENTS'] = 'a.Achievement,';
$sqlSelectfield['ATTACHMENT'] = "IF ((a.Attachment!='' AND a.Attachment IS NOT NULL), CONCAT('<a href=\"attach.php?RecordID=', a.RecordID, '\" target=\"_blank\"><img src=\"$image_path/icon/attachment.gif\" border=0></a>'), '&nbsp;'),";
$sqlSelectfield['COMMENT'] = 'a.TeacherComment,';
$sqlSelectfield['STATUS'] = "  CASE a.RecordStatus
                WHEN 1 THEN '<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title=\"{$ec_iPortfolio['pending']}\">'
                WHEN 2 THEN '<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif\" title=\"{$ec_iPortfolio['approved']}\">'
                WHEN 3 THEN '<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif\" title=\"{$ec_iPortfolio['rejected']}\">'
                WHEN 4 THEN '<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_teacher_input.gif\" title=\"{$ec_iPortfolio['teacher_submit_record']}\">'
                ELSE '--' END
              as RecordStatus,";
$sqlSelectfield['APPROVALDATE'] = "IFNULL(DATE_FORMAT(a.ProcessDate,'%Y-%m-%d'),'--') AS ProcessDate,";
if($IntExt == 1){
	$sqlSelectfield['APPROVER'] = "IF(a.RecordStatus = 1 OR IFNULL(".getNameFieldByLang("d.").", '') = '', '--', ".getNameFieldByLang("d.").") AS Processor,";
}else {
	$sqlSelectfield['APPROVER'] = "IF(IFNULL(".getNameFieldByLang("d.").", '') = '', '--', ".getNameFieldByLang("d.").") AS Processor,";
}

// Added: 2015-01-21 - for St. Paul PAS cust - get student details
if($sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme']){
	$sqlSelectfield['STUDENT_DETAILS'] = "IF(a.Details<>'' AND a.Details is not null, a.Details, c.Details) AS Details,";
}

// End of SQL Statement Settings <<<<<<<<<<<<<<<<<<<<<

if(isset($sys_custom['IPF_HIDE_COLUMN'][$TmplName])){
	// >>>>>>>>>>>>>>> Get SQL select Statement Condition
	
	// empty sql select field if that column is hidden  
	$iMax = count($cfg_column_displayConfig[$TabName]);
	foreach ($IPF_HIDE_COLUMN as $colName=>$hideItem){
		for($i=0;$i<$iMax;$i++){
			if($colName == $cfg_column_displayConfig[$TabName][$i]){
					$sqlSelectfield[$colName] = '';
			}
		}
	}
	// End Of Get SQL select Statement Condition <<<<<<<<<<<<<
	
	
	//>>>>>>>>>>> Count hidden columns
	
	// $countHide = count($IPF_HIDE_COLUMN); 
	$countHide = 0;
	foreach($IPF_HIDE_COLUMN as $colName=>$hideItem){
		if($hideItem){
			$countHide ++;
		} 
	}
	// End of Count hidden columns <<<<<<<<<<<<<
}

//INIT variable
// In this page, admin and group teacher has group right
$isAdmin = (strstr($ck_function_rights, "Profile:OLR")) ? 1 : 0 ;

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = ($isReadOnly) ? "Teacher_OLEView" : "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");
########################################################
# Tab Menu : Start
########################################################

$TabIndex = empty($IntExt) ? IPF_CFG_SLP_MGMT_OLE : IPF_CFG_SLP_MGMT_OUTSIDE;

$TabMenuArr = libpf_tabmenu::getSlpMgmtTabs($TabIndex, 0);
### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags($TabIndex);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

########################################################
# Page Config : Start
########################################################
# tab menu
# hide this if SIS
if(!$sys_custom['IPF_HIDE_COLUMN']['SIS'] ) {
	$tab_menu_html = $lpf_ui->GET_TAB_MENU($TabMenuArr);
}

$currentAcademicYear = Get_Current_Academic_Year_ID();
//$displayEditFunctions = (!$isReadOnly && strstr($ck_function_rights, "Profile:OLR")) ? true : false;
$displayEditFunctions = (!$isReadOnly) ? true : false;
if ($page_size_change == 1)
{
  setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
  $ck_page_size = $numPerPage;
}

# !strstr($ck_function_rights, "Profile:OLR") : non-admin
# strstr($ck_user_rights_ext, "ole:form_t") : OLE viewable by class teacher only
$viewAllClass = (strstr($ck_function_rights, "Profile:OLR") || !strstr($ck_user_rights_ext, "ole:form_t"));

// [2015-0324-1420-39164] check for St. Paul CUST display 
$StPaulField = $sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme'] && $IntExt != 1;

########################################################
# Page Config : End
########################################################

########################################################
# Operations : Start
########################################################
# class selection
if(!$viewAllClass)
{
  $lpf_acc = new libpf_account_teacher();
  $lpf_acc->SET_CLASS_VARIABLE("user_id", $UserID);
  $class_teach_arr = $lpf_acc->GET_CLASS_TEACHER_CLASS();
}

$class_name = $i_general_WholeSchool;
$lpf_fc = new libpf_formclass();
$t_classlevel_arr = $lpf_fc->GET_CLASSLEVEL_LIST();
for($i=0; $i<count($t_classlevel_arr); $i++)
{
  $t_classlevel_id = $t_classlevel_arr[$i]["YearID"];
  $t_classlevel_name = $t_classlevel_arr[$i]["YearName"];

  $lpf_fc->SET_CLASS_VARIABLE("YearID", $t_classlevel_id);
  $t_class_arr = $lpf_fc->GET_CLASS_LIST();
  
  for($j=0; $j<count($t_class_arr); $j++)
  {
    $t_yc_id = $t_class_arr[$j][0];
    $t_yc_title = Get_Lang_Selection($t_class_arr[$j]['ClassTitleB5'], $t_class_arr[$j]['ClassTitleEN']);
    if(!is_array($class_teach_arr) || in_array($t_yc_id, $class_teach_arr))
    {
      $class_arr[$t_classlevel_name][] = array($t_yc_id, $t_yc_title);
    }
    
    if($t_yc_id == $YearClassID)
    {
      $class_name = $t_yc_title;
    }
  }
}

// Generate class selection with optgroup
$class_selection_html = $lpf_ui->GEN_CLASS_SELECTION_OPTGROUP($class_arr, "name='YearClassID' onChange='this.form.submit()'", $YearClassID);

// Status selection
$status_selection_html = "<SELECT name='status' onChange='this.form.submit()'>";
$status_selection_html .= "<OPTION value='' ".($status==0?"SELECTED":"").">".$ec_iPortfolio['all_status']."</OPTION>";
$status_selection_html .= "<OPTION value='1' ".($status==1?"SELECTED":"").">".$ec_iPortfolio['pending']."</OPTION>";
$status_selection_html .= "<OPTION value='2' ".($status==2?"SELECTED":"").">".$ec_iPortfolio['approved']."</OPTION>";
$status_selection_html .= "<OPTION value='3' ".($status==3?"SELECTED":"").">".$ec_iPortfolio['rejected']."</OPTION>";
$status_selection_html .= "<OPTION value='4' ".($status==4?"SELECTED":"").">".$ec_iPortfolio['teacher_submit_record']."</OPTION>";
$status_selection_html .= "</SELECT>";

//$ole_assign_student_btn_html = (!$isReadOnly && strstr($ck_function_rights, "Profile:OLR")) ? "
// /$ole_assign_student_btn_html = (!$isReadOnly) ? $linterface->GET_LNK_EXPORT("ole_assign_student.php?EventID=".$EventID."&IntExt=".$IntExt,$Lang['iPortfolio']['OLE']['NewStudent'],"","","",0)  : "";
$ole_assign_student_btn_html= "<div class=\"Conntent_tool\"><a href=\"ole_assign_student.php?EventID=".$EventID."&IntExt=".$IntExt."\" class=\"contenttool\">{$Lang['iPortfolio']['OLE']['NewStudent']} </a></div>";

########################################################
# Operations : End
########################################################

// 2015-06-10 Commented by Omas Moved to lower part of the code
//########################################################
//# CNECC Customization : Start
//########################################################
//if($sys_custom['cnecc_SS'])
//{
//	include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cnecc/libpf-slp-cnecc.php");
//	$libpf_slp_cnecc = new libpf_slp_cnecc();
//	if ($libpf_slp_cnecc->isSunshineProgram($EventID))
//	{
//		$ole_assign_student_btn_html = "<span style='color:red;'>* ".$Lang["Cust_Cnecc"]["CannotAssignStudentsForSunshinePrograms"]."</span>";
//		$cnecc_customize_html = $libpf_slp_cnecc->getResultDisplayCustomization($EventID);
//		$displayEditFunctions = false;
//	} else {
//		// do nothing
//	}
//}
//########################################################
//# CNECC Customization : End
//########################################################
//
//// if SIS hide export 
//if(!$sys_custom['IPF_HIDE_COLUMN']['SIS']){
//	$ole_assign_student_btn_html .= $linterface->GET_LNK_EXPORT("javascript:goExport()",$button_export,"","","",0);
//}

########################################################
# Operation result : Start
########################################################
switch($msg)
{
  case 1:
    $msg = "add";
    break;
  case 2:
    $msg = "update";
    break;
  case 3:
    $msg = "delete";
    break;
  case 4:
  	$msg = "update_failed";
  	break;
  default:
    $msg = "";
    break;
}

$op_result = ($msg == "") ? "" : $linterface->GET_SYS_MSG($msg);

########################################################
# Operation result : End
########################################################

########################################################
# Program Basic : Start
########################################################
$data = $lpf->RETURN_OLE_PROGRAM_BY_PROGRAMID_MAX($EventID);
if (is_array($data)) {
  $startdate = $data['StartDate'];
  $enddate = $data['EndDate'];
  $eng_title_html = $data['Title'];
  //$chiTitle = $data['TitleChi'];
  $category = $data['Category'];
  $subCategoryID = $data['SubCategoryID'];
  $ele = $data['ELE'];
  $organization_html = ($data['Organization'] == "") ? "--" : $data['Organization'];
  $engDetails_html = ($data['Details'] == "") ? "--" : intranet_htmlspecialchars(nl2br($data['Details']));
  // Added: 2015-01-21 - for St. Paul PAS cust - not display program details
  $engDetails_html = $sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme']? $ec_iPortfolio['pas_program_details'] : $engDetails_html;
  //$chiDetails = $data['DetailsChi'];
  $remark_html = ($data['SchoolRemarks'] == "") ? "--" : nl2br($data['SchoolRemarks']);
  //$input_date = $data['InputDate'];
  $modified_date_html = $data['ModifiedDate'];
  $period_html = $data['Period'];
  //$int_ext = $data['IntExt'];
  $canJoin = $data['CanJoin'];
  $canJoinStartDate = $data['CanJoinStartDate'];
  $canJoinEndDate = $data['CanJoinEndDate'];
  $creator_name_html = $data['UserName'];
  $autoApprove = $data['AUTOAPPROVE'];
  $maximumHours = $data['MaximumHours'];
  $defaultHours = $data['DefaultHours'];
  $compulsoryFields = $data['CompulsoryFields'];
  $defaultApprover = $data['DefaultApprover'];
  $ModifiedUser_html = ($data['ModifiedUser'] == "") ? "" : " ({$data['ModifiedUser']})";
  $IsSAS = $data['IsSAS'];
  $IsOutsideSchool = $data['IsOutsideSchool'];
  $CreatorID = $data['CreatorID'];
}

### case #E78120
if($manageSelfAcc){
	if($CreatorID == $_SESSION['UserID']){

		$allow_Assign_Delete = 1;
	}
	else if($lpf->IS_IPF_SUPERADMIN()){
		$allow_Assign_Delete = 1;
	}
	else{
		$allow_Assign_Delete = 0;
	}
}
else{
	$allow_Assign_Delete = 1;
}

if(!$allow_Assign_Delete){
	$ole_assign_student_btn_html = '';	
}

// 2015-06-10 Omas moved to here 
########################################################
# CNECC Customization : Start
########################################################
if($sys_custom['cnecc_SS'])
{
	include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cnecc/libpf-slp-cnecc.php");
	$libpf_slp_cnecc = new libpf_slp_cnecc();
	if ($libpf_slp_cnecc->isSunshineProgram($EventID))
	{
		$ole_assign_student_btn_html = "<span style='color:red;'>* ".$Lang["Cust_Cnecc"]["CannotAssignStudentsForSunshinePrograms"]."</span>";
		$cnecc_customize_html = $libpf_slp_cnecc->getResultDisplayCustomization($EventID);
		$displayEditFunctions = false;
	} else {
		// do nothing
	}
}
########################################################
# CNECC Customization : End
########################################################

// if SIS hide export 
if(!$sys_custom['IPF_HIDE_COLUMN']['SIS']){
	$ole_assign_student_btn_html .= $linterface->GET_LNK_EXPORT("javascript:goExport()",$button_export,"","","",0);
}
$ole_assign_student_btn_html .= $linterface->GET_LNK_UPLOAD("ole_student_attachment_upload.php?EventID=$EventID",'',$Lang['iPortfolio']['OLE']['UploadButton']);

$IsSASText = ($IsSAS) ? $ec_iPortfolio['SLP']['Yes'] : $ec_iPortfolio['SLP']['No'];
$IsOutsideSchoolText = ($IsOutsideSchool) ? $Lang['iPortfolio']['OLE']['Outside_School'] : $Lang['iPortfolio']['OLE']['Inside_School'];

$Cats = $lpf->GET_OLR_Category(0, true);
$SubCats = $lpf->GET_OLE_SUBCATEGORY($category);

### default Approver ###
$defaultApprover_html = $lpf->GET_TEACHER_NAME($defaultApprover);
// [Defect List #2866] display "--" if empty
$defaultApprover_html = (trim($defaultApprover_html) == "")? "--" : $defaultApprover_html; 

$submit_type_html = ($IntExt == 1) ? $iPort["external_record"] : $iPort["internal_record"];

if($startdate=="" || $startdate=="0000-00-00") { $date_html = "--"; }
else if($enddate!="" && $enddate != "0000-00-00") { $date_html = $startdate."&nbsp;".$profiles_to."&nbsp;".$enddate; }
else { $date_html = $startdate; }

$category_html = $Cats[$category];
$sub_category_html = ($SubCats[$subCategoryID] == "") ? "--" : $SubCats[$subCategoryID];

# Component display
if($ele!="")
{
	# get the ELE code array
	$DefaultELEArray = $lpf->GET_ELE();
	$ELEArr = explode(",", $ele);
	for($i=0; $i<sizeof($ELEArr); $i++)
	{
		$t_ele = trim($ELEArr[$i]);
		$component_html .= "- ".$DefaultELEArray[$t_ele]."<br />";
	}
}
else
{
  $component_html = "--";
}
$h_ShowOleComponent = ($IntExt == 1)? false : true;		// hide the OLE Component Info for External Program
$h_ShowAllowStudentJoin = ($IntExt == 1)? false : true;		// hide the OLE Component Info for External Program

$joinPeriodDisplayStatus = 'none';
$canJoin_html = $ec_iPortfolio['SLP']['No'];
$canJoinPeriod_html = "--";
$autoApprove_html = $ec_iPortfolio['SLP']['No'];

if ($canJoin) {
  $joinPeriodDisplayStatus = '';
  $canJoinStartText = (isset($canJoinStartDate) && !empty($canJoinStartDate) && $canJoinStartDate != '0000-00-00') ? $canJoinStartDate : " -- ";
  $canJoinEndText = (isset($canJoinEndDate) && !empty($canJoinEndDate) && $canJoinEndDate != '0000-00-00') ? $canJoinEndDate : " -- ";

// [Defect List #2866] display "No" if empty
//$autoApprove_html = ($autoApprove) ? $ec_iPortfolio['SLP']['Yes'] : $autoApproveText;
  $autoApprove_html = ($autoApprove) ? $ec_iPortfolio['SLP']['Yes'] : $autoApprove_html;
  $canJoin_html = $ec_iPortfolio['SLP']['Yes'];
  $canJoinPeriod_html = $ec_iPortfolio['SLP']['From']." ".$canJoinStartText." " .$ec_iPortfolio['SLP']['to']." ".$canJoinEndText;
  $maximumHours_html = empty($maximumHours)?' -- ':$maximumHours;
  $defaultHours_html = empty($defaultHours)?' -- ':$defaultHours;
  
  if (!empty($compulsoryFields)) {
		$temp_codes = str_split($compulsoryFields);
		if (is_array($temp_codes)) {
			foreach($temp_codes as $key => $value) {
				$temp_lang[] = $ipf_cfg["OLE_PROGRAM_CompulsoryFields__Lang"][$value];
			}
		}
		$compulsoryFields_html = implode(",&nbsp;&nbsp;",$temp_lang);
	} else {
		$compulsoryFields_html = ' -- ';
	}
}

// [2015-0324-1420-39164] show Teacher-in-charge
if($sys_custom['iPortfolio_ole_record_tic']){
	$teacher_PIC = $lpf->GET_OLE_PROGRAM_TEACHER_IN_CHARGE_BY_RECORDID($EventID);
	
	$teacher_in_charge = "--";
	$delim = "";
	if(count($teacher_PIC) > 0){
		$teacher_in_charge = "";
		foreach($teacher_PIC as $pic){
			$teacher_in_charge .= $delim.$pic['UserName'];
			$delim = ", ";
		}
	}
}
########################################################
# Program Basic : End
########################################################

########################################################
# Student Record : Start
########################################################
$pageSizeChangeEnabled = true;
$checkmaster = true; 
//echo $field;
if ($order=="") $order=0;
if ($field=="") $field=2;
$LibTable = new libpf_dbtable($field, $order, $pageNo);


# This line is used to handle the classnumber ordering
//$orderby_classno = " concat(TRIM(SUBSTRING_INDEX(b.ClassNumber, '-', 1)), IF(INSTR(b.ClassNumber, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(b.ClassNumber, '-', -1)), 10, '0'))) ";
$orderby_classno = " concat(TRIM(SUBSTRING_INDEX(b.ClassName, '-', 1)), IF(INSTR(CAST(b.ClassNumber AS char), '-') != 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(CAST(b.ClassNumber AS char), '-', -1)), 10, '0'))) ";

if($IntExt == 1)
{
	# use getNameFieldByLang("b.") to sort it by username rather then the whole html link string, i.e. sort bu b.EnglishName
	$LibTable->field_array = array(getNameFieldByLang("b."), $orderby_classno, "a.Role","a.Achievement", "a.Remark", "RecordStatus", "ProcessDate", "Processor");
	$conds =  " AND a.IntExt ='EXT'";
}
else
{
	$LibTable->field_array = array(getNameFieldByLang("b."), $orderby_classno, "a.Role","a.Achievement", "a.Hours", "a.Remark", "RecordStatus", "ProcessDate", "Processor");
	$conds =  " AND a.IntExt ='INT'";
}

$conds .= ($status=="") ? "" : " AND a.RecordStatus = '$status'";
//$conds .= ($category=="") ? "" : " AND a.Category = '$category'";
//$conds .= ($ELE=="") ? "" : " AND a.ELE LIKE '%$ELE%'";
//$conds .= ($YearClassID=="") ? "" : " AND ycu.YearClassID = $YearClassID ";
if($YearClassID!="")
{
	$sql = "SELECT DISTINCT UserID FROM {$intranet_db}.YEAR_CLASS_USER WHERE YearClassID = '{$YearClassID}'";
	$uid_arr = $ldb->returnVector($sql);
	$conds .= empty($uid_arr) ? " AND false " : " AND b.UserID IN (".implode(", ", $uid_arr).") ";
}

$namefield = getNameFieldByLang("b.");
$classfield = "CONCAT(b.ClassName,'-', LPAD(b.ClassNumber, 2, '0'))";

// >>>>>>>>> // FOR SIS cust 

// disable hyperlink in SIS 
$defaultStudentName = "CONCAT('<a class=\"tablelink\" href=\"ole_student.php?IntExt={$IntExt}&StudentID=', a.UserID, '\">', $namefield, '</a>') as UserName,";
$defaultClass = "CONCAT('<a class=\"tablelink\" href=\"ole_student.php?IntExt={$IntExt}&StudentID=', a.UserID, '\">', $classfield, '</a>'),";

if($sys_custom['IPF_HIDE_COLUMN']['SIS']){
	$defaultStudentName = "CONCAT($namefield) as UserName,";
	$defaultClass = "CONCAT($classfield),";
}

$sqlStudentName = $defaultStudentName;
$sqlClass = $defaultClass;
// End of Cust for SIS <<<<<<<<<<<<<<<<<


// load approval settings
//$ApprovalSetting = $lpf->GET_OLR_APPROVAL_SETTING();
//if ($ApprovalSetting == $ipf_cfg["RECORD_APPROVAL_SETTING"]["onlyTeachersAdminsWithApprovalRight"])	//only class teachers and admin show checkbox
//{
//	//CHECK WHETHER THE LOGIN USER IS A CLASS TEACHER
//	//$sql = "SELECT ClassID FROM INTRANET_CLASSTEACHER WHERE UserID = '$UserID'";
//	$sql = "SELECT YearClassTeacherID FROM {$intranet_db}.YEAR_CLASS_TEACHER as yct INNER JOIN {$intranet_db}.YEAR_CLASS as yc on yct.YearClassID = yc.YearClassID and yc.AcademicYearID = '{$currentAcademicYear}' WHERE yct.UserID = '{$UserID}'";
//
//	$result = $ldb->returnVector($sql);
//	$isClassTeacher = (count($result) == 0) ? false : true;
//	$edit_field = (strstr($ck_function_rights, "Profile:OLR") || $isClassTeacher) ? " , CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>')" : ", if(c.CreatorID = ".$UserID.", CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>'), '&nbsp;')";
//}
//else if ($ApprovalSetting == $ipf_cfg["RECORD_APPROVAL_SETTING"]["studentSelectApprovalTeacher"])	//only the assigned teacher or admin show checkbox
//{
//  $edit_field = (strstr($ck_function_rights, "Profile:OLR")) ? " , CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>')" : " , if((a.ApprovedBy = ".$UserID." OR c.CreatorID = ".$UserID.") , CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>'), '&nbsp;')";
//}
//else if ($ApprovalSetting == $ipf_cfg["RECORD_APPROVAL_SETTING"]["noApprovalNeeded"])	//show checkbox to all
//{
//	$edit_field = " , CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>')";
//}

if (empty($IntExt)) {
	$INT_EXT_String = "INT";
} else {
	$INT_EXT_String = "EXT";
}
$approvalSettings = $lpf->GET_OLR_APPROVAL_SETTING_DATA2($INT_EXT_String);
if($approvalSettings['IsApprovalNeed'])
{
	// shows checkbox for admin and group teacher
	if($isAdmin)
	{
		//$edit_field = " , CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>')";
		$edit_field = "  CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>')";
	}
	else
	{
		$isClassTeacher = false;
		if ($approvalSettings['Elements']['ClassTeacher'])
		{
			//CHECK WHETHER THE LOGIN USER IS A CLASS TEACHER
			$sql = "SELECT YearClassTeacherID FROM {$intranet_db}.YEAR_CLASS_TEACHER as yct INNER JOIN {$intranet_db}.YEAR_CLASS as yc on yct.YearClassID = yc.YearClassID and yc.AcademicYearID = '{$currentAcademicYear}' WHERE yct.UserID = '{$UserID}'";
		
			$result = $ldb->returnVector($sql);
			$isClassTeacher = (count($result) == 0) ? false : true;
		}
	
		if($isClassTeacher)
		{
//			$edit_field = ", CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>')";
			$edit_field = " CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>')";
		}
		else
		{
			//the assigned teacher show checkbox
//			$edit_field = ", if((a.ApprovedBy = ".$UserID." OR a.RequestApprovedBy = ".$UserID." OR c.CreatorID = ".$UserID.") , CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>'), '&nbsp;')";
			$edit_field = " if((a.ApprovedBy = ".$UserID." OR a.RequestApprovedBy = ".$UserID." OR c.CreatorID = ".$UserID.") , CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>'), '&nbsp;')";
		}
	}

/*
	else if ($approvalSettings['Elements']['ClassTeacher'])	//only class teachers and admin show checkbox
	{
		//CHECK WHETHER THE LOGIN USER IS A CLASS TEACHER
		//$sql = "SELECT ClassID FROM INTRANET_CLASSTEACHER WHERE UserID = '$UserID'";
		$sql = "SELECT YearClassTeacherID FROM {$intranet_db}.YEAR_CLASS_TEACHER as yct INNER JOIN {$intranet_db}.YEAR_CLASS as yc on yct.YearClassID = yc.YearClassID and yc.AcademicYearID = '{$currentAcademicYear}' WHERE yct.UserID = '{$UserID}'";
	
		$result = $ldb->returnVector($sql);
		$isClassTeacher = (count($result) == 0) ? false : true;
		$edit_field = ($isClassTeacher) ? " , CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>')" : ", if(c.CreatorID = ".$UserID.", CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>'), '&nbsp;')";
	}
	else if ($approvalSettings['Elements']['Self'])	//only the assigned teacher or admin show checkbox
	{
	  // $edit_field = ", if((a.ApprovedBy = ".$UserID." OR c.CreatorID = ".$UserID.") , CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>'), '&nbsp;')";
	  $edit_field = ", if((a.ApprovedBy = ".$UserID." OR a.RequestApprovedBy = ".$UserID." OR c.CreatorID = ".$UserID.") , CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>'), '&nbsp;')";
	}
*/ 

}
else	//show checkbox to all
{
//	$edit_field = " , CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>')";
	$edit_field = "  CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>')";
}
if($sys_custom['iPf']['displayIneRC'] && ($IntExt==0 || $IntExt=='') ){
	// a.displayIneRC :(1 = tick , 0 = '')
	$displayIneRC = " IF(a.displayIneRC = 1, '<img src=\"/images/{$LAYOUT_SKIN}/icon_tick_green.gif\" />', '') as displayIneRC, ";
}

if($sys_custom['NgWah_SAS']){
	$displaySASPoint = " a.SASPoint, ";

}

if($IntExt == 1)
{
  $sql =  "
            SELECT
              a.RecordStatus,".
              $sqlStudentName.
              $sqlClass.
              $sqlSelectfield['STUDENT_DETAILS'].
              $sqlSelectfield['OLE_ROLE'].
              $sqlSelectfield['ACHIEVEMENTS'].
              $displaySASPoint.
              $sqlSelectfield['ATTACHMENT'].
              $sqlSelectfield['COMMENT'].
              $sqlSelectfield['STATUS'].
			  $sqlSelectfield['APPROVALDATE'].
              $sqlSelectfield['APPROVER'].
              $edit_field.",
              CONCAT(b.ClassName,' - ', LPAD(b.ClassNumber, 2, '0')) AS ClassNo
            FROM
              {$eclass_db}.OLE_STUDENT as a
            INNER JOIN {$eclass_db}.OLE_PROGRAM as c
              ON a.ProgramID = c.ProgramID
            INNER JOIN {$intranet_db}.INTRANET_USER as b
              ON a.UserID = b.UserID
            LEFT JOIN {$intranet_db}.INTRANET_USER as d
              ON a.ApprovedBy = d.UserID
            WHERE
              a.ProgramID = '$EventID'
              $conds
              $program_cond
          ";
}
else
{
  $sql =  "
            SELECT
              a.RecordStatus,".
              $sqlStudentName.
              $sqlClass.
              $sqlSelectfield['STUDENT_DETAILS'].
              $sqlSelectfield['OLE_ROLE'].
              $sqlSelectfield['ACHIEVEMENTS']."
              a.Hours,".
              $displaySASPoint.
              $sqlSelectfield['ATTACHMENT'].
              $sqlSelectfield['COMMENT'].
              $sqlSelectfield['STATUS'].
			  $sqlSelectfield['APPROVALDATE'].
              $sqlSelectfield['APPROVER'].
              $displayIneRC.$edit_field.",
              CONCAT(b.ClassName,' - ', LPAD(b.ClassNumber, 2, '0')) AS ClassNo
            FROM
              {$eclass_db}.OLE_STUDENT as a
            INNER JOIN {$eclass_db}.OLE_PROGRAM as c
              ON a.ProgramID = c.ProgramID
            INNER JOIN {$intranet_db}.INTRANET_USER as b
              ON a.UserID = b.UserID
            LEFT JOIN {$intranet_db}.INTRANET_USER as d
              ON a.ApprovedBy = d.UserID
            WHERE
              a.ProgramID = '$EventID'
              $conds
              $program_cond
          ";
}
//echo $sql."<Br/>";
//debug_r(htmlspecialchars($sql));
// TABLE INFO
$LibTable->sql = $sql;
$LibTable->db = $intranet_db;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
if ($IsExport)
{
	$LibTable->page_size = 99999;
} else
{
	$LibTable->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
}
if ($page_size_change!="") $li->page_size = $numPerPage;// no_col = total col - number of hidden col
$LibTable->no_col = ($IntExt == 1) ? 12-$countHide : 13-$countHide;
$LibTable->no_col = ($displayEditFunctions) ? $LibTable->no_col : ($LibTable->no_col - 1);

// Added: 2015-01-21 - for St. Paul PAS cust - add 1 more column
if($sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme']){
	$LibTable->no_col++;
}

if($sys_custom['iPf']['displayIneRC'] && ($IntExt==0 || $IntExt=='')  ){
	$LibTable->no_col++;
}

$LibTable->table_tag = "<table align='center' width='100%' border='0' cellpadding='3' cellspacing='1'>";
// Control column width
/*
$LibTable->table_tag .= "<col /><col width='100' /><col width='50' /><col width='100' /><col width='250' />";
$LibTable->table_tag .= ($IntExt == 1) ? "" : "<col width='50' />";
$LibTable->table_tag .= "<col width='15' /><col width='100' /><col width='50' /><col width='100' /><col width='100' />";*/

//$LibTable->row_alt = array("#FFFFFF", "F3F3F3");
$LibTable->row_alt = array("", "");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "middle";

// TABLE COLUMN
//$LibTable->column_list .= "<tr class='tabletop'>\n";
// instead of hardcode 
$currentCol = 0;

$LibTable->column_list .= "<td class='tabletop' width='1%' align='center' ><span class='tabletoplink'>#</span></td>\n";
$LibTable->column_list .= "<td class='tabletop' nowrap='nowrap' width='15%' align='center'>".$LibTable->column($currentCol,$iPort['student_name'], 1)."</td>\n";
$currentCol ++;
$LibTable->column_list .= "<td class='tabletop' width='6%' align='center'>".$LibTable->column($currentCol,$ec_iPortfolio['class'], 1)."</td>\n";
$currentCol ++;

// Added: 2015-01-21 - for St. Paul PAS cust - add 1 more column
if($sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme']){
	$LibTable->column_list .= "<td class='tabletop' width='12%' align='center'>".$LibTable->column($currentCol, $ec_iPortfolio['details'], 1)."</td>\n";
	$currentCol ++;
}

if(!$IPF_HIDE_COLUMN['OLE_ROLE']){
	$LibTable->column_list .= "<td class='tabletop' width='10%' align='center' >".$LibTable->column($currentCol,$ec_iPortfolio['ole_role'], 1)."</td>\n";
	$currentCol ++;
}
if(!$IPF_HIDE_COLUMN['ACHIEVEMENTS'] ){
$LibTable->column_list .= "<td class='tabletop' width='15%' align='center'>".$LibTable->column($currentCol,$ec_iPortfolio['achievement'], 1)."</td>\n";
$currentCol ++;
}
if($IntExt == 1)
{
}
else
{
  $LibTable->column_list .= "<td class='tabletop' align='center' width='5%'>".$LibTable->column($currentCol,$ec_iPortfolio['hours'], 1)."</td>\n";
  $currentCol ++;
}
if($sys_custom['NgWah_SAS']){
	$LibTable->column_list .= "<td class='tabletop' align='center' width='5%'>".$LibTable->column($currentCol,$Lang['iPortfolio']['SAS_Settings']['Credit_pt'],1)."</td>\n";
	$currentCol ++;
	$LibTable->no_col++;
}
if(!$IPF_HIDE_COLUMN['ATTACHMENT']){
	$LibTable->column_list .= "<td class='tabletop' width='5%' align='center'><span class='tabletoplink'>".$ec_iPortfolio['attachment_only']."</span></td>\n";
	$currentCol ++;
}
if(!$IPF_HIDE_COLUMN['COMMENT']){
	$LibTable->column_list .= "<td class='tabletop' align='center' width='10%'>".$LibTable->column($currentCol,$ec_iPortfolio['comment'], 1)."</td>\n";
	$currentCol ++;
}
if(!$IPF_HIDE_COLUMN['STATUS']){
$LibTable->column_list .= "<td class='tabletop' align='center' width='3%'>".$LibTable->column($currentCol,$ec_iPortfolio['status'], 1)."</td>\n";
$currentCol ++;
}
if(!$IPF_HIDE_COLUMN['APPROVALDATE']){
$LibTable->column_list .= "<td class='tabletop' align='center' width='12%'>".$LibTable->column($currentCol,$Lang['iPortfolio']['approvaldate'], 1)."</td>\n";
$currentCol ++;
}
if(!$IPF_HIDE_COLUMN['APPROVER']){
$LibTable->column_list .= "<td class='tabletop' align='center' width='10%'>".$LibTable->column($currentCol,$Lang['iPortfolio']['approver'], 1)."</td>\n";
$currentCol ++;
}
if($sys_custom['iPf']['displayIneRC'] && ($IntExt==0 || $IntExt=='')  ){
$LibTable->column_list .= "<td class='tabletop' align='center' width='10%'>".$LibTable->column($currentCol,$Lang['iPortfolio']['OLE']['DisplayIneRC'], 1)."</td>\n";
$currentCol ++;
}
$LibTable->column_list .= ($displayEditFunctions) ? "<td  class='tabletop' align='center' width='1%'>".$LibTable->check("record_id[]")."</td>\n" : "";


$LibTable->column_array = array(10,10,0,10,0,0,10,10);
//$LibTable->column_array = array(10,10,0,0);

if ($IsExport)
{
	# export to CSV
	
	$le = new libexporttext();

	$export_header = array($iPort['student_name'], $ec_iPortfolio['class'], $ec_iPortfolio['ole_role'], $ec_iPortfolio['achievement']);
	if($IntExt == 1)
	{
	} else
	{
		$export_header[] = $ec_iPortfolio['hours'];
	}
	$export_header[] = $ec_iPortfolio['attachment_only'];
	  $export_header[] = $ec_iPortfolio['comment'];
	  $export_header[] = $ec_iPortfolio['status'];
	  $export_header[] = $Lang['iPortfolio']['approvaldate'];
	  $export_header[] = $Lang['iPortfolio']['approver'];
	  
	# loop
	$export_ary = $LibTable->exportOLE_StudentRecordList();
	
	$export_text = $le->GET_EXPORT_TXT($export_ary,$export_header);
	$filename = "OLE_members_id{$EventID}_".$eng_title_html.".csv";
	$le->EXPORT_FILE($filename,$export_text); 
	    
	intranet_closedb();
	die();
} else
{
	# HTML 
	$stu_rec_html = $LibTable->displayOLE_StudentRecordList();
	if ($LibTable->navigationHTML!="")
	{
	  $stu_rec_html .= "<table align='center' width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
	  $stu_rec_html .= "<tr class='tablebottom'>";
	  $stu_rec_html .= "<td class=\"tabletext\" align=\"right\">".$LibTable->navigationHTML."</td>";
	  $stu_rec_html .= "</tr>";
	  $stu_rec_html .= "</table>";
	}
	
	########################################################
	# Student Record : End
	########################################################
	
	########################################################
	# Layout Display
	########################################################
	$MenuArr = array();
	$MenuArr[] = array($ec_iPortfolio['program_list'], "ole.php?IntExt=".$IntExt);
	$MenuArr[] = array($eng_title_html, "");
	$navigation_html = $linterface->GET_NAVIGATION($MenuArr);
	if(!$isReadOnly){
		$EditButton = $linterface->GET_SMALL_BTN($Lang['Btn']['Edit'], "button","location.href='ole_new.php?record_id=$EventID&IntExt=$IntExt'","EditBtn");
	}
	
	$linterface->LAYOUT_START();
	include_once("template/ole_event_studentview.tmpl.php");
	$linterface->LAYOUT_STOP();
	intranet_closedb();
} 
?>