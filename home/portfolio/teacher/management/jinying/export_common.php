<?
/*
 *  2019-12-02 [Cameron]
 *      - add function Export_StandardWoAward()
 *
 * 	2017-09-13 [Cameron]
 * 		- fix: change WebSAMS to WebSAMSRegNo	(case #J123442)
 * 		- add two columns: OrganizationNameEng & OrganizationNameChi in Export_BTP_IC() (case #J125590)
 * 
 * 	2017-08-28 [Cameron]
 * 		- export WebSAMSRegNo next to class number for consistency
 * 
 * 	2017-06-28 [Cameron]
 * 		- fix grammatical error in Export_OFS_SO(): 1 good point	[case #J119404]
 * 
 * 	2017-04-26 [Cameron]
 * 		- create this file
 */
 
 function Validate_Performance_Cell($cell,$code) {
 	global $activeSheet, $Lang;
 	
	//Data Validation list
    $objValidation = $activeSheet->getCell($cell)->getDataValidation();
    $objValidation->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
    $objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_STOP);
    $objValidation->setAllowBlank(false);
    $objValidation->setShowDropDown(true);
	$objValidation->setShowErrorMessage(true);
    $objValidation->setErrorTitle($Lang['iPortfolio']['JinYing']['Export']['Error']['InputError']);
    $objValidation->setError($Lang['iPortfolio']['JinYing']['Export']['Error']['NotInList']);
    $objValidation->setFormula1('"'.implode(', ',$Lang['iPortfolio']['JinYing']['CodeName'][$code]).'"');
 }
 

 function Validate_RecommendMerit_Cell($cell) {
 	global $activeSheet, $Lang;
 	
	//Data Validation list
    $objValidation = $activeSheet->getCell($cell)->getDataValidation();
    $objValidation->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
    $objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_STOP);
    $objValidation->setAllowBlank(true);
    $objValidation->setShowDropDown(true);
	$objValidation->setShowErrorMessage(true);
    $objValidation->setErrorTitle($Lang['iPortfolio']['JinYing']['Export']['Error']['InputError']);
    $objValidation->setError($Lang['iPortfolio']['JinYing']['Export']['Error']['NotInList']);
    $objValidation->setFormula1('"'.implode(', ',$Lang['iPortfolio']['JinYing']['RecommendAward']).'"');
 }


 function Validate_ServiceHours_Cell($cell) {
 	global $activeSheet, $Lang;
 	
	//Data Validation list
    $objValidation = $activeSheet->getCell($cell)->getDataValidation();
    $objValidation->setType(PHPExcel_Cell_DataValidation::TYPE_WHOLE);
    $objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_STOP);
    $objValidation->setAllowBlank(true);
	$objValidation->setShowErrorMessage(true);
    $objValidation->setErrorTitle($Lang['iPortfolio']['JinYing']['Export']['Error']['InputError']);
    $objValidation->setError($Lang['iPortfolio']['JinYing']['Export']['Error']['InputInteger4ServiceHours']);
    $objValidation->setFormula1(1);
    $objValidation->setFormula2(300);
 }


 function Validate_RecordID($cell, $recordID) {
 	global $activeSheet, $Lang;
 	
	//Data Validation list
    $objValidation = $activeSheet->getCell($cell)->getDataValidation();
    $objValidation->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
    $objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_STOP);
    $objValidation->setAllowBlank(false);
	$objValidation->setShowErrorMessage(true);
	$objValidation->setShowDropDown(false);
    $objValidation->setErrorTitle($Lang['iPortfolio']['JinYing']['Export']['Error']['ReadOnlyTitle']);
    $objValidation->setError($Lang['iPortfolio']['JinYing']['Export']['Error']['ReadOnlyContent']);
    $objValidation->setFormula1('"'.$recordID.'"');
 }

 
 function Export_Standard($code) {
 	global $activeSheet, $semesters, $Lang, $data;

	for ($i=0,$iMax=count($data);$i<$iMax;$i++) {
		$row = $data[$i];
		$r = $i+2;
				
		$activeSheet->setCellValue('A'.$r, $row['ClassName']);
		$activeSheet->setCellValue('B'.$r, $row['ClassNumber']);
		$activeSheet->setCellValue('C'.$r, $row['WebSAMSRegNo']);
		$activeSheet->setCellValue('D'.$r, $row['StudentName']);
		$activeSheet->setCellValue('E'.$r, $Lang['iPortfolio']['JinYing']['CodeName'][$code][$row['PerformanceCode']]);
		$activeSheet->setCellValue('F'.$r, $Lang['iPortfolio']['JinYing']['CodeDesc']['JYR'][$row['PerformanceCode']]);
		$activeSheet->setCellValue('G'.$r, $Lang['iPortfolio']['JinYing']['CodeAward'][$row['PerformanceCode']]);
		$activeSheet->setCellValue('H'.$r, $semesters[$row['YearTermID']]);
		$activeSheet->setCellValue('I'.$r, $row['RecordID']);
	
		Validate_Performance_Cell('E'.$r, $code);
		Validate_RecordID('I'.$r, $row['RecordID']);
	}
 }
 
 
 function Export_With_ActivityDate($code) {
 	global $activeSheet, $Lang, $data;

	for ($i=0,$iMax=count($data);$i<$iMax;$i++) {
		$row = $data[$i];
		$r = $i+2;
				
		$activeSheet->setCellValue('A'.$r, $row['ClassName']);
		$activeSheet->setCellValue('B'.$r, $row['ClassNumber']);
		$activeSheet->setCellValue('C'.$r, $row['WebSAMSRegNo']);
		$activeSheet->setCellValue('D'.$r, $row['StudentName']);
		$activeSheet->setCellValue('E'.$r, $row['ActivityDate'] == '0000-00-00' ? '' : $row['ActivityDate']);
		$activeSheet->getStyle('E'.$r)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);		
		$activeSheet->setCellValue('F'.$r, $row['ActivityNameEng']);
		$activeSheet->setCellValue('G'.$r, $row['ActivityNameChi']);
		$activeSheet->setCellValue('H'.$r, $Lang['iPortfolio']['JinYing']['CodeName'][$code][$row['PerformanceCode']]);
		$activeSheet->setCellValue('I'.$r, $Lang['iPortfolio']['JinYing']['CodeAward'][$row['PerformanceCode']]);
		$activeSheet->setCellValue('J'.$r, $row['RecordID']);
	
		Validate_Performance_Cell('H'.$r, $code);
		Validate_RecordID('J'.$r, $row['RecordID']);
	}
 }


 function Export_Physical($code) {
 	global $activeSheet, $Lang, $data;

	for ($i=0,$iMax=count($data);$i<$iMax;$i++) {
		$row = $data[$i];
		$r = $i+2;
				
		$activeSheet->setCellValue('A'.$r, $row['ClassName']);
		$activeSheet->setCellValue('B'.$r, $row['ClassNumber']);
		$activeSheet->setCellValue('C'.$r, $row['WebSAMSRegNo']);
		$activeSheet->setCellValue('D'.$r, $row['StudentName']);
		$activeSheet->setCellValue('E'.$r, $Lang['iPortfolio']['JinYing']['CodeName'][$code][$row['PerformanceCode']]);
		$activeSheet->setCellValue('F'.$r, $Lang['iPortfolio']['JinYing']['CodeDesc']['JYR'][$row['PerformanceCode']]);
		$activeSheet->setCellValue('G'.$r, $Lang['iPortfolio']['JinYing']['CodeAward'][$row['PerformanceCode']]);
		$activeSheet->setCellValue('H'.$r, $row['RecordID']);
	
		Validate_Performance_Cell('E'.$r, $code);
		Validate_RecordID('H'.$r, $row['RecordID']);
	}
 }
 
 
 function Export_OFS_SI() {
 	global $activeSheet, $Lang, $data, $PATH_WRT_ROOT;
	include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/libpf-slp-tmchkwc.php");
	$obj = new objPrinting();

	for ($i=0,$iMax=count($data);$i<$iMax;$i++) {
		$row = $data[$i];
		$r = $i+2;
				
		$activeSheet->setCellValue('A'.$r, $row['ClassName']);
		$activeSheet->setCellValue('B'.$r, $row['ClassNumber']);
		$activeSheet->setCellValue('C'.$r, $row['WebSAMSRegNo']);
		$activeSheet->setCellValue('D'.$r, $row['StudentName']);
		$activeSheet->setCellValue('E'.$r, $row['ActivityNameEng']);
		$activeSheet->setCellValue('F'.$r, $row['ActivityNameChi']);
		$activeSheet->setCellValue('G'.$r, $Lang['iPortfolio']['JinYing']['CodeName']['OFS-SI'][$row['PerformanceCode']]);
		$activeSheet->setCellValue('H'.$r, $Lang['iPortfolio']['JinYing']['RecommendAward'][$row['RecommendMerit']]);
		$performanceDesc = Get_Lang_Selection($row['ActivityNameChi'],$row['ActivityNameEng']);
		$activeSheet->setCellValue('I'.$r, $performanceDesc);
		$award = $obj->getServiceInSchoolAward($row['RecommendMerit']);
		$activeSheet->setCellValue('J'.$r, $award);
		$activeSheet->setCellValue('K'.$r, $row['RecordID']);
	
		Validate_Performance_Cell('G'.$r, 'OFS-SI');
		Validate_RecommendMerit_Cell('H'.$r);
		Validate_RecordID('K'.$r, $row['RecordID']);
	} 
	
	unset($obj);	
 }


 function Export_OFS_SO() {
 	global $activeSheet, $Lang, $data, $ljy;
	
	for ($i=0,$iMax=count($data);$i<$iMax;$i++) {
		$row = $data[$i];
		$r = $i+2;
				
		$activeSheet->setCellValue('A'.$r, $row['ClassName']);
		$activeSheet->setCellValue('B'.$r, $row['ClassNumber']);
		$activeSheet->setCellValue('C'.$r, $row['WebSAMSRegNo']);
		$activeSheet->setCellValue('D'.$r, $row['StudentName']);
		$activeSheet->setCellValue('E'.$r, $row['ServiceHours']);
		$activeSheet->getStyle('E'.$r)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

		$nbrGP = $ljy->getGoodPointsByHours($row['ServiceHours']);
		if ($nbrGP) {
			$award = sprintf($Lang['iPortfolio']['JinYing']['CodeAward'][$row['PerformanceCode']],$nbrGP);
			if (($_SESSION['intranet_session_language'] == 'en') && ($nbrGP>1)) {
				$award .= 's';
			}
		}
		else {
			$award = '-';
		}
		
		$activeSheet->setCellValue('F'.$r, $award);
		$activeSheet->setCellValue('G'.$r, $row['RecordID']);
	
		Validate_ServiceHours_Cell('E'.$r);
		Validate_RecordID('G'.$r, $row['RecordID']);
	} 
 }
 
 
 function Export_BTP_EA() {
 	global $activeSheet, $Lang, $data;

	for ($i=0,$iMax=count($data);$i<$iMax;$i++) {
		$row = $data[$i];
		$r = $i+2;
				
		$activeSheet->setCellValue('A'.$r, $row['ClassName']);
		$activeSheet->setCellValue('B'.$r, $row['ClassNumber']);
		$activeSheet->setCellValue('C'.$r, $row['WebSAMSRegNo']);
		$activeSheet->setCellValue('D'.$r, $row['StudentName']);
		$activeSheet->setCellValue('E'.$r, $row['ActivityNameEng']);
		$activeSheet->setCellValue('F'.$r, $row['ActivityNameChi']);
		$activeSheet->setCellValue('G'.$r, $Lang['iPortfolio']['JinYing']['CodeName']['BTP-EA'][$row['PerformanceCode']]);
		
		$performanceDesc = Get_Lang_Selection($row['ActivityNameChi'],$row['ActivityNameEng']).' : '.$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR'][$row['PerformanceCode']];
		$activeSheet->setCellValue('H'.$r, $performanceDesc);
		$activeSheet->setCellValue('I'.$r, $Lang['iPortfolio']['JinYing']['CodeAward'][$row['PerformanceCode']]);
		$activeSheet->setCellValue('J'.$r, $row['RecordID']);
	
		Validate_Performance_Cell('G'.$r, 'BTP-EA');
		Validate_RecordID('J'.$r, $row['RecordID']);
	}
 	
 }
 

 function Export_BTP_IC() {
 	global $activeSheet, $Lang, $data, $PATH_WRT_ROOT;
	include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/libpf-slp-tmchkwc.php");
	$obj = new objPrinting();

	for ($i=0,$iMax=count($data);$i<$iMax;$i++) {
		$row = $data[$i];
		$r = $i+2;
				
		$activeSheet->setCellValue('A'.$r, $row['ClassName']);
		$activeSheet->setCellValue('B'.$r, $row['ClassNumber']);
		$activeSheet->setCellValue('C'.$r, $row['WebSAMSRegNo']);
		$activeSheet->setCellValue('D'.$r, $row['StudentName']);
		
		$activeSheet->setCellValue('E'.$r, $row['ActivityDate'] == '0000-00-00' ? '' : $row['ActivityDate']);
		$activeSheet->getStyle('E'.$r)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);		
		$activeSheet->setCellValue('F'.$r, $row['ActivityNameEng']);
		$activeSheet->setCellValue('G'.$r, $row['ActivityNameChi']);
		$activeSheet->setCellValue('H'.$r, $row['OrganizationNameEng']);
		$activeSheet->setCellValue('I'.$r, $row['OrganizationNameChi']);
		$activeSheet->setCellValue('J'.$r, $Lang['iPortfolio']['JinYing']['CodeName']['BTP-IC'][$row['PerformanceCode']]);
		$activeSheet->setCellValue('K'.$r, $row['AwardNameEng']);
		$activeSheet->setCellValue('L'.$r, $row['AwardNameChi']);
		$activeSheet->setCellValue('M'.$r, $Lang['iPortfolio']['JinYing']['RecommendAward'][$row['RecommendMerit']]);
		
		$performanceCode = $row['PerformanceCode'];
		$performanceDesc = Get_Lang_Selection($row['ActivityNameChi'],$row['ActivityNameEng']);
		if ($_SESSION['intranet_session_language'] == 'en') { 
			$performanceDesc .= '('.$row['ActivityNameChi'].')';
		}
		
		switch ($performanceCode) {
			case 'BTP-IC-P-01':
				$performanceDesc .= ' : ';
				$performanceDesc .= $Lang['iPortfolio']['JinYing']['CodeDesc']['JYR'][$performanceCode];
				$award = $Lang['iPortfolio']['JinYing']['CodeAward'][$performanceCode];
				break;
			case 'BTP-IC-P-02':
				$performanceDesc .= ' : ';
				$performanceDesc .= $Lang['iPortfolio']['JinYing']['CodeDesc']['JYR'][$performanceCode];
				$award = $Lang['iPortfolio']['JinYing']['CodeAward'][$performanceCode];
				break;
			case 'BTP-IC-P-03': 
				$performanceDesc .= ' : ';
				$performanceDesc .= Get_Lang_Selection($row['AwardNameChi'],$row['AwardNameEng']);
				$award = $obj->getInterSchoolCompetitionsAward($row['RecommendMerit']);
				break;
			default:
				$award = '';
				break;
		} 
		
		$activeSheet->setCellValue('N'.$r, $performanceDesc);
		$activeSheet->setCellValue('O'.$r, $award);
		$activeSheet->setCellValue('P'.$r, $row['RecordID']);
	
		Validate_Performance_Cell('J'.$r, 'BTP-IC');
		Validate_RecommendMerit_Cell('M'.$r);
		Validate_RecordID('P'.$r, $row['RecordID']);
	}
 	unset($obj);
 }

# used in EPW-PW (Physical well being), EPW-OC (Overall comment)
function Export_StandardWoAward($code) {
    global $activeSheet, $semesters, $Lang, $data;

    for ($i=0,$iMax=count($data);$i<$iMax;$i++) {
        $row = $data[$i];
        $r = $i+2;

        $activeSheet->setCellValue('A'.$r, $row['ClassName']);
        $activeSheet->setCellValue('B'.$r, $row['ClassNumber']);
        $activeSheet->setCellValue('C'.$r, $row['WebSAMSRegNo']);
        $activeSheet->setCellValue('D'.$r, $row['StudentName']);
        $activeSheet->setCellValue('E'.$r, $Lang['iPortfolio']['JinYing']['CodeName'][$code][$row['PerformanceCode']]);
        $activeSheet->setCellValue('F'.$r, $Lang['iPortfolio']['JinYing']['CodeDesc']['JYR'][$row['PerformanceCode']]);
        $activeSheet->setCellValue('G'.$r, $semesters[$row['YearTermID']]);
        $activeSheet->setCellValue('H'.$r, $row['RecordID']);

        Validate_Performance_Cell('E'.$r, $code);
        Validate_RecordID('H'.$r, $row['RecordID']);
    }
}


?>