<?php
// using:
/*
 * 2018-04-18 Cameron
 * - fix retrieving $academic_start_year and $academic_end_year
 *
 * 2018-04-16 Cameron
 * - retrieve numMedalAll, numMedalNone, percentMedalAll and percentMedalNone for ByMethod='Form'
 *
 * 2017-02-15 Cameron
 * - create this file
 */
@SET_TIME_LIMIT(216000);
ini_set('memory_limit', '256M');

$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/iportfolio_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/customize/tmchkwc/config.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/customize/tmchkwc/libpf-jinying.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

iportfolio_auth("T"); // teacher
intranet_opendb();

if (! $sys_custom['iPf']['JinYingScheme']) {
    exit();
}

$AcademicYearID = $_POST['AcademicYearID'];
if (! $AcademicYearID) {
    return '';
}
$ByMethod = $_POST['ByMethod'];
if (! $ByMethod) {
    return '';
}

$ljy = new libJinYing();

$ljy->setAcademicYearID($AcademicYearID);

$academic_start_year = substr(getStartDateOfAcademicYear($AcademicYearID), 0, 4);
$academic_end_year = substr(getEndDateOfAcademicYear($AcademicYearID), 0, 4);

switch ($ByMethod) {
    case 'Form':
        $stat = $ljy->getMedalStatByClassLevel();
        $column_one = $Lang['iPortfolio']['JinYing']['Statistics']['Form'];
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByForm'], $academic_start_year, $academic_end_year);
        break;
    
    case 'Class':
        $stat = $ljy->getMedalStatByClass();
        $column_one = $Lang['iPortfolio']['JinYing']['Statistics']['Class'];
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByClass'], $academic_start_year, $academic_end_year);
        break;
}

$space = $intranet_session_language == 'en' ? ' ' : '';

$exportColumn = array();
$exportColumn[0][] = $column_one;
$exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Total'];
$exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Medal']['Gold'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
$exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Medal']['Gold'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
$exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Medal']['Silver'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
$exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Medal']['Silver'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
$exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Medal']['Bronze'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
$exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Medal']['Bronze'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
if ($ByMethod == 'Form') {
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Medal']['AllMedal'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Medal']['AllMedal'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Medal']['NoMedal'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Medal']['NoMedal'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
}
$lexport = new libexporttext();

$result_data = array();
foreach ((array) $stat as $r => $v) {
    $tmpRow = array();
    $tmpRow[] = $v['Name'];
    $tmpRow[] = $v['NumStudent'];
    $tmpRow[] = $v['NumGold'];
    $tmpRow[] = $v['PercentGold'] . '%';
    $tmpRow[] = $v['NumSilver'];
    $tmpRow[] = $v['PercentSilver'] . '%';
    $tmpRow[] = $v['NumBronze'];
    $tmpRow[] = $v['PercentBronze'] . '%';
    if ($ByMethod == 'Form') {
        $tmpRow[] = $v['NumMedalAll'];
        $tmpRow[] = $v['PercentMedalAll'] . '%';
        $tmpRow[] = $v['NumMedalNone'];
        $tmpRow[] = $v['PercentMedalNone'] . '%';
    }
    $result_data[] = $tmpRow;
    unset($tmpRow);
}

$export_content = $lexport->GET_EXPORT_TXT($result_data, $exportColumn, "\t", "\r\n", "\t", 0, '11');

intranet_closedb();

$filename = $title."_" . date("Ymd") . ".csv";

$lexport->EXPORT_FILE($filename, $export_content, false, false, "UTF-8");
?>