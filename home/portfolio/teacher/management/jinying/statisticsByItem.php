<?php

// Modified by
/*
 * 2019-12-04 [Cameron]
 * - modify showOrHideTerm() so that the items are based on $conf['JinYingItemCodeWithTerm']  [case #J161484]
 *
 * 2018-05-14 [Cameron]
 * - add By AllItems (Export)
 *
 * 2018-05-03 [Cameron]
 * - move export button to result page
 * - add image loading process when submit for different criteria
 * - reload page when export
 *
 * 2018-04-18 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/iportfolio_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libportfolio.php");
include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/customize/tmchkwc/libpf-jinying.php");

iportfolio_auth("T"); // teacher
intranet_opendb();

if (! $sys_custom['iPf']['JinYingScheme']) {
    header("Location: /home/portfolio/");
    exit();
}

$lpf = new libportfolio();
$lfcm = new form_class_manage();

if (! $lpf->IS_IPF_SUPERADMIN() && ! $_SESSION['SSV_USER_ACCESS']['other-iPortfolio']) { // only allow iPortfolio admin to access
    header("Location: /home/portfolio/");
    exit();
}

$AcademicYearID = $_POST['AcademicYearID'] ? $_POST['AcademicYearID'] : Get_Current_Academic_Year_ID();

// Acadermic Year Selection
$yearFilter = getSelectAcademicYear("AcademicYearID", '', 1, 0, $AcademicYearID);

// Build Item List
$itemList_array = array();
$itemList_array[] = array(
    '',
    $Lang['iPortfolio']['JinYing']['SelectItem']
);
foreach ($Lang['iPortfolio']['JinYing']['Item'] as $scope => $item) {
    $itemList_array[] = array(
        'OPTGROUP',
        $Lang['iPortfolio']['JinYing']['Scope'][$scope]
    ); // (value, name)
    foreach ($item as $k => $v) {
        $code = "$scope-$k";
        $itemList_array[] = array(
            $code,
            $v
        );
    }
}
$itemFilter = returnSelection($itemList_array, '', 'Item', 'ID="Item"');

$currentYearTerm = getCurrentAcademicYearAndYearTerm();
$currentTerm = $currentYearTerm['YearTermID'];

// Acadermic Term Selection
$semesterFilter = getSelectSemester2('name="Semester" id="Semester" style="display:none"', $currentTerm, $ParQuoteValue = 1, $AcademicYearID);

// Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_JinYing_Scheme";

// ## Title ###
$ljy = new libJinYing();
$TAGS_OBJ = $ljy->getJinYingTab('Statistics');
$statisticsAnalysisTab = $ljy->getStatisticsAnalysisTabMenu('ByItem');

$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

?>
<style>
.green {
	color: #00b33c;
}
</style>

<script type="text/JavaScript" language="JavaScript">
    var isLoading = true;
    var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';

	function hideOptionLayer()
	{
		$('#form1').attr('style', 'display: none');
		$('.spanHideOption').attr('style', 'display: none');
		$('.spanShowOption').attr('style', '');
	}
	
	function showOptionLayer()
	{
		$('#form1').attr('style', '');
		$('.spanShowOption').attr('style', 'display: none');
		$('.spanHideOption').attr('style', '');
	}

	$(document).ready(function(){

		$('#Item').change(function(){
			showOrHideTerm($(this).val());
		});

		$('#Item').click(function(){
			$('#Specific').attr('checked',true);
		});

		$('#AllItems').click(function(){
			$('#form_result').html('');
		});
		
		$('#AcademicYearID').change(function(){
	        isLoading = true;
	        $('#semesterSpan').html(loadingImg);
			
			$.ajax({
				dataType: "json",
				type: "POST",
				url: 'ajax_get_academic_year_terms.php',
				data : {'AcademicYearID': $('#AcademicYearID').val(), 
						'Item': $('#Item').val()
						},		  
				success: function(ajaxReturn) {
		        	if (ajaxReturn != null && ajaxReturn.success){
	        			$('#semesterSpan').html(ajaxReturn.html);
	        			showOrHideTerm($('#Item').val());
	        			isLoading = false;
		        	}
				},
				error: show_ajax_error
			});
		});
		
		$('#submit_result').click(function(){
			if ($('#Specific').is(':checked')) {
    			if ($('#Item').val() == '') {
    				alert("<?php echo $Lang['iPortfolio']['JinYing']['Statistics']['WarningSelectItem']?>");
    				return;					
    			}
    
    	        isLoading = true;
    			$('#form_result').html(loadingImg);
    			
    			$.ajax({
    				dataType: "json",
    				type: "POST",
    				url: 'ajax_get_stat_by_item_result.php',
    				data : $("#form1").serialize(),		  
    				success: load_stat_by_item_result_layout,
    				error: show_ajax_error
    			});
			}
			else {	// export all items
				var original_action = document.form1.action;
				var url = "export_by_item_statistics_all.php";
				document.form1.action = url;
				document.form1.submit();
				document.form1.action = original_action;
			}
		});
				
	});

	function showOrHideTerm(itemCode) 
	{
        <?php echo "var codeWithTermAry = ['".implode("','",$conf['JinYingItemCodeWithTerm'])."'];"; ?>
        if (codeWithTermAry.indexOf(itemCode) != -1) {
            $('#Semester').css("display","");
        }
        else {
            $('#Semester').css("display","none");
        }
	}

	function load_stat_by_item_result_layout(ajaxReturn) {
		if (ajaxReturn != null && ajaxReturn.success){
//			$('#form1').attr('style','display: none');
			$('#form_result').html(ajaxReturn.html);
// 			$('#spanShowOption').show();
// 			$('#spanHideOption').hide();
			showOptionLayer();
			$('#report_show_option').addClass("report_option report_hide_option");
			isLoading = false;
//			window.scrollTo(0, 0);
		}
	}	
	
	function show_ajax_error() {
		alert('<?=$Lang['iPortfolio']['JinYing']['ajaxError']?>');
	}
	
	function js_export() {
		var original_action = document.form1.action;
		var url = "export_by_item_statistics.php";
		document.form1.action = url;
		document.form1.submit();
		document.form1.action = original_action;
		$('#submit_result').click();	
	}

</script>

<div>
  <?php echo $statisticsAnalysisTab;?>
</div>

<div id="report_show_option">
	<span id="spanShowOption" class="spanShowOption" style="display: none">
		<a href="javascript:showOptionLayer();"><?=$Lang['iPortfolio']['JinYing']['Medal']['ShowOption']?></a>
	</span> <span id="spanHideOption" class="spanHideOption"
		style="display: none"> <a href="javascript:hideOptionLayer();"><?=$Lang['iPortfolio']['JinYing']['Medal']['HideOption']?></a>
	</span>

	<form name="form1" method="post" id="form1">

		<table class="form_table_v30">
			<tr>
				<td class="field_title"><?=$Lang['General']['SchoolYear']?><span
					class=tabletextrequire>*</span></td>
				<td><?=$yearFilter?></td>
			</tr>

			<tr>
				<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['iPortfolio']['JinYing']['ActivityItems']?><span
					class=tabletextrequire>*</span></td>
				<td class="tabletext" width="80%"><label><input type="radio"
						name="ByMethod" id="AllItems" value="AllItems" checked><?=$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['AllItems']?></label></br>
					<label for="Specific"><input type="radio" name="ByMethod"
						id="Specific" value="Specific">				
					<?=$itemFilter?>
					<span id="semesterSpan"><?=$semesterFilter ?></span>
					<?=$Lang['iPortfolio']['JinYing']['Remark']?></label></td>
			</tr>

			<tr>
				<td colspan=2 class="tabletextremark"><?php echo $Lang['iPortfolio']['JinYing']['ImportRemarks']['Mandatory'];?></td>
			</tr>

		</table>

		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "", "submit_result")?>&nbsp;
		</div>
	</form>
</div>
<div id="form_result"></div>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>