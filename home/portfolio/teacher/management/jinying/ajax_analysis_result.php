<?php
/*
 * Log
 *
 * Purpose: return statistics analysis result
 *
 * Date: 2018-05-03 [Cameron]
 * - add export button in this page
 * - add separate line before the row for junior total
 *
 * Date: 2018-04-16 [Cameron]
 * - retrieve numMedalAll, numMedalNone, percentMedalAll and percentMedalNone for ByMethod='Form'
 * - fix retrieving $academic_start_year and $academic_end_year
 *
 * Date: 2017-02-15 [Cameron]
 * create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/iportfolio_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/json.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/customize/tmchkwc/config.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/customize/tmchkwc/libpf-jinying.php");

iportfolio_auth("T"); // teacher
intranet_opendb();

$json['success'] = false;
if (! $sys_custom['iPf']['JinYingScheme']) {
    exit();
}

$AcademicYearID = $_POST['AcademicYearID'];
if (! $AcademicYearID) {
    return '';
}
$ByMethod = $_POST['ByMethod'];
if (! $ByMethod) {
    return '';
}

$ljy = new libJinYing();

$ljy->setAcademicYearID($AcademicYearID);

$academic_start_year = substr(getStartDateOfAcademicYear($AcademicYearID), 0, 4);
$academic_end_year = substr(getEndDateOfAcademicYear($AcademicYearID), 0, 4);

switch ($ByMethod) {
    case 'Form':
        $stat = $ljy->getMedalStatByClassLevel();
        $column_one = $Lang['iPortfolio']['JinYing']['Statistics']['Form'];
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByForm'], $academic_start_year, $academic_end_year);
        $extraColumnTitleRow1 = '<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Medal']['AllMedal'] . '</th>
            <th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Medal']['NoMedal'] . '</th>';
        $extraColumnTitleRow2 = '<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>';
        break;
    
    case 'Class':
        $stat = $ljy->getMedalStatByClass();
        $column_one = $Lang['iPortfolio']['JinYing']['Statistics']['Class'];
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByClass'], $academic_start_year, $academic_end_year);
        $extraColumnTitleRow1 = '';
        $extraColumnTitleRow2 = '';
        break;
}

$x = '
<style>
	.align_center {text-align:center !important;}
</style>
<form name="form2" id="form2" method="POST">
	<div id="toolbox" class="content_top_tool">
		<div class="Conntent_tool">
			<a href="javascript:js_export();" class="export">' . $Lang['Btn']['Export'] . '</a>
		</div>
		<br style="clear:both" />
	</div>

	<div style="font-weight:bold; text-align:center;">' . $title . '</div><br>
	<table width="98%" border="0" cellpadding="0" cellspacing="0" class="common_table_list">
		<tr>
			<th class="sub_row_top align_center" rowspan="2">' . $column_one . '</th>
			<th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Total'] . '</th>
			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Medal']['Gold'] . '</th>
			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Medal']['Silver'] . '</th>
			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Medal']['Bronze'] . '</th>' . $extraColumnTitleRow1 . '
		</tr>
		<tr>
			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>' . $extraColumnTitleRow2 . '
		</tr>';

foreach ((array) $stat as $r => $v) {
    $rowClass = ($r == 'junior') ? 'total_row' : '';
    $x .= '	<tr class="' . $rowClass . '"><td class="align_center">' . $v['Name'] . '</td>
					<td class="align_center">' . $v['NumStudent'] . '</td>
					<td class="align_center">' . $v['NumGold'] . '</td>
					<td class="align_center">' . $v['PercentGold'] . '%</td>
					<td class="align_center">' . $v['NumSilver'] . '</td>
					<td class="align_center">' . $v['PercentSilver'] . '%</td>
					<td class="align_center">' . $v['NumBronze'] . '</td>
					<td class="align_center">' . $v['PercentBronze'] . '%</td>';
    if ($ByMethod == 'Form') {
        $x .= '<td class="align_center">' . $v['NumMedalAll'] . '</td>
					<td class="align_center">' . $v['PercentMedalAll'] . '%</td>
					<td class="align_center">' . $v['NumMedalNone'] . '</td>
					<td class="align_center">' . $v['PercentMedalNone'] . '%</td>';
    }
    $x .= ' </tr>';
}
$x .= '</table>

</form>';

$json['success'] = true;
$x = remove_dummy_chars_for_json($x);
$json['html'] = $x;

$jsonObj = new JSON_obj();
echo $jsonObj->encode($json);

intranet_closedb();
?>