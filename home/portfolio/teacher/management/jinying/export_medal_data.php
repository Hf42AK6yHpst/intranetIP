<?php
// using:    
/*
 * 	Note:	Please edit in utf-8
 *
 *  2020-07-13 [Cameron]
 *      - pass argument NrScopeMet to getStudentWithMedal() [case #J189433]
 *
 *  2019-12-10 [Cameron]
 *      - add column TargetMetScopeNumber
 *
  * 2019-12-04 [Cameron]
 *      - pass argument $medal to getStudentWithMedal()
 *
 * 	2017-10-24 Cameron
 * 		- create this file
 */

@SET_TIME_LIMIT(216000);
ini_set('memory_limit','512M');
date_default_timezone_set('Asia/Hong_Kong');

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/config.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/libpf-jinying.php");
include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel.php");

iportfolio_auth("T");	// teacher
intranet_opendb();

if (!$sys_custom['iPf']['JinYingScheme']) {
	exit;
}

$AcademicYearID = $_POST['AcademicYearID'];
if (!$AcademicYearID) {
	return '';	
}

$ClassLevel = $_POST['ClassLevel'];
if (!$ClassLevel) {
	return '';	
}


## Step 1: get filters
$ljy = new libJinYing();
$filter = array();
$ljy->setAcademicYearID($AcademicYearID);
$ljy->setClassLevel(implode(',',$ClassLevel));
$ljy->setAwarded($Awarded);
$ljy->setNotAwarded($NotAwarded);

$filter['BSQ_GP'] = $_POST['BSQ_GP'];
$filter['OFS_GP'] = $_POST['OFS_GP'];
$filter['BTP_GP'] = $_POST['BTP_GP'];
$filter['BSQ_TM'] = $_POST['BSQ_TM'];
$filter['EPW_TM'] = $_POST['EPW_TM'];
$filter['BTP_TM'] = $_POST['BTP_TM'];
$medal = $_POST['PresetMedalCriteria'];
$nrScopeMet = $_POST['NrScopeMet'];

## Step 2: get export data
$data = $ljy->getStudentWithMedal($filter, $medal, $nrScopeMet);

## Step 3: output excel file
$objPHPExcel = new PHPExcel();	// Create new PHPExcel object
$file_name = 'award_medal_info_'.date('Ymd').'.xlsx';
$export_subject = 'award medal information';

// Set properties
$objPHPExcel->getProperties()->setCreator("eClass")
							 ->setLastModifiedBy("eClass")
							 ->setTitle("Medal")
							 ->setSubject($export_subject)
							 ->setDescription($export_subject)
							 ->setKeywords("iPortfolio JinYing Award Medal")
							 ->setCategory("iPortfolio");

// Set default font
$objPHPExcel->getDefaultStyle()->getFont()->setName('新細明體');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);

$titleStyleAry = array('fill' => 
							array(
								'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'	=> array('argb' => 'FF7E7E7E')
							),
						'borders' => array(
							'allborders' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => 'FFFFFFFF'),
							)
						)
		 			);

// Create a first sheet
$objPHPExcel->setActiveSheetIndex(0);

$activeSheet = $objPHPExcel->getActiveSheet();

// build header row
$activeSheet->setCellValue('A1', $Lang['iPortfolio']['JinYing']['Report']['ClassName']);
$activeSheet->setCellValue('B1', $Lang['iPortfolio']['JinYing']['Report']['ClassNumber']);
$activeSheet->setCellValue('C1', $Lang['iPortfolio']['JinYing']['Report']['StudentName']);
$activeSheet->setCellValue('D1', $Lang['iPortfolio']['JinYing']['Scope']['BSQ']);
$activeSheet->setCellValue('F1', $Lang['iPortfolio']['JinYing']['Scope']['EPW']);
$activeSheet->setCellValue('G1', $Lang['iPortfolio']['JinYing']['Scope']['OFS']);
$activeSheet->setCellValue('H1', $Lang['iPortfolio']['JinYing']['Scope']['BTP']);
$activeSheet->setCellValue('J1', $Lang['iPortfolio']['JinYing']['Medal']['TargetMetScopeNumber']);
$activeSheet->setCellValue('K1', $Lang['iPortfolio']['JinYing']['Medal']['Award']);
$activeSheet->setCellValue('D2', $Lang['iPortfolio']['JinYing']['Medal']['GoodPoint']);
$activeSheet->setCellValue('E2', $Lang['iPortfolio']['JinYing']['Medal']['TargetMet']);
$activeSheet->setCellValue('F2', $Lang['iPortfolio']['JinYing']['Medal']['TargetMet']);
$activeSheet->setCellValue('G2', $Lang['iPortfolio']['JinYing']['Medal']['GoodPoint']);
$activeSheet->setCellValue('H2', $Lang['iPortfolio']['JinYing']['Medal']['GoodPoint']);
$activeSheet->setCellValue('I2', $Lang['iPortfolio']['JinYing']['Medal']['TargetMetWiGood']);

$activeSheet->mergeCells('A1:A2');
$activeSheet->mergeCells('B1:B2');
$activeSheet->mergeCells('C1:C2');
$activeSheet->mergeCells('D1:E1');
$activeSheet->mergeCells('H1:I1');
$activeSheet->mergeCells('J1:J2');
$activeSheet->mergeCells('K1:K2');

$activeSheet->getStyle('A1:C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$activeSheet->getStyle('D1:E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$activeSheet->getStyle('H1:I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$activeSheet->getStyle('J1:J2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$activeSheet->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$activeSheet->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$activeSheet->getStyle('D2:I2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$activeSheet->getStyle('A1:C2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$activeSheet->getStyle('J1:J2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$activeSheet->getStyle('K1:K2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$activeSheet->getStyle('A1:K2')->applyFromArray($titleStyleAry);
$activeSheet->getStyle('A1:K2')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);


// export data
for ($i=0,$iMax=count($data);$i<$iMax;$i++) {
	$row = $data[$i];
	$r = $i+3;		// start from 3rd row
			
	$activeSheet->setCellValue('A'.$r, $row['ClassName']);
	$activeSheet->setCellValue('B'.$r, $row['ClassNumber']);
	$activeSheet->setCellValue('C'.$r, $row['StudentName']);
	$activeSheet->setCellValue('D'.$r, $row['GoodPoint_BSQ']);
	$activeSheet->setCellValue('E'.$r, $row['TargetMet_BSQ']);
	$activeSheet->setCellValue('F'.$r, $row['TargetMet_EPW']);
	$activeSheet->setCellValue('G'.$r, $row['GoodPoint_OFS']);
	$activeSheet->setCellValue('H'.$r, $row['GoodPoint_BTP']);
	$activeSheet->setCellValue('I'.$r, $row['TargetMet_BTP']);
    $activeSheet->setCellValue('J'.$r, $row['TargetMetScopeNumber']);
	$activeSheet->setCellValue('K'.$r, ($row['Medal'] ? $Lang['iPortfolio']['JinYing']['Medal'][$row['Medal']] : '-'));

}

// freeze first two column and top row
$activeSheet->freezePane('A3');	

// Rename sheet
$activeSheet->setTitle($export_subject);


intranet_closedb();

// output file
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$file_name.'"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');

exit;

?>