<?php

// Modified by 
/*
 *  2019-12-02 [Cameron]
 *      - add case for EPW-PW and EPW-OC [case #J161484]
 *
 * 	2017-09-15 [Cameron]
 * 		- fix: should unchecked Semester for items not require semester when click export button [case #J123442]
 *  
 * 	2017-09-13 [Cameron]
 * 		- fix typo error when calling load_class_list_layout [case #J123442]
 * 
 *	2017-05-04 [Cameron]
 *		- add version checking, don't allow to access this page if php version < 5.2
 *
 * 	2017-04-25 [Cameron]
 * 		- create this file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/libpf-jinying.php");


iportfolio_auth("T");	// teacher
intranet_opendb();

if (!$sys_custom['iPf']['JinYingScheme']) {
	header("Location: /home/portfolio/");
	exit;
}

$lpf = new libportfolio();
$lfcmu = new form_class_manage_ui();

if(!$lpf->IS_IPF_SUPERADMIN() && !$_SESSION['SSV_USER_ACCESS']['other-iPortfolio']){	// only allow iPortfolio admin to access
	header("Location: /home/portfolio/");
	exit;
}

if (phpversion_compare('5.2') == 'ELDER') {
	header("Location: index.php");
	exit;
}

$AcademicYearID = $_POST['AcademicYearID'] ? $_POST['AcademicYearID'] : Get_Current_Academic_Year_ID();
$currentYearTerm = getCurrentAcademicYearAndYearTerm();
$currentTerm = $currentYearTerm['YearTermID'];
 
# Acadermic Year Selection        	
$yearFilter = getSelectAcademicYear("AcademicYearID", '', 1, 0, $AcademicYearID);


# Acadermic Term Selection
$semesterFilter = getSelectSemester2('name="Semester[]" id="Semester" multiple size="3" style="display:none"',$currentTerm, $ParQuoteValue=1, $AcademicYearID);


# Build Item List
$itemList_array = array();
$itemList_array[] = array('',$Lang['iPortfolio']['JinYing']['SelectItem']); 
foreach($Lang['iPortfolio']['JinYing']['Item'] as $scope=>$item) {
	$itemList_array[] = array('OPTGROUP',$Lang['iPortfolio']['JinYing']['Scope'][$scope]);	// (value, name)
	foreach($item as $k=>$v) {
		$code = "$scope-$k";
		$itemList_array[] = array($code,$v);	
	}
}
$itemFilter = returnSelection($itemList_array, '', 'Item', 'ID="Item"');


# Class Selection
$classFilter = $lfcmu->Get_Class_Selection($AcademicYearID, $YearID='', $ID_Name='YearClassID[]', $SelectedYearClassID='', $Onchange='', $noFirst=0, $isMultiple=1);

# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_JinYing_Scheme";

### Title ###
$ljy = new libJinYing();
$TAGS_OBJ = $ljy->getJinYingTab('JinYing');

# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['JinYing']['JinYingScheme'],"index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Export'],"");
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

?>
<style>
	.green {color:#00b33c;}
</style>

<script type="text/JavaScript" language="JavaScript">

<?php echo "var codeWithTermAry = ['".implode("','",$conf['JinYingItemCodeWithTerm'])."'];"; ?>
$(document).ready(function(){
	$('#Item').change(function(e) {
	  	e.preventDefault();
	  	$('#SemesterRow').css("display","none");
	  	if ($(this).val() != '') {
            var currentItem = $(this).val();
		  	$('#ExportBtn').css("display","");
            if (codeWithTermAry.indexOf(currentItem) != -1) {
                $('#Semester').css("display","");
                $('#SemesterRow').css("display","");
            }
            else {
                $('#Semester').css("display","none");
                $('#SemesterRow').css("display","none");
                $('#Semester option').attr('selected', false);
            }
	  	}
	});
	
	$('#AcademicYearID').change(function(e){
		e.preventDefault();
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_get_class_list.php',
			data : {
					'AcademicYearID': $(this).val(),
					'Item': $('#Item').val()
				},		  
			success: load_class_list_layout,
			error: show_ajax_error
		});
	});
	
	$('#SelectAllBtnClass').click(function() {
		$('#YearClassID\\[\\] option').attr('selected', true);  		
	});

	$('#SelectAllBtnSemester').click(function() {
		$('#Semester option').attr('selected', true);  		
	});
		
});	// end $(document).ready()

function js_export() {
    var currentItem = $('#Item').val();
	if (currentItem == '') {
		alert('<?=$Lang['iPortfolio']['JinYing']['Export']['WarningSelectItem']?>');
	}
	else if ($('#YearClassID\\[\\]').val() == '') {
		alert('<?=$Lang['iPortfolio']['JinYing']['Export']['WarningSelectClassName']?>');
	}
	else if ((($('#Semester').val() == '') || ($('#Semester').val() == null)) && (codeWithTermAry.indexOf(currentItem) != -1)) {
		alert('<?=$Lang['iPortfolio']['JinYing']['Export']['WarningSelectTerm']?>');
	}
	else {
		if (codeWithTermAry.indexOf(currentItem) != -1) {
			// do nothing
		}
		else {
			$('#Semester option').attr('selected', false);
		}
		var original_action = document.form1.action;
		var url = "export_jinying_data.php";
		document.form1.action = url;
		document.form1.submit();
		document.form1.action = original_action;
	}	
}

function load_class_list_layout(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
	  	$('#ClassFilter').html(ajaxReturn.html);
	  	$('#SemesterFilter').html(ajaxReturn.semesterFilter);
	}
}	

function show_ajax_error() {
	alert('<?=$Lang['iPortfolio']['JinYing']['ajaxError']?>');
}
	
</script>

<form name="form1" method="post" id="form1">

<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br />
	
	<table class="form_table_v30">
		<tr>
			<td class="field_title"><?=$Lang['General']['SchoolYear']?><span class="tabletextrequire">*</span></td>
			<td><?=$yearFilter?></td>
		</tr>

		<tr>
			<td class="field_title"><?=$Lang['iPortfolio']['JinYing']['ActivityItems']?><span class="tabletextrequire">*</span></td>
			<td><?=$itemFilter?><?=$Lang['iPortfolio']['JinYing']['Remark']?></td>
		</tr>

		<tr id="SemesterRow" style="display:none">
			<td class="field_title"><?=$Lang['General']['Term']?><span class="tabletextrequire">*</span></td>
			<td><span id="SemesterFilter"><?=$semesterFilter?></span>
				<?=$linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "", 'SelectAllBtnSemester');?>
				<span class="tabletextremark"><?=$Lang['iPortfolio']['JinYing']['Export']['PressCtrlKey']?></span>
			</td>
		</tr>

		<tr>
			<td class="field_title"><?=$Lang['Header']['Menu']['Class']?><span class="tabletextrequire">*</span></td>
			<td><span id="ClassFilter"><?=$classFilter?></span>
				<?=$linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "", 'SelectAllBtnClass');?>
				<span class="tabletextremark"><?=$Lang['iPortfolio']['JinYing']['Export']['PressCtrlKey']?></span>
			</td>
		</tr>
 
 		<tr>
 			<td colspan=2 class="tabletextremark"><?=$Lang['iPortfolio']['JinYing']['ImportRemarks']['Mandatory']?></td>
 		</tr>
	</table>
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "button", "js_export();", "ExportBtn")?>
		<p class="spacer"></p>
	</div>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>