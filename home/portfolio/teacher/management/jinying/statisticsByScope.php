<?php

// Modified by
/*
 * 2018-05-03 [Cameron]
 * - move export button to result page
 * - add image loading process when submit for different criteria
 * - reload page when export
 *
 * 2018-04-17 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/iportfolio_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libportfolio.php");
include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/customize/tmchkwc/libpf-jinying.php");

iportfolio_auth("T"); // teacher
intranet_opendb();

if (! $sys_custom['iPf']['JinYingScheme']) {
    header("Location: /home/portfolio/");
    exit();
}

$lpf = new libportfolio();
$lfcm = new form_class_manage();

if (! $lpf->IS_IPF_SUPERADMIN() && ! $_SESSION['SSV_USER_ACCESS']['other-iPortfolio']) { // only allow iPortfolio admin to access
    header("Location: /home/portfolio/");
    exit();
}

$AcademicYearID = $_POST['AcademicYearID'] ? $_POST['AcademicYearID'] : Get_Current_Academic_Year_ID();

// Acadermic Year Selection
$yearFilter = getSelectAcademicYear("AcademicYearID", '', 1, 0, $AcademicYearID);

// Build Item List
$scopeListAry = array();
$scopeListAry[] = array(
    '',
    $Lang['iPortfolio']['JinYing']['SelectItem']
);
foreach ($Lang['iPortfolio']['JinYing']['Scope'] as $_code => $_name) {
    $scopeListAry[] = array(
        $_code,
        $_name
    );
}
$scopeFilter = returnSelection($scopeListAry, '', 'Scope', 'ID="Scope"');

$numberAry = array();
for ($i = 0; $i <= 20; $i ++) {
    $numberAry[] = array(
        $i,
        $i
    );
}

// Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_JinYing_Scheme";

// ## Title ###
$ljy = new libJinYing();
$TAGS_OBJ = $ljy->getJinYingTab('Statistics');
$statisticsAnalysisTab = $ljy->getStatisticsAnalysisTabMenu('ByScope');

$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

?>
<style>
.green {
	color: #00b33c;
}
</style>

<script type="text/JavaScript" language="JavaScript">
    var isLoading = true;
    var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';

	function hideOptionLayer()
	{
		$('#form1').attr('style', 'display: none');
		$('.spanHideOption').attr('style', 'display: none');
		$('.spanShowOption').attr('style', '');
	}
	
	function showOptionLayer()
	{
		$('#form1').attr('style', '');
		$('.spanShowOption').attr('style', 'display: none');
		$('.spanHideOption').attr('style', '');
	}

	$(document).ready(function(){

		$('#Scope').change(function(){
			switch($(this).val()) {
				case 'BSQ':
					$('#GoodPointGold').val('<?php echo $conf['MedalRequirement']['Gold']['BSQ']['GoodPoint'];?>');
					$('#TargetMetGold').val('<?php echo $conf['MedalRequirement']['Gold']['BSQ']['TargetMet'];?>');
					$('#GoodPointSilver').val('<?php echo $conf['MedalRequirement']['Silver']['BSQ']['GoodPoint'];?>');
					$('#TargetMetSilver').val('<?php echo $conf['MedalRequirement']['Silver']['BSQ']['TargetMet'];?>');
					$('#GoodPointBronze').val('<?php echo $conf['MedalRequirement']['Bronze']['BSQ']['GoodPoint'];?>');
					$('#TargetMetBronze').val('<?php echo $conf['MedalRequirement']['Bronze']['BSQ']['TargetMet'];?>');

					$('#targetMetLable').html("<?php echo $Lang['iPortfolio']['JinYing']['Medal']['TargetMet'];?>");
    				$('#goodPointRow').css({'display':''});
    				$('#targetMetRow').css({'display':''});
    				$('#medalFilterDiv').css({'display':''});
					break;
				case 'EPW':
					$('#GoodPointGold').val('0');
					$('#TargetMetGold').val('<?php echo $conf['MedalRequirement']['Gold']['EPW']['TargetMet'];?>');
					$('#GoodPointSilver').val('0');
					$('#TargetMetSilver').val('<?php echo $conf['MedalRequirement']['Silver']['EPW']['TargetMet'];?>');
					$('#GoodPointBronze').val('0');
					$('#TargetMetBronze').val('<?php echo $conf['MedalRequirement']['Bronze']['EPW']['TargetMet'];?>');

					$('#targetMetLable').html("<?php echo $Lang['iPortfolio']['JinYing']['Medal']['TargetMet'];?>");
    				$('#goodPointRow').css({'display':'none'});
    				$('#targetMetRow').css({'display':''});
    				$('#medalFilterDiv').css({'display':''});
					break;
				case 'OFS':
					$('#GoodPointGold').val('<?php echo $conf['MedalRequirement']['Gold']['OFS']['GoodPoint'];?>');
					$('#TargetMetGold').val('0');
					$('#GoodPointSilver').val('<?php echo $conf['MedalRequirement']['Silver']['OFS']['GoodPoint'];?>');
					$('#TargetMetSilver').val('0');
					$('#GoodPointBronze').val('<?php echo $conf['MedalRequirement']['Bronze']['OFS']['GoodPoint'];?>');
					$('#TargetMetBronze').val('0');

					$('#targetMetLable').html("<?php echo $Lang['iPortfolio']['JinYing']['Medal']['TargetMet'];?>");
    				$('#goodPointRow').css({'display':''});
    				$('#targetMetRow').css({'display':'none'});
    				$('#medalFilterDiv').css({'display':''});
					break;
				case 'BTP':
					$('#GoodPointGold').val('<?php echo $conf['MedalRequirement']['Gold']['BTP']['GoodPoint'];?>');
					$('#TargetMetGold').val('<?php echo $conf['MedalRequirement']['Gold']['BTP']['TargetMet'];?>');
					$('#GoodPointSilver').val('<?php echo $conf['MedalRequirement']['Silver']['BTP']['GoodPoint'];?>');
					$('#TargetMetSilver').val('<?php echo $conf['MedalRequirement']['Silver']['BTP']['TargetMet'];?>');
					$('#GoodPointBronze').val('<?php echo $conf['MedalRequirement']['Bronze']['BTP']['GoodPoint'];?>');
					$('#TargetMetBronze').val('<?php echo $conf['MedalRequirement']['Bronze']['BTP']['TargetMet'];?>');

					$('#targetMetLable').html("<?php echo $Lang['iPortfolio']['JinYing']['Medal']['TargetMetWiGood'];?>");
    				$('#goodPointRow').css({'display':''});
    				$('#targetMetRow').css({'display':''});
    				$('#medalFilterDiv').css({'display':''});
					break;
				default:
					$('#GoodPointGold').val('0');
    				$('#TargetMetGold').val('0');
    				$('#GoodPointSilver').val('0');
    				$('#TargetMetSilver').val('0');
    				$('#GoodPointBronze').val('0');
    				$('#TargetMetBronze').val('0');
    				
    				$('#targetMetLable').html("<?php echo $Lang['iPortfolio']['JinYing']['Medal']['TargetMet'];?>");
    				$('#goodPointRow').css({'display':''});
    				$('#targetMetRow').css({'display':''});
    				$('#medalFilterDiv').css({'display':'none'});
					break;
			}				
		});
		
		$('#submit_result').click(function(){
			if ($('#Scope').val() == '') {
				alert("<?php echo $Lang['iPortfolio']['JinYing']['Statistics']['WarningSelectScope']?>");
				return;					
			}
			if ($('#GoodPointGold').val() < $('#GoodPointSilver').val()) {
				alert("<?php echo $Lang['iPortfolio']['JinYing']['Statistics']['WarningGoldLessSilverGoodPoint'];?>")
				return;
			}
			if ($('#GoodPointSilver').val() < $('#GoodPointBronze').val()) {
				alert("<?php echo $Lang['iPortfolio']['JinYing']['Statistics']['WarningSilverLessBronzeGoodPoint'];?>")
				return;
			} 			
			if ($('#TargetMetGold').val() < $('#TargetMetSilver').val()) {
				alert("<?php echo $Lang['iPortfolio']['JinYing']['Statistics']['WarningGoldLessSilverTargetMet'];?>")
				return;
			}
			if ($('#TargetMetSilver').val() < $('#TargetMetBronze').val()) {
				alert("<?php echo $Lang['iPortfolio']['JinYing']['Statistics']['WarningSilverLessBronzeTargetMet'];?>")
				return;
			} 			

	        isLoading = true;
			$('#form_result').html(loadingImg);
			
			$.ajax({
				dataType: "json",
				type: "POST",
				url: 'ajax_get_scope_result.php',
				data : $("#form1").serialize(),		  
				success: load_scope_result_layout,
				error: show_ajax_error
			});
		});
				
	});

	function load_scope_result_layout(ajaxReturn) {
		if (ajaxReturn != null && ajaxReturn.success){
//			$('#form1').attr('style','display: none');
			$('#form_result').html(ajaxReturn.html);
// 			$('#spanShowOption').show();
// 			$('#spanHideOption').hide();
			showOptionLayer();
			$('#report_show_option').addClass("report_option report_hide_option");
			isLoading = false;
//			window.scrollTo(0, 0);
		}
	}	
	
	function show_ajax_error() {
		alert('<?=$Lang['iPortfolio']['JinYing']['ajaxError']?>');
	}
	
	function js_export() {
		var original_action = document.form1.action;
		var url = "export_scope_statistics.php";
		document.form1.action = url;
		document.form1.submit();
		document.form1.action = original_action;
		$('#submit_result').click();	
	}

</script>

<div>
  <?php echo $statisticsAnalysisTab;?>
</div>

<div id="report_show_option">
	<span id="spanShowOption" class="spanShowOption" style="display: none">
		<a href="javascript:showOptionLayer();"><?=$Lang['iPortfolio']['JinYing']['Medal']['ShowOption']?></a>
	</span> <span id="spanHideOption" class="spanHideOption"
		style="display: none"> <a href="javascript:hideOptionLayer();"><?=$Lang['iPortfolio']['JinYing']['Medal']['HideOption']?></a>
	</span>

	<form name="form1" method="post" id="form1">

		<table class="form_table_v30">
			<tr>
				<td class="field_title"><?=$Lang['General']['SchoolYear']?><span
					class=tabletextrequire>*</span></td>
				<td><?=$yearFilter?></td>
			</tr>

			<tr>
				<td class="field_title"><?=$Lang['iPortfolio']['JinYing']['ScopeName']?><span
					class=tabletextrequire>*</span></td>
				<td><?=$scopeFilter?>
					<div id="medalFilterDiv" style="display: none;">
						<table style="border-collapse: collapse;">
							<tr>
								<td></td>
								<td><?php echo $Lang['iPortfolio']['JinYing']['Medal']['Gold'];?></td>
								<td><?php echo $Lang['iPortfolio']['JinYing']['Medal']['Silver'];?></td>
								<td><?php echo $Lang['iPortfolio']['JinYing']['Medal']['Bronze'];?></td>
							</tr>
							<tr id="goodPointRow">
								<td id="goodPointLable"><?php echo $Lang['iPortfolio']['JinYing']['Medal']['GoodPoint'];?></td>
								<td><?php echo getSelectByArray($numberAry, 'name="GoodPointGold" id="GoodPointGold"');?></td>
								<td><?php echo getSelectByArray($numberAry, 'name="GoodPointSilver" id="GoodPointSilver"');?></td>
								<td><?php echo getSelectByArray($numberAry, 'name="GoodPointBronze" id="GoodPointBronze"').$Lang['iPortfolio']['JinYing']['Medal']['Point'].$Lang['iPortfolio']['JinYing']['Medal']['OrAbove'];?></td>
							</tr>
							<tr id="targetMetRow">
								<td id="targetMetLable"><?php echo $Lang['iPortfolio']['JinYing']['Medal']['TargetMet'];?></td>
								<td><?php echo getSelectByArray($numberAry, 'name="TargetMetGold" id="TargetMetGold"');?></td>
								<td><?php echo getSelectByArray($numberAry, 'name="TargetMetSilver" id="TargetMetSilver"');?></td>
								<td><?php echo getSelectByArray($numberAry, 'name="TargetMetBronze" id="TargetMetBronze"').$Lang['iPortfolio']['JinYing']['Medal']['Times'].$Lang['iPortfolio']['JinYing']['Medal']['OrAbove'];?></td>
							</tr>
						</table>
					</div></td>
			</tr>

			<tr>
				<td colspan=2 class="tabletextremark"><?php echo $Lang['iPortfolio']['JinYing']['ImportRemarks']['Mandatory'];?></td>
			</tr>

		</table>

		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "", "submit_result")?>&nbsp;
		</div>
	</form>
</div>
<div id="form_result"></div>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>