<?php
/*
 * 	Log
 * 	
 * 	Purpose: return import layout according to selected item
 *
 *  Date:   2019-12-03 [Cameron]
 *          - handle export column title for EPW-PW and EPW-OC
 *          - handle export column property for EPW-PW, EPW-OC, BTP-IS, BTP-TP
 *
 *  Date:   2019-12-02 [Cameron]
 *          - add sample file linking for EPW-PW, EPW-OC, BTP-IS, BTP-TP
 *
 *	Date:	2017-09-13 [Cameron]
 *			- add OrganizationNameEng & OrganizationNameChi for BTP-IC form 
 * 
 *	Date:	2017-08-28 [Cameron]
 *			- change the column position because of adding WebSAMS column next to class number in excel
 *
 *	Date:	2017-04-28 [Cameron]
 *			- add update / delete function
 *
 * 	Date:	2016-11-23 [Cameron]
 * 			create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/json.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio.php");
iportfolio_auth("T");	// teacher

$json['success'] = false;
if (!$sys_custom['iPf']['JinYingScheme']) {
	exit;
}

//$lpf = new libportfolio();
//if(!$lpf->IS_IPF_SUPERADMIN() && !$_SESSION['SSV_USER_ACCESS']['other-iPortfolio']){	// only allow iPortfolio admin to access
//	exit;
//}

$Item = $_POST['Item'];
if (!$Item) {
	return '';	
}

$ActionType = $_POST['ActionType'];
$ActionSubtype = $_POST['ActionSubtype'];

list($scope,$item) = explode('-',$Item);

$sample_file_name = "sample/";
if ($ActionType == '2') {		// update / delete
	$sample_file_name .= "edit/";
	$suffix = "_2";
}
else {
	$suffix = "";
}

switch($Item) {
	case "BSQ-HW":
		$sample_file_name .= "hand_in_all_homework".$suffix.".xlsx";
		break;
	case "BSQ-PC":
		$sample_file_name .= "punctuality".$suffix.".xlsx";	
		break;
	case "BSQ-AD":
		$sample_file_name .= "attendance".$suffix.".xlsx";		
		break;
	case "BSQ-AI":
		$sample_file_name .= "attire".$suffix.".xlsx";	
		break;
	case "BSQ-RC":
		$sample_file_name .= "extensive_reading_chinese".$suffix.".xlsx";
		break;
	case "BSQ-RE":
		$sample_file_name .= "extensive_reading_english".$suffix.".xlsx";
		break;
	case "BSQ-EC":
		$sample_file_name .= "excursion".$suffix.".xlsx";
		break;
	case "BSQ-TA":
		$sample_file_name .= "bsq_training".$suffix.".xlsx";
		break;
	case "EPW-PY":
		$sample_file_name .= "physique".$suffix.".xlsx";
		break;
	case "EPW-PJ":
		$sample_file_name .= "physical_agility_jogging".$suffix.".xlsx";
		break;
	case "EPW-PS":
		$sample_file_name .= "physical_agility_swimming".$suffix.".xlsx";
		break;
    case "EPW-PW":
        $sample_file_name .= "physical_well_being".$suffix.".xlsx";
        break;
    case "EPW-CA":
        $sample_file_name .= "participation_in_christian_activities".$suffix.".xlsx";
        break;
	case "EPW-TA":
		$sample_file_name .= "epw_training".$suffix.".xlsx";
		break;
    case "EPW-OC":
        $sample_file_name .= "overall_comment".$suffix.".xlsx";
        break;
	case "OFS-SI":
		$sample_file_name .= "services_in_school".$suffix.".xlsx";
		break;
	case "OFS-SO":
		$sample_file_name .= "services_outside_school".$suffix.".xlsx";
		break;
	case "BTP-EA":
		$sample_file_name .= "participation_in_extracurricular_activities".$suffix.".xlsx";
		break;
    case "BTP-IS":
        $sample_file_name .= "participation_in_school_competitions".$suffix.".xlsx";
        break;
	case "BTP-IC":
		$sample_file_name .= "participation_in_external_competitions".$suffix.".xlsx";
		break;
    case "BTP-TP":
        $sample_file_name .= "talent_performance".$suffix.".xlsx";
        break;
}

# import column explanation and remarks
$excel_format = "";
$delim = "<br>";


$specific_code = array('BTP-EA','BTP-IC','OFS-SI','OFS-SO');	// columns titles are different to each other 
$with_activity_code = array('BSQ-EC','BSQ-TA','EPW-TA');
 
$colTitle = array();
if (in_array($Item,$specific_code)) {
	if ($Lang['iPortfolio']['JinYing']['ImportColumns'][$Item]) {
		$colTitle = array_values($Lang['iPortfolio']['JinYing']['ImportColumns'][$Item]);
	}
}
else if (in_array($Item,$with_activity_code)) {
	if ($Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']) {
		$colTitle = array_values($Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']);
	}
}
else {
	if ($Lang['iPortfolio']['JinYing']['ImportColumns']['STANDARD']) {
		$colTitle = array_values($Lang['iPortfolio']['JinYing']['ImportColumns']['STANDARD']);
	}
}

if ($ActionType == '2') {		// update / delete, contain extra columns 
	switch($Item) {
		case 'BSQ-EC':
		case 'BSQ-TA':
		case 'EPW-TA':
			$columnCode = 'WITH-ACTIVITY-DATE';
			break;
		case 'EPW-PY':
		case 'EPW-PJ':
		case 'EPW-PS':
        case 'BTP-TP':
        case 'BTP-IS':
			$columnCode = 'PHYSICAL';
			break;
        case 'EPW-PW':
        case 'EPW-OC':
            $columnCode = 'STANDARD-WO-AWARD';
            break;
		case 'OFS-SI':
		case 'OFS-SO':
		case 'BTP-EA':
		case 'BTP-IC':
			$columnCode = $Item;
			break;
		default:
			$columnCode = 'STANDARD';
			break;
	}
	$updateColTitle = array_values($Lang['iPortfolio']['JinYing']['ImportColumns4Update'][$columnCode]);
	$colTitle = array_merge($colTitle,$updateColTitle);
}


$colRemark = array();
$colProperty = array();
$remark_array = array();
for($i=0, $iMax=count($colTitle); $i<$iMax; $i++){
	$colRemark[$i] = '';		// initialize column remark
	$colProperty[$i] = 0;
}

$colRemark[0] = '<span class="tabletextremark">('.$Lang['iPortfolio']['JinYing']['ImportRemarks']['FillInEnglish'].')</span>';
$colProperty[2] = 1;	// 1 - mandatory
$colProperty[3] = 1;

// set insert (default) column property and remark
switch($Item) {
	case 'BTP-EA':
	case 'OFS-SI':
		$colProperty[4] = 2;	// 2 - reference
		$colProperty[5] = 2;
		$colProperty[6] = 1;
		$remark_array = $Lang['iPortfolio']['JinYing']['ImportRemarks'][$Item];
		break;

	case 'BSQ-EC':
	case 'BSQ-TA':
	case 'EPW-TA':
		$colProperty[5] = 2;	
		$colProperty[6] = 2;
		$colProperty[7] = 1;
		$remark_array = $Lang['iPortfolio']['JinYing']['ImportRemarks']['ACTIVITY'];
		break;
	case 'BTP-IC':
		$colProperty[5] = 2;	
		$colProperty[6] = 2;
		$colProperty[9] = 1;
		$remark_array = $Lang['iPortfolio']['JinYing']['ImportRemarks']['ACTIVITY'];
		break;

	default:
		$colProperty[4] = 1;
		$remark_array = $Lang['iPortfolio']['JinYing']['ImportRemarks']['STANDARD'];
		break;
}

if ($ActionType == '2') {		// update / delete
	$colProperty[2] = 0;		// remvoe mandatory
	$colProperty[3] = 0;
	
	switch($Item) {
		case 'BSQ-EC':
		case 'BSQ-TA':
		case 'EPW-TA':
		case 'BTP-EA':
			$colProperty[9] = 1;
			break;
		case 'EPW-PY':
		case 'EPW-PJ':
		case 'EPW-PS':
        case 'EPW-PW':
        case 'EPW-OC':
        case 'BTP-IS':
        case 'BTP-TP':
			$colProperty[7] = 1;
			break;
		case 'OFS-SI':
			$colProperty[10] = 1;
			break;		
		case 'OFS-SO':
			$colProperty[6] = 1;		
		case 'BTP-IC':
			$colProperty[15] = 1;
			break;
		default:
			$colProperty[8] = 1;
			break;
	}
	
	if ($ActionSubtype == '2') {	// delete
		switch($Item) {
			case 'BSQ-EC':
			case 'BSQ-TA':
			case 'EPW-TA':
				$colProperty[5] = 0;
				$colProperty[6] = 0;
				$colProperty[7] = 0;
				break;
			case 'BTP-IC':
				$colProperty[5] = 0;
				$colProperty[6] = 0;
				$colProperty[9] = 0;
				break;
			case 'BTP-EA':
			case 'OFS-SI':
            case 'BTP-IS':
            case 'BTP-TP':
				$colProperty[4] = 0;
				$colProperty[5] = 0;
				$colProperty[6] = 0;				
				break;
			default:
				$colProperty[4] = 0;
				break;
		}
		$remark_array = $Lang['iPortfolio']['JinYing']['ImportRemarks']['STANDARD'];		
	}

}

$linterface = new interface_html();
$excel_format = $linterface->Get_Import_Page_Column_Display($colTitle, $colProperty, $colRemark);

$excel_remarks = "";
for($i=0, $iMax=count($remark_array); $i<$iMax; $i++){
	if($i!=0) $excel_remarks .= $delim;
	$excel_remarks .= $remark_array[$i];
}

# import file interface
$x = '';
$x .= '<table class="form_table_v30" id="import_table">'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$i_select_file.' <span class="tabletextremark">('.$Lang['iPortfolio']['JinYing']['Import']['FileFormat'].')</span><span class="tabletextrequire">*</span></td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<input class="file" type="file" name="userfile">'."\r\n";
			$x .= '<br><br>'."\r\n";
			$x .= '<a class="tablelink" href="download.php?FileName_e='.getEncryptedText($sample_file_name).'" target="_self">'."\r\n";
				$x .= '<img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/icon_files/xls.gif" border="0" align="absmiddle" hspace="3">'."\r\n";
				$x .= $import_csv['download']."\r\n";
			$x .= '</a>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	$x .= '<tr>'."\r\n";			
		$x .= '<td class="field_title">'.$Lang['General']['ImportArr']['DataColumn'].'</td>'."\r\n";
		$x .= '<td>'.$excel_format."</td>\r\n";
	$x .= '</tr>'."\r\n";
	
	$x .= '<tr>'."\r\n";		
		$x .= '<td colspan=2 class="tabletextremark">'.$excel_remarks."</td>\r\n";
	$x .= '</tr>'."\r\n";		
	
$x .= '</table>'."\r\n";

$json['success'] = true;
$x = remove_dummy_chars_for_json($x);
$json['html'] = $x;

$jsonObj = new JSON_obj();
echo $jsonObj->encode($json);

?>