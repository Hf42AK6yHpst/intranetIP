<?php
/*
 * 	Log
 * 	
 * 	Purpose: save jinying scheme data
 * 
 * 	Default current academic year
 *
 *	Date:	2017-09-13 [Cameron]
 *			add two fields: OrganizationNameEng & OrganizationNameChi
 *
 * 	Date:	2016-11-24 [Cameron]
 * 			create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/libpf-jinying.php");

iportfolio_auth("T");	// teacher
intranet_opendb();

if (!$sys_custom['iPf']['JinYingScheme']) {
	header("Location: /home/portfolio/");
	exit;
}


$lpf = new libportfolio();
if(!$lpf->IS_IPF_SUPERADMIN() && !$_SESSION['SSV_USER_ACCESS']['other-iPortfolio']){	// only allow iPortfolio admin to access
	header("Location: /home/portfolio/");
	exit;
}

# start processing data
$successAry = array();

### retrieve import data
$temp_table = $eclass_db.'.PORTFOLIO_JINYING_SCHEME_IMPORT';
$table_name = $eclass_db.'.PORTFOLIO_JINYING_SCHEME';
$sql = "Select * From $temp_table Where BatchID = '".$_SESSION['UserID']."'";
$importDataAry = $lpf->returnResultSet($sql);
$numOfImportData = count($importDataAry);
$insertAry = array();	
for ($i=0; $i<$numOfImportData; $i++) {
	$r = $importDataAry[$i];
	$insertAry[] = " ('".
		$lpf->Get_Safe_Sql_Query($r['UserID'])."', '".
		$lpf->Get_Safe_Sql_Query($r['AcademicYearID'])."', '".
		$lpf->Get_Safe_Sql_Query($r['YearTermID'])."', '".
		$lpf->Get_Safe_Sql_Query($r['PerformanceCode'])."', '".
		$lpf->Get_Safe_Sql_Query($r['ActivityDate'])."', '".
		$lpf->Get_Safe_Sql_Query($r['ActivityNameEng'])."', '".
		$lpf->Get_Safe_Sql_Query($r['ActivityNameChi'])."', '".
		$lpf->Get_Safe_Sql_Query($r['AwardNameEng'])."', '".
		$lpf->Get_Safe_Sql_Query($r['AwardNameChi'])."', '".
		$lpf->Get_Safe_Sql_Query($r['RecommendMerit'])."', '".
		$lpf->Get_Safe_Sql_Query($r['OrganizationNameEng'])."', '".
		$lpf->Get_Safe_Sql_Query($r['OrganizationNameChi'])."', '".
		$lpf->Get_Safe_Sql_Query($r['ServiceHours'])."', ".
		"NOW(), '".$_SESSION['UserID']."')";
}

if (count($insertAry) > 0) {
	$field = "UserID,AcademicYearID,YearTermID,PerformanceCode,ActivityDate,ActivityNameEng,ActivityNameChi,AwardNameEng,AwardNameChi,RecommendMerit,OrganizationNameEng,OrganizationNameChi,ServiceHours,InputDate,InputBy";
	$insertChunkAry = array_chunk($insertAry, 1000);
	$numOfChunk = count($insertChunkAry);
	
	$lpf->Start_Trans();
	for ($i=0; $i<$numOfChunk; $i++) {
		$_insertAry = $insertChunkAry[$i];
		
		$sql = "INSERT INTO ".$table_name." ($field) VALUES ".implode(', ', $_insertAry);
		$successAry[] = $lpf->db_db_query($sql);
	}

	if (!in_array(false,$successAry)) {
		$lpf->Commit_Trans();
	}
	else {
		$lpf->RollBack_Trans();
		$numOfImportData = 0;
	}
}
unset($insertAry);

# Page heading setting
$linterface = new interface_html();

# tag information	
$CurrentPage = "Teacher_JinYing_Scheme";

$ljy = new libJinYing();
$TAGS_OBJ = $ljy->getJinYingTab('JinYing');

$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION = $ljy->getImportPageNavigation();
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

# handle return message
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);


$AcademicYearID = $_POST['AcademicYearID'] ? $_POST['AcademicYearID'] : Get_Current_Academic_Year_ID(); 
$Item = $_POST['Item'];		// ItemCode
$Semester = $_POST['Semester'];

list($scope,$item) = explode('-',$Item);
if ($Semester) {
	$semesters = getSemesters($AcademicYearID);
	$termName = ' ( '.$semesters[$Semester] . ' )';
}
else {
	$termName = '';
}

$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['iPortfolio']['JinYing']['Item'][$scope][$item].$termName.'</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['importInfoTbl'] = $x;


# result display
$htmlAry['numOFSuccessDisplay'] = $numOfImportData.' '.$Lang['iPortfolio']['JinYing']['Import']['Successful'];


### step display
$htmlAry['generalImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep=3,array_values($Lang['iPortfolio']['JinYing']['ImportStepArr']));

### action buttons
$htmlAry['backBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");


?>
<script type="text/JavaScript" language="JavaScript">

function goBack() {
	window.location = 'import_jinying.php';
}

</script>
<form id="form1" name="form1" method="POST">
<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br />
	
	<?=$htmlAry['generalImportStepTbl']?>
	
	<div class="table_board" style="width:100%;">
		<?=$htmlAry['importInfoTbl']?>
		<br style="clear:both;" />
		<br style="clear:both;" />
		
		<div style="width:100%; text-align:center;">
			<?=$htmlAry['numOFSuccessDisplay']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>