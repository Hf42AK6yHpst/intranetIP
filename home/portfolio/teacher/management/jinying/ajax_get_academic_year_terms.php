<?php
/*
 * Log
 *
 * Date: 2019-12-03 [Cameron]
 *  - modify logic related to $conf['JinYingItemCodeWithTerm'] as the codes cover scope [case #J161484]
 *
 * Date: 2018-04-19 [Cameron]
 * create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/iportfolio_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
include_once ($PATH_WRT_ROOT . "includes/form_class_manage_ui.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/customize/tmchkwc/config.php");

include_once ($PATH_WRT_ROOT . "includes/json.php");
iportfolio_auth("T"); // teacher
intranet_opendb();

$json['success'] = false;
if (! $sys_custom['iPf']['JinYingScheme']) {
    exit();
}

$AcademicYearID = $_POST['AcademicYearID'];
if (! $AcademicYearID) {
    return '';
}

$currentYearTerm = getCurrentAcademicYearAndYearTerm();
if ($currentYearTerm['AcademicYearID'] == $AcademicYearID) { // current academic year
    $selectYearTermID = $currentYearTerm['YearTermID'];
} else {
    $terms = getAllSemesterByYearID($AcademicYearID);
    $selectYearTermID = count($terms) ? $terms[0]['YearTermID'] : 0;
}

$semesterCss = 'none';
if ($Item) {
    list ($scope, $item) = explode('-', $Item);
    $itemName = $Lang['iPortfolio']['JinYing']['Item'][$scope][$item];
    if (in_array($Item, $conf['JinYingItemCodeWithTerm'])) {
        $semesterCss = '';
    }
}

// Acadermic Term Selection
$x = getSelectSemester2('name="Semester" id="Semester" style="' . $semesterCss . '"', $selectYearTermID, $ParQuoteValue = 1, $AcademicYearID);

$json['success'] = true;
$x = remove_dummy_chars_for_json($x);
$json['html'] = $x;

$jsonObj = new JSON_obj();
echo $jsonObj->encode($json);

intranet_closedb();
?>