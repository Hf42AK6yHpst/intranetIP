<?php

// Modified by 
/*
 *  2019-12-03 [Cameron]
 *      - modify logic related to $conf['JinYingItemCodeWithTerm'] as the codes cover scope [case #J161484]
 * 
 * 	2017-08-10 [Cameron]
 * 		- fix bug for getting correct TermID when change AcademicYear [case #J121691]
 * 
 *	2017-05-04 [Cameron]
 *		- add version checking, don't show import / export button if php version < 5.2
 *
 * 	2017-04-27 [Cameron]
 * 		- set default term to current term
 * 
 * 	2017-04-25 [Cameron]
 * 		- add Export button
 * 
 * 	2016-11-18 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/libpf-jinying.php");


iportfolio_auth("T");	// teacher
intranet_opendb();

if (!$sys_custom['iPf']['JinYingScheme']) {
	header("Location: /home/portfolio/");
	exit;
}

$lpf = new libportfolio();
$lfcm = new form_class_manage();

if(!$lpf->IS_IPF_SUPERADMIN() && !$_SESSION['SSV_USER_ACCESS']['other-iPortfolio']){	// only allow iPortfolio admin to access
	header("Location: /home/portfolio/");
	exit;
}

$currentAcademicYearID = Get_Current_Academic_Year_ID();
$AcademicYearID = $_POST['AcademicYearID'] ? $_POST['AcademicYearID'] : $currentAcademicYearID;
$currentYearTerm = getCurrentAcademicYearAndYearTerm();
$currentTerm = $currentYearTerm['YearTermID'];

$terms = getAllSemesterByYearID($AcademicYearID);

if ($AcademicYearID == $currentAcademicYearID) {
	$defaultTermID = $currentTerm;
	unset($currentTerm);  
}
else {
	$defaultTermID = $terms ? $terms[0]['YearTermID'] : 0;
}

$Semester = $_POST['Semester'] ? $_POST['Semester'] : (count($terms) ? $defaultTermID : 0);

# Acadermic Year Selection        	
$yearFilter = getSelectAcademicYear("AcademicYearID", 'onChange="document.form1.Semester.value=0;document.form1.action=\'index.php\';document.form1.submit();"', 1, 0, $AcademicYearID);
# Acadermic Term Selection
$semesterFilter = getSelectSemester2('name="Semester" id="Semester" onChange="document.form1.action=\'index.php\';document.form1.submit();"',$Semester, $ParQuoteValue=1, $AcademicYearID);

$classTitle = Get_Lang_Selection('ClassTitleB5','ClassTitleEN');

$classList = $lfcm->Get_All_Year_Class($AcademicYearID);
$classList = BuildMultiKeyAssoc($classList,$classTitle,$classTitle,1);

$result = array();
$itemList_array = array();

foreach($Lang['iPortfolio']['JinYing']['Item'] as $scope=>$item) {
	foreach($item as $k=>$v) {
		$code = "$scope-$k";
		$itemList_array[$code] = $v;
		$termCond = in_array($code,$conf['JinYingItemCodeWithTerm']) ? "AND p.YearTermID='".$Semester."'" : "";
		$sql = "SELECT $classTitle AS ClassName, COUNT(DISTINCT p.UserID) AS NrRec 
				FROM 	YEAR_CLASS yc
				LEFT JOIN YEAR_CLASS_USER ycu ON ycu.YearClassID=yc.YearClassID
				LEFT JOIN {$eclass_db}.PORTFOLIO_JINYING_SCHEME p  
						ON p.UserID=ycu.UserID 
						".$termCond." 
						AND p.PerformanceCode LIKE '".$code."%'
						AND p.AcademicYearID='".$AcademicYearID."' 
				WHERE yc.AcademicYearID='".$AcademicYearID."'
				GROUP BY yc.YearClassID";
		$rs = $lpf->returnResultSet($sql);

		if (count($rs)) {
			foreach($rs as $r) {
				$result[$r['ClassName']][$code] = $r['NrRec'];
			}
		}
		
	}
}


# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_JinYing_Scheme";
$CurrentPageName = $Lang['iPortfolio']['JinYing']['JinYingScheme'];
### Title ###
$ljy = new libJinYing();
$TAGS_OBJ = $ljy->getJinYingTab('JinYing');

$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

//$btnAry[] = array('import', 'import_jinying.php');
//$deleteBtn = '<div class="conntenttool"><a href="delete_jinying.php" style="background-position: 0px -340px;">'.$button_delete.'</a></div>';
//$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);
if (phpversion_compare('5.2') != 'ELDER') {
	$splitter = ' | ';
	$importBtn = '<a style="float:left" href="import_jinying.php" class="contenttool"><img src="/images/'.$LAYOUT_SKIN.'/icon_import.gif" hspace="4" border="0" align="absmiddle">'.$Lang['Btn']['Import'].$splitter.'</a>';
	$exportBtn = '<a style="float:left" href="export_jinying.php" class="contenttool"><img src="/images/'.$LAYOUT_SKIN.'/icon_export.gif" hspace="4" border="0" align="absmiddle">'.$Lang['Btn']['Export'].'</a>';	
}
else {
	$splitter = '';
	$importBtn = '';
	$exportBtn = '';
}
$deleteBtn = '<a style="float:left" href="delete_jinying.php" class="contenttool"><img src="/images/'.$LAYOUT_SKIN.'/icon_delete.gif" hspace="4" border="0" align="absmiddle">'.$Lang['Btn']['Delete'].$splitter.'</a>';


?>

<form name="form1" method="POST">
	<div class="content_top_tool">
		<div><?=$importBtn . $deleteBtn . $exportBtn?></div><br>
		<br style="clear:both" />
	</div>

	<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom">
				<div class="table_filter">
					<?=$yearFilter?>
					<?= $semesterFilter ?>
				</div> 
			</td>
		</tr>
	</table>
	</div>

<table width="98%" border="0" cellpadding="0" cellspacing="0" class='common_table_list'>
	<tr>
		<td></td>
<? foreach((array)$itemList_array as $item):?>			
		<th class="sub_row_top"><?=$item?></th>
<? endforeach;?>		
	</tr>

<? foreach((array)$classList as $className):?>		
	<tr>
		<td><?=$className?></td>
	<? foreach((array)$itemList_array as $k=>$v):?>
		<td><?=$result[$className][$k]?></td>
	<? endforeach;?>
	</tr>		
<? endforeach;?>
</table>
<div><?=$Lang['iPortfolio']['JinYing']['Remark']?></div>	
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>