<?php
/*
 * Using: 
 *
 * Purpose: return statistics analysis result by item
 *
 * Date: 2019-12-12 [Cameron]
 * - add function getPerformanceTableGoodPassFailNotImport(), getPerformanceTableGoodPassNoData()
 * - add case EPW-PW, EPW-OC, BTP-IS, BTP-TP
 *
 * Date: 2018-05-03 [Cameron]
 * - add export button in this page
 * - add separate line before the row for junior total
 *
 * Date: 2018-04-26 [Cameron]
 * - fix swap column of good and pass in getPerformanceTableGoodPassFail()
 *
 * Date: 2018-04-18 [Cameron]
 * create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/iportfolio_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/json.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/customize/tmchkwc/config.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/customize/tmchkwc/libpf-jinying.php");

iportfolio_auth("T"); // teacher
intranet_opendb();

$json['success'] = false;
if (! $sys_custom['iPf']['JinYingScheme']) {
    exit();
}

$AcademicYearID = $_POST['AcademicYearID'];
if (! $AcademicYearID) {
    return '';
}
$item = $_POST['Item'];
if (! $item) {
    return '';
}
$semester = $_POST['Semester'];

$ljy = new libJinYing();

$ljy->setAcademicYearID($AcademicYearID);
$classLevelAry = $ljy->getAllClassLevel();
if (count($classLevelAry)) {
    $ljy->setClassLevel(implode(',', $classLevelAry)); // set ClassLevel because some functions use it in $ljy, e.g. setGoodPoint_BSQ
}

$academic_start_year = substr(getStartDateOfAcademicYear($AcademicYearID), 0, 4);
$academic_end_year = substr(getEndDateOfAcademicYear($AcademicYearID), 0, 4);

$stat = $ljy->getMedalStatByItem($item, $semester);
list ($scope, $subItem) = explode('-', $item);

$itemName = str_replace('#', '', $Lang['iPortfolio']['JinYing']['Item'][$scope][$subItem]);
if ($semester) {
    $semesterName = $ljy->getSemesterName($semester);
} else {
    $semesterName = '';
}

switch ($item) {
    
    case 'BSQ-HW':
    case 'BSQ-PC':
        $itemName .= '-' . $semesterName;
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $x = getPerformanceTableGoodPassFail($title, $stat);
        break;
    
    case 'BSQ-AD':
        $itemName .= '-' . $semesterName;
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $x = getPerformanceTableGoodFail($title, $stat);
        break;
    
    case 'BSQ-AI':
    case 'BSQ-RC':
    case 'BSQ-RE':
    case 'EPW-CA':
    case 'EPW-OC':
        $itemName .= '-' . $semesterName;
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $x = getPerformanceTablePassFail($title, $stat);
        break;
    
    case 'EPW-PY':
    case 'EPW-PJ':
    case 'EPW-PS':
    case 'BTP-TP':
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $x = getPerformanceTablePassFail($title, $stat);
        break;
    
    case 'BSQ-EC':
    case 'BSQ-TA':
    case 'EPW-TA':
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $x = getPerformanceTableGoodPassFailNotJoin($title, $stat);
        break;

    case 'EPW-PW':
        $itemName .= '-' . $semesterName;
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $x = getPerformanceTableGoodPassFailNotImport($title, $stat);
        break;

    case 'OFS-SI':
    case 'OFS-SO':
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $x = getPerformanceTableService($title, $stat);
        break;
    
    case 'BTP-EA':
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $x = getPerformanceTableExtracurricularActivities($title, $stat);
        break;
    
    case 'BTP-IC':
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $x = getPerformanceTableExternalCompetitions($title, $stat);
        break;

    case 'BTP-IS':
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $x = getPerformanceTableGoodPassNoData($title, $stat);
        break;
}

$json['success'] = true;
$x = remove_dummy_chars_for_json($x);
$json['html'] = $x;

$jsonObj = new JSON_obj();
echo $jsonObj->encode($json);

intranet_closedb();

// ############################ start functions
function getExportButton()
{
    global $Lang;
    $x = '
    <div id="toolbox" class="content_top_tool">
    <div class="Conntent_tool">
    <a href="javascript:js_export();" class="export">' . $Lang['Btn']['Export'] . '</a>
    </div>
    <br style="clear:both" />
    </div>';
    return $x;
}

function getPerformanceTableGoodPassFail($title, $stat)
{
    global $Lang;
    
    if ($stat['junior']['NumNoData'] > 0 || $stat['high']['NumNoData'] > 0) {
        $isNoData = true; // whether there's student that hasn't imported data or not
        $extraColumnTitleRow1 = '<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . '</th>';
        $extraColumnTitleRow2 = '<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
                <th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>';
    } else {
        $isNoData = false;
    }
    $x = '
    <style>
    	.align_center {text-align:center !important;}
    </style>
    <form name="form2" id="form2" method="POST">
        ' . getExportButton() . '
    	<div style="font-weight:bold; text-align:center;">' . $title . '</div><br>
    	<table width="98%" border="0" cellpadding="0" cellspacing="0" class="common_table_list">
    		<tr>
    			<th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Form'] . '</th>
                <th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Total'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeMerit'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['MeritExcludeTargetMet'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . '</th>' . $extraColumnTitleRow1 . '
    		</tr>
    		<tr>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>' . $extraColumnTitleRow2 . '
    		</tr>';
    
    foreach ((array) $stat as $r => $v) {
        $rowClass = ($r == 'junior') ? 'total_row' : '';
        $x .= '	<tr class="' . $rowClass . '"><td class="align_center">' . $v['Name'] . '</td>
                        <td class="align_center">' . $v['NumStudent'] . '</td>
    					<td class="align_center">' . $v['NumPass'] . '</td>
    					<td class="align_center">' . $v['PercentPass'] . '</td>
    					<td class="align_center">' . $v['NumGood'] . '</td>
    					<td class="align_center">' . $v['PercentGood'] . '</td>
    					<td class="align_center">' . $v['NumFail'] . '</td>
    					<td class="align_center">' . $v['PercentFail'] . '</td>';
        if ($isNoData) {
            $x .= '<td class="align_center">' . $v['NumNoData'] . '</td>
    					<td class="align_center">' . $v['PercentNoData'] . '</td>';
        }
        $x .= ' </tr>';
    }
    $x .= '</table>
    
    </form>';
    
    return $x;
}

function getPerformanceTableGoodFail($title, $stat)
{
    global $Lang;
    
    if ($stat['junior']['NumNoData'] > 0 || $stat['high']['NumNoData'] > 0) {
        $isNoData = true; // whether there's student that hasn't imported data or not
        $extraColumnTitleRow1 = '<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . '</th>';
        $extraColumnTitleRow2 = '<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
                <th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>';
    } else {
        $isNoData = false;
    }
    $x = '
    <style>
    	.align_center {text-align:center !important;}
    </style>
    <form name="form2" id="form2" method="POST">
        ' . getExportButton() . '
    	<div style="font-weight:bold; text-align:center;">' . $title . '</div><br>
    	<table width="98%" border="0" cellpadding="0" cellspacing="0" class="common_table_list">
    		<tr>
    			<th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Form'] . '</th>
                <th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Total'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Merit'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . '</th>' . $extraColumnTitleRow1 . '
    		</tr>
    		<tr>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>' . $extraColumnTitleRow2 . '
    		</tr>';
    
    foreach ((array) $stat as $r => $v) {
        $rowClass = ($r == 'junior') ? 'total_row' : '';
        $x .= '	<tr class="' . $rowClass . '"><td class="align_center">' . $v['Name'] . '</td>
                        <td class="align_center">' . $v['NumStudent'] . '</td>
    					<td class="align_center">' . $v['NumGood'] . '</td>
    					<td class="align_center">' . $v['PercentGood'] . '</td>
    					<td class="align_center">' . $v['NumFail'] . '</td>
    					<td class="align_center">' . $v['PercentFail'] . '</td>';
        if ($isNoData) {
            $x .= '<td class="align_center">' . $v['NumNoData'] . '</td>
    					<td class="align_center">' . $v['PercentNoData'] . '</td>';
        }
        $x .= ' </tr>';
    }
    $x .= '</table>
        
    </form>';
    
    return $x;
}

function getPerformanceTablePassFail($title, $stat)
{
    global $Lang;
    
    if ($stat['junior']['NumNoData'] > 0 || $stat['high']['NumNoData'] > 0) {
        $isNoData = true; // whether there's student that hasn't imported data or not
        $extraColumnTitleRow1 = '<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . '</th>';
        $extraColumnTitleRow2 = '<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
                <th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>';
    } else {
        $isNoData = false;
    }
    $x = '
    <style>
    	.align_center {text-align:center !important;}
    </style>
    <form name="form2" id="form2" method="POST">
        ' . getExportButton() . '
    	<div style="font-weight:bold; text-align:center;">' . $title . '</div><br>
    	<table width="98%" border="0" cellpadding="0" cellspacing="0" class="common_table_list">
    		<tr>
    			<th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Form'] . '</th>
                <th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Total'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMet'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . '</th>' . $extraColumnTitleRow1 . '
    		</tr>
    		<tr>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>' . $extraColumnTitleRow2 . '
    		</tr>';
    
    foreach ((array) $stat as $r => $v) {
        $rowClass = ($r == 'junior') ? 'total_row' : '';
        $x .= '	<tr class="' . $rowClass . '"><td class="align_center">' . $v['Name'] . '</td>
                        <td class="align_center">' . $v['NumStudent'] . '</td>
    					<td class="align_center">' . $v['NumPass'] . '</td>
    					<td class="align_center">' . $v['PercentPass'] . '</td>
    					<td class="align_center">' . $v['NumFail'] . '</td>
    					<td class="align_center">' . $v['PercentFail'] . '</td>';
        if ($isNoData) {
            $x .= '<td class="align_center">' . $v['NumNoData'] . '</td>
    					<td class="align_center">' . $v['PercentNoData'] . '</td>';
        }
        $x .= ' </tr>';
    }
    $x .= '</table>
        
    </form>';
    
    return $x;
}

function getPerformanceTableGoodPassFailNotJoin($title, $stat)
{
    global $Lang;
    
    $x = '
    <style>
    	.align_center {text-align:center !important;}
    </style>
    <form name="form2" id="form2" method="POST">
        ' . getExportButton() . '
    	<div style="font-weight:bold; text-align:center;">' . $title . '</div><br>
    	<table width="98%" border="0" cellpadding="0" cellspacing="0" class="common_table_list">
    		<tr>
    			<th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Form'] . '</th>
                <th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Total'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipation'] . '</th>
                <th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipation'] . '</th>
    		</tr>
    		<tr>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    		</tr>';
    
    foreach ((array) $stat as $r => $v) {
        $rowClass = ($r == 'junior') ? 'total_row' : '';
        $x .= '	<tr class="' . $rowClass . '"><td class="align_center">' . $v['Name'] . '</td>
                        <td class="align_center">' . $v['NumStudent'] . '</td>
    					<td class="align_center">' . $v['NumGood'] . '</td>
    					<td class="align_center">' . $v['PercentGood'] . '</td>
    					<td class="align_center">' . $v['NumPass'] . '</td>
    					<td class="align_center">' . $v['PercentPass'] . '</td>
    					<td class="align_center">' . $v['NumFail'] . '</td>
    					<td class="align_center">' . $v['PercentFail'] . '</td>
                        <td class="align_center">' . $v['NumNoData'] . '</td>
    					<td class="align_center">' . $v['PercentNoData'] . '</td>
                </tr>';
    }
    $x .= '</table>
        
    </form>';
    
    return $x;
}

function getPerformanceTableService($title, $stat)
{
    global $Lang;
    
    $x = '
    <style>
    	.align_center {text-align:center !important;}
    </style>
    <form name="form2" id="form2" method="POST">
        ' . getExportButton() . '
    	<div style="font-weight:bold; text-align:center;">' . $title . '</div><br>
    	<table width="98%" border="0" cellpadding="0" cellspacing="0" class="common_table_list">
    		<tr>
    			<th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Form'] . '</th>
                <th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Total'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Participation'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Merit'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoMeritWithParticipation'] . '</th>
                <th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipation'] . '</th>
    		</tr>
    		<tr>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    		</tr>';
    
    foreach ((array) $stat as $r => $v) {
        $rowClass = ($r == 'junior') ? 'total_row' : '';
        $x .= '	<tr class="' . $rowClass . '"><td class="align_center">' . $v['Name'] . '</td>
                        <td class="align_center">' . $v['NumStudent'] . '</td>
    					<td class="align_center">' . $v['NumJoin'] . '</td>
    					<td class="align_center">' . $v['PercentJoin'] . '</td>
    					<td class="align_center">' . $v['NumGood'] . '</td>
    					<td class="align_center">' . $v['PercentGood'] . '</td>
    					<td class="align_center">' . $v['NumPass'] . '</td>
    					<td class="align_center">' . $v['PercentPass'] . '</td>
                        <td class="align_center">' . $v['NumNoData'] . '</td>
    					<td class="align_center">' . $v['PercentNoData'] . '</td>
                </tr>';
    }
    $x .= '</table>
        
    </form>';
    
    return $x;
}

function getPerformanceTableExtracurricularActivities($title, $stat)
{
    global $Lang;
    
    $x = '
    <style>
    	.align_center {text-align:center !important;}
    </style>
    <form name="form2" id="form2" method="POST">
        ' . getExportButton() . '
    	<div style="font-weight:bold; text-align:center;">' . $title . '</div><br>
    	<table width="98%" border="0" cellpadding="0" cellspacing="0" class="common_table_list">
    		<tr>
    			<th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Form'] . '</th>
                <th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Total'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipationEA'] . '</th>
                <th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipationEA'] . '</th>
    		</tr>
    		<tr>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    		</tr>';
    
    foreach ((array) $stat as $r => $v) {
        $rowClass = ($r == 'junior') ? 'total_row' : '';
        $x .= '	<tr class="' . $rowClass . '"><td class="align_center">' . $v['Name'] . '</td>
                        <td class="align_center">' . $v['NumStudent'] . '</td>
    					<td class="align_center">' . $v['NumGood'] . '</td>
    					<td class="align_center">' . $v['PercentGood'] . '</td>
    					<td class="align_center">' . $v['NumPass'] . '</td>
    					<td class="align_center">' . $v['PercentPass'] . '</td>
    					<td class="align_center">' . $v['NumFail'] . '</td>
    					<td class="align_center">' . $v['PercentFail'] . '</td>
                        <td class="align_center">' . $v['NumNoData'] . '</td>
    					<td class="align_center">' . $v['PercentNoData'] . '</td>
                </tr>';
    }
    $x .= '</table>
        
    </form>';
    
    return $x;
}

function getPerformanceTableExternalCompetitions($title, $stat)
{
    global $Lang;
    
    $x = '
    <style>
    	.align_center {text-align:center !important;}
    </style>
    <form name="form2" id="form2" method="POST">
        ' . getExportButton() . '
    	<div style="font-weight:bold; text-align:center;">' . $title . '</div><br>
    	<table width="98%" border="0" cellpadding="0" cellspacing="0" class="common_table_list">
    		<tr>
    			<th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Form'] . '</th>
                <th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Total'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipationEC'] . '</th>
                <th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['ParticipationWithoutPrize'] . '</th>
                <th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['PrizeWithoutMerit'] . '</th>
                <th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['PrizeWithMerit'] . '</th>
                <th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipationEC'] . '</th>
    		</tr>
    		<tr>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    		</tr>';
    
    foreach ((array) $stat as $r => $v) {
        $rowClass = ($r == 'junior') ? 'total_row' : '';
        $x .= '	<tr class="' . $rowClass . '"><td class="align_center">' . $v['Name'] . '</td>
                        <td class="align_center">' . $v['NumStudent'] . '</td>
    					<td class="align_center">' . $v['NumGood'] . '</td>
                        <td class="align_center">' . $v['PercentGood'] . '</td>
    					<td class="align_center">' . $v['NumPass'] . '</td>
    					<td class="align_center">' . $v['PercentPass'] . '</td>
    					<td class="align_center">' . $v['NumFail'] . '</td>
    					<td class="align_center">' . $v['PercentFail'] . '</td>
    					<td class="align_center">' . $v['NumJoin'] . '</td>
                        <td class="align_center">' . $v['PercentJoin'] . '</td>
    					<td class="align_center">' . $v['NumNoMerit'] . '</td>
    					<td class="align_center">' . $v['PercentNoMerit'] . '</td>
    					<td class="align_center">' . $v['NumMerit'] . '</td>
    					<td class="align_center">' . $v['PercentMerit'] . '</td>
                        <td class="align_center">' . $v['NumNoData'] . '</td>
    					<td class="align_center">' . $v['PercentNoData'] . '</td>
                </tr>';
    }
    $x .= '</table>
        
    </form>';
    
    return $x;
}

function getPerformanceTableGoodPassFailNotImport($title, $stat)
{
    global $Lang;

    $x = '
    <style>
    	.align_center {text-align:center !important;}
    </style>
    <form name="form2" id="form2" method="POST">
        ' . getExportButton() . '
    	<div style="font-weight:bold; text-align:center;">' . $title . '</div><br>
    	<table width="98%" border="0" cellpadding="0" cellspacing="0" class="common_table_list">
    		<tr>
    			<th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Form'] . '</th>
                <th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Total'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . '</th>
                <th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . '</th>
    		</tr>
    		<tr>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    		</tr>';

    foreach ((array) $stat as $r => $v) {
        $rowClass = ($r == 'junior') ? 'total_row' : '';
        $x .= '	<tr class="' . $rowClass . '"><td class="align_center">' . $v['Name'] . '</td>
                        <td class="align_center">' . $v['NumStudent'] . '</td>
    					<td class="align_center">' . $v['NumGood'] . '</td>
    					<td class="align_center">' . $v['PercentGood'] . '</td>
    					<td class="align_center">' . $v['NumPass'] . '</td>
    					<td class="align_center">' . $v['PercentPass'] . '</td>
    					<td class="align_center">' . $v['NumFail'] . '</td>
    					<td class="align_center">' . $v['PercentFail'] . '</td>
                        <td class="align_center">' . $v['NumNoData'] . '</td>
    					<td class="align_center">' . $v['PercentNoData'] . '</td>
                </tr>';
    }
    $x .= '</table>
        
    </form>';

    return $x;
}

function getPerformanceTableGoodPassNoData($title, $stat)
{
    global $Lang;

    if ($stat['junior']['NumNoData'] > 0 || $stat['high']['NumNoData'] > 0) {
        $isNoData = true; // whether there's student that hasn't imported data or not
        $extraColumnTitleRow1 = '<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . '</th>';
        $extraColumnTitleRow2 = '<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
                <th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>';
    } else {
        $isNoData = false;
    }

    $x = '
    <style>
    	.align_center {text-align:center !important;}
    </style>
    <form name="form2" id="form2" method="POST">
        ' . getExportButton() . '
    	<div style="font-weight:bold; text-align:center;">' . $title . '</div><br>
    	<table width="98%" border="0" cellpadding="0" cellspacing="0" class="common_table_list">
    		<tr>
    			<th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Form'] . '</th>
                <th class="sub_row_top align_center" rowspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['Total'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . '</th>
    			<th class="sub_row_top align_center" colspan="2">' . $Lang['iPortfolio']['JinYing']['Report']['NoRecord'] . '</th>' . $extraColumnTitleRow1 . '
    		</tr>
    		<tr>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Count'] . '</th>
    			<th class="sub_row_top align_center">' . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] . '</th>' . $extraColumnTitleRow2 . '
    		</tr>';

    foreach ((array) $stat as $r => $v) {
        $rowClass = ($r == 'junior') ? 'total_row' : '';
        $x .= '	<tr class="' . $rowClass . '"><td class="align_center">' . $v['Name'] . '</td>
                        <td class="align_center">' . $v['NumStudent'] . '</td>
    					<td class="align_center">' . $v['NumGood'] . '</td>
    					<td class="align_center">' . $v['PercentGood'] . '</td>
    					<td class="align_center">' . $v['NumPass'] . '</td>
    					<td class="align_center">' . $v['PercentPass'] . '</td>
    					<td class="align_center">' . $v['NumFail'] . '</td>
    					<td class="align_center">' . $v['PercentFail'] . '</td>';
        if ($isNoData) {
            $x .= '<td class="align_center">' . $v['NumNoData'] . '</td>
    					<td class="align_center">' . $v['PercentNoData'] . '</td>';
        }
        $x .= ' </tr>';
    }
    $x .= '</table>
        
    </form>';

    return $x;
}

// ############################ end functions

?>