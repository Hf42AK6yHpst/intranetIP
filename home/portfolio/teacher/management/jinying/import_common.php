<?
/*
 * 	Log
 * 
 * 	Note: please use utf-8 to edit
 * 
 * 	Purpose: common function for JinYin import
 *
 *  2019-12-03 [Cameron]
 *      - modify getRecommendAwardCode() to support more awards [case #J161484]
 *
 *  2019-07-04 [Cameron]
 *      - fix activity index in check_duplicate_jinying_record_in_excel() [case #J164242]
 *      - add parameter awardEng and awardChi to check_duplicate_jinying_record() for checking duplicate BTP_IC_P03 record 
 *          only if award name are the same (allow more than one award in the same activity)
 *      - add function check_duplicate_jinying_record_in_excel_btp_ic()
 *      - pass argument awardNameEng and awardNameChi to check_duplicate_jinying_record_by_RecordID() in validate_BTP_IC_4update()
 *      - add parameter awardEng and awardChi to check_duplicate_jinying_record_by_RecordID()
 *
 *	2017-09-13 [Cameron]
 *		- Add OrganizationNameEng & OrganizationNameChi for BTP-IC form
 *
 *	2017-08-28 [Cameron]
 *		- change to use webSAMS instead of ClassName + ClassNumber to identify student when import, hence change the column position according for all related functions in this file
 *		- add checking for StudentName from excel vs that from INTRANET_USER (EnglishName or ChineseName) for new import 
 *
 * 	2017-06-28 [Cameron]
 * 		improvement: 1. add function check_duplicate_jinying_record_in_excel() and apply it to validate_item_with_activity(),
 * 			validate_OFS_SI(), validate_BTP_EA(), validate_BTP_IC() [case #J119111]
 * 			2. modify check_duplicate_jinying_record(), check both ActivityNameEng and AcitivityNameChi if they exist (either one is the same is treated as duplicate)
 *  
 * 	2017-05-04 [Cameron]
 * 		add validation and error table for update/delete
 * 
 * 	2016-11-24 [Cameron] create this file
 *  
 */


function getRecommendAwardCode($recommendAward) {
    $code = '';
    if (!empty($recommendAward)) {
        $recommendAward = substr($recommendAward,2);
        switch ($recommendAward) {
            case '優點1個':
                $code = 'Merit_1';
                break;
            case '優點2個':
                $code = 'Merit_2';
                break;
            case '小功1個':
                $code = 'MinorM_1';
                break;
            case '小功1個及優點1個':
                $code = 'MinorM_1_Merit_1';
                break;
            case '小功1個及優點2個':
                $code = 'MinorM_1_Merit_2';
                break;
            case '小功2個':
                $code = 'MinorM_2';
                break;
            case '小功2個及優點1個':
                $code = 'MinorM_2_Merit_1';
                break;
            case '小功2個及優點2個':
                $code = 'MinorM_2_Merit_2';
                break;
            case '大功1個':
                $code = 'MajorM_1';
                break;
        }
    }
    return $code;
}

// return true if duplicate ( already exist )
// $recordID is for update checking, 
//	activity name in English has higher priority in comparison: compare English Name if it exist, otherwise, compare Chinese Name 
function check_duplicate_jinying_record($_userID,$AcademicYearID,$_performance_code,$YearTermID=0,$activityEng='',$activityChi='',$recordID='',$awardEng='',$awardChi='') {
	global $lpf, $eclass_db;
	$table_name = $eclass_db.'.PORTFOLIO_JINYING_SCHEME';
	$yearTermCond = $YearTermID ? " AND YearTermID='".$lpf->Get_Safe_Sql_Query($YearTermID)."'" : "";
	$recordIDCond = $recordID ? " AND RecordID<>'".$recordID."'" : "";
	 
	$sql = "SELECT PerformanceCode FROM " . $table_name .
			" WHERE UserID='".$lpf->Get_Safe_Sql_Query($_userID)."'".
			" AND AcademicYearID='". $lpf->Get_Safe_Sql_Query($AcademicYearID)."'". 
			" AND PerformanceCode LIKE '". $lpf->Get_Safe_Sql_Query(substr($_performance_code,0,6))."%'".
			$yearTermCond.$recordIDCond;
	if ($activityEng && $activityChi) {
		$sql .= " AND (ActivityNameEng='". $lpf->Get_Safe_Sql_Query($activityEng)."' OR ActivityNameChi='". $lpf->Get_Safe_Sql_Query($activityChi)."')";
	}
	else if ($activityEng) {
		$sql .= " AND ActivityNameEng='". $lpf->Get_Safe_Sql_Query($activityEng)."'";
	}
	else if ($activityChi) {
		$sql .= " AND ActivityNameChi='". $lpf->Get_Safe_Sql_Query($activityChi)."'";
	}
	
	if ($awardEng && $awardChi) {
	    $sql .= " AND (AwardNameEng='". $lpf->Get_Safe_Sql_Query($awardEng)."' OR AwardNameChi='". $lpf->Get_Safe_Sql_Query($awardChi)."')";
	}
	else if ($awardEng) {
	    $sql .= " AND AwardNameEng='". $lpf->Get_Safe_Sql_Query($awardEng)."'";
	}
	else if ($awardChi) {
	    $sql .= " AND AwardNameChi='". $lpf->Get_Safe_Sql_Query($awardChi)."'";
	}
	
	$exist_rs = $lpf->returnResultSet($sql);

	return count($exist_rs) ? true : false;
}


// return true if duplicate ( already exist )
function check_duplicate_jinying_record_by_RecordID($recordID, $_performance_code, $activityEng='', $activityChi='', $awardEng='', $awardChi='') {
	global $lpf, $eclass_db;
	$table_name = $eclass_db.'.PORTFOLIO_JINYING_SCHEME';
	
	$sql = "SELECT UserID, AcademicYearID, YearTermID FROM ".$table_name." WHERE RecordID='".$recordID."'";
	$rs = $lpf->returnResultSet($sql);
	if (count($rs)) {
		$rs = current($rs);
		$_userID = $rs['UserID'];
		$AcademicYearID = $rs['AcademicYearID'];
		$YearTermID = $rs['YearTermID'];
		$ret = check_duplicate_jinying_record($_userID,$AcademicYearID,$_performance_code,$YearTermID,$activityEng,$activityChi,$recordID,$awardEng,$awardChi);
	}
	else {
		$ret = false;
	}
	return $ret;
}

function check_duplicate_jinying_record_in_excel($data,$webSAMS,$Item,$activityEng='',$activityChi='') {
	$count = 0;
	switch ($Item) {
		case 'BSQ-EC':
		case 'BSQ-TA':
		case 'EPW-TA':
			$engActivityIdx = 5;
			$chiActivityIdx = 6;
			break;
		case 'OFS-SI':
		case 'BTP-EA':
			$engActivityIdx = 4;
			$chiActivityIdx = 5;
			break;
	}
	
	$numOfData = count($data);
	for ($i=0; $i<$numOfData; $i++) {
		$_row = $data[$i];
		$_webSAMS = trim($_row[2]);
		$_activityNameEng = trim($_row[$engActivityIdx]);
		$_activityNameChi = trim($_row[$chiActivityIdx]);
		if ($activityEng && $activityChi) {
			if (($_webSAMS == $webSAMS) && (($_activityNameEng == $activityEng) || ($_activityNameChi == $activityChi))) {
				$count++;
			}
		}
		else if ($activityEng) {
			if (($_webSAMS == $webSAMS) && ($_activityNameEng == $activityEng)) {
				$count++;
			}
		}
		else if ($activityChi) {
			if (($_webSAMS == $webSAMS) && ($_activityNameChi == $activityChi)) {
				$count++;
			}
		} 
	}
	return $count > 1 ? true : false; 
}

function check_duplicate_jinying_record_in_excel_btp_ic($data,$webSAMS,$activityEng='',$activityChi='', $awardEng='', $awardChi='') 
{
    global $Lang;
    
    $count = 0;
    $engActivityIdx = 5;
    $chiActivityIdx = 6;
    $performanceIdx = 9;
    $engAwardIdx = 10;
    $chiAwardIdx = 11;
    
    $numOfData = count($data);
    for ($i=0; $i<$numOfData; $i++) {
        $_row = $data[$i];
        $_webSAMS = trim($_row[2]);
        $_activityNameEng = trim($_row[$engActivityIdx]);
        $_activityNameChi = trim($_row[$chiActivityIdx]);

        if ($_row[$performanceIdx] == $Lang['iPortfolio']['JinYing']['CodeName']['BTP-IC']['BTP-IC-P-03']) {
            $_awardEng = trim($_row[$engAwardIdx]);
            $_awardChi = trim($_row[$chiAwardIdx]);
            
            if ($activityEng && $activityChi) {
                if ($awardEng && $awardChi) {
                    if (($_webSAMS == $webSAMS) && (($_activityNameEng == $activityEng) || ($_activityNameChi == $activityChi)) && (($_awardEng == $awardEng) || ($_awardChi == $awardChi))) {
                        $count++;
                    }
                }
                else if ($awardEng) {
                    if (($_webSAMS == $webSAMS) && (($_activityNameEng == $activityEng) || ($_activityNameChi == $activityChi)) && ($_awardEng == $awardEng)) {
                        $count++;
                    }
                }
                else if ($awardChi) {
                    if (($_webSAMS == $webSAMS) && (($_activityNameEng == $activityEng) || ($_activityNameChi == $activityChi)) && ($_awardChi == $awardChi)) {
                        $count++;
                    }
                }
                else {
                    if (($_webSAMS == $webSAMS) && (($_activityNameEng == $activityEng) || ($_activityNameChi == $activityChi)) && (($_awardEng == '') || ($_awardChi == ''))) {
                        $count++;
                    }
                }
            }
            else if ($activityEng) {
                if ($awardEng && $awardChi) {
                    if (($_webSAMS == $webSAMS) && ($_activityNameEng == $activityEng) && (($_awardEng == $awardEng) || ($_awardChi == $awardChi))) {
                        $count++;
                    }
                }
                else if ($awardEng) {
                    if (($_webSAMS == $webSAMS) && ($_activityNameEng == $activityEng) && ($_awardEng == $awardEng)) {
                        $count++;
                    }
                }
                else if ($awardChi) {
                    if (($_webSAMS == $webSAMS) && ($_activityNameEng == $activityEng) && ($_awardChi == $awardChi)) {
                        $count++;
                    }
                }
                else {
                    if (($_webSAMS == $webSAMS) && ($_activityNameEng == $activityEng) && (($_awardEng == '') || ($_awardChi == ''))) {
                        $count++;
                    }
                }
            }
            else if ($activityChi) {
                if ($awardEng && $awardChi) {
                    if (($_webSAMS == $webSAMS) && ($_activityNameChi == $activityChi) && (($_awardEng == $awardEng) || ($_awardChi == $awardChi))) {
                        $count++;
                    }
                }
                else if ($awardEng) {
                    if (($_webSAMS == $webSAMS) && ($_activityNameChi == $activityChi) && ($_awardEng == $awardEng)) {
                        $count++;
                    }
                }
                else if ($awardChi) {
                    if (($_webSAMS == $webSAMS) && ($_activityNameChi == $activityChi) && ($_awardChi == $awardChi)) {
                        $count++;
                    }
                }
                else {
                    if (($_webSAMS == $webSAMS) && ($_activityNameChi == $activityChi) && (($_awardEng == '') || ($_awardChi == ''))) {
                        $count++;
                    }
                }
            }
        }
        else {
            if ($activityEng && $activityChi) {
                if (($_webSAMS == $webSAMS) && (($_activityNameEng == $activityEng) || ($_activityNameChi == $activityChi))) {
                    $count++;
                }
            }
            else if ($activityEng) {
                if (($_webSAMS == $webSAMS) && ($_activityNameEng == $activityEng)) {
                    $count++;
                }
            }
            else if ($activityChi) {
                if (($_webSAMS == $webSAMS) && ($_activityNameChi == $activityChi)) {
                    $count++;
                }
            }
        }
    }
    return $count > 1 ? true : false;
}

function validate_item_with_term($data, $studentWebSAMS_array, $Item, $AcademicYearID, $Semester) {
	global $Lang, $lpf;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_webSAMSRegNo = '#'.$_webSAMS;		// add prefix # as INTRANET_USER inlcudes it
		$_studentName = trim($_row[3]);
		$_performance = trim($_row[4]);
		
		## 1. check if WebSAMS number exist or not
		$_userID = '';
		if ( $_webSAMS != '' && isset($studentWebSAMS_array[$_webSAMSRegNo]['UserID'])) {
			if (($_studentName != $studentWebSAMS_array[$_webSAMSRegNo]['StudentNameEng']) && ($_studentName != $studentWebSAMS_array[$_webSAMSRegNo]['StudentNameChi'])) {
				$errorMsgAssoAry[$_rowNum][] = 'StudentNameNotMatch';
			} 
			else {
				$_userID = $studentWebSAMS_array[$_webSAMSRegNo]['UserID'];
			}
		}
		else {
			$errorMsgAssoAry[$_rowNum][] = 'StudentNotFound';
		}
		
		## 2. check if performance is empty or not
		if (empty($_performance)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyPerformance';
			$_performance_code = '';
		}
		else {
			## 3. check if performance is in preset list or not
			$_performance_code = array_search($_performance,$Lang['iPortfolio']['JinYing']['CodeName'][$Item]);
			if ($_performance_code === false) {
				$errorMsgAssoAry[$_rowNum][] = 'InvalidPerformanceCode';
				$_performance_code = '';
			}
			else {
				if ($Semester) {
					## 4. check duplicate record
					$check_result = check_duplicate_jinying_record($_userID,$AcademicYearID,$_performance_code,$Semester);
					if ($check_result) {
						$errorMsgAssoAry[$_rowNum][] = 'RecordExist';
					}
				}
				else {
					$errorMsgAssoAry[$_rowNum][] = 'EmptySemester';
				}		
			}					
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_userID)."', '".
						$lpf->Get_Safe_Sql_Query($AcademicYearID)."', '".
						$lpf->Get_Safe_Sql_Query($Semester)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', '".
						$lpf->Get_Safe_Sql_Query($_className)."', '".
						$lpf->Get_Safe_Sql_Query($_classNumber)."', '".
						$lpf->Get_Safe_Sql_Query($_studentName)."', '".
						$lpf->Get_Safe_Sql_Query($_webSAMS)."', '".
						$lpf->Get_Safe_Sql_Query($_performance)."', ".
						"NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ".
						"NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}


function validate_item_without_term($data, $studentWebSAMS_array, $Item, $AcademicYearID, $Semester) {
	global $Lang, $lpf;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_webSAMSRegNo = '#'.$_webSAMS;		// add prefix # as INTRANET_USER inlcudes it
		$_studentName = trim($_row[3]);
		$_performance = trim($_row[4]);

		## 1. check if class name + class number exist or not
		$_userID = '';
		if ( $_webSAMS != '' && isset($studentWebSAMS_array[$_webSAMSRegNo]['UserID'])) {
			if (($_studentName != $studentWebSAMS_array[$_webSAMSRegNo]['StudentNameEng']) && ($_studentName != $studentWebSAMS_array[$_webSAMSRegNo]['StudentNameChi'])) {
				$errorMsgAssoAry[$_rowNum][] = 'StudentNameNotMatch';
			} 
			else {
				$_userID = $studentWebSAMS_array[$_webSAMSRegNo]['UserID'];
			}
		}
		else {
			$errorMsgAssoAry[$_rowNum][] = 'StudentNotFound';
		}
		
		## 2. check if performance is empty or not
		if (empty($_performance)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyPerformance';
			$_performance_code = '';
		}
		else {
			## 3. check if performance is in preset list or not
			$_performance_code = array_search($_performance,$Lang['iPortfolio']['JinYing']['CodeName'][$Item]);
			if ($_performance_code === false) {
				$errorMsgAssoAry[$_rowNum][] = 'InvalidPerformanceCode';
				$_performance_code = '';
			}
			else {
				## 4. check duplicate record
				$check_result = check_duplicate_jinying_record($_userID,$AcademicYearID,$_performance_code,$Semester);
				if ($check_result) {
					$errorMsgAssoAry[$_rowNum][] = 'RecordExist';
				}
			}					
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_userID)."', '".
						$lpf->Get_Safe_Sql_Query($AcademicYearID)."', '".
						$lpf->Get_Safe_Sql_Query($Semester)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', '".
						$lpf->Get_Safe_Sql_Query($_className)."', '".
						$lpf->Get_Safe_Sql_Query($_classNumber)."', '".
						$lpf->Get_Safe_Sql_Query($_webSAMS)."', '".
						$lpf->Get_Safe_Sql_Query($_studentName)."', '".
						$lpf->Get_Safe_Sql_Query($_performance)."', ".
						"NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ".
						"NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}


function validate_item_with_activity($data, $studentWebSAMS_array, $Item, $AcademicYearID, $Semester) {
	global $Lang, $lpf;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_webSAMSRegNo = '#'.$_webSAMS;		// add prefix # as INTRANET_USER inlcudes it
		$_studentName = trim($_row[3]);
		$_activityDate = trim($_row[4]);
		if (is_numeric($_activityDate)) {
			$_activityDate = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($_activityDate));
		}
		$_activityNameEng = trim($_row[5]);
		$_activityNameChi = trim($_row[6]);
		$_performance = trim($_row[7]);
		
		## 1. check if class name + class number exist or not
		$_userID = '';
		if ( $_webSAMS != '' && isset($studentWebSAMS_array[$_webSAMSRegNo]['UserID'])) {
			if (($_studentName != $studentWebSAMS_array[$_webSAMSRegNo]['StudentNameEng']) && ($_studentName != $studentWebSAMS_array[$_webSAMSRegNo]['StudentNameChi'])) {
				$errorMsgAssoAry[$_rowNum][] = 'StudentNameNotMatch';
			} 
			else {
				$_userID = $studentWebSAMS_array[$_webSAMSRegNo]['UserID'];
			}
		}
		else {
			$errorMsgAssoAry[$_rowNum][] = 'StudentNotFound';
		}
		
		## 2. check if activity name is empty or not
		if (empty($_activityNameEng) && empty($_activityNameChi)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyActivity';
		}
		
		## 3. check if performance is empty or not
		if (empty($_performance)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyPerformance';
			$_performance_code = '';
		}
		else {
			## 4. check if performance is in preset list or not
			$_performance_code = array_search($_performance,$Lang['iPortfolio']['JinYing']['CodeName'][$Item]);
			if ($_performance_code === false) {
				$errorMsgAssoAry[$_rowNum][] = 'InvalidPerformanceCode';
				$_performance_code = '';
			}
			else {
				## 5. check duplicate record
//				$activity = $_activityNameEng ? $_activityNameEng : $_activityNameChi;
				$check_result = check_duplicate_jinying_record($_userID,$AcademicYearID,$_performance_code,$Semester,$_activityNameEng,$_activityNameChi);
				if ($check_result) {
					$errorMsgAssoAry[$_rowNum][] = 'RecordExist';
				}
				else {
					$check_result = check_duplicate_jinying_record_in_excel($data,$_webSAMS,$Item,$_activityNameEng,$_activityNameChi);
					if ($check_result) {
						$errorMsgAssoAry[$_rowNum][] = 'RecordDuplicate';
					}
				}		
			}					
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_userID)."', '".
						$lpf->Get_Safe_Sql_Query($AcademicYearID)."', '".
						$lpf->Get_Safe_Sql_Query($Semester)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', '".
						$lpf->Get_Safe_Sql_Query($_className)."', '".
						$lpf->Get_Safe_Sql_Query($_classNumber)."', '".
						$lpf->Get_Safe_Sql_Query($_webSAMS)."', '".
						$lpf->Get_Safe_Sql_Query($_studentName)."', '".
						$lpf->Get_Safe_Sql_Query($_performance)."', '".
						$lpf->Get_Safe_Sql_Query($_activityDate)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameChi)."', ".
						"NULL, NULL, NULL, NULL, NULL, NULL, NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}


function validate_OFS_SI($data, $studentWebSAMS_array, $Item, $AcademicYearID, $Semester) {
	global $Lang, $lpf;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_webSAMSRegNo = '#'.$_webSAMS;		// add prefix # as INTRANET_USER inlcudes it
		$_studentName = trim($_row[3]);
		$_activityNameEng = trim($_row[4]);
		$_activityNameChi = trim($_row[5]);
		$_performance = trim($_row[6]);
		$_recommendMerit = trim($_row[7]);
		if (!empty($_recommendMerit)) {
			$_recommendMerit = getRecommendAwardCode($_recommendMerit);
		}
		
		## 1. check if class name + class number exist or not
		$_userID = '';
		if ( $_webSAMS != '' && isset($studentWebSAMS_array[$_webSAMSRegNo]['UserID'])) {
			if (($_studentName != $studentWebSAMS_array[$_webSAMSRegNo]['StudentNameEng']) && ($_studentName != $studentWebSAMS_array[$_webSAMSRegNo]['StudentNameChi'])) {
				$errorMsgAssoAry[$_rowNum][] = 'StudentNameNotMatch';
			} 
			else {
				$_userID = $studentWebSAMS_array[$_webSAMSRegNo]['UserID'];
			}
		}
		else {
			$errorMsgAssoAry[$_rowNum][] = 'StudentNotFound';
		}
		
		## 2. check if activity name is empty or not
		if (empty($_activityNameEng) && empty($_activityNameChi)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyService';
		}
		
		## 3. check if performance is empty or not
		if (empty($_performance)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyPerformance';
			$_performance_code = '';
		}
		else {
			## 4. check if performance is in preset list or not
			$_performance_code = array_search($_performance,$Lang['iPortfolio']['JinYing']['CodeName'][$Item]);
			if ($_performance_code === false) {
				$errorMsgAssoAry[$_rowNum][] = 'InvalidPerformanceCode';
				$_performance_code = '';
			}
			## 5. check if performance match the award or not
			else if ($_performance_code != 'OFS-SI-P-01' && (!empty($_recommendMerit))) {
				$errorMsgAssoAry[$_rowNum][] = 'PerformanceConflictAward';
				$_awardNameEng = '';
				$_awardNameChi = '';
				$_recommendMerit = '';
			}
			else {
				## 6. check duplicate record
//				$activity = $_activityNameEng ? $_activityNameEng : $_activityNameChi;
				$check_result = check_duplicate_jinying_record($_userID,$AcademicYearID,$_performance_code,$Semester,$_activityNameEng,$_activityNameChi);
				if ($check_result) {
					$errorMsgAssoAry[$_rowNum][] = 'RecordExist';
				}
				else {
					$check_result = check_duplicate_jinying_record_in_excel($data,$_webSAMS,$Item,$_activityNameEng,$_activityNameChi);
					if ($check_result) {
						$errorMsgAssoAry[$_rowNum][] = 'RecordDuplicate';
					}
				}		
			}					
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_userID)."', '".
						$lpf->Get_Safe_Sql_Query($AcademicYearID)."', '".
						$lpf->Get_Safe_Sql_Query($Semester)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', '".
						$lpf->Get_Safe_Sql_Query($_className)."', '".
						$lpf->Get_Safe_Sql_Query($_classNumber)."', '".
						$lpf->Get_Safe_Sql_Query($_webSAMS)."', '".
						$lpf->Get_Safe_Sql_Query($_studentName)."', '".
						$lpf->Get_Safe_Sql_Query($_performance)."', ".
						"NULL, '".
						$lpf->Get_Safe_Sql_Query($_activityNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameChi)."', ".
						"NULL, NULL,'".
						$lpf->Get_Safe_Sql_Query($_recommendMerit)."', ".
						"NULL, NULL, NULL, NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}


function validate_OFS_SO($data, $studentWebSAMS_array, $Item, $AcademicYearID, $Semester) {
	global $Lang, $lpf;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_webSAMSRegNo = '#'.$_webSAMS;		// add prefix # as INTRANET_USER inlcudes it
		$_studentName = trim($_row[3]);
		$_serviceHours = trim($_row[4]);
		
		## 1. check if class name + class number exist or not
		$_userID = '';
		if ( $_webSAMS != '' && isset($studentWebSAMS_array[$_webSAMSRegNo]['UserID'])) {
			if (($_studentName != $studentWebSAMS_array[$_webSAMSRegNo]['StudentNameEng']) && ($_studentName != $studentWebSAMS_array[$_webSAMSRegNo]['StudentNameChi'])) {
				$errorMsgAssoAry[$_rowNum][] = 'StudentNameNotMatch';
			} 
			else {
				$_userID = $studentWebSAMS_array[$_webSAMSRegNo]['UserID'];
			}
		}
		else {
			$errorMsgAssoAry[$_rowNum][] = 'StudentNotFound';
		}
		
		## 2. check if activity name is empty or not
		if (!is_numeric($_serviceHours)) {
			$errorMsgAssoAry[$_rowNum][] = 'NotNumeric';
		}
		else {
			$_performance_code = 'OFS-SO-P-01';
			## 6. check duplicate record
			$check_result = check_duplicate_jinying_record($_userID,$AcademicYearID,$_performance_code,$Semester);
			if ($check_result) {
				$errorMsgAssoAry[$_rowNum][] = 'RecordExist';
			}		
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_userID)."', '".
						$lpf->Get_Safe_Sql_Query($AcademicYearID)."', '".
						$lpf->Get_Safe_Sql_Query($Semester)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', '".
						$lpf->Get_Safe_Sql_Query($_className)."', '".
						$lpf->Get_Safe_Sql_Query($_classNumber)."', '".
						$lpf->Get_Safe_Sql_Query($_webSAMS)."', '".
						$lpf->Get_Safe_Sql_Query($_studentName)."', ".
						"NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '".
						$lpf->Get_Safe_Sql_Query($_serviceHours)."', ".												
						"NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}


function validate_BTP_EA($data, $studentWebSAMS_array, $Item, $AcademicYearID, $Semester) {
	global $Lang, $lpf;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_webSAMSRegNo = '#'.$_webSAMS;		// add prefix # as INTRANET_USER inlcudes it
		$_studentName = trim($_row[3]);
		$_activityNameEng = trim($_row[4]);
		$_activityNameChi = trim($_row[5]);
		$_performance = trim($_row[6]);
		
		## 1. check if class name + class number exist or not
		$_userID = '';
		if ( $_webSAMS != '' && isset($studentWebSAMS_array[$_webSAMSRegNo]['UserID'])) {
			if (($_studentName != $studentWebSAMS_array[$_webSAMSRegNo]['StudentNameEng']) && ($_studentName != $studentWebSAMS_array[$_webSAMSRegNo]['StudentNameChi'])) {
				$errorMsgAssoAry[$_rowNum][] = 'StudentNameNotMatch';
			} 
			else {
				$_userID = $studentWebSAMS_array[$_webSAMSRegNo]['UserID'];
			}
		}
		else {
			$errorMsgAssoAry[$_rowNum][] = 'StudentNotFound';
		}
		
		## 2. check if club name is empty or not
		if (empty($_activityNameEng) && empty($_activityNameChi)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyClubName';
		}
		
		## 3. check if performance is empty or not
		if (empty($_performance)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyPerformance';
			$_performance_code = '';
		}
		else {
			## 4. check if performance is in preset list or not
			$_performance_code = array_search($_performance,$Lang['iPortfolio']['JinYing']['CodeName'][$Item]);
			if ($_performance_code === false) {
				$errorMsgAssoAry[$_rowNum][] = 'InvalidPerformanceCode';
				$_performance_code = '';
			}
			else {
				## 5. check duplicate record
//				$activity = $_activityNameEng ? $_activityNameEng : $_activityNameChi;
				$check_result = check_duplicate_jinying_record($_userID,$AcademicYearID,$_performance_code,$Semester,$_activityNameEng,$_activityNameChi);
				if ($check_result) {
					$errorMsgAssoAry[$_rowNum][] = 'RecordExist';
				}
				else {
					$check_result = check_duplicate_jinying_record_in_excel($data,$_webSAMS,$Item,$_activityNameEng,$_activityNameChi);
					if ($check_result) {
						$errorMsgAssoAry[$_rowNum][] = 'RecordDuplicate';
					}
				}		
			}					
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_userID)."', '".
						$lpf->Get_Safe_Sql_Query($AcademicYearID)."', '".
						$lpf->Get_Safe_Sql_Query($Semester)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', '".
						$lpf->Get_Safe_Sql_Query($_className)."', '".
						$lpf->Get_Safe_Sql_Query($_classNumber)."', '".
						$lpf->Get_Safe_Sql_Query($_webSAMS)."', '".
						$lpf->Get_Safe_Sql_Query($_studentName)."', '".
						$lpf->Get_Safe_Sql_Query($_performance)."', ".
						"NULL, '".
						$lpf->Get_Safe_Sql_Query($_activityNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameChi)."', ".
						"NULL, NULL, NULL, ".
						"NULL, NULL, NULL, NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}


function validate_BTP_IC($data, $studentWebSAMS_array, $Item, $AcademicYearID, $Semester) {
	global $Lang, $lpf;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_webSAMSRegNo = '#'.$_webSAMS;		// add prefix # as INTRANET_USER inlcudes it
		$_studentName = trim($_row[3]);
		$_activityDate = trim($_row[4]);
		if (is_numeric($_activityDate)) {
			$_activityDate = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($_activityDate));
		}
		$_activityNameEng = trim($_row[5]);
		$_activityNameChi = trim($_row[6]);
		$_organizationNameEng = trim($_row[7]);
		$_organizationNameChi = trim($_row[8]);
		$_performance = trim($_row[9]);
		$_awardNameEng = trim($_row[10]);
		$_awardNameChi = trim($_row[11]);
		$_recommendMerit = trim($_row[12]);
		
		if (!empty($_recommendMerit)) {
			$_recommendMerit = getRecommendAwardCode($_recommendMerit);
		}
		
		## 1. check if class name + class number exist or not
		$_userID = '';
		if ( $_webSAMS != '' && isset($studentWebSAMS_array[$_webSAMSRegNo]['UserID'])) {
			if (($_studentName != $studentWebSAMS_array[$_webSAMSRegNo]['StudentNameEng']) && ($_studentName != $studentWebSAMS_array[$_webSAMSRegNo]['StudentNameChi'])) {
				$errorMsgAssoAry[$_rowNum][] = 'StudentNameNotMatch';
			} 
			else {
				$_userID = $studentWebSAMS_array[$_webSAMSRegNo]['UserID'];
			}
		}
		else {
			$errorMsgAssoAry[$_rowNum][] = 'StudentNotFound';
		}
		
		## 2. check if activity name is empty or not
		if (empty($_activityNameEng) && empty($_activityNameChi)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyActivity';
		}
		
		## 3. check if performance is empty or not
		if (empty($_performance)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyPerformance';
			$_performance_code = '';
		}
		else {
			## 4. check if performance is in preset list or not
			$_performance_code = array_search($_performance,$Lang['iPortfolio']['JinYing']['CodeName'][$Item]);
			if ($_performance_code === false) {
				$errorMsgAssoAry[$_rowNum][] = 'InvalidPerformanceCode';
				$_performance_code = '';
			}
			## 5. check if performance match the award or not
			else if ($_performance_code != 'BTP-IC-P-03' && (!empty($_awardNameEng) || !empty($_awardNameChi) || !empty($_recommendMerit))) {
				$errorMsgAssoAry[$_rowNum][] = 'PerformanceConflictAward';
				$_awardNameEng = '';
				$_awardNameChi = '';
				$_recommendMerit = '';
			}
			else {
				## 6. check duplicate record
//				$activity = $_activityNameEng ? $_activityNameEng : $_activityNameChi;
			    $check_result = check_duplicate_jinying_record($_userID,$AcademicYearID,$_performance_code,$Semester,$_activityNameEng,$_activityNameChi,$recordID='',$_awardNameEng,$_awardNameChi);
				if ($check_result) {
					$errorMsgAssoAry[$_rowNum][] = 'RecordExist';
				}
				else {
				    $check_result = check_duplicate_jinying_record_in_excel_btp_ic($data,$_webSAMS,$_activityNameEng,$_activityNameChi,$_awardNameEng,$_awardNameChi);
					if ($check_result) {
						$errorMsgAssoAry[$_rowNum][] = 'RecordDuplicate';
					}
				}		
			}					
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_userID)."', '".
						$lpf->Get_Safe_Sql_Query($AcademicYearID)."', '".
						$lpf->Get_Safe_Sql_Query($Semester)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', '".
						$lpf->Get_Safe_Sql_Query($_className)."', '".
						$lpf->Get_Safe_Sql_Query($_classNumber)."', '".
						$lpf->Get_Safe_Sql_Query($_webSAMS)."', '".
						$lpf->Get_Safe_Sql_Query($_studentName)."', '".
						$lpf->Get_Safe_Sql_Query($_performance)."', '".
						$lpf->Get_Safe_Sql_Query($_activityDate)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameChi)."', '".
						$lpf->Get_Safe_Sql_Query($_awardNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_awardNameChi)."', '".
						$lpf->Get_Safe_Sql_Query($_recommendMerit)."', '".
						$lpf->Get_Safe_Sql_Query($_organizationNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_organizationNameChi)."', ".
						"NULL, NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}


## validate functions for update start
function validate_item_4update_standard($data, $Item, $recordIDPos=8) {
	global $Lang, $lpf, $ljy;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();
	
	$_recordIDAry = array();
	$foundRecordIDs = array();
	for ($i=0; $i<$numOfData; $i++) {
		$recordID =  substr(trim($data[$i][$recordIDPos]),4);
		if ($recordID) {
			$_recordIDAry[] = $recordID;
			$data[$i][$recordIDPos] = $recordID;
		}
	}
	if (count($_recordIDAry)) {
		$foundRecordIDs = $ljy->getRecordID($_recordIDAry);
	}
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_performance = trim($_row[4]);
		$_recordID = trim($_row[$recordIDPos]);
		
		## 1. check if record exist or not
		if (!in_array($_recordID, $foundRecordIDs)) {
			$errorMsgAssoAry[$_rowNum][] = 'RecordNotFound';
		}
		
		## 2. check if performance is empty or not
		if (empty($_performance)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyPerformance';
			$_performance_code = '';
		}
		else {
			## 3. check if performance is in preset list or not
			$_performance_code = array_search($_performance,$Lang['iPortfolio']['JinYing']['CodeName'][$Item]);
			if ($_performance_code === false) {
				$errorMsgAssoAry[$_rowNum][] = 'InvalidPerformanceCode';
				$_performance_code = '';
			}
			else {
				## 4. check duplicate record
				$check_result = check_duplicate_jinying_record_by_RecordID($_recordID, $_performance_code);
				if ($check_result) {
					$errorMsgAssoAry[$_rowNum][] = 'RecordExist';
				}
			}					
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_recordID)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', '".
						$lpf->Get_Safe_Sql_Query($_performance)."', ".
						"NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ".
						"NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}

function validate_item_4update_with_activity($data, $Item) {
	global $Lang, $lpf, $ljy;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();
	
	$_recordIDAry = array();
	$foundRecordIDs = array();
	for ($i=0; $i<$numOfData; $i++) {
		$recordID =  substr(trim($data[$i][9]),4);
		if ($recordID) {
			$_recordIDAry[] = $recordID;
			$data[$i][9] = $recordID;
		}
	}
	if (count($_recordIDAry)) {
		$foundRecordIDs = $ljy->getRecordID($_recordIDAry);
	}
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_activityDate = trim($_row[4]);
		if (is_numeric($_activityDate)) {
			$_activityDate = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($_activityDate));
		}
		$_activityNameEng = trim($_row[5]);
		$_activityNameChi = trim($_row[6]);
		$_performance = trim($_row[7]);
		$_recordID = trim($_row[9]);
		
		## 1. check if record exist or not
		if (!in_array($_recordID, $foundRecordIDs)) {
			$errorMsgAssoAry[$_rowNum][] = 'RecordNotFound';
		}
		
		## 2. check if activity name is empty or not
		if (empty($_activityNameEng) && empty($_activityNameChi)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyActivity';
		}
		
		## 3. check if performance is empty or not
		if (empty($_performance)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyPerformance';
			$_performance_code = '';
		}
		else {
			## 4. check if performance is in preset list or not
			$_performance_code = array_search($_performance,$Lang['iPortfolio']['JinYing']['CodeName'][$Item]);
			if ($_performance_code === false) {
				$errorMsgAssoAry[$_rowNum][] = 'InvalidPerformanceCode';
				$_performance_code = '';
			}
			else {
				## 5. check duplicate record
//				$activity = $_activityNameEng ? $_activityNameEng : $_activityNameChi;
				$check_result = check_duplicate_jinying_record_by_RecordID($_recordID, $_performance_code, $_activityNameEng,$_activityNameChi);
				if ($check_result) {
					$errorMsgAssoAry[$_rowNum][] = 'RecordExist';
				}		
			}					
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_recordID)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', '".
						$lpf->Get_Safe_Sql_Query($_performance)."', '".
						$lpf->Get_Safe_Sql_Query($_activityDate)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameChi)."', ".
						"NULL, NULL, NULL, NULL, NULL, NULL, NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}

function validate_OFS_SI_4update($data, $Item) {
	global $Lang, $lpf, $ljy;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();

	$_recordIDAry = array();
	$foundRecordIDs = array();
	for ($i=0; $i<$numOfData; $i++) {
		$recordID =  substr(trim($data[$i][10]),4);
		if ($recordID) {
			$_recordIDAry[] = $recordID;
			$data[$i][10] = $recordID;
		}
	}
	if (count($_recordIDAry)) {
		$foundRecordIDs = $ljy->getRecordID($_recordIDAry);
	}
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_activityNameEng = trim($_row[4]);
		$_activityNameChi = trim($_row[5]);
		$_performance = trim($_row[6]);
		$_recommendMerit = trim($_row[7]);
		if (!empty($_recommendMerit)) {
			$_recommendMerit = getRecommendAwardCode($_recommendMerit);
		}
		$_recordID = trim($_row[10]);
		
		## 1. check if record exist or not
		if (!in_array($_recordID, $foundRecordIDs)) {
			$errorMsgAssoAry[$_rowNum][] = 'RecordNotFound';
		}
		
		## 2. check if activity name is empty or not
		if (empty($_activityNameEng) && empty($_activityNameChi)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyService';
		}
		
		## 3. check if performance is empty or not
		if (empty($_performance)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyPerformance';
			$_performance_code = '';
		}
		else {
			## 4. check if performance is in preset list or not
			$_performance_code = array_search($_performance,$Lang['iPortfolio']['JinYing']['CodeName'][$Item]);
			if ($_performance_code === false) {
				$errorMsgAssoAry[$_rowNum][] = 'InvalidPerformanceCode';
				$_performance_code = '';
			}
			## 5. check if performance match the award or not
			else if ($_performance_code != 'OFS-SI-P-01' && (!empty($_recommendMerit))) {
				$errorMsgAssoAry[$_rowNum][] = 'PerformanceConflictAward';
				$_awardNameEng = '';
				$_awardNameChi = '';
				$_recommendMerit = '';
			}
			else {
				## 6. check duplicate record
//				$activity = $_activityNameEng ? $_activityNameEng : $_activityNameChi;
				$check_result = check_duplicate_jinying_record_by_RecordID($_recordID, $_performance_code, $_activityNameEng,$_activityNameChi);
				if ($check_result) {
					$errorMsgAssoAry[$_rowNum][] = 'RecordExist';
				}		
			}					
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_recordID)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', '".
						$lpf->Get_Safe_Sql_Query($_performance)."', ".
						"NULL, '".
						$lpf->Get_Safe_Sql_Query($_activityNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameChi)."', ".
						"NULL, NULL,'".
						$lpf->Get_Safe_Sql_Query($_recommendMerit)."', ".
						"NULL, NULL, NULL, NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}

function validate_OFS_SO_4update($data, $Item) {
	global $Lang, $lpf, $ljy;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();
	
	$_recordIDAry = array();
	$foundRecordIDs = array();
	for ($i=0; $i<$numOfData; $i++) {
		$recordID =  substr(trim($data[$i][6]),4);
		if ($recordID) {
			$_recordIDAry[] = $recordID;
			$data[$i][6] = $recordID;
		}
	}
	if (count($_recordIDAry)) {
		$foundRecordIDs = $ljy->getRecordID($_recordIDAry);
	}
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_serviceHours = trim($_row[4]);
		$_recordID = trim($_row[6]);
		
		## 1. check if record exist or not
		if (!in_array($_recordID, $foundRecordIDs)) {
			$errorMsgAssoAry[$_rowNum][] = 'RecordNotFound';
		}
		
		## 2. check if activity name is empty or not
		if (!is_numeric($_serviceHours)) {
			$errorMsgAssoAry[$_rowNum][] = 'NotNumeric';
		}
		else {
			$_performance_code = 'OFS-SO-P-01';
			## 3. check duplicate record
			$check_result = check_duplicate_jinying_record_by_RecordID($_recordID, $_performance_code);
			if ($check_result) {
				$errorMsgAssoAry[$_rowNum][] = 'RecordExist';
			}		
			
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_recordID)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', ".
						"NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '".
						$lpf->Get_Safe_Sql_Query($_serviceHours)."', ".												
						"NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}

function validate_BTP_EA_4update($data, $Item) {
	global $Lang, $lpf, $ljy;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();
	
	$_recordIDAry = array();
	$foundRecordIDs = array();
	for ($i=0; $i<$numOfData; $i++) {
		$recordID =  substr(trim($data[$i][9]),4);
		if ($recordID) {
			$_recordIDAry[] = $recordID;
			$data[$i][9] = $recordID;
		}
	}
	if (count($_recordIDAry)) {
		$foundRecordIDs = $ljy->getRecordID($_recordIDAry);
	}
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_activityNameEng = trim($_row[4]);
		$_activityNameChi = trim($_row[5]);
		$_performance = trim($_row[6]);
		$_recordID = trim($_row[9]);
		
		## 1. check if record exist or not
		if (!in_array($_recordID, $foundRecordIDs)) {
			$errorMsgAssoAry[$_rowNum][] = 'RecordNotFound';
		}
		
		## 2. check if club name is empty or not
		if (empty($_activityNameEng) && empty($_activityNameChi)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyClubName';
		}
		
		## 3. check if performance is empty or not
		if (empty($_performance)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyPerformance';
			$_performance_code = '';
		}
		else {
			## 4. check if performance is in preset list or not
			$_performance_code = array_search($_performance,$Lang['iPortfolio']['JinYing']['CodeName'][$Item]);
			if ($_performance_code === false) {
				$errorMsgAssoAry[$_rowNum][] = 'InvalidPerformanceCode';
				$_performance_code = '';
			}
			else {
				## 5. check duplicate record
//				$activity = $_activityNameEng ? $_activityNameEng : $_activityNameChi;
				$check_result = check_duplicate_jinying_record_by_RecordID($_recordID, $_performance_code, $_activityNameEng,$_activityNameChi);
				if ($check_result) {
					$errorMsgAssoAry[$_rowNum][] = 'RecordExist';
				}		
			}					
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_recordID)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', '".
						$lpf->Get_Safe_Sql_Query($_performance)."', ".
						"NULL, '".
						$lpf->Get_Safe_Sql_Query($_activityNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameChi)."', ".
						"NULL, NULL, NULL, ".
						"NULL, NULL, NULL, NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}

function validate_BTP_IC_4update($data, $Item) {
	global $Lang, $lpf, $ljy;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();
	
	$_recordIDAry = array();
	$foundRecordIDs = array();
	for ($i=0; $i<$numOfData; $i++) {
		$recordID =  substr(trim($data[$i][15]),4);
		if ($recordID) {
			$_recordIDAry[] = $recordID;
			$data[$i][15] = $recordID;
		}
	}
	if (count($_recordIDAry)) {
		$foundRecordIDs = $ljy->getRecordID($_recordIDAry);
	}
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_activityDate = trim($_row[4]);
		if (is_numeric($_activityDate)) {
			$_activityDate = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($_activityDate));
		}
		$_activityNameEng = trim($_row[5]);
		$_activityNameChi = trim($_row[6]);
		$_organizationNameEng = trim($_row[7]);
		$_organizationNameChi = trim($_row[8]);
		$_performance = trim($_row[9]);
		$_awardNameEng = trim($_row[10]);
		$_awardNameChi = trim($_row[11]);
		$_recommendMerit = trim($_row[12]);
		if (!empty($_recommendMerit)) {
			$_recommendMerit = getRecommendAwardCode($_recommendMerit);
		}
		$_recordID = trim($_row[15]);
		
		## 1. check if record exist or not
		if (!in_array($_recordID, $foundRecordIDs)) {
			$errorMsgAssoAry[$_rowNum][] = 'RecordNotFound';
		}
		
		## 2. check if activity name is empty or not
		if (empty($_activityNameEng) && empty($_activityNameChi)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyActivity';
		}
		
		## 3. check if performance is empty or not
		if (empty($_performance)) {
			$errorMsgAssoAry[$_rowNum][] = 'EmptyPerformance';
			$_performance_code = '';
		}
		else {
			## 4. check if performance is in preset list or not
			$_performance_code = array_search($_performance,$Lang['iPortfolio']['JinYing']['CodeName'][$Item]);
			if ($_performance_code === false) {
				$errorMsgAssoAry[$_rowNum][] = 'InvalidPerformanceCode';
				$_performance_code = '';
			}
			## 5. check if performance match the award or not
			else if ($_performance_code != 'BTP-IC-P-03' && (!empty($_awardNameEng) || !empty($_awardNameChi) || !empty($_recommendMerit))) {
				$errorMsgAssoAry[$_rowNum][] = 'PerformanceConflictAward';
				$_awardNameEng = '';
				$_awardNameChi = '';
				$_recommendMerit = '';
			}
			else {
				## 6. check duplicate record
//				$activity = $_activityNameEng ? $_activityNameEng : $_activityNameChi;
			    $check_result = check_duplicate_jinying_record_by_RecordID($_recordID, $_performance_code, $_activityNameEng,$_activityNameChi,$_awardNameEng,$_awardNameChi);
				if ($check_result) {
					$errorMsgAssoAry[$_rowNum][] = 'RecordExist';
				}		
			}					
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_recordID)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', '".
						$lpf->Get_Safe_Sql_Query($_performance)."', '".
						$lpf->Get_Safe_Sql_Query($_activityDate)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameChi)."', '".
						$lpf->Get_Safe_Sql_Query($_awardNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_awardNameChi)."', '".
						$lpf->Get_Safe_Sql_Query($_recommendMerit)."', '".
						$lpf->Get_Safe_Sql_Query($_organizationNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_organizationNameChi)."', ".
						"NULL, NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}
// end validate update functions


## validate functions for delete start
function validate_item_4delete_standard($data, $Item, $recordIDPos=8) {
	global $Lang, $lpf, $ljy;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();
	
	$_recordIDAry = array();
	$foundRecordIDs = array();
	for ($i=0; $i<$numOfData; $i++) {
		$recordID =  substr(trim($data[$i][$recordIDPos]),4);
		if ($recordID) {
			$_recordIDAry[] = $recordID;
			$data[$i][$recordIDPos] = $recordID;
		}
	}
	if (count($_recordIDAry)) {
		$foundRecordIDs = $ljy->getRecordID($_recordIDAry);
	}
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_recordID = trim($_row[$recordIDPos]);
		$_performance = trim($_row[4]);
		$_performance_code = array_search($_performance,$Lang['iPortfolio']['JinYing']['CodeName'][$Item]);
		
		## 1. check if record exist or not
		if (!in_array($_recordID, $foundRecordIDs)) {
			$errorMsgAssoAry[$_rowNum][] = 'RecordNotFound';
		}
		
		## 2. check if performance is empty or not
		if (empty($_performance)) {
			$_performance_code = '';
		}
		else {
			$_performance_code = array_search($_performance,$Lang['iPortfolio']['JinYing']['CodeName'][$Item]);
			if ($_performance_code === false) {
				$_performance_code = '';
			}
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_recordID)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', '".
						$lpf->Get_Safe_Sql_Query($_performance)."', ".
						"NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ".
						"NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}

function validate_item_4delete_with_activity($data, $Item) {
	global $Lang, $lpf, $ljy;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();
	
	$_recordIDAry = array();
	$foundRecordIDs = array();
	for ($i=0; $i<$numOfData; $i++) {
		$recordID =  substr(trim($data[$i][9]),4);
		if ($recordID) {
			$_recordIDAry[] = $recordID;
			$data[$i][9] = $recordID;
		}
	}
	if (count($_recordIDAry)) {
		$foundRecordIDs = $ljy->getRecordID($_recordIDAry);
	}
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_activityDate = trim($_row[4]);
		if (is_numeric($_activityDate)) {
			$_activityDate = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($_activityDate));
		}
		$_activityNameEng = trim($_row[5]);
		$_activityNameChi = trim($_row[6]);
		$_performance = trim($_row[7]);
		$_recordID = trim($_row[9]);
		
		## 1. check if record exist or not
		if (!in_array($_recordID, $foundRecordIDs)) {
			$errorMsgAssoAry[$_rowNum][] = 'RecordNotFound';
		}
		
		if (empty($_performance)) {
			$_performance_code = '';
		}
		else {
			$_performance_code = array_search($_performance,$Lang['iPortfolio']['JinYing']['CodeName'][$Item]);
			if ($_performance_code === false) {
				$_performance_code = '';
			}
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_recordID)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', '".
						$lpf->Get_Safe_Sql_Query($_performance)."', '".
						$lpf->Get_Safe_Sql_Query($_activityDate)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameChi)."', ".
						"NULL, NULL, NULL, NULL, NULL, NULL, NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}

function validate_OFS_SI_4delete($data, $Item) {
	global $Lang, $lpf, $ljy;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();

	$_recordIDAry = array();
	$foundRecordIDs = array();
	for ($i=0; $i<$numOfData; $i++) {
		$recordID =  substr(trim($data[$i][10]),4);
		if ($recordID) {
			$_recordIDAry[] = $recordID;
			$data[$i][10] = $recordID;
		}
	}
	if (count($_recordIDAry)) {
		$foundRecordIDs = $ljy->getRecordID($_recordIDAry);
	}
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_activityNameEng = trim($_row[4]);
		$_activityNameChi = trim($_row[5]);
		$_performance = trim($_row[6]);
		$_recommendMerit = trim($_row[7]);
		if (!empty($_recommendMerit)) {
			$_recommendMerit = getRecommendAwardCode($_recommendMerit);
		}
		$_recordID = trim($_row[10]);
		
		## 1. check if record exist or not
		if (!in_array($_recordID, $foundRecordIDs)) {
			$errorMsgAssoAry[$_rowNum][] = 'RecordNotFound';
		}
		
		if (empty($_performance)) {
			$_performance_code = '';
		}
		else {
			$_performance_code = array_search($_performance,$Lang['iPortfolio']['JinYing']['CodeName'][$Item]);
			if ($_performance_code === false) {
				$_performance_code = '';
			}
			## 5. check if performance match the award or not
			else if ($_performance_code != 'OFS-SI-P-01' && (!empty($_recommendMerit))) {
				$_awardNameEng = '';
				$_awardNameChi = '';
				$_recommendMerit = '';
			}
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_recordID)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', '".
						$lpf->Get_Safe_Sql_Query($_performance)."', ".
						"NULL, '".
						$lpf->Get_Safe_Sql_Query($_activityNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameChi)."', ".
						"NULL, NULL,'".
						$lpf->Get_Safe_Sql_Query($_recommendMerit)."', ".
						"NULL, NULL, NULL, NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}

function validate_OFS_SO_4delete($data, $Item) {
	global $Lang, $lpf, $ljy;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();
	
	$_recordIDAry = array();
	$foundRecordIDs = array();
	for ($i=0; $i<$numOfData; $i++) {
		$recordID =  substr(trim($data[$i][6]),4);
		if ($recordID) {
			$_recordIDAry[] = $recordID;
			$data[$i][6] = $recordID;
		}
	}
	if (count($_recordIDAry)) {
		$foundRecordIDs = $ljy->getRecordID($_recordIDAry);
	}
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_serviceHours = trim($_row[4]);
		$_recordID = trim($_row[6]);
		
		## 1. check if record exist or not
		if (!in_array($_recordID, $foundRecordIDs)) {
			$errorMsgAssoAry[$_rowNum][] = 'RecordNotFound';
		}
		
		if (!is_numeric($_serviceHours)) {
			$_performance_code = '';
		}
		else {
			$_performance_code = 'OFS-SO-P-01';
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_recordID)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', ".
						"NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '".
						$lpf->Get_Safe_Sql_Query($_serviceHours)."', ".												
						"NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}

function validate_BTP_EA_4delete($data, $Item) {
	global $Lang, $lpf, $ljy;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();
	
	$_recordIDAry = array();
	$foundRecordIDs = array();
	for ($i=0; $i<$numOfData; $i++) {
		$recordID =  substr(trim($data[$i][9]),4);
		if ($recordID) {
			$_recordIDAry[] = $recordID;
			$data[$i][9] = $recordID;
		}
	}
	if (count($_recordIDAry)) {
		$foundRecordIDs = $ljy->getRecordID($_recordIDAry);
	}
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_activityNameEng = trim($_row[4]);
		$_activityNameChi = trim($_row[5]);
		$_performance = trim($_row[6]);
		$_recordID = trim($_row[9]);
		
		## 1. check if record exist or not
		if (!in_array($_recordID, $foundRecordIDs)) {
			$errorMsgAssoAry[$_rowNum][] = 'RecordNotFound';
		}
		
		
		## 3. check if performance is empty or not
		if (empty($_performance)) {
			$_performance_code = '';
		}
		else {
			$_performance_code = array_search($_performance,$Lang['iPortfolio']['JinYing']['CodeName'][$Item]);
			if ($_performance_code === false) {
				$_performance_code = '';
			}
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_recordID)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', '".
						$lpf->Get_Safe_Sql_Query($_performance)."', ".
						"NULL, '".
						$lpf->Get_Safe_Sql_Query($_activityNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameChi)."', ".
						"NULL, NULL, NULL, ".
						"NULL, NULL, NULL, NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}

function validate_BTP_IC_4delete($data, $Item) {
	global $Lang, $lpf, $ljy;

	$numOfData = count($data);
	$errorMsgAssoAry = array();
	$insertAry = array();
	
	$_recordIDAry = array();
	$foundRecordIDs = array();
	for ($i=0; $i<$numOfData; $i++) {
		$recordID =  substr(trim($data[$i][15]),4);
		if ($recordID) {
			$_recordIDAry[] = $recordID;
			$data[$i][15] = $recordID;
		}
	}
	if (count($_recordIDAry)) {
		$foundRecordIDs = $ljy->getRecordID($_recordIDAry);
	}
	
	for ($i=0; $i<$numOfData; $i++) {
		$_rowNum = $i+2;
		$_row = $data[$i];
		$_activityDate = trim($_row[4]);
		if (is_numeric($_activityDate)) {
			$_activityDate = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($_activityDate));
		}
		$_activityNameEng = trim($_row[5]);
		$_activityNameChi = trim($_row[6]);
		$_organizationNameEng = trim($_row[7]);
		$_organizationNameChi = trim($_row[8]);
		$_performance = trim($_row[9]);
		$_awardNameEng = trim($_row[10]);
		$_awardNameChi = trim($_row[11]);
		$_recommendMerit = trim($_row[12]);
		if (!empty($_recommendMerit)) {
			$_recommendMerit = getRecommendAwardCode($_recommendMerit);
		}
		$_recordID = trim($_row[15]);
		
		## 1. check if record exist or not
		if (!in_array($_recordID, $foundRecordIDs)) {
			$errorMsgAssoAry[$_rowNum][] = 'RecordNotFound';
		}
		
		## 3. check if performance is empty or not
		if (empty($_performance)) {
			$_performance_code = '';
		}
		else {
			$_performance_code = array_search($_performance,$Lang['iPortfolio']['JinYing']['CodeName'][$Item]);
			if ($_performance_code === false) {
				$_performance_code = '';
			}
			## 5. check if performance match the award or not
			else if ($_performance_code != 'BTP-IC-P-03' && (!empty($_awardNameEng) || !empty($_awardNameChi) || !empty($_recommendMerit))) {
				$_awardNameEng = '';
				$_awardNameChi = '';
				$_recommendMerit = '';
			}
		}
		
		$insertAry[] = " ('".$_SESSION['UserID']."', '".
						$lpf->Get_Safe_Sql_Query($_recordID)."', '".
						$lpf->Get_Safe_Sql_Query($_performance_code)."', '".
						$lpf->Get_Safe_Sql_Query($_performance)."', '".
						$lpf->Get_Safe_Sql_Query($_activityDate)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_activityNameChi)."', '".
						$lpf->Get_Safe_Sql_Query($_awardNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_awardNameChi)."', '".
						$lpf->Get_Safe_Sql_Query($_recommendMerit)."', '".
						$lpf->Get_Safe_Sql_Query($_organizationNameEng)."', '".
						$lpf->Get_Safe_Sql_Query($_organizationNameChi)."', ".
						"NULL, NOW(), '".$_SESSION['UserID']."')";
		
	}	// end loop number of data

	return array($errorMsgAssoAry,$insertAry);	
}

## validate functions for delete end


###################################################################################################

function get_error_table_header($Item, $showRecordID=false) {
	global $Lang;

	switch($Item) {
		case 'BTP-EA':
		case 'BTP-IC':
		case 'OFS-SI':
		case 'OFS-SO':
			$columnCode = $Item;
			break;	
		case 'BSQ-EC':
		case 'BSQ-TA':
		case 'EPW-TA':
			$columnCode = 'WITH-ACTIVITY';
			break;	
		default:
			$columnCode = 'STANDARD';
			break;
	}
	
	$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\n";
		$x .= '<thead>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<th>'.$Lang['General']['ImportArr']['Row'].'</th>'."\n";
				$x .= ($showRecordID) ? '<th>'.$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['STANDARD']['RecordID'].'</th>'."\n" : '';				
			foreach((array)$Lang['iPortfolio']['JinYing']['ImportColumns'][$columnCode] as $h) {
				$x .= '<th>'.$h.'</th>'."\n";
			}				
				$x .= '<th>'.$Lang['General']['Remark'].'</th>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</thead>'."\n";
		$x .= '<tbody>'."\n";
	return $x;
}

function get_error_table_item_with_term($data, $errorMsgAssoAry, $Item){
	global $Lang;
	$x = get_error_table_header($Item);
	
	foreach ((array)$errorMsgAssoAry as $_rowNum => $_errorAry) {
		$aryIndex = $_rowNum - 2;
		$_row = $data[$aryIndex];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_studentName = trim($_row[3]);
		$_performance = trim($_row[4]);
		
		$_errorDisplayAry = array();

		
		if (in_array('StudentNotFound', $_errorAry)) {
			$_webSAMS = '<span class="tabletextrequire">'.($_webSAMS ? $_webSAMS : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['StudentNotFound'];
		}

		if (in_array('StudentNameNotMatch', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.($_studentName ? $_studentName : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['StudentNameNotMatch'];
		}
		
		if (in_array('EmptyPerformance', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyPerformance'];
		}

		if (in_array('InvalidPerformanceCode', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">'.$_performance.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['InvalidPerformanceCode'];
		}

		if (in_array('RecordExist', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordExist'];
		}

		if (in_array('EmptySemester', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptySemester'];
		}
		
		$_errorDisplay = '- '.implode('<br />- ', $_errorDisplayAry);
		
		$x .= '<tr>'."\n";
			$x .= '<td>'.$_rowNum.'</td>'."\n";
			$x .= '<td>'.$_className.'</td>'."\n";
			$x .= '<td>'.$_classNumber.'</td>'."\n";
			$x .= '<td>'.$_webSAMS.'</td>'."\n";
			$x .= '<td>'.$_studentName.'</td>'."\n";
			$x .= '<td>'.$_performance.'</td>'."\n";
			$x .= '<td>'.$_errorDisplay.'</td>'."\n";
		$x .= '</tr>'."\n";
	}	// foreach
	$x .= '</tbody>'."\n";
	$x .= '</table>'."\n";
	return $x;	
}

function get_error_table_item_without_term($data, $errorMsgAssoAry, $Item){
	global $Lang;
	$x = get_error_table_header($Item);
	
	foreach ((array)$errorMsgAssoAry as $_rowNum => $_errorAry) {
		$aryIndex = $_rowNum - 2;
		$_row = $data[$aryIndex];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_studentName = trim($_row[3]);
		$_performance = trim($_row[4]);
		
		$_errorDisplayAry = array();

		
		if (in_array('StudentNotFound', $_errorAry)) {
			$_webSAMS = '<span class="tabletextrequire">'.($_webSAMS ? $_webSAMS : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['StudentNotFound'];
		}

		if (in_array('StudentNameNotMatch', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.($_studentName ? $_studentName : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['StudentNameNotMatch'];
		}
		
		if (in_array('EmptyPerformance', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyPerformance'];
		}

		if (in_array('InvalidPerformanceCode', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">'.$_performance.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['InvalidPerformanceCode'];
		}

		if (in_array('RecordExist', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordExist'];
		}

		$_errorDisplay = '- '.implode('<br />- ', $_errorDisplayAry);
		
		$x .= '<tr>'."\n";
			$x .= '<td>'.$_rowNum.'</td>'."\n";
			$x .= '<td>'.$_className.'</td>'."\n";
			$x .= '<td>'.$_classNumber.'</td>'."\n";
			$x .= '<td>'.$_webSAMS.'</td>'."\n";
			$x .= '<td>'.$_studentName.'</td>'."\n";
			$x .= '<td>'.$_performance.'</td>'."\n";
			$x .= '<td>'.$_errorDisplay.'</td>'."\n";
		$x .= '</tr>'."\n";
	}	// foreach
	$x .= '</tbody>'."\n";
	$x .= '</table>'."\n";
	return $x;	
}

function get_error_table_item_with_activity($data, $errorMsgAssoAry, $Item){
	global $Lang;
	$x = get_error_table_header($Item);
	
	foreach ((array)$errorMsgAssoAry as $_rowNum => $_errorAry) {
		$aryIndex = $_rowNum - 2;
		$_row = $data[$aryIndex];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_studentName = trim($_row[3]);
		$_activityDate = trim($_row[4]);
		if (is_numeric($_activityDate)) {
			$_activityDate = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($_activityDate));
		}
		$_activityNameEng = trim($_row[5]);
		$_activityNameChi = trim($_row[6]);
		$_performance = trim($_row[7]);
		
		$_errorDisplayAry = array();
		
		if (in_array('StudentNotFound', $_errorAry)) {
			$_webSAMS = '<span class="tabletextrequire">'.($_webSAMS ? $_webSAMS : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['StudentNotFound'];
		}

		if (in_array('StudentNameNotMatch', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.($_studentName ? $_studentName : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['StudentNameNotMatch'];
		}
		
		if (in_array('EmptyActivity', $_errorAry)) {
			$_activityNameEng = '<span class="tabletextrequire">***</span>';	
			$_activityNameChi = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyActivity'];
		}

		if (in_array('EmptyPerformance', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyPerformance'];
		}

		if (in_array('InvalidPerformanceCode', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">'.$_performance.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['InvalidPerformanceCode'];
		}

		if (in_array('RecordExist', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordExist'];
		}

		if (in_array('RecordDuplicate', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordDuplicate'];
		}
		
		$_errorDisplay = '- '.implode('<br />- ', $_errorDisplayAry);
		
		$x .= '<tr>'."\n";
			$x .= '<td>'.$_rowNum.'</td>'."\n";
			$x .= '<td>'.$_className.'</td>'."\n";
			$x .= '<td>'.$_classNumber.'</td>'."\n";
			$x .= '<td>'.$_webSAMS.'</td>'."\n";
			$x .= '<td>'.$_studentName.'</td>'."\n";
			$x .= '<td>'.$_activityDate.'</td>'."\n";
			$x .= '<td>'.$_activityNameEng.'</td>'."\n";
			$x .= '<td>'.$_activityNameChi.'</td>'."\n";
			$x .= '<td>'.$_performance.'</td>'."\n";
			$x .= '<td>'.$_errorDisplay.'</td>'."\n";
		$x .= '</tr>'."\n";
	}	// foreach
	$x .= '</tbody>'."\n";
	$x .= '</table>'."\n";
	return $x;	
}

function get_error_table_OFS_SI($data, $errorMsgAssoAry, $Item){
	global $Lang;
	$x = get_error_table_header($Item);
	
	foreach ((array)$errorMsgAssoAry as $_rowNum => $_errorAry) {
		$aryIndex = $_rowNum - 2;
		$_row = $data[$aryIndex];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_studentName = trim($_row[3]);
		$_activityNameEng = trim($_row[4]);
		$_activityNameChi = trim($_row[5]);
		$_performance = trim($_row[6]);
		$_recommendMerit = trim($_row[7]);
		if (!empty($_recommendMerit)) {
			$_recommendMerit = substr($_recommendMerit,2);
		}
		
		$_errorDisplayAry = array();
		
		if (in_array('StudentNotFound', $_errorAry)) {
			$_webSAMS = '<span class="tabletextrequire">'.($_webSAMS ? $_webSAMS : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['StudentNotFound'];
		}

		if (in_array('StudentNameNotMatch', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.($_studentName ? $_studentName : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['StudentNameNotMatch'];
		}
		
		if (in_array('EmptyService', $_errorAry)) {
			$_activityNameEng = '<span class="tabletextrequire">***</span>';	
			$_activityNameChi = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyService'];
		}

		if (in_array('EmptyPerformance', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyPerformance'];
		}

		if (in_array('InvalidPerformanceCode', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">'.$_performance.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['InvalidPerformanceCode'];
		}

		if (in_array('PerformanceConflictAward', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">'.$_performance.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['PerformanceConflictAward'];
		}

		if (in_array('RecordExist', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordExist'];
		}

		if (in_array('RecordDuplicate', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordDuplicate'];
		}
		
		$_errorDisplay = '- '.implode('<br />- ', $_errorDisplayAry);
		
		$x .= '<tr>'."\n";
			$x .= '<td>'.$_rowNum.'</td>'."\n";
			$x .= '<td>'.$_className.'</td>'."\n";
			$x .= '<td>'.$_classNumber.'</td>'."\n";
			$x .= '<td>'.$_webSAMS.'</td>'."\n";
			$x .= '<td>'.$_studentName.'</td>'."\n";
			$x .= '<td>'.$_activityNameEng.'</td>'."\n";
			$x .= '<td>'.$_activityNameChi.'</td>'."\n";
			$x .= '<td>'.$_performance.'</td>'."\n";
			$x .= '<td>'.$_recommendMerit.'</td>'."\n";
			$x .= '<td>'.$_errorDisplay.'</td>'."\n";
		$x .= '</tr>'."\n";
	}	// foreach
	$x .= '</tbody>'."\n";
	$x .= '</table>'."\n";
	return $x;	
}


function get_error_table_OFS_SO($data, $errorMsgAssoAry, $Item){
	global $Lang;
	$x = get_error_table_header($Item);
	
	foreach ((array)$errorMsgAssoAry as $_rowNum => $_errorAry) {
		$aryIndex = $_rowNum - 2;
		$_row = $data[$aryIndex];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_studentName = trim($_row[3]);
		$_serviceHours = trim($_row[4]);
		
		$_errorDisplayAry = array();
		
		if (in_array('StudentNotFound', $_errorAry)) {
			$_webSAMS = '<span class="tabletextrequire">'.($_webSAMS ? $_webSAMS : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['StudentNotFound'];
		}

		if (in_array('StudentNameNotMatch', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.($_studentName ? $_studentName : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['StudentNameNotMatch'];
		}
		
		if (in_array('NotNumeric', $_errorAry)) {
			$_serviceHours = '<span class="tabletextrequire">'.$_serviceHours.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['NotNumeric'];
		}

		if (in_array('RecordExist', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordExist'];
		}
		
		$_errorDisplay = '- '.implode('<br />- ', $_errorDisplayAry);
		
		$x .= '<tr>'."\n";
			$x .= '<td>'.$_rowNum.'</td>'."\n";
			$x .= '<td>'.$_className.'</td>'."\n";
			$x .= '<td>'.$_classNumber.'</td>'."\n";
			$x .= '<td>'.$_webSAMS.'</td>'."\n";
			$x .= '<td>'.$_studentName.'</td>'."\n";
			$x .= '<td>'.$_serviceHours.'</td>'."\n";
			$x .= '<td>'.$_errorDisplay.'</td>'."\n";
		$x .= '</tr>'."\n";
	}	// foreach
	$x .= '</tbody>'."\n";
	$x .= '</table>'."\n";
	return $x;	
}


function get_error_table_BTP_EA($data, $errorMsgAssoAry, $Item){
	global $Lang;
	$x = get_error_table_header($Item);
	
	foreach ((array)$errorMsgAssoAry as $_rowNum => $_errorAry) {
		$aryIndex = $_rowNum - 2;
		$_row = $data[$aryIndex];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_studentName = trim($_row[3]);
		$_activityNameEng = trim($_row[4]);
		$_activityNameChi = trim($_row[5]);
		$_performance = trim($_row[6]);
		
		$_errorDisplayAry = array();
		
		if (in_array('StudentNotFound', $_errorAry)) {
			$_webSAMS = '<span class="tabletextrequire">'.($_webSAMS ? $_webSAMS : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['StudentNotFound'];
		}

		if (in_array('StudentNameNotMatch', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.($_studentName ? $_studentName : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['StudentNameNotMatch'];
		}
		
		if (in_array('EmptyClubName', $_errorAry)) {
			$_activityNameEng = '<span class="tabletextrequire">***</span>';	
			$_activityNameChi = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyClubName'];
		}

		if (in_array('EmptyPerformance', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyPerformance'];
		}

		if (in_array('InvalidPerformanceCode', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">'.$_performance.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['InvalidPerformanceCode'];
		}

		if (in_array('RecordExist', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordExist'];
		}
		
		if (in_array('RecordDuplicate', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordDuplicate'];
		}
		
		$_errorDisplay = '- '.implode('<br />- ', $_errorDisplayAry);
		
		$x .= '<tr>'."\n";
			$x .= '<td>'.$_rowNum.'</td>'."\n";
			$x .= '<td>'.$_className.'</td>'."\n";
			$x .= '<td>'.$_classNumber.'</td>'."\n";
			$x .= '<td>'.$_webSAMS.'</td>'."\n";
			$x .= '<td>'.$_studentName.'</td>'."\n";
			$x .= '<td>'.$_activityNameEng.'</td>'."\n";
			$x .= '<td>'.$_activityNameChi.'</td>'."\n";
			$x .= '<td>'.$_performance.'</td>'."\n";
			$x .= '<td>'.$_errorDisplay.'</td>'."\n";
		$x .= '</tr>'."\n";
	}	// foreach
	$x .= '</tbody>'."\n";
	$x .= '</table>'."\n";
	return $x;	
}


function get_error_table_BTP_IC($data, $errorMsgAssoAry, $Item){
	global $Lang;
	$x = get_error_table_header($Item);
	
	foreach ((array)$errorMsgAssoAry as $_rowNum => $_errorAry) {
		$aryIndex = $_rowNum - 2;
		$_row = $data[$aryIndex];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_studentName = trim($_row[3]);
		$_activityDate = trim($_row[4]);
		if (is_numeric($_activityDate)) {
			$_activityDate = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($_activityDate));
		}
		$_activityNameEng = trim($_row[5]);
		$_activityNameChi = trim($_row[6]);
		$_organizationNameEng = trim($_row[7]);
		$_organizationNameChi = trim($_row[8]);
		$_performance = trim($_row[9]);
		$_awardNameEng = trim($_row[10]);
		$_awardNameChi = trim($_row[11]);
		$_recommendMerit = trim($_row[12]);
		if (!empty($_recommendMerit)) {
			$_recommendMerit = substr($_recommendMerit,2);
		}
		
		$_errorDisplayAry = array();
		
		if (in_array('StudentNotFound', $_errorAry)) {
			$_webSAMS = '<span class="tabletextrequire">'.($_webSAMS ? $_webSAMS : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['StudentNotFound'];
		}

		if (in_array('StudentNameNotMatch', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.($_studentName ? $_studentName : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['StudentNameNotMatch'];
		}
		
		if (in_array('EmptyActivity', $_errorAry)) {
			$_activityNameEng = '<span class="tabletextrequire">***</span>';	
			$_activityNameChi = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyActivity'];
		}

		if (in_array('EmptyPerformance', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyPerformance'];
		}

		if (in_array('InvalidPerformanceCode', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">'.$_performance.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['InvalidPerformanceCode'];
		}

		if (in_array('PerformanceConflictAward', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">'.$_performance.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['PerformanceConflictAward'];
		}

		if (in_array('RecordExist', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordExist'];
		}

		if (in_array('RecordDuplicate', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordDuplicate'];
		}
		
		$_errorDisplay = '- '.implode('<br />- ', $_errorDisplayAry);
		
		$x .= '<tr>'."\n";
			$x .= '<td>'.$_rowNum.'</td>'."\n";
			$x .= '<td>'.$_className.'</td>'."\n";
			$x .= '<td>'.$_classNumber.'</td>'."\n";
			$x .= '<td>'.$_webSAMS.'</td>'."\n";
			$x .= '<td>'.$_studentName.'</td>'."\n";
			$x .= '<td>'.$_activityDate.'</td>'."\n";
			$x .= '<td>'.$_activityNameEng.'</td>'."\n";
			$x .= '<td>'.$_activityNameChi.'</td>'."\n";
			$x .= '<td>'.$_organizationNameEng.'</td>'."\n";
			$x .= '<td>'.$_organizationNameChi.'</td>'."\n";
			$x .= '<td>'.$_performance.'</td>'."\n";
			$x .= '<td>'.$_awardNameEng.'</td>'."\n";
			$x .= '<td>'.$_awardNameChi.'</td>'."\n";
			$x .= '<td>'.$_recommendMerit.'</td>'."\n";
			$x .= '<td>'.$_errorDisplay.'</td>'."\n";
		$x .= '</tr>'."\n";
	}	// foreach
	$x .= '</tbody>'."\n";
	$x .= '</table>'."\n";
	return $x;	
}


// error function for update
function get_error_table_item_4update_standard($data, $errorMsgAssoAry, $Item, $recordIDPos=8){
	global $Lang;
	$x = get_error_table_header($Item, true);	// include recordID
	
	foreach ((array)$errorMsgAssoAry as $_rowNum => $_errorAry) {
		$aryIndex = $_rowNum - 2;
		$_row = $data[$aryIndex];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_studentName = trim($_row[3]);
		$_performance = trim($_row[4]);
		$_recordID = trim($_row[$recordIDPos]);
		
		$_errorDisplayAry = array();

		
		if (in_array('RecordNotFound', $_errorAry)) {
			$_recordID = '<span class="tabletextrequire">'.($_recordID ? $_recordID : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordIDNotFound'];
		}
		
		if (in_array('EmptyPerformance', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyPerformance'];
		}

		if (in_array('InvalidPerformanceCode', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">'.$_performance.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['InvalidPerformanceCode'];
		}

		if (in_array('RecordExist', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordExist'];
		}

		$_errorDisplay = '- '.implode('<br />- ', $_errorDisplayAry);
		
		$x .= '<tr>'."\n";
			$x .= '<td>'.$_rowNum.'</td>'."\n";
			$x .= '<td>'.$_recordID.'</td>'."\n";
			$x .= '<td>'.$_className.'</td>'."\n";
			$x .= '<td>'.$_classNumber.'</td>'."\n";
			$x .= '<td>'.$_webSAMS.'</td>'."\n";
			$x .= '<td>'.$_studentName.'</td>'."\n";
			$x .= '<td>'.$_performance.'</td>'."\n";
			$x .= '<td>'.$_errorDisplay.'</td>'."\n";
		$x .= '</tr>'."\n";
	}	// foreach
	$x .= '</tbody>'."\n";
	$x .= '</table>'."\n";
	return $x;	
}

function get_error_table_item_4update_with_activity($data, $errorMsgAssoAry, $Item){
	global $Lang;
	$x = get_error_table_header($Item, true);	// include recordID
	
	foreach ((array)$errorMsgAssoAry as $_rowNum => $_errorAry) {
		$aryIndex = $_rowNum - 2;
		$_row = $data[$aryIndex];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_studentName = trim($_row[3]);
		$_activityDate = trim($_row[4]);
		if (is_numeric($_activityDate)) {
			$_activityDate = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($_activityDate));
		}
		$_activityNameEng = trim($_row[5]);
		$_activityNameChi = trim($_row[6]);
		$_performance = trim($_row[7]);
		$_recordID = trim($_row[9]);
		
		$_errorDisplayAry = array();
		
		if (in_array('RecordNotFound', $_errorAry)) {
			$_recordID = '<span class="tabletextrequire">'.($_recordID ? $_recordID : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordIDNotFound'];
		}
		
		if (in_array('EmptyActivity', $_errorAry)) {
			$_activityNameEng = '<span class="tabletextrequire">***</span>';	
			$_activityNameChi = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyActivity'];
		}

		if (in_array('EmptyPerformance', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyPerformance'];
		}

		if (in_array('InvalidPerformanceCode', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">'.$_performance.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['InvalidPerformanceCode'];
		}

		if (in_array('RecordExist', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordExist'];
		}
		
		$_errorDisplay = '- '.implode('<br />- ', $_errorDisplayAry);
		
		$x .= '<tr>'."\n";
			$x .= '<td>'.$_rowNum.'</td>'."\n";
			$x .= '<td>'.$_recordID.'</td>'."\n";
			$x .= '<td>'.$_className.'</td>'."\n";
			$x .= '<td>'.$_classNumber.'</td>'."\n";
			$x .= '<td>'.$_webSAMS.'</td>'."\n";
			$x .= '<td>'.$_studentName.'</td>'."\n";
			$x .= '<td>'.$_activityDate.'</td>'."\n";
			$x .= '<td>'.$_activityNameEng.'</td>'."\n";
			$x .= '<td>'.$_activityNameChi.'</td>'."\n";
			$x .= '<td>'.$_performance.'</td>'."\n";
			$x .= '<td>'.$_errorDisplay.'</td>'."\n";
		$x .= '</tr>'."\n";
	}	// foreach
	$x .= '</tbody>'."\n";
	$x .= '</table>'."\n";
	return $x;	
}

function get_error_table_OFS_SI_4update($data, $errorMsgAssoAry, $Item){
	global $Lang;
	
	$x = get_error_table_header($Item, true);	// include recordID
	
	foreach ((array)$errorMsgAssoAry as $_rowNum => $_errorAry) {
		$aryIndex = $_rowNum - 2;
		$_row = $data[$aryIndex];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_studentName = trim($_row[3]);
		$_activityNameEng = trim($_row[4]);
		$_activityNameChi = trim($_row[5]);
		$_performance = trim($_row[6]);
		$_recommendMerit = trim($_row[7]);
		if (!empty($_recommendMerit)) {
			$_recommendMerit = substr($_recommendMerit,2);
		}
		$_recordID = trim($_row[10]);
		
		$_errorDisplayAry = array();

		if (in_array('RecordNotFound', $_errorAry)) {
			$_recordID = '<span class="tabletextrequire">'.($_recordID ? $_recordID : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordIDNotFound'];
		}
		
		if (in_array('EmptyService', $_errorAry)) {
			$_activityNameEng = '<span class="tabletextrequire">***</span>';	
			$_activityNameChi = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyService'];
		}

		if (in_array('EmptyPerformance', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyPerformance'];
		}

		if (in_array('InvalidPerformanceCode', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">'.$_performance.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['InvalidPerformanceCode'];
		}

		if (in_array('PerformanceConflictAward', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">'.$_performance.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['PerformanceConflictAward'];
		}

		if (in_array('RecordExist', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordExist'];
		}
		
		$_errorDisplay = '- '.implode('<br />- ', $_errorDisplayAry);
		
		$x .= '<tr>'."\n";
			$x .= '<td>'.$_rowNum.'</td>'."\n";
			$x .= '<td>'.$_recordID.'</td>'."\n";
			$x .= '<td>'.$_className.'</td>'."\n";
			$x .= '<td>'.$_classNumber.'</td>'."\n";
			$x .= '<td>'.$_webSAMS.'</td>'."\n";
			$x .= '<td>'.$_studentName.'</td>'."\n";
			$x .= '<td>'.$_activityNameEng.'</td>'."\n";
			$x .= '<td>'.$_activityNameChi.'</td>'."\n";
			$x .= '<td>'.$_performance.'</td>'."\n";
			$x .= '<td>'.$_recommendMerit.'</td>'."\n";
			$x .= '<td>'.$_errorDisplay.'</td>'."\n";
		$x .= '</tr>'."\n";
	}	// foreach
	$x .= '</tbody>'."\n";
	$x .= '</table>'."\n";
	return $x;	
}

function get_error_table_OFS_SO_4update($data, $errorMsgAssoAry, $Item){
	global $Lang;
	$x = get_error_table_header($Item, true);	// include recordID
	
	foreach ((array)$errorMsgAssoAry as $_rowNum => $_errorAry) {
		$aryIndex = $_rowNum - 2;
		$_row = $data[$aryIndex];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_studentName = trim($_row[3]);
		$_serviceHours = trim($_row[4]);
		$_recordID = trim($_row[6]);
		
		$_errorDisplayAry = array();

		if (in_array('RecordNotFound', $_errorAry)) {
			$_recordID = '<span class="tabletextrequire">'.($_recordID ? $_recordID : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordIDNotFound'];
		}
		
		if (in_array('NotNumeric', $_errorAry)) {
			$_serviceHours = '<span class="tabletextrequire">'.$_serviceHours.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['NotNumeric'];
		}

		if (in_array('RecordExist', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordExist'];
		}
		
		$_errorDisplay = '- '.implode('<br />- ', $_errorDisplayAry);
		
		$x .= '<tr>'."\n";
			$x .= '<td>'.$_rowNum.'</td>'."\n";
			$x .= '<td>'.$_recordID.'</td>'."\n";
			$x .= '<td>'.$_className.'</td>'."\n";
			$x .= '<td>'.$_classNumber.'</td>'."\n";
			$x .= '<td>'.$_webSAMS.'</td>'."\n";
			$x .= '<td>'.$_studentName.'</td>'."\n";
			$x .= '<td>'.$_serviceHours.'</td>'."\n";
			$x .= '<td>'.$_errorDisplay.'</td>'."\n";
		$x .= '</tr>'."\n";
	}	// foreach
	$x .= '</tbody>'."\n";
	$x .= '</table>'."\n";
	return $x;	
}

function get_error_table_BTP_EA_4update($data, $errorMsgAssoAry, $Item){
	global $Lang;
	$x = get_error_table_header($Item, true);	// include recordID
	
	foreach ((array)$errorMsgAssoAry as $_rowNum => $_errorAry) {
		$aryIndex = $_rowNum - 2;
		$_row = $data[$aryIndex];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_studentName = trim($_row[3]);
		$_activityNameEng = trim($_row[4]);
		$_activityNameChi = trim($_row[5]);
		$_performance = trim($_row[6]);
		$_recordID = trim($_row[9]);
		
		$_errorDisplayAry = array();
		
		if (in_array('RecordNotFound', $_errorAry)) {
			$_recordID = '<span class="tabletextrequire">'.($_recordID ? $_recordID : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordIDNotFound'];
		}
		
		if (in_array('EmptyClubName', $_errorAry)) {
			$_activityNameEng = '<span class="tabletextrequire">***</span>';	
			$_activityNameChi = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyClubName'];
		}

		if (in_array('EmptyPerformance', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyPerformance'];
		}

		if (in_array('InvalidPerformanceCode', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">'.$_performance.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['InvalidPerformanceCode'];
		}

		if (in_array('RecordExist', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordExist'];
		}
		
		$_errorDisplay = '- '.implode('<br />- ', $_errorDisplayAry);
		
		$x .= '<tr>'."\n";
			$x .= '<td>'.$_rowNum.'</td>'."\n";
			$x .= '<td>'.$_recordID.'</td>'."\n";
			$x .= '<td>'.$_className.'</td>'."\n";
			$x .= '<td>'.$_classNumber.'</td>'."\n";
			$x .= '<td>'.$_webSAMS.'</td>'."\n";
			$x .= '<td>'.$_studentName.'</td>'."\n";
			$x .= '<td>'.$_activityNameEng.'</td>'."\n";
			$x .= '<td>'.$_activityNameChi.'</td>'."\n";
			$x .= '<td>'.$_performance.'</td>'."\n";
			$x .= '<td>'.$_errorDisplay.'</td>'."\n";
		$x .= '</tr>'."\n";
	}	// foreach
	$x .= '</tbody>'."\n";
	$x .= '</table>'."\n";
	return $x;	
}

function get_error_table_BTP_IC_4update($data, $errorMsgAssoAry, $Item){
	global $Lang;
	$x = get_error_table_header($Item, true);	// include recordID
	
	foreach ((array)$errorMsgAssoAry as $_rowNum => $_errorAry) {
		$aryIndex = $_rowNum - 2;
		$_row = $data[$aryIndex];
		$_className = trim($_row[0]);
		$_classNumber = trim($_row[1]);
		$_webSAMS = trim($_row[2]);
		$_studentName = trim($_row[3]);
		$_activityDate = trim($_row[4]);
		if (is_numeric($_activityDate)) {
			$_activityDate = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($_activityDate));
		}
		$_activityNameEng = trim($_row[5]);
		$_activityNameChi = trim($_row[6]);
		$_organizationNameEng = trim($_row[7]);
		$_organizationNameChi = trim($_row[8]);
		$_performance = trim($_row[9]);
		$_awardNameEng = trim($_row[10]);
		$_awardNameChi = trim($_row[11]);
		$_recommendMerit = trim($_row[12]);
		if (!empty($_recommendMerit)) {
			$_recommendMerit = substr($_recommendMerit,2);
		}
		$_recordID = trim($_row[15]);
		
		$_errorDisplayAry = array();
		
		if (in_array('RecordNotFound', $_errorAry)) {
			$_recordID = '<span class="tabletextrequire">'.($_recordID ? $_recordID : '***').'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordIDNotFound'];
		}
		
		if (in_array('EmptyActivity', $_errorAry)) {
			$_activityNameEng = '<span class="tabletextrequire">***</span>';	
			$_activityNameChi = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyActivity'];
		}

		if (in_array('EmptyPerformance', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">***</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['EmptyPerformance'];
		}

		if (in_array('InvalidPerformanceCode', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">'.$_performance.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['InvalidPerformanceCode'];
		}

		if (in_array('PerformanceConflictAward', $_errorAry)) {
			$_performance = '<span class="tabletextrequire">'.$_performance.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['PerformanceConflictAward'];
		}

		if (in_array('RecordExist', $_errorAry)) {
			$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
			$_errorDisplayAry[] = $Lang['iPortfolio']['JinYing']['ImportError']['RecordExist'];
		}
		
		$_errorDisplay = '- '.implode('<br />- ', $_errorDisplayAry);
		
		$x .= '<tr>'."\n";
			$x .= '<td>'.$_rowNum.'</td>'."\n";
			$x .= '<td>'.$_recordID.'</td>'."\n";
			$x .= '<td>'.$_className.'</td>'."\n";
			$x .= '<td>'.$_classNumber.'</td>'."\n";
			$x .= '<td>'.$_webSAMS.'</td>'."\n";
			$x .= '<td>'.$_studentName.'</td>'."\n";
			$x .= '<td>'.$_activityDate.'</td>'."\n";
			$x .= '<td>'.$_activityNameEng.'</td>'."\n";
			$x .= '<td>'.$_activityNameChi.'</td>'."\n";
			$x .= '<td>'.$_organizationNameEng.'</td>'."\n";
			$x .= '<td>'.$_organizationNameChi.'</td>'."\n";
			$x .= '<td>'.$_performance.'</td>'."\n";
			$x .= '<td>'.$_awardNameEng.'</td>'."\n";
			$x .= '<td>'.$_awardNameChi.'</td>'."\n";
			$x .= '<td>'.$_recommendMerit.'</td>'."\n";
			$x .= '<td>'.$_errorDisplay.'</td>'."\n";
		$x .= '</tr>'."\n";
	}	// foreach
	$x .= '</tbody>'."\n";
	$x .= '</table>'."\n";
	return $x;	
}

?>