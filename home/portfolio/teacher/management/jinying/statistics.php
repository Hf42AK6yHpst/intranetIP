<?php

// Modified by
/*
 * 2018-05-03 [Cameron]
 * - move export button to result page
 * - add image loading process when submit for different criteria
 * - reload page when export
 *  
 * 2018-04-17 [Cameron]
 * - add statistics analysis tab
 *
 * 2017-10-25 [Cameron]
 * - add label to enclose radio button for easy select
 *
 * 2017-02-15 [Cameron]
 * - add export button
 *
 * 2017-01-09 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/iportfolio_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libportfolio.php");
include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/customize/tmchkwc/libpf-jinying.php");

iportfolio_auth("T"); // teacher
intranet_opendb();

if (! $sys_custom['iPf']['JinYingScheme']) {
    header("Location: /home/portfolio/");
    exit();
}

$lpf = new libportfolio();
$lfcm = new form_class_manage();

if (! $lpf->IS_IPF_SUPERADMIN() && ! $_SESSION['SSV_USER_ACCESS']['other-iPortfolio']) { // only allow iPortfolio admin to access
    header("Location: /home/portfolio/");
    exit();
}

$AcademicYearID = $_POST['AcademicYearID'] ? $_POST['AcademicYearID'] : Get_Current_Academic_Year_ID();

// Acadermic Year Selection
$yearFilter = getSelectAcademicYear("AcademicYearID", '', 1, 0, $AcademicYearID);

// Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_JinYing_Scheme";

// ## Title ###
$ljy = new libJinYing();
$TAGS_OBJ = $ljy->getJinYingTab('Statistics');
$statisticsAnalysisTab = $ljy->getStatisticsAnalysisTabMenu('ByReceive');

$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

?>
<style>
.green {
	color: #00b33c;
}
</style>

<script type="text/JavaScript" language="JavaScript">
    var isLoading = true;
    var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';

	function hideOptionLayer()
	{
		$('#form1').attr('style', 'display: none');
		$('.spanHideOption').attr('style', 'display: none');
		$('.spanShowOption').attr('style', '');
	}
	
	function showOptionLayer()
	{
		$('#form1').attr('style', '');
		$('.spanShowOption').attr('style', 'display: none');
		$('.spanHideOption').attr('style', '');
	}

	$(document).ready(function(){

		$('#submit_result').click(function(){
	        isLoading = true;
			$('#form_result').html(loadingImg);
			
			$.ajax({
				dataType: "json",
				type: "POST",
				url: 'ajax_analysis_result.php',
				data : $("#form1").serialize(),		  
				success: load_analysis_result_layout,
				error: show_ajax_error
			});
		});
				
	});

	function load_analysis_result_layout(ajaxReturn) {
		if (ajaxReturn != null && ajaxReturn.success){
//			$('#form1').attr('style','display: none');
			$('#form_result').html(ajaxReturn.html);
//  			$('#spanShowOption').show();
//  			$('#spanHideOption').hide();
 			showOptionLayer();
			$('#report_show_option').addClass("report_option report_hide_option");
			isLoading = false;
//			window.scrollTo(0, 0);
		}
	}	
	
	function show_ajax_error() {
		alert('<?=$Lang['iPortfolio']['JinYing']['ajaxError']?>');
	}
	
	function js_export() {
		var original_action = document.form1.action;
		var url = "export_statistics.php";
		document.form1.action = url;
		document.form1.submit();
		document.form1.action = original_action;
		$('#submit_result').click();	
	}
	
</script>

<div>
  <?php echo $statisticsAnalysisTab;?>
</div>

<div id="report_show_option">
	<span id="spanShowOption" class="spanShowOption" style="display: none">
		<a href="javascript:showOptionLayer();"><?=$Lang['iPortfolio']['JinYing']['Medal']['ShowOption']?></a>
	</span> <span id="spanHideOption" class="spanHideOption"
		style="display: none"> <a href="javascript:hideOptionLayer();"><?=$Lang['iPortfolio']['JinYing']['Medal']['HideOption']?></a>
	</span>

	<form name="form1" method="post" id="form1">

		<table class="form_table_v30">
			<tr>
				<td class="field_title"><?=$Lang['General']['SchoolYear']?></td>
				<td><?=$yearFilter?></td>
			</tr>

			<tr>
				<td class="field_title"><?=$Lang['iPortfolio']['JinYing']['StatisticsByMethod']?></td>
				<td><label><input type="radio" name="ByMethod" id="ByForm"
						value="Form" checked><?=$Lang['iPortfolio']['JinYing']['Statistics']['ByForm']?></label>&nbsp;
					<label><input type="radio" name="ByMethod" id="ByClass"
						value="Class"><?=$Lang['iPortfolio']['JinYing']['Statistics']['ByClass']?></label></td>
			</tr>

		</table>

		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "", "submit_result")?>&nbsp;
		</div>
	</form>
</div>
<div id="form_result"></div>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>