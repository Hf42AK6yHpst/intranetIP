<?php

// Modified by 
/*
 *  2019-12-03 [Caemron]
 *      - modify logic related to $conf['JinYingItemCodeWithTerm'] as the codes cover scope [case #J161484]
 *
 * 	2016-11-28 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/config.php");

iportfolio_auth("T");	// teacher
intranet_opendb();

if (!$sys_custom['iPf']['JinYingScheme']) {
	header("Location: /home/portfolio/");
	exit;
}

$lpf = new libportfolio();
if(!$lpf->IS_IPF_SUPERADMIN() && !$_SESSION['SSV_USER_ACCESS']['other-iPortfolio']){	// only allow iPortfolio admin to access
	header("Location: /home/portfolio/");
	exit;
}

if(sizeof($_POST)==0) {
	header("Location: index.php");	
}

$RecordInfo = $_POST['RecordInfo'];
$result = array();
if (count($RecordInfo)) {
	$lpf->Start_Trans();
	foreach((array)$RecordInfo as $rinfo) {
		list($yearClassID,$academicYearID,$yearTermID,$code) = explode("^~",$rinfo);
		if ($yearClassID && $academicYearID && $code) {
			if (in_array($code,$conf['JinYingItemCodeWithTerm'])) {
				$term_cond = "AND YearTermID='".$yearTermID."'";
			}
			else {
				$term_cond = "";
			}
			$sql = "DELETE FROM ".$eclass_db.".PORTFOLIO_JINYING_SCHEME WHERE 
						AcademicYearID='".$academicYearID."'
					".$term_cond."
					AND PerformanceCode LIKE '".$code."%'
					AND UserID IN (SELECT UserID FROM YEAR_CLASS_USER
						WHERE YearClassID='".$yearClassID."')";

			$result[] = $lpf->db_db_query($sql);		
		}
	}
	if (!in_array(false,$result)) {
		$lpf->Commit_Trans();
		$xmsg = 'DeleteSuccess';
	}
	else {
		$lpf->RollBack_Trans();
		$xmsg = 'DeleteUnsuccess';
	}
	
}
else {
	$xmsg = 'DeleteUnsuccess';
}

intranet_closedb();

header("Location: delete_jinying.php?returnMsgKey=".$xmsg."&AcademicYearID=".$AcademicYearID."&Item=".$Item."&Semester=".$Semester);

?>