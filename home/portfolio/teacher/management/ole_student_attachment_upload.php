<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
// include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-teacher.php");
// include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
// include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
 include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-mgmt-group.php");
// include_once($PATH_WRT_ROOT."includes/libexporttext.php");
intranet_auth();
intranet_opendb();

$action = $_SESSION["SESS_OLE_ACTION"];
$isReadOnly = ($action == $ipf_cfg["OLE_ACTION"]["view"]) ? 1 : 0 ;

$ldb = new libdb();
$lpf = new libpf_slp();

$lpf_mgmt_grp = new libpf_mgmt_group();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

########################################################
# HIDE COL FOR SIS 20140319 TEMPLATE SETTINGS 
########################################################

// set current tab name for sys_custom 
/* Please do not change the Value 
 * Unless you have understood how hide column works as well as INT/EXT means */

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = ($isReadOnly) ? "Teacher_OLEView" : "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

#step
// $STEPS_OBJ[] = array($Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][0], 1);
// $STEPS_OBJ[] = array($Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][1], 0);
// $STEPS_OBJ[] = array($Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][2], 0);
$stepAry[] = $Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][0];
$stepAry[] = $Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][1];
$stepAry[] = $Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][2];
$StepObj = $linterface->GET_IMPORT_STEPS($CurrStep=1, $stepAry);
// $StepObj = $linterface->GET_STEPS($STEPS_OBJ);


$TabIndex = empty($IntExt) ? IPF_CFG_SLP_MGMT_OLE : IPF_CFG_SLP_MGMT_OUTSIDE;

$TabMenuArr = libpf_tabmenu::getSlpMgmtTabs($TabIndex, 0);
### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags($TabIndex);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$data = $lpf->RETURN_OLE_PROGRAM_BY_PROGRAMID_MAX($EventID);
if (is_array($data)) {
	$eng_title_html = $data['Title'];
}

#nagigation
$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['program_list'], "ole.php?IntExt=".$IntExt);
$MenuArr[] = array($eng_title_html, "ole_event_studentview.php?IntExt=".$IntExt."&EventID=".$EventID."&Year=".$Year."&FromPage=program");
$MenuArr[] = array($Lang['iPortfolio']['OLE']['UploadButton'], "");
$navigation_html = $linterface->GET_NAVIGATION($MenuArr);





# Remarks list
$RemarkAry = array();
for($i=1; $i<=sizeof($Lang['iPortfolio']['OLE']['UploadAttachment']['RemarksArr']);$i++){
	$RemarkAry[] = $Lang['iPortfolio']['OLE']['UploadAttachment']['RemarksArr'][$i-1];
}
$htmlAry['RemarkBox'] = $linterface->Get_Warning_Message_Box($Lang['iPortfolio']['OLE']['UploadAttachment']['Remarks'],$RemarkAry);
	
$submitBtn = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn");
$backBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.history.back();", "backBtn");
	

// 	include_once("template/ole_event_studentview.tmpl.php");

$linterface->LAYOUT_START();
?>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><?=$navigation_html?></td>
							</tr>
						</table>
					</td>
					<td align="right"></td>
				</tr>
				</table>
				<br>
			<form name="form1" method="post" action="ole_student_attachment_upload_confirm.php?EventID=<?=$EventID?>" onsubmit="return checkform()"  enctype="multipart/form-data">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" >
				<tr>
					<td>
						<?=$StepObj?>
					</td>
				</tr>
				<tr>
				
				</tr>
			</table>
			<br>
			<div style="width:90%;margin:0 auto;"> 
				<?=$htmlAry['RemarkBox']?>
				<table width="100%" border="0" cellspacing="0" cellpadding="2" class="form_table_v30">
					<tr>
						<td class="formfieldtitle field_title" width="30%"><?=$Lang['iPortfolio']['OLE']['UploadAttachment']['File']?></td>
						<td class="tabletext"><input id="UploadFile" type="file" name="UploadFile"></td>
					</tr>
					<tr>
						<td class="formfieldtitle field_title" width="30%"><?=$Lang['iPortfolio']['OLE']['UploadAttachment']['FileSample']?></td>
						<td class="tabletext">
							<a href='SingleFileSample.zip' class='tablelink'><?=$Lang['iPortfolio']['OLE']['UploadAttachment']['DownloadSingleFileSample']?></a>
							<br>
							<a href='MultipleFileSample.zip' class='tablelink'><?=$Lang['iPortfolio']['OLE']['UploadAttachment']['DownloadMultipleFileSample']?></a>
						
						</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline">
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<?=$submitBtn?>
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="1" height="1">
			<?= $backBtn ?>
			<br><br>
		</td>
	</tr>
</table>
</form>

<script>
function checkform()
{
	var filename = $("#UploadFile").val();
	var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase();
	if(fileext!=".zip")
	{	
		alert("<?=$Lang['iPortfolio']['OLE']['UploadAttachment']['WarnMsg']['PleaseSelectZIP']?>");
		return false;
	}
	return true;
}
</script>

<?	
$linterface->LAYOUT_STOP();
intranet_closedb();
?>