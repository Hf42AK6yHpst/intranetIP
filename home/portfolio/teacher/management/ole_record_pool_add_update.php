<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_auth();
intranet_opendb();

$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

$OLERecArr = $LibPortfolio->GET_OLE_RECORD_ASSIGNED($StudentID, $IntExt);
$OLERecArrNew = array_merge($OLERecArr, $record_id);

$LibPortfolio->SET_OLE_RECORD_ORDER_SLP($StudentID, $OLERecArrNew, $IntExt);

intranet_closedb();

?>

<script language="JavaScript">
	window.opener.location.href = "ole_record_pool.php?msg=add&IntExt=<?=$IntExt?>&StudentID=<?=$StudentID?>";
	window.close();
</script>