<?php
# *remark 1 - please do not use variable name $role directly, since this will collide with the session variable
/** [Modification Log] Modifying By: 
 * *******************************************
 * 2015-01-21 Bill
 * for St. Paul PAS cust
 * - not allow modify OLE program content even $programIsEditable = true
 * 
 * 2012-06-27 Bill ( 2012-0626-1533-29073 )
 * -variable role from previous page has been used by other page.
 * -role changed to role2, the $var_role of this page has been updated.
 * 
 * 2010-11-22 Max (201011191622)
 * - Change the approved_by to requestApprovedBy
 * 
 * 2010-06-30 Max (201006301329)
 * - get role from previous page specified to get from $_POST: refer to remark 1 above,
 * 	 and changed variable name $role to $var_role
 * *******************************************
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");


intranet_auth();
intranet_opendb();

$LibPortfolio = new libportfolio();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$LibPortfolio->ACCESS_CONTROL("ole");

//form passing variable
$programIsEditable  = trim($programIsEditable);

if($SubmitType == "")
{
	if($IntExt == 1)
	$SubmitType = "EXT";
	else
	$SubmitType = "INT";
}

$is_attachment = false;
for ($i=1; $i<=$attachment_size; $i++)
{
	if (trim(${"ole_file".$i})!="")
	{
		$is_attachment = true;
	}
}
$libpf_slp = new libpf_slp();
$approvalSettings = $libpf_slp->GET_OLR_APPROVAL_SETTING_DATA2($SubmitType);
//debug_r($approvalSettings);

$ClassTeacherUserID = $LibPortfolio->GET_CLASS_TEACHER($ClassName);
$ClassTeacherUserID = $ClassTeacherUserID[0];

//$ApprovalPerson = ($ApprovalSetting==1) ? $approved_by : $ClassTeacherUserID;

//admin can change the approved by no matter the setting is what , comment out this code
//$requestApprovedBy = ($approvalSettings['IsApprovalNeed'] && $approvalSettings['Elements']['Self']) ? $request_approved_by : "-1" ;

$requestApprovedBy = ($request_approved_by == "") ? -1 : $request_approved_by;

//debug_r($requestApprovedBy);

$title = HTMLtoDB($title);
$category = HTMLtoDB($category);
$var_role = HTMLtoDB($_POST["role2"]);
$achievement = HTMLtoDB($achievement);
$SchoolRemarks = HTMLtoDB($SchoolRemarks);
$organization = HTMLtoDB($organization);
$details = HTMLtoDB($details);

$ELEList = (sizeof($ele)!=0) ? implode(",", $ele) : "";
$enddate = ($startdate=="" || $startdate=="0000-00-00") ? "" : $enddate;
if($RecordID=="")
{
	$fields = "(UserID, Title, Category, ELE, Role, Hours, Achievement, Organization";
	$fields .= ", RequestApprovedBy";
	$fields .= ", Details, Remark";
	$fields .= ", RecordStatus, StartDate, EndDate, InputDate, ModifiedDate, TeacherComment, IntExt)";
	$values = "('".$StudentID."', '".$title."', '".$category."', '".$ELEList."', '".$var_role."', '".$hours."', '".$achievement."', '".$organization."'";
	$values .= ", '".$requestApprovedBy."'";
	$values .= ", '".$details."', '".$SchoolRemarks."'";
//	$RecorStatus = ($ApprovalSetting==2) ? 2 : 1;
	$RecorStatus = (!$approvalSettings['IsApprovalNeed']) ? 2 : 1;
	$values .= ", '{$RecorStatus}', '".$startdate."', '".$enddate."', now(), now(), '".$TeacherComment."' , '".$SubmitType."')";

	$sql = "INSERT INTO {$eclass_db}.OLE_STUDENT $fields VALUES $values";

	$LibPortfolio->db_db_query($sql);
	$RecordID = $LibPortfolio->db_insert_id();
	$msg = 1;
} 
else
{
  $sql = "SELECT ProgramID FROM {$eclass_db}.OLE_STUDENT WHERE RecordID = '$RecordID'";
  $ProgramID = $LibPortfolio->returnVector($sql);
  
	$fields_values = "ProgramType = 'T', ";
	$fields_values = "Title = '$title', ";
	$fields_values .= "Category = '$category', ";
	$fields_values .= "ELE = '$ELEList', ";
	$fields_values .= "Organization = '$organization', ";
	$fields_values .= "Details = '$details', ";
	$fields_values .= "SchoolRemarks = '$SchoolRemarks', ";
	$fields_values .= "StartDate = '$startdate', ";
	$fields_values .= "EndDate = '$enddate', ";
	$fields_values .= "ModifiedDate = now(),";
	$fields_values .= "IntExt = '$SubmitType' ";
	
	// Added: 2015-01-21 - for St. Paul PAS cust - not update OLE program
	if($programIsEditable && !$sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme']){
		$sql = "UPDATE {$eclass_db}.OLE_PROGRAM SET $fields_values WHERE ProgramID = '".$ProgramID[0]."'  ";
	}
	$LibPortfolio->db_db_query($sql);

	$fields_values = "Title = '$title', ";
	$fields_values .= "Category = '$category', ";
	$fields_values .= "ELE = '$ELEList', ";
	$fields_values .= "Role = '$var_role', ";
	$fields_values .= "Hours = '$hours', ";
	$fields_values .= "Achievement = '$achievement', ";
	$fields_values .= "Organization = '$organization', ";
	$fields_values .= "RequestApprovedBy = '$requestApprovedBy', ";
	$fields_values .= "Details = '$details', ";
	$fields_values .= "StartDate = '$startdate', ";
	$fields_values .= "EndDate = '$enddate', ";
	$fields_values .= "IntExt = '$SubmitType', ";
	$fields_values .= "ModifiedDate = now()";
//	$sql = "UPDATE {$eclass_db}.OLE_STUDENT SET $fields_values WHERE RecordID = '$RecordID' AND UserID='$StudentID' AND RecordStatus=1 ";
	$sql = "UPDATE {$eclass_db}.OLE_STUDENT SET $fields_values WHERE RecordID = '$RecordID' AND UserID='$StudentID' ";
	
	$LibPortfolio->db_db_query($sql);
	$msg = 2;
}

$LibFS = new libfilesystem();

$fileArr = "";

$attachments = "";
// use previous session_id if exists
if ($attachment_size_current>0)
{
	$tmp_arr = explode("/", ${"file_current_0"});
	$SessionID = trim($tmp_arr[0]);
}
if ($SessionID=="")
{
	$SessionID = session_id();
}
$folder_prefix = $eclass_filepath."/files/portfolio/ole/r".$RecordID."/".$SessionID;

# remove unwanted files
for ($i=0; $i<$attachment_size_current; $i++)
{
	$attach_file = ${"file_current_".$i};
	if (${"is_need_".$i})
	{
		$attachments .= (($attachments=="")?"":":") . $attach_file;
		$fileArr[] = $attach_file;
	} else
	{
		// remove the corresponding file
		$LibFS->file_remove($folder_prefix."/".stripslashes(basename($attach_file)));
		
		
	}
}

# add new files
if ($is_attachment)
{
	
	$LibFS->folder_new($eclass_filepath."/files/portfolio/ole/r".$RecordID);
	$LibFS->folder_new($folder_prefix);
	
	# copy the files
	for ($i=1; $i<=$attachment_size; $i++)
	{
		$loc = ${"ole_file".($i)};
		$name_hidden = ${"ole_file".$i."_hidden"};
		$filename = (trim($name_hidden)!="") ? $name_hidden : ${"ole_file".($i)."_name"};

		if ($loc!="none" && file_exists($loc))
		{
			if(strpos($filename, ".")==0)
			{
				// Fail
				$isOk = false;
			}
			else
			{
				// Success
				$LibFS->item_copy($loc, stripslashes($folder_prefix."/".$filename));

				$tmp_attach = $SessionID."/".$filename;
				$exist_flag=0;
				for($j=0; $j<sizeof($fileArr); $j++)
				{
					if($tmp_attach==$fileArr[$j])
					{
						$exist_flag = 1;
						break;
					}
				}
				if($exist_flag!=1)
				{
					$attachments .= (($attachments=="")?"":":") . $tmp_attach;
					$fileArr[] = $tmp_attach;
				}
			}
		}
	}
}

$award_file_sql = (trim($attachments)=="") ? "NULL" : "'$attachments'";
$sql = "	UPDATE
				{$eclass_db}.OLE_STUDENT
			SET
				Attachment=$award_file_sql
			WHERE
				RecordID = '$RecordID'
		";
$LibPortfolio->db_db_query($sql);


intranet_closedb();
//exit();
if ($FromPage == "program")
	header("Location: ole_event_studentview.php?msg=$msg&EventID=$EventID&Year=$Year&FromPage=$FromPage&IntExt=$IntExt");
else	
	header("Location: ole_student.php?msg=$msg&StudentID=$StudentID&Year=$Year&FromPage=$FromPage&IntExt=$IntExt");




?>
