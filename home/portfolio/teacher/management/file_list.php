<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_auth();
intranet_opendb();

$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

$attachments_html = "";

$validRecord = $LibPortfolio->CHECK_OLE_RECORDID_EXIST($RecordID);

if($validRecord!=1)
{
	$attachments_html = "<br>".$ec_iPortfolio['no_record_found'];
}
else
{
	$valid = 1;
	

	if($valid==1)
	{
		$data = $LibPortfolio->RETURN_OLE_RECORD_BY_RECORDID($RecordID);
		$ole_file = $data["Attachment"];

// 		$tmp_arr = explode("\:", $ole_file);
		$tmp_arr = explode(":", $ole_file);

		$folder_prefix0 = $eclass_filepath."/files/portfolio/ole/r".$RecordID;
		$http = checkHttpsWebProtocol()? "https" : "http";
		$folder_url = $http . "://".$eclass_httppath."/files/portfolio/ole/r".$RecordID;
		$firtFile = "";

		$attachments_html .= "<SELECT name=file onChange='changeFile(this.value)'>";
		for ($i=0; $i<sizeof($tmp_arr); $i++)
		{
			$bcolor = ($attach_count%2!=0) ? "bgcolor='#EEEEEE'" : "bgcolor='#FFFFFF'";
			$attach_file = $tmp_arr[$i];
			if (file_exists($folder_prefix0."/".$attach_file))
			{
				# Eric Yip (20090522): Handle chinese-character file name
				$attach_file_url = urlencode($attach_file);
				$attach_file_url = str_replace(array("%2F", "%26", "+"), array("/", "&", " "), $attach_file_url);

				if($firstFile=="")
					//$firstFile = $folder_url."/".$attach_file;
					$firstFile = $folder_url."/".$attach_file_url;

				$file_size = ceil(filesize($folder_prefix0."/".$attach_file)/1024) . " " . $file_kb;
				//$attachments_html .= "<OPTION value=\"".$folder_url."/".$attach_file."\">&nbsp; ".basename($attach_file)." &nbsp;($file_size) </OPTION>";
				$attachments_html .= "<OPTION value=\"".$folder_url."/".$attach_file_url."\">&nbsp; ".get_file_basename($attach_file)." &nbsp;($file_size) </OPTION>";
			}
		}
		$attachments_html .= "</SELECT>";
	}
	else
	{
		$attachments_html .= "<br />".$ec_iPortfolio['no_privilege_to_read_file'];
	}
}
?>
<html>
<head>
<?=returnHtmlMETA()?>
<script language="JavaScript" type="text/JavaScript">
function changeFile(file){

		parent.FileListBottomFrame.location = file ;

		return;
}
</script>
</head>

<body bgcolor="#CFE6FE" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<link href="http://<?=$eclass_httppath?>/src/includes/css/style_portfolio_body.css" rel="stylesheet" type="text/css">
<FORM name="form1" method="GET">
<table width="100%" border="0" height="100%">
<tr>
	<td align="center"><?= $iPort['attachments'] ." : &nbsp;" .$attachments_html?></td>
</tr>
</table>
<input type="hidden" name="flag" value="1" >
</FORM>

</body>
</html>

<script language="JavaScript">changeFile('<?=$firstFile?>');</script>

<?php
intranet_closedb();
?>