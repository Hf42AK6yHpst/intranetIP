<?php 
/*
 * Change Log:
 * Date	2017-11-03 Omas specify the database avoid sql error
 * Date 2017-07-04 Villa Support Search the className either in Chinese or English 
 * Date 2016-12-22 Villa Open the file
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
$libdb = new libdb();

intranet_auth();
intranet_opendb();
$ayid = Get_Current_Academic_Year_ID();
switch ($ajaxFieldName){
	case 'ClassName':
		$temp = explode(' ',$q);
		$filter = $libdb->Get_Safe_Sql_Like_Query($temp[0]);
		$cond = "( CONCAT(yc.ClassTitleB5,ycu.ClassNumber) LIKE '%$filter%' OR CONCAT(lower(yc.ClassTitleEN),ycu.ClassNumber) LIKE '%$filter%')";
		if(isset($temp['1'])){
			$filter = $libdb->Get_Safe_Sql_Like_Query($temp[1]);
			$cond .= "AND (iu.EnglishName LIKE '%$filter%' OR  iu.ChineseName LIKE '%$filter%') ";
		}
		break;
	case 'UserLogin':
		$filter = $libdb->Get_Safe_Sql_Like_Query($q);
		$cond = " iu.UserLogin LIKE '%$filter%' ";
		break;
}
$sql= "
		SELECT 
			yc.ClassTitleEN, yc.ClassTitleB5, ycu.ClassNumber, iu.EnglishName, iu.ChineseName, iu.UserID
		FROM
			{$intranet_db}.INTRANET_USER iu
		INNER JOIN
			{$intranet_db}.YEAR_CLASS_USER ycu ON iu.UserID = ycu.UserID
		INNER JOIN
			{$intranet_db}.YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID and yc.AcademicYearID = '$ayid'
		WHERE
			$cond
		ORDER BY	
			yc.ClassTitleEN, ycu.ClassNumber
		LIMIT 30
			";

$rs = $libdb->returnResultSet($sql);

foreach($rs as $_rs){
	$data .= Get_Lang_Selection($_rs['ChineseName'], $_rs['EnglishName'])."(".Get_Lang_Selection($_rs['ClassTitleB5'],$_rs['ClassTitleEN'])."-".$_rs['ClassNumber'].")".'|'.'|'.'U'.$_rs['UserID']."\n";
}
echo $data;
intranet_closedb();
?>