<SCRIPT LANGUAGE="Javascript">
<? if($sys_custom['iPf']['displayIneRC'] && ($IntExt==0 || $IntExt=='')  ){ ?>
function displayeRC(obj,element,page,status){
  if(countChecked(obj,element)==0)
    alert(globalAlertMsg2);
  else{
      obj.action=page;
      obj.displayIneRC.value=status;
      obj.submit();
  }
}
<?}?>

function checkApprove(obj,element,page,status){
  if(countChecked(obj,element)==0)
    alert(globalAlertMsg2);
  else{
    if(confirm(globalAlertMsg4)){
      obj.action=page;
      obj.approve.value=status;
      obj.submit();
    }
  }
}

function checkRejectNoDel(obj,element,page,status){
  if(countChecked(obj,element)==0)
    alert(globalAlertMsg2);
  else{
    if(confirm(globalAlertMsgRejectNoDel)){
      obj.action=page;
      obj.approve.value=status;
      obj.submit();
    }
  }
}

function goExport() {
	document.form1.IsExport.value = 1;
	document.form1.submit();
	document.form1.IsExport.value = 0;	
}
</SCRIPT>

<FORM name="form1" method="GET">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td align="center">
      <?=$tab_menu_html?>
  	</td>
  </tr>
  <tr>
    <td class="navigation"><?=$navigation_html ?></td>
  </tr>
  <tr>
    <td>
      <table align="center" width="96%" border="0" cellpadding="5" cellspacing="0" style="border:dashed 1px #666666">
        <tr>
          <td width="80%" valign="top">
            <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
              <div class="table_row_tool row_content_tool">
  				<?=$EditButton?>
  			  </div> 
    <?php if (!$IPF_HIDE_COLUMN_TMPL['SUBMISSION_TYPE']) { ?>
              <tr>
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $iPort['submission_type']; ?></span></td>
                <td width="80%" valign="top"><?=$submit_type_html?></td>
              </tr>
          <?php } ?>
              
              <tr>
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['title']; ?></span></td>
                <td width="80%" valign="top"><font size="3"><b><?=$eng_title_html?></b></font></td>
              </tr>
              <tr>
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
                <td><?=$period_html?></td>
              </tr>
              
          <?php if (!$IPF_HIDE_COLUMN_TMPL['DATE_PERIOD']) { ?>
              <tr>
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['date']?> / <?=$ec_iPortfolio['year_period']?></span></td>
                <td><span class="tabletext"><?=$date_html?></span></td>
              </tr>
      		<?php } ?>
              
              <!-- category -->
              <tr valign="top">
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $ec_iPortfolio['category']; ?></span></td>
                <td><?=$category_html?></td>
              </tr>
      			
	    <?php if (!$IPF_HIDE_COLUMN_TMPL['SUB_CATEGORY']) { ?>	
              <!-- sub category -->
              <tr valign="top">
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $ec_iPortfolio['sub_category']; ?></span></td>
                <td><?=$sub_category_html?></td>
              </tr>
      <?php } ?>
      
              <!-- component of ole -->
              <? if ($h_ShowOleComponent) { ?>
	              <tr valign="top">
	                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $ec_iPortfolio['ele']; ?></span></td>
	                <td><?=$component_html?></td>
	              </tr>
	          <? } ?>
      
	<?php if(!$StPaulField){ ?>
              <!-- allow students to join -->
			  <?if($h_ShowAllowStudentJoin){?>
				  <tr valign="top">
					<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['SLP']['AllowStudentsToJoin'] ?></span></td>
					<td><?=$canJoin_html ?></td>
				  </tr>
			  <?}?>
              <tr valign="top" style="display: <?= $joinPeriodDisplayStatus ?>">
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['SLP']['JoinPeriod'] ?></span></td>
                <td><?=$canJoinPeriod_html?> </td>
              </tr>
      
              <!-- auto approve -->
              <tr valign="top" style="display: <?= $joinPeriodDisplayStatus ?>">
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['SLP']['AutoApprove'] ?></span></td>
                <td><?= $autoApprove_html ?> </td>
              </tr>
              
              <!-- maximum hours -->
              <tr valign="top" style="display: <?= $joinPeriodDisplayStatus ?>">
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['SLP']['MaximumHours'] ?></span></td>
                <td><?= $maximumHours_html ?> </td>
              </tr>
              
              <!-- default hours -->
              <tr valign="top" style="display: <?= $joinPeriodDisplayStatus ?>">
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['SLP']['DefaultHours'] ?></span></td>
                <td><?= $defaultHours_html ?> </td>
              </tr>
              
              <!-- compulsory fields -->
              <tr valign="top" style="display: <?= $joinPeriodDisplayStatus ?>">
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['SLP']['CompulsoryFields'] ?></span></td>
                <td><?= $compulsoryFields_html ?> </td>
              </tr>
              
               <!-- default Approver -->
              <tr valign="top" style="display: <?= $joinPeriodDisplayStatus ?>">
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['SLP']['DefaultApprover'] ?></span></td>
                <td><?= $defaultApprover_html?> </td>
              </tr>
              
      <?php if (!$IPF_HIDE_COLUMN_TMPL['ORGANIZATION']) { ?>
              <!-- organization -->
              <tr>
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['organization']?></span></td>
                <td><?=$organization_html?></td>
              </tr>
       <?php } ?>
              
              <!-- details -->
              <tr valign="top">
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['details']?></span></td>
                <td><?=$engDetails_html?></td>
              </tr>
              
              <tr valign="top">
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['school_remark']?></span></td>
                <td><?=$remark_html?></td>
              </tr>
    <?php } ?>
    
<?php if($sys_custom['iPortfolio_ole_record_tic']){ ?>
            <!-- Teacher-in-charge -->
            <tr valign="top">
	            <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['iPortfolio']['OLE']['TeacherInCharge']?></span></td>
	            <td><?=$teacher_in_charge?></td>
          	</tr>
<?php } ?>

			<!-- HKUGAC Customization -->
<?php if (isset($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) && $sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) { ?>
	
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['iPortfolio']['OLE']['SAS_with_link']?></span></td>
				<td><?=$IsSASText?></td>
			</tr>
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['iPortfolio']['OLE']['In_Outside_School']?></span></td>
				<td><?=$IsOutsideSchoolText?></td>
			</tr>
<?php } ?>
		 <?php if (!$IPF_HIDE_COLUMN_TMPL['CREATEDBY']) { ?>
              <!-- created by -->
              <tr valign="top">
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['SLP']['CreatedBy']?></span></td>
                <td><?= $creator_name_html ?></td>
              </tr>
          <?php } ?>
              
          <?php if (!$IPF_HIDE_COLUMN_TMPL['LASTUPDATED']) { ?>    
              <!-- last modified date -->
              <tr valign="top">
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$iPort["last_updated"]?></span></td>
                <td><?= $modified_date_html ?><?=$ModifiedUser_html?></td>
              </tr>
         <?php } ?>
              
              <?=$cnecc_customize_html?>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table width="96%" align="center" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td height="30" valign="absmiddle"><?=$ole_assign_student_btn_html?></td>
                <td align="right"><?=$op_result?></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" cellspacing="0" cellpadding="0">
              <tr class="table-action-bar">
                <td>
                  <?=$class_selection_html?>
                  <?=$status_selection_html?>
                </td>
                <td align="right">
<?php if($displayEditFunctions) { ?>
                  <table border="0" cellpadding="0" cellspacing="0" id="editFunctions">
                    <tbody>
                      <tr>
                        <td>
                            <div class="common_table_tool">
                                <?php if($sys_custom['iPf']['displayIneRC'] && ($IntExt==0 || $IntExt=='') ): ?>
                                    <a href="javascript:displayeRC(document.form1,'record_id[]','display_eRC_update.php', 1)" class="tool_approve"><?php echo $Lang['iPortfolio']['OLE']['DisplayIneRC'];?></a>
                                    <a href="javascript:displayeRC(document.form1,'record_id[]','display_eRC_update.php', 0)" class="tool_reject"><?php echo $Lang['iPortfolio']['OLE']['HideIneRC'];?></a>
                                <?php endif;?>
                                <a href="javascript:checkApprove(document.form1,'record_id[]','student_ole_status_update2.php', 2)" class="tool_approve"><?php echo $button_approve;?></a>
                                <a href="javascript:checkRejectNoDel(document.form1,'record_id[]','student_ole_status_update2.php', 3)" class="tool_reject"><?php echo $button_reject;?></a>
                                <a href="javascript:checkEditMultiple(document.form1,'record_id[]','ole_student_edit2.php',0)" class="tool_edit"><?php echo $ec_iPortfolio['edit'];?></a>
                                <?php if($allow_Assign_Delete):?>
                                    <a href="javascript:checkRemove(document.form1,'record_id[]','ole_student_remove.php')" class="tool_delete"><?php echo $ec_iPortfolio['delete'];?></a>
                                <?php endif;?>
                            </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
<?php } ?>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table align="center" width="100%" cellpadding="0" cellspacing="0">
              <tr>
                <td>
                  <?=$stu_rec_html?>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<input type="hidden" name="pageNo" value="<?= $LibTable->pageNo; ?>" />
<input type="hidden" name="order" value="<?= $LibTable->order; ?>" />
<input type="hidden" name="field" value="<?= $LibTable->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$LibTable->page_size?>" />
<input type="hidden" name="EventID" value="<?= $EventID; ?>" />
<input type="hidden" name="IntExt" value="<?=$IntExt?>">
<input type="hidden" name="FromPage" value="program" />
<input type="hidden" name="IsExport" value="0" />
<input type="hidden" name="approve" />

<? if($sys_custom['iPf']['displayIneRC'] && ($IntExt==0 || $IntExt=='')  ){ ?>
<input type="hidden" name="displayIneRC" />
<? } ?>
</FORM>