<script language="JavaScript">

function swap_row(jParRowObj, jParRecordID, jParDir)
{
	if(!$('.swap_btn').attr('clickable'))
		return
	
	StartRowIndex = <?=($IntExt==0)?2:1?>;

	var ParentTable = jParRowObj.parentNode;
	var nRows = ParentTable.rows;
	var SourceRowIndex = jParRowObj.rowIndex;
	var TargetRowIndex = SourceRowIndex + jParDir;
	
	// Check outrange
	if(TargetRowIndex >= nRows.length || TargetRowIndex < StartRowIndex)
		return;

	// Disable button
	$('.swap_btn').removeAttr('clickable');
	
	// Update database
  $.ajax({
    method: "get",
    url: "../../ajax/swap_ole_row_ajax.php",
    data: "SourceRecordID="+jParRecordID+"&Direction="+jParDir+"&IntExt=<?=$IntExt?>",
    success: function(html) {
			// Swap row
			for(i=1; i<jParRowObj.cells.length; i++)
			{
				SourceRowCellHTML = jParRowObj.cells[i].innerHTML;
				ParentTable.rows[SourceRowIndex].cells[i].innerHTML = ParentTable.rows[TargetRowIndex].cells[i].innerHTML;
				ParentTable.rows[TargetRowIndex].cells[i].innerHTML = SourceRowCellHTML;
				$('.swap_btn').attr('clickable',true);
			}
    },
    error: function() {
			alert("<?=$ec_warning['update_fail']?>");
		}
  });
}

</script>

<FORM name="form1" method="GET">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
			<table width="96%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
            <!-- CONTENT HERE -->
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><?=$tab_menu_html?></td>				 
              </tr>
              <tr>
                <td class="navigation"><?=$navigation_html?></td>
              </tr>
              <tr>
								<td class="tabletext">
									<table width="100%" border="0" cellspacing="0" cellpadding="2">
										<tr>
<?php if($new_rec_allow) { ?>
											<td>
  											<a href="javascript:newWindow('ole_record_pool_add.php?IntExt=<?=$IntExt?>&StudentID=<?=$StudentID?>', 1)" class="contenttool">
  											<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_new.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_new?> </a>
											</td>
<?php } ?>
											<?=$op_result?>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="right">
                                    <div class="common_table_tool">
                                        <a href="javascript:checkRemove(document.form1,'record_id[]','ole_record_pool_remove.php')" class="tool_delete"><?php echo $ec_iPortfolio['delete'];?></a>
                                    </div>
								</td>
							</tr>
							<tr>
							 <td>
							   <?=$table_content?>
							 </td>
							</tr>
						</table>
            <!-- End of CONTENT -->
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $LibTable->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $LibTable->order; ?>">
<input type="hidden" name="field" value="<?php echo $LibTable->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$LibTable->page_size?>" />

<input type="hidden" name="StudentID" value="<?=$StudentID?>" />
<input type=hidden name="IntExt" value="<?=$IntExt?>">

</form>