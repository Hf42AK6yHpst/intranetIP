<script language="JavaScript">

function swap_row(jParRowObj, jParRecordID, jParDir)
{
	StartRowIndex = <?=($IntExt==0)?2:1?>;
	
	var ParentTable = jParRowObj.parentNode;
	var nRows = ParentTable.rows;
	var SourceRowIndex = jParRowObj.rowIndex;
	var TargetRowIndex = SourceRowIndex + jParDir;
	
	// Check outrange
	if(TargetRowIndex >= nRows.length || TargetRowIndex < StartRowIndex)
		return;
	// Update database
  $.ajax({
    method: "get",
    url: "../../ajax/swap_pkms_ole_row_ajax.php",
    data: "SourceRecordID="+jParRecordID+"&Direction="+jParDir+"&IntExt=<?=$IntExt?>",
    success: function(html) {
			// Swap row
			for(i=1; i<jParRowObj.cells.length; i++)
			{
				SourceRowCellHTML = jParRowObj.cells[i].innerHTML;
				ParentTable.rows[SourceRowIndex].cells[i].innerHTML = ParentTable.rows[TargetRowIndex].cells[i].innerHTML;
				ParentTable.rows[TargetRowIndex].cells[i].innerHTML = SourceRowCellHTML;
			}
    },
    error: function() {
			alert("<?=$ec_warning['update_fail']?>");
		}
  });
}

</script>

<FORM name="form1" method="GET">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
			<table width="96%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
            <!-- CONTENT HERE -->
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><?=$tab_menu_html?></td>				 
              </tr>
              <tr>
                <td class="navigation"><?=$navigation_html?></td>
              </tr>
              <tr>
								<td class="tabletext">
									<table width="100%" border="0" cellspacing="0" cellpadding="2">
										<tr>
<?php if($new_rec_allow) { ?>
											<td>
  											<a href="javascript:newWindow('pkms_record_pool_add.php?IntExt=<?=$IntExt?>&StudentID=<?=$StudentID?>', 1)" class="contenttool">
  											<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_new.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_new?> </a>
											</td>
<?php } ?>
											<?=$op_result?>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="right">		
									<table border="0" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td width="21"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_01.gif" height="23" width="21"></td>
												<td background="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_02.gif">
													<table border="0" cellpadding="0" cellspacing="2">
														<tbody>
															<tr>
																<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/10x10.gif" width="5"></td>
																<td nowrap="nowrap"><a href="javascript:checkRemove(document.form1,'record_id[]','pkms_record_pool_remove.php')" class="tabletool"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/icon_delete.gif"  name="imgDelete" align="absmiddle" border="0" ><?=$ec_iPortfolio['delete'] ?> </a></td>
															</tr>
														</tbody>
													</table>
												</td>
												<td width="6"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_03.gif" height="23" width="6"></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
							 <td>
							   <?=$table_content?>
							 </td>
							</tr>
						</table>
            <!-- End of CONTENT -->
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $LibTable->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $LibTable->order; ?>">
<input type="hidden" name="field" value="<?php echo $LibTable->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$LibTable->page_size?>" />

<input type="hidden" name="StudentID" value="<?=$StudentID?>" />
<input type=hidden name="IntExt" value="<?=$IntExt?>">

</form>