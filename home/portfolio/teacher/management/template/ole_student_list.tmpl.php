<?php
/**
 * [Modification Log] Modifying By: 
 * ************************************************
 * 2010-05-06 Max (201005061355)
 * - More eye catching Link
 * ************************************************
 */
?>
<script language="JavaScript">
function doExport(my_type){
	newWindow("../../profile/"+my_type+"/data_export.php", 27);
}

function jSUBMIT_FORM(){
  document.form1.submit();
}

function js_Changed_Keyword(e) {
	Click_SearchBox(e, document.getElementById('form1'));
}
</script>

<FORM name="form1" method="GET">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td align="center">
      <?=$tab_menu_html?>
  	</td>
  </tr>
  <tr>
    <td>
    	<?=$new_btn_html?>&nbsp;&nbsp;<?=$import_btn_html?>&nbsp;&nbsp;<?=$export_btn_html?>&nbsp;&nbsp;<?=$comment_html?>
    	<?=$searchbox_html?>
    </td>
  </tr>
  <tr>
    <td>
      <?=$ay_selection_html?>
      <?=$student_selection_html?>
      <?=$yc_selection_html?>
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="100%">
            <table border="0" cellpadding="3" cellspacing="0">
    					<tr>					
    						<td class="navigation">
    						<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" align="absmiddle">
    						<?=$ec_iPortfolio['student_list']?>				
    						</td>
    					</tr>					
  					</table>
					</td>
        </tr>
        <tr>
          <td colspan="2">
            <?=$table_content?>
      		</td>
      	</tr>
      </table>
    </td>
  </tr>
</table>

<input type="hidden" name="pageNo" value="<?= $pageNo ?>">
<input type="hidden" name="order" value="<?= $order ?>">
<input type="hidden" name="field" value="<?= $field ?>">
<input type="hidden" name="numPerPage" value="<?= $numPerPage ?>">
<input type="hidden" name="page_size_change">
<input type="hidden" name="FromPage" value="program" >
<input type="hidden" name="IntExt" value="<?=$IntExt?>">
</FORM>