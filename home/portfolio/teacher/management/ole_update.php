<?php
# modifing by 

##########################################################
## Modification Log
## 2016-05-23 Henry HM [2016-0415-1449-22214]
## - Added field of "Default Hours"
##
## 2015-05-06: Bill	[2015-0324-1420-39164]
## - Add Teacher-in-charge to DB
##
## 2015-01-12: Bill	
## - Update SAS Category and SAS Credit Point for Ng Wah SAS Cust
##
## 2010-02-22: Max (201002181147)
## - Add config for cnecc
##########################################################
/**
 * Type			: Enhancement
 * Date 		: 200911230911
 * Description	: Teacher role
 * 				  1C) Add fields [Allow student to join], [Join period]
 * 				  Student role
 * 				  2) Allow students to see the list of Student Learning Profile(SLP) to join, show within editable period and type == 1
 * By			: Max Wong
 * Case Number	: 200911230911MaxWong
 * C=CurrentIssue 
 * ----------------------------------
 * Type			: Enhancement
 * Date 		: 200911131142
 * Description	: 1C) Enhance the form to New an OLE with chinese title and chinese details
 * C=CurrentIssue 2) The import page is required to support chinese title, chinese details and add school remarks
 * Case Number	: 200911131142MaxWong
 */ 



$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libpf-ole-program-tag.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/ole_program.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting.php");


intranet_auth();
intranet_opendb();



# Get parameters from previous page
$allowStudentsToJoin = $_POST['allowStudentsToJoin'];
$allowStudentsToJoin = (isset($allowStudentsToJoin)) ? $allowStudentsToJoin : 0 ;
//echo "This is the allow student to join -> $allowStudentsToJoin<br/>";die;
//HANDLE USER INPUT VARIABLE
if ($allowStudentsToJoin == 0) {
	$jp_periodStart = "null";
	$jp_periodEnd = "null";
	$joinableYear = "null";
	$autoApprove = 0;
} else {
	$autoApprove			= $_POST['autoApprove'];
//	$jp_periodStart			= $_POST['jp_periodStart'];
//	$jp_periodEnd			= $_POST['jp_periodEnd'];
  $joinableYear			= (sizeof($_POST['Year'])!=0) ? implode(",", $_POST['Year']) : "";
	$jp_periodStart			= ($_POST['jp_periodStart'] == "")? "null": $_POST['jp_periodStart'];
	$jp_periodEnd			= ($_POST['jp_periodEnd'] == "") ? "null": $_POST['jp_periodEnd'];
}
$academicYearID = $_POST['academicYearID'];
$yearTermID = $_POST['YearTermID'];	
$title = HTMLtoDB($title);
$category = HTMLtoDB($category);
$role = HTMLtoDB($role);
$SchoolRemarks = HTMLtoDB($SchoolRemarks);
$organization = HTMLtoDB($organization);
$details = HTMLtoDB($details);
$ELEList = (sizeof($ele)!=0) ? implode(",", $ele) : "";
$startdate = $startdate ? date($startdate) : date("Y-m-d");
$enddate = ($startdate=="" || $startdate=="0000-00-00") ? "" : $enddate; // set end date as empty if start date is empty
$enddate = ($enddate=="" || $enddate=="0000-00-00") ?"null":$enddate;
$maximumHours = $_POST['maximumHours'];
$defaultHours = $_POST['defaultHours'];
$compulsoryFields = is_array($_POST['compulsoryFields'])?implode("",$_POST['compulsoryFields']):"";
$MergeProgramIDs = unserialize(stripslashes($MergeProgramIDs));
$Action = $_POST['Action'];
//$comeFrom = $_POST['comeFrom'];
// Added: 2015-01-12
$SASPoint = ($SASCategory == 0)? '' : $SASPoint;
$SASPoint = (trim($SASPoint) == '')? '' : intval($SASPoint);

$LibPortfolio = new libportfolio();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$LibPortfolio->ACCESS_CONTROL("ole");


# Find the academic school year	
$li = new libdb();

if($RecordID=="")
{
//	debug_r();
	$objOLEPROGRAM = new ole_program();
	$objOLEPROGRAM->setProgramType($ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]);
	$objOLEPROGRAM->setTitle($englishTitle);
	$objOLEPROGRAM->setTitleChi($chineseTitle);
	$objOLEPROGRAM->setStartDate($startdate);
	$objOLEPROGRAM->setEndDate($enddate);
	$objOLEPROGRAM->setCategory($category);
	$objOLEPROGRAM->setSubCategoryID($subCategory);
	$objOLEPROGRAM->setOrganization($organization);
	$objOLEPROGRAM->setDetails($englishDetails);
	$objOLEPROGRAM->setDetailsChi($chineseDetails);
	$objOLEPROGRAM->setSchoolRemarks($SchoolRemarks);
	$objOLEPROGRAM->setELE($ELEList);
	$objOLEPROGRAM->setModifyBy($UserID);
	$objOLEPROGRAM->setCreatorID($UserID);
	$objOLEPROGRAM->setIntExt($SubmitType);
	$objOLEPROGRAM->setAcademicYearID($academicYearID);
	$objOLEPROGRAM->setYearTermID($yearTermID);
	$objOLEPROGRAM->setCanJoin($allowStudentsToJoin);
	$objOLEPROGRAM->setCanJoinStartDate($jp_periodStart);
	$objOLEPROGRAM->setCanJoinEndDate($jp_periodEnd);
	$objOLEPROGRAM->setAutoApprove($autoApprove);
	$objOLEPROGRAM->setJoinableYear($joinableYear);
	$objOLEPROGRAM->setMaximumHours($maximumHours);
	$objOLEPROGRAM->setDefaultHours($defaultHours);
	$objOLEPROGRAM->setCompulsoryFields($compulsoryFields); 
	$objOLEPROGRAM->setDefaultApprover($request_approved_by);
	$objOLEPROGRAM->setIsSAS($IsSAS);
	$objOLEPROGRAM->setIsOutsideSchool($InOutSideSchool);
	
	// Added: 2015-01-12
	if($sys_custom['NgWah_SAS']){
		$objOLEPROGRAM->setSASCategory($SASCategory);
		$objOLEPROGRAM->setSASPoint($SASPoint);
	}
	
//	if (strtoupper($Action)=="MERGE") {
//		$objOLEPROGRAM->setComeFrom($comeFrom);
//	} else {
		$objOLEPROGRAM->setComeFrom($ipf_cfg["OLE_PROGRAM_COMEFROM"]["teacherInput"]);
//	}
	
	
	$RecordID = $objOLEPROGRAM->SaveProgram();	// RecordID here is the ProgramID indeed

	if ($RecordID && is_array($MergeProgramIDs)) {
		$mergeResult = ole_program::MergePrograms($RecordID,$MergeProgramIDs);
	}
	

	$msg = 1;
}
else
{

	$objOLEPROGRAM = new ole_program($RecordID);

	$objOLEPROGRAM->setTitle($englishTitle);
	$objOLEPROGRAM->setTitleChi($chineseTitle);
	$objOLEPROGRAM->setStartDate($startdate);
	$objOLEPROGRAM->setEndDate($enddate);
	$objOLEPROGRAM->setCategory($category);
	$objOLEPROGRAM->setSubCategoryID($subCategory);
	$objOLEPROGRAM->setOrganization($organization);
	$objOLEPROGRAM->setDetails($englishDetails);
	$objOLEPROGRAM->setDetailsChi($chineseDetails);
	$objOLEPROGRAM->setSchoolRemarks($SchoolRemarks);
	$objOLEPROGRAM->setELE($ELEList);
	$objOLEPROGRAM->setModifyBy($UserID);
//	$objOLEPROGRAM->setCreatorID($UserID);  //SINCE IT IS A UPDATE ACTION, SUPPOSE CANNOT UPDATE THE PROGRAM CREATOR ID
	$objOLEPROGRAM->setIntExt($SubmitType);
	$objOLEPROGRAM->setAcademicYearID($academicYearID);
	$objOLEPROGRAM->setYearTermID($yearTermID);
	$objOLEPROGRAM->setCanJoin($allowStudentsToJoin);
	$objOLEPROGRAM->setCanJoinStartDate($jp_periodStart);
	$objOLEPROGRAM->setCanJoinEndDate($jp_periodEnd);
	$objOLEPROGRAM->setAutoApprove($autoApprove);
	$objOLEPROGRAM->setJoinableYear($joinableYear);
	$objOLEPROGRAM->setMaximumHours($maximumHours);
	$objOLEPROGRAM->setDefaultHours($defaultHours);
	$objOLEPROGRAM->setCompulsoryFields($compulsoryFields);
	$objOLEPROGRAM->setDefaultApprover($request_approved_by);
	$objOLEPROGRAM->setIsSAS($IsSAS);
	$objOLEPROGRAM->setIsOutsideSchool($InOutSideSchool);
	
	// Added: 2015-01-12
	if($sys_custom['NgWah_SAS']){
		$objOLEPROGRAM->setSASCategory($SASCategory);
		$objOLEPROGRAM->setSASPoint($SASPoint);
	}
	
	$objOLEPROGRAM->SaveProgram();
	
	//SYNC THE PROGRAM TITLE NAME FOR OLE_STUDENT ALSO
	$fields_values = "Title = '$englishTitle', ";
	$fields_values .= "Category = '$category', ";
	$fields_values .= "ELE = '$ELEList', ";
	$fields_values .= "Organization = '$organization', ";
	$fields_values .= "StartDate = '$startdate', ";
	$fields_values .= "EndDate = '$enddate', ";
	$fields_values .= "ModifiedDate = now(),";
	$fields_values .= "IntExt = '$SubmitType' ";
	$sql = "UPDATE {$eclass_db}.OLE_STUDENT SET $fields_values WHERE ProgramID = '$RecordID'  ";
	$LibPortfolio->db_db_query($sql);
	
	$msg = 2;
}

# Add Program mapping
if($RecordID != "")
{
	$lpf_ole_program_tag = new libpf_ole_program_tag();
	$lpf_ole_program_tag->SET_CLASS_VARIABLE("program_id", $RecordID);
	$lpf_ole_program_tag->SET_CLASS_VARIABLE("tag_id", $tagID);
	$lpf_ole_program_tag->DELETE_PROGRAM_MAPPING(); // 1.delete all program mapping
	if($tagID !="")
	{
		$lpf_ole_program_tag->ADD_PROGRAM_MAPPING(); // 2.re-add program mapping
	}
  
	# OEA Mapping
	$liboea = new liboea();
	if ($liboea->isEnabledOEAItem())
	{
		if ($IsOEAMapping)
		{
			$_MappingDataArr = array();
			
			$_MappingDataArr['OEA_ProgramCode'] = $ItemCode;
			$_MappingDataArr['OLE_ProgramID'] = $RecordID;
			$_MappingDataArr['DefaultAwardBearing'] = $awardBearing;
			//$_MappingDataArr['DefaultParticipation'] = $_OLE_ProgramNature;
			if ($liboea->Check_If_OLE_OEA_Mapping_Exist($RecordID))
			{
				// update
				$resultOEA = $liboea->Update_OLE_OEA_Mapping($RecordID, $_MappingDataArr);
			}
			else
			{
				// insert
				$resultOEA = $liboea->Insert_OLE_OEA_Mapping($_MappingDataArr);
			}
		} else
		{
			# try to remove OEA mapping!
			if ($liboea->Check_If_OLE_OEA_Mapping_Exist($RecordID))
			{
				$resultOEA = $liboea->Remove_OLE_OEA_Mapping($RecordID);
			}
		}
	}
	
	# [2015-0324-1420-39164] - Add Teacher-in-charge to DB
	if($sys_custom['iPortfolio_ole_record_tic']){
		
		// remove current PICs
		$sql = "DELETE FROM {$eclass_db}.OLE_PROGRAM_TIC WHERE ProgramID = $RecordID";
		$LibPortfolio->db_db_query($sql);
		
		// add PICs
		if(isset($teacher_PIC) && sizeof($teacher_PIC) != 0)
		{
			$fieldname = "ProgramID,TeacherID,InputBy,DateInput,ModifiedBy,DateModified";
			$values = "";
			
			$delim = "";
			$teacher_PIC = array_unique($teacher_PIC);
			for($i=0; $i<sizeof($teacher_PIC); $i++) {
				$this_PIC = $teacher_PIC[$i];
				$this_PIC = str_replace("&#160;", "", $this_PIC);
				$this_PIC = str_replace("U", "", $this_PIC);
				if(trim($this_PIC) != "") {
					$values .= "$delim ('$RecordID','$this_PIC','$UserID',now(),'$UserID',now())";
					$delim = ",";
				}
			}
			
			$sql = "INSERT INTO {$eclass_db}.OLE_PROGRAM_TIC ($fieldname) VALUES $values";
			$LibPortfolio->db_db_query($sql);
		}
	}
}

/////////////////////// CNECC Cust :start/////////////////////////
if($sys_custom['cnecc_SS'])
{
	$setSunshine = $setSunshine;
	$cnecc_event_id = $cnecc_event_id;
	$sizeOfCnecc_event_id = count($cnecc_event_id);

	$eventInsertArray = array();
	$eventUpdateArray = array();
	
	for($i=0;$i<$sizeOfCnecc_event_id;$i++) {
			$attstr = "cnecc_event_info_A".$cnecc_event_id[$i];
			$rolstr = "cnecc_event_info_R".$cnecc_event_id[$i];
			$poistr = "cnecc_event_info_P".$cnecc_event_id[$i];
			$attvar = ${$attstr}[0];
			$rolvar = ${$rolstr}[0];
			$poivar = ${$poistr}[0];
		if (substr($cnecc_event_id[$i],0,2) == "_N") {
			$eventInsertArray[] = array("ATTAINMENT"=>${attvar},"ROLE"=>${rolvar},"POINT"=>${poivar});
		} else {
			$eventUpdateArray[$cnecc_event_id[$i]]["ATTAINMENT"] = ${attvar};
			$eventUpdateArray[$cnecc_event_id[$i]]["ROLE"] = ${rolvar};
			$eventUpdateArray[$cnecc_event_id[$i]]["POINT"] = ${poivar};
		}
	}
//	debug_r($eventUpdateArray);die;
	include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cnecc/libpf-slp-cnecc.php");
	$libpf_slp_cnecc = new libpf_slp_cnecc();

	if ($setSunshine == 1) {
		$q_result = $libpf_slp_cnecc->saveCcseProgram($RecordID, $eventInsertArray, $eventUpdateArray);
	} else {
		$libpf_slp_cnecc->removeCcseProgram($RecordID);
	}
}
/////////////////////// CNECC Cust :end/////////////////////////
intranet_closedb();




if(trim($passBackURL) != ""){
	$targetURL = trim($passBackURL);
	$passBackParameter = $passBackParameter;
$redirect = <<<HTML
	<html>
	<body>	
	<form name="form1" method="post" action="$passBackURL">
	<input type ="hidden" name="passBackParameter" value="$passBackParameter">
	<input type ="hidden" name="passBackURL" value="$passBackURL">
	</form>
	<script>
	document.form1.submit();
	</script>
	</body>
	</html>
HTML;
echo $redirect;
	exit();
}


########################################
## END HADNLE REDIRECT PATH base on action save in session
########################################


########################################
## Handling merge result if merge performed

if ($mergeResult===true) {
	$mergeResultToNextPage = 'msg=2';
} else if (isset($mergeResult)) {
	$mergeResultToNextPage = 'msg=4';
}
if (isset($mergeResultToNextPage)) {
	header("Location: ole_event_studentview.php?IntExt=$IntExt&EventID=$RecordID&$mergeResultToNextPage");
	exit();
}
########################################



if($RecordID=="")
{
	header("Location: ole_event_result.php?msg=$msg&Year=$Year&EventID=$RecordID&IntExt=$IntExt");
	exit();
}
else
{
	if($StudentID=="")
	{
		header("Location: ole_event_result.php?msg=$msg&Year=$Year&EventID=$RecordID&IntExt=$IntExt");
		exit();
	}
}

?>

<!-- Eric Yip (20090702): To add OLE Record directly to dedicate student -->
<html>
<body>
<form name="form1" method="POST" action="ole_student_new2.php?IntExt=<?=$IntExt?>" >

<input type="hidden" name="EventID" value="<?=$RecordID?>" />
<input type="hidden" name="student[]" value="<?=$StudentID?>" />
<input type="hidden" name="backURL" value="ole_student.php?IntExt=<?=$IntExt?>&StudentID=<?=$StudentID?>" />

</form>

<script language="JavaScript">
	document.form1.submit();
</script>

</body>
</html>