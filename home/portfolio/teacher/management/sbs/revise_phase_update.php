<?php

// Modifing by key
/**
 * 2009-12-03: Eric Yip
 * - Distiguish between sbs and weblog in handin 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");

iportfolio_auth("TA");
intranet_auth();
intranet_opendb();

$lpf = new libpf_sbs();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

$lgs = new growth_scheme();

$phase_obj = $lgs->getPhaseInfo($phase_id);
$phase_obj = $phase_obj[0];
$answer = HTMLtoDB($aStr);

$li = new libdb();
if ($handin_id!="")
{
	if($ReturnPage == "view_student_info")
	{
		$user_id = $CurParUserID;
	}
	
	$sql = "UPDATE $lpf->course_db.handin SET answer='$answer', filename=NULL, inputdate=now(), status='L', comment=NULL, marked_by = '$ck_user_id' WHERE handin_id='$handin_id' and user_id='$user_id'";
	$msg = 2;
} 
else
{
	if($ReturnPage == "view_student_info")
	{
		$user_id = $CurParUserID;
	}
	
	$newStatus = "NULL";
	$sql = "INSERT INTO $lpf->course_db.handin (assignment_id, user_id, answer, comment, marked_by, status, inputdate, type) VALUES ('$phase_id', '$user_id', '$answer', '$newStatus', '$ck_user_id', 'L', now(), 'sbs')";
	$msg = 1;
}

$li->db_db_query($sql);

intranet_closedb();

if($ReturnPage == "phase_form")
{
	header("Location: phase_form.php?StudentID={$StudentID}&ClassName={$ClassName}&user_id={$user_id}&parent_id={$assignment_id}&assignment_id={$phase_id}&msg=$msg");	
}
else if($ReturnPage == "view_student_info")
{
	header("Location: view_student_info.php?ParUserID={$CurParUserID}&parent_id={$assignment_id}&phase_id={$phase_id}&msg=$msg");
}
else
{
	header("Location: revise_phase.php?StudentID={$StudentID}&ClassName={$ClassName}&user_id={$user_id}&phase_id={$phase_id}&assignment_id={$assignment_id}&msg=$msg");
}
?>
