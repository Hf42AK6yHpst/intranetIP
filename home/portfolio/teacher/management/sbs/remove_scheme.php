<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");

iportfolio_auth("TA");
intranet_auth();
intranet_opendb();

$li = new libdb();
$lpf = new libpf_sbs();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

// resolve the assignment_id to array
$tmp_assignment_id = array();
$tmp_assignment_id[] = $assignment_id;
$assignment_id = $tmp_assignment_id;

if (is_array($assignment_id))
{
	$assignment_id_str = implode(",", $assignment_id);

	# find child assignments (phases)
	$sql = "SELECT assignment_id FROM $lpf->course_db.assignment WHERE parent_id IN ($assignment_id_str) ";
	$row = $li->returnVector($sql);

	# delete handins
	if (sizeof($row)>0)
	{
		$assignment_id_str .= "," . implode(",", $row);
	}
	$sql = "DELETE FROM $lpf->course_db.handin WHERE assignment_id IN ($assignment_id_str) ";
	$li->db_db_query($sql);

	# from joining records
	$sql = "DELETE FROM $lpf->course_db.grouping_function WHERE function_id IN ($assignment_id_str) AND function_type='A' ";
	$li->db_db_query($sql);

	# delete assignments
	$sql = "DELETE FROM $lpf->course_db.assignment WHERE assignment_id IN ($assignment_id_str) ";
	$li->db_db_query($sql);

	$msg = 3;
}

header("Location: index.php?msg=$msg");

intranet_closedb();
?>
