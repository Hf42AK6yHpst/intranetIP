<?php

# Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();
$lpf = new libpf_sbs();
// check iportfolio admin user
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

$lexport = new libexporttext();

$lgs = new growth_scheme();
$phase_id = (is_array($phase_id)) ? $phase_id[0] : $phase_id;

$phase_obj = $lgs->getPhaseInfo($phase_id);
$phase_obj = $phase_obj[0];

# relevant person preparation
$target_person = $lpf->GET_ROLE_TYPE($phase_obj["person_response"]);

$aid  = $phase_obj["parent_id"];

$assignment_obj = $lgs->getSchemeInfo($aid);
$assignment_obj = $assignment_obj[0];

# get handed-in user
$handin_obj = $lgs->getHandedinUserForExport($phase_obj);

$relevant_phase = $lgs->getRelevantPhaseText($phase_obj["answer"]);
	
$InfoContent .= "\"".$ec_iPortfolio['growth_scheme_title']." : ".$assignment_obj["title"]."\"\n";
$InfoContent .= "\"".$ec_iPortfolio['growth_phase_title']." : ".$phase_obj["title"]."\"\n";
$InfoContent .= "\"".$ec_iPortfolio['growth_description']." : ".$phase_obj["instruction"]."\"\n";
$InfoContent .= "\"".$ec_iPortfolio['growth_target_person']." : ".$target_person."\"\n";
$InfoContent .= "\"".$ec_iPortfolio['growth_phase_period']." : ".$phase_obj["starttime"]." ".$profiles_to." ".$phase_obj["deadline"] ."\"\n";
$InfoContent .= "\"".$ec_iPortfolio['growth_relevant_phase']." : ".$relevant_phase."\"\n";
$InfoContent .= "\n";
	
# process the question string
list($QuestionContent, $QuestionInfoArr) = $lgs->returnExportQuestionContent($phase_obj);

# process the answer string
$HandinContent = $lgs->returnExportHandinContent($handin_obj, $QuestionInfoArr);

$export_content = $InfoContent.$QuestionContent.$HandinContent;

$u_agent = $_SERVER['HTTP_USER_AGENT'];
if(preg_match('/MSIE/i',$u_agent))
  $filename = iconv("UTF-8", "Big5", $phase_obj["title"]).".csv";
else
  $filename = $assignment_obj["title"].".csv";
	
// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>