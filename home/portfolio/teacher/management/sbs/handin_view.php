<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio_group.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_SchoolBasedScheme";
$CurrentPageName = $iPort['menu']['school_based_scheme'];

### Init Library ###
$lpf = new libpf_sbs();
$lpf_ui = new libpf_sbs();
$lo = new libportfolio_group($lpf->course_db);
$luser = new libuser($UserID);
$lgs = new growth_scheme();

// check iportfolio admin user
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

//////////////////////////////////////////////////////
////////////   GET THE PAGE CONTENT  /////////////////
//////////////////////////////////////////////////////
// page number settings
if ($order=="") $order=1;
if ($field=="") $field=3;

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if ($pageNo == "")
$pageNo = 1;

////////////////////////////////////////////////
$phase_obj = $lgs->getPhaseInfo($phase_id);
$phase_obj = $phase_obj[0];
$teacher_subject_id = $phase_obj[11];
$person_response = $phase_obj[8];
$person_right = $lgs->getPhaseRight($ck_memberType, $phase_obj);
$ParArr["person_right"] = $person_right;

$assignment_id  = $phase_obj["parent_id"];
$assignment_obj = $lgs->getSchemeInfo($assignment_id);
$assignment_obj = $assignment_obj[0];

// [2009-02-09] Get the selected group list (class list)
$GroupListInData = $lo->getGroupsListIn_DataValue($assignment_id, "A");
$GroupIDs = array();
for($i = 0; $i < count($GroupListInData); $i++)
{
	$GroupIDs[] = $GroupListInData[$i]["group_id"];
}
$SelectedClassList = $lo->getClassListByGroupIDList($GroupIDs, $assignment_id);

//debug_r($SelectedClassList);

# get handined user
# Call getSchemeUsers twice, one for class teacher classes, another for subject teacher classes,
# in order to handle subject groups properly and the case that a teacher who are both class teacher and subject teacher properly.
if($lpf->IS_IPF_ADMIN())
{
	$class_taught_arr = "";
}
else if($person_response == "CT")
{
	$class_taught_arr = $lpf->GET_CLASS_TEACHER_CLASS();
	$st_class_taught_arr = array();
	$all_class_taught_arr = $class_taught_arr;
}
else if($person_response == "ST")
{
	$class_taught_arr = array();
	$st_class_taught_arr = $lpf->GET_SUBJECT_TEACHER_CLASS($teacher_subject_id);
	$all_class_taught_arr = $st_class_taught_arr;
}
else
{
	$class_taught_arr = $lpf->GET_CLASS_TEACHER_CLASS();
	$st_class_taught_arr = $lpf->GET_SUBJECT_TEACHER_CLASS();
	$all_class_taught_arr = array_merge($class_taught_arr, $st_class_taught_arr);
	$teacher_subject_id = "";
}

// Please check carefully the class list
# added by Kit 20090209
if(count($SelectedClassList) > 0)
{
	if($lpf->IS_IPF_ADMIN())
	{
		$class_taught_arr = $SelectedClassList;
	}
	else 
	{
		if(is_array($class_taught_arr) && is_array($SelectedClassList))
		{			
			$class_taught_arr = array_intersect($SelectedClassList, $class_taught_arr);
		}
		if(is_array($st_class_taught_arr) && is_array($SelectedClassList))
		{
			$st_class_taught_arr = array_intersect($SelectedClassList, $st_class_taught_arr);
		}
	}
}


# Modified by Kit 20090209
if(is_array($class_taught_arr) || $lpf->IS_IPF_ADMIN())
{
	//$PhaseUsers = $lgs->getSchemeUsers($phase_id, "", $class_taught_arr);
	//$PhaseUsers = $lgs->getSchemeUsers($assignment_id, "", $class_taught_arr);
	if (is_array($class_taught_arr)) {
		if (count($class_taught_arr) > 0) {
			$PhaseUsers_ct = $lgs->getSchemeUsers($assignment_id, "", $class_taught_arr);
		}
		else {
			$PhaseUsers_ct = array();
		}
		if (count($st_class_taught_arr) > 0) {
			$PhaseUsers_st = $lgs->getSchemeUsers($assignment_id, "", $st_class_taught_arr);
			// Only accept students actually taught by the subject teacher only.
			$subject_students = $lpf->IP_USER_ID_TO_EC_USER_ID_SET_OPERATION($lpf->GET_SUBJECT_TEACHER_STUDENT($ck_intranet_user_id, $teacher_subject_id));
			$PhaseUsers_st = array_intersect($PhaseUsers_st, $subject_students);
		}
		else {
			$PhaseUsers_st = array();
		}
		$PhaseUsers = array_unique(array_merge($PhaseUsers_ct, $PhaseUsers_st));
	}
	else {
		$PhaseUsers = $lgs->getSchemeUsers($assignment_id);
	}
	# get handined user
	$HandinListUsers = $lgs->getHandedinUser($phase_id);
	
	# get Not handed in Student List
	for($i=0; $i<sizeof($PhaseUsers); $i++)
	{
		if(!in_array($PhaseUsers[$i], $HandinListUsers))
		{
			$StudentIDArr[] = $PhaseUsers[$i];
		}
		else
		{
			$StudentIDArr_HandIn[] = $PhaseUsers[$i];
		}
	}
	
	$HandedinStudentArr = $lgs->getUserInfo($StudentIDArr_HandIn);
	$NotHandedinStudentArr = $lgs->getUserInfo($StudentIDArr);
}


$HandInArr["HandinListUsers"] = $HandinListUsers;
$HandInArr["NotHandedinStudentArr"] = $NotHandedinStudentArr;

$TmpClassArr = array();
$TmpClassArr = $lpf->GET_CLASS_LIST($Year, $ClassLevel, $all_class_taught_arr);

### Contruct the table row ###
$UserRecordArr = array();
$NumOfHandIn = 0;
// loop handin student
$countNum = 0;

for($i = 0; $i < count($HandedinStudentArr); $i++)
{
	$TmpUserID = $HandedinStudentArr[$i]["user_id"];
	$Name = $HandedinStudentArr[$i]["firstname"];
	$TmpClass = $HandedinStudentArr[$i]["class_number"];
	list($ClassName, $ClassNo) = explode('-', $TmpClass);
	$ClassName = trim($ClassName);
	$ClassNo = (int) trim($ClassNo);
	//$ClassNo = ($ClassNo == "") ? $ClassNo : str_pad(trim($ClassNo), 2, "0", STR_PAD_LEFT);
	
	$TmpData = $lgs->getHandedinUserSubmittedDate($phase_id,"", $TmpUserID);

	# Eric Yip (20090619): Not use LastUsed as submit date
/*	
	$SubmitDate1 = $TmpData[0]["LastUsed"];
	$SubmitDate2 = $TmpData[0]["inputdate"];
	
	if($SubmitDate1 != "" && $SubmitDate2 != "")
	{
		if($SubmitDate1 > $SubmitDate2)
		{
			$SubmitDate = $SubmitDate1;
		}
		else
		{
			$SubmitDate = $SubmitDate2;
		}
	}
	
	if($SubmitDate == "" && $SubmitDate1 != "")
	$SubmitDate = $SubmitDate1;
	else if($SubmitDate == "" && $SubmitDate2 != "")
	$SubmitDate = $SubmitDate2;
*/
	$SubmitDate = $TmpData[0]["inputdate"];

	if($SubmitDate == "")
	$SubmitDate = "--";
	
	$canList = false;
	
	if($ClassName == $Class)
	{
		$canList = true;
	}	
	else if($Class == "" && $ClassLevel != "")
	{
		if(in_array($ClassName, $TmpClassArr))
		$canList = true;
	}
	else if($Class == "" && $ClassLevel == "")
	{
		$canList = true;
	}
	
	if($canList == true)
	{
		$TmpArr = array("UserID" => $TmpUserID, "UserName" => $Name, "ClassName" => $ClassName, "ClassNo" =>  $ClassNo, "SubmitDate" => $SubmitDate, "Status" => 1);
		$UserRecordArr[] = $TmpArr;
		$NumOfHandIn++;
		
	$countNum++;
	$TableContent_1 .= "<tr class=\"tablerow1\" height=\"25\">";
	$TableContent_1 .= "<td class=\"row_on\" align=\"center\">".$countNum."</td>";
	$TableContent_1 .= "<td class=\"row_on tabletext\">".$Name."</td>";
	$TableContent_1 .= "<td class=\"row_on\">".$ClassName."</td>";
	$TableContent_1 .= "<td class=\"row_on\">".$ClassNo."</td>";
	$TableContent_1 .= "<td class=\"row_on\"><a href=\"view_student_info.php?ParUserID=$TmpUserID&phase_id=$phase_id&parent_id=$parent_id\"><img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a></td>";
	$TableContent_1 .= "<td class=\"row_on\">".$SubmitDate."</td>";
	$TableContent_1 .= "</tr>";
	}
}

for($i = 0; $i < count($NotHandedinStudentArr); $i++)
{
	$TmpUserID = $NotHandedinStudentArr[$i]["user_id"];
	$Name = $NotHandedinStudentArr[$i]["firstname"];
	$TmpClass = $NotHandedinStudentArr[$i]["class_number"];
	list($ClassName, $ClassNo) = explode('-', $TmpClass);
	$ClassName = trim($ClassName);
	$ClassNo = (int) trim($ClassNo);
	//$ClassNo = ($ClassNo == "") ? $ClassNo : str_pad(trim($ClassNo), 2, "0", STR_PAD_LEFT);
	
	$SubmitDate = "--";
	
	if($ClassName == "")
	$ClassName = "&nbsp;";
	
	if($ClassNo == "")
	$ClassNo = "&nbsp;";
	
	$canList = false;
	
	if($ClassName == $Class)
	{
		$canList = true;
	}	
	else if($Class == "" && $ClassLevel != "")
	{
		if(in_array($ClassName, $TmpClassArr))
		$canList = true;
	}
	else if($Class == "" && $ClassLevel == "")
	{
		$canList = true;
	}
	
	if($canList == true)
	{
		$TmpArr = array("UserID" => $TmpUserID, "UserName" => $Name, "ClassName" => $ClassName, "ClassNo" =>  $ClassNo, "SubmitDate" => $SubmitDate, "Status" => 0);
		$UserRecordArr[] = $TmpArr;
		
	$countNum++;
	$TableContent_2 .= "<tr class=\"tablerow1\" height=\"25\">";
	$TableContent_2 .= "<td class=\"row_off\" align=\"center\">".$countNum."</td>";
	$TableContent_2 .= "<td class=\"row_off tabletext\">".$Name."</td>";
	$TableContent_2 .= "<td class=\"row_off\">".$ClassName."</td>";
	$TableContent_2 .= "<td class=\"row_off\">".$ClassNo."</td>";
	$TableContent_2 .= "<td class=\"row_off\">--</td>";
	$TableContent_2 .= "<td class=\"row_off\">--</td>";
	$TableContent_2 .= "</tr>";
	}
}

$TmpContent = "";

$SortFieldArr = array("UserID", "UserName", "ClassName", "ClassNo", "Status", "SubmitDate");

$target = array();
$TmpField = $SortFieldArr[$field+1];
foreach ($UserRecordArr as $key => $row) 
{
	$target[$key] = $row[$TmpField];
	$classnameArr[$key] = $row["ClassName"];
	$classnumberArr[$key] = $row["ClassNo"];
}

if(is_array($UserRecordArr) && sizeof($UserRecordArr) > 0)
{
	if($order)
	array_multisort($target, SORT_DESC,$classnameArr, SORT_ASC, $classnumberArr, SORT_ASC, $UserRecordArr);
	else
	array_multisort($target, SORT_ASC,$classnameArr, SORT_ASC, $classnumberArr, SORT_ASC, $UserRecordArr);
}

$StartIndex = ($pageNo - 1) * $page_size;
$EndIndex = $StartIndex + $page_size;

if($StartIndex == "" || $StartIndex < 0)
$StartIndex = 0;

if($EndIndex > count($UserRecordArr))
$EndIndex = count($UserRecordArr);

for($i = $StartIndex; $i < $EndIndex; $i++)
{
	$TmpUserID = $UserRecordArr[$i]["UserID"];
	$TmpUserName = $UserRecordArr[$i]["UserName"];
	$TmpClassName = $UserRecordArr[$i]["ClassName"];
	$TmpClassNo = $UserRecordArr[$i]["ClassNo"];
	$TmpSubmitDate = $UserRecordArr[$i]["SubmitDate"];
	$TmpStatus = $UserRecordArr[$i]["Status"];
	
	if($TmpClassName == "")
	$TmpClassName = "&nbsp;";
	
	if($TmpClassNo == "")
	$TmpClassNo = "&nbsp;";
	
	
	if($TmpStatus == 1)
	{
		$TmpContent .= "<tr class=\"tablerow1\" height=\"25\">";
		$TmpContent .= "<td class=\"row_on\" align=\"center\">".($i + 1)."</td>";
		$TmpContent .= "<td class=\"row_on tabletext\">".$TmpUserName."</td>";
		$TmpContent .= "<td class=\"row_on\">".$TmpClassName."</td>";
		$TmpContent .= "<td class=\"row_on\">".$TmpClassNo."</td>";
		$TmpContent .= "<td class=\"row_on\"><a href=\"view_student_info.php?ParUserID=$TmpUserID&phase_id=$phase_id&parent_id=$parent_id\"><img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a></td>";
		$TmpContent .= "<td class=\"row_on\">".$TmpSubmitDate."</td>";
		$TmpContent .= "</tr>";
	}
	else
	{
		$TmpContent .= "<tr class=\"tablerow1\" height=\"25\">";
		$TmpContent .= "<td class=\"row_off\" align=\"center\">".($i + 1)."</td>";
		$TmpContent .= "<td class=\"row_off tabletext\">".$TmpUserName."</td>";
		$TmpContent .= "<td class=\"row_off\">".$TmpClassName."</td>";
		$TmpContent .= "<td class=\"row_off\">".$TmpClassNo."</td>";
		//$TmpContent .= "<td class=\"row_off\">--</td>";
		$TmpContent .= "<td class=\"row_off\"><a href=\"view_student_info.php?ParUserID=$TmpUserID&phase_id=$phase_id&parent_id=$parent_id\"><img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a></td>";
		$TmpContent .= "<td class=\"row_off\">--</td>";
		$TmpContent .= "</tr>";
	}
}


//$TableContent .= $TableContent_1.$TableContent_2;
$TableContent = $TmpContent;

### End Contruct the table row ###
$HandInArr["Content"] = $TableContent;

$li = new libdbtable2007($field, $order, $pageNo);
$li->total_row = $countNum;
$li->field_array = array("UserName", "ClassName", "ClassNumber", "Status", "SubmittedDate");
//$li->sql = $sql;
$li->no_col = 6;
$li->table_tag = "<table bgcolor='#cccccc' border='0' cellpadding='4' cellspacing='1' width='95%'>";
$li->row_alt = array("#FFFFFF", "#F3F3F3");
$li->row_height = 25;
$li->plain_column = 1;
$li->plain_column_css = "tablelink";
$li->record_num_mode = "iportfolio"; // for school based scheme total record display
$li->IsColOff = "iPortfolioPhaseHandinList";
$li->title = $iPort['table']['records'];

// TABLE COLUMN
$li->column_list .= "<td width='5%' class='tabletop tabletopnolink' height='25' align='center'>#</span></td>\n";
$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(0,$iPort['student_name'], 1)."</td>\n";
$li->column_list .= "<td width='10%' nowrap='nowrap'>".$li->column(1,$iPort['class'], 1)."</td>\n";
$li->column_list .= "<td width='10%' nowrap='nowrap'>".$li->column(2,$iPort['class_no'], 1)."</td>\n";
$li->column_list .= "<td width='10%' nowrap='nowrap'>".$li->column(3,$iPort['content'], 1)."</td>\n";
$li->column_list .= "<td width='20%' nowrap='nowrap'>".$li->column(4,$iPort['submitted_date'], 1)."</td>\n";
$li->column_array = array(10,10,0,0,0,0);
### end table

$ParArr["Role"] = "TEACHER";
$ParArr["assignment_id"] = $parent_id;
$ParArr["parent_id"] = $parent_id;
$SchemeData = $lpf->GET_SCHOOL_BASED_SCHEME($ParArr);
$ParArr["SchemeData"] = $SchemeData;

$ParArr["phase_id"] = $phase_id;
$ParArr["assignment_id"] = $phase_id;
$PhaseData = $lpf->GET_PHASE_INFO($ParArr);
$ParArr["PhaseData"] = $PhaseData;

$MenuTitle = " <strong>".$SchemeData[0]["title"]."</strong>";

$PhaseTitle = $PhaseData[0]["title"];
$SchemeTitle = $SchemeData[0]["title"];

$MenuArr = array();
$MenuArr[] = array($iPort["scheme_list"], "index.php");
$MenuArr[] = array($SchemeTitle, "");
$MenuArr[] = array($PhaseTitle, "");

$ClassLevelStr = $lpf_ui->GEN_CLASSLEVEL_SELECTION($Year, $ClassLevel, "name='ClassLevel' onChange=\"jCHANGE_FIELD('classlevel')\"", $class_taught_arr);
$ClassStr = $lpf_ui->GEN_CLASS_SELECTION($Year, $ClassLevel, $Class, $class_taught_arr);

$TotalNum = count($UserRecordArr);
$HandinNum = $NumOfHandIn;

if($TotalNum == "")
$TotalNum = 0;

if($HandinNum == "")
$HandinNum = 0;

$Content = "";
$Content .= $lpf_ui->GET_NAVIGATION($MenuArr);
$Content .= $lpf_ui->GET_PHASE_HANDIN_LIST($ParArr);

$Content .= "<FORM method='POST' name='form1' action='handin_view.php'>";
$Content .= "<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\"><tr>";
$Content .= "<td align=\"left\">".$ClassLevelStr.$ClassStr."</td>";
$Content .= "<td align=\"right\" class=\"tabletext\">".$HandinNum." ".$iPort["of"]." ".$TotalNum." ".$iPort["students_submitted"]."</td>";
$Content .= "</tr></table>";

$Content_Tail = "<input type=\"hidden\" name=\"FieldChanged\" />";
$Content_Tail .= "<input type=\"hidden\" name=\"phase_id\" value=\"".$phase_id."\" />";
$Content_Tail .= "<input type=\"hidden\" name=\"parent_id\" value=\"".$parent_id."\" />";
$Content_Tail .= "</FORM>";
$Content_Tail .= $lpf_ui->GET_SCHEME_LAYOUT_CLOSE($ParArr);
//////////////////////////////////////////////////////
//////////// END  GET THE PAGE CONTENT  //////////////
//////////////////////////////////////////////////////

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	document.form1.FieldChanged.value = jParField;
	document.form1.action = "handin_view.php";
	document.form1.submit();
}
</SCRIPT>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<!-- CONTENT HERE -->
<?

echo $Content;

//$li->displayPlain();
echo $li->display($HandInArr);

echo "<input type=\"hidden\" name=\"order\" value=\"".$li->order."\" />";
echo "<input type=\"hidden\" name=\"pageNo\" value=\"".$li->pageNo."\" />";
echo "<input type=\"hidden\" name=\"field\" value=\"".$li->field."\" />";
echo "<input type=\"hidden\" name=\"page_size_change\" value=\"".$page_size_change."\" />";
echo "<input type=\"hidden\" name=\"numPerPage\" value=\"".$li->page_size."\" />";

echo $Content_Tail;

?>


</td></tr></table>
</td></tr></table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
