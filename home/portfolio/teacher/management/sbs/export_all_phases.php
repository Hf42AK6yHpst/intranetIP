<?php

# Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();
$lpf = new libpf_sbs();
// check iportfolio admin user
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

/**
 * Get the export content of a SBS phase, in Unicode format.
 * Copied and modified from home/portfolio/teacher/management/sbs/export_phase.php.
 * @return array($export_content, $filename)
 */
function my_get_sbs_phase_export_content($phase_id)
{
	if ($phase_id == '') throw new Exception("phase_id should be given.");
	
	global $usertype_ct, $usertype_p, $usertype_s, $ec_iPortfolio, $profiles_to;
	
	$lgs = new growth_scheme();
	$lpf = new libpf_sbs();
	
	$phase_obj = $lgs->getPhaseInfo($phase_id);
	$phase_obj = $phase_obj[0];
	
	# relevant person preparation
	$target_person = $lpf->GET_ROLE_TYPE($phase_obj["person_response"]);
	
	$aid  = $phase_obj["parent_id"];
	
	$assignment_obj = $lgs->getSchemeInfo($aid);
	$assignment_obj = $assignment_obj[0];
	
	# get handed-in user
	$handin_obj = $lgs->getHandedinUserForExport($phase_obj);
	
	$relevant_phase = $lgs->getRelevantPhaseText($phase_obj["answer"]);
	
	$InfoContent .= "\"".$ec_iPortfolio['growth_scheme_title']." : ".$assignment_obj["title"]."\"\n";
	$InfoContent .= "\"".$ec_iPortfolio['growth_phase_title']." : ".$phase_obj["title"]."\"\n";
	$InfoContent .= "\"".$ec_iPortfolio['growth_description']." : ".$phase_obj["instruction"]."\"\n";
	$InfoContent .= "\"".$ec_iPortfolio['growth_target_person']." : ".$target_person."\"\n";
	$InfoContent .= "\"".$ec_iPortfolio['growth_phase_period']." : ".$phase_obj["starttime"]." ".$profiles_to." ".$phase_obj["deadline"] ."\"\n";
	$InfoContent .= "\"".$ec_iPortfolio['growth_relevant_phase']." : ".$relevant_phase."\"\n";
	$InfoContent .= "\n";
	
	# process the question string
	list($QuestionContent, $QuestionInfoArr) = $lgs->returnExportQuestionContent($phase_obj);
	
	# process the answer string
	$HandinContent = $lgs->returnExportHandinContent($handin_obj, $QuestionInfoArr);
	
	$export_content = $InfoContent.$QuestionContent.$HandinContent;
	
	// Derive a file name for the CSV.
	$filename = "{$assignment_obj["title"]} - {$phase_obj["title"]}.csv";
	// Replace some characters in the file name with '_' to prevent problems.
	$filename = str_replace(array('/', '\\', ':', '*', '?', '"', '<', '>', '|'), '_', $filename);
	
	// Convert to Unicode, following libexporttext.php::EXPORT_FILE().
	// Add BOM (Byte Order Mark) to identify the file is in UTF-16LE encoding
	$export_content = "\xFF\xFE" . mb_convert_encoding($export_content,'UTF-16LE','UTF-8');
	
	return array($export_content, $filename);
}

/**
 * Get the export content of a SBS scheme, a ZIP file containing the CSVs for the SBS phases in the scheme.
 * @return array($export_content, $filename)
 */
function my_get_sbs_scheme_export_content($parent_id)
{
	if ($parent_id == '') throw new Exception("parent_id should be given.");
	
	$lgs = new growth_scheme();
	$lpf = new libpf_sbs();
	
	$assignment_obj = $lgs->getSchemeInfo($parent_id);
	$assignment_obj = $assignment_obj[0];
	if (!$assignment_obj) throw new Exception("Cannot find the SBS scheme with the ID: $parent_id.");
	
	// Create a temporary directory for the CSVs.
	$temp_dir = tempnam('', 'sbs');
	unlink($temp_dir);
	mkdir($temp_dir);
	
	# get all phases in a scheme
	$sql = "
		SELECT
			assignment_id, title, marking_scheme
		FROM
			{$lpf->course_db}.assignment
		WHERE
			parent_id='$parent_id' AND assignment_id<>''
		ORDER BY
			marking_scheme desc ,starttime, deadline ";
	
	$rsPhases = $lpf->returnArray($sql);
	
	# get export content for each phase
	for($i=0; $i < sizeof($rsPhases); $i++)
	{
		$phase_id = $rsPhases[$i]['assignment_id'];
		
		list($export_content, $filename) = my_get_sbs_phase_export_content($phase_id);
		
		// ZIP clients may not support UTF-8 well.
		$filename = iconv('utf-8', 'big5-hkscs', $filename);
		file_put_contents("$temp_dir/$filename", $export_content);
	}
	
	// No phase? Put a dummy file for ZIP purpose.
	if (count($rsPhases) == 0) {
		file_put_contents("$temp_dir/no phase", '');
	}
	
	// Create the ZIP file for the CSVs, and get its content.
	$temp_zip = "$temp_dir.zip";
	$lf = new libfilesystem();
	$lf->file_zip('.', $temp_zip, $temp_dir);
	$export_content = file_get_contents($temp_zip);
	
	// Remove the temporary directory and file.
	$lf->folder_remove_recursive($temp_dir);
	$lf->file_remove($temp_zip);
	
	// Derive a file name for the ZIP file.
	$filename = "{$assignment_obj["title"]}.zip";
	// Replace some characters in the file name with '_' to prevent problems.
	$filename = str_replace(array('/', '\\', ':', '*', '?', '"', '<', '>', '|'), '_', $filename);
	
	return array($export_content, $filename);
}

list($export_content, $filename) = my_get_sbs_scheme_export_content($parent_id);
output2browser($export_content, $filename);

intranet_closedb();
?>