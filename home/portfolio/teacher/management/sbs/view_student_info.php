<?php

# Modifing by Pun

/**************************************************
 * Parameters:
 * - $ParUserID : classroom user_id
 * - $StudentID : intranet UserID 
 **************************************************/  

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio_group.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_SchoolBasedScheme";
$CurrentPageName = $iPort['menu']['school_based_scheme'];

### set Library ###
$lgs = new growth_scheme();
$lpf = new libpf_sbs();
$lpf_ui = new libpf_sbs();
$lo = new libportfolio_group($lpf->course_db);

// check iportfolio admin user
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

# Added on 20090210 by Kit
# Get Course User ID if empty $ParUserID
# Change Class -> get first student
if($ClassName && $ClassChanged==1)
{
	$StdAry = $lpf->returnStudentListData($ClassName);
	$StudentID = $StdAry[0][0];	
}
$ParUserID = ($ParUserID)? $ParUserID: $lpf->getCourseUserID($StudentID);

$user_id = $lpf->EC_USER_ID_TO_IP_USER_ID($ParUserID);

$StudentIDArr = array();
$StudentIDArr[] = $ParUserID;
$UserArr = $lgs->getUserInfo($StudentIDArr);

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");
	
$phase_obj = $lgs->getPhaseInfo($phase_id); // assignment_id also is phase_id
$phase_obj = $phase_obj[0];
$person_response = $phase_obj[8];
$teacher_subject_id = $phase_obj[11];

# added on 20090209 by Kit, for get Class and user drop down list
# get target group(s) of the scheme
$sql = "SELECT DISTINCT group_id FROM {$lpf->course_db}.grouping_function WHERE function_id = '{$phase_obj["parent_id"]}' AND function_type = 'A'";
$GroupIDArray = $lpf->returnVector($sql);

if(!empty($GroupIDArray))
{
	# get target classes
	$GroupIDList = implode(",", $GroupIDArray);

/*	
	if($lpf->IS_IPF_ADMIN() || $lpf->IS_ADMIN())
	{
		$ClassTaughtList = "";
	}
	else
	{
		if($person_response == "CT")
		{
			$ClassTaughtList = " AND iu.ClassName IN ('".implode("','", $lpf->GET_CLASS_TEACHER_CLASS())."')";
		}
		else if($person_response == "ST")
		{
			$ClassTaughtList = " AND iu.ClassName IN ('".implode("','", $lpf->GET_SUBJECT_TEACHER_CLASS($teacher_subject_id))."')";
		}
		else
		{
			$ClassTaughtList = " AND iu.ClassName IN ('".implode("','", array_merge($lpf->GET_SUBJECT_TEACHER_CLASS(), $lpf->GET_CLASS_TEACHER_CLASS()))."')";
		}
	}
	
	//$ClassTaughtList = ($lpf->IS_IPF_ADMIN() || $lpf->IS_ADMIN()) ? "" : " AND iu.ClassName IN (".implode(",", array_merge($lpf->GET_SUBJECT_TEACHER_CLASS(), $lpf->GET_CLASS_TEACHER_CLASS())).")";
	
	$sql = "SELECT DISTINCT
				iu.ClassName AS ClassName,
				iu.ClassName As ClassName_Title
			FROM 
				{$lpf->course_db}.user_group as ug
				LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT as ps ON ug.user_id = ps.CourseUserID
				LEFT JOIN {$intranet_db}.INTRANET_USER as iu ON ps.UserID = iu.UserID
			WHERE
				iu.RecordType='2'
				AND ps.WebSAMSRegNo IS NOT NULL
				AND ps.UserKey IS NOT NULL
				AND ps.IsSuspend = 0
				AND ug.group_id IN ({$GroupIDList})
				$ClassTaughtList
			ORDER BY 
				iu.ClassName
			";
	$ClassArray = $lpf->returnArray($sql);
*/
	if($lpf->IS_IPF_ADMIN() || $lpf->IS_ADMIN())
	{
		$taught_class_conds = "";
	}
	else
	{
		if($person_response == "CT")
		{
		  $ClassTaughtList = $lpf->GET_CLASS_TEACHER_CLASS();
		}
		else if($person_response == "ST")
		{
			$ClassTaughtList = $lpf->GET_SUBJECT_TEACHER_CLASS($teacher_subject_id);
		}
		else
		{
		  $ClassTaughtList = array_merge($lpf->GET_SUBJECT_TEACHER_CLASS(), $lpf->GET_CLASS_TEACHER_CLASS());
		}
		
  	$taught_class_conds = " AND (yc.ClassTitleB5 IN ('".implode("','", $ClassTaughtList)."') ";
  	$taught_class_conds .= "OR yc.ClassTitleEN IN ('".implode("','", $ClassTaughtList)."'))";
	}
	

  $sql =  "
            SELECT DISTINCT
              yc.ClassTitleEN AS ClassName,
              ".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN")." AS ClassName_Title
            FROM
              {$lpf->course_db}.user_group as ug
              INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT as ps ON ug.user_id = ps.CourseUserID
              INNER JOIN {$intranet_db}.INTRANET_USER as iu ON ps.UserID = iu.UserID
              INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu ON iu.UserID = ycu.UserID
              INNER JOIN {$intranet_db}.YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID
            WHERE
              iu.RecordType='2'
              AND ps.WebSAMSRegNo IS NOT NULL
              AND ps.UserKey IS NOT NULL
              AND ps.IsSuspend = 0
              AND ug.group_id IN ({$GroupIDList})
              AND yc.AcademicYearID = ".Get_Current_Academic_Year_ID()."
              {$taught_class_conds}
            ORDER BY
              yc.Sequence
          ";
  $ClassArray = $lpf->returnArray($sql);
	
	//debug_r($ClassArray);
}
else
{
	# get class selections
	$ClassArray  = $lpf->getActivatedClass("", $person_response, $teacher_subject_id);
	//$ClassArray  = "";
}

if($ClassArray)
{
	# Get selected USER ID and ClassName for selection
	if($ClassName=="")
	{
		$StudentID = $user_id;
		$TempStudentArr = $lpf->GET_STUDENT_DATA($StudentID);
		$ClassName = $TempStudentArr[0]["ClassName"];
	}
	
	$ClassSelect = getSelectByArrayTitle($ClassArray, "name='ClassName' onChange='this.form.ClassChanged.value=1;this.form.submit()'", "", $ClassName, 1);
	
	# get student selections
	$StudentArray = $lpf->returnStudentListData($ClassName);

	# Consider subject group if needed.
	if (!$lpf->IS_IPF_ADMIN() && !$lpf->IS_ADMIN()) {
		if($person_response == "ST")
		{
			$subject_teacher_classes = $lpf->GET_SUBJECT_TEACHER_CLASS($teacher_subject_id);
		}
		else
		{
			// Ignore the class teacher classes.
			$subject_teacher_classes = array_diff($lpf->GET_SUBJECT_TEACHER_CLASS(), $lpf->GET_CLASS_TEACHER_CLASS());
			$teacher_subject_id = "";
		}
		
		if (is_array($subject_teacher_classes) && in_array($ClassName, $subject_teacher_classes)) {
			$subject_teacher_students = $lpf->GET_SUBJECT_TEACHER_STUDENT($ck_intranet_user_id, $teacher_subject_id);
			$StudentArray_new = array();
			foreach ($StudentArray as $StudentArray_entry) {
				if (in_array($StudentArray_entry[0], $subject_teacher_students)) {
					$StudentArray_new[] = $StudentArray_entry;
				}
			}
			$StudentArray = $StudentArray_new;
		}
	}
	
	if($StudentID=="" || $ClassChanged==1)
		$StudentID = $StudentArray[0][0];
	$StudentSelect = getSelectByArrayTitle($StudentArray, "name='StudentID' onChange='document.form1.submit()'", "", $StudentID, 1);

}
$ParArr["ClassSelect"] = $ClassSelect;
$ParArr["StudentSelect"] = $StudentSelect;
# added end

# get handined user
if($lpf->IS_IPF_ADMIN())
{
	$class_taught_arr = "";
}
else if($person_response == "CT")
{
	$class_taught_arr = $lpf->GET_CLASS_TEACHER_CLASS();
}
else if($person_response == "ST")
{
	$class_taught_arr = $lpf->GET_SUBJECT_TEACHER_CLASS($teacher_subject_id);
}
else
{
	$class_taught_arr = $lpf->GET_CLASS_TEACHER_CLASS();
}
	
	
$HandinListUsersID = $lgs->getHandedinUser($phase_id, "", $class_taught_arr);

//debug_r($phase_id);
//debug_r($class_taught_arr);
//debug_r($HandinListUsersID);

for($i=0; $i<count($HandinListUsersID); $i++)
{
	$HandinListUsersDetail[$HandinListUsersID[$i]] = $lpf->GET_STUDENT_DATA($lpf->EC_USER_ID_TO_IP_USER_ID($HandinListUsersID[$i]));
	$HandinListUsersDetail[$HandinListUsersID[$i]] = $HandinListUsersDetail[$HandinListUsersID[$i]][0];

	if($ParClassName == "" && $ParUserID == $HandinListUsersID[$i])
		$DefaultClass = $HandinListUsersDetail[$HandinListUsersID[$i]]['ClassName'];
	else if($ParClassName != "")
		$DefaultClass = $ParClassName;

	if(!isset($HandinClass) || !in_multi_array($HandinListUsersDetail[$HandinListUsersID[$i]]['ClassName'], $HandinClass))
		$HandinClass[] = array($HandinListUsersDetail[$HandinListUsersID[$i]]['ClassName'],
						$HandinListUsersDetail[$HandinListUsersID[$i]]['ClassName']);
}
for($i=0; $i<count($HandinListUsersID); $i++)
{
	//debug_r($HandinListUsersDetail[$HandinListUsersID[$i]]['ClassName']);
	//debug_r($DefaultClass);
	if($HandinListUsersDetail[$HandinListUsersID[$i]]['ClassName'] == $DefaultClass)
		$HandinListUsers[] = array($HandinListUsersID[$i],
							"(".$HandinListUsersDetail[$HandinListUsersID[$i]]['ClassName']." - ".$HandinListUsersDetail[$HandinListUsersID[$i]]['ClassNumber'].") ".($intranet_session_language == "b5" ? $HandinListUsersDetail[$HandinListUsersID[$i]]['ChineseName'] : $HandinListUsersDetail[$HandinListUsersID[$i]]['EnglishName'])
							);
}

	if(is_array($class_taught_arr))
	{
		$TmpHandinClass = array();
		for($i = 0; $i < count($HandinClass); $i++)
		{
			$TmpClass = $HandinClass[$i][0];
			if(in_array($TmpClass, $class_taught_arr))
			$TmpHandinClass[] = array($HandinClass[$i][0], $HandinClass[$i][1]);
		}
	}
	
if($TmpHandinClass != "")
{
	$HandinClass = $TmpHandinClass;
}

//debug_r($HandinClass);
//debug_r($HandinListUsers);

if($HandinClass != "" && $HandinListUsers != "")
{
	$ClassSelection = getSelectByArrayTitle($HandinClass, "name='ParClassName' id='ParClassName' onChange='jCHANGE_STUDENT()'", "", $DefaultClass, true);
	$UserSelection = getSelectByArrayTitle($HandinListUsers, "name='ParUserID' id='ParUserID' onChange='jCHANGE_STUDENT()'", "", $ParUserID, true);
}

# added on 20090212 by Kit, get student photo
# Retrieve student info
$student_obj = $lpf->GET_STUDENT_OBJECT($StudentID);
	
# Set links for photo
if(strpos($student_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $student_obj['PhotoLink'] != "")
	$student_obj['PhotoLink'] = str_replace("<!--ImageStyle-->", "", $student_obj['PhotoLink']);
else if($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
else
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";


////////////////// Gen Content //////////////////

# get handin object
$handin_obj = $lgs->getStudentPhaseHandin($ParUserID, $phase_obj);
$relevant_phase_html = $lgs->getRelevantPhaseViewAnswer($phase_obj["answer"], $ParUserID);

$phase_obj["correctiondate"] = $handin_obj["correctiondate"];
$phase_obj["status"] = $handin_obj["status"];

if($DefaultClass == "") {
	if ($HandinClass[0][0]) {
		$ClassName = $HandinClass[0][0];
	}
}
else
	$ClassName = $DefaultClass;

$person_right = $lgs->getPhaseRight($ck_memberType, $phase_obj);

//if($person_right == "VIEW" && $ck_memberType == "T" && $person_response == "S")
//$person_right = "DO";

$ParArr["person_right"] = $person_right;
# preset parameter array
$ParArr["ParUserID"] = $ParUserID;
$ParArr["StudentID"] = $UserID;
$ParArr["Role"] = "TEACHER";
$ParArr["Mode"] = "view";
$ParArr["assignment_id"] = $phase_id;
$ParArr["parent_id"] = $parent_id;
$ParArr["CurrentPage"] = "PhaseView";
$ParArr["selectionActionPage"] = "view_student_info.php";
$ParArr["StudentPhoto"] = $student_obj['PhotoLink'];


$PhaseData = $lpf->GET_PHASE_INFO($ParArr);
$SchemeData = $lpf->GET_SCHOOL_BASED_SCHEME($ParArr);

$PhaseTitle = $PhaseData[0]["title"];
$SchemeTitle = $SchemeData[0]["title"];

$ParArr["SchemeTitle"] = $SchemeTitle;
$ParArr["PhaseTitle"] = $PhaseTitle;

$KIS_edit_role = true;
if($_SESSION["platform"]=="KIS"){
	if(!$_SESSION["SSV_USER_ACCESS"]["other-iPortfolio"] && (time() > strtotime($PhaseData[0]["deadline"]))){
		$KIS_edit_role = false;
	}
}

$ParArr["kis_edit_form"] = $KIS_edit_role;

# get all phases selection in the scheme, added on 20090213 by Kit
$ParArr["use_parent_id"] = true; // use parent id as assignment id,  to get phases in scheme for below function
$TempPhaseArr = $lpf->GET_SCHOOL_BASED_SCHEME_WITH_PHASE($ParArr);
$PhaseArray = array();
for($i=0; $i < sizeof($TempPhaseArr); $i++)
{
	$PhaseArray[] = array($TempPhaseArr[$i]["assignment_id"], $TempPhaseArr[$i]["title"]); 	
}
$PhaseSelect = getSelectByArrayTitle($PhaseArray, "name='PhaseID' onChange='document.form1.phase_id.value=this.value; document.form1.submit()'", "", $phase_id, 1);
###

$MenuArr = array();
$MenuArr[] = array($iPort["scheme_list"], "index.php");
$MenuArr[] = array($SchemeTitle, "");
if(sizeof($PhaseArray) > 1)
$MenuArr[] = array($PhaseSelect, "");
else
$MenuArr[] = array($PhaseTitle, "");
$MenuArr[] = array($UserArr[0]["firstname"]." (".$UserArr[0]["class_number"].")", "");
/////////////// END Gen Content ////////////////////


$linterface->LAYOUT_START();
?>

<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/layer.js"></script>

<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/online_form_edit.js"></script>
<!--
<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/online_form_view.js"></script>
-->

<form name="ansForm" method="post" action="revise_phase_update.php" onSubmit="return checkform(this)">
	<input type="hidden" name="qStr" value="<?=$phase_obj["answersheet"]?>">
<!--
	<input type="hidden" name="aStr" value="<?=preg_replace('(\r\n|\n)', "<br />", $handin_obj["answer"])?>">
-->
	<input type="hidden" name="aStr" value="<?=$handin_obj["answer"]?>">
	<input type="hidden" name="handin_id" value="<?=$handin_obj["handin_id"]?>">
	<input type="hidden" name="phase_id" value="<?=$phase_id?>">
	<input type="hidden" name="assignment_id" value="<?=$parent_id?>">
	<input type="hidden" name="answersheet" value="<?=$phase_obj["answersheet"]?>">
	<input type="hidden" name="fieldname" value="answersheet">
	<input type="hidden" name="formname" value="ansForm">
	<input type="hidden" name="user_id" value="<?=$user_id?>">
	<input type="hidden" name="CurParUserID" value="<?=$ParUserID?>">
	<input type="hidden" name="ReturnPage" value="view_student_info">
</form>

<SCRIPT LANGUAGE="Javascript">
function checkform(myObj)
{
	var isst = false;
	if (!check_text(myObj.title, "<?=$ec_warning['growth_phase_title']?>")) return false;
	if (typeof(myObj.starttime)!="undefined")
	{
		if (myObj.starttime.value!="")
		{
			if(!check_date(myObj.starttime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			{
				isst = true;
			}
		}
	}
	if (typeof(myObj.endtime)!="undefined")
	{
		if (myObj.endtime.value!="")
		{
			if(!check_date(myObj.endtime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			if (isst)
			{
				if(!compareTime(myObj.starttime, myObj.sh, myObj.sm, myObj.endtime, myObj.eh, myObj.em)) {
					myObj.starttime.focus();
					alert("<?= $w_alert['start_end_time2'] ?>");
					return false;
				}
			}
		}
	}
	
	/*
	checkOption(myObj.elements["relevant_phase[]"]);
	for(var i=0; i<myObj.elements["relevant_phase[]"].length; i++)
	{
		myObj.elements["relevant_phase[]"].options[i].selected = true;
	}
	*/
	
	return true;
}

function editOnlineForm()
{
	postInstantForm(document.form1, "online_form/edit.php", "post", 10, "ec_popup10");
}

function jSubmitForm()
{
	finish();

	<?php if($sys_custom['iPf']['chiuchunkg']['SBS']): ?>
	var isAllFill = true;
	var allInputName = [];
	$('#answersheet input[type="radio"]').each(function(){
		allInputName.push($(this).attr('name'));
	});
	$.each(allInputName, function(index, name){
		if($('#answersheet input[name="'+name+'"]:checked').length == 0){
			isAllFill = false;
		}
	});
	if(!isAllFill && !confirm('<?=$Lang['iPortfolio']['ChiuChunKG']['SBS']['someFieldNotAns'] ?>')){
		return false;
	}
	<?php endif; ?>
	
	var obj = document.ansForm;
	obj.submit();
}

function jClearForm() {
	$('form#answersheet input').each( function() {
		switch ($(this).get(0).tagName.toUpperCase()) {
			case 'INPUT':
				var objectType = $(this).attr('type').toUpperCase();
				switch (objectType) {
					case 'TEXT':
						$(this).val('');
						break;
					case 'RADIO':
					case 'CHECKBOX':
						$(this).attr('checked', '');
						break;
				}
				break;
			case 'TEXTAREA':
				$(this).val('');
				break;
			case 'SELECT':
				if ($(this).attr('multiple')) {
					$(this + ' option').attr('selected', '');
				}
				else {
					$(this).val('');
				}
				break;
		}
	});
}

function jCancelForm()
{
	self.location.href = "handin_view.php?phase_id=<?=$phase_id?>&parent_id=<?=$parent_id?>";
}

function jPrint(phase_id, assignment_id)
{
	//var url = "print_phase.php?ViewOnly=1&phase_id="+phase_id+"&assignment_id="+assignment_id+"&ParUserID=<?=$ParUserID?>";
	<?php if($sys_custom['iPf']['hartspreschool']['SBS']){ ?>
	var url = "../../../student/sbs/print_pdf_hartspreschool.php";
	<?php }else if($sys_custom['iPf']['chiuchunkg']['SBS']){ ?>
		var url = "../../../student/sbs/print_pdf_chiuchunkg.php";
	<?php }else{ ?>
		var url = "../../../student/sbs/print_phase_menu.php";
	<?php } ?>
	var param = "?phase_id="+phase_id+"&assignment_id="+assignment_id+"&user_id=<?=$ParUserID?>";

	url = url + param;
	newWindow(url, 28);
}

function jCHANGE_STUDENT()
{
	url = "view_student_info.php?ParUserID="+document.getElementById('ParUserID').value+"&phase_id=<?=$phase_id?>&parent_id=<?=$parent_id?>&ParClassName="+document.getElementById('ParClassName').value;
	self.location.href = url;
}

////////////////////////////////////////////////////////////////////////

// answer form
answer_sheet = "<span class=title><?=$ec_form_word['growth_scheme_form']?></span>";
answersheet_template = "<?=$ec_form_word['answersheet_template']?>";
answersheet_header = "<?=$ec_form_word['question_title']?>";
answersheet_type = "<?=$ec_form_word['fill_in_method']?>";
no_options_for = "<?=$ec_form_word['no_options_for']?>";
pls_specify_type = "<?=$ec_form_word['fill_in_type']?>";
pls_fill_in = "<?=$ec_form_word['fill_in_content']?>";
chg_title = "<?=$ec_form_word['change_heading']?>";
chg_template = "<?=$ec_form_word['confirm_to_template']?>";

answersheet_tf = "<?=$ec_form_word['answersheet_tf']?>";
answersheet_mc = "<?=$ec_form_word['answersheet_mc']?>";
answersheet_mo = "<?=$ec_form_word['answersheet_mo']?>";
answersheet_sq1 = "<?=$ec_form_word['answersheet_sq1']?>";
answersheet_sq2 = "<?=$ec_form_word['answersheet_sq2']?>";
answersheet_option = "<?=$answersheet_option?>";

answersheet_not_applicable = "<?=$ec_form_word['answersheet_not_applicable']?>";

/************************************************************/
answersheet_likert = "<?=$ec_form_word['answersheet_ls']?>";
answersheet_table = "<?=$ec_form_word['answersheet_tl']?>";
answersheet_question = "<?=$ec_form_word['answersheet_no_ques']?>";
no_questions_for = "<?=$ec_form_word['answersheet_no_select_ques']?>";
question_scale = "<?=$ec_form_word['question_scale']?>";
question_question = "<?=$ec_form_word['question_question']?>";
/************************************************************/

button_submit = " <?=$button_submit?> ";
button_add = " <?=$button_add?> ";
button_cancel = " <?=$button_cancel?> ";
button_update = " <?=$button_update?> ";

background_image = "";

var sheet = new Answersheet();

// attention: MUST replace '"' to '&quot;'
var tmpStr = document.ansForm.qStr.value;
var tmpStrA = document.ansForm.aStr.value;

//sheet.qString = tmpStr.replace(/\"/g, "&quot;");
sheet.qString = tmpStr.replace(/\&quot;/g, "\"");
sheet.aString = tmpStrA.replace(/\&quot;/g, "\"");
sheet.mode = 1;	// 0:edit 1:fill in application
sheet.displaymode = <?=$phase_obj["sheettype"]?>;	// 0:edit 1:fill in application
sheet.answer = sheet.sheetArr();

// temp
var form_templates = new Array();

sheet.templates = form_templates;


var myQue = "<?=preg_replace('(\r\n|\n)', '<br>', str_replace('&amp;', '&', $phase_obj['answersheet']))?>";
var msg_not_answered = "<i><font color=gray>&lt;<?=$ec_iPortfolio['form_not_answered']?>&gt;</font></i>";
var displaymode = <?=$phase_obj["sheettype"]?>;

var myAns = "<?=preg_replace('(\r\n|\n)', '<br>', str_replace('&amp;', '&', $handin_obj['answer']))?>";

<?php if($sys_custom['iPf']['chiuchunkg']['SBS']): ?>
$(function(){
	$('[name^="TB_"]').find('tr:first td:eq(1)').width(350)
});
<?php endif; ?>
</SCRIPT>

<?php
$ParArr["SheetData"] = "<script language='javascript'>document.write(editPanel());</script>";
//$ParArr["SheetData"] = "<script language='javascript'>document.write(viewForm(myQue, myAns));</script>"; 

$Content .= "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n";
$Content .= "<tr><td>\n";
$Content .= $lpf_ui->GET_NAVIGATION($MenuArr);
$Content .= "</td><td align='right'>\n";
$Content .= $ClassSelection." ".$UserSelection;
$Content .= "</td></tr>\n";
$Content .= "</table>";
$Content .= $lpf_ui->GET_PHASE_FORM($ParArr);
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><?= getSystemMessage($msg, $err) ?></td>
</tr>
</table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<!-- CONTENT HERE -->
<?=$Content?>
</td></tr></table>
</td></tr></table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>