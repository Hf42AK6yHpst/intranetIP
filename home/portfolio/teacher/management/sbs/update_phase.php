<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lpf = new libpf_sbs();

// check iportfolio admin user
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

$title = HTMLtoDB($title);
$instruction = HTMLtoDB($instruction);

if($sheet_format==0)
{
	$answersheet = HTMLtoDB($answersheet);
}
else
{
	$sheettype = 2;
	$answersheet = HTMLtoDB($contents);
}
$html_opt = 1;
$attachment = "";
$worktype = 7;
$starttime = (trim($starttime)!="") ? "'".$starttime." $sh:$sm:00'" : "NULL";
$endtime = (trim($endtime)!="") ? "'".$endtime." $eh.$em:59'" : "NULL";
$SubjectID = ($marking_scheme=="ST") ? $SubjectID : "";

// $marking_scheme = "S", "FT", "P", "ST"
if (is_array($relevant_phase))
{
	$answer = implode(",", $relevant_phase);
}

if ($phase_id=="")
{
	$msg = 1;
	
	$fieldname = "title, worktype, instruction, html_opt, status, attachment, answer, deadline, parent_id, marking_scheme, starttime, answersheet, sheettype, teacher_subject_id, answer_display_mode, ";
	$sql  = "INSERT INTO $lpf->course_db.assignment ($fieldname inputdate, modified) values (";
	$sql .= "'$title', $worktype, '$instruction', '$html_opt', '$status', '$attachment', '$answer', $endtime, '$assignment_id', '$marking_scheme', $starttime, '$answersheet', '$sheettype', '$SubjectID', '$answer_display_mode', ";
	$sql .= "now(), now())";
	
	$li->db_db_query($sql);
	$phase_id = $li->db_insert_id();
} 
else
{
	$msg = 2;
	$fieldname  = "title = '$title', ";
	$fieldname .= "instruction = '$instruction', ";
	$fieldname .= "html_opt = '$html_opt', ";
	$fieldname .= "status = '$status', ";
	$fieldname .= "starttime=$starttime, ";
	$fieldname .= "answersheet = '$answersheet', ";
	$fieldname .= "sheettype = '$sheettype', ";
	$fieldname .= "answer = '$answer', ";
	$fieldname .= "deadline=$endtime, ";
	$fieldname .= "marking_scheme='$marking_scheme', ";
	$fieldname .= "teacher_subject_id='$SubjectID', ";
	$fieldname .= "answer_display_mode='$answer_display_mode', ";
	$fieldname .= "modified = now() ";
	$sql = "UPDATE $lpf->course_db.assignment SET $fieldname WHERE assignment_id='$phase_id' AND worktype=$worktype ";
	
	$li->db_db_query($sql);
}

intranet_closedb();

if($NextAction == "NewPhase")
header("Location: new_phase.php?msg=$msg&assignment_id=$assignment_id");
else
header("Location: index.php?msg=$msg");
?>
