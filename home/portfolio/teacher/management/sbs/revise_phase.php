<?php

// Modifing by key

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");

iportfolio_auth("TA");
intranet_auth();
intranet_opendb();

$lpf = new libpf_sbs();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

$lgs = new growth_scheme();

$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_SchoolBasedScheme";
$CurrentPageName = $iPort['menu']['school_based_scheme'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

# get target group(s) of the scheme
$sql = "SELECT DISTINCT group_id FROM {$lpf->course_db}.grouping_function WHERE function_id = '{$assignment_id}' AND function_type = 'A'";
$GroupIDArray = $lpf->returnVector($sql);

if(!empty($GroupIDArray))
{
	# get target classes
	$GroupIDList = implode(",", $GroupIDArray);
	$ClassTaughtList = ($lpf->IS_IPF_ADMIN() || $lpf->IS_ADMIN()) ? "" : " AND iu.ClassName IN (".implode(",", array_merge($lpf->GET_SUBJECT_TEACHER_CLASS(), $lpf->GET_CLASS_TEACHER_CLASS())).")";
	$sql = "SELECT DISTINCT
				iu.ClassName AS ClassName,
				iu.ClassName As ClassName_Title
			FROM 
				user_group as ug
				LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT as ps ON ug.user_id = ps.CourseUserID
				LEFT JOIN {$intranet_db}.INTRANET_USER as iu ON ps.UserID = iu.UserID
			WHERE
				iu.RecordType='2'
				AND ps.WebSAMSRegNo IS NOT NULL
				AND ps.UserKey IS NOT NULL
				AND ps.IsSuspend = 0
				AND ug.group_id IN ({$GroupIDList})
				$ClassTaughtList
			ORDER BY 
				iu.ClassName
			";
	$ClassArray = $lpf->returnArray($sql);
}
else
{
	# get class selections
	$ClassArray  = $lpf->getActivatedClass();
}
if($ClassName=="")
	$ClassName = $ClassArray[0][0];

$ClassSelect = getSelectByArrayTitle($ClassArray, "name='ClassName' onChange='this.form.ClassChanged.value=1;this.form.submit()'", "", $ClassName, 1);

# get student selections
$StudentArray = $lpf->returnStudentListData($ClassName);
if($StudentID=="" || $ClassChanged==1)
	$StudentID = $StudentArray[0][0];
$StudentSelect = getSelectByArrayTitle($StudentArray, "name='StudentID' onChange='document.form1.submit()'", "", $StudentID, 1);

# get phase object
$phase_obj = $lgs->getPhaseInfo($phase_id);
$phase_obj = $phase_obj[0];

# get assignment object
$assignment_obj = $lgs->getSchemeInfo($assignment_id);
$assignment_obj = $assignment_obj[0];

# get handin object
$user_id = $lpf->getCourseUserID($StudentID);
$handin_obj = $lgs->getStudentPhaseHandin($user_id, $phase_obj);
$relevant_phase_html = $lgs->getRelevantPhaseViewAnswer($phase_obj["answer"], $user_id);
$relevant_phase_html = addslashes($relevant_phase_html);

$phase_obj["correctiondate"] = $handin_obj["correctiondate"];
$phase_obj["status"] = $handin_obj["status"];

# define photo showing 
$luser = new libuser($user_id, $ck_course_id);
$student_name_html = $luser->user_name();
$StudentPhoto = $lgs->getStudentPhoto($user_id);
$user_photo_js = str_replace('"', '\"', $StudentPhoto)."<br>".str_replace("'", "\'", $student_name_html);
$student_name_html = "";

# define the cancel action
$submit_confirm_alert = ($phase_obj["sheettype"]==2) ? $ec_warning['growth_phase_viewed'] : $ec_warning['form_handin'] ;

$phase_obj["answersheet"] = ($phase_obj["sheettype"]!=2) ? preg_replace('(\r\n|\n)', "<br>", $phase_obj["answersheet"]) : preg_replace('(\r\n|\n)', "", $phase_obj["answersheet"]);

?>
<script language="javascript">
function submitForm(){
	if (typeof(document.ansForm)!="undefined")
	{
		if (confirm("<?=$submit_confirm_alert?>"))
		{
			finish();
			document.ansForm.submit();
		}
	} else
	{
		alert("<?=$ec_warning['no_permission']?>");
	}
}
function jCancel()
{
	self.location.href = "handin_view.php?phase_id=<?=$phase_id?>&parent_id=<?=$assignment_id?>";
}

function jSubmit()
{
	submitForm();
}

function jPrint()
{
	newWindow("<?=$PATH_WRT_ROOT?>home/portfolio/student/sbs/print_phase_menu.php?admin_view=1&user_id=<?=$user_id?>&phase_id=<?=$phase_id?>&assignment_id=<?=$assignment_id?>",28);
}

function viewPhaseAnswer(phase_id, user_id)
{
	newWindow("<?=$PATH_WRT_ROOT?>home/portfolio/student/sbs/view_phase_popup.php?admin_view=1&user_id="+user_id+"&phase_id="+phase_id, 27);
}
</script>

<form name="form1" method="post" action="revise_phase.php">
<table width="100%" height="30"  border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td class="chi_content_15" align="right"><?=$ClassSelect?>&nbsp;<?=$StudentSelect?></td>
	</tr>
</table>

<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td colspan=2><?= getSystemMessage($msg, $err) ?></td>
</tr>
<?=$student_name_html?>
</table>

<input type="hidden" name="ClassChanged" value="0" />
<input type="hidden" name="phase_id" value="<?=$phase_id?>" />
<input type="hidden" name="assignment_id" value="<?=$assignment_id?>" />
</form>

<form name="ansForm" method="post" action="revise_phase_update.php">
	<input type="hidden" name="qStr" value="<?=$phase_obj["answersheet"]?>">
	<input type="hidden" name="aStr" value="<?=preg_replace('(\r\n|\n)', "<br>", $handin_obj["answer"])?>">
	<input type="hidden" name="phase_id" value="<?=$phase_id?>">
	<input type="hidden" name="user_id" value="<?=$user_id?>">
	<input type="hidden" name="assignment_id" value="<?=$assignment_id?>">
	<input type="hidden" name="handin_id" value="<?=$handin_obj["handin_id"]?>">
	<input type="hidden" name="StudentID" value="<?=$StudentID?>">
	<input type="hidden" name="ClassName" value="<?=$ClassName?>">
</form>

<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/layer.js"></script>
<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/online_form_edit.js"></script>

<table width="100%" height="30"  border="0" cellpadding="0" cellspacing="0">
<tr><td>


<script language="Javascript">
var form_title_main = "<?=$phase_obj['title']?>";
var form_description = "<?=preg_replace('(\r\n|\n)', '<br>', $phase_obj['instruction'])?>";
var form_reference = "<?=$relevant_phase_html?>";
var form_user_photo = "<?=$user_photo_js?>";

answer_sheet = "<span class=title><?=$ec_form_word['growth_scheme_form']?></span>";
answersheet_template = "<?=$ec_form_word['answersheet_template']?>";
answersheet_header = "<?=$ec_form_word['question_title']?>";
answersheet_type = "<?=$ec_form_word['fill_in_method']?>";
no_options_for = "<?=$ec_form_word['no_options_for']?>";
pls_specify_type = "<?=$ec_form_word['fill_in_type']?>";
pls_fill_in = "<?=$ec_form_word['fill_in_content']?>";
chg_title = "<?=$ec_form_word['change_heading']?>";
chg_template = "<?=$ec_form_word['confirm_to_template']?>";

answersheet_tf = "<?=$ec_form_word['answersheet_tf']?>";
answersheet_mc = "<?=$ec_form_word['answersheet_mc']?>";
answersheet_mo = "<?=$ec_form_word['answersheet_mo']?>";
answersheet_sq1 = "<?=$ec_form_word['answersheet_sq1']?>";
answersheet_sq2 = "<?=$ec_form_word['answersheet_sq2']?>";
answersheet_option = "<?=$answersheet_option?>";
answersheet_not_applicable = "<?=$ec_form_word['answersheet_not_applicable']?>";

/************************************************************/
answersheet_likert = "<?=$ec_form_word['answersheet_ls']?>";
answersheet_table = "<?=$ec_form_word['answersheet_tl']?>";
answersheet_question = "<?=$ec_form_word['answersheet_no_ques']?>";
no_questions_for = "<?=$ec_form_word['answersheet_no_select_ques']?>";
question_scale = "<?=$ec_form_word['question_scale']?>";
question_question = "<?=$ec_form_word['question_question']?>";
/************************************************************/

button_submit = " <?=$button_submit?> ";
button_add = " <?=$button_add?> ";
button_cancel = " <?=$button_cancel?> ";
button_update = " <?=$button_update?> ";

background_image = "";

// added by KELLY
submit_action = " onSubmit=\"return false\"";

var sheet = new Answersheet();
// attention: MUST replace '"' to '&quot;'
var tmpStr = document.ansForm.qStr.value;
var tmpStrA = document.ansForm.aStr.value;
//sheet.qString = tmpStr.replace(/\"/g, "&quot;");
//sheet.aString = tmpStrA.replace(/\"/g, "&quot;");
sheet.qString = tmpStr.replace(/\&quot;/g, "\"");
sheet.aString = tmpStrA.replace(/\&quot;/g, "\"");
sheet.mode = 1;	// 0:edit 1:fill in application
sheet.displaymode = "<?=$phase_obj["sheettype"]?>";
sheet.answer = sheet.sheetArr();

// temp
var form_templates = new Array();

sheet.templates = form_templates;
document.write(editPanel());
</script>

</td>
</tr>
</table>

<table width=100% border="0" cellpadding="0" cellspacing="5">
<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
</tr>
</table>

<table width=100% border="0" cellpadding="0" cellspacing="0">
<tr>
	<td align="center">
		<?=$linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "button", "jSubmit();", "btn_submit");?>
		<?=$linterface->GET_ACTION_BTN($iPort["btn"]["print"], "button", "jPrint();", "btn_print");?>
		<?=$linterface->GET_ACTION_BTN($iPort["btn"]["cancel"], "button", "jCancel();", "btn_cancel");?>
	</td>
</tr>
</table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
