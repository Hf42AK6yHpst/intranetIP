<?php

// Modifing by

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lpf = new libpf_sbs();

// check iportfolio admin user
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

// update status only
if($UpdateSatus != "")
{
	// resolve the assignment_id to array
	$tmp_assignment_id = array();
	$tmp_assignment_id[] = $assignment_id;
	$assignment_id = $tmp_assignment_id;

	$msg = 2; // update scheme
	$sql = "UPDATE $lpf->course_db.assignment SET status='".$UpdateSatus."' WHERE assignment_id IN (".implode(",", $assignment_id).")";
	$li->db_db_query($sql);

	header("Location: index.php?msg=$msg$page");
	intranet_closedb();
	exit;
}

// update from edit / new scheme
$title = HTMLtoDB($title);
$instruction = HTMLtoDB($instruction);
$html_opt = 1;
$attachment = "";
$worktype = 6;

if ($assignment_id=="")
{
	$msg = 1; // add scheme
	$fieldname = "title, worktype, instruction, html_opt, status, attachment, answer, deadline, form_id,";
	$sql  = "INSERT INTO $lpf->course_db.assignment ($fieldname inputdate, modified) values (";
	$sql .= "'$title', $worktype, '$instruction', '$html_opt', '$status', '$attachment', '$answer', '$deadline', '$formId',";
	$sql .= "now(), now())";
	$li->db_db_query($sql);
	$assignment_id = $li->db_insert_id();
}
else
{
	$msg = 2; // update scheme
	$fieldname  = "title = '$title', ";
	$fieldname .= "instruction = '$instruction', ";
	$fieldname .= "html_opt = '$html_opt', ";
	$fieldname .= "status = '$status', ";
	$fieldname .= "form_id = '$formId', ";
	//$fieldname .= "attachment = '$attachment', ";
	//$fieldname .= "answer = '$answer', ";
	//$fieldname .= "deadline = '$deadline', ";
	$fieldname .= "modified = now() ";
	$sql = "UPDATE $lpf->course_db.assignment SET $fieldname WHERE assignment_id = $assignment_id AND worktype=$worktype ";
	$li->db_db_query($sql);
}

// update user_group
$sql = "DELETE FROM $lpf->course_db.grouping_function WHERE function_type = 'A' AND function_id = $assignment_id";
$li->db_db_query($sql);

if ($assignment_id !="" && $assignment_id > 0)
{
	for ($i = 0; $i < sizeof($target); $i++)
	{
		$group_id = $target[$i];
		$sql = "INSERT INTO $lpf->course_db.grouping_function (group_id, function_id, function_type) VALUES ($group_id, $assignment_id, 'A')";

		$li->db_db_query($sql);
	}
}

intranet_closedb();

if($msg == 1)
	header("Location: new_phase_index.php?msg=$msg&assignment_id=$assignment_id");
	else
		header("Location: index.php?msg=$msg");
		?>
