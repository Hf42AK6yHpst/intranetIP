<?php
// using : 
################## Change Log [Start] #################
#
#	Date	:	2015-05-15	Omas
# 			Create this page
#
################## Change Log [End] ###################
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lexport = new libexporttext();
$libpf_slp = new libpf_slp();

$exportDataAry[] = array('5A','1','','中五學生自述','自小，我是一個對數字敏感，已喜歡研究有關數學和科學方面的知識。我在中五時曾擔任校內數學學會的副主席.......');
$exportDataAry[] = array('6B','2','167484','中六學生自述','我是一個具有領導才能、擅於溝通和有毅力的人。    　　在校內，我曾於中四被老師推薦擔任英文學會主席一職……'); 

$exportHeaderAry = $libpf_slp->getImportSelfAccountHeader();
$colProperty = $libpf_slp->getImportSelfAccountColumnProperty();
$export_header = $lexport->GET_EXPORT_HEADER_COLUMN($exportHeaderAry,$colProperty);
$export_content = $lexport->GET_EXPORT_TXT($exportDataAry, $export_header);

intranet_closedb();
$filename = "import_student_self_account_template.csv";
$lexport->EXPORT_FILE($filename, $export_content);
?>