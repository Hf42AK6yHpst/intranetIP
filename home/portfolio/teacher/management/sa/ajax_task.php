<?php
################## Change Log [Start] #################
#
#	Date	:	2015-05-15	Omas
# 			Create this page
#
################## Change Log [End] ###################
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();


if($task = 'validateSelfAccountImport'){
	//$webSAMSCheckingArr = array_filter(Get_Array_By_Key($data,'2'));
	
	### Include js libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
	include_once($PATH_WRT_ROOT.'includes/libuser.php');
	
	$limport = new libimporttext();
	$libuser = new libuser();
	$libpf_slp = new libpf_slp();
	
	$targetFilePath = standardizeFormPostValue($_GET['targetFilePath']);
	
	### Get Data from the csv file
	$columnTitleAry = $libpf_slp->getImportSelfAccountHeader();
	$columnPropertyAry = $libpf_slp->getImportSelfAccountColumnProperty();
	$csvData = $limport->GET_IMPORT_TXT_WITH_REFERENCE($targetFilePath, '', '', $columnTitleAry, $columnPropertyAry);
	$csvColName = array_shift($csvData);
	$numOfData = count($csvData);

	$errorCount = 0;
	$successCount = 0;
	$rowErrorRemarksAry = array();
	$insertTempDataAry = array();
	
	for ($i=0; $i<$numOfData; $i++) {
		
		$_className = trim($csvData[$i][0]); // Class Name
		$_classNumber = trim($csvData[$i][1]); // Class Number
		$_webSAMS = trim($csvData[$i][2]); // WebSAMS Number
		$_selfAccountTitle = $csvData[$i][3]; // Title
		$_selfAccountTitle = $libpf_slp->Get_Safe_Sql_Query(standardizeFormPostValue($_selfAccountTitle)); // Title
		$_selfAccount = $csvData[$i][4]; // Self Account
		$_selfAccount = $libpf_slp->Get_Safe_Sql_Query(standardizeFormPostValue($_selfAccount));
		
		# Validate student
		if($_webSAMS != ''){
			$_webSAMS = str_replace('#','',$_webSAMS);
			$_userId = $libuser->getUserIDByWebSamsRegNo('#'.$_webSAMS);
			
			if(empty($_userId)){
			$ErrorArr[$i] = 'WebSAMS';
			}
			else{
				$successCount++;
			}
		}
		else{
			$temp = $libuser->GET_USER_BY_CLASS_CLASSNO($_className, $_classNumber);
			$_userId = $temp[0]['UserID'];
			
			if(empty($_userId)){
			$ErrorArr[$i] = 'Classname';
			}
			else{
				$successCount++;
			}
		}
		
		### Insert Data in Temp Table
		$TempValueArr[] = '("'.$_SESSION['UserID'].'","'.$i.'","'.$_userId.'","'.$_selfAccountTitle.'","'.$_selfAccount.'", now())';
		
		### Update Processing Display
		Flush_Screen_Display(1);
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= '$("span#BlockUISpan", window.parent.document).html("'.($i + 1).'");';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}
	$errorCount = count($ErrorArr);
	
	if($errorCount == 0){
		$libpf_slp->insertSelfAccountImportTempData($TempValueArr);
	}
	
	### Display Record Error Table
	//$errorCountDisplay = ($errorCount ? "<font color=\"red\"> ": "") . $errorCount . ($errorCount ? "</font>" : "");
	
	$x = '';
	if($errorCount > 0) {
		// get table header
		$csvHeaderAry = $libpf_slp->getImportSelfAccountHeader();
		$columnTitleDisplayAry = Get_Lang_Selection($csvHeaderAry['Ch'], $csvHeaderAry['En']);
		$numOfColumn = count($columnTitleDisplayAry);
		
		$x .= '<table class="common_table_list_v30 view_table_list_v30">';
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th>'.$Lang['General']['ImportArr']['Row'].'</th>';
						for ($i=0; $i<$numOfColumn; $i++) {
							$x .= '<th>'.$columnTitleDisplayAry[$i].'</th>';
						}
					$x .= '<th>'.$Lang['General']['Remark'].'</th>';
				$x .= '</tr>';
			$x .= '</thead>';
			
			$x .= '<tbody>';
			foreach($ErrorArr as $i => $Error){
				$_className = $csvData[$i][0]; // Class Name
				$_classNumber = $csvData[$i][1]; // Class Number
				$_webSAMS = $csvData[$i][2]; // WebSAMS Number
				$_selfAccountTitle = $csvData[$i][3]; // Title
				$_selfAccountTitle = $libpf_slp->Get_Safe_Sql_Query(standardizeFormPostValue($_selfAccountTitle)); // Title
				$_selfAccount = $csvData[$i][4]; // Self Account
				$_selfAccount = $libpf_slp->Get_Safe_Sql_Query(standardizeFormPostValue($_selfAccount));
				if($Error =='Classname'){
					$css_1 = 'red';
					$css_2 = '';
				}
				else if($Error == 'WebSAMS'){
					$css_1 = '';
					$css_2 = 'red';
				}
				else{
					$css_1 = '';
					$css_2 = '';
				}
				
				$x .= '<tr>';
					$x .= '<td class="tabletext">'. ($i+3) .'</td>';
					$x .= '<td class="tabletext '.$css_1.'">'. $_className .'</td>';
					$x .= '<td class="tabletext '.$css_1.'">'. $_classNumber .'</td>';
					$x .= '<td class="tabletext '.$css_2.'">'. $_webSAMS .'</td>';
					$x .= '<td class="tabletext">'. $_selfAccountTitle .'</td>';
					$x .= '<td class="tabletext">'. $_selfAccount .'</td>';
					$x .= '<td class="tabletext">'.$Lang['SysMgr']['FormClassMapping']['StudentNotExist'].'</td>';
				$x .= '</tr>';
			}
			$x .= '</tbody>';
		$x .= '</table>';
		$htmlAry['errorTbl'] = $x;
	}
	
	$errorCountDisplay = ($errorCount > 0) ? "<font color=\"red\">".$errorCount."</font>" : $errorCount;
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$htmlAry['errorTbl'].'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$successCount.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$errorCountDisplay.'\';';
		
		if ($errorCount == 0) {
			$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton");';
			$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).attr("disabled","");';
		}
		
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}

intranet_closedb();
?>