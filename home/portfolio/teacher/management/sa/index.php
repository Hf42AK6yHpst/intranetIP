<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * 2015-05-15 Omas Add import button
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");

intranet_auth();
intranet_opendb();

/**********************************************************************
 * This page is mainly divided into THREE parts ==> INPUT, PROCESS AND OUTPUT.
 * Please put the code in corresponding parts for clearance.
 **********************************************************************/
/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~INPUT~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
###############################################
##	get POST data
//~ data for libdbtable settings
$field = $_POST["field"];
$order = isset($_POST["order"])?$_POST["order"]:1;
$pageNo = $_POST["pageNo"];
$page_size_change = $_POST["page_size_change"];	//	To refresh the page when change the $numPerPage, (without it cannot change the page size)
$numPerPage = !empty($_POST["numPerPage"])?$_POST["numPerPage"]:$page_size;	//	The parameter $page_size seems to be set in global.php, pls use $numPerPage to avoid modified the $page_size

//~ other parameters
$keyword = trim($_POST["keyword"]);	//	This is the string for searching
$YearClassID = $_POST["YearClassID"];
###############################################



/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~PROCESS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
if(strstr($ck_user_rights, ":manage:"))	//	iportfolio admin
{
	$IS_MANAGE = true;
} else {
	$IS_MANAGE = false;
}
###############################################
##	HTML - class_selection
$libportfolio_ui = new libportfolio_ui();
$libpf_slp = new libpf_slp();
if($IS_MANAGE)	//	iportfolio admin
{
//	$groupIds = $libpf_slp->getUserGroupsInSelfAccount();
} else {
	$groupIds = $libpf_slp->getUserGroupsInSelfAccount($UserID);
}
$userClassInfo = $libpf_slp->getClassInfoByGroupIds($groupIds);
$class_arr = $libpf_slp->refineUserClassInfo($userClassInfo);
$html["class_selection"] = $libportfolio_ui->GEN_CLASS_SELECTION_OPTGROUP($class_arr, "name='YearClassID' id='YearClass' onChange='reload();'",$YearClassID);
###############################################

$hiddenField_html='';
for($i=0,$i_MAX=count($groupIds);$i<$i_MAX;$i++)
{
     $hiddenField_html .= '<input type="hidden" id="groupIds[]" name="groupIds[]" value="'.$groupIds[$i].'" />';
}



###############################################
##	HTML - keyword
$html["keyword"] = stripslashes(trim($keyword));
###############################################


###############################################
##	Construct the display table
$libportfolio = new libportfolio();

//~ Table basic settings	--------------------------||
$libdbtable2007 = new libdbtable2007($field, $order, $pageNo);
$libdbtable2007->page_size = $numPerPage;	//	the constructor does not provide setting page size, which is the number of records display per page

//~ Table headers			--------------------------||
$columnCount=0;
$libdbtable2007->column_list .= "<th width='1'>#</th>\n";
$libdbtable2007->column_list .= "<th>".$libdbtable2007->column($columnCount++, $i_UserClassName)."</th>\n";	//	column($fieldIndex,$columnTitle)
$libdbtable2007->column_list .= "<th>".$libdbtable2007->column($columnCount++, $i_UserClassNumber)."</th>\n";
$libdbtable2007->column_list .= "<th>".$libdbtable2007->column($columnCount++, $i_UserName)."</th>\n";
$libdbtable2007->column_list .= "<th>".$libdbtable2007->column($columnCount++, $i_general_title)."</th>\n";
$libdbtable2007->column_list .= "<th>".$libdbtable2007->column($columnCount++, $Lang['General']['LastModified'])."</th>\n";
if ($libportfolio->isUseApprovalInSelfAccount()) {
	$libdbtable2007->column_list .= "<th>".$libdbtable2007->column($columnCount++, $ec_iPortfolio['approved_by'])."</th>\n";
	$libdbtable2007->column_list .= "<th>".$libdbtable2007->column($columnCount++, $ec_iPortfolio['process_date'])."</th>\n";
}
$libdbtable2007->column_list .= "<th width='1'>".$libdbtable2007->check("YearClassUserID[]")."</th>\n";

//~	Get submission period	--------------------------||
$student_ole_config_file = "$eclass_root/files/self_account_config.txt";
$filecontent = trim(get_file_content($student_ole_config_file));
list($starttime, $sh, $sm, $endtime, $eh, $em) = unserialize($filecontent);
$startTimestamp = strtotime($starttime." ".$sh.":".$sm.":"."00");
$endTimestamp = strtotime($endtime." ".$eh.":".$em.":"."00");
$currentTimestamp = time();

//~	Setting content			--------------------------||
$keyword = addslashes($keyword);
$currentAcademicYearId = Get_Current_Academic_Year_ID();
$userNameField = getNameFieldByLang("IU.");
$approvedByNameField = getNameFieldByLang("IU2.");
if (!empty($YearClassID)) {
	$cond .= " AND YC.YEARCLASSID = $YearClassID ";
}

$coursedb = classNamingDB($ck_course_id);
if (!empty($groupIds)) {
	$groupsCond = "AND ug.group_id IN (".implode(",",$groupIds).")";
	$groupsJoinCond = "
INNER JOIN {$coursedb}.user_group ug 
	ON uc.user_id = ug.user_id
INNER JOIN {$coursedb}.mgt_grouping_function mgf 
	ON ug.group_id = mgf.group_id AND INSTR(mgf.function_right, 'Profile:SelfAccount') > 0";
}

$checkBoxField = "CONCAT('<input type=\"checkbox\" name=\"YearClassUserID[]\" value=\"', YCU.YEARCLASSUSERID,'\">')";

if (!empty($starttime)) {
	$html["out_of_period"] = $iPort['period_start_end']." ".date("Y-m-d H:i:s",$startTimestamp);
}
if (!empty($endtime)) {
	$html["out_of_period"] .= " - ".date("Y-m-d H:i:s",$endTimestamp);
}
$sql = "SELECT distinct
	IU.CLASSNAME,
	IU.CLASSNUMBER,
	$userNameField,
	SAS.TITLE,
	IF (SAS.MODIFIEDDATE = ''||SAS.MODIFIEDDATE IS NULL, '--', SAS.MODIFIEDDATE) AS MODIFIEDDATE,";
if ($libportfolio->isUseApprovalInSelfAccount()) {
$sql .= "
	IF (SAS.APPROVEDBY = ''||SAS.APPROVEDBY IS NULL, '--', $approvedByNameField) AS APPROVEDBY,
	IF (SAS.APPROVEDDATE = ''||SAS.APPROVEDDATE IS NULL, '--', SAS.APPROVEDDATE) AS APPROVEDDATE,";
}
$sql .="
	$checkBoxField";
if ($libportfolio->isUseApprovalInSelfAccount()) {
$sql .= ",
	IF (SAS.RECORDSTATUS = 1, 'row_avaliable', '') AS trCustClass";
}

$sql .= "
FROM {$intranet_db}.INTRANET_USER IU
INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT PS ON IU.USERID = PS.USERID AND PS.ISSUSPEND = 0 
LEFT JOIN {$eclass_db}.SELF_ACCOUNT_STUDENT SAS
	ON SAS.USERID = IU.USERID AND SAS.DEFAULTSA = '".$ipf_cfg["DB_SELF_ACCOUNT_STUDENT_DefaultSA"]["SLP"]."'
LEFT JOIN {$intranet_db}.INTRANET_USER IU2
	ON SAS.APPROVEDBY = IU2.USERID AND IU2.RECORDTYPE = 1
INNER JOIN {$intranet_db}.YEAR_CLASS_USER YCU
	ON YCU.USERID = IU.USERID
INNER JOIN {$intranet_db}.YEAR_CLASS YC
	ON YCU.YEARCLASSID = YC.YEARCLASSID
INNER JOIN {$eclass_db}.user_course uc 
	ON uc.user_email = IU.USEREMAIL
$groupsJoinCond
WHERE YC.ACADEMICYEARID = $currentAcademicYearId
	AND (IU.RECORDTYPE=".TYPE_STUDENT." AND IU.RECORDSTATUS = ".STATUS_APPROVED.")
	$cond
	$groupsCond
	AND uc.course_id = $ck_course_id
	AND (IU.CLASSNAME LIKE '%$keyword%'
	OR IU.CLASSNUMBER LIKE '%$keyword%'
	OR $userNameField LIKE '%$keyword%'
	OR $approvedByNameField LIKE '%$keyword%'
	OR SAS.TITLE LIKE '%$keyword%'
	OR SAS.APPROVEDDATE LIKE '%$keyword%')
";
//hdebug_r(htmlspecialchars($sql));
//hdebug_r($sql);
$libdbtable2007->sql = $sql;

//~ Control Display			--------------------------||
$libdbtable2007->IsColOff = "IP25_table";	//	choosing layout
$libdbtable2007->field_array = array("IU.CLASSNAME","IU.CLASSNUMBER",$userNameField, "SAS.TITLE", "SAS.MODIFIEDDATE");	//	set the sql field being display
if ($libportfolio->isUseApprovalInSelfAccount()) {
	$libdbtable2007->field_array[] = "SAS.APPROVEDBY";
	$libdbtable2007->field_array[] = "SAS.APPROVEDDATE";
}
$libdbtable2007->no_col = 1/* row number */+count($libdbtable2007->field_array)+1/* checkbox */;	//	set the number of fields to be displayed, at least 2, becoz the 1st row is used to display the number of rows
$libdbtable2007->column_array = array(0,0,0,0,0);	//	use to control the text style
$libdbtable2007->fieldorder2 = ' , IU.CLASSNUMBER';

//~ Get the HTML			--------------------------||
$displayTableHtml = $libdbtable2007->display();
$table_hidden = <<<HTMLEND
<input type="hidden" name="pageNo" value="{$libdbtable2007->pageNo}">
<input type="hidden" name="order" value="{$libdbtable2007->order}">
<input type="hidden" name="field" value="{$libdbtable2007->field}">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="{$libdbtable2007->page_size}">
HTMLEND;
$displayTableHtml .= $table_hidden;
###############################################


###############################################
##	HTML - display_table (containing the hidden form fields for the table settings)
$html["display_table"] = $displayTableHtml;
###############################################

###############################################
##	HTML - legend_approval
if ($libportfolio->isUseApprovalInSelfAccount()) {
	$html["legend"] = "<div class='record_indication'><ul><li class='title_row_normal'><em>&nbsp;</em><span>{$ec_iPortfolio['pending']}</span></li><li class='title_row_avaliable'><em>&nbsp;</em><span>{$ec_iPortfolio['approved']}</span></li></ul><p class='spacer' /></div>";
}
###############################################

/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~OUTPUT~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
###############################################
##	Preparation before start a new page
$CurrentPage = "Teacher_SelfAccount";	//	set the current page in the $MODULE_OBJ
$MODULE_OBJ = $libportfolio->GET_MODULE_OBJ_ARR("Teacher");	//	Get the MODULE_OBJ in specific module (in this case get from libportfolio), which is used in $linterface to show the left menu

$CurrentPageName = $ec_iPortfolio['self_account'];	//	set the current page name  
$TAGS_OBJ[] = array($CurrentPageName,"");	//	Put the current page name to the $TAGS_OBJ, which is used in $linterface to show the page name
###############################################

###############################################
##	Create the page
$PAGE_NAVIGATION[] = array($CurrentPageName);
$linterface = new interface_html();	//	cannot use $interface_html as variable(i.e. $interface_html = new interface_html();), will cause error, reason: unknown
$h_ExportIcon = $linterface->Get_Content_Tool_v30('export', 'javascript:js_Go_Export();');
$h_ImportIcon = $linterface->Get_Content_Tool_v30('import', 'javascript:js_Go_Import();');

$html["navigation"] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
$linterface->LAYOUT_START();
include_once("templates/index.tmpl.php");	//	the content besides the new page layout
$linterface->LAYOUT_STOP();
###############################################

intranet_closedb();
?>
