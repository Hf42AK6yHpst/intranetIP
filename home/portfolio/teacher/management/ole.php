<?php
## Modifying By: 
####################################
##	Modification Log:
##	2017-01-06 Omas
##	- improve performance by adding $yearProgConds #F110154
##	2016-05-20 Omas
##	- fixed php 5.4 error
##	2016-03-15 Kenneth
##	- add import programme
##	2014-12-05 Bill
##	- hide some checkboxes if "batch manage self-added records only" in Group Settings is set
##	2010-02-25 Max	(201002181147)
##	- add filter for cnecc sunshine customize
####################################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-mgmt-group.php");

intranet_auth();
intranet_opendb();
//DEFINE VARIABLE
define ("IS_EDITABLE", 1);
define ("IS_NOT_EDITABLE", 0);

//GET PASSING VARIABLE
$IntExtType = ($IntExt == 1) ? "EXT" : "INT";

$action = $_SESSION["SESS_OLE_ACTION"];
$isReadOnly = ($action == $ipf_cfg["OLE_ACTION"]["view"]) ? 1 : 0 ;
$no_of_col = ($isReadOnly) ? 8 : 9;
define ("STANDARD_NO_OF_COLUMN", $no_of_col);

//init Class
$ldb = new libdb();
$lpf_mgmt_grp = new libpf_mgmt_group();
$LibPortfolio = new libpf_slp();
$lpf_ui = new libportfolio_ui();
$lay = new academic_year();
$linterface = new interface_html();

$approvalSettings  = $LibPortfolio->GET_OLR_APPROVAL_SETTING_DATA2($IntExtType);
if($approvalSettings["Elements"]["ClassTeacher"] == 1){
	$allowClassTeacherApprove = 1;	
}

//INIT variable
$mgmt_ele_arr = array();

# Commented: Ensure when using "EXT", group setting - "batch manage self-added records only" can be checked
//if($IntExtType == "INT"){
	$ec_uID = $LibPortfolio->IP_USER_ID_TO_EC_USER_ID($UserID);
	$group_ids = $lpf_mgmt_grp->GET_USER_IN_GROUP($ec_uID);
	
if($IntExtType == "INT"){
	// for the component group , it support with INT only
	//get USER's OLE component
	$group_id_arr = $group_ids;
	$mgmt_ele_arr = $lpf_mgmt_grp->getGroupComponent($group_id_arr);
}

$sqlDebug = array(); // for debug , store the sql
$sql = "CREATE TEMPORARY TABLE tempProgramList (ProgramID int(11),editable int , PRIMARY KEY (ProgramID))";

# Check if any group set - "batch manage self-added records only"
for($i=0; $i<count($group_ids); $i++){
	$lpf_mgmt_grp->setGroupID($group_ids[$i]);
	$lpf_mgmt_grp->setGroupProperty();
	$lpf_mgmt_grp->setGroupFunctionRight();
	
	# Get the fucntion settings
	$gf_arrTemp = $lpf_mgmt_grp->getGroupFunctionRight();
	
	# bmsa (Profile:DelSelfAccountOnly) => change to short form 
	# due to DB field "function_right varchar(255)" character size limitation
	$manageSelfAcc = in_array("bmsa", $gf_arrTemp);
	if($manageSelfAcc){
		break;
	}
}

//debug_r($sql);
//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
$ldb->db_db_query($sql);
$sqlDebug[] = $sql;
$sql = "";


//1) the teacher is a OLE ADMIN and
//2a) the teacher is big admin "strstr($ck_function_rights, ":manage:")" OR
//2b) the teacher in a group without ELE component restriction "sizeof($mgmt_ele_arr) == 0"
$isAdmin = $LibPortfolio->isOLEMasterAdmin($mgmt_ele_arr);
//debug_r($isAdmin);
//debug_r($action);
if($isAdmin || ($action == $ipf_cfg["OLE_ACTION"]["view"]))
{
//	echo "is admin<br/>";
	//if login is a admin , insert all record into the temp table with editable is true
	$sql = "insert ignore into tempProgramList(ProgramID , editable) select distinct ProgramID , ".IS_EDITABLE." from {$eclass_db}.OLE_PROGRAM where IntExt  = '{$IntExtType}'";

//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
	$ldb->db_db_query($sql);
//	debug_r(" admin == >".$sql);
	$sqlDebug[] = $sql;	
	$sql = "";
}else
{
	if($allowClassTeacherApprove == 1){
		$sql = "";
		//insert all the record is his / her class student , 
		// if the program is created by a student , it is editable
		// if the program is created by a teacher , it cannot be edited
		$sql = "INSERT IGNORE INTO tempProgramList (ProgramID , editable) ";
		$sql .= "SELECT DISTINCT op.ProgramID , if(op.ComeFrom = ".$ipf_cfg["OLE_STUDENT_COMEFROM"]["studentInput"]." ,".IS_EDITABLE." ,".IS_NOT_EDITABLE.") as 'editValue' ";
		$sql .= "FROM {$eclass_db}.OLE_PROGRAM op ";
		$sql .= "INNER JOIN {$eclass_db}.OLE_STUDENT os ON op.ProgramID = os.ProgramID ";
		$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON os.UserID = ycu.UserID ";
		$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_TEACHER yct ON ycu.YearClassID = yct.YearClassID ";
		$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON yc.YearClassID = yct.YearClassID AND yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." ";
		$sql .= "WHERE yct.UserID = {$UserID} ";
		$sql .= "ON DUPLICATE KEY UPDATE editable = values(editable)";
//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");

		$ldb->db_db_query($sql);
		$sqlDebug[] = $sql;
//		debug_r(" class teacher ===>".$sql);
	}
	$sql = "";
	
	//insert all the record is request approve the teacher , including program is created by teacher or student, editable is depend on t.ComeFrom
	// if the program is created by a student , it is editable
	// if the program is created by a teacher , it cannot be edited
	$sql = "INSERT IGNORE INTO tempProgramList (ProgramID , editable) 
				SELECT DISTINCT t.ProgramID , if(t.ComeFrom = ".$ipf_cfg["OLE_STUDENT_COMEFROM"]["studentInput"]." ,".IS_EDITABLE." ,".IS_NOT_EDITABLE.") as 'editValue'
					from {$eclass_db}.OLE_PROGRAM as t 
					inner join {$eclass_db}.OLE_STUDENT as s on s.programid = t.programid 
					where s.RequestApprovedBy = {$UserID}  or s.ApprovedBy = {$UserID}
				 	ON DUPLICATE KEY UPDATE editable = values(editable)
					";

//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
	$ldb->db_db_query($sql);
	$sqlDebug[] = $sql;
//	debug_r(" request approve by self ===>".$sql);
	
	//insert all the record for the program is created by the teacher , editable is true
	$sql = "insert ignore into tempProgramList (ProgramID , editable) SELECT DISTINCT ProgramID , ".IS_EDITABLE." FROM {$eclass_db}.OLE_PROGRAM WHERE CreatorID = '{$UserID}'
					ON DUPLICATE KEY UPDATE editable = ".IS_EDITABLE."
				 ";	
//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
//	debug_r(" create by self ===>".$sql);

	$ldb->db_db_query($sql);		
	$sqlDebug[] = $sql;
//debug_r($group_id_arr);
//debug_r($mgmt_ele_arr);
	//insert all the record for the group component program for the teacher , editable is true
	if(!empty($group_id_arr))
	{
		$conds = "";
		for($i=0, $i_max=count($mgmt_ele_arr); $i<$i_max; $i++)
		{
			$mgmt_ele = $mgmt_ele_arr[$i];
		
			$conds .= "OR INSTR(op.ELE, '{$mgmt_ele}') > 0 ";
		}
		
		$sql = "INSERT IGNORE INTO tempProgramList (ProgramID,editable) ";
		$sql .= "SELECT DISTINCT op.ProgramID , ".IS_EDITABLE." FROM {$eclass_db}.OLE_PROGRAM op ";
		$sql .= "WHERE 0 {$conds}";
		$sql .= "ON DUPLICATE KEY UPDATE editable = values(editable)";

//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
//debug_r(" component ===>".$sql);	
		$ldb->db_db_query($sql);	
		$sqlDebug[] = $sql;
	}

}

//$sql = "select * from tempProgramList ";
//$result = $ldb->returnArray($sql);
//debug_r($result);
//$ldb = new libdb();
//$LibPortfolio = new libpf_slp();

//$lpf_mgmt_grp = new libpf_mgmt_group();
//$ec_uID = $LibPortfolio->IP_USER_ID_TO_EC_USER_ID($UserID);

// template for teacher page
##########################################
# set the current page title
##########################################
$CurrentPage = ($isReadOnly) ? "Teacher_OLEView" : "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];
if ($page_size_change == 1)
{
     setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
     $ck_page_size = $numPerPage;
}

########################################################
# Tab Menu : Start
########################################################

$TabIndex = empty($IntExt) ? IPF_CFG_SLP_MGMT_OLE : IPF_CFG_SLP_MGMT_OUTSIDE;

$TabMenuArr = libpf_tabmenu::getSlpMgmtTabs($TabIndex, 0);
### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags($TabIndex);
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

########################################################
# Operations : Start
########################################################
# tab menu
// hide tab if SIS
if(!$sys_custom['IPF_HIDE_COLUMN']['SIS'] ) {
	$tab_menu_html = $lpf_ui->GET_TAB_MENU($TabMenuArr);
}

# academic year selection

$academic_year_arr = $lay->Get_All_Year_List();
$academicYearID = (isset($academicYearID))? $academicYearID : Get_Current_Academic_Year_ID();
$ay_selection_html = getSelectByArray($academic_year_arr, "name='academicYearID' onChange='jSUBMIT_FORM()'", $academicYearID, 1, 0, $i_Attendance_AllYear, 2);

# component selection
$ele_selection_html = ($IntExt == 1) ? "" : $LibPortfolio->GET_ELE_SELECTION("name='ELE' onChange='jSUBMIT_FORM()'", $ELE, 1);

# cateogry selection
$category_selection_html = $LibPortfolio->GET_OLE_CATEGORY_SELECTION("name='category' onChange='jSUBMIT_FORM()'", $category, 1);

# subcategory selection
$subcategory_selection_html = $LibPortfolio->GET_SUBCATEGORY_SELECTION("name='subcategory' onChange='document.form1.submit()'", $subcategory, 1, $category);

# record type selection
$RecordArray[] = array("all", $ec_iPortfolio['all_programmes']);
if(strstr($ck_function_rights, "Profile:OLR"))
{
	$RecordArray[] = array("my", $ec_iPortfolio['my_programmes']);
	
	if($sys_custom['cnecc_SS'])
	{
		include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cnecc/libpf-slp-cnecc.php");
		$RecordArray[] = array("sunshine", $Lang["Cust_Cnecc"]["SunshineProgram"]);		
	}
}
$RecordArray[] = array("teacher", $ec_iPortfolio['SLP']['TeacherCreatedProgram']);
$RecordArray[] = array("student", $ec_iPortfolio['SLP']['StudentCreatedProgram']);
$record_own = (!isset($record_own)) ? $RecordArray[0][0] : $record_own;
$record_selection_html = getSelectByArray($RecordArray, "name='record_own' onChange='jSUBMIT_FORM()'", $record_own, 0, 1, "", 2);

# title search
$searching_html = "<div class=\"Conntent_search\"><input name=\"search_text\" id=\"search_text\" type=\"text\" value=\"{$search_text}\" /></div>";

// Hide import function for SIS
//if(!$isReadOnly && strstr($ck_function_rights, "Profile:OLR")){
if(!$isReadOnly && strstr($ck_function_rights, "Profile:OLR")&& !($sys_custom['IPF_HIDE_COLUMN']['SIS'])){
	# import button
//	$import_btn_html = strstr($ck_function_rights, "ImportData") ? "<td nowrap><a href=\"ole_import.php?IntExt={$IntExt}&FromPage=pview\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_import.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$button_import}</a></td>" : "";
//	if($IntExt==0){
//		$import_btn_html .= strstr($ck_function_rights, "ImportData") ? "<td nowrap style=\"width:150px;\"><a href=\"ole_import_programme.php?IntExt={$IntExt}&FromPage=pview\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_import.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\">".$iPort['menu']['Import']['Programme']."</a></td>" : "";
//	}
/*
	$import_btn_html .= strstr($ck_function_rights, "ImportData") ? "<td nowrap><a href=\"import/\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_import.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$button_import} 2</a></td>" : "";
*/
	// with sub-button
	$subBtnAry = array();

	$subBtnAry[] = array('ole_import_programme.php?IntExt='.$IntExt.'&FromPage=pview"', $iPort['menu']['Import']['Programme']);
	
	$subBtnAry[] = array('ole_import.php?IntExt='.$IntExt.'&FromPage=pview"', $iPort['menu']['Import']['ProgrammeWithStudent']);
	$btnAry[] = array('import', 'javascript: void(0);', '', $subBtnAry);

	$import_btn_html = strstr($ck_function_rights, "ImportData") ? ''.$linterface->Get_Content_Tool_By_Array_v30($btnAry).'':'';
	if($sys_custom['iPf']['OEA']['lasalle_preMapOEA']){
		if($LibPortfolio->IS_IPF_SUPERADMIN()){
            $import_btn_html .= '<div class="Conntent_tool"><a href="import/?mod=import&task=lasalle&r_intext='.$ipf_cfg["OLE_TYPE_INT"]["INT"].'" class="import">'.$Lang['Btn']['Import'].' (OEA Trial)</a></div>';
		}
	}
	
	
}

if($isReadOnly && strstr($ck_function_rights, "ExportData")){
	# export button
    $export_btn_html = '<div class="Conntent_tool"><a href="javascript:doExport(\'ole\')" class="export">'.$Lang['Btn']['Export'].'</a></div>';
}

if(!$isReadOnly && strstr($ck_function_rights, "Profile:OLR")){
	# new button
    $new_btn_html = '<div class="Conntent_tool"><a href="ole_new.php?IntExt='.$IntExt.'" class="new">'.$Lang['Btn']['New'].'</a></div>';
}
# LaSalle customarization: comment input button
//$comment_html = ($LibPortfolio->GET_CUSTOMARIZE_SCHOOL_SLP() == "LaSalle") ? "<td nowrap><a href=\"ole_comment.php\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_comment.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$ec_iPortfolio['ole_advice']}</a></td>" : "";
########################################################
# Operations : End
########################################################

########################################################
# Operation result : Start
########################################################
$op_result = ($msg == "") ? "" : "<tr><td align=\"right\">".$linterface->GET_SYS_MSG($msg)."</td></tr>";
########################################################
# Operation result : End
########################################################

########################################################
# Table content : Start
########################################################
$ELEArray = $LibPortfolio->GET_ELE();

//IF show EXT program , ELE count should be zero
$ELECount = ($IntExtType == "EXT") ? 0 : count($ELEArray);

$pageSizeChangeEnabled = true;
$checkmaster = true;

if (isset($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) && $sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE'])
{
	$IsSAS_selection_html = "SAS";
	$IsSASArr[] = array("", $Lang['iPortfolio']['OLE']['SAS_ALL']);
	$IsSASArr[] = array("0", $Lang['iPortfolio']['OLE']['SAS_NOT']);
	$IsSASArr[] = array("1", $Lang['iPortfolio']['OLE']['SAS']);
	$IsSAS_selection_html = $linterface->GET_SELECTION_BOX($IsSASArr, "onChange='this.form.submit()' name='IsSAS'", "", $IsSAS);
	
	$InOutsideSchoolArr[] = array("", $Lang['iPortfolio']['OLE']['In_Outside_School']);
	$InOutsideSchoolArr[] = array("0", $Lang['iPortfolio']['OLE']['Inside_School']);
	$InOutsideSchoolArr[] = array("1", $Lang['iPortfolio']['OLE']['Outside_School']);
	$InsideOutSide_selection_html = $linterface->GET_SELECTION_BOX($InOutsideSchoolArr, "onChange='this.form.submit()' name='IsOutsideSchool'", "", $IsOutsideSchool);
}

/******************** Handle for programme approver [Start] ********************/
// JS Array for storing editable programmes
$editable_program_arr = $ldb->returnVector("SELECT ProgramID FROM tempProgramList where editable = 1");
$html_js_editable_programme = "[".implode(", ", $editable_program_arr)."];\n";
/********************************************************************************
 * Temporary table for storing accessible records of teachers [End]
 ********************************************************************************/ 

/********************************************************************************
 * Temporary table for storing records counts of programmes [Start]
 ********************************************************************************/ 

// for improving performance F110154 
if($academicYearID != ''){
	$sql = "SELECT ProgramID FROM {$eclass_db}.OLE_PROGRAM where AcademicYearID = '$academicYearID'";
	$targetYearProgramIDArr = $ldb->returnVector($sql);
	$yearProgConds = " AND os.ProgramID IN ('".implode("','", $targetYearProgramIDArr)."') ";
}else{
	$yearProgConds = "";
}

// Temp table for record count
$sql = "CREATE TEMPORARY TABLE tempRecCount (ProgramID int(11), allCount int(8), approveCount int(8), pendingCount int(8), PRIMARY KEY (ProgramID))";
//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");

$ldb->db_db_query($sql);
$sqlDebug[] = $sql;
// for improving performance F110154
//$sql = "INSERT INTO tempRecCount (ProgramID, allCount) SELECT os.ProgramID, count(*) FROM {$eclass_db}.OLE_STUDENT os INNER JOIN {$intranet_db}.INTRANET_USER iu ON os.UserID = iu.UserID WHERE os.IntExt = '".($IntExt==1?"EXT":"INT")."' GROUP BY os.ProgramID";
$sql = "INSERT INTO tempRecCount (ProgramID, allCount) SELECT os.ProgramID, count(*) FROM {$eclass_db}.OLE_STUDENT os INNER JOIN {$intranet_db}.INTRANET_USER iu ON os.UserID = iu.UserID WHERE os.IntExt = '".($IntExt==1?"EXT":"INT")."' $yearProgConds GROUP BY os.ProgramID";

//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
$ldb->db_db_query($sql);
$sqlDebug[] = $sql;
// for improving performance F110154
//$sql = "INSERT INTO tempRecCount (ProgramID, approveCount) SELECT os.ProgramID, count(*) as cnt FROM {$eclass_db}.OLE_STUDENT os INNER JOIN {$intranet_db}.INTRANET_USER iu ON os.UserID = iu.UserID WHERE os.IntExt = '".($IntExt==1?"EXT":"INT")."' AND os.RecordStatus IN (2,4) GROUP BY os.ProgramID ON DUPLICATE KEY UPDATE approveCount = VALUES(approveCount)";
$sql = "INSERT INTO tempRecCount (ProgramID, approveCount) SELECT os.ProgramID, count(*) as cnt FROM {$eclass_db}.OLE_STUDENT os INNER JOIN {$intranet_db}.INTRANET_USER iu ON os.UserID = iu.UserID WHERE os.IntExt = '".($IntExt==1?"EXT":"INT")."' AND os.RecordStatus IN (2,4)  $yearProgConds  GROUP BY os.ProgramID ON DUPLICATE KEY UPDATE approveCount = VALUES(approveCount)";

//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
$ldb->db_db_query($sql);
$sqlDebug[] = $sql;
// for improving performance F110154
//$sql = "INSERT INTO tempRecCount (ProgramID, pendingCount) SELECT os.ProgramID, count(*) as cnt FROM {$eclass_db}.OLE_STUDENT os INNER JOIN {$intranet_db}.INTRANET_USER iu ON os.UserID = iu.UserID WHERE os.IntExt = '".($IntExt==1?"EXT":"INT")."' AND os.RecordStatus = 1 GROUP BY os.ProgramID ON DUPLICATE KEY UPDATE pendingCount = VALUES(pendingCount)";
$sql = "INSERT INTO tempRecCount (ProgramID, pendingCount) SELECT os.ProgramID, count(*) as cnt FROM {$eclass_db}.OLE_STUDENT os INNER JOIN {$intranet_db}.INTRANET_USER iu ON os.UserID = iu.UserID WHERE os.IntExt = '".($IntExt==1?"EXT":"INT")."' AND os.RecordStatus = 1 $yearProgConds GROUP BY os.ProgramID ON DUPLICATE KEY UPDATE pendingCount = VALUES(pendingCount)";

//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
$ldb->db_db_query($sql);
$sqlDebug[] = $sql;
/********************************************************************************
 * Temporary table for storing records counts of programmes [End]
 ********************************************************************************/
 
/********************************************************************************
 * Temporary table for storing programme details [Start]
 ********************************************************************************/  

// Temp table for programme detail
$sql = "CREATE TEMPORARY TABLE tempProgramDetail (ProgramID int(11), Creator varchar(255), CreatePersonID int(8), Modifier varchar(255), AYearID int(8), AYear varchar(255), AYearTerm varchar(255), PRIMARY KEY (ProgramID)) DEFAULT CHARSET=utf8";

//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
$ldb->db_db_query($sql);
$sqlDebug[] = $sql;
$sql = "INSERT INTO tempProgramDetail (ProgramID, Creator, CreatePersonID) SELECT op.ProgramID, IFNULL(".getNameFieldByLang("iu.").", '--'), iu.UserID FROM {$eclass_db}.OLE_PROGRAM op LEFT JOIN {$intranet_db}.INTRANET_USER iu ON op.CreatorID = iu.UserID";

//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
$ldb->db_db_query($sql);
$sqlDebug[] = $sql;
$sql = "INSERT INTO tempProgramDetail (ProgramID, Modifier) SELECT op.ProgramID, ".getNameFieldByLang("iu.")." FROM {$eclass_db}.OLE_PROGRAM op LEFT JOIN {$intranet_db}.INTRANET_USER iu ON op.ModifyBy = iu.UserID ON DUPLICATE KEY UPDATE Modifier = VALUES(Modifier)";

//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
$ldb->db_db_query($sql);
$sqlDebug[] = $sql;
$sql = "INSERT INTO tempProgramDetail (ProgramID, AYearID, AYear, AYearTerm) ";
$sql .= "SELECT op.ProgramID, op.AcademicYearID, ".Get_Lang_Selection("ay.YearNameB5","ay.YearNameEN")." AS AYear, ".Get_Lang_Selection("ayt.YearTermNameB5", "ayt.YearTermNameEN")." AS AYearTerm FROM {$eclass_db}.OLE_PROGRAM op ";
$sql .= "LEFT JOIN {$intranet_db}.ACADEMIC_YEAR AS ay ON op.AcademicYearID = ay.AcademicYearID ";
$sql .= "LEFT JOIN {$intranet_db}.ACADEMIC_YEAR_TERM AS ayt ON op.YearTermID = ayt.YearTermID ";
$sql .= "ON DUPLICATE KEY UPDATE AYearID = VALUES(AYearID), AYear = VALUES(AYear), AYearTerm = VALUES(AYearTerm)";

//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
$ldb->db_db_query($sql);
$sqlDebug[] = $sql;
/********************************************************************************
 * Temporary table for storing programme details [End]
 ********************************************************************************/
# CNECC extra
if(strstr($ck_function_rights, "Profile:OLR") && $sys_custom['cnecc_SS'] && $record_own == "sunshine")
{
  $cnecc_sql_join = " INNER JOIN {$eclass_db}.CUSTOM_CNECC_SS_EVENT CCSE ON CCSE.PROGRAMID = op.PROGRAMID ";
  $cnecc_sql_group = " GROUP BY CCSE.PROGRAMID ";
}

# Filter conditions
$cond = empty($academicYearID) ? "" : " AND t_pd.AYearID = '".$academicYearID."'";
$cond .= empty($ELE) ? "" : " AND INSTR(op.ELE, '{$ELE}') > 0";
$cond .= empty($category) ? "" : " AND op.Category = '".$category."'";
$cond .= empty($subcategory) ? "":" and op.SubCategoryID = '".$subcategory."'";
$cond .= empty($search_text) ? "" : " AND op.Title like '%".$search_text."%'";

switch ($record_own)
{
	case "teacher": # Select of Teacher Created Programmes
		$cond .= " AND op.ProgramType = 'T' ";
		break;
	case "student": # Select of Student Created Programmes
		$cond .= " AND op.ProgramType = 'S' ";
		break;

	case "my": # Select of My Programmes
		$_tmpSQL = "SELECT DISTINCT ProgramID FROM {$eclass_db}.OLE_STUDENT WHERE (ApprovedBy = '{$UserID}' or RequestApprovedBy = '{$UserID}')";

		$_tmpResult = $ldb->returnVector($_tmpSQL);
		$_programIDStr = "";
		if(sizeof($_tmpResult)  > 0 && is_array($_tmpResult)){
			$_programIDStr = implode(",",$_tmpResult);
		}
		$cond .= " AND (op.CreatorID = {$UserID} ";
		if($_programIDStr != ""){
			$cond .= " OR op.ProgramID IN ({$_programIDStr}) ";
		}
		$cond .= ")";
		$_programIDStr = "";
		break;

	default:
		break;
}
#########################
# Main query
#########################
if ($order=="") $order=0;
if ($field=="") $field=3;
$LibTable = new libpf_dbtable($field, $order, $pageNo);
$sql =  "SELECT ";
$sql .= "CONCAT('<a ";
$sql .= "class=\"tablelink\" ";
$sql .= "href=\"ole_event_studentview.php?IntExt={$IntExt}&EventID=', ";
$sql .= "op.ProgramID, ";
$sql .= "'&Year={$Year}&FromPage=program\" title=\"', ";

//////////	Creator
$sql .= "CONCAT('".$ec_iPortfolio['SLP']['CreatedBy'].": ', t_pd.Creator), ";
//////////	Last Update
$sql .= "'&#10;', ";	// Line Break
$sql .= "CONCAT('{$iPort["last_updated"]}: ', DATE_FORMAT(op.ModifiedDate, '%Y-%m-%d'), IF(t_pd.Modifier IS NULL, '', CONCAT(' (', t_pd.Modifier, ')'))), ";

$sql .= "'\">', op.Title, '</a>'), ";
$sql .= "
            IF(t_pd.AYear IS NULL, '--', CONCAT(t_pd.AYear, IF(t_pd.AYearTerm IS NULL, '', CONCAT('<br />', t_pd.AYearTerm)))),
            ".Get_Lang_Selection("oc.ChiTitle","oc.EngTitle").",
        ";
if ($IntExt != 1)
{
  foreach($ELEArray as $ELECode => $ELETitle)
	{
    $sql .= "IF(INSTR(op.ELE, '".$ELECode."') > 0, '<img onload=\"this.parentNode.title=\'".$ELETitle."\'\" src=\"/images/2009a/icon_tick_green.gif\" />', '<img onload=\"this.parentNode.title=\'".$ELETitle."\'\" src=\"/images/2009a/icon_tick_green.gif\" style=\"display:none\"/>'),";
	}
}

//$testCond = "  and title like 'fai_%'";

if(!$manageSelfAcc){
	$sql_checkBox = ($isReadOnly) ? "" : ", CONCAT('<input onClick=\"document.form1.checkmaster.checked=false\" type=\"checkbox\" name=\"record_id[]\" value=', op.ProgramID ,'>',if(t_pl.editable = 0,'<span class=\"tabletextrequire\">#</span>','')) as 'checkbox'";
} 
# Hide checkbox if group set - "batch manage self-added records only"
else {
	$sql_checkBox = ($isReadOnly) ? "" : ", IF(t_pd.CreatePersonID = '".$ck_intranet_user_id."', CONCAT('<input onClick=\"document.form1.checkmaster.checked=false\" type=\"checkbox\" name=\"record_id[]\" value=\"', op.ProgramID ,'\">', if(t_pl.editable = '0','<span class=\"tabletextrequire\">#</span>','') ), '') as 'checkbox'";
}

$h_checkAllBox = ($isReadOnly) ? "" : "<td rowspan=\"2\" >".$LibTable->check("record_id[]")."</td>\n";

/*
if($action == $ipf_cfg["OLE_ACTION"]["view"]){
	//if action is view , reset all the checkbox to empty, either in the SQL and HTML
	$sql_checkBox = " \"&nbsp;\" as 'checkbox' ";
	$h_checkAllBox = "&nbsp;";
}
*/

if ($IsSAS=="")
{
	# for all records
} elseif ($IsSAS=="1")
{
	$cond .= " AND op.IsSAS='1' ";
} else
{
	$cond .= " AND op.IsSAS='0' ";
}


if ($IsOutsideSchool=="")
{
	# for all records
} elseif ($IsOutsideSchool=="1")
{
	$cond .= " AND op.IsOutsideSchool='1' ";
} else
{
	$cond .= " AND op.IsOutsideSchool='0' ";
}

$sql .= "
            IF(DATE_FORMAT(op.EndDate, '%Y-%m-%d') = '0000-00-00', IF(DATE_FORMAT(op.StartDate, '%Y-%m-%d') = '0000-00-00', '--', DATE_FORMAT(op.StartDate, '%Y-%m-%d')), CONCAT(DATE_FORMAT(op.StartDate, '%Y-%m-%d'), ' {$profiles_to} ', DATE_FORMAT(op.EndDate, '%Y-%m-%d'))) AS period,
            IF(tempRecCount.allCount IS NULL, 0, tempRecCount.allCount) AS stuCount,
            IF(tempRecCount.approveCount IS NULL, 0, tempRecCount.approveCount) AS approveCount,
            IF(tempRecCount.pendingCount IS NULL, 0, tempRecCount.pendingCount) AS pendingCount
            {$sql_checkBox}
          FROM
            {$eclass_db}.OLE_PROGRAM AS op
          INNER JOIN tempProgramList AS t_pl
          	ON op.ProgramID = t_pl.ProgramID
          {$cnecc_sql_join}
          LEFT JOIN {$eclass_db}.OLE_CATEGORY AS oc
            ON op.Category = oc.RecordID
          LEFT JOIN tempRecCount
            ON op.ProgramID = tempRecCount.ProgramID
          LEFT JOIN tempProgramDetail t_pd
            ON op.ProgramID = t_pd.ProgramID
          WHERE
            op.IntExt = '{$IntExtType}'
            $cond
			$testCond
            $cnecc_sql_group
        ";
//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
$sqlDebug[] = $sql;

//debug_r(htmlspecialchars($sql));
// TABLE INFO
$LibTable->field_array = array("op.Title", "op.AcademicYearID", "op.Category", "op.StartDate", "stuCount", "approveCount", "pendingCount");
$LibTable->sql = $sql;
//$LibTable->title = $ec_iPortfolio['ole'];


//for($i = 0, $i_max = count($sqlDebug); $i < $i_max; $i++){
//	$tmpSql = $sqlDebug[$i];
//	debug_pr($tmpSql);
//	//error_log($tmpSql."    e:[".mysql_error()."]<----".date("Y-m-d H:i:s")." f:".__FILE__."\n\n\n", 3, "/tmp/cccc.txt");
//}
//die();
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $LibTable->page_size = $numPerPage;
$LibTable->no_col = STANDARD_NO_OF_COLUMN + $ELECount;
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1'>";
//$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
$LibTable->row_alt = array("", "");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

// TABLE COLUMN
//FIRST COLUMN
$LibTable->column_list .= "<tr class='tabletop'>\n";
$LibTable->column_list .= "<td rowspan=\"2\" class=\"tabletopnolink\" height='25' align='center' >#</span></td>\n";
$LibTable->column_list .= "<td rowspan=\"2\" class=\"tabletopnolink\" width=\"150\" >".$LibTable->column(0,$ec_iPortfolio['title'], 1)."</td>\n";
$LibTable->column_list .= "<td rowspan=\"2\" class=\"tabletopnolink\" align='center' >".$LibTable->column(1,$ec_iPortfolio['by_year'], 1)."</td>";
$LibTable->column_list .= "<td rowspan=\"2\" class=\"tabletopnolink\" align='center' >".$LibTable->column(2,$ec_iPortfolio['category'], 1)."</td>";
if ($ELECount > 0) {
	$LibTable->column_list .= ($IntExtType == "INT") ? "<td colspan=\"".$ELECount."\" class=\"tabletopnolink\" align=\"center\" nowrap='nowrap'>".$ec_iPortfolio['ele']."</td>\n" : "";
}
$LibTable->column_list .= "<td rowspan=\"2\" >".$LibTable->column(3,$ec_iPortfolio['date']."/".$ec_iPortfolio['period'], 1)."</td>\n";
$LibTable->column_list .= "<td colspan=\"3\" class=\"tabletopnolink\" align=\"center\" nowrap='nowrap'>".$ec_iPortfolio['number_of_record']."</td>\n";
$LibTable->column_list .= $h_checkAllBox;
$LibTable->column_list .= "</tr>\n";

//SECOND COLUMN
$LibTable->column_list .= "<tr class=\"tabletop\">";
if ($IntExtType == "INT")
{
	
	foreach($ELEArray as $ELECode => $ELETitle)
	{
		$ELEDisplay = str_replace(array("[", "]"), "", $ELECode);
		$ELEDisplay = is_numeric($ELEDisplay) ? $ELETitle : $ELEDisplay;
		$LibTable->column_list .= "<td align='center' class='tabletopnolink'><b><span title='".$ELETitle."'>".$ELEDisplay."</span></b></td>";
	}
	
	if ($ELECount > 0) {
		// fix php 5.4 error Omas
  		//$LibTable->column_array = array_merge(array(0,3,3), array_fill(0, (array)$ELECount, 3), array(0,3,3,3,3));
		$LibTable->column_array = array_merge(array(0,3,3), array_fill(0, $ELECount, 3), array(0,3,3,3,3));
	}
  	else {
  		$LibTable->column_array = array_merge(array(0,3,3), array(0,3,3,3,3));
  	}				
}
else
{
  $LibTable->column_array = array(0,3,3,0,3,3,3,3);
}

$LibTable->column_list .= "<td align='center' class='tabletopnolink'>".$LibTable->column(4,$i_status_all, 1)."</td>\n";
$LibTable->column_list .= "<td align='center' class='tabletopnolink'>".$LibTable->column(5,$i_status_approved, 1)."</td>\n";
$LibTable->column_list .= "<td align='center' class='tabletopnolink'>".$LibTable->column(6,$i_status_waiting, 1)."</td>\n";
$LibTable->column_list .= "</tr>";

$table_content = $LibTable->displayPlain();
$table_content .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
$table_content .= ($LibTable->navigationHTML!="") ? "<tr class='tablebottom'><td class=\"tabletext\" align=\"right\">".$LibTable->navigationHTML."</td></tr>" : "";
$table_content .= "</table>";
//debug_r($LibTable);
########################################################
# Table content : End
########################################################

########################################################
# Layout Display
########################################################
$linterface->LAYOUT_START();
include_once("template/ole_list.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>