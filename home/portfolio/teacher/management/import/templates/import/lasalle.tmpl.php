<script>
function js_Continue()
{
	if ($('input#csvfile').val() == '') {
		alert('<?=$Lang['General']['warnSelectcsvFile']?>');
		$('input#csvfile').focus();
		return false;
	}

	$('#ContinueBtn').attr('disabled',true);
	
	$('input#mod').val('import');
	$('input#task').val('lasalleImportStep1');
	$('form#form1').submit();
}
function js_Go_Back(){
	window.location = '/home/portfolio/teacher/management/ole.php';
}
</script>

<form action="index.php" method="post" name="form1" id="form1" enctype="multipart/form-data">
	<br style="clear:both;" />
	<?=$h_StepTable?>
	<div class="table_board">
<table id="html_body_frame" width="100%">
			<tr><td align="center">&nbsp;</td></tr>
			<tr>
				<td>
					<table class="form_table_v30" border = "0">
						<tr>
							<td class="field_title"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
							<td class="tabletext">
								<input class="file" type="file" name="csvfile" id="csvfile">
								<br />
							</td>
						</tr>
						<tr>
							<td class="field_title"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
							<td><?=$h_guide?><br/></td>
						</tr>
						<tr>
							<td class="field_title"><?=$Lang['iPortfolio']['IMPORT']['CodeGuide']?></td>
							<td><?=$ReferenceHTML?></td>
						</tr>
						<tr>
							<td class="field_title"><?=$import_csv['download']?></td>
							<td><?=$h_download?><br/></td>
						</tr>

						<tr>
							<td class="tabletextremark" colspan="2">
								<?=$h_MandatoryRemarks?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		</div>
	<a id="DownloadOEAMappingTmp" href='download_instruction.php?MassMailingID=58&width=780' title="<?=$Lang['iPortfolio']['OEA']['DownloadTemplateInstruction']?>" class='thickbox''></a>
	<div class="edit_bottom_v30">

		<?=$h_ContinueBtn?>

		<?=$h_CancelBtn?>
	</div>
	<input type="hidden" name="r_intext" id="r_intext" value="<?=$r_intext?>">
	<input type="hidden" name="mod" id="mod" value="">
	<input type="hidden" name="task" id="task" value="">

	<!--input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="FromImport" name="FromImport" value="1" />
	<input type="hidden" id="IsStop" name="IsStop" value="n" />
	<input type="hidden" id="StartIndex" name="StartIndex" value="0" />
	<input type="hidden" id="secretFileKey" name="secretFileKey" value="" />
	<input type="hidden" id="phKey" name="phKey" value="<?=$phKey?>" /-->
</form>


