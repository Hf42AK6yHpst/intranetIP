<script>
function js_Continue(){
	$('#ContinueBtn').attr('disabled',true);

	$('input#task').val('lasalleImportStep2');
	$('form#form1').submit();
}

function js_Go_Back(){
	window.location = 'index.php?mod=import&task=lasalle&r_intext=<?=$r_intext?>';
}

function js_Cancel(){
	window.location = '/home/portfolio/teacher/management/ole.php';
}
</script>
<form action="index.php" method="POST" name="form1" id="form1">

<br style="clear:both;" />
	<?=$h_StepTable?>
	<div class="table_board">
		<table id="html_body_frame" style="width:100%;">
			<tr>
				<td>
					<br style="clear:both;" />
					
					<table class="form_table_v30">
						<tr>
							<td class="field_title"><?=$Lang['General']['SuccessfulRecord']?></td>
							<td><div id="SuccessCountDiv"><?=$h_SuccessfulRecord?><!--<?=$Lang['General']['EmptySymbol']?>--></div></td>
						</tr>
						<tr>
							<td class="field_title"><?=$Lang['General']['FailureRecord']?></td>
							<td><div id="FailCountDiv"><?=$h_FailureRecord?><!--<?=$Lang['General']['EmptySymbol']?>--></div></td>
						</tr>
					</table>
					<br style="clear:both;" />
					
					<div id="ErrorTableDiv">&nbsp;
					<?php
					echo $h_error;
					?></div>
				</td>
			</tr>
		</table>
	</div>


	<div class="edit_bottom_v30">
	<input type = "hidden" name="importFile" value="<?=$encodeTargetFilePath?>">
		<input type = "hidden" name="r_intext" id="r_intext" value="<?=$r_intext?>">
		<input type = "hidden" name="mod" id="mod" value="import">
		<input type = "hidden" name="task" id="task" value="">
		<?=$ContinueBtn?>
		<?=$BackBtn?>
		<?=$CancelBtn?>

	</div>

</form>