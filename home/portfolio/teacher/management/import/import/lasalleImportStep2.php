<?
include_once($intranet_root."/includes/libimporttext.php");
include ($intranet_root.'/includes/import/importDataManager.php');
include ($intranet_root.'/includes/import/importData.php');
include ($intranet_root.'/includes/import/importDataDefinition.php');
include ($intranet_root.'/includes/portfolio25/import/oleImportData.php');
include ($intranet_root.'/includes/portfolio25/import/oleLasalleImportData.php');

$importFile = base64_decode($importFile);




$importObj = new libimporttext();

$objOleImportData = new oleImportData();
$objOleImportData->setBatchNo($importFile);
if($r_intext ==$ipf_cfg["OLE_TYPE_INT"]["EXT"]){
	$objOleImportData->setOLEIntExt($ipf_cfg["OLE_TYPE_STR"]["EXT"]);
}else{
	$objOleImportData->setOLEIntExt($ipf_cfg["OLE_TYPE_STR"]["INT"]);
}

$objLasalleImportData = new oleLasalleImportData();
$objLasalleImportData->setBatchNo($importFile);

$objOleImportData->setExtraImportData($objLasalleImportData);

$DefaultCsvHeaderArr = $objOleImportData->getDataDefaultCsvHeaderArr();
$ColumnPropertyArr = $objOleImportData->getDataColumnPropertyArr();


$dataArray = $importObj->GET_IMPORT_TXT_WITH_REFERENCE($importFile, "", "", $DefaultCsvHeaderArr, $ColumnPropertyArr);

$arrayWithDataOnly = $objOleImportData->dataHandle($dataArray , $DefaultCsvHeaderArr, $ColumnPropertyArr);

$objImportDataManager = new importDataManager($objOleImportData);
$objImportDataManager->setImportDataSource($arrayWithDataOnly);
$objImportDataManager->processImportData();

header("Location: index.php?mod=import&task=lasalleImportFinal");
?>