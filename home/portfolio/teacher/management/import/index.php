<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");


//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");


intranet_auth();
intranet_opendb();
$lpf = new libportfolio2007();

if($lpf->IS_IPF_SUPERADMIN()){
	//do nothing
}else{
	//not allow to access this page , redirect to iPF home page
	header("Location: /home/portfolio/");
}

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$linterface = new interface_html();

$mod = trim($mod);
$task = trim($task);

if($mod == ''){
	//if no mod and task define , redirect to iPF home page
	header("Location: /home/portfolio/");
	exit();
}




$TAGS_OBJ = array(array($pageCaption));
//$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");


$task_script = $mod.'/'.$task.'.php';

if (file_exists($task_script)) {
	include_once($task_script);
}else {
	$linterface->LAYOUT_START();
	echo "error! task not find<br/>";
	$linterface->LAYOUT_STOP();
}
			



intranet_closedb();
exit();
?>