<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_auth();
intranet_opendb();


$LibPortfolio = new libportfolio();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$LibPortfolio->ACCESS_CONTROL("ole");

$RecordArr = (is_array($record_id)) ? $record_id : array($record_id);
if (is_array($RecordArr))
{
	$RecordList = implode($RecordArr, ",");
}
else
{
	$RecordList = $record_id;
}

if (!isset($approve))
	$approve = 2;

# RecordStatus = 1 (only pending record can be deleted)
//$sql = "UPDATE {$eclass_db}.OLE_STUDENT SET ApprovedBy = IF(IFNULL(ApprovedBy, 0) = 0 OR RecordStatus <> '{$approve}', '{$UserID}', ApprovedBy), RecordStatus = '$approve', ProcessDate = now(), ModifiedDate = now() WHERE RecordID IN ($RecordList)";
$sql = "UPDATE {$eclass_db}.OLE_STUDENT SET ApprovedBy = {$UserID} , RecordStatus = '$approve', ProcessDate = now(), ModifiedDate = now() WHERE RecordID IN ($RecordList)";
$LibPortfolio->db_db_query($sql);

intranet_closedb();

if ($FromPage == "program")
	header("Location: ole_event_studentview.php?msg=2&EventID=$EventID&Year=$Year&FromPage=$FromPage&IntExt=$IntExt");
else
	header("Location: ole_student.php?msg=2&StudentID=$StudentID&page_size_change=$page_size_change&numPerPage=$numPerPage&pageNo=$pageNo&order=$order&field=$field&IntExt=$IntExt");
?>

