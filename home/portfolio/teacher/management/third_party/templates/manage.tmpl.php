<script>

function checkform(formObj){
	if($('#title').val() == ""){
		alert('<?=$assignments_alert_msg7?>');
		return false;
	}
	return true;
}

</script>

<form name="form1" method="post" action="manage_update.php" enctype="multipart/form-data" onSubmit="return checkform(this)">
	<?= $html["navigation"]?>
	
	<table width="95%" border="0" cellspacing="0" cellpadding="5"
		align="center">
		<tbody>
			<tr>
				<td valign="top">
					<!-- CONTENT HERE -->

					<div class="table_board">
						<span class="sectiontitle_v30"> <?=$iPort['menu']['third_party']?></span><br>

						<table align="center" width="100%" border="0" cellpadding="5"
							cellspacing="0">
							<tbody>
								<tr>
									<td valign="top" nowrap="nowrap" class="formfieldtitle"><span
										class="tabletext"><?=$iPort['Assessment']['Title']?><span class="tabletextrequire">*</span></span>
									</td>
									<td width="80%" valign="top">
										<table border="0" cellpadding="0" cellspacing="0" width="60%">
											<tbody>
												<tr>
													<td width="100%">
														<input autocomplete="off" id="title" name="title" type="text" size="50" value="<?=$title?>" class="textboxtext ac_input" maxlength="255">
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td valign="top" nowrap="nowrap" class="formfieldtitle"><span
										class="tabletext"><?=$ec_iPortfolio['remark']?></span>
									</td>
									<td width="80%" valign="top">
										<table border="0" cellpadding="0" cellspacing="0" width="60%">
											<tbody>
												<tr>
													<td width="100%">
														<textarea id="remarks" name="remarks" style="width:99%;height:100px;"><?=$remarks?></textarea>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="2" >
										<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
										</table>

										<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
											<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$ec_iPortfolio['mandatory_field_description']?></td></tr>
											<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
											<tr>
												<td align="center">
													<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
													<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back();") ?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
	<br>
	<!-- End of CONTENT -->
	</div>
	</td>
	</tr>
	</tbody>
	</table>

	<input type="hidden" name="action" value="<?=$html["action"]?>">
	<input type="hidden" name="assessmentId" value="<?=$assessment_id?>">
	<input type="hidden" name="callback" value="<?=$callback?>">
</form>
