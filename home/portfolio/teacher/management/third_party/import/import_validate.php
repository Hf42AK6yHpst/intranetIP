<?php
# using: yat

/*	Remark:	!important to read for ADDING NEW FIELDS in csv
			Bookmark: note_1
			-	the array position should be maintain carefully whenever added fields in the csv,
				the array number will be shifted if adding csv fields before the SchoolYear field
			-	it will trigger an error whenever the SchoolYear is not set to trigger auto detect SchoolYear							
**/
/** [Modification Log] Modifying By: 
 * *******************************************
 * 	Modification Log
 * 
 * 2016-05-27 Henry HM
 * Added field of "Default Hours"
 * 
 * 2016-03-15 Kenneth
 * 	Created
 * 
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/ole_program.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libpf-ole.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once("$intranet_root/includes/portfolio25/oleCategory/libpf-category.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-thirdparty.php");

# Error Code
define ("ERROR_TYPE_STUDENT_NOT_FOUND", 1);
define ("ERROR_TYPE_DUPLICATED_CLASSNUM", 2);
define ("ERROR_TYPE_PROGRAM_NOT_FOUND", 3);
define ("ERROR_TYPE_DATEFORMAT_NOT_CORRECT", 4);
define ("ERROR_TYPE_ACADEMIC_YEAR_NOT_FOUND", 5);
define ("ERROR_TYPE_TERM_NOT_FOUND", 6);
define ("ERROR_TYPE_EMPTY_TITLE", 7);
define ("ERROR_TYPE_NUMBER_OF_DATA_NOT_MATCH",8);
define ("ERROR_TYPE_CATEGORY_NOT_FOUND",9);
define ("ERROR_TYPE_SUBCATEGORY_NOT_FOUND",10);
define ("ERROR_TYPE_INSIDEOUTSIDE_NOT_FOUND",11);
define ("ERROR_TYPE_ISSAS_NOT_FOUND",12);
define ("ERROR_TYPE_ELE_NOT_MATCH",13);
define ("ERROR_TYPE_CAN_JOIN",14);
define ("ERROR_TYPE_JOIN_START_DATE_NOT_FOUND",15);
define ("ERROR_TYPE_APPLICABLE_FORM_NOT_FOUND",16);
define ("ERROR_TYPE_AUTO_APPROVAL_NOT_FOUND",17);
define ("ERROR_TYPE_JOIN_STARTDATE_INVALID",18);
define ("ERROR_TYPE_JOIN_ENDDATE_INVALID",19);
define ("ERROR_TYPE_APPLICABLE_FORM_INVALID",20);
define ("ERROR_TYPE_MAX_HOUR_NEGATIVE",21);
define ("ERROR_TYPE_COMPULSORY_FIELDS_INVALID",22);
define ("ERROR_TYPE_USER_NOT_FOUND",23);
define ("ERROR_TYPE_USER_NOT_TEACHER",24);
define ("ERROR_TYPE_JOIN_DATE_COEXIST",25);
define ("ERROR_TYPE_PROGRAM_ALREADY_EXIST",26);
define ("ERROR_TYPE_APPROVER_SHOULD_NOT_EXIST",27);
define ("ERROR_TYPE_DEFAULT_HOUR_NEGATIVE",28);
define ("ERROR_TYPE_EMPTY_USER_LOGIN", 29);
define ("ERROR_TYPE_EMPTY_RESULT", 30);
define ("ERROR_TYPE_USER_NOT_EXISTS", 31);

$loginUserId = $_SESSION['UserID'];

unset($error_data);
unset($valid_data);
$count_success = 0;
if ($g_encoding_unicode) {
	$import_coding = ($g_chinese == "") ? "b5" : "gb";
}

# uploaded file information
$filepath = $userfile;
$filename = $userfile_name;

$TmpIntExt = getCovertedIntExt(trim($IntExt));

$uploaderUserId = $UserID;  // variable from session

$count_new = 0;
$count_updated = 0;
$display_content = "";


if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath))
{
	header("Location: index.php");
	exit;
}
else
{
	intranet_opendb();
	$lpf = new libpf_slp();
	$lpf_ole = new libpf_ole();

	$lpf->CHECK_ACCESS_IPORTFOLIO();
	//$lpf->ACCESS_CONTROL("ole");

	$CurrentPage = "Teacher_ThirdParty";
	$CurrentPageName = $iPort['menu']['third_party'];

	$TAGS_OBJ[] = array($CurrentPageName,"");
	$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

	$linterface = new interface_html();
	$linterface->LAYOUT_START();

	$li = new libdb();
	$lo = new libfilesystem();
	$limport = new libimporttext();
	$libCategory = new Category();
	$lpf_tp = new libpf_third_party();

	$ele_list = $lpf->GET_OLE_ELE_LIST();
    $ext = strtoupper($lo->file_ext($filename));


    if ($ext == ".CSV" || $ext == ".TXT")
    {
       $data = $limport->GET_IMPORT_TXT($filepath);
       $header_row = array_shift($data);                   # drop the title bar
    }
	
    # Check Title Row
	//debug_pr($IntExt);
	//$file_format = getFileFormat($IntExt);
	$file_format =  array('Title','Description','StartDate','EndDate','UserLogin','Result','Remarks');
	
	
	//debug_pr($file_format);
    $format_wrong = false;
    for ($i=0; $i<sizeof($file_format); $i++)
    {
         if ($header_row[$i]!=$file_format[$i])
         {
         	
             $format_wrong = true;
             break;
         }
    }
    
    
	######################################
	#
	#	Clear Temp record
	#
	######################################
	
    if ($format_wrong)
    {
        $correct_format = "<table border='0' cellpadding='5' cellspacing='0'>\n";
        for ($i=0; $i<sizeof($file_format); $i++)
        {
             $correct_format .= "<tr><td>".$file_format[$i]."</td></tr>\n";
        }
        $correct_format .= "</table>\n";

        $wrong_format = "<table border='0' cellpadding='5' cellspacing='0'>\n";
        for ($i=0; $i<sizeof($header_row); $i++)
        {
			$field_title = ($header_row[$i]!=$file_format[$i]) ? "<u>".$header_row[$i]."</u>" : $header_row[$i];
			$wrong_format .= "<tr><td>".$field_title."</td></tr>\n";
        }
        $wrong_format .= "</table>\n";

        $display_content .= "<br><span class='chi_content_15'>".$ec_guide['import_error_wrong_format']."</span><br>\n";
        $display_content .= "<table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
		$display_content .= "<tr><td valign='top'>$correct_format</td><td width='90%' align='center'>VS</td><td valign='top'>$wrong_format</td></tr>\n";
        $display_content .= "</table>\n";
    }
    else
    {
		//FORMAT IS CORRECT , START THE PROCESS

		//$li->Start_Trans();
		$sqlRes = array();
		
		$ELEKeyArr = array_keys($lpf->GET_ELE());
		$CategoryInfoArr = $libCategory->GET_CATEGORY_LIST($ParShowInactive=false);
		$CategoryIDArr = Get_Array_By_Key($CategoryInfoArr, 'RecordID');
		$sizeOfData = count($data);
		for ($i=0; $i<$sizeOfData; $i++)	//	This data get the data from import file
		{
			//'Title','Description','StartDate','EndDate','UserLogin','Result','Remarks'
			$dataArray='';
			list($dataArray['Title'], $dataArray['Description'], $dataArray['StartDate'], $dataArray['EndDate'], $dataArray['UserLogin'], $dataArray['Result'], $dataArray['Remarks']) = $data[$i];
			
			
			//check date format with "yyyy-mm-dd"
			$dataArray['StartDate']  = getDefaultDateFormat($dataArray['StartDate']);
			$dataArray['EndDate'] = getDefaultDateFormat($dataArray['EndDate']);
			/*
			//Check Program already exist
			if(empty($dataArray['term'])){
				$addtional_conds = " AND ol.YearTermID ='0' ";
				$addtional_join = "";
			}else{
				$addtional_conds = " AND (YearTermNameEN='".$dataArray['term']."' OR YearTermNameB5='".$dataArray['term']."') ";
				$addtional_join = " INNER JOIN ACADEMIC_YEAR_TERM AS ayt ON (ay.AcademicYearID=ayt.AcademicYearID AND ol.YearTermID = ayt.YearTermID)";
			}
			$sql = "SELECT COUNT(ProgramID) AS num FROM {$eclass_db}.OLE_PROGRAM AS ol 
					INNER JOIN ACADEMIC_YEAR AS ay ON (ay.AcademicYearID=ol.AcademicYearID)
					$addtional_join
					WHERE ol.Title = '".$dataArray['title']."' AND 
					(YearNameEN = '".$dataArray['schoolYear']."' OR YearNameB5 = '".$dataArray['schoolYear']."' ) $addtional_conds";
// 			die($sql);
			$countReturn = $li->returnResultSet($sql);
			if($countReturn[0]['num']>0){
				$error_data[] = array($i,ERROR_TYPE_PROGRAM_ALREADY_EXIST,$data[$i]);
				continue;
			}
			*/

			if(
					checkInputDateIsValid($dataArray['StartDate']) == false || 
					(trim($dataArray['EndDate']) != "" && checkInputDateIsValid($dataArray['EndDate']) == false)  //case for if $t_end is empty , no need to check date format
				)
			{
				$error_data[] = array($i,ERROR_TYPE_DATEFORMAT_NOT_CORRECT,$data[$i]);
				continue;  
			}
			/*
			 * Y	T
			 * 0	0	->current year, all terms
			 * 0	1	->current year, specific term
			 * 1	0	->specific year, all terms
			 * 1	1	->specific year, specific term
			 */			
			# put academic year with current year
			$fcm = new form_class_manage();

			# put academic year with current year in start date
			$currentYearAndYearTerm = getAcademicYearInfoAndTermInfoByDate($dataArray['StartDate']);

			if (isset($dataArray['schoolYear']) && $dataArray['schoolYear'] != "") {
				// do nothing
			} else {
				$dataArray['schoolYear'] = $currentYearAndYearTerm[1];
				//$dataArray['term'] = $currentYearAndYearTerm[3]; // *Bookmark: note_1 <==Assign the value from DB to data array, must map with corresponding position
			}
			
			$academicInfoWithTermInfo = getAcademicInfoWithTermInfo();
			$academicYearExist = false;
			$termExist = false;
			
			foreach($academicInfoWithTermInfo as $key => $academicElements) {
				if ($dataArray['schoolYear'] == $academicElements["ACADEMICYEARNAMEEN"] || $dataArray['schoolYear'] == $academicElements["ACADEMICYEARNAMEB5"]) {
					$academicYearExist = true;
					
					$sizeOfTerm = count($academicElements["YEARTERMINFO"]);
					if (empty($dataArray['term'])) {
						$termExist = true;
					} else {
						for($j=0;$j<$sizeOfTerm;$j++) {
							$termElement = $academicElements["YEARTERMINFO"][$j];
							if ($dataArray['term'] == $termElement["YEARTERMNAMEEN"] || $dataArray['term'] == $termElement["YEARTERMNAMEB5"]) {
								$termExist = true;
							}
						}
					}
				}
			}
			# check the academic year id
			if ($academicYearExist) {
				// do nothing
			} else {
				$error_data[] = array($i,ERROR_TYPE_ACADEMIC_YEAR_NOT_FOUND,$data[$i]);
				continue;
			}
			# check the term id	
			if ($termExist) {
				// do nothing
			} else {
				$error_data[] = array($i,ERROR_TYPE_TERM_NOT_FOUND,$data[$i]);
				continue;
			}

			//check if applicable forms are valid
			if($IntExt==0){
				if($dataArray['AllowStudentsToJoin']=='Y'||$dataArray['AllowStudentsToJoin']=='y'){
					$applicableFormArray = explode(';',$dataArray['ApplicableForm']);
					$fcm = new form_class_manage();
					$yearClassArr = $fcm->Get_Class_List_By_Academic_Year(Get_Current_Academic_Year_ID());
					$year_arr = array();
					for($j=0; $j<count($yearClassArr); $j++)
					{
					  $year_arr[] = $yearClassArr[$j]["YearName"];
					}
					$year_arr = array_values(array_map("unserialize", array_unique(array_map("serialize", $year_arr)))); 
					$allFoundInArray = true;
					foreach($applicableFormArray as $applicableForm){
						$_isFoundInArray = in_array($applicableForm,$year_arr);
						if(!$_isFoundInArray){
							$allFoundInArray = false;
						}else{
							//$tempYearIDArray[]=
						}
					}
					if(!$allFoundInArray){
						$error_data[] = array($i,ERROR_TYPE_APPLICABLE_FORM_INVALID,$data[$i]);
						continue;
					}
					$tempYearInfoArray = array();
					$year = new Year();
					foreach($applicableFormArray as $applicableForm){
						$tempYearInfoArray[] = $year->Get_Year_Info_By_Name($applicableForm);
					}
					$dataArray['ApplicableForm'] = implode(',',Get_Array_By_Key($tempYearInfoArray,'YearID'));
				}
			}
			
			//check max hour must not be negative
			if(!($dataArray['MaximumHours']>=0||$dataArray['MaximumHours']=='')){
				$error_data[] = array($i,ERROR_TYPE_MAX_HOUR_NEGATIVE,$data[$i]);
				continue;
			}
			
			//check default hour must not be negative
			if(!($dataArray['DefaultHours']>=0||$dataArray['DefaultHours']=='')){
				$error_data[] = array($i,ERROR_TYPE_DEFAULT_HOUR_NEGATIVE,$data[$i]);
				continue;
			}



			# check the header column and the content column number
			if (count($header_row) != count($data[$i])) {
				$error_data[] = array($i,ERROR_TYPE_NUMBER_OF_DATA_NOT_MATCH,$data[$i]);
				continue;
			}		
			
			
//			debug_pr($dataArray);
			# check empty title
			if (empty($dataArray['Title'])) {
				$error_data[] = array($i,ERROR_TYPE_EMPTY_TITLE,$data[$i]);
				continue;
			}
			
			# check empty user login
			if (empty($dataArray['UserLogin'])) {
				$error_data[] = array($i,ERROR_TYPE_EMPTY_USER_LOGIN,$data[$i]);
				continue;
			}
			
			# check if user login valid
			$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='".$dataArray['UserLogin']."'";
			$dataArray['UserID'] = current($li->returnVector($sql));
			if (empty($dataArray['UserID'])) {
				$error_data[] = array($i,ERROR_TYPE_USER_NOT_EXISTS,$data[$i]);
				continue;
			}
			
			# check empty result
			if (empty($dataArray['Result'])) {
				$error_data[] = array($i,ERROR_TYPE_EMPTY_RESULT,$data[$i]);
				continue;
			}
			
			$valid_data[] = $dataArray;
			
			//debug_pr($data);
			$count_success++;	
		}
		# Display import stats
		$display_content = "<div class='tabletext' style='text-align: center'>".$Lang['General']['SuccessfulRecord']." : <b>$count_success</b></div><br>";
		$display_content .= "<div class='tabletext' style='text-align: center'>".$Lang['General']['FailureRecord']." : <b>".count($error_data)."</b></div><br>";
		
	}
}


if (sizeof($error_data)>0)
{
    $error_table = "<br><table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
    $error_table .= "<tr class=tableTitle bgcolor='#CFE6FE'><td>".$ec_guide['import_error_row']."</td><td>".$ec_guide['import_error_reason']."</td><td>".$ec_guide['import_error_detail']."</td></tr>\n";

    for ($i=0; $i<sizeof($error_data); $i++)
    {
         list ($t_row, $t_type, $t_data) = $error_data[$i];
         $t_row++;     # set first row to 1
         $css_color = ($i%2==0) ? "#FFFFFF" : "#F3F3F3";
         $error_table .= "<tr bgcolor='$css_color'><td class='tabletext'>$t_row</td><td class='tabletext'>";
         $reason_string = "Unknown";
         switch ($t_type)
         {
                 case ERROR_TYPE_STUDENT_NOT_FOUND:
                      $reason_string = $ec_guide['import_error_no_user'];
                      break;
				case ERROR_TYPE_DUPLICATED_CLASSNUM:
                      $reason_string = $ec_guide['import_error_duplicate_classnum'];
                      break;
			    case ERROR_TYPE_PROGRAM_NOT_FOUND:
					  $reason_string = $ec_guide['import_programid_not_found'];	
					  break;
				case ERROR_TYPE_DATEFORMAT_NOT_CORRECT:
				case ERROR_TYPE_JOIN_STARTDATE_INVALID:
				case ERROR_TYPE_JOIN_STARTDATE_INVALID:
					  $reason_string = $ec_guide['import_dateformat_not_correct'];	
					  break;
				case ERROR_TYPE_ACADEMIC_YEAR_NOT_FOUND:
					  $reason_string = $ec_guide['import_academic_year_not_found'];	
					  break;
				case ERROR_TYPE_TERM_NOT_FOUND:
					  $reason_string = $ec_guide['import_type_term_not_found'];	
					  break;
				case ERROR_TYPE_EMPTY_TITLE:
					$reason_string = $ec_guide['import_empty_title'];
					break;
				case ERROR_TYPE_NUMBER_OF_DATA_NOT_MATCH:
					$reason_string = $ec_guide['import_header_content_column_not_match'];
					break;
				case ERROR_TYPE_CATEGORY_NOT_FOUND:
					$reason_string = $ec_guide['import_ole_category_not_found'];
					break;
				case ERROR_TYPE_SUBCATEGORY_NOT_FOUND:
					$reason_string = $ec_guide['import_ole_subcategory_not_found'];
					break;
				case ERROR_TYPE_INSIDEOUTSIDE_NOT_FOUND:
					$reason_string = $ec_guide['import_ole_insideoutside_not_found'];
					break;
				case ERROR_TYPE_ISSAS_NOT_FOUND:
					$reason_string = $ec_guide['import_ole_isSAS_not_found'];
					break;
				case ERROR_TYPE_ELE_NOT_MATCH:
					$reason_string = $ec_guide['import_ole_ele_not_match'];
					break;
				case ERROR_TYPE_CAN_JOIN:
					$reason_string = $ec_guide['import_ole_can_join'];
					break;
				case ERROR_TYPE_JOIN_START_DATE_NOT_FOUND:
					$reason_string = $ec_guide['import_ole_join_start_date_not_found'];
					break;
				case ERROR_TYPE_APPLICABLE_FORM_NOT_FOUND:
					$reason_string = $ec_guide['import_ole_applicable_form_not_found'];
					break;
				case ERROR_TYPE_AUTO_APPROVAL_NOT_FOUND:
					$reason_string = $ec_guide['import_ole_auto_approval_not_found'];
					break;
				case ERROR_TYPE_APPLICABLE_FORM_INVALID:
					$reason_string = $ec_guide['import_ole_applicable_form_invalid'];
					break;	
				case ERROR_TYPE_MAX_HOUR_NEGATIVE:
					$reason_string = $ec_guide['import_ole_max_hour_negative'];
					break;	
				case ERROR_TYPE_DEFAULT_HOUR_NEGATIVE:
					$reason_string = $ec_guide['import_ole_default_hour_negative'];
					break;	
				case ERROR_TYPE_USER_NOT_FOUND:
					$reason_string = $ec_guide['import_ole_user_not_found'];
					break;	
				case ERROR_TYPE_USER_NOT_TEACHER:
					$reason_string = $ec_guide['import_ole_user_not_teacher'];
					break;
				case ERROR_TYPE_JOIN_DATE_COEXIST:
					$reason_string = $ec_guide['import_ole_join_date_not_coexist'];
				break;
				case ERROR_TYPE_PROGRAM_ALREADY_EXIST:
					$reason_string = $ec_guide['import_ole_program_title_already_exist'];
				break;
				case ERROR_TYPE_COMPULSORY_FIELDS_INVALID:
					$reason_string = $ec_guide['import_ole_compulsory_field_invalid'];
				break;
				case ERROR_TYPE_APPROVER_SHOULD_NOT_EXIST:
					$reason_string = $ec_guide['import_ole_user_should_not_exist'];
				break;
				case ERROR_TYPE_EMPTY_USER_LOGIN:
					$reason_string = $ec_guide['import_user_login_empty'];
					break;
				case ERROR_TYPE_EMPTY_RESULT:
					$reason_string = $ec_guide['import_user_result_empty'];
				break;
				case ERROR_TYPE_USER_NOT_EXISTS:
					$reason_string = $ec_guide['import_user_login_invalid'];
				break;
				default:
                     $reason_string = $ec_guide['import_error_unknown'];
                     break;
         }
         $error_table .= $reason_string;
         $error_table .= "</td><td class='tabletext'>".implode(",",$t_data)."</td></tr>\n";
    }
    $error_table .= "</table>\n";
    $display_content .= $error_table;
}else{
	foreach($valid_data as $d){
		$display_content .= "<input type='hidden' name='import_data[]' value='".serialize($d)."'>";
	}	
}

### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=2);

##################################
#	Buttons
##################################
$x='';
if(count($error_data)>0||$format_wrong){
	$x.=$linterface->GET_ACTION_BTN($ec_guide['import_back'], "button", "self.location='index.php';");
}else{
	$x.=$linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "submit", "");
}
$x.=$linterface->GET_ACTION_BTN($button_back, "button", "self.location='../index.php'");
$htmlAry['Btns']=$x;


//debug_pr($IntExt);
?>
	<table align="left" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
		<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" border="0" align="absmiddle"> <?=$button_import?> </a>
		</td>
		<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
	</tr>
	</table>
	
	<br />
	
<FORM enctype="multipart/form-data" method="POST" name="form1" action="import_final.php">
	<?=$htmlAry['steps']?>
	<br />
	<?= $display_content ?>
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	<p align="center">
	<?= $htmlAry['Btns'] ?>
	</p>
</FORM>



<?php
/* 200911030000MaxWong */
////////////////
//  FUNCTION ///
////////////////
function getCovertedIntExt($ParIntExt=0) {
	if ($ParIntExt == 0) {
		return "INT";
	} else if ($ParIntExt == 1) {
		return "EXT";
	}
	return null;
}

function getConvertedEle($ParImportEle) {
	global $ipf_cfg;
	
	$lpf_ole = new libpf_ole();
	
	$ELEArray = $lpf_ole->Get_ELE_Info(array($ipf_cfg["OLE_ELE_RecordStatus"]["Public"], $ipf_cfg["OLE_ELE_RecordStatus"]["DefaultAndPublic"]));
	$ELEAssoArr = BuildMultiKeyAssoc($ELEArray, 'RecordID');
	
	$ELE_IDArr = explode(";", $ParImportEle);
	$numOfELE = count($ELE_IDArr);
	$ELE_CodeArr = array();
	for ($k=0; $k<$numOfELE; $k++) {
		$thisELE_ID = $ELE_IDArr[$k];
		$thisELE_Code = $ELEAssoArr[$thisELE_ID]['ELECode'];
		if ($thisELE_Code != '') {
			$ELE_CodeArr[] = $thisELE_Code;
		}else{
			return "ERROR_NOT_FOUND";
		}
	}
	$ELE_CodeText = implode(',', (array)$ELE_CodeArr);
	
	return $ELE_CodeText;
}

function getAcademicInfoWithTermInfo() {
		global $intranet_db, $li, $intranet_session_language;

	$sql = "
SELECT
	AY.ACADEMICYEARID,
	AY.YEARNAMEEN,
	AY.YEARNAMEB5,
	AYT.YEARTERMID,
	AYT.YEARTERMNAMEEN,
	AYT.YEARTERMNAMEB5
FROM
	" . $intranet_db . ".ACADEMIC_YEAR AY
	LEFT JOIN " . $intranet_db . ".ACADEMIC_YEAR_TERM AYT
		ON AY.ACADEMICYEARID = AYT.ACADEMICYEARID
			";
	$result = $li->returnArray($sql);
	$returnArray = array();
	$sizeOfResult = count($result);
	for($i=0; $i<$sizeOfResult; $i++) {
		if (isset($result[$i]["ACADEMICYEARID"]) && $result[$i]["ACADEMICYEARID"] != "") {
			$yearTermArray = array("YEARTERMID"=>$result[$i]["YEARTERMID"],
								"YEARTERMNAMEEN"=>$result[$i]["YEARTERMNAMEEN"],
								"YEARTERMNAMEB5"=>$result[$i]["YEARTERMNAMEB5"]);
			
			$returnArray[$result[$i]["ACADEMICYEARID"]]["ACADEMICYEARNAMEEN"] = $result[$i]["YEARNAMEEN"];
			$returnArray[$result[$i]["ACADEMICYEARID"]]["ACADEMICYEARNAMEB5"] = $result[$i]["YEARNAMEB5"];
			$returnArray[$result[$i]["ACADEMICYEARID"]]["YEARTERMINFO"][] = $yearTermArray;
		}
	}
	return $returnArray;
}
function getAcademicYearIdByName($ParAcademicYearName="") {
	global $intranet_db, $li;

	$sql = "
SELECT
	ACADEMICYEARID
FROM
	" . $intranet_db . ".ACADEMIC_YEAR
WHERE
	YEARNAMEEN = '" . $ParAcademicYearName . "'
	OR
	YEARNAMEB5 = '" . $ParAcademicYearName . "'
			";
	$result = $li->returnArray($sql);
	return $result[0]["ACADEMICYEARID"];
}
function getTermIdByAcademicYearAndName($ParAcademicYearName="",$ParTermName="") {
		global $intranet_db, $li;

	$sql = "
SELECT
	AY.ACADEMICYEARID,
	AYT.YEARTERMID
FROM
	" . $intranet_db . ".ACADEMIC_YEAR AY
	LEFT JOIN " . $intranet_db . ".ACADEMIC_YEAR_TERM AYT
		ON AY.ACADEMICYEARID = AYT.ACADEMICYEARID
WHERE
	(AY.YEARNAMEEN = '" . $ParAcademicYearName . "'
	OR
	AY.YEARNAMEB5 = '" . $ParAcademicYearName . "')
	AND
	(AYT.YEARTERMNAMEEN = '" . $ParTermName . "'
	OR
	AYT.YEARTERMNAMEB5 = '" . $ParTermName . "'
	)
			";
	$result = $li->returnArray($sql);
	return $result[0]["YEARTERMID"];
}
function writeTempProgram($dataDetailArr="",$creatorID , $eleConfigAry="",$db="") {
	global $eclass_db;
	global $ipf_cfg;
	global $loginUserId;
	global $IntExt;
	if (isset($db) && $db != "") {
	} else {
		$db = new libdb;
	}




	//$DataArray = array_combine($dataHeaderArr, $dataDetailArr);
//	debug_pr($dataDetailArr);
	$academicYearId = getAcademicYearIdByName($dataDetailArr["schoolYear"]);
	$yearTermId = getTermIdByAcademicYearAndName($dataDetailArr["schoolYear"], $dataDetailArr["term"]);

	$returnVal = 0;
	$t_ele = $dataDetailArr["ele"];

	if ($dataDetailArr['startDate'] != "")
	{
		$Cond .= " AND StartDate='".$dataDetailArr['startDate']."' ";
	}
	if ($dataDetailArr['endDate'] != "")
	{
		$Cond .= " AND EndDate='".$dataDetailArr['endDate']."' ";
	}
	if ($dataDetailArr['catID'] != "")
	{
		$Cond .= " AND Category='".addslashes($dataDetailArr['catID'])."' ";
	}
	
	if($t_ele == ''){
			$componentCond = ' and ELE = \''.$t_ele .'\'';
	}else{
		$inputElE = array();
		$inputELEStr  = '';
		$_userInputEle = explode(';',$t_ele);
		$_userInputEle = array_filter($_userInputEle);
		
		foreach($eleConfigAry as $eleItems => $eleDetails){
			$_eleID = $eleDetails['RecordID'];

			if(in_array($_eleID, $_userInputEle)){
				//if this setting ELE in the user input ELE , record this DefaultID

				//for user self ele , the defaultid is a integer format , not [OTHER] , with "[", so append to it for DB checking
				$inputElE[] = (is_numeric($eleDetails['DefaultID']))? '['.$eleDetails['DefaultID'].']': $eleDetails['DefaultID'];
			}
		}
		

		if(count($inputElE) > 0){
			$inputELEStr = implode(',',$inputElE);
		}
//error_log("111--->".$inputELEStr."\n", 3, "/tmp/aaa.txt");
		if(trim($inputELEStr) != ''){
			$componentCond = ' and ELE = \''.$inputELEStr.'\'';
		}
		
	}
//error_log("111--->".$inputELEStr." 	componentCond  ===>".$componentCond."\n", 3, "/tmp/aaa.txt");
	$intExtCond = " AND IntExt = '0' ";
// $academicYearId = getAcademicYearIdByName($DataArray["SchoolYear"]);
// 	$yearTermId = getTermIdByAcademicYearAndName($DataArray["SchoolYear"], $DataArray["Term"]);
	$Sql = 	"SELECT
					ProgramID
					FROM
						{$eclass_db}.OLE_PROGRAM
					WHERE
						Title = '".addslashes($dataDetailArr['title'])."'
						$Cond
						$componentCond
						$intExtCond
						and AcademicYearID=$academicYearId 
				";
	if($yearTermId)
		$Sql .= " and YearTermID=$yearTermId ";
//error_log($Sql."\n", 3, "/tmp/aaa.txt");

	$ReturnArr1 = $db->returnVector($Sql);
//	if (count($ReturnArr1) > 0)
//	{
//		//BECAREFULL, IF THERE IS MORE THAN ONE TITLE WITH THE SAME STARTDATE AND END DATE, IT MAY HAVE ERROR. 
//		//NOW IS RETURN THE FISRT OCCURANCE RECORD
//
////error_log("return old one ".$ReturnArr1[0]."\n", 3, "/tmp/aaa.txt");
//		return $ReturnArr1[0];
//	}
//	else
//	{

		if($dataDetailArr['insideOutside']=='O'||$dataDetailArr['insideOutside']=='o'){
			$isOutsideSchool = 1;
		}else{
			$isOutsideSchool = 0;
		}

		if($dataDetailArr['isSAS']=='Y'||$dataDetailArr['isSAS']=='y'||$dataDetailArr['isSAS']=='1'){
			$isSAS = 1;
		}else{
			$isSAS = 0;
		}

		$ELEList = getConvertedEle($t_ele);

		$programType = $ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"];
		$title = addslashes($dataDetailArr["title"]);
		$startDate = addslashes($dataDetailArr["startDate"]);
		$endDate = addslashes($dataDetailArr["endDate"]);
		$category = addslashes($dataDetailArr["catID"]);
		$subcategory = addslashes($dataDetailArr["subCatID"]);
		$organisation = addslashes($dataDetailArr["organisation"]);
		$details = addslashes($dataDetailArr["details"]);
		$remarks = addslashes($dataDetailArr["remark"]);
		$comeFrom = $ipf_cfg["OLE_PROGRAM_COMEFROM"]["teacherImport"];
		$allowToJoin = convertYesAndNo(addslashes($dataDetailArr["AllowStudentsToJoin"]));
		$joinStartDate = addslashes($dataDetailArr["JoinPeriodStartDate"]);
		$joinEndDate = addslashes($dataDetailArr["JoinPeriodEndDate"]);
		$applicableForm = convertSeperator(addslashes($dataDetailArr["ApplicableForm"]));
		$autoApproval = convertYesAndNo(addslashes($dataDetailArr["AutoApproval"]));
		$maxHour = addslashes($dataDetailArr["MaximumHours"]);
		$defaultHour = addslashes($dataDetailArr["DefaultHours"]);
		$compulsoryFields = convertSeperator(addslashes($dataDetailArr["CompulsoryFields"]));
		
		$intExt = ($IntExt==1)?"EXT":"INT";
		
		# get user Login
		$sql = "SELECT DISTINCT
						UserID
					FROM
						INTRANET_USER
							WHERE  
						UserLogin = '". $dataDetailArr['DefaultApprover'] ."'
				";
		$UserArr = $db->returnVector($sql);
		
		$defaultApprover = addslashes($UserArr[0]);
		$values = "'$loginUserId','$programType','$title','$startDate','$endDate', '$category', 
				'$subcategory','$organisation','$details','$remarks','$inputELEStr',
						'$intExt', '$academicYearId','$yearTermId', '$comeFrom', '$isSAS', '$isOutsideSchool','$loginUserId',
						'$allowToJoin','$joinStartDate','$joinEndDate','$applicableForm','$autoApproval','$maxHour','$defaultHour','$compulsoryFields','$defaultApprover'";
		$sql = "INSERT INTO {$eclass_db}.TEMP_OLE_PROGRAM 
						(LoginUserID,ProgramType,Title,StartDate,EndDate,Category,
							SubCategoryID,Organization,Details,SchoolRemarks,ELE,
							IntExt,AcademicYearID,YearTermID,ComeFrom,IsSAS,IsOutsideSchool,CreatorID,
							CanJoin,CanJoinStartDate,CanJoinEndDate,JoinableYear,AutoApprove,MaximumHours,DefaultHours,CompulsoryFields,DefaultApprover			) VALUES ($values)";
//		debug_pr($sql);
		$db->db_db_query($sql);
//error_log("return new ".$returnVal."\n", 3, "/tmp/aaa.txt");

//	}

	//PROGRAM ID MUST BE A INTEGER 
	return intval($returnVal);
}
function convertYesAndNo($yesAndNo){
	if($yesAndNo=='Y'||$yesAndNo=='y'||$yesAndNo=='1'){
		$returnData = 1;
	}else{
		$returnData = 0;
	}
	return $returnData;
}
function convertSeperator($data){
	return str_replace(';',',',str_replace(' ','',$data));
}
function getDbFields($ParIntExt) {
	global $sys_custom;
	define("DBF_LOGINUSERID", "LoginUserID");
	define("DBF_USERID", "UserID");
	define("DBF_TITLE", "Title");
	define("DBF_CATEGORY", "Category");
	define("DBF_ELE", "ELE");
	define("DBF_ORGANIZATION", "Organization");
	define("DBF_ROLE", "Role");
	define("DBF_HOURS", "Hours");
	define("DBF_ACHIEVEMENT", "Achievement");
	define("DBF_DETAILS", "Details");
	define("DBF_STARTDATE", "StartDate");
	define("DBF_ENDDATE", "EndDate");
	define("DBF_APPROVEDBY", "ApprovedBy");
	define("DBF_REMARK", "Remark");
	define("DBF_RECORDSTATUS", "RecordStatus");
	define("DBF_MODIFIEDDATE", "ModifiedDate");
	define("DBF_INPUTDATE", "InputDate");
	define("DBF_INTEXT", "IntExt");
	define("DBF_PROGRAMID", "ProgramID");
	define("DBF_COMEFROM", "ComeFrom");

	
	$fields = array();
	array_push($fields, DBF_LOGINUSERID);
	array_push($fields, DBF_USERID);
	array_push($fields, DBF_TITLE);
	array_push($fields, DBF_CATEGORY);

	if ($ParIntExt == 0) array_push($fields, DBF_ELE);
	array_push($fields, DBF_ORGANIZATION);
	array_push($fields, DBF_ROLE);
	if ($ParIntExt == 0) array_push($fields, DBF_HOURS);
	array_push($fields, DBF_ACHIEVEMENT);
  	array_push($fields, DBF_DETAILS);
	array_push($fields, DBF_STARTDATE);
	array_push($fields, DBF_ENDDATE);
	array_push($fields, DBF_APPROVEDBY);
	array_push($fields, DBF_REMARK);
	array_push($fields, DBF_RECORDSTATUS);
	array_push($fields, DBF_MODIFIEDDATE);
	array_push($fields, DBF_INPUTDATE);
	array_push($fields, DBF_INTEXT);

	array_push($fields, DBF_PROGRAMID);
	array_push($fields, DBF_COMEFROM);
	
	
	return implode(",",$fields);
}

$linterface->LAYOUT_STOP();
intranet_closedb();
function checkInputDateIsValid($date){
		//match the format of the date
if (preg_match ("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $date, $parts))
{
			//check weather the date is valid of not
	if(checkdate($parts[2],$parts[3],$parts[1]))
		return true;
	else
		return false;
	}
	else
	{
		return false;
	}
}
?>
