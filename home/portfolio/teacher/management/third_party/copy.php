<?php 
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-teacher.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-thirdparty.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
iportfolio_auth("T");
intranet_opendb();

$libpf_tp = new libpf_third_party();
$result = 1;
foreach($assessment_id as $aid){
	$AssessmentArr = $libpf_tp->getAssessmentInfo($aid);
	$title = $AssessmentArr['AssessmentTitle']."(".$button_copy.")";  
	$remarks = $AssessmentArr['Remark'];
	$msg = $libpf_tp->addAssessmentInfo($title,$remarks);
	if($msg==0)$result=0;
}

intranet_closedb();

header('Location: index.php?msg='.$result);
?>