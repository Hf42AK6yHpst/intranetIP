<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_auth();
intranet_opendb();

/**********************************************************************
 * This page is mainly divided into THREE parts ==> INPUT, PROCESS AND OUTPUT.
 * Please put the code in corresponding parts for clearance.
 **********************************************************************/
/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~INPUT~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
###############################################
##	get POST data
//~ other parameters
$YearClassUserID = $_POST["YearClassUserID"];

###############################################



/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~PROCESS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
###############################################
##	JS - student_id_to_load
if (is_array($YearClassUserID)) {
	$id_to_load = implode(",",$YearClassUserID);
}
$js["year_class_user_id_to_load"] = "var year_class_user_id_to_load = [".$id_to_load."];";
###############################################

###############################################
##	Construct textarea
$html["textarea_template"] = addslashes("<textarea style='width:100%' rows='10' name='{{{ OBJ_NAME }}}'>{{{ CONTENT }}}</textarea>");
###############################################

/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~OUTPUT~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
###############################################
##	Preparation before start a new page
$libportfolio = new libportfolio();
$CurrentPage = "Teacher_TeacherComment";	//	set the current page in the $MODULE_OBJ
$MODULE_OBJ = $libportfolio->GET_MODULE_OBJ_ARR("Teacher");	//	Get the MODULE_OBJ in specific module (in this case get from libportfolio), which is used in $linterface to show the left menu

$CurrentPageName = $button_edit.(strtoupper($intranet_session_language)=="EN"?" ":"").$ec_iPortfolio['teacher_comment'];	//	set the current page name  
$TAGS_OBJ[] = array($CurrentPageName,"");	//	Put the current page name to the $TAGS_OBJ, which is used in $linterface to show the page name
###############################################

###############################################
##	Create the page
$PAGE_NAVIGATION[] = array($ec_iPortfolio['teacher_comment'], "index.php");
$PAGE_NAVIGATION[] = array($CurrentPageName);
$linterface = new interface_html();	//	cannot use $interface_html as variable(i.e. $interface_html = new interface_html();), will cause error, reason: unknown
$html["navigation"] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
$linterface->LAYOUT_START();
include_once("templates/teacher_comment_edit.tmpl.php");	//	the content besides the new page layout
$linterface->LAYOUT_STOP();
###############################################

intranet_closedb();
?>
