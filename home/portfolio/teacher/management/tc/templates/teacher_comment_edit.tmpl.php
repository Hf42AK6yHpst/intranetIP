<script>
<!--
$(document).ready(function() {
	<?=$js["year_class_user_id_to_load"]?>
	teacherCommentLoad(year_class_user_id_to_load.toString());
});
/* object debug */
function jdebug(objName) {
	obj = eval(objName);
	var temp = "";
	for (x in obj)
			temp += x + ": " + obj[x] + "\n";
	alert (temp);
}
function teacherCommentLoad(ParYearClassUserIds) {
	$.ajax({
		url:      "ajax/ajax_teacher_comment_handler.php",
		type:     "POST",
		data:     "Action=LOAD&yearClassUserIds="+ParYearClassUserIds,
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
				$teacherComment = $(xml).find("teacherComment");
				var studentCount = 0;
				$.each($teacherComment,function() {
					var recordid = $(this).find("recordid").text();
					var yearclassuserid = $(this).find("yearclassuserid").text();
					var studentid = $(this).find("studentid").text();
					var classtitle = $(this).find("classtitle").text();
					var classnumber = $(this).find("classnumber").text();
					var username = $(this).find("username").text();
					var comment = $(this).find("comment").text();
					var modifiedDate = $(this).find("modifiedDate").text();
					var modifiedBy = $(this).find("modifiedBy").text();
					
					if (modifiedDate=='') {
						modifiedDate = "--";
					}
					if (modifiedBy=='') {
						modifiedBy = "--";
					}
					if ($("#teacherComment_"+studentid).length) {
						$("#teacherComment_"+studentid+"_comment").val(comment);
						$("#teacherComment_"+studentid+"_modifiedDate").text(modifiedDate);
						$("#teacherComment_"+studentid+"_modifiedBy").text(modifiedBy);							
					} else {
						var textareaContent = "<?=$html["textarea_template"]?>".replace(/{{{ OBJ_NAME }}}/g,"teacherComment_"+studentid+"_comment").replace("{{{ CONTENT }}}",comment);

						$modifiedDateTable = $("<table width='100%'></table>")
												.attr("width","100%")
												.append($("<tr></tr>")
													.append($("<td></td>").attr("nowrap","nowrap").attr("width","30%").html("<b><?=$Lang['General']['LastModified']?>: </b>").append($("<span></span>").attr("id","teacherComment_"+studentid+"_modifiedDate").text(modifiedDate)))
													.append($("<td></td>").attr("nowrap","nowrap").attr("width","30%").html("<b><?=$Lang['General']['LastModifiedBy']?>: </b>").append($("<span></span>").attr("id","teacherComment_"+studentid+"_modifiedBy").text(modifiedBy)))
													.append($("<td></td>")
														.append($("<input/>")
															.attr("type","button")
															.addClass("formbutton")
															.mouseover(function() {$(this).attr("class",'formbuttonon');})
															.mouseout(function() {$(this).attr("class",'formbutton');})
															.bind("click",function() {Save(studentid,yearclassuserid);})
															.val("<?=$button_save?>")
															.css("float","right"))));
															
						$("#teacherCommentDetails").append(
														$("<div></div>").attr("id","teacherComment_"+studentid)
																		.append($("<div></div>")
																			.append($("<span></span>").hide().attr("id","teacherComment_"+studentid+"_updateSuccess").css("float","right").html("<?=addslashes(str_replace("\n"," ",$linterface->GET_SYS_MSG("update")))?>"))
																			.append($("<span></span>").hide().attr("id","teacherComment_"+studentid+"_updateFail").css("float","right").html("<?=addslashes(str_replace("\n"," ",$linterface->GET_SYS_MSG("update_failed")))?>")))
																		.append(
																				$("<div></div>").html("<u><?=$i_identity_student?> "+(++studentCount)+" :"+classtitle+"("+classnumber+")"+" - "+username+"</u>")
																				)
																		.append(
																				$(textareaContent)
																				)
																		.append(
																				$("<div></div>").append($modifiedDateTable)
																				)
													);

						$("#teacherCommentDetails").append("<br/>");
					}		
				});
			}
	});
}
function Save(ParStudentId,ParYearClassUserId) {
	
	var comment = $("textarea[name=teacherComment_"+ParStudentId+"_comment]").val();

	$.ajax({
		url:      "ajax/ajax_teacher_comment_handler.php",
		type:     "POST",
		data:     "Action=UPDATE&studentId="+ParStudentId+"&comment="+comment,
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					fail(ParStudentId);
					alert(xhr.responseText);
				  },
		success:  function(xml){
			if (Number($(xml).find("result").text())==1) {
				success(ParStudentId);
				teacherCommentLoad(ParYearClassUserId);
			} else {
				fail(ParStudentId);
			}
		}
	});
	
}
function success(ParStudentId) {
	$("#teacherComment_"+ParStudentId+"_updateSuccess").show().fadeOut(5000);
	$("#teacherComment_"+ParStudentId+"_updateFail").hide();
}
function fail(ParStudentId) {
	$("#teacherComment_"+ParStudentId+"_updateFail").show().fadeOut(5000);
	$("#teacherComment_"+ParStudentId+"_updateSuccess").hide();
}
-->
</script>

<form name="form1" action="" method="POST">
	<?= $html["navigation"] ?>
	<br /><br />
	<p class="spacer"/>
	<div id="teacherCommentDetails">
	</div>
	<div class="dotline"></div><br/>
	<div style="text-align:center"><input type="button" class="formbutton" onmouseover="javascript: $(this).attr('class','formbuttonon');" onmouseout="javascript: $(this).attr('class','formbutton');" onclick="self.location='./index.php'" value="<?=$button_back?>"/></div>
</form>