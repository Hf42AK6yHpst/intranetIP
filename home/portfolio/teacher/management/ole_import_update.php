<?php
# using: yat

/*	Remark:	!important to read for ADDING NEW FIELDS in csv
			Bookmark: note_1
			-	the array position should be maintain carefully whenever added fields in the csv,
				the array number will be shifted if adding csv fields before the SchoolYear field
			-	it will trigger an error whenever the SchoolYear is not set to trigger auto detect SchoolYear							
**/
###################################################################
#
#	not using anymore!!!!!! 
#	pls go to ole_import_step1.php
#
###################################################################
/** [Modification Log] Modifying By: 
 * *******************************************
 * 	Modification Log
 * 2016-02-11 Kenneth
 * - Modified logic to check subcategory / isoutsideschool / isSAS
 * - Add GET param intExt in back button
 * 
 * 2013-02-20 YatWoon 
 * - Modified check program need create or not, add "school year" and "term" checking [Case#2013-0220-1424-48054]
 *
 * 2011-09-02 Ivan [2011-0826-0949-55066]
 * - Modified getConvertedEle(), re-write this function to cater the case that some ELE has been deleted by the user
 * 
 * 2011-06-29 Ivan
 * - Moved Organization Column in front of the ELE Column
 * 
 * 2010-08-30 Max (201008300909)
 * - Fix for ommited format wrong display
 * 
 * 2010-08-11 Max (201008111412)
 * - Add support for new 2 fields in csv, LoginName and WebSAMSRegNo
 * 
 * 	2010-07-23 Max (201007231411)
 * 	- Bug Fix for getting wrong of SchoolRemarks to DB
 * 
 * 	2010-04-09 Max (201004091545)
 * 	- added checking for the no. of header column and the content column
 * 
 * 	2010-02-19 Max (201002191550)
 * 	- trim intake t_engTitle
 * 	- trim intake t_organization
 * 	- check missing title
 * 	- check Chinese Class Name intake and validate interger class number <=== already checked before
 * 	- insert to db with "ComeFrom" field
 * 	- replace the language in lang file
 * 
 * 	2010-01-26 Max ()
 * 	- Modify function [getAcademicInfoWithTermInfo()] to output with both
 * 	Chinese and English Academic Years and Terms
 * 
 * 	2010-01-21 Max (201001211026)
 * 	- Modify the import to support with SchoolYear and Term
 * 
 * 	2010-01-13 Max (200901131403)
 * 	- Fix import saving wrong ele
 * 	- function [getCovertedIntExt()] and [getConvertedEle()]added
 * 
 *  2009-12-30: Max (200912281012)
 *  - modify code to manipulate with ole_program class

 * Type			: Depression
 * Date 		: 200911201021
 * Description	: 1) Depress the enhancement 200911131142MaxWong, 200911121040MaxWong
 * 						-a) comment out Chinese Input fields in the [New] page
 * 						-bC) fallback the corresponding Download Sample to previous version
 * 						    that is not supporting ChiTitle and ChiDetails only
 * 							and modify back ole_import_update.php to not importing chinese fields
 * 
 * 						    fallback the current version Download Sample student_olr_sample.csv:
 * 						    student_olr_sample.csv.200911201044 -> student_olr_sample.csv.20091113
 * 						
 * 						-c) comment out the selection of English, Chinese and Bilingual for Report Type -> Student Learning Profile
 * 						    in [Student Report Printing] page
 * Case Number	: 200911201021MaxWong
 * C=CurrentIssue
 * ----------------------------------
 * Type			: Enhancement
 * Date 		: 200911131142
 * Description	: 1) Enhance the form to New an OLE with chinese title and chinese details
 * C=CurrentIssue 2C) The import page is required to support chinese title, chinese details and add school remarks
 * Case Number	: 200911131142MaxWong
 * ----------------------------------
 * Type			: Enhancement
 * Description	: Automatically insert a new program before student's record is added or updated.
 * By			: Max Wong
 * Date			: 200911030000
 * ----------------------------------
 * Type			: Enhancement
 * Description	: When dates are not available in input file, set it as null before insert new records to students.
 * By			: Max Wong
 * Date			: 200911040000
 * ----------------------------------
 * Type			: Enhancement
 * Description	: Change the checking of "ELE" field to "ComponentCode"
 * By			: Max Wong
 * Date			: 200911051436
 * 
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/ole_program.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libpf-ole.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once("$intranet_root/includes/portfolio25/oleCategory/libpf-category.php");

# Error Code
define ("ERROR_TYPE_STUDENT_NOT_FOUND", 1);
define ("ERROR_TYPE_DUPLICATED_CLASSNUM", 2);
define ("ERROR_TYPE_PROGRAM_NOT_FOUND", 3);
define ("ERROR_TYPE_DATEFORMAT_NOT_CORRECT", 4);
define ("ERROR_TYPE_ACADEMIC_YEAR_NOT_FOUND", 5);
define ("ERROR_TYPE_TERM_NOT_FOUND", 6);
define ("ERROR_TYPE_EMPTY_TITLE", 7);
define ("ERROR_TYPE_NUMBER_OF_DATA_NOT_MATCH",8);
define ("ERROR_TYPE_CATEGORY_NOT_FOUND",9);
define ("ERROR_TYPE_SUBCATEGORY_NOT_FOUND",10);
define ("ERROR_TYPE_INSIDEOUTSIDE_NOT_FOUND",11);
define ("ERROR_TYPE_ISSAS_NOT_FOUND",12);


unset($error_data);
$count_success = 0;
if ($g_encoding_unicode) {
	$import_coding = ($g_chinese == "") ? "b5" : "gb";
}

# uploaded file information
$filepath = $userfile;
$filename = $userfile_name;

$TmpIntExt = getCovertedIntExt(trim($IntExt));

$uploaderUserId = $UserID;  // variable from session

$count_new = 0;
$count_updated = 0;
$display_content = "";


if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath))
{
	header("Location: ole_import.php?IntExt=$IntExt&?FromPage=$FromPage");
	exit;
}
else
{
	intranet_opendb();
	$lpf = new libpf_slp();
	$lpf_ole = new libpf_ole();

	$lpf->CHECK_ACCESS_IPORTFOLIO();
	//$lpf->ACCESS_CONTROL("ole");

	$CurrentPage = "Teacher_OLE";
	$CurrentPageName = $iPort['menu']['ole'];
	$TAGS_OBJ[] = array($CurrentPageName,"");
	$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

	$linterface = new interface_html();
	$linterface->LAYOUT_START();

	$li = new libdb();
	$lo = new libfilesystem();
	$limport = new libimporttext();
	$libCategory = new Category();

	$ele_list = $lpf->GET_OLE_ELE_LIST();
    $ext = strtoupper($lo->file_ext($filename));


    if ($ext == ".CSV" || $ext == ".TXT")
    {
       $data = $limport->GET_IMPORT_TXT($filepath);
       $header_row = array_shift($data);                   # drop the title bar
    }
	
    # Check Title Row
	//debug_pr($IntExt);
	$file_format = getFileFormat($IntExt);
	//debug_pr($file_format);
    $format_wrong = false;
    for ($i=0; $i<sizeof($file_format); $i++)
    {
         if ($header_row[$i]!=$file_format[$i])
         {
         	
             $format_wrong = true;
             break;
         }
    }
	
    if ($format_wrong)
    {
        $correct_format = "<table border='0' cellpadding='5' cellspacing='0'>\n";
        for ($i=0; $i<sizeof($file_format); $i++)
        {
             $correct_format .= "<tr><td>".$file_format[$i]."</td></tr>\n";
        }
        $correct_format .= "</table>\n";

        $wrong_format = "<table border='0' cellpadding='5' cellspacing='0'>\n";
        for ($i=0; $i<sizeof($header_row); $i++)
        {
			$field_title = ($header_row[$i]!=$file_format[$i]) ? "<u>".$header_row[$i]."</u>" : $header_row[$i];
			$wrong_format .= "<tr><td>".$field_title."</td></tr>\n";
        }
        $wrong_format .= "</table>\n";

        $display_content .= "<br><span class='chi_content_15'>".$ec_guide['import_error_wrong_format']."</span><br>\n";
        $display_content .= "<table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
		$display_content .= "<tr><td valign='top'>$correct_format</td><td width='90%' align='center'>VS</td><td valign='top'>$wrong_format</td></tr>\n";
        $display_content .= "</table>\n";
    }
    else
    {
		//FORMAT IS CORRECT , START THE PROCESS
//		echo "this is the line";die;
		$li->Start_Trans();
		$sqlRes = array();
		
		$ELEKeyArr = array_keys($lpf->GET_ELE());
		$CategoryInfoArr = $libCategory->GET_CATEGORY_LIST($ParShowInactive=false);
		$CategoryIDArr = Get_Array_By_Key($CategoryInfoArr, 'RecordID');
//	debug_pr($data);
		for ($i=0; $i<sizeof($data); $i++)	//	This data get the data from import file
		{
			if($IntExt == 1)
			{
				//list($t_class_name, $t_class_num, $t_login_name, $t_websams_regno, $t_start, $t_end, $t_engTitle, $t_schoolYear, $t_term, $t_engDetails, $t_category, $t_role, $t_achievement, $t_remark, $t_organization, $t_schoolRemarks) = $data[$i];
				list($t_class_name, $t_class_num, $t_login_name, $t_websams_regno, $t_start, $t_end, $t_engTitle, $t_schoolYear, $t_term, $t_engDetails, $t_category,$t_subcategory, $t_role, $t_achievement, $t_remark, $t_organization) = $data[$i];
				if($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']){
					list($t_class_name, $t_class_num, $t_login_name, $t_websams_regno, $t_start, $t_end, $t_engTitle, $t_schoolYear, $t_term, $t_engDetails, $t_category, $t_subcategory, $t_role, $t_achievement, $t_remark, $t_organization, $t_insideOutside, $t_isSAS) = $data[$i];
				}
			
			}
			else
			{
				//list($t_class_name, $t_class_num, $t_login_name, $t_websams_regno, $t_start, $t_end, $t_engTitle, $t_schoolYear, $t_term, $t_engDetails, $t_category, $t_role, $t_hours, $t_achievement, $t_remark, $t_ele, $t_organization, $t_schoolRemarks) = $data[$i];
				list($t_class_name, $t_class_num, $t_login_name, $t_websams_regno, $t_start, $t_end, $t_engTitle, $t_schoolYear, $t_term, $t_engDetails, $t_category, $t_role, $t_hours, $t_achievement, $t_remark, $t_organization, $t_ele) = $data[$i];
			}

			//check date format with "yyyy-mm-dd"
			if(
					checkInputDateIsValid($t_start) == false || 
					(trim($t_end) != "" && checkInputDateIsValid($t_end) == false)  //case for if $t_end is empty , no need to check date format
				)
			{
				$error_data[] = array($i,ERROR_TYPE_DATEFORMAT_NOT_CORRECT,$data[$i]);
				continue;  //skip the insert
			}

			/*
			 * Y	T
			 * 0	0	->current year, all terms
			 * 0	1	->current year, specific term
			 * 1	0	->specific year, all terms
			 * 1	1	->specific year, specific term
			 */			
			# put academic year with current year
			$fcm = new form_class_manage();

			# put academic year with current year in start date
			$currentYearAndYearTerm = getAcademicYearInfoAndTermInfoByDate($t_start);

			if (isset($t_schoolYear) && $t_schoolYear != "") {
				// do nothing
			} else {
				$t_schoolYear = $currentYearAndYearTerm[1];
				$data[$i][7] = $currentYearAndYearTerm[1]; // *Bookmark: note_1 <==Assign the value from DB to data array, must map with corresponding position
			}
			
			$academicInfoWithTermInfo = getAcademicInfoWithTermInfo();
			$academicYearExist = false;
			$termExist = false;

			foreach($academicInfoWithTermInfo as $key => $academicElements) {
//				echo "$t_schoolYear vs ".$academicElements["ACADEMICYEARNAMEEN"]."<br>";
//				echo "$t_schoolYear vs ".$academicElements["ACADEMICYEARNAMEB5"]."<br>";
				if ($t_schoolYear == $academicElements["ACADEMICYEARNAMEEN"] || $t_schoolYear == $academicElements["ACADEMICYEARNAMEB5"]) {
					$academicYearExist = true;
					
					$sizeOfTerm = count($academicElements["YEARTERMINFO"]);
					if (empty($t_term)) {
						$termExist = true;
					} else {
						for($j=0;$j<$sizeOfTerm;$j++) {
							$termElement = $academicElements["YEARTERMINFO"][$j];
							if ($t_term == $termElement["YEARTERMNAMEEN"] || $t_term == $termElement["YEARTERMNAMEB5"]) {
								$termExist = true;
							}
						}
					}
				}
			}

			# check the academic year id
			if ($academicYearExist) {
				// do nothing
			} else {
				$error_data[] = array($i,ERROR_TYPE_ACADEMIC_YEAR_NOT_FOUND,$data[$i]);
				continue;
			}
			# check the term id			
			if ($termExist) {
				// do nothing
			} else {
				$error_data[] = array($i,ERROR_TYPE_TERM_NOT_FOUND,$data[$i]);
				continue;
			}

			# check the header column and the content column number
			if (count($header_row) != count($data[$i])) {
				$error_data[] = array($i,ERROR_TYPE_NUMBER_OF_DATA_NOT_MATCH,$data[$i]);
				continue;
			} else {
				# This getProgramID insert a new record to OLE_PROGRAM if a program does not exist
//				$t_programID = getProgramID($header_row, $data[$i], $uploaderUserId,$TmpIntExt,$ELEKeyArr,$li);
				$t_programID = getProgramID($header_row, $data[$i], $uploaderUserId,$TmpIntExt,$ele_list,$li);

			}
			
			if($t_programID <= 0)
			{
				//CANNOT FIND THE RELATED PROGRAM ID IN THE OLE PROGRAM , SKIP / STOP TO INSERT STUDENT RECORD TO OLE_STUDENT
				$error_data[] = array($i,ERROR_TYPE_PROGRAM_NOT_FOUND,$data[$i]);
				continue;
			}
			
			# check empty title
			if (empty($t_engTitle)) {
				$error_data[] = array($i,ERROR_TYPE_EMPTY_TITLE,$data[$i]);
				continue;
			} 
			
			# check OLE Category exist or not
			$t_category = trim($t_category);
			if ($t_category=='' || !in_array($t_category, (array)$CategoryIDArr)) {
				$error_data[] = array($i,ERROR_TYPE_CATEGORY_NOT_FOUND,$data[$i]);
				continue;
			}
			## check subcategory matches category
			if($IntExt == 1){
				$subcategoryInfo = $libCategory -> GET_SUBCATEGORY_LIST(false,'',$t_category);
				$subCategoryIDArr = Get_Array_By_Key($subcategoryInfo, 'SubCatID');
				
				$t_subcategory = trim($t_subcategory);
				if($t_subcategory=''||!in_array($t_subcategory,(array)$subCategoryIDArr)){
					$error_data[] = array($i,ERROR_TYPE_SUBCATEGORY_NOT_FOUND,$data[$i]);
					continue;
				}
			}
			
			if($IntExt == 1&&$sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']){
				## CHECK ISOUTSIDE & toIsOutSideSchool
				if($t_insideOutside!='I'&&$t_insideOutside!='O'&&$t_insideOutside!='i'&&$t_insideOutside!='o'){
					$error_data[] = array($i,ERROR_TYPE_INSIDEOUTSIDE_NOT_FOUND,$data[$i]);
					continue;
				}
				if($t_insideOutside=='I'||$t_insideOutside=='i'){
					$t_insideOutside = 0;
				}else{
					$t_insideOutside = 1;	//isOutsideSchool in db
				}
			
				## CHECK ISSAS!!!
				if($t_isSAS!='Y'&&$t_isSAS!='y'&&$t_isSAS!='N'&&$t_isSAS!='n'&&$t_isSAS!='1'&&$t_isSAS!='0'){
					$error_data[] = array($i,ERROR_TYPE_ISSAS_NOT_FOUND,$data[$i]);
					continue;
				}
				if($t_isSAS=='Y'&&$t_isSAS=='y'&&$t_isSAS=='1'){
					$t_isSAS = 1;
				}else{
					$t_isSAS = 0;
				}
			
			}
			
			


			$t_class_name = trim($t_class_name);
			$t_class_num = trim($t_class_num);
			
			//	if both the classname and the classnum do not exist, go to check the login name and then the websams
            if ( ($t_class_name != "" && $t_class_num != "") 
            		|| ($t_class_name == "" && $t_class_num == "" && $t_login_name != "") 
            		|| ($t_class_name == "" && $t_class_num == "" && $t_websams_regno !="") )
            {
            	if ($t_class_name != "" && $t_class_num != "") {
            		$cond = "
							AND ClassName = '$t_class_name'
							AND (ClassNumber = '$t_class_num' OR CONCAT('0',ClassNumber) = '$t_class_num' OR ClassNumber = '0".$t_class_num."')";
            	} else if ($t_login_name != "") {
            		$cond = " AND UserLogin = '$t_login_name' ";
            	} else if ($t_websams_regno !="") {
            		$cond = " AND WebSAMSRegNo = '$t_websams_regno' ";
            	}
				# get UserID
				$sql = "SELECT DISTINCT
							UserID
						FROM
							INTRANET_USER
						WHERE
							RecordType = '2'
							AND RecordStatus IN (1,2)
							$cond
					";
				$UserArr = $li->returnVector($sql);
				if (sizeof($UserArr)>0)
                {
					if(sizeof($UserArr)>1)
					{
						$error_data[] = array($i,ERROR_TYPE_DUPLICATED_CLASSNUM,$data[$i]);
						continue;
					}

					
					# handle ele data
					$ELEList = getConvertedEle($t_ele);

					$ole_UserId = addslashes($UserArr[0]);
					$t_engTitle = trim(addslashes($t_engTitle));
					$t_category = trim(addslashes($t_category));
					$t_organization = trim(addslashes($t_organization));
					$t_role = trim(addslashes($t_role));
					$t_hours = trim(addslashes($t_hours));
					$t_achievement = trim(addslashes($t_achievement));
					$t_engDetails = trim(addslashes($t_engDetails));
					
					$t_start = trim(addslashes($t_start));
					$t_end = trim(addslashes($t_end));
					$t_remark = trim(addslashes($t_remark));
					
					$TmpIntExt = trim(addslashes($TmpIntExt));
					$t_start = trim(addslashes($t_start));
					$t_start = trim(addslashes($t_start));
					$t_start = trim(addslashes($t_start));
									
					$fields = getDbFields($IntExt);
					####################################
					#
					#	ProgramID must be appended at last	
					#
					####################################
					if ($t_start == "") {
						$t_start = 'null';
					} else {
						$t_start = "'" . $t_start . "'";
					}
					if ($t_end == "") {
						$t_end = 'null';
					} else {
						$t_end = "'" . $t_end . "'";
					}
					
					if($IntExt == 1) {	#ProgramID must be appended at last
						$values = "'$ole_UserId', '$t_engTitle', '$t_category', '$t_organization','$t_role', '$t_achievement', '$t_engDetails', $t_start, $t_end, '$ck_intranet_user_id', '$t_remark', '4', now(), now(), '$TmpIntExt', '$t_programID', " . $ipf_cfg["OLE_PROGRAM_COMEFROM"]["teacherImport"];
					}
					else {	#ProgramID must be appended at last
						$values = "'$ole_UserId', '$t_engTitle', '$t_category', '$ELEList', '$t_organization','$t_role', '$t_hours', '$t_achievement', '$t_engDetails', $t_start, $t_end, '$ck_intranet_user_id', '$t_remark', '4', now(), now(), '$TmpIntExt', '$t_programID', " . $ipf_cfg["OLE_PROGRAM_COMEFROM"]["teacherImport"];
					}
					
					$sql = "INSERT INTO {$eclass_db}.OLE_STUDENT ($fields) VALUES ($values) ";
					$q_result = $li->db_db_query($sql);
					$sqlRes[] = $q_result;

					if ($q_result) {
						$count_success++;
					}
                 }
				 else
                 {
                 	 $sqlRes[] = false;	//	When any user cannot be found, put false to the sqlRes to trigger a rollback
                     $error_data[] = array($i,ERROR_TYPE_STUDENT_NOT_FOUND, $data[$i]);
                 }
			} else {
                 	 $sqlRes[] = false;	//	When classname / classno is empty, put false to the sqlRes to trigger a rollback
                 	 $error_data[] = array($i,ERROR_TYPE_STUDENT_NOT_FOUND, $data[$i]);
			}
		}
		
		//if (in_array(false,$sqlRes)) {
		if (in_array(false,$sqlRes) || count((array)$error_data) > 0) {
			$li->RollBack_Trans();
		} else {
			$li->Commit_Trans();
		}
	}
}




# Display import stats
if ($format_wrong) {
	// do nothing
} else if (!$format_wrong && !in_array(false,$sqlRes))
{
	$display_content = "<div class='tabletext' style='text-align: center'>".$ec_guide['import_update_no']." : <b>$count_success</b></div><br>";
} else {
	$display_content = "<div class='tabletext' style='text-align: center'>{$ec_iPortfolio['NoRecordsUpdated']}</div><br>";
}

if (sizeof($error_data)>0)
{
    $error_table = "<br><table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
    $error_table .= "<tr class=tableTitle bgcolor='#CFE6FE'><td>".$ec_guide['import_error_row']."</td><td>".$ec_guide['import_error_reason']."</td><td>".$ec_guide['import_error_detail']."</td></tr>\n";

    for ($i=0; $i<sizeof($error_data); $i++)
    {
         list ($t_row, $t_type, $t_data) = $error_data[$i];
         $t_row++;     # set first row to 1
         $css_color = ($i%2==0) ? "#FFFFFF" : "#F3F3F3";
         $error_table .= "<tr bgcolor='$css_color'><td class='tabletext'>$t_row</td><td class='tabletext'>";
         $reason_string = "Unknown";
         switch ($t_type)
         {
                 case ERROR_TYPE_STUDENT_NOT_FOUND:
                      $reason_string = $ec_guide['import_error_no_user'];
                      break;
				case ERROR_TYPE_DUPLICATED_CLASSNUM:
                      $reason_string = $ec_guide['import_error_duplicate_classnum'];
                      break;
			    case ERROR_TYPE_PROGRAM_NOT_FOUND:
					  $reason_string = $ec_guide['import_programid_not_found'];	
					  break;
				case ERROR_TYPE_DATEFORMAT_NOT_CORRECT:
					  $reason_string = $ec_guide['import_dateformat_not_correct'];	
					  break;
				case ERROR_TYPE_ACADEMIC_YEAR_NOT_FOUND:
					  $reason_string = $ec_guide['import_academic_year_not_found'];	
					  break;
				case ERROR_TYPE_TERM_NOT_FOUND:
					  $reason_string = $ec_guide['import_type_term_not_found'];	
					  break;
				case ERROR_TYPE_EMPTY_TITLE:
					$reason_string = $ec_guide['import_empty_title'];
					break;
				case ERROR_TYPE_NUMBER_OF_DATA_NOT_MATCH:
					$reason_string = $ec_guide['import_header_content_column_not_match'];
					break;
				case ERROR_TYPE_CATEGORY_NOT_FOUND:
					$reason_string = $ec_guide['import_ole_category_not_found'];
					break;
				case ERROR_TYPE_SUBCATEGORY_NOT_FOUND:
					$reason_string = $ec_guide['import_ole_subcategory_not_found'];
					break;
				case ERROR_TYPE_INSIDEOUTSIDE_NOT_FOUND:
					$reason_string = $ec_guide['import_ole_insideoutside_not_found'];
					break;
				case ERROR_TYPE_ISSAS_NOT_FOUND:
					$reason_string = $ec_guide['import_ole_isSAS_not_found'];
					break;
				default:
                     $reason_string = $ec_guide['import_error_unknown'];
                     break;
         }
         $error_table .= $reason_string;
         $error_table .= "</td><td class='tabletext'>".implode(",",$t_data)."</td></tr>\n";
    }
    $error_table .= "</table>\n";
    $display_content .= $error_table;
}
?>
	<table align="left" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
		<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" border="0" align="absmiddle"> <?=$button_import?> </a>
		</td>
		<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
	</tr>
	</table>
	
	<br />
	
<FORM enctype="multipart/form-data" method="POST" name="form1">
	<br />
	<?= $display_content ?>
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	<p>
	<?= $linterface->GET_ACTION_BTN($ec_guide['import_back'], "button", "self.location='ole_import.php?IntExt=$IntExt&FromPage=$FromPage';") ?>
	<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='".($FromPage=="sview"?"ole_studentview.php":"ole.php?IntExt=$IntExt")."'") ?>
	</p>
</FORM>

<?php
/* 200911030000MaxWong */
////////////////
//  FUNCTION ///
////////////////
function getCovertedIntExt($ParIntExt=0) {
	if ($ParIntExt == 0) {
		return "INT";
	} else if ($ParIntExt == 1) {
		return "EXT";
	}
	return null;
}

// Commented by Ivan (20110902) [2011-0826-0949-55066]
//function getConvertedEle($ParImportEle) {
//	$lpf = new libpf_slp();
//	$ELEKeyArr = array_keys($lpf->GET_ELE());
//	
//	$TmpELEArr = explode(";", $ParImportEle);
//	$ParImportEle = "";
//	for($k=0; $k<sizeof($TmpELEArr); $k++)
//	{
//		$pos = $TmpELEArr[$k]-1;
//		$ParImportEle .= ($ParImportEle=="") ? $ELEKeyArr[$pos] : ($ELEKeyArr[$pos]=="" ? "" : ",".$ELEKeyArr[$pos]);
//	}
//	return $ParImportEle;
//}
function getConvertedEle($ParImportEle) {
	global $ipf_cfg;
	
	$lpf_ole = new libpf_ole();
	
	$ELEArray = $lpf_ole->Get_ELE_Info(array($ipf_cfg["OLE_ELE_RecordStatus"]["Public"], $ipf_cfg["OLE_ELE_RecordStatus"]["DefaultAndPublic"]));
	$ELEAssoArr = BuildMultiKeyAssoc($ELEArray, 'RecordID');

	$ELE_IDArr = explode(";", $ParImportEle);
	$numOfELE = count($ELE_IDArr);
	$ELE_CodeArr = array();
	for ($k=0; $k<$numOfELE; $k++) {
		$thisELE_ID = $ELE_IDArr[$k];
		$thisELE_Code = $ELEAssoArr[$thisELE_ID]['ELECode'];
		
		if ($thisELE_Code != '') {
			$ELE_CodeArr[] = $thisELE_Code;
		}
	}
	$ELE_CodeText = implode(',', (array)$ELE_CodeArr);
	
	return $ELE_CodeText;
}

function getAcademicInfoWithTermInfo() {
		global $intranet_db, $li, $intranet_session_language;

	$sql = "
SELECT
	AY.ACADEMICYEARID,
	AY.YEARNAMEEN,
	AY.YEARNAMEB5,
	AYT.YEARTERMID,
	AYT.YEARTERMNAMEEN,
	AYT.YEARTERMNAMEB5
FROM
	" . $intranet_db . ".ACADEMIC_YEAR AY
	LEFT JOIN " . $intranet_db . ".ACADEMIC_YEAR_TERM AYT
		ON AY.ACADEMICYEARID = AYT.ACADEMICYEARID
			";
	$result = $li->returnArray($sql);
	$returnArray = array();
	$sizeOfResult = count($result);
	for($i=0; $i<$sizeOfResult; $i++) {
		if (isset($result[$i]["ACADEMICYEARID"]) && $result[$i]["ACADEMICYEARID"] != "") {
			$yearTermArray = array("YEARTERMID"=>$result[$i]["YEARTERMID"],
								"YEARTERMNAMEEN"=>$result[$i]["YEARTERMNAMEEN"],
								"YEARTERMNAMEB5"=>$result[$i]["YEARTERMNAMEB5"]);
			
			$returnArray[$result[$i]["ACADEMICYEARID"]]["ACADEMICYEARNAMEEN"] = $result[$i]["YEARNAMEEN"];
			$returnArray[$result[$i]["ACADEMICYEARID"]]["ACADEMICYEARNAMEB5"] = $result[$i]["YEARNAMEB5"];
			$returnArray[$result[$i]["ACADEMICYEARID"]]["YEARTERMINFO"][] = $yearTermArray;
		}
	}
	return $returnArray;
}
function getAcademicYearIdByName($ParAcademicYearName="") {
	global $intranet_db, $li;

	$sql = "
SELECT
	ACADEMICYEARID
FROM
	" . $intranet_db . ".ACADEMIC_YEAR
WHERE
	YEARNAMEEN = '" . $ParAcademicYearName . "'
	OR
	YEARNAMEB5 = '" . $ParAcademicYearName . "'
			";
	$result = $li->returnArray($sql);
	return $result[0]["ACADEMICYEARID"];
}
function getTermIdByAcademicYearAndName($ParAcademicYearName="",$ParTermName="") {
		global $intranet_db, $li;

	$sql = "
SELECT
	AY.ACADEMICYEARID,
	AYT.YEARTERMID
FROM
	" . $intranet_db . ".ACADEMIC_YEAR AY
	LEFT JOIN " . $intranet_db . ".ACADEMIC_YEAR_TERM AYT
		ON AY.ACADEMICYEARID = AYT.ACADEMICYEARID
WHERE
	(AY.YEARNAMEEN = '" . $ParAcademicYearName . "'
	OR
	AY.YEARNAMEB5 = '" . $ParAcademicYearName . "')
	AND
	(AYT.YEARTERMNAMEEN = '" . $ParTermName . "'
	OR
	AYT.YEARTERMNAMEB5 = '" . $ParTermName . "'
	)
			";
	$result = $li->returnArray($sql);
	return $result[0]["YEARTERMID"];
}
function getProgramID($dataHeaderArr="", $dataDetailArr="",$creatorID , $intExtType = "", $eleConfigAry="",$db="") {
	global $eclass_db;
	global $ipf_cfg;
	if (isset($db) && $db != "") {
	} else {
		$db = new libdb;
	}

	$DataArray = array_combine($dataHeaderArr, $dataDetailArr);
	//debug_pr($DataArray);
	$academicYearId = getAcademicYearIdByName($DataArray["SchoolYear"]);
	$yearTermId = getTermIdByAcademicYearAndName($DataArray["SchoolYear"], $DataArray["Term"]);

	$returnVal = 0;
	$t_ele = $DataArray["ComponentCode"];

	if ($DataArray['StartDate'] != "")
	{
		$Cond .= " AND StartDate='".$DataArray['StartDate']."' ";
	}
	if ($DataArray['EndDate'] != "")
	{
		$Cond .= " AND EndDate='".$DataArray['EndDate']."' ";
	}
	if ($DataArray["Category"] != "")
	{
		$Cond .= " AND Category='".addslashes($DataArray["Category"])."' ";
	}
	
	if($t_ele == ''){
			$componentCond = ' and ELE = \''.$t_ele .'\'';
	}else{
		$inputElE = array();
		$inputELEStr  = '';
		$_userInputEle = explode(';',$t_ele);
		$_userInputEle = array_filter($_userInputEle);
		
		foreach($eleConfigAry as $eleItems => $eleDetails){
			$_eleID = $eleDetails['RecordID'];

			if(in_array($_eleID, $_userInputEle)){
				//if this setting ELE in the user input ELE , record this DefaultID

				//for user self ele , the defaultid is a integer format , not [OTHER] , with "[", so append to it for DB checking
				$inputElE[] = (is_numeric($eleDetails['DefaultID']))? '['.$eleDetails['DefaultID'].']': $eleDetails['DefaultID'];
			}
		}
		

		if(count($inputElE) > 0){
			$inputELEStr = implode(',',$inputElE);
		}
//error_log("111--->".$inputELEStr."\n", 3, "/tmp/aaa.txt");
		if(trim($inputELEStr) != ''){
			$componentCond = ' and ELE = \''.$inputELEStr.'\'';
		}
		
	}
//error_log("111--->".$inputELEStr." 	componentCond  ===>".$componentCond."\n", 3, "/tmp/aaa.txt");
	$intExtCond = " AND IntExt = '".$intExtType."' ";
// $academicYearId = getAcademicYearIdByName($DataArray["SchoolYear"]);
// 	$yearTermId = getTermIdByAcademicYearAndName($DataArray["SchoolYear"], $DataArray["Term"]);
	$Sql = 	"SELECT
					ProgramID
					FROM
						{$eclass_db}.OLE_PROGRAM
					WHERE
						Title = '".addslashes($DataArray['Title'])."'
						$Cond
						$componentCond
						$intExtCond
						and AcademicYearID=$academicYearId 
				";
	if($yearTermId)
		$Sql .= " and YearTermID=$yearTermId ";
//error_log($Sql."\n", 3, "/tmp/aaa.txt");

	$ReturnArr1 = $db->returnVector($Sql);
	if (count($ReturnArr1) > 0)
	{
		//BECAREFULL, IF THERE IS MORE THAN ONE TITLE WITH THE SAME STARTDATE AND END DATE, IT MAY HAVE ERROR. 
		//NOW IS RETURN THE FISRT OCCURANCE RECORD

//error_log("return old one ".$ReturnArr1[0]."\n", 3, "/tmp/aaa.txt");
		return $ReturnArr1[0];
	}
	else
	{

		if($DataArray["Inside / Outside School"]=='O'||$DataArray["Inside / Outside School"]=='o'){
			$isOutsideSchool = 1;
		}else{
			$isOutsideSchool = 0;
		}

		if($DataArray["Belong to SAS"]=='Y'||$DataArray["Belong to SAS"]=='y'||$DataArray["Belong to SAS"]=='1'){
			$isSAS = 1;
		}else{
			$isSAS = 0;
		}

		$ELEList = getConvertedEle($t_ele);
//error_log("converted ".$ELEList."\n", 3, "/tmp/aaa.txt");
		# insert a new OLE_PROGRAM record
		$objOLEPROGRAM = new ole_program();
		
		$objOLEPROGRAM->setProgramType($ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]);
		$objOLEPROGRAM->setTitle(addslashes($DataArray["Title"]));
		$objOLEPROGRAM->setStartDate($DataArray["StartDate"]);
		$objOLEPROGRAM->setEndDate($DataArray["EndDate"]);
		$objOLEPROGRAM->setCategory(addslashes($DataArray["Category"]));
		$objOLEPROGRAM->setSubCategoryID(addslashes($DataArray["Subcategory"]));
		$objOLEPROGRAM->setOrganization(addslashes($DataArray["Organization"]));
		$objOLEPROGRAM->setDetails(addslashes($DataArray["Details"]));
		$objOLEPROGRAM->setSchoolRemarks(addslashes($DataArray["ProgramRemarks"]));
		$objOLEPROGRAM->setELE($inputELEStr);
		// InputDate default = now()
		// ModifiedDate default = now()
		$objOLEPROGRAM->setCanJoinStartDate("null"); // remember to set a null value if not use canJoin
		$objOLEPROGRAM->setCanJoinEndDate("null"); // remember to set a null value if not use canJoin
		$objOLEPROGRAM->setIntExt($intExtType);
		$objOLEPROGRAM->setAcademicYearID($academicYearId);
		$objOLEPROGRAM->setYearTermID($yearTermId);
		$objOLEPROGRAM->setCreatorID($creatorID);
		$objOLEPROGRAM->setComeFrom($ipf_cfg["OLE_PROGRAM_COMEFROM"]["teacherImport"]);
		$objOLEPROGRAM->setIsOutsideSchool($isOutsideSchool);
		$objOLEPROGRAM->setIsSAS($isSAS);
		$returnVal = $objOLEPROGRAM->SaveProgram();
//error_log("return new ".$returnVal."\n", 3, "/tmp/aaa.txt");

	}

	//PROGRAM ID MUST BE A INTEGER 
	return intval($returnVal);
}

function getDbFields($ParIntExt) {
	global $sys_custom;
	define("DBF_USERID", "UserID");
	define("DBF_TITLE", "Title");
	define("DBF_CATEGORY", "Category");
	define("DBF_ELE", "ELE");
	define("DBF_ORGANIZATION", "Organization");
	define("DBF_ROLE", "Role");
	define("DBF_HOURS", "Hours");
	define("DBF_ACHIEVEMENT", "Achievement");
	define("DBF_DETAILS", "Details");
	define("DBF_STARTDATE", "StartDate");
	define("DBF_ENDDATE", "EndDate");
	define("DBF_APPROVEDBY", "ApprovedBy");
	define("DBF_REMARK", "Remark");
	define("DBF_RECORDSTATUS", "RecordStatus");
	define("DBF_MODIFIEDDATE", "ModifiedDate");
	define("DBF_INPUTDATE", "InputDate");
	define("DBF_INTEXT", "IntExt");
	define("DBF_PROGRAMID", "ProgramID");
	define("DBF_COMEFROM", "ComeFrom");

	
	$fields = array();
	array_push($fields, DBF_USERID);
	array_push($fields, DBF_TITLE);
	array_push($fields, DBF_CATEGORY);

	if ($ParIntExt == 0) array_push($fields, DBF_ELE);
	array_push($fields, DBF_ORGANIZATION);
	array_push($fields, DBF_ROLE);
	if ($ParIntExt == 0) array_push($fields, DBF_HOURS);
	array_push($fields, DBF_ACHIEVEMENT);
  	array_push($fields, DBF_DETAILS);
	array_push($fields, DBF_STARTDATE);
	array_push($fields, DBF_ENDDATE);
	array_push($fields, DBF_APPROVEDBY);
	array_push($fields, DBF_REMARK);
	array_push($fields, DBF_RECORDSTATUS);
	array_push($fields, DBF_MODIFIEDDATE);
	array_push($fields, DBF_INPUTDATE);
	array_push($fields, DBF_INTEXT);

	array_push($fields, DBF_PROGRAMID);
	array_push($fields, DBF_COMEFROM);
	
	
	return implode(",",$fields);
}
function getFileFormat($ParIntExt)
{
	global $sys_custom;
    define("CSV_CLASSNAME", "ClassName");
    define("CSV_CLASSNUMBER", "ClassNumber");
    
    define("CSV_LOGINNAME", "LoginName");
    define("CSV_WEBSAMSREGNO", "WebSAMSRegNo");
    
    define("CSV_STARTDATE", "StartDate");
    define("CSV_ENDDATE", "EndDate");
    define("CSV_TITLE", "Title");
    define("CSV_SCHOOLYEAR", "SchoolYear");
    define("CSV_TERM", "Term");
    define("CSV_DETAILS", "Details");
    define("CSV_CATEGORY", "Category");
    define("CSV_ROLE", "Role");
    define("CSV_HOURS", "Hours");
    define("CSV_ACHIEVEMENT", "Achievement");
    define("CSV_PROGRAMREMARKS", "ProgramRemarks");
    define("CSV_COMPONENTCODE", "ComponentCode");
    define("CSV_ORGANIZATION", "Organization");
    if($ParIntExt==1){
    	define("CSV_SUBCATEGORY", "Subcategory");
    }
        	
    if($ParIntExt==1&&$sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']){

    	define("CSV_INSIDEOUTSIDESCHOOL", "Inside / Outside School");
    	define("CSV_BELONGTOSAS", "Belong to SAS");
    }
    //2016-02-09
    
    $file_format = array();
    array_push($file_format, CSV_CLASSNAME);
    array_push($file_format, CSV_CLASSNUMBER);
    
    array_push($file_format, CSV_LOGINNAME);
    array_push($file_format, CSV_WEBSAMSREGNO);
    
    array_push($file_format, CSV_STARTDATE);
    array_push($file_format, CSV_ENDDATE);
    array_push($file_format, CSV_TITLE);
    array_push($file_format, CSV_SCHOOLYEAR);
    array_push($file_format, CSV_TERM);
    array_push($file_format, CSV_DETAILS);
    array_push($file_format, CSV_CATEGORY);
    if($ParIntExt==1){
    	array_push($file_format, CSV_SUBCATEGORY);
    }
    array_push($file_format, CSV_ROLE);
    if ($ParIntExt == 0) array_push($file_format, CSV_HOURS);
    array_push($file_format, CSV_ACHIEVEMENT);
    array_push($file_format, CSV_PROGRAMREMARKS);
    array_push($file_format, CSV_ORGANIZATION);
    if($ParIntExt==1&&$sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']){
    	array_push($file_format, CSV_INSIDEOUTSIDESCHOOL);
    	array_push($file_format, CSV_BELONGTOSAS);
    }
    if ($ParIntExt == 0) array_push($file_format, CSV_COMPONENTCODE);

	return $file_format;
}

?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
//////////function ///////////
/*
function checkDateFormatIsValid($dateInput)
{
	//date format should be match with yyyy-mm-dd
	if (preg_match("/[0-9]{4}-[0-9]{2}-[0-9]{2}/", $dateInput)) {
		return true;
	}
	else
	{
		return false;
	}
}
*/
function checkInputDateIsValid($date){
		//match the format of the date
if (preg_match ("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $date, $parts))
{
			//check weather the date is valid of not
	if(checkdate($parts[2],$parts[3],$parts[1]))
		return true;
	else
		return false;
	}
	else
	{
		return false;
	}
}
?>