<?php
## Modifying By: Max
####################################
##	Modification Log:
##	2010-02-25 Max	(201002181147)
##	- add filter for cnecc sunshine customize
####################################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-mgmt-group.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libpf-OLETeacherRecord.php");

intranet_auth();
intranet_opendb();

$task = trim($task);
$task = ($task == "")? "listOLE": $task;

$lpf = new libpf_slp();
$linterface = new interface_html();
$CurrentPage = "Teacher_OLE";

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$lpf->ADMIN_ACCESS_PAGE();

//$TAGS_OBJ = array(array($Lang['iPortfolio']['OEA']['JUPAS']));
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

switch($task){
  case "ajax":
    $task_script = "../ajax/" . str_replace("../", "", $script) . ".php";

		// Task exists?
		if (file_exists($task_script)) {
			include_once($task_script);
		}
		else {
			$linterface->LAYOUT_START();
			echo "error! task not find<br/>";
			$linterface->LAYOUT_STOP();
		}
		break;
		
	default:
		// By default, open the script with the same name as the task (after appended with ".php").
		// Prevent searching parent directories, like 'task=../../index'.
		//$task_script = "task/" . str_replace("../", "", $task) . ".php";
		$task_script = "task/" . $task . ".php";
		$template_script = "template/" . $task . ".tmpl.php";
		
		// Task exists?
		if (file_exists($task_script)) {
			include_once($task_script);
		}
		else {
			$linterface->LAYOUT_START();
			echo "error! task not find<br/>";
			$linterface->LAYOUT_STOP();
		}
		break;
			
}
intranet_closedb();
exit();
?>