<?php 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");

intranet_auth();
intranet_opendb();

$action = $_SESSION["SESS_OLE_ACTION"];
$isReadOnly = ($action == $ipf_cfg["OLE_ACTION"]["view"]) ? 1 : 0 ;

$ldb = new libdb();
$lpf = new libpf_slp();
$luser = new libuser();
$lfs = new libfilesystem();
$laccount = new libaccountmgmt();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

########################################################
# HIDE COL FOR SIS 20140319 TEMPLATE SETTINGS
########################################################

$linterface = new interface_html();
// set the current page title
$CurrentPage = ($isReadOnly) ? "Teacher_OLEView" : "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

#step
// $STEPS_OBJ[] = array($Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][0], 0);
// $STEPS_OBJ[] = array($Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][1], 0);
// $STEPS_OBJ[] = array($Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][2], 1);
// $StepObj = $linterface->GET_STEPS($STEPS_OBJ);
$stepAry[] = $Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][0];
$stepAry[] = $Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][1];
$stepAry[] = $Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][2];
$StepObj = $linterface->GET_IMPORT_STEPS($CurrStep=3, $stepAry);

$TabIndex = empty($IntExt) ? IPF_CFG_SLP_MGMT_OLE : IPF_CFG_SLP_MGMT_OUTSIDE;

$TabMenuArr = libpf_tabmenu::getSlpMgmtTabs($TabIndex, 0);
### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags($TabIndex);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$data = $lpf->RETURN_OLE_PROGRAM_BY_PROGRAMID_MAX($EventID);
if (is_array($data)) {
	$eng_title_html = $data['Title'];
}

#nagigation
$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['program_list'], "ole.php?IntExt=".$IntExt);
$MenuArr[] = array($eng_title_html, "ole_event_studentview.php?IntExt=".$IntExt."&EventID=".$EventID."&Year=".$Year."&FromPage=program");
$MenuArr[] = array($Lang['iPortfolio']['OLE']['UploadButton'], "");
$navigation_html = $linterface->GET_NAVIGATION($MenuArr);

# gen button
if(count($tmpfilelist) !== $NoOfUploadSuccess){
	$Disabled=1;
}
$donebtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "window.location ='ole_event_studentview.php?EventID=$EventID'");

// # upload file handling start
// $filename = $_FILES["UploadFile"]["name"];
// $tmpfilepath = $_FILES["UploadFile"]["tmp_name"];
// $fileext = $lfs->file_ext($filename);

$tmpfilelist= explode(",",$_POST['tmpfile']);
$RecordIDAry = explode(",",$_POST['RecordID']);


$ole_portfolio_path= $eclass_root."/files/portfolio/ole";

//  debug_Pr($tmpfilelist);
//  debug_pr($RecordIDAry);
//************* copy 
$lfs->set_subfolder_recursive(false);
	$NoOfUploadSuccess = 0;
	for($i=0; $i< count($tmpfilelist); $i++)
	{
		$thisUserInfo = '';
		if(is_dir($tmpfilelist[$i]) == 1){
			$tmpfilelistAry[$RecordIDAry[$i]] = $lfs->return_folderlist($tmpfilelist[$i],true);
		}else{
			$tmpfilelistAry[$RecordIDAry[$i]] = array($tmpfilelist[$i]);
		}
		
		if(file_exists($ole_portfolio_path.'/r'.$RecordIDAry[$i]))
		{
			//$lfs->folder_remove_recursive($ole_portfolio_path.'/r'.$RecordIDAry[$i]);
			// 			$lfs->item_remove($ole_portfolio_path.'/r'.$RecordIDAry[$i]);
			
		    shell_exec('rm -rf \''.OsCommandSafe($ole_portfolio_path).'/r'.OsCommandSafe($RecordIDAry[$i]).'\'');
		}
		
		$lfs->folder_new($ole_portfolio_path.'/r'.$RecordIDAry[$i]);
	}
	
	//debug_pr($tmpfilelist);
// 	debug_pr($tmpfilelistAry);
	//exit;
	for($j=0;$j<count($RecordIDAry);$j++){
		$UploadSuccess = false;
		$autoname = md5(uniqid().time().$_SESSION['UserID']);
		$target_attachment_path =$ole_portfolio_path.'/r'.$RecordIDAry[$j].'/'.$autoname;

		$lfs->folder_new($target_attachment_path);
		
		for($k=0;$k<count($tmpfilelistAry[$RecordIDAry[$j]]);$k++){
// 			if($lfs->lfs_copy($tmpfilelistAry[$RecordIDAry[$j]][$k],$target_attachment_path.'/'.$lfs->get_file_basename($tmpfilelistAry[$RecordIDAry[$j]][$k])))
			if($lfs->lfs_copy($tmpfilelistAry[$RecordIDAry[$j]][$k],$target_attachment_path))
				{
// 				echo 'exist? '.(file_exists($target_attachment_path.'/'.$lfs->get_file_basename($tmpfilelistAry[$RecordIDAry[$j]][$k]))?'y':'n');
				$UploadSuccess = true;
				
				$filename[$RecordIDAry[$j]][] =$autoname.'/'.basename($tmpfilelistAry[$RecordIDAry[$j]][$k]);
			}
		}
		$NoOfUploadSuccess++;
	}
	//debug_pr($filename);
	
	for($j=0;$j<count($filename);$j++){
		$filenameList = implode(":",$filename[$RecordIDAry[$j]]);
		$ole_file_sql ="'$filenameList'";
		$sql = "UPDATE
					{$eclass_db}.OLE_STUDENT
				SET
					Attachment=$ole_file_sql
				WHERE
					RecordID = '$RecordIDAry[$j]'
				";
		$lpf->db_db_query($sql);
//		debug_pr($sql);
	}	


$linterface->LAYOUT_START();
?>

<br>
<form name="form1" id="form1" method="POST">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><?=$navigation_html?></td>
					</tr>
				</table>
			</td>
			<td align="right"></td>
		</tr>
	</table>
		<br>
		
	<table width="100%"  border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<?=$StepObj?>
				</td>
			</tr>
	</table>
	<br>
	
	<div class="table_board" tyle="width:90%;margin:0 auto;"> 
		<table width="100%">
			<tr><td style="text-align:center;">
				<span id="NumOfProcessedPageSpan"><?=$NoOfUploadSuccess?></span> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?>
			</td></tr>
		</table>
		<?=$htmlAry['iframe']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
	
 <div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$donebtn?>
		<p class="spacer"></p>
	</div>
</form>
<?	
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
