<?php
# Under Development : 
## Using By :  
##########################################################
## Modification Log
##
##	2016-05-23 Henry HM	[2016-0415-1449-22214]
##	- display default hours
##
##	2015-05-12 Bill	[2015-0324-1420-39164]
##	- Add Field "Teacher-in-charge"
##	- Hide "Allow Participation Reporting", "Map to OEA", "Partner Organizations", "Details / Name of Activities", "Student Remark"
##
##	2015-05-11 Bill	[Defect List #2866]
##	- ensure fields display "--" if value is empty
##
##	2015-03-04 Omas
##	Pass Parameter to edit button &IntExt=$IntExt
##
##	2015-01-12 Omas	
##	Add edit button
##
## 2010-02-23: Max (201002181147)
## - Customize for CNECC
##
## 2010-01-20: Max (201001201158)
## - Remove the button for Assign Student when the program canJoin = false
##
## 2009-12-30: Max (200912281012)
## - add display of sub category
##
## 
## 2009-12-08: Max (200912081532)
## -Fix Case: retrieving data from function PROGRAM_BY_PROGRAMID_MAX
##########################################################
/**
 * Type			: Enhancement
 * Date 		: 200911230911
 * Description	: Teacher role
 * 				  1C) Add fields [Allow student to join], [Join period]
 * 				  Student role
 * 				  2) Allow students to see the list of Student Learning Profile(SLP) to join, show within editable period and type == 1
 * By			: Max Wong
 * Case Number	: 200911230911MaxWong
 * C=CurrentIssue 
 * ----------------------------------
 * Type			: Depression
 * Date 		: 200911201021
 * Description	: 1) Depress the enhancement 200911131142MaxWong, 200911121040MaxWong
 * 						-a) comment out Chinese Input fields in the [New] page
 * 						-bC) fallback the corresponding Download Sample to previous version
 * 						    that is not supporting ChiTitle and ChiDetails only
 * 							and modify back ole_import_update.php to not importing chinese fields
 * 
 * 						    fallback the current version Download Sample student_olr_sample.csv:
 * 						    student_olr_sample.csv.200911201044 -> student_olr_sample.csv.20091113
 * 						
 * 						-c) comment out the selection of English, Chinese and Bilingual for Report Type -> Student Learning Profile
 * 						    in [Student Report Printing] page
 * Case Number	: 200911201021MaxWong
 * C=CurrentIssue
 * ----------------------------------
 * Type			: Enhancement
 * Date 		: 200911131142
 * Description	: 1C) Enhance the form to New an OLE with chinese title and chinese details
 * C=CurrentIssue 2) The import page is required to support chinese title, chinese details and add school remarks
 * Case Number	: 200911131142MaxWong
 */ 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-ole-program-tag.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

// Initializing classes
$LibPortfolio = new libpf_slp();
$LibUser = new libuser($UserID);
$LibUserStudent = new libuser($StudentID);
$lpf_ole_program_tag = new libpf_ole_program_tag();
if ($intranet_session_language == "en")
{
	$StudentInfo = $LibUserStudent->EnglishName." (".$LibUserStudent->ClassName."-".$LibUserStudent->ClassNumber.")";
}
else
{
	$StudentInfo = $LibUserStudent->ChineseName." (".$LibUserStudent->ClassName."-".$LibUserStudent->ClassNumber.")";
}

$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$LibPortfolio->ACCESS_CONTROL("ole");

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR("Teacher");

# load approval setting
$ApprovalSetting = $LibPortfolio->GET_OLR_APPROVAL_SETTING();

$vars = "ClassName=$ClassName&StudentID=$StudentID";

$ValidPeriod = 1;

// [2015-0324-1420-39164] check for St. Paul CUST display
$StPaulHideField = $sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme'] && $IntExt != 1;

#####################################
if($displayBy=="")
	$displayBy = "Record";

# define the buttons according to the variable $displayBy
$bcolor_record = ($displayBy=="Record") ? "#CFE6FE" : "#FFD49C";
$bcolor_stat = ($displayBy=="Stat") ? "#CFE6FE" : "#FFD49C";
//$bcolor_analysis = ($displayBy=="Analysis") ? "#CFE6FE" : "#FFD49C";

$gif_record = ($displayBy=="Record") ? "l" : "o";
$gif_stat = ($displayBy=="Stat") ? "l" : "o";
//$gif_analysis = ($displayBy=="Analysis") ? "l" : "o";

$link_record = ($displayBy=="Record") ? $ec_iPortfolio['olr_display_record'] : "<a href='index.php?$vars&displayBy=Record' class='link_a'>".$ec_iPortfolio['olr_display_record']."</a>";
$link_stat = ($displayBy=="Stat") ? $ec_iPortfolio['olr_display_stat'] : "<a href='index_stat.php?$vars&displayBy=Stat' class='link_a'>".$ec_iPortfolio['olr_display_stat']."</a>";
//$link_analysis = ($displayBy=="Analysis") ? $ec_iPortfolio['olr_analysis'] : "<a href='index_analysis.php?$vars&displayBy=Analysis' class='link_a'>".$ec_iPortfolio['olr_display_analysis']."</a>";
####################################

// load certificate submission period settings
$student_award_config_file = "$eclass_root/files/student_ole_config.txt";
$filecontent = trim(get_file_content($student_award_config_file));
list($starttime, $sh, $sm, $endtime, $eh, $em) = unserialize($filecontent);

$starttime = (trim($starttime)!="") ? $starttime." $sh:$sm:00" : "";
$endtime = (trim($endtime)!="") ? $endtime." $eh.$em:59" : "";

$ValidPeriod = (($starttime!="" && $starttime!="0000-00-00 00:00:00") || ($endtime!="" && $endtime!="0000-00-00 00:00:00")) ? validatePeriod($starttime, $endtime) : 1;

// Create a status filter
$status_selection = "<SELECT name='status' onChange='this.form.submit()'>";
$status_selection .= "<OPTION value='' ".($status==0?"SELECTED":"").">".$ec_iPortfolio['all_status']."</OPTION>";
$status_selection .= "<OPTION value='1' ".($status==1?"SELECTED":"").">".$ec_iPortfolio['pending']."</OPTION>";
$status_selection .= "<OPTION value='2' ".($status==2?"SELECTED":"").">".$ec_iPortfolio['approved']."</OPTION>";
$status_selection .= "<OPTION value='3' ".($status==3?"SELECTED":"").">".$ec_iPortfolio['rejected']."</OPTION>";
$status_selection .= "<OPTION value='4' ".($status==4?"SELECTED":"").">".$ec_iPortfolio['teacher_submit_record']."</OPTION>";
$status_selection .= "</SELECT>";
$status_selection_html = $ec_iPortfolio['status']." : ".$status_selection;

# cateogry selection
//$category_selection = $LibPortfolio->GET_OLE_CATEGORY_SELECTION("name='category' onChange='this.form.submit()'", $category, 1);
//$category_selection_html = $ec_iPortfolio['category']." : ".$category_selection;
//$category_selection_html = $category_selection;

$Cats = $LibPortfolio->GET_OLR_Category(0, true);

$ELEArray = $LibPortfolio->GET_ELE();
$ELECodeArray = array_keys($ELEArray);
if (count($ELECodeArray)>0)
{
	$ELEImplode = implode(",",$ELECodeArray);
}
$ELECount = count($ELECodeArray);

# ELE selection
$ele_selection = $LibPortfolio->GET_ELE_SELECTION("name='ELE' onChange='this.form.submit()'", $ELE, 1);

# Searching box
$searching_html = "<input name='search_text' value='".$search_text."' >&nbsp;<input class='button_g' onClick=\"this.form.submit();\" type='button' value=\"".$button_search."\">";

$ClassArray = $LibPortfolio->RETURN_OLE_CLASS_BY_PROGRAMID($EventID);
$ClassSelectionHTML = $linterface->GET_SELECTION_BOX($ClassArray,"name='Class' onChange='this.form.submit()'", $i_general_all, $Class);




$linterface->LAYOUT_START();


if ($msg == "3")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("delete")."</td></tr>";
}
else if ($msg == "1")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("add")."</td></tr>";
}
else if ($msg == "2")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("update")."</td></tr>";
}
else
{
	$xmsg = "";
}


$data = $LibPortfolio->RETURN_OLE_PROGRAM_BY_PROGRAMID_MAX($EventID);

	if (is_array($data)) {
		$startdate = $data['StartDate'];
		$enddate = $data['EndDate'];
		$engTitle = $data['Title'];
		$chiTitle = $data['TitleChi'];
		$category = $data['Category'];
		$subCategoryID = $data['SubCategoryID'];
		$ele = $data['ELE'];
		$organization = $data['Organization'];
		$engDetails = intranet_htmlspecialchars($data['Details']);
		$chiDetails = $data['DetailsChi'];
		$remark = $data['SchoolRemarks'];
		$modified_date_html = $data['ModifiedDate'];
		$input_date = $data['InputDate'];
		$modified_date = $data['ModifiedDate'];
		$period = $data['Period'];
		$int_ext = $data['IntExt'];
		$canJoin = $data['CanJoin'];
		$canJoinStartDate = $data['CanJoinStartDate'];
		$canJoinEndDate = $data['CanJoinEndDate'];
		$userName = $data['UserName'];
		$autoApprove = $data['AUTOAPPROVE'];
		$maximumHours = $data['MaximumHours'];
		$defaultHours = $data['DefaultHours'];
		$compulsoryFields = $data['CompulsoryFields'];
		$DefaultApproverText = $LibPortfolio-> GET_TEACHER_NAME ($data['DefaultApprover']);
		$ModifiedUser_html = ($data['ModifiedUser'] == "") ? "" : " ({$data['ModifiedUser']})";
		$IsSAS = $data['IsSAS'];
		$IsOutsideSchool = $data['IsOutsideSchool'];
	}

$IsSASText = ($IsSAS) ? $ec_iPortfolio['SLP']['Yes'] : $ec_iPortfolio['SLP']['No'];
$IsOutsideSchoolText = ($IsOutsideSchool) ? $Lang['iPortfolio']['OLE']['Outside_School'] : $Lang['iPortfolio']['OLE']['Inside_School'];

$SubCats = $LibPortfolio->GET_OLE_SUBCATEGORY($category);

if($int_ext == "EXT")
$SubmitType = $iPort["external_record"];
else
$SubmitType = $iPort["internal_record"];

if($startdate=="" || $startdate=="0000-00-00")
	$date_display = "--";
else if($enddate!="" && $enddate != "0000-00-00")
	$date_display = $startdate."&nbsp;".$profiles_to."&nbsp;".$enddate;
else
	$date_display = $startdate;


# get ELE Array
if($ele!="")
{
	# get the ELE code array
	$DefaultELEArray = $LibPortfolio->GET_ELE();
	$ELEArr = explode(",", $ele);
	for($i=0; $i<sizeof($ELEArr); $i++)
	{
		$t_ele = trim($ELEArr[$i]);
		$ele_display .= "- ".$DefaultELEArray[$t_ele]."<br />";
	}
}
else
	$ele_display = "--";

if ($FromPage == "program")
	$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "self.location='ole.php?IntExt=$IntExt&Year=$Year'");
else
	$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "self.location='ole.php?IntExt=$IntExt&Year=$Year'");
	
# Setting for Join Period
$joinPeriodDisplayStatus = 'none';
$canJoinText = $ec_iPortfolio['SLP']['No'];
$canJoinStartText = " -- ";
$canJoinEndText = " -- ";
$autoApproveText = $ec_iPortfolio['SLP']['No'];

if ($canJoin) {
	$joinPeriodDisplayStatus = '';
	$canJoinText = $ec_iPortfolio['SLP']['Yes'];
	if (isset($canJoinStartDate) && !empty($canJoinStartDate) && $canJoinStartDate != '0000-00-00') {
		$canJoinStartText = $canJoinStartDate;
	}
	if (isset($canJoinEndDate) && !empty($canJoinEndDate) && $canJoinEndDate != '0000-00-00') {
		$canJoinEndText = $canJoinEndDate;
	}
	if ($autoApprove) {
		$autoApproveText = $ec_iPortfolio['SLP']['Yes'];
	}
	
	$maximumHoursText = empty($maximumHours)?' -- ':$maximumHours;
	$defaultHoursText = empty($defaultHours)?' -- ':$defaultHours;
	
	if (!empty($compulsoryFields)) {
		$temp_codes = str_split($compulsoryFields);
		if (is_array($temp_codes)) {
			foreach($temp_codes as $key => $value) {
				$temp_lang[] = $ipf_cfg["OLE_PROGRAM_CompulsoryFields__Lang"][$value];
			}
		}
		$compulsoryFieldsText = implode(",&nbsp;&nbsp;",$temp_lang);
	} else {
		$compulsoryFieldsText = ' -- ';
	}

}

// [2015-0324-1420-39164] show Teacher-in-charge
if($sys_custom['iPortfolio_ole_record_tic']){
	$teacher_PIC = $LibPortfolio->GET_OLE_PROGRAM_TEACHER_IN_CHARGE_BY_RECORDID($EventID);
	
	$teacher_in_charge = "--";
	$delim = "";
	if(count($teacher_PIC) > 0){
		$teacher_in_charge = "";
		foreach($teacher_PIC as $pic){
			$teacher_in_charge .= $delim.$pic['UserName'];
			$delim = ", ";
		}
	}
}

	$assignStudentNowButton = $linterface->GET_ACTION_BTN($ec_iPortfolio['assign_student_now'], "button", "document.form1.submit()");
	
	$EditButton = $linterface->GET_SMALL_BTN($Lang['Btn']['Edit'], "button","location.href='ole_new.php?record_id=$EventID&IntExt=$IntExt'","EditBtn");			
//////////////////////// CNECC CUST:START ///////////////////////////
if($sys_custom['cnecc_SS'])
{
	include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cnecc/libpf-slp-cnecc.php");
	$libpf_slp_cnecc = new libpf_slp_cnecc();
	if ($libpf_slp_cnecc->isSunshineProgram($EventID)) {
		$assignStudentNowButton = "<span style='color:red;'>* ".$Lang["Cust_Cnecc"]["CannotAssignStudentsForSunshinePrograms"]."</span><br />";
		$cnecc_customize = $libpf_slp_cnecc->getResultDisplayCustomization($EventID);
	} else {
		// do nothing
	}
}
//////////////////////// CNECC CUST:START ///////////////////////////



# Program tag
$lpf_ole_program_tag = new libpf_ole_program_tag();
$lpf_ole_program_tag->SET_CLASS_VARIABLE("program_id", $EventID);
$lpf_ole_program_tag->SET_PROGRAM_TAG_PROPERTY();
$program_tag_display = ($intranet_session_language == "b5") ? $lpf_ole_program_tag->GET_CLASS_VARIABLE("chi_title") : $lpf_ole_program_tag->GET_CLASS_VARIABLE("eng_title");
$program_tag_display = ($program_tag_display == "") ? "--" : $program_tag_display;
?>

<SCRIPT LANGUAGE="Javascript">
</SCRIPT>

<FORM name="form1" method="GET" action="ole_assign_student.php" >
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	<table width="96%" border="0" cellspacing="0" cellpadding="8">
	<?=$xmsg?>
	<tr>
		<td>
		<!-- CONTENT HERE -->
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="tabletext">
			<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td>
				<a href="ole_new.php?IntExt=<?=$IntExt?>"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_new.gif" width="18" height="18" border="0" align="absmiddle"> <?=$ec_iPortfolio['new_ole_activity']?></a>
				</td>
				<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
			</tr>
			</table>
			</td>
			<td align="right" valign="bottom" class="tabletext">&nbsp;</td>
		</tr>
		</table>
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" style="border:dashed 1px #666666">
		<tr>
			<td width="80%" valign="top">
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td colspan="2" align="right"><?=$EditButton?></td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $iPort['submission_type']; ?></span></td>
				<td width="80%" valign="top"><?=$SubmitType?></td>
				</tr>
			<tr>
			
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['title'] ?></span></td>
				<td width="80%" valign="top"><?=$engTitle?></td>
			</tr>
			<?/*200911201021MaxWong This comment may release later on<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['chinese'] . ($intranet_session_language == "en" ? " " : "") . $ec_iPortfolio['title']; ?></span></td>
				<td width="80%" valign="top"><?=$chiTitle?></td>
			</tr>*/?>
			
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
				<td><?=$period?></td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['date']?> / <?=$ec_iPortfolio['year_period']?></span></td>
				<td><span class="tabletext"><?=$date_display?></span></td>
			</tr>
			
			<!-- category -->
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $ec_iPortfolio['category']; ?></span></td>
				<td><?=$Cats[$category]?></td>
			</tr>
			
			<!-- sub category -->
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['sub_category'] ?></span></td>
				<td><?=(($SubCats[$subCategoryID] == "") ? "--" : $SubCats[$subCategoryID])?></td>
			</tr>
			
			<!-- addition category -->
			<?/*<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $ec_iPortfolio['tag']; ?></span></td>
				<td><?=$program_tag_display?></td>
			</tr>*/?>
			
			<!-- component of ole -->
			<? 	if ($IntExt) {
					// hide component of ole for external program
				} else {
			?>
				<tr valign="top">
					<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $ec_iPortfolio['ele']; ?></span></td>
					<td><?=$ele_display?></td>
				</tr>
			<?	} ?>
			
<?php if(!$StPaulHideField) { ?>
				
			<!-- allow students to join -->
<?
if (strtolower($int_ext) == $ipf_cfg["OLE_TYPE_STR"]["INT"]){
?>
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['SLP']['AllowStudentsToJoin'] ?></span></td>
				<td><?= $canJoinText ?></td>
			</tr>
<?
}
?>			
			<!-- join period -->
			<tr valign="top" style="display: <?= $joinPeriodDisplayStatus ?>">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['SLP']['JoinPeriod'] ?></span></td>
				<td><?= $ec_iPortfolio['SLP']['From'] . " " . $canJoinStartText . " " . $ec_iPortfolio['SLP']['to'] . " " . $canJoinEndText?> </td>
			</tr>
			
			<!-- auto approve -->
			<tr valign="top" style="display: <?= $joinPeriodDisplayStatus ?>">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['SLP']['AutoApprove'] ?></span></td>
				<td><?= $autoApproveText ?> </td>
			</tr>
			
			<!-- maximum hours -->
			<tr valign="top" style="display: <?= $joinPeriodDisplayStatus ?>">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['SLP']['MaximumHours'] ?></span></td>
				<td><?= $maximumHoursText ?> </td>
			</tr>
			
			<!-- default hours -->
			<tr valign="top" style="display: <?= $joinPeriodDisplayStatus ?>">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['SLP']['DefaultHours'] ?></span></td>
				<td><?= $defaultHoursText ?> </td>
			</tr>
			
			<!-- compulsory fields -->
			<tr valign="top" style="display: <?= $joinPeriodDisplayStatus ?>">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['SLP']['CompulsoryFields'] ?></span></td>
				<td><?= ($compulsoryFieldsText=='')?"--": $compulsoryFieldsText?> </td>
			</tr>
			
			<!-- Default Approver -->
			<tr valign="top" style="display: <?= $joinPeriodDisplayStatus ?>">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['SLP']['DefaultApprover'] ?></span></td>
				<td><?=($DefaultApproverText=='')?"--": $DefaultApproverText ?> </td>
			</tr>
			
			<!-- partner organization -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['organization']?></span></td>
				<td><?=($organization=='')? "--":$organization?></td>
			</tr>
			
			<!-- english details -->
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['details'] ?></span></td>
				<td><?=($engDetails==""?"--":nl2br($engDetails))?></td>
			</tr>
			<?/*200911201021MaxWong This comment may release later on
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['chinese'] . ($intranet_session_language == "en" ? " " : "") . $ec_iPortfolio['details']?></span></td>
				<td><?=($chiDetails==""?"--":nl2br($chiDetails))?></td>
			</tr>*/?>

			<!-- school remarks -->
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['school_remark']?></span></td>
				<td><?=($remark==""?"--":nl2br($remark))?></td>
			</tr>
			
<?php } ?>		
    
<?php if($sys_custom['iPortfolio_ole_record_tic']){ ?>
            <!-- Teacher-in-charge -->
            <tr valign="top">
	            <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['iPortfolio']['OLE']['TeacherInCharge']?></span></td>
	            <td><?=$teacher_in_charge?></td>
          	</tr>
<?php } ?>

			<!-- HKUGAC Customization -->
<?php if (isset($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) && $sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) { ?>
	
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['iPortfolio']['OLE']['SAS_with_link']?></span></td>
				<td><?=$IsSASText?></td>
			</tr>
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['iPortfolio']['OLE']['In_Outside_School']?></span></td>
				<td><?=$IsOutsideSchoolText?></td>
			</tr>
<?php } ?>

			
			<!-- created by -->
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['SLP']['CreatedBy']?></span></td>
				<td><?= $userName ?></td>
			</tr>
			
      <!-- last modified date -->
      <tr valign="top">
        <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$iPort["last_updated"]?></span></td>
        <td><?= $modified_date_html ?><?=$ModifiedUser_html?></td>
      </tr>
			
			<?=$cnecc_customize?>
			</table>
			</td>
		</tr>
		</table>
		<br />

		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" >
		<tr>
			<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center">
				<?=$assignStudentNowButton?>
				<?= $BackBtn ?>
			</tr>
			</table>
			</td>
		</tr>
		</table>

		<!-- End of CONTENT -->
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $LibTable->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $LibTable->order; ?>" />
<input type="hidden" name="field" value="<?php echo $LibTable->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$LibTable->page_size?>" />
<input type="hidden" name="EventID" value="<?php echo $EventID; ?>" />
<input type="hidden" name="FromPage" value="<?=$FromPage?>" />
<input type="hidden" name="Year" value="<?=$Year?>" />
<input type="hidden" name="IntExt" value="<?=$IntExt?>" />

</form>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
