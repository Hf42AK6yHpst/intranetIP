<?php

// Modifing by 
/*
 * 	Purpose: read file content of iportfolio other report item 
 * 	
 * 	Log
 * 
 * 	Date:	2015-11-16 [Cameron] create this file 
 * 	
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio_others/libportfoliootherreportitem.php");


intranet_auth();
intranet_opendb();

$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

$lpf = new libportfolio2007();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$ReportItemID = $_REQUEST['ReportItemID'];
$lpfori = new libportfoliootherreportitem($ReportItemID);
$ReportFile = $lpfori->getReportFile();
$IsDir = $lpfori->getIsDir();
$rs = $lpfori->getItemOwner();
if ($IsDir) {
	header("Location:	{$PATH_WRT_ROOT}file/iportfolio/other_report/{$ReportFile}");
}
else {
	$extension = substr($ReportFile,strrpos($ReportFile,'.'));
	
	$target = $intranet_root."/file/iportfolio/other_report/".$ReportFile;
	if ($rs) {
		$output_filename = $rs['ReportTitle'] . '_' . $rs['StudentName'] . '_' . $rs['ClassName'].'_'.$rs['ClassNumber'];
		if ($extension) {
			$output_filename .= $extension;
		}
	
		$useragent = $_SERVER['HTTP_USER_AGENT'];
		if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
		    $browser_version=$matched[1];
		    $browser = 'IE';
		} elseif (preg_match( '|Opera ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
		    $browser_version=$matched[1];
		    $browser = 'Opera';
		} elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
			$browser_version=$matched[1];
			$browser = 'Firefox';
		} elseif(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
			$browser_version=$matched[1];
			$browser = 'Safari';
		} else {
			// browser not recognized!
		    $browser_version = 0;
		    $browser = 'other';
		}
		$content = get_file_content($target);
		
		output2browser($content, $output_filename);
	}
	else {
		echo 'error';
		exit;	
	}
}
intranet_closedb();

?>
