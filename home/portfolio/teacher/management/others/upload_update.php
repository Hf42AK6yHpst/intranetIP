<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/portfolio_others/libportfoliootherreportitem.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
//$CurrentPage = "Teacher_OLE";
$CurrentPage = "OtherReports_Upload_File";
$CurrentPageName = $iPort['menu']['ole'];

$lpf = new libpf_slp();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$OtherReportID = $_POST['OtherReportID'];
$student_id = $_POST['student'];

$result = "AddUnsuccess";
if (is_array($_FILES) && sizeof($_FILES)>0)
{
	foreach($_FILES as $k => $f)
	{
		if ($f["name"]!="" && $f["tmp_name"]!="")
		{
			$timestamp = time();
			$hash = md5($timestamp.$student_id);
			$part_dir = "r_".$OtherReportID."/".$hash;
			$dst_dir = $PATH_WRT_ROOT."file/iportfolio/other_report/".$part_dir;
			$lf = new libfilesystem();
			if (!file_exists($dst_dir)) {
				$lf->folder_new($dst_dir);
			}
			
			$file_extension = substr($f["name"],strrpos($f["name"],'.'));
			$dst_file = $dst_dir."/".$timestamp.$file_extension;
			move_uploaded_file($f["tmp_name"], $dst_file);
			
			if ($file_extension == '.zip') {
				$lf->unzipFile($dst_file,$dst_dir);
				$IsDir = 1;
				$original_file_name = substr($f["name"],0,strrpos($f["name"],'.'));
				$ReportFile = $part_dir."/".$original_file_name."/index.html";
				$lf->file_remove($dst_file);		// remove zip file
			}
			else {
				$IsDir = 0;
				$ReportFile = $part_dir."/".$timestamp.$file_extension;
			}
			unset($lf);
			
			$result = "UpdateSuccess";	
			
			// save record in db
			$lpfori = new libportfoliootherreportitem();
			$lpfori->setOtherReportID($OtherReportID);
			$lpfori->setStudentID($student_id);
			$lpfori->setReportFile($ReportFile);
			$lpfori->setIsDir($IsDir);
			
			$ReportItemID = $lpfori->save();

			$result = $ReportItemID ? "AddSuccess" : "AddUnsuccess";
			unset($lpfori);

		}
	}
}


intranet_closedb();

?>
<script>
	window.opener.location.href = window.opener.location.href + "?xmsg=<?=$result?>&OtherReportID=<?=$OtherReportID?>";
	window.close();
</script>
