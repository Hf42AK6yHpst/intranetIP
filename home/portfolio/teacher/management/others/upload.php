<?php
// Modifing by 
/*
 * 	Log
 * 
 * 	
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
//iportfolio_auth("T");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio_others/libportfoliootherreport.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_opendb();

$linterface = new interface_html("popup.html");
$lpf = new libpf_slp();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

$lc = new libclass();

//$CurrentPage = "Teacher_OLE";
//$CurrentPageName = $iPort['menu']['ole'];

$CurrentPage = "OtherReports_Upload_File";
$title = $iPort['OLE_Upload_File'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$OtherReportID = $_GET['OtherReportID'];

$lpfor = new libportfoliootherreport($OtherReportID);
$ReportTitle = $lpfor->getReportTitle();


if ($class_name) {
	$student_list = $lc->getStudentNameListWClassNumberByClassName($class_name);
}
else {
//   	$name_field = getNameFieldWithClassNumberByLang();
//   	$sql = "SELECT UserID, $name_field FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2) ORDER BY ClassNumber";
//   	$student_list = $lc->returnArray($sql,2);
}
$student_option_list = getSelectByArray($student_list, 'name="student"');


$linterface->LAYOUT_START();

?>
<SCRIPT LANGUAGE="Javascript">
function Switch_Class(class_name)
{
	document.form1.action = "upload.php?OtherReportID=<?=$OtherReportID?>";
	document.form1.submit();
}
function check_form() {
	var ret = false;
	var file = document.getElementById('file').files[0];
	
	if (document.form1.class_name.value == '') {
		alert("<?=$Lang['iPortfolio']['OtherReports']['PleaseSelectClass']?>");
	}
	else if (document.form1.student.value == '') {
		alert("<?=$Lang['iPortfolio']['OtherReports']['PleaseSelectStudent']?>");
	}
	else if ($('#file').val() == '') {
		alert("<?=$Lang['iPortfolio']['OtherReports']['PleaseSelectFile']?>");
	}
	else if (file && (file.size <= 0)) {
		alert("<?=$Lang['iPortfolio']['OtherReports']['ZeroFileSize']?>");		
	}
//	else if (file && (file.size > 104857600)) {	// 100M
//		alert("<?=$Lang['iPortfolio']['OtherReports']['FileSizeExceed']?>");		
//	}
	else if (!$('#file').val().toLowerCase().match(/\.(pdf|zip)$/)) {
		alert("<?=$Lang['iPortfolio']['OtherReports']['InvalidFileType']?>");
	}	 
	else {
		document.form1.btn_upload.disabled=true;
		ret = true;	
	}
	return ret;
}
</SCRIPT>


<FORM enctype="multipart/form-data" action="upload_update.php" method="POST" name="form1" onSubmit="return check_form();">
	<table width="450" border="0" cellspacing="0" cellpadding="0" style="border-spacing: 5px;">
		<tr>
          <td colspan="2"><?=$ReportTitle?></td>
        </tr>
		<tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['iPortfolio']['OtherReports']['Class']?></td>        
        	<td class="tabletext" width="70%"><?=$lc->getSelectClass('name="class_name" onchange="Switch_Class(this.value);"',$class_name,0)?></td>
        </tr>

        <tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['iPortfolio']['OtherReports']['Student']?></td>
        	<td class="tabletext" width="70%"><?=$student_option_list?></td>
        </tr>
                
        <tr>
          	<td valign="top" class="formfieldtitle tabletext"><?=$iPort["upload_file"]?>: </td>
          	<td class="tabletext" width="70%"><input class="file" type="file" id="file" name="File"/><br /></td>
        </tr>
    </table>
    <br />
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td align="center">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="submit" value="<?=$button_upload?>" name="btn_upload">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="button" value="<?=$button_cancel?>" onClick="self.close()">
		</td>
	</tr>
	</table>
	<input type="hidden" name="OtherReportID" id="OtherReportID" value="<?=$OtherReportID?>">
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
