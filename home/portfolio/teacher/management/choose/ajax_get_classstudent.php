<?php
################# Change Log [Start] #####
#
#	Date	:	2016-03-22	Omas
#				add display student left / suspend status
#
################## Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$sql = "SELECT iu.UserID, CONCAT(".getNameFieldByLang2("iu.").", ' (', ".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN").", ' - ', ycu.ClassNumber, ')', IF(iu.RecordStatus=1,'',IF(iu.RecordStatus=3,' [".$Lang['Status']['Left']."]', ' [".$Lang['Status']['Suspend']."]')) ) AS DisplayName ";
$sql .= "FROM {$intranet_db}.YEAR y ";
$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON y.YearID = yc.YearID ";
$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON ycu.YearClassID = yc.YearClassID ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON ycu.UserID = iu.UserID ";
$sql .= "WHERE ycu.YearClassID IN (".implode(", ", $YearClassID).") ";
$sql .= "ORDER BY y.Sequence, yc.Sequence, ycu.ClassNumber";
$student_arr = $li->returnArray($sql);

$html_student_selection = "<select name=\"StudentID[]\" multiple=\"multiple\" size=\"7\" style=\"width:100%\">";
for($i=0, $i_max=count($student_arr); $i<$i_max; $i++)
{
	if($student_arr[$i]["DisplayName"] != null || $student_arr[$i]["DisplayName"] != ''){
		$_uID = $student_arr[$i]["UserID"];
		$_uName = $student_arr[$i]["DisplayName"];
	
		$html_student_selection .= "<option value=\"{$_uID}\">{$_uName}</option>";
	}
}
$html_student_selection .= "</select>";

echo $html_student_selection;

intranet_closedb();
?>