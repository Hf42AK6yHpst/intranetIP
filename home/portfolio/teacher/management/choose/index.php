<?php

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");

intranet_auth();
intranet_opendb();

$lpf = new libportfolio2007();
$CurrentPage = "Management_AwardInput";

$linterface = new interface_html("popup.html");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();
$MODULE_OBJ['title'] = $iDiscipline['select_students'];

$lpf_fc = new libpf_formclass();

$year_arr = $lpf_fc->GET_CLASSLEVEL_LIST();
$html_year_selection = "<select name=\"YearID[]\" multiple=\"multiple\" size=\"7\" style=\"width:100%\">";
for($i=0, $i_max=count($year_arr); $i<$i_max; $i++)
{
	$_yID = $year_arr[$i]["YearID"];
	$_yName = $year_arr[$i]["YearName"];

	$html_year_selection .= "<option value=\"{$_yID}\">{$_yName}</option>";
}
$html_year_selection .= "</select>";

################################################################

$linterface->LAYOUT_START();
?>

<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />

<script language="javascript">

function addOptions(){
	$.ajax({
		type: "POST",
		url: "ajax_get_selectedstudent.php",
		data: $("form[name=form1]").serialize(),
		dataType: "xml",
		beforeSend: function(){
			tb_show("<?=$Lang['General']['Loading']?>", "#TB_inline?height=120&width=400&inlineId=loading&modal=true");
		},
		success: function(xml){
			var openerSelectObj = $(window.opener.document).find("select[name='<?=$fieldname?>']");
			var openerSelectOption = $(openerSelectObj).find("option");

			var currentOptions = new Array();
			$(openerSelectOption).each(function(){
				currentOptions.push($(this).val());
			});

			$(xml).find("option").each(function(){
				var optionVal = $(this).attr("value");
				var optionText = $(this).text();
			
				if($.inArray(optionVal, currentOptions) == -1)
				{
					$(openerSelectObj).append(
						"<option value="+optionVal+">"+optionText+"</option>"
					);
				}
			});
			
			tb_remove();
		}
	});

}

$(document).ready(function(){
	$("#year_selectall").click(function(){
		$("select[name='YearID[]'] option").attr("selected", true);
	});
	
	$("#year_add").click(function(){
		$("input[name=add_type]").val("year");
		addOptions();
	});

	$("#year_expand").click(function(){
		var parStr = "";
		var yearID = $("select[name='YearID[]'] option:selected");
		
		// hide student row if year selection changes
		$("#student_cell").html("");
		$("#student_row").hide();

		// if no year selected, hide class selection		
		if($(yearID).size() > 0)
		{
			$.each(yearID, function(){
				var val = $(this).val();
			
				parStr += "&YearID[]="+val;
			});
			
			$.ajax({
				type: "POST",
				url: "ajax_get_yearclass.php",
				data: "1"+parStr,
				success: function(msg){
					$("#class_cell").html(msg);
					$("#class_row").show();
				}
			});
		}
		else{
			$("#class_cell").html("");
			$("#class_row").hide();
		}
	});
	
	$("#yearclass_selectall").click(function(){
		$("select[name='YearClassID[]'] option").attr("selected", true);
	});
	
	$("#yearclass_add").click(function(){
		$("input[name=add_type]").val("yearclass");
		addOptions();
	});
	
	$("#yearclass_expand").live("click", function(){
		var parStr = "";
		var yearClassID = $("select[name='YearClassID[]'] option:selected");
		
		if($(yearClassID).size() > 0)
		{
			$.each(yearClassID, function(){
				var val = $(this).val();
			
				parStr += "&YearClassID[]="+val;
			});
			
			$.ajax({
				type: "POST",
				url: "ajax_get_classstudent.php",
				data: "1"+parStr,
				success: function(msg){
					$("#student_cell").html(msg);
					$("#student_row").show();
				}
			});
		}
		else{
			$("#student_cell").html("");
			$("#student_row").hide();
		}
	});
	
	$("#student_selectall").click(function(){
		$("select[name='StudentID[]'] option").attr("selected", true);
	});
	
	$("#student_add").click(function(){
		$("input[name=add_type]").val("student");
		addOptions();
	});
});
</script>

<form name="form1" action="index.php" method="post">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
	<tr>
		<td align="center">

			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table width="100%" cellpadding="5" cellspacing="0" border="0">
							<col width="25%">
							<col width="60%">
							<col width="15%">
							
							<!-- Class Level -->
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_ClassLevel?></td>
								<td><?=$html_year_selection?></td>
								<td>
									<?=$linterface->GET_BTN($button_add, "button", "", "year_add")?>
									<?=$linterface->GET_BTN($Lang['Btn']['Expand'], "button", "", "year_expand")?>
									<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "", "year_selectall")?>
								</td>
							</tr>
							
							<!-- Class -->
							<tr id="class_row" style="display:none">
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iPort['class']?></td>
								<td id="class_cell">&nbsp;</td>
								<td>
									<?=$linterface->GET_BTN($button_add, "button", "", "yearclass_add")?>
									<?=$linterface->GET_BTN($Lang['Btn']['Expand'], "button", "", "yearclass_expand")?>
									<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "", "yearclass_selectall")?>
								</td>
							</tr>
							
							<!-- Student -->
							<tr id="student_row" style="display:none">
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iPort['student_name']?></td>
								<td id="student_cell">&nbsp;</td>
								<td>
									<?=$linterface->GET_BTN($button_add, "button", "", "student_add")?>
									<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "", "student_selectall")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
							<tr>
								<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
							</tr>
							<tr>
								<td align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="add_type" />
<input type="hidden" name="fieldname" value="<?=$fieldname ?>" />
</form>

<div id="loading" style="display:none">
	<div id='TB_title'>
		<div id='TB_ajaxWindowTitle'><img src="/images/<?=$LAYOUT_SKIN?>/indicator.gif"> <?=$Lang['General']['Loading']?></div>
	</div>
	<br />
  <div>
    <?=$Lang['iPortfolio']['alertLeavePage']?>
  </div>
</div>
<?

$linterface->LAYOUT_STOP();

?>
