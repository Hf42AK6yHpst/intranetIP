<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_auth();
intranet_opendb();

$lpf = new libportfolio(); // for libwordtemplates_ipf

$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
$ay_selection_html = getSelectByArray($academic_year_arr, "name='academicYearID' onChange='get_yearterm_opt()'", "", 0, 0, "", 2);

$academic_yearterm_arr = array();
$ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", "", 0, 0, "", 2);

// template for teacher page
$linterface = new interface_html();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>New Text</title>
<link href="/templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
</head>

<script language="JavaScript">

function checkform(formObj)
{	
	if($("[name=academicYearID]").val()=="")
	{
		formObj.academicYearID.focus();
		alert(globalAlertMsg18);
		return false;
  }
  
  var programID = parent.document.getElementsByName("record_id[]");

  for(var i=0; i<programID.length; i++)
  {
    if(!programID[i].checked) continue;
  
    var input = document.createElement("input");
    input.setAttribute("type", "hidden");
    input.setAttribute("name", "record_id[]");
    input.setAttribute("value", programID[i].value);
    formObj.appendChild(input);
  }

	return true;
}

function get_yearterm_opt()
{
  var ay_id = $("[name=academicYearID]").val();
  if(ay_id == ""){
    $("#ayterm_cell").html('');
    return;
  }
  
	$.ajax({
		type: "GET",
		url: "../../ajax/ajax_get_yearterm_opt.php",
		data: "ay_id="+ay_id,
		success: function (msg) {
      if(msg != "")
      {
        $("#ayterm_cell").html(msg);
      }
		}
	});
}

</script>

<body>
<FORM name="form1" method="post" action="ole_change_year_update.php" onSubmit="return checkform(this)">
<div class="edit_pop_board edit_pop_board_simple">
  <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center" >
    <!-- year term -->
    <tr>
    	<td width="200" valign="top" nowrap="nowrap" class="formfieldtitle">
    	  <span class="tabletext"><?=$ec_iPortfolio['year']?><span class="tabletextrequire">*</span></span></td>
    	</td>
    	<td nowrap='nowrap'><?=$ay_selection_html?></td>
    </tr>
		<tr>
		  <td><?=$ec_iPortfolio['term']?></td>
		  <td nowrap='nowrap' id='ayterm_cell'><?=$ayterm_selection_html?></td>
		</tr>
    
		<tr>
			<td height="1" class="dotline" colspan="2"  ><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
		</tr>

		<tr>
			<td  colspan="2" align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="tabletextremark"><?=$ec_iPortfolio['mandatory_field_description']?></td>
				<td align="right">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "parent.tb_remove()") ?>
				</td>
				</tr>
				</table>
			</td>
		</tr>
  </table>
</div>
</form>
</body>
</html>

<?php
intranet_closedb();
?>