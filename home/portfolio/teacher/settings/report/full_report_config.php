<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * 	2015-04-20 Omas
 * - Added CSS SLP Report Tab
 * 
 *   2014-07-15 Bill
 * - Added PKMS SLP Report Tab
 * 
 * 2010-07-15 Max (201007150955)
 * - Modified the method of getting $NoPhotoConfig, $NoStudentDetailsConfig and $NoSchoolNameConfig
 * 
 * *******************************************
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

// load settings
$portfolio_report_config_file = "$eclass_root/files/portfolio_report_config.txt";
$filecontent = trim(get_file_content($portfolio_report_config_file));
$config_array = unserialize($filecontent);

// Photo displaying config
# 201007151009, seems getting the config from wrong position 13-15 of array,
# changed to 14-16 <== Whenever added a new column, the number will be shifted by 1
$PhotoConfig = !$config_array[15];
$StudentDetailsConfig = !$config_array[16];
$SchoolNameConfig = !$config_array[17];

#Report Setting Name and Right
$report_setting = array("default",$ec_iPortfolio['guardian_info'], $ec_iPortfolio['merit'], $ec_iPortfolio['assessment_report']."(".$ec_iPortfolio['display_by_score'].")", $ec_iPortfolio['assessment_report']."(".$ec_iPortfolio['display_by_rank'].")", $ec_iPortfolio['assessment_report']."(".$ec_iPortfolio['display_by_grade'].")", $ec_iPortfolio['assessment_report']."(".$ec_iPortfolio['display_by_stand_score'].")", $ec_iPortfolio['activity'], $ec_iPortfolio['award'], $ec_iPortfolio['teacher_comment'], $ec_iPortfolio['attendance'], $ec_iPortfolio['service'], $ec_iPortfolio['other_learning_record'],$iPort["external_record"], $ec_iPortfolio['student_self_account']);
$user_right_array = array("default","assessment_report","merit","assessment_report","assessment_report","assessment_report","assessment_report","activity","award","teacher_comment","attendance","service","ole","ole","");


//debug_r($config_array);
// order selection
for($j=1; $j<=count($report_setting)-1; $j++)
{
	$t_order = $config_array[$j][0];
	$t_display = $config_array[$j][1];
	$disabled = ($t_display==1) ? " " : "disabled";

	
	//debug_r($t_order);

	${"order_select".$j} = $t_order;
	
	$check = ($t_display==1) ? "CHECKED" : "";
	${"display_check".$j} = "<input type='checkbox' name='display$j' onClick='set_disable($j)' value=1 $check>";

	if($j==3 || $j==4 || $j==5 || $j==6)
	{
		$t_component = $config_array[$j][2];
		$check = ($t_component==1) ? "CHECKED" : "";

		${"component_check".$j} = "<input type='checkbox' name='component$j' value=1 $check $disabled>".$ec_iPortfolio['show_component_sub'];
	}
	else if($j==12)
	{
		$OLE_fields =	array	(
													$ec_iPortfolio['category'],
													$ec_iPortfolio['ele'],
													$ec_iPortfolio['ole_role'],
													$ec_iPortfolio['hours'],
													$ec_iPortfolio['achievement'],
													$ec_iPortfolio['details'],
													$ec_iPortfolio['school_remark']
												);
	
		$t_component = explode(",", $config_array[$j][2]);

		${"component_check".$j} = "<table border=0 cellpadding=2 cellspacing=0>";
		for($k=0; $k<count($OLE_fields); $k++)
		{
			$check = (in_array($k, $t_component)) ? "CHECKED" : "";
			${"component_check".$j} .=	"
																		<tr>
																			<td valign='top'><input type='checkbox' name='component".$j."[]' value=$k $check $disabled></td>
																			<td valign='bottom'>".$OLE_fields[$k]."</td>
																		</tr>
																	";
		}
		${"component_check".$j} .= "</table>";
	}
}

# setting rows
for($i=1;$i<=count($report_setting)-1;$i++)
{
	$t_display = $config_array[$i][1];
	$bclass = ($t_display==1) ? "class='row_on'" : "class='row_off'";
  if($user_right_array[$i] == "" || strstr($ck_user_rights_ext, $user_right_array[$i]))
  {
  	if($i!=12)
  	{
  		$template_row[$i] = "<tr class='tablerow1'>
  							<td><table border='0' cellpadding='0' cellspacing='0' width='100%' height='40'><tbody><tr><td id='td".$i."n1' $bclass align='center'>".${'display_check'.$i}."<input type='hidden' value='$i' name='order[]'/></td></tr></tbody></table></td>";
  	
  		$template_row[$i] .= "<td><table border='0' cellpadding='0' cellspacing='0' width='100%' height='40'><tbody><tr><td id='td".$i."n2' $bclass align='center'><table border='0' cellpadding='2' cellspacing='0'><tbody><tr><td><a href='javascript:;' onClick='javascript:swapRowUp(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode);'><img src='".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_a_off.gif' border='0' height='13' width='13'></a></td></tr><tr><td><a href='javascript:;' onClick='swapRowDown(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode)'><img src='".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_d_off.gif' border='0' height='13' width='13'></a></td></tr></tbody></table></td></tr></tbody></table></td>";
  							
  		$template_row[$i]  .= "<td><table border='0' cellpadding='0' cellspacing='0' width='100%' height='40'><tbody><tr><td id='td".$i."n3' $bclass><span class='tabletext'>".$report_setting[$i]."</span></td></tr></tbody></table></td>";
  		if($i==3|| $i==4 || $i==5 || $i==6)
  		{
  						$template_row[$i] .="<td><table border='0' cellpadding='0' cellspacing='0' width='100%' height='40'><tbody><tr><td id='td".$i."n4' $bclass>".${'component_check'.$i}."</td></tr></tbody></table></td></tr>";
  		}else{
  						$template_row[$i] .="<td><table border='0' cellpadding='0' cellspacing='0' width='100%' height='40'><tbody><tr><td id='td".$i."n4' $bclass>&nbsp;</td></tr></tbody></table></td></tr>";
  		}
  	}
  	else
  	{
  		$template_row[$i] = "<tr class='tablerow1'>
  							<td height='100%'><table border='0' cellpadding='0' cellspacing='0' width='100%' height='200'><tbody><tr><td id='td".$i."n1' $bclass align='center'>".${'display_check'.$i}."<input type='hidden' value='$i' name='order[]'/></td></tr></tbody></table></td>";
  	
  		$template_row[$i] .= "<td height='100%'><table border='0' cellpadding='0' cellspacing='0' width='100%' height='200'><tbody><tr><td id='td".$i."n2' $bclass align='center'><table border='0' cellpadding='2' cellspacing='0'><tbody><tr><td><a href='javascript:;' onClick='javascript:swapRowUp(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode);'><img src='".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_a_off.gif' border='0' height='13' width='13'></a></td></tr><tr><td><a href='javascript:;' onClick='swapRowDown(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode)'><img src='".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_d_off.gif' border='0' height='13' width='13'></a></td></tr></tbody></table></td></tr></tbody></table></td>";
  							
  		$template_row[$i]  .= "<td height='100%'><table border='0' cellpadding='0' cellspacing='0' width='100%' height='200'><tbody><tr><td id='td".$i."n3' $bclass><span class='tabletext'>".$report_setting[$i]."</span></td></tr></tbody></table></td>";
  		$template_row[$i] .="<td height='100%'><table border='0' cellpadding='0' cellspacing='0' width='100%' height='200'><tbody><tr><td id='td".$i."n4' $bclass  style='padding-top:10px;padding-bottom:10px;'>".${'component_check'.$i}."</td></tr></tbody></table></td></tr>";
  	}
  }
}


# define the navigation, page title and table size
// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_Reports";
$CurrentPageName = $iPort['menu']['reports'];

$lpf = new libportfolio2007();

// check iportfolio admin user
if (!$lpf->IS_ADMIN_USER($UserID) && !$lpf->IS_TEACHER())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$luser = new libuser($UserID);

// Tab Menu Settings
$TabMenuArr = array();
$TabMenuArr[] = array("full_report_config.php", $ec_iPortfolio['full_report_config'], 1);
$TabMenuArr[] = array("slp_config.php", $ec_iPortfolio['slp_config'], 0);
if ($sys_custom['iPf']['pkms']['Report']['SLP']){
	$TabMenuArr[] = array("plms_slp_config.php", $ec_iPortfolio['pkms_SLP_config'], 0);
}
if($sys_custom['iPf']['css']['Report']['SLP']){
	$TabMenuArr[] = array("css_slp_config.php", 'Student Development Progress Report', 0);
}
$TabMenuArr[] = array("student_transcript_config_edit.php", $ec_iPortfolio['transcript_config'], 0);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

?>

<script language="javascript">
function checkform(obj)
{
	var i, j, order_value, display, exist, null_exist;
	var orderArr = new Array(12);

	for(i=1; i<=12; i++)
	{
		order_obj = eval("obj.order"+i);
		display_obj = eval("obj.display"+i);

		if(typeof(order_obj)!="undefined" && typeof(order_obj)!="undefined" && display_obj.checked==true)
		{
			exist = 0;
			null_exist = 0;
			order_value = order_obj.value;
			for(j=1; j<=12; j++)
			{
				if(order_value=="")
				{
					null_exist=1;
					break
				}
				else if(orderArr[j]==order_value)
				{
					exist=1;
					break;
				}
			}
		
			if (null_exist==1)
			{
				alert("<?=$ec_warning['order_required']?>");
				return false;
			}
			else if(exist==1)
			{
				alert("<?=$ec_warning['order_repeat']?>");
				return false;
			}
			else
			{
				orderArr[i] = order_value;
			}
		}	
	}
	return true;
}

function set_disable(id)
{
	var formObj = document.form1;
	var display, set_value;

	display = eval("formObj.display"+id+".checked");
	set_value = (display==false) ? "true" : "false";
	
	for(i=1;i<=4;i++)
	{
		new_id= "td"+id+"n"+i;
		if(display==false)
		{	
			document.getElementById(new_id).className="row_off";
		}else{

		    document.getElementById(new_id).className="row_on";	
		}
	}
	
	
	//eval("formObj.order"+id+".disabled="+set_value);
	if(id==3 || id==4 || id==5 || id==6)
	{
		eval("formObj.component"+id+".disabled="+set_value);
	}
	else if (id==12)
	{
		for(i=0; i<formObj.elements.length; i++)
		{
			if(formObj.elements[i].name == "component"+id+"[]")
				eval("formObj.elements["+i+"].disabled="+set_value);
		}
	}

}


function swapRowUp(chosenRow) {

	var mainTable = document.getElementById("mainTable"); 
	// Explain chosenRow
	//var test = chosenRow.innerHTML;
	//alert(test);
	//var test = chosenRow.rowIndex;
	//alert(test);
 if (chosenRow.rowIndex != 0 && chosenRow.rowIndex != 1) {
   moveRow(chosenRow, chosenRow.rowIndex-1); 
 }
} 
function swapRowDown(chosenRow) {
	var mainTable = document.getElementById("mainTable"); 
	//var test = mainTable.rows.length;
	//alert(test);
 if (chosenRow.rowIndex != mainTable.rows.length-1) {
   moveRow(chosenRow, chosenRow.rowIndex+1); 
 }
} 
//moves the target row object to the input row index 
function moveRow(targetRow, newIndex) {
//since we are not actually swapping 
//but simulating a swap, have to "skip over" 
//the current index 
 if (newIndex > targetRow.rowIndex) {
   newIndex++; 
 }
//establish proper reference to the table 
 var mainTable = document.getElementById('mainTable'); 
//insert a new row at the new row index 
 var theCopiedRow = mainTable.insertRow(newIndex); 
//copy all the cells from the row to move 
//into the new row 
 for (var i=0; i<targetRow.cells.length; i++) {
		
	 	var oldCell = targetRow.cells[i]; 
   		var newCell = document.createElement("TD"); 
   		
   		if(i==0 || i==1){newCell.align="center";}
   		//newCell.ClassName="row_on";
   		//alert(targetRow.cells[i].innerHTML);
		newCell.innerHTML = oldCell.innerHTML; 
		// copy height attribute
		if(oldCell.getAttribute("height") != "")
			newCell.setAttribute("height", oldCell.getAttribute("height"));
		theCopiedRow.appendChild(newCell); 
		copyChildNodeValues(targetRow.cells[i], newCell);	

 } 
//delete the old row 
 mainTable.deleteRow(targetRow.rowIndex); 
} 

function copyChildNodeValues(sourceNode, targetNode) {
	var mainTable = document.getElementById("mainTable"); 
 for (var i=0; i < sourceNode.childNodes.length; i++) {
   try{
     targetNode.childNodes[i].value = sourceNode.childNodes[i].value;
   }
   catch(e){

   }
 }
}

</script>

<!-- ===================================== Body Contents ============================= -->
<form name="form1" method="post" action="full_report_config_update.php" onSubmit="return checkform(this)">
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <!-- Tab Start -->
      <?=$lpf->GET_TAB_MENU($TabMenuArr);?>
      <!-- Tab End -->
    </td>
  </tr>
  <tr><td><?=$linterface->GET_SYS_MSG($msg); ?></td></tr>
  <tr>
    <td align="center">
    <!-- Cap Setting Start -->
      <table width="100%" border="0" cellspacing="0" cellpadding="6">
        <tr class="tabletop">
          <td class="tabletopnolink" width="5%"><?=$iPort["display"]?></td>
          <td class="tabletopnolink" width="5%"><?=$iPort["order"]?></td>
          <td class="tabletopnolink" width="60%">&nbsp;</td>
          <td width="30%">&nbsp;</td>
        </tr>
        <tr class="tablerow1">
          <td class="row_on" align="center"><input type="checkbox" name="PhotoConfig" id="photo_1" value="1" <?=($PhotoConfig==1?"CHECKED":"")?> /></td>
          <td class="row_on" align="center">--</td>
          <td class="row_on tabletext"><?=$ec_iPortfolio['not_display_student_photo']?></td>
          <td class="row_on tabletext">&nbsp;</td>
        </tr>
        <tr class="tablerow1">
          <td class="row_on" align="center"><input type="checkbox" name="StudentDetailsConfig" id="details_1" value="1" <?=($StudentDetailsConfig==1?"CHECKED":"")?> /></td>
          <td class="row_on" align="center">--</td>
          <td class="row_on tabletext"><?=$ec_iPortfolio['no_student_details']?></td>
          <td class="row_on tabletext">&nbsp;</td>
        </tr>
        <tr class="tablerow1">
          <td class="row_on" align="center"><input type="checkbox" name="SchoolNameConfig"id="school_1" value="1" <?=($SchoolNameConfig==1?"CHECKED":"")?> /></td>
          <td class="row_on" align="center">--</td>
          <td class="row_on tabletext"><?=$ec_iPortfolio['no_school_name']?></td>
          <td class="row_on tabletext">&nbsp;</td>
        </tr>
      <!--
      <tr class="tabletop">
      <td class="tabletopnolink" width="5%">&nbsp;</td>
      <td class="tabletopnolink" align="center" width="5%">&nbsp;</td>
      <td width="60%">&nbsp;</td>
      <td width="30%">&nbsp;</td>
      </tr>
      -->
      </table>
   
     <!--	<tr><td colspan="4"> -->
      <table id="mainTable" border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr class="tabletop" height="25">
          <td class="tabletopnolink" width="6%">&nbsp;</td>
          <td class="tabletopnolink" align="center" width="7%">&nbsp;</td>
          <td width="60%">&nbsp;</td>
          <td width="27%">&nbsp;</td>
        </tr>
      	
      <?
      for($i=0;$i<=count($report_setting)-1;$i++)
      {
      	for($j=0;$j<=count($report_setting)-1;$j++)
      	{
      		if($i==${"order_select".$j})
      		{
      			echo $template_row[$j];
      		}
      		
      	}	
      	
      }
      ?>
      </table> 
  	<!-- Cap Setting End -->
  	
    	<br />
      <table width="95%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td align="center">
            <input class="formbutton" type="submit" value="<?=$button_submit?>" name="btn_submit" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
            <input name="Reset" class="formbutton" value="<?=$button_reset?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" type="reset">
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>
<!-- ===================================== Body Contents (END) ============================= -->




<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
