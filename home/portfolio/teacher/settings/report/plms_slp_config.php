<?php

/** [Modification Log] Modifying By: 
 * *******************************************
 * 2014-07-15 Bill
 * - PKMS SLP Report Setting
 * *******************************************
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

// load settings
//$objIpfSetting = iportfolio_settings::getInstance();
//$reportIsAllowed		= $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpIsAllowed"]);
//$reportIssueDate		= $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpIssuesDate"]);
//$reportFormAllow		= $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpFormAllowed"]);
//$reportIsPrintIssueDate = $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpWithIssuesDate"]);
//
//$reportFormAllowInfo = explode(',',$reportFormAllow);

//$portfolio_report_config_file = "$eclass_root/files/portfolio_slp_config.txt";
//if(file_exists($portfolio_report_config_file))
//{
//	list($filecontent, $OLESettings, $ExtSettings,$pageBreakSetting) = explode("\n", trim(get_file_content($portfolio_report_config_file)));
//	$config_array = unserialize($filecontent);
//	$OLESettings = unserialize($OLESettings);
//	$ExtSettings = unserialize($ExtSettings);
//	$pageBreakSetting = unserialize($pageBreakSetting);
//}
//else
//{
//	for($j=1; $j<=count($report_setting)-1; $j++)
//	{
//		$config_array[$j][0] = $j;
//		$config_array[$j][1] = 1;
//	}
//}

// Load Pui Kui SLP Setting
$portfolio__plms_report_config_file = "$eclass_root/files/portfolio_plms_slp_config.txt";
if(file_exists($portfolio__plms_report_config_file))
{
	list($r_formAllowed, $startdate, $enddate,$allowStudentPrintPKMSSLP,$issuedate,$r_isPrintIssueDate,$noOfPKMSSLPRecord) = explode("\n", trim(get_file_content($portfolio__plms_report_config_file)));
	
	$reportIsAllowed = unserialize($allowStudentPrintPKMSSLP);
	$reportIssueDate = unserialize($issuedate);
	$reportIsPrintIssueDate = unserialize($r_isPrintIssueDate);
	$reportFormAllowInfo = unserialize($r_formAllowed);
	$selectStartDate = unserialize($startdate);
	$selectEndDate = unserialize($enddate);
	$noOfPKMSSLPRecord = unserialize($noOfPKMSSLPRecord);
	
	if(!is_array($reportFormAllowInfo)){
		$reportFormAllowInfo = array();
	}
	
	if($noOfPKMSSLPRecord == null){
		$noOfPKMSSLPRecord = 10;
	}
	
}
// Default setting: Not allow, empty form and record = 10
else
{
	$reportIsAllowed = 0;
	$noOfPKMSSLPRecord = 10;
	$reportFormAllowInfo = array();
}

# define the navigation, page title and table size

// template for teacher page
$linterface = new interface_html();

// set the current page title
$CurrentPage = "Settings_Reports";
$CurrentPageName = $iPort['menu']['reports'];

$lpf = new libportfolio2007();

// check iportfolio admin user and Pui Kui Customization
if (!$sys_custom['iPf']['pkms']['Report']['SLP'] || (!$lpf->IS_ADMIN_USER($UserID) && !$lpf->IS_TEACHER()))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$luser = new libuser($UserID);

// display forms check box
$formCheck = getFormAllowCheckBoxUI($reportFormAllowInfo);

// Radio button: allow student to print report 
if($reportIsAllowed == 1){
	// true
	$allowStudentPrintPKMSSLPT = ' checked ';
} else {
	// false
	$allowStudentPrintPKMSSLPF = ' checked ';
}

// Checkbox: print issue date in report 
if($reportIsPrintIssueDate == 1){
	$reportIsPrintIssueDateChecked = ' checked ';
}

// Tab Menu Settings
$TabMenuArr = array();
$TabMenuArr[] = array("full_report_config.php", $ec_iPortfolio['full_report_config'], 0);
$TabMenuArr[] = array("slp_config.php", $ec_iPortfolio['slp_config'], 0);
$TabMenuArr[] = array("plms_slp_config.php", $ec_iPortfolio['pkms_SLP_config'], 1);
$TabMenuArr[] = array("student_transcript_config_edit.php", $ec_iPortfolio['transcript_config'], 0);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

?>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />

<script language="javascript">

function checkAllForm (flag){

   if (flag == 0)
   {
      $("input[id^='div_formAllowed_']").attr('checked', false);
   }
   else
   {
      $("input[id^='div_formAllowed_']").attr('checked', true);
   }
	
	return;
}

function checkform(obj)
{

	//ALLOW STUDENT PRINT REPORT IS CHECKED
	if($('#allowStudentPrintPKMSSLP_yes').attr('checked')){
		
		//CHECK FORM AT LEAST ONE FORM IS SELECTED
		var formChecked = $("input[name='r_formAllowed[]']:checked").size(); 				
		if (formChecked == 0) { 
			alert("<?=$Lang['iPortfolio']['OEA']['ApplicableFormsEmpty']?>");
			return false; 
		} 
	}
	
	//CHECK ISSUE DATE INPUT
	if(!check_date(document.getElementById("issuedate"), "<?=$i_invalid_date?>")){
			return false;
	}
	
	//CHECK START AND END DATE INPUT
	if(!check_date(document.getElementById("startdate"), "<?=$i_invalid_date?>") || 
		!check_date(document.getElementById("enddate"), "<?=$i_invalid_date?>")){
			return false;
	}

	//CHECK DATE RANGE INPUT
	if((new Date(document.getElementById("startdate").value).getTime()) > 
			(new Date(document.getElementById("enddate").value).getTime())){
		alert("<?=$Lang['General']['JS_warning']['InvalidDateRange']?>");
		return false;
	}
		
	return true;

}

</script>

<!-- ===================================== Body Contents ============================= -->
<form name="form1" method="post" action="plms_slp_config_update.php" onSubmit="return checkform(this)" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	    <td>
	      <!-- Tab Start -->
	      <?=$lpf->GET_TAB_MENU($TabMenuArr);?>
	      <!-- Tab End -->
	    </td>
  	</tr>
  	
	<tr><td><?=$linterface->GET_SYS_MSG($msg,$failMsg)?></td></tr>
	
	
<!-- ===================================== SLP Contents ============================= -->

<tr><td>

<table width="100%" border="0" cellpadding="2" cellspacing="2">

<tr><td>

	<div id="div_studentPKMSSLPSetting">
		<table width="100%" border="0" cellpadding="2" cellspacing="2">
			
			<tr id="targetformRow">
				<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['SLP']['JoinableYear']?>&nbsp;</td>
				<td>
<?php
	echo $formCheck;
?>&nbsp;
				</td>
			</tr>
			
			<tr id="RecordNoRow">
				<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$Lang['iPortfolio']['plms_SLP_REPORT']['NumberOfOLE']?></td>
				<td><?= $noOfPKMSSLPRecord ?></td>
				<input type="hidden" name="noOfRecord" value="<?= $noOfPKMSSLPRecord ?>">				
			</tr>
			
			<tr id="selectPeriodRow">
				<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$Lang['iPortfolio']['plms_SLP_REPORT']['SelectOLERecord']?></td>
				<td>
					<?= $linterface->GET_DATE_PICKER("startdate", $selectStartDate, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="startdate",$SkipIncludeJS=0, $CanEmptyField=0); ?>
					- &nbsp;
					<?= $linterface->GET_DATE_PICKER("enddate", $selectEndDate, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="enddate",$SkipIncludeJS=0, $CanEmptyField=0); ?>
				</td>
			</tr>
			
			<tr id="allowPrintRow">
				<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$Lang['iPortfolio']['SLP_REPORT']['AllowForStudentToPrint']?></td>
				<td>
					<input type = "radio" value="1" name="allowStudentPrintPKMSSLP" id ="allowStudentPrintPKMSSLP_yes" <?=$allowStudentPrintPKMSSLPT?>><label for="allowStudentPrintPKMSSLP" ><?=$Lang['General']['Yes']?></label> 
					<input type = "radio" value="0" name="allowStudentPrintPKMSSLP" id ="allowStudentPrintPKMSSLP_no" <?=$allowStudentPrintPKMSSLPF?>><label for="allowStudentPrintPKMSSLP" ><?=$Lang['General']['No']?></label> 
				</td>
			</tr>
			
			<tr id='issueDateRow'>
				<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['date_issue']?></span></td>
				<td>
					<?= $linterface->GET_DATE_PICKER("issuedate", $reportIssueDate, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="issuedate",$SkipIncludeJS=0, $CanEmptyField=0); ?>
					<br/>
					<div id="printIssueDate"> <input type = "checkbox" name="r_isPrintIssueDate" value = "1" id ="div_isPrintIssueDate" <?=$reportIsPrintIssueDateChecked?>><label for="div_isPrintIssueDate" ><?=$Lang['iPortfolio']['print_date_issue']?></label></div>
				</td>
			</tr>
			
		</table>
	</div>
	
</td></tr>

</table>

</td></tr>

<!-- ===================================== SLP Contents (END) ============================= -->

<tr><td>

		<table width="95%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="center">
					<input class="formbutton" type="submit" value="<?=$button_submit?>" name="btn_submit" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
					<input name="Reset" class="formbutton" value="<?=$button_reset?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" type="reset">
				</td>
			</tr>
		</table>

</td></tr>

</table>

</form>
<!-- ===================================== Body Contents (END) ============================= -->

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

function getFormAllowCheckBoxUI($reportFormAllowInfo = array()){
	$FormPerRow = 3;
	$libfcm = new form_class_manage();
	$FormInfoArr = $libfcm->Get_Form_List($GetMappedClassInfo=false, $ActiveOnly=1);

	$formCheck = '';
	$formCheck .= '<table border="0" cellpadding="2" cellspacing="2">'."\n";
	
	for ($i = 0,$i_max = count($FormInfoArr); $i < $i_max; $i++){
		$_YearID =  $FormInfoArr[$i]['YearID'];
		$_YearName =  $FormInfoArr[$i]['YearName'];
		$_checked = in_array($_YearID,$reportFormAllowInfo) ?  ' checked ' : '' ;
		$_divID = 'div_formAllowed_'.$_YearID;

		if ($i % $FormPerRow == 0) {
			$h_formChk .= '<tr>'."\n";
		}

		$formCheck .= '<td >&nbsp;</td>'."\n";
		$formCheck .= '<td >'."\n";
		$formCheck .= '<input type ="checkbox" name="r_formAllowed[]" value="'.$_YearID.'" '.$_checked.' id='.$_divID.'><label for ="'.$_divID.'">'.$_YearName.'</label><br/>'."\n";
		$formCheck .= '</td>'."\n";
						
		if ( (($i+1) % $FormPerRow == 0) || ($i==$i_max-1)) {
			$formCheck .= '</tr>'."\n";
		}

	}
	$formCheck .= '</table>';
	$formCheck = '<input type = "checkbox" name="checkFormAll" value ="1" id="div_checkFormAll" onclick="checkAllForm(this.checked)"><label for ="div_checkFormAll">Check All</label><br/>'.$formCheck;
	return $formCheck;
}

?>
