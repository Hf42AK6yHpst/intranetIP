<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * 2010-07-27 Max (201007261726)
 * - Add configuration for student details
 * 
 * 2010-06-04 Max (201006041431)
 * - Added setting for page-break
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once("$eclass_filepath/src/includes/php/lib-filesystem.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_settings.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

/******* Upload School Header Image  ******/

# get file name
$tempFile = $_FILES['uploadFile']['tmp_name'];
$origFileName =  $_FILES['uploadFile']['name'];

if (!empty($_FILES) && $origFileName!='') {
	$libfilesystem = new libfilesystem();
		
	$ext = strtolower($libfilesystem->file_ext($origFileName));
	
	$allowedExtensions = array(".tif",".jpg",".jpeg",".gif",".png");

	if(!in_array($ext,$allowedExtensions))
	{
		header("Location: slp_config.php?UploadPhotoFail=1");	
		exit;
	}
	
	# create folder
	$dbStorePath = $ipf_cfg['SLPArr']['SLPFolderPath']; 
	$targetPath = $intranet_root.$dbStorePath;	

	if(is_dir($targetPath)) 
	{
		$dh = opendir($targetPath);
 
		$LogofileName = $ipf_cfg['SLPArr']['SLPSchoolHeaderImageName'];
		$ExistFileName = '';
		 
		while (($file = readdir($dh)) !== false)
		{		
			$pos = strpos($file,$LogofileName);
			
			if($pos === false) {
				 // string needle NOT found in $file
			}
			else {
			 	unlink($targetPath.$file);
			}
		}
		closedir($dh);
	}
	else
	{
		$libfilesystem->folder_new($targetPath);
		chmod($targetPath, 0755);
	}
	
	

	# encode file name
	$targetFileName = $ipf_cfg['SLPArr']['SLPSchoolHeaderImageName'];
	$targetFile = $targetPath.$targetFileName.$ext;
	
	$uploadResult = move_uploaded_file($tempFile, $targetFile);

}
/******* END Upload School Header Image  ******/



for($i=1; $i<=5; $i++)
{
	for($j=0;$j<=4;$j++)
	{
		if($i==$order[$j])
		{
			$t_order = $j+1;
		}
	}
	
	if($t_order != "" && ($max_order_used == "" || $t_order > $max_order_used))
	{
		$max_order_used = $t_order;
	}
	
	//$t_order = ${"order".$i};
	$t_display = ${"display".$i};
	$t_component = ${"component".$i};
	$config_array[$i] = array($t_order, $t_display, $t_component);
	
	unset($t_order);
}

# Set the order of item to last if the item is unavailable
for($i=1; $i<=5; $i++)
{
	if($config_array[$i][0] == "")
	{
		$config_array[$i][0] = $max_order_used+1;
		$max_order_used++;
	}
}

/*
$config_array[] = ($PhotoConfig=="") ? 0 : $PhotoConfig;
$config_array[] = ($StudentDetailsConfig=="") ? 0 : $StudentDetailsConfig;
$config_array[] = ($SchoolNameConfig=="") ? 0 : $SchoolNameConfig;
*/

########################################
##	handling Student Details Config
$StudentDetailsConfig_Final[] = ($StudentDetailsConfig=="") ? 1 : !$StudentDetailsConfig;

$sdc_elements = array(
						"StudentName",
						"DateOfBirth",
						"EducationInstitution",
						"AdmissionDate",
						"SchoolAddress",
						"SchoolPhone",
						"HKID",
						"Gender",
						"SchoolCode",
				);
$sdc_elements_value = array (
						$sdc_elements[0]=>${"sdc_".$sdc_elements[0]},
						$sdc_elements[1]=>${"sdc_".$sdc_elements[1]},
						$sdc_elements[2]=>${"sdc_".$sdc_elements[2]},
						$sdc_elements[3]=>${"sdc_".$sdc_elements[3]},
						$sdc_elements[4]=>${"sdc_".$sdc_elements[4]},
						$sdc_elements[5]=>${"sdc_".$sdc_elements[5]},
						$sdc_elements[6]=>${"sdc_".$sdc_elements[6]},
						$sdc_elements[7]=>${"sdc_".$sdc_elements[7]},
						$sdc_elements[8]=>${"sdc_".$sdc_elements[8]},
				);
$StudentDetailsConfig_Final[] = $sdc_elements_value;

$config_array[] = $StudentDetailsConfig_Final;
########################################

########################################
## handle the page break settings
for($i=1;$i<=5;$i++) {
	$pageBreakSetting[$i]["IS_SET_PAGE_BREAK"] = (${"pageBreakSet_$i"}?1:0);
}
########################################
//debug_pr($RecordsAllowed);
//debug_pr($ExtRecordsAllowed);
//debug_pr($ClassLevel);
for($i=0; $i<count($ClassLevel); $i++)
{
	$OLEExtra[$ClassLevel[$i]][] = $RecordsAllowed[$i];
	$OLEExtra[$ClassLevel[$i]][] = (is_array($TeacherAllowed) && in_array($ClassLevel[$i], $TeacherAllowed)) ? 1 : 0;
	$OLEExtra[$ClassLevel[$i]][] = (is_array($StudentAllowed) && in_array($ClassLevel[$i], $StudentAllowed)) ? 1 : 0;
}
$OLEExtra["NoPrintToLimit"] = $NoPrintToLimit;

for($i=0; $i<count($ExtClassLevel); $i++)
{
	$ExtExtra[$ExtClassLevel[$i]][] = $ExtRecordsAllowed[$i];
	$ExtExtra[$ExtClassLevel[$i]][] = (is_array($ExtTeacherAllowed) && in_array($ExtClassLevel[$i], $ExtTeacherAllowed)) ? 1 : 0;
	$ExtExtra[$ExtClassLevel[$i]][] = (is_array($ExtStudentAllowed) && in_array($ExtClassLevel[$i], $ExtStudentAllowed)) ? 1 : 0;
}
$ExtExtra["NoPrintToLimit"] = $ExtNoPrintToLimit;

//debug_r($config_array);
$portfolio_report_config_file = "$eclass_root/files/portfolio_slp_config.txt";
$li_fm = new fileManager($ck_course_id, 0, 0);
$li_fm->writeFile(serialize($config_array)."\n".serialize($OLEExtra)."\n".serialize($ExtExtra)."\n".serialize($pageBreakSetting), $portfolio_report_config_file);

$objIpfSetting = iportfolio_settings::getInstance();

$slpReportLang = is_array($r_slpReportLang) ? implode(',',$r_slpReportLang):  '';
$formAllowed = is_array($r_formAllowed) ? implode(',',$r_formAllowed):  '';

if($r_allowStudentPrintSLP != 1){
	//not equal 1 , not allow to student to print SLP , reset all the setting to empty
	$r_allowStudentPrintSLP = 0;
	$slpReportLang = '';
	$formAllowed = '';
	$r_issuedate = '';
	$r_isPrintIssueDate =0;

}

$IsShowImage = ($IsShowImage=='')? 0:1;

$objIpfSetting->updateSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpIsAllowed"],$r_allowStudentPrintSLP);
$objIpfSetting->updateSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpIssuesDate"],$r_issuedate);
$objIpfSetting->updateSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpLang"],$slpReportLang);
$objIpfSetting->updateSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpFormAllowed"],$formAllowed);
$objIpfSetting->updateSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpWithIssuesDate"],$r_isPrintIssueDate);
$objIpfSetting->updateSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpWithSchoolHeaderImage"],$IsShowImage);
$objIpfSetting->updateSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["academicScoreDisplayMode"],$academicScoreDisplayMode);
$objIpfSetting->updateSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["displayFullMark"],$displayFullMark);
$objIpfSetting->updateSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["displayComponentSubject"],$displayComponentSubject);


intranet_closedb();

header("Location: slp_config.php?msg=update");
?>
