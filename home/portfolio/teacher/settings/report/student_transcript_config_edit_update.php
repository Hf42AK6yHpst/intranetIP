<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once("$eclass_filepath/src/includes/php/lib-filesystem.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");

intranet_auth();
intranet_opendb();

$fm = new fileManager($ck_course_id, 0, 0);
$fs = new phpduoFileSystem();

if($school_badge!="")
{
	$folder_prefix = $eclass_filepath."/files/portfolio/transcript/template_0/images";
	if(!file_exists($folder_prefix))
	{
		$fs->createFolder($folder_prefix);
	}
	$filename = "badge.gif";
	$loc = $school_badge;

	if ($loc!="none" && file_exists($loc))
	{
		if(strpos($filename, ".")!=0)
		{
			$fm->createFolder($folder_prefix);
			$fs->phpduoCopy($loc, stripslashes($folder_prefix."/".$filename));
		}
	}
}

if($CSV_file!="")
{
	$folder_prefix = $eclass_filepath."/files/portfolio/transcript/template_0/csv";
	if(!file_exists($folder_prefix))
	{
		$fs->createFolder($folder_prefix);
	}
	$filename = "data.csv";
	$loc = $CSV_file;

	if ($loc!="none" && file_exists($loc))
	{
		if(strpos($filename, ".")!=0)
		{
			$fs->phpduoCopy($loc, stripslashes($folder_prefix."/".$filename));
		}
	}
}

// $transcript_content["school_address"] = stripslashes($school_address);
$transcript_content["school_description"] = stripslashes($school_description);
$transcript_content["PhotoConfig"] = $PhotoConfig;

$portfolio_setting_file = "$eclass_filepath/files/portfolio_transcript.txt";

$li_fm = new phpduoFileSystem($eclass_db, 0, 0);

$li_fm->writeFile(serialize($transcript_content), $portfolio_setting_file);

header("Location: student_transcript_config_edit.php?msg=2"); 
?>
