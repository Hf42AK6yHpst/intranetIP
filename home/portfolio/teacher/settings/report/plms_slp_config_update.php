<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once("$eclass_filepath/src/includes/php/lib-filesystem.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_settings.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$portfolio_plms_report_config_file = "$eclass_root/files/portfolio_plms_slp_config.txt";
$li_fm = new fileManager($ck_course_id, 0, 0);
$li_fm->writeFile(serialize($r_formAllowed)."\n".serialize($startdate)."\n".serialize($enddate)."\n".
			serialize($allowStudentPrintPKMSSLP)."\n".serialize($issuedate)."\n".serialize($r_isPrintIssueDate)."\n".serialize($noOfRecord), $portfolio_plms_report_config_file);

intranet_closedb();

header("Location: plms_slp_config.php?msg=update");

?>