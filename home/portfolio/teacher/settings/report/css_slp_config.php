<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * 2015-04-20 Omas
 * - Create this page
 * 
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");

//include_once($PATH_WRT_ROOT."includes/libuser.php");/home/web/eclass40/intranetIP25/includes/form_class_manage.php
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

// Tab Menu Settings
$TabMenuArr = array();
$TabMenuArr[] = array("full_report_config.php", $ec_iPortfolio['full_report_config'], 0);
$TabMenuArr[] = array("slp_config.php", $ec_iPortfolio['slp_config'], 0);
if ($sys_custom['iPf']['pkms']['Report']['SLP']){
	$TabMenuArr[] = array("plms_slp_config.php", $ec_iPortfolio['pkms_SLP_config'], 0);
}
if($sys_custom['iPf']['css']['Report']['SLP']){
	$TabMenuArr[] = array("css_slp_config.php", 'Student Development Progress Report', 1);
}
$TabMenuArr[] = array("student_transcript_config_edit.php", $ec_iPortfolio['transcript_config'], 0);

$linterface = new interface_html();
$lpf = new libportfolio2007();
$libclass = new libclass();
$fcm = new form_class_manage();
$libgeneralsettings = new libgeneralsettings();


// set the current page title
$CurrentPage = "Settings_Reports";
$CurrentPageName = $iPort['menu']['reports'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

// get teacher list
$TeacherList = $fcm->Get_Teaching_Staff_List();
$TeacherListAssoAry = BuildMultiKeyAssoc($TeacherList,'UserID',array('Name'),1);

### get year form list
$formAry = $libclass->getLevelArray();
$formIDAry = Get_Array_By_Key($formAry, 'ClassLevelID');

### get previous setting
$getGeneralSettingArr = array();
for($i=0; $i<$numForm;$i++){
	$getGeneralSettingArr[] = "'cust_CSS_slp_HeadOfSchoolConfig_$formIDAry[$i]'";
}
$GeneralSettingArr = $libgeneralsettings->Get_General_Setting('iPortfolio',$getGeneralSettingArr);

### Build head of school selection table
$numForm = count($formAry);
$headOfSchoolSetting = '';
$headOfSchoolSetting .= '<table width="100%">';
for($i=0; $i<$numForm;$i++){
	$_classLevelId = $formAry[$i]['ClassLevelID'];
	$_generalSettingName = $GeneralSettingArr['cust_CSS_slp_HeadOfSchoolConfig_'.$_classLevelId];
	
	$headOfSchoolSetting .= '<tr>';
		$headOfSchoolSetting .= '<td>'.$formAry[$i]['LevelName'].'</td>';
		$headOfSchoolSetting .= '<td>'.getSelectByAssoArray($TeacherListAssoAry, 'name="TeacherList['.$_classLevelId.']" id="TeacherList['.$_classLevelId.']"', $_generalSettingName, $all=0, 0, $Lang['SysMgr']['FormClassMapping']['AddTeacher']).'</td>';
	$headOfSchoolSetting .= '</tr>';
}
$headOfSchoolSetting .= '</table>';

$linterface->LAYOUT_START();
?>

<!-- Tab Start -->
<?=$lpf->GET_TAB_MENU($TabMenuArr);?>
<!-- Tab End -->
<form name="form1" method="post" action="css_slp_config_update.php" onSubmit="true">
<div class="table_board">
<table class="form_table_v30" width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>
  	<td class="field_title">Head Of School</td>
  	<td><?=$headOfSchoolSetting?></td>
  </tr>
</table>
<br style="clear:both;">
<div class="edit_bottom_v30">
	<input class="formbutton" type="submit" value="<?=$button_submit?>" name="btn_submit" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
<div>
</div>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>