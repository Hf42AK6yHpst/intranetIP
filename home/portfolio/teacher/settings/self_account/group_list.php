<?php
// Using By :

##############################################
#
#	2020-01-10  Bill    [DM#3730]
#	- review sql to ensure include valid students / staffs only
#
#	2019-11-06  Bill    [2019-1101-1024-09073]
#	- review sql to ensure include valid students / staffs only
#
##############################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

// Template for student page
$linterface = new interface_html();

// Set the current page title
$CurrentPage = "Settings_SelfAccount";
$CurrentPageName = $ec_iPortfolio['self_account']." ".$iPort['menu']['settings'];

$lpf = new libportfolio2007();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

// Tab Menu Settings
//*** Current page index*****
// 0 -   * Components of OLE
// 1 -   * Group List
$currentPageIndex = 1;
$TabMenuArr = libpf_tabmenu::getSaSettingsTags($currentPageIndex);

$pageSizeChangeEnabled = true;

# Table SQL
$keyword = trim($keyword);
if($field == "") $field = 0;

//$name_field = getNameFieldByLang("b.");
$teacher_name_field = getNameFieldByLang("iu_t.","",true);
$teacher_name_field = "IF((um_t.user_id IS NOT NULL AND (um_t.status IS NULL OR um_t.status <> 'deleted')), $teacher_name_field, NULL)";
$student_name_field = getNameFieldByLang("iu_s.","",true);
$student_name_field = "IF((um_s.user_id IS NOT NULL AND (um_s.status IS NULL OR um_s.status <> 'deleted')), $student_name_field, NULL)";

$conds = "WHERE (g.group_name LIKE '%".str_replace(array("%","_"), array("\\%","\\_"), htmlspecialchars(addslashes($keyword),ENT_QUOTES))."%')";

$sql = "SELECT 
            DISTINCT g.group_id,
            g.group_name,
            GROUP_CONCAT(
                    $teacher_name_field ORDER BY $teacher_name_field SEPARATOR ', '
            ),
            GROUP_CONCAT(
                    $student_name_field ORDER BY $student_name_field SEPARATOR ', '
            ),
            CONCAT('<a class=\"tablelink\" href=\"group_user_list.php?group_id=', g.group_id, '\">', 
                    SUM(
                        IF(ug.user_id IS NOT NULL AND (
				            (um_s.user_id IS NOT NULL AND (um_s.status IS NULL OR um_s.status <> 'deleted')) OR 
				            (um_t.user_id IS NOT NULL AND (um_t.status IS NULL OR um_t.status <> 'deleted'))
			            ), 1, 0)
                    ),
                    '</a>' 
            ),
            g.inputdate
        FROM
            grouping g
            INNER JOIN mgt_grouping_function mgf ON (g.group_id = mgf.group_id AND INSTR(mgf.function_right, 'Profile:SelfAccount') > 0)
            LEFT JOIN user_group ug ON (g.group_id = ug.group_id)
            LEFT JOIN usermaster um_t ON (ug.user_id = um_t.user_id AND um_t.memberType = 'T')
            LEFT JOIN usermaster um_s ON (ug.user_id = um_s.user_id AND um_s.memberType = 'S')
            LEFT JOIN {$eclass_db}.user_course uc_t ON (um_t.user_id = uc_t.user_id AND uc_t.course_id = $ck_course_id)
            LEFT JOIN {$intranet_db}.INTRANET_USER iu_t ON (uc_t.user_email = iu_t.UserEmail AND iu_t.RecordType = 1)
            LEFT JOIN {$eclass_db}.user_course uc_s ON (um_s.user_id = uc_s.user_id AND uc_s.course_id = $ck_course_id)
            LEFT JOIN {$intranet_db}.INTRANET_USER iu_s ON (uc_s.user_email = iu_s.UserEmail AND iu_s.RecordType = 2)
        {$conds}
        GROUP BY
            ug.group_id ";
 //echo htmlspecialchars($sql);

# Table Info
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("group_id", "group_name", "count(DISTINCT ug.user_id)", "inputdate");
$li->db = $lpf->course_db;
$li->sql = $sql;
$li->page_size = ($numPerPage=="") ? $page_size : $numPerPage;
$li->no_col = sizeof($li->field_array) + 3;
$li->title = $i_admintitle_eclass;
$li->column_array = array(0,0,0,0,0,0,0,0);
$li->IsColOff = "IP25_table";
//2014-0226-1014-31184
$li->count_mode = 1;

// Table Column
$li->column_list .= "<th width='5%' class='tabletop tabletopnolink' style='vertical-align:middle'>&nbsp;#&nbsp;</th>\n";
$li->column_list .= "<th width='10%' class='tabletop' style='vertical-align:middle'>".$li->column(0, $Lang['iPortfolio']['GroupID'])."</th>\n";
$li->column_list .= "<th width='25%' class='tabletop' style='vertical-align:middle'>".$li->column(1, $Lang['iPortfolio']['GroupName'])."</th>\n";
$li->column_list .= "<th width='15%' class='tabletop tabletopnolink' style='vertical-align:middle'>{$Lang['iPortfolio']['TeacherTutor']}</th>\n";
$li->column_list .= "<th width='20%' class='tabletop tabletopnolink' style='vertical-align:middle'>{$iPort['usertype_s']}</th>\n";
$li->column_list .= "<th width='10%' class='tabletop' style='vertical-align:middle'>".$li->column(2, $i_eClassNumUsers)."</th>\n";
$li->column_list .= "<th width='15%' class='tabletop' style='vertical-align:middle'>".$li->column(3, $i_eClassInputdate)."</th>\n";

// Table Function Bar
$searchTag = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
$searchTag .= "<td><input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag .= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3")."</td>";
$searchTag .= "</tr></table>";

$html_instruction = $linterface->Get_Warning_Message_Box($i_Discipline_System_Conduct_Instruction, $Lang['iPortfolio']['GroupList_instruction']);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<br />
<form name="form1" method="get" action="group_list.php">
  <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td><?=$lpf->GET_TAB_MENU($TabMenuArr)?></td>
    </tr>
    <tr>
      <td align="center">
        <table width="96%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" class="tabletext">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
                </tr>
                <tr>
                  <td><?=$html_instruction?></td>
                </tr>
                <tr><td align="right"><?=$searchTag?></td></tr>
                <tr>
                  <td colspan="2">
                    <?=$li->display();?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br />

  <input type="hidden" name="pageNo" value="<?=$li->pageNo ?>" />
  <input type="hidden" name="order" value="<?=$li->order ?>">
  <input type="hidden" name="field" value="<?=$li->field ?>" />
  <input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
  <input type="hidden" name="numPerPage" value="<?=$li->page_size ?>" />
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>