<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
intranet_auth();
intranet_opendb();

$lpf = new libportfolio();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$sql =  "DELETE FROM {$lpf->course_db}.user_group WHERE group_id = {$group_id} AND user_id IN (".implode(",", $user_id).")";

$lpf->db_db_query($sql);


header("Location: group_user_list.php?group_id=$group_id&msg=remove");
intranet_closedb();
?>