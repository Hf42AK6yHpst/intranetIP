<?php
/*
 * 20170505 Pun
 *  - Fixed php5.4 display warning
 * 20140709 Ivan [A64049]
 * 	- added KIS student group remarks
 */
$conds = ($search_text == "") ? "" : " AND g.group_name LIKE '%".mysql_real_escape_string(stripslashes($search_text))."%'";
$search_text = htmlentities(stripslashes($search_text), ENT_QUOTES, "UTF-8");

//get the list

if ($order=="") $order=1;
if ($field=="") $field=0;
$pageSizeChangeEnabled = true;
$LibTable = new libpf_dbtable($field, $order, $pageNo);

$sql = "CREATE TEMPORARY TABLE {$eclass_db}.tempMemberCnt ";
$sql .= "SELECT group_id, count(DISTINCT iu.UserID) AS memberCnt ";
$sql .= "FROM {$lpf->course_db}.user_group ug ";
$sql .= "INNER JOIN {$lpf->course_db}.usermaster um ON ug.user_id = um.user_id ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON um.user_email = iu.UserEmail ";
$sql .= "WHERE (um.status IS NULL OR um.status <> 'deleted') ";
$sql .= "GROUP BY ug.group_id";
$lpf->db_db_query($sql);

$sql =  "SELECT g.group_name, CONCAT('<a href=\"index.php?group_type={$group_type}&task=listMember&group_id=', g.group_id, '\">', IFNULL(t_mc.memberCnt, 0), '</a>'), DATE_FORMAT(g.modified, '%Y-%m-%d %H:%i:%s'), CONCAT('<input type=\"checkbox\" name=\"group_id[]\" value=\"', g.group_id, '\">') ";
$sql .= "FROM grouping g ";
$sql .= "LEFT JOIN {$eclass_db}.tempMemberCnt AS t_mc ON g.group_id = t_mc.group_id ";
$sql .= "WHERE g.has_right = {$has_right} {$conds} ";

$LibTable->sql = $sql;
$LibTable->field_array = array("g.group_name", "IFNULL(t_mc.memberCnt, 0)", "g.modified");
$LibTable->db = $lpf->course_db;
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($num_per_page=="") ? $page_size : $num_per_page;
if ($page_size_change!="" && $li) $li->page_size = $numPerPage;
$LibTable->no_col = 5;
$LibTable->noNumber = false;
	
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
//$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";
$LibTable->column_array = array(0, 3, 0, 3, 0, 0, 0);

// TABLE COLUMN
$LibTable->column_list = "<tr class='tabletop'>\n";
$LibTable->column_list .= "<th width='5%' align='center' class='num_check'>#</th>\n";
$LibTable->column_list .= "<th width='40%'>".$LibTable->column(0,$Lang['iPortfolio']['GroupName'])."</th>\n";
$LibTable->column_list .= "<th width='30%' align='center' nowrap>".$LibTable->column(1,$Lang['iPortfolio']['NumGroupMember'])."</th>\n";
$LibTable->column_list .= "<th width='20%' nowrap>".$LibTable->column(2,$ec_iPortfolio['SAMS_last_update'])."</th>\n";
$LibTable->column_list .= "<th width='5%' align='center'>".$LibTable->check("group_id[]")."</th>\n";
$LibTable->column_list .= "</tr>\n";	

$MenuArr = array();
$MenuArr[] = array($Lang['iPortfolio']['GroupList'], "");
$PageNavigationDisplay = $linterface->GET_NAVIGATION($MenuArr);


if ($_SESSION["platform"]=="KIS" && $group_type==IPF_CFG_GROUP_SETTING_STD) {
	$htmlAry['remarksMsg'] = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['iPortfolio']['studentGroupRemark_KIS']);
}


$GroupListTable = $LibTable->displayPlain();
$GroupListTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
if ($LibTable->navigationHTML!="") {
  $GroupListTable .= "<tr class='tablebottom'>";
  $GroupListTable .= "<td  class=\"tabletext\" align=\"right\">";
  $GroupListTable .= $LibTable->navigationHTML;
  $GroupListTable .= "</td>";
  $GroupListTable .= "</tr>";
}
$GroupListTable .= "</table>";

$linterface->LAYOUT_START();
include_once("template/listGroup.tmpl.php");
$linterface->LAYOUT_STOP();
?>