<?php
include_once($PATH_WRT_ROOT."includes/libeclass.php");

$uids = $st;
$course_id = $lpf->course_id;
$lo = new libeclass($course_id);
$sql = "SELECT user_id, 
		IF('".$intranet_session_language."'='en',if(trim(CONCAT(lastname,' ',firstname))=null||trim(CONCAT(lastname,' ',firstname))='',chinesename,CONCAT(lastname,' ',firstname)),if(trim(chinesename)=null||trim(chinesename)='',CONCAT(lastname,' ',firstname),chinesename)) as Displayname, 
		memberType FROM $eclass_db.user_course WHERE user_id IN (".$uids.") AND course_id = ".$course_id;
$row = $lo->returnArray($sql,3);
$x .= "<table width=400 border=1 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr class='tabletop'>";
$x .= "<td  width=10>#</td>\n";
$x .= "<td  width=85% nowrap>$i_UserName</td>\n";
$x .= "<td  width=10% nowrap>$i_eClass_Identity</td>\n";
$x .= "</tr>\n";
for($i=0; $i<sizeof($row); $i++){
	$user_id = $row[$i][0];
	$Displayname = $row[$i][1];
	$member = $row[$i][2];
	$css = ($i%2==0) ? "" : "2";
	$x .= "<tr>";
	$x .= "<td class=tableContent$css>".($i+1)."</td>\n";
	$x .= "<td class=tableContent$css>$Displayname</td>\n";
	$x .= "<td class=tableContent$css>$member <input type=hidden name='user_id[]' value='$user_id'>";
	if (strtoupper($member)=="T")
	{
		$x .= "<input type=hidden name='teacher_removed[]' value='$user_id'>";
		$teacher_delete .= ($teacher_delete!="") ? ",".$user_id : $user_id;
	}
	$x .= "</td>\n";
	$x .= "</tr>\n";
}
$x .= "</table>\n";

if ($teacher_delete!="")
{
	######################### GET EXISTING TEACHER LIST #########################
	$ec_db_name = $lo->db_prefix."c$course_id";
	$sql = "SELECT user_id, LTRIM(CONCAT(ifnull(lastname,''), ' ', ifnull(firstname,''))) as t_name, user_email ";
	$sql .= "FROM ".$ec_db_name.".usermaster ";
	$sql .= "WHERE memberType='T' AND (status IS NULL OR status='') AND user_id NOT IN ($teacher_delete)";
	$sql .= "ORDER BY t_name ";
	$row_e = $lo->returnArray($sql, 3);

	$sql = "SELECT UserID, EnglishName, ChineseName, UserEmail ";
	$sql .= "FROM INTRANET_USER ";
	$sql .= "WHERE RecordType = 1 ";
	$sql .= "ORDER BY EnglishName ";
	$li = new libdb();
	$row_i = $li->returnArray($sql, 4);

	$sql = "SELECT user_email ";
	$sql .= "FROM ".$ec_db_name.".usermaster ";
	$sql .= "WHERE user_id IN ($teacher_delete)";
	$row_d = $lo->returnArray($sql, 1);
	$exclus_email = array();
	for ($i=0; $i<sizeof($row_d); $i++)
	{
		$exclus_email[] = trim($row_d[$i][0]);
	}

	$teacher_exist_list = "<select name='teacher_benefit'>\n";
	$teacher_exist_list .= "<option value='0'>".$i_ec_file_no_transfer."</option>\n";
	$teacher_exist_list .= "<option value=''>____________________</option>";
	$teacher_exist_list .= "<option value=''>&nbsp;-- ".$i_ec_file_exist_teacher." --&nbsp;</option>\n";
	$cours_teacher_size = sizeof($row_e);
	for ($i=0; $i<$cours_teacher_size; $i++)
	{
		$teacher_exist_list .= "<option value='".$row_e[$i][0]."'>".$row_e[$i][1]."</option>\n";
		$exclus_email[] = trim($row_e[$i][2]);
	}

	$teacher_exist_list .= "<option value=''>____________________</option>";
	$teacher_exist_list .= "<option value=''>&nbsp;-- ".$i_ec_file_not_exist_teacher." --&nbsp;</option>\n";
	for ($i=0; $i<sizeof($row_i); $i++)
	{
		if (!in_array($row_i[$i][3], $exclus_email))
		{
			$chiname = ($row_i[$i][2]==""? "": "(".$row_i[$i][2].")" );
			$teacher_exist_list .= "<option value='".$row_i[$i][0]."'>".$row_i[$i][1]." $chiname</option>\n";
		}
	}

	$teacher_exist_list .= "</select>\n";
	$y = "<br><br><br>";
	$y .= "<u>" . $i_ec_file_msg_transfer . "</u><br><br><blockquote>" . $teacher_exist_list ."</blockquote>";
}
?>
<script language="javascript">
function checkform(obj){
	if (typeof(obj.teacher_benefit)!="undefined")
	{
		if (obj.teacher_benefit.options[obj.teacher_benefit.selectedIndex].value=="")
		{
			alert("<?=$i_ec_file_warning2 ?>");
			return false;
		}
		if (confirm("<?=$i_ec_file_user_delete_confirm?>"))
		{			
			if (obj.teacher_benefit.selectedIndex><?=$cours_teacher_size?>+4)
			{
				obj.is_user_import.value = 1;
				if (confirm("<?=$i_ec_file_confirm2?>"))
				{
					return true;
				} else
				{
					return false;
				}
			} else
			{
				return true;
			}
		} else
		{
			return false;
		}
	} else
	{
		return true;
	}
}
</script>
<html>
<head>
<META http-equiv=Content-Type content='text/html; charset=<?=$charset?>'></head>
<script language=JavaScript src=/lang/script.<?php echo $intranet_session_language; ?>.js></script>
<script language=JavaScript1.2 src=/templates/script.js></script>
<link rel=stylesheet href=/templates/style.css>
<link href="/templates/2020a/css/content_25.css" rel="stylesheet" type="text/css">
<body topmargin=0 leftmargin=0 marginheight=0 marginwidth=0 bgcolor="#F0FAFA">
<table width=100% height=100% border=0 cellpadding=0 cellspacing=0>
<tr><td width="605" bgcolor="#FFFFFF">
<form name="form1" method="post" action="index.php" onSubmit="return checkform(this)">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>

<u><?= $i_ec_file_user_delete ?></u>
<br><br>
<?= $x . $y ?>

</blockquote>
<br/><br/>
<div class="edit_bottom">
	<span> </span>
	<p class="spacer"></p>
	<input class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_submit?>" type="submit">
	<input class="formbutton" onclick="parent.tb_remove()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" type="button">
	<p class="spacer"></p>
</div>
</td>
</tr>
</table>

<input type="hidden" name="task" value="deleteTeacherUpdate" />
<input type=hidden name=course_id value=<?php echo $lo->course_id; ?>>
<input type='hidden' name='is_user_import' value='0'>
</form>

</td></tr></table>
</body>
</html>
<?php
eclass_closedb();
?>