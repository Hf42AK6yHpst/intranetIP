<?php
/*
 * Change Log:
 * Date	2017-05-31 Villa #E117855
 * -	Comment unknown object $li without created
 */


$lpf_mgmt_group = new libpf_mgmt_group();
$lpf_mgmt_group->setGroupID($group_id);
$lpf_mgmt_group->setGroupProperty();
$group_name = $lpf_mgmt_group->getGroupName();
//$lpf_mgmt_group->setGroupMember();

$conds = ($search_text == "") ? "" : " AND (iu.EnglishName LIKE '%".mysql_real_escape_string(stripslashes($search_text))."%' OR iu.ChineseName LIKE '%".mysql_real_escape_string(stripslashes($search_text))."%')";
$search_text = htmlentities(stripslashes($search_text), ENT_QUOTES, "UTF-8");

//get the list
//define('isStudentTypeGroup',0);
//define('isAdminTypeGroup',1);
$group_type = ($group_type == '') ? $ipf_cfg['GroupSetting']['isStudentTypeGroup']: $group_type;  // default 0 --> student type group

$dbNOCol = 3;  
$dbArrayColumn = array();
$dbFieldArry = array();
$dbSqlStudentField = '';
if($group_type == $ipf_cfg['GroupSetting']['isAdminTypeGroup']){
	$dbFieldArry = array("DisplayName");
	$dbArrayColumn = array(0,0,0);
	$dbNOCol = 3;
}else{
	$dbFieldArry = array('DisplayName','ClassName','ClassNumber');
	$dbArrayColumn = array(0,0,0,0,0);
	$dbNOCol = 5;
	$dbSqlStudentField = ', iu.classname as \'ClassName\',iu.classnumber as \'ClassNumber\'';
}

/******* added by connie ******/
$dbFieldArry = array('DisplayName','ClassName','ClassNumber','identity');
$dbArrayColumn = array(0,0,0,0,0,0);
$dbNOCol = 6;
$EmptySymbol = '--';
$dbSqlStudentField = ',(CASE
                    		WHEN iu.classname is NULL OR iu.classname = "" THEN
                       		 	"'.$EmptySymbol .'"
                    		ELSE
                        		iu.classname
                    		END) as \'ClassName\',
						(CASE
                    		WHEN iu.classnumber is NULL OR iu.classnumber = "" THEN
                       		 	"'.$EmptySymbol .'"
                    		ELSE
                        		iu.classnumber
                    		END) as \'ClassNumber\',			
						(CASE
                    		WHEN memberType  = "S" THEN
                       		 	"'.$iPort['usertype_s'] .'"
                    		ELSE
                        		"'.$iPort['usertype_t'].'"
                    		END) as \'identity\' ';
						
/******* END added by connie ******/

if ($order=="") $order=1;
if ($field=="") $field=0;
$pageSizeChangeEnabled = true;
$LibTable = new libpf_dbtable($field, $order, $pageNo);

$sql =  "SELECT ".getNameFieldByLang("iu.")." AS DisplayName ".$dbSqlStudentField." , CONCAT('<input type=\"checkbox\" name=\"teacher_id[]\" value=\"', um.user_id, '\">') ";
$sql .= "FROM user_group ug ";
$sql .= "INNER JOIN usermaster um ON ug.user_id = um.user_id ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON iu.UserEmail = um.user_email ";
$sql .= "WHERE um.status is null and ug.group_id = {$group_id} {$conds} ";
//$sql .= "order by iu.ClassName,iu.ClassNumber,iu.EnglishName";
//hdebug_r($sql);
$LibTable->sql = $sql;

$LibTable->field_array = $dbFieldArry;
$LibTable->db = $lpf->course_db;

$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($num_per_page=="") ? $page_size : $num_per_page;
// if ($page_size_change!="") $li->page_size = $numPerPage;

$LibTable->no_col = $dbNOCol;
$LibTable->noNumber = false;
	
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
//$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";


// TABLE COLUMN
$LibTable->column_list = "<tr class='tabletop'>\n";
$LibTable->column_list .= "<th width='30' align='center' class='num_check'>#</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(0,$Lang['iPortfolio']['GroupMemberName'])."</th>\n";

//if($group_type == isStudentTypeGroup){
//	$LibTable->column_list .= "<th>".$LibTable->column(1,$ec_iPortfolio['class'])."</th>\n";
//	$LibTable->column_list .= "<th>".$LibTable->column(2,$ec_iPortfolio['number'])."</th>\n";
//}
/******* added by connie ******/
$LibTable->column_list .= "<th>".$LibTable->column(1,$ec_iPortfolio['ClassName'])."</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(2,$ec_iPortfolio['ClassNumber'])."</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(3,$ec_iPortfolio['identity'])."</th>\n";
/******* END added by connie ******/

$LibTable->column_list .= "<th width='30'>".$LibTable->check("teacher_id[]")."</th>\n";
$LibTable->column_list .= "</tr>\n";	

$LibTable->column_array = $dbArrayColumn;

$MenuArr = array();
$MenuArr[] = array($Lang['iPortfolio']['GroupList'], "index.php?group_type={$group_type}&task=listGroup");
$MenuArr[] = array($group_name, "");
$PageNavigationDisplay = $linterface->GET_NAVIGATION($MenuArr);


$MemberListTable = $LibTable->displayPlain();
$MemberListTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
if ($LibTable->navigationHTML!="") {
  $MemberListTable .= "<tr class='tablebottom'>";
  $MemberListTable .= "<td  class=\"tabletext\" align=\"right\">";
  $MemberListTable .= $LibTable->navigationHTML;
  $MemberListTable .= "</td>";
  $MemberListTable .= "</tr>";
}
$MemberListTable .= "</table>";


$linterface->LAYOUT_START();
include_once("template/listMember.tmpl.php");
$linterface->LAYOUT_STOP();

?>