<?php
## Modifying By
##
## Date : 2018-01-17 Omas modified sql to exclude suspended teachers

include_once("../../../../../includes/libgrouping.php");
$li = new libgrouping();/**/
$course_db = $lpf->course_db;
$t_namefield = getNameFieldByLang("iu.");
$s_namefield = getNameFieldWithClassNumberByLang("iu.");

$SelectedMemberListSelection = "<select name=\"uID[]\" id=\"SelectedUser\" size=\"10\" style=\"width:100%\" multiple>";
$SelectedMemberListSelection .= "</select>";

$row = $li->returnUserForTypeExcludeGroup(1,'', " AND RecordStatus = 1");
$row2 = $li->returnUserForTypeExcludeCourse(1,$lpf->course_id, " AND u.RecordStatus = 1");
for($i=0; $i<count($row2); $i++) {
	$row2[$i] = $row2[$i][0];
}


$MemberListSelection = "<select id=\"User2Select\" size=\"10\" style=\"width:100%\" multiple>";
$MemberListSelection .= "<optgroup label=\"{$Lang['Identity']['TeachingStaff']}\">";

	for($i=0; $i<sizeof($row); $i++){
		if(in_array($row[$i][0], $row2)){
			$t_uid = $row[$i][0];
			$t_uname = $row[$i][1];
		$b++;
			$MemberListSelection .= "<option value=\"{$t_uid}\">{$t_uname}</option>";
		}
	}
$MemberListSelection .= "</optgroup>";
$MemberListSelection .= "</select>";

$WarningMsg = $linterface->Get_Warning_Message_Box($eDiscipline["Instruction"], $Lang['iPortfolio']['onlyPortfolioUser']);

include_once("template/newTeacher.tmpl.php");
?>