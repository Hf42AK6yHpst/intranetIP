<?php
//Using:

/*******************************
 * 2016-07-15 Henry HM
 * - Initial $li if it is not initialized (PHP 5.4+)
 * 
 *******************************/
 
$lpf_mgmt_group = new libpf_mgmt_group();
$lpf_mgmt_group->setGroupID($group_id);
$lpf_mgmt_group->setGroupProperty();
$group_name = $lpf_mgmt_group->getGroupName();
//$lpf_mgmt_group->setGroupMember();
$_handleLastNameSql = 'if(lastname is NULL or trim(lastname) = \'\' , \'\',lastname)';
$conds = ($search_text == "") ? "" : ($intranet_session_language == "en")?" AND (CONCAT({$_handleLastNameSql},' ',firstname) LIKE '%".mysql_real_escape_string(stripslashes($search_text))."%')":" AND (Chinesename LIKE '%".mysql_real_escape_string(stripslashes($search_text))."%')";
$search_text = htmlentities(stripslashes($search_text), ENT_QUOTES, "UTF-8");

//get the list
define('isTeacherTypeGroup',2);

$dbNOCol = 3;  
$dbArrayColumn = array();
$dbFieldArry = array();
$dbSqlStudentField = '';


$dbFieldArry = array('DisplayName','identity');
$dbArrayColumn = array(0,0,0,0);
$dbNOCol = 4;
$EmptySymbol = '--';

if ($order=="") $order=1;
if ($field=="") $field=0;
$pageSizeChangeEnabled = true;
$LibTable = new libpf_dbtable($field, $order, $pageNo);

$sql = "SELECT
		IF('".$intranet_session_language."'='en',if(trim(CONCAT({$_handleLastNameSql},' ',firstname))=null||trim(CONCAT({$_handleLastNameSql},' ',firstname))='',chinesename,CONCAT({$_handleLastNameSql},' ',firstname)),if(trim(chinesename)=null||trim(chinesename)='',CONCAT({$_handleLastNameSql},' ',firstname),chinesename)) as Displayname,  
		'".$iPort['usertype_t']."' as identity,
		CONCAT('<input type=\"checkbox\" name=\"teacher_id[]\" value=\"', user_id, '\">') FROM $eclass_db.user_course WHERE course_id = 
		".$lpf->course_id." and memberType='T' {$conds}";

$LibTable->sql = $sql;

$LibTable->field_array = $dbFieldArry;
$LibTable->db = $lpf->course_db;

$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($num_per_page=="") ? $page_size : $num_per_page;
if ($page_size_change!=""){
	if(!isset($li)){
		$li = new stdClass();
	}
	$li->page_size = $numPerPage;	
}

$LibTable->no_col = $dbNOCol;
$LibTable->noNumber = false;
	
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
//$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";


// TABLE COLUMN
$LibTable->column_list = "<tr class='tabletop'>\n";
$LibTable->column_list .= "<th width='30' align='center' class='num_check'>#</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(0,$Lang['iPortfolio']['GroupMemberName'])."</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(3,$ec_iPortfolio['identity'])."</th>\n";
$LibTable->column_list .= "<th width='30'>".$LibTable->check("teacher_id[]")."</th>\n";
$LibTable->column_list .= "</tr>\n";	

$LibTable->column_array = $dbArrayColumn;

$MenuArr = array();
$MenuArr[] = array($Lang['iPortfolio']['GroupList'], "index.php?group_type={$group_type}&task=listGroup");
$MenuArr[] = array($group_name, "");
$PageNavigationDisplay = $linterface->GET_NAVIGATION($MenuArr);


$MemberListTable = $LibTable->displayPlain();
$MemberListTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
if ($LibTable->navigationHTML!="") {
  $MemberListTable .= "<tr class='tablebottom'>";
  $MemberListTable .= "<td class=\"tabletext\" align=\"right\">";
  $MemberListTable .= $LibTable->navigationHTML;
  $MemberListTable .= "</td>";
  $MemberListTable .= "</tr>";
}
$MemberListTable .= "</table>";


$linterface->LAYOUT_START();
include_once("template/listTeacher.tmpl.php");
$linterface->LAYOUT_STOP();
?>