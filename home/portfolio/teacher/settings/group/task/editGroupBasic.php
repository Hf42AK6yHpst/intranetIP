<?php

$lpf_mgmt_group = new libpf_mgmt_group();
$lpf_mgmt_group->setGroupID($group_id);
$lpf_mgmt_group->setGroupProperty();

$BoxTitle = $button_edit;
$GroupName = $lpf_mgmt_group->getGroupName();
$GroupName = htmlentities($GroupName, ENT_COMPAT, "UTF-8");
$GroupDesc = $lpf_mgmt_group->getGroupDesc();
$GroupDesc = htmlentities($GroupDesc, ENT_COMPAT, "UTF-8");
$GroupID = $lpf_mgmt_group->getGroupID();

$GroupNameInput = "<input type=\"text\" name=\"group_name\" id=\"group_name\" value=\"{$GroupName}\" size=\"25\" maxlength=\"20\" />";
$GroupDescInput = "<textarea name=\"group_desc\" cols=\"40\" rows=\"5\" wrap=\"virtual\">{$GroupDesc}</textarea>";

# tab menu
$TabMenuArr = libpf_tabmenu::getGroupSettingsTags($group_type, 0);
$tab_menu_html = $lpf_ui->GET_TAB_MENU($TabMenuArr);

# navigation
$MenuArr = array();
$MenuArr[] = array($Lang['iPortfolio']['GroupList'], "index.php?group_type=".$group_type);
$MenuArr[] = array($GroupName, "");
$navigation_html = $linterface->GET_NAVIGATION($MenuArr);

$linterface->LAYOUT_START();
include_once("template/updateGroupBasic.tmpl.php");
$linterface->LAYOUT_STOP();
?>