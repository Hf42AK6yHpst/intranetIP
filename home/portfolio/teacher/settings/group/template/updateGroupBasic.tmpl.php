<script language="JavaScript">

function change_edit_type(edit_type){
	switch(edit_type)
	{
		case <?=IPF_CFG_GROUP_SETTING_BASIC?>:
			task = "editGroupBasic";
			break;
		case <?=IPF_CFG_GROUP_SETTING_RIGHT?>:
			task = "editGroupRight";
			break;
	}

	window.location = "index.php?group_type=<?=$group_type?>&task="+task+"&group_id=<?=$group_id?>";
}

function checkform(formObj)
{
  var group_name = document.getElementById("group_name");
	
	if(group_name.value == "")
	{
		alert("<?=$Lang['Group']['EmptyGroupName']?>");
		group_name.focus();
		return false;
	}

	return true;  
}

</script>

<form name="form1" action="index.php" method="post" onSubmit="return checkform(this)">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	  <tr>
	    <td class="navigation"><?=$navigation_html ?><span style="float:right"><?=$linterface->GET_SYS_MSG($msg)?></span></td>
	  </tr>
	  <tr>
	    <td align="center">
	      <?=$tab_menu_html?>
	  	</td>
	  </tr>
	  <tr>
	    <td>
		    <table class="form_table inside_form_table">
		      <col class="field_title" />
		      <col  class="field_c" />
		      <tr>
		        <td><?=$Lang['iPortfolio']['GroupName']?></td>
		        <td>:</td>
		        <td><?=$GroupNameInput?></td>
		      </tr>
		      <tr>
		        <td><?=$iPort["description"]?></td>
		        <td>:</td>
		        <td><?=$GroupDescInput?></td>
		      </tr>
		    </table>
		  </td>
		</tr>
	</table>
	<div class="edit_bottom">
	  <p class="spacer"></p>
	  <input type="submit" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_submit?>" />
	  <input type="button" class="formbutton" onclick="window.location='index.php?group_type=<?=$group_type?>&task=listGroup'" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" />
	  <p class="spacer"></p>
	</div>
	
	<input type="hidden" name="group_type" value="<?=$group_type?>" />
	<input type="hidden" name="task" value="updateGroupBasic" />
	<input type="hidden" name="group_id" value="<?=$GroupID?>" />
</form>