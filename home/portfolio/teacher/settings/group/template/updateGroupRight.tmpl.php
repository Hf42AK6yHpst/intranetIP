<script language="JavaScript">

function change_edit_type(edit_type){
	switch(edit_type)
	{
		case <?=IPF_CFG_GROUP_SETTING_BASIC?>:
			task = "editGroupBasic";
			break;
		case <?=IPF_CFG_GROUP_SETTING_RIGHT?>:
			task = "editGroupRight";
			break;
	}

	window.location = "index.php?group_type=<?=$group_type?>&task="+task+"&group_id=<?=$group_id?>";
}

function disable_component(mgmt_all){
	if(mgmt_all)
	{
		$("#ele_table").find("input[name='ELE_ID[]']").attr("disabled", true);
	}
	else
	{
		$("#ele_table").find("input[name='ELE_ID[]']").attr("disabled", false);
	}
}

$(document).ready(function(){

	$("input[name='gfunction[]']").click(function(){
		if($(this).val() == "Profile:OLR")
		{
			var isChecked = $(this).attr("checked");
		
			if(isChecked)
			{
				// show component selection
				$("#ele_table").show();
				$("#ele_table").find("input").attr("disabled", false);
				
				// set disable status for components
				var mgmt_all = ($("#ele_table").find("input[name=ele_mgmt]:checked").val() == 1);
				disable_component(mgmt_all);
			}
			else
			{
				// hide component selection
				$("#ele_table").hide();
				$("#ele_table").find("input").attr("disabled", true);
				
				// set disable status for components
				var mgmt_all = ($("#ele_table").find("input[name=ele_mgmt]:checked").val() == 1);
				disable_component(mgmt_all);
			}
		}
	});
	
	$("#ele_table").find("input[name=ele_mgmt]").click(function(){
		var mgmt_all = ($(this).val() == 1);
		disable_component(mgmt_all);
	});

});

</script>

<form name="form1" action="index.php" method="post">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	  <tr>
	    <td class="navigation"><?=$navigation_html ?><span style="float:right"><?=$linterface->GET_SYS_MSG($msg)?></td>
	  </tr>
	  <tr>
	    <td align="center">
	      <?=$tab_menu_html?>
	  	</td>
	  </tr>
	  <tr>
	    <td>
		    <?=$html_function_table?>
		  </td>
		</tr>
	</table>
  <div class="edit_bottom">
    <p class="spacer"></p>
    <input type="submit" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_submit?>" />
    <input type="button" class="formbutton" onclick="window.location='index.php?group_type=<?=$group_type?>&task=listGroup'" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" />
    <p class="spacer"></p>
  </div>

	<input type="hidden" name="group_type" value="<?=$group_type?>" />
	<input type="hidden" name="task" value="updateGroupRight" />
	<input type="hidden" name="group_id" value="<?=$group_id?>" />
</form>