<?php

$lpf_mgmt_group = new libpf_mgmt_group();
$lpf_mgmt_group->setGroupID($group_id);
$lpf_mgmt_group->setGroupProperty();
$group_title = $lpf_mgmt_group->getTitle();
//$lpf_mgmt_group->setGroupMember();

$conds = ($search_text == "") ? "" : " AND (iu.EnglishName LIKE '%".mysql_real_escape_string(stripslashes($search_text))."%' OR iu.ChineseName LIKE '%".mysql_real_escape_string(stripslashes($search_text))."%')";
$search_text = htmlentities(stripslashes($search_text), ENT_QUOTES, "UTF-8");

//get the list

if ($order=="") $order=1;
if ($field=="") $field=0;
$pageSizeChangeEnabled = true;
$LibTable = new libpf_dbtable($field, $order, $pageNo);

$sql =  "SELECT ".getNameFieldByLang("iu.")." AS DisplayName, CONCAT('<input type=\"checkbox\" name=\"teacher_id[]\" value=\"', iu.UserID, '\">') ";
$sql .= "FROM {$eclass_db}.PORTFOLIO_GROUP_MEMBER pgm ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ";
$sql .= "ON iu.UserID = pgm.UserID ";
$sql .= "WHERE pgm.GroupID = {$group_id} {$conds} ";

$LibTable->sql = $sql;
$LibTable->field_array = array("DisplayName");
$LibTable->db = $intranet_db;
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($num_per_page=="") ? $page_size : $num_per_page;
if ($page_size_change!="") $li->page_size = $numPerPage;
$LibTable->no_col = 3;
$LibTable->noNumber = false;
	
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
//$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";
$LibTable->column_array = array(0, 0, 3, 3, 0, 0, 0);

// TABLE COLUMN
$LibTable->column_list = "<tr class='tabletop'>\n";
$LibTable->column_list .= "<th width='30' align='center' class='num_check'>#</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(0,$Lang['iPortfolio']['GroupMemberName'])."</th>\n";
$LibTable->column_list .= "<th width='30'>".$LibTable->check("teacher_id[]")."</th>\n";
//$LibTable->column_list .= "<th nowrap>".$LibTable->column(1,$ec_iPortfolio['SAMS_last_update'])."</th>\n";
$LibTable->column_list .= "</tr>\n";	

$MenuArr = array();
$MenuArr[] = array($Lang['iPortfolio']['GroupList'], "index.php?task=listGroup");
$MenuArr[] = array($group_title, "");
$PageNavigationDisplay = $linterface->GET_NAVIGATION($MenuArr);


$MemberListTable = $LibTable->displayPlain();
$MemberListTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
if ($LibTable->navigationHTML!="") {
  $MemberListTable .= "<tr class='tablebottom'>";
  $MemberListTable .= "<td  class=\"tabletext\" align=\"right\">";
  $MemberListTable .= $LibTable->navigationHTML;
  $MemberListTable .= "</td>";
  $MemberListTable .= "</tr>";
}
$MemberListTable .= "</table>";


$linterface->LAYOUT_START();
include_once("template/listMember.tmpl.php");
$linterface->LAYOUT_STOP();

?>