<?php
## Modifying By

$namefield = getNameFieldByLang("iu.");

$SelectedMemberListSelection = "<select name=\"TeacherID[]\" id=\"SelectedTeacher\" size=\"10\" style=\"width:100%\" multiple>";
$SelectedMemberListSelection .= "</select>";

$sub_sql = "SELECT ";
$sub_sql .= "iu.UserID ";
$sub_sql .= "FROM {$eclass_db}.PORTFOLIO_GROUP_MEMBER pgm ";
$sub_sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ";
$sub_sql .= "ON iu.UserID = pgm.UserID ";
$sub_sql .= "WHERE pgm.GrouPID = {$GroupID}";

$sql = "SELECT ";
$sql .= "iu.UserID, {$namefield} AS DisplayName ";
$sql .= "FROM {$eclass_db}.user_course uc ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ";
$sql .= "ON iu.UserEmail = uc.user_email ";
$sql .= "WHERE uc.course_id = {$ck_course_id} AND iu.RecordType = 1 ";
$sql .= "AND iu.UserID NOT IN ({$sub_sql})";
$teacher_arr = $lpf->returnArray($sql);

$MemberListSelection = "<select id=\"Teacher2Select\" size=\"10\" style=\"width:100%\" multiple>";
for($i=0, $i_max=count($teacher_arr); $i<$i_max; $i++)
{
	$t_uid = $teacher_arr[$i]["UserID"];
	$t_uname = $teacher_arr[$i]["DisplayName"];

	$MemberListSelection .= "<option value=\"{$t_uid}\">{$t_uname}</option>";
}
$MemberListSelection .= "</select>";

include_once("template/newMember.tmpl.php");
?>