<?php

$lpf_mgmt_group = new libpf_mgmt_group();

$lpf_mgmt_group->setTitle($title);

if(!empty($group_id))
{
	$lpf_mgmt_group->setInputBy($UserID);
	$lpf_mgmt_group->setModifyBy($UserID);
  $lpf_mgmt_group->setGroupID($group_id);

  $lpf_mgmt_group->UPDATE_GROUP();
  $msg = "update";
}
else
{
	$lpf_mgmt_group->setModifyBy($UserID);
	$lpf_mgmt_group->setType($ipf_cfg["PORTFOLIO_GROUP_Type"]["ole_mgmt"]);

	$lpf_mgmt_group->ADD_GROUP();
	$msg = "add";
}

?>

<script>
  parent.index_reload_page('<?=$msg?>'); 
</script>
