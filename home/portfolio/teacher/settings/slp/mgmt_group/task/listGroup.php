<?php

$conds = ($search_text == "") ? "" : " AND Title LIKE '%".mysql_real_escape_string(stripslashes($search_text))."%'";
$search_text = htmlentities(stripslashes($search_text), ENT_QUOTES, "UTF-8");

//get the list

if ($order=="") $order=1;
if ($field=="") $field=0;
$pageSizeChangeEnabled = true;
$LibTable = new libpf_dbtable($field, $order, $pageNo);

$sql =  "SELECT CONCAT('<a href=\"index.php?task=listMember&group_id=', pg.GroupID, '\">', pg.Title, '</a>'), COUNT(DISTINCT pgm.UserID) AS memberCnt, DATE_FORMAT(pg.DateModified, '%Y-%m-%d %H:%i:%s'), CONCAT('<input type=\"checkbox\" name=\"group_id[]\" value=\"', pg.GroupID, '\">') ";
$sql .= "FROM {$eclass_db}.PORTFOLIO_GROUP pg ";
$sql .= "LEFT JOIN {$eclass_db}.PORTFOLIO_GROUP_MEMBER pgm ";
$sql .= "ON pg.GroupID = pgm.GroupID ";
$sql .= "WHERE 1 {$conds} ";
$sql .= "GROUP BY pgm.GroupID";

$LibTable->sql = $sql;
$LibTable->field_array = array("Title", "memberCnt", "DateModified");
$LibTable->db = $intranet_db;
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($num_per_page=="") ? $page_size : $num_per_page;
if ($page_size_change!="") $li->page_size = $numPerPage;
$LibTable->no_col = 5;
$LibTable->noNumber = false;
	
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
//$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";
$LibTable->column_array = array(0, 3, 0, 3, 0, 0, 0);

// TABLE COLUMN
$LibTable->column_list = "<tr class='tabletop'>\n";
$LibTable->column_list .= "<th width='5%' align='center' class='num_check'>#</th>\n";
$LibTable->column_list .= "<th width='40%'>".$LibTable->column(0,$Lang['iPortfolio']['GroupName'])."</th>\n";
$LibTable->column_list .= "<th width='10%' align='center' nowrap>".$LibTable->column(1,$Lang['iPortfolio']['NumGroupMember'])."</th>\n";
$LibTable->column_list .= "<th width='40%' nowrap>".$LibTable->column(2,$ec_iPortfolio['SAMS_last_update'])."</th>\n";
$LibTable->column_list .= "<th width='5%' align='center'>".$LibTable->check("group_id[]")."</th>\n";
$LibTable->column_list .= "</tr>\n";	

$MenuArr = array();
$MenuArr[] = array($Lang['iPortfolio']['GroupList'], "");
$PageNavigationDisplay = $linterface->GET_NAVIGATION($MenuArr);


$GroupListTable = $LibTable->displayPlain();
$GroupListTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
if ($LibTable->navigationHTML!="") {
  $GroupListTable .= "<tr class='tablebottom'>";
  $GroupListTable .= "<td  class=\"tabletext\" align=\"right\">";
  $GroupListTable .= $LibTable->navigationHTML;
  $GroupListTable .= "</td>";
  $GroupListTable .= "</tr>";
}
$GroupListTable .= "</table>";

$linterface->LAYOUT_START();
include_once("template/listGroup.tmpl.php");
$linterface->LAYOUT_STOP();
?>