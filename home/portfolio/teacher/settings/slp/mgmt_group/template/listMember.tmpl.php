<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script language="JavaScript">
function index_reload_page(msg){
  this.tb_remove();     //Close thickbox
  window.location.reload();      //Refresh page
}

function delete_member(obj,element){
	var check_size = $("input[name='"+element+"']:checked").length;

  if(check_size==0)
  {
		alert(globalAlertMsg2);
	}
  else{
    if(confirm(globalAlertMsg3)){	            
      obj.task.value = 'deleteMember';                
      obj.submit();				             
    }
  }

}
</script>

<form name="form1" id="form1" action="index.php" method="get">
<!--###### Content Board Start ######-->
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="bottom">
    <!-- ###### Tabs Start ######-->
    <?=$TabMenuDisplay?>
    <!-- ###### Tabs End ######-->					
    </td>
  </tr>

  <tr> 
    <td class="main_content">
      <div class="navigation">
        <?=$PageNavigationDisplay?>
      </div>
      <div class="content_top_tool">
        <div class="Conntent_tool"> <a href="index.php?task=newMember&GroupID=<?=$group_id?>&KeepThis=true&TB_iframe=true&height=380&width=750" class="new thickbox" title="<?=$button_new?>"> <?=$button_new?></a></div>
        <div class="Conntent_search">
          <input name="search_text" value="<?=$search_text?>" type="text"/>
        </div>
        <br style="clear:both" />
      </div>
      <div class="table_board">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="bottom"><div class="table_filter"></div></td>
            <td valign="bottom"><div class="common_table_tool">
              <a href="javascript:delete_member(document.form1,'teacher_id[]')" class="tool_delete" title="Delete"><?=$ec_iPortfolio['delete']?></a>
            </div></td>
          </tr>
        </table>
			        
        <?=$MemberListTable?>
      </div>
    </td>
  </tr>
</table>
<!--###### Content Board End ######-->
<input type="hidden" name="order" value=<?=$order?> />
<input type="hidden" name="pageNo" value=<?=$li->pageNo?> />
<input type="hidden" name="field" value=<?=$field?> />
<input type="hidden" name="page_size_change" value=<?=$page_size_change?> />
<input type="hidden" name="numPerPage" value=<?=$li->page_size?> />

<input type="hidden" name="group_id" value="<?=$group_id?>" />
<input type="hidden" name="task" value="listMember" />
<input type="hidden" name="status" />
</form>