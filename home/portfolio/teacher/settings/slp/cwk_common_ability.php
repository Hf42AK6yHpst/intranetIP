<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cwk/lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";

//$luser = new libuser($UserID);
$lpf = new libportfolio2007();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

##################################################
############  Cotnent Here #######################
##################################################

// page number settings
if ($order=="") $order=1;
if ($field=="") $field=0;

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;

$sql =  "
          SELECT
            CONCAT('<a href=\"cwk_edit_ability.php?AbilityID=', AbilityID ,'\">', AbilityCode, '</a>'),
            AbilityName,
            CONCAT('<input type=checkbox name=\"AbilityID[]\" value=', AbilityID ,'>')
          FROM
            {$eclass_db}.CWK_COMMON_ABILITY
        ";

$li = new libpf_dbtable($field, $order, $pageNo);
$li->field_array = array("AbilityCode", "AbilityName");
$li->sql = $sql;
$li->no_col = 4;
$li->table_tag = "<table bgcolor='#cccccc' border='0' cellpadding='4' cellspacing='1' width='100%'>";
$li->row_alt = array("#FFFFFF", "#F3F3F3");
$li->row_height = 25;
$li->plain_column = 1;
$li->plain_column_css = "tablelink";
//$li->record_num_mode = "iportfolio"; // for school based scheme total record display
//$li->IsColOff = 2;
$li->title = $iPort['table']['records'];

// TABLE COLUMN
$li->column_list .= "<td class='tabletop tabletopnolink' height='25' align='center'>#</span></td>\n";
$li->column_list .= "<td width='45%' nowrap='nowrap'>".$li->column(0,$Lang["Cust_Cwk"]["CommonAbilityCode"], 1)."</td>\n";
$li->column_list .= "<td width='45%' nowrap='nowrap'>".$li->column(1,$Lang["Cust_Cwk"]["CommonAbilityName"], 1)."</td>\n";
$li->column_list .= "<td>".$li->check("AbilityID[]")."</td>\n";
$li->column_array = array(0,0,0,3);
##################################################
##################################################
##################################################

// Table Action Button (JS, Image, Image_name, title)
$TableActionArr = array();
$TableActionArr[] = array("javascript:checkEdit(document.form1,'AbilityID[]','cwk_edit_ability.php')", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_edit.gif", "imgEdit", $ec_iPortfolio['edit'], "tool_edit");
$TableActionArr[] = array("javascript:checkRemove(document.form1,'AbilityID[]','cwk_ability_remove.php')", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_delete.gif", "imgDelete", $ec_iPortfolio['delete'], "tool_delete");

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_CWK);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<form name="form1" action="cwk_common_ability.php" method="POST">
  <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><?=$lpf->GET_NEW_BUTTON("cwk_new_ability.php", $iPort["new"])?></td>
            <td align="right"><?=$linterface->GET_SYS_MSG($msg)?></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right" valign="bottom">
              <?=$lpf->GET_TABLE_ACTION_BTN($TableActionArr)?>
            </td>
          </tr>
        </table>
        <?=$li->displayPlain()?>
<?
if ($li->navigationHTML!="")
{
	echo "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
	echo "<tr class='tablebottom'>";
	echo "<td class=\"tabletext\" align=\"right\">";
	echo $li->navigation(1);
	echo "</td>";
	echo "</tr>";
	echo "</table>";
}
?>

      </td>
    </tr>
  </table>
  <input type="hidden" name="order" value="<?=$li->order?>" />
  <input type="hidden" name="pageNo" value="<?=$li->pageNo?>" />
  <input type="hidden" name="field" value="<?=$li->field?>" />
  <input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
  <input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>