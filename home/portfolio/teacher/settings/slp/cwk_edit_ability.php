<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cwk/lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";

//$luser = new libuser($UserID);
$lpf = new libportfolio2007();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

##################################################
############  Cotnent Here #######################
##################################################

$AbilityID = is_array($AbilityID) ? $AbilityID[0] : $AbilityID;
$sql = "SELECT AbilityCode, AbilityName FROM {$eclass_db}.CWK_COMMON_ABILITY ";
$sql .= "WHERE AbilityID = {$AbilityID}";
list($AbilityCode, $AbilityName) = current($lpf->returnArray($sql));

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_CWK);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<script language="JavaScript">
function checkform(formObj){
	if(formObj.AbilityCode.value=="")
	{
		alert("<?=$ec_warning['title']?>");
		formObj.AbilityCode.focus();
		return false;
	}
	if(formObj.AbilityName.value=="")
	{
		alert("<?=$ec_warning['title']?>");
		formObj.AbilityName.focus();
		return false;
	}
	
	formObj.submit();
}

</script>

<form name="form1" action="cwk_ability_update.php" method="POST">
  <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
  			<table width="95%" border="0" cellspacing="0" cellpadding="4" align="center">
  				<tr>
  					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="15%">
  						<?=$Lang["Cust_Cwk"]["CommonAbilityCode"]?>
  						<span class="tabletextrequire">*</span>
  					</td>
  					<td valign="top" class="tabletext"><input class="text" type="text" name="AbilityCode" size="40" maxlength="255" value="<?=$AbilityCode?>" /></td>
  				</tr>
  				<tr>
  					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="15%">
  						<?=$Lang["Cust_Cwk"]["CommonAbilityName"]?>
  						<span class="tabletextrequire">*</span>
  					</td>
  					<td valign="top" class="tabletext"><input class="text" type="text" name="AbilityName" size="40" maxlength="255" value="<?=$AbilityName?>" /></td>
  				</tr>
  			</table>
      </td>
    </tr>
  </table>
  
	<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
		<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td class="tabletextremark" colspan="6"><?=$ec_iPortfolio['mandatory_field_description']?></td>
		</tr>
		<tr>
			<td align="center" colspan="6">
				<div style="padding-top: 5px">
				<?= $linterface->GET_ACTION_BTN($button_submit, "button","javascript: checkform(document.form1);")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()")?>&nbsp;
				</div>
			</td>
		</tr>
	</table>
	
	<input type="hidden" name="AbilityID" value="<?=$AbilityID?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>