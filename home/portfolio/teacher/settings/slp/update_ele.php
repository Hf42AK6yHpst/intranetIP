<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lpf = new libportfolio();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

if($Action == "activate")
{
	$fields_values = "RecordStatus = '1', ModifiedDate = now()";
	$sql = "UPDATE {$eclass_db}.OLE_ELE SET $fields_values WHERE RecordID IN (".implode(",", $RecordID).") AND RecordStatus = '0'";
	$li->db_db_query($sql);
	$fields_values = "RecordStatus = '2', ModifiedDate = now()";
	$sql = "UPDATE {$eclass_db}.OLE_ELE SET $fields_values WHERE RecordID IN (".implode(",", $RecordID).") AND RecordStatus = '3'";
	$li->db_db_query($sql);
	$msg = 'activate';
}
else if($Action == "deactivate")
{
	$fields_values = "RecordStatus = '0', ModifiedDate = now()";
	$sql = "UPDATE {$eclass_db}.OLE_ELE SET $fields_values WHERE RecordID IN (".implode(",", $RecordID).") AND RecordStatus = '1'";
	$li->db_db_query($sql);
	$fields_values = "RecordStatus = '3', ModifiedDate = now()";
	$sql = "UPDATE {$eclass_db}.OLE_ELE SET $fields_values WHERE RecordID IN (".implode(",", $RecordID).") AND RecordStatus = '2'";
	$li->db_db_query($sql);
	$msg = 'suspend';
}
else if($Action == "remove")
{	# Default cannot be removed
	$sql = "DELETE FROM {$eclass_db}.OLE_ELE WHERE RecordID IN (".implode(",", $RecordID).") AND (RecordStatus = 0 OR RecordStatus = 1)";
	$li->db_db_query($sql);
	
	$sql = "DELETE FROM {$eclass_db}.OLE_ELE_MGMT_GROUP WHERE ComponentID IN (".implode(",", $RecordID).")";
	$li->db_db_query($sql);
	$msg = 'delete';
}
else
{
	$ChiTitle = HTMLtoDB($ChiTitle);
	$EngTitle = HTMLtoDB($EngTitle);
	
	if($RecordID!="")
	{
		$fields_values = "ChiTitle = '$ChiTitle', ";
		$fields_values .= "EngTitle = '$EngTitle', ";
		$fields_values .= "RecordStatus = '$status', ";
		$fields_values .= "ModifiedDate = now()";
		$sql = "UPDATE {$eclass_db}.OLE_ELE SET $fields_values WHERE RecordID = '$RecordID'";
		$li->db_db_query($sql);
		$msg = 'update';
	}
	else
	{
		$fields = "ChiTitle, EngTitle, RecordStatus, ModifiedDate, InputDate";
		$values = "'$ChiTitle', '$EngTitle', '$status', now(), now()";
		$sql = "INSERT INTO {$eclass_db}.OLE_ELE ($fields) VALUES ($values) ";
		$li->db_db_query($sql);
		$RecordID = $li->db_insert_id(); 
		$msg = 'add';
	}
	
/*	
	$sql = "DELETE FROM {$eclass_db}.OLE_ELE_MGMT_GROUP WHERE ComponentID = {$RecordID}";
	$li->db_db_query($sql);
	$field_values = array();
	for($i=0, $i_max=count($GroupID); $i<$i_max; $i++)
	{
		$field_values[] = "({$RecordID}, {$GroupID[$i]}, NOW(), {$UserID})";
	}
	if(!empty($field_values))
	{
		$sql = "INSERT IGNORE INTO {$eclass_db}.OLE_ELE_MGMT_GROUP ";
		$sql .= "(ComponentID, GroupID, DateInput, InputBy) VALUES ";
		$sql .= implode(", ", $field_values);
		$li->db_db_query($sql);
	}
*/
}

intranet_closedb();

header("Location: ele.php?msg=$msg");
?>
