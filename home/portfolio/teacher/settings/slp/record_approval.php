<?php
## Using By : 
##############################################
##	Modification Log:
## 2010-04-21: Max (201004211152)
## - Get variable IsEditable and pass to function for further operation

##	2010-02-18 Max (201002180941)
##	- Get the tag menu from includes/portfolio25/libpf-tabmenu.php instead of slpsettingcommonfunction.php

##	2010-01-20 Max(201001201615)
##	- Generate the Tag Menu By function call 
##############################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";

$lpf = new libpf_slp();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

##################################################
############  Cotnent Here #######################
##################################################
// Tab Menu Settings
//*** current page index*****
// 0 -   * Components of OLE
// 1 -   * Programme Category
// 2 -   * Management Group
// 3 -   * Preset Participation Role
// 4 -   * Submission Period
// 5 -   * Record Approval
$currentPageIndex = 5;
$TabMenuArr = libpf_tabmenu::getSlpSettingsTags(IPF_CFG_SLP_SETTING_OLE, $currentPageIndex);

// load approval settings
//$ApprovalSetting = $lpf->GET_OLR_APPROVAL_SETTING();
//$ParArr["ApprovalSetting"] = $ApprovalSetting;
//$IsEditable = $lpf->GET_OLR_EDITABLE_SETTING();

//$Content = "";
//$Content .= $lpf->GET_TAB_MENU($TabMenuArr);
//$Content .= $linterface->GET_SYS_MSG($msg);
//$Content .= $lpf->GET_RECORD_APPROVAL_FORM($ParArr, $IsEditable);



$approvalSettings = $lpf->GET_OLR_APPROVAL_SETTING_DATA2();
$CHECKED = " checked = 'checked' ";

if ($approvalSettings['INT']['IsApprovalNeed']) {
	$h_check_int_approvalNeed = $CHECKED;
	if ($approvalSettings['INT']['Elements']['ClassTeacher']) {
		$h_check_int_classTeacher = $CHECKED;
	}
	if ($approvalSettings['INT']['Elements']['Self']) {
		$h_check_int_self = $CHECKED;
	}
	$int_openID = "int_setting_approve";
	$int_closeID = "int_setting_noapprove";
} else {
	$h_check_int_noApprovalNeed = $CHECKED;
	if ($approvalSettings['INT']['Elements']['Editable']) {
		$h_check_int_editable = $CHECKED;
	}
	$int_openID = "int_setting_noapprove";
	$int_closeID = "int_setting_approve";
}
$js_openCloseINT = " showOptions('$int_openID','$int_closeID'); ";

if ($approvalSettings['EXT']['IsApprovalNeed']) {
	$h_check_ext_approvalNeed = $CHECKED;
	if ($approvalSettings['EXT']['Elements']['ClassTeacher']) {
		$h_check_ext_classTeacher = $CHECKED;
	}
	if ($approvalSettings['EXT']['Elements']['Self']) {
		$h_check_ext_self = $CHECKED;
	}
	$ext_openID = "ext_setting_approve";
	$ext_closeID = "ext_setting_noapprove";
} else {
	$h_check_ext_noApprovalNeed = $CHECKED;
	if ($approvalSettings['EXT']['Elements']['Editable']) {
		$h_check_ext_editable = $CHECKED;
	}
	$ext_openID = "ext_setting_noapprove";
	$ext_closeID = "ext_setting_approve";
}
$js_openCloseEXT = " showOptions('$ext_openID','$ext_closeID'); ";



##################################################
##################################################
##################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_OLE);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
function jReset()
{
	var obj = document.form1;
	obj.reset();
}

function jSubmit()
{
//var lang_checked = $("input[id^='ext_approve_']:checked").size();   
var errStr = "";
		var int_approve = $("#int_setting_approve:checked").val();
		var ext_approve = $("#ext_setting_approve:checked").val();
		if(int_approve == 1){
			var int_approve_classteacher = $("#int_approve_classteacher:checked").val();
			var int_approve_self = $("#int_approve_self:checked").val();

			//check whether set whom can approve
			if(int_approve_classteacher != 1 && int_approve_self != 1){
				errStr = '<?=$iPort["msg"]["selectOneItem"]?>\n';		
			}			
		}
		if(ext_approve == 1){
			var ext_approve_classteacher = $("#ext_approve_classteacher:checked").val();
			var ext_approve_self = $("#ext_approve_self:checked").val();

			//check whether set whom can approve
			if(ext_approve_classteacher != 1 && ext_approve_self != 1){
				errStr = '<?=$iPort["msg"]["selectOneItem"]?>\n';
			}
		}

		if(errStr == ""){
			var obj = document.form1;
			obj.submit();

		}else{
			alert(errStr);
			return false;
		}
}
function setAllowEditDisplay() {
	var approvalSetting = $("input[name=ApprovalSetting]:checked").val();
	
	if (approvalSetting == 2) {
		
		$("span[id=allowEditSpan]").show();
	} else {
		$("span[id=allowEditSpan]").hide();
	}
}
$(document).ready(function() {
	setAllowEditDisplay();
	<?=$js_openCloseINT?>
	<?=$js_openCloseEXT?>
});

function showOptions(ParOpenID,ParCloseID) {
	$('#'+ParCloseID).parent().children(".options").slideUp();
	$('#'+ParOpenID).parent().children(".options").slideDown();
}
</SCRIPT>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<?=$lpf->GET_TAB_MENU($TabMenuArr)?>
<?=$linterface->GET_SYS_MSG($msg)?>
<form name="form1" action="update_record_approval.php" method="post">
<table width="88%" border="0" cellpadding="5" cellspacing="0">
	<tbody>
		<tr>
			<td>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tbody>
					<tr>
						<td width="80%" valign="top">
						<table cellspacing="0" cellpadding="2" border="0" width="100%">
							<tbody>
								<!-- INT -->
								<tr><td colspan=2><?=$linterface->GET_NAVIGATION2($iPort["internal_record"])?></td></tr>
								<tr>
									<td class="formfieldtitle" rowspan=2 width="20%"><?=$ec_iPortfolio['record_approval_setting']?></td>
									<td nowrap=""><input id="int_setting_noapprove" type="radio"
										onclick="javascript: showOptions('int_setting_noapprove','int_setting_approve');" value="0"
										name="approvalSettings[INT][IsApprovalNeed]" <?=$h_check_int_noApprovalNeed?>><label
										for="int_setting_noapprove"><?=$iPort['NoApprovalNeeded']?></label><br>
										<span id="int_allowEditSpan" class="options" style="display:none">&nbsp;&nbsp;
											<!-- Editable -->
											<input
											type="checkbox" id="int_noapprove_allowEdit" name="approvalSettings[INT][Elements][Editable]" value="1" <?=$h_check_int_editable?>>
										<label for="int_noapprove_allowEdit"><?=$ec_iPortfolio['record_approval_setting_editable']?></label></span>
									</td>
								</tr>
								<tr>
									<td nowrap=""><input id="int_setting_approve" type="radio"
										onclick="javascript: showOptions('int_setting_approve','int_setting_noapprove');" value="1"
										name="approvalSettings[INT][IsApprovalNeed]" <?=$h_check_int_approvalNeed?>><label
										for="int_setting_approve"><?=$iPort['ApprovalNeeded']?></label><br>
										<span id="int_approveDetailSpan" class="options" style="display:none">&nbsp;&nbsp;
										
											<!-- Class Teacher -->
											<input id="int_approve_classteacher" type="checkbox"
											onclick="javascript: setAllowEditDisplay();" value="1"
											name="approvalSettings[INT][Elements][ClassTeacher]" <?=$h_check_int_classTeacher?>>
											<label for="int_approve_classteacher"><?=$iPort['RecordApproval_ClassTeacher']?></label>
											<br/>&nbsp;&nbsp;
											
											<!-- Self -->
											<input id="int_approve_self" type="checkbox"
											onclick="javascript: setAllowEditDisplay();" value="1"
											name="approvalSettings[INT][Elements][Self]" <?=$h_check_int_self?>>
											<label for="int_approve_self"><?=$ec_iPortfolio['record_approval_setting_selective']?></label></span>
										</td>
								</tr>
								<tr>
								<td colspan=2>&nbsp;</td>
								</tr>
								<!-- EXT -->
								<tr><td colspan=2><?=$linterface->GET_NAVIGATION2($iPort["external_record"])?></td></tr>
								<tr>
									<td class="formfieldtitle" rowspan=2><?=$ec_iPortfolio['record_approval_setting']?></td>
									<td nowrap=""><input id="ext_setting_noapprove" type="radio"
										onclick="javascript: showOptions('ext_setting_noapprove','ext_setting_approve');" value="0"
										name="approvalSettings[EXT][IsApprovalNeed]" <?=$h_check_ext_noApprovalNeed?>><label
										for="ext_setting_noapprove"><?=$iPort['NoApprovalNeeded']?></label><br>
										<span id="ext_allowEditSpan" class="options" style="display:none">&nbsp;&nbsp;
											<!-- Editable -->
											<input
											type="checkbox" id="ext_noapprove_allowEdit" name="approvalSettings[EXT][Elements][Editable]" value="1" <?=$h_check_ext_editable?>>
										<label for="ext_noapprove_allowEdit"><?=$ec_iPortfolio['record_approval_setting_editable']?></label></span>
									</td>
								</tr>
								<tr>
									<td nowrap=""><input id="ext_setting_approve" type="radio"
										onclick="javascript: showOptions('ext_setting_approve','ext_setting_noapprove');" value="1"
										name="approvalSettings[EXT][IsApprovalNeed]" <?=$h_check_ext_approvalNeed?>><label
										for="ext_setting_approve"><?=$iPort['ApprovalNeeded']?></label><br>
										<span id="ext_approveDetailSpan" class="options" style="display:none">&nbsp;&nbsp;
											
											<!-- Class Teacher -->
											<input id="ext_approve_classteacher" type="checkbox"
											onclick="javascript: setAllowEditDisplay();" value="1"
											name="approvalSettings[EXT][Elements][ClassTeacher]" <?=$h_check_ext_classTeacher?>>
											<label for="ext_approve_classteacher"><?=$iPort['RecordApproval_ClassTeacher']?></label>
											<br/>&nbsp;&nbsp;
											
											<!-- Self -->
											<input id="ext_approve_self" type="checkbox"
											onclick="javascript: setAllowEditDisplay();" value="1"
											name="approvalSettings[EXT][Elements][Self]" <?=$h_check_ext_self?>>
											<label for="ext_approve_self"><?=$ec_iPortfolio['record_approval_setting_selective']?></label></span>
									</td>
								</tr>
								<tr></tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr>
			<td height="1" class="dotline"><img src="//10x10.gif" width="10"
				height="1"></td>
		</tr>
		<tr>
			<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td align="center"><?=$linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "button", "jSubmit();", "btnSubmit")?>&nbsp;<?=$linterface->GET_ACTION_BTN($iPort["btn"]["reset"], "button", "jReset();", "btnReset")?></td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
</form>

</td></tr></table>
</td></tr></table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
