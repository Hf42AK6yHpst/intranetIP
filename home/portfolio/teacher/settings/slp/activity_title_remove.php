<?php
# Under Development : 20091120
// Modifing by 
/**
 * Type			: Bug Fixing, Enhancement
 * Date 		: 200911201842
 * Description	: 1C) The delete function cannot perform deleting all the selected records but the 1st one
 * By			: Max Wong
 * Case Number	: 200911201842MaxWong
 * C=CurrentIssue 
 */


$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");

intranet_auth();
intranet_opendb();


$luser = new libuser($UserID);
$lpf = new libpf_slp();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

if (isset($temp_type))
{
	$lf = new libwordtemplates_ipf(3);
	for ($i=0; $i<sizeof($RecordID); $i++)
	{
		if ($i==0)
		{
			$TitleID = $RecordID[$i];
		}
		else
		{
			$TitleID = $RecordID[$i] - $i; //shifting of index aftering deletion of previous record
		}
		
		
		
		$data = $lf->getTemplatesContent($temp_type);
		
//		$NewData = "";
//		$DataArray = explode("\n", $data);
		$NewData = "";
		$DataArray = array();
		$DataArray = explode("\n", $data);
		if(!empty($DataArray))
		{
			/* 200911201842MaxWong */
//			for($k=0; $k<sizeof($DataArray); $k++)
//			{
//				if(!($TitleID!="" && $k==$TitleID)) {
//					$t_title = intranet_undo_htmlspecialchars(addslashes(trim($DataArray[$k])));
//					$NewData .= $t_title."\n";
//				}
//			}
			$firstFlag = true;
			for($k=0; $k<sizeof($DataArray); $k++)
			{
				if(!(isset($TitleID) && $k==$TitleID)) {
					$t_title = intranet_undo_htmlspecialchars(addslashes(trim($DataArray[$k])));
					if ($firstFlag) {
						$NewData .= $t_title;
						$firstFlag = false;
					} else {
						$NewData .= "\n".$t_title;
					}
				}
			}
			/* 200911201842MaxWong */
			 
		}
	
		$lf->updateTemplatesContent($temp_type, stripslashes(intranet_htmlspecialchars($NewData)));
	}
	$msg = "delete";
}

intranet_closedb();

header("Location: activity_title_template.php?msg=$msg&temp_type=$temp_type");
?>