<?php

$lpf_subcat = new subCategory();
$lpf_subcat->SET_CLASS_VARIABLE('chi_title', $chi_title);
$lpf_subcat->SET_CLASS_VARIABLE('eng_title', $eng_title);
$lpf_subcat->SET_CLASS_VARIABLE('record_status', $record_status);
$lpf_subcat->SET_CLASS_VARIABLE('category_id', $category_id);

if(!empty($subcategory_id))
{
  $lpf_subcat->SET_CLASS_VARIABLE('subcategory_id', $subcategory_id);
  $lpf_subcat->UPDATE_SUBCATEGORY();
}
else
{
  $lpf_subcat->ADD_SUBCATEGORY();
}

?>

<script>
  parent.index_reload_page('msg'); 
</script>
