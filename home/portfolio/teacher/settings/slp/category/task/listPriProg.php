<?php
#Using By : 

include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libpf-ole.php");
$lpf_ole = new libpf_ole();

$lpf_cat = new Category();
$lpf_cat->SET_CLASS_VARIABLE("category_id", $CatID);
$lpf_cat->SET_CATEGORY_PROPERTY();
$CatChiName = $lpf_cat->GET_CLASS_VARIABLE("chi_title");
$CatEngName = $lpf_cat->GET_CLASS_VARIABLE("eng_title");
$t_CatRecStatus = $lpf_cat->GET_CLASS_VARIABLE("record_status");
switch($t_CatRecStatus)
{
  case 0:
    $CatRecStatus = $button_private;
    break;
  case 1:
    $CatRecStatus = $button_public;
    break;
  case 2:
    $CatRecStatus = $ec_iPortfolio['default']." - ".$button_public;
    break;
  case 3:
    $CatRecStatus = $ec_iPortfolio['default']." - ".$button_private;
    break;
}

# Display type selection
$disp_type_select_html = "<span>".$ec_iPortfolio['AllPresetProgrammeName']."</span> | <a href=\"index.php?task=listSubCat&CatID=".$CatID."\">".$ec_iPortfolio['NoOfSubcategory']."</a>";


$eleAry = $lpf_ole->Get_ELE_Info();
$numOfEle = count($eleAry);
$eleTitleField = "REPLACE(opt.ELE, ',', ', ')";
for ($i=0; $i<$numOfEle; $i++) {
	$_title = Get_Lang_Selection($eleAry[$i]['ChiTitle'], $eleAry[$i]['EngTitle']);
	$_eleCode = $eleAry[$i]['ELECode'];
	
	$eleTitleField = "REPLACE(".$eleTitleField.", '".$lpf_ole->Get_Safe_Sql_Query($_eleCode)."', '".$lpf_ole->Get_Safe_Sql_Query($_title)."')"; 
}

//get the list

if ($order=="") $order=1;
if ($field=="") $field=0;
$pageSizeChangeEnabled = true;
$LibTable = new libdbtable2007($field, $order, $pageNo);

$keywords = null;
if($prog_searching_keywords != NULL){
	$keywords = "AND opt.EngTitle LIKE '%".$prog_searching_keywords."%'";
}

$sql =  "
          SELECT
            opt.EngTitle,
            IF(opt.SubCategoryID IS NULL, '--', ".Get_Lang_Selection("osc.ChiTitle", "osc.EngTitle").") AS SubCatName,
            IF(opt.ELE IS NULL OR opt.ELE = '', '--', $eleTitleField) AS ELE,
            CASE opt.RecordStatus
              WHEN 0 THEN '{$button_private}'
              WHEN 1 THEN '{$button_public}'
              WHEN 2 THEN '{$ec_iPortfolio['default']} - {$button_public}'
              WHEN 3 THEN '{$ec_iPortfolio['default']} - {$button_private}'
            END,
            DATE_FORMAT(opt.ModifiedDate, '%Y-%m-%d') AS ModifiedDate,
            CONCAT('<input type=\"checkbox\" name=\"record_id[]\" value=\"', opt.ProgramTitleID, '\">')
          FROM
            {$eclass_db}.OLE_PROGRAM_TITLE AS opt
          LEFT JOIN
            {$eclass_db}.OLE_SUBCATEGORY AS osc
          ON
            osc.SubCatID = opt.SubCategoryID
          WHERE
            opt.CategoryID = '".$CatID."' $keywords
        ";

//echo htmlspecialchars($sql);
$LibTable->sql = $sql;
$LibTable->field_array = array("EngTitle", "RecordStatus", "ModifiedDate");
$LibTable->db = $intranet_db;
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($num_per_page=="") ? $page_size : $num_per_page;
if ($page_size_change!="") $li->page_size = $numPerPage;
$LibTable->no_col = 7;
$LibTable->noNumber = false;
	
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
//$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";
$LibTable->column_array = array(0, 0, 0, 0, 0);

// TABLE COLUMN
$LibTable->column_list = "<tr class='tabletop'>\n";
$LibTable->column_list .= "<th class='num_check'>#</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(0,$ec_iPortfolio['title'])."</th>\n";
$LibTable->column_list .= "<th>" . $ec_iPortfolio['sub_category'] . "</th>\n";
$LibTable->column_list .= "<th>" . $iPort["ele"] . "</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(1,$ec_iPortfolio['status'])."</th>\n";
$LibTable->column_list .= "<th nowrap>".$LibTable->column(2,$ec_iPortfolio['SAMS_last_update'])."</th>\n";
$LibTable->column_list .= "<th>".$LibTable->check("record_id[]")."</th>\n";
$LibTable->column_list .= "</tr>\n";		

$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['Category'], "index.php?task=listCat");
$MenuArr[] = array($CatChiName." (".$CatEngName.")", "");
$PageNavigationDisplay = $linterface->GET_NAVIGATION($MenuArr);

ob_start();
$LibTable->displayPlain();
$ProgTitleListTable = ob_get_contents();
ob_end_clean();
$ProgTitleListTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
if ($LibTable->navigationHTML!="") {
  $ProgTitleListTable .= "<tr class='tablebottom'>";
  $ProgTitleListTable .= "<td  class=\"tabletext\" align=\"right\">";
  $ProgTitleListTable .= $LibTable->navigation(1);
  $ProgTitleListTable .= "</td>";
  $ProgTitleListTable .= "</tr>";
}
$ProgTitleListTable .= "</table>";


$linterface->LAYOUT_START();
include_once("template/listPriProg.tmpl.php");
$linterface->LAYOUT_STOP();
?>