<?php

$lpf_sub_cat = new subCategory();
$lpf_sub_cat->SET_CLASS_VARIABLE("subcategory_id", $SubCatID);
$lpf_sub_cat->SET_SUBCATEGORY_PROPERTY();
$CatID = $lpf_sub_cat->GET_CLASS_VARIABLE("category_id");
$SubCatChiName = $lpf_sub_cat->GET_CLASS_VARIABLE("chi_title");
$SubCatEngName = $lpf_sub_cat->GET_CLASS_VARIABLE("eng_title");

$lpf_cat = new Category();
$lpf_cat->SET_CLASS_VARIABLE("category_id", $CatID);
$lpf_cat->SET_CATEGORY_PROPERTY();
$CatChiName = $lpf_cat->GET_CLASS_VARIABLE("chi_title");
$CatEngName = $lpf_cat->GET_CLASS_VARIABLE("eng_title");
$CatArr = $lpf_cat->GET_CATEGORY_LIST();

$ProgTitleCatInput = "<select name=\"category_id\" onChange=\"getSubCat()\">";
$ProgTitleCatInput .= "<option value=\"\">--</option>";
for($i=0; $i<count($CatArr); $i++)
{
  $t_cat_id = $CatArr[$i]["RecordID"];
  $t_cat_title = ($intranet_session_language == "b5") ? $CatArr[$i]["ChiTitle"] : $CatArr[$i]["EngTitle"];
  
  $ProgTitleCatInput .= "<option value=\"".$t_cat_id."\" ".$t_check.">".$t_cat_title."</option>";
}
$ProgTitleCatInput .= "</select>";

$ProgTitleSubCatInput = "<select name=\"subcategory_id\">";
$ProgTitleSubCatInput .= "</select>";

include_once("template/moveProgTitle.tmpl.php");
?>