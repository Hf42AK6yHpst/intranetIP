<?php
#Using By : 
$lpf_cat = new Category();
$lpf_cat->SET_CLASS_VARIABLE("category_id", $CatID);
$lpf_cat->SET_CATEGORY_PROPERTY();
$CatChiName = $lpf_cat->GET_CLASS_VARIABLE("chi_title");
$CatEngName = $lpf_cat->GET_CLASS_VARIABLE("eng_title");
$t_CatRecStatus = $lpf_cat->GET_CLASS_VARIABLE("record_status");
switch($t_CatRecStatus)
{
  case 0:
    $CatRecStatus = $button_private;
    break;
  case 1:
    $CatRecStatus = $button_public;
    break;
  case 2:
    $CatRecStatus = $ec_iPortfolio['default']." - ".$button_public;
    break;
  case 3:
    $CatRecStatus = $ec_iPortfolio['default']." - ".$button_private;
    break;
}

# Display type selection
$disp_type_select_html = "<a href=\"index.php?task=listPriProg&CatID=".$CatID."\">" . $ec_iPortfolio['AllPresetProgrammeName'] . "</a> | <span>" . $ec_iPortfolio['sub_category'] . "</span>";

//get the list

if ($order=="") $order=1;
if ($field=="") $field=0;
$pageSizeChangeEnabled = true;
$LibTable = new libdbtable2007($field, $order, $pageNo);

$keywords = null;
if($subcat_searching_keywords != NULL){
	$keywords = "AND osc.EngTitle LIKE '%".$subcat_searching_keywords."%'";
}
$sql =  "
          SELECT
            CONCAT('<a href=\"index.php?task=listProg&SubCatID=', osc.SubCatID, '\" class=\"tablelink\">', osc.EngTitle, '</a>'),
            CONCAT('<a href=\"index.php?task=listProg&SubCatID=', osc.SubCatID, '\" class=\"tablelink\">', osc.ChiTitle, '</a>'),
            IF(opt.progTitleCount IS NULL, '0', opt.progTitleCount) AS progTitleCount,
            CASE osc.RecordStatus
              WHEN 0 THEN '{$button_private}'
              WHEN 1 THEN '{$button_public}'
              WHEN 2 THEN '{$ec_iPortfolio['default']} - {$button_public}'
              WHEN 3 THEN '{$ec_iPortfolio['default']} - {$button_private}'
            END,
            DATE_FORMAT(osc.ModifiedDate, '%Y-%m-%d') AS ModifiedDate,
            CONCAT('<input type=\"checkbox\" name=\"record_id[]\" value=\"', osc.SubCatID, '\">')
          FROM
            {$eclass_db}.OLE_SUBCATEGORY AS osc
          LEFT JOIN
            (
              SELECT
                SubCategoryID,
                count(*) AS progTitleCount
              FROM
                {$eclass_db}.OLE_PROGRAM_TITLE
              GROUP BY
                SubCategoryID
            ) AS opt
          ON
            osc.SubCatID = opt.SubCategoryID
          WHERE
            osc.CatID = '".$CatID."' $keywords
        ";
//echo htmlspecialchars($sql);
$LibTable->sql = $sql;
$LibTable->field_array = array("EngTitle", "ChiTitle", "progTitleCount", "RecordStatus", "ModifiedDate");
$LibTable->db = $intranet_db;
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($num_per_page=="") ? $page_size : $num_per_page;
if ($page_size_change!="") $li->page_size = $numPerPage;
$LibTable->no_col = 7;
$LibTable->noNumber = false;
	
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
//$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";
$LibTable->column_array = array(0, 0, 3, 0, 0, 0);

// TABLE COLUMN
$LibTable->column_list = "<tr class='tabletop'>\n";
$LibTable->column_list .= "<th class='num_check'>#</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(0,$ec_iPortfolio['sub_category'].$ec_iPortfolio['title']." (".$ec_iPortfolio['lang_eng'].")")."</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(1,$ec_iPortfolio['sub_category'].$ec_iPortfolio['title']." (".$ec_iPortfolio['lang_chi'].")")."</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(2,$ec_iPortfolio['NoOfPresetProgrammeName'])."</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(3,$ec_iPortfolio['status'])."</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(4,$ec_iPortfolio['SAMS_last_update'])."</th>\n";
$LibTable->column_list .= "<th>".$LibTable->check("record_id[]")."</th>\n";
$LibTable->column_list .= "</tr>\n";	

$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['Category'], "index.php?task=listCat");
$MenuArr[] = array($CatChiName." (".$CatEngName.")", "");
$PageNavigationDisplay = $linterface->GET_NAVIGATION($MenuArr);

ob_start();
$LibTable->displayPlain();
$SubCategoryListTable = ob_get_contents();
ob_end_clean();
$SubCategoryListTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
if ($LibTable->navigationHTML!="") {
  $SubCategoryListTable .= "<tr class='tablebottom'>";
  $SubCategoryListTable .= "<td  class=\"tabletext\" align=\"right\">";
  $SubCategoryListTable .= $LibTable->navigation(1);
  $SubCategoryListTable .= "</td>";
  $SubCategoryListTable .= "</tr>";
}
$SubCategoryListTable .= "</table>";


$linterface->LAYOUT_START();
include_once("template/listSubCat.tmpl.php");
$linterface->LAYOUT_STOP();
?>