<?php
########Change Log#########
#
#	Date:	2016-07-06	Kenneth
#	- lang fix of radio buttons
#
###########################
$lpf_sub_cat = new subCategory();
$lpf_sub_cat->SET_CLASS_VARIABLE('subcategory_id', $subcategory_id);
$lpf_sub_cat->SET_SUBCATEGORY_PROPERTY();

$SubCatID = $subcategory_id;
$CatID = $lpf_sub_cat->GET_CLASS_VARIABLE("category_id");
$SubCatChiName = $lpf_sub_cat->GET_CLASS_VARIABLE("chi_title");
$SubCatEngName = $lpf_sub_cat->GET_CLASS_VARIABLE("eng_title");
$SubCatRecStatus = $lpf_sub_cat->GET_CLASS_VARIABLE("record_status");

if($SubCatRecStatus == 2 || $SubCatRecStatus == 3)
{
  $SubCatEngNameInput = "<input name=\"eng_title\" type=\"text\" value=\"".$SubCatEngName."\" id=\"eng_title\" class=\"textbox\" READONLY />";
  $SubCatChiNameInput = "<input name=\"chi_title\" type=\"text\" value=\"".$SubCatChiName."\" id=\"chi_title\" class=\"textbox\" READONLY />";
  
  $SubCatStatusInput = "<input type=\"radio\" name=\"record_status\" id=\"status_active\" value=\"2\" ".($SubCatRecStatus==2?"checked=\"checked\"":"")." /> <label for=\"status_active\">".$ec_iPortfolio['Active']."</label>";
  $SubCatStatusInput .= "<input type=\"radio\" name=\"record_status\" id=\"status_suspend\" value=\"3\" ".($SubCatRecStatus==3?"checked=\"checked\"":"")." /> <label for=\"status_suspend\">".$ec_iPortfolio['Suspend']."</label>";
}
else
{
  $SubCatEngNameInput = "<input name=\"eng_title\" type=\"text\" value=\"".$SubCatEngName."\" id=\"eng_title\" class=\"textbox\"/>";
  $SubCatChiNameInput = "<input name=\"chi_title\" type=\"text\" value=\"".$SubCatChiName."\" id=\"chi_title\" class=\"textbox\"/>";
  
  $SubCatStatusInput = "<input type=\"radio\" name=\"record_status\" id=\"status_active\" value=\"1\" ".($SubCatRecStatus==1?"checked=\"checked\"":"")." /> <label for=\"status_active\">".$ec_iPortfolio['Active']."</label>";
  $SubCatStatusInput .= "<input type=\"radio\" name=\"record_status\" id=\"status_suspend\" value=\"0\" ".($SubCatRecStatus==0?"checked=\"checked\"":"")." /> <label for=\"status_suspend\">".$ec_iPortfolio['Suspend']."</label>";
}


include_once("template/updateSubCat.tmpl.php");
?>