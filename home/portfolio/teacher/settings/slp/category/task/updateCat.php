<?php

$lpf_cat = new Category();
$lpf_cat->SET_CLASS_VARIABLE('chi_title', $chi_title);
$lpf_cat->SET_CLASS_VARIABLE('eng_title', $eng_title);
$lpf_cat->SET_CLASS_VARIABLE('record_status', $record_status);

if(!empty($category_id))
{
  $lpf_cat->SET_CLASS_VARIABLE('category_id', $category_id);
  $lpf_cat->UPDATE_CATEGORY();
}
else
{
  $lpf_cat->ADD_CATEGORY();
}

?>

<script>
  parent.index_reload_page('msg'); 
</script>
