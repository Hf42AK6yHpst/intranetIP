<?php

# Prepare data
$lpf_slp = new libpf_slp();
$component_arr = $lpf_slp->GET_ELE();

$lpf_cat = new Category();
$CatArr = $lpf_cat->GET_CATEGORY_LIST();

$lpf_sub_cat = new subCategory();
if($SubCatID == "")
{
  $lpf_sub_cat->SET_CLASS_VARIABLE("category_id", $CatID);
}
else
{
  $lpf_sub_cat->SET_CLASS_VARIABLE("subcategory_id", $SubCatID);
  $lpf_sub_cat->SET_SUBCATEGORY_PROPERTY();
  $CatID = $lpf_sub_cat->GET_CLASS_VARIABLE("category_id");
}
$SubCatArr = $lpf_sub_cat->GET_SUBCATEGORY_LIST();

# Title
$ProgTitleEngNameInput = "<input name=\"eng_title\" type=\"text\" id=\"eng_title\" class=\"textbox\"/>";
$ProgTitleChiNameInput = "<input name=\"chi_title\" type=\"hidden\" id=\"chi_title\" value=\"\" />";

# Status
$ProgTitleStatusInput = "<input type=\"radio\" name=\"record_status\" id=\"status_active\" value=\"1\" checked=\"checked\" /> <label for=\"status_active\">" . $ec_iPortfolio['Active'] . "</label>";
$ProgTitleStatusInput .= "<input type=\"radio\" name=\"record_status\" id=\"status_suspend\" value=\"0\" /> <label for=\"status_suspend\">" . $ec_iPortfolio['Suspend'] . "</label>";

# Component
$ProgTitleComponentInput = "";
foreach($component_arr AS $component_code => $component_title)
{
  $ProgTitleComponentInput .= "<input type=\"checkbox\" name=\"component[]\" id=\"".$component_code."\" value=\"".$component_code."\" /> <label for=\"".$component_code."\">".$component_title."</label><br />";
}

# Category
$ProgTitleCatInput = "<select name=\"category_id\" onChange=\"getSubCat()\">";
$ProgTitleCatInput .= "<option value=\"\">--</option>";
for($i=0; $i<count($CatArr); $i++)
{
  $t_cat_id = $CatArr[$i]["RecordID"];
  $t_cat_title = ($intranet_session_language == "b5") ? $CatArr[$i]["ChiTitle"] : $CatArr[$i]["EngTitle"];
  
  $t_check = ($t_cat_id == $CatID) ? "selected=\"selected\"" : "";
  
  $ProgTitleCatInput .= "<option value=\"".$t_cat_id."\" ".$t_check.">".$t_cat_title."</option>";
}
$ProgTitleCatInput .= "</select>";

# Sub-category
$ProgTitleSubCatInput = "<select name=\"subcategory_id\">";
$ProgTitleSubCatInput .= "<option value=\"\">--</option>";
for($j=0; $j<count($SubCatArr); $j++)
{
  $t_subcat_id = $SubCatArr[$j]["SubCatID"];
  $t_subcat_title = ($intranet_session_language == "b5") ? $SubCatArr[$j]["ChiTitle"] : $SubCatArr[$j]["EngTitle"];
  $t_check = ($SubCatID == $t_subcat_id) ? "selected=\"selected\"" : "";

  $ProgTitleSubCatInput .= "<option value=\"".$t_subcat_id."\" ".$t_check.">".$t_subcat_title."</option>";
}
$ProgTitleSubCatInput .= "</select>";

include_once("template/updateProgTitle.tmpl.php");
?>