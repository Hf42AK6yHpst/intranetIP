<?php
########Change Log#########
#
#	Date:	2016-07-06	Kenneth 
#	- lang fix of radio buttons 
#
###########################
# Prepare data
$lpf_slp = new libpf_slp();
$component_arr = $lpf_slp->GET_ELE();

$lpf_cat = new Category();
$CatArr = $lpf_cat->GET_CATEGORY_LIST();

$lpf_prog_title = new libpf_program_title();
$lpf_prog_title->SET_CLASS_VARIABLE('program_title_id', $ProgTitleID);
$lpf_prog_title->SET_PROGRAM_TITLE_PROPERTY();

$CatID = $lpf_prog_title->GET_CLASS_VARIABLE("category_id");
$SubCatID = $lpf_prog_title->GET_CLASS_VARIABLE("sub_category_id");
$ProgTitleChiName = $lpf_prog_title->GET_CLASS_VARIABLE("chi_title");
$ProgTitleEngName = $lpf_prog_title->GET_CLASS_VARIABLE("eng_title");
$ProgTitleRecStatus = $lpf_prog_title->GET_CLASS_VARIABLE("record_status");
$ProgTitleComponent = $lpf_prog_title->GET_CLASS_VARIABLE("component");

$lpf_sub_cat = new subCategory();
$lpf_sub_cat->SET_CLASS_VARIABLE("category_id", $CatID);
$SubCatArr = $lpf_sub_cat->GET_SUBCATEGORY_LIST();

# Title, Status, Component
if($ProgTitleRecStatus == 2 || $ProgTitleRecStatus == 3)
{
  $ProgTitleEngNameInput = "<input name=\"eng_title\" type=\"text\" value=\"".$ProgTitleEngName."\" id=\"eng_title\" class=\"textbox\" READONLY />";
  $ProgTitleChiNameInput = "<input name=\"chi_title\" type=\"text\" value=\"".$ProgTitleChiName."\" id=\"chi_title\" class=\"textbox\" READONLY />";
  
  $ProgTitleStatusInput = "<input type=\"radio\" name=\"record_status\" id=\"status_active\" value=\"2\" ".($ProgTitleRecStatus==2?"checked=\"checked\"":"")." /> <label for=\"status_active\">".$ec_iPortfolio['Active']."</label>";
  $ProgTitleStatusInput .= "<input type=\"radio\" name=\"record_status\" id=\"status_suspend\" value=\"3\" ".($ProgTitleRecStatus==3?"checked=\"checked\"":"")." /> <label for=\"status_suspend\">".$ec_iPortfolio['Suspend']."</label>";
}
else
{
  $ProgTitleEngNameInput = "<input name=\"eng_title\" type=\"text\" value=\"".$ProgTitleEngName."\" id=\"eng_title\" class=\"textbox\"/>";
  $ProgTitleChiNameInput = "<input name=\"chi_title\" type=\"hidden\" value=\"".$ProgTitleChiName."\" id=\"chi_title\"/>";
  
  $ProgTitleStatusInput = "<input type=\"radio\" name=\"record_status\" id=\"status_active\" value=\"1\" ".($ProgTitleRecStatus==1?"checked=\"checked\"":"")." /> <label for=\"status_active\">".$ec_iPortfolio['Active']."</label>";
  $ProgTitleStatusInput .= "<input type=\"radio\" name=\"record_status\" id=\"status_suspend\" value=\"0\" ".($ProgTitleRecStatus==0?"checked=\"checked\"":"")." /> <label for=\"status_suspend\">".$ec_iPortfolio['Suspend']."</label>";
  
  $ProgTitleComponentInput = "";
  foreach($component_arr AS $component_code => $component_title)
  {
    $t_check = in_array($component_code, $ProgTitleComponent) ? "checked=\"checked\"" : "";
  
    $ProgTitleComponentInput .= "<input type=\"checkbox\" name=\"component[]\" id=\"".$component_code."\" value=\"".$component_code."\" ".$t_check." /> <label for=\"".$component_code."\">".$component_title."</label><br />";
  }
}

# Category
$ProgTitleCatInput = "<select name=\"category_id\" onChange=\"getSubCat()\">";
$ProgTitleCatInput .= "<option value=\"\">--</option>";
for($i=0; $i<count($CatArr); $i++)
{
  $t_cat_id = $CatArr[$i]["RecordID"];
  $t_cat_title = ($intranet_session_language == "b5") ? $CatArr[$i]["ChiTitle"] : $CatArr[$i]["EngTitle"];
  
  $t_check = ($CatID == $t_cat_id) ? "selected=\"selected\"" : "";
  
  $ProgTitleCatInput .= "<option value=\"".$t_cat_id."\" ".$t_check.">".$t_cat_title."</option>";
}
$ProgTitleCatInput .= "</select>";

# Sub-category
$ProgTitleSubCatInput = "<select name=\"subcategory_id\">";
$ProgTitleSubCatInput .= "<option value=\"\">--</option>";
for($j=0; $j<count($SubCatArr); $j++)
{
  $t_subcat_id = $SubCatArr[$j]["SubCatID"];
  $t_subcat_title = ($intranet_session_language == "b5") ? $SubCatArr[$j]["ChiTitle"] : $SubCatArr[$j]["EngTitle"];
  $t_check = ($SubCatID == $t_subcat_id) ? "selected=\"selected\"" : "";

  $ProgTitleSubCatInput .= "<option value=\"".$t_subcat_id."\" ".$t_check.">".$t_subcat_title."</option>";
}
$ProgTitleSubCatInput .= "</select>";

include_once("template/updateProgTitle.tmpl.php");
?>