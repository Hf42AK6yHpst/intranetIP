<?php

$lpf_progtitle = new libpf_program_title();
$lpf_progtitle->SET_CLASS_VARIABLE('chi_title', $chi_title);
$lpf_progtitle->SET_CLASS_VARIABLE('eng_title', $eng_title);
$lpf_progtitle->SET_CLASS_VARIABLE('record_status', $record_status);
$lpf_progtitle->SET_CLASS_VARIABLE('category_id', $category_id);
$lpf_progtitle->SET_CLASS_VARIABLE('sub_category_id', $subcategory_id);
$lpf_progtitle->SET_CLASS_VARIABLE('component', $component);

if(!empty($prog_title_id))
{
  $lpf_progtitle->SET_CLASS_VARIABLE('program_title_id', $prog_title_id);
  $lpf_progtitle->EDIT_PROGRAM_TITLE();
}
else
{
  $lpf_progtitle->ADD_PROGRAM_TITLE();
}

?>

<script>
  parent.index_reload_page('msg'); 
</script>
