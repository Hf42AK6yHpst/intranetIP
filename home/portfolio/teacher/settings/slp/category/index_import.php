<?php
// Modifing by 

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/oleCategory/libpf-category.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oleCategory/libpf-subCategory.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oleCategory/libpf-program-title.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dynReport/JSON.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libpf-ole.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");

intranet_auth();
intranet_opendb();

// Initializing classes
$LibUser = new libuser($UserID);
$lpf = new libportfolio2007();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$libpf_ole = new libpf_ole();
$LibPortfolio = new libpf_slp();
$linterface = new interface_html();

$libCategory = new Category();

### Title ###
$CurrentPage = "Settings_OLE";
$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_OLE);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

### Tab Menu Settings ###
$TabMenuArr  = getTabMenuArr();

//*** current page index*****
// 0 -   * Components of OLE
// 1 -   * Programme Category
// 2 -   * Management Group
// 3 -   * Preset Participation Role
// 4 -   * Submission Period
// 5 -   * Record Approval

$currentPageIndex = 1;
$TabMenuArr = libpf_tabmenu::getSlpSettingsTags(IPF_CFG_SLP_SETTING_OLE, $currentPageIndex);
$TabMenuDisplay = $lpf->GET_TAB_MENU($TabMenuArr);

// Step display
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$linterface->LAYOUT_START();

// All ELE components
$ELEArray = array_values($LibPortfolio->GET_ELE());
$ELEArrayComp = array_keys($LibPortfolio->GET_ELE());

$ELEArray = $libpf_ole->Get_ELE_Info(array($ipf_cfg["OLE_ELE_RecordStatus"]["Public"], $ipf_cfg["OLE_ELE_RecordStatus"]["DefaultAndPublic"]), 'RecordID');

$ELE_HTML = "<table width='90%' border='0' cellspacing='0' cellpadding='3'>";
$ELE_HTML .= "<tr><td nowrap='nowrap'><u>".$ec_iPortfolio['ele_code']."</u></td></tr>";
$numOfELE = count($ELEArray);

// Build ELE components code table
for($i=0; $i<$numOfELE; $i++) {

	$_ELE_Name = Get_Lang_Selection($ELEArray[$i]['ChiTitle'], $ELEArray[$i]['EngTitle']);
	$_ELE_short = $ELEArray[$i]['component'];
	
	$ELE_HTML .= "<tr><td>".$ELEArrayComp[$i]." - ".$_ELE_Name."</td></tr>";
}
$ELE_HTML .= "</table>";

/*
	//$RemarksTable_HTML = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['iPortfolio']['SLP']['ImportRemarks'], $others="");
//}

if($IntExt == 1)
{
	$Import_Guide = $iPort["ole_import_guide_ext"];
	$SampleFile = "student_olr_sample2.csv";
}
else
{
	$Import_Guide = $iPort["ole_import_guide_int"];
	$SampleFile = "student_olr_sample.csv";
}
*/
$sampleFileHref = GET_CSV('import_preset_programme.csv');
$sample_file .= "<a class=\"tablelink\" href=\"". $href ."\" target=\"_blank\">[". $i_general_clickheredownloadsample ."]</a><br>";

$ReferenceHTML = "<table width='80%' border='0' cellspacing='0' cellpadding='3'>";
$ReferenceHTML .= "<tr><td colspan='3'>".$Import_Guide."</td></tr>";
$ReferenceHTML .= "<tr><td valign='top'>".$ELE_HTML."</td></tr>";
$ReferenceHTML .= "</table>";

function getTabMenuArr()
{
	global $iPort;
	$TabMenuArr = array();
	$TabMenuArr[] = array("ele.php", $iPort["ele"], 0);
	$TabMenuArr[] = array("category_index.php", $iPort["category"], 1);
	$TabMenuArr[] = array("tag.php", $iPort["tag"], 0);
	$TabMenuArr[] = array("activity_title_template.php", $iPort["activity_title_template"], 0);
	$TabMenuArr[] = array("activity_role_template.php", $iPort["activity_role_template"], 0);
	$TabMenuArr[] = array("student_submission_period.php", $iPort["student_submission_period"], 0);
	$TabMenuArr[] = array("record_approval.php", $iPort["record_approval"], 0);

	return $TabMenuArr;
}
?>

<script language="JavaScript">
</script>

<?=$TabMenuDisplay?>
<table align="left" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" border="0" align="absmiddle"> <?=$button_import?> </a>
	</td>
	<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
</tr>
</table>

<FORM name="form1" method="post" action="index_import_confirm.php" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr><td>
	
	<?= $linterface->GET_STEPS($STEPS_OBJ) ?>
	
	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center" >
	<!-- <tr><td colspan="2" align="center"><?=$RemarksTable_HTML?></td></tr> -->
	<br/>
	<tr><td valign="top" >
	
		<!-- CONTENT HERE -->
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
		
			<!-- input file -->
			<tr>
				<td><?=$ec_iPortfolio['SAMS_CSV_file']?>:</td>
				<td><input class=file type=file name=userfile size=25></td>
			</tr>
		
			<!-- Descption -->
			<tr>
			<td colspan="2">
				<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td>
					<?=$ReferenceHTML?>
				</td></tr></table>
			</td>
			</tr>
		
			<!-- dot line -->
			<tr>
				<td height="1" class="dotline" colspan="2"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
			</tr>	
	
			<!-- button -->
			<tr>
				<td  colspan="2" align="right">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="45%"><a class="contenttool" href="<?=$sampleFileHref?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_files/xls.gif" border="0" align="absmiddle" hspace="3"><?=$import_csv['download']?></a></td>
					<td width="55%" align="left">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()")?>
					</td>
				</tr>
				</table></td>
			</tr>
		
		</table>
		<!-- End of CONTENT -->
	
	</td></tr>
	</table>
	
</td></tr>
</table>

<input type="hidden" name="ClassName" value="<?=$ClassName?>" >
<input type="hidden" name="StudentID" value="<?=$StudentID?>" >
<input type="hidden" name="attachment_size" value="0" >
<input type="hidden" name="RecordID" value="<?=$record_id?>" >
<input type="hidden" name="attachment_size_current" value="<?=$attach_count?>" >
<input type="hidden" name="ApprovalSetting" value="<?=$ApprovalSetting?>" >
<input type="hidden" name="SelectedStatus" value="<?=$SelectedStatus?>" >
<input type="hidden" name="SelectedELE" value="<?=$SelectedELE?>" >
<input type="hidden" name="SelectedYear" value="<?=$SelectedYear?>" >
<input type="hidden" name="FromPage" value="<?=$FromPage?>" >
<input type="hidden" name="IntExt" value="<?=$IntExt?>" >

</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>