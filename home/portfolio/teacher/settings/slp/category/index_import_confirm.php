<?php
// Modifing by 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/oleCategory/libpf-category.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oleCategory/libpf-subCategory.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oleCategory/libpf-program-title.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/ole_program.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libpf-ole.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

# Error Code
define ("ERROR_TYPE_NUMBER_OF_DATA_NOT_MATCH", 1);
define ("ERROR_TYPE_EMPTY_CATEGORY_NAME", 2);
define ("ERROR_TYPE_EMPTY_PROGRAM", 3);
define ("ERROR_TYPE_EMPTY_OLE_COMPONENT", 4);
define ("ERROR_TYPE_OLE_COMPONENT_NOT_FOUND", 5);

unset($error_data);
$count_success = 0;
if ($g_encoding_unicode) {
	$import_coding = ($g_chinese == "") ? "b5" : "gb";
}

# uploaded file information
$filepath = $userfile;
$filename = $userfile_name;

$TmpIntExt = getCovertedIntExt(trim($IntExt));

$uploaderUserId = $UserID;  // variable from session

$count_new = 0;
$count_updated = 0;
$display_content = "";
if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath))
{	
	header("Location: index_import.php");
	exit;
}
else
{
	intranet_opendb();
	$lpf = new libpf_slp();
	$lpf_ole = new libpf_ole();
	$objTemptable = new Timetable($TimetableID);
	$lpf->CHECK_ACCESS_IPORTFOLIO();
	//$lpf->ACCESS_CONTROL("ole");

	$CurrentPage = "Settings_OLE";
	$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_OLE);
	$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

	### Tab Menu Settings ###
	$TabMenuArr  = getTabMenuArr();

	//*** current page index*****
	// 0 -   * Components of OLE
	// 1 -   * Programme Category
	// 2 -   * Management Group
	// 3 -   * Preset Participation Role
	// 4 -   * Submission Period
	// 5 -   * Record Approval

	$currentPageIndex = 1;
	$TabMenuArr = libpf_tabmenu::getSlpSettingsTags(IPF_CFG_SLP_SETTING_OLE, $currentPageIndex);
	$TabMenuDisplay = $lpf->GET_TAB_MENU($TabMenuArr);

	$linterface = new interface_html();
	$linterface->LAYOUT_START();

	$li = new libdb();
	$lo = new libfilesystem();
	$limport = new libimporttext();
	
    $ext = strtoupper($lo->file_ext($filename));

	$fileempty = false;

    if ($ext == ".CSV" || $ext == ".TXT")
    {
       	$data = $limport->GET_IMPORT_TXT($filepath);
		if($data == NULL){
			$fileempty = true;
		} else {
       		$header_row = array_shift($data);                   # drop the title bar
		}
    }
	
    # Check Title Row
	$file_format = getFileFormat($IntExt);
	
    $format_wrong = false;
    $file_size = sizeof($file_format);
    for ($i=0; $i<$file_size; $i++)
    {
         if ($header_row[$i]!=$file_format[$i])
         {
             $format_wrong = true;
             break;
         }
    }
	# Incorrect header format
    if($fileempty){
    	$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
		$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
		$STEPS_OBJ[] = array($i_general_imported_result, 0);
    }
    elseif ($format_wrong)
    {	
    	
    	$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
		$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
		$STEPS_OBJ[] = array($i_general_imported_result, 0);
		
        $correct_format = "<table border='0' cellpadding='5' cellspacing='0'>\n";
        
        for ($i=0; $i<$file_size; $i++)
        {
             $correct_format .= "<tr><td>".$file_format[$i]."</td></tr>\n";
        }
        $correct_format .= "</table>\n";

        $wrong_format = "<table border='0' cellpadding='5' cellspacing='0'>\n";
        $header_size = sizeof($header_row);
        for ($i=0; $i<$header_size; $i++)
        {
			$field_title = ($header_row[$i]!=$file_format[$i]) ? "<u>".$header_row[$i]."</u>" : $header_row[$i];
			$wrong_format .= "<tr><td>".$field_title."</td></tr>\n";
        }
        $wrong_format .= "</table>\n";

        $display_content .= "<table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
		$display_content .= "<tr><td valign='top'>$correct_format</td><td width='90%' align='center'>VS</td><td valign='top'>$wrong_format</td></tr>\n";
        $display_content .= "</table>\n";
        
    }
    # Correct header format	
    else
    {
		$li->Start_Trans();
		
		// Remove old temp table
		$sql = "Delete from {$eclass_db}.TEMP_OLE_PROGRAM_IMPORT Where UserID = $uploaderUserId";
		$li->db_db_query($sql);
		
		// Get All ELE Component
		$ELEKeyArr = array_keys($lpf->GET_ELE());
		$ele_list = $lpf->GET_OLE_ELE_LIST();
		$ELETitleArr = Get_Array_By_Key($ele_list, 'DefaultID');
		$ELEIDArr = Get_Array_By_Key($CategoryInfoArr, 'RecordID');
		$ELE_title_size = sizeof($ELETitleArr);
		for($i=0; $i<$ELE_title_size;$i++){
			if(strpos($ELETitleArr[$i],'[') === false){
				$ELETitleArr[$i] = '['.$ELETitleArr[$i].']';
			}
		}
		
		$import_table = "<br><table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
		$import_table .= "<tr class=tableTitle bgcolor='#CFE6FE'><td>".$Lang['iPortfolio']['cat_name_eng']."</td><td>".$Lang['iPortfolio']['sub_cat_name_eng']."</td><td>".$Lang['iPortfolio']['program_name']."</td><td>".$Lang['iPortfolio']['old_compo']."</td></tr>\n";
		$css_color = ($i%2==0) ? "#FFFFFF" : "#F3F3F3";
        $data_size = sizeof($data);
        for ($i=0; $i<$data_size; $i++)	//	This data get the data from import file
		{		
			list($t_cat_name, $t_subcat_name, $t_pro_name, $t_ELE) = $data[$i];
				
			$cat_exist = 0;
			$sub_cat_ids = 0;
			# check the header column and the content column number
			if (count($header_row) != count($data[$i])) {
				$error_data[] = array($i,ERROR_TYPE_NUMBER_OF_DATA_NOT_MATCH,$corrected[$i]);
				continue;
			}
				
			# check empty category name
			if(empty($t_cat_name)){
				$error_data[] = array($i,ERROR_TYPE_EMPTY_CATEGORY_NAME,$data[$i]);
				continue;
			}
					
			# check empty program name
			if(empty($t_pro_name)){
				$error_data[] = array($i,ERROR_TYPE_EMPTY_PROGRAM,$data[$i]);
				continue;
			}
				
			# check empty ELE
			if(empty($t_ELE)){
				$error_data[] = array($i,ERROR_TYPE_EMPTY_OLE_COMPONENT,$data[$i]);
				continue;
			}
				
			# not exist ELE
			$enter_ELE = explode(',',$t_ELE);
			$import_ELE_size = sizeof($enter_ELE);
			for($j=0;$j<$import_ELE_size;$j++){
				if(!in_array($enter_ELE[$j], $ELETitleArr, true)){
					$error_data[] = array($i,ERROR_TYPE_OLE_COMPONENT_NOT_FOUND,$data[$i]);
					break;
				}
			}
			if($import_ELE_size > 1){
				$t_ELE = implode(',',array_unique($enter_ELE));
			}
			$import_table .= "<tr bgcolor='$css_color'><td class='tabletext'>".$data[$i][0]."</td><td class='tabletext'>".$data[$i][1]."</td><td class='tabletext'>".$data[$i][2]."</td><td class='tabletext'>".$t_ELE."</td></tr>\n";
            
            // store into temp table
			$sql = "INSERT INTO {$eclass_db}.TEMP_OLE_PROGRAM_IMPORT
					(UserID, RowNumber, ProgramTitle, CatTitle, SubCatTitle, Component)
					VALUES ('$uploaderUserId','".$li->Get_Safe_Sql_Query($i)."', '".$li->Get_Safe_Sql_Query($t_pro_name)."', '".$li->Get_Safe_Sql_Query($t_cat_name)."', '".$li->Get_Safe_Sql_Query($t_subcat_name)."', '".$li->Get_Safe_Sql_Query($t_ELE)."')";	
			
			$q_result = $li->db_db_query($sql);
							
    	}
    	# check any error data    	
		if (count((array)$error_data) > 0) {
			$li->RollBack_Trans();
		} else {
			$import_table .= "</table>\n";
			$li->Commit_Trans();
		}
		
	}

}

# Display import stats
if (sizeof($error_data)>0)
{	
	
	$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
	$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
	$STEPS_OBJ[] = array($i_general_imported_result, 0);
	
    $error_table = "<br><table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
    $error_table .= "<tr class=tableTitle bgcolor='#CFE6FE'><td>".$ec_guide['import_error_row']."</td><td>".$ec_guide['import_error_reason']."</td><td>".$ec_guide['import_error_detail']."</td></tr>\n";
	$error_data_size = sizeof($error_data);
    for ($i=0; $i<$error_data_size; $i++)
    {
    
         list ($t_row, $t_type, $t_data) = $error_data[$i];
         $t_row += 2;     # set first row to 1
         $css_color = ($i%2==0) ? "#FFFFFF" : "#F3F3F3";
         $error_table .= "<tr bgcolor='$css_color'><td class='tabletext'>$t_row</td><td class='tabletext'>";
         $reason_string = "Unknown";
    
         switch ($t_type)
         {
                case ERROR_TYPE_NUMBER_OF_DATA_NOT_MATCH:
					$reason_string = $ec_guide['import_header_content_column_not_match'];
					break;
				case ERROR_TYPE_EMPTY_CATEGORY_NAME:
					$reason_string = $ec_guide['import_empty_category_title'];
					break;
				case ERROR_TYPE_EMPTY_PROGRAM:
					$reason_string = $ec_guide['import_empty_program_title'];
					break;
				case ERROR_TYPE_EMPTY_OLE_COMPONENT:
					$reason_string = $ec_guide['import_empty_ELE'];
					break;
				case ERROR_TYPE_OLE_COMPONENT_NOT_FOUND:
					$reason_string = $ec_guide['import_ELE_not_found'];
					break;
				default:
                     $reason_string = $ec_guide['import_error_unknown'];
                     break;
         }
    
         $error_table .= $reason_string;
         $error_table .= "</td><td class='tabletext'>".implode(",",$t_data)."</td></tr>\n";
    
    }
    $error_table .= "</table>\n";
    $display_content .= $error_table;

} elseif (!$format_wrong) {

	$display_content .= $import_table;	
	
	// Step display
	$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
	$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
	$STEPS_OBJ[] = array($i_general_imported_result, 0);
	
}

function getTabMenuArr()
{
	global $iPort;
	$TabMenuArr = array();
	$TabMenuArr[] = array("ele.php", $iPort["ele"], 0);
	$TabMenuArr[] = array("category_index.php", $iPort["category"], 1);
	$TabMenuArr[] = array("tag.php", $iPort["tag"], 0);
	$TabMenuArr[] = array("activity_title_template.php", $iPort["activity_title_template"], 0);
	$TabMenuArr[] = array("activity_role_template.php", $iPort["activity_role_template"], 0);
	$TabMenuArr[] = array("student_submission_period.php", $iPort["student_submission_period"], 0);
	$TabMenuArr[] = array("record_approval.php", $iPort["record_approval"], 0);

	return $TabMenuArr;
}

?>	
	
<?=$TabMenuDisplay?>
<table align="left" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" border="0" align="absmiddle"> <?=$button_import?> </a>
	</td>
	<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
</tr>
</table>
<br />
<FORM enctype="multipart/form-data" method="POST" name="form" action="index_import_update.php" >
	<div align="right">
	<?= $data == NULL? $linterface->GET_SYS_MSG("",$ec_guide['import_error_empty_import_data']) : "" ?>
	<?= ($format_wrong && $data != NULL)? $linterface->GET_SYS_MSG("",$ec_guide['import_error_wrong_format']) : "" ?>
	<?= sizeof($error_data)? $linterface->GET_SYS_MSG("",$ec_guide['import_error_wrong_import_data']) : "" ?>
	<br/>
	</div>
	<?= $linterface->GET_STEPS($STEPS_OBJ) ?>
	<br/>
	<?= $display_content ?>
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	<div align="center" style="padding-top:5px;">
	<?= (sizeof($error_data) || $format_wrong)? $linterface->GET_ACTION_BTN($ec_guide['import_back'], "button", "javascript:history.back();") : $linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "submit", "javascript: document.form.submit();");  ?>
	<?= (sizeof($error_data) || $format_wrong)? $linterface->GET_ACTION_BTN($iPort["btn"]["cancel"], "button", "self.location='index.php'") : $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "javascript:history.back();"); ?>
	</div>
	
	<input type="hidden" name="modes" value="1" >
	<input type="hidden" name="userfile" value="<?= $filepath ?>" >
	<input type="hidden" name="userfile_name" value="<?= $filename ?>" >
	
</FORM>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

function getCovertedIntExt($ParIntExt=0) {
	if ($ParIntExt == 0) {
		return "INT";
	} else if ($ParIntExt == 1) {
		return "EXT";
	}
	return null;
}

function getFileFormat($ParIntExt)
{
    define("CSV_CATEGORYNAME", "CategoryName (Eng)");
    define("CSV_SUBCATEGORYNAME", "SubCategoryName (Eng)");
    
    define("CSV_PROGRAMMENAME", "Programme Name");
    define("CSV_OLECOMPONENT", "OLE Component(s)");
    
    $file_format = array();
    array_push($file_format, CSV_CATEGORYNAME);
    array_push($file_format, CSV_SUBCATEGORYNAME);
    
    array_push($file_format, CSV_PROGRAMMENAME);
    array_push($file_format, CSV_OLECOMPONENT);

	return $file_format;
}

?>