<?php
## Using By : Max
##############################################
##	Modification Log:
##	2010-02-18 Max (201002180941)
##	- Get the tag menu from includes/portfolio25/libpf-tabmenu.php instead of slpsettingcommonfunction.php

##	2010-01-20 Max(201001201615)
##	- Generate the Tag Menu By function call 
##############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/oleCategory/libpf-category.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oleCategory/libpf-subCategory.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oleCategory/libpf-program-title.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/dynReport/JSON.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
// temp lang
//include_once("temp_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();


$task = trim($task);
$task = ($task == "")? "listCat" : $task;
// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";

$luser = new libuser($UserID);
$lpf = new libportfolio2007();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

############  Cotnent Here ######################
// page number settings

// Tab Menu Settings
$TabMenuArr  = getTabMenuArr();

// Table Action Button (JS, Image, Image_name, title)
$TableActionArr = array();
$TableActionArr = getTableActionArr();

#################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_OLE);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

// Tab Menu Settings
//*** current page index*****
// 0 -   * Components of OLE
// 1 -   * Programme Category
// 2 -   * Management Group
// 3 -   * Preset Participation Role
// 4 -   * Submission Period
// 5 -   * Record Approval
$currentPageIndex = 1;
$TabMenuArr = libpf_tabmenu::getSlpSettingsTags(IPF_CFG_SLP_SETTING_OLE, $currentPageIndex);

$TabMenuDisplay = $lpf->GET_TAB_MENU($TabMenuArr);

//echo "ttt ".$task."<br/>";
/*debug_r($_POST);
echo "ttt ".$task."<br/>";

exit();*/
switch($task)
{
/*
	case 'listCat': //LIST CATEGORY
		$linterface->LAYOUT_START();
		include_once("template/listCat.tmpl.php");
		$linterface->LAYOUT_STOP();
	break;

	case 'listSubCat': //LIST SUB CATEGORY
	  $PAGE_NAVIGATION[] = array("Category List"); // tmp
    $PAGE_NAVIGATION[] = array("Cat 1"); // tmp
		$linterface->LAYOUT_START();
		include_once("template/listSubCat.tmpl.php");
		$linterface->LAYOUT_STOP();
	break;

	case 'listProg': //LIST PROGRAM
	  $PAGE_NAVIGATION[] = array("Category List"); // tmp
    $PAGE_NAVIGATION[] = array("Cat 1 Sub Cat List"); // tmp
    $PAGE_NAVIGATION[] = array("Sub Cat"); // tmp    
		$linterface->LAYOUT_START();
		include_once("template/listProg.tmpl.php");
		$linterface->LAYOUT_STOP();
	break;

	case 'cat_update':
		saveCat();
		break;
*/
  case "ajax":
    $task_script = "ajax/" . str_replace("../", "", $script) . ".php";

		// Task exists?
		if (file_exists($task_script)) {
			include_once($task_script);
		}
		else {
			$linterface->LAYOUT_START();
			echo "error! task not find<br/>";
			$linterface->LAYOUT_STOP();
		}
		break;
		
	default:
		// By default, open the script with the same name as the task (after appended with ".php").
		// Prevent searching parent directories, like 'task=../../index'.
		$task_script = "task/" . str_replace("../", "", $task) . ".php";
		
		// Task exists?
		if (file_exists($task_script)) {
			include_once($task_script);
		}
		else {
			$linterface->LAYOUT_START();
			echo "error! task not find<br/>";
			$linterface->LAYOUT_STOP();
		}
		break;
			
}

intranet_closedb();
exit();



//////////////////
//FUNCTION LIST //
//////////////////
/*
function saveCat()
{

 # DO SAVE 
 //....
 
 
 
 
 echo "
 <script>
    parent.index_reload_page('msg'); 
 </script>
 " ;
}
*/
function getTabMenuArr()
{
	global $iPort;
	$TabMenuArr = array();
	$TabMenuArr[] = array("ele.php", $iPort["ele"], 0);
	$TabMenuArr[] = array("category_index.php", $iPort["category"], 1);
	$TabMenuArr[] = array("tag.php", $iPort["tag"], 0);
	$TabMenuArr[] = array("activity_title_template.php", $iPort["activity_title_template"], 0);
	$TabMenuArr[] = array("activity_role_template.php", $iPort["activity_role_template"], 0);
	$TabMenuArr[] = array("student_submission_period.php", $iPort["student_submission_period"], 0);
	$TabMenuArr[] = array("record_approval.php", $iPort["record_approval"], 0);

	return $TabMenuArr;
}

function getTableActionArr()
{
	global $PATH_WRT_ROOT,$image_path,$LAYOUT_SKIN,$ec_iPortfolio;

	$TableActionArr = array();
	$TableActionArr[] = array("javascript:jCheckPick(document.form1,'RecordID[]','update_ele.php?Action=activate', 1, 1)", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_approve.gif", "imgPublic", $ec_iPortfolio['activate']);
	$TableActionArr[] = array("javascript:jCheckPick(document.form1,'RecordID[]','update_ele.php?Action=deactivate', 2, 1)", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_reject.gif", "imgPrivate", $ec_iPortfolio['deactive']);
	$TableActionArr[] = array("javascript:if(jCHECK_OP('".$ec_warning["remove_default"]."')) checkRemove(document.form1,'RecordID[]','update_ele.php?Action=remove')", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_delete.gif", "imgDelete", $ec_iPortfolio['delete']);
	$TableActionArr[] = array("javascript:if(jCHECK_OP('".$ec_warning["edit_default"]."')) checkEdit(document.form1,'RecordID[]','edit_ele.php')", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_edit.gif", "imgEdit", $ec_iPortfolio['edit']);
	
	return $TableActionArr;
}


?>

