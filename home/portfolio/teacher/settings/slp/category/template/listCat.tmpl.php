<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script language="JavaScript">
function index_reload_page(msg){
  this.tb_remove();     //Close thickbox
  window.location.reload();      //Refresh page
}
function edit_category(obj,element){
  if(countChecked(obj,element)==1) {
    var cat_obj = document.getElementsByName(element);
    
    for(var i=0; i<cat_obj.length; i++)
    {
      if(cat_obj[i].checked)
      {
        var cat_id = cat_obj[i].value;
        break;
      }
    }
  
    //tb_show("","task/editCat.php?category_id="+cat_id+"&KeepThis=true&TB_iframe=true&height=200&width=500","");tb_show("","task/editCat.php?category_id="+cat_id+"&KeepThis=true&TB_iframe=true&height=200&width=500","");
    tb_show("","index.php?task=editCat&category_id="+cat_id+"&KeepThis=true&TB_iframe=true&height=200&width=500","");
  } else {
    alert(globalAlertMsg1);
  }
}
function change_category_status(obj,element,status){
  if(countChecked(obj,element)==0)
    alert(globalAlertMsg2);
  else{
    var msg = (status == 'active') ? globalAlertActivate : globalAlertMsg5;
  
    if(confirm(msg)){
      obj.status.value = status;
      obj.task.value = 'changeCatStatus';
      obj.submit();
    }
  }
}
function delete_category(obj,element){
  if(countChecked(obj,element)==0)
          alert(globalAlertMsg2);
  else{
    if(confirm(globalAlertMsg3)){	            
      obj.task.value = 'deleteCat';                
      obj.submit();				             
    }
  }
}

</script>

<form name="form1" id="form1" action="index.php" method="post">
<!--###### Content Board Start ######-->
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="bottom">
    <!-- ###### Tabs Start ######-->
    <?=$TabMenuDisplay?>
    <!-- ###### Tabs End ######-->					
    </td>
  </tr>

  <tr> 
    <td class="main_content">
      <div class="navigation">
        <?=$PageNavigationDisplay?>
      </div>
      <div class="content_top_tool">
      <br>
        <? $root_path = "../../../../../../../";?>
          <div class="Conntent_tool">
              <a href="index.php?task=newCat&KeepThis=true&TB_iframe=true&height=200&width=500" class="new thickbox"><?=$Lang['Btn']['New']?></a>
          </div>
          <div class="Conntent_tool">
              <a href="index_import.php" class="import"><?=$Lang['Btn']['Import']?></a>
          </div>
        <div class="Conntent_search" >
          	<input name="cat_searching_keywords" type="text" value="<?= $cat_searching_keywords ?>"/>
        </div>
        <br style="clear:both" />
      </div>
      <div class="table_board">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="bottom"><div class="table_filter"></div></td>
            <td valign="bottom"><div class="common_table_tool">
              <a href="javascript:change_category_status(document.form1,'record_id[]','deactive')" class="tool_reject"><?=$ec_iPortfolio['deactive']?></a>
              <a href="javascript:change_category_status(document.form1,'record_id[]','active')" class="tool_approve"><?=$ec_iPortfolio['activate']?></a>
              <a href="javascript:edit_category(document.form1,'record_id[]')" class="tool_edit" title="Edit"><?=$ec_iPortfolio['edit']?></a>
              <a href="javascript:delete_category(document.form1,'record_id[]')" class="tool_delete" title="Delete"><?=$ec_iPortfolio['delete']?></a>
            </div></td>
          </tr>
        </table>
        
        <?=$CategoryListTable?>
      </div>
    </td>
  </tr>
</table>
<!--###### Content Board End ######-->
<input type="hidden" name="order" value=<?=$order?> />
<input type="hidden" name="pageNo" value=<?=$li->pageNo?> />
<input type="hidden" name="field" value=<?=$field?> />
<input type="hidden" name="page_size_change" value=<?=$page_size_change?> />
<input type="hidden" name="numPerPage" value=<?=$li->page_size?> />

<input type="hidden" name="task" />
<input type="hidden" name="status" />
</form>