<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>New Text</title>
<link href="/templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
</head>

<script language="JavaScript">

function getSubCat(){
  var cat_id = $("[name=category_id]").val();

  $.ajax({
    type: "POST",
    url: "index.php",
    data: "task=ajax&script=get_subcat&CatID="+cat_id,
    dataType: "json",
    success: function(data){
      $("[name=subcategory_id]").children().remove().end().append($("<option></option>").val("").html("--"));
      
      $.each(data, function(){
        $("[name=subcategory_id]").append( 
          $("<option></option>").val(this.SubCatID).html(<?=Get_Lang_Selection("this.ChiTitle", "this.EngTitle")?>)
        ); 
      });
    }
  });
}

function checkform(formObj)
{
  if($("[name=category_id]").val() == "")
  {
    alert("AAA");
    return false;
  }

  var moveProgTitle = parent.document.getElementsByName("record_id[]");

  for(var i=0; i<moveProgTitle.length; i++)
  {
    if(!moveProgTitle[i].checked) continue;
  
    var input = document.createElement("input");
    input.setAttribute("type", "hidden");
    input.setAttribute("name", "record_id[]");
    input.setAttribute("value", moveProgTitle[i].value);
    document.getElementById("form1").appendChild(input);
  }

  return true;  
}

</script>

<body>
<form name="form1" id="form1" action="index.php" method="post" onSubmit="return checkform(this)">
<div class="edit_pop_board edit_pop_board_simple">
  <h1><?=$iPort["activity_title_template"]?> &gt; <span><?=$ec_iPortfolio['SLP']['MoveTo']?> </span></h1>
  <div class="edit_pop_board_write">
    <table class="form_table inside_form_table">
    <col class="field_title" />
    <col  class="field_c" />
      <tr>
        <td><?=$ec_iPortfolio['SLP']['Category']?></td>
        <td>:</td>
        <td><?=$ProgTitleCatInput?></td>
      </tr>
      <tr>
        <td><?=$ec_iPortfolio['SLP']['SubCategory']?></td>
        <td>:</td>
        <td><?=$ProgTitleSubCatInput?></td>
      </tr>
    </table>
    <br />
  </div>
  <div class="edit_bottom">
    <p class="spacer"></p>
    <input type="submit" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="Submit" />
    <input type="button" class="formbutton" onclick="parent.tb_remove()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="Cancel" />
    <p class="spacer"></p>
  </div>
</div>
<input type="hidden" name="task" value="updateMoveProgTitle" />
</body>
</html>