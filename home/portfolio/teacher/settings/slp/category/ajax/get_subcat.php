<?php

if(empty($CatID))
{
  $subcat_str = '{}';
}
else
{
  $lpf_sub_cat = new subCategory();
  $lpf_sub_cat->SET_CLASS_VARIABLE("category_id", $CatID);
  $sub_cat_arr = $lpf_sub_cat->GET_SUBCATEGORY_LIST();
  
  $json = new Services_JSON();
  $subcat_str = (count($sub_cat_arr) > 0) ? $json->encode($sub_cat_arr) : '{}';
}

echo $subcat_str;

?>