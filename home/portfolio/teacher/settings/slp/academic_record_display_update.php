<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lpf = new libportfolio();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$school_record_config_file = "$eclass_root/files/school_record_config.txt";
$li_fs = new libfilesystem();

$config_array = unserialize($li_fs->file_read($school_record_config_file));
$config_array["academic_record_display"] = empty($academic_display) ? array() : $academic_display;
$li_fs->file_write(serialize($config_array), $school_record_config_file);
$msg = "update";

intranet_closedb();

header("Location: academic_record_display.php?msg=$msg");
?>
