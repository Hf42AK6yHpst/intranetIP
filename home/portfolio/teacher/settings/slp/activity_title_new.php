<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";
$CurrentPageName = $iPort['menu']['ole']." ".$iPort['menu']['settings'];

$luser = new libuser($UserID);
$lpf = new libpf_slp();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

# Get the type name
$lf = new libwordtemplates_ipf(3);
$file_array = $lf->file_array;
for($i=0; $i<sizeof($file_array); $i++)
{
	$type_id = trim($file_array[$i][0]);
	$type_name = trim($file_array[$i][1]);
	if($type_id==$temp_type)
	{
		$TypeName = $type_name;
		break;
	}
}

$DefaultELEArray = array();
$TitleID = $RecordID[0];
if($TitleID!="")
{	
	$data = $lf->getTemplatesContent($temp_type);
	if(!empty($data))
	{
		$DataArray = explode("\n", $data);
		$CurrentTitle = $DataArray[$TitleID];
		if(!empty($CurrentTitle))
		{
			$TempArray = explode("\t", $CurrentTitle);
			$Title = $TempArray[0];
			$ELEList = $TempArray[1];
			if(!empty($ELEList)) {
				$DefaultELEArray = explode(",", $ELEList);
			}
		}
	}
}

# generate ELE table
$ELEArray = $lpf->GET_ELE();

if(!empty($ELEArray))
{
	$ELETable = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
	foreach($ELEArray as $ELE_ID => $ELE_Name)
	{
		$checked = (is_array($DefaultELEArray) && in_array($ELE_ID, $DefaultELEArray)) ? "CHECKED='CHECKED'" : "";
		$ELETable .= "<tr>";
		$ELETable .= "<td width=\"30%\"><INPUT type='checkbox' name='ele[]' value='".$ELE_ID."' id='".$ELE_ID."' {$checked}><label for='".$ELE_ID."'>".$ELE_Name."</label></td>";
		$ELETable .= "</tr>";
		$count++;
	}
	$ELETable .= "</table>";
}


?>

<script language="JavaScript">
function checkform(formObj){
	if(formObj.Title.value=="")
	{
		alert("<?=$ec_warning['title']?>");
		formObj.Title.focus();
		return false;
	}

	formObj.submit();
}

</script>

<form name="form1" action="activity_title_new_update.php" method="POST" >
	<br/>
	<table width="95%" border="0" cellspacing="0" cellpadding="0">
		<tr><td>
			<table id="body_frame1" width="100%" border="0" cellspacing="0" cellpadding="4" align="center">
				<br />
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="15%">
					<?= $ec_iPortfolio['mtype'] ?> 
					</td>
					<td valign="top" nowrap="nowrap" class="tabletext">
						<?= $TypeName ?>
						<br/>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $iPort["title"] ?> 
						<span class="tabletextrequire">*</span>
					</td>
					<td valign="top" class="tabletext"><input class="text" type="text" name="Title" size="50" maxlength="255" value="<?=$Title?>">
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$iPort["default_ele"]?>
					</td>
					<td>
						<?= $ELETable ?>
					</td>
					
				</tr>
					
				</tr>
			</table>
		</td></tr>
	</table>
	
	<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
		<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td class="tabletextremark" colspan="6"><?=$ec_iPortfolio['mandatory_field_description']?></td>
		</tr>
		<tr>
			<td align="center" colspan="6">
				<div style="padding-top: 5px">
					<?= $linterface->GET_ACTION_BTN($button_submit, "button","javascript: checkform(document.form1);")?>&nbsp;
					<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
					<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()")?>&nbsp;
				</div>
			</td>
		</tr>
	</table>
	
  <input type="hidden" name="temp_type" value="<?=$temp_type?>" />
  <input type="hidden" name="TitleID" value="<?=$TitleID?>" />
</form>

<!-- ENd Content Here -->


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>

