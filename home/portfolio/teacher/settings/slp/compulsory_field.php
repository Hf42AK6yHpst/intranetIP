<?php
## Using By : 
##############################################
##	Modification Log:
## 	2015-10-19 Omas
## -create this page
##
##############################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_settings.php");
//include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_period_settings.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";

# Get all period settings from DB
//$objIpfSetting = iportfolio_settings::getInstance();
//$objIpfPeriodSetting = new iportfolio_period_settings();
//$settingsArray = $objIpfPeriodSetting->getSettingsArray();

$lpf = new libpf_slp();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

##################################################
############  Cotnent Here #######################
##################################################

// Tab Menu Settings
//*** current page index*****
// 0 -   * Components of OLE
// 1 -   * Programme Category
// 2 -   * Management Group
// 3 -   * Preset Participation Role
// 4 -   * Submission Period
// 5 -   * Record Approval
$currentPageIndex = 7;
$TabMenuArr = libpf_tabmenu::getSlpSettingsTags(IPF_CFG_SLP_SETTING_OLE, $currentPageIndex);


## OLE
$OLE_Title = $linterface->GET_NAVIGATION2_IP25($ec_iPortfolio['ole']);
$objIpfSetting = new iportfolio_settings();
$CompulsorySettingArr = explode(',',$objIpfSetting->findSetting($ipf_cfg["Student_OLE_CompulsoryFields"]["SettingName"]));
$OLE_items = '';
foreach($ipf_cfg["Student_OLE_CompulsoryFields"]["Field"] as $fieldname){
	if(in_array($fieldname,$CompulsorySettingArr)){
		$isCheck = 1;
	}
	else{
		$isCheck = 0;
	}
	$OLE_items .= $linterface->Get_Checkbox('Student_OLE_Compulsory_'.$fieldname, 'Student_OLE_Compulsory[]', $fieldname, $isCheck, '', $ec_iPortfolio[$fieldname]);
	$OLE_items .= '<br>';
}

## OLE Ext
$OLE_Ext_Title = $linterface->GET_NAVIGATION2_IP25($iPort['external_record']);
$CompulsorySettingArr = explode(',',$objIpfSetting->findSetting($ipf_cfg["Student_OLEExt_CompulsoryFields"]["SettingName"]));
$OLE_Ext_items = '';
foreach($ipf_cfg["Student_OLEExt_CompulsoryFields"]["Field"] as $fieldname){
	if(in_array($fieldname,$CompulsorySettingArr)){
		$isCheck = 1;
	}
	else{
		$isCheck = 0;
	}
	$OLE_Ext_items .= $linterface->Get_Checkbox('Student_OLEExt_Compulsory_'.$fieldname, 'Student_OLEExt_Compulsory[]', $fieldname, $isCheck, '', $ec_iPortfolio[$fieldname]);
	$OLE_Ext_items .= '<br>';
}

$Content = "";
$Content .= $lpf->GET_TAB_MENU($TabMenuArr);
$Content .= $linterface->GET_SYS_MSG($msg);
$Content .= '<table width="100%">';
$Content .= '<tr>';
$Content .= '<td colspan="2">'.$linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['iPortfolio']['SLP_Setting']['CompulsoryRemarks']).'</td>';
$Content .= '</tr>';
$Content .= '<tr>';
$Content .= '<td colspan="2">'.$OLE_Title.'</td>';
$Content .= '</tr>';
$Content .= '<tr>';
$Content .= '<td width="40"></td>';
$Content .= '<td>'.$OLE_items.'</td>';
$Content .= '</tr>';
$Content .= '<tr>';
$Content .= '<td colspan="2">'.$OLE_Ext_Title.'</td>';
$Content .= '</tr>';
$Content .= '<tr>';
$Content .= '<td></td>';
$Content .= '<td>'.$OLE_Ext_items.'</td>';
$Content .= '</tr>';
$Content .= '</table>';


### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
//$htmlAry['resetBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Reset'], "button", "goReset()", 'resetBtn');

##################################################
##################################################
##################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_OLE);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>
<script language="Javascript">

function goSubmit(){
	$('form#form1').attr('action', 'compulsory_field_update.php').submit();
}
function goReset(){
	
}
</script>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<form id="form1" name="form1" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
	<div class="table_board">
		<?=$Content?>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['resetBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
</td></tr></table>
</form>
</td></tr>
</table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
