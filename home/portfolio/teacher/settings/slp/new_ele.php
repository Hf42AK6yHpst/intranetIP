<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";

### Init Library ###
$lpf = new libpf_slp();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

// Tab Menu Settings
//*** current page index*****
// 0 -   * Components of OLE
// 1 -   * Programme Category
// 2 -   * Management Group
// 3 -   * Preset Participation Role
// 4 -   * Submission Period
// 5 -   * Record Approval
$currentPageIndex = 0;
$TabMenuArr = libpf_tabmenu::getSlpSettingsTags(IPF_CFG_SLP_SETTING_OLE, $currentPageIndex);

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_OLE);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['list'], "ele.php");
$MenuArr[] = array($button_new, "");

//////////////////////////////////////////////////////
////////////   GET THE PAGE CONTENT  /////////////////
//////////////////////////////////////////////////////
$Content = "";
$Content .= $lpf->GET_ELE_FORM($ParArr);
//////////////////////////////////////////////////////
//////////// END  GET THE PAGE CONTENT  //////////////
//////////////////////////////////////////////////////

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
function checkform(myObj)
{
	if (!check_text(myObj.EngTitle, "<?=$iPort["msg"]["warning_eng_title"]?>")) return false;
	if (!check_text(myObj.ChiTitle, "<?=$iPort["msg"]["warning_chi_title"]?>")) return false;
	
	return true;
} // end function check form

function jCancel()
{
	self.location.href = "ele.php";
}

function jReset()
{
	obj = document.form1;
	obj.reset();
}

function jSubmit()
{
	var obj = document.form1;
	if(checkform(obj))
	obj.submit();
}
</SCRIPT>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td><?=$lpf->GET_TAB_MENU($TabMenuArr)?></td></tr>
<tr><td class="navigation"><?=$linterface->GET_NAVIGATION($MenuArr)?></td></tr>
<tr><td align="center">
<!-- CONTENT HERE -->
<?=$Content?>
</td></tr></table>
</td></tr></table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
