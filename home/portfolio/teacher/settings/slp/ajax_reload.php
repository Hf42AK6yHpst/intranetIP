<?php
## Using By : ivan 
##############################################
##	Modification Log:
##############################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic-ui.php");

$libpf_academic = new libpf_academic();
$libpf_academic_ui = new libpf_academic_ui();

$Action = stripslashes($_REQUEST['Action']);
if ($Action == 'Reload_FullMark_Table')
{
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
	
	$AcademicYearID = $_POST['AcademicYearID'];
	$YearID = $_POST['YearID'];
	$YearID = ($YearID==-1)? '' : $YearID;
	$ViewMode = $_POST['ViewMode'];
	
	echo $libpf_academic_ui->Get_Settings_Subject_FullMark_Table($AcademicYearID, $YearID, $ViewMode);
}
?>