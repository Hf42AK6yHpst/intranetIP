<?php
## Using By : 
##############################################
##	Modification Log:
##############################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";

$lpf = new libpf_slp();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

##################################################
############  Cotnent Here #######################
##################################################
// Tab Menu Settings
//*** current page index*****
// 0 -   * Components of OLE
// 1 -   * Academic Result Type Display (for student)
$currentPageIndex = 2;
$TabMenuArr = libpf_tabmenu::getSlpSettingsTags(IPF_CFG_SLP_SETTING_SR, $currentPageIndex);

$school_record_config_file = "$eclass_root/files/school_record_config.txt";
$li_fs = new libfilesystem();
$config_arr = unserialize($li_fs->file_read($school_record_config_file));

// Check file not exist = not initialized
// Check value in array = selected
$score_check = (!file_exists($school_record_config_file) || (is_array($config_arr["academic_record_display"]) && in_array("Score", $config_arr["academic_record_display"]))) ? "CHECKED" : "";
$grade_check = (!file_exists($school_record_config_file) || (is_array($config_arr["academic_record_display"]) && in_array("Grade", $config_arr["academic_record_display"]))) ? "CHECKED" : "";
$scoregrade_check = (!file_exists($school_record_config_file) || (is_array($config_arr["academic_record_display"]) && in_array("ScoreGrade", $config_arr["academic_record_display"]))) ? "CHECKED" : "";
$rank_check = (!file_exists($school_record_config_file) || (is_array($config_arr["academic_record_display"]) && in_array("Rank", $config_arr["academic_record_display"]))) ? "CHECKED" : "";
$std_check = (!file_exists($school_record_config_file) || (is_array($config_arr["academic_record_display"]) && in_array("StandardScore", $config_arr["academic_record_display"]))) ? "CHECKED" : "";

##################################################
##################################################
##################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_SR);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<form name="form1" action="academic_record_display_update.php" method="POST">
  <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="right"><?=$linterface->GET_SYS_MSG($msg)?></td>
    </tr>
    <tr>
      <td>
        <?=$lpf->GET_TAB_MENU($TabMenuArr)?>
      
  			<table width="95%" border="0" cellspacing="0" cellpadding="4" align="center">
  				<tr>
  					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="15%">
  						<?=$Lang['iPortfolio']['StudentViewDisplay']?>
  					</td>
            <td valign="top" class="tabletext">
              <input type="checkbox" name="academic_display[]" id="score" value="Score" <?=$score_check?> /> <label for="score"><?=$ec_iPortfolio['display_by_score']?></label><br />
              <input type="checkbox" name="academic_display[]" id="grade" value="Grade" <?=$grade_check?> /> <label for="grade"><?=$ec_iPortfolio['display_by_grade']?></label><br />
              <input type="checkbox" name="academic_display[]" id="scoregrade" value="ScoreGrade" <?=$scoregrade_check?> /> <label for="scoregrade"><?=$ec_iPortfolio['display_by_score_grade']?></label><br />
              <input type="checkbox" name="academic_display[]" id="rank" value="Rank" <?=$rank_check?> /> <label for="rank"><?=$ec_iPortfolio['display_by_rank']?></label><br />
              <input type="checkbox" name="academic_display[]" id="std" value="StandardScore" <?=$std_check?> /> <label for="std"><?=$ec_iPortfolio['display_by_stand_score']?></label>
            </td>
  				</tr>
  			</table>
      </td>
    </tr>
  </table>
  
	<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
		<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center" colspan="6">
				<div style="padding-top: 5px">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
				</div>
			</td>
		</tr>
	</table>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
