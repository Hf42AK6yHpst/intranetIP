<?php
## Using By : 
##############################################
##	Modification Log:
##	2016-07-06 (Omas)
##	- added copy from other academic year
##	
##	2015-03-09 (Omas)
##	- hide tips
##############################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

// Get View Mode
// $ViewMode = stripslashes($_REQUEST['ViewMode']);
// $AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);
// $YearID = stripslashes($_REQUEST['YearID']);
$ViewMode = (isset($_REQUEST['ViewMode'])? $ViewMode : '' );
$AcademicYearID = (isset($_REQUEST['AcademicYearID'])? $AcademicYearID: Get_Current_Academic_Year_ID());
$YearID = (isset($YearID)? $_REQUEST['YearID']: '');

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";

$lpf = new libpf_slp();
$libfcm_ui = new form_class_manage_ui();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

##################################################
############  Cotnent Here #######################
##################################################
// Tab Menu Settings
//*** current page index*****
// 0 -   * Components of OLE
// 1 -   * Academic Result Type Display (for student)
$currentPageIndex = 1;
$TabMenuArr = libpf_tabmenu::getSlpSettingsTags(IPF_CFG_SLP_SETTING_SR, $currentPageIndex);


// Export , Copy
$Toolbar = $linterface->Get_Content_Tool_v30("export", "javascript:js_Go_Export();");
$Toolbar .= $linterface->Get_Content_Tool_v30("copy", "javascript:js_Go_Copy();", $Lang['SysMgr']['FormClassMapping']['CopyFromOtherAcademicYear'] );

// View Mode
$ViewModeArr = array();
$ViewModeArr[] = array($Lang['General']['Grade'], "javascript:js_Change_ViewMode('Grade');", '', ($ViewMode=='Grade')? 1 : 0);
$ViewModeArr[] = array($Lang['General']['Mark'], "javascript:js_Change_ViewMode('Mark');", '', ($ViewMode=='Mark')? 1 : 0);
$ViewModeArr[] = array($Lang['General']['All'], "javascript:js_Change_ViewMode('');", '', ($ViewMode=='')? 1 : 0);
$ViewModeButton = $linterface->GET_CONTENT_TOP_BTN($ViewModeArr); 

// Warning
$WarningBox = $linterface->Get_Warning_Message_Box('', $Lang['iPortfolio']['SubjectFullMarkInputWarning']);

// Tips Instruction
$IipsBox = $linterface->Get_Warning_Message_Box($Lang['General']['Tips'], $Lang['iPortfolio']['SubjectFullMarkInputTips'], $others="");


// Academic Year Selection
$AcademicYearSelection = getSelectAcademicYear('AcademicYearID', $tag='onchange="js_Changed_AcademicYear_Selection(this.value);"', $noFirst=1, $noPastYear=0, $AcademicYearID);

// Form Selection
$FormSelection = $libfcm_ui->Get_Form_Selection('YearID', $YearID, 'js_Changed_Form_Selection(this.value);', $noFirst=1, $isAll=1, $isMultiple=0);

// Save Button
$SaveBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "js_Save_FullMark_Info();", $id="Btn_Save");

##################################################
##################################################
##################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_SR);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

if($msg === 1)
	$xmsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
else if($msg === 0)
	$xmsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
else
	$xmsg = '';

$linterface->LAYOUT_START($xmsg);
// echo $linterface->Include_Cookies_JS_CSS();
echo $linterface->Include_CopyPaste_JS_CSS('2016');
echo $linterface->Include_Excel_JS_CSS();
?>
<script>
var jsCurAcademicYearID = '<?php echo $AcademicYearID?>';
var jsCurYearID = '<?php echo $YearID?>';

// var arrCookies = new Array();
// arrCookies[arrCookies.length] = "AcademicYearID";
// arrCookies[arrCookies.length] = "YearID";

$(document).ready( function() {
	<? if($clearCoo) { ?>
// 		for(i=0; i<arrCookies.length; i++)
// 		{
// 			var obj = arrCookies[i];
// 			//alert('obj = ' + obj);
// 			$.cookies.del(obj);
// 		}
	<? } ?>
	
// 	Blind_Cookies_To_Object();
	
// 	// Store the cookies value and apply them to the selection
// 	jsCurAcademicYearID = $.cookies.get(arrCookies[0]);

	if (jsCurAcademicYearID == null || jsCurAcademicYearID == '')
		jsCurAcademicYearID = $('select#AcademicYearID').val();
	
// 	jsCurYearID = $.cookies.get(arrCookies[1]);
	if (jsCurYearID == null || jsCurYearID == '')
		jsCurYearID = $('select#YearID option:nth-child(2)').val();
		
	$('select#AcademicYearID').val(jsCurAcademicYearID);
	$('select#YearID').val(jsCurYearID);
	
	js_Reload_FullMark_Table();
});

function js_Changed_AcademicYear_Selection(jsAcademicYearID) {
	jsCurAcademicYearID = jsAcademicYearID;
	js_Reload_FullMark_Table();
}

function js_Changed_Form_Selection(jsYearID) {

	var jsReload = true;
	if (jsYearID == '-1') {
		if (confirm('<?=$Lang['iPortfolio']['LoadAllFullMarkWarning']?>')) {
			jsReload = true;
		}
		else {
			jsReload = false;
			$('select#YearID').val(jsCurYearID);
		}
	}
	
	if (jsReload) {
		jsCurYearID = jsYearID;
		js_Reload_FullMark_Table();
	} 
}

function js_Reload_FullMark_Table() {
	$('div#FullMarkDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Reload_FullMark_Table',
			AcademicYearID: jsCurAcademicYearID,
			YearID: jsCurYearID,
			ViewMode : '<?=$ViewMode?>'
		},
		function(ReturnData)
		{
			jQuery.excel('dataRow');
		}
	);
}

function js_Save_FullMark_Info() {
	
	Block_Element('FullMarkDiv');
	js_Disable_Button('Btn_Save');
	
	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
	jsSubmitString += '&Action=Save_Subject_FullMark';
	$.ajax({  
		type: "POST",  
		url: "ajax_update.php",
		data: jsSubmitString,
		success: function(data) {
			
			if (data=='1')
			{
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
				js_Reload_FullMark_Table();
			}
			else
			{
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>');
			}
			
			UnBlock_Element('FullMarkDiv');
			js_Enable_Button('Btn_Save');
			Scroll_To_Top();
		} 
	});  
	return false;
}

function js_Change_ViewMode(jsViewMode)
{
	$('input#ViewMode').val(jsViewMode);
	$('form#form1').submit();
}

function js_Go_Export()
{
	$('form#form1').attr('action', 'subject_full_mark_export.php').submit();
	$('form#form1').attr('action', 'subject_full_mark.php');
}

function js_Go_Copy()
{
	$('form#form1').attr('action', 'subject_full_mark_copy.php').submit();
}
</script>
<form id="form1" name="form1" action="subject_full_mark.php" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td><?=$lpf->GET_TAB_MENU($TabMenuArr)?></td></tr>
		<tr>
			<td style="padding-left:7px;">
				<div class="content_top_tool">
					<div class="Conntent_tool"><?=$Toolbar?></div>
				</div>
				
				<div class="content_top_tool">
					<?=$ViewModeButton?>
				</div>
				
				<div><?=$WarningBox?></div>
				<div><?=$IipsBox?></div>
				
				<div class="table_filter">
					<?=$AcademicYearSelection?>
					&nbsp;
					<?=$FormSelection?>
				</div>
				<br style="clear:both;" />
				<br style="clear:both;" />
				
				<div id="FullMarkDiv"></div>
				<br style="clear:both;" />
				
				<div class="edit_bottom_v30">
					<?=$SaveBtn?>
				</div>
			</td>
		</tr>
	</table>
	
	<textarea style="height: 1px; width: 1px; visibility:hidden;" id="text1" name="text1"></textarea>
	
	<input type="hidden" id="ViewMode" name="ViewMode" value="<?=$ViewMode?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>