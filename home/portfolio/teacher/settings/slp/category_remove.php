<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lpf = new libportfolio();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$RecordArray = (is_array($RecordID)) ? $RecordID : array($RecordID);
$RecordList = implode($RecordArray, ",");

# RecordStatus = 1 (only pending record can be deleted)
$sql = "DELETE FROM {$eclass_db}.OLE_CATEGORY WHERE RecordID IN ({$RecordList})";
$success = $li->db_db_query($sql);

# remove the corresponding title template file
if($success)
{
	$fs = new libfilesystem();
	for($i=0; $i<sizeof($RecordArray); $i++)
	{
		$record_id = $RecordArray[$i];
		$file = $eclass_filepath."/files/portfolio_olr_".$record_id.".txt";
		$deleted = $fs->file_remove($file);
	}
}

intranet_closedb();

header("Location: category.php?msg=delete");
?>