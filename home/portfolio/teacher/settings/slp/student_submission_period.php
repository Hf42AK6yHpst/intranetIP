<?php
## Using By : Bill
##############################################
##	Modification Log:
##	2014-12-23 (Bill) [#J62561]
##	- Get all period settings of different forms from DB
##	- added js function checkAll_allow_submit() and SetAllTime(), modified checkform()
##
##	2010-02-18 Max (201002180941)
##	- Get the tag menu from includes/portfolio25/libpf-tabmenu.php instead of slpsettingcommonfunction.php
##
##	2010-01-20 Max(201001201615)
##	- Generate the Tag Menu By function call 
##############################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
//include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_settings.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_period_settings.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";

# Get all period settings from DB
//$objIpfSetting = iportfolio_settings::getInstance();
$objIpfPeriodSetting = new iportfolio_period_settings();
$settingsArray = $objIpfPeriodSetting->getSettingsArray();

$lpf = new libpf_slp();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

##################################################
############  Cotnent Here #######################
##################################################

// Tab Menu Settings
//*** current page index*****
// 0 -   * Components of OLE
// 1 -   * Programme Category
// 2 -   * Management Group
// 3 -   * Preset Participation Role
// 4 -   * Submission Period
// 5 -   * Record Approval
$currentPageIndex = 4;
$TabMenuArr = libpf_tabmenu::getSlpSettingsTags(IPF_CFG_SLP_SETTING_OLE, $currentPageIndex);

$Content = "";
$Content .= $lpf->GET_TAB_MENU($TabMenuArr);
$Content .= $linterface->GET_SYS_MSG($msg);
$Content .= $lpf->GET_SUBMISSION_PERIOD_FORM($settingsArray);

##################################################
##################################################
##################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_OLE);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
<!--
function checkform(obj)
{
	// Check all time settings
	var recordType = ["OLE", "EXT", "SLP"];
	
	for(var i=0; i<recordType.length; i++){
		var starttimeElements = document.getElementsByName('starttime_' + recordType[i] + '[]');
		var starthrElements = document.getElementsByName('sh_' + recordType[i] + '[]');
		var startminElements = document.getElementsByName('sm_' + recordType[i] + '[]');
		var endtimeElements = document.getElementsByName('endtime_' + recordType[i] + '[]');
		var endhrElements = document.getElementsByName('eh_' + recordType[i] + '[]');
		var endminElements = document.getElementsByName('em_' + recordType[i] + '[]');
		
		var classCount = starttimeElements.length;
		for (var j=0; j<classCount; j++){
			var isst = false;
			var startElement = starttimeElements[j];
			var starthr = starthrElements[j];
			var startmin = startminElements[j];
			var endElement = endtimeElements[j];
			var endhr = endhrElements[j];
			var endmin = endminElements[j];
			var check_submit = document.getElementById('allow_submit_' + recordType[i] + '_' + j);
			
			// Checking for start time
			if (typeof(startElement)!="undefined" && startElement.value!="")
			{
				if(!check_date(startElement, "<?php echo $assignments_alert_msg9; ?>")) {
					startElement.focus();
					return false;
				}
				isst = true;
			}
			
			// Checking for end time
			if (typeof(endElement)!="undefined" && endElement.value!="")
			{
				if(!check_date(endElement, "<?php echo $assignments_alert_msg9; ?>")) {
					endElement.focus();
					return false;
				}
				
				if (isst)
				{
					// Endtime < Starttime
					if(!compareTime(startElement, starthr, startmin, endElement, endhr, endmin)) {
						startElement.focus();
						alert("<?= $w_alert['start_end_time2'] ?>");
						return false;
					}
					
					// Endtime == Starttime
					if(startElement.value==endElement.value && starthr.value==endhr.value && startmin.value==endmin.value){
						startElement.focus();
						alert("<?= $w_alert['start_end_time2'] ?>");
						return false;
					}
				}
			}
			
			// Checking for empty start time and end time
			if (startElement.value=="" && endElement.value=="" && check_submit.checked)
			{
				startElement.focus();
				alert("<? echo $assignments_alert_msg9 ?>");
				return false;
			}
		}
	}
	return true;
}

function checkAll_allow_submit(recordType, count, value){
	// Set all checkbox in table
    for(i=0 ; i<count; i++) {
		document.getElementById('allow_submit_' + recordType + '_' + i).checked = value;
		document.getElementById('allow_submit_' + recordType + '_' + i).value = value;
    }
}

function SetAllTime(recordType, timeType, count){
	// Set start time or end time in table
	if(timeType == 'start'){
		var timeName = 'starttime_' + recordType;
		var hourName = 'sh_' + recordType;
		var minName = 'sm_' + recordType;
	} else {
		var timeName = 'endtime_' + recordType;
		var hourName = 'eh_' + recordType;
		var minName = 'em_' + recordType;
	}
	
	var hourAllElements = document.getElementsByName(hourName);
	var hourElements = document.getElementsByName(hourName + '[]');
	var minAllElements = document.getElementsByName(minName);
	var minElements = document.getElementsByName(minName + '[]');
	
	var date = document.getElementById(timeName).value;
	var hour = hourAllElements[0].value;
	var min = minAllElements[0].value;
	if(count > 0){
		for(var i=0; i<count; i++){
			document.getElementById(timeName + '_' + i).value = date;
			hourElements[i].value = hour;
			minElements[i].value = min;
		}
	}
}

function jReset()
{
	obj = document.form1;
	obj.reset();
}

function jSubmit()
{
	obj = document.form1;
	if(checkform(obj))
	obj.submit();
}
-->
</SCRIPT>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<!-- CONTENT HERE -->
<?=$Content?>
</td></tr></table>
</td></tr>
</table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
