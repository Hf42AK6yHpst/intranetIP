<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");

$type = trim($type);
if($type == ''){
	//should not be '', for safe, redirect to ipf home/portfolio/
	header("Location: /home/portfolio");
	exit();
}

intranet_auth();
intranet_opendb();

$lpf = new libportfolio();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$lf = new libwordtemplates_ipf($type);
$lf->updateTemplatesContent(0, stripslashes(htmlspecialchars($data)));

$msg = "update";

intranet_closedb();

header("Location: preset_item_template.php?value=".base64_encode('msg='.$msg.'&type='.$type));
?>