<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";
$CurrentPageName = $iPort['menu']['ole']." ".$iPort['menu']['settings'];

$luser = new libuser($UserID);
$lpf = new libportfolio2007();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

// page number settings
if ($order=="") $order=0;
if ($field=="") $field=2;

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if ($pageNo == "")
$pageNo = 1;

# Get default ELE
$sql =	"
					SELECT
						RecordID
					FROM
						{$eclass_db}.OLE_CATEGORY
					WHERE 
						RecordStatus = 2 OR RecordStatus = 3
				";
$DefaultProgram = $lpf->returnVector($sql);

////////////////////////////////////////////////////////
///// TABLE SQL
if ($order=="") $order=0;
if ($field=="") $field=2;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("EngTitle", "ChiTitle", "RecordStatus", "ModifiedDate");

/*
$sql = "SELECT
			if(RecordStatus=2, EngTitle, CONCAT('<a class=\"tablelink\" href=\"category_new.php?RecordID=', RecordID ,'\" >', EngTitle, '</a>')),
			if(RecordStatus=2, ChiTitle, CONCAT('<a class=\"tablelink\" href=\"category_new.php?RecordID=', RecordID ,'\" >', ChiTitle, '</a>')),
			if(RecordStatus=2, '{$ec_iPortfolio['default']}', if(RecordStatus=0, '{$button_private}', '{$button_public}')),
			ModifiedDate,
			if(RecordStatus=2, '<img src=\"$image_path/red_checkbox.gif\" vspace=3 hspace=4 border=0>', CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"RecordID[]\" value=', RecordID ,'>'))
		FROM
		{$eclass_db}.OLE_CATEGORY
		";
*/
$sql = "SELECT
			if((RecordStatus=2 || RecordStatus=3), EngTitle, CONCAT('<a class=\"tablelink\" href=\"category_new.php?RecordID=', RecordID ,'\" >', EngTitle, '</a>')),
			if((RecordStatus=2 || RecordStatus=3), ChiTitle, CONCAT('<a class=\"tablelink\" href=\"category_new.php?RecordID=', RecordID ,'\" >', ChiTitle, '</a>')),
			CASE RecordStatus
				WHEN 0 THEN '{$button_private}'
				WHEN 1 THEN '{$button_public}'
				WHEN 2 THEN '{$ec_iPortfolio['default']} - {$button_public}'
				WHEN 3 THEN '{$ec_iPortfolio['default']} - {$button_private}'
			END,
			ModifiedDate,
			CONCAT('<input type=checkbox name=\"RecordID[]\" value=', RecordID ,'>')
		FROM
		{$eclass_db}.OLE_CATEGORY
		";

// debug test
//$t_sql = "SELECT * from {$eclass_db}.OLE_CATEGORY";
//$row = $lpf->returnArray($t_sql);
//debug_r($row);

// TABLE INFO
$li->sql = $sql;
$li->db = $intranet_db;
$li->title = $ec_iPortfolio["category"];
$li->no_msg = $no_record_msg;
$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
$li->no_col = 6;
$li->row_height = 25;
$li->plain_column = 1;
$li->plain_column_css = "tablelink";
//$li->record_num_mode = "iportfolio"; // for school based scheme total record display
$li->title = $iPort['table']['records'];
$li->table_tag = "<table bgcolor='#cccccc' border='0' cellpadding='4' cellspacing='1' width='100%'>";
$li->row_alt = array("#FFFFFF", "#F3F3F3");
$li->row_valign = "top";

// TABLE COLUMN
$li->column_list .= "<td class='tabletop tabletopnolink' height='25' align='center'>#</span></td>\n";
$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(0,$ec_iPortfolio['title']." (".$ec_iPortfolio['lang_eng'].")", 1)."</td>\n";
$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(1,$ec_iPortfolio['title']." (".$ec_iPortfolio['lang_chi'].")", 1)."</td>\n";
$li->column_list .= "<td width='10%' nowrap='nowrap'>".$li->column(2,$ec_iPortfolio['status'], 1)."</td>\n";
$li->column_list .= "<td width='20%' nowrap='nowrap'>".$li->column(3,$ec_iPortfolio['SAMS_last_update'], 1)."</td>\n";
$li->column_list .= "<td>".$li->check("RecordID[]")."</td>\n";
$li->column_array = array(10,10,0,0,0,0);

// Tab Menu Settings
$TabMenuArr = array();
$TabMenuArr[] = array("ele.php", $iPort["ele"], 0);
$TabMenuArr[] = array("category.php", $iPort["category"], 1);
$TabMenuArr[] = array("activity_title_template.php", $iPort["activity_title_template"], 0);
$TabMenuArr[] = array("activity_role_template.php", $iPort["activity_role_template"], 0);
$TabMenuArr[] = array("student_submission_period.php", $iPort["student_submission_period"], 0);
$TabMenuArr[] = array("record_approval.php", $iPort["record_approval"], 0);

// Table Action Button (JS, Image, Image_name, title)
$TableActionArr = array();
$TableActionArr[] = array("javascript:document.form1.Action.value='activate';jCheckPick(document.form1,'RecordID[]','category_update.php', 1, 1)", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_approve.gif", "imgPublic", $ec_iPortfolio['activate'], "tool_approve");
$TableActionArr[] = array("javascript:document.form1.Action.value='deactivate';jCheckPick(document.form1,'RecordID[]','category_update.php', 2, 1)", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_reject.gif", "imgPrivate", $ec_iPortfolio['deactive'], "tool_reject");
$TableActionArr[] = array("javascript:if(jCHECK_OP('".$ec_warning["edit_default"]."')) checkEdit(document.form1,'RecordID[]','category_new.php')", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_edit.gif", "imgEdit", $ec_iPortfolio['edit'], "tool_edit");
$TableActionArr[] = array("javascript:if(jCHECK_OP('".$ec_warning["remove_default"]."')) checkRemove(document.form1,'RecordID[]','category_remove.php')", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_delete.gif", "imgDelete", $ec_iPortfolio['delete'], "tool_delete");

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
	globalAlertDeactivate = '<?=$iPort["msg"]["confirm_deactivate"]?>';
	defaultProgram = new Array(<?=implode(',',$DefaultProgram)?>)
	
	function jCHECK_OP(jParErrorMsg)
	{
		OP_allow = true;
		for(i=0; i<document.form1.elements.length; i++)
		{
			if(document.form1.elements[i].name == "RecordID[]" && document.form1.elements[i].checked)
			{
				for(j=0; j<defaultProgram.length; j++)
				{
					if(document.form1.elements[i].value == defaultProgram[j])
					{
						OP_allow = false;
						break;
					}
				}
			}
			
			if(!OP_allow)
			{
				alert(jParErrorMsg);
				break;
			}
		}			
		
		return OP_allow;
	}

</SCRIPT>

<br />
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<FORM name="form1" method="GET">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
				<?
				echo $lpf->GET_TAB_MENU($TabMenuArr);
				echo $linterface->GET_SYS_MSG($msg);
				echo $lpf->GET_NEW_BUTTON("category_new.php", $iPort["new"]);
				echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
				echo "<tr><td align=\"right\" valign=\"bottom\">";
				echo $lpf->GET_TABLE_ACTION_BTN($TableActionArr);
				echo "</td></tr></table>";
				
				?>
			</td></tr></table>
		</td>
	</tr>
	<tr>
	  <td valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		
	    <tr>
	       <td align="center" colspan=2>
					<?= $li->displayPlain() ?>
				<?
				if ($li->navigationHTML!="")
				{
					
					echo "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
					echo "<tr class='tablebottom'>";
					echo "<td class=\"tabletext\" align=\"right\">";
					echo $li->navigation(1);
					echo "</td>";
					echo "</tr>";
					echo "</table>";
				}
				?>
		</td>
	</tr>
	</table>
	</td>
	</tr>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=numPerPage value="<?=$numPerPage?>">
<input type=hidden name=page_size_change value="<?=$page_size_change?>">

<input type=hidden name=Action>

</FORM>
</table>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
