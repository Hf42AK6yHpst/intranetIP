<?php
## Using By : 
##############################################
##	Modification Log:
##	2010-02-18 Max (201002180941)
##	- Get the tag menu from includes/portfolio25/libpf-tabmenu.php instead of slpsettingcommonfunction.php

##	2010-01-20 Max(201001201615)
##	- Generate the Tag Menu By function call 
##############################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";

$luser = new libuser($UserID);
$lpf = new libportfolio2007();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_OLE);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

// load settings
# word templates
$lf = new libwordtemplates_ipf(4);
$file_array = $lf->file_array;
$data = $lf->getTemplatesContent(0);

// Tab Menu Settings
//*** current page index*****
// 0 -   * Components of OLE
// 1 -   * Programme Category
// 2 -   * Management Group
// 3 -   * Preset Participation Role
// 4 -   * Submission Period
// 5 -   * Record Approval
$currentPageIndex = 3;
$TabMenuArr = libpf_tabmenu::getSlpSettingsTags(IPF_CFG_SLP_SETTING_OLE, $currentPageIndex);

?>

<SCRIPT LANGUAGE="Javascript">
isToolTip = true;
isMenu = true;

</SCRIPT>


<form name="form1" action="activity_role_update.php" method="POST">
	<table width="100%"  border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
					<?
					echo $lpf->GET_TAB_MENU($TabMenuArr);
					echo $linterface->GET_SYS_MSG($msg);
					?>
				</td></tr></table>
			</td>
		</tr>
	</table>
	<table width="50%" border="0" cellspacing="0" cellpadding="4" align="center">
		<tr><td valign="top"><?= $ec_iPortfolio['activity_role_template_instruction'] ?></td></tr>
		<tr>
			<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr><td>
					<textarea class="textboxtext" name="data" ROWS="20"><?= $data?></textarea>
				</td></tr>
				</table>
			</td>
		</tr>
	</table>
	<table width="95%" border="0" cellspacing="0" cellpadding="1" align="center">
		<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr><td align="center" colspan="6">
		<div style="padding-top: 5px">
		<?= $linterface->GET_ACTION_BTN($button_submit, "button","javascript:form1.submit()")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
		</div>
		</td></tr>
	</table>
</form>



<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>

