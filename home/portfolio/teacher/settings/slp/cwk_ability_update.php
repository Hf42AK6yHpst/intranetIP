<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lpf = new libportfolio();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$AbilityCode = htmlspecialchars($AbilityCode);
$AbilityName = htmlspecialchars($AbilityName);

if(empty($AbilityID))
{
  $sql = "INSERT INTO {$eclass_db}.CWK_COMMON_ABILITY ";
  $sql .= "(AbilityCode, AbilityName, DateInput, InputBy, ModifyBy) ";
  $sql .= "VALUES ('{$AbilityCode}', '{$AbilityName}', NOW(), {$UserID}, {$UserID}) ";
  $li->db_db_query($sql);
  $msg = "add";
}
else
{
  $sql = "UPDATE {$eclass_db}.CWK_COMMON_ABILITY SET ";
  $sql .= "AbilityCode = '{$AbilityCode}', ";
  $sql .= "AbilityName = '{$AbilityName}', ";
  $sql .= "ModifyBy = {$UserID} ";
  $sql .= "WHERE AbilityID = {$AbilityID}";
  $li->db_db_query($sql);
  $msg = "update";
}

intranet_closedb();
header("Location: cwk_common_ability.php?msg=$msg");
?>