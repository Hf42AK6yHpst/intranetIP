<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
intranet_auth();
intranet_opendb();

function updateProfileClassHistory_Year($dbObj, $table_name, $table_field, $t_old_year, $t_new_year, &$log_text_arr){
  global $intranet_db;

  $sql = "SELECT {$table_field} FROM {$intranet_db}.{$table_name} WHERE AcademicYear = '".$t_old_year."'";
  $recordID_arr = $dbObj->returnVector($sql);
  if(count($recordID_arr) > 0)
  {
    $sql = "UPDATE {$intranet_db}.{$table_name} SET AcademicYear = '".$t_new_year."' WHERE {$table_field} IN (".implode(",", $recordID_arr).")";
    $logArr = execQuery($dbObj, $sql);
    array_push($logArr, $t_old_year);
    $log_text_arr[] = prepareLogText($logArr);
  }
}

function updateProfileTable_Year($dbObj, $table_name, $table_field, $t_old_year, $t_new_year, &$log_text_arr){
  global $intranet_db;

  $sql = "SELECT {$table_field} FROM {$intranet_db}.{$table_name} WHERE Year = '".$t_old_year."'";
  $recordID_arr = $dbObj->returnVector($sql);
  if(count($recordID_arr) > 0)
  {
    $sql = "UPDATE {$intranet_db}.{$table_name} SET Year = '".$t_new_year."' WHERE {$table_field} IN (".implode(",", $recordID_arr).")";
    $logArr = execQuery($dbObj, $sql);
    array_push($logArr, $t_old_year);
    $log_text_arr[] = prepareLogText($logArr);
  }
}

function updateIPFTable_Year($dbObj, $table_name, $t_old_year, $t_ay_id, $t_new_year, &$log_text_arr){
  global $eclass_db;

  $sql = "SELECT RecordID FROM {$eclass_db}.{$table_name} WHERE Year = '".$t_old_year."'";
  $recordID_arr = $dbObj->returnVector($sql);
  if(count($recordID_arr) > 0)
  {
    $sql = "UPDATE {$eclass_db}.{$table_name} SET AcademicYearID = '".$t_ay_id."', Year = '".$t_new_year."' WHERE RecordID IN (".implode(",", $recordID_arr).")";
    $logArr = execQuery($dbObj, $sql);
    array_push($logArr, $t_old_year);
    $log_text_arr[] = prepareLogText($logArr);
  }
}

function updateProfileTable_Semester($dbObj, $table_name, $table_field, $t_year_name, $t_old_sem_name, $t_new_sem_name, &$log_text_arr){
  global $intranet_db;

  $sql = "SELECT {$table_field} FROM {$intranet_db}.{$table_name} WHERE Year = '".$t_year_name."' AND Semester = '".$t_old_sem_name."'";
  $recordID_arr = $dbObj->returnVector($sql);
  if(count($recordID_arr) > 0)
  {
    $sql = "UPDATE {$intranet_db}.{$table_name} SET Semester = '".$t_new_sem_name."' WHERE {$table_field} IN (".implode(",", $recordID_arr).")";
    $logArr = execQuery($dbObj, $sql);
    array_push($logArr, $t_old_sem_name);
    $log_text_arr[] = prepareLogText($logArr);
  }
}

function updateIPFTable_Semester($dbObj, $table_name, $t_year_name, $t_old_sem_name, $t_yt_id, $t_new_sem_name, &$log_text_arr){
  global $eclass_db;

  $sql = "SELECT RecordID FROM {$eclass_db}.{$table_name} WHERE Year = '".$t_year_name."' AND Semester = '".$t_old_sem_name."'";
  $recordID_arr = $dbObj->returnVector($sql);
  if(count($recordID_arr) > 0)
  {
    $sql = "UPDATE {$eclass_db}.{$table_name} SET YearTermID = '".$t_yt_id."', Semester = '".$t_new_sem_name."' WHERE RecordID IN (".implode(",", $recordID_arr).")";
    $logArr = execQuery($dbObj, $sql);
    array_push($logArr, $t_old_sem_name);
    $log_text_arr[] = prepareLogText($logArr);
  }
}

function execQuery($dbObj, $sql){
  $dbObj->db_db_query($sql);

  $affected_row = mysql_affected_rows();
  $error = mysql_error();
  
  $logArr = array(
              $sql,
              $affected_row,
              $error
            );
            
  return $logArr;
}

function prepareLogText($logArr){
  if(empty($logArr)) return "";

  list($sql, $affected_row, $error, $orig_year) = $logArr;

  # prepare log text
  $log_text = "Query run: ".$sql."\n";
  $log_text .= "Number of affected rows: ".$affected_row."\n";
  $log_text .= "Error: ".$error."\n";
  $log_text .= "Original Year: ".$orig_year."\n\n";
  
  return $log_text;
}

function writeLog($file_handler, $logArr){
  if(empty($logArr)) return;

  for($i=0; $i<count($logArr); $i++)
  {
    $log_text .= $logArr[$i];
  }
  
  fwrite($file_handler, $log_text);
}

$li = new libdb();
$lay = new academic_year();
$lyt = new academic_year_term();

# for log
$log_location = $intranet_root."/file/iportfolio/log";
if(!file_exists($log_location))
{
  mkdir($log_location, 0777, true);
}
$fp = fopen($log_location."/sr_year_".date("Y-m-d").".log", "a");

# Start Time Log
$log_text = "///////////////////////////////////\n";
$log_text .= "/////// Start Time:         ///////\n";
$log_text .= "/////// ".date("Y-m-d H:i:s")." ///////\n";
$log_text .= "///////////////////////////////////\n";
$log_text_arr[] = $log_text;

for($i=0; $i<count($module); $i++)
{
  $module_title = $module[$i];
  
  $t_old_year = addslashes($old_year[$i]);
  $t_ay_id = $ay_id[$i];
  
  # To prevent saving empty data if year is not set
  if($t_ay_id == "") continue;
  $lay->Get_Academic_Year_Info($t_ay_id);
  $t_year_name = $lay->YearNameEN;
  
  switch($module_title)
  {
    case "ClassHistory":
      updateProfileClassHistory_Year($li, "PROFILE_CLASS_HISTORY", "RecordID", $t_old_year, $t_year_name, &$log_text_arr);
      break;
    case "activity":
      # Year
      updateProfileTable_Year($li, "PROFILE_STUDENT_ACTIVITY", "StudentActivityID", $t_old_year, $t_year_name, &$log_text_arr);
      updateIPFTable_Year($li, "ACTIVITY_STUDENT", $t_old_year, $t_ay_id, $t_year_name, &$log_text_arr);
      
      # Semester
      if(is_array($old_semester[$module_title][$t_old_year]) && is_array($YearTermID[$module_title][$t_old_year]))
      {
        for($j=0; $j<count($old_semester[$module_title][$t_old_year]); $j++)
        {
          $t_old_sem_name = $old_semester[$module_title][$t_old_year][$j];
          $t_yt_id = $YearTermID[$module_title][$t_old_year][$j];
          
          if($t_yt_id == "") continue;
          $t_new_sem_name = $lyt->Get_YearNameByID($t_yt_id);
          
          updateProfileTable_Semester($li, "PROFILE_STUDENT_ACTIVITY", "StudentActivityID", $t_year_name, $t_old_sem_name, $t_new_sem_name, &$log_text_arr);
          updateIPFTable_Semester($li, "ACTIVITY_STUDENT", $t_year_name, $t_old_sem_name, $t_yt_id, $t_new_sem_name, &$log_text_arr);
        }
      }
      break;
    case "assessment_report":
      # Year
      updateIPFTable_Year($li, "ASSESSMENT_STUDENT_SUBJECT_RECORD", $t_old_year, $t_ay_id, $t_year_name, &$log_text_arr);
      updateIPFTable_Year($li, "ASSESSMENT_STUDENT_MAIN_RECORD", $t_old_year, $t_ay_id, $t_year_name, &$log_text_arr);
      
      # Semester
      if(is_array($old_semester[$module_title][$t_old_year]) && is_array($YearTermID[$module_title][$t_old_year]))
      {
        for($j=0; $j<count($old_semester[$module_title][$t_old_year]); $j++)
        {
          $t_old_sem_name = $old_semester[$module_title][$t_old_year][$j];
          $t_yt_id = $YearTermID[$module_title][$t_old_year][$j];
          
          if($t_yt_id == "") continue;
          $t_new_sem_name = $lyt->Get_YearNameByID($t_yt_id);
          
          updateIPFTable_Semester($li, "ASSESSMENT_STUDENT_SUBJECT_RECORD", $t_year_name, $t_old_sem_name, $t_yt_id, $t_new_sem_name, &$log_text_arr);
          updateIPFTable_Semester($li, "ASSESSMENT_STUDENT_MAIN_RECORD", $t_year_name, $t_old_sem_name, $t_yt_id, $t_new_sem_name, &$log_text_arr);
        }
      }
      break;
    case "attendance":
      updateIPFTable_Year($li, "ATTENDANCE_STUDENT", $t_old_year, $t_ay_id, $t_year_name, &$log_text_arr);
      
      # Semester
      if(is_array($old_semester[$module_title][$t_old_year]) && is_array($YearTermID[$module_title][$t_old_year]))
      {
        for($j=0; $j<count($old_semester[$module_title][$t_old_year]); $j++)
        {
          $t_old_sem_name = $old_semester[$module_title][$t_old_year][$j];
          $t_yt_id = $YearTermID[$module_title][$t_old_year][$j];
          
          if($t_yt_id == "") continue;
          $t_new_sem_name = $lyt->Get_YearNameByID($t_yt_id);
          
          updateIPFTable_Semester($li, "ATTENDANCE_STUDENT", $t_year_name, $t_old_sem_name, $t_yt_id, $t_new_sem_name, &$log_text_arr);
        }
      }
      break;
    case "award":
      updateProfileTable_Year($li, "PROFILE_STUDENT_AWARD", "StudentAwardID", $t_old_year, $t_year_name, &$log_text_arr);
      updateIPFTable_Year($li, "AWARD_STUDENT", $t_old_year, $t_ay_id, $t_year_name, &$log_text_arr);
      
      # Semester
      if(is_array($old_semester[$module_title][$t_old_year]) && is_array($YearTermID[$module_title][$t_old_year]))
      {
        for($j=0; $j<count($old_semester[$module_title][$t_old_year]); $j++)
        {
          $t_old_sem_name = $old_semester[$module_title][$t_old_year][$j];
          $t_yt_id = $YearTermID[$module_title][$t_old_year][$j];
          
          if($t_yt_id == "") continue;
          $t_new_sem_name = $lyt->Get_YearNameByID($t_yt_id);
          
          updateProfileTable_Semester($li, "PROFILE_STUDENT_AWARD", "StudentAwardID", $t_year_name, $t_old_sem_name, $t_new_sem_name, &$log_text_arr);
          updateIPFTable_Semester($li, "AWARD_STUDENT", $t_year_name, $t_old_sem_name, $t_yt_id, $t_new_sem_name, &$log_text_arr);
        }
      }
      break;
    case "teacher_comment":
      updateIPFTable_Year($li, "CONDUCT_STUDENT", $t_old_year, $t_ay_id, $t_year_name, &$log_text_arr);
      
      # Semester
      if(is_array($old_semester[$module_title][$t_old_year]) && is_array($YearTermID[$module_title][$t_old_year]))
      {
        for($j=0; $j<count($old_semester[$module_title][$t_old_year]); $j++)
        {
          $t_old_sem_name = $old_semester[$module_title][$t_old_year][$j];
          $t_yt_id = $YearTermID[$module_title][$t_old_year][$j];
          
          if($t_yt_id == "") continue;
          $t_new_sem_name = $lyt->Get_YearNameByID($t_yt_id);
          
          updateIPFTable_Semester($li, "CONDUCT_STUDENT", $t_year_name, $t_old_sem_name, $t_yt_id, $t_new_sem_name, &$log_text_arr);
        }
      }
      break;
    case "merit":
      updateProfileTable_Year($li, "PROFILE_STUDENT_MERIT", "StudentMeritID", $t_old_year, $t_year_name, &$log_text_arr);
      updateIPFTable_Year($li, "MERIT_STUDENT", $t_old_year, $t_ay_id, $t_year_name, &$log_text_arr);
      
      # Semester
      if(is_array($old_semester[$module_title][$t_old_year]) && is_array($YearTermID[$module_title][$t_old_year]))
      {
        for($j=0; $j<count($old_semester[$module_title][$t_old_year]); $j++)
        {
          $t_old_sem_name = $old_semester[$module_title][$t_old_year][$j];
          $t_yt_id = $YearTermID[$module_title][$t_old_year][$j];
          
          if($t_yt_id == "") continue;
          $t_new_sem_name = $lyt->Get_YearNameByID($t_yt_id);
          
          updateProfileTable_Semester($li, "PROFILE_STUDENT_MERIT", "StudentMeritID", $t_year_name, $t_old_sem_name, $t_new_sem_name, &$log_text_arr);
          updateIPFTable_Semester($li, "MERIT_STUDENT", $t_year_name, $t_old_sem_name, $t_yt_id, $t_new_sem_name, &$log_text_arr);
        }
      }
      break;
    case "service":
      updateProfileTable_Year($li, "PROFILE_STUDENT_SERVICE", "StudentServiceID", $t_old_year, $t_year_name, &$log_text_arr);
      updateIPFTable_Year($li, "SERVICE_STUDENT", $t_old_year, $t_ay_id, $t_year_name, &$log_text_arr);
      
      # Semester
      if(is_array($old_semester[$module_title][$t_old_year]) && is_array($YearTermID[$module_title][$t_old_year]))
      {
        for($j=0; $j<count($old_semester[$module_title][$t_old_year]); $j++)
        {
          $t_old_sem_name = $old_semester[$module_title][$t_old_year][$j];
          $t_yt_id = $YearTermID[$module_title][$t_old_year][$j];
          
          if($t_yt_id == "") continue;
          $t_new_sem_name = $lyt->Get_YearNameByID($t_yt_id);
          
          updateProfileTable_Semester($li, "PROFILE_STUDENT_SERVICE", "StudentServiceID", $t_year_name, $t_old_sem_name, $t_new_sem_name, &$log_text_arr);
          updateIPFTable_Semester($li, "SERVICE_STUDENT", $t_year_name, $t_old_sem_name, $t_yt_id, $t_new_sem_name, &$log_text_arr);
        }
      }
      break;
  }
}

# End Time Log
$log_text = "///////////////////////////////////\n";
$log_text .= "/////// End Time:           ///////\n";
$log_text .= "/////// ".date("Y-m-d H:i:s")." ///////\n";
$log_text .= "///////////////////////////////////\n\n";
$log_text_arr[] = $log_text;

writeLog($fp, $log_text_arr);
fclose($fp);

intranet_closedb();

header("Location: sr_year_list.php?msg=update");
?>