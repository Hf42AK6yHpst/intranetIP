<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";
$CurrentPageName = $iPort['menu']['ole']." ".$iPort['menu']['settings'];

$luser = new libuser($UserID);
$lpf = new libpf_slp();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");


// load settings
# word templates
$lf = new libwordtemplates_ipf(3);
$file_array = $lf->file_array;
if (!isset($temp_type))
{
	$temp_type = 1;
}
$data = $lf->getTemplatesContent($temp_type);						

				
# get ELE Array
$ELEArray = $lpf->GET_ELE();

if (!empty($data))
{
	$DataArray = explode("\n", $data);
	for($i=0; $i<sizeof($DataArray); $i++)
	{
		$p_title = trim($DataArray[$i]);
		if(!empty($p_title))
		{
			$TempArray = explode("\t", $p_title);
			$PresetTitle = $TempArray[0];
			$PresetELEList = $TempArray[1];
			if(!empty($PresetELEList))
			{
				$ele_array = explode(",", $PresetELEList);
				$ELEDisplay = "";
				for($k=0; $k<sizeof($ele_array); $k++)
				{
					$ele_id = trim($ele_array[$k]);
					$ele_title = trim($ELEArray[$ele_id]);
					if ($ele_title != "" || $ele_title != NULL)
					{
						$ELEDisplay .= $ele_title."<br />";
					}
				}
			}
			else
				$ELEDisplay = "--";
				
			$displayInfoArr[$i][0] = $PresetTitle;
			$displayInfoArr[$i][1] = $ELEDisplay;
		}
	}
	
}
else
{
	$TitleTable .= "<tr><td colspan='3' class='chi_content_12' align='center'>".$no_record_msg."</td></tr>";
}
$TitleTable .= "</table>";
						

############  Cotnent Here ######################
// page number settings
if ($order=="") $order=1;
if ($field=="") $field=0;

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if ($pageNo == "")
$pageNo = 1;

$li = new libdbtable2007($field, $order, $pageNo);
$li->no_col = 4;
$li->table_tag = "<table bgcolor='#cccccc' border='0' cellpadding='4' cellspacing='1' width='100%'>";
$li->row_alt = array("#FFFFFF", "#F3F3F3");
$li->row_height = 25;
$li->plain_column = 1;
$li->plain_column_css = "tablelink";
$li->title = $iPort['table']['records'];
$li->total_row = sizeof($displayInfoArr);
$li->n_start = 1;

// TABLE COLUMN
$li->column_list .= "<td width='2%' class='tabletop tabletopnolink' height='25' align='center'>#</span></td>\n";
$li->column_list .= "<td width='48%' class='tabletop tabletopnolink' nowrap='nowrap'>".$ec_iPortfolio['title']."</td>\n";
$li->column_list .= "<td width='48%' class='tabletop tabletopnolink' nowrap='nowrap'>".$iPort["default_ele"]."</td>\n";
$li->column_list .= "<td align='center' nowrap='nowrap'>".$li->check("RecordID[]")."</td>\n";

// Tab Menu Settings
$TabMenuArr = array();
$TabMenuArr[] = array("ele.php", $iPort["ele"], 0);
$TabMenuArr[] = array("category.php", $iPort["category"], 0);
$TabMenuArr[] = array("tag.php", $iPort["tag"], 0);
$TabMenuArr[] = array("activity_title_template.php", $iPort["activity_title_template"], 1);
$TabMenuArr[] = array("activity_role_template.php", $iPort["activity_role_template"], 0);
$TabMenuArr[] = array("student_submission_period.php", $iPort["student_submission_period"], 0);
$TabMenuArr[] = array("record_approval.php", $iPort["record_approval"], 0);

// Table Action Button (JS, Image, Image_name, title)
$TableActionArr = array();
$TableActionArr[] = array("javascript:checkEdit(document.form1,'RecordID[]','activity_title_new.php')", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_edit.gif", "imgEdit", $ec_iPortfolio['edit'], "tool_edit");
$TableActionArr[] = array("javascript:checkRemove(document.form1,'RecordID[]','activity_title_remove.php')", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_delete.gif", "imgDelete", $ec_iPortfolio['delete'], "tool_delete");

##################################################

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
isToolTip = true;
isMenu = true;

</SCRIPT>

<FORM name="form1" method="GET">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<!-- Content Here -->
<?
echo "<form name=\"form1\" id=\"form1\" action=\"activity_title_template.php\" method=\"post\">";
echo $lpf->GET_TAB_MENU($TabMenuArr);
echo $linterface->GET_SYS_MSG($msg);
echo $lpf->GET_NEW_BUTTON("activity_title_new.php?temp_type=$temp_type", $iPort["new"]);

echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
echo "<tr><td align=\"left\" valign=\"bottom\">";
echo returnSelection($file_array , $temp_type, "temp_type", "onChange=\"self.location='activity_title_template.php?temp_type='+this.value\"");
echo "</td><td align=\"right\" valign=\"bottom\">";
echo $lpf->GET_TABLE_ACTION_BTN($TableActionArr);
echo "</td></tr></table>";

?>

<?
//display $displayInfoArr in a table
	$row_valign = ($li->row_valign!="") ? "valign='".$li->row_valign."'" : "";
	$li->n_start=($li->pageNo-1)*$li->page_size;
	
	$total_col = $li->no_col;
	$bg_style1 = ($li->row_alt[0]!="") ? "bgcolor='".$li->row_alt[0]."' class='tabletext'" : "";
	$bg_style2 = ($li->row_alt[1]!="") ? "bgcolor='".$li->row_alt[1]."' class='tabletext'" : "";

	$x = ($li->table_tag!="") ? $li->table_tag : "<table width='100%' border='0' cellpadding='4' cellspacing='1'>\n";
	$x .= $li->displayPlainColumn(1);

	if (sizeof($displayInfoArr)==0)
	{
		$x.="<tr><td $bg_style1 align=center colspan=".$li->no_col." height='80'>".$li->no_msg."</td></tr>\n";
	} else
	{
		foreach ($displayInfoArr as $key => $row) {
		    $InfoArr[$key]  = $row[$field];
		}
//		array_multisort($InfoArr, SORT_STRING, $displayInfoArr);
//		$displayInfoArr = ($order==0) ? array_reverse($displayInfoArr) : $displayInfoArr;

		for ($i=$li->n_start; $i<sizeof($displayInfoArr) ; $i++)
		{
			
			if($i+1>$li->n_start+$li->page_size) break;
			$x.="<tr>\n";
			$bg_style_row = ($i%2) ? $bg_style1 : $bg_style2;
			$x.= ($li->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($i+1)."&nbsp;</td>\n" ;
			for($j=0; $j<$total_col-2; $j++){
				$t = $displayInfoArr[$i][$j];
				$x .= $li->displayPlainCell($j, $t, $bg_style_row, 1);
			}
			$x .= $li->displayCell($total_col,"<input align='center' type='checkbox' name='RecordID[]' value='".$i."'>", $li->returnCellClassID().$css,"$bg_style_row align='center' valign='center'");
			$x .= "</tr>\n";
		}
		
	}
	$x .= "</table>\n";
	if (sizeof($displayInfoArr)>0)
	{
		$li->navigationHTML = $li->navigation(1);
	}

	echo $x;
?>


<?

if ($li->navigationHTML!="")
{
	echo "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
	echo "<tr class='tablebottom'>";
	echo "<td class=\"tabletext\" align=\"right\">";
	echo $li->navigation(1);
	echo "</td>";
	echo "</tr>";
	echo "</table>";
}

echo "<input type=\"hidden\" name=\"order\" value=\"".$order."\" />";
echo "<input type=\"hidden\" name=\"pageNo\" value=\"".$li->pageNo."\" />";
echo "<input type=\"hidden\" name=\"field\" value=\"".$field."\" />";
echo "<input type=\"hidden\" name=\"page_size_change\" value=\"".$page_size_change."\" />";
echo "<input type=\"hidden\" name=\"numPerPage\" value=\"".$numPerPage."\" />";
echo "<input type=\"hidden\" name=\"tempType\" value=\"".$tempType."\" />";
echo "</form>";
?>
<!-- ENd Content Here -->
</td></tr></table>
</td></tr></table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>

