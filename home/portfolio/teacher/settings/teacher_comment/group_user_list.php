<?php
//modifying by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_TeacherComment";
$CurrentPageName = $Lang['iPortfolio']['TeacherComment']." ".$iPort['menu']['settings'];

$lpf = new libportfolio2007();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

// Tab Menu Settings
//*** current page index*****
// 0 -   * Components of OLE
// 1 -   * Group List
$currentPageIndex = 1;
$TabMenuArr = libpf_tabmenu::getTcSettingsTags($currentPageIndex);


$ImportBtn 	= "<a href=\"javascript:newWindow('group_student_assign.php?group_id=".$group_id."',2)\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_import.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_import . "</a>";
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'user_id[]','group_user_remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";

$name_field = getNameFieldForRecord2eClassOnly("um.");

$sql = "
  SELECT 
    ".$name_field." as user_name, 
    um.class_number,
    um.memberType, 
    if(um.memberType = 'S', CONCAT('<input type=\"checkbox\" name=\"user_id[]\" value=\"', um.user_id ,'\">'), '&nbsp;'),
    if(um.class_number is null or um.class_number = '','',concat(left(um.class_number,LOCATE(' - ',um.class_number)), ' - ' , LPAD(TRIM(SUBSTRING(um.class_number, LOCATE(' - ',um.class_number) + 3)), 3, '0'))) as class_order
  FROM
    user_group ug
  INNER JOIN usermaster um
    ON ug.user_id = um.user_id
  WHERE 
    ug.group_id = $group_id
";

$pageSizeChangeEnabled = true;

# TABLE INFO
if($field == "") $field = 1;
if($order == "") $order = 1;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("user_name", "class_order", "memberType");
$li->db = $lpf->course_db;
$li->sql = $sql;
$li->page_size = ($numPerPage=="") ? $page_size : $numPerPage;
$li->no_col = 5;
$li->IsColOff = "IP25_table";

// TABLE COLUMN
$li->column_list .= "<th width='1' class='tabletop tabletopnolink'>&nbsp;#&nbsp;</th>\n";
$li->column_list .= "<th width='60%' class='tabletop'>".$li->column(0, $i_UserName)."</th>\n";
$li->column_list .= "<th width='30%' class='tabletop'>".$li->column(1, "$i_UserClassName ($i_UserClassNumber)")."</th>\n";
$li->column_list .= "<th width='10%' class='tabletop'>".$li->column(2, $i_eClass_Identity)."</th>\n";
$li->column_list .= "<th width='1' class='tabletop'>".$li->check("user_id[]")."</th>\n";

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

$sql = "SELECT group_name FROM {$lpf->course_db}.grouping WHERE group_id = {$group_id}";
$group_name = current($lpf->returnVector($sql));
$PAGE_NAVIGATION = $group_name;

?>

<br />
<form name="form1" method="get" action="group_user_list.php">
  <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td><?=$lpf->GET_TAB_MENU($TabMenuArr)?></td>
    </tr>
    <tr>
      <td><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION) ?></td>
    </tr>
    <tr>
      <td align="center">
        <table width="96%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" class="tabletext" align="center">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table border="0" cellspacing="0" cellpadding="2">
                      <tr>
                        <td><p><?=$ImportBtn?></p></td>
                      </tr>
                    </table>
                  </td>
                  <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
                </tr>
                <tr>
                  <td align="left" valign="bottom">&nbsp;</td>
                  <td align="right" valign="bottom" height="28">
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
                        <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
                          <table border="0" cellspacing="0" cellpadding="2">
                            <tr>
                              <td nowrap><?=$delBtn?></td>
                            </tr>
                          </table>
                        </td>
                        <td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="2" align="center">
                    <?=$li->display();?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>        
        <table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
          <tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
          </tr>
          <tr>
            <td align="center">
              <?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:self.location ='group_list.php';","cancelbtn") ?>
            </td>
          </tr>
        </table>
      </td>
    </tr>        
  </table>
  <br />
  
  <input type="hidden" name="group_id" value="<?=$group_id?>" />
  <input type="hidden" name="pageNo" value="<?=$li->pageNo?>" />
  <input type="hidden" name="order" value="<?=$li->order?>">
  <input type="hidden" name="field" value="<?=$li->field?>" />
  <input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
  <input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
