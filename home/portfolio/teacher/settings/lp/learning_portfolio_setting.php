<?php

// Modifing by 
/**
 * 2020-10-22 Crystal [ip.2.5.11.11.1]
 *       - hide all facebook like function
 * */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:PeerReview") || !strstr($ck_user_rights, ":web:"));
//auth("T");
$lpf = new libportfolio2007();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

// load settings
$lp_config_file = "$eclass_root/files/lp_config.txt";
$li_fs = new libfilesystem();
$config_array = unserialize($li_fs->file_read($lp_config_file));

$reset_btn_check = ($config_array["disable_reset"] == 1) ? "CHECKED" : "";
$facebook_btn_check = ($config_array["disable_facebook"] == 1) ? "CHECKED" : "";

$currentPageIndex = 1;
$TabMenuArr = libpf_tabmenu::getLpSettingsTags($currentPageIndex);

# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Settings_LearningPortfolio";
$CurrentPageName = $iPort['menu']['learning_portfolio'];
### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

?>


<script language="javascript">

function jSubmit()
{
	var obj = document.form1;
	obj.submit();
}

</script>

<form name="form1" id="form1" method="post" action="learning_portfolio_setting_update.php">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr><td align="center">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr><td>
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
          <tr>
            <td align="right"><?=$linterface->GET_SYS_MSG($msg) ?></td>
          </tr>
          <tr>
            <td class="tab_underline">
              <?=$lpf->GET_TAB_MENU($TabMenuArr) ?>
            </td>
          </tr>
          <tr>
            <td align="center">
              <table width="90%" border="0" cellspacing="0" cellpadding="4">
                <tr>
                	<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" width="15%" valign="top" nowrap="yes"><?=$Lang['iPortfolio']['resetButton']?></td>
                	<td>
                    <span class="tabletext"><input type="checkbox" name="disable_reset" id="disable_reset" <?=$reset_btn_check?> />&nbsp;<label for="disable_reset"><?=$Lang['Btn']['Disable']?></label></span>
                	</td>
                </tr>
            	</table>
              <table width="90%" border="0" cellspacing="0" cellpadding="4">
                <tr>
                  <td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
                </tr>
                <tr>
                  <td align="center">
                    <input name="submit_btn" class="formbutton" value="<?=$iPort['btn']['submit']?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" type="Button" onClick="jSubmit();">
                    <input name="reset_btn" class="formbutton" value="<?=$iPort['btn']['reset']?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" type="Reset">
                  </td>
                </tr>
              </table>
          	</td>
          </tr>
        </table>
      </td></tr>
    </table>
    <br />
  </td></tr>
</table>
</form>
 
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>