<?php
## Using By :  

/******************************************************************************
 * Modification log:
 * 2015-01-05 (Bill)
 * - Create file
 *****************************************************************************/

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/ngwah/libpf-slp-ngwah.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Access Right Check
$lpf = new libportfolio2007();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

# Template for page
$linterface = new interface_html();

# Page Title
$CurrentPage = "Settings_SAS";

# Title
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

# Tag
$TAGS_OBJ[] = array($Lang['iPortfolio']['SAS_Settings']['Category_Credit_Point'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/SAS/category_credit_point.php", 0);
$TAGS_OBJ[] = array($Lang['iPortfolio']['SAS_Settings']['Award'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/SAS/award.php", 1);
$TAGS_OBJ[] = array($Lang['iPortfolio']['SAS_Settings']['Release_Student'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/SAS/release_students.php", 0);

# Get YearID
$yearID = ($yearID == '')? Get_Current_Academic_Year_ID() : $yearID;

# Get SAS Settings
$SAS = new studentInfo('', $yearID);
$data_Ary = $SAS->getSASSettings('award');
$data_Ary = $data_Ary['award'];

$SAS_settings = array();
if(count($data_Ary) == 0)
{
	$SAS_settings = $SAS->getDefaultSASSettings('award');
}
else 
{
	$settings = explode(';;', $data_Ary['SettingValue']);
		
	for($i = 0; $i<count($settings); $i++){
		$stored_value = explode('###', $settings[$i]);
		$SAS_settings[] = array($stored_value[0], $stored_value[1], $stored_value[2], $stored_value[3]);
	}
}

$content = '';
for($i=0; $i<count($SAS_settings); $i++){
	$content .= "<tr>";
	$content .= "<td><input name=\"award[]\" value=\"".$SAS_settings[$i][0]."\"</td>";
	$content .= "<td><input name=\"award_en[]\" value=\"".$SAS_settings[$i][1]."\"</td>";
	$content .= "<td><input name=\"award_spt[]\" value=\"".$SAS_settings[$i][2]."\"</td>";
	$content .= "<td><input name=\"award_ept[]\" value=\"".$SAS_settings[$i][3]."\"</td>";
	$content .= "</tr>";
}

$linterface->LAYOUT_START();

?>

<SCRIPT LANGUAGE="Javascript">

function checkform(){
	var scoreArray = new Array();
	
	var awards = document.getElementsByName('award[]');
	var awardens = document.getElementsByName('award_en[]');
	var awardstarts = document.getElementsByName('award_spt[]');
	var awardends = document.getElementsByName('award_ept[]');
	
	var submit = false;
	$("#btnSubmit").attr("disabled", true);
	
	if (awards.length > 0){
		submit = true;
		var previous = 1000;
		
		for(var i = 0; i < awards.length; i++){
			// Chinese Name empty
			if(awards[i].value.trim() == '')
			{
				alert('<?=$Lang['General']['JS_warning']['InputName']?>');
				awards[i].focus();
				submit = false;
				break;
			}
			// English Name empty
			else if(awardens[i].value.trim() == '')
			{
				alert('<?=$Lang['General']['JS_warning']['InputName']?>');
				awardens[i].focus();
				submit = false;
				break;
			}
			// Credit Points empty
			else if(awardstarts[i].value.trim() == '' && awardends[i].value.trim() == '')
			{
				alert('<?=$Lang['General']['JS_warning']['InputPositiveInteger']?>');
				awardstarts[i].focus();
				submit = false;
				break;
			}
			// Maximum Credit Point not positive integer
			else if(awardends[i].value.trim() != '' && (awardends[i].value < 0 || !(/^\+?(0|[1-9]\d*)$/.test(awardends[i].value))))
			{
				alert('<?=$Lang['General']['JS_warning']['InputPositiveInteger']?>');
				awardends[i].focus();
				submit = false;
				break;
			}
//			// Maximum Credit Point larger than previous Credit Point
//			else if(awardends[i].value.trim() != '' && awardends[i].value > previous)
//			{
//				alert('<?=$Lang['iPortfolio']['SAS_Settings']['JS_CreditOutOfRange']?>');
//				awardends[i].focus();
//				submit = false;
//				break;
//			}
			// Minimum Credit Point not positive integer
			else if(awardstarts[i].value.trim() != '' && (awardstarts[i].value < 0 || !(/^\+?(0|[1-9]\d*)$/.test(awardstarts[i].value))))
			{
				alert('<?=$Lang['General']['JS_warning']['InputPositiveInteger']?>');
				awardstarts[i].focus();
				submit = false;
				break;
			}
//			// Minimum Credit Point larger than previous Credit Point
//			else if(awardstarts[i].value.trim() != '' && awardstarts[i].value > previous)
//			{	
//				alert('<?=$Lang['iPortfolio']['SAS_Settings']['JS_CreditOutOfRange']?>');
//				awardstarts[i].focus();
//				submit = false;
//				break;
//			}
			// Maximum Credit Point larger than Minimum Credit Point
			else if(awardstarts[i].value.trim() != '' && awardends[i].value.trim() != '' && awardstarts[i].value > awardends[i].value)
			{
				alert('<?=$Lang['iPortfolio']['SAS_Settings']['JS_MinCreditLargerThanMaxCredit']?>');
				awardstarts[i].focus();
				submit = false;
				break;
			}
			
//			 Set Credit Point for next Checking
//			if(awardstarts[i].value.trim() != '')
//			{
//				previous = awardstarts[i].value;
//			} 
//			else 
//			{
//				previous = awardends[i].value;
//			}

			// Store Credit Point for Sorting
			scoreArray.push([i, awardstarts[i].value.trim(), awardends[i].value.trim()]);
		}
		
		// Credit Point Range Checking
		// No Error & More than 1 Awards
		if(submit && scoreArray.length > 1){
			
			// Sorting
			scoreArray.sort(function(a, b) {
			    return a[1] - b[1];
			});
			
			// Checking
			for(var i = 0; i < (scoreArray.length - 1); i++) {
				// Minimum Credit Point
			    if(scoreArray[i][1] != ''){
			    	if(scoreArray[i][1] >= scoreArray[(i+1)][1] || (scoreArray[(i+1)][2] != '' && scoreArray[i][1] >= scoreArray[(i+1)][2])){
				    	alert('<?=$Lang['iPortfolio']['SAS_Settings']['JS_CreditOutOfRange']?>');
				    	submit = false;
						break;
				  	}
				}
				// Maximum Credit Point
				if(scoreArray[i][2] != ''){
				  	if(scoreArray[i][2] >= scoreArray[(i+1)][1] || (scoreArray[(i+1)][2] != '' && scoreArray[i][2] >= scoreArray[(i+1)][2])){
				    	alert('<?=$Lang['iPortfolio']['SAS_Settings']['JS_CreditOutOfRange']?>');
				    	submit = false;
						break;
				   	}
				}
			}
		}
	}

	if(submit){
		document.form1.submit();
	} else {
		$("#btnSubmit").attr("disabled", false);
	}
	
}

</SCRIPT>

<table id="html_body_frame" width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr><td align="center">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td>

<form name="form1" id="form1" action="award_update.php" method="post">

<?=$linterface->GET_NAVIGATION2($Lang['iPortfolio']['SAS_Settings']['Award'])?>

<br>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="left"><?=getSelectAcademicYear("yearID", $tag="onChange='if(this.value > 0) {document.form1.action=\"award.php\"; document.form1.submit();}'", $noFirst=0, $noPastYear=0, $targetYearID=$yearID)?></td>
		<td align="right"><?=$linterface->GET_SYS_MSG($msg)?></td>
	</tr>
</table>

<br>

<!-- Content Here -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="common_table_list_v30 edit_table_list_v30">
	<tr>
		<th width="25%"><?=$Lang['iPortfolio']['SAS_Settings']['Award_Ch']?></th>
		<th width="25%"><?=$Lang['iPortfolio']['SAS_Settings']['Award_En']?></th>
		<th width="25%"><?=$Lang['iPortfolio']['SAS_Settings']['Min_Credit_pt']?></th>
		<th width="25%"><?=$Lang['iPortfolio']['SAS_Settings']['Max_Credit_pt']?></th>
	</tr>
	
	<?=$content?>
</table>

<br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">

<?=$linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "button", "checkform();", "btnSubmit")?>
&nbsp;
<?=$linterface->GET_ACTION_BTN($iPort["btn"]["reset"], "reset", "", "btnReset")?>

</td></tr>
</table>

</form>

<!-- ENd Content Here -->
</td></tr></table>
</td></tr></table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
