<?php
## Using By :  

/******************************************************************************
 * Modification log:
 * 2015-01-05 (Bill)
 * - Create file
 *****************************************************************************/

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/ngwah/libpf-slp-ngwah.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Access Right Check
$lpf = new libportfolio2007();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

# Template for page
$linterface = new interface_html();

# Page Title
$CurrentPage = "Settings_SAS";

# Title
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

# Tag
$TAGS_OBJ[] = array($Lang['iPortfolio']['SAS_Settings']['Category_Credit_Point'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/SAS/category_credit_point.php", 1);
$TAGS_OBJ[] = array($Lang['iPortfolio']['SAS_Settings']['Award'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/SAS/award.php", 0);
$TAGS_OBJ[] = array($Lang['iPortfolio']['SAS_Settings']['Release_Student'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/SAS/release_students.php", 0);

# Get YearID
$yearID = ($yearID == '')? Get_Current_Academic_Year_ID() : $yearID;

# Get SAS Settings
$SAS = new studentInfo('', $yearID);
$data_Ary = $SAS->getSASSettings('category');
$data_Ary = $data_Ary['category'];

$SAS_settings = array();
if(count($data_Ary) == 0)
{
	# Get default settings
	$SAS_settings = $SAS->getDefaultSASSettings('category', $Lang['iPortfolio']['SAS_Settings']);
}
else 
{
	$cat_name = $SAS->getCategoryName();
	$settings = explode(';;', $data_Ary['SettingValue']);
		
	for($i = 0; $i<count($settings); $i++){
		$stored_value = explode('###', $settings[$i]);
		$target_name = $cat_name[($stored_value[0] - 1)];
			
		$SAS_settings[] = array($stored_value[0], $Lang['iPortfolio']['SAS_Settings'][$target_name], $stored_value[1]);
	}
}

# Build Table
$content = '';
for($i=0; $i<count($SAS_settings); $i++){
	$content .= "<tr>";
	$content .= "<td>";
	$content .= $SAS_settings[$i][1];
	$content .= "<input type=\"hidden\" name=\"cat_id[]\" value=\"".$SAS_settings[$i][0]."\">";
	$content .= "</td>";
	$content .= "<td>";
	$content .= "<input name=\"cat_pt[]\" value=\"".$SAS_settings[$i][2]."\">";
	$content .= "</td>";
	$content .= "</tr>";
}

$linterface->LAYOUT_START();

?>

<SCRIPT LANGUAGE="Javascript">
	
function checkform(){
	var cats = document.getElementsByName('cat_pt[]');
	var submit = false;
	$("#btnSubmit").attr("disabled", true);
	
	if (cats.length > 0){
		submit = true;
		for(var i = 0; i < cats.length; i++){
			var cat_pts = cats[i].value;
			
			if(cat_pts.trim() == '' || cat_pts < 0 || !(/^\+?(0|[1-9]\d*)$/.test(cat_pts))){
				cats[i].focus();
				submit = false;
				break;
			}
		}
	}
	
	if(submit){
		document.form1.submit();
	} else {
		alert('<?=$Lang['General']['JS_warning']['InputPositiveInteger']?>');
		$("#btnSubmit").attr("disabled", false);
	}
}

</SCRIPT>

<table id="html_body_frame" width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr><td align="center">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td>

<form name="form1" id="form1" action="category_credit_point_update.php" method="post">

<?=$linterface->GET_NAVIGATION2($Lang['iPortfolio']['SAS_Settings']['Category_Credit_Point'])?>

<br>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="left"><?=getSelectAcademicYear("yearID", $tag="onChange='if(this.value > 0) {document.form1.action=\"category_credit_point.php\"; document.form1.submit();}'", $noFirst=0, $noPastYear=0, $targetYearID=$yearID)?></td>
		<td align="right"><?=$linterface->GET_SYS_MSG($msg)?></td>
	</tr>
</table>

<br>

<!-- Content Here -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="common_table_list_v30 edit_table_list_v30">
	<tr>
		<th width="50%"><?=$Lang['iPortfolio']['SAS_Settings']['Category']?></th>
		<th width="50%"><?=$Lang['iPortfolio']['SAS_Settings']['Max_Credit_pt']?></th>
	</tr>
	
	<?=$content?>
</table>

<br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">

<?=$linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "button", "checkform();", "btnSubmit")?>
&nbsp;
<?=$linterface->GET_ACTION_BTN($iPort["btn"]["reset"], "reset", "", "btnReset")?>

</td></tr>
</table>

</form>

<!-- ENd Content Here -->
</td></tr></table>
</td></tr></table>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
