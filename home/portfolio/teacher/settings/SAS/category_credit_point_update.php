<?php
## Using By :  

/******************************************************************************
 * Modification log:
 * 2015-01-09 (Bill)
 * - Create file
 *****************************************************************************/

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/ngwah/libpf-slp-ngwah.php");

intranet_auth();
intranet_opendb();

# Access Right Check
$lpf = new libportfolio2007();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$values = '';
$count = count($cat_id);
for($i=0; $i<$count; $i++){
	$values .= $cat_id[$i].'###'.intval($cat_pt[$i]);
	if($i < ($count-1)){
		$values .= ';;';
	}
}

$SASsettings = new studentInfo('', $yearID);
$success = $SASsettings->setSASSettings('category', $values);

$msg = ($success == 1)? 'update' : 'add_failed';

intranet_closedb();
header("Location: category_credit_point.php?msg=$msg&yearID=$yearID");

?>