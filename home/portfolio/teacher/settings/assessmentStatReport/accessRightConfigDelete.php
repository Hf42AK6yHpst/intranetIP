<?php


$PATH_WRT_ROOT = '../../../../../';

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$obj = new libportfolio2007();

if (!$obj->IS_ADMIN_USER($UserID) && !$obj->IS_TEACHER())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$redirect = $_REQUEST['redirect'];

$recordIdList = (array)$_REQUEST['RecordIdCheckBox'];
$recordIdList = implode(",", $recordIdList);
$recordIdList = IntegerSafe($recordIdList);


$Msg = $Lang['General']['ReturnMessage']['DeleteSuccess'];
if($recordIdList != ''){
	$sql = "DELETE FROM ASSESSMENT_ACCESS_RIGHT WHERE RecordID IN ($recordIdList)";
	$obj->db_db_query($sql);
	
	$sql = "SELECT * FROM ASSESSMENT_ACCESS_RIGHT WHERE RecordID IN ($recordIdList)";
	$rs = $obj->returnResultSet($sql);
	if(count($rs)){
		$Msg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
	}
}


header("Location: {$redirect}?Msg={$Msg}");

intranet_closedb();

?>