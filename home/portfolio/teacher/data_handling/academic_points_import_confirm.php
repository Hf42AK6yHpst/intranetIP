<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

$lpf = new libpf_slp();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

########################################################
# Tab Menu : Start
########################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_STPAUL_ACADEMIC);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

############  Cotnent Here ######################
$libacademic_year = new academic_year();


# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$limport = new libimporttext();
if ($_FILES["csv_file"]["tmp_name"]!="")
{

	$file_format = array(
			'Student Name',
			'RegNo',
			'Year',
			'Term',
			'Subject Code',
			'Score'
	);

	$flagAry = array(
			'1',
			'1',
			'1',
			'1',
			'1',
			'1'
	);
	$format_wrong = false;
	$data = $limport->GET_IMPORT_TXT($_FILES["csv_file"]["tmp_name"]);

	$counter = 1;

	if(is_array($data))
	{
		$col_name = array_shift($data);
	}

	for($i=0; $i<sizeof($file_format); $i++)
	{
		if (strtoupper($col_name[$i])!=strtoupper($file_format[$i]))
		{
			$format_wrong = true;
			break;
		}
	}


	if ($format_wrong)
	{
		header("location: academic_points_import.php?xmsg=wrong_header");
		exit();
	}
	if(sizeof($data)==0)
	{
		header("location: academic_points_import.php?xmsg=import_no_record");
		exit();
	}
	
	# get all RegNo from DB
	$sql = "select REPLACE(WebSAMSRegNo, '#', '') AS RegNo, UserID As StudentID from ".$eclass_db.".PORTFOLIO_STUDENT order by WebSAMSRegNo";
	$rows = $lpf->returnArray($sql);
	
	for ($i=0; $i<sizeof($rows); $i++)
	{
		$StudentFound[strtoupper($rows[$i]["RegNo"])] = $rows[$i]["StudentID"];
	}
	unset($rows);

	# get all School Years and Semester
	$rows = $libacademic_year->Get_All_Year_List("en");
	for ($i=0; $i<sizeof($rows); $i++)
	{
		$AcademicYears[str_replace(" ", "", $rows[$i]["AcademicYearName"])] = $rows[$i]["AcademicYearID"];
//		
//		# get all semesters in a school year
//		$semRows = getAllSemesterByYearID($rows[$i]["AcademicYearID"]);
//		for($j=0; $j<sizeof($semRows); $j++){
//			$termsList[$rows[$i]["AcademicYearID"]][str_replace(" ", "", $semRows[$j]["TermTitle"])] = $semRows[$j]["YearTermID"];
//		}
	}
	unset($rows);
	
	$sql = "select RecordID, EN_SNAME from ".$intranet_db.".ASSESSMENT_SUBJECT order by RecordStatus";
	$rows = $lpf->returnArray($sql);
	for ($i=0; $i<sizeof($rows); $i++)
	{
		$SubjectsFound[strtoupper($rows[$i]["EN_SNAME"])] = $rows[$i]["RecordID"];
	}
	unset($rows);

	# verify RegNo, AcademicYear & SubjectCode
	for ($i=0; $i<sizeof($data); $i++)
	{
		list($RowStudentName, $RowRegNo, $RowYear, $RowTerm, $RowSubjectCode, $RowScore) = $data[$i];
		$RowRegNo = trim($RowRegNo);
		$RowYear = trim($RowYear);
		$RowTerm = trim($RowTerm);
		$RowSubjectCode = trim($RowSubjectCode);
		$IsOk = true;
		if ($StudentFound[strtoupper(str_replace("#", "", trim($RowRegNo)))]=="")
		{
			# no such RegNo
			$DataError[$i][] = "no student with such RegNo";
			$IsOk = false;
		}
		if ($AcademicYears[str_replace(" ", "", $RowYear)]=="")
		{
			# no such Year
			$DataError[$i][] = "no such academic year is found in system";
			$IsOk = false;
		} 
//		else {
//			if($termsList[$AcademicYears[str_replace(" ", "", $RowYear)]][str_replace(" ", "", $RowTerm)]==""){
//				# no such Term
//				$DataError[$i][] = "no such term is found in system";
//				$IsOk = false;
//			}
//		}
		if ($SubjectsFound[strtoupper($RowSubjectCode)]=="")
		{
			# no such Subject code
			$DataError[$i][] = "no such subject code is found in system";
			$IsOk = false;
		}
		
		# insert to template table
		if ($IsOk)
		{
			$data_confirmed[] = array($StudentFound[strtoupper(str_replace("#", "", $RowRegNo))], $AcademicYears[str_replace(" ", "", $RowYear)], $RowTerm, $SubjectsFound[strtoupper($RowSubjectCode)], $RowScore);

		}
	}
}

	$counter_e = 0;
	if (sizeof($DataError)>0)
	{
		$checked_result = sizeof($DataError) . " ".$Lang['iPortfolio']['SLP_REPORT']['AcademicPerformance_Data_invalid'].":";
		
		$checked_result .= '<table width="80%" border="0" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC" align="center">' .
		'<thead><tr class="tabletop"><td align="center" width="10%">'.$ec_guide['import_error_row'].'</td><td>'.$ec_guide['import_error_reason'].'</td></tr></thead><tbody>';
		foreach ($DataError AS $RowNumber => $ReasonObj)
		{
			
			$counter_e ++;
			$tr_class = ($counter_e%2==1) ? "tablerow1" : "tablerow2";
		
			$RowNumber += 2;
			$Reasons = implode(", ", $ReasonObj);
			$checked_result .= "<tr class='{$tr_class}'><td align='center'>{$RowNumber}</td><td>{$Reasons}</td></tr>\n";
		}
		$checked_result .= "</table>";
	} else
	{
		$checked_result = sizeof($data_confirmed) . " ".$Lang['iPortfolio']['SLP_REPORT']['AcademicPerformance_Data_valid'];
	}



$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<br />
<form name="form1" method="post" action="academic_points_import_update.php" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td colspan="2" align="center" height="90">
			<?=$checked_result?>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?php
			if (sizeof($DataError)>0)
			{
				echo $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button",  "self.location='./academic_points_import.php'");
			} else
			{
				echo $linterface->GET_ACTION_BTN($Lang['Btn']['Continue'], "submit");
			}
			
			?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "self.location='./academic_points.php'") ?>
		</td>
	</tr>
</table>
<input type="hidden" value='<?=urlencode(serialize($data_confirmed))?>' name="ImportData">
</form>
<br />


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
