<?php
/*
 * Change Log:
 * Date: 2017-02-28 Villa 
 * -	F113745  Fix Wrong class number 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

$lpf = new libpf_slp();
$ldb = new libdb();
$libclass = new libclass();

if(!$_SESSION["USER_BASIC_INFO"]["is_class_teacher"]){
	$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
	$lpf->ADMIN_ACCESS_PAGE();
}

########################################################
# Tab Menu : Start
########################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_CUST_DATA_SPTSS);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

############  Cotnent Here ######################

$allowed=false;
$rows_class_teacher=$libclass->returnClassTeacherByClassID($YearClassId);
foreach((array)$rows_class_teacher as $row_class_teacher){
	if($row_class_teacher['UserID']==$UserID){
		$allowed=true;
	}
}
if($lpf->IS_IPF_ADMIN()){
	$allowed=true;
}
if(!$allowed){
	echo 'You have no priviledge to access this page.';
	exit();
}


if($_POST['cmd']=='SUBMIT'){
	$YearClassId = $_POST['YearClassId'];
	
	$i=0;
	while(isset($_POST['StudentID_'.$i]) && $_POST['StudentID_'.$i]>-1){
		$PersonalCharacterID = $_POST['PersonalCharacterID_'.$i];
		$StudentID = $_POST['StudentID_'.$i];
		
		if($PersonalCharacterID == ''){
			$sql='INSERT INTO '.$eclass_db.'.`CUSTOM_SPTSS_STUDENT_PERSONAL_CHARACTER`(YearClassID,StudentID,Integrity,Courtesy,RightFromWrong,Relationship,LearningAttitude,Autonomy,ModifiedDate,ModifiedBy)' .
					' VALUES(\''.$YearClassId.'\',\''.$StudentID.'\',\''.$_POST['Integrity_'.$i].'\',\''.$_POST['Courtesy_'.$i].'\',\''.$_POST['RightFromWrong_'.$i].'\',\''.$_POST['Relationship_'.$i].'\',\''.$_POST['LearningAttitude_'.$i].'\',\''.$_POST['Autonomy_'.$i].'\',NOW(),\''.$UserID.'\')';
			$ldb->db_db_query($sql);
		}else{
			$sql='UPDATE '.$eclass_db.'.`CUSTOM_SPTSS_STUDENT_PERSONAL_CHARACTER` SET Integrity=\''.$_POST['Integrity_'.$i].'\',Courtesy=\''.$_POST['Courtesy_'.$i].'\',RightFromWrong=\''.$_POST['RightFromWrong_'.$i].'\',Relationship=\''.$_POST['Relationship_'.$i].'\',LearningAttitude=\''.$_POST['LearningAttitude_'.$i].'\',Autonomy=\''.$_POST['Autonomy_'.$i].'\',ModifiedDate=NOW(),ModifiedBy=\''.$UserID.'\' WHERE `PersonalCharacterID`='.$_POST['PersonalCharacterID_'.$i].';';
			$ldb->db_db_query($sql);
		}
		
		$i++;
	}

	header("Location: ".'personal_character_db_by_class.php');
}
// $sql =  "SELECT
//             CONCAT(".getNameFieldByLang2("iu.").") AS DisplayName,
//             ".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN")." AS ClassTitle,
//             iu.ClassNumber AS ClassNumber,
//             CONCAT(".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", ' - ', iu.ClassNumber) AS ClassInfo,
//             IF(MAX(csspc.ModifiedDate) IS NULL, '--', MAX(csspc.ModifiedDate)) AS latestModify,
// 			yc.YearClassID,iu.UserID," .
// 			"csspc.*
// 			FROM
// 			{$intranet_db}.INTRANET_USER AS iu
// 			INNER JOIN
// 			{$intranet_db}.YEAR_CLASS_USER AS ycu
// 			ON
// 			iu.UserID = ycu.UserID
// 			INNER JOIN
// 			{$intranet_db}.YEAR_CLASS AS yc
// 			ON
// 			ycu.YearClassID = yc.YearClassID
// 			LEFT JOIN
// 			{$eclass_db}.CUSTOM_SPTSS_STUDENT_PERSONAL_CHARACTER AS csspc
// 			ON
// 			csspc.StudentID = iu.UserID AND yc.YearClassID = csspc.YearClassID
// 			WHERE
// 			yc.YearClassID = ".$YearClassId."
//           GROUP BY
//             iu.UserID
//           ORDER BY
//             iu.ClassNumber
//         ";
$sql =  "SELECT
            CONCAT(".getNameFieldByLang2("iu.").") AS DisplayName,
            ".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN")." AS ClassTitle,
            ycu.ClassNumber AS ClassNumber,
            CONCAT(".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", ' - ', iu.ClassNumber) AS ClassInfo,
            IF(MAX(csspc.ModifiedDate) IS NULL, '--', MAX(csspc.ModifiedDate)) AS latestModify,
			yc.YearClassID,iu.UserID," .
					"csspc.*
          FROM
            {$intranet_db}.INTRANET_USER AS iu
          INNER JOIN
            {$intranet_db}.YEAR_CLASS_USER AS ycu
          ON
            iu.UserID = ycu.UserID
          INNER JOIN
            {$intranet_db}.YEAR_CLASS AS yc
          ON
            ycu.YearClassID = yc.YearClassID
          LEFT JOIN
            {$eclass_db}.CUSTOM_SPTSS_STUDENT_PERSONAL_CHARACTER AS csspc
          ON
            csspc.StudentID = iu.UserID AND yc.YearClassID = csspc.YearClassID
          WHERE
            yc.YearClassID = ".$YearClassId."
          GROUP BY
            iu.UserID
          ORDER BY
            ycu.ClassNumber
        ";
$rows_personal_character=$ldb->returnArray($sql);
############  END Cotnent Here ######################

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
echo $linterface->Include_CopyPaste_JS_CSS();
?>

<br />
<FORM name="form1" method="POST">
<table class="common_table_list_v30 edit_table_list_v30">
	<tr>
		<th>#</th>
		<th><?php echo $Lang['SysMgr']['FormClassMapping']['Class']; ?></th>
		<th><?php echo $Lang['SysMgr']['FormClassMapping']['ClassNo']; ?></th>
		<th><?php echo $Lang['iPortfolio']['SPTSS']['SPC']['Student']; ?></th>
		<th><?php echo $Lang['iPortfolio']['SPTSS']['SPC']['Integrity']; ?></th>
		<th><?php echo $Lang['iPortfolio']['SPTSS']['SPC']['Courtesy']; ?></th>
		<th><?php echo $Lang['iPortfolio']['SPTSS']['SPC']['Right From Wrong']; ?></th>
		<th><?php echo $Lang['iPortfolio']['SPTSS']['SPC']['Relationship']; ?></th>
		<th><?php echo $Lang['iPortfolio']['SPTSS']['SPC']['Learning Attitude']; ?></th>
		<th><?php echo $Lang['iPortfolio']['SPTSS']['SPC']['Autonomy']; ?></th>
	</tr>
	<?php $i=0; ?>
	<?php foreach((array)$rows_personal_character as $row_personal_character){ ?>
		<tr>
			<td><?php echo ($i+1); ?></td>
			<td><?php echo $row_personal_character['ClassTitle']; ?></td>
			<td><?php echo $row_personal_character['ClassNumber']; ?></td>
			<td>
				<?php echo $row_personal_character['DisplayName']; ?>
				<input type="hidden" name="StudentID_<?php echo $i; ?>" value="<?php echo $row_personal_character['UserID']; ?>" >
				<input type="hidden" name="PersonalCharacterID_<?php echo $i; ?>" value="<?php echo $row_personal_character['PersonalCharacterID']; ?>" >
			</td>
			<td><?php echo $linterface->GET_TEXTBOX('mark['.$i.'][0]', 'Integrity_'.$i, $row_personal_character['Integrity'], $OtherClass='', $OtherPar=array('maxlength'=>45,'onpaste'=>'isPasteContent(event,\'0\', \''.$i.'\')','onkeyup'=>'isPasteContent(event,\'0\', \''.$i.'\')','onkeydown'=>'isPasteContent(event,\'0\', \''.$i.'\')')); ?></td>
			<td><?php echo $linterface->GET_TEXTBOX('mark['.$i.'][1]', 'Courtesy_'.$i, $row_personal_character['Courtesy'], $OtherClass='', $OtherPar=array('maxlength'=>45,'onpaste'=>'isPasteContent(event,\'1\', \''.$i.'\')','onkeyup'=>'isPasteContent(event,\'1\', \''.$i.'\')','onkeydown'=>'isPasteContent(event,\'1\', \''.$i.'\')')); ?></td>
			<td><?php echo $linterface->GET_TEXTBOX('mark['.$i.'][2]', 'RightFromWrong_'.$i, $row_personal_character['RightFromWrong'], $OtherClass='', $OtherPar=array('maxlength'=>45,'onpaste'=>'isPasteContent(event,\'2\', \''.$i.'\')','onkeyup'=>'isPasteContent(event,\'2\', \''.$i.'\')','onkeydown'=>'isPasteContent(event,\'2\', \''.$i.'\')')); ?></td>
			<td><?php echo $linterface->GET_TEXTBOX('mark['.$i.'][3]', 'Relationship_'.$i, $row_personal_character['Relationship'], $OtherClass='', $OtherPar=array('maxlength'=>45,'onpaste'=>'isPasteContent(event,\'3\', \''.$i.'\')','onkeyup'=>'isPasteContent(event,\'3\', \''.$i.'\')','onkeydown'=>'isPasteContent(event,\'3\', \''.$i.'\')')); ?></td>
			<td><?php echo $linterface->GET_TEXTBOX('mark['.$i.'][4]', 'LearningAttitude_'.$i, $row_personal_character['LearningAttitude'], $OtherClass='', $OtherPar=array('maxlength'=>45,'onpaste'=>'isPasteContent(event,\'4\', \''.$i.'\')','onkeyup'=>'isPasteContent(event,\'4\', \''.$i.'\')','onkeydown'=>'isPasteContent(event,\'4\', \''.$i.'\')')); ?></td>
			<td><?php echo $linterface->GET_TEXTBOX('mark['.$i.'][5]', 'Autonomy_'.$i, $row_personal_character['Autonomy'], $OtherClass='', $OtherPar=array('maxlength'=>45,'onpaste'=>'isPasteContent(event,\'5\', \''.$i.'\')','onkeyup'=>'isPasteContent(event,\'5\', \''.$i.'\')','onkeydown'=>'isPasteContent(event,\'5\', \''.$i.'\')')); ?></td>
		</tr>
		<?php $i++; ?>
	<?php } ?>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td height="1" class="dotline"><img src="<?php echo $image_path.'/'.$LAYOUT_SKIN; ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td style="text-align:center;">
			<?php if(count($rows_personal_character)<=0){ ?>
			<?php }else{ ?>
				<?php echo $linterface->GET_ACTION_BTN($button_submit, "submit", ""); ?>
			<?php } ?>
			<?php echo $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back();"); ?>
		</td>
	</tr>
</table>

<input type="hidden" name="YearClassId" value="<?php echo $YearClassId; ?>" >
<input type="hidden" name="cmd" value="SUBMIT" >
<textarea id="text1" name="text1" style="display:none" cols="10" rows="4"></textarea>
</form>

<SCRIPT LANGUAGE="Javascript">
	var xno = "6";yno = "<?php echo $i; ?>";		// set table size
	var jsDefaultPasteMethod = "text";		// for copypaste.js
</SCRIPT>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
