<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

$lpf = new libpf_slp();
$ldb = new libdb();
$libclass = new libclass();


if(!$_SESSION["USER_BASIC_INFO"]["is_class_teacher"]){
	$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
	$lpf->ADMIN_ACCESS_PAGE();
}

########################################################
# Tab Menu : Start
########################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_CUST_DATA_SPTSS);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

############  Cotnent Here ######################

$allowed=false;
$rows_class_teacher=$libclass->returnClassTeacherByClassID($YearClassId);
foreach((array)$rows_class_teacher as $row_class_teacher){
	if($row_class_teacher['UserID']==$UserID){
		$allowed=true;
	}
}
if($lpf->IS_IPF_ADMIN()){
	$allowed=true;
}
if(!$allowed){
	echo 'You have no priviledge to access this page.';
	exit();
}

if($_POST['cmd']=='SUBMIT'){
	$YearClassId = $_POST['YearClassId'];
	$StudentID = $_POST['StudentID'];
	$PersonalCharacterID = $_POST['PersonalCharacterID'];
	
	if($PersonalCharacterID == ''){
		$sql='INSERT INTO '.$eclass_db.'.`CUSTOM_SPTSS_STUDENT_PERSONAL_CHARACTER`(YearClassID,StudentID,Integrity,Courtesy,RightFromWrong,Relationship,LearningAttitude,Autonomy,ModifiedDate,ModifiedBy)' .
				' VALUES(\''.$YearClassId.'\',\''.$StudentID.'\',\''.$_POST['Integrity'].'\',\''.$_POST['Courtesy'].'\',\''.$_POST['RightFromWrong'].'\',\''.$_POST['Relationship'].'\',\''.$_POST['LearningAttitude'].'\',\''.$_POST['Autonomy'].'\',NOW(),\''.$UserID.'\')';
		$ldb->db_db_query($sql);
	}else{
		$sql='UPDATE '.$eclass_db.'.`CUSTOM_SPTSS_STUDENT_PERSONAL_CHARACTER` SET Integrity=\''.$_POST['Integrity'].'\',Courtesy=\''.$_POST['Courtesy'].'\',RightFromWrong=\''.$_POST['RightFromWrong'].'\',Relationship=\''.$_POST['Relationship'].'\',LearningAttitude=\''.$_POST['LearningAttitude'].'\',Autonomy=\''.$_POST['Autonomy'].'\',ModifiedDate=NOW(),ModifiedBy=\''.$UserID.'\' WHERE `PersonalCharacterID`='.$_POST['PersonalCharacterID'].';';
		$ldb->db_db_query($sql);
	}

	header("Location: ".'personal_character_db.php');
}

$sql =  "SELECT
            CONCAT(".getNameFieldByLang2("iu.").") AS DisplayName,
            CONCAT(".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", ' - ', iu.ClassNumber) AS ClassInfo,
            IF(MAX(csspc.ModifiedDate) IS NULL, '--', MAX(csspc.ModifiedDate)) AS latestModify,
			yc.YearClassID," .
					"csspc.*
          FROM
            {$intranet_db}.INTRANET_USER AS iu
          INNER JOIN
            {$intranet_db}.YEAR_CLASS_USER AS ycu
          ON
            iu.UserID = ycu.UserID
          INNER JOIN
            {$intranet_db}.YEAR_CLASS AS yc
          ON
            ycu.YearClassID = yc.YearClassID
          LEFT JOIN
            {$eclass_db}.CUSTOM_SPTSS_STUDENT_PERSONAL_CHARACTER AS csspc
          ON
            csspc.StudentID = iu.UserID AND yc.YearClassID = csspc.YearClassID
          WHERE
            yc.YearClassID = ".$YearClassId."
            AND iu.UserID = ".$StudentID."
          GROUP BY
            iu.UserID
        ";
$rows_personal_character=$ldb->returnArray($sql);
$row_personal_character=$rows_personal_character[0];
$value=$row_personal_character;

$fields=array(
 	array(
 		'id'=>'Student',
 		'label'=>$Lang['iPortfolio']['SPTSS']['SPC']['Student'],
 		'control'=>$row_personal_character['DisplayName'].' ('.$row_personal_character['ClassInfo'].')',
 		'error'=>array(),
 	),
 	array(
 		'id'=>'Integrity',
 		'label'=>$Lang['iPortfolio']['SPTSS']['SPC']['Integrity'],
 		'control'=>$linterface->GET_TEXTBOX('Integrity', 'Integrity', $value['Integrity'], $OtherClass='', $OtherPar=array('maxlength'=>45)),
 		'error'=>array(),
 	),
 	array(
 		'id'=>'Courtesy',
 		'label'=>$Lang['iPortfolio']['SPTSS']['SPC']['Courtesy'],
 		'control'=>$linterface->GET_TEXTBOX('Courtesy', 'Courtesy', $value['Courtesy'], $OtherClass='', $OtherPar=array('maxlength'=>45)),
 		'error'=>array(),
 	),
 	array(
 		'id'=>'RightFromWrong',
 		'label'=>$Lang['iPortfolio']['SPTSS']['SPC']['Right From Wrong'],
 		'control'=>$linterface->GET_TEXTBOX('RightFromWrong', 'RightFromWrong', $value['RightFromWrong'], $OtherClass='', $OtherPar=array('maxlength'=>45)),
 		'error'=>array(),
 	),
 	array(
 		'id'=>'Relationship',
 		'label'=>$Lang['iPortfolio']['SPTSS']['SPC']['Relationship'],
 		'control'=>$linterface->GET_TEXTBOX('Relationship', 'Relationship', $value['Relationship'], $OtherClass='', $OtherPar=array('maxlength'=>45)),
 		'error'=>array(),
 	),
 	array(
 		'id'=>'LearningAttitude',
 		'label'=>$Lang['iPortfolio']['SPTSS']['SPC']['Learning Attitude'],
 		'control'=>$linterface->GET_TEXTBOX('LearningAttitude', 'LearningAttitude', $value['LearningAttitude'], $OtherClass='', $OtherPar=array('maxlength'=>45)),
 		'error'=>array(),
 	),
 	array(
 		'id'=>'Autonomy',
 		'label'=>$Lang['iPortfolio']['SPTSS']['SPC']['Autonomy'],
 		'control'=>$linterface->GET_TEXTBOX('Autonomy', 'Autonomy', $value['Autonomy'], $OtherClass='', $OtherPar=array('maxlength'=>45)),
 		'error'=>array(),
 	),
);

############  END Cotnent Here ######################

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<SCRIPT LANGUAGE="Javascript">

</SCRIPT>

<br />
<FORM name="form1" method="POST">
<table class="form_table_v30">
	<?php if(count($fields)>0){?>
		<?php foreach($fields as $field){ ?>
			<tr>
				<td class="field_title"><label for="<?php echo $field['id']; ?>"><?php echo $field['label']; ?></label></td>
				<td>
					<?php echo $field['control']; ?>
					<?php foreach((array)$field['error'] as $error){?>
						<?php echo $error ?>
					<?php } ?>
				</td>
			</tr>
		<?php } ?>
	<?php } ?>
	<tr>
		<td colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr><td align="left" class="tabletextremark"><?=$ec_iPortfolio['mandatory_field_description']?></td></tr>
				<tr>
					<td style="text-align:center;">
						<?php echo $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
						<?php echo $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back();") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="YearClassId" value="<?php echo $YearClassId; ?>" >
<input type="hidden" name="StudentID" value="<?php echo $StudentID; ?>" >
<input type="hidden" name="PersonalCharacterID" value="<?php echo $value['PersonalCharacterID']; ?>" >
<input type="hidden" name="cmd" value="SUBMIT" >
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
