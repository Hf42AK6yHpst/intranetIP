<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

$lpf = new libpf_slp();


if(!$_SESSION["USER_BASIC_INFO"]["is_class_teacher"]){
	$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
	$lpf->ADMIN_ACCESS_PAGE();
}

########################################################
# Tab Menu : Start
########################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_CUST_DATA_SPTSS);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

############  Cotnent Here ######################

### view tag
// $viewTabAry[] = array($displayLang, $onclickJs, $iconPath, $isSelectedTab);
$curViewTab = 'view2';
$viewTabAry = array();
$viewTabAry[] = array(Get_Lang_Selection('學生','Student'), 'personal_character_db.php', '', $curViewTab=='view2');
$viewTabAry[] = array(Get_Lang_Selection('班別','Class'), 'personal_character_db_by_class.php', '', $curViewTab=='view1');
$htmlAry['viewTag'] = $linterface->GET_CONTENT_TOP_BTN($viewTabAry);

$libacademic_year = new academic_year();

# academic year selection
$academic_year_arr = $libacademic_year->Get_All_Year_List();
$academicYearID = (isset($academicYearID))? $academicYearID : Get_Current_Academic_Year_ID();
$select_academic_year = getSelectByArray($academic_year_arr, 'name="academicYearID" class="select_academic_year" onchange="select_academic_year_onchange();jSUBMIT_FORM();"', $academicYearID, 1, 1, $i_Attendance_AllYear, 2);

# year class selection
$lfcm = new form_class_manage();
$t_year_class_arr = $lfcm->Get_Class_List_By_Academic_Year($academicYearID);
$year_class_arr = array();
for($i=0; $i<count($t_year_class_arr); $i++)
{
  $t_year_class_id = $t_year_class_arr[$i]['YearClassID'];
  $t_year_class_title = Get_Lang_Selection($t_year_class_arr[$i]['ClassTitleB5'],$t_year_class_arr[$i]['ClassTitleEN']);
  
  $year_class_arr[] = array($t_year_class_id, $t_year_class_title);
}
$select_class = getSelectByArray($year_class_arr, 'name="yearClassID" class="select_class" onchange="select_class_onchange();jSUBMIT_FORM();"', $yearClassID, 1, 0, $i_general_all_classes, 2);

# student data
$cond = empty($yearClassID) ? "" : " AND yc.YearClassID = ".$yearClassID;
if($_SESSION["USER_BASIC_INFO"]["is_class_teacher"] && !$lpf->IS_IPF_ADMIN()){
	$cond .= " AND yc.YearClassID in (SELECT `YearClassID`FROM`YEAR_CLASS_TEACHER` WHERE UserID='".$UserID."')";
}
$sql =  "SELECT
            CONCAT('<a href=\"personal_character_db_update.php?StudentID=', iu.UserID, '&YearClassId=', yc.YearClassID, '\" class=\"tablelink\">', ".getNameFieldByLang2("iu.").", '</a>') AS DisplayName,
            CONCAT(".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", ' - ', iu.ClassNumber) AS ClassInfo,
            IF(MAX(csspc.ModifiedDate) IS NULL, '--', MAX(csspc.ModifiedDate)) AS latestModify,
			yc.YearClassID," .
					"csspc.*
          FROM
            {$intranet_db}.INTRANET_USER AS iu
          INNER JOIN
            {$intranet_db}.YEAR_CLASS_USER AS ycu
          ON
            iu.UserID = ycu.UserID
          INNER JOIN
            {$intranet_db}.YEAR_CLASS AS yc
          ON
            ycu.YearClassID = yc.YearClassID
          LEFT JOIN
            {$eclass_db}.CUSTOM_SPTSS_STUDENT_PERSONAL_CHARACTER AS csspc
          ON
            csspc.StudentID = iu.UserID  AND yc.YearClassID = csspc.YearClassID
          WHERE
            yc.AcademicYearID = ".$academicYearID."
            $cond
          GROUP BY
            iu.UserID
        ";
        
if ($order=="") $order=1;
if ($field=="") $field=1;
$LibTable = new libpf_dbtable($field, $order, $pageNo);
$LibTable->field_array = array("DisplayName", "iu.ClassName, iu.ClassNumber", "latestModify");
$LibTable->sql = $sql;
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($numPerPage==''?'50':$numPerPage);
$LibTable->no_col = 4;
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

// TABLE COLUMN
$LibTable->column_list .= "<tr class='tabletop'>\n";
$LibTable->column_list .= "<td height='25' align='center' class=\"tabletopnolink\" >#</span></td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' width='100' >".$LibTable->column(0,$ec_iPortfolio['student'], 1)."</td>\n";
$LibTable->column_list .= "<td class=\"tabletopnolink\">".$LibTable->column(1,$i_general_class."-".$i_ClassNumber, 1)."</td>";
$LibTable->column_list .= "<td class=\"tabletopnolink\">".$LibTable->column(2,$ec_iPortfolio['last_update'], 1)."</td>\n";
$LibTable->column_list .= "</tr>\n";
$LibTable->column_array = array(0,0,0);
$table_content = $LibTable->displayPlain();
$table_content .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
$table_content .= ($LibTable->navigationHTML!="") ? "<tr class='tablebottom'><td class=\"tabletext\" align=\"right\">".$LibTable->navigation(1)."</td></tr>" : "";
$table_content .= "</table>";



############  END Cotnent Here ######################

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<SCRIPT LANGUAGE="Javascript">
function jSUBMIT_FORM(){
  document.form1.submit();
}

function select_academic_year_onchange(){
	$('.select_class').val('');
	$('#pageNo').val('1');
}

function select_class_onchange(){
	$('#pageNo').val('1');
}

</SCRIPT>

<br />
<?php echo $htmlAry['viewTag']; ?>
<FORM name="form1" method="GET">
	<?php echo $select_academic_year; ?>
	<?php if(!$_SESSION["USER_BASIC_INFO"]["is_class_teacher"]){echo $select_class;} ?>
	<?php echo $table_content; ?>
	
	<input type="hidden" name="pageNo" id="pageNo" value="<?php echo $pageNo ?>">
	<input type="hidden" name="order" value="<?php echo $order ?>">
	<input type="hidden" name="field" value="<?php echo $field ?>">
	<input type="hidden" name="numPerPage" value="<?php echo $numPerPage ?>">
	<input type="hidden" name="page_size_change">
	<input type="hidden" name="FromPage" value="program" >
	<input type="hidden" name="IntExt" value="<?=$IntExt?>">
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
