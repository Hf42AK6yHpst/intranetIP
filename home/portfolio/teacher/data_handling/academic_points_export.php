<?php 

// Modifing by
/*
 * 2016-11-29	Omas
 * 		- Create the page 
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lexport = new libexporttext();
$libacademic_year = new academic_year();
$linterface = new interface_html();
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

$lpf = new libpf_slp();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$academicYearID = $_GET['AcademicYearID'];
$termNumber = $_GET['TermNumber'];
if($academicYearID != '' && $termNumber != ''){
	$fields = " StudentID, AcademicYearID, TermID, SubjectID, SCORE ";
	$sql = "SELECT $fields FROM $eclass_db.SLP_STUDENT_SUBJECT_POINTS WHERE AcademicYearID = '$academicYearID' AND TermID = '$termNumber'";
	$result = $lpf->returnResultSet($sql);
	$studentIDArr = Get_Array_By_Key($result, 'StudentID');
	
	$sql = "SELECT UserID, EnglishName, ChineseName, WebSAMSRegNo  FROM $intranet_db.INTRANET_USER WHERE UserID IN ('".implode("','", (array)$studentIDArr)."')";
	$studentInfoArr = $lpf->returnResultSet($sql);
	
	if(count($studentIDArr) != count($studentInfoArr)){
		// find Archived student 
		$sql = "SELECT UserID, EnglishName, ChineseName, WebSAMSRegNo  FROM $intranet_db.INTRANET_ARCHIVE_USER WHERE UserID IN ('".implode("','", (array)$studentIDArr)."')";
		$aStudentInfoArr = $lpf->returnResultSet($sql);
		$studentInfoArr = array_merge((array)$studentInfoArr, (array)$aStudentInfoArr);
	}
	$studentAssoc = BuildMultiKeyAssoc($studentInfoArr, 'UserID');
	
	// subject
	$sql = "select RecordID, EN_SNAME from ".$intranet_db.".ASSESSMENT_SUBJECT order by RecordStatus";
	$subjectAssoc = BuildMultiKeyAssoc($lpf->returnResultSet($sql), 'RecordID', array('EN_SNAME'),1 );
	
	// academicYear
	$academicYearAssoc = BuildMultiKeyAssoc( $libacademic_year->Get_All_Year_List("en"),'AcademicYearID', array('AcademicYearName'), 1 );
	
	$i = 0;
	foreach((array)$result as $_dataInfo){
		$exportData[$i][] = $studentAssoc[$_dataInfo['StudentID']][Get_Lang_Selection('ChineseName','EnglishName')];
		$exportData[$i][] = $studentAssoc[$_dataInfo['StudentID']]['WebSAMSRegNo'];
		$exportData[$i][] = $academicYearAssoc[$_dataInfo['AcademicYearID']];
		$exportData[$i][] = $_dataInfo['TermID'];
		$exportData[$i][] = $subjectAssoc[$_dataInfo['SubjectID']];
		$exportData[$i][] = $_dataInfo['SCORE'];
		$i++;
	}
	$exportColumn = array('Student Name',
								'RegNo',
								'Year',
								'Term',
								'Subject Code',
								'Score');
	$filename = 'OutstandingAcademicPerformance_'.$academicYearAssoc[$academicYearID].'.csv';
	$export_content = $lexport->GET_EXPORT_TXT($exportData, $exportColumn);
	$lexport->EXPORT_FILE($filename, $export_content);
}
?>