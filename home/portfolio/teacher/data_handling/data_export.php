<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio{$LAYOUT_SKIN}.php");

intranet_auth();
intranet_opendb();

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_DataExport";
$CurrentPageName = $iPort['menu']['data_export'];

$lpf = new libportfolio();
$luser = new libuser($UserID);

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

############  Cotnent Here ######################

$ButtonClass = "class=\"formsubbutton\" onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\"";
$PreviousClass = "class=\"tablerow2\"";
# merit row
if(strstr($ck_user_rights_ext, "merit"))
{
	$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";
	
	$MeritRow = "<tr valign=\"middle\" ".$PreviousClass.">
                <td align=center nowrap>".$ec_iPortfolio['merit']."</td>
                <td align=center><input onClick=\"doExport('merit')\" type=button value=\"".$button_export."\" $ButtonClass></td>
              </tr>";
}

# assessment row
if(strstr($ck_user_rights_ext, "assessment_report"))
{
$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";

	$AssessmentRow = "<tr valign=\"middle\" ".$PreviousClass.">
                <td align=center nowrap>".$ec_iPortfolio['academic_result']."</td>
                <td align=center><input onClick=\"doExport('assessment')\" type=button value=\"".$button_export."\" $ButtonClass></td>
              </tr>";
}

# activity row
if(strstr($ck_user_rights_ext, "activity"))
{
$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";

	$ActivityRow = "<tr valign=\"middle\" ".$PreviousClass.">
                <td align=center nowrap>".$ec_iPortfolio['activity']."</td>
                <td align=center><input onClick=\"doExport('activity')\" type=button value=\"".$button_export."\" $ButtonClass></td>
              </tr>";
}

# award row
if(strstr($ck_user_rights_ext, "award"))
{
$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";

	$AwardRow = "<tr valign=\"middle\" ".$PreviousClass.">
                <td align=center nowrap>".$ec_iPortfolio['award']."</td>
                <td align=center><input onClick=\"doExport('award')\" type=button value=\"".$button_export."\" $ButtonClass></td>
			  </tr>";
}

# teacher comment row
if(strstr($ck_user_rights_ext, "teacher_comment"))
{
$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";

	$TeacherCommentRow = "<tr valign=\"middle\" ".$PreviousClass.">
                <td align=center nowrap>".$ec_iPortfolio['teacher_comment']."</td>
                <td align=center><input onClick=\"doExport('comment')\" name=\"button\" type=button value=\"".$button_export."\" $ButtonClass></td>
              </tr>";
}

# attendance row
if(strstr($ck_user_rights_ext, "attendance"))
{
$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";

	$AttendanceRow = "<tr valign=\"middle\" ".$PreviousClass.">
                <td align=center nowrap>".$ec_iPortfolio['attendance']."</td>
                <td align=center><input onClick=\"doExport('attendance')\" type=button value=\"".$button_export."\" $ButtonClass></td>
              </tr>";
}

# service row
if(strstr($ck_user_rights_ext, "service"))
{
$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";

	$ServiceRow = "<tr valign=\"middle\" ".$PreviousClass.">
                <td align=center nowrap>".$ec_iPortfolio['service']."</td>
                <td align=center><input onClick=\"doExport('service')\" type=button value=\"".$button_export."\" $ButtonClass></td>
              </tr>";
}

# olr row
if(strstr($ck_user_rights_ext, "ole"))
{
$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";

	$OLERow = "<tr valign=\"middle\" ".$PreviousClass.">
                <td align=center nowrap>".$ec_iPortfolio['ole']."</td>
                <td align=center><input onClick=\"doExport('ole')\" type=button value=\"".$button_export."\" $ButtonClass></td>
              </tr>";
}

# Table Open
$TableOpen =  "<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"1\" bgcolor=\"#CCCCCC\">";
# Table Close
$TableClose = "</table>";
# Form Open
$FormOpen = "<FORM method=\"POST\" name=\"form1\">";
# Form Close
$FormClose = "</FORM>";

$Content = "";
$Content .= $FormOpen;
$Content .= $TableOpen;
// row shown
$Content .= $MeritRow;
$Content .= $AssessmentRow;
$Content .= $ActivityRow;
$Content .= $AwardRow;
$Content .= $TeacherCommentRow;
$Content .= $AttendanceRow;
$Content .= $ServiceRow;
$Content .= $OLERow;

$Content .= $TableClose;
$Content .= $FormClose;

############  END Cotnent Here ######################


$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
function doExport(my_type){
	newWindow("../../profile/"+my_type+"/data_export.php", 27);
}
</SCRIPT>

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="96%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<?=$Content?>
</td></tr></table>
</td></tr></table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
