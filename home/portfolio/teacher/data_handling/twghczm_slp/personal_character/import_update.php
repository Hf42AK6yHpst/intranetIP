<?php
// using:

/**
 * [Modification Log] Modifying By:
 * *******************************************
 *
 *
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
iportfolio_auth("T");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libimporttext.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/iportfolio_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libuser.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libportfolio.php");
include_once ($PATH_WRT_ROOT . "includes/libportfolio2007a.php");
include_once ($PATH_WRT_ROOT . "includes/libpf-slp.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/libpf-tabmenu.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/iPortfolioConfig.inc.php");
include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");

// Error Code
define("ERROR_TYPE_STUDENT_NOT_FOUND", 1);
define("ERROR_TYPE_DUPLICATED_CLASSNUM", 2);

$loginUserId = $_SESSION['UserID'];

unset($error_data);
$count_success = 0;
if ($g_encoding_unicode) {
    $import_coding = ($g_chinese == "") ? "b5" : "gb";
}

// uploaded file information
$filepath = $userfile;
$filename = $userfile_name;

$uploaderUserId = $UserID; // variable from session

$count_new = 0;
$count_updated = 0;
$display_content = "";
if (! $sys_custom['iPf']['twghczm']['Report']['SLP']) {
    header("location: /home/");
    exit();
}

if ($filepath == "none" || $filepath == "" || ! is_uploaded_file($filepath)) {
    header("Location: import.php?FromPage=$FromPage");
    exit();
} else {
    intranet_opendb();
    $lpf = new libpf_slp();
    
    $lpf->CHECK_ACCESS_IPORTFOLIO();
    // $lpf->ACCESS_CONTROL("ole");
    $academicYearID = $_POST['academicYearID'];
    
    $CurrentPage = "Teacher_OLE";
    $CurrentPageName = $iPort['menu']['ole'];
    
    $TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_CUST_DATA_CZM);
    $MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");
    
    $linterface = new interface_html();
    $linterface->LAYOUT_START();
    
    $li = new libdb();
    $lo = new libfilesystem();
    $limport = new libimporttext();
    
    $ele_list = $lpf->GET_OLE_ELE_LIST();
    $ext = strtoupper($lo->file_ext($filename));
    
    if ($ext == ".CSV" || $ext == ".TXT") {
        $data = $limport->GET_IMPORT_TXT($filepath);
        $header_row = array_shift($data); // drop the title bar
    }
   
    // Check Title Row
    // debug_pr($IntExt);
    $file_format = getFileFormat($IntExt);
 
    $format_wrong = false;
    for ($i = 0; $i < sizeof($file_format); $i ++) {
        if ($header_row[$i] != $file_format[$i]) {
            $format_wrong = true;
            break;
        }
    }
    
    // #####################################
    //
    // Clear Temp record
    //
    // #####################################
    
    $sql = "DELETE FROM {$eclass_db}.TEMP_CUSTOM_STUDENT_PERSONAL_CHARACTER WHERE LoginUserID = " . $loginUserId;
    $li->db_db_query($sql);
    
    $sql = "DELETE FROM {$eclass_db}.TEMP_CUSTOM_STUDENT_REMARK WHERE LoginUserID = " . $loginUserId;
    $li->db_db_query($sql);
    
    if ($format_wrong) {
        $correct_format = "<table border='0' cellpadding='5' cellspacing='0'>\n";
        for ($i = 0; $i < sizeof($file_format); $i ++) {
            $correct_format .= "<tr><td>" . $file_format[$i] . "</td></tr>\n";
        }
        $correct_format .= "</table>\n";
        
        $wrong_format = "<table border='0' cellpadding='5' cellspacing='0'>\n";
        for ($i = 0; $i < sizeof($header_row); $i ++) {
            $field_title = ($header_row[$i] != $file_format[$i]) ? "<u>" . $header_row[$i] . "</u>" : $header_row[$i];
            $wrong_format .= "<tr><td>" . $field_title . "</td></tr>\n";
        }
        $wrong_format .= "</table>\n";
        
        $display_content .= "<br><span class='chi_content_15'>" . $ec_guide['import_error_wrong_format'] . "</span><br>\n";
        $display_content .= "<table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
        $display_content .= "<tr><td valign='top'>$correct_format</td><td width='90%' align='center'>VS</td><td valign='top'>$wrong_format</td></tr>\n";
        $display_content .= "</table>\n";
    } else {
        // FORMAT IS CORRECT , START THE PROCESS
        
        $li->Start_Trans();
        $sqlRes = array();
        
        $PersonalCharacter = $Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type'];
        for ($i = 0; $i < sizeof($data); $i ++) // This data get the data from import file
        {
            $t_ps = array();
            list ($t_websams_regno, $t_class_name, $t_class_num, $t_ps[0], $t_ps[1], $t_ps[2], $t_ps[3], $t_ps[4], $t_ps[5], $t_ps[6], $t_ps[7], $t_ps[8],$Remark) = $data[$i];
            ksort($t_ps);          
            if ($t_websams_regno != "") {
                $cond = " AND WebSAMSRegNo = '$t_websams_regno' ";
                // get UserID
                $sql = "SELECT DISTINCT
							UserID
						FROM
							INTRANET_USER
						WHERE
							RecordType = '2'
							AND RecordStatus IN ('0','1','2','3')
							$cond
					";
                $UserArr = $li->returnVector($sql);
                
                if (sizeof($UserArr) > 0) {
                    $StudentId = addslashes($UserArr[0]);
                    $fields = getDbFields();
                    
                    $YearClassId = getYearClassIdByAcademicYearID($academicYearID, $StudentId);
      
             //       $_row = $i + 1;
                    $sql = "INSERT INTO {$eclass_db}.TEMP_CUSTOM_STUDENT_REMARK (AcademicYearID,YearClassID,StudentID,Remark,ModifiedDate,LoginUserID) VALUES ('$academicYearID','$YearClassId','$StudentId','$Remark',now(),'$uploaderUserId') ";
                    
                    $q_result = $li->db_db_query($sql);
                    
                    for ($j = 0; $j < sizeof($PersonalCharacter); $j ++) {
                        $grade = trim(addslashes($t_ps[$j]));
                        
                        $values = "'$academicYearID','$YearClassId','$StudentId', '$j', '$grade', now(),'$uploaderUserId'";
                        
                        $sql = "INSERT INTO {$eclass_db}.TEMP_CUSTOM_STUDENT_PERSONAL_CHARACTER ($fields) VALUES ($values) ";
                        
                        $q_result = $li->db_db_query($sql);
                        
                        $sqlRes[] = $q_result;
                    }
                     
                     
                    
                    if ($q_result) {
                        $count_success ++;
                    }
                } else {
                    $sqlRes[] = false; // When any user cannot be found, put false to the sqlRes to trigger a rollback
                    $error_data[] = array(
                        $i,
                        ERROR_TYPE_STUDENT_NOT_FOUND,
                        $data[$i]
                    );
                }
            } else {
                $sqlRes[] = false; // When classname / classno is empty, put false to the sqlRes to trigger a rollback
                $error_data[] = array(
                    $i,
                    ERROR_TYPE_STUDENT_NOT_FOUND,
                    $data[$i]
                );
            }
        }
        
        if (in_array(false, $sqlRes) || count((array) $error_data) > 0) {
            $li->RollBack_Trans();
        } else {
            $li->Commit_Trans();
        }
        
        // Display import stats
        $display_content = "<div class='tabletext' style='text-align: center'>" . $Lang['General']['SuccessfulRecord'] . " : <b>$count_success</b></div><br>";
        $display_content .= "<div class='tabletext' style='text-align: center'>" . $Lang['General']['FailureRecord'] . " : <b>" . count($error_data) . "</b></div><br>";
    }
}

if (sizeof($error_data) > 0) {
    $error_table = "<br><table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
    $error_table .= "<tr class=tableTitle bgcolor='#CFE6FE'><td>" . $ec_guide['import_error_row'] . "</td><td>" . $ec_guide['import_error_reason'] . "</td><td>" . $ec_guide['import_error_detail'] . "</td></tr>\n";
    
    for ($i = 0; $i < sizeof($error_data); $i ++) {
        list ($t_row, $t_type, $t_data) = $error_data[$i];
        $t_row ++; // set first row to 1
        $css_color = ($i % 2 == 0) ? "#FFFFFF" : "#F3F3F3";
        $error_table .= "<tr bgcolor='$css_color'><td class='tabletext'>$t_row</td><td class='tabletext'>";
        $reason_string = "Unknown";
        switch ($t_type) {
            case ERROR_TYPE_STUDENT_NOT_FOUND:
                $reason_string = $ec_guide['import_error_no_user'];
                break;
            default:
                $reason_string = $ec_guide['import_error_unknown'];
                break;
        }
        $error_table .= $reason_string;
        $error_table .= "</td><td class='tabletext'>" . implode(",", $t_data) . "</td></tr>\n";
    }
    $error_table .= "</table>\n";
    $display_content .= $error_table;
}

// ## steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep = 2);

// #################################
// Buttons
// #################################
$x = '';
if (count($error_data) > 0 || $format_wrong) {
    $x .= $linterface->GET_ACTION_BTN($ec_guide['import_back'], "button", "self.location='import.php?academicYearID=$academicYearID'");
} else {
    $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "submit", "");
}
$x .= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='import.php?academicYearID=$academicYearID'");
$htmlAry['Btns'] = $x;

// debug_pr($IntExt);
?>
<table align="left" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><img
			src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif"
			border="0" align="absmiddle"> <?=$button_import?> </a></td>
		<td><img
			src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
	</tr>
</table>

<br />

<FORM enctype="multipart/form-data" method="POST" name="form1"
	action="import_save.php">
	<?=$htmlAry['steps']?>
	<br />
	<?= $display_content ?>
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td height="1" class="dotline"><img
				src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10"
				height="1"></td>
		</tr>
	</table>
	<p align="center">
	<?= $htmlAry['Btns'] ?>
	</p>

</FORM>



<?php

function getYearClassIdByAcademicYearID($academicYearID, $StudentID)
{
    global $intranet_db, $li;
    $sql = "SELECT yc.YearClassID 
			FROM YEAR_CLASS as yc
			INNER JOIN YEAR_CLASS_USER as ycu ON (ycu.YearClassID = yc.YearClassID)
			WHERE ycu.UserID = '$StudentID' and yc.AcademicYearID = '$academicYearID'";
    $result = $li->returnArray($sql);
    
    return $result[0]['YearClassID'];
}

function getDbFields()
{
    global $sys_custom;
    // define("DBF_TEMPID", "TempID");
    define("DBF_ACADEMICYEARID", "AcademicYearID");
    define("DBF_YEARCLASSID", "YearClassID");
    define("DBF_STUDENTID", "StudentID");
    define("DBF_TYPEID", "TypeID");
    define("DBF_GRADE", "Grade");
    define("DBF_MODIFIEDDATE", "ModifiedDate");
    define("DBF_LOGINUSERID", "LoginUserID");
    
    $fields = array();
    // array_push($fields, DBF_TEMPID);
    array_push($fields, DBF_ACADEMICYEARID);
    array_push($fields, DBF_YEARCLASSID);
    array_push($fields, DBF_STUDENTID);
    array_push($fields, DBF_TYPEID);
    array_push($fields, DBF_GRADE);
    array_push($fields, DBF_MODIFIEDDATE);
    array_push($fields, DBF_LOGINUSERID);
    
    return implode(",", $fields);
}

function getFileFormat()
{
    global $sys_custom, $Lang;
    define("CSV_WEBSAMSREGNO", "WebSAMSRegNo");
    define("CSV_CLASS", "Class");
    define("CSV_CLASSNUMBER", "ClassNumber");
    define("CSV_REMARKS", "Remarks");
    
    $file_format = array();
    array_push($file_format, CSV_WEBSAMSREGNO);
    array_push($file_format, CSV_CLASS);
    array_push($file_format, CSV_CLASSNUMBER);
  
    
    $PersonalCharacter = $Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type'];
    for ($i = 0; $i < sizeof($PersonalCharacter); $i ++) {
        define($PersonalCharacter[$i], "$PersonalCharacter[$i]");
        array_push($file_format, $PersonalCharacter[$i]);
    }
    array_push($file_format, CSV_REMARKS);
    return $file_format;
}
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
