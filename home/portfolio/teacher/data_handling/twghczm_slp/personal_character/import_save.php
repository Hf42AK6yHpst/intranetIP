<?php
# using: anna

/** [Modification Log] Modifying By: 
 * *******************************************
 * 	Modification Log

 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libpf-ole.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_opendb();
$lpf = new libpf_slp();

if (!$sys_custom['iPf']['twghczm']['Report']['SLP']) {
	header("location: /home/");
	exit;
}


$lpf->CHECK_ACCESS_IPORTFOLIO();

$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface = new interface_html();


$li = new libdb();
//$lo = new libfilesystem();
$limport = new libimporttext();


$ele_list = $lpf->GET_OLE_ELE_LIST();

$loginUserId = $_SESSION['UserID'];


$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_CUST_DATA_CZM);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");
################################################
#	Get Data From Temp Table
################################################

##############START Insert Remark################# 
$sql = "SELECT * FROM {$eclass_db}.TEMP_CUSTOM_STUDENT_REMARK WHERE LoginUserID=" . $loginUserId ." ORDER BY TempID ASC ";
$returnStudentRemarkAry = $li->returnArray($sql);


for($i=0;$i<sizeof($returnStudentRemarkAry);$i++){
    $AcademicYearID = $returnStudentRemarkAry[$i]['AcademicYearID'];
    $YearClassID= $returnStudentRemarkAry[$i]['YearClassID'];
    $StudentID = $returnStudentRemarkAry[$i]['StudentID'];
    $Remark= $returnStudentRemarkAry[$i]['Remark'];
    $ModifiedDate= $returnStudentRemarkAry[$i]['ModifiedDate'];
    $ModifiedBy= $returnStudentRemarkAry[$i]['LoginUserID'];
    
    $sql = "Select Remark From $eclass_db.CUSTOM_STUDENT_REMARKS
            WHERE StudentID = '{$StudentID}' and YearClassID='{$YearClassID}'";
    $RemarkAry =  $lpf->returnVector($sql);
  
    if(empty($RemarkAry)){
        $sql='INSERT INTO '.$eclass_db.'.`CUSTOM_STUDENT_REMARKS`
						(AcademicYearID,YearClassID,StudentID,Remark,ModifiedDate,ModifiedBy)' .
						' VALUES
					(\''.$AcademicYearID.'\',\''.$YearClassID.'\',\''.$StudentID.'\',\''.$Remark.'\',NOW(),\''.$ModifiedBy.'\')';
        
        $lpf->db_db_query($sql);      
    }else{
        $sql='UPDATE '.$eclass_db.'.`CUSTOM_STUDENT_REMARKS`
					SET
						Remark =\''.$Remark.'\',
						ModifiedDate=NOW(),
						ModifiedBy=\''.$ModifiedBy.'\'
					WHERE
						`StudentID`='.$StudentID.' AND `YearClassID` = \''.$YearClassID.'\';';
        $lpf->db_db_query($sql);
    }   
}
#############Insert Remarks END###########


$sql = "SELECT * FROM {$eclass_db}.TEMP_CUSTOM_STUDENT_PERSONAL_CHARACTER WHERE LoginUserID=" . $loginUserId ." ORDER BY TempID ASC ";
$returnStudentPersonalCharacterArr = $li->returnArray($sql);

$countStudentPersonalCharacterArr= count($returnStudentPersonalCharacterArr);

$fields = "AcademicYearID,YearClassID,StudentID,TypeID,Grade,ModifiedDate,ModifiedBy";
$successStd = 0;

$PersonalCharacter = $Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type'];
for($i=0;$i<$countStudentPersonalCharacterArr;$i++){
    $_AcademicYearID = $returnStudentPersonalCharacterArr[$i]['AcademicYearID'];
    $_YearClassID= $returnStudentPersonalCharacterArr[$i]['YearClassID'];
	$_StudentID= $returnStudentPersonalCharacterArr[$i]['StudentID'];
	$_TypeID= $returnStudentPersonalCharacterArr[$i]['TypeID'];
	$_Grade= $returnStudentPersonalCharacterArr[$i]['Grade'];

	$_AcademicYearID = $li->Get_Safe_Sql_Query($_AcademicYearID);
	$_YearClassID= $li->Get_Safe_Sql_Query($_YearClassID);
	$_StudentID= $li->Get_Safe_Sql_Query($_StudentID);
	$_TypeID= $li->Get_Safe_Sql_Query($_TypeID);
	$_Grade= $li->Get_Safe_Sql_Query($_Grade);

	$values = "'$_AcademicYearID','$_YearClassID','$_StudentID','$_TypeID','$_Grade',now(),'$UserID'";
	
	$SQL = "SELECT TypeID
			FROM $eclass_db.CUSTOM_STUDENT_PERSONAL_CHARACTER
			WHERE StudentID = '{$_StudentID}' and YearClassID='{$_YearClassID}'";
	$TypeIDAry =  $lpf->returnVector($SQL);
	
	
		
	if(!in_array($_TypeID,$TypeIDAry)){
		$sql='INSERT INTO '.$eclass_db.'.`CUSTOM_STUDENT_PERSONAL_CHARACTER`
						('.$fields.')' .
						' VALUES
					('.$values.')';
	
		$q_result = $lpf->db_db_query($sql);
	}else{
			$sql='UPDATE '.$eclass_db.'.`CUSTOM_STUDENT_PERSONAL_CHARACTER`
					SET
						Grade=\''.$_Grade.'\',
						ModifiedDate = now(),
						ModifiedBy=\''.$UserID.'\'
					WHERE
						`StudentID`='.$_StudentID.' and `TypeID` = \''.$_TypeID.'\' and `YearClassID` = \''.$_YearClassID.'\';';
			
			$q_result = $lpf->db_db_query($sql);
	}

	$successStd +=$q_result;
}	
$successStd = $successStd/count($PersonalCharacter);
### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=3);

### Buttons
$htmlAry['doneBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "self.location='personal_character_db_by_class.php'");

$linterface->LAYOUT_START();
?>

<form name="form1" id="form1" method="POST">

	<?=$htmlAry['steps']?>
	
	<div class="table_board">
		<table width="100%">
			<tr><td style="text-align:center;">
				<span id="NumOfProcessedPageSpan"><?=($successStd!=0)?$successStd:$successProgram?></span> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?>
			</td></tr>
		</table>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['doneBtn']?>
		<p class="spacer"></p>
	</div>

</form>	
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>