<?php

// Modifing by 
/*
 * 	Log
 *
 * 	Date:	2016-01-27 [Cameron]
 * 			use larger new window (change window type) for merit, activity and comment 
 *  
 * 	Date:	2016-01-25 [Cameron]
 * 			Add Import button for Merit/Demerit and Activity 
 * 			if $sys_custom['iPf']['SchoolRecord']['ImportWebsams']['ANPActivityComment'] is true
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

//debug_r($_SESSION);
// template for teacher page
$linterface = new interface_html();
// set the current page title
//$CurrentPage = "Teacher_UpdateInfo";
// $CurrentPageName = $iPort['menu']['update_info_eclass_websams'];
//$CurrentPageName = $iPort['menu']['school_records'];
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

$lpf = new libpf_slp();
$luser = new libuser($UserID);

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

########################################################
# Tab Menu : Start
########################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_SCHOOL_RECORD);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

############  Cotnent Here ######################

$ButtonClass = "class=\"formsubbutton\" onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\"";

# if there is plugin of eDiscipline
$CommentUpdate = ($plugin['Discipline']) ? "<input name='button' type=button value='".$button_update."' onClick=\"doUpdateFromIP('comment')\" $ButtonClass>" : "&nbsp;";

$PreviousClass = "class=\"tablerow2\"";
# merit row
if(strstr($ck_user_rights_ext, "merit"))
{
	$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";
	
	$MeritRow = "<tr valign=\"middle\" ".$PreviousClass.">
                <td nowrap='nowrap'>".$ec_iPortfolio['merit']."</td>
                <td align=center><input onClick=\"doUpdateFromIP('merit')\" name=\"button\" type=\"button\" value=\"".$button_update."\" $ButtonClass>
                </td>
				<td align=center>".
				($sys_custom['iPf']['SchoolRecord']['ImportWebsams']['ANPActivityComment'] ? "<input onClick=\"doImport('merit')\" type=button value=\"".$button_import."\" $ButtonClass>":"&nbsp;")."</td>
								<td align=center><input onClick=\"doExport('merit')\" type=button value=\"".$button_export."\" $ButtonClass></td>
								<td align=center><input onClick=\"doRemove('merit')\" type=button value=\"".$button_erase."\" $ButtonClass></td>
                <td align=center nowrap='nowrap'>".$lpf->getMeritLastInputDate()."</td>
              </tr>";
}

# assessment row
if(strstr($ck_user_rights_ext, "assessment_report"))
{
	$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";

	$UpdateBtn = ($plugin['ReportCard']) ? "<input name=\"button\" type=button value=\"".$button_update."\" onClick=\"doUpdateFromIP('assessment')\" $ButtonClass>" : "&nbsp;";

	$AssessmentRow = "<tr valign=\"middle\" ".$PreviousClass.">
                <td nowrap='nowrap'>".$ec_iPortfolio['assessment_report']."</td>
                <td align=center><!--".$UpdateBtn."--></td>
                <td align=center><input onClick=\"doImport('assessment')\" type=button value=\"".$button_import."\" $ButtonClass></td>
				{$ReportCardButtonCell}
				<td align=center><input onClick=\"doExport('assessment')\" type=button value=\"".$button_export."\" $ButtonClass></td>
				<td align=center><input onClick=\"doRemove('assessment')\" type=button value=\"".$button_erase."\" $ButtonClass></td>
                <td align=center nowrap='nowrap'>".$lpf->getAssessmentLastInputDate()."</td>
              </tr>";
}

# activity row
if(strstr($ck_user_rights_ext, "activity"))
{
	$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";

	$ActivityRow = "<tr valign=\"middle\" ".$PreviousClass.">
                <td nowrap='nowrap'>".$ec_iPortfolio['activity']."</td>
                <td align=center><input name=\"button\" type=button value=\"".$button_update."\" onClick=\"doUpdateFromIP('activity')\" $ButtonClass></td>
                <td align=center>".
				($sys_custom['iPf']['SchoolRecord']['ImportWebsams']['ANPActivityComment'] ? "<input onClick=\"doImport('activity')\" type=button value=\"".$button_import."\" $ButtonClass>":"&nbsp;")."</td>
        <td align=center><input onClick=\"doExport('activity')\" type=button value=\"".$button_export."\" $ButtonClass></td>
				<td align=center><input onClick=\"doRemove('activity')\" type=button value=\"".$button_erase."\" $ButtonClass></td>
                <td align=center nowrap='nowrap'>".$lpf->getActivityLastInputDate()."</td>
              </tr>";
}

# award row
if(strstr($ck_user_rights_ext, "award"))
{
$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";

	$AwardRow = "<tr valign=\"middle\" ".$PreviousClass.">
                <td nowrap='nowrap'>".$ec_iPortfolio['award']."</td>
                <td align=center><input name=\"button\" type=button value=\"".$button_update."\" onClick=\"doUpdateFromIP('award')\" $ButtonClass></td>
                <td align=center>&nbsp;</td>
        <td align=center><input onClick=\"doExport('award')\" type=button value=\"".$button_export."\" $ButtonClass></td>
				<td align=center><input onClick=\"doRemove('award')\" type=button value=\"".$button_erase."\" $ButtonClass></td>
                <td align=center nowrap='nowrap'>".$lpf->getAwardLastInputDate()."</td>
              </tr>";
}

# teacher comment row
if(strstr($ck_user_rights_ext, "teacher_comment"))
{
$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";

	$TeacherCommentRow = " <tr valign=\"middle\" ".$PreviousClass.">
                <td nowrap='nowrap'>".$ec_iPortfolio['teacher_comment']."</td>
                <td align=center>".$CommentUpdate."</td>
                <td align=center><input onClick=\"doImport('comment".($sys_custom['iPf']['SchoolRecord']['ImportWebsams']['ANPActivityComment'] ?"2":"")."')\" name=\"button\" type=button value=\"".$button_import."\" $ButtonClass></td>
        <td align=center><input onClick=\"doExport('comment')\" name=\"button\" type=button value=\"".$button_export."\" $ButtonClass></td>
				<td align=center><input onClick=\"doRemove('comment')\" type=button value=\"".$button_erase."\" $ButtonClass></td>
                <td align=center nowrap='nowrap'>".$lpf->getCommentLastInputDate()."</td>
              </tr>";
}

# attendance row
if(strstr($ck_user_rights_ext, "attendance"))
{
$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";

	$AttendanceRow = "<tr valign=\"middle\" ".$PreviousClass.">
                <td nowrap='nowrap'>".$ec_iPortfolio['attendance']."</td>
                <td align=center><input name=\"button\" type=button onClick=\"doUpdateFromIP('attendance')\" value=\"".$button_update."\" $ButtonClass>
                <td align=center>&nbsp;</td>
        <td align=center><input onClick=\"doExport('attendance')\" type=button value=\"".$button_export."\" $ButtonClass></td>
				<td align=center><input onClick=\"doRemove('attendance')\" type=button value=\"".$button_erase."\" $ButtonClass></td>
                <td align=center nowrap='nowrap'>".$lpf->getAttendanceLastInputDate()."</td>
              </tr>	";
}

# service row
if(strstr($ck_user_rights_ext, "service"))
{
$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";

	$ServiceRow = "<tr valign=\"middle\" ".$PreviousClass.">
                <td nowrap='nowrap'>".$ec_iPortfolio['service']."</td>
                <td align=center><input name=\"button\" type=button onClick=\"doUpdateFromIP('service')\" value=\"".$button_update."\" $ButtonClass>
                <td align=center>&nbsp;</td>
        <td align=center><input onClick=\"doExport('service')\" type=button value=\"".$button_export."\" $ButtonClass></td>
				<td align=center><input onClick=\"doRemove('service')\" type=button value=\"".$button_erase."\" $ButtonClass></td>
                <td align=center nowrap='nowrap'>".$lpf->getServiceLastInputDate()."</td>
              </tr>";
}

# olr row
if(strstr($ck_user_rights_ext, "ole"))
{
$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";

	$OLERow = "<tr valign=\"middle\" ".$PreviousClass.">
                <td nowrap='nowrap'>".$ec_iPortfolio['ole']."</td>
                <td align=center>&nbsp;</td>
                <td align=center>&nbsp;</td>
                <td align=center><input onClick=\"doExport('ole')\" type=button value=\"".$button_export."\" $ButtonClass></td>
                <td align=center>&nbsp;</td>
                <td align=center>".$lpf->getOLELastInputDate()."</td>
              </tr>";
}

# heading intro
$IntroMsg = "<p align='center'><span>".$ec_iPortfolio['SAMS_import_intro']."</span></p>";
# Table Open
$TableOpen =  "<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"1\">";
# Table Close
$TableClose = "</table>";
# Note
$Note = "<br /><span style='color: red;'>*</span>".$ec_iPortfolio['Record_Approval_Note']."</span>";
# Form Open
$FormOpen = "<FORM method=\"POST\" name=\"form1\">";
# Form Close
$FormClose = "</FORM>";

# Header Row
$HeaderRow = "<tr class=\"tabletop\">";
$HeaderRow .= "<td >&nbsp;</td>";
$HeaderRow .= "<td nowrap='nowrap' align=\"center\" class=\"tabletopnolink\">&nbsp;".$ec_iPortfolio['eClass_update']."<span style='color: red;'>*</span>&nbsp;</td>";
$HeaderRow .= "<td nowrap='nowrap' align=\"center\" class=\"tabletopnolink\">&nbsp;".$ec_iPortfolio['SAMS_import']."&nbsp;</td>";
$HeaderRow .= "<td nowrap='nowrap' align=\"center\" class=\"tabletopnolink\">&nbsp;".$iPort['menu']['data_export']."&nbsp;</td>";
$HeaderRow .= "<td nowrap='nowrap' align=\"center\" class=\"tabletopnolink\">&nbsp;".$ec_iPortfolio['erase_info']."&nbsp;</td>";
$HeaderRow .= "<td nowrap='nowrap' align=\"center\" class=\"tabletopnolink\">&nbsp;".$ec_iPortfolio['SAMS_last_record_change']."&nbsp;</td>";
$HeaderRow .= "</tr>";

#Student info row
$StudentRow = "<tr valign=\"middle\" class=\"tablerow1\">";
$StudentRow .= "<td nowrap='nowrap' class=\"tabletext\">".$ec_iPortfolio['student_info']."</td>";
$StudentRow .= "<td align=center class=\"tabletext\">&nbsp;</td>";
$StudentRow .= "<td align=center class=\"tabletext\"><input onClick=\"doImport('student_data')\" type=button value=\"".$button_import."\" $ButtonClass></td>";
$StudentRow .= "<td align=center class=\"tabletext\">&nbsp;</td>";
$StudentRow .= "<td align=center class=\"tabletext\">&nbsp;</td>";
$StudentRow .= "<td align=center nowrap='nowrap' class=\"tabletext\">".$lpf->getStudentInfoLastInputDate()."</td>";
$StudentRow .= "</tr>";

#Guardian Row
$GuardianRow = "<tr valign=\"middle\" class=\"tablerow2\">";
$GuardianRow .= "<td nowrap='nowrap' class=\"tabletext\">".$ec_iPortfolio['guardian_info']."</td>";
$GuardianRow .= "<td class=\"tabletext\">&nbsp;</td>";
$GuardianRow .= "<td align=center class=\"tabletext\"><input onClick=\"doImport('guardian')\" name=\"button\" type=button value=\"".$button_import."\" $ButtonClass></td>";
$GuardianRow .= "<td align=center class=\"tabletext\">&nbsp;</td>";
$GuardianRow .= "<td align=center class=\"tabletext\">&nbsp;</td>";
$GuardianRow .= "<td align=center nowrap='nowrap' class=\"tabletext\">".$lpf->getGuardianLastInputDate()."</td>";
$GuardianRow .= "</tr>";

$Content = "";
$Content .= $IntroMsg;
$Content .= $FormOpen;
$Content .= $TableOpen;
// row shown
$Content .= $HeaderRow;
//$Content .= $StudentRow.$GuardianRow;
$Content .= $MeritRow;
$Content .= $AssessmentRow;
$Content .= $ActivityRow;
$Content .= $AwardRow;
$Content .= $TeacherCommentRow;
$Content .= $AttendanceRow;
$Content .= $ServiceRow;
//$Content .= $OLERow;

$Content .= $TableClose;
$Content .= $Note;
$Content .= $FormClose;
############  END Cotnent Here ######################

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
function doImport(my_type){
	var winType;
	if ((my_type == "merit") || (my_type == "activity") || (my_type == "comment2")) {
		winType = 93;
	}
	else {
		winType = 27;
	}
	if (my_type == "comment2") {
		newWindow("../../profile/comment/import_sams2.php", winType);	
	}
	else {
		newWindow("../../profile/"+my_type+"/import_sams.php", winType);	
	}	
}
function doUpdateFromIP(my_type){
	newWindow("../../profile/"+my_type+"/data_syn.php", 27);
}
function doRemove(my_type){
	newWindow("../../profile/"+my_type+"/data_remove.php", 27);
}
function doExport(my_type){
	newWindow("../../profile/"+my_type+"/data_export.php", 27);
}
</SCRIPT>

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="96%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<?=$Content?>
</td></tr></table>
</td></tr></table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
