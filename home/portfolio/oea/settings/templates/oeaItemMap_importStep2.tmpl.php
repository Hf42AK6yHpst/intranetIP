<?php
echo $h_TopTabMenu;
?>
<script language="javascript">
$('document').ready(function () {
	Block_Document('<?=$h_ProgressMsg?>');
});

function js_Go_Back()
{
	window.location = 'index.php?task=oeaItemMap_importStep1';
}

function js_Cancel()
{
	window.location = 'index.php?task=oeaItemMap';
}

function js_Continue()
{
	$('input#task').val('oeaItemMap_importStep3');
	$('form#form1').submit();
}
</script>
<form method="POST" name="form1" id="form1" action="index.php" enctype="multipart/form-data">
	<?=$h_Navigation?>
	<br style="clear:both;" />
	<?=$h_StepTable?>
	<div class="table_board">
		<table id="html_body_frame" style="width:100%;">
			<tr>
				<td>
					<?=$h_overrideStudentOeaWarning?>
					<br style="clear:both;" />
					
					<table class="form_table_v30">
						<tr>
							<td class="field_title"><?=$Lang['General']['SuccessfulRecord']?></td>
							<td><div id="SuccessCountDiv"><?=$Lang['General']['EmptySymbol']?></div></td>
						</tr>
						<tr>
							<td class="field_title"><?=$Lang['General']['FailureRecord']?></td>
							<td><div id="FailCountDiv"><?=$Lang['General']['EmptySymbol']?></div></td>
						</tr>
					</table>
					<br style="clear:both;" />
					
					<div id="ErrorTableDiv"></div>
				</td>
			</tr>
		</table>
	</div>
	
	<div class="edit_bottom_v30">
		<?=$ContinueBtn?>
		<?=$BackBtn?>
		<?=$CancelBtn?>
	</div>
	
	<iframe id="ImportIFrame" name="ImportIFrame" src="<?=$h_iFrameSrc?>" style="width:100%;height:50px;border:0px;display:none;"></iframe>
	
	<input type="hidden" id="task" name="task" value="" />
	
	<!-- For Step 3 use -->
	<input type="hidden" id="numOfCsvData" name="numOfCsvData" value="<?=$numOfCsvData?>" />
	<input type="hidden" id="OverrideStudentOea" name="OverrideStudentOea" value="<?=$OverrideStudentOea?>" />
</form>