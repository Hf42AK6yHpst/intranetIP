<?
echo $h_Include_Js_Css;
echo $h_TopTabMenu;
?>
<script>
var arrCookies = new Array();
arrCookies[arrCookies.length] = "Keyword";
arrCookies[arrCookies.length] = "AcademicYearID";
arrCookies[arrCookies.length] = "ProgramNature";
arrCookies[arrCookies.length] = "MappingStatus";

var jsField = '<?=$field?>';
var jsOrder = '<?=$order?>';
var jsPageNo = '<?=$pageNo?>';
var jsCurKeyword = '<?=$Keyword?>';
var jsCurAcademicYearID = '<?=$AcademicYearID?>';
var jsCurProgramNature = '<?=$ProgramNature?>';
var jsCurMappingStatus = '<?=$MappingStatus?>';

$(document).ready( function() {	
	<? if($clearCoo) { ?>
		for(i=0; i<arrCookies.length; i++)
		{
			var obj = arrCookies[i];
			//alert('obj = ' + obj);
			$.cookies.del(obj);
		}
	<? } ?>
	
	Blind_Cookies_To_Object();
	js_Reload_DB_Table(1);
	$('input#Keyword').focus();
});

function js_Go_Import()
{
	window.location = 'index.php?task=oeaItemMap_importStep1';
}

function js_Go_Export()
{
	$('input#task').val('oeaItemMap_export');
	$('form#form1').submit();
	js_Reset_Page_Task();
}

function js_Reset_Page_Task()
{
	$('input#task').val('oeaItemMap');
}

function js_Reload_DB_Table(jsInitialize)
{
	if (jsInitialize == 1) {	// if not the first load of the table, 
		// follow current setting => do nth		
	}
	else {
		// not intialize => changed filtering => start from first page
		jsPageNo = 1;
	}
	
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"index.php", 
		{
			task: 'ajax',
			script: 'ajax_reload',
			action: 'Settings_OLE_Mapping_DB_Table',
			field: jsField,
			order: jsOrder,
			pageNo: jsPageNo,
			Keyword: jsCurKeyword,
			AcademicYearID: jsCurAcademicYearID,
			ProgramNature: jsCurProgramNature,
			MappingStatus: jsCurMappingStatus
		},
		function(ReturnData)
		{
			//SetCookies();	// save the filter values in cookies
			initThickBox();
		}
	);
}

function js_Changed_Keyword(jsValue, e)
{
	if (Check_Pressed_Enter(e) == true) {
		jsCurKeyword = jsValue;
		js_Reload_DB_Table();
	}
}

function js_Changed_Filtering(jsFilterID, jsValue)
{
	eval('jsCur' + jsFilterID + ' = "' + jsValue + '";');
	js_Reload_DB_Table();
}






function js_edit_MappingAction(p_id) {
	$('input#curOleProgramId').val(p_id);
	$.post(
		"index.php", 
		{ 
			r_programid: p_id,
			IsForOLEEdit: 1,
			fromJupasMappingSettings: 1,
			task: 'ajax',
			script: 'ajax_edit_oea_mapping'
			 },
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);
			//js_Onkeyup_Info_Textarea('Description', 'DescWordCountSpan', '0', '300');
			//document.getElementById('tempDisplay').innerHTML = ReturnData;	
		}
	);	
}

function js_show_layer(layername) { //duplicate source from "task=listStuAllInfo"
	$('#'+layername).show('slow');
}

function loadNextPage(layername, jsLoadContent) { // duplicate source from "task=listStuAllInfo"
	var content = (jsLoadContent=="advanceSearch") ? "ajax_advance_search" : "ajax_get_suggestion_list";
	//var ole_title = $('input#ole_title').val();	// dynamic for this page. so, pass the programid to ajax and get the name in ajax
	
	$.post(
		"index.php", 
		{ 
			task: 'ajax',
			script: content,
			oleProgramId: $('input#curOleProgramId').val()
			
			 },
		function(ReturnData)
		{
			$('div#'+layername).html(ReturnData);	
			$('div#'+layername).show();
			$('div#TableDiv').hide();
			$('div#buttonDiv').hide();
		}
	);		
}
/*
function copyback(code) { // duplicate source from "task=listStuAllInfo"

	$('input#ItemCode').val(code);

	checkOEAcategory(code);	

	document.getElementById('selectedItemSpan').innerHTML = "<b><label for='ItemCode_"+code+"'>"+document.getElementById('hiddenItemName').value+"</label></b>";
	loadPreviousPage();

//	if (code == '00000') {
//		$('textarea#Description').attr('readonly', '');
//	}
//	else {
//		$('textarea#Description').attr('readonly', 'readonly');
//	}
}
*/
function checkOEAcategory(itemCode) { //duplicate source from "home/portfolio/teacher/management/ole_new.php"

	document.getElementById('span_OEA_Category').innerHTML = itemAry[itemCode][1];

	document.getElementById('CatCode').value =  itemAry[itemCode][0];

}
function Show_Advance_Search() // duplicate source from "home/portfolio/teacher/management/ole_new.php"
{
	var currentRecordID = $('input#currentRecordID').val();
	var ole_title = $('input#englishTitle').val();
	$.post(
		"../../oea/student/index.php", 
		{ 
			currentRecordID: currentRecordID,
			IsForOLEEdit: 0,
			ole_title: ole_title,
			task: "ajax",
			script: "ajax_advance_search"
			 },
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);
		}
	);
}
function CopyOthers() { 
	$.post(
		"index.php", 
		{ 
			task: "ajax",
			script: "ajax_getOleProgramName",
			oleProgramId: $('input#curOleProgramId').val()
		},
		function(ReturnData) {
			document.getElementById('selectedItemSpan').innerHTML = "<b>" + ReturnData + "</b>";
			$('input#ItemCode').val('<?=$oea_cfg["OEA_Default_Category_Others"]?>');
			checkOEAcategory('<?=$oea_cfg["OEA_Default_Category_Others"]?>');
			$('#MapOptions').hide('slow');
			
			//$('textarea#Description').attr('readonly', '');
		}
	);
}


function copyback(code) {	// duplicate source from "task=listStuAllInfo"
	$('input#ItemCode').val(code);
	checkOEAcategory(code);	
	
	document.getElementById('selectedItemSpan').innerHTML = "<b><label for='ItemCode_"+code+"'>"+document.getElementById('hiddenItemName').value+"</label></b>";
	loadPreviousPage();
	
//	if (code == '<?=$oea_cfg["OEA_Default_Category_Others"]?>') {
//		$('textarea#Description').attr('readonly', '');
//	}
//	else {
//		$('textarea#Description').attr('readonly', 'readonly');
//	}
}

function loadPreviousPage() {	// duplicate source from "task=listStuAllInfo"
	$('div#SearchDiv').hide();
	$('div#TableDiv').show();
	$('div#buttonDiv').show();
	$('#MapOptions').hide('slow');
}

function resetAdvanceLayerDisplay() {	// duplicate source from "task=listStuAllInfo"
	$('input#AdvanceLayerDisplay').val(0);
}

function doSave() {
	var canSubmit = true;
	
	// Get the value in the tickbox as the thickbox is not in "form1"
	var targetOeaProgramCode = Trim($('input#ItemCode').val());
	if (targetOeaProgramCode == '') {
		alert('<?=$Lang['iPortfolio']['OEA']['jsWarning']['ChooseOeaProgram']?>');
		canSubmit = false;
		return false;
	}
	
	if (canSubmit) {
		// set oea program code
		$('input#hiddenOeaProgramCode').val(targetOeaProgramCode);
		
		// set award bearing
		var awardBearing = $('input#awardBearing1').attr('checked')? '<?=$oea_cfg["OEA_AwardBearing_Type_Yes"]?>' : '<?=$oea_cfg["OEA_AwardBearing_Type_No"]?>'; 
		$('input#hiddenOeaAwardBearing').val(awardBearing);
		
		// set override student data settings
		var overrideStudentVal = ($('input#overrideStudentOeaChk').attr('checked'))? 1 : 0;
		$('input#hiddenOverrideStudent').val(overrideStudentVal);
		
		// form submit
		$('input#task').val('oeaItemMap_update');
		$('form#form1').submit();
	}
}

function doDelete() {
	if (confirm('<?=$Lang['iPortfolio']['OEA']['jsWarning']['DeleteMapping']?>')) {
		$('input#task').val('oeaItemMap_delete');
		$('form#form1').submit();
	}
}

function triggerOverrideWarning(isOverride) {
	if (isOverride) {
		$('div#overrideStudentRecordWarningDiv').show();
	}
	else {
		$('div#overrideStudentRecordWarningDiv').hide();
	}
}

<?=$h_js?>

</script>
<form id="form1" name="form1" method="post" action="index.php" onsubmit="return false;">
	<table id="html_body_frame" width="100%" cellpadding="3">
		<tr>
			<td colspan="2">
				<div class="content_top_tool">
					<div class="Conntent_tool">
						<?=$btn_Import?>
						<!--<?=$btn_Export?>-->
					</div>
					<?=$SearchboxDiv?>
				</div>	
			</td>
		</tr>
		<tr>
			<td>
				<div class="table_filter"><?=$h_AcademicYearSelection?></div>
				<div class="table_filter"><?=$h_ProgramNatureSelection?></div>
				<div class="table_filter"><?=$h_MappingStatusSelection?></div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div id="DBTableDiv"></div>
			</td>
		</tr>
	</table>
		
	<input type="hidden" id="task" name="task" value="oeaItemMap" />
	<input type="hidden" id="curOleProgramId" name="curOleProgramId" value="" />
	<input type="hidden" id="hiddenOeaProgramCode" name="oeaProgramCode" value="" />
	<input type="hidden" id="hiddenOeaAwardBearing" name="oeaAwardBearing" value="" />
	<input type="hidden" id="hiddenOverrideStudent" name="overrideStudentOea" value="" />
</form>	