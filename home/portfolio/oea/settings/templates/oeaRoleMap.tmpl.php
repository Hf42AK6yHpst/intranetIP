<?
// using: connie



echo $h_Include_Js_Css;
echo $h_TopTabMenu;
?>
<script>

$(document).ready(function(){

	$('input#Keyword').focus().keyup(function(e){
			var keynum = Get_KeyNum(e);
			
			if (keynum==13) // keynum==13 => Enter
				js_Reload_Role_Selection();
	});
	
	js_Reload_selectedRole();		
	js_Reload_Role_Selection();
	
	
	$("#SortBySelect").change(function () {
		
		 var Sortby= $('#SortBySelect option:selected').val();		
	  	js_Reorder_Selection_List('RoleSelSel',Sortby);
	  	
	}).change();
	
	
	var Sortby= $('#SortBySelect option:selected').val();
	js_Reorder_Selection_List('RoleSelSel',Sortby);	
	
	js_Number_changed();

    
});

function js_Number_changed()
{
	
	//the left box
	var length = $('select#RoleSelSel option').length;	  	
	$('#RolSelNoID').text('<?=$Lang['iPortfolio']['OEA']['roleMapArr']['NoOfRole']?>'+': '+ length);
	
	
	//the right boxes
	
	//pass the php array into js array (Role Type Array)
	var OEA_Role_Key_Array = ["<?= join("\", \"", $OEA_RoleTypeKeyArr); ?>"];
	var jsNumOfRole = OEA_Role_Key_Array.length;
	var i;
	
	for (i=0; i<jsNumOfRole; i++)  
	{
		var thisRoleType = OEA_Role_Key_Array[i];
		var selected_roleID = 'SelectedRoleSel'+'_'+thisRoleType;
		var noID = thisRoleType+"_noID";
	  	
	  	var length = $('select#'+selected_roleID+' option').length;
	  	
	  	$('#'+noID).text('<?=$Lang['iPortfolio']['OEA']['roleMapArr']['NoOfRole']?>'+': '+ length);
	}
	
}

function js_Reload_Role_Selection() {
	
	// Change to reload image for the role selection
	$('#RoleSelDivID','form#form1').html('<?=$linterface->Get_Ajax_Loading_Image()?>');


	//Get search box text
	jsKeyword = Trim($('input#Keyword').val());


	//pass the php array into js array (Role Type Array)
	var OEA_Role_Key_Array = ["<?= join("\", \"", $OEA_RoleTypeKeyArr); ?>"];
	var jsNumOfRole = OEA_Role_Key_Array.length;
	var i;
	
	// Get the selected roles
	var ExcludeRolePara = '';
	
	for (i=0; i<jsNumOfRole; i++)     //for(var key in OEA_Role_Key_Array)	
	{
		var selected_roleID = 'SelectedRoleSel'+'_'+OEA_Role_Key_Array[i];
	  	$("select#"+selected_roleID+" option").each(function(i){
	  		var thisValue = $(this).val();
	  		var thisValueArr = thisValue.split(":__:");
	  		var thisRole = thisValueArr[0];
	  		encodeURIComponent
			ExcludeRolePara += "&ExcludeRoleArr[]=" + encodeURIComponent(thisRole);
		});
	}

		
	// Prepare ajax data
	
	var url = "index.php";
	var postContent = "task=ajax&script=ajax_oeaRoleMap_handler&Action=reload_selected_role";
	postContent += "&Action=reload_role_selection";
	postContent += ExcludeRolePara;
	postContent += "&SearchRole=" +jsKeyword;
	
	//alert('here1');
	// Call ajax to reload role selection
	$.ajax({
		async: false,
		type: "POST",
    	url: url,
    	data: postContent,
    	success: function(data) {
    		//alert('here2');
    		$('#RoleSelDivID').html(data);
			$('#RoleSelSel').attr({size: '<?=$RoleSelSel_size?>'});
			$("#RoleSelSel").width($("#RoleSelSel").parent().width());
			var Sortby= $('#SortBySelect option:selected').val();
			js_Reorder_Selection_List('RoleSelSel',Sortby);			
			
			js_Number_changed();
      	}
	});
}

function js_Reload_selectedRole() {
	
	//pass the php array into js array (Role Type Array)
	var OEA_Role_Key_Array = ["<?= join("\", \"", $OEA_RoleTypeKeyArr) ?>"];
	var jsNumOfRole = OEA_Role_Key_Array.length;
	var i;
	
	for (i=0; i<jsNumOfRole; i++)
	{
		var thisRoleType = OEA_Role_Key_Array[i];
		var selected_roleID = thisRoleType+"_divID";
		
		var selectSelID ='SelectedRoleSel_'+thisRoleType;
	
		$('#'+selected_roleID).html('<?=$linterface->Get_Ajax_Loading_Image()?>');
		
		// Prepare ajax data
		var url = "index.php";
		var postContent = "task=ajax&script=ajax_oeaRoleMap_handler&Action=reload_selected_role";
		postContent += "&Action=reload_selected_role";
		postContent += "&RoleType=" +thisRoleType;


		// Call ajax to reload role selection
		$.ajax({
			async: false,
			type: "POST",
	    	url: url,
	    	data: postContent,
	    	success: function(data) {
				$('#'+selected_roleID).html(data);
				var Sortby= $('#SortBySelect option:selected').val();
				js_Reorder_Selection_List(selectSelID,Sortby);	
				js_Number_changed();
	      	}
		});
	
	}
	
}


function js_button_functions(indicator,SelectedRoleSel)
{
	
	if(indicator=='AddAll')
	{
		$("select#RoleSelSel option").each(function(){$(this).attr("selected","selected");}); 
		MoveSelectedOptions("RoleSelSel", SelectedRoleSel);
	}
	else if(indicator=='Add')
	{
		MoveSelectedOptions("RoleSelSel", SelectedRoleSel);
		
	}
	else if(indicator=='Remove')
	{
		MoveSelectedOptions(SelectedRoleSel, "RoleSelSel");
		
	}
	else if(indicator=='RemoveAll')
	{
		$("#"+SelectedRoleSel+" option").each(function(){$(this).attr("selected","selected");});
    	MoveSelectedOptions(SelectedRoleSel, "RoleSelSel");
	}
	$("#"+SelectedRoleSel).width($("#"+SelectedRoleSel).parent().width());
	
	js_Number_changed();
}


function MoveSelectedOptions(from_select_id, to_select_id)
{
	$("#"+from_select_id+" option:selected").each(function(i){
    	var role_id = $(this).val();
    	var role_name = $(this).text();
	
    	$("#"+to_select_id).append($("<option></option>").attr("value",role_id).text(role_name));
    	$(this).remove();
	});
  
	$("#"+from_select_id).width($("#"+from_select_id).parent().width());
	$("#"+to_select_id).width($("#"+to_select_id).parent().width());
	 
	var Sortby= $('#SortBySelect option:selected').val();

	js_Reorder_Selection_List(from_select_id,Sortby);
	js_Reorder_Selection_List(to_select_id,Sortby);
	
}

function js_Reorder_Selection_List(selectId,indicator) {
 
	var selectList = document.getElementById(selectId);

	for (var i = 0; i < selectList.length; i++) {
		for (var j=i; j < selectList.length; j++) {
			
			var i_value = selectList.options[i].value;
			i_valueArr = i_value.split(":__:");
			
			var j_value = selectList.options[j].value;
			j_valueArr = j_value.split(":__:");	
			
			var i_role = 1;
			var j_role = 1;
			var compareBool = false;
			
			if(indicator=='number')
			{
				i_role = parseInt(i_valueArr[1]);
				j_role =parseInt(j_valueArr[1]);
				compareBool = i_role < j_role;
				
			}
			else if(indicator=='title')
			{
				i_role = i_valueArr[0];						
				j_role =j_valueArr[0];
				compareBool = i_role > j_role;
			}
			

			if (compareBool) {
				var tempOption1 = new Option(selectList.options[i].text,selectList.options[i].value);
				var tempOption2 = new Option(selectList.options[j].text,selectList.options[j].value);
				selectList.options[i] = tempOption2;
				selectList.options[j] = tempOption1;
			}
		}
  }
}


function js_Save() {
	
	//pass the php array into js array (Role Type Array)
	var OEA_Role_Key_Array = ["<?= join("\", \"", $OEA_RoleTypeKeyArr); ?>"];
	var jsNumOfRole = OEA_Role_Key_Array.length;
	var i;

	var InsertRolePara ='';

	//for(var key in OEA_Role_Key_Array)
	
	for (i=0; i<jsNumOfRole; i++)
	{				
		var thisRoleType = OEA_Role_Key_Array[i];
				
		var selected_roleID = 'SelectedRoleSel'+'_'+thisRoleType;
	
		// Get the selected roles
	  	$("select#"+selected_roleID+" option").each(function(i){
	  		var thisValue = $(this).val();
	  		var roleValue_arr = thisValue.split(":__:");	
			
			var insertData = roleValue_arr[0]+":__:"+thisRoleType;
			InsertRolePara += "&InsertRoleArr[]=" + encodeURIComponent(insertData);			

		});
	}
	
	
	// Prepare ajax data
	var url = "index.php";
	var postContent = "task=ajax&script=ajax_oeaRoleMap_handler";
	postContent += "&Action=save_action";
	postContent += InsertRolePara;

	// Call ajax to reload role selection
	$.ajax({
		async: false,
		type: "POST",
    	url: url,
    	data: postContent,
    	success: function(data) {
    		
			window.location="index.php?task=oeaRoleMap&msg="+data;
      	}
	});

}



</script>
<form id="form1" name="form1" method="post" action="index.php" onsubmit="return false;">
	 
	
<table class="form_table">


  <tr> 
    <td>
  		<table  border="0" cellspacing="0" cellpadding="0">


  			<tr> 				
  				<td  rowspan='<?=$count_OEA_RoleTypeArr?>' bgcolor="#EEEEEE">
					<table bgcolor="#EEEEEE">
						<tr>	
							<td style="text-align:left"><?=$Lang['iPortfolio']['OEA']['SortedBy']?>:</td>		
							<td>
								<select id="SortBySelect">
									<option value='title' selected="yes"><?=$Lang['iPortfolio']['OEA']['Role']?></option>
									<option value='number'><?=$Lang['iPortfolio']['OEA']['NoOfUser']?></option>
								</select>
							</td>	
							<td colspan='1' align="right" bgcolor="#EEEEEE"><?=$SearchBox?></td>
						</tr>	
						<tr>
							<td bgcolor="#EEEEEE"><?=$Lang['iPortfolio']['OEA']['Role']?>:</td>			
							<td>&nbsp;</td>			
	  						<td id='RolSelNoID' bgcolor="#EEEEEE" style="text-align:right"></td>
	  					</tr>
	  					<tr>
							<td bgcolor="#EEEEEE" align="center" colspan='3'>
								<div id="RoleSelDivID">						
			  					</div>
			  					
			          		</td>
		          		</tr>
		          		
		          		<tr>
		          			<td bgcolor="#EEEEEE" colspan='3'>
		          				<span class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?> </span>
		          			</td>
		          		</tr>
		          		
	          		</table>
	          	</td>

				<td> &nbsp;&nbsp;
				<td/>
	          	
	          	<td >
	          		<table  border="0" cellspacing="0" cellpadding="0" >
	          			<tr>
	          				<td bgcolor="#FFFFFF">&nbsp;</td>
	          				<td bgcolor="#FFFFFF">&nbsp;</td>
	          				<td bgcolor="#FFFFFF">&nbsp;</td>
	          				<td bgcolor="#EFFEE2"  class="steptitletext"><?=$Lang['iPortfolio']['OEA']['roleMapArr']['RoleSelected']?> </td>
						</tr>
						<tr>
						<?=$html_first_selectedRoleArea?>
						<?=$html_selectedRoleArea?>
						</tr>
						
					</table>
				</td>
				
			</tr>
	
      </table>
      <p class="spacer"></p>
    </td>
  </tr>
</table>



<div class="edit_bottom"> <!-- <span> 更新日期 :兩天前由管理員</span> -->
  <p class="spacer"></p>
    <input type="button" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$iPort["btn"]["save"]?>" onclick="js_Save();" />
    <input type="button" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$iPort["btn"]["cancel"]?>" onclick="window.location='index.php'" />
  <p class="spacer"></p>
</div>

<!--
<input type="hidden" name="stepSet" id="stepSet" />
<input type="hidden" name="trgSchemeID" id="trgSchemeID" value="<?=$html_schemeID?>"/>-->
</form>	