<?php
# modifying : henry chow

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_page_size", "numPerPage");
//$arrCookies[] = array("ck_ipf_oea_teacher_mgmt_YearClassID", "YearClassID");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	//$YearClassID = '';
}
else 
	updateGetCookies($arrCookies);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$liboea = new liboea();


$isSuperAdmin = $lpf->IS_IPF_SUPERADMIN();
$returnTeachingClassOnly = ($isSuperAdmin)? 0 : 1;

### Import Icon
$h_ImportIcon = $linterface->Get_Content_Tool_v30('import', 'javascript:js_Go_Import();');

### Export Icon
$h_ExportIcon = $linterface->Get_Content_Tool_v30('export', 'javascript:js_Go_Export();');

### Int / Ext Type Selection
$h_IntExtTypeSelection = '';


### DB Table
$field = ($field=="")? 0 : $field;
$order = ($order=="")? 1 : $order;
$pageNo = ($pageNo=="")? 0 : $pageNo;
$li = new libdbtable2007($field, $order, $pageNo);

$DBTableInfoArr = $liboea->getSettingsOleOeaMappingTableSql();
$li->sql = $DBTableInfoArr['DBTableSql'];
$li->field_array = $DBTableInfoArr['DBTableFieldArr'];
$li->no_col = 4;
$li->IsColOff = 'IP25_table';

//debug_pr($li->built_sql());

# TABLE COLUMN
$pos = 0;
$li->column_list .= "<th width='1'>#</th>\n";
$li->column_list .= "<th width='".$ClassWidth."%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['SysMgr']['FormClassMapping']['Class'])."</th>\n";
$li->column_list .= "<th width='".$ClassNumberWidth."%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['SysMgr']['FormClassMapping']['ClassNo'])."</th>\n";
$li->column_list .= "<th width='".$StudentNameWidth."%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['SysMgr']['FormClassMapping']['StudentName'])."</th>\n";
if ($showOEAItem) {
	$li->column_list .= "<th width='".$OEAColumnWidth."%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['iPortfolio']['OEA']['OEA_Name'])."</th>\n";
}
if ($showAddiInfo) {
	$li->column_list .= "<th width='".$OEAColumnWidth."%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['iPortfolio']['OEA']['AdditionalInformation'])."</th>\n";
}
if ($showAbility) {
	$li->column_list .= "<th width='".$OEAColumnWidth."%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['iPortfolio']['OEA']['PersonalAndGeneralAbility'])."</th>\n";
}
if ($showAcademic) {
	$li->column_list .= "<th width='".$OEAColumnWidth."%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['iPortfolio']['OEA']['AcademicPerformance'])."</th>\n";
}
if ($showSuppInfo) {
	$li->column_list .= "<th width='".$OEAColumnWidth."%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['iPortfolio']['OEA']['SupplementaryInformationFromPrincipal'])."</th>\n";
}
//$li->column_list .= "<th width='1' class='tabletop tabletoplink'>".$li->check("programID[]")."</th>\n";

$h_dbtable .= $li->display();


$linterface->LAYOUT_START();
include_once($template_script);
$linterface->LAYOUT_STOP();
?>