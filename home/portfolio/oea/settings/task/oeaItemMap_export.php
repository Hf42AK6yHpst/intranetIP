<?php


# modifying : 

/******************************************************************************
 * Modification log:
 * 
 * Yuen (2011-12-02):
 * 		- in order to not the occupy CPU too long, use for a while to free CPU for other operations
 * 
 * Yuen (2011-11-15): 
 * 		- provide suggestions in column F
 * 
 * 
 *****************************************************************************/

@SET_TIME_LIMIT(216000);
@ini_set(memory_limit, -1);

StartTimer();

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$FromImport = $_REQUEST['FromImport'];
$OLENatureArr = $_REQUEST['OLENatureArr'];
$MappingStatusArr = $_REQUEST['MappingStatusArr'];
$AcademicYearID = $_REQUEST['AcademicYearID'];
$Keyword = $_REQUEST['Keyword'];
$field = $_REQUEST['field'];
$order = $_REQUEST['order'];
$FormIDArr = $_REQUEST['FormIDArr'];
$WithAuggestions = true;

$liboea = new liboea();
$lexport = new libexporttext();

$ShowInUIOnly = ($FromImport)? 0 : 1;

### Csv Header
$ExportColumnTitleArr = array();
$ColumnTitleInfoArr = $liboea->getOLEMappingImportColumnInfoArray($ShowInUIOnly);
$numOfColumn = count($ColumnTitleInfoArr);

$ColumnTitleArr = $liboea->getOLEMappingImportColumnTitleArray($ShowInUIOnly);
$ColumnPropertyArr = $liboea->getOLEMappingImportColumnPropertyArray($forGetAllCsvContent=0, $ShowInUIOnly);
$ExportColumnTitleArr = $lexport->GET_EXPORT_HEADER_COLUMN($ColumnTitleArr, $ColumnPropertyArr);


$liboea_setting = new liboea_setting();
$liboea_setting_search = new liboea_setting_search($liboea_setting);


### Csv Content
$ExportDataArr = array();
if ($FromImport) {
	$OLEMappingInfoArr = $liboea->Get_OLE_Program_OEA_Mapping_Info_Array($OLENatureArr, $MappingStatusArr, '', 0, '', '', '', $FormIDArr);
}
else {
	$OLEMappingInfoArr = $liboea->Get_Settings_OLE_Mapping_Index_DBTable($field, $order, $pageNo='', $ForExport=1, $Keyword, $AcademicYearID);
}
$numOfOLE = count($OLEMappingInfoArr);

# dev site
if ($DebugMode)
{
	//$numOfOLE = 500;
}

if ($FromImport)
{
	$Interval = 50;
	
	if ($StartIndex=="")
		$StartIndex = 0;
	
	$NextStop = $StartIndex + $Interval;
	if ($NextStop>=$numOfOLE)
	{
		$NextStop = $numOfOLE;
	}
} else
{
	$StartIndex = 0;
	$NextStop = $numOfOLE;
}

for ($i=$StartIndex; $i<$NextStop; $i++)
{
	for ($j=0; $j<$numOfColumn; $j++)
	{
		$_TitleDBField = $ColumnTitleInfoArr[$j]['DBField'];
		
		if ($_TitleDBField == 'OEA_AwardBearing')
		{
			$_Value = $OLEMappingInfoArr[$i]['OEA_DefaultAwardBearing'];
		} elseif ($_TitleDBField == 'OEA_ProgramCode')
		{
			$_Value = $OLEMappingInfoArr[$i]['OEA_ProgramCode'];
		} elseif ($_TitleDBField == 'OEA_Suggestions')
		{
			if ($WithAuggestions)
			{			
				$liboea_setting_search->setCriteriaTitle($OLEMappingInfoArr[$i]['OLE_ProgramName']);
				$liboea_setting_search->getSuggestResult();

				$SuggestedArr = $liboea_setting_search->getSearchResultTitle();
				$SuggestedString = "";
				if (sizeof($SuggestedArr)>0)
				{
					for ($sii=0; $sii<sizeof($SuggestedArr); $sii++)
					{
						$SuggestedString .= (($SuggestedString!="") ? ";\n" : "") . $SuggestedArr[$sii][0] . " : ".$SuggestedArr[$sii][1];
					} 
				}
				$_Value =  $SuggestedString;
			}
		} else {
			$_Value = $OLEMappingInfoArr[$i][$_TitleDBField];
		}
		
		if ($FromImport == 0 && $_TitleDBField == 'OLE_ProgramNature') {
			$_Value = $liboea->getParticipationTypeDisplayByCode($_Value);
		}
//		else if ($FromImport) {
//			if ($_TitleDBField=='OEA_ProgramCode' || $_TitleDBField=='OEA_ProgramName' || $_TitleDBField=='OEA_DefaultAwardBearing') {
//				$_Value = '';
//			}
//		}
		
		$ExportDataArr[$i-$StartIndex][] = $_Value;
	}
	
	# in order to not the occupy CPU too long, free CPU for a while for other operations
	if ($i>0 && $i%25==0)
	{
		sleep(1);
	}
	
}

/*
if ($StartIndex>=20)
{
	debug($StartIndex,$NextStop);
	debug_r($ExportDataArr);
}
*/

//($data, $ColumnDef, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=0)
//$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportDataArr, $ExportColumnTitleArr);
$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportDataArr, $ExportColumnTitleArr, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", true);

// yuen: to test
$filename = ($FromImport)? "import_ole_oea_mapping_sample.csv" : "ole_oea_mapping.csv";

# yuen: append to file - secretFileKey

if ($FromImport)
{
	
	$TimeTaken = StopTimer(10, true);
	$recordLeft = $numOfOLE - $NextStop;
	$timeLeft = ($TimeTaken/$Interval)*$recordLeft;

	if ($NextStop>=$numOfOLE)
	{
		$ProgressStatus = "COMPLETE";
	} else
	{
		$ProgressStatus = "ToContinue";
	}
	
	$libfm = new libfilesystem();
	$iport_folder = $PATH_WRT_ROOT."file/iportfolio";
	if (!file_exists($iport_folder))
	{
		# create
		$libfm->folder_new($iport_folder);
	}
	$iport_folder .= "/tmp";
	if (!file_exists($iport_folder))
	{
		# create
		$libfm->folder_new($iport_folder);
	}
	# load the contents
	$csv_file = $iport_folder ."/oea_mapping_tmp_".$secretFileKey.".csv";

	if (file_exists($csv_file))
	{
		$pre_file_content = get_file_content($csv_file);
		$export_content1 = substr($export_content, strpos($export_content, "\r\n")+strlen("\r\n"));
		$export_content = substr($export_content1, strpos($export_content1, "\r\n")+strlen("\r\n"));
		$file_content = $pre_file_content .mb_convert_encoding("\r\n".$export_content,'UTF-16LE','UTF-8');
	} else
	{
		$file_content = mb_convert_encoding($export_content,'UTF-16LE','UTF-8');
		// Add BOM (Byte Order Mark) to identify the file is in UTF-16LE encoding
		$file_content = "\xFF\xFE".$file_content;
	}
	write_file_content($file_content, $csv_file);
	
	if ($NextStop/$numOfOLE>0.05)
	{
		# provide estimation after 5% progress
		$timeLeftMin = ceil($timeLeft / 60);
	}
	//if ()
	
	echo "$ProgressStatus|$numOfOLE|$NextStop|$timeLeftMin";
} else
{
	$lexport->EXPORT_FILE($filename, $export_content);
}
?>