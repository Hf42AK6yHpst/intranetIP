<?php
$liboea = new liboea();
$liboea_academic = new liboea_academic();

$h_TopTabMenu = $LibPortfolio->GET_TAB_MENU(libpf_tabmenu::getJupasAdminAcademicSettingsTags('ADMIN_SETTING_ACADEMIC', $task));

$h_Toolbar = $linterface->Get_Content_Tool_v30("export", "javascript:js_Go_Export();", $Lang['iPortfolio']['OEA']['AcademicArr']['ExportForJUPAS']);

$h_SubjectJupasStatusSelection = $liboea_academic->Get_Subject_Jupas_Status_Selection('SubjectJupasStatusSel', 'SubjectJupasStatus', '', 'js_Changed_Subject_Jupas_Status_Selection(this.value);');

$SaveBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "js_Save_Settings();", $id="Btn_Save");


$linterface->LAYOUT_START();
include_once($template_script);
$linterface->LAYOUT_STOP();
?>