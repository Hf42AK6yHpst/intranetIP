<?
include_once($PATH_WRT_ROOT.'includes/portfolio25/slp/ole_program.php');

$oleProgramId = $_POST['curOleProgramId'];
$oeaProgramCode = $_POST['oeaProgramCode'];
$oeaAwardBearing = $_POST['oeaAwardBearing'];
$overrideStudentOea = $_POST['overrideStudentOea'];

$liboea = new liboea();
$objOleProgram = new ole_program($oleProgramId);

if (strtoupper($objOleProgram->getIntExt()) == strtoupper($ipf_cfg["OLE_TYPE_STR"]["INT"])) {
	$oeaParticipation = $oea_cfg["OEA_Participation_Type_School"];
}
else if (strtoupper($objOleProgram->getIntExt()) == strtoupper($ipf_cfg["OLE_TYPE_STR"]["EXT"])) {
	$oeaParticipation = $oea_cfg["OEA_Participation_Type_Private"];
}

$mappingDataArr = array();
$mappingDataArr['OEA_ProgramCode'] = $oeaProgramCode;
$mappingDataArr['OLE_ProgramID'] = $oleProgramId;
$mappingDataArr['DefaultAwardBearing'] = $oeaAwardBearing;
$mappingDataArr['DefaultParticipation'] = $oeaParticipation;
if ($liboea->Check_If_OLE_OEA_Mapping_Exist($oleProgramId)) {
	// update
	$SuccessArr['UpdateMapping'] = $liboea->Update_OLE_OEA_Mapping($oleProgramId, $mappingDataArr);
}
else {
	// insert
	$SuccessArr['InsertMapping'] = $liboea->Insert_OLE_OEA_Mapping($mappingDataArr);
}

if ($overrideStudentOea) {
	$SuccessArr['OverrideStudentMapping'] = $liboea->Update_OLE_OEA_Mapping_Student_Mapped_OEA($oleProgramId);
}

$ReturnMsgKey = (in_array(false, $SuccessArr))? 'UpdateUnsuccess' : 'UpdateSuccess';
header('Location: ?task=oeaItemMap&ReturnMsgKey='.$ReturnMsgKey);
?>