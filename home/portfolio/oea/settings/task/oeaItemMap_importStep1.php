<?php


# modifying : 

/******************************************************************************
 * Modification log:
 * 
 * Yuen (2011-12-05):
 * 			use AJAX to prepare for the download of mapping template
 * 
 * 
 *****************************************************************************/
 
$liboea = new liboea();

$h_TopTabMenu = $LibPortfolio->GET_TAB_MENU(libpf_tabmenu::getJupasAdminOEASettingsTags('ADMIN_SETTING_OEA', $task));


### Navigation
$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['OEA']['itemMap'], "javascript:js_Go_Back();");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");
$h_Navigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


### Steps Table
$h_StepTable = $linterface->GET_IMPORT_STEPS($CurrStep=1);

### Remarks Table
//$h_RemarksTable = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['iPortfolio']['OEA']['OLEMappingRemarks']);


### Override Student records checkbox
$h_overrideStudentOeaChk = $linterface->Get_Checkbox('OverrideStudentOeaChk', 'OverrideStudentOea', 1, $isChecked=0, $Class='', $Lang['iPortfolio']['OEA']['OverrideStudentMappedRecords']);


### Jupas OEA Program Info
$thisHref = $liboea->getJupasOeaItemInfoXlsFilePath();
$h_DownloadOEACsvFile = $linterface->Get_CSV_Sample_Download_Link($thisHref, $Lang['iPortfolio']['OEA']['ClickHereToDownloadOEAInfo']);


### OLE Mapping File
# Internal Checkbox
$h_InternalChk = $linterface->Get_Checkbox('OLENatureChk_Internal', 'OLENatureArr[]', 'INT', $isChecked=1, $Class='', $Lang['iPortfolio']['OEA']['Internal'], $Onclick='', $Disabled='');
# External Checkbox
$h_ExternalChk = $linterface->Get_Checkbox('OLENatureChk_External', 'OLENatureArr[]', 'EXT', $isChecked=1, $Class='', $Lang['iPortfolio']['OEA']['External'], $Onclick='', $Disabled='');
$h_OLENatureWarning = $linterface->Get_Form_Warning_Msg('WarningDiv_OLENature', $Lang['iPortfolio']['OEA']['jsWarningArr']['OLE_SelectProgramNature'], 'WarningDiv');

# Mapped
$h_MappedChk = $linterface->Get_Checkbox('MappingStatusChk_Mapped', 'MappingStatusArr[]', 1, $isChecked=1, $Class='', $Lang['iPortfolio']['OEA']['Mapped'], $Onclick='', $Disabled='');
# Not Mapped
$h_NotMappedChk = $linterface->Get_Checkbox('MappingStatusChk_NotMapped', 'MappingStatusArr[]', 0, $isChecked=1, $Class='', $Lang['iPortfolio']['OEA']['NotMapped'], $Onclick='', $Disabled='');
$h_MappingStatusWarning = $linterface->Get_Form_Warning_Msg('WarningDiv_MappingStatus', $Lang['iPortfolio']['OEA']['jsWarningArr']['OLE_SelectMappingStatus'], 'WarningDiv');

# Form Checkboxes
$h_FormCheckboxesTable = $liboea->Get_Form_Checkboxes_Table($liboea->getJupasApplicableFormIDArr(), $FormPerRow=3, $ShowAppliedFormOnly=0);
$h_FormCheckboxesWarning = $linterface->Get_Form_Warning_Msg('WarningDiv_Form', $Lang['General']['JS_warning']['SelectAForm'], 'WarningDiv');

# File Download
//$h_DownloadOLECsvFile = $linterface->Get_CSV_Sample_Download_Link("#TB_inline?height=500&width=750&inlineId=FakeLayer", $Lang['iPortfolio']['OEA']['ClickHereToDownloadMappingFile'], "onClick='js_Go_Download_OEA_MappingConfirm();return false;'");
//$h_DownloadOLECsvFile = "<a href='download_instruction.php?MassMailingID=58&width=780&Title=sdfkdsfh' title='".$Lang['iPortfolio']['OEA']['DownloadTemplateInstruction']."' class='thickbox''>".$linterface->Get_Excel_Image() ." ". $Lang['iPortfolio']['OEA']['ClickHereToDownloadMappingFile']."</a>";
//$h_DownloadOLECsvFile = "<a href='download_instruction.php?MassMailingID=58&width=780&Title=sdfkdsfh' title='".$Lang['iPortfolio']['OEA']['DownloadTemplateInstruction']."' class='thickbox''>".$linterface->Get_Excel_Image() ." ". $Lang['iPortfolio']['OEA']['ClickHereToDownloadMappingFile']."</a>";
$h_DownloadOLECsvFile = $linterface->Get_CSV_Sample_Download_Link("javascript:js_Go_Download_OEA_Mapping();", $Lang['iPortfolio']['OEA']['ClickHereToDownloadMappingFile'], "");


### Column Description
$ColumnTitleArr = $liboea->getOLEMappingImportColumnTitleArray();
$ColumnTitleArr = Get_Lang_Selection($ColumnTitleArr['Ch'], $ColumnTitleArr['En']);
$ColumnPropertyArr = $liboea->getOLEMappingImportColumnPropertyArray();
$ColumnDisplay = $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr);


### Remarks
$h_MandatoryRemarks = $linterface->MandatoryField();
$h_ReferenceRemarks = $linterface->ReferenceField();


### Buttons
$h_ContinueBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Continue();");
$h_CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Go_Back();");

$ReturnMsg = $Lang['General']['ReturnMessage'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);
include_once($template_script);
$linterface->LAYOUT_STOP();
?>