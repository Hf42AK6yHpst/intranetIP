<?
$liboea = new liboea();

if($sh<=9) $sh = "0".$sh;
if($sm<=9) $sm = "0".$sm;
if($eh<=9) $eh = "0".$eh;
if($em<=9) $em = "0".$em;

$periodStart = $startDate." ".$sh.":".$sm.":00";
$periodEnd = $endDate." ".$eh.":".$em.":59";
$studentSubmissionPeriod = $periodStart."#".$periodEnd;

if(!isset($MaxOea) || $MaxOea=="") $MaxOea = $oea_cfg["Setting"]["Max_No_Of_Oea_Record"];
if(!isset($AutoApproval) || $AutoApproval=="") $AutoApproval = 0;


//$OeaAutoApprovalPart2 = (!isset($AutoApprovalPart2) || $AutoApprovalPart2=="")? 1 : $AutoApprovalPart2 ;
$OeaAutoApprovalPart2 = $AutoApprovalPart2;

if($AutoApproval) $AllowEdit = 1;

//HANDLE SETTING FOR ALLOW INT, EXT APPLY TO PART 1 OR PART 2
$int_S = trim($int_S);
$ext_S = trim($ext_S);


$int_P = trim($int_P);
$ext_P = trim($ext_P);


$part_setting['S'] = $oea_cfg["OEA_Participation_Type_School"].$oea_cfg["Setting"]["Default_Separator2"].$int_S.$oea_cfg["Setting"]["Default_Separator2"].$ext_S;
$part_setting['P'] = $oea_cfg["OEA_Participation_Type_Private"].$oea_cfg["Setting"]["Default_Separator2"].$int_P.$oea_cfg["Setting"]["Default_Separator2"].$ext_P;
$StudentApplyOLEToPart = implode($oea_cfg["Setting"]["Default_ModuleInUse_Separator"],$part_setting);
//END OF SETTING FOR ALLOW INT, EXT APPLY TO PART 1 OR PART 2

$liboea->setMaxNoOfOea($MaxOea);
$liboea->setOeaAutoApproval($AutoApproval);
$liboea->setOeaAutoApprovalPart2($OeaAutoApprovalPart2);
$liboea->setEditableWhenApproved($AllowEdit);
$liboea->setStudentSubmissionPeriod($studentSubmissionPeriod); 
$liboea->setAllowStudentNewOEA_NotFromOLE($AllowStudentNewOEA_NotFromOLE);

$liboea->setForceStudentToApplyRoleMapping($ForceStudentToApplyRoleMapping);

$liboea->setStudentApplyOLEToPart($StudentApplyOLEToPart);

$newPartI = ($newPartI == '') ? 0: $newPartI;
$newPartII = ($newPartII == '') ? 0: $newPartII;

$liboea->setPartIAllowForNewOEA($newPartI);
$liboea->setPartIIAllowForNewOEA($newPartII);

$liboea->setAllowStudentApplyOthersOea($AllowStudentApplyOthersOea);

$result = $liboea->saveSetting();

if(in_array(0, $result)) {		# some settings cannot be updated
	header("Location: index.php?task=oeaSettings&msg=".$Lang['iPortfolio']['OEA']['SettingsSaveUnsuccessfully']);	
} else {
	header("Location: index.php?task=oeaSettings&msg=".$Lang['iPortfolio']['OEA']['SettingsSaveSuccessfully']);
}
?>