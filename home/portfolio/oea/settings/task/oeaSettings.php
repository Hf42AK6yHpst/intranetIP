﻿<?
# modifying : 
$liboea = new liboea();

$h_TopTabMenu = $LibPortfolio->GET_TAB_MENU(libpf_tabmenu::getJupasAdminOEASettingsTags('ADMIN_SETTING_OEA', $task));

###### OEA Remarks######
$h_OEA_Remarks = '<fieldset id="remarksFieldset" class="instruction_box"><legend class="instruction_title" style="color:red">Remarks</legend><span id="remarksSpan">'.$Lang['iPortfolio']['JUPAS']['RemarksOEA'].'</span></fieldset>';

$x .= "<br style='clear:both'>";

$x .= "<form name=\"form1\" id=\"form1\" action=\"index.php\" method=\"post\" onSubmit=\"return checkForm();\">";

//$x .= "<div class='table_row_tool row_content_tool'><input type='button' class='formsmallbutton' onClick='javascript:EditInfo();' name='EditBtn' id='EditBtn' value='".$button_edit."'   onMouseOver='this.className=\"formsmallbuttonon\"' onMouseOut='this.className=\"formsmallbutton\"'/></div>";
		
############ Table for Max no. of OEA record [Start] ############
$MaxOea = $liboea->getMaxNoOfOea();
if($MaxOea=="") {
	$MaxOea = $oea_cfg["Setting"]["Max_No_Of_Oea_Record"];
}
$MaxNoInOption = $oea_cfg["Setting"]["Max_No_Of_Oea_Record"];
$ary = array();
for($i=1; $i<=$MaxNoInOption; $i++) {
	$ary[] = array($i,$i);	
}

$maxOeaSelect = getSelectByArray($ary, 'name="MaxOea" id="MaxOea"', $MaxOea, 0, 1);

$x .= $linterface->GET_NAVIGATION2($Lang['iPortfolio']['OEA']['GeneralSetting']);
$x .= "<table class='form_table_v30'>";
$x .= "<tr>";
$x .= "<td class='field_title' width='45%'>".$Lang['iPortfolio']['OEA']['MaxNoOfOeaAllow']."</td>";
$x .= "<td width='55%'><span class='Edit_Hide'>$MaxOea</span><span class='Edit_Show' style='display:none'>$maxOeaSelect</span></td>";
$x .= "</tr>";
$x .= "</table>";
############ Table for Max no. of OEA record [End] ############

$x .= "<br style='clear:both'>";

############ Table for Submission Period [Start] ############

$sh = "00";
$sm = "00";
$eh = "23";
$em = "59";

$period = $liboea->getStudentSubmissionPeriod();
$periodAry = explode("#",$period);
$periodStart = $periodAry[0];
$periodEnd = $periodAry[1];
$startDate = substr($periodStart,0,10);
$endDate = substr($periodEnd,0,10);
$startHr = substr($periodStart,11,2);
$startMin = substr($periodStart,14,2);
$endHr = substr($periodEnd,11,2);
$endMin = substr($periodEnd,14,2);

$displayStart = ($periodStart=="") ? "-" : $periodStart;
$displayEnd = ($periodEnd=="") ? "-" : $periodEnd;

$allowStudentNewOEA_NotFromOLE = $liboea->getAllowStudentNewOEA_NotFromOLE();


//GENERATE THE DISPLAY FOR SELECT PART I OR PART II FOR STUDENT CREATE NEW OEA FROM BLANK

$isAllowNewOEAPartI  = $liboea->getPartIAllowForNewOEA();
$isAllowNewOEAPartII = $liboea->getPartIIAllowForNewOEA();


$PartI_IIOptionForNewOEADisplay = '';
if($isAllowNewOEAPartI){
	$allowNewOEAPartICheck = ' CHECKED ';
	$PartI_IIOptionForNewOEADisplay .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$Lang['iPortfolio']['OEA']['Part1'].'<br/>';
}
if($isAllowNewOEAPartII){
	$allowNewOEAPartIICheck = ' CHECKED ';
	$PartI_IIOptionForNewOEADisplay .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$Lang['iPortfolio']['OEA']['Part2'].'';
}
if($PartI_IIOptionForNewOEADisplay != ''){
	$PartI_IIOptionForNewOEADisplay = '&nbsp;&nbsp;&nbsp;&nbsp;'.$Lang['iPortfolio']['OEA']['SettingPartSrc'].'<br/>'.$PartI_IIOptionForNewOEADisplay;
}

$h_PartI_IIOptionForNewOEA = '';

$newPartIDisable = '';
$newPartIIDisable = '';
if(!$allowStudentNewOEA_NotFromOLE){
	//NOT ALLOW STUDENT TO CREATE NEW OEA
	$newPartIDisable = ' DISABLED ';
	$newPartIIDisable = ' DISABLED ';
}

$disableForoptionForNewForPartI_II = '';
if(!$allowStudentNewOEA_NotFromOLE){
	$disableForoptionForNewForPartI_II = ' DISABLED ';
}

$h_PartI_IIOptionForNewOEA .= '&nbsp;&nbsp;&nbsp;&nbsp;<input type = "checkbox" id="newPartI" name="newPartI" value="1" '.$allowNewOEAPartICheck.' '.$disableForoptionForNewForPartI_II.'/>';
$h_PartI_IIOptionForNewOEA .= '<label for="newPartI">'.$Lang['iPortfolio']['OEA']['Part1'].'</label>';
$h_PartI_IIOptionForNewOEA .= '<br/>';
$h_PartI_IIOptionForNewOEA .= '&nbsp;&nbsp;&nbsp;&nbsp;<input type = "checkbox" id="newPartII" name="newPartII" value="1" '.$allowNewOEAPartIICheck.' '.$disableForoptionForNewForPartI_II.'/>';
$h_PartI_IIOptionForNewOEA .= '<label for="newPartII">'.$Lang['iPortfolio']['OEA']['Part2'].'</label>';


$allowStudentNewOEA_NotFromOLE_Display = ($allowStudentNewOEA_NotFromOLE)? $Lang['General']['Yes'] : $Lang['General']['No']; 
$allowStudentNewOEA_NotFromOLE_Yes_Checked = ($allowStudentNewOEA_NotFromOLE)? 1 : 0;
$allowStudentNewOEA_NotFromOLE_No_Checked = ($allowStudentNewOEA_NotFromOLE)? 0 : 1;

$x .= $linterface->GET_NAVIGATION2($Lang['iPortfolio']['OEA']['SubmissionSettings']);
$x .= "<table class='form_table_v30' >";

$x .= "<tr>";
$x .= "<td class='field_title' width='45%'>".$iPort["start_time"]."</td>";
$x .= "<td width='55%'>";
$x .= "<span class='Edit_Hide'>$displayStart</span><span class='Edit_Show' style='display:none'>".$linterface->GET_DATE_PICKER("startDate",$startDate)." ".returnHour('sh', $startHr, 0)." : ".returnMinute('sm', $startMin, 0, $displayEveryMinute=1)."</span>";
$x .= "</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='field_title'>".$iPort["end_time"]."</td>";
$x .= "<td>";
$x .= "<span class='Edit_Hide'>$displayEnd</span><span class='Edit_Show' style='display:none'>".$linterface->GET_DATE_PICKER("endDate",$endDate)." ".returnHour('eh', $endHr, 0)." : ".returnMinute('em', $endMin, 0, $displayEveryMinute=1)."</span>";
$x .= "</td>";
$x .= "</tr>";



############ Table for allow student apply which PART and OLE############

$part_setting = array();
$part_setting = $liboea->getSettingForPartToType();

//$x .= $linterface->GET_NAVIGATION2($Lang['iPortfolio']['OEA']['ApprovalSetting']);


$partName = $oea_cfg["OEA_Participation_Type_School"];
$x .= _displayTypeSettingUI($partName,$part_setting[$partName]);

$partName = $oea_cfg["OEA_Participation_Type_Private"];
$x .= _displayTypeSettingUI($partName,$part_setting[$partName]);

############ Table for allow student apply which PART and OLE [End] ############






############ Table for FOR Student USE ROLE MAP############
$isForceStudentToApplyRoleMapping = $liboea->getForceStudentToApplyRoleMapping();

$noChecked   = '';
$yesChecked  = '';

//if($ForceStudentToApplyRoleMapping == "") $isAutoApproval = $oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"];
if($isForceStudentToApplyRoleMapping) {
	$yesChecked = " checked";	
	$autoDisplay = $i_general_yes;
} else {
	$noChecked = " checked";
	$autoDisplay = $i_general_no;
}

//$x .= $linterface->GET_NAVIGATION2($Lang['iPortfolio']['OEA']['ApprovalSetting']);
$x .= "<tr>";
$x .= "<td class='field_title' width='45%'>".$Lang['iPortfolio']['OEA']['SettingForceWithRoleMapping']."</td>";
$x .= "<td width='55%'>";
$x .= "<span class='Edit_Hide'>$autoDisplay </span>";
$x .= "<span class='Edit_Show' style='display:none'>";

$x .= "<input type='radio' name='ForceStudentToApplyRoleMapping' id='ForceStudentToApplyRoleMapping0' value='1' $yesChecked>";
$x .= "<label for='ForceStudentToApplyRoleMapping0'>$i_general_yes</label>";

$x .= "<input type='radio' name='ForceStudentToApplyRoleMapping' id='ForceStudentToApplyRoleMapping1' value='0' $noChecked>";
$x .= "<label for='ForceStudentToApplyRoleMapping1'>$i_general_no</label></span>";

$x .= "</td>";
$x .= "</tr>";
############ Table for OEA Approval setting [End] ############


############ Table for ALLOW STUDENT NEW A OEA ############
$x .= "<tr>";
$x .= "<td class='field_title'>".$Lang['iPortfolio']['OEA']["AllowStudentNewOEA_NotFromOLE"]."</td>";
$x .= "<td>";
	$x .= "<span class='Edit_Hide'>".$allowStudentNewOEA_NotFromOLE_Display."</span>";
	$x .= "<span class='Edit_Show' style='display:none'>";
		$x .= $linterface->Get_Radio_Button('AllowStudentNewOEA_NotFromOLE_Yes', 'AllowStudentNewOEA_NotFromOLE', 1, $allowStudentNewOEA_NotFromOLE_Yes_Checked, $Class="", $Lang['General']['Yes'], $Onclick="enablePartOption(1)",$isDisabled=0);
		$x .= $linterface->Get_Radio_Button('AllowStudentNewOEA_NotFromOLE_No', 'AllowStudentNewOEA_NotFromOLE', 0, $allowStudentNewOEA_NotFromOLE_No_Checked, $Class="", $Lang['General']['No'], $Onclick="enablePartOption(0)",$isDisabled=0);
	$x .= "</span>";
	$x .= '<br/>';
	$x .= '<span class="Edit_Hide">'.$PartI_IIOptionForNewOEADisplay.'</span>';
	$x .= '<span class="Edit_Show" style="display:none">'.$h_PartI_IIOptionForNewOEA.'</span>';
$x .= "</td>";
$x .= "</tr>";
############ Table for ALLOW STUDENT NEW A OEA END############


############ Allow Student Submit "Others" OEA ############
$AllowStudentApplyOthersOea = $liboea->getAllowStudentApplyOthersOea();
$AllowStudentApplyOthersOea_Display = ($AllowStudentApplyOthersOea)? $Lang['General']['Yes'] : $Lang['General']['No'];

$noChecked   = '';
$yesChecked  = '';
if($AllowStudentApplyOthersOea) {
	$yesChecked = " checked";	
	$autoDisplay = $Lang['General']['Yes'];
} else {
	$noChecked = " checked";
	$autoDisplay = $Lang['General']['No'];
}

$x .= "<tr>";
$x .= "<td class='field_title'>".$Lang['iPortfolio']['OEA']['AllowStudentSubmitOthersOea']."</td>";
$x .= "<td>";
$x .= "<span class='Edit_Hide'>$autoDisplay </span>";
$x .= "<span class='Edit_Show' style='display:none'>";

$x .= "<input type='radio' name='AllowStudentApplyOthersOea' id='AllowStudentApplyOthersOea1' value='1' $yesChecked>";
$x .= "<label for='AllowStudentApplyOthersOea1'>".$Lang['General']['Yes']."</label>";

$x .= "<input type='radio' name='AllowStudentApplyOthersOea' id='AllowStudentApplyOthersOea0' value='0' $noChecked>";
$x .= "<label for='AllowStudentApplyOthersOea0'>".$Lang['General']['No']."</label></span>";

$x .= "</td>";
$x .= "</tr>";
############ Allow Student Submit "Others" OEA [End] ############



$x .= "</table>";
############ Table for Submission Period [End] ############

$x .= "<br style='clear:both'>";


############ Table for OEA Approval setting [Start] ############
$yesChecked = '';
$noChecked = '';
$autoDisplay = '';

$isAutoApproval = $liboea->getOeaAutoApproval();


if($isAutoApproval=="") $isAutoApproval = $oea_cfg["OEA_STUDENT_RECORDSTATUS_PENDING"];

if($isAutoApproval) {	# need approval ?
	$noChecked = " checked";	
	$autoDisplay = $i_general_no;
} else {
	$yesChecked = " checked";
	$autoDisplay = $i_general_yes;
}

$x .= $linterface->GET_NAVIGATION2($Lang['iPortfolio']['OEA']['ApprovalSetting']);
$x .= "<table class='form_table_v30'>";
$x .= "<tr>";
$x .= "<td class='field_title' width='45%'>".$Lang['iPortfolio']['OEA']['Part1NeedApproval']."</td>";
$x .= "<td width='55%'>";
$x .= "<span class='Edit_Hide'>$autoDisplay </span>";
$x .= "<span class='Edit_Show' style='display:none'>";
$x .= "<input type='radio' name='AutoApproval' id='AutoApproval0' value='0' $yesChecked><label for='AutoApproval0'>$i_general_yes</label><input type='radio' name='AutoApproval' id='AutoApproval1' value='1' $noChecked><label for='AutoApproval1'>$i_general_no</label></span>";
$x .= "</td>";
$x .= "</tr>";
$x .= "</table>";
$x .= "<br>";
############ Table for OEA Approval setting [End] ############

############ Table for OEA Part 2 Approval setting [Start] ############
$isAutoApprovalPart2 = $liboea->getOeaAutoApprovalPart2();

$noChecked   = '';
$yesChecked  = '';
$autoDisplay = '';
//If have not set the approval setting in part2 , default is no need to approval
if($isAutoApprovalPart2=="") $isAutoApproval = $oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"];
if($isAutoApprovalPart2) {	# need approval ?
	$noChecked = " checked";	
	$autoDisplay = $i_general_no;
} else {
	$yesChecked = " checked";
	$autoDisplay = $i_general_yes;
}


$x .= "<table class='form_table_v30'>";
$x .= "<tr>";
$x .= "<td class='field_title' width='45%'>".$Lang['iPortfolio']['OEA']['Part2NeedApproval']."</td>";
$x .= "<td width='55%'>";
$x .= "<span class='Edit_Hide'>$autoDisplay </span>";
$x .= "<span class='Edit_Show' style='display:none'>";
$x .= "<input type='radio' name='AutoApprovalPart2' id='AutoApproval0Part2' value='0' $yesChecked><label for='AutoApproval0Part2'>$i_general_yes</label><input type='radio' name='AutoApprovalPart2' id='AutoApproval1Part2' value='1' $noChecked><label for='AutoApproval1Part2'>$i_general_no</label></span>";
$x .= "</td>";
$x .= "</tr>";
$x .= "</table>";
$x .= "<br>";
############ Table for OEA Approval setting [End] ############



$x .= "<br>";

$x .= "<div class='edit_bottom_v30 Edit_Hide'>";
$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Edit'], "button", 'javascript:EditInfo();', $id="EditBtn");
$x .= "</div>";

$x .= "<div class='edit_bottom_v30 Edit_Show' style='display:none'>";
$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "submit", "", "UpdateBtn", "style='display: none'")." ";
$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["cancel"], "button", "ResetInfo();", "cancelbtn", "style='display: none'");
$x .= "</div>";

$x .= "<input type='hidden' name='task' id='task' value=''>";
$x .= "<input type='hidden' name='script' id='script' value=''>";

$x .= "</form>";
$x .= "<br />";

$linterface->LAYOUT_START();
include_once($template_script);
$linterface->LAYOUT_STOP();


function _displayTypeSettingUI($part, $partDetailsAry){
	global $Lang,$ipf_cfg,$iPort,$oea_cfg;

	$html = '';   // store the return HTML string

	$type_display = '';   // caption --> display the part support with int or ext
	$part_checked = '';		//part checkbox with check or not
	$int_checked  = '';		//int checkbox with check or not
	$ext_checked  = '';		//ext checkbox with check or not

	$partName = ($part == $oea_cfg["OEA_Participation_Type_School"])? $Lang['iPortfolio']['OEA']['Part1'].' - '.$Lang['iPortfolio']['OEA']['OEA_Int']:$Lang['iPortfolio']['OEA']['Part2'].' - '.$Lang['iPortfolio']['OEA']['OEA_Ext'];

	$captionTitle = ($part == $oea_cfg["OEA_Participation_Type_School"]) ? $Lang['iPortfolio']['OEA']['SettingSubmitType'] : '';

	//passing $part can support with int / ext
	$with_int = $partDetailsAry[0];  //store support int 
	$with_ext = $partDetailsAry[1];	 //store support ext

	$tmp_type_display = array(); // temp array to store display

	if($with_int == ''){
		// do nothing 
	}else{
		$part_checked = ' checked ';
		$int_checked = ' checked ';		
		$tmp_type_display[] = $iPort["internal_record"];
	}

	if($with_ext == ''){
		//do nothing
	}else{
		$part_checked = ' checked ';
		$ext_checked = ' checked ';
		$tmp_type_display[] = $iPort["external_record"];
	}

	$type_display = (sizeof($tmp_type_display) > 0)? implode('<br/>&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;',$tmp_type_display): '';

	//handle the HTML input name 
	//$partCheckBox = 'part_'.$part;
	$intCheckBox = 'int_'.$part;
	$extCheckBox = 'ext_'.$part;

	$captionSource = $Lang['iPortfolio']['OEA']['SettingOEASrc'].":<br>";
	if($type_display == ''){
		$submitCaption = '&nbsp;&nbsp;<font color="red">('.$Lang['iPortfolio']['OEA']['SettingNotAllowSubmit'].')</font>';
		$captionSource = '';
	}

	$html .= "<tr>";
	$html .= "<td class='field_title' width='45%'>".$captionTitle."</td>";
	$html .= "<td width='55%'>";

	$html .= "<span class='Edit_Hide'>".$partName.$submitCaption."<br/>&nbsp;&nbsp;&nbsp;&nbsp".$captionSource."&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp".$type_display."</span>";
	$html .= "<span class='Edit_Show' style='display:none'>";
	$html .= $partName.'<br/>';
	$html .= '&nbsp;&nbsp;&nbsp;&nbsp;';

//	$html .= '<input type= "checkbox" name="'.$partCheckBox.'" id="'.$partCheckBox.'" value="1" "'.$part_checked.'"><label for = "'.$partCheckBox.'">'.$partName.'</label><br/>';

	$html .= "<input type='checkbox' name=\"{$intCheckBox}\" id=\"{$intCheckBox}\" value='".$ipf_cfg["OLE_TYPE_STR"]["INT"]."' {$int_checked} >";
	$html .= "<label for=\"{$intCheckBox}\">".$iPort["internal_record"]."</label>";

	$html .= '<br/>';
	$html .= '&nbsp;&nbsp;&nbsp;&nbsp;';
	$html .= "<input type='checkbox' name=\"{$extCheckBox}\" id=\"{$extCheckBox}\" value='".$ipf_cfg["OLE_TYPE_STR"]["EXT"]."' {$ext_checked} >";
	$html .= "<label for=\"{$extCheckBox}\">".$iPort["external_record"]."</label>";

	$html .= "</td>";
	$html .= "</tr>";


	return $html;
}

?>