<?
//using connie

$liboea = new liboea();
$lpf_ole = new libpf_ole();
$loea_item = new liboea_item();
//$linterface = new interface_html();

$h_TopTabMenu = $LibPortfolio->GET_TAB_MENU(libpf_tabmenu::getJupasAdminOEASettingsTags('ADMIN_SETTING_OEA', $task));

$h_Include_Js_Css = $liboea->Include_JS_CSS();



$OEA_RoleTypeArr = $loea_item->get_OEA_Role_Array();
$count_OEA_RoleTypeArr = count($OEA_RoleTypeArr);

$RoleSelSel_size = 11*$count_OEA_RoleTypeArr;

# SearchBox
$SearchBox = $linterface->Get_Search_Box_Div('Keyword', $Keyword);

### the rows of selected role Area###

$html_first_selectedRoleArea='';
$html_selectedRoleArea='';

//debug_r($OEA_RoleTypeArr);

$counter=0;
$OEA_RoleTypeKeyArr = array();
foreach((array)$OEA_RoleTypeArr as $_type=>$_typeName)
{ 
	$OEA_RoleTypeKeyArr[]=$_type;
	
	$_addAllID = 'AddAll'.'_'.$_type;
	$_addID = 'Add'.'_'.$_type;
	$_removeAllID = 'RemoveAll'.'_'.$_type;
	$_removeID = 'Remove'.'_'.$_type;
	
	$_selectID = 'SelectedRoleSel'.'_'.$_type;
	$_selectNameArr = $_type.'_SelectedRoleNameArr[]';
	
	$_divID= $_type."_divID";
	$_noID = $_type."_noID";
	
	$_html='';
	$_html=<<<html
          		<td>
          			<br/><br/><br/>
		            <input id="{$_addAllID}" onclick="js_button_functions('AddAll','{$_selectID}');" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;&gt;" style="width:40px;" title="{$Lang['Btn']['AddAll']}"/><br />
		            <input id="{$_addID}" onclick="js_button_functions('Add','{$_selectID}');" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;" style="width:40px;" title="{$Lang['Btn']['AddSelected']}"/><br /><br />
		         
		            <input id="{$_removeID}" onclick="js_button_functions('Remove','{$_selectID}');" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;" style="width:40px;" title="{$Lang['Btn']['RemoveSelected']}"/><br />
		            <input id="{$_removeAllID}" onclick="js_button_functions('RemoveAll','{$_selectID}');" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;&lt;" style="width:40px;" title="{$Lang['Btn']['RemoveAll']}"/>
		            
		        </td>
		        <td> &nbsp;&nbsp;
				<td/>
          		<td bgcolor="#EFFEE2">
          		 	<table>
          		 		<tr>
          		 			<td>{$_typeName}</td>
          		 			<td id={$_noID} style="text-align:right"></td>
						</tr>
          		 		<tr>
          		 			<td colspan='2'>
          		 				<div id="{$_divID}">																	
								</div>
							</td>
						</tr>
					</table>
          		</td>
html;
        
     if($counter==0)
     {
     	$html_first_selectedRoleArea=$_html;
     }
     else
     {
     	$html_selectedRoleArea .="<tr>$_html</tr>";
     }   
     $counter++;
}

$ReturnMsg = '';
if(isset($msg))
{
	$ReturnMsg = ($msg=='1')? $Lang['General']['ReturnMessage']['RecordSaveSuccess'] : $Lang['General']['ReturnMessage']['RecordSaveUnSuccess'];
}

$linterface->LAYOUT_START($ReturnMsg);
include_once($template_script);
$linterface->LAYOUT_STOP();
?>

