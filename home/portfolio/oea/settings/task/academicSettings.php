<?php
$liboea = new liboea();
$fcm_ui = new form_class_manage_ui();

$h_TopTabMenu = $LibPortfolio->GET_TAB_MENU(libpf_tabmenu::getJupasAdminAcademicSettingsTags('ADMIN_SETTING_ACADEMIC', $task));


### Form Selection
//$h_FormSelection = $fcm_ui->Get_Form_Selection('YearID', $SettingsArr['YearID'], $thisOnChange, $noFirst=1, $isAll=0);

### Academic Year Selection
$Academic_AcademicYearID = $liboea->getAcademic_AcademicYearID();
$Academic_YearTermID = $liboea->getAcademic_YearTermID();
$h_AcademicSourceNavigation = $linterface->GET_NAVIGATION2_IP25($Lang['iPortfolio']['OEA']['AcademicArr']['DataSource']);
$h_AcademicYearSelection = getSelectAcademicYear('AcademicYearID', 'onchange="js_Changed_AcademicYear_Selection(this.value);"', $noFirst=1, $noPastYear=0, $Academic_AcademicYearID);



### Percentile Settings
$h_PercentileNavigation = $linterface->GET_NAVIGATION2_IP25($Lang['iPortfolio']['OEA']['AcademicArr']['Percentile']);
$h_PercentileManualAdjChk = $linterface->Get_Checkbox('Percentile_ManualAdj', 'Percentile_ManualAdj', $Value=1, $isChecked=0, $Class='', $Display='', $Onclick='', $Disabled='');


### Overall Rating Settings
$Academic_PercentileMapWithOverAllRating = $liboea->getAcademic_PercentileMapWithOverAllRating();
$h_OverallRatingNavigation = $linterface->GET_NAVIGATION2_IP25($Lang['iPortfolio']['OEA']['AcademicArr']['OverallRating']);
$h_MappingCriteriaTable = $liboea->Get_Academic_Settings_Overall_Rating_Mapping_Table($Academic_PercentileMapWithOverAllRating);

$h_OverallRatingManualAdjChk = $linterface->Get_Checkbox('OverallRating_ManualAdj', 'OverallRating_ManualAdj', $Value=1, $isChecked=0, $Class='', $Display='', $Onclick='', $Disabled='');



//$h_MapByPercentileRadio = $linterface->Get_Radio_Button('OverallRatingMapping_Percentile', 'OverallRatingMapping', $Value='Percentile', $isChecked=1, $Class="", $Display=$Lang['iPortfolio']['OEA']['AcademicArr']['Percentile'], $Onclick="js_Changed_Mapping_Settings('Percentile');", $isDisabled=0);
//$thisHeight = $liboea->getThickboxHeight();
//$thisWidth = $liboea->getThickboxWidth();
//$thisOnclick = "js_Get_Settings_Layer('Percentile');";
//$thisDivID = "OverallRatingMapping_Percentile_SettingsDiv";
//$h_OverallRatingByPercentileSettingsDiv = $linterface->Get_Thickbox_Div($thisHeight, $thisWidth, 'setting_row', $Title, $thisOnclick, $InlineID="FakeLayer", $Content="", $thisDivID, $LinkID='');
//$h_MapByScoreRadio = $linterface->Get_Radio_Button('OverallRatingMapping_Score', 'OverallRatingMapping', $Value='Score', $isChecked=0, $Class="", $Display=$Lang['iPortfolio']['OEA']['AcademicArr']['Score'], $Onclick="js_Changed_Mapping_Settings('Score');", $isDisabled=0);
//
//$thisOnclick = "js_Get_Settings_Layer('Score');";
//$thisDivID = "OverallRatingMapping_Score_SettingsDiv";
//$thisTags = 'style="display:none;"';
//$h_OverallRatingByScoreSettingsDiv = $linterface->Get_Thickbox_Div($thisHeight, $thisWidth, 'setting_row', $Title, $thisOnclick, $InlineID="FakeLayer", $Content="", $thisDivID, $LinkID='', $thisTags);


### Button
$SaveBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "js_Save_Settings();", $id="Btn_Save");


### Include thickbox library
$h_IncludeJsCss = $liboea->Include_JS_CSS();

$linterface->LAYOUT_START();
include_once($template_script);
$linterface->LAYOUT_STOP();
?>