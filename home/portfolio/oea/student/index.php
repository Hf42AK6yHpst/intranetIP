<?
# modifying :  

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
//include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_item.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libole_student_item.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting_search.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_student.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_additional_info.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_supplementary_info.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libpf-ole.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_item.php");

intranet_auth();
intranet_opendb();



//RECEIVE VARIABLE
$task		= trim($task);
$script		= trim($script);// for ajax
$LibPortfolio = new libportfolio2007();
$liboea = new liboea();

if ($task=='') {
	if ($liboea->isEnabledOEAItem()) {
		$task = "index";
	}
	else if ($liboea->isEnabledAdditionalInfo()) {
		$task = "addiInfo_list";
	} 
}




### Top Tab Menu
/*
 * To simplify the mechanism of generating the Top Tab Menu, please kindly name the task and template php as follows for different tabs:
 * OEA tab: oea_xxx.php
 * Additional Info tab: addiInfo_xxx.php
 * Ability tab: ability_xxx.php
 * Academic Performance tab: academic_xxx.php
 * Supplementary tab: suppInfo_xxx.php
 */
$TabMenuArr = libpf_tabmenu::getJupasStudentSettingsTags('STUDENT_SETTING', $task);
$h_TopTabMenu = $LibPortfolio->GET_TAB_MENU($TabMenuArr);


$linterface = new interface_html("iportfolio_default.html");
$CurrentPage = "Student_OEA";
$CurrentPageName = $Lang['iPortfolio']['OEA']['JUPAS'];

### Title ###
$TAGS_OBJ[] = array($Lang['iPortfolio']['OEA']['OEA_Name']);
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR();


switch($task)
{
  case "ajax":
  	$task_script = "../ajax/" . str_replace("../", "", $script) . ".php";
    
    	
    
		// Task exists?
		if (file_exists($task_script)) 
		{
			include_once($task_script);
		} else {
			//$linterface->LAYOUT_START();
			echo "error! task not find (task_script = $task_script)<br/>";
			//$linterface->LAYOUT_STOP();
		}
		break;
		
	default:
		// By default, open the script with the same name as the task (after appended with ".php").
		// Prevent searching parent directories, like 'task=../../index'.
		$task = str_replace("../", "", $task);
		$task_script = "task/" . $task . ".php";
		$template_script = "templates/" . $task . ".tmpl.php";
		
		// Task exists?
		if (file_exists($task_script)) {
			include_once($task_script);
		}
		else {
			$linterface->LAYOUT_START();
			echo "error! task not find<br/>";
			$linterface->LAYOUT_STOP();
		}
		break;
			
}

intranet_closedb();
?>
