<?php
$AdditionalInfoID = IntegerSafe($_REQUEST['AdditionalInfoID']);

$ObjAddiInfo = new liboea_additional_info($AdditionalInfoID);
if(is_numeric($AdditionalInfoID)){
	$thisInfoStudentID = $ObjAddiInfo->getStudentID();
	if($_SESSION['UserID'] != $thisInfoStudentID ){
		//if this AdditionalInfoID does not belong to the student, rediect to front page.
		//block for student key in the ID directly in the URL
		header("Location: /home/portfolio/oea/student/");
	}
}

$h_JupasItemCode = liboea::getAddiInfoInternalCode();


### Navigation
$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['OEA']['AdditionalInformation'], "javascript:js_Back_To_Additional_Info_List()");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");
$h_PageNavigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

### Instruction
$h_InstructionTable = $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $Lang['iPortfolio']['OEA']['AddiInfoEditInstruction']);

### Edit Table
$h_EditTable = $ObjAddiInfo->getEditTable();

### Button
$h_SaveButton = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", $onclick="js_Save_Additional_Info();", $id="Btn_Save");
$h_BackButton = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", $onclick="js_Back_To_Additional_Info_List();", $id="Btn_Back");


### Include js and css
$h_IncludeJsCss = liboea::Include_JS_CSS();

$linterface->LAYOUT_START();
include_once($template_script);
$linterface->LAYOUT_STOP();
?>