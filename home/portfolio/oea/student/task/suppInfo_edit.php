<?php
$SupplementaryInfoID = $_REQUEST['SupplementaryInfoID'];
$ObjSuppInfo = new liboea_supplementary_info($SupplementaryInfoID);
$h_JupasItemCode = liboea::getSuppInfoInternalCode();


### Navigation
$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['OEA']['SupplementaryInformation'], "javascript:js_Back_To_Supplementary_Info_List()");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");
$h_PageNavigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

### Instruction
$h_InstructionTable = $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $Lang['iPortfolio']['OEA']['SuppInfoEditInstruction']);

### Edit Table
$h_EditTable = $ObjSuppInfo->getEditTable();

### Button
$h_SaveButton = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", $onclick="js_Save_Supplementary_Info();", $id="Btn_Save");
$h_BackButton = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", $onclick="js_Back_To_Supplementary_Info_List();", $id="Btn_Back");


$linterface->LAYOUT_START();
include_once($template_script);
$linterface->LAYOUT_STOP();
?>