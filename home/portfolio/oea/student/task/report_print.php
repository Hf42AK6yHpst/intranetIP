<?


include_once($intranet_root."/includes/portfolio25/oea/liboea_report.php");
include_once($intranet_root."/includes/portfolio25/slp/libpf-ole.php");
include_once($intranet_root."/includes/portfolio25/oleCategory/libpf-category.php");
include_once($intranet_root.'/includes/libuser.php');


$printMode = $_POST['printMode'];
$includeOleData = $_POST['includeOleData'];
$StudentIDArr = $_POST['StudentIDArr'];


$StudentID = $_SESSION['UserID'];

if(is_numeric($StudentID)){
	//do nothing
}else{
	exit();
}


$objOeaReport = new liboea_report();
$StudentIDArr = array($StudentID);
$studentData = $objOeaReport->getStudentDataForReport($StudentIDArr, $includeOleData);

switch($printMode){
	case $oea_cfg['printMode']['html']:
		$h_table = $objOeaReport->printReportAsHtml($studentData, $includeOleData);
	break;
	case $oea_cfg['printMode']['csv']:
		$objOeaReport->printReportAsCSV($studentData, $includeOleData);
		exit();
	break;
}

$h_reportTitle = $objOeaReport->getReportTitle();

$sql = 'select HKJApplNo, ClassName,ClassNumber , '.getNameFieldByLang("i.").' as studentName from INTRANET_USER as i where userid = '.$StudentID;
$objDb = new libdb();
$studentInfo = current($objDb->returnResultSet($sql));

$h_studentName = $studentInfo['studentName'];
$h_studentClassName = $studentInfo['ClassName'];
$h_studentClassNumbe = $studentInfo['ClassNumber'];
$h_studentHKJApplNo = $studentInfo['HKJApplNo'];


$h_studentHKJApplNo = ($h_studentHKJApplNo == '' || $h_studentHKJApplNo == 0)? '--':$h_studentHKJApplNo;
$h_studentClassNumber = ($h_studentClassNumber == '')? '' :'('.$h_studentClassNumber.')';


$h_printButton = "<table width='100%' align='center' class='print_hide'><tr>";
$h_printButton .= "<td align='right'>";
$h_printButton .= $linterface->GET_BTN($button_print, "button","javascript:window.print()");
$h_printButton .= "</td></tr></table>";



$h_printDate = date('Y-m-d');
include_once($template_script);

exit();

?>