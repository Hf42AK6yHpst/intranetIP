<?
# modifying : 

$h_HiddenOLE_Item = "";

$currentPos = (isset($currentPos) && $currentPos!=0) ? $currentPos : 1;


# till end of selection
if($currentPos>sizeof($recordID)) {
	header("Location: index.php?msg=$msg");
	exit;
}

$_id = $recordID[$currentPos-1];


$liboea = new liboea();
$_objOLE_ITEM = new libole_student_item($_id);
$ole_ProgramID = $_objOLE_ITEM->getProgramID();
$_objOEA_ITEM = new liboea_item();
$liboea_setting = new liboea_setting();
//$liboea_setting_search = new liboea_setting_search($liboea_setting);


$_objOEA_ITEM->setTempOLE_STUDENT_RecordID($_id);

### Navigation
$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['OEA']['OEA_List'], "javascript:js_Back_To_OEA_List()");
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['OEA']['OEA_CreateFromOLE'], "");
$h_navigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

# step information
$STEPS_OBJ[] = array($Lang['iPortfolio']['OEA']['NewFromOLE_Step1'], 0);
$STEPS_OBJ[] = array($Lang['iPortfolio']['OEA']['NewFromOLE_Step2'], 1);
$step_navigation = $linterface->GET_STEPS_IP25($STEPS_OBJ);


$countText = "
<br style='clear:both'>
	<div class='this_form' style='width:550'>
	<table class='form_table_v30'>
	<tr>
		<td class='field_title' width='35%'>".$Lang['iPortfolio']['OEA']['Add_CountText_1']."</td>
		<td width='15%'>".sizeof($recordID)."</td>
		<td class='field_title' width='35%'>".$Lang['iPortfolio']['OEA']['Add_CountText_2']."</td>
		<td width='15%'>".($currentPos-1)."</td>
	</tr>
	</table><br>";
	
$h_totalRecordSelected = sizeof($recordID);
$h_thisPos = $currentPos - 1;	
$h_percent = ceil(($h_thisPos/$h_totalRecordSelected)*100);
	
$h_savedRecord = ($currentPos-1);

# load temp parameters for display table 
$_objOEA_ITEM->setTempOLE_STUDENT_RecordID($_id);
$oea_start = substr($_objOLE_ITEM->getStartDate(),0,4);
$_objOEA_ITEM->setTempStartYear($oea_start);
$oea_end = ($_objOLE_ITEM->getEndDate()=="") ? $oea_start : substr($_objOLE_ITEM->getEndDate(),0,4);
$_objOEA_ITEM->setTempEndYear($oea_end);
$oea_Participation = ($_objOLE_ITEM->getIntExt() == 'INT')? 'S' : 'P';
$_objOEA_ITEM->setParticipation($oea_Participation);
//$oea_Description = $_objOLE_ITEM->getDetails();
$oea_Description = trim($_objOLE_ITEM->getProgramDetails());
if ($oea_Description == '') {
	$oea_Description = trim($_objOLE_ITEM->getDetails());
}
$_objOEA_ITEM->setDescription($oea_Description);

$mapped_OEA_Code = $liboea_setting->getDefaultMappedOEACode($ole_ProgramID);
if ($mapped_OEA_Code == $oea_cfg["OEA_Default_Category_Others"]) {
	$_objOEA_ITEM->setTitle($_objOLE_ITEM->getTitle());
}

# load table of OLE & OEA
$h_result = $_objOEA_ITEM->getOeaItemTable();

$js_css = $liboea->Include_JS_CSS();


# prepare js for display of OEA category
//	$default_oea_all = $liboea_setting->get_OEA_Item();
//	if(sizeof($default_oea_all)>0) {
//		$js_default_item_array = "var itemAry = new Array();\n";
//		foreach($default_oea_all as $_key=>$_ary) {
//			$js_default_item_array .= "itemAry[\"$_key\"] = [\"".$_ary['CatCode']."\",\"".$_ary['CatName']."\"];\n";
//		}
//	}
//	$h_js = $js_default_item_array."\n";
$h_js = $liboea->getOeaItemAryForJs();
	
	
# hidden field 
for($i = 0, $i_max = sizeof($recordID); $i< $i_max;$i++){
	$h_HiddenOLE_Item .= "<input type =\"hidden\" name=\"recordID[]\" id=\"recordID[]\" value=\"".$recordID[$i]."\">\n";
}
$h_HiddenOLE_Item .= "<input type=\"hidden\" name=\"currentRecordID\" id=\"currentRecordID\" value=\"$_id\">";
$h_HiddenOLE_Item .= "<input type=\"hidden\" name=\"ole_ProgramID\" id=\"ole_ProgramID\" value=\"$ole_ProgramID\">";
$h_HiddenOLE_Item .= "<input type=\"hidden\" name=\"currentPos\" id=\"currentPos\" value=\"$currentPos\">";
// commented by Ivan on 10 Nov 2011: This hidden form is defined in template.php
//$h_HiddenOLE_Item .= "<input type=\"hidden\" name=\"task\" id=\"task\" value=\"$task\">";

# button
if($currentPos < sizeof($recordID)) {
	$h_button = $linterface->GET_ACTION_BTN($Lang['iPortfolio']['OEA']['SaveAndNext'], "button", "doSave()", "submitBtn")."&nbsp;";
	$h_button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "js_Back_To_OEA_List()");

} else { 
	$h_button = $linterface->GET_ACTION_BTN($Lang['iPortfolio']['OEA']['SaveAndFinish'], "button", "doSave()", "submitBtn")."&nbsp;";
	$h_button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "js_Back_To_OEA_List()");
}



$linterface->LAYOUT_START();
include_once($template_script);
$linterface->LAYOUT_STOP();

?>
