<?php
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];

$liboea = new liboea();
$ObjStudent = new liboea_student($_SESSION['UserID']);

$ObjStudentSuppInfo = $ObjStudent->getSupplementaryInfo();
$SuppInfoID = $ObjStudentSuppInfo->getObjectID();
$SuppInfoCode = $ObjStudentSuppInfo->getObjectInternalCode();
$SuppInfoApprovalStatus = $ObjStudentSuppInfo->getRecordStatus();
$ApprovedStatus = $ObjStudentSuppInfo->get_approved_status();


### Status Info
if ($SuppInfoApprovalStatus === '' || $SuppInfoApprovalStatus == null) {
	// no record => do nth
}
else {
	// have record => display status
	$h_StatusDisplay = $liboea->getApprovalStatusDisplay($SuppInfoCode, $ObjStudentSuppInfo->getRecordStatus(), $ObjStudentSuppInfo->getApprovedBy(), $ObjStudentSuppInfo->getApprovalDate());
	$h_StatusDisplayDiv = '<div style="float:left;">'.$Lang['iPortfolio']['OEA']['ApprovalStatus'].': '.$h_StatusDisplay.'</div>'."\n";
}


### Edit Button (top right)
//$BtnArr = array();
//if ($SuppInfoApprovalStatus == $ApprovedStatus) {
//	// do nth
//}
//else {
//	$BtnArr[] = array('edit', "javascript:js_Edit_SuppInfo('$SuppInfoID');");
//}
//$h_ActionButtonDiv = '<div>'.$linterface->Get_DBTable_Action_Button_IP25($BtnArr).'</div>';


### Additional Info Content
$h_ContentTable = $ObjStudentSuppInfo->getViewTable();


### Edit Button
if ($SuppInfoApprovalStatus == $ApprovedStatus) {
	// do nth
}
else {
	$h_EditButton = $linterface->GET_ACTION_BTN($Lang['Btn']['Edit'], "button", "js_Edit_SuppInfo('$SuppInfoID');", $id="Btn_Edit");
}

		

$ReturnMsg = $Lang['iPortfolio']['OEA'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);
include_once($template_script);
$linterface->LAYOUT_STOP();
?>