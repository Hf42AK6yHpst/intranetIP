<?
# modifying : henry chow

$liboea = new liboea();
//$liboea_setting = new liboea_setting();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$sql = $liboea->getOEA_StudentList_SQL();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if($field=="") $field = 0;

$li = new libdbtable2007($field, $order, $pageNo);

$li->sql = $sql;
$li->field_array = array("TITLE", "CATEGORY", "ROLE", "ACHIEVEMENT");
$li->no_col = sizeof($li->field_array)+2;
$li->IsColOff = "OEA_StudentList";

# TABLE COLUMN
$pos = 0;
$li->column_list .= "<th width='1'>#</th>\n";
$li->column_list .= "<th width='30%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['iPortfolio']['OEA']['Title'])."</th>\n";
$li->column_list .= "<th width='20%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['iPortfolio']['OEA']['Category'])."</th>\n";
$li->column_list .= "<th width='20%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['iPortfolio']['OEA']['Role'])."</th>\n";
$li->column_list .= "<th width='30%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['iPortfolio']['OEA']['Achievement'])."</th>\n";
$li->column_list .= "<th width='1' class='tabletop tabletoplink'>".$li->check("programID[]")."</th>\n";

$h_content .= $li->display();

$h_msg_row = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";

$linterface->LAYOUT_START();
include_once("templates/index.tmpl.php");
$linterface->LAYOUT_STOP();
?>