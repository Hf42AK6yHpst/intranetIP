<?php
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];

$liboea = new liboea();
$ObjStudent = new liboea_student($_SESSION['UserID']);

$ObjStudentAddiInfo = $ObjStudent->getAdditionalInfo();
$AddiInfoID = $ObjStudentAddiInfo->getObjectID();
$AddiInfoCode = $ObjStudentAddiInfo->getObjectInternalCode();
$AddiInfoApprovalStatus = $ObjStudentAddiInfo->getRecordStatus();
$ApprovedStatus = $ObjStudentAddiInfo->get_approved_status();


### Status Info
if ($AddiInfoApprovalStatus === '' || $AddiInfoApprovalStatus == null) {
	// no record => do nth
}
else {
	// have record => display status
	$h_StatusDisplay = $liboea->getApprovalStatusDisplay($AddiInfoCode, $ObjStudentAddiInfo->getRecordStatus(), $ObjStudentAddiInfo->getApprovedBy(), $ObjStudentAddiInfo->getApprovalDate());
	$h_StatusDisplayDiv = '<div style="float:left;">'.$Lang['iPortfolio']['OEA']['ApprovalStatus'].': '.$h_StatusDisplay.'</div>'."\n";
}


### Edit Button (top right)
//$BtnArr = array();
//if ($AddiInfoApprovalStatus == $ApprovedStatus) {
//	// do nth
//}
//else {
//	$BtnArr[] = array('edit', "javascript:js_Edit_AddiInfo('$AddiInfoID');");
//}
//$h_ActionButtonDiv = '<div>'.$linterface->Get_DBTable_Action_Button_IP25($BtnArr).'</div>';


### Additional Info Content
$h_ContentTable = $ObjStudentAddiInfo->getViewTable();


### Edit Button
if ($AddiInfoApprovalStatus == $ApprovedStatus) {
	// do nth
}
else {
	$h_EditButton = $linterface->GET_ACTION_BTN($Lang['Btn']['Edit'], "button", "js_Edit_AddiInfo('$AddiInfoID');", $id="Btn_Edit");
}


### Include js and css
$h_IncludeJsCss = liboea::Include_JS_CSS();


$ReturnMsg = $Lang['iPortfolio']['OEA'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);
include_once($template_script);
$linterface->LAYOUT_STOP();
?>