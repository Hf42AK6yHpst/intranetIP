﻿<?
echo $h_TopTabMenu;
//echo '<br style="clear:both"/>'."\n";
echo $h_navigation;
//echo '<br style="clear:both"/>'."\n";
echo $js_css;
?>

<script language='javascript'>
function goRemove(obj,element,page) {
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else {
		if(confirm("<?=$h_confirm_delete_msg?>")) {
			document.getElementById('task').value = 'removeOEA_update';
			document.form1.action = page;
			goSubmit();
		}
	}
}

function goEdit(obj, element, page) {
	if(countChecked(obj,element)!=1)
		alert(globalAlertMsg1);
	else {
			document.getElementById('task').value = 'oea_edit_oea';
			document.form1.action = page;
			goSubmit();
	}
}

function goSubmit() {
	document.form1.submit();	
}

function goAdd(val) {
	if(val==1) {
		//$('input#task').val('addFromOLE');
		self.location.href = "index.php?task=addFromOLE";
	} else {
		//$('input#task').val('oea_new');
		self.location.href = "index.php?task=oea_new";
	}
	//document.form1.submit();
}


</script>

<?=$h_Disclaimer?>
<?=$h_TableText?>

<?=$h_new_button?>
<br style="clear:both" />
<form name ="form1" method="post" value="index.php" id="form1">
<? if($isSubmissionPeriod) { ?>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	
	<tr>
		<td height="30"><?=$participationSelect?></td>
		<td valign="bottom">
		
			<div class="common_table_tool">
				
				<a class="tool_edit" href="javascript:goEdit(document.form1,'programID[]','index.php')"><?=$Lang['Btn']['Edit']?></a>
				
				<a class="tool_delete" href="javascript:goRemove(document.form1,'programID[]','index.php')"><?=$Lang['Btn']['Delete']?></a>  
			</div>	
			
		</td>
	</tr>
</table>
<? } ?>
<div style="width:100%;">
<?=$h_content?>

<input type='hidden' name='page_size_change' id='page_size_change' value=''>
<input type='hidden' name='pageNo' id='pageNo' value='<?=$pageNo?>'>
<input type='hidden' name='order' id='order' value='<?=$order?>'>
<input type='hidden' name='field' id='field' value='<?=$field?>'>
<input type='hidden' name='numPerPage' id='numPerPage' value='<?=$numPerPage?>'>
<input type='hidden' name='task' id='task' value=''>
</div>	
</form>

<?=$h_result2?>

<script language="javascript">
<? if($msg!="") { ?>
Get_Return_Message("<?=$msg?>");	
<? } ?>
</script>