<?
echo $h_IncludeJsCss;
echo $h_navigation;
echo $h_TopTabMenu;
?>
<script language="javascript">
var jsMaxWordCount = '<?=liboea_additional_info::getDetailsMaxWordCount()?>';
var jsMaxCharCount = '<?=liboea_additional_info::getDetailsMaxLength()?>';

$(document).ready( function() {	
	//js_Update_Text_Counter(strip_tags($('div#AddiInfoDetailsDiv').html()), 'AddiInfo_WordCountSpan', jsMaxWordCount);
	//js_Update_Text_Counter(strip_tags($('div#AddiInfoDetailsDiv').html()), 'AddiInfo_WordCountSpan', jsMaxCharCount, 'char');
	//js_Update_Text_Counter(strip_tags($('textarea#AddiInfoDetailsTextarea').val()), 'AddiInfo_WordCountSpan', jsMaxCharCount, 'char');
	js_Update_Text_Counter(strip_tags($('textarea#AddiInfoDetailsTextarea').val()), 'AddiInfo_WordCountSpan', jsMaxCharCount, '');
});


function js_Edit_AddiInfo(jsAddiInfoID)
{
	jsAddiInfoID = jsAddiInfoID || '';
	window.location = "index.php?task=addiInfo_edit&AdditionalInfoID=" + jsAddiInfoID;
}
</script>

<form id="form1" name="form1" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="center">
				<?=$h_StatusDisplayDiv?>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center" class="stu_info_log">
							<?=$h_ContentTable?>
							<div class="edit_bottom_v30">
								<?=$h_EditButton?>
								<p class="spacer"></p>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
<br/>