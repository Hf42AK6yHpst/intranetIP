<?
echo $h_navigation;
echo $h_TopTabMenu;
?>
<script language="javascript">
$(document).ready( function() {	
	$('input#SuppInfoTitle').focus();
});

function js_Back_To_Supplementary_Info_List()
{
	window.location = "index.php?task=suppInfo_list";
}

function js_Save_Supplementary_Info()
{
//	if (Trim($('input#SuppInfoTitle').val()) == '')
//	{
//		alert('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['SuppInfo_Input_Title']?>');
//		$('input#SuppInfoTitle').focus();
//		return false;
//	}
	
	if (Trim($('textarea#SuppInfoDetails').val()) == '')
	{
		alert('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['SuppInfo_Input_Content']?>');
		$('textarea#SuppInfoDetails').focus();
		return false;
	}
	
	var jsScript = 'ajax_' + '<?=$h_JupasItemCode?>';
	$('input#task').val('ajax');
	$('input#script').val(jsScript);
	$('input#action').val('Save');
	
	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
	$.ajax({  
		type: "POST",  
		url: "index.php",
		data: jsSubmitString,  
		success: function(data) {
			var jsReturnMsgKey = '';
			if (data=='' || data==0) {
				Get_Return_Message('<?=$Lang['iPortfolio']['OEA']['SuppInfoSaveUnsuccessfully']?>');
			} else {
				window.location = "index.php?task=suppInfo_list&ReturnMsgKey=SuppInfoSaveSuccessfully";
				//$('input#ObjectID').val(data);
			}
		} 
	});  
	return false;
}
</script>
<form id="form1" name="form1" method="POST" action="index.php">
	<table id="html_body_frame" width="100%">
		<tr><td align="left" class="navigation"><?=$h_PageNavigation?></td></tr>
		<tr><td align="center"><?=$h_InstructionTable?></td></tr>
		<tr>
			<td align="center" valign="top" class="stu_info_log tabletext">
				<br style="clear:both;" />
				<?=$h_EditTable?>
				<br style="clear:both;" />
				<div class="edit_bottom_v30">
					<p class="spacer"></p>
					<?=$h_SaveButton?>
					<?=$h_BackButton?>
				</div>
			</td>
		</tr>
	</table>
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="script" name="script" value="" />
	<input type="hidden" id="action" name="action" value="" />
	<input type="hidden" id="ObjectID" name="ObjectID" value="<?=$SupplementaryInfoID?>" />
	<input type="hidden" id="StudentID" name="StudentID" value="<?=$_SESSION['UserID']?>" />
</form>
<br/>