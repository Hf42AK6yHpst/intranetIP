<?
#  Editing by 

echo $h_IncludeJsCss;
echo $h_navigation;
echo $h_TopTabMenu;
?>
<script language="javascript">
var jsMaxWordCount = parseInt('<?=liboea_additional_info::getDetailsMaxWordCount()?>');
var jsMaxCharCount = parseInt('<?=liboea_additional_info::getDetailsMaxLength()?>');

$(document).ready( function() {	
	$('textarea#AddiInfoDetails').focus();
	js_Onkeyup_Info_Textarea('AddiInfoDetails', 'AddiInfo_WordCountSpan', jsMaxWordCount, jsMaxCharCount);
	//js_Onkeyup_Info_Textarea('AddiInfoDetails', 'AddiInfo_WordCountSpan', jsMaxWordCount, jsMaxCharCount, 'char');
});

function js_Back_To_Additional_Info_List()
{
	window.location = "index.php?task=addiInfo_list";
}

//function js_Pressed_Info_Textarea(jsObjID, jsDisplaySpanID)
//{
//	js_Onkeyup_Info_Textarea(jsObjID, jsDisplaySpanID, jsMaxWordCount, jsMaxCharCount);
//}

function js_Save_Additional_Info()
{
//	if (Trim($('input#AddiInfoTitle').val()) == '')
//	{
//		alert('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_Input_Title']?>');
//		$('input#AddiInfoTitle').focus();
//		return false;
//	}
	
	var jsAddiInfoText = Trim($('textarea#AddiInfoDetails').val());
	
	if (jsAddiInfoText == '')
	{
		//alert('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_Input_Content']?>');
		$('textarea#AddiInfoDetails').focus();
		return false;
	}
	
	// Check character count -> cannot submit if exceeded limit
	var jsCharCount = parseInt(js_Get_Jupas_Character_Count(jsAddiInfoText));
	if (jsMaxCharCount > 0 && jsCharCount > jsMaxCharCount) {
		var jsWarningMsg = js_Get_Content_Exceed_Maximum_Warning_Msg('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_Greater_Than_The_Max']?>', jsMaxCharCount);
		alert(jsWarningMsg);
		$('textarea#AddiInfoDetails').focus();
		return false;
	}
	
	// Check word count -> still can save if the user confirm even if exceeded limit
	var jsWordCount = parseInt(js_Get_Word_Count(jsAddiInfoText));
	if (jsMaxWordCount > 0 && jsWordCount > jsMaxWordCount) {
		var jsWarningMsg = js_Get_Content_Exceed_Maximum_Warning_Msg('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_Greater_Than_The_Max_Confirm']?>', jsMaxWordCount);
		if (confirm(jsWarningMsg)) {
			// continue the submit process
		}
		else {
			$('textarea#AddiInfoDetails').focus();
			return false;
		}
	}
	
	var jsScript = 'ajax_' + '<?=$h_JupasItemCode?>';
	$('input#task').val('ajax');
	$('input#script').val(jsScript);
	$('input#action').val('Save');
	
	$('input#Btn_Save').attr('disabled', 'disabled');
	var jsSubmitString = Get_Form_Values(document.getElementById('form1')) + '&checkPending=1';
	$.ajax({  
		type: "POST",  
		url: "index.php",
		data: jsSubmitString,  
		success: function(data) {
			var jsReturnMsgKey = '';
			if (data=='' || data==0) {
				$('input#Btn_Save').attr('disabled', '');
				Get_Return_Message('<?=$Lang['iPortfolio']['OEA']['AddiInfoSaveUnsuccessfully']?>');
			} else {
				window.location = "index.php?task=addiInfo_list&ReturnMsgKey=AddiInfoSaveSuccessfully";
				//$('input#ObjectID').val(data);
			}
		} 
	});  
	return false;
}

function showSelfAccountInfoDetails(selfAccountDivID){		
	var curr_status = $("#selfAccount_"+selfAccountDivID).attr("class");

	if(curr_status == "show"){
		$("#selfAccount_"+selfAccountDivID).hide();
		$("#selfAccount_"+selfAccountDivID).attr("class","hide");
	}else{
		$("#selfAccount_"+selfAccountDivID).show();
		$("#selfAccount_"+selfAccountDivID).attr("class","show");
	}
}	
</script>
<form id="form1" name="form1" method="POST" action="index.php">
	<table id="html_body_frame" width="100%">
		<tr><td align="left" class="navigation"><?=$h_PageNavigation?></td></tr>
		<tr><td align="center"><?=$h_InstructionTable?></td></tr>
		<tr>
			<td align="center" valign="top" class="stu_info_log tabletext">
				<br style="clear:both;" />
				<?=$h_EditTable?>
				<br style="clear:both;" />
				<div class="edit_bottom_v30">
					<p class="spacer"></p>
					<?=$h_SaveButton?>
					<?=$h_BackButton?>
				</div>
			</td>
		</tr>
	</table>
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="script" name="script" value="" />
	<input type="hidden" id="action" name="action" value="" />
	<input type="hidden" id="ObjectID" name="ObjectID" value="<?=$AdditionalInfoID?>" />
	<input type="hidden" id="StudentID" name="StudentID" value="<?=$_SESSION['UserID']?>" />
</form>
<br/>