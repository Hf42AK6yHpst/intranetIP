<?
# modifying : 

echo $h_TopTabMenu;
echo '<br style="clear:both"/>'."\n";
echo $h_navigation;
echo '<br style="clear:both"/>'."\n";
/*
echo $step_navigation;
echo '<br style="clear:both"/>'."\n";
*/
echo $h_msg_row;

?>
<script language="javascript">
function js_Back_To_OEA_List() {
	self.location.href = "index.php";	
}

function goAdd() {
	var recordSelected = countChecked(document.form1, 'recordID[]');
	if(recordSelected > '<?=$OeaQuotaRemain?>') {
		alert("<?=$Lang['iPortfolio']['OEA']['OeaQuotaIsNotEnough']?>");	
	} else {
		if(confirm("<?=$Lang['iPortfolio']['OEA']['jsWarning']['StartToFillInDetails']?>")) {
			checkEditMultiple(document.form1,'recordID[]','index.php');
		}
	}
}

function countTotal() {
	var currentTotal = countChecked(document.form1, 'recordID[]');
	
	$('#thisTotal').val(currentTotal);
	$('#TotalSpan').html(currentTotal);
}

function showSpan(layername) {
	$('#'+layername).show('slow');	
}

function hideSpan(layername) {
	$('#'+layername).hide('slow');	
}

function ShowHideLayer(fieldname, layername) {
	if(document.getElementById(fieldname).value=='' || document.getElementById(fieldname).value==0) {
		$('#'+fieldname).val(1);
		showSpan(layername);	
	} else {
		$('#'+fieldname).val(0);
		hideSpan(layername);			
	}
}
</script>

<form name ="form1" method="post" value="index.php" id="form1">
<?=$h_instructionBox?>
<?=$h_QuotaText?>
<?=$h_selected_layer?>
<?=$h_content?>
<br style="clear:both">
<div style="width:100%">
	<span  class="tabletextrequire">*</span> <span class="tabletextremark"><?=$Lang['iPortfolio']['OEA']['Self_Mapped']?></span>
	<div class="edit_bottom_v30">
	<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Add'], "button", "goAdd()")?>
	<?=$linterface->GET_ACTION_BTN($button_cancel, "button", "js_Back_To_OEA_List()")?>
	</div>
</div>
<input type='hidden' name='task' id='task' value='slpToOea'>
<input type='hidden' name='thisTotal' id='thisTotal' value='0'>
<input type='hidden' name='selectedOeaFlag' id='selectedOeaFlag' value='0'>
</form>
