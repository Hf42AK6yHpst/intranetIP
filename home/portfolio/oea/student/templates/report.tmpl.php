<?
echo $h_TopTabMenu;

?>
<script language="javascript">
$(document).ready( function() {	

});


function jsGoPrint() {
	$('input#task').val('report_print');
	$('input#printMode').val('<?=$oea_cfg['printMode']['html']?>');
	$('form#form1').attr('target', '_blank').submit();
}

function jsGoExport() {
	$('input#task').val('report_print');
	$('input#printMode').val('<?=$oea_cfg['printMode']['csv']?>');
	$('form#form1').attr('target', '_self').submit();
}
</script>

<br />
<form id="form1" name="form1" method="POST" action="index.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td>
			<?= $linterface->Get_Warning_Message_Box($eDiscipline["Instruction"], $ec_warning['OeaReportPrinting_instruction_for_student'], "")?>
				<!--
				<div class="table_board">
					<table width="100%" class="form_table_v30">
						<col class="field_title">
						<col class="field_c">
						<tr>
							<td class="field_title"><?=$Lang['iPortfolio']['OEA']['ReportArr']['IncludeOleData']?></td>
							<td><input type="checkbox" id="includeOleData" name="includeOleData" value="1" /></td>
						</tr>
					</table>
				</div>
				-->
				<br style="clear:both;" />
				
				
				<div class="edit_bottom_v30">
					<?=$h_buttonPrint?>
					<?=$h_buttonExport?>
				</div>
			</td>
		</tr>
	</table>
	
	<input type='hidden' name='task' id='task' value='' />
	<input type='hidden' name='script' id='script' value='' />
	<input type='hidden' name='action' id='action' value='' />
	<input type='hidden' name='printMode' id='printMode' value='' />
	
</form>
<br/>