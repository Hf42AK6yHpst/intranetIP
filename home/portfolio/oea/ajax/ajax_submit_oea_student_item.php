<?php
# modifying : ivan
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_item.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/oeaConfig.inc.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

intranet_opendb();

$liboea = new liboea();
$liboea_setting = new liboea_setting();


if ($recordID == '') {
	$isEdit = false;
}
else if(!is_array($recordID)) {			# if $recordID is an array, $record from "New From OLE Record"
	$editRecordID = $recordID;
	$isEdit = true;
}
else {
	$isEdit = false;
}


$_objOEA_ITEM = new liboea_item($editRecordID);
if ($isEdit) {
	$StudentID = $_objOEA_ITEM->getStudentID();
}
else {
	$StudentID = $_SESSION['UserID'];
}


### Check if the record can be submit or not
$canSubmit = true;
if ($_SESSION['UserType'] == USERTYPE_STAFF) {
	$canSubmit = true;
}
else {
	$maxNumOfOea = $liboea->getMaxNoOfOea();
	$maxNumOfOea = ($isEdit)? $maxNumOfOea + 1 : $maxNumOfOea;
	$studentNumOfOeaApplied = count($liboea->getStudentOeaItemInfoArr($StudentID));
	
	if ($studentNumOfOeaApplied >= $maxNumOfOea) {
		//No_Access_Right_Pop_Up();
		$echoString = $Lang['iPortfolio']['OEA']['OeaNumExceedLimit'];
		$canSubmit = false;
	}
	
	if ($liboea->getIsSubmissionPeriod()==false) {
		$echoString = $Lang['iPortfolio']['OEA']['SubmissionPeriodHasPast'];
		$canSubmit = false;
	}
}

if ($canSubmit) {
	$oeaItemAry = $liboea_setting->get_OEA_Default_Item($returnAssociAry=1);
	
	$dataAry = array();
	if ($AchievementID=="") {
		$AchievementID = "N";
	}
	
	if($ItemCode==$oea_cfg["OEA_Default_Category_Others"]) {
		$oeaTitle = ($oea_title=="") ? $stored_oea_title : $oea_title;
		$oeaTitle = trim(stripslashes($oeaTitle));
		//$Description = trim(stripslashes($Description));
	}
	else { 
		$oeaTitle = $oeaItemAry[$ItemCode];
		//$Description = '';
	}
	$Description = trim(stripslashes($Description));
	
	
	//$autoApproval = $liboea->getOeaAutoApproval();
	//// $recordStatus = ($autoApproval) ? $oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"] : $oea_cfg["OEA_STUDENT_RECORDSTATUS_PENDING"];
	//if ($autoApproval || $Participation=='P') {
	//	$recordStatus = $oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"];
	//}
	//else {
	//	$recordStatus = $oea_cfg["OEA_STUDENT_RECORDSTATUS_PENDING"];
	//}
	
	$autoApprovalPart1 = $liboea->getOeaAutoApproval();
	$autoApprovalPart2 = $liboea->getOeaAutoApprovalPart2();
	if($_SESSION['UserType']==USERTYPE_STUDENT) {
		if ($Participation=='S') {
			if ($autoApprovalPart1) {
				$recordStatus = $oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"];
			}
			else {
				$recordStatus = $oea_cfg["OEA_STUDENT_RECORDSTATUS_PENDING"];
			}
		}
		else if ($Participation=='P') {
			if ($autoApprovalPart2) {
				$recordStatus = $oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"];
			}
			else {
				$recordStatus = $oea_cfg["OEA_STUDENT_RECORDSTATUS_PENDING"];
			}
		}
	} else {
		// Auto change to approved if teacher edited the OEA
		$recordStatus = $oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"];
	}
	
	if ($recordStatus == $oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"]) {
		$_objOEA_ITEM->setTempApprovalDate('now()');
		$_objOEA_ITEM->setTempApprovedBy($UserID);
	}
	
	
	$_objOEA_ITEM->setTempStudentID($UserID);
	$_objOEA_ITEM->setTempTitle($oeaTitle);
	$_objOEA_ITEM->setTempStartYear($startdate);
	$_objOEA_ITEM->setTempEndYear($enddate);
	$_objOEA_ITEM->setTempAwardBearing($awardBearing);
	$_objOEA_ITEM->setTempParticipation($Participation);
	$_objOEA_ITEM->setTempParticipationNature($ParticipationNature);
	$_objOEA_ITEM->setTempRole($RoleID);
	$_objOEA_ITEM->setTempAchievement($AchievementID);
	$_objOEA_ITEM->setTempOEA_ProgramCode($ItemCode);
	$_objOEA_ITEM->setTempOLE_STUDENT_RecordID($currentRecordID);
	$_objOEA_ITEM->setTempOLE_PROGRAM_ProgramID($ole_ProgramID);
	$_objOEA_ITEM->setTempRecordStatus($recordStatus);
	$_objOEA_ITEM->setTempDescription($Description);
	
	$result = $_objOEA_ITEM->save();
	
	intranet_closedb();
	
	$echoString = $result? $Lang['iPortfolio']['OEA']['OeaItemSaveSuccessfully'] : $Lang['iPortfolio']['OEA']['OeaItemSaveUnsuccessfully'];
}
	

echo $echoString;
?>