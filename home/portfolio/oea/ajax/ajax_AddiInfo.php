<?php
$action = trim(urldecode(stripslashes($_REQUEST['action'])));
$ObjectID = IntegerSafe(trim(urldecode(stripslashes($_REQUEST['ObjectID']))));
$Obj = new liboea_additional_info($ObjectID);

if ($action == 'Save')
{
	$Title = trim(urldecode(stripslashes($_REQUEST['AddiInfoTitle'])));
	$Details = trim(urldecode(stripslashes($_REQUEST['AddiInfoDetails'])));
	$StudentID = IntegerSafe(trim(urldecode(stripslashes($_REQUEST['StudentID']))));
	$checkPending = $_REQUEST['checkPending'];
	
	if ($ck_memberType == 'S' && $ck_intranet_user_id != $StudentID) {
		echo '0';
	}
	else {
		$Obj->setTitle($Title);
		$Obj->setDetails($Details);
		
		if ($ObjectID == '') {
			$Obj->setStudentID($StudentID);
		}
		
		if ($checkPending) {
			$RecordStatus = $Obj->getRecordStatus();
			if ($RecordStatus == $Obj->get_rejected_status()) {
				$Obj->setRecordStatus($Obj->get_pending_status());
			}
		}
		
		$ResultID = $Obj->save();
		echo $ResultID;
	}
}
else if ($action == 'Load_View_Mode')
{
	echo $Obj->getViewTable();
}
else if ($action == 'Load_Edit_Mode')
{
	echo $Obj->getEditTable();
}
else if ($action == 'Approve')
{
	if ($ObjectID == '') {
		$Obj->setStudentID($StudentID);
	}
	
	$success = $Obj->approve_record();
	echo ($success)? '1' : '0';
}
else if ($action == 'Reject')
{
	if ($ObjectID == '') {
		$Obj->setStudentID($StudentID);
	}
	
	$success = $Obj->reject_record();
	echo ($success)? '1' : '0';
}
else if ($action == 'Load_Approval_Status_Display')
{
	$liboea = new liboea();
	echo $liboea->getApprovalStatusDisplay($Obj->getObjectInternalCode(), $Obj->getRecordStatus(), $Obj->getApprovedBy(), $Obj->getApprovalDate());
}

?>