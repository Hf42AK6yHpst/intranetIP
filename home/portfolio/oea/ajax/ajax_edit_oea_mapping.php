<script>
$(document).ready( function() {
	// enable to display the div in table genenerate from f:getOeaItemTable (div:TableDiv)
	$('#TableDiv').show();
});
</script>
<?

$liboea_setting = new liboea_setting();
$_objOEA_ITEM = new liboea_item();
$liboea = new liboea();

$forOLEEdit = (isset($_POST['IsForOLEEdit']))? $_POST['IsForOLEEdit'] : true;
$fromJupasMappingSettings = $_POST['fromJupasMappingSettings'];

$r_programid = $_POST['r_programid'];
$engTitle = $_POST['engTitle'];
if ($engTitle == '' && $r_programid != '') {
	include_once($PATH_WRT_ROOT.'includes/portfolio25/slp/ole_program.php');
	$objOleProgram = new ole_program($r_programid);
	$engTitle = $objOleProgram->getTitle();
}

if ($r_programid!="")
{
	$_objOEA_ITEM->setEditMode(1);
	$_objOEA_ITEM->setNewRecord($isNewRecord=1);
	$IsOEAMapped = $_objOEA_ITEM->loadRecordFromOLEMapping($r_programid);
	
	if ($IsOEAMapped && $_objOEA_ITEM->getOEA_ProgramCode()==$oea_cfg["OEA_Default_Category_Others"])
	{
		$_objOEA_ITEM->setTitle( $Lang['iPortfolio']['OEA']['UseOleName']. " (".$engTitle.")");
	}
	$OEAMappingChecked = ($IsOEAMapped) ? " checked='checked' " : "";
} else
{

	$_objOEA_ITEM->setNewRecord($isNewRecord=1);
}

$h_result = '';
$h_result .= $_objOEA_ITEM->getOeaItemTable($forOLEEdit, $engTitle, $r_programid, $fromJupasMappingSettings);
if ($fromJupasMappingSettings) {
	$disabledDeleteBtn = ($IsOEAMapped)? false : true;
	$h_result .= "<div class=\"edit_bottom_v30\" id='buttonDiv'>";
		$h_result .= $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "doSave()", "submitBtn")."&nbsp;";
		$h_result .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "resetAdvanceLayerDisplay();window.top.tb_remove()", "cancelBtn")."&nbsp;";
		$h_result .= $linterface->GET_ACTION_BTN($Lang['Btn']['Delete'], "button", "doDelete()", "submitBtn", $ParOtherAttribute="", $disabledDeleteBtn, "formbutton_alert");
	$h_result .= "</div>";
}

echo $h_result;


?>