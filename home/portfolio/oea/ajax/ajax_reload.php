<?php
$action = trim(urldecode(stripslashes($_POST['action'])));

$liboea = new liboea();

if ($action == 'Settings_OLE_Mapping_DB_Table')
{
	$field = trim(stripslashes($_POST['field']));
	$order = trim(stripslashes($_POST['order']));
	$pageNo = trim(stripslashes($_POST['pageNo']));
	$Keyword = trim(stripslashes($_POST['Keyword']));
	$AcademicYearID = trim(stripslashes($_POST['AcademicYearID']));
	$ProgramNature = trim(stripslashes($_POST['ProgramNature']));
	$MappingStatus = trim(stripslashes($_POST['MappingStatus']));
	
	echo $liboea->Get_Settings_OLE_Mapping_Index_DBTable($field, $order, $pageNo, $ForExport=0, $Keyword, $AcademicYearID, $ProgramNature, $MappingStatus);
}
else if ($action == 'Settings_Academic_Subject')
{
	$SubjectJupasStatus = trim(urldecode(stripslashes($_POST['SubjectJupasStatus'])));
	
	$liboea_academic = new liboea_academic();
	echo $liboea_academic->Get_Subject_Settings_Table($SubjectJupasStatus);
}
else if ($action == 'Jupas_Basic_Settings_View_Mode_Table') {
	echo $liboea->Get_Jupas_Basic_Settings_View_Mode_Table();
}
else if ($action == 'Jupas_Basic_Settings_Edit_Mode_Table') {
	echo $liboea->Get_Jupas_Basic_Settings_Edit_Mode_Table();
}
else if ($action == 'studentSelection') {
	include_once($PATH_WRT_ROOT.'includes/form_class_manage_ui.php');
	$libfcm = new form_class_manage();
	$libfcm_ui = new form_class_manage_ui();
	
	$YearClassID = $_POST['YearClassID'];
	$SelectionID = $_POST['SelectionID'];
	
	$isSuperAdmin = $lpf->IS_IPF_SUPERADMIN();
	$returnTeachingClassOnly = ($isSuperAdmin || strstr($ck_function_rights, "Jupas:All"))? 0 : 1;
	
	### Class Selection
	$YearClassIDSelected = $YearClassID; 
	$YearClassIDSelected = ($YearClassIDSelected=='0')? '' : $YearClassIDSelected;
	if (substr($YearClassIDSelected, 0, 2) == '::') {
		// selected a Class
		$YearID = '';
		$YearClassIDArr = array(str_replace('::', '', $YearClassIDSelected));
	}
	else {
		// selected a Form
		$YearID = $YearClassIDSelected;
		if ($YearID == '') {
			// All Forms => Get All Applicable Forms of Jupas
			$YearID = $liboea->getJupasApplicableFormIDArr();
		}
		$YearClassInfoArr = $libfcm->Get_Class_List_By_Academic_Year(Get_Current_Academic_Year_ID(), $YearID, $returnTeachingClassOnly);
		$YearClassIDArr = Get_Array_By_Key($YearClassInfoArr, 'YearClassID');
	}
	
	$iPfStudentInfoArr = $lpf->Get_Student_With_iPortfolio($YearClassIDArr, 0);
	$iPfStudentIDArr = Get_Array_By_Key($iPfStudentInfoArr, 'UserID');
	
	$studentSelection = $libfcm_ui->Get_Student_Selection($SelectionID, $YearClassIDArr, $SelectedStudentID, '', $noFirst=1, $isMultiple=1, $isAll=0, $ExcludedStudentIDArr='', $iPfStudentIDArr);
	$selectAllBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('".$SelectionID."', 1);");
	
	echo $linterface->Get_MultipleSelection_And_SelectAll_Div($studentSelection, $selectAllBtn, $SpanID='');
}
?>