<?
$keyword = trim(urldecode(stripslashes($_POST['keyword'])));
$PageNo = trim(stripslashes($_POST['PageNo']));
$NumPerPage = trim(stripslashes($_POST['NumPerPage']));

//error_log("11 PageNo --> ".$PageNo." NumPerPage -->".$NumPerPage."\n", 3, "/tmp/aaa.txt");
$PageNo = (is_numeric($PageNo)) ? $PageNo : 1;
$NumPerPage = (is_numeric($NumPerPage))? $NumPerPage : $oea_cfg['defaultNumPerPage'] ;



$liboea_setting = new liboea_setting();
$liboea_setting_search = new liboea_setting_search($liboea_setting);

$default_oea_item = $liboea_setting->get_OEA_Default_Item($associateAry=1);
# prepare available array for advance search
$availableAry = $liboea_setting->getOEADefaultItemByCategory($CatCode);

# set available array for advance search
$liboea_setting_search->setAdvanceAvaliableSearchAry($availableAry);

# set "keyword" to advance search 
$liboea_setting_search->setAdvanceSearchTextStr($keyword);

$liboea_setting_search->getAdvanceSearchTextResult();
//error_log("PageNo --> ".$PageNo." NumPerPage -->".$NumPerPage."\n", 3, "/tmp/aaa.txt");
echo $liboea_setting_search->displayAdvanceSearchResult($PageNo, $NumPerPage);

?>