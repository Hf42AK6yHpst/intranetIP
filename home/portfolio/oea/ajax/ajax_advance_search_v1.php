<?php
# modifying : henry chow

//$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
		
$linterface = new interface_html();
		
$liboea_setting = new liboea_setting();

$default_oea_category = $liboea_setting->get_OEA_Default_Category($associateAry=0);

/*
$delim = "";
$jsKeyword = "";
for($i=0, $i_max=sizeof($default_oea_item); $i<$i_max; $i++) {
	$jsKeyword .= $delim.'"'.intranet_htmlspecialchars($default_oea_item[$i][1]).'"';
	$delim = ", ";
}
if($jsKeyword=="") $jsKeyword = '""';
*/
$categorySelect = getSelectByArray($default_oea_category, 'name="CatCode2" id="CatCode2" onChange="goGenerateResult()"', $CatCode2, 0, 0, $i_general_all);

$x = "<form name='form1' id='form1'>";
$x .= "<table width='90%'>";
$x .= "<tr>";
$x .= "<td>".$Lang['iPortfolio']['OEA']['ByKeyword']."</td>";
$x .= "<td><input type='text' name='keyword' id='keyword' class='textbox_name' onKeyUp='goGenerateResult()'></td>";
$x .= "</tr>";
$x .= "<tr>";
$x .= "<td>".$Lang['iPortfolio']['OEA']['ByCategory']."</td>";
$x .= "<td>$categorySelect</td>";
$x .= "</tr>";
$x .= "</table>";
$x .= "<div class='edit_bottom_v30'>";
$x .= $linterface->GET_ACTION_BTN($button_search, "button", "goGenerateResult()");
$x .= "</div>";
$x .= "<div id='ResultSpan' style='width:700px;height:300px;overflow:auto;'></div>";
$x .= "</form>";

intranet_closedb();

echo $x;
?>
<script language="javascript">
function goGenerateResult() {
	document.getElementById('ResultSpan').innerHTML = "<?=$i_general_loading?>";
	
	wordXmlHttp = GetXmlHttpObject();
   
	if (wordXmlHttp == null)
	{
		alert (errAjax);
		return;
	} 
    
	var url = '../ajax/ajax_advance_search_result_table.php';
	var postContent = 'CatCode='+document.getElementById('CatCode2').value;
	postContent += '&keyword='+document.getElementById('keyword').value;
	postContent += '&PATH_WRT_ROOT=<?=$PATH_WRT_ROOT?>';
	
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
			
			document.getElementById('ResultSpan').innerHTML = wordXmlHttp.responseText;
			document.getElementById('ResultSpan').style.visibility = 'visible';
			document.getElementById('ResultSpan').style.display = 'inline';
		} 
	};
	wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
	
}

function copyback(code) {
	
	Hide_Table('suggestionTable', 'Stud_Hide_Btn', 'Stud_Show_Btn');
	clearRadioSelection();
	checkOEAcategory(code);	
	
	document.getElementById('selectedItemSpan').innerHTML = "<input type='radio' name='ItemCode' value='"+code+"' id='ItemCode_"+code+"' checked onClick='checkOEAcategory(this.value)'><label for='ItemCode_"+code+"'>"+document.getElementById('hiddenItemName').value+"</label><br><br>";
	window.top.tb_remove();
}

function clearRadioSelection() {
	$('input[name="ItemCode"]').attr('checked', false);	
	$('input#ItemCode').val('');
}

function assignItemToTemp(tempCode, tempName) {
	$('input#hiddenItemCode').val(tempCode);
	$('input#hiddenItemName').val(tempName);
}
</script>
