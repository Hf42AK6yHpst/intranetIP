<?php
//using : ivan
$action = trim(urldecode(stripslashes($_REQUEST['action'])));

$liboea = new liboea();

if ($action == 'Import_OleOeaItemMapping')
{
	$OverrideStudentOea = $_REQUEST['OverrideStudentOea'];
	$liboea_setting = new liboea_setting();
	
	$liboea->Start_Trans();
	
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	
	$ImportColumnInfoArr = $liboea->getOLEMappingImportColumnInfoArray();
	$numOfDBColumn = count($ImportColumnInfoArr);
	$ImportDataArr = $liboea->Get_Import_OLE_OEA_Mapping_Temp_Data();
	$numOfImportData = count($ImportDataArr);
	
	$InsertDataArr = array();
	for ($i=0; $i<$numOfImportData; $i++)
	{
		$_TempID = $ImportDataArr[$i]['TempID'];
		$_RowNumber = $ImportDataArr[$i]['RowNumber'];
		$_OLE_ProgramID = $ImportDataArr[$i]['OLE_ProgramID'];
		$_OLE_ProgramNature = $ImportDataArr[$i]['OLE_ProgramNature'];
		$_OEA_ProgramCode = $ImportDataArr[$i]['OEA_ProgramCode'];
		$_OEA_AwardBearing = $ImportDataArr[$i]['OEA_AwardBearing'];
		
		$_MappingDataArr = array();
		$_MappingDataArr['OEA_ProgramCode'] = $_OEA_ProgramCode;
		$_MappingDataArr['OLE_ProgramID'] = $_OLE_ProgramID;
		$_MappingDataArr['DefaultAwardBearing'] = $_OEA_AwardBearing;
		$_MappingDataArr['DefaultParticipation'] = $_OLE_ProgramNature;
		
		if ($liboea->Check_If_OLE_OEA_Mapping_Exist($_OLE_ProgramID)) {
			// update
			$SuccessArr[$_RowNumber] = $liboea->Update_OLE_OEA_Mapping($_OLE_ProgramID, $_MappingDataArr);
		}
		else {
			// insert
			$SuccessArr[$_RowNumber] = $liboea->Insert_OLE_OEA_Mapping($_MappingDataArr);
		}
		
		// Update the corresponding mapped OEA records
		if ($OverrideStudentOea) {
			$SuccessArr['OverrideStudentMapping'] = $liboea->Update_OLE_OEA_Mapping_Student_Mapped_OEA($_OLE_ProgramID);
		}
		
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Processed_Number_Span").innerHTML = "'.($i + 1).'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		Flush_Screen_Display(1);
	}
	
	
	### Delete the temp data first
	$SuccessArr['DeleteTempData'] = $liboea->Delete_Import_OLE_OEA_Mapping_Temp_Data();
	
	
	### Roll Back data if import failed
	if (in_multi_array(false, $SuccessArr))
	{
		$liboea->RollBack_Trans();
		$ImportStatus = $Lang['iPortfolio']['OEA']['ImportMappingFailed'];
	}
	else
	{
		$liboea->Commit_Trans();
		$ImportStatus = $Lang['iPortfolio']['OEA']['ImportMappingSuccess'];
	}
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ImportStatusSpan").innerHTML = "'.$ImportStatus.'";';
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}
else if ($action == 'AcademicSettings') {
	$AcademicYearID		= $_POST['AcademicYearID'];
	$YearTermID			= $_POST['YearTermID'];
	$overAllSetting		= $_POST['overAllMapping'];
	
	
	$tmpAry = array();
	foreach ((array)$overAllSetting as $code=>$setValue){
		$tmpAry[] = $code.$oea_cfg["Setting"]["Default_ModuleInUse_Separator"].$setValue;
	}

	$overAllMappingStr = '';	
	$overAllMappingStr = implode($oea_cfg["Setting"]["Default_Separator2"],$tmpAry);
	
	$liboea->setAcademic_AcademicYearID($AcademicYearID);
	$liboea->setAcademic_YearTermID($YearTermID);
	$liboea->setAcademic_PercentileMapWithOverAllRating($overAllMappingStr);

	$SuccessArr = $liboea->saveSetting();
	echo (in_array(false, (array)$SuccessArr))? '0' : '1';
}
else if ($action == 'AcademicSubject') {
	$JupasSubjectCodeArr = $_POST['JupasSubjectCodeArr'];
	$ApplyToJupasArr = $_POST['ApplyToJupasArr'];
	
	$liboea_academic = new liboea_academic();
	
	$Success = $liboea_academic->Save_Subject_Info($JupasSubjectCodeArr, $ApplyToJupasArr);
	echo ($Success)? '1' : '0';
}
else if ($action == 'JupasBasicSettings') {
	$SchoolCode = trim(urldecode(stripslashes($_POST['SchoolCode'])));
	
	$JupasApplicableFormIDArr = $_POST['FormIDArr'];
	$JupasApplicableFormIDList = implode(',', (array)$JupasApplicableFormIDArr);
	
	$ModuleInUseArr = $_POST['ModuleInUseArr'];
	$ModuleInUseList = implode($oea_cfg["Setting"]["Default_ModuleInUse_Separator"], (array)$ModuleInUseArr);
	
	$liboea->setSchoolCode($SchoolCode);
	$liboea->setJupasApplicableFormIDList($JupasApplicableFormIDList);
	$liboea->setModuleInUse($ModuleInUseList);
	
	$SuccessArr = $liboea->saveSetting();
	echo (in_array(false, (array)$SuccessArr))? '0' : '1';
}
?>