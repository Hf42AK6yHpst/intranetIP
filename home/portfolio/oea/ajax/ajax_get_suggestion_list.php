<?php
# modifying : henry chow

$ole_title = trim($_POST['ole_title']);
$oleProgramId = $_POST['oleProgramId'];
if ($ole_title == '' && $oleProgramId != '') {
	// get the title by program id
	include_once($PATH_WRT_ROOT.'includes/portfolio25/slp/ole_program.php');
	$objOleProgram = new ole_program($oleProgramId);
	$ole_title = $objOleProgram->getTitle();
}

$linterface = new interface_html();

$liboea_setting = new liboea_setting();
$liboea_setting_search = new liboea_setting_search($liboea_setting);

$default_oea_category = $liboea_setting->get_OEA_Default_Category($associateAry=0);

	$liboea_setting_search->setCriteriaTitle($ole_title);
	$liboea_setting_search->getSuggestResult();
	$oea_itemSelect = $liboea_setting_search->loadSuggestResultDisplay();


$categorySelect = getSelectByArray($default_oea_category, 'name="CatCode2" id="CatCode2" onChange="goGenerateResult()"', $CatCode2, 0, 0, $i_general_all);

$x = "<form name='form2' id='form2'>";
$x .= "<div style='width:100%;height:410px;overflow:auto'>";
$x .= "<table width='95%'>";
$x .= "<tr>";
$x .= "<td>".$Lang['iPortfolio']['OEA']['OLE_Title']." : $ole_title<br style='clear:both'><br style='clear:both'></td>";
$x .= "</tr>";
$x .= "<tr>";
$x .= "<td>$oea_itemSelect</td>";
$x .= "</tr>";
$x .= "</table>";
$x .= "</div>";
$x .= "<div class='edit_bottom_v30'>";
if($liboea_setting_search->getResultsetSize()>0) {
	//$button .= $linterface->GET_ACTION_BTN($button_submit, "button", "copyback(document.form2.hiddenItemCode.value)")." ";
}
$button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.top.tb_remove()")." ";
$x .= $button;

if (!$IsForOLEEdit && $_SESSION['UserType']==USERTYPE_STAFF)
{
	$x .= $linterface->GET_ACTION_BTN($button_previous_page, "button", "loadPreviousPage()");
}


$x .= "</div>";
/*
$x .= "<div id='ResultSpan' style='width:700px;height:300px;overflow:auto;'></div>";
*/
$x .= "</form>";
intranet_closedb();

echo $x;
?>
<script language="javascript">

<? if($IsForOLEEdit || $_SESSION['UserType']==USERTYPE_STUDENT) { ?>
function copyback(code) {
	//clearRadioSelection();
	$('input#ItemCode').val(code);
	checkOEAcategory(code);	
	
	document.getElementById('selectedItemSpan').innerHTML = "<b><label for='ItemCode_"+code+"'>"+document.getElementById('hiddenItemName').value+"</label></b>";
	$('#MapOptions').hide('slow');
	$('#divPencil').show();
	//$('textarea#Description').attr('readonly', 'readonly');
	window.tb_remove();
}
<? } ?>

function clearRadioSelection() {
	$('input[name="ItemCode"]').attr('checked', false);	
	$('input#ItemCode').val('');
}

function assignItemToTemp(tempCode, tempName) {
	$('input#hiddenItemCode').val(tempCode);
	$('input#hiddenItemName').val(tempName);
}
</script>
