<?php
### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_ipf_oea_teacher_mgmt_YearClassID", "YearClassID");
$arrCookies[] = array("ck_ipf_oea_teacher_mgmt_TargetApprovalStatus", "TargetApprovalStatus");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$YearClassID = '';
	$TargetApprovalStatus = '';
}
else 
	updateGetCookies($arrCookies);

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;



$ReturnMsgKey = trim(stripslashes($_REQUEST['ReturnMsgKey']));


$liboea = new liboea();
$libpf_ui = new libportfolio_ui();
$libclass = new libclass();
$libfcm = new form_class_manage();
$libfcm_ui = new form_class_manage_ui();


$isSuperAdmin = $lpf->IS_IPF_SUPERADMIN();
$returnTeachingClassOnly = ($isSuperAdmin || strstr($ck_function_rights, "Jupas:All"))? 0 : 1;


### Disclaimer
//$h_Disclaimer = $linterface->Get_Warning_Message_Box('<span class="tabletextrequire">'.$Lang['iPortfolio']['OEA']['Disclaimer'].'</span>', $Lang['iPortfolio']['OEA']['Disclaimer_TeacherView']);

//$h_forumMessage = $linterface->Get_Warning_Message_Box('<span class="tabletextrequire">'.$Lang['iPortfolio']['OEA']['discussForumCaption'].'</span>', $Lang['iPortfolio']['OEA']['discussForumMessage']);

### Export Icon
$h_ExportIcon = $linterface->Get_Content_Tool_v30('export', 'javascript:js_Go_Export();');
if($sys_custom['iPf']['OEA']['lasalle_preMapOEA'] == true){
//	$h_lasalle_cust = '&nbsp;&nbsp;<a href="?task=preMapStudent">Edit PreMap</a>';
	$h_lasalle_cust = $linterface->Get_Content_Tool_v30('edit', '?task=preMapStudent', $Lang['iPortfolio']['OEA']['ViewPreMap']);
}
if ($liboea->isEnabledAcademicPerformance()) {
	$h_GenerateAcademicResultIcon = $linterface->Get_Content_Tool_v30('generate', 'javascript:js_Go_Generate_AcademicResult();', $Lang['iPortfolio']['OEA']['AcademicArr']['GenerateAcademicPerformance']);	
}


### Class Selection
$YearClassIDSelected = $YearClassID; 
$YearClassIDSelected = ($YearClassIDSelected=='0')? '' : $YearClassIDSelected;
if (substr($YearClassIDSelected, 0, 2) == '::') {
	// selected a Class
	$YearID = '';
	$YearClassID = str_replace('::', '', $YearClassIDSelected);
}
else {
	// selected a Form
	$YearID = $YearClassIDSelected;
	$YearClassID = '';
}
$tag = ' id="YearClassID" name="YearClassID" onchange="js_Reload_Page();" '; 
$ApplicableFormIDArr = $liboea->getJupasApplicableFormIDArr();
$h_ClassSelection = $libclass->getSelectClassWithWholeForm($tag, $YearClassIDSelected, $Lang['SysMgr']['FormClassMapping']['AllClass'], "", Get_Current_Academic_Year_ID(), $ApplicableFormIDArr, $returnTeachingClassOnly, $HideNoClassForm=1);

### Status Selection
$TargetApprovalStatus = ($TargetApprovalStatus=='')? '-1' : $TargetApprovalStatus;
$h_StatusSelection = $liboea->getStudentApprovalStatusSelection('TargetApprovalStatus', $TargetApprovalStatus, 'js_Reload_Page();');


### Content Table
$numOfOEAColumn = 0;
$rowspanTag = '';
$h_RemarksMsgTable = '';
$DepreciatedModuleDetected = array();
$showOEAItem = $liboea->isEnabledOEAItem();
if ($showOEAItem) {
	$numOfOEAColumn = $numOfOEAColumn + 2;
	$rowspanTag = " rowspan='2' ";
	$DepreciatedModuleDetected[] = $Lang['iPortfolio']['OEA']['ModuleTitleArr'][$oea_cfg["JupasItemInternalCode"]["OeaItem"]];
}
$showAddiInfo = $liboea->isEnabledAdditionalInfo();
if ($showAddiInfo) {
	$DepreciatedModuleDetected[] = $Lang['iPortfolio']['OEA']['ModuleTitleArr'][$oea_cfg["JupasItemInternalCode"]["AddiInfo"]];
	$numOfOEAColumn++;
}
$showAbility = $liboea->isEnabledAbility();
if ($showAbility) {
	$numOfOEAColumn++;
}
$showAcademic = $liboea->isEnabledAcademicPerformance();
if ($showAcademic) {
	$numOfOEAColumn++;
}
$showSuppInfo = $liboea->isEnabledSupplementaryInfo();
if ($showSuppInfo) {
	$numOfOEAColumn++;
}
if($DepreciatedModuleDetected){
	//$RemarksMsg = implode(' '.$Lang['iPortfolio']['JUPAS']['and'].' ',$DepreciatedModuleDetected).' '.$Lang['iPortfolio']['JUPAS']['WillNotBeUsed'];
	//$h_RemarksMsgTable = $linterface->Get_Warning_Message_Box('<span style="color:red">'.$Lang['General']['Remark'].'</span>', $RemarksMsg);
	$h_RemarksMsgTable = $linterface->Get_Warning_Message_Box('<span style="color:red">'.$Lang['General']['Remark'].'</span>', $Lang['iPortfolio']['JUPAS']['RemarksOEA']);	
}

$h_table = '';
if ($numOfOEAColumn == 0)
{
	$h_dbtable .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">'."\n";
		$h_dbtable .= '<tr>'."\n";
			$h_dbtable .= '<td align="center" class="stu_info_log">'.$Lang['iPortfolio']['OEA']['AllSettingsAreDisabled'].'</td>'."\n";
		$h_dbtable .= '</tr>'."\n";
	$h_dbtable .= '</table>'."\n";
}
else
{
	$ClassWidth = 10;
	$ClassNumberWidth = 5;
	$StudentNameWidth = 20;
	$ApplicationNumber = 10;
	$OEAColumnWidth = floor((100 - $ClassWidth - $ClassNumberWidth - $StudentNameWidth - $ApplicationNumber) / $numOfOEAColumn);
	
	$field = ($field=="")? 0 : $field;
	$order = ($order=="")? 1 : $order;
	$pageNo = ($pageNo=="")? 0 : $pageNo;
	$li = new libdbtable2007($field, $order, $pageNo);
	
	$DBTableInfoArr = $liboea->getTeacherManagementIndexSql($YearID, $YearClassID, $returnTeachingClassOnly, $TargetApprovalStatus);
	$li->sql = $DBTableInfoArr['DBTableSql'];
	$li->field_array = $DBTableInfoArr['DBTableFieldArr'];
	$li->no_col = 5 + $numOfOEAColumn;
	$li->IsColOff = 'IP25_table';
	$li->fieldorder2 = ', ClassNumber ';
	
	//debug_pr($li->built_sql());
	//debug_pr($li->returnArray($li->built_sql()));
	
	# TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<th $rowspanTag width='1'>#</th>\n";
	$li->column_list .= "<th $rowspanTag width='".$ClassWidth."%'>".$li->column_IP25($pos++, $Lang['SysMgr']['FormClassMapping']['Class'])."</th>\n";
	$li->column_list .= "<th $rowspanTag width='".$ClassNumberWidth."%'>".$li->column_IP25($pos++, $Lang['SysMgr']['FormClassMapping']['ClassNo'])."</th>\n";
	$li->column_list .= "<th $rowspanTag width='".$StudentNameWidth."%'>".$li->column_IP25($pos++, $Lang['SysMgr']['FormClassMapping']['StudentName'])."</th>\n";
	$li->column_list .= "<th $rowspanTag width='".$ApplicationNumber."%'>".$li->column_IP25($pos++, $Lang['iPortfolio']['OEA']['ApplicationNumber'])."</th>\n";
	if ($showOEAItem) {
		$li->column_list .= "<th colspan='2'>".$Lang['iPortfolio']['OEA']['OEA_ShortName']."</th>\n";
	}
	if ($showAddiInfo) {
		$li->column_list .= "<th $rowspanTag width='".$OEAColumnWidth."%'>".$Lang['iPortfolio']['OEA']['AdditionalInformation']."</th>\n";
	}
	if ($showAbility) {
		$li->column_list .= "<th $rowspanTag width='".$OEAColumnWidth."%'>".$Lang['iPortfolio']['OEA']['PersonalAndGeneralAbility']."</th>\n";
	}
	if ($showAcademic) {
		$li->column_list .= "<th $rowspanTag width='".$OEAColumnWidth."%'>".$Lang['iPortfolio']['OEA']['AcademicPerformance']."</th>\n";
	}
	if ($showSuppInfo) {
		$li->column_list .= "<th $rowspanTag width='".$OEAColumnWidth."%'>".$Lang['iPortfolio']['OEA']['SupplementaryInformation']."</th>\n";
	}
	
	if ($showOEAItem) {
		$li->column_list .= "</tr>\n";
		$li->column_list .= "<tr>\n";
			$li->column_list .= "<th width='".$OEAColumnWidth."%'>".$li->column_IP25($pos++, $Lang['General']['Approved'])."</th>\n";
			$li->column_list .= "<th width='".$OEAColumnWidth."%'>".$li->column_IP25($pos++, $Lang['General']['Pending'])."</th>\n";
	}
	
	
	
	$h_dbtable .= $li->display();
}

//$h_ApprovalStatusRemarks = $liboea->getApprovalStatusRemarksDiv();
$h_notActivatedPortfolioRemarks = $libpf_ui->Get_iPortfolio_Not_Activated_Remarks();
$h_archiveStudentRemarks = $linterface->ArchiveStudentField();


$ReturnMsg = $Lang['iPortfolio']['OEA']['AcademicArr']['ReturnMsgArr'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);
include_once($template_script);
$linterface->LAYOUT_STOP();
?>