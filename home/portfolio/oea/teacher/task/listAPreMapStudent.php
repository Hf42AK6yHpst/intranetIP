<?
$StudentID = trim($StudentID);

$libfcm = new form_class_manage();
$libfcm_ui = new form_class_manage_ui();
$liboea = new liboea();

### Get Student Info
$StudentInfoArr = $libfcm->Get_Student_Class_Info_In_AcademicYear($StudentID, Get_Current_Academic_Year_ID());
$h_ClassName = Get_Lang_Selection($StudentInfoArr[0]['ClassTitleB5'], $StudentInfoArr[0]['ClassTitleEN']);
$h_ClassNumber = $StudentInfoArr[0]['ClassNumber'];
$h_StudentName = Get_Lang_Selection($StudentInfoArr[0]['ChineseName'], $StudentInfoArr[0]['EnglishName']);

### Get Student Name Drop-down List
$YearClassID = $StudentInfoArr[0]['YearClassID'];
$h_StudentSelection = $libfcm_ui->Get_Student_Selection('StudentID', $YearClassID, $StudentID, 'js_Changed_Student_Selection(this.value);', $noFirst=1, $isMultiple=0, $isAll=0);



$h_OEAItemTable = '';

$h_OEAItemTable .= $liboea->getTeacher_listStuAllInfo_oeaPreMapStudentItemDiv($StudentID);
	
$h_OEAItemTable .= '<br style="clear:both;" />'."\n";
$h_OEAItemTable .= '<br style="clear:both;" />'."\n";
$h_OEAItemTable .= '<div class="edit_bottom_v30" style="height:10px;"></div>'."\n";



$linterface->LAYOUT_START($ReturnMsg);
include_once($template_script);
$linterface->LAYOUT_STOP();
?>