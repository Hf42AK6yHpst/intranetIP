<?
include_once($PATH_WRT_ROOT."includes/libuser.php");
$objUser = new libuser($UserID);

$currentTime = date("YmdGis");
$urlValue[] = "engName=".htmlentities(urlencode($objUser->EnglishName));
$urlValue[] = "chiName=".htmlentities(urlencode($objUser->ChineseName));
$urlValue[] = "email=".htmlentities(urlencode($objUser->UserEmail));
$urlValue[] = "currentTime=".htmlentities(urlencode($currentTime));
$urlValue[] = "serverName=".htmlentities(urlencode($_SERVER["SERVER_NAME"]));
$urlValue[] = "serverADDR=".htmlentities(urlencode($_SERVER["SERVER_ADDR"]));


$parameter = implode('&',$urlValue);

$parameter =($parameter);

$parameter = base64_encode($parameter);
$redirect='index.php?task=viewForum&par='.$parameter;

header("Location: ".$redirect);
?>