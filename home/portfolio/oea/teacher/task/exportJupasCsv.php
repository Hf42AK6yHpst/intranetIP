<?php
### Get Form Values
$YearID = trim(urldecode(stripslashes($_REQUEST['YearID_Export'])));
$YearClassID = trim(urldecode(stripslashes($_REQUEST['YearClassID_Export'])));
$ReturnTeachingClassOnly = trim(urldecode(stripslashes($_REQUEST['ReturnTeachingClassOnly_Export'])));


### Initialize libraries
$liboea = new liboea();
$libfcm = new form_class_manage();
$libfs = new libfilesystem();
$libexport = new libexporttext();
$SuccessArr = array();

/* Zip file name:
 * If selected all Classes => "jupas.zip"
 * If selected a Form => "jupas_formName.zip"
 * If selected a Class => "jupas_className.zip"
 */
$ZipFileNameSuffix = '';
if ($YearID != '') {
	$ObjYear = new Year($YearID);
	$ZipFileNameSuffix .= '_'.$ObjYear->Get_Year_Name();
}
else if ($YearClassID != '') {
	$ObjYearClass = new year_class($YearClassID);
	$ZipFileNameSuffix .= '_'.$ObjYearClass->Get_Class_Name('en');
}


### Prepare csv and zip files Folder
$TempFolder = $intranet_root."/file/temp/ipf_jupas_export";
$TempUserFolder = $TempFolder.'/'.$_SESSION['UserID'];
$TempCsvFolder = $TempUserFolder.'/jupas'.$ZipFileNameSuffix;

$ZipFileName = "jupas".$ZipFileNameSuffix.".zip";
$ZipFilePath = $TempUserFolder.'/'.$ZipFileName;

if (!file_exists($TempCsvFolder)) {
	$SuccessArr['CreateTempCsvFolder'] = $libfs->folder_new($TempCsvFolder);
}


### Get Student List
if ($YearClassID != '') {
	$YearClassIDArr = array($YearClassID);
}
else {
	if ($YearID == '') {
		// All Forms => Get All Applicable Forms of Jupas
		$YearID = $liboea->getJupasApplicableFormIDArr();
	}
	
	$YearClassInfoArr = $libfcm->Get_Class_List_By_Academic_Year(Get_Current_Academic_Year_ID(), $YearID, $ReturnTeachingClassOnly);
	$YearClassIDArr = Get_Array_By_Key($YearClassInfoArr, 'YearClassID');
}
//$StudentInfoArr = $libfcm->Get_Student_By_Class($YearClassIDArr);
$StudentInfoArr = $lpf->Get_Student_With_iPortfolio($YearClassIDArr, 0, $withJupasAppNoOnly=true);
$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
$StudentCsvInfoArr = $liboea->getStudentInfoForCsv($StudentIDArr);

if ($liboea->isEnabledOEAItem()) {
	$SuccessArr['GenerateCsv']['OeaItem'] = $libfs->file_write($liboea->getOEAItemJupasCsvExportContent($StudentCsvInfoArr), $TempCsvFolder.'/'.$liboea->getOEAItemJupasCsvName());
}

if ($liboea->isEnabledAdditionalInfo()) {
	$SuccessArr['GenerateCsv']['AddiInfo'] = $libfs->file_write($liboea->getAddiInfoJupasCsvExportContent($StudentCsvInfoArr), $TempCsvFolder.'/'.$liboea->getAddiInfoJupasCsvName());
}

if ($liboea->isEnabledAbility()) {
	$SuccessArr['GenerateCsv']['Ability'] = $libfs->file_write($liboea->getAbilityJupasCsvExportContent($StudentCsvInfoArr), $TempCsvFolder.'/'.$liboea->getAbilityJupasCsvName());
}

if ($liboea->isEnabledAcademicPerformance()) {
	$SuccessArr['GenerateCsv']['AcademicPercentile'] = $libfs->file_write($liboea->getAcademicJupasCsvExportContent($StudentCsvInfoArr), $TempCsvFolder.'/'.$liboea->getAcademicJupasCsvName('percentile'));
	$SuccessArr['GenerateCsv']['AcademicRating'] = $libfs->file_write($liboea->getAcademicJupasCsvExportContent($StudentCsvInfoArr,true), $TempCsvFolder.'/'.$liboea->getAcademicJupasCsvName('rating'));
}

if ($liboea->isEnabledSupplementaryInfo()) {
	$SuccessArr['GenerateCsv']['SuppInfo'] = $libfs->file_write($liboea->getSuppInfoJupasCsvExportContent($StudentCsvInfoArr), $TempCsvFolder.'/'.$liboea->getSuppInfoJupasCsvName());
}


### Delete old zip file and zip the current csv files
$SuccessArr['DeleteLastGeneratedZipFile'] = $libfs->file_remove($ZipFilePath);
$SuccessArr['ZipCsvFiles'] = $libfs->file_zip('jupas'.$ZipFileNameSuffix, $ZipFilePath, $TempUserFolder);
	
### remove the folder
$SuccessArr['DeleteTempCsvFiles'] = $libfs->folder_remove_recursive($TempCsvFolder);


### output the zip file to browser and let the user download it.
output2browser(get_file_content($ZipFilePath), $ZipFileName);
?>