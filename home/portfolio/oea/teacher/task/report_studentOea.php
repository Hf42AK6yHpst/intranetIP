<?php

if($_SESSION['UserType']==USERTYPE_STUDENT){
	header("Location: /home");
}
$CurrentPage = "Teacher_OEA_Report";

$liboea = new liboea();
$libclass = new libclass();

// override index.php settings
$TAGS_OBJ = array(array($Lang['iPortfolio']['OEA']['ReportArr']['StudentOea']));

$isSuperAdmin = $lpf->IS_IPF_SUPERADMIN();
$returnTeachingClassOnly = ($isSuperAdmin || strstr($ck_function_rights, "Jupas:All"))? 0 : 1;


### Class Selection
$YearClassIDSelected = $YearClassID; 
$YearClassIDSelected = ($YearClassIDSelected=='0')? '' : $YearClassIDSelected;
if (substr($YearClassIDSelected, 0, 2) == '::') {
	// selected a Class
	$YearID = '';
	$YearClassID = str_replace('::', '', $YearClassIDSelected);
}
else {
	// selected a Form
	$YearID = $YearClassIDSelected;
	$YearClassID = '';
}
$tag = ' id="YearClassID" name="YearClassID" onchange="reloadStudentSelection();" '; 
$applicableFormIDArr = $liboea->getJupasApplicableFormIDArr();
$h_classSelection = $libclass->getSelectClassWithWholeForm($tag, $YearClassIDSelected, $Lang['SysMgr']['FormClassMapping']['AllClass'], "", Get_Current_Academic_Year_ID(), $applicableFormIDArr, $returnTeachingClassOnly, $HideNoClassForm=1);


### Report Type Selection
$h_reportType = '';
$numOfCustReport = count((array)$sys_custom['iPf']['JUPASArr']['CustReportArr']);
$numOfEnabledCustReport = 0;
foreach((array)$sys_custom['iPf']['JUPASArr']['CustReportArr'] as $_custReportKey => $_enableFlag) {
	if ($_enableFlag) {
		$numOfEnabledCustReport++;
	}
}
if ($numOfEnabledCustReport > 0) {
	// have customized report => show related options
	$h_reportType .= '<tr>'."\r\n";
		$h_reportType .= '<td class="field_title">'.$ec_iPortfolio['report_type'].'</td>'."\r\n";
		$h_reportType .= '<td>'."\r\n";
			// General Report Options
			$reportKey = 'StudentOeaReport';
			$h_reportType .= $linterface->Get_Radio_Button('reportType_'.$reportKey, 'reportType', $reportKey, $isChecked=1, $Class="", $Lang['iPortfolio']['OEA']['ReportArr']['DisplayInOeaFormat'])."\r\n";
			
			
			// Customized Report Options
			foreach((array)$sys_custom['iPf']['JUPASArr']['CustReportArr'] as $_custReportKey => $_enableFlag) {
				if ($_enableFlag) {
					$_custReportName = $Lang['iPortfolio']['OEA']['ReportArr']['CustReportTitleArr'][$_custReportKey];
					$h_reportType .= '<br />'."\r\n";
					$h_reportType .= $linterface->Get_Radio_Button('reportType_'.$_custReportKey, 'reportType', $_custReportKey, $isChecked=0, $Class="", $_custReportName);
				}
			}
		$h_reportType .= '</td>'."\r\n";
	$h_reportType .= '</tr>'."\r\n";
}
else {
	// only general student oea report
	$h_reportType .= '<input type="hidden" id="reportType" name="reportType" value="StudentOeaReport">'."\r\n";
}


$h_buttonPrint = $linterface->GET_ACTION_BTN($Lang['Btn']['Print'], "button", $onclick="jsGoPrint();", $id="btnPrint");
$h_buttonExport = $linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "button", $onclick="jsGoExport();", $id="btnExport");

$ReturnMsg = $Lang['iPortfolio']['OEA']['AcademicArr']['ReturnMsgArr'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);
include_once($template_script);
$linterface->LAYOUT_STOP();
?>