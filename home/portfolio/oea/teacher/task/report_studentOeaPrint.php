<?php
include_once($intranet_root."/includes/portfolio25/oea/liboea_report.php");
include_once($intranet_root."/includes/portfolio25/slp/libpf-ole.php");
include_once($intranet_root."/includes/portfolio25/oleCategory/libpf-category.php");
//include_once($PATH_WRT_ROOT.'includes/libuser.php');
//include_once($PATH_WRT_ROOT.'includes/libexporttext.php');


//$liboea_item = new liboea_item();
//$lexport = new libexporttext();

$printMode = $_POST['printMode'];
$includeOleData = $_POST['includeOleData'];
$StudentIDArr = $_POST['StudentIDArr'];
$reportType = $_POST['reportType'];


### Get Student info
switch ($reportType) {
	case 'StudentOeaReport':
		$objOeaReport = new liboea_report();
		break;
	case 'LaSalle_StudentMappedOeaOleInfoReport':
		include_once($intranet_root.'/includes/portfolio25/oea/liboea_report_laSalle_oleReport.php');
		$objOeaReport = new liboea_report_laSalle_oleReport();
		break;
	default:
		$objOeaReport = new liboea_report();
}
$studentData = $objOeaReport->getStudentDataForReport($StudentIDArr, $includeOleData);


switch($printMode){
	case $oea_cfg['printMode']['html']:
		$h_table = $objOeaReport->printReportAsHtml($studentData, $includeOleData);
	break;
	case $oea_cfg['printMode']['csv']:
		$objOeaReport->printReportAsCSV($studentData, $includeOleData);
		exit();
	break;
}

$h_reportTitle = $objOeaReport->getReportTitle();

$h_printButton = "<table width='100%' align='center' class='print_hide'><tr>";
$h_printButton .= "<td align='right'>";
$h_printButton .= $linterface->GET_BTN($button_print, "button","javascript:window.print()");
$h_printButton .= "</td></tr></table>";


include_once($template_script);

exit();
?>