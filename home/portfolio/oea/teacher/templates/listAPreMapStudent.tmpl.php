<script>
function js_Changed_Student_Selection(jsStudentID) {
	js_Reload_Page(jsStudentID);
}

function js_Reload_Page(jsStudentID) {
	window.location = 'index.php?task=listAPreMapStudent&StudentID=' + jsStudentID;
}

function js_delete_item(jsParticipationType)
{

	var jsCheckboxClass = 'OEA_RecordIDChk_' + jsParticipationType;
	if ($('input.' + jsCheckboxClass + ':checked').length == 0) {
		alert('Please select at least one OEA item.');
	}
	else {
		var jsRecordIDList = Get_Checkbox_Value_By_Class(jsCheckboxClass);
		$('#r_recordID').val(jsRecordIDList);
		$('#task').val('deleteOEAPreMapItem');
		$('form#form1').submit();		
	}

}


</script>
<form id="form1" name="form1" method="POST" action="index.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr><td align="left" class="navigation"><?=$h_PageNavigation?></td></tr>
		<tr><td align="center"><?=$h_Disclaimer?></td></tr>
		<tr>
			<td align="center">
				
				<!-- Student Info Table -->
				<table class="form_table_v30">
					<tr>
						<td class="field_title"><?=$Lang['SysMgr']['FormClassMapping']['Class']?></td>
						<td><?=$h_ClassName?></td>
					</tr>
					<tr>
						<td class="field_title"><?=$Lang['SysMgr']['FormClassMapping']['StudentName']?></td>
						<td><?=$h_StudentSelection?></td>
					</tr>
				</table>
				<br style="clear:both;" />

				<?=$h_OEAItemTable?>

			</td>
		</tr>
	</table>
	
	<div class="edit_bottom_v30" style="border-top:0px;">
		<?=$h_BtnPreviousStudent?>
		<?=$h_BtnBack?>
		<?=$h_BtnNextStudent?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="script" name="script" value="" />
	<input type="hidden" id="action" name="action" value="" />
	<input type="hidden" id="r_recordID" name="r_recordID" value="" />
	<input type="hidden" id="r_sid" name="r_sid" value="<?=$StudentID?>" />
	<input type="hidden" id="r_returnTask" name="r_returnTask" value="listAPreMapStudent" />
	<input type="hidden" id="AdvanceLayerDisplay" name="AdvanceLayerDisplay" value="0" />
	
	
	
	<div id="DescDivID" class="selectbox_layer"  style="visibility:hidden; width:300px; height:200px;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td><?=$linterface->GET_NAVIGATION2_IP25($Lang['iPortfolio']['OEA']['Description']) ?></td></tr>
		<tr>
			<td>
				<div id="DescDivContent"></div>
			</td>
		</tr>
	</table>
	</div>
	
</form>