<?
echo $h_navigation;
echo $h_msg_row;
?>
<script language="javascript">
$(document).ready( function() {	
	
});

function js_Reload_Page() {
	var jsYearClassID = $('select#YearClassID').val();
	var jsTargetApprovalStatus = $('select#TargetApprovalStatus').val();
	
	window.location = '?YearClassID=' + jsYearClassID + '&TargetApprovalStatus=' + jsTargetApprovalStatus;
}

function js_Go_Export() {
	$('input#task').val('exportJupasCsv');
	$('form#form1').submit();
	
	// restore task for page reload
	$('input#task').val('index');
}

function js_Go_Generate_AcademicResult() {
	$('input#task').val('generateAcademicResult_confirm');
	$('form#form1').submit();
}

</script>
<form id="form1" name="form1" method="POST" action="index.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr><td align="center"><?=$h_Disclaimer?></td></tr>
		<tr>
			<td>
				<div class="content_top_tool">
					<div class="Conntent_tool">
						<?=$h_RemarksMsgTable?>
						<?=$h_ExportIcon?>
						<?=$h_GenerateAcademicResultIcon?>
						<?=$h_lasalle_cust?>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="table_board">
					<?=$h_ClassSelection?>
					<?=$h_StatusSelection?>
					<br />
					<br />
					<?=$h_dbtable?>
					<br />
					<?=$h_notActivatedPortfolioRemarks?>
					<?=$h_archiveStudentRemarks?>
				</div>
			</td>
		</tr>
	</table>
	
	<input type='hidden' name='task' id='task' value='' />
	<input type='hidden' name='page_size_change' id='page_size_change' value='' />
	<input type='hidden' name='pageNo' id='pageNo' value='<?=$pageNo?>' />
	<input type='hidden' name='order' id='order' value='<?=$order?>' />
	<input type='hidden' name='field' id='field' value='<?=$field?>' />
	<input type='hidden' name='numPerPage' id='numPerPage' value='<?=$numPerPage?>' />
	
	<input type='hidden' name='YearID_Export' id='YearID_Export' value='<?=$YearID?>' />
	<input type='hidden' name='YearClassID_Export' id='YearClassID_Export' value='<?=$YearClassID?>' />
	<input type='hidden' name='ReturnTeachingClassOnly_Export' id='ReturnTeachingClassOnly_Export' value='<?=$returnTeachingClassOnly?>' />
</form>
<br/>