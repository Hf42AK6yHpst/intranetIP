// Define a ToolbarSet based on the Basic2 ToolbarSet in fckconfig.js.
/*
FCKConfig.ToolbarSets["Basic2"] = [
['Undo','Redo',
'Bold','Italic','Underline','StrikeThrough','Subscript','Superscript',
'TextColor','BGColor',
'FontName','FontSize',
'JustifyLeft','JustifyCenter','JustifyRight'],
['OrderedList','UnorderedList','Outdent','Indent',
'Link','Unlink','Table','Rule','SpecialChar']

] ;
*/

// Henry Yuen (2010-07-14): support more functionalities on editing table cells
FCKConfig.PluginsPath = FCKConfig.BasePath + 'plugins/' ;
FCKConfig.Plugins.Add( 'dragresizetable' );
FCKConfig.Plugins.Add( 'tablecommands' ) ;

FCKConfig.ToolbarSets["StudentReportTemplate"] = [
['Undo','Redo',
'Underline','Subscript','Superscript',
'TextColor','BGColor',
'FontSize',
'JustifyLeft','JustifyCenter','JustifyRight'],
['OrderedList','UnorderedList','Rule',
'Table','-','TableInsertRowAfter','TableDeleteRows','TableInsertColumnAfter','TableDeleteColumns','TableInsertCellAfter','TableDeleteCells','TableMergeCells','TableHorizontalSplitCell','TableCellProp']

] ;

// Begin Henry Yuen (2010-04-21): support attaching image in header
FCKConfig.ToolbarSets["StudentReportTemplate_withInsertImageFlash"] = [
['Undo','Redo','InsertImageFlash',
'Underline','Subscript','Superscript',
'TextColor','BGColor',
'FontSize',
'JustifyLeft','JustifyCenter','JustifyRight'],
['OrderedList','UnorderedList','Rule',
'Table','-','TableInsertRowAfter','TableDeleteRows','TableInsertColumnAfter','TableDeleteColumns','TableInsertCellAfter','TableDeleteCells','TableMergeCells','TableHorizontalSplitCell','TableCellProp']

] ;
// End Henry Yuen (2010-04-21): support attaching image in header

// Begin Bill Wong (2019-12-13): support text justify full
FCKConfig.ToolbarSets["StudentReportTemplate_withInsertImageFlash_withTextJustifyFull"] = [
['Undo','Redo','InsertImageFlash',
'Underline','Subscript','Superscript',
'TextColor','BGColor',
'FontSize',
'JustifyLeft','JustifyCenter','JustifyRight','JustifyFull'],
['OrderedList','UnorderedList','Rule',
'Table','-','TableInsertRowAfter','TableDeleteRows','TableInsertColumnAfter','TableDeleteColumns','TableInsertCellAfter','TableDeleteCells','TableMergeCells','TableHorizontalSplitCell','TableCellProp']

] ;
// End Bill Wong (2019-12-13): support text justify full




