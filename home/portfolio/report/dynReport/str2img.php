<?php

// Get the string to show in the output image.
$image_string = "[{$_REQUEST['s']}]";

// Infer the size of the image, reserving some space for decoration.
$image_font = dirname(__FILE__) . '/fonts/wt014.ttf';
$image_font_size = 10;
$image_string_bbox = imagettfbbox($image_font_size, 0, $image_font, $image_string);
$image_width = $image_string_bbox[4] - $image_string_bbox[0] + 2 + 3;
$image_height = $image_string_bbox[1] - $image_string_bbox[5] + 2 + 3;

// Create a new image instance
$im = imagecreate($image_width, $image_height);

// Allocate colors.
$color_white = imagecolorallocate($im, 0xFF, 0xFF, 0xFF);
$color_grey = imagecolorallocate($im, 0xB0, 0xB0, 0xB0);
$color_black = imagecolorallocate($im, 0x00, 0x00, 0x00);

// Draw the white background
imagefilledrectangle($im, 0, 0, $image_width, $image_height, $color_white);
// Draw the grey button shadow.
imagefilledpolygon($im,
	$points = array(
		3, $image_height - 1,
		$image_width - 4, $image_height - 1,
		$image_width - 1, $image_height - 4,
		$image_width - 1, 3
	),
	count($points)/2, $color_grey
);
// Draw the black button.
imagefilledpolygon($im,
	$points = array(
		2, 0,
		$image_width - 4, 0,
		$image_width - 2, 2,
		$image_width - 2, $image_height - 4,
		$image_width - 4, $image_height - 2,
		2, $image_height - 2,
		0, $image_height - 4,
		0, 2
	),
	count($points)/2, $color_black
);

// Draw a text string on the image
imagettftext($im, $image_font_size, 0, 2 - $image_string_bbox[0], 2 - $image_string_bbox[5], $color_white, $image_font, $image_string);

// Number of seconds before expiration.
// Note that cache will not work if this page is called as a task through index.php.
// Mote that some Firefox extensions may disable cache.
$expires = 60 * 60;
header("Pragma: public");
header("Cache-Control: max-age=".$expires);
header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$expires) . ' GMT');

// Output the image to browser
header('Content-type: image/gif');

imagegif($im);
imagedestroy($im);

?>
