<?php // Modifying: Shing ?>
<link rel="stylesheet" href="<?=$intranet_httppath?>/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script language="JavaScript" src="<?=$intranet_httppath?>/templates/jquery/thickbox.js"></script>
<script>
var StudentReportTemplateEditPage = {
	/**
	 * template_data is an object which mirrors the data in StudentReportTemplate, and
	 * contains the following properties (some of the properties may be missing):
	 * {
	 * 		'template_title': ?,
	 * 		'is_to_show_header_on_all_pages': ?,
	 * 		'header_template': ?,
	 * 		'footer_template': ?,
	 * 		'sections': [
	 * 			{
	 * 				'section_type': table/free_text/page_break,
	 * 				'free_text': { // For free text section only.
	 * 					'text_template': ?
	 * 				},
	 * 				'table': { // For table section only.
	 * 					'section_title': ?,
	 * 					'sort_by_column_number': 1/2/3/...,
	 * 					'is_ascending': ?,
	 * 					'columns': [
	 * 						{
	 * 							'title': ?,
	 * 							'width': ?
	 * 						},
	 * 						...
	 * 					],
	 * 					'data_sources': [
	 * 						{
	 * 							'name': OLE/Attendance Record/Activity/Merit/...,
	 * 							'max_num_of_records': ?,
	 * 							'fields': [
	 * 								field_name1, field_name2, ...
	 * 							]
	 * 						},
	 * 						...
	 * 					],
	 * 					// Style information.
	 * 					'border_size': ?,
	 * 					'border_color': ?,
	 * 					'num_of_records_to_show_per_row': 1/2/3,
	 * 					'section_title_style': {
	 * 						'font_family': ?,
	 * 						'font_size': ?,
	 * 						'is_bold': ?,
	 * 						'is_italic': ?,
	 * 						'is_underline': ?,
	 * 						'text_color': ?,
	 * 						'background_color': ?
	 * 					},
	 * 					'column_title_style': {
	 * 						'font_family': ?,
	 * 						'font_size': ?,
	 * 						'is_bold': ?,
	 * 						'is_italic': ?,
	 * 						'is_underline': ?,
	 * 						'text_color': ?,
	 * 						'background_color': ?
	 * 					},
	 * 					'row_text_style': {
	 * 						'font_family': ?,
	 * 						'font_size': ?,
	 * 						'is_bold': ?,
	 * 						'is_italic': ?,
	 * 						'is_underline': ?,
	 * 						'text_color': ?,
	 * 						'background_color': ?
	 * 					}
	 * 				}
	 * 			},
	 * 			...
	 * 		]
	 * }
	 */
	_template_data: <?= count($template_data) > 0 ? $json->encode($template_data) : '{}' ?>,
	
	/**
	 * Used by StudentReportTemplateEditTablePage to get some form data for a table in the edit page, given its section index.
	 * @return an object containing some properties from _template_data, so see _template_data for details:
	 * 		{ // For table section only.
	 * 			'section_title': ?,
	 * 			'sort_by_column_number': 1/2/3/...,
	 * 			'is_ascending': ?,
	 *			...
	 * 		}
	 * Return null on error.
	 */
	get_table_data: function(section_index) {
		// Check parameters.
		var data = this._template_data;
		if (!data) {alert('Template data is not available.'); return null;}
		if (!data.sections || data.sections.length == 0) {alert('There is no section in the template.'); return null;}
		var section = data.sections[section_index];
		if (!section) {alert('The section with index "' + section_index + '" does not exist in the template.'); return null;}
		var section_type = section.section_type;
		if (section_type != 'table') {alert('The section with index "' + section_index + '" is a "' + section_type + '" section, not a table section.'); return null;}
		var table_data = section.table;
		if (!table_data) {alert('There is no table data in the section.'); return null;}
		
		// Copy and return the table data.
		return $.extend(true, {}, table_data);
	},
	
	/**
	 * Used by StudentReportTemplateEditTablePage to add some form data for a table in the edit page.
	 * @param table_data Similar in structure as that returned by get_table_data().
	 */
	add_table_data: function(table_data) {
		// Check parameters.
		if (!table_data) {alert('Table data is not given.'); return;}
		if (typeof table_data != 'object') {alert('Table data should be an object.'); return;}
		if (!table_data.hasOwnProperty('section_title')) {alert('Table data should provide a section title.'); return;}
		if (table_data.hasOwnProperty('columns') && !$.isArray(table_data.columns)) {alert('Columns should be an array.'); return;}
		if (table_data.hasOwnProperty('data_sources') && !$.isArray(table_data.data_sources)) {alert('Data sources should be an array.'); return;}
		
		if (!this._template_data) this._template_data = {};
		var data = this._template_data;
		if (!data.sections) data.sections = [];
		data.sections.push({
			section_type: 'table',
			// Clone the table data.
			'table': $.extend(true, {}, table_data)
		});
	},
	
	/**
	 * Used by StudentReportTemplateEditTablePage to update some form data for a table in the edit page, given its section index.
	 * @param table_data Similar in structure as that returned by get_table_data(). Style information is ignored.
	 */
	update_table_data: function(section_index, table_data) {
		// Check parameters.
		var data = this._template_data;
		if (!data) {alert('Template data is not available.'); return;}
		if (!data.sections || data.sections.length == 0) {alert('There is no section in the template.'); return;}
		var section = data.sections[section_index];
		if (!section) {alert('The section with index "' + section_index + '" does not exist in the template.'); return;}
		var section_type = section.section_type;
		if (section_type != 'table') {alert('The section with index "' + section_index + '" is a "' + section_type + '" section, not a table section.'); return;}
		if (!section.table) section.table = {};
		var existing_table_data = section.table;
		
		if (!table_data) {alert('Table data is not given.'); return;}
		if (typeof table_data != 'object') {alert('Table data should be an object.'); return;}
		if (!table_data.hasOwnProperty('section_title')) {alert('Table data should provide a section title.'); return;}
		if (table_data.hasOwnProperty('columns') && !$.isArray(table_data.columns)) {alert('Columns should be an array.'); return;}
		if (table_data.hasOwnProperty('data_sources') && !$.isArray(table_data.data_sources)) {alert('Data sources should be an array.'); return;}
		
		// Provide default for missing properties.
		table_data = $.extend(
			{
				section_title: null,
				sort_by_column_number: 1,
				is_ascending: false,
				columns: [],
				data_sources: []
			},
			table_data
		);
		
		// Merge columns separately to preserve column width, if no column is added/removed.
		if (existing_table_data.columns && table_data.columns && existing_table_data.columns.length == table_data.columns.length) {
			$.each(table_data.columns, function(i, val) {
				$.extend(existing_table_data.columns[i], val);
			});
			// No need to merge again.
			delete table_data.columns;
		}
		// Merge other properties, keeping style information.
		$.extend(existing_table_data, table_data);
	}
};
</script>
			  <!--###### Content Board Start ######-->
			  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
					<td height="33" background="images/2007a/content_01_bg.gif" class="contentitle_bar"><img src="images/2007a/10x10.gif" width="13" height="33"></td>
					<td valign="bottom" background="images/2007a/content_02_bg.gif" class="contentitle_bar">
						<!-- ###### <div id="contenttitle">Re</div>###### -->
		    <!-- ###### Tabs Start ######-->
								
								<div id="Content_tab">
											<ul>
											<li id="current"><a href="#"><span>Report Template</span></a></li>
											<li><a href="#"><span>?????</span></a></li>
											<li></li>
											<li></li>
										</ul>
									</div>
									
									<!-- ###### Tabs End ######-->					</td>
					<td height="33" background="images/2007a/content_03_bg.gif" class="contentitle_bar"><img src="images/2007a/10x10.gif" width="11" height="33"></td>
					</tr>
                    <tr> 
                      <td width="13" background="images/2007a/content_04.gif">&nbsp;</td>
                      <td class="main_content"><div class="navigation"><img src="images/2007a/nav_arrow.gif" width="15" height="15" align="absmiddle" /><a href="index.php">Template  List</a><img src="images/2007a/nav_arrow.gif" width="15" height="15" align="absmiddle" />New Template<br />
                      </div>
                     
                      <table class="form_table">

                        <col class="field_title" />
                        <col  class="field_c" />
                        <tr>
                          <td><span class="tabletextrequire">*</span>Template Title</td>
                          <td>:</td>
                          <td ><input name="textfield" type="text" id="textfield" class="textbox_name" value="<?= htmlspecialchars($template->get_template_title()) ?>"/></td>
                        </tr>
                        <tr>
                          <td colspan="3"><em class="form_sep_title"> -  Report Template -</em></td>
                        </tr>

                        <tr>
                          <td colspan="3">
                           <!--###########report Content ##############-->     
                          <div class="report_template_paper"> 
                          
                          <!-- Header -->
                          <div class="template_part" > 
                              <h1> <span class="row_content">- Header -</span> 
                                    <div class="table_row_tool row_content_tool"><a href="#" class="edit " title="Edit"></a></div> 
                                    <p class="spacer"></p>
                              </h1>
                              <p class="spacer"></p> 
                             <div class="template_part_content">
                         	 	 <span class="content_none">&lt; Report Title Here &gt;</span>
                             </div>
                          </div>
                          <br />
                          <!-- Content -->
                          <div class="template_part" > 
                              <h1> <span class="row_content">- Data Content -</span> 
                                  <div class="table_row_tool row_content_tool"></div>
                              <p class="spacer"></p>
                              </h1>
                              <p class="spacer"></p> 
                         	  <div class="template_part_content">
                         	 	 <span class="content_none">&lt;Data Content Here &gt;</span>
                         	  </div>
                               <div class="new_content">
								<div class="new_content_btn_tool" onclick="MM_showHideLayers('new_tool_b','','show')"><a href="#">Add</a></div>
								<div class="new_content_btn_tool_layer"  id="new_tool_b" onClick="MM_showHideLayers('new_tool_b','','hide')">
								<a href="index.php?task=edit_section_table&amp;KeepThis=true&amp;TB_iframe=true&amp;height=500&amp;width=800" class="thickbox">from iPortfolio</a>								<!--<a href="#">import csv</a>-->	
								<a href="#">Free Text</a>	   
                                <a href="#" class="page_break">Page Break</a>	
								</div>
  							  </div>

								 <p class="spacer"></p>
                        </div>
                        
                          <br />
                          <!-- Footer -->
                            <div class="template_part" > 
                              <h1> <span class="row_content">- Footer -</span>
                               <div class="table_row_tool row_content_tool"><a href="#" class="edit " title="Edit"></a></div>
                           
	                            <p class="spacer"></p>
                               </h1>
                          		<p class="spacer"></p> 
                          		<div class="template_part_content">
                         	 	  <span class="content_none">&lt;Footer Here&gt;</span>
                           		</div> 
                           		<p class="spacer"></p>
                           	 </div>
                          
                          </div>
                           <!--#########################-->                         
                           </td>
                        </tr>

                        <tr>
                          <td>Target Class</td>
                          <td>:</td>
                          <td ><input type="checkbox" name="checkbox7" id="checkbox7" />
                            All
                            <br />
  <input type="checkbox" name="checkbox" id="checkbox" />
                            F1
                              <input type="checkbox" name="checkbox2" id="checkbox2" />
F2
<input type="checkbox" name="checkbox3" id="checkbox3" />
F3
<input type="checkbox" name="checkbox4" id="checkbox4" />
F4
<input type="checkbox" name="checkbox5" id="checkbox5" />
F5
<input type="checkbox" name="checkbox6" id="checkbox6" />
F6</td>
                        </tr>
                      </table>
                      <div class="edit_bottom">
                        <span> Last updated : 2 days ago by Admin</span>
                        <p class="spacer"></p>
                        <input name="submit" type="button" class="formbutton"
		 onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="Save as Draft" />
                        <input name="submit2" type="button" class="formbutton"
		 onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="Save and  Submit" />
                        <input name="submit2" type="button" class="formsubbutton" onclick="MM_goToURL('parent','school_setting_subject_class.htm');return document.MM_returnValue" 
		onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value="Cancel" />
                        <p class="spacer"></p>
                      </div>
                                      </td>
                      <td width="11" background="images/2007a/content_06.gif">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td width="13" height="10"><img src="images/2007a/content_07.gif" width="13" height="10"></td>
                      <td background="images/2007a/content_08.gif" height="10"><img src="images/2007a/content_08.gif" width="13" height="10"></td>
                      <td width="11" height="10"><img src="images/2007a/content_09.gif" width="11" height="10"></td>
                    </tr>
                  </table>
		    <!--###### Content Board End ######-->
