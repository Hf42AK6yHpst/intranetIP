<?php
// Editing by 
/*********************************************** Change Log *************************************************
 * 2013-03-20 (Carlos): add tab item [Release to student]
 */
// Intended to be included by other templates to show the tab menu.

$TabMenuArr = array();
$TabMenuArr[] = array("index.php?task=report", srt_lang('Print Report'), $task == 'index' || $task == 'report');
$TabMenuArr[] = array("index.php?task=list", srt_lang('Report Template'), $task == 'list' || $task == 'edit');
$TabMenuArr[] = array("index.php?task=release_student&clearCoo=1", $Lang['iPortfolio']['DynamicReport']['ReleaseToStudent'], $task == 'release_student');

echo $lpf->GET_TAB_MENU($TabMenuArr);

// Is it good to show the system messages here?
echo $linterface->GET_SYS_MSG($xmsg);

?>
