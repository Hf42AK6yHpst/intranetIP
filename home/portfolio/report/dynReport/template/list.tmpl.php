<!-- Modify by  -->
<!-- modifications  
	Henry Yuen (2010-07-16): introduce sample template, indicated by inputdate = NULL  
-->

<!-- ###### Tabs Start ######-->
<?php include 'tab_menu.tmpl.php'; ?>
<!-- ###### Tabs End ######-->
			  <!--###### Content Board Start ######-->
			  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="main_content"><div class="content_top_tool">
                          <div class="Conntent_tool"> <a href="index.php?task=edit" class="new"> <?= srt_lang('New') ?></a></div>
                        <div class="Conntent_search" style="display:none">
                            <input name="" type="text"/>
                          </div>
                        <br style="clear:both" />
                        </div>
                          <div class="table_board">
                          	<br style="clear:both;" />
                          	
                          	<?php if (count($default_template_list) > 0) { ?>
	                          	<?=$h_navigation_default?><?=$h_showHideDefault?>
	                          	<br style="clear:both;" />
	                          	<div id="<?=$h_defaultTemplateDivID?>">
		                          	<table class="common_table_list">
		                              <thead>
		                                <tr>
		                                  <th class="num_check">#</th>
		                                  <th><?= srt_lang('Template Title') ?></th>
		                                  <th><?= srt_lang('Data') ?></th>
		                                </tr>
		                              </thead>
		                              <tbody>
		                                <?php foreach ($default_template_list as $i => $template): ?>
		                                <tr>
		                                  <td><?= $i + 1 ?></td>
		                                  <td style="width:25%;"><a href="index.php?task=edit&amp;template_id=<?= $template['template_id'] ?>"><?= htmlspecialchars($template['template_title']) ?></a></td>
		                                  <td><?= htmlspecialchars(translate_data_source_list_string($template['data_source_list'])) ?>&nbsp;</td>
		                                </tr>
		                                <?php endforeach; ?>
		                              </tbody>
		                            </table>
		                        </div>
	                            <br style="clear:both;" />
                            <?php } ?>
                            
                            <?=$h_navigation_custom?>
							<table class="common_table_list">
                              <thead>
                                <tr>
                                  <th class="num_check">#</th>
                                  <th><?= srt_lang('Template Title') ?></th>
                                  <th><?= srt_lang('Data') ?></th>
                                  <th><?= srt_lang('Last Updated') ?></th>
                                  <th>&nbsp;</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach ($template_list as $i => $template): ?>
                                <tr>
                                  <td><?= $i + 1 ?></td>
                                  <td style="width:25%;"><a href="index.php?task=edit&amp;template_id=<?= $template['template_id'] ?>"><?= htmlspecialchars($template['template_title']) ?></a></td>
                                  <td><?= htmlspecialchars(translate_data_source_list_string($template['data_source_list'])) ?>&nbsp;</td>
                                  <td><?= $template['modified'] ? date("Y-m-d", strtotime($template['modified'])) : '-' ?></td>                                                                   
                                  <!-- Henry Yuen (2010-07-16): sample template cannot be deleted -->
                                  <?
                                  	if($template['inputdate'] != NULL){
								  ?>                                  		                                  	                                  	
                                  	<td><div class="table_row_tool"><a href="index.php?task=remove&amp;template_id=<?= $template['template_id'] ?>" class="delete_dim" title="<?= srt_lang('Delete') ?>" onclick="return confirm('<?= srt_lang('Are you sure you want to delete this template?') ?>')"></a></div></td>
<!--                                    <td>
                                        <a href="index.php?task=remove&amp;template_id=<?= $template['template_id'] ?>" title="<?= srt_lang('Delete') ?>" onclick="return confirm('<?= srt_lang('Are you sure you want to delete this template?') ?>')"><img src="/images/<?php echo $LAYOUT_SKIN;?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"></a>
                                    </td>-->
                                  <?
                                  	}
                                  	else{
                                  ?>
                                  	<td><div class="table_row_tool">&nbsp;</div></td>
                                  <?
                                    }
                                  ?>
                                </tr>
                                <?php endforeach; ?>
                              </tbody>
                            </table>
                        </div>
                      </td>
                    </tr>
                  </table>
		    <!--###### Content Board End ######-->
