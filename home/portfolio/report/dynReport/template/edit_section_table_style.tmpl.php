<!-- Modify by Henry Yuen-->
<!-- Modifications:
	Ivan (2013-01-08): added English font Courier, Helvetica, Times New Roman
	Henry Yuen (2010-06-30): add section description
-->
<script>
var EditStylePage ={	
	_style_data : 	parent.StudentReportTemplateEditPage.get_table_style_data(<?=$section_index?>),	
	on_init: function(){		
		//alert(uneval(this._style_data));
		
		// Set a preview callback for the color picker.
		if (window.setColor) {
			setColor.preview_callback = function(color) {
				EditStylePage.update_preview();
				
				//curId - Global var from colorchooser.js

				if(curId.indexOf("font") == 0)
					EditStylePage.update_font_color(curId, color);					
				else if(curId.indexOf("bg") == 0)
					EditStylePage.update_bg_color(curId,color);
				else					
					EditStylePage.update_table_border_color(curId, color);
			}
		}
		
		// INSERT TEMPLATE HTML
		$('#td_sec_des').html(this.clone_style_template('section_description_style'));
		$('#td_sec').html(this.clone_style_template('section_title_style'));
		$('#td_col').html(this.clone_style_template('column_title_style'));
		$('#td_row').html(this.clone_style_template('row_text_style'));
		$('#td_stat').html(this.clone_style_template('row_statistics_style'));
		
	
		//--------------------------------------------------------------------------
		// Set default values for style data
		this.init_style_data();		
		
		//---- TABLE BORDER
		this.update_table_border_size_display("border_table_display");
		
		//---- TABLE FONT BACKGROUND
		this.update_table_border_color("border_color_border_table", this._style_data.border_color);
		
		

		//---- SECTION			
		// font-family	+ size		
		this.update_font_display("section_title_style");
		this.update_size_display("section_title_style");
						
		// style
		this.update_option("section_title_style", Boolean(this._style_data.section_title_style.is_bold), 'bold');		
		this.update_option("section_title_style", Boolean(this._style_data.section_title_style.is_underline), 'underline');						
		this.update_option("section_title_style", Boolean(this._style_data.section_title_style.is_italic), 'italic');
			
		// font-color + font-background						
		this.update_font_color("section_title_style", this._style_data.section_title_style.text_color);				
		this.update_bg_color("section_title_style", this._style_data.section_title_style.background_color);
		
		//---- SECTION DESCRIPTION	
		// font-family	+ size		
		this.update_font_display("section_description_style");
		this.update_size_display("section_description_style");
						
		// style
		this.update_option("section_description_style", Boolean(this._style_data.section_description_style.is_bold), 'bold');		
		this.update_option("section_description_style", Boolean(this._style_data.section_description_style.is_underline), 'underline');						
		this.update_option("section_description_style", Boolean(this._style_data.section_description_style.is_italic), 'italic');
			
		// font-color + font-background						
		this.update_font_color("section_description_style", this._style_data.section_description_style.text_color);				
		this.update_bg_color("section_description_style", this._style_data.section_description_style.background_color);
		
			
		//--------------------------------------------------------------------------
		//---- COLUMN		
		// font-family	+ size
		this.update_font_display("column_title_style");
		this.update_size_display("column_title_style");
			
		// style
		this.update_option("column_title_style", Boolean(this._style_data.column_title_style.is_bold), 'bold');			
		this.update_option("column_title_style", Boolean(this._style_data.column_title_style.is_underline), 'underline');							
		this.update_option("column_title_style", Boolean(this._style_data.column_title_style.is_italic),'italic');	
			
		// font-color + font-background
		this.update_font_color("column_title_style", this._style_data.column_title_style.text_color);
		this.update_bg_color("column_title_style", this._style_data.column_title_style.background_color);
					
		//--------------------------------------------------------------------------
		//---- ROW		
		// font-family	+ size
		this.update_font_display("row_text_style");
		this.update_size_display("row_text_style");	
						
		// style
		this.update_option("row_text_style", Boolean(this._style_data.row_text_style.is_bold), 'bold');			
		this.update_option("row_text_style", Boolean(this._style_data.row_text_style.is_underline), 'underline');
		this.update_option("row_text_style", Boolean(this._style_data.row_text_style.is_italic),'italic');
			
		// font-color + font-background	
		this.update_font_color("row_text_style", this._style_data.row_text_style.text_color);
		this.update_bg_color("row_text_style", this._style_data.row_text_style.background_color);
		
		//--------------------------------------------------------------------------
		//---- STATISTICS		
		// font-family	+ size
		this.update_font_display("row_statistics_style");
		this.update_size_display("row_statistics_style");	
						
		// style
		this.update_option("row_statistics_style", Boolean(this._style_data.row_statistics_style.is_bold), 'bold');			
		this.update_option("row_statistics_style", Boolean(this._style_data.row_statistics_style.is_underline), 'underline');
		this.update_option("row_statistics_style", Boolean(this._style_data.row_statistics_style.is_italic),'italic');
			
		// font-color + font-background	
		this.update_font_color("row_statistics_style", this._style_data.row_statistics_style.text_color);
		this.update_bg_color("row_statistics_style", this._style_data.row_statistics_style.background_color);
				
		//--------------------------------------------------------------------------
		// Generate Column(s) width section
		this.generate_column();		
		
		//--------------------------------------------------------------------------
		// Update preview sample display
		this.update_preview(); 		
	},
	/**
	 * Init style data incase some properties are uninitialized
	 */
	init_style_data: function(){
		this._style_data = $.extend(
				true,
				// Provide some default styles.
				{
					border_size: 1,
					border_color:'#BBBBBB',
					section_title_style: {						
						font_family: 'arialunicid0',
						font_size: 12,
//						is_bold: true,
						is_bold: false,
						is_italic: false,
						is_underline: false,
						text_color: '#FFFFFF',
						background_color: '#BBBBBB'
					},
					section_description_style: {						
						font_family: 'arialunicid0',
						font_size: 12,
//						is_bold: true,
						is_bold: false,
						is_italic: false,
						is_underline: false,
						text_color: '#FFFFFF',
						background_color: '#BBBBBB'
					},
					column_title_style: {
						font_family: 'arialunicid0',
						font_size: 10,
//						is_bold: true,
						is_bold: false,
						is_italic: false,
						is_underline: false,
						text_color: '#FFFFFF',
						background_color: '#888888'
					},
					row_text_style: {
						font_family: 'arialunicid0',
						font_size: 8,
						is_bold: false,
						is_italic: false,
						is_underline: false,
						text_color: '#000000',
						background_color: '#FFFFFF'
					},
					row_statistics_style: {
						font_family: 'arialunicid0',
						font_size: 8,
						is_bold: false,
						is_italic: false,
						is_underline: false,
						text_color: '#000000',
						background_color: '#FFFFFF'
					}
				},
				this._style_data
			);
		
	},
	
	/** 
	 * Hide ALL popup select box
	 */
	close_selectbox: function(ignoreResetColor){
		var isOpenClass = "isOpen";
		var ignoreResetColor = ignoreResetColor || '';

		$(".selectbox_layer").hide();
		
		if(ignoreResetColor=="")
			$(".choose_color").removeClass(isOpenClass);
		
	},
	/**
	 * Toggle the display of color selection div
	 */
	toggle_pickColor: function(id){
		var isOpenClass = "isOpen";
		this.close_selectbox(1);
				
		if($("#"+id+"Div").hasClass(isOpenClass)){
			//Hide			
			$(".choose_color").removeClass(isOpenClass);
		}else{		
			//Show , reset color class for other color pickers incase they were open also
			this.close_selectbox();
			
			//Add Color class - indicating colorpicker is opened for object N
			$("#"+id+"Div").addClass(isOpenClass);
		
			pickColor(id);
		}
		
	},
	
	/**
	 * Toggle hide/show of popup select box
	 */
	toggle_selectbox: function(objContainer){
		var action = true;
		
		if($("#"+objContainer).css("display") != "none"){
			action = false;
		}
		
		this.close_selectbox();
		
		if(action)
			$("#"+objContainer).show();
		else
			$("#"+objContainer).hide();
	},	
	/**
	 * Generate setting style template 
	 */
	clone_style_template: function(layer_id){
		var style_tpl = $('#template_style').html();
		
		return style_tpl.replace(/---(\w+)---/g, function(str, p1) {
				switch (p1) {
					case 'layer_id':
						return layer_id;
						
					default:
						// Ignore unknown placeholders.
						return str;
				}
			});
		
	},	
	/**
	 * Get option classname - bold , underline, italic
	 */
	get_option_class: function(option_type, status){
		var classname = "";
		switch(option_type){
			case "bold": classname = (status)? "choose_bold": "choose_bold_on"; break;
			case "underline": classname = (status)? "choose_underline": "choose_underline_on"; break;
			case "italic": classname = (status)? "choose_italic": "choose_italic_on"; break;
			default: classname = (status)? "choose_bold": "choose_bold_on"; break;
		}
		return classname;
	},	
	/**
	 * Generate dynamic number of columns for "Preview Sample" display
	 */
	generate_column:function(){
		var col = "";
		if(this._style_data.columns){
			var max = this._style_data.columns.length;

			for(var i = 0; i<max ; i++){
				index = i+1;

				var default_width = Math.round(100/max);
				// Adjust default width for the last column.
				if (i == max - 1) default_width += 100 - Math.round(100/max)*max;
				var width = parseInt(this._style_data.columns[i].width);
				if (isNaN(width)) width = default_width;
				// Escape the title.
				col +=  (this._style_data.columns[i].title)? $('<div/>').text(this._style_data.columns[i].title).html() : "Column ("+index+")";
				// Escape the column width too, in case special characters are entered.
	          	col += '<label>'+
	          				'<input name="column_w[]" type="text" class="column_w" id="column_w_'+i+'" maxlength="3" size="3" value="'
	          					+ $('<div/>').text(width).html().replace(/"/g, '&quot;')
	          					+ '" />%'+
	          			'</label>&nbsp;';
			}
			
			$("#col_w_container").html(col);			
		}		
	},	
	
	/**
	 * Update Font style data + selection classname
	 */
	update_font: function(obj){
		
		var value = $(obj).attr('value');
		var layer_id = $(obj).attr('layer_id');
		
		this._style_data[layer_id].font_family = value;		
		this.update_font_display(layer_id);
				
	},
	/**
	 * Update selected font display 
	 */
	update_font_display: function(layer_id){
		
		var value = this._style_data[layer_id].font_family;

		$("a[layer_id = "+layer_id+"][type=font]").removeClass('selected_attendance');
		$("a[layer_id = "+layer_id+"][value= "+value+"]").addClass('selected_attendance');
		
		var display_value = $("a[layer_id="+layer_id+"][value="+value+"]").text();				
		$("#font_family_"+layer_id+"_display").text(display_value);
		
		// Henry Yuen (2010-07-15): default font-family
		if($("#font_family_"+layer_id+"_display").text() == ""){			
			$("#font_family_"+layer_id+"_display").text("ArialUnicodeMS ");
		}		
		
		this.update_preview();
	},	
	/**
	 * Update Size style data + selection classname
	 */
	update_size: function(obj){
		
		var value = $(obj).attr('value');
		var layer_id = $(obj).attr('layer_id');
		
		this._style_data[layer_id].font_size = value;
		this.update_size_display(layer_id);
	},
	/**
	 * Update the selected size display
	 */
	update_size_display: function(layer_id){
		var value = this._style_data[layer_id].font_size; 
		$("a[layer_id = "+layer_id+"][type=size]").removeClass('selected_attendance');
		$("a[layer_id="+layer_id+"][value="+value+"]").addClass('selected_attendance');
		$("#font_size_"+layer_id+"_display").text("<?= srt_lang('Size') ?> "+value);
		this.update_preview();
	},	
	
	/**
	 * Update option style UI & data obj - Bold, Underline, Italic
	 */
	update_option: function(obj, value, type){
		
		var fromInit = false;
		var layer_id = (typeof obj == "string")? obj : $(obj).attr("layer_id");
		
		switch(type){
			case "bold" :		value = value? value : "choose_bold"; break;
			case "underline": 	value = value? value : "choose_underline"; break;
			case "italic": 		value = value? value : "choose_italic"; break;
		}
		
		//FROM INIT -  update style					
		if(typeof value == "boolean"){						
			fromInit = true;						
		}		
			
		if($(obj).hasClass(value) || (fromInit && value==true)){									
			value = (fromInit)? this.get_option_class(type, value) : value;			
			$("#"+type+"_"+layer_id).removeClass(value);
			$("#"+type+"_"+layer_id).addClass(value+'_on');			
			value = 1;
		}else{
			$("#"+type+"_"+layer_id).removeClass(value+"_on");
			$("#"+type+"_"+layer_id).addClass(value);			
			value = 0;
		}
		
		switch(type){
			case "bold" :		this._style_data[layer_id].is_bold = value; break;
			case "underline": 	this._style_data[layer_id].is_underline = value; break;
			case "italic": value : this._style_data[layer_id].is_italic = value; break;
		}		
		this.update_preview();
	},	
	/**
	 * Set font color UI after clicking on color picker box
	 */
	update_font_color: function(layer_id, color){		
		$("#font_color_"+layer_id+"Div").css("background-color", color);
		layer_id = layer_id.replace("font_color_", "");		
		this._style_data[layer_id].text_color = color;
		this.update_preview();
	},
	
	/**
	 * Set background color UI after clicking on color picker box
	 */
	update_bg_color: function(layer_id, color){
		
		$("#bg_color_"+layer_id+"Div").css("background-color", color);
		layer_id = layer_id.replace("bg_color_", "");
		this._style_data[layer_id].background_color = color;
		this.update_preview();
	},
	
	/**
	 * Update table border size 
	 */
	update_table_border_size: function(obj){
		
		var layer_id = "border_table_display";		
		var value = $(obj).attr("value");
		
		this._style_data.border_size = value;		
		this.update_table_border_size_display(layer_id);		
	},		
	/**
	 * Update the selected table border size UI
	 */
	update_table_border_size_display: function(layer_id){
		
		var value = this._style_data.border_size;
		
		$("a[layer_id = "+layer_id+"][type=bordersize]").removeClass('selected_attendance');
		$("a[layer_id="+layer_id+"][value="+value+"]").addClass('selected_attendance');
		
		if(value > 0)
			display_value = "Size "+value+"px";
		else 
			display_value = "None";
				
		$("#border_table_display").text(display_value);
		this.update_preview();
	},
	
	update_table_border_color: function(layer_id , color){
		$("#"+layer_id+"Div").css("background-color", color);		
		this._style_data.border_color = color;
		this.update_preview();
	},
	
	/**
	 * Update the sample table CSS
	 */
	update_preview: function(){
		$("#pre_tb").css(this._convert_template_style_to_css_style(this._style_data));
		$("#pre_sec").css(this._convert_template_style_to_css_style(this._style_data.section_title_style));
		$("#pre_sec_des").css(this._convert_template_style_to_css_style(this._style_data.section_description_style));
		$("#pre_tb").css(this._convert_template_style_to_css_style(this._style_data.border));		
		$(".pre_col").css(this._convert_template_style_to_css_style(this._style_data.column_title_style));
		$(".pre_row").css(this._convert_template_style_to_css_style(this._style_data.row_text_style));
		$(".stat_row").css(this._convert_template_style_to_css_style(this._style_data.row_statistics_style));
	},
	
	/**
	 * Convert template style to CSS style, for display purpose.
	 * @param style_data An object containing the following properties:
	 * 		{
	 * 			'font_family': ?,
	 * 			'font_size': ?,
	 * 			'is_bold': ?,
	 * 			'is_italic': ?,
	 * 			'is_underline': ?,
	 * 			'text_color': ?,
	 * 			'background_color': ?
	 * 		}
	 * @return A key/value object containing CSS style properties for use in jQuery.css().
	 */
	_convert_template_style_to_css_style: function(style_data) {
		var css = {};
		var v;
		if (!style_data) return css;
		if (v = style_data['font_family']) css["font-family"] = v;
		if (v = style_data['font_size']) css["font-size"] = v;
		if (v = style_data['is_bold']) { css["font-weight"] = "bold";}else{css["font-weight"] = "normal";}
		if (v = style_data['is_italic']){ css["font-style"] = "italic";}else{css["font-style"] = "";}
		if (v = style_data['is_underline']){ css["text-decoration"] = "underline"; }else{css["text-decoration"] = "";}
		if (v = style_data['text_color']) css["color"] = v;
		if (v = style_data['background_color']) css["background-color"] = v;
		if (v = style_data['border_size']) css["border"] = v+"px solid "+  style_data['border_color'];
		
		return css;
	},
	
	onSubmit: function(){
		try {
			// Check whether the column widths are given.
			// Trim the values too.
			// Check whether the column widths are positive integers.
			// Check whether the total column width is 100%.
			var total_column_width = 0;
			$('.column_w').each(function() {
				var column_width = $.trim($(this).val());
				$(this).val(column_width);
				if (column_width == "") {
					$(this).focus();
					throw("<?= srt_lang('Please fill in column width.') ?>");
				}
				if (parseInt(column_width) != column_width - 0 || column_width <= 0) {
					$(this).focus();
					throw('<?= srt_lang('Please enter a positive integer.') ?>');
				}
				total_column_width += parseInt(column_width);
			});
			if (total_column_width != 100) {
				throw('<?= srt_lang('The total column width is {width}%, but it must be 100%.') ?>'.replace(/{width}/, total_column_width));
			}
		} catch (e) {
			alert(e);
			return;
		}
		
		// Get the column widths, append the '%' symbol.
		this._style_data.columns = $.map($('.column_w'), function(n, i) {
			return {
				width: $(n).val() + '%'
			};
		});
		
		parent.StudentReportTemplateEditPage.update_table_style_data('<?=$section_index?>', this._style_data);		
		parent.tb_remove();	
	}
};

$(document).ready(function() {		
	EditStylePage.on_init();
	
	setDiv();		// Func in colorchooser.js
});
</script>
<script language="JavaScript" src="../../../../templates/colorchooser.js"></script>


<div class="edit_pop_board">
	<h1><span>  <?= srt_lang('Section Style') ?></span></h1>
	<div class="edit_pop_board_write" style="height:180px">
		<em class="tabletextremark">- <?= srt_lang('Preview Sample') ?> -</em>		
		<div  class="section_sample"> 
			<span style="display:block" id='pre_sec'><?= srt_lang('Section Title') ?></span>
			<!-- Henry Yuen (2010-06-30): add section description-->
			<span style="display:block" id='pre_sec_des'><?= srt_lang('Section Description') ?></span>			
       		<table id="pre_tb"style="border:1px solid #CCCCCC;	width: 100%; border-collapse:separate; border-spacing: 0px;*border-collapse: expression('separate', cellSpacing = '0px');clear:both;" >
            <tr>
              <th class="pre_col" style="padding:3px;padding-top:5px;padding-bottom:5px;background-color: #A6A6A6;font-weight: normal;color: #FFFFFF;text-align:left;	border-bottom: 1px solid #CCCCCC;border-right: 1px solid #CCCCCC;"><?= srt_lang('Column Title') ?> (1)</th>
              <th class="pre_col" style="padding:3px;padding-top:5px;padding-bottom:5px;background-color: #A6A6A6;font-weight: normal;color: #FFFFFF;text-align:left;	border-bottom: 1px solid #CCCCCC;border-right: 1px solid #CCCCCC;"><?= srt_lang('Column Title') ?> (2)</th>
              <th class="pre_col" style="padding:3px;padding-top:5px;padding-bottom:5px;background-color: #A6A6A6;font-weight: normal;color: #FFFFFF;text-align:left;	border-bottom: 1px solid #CCCCCC;border-right: 1px solid #CCCCCC;"><?= srt_lang('Column Title') ?> (3)</th>
            </tr>
            <tr>
              <td class="pre_row" style="padding:3px;background-color: #FFFFFF;	border-bottom: 1px solid #CCCCCC;border-right: 1px solid #f1f1f1;vertical-align:top;padding-top:5px;padding-bottom:5px;"><?= srt_lang('Data') ?></td>
              <td class="pre_row" style="padding:3px;background-color: #FFFFFF;	border-bottom: 1px solid #CCCCCC;border-right: 1px solid #f1f1f1;vertical-align:top;padding-top:5px;padding-bottom:5px;"><?= srt_lang('Data') ?></td>
              <td class="pre_row" style="padding:3px;background-color: #FFFFFF;	border-bottom: 1px solid #CCCCCC;border-right: 1px solid #f1f1f1;vertical-align:top;padding-top:5px;padding-bottom:5px;"><?= srt_lang('Data') ?></td>
            </tr>
            <tr>
              <td class="pre_row" style="padding:3px;background-color: #FFFFFF;	border-bottom: 1px solid #CCCCCC;border-right: 1px solid #f1f1f1;vertical-align:top;padding-top:5px;padding-bottom:5px;"><?= srt_lang('Data') ?></td>
              <td class="pre_row" style="padding:3px;background-color: #FFFFFF;	border-bottom: 1px solid #CCCCCC;border-right: 1px solid #f1f1f1;vertical-align:top;padding-top:5px;padding-bottom:5px;"><?= srt_lang('Data') ?></td>
              <td class="pre_row" style="padding:3px;background-color: #FFFFFF;	border-bottom: 1px solid #CCCCCC;border-right: 1px solid #f1f1f1;vertical-align:top;padding-top:5px;padding-bottom:5px;"><?= srt_lang('Data') ?></td>
            </tr>
            <tr>
              <td class="pre_row" style="padding:3px;background-color: #FFFFFF;	border-bottom: 1px solid #CCCCCC;border-right: 1px solid #f1f1f1;vertical-align:top;padding-top:5px;padding-bottom:5px;">...</td>
              <td class="pre_row" style="padding:3px;background-color: #FFFFFF;	border-bottom: 1px solid #CCCCCC;border-right: 1px solid #f1f1f1;vertical-align:top;padding-top:5px;padding-bottom:5px;">...</td>
              <td class="pre_row" style="padding:3px;background-color: #FFFFFF;	border-bottom: 1px solid #CCCCCC;border-right: 1px solid #f1f1f1;vertical-align:top;padding-top:5px;padding-bottom:5px;">...</td>
            </tr>
            <tr>
              <td class="stat_row" style="padding:3px;background-color: #FFFFFF;	border-bottom: 1px solid #CCCCCC;border-right: 1px solid #f1f1f1;vertical-align:top;padding-top:5px;padding-bottom:5px;"><?= $Lang['iPortfolio']['DynamicReport']['StatisticsArr']['StatisticsData'] ?></td>
              <td class="stat_row" style="padding:3px;background-color: #FFFFFF;	border-bottom: 1px solid #CCCCCC;border-right: 1px solid #f1f1f1;vertical-align:top;padding-top:5px;padding-bottom:5px;"><?= $Lang['iPortfolio']['DynamicReport']['StatisticsArr']['StatisticsData'] ?></td>
              <td class="stat_row" style="padding:3px;background-color: #FFFFFF;	border-bottom: 1px solid #CCCCCC;border-right: 1px solid #f1f1f1;vertical-align:top;padding-top:5px;padding-bottom:5px;"><?= $Lang['iPortfolio']['DynamicReport']['StatisticsArr']['StatisticsData'] ?></td>
            </tr>
			</table>
  		</div>
 	</div>
 	<!--  END OF DISPLAY STYLE -->
 
 
  <div class="edit_pop_board_write" style="height:225px"><em class="tabletextremark">- <?= srt_lang('Style Setting') ?> -</em>
    <table class="form_table">
    
     <!-- //----------------     SECTION    --------------------- -->
      <tr>
        <td><?= srt_lang('Section Title') ?></td>
        <td>:</td>
        <td id="td_sec"><!-- Generate style template --></td>
      </tr>
      
      <!-- //----------------   Henry Yuen (2010-06-30) SECTION Descrtion  --------------------- -->
      <tr>
        <td><?= srt_lang('Section Description') ?></td>
        <td>:</td>
        <td id="td_sec_des"><!-- Generate style template --></td>
      </tr>
      
      <!-- //----------------     TABLE BORDER  --------------------- -->
      <tr>
        <td><?= srt_lang('Table Border') ?></td>
        <td>:</td>
        <td id="td_border"><div class="selectbox_group_more">
            <div class="selectbox_group selectbox_group_show">
            	<a href="Javascript:void(0);" id="border_table_display" onclick="EditStylePage.toggle_selectbox('border_table')">Size 1px</a>
            	<input type="hidden" id="hidden_border_table_display" name="hidden_border_table_display" value="1" />
            </div>
          <br style="clear:both" />
            <div id="border_table" class="selectbox_layer" onclick="EditStylePage.close_selectbox('border_table')"  style="display:none;visibility:visible;">
            <nobr class="select_attendance_status">            
            <span  class="selectbox_list_type"> <?= srt_lang('Border Size') ?></span>
            <a href="javascript:void(0);" onClick="EditStylePage.update_table_border_size(this)" id="0_border_table" class="border_table_display" value="0" type="bordersize" layer_id = "border_table_display"><?= srt_lang('None') ?></a>
            <a href="javascript:void(0);" onClick="EditStylePage.update_table_border_size(this)" id="1_border_table" class="border_table_display" value="1" type="bordersize" layer_id = "border_table_display">1 px</a>
            <!-- Only support toggling the display of the border now, not support setting the border size yet.
            <a href="javascript:void(0);" onClick="EditStylePage.update_table_border_size(this)" id="2_border_table" class="border_table_display" value="2" type="bordersize" layer_id = "border_table_display">2 px</a>
            <a href="javascript:void(0);" onClick="EditStylePage.update_table_border_size(this)" id="3_border_table" class="border_table_display" value="3" type="bordersize" layer_id = "border_table_display">3 px</a>
            -->
            </nobr>
            </div>
        </div>
            <div class="selectbox_group_more" style="display:none">
              <div id="bg_color_border_table_container" style="float:left;">
              <div class="choose_color colorselector" style="background:#000000;" title="<?= srt_lang('Choose Border Color') ?>" onClick="javascript:EditStylePage.toggle_pickColor('border_color_border_table');" id="border_color_border_tableDiv"><a href="javascript:void(0);">&nbsp;</a></div>              
              </div>				
            </div>
          <p class="spacer"></p></td>
      </tr>
      
      <!-- ----------------     COLUMN    --------------------- -->
      <tr>
        <td><?= srt_lang('Column Title') ?></td>
        <td>:</td>
        <td id="td_col"><!-- Generate style template --></td>
      </tr>
      
      <!-- //----------------     ROW   --------------------- -->
      <tr>
        <td><?= srt_lang('Data') ?></td>
        <td>:</td>
        <td id="td_row"><!-- Generate style template --></td>
      </tr>
      
      <!-- //----------------     STATISTICS   --------------------- -->
      <tr>
        <td><?= $Lang['iPortfolio']['DynamicReport']['StatisticsArr']['StatisticsData'] ?></td>
        <td>:</td>
        <td id="td_stat"><!-- Generate style template --></td>
      </tr>
      
      <!-- -----------------   COLUMN WIDTH ------------------ -->
      <tr>
        <td><?= srt_lang('Column Width') ?></td>
        <td>:</td>
        <td id="col_w_container"><!-- --------- Generate column width --------- --></td>
      </tr>
      
      
      <!--<tr>
        <td>Split Table</td>
        <td>:</td>
        <td ><input name="radio" type="radio" id="radio3" value="radio" checked="checked" />
            
Remain 1

<input type="radio" name="radio" id="radio" value="radio"  onclick="MM_goToURL('self','iportfolio_teacher_setting_report_template_section_style2.htm');return document.MM_returnValue"/>
<img src="images/2007a/split_2.gif" width="20" height="20" align="absmiddle" /> into 2  parts
<input type="radio" name="radio" id="radio4" value="radio" />
<img src="images/2007a/split_3.gif" width="20" height="20" align="absmiddle" /> into 3  parts</td>
      </tr>
      
      --> 
    </table>
  </div>
  
<!--   ---------     SUBMIT / CANCEL button panel     --------   -->
<div class="edit_bottom">
		<p class="spacer"></p>
		<input name="submit2" type="button" class="formbutton" onclick="EditStylePage.onSubmit();"
		 onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?= srt_lang('Submit') ?>" />
		<input name="cancel" type="button" class="formbutton" onclick="parent.tb_remove();" 
		onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?= srt_lang('Cancel') ?>" />
		<p class="spacer"></p>
  </div>
</div>

<!--  --------    Style template for DUPLICATING     --------- -->
<div id="template_style" style="display:none;">
	<span class="form_field_sub"><?= srt_lang('Text') ?></span>
        <div class="selectbox_group_more">
          <div class="selectbox_group selectbox_group_show" > <a href="javascript:void(0);" onclick="EditStylePage.toggle_selectbox('font_family_---layer_id---')" id='font_family_---layer_id---_display'>Verdana</a> </div>
        <br style="clear:both" />
          <div id="font_family_---layer_id---" class="selectbox_layer" onclick="EditStylePage.close_selectbox('font_family_---layer_id---')" style="display:none;visibility:visible;"> 
          <nobr class="select_attendance_status"> 
          <span class="selectbox_list_type"> <?= srt_lang('Font') ?></span>           
          <a href="javascript:void(0)" onclick="EditStylePage.update_font(this)"  style="font-family:ArialUnicodeMS" type='font' value='arialunicid0' layer_id='---layer_id---'>ArialUnicodeMS</a>
          <a href="javascript:void(0)" onclick="EditStylePage.update_font(this)"  style="font-family:msungstdlight" type='font' value='msungstdlight' layer_id='---layer_id---'>MSung Light (English only)</a>          
          <a href="javascript:void(0)" onclick="EditStylePage.update_font(this)"  style="font-family:stsongstdlight" type='font' value='stsongstdlight' layer_id='---layer_id---'>STSung Light (English only)</a>
          <a href="javascript:void(0)" onclick="EditStylePage.update_font(this)"  style="font-family:droidserif-regular" type='font' value='droidserif-regular' layer_id='---layer_id---'>Droid Serif Regular (English only)</a>
          <a href="javascript:void(0)" onclick="EditStylePage.update_font(this);" style="font-family:Courier" type='font' value='courier' layer_id='---layer_id---'>Courier (English only)</a> 
          <a href="javascript:void(0)" onclick="EditStylePage.update_font(this);" style="font-family:Helvetica" type='font' value='helvetica' layer_id='---layer_id---'>Helvetica (English only)</a> 
          <a href="javascript:void(0)" onclick="EditStylePage.update_font(this);" style="font-family:Times New Roman" type='font' value='times' layer_id='---layer_id---'>Times New Roman (English only)</a> 
          
          <!-- Remove English-only fonts.
          <a href="javascript:void(0)" onclick="EditStylePage.update_font(this)"  style="font-family:ArialUnicodeMS " type='font' value='arialunicid0' layer_id='---layer_id---'>ArialUnicodeMS</a>
          <a href="javascript:void(0)" onclick="EditStylePage.update_font(this);" style="font-family:Courier" type='font' value='courier' layer_id='---layer_id---'>Courier</a> 
          <a href="javascript:void(0)" onclick="EditStylePage.update_font(this);" style="font-family:Helvetica" type='font' value='helvetica' layer_id='---layer_id---'>Helvetica</a> 
          <a href="javascript:void(0)" onclick="EditStylePage.update_font(this);" style="font-family:Times New Roman " type='font' value='times' layer_id='---layer_id---'>Times New Roman</a> 
          -->
          </nobr>       
          </div>
        </div>
      <div class="selectbox_group_more">
          <div class="selectbox_group selectbox_group_show"><a href="javascript:void(0);" onclick="EditStylePage.toggle_selectbox('font_size_---layer_id---')" id='font_size_---layer_id---_display'>Size 12</a> </div>
        <br style="clear:both" />
          <div id="font_size_---layer_id---" class="selectbox_layer" onclick="EditStylePage.close_selectbox('font_size_---layer_id---')" style="display:none;visibility:visible;"> 
          <nobr class="select_attendance_status"> 
          <span class="selectbox_list_type"> <?= srt_lang('Font Size') ?> (px)</span>
          <a href="javascript:void(0)" onclick="EditStylePage.update_size(this);" style="font-size:8px"  type='size' layer_id='---layer_id---' value="8">8</a>
          <a href="javascript:void(0)" onclick="EditStylePage.update_size(this);" style="font-size:9px"  type='size' layer_id='---layer_id---' value="9">9</a> 
          <a href="javascript:void(0)" onclick="EditStylePage.update_size(this);" style="font-size:10px" type='size' layer_id='---layer_id---' value="10">10</a> 
          <a href="javascript:void(0)" onclick="EditStylePage.update_size(this);" style="font-size:11px" type='size' layer_id='---layer_id---' value="11">11</a> 
          <a href="javascript:void(0)" onclick="EditStylePage.update_size(this);" style="font-size:12px" type='size' layer_id='---layer_id---' value="12" class="selected_attendance" >12</a> 
          <a href="javascript:void(0)" onclick="EditStylePage.update_size(this);" style="font-size:13px" type='size' layer_id='---layer_id---' value="13">13</a> 
          <a href="javascript:void(0)" onclick="EditStylePage.update_size(this);" style="font-size:14px" type='size' layer_id='---layer_id---' value="14">14</a>
          <a href="javascript:void(0)" onclick="EditStylePage.update_size(this);" style="font-size:16px" type='size' layer_id='---layer_id---' value="16">16</a>
          <a href="javascript:void(0)" onclick="EditStylePage.update_size(this);" style="font-size:18px; line-height:18px;" type='size' layer_id='---layer_id---' value="18">18</a>
          <a href="javascript:void(0)" onclick="EditStylePage.update_size(this);" style="font-size:20px; line-height:20px;" type='size' layer_id='---layer_id---' value="20">20</a>
          <a href="javascript:void(0)" onclick="EditStylePage.update_size(this);" style="font-size:22px; line-height:22px;" type='size' layer_id='---layer_id---' value="22">22</a>
          <a href="javascript:void(0)" onclick="EditStylePage.update_size(this);" style="font-size:24px; line-height:24px;" type='size' layer_id='---layer_id---' value="24">24</a>
          <a href="javascript:void(0)" onclick="EditStylePage.update_size(this);" style="font-size:26px; line-height:26px;" type='size' layer_id='---layer_id---' value="26">26</a>
          <a href="javascript:void(0)" onclick="EditStylePage.update_size(this);" style="font-size:28px; line-height:28px;" type='size' layer_id='---layer_id---' value="28">28</a>
          <a href="javascript:void(0)" onclick="EditStylePage.update_size(this);" style="font-size:36px; line-height:36px;" type='size' layer_id='---layer_id---' value="36">36</a>
          <a href="javascript:void(0)" onclick="EditStylePage.update_size(this);" style="font-size:48px; line-height:48px;" type='size' layer_id='---layer_id---' value="48">48</a>
          <a href="javascript:void(0)" onclick="EditStylePage.update_size(this);" style="font-size:72px; line-height:72px;" type='size' layer_id='---layer_id---' value="72">72</a>
          </nobr> 
          </div>
      </div>
      <div class="selectbox_group_more">
          <div class="choose_color" style="background:#99CC00;" title="<?= srt_lang('Choose Font Color') ?>" 
          onClick="javascript:EditStylePage.toggle_pickColor('font_color_---layer_id---');" id="font_color_---layer_id---Div">
          <a href="javascript:void(0);">&nbsp;</a></div>
          
        <div class="choose_style"> 
        	<!-- Bold and italic are not supported by Chinese fonts yet. -->
        	<a href="javascript:void(0)" style="display:none" onclick="EditStylePage.update_option(this, 'choose_bold', 'bold');" class="choose_bold" title="<?= srt_lang('Bold') ?>" type='option' layer_id="---layer_id---" id="bold_---layer_id---">&nbsp;</a> 
        	<a href="javascript:void(0)" onclick="EditStylePage.update_option(this, 'choose_underline','underline');" class="choose_underline" title="<?= srt_lang('Underline') ?>"  type='option' layer_id="---layer_id---" id="underline_---layer_id---">&nbsp;</a> 
        	<a href="javascript:void(0)" style="display:none" onclick="EditStylePage.update_option(this, 'choose_italic', 'italic');" class="choose_italic" title="<?= srt_lang('Italic') ?>" type='option' layer_id="---layer_id---" id="italic_---layer_id---">&nbsp;</a> 
        </div>
      </div>
      <span class="form_field_sub">&nbsp;&nbsp;&nbsp;&nbsp; <?= srt_lang('Background Color') ?> </span>
        <div class="selectbox_group_more" >
          <div class="choose_color" style="background:#99CC00;" title="<?= srt_lang('Choose Font Background Color') ?>"
          onClick="javascript:EditStylePage.toggle_pickColor('bg_color_---layer_id---');" id="bg_color_---layer_id---Div"
          ><a href="javascript:void(0);">&nbsp;</a></div>
        </div>
</div>