<!-- Modify by -->
<script>
var EditFooterPage ={
		
	_footer_data : 	parent.StudentReportTemplateEditPage.get_footer_data(),
	
	on_init: function(){
		var objData = this._footer_data;		
		$("#chk_display_page").attr("checked",objData.is_to_show_footer_on_last_page_only);
		<!-- Begin Henry Yuen (2010-04-20): footer height can be set by user --> 
		$('#height').val(objData.height);
		<!-- End Henry Yuen (2010-04-20): footer height can be set by user -->		
	},
		
	FillIn: function(){
		var field = FCKeditorAPI.GetInstance('Content');				
		field.InsertHtml($("#genVariable").val());
	},
	
	onSubmit: function(){
		<!-- Begin Henry Yuen (2010-04-20): footer height can be set by user --> 
		var height = $('#height').val();
		if(!(parseInt(height) >= 1 && parseInt(height) <= 100) || height.toString().search(/^-?[0-9]+$/) != 0){
			alert("<?= srt_lang('Panel Height') ?>: (1-100)");
			$('#height').focus();
			return false;
		}		
		<!-- End Henry Yuen (2010-04-20): footer height can be set by user -->
				
		var objData = {
			'is_to_show_footer_on_last_page_only': $('#chk_display_page:checked').length > 0 ? 1 : 0, 
			'footer_template' : FCKeditorAPI.GetInstance("Content").EditorWindow.parent.FCK.GetHTML(),
			<!-- Begin Henry Yuen (2010-04-20): footer height can be set by user --> 
			'height' : $('#height').val()
			<!-- End Henry Yuen (2010-04-20): footer height can be set by user --> 
		};
		parent.StudentReportTemplateEditPage.update_footer_data(objData);		
		parent.tb_remove();	
	}
};

$(document).ready(function() {		
	EditFooterPage.on_init();
});

function FCKeditor_OnComplete(EDITOR){
	var objData = EditFooterPage._footer_data;
	// Prevent JS error in IE if template is undefined.
	if (objData.footer_template) {
		EDITOR.SetHTML(objData.footer_template);
	}
}

</script>


<div class="edit_pop_board">
	<h1><span> <?= srt_lang('Edit Footer') ?> </span></h1>
    <div class="edit_pop_board_write">
      <table class="form_table" style="width:100%; margin:0 auto">
        <tr>                    
          <td ><input name="chk_display_page" type="checkbox" id="chk_display_page" /><?= srt_lang('Show on last page only') ?></td>          
        </tr>
        <!-- Begin Henry Yuen (2010-04-20): footer height can be set by user -->
        <tr>                    
          <td ><?= srt_lang('Height') ?>: <input name="height" type="textbox" id="height" size="3" maxlength="3" value="30" /> mm (1-100)</td>          
        </tr>        
        <!-- End Henry Yuen (2010-04-20): footer height can be set by user -->        
        <tr><td>
        <div id="genVariableDiv">
        	<?= srt_lang('Auto-fill Information') ?>:
        	<?=$select?>
        	<input type="button" onClick="EditFooterPage.FillIn()" value="<?=srt_lang('Insert')?>">        
        </div>
        </td></tr>
        <tr>
        <td id="fck_panel"><?=$objHtmlEditor->Create()?></td>
        </tr>
        <col class="field_title" />
        <col  class="field_c" />
      </table>
    </div>
<div class="edit_bottom">
		<p class="spacer"></p>
		<input name="submit2" type="button" class="formbutton" onclick="EditFooterPage.onSubmit();"
		 onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?= srt_lang('Submit') ?>" />
		<input name="submit2" type="button" class="formbutton" onclick="parent.tb_remove()" 
		onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?= srt_lang('Cancel') ?>" />
		<p class="spacer"></p>
	</div>
</div>

