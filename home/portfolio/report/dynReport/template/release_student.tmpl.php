<?php
// Editing by 
//echo $linterface->Include_Cookies_JS_CSS();

include 'tab_menu.tmpl.php';

?>
<script type="text/javascript" language="JavaScript">

function js_Reload_Table()
{
	$('#task').val('ajax_release_student_task');
	$('#ajax_task').val('getReleaseStudentDynamicReportDBTable');
	var postData = $('#form1').serialize();
	
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"index.php", 
		postData,
		function(ReturnData) {
			$('#task').val('release_student');
			$('#ajax_task').val('');
			Scroll_To_Top();
		}
	);
}

function js_Search_Report(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		js_Reload_Table();
	}
	else
		return false;
}

function js_Release_To_Student(releaseStatus)
{
	$('#task').val('ajax_release_student_task');
	$('#ajax_task').val('releaseDynamicReportToStudent');
	var postData = $('form#form1').serialize();
	postData += '&ReleaseUnrelease=' + releaseStatus;
	
	Block_Element('form1');
	$.post(
		'index.php',
		postData,
		function(returnData){
			UnBlock_Element('form1');
			if(returnData == "1") {
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
			} else {
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnSuccess']?>');
			}
			js_Reload_Table();
		}
	);
}

function js_Release_Event(release)
{
	if(release == '1') {
		checkEditMultiple2(document.getElementById('form1'), 'RecordID[]', 'js_Release_To_Student(1)');
	} else {
		checkEditMultiple2(document.getElementById('form1'), 'RecordID[]', 'js_Release_To_Student(0)');
	}
}

function js_Regenerate_Event()
{
	checkEditMultiple2(document.getElementById('form1'), 'RecordID[]', 'js_Regenerate_Report()');
}

function js_Regenerate_Report()
{
	$('#task').val('regenerate_student_report');
	var postData = $('form#form1').serialize();
	
	Block_Element('form1');
	$.post(
		'index.php',
		postData,
		function(returnData){
			UnBlock_Element('form1');
			if(returnData == "") {
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
			} else {
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnSuccess']?>');
			}
			js_Reload_Table();
		}
	);
}

function js_Delete_Event()
{
	checkRemove2(document.getElementById('form1'), 'RecordID[]', 'js_Delete_Record()');
}

function js_Delete_Record()
{
	$('#task').val('ajax_release_student_task');
	$('#ajax_task').val('deleteReleasedStudentDynamicReport');
	var postData = $('form#form1').serialize();
	
	Block_Element('form1');
	$.post(
		'index.php',
		postData,
		function(returnData){
			UnBlock_Element('form1');
			if(returnData == "1") {
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>');
			} else {
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['DeleteUnSuccess']?>');
			}
			js_Reload_Table();
		}
	);
}

$(document).ready(function(){
	//js_Reload_Table();
});
</script>

<br />
<form id="form1" name="form1" method="POST" action="index.php?task=release_student" onsubmit="return false;">
	<br style="clear:both" />
	<div class="content_top_tool">
		<!--
		<div class="Conntent_tool">
			<br style="clear:both" />
		</div>
		-->
		<?=$h_toolButton?>
		<?=$h_searchBox?>
	</div>
	<br style="clear:both" />
	
	<?=$h_navigation?>
	
	<div class="table_board">
		<?=$h_actionButton?>
		<div id="DBTableDiv">
			<?=$h_dbtable?>
		</div>	
	</div>
	
	<input type="hidden" id="task" name="task" value="release_student" />
	<input type="hidden" id="ajax_task" name="ajax_task" value="" />
</form>
<br/>
