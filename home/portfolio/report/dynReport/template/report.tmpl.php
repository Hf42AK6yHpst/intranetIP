<?php
// Editing by 
/****************************************** Change Log *************************************************
 * 2017-06-22 (Villa):	modified on_submit(): alert user if no student selected
 * 2015-11-04 (Omas):	added setting for teacher comments
 * 2015-04-15 (Omas):	improved UI and add js searchForAlumni(),checkAllAlumniCheckBox(), modified on_submit() to control selection between alumni and current student
 * 2013-03-20 (Carlos):	add options [Release to student] and [Release status]
 */
?>
<script>
// Called when the print button is clicked.
function on_submit(form, printFormat) {
	
	if($('#trgType_current').attr('checked')){
		try {
			// Check whether the class and the template are selected.
			if (!$('#targetClass').val()) throw '<?= srt_lang('Please select a class.') ?>';
			if (!$('#template_id').val()) throw '<?= srt_lang('Please select a template.') ?>';
			
			if (!$('input[name=AcademicYearIDs[]]:checked').length > 0) throw '<?= srt_lang('Please select at least one year.') ?>';
			if (!($('input[name=AcademicYearIDs[]]:checked').length > 1 || ($('#YTCheckMaster').attr('checked') || $('input[name=YearTermIDs[]]:checked').length > 0))) throw '<?= srt_lang('Please select at least one semester.') ?>';
			if (!$('select#targetStudentSel option:selected').length>0) throw '<?= srt_lang('Please select at least one student.') ?>';
			$('input#printFormat').val(printFormat);
			
			if (printFormat == '<?=$ipf_cfg['dynReport']['batchPrintMode']['pdf']?>') {
				on_print_with_progress();
				form.target = 'print_with_progress_iframe';
				form.submit();
			}
			else if ('<?=$ipf_cfg['dynReport']['batchPrintMode']['html']?>') {
				form.target = '_blank';
				form.submit();
			}
		}
		catch (e) {
			alert(e);
		}
	}
	else{
		var student_checked = $("input[id^='alumni_studentArr_']:checked").size();
		if( student_checked > 0 ){
			try {
				if (!$('#template_id').val()) throw '<?= srt_lang('Please select a template.') ?>';
				if (!$('input[name=AcademicYearIDs[]]:checked').length > 0) throw '<?= srt_lang('Please select at least one year.') ?>';
				if (!($('input[name=AcademicYearIDs[]]:checked').length > 1 || ($('#YTCheckMaster').attr('checked') || $('input[name=YearTermIDs[]]:checked').length > 0))) throw '<?= srt_lang('Please select at least one semester.') ?>';
			
				if (printFormat == '<?=$ipf_cfg['dynReport']['batchPrintMode']['pdf']?>') {
				on_print_with_progress();
				form.target = 'print_with_progress_iframe';
				form.submit();
			}
			else if ('<?=$ipf_cfg['dynReport']['batchPrintMode']['html']?>') {
				form.target = '_blank';
				form.submit();
			}
			}
			catch (e) {
				alert(e);
			}
		}
		else{
			alert('<?=$Lang['ModuleLicense']['Err_NoStudentsSelected']?>');
			$("#r_alumni_studentName").focus();
		}
	}
}

/**
 * Called by on_submit() before calling the task print_with_progress, to prepare the UI:
 * - Hide the Print button.
 * - Show and disable the Download button.
 * - Show and reset the progress bar.
 */
function on_print_with_progress() {
	$('#export_pdf_button').hide();
	$('#export_html_button').hide();
	$('#download_button').show();
	$('#download_button').attr('disabled', 'disabled');
	$('#download_button').removeClass('formbutton');
	$('#download_button').addClass('formbutton_disable');
	$('#progress_container').css('visibility', 'visible');
	on_progress_update(0);
}

/**
 * Called when the download button is clicked.
 * Also rollback the UI changes made by on_print_with_progress().
 */
function on_download() {
	location.href='index.php?task=print_by_ticket&ticket=' + on_download_ready.ticket;
	
	$('#export_pdf_button').show();
	$('#export_html_button').show();
	$('#download_button').hide();
	$('#progress_container').css('visibility', 'hidden');
}

// A callback for the task print_with_progress.
// Called to update the progress.
function on_progress_update(percentage) {
	$('#progress').text(percentage + '%');
	$('#progress_bar').css('width', percentage + '%');
}
// A callback for the task print_with_progress.
// Called when the download is ready.
function on_download_ready(ticket) {
	// Save the ticket for retrieving the report later by on_download().
	on_download_ready.ticket = ticket;
	
	// Enable the Download button.
	$('#download_button').removeAttr('disabled');
	$('#download_button').removeClass('formbutton_disable');
	$('#download_button').addClass('formbutton');
}

// Copied and modified from home/portfolio/profile/report/template/report_printing_menu.tmpl.php.
// Called to change the student list.
function jCHANGE_CLASS()
{
	if($("#targetClass").val() != "")
	{
		$.ajax({
			type: "GET",
			url: "index.php",
			data: {
				task: 'report_get_class_student',
				targetClass: $("#targetClass").val(),
				isMultiple: 1
			},
			beforeSend: function () {
				$("#studentRow").find("td").eq(1).html('<img src="<?=$intranet_httppath?>/images/2009a/indicator.gif" />');
			},
			success: function (msg) {
				if(msg != "")
				{
					$("#studentRow").find("td").eq(1).html(msg);
					$("select#targetStudentSel").change(function(){
						jCHANGE_STUDENT();
					});
					js_Select_All('targetStudentSel', 1);
					jCHANGE_STUDENT();
				}
			}
		});
		
		$("#studentRow").show();
	}
	else
	{
		$("#studentRow").hide();
	}
}

function jCHANGE_ALUMNI() {
	// delay run the check of num by Omas
	setTimeout(function(){
		var numOfSelected = $('input[name=' + 'alumni_StudentID[]' + ']:checked').length;
	
		console.log(numOfSelected);
		if (numOfSelected > 1) {
			$("#batchPrintModeTr").show();
		}
		else {
			$("#batchPrintModeTr").hide();
		}
	},100); 
}

function jCHANGE_STUDENT() {
	if ($("select#targetStudentSel option:selected").length > 1) {
		$("#batchPrintModeTr").show();
	}
	else {
		$("#batchPrintModeTr").hide();
	}
}

function jCHECK_YEARTERM(checkMasterObj)
{
	var checked_status = checkMasterObj.checked;
  $("input[name=YearTermIDs[]]").each(function()
	{
		this.checked = checked_status;
		this.disabled = checked_status;
	});
}

function jSHOW_YEARTERM(){
  var ayChecked = $("input[name=AcademicYearIDs[]]:checked").length;
  
  if(ayChecked == 1)
  {
    // call ajax to return year term selection only when one year is selected
		$.ajax({
			type: "GET",
			url: "../../ajax/ajax_get_yearterm_multiopt.php",
			data: {
				ay_id: $("input[name=AcademicYearIDs[]]:checked").val(),
				without_background: 1
			},
			beforeSend: function () {
				$("#yearTermRow").find("td").eq(1).html('<img src="<?=$intranet_httppath?>/images/2009a/indicator.gif" />');
			},
			success: function (msg) {
				if(msg != "")
				{
					$("#yearTermRow").find("td").eq(1).html(msg);
				}
			}
		});
		
		$("#yearTermRow").show();
  }
  else
  {
    $("#yearTermRow").find("td").eq(1).html("");
    $("#yearTermRow").hide();
  }
}

function on_change_show_no_data_table(jsValue) {
	if (jsValue == 1) {
		// Show Content Div
		$('div#NoDataTableContentDiv').slideDown();
	}
	else {
		// Hide Content Div
		$('div#NoDataTableContentDiv').slideUp();
	}
}

function displayOLEOrderOptions(fromOption){
	if(fromOption == 1){
		$("#id_OLEOrderOptionsDiv").hide();
	}else{
		$("#id_OLEOrderOptionsDiv").show();
	}
}

function on_release_to_student(isOn, elementId)
{
	if(isOn == 1) {
		$('#' + elementId).show();
	} else {
		$('#' + elementId).hide();
	}
}

function changeSelectReportTarget(reportTargetType){
 /*reportTargetType = 1  , print for current student
  *reportTargetType = 2  , print for alumni student
 */
	if(reportTargetType == 1){
		$("#studentClassRow").show();
		$("#alumniRow").hide();

		//reset alumni UI related item
	    $("#trgType_current").attr('checked',true);	
		$("#alumni_div").html(''); //reset HTML table , result set of "ALUMNI STUDENT"
		$("#r_alumni_studentName").val('');
		$("#r_alumni_studentWebsams").val('');

	}else{
		$("#ipf_targetForCurrent").hide();
		$("#alumniRow").show();
		$("#studentClassRow").hide();
		$("#studentRow").hide();
	}

}

function searchForAlumni(){
	var s_name = $("#r_alumni_studentName").val();
	var s_websams = $("#r_alumni_studentWebsams").val();
	
	if(s_name == '' && s_websams == ''){
		alert('<?= $iPort["msg"]["InputOneItem"]?>');
		$('#r_alumni_studentName').focus();
		return false;
	}

	$.ajax({
    url:      "searchAlumniStudent.php",
    type:     "POST",
    async:    true,
    data:     "r_alumni_studentName="+s_name+"&r_alumni_studentWebsams="+s_websams,
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){
					$("#alumni_div").html(data);
              }
	});

}

function checkAllAlumniCheckBox (flag){
   if (flag == true){
      $("input[id^='alumni_studentArr_']").attr('checked', true);
   }
   else{
      $("input[id^='alumni_studentArr_']").attr('checked', false);
   }
	return;
}

// bind "check all" action to Academic Year "Check All" checkbox
$(document).ready(function()
{
	//default semester
	jLOAD_COMMENTS_SEMESTER();
	
	//default current student
	changeSelectReportTarget(1);
	
	$("#AYCheckMaster").click(function()				
	{
		var checked_status = this.checked;
    	$("input[name=AcademicYearIDs[]]").each(function()
		{
			this.checked = checked_status;
     		$("#yearTermRow").find("td").eq(1).html("");
      		$("#yearTermRow").hide();
		});
		
		// Check if there is only one year and show semester selection
		jSHOW_YEARTERM();
	});
	
	$("input[name=AcademicYearIDs[]]").click(function(){
		jSHOW_YEARTERM();
	});
	
	$("input[name=AcademicYearIDs[]]").click(function(){
		jSHOW_YEARTERM();
	});
});

function jLOAD_COMMENTS_SEMESTER()
{
  var targetAYear = document.getElementById('commentsAY').value;
  var commentsSemDiv = document.getElementById("commentsSemDiv");
  commentsSemDiv.innerHTML = '<?='<img src="/images/'.$LAYOUT_SKIN.'/indicator.gif">'?>';
  $.ajax({
			type: 'GET',
			url: "../../ajax/get_semester_selection_ajax.php",
			async: false,
			data: {	academicyearId: targetAYear},
			success: function(responseText){
        		commentsSemDiv.innerHTML = responseText;
			}
  });
}

</script>
<!-- ###### Tabs Start ######-->
<?php include 'tab_menu.tmpl.php'; ?>
<!-- ###### Tabs End ######-->
<div style="width:100%;" align="center">
	<div style="width:98%;">
		<form name="form1" action="index.php?task=print_with_progress" method="post">
			<?=$h_GeneralNavigation?>
			<br style="clear:both;" />
			<table class="form_table_v30" style="width:100%;">
				<tr>
					<td  class="field_title"><?= srt_lang('Template') ?></td>
					<td valign="top" class="tabletext">
						<!--<?= getSelectByArray(htmlspecialchars_deep($template_manager->get_list_of_printable_report_templates_as_id_and_name()), 'name="template_id" id="template_id"'); ?>-->
						<?= getSelectByAssoArray(htmlspecialchars_deep($template_manager->get_list_of_printable_report_templates_as_id_and_name_with_optgroup()), 'name="template_id" id="template_id"'); ?>
					</td>
				</tr>
				<tr>
					<td class="field_title"><?= $Lang['Identity']['Student']?></td>
					<td class="tabletext">
						<input type="radio" name="trgType" value = "current" id="trgType_current" onclick="changeSelectReportTarget('1')" checked><label for="trgType_current"><?= $iPort["CurrentStudent"]?></label>&nbsp;&nbsp;
						<input type="radio" name="trgType" value = "alumni" id="trgType_alumni" onclick="changeSelectReportTarget('2')"><label for="trgType_alumni"><?= $Lang['Identity']['Alumni']?></label>
					</td>
				</tr>
				
				<tr id="studentClassRow" style='display:none'>
					<td  class="field_title"><?= srt_lang('Class') ?></td>
					<td valign="top" class="tabletext">
						<?= $libclass->getSelectClass('name="targetClass" id="targetClass" onChange="jCHANGE_CLASS()"'); ?>
					</td>
				</tr>
				<tr id='studentRow' style='display:none'>
					<td nowrap="nowrap" class="field_title"><?=$i_identity_student?></td>
					<td valign="top" class="tabletext">
					</td>
				</tr>
				
				<tr id="alumniRow" style="display:none">
					<td class="field_title"><?=$Lang['Identity']['Alumni']?></td>
					<td>
						<table border="0">
							<tr>
								<td><?=$Lang['AccountMgmt']['StudentName']?> : </td><td><input type="text" name="r_alumni_studentName" id="r_alumni_studentName"></td>
								<td><?=$ec_iPortfolio['heading']['student_regno']?> : </td><td><input type="text" name="r_alumni_studentWebsams" id="r_alumni_studentWebsams"></td>
								<td><input type="button" onClick="searchForAlumni()" value="<?=$Lang['Btn']['Search']?>"></td>
							</tr>
						</table>
						<div id="alumni_div"></div>
					</td>
				</tr>
				
				<tr>
					<td nowrap="nowrap" class="field_title"><?=$ec_iPortfolio['year']?></td>
					<td valign="top" class="tabletext">
					  <?=$ay_selection_html?>
					</td>
				</tr>
				<tr id='yearTermRow' style='display:none'>
					<td nowrap="nowrap" class="field_title"><?=$ec_iPortfolio['semester']?></td>
					<td valign="top" class="tabletext">
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap" class="field_title"><?=$Lang['iPortfolio']['DynamicReport']['PrintingDirection']?></td>
					<td valign="top" class="tabletext">
						<?=$linterface->Get_Radio_Button('PrintingDirectionRadio_P', 'PrintingDirection', $ipf_cfg['dynReport']['printingDirection']['portrait'], $isChecked=1, $Class="", $Lang['iPortfolio']['DynamicReport']['Portrait'])?>
						<?=$linterface->Get_Radio_Button('PrintingDirectionRadio_L', 'PrintingDirection', $ipf_cfg['dynReport']['printingDirection']['landscape'], $isChecked=0, $Class="", $Lang['iPortfolio']['DynamicReport']['Landscape'])?>
					</td>
				</tr>

					<tr id='batchPrintModeTr' style='display:none'>
						<td nowrap="nowrap" class="field_title"><?=$Lang['iPortfolio']['StudentReport']['PrintingMode']?></td>
						<td valign="top" class="tabletext">
							<?=$linterface->Get_Radio_Button('batchPrintMode_all', 'batchPrintMode', $ipf_cfg['dynReport']['batchPrintMode']['all'], $isChecked=1, $Class="", $Lang['iPortfolio']['StudentReport']['PrintingMode_All'])?>
							<br />
							<?=$linterface->Get_Radio_Button('batchPrintMode_zip', 'batchPrintMode', $ipf_cfg['dynReport']['batchPrintMode']['zip'], $isChecked=0, $Class="", $Lang['iPortfolio']['StudentReport']['PrintingMode_Zip'])?>
						</td>
					</tr>
				
				<tr>
					<td nowrap="nowrap" class="field_title"><?=$Lang['iPortfolio']['DynamicReport']['ReleaseToStudent']?></td>
					<td valign="top" class="tabletext">
						<?=$linterface->Get_Radio_Button('ReleaseToStudent_Y','ReleaseToStudent', '1', 0,'', $Lang['General']['Yes'], 'on_release_to_student(this.value,\'releaseStatusTr\');') ?>
						<?='&nbsp;'.$linterface->Get_Radio_Button('ReleaseToStudent_N','ReleaseToStudent', '0', 1,'', $Lang['General']['No'], 'on_release_to_student(this.value,\'releaseStatusTr\');') ?>
					</td>
				</tr>
				
				<tr id="releaseStatusTr" style="display:none">
					<td nowrap="nowrap" class="field_title"><?=$Lang['iPortfolio']['DynamicReport']['ReleaseStatus']?></td>
					<td valign="top" class="tabletext">
						<?=$linterface->Get_Radio_Button('ReleaseStatus_Y','ReleaseStatus', '1', 0,'', $Lang['iPortfolio']['DynamicReport']['Release'], '') ?>
						<?='&nbsp;'.$linterface->Get_Radio_Button('ReleaseStatus_N','ReleaseStatus', '0', 1,'', $Lang['iPortfolio']['DynamicReport']['Unrelease'], '') ?>
					</td>
				</tr>
				
			</table>
			<br style="clear:both;" />
			
			<div style="float:left;"><?=$h_AdvancedNavigation?></div>
			<div style="float:left;">&nbsp;&nbsp;&nbsp;&nbsp;</div>
			<div style="float:left;"><?=$h_ShowHideAdvSettingsSpan?></div>
			<br style="clear:both;" />
			
			<div id="AdvSettingsDiv" style="display:none;">

				<div style="float:left;"><?=$h_IssuesDateTitle?></div>
				<br/>
				<table class="form_table_v30" style="width:100%;">
					<tr>
						<td nowrap="nowrap" class="field_title"><?=$Lang['iPortfolio']['DynamicReport']['IssueDate']?></td>
						<td valign="top" class="tabletext">
							<input type="text" class="textboxnum" name="IssueDate" id="IssueDate" value="<?=date("Y-m-d")?>" />(<?=$Lang['iPortfolio']['DynamicReport']['IssueDateGuide']?>)
						</td>
		            </tr>
				</table>
				<br style="clear:both;" />


				<div style="float:left;"><?=$h_DataContentDisplaySubTitle?></div>
				<br style="clear:both;" />
				<table class="form_table_v30" style="width:100%;">
					<tr>
		              <td class="field_title"><?=$Lang['iPortfolio']['DynamicReport']['EmptyDataSymbol']?></td>
		              <td>
		                <input type="text" class="textboxnum" name="EmptyDataSymbol" id="EmptyDataSymbol" value="" />
		              </td>
		            </tr>
		            <tr>
		              <td class="field_title"><?=$Lang['iPortfolio']['DynamicReport']['ShowNoDataTable']?></td>
		              <td>
		              	<table style="width:100%;">
		              		<tr>
		              			<td style="width:1%;border:0px;"><input type="radio" name="ShowNoDataTable" id="ShowNoDataTable_Yes" value="1" onchange="on_change_show_no_data_table(this.value);" CHECKED /></td>
		              			<td style="border:0px;">
		              				<label for="ShowNoDataTable_Yes"><?=$Lang['General']['Yes']?></label>
		              				<br />
		              				<div id="NoDataTableContentDiv" style="width:100%;">
				              			<?=$Lang['iPortfolio']['DynamicReport']['NoDataTableContent']?>&nbsp;&nbsp;
				              			<input type="text" class="textboxtext" style="width:80%;" name="NoDataTableContent" id="NoDataTableContent" value="<?=intranet_htmlspecialchars($no_record_msg)?>" />
				              		</div>
		              			</td>
		              		</tr>
		              		<tr>
		              			<td style="border:0px;"><input type="radio" name="ShowNoDataTable" id="ShowNoDataTable_No" value="0" onchange="on_change_show_no_data_table(this.value);" /></td>
		              			<td style="border:0px;"><label for="ShowNoDataTable_No"><?=$Lang['General']['No']?></label></td>
		              		</tr>
		              	</table>
		              </td>
		            </tr>
				</table>
				<br style="clear:both;" />
			
				<div style="float:left;"><?=$h_RecordSubTitle?></div>
				<table class="form_table_v30" style="width:100%;">
					<tr>
		              <td class="field_title"><?=$Lang['iPortfolio']['SLPRecord']?></td>
		              <td>
		                <input type="radio" name="slpPrintRec" id="slpRecAll" value="all" CHECKED onclick="displayOLEOrderOptions(1);"/> <label for="slpRecAll"><?=$Lang['iPortfolio']['allRecord']?></label>
		                <br />
		                <input type="radio" name="slpPrintRec" id="slpRecSel" value="sel" onclick="displayOLEOrderOptions(2);"/> <label for="slpRecSel"><?=$Lang['iPortfolio']['selectedRecord']?></label>
							<div id="id_OLEOrderOptionsDiv" style="display:none;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="rdDisplayOLEOrderOptions" id="id_rdDisplayOLEOrderOptions1" value = "studentSelected" CHECKED/>
								<label for="id_rdDisplayOLEOrderOptions1"><?=$Lang['iPortfolio']['DynamicReport']['OrderByStudentSelectedOrdering']?></label>
								<br/>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="rdDisplayOLEOrderOptions" id="id_rdDisplayOLEOrderOptions2" value = "sameAsReportConfig"/>
								<label for="id_rdDisplayOLEOrderOptions2"><?=$Lang['iPortfolio']['DynamicReport']['OrderByReportSetting']?></label>
							</div>
							
		              </td>
		            </tr>
				</table>
				<br style="clear:both;" />
				
				<div style="float:left;"><?=$h_AcademicResultSubTitle?></div>
				<table class="form_table_v30" style="width:100%;">
					<tr>
						<td nowrap="nowrap" class="field_title"><?=$Lang['iPortfolio']['DynamicReport']['DisplayFullMark']?></td>
						<td valign="top" class="tabletext">
							<?=$linterface->Get_Radio_Button('DisplayFullMark_Yes', 'DisplayFullMark', '1', $isChecked=1, $Class="", $Lang['General']['Yes'])?>
							<?=$linterface->Get_Radio_Button('DisplayFullMark_No', 'DisplayFullMark', '0', $isChecked=0, $Class="", $Lang['General']['No'])?>
						</td>
		            </tr>
				</table>
				<br style="clear:both;" />
				
				<div style="float:left;"><em class="form_sep_title">- <?=$ec_iPortfolio['title_teacher_comments']?> -</em></div>
				<table class="form_table_v30" style="width:100%;">
					<tr>
						<td nowrap="nowrap" class="field_title"><?=$ec_iPortfolio['school_year']?></td>
						<td valign="top" class="tabletext">
							<?=$comments_ay_select?><div id="commentsSemDiv" style="display:inline;"></div>
						</td>
		            </tr>
				</table>
			</div>
			<br style="clear:both;" />
			
			<div class="edit_bottom_v30">
				<table style="width:100%;text-align:center;">
					<tr>
						<td align="center">
							<div id="progress_container" style="position: relative; visibility: hidden; width: 150px; text-align: left; background-color: #f6f6f6; border: 1px solid #aeaeae; padding: 2px; margin-bottom: 5px">
								<div id="progress" style="position:absolute; width:100%; height:14x; text-align: center; color: #333333; font-size: 11px; font-family:Verdana, Arial, Helvetica">0%</div>
								<img id="progress_bar" src="<?=$intranet_httppath?>/images/2009a/iMail/usage_bar.gif" align="absmiddle" height="14" width="0%">
							</div>
						</td>
					</tr>
				</table>
				<? /* $linterface->GET_ACTION_BTN($Lang['Btn']['Print'], "button", "on_submit(this.form, '".$ipf_cfg['dynReport']['batchPrintMode']['html']."')", 'export_html_button') */ ?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($ec_iPortfolio['SLP']['ExportAsPdf'], "button", "on_submit(this.form, '".$ipf_cfg['dynReport']['batchPrintMode']['pdf']."')", 'export_pdf_button')?>&nbsp;	
				<?= $linterface->GET_ACTION_BTN(srt_lang('Download'), "button", "on_download()", 'download_button', 'style="display:none"') ?>&nbsp;
			</div>
			
			<input type="hidden" id="printFormat" name="printFormat" value="" />
		</form>
		<iframe name="print_with_progress_iframe" frameborder="0" height="100px" width="100%"></iframe>
	</div>
</div>