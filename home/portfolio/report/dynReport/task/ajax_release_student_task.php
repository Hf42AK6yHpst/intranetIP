<?php
// editing by 
if(!isset($lpf)) {
	$lpf = new libportfolio2007();
}

$ajax_task = $_REQUEST['ajax_task'];

switch($ajax_task)
{
	case "getReleaseStudentDynamicReportDBTable":
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		$filterAry = array('Keyword' => $Keyword);
		$filterAry['ReleaseStatus'] = $_REQUEST['ReleaseStatus'];
		$filterAry['TemplateID'] = $_REQUEST['TemplateID'];
		$filterAry['GenerateBy'] = $_REQUEST['GenerateBy'];
		$filterAry['GenerateStartDate'] = $_REQUEST['GenerateStartDate'];
		$filterAry['GenerateEndDate'] = $_REQUEST['GenerateEndDate'];
		
		$field = trim(urldecode(stripslashes($_REQUEST['field'])));
		$order = trim(urldecode(stripslashes($_REQUEST['order'])));
		$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
		$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));
		
		$arrCookies = array();
		$arrCookies[] = array("ck_ipf_dynamic_report_release_student_page_size", "numPerPage");
		$arrCookies[] = array("ck_ipf_dynamic_report_release_student_page_number", "pageNo");
		$arrCookies[] = array("ck_ipf_dynamic_report_release_student_order", "order");
		$arrCookies[] = array("ck_ipf_dynamic_report_release_student_field", "field");
		$arrCookies[] = array("ck_ipf_dynamic_report_release_student_keyword", "Keyword");
		updateGetCookies($arrCookies);
		
		echo $lpf->getReleaseStudentDynamicReportDBTable($filterAry, $field, $order, $pageNo, $numPerPage);
	break;
	
	case "releaseDynamicReportToStudent":
		$RecordID = $_REQUEST['RecordID'];
		$ReleaseUnrelease = $_REQUEST['ReleaseUnrelease'];
		
		$success = $lpf->releaseDynamicReportToStudent($RecordID,$ReleaseUnrelease);
		echo $success? "1" : "0";
	break;
	
	case "deleteReleasedStudentDynamicReport":
		$RecordID = $_REQUEST['RecordID'];
		
		$success = $lpf->deleteReleasedStudentDynamicReport($RecordID);
		echo $success? "1" : "0";
	break;
}

?>