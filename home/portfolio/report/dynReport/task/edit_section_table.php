<?php
// Using By: 
/*
 * 2013-01-09 Ivan
 * 	- added Statistics Selection template coding
 */

include_once("$intranet_root/includes/portfolio25/dynReport/StudentReportDataSourceManager.php");
include_once("$intranet_root/includes/portfolio25/dynReport/JSON.php");
include_once($PATH_WRT_ROOT.'/includes/portfolio25/slp/libpf-ole.php');
include_once($PATH_WRT_ROOT.'includes/portfolio25/oleCategory/libpf-category.php');


## Get Params
$section_index = (isset($_REQUEST['section_index']))? trim($_REQUEST['section_index']) : "";

# Get Data source info
$objDataSrc = new StudentReportDataSourceManager();
$aryData = $objDataSrc->get_data_source_to_fields_map();
$data_source_and_field_display_names = $objDataSrc->get_data_source_and_field_display_names();


$json = new Services_JSON();
$lpf_dynReport_ui = new StudentReport_ui();

// Include the template, which is relative to the index page.$linterface = new interface_html("popup4.html");
$linterface = new interface_html("popup5.html");


### Tab
$TabMenuArr = array();
$TabMenuArr[] = array("javascript:EditSectionPage.switch_tab('display');", $Lang['iPortfolio']['Display'], 1, 'tab_menu_display');
$TabMenuArr[] = array("javascript:EditSectionPage.switch_tab('filter');", $Lang['iPortfolio']['Filter'], 0, 'tab_menu_filter');
$h_TabMenu = $linterface->GET_TAB_MENU($TabMenuArr);
	
	
### Div Content
$h_filter_div = $lpf_dynReport_ui->Get_DataContent_Criteria_Layer();


### Statistics Selection template
$optionAry = $Lang['iPortfolio']['DynamicReport']['StatisticsArr']['StatOptionAry'];
$x = '';
$x .= '<option value="">'.$Lang['iPortfolio']['DynamicReport']['StatisticsArr']['Blank'].'</option>';
foreach ((array)$optionAry as $_statTypeCode => $_display) {
	$x .= '<option value="'.$_statTypeCode.'">'.$_display.'</option>';
}
$h_statisticsOption = $x;


$linterface->LAYOUT_START();
include_once("template/edit_section_table.tmpl.php");
$linterface->LAYOUT_STOP();

?>