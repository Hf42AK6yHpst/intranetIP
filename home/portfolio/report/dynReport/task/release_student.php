<?php
// Editing by 
include_once("$intranet_root/includes/libdbtable.php");
include_once("$intranet_root/includes/libdbtable2007a.php");
//include_once("$intranet_root/includes/libclass.php");
//include_once("$intranet_root/includes/form_class_manage.php");
include_once("$intranet_root/includes/portfolio25/dynReport/StudentReportTemplateManager.php");

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_ipf_dynamic_report_release_student_page_size", "numPerPage");
$arrCookies[] = array("ck_ipf_dynamic_report_release_student_page_number", "pageNo");
$arrCookies[] = array("ck_ipf_dynamic_report_release_student_order", "order");
$arrCookies[] = array("ck_ipf_dynamic_report_release_student_field", "field");
$arrCookies[] = array("ck_ipf_dynamic_report_release_student_keyword", "Keyword");

if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
	$ck_ipf_dynamic_report_release_student_page_size = '';
}
else {
	updateGetCookies($arrCookies);
}

if(!isset($lpf)) {
	$lpf = new libportfolio2007();
}
$template_manager = new StudentReportTemplateManager();

$filterAry = array();

$currentAcademicYearID = Get_Current_Academic_Year_ID();
$academicYearStartDate = date("Y-m-d",strtotime(getStartDateOfAcademicYear($currentAcademicYearID)));
$academicYearEndDate = date("Y-m-d", strtotime(getEndDateOfAcademicYear($currentAcademicYearID)));
$GenerateStartDate = isset($_REQUEST['GenerateStartDate']) && $_REQUEST['GenerateStartDate'] != ""? $_REQUEST['GenerateStartDate']: $academicYearStartDate;
$GenerateEndDate = isset($_REQUEST['GenerateEndDate']) && $_REQUEST['GenerateEndDate'] != ""? $_REQUEST['GenerateEndDate']: $academicYearEndDate;

### Navigation 
$NavigationArr = array();
$NavigationArr[] = array($Lang['iPortfolio']['DynamicReport']['ReleaseToStudent']);
$h_navigation = $linterface->GET_NAVIGATION_IP25($NavigationArr);


### Toolbar
$h_searchBox = $linterface->Get_Search_Box_Div('Keyword', $_REQUEST['Keyword'], 'onkeypress="js_Search_Report(event);"');	// keyword initiated by JS

$h_toolButton = '';
$h_toolButton .= srt_lang('Template').': '.getSelectByAssoArray(htmlspecialchars_deep($template_manager->get_list_of_printable_report_templates_as_id_and_name_with_optgroup()), 'name="TemplateID" id="TemplateID" onchange="js_Reload_Table();" ', $_REQUEST['TemplateID']);

$generateByAry = $lpf->getReleasedStudentDynamicReportGenerator();
$h_toolButton .= '&nbsp;|&nbsp;'.$Lang['iPortfolio']['SLP']['DynamicReport']['GeneratedBy'].': '.$linterface->GET_SELECTION_BOX($generateByAry, ' id="GenerateBy" name="GenerateBy" onchange="js_Reload_Table();" ', $Lang['General']['All'], $_REQUEST['GenerateBy']);

$releaseStatusAry = array();
$releaseStatusAry[] = array('1',$Lang['iPortfolio']['DynamicReport']['Released']);
$releaseStatusAry[] = array('0',$Lang['iPortfolio']['DynamicReport']['Unrelease']);
$h_toolButton .= '&nbsp;|&nbsp;'.$Lang['General']['Status'].': '. $linterface->GET_SELECTION_BOX($releaseStatusAry, ' id="ReleaseStatus" name="ReleaseStatus" onchange="js_Reload_Table();" ', $Lang['General']['All'], $_REQUEST['ReleaseStatus']);

//$h_toolButton .= '&nbsp;|&nbsp;';
$h_toolButton .= '<br style="clear:both;" />';
$h_toolButton .= $Lang['iPortfolio']['SLP']['DynamicReport']['GeneratedDate']. ': '. $Lang['General']['From'] . $linterface->GET_DATE_PICKER("GenerateStartDate", $GenerateStartDate).'&nbsp;'.$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("GenerateEndDate", $GenerateEndDate);
$h_toolButton .= '&nbsp;'.$linterface->GET_SMALL_BTN($Lang['Btn']['Apply'],'button','js_Reload_Table();',"ApplyButton");

$filterAry['Keyword'] = trim($_REQUEST['Keyword']);
$filterAry['TemplateID'] = $_REQUEST['TemplateID'];
$filterAry['GenerateBy'] = $_REQUEST['GenerateBy'];
$filterAry['ReleaseStatus'] = $_REQUEST['ReleaseStatus'];
$filterAry['GenerateStartDate'] = $GenerateStartDate;
$filterAry['GenerateEndDate'] = $GenerateEndDate;

### Table Action Button
$ActionButtonArr = array();
$ActionButtonArr[] = array('assign_to_student', 'javascript:js_Release_Event(1);',$Lang['iPortfolio']['DynamicReport']['Release']);
$ActionButtonArr[] = array('unassign_to_student', 'javascript:js_Release_Event(0);',$Lang['iPortfolio']['DynamicReport']['Unrelease']);
$ActionButtonArr[] = array('other', 'javascript:js_Regenerate_Event();',$Lang['iPortfolio']['DynamicReport']['RegenerateReport']);
$ActionButtonArr[] = array('delete', 'javascript:js_Delete_Event();');
$h_actionButton = $linterface->Get_DBTable_Action_Button_IP25($ActionButtonArr);

$h_dbtable = $lpf->getReleaseStudentDynamicReportDBTable($filterAry, $field, $order, $pageNo, $numPerPage);

$ReturnMsg = $Lang['General']['ReturnMessage'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);
include_once("template/release_student.tmpl.php");
$linterface->LAYOUT_STOP();

?>