<?php
#using: 

include_once("$intranet_root/includes/portfolio25/dynReport/StudentReportTemplateManager.php");
include_once("$intranet_root/includes/portfolio25/dynReport/StudentReportDataSourceManager.php");
// json.php under includes cannot handle null values, so use Services_JSON.
include_once("$intranet_root/includes/portfolio25/dynReport/JSON.php");


$template_id = $_REQUEST['template_id'];
if ($template_id) {
	// Edit a template.
	$template_manager = new StudentReportTemplateManager();
	$template = $template_manager->load($template_id);
	if ($template === false) {
		echo 'Template does not exist.';
		exit();
	}
}
else {
	// Add a template.
	$template = new StudentReportTemplate();
}
$template_data = $template->get_data_as_array();
$template_inputdate = $template->get_inputdate(); // Henry Yuen (2010-07-26)
$template_is_default = $template->get_is_default();

if ($template_is_default) {
	$h_edit_default_template_warning = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['iPortfolio']['DynamicReport']['WarningArr']['EditDefaultTemplate']);
}

//debug_pr($template_data);

# Henry Yuen (2010-08-23): bug fix
if($template_id == ''){
	$template_inputdate = 'new'; // indicate this is a new template 	
}

$data_source_manager = new StudentReportDataSourceManager();
$json = new Services_JSON();
	
// Include the template, which is relative to the index page.
$linterface->LAYOUT_START();
include_once("template/edit.tmpl.php");
$linterface->LAYOUT_STOP();

?>
