<?php

include_once("$intranet_root/includes/portfolio25/dynReport/StudentReportDataSourceManager.php");
include_once("$intranet_root/includes/portfolio25/dynReport/StudentReportTemplateManager.php");

$template_manager = new StudentReportTemplateManager();
$template_list = $template_manager->get_list_of_all_report_templates($defaultOnly=false);
$default_template_list = $template_manager->get_list_of_all_report_templates($defaultOnly=true);

$h_navigation_default = $linterface->GET_NAVIGATION2_IP25($Lang['General']['Default']);
$h_navigation_custom = $linterface->GET_NAVIGATION2_IP25($Lang['General']['Custom']);

$h_defaultTemplateDivID = 'defaultTemplateDiv';
$h_ShowSectionSpanID = 'spanShowOption_'.$h_defaultTemplateDivID;
$h_HideSectionSpanID = 'spanHideOption_'.$h_defaultTemplateDivID;
$h_showHideDefault = '';
$h_showHideDefault .= '<span style="float:right;">'."\n";
	$h_showHideDefault .= '<span id="'.$h_ShowSectionSpanID.'" style="display:none;width:100%;float:right;">'."\n";
		$h_showHideDefault .= $linterface->Get_Show_Option_Link("javascript:js_Show_Option_Div('".$h_defaultTemplateDivID."');", "float:right;", $Lang['Btn']['ShowSection']);
	$h_showHideDefault .= '</span>'."\n";
	$h_showHideDefault .= '<span id="'.$h_HideSectionSpanID.'" style="width:100%;float:right;">'."\n";
		$h_showHideDefault .= $linterface->Get_Hide_Option_Link("javascript:js_Hide_Option_Div('".$h_defaultTemplateDivID."');", "float:right;", $Lang['Btn']['HideSection']);
	$h_showHideDefault .= '</span>'."\n";
$h_showHideDefault .= '</span>'."\n";

/**
 * Translate a data source list string, which is of the format:
 * 		data_source1, data_source2, ...
 */
function translate_data_source_list_string($data_source_list) {
	static $data_source_and_field_display_names = null;
	if (!isset($data_source_and_field_display_names)) {
		$data_source_manager = new StudentReportDataSourceManager();
		$data_source_and_field_display_names = $data_source_manager->get_data_source_and_field_display_names();
	}
	
	$data_source_list = explode(', ', $data_source_list);
	foreach ($data_source_list as &$data_source) {
		$data_source = $data_source_and_field_display_names[$data_source] != '' ?
			$data_source_and_field_display_names[$data_source] : $data_source;
	}
	return join(', ', $data_source_list);
}

// Include the template, which is relative to the index page.
$linterface->LAYOUT_START();
include_once("template/list.tmpl.php");
$linterface->LAYOUT_STOP();

?>
