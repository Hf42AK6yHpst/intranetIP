<?php

// Get the student list for a selected class during report printing, through Ajax.

include_once("$intranet_root/includes/libclass.php");

$targetClass = $_REQUEST['targetClass'];
$isMultiple = $_REQUEST['isMultiple'];

$libclass = new libclass();
// echo $libclass->getStudentSelectByClass($targetClass, 'name="targetStudent"', '', true,$type="", $typeID=-1, $recordStatus="1,2");

/*
$requestRecordStatus = '0,1,2';

if($sys_custom['iPf']['DynamicReport']['dontPrintSuspended']){
	// 0 is suspended
	$requestRecordStatus = '1,2';
}
*/
# consistent with SLP report, don't show suspended student
$requestRecordStatus = '1';


$attr = ' id="targetStudentSel" name="targetStudent[]" ';
$isAll = true;
if ($isMultiple) {
	$attr .= ' multiple size="10" ';
	$isAll = false;
}

// 2013-0805-0935-09073
$activatedPortfolioStudentAry = $lpf->GET_STUDENT_LIST_DATA($targetClass, $ParActivated=true, $ParSearchName="", $ParYearID="", $ParField=0, $ParOrder=1, $ParClassNameLang='en');
$activatedPortfolioStudentIdAry = Get_Array_By_Key($activatedPortfolioStudentAry, 'UserID');

$selection = $libclass->getStudentSelectByClass($targetClass, $attr, '', $isAll, $type="", $typeID=-1, $requestRecordStatus, $activatedPortfolioStudentIdAry);

if ($isMultiple) {
	$selectAllBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('targetStudentSel', 1);");
	$returnString = $linterface->Get_MultipleSelection_And_SelectAll_Div($selection, $selectAllBtn);
}
else {
	$returnString = $selection;
}

echo $returnString;
?>