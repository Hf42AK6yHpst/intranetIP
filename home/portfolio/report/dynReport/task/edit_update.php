<?php
#using: Bill Mak

include_once("$intranet_root/includes/portfolio25/dynReport/StudentReportTemplateManager.php");
// json.php under includes cannot handle null values, so use Services_JSON.
include_once("$intranet_root/includes/portfolio25/dynReport/JSON.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");


$request_data = stripslashes_deep_if_magic_quotes_is_on($_REQUEST);
$template_id = $request_data['template_id'];
$parent_template_id = $request_data['parent_template_id'];
$template_data = $request_data['template_data'];
$json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
$template_manager = new StudentReportTemplateManager();

function update_image_folder($matches) {
	//uses for preg_replace_callback()
	global $parent_template_id, $template_id;
	return str_replace("DR_$parent_template_id","DR_$template_id",$matches[0]);
}

# Begin Henry Yuen (2010-04-22): support attaching image in header
# new logic in handling edit_update because header image require additional steps
# 		new template : create -> modify image path -> update
# 		old template : modify image path -> update


if (!$template_id) 
{
	if(!$parent_template_id) {
		$template_data = $json->decode($template_data);
		$result = $template_manager->add_by_form_data($template_data);
		$template_id = $result;
		$xmsg = $result ? 'add' : 'add_failed';
	} else {
		##Copy template
		#
		#retrieve an id first#
		$template_empty = $json->decode("{\"template_title\":\"empty\"}");
		$result = $template_manager->add_by_form_data($template_empty);
		$template_id = $result;
		$xmsg = $result ? 'add' : 'add_failed';
		######################
		if(strcmp($xmsg,"add")==0) {
			$template_data = preg_replace_callback('/<img[^>]+>/', 'update_image_folder', $template_data);
			$template_data = $json->decode($template_data);
			$result = $template_manager->update_by_form_data($template_id, $template_data);
			$xmsg = $result ? 'add' : 'add_failed';
			#####################copy entire image folder from parent#
			##########################################################
			$lfs = new libfilesystem();
			#define the mum and son image storage path
			$son_image_path = $PATH_WRT_ROOT.($lfs->returnFlashImageInsertPath($cfg['fck_image']['DynamicReport'], $template_id));
			$mum_image_path = $PATH_WRT_ROOT.($lfs->returnFlashImageInsertPath($cfg['fck_image']['DynamicReport'], $parent_template_id));
			#create the son's folder
			if(file_exists($mum_image_path)){
				if(mkdir($son_image_path,0755))  {
					$mum_dir = opendir($mum_image_path);
					#read in mum's subfolders' name and create the same in son's
					while(false !== ( $mum_subdir_name = readdir($mum_dir))) {
						if($mum_subdir_name !="." && $mum_subdir_name != ".." ) {
							$son_subdir = $son_image_path.'/'.$mum_subdir_name;
							mkdir($son_subdir, 0755);
							$mum_subdir = $mum_image_path.'/'.$mum_subdir_name;
							$handling_dir = opendir($mum_subdir);
							#read in mum's images stored in subfolder and create the same in son's
							while(false !== ($mum_subdir_files = readdir($handling_dir))) {
								if($mum_subdir_files !="." && $mum_subdir_files != ".." ){
									copy($mum_subdir.'/'.$mum_subdir_files,$son_subdir.'/'.$mum_subdir_files);
									chmod($son_subdir.'/'.$mum_subdir_files,0755);
								}
							}
							closedir($handling_dir);
						}
					}
					closedir($mum_dir);
				}
			}
			###########################################################
			###########################################################
		}
	}	
}
else {
	$template_data = $json->decode($template_data);
	$result = $template_manager->update_by_form_data($template_id, $template_data);
	$xmsg = $result ? 'update' : 'update_failed';
}

$isNew = !$template_id;

if ($isNew) {
	$result = $template_manager->add_by_form_data($template_data);
	$template_id = $result;	
}
$lf = new libfilesystem();
$template_data['header_template'] = ($lf->copy_fck_flash_image_upload($template_id, $template_data['header_template'], $PATH_WRT_ROOT, $cfg['fck_image']['DynamicReport']));
$template_data['footer_template'] = ($lf->copy_fck_flash_image_upload($template_id, $template_data['footer_template'], $PATH_WRT_ROOT, $cfg['fck_image']['DynamicReport']));

# TCPDF cannot recognize style=width: xxx; height: xxx
$template_data['header_template'] = preg_replace('/style="width: (\d+)px; height: (\d+)px;?"/', 'width="$1" height="$2"', $template_data['header_template']);
$template_data['footer_template'] = preg_replace('/style="width: (\d+)px; height: (\d+)px;?"/', 'width="$1" height="$2"', $template_data['footer_template']);

$result = $template_manager->update_by_form_data($template_id, $template_data); 		
	
if($isNew){
	$xmsg = $result ? 'add' : 'add_failed';
}	
else{
	$xmsg = $result ? 'update' : 'update_failed';
}
# End Henry Yuen (2010-04-22): support attaching image in header
header("Location: index.php?task=edit&template_id=$template_id&xmsg=$xmsg");

?>
