<?php
// Editing by 
################# Change Log [Start] #####
#
#	Date	:	2015-11-04	Omas
# 				added setting for teacher comment
#
################## Change Log [End] ######
// Show a form for selecting class and template for report printing.

include_once("$intranet_root/includes/libclass.php");
include_once("$intranet_root/includes/form_class_manage.php");
include_once("$intranet_root/includes/portfolio25/dynReport/StudentReportTemplateManager.php");

$libclass = new libclass();
$template_manager = new StudentReportTemplateManager();

# Generate multiple year checkboxes
$libacademic_year = new academic_year();
$ay_arr = $libacademic_year->Get_All_Year_List();

$academicYearAssoArr = array();
//$ay_selection_html = "<table width=\"450\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" style=\"background: #EEEEEE\">";
$ay_selection_html = "<table width=\"450\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
$ay_selection_html .= "<tr><td style=\"border:0px;\"><input type=\"checkbox\" id=\"AYCheckMaster\" /> <label for=\"AYCheckMaster\">".$button_check_all."</label></td><td style=\"border:0px;\">&nbsp;</td><td style=\"border:0px;\">&nbsp;</td></tr>";
for($i=0; $i<ceil(count($ay_arr)/3)*3; $i++)
{
	$thisAcademicYearID = $ay_arr[$i][0];
	$thisAcademicYearName = $ay_arr[$i][1];
	
	# for teacher comment ay selection		
	if($thisAcademicYearName!='' && $thisAcademicYearID !=''){
	$academicYearAssoArr[$thisAcademicYearID] = $thisAcademicYearName;
	}
	//$thisChecked = ($thisAcademicYearID == Get_Current_Academic_Year_ID())? 'checked' : '';
	
	if($i % 3 == 0) {
		$ay_selection_html .= "<tr>";
	}
	  
	if(isset($ay_arr[$i])){
		$ay_selection_html .= "<td width=\"150\" style=\"border:0px;\"><input type=\"checkbox\" name=\"AcademicYearIDs[]\" id=\"ay_".$thisAcademicYearID."\" value=\"".$thisAcademicYearID."\" ".$thisChecked." /> <label for=\"ay_".$thisAcademicYearID."\">".$thisAcademicYearName."</label></td>";
	}
	else {
		$ay_selection_html .= "<td width=\"150\" style=\"border:0px;\">&nbsp;</td>";
	}
	  
	if($i % 3 == 2) {
		$ay_selection_html .= "</tr>";
	}
}
$ay_selection_html .= "</table>";

### Navigation
$h_GeneralNavigation = $linterface->GET_NAVIGATION2_IP25($Lang['iPortfolio']['GeneralSettings']);
$h_AdvancedNavigation = $linterface->GET_NAVIGATION2_IP25($Lang['iPortfolio']['AdvancedSettings']);


### Sub-Title
$h_DataContentDisplaySubTitle = $linterface->Get_Form_Separate_Title($Lang['iPortfolio']['DynamicReport']['DataContentDisplay']);
$h_RecordSubTitle = $linterface->Get_Form_Separate_Title($Lang['General']['Record']);
$h_AcademicResultSubTitle = $linterface->Get_Form_Separate_Title($Lang['iPortfolio']['AcademicResult']);
$h_IssuesDateTitle = $linterface->Get_Form_Separate_Title($Lang['iPortfolio']['DynamicReport']['IssueDate']);


### Show Hide Advanced Settings
$h_ShowHideAdvSettingsSpan = '';
$h_ShowHideAdvSettingsSpan .= '<span>'."\n";
	$h_ShowHideAdvSettingsSpan .= '<span id="spanShowOption_AdvSettingsDiv">'."\n";
		$h_ShowHideAdvSettingsSpan .= $linterface->Get_Show_Option_Link("javascript:js_Show_Option_Div('AdvSettingsDiv');", "", $Lang['Btn']['Show']);
	$h_ShowHideAdvSettingsSpan .= '</span>'."\n";
	$h_ShowHideAdvSettingsSpan .= '<span id="spanHideOption_AdvSettingsDiv" style="display:none;">'."\n";
		$h_ShowHideAdvSettingsSpan .= $linterface->Get_Hide_Option_Link("javascript:js_Hide_Option_Div('AdvSettingsDiv');", "", $Lang['Btn']['Hide']);
	$h_ShowHideAdvSettingsSpan .= '</span>'."\n";
$h_ShowHideAdvSettingsSpan .= '</span>'."\n";

### teacher comments selection 
$comments_ay_select = getSelectByAssoArray($academicYearAssoArr, 'name="commentsAY" id="commentsAY" onChange="jLOAD_COMMENTS_SEMESTER();"', Get_Current_Academic_Year_ID(), 0, 1, "", 1);

// Include the template, which is relative to the index page.
$linterface->LAYOUT_START();
include_once("template/report.tmpl.php");
$linterface->LAYOUT_STOP();

?>
