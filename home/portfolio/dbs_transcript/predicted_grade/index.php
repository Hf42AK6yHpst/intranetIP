<?php
// Using: Bill

############# Change Log [Start] ################
#
#   Date:   2018-09-17 (Bill)   [2017-1207-0956-53277]
#           Create file
# 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript_ui.php");

iportfolio_auth("T");
intranet_opendb();

$linterface = new interface_html();

$fcm = new form_class_manage();
$scm = new subject_class_mapping();
$scm_ui = new subject_class_mapping_ui();
$subject = new subject();

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();
$lpf_dbs_ui = new libpf_dbs_transcript_ui();

// Check User Role
$isPortfolioSuperAdmin = $lpf->IS_IPF_SUPERADMIN();
$isSubjectTeacher = !$isPortfolioSuperAdmin && $lpf->isSubjectTeacher();
$isClassTeacher = !$isPortfolioSuperAdmin && $lpf->IS_CLASS_TEACHER();

// Get Curriculum
$allCurriculumAry = $lpf_dbs->getCurriculumSetting('', '', '', '', $ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"]);
$allCurriculumAry = BuildMultiKeyAssoc($allCurriculumAry, 'CurriculumID');
$defaultCurriculumID = reset($allCurriculumAry);
$defaultCurriculumID = $defaultCurriculumID['CurriculumID'];

$curriculumID = $_POST['curriculumID']? $_POST['curriculumID'] : $_GET['curriculumID'];
$curriculumID = $curriculumID? $curriculumID : 2;

// Check Curriculum Type
$targetCurriculum = $allCurriculumAry[$curriculumID];
$isIBTranscriptReport = $targetCurriculum['Code'] == 'IB' || $targetCurriculum['Code'] == 'IBDP';

// Curriculum Selection
$html["curriculumSelection"] = $lpf_dbs_ui->getCurriculumSelection('curriculumID', $selected=$curriculumID, $onchange='document.form1.submit();', $noFirst=1, $firstTitleText='', $useCode=true, $forPredictedGrade=true);

// Get Curriculum Form
$ClassLvlArr = $lpf_dbs->getCurriculumFormClassList($curriculumID, "", $groupByYear=true);
if(!empty($ClassLvlArr))
{
    $formID = $_POST['formID']? $_POST['formID'] : $_GET['formID'];
    $availableYearIDArr = Get_Array_By_Key($ClassLvlArr, 'YearID');
    if(!in_array($formID, (array)$availableYearIDArr)) {
        $formID = '';
    }
    
    $defaultYearID = reset($ClassLvlArr);
    $defaultYearID = $defaultYearID['YearID'];
    $formID = $formID? $formID : $defaultYearID;
    
    // Form Selection
    $temp = '<select name="formID" id="formID" class="formtextbox" onChange="document.form1.submit();">';
    for ($i=0; $i<sizeof($ClassLvlArr); $i++) {
        $temp .= '<option value="'.$ClassLvlArr[$i][1].'"';
        if($formID == $ClassLvlArr[$i][1]) {
            $temp .= ' selected';
        }
        $temp .= '>'.$ClassLvlArr[$i][0].'</option>'."\n";
    }
    $temp .= '</select>'."\n";
    $html["formSelection"] = $temp;
}

if($formID > 0)
{
    // Get Year Term
    $currentTermIDAry = getSemesters(Get_Current_Academic_Year_ID());
    $currentTermIDAry = array_keys($currentTermIDAry);
    
    // Get Teaching Class
    $allClassStudentIDAry = array();
    if($isClassTeacher)
    {
        $allClassAry = $fcm->Get_Class_Teacher_Class($_SESSION['UserID']);
        $allRelatedClassIDAry = Get_Array_By_Key($allClassAry, 'ClassID');
        
        // Get Class Student
        $allClassStudentIDAry = $fcm->Get_Student_By_Class($allRelatedClassIDAry);
        $allClassStudentIDAry = Get_Array_By_Key($allClassStudentIDAry, 'UserID');
    }
    
    // Get Teaching Subject Group
    $targetTeacherID = $isPortfolioSuperAdmin? '' : $_SESSION['UserID'];
    $allSubjectGroupAry = $subject->Get_Subject_Group_List($currentTermIDAry, $formID, '', $targetTeacherID, $returnAsso=0, $YearClassID='', $SubjectID='', $InYearClassOnly=false);
    
    // Get Subject Group Student
    $SubjectGroupIDArr = Get_Array_By_Key($allSubjectGroupAry, 'SubjectGroupID');
    $allSubjectGroupStudentIDArr = $scm->Get_Subject_Group_Student_List($SubjectGroupIDArr);
    $allSubjectGroupStudentIDArr = Get_Array_By_Key($allSubjectGroupStudentIDArr, 'UserID');
    
    // Get related Class
    if(!$isPortfolioSuperAdmin)
    {
        $allRelatedStudentIDAry = array_values(array_unique(array_merge($allClassStudentIDAry, $allSubjectGroupStudentIDArr)));
        $allRelatedClassList = $lpf_dbs->getClassListFromStudent($allRelatedStudentIDAry);
        $allRelatedClassIDList = Get_Array_By_Key($allRelatedClassList, 'YearClassID');
    }
    
    // Get Curriculum Class
    $ClassArr = $lpf_dbs->getCurriculumFormClassList($curriculumID, $formID, $groupByYear=false);
    if(!empty($ClassArr))
    {
        $classID = $_POST['classID']? $_POST['classID'] : $_GET['classID'];
        $availableClassIDArr = Get_Array_By_Key($ClassArr, 'YearClassID');
        if(!in_array($classID, (array)$availableClassIDArr) || (!$isPortfolioSuperAdmin && !in_array($classID, (array)$allRelatedClassIDList))) {
            $classID = '';
        }
        
        $defaultClassID = reset($availableClassIDArr);
        if(!$isPortfolioSuperAdmin)
        {
            $defaultClassID = '';
            foreach((array)$allRelatedClassIDList as $thisRelatedClassID) {
                if(in_array($thisRelatedClassID, (array)$availableClassIDArr)) {
                    $defaultClassID = $thisRelatedClassID;
                    break;
                }
            }
        }
        $classID = $classID ? $classID : $defaultClassID;
    }
    else {
        $classID = '';
    }
    
    // Class Selection
    if(!empty($ClassArr))
    {
        $temp = '<select name="classID" id="classID" class="formtextbox" onChange="document.form1.submit();">';
        for ($i=0; $i<sizeof($ClassArr); $i++) {
            if(!$isPortfolioSuperAdmin && !in_array($ClassArr[$i][3], (array)$allRelatedClassIDList)) {
                continue;
            }
            
            $temp .= '<option value="'.$ClassArr[$i][3].'"';
            if($classID == $ClassArr[$i][3]) {
                $temp .= ' selected';
            }
            $temp .= '>'.$ClassArr[$i][2].'</option>'."\n";
        }
        $temp .= '</select>'."\n";
        $html["classSelection"] = $temp;
    }
    
    $allClassStudentIDAry = array();
    if($isClassTeacher)
    {
        $allClassAry = $fcm->Get_Class_Teacher_Class($_SESSION['UserID']);
        $allRelatedClassIDAry = Get_Array_By_Key($allClassAry, 'ClassID');
        
        // Get Class Student
        $allClassStudentIDAry = $fcm->Get_Student_By_Class($allRelatedClassIDAry);
        $allClassStudentIDAry = Get_Array_By_Key($allClassStudentIDAry, 'UserID');
    }
    
    if($classID > 0)
    {
        // Get Current Class Subject Group
        $targetTeacherID = ($isPortfolioSuperAdmin || ($isClassTeacher && in_array($classID, $allRelatedClassIDAry)))? '' : $_SESSION['UserID'];
        $allSubjectGroupAry = $subject->Get_Subject_Group_List($currentTermIDAry, $formID, '', $targetTeacherID, $returnAsso=0, $classID, $SubjectID='', $InYearClassOnly=false);
        $allRelatedSubjectIDAry = Get_Array_By_Key($allSubjectGroupAry, 'RecordID');
        
        $subjectID = $_POST['subjectID']? $_POST['subjectID'] : $_GET['subjectID'];
        $subjectID = $subjectID? $subjectID : $allRelatedSubjectIDAry[0];
        if(!in_array($subjectID, (array)$allRelatedSubjectIDAry)) {
            $subjectID = '';
        }
        
        $defaultSubjectID = reset($allRelatedSubjectIDAry);
        $subjectID = $subjectID? $subjectID : $defaultSubjectID;
        
        // Get Subject Selection
        $html["subjectSelection"] = $scm_ui->Get_Subject_Selection('subjectID', $selected=$subjectID, $onchange='document.form1.submit();', $noFirst=1, $firstTitle='', '', $onfocus='', $filterSubjectWithoutSG=1, $isMultiple=0, $allRelatedSubjectIDAry, $excludeSubjectIDArr=array());
        
        if($subjectID > 0)
        {
            // Get Target Subect Group
            $allSubjectGroupAry = $subject->Get_Subject_Group_List($currentTermIDAry, $formID, '', $targetTeacherID, $returnAsso=0, $classID, $subjectID, $InYearClassOnly=true);
            $allSubjectGroupIDArr = Get_Array_By_Key($allSubjectGroupAry, 'SubjectGroupID');
            
            // Get Target Subject Group Student
            $allSubjectGroupStudentIDAry = $scm->Get_Subject_Group_Student_List($allSubjectGroupIDArr);
            $allSubjectGroupStudentIDAry = Get_Array_By_Key($allSubjectGroupStudentIDAry, 'UserID');
            
            $studentCurriculumSettingAry = $lpf_dbs->getStudentCurriculumSettings($allSubjectGroupStudentIDAry);
            if(!empty($studentCurriculumSettingAry))
            {
                // Get Subject Group Student with target Curriculum
                $studentCurriculumSettingAry = BuildMultiKeyAssoc($studentCurriculumSettingAry, array('CurriculumID', 'StudentID'), 'StudentID', 1, 0);
                $studentWithTargetCurriculumAry = $studentCurriculumSettingAry[$curriculumID];
                
                // Get Subject Group Student without any Curriculum Settings (mainly for new Class Students)
                $studentWithoutCurriculumSettingAry = $lpf_dbs->getStudentWithoutCurriculumSettings($allSubjectGroupStudentIDAry, $curriculumID);
                $studentWithoutCurriculumSettingAry = Get_Array_By_Key($studentWithoutCurriculumSettingAry, 'UserID');
                $allSubjectGroupStudentIDAry = array_merge((array)$studentWithTargetCurriculumAry, (array)$studentWithoutCurriculumSettingAry);
            }
            
            // Get Target Subject Group Teacher
            if(!$isPortfolioSuperAdmin && $isSubjectTeacher)
            {
                $allSubjectGroupTeacherIDAry = $scm->Get_Subject_Group_Teacher_ListArr($allSubjectGroupIDArr);
                foreach($allSubjectGroupTeacherIDAry as $thisSubjectGroupTeacherIDAry) {
                    foreach($thisSubjectGroupTeacherIDAry as $thisSubjectTeacher)
                    {
                        // Check if current subject teacher
                        if($_SESSION['UserID'] == $thisSubjectTeacher['UserID']) {
                            $isCurrentSubjectTeacher = true;
                            break;
                        }
                    }
                }
            }
        }
    }
}

$isDisplayTable = count($allSubjectGroupStudentIDAry) > 0;
$isViewOnly = $isDisplayTable && !$isPortfolioSuperAdmin && !($isSubjectTeacher && $isCurrentSubjectTeacher);

# Get Edit Period (Subject Teacher)
if($isSubjectTeacher)
{
    $currentDateTime = strtotime(date('Y-m-d 00:00:00'));
    
    $predictPeriod1 = $lpf_dbs->getAdjustmentPeriodSettings('', 2);
    $predictPeriod1 = $predictPeriod1[0];
    $isAllowEditPeriod1 = $predictPeriod1['PeriodStart'] != '' && $predictPeriod1['PeriodEnd'] != '' &&
                            $currentDateTime >= strtotime($predictPeriod1['PeriodStart']) && $currentDateTime <= strtotime($predictPeriod1['PeriodEnd']);
    if($isAllowEditPeriod1) {
        $html["adjustmentStartDate"] = substr($predictPeriod1['PeriodStart'], 0, 10);
        $html["adjustmentEndDate"] = substr($predictPeriod1['PeriodEnd'], 0, 10);
    }
    
    $predictPeriod2 = $lpf_dbs->getAdjustmentPeriodSettings('', 3);
    $predictPeriod2 = $predictPeriod2[0];
    $isAllowEditPeriod2 = $predictPeriod2['PeriodStart'] != '' && $predictPeriod2['PeriodEnd'] != '' &&
                            $currentDateTime >= strtotime($predictPeriod2['PeriodStart']) && $currentDateTime <= strtotime($predictPeriod2['PeriodEnd']);
    if($isAllowEditPeriod2) {
        $html["adjustmentStartDate"] = substr($predictPeriod2['PeriodStart'], 0, 10);
        $html["adjustmentEndDate"] = substr($predictPeriod2['PeriodEnd'], 0, 10);
    }
}

// Table settings
$field = isset($_POST["field"])? $_POST["field"] : $_GET["field"];
$order = isset($_POST["order"])? $_POST["order"] : (isset($_GET["order"])? $_GET["order"] : 1);
$pageNo = isset($_POST["pageNo"])? $_POST["pageNo"] : $_GET["pageNo"];
$page_size_change = isset($_POST["page_size_change"])? $_POST["page_size_change"] : $_GET["page_size_change"];
$numPerPage = !empty($_POST["numPerPage"])? $_POST["numPerPage"] : (!empty($_GET["numPerPage"])? $_GET["numPerPage"] : $page_size);

// Table setups
$pfTable = new libpf_dbtable($field, $order, $pageNo);
$pfTable->page_size = $numPerPage;

// Table headers
$columnCount = 0;

if($isIBTranscriptReport)
{
    $pfTable->column_list .= "<th width='2' nowrap>#</th>\n";
    $pfTable->column_list .= "<th width='14%' nowrap>".$pfTable->column($columnCount++, $i_general_class)."</th>\n";
    $pfTable->column_list .= "<th width='10%'>".$pfTable->column($columnCount++, $i_ClassNumber)."</th>\n";
    $pfTable->column_list .= "<th width='26%'>".$pfTable->column($columnCount++, $i_UserStudentName)."</th>\n";
    $pfTable->column_list .= "<th width='16%'>".$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['MinGrade']."</th>\n";
    $pfTable->column_list .= "<th width='16%'>".$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['MaxGrade']."</th>\n";
    $pfTable->column_list .= "<th width='16%'>".$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Title']."</th>\n";
    
    $pfTable->field_array = array("ClassName", "ClassNumber", "StudentName", "PredictedGradeMin", "PredictedGradeMax", "InputFields");
    $pfTable->column_array = array(0,0,0,0,0,0,0);
    
    $pfTable->IsColOff = "iPortfolio_DBS_Predicted_Grade_IB";
}
else
{
    $pfTable->column_list .= "<th width='2' nowrap>#</th>\n";
    $pfTable->column_list .= "<th width='14%' nowrap>".$pfTable->column($columnCount++, $i_general_class)."</th>\n";
    $pfTable->column_list .= "<th width='10%'>".$pfTable->column($columnCount++, $i_ClassNumber)."</th>\n";
    $pfTable->column_list .= "<th width='26%'>".$pfTable->column($columnCount++, $i_UserStudentName)."</th>\n";
    $pfTable->column_list .= "<th width='12%'>".$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['OriginalGrade']."</th>\n";
    $pfTable->column_list .= "<th width='12%'>".$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Amendment1']."</th>\n";
    $pfTable->column_list .= "<th width='12%'>".$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Amendment2']."</th>\n";
    $pfTable->column_list .= "<th width='12%'>".$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['FinalGrade']."</th>\n";
    
    $pfTable->field_array = array("ClassName", "ClassNumber", "StudentName", "PredictedGradeOriginal", "PredictedGradeInput1", "PredictedGradeInput2", "PredictedGradeFinal");
    $pfTable->column_array = array(0,0,0,0,0,0,0,0);
    
    $pfTable->IsColOff = "iPortfolio_DBS_Predicted_Grade";
}

// Table controls
// $pfTable->no_col = 1/* row number */+count($pfTable->field_array)+1/* checkbox */;
$pfTable->no_col = count($pfTable->field_array) + 1;
$pfTable->fieldorder2 = " , iu.ClassName, iu.ClassNumber+0 ";

// Table SQL
if($isIBTranscriptReport && $subjectID > 0) {
    $pfTable->sql = $lpf_dbs->getStudentAdjustMinMax('', $allSubjectGroupStudentIDAry, $formID, $subjectID, $subjectGroupID, $isPortfolioSuperAdmin, $returnSQL=true, $isViewOnly);
    $lastModifiedAdjust = $lpf_dbs->getStudentAdjustMinMaxLastModify($allSubjectGroupStudentIDAry, $formID, $subjectID, $subjectGroupID, $isPortfolioSuperAdmin);
} else if(!$isIBTranscriptReport && $subjectID > 0) {
    //$pfTable->sql = $lpf_dbs->getStudentAdjustment('', '', -1, -1, $isPortfolioSuperAdmin, $predictedPhase='', $predictedType='', $returnSQL=true);
    $pfTable->sql = $lpf_dbs->getStudentAdjustment('', $allSubjectGroupStudentIDAry, $formID, $subjectID, $subjectGroupID, '', '', $isPortfolioSuperAdmin, $returnSQL=true, $isViewOnly);
    $lastModifiedAdjust = $lpf_dbs->getStudentAdjustmentLastModify($allSubjectGroupStudentIDAry, $formID, $subjectID, $subjectGroupID, $isPortfolioSuperAdmin);
}

// Last Modified Info
$lastModifiedDisplay = '';
if(!empty($lastModifiedAdjust) && $lastModifiedAdjust['ModifiedBy'] != '' && $lastModifiedAdjust['DateModified'] != '')
{
    // Modified User
    include_once($PATH_WRT_ROOT."includes/libuser.php");
    $modifiedByUser = new libuser($lastModifiedAdjust['ModifiedBy']);
    $modifiedByUserName = Get_Lang_Selection($modifiedByUser->ChineseName, $modifiedByUser->EnglishName);
    
    // Modified DateTime
    $modifiedDate = $lastModifiedAdjust['DateModified'];
    
    $lastModifiedDisplay = str_replace('<!--DaysAgo-->', $modifiedDate, $Lang['General']['LastModifiedInfoRemark']);
    $lastModifiedDisplay = str_replace('<!--LastModifiedBy-->', $modifiedByUserName, $lastModifiedDisplay);
}

// Get table
if($isDisplayTable)
{
$displayTableHtml = $pfTable->display();
$tableHiddenFields = <<<HTMLEND
<input type="hidden" name="pageNo" value="{$pfTable->pageNo}">
<input type="hidden" name="order" value="{$pfTable->order}">
<input type="hidden" name="field" value="{$pfTable->field}">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="{$pfTable->page_size}">
HTMLEND;
$displayTableHtml .= $tableHiddenFields;
}
###############################################

###############################################
###	HTML - display table
$html["display_table_fields"] = $displayTableHtml;

# Button
$htmlAry["EditButton"] = "";
$htmlAry["EditButton"] .= $linterface->GET_ACTION_BTN($button_submit, "button", "goSubmit()", "submitBtn");
$htmlAry["EditButton"] .= "&nbsp;&nbsp;";
$htmlAry["EditButton"] .= $linterface->GET_ACTION_BTN($button_back, "button", "goBack()", "backBtn");
$htmlAry["ViewButton"] = "";
$htmlAry["ViewButton"] .= $linterface->GET_ACTION_BTN($button_edit, "button", "goEdit()", "editBtn");

### Page ###
$CurrentPage = "Teacher_Transcript_PredictedGrade";

### Title ###
$TAGS_OBJ = $lpf_dbs_ui->getPredictedGradeTab('index', !$isPortfolioSuperAdmin);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

include_once("../templates/predicted_grade.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>