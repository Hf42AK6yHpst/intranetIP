<?php
// Using: Bill

############# Change Log [Start] ################
#
#   Date:   2018-08-29 (Bill)   [2017-1207-0956-53277]
#           Create file
# 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript_ui.php");

iportfolio_auth("T");
intranet_opendb();

$linterface = new interface_html();

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();
$lpf_dbs_ui = new libpf_dbs_transcript_ui();

# Get Period Settings
$predictPeriod1 = $lpf_dbs->getAdjustmentPeriodSettings('', 2);
$predictPeriod1 = $predictPeriod1[0];
$predictPeriod2 = $lpf_dbs->getAdjustmentPeriodSettings('', 3);
$predictPeriod2 = $predictPeriod2[0];

# Build Subject table
$x = "	<table class='common_table_list_v30 edit_table_list_v30' cellspacing='0' cellpadding='5'>
			<tr>
				<th>".$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['AdjustmentPeriodSetting'] ."</th>
				<th>".$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['AdjustmentStartDate'] ."</th>
				<th>".$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['AdjustmentEndDate'] ."</th>
			</tr>
            <tr>
        		<td>
        			<span>".$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Amendment1']."</span>
        		</td>
            	<td>".
            	    $linterface->GET_DATE_PICKER("start_date[2]", substr($predictPeriod1['PeriodStart'], 0, 10), "", "yy-mm-dd", "", "", "", "", "start_date_1", 0, 1)."
    			</td>
    		    <td>".
    		        $linterface->GET_DATE_PICKER("end_date[2]", substr($predictPeriod1['PeriodEnd'], 0, 10), "", "yy-mm-dd", "", "", "", "", "end_date_1", 0, 1)."
                </td>
    	 	</tr>
            <tr>
        		<td>
        			<span>".$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Amendment2']."</span>
        		</td>
            	<td>".
            	    $linterface->GET_DATE_PICKER("start_date[3]", substr($predictPeriod2['PeriodStart'], 0, 10), "", "yy-mm-dd", "", "", "", "", "start_date_2", 0, 1)."
    			</td>
    		    <td>".
    		        $linterface->GET_DATE_PICKER("end_date[3]", substr($predictPeriod2['PeriodEnd'], 0, 10), "", "yy-mm-dd", "", "", "", "", "end_date_2", 0, 1)."
                </td>
    	 	</tr>
        </table>";
$htmlAry["PeriodTable"] = $x;

### Page ###
$CurrentPage = "Teacher_Transcript_PredictedGrade";

### Title ###
$TAGS_OBJ = $lpf_dbs_ui->getPredictedGradeTab('period');
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

include_once("../templates/predicted_grade_period.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>