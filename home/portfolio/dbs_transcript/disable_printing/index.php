<?php
// Modified by
/*
 * Modification Log:
 *  Date: 2018-08-16 (Bill)
 *      - Create file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");

iportfolio_auth("T");
intranet_opendb();

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();
$lfcm_ui = new form_class_manage_ui();

// Table settings
$field = isset($_POST["field"])? $_POST["field"] : $_GET["field"];
$order = isset($_POST["order"])? $_POST["order"] : (isset($_GET["order"])? $_GET["order"] : 1);
$pageNo = isset($_POST["pageNo"])? $_POST["pageNo"] : $_GET["pageNo"];
$page_size_change = isset($_POST["page_size_change"])? $_POST["page_size_change"] : $_GET["page_size_change"];
$numPerPage = !empty($_POST["numPerPage"])? $_POST["numPerPage"] : (!empty($_GET["numPerPage"])? $_GET["numPerPage"] : $page_size);

// filter
$classId = isset($_POST['classId'])? $_POST['classId'] : $_GET['classId'];
$status = isset($_POST['status'])? $_POST['status'] : $_GET['status'];

$statusArr = array();
$statusArr[] = array($ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Inactive"], $Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Unlocked']);
$statusArr[] = array($ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"], $Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Locked']);

# Selection
$html["selection"] = $lfcm_ui->Get_Class_Selection(Get_Current_Academic_Year_ID(), '', 'classId', $classId, 'document.form1.submit()', 0, 0, 0, 0, Get_Selection_First_Title($Lang['General']['All']));
$html["selection"] .= "&nbsp;";
$html["selection"] .= getSelectByArray($statusArr, ' id="status" name="status" onchange="document.form1.submit()" ', $status, $isAll=0, $noFirst=0, Get_Selection_First_Title($Lang['General']['All']));

// Table setups
$pfTable = new libpf_dbtable($field, $order, $pageNo);
$pfTable->page_size = $numPerPage;

// Table headers
$columnCount = 0;
$pfTable->column_list .= "<th width='1'>#</th>\n";
$pfTable->column_list .= "<th nowrap>".$pfTable->column($columnCount++, $i_general_class)."</th>\n";
$pfTable->column_list .= "<th>".$pfTable->column($columnCount++, $Lang['Identity']['Student'])."</th>\n";
$pfTable->column_list .= "<th>".$pfTable->column($columnCount++, $Lang['General']['Reason'])."</th>\n";
$pfTable->column_list .= "<th nowrap>".$pfTable->column($columnCount++, $i_RecordDate)."</th>\n";
$pfTable->column_list .= "<th>".$pfTable->column($columnCount++, $Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['IssuedBy'])."</th>\n";
$pfTable->column_list .= "<th>".$pfTable->column($columnCount++, $Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Status'])."</th>\n";
$pfTable->column_list .= "<th width='1'>".$pfTable->check("recordIds[]")."</th>\n";

// Table controls
$pfTable->IsColOff = "IP25_table";
$pfTable->field_array = array("ClassName", "StudentName", "Reason", "DateInput", "InputName", "StatusDisplay");
$pfTable->no_col = 1/* row number */+count($pfTable->field_array)+1/* checkbox */;
$pfTable->column_array = array(0,0,0,0,0);

$pfTable->sql = $lpf_dbs->getDisabledStudentList($recordID='', $classId, $status, $search_text, $returnSQL=true);

// Get table
$displayTableHtml = $pfTable->display();
$table_hidden = <<<HTMLEND
<input type="hidden" name="pageNo" value="{$pfTable->pageNo}">
<input type="hidden" name="order" value="{$pfTable->order}">
<input type="hidden" name="field" value="{$pfTable->field}">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="{$pfTable->page_size}">
<input type="hidden" name="isNew" value="0">
HTMLEND;
$displayTableHtml .= $table_hidden;
###############################################

###############################################
###	HTML - display table
$html["display_table"] = $displayTableHtml;

### Page ###
$linterface = new interface_html();
$CurrentPage = "Teacher_Transcript_DisableTranscriptPrinting";
$CurrentPageName = $Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Title'];

### Title ###
$TAGS_OBJ[] = array($Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Title'], "");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

######## Display #########
// $PAGE_NAVIGATION[] = array($CurrentPageName);
// $html["navigation"] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);

include_once("../templates/disable_printing_list.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>