<?php
// Modifying By:
/*
 * Modification Log:
 *  Date: 2018-08-16 (Bill)
 *      - Create file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");

iportfolio_auth("T");
intranet_opendb();

$dataAry = array();
if(!$_POST['RecordID']) {
    if($_POST['target_type'] == 1)
    {
        $dataAry['StudentID'] = $_POST['TargetID'];
        
        if(count($_POST['studentTargetID']) == 1) {
            $classId = $_POST['studentTargetID'][0];
        }
    }
    else
    {
        $dataAry['StudentID'] = $_POST['TargetID_Year'];
    }
} else {
    $classId = $_POST['classId'];
}
$dataAry['Reason'] = $_POST['Reason'];
$dataAry['RecordStatus'] = $_POST['status'];

$lpf_dbs = new libpf_dbs_transcript();
$result = $lpf_dbs->insertUpdateDisabledStudentList($dataAry, $_POST['RecordID']);

$parms = 'pageNo='.$_POST['pageNo'].'&order='.$_POST['order'].'&field='.$_POST['field'].'&page_size_change='.$_POST['page_size_change'].'&numPerPage='.$_POST['numPerPage'];
$parms .= '&classId='.$classId.'&status='.$_POST['filter_status'];

$msg = $result? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: index.php?'.$parms.'&msg='.$msg);
?>