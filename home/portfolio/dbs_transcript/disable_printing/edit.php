<?php
// Modified by
/*
 * Modification Log:
 *  Date: 2018-08-16 (Bill)
 *      - Create file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript_ui.php");

iportfolio_auth("T");
intranet_opendb();

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();
$lpf_dbs_ui = new libpf_dbs_transcript_ui();

$is_edit = !$_POST['isNew'] && !empty($_POST['recordIds'][0]);
if($is_edit) {
    $disable_student = $lpf_dbs->getDisabledStudentList($_POST['recordIds'][0]);
    $disable_student = $disable_student[0];
    
    $post_class_id = $_POST['classId'];
}
else {
    $html["year_selection"] = $lpf_dbs_ui->getAcademicYearClassHistorySelection('targetYearID', '', $onChange='loadYearStudentList(this.value)', $noFirst=0, $firstTitleText='');
}

// Hidden Input fields
$table_hidden = <<<HTMLEND
<input type="hidden" name="pageNo" value="{$_POST['pageNo']}">
<input type="hidden" name="order" value="{$_POST['order']}">
<input type="hidden" name="field" value="{$_POST['field']}">
<input type="hidden" name="page_size_change" value="{$_POST['page_size_change']}">
<input type="hidden" name="numPerPage" value="{$_POST['numPerPage']}">

<input type="hidden" name="classId" value="{$post_class_id}">
<input type="hidden" name="filter_status" value="{$_POST['status']}">
<!--<input type="hidden" name="search_text" value="{$_POST['search_text']}">-->

<input type="hidden" id="RecordID" name="RecordID" value="{$disable_student['RecordID']}">
HTMLEND;
###############################################

###############################################
###	HTML - display table
$html["display_table"] = $table_hidden;

### Page ###
$linterface = new interface_html();
$CurrentPage = "Teacher_Transcript_DisableTranscriptPrinting";
$CurrentPageName = $Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Title'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName, "");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

######## Display #########
$TagTitle = $is_edit? $button_edit : $button_new;
// $PAGE_NAVIGATION[] = array($CurrentPageName);
$PAGE_NAVIGATION[] = array($TagTitle);
$html["navigation"] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$linterface->LAYOUT_START();

include_once("../templates/disable_printing_edit.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>