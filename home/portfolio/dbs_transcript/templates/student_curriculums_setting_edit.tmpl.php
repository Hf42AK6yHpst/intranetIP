<script type="text/javascript">
function goSubmit() {
	$('form#form1').attr('action', 'edit_update.php').submit();
}

function goBack() {
	window.location.href = 'index.php';
}

function changeClass() {
	$('form#form1').attr('action', 'edit.php').submit();
}

function applyAllCurriculum(val) {
	if(val != '') {
    	$('select[name^="curriculumID["]').each(function(){
    		$(this).val(val);
    	});
	}
}
</script>

<form name="form1" id="form1" method="POST" action="edit_update.php">
	<div class="table_board">
		<br />
		
		<div class="table_filter">
			<?=$htmlAry["classSelection"]?>
		</div>
		<p class="spacer"></p>
		<br />
		
		<div class="subject_level_table">
    		<?=$htmlAry['displayTable']?>
		</div>
		<div class="subject_level_table2" style="display: none">
    		<?=$htmlAry['dataTable']?>
		</div>
	</div>
	
	<?=$htmlAry['hiddenField']?>
</form>
<br/>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="4" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
	  	<td>
	    	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="edit_button">
	      		<tr><td align="center" valign="bottom"><?=$htmlAry["EditButton"]?></td></tr>
	  		</table>
	  	</td>
	</tr>
</table>