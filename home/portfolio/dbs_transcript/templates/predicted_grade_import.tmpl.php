<form name="form1" method="post" action="<?=$ImportTargetUrl?>" enctype="multipart/form-data">
	<br />
	
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
	    <td>
			<table width="91%" border="0" cellpadding="0" cellspacing="0" align="center" >
			<tr>
    			<td>
    				<table class="form_table_v30">
        				<?php if($ImportStep == 1) { ?>
        					<tr>
        						<td valign="top" nowrap="nowrap" class="field_title"><?=$linterface->RequiredSymbol()." ".$Lang['iPortfolio']['DBSTranscript']['Curriculums']?></td>
        						<td><?=$html["curriculumSelection"]?></td> 
        					</tr>
        					<tr id='phaseRow' style='display:none'>
        						<td valign="top" nowrap="nowrap" class="field_title"><?=$linterface->RequiredSymbol()." ".$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['ImportGrade']?></td>
        						<td>
        							<select name="importPhase" id="importPhase">';
                						<option value="1"><?=$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['OriginalGrade']?></option>
                						<option value="4"><?=$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['FinalGrade']?></option>
        							</select>
        						</td> 
        					</tr>
            				<tr>
                				<td width="100%" valign="top" nowrap="nowrap" class="field_title" ><?=$linterface->RequiredSymbol()." ".$Lang['General']['SourceFile']?></td>
                			  	<td class="tabletext"><input class="file" type="file" name="csvfile"></td>
            			  	</tr>
            				<tr id='csvIBRow' style='display:none'>
                				<td width="100%" valign="top" nowrap="nowrap" class="field_title"><?= $Lang['General']['CSVSample'] ?></td>
                				<td valign="top" nowrap="nowrap" colspan='2' class="tabletext">
                					<a id="SampleCsvLinkIB" class="tablelink" href="<?=GET_CSV('PG_IB.csv', '../home/portfolio/dbs_transcript/csv/', 0)?>" target="_blank">
                					<img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_files/xls.gif' border='0' align='absmiddle' hspace='3'> 
            						<?=$i_general_clickheredownloadsample?>
        						</td>
        					</tr>
            				<tr id='csvDSERow' style='display:none'>
                				<td width="100%" valign="top" nowrap="nowrap" class="field_title"><?= $Lang['General']['CSVSample'] ?></td>
                				<td valign="top" nowrap="nowrap" colspan='2' class="tabletext">
                					<a id="SampleCsvLinkDSE" class="tablelink" href="<?=GET_CSV('PG_DSE.csv', '../home/portfolio/dbs_transcript/csv/', 0)?>" target="_blank">
                					<img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_files/xls.gif' border='0' align='absmiddle' hspace='3'> 
            						<?=$i_general_clickheredownloadsample?>
        						</td>
        					</tr>
        			  	<?php } else if ($ImportStep == 2) { ?>
            				<tr>
        						<td valign="top" nowrap="nowrap" class="field_title" colspan="2" style="text-align:center">
        							<?php if($valid_data_count > 0) { ?>
        								<b><?=$valid_data_count?></b> <?=$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['ValidData']?>
    									<input type='hidden' name='curriculumID' value='<?=$curriculumID?>' />
    								<?php } else { ?>
    									<?=$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['InvalidData']?>
    								<?php } ?>
    							</td>
            			  	</tr>
        			  	<?php } else if ($ImportStep == 3) { ?>
            				<tr>
        						<td valign="top" nowrap="nowrap" class="field_title" colspan="2" style="text-align:center">
    								<b><?=$successCount?></b> <?=$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['ImportedData']?>
    							</td>
            			  	</tr>
        			  	<?php } ?>
    				</table>
    			</td>
			</tr>
			</table>
			<?php if($htmlAry["ErrorTable"] != '') { ?>
				<?= $linterface->GET_NAVIGATION2_IP25($Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['InvalidImport']) ?>
				<?= $htmlAry["ErrorTable"] ?>
			<?php } ?>
		</td>
	</tr>
	</table>
	
	<div id='table_content'><?= $html["display_table_fields"] ?></div>
</form>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?php if($ImportStep == 1) { ?>
    	<input type="button" value="<?=$Lang["Btn"]["Submit"]?>" class="formbutton_v30 print_hide " onclick="document.form1.submit()" id="submitBtn" name="submitBtn">
    	<input type="button" value="<?=$Lang["Btn"]["Back"]?>" class="formbutton_v30 print_hide " onclick="history.back()" id="backBtn" name="backBtn">
  	<?php } else if ($ImportStep == 2) { ?>
  		<?php if ($canSubmit) { ?>
        	<input type="button" value="<?=$Lang["Btn"]["Submit"]?>" class="formbutton_v30 print_hide " onclick="document.form1.submit()" id="submitBtn" name="submitBtn">
      	<?php } ?>
    	<input type="button" value="<?=$Lang["Btn"]["Back"]?>" class="formbutton_v30 print_hide " onclick="history.back()" id="backBtn" name="backBtn">
  	<?php } else if ($ImportStep == 3) { ?>
    	<input type="button" value="<?=$button_finish?>" class="formbutton_v30 print_hide " onclick="document.form1.submit()" id="backBtn" name="backBtn">
  	<?php } ?>
	<p class="spacer"></p>
</div>

<script type="text/javascript" language="JavaScript">
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image();?>';
var academicYearId = '<?=Get_Current_Academic_Year_ID()?>';

$(document).ready(function() {
	<?php if($ImportStep == 1) { ?>
	    updatedCurriculum($('#curriculumID').val());
	<?php } ?>
});

<?php if($ImportStep == 1) { ?>
    function updatedCurriculum(val)
    {
    	$('#phaseRow').hide();
    	$('#csvIBRow').hide();
    	$('#csvDSERow').hide();
    	if(val == <?=$IBCurriculumID?>) {
        	$('#csvIBRow').show();
    	}
    	if(val == <?=$DSECurriculumID?>) {
    		$('#phaseRow').show();
        	$('#csvDSERow').show();
    	}
    }
<?php } ?>
</script>