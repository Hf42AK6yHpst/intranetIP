<form name="form1" action="update.php" method="POST">
	<?=$html["navigation"]?>
	<br />
	
	<table class="form_table_v30">
    	<tbody>
    		<tr>
    			<td class="field_title">
    				<span class="tabletextrequire">*</span>
    				<?=$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['Code']?>
    			</td>
    			<td>
    				<input type="text" maxlength="25" class="textboxnum requiredField" name="Code" id="Code" value="<?=$curriculum['Code']?>">
    				<div style="display:none;" class="warnMsgDiv" id="Code_Warn">
    					<span class="tabletextrequire">*<?=$Lang["iPortfolio"]["DBSTranscript"]["WarningArr"]["InputWarning"].$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['Code']?></span>
    				</div>
    				<div style="display:none;" class="warnMsgDiv" id="Code_Warn2">
    					<span class="tabletextrequire">*<?=$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['Code'].$Lang["iPortfolio"]["DBSTranscript"]["WarningArr"]["DuplicateWarning"]?></span>
    				</div>
    			</td>
    		</tr>
    		<tr> 
    			<td class="field_title">
    				<span class="tabletextrequire">*</span>
    				<?=$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['NameEn']?>
    			</td>
    			<td>
    				<input type="text" maxlength="255" class="textboxtext requiredField" name="NameEn" id="NameEn" value="<?=$curriculum['NameEn']?>">
    				<div style="display:none;" class="warnMsgDiv" id="NameEn_Warn">
    					<span class="tabletextrequire">*<?=$Lang["iPortfolio"]["DBSTranscript"]["WarningArr"]["InputWarning"].$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['NameEn']?></span>
    				</div>
    				<div style="display:none;" class="warnMsgDiv" id="NameEn_Warn2">
    					<span class="tabletextrequire">*<?=$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['NameEn'].$Lang["iPortfolio"]["DBSTranscript"]["WarningArr"]["DuplicateWarning"]?></span>
    				</div>
    			</td>
    		</tr>
    		<tr>
    			<td class="field_title">
    				<span class="tabletextrequire">*</span>
    				<?=$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['NameCh']?>
    			</td>
    			<td>
    				<input type="text" maxlength="255" class="textboxtext requiredField" name="NameCh" id="NameCh" value="<?=$curriculum['NameCh']?>">
    				<div style="display:none;" class="warnMsgDiv" id="NameCh_Warn">
    					<span class="tabletextrequire">*<?=$Lang["iPortfolio"]["DBSTranscript"]["WarningArr"]["InputWarning"].$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['NameCh']?></span>
    				</div>
    				<div style="display:none;" class="warnMsgDiv" id="NameCh_Warn2">
    					<span class="tabletextrequire">*<?=$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['NameCh'].$Lang["iPortfolio"]["DBSTranscript"]["WarningArr"]["DuplicateWarning"]?></span>
    				</div>
    			</td>
    		</tr>
    		<tr>
    			<td class="field_title">
    				<?=$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['SubjectLevel']?>
    			</td>
    			<td>
					<textarea wrap="virtual" onfocus="this.rows=5;" rows="2" cols="90" id="SubjectLevel" name="SubjectLevel" class="tabletext requiredField"><?=$curriculum['SubjectLevel']?></textarea>
    			</td>
    		</tr>
    		<tr>
    			<td class="field_title">
    				<span class="tabletextrequire">*</span>
    				<?=$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['Status']?>
    			</td>
    			<td>
    				<input type="radio" name="status" id="status_activate" value='1' <?=($curriculum['Status']? 'checked' : '')?>/>
    				<label for="status_activate"> <?=$iPort['activate']?> </label>
    				<input type="radio" name="status" id="status_deactivate" value="0" <?=($curriculum['Status']? '' : 'checked')?>/>
    				<label for="status_deactivate"> <?=$iPort['deactivate']?> </label>
    			</td>
    		</tr>
    	</tbody>
    </table>
    
    <?=$html["display_table"]?>
</form>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<input type="button" value="<?=$Lang["Btn"]["Submit"]?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
	<input type="button" value="<?=$Lang["Btn"]["Back"]?>" class="formbutton_v30 print_hide " onclick="history.back()" id="backBtn" name="backBtn">
	<p class="spacer"></p>
</div>

<script>
var duplicateType = '';
function validateForm(){
	var Code = $('#Code').val();
	var NameEn = $('#NameEn').val();
	var NameCh = $('#NameCh').val();
	var CurriculumID = $('#CurriculumID').val();
	
	if(Code != '' && NameEn != '' && NameCh != '') {
		var data = {
			type: 'checkCurriculums',
			Code: Code,
			NameEn: NameEn,
			NameCh: NameCh,
			CurriculumID: CurriculumID
		};
    	$.ajax({
    		method: 'post',
    		url: '../ajax/ajax_validate.php',
    		data: data,
    		success: function(res){
    			duplicateType = res;
    		}
    	});
	}
}

function checkForm(){
	var check = true;
	if(!$('#Code').val()) {
		$('#Code_Warn').show();
		$('#Code').focus();
		check = false;
	}
	if(!$('#NameEn').val()) {
		$('#NameEn_Warn').show();
		$('#NameEn').focus();
		check = false;
	}
	if(!$('#NameCh').val()) {
		$('#NameCh_Warn').show();
		$('#NameCh').focus();
		check = false;
	}
	return check;
}

function goSubmit(){
	$('.warnMsgDiv').hide();
	$('#submitBtn').attr('disabled', true);
	$('#backBtn').attr('disabled', true);

	validateForm();
	setTimeout(function(){
		if(checkForm() && duplicateType == '') {
			form1.submit();
		} else {
			if(duplicateType != '') {
				$('#' + duplicateType + '_Warn2').show();
			}
			
			$('#submitBtn').attr('disabled', false);
			$('#backBtn').attr('disabled', false);
			return false;
		}
	}, 1000);
}
</script>