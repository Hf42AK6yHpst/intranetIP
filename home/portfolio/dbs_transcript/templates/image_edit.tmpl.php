<form name="form1" action="update.php" method="POST" enctype="multipart/form-data">
	<?=$html["navigation"]?>
	<br />
	
	<table class="form_table_v30">
    	<tbody>
    		<tr>
    			<td class="field_title">
    				<span class="tabletextrequire">*</span>
    				<?=$Lang['iPortfolio']['DBSTranscript']['Curriculums']?>
    			</td>
    			<td>
    				<?=$html["CurriculumSelection"]?>
    			</td>
    		</tr>
    		<?php if(!$isSignature) { ?>
        		<tr>
        			<td class="field_title">
        				<span class="tabletextrequire">*</span>
        				<?=$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Description']?>
        			</td>
        			<td>
        				<input type="text" maxlength="255" class="textboxtext requiredField" name="Description" id="Description" value="<?=$image['Description']?>">
        				<div style="display:none;" class="warnMsgDiv" id="Description_Warn">
        					<span class="tabletextrequire">*<?=$Lang["iPortfolio"]["DBSTranscript"]["WarningArr"]["InputWarning"].$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Description']?></span>
        				</div>
        				<div style="display:none;" class="warnMsgDiv" id="Description_Warn2">
        					<span class="tabletextrequire">*<?=$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Description'].$Lang["iPortfolio"]["DBSTranscript"]["WarningArr"]["DuplicateWarning"]?></span>
        				</div>
        			</td>
        		</tr>
    		<?php } else { ?>
        		<tr>
        			<td class="field_title">
        				<span class="tabletextrequire">*</span>
        				<?=$Lang['iPortfolio']['DBSTranscript']['ReportType']?>
        			</td>
        			<td>
        				<?=$html["ReportTypeSelection"]?>
        			</td>
        		</tr>
        		<tr>
        			<td class="field_title">
        				<span class="tabletextrequire">*</span>
        				<?=$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['SignatureName']?>
        			</td>
        			<td>
        				<input type="text" maxlength="255" class="textboxtext requiredField" name="SignatureName" id="SignatureName" value="<?=$image['SignatureName']?>">
        				<div style="display:none;" class="warnMsgDiv" id="SignatureName_Warn">
        					<span class="tabletextrequire">*<?=$Lang["iPortfolio"]["DBSTranscript"]["WarningArr"]["InputWarning"].$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['SignatureName']?></span>
        				</div>
        				<div style="display:none;" class="warnMsgDiv" id="SignatureName_Warn2">
        					<span class="tabletextrequire">*<?=$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['SignatureName'].$Lang["iPortfolio"]["DBSTranscript"]["WarningArr"]["DuplicateWarning"]?></span>
        				</div>
        			</td>
        		</tr>
        		<tr>
        			<td class="field_title">
        				<?=$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['SignatureDetails']?>
        			</td>
        			<td>
    					<textarea wrap="virtual" onfocus="this.rows=5;" rows="2" cols="90" id="SignatureDetails" name="SignatureDetails" class="tabletext requiredField"><?=$image['SignatureDetails']?></textarea>
        			</td>
        		</tr>
    		<?php } ?>
    		<tr>
    			<td class="field_title">
    				<span class="tabletextrequire">*</span>
    				<?=$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Image']?>
    			</td>
    			<td>
    				<input type="file" accept="image/*" name="fileToUpload" id="fileToUpload">
    				<br>
    				<!-- Display Image -->
    				<?php if($hasPhotoDispaly) { ?>
    					<div id='imageDiv'>
    						<?=$image['displayImage']?>
                            <!--
                            <br>
    						<span class="table_row_tool row_content_tool"><a onclick="goDelete();" title="刪除" class="delete" href="javascript:void(0);"></a></span><br style="clear:both;">
    						-->
    					</div>
    				<?php }?>
    				<div style="display:none;" class="warnMsgDiv" id="fileToUpload_Warn">
    					<span class="tabletextrequire">*<?=$Lang["iPortfolio"]["DBSTranscript"]["WarningArr"]["InputWarning"].$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Image']?></span>
    				</div>
    			</td>
    		</tr>
    	</tbody>
    </table>
    
    <?=$html["display_table"]?>
</form>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<input type="button" value="<?=$Lang["Btn"]["Submit"]?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
	<input type="button" value="<?=$Lang["Btn"]["Back"]?>" class="formbutton_v30 print_hide " onclick="history.back()" id="backBtn" name="backBtn">
	<p class="spacer"></p>
</div>

<script>
var duplicateType = '';
function validateForm(){
	var Description = '';
	var SignatureName = '';
	if($('#Description')[0]) {
		Description = $('#Description').val();
	}
	if($('#SignatureName')[0]) {
		SignatureName = $('#SignatureName').val();
	}
	var ImageID = $('#ImageID').val();
	var CurriculumID = $('#CurriculumID').val();
	var ReportTypeID = '';
	if($('#ReportTypeID')[0]) {
		var ReportTypeID = $('#ReportTypeID').val();
	}
	
	if(CurriculumID != '' && (Description != '' || SignatureName != '')) {
		var data = {
			type: 'checkImages',
			CurriculumID: CurriculumID,
			ReportTypeID:ReportTypeID,
			Description: Description,
			SignatureName: SignatureName,
			ImageID: ImageID,
			ImageType: '<?=$image_type?>'
		};
    	$.ajax({
    		method: 'post',
    		url: '../ajax/ajax_validate.php',
    		data: data,
    		success: function(res){
    			duplicateType = res;
    		}
    	});
	}
}

function checkForm(){
	var check = true;
	if($('#Description')[0] && !$('#Description').val()) {
		$('#Description_Warn').show();
		$('#Description').focus();
		check = false;
	}
	if($('#SignatureName')[0] && !$('#SignatureName').val()) {
		$('#SignatureName_Warn').show();
		$('#SignatureName').focus();
		check = false;
	}
	if($('#ImageLink').val() == '' && !$('#fileToUpload').val()) {
		$('#fileToUpload_Warn').show();
		$('#fileToUpload').focus();
		check = false;
	}
	return check;
}

function goSubmit(){
	$('.warnMsgDiv').hide();
	$('#submitBtn').attr('disabled', true);
	$('#backBtn').attr('disabled', true);

	validateForm();
	setTimeout(function(){
		if(checkForm() && duplicateType == '') {
			form1.submit();
		} else {
			if(duplicateType != '') {
				$('#' + duplicateType + '_Warn2').show();
			}
			
			$('#submitBtn').attr('disabled', false);
			$('#backBtn').attr('disabled', false);
			return false;
		}
	}, 1000);
}
</script>