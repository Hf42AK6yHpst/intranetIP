<script language="JavaScript">
var SearchTextFocus = false;

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD() {
	document.form1.action = "index.php";
    document.form1.submit();
}

$(document).ready(function() {
    $("input[name=search_name]").keypress(function(event){
        if(event.keyCode == 13) {
        	document.form1.action = "index.php";
        	document.form1.submit();
        }
    });
});
</script>

<form name="form1" method="POST">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" bgcolor="#FFFFFF">
			<table width="98%" border="0" cellspacing="3" cellpadding="0">
				<tr><td>
						<br/>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr><td>
									<table border="0" cellspacing="0" cellpadding="3" width="100%">
										<tr>
											<td>
												<?php if($isAlumniAccess) { ?>
													<?=$html["yearSelection"]?>
												<?php } ?>
												<?=$htmlAry["formSelection"]?>
											</td>
											<td align="right" valign="middle" class="thumb_list">
												<?php if(!$isAlumniAccess) { ?>
													<span> <?=$i_identity_student?> </span> | <a href="index.php?isAlumni=1"><?=$i_identity_alumni?></a>
												<?php } else { ?>
													<a href="index.php"> <?=$i_identity_student?> </a> | <span> <?=$i_identity_alumni?> </span></a>
												<?php } ?>
											</td>
										</tr>
									</table>
							</td></tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<?=$html["display_table"]?>
								</td>
							</tr>
						</table>
				</td></tr>
			</table>
			<br>
	</td></tr>
</table>

<?=$html["table_hidden_field"]?>
</form>