<?php
// Modified by
/*
 * Modification Log:
 *  Date: 2018-08-16 (Bill)
 *      - Create file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");

iportfolio_auth("T");
intranet_opendb();

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();

$is_edit = !$_POST['isNew'] && !empty($_POST['curriculumIds'][0]);
if($is_edit) {
    $curriculum = $lpf_dbs->getCurriculumSetting($_POST['curriculumIds'][0]);
    $curriculum = $curriculum[0];
}

// Hidden Input fields
$table_hidden = <<<HTMLEND
<input type="hidden" name="pageNo" value="{$_POST['pageNo']}">
<input type="hidden" name="order" value="{$_POST['order']}">
<input type="hidden" name="field" value="{$_POST['field']}">
<input type="hidden" name="page_size_change" value="{$_POST['page_size_change']}">
<input type="hidden" name="numPerPage" value="{$_POST['numPerPage']}">

<input type="hidden" id="CurriculumID" name="CurriculumID" value="{$curriculum['CurriculumID']}">
HTMLEND;
###############################################

###############################################
###	HTML - display table
$html["display_table"] = $table_hidden;

### Page ###
$linterface = new interface_html();
$CurrentPage = "Teacher_Transcript_CurriculumsSetting";
$CurrentPageName = $Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['Title'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName, "");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

######## Display #########
$TagTitle = $is_edit? $button_edit : $button_new;
// $PAGE_NAVIGATION[] = array($CurrentPageName);
$PAGE_NAVIGATION[] = array($TagTitle);
$html["navigation"] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$linterface->LAYOUT_START();

include_once("../templates/curriculums_setting_edit.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>