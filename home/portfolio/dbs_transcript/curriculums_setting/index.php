<?php
// Modified by
/*
 * Modification Log:
 *  Date: 2018-08-16 (Bill)
 *      - Create file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");

iportfolio_auth("T");
intranet_opendb();

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();

// Table settings
$field = isset($_POST["field"])? $_POST["field"] : $_GET["field"];
$order = isset($_POST["order"])? $_POST["order"] : (isset($_GET["order"])? $_GET["order"] : 1);
$pageNo = isset($_POST["pageNo"])? $_POST["pageNo"] : $_GET["pageNo"];
$page_size_change = isset($_POST["page_size_change"])? $_POST["page_size_change"] : $_GET["page_size_change"];
$numPerPage = !empty($_POST["numPerPage"])? $_POST["numPerPage"] : (!empty($_GET["numPerPage"])? $_GET["numPerPage"] : $page_size);

// Table setups
$pfTable = new libpf_dbtable($field, $order, $pageNo);
$pfTable->page_size = $numPerPage;

// Table headers
$columnCount = 0;
$pfTable->column_list .= "<th width='1'>#</th>\n";
$pfTable->column_list .= "<th nowrap>".$pfTable->column($columnCount++, $Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['Code'])."</th>\n";
$pfTable->column_list .= "<th>".$pfTable->column($columnCount++, $Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['NameEn'])."</th>\n";
$pfTable->column_list .= "<th>".$pfTable->column($columnCount++, $Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['NameCh'])."</th>\n";
$pfTable->column_list .= "<th nowrap>".$pfTable->column($columnCount++, $Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['SubjectLevel'])."</th>\n";
$pfTable->column_list .= "<th>".$pfTable->column($columnCount++, $Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['Status'])."</th>\n";
$pfTable->column_list .= "<th width='1'>".$pfTable->check("curriculumIds[]")."</th>\n";

// Table controls
$pfTable->IsColOff = "IP25_table";
$pfTable->field_array = array("Code", "NameEn", "NameCh", "SubjectLevel", "Status");
$pfTable->no_col = 1/* row number */+count($pfTable->field_array)+1/* checkbox */;
$pfTable->column_array = array(0,0,0,0,0);

$pfTable->sql = $lpf_dbs->getCurriculumSetting('', '', '', '', '', $returnSQL=true);

// Get table
$displayTableHtml = $pfTable->display();
$table_hidden = <<<HTMLEND
<input type="hidden" name="pageNo" value="{$pfTable->pageNo}">
<input type="hidden" name="order" value="{$pfTable->order}">
<input type="hidden" name="field" value="{$pfTable->field}">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="{$pfTable->page_size}">
<input type="hidden" name="isNew" value="0">
HTMLEND;
$displayTableHtml .= $table_hidden;
###############################################

###############################################
###	HTML - display table
$html["display_table"] = $displayTableHtml;

### Page ###
$linterface = new interface_html();
$CurrentPage = "Teacher_Transcript_CurriculumsSetting";
$CurrentPageName = $Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['Title'];

### Title ###
$TAGS_OBJ[] = array($Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['Title'], "");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

######## Display #########
// $PAGE_NAVIGATION[] = array($CurrentPageName);
// $html["navigation"] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);

include_once("../templates/curriculums_setting_list.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>