<?php
// Modifying By:
/*
 * Modification Log:
 *  Date: 2018-08-16 (Bill)
 *      - Create file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");

iportfolio_auth("T");
intranet_opendb();

$lpf_dbs = new libpf_dbs_transcript();
$statusAry = $lpf_dbs->getStatus();

$dataAry = array();
$dataAry['Status'] = $statusAry["Activate"];

$result = array();
foreach((array)$_POST['curriculumIds'] as $thisCurriculumID) {
    $result[] = $lpf_dbs->insertUpdateCurriculumSetting($dataAry, $thisCurriculumID);
}

$parms = 'pageNo='.$_POST['pageNo'].'&order='.$_POST['order'].'&field='.$_POST['field'].'&page_size_change='.$_POST['page_size_change'].'&numPerPage='.$_POST['numPerPage'];

$msg = !in_array(false, $result)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: index.php?'.$parms.'&msg='.$msg);
?>