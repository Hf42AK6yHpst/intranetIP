<?php
// Using: Bill

############# Change Log [Start] ################
#
#   Date:   2018-08-29 (Bill)   [2017-1207-0956-53277]
#           Create file
# 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript_ui.php");

iportfolio_auth("T");
intranet_opendb();

$linterface = new interface_html();

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();

# Subject Grade options
$gradingOpts = array();
foreach($ipf_cfg["DSBTranscript"]["SUBJECT_GRADE"] as $thisGrade) {
    $gradingOpts[] = array($thisGrade, $thisGrade);
}

# Get all gradings
$gradingMappingAry = $lpf_dbs->getIBGradeMapping();
$gradingMappingAry = BuildMultiKeyAssoc($gradingMappingAry, array("SubjectCode", "SubjectLevel", "IBGrade"));

# Build Subject table
$allSubjectTableView = '';
$allSubjectTableEdit = '';
$subjectMathTableView = '';
$subjectMathTableEdit = '';
$subjectChiTableView = '';
$subjectChiTableEdit = '';
foreach((array)$ipf_cfg["DSBTranscript"]["IB_GRADE"] as $IBGrade)
{
    // All subjects except Maths and Chinese
    $thisGradeMapping = $gradingMappingAry['ALL'][''][$IBGrade];
    $selected_value = $thisGradeMapping['TargetGrade'];
    $select_tag = " id='subjectGrade_".$thisGradeMapping['MappingID']."' name='subjectGrade[".$thisGradeMapping['MappingID']."]' ";
    $gradeSelection = getSelectByArray($gradingOpts, $select_tag, $selected_value, 0, 1);
    
    $allSubjectTableView .= "<tr>";
        $allSubjectTableView .= "<td class='tabletext'>".$IBGrade."</td>";
        $allSubjectTableView .= "<td class='tabletext'>".$selected_value."</td>";
    $allSubjectTableView .= "</tr>";
    
    $allSubjectTableEdit .= "<tr>";
        $allSubjectTableEdit .= "<td class='tabletext'>".$IBGrade."</td>";
        $allSubjectTableEdit .= "<td class='tabletext'>".$gradeSelection."</td>";
    $allSubjectTableEdit .= "</tr>";
    
    // IB G10 'Pre-IB Mathematics'
    $thisGradeMapping = $gradingMappingAry['MS'][''][$IBGrade];
    $selected_value1 = $thisGradeMapping['TargetGrade'];
    $select_tag = " id='subjectGrade_".$thisGradeMapping['MappingID']."' name='subjectGrade[".$thisGradeMapping['MappingID']."]' ";
    $gradeSelection1 = getSelectByArray($gradingOpts, $select_tag, $selected_value1, 0, 1);
    
    $thisGradeMapping = $gradingMappingAry['MATH']['SL'][$IBGrade];
    $selected_value2 = $thisGradeMapping['TargetGrade'];
    $select_tag = " id='subjectGrade_".$thisGradeMapping['MappingID']."' name='subjectGrade[".$thisGradeMapping['MappingID']."]' ";
    $gradeSelection2 = getSelectByArray($gradingOpts, $select_tag, $selected_value2, 0, 1);
    
    $thisGradeMapping = $gradingMappingAry['MATH']['HL'][$IBGrade];
    $selected_value3 = $thisGradeMapping['TargetGrade'];
    $select_tag = " id='subjectGrade_".$thisGradeMapping['MappingID']."' name='subjectGrade[".$thisGradeMapping['MappingID']."]' ";
    $gradeSelection3 = getSelectByArray($gradingOpts, $select_tag, $selected_value3, 0, 1);
    
    $subjectMathTableView .= "<tr>";
        $subjectMathTableView .= "<td class='tabletext'>".$IBGrade."</td>";
        $subjectMathTableView .= "<td class='tabletext'>".$selected_value1."</td>";
        $subjectMathTableView .= "<td class='tabletext'>".$selected_value2."</td>";
        $subjectMathTableView .= "<td class='tabletext'>".$selected_value3."</td>";
    $subjectMathTableView .= "</tr>";
    
    $subjectMathTableEdit .= "<tr>";
        $subjectMathTableEdit .= "<td class='tabletext'>".$IBGrade."</td>";
        $subjectMathTableEdit .= "<td class='tabletext'>".$gradeSelection1."</td>";
        $subjectMathTableEdit .= "<td class='tabletext'>".$gradeSelection2."</td>";
        $subjectMathTableEdit .= "<td class='tabletext'>".$gradeSelection3."</td>";
    $subjectMathTableEdit  .= "</tr>";
    
    // IB G10 'Pre-IB Chinese'
    $thisGradeMapping = $gradingMappingAry['C_LIT'][''][$IBGrade];
    $selected_value1 = $thisGradeMapping['TargetGrade'];
    $select_tag = " id='subjectGrade_".$thisGradeMapping['MappingID']."' name='subjectGrade[".$thisGradeMapping['MappingID']."]' ";
    $gradeSelection1 = getSelectByArray($gradingOpts, $select_tag, $selected_value1, 0, 1);
    
    $thisGradeMapping = $gradingMappingAry['CH_LIT'][''][$IBGrade];
    $selected_value2 = $thisGradeMapping['TargetGrade'];
    $select_tag = " id='subjectGrade_".$thisGradeMapping['MappingID']."' name='subjectGrade[".$thisGradeMapping['MappingID']."]' ";
    $gradeSelection2 = getSelectByArray($gradingOpts, $select_tag, $selected_value2, 0, 1);
    
    $thisGradeMapping = $gradingMappingAry['CH_B'][''][$IBGrade];
    $selected_value3 = $thisGradeMapping['TargetGrade'];
    $select_tag = " id='subjectGrade_".$thisGradeMapping['MappingID']."' name='subjectGrade[".$thisGradeMapping['MappingID']."]' ";
    $gradeSelection3 = getSelectByArray($gradingOpts, $select_tag, $selected_value3, 0, 1);
    
    $subjectChiTableView .= "<tr>";
        $subjectChiTableView .= "<td class='tabletext'>".$IBGrade."</td>";
        $subjectChiTableView .= "<td class='tabletext'>".$selected_value1."</td>";
        $subjectChiTableView .= "<td class='tabletext'>".$selected_value2."</td>";
        $subjectChiTableView .= "<td class='tabletext'>".$selected_value3."</td>";
    $subjectChiTableView .= "</tr>";
    
    $subjectChiTableEdit .= "<tr>";
        $subjectChiTableEdit .= "<td class='tabletext'>".$IBGrade."</td>";
        $subjectChiTableEdit .= "<td class='tabletext'>".$gradeSelection1."</td>";
        $subjectChiTableEdit .= "<td class='tabletext'>".$gradeSelection2."</td>";
        $subjectChiTableEdit .= "<td class='tabletext'>".$gradeSelection3."</td>";
    $subjectChiTableEdit  .= "</tr>";
}
$htmlAry['AllSubjectTable']['View'] = $allSubjectTableView;
$htmlAry['AllSubjectTable']['Edit'] = $allSubjectTableEdit;
$htmlAry['MathSubjectTable']['View'] = $subjectMathTableView;
$htmlAry['MathSubjectTable']['Edit'] = $subjectMathTableEdit;
$htmlAry['ChiSubjectTable']['View'] = $subjectChiTableView;
$htmlAry['ChiSubjectTable']['Edit'] = $subjectChiTableEdit;

# Button
$htmlAry["EditButton"] = "";
$htmlAry["EditButton"] .= $linterface->GET_ACTION_BTN($button_submit, "button", "goSubmit()", "submitBtn");
$htmlAry["EditButton"] .= "&nbsp;&nbsp;";
$htmlAry["EditButton"] .= $linterface->GET_ACTION_BTN($button_back, "button", "goBack()", "backBtn");
$htmlAry["ViewButton"] = "";
$htmlAry["ViewButton"] .= $linterface->GET_ACTION_BTN($button_edit, "button", "goEdit()", "editBtn");

### Page ###
$CurrentPage = "Teacher_Transcript_GradingScaleForPreIBSubject";

### Title ###
$TAGS_OBJ[] = array($Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['Title']);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

include_once("../templates/grading_scale.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>