<?php
// Modified by
/*
 * Modification Log:
 *  Date: 2018-08-16 (Bill)
 *      - Create file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript_ui.php");

$reportTypeAry = $_REQUEST['ReportType'];
//$reportFormAry = $_REQUEST['TargetForm'];
$curriculumID = $_REQUEST['curriculumID'];
$findTarget = $_REQUEST['findTarget'];
$targetType = $_REQUEST['TargetType'];
$targetIDAry = $_REQUEST['TargetID'];
$headerImageID = $_REQUEST['headerImageID'];
$backCoverImageID = $_REQUEST['backCoverImageID'];
$leftSignatureID = $_REQUEST['leftSignatureID'];
$rightSignatureID = $_REQUEST['rightSignatureID'];

$reportFormAry = array();
$reportStartForm = IntegerSafe($_REQUEST['TargetFormStart']);
if($reportStartForm == '') {
    $reportStartForm = 9;
}
$reportEndForm = IntegerSafe($_REQUEST['TargetFormEnd']);
if($reportStartForm <= $reportEndForm) {
    for($i=$reportStartForm; $i<=$reportEndForm; $i++) {
        $reportFormAry[] = $i;
    }
}

$target_type = $_REQUEST['target_type'];
if($target_type != 1) {
    $targetType = 'student';
    $targetIDAry = $_REQUEST['TargetID_Year'];
}
if($findTarget != 1) {
    $targetType = "student";
    $targetIDAry = $_REQUEST['search_by_name'];
}

iportfolio_auth("T");
intranet_opendb();

$libdb = new libdb();
$lpf_dbs = new libpf_dbs_transcript();
$fcm = new form_class_manage();

# Year & Term
$thisAcademicYearID = Get_Current_Academic_Year_ID();
$thisAcademicTermID = getCurrentSemesterID();
if($target_type != 1) {
    $thisAcademicYearID = $_REQUEST['targetYearID'];
}

# Curriculum
$allCurriculumAry = $lpf_dbs->getCurriculumSetting('', '', '', '', $ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"]);
$allCurriculumAry = BuildMultiKeyAssoc($allCurriculumAry, 'CurriculumID');
$targetCurriculum = $allCurriculumAry[$curriculumID];
$isIBTranscriptReport = $targetCurriculum['Code'] == 'IB' || $targetCurriculum['Code'] == 'IBDP';
$isDBSTranscriptReport = $targetCurriculum['Code'] == 'DSE' || $targetCurriculum['Code'] == 'HKDSE';
// $isIBTranscriptReport = false;

# Report Target
// Level
if($targetType == "form")
{
    $targetIDCond = " AND y.YearID IN ('".implode("','", (array)$targetIDAry)."') ";
    $extraCond = '';
    
    // Get Class Student
    $classStudentIdAry = array();
    foreach($targetIDAry as $thisYearId)
    {
        $thisCurriculumClassAry = $lpf_dbs->getCurriculumClassList($curriculumID);
        $thisCurriculumClassAry = Get_Array_By_Key($thisCurriculumClassAry, 'YearClassID');
        
        $thisLevelStudentIDAry = $fcm->Get_Student_By_Form('', $thisYearId);
        $FormClassStudentIDAry = BuildMultiKeyAssoc($thisLevelStudentIDAry, array('YearClassID', 'UserID'), 'UserID', 1, 0);
        foreach($FormClassStudentIDAry as $thisYearClassId => $thisClassStudentIDAry)
        {
            if(!in_array($thisYearClassId, (array)$thisCurriculumClassAry)) {
                continue;
            }
            
            $studentCurriculumSettingAry = $lpf_dbs->getStudentCurriculumSettings($thisClassStudentIDAry);
            if(!empty($studentCurriculumSettingAry))
            {
                // Get Class Student with target Curriculum
                $studentCurriculumSettingAry = BuildMultiKeyAssoc($studentCurriculumSettingAry, array('CurriculumID', 'StudentID'), 'StudentID', 1, 0);
                $studentWithTargetCurriculumAry = $studentCurriculumSettingAry[$curriculumID];
                
                // Get Class Student without any Curriculum Settings (mainly for new Class Students)
                $studentWithoutCurriculumSettingAry = $lpf_dbs->getStudentWithoutCurriculumSettings($thisClassStudentIDAry, $curriculumID);
                $studentWithoutCurriculumSettingAry = Get_Array_By_Key($studentWithoutCurriculumSettingAry, 'UserID');
                $thisClassStudentIDAry = array_merge((array)$studentWithTargetCurriculumAry, (array)$studentWithoutCurriculumSettingAry);
            }
            $classStudentIdAry = array_merge($classStudentIdAry, $thisClassStudentIDAry);
        }
    }
    if(!empty($classStudentIdAry)) {
        $extraCond = " AND iu.UserID IN (".implode(",", (array)$classStudentIdAry).") ";
    }
    //$extraCond = " AND (yc.CurriculumID = '$curriculumID' $extraCond2) ";
    
//     $targetField = "YearID";
//
//     $sql = "SELECT
// 				YearID, YearName as Name
// 			FROM
// 				YEAR
// 			WHERE
// 				YearID IN ('".implode("','", (array)$targetIDAry)."')";
//     $target_name = $libdb->returnArray($sql);
//     $target_name = BuildMultiKeyAssoc((array)$target_name, array("YearID"));
}
// Class
else if($targetType == "class")
{
    $targetIDCond = " AND yc.YearClassID IN ('".implode("','", (array)$targetIDAry)."') ";
    $extraCond = '';
    
    // Get Class Student
    $classStudentIdAry = array();
    foreach($targetIDAry as $thisYearClassId)
    {
        $thisClassStudentIDAry = $fcm->Get_Student_By_Class($thisYearClassId);
        $thisClassStudentIDAry = Get_Array_By_Key($thisClassStudentIDAry, 'UserID');
        
        $studentCurriculumSettingAry = $lpf_dbs->getStudentCurriculumSettings($thisClassStudentIDAry);
        if(!empty($studentCurriculumSettingAry))
        {
            // Get Class Student with target Curriculum
            $studentCurriculumSettingAry = BuildMultiKeyAssoc($studentCurriculumSettingAry, array('CurriculumID', 'StudentID'), 'StudentID', 1, 0);
            $studentWithTargetCurriculumAry = $studentCurriculumSettingAry[$curriculumID];
            
            // Get Class Student without any Curriculum Settings (mainly for new Class Students)
            $studentWithoutCurriculumSettingAry = $lpf_dbs->getStudentWithoutCurriculumSettings($thisClassStudentIDAry, $curriculumID);
            $studentWithoutCurriculumSettingAry = Get_Array_By_Key($studentWithoutCurriculumSettingAry, 'UserID');
            $thisClassStudentIDAry = array_merge((array)$studentWithTargetCurriculumAry, (array)$studentWithoutCurriculumSettingAry);
        }
        $classStudentIdAry = array_merge($classStudentIdAry, $thisClassStudentIDAry);
    }
    if(!empty($classStudentIdAry)) {
        $extraCond = " AND iu.UserID IN (".implode(",", (array)$classStudentIdAry).") ";
    }
//     $targetField = "YearClassID";
//
//     $sql = "SELECT
// 				YearClassID, ClassTitleB5 as Name
// 			FROM
// 				YEAR_CLASS
// 			WHERE
// 				AcademicYearID = '".Get_Current_Academic_Year_ID()."' AND YearClassID IN ('".implode("','", (array)$targetIDAry)."')";
//     $target_name = $libdb->returnArray($sql);
//     $target_name = BuildMultiKeyAssoc((array)$target_name, array("YearClassID"));
}
// Student
else
{
    $targetIDCond = " AND ycu.UserID IN ('".implode("','", (array)$targetIDAry)."') ";
    $extraCond = "";
//     $targetField = "YearClassID";
}

# Get disabled Student
$cond_disabled = '';
$disabledStudentIDAry = $lpf_dbs->getDisabledStudentList('', '', $ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"]);
$disabledStudentIDAry = Get_Array_By_Key($disabledStudentIDAry, 'StudentID');
if(!empty($disabledStudentIDAry)) {
    $cond_disabled = " AND iu.UserID NOT IN (".implode(",", $disabledStudentIDAry).") ";
}

# Target Student
if($target_type == 1)
{
    $sql = "SELECT
    			iu.UserID,
    			y.YearID,
                y.YearName,
                (CONVERT(SUBSTRING(y.WEBSAMSCode, 2, 1), UNSIGNED INTEGER) + 6) as YearNumber,
    			yc.YearClassID,
    			yc.ClassTitleEN as ClassName,
    			ycu.ClassNumber,
    			iu.EnglishName as StudentName,
                iu.DateOfBirth,
                iup.AdmissionDate,
                iup.GraduationDate
    		FROM
                INTRANET_USER as iu
        		LEFT JOIN YEAR_CLASS_USER as ycu ON (ycu.UserID = iu.UserID)
        		LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '".$thisAcademicYearID."')
        		LEFT JOIN YEAR as y ON (y.YearID = yc.YearID)
                LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as iup ON (iu.UserID = iup.UserID)
    		WHERE
    			yc.AcademicYearID = '".$thisAcademicYearID."'
    			$targetIDCond
    			$extraCond
    			$cond_disabled
    		GROUP BY
    			iu.UserID
    		ORDER BY
    		    y.Sequence, yc.Sequence, ycu.ClassNumber";
    $studentInfoAry = $libdb->returnResultSet($sql);
}
else
{
    if($findTarget == 1)
    {
        $sql = "SELECT
                    IF(iu.UserID IS NOT NULL, iu.UserID, iau.UserID) as UserID,
                    y.YearID,
                    y.YearName,
                    (CONVERT(SUBSTRING(y.WEBSAMSCode, 2, 1), UNSIGNED INTEGER) + 6) as YearNumber,
                    yc.YearClassID,
                    yc.ClassTitleEN as ClassName,
                    ycu.ClassNumber,
                    IF(iu.UserID IS NOT NULL, iu.EnglishName, iau.EnglishName) as StudentName,
                    IF(iu.UserID IS NOT NULL, iu.DateOfBirth, iau.DateOfBirth) as DateOfBirth,
                    IF(iu.UserID IS NOT NULL, iup.AdmissionDate, iaup.AdmissionDate) as AdmissionDate,
                    IF(iu.UserID IS NOT NULL, iup.GraduationDate, iaup.GraduationDate) as GraduationDate
                FROM
                    YEAR_CLASS_USER as ycu
                    INNER JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '".$thisAcademicYearID."' )
                    INNER JOIN YEAR as y ON (y.YearID = yc.YearID)
                    LEFT JOIN INTRANET_USER as iu ON (ycu.UserID = iu.UserID)
                    LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as iup ON (iu.UserID = iup.UserID)
                    LEFT JOIN INTRANET_ARCHIVE_USER as iau ON (ycu.UserID = iau.UserID)
                    LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as iaup ON (iau.UserID = iaup.UserID)
                WHERE
                    (iu.UserID IS NOT NULL OR iau.UserID IS NOT NULL) AND
                    yc.AcademicYearID = '".$thisAcademicYearID."' 
                    $targetIDCond
                    $extraCond
                    $cond_disabled
                GROUP BY
                    IF(iu.UserID IS NOT NULL, iu.UserID, iau.UserID)
                ORDER BY
                    y.Sequence, yc.Sequence, ycu.ClassNumber";
        $studentInfoAry = $libdb->returnResultSet($sql);
    }
    else
    {
        $sql = "SELECT
                    IF(iu.UserID IS NOT NULL, iu.UserID, iau.UserID) as UserID,
                    y.YearID,
                    y.YearName,
                    (CONVERT(SUBSTRING(y.WEBSAMSCode, 2, 1), UNSIGNED INTEGER) + 6) as YearNumber,
                    yc.YearClassID,
                    yc.ClassTitleEN as ClassName,
                    ycu.ClassNumber,
                    IF(iu.UserID IS NOT NULL, iu.EnglishName, iau.EnglishName) as StudentName,
                    IF(iu.UserID IS NOT NULL, iu.DateOfBirth, iau.DateOfBirth) as DateOfBirth,
                    IF(iu.UserID IS NOT NULL, iup.AdmissionDate, iaup.AdmissionDate) as AdmissionDate,
                    IF(iu.UserID IS NOT NULL, iup.GraduationDate, iaup.GraduationDate) as GraduationDate
                FROM
                    YEAR_CLASS_USER as ycu
                    INNER JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID)
                    INNER JOIN YEAR as y ON (y.YearID = yc.YearID)
                    LEFT JOIN INTRANET_USER as iu ON (ycu.UserID = iu.UserID)
                    LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as iup ON (iu.UserID = iup.UserID)
                    LEFT JOIN INTRANET_ARCHIVE_USER as iau ON (ycu.UserID = iau.UserID)
                    LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as iaup ON (iau.UserID = iaup.UserID)
                WHERE
                    (iu.UserID IS NOT NULL OR iau.UserID IS NOT NULL)
                    $targetIDCond
                    $extraCond
                    $cond_disabled
                ORDER BY
                    y.Sequence, ycu.YearClassUserID, yc.Sequence, ycu.ClassNumber";
        $tempAry = $libdb->returnResultSet($sql);

        $studentInfoAry = array();
        foreach($tempAry as $tempDataAry)
        {
            $studentInfoAry[$tempDataAry['UserID']] = $tempDataAry;
        }
        $studentInfoAry = array_values($studentInfoAry);
    }
}
$studentIDAry = Get_Array_By_Key($studentInfoAry, "UserID");
$student_count = count($studentInfoAry);

# Target Student History
$sql = "SELECT
            ycu.UserID, yc.AcademicYearID, ay.YearNameEN as AcademicYearName, 
            yc.ClassTitleEN as ClassName, ycu.ClassNumber, yc.WEBSAMSCode as ClassWebSAMSCode,
            y.YearID, y.YearName, (CONVERT(SUBSTRING(y.WEBSAMSCode, 2, 1), UNSIGNED INTEGER) + 6) as YearNumber
        FROM
            YEAR_CLASS_USER as ycu
            INNER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
            INNER JOIN YEAR as y ON (yc.YearID = y.YearID)
            INNER JOIN ACADEMIC_YEAR_TERM as ayt ON (yc.AcademicYearID = ayt.AcademicYearID)
            INNER JOIN ACADEMIC_YEAR as ay ON (ayt.AcademicYearID = ay.AcademicYearID)
        WHERE
            ycu.UserID IN ('".implode("','", (array)$studentIDAry)."') 
        GROUP BY
            ycu.UserID, yc.AcademicYearID, yc.ClassTitleEN, yc.ClassTitleB5, ycu.ClassNumber, yc.WEBSAMSCode, yc.YearID, y.YearName
        ORDER BY
            ayt.TermStart ";
$studentClassHistoryAry = $libdb->returnResultSet($sql);
$numOfData = count($studentClassHistoryAry);

# Analyze Student Class History
$studentDataAssoAry = array();
for ($i=0; $i < $numOfData; $i++)
{
    $_studentId = $studentClassHistoryAry[$i]['UserID'];
    $_formId = $studentClassHistoryAry[$i]['YearID'];
    $_formName = $studentClassHistoryAry[$i]['YearName'];
    $_formNumber = $studentClassHistoryAry[$i]['YearNumber'];
    $_academicYearId = $studentClassHistoryAry[$i]['AcademicYearID'];
    $_academicYearName = $studentClassHistoryAry[$i]['AcademicYearName'];

    // Define data format first
    if (!isset($studentDataAssoAry[$_studentId])) {
        $studentDataAssoAry[$_studentId]['firstFormNumber'] = $_formNumber;
        $studentDataAssoAry[$_studentId]['firstAcademicYearId'] = $_academicYearId;
        
        $studentDataAssoAry[$_studentId]['classHistoryAry'] = array();
        $studentDataAssoAry[$_studentId]['classHistoryAry']['firstFormId'] = '';
        $studentDataAssoAry[$_studentId]['classHistoryAry']['firstFormNumber'] = '';
        $studentDataAssoAry[$_studentId]['classHistoryAry']['firstAcademicYearId'] = '';
        $studentDataAssoAry[$_studentId]['classHistoryAry']['lastFormId'] = '';
        $studentDataAssoAry[$_studentId]['classHistoryAry']['lastFormNumber'] = '';
        $studentDataAssoAry[$_studentId]['classHistoryAry']['lastAcademicYearId'] = '';
    }
    
//     // for 146 testing
//     if($_academicYearId == 2) {
//         $_formNumber = 9;
//     }
//     if($_academicYearId == 9) {
//         $_formNumber = 10;
//     }
//     if($_academicYearId == 42) {
//         $_formNumber = 11;
//     }
//     if($_academicYearId == 22) {
//         $_formNumber = 12;
//     }
    
    $studentDataAssoAry[$_studentId]['classHistoryAry']['AcademicYearAssoAry'][$_academicYearId]['formId'] = $_formId;
    $studentDataAssoAry[$_studentId]['classHistoryAry']['AcademicYearAssoAry'][$_academicYearId]['formName'] = $_formName;
    $studentDataAssoAry[$_studentId]['classHistoryAry']['AcademicYearAssoAry'][$_academicYearId]['formNumber'] = $_formNumber;
    $studentDataAssoAry[$_studentId]['classHistoryAry']['AcademicYearAssoAry'][$_academicYearId]['academicYearId'] = $_academicYearId;
    $studentDataAssoAry[$_studentId]['classHistoryAry']['AcademicYearAssoAry'][$_academicYearId]['academicYearName'] = $_academicYearName;
    $studentDataAssoAry[$_studentId]['classHistoryAry']['AcademicYearAssoAry'][$_academicYearId]['studentStudied'] = true;
}
foreach ((array)$studentDataAssoAry as $_studentId => $_studentDataAry)
{
    // Find student last year in school
    $_studentLastStudiedAcademicYearId = '';
    foreach ((array)$_studentDataAry['classHistoryAry']['AcademicYearAssoAry'] as $__academicYearId => $__academicYearInfoAry)  {
        $__studentStudied = $__academicYearInfoAry['studentStudied'];
        if ($__studentStudied) {
            $_studentLastStudiedAcademicYearId = $__academicYearId;
        }
    }
    
    // Remove year data after student left school - data after last studied year
    $_startToDelete = false;
    foreach ((array)$_studentDataAry['classHistoryAry']['AcademicYearAssoAry'] as $__academicYearId => $__academicYearInfoAry) {
        if ($_startToDelete) {
            unset($studentDataAssoAry[$_studentId]['classHistoryAry']['AcademicYearAssoAry'][$__academicYearId]);
        }
        if ($__academicYearId == $_studentLastStudiedAcademicYearId) {
            $_startToDelete = true;
        }
    }
}
foreach ((array)$studentDataAssoAry as $_studentId => $_studentDataAry)
{
    $_studentNotStudyingAllForm = true;
    foreach ((array) $_studentDataAry['classHistoryAry']['AcademicYearAssoAry'] as $__academicYearId => $__academicYearInfoAry) {
        $__isStudentStudiedThisYear = $__academicYearInfoAry['studentStudied'];
        if ($__isStudentStudiedThisYear) {
            $_studentNotStudyingAllForm = false;
            break;
        }
    }
    
    // Remove year data if student not studied
    if ($_studentNotStudyingAllForm) {
        unset($studentDataAssoAry[$_studentId]['classHistoryAry']);
    }
}
foreach ((array)$studentDataAssoAry as $_studentId => $_studentDataAry)
{
    $_dataAry = $_studentDataAry['classHistoryAry']['AcademicYearAssoAry'];
    foreach ((array)$_dataAry as $__academicYearId => $__academicYearInfoAry) {
        $__formId = $__academicYearInfoAry['formId'];
        $__formNumber = $__academicYearInfoAry['formNumber'];
        
        if ($studentDataAssoAry[$_studentId]['classHistoryAry']['firstFormId'] === '') {
            $studentDataAssoAry[$_studentId]['firstFormNumber'] = $__formNumber;
            $studentDataAssoAry[$_studentId]['firstAcademicYearId'] = $__academicYearId;
            
            $studentDataAssoAry[$_studentId]['classHistoryAry']['firstFormId'] = $__formId;
            $studentDataAssoAry[$_studentId]['classHistoryAry']['firstFormNumber'] = $__formNumber;
            $studentDataAssoAry[$_studentId]['classHistoryAry']['firstAcademicYearId'] = $__academicYearId;
        }
        
        $studentDataAssoAry[$_studentId]['classHistoryAry']['lastFormId'] = $__formId;
        $studentDataAssoAry[$_studentId]['classHistoryAry']['lastFormNumber'] = $__formNumber;
        $studentDataAssoAry[$_studentId]['classHistoryAry']['lastAcademicYearId'] = $__academicYearId;
    }
}

# Analyze Student Form Number History
$studentFormNumberAssoAry = array();
foreach ((array)$studentDataAssoAry as $_studentId => $_studentDataAry)
{
    $_dataAry = $_studentDataAry['classHistoryAry']['AcademicYearAssoAry'];
    foreach ((array)$_dataAry as $__academicYearId => $__academicYearInfoAry) {
        $__formId = $__academicYearInfoAry['formId'];
        $__formNumber = $__academicYearInfoAry['formNumber'];
        if(($isIBTranscriptReport && $__formNumber < 9) || ($isDBSTranscriptReport && $__formNumber < 7) || $__formNumber > 12) {
            continue;
        }
        
        $studentFormNumberAssoAry[$_studentId][$__formNumber][] = $__academicYearInfoAry;
    }
}

# Target Grade Mapping
$gradingMappingAry = $lpf_dbs->getIBGradeMapping();
$gradingMappingAry = BuildMultiKeyAssoc($gradingMappingAry, array("SubjectCode", "SubjectLevel", "IBGrade"));

# Target Subject Level
$studentSubjectLevelAry = $lpf_dbs->getSubjectLevel('', '', $studentIDAry);
$studentSubjectLevelAry = BuildMultiKeyAssoc($studentSubjectLevelAry, array('StudentID', 'SubjectID'), 'SubjectLevel', 1, 0);

# Target Subject Result
$sql = "SELECT
            asjr.UserID, asjr.AcademicYearID, IF(asjr.IsAnnual = 1, '0', asjr.YearTermID) as YearTermID,
            asjr.SubjectID, asj.EN_DES as SubjectName, asj.CODEID as SubjectCode, asjr.Score, asjr.Grade 
        FROM
            {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as asjr
            INNER JOIN {$intranet_db}.ASSESSMENT_SUBJECT as asj ON (asjr.SubjectID = asj.RecordID AND asj.RecordStatus = 1)
        WHERE
            asjr.UserID IN ('".implode("','", (array) $studentIDAry)."') AND  
            asj.CODEID != 'CLASS' AND 
            asjr.SubjectComponentCode IS NULL AND
            asjr.TermAssessment IS NULL
        ORDER BY
            asjr.UserID, asj.DisplayOrder ";
$studentSubjectResult = $libdb->returnResultSet($sql);
$studentSubjectResultAry = BuildMultiKeyAssoc((array)$studentSubjectResult, array("UserID", "AcademicYearID", "YearTermID", "SubjectID"));
$studentSubjectGroupedResultAry = BuildMultiKeyAssoc((array)$studentSubjectResult, array("UserID", "SubjectID", "AcademicYearID", "YearTermID"));

# Target Subject Group
$sql = "SELECT
            stcu.UserID, st.SubjectID, asj.EN_DES as SubjectName, asj.CODEID as SubjectCode, 'PENDING' as Grade, ayt.AcademicYearID, ayt.YearTermID, asj.DisplayOrder
        FROM
            SUBJECT_TERM_CLASS_USER as stcu
            INNER JOIN SUBJECT_TERM as st ON (st.SubjectGroupID = stcu.SubjectGroupID)
            INNER JOIN ASSESSMENT_SUBJECT as asj ON (st.SubjectID = asj.RecordID AND asj.RecordStatus = 1)
            INNER JOIN ACADEMIC_YEAR_TERM as ayt ON (st.YearTermID = ayt.YearTermID)
        WHERE
            stcu.UserID IN ('".implode("', '", (array)$studentIDAry)."') AND 
            asj.CODEID != 'CLASS'
        GROUP BY
            stcu.UserID, st.SubjectID, ayt.YearTermID
        ORDER BY
            stcu.UserID, st.SubjectID, ayt.TermStart";
$studentRelatedSubjectList = $libdb->returnResultSet($sql);
$studentRelatedSubjectAry = BuildMultiKeyAssoc((array)$studentRelatedSubjectList, array("UserID", "YearTermID", "SubjectID"), array("SubjectID", "SubjectName", "SubjectCode", "Grade", "DisplayOrder"), 1, 0);

# Get 1st Year Term
$allAcademicYearAry = array();
$allAcademicYearIDAry = array_values(BuildMultiKeyAssoc((array)$studentRelatedSubjectList, "AcademicYearID", "AcademicYearID", 1, 0));
$sql = "SELECT
            YearTermID, AcademicYearID
        FROM
            ACADEMIC_YEAR_TERM
        WHERE
            AcademicYearID IN ('".implode("', '", (array)$allAcademicYearIDAry)."')
        ORDER BY
            TermStart";
$academicYearTermAry = $libdb->returnResultSet($sql);
foreach($academicYearTermAry as $thisYearTermInfo) {
    $allAcademicYearAry[$thisYearTermInfo['AcademicYearID']][] = $thisYearTermInfo['YearTermID'];
}

# Get Student Predcited Grade
if($isIBTranscriptReport) {
    $predictedAdjustment = $lpf_dbs->getStudentAdjustMinMax('', $studentIDAry, '', '', '', $portfolioAdmin=true);
    $predictedAdjustment = BuildMultiKeyAssoc((array)$predictedAdjustment, array('StudentID', 'SubjectID'));
} else {
    $predictedAdjustment = $lpf_dbs->getStudentAdjustment('', $studentIDAry, '', '', '', '', $predictedType='0', $portfolioAdmin=true);
    $predictedAdjustment = BuildMultiKeyAssoc((array)$predictedAdjustment, array('StudentID', 'SubjectID', 'PredictPhase'));
}

# Report Images
$headerImageDisplay = '';
$backCoverImageDisplay = '';
$schoolSealImageDisplay = '';
$schoolSealStyleDisplay = '';
$signatureImageDisplay = array();
$signatureImageDisplay['L']['PG'] = '';
$signatureImageDisplay['L']['TS'] = '';
$signatureImageDisplay['R']['PG'] = '';
$signatureImageDisplay['R']['TS'] = '';
$signatureNameDisplay = array();
$signatureNameDisplay['L']['PG'] = '';
$signatureNameDisplay['L']['TS'] = '';
$signatureNameDisplay['R']['PG'] = '';
$signatureNameDisplay['R']['TS'] = '';
$signatureDetailsDisplay = array();
$signatureDetailsDisplay['L']['PG'] = '';
$signatureDetailsDisplay['L']['TS'] = '';
$signatureDetailsDisplay['R']['PG'] = '';
$signatureDetailsDisplay['R']['TS'] = '';
$signatureStyleDisplay = array();
$signatureStyleDisplay['L']['PG'] = '';
$signatureStyleDisplay['L']['TS'] = '';
$signatureStyleDisplay['R']['PG'] = '';
$signatureStyleDisplay['R']['TS'] = '';

# Header Image
$headerImage = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["header"], $headerImageID);
if($headerImageID > 0 && !empty($headerImage))
{
    $imageUrl = $PATH_WRT_ROOT.$ipf_cfg["DSBTranscript"]["IMAGE_DIRECTORY"].$headerImage[0]['ImageLink'];
    if(file_exists($imageUrl))
    {
        // [2020-0715-1454-06098] adjust header size
        //$headerImageDisplay = '<img src="'.$imageUrl.'" style="width: 180mm;">';
        //$headerImageDisplay = '<img src="'.$imageUrl.'" style="width: 171mm; max-height: 24mm;">';
        $headerImageDisplay = '<img src="'.$imageUrl.'" style="width: 176mm; max-height: 24mm;">';
    }
}

# Back Cover Image
$backCoverImage = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["back_cover"], $backCoverImageID);
if($backCoverImageID > 0 && !empty($backCoverImage))
{
    $imageUrl = $PATH_WRT_ROOT.$ipf_cfg["DSBTranscript"]["IMAGE_DIRECTORY"].$backCoverImage[0]['ImageLink'];
    if(file_exists($imageUrl))
    {
        $backCoverImageUrl = $imageUrl;

        // Get image width (in mm)
        $backCoverImageSize = getimagesize($backCoverImageUrl);
        $backCoverImageSize = $backCoverImageSize[0] * 0.264583333;

        // Get image page - x (in mm)
        $backCoverImageLeft = 0;
        /*
        if($backCoverImageSize > 0)
        {
            $backCoverImageLeft = round((210 - $backCoverImageSize) / 2);
            if($backCoverImageLeft < 0) {
                $backCoverImageLeft = 0;
            }
        }
        */
    }
}

# School Seal Image
$schoolSealImage = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["school_seal"], $schoolSealImageID);
if($schoolSealImageID > 0 && !empty($schoolSealImageID))
{
    $imageUrl = $PATH_WRT_ROOT.$ipf_cfg["DSBTranscript"]["IMAGE_DIRECTORY"].$schoolSealImage[0]['ImageLink'];
    if(file_exists($imageUrl))
    {
        $schoolSealImageDisplay = '<img src="'.$imageUrl.'" style="width: 31mm; max-height: 30mm;">';
    }
}
// $schoolSealStyleDisplay = $schoolSealImageDisplay == ''? ' style="border-bottom-color: white" ' : '';

# Signature 1 Image (Left)
$leftSignatureImage = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["signature"], $leftSignatureID);
if($leftSignatureID > 0 && !empty($leftSignatureImage))
{
    $imageUrl = $PATH_WRT_ROOT.$ipf_cfg["DSBTranscript"]["IMAGE_DIRECTORY"].$leftSignatureImage[0]['ImageLink'];
    if(file_exists($imageUrl))
    {
        $signatureImageDisplay['L']['TS'] = '<img src="'.$imageUrl.'" style="max-width: 40mm; max-height: 30mm;"/>';
    }
    $signatureNameDisplay['L']['TS'] = $leftSignatureImage[0]['SignatureName'];
    $signatureDetailsDisplay['L']['TS'] = $leftSignatureImage[0]['SignatureDetails'];
}
$signatureStyleDisplay['L']['TS'] = $signatureImageDisplay['L']['TS'] == ''? ' style="border-bottom-color: white" ' : '';

# Signature 1 Image (Right)
$rightSignatureImage = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["signature"], $rightSignatureID);
if($rightSignatureID > 0 && !empty($rightSignatureImage))
{
    $imageUrl = $PATH_WRT_ROOT.$ipf_cfg["DSBTranscript"]["IMAGE_DIRECTORY"].$rightSignatureImage[0]['ImageLink'];
    if(file_exists($imageUrl))
    {
        $signatureImageDisplay['R']['TS'] = '<img src="'.$imageUrl.'" style="max-width: 40mm; max-height: 30mm;"/>';
    }
    $signatureNameDisplay['R']['TS'] = $rightSignatureImage[0]['SignatureName'];
    $signatureDetailsDisplay['R']['TS'] = $rightSignatureImage[0]['SignatureDetails'];
}
$signatureStyleDisplay['R']['TS'] = $signatureImageDisplay['R']['TS'] == ''? ' style="border-bottom-color: white" ' : '';

# Signature 2 Image (Left)
$leftSignature2Image = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["signature"], $leftSignatureID2);
if($leftSignatureID2 > 0 && !empty($leftSignature2Image))
{
    $imageUrl = $PATH_WRT_ROOT.$ipf_cfg["DSBTranscript"]["IMAGE_DIRECTORY"].$leftSignature2Image[0]['ImageLink'];
    if(file_exists($imageUrl))
    {
        $signatureImageDisplay['L']['PG'] = '<img src="'.$imageUrl.'" style="max-width: 40mm; max-height: 30mm;"/>';
    }
    $signatureNameDisplay['L']['PG'] = $leftSignature2Image[0]['SignatureName'];
    $signatureDetailsDisplay['L']['PG'] = $leftSignature2Image[0]['SignatureDetails'];
}
$signatureStyleDisplay['L']['PG'] = $signatureImageDisplay['L']['PG'] == ''? ' style="border-bottom-color: white" ' : '';

# Signature 2 Image (Right)
$rightSignature2Image = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["signature"], $rightSignatureID2);
if($rightSignatureID2 > 0 && !empty($rightSignature2Image))
{
    $imageUrl = $PATH_WRT_ROOT.$ipf_cfg["DSBTranscript"]["IMAGE_DIRECTORY"].$rightSignature2Image[0]['ImageLink'];
    if(file_exists($imageUrl))
    {
        $signatureImageDisplay['R']['PG'] = '<img src="'.$imageUrl.'" style="max-width: 40mm; max-height: 30mm;"/>';
    }
    $signatureNameDisplay['R']['PG'] = $rightSignature2Image[0]['SignatureName'];
    $signatureDetailsDisplay['R']['PG'] = $rightSignature2Image[0]['SignatureDetails'];
}
$signatureStyleDisplay['R']['PG'] = $signatureImageDisplay['R']['PG'] == ''? ' style="border-bottom-color: white" ' : '';

# Initiate mPDF Object
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
// $pdf_obj = new mPDF('', 'A4', 0, '', 10, 10, 30, 1, 10);
$pdf_obj = new mPDF('', 'A4', 0, '', 10, 10, 34, 1, 12);
// $pdf_obj->setAutoTopMargin = 'stretch';
$pdf_obj->splitTableBorderWidth = 0.1;
//$pdf_obj->SetImportUse();

# Report Header
// [2020-0715-1454-06098] adjust header alignment
//$reportHeader = '<div style="text-align: center;padding-left:-7mm;"><span style="">'.$headerImageDisplay.'</span></div>';
$reportHeader = '<div style="text-align: center; padding-left: -1mm;"><span style="">'.$headerImageDisplay.'</span></div>';
$pdf_obj->DefHTMLHeaderByName("titleHeader", $reportHeader);
$pdf_obj->SetHTMLHeaderByName("titleHeader");
$pdf_obj->DefHTMLHeaderByName("emptyHeader", '');

# CSS
$pdf_obj->writeHTML("<head>");
$css = "<style TYPE='text/css'>
				body {
					font-family: \"Times New Roman\";
				}
                
                /* Report Header */
				table.report_header {
					font-size: 11.5pt;
                    margin: auto;
				}
				table.report_header td {
					text-align: left;
					line-height: 3.8mm;
				}
                table.report_header td:nth-child(even) {
                    /*font-weight: bold;*/
                }
				table.report_header td.report_title {
                    font-weight: bold;
					line-height: 8.5mm;
					text-align: center;
					vertical-align: top;
				}

				table.dbs_report_header td {
					text-align: left;
					line-height: 3.5mm;
				}
                table.dbs_report_header td.report_title {
					line-height: 8.5mm;
                }
                
                /* Predicted Report */
                table.predicted_report_content {
					font-size: 11pt;
					line-height: 8mm;
                    padding-left: 2mm;
                }
                table.predicted_report_content td.content_td {
                    height: 120.5mm;
                }

                table.intro_content td {
					font-size: 10pt;
                    text-align: justify;
                }
                
                /* Transcript Report */
                table.main_report_content {
                    border-bottom: 0.5px solid black;
				}
                table.main_report_content td.content_td {
                    border-right: 0.5px solid black;
                    height: 147.5mm;
                }
                
                /* Transcript - DSE Report Content */
                table.dse_report_content {
					font-size: 10pt;
					line-height: 4mm;
                    height: 190mm;
                    border-bottom: 0.5px solid black;
				}
                table.dse_report_content td {
                    line-height: 2.6mm;
					text-align: center;
                    vertical-align: top;
                    border-right: 0.5px solid black;
				}
                table.dse_report_content tr.dse_table_header td {
                    font-weight: bold;
                    line-height: 2.6mm;
				}
                
                /* Transcript - IB Report Content */
                table.ib_report_content {
					font-size: 10pt;
					line-height: 4.5mm;
                }
                table.ib_report_content td:nth-child(2), table.ib_report_content td:nth-child(3) {
					text-align: center;
                }
                
                /* Report Remarks */
                table.report_remarks td {
					font-size: 8pt;
                    font-style: italic;
                    padding: 0;
				}
                
                /* Report Columns */
                table td.table_col1 {
					vertical-align: bottom;
                    font-weight: bold;
				}
                table td.table_col2 {
					font-size: 7pt;
                    font-weight: bold;
					line-height: 3mm;
                    text-align: center;
				}

                /* Predicted Report - Report Columns */
                table.predicted_report_content td {
					font-size: 12pt;
                    font-weight: bold;
                    text-align: left;
                }
                table.predicted_report_content tr.core_row td {
					line-height: 4mm;
                }
                table.predicted_report_content tr.summary_row td {
					line-height: 10mm;
                }
                table.predicted_report_content td.table_col1 {
					font-size: 10pt;
                    font-style: italic;
                    text-align: left;
                    padding-bottom: 2mm;
				}
                table.predicted_report_content td.table_col2 {
					font-size: 10pt;
                    font-style: italic;
                    text-align: center;
                    padding-bottom: 2mm;
				}
                
                /* Report Signature */
                table.signature_table td {
                    text-align: center;
				}
                table.signature_table tr.signature_image_row td {
                    text-align: center;
                    vertical-align: bottom;
				}
                table.signature_table tr.signature_type_row td {
                    vertical-align: top;
				}
                table td.report_signature {
                    border-bottom: 1px solid black;
                    text-align: center;
                    vertical-align: bottom;
                    height: 38mm;
				}
                table td.report_signature_remarks {
                    font-size: 9pt;
                    vertical-align: top;
                    padding-top: -5px;
                    height: 12mm;
				}
                table td.report_signature_remarks_2 {
                    font-size: 7pt;
                    font-style: italic;
					text-align: right;
                    border-top: 0.5px solid black;
				}
                table td.pg_report_signature {
                    height: 39.5mm;
				}

                .dotted_seperator {
                    font-size: 2mm;
                    font-weight: normal;
                    padding: 0;
                    margin: 0;
                    height: 1mm;
                    line-height: 1mm;
                    letter-spacing: 4px;
                }
			</style>";
$pdf_obj->writeHTML($css);
$pdf_obj->writeHTML("</head>");

# Report Title
//$TranscriptReportTitle = $isIBTranscriptReport? 'INTERNATIONAL BACCALAUREATE SCHOOL TRANSCRIPT' : 'OFFICIAL TRANSCRIPT';
//$PredictedGradeReportTitle = $isIBTranscriptReport? 'INTERNATIONAL BACCALAUREATE PREDICTED GRADES' : 'HONG KONG DIPLOMA OF SECONDARY EDUCATION PREDICTED GRADES';
$TranscriptReportTitle = 'SCHOOL TRANSCRIPT';
$PredictedGradeReportTitle = 'PREDICTED GRADES';

# Report Grade Table
$gradeLevelAry = array();
if($isIBTranscriptReport)
{
    $gradeLevelAry[] = array(9);
    $gradeLevelAry[] = array(10);
    $gradeLevelAry[] = array(11, 12);
}
else if($isDBSTranscriptReport)
{
//     $gradeLevelAry[] = array(9, 10);
//     $gradeLevelAry[] = array(11, 12);
    $gradeLevelAry[] = array(7, 8, 9);
    $gradeLevelAry[] = array(10, 11, 12);

    // remove not selected form
    foreach((array)$gradeLevelAry as $thisRow => $thisRowGradeLevel) {
        foreach($thisRowGradeLevel as $thisCol => $thisGradeLevel) {
            if(!in_array($thisGradeLevel, $reportFormAry)) {
                unset($gradeLevelAry[$thisRow][$thisCol]);
            }
        }
        $gradeLevelAry[$thisRow] = array_values($gradeLevelAry[$thisRow]);
    }

    foreach((array)$gradeLevelAry as $thisRow => $thisRowGradeLevel) {
        // remove entire if no valid form
        if(empty($thisRowGradeLevel)) {
            unset($gradeLevelAry[$thisRow]);
        }
        // add empty col if removed not selected forms before
        else if(count($thisRowGradeLevel) < 3) {
            for($i=count($thisRowGradeLevel); $i<3; $i++) {
                $gradeLevelAry[$thisRow][] = 20 + $i;
            }
        }
    }
    $gradeLevelAry = array_values($gradeLevelAry);
}
else
{
    $gradeLevelAry[] = array(7, 8, 9, 10, 11);
    $gradeLevelAry[] = array(12, 13, 14, 15, 16);       // > 12 for empty col only

    // remove not selected form
    foreach((array)$gradeLevelAry as $thisRow => $thisRowGradeLevel) {
        foreach($thisRowGradeLevel as $thisCol => $thisGradeLevel) {
            if(!in_array($thisGradeLevel, $reportFormAry)) {
                unset($gradeLevelAry[$thisRow][$thisCol]);
            }
        }
        $gradeLevelAry[$thisRow] = array_values($gradeLevelAry[$thisRow]);
    }

    foreach((array)$gradeLevelAry as $thisRow => $thisRowGradeLevel) {
        // remove entire if no valid form
        if(empty($thisRowGradeLevel)) {
            unset($gradeLevelAry[$thisRow]);
        }
        // add empty col if removed not selected forms before
        else if(count($thisRowGradeLevel) < 5) {
            for($i=count($thisRowGradeLevel); $i<5; $i++) {
                $gradeLevelAry[$thisRow][] = 20 + $i;
            }
        }
    }
    $gradeLevelAry = array_values($gradeLevelAry);
}

# Report Remarks
if($isIBTranscriptReport)
{
    $remarksContent = 'The predicted grades and overall diploma score are provided here as an indication of how a student may perform on the
                        final International Baccalaureate examinations and assessments. Predicted grades are based on each subject teacher\'s
                        estimation of the student\'s most recent and most consistent performance in both formative and summative assessments
                        throughout the course. The student\'s actual achievement on the final examinations may differ depending on the
                        student\'s efforts and on the interpretation of IB marking schemes by external examiners and moderators.';
}
else
{
    $remarksContent = 'The predicted grades are provided here as an indication of how a student may perform on the final Hong Kong
                        Diploma of Secondary Education (HKDSE) examinations and assessments. Predicted grades are based on each
                        subject teacher\'s estimation of the student\'s most recent and most consistent performance in both formative and
                        summative assessments throughout the course. The student\'s actual achievement on the final examinations may
                        differ depending on the student\'s efforts and on the interpretation of HKDSE marking schemes by external
                        examiners and on statistical moderation of results. Please refer to our school profile for a description of the grades
                        awarded.';
}

# Report Issue Date
$thisIssueDate = date('j F Y');

# Report Table Style
$reportHeaderDivStyle = $isIBTranscriptReport? ' style="text-align:center" ' : '';
//$reportHeaderWidthStyle = $isIBTranscriptReport? ' width="90%" ' : ' width="100%" ';
$reportHeaderWidthStyle = ' width="99%" ';

# Predicted Grade Table Style
$reportTableHeightStyle = $isIBTranscriptReport? ' style="height: 124.5mm" ' : '';
$reportTitleHeightStyle = $isDBSTranscriptReport? ' style="line-height: 10.5mm" ' : '';

$report_header_class = $isIBTranscriptReport? '' : ' dbs_report_header ';
$report_table_class = $isIBTranscriptReport? '' : ' dbs_report_table ';

# Report Table
$pdf_obj->writeHTML("<body>");

// loop students
foreach((array)$studentInfoAry as $thisStudentCount => $thisStudentInfo)
{
    $studentDOB = '---';
    if(!is_date_empty($thisStudentInfo['DateOfBirth'])) {
        $studentDOB = date('d/m/Y', strtotime($thisStudentInfo['DateOfBirth']));
    }
    $studentAdmissionDate = '---';
    if(!is_date_empty($thisStudentInfo['AdmissionDate'])) {
        $studentAdmissionDate = date('F Y', strtotime($thisStudentInfo['AdmissionDate']));
    }
    $studentGraduationDate = '---';
    if(!is_date_empty($thisStudentInfo['GraduationDate'])) {
        $studentGraduationDate = date('F Y', strtotime($thisStudentInfo['GraduationDate']));
    }
    
    # Report - Predicted Grade
    if(in_array($ipf_cfg["DSBTranscript"]["REPORT_TYPE"]["PredictedGrade"], $reportTypeAry))
    {
        // Report - Student Info
        $reportStudentInfo = '';
        $reportStudentInfo .= '<div '.$reportHeaderDivStyle.'>
                                    <table class="report_header" '.$reportHeaderWidthStyle.'>
                                        <tr>
                                            <td class="report_title" colspan="4" '.$reportTitleHeightStyle.'><u>'.$PredictedGradeReportTitle.'</u></td>
                                        </tr>
                                        <tr>
                                            <td>Name:</td>
                                            <td colspan="3">&nbsp;'.$thisStudentInfo['StudentName'].'</td>
                                        </tr>
                                        <tr>
                                            <td width="28.5%">Date of Birth (dd/mm/yyy):</td>
                                            <td width="27.5%">&nbsp;'.$studentDOB.'</td>
                                            <td width="26%">Grade:</td>
                                            <td width="17.5%">'.$thisStudentInfo['YearNumber'].'</td>
                                        </tr>
                                        <tr>
                                            <td>Date of Entry:</td>
                                            <td>&nbsp;'.$studentAdmissionDate.'</td>
                                            <td>Withdrawal/Graduation:</td>
                                            <td>'.$studentGraduationDate.'</td>
                                        </tr>';
//         if(!$isIBTranscriptReport) {
//             $reportStudentInfo .= '     <tr>
//                                             <td>Curriculum:</td>
//                                             <td colspan="3">'.$targetCurriculum['NameEn'].' ('.$targetCurriculum['Code'].')'.'</td>
//                                          </tr>';
//         }
        $reportStudentInfo .= '     </table>
                                    <hr style="margin: 1.5mm 0; color: black"/>
                                </div>';
        $pdf_obj->writeHTML($reportStudentInfo);
        
        // Report - Introduction
        $introductionContent = '<div width="100%">
                                    <table class="intro_content" width="100%" align="center">
                                        <tr><td width="100%">'.$remarksContent.'</td></tr>
                                    </table>
                                </div>';
        $pdf_obj->writeHTML($introductionContent);
//         debug_pr($predictedAdjustment);//die();

        // Report - Predicted Grade Result
        $reportPredictedResult = '';
        if($isIBTranscriptReport)
        {
            $SubjectTable = '<table class="predicted_report_content" width="70%">
                                <tr>
                                    <!--
                                    <td class="table_col1" width="40%">
                                        <span style="font-size:12pt">S</span>UBJECT
                                    </td>
                                    <td class="table_col1" width="22%">
                                        <span style="font-size:12pt">L</span>EVEL
                                    </td>
                                    <td class="table_col2" width="38%">
                                        <span style="font-size:12pt">P</span>REDICTED <span style="font-size:12pt">G</span>RADE
                                    </td>
                                    -->
                                    <td class="table_col1" width="62%">
                                        <span style="font-size:12pt">S</span>UBJECT
                                    </td>
                                    <td class="table_col2" width="38%">
                                        <span style="font-size:12pt">P</span>REDICTED <span style="font-size:12pt">G</span>RADE
                                    </td>
                                </tr>';
            
            $CorePoints = 0;
            $CoreTotals = 0;
            $LevelPoints = 0;
            $LevelTotals = 0;
            $TotalPoints = 0;
            $TotalTotals = 0;
            
            $CoreSubjectRow = '';
            $LevelSubjectRow = '';

            // [2020-0220-1624-36098] added subject name sorting
            $studentAllRelatedSubjectAry = $studentRelatedSubjectAry[$thisStudentInfo['UserID']][$thisAcademicTermID];
            if(is_array($studentAllRelatedSubjectAry) && !empty($studentAllRelatedSubjectAry))
            {
                $studentAllRelatedSubjectAry = array_values($studentAllRelatedSubjectAry);
                $studentAllRelatedSubjectAry = sortByColumn((array)$studentAllRelatedSubjectAry, 'SubjectName');
            }

            // loop Subjects
            foreach((array)$studentAllRelatedSubjectAry as $thisRelatedSubjectInfo)
            {
                // Subject Level
                $thisRelatedSubjectID = $thisRelatedSubjectInfo['SubjectID'];
                $thisRelatedSubjectLevel = $studentSubjectLevelAry[$thisStudentInfo['UserID']][$thisRelatedSubjectID];
                $thisSubjectLevelDisplay = $thisRelatedSubjectLevel == 'HL'? 'Higher' : 'Standard';
                
                // Subject Predicted Grades
                $thisPredictedGradeDisplay = '<i>Pending</i>';
                $thisSubjectPredictedGrade = $predictedAdjustment[$thisStudentInfo['UserID']][$thisRelatedSubjectID];
                if(!empty($thisSubjectPredictedGrade))
                {
                    $predictedFinalGrade = trim($thisSubjectPredictedGrade['PredictedGrade']);
                    $predictedGradeMax = trim($thisSubjectPredictedGrade['PredictedGradeMax']);
                    $thisPredictedGradeDisplay = ($predictedFinalGrade != '' && $predictedFinalGrade != 'NC')? $predictedFinalGrade : ($predictedGradeMax != ''? $predictedGradeMax : $thisPredictedGradeDisplay);
                }
                
                // Core Subjects
                $isCoreSubject = $thisRelatedSubjectLevel == '';
                if($isCoreSubject)
                {
                    $thisSubjectLevelDisplay = 'Core';

                    $CoreSubjectRow.= ' <tr class="core_row">
                                            <td style="font-weight: normal">'.$thisRelatedSubjectInfo['SubjectName'].'</td>
                                            <!--<td style="font-weight: normal">'.$thisSubjectLevelDisplay.'</td>-->
                                            <td align="center">'.$thisPredictedGradeDisplay.'</td>
                                        </tr>';

                    $CorePoints += (int)$thisPredictedGradeDisplay;
                    $TotalPoints += (int)$thisPredictedGradeDisplay;
                    $CoreTotals += 7;
                    $TotalTotals += 7;
                }
                // Subjects with Levels
                else
                {
                    $LevelSubjectRow .= '<tr>
                                            <td>'.$thisRelatedSubjectInfo['SubjectName'].'</td>
                                            <!--<td>'.$thisSubjectLevelDisplay.'</td>-->
                                            <td align="center">'.$thisPredictedGradeDisplay.'</td>
                                        </tr>';
                    
                    $LevelPoints += (int)$thisPredictedGradeDisplay;
                    $TotalPoints += (int)$thisPredictedGradeDisplay;
                    $LevelTotals += 7;
                    $TotalTotals += 7;
                }
            }
            
            // Rows - Subjects with Levels
            if($LevelSubjectRow != '')
            {
                $SubjectTable .= $LevelSubjectRow;
                $SubjectTable .= ' <tr class="summary_row">
                                        <!--<td colspan="2" align="right">Total Subject Points (out of '.$LevelTotals.'):</td>-->
                                        <td align="right">Total Subject Points (out of '.$LevelTotals.'):</td>
                                        <td align="center">'.$LevelPoints.'</td>
                                    </tr>';
            }
            
            // Rows - Core Subjects
            if($CoreSubjectRow != '')
            {
                $SubjectTable .= $CoreSubjectRow;
                $SubjectTable .= ' <tr class="summary_row">
                                        <!--<td colspan="2" align="right">Total Core Points (out of '.$CoreTotals.'):</td>-->
                                        <td align="right">Total Core Points (out of '.$CoreTotals.'):</td>
                                        <td align="center">'.$CorePoints.'</td>
                                    </tr>';
            }
            
            // Rows - Total Points
            $SubjectTable .= ' <tr class="summary_row">
                                    <!--<td colspan="2" align="right">Total IB Diploma Points (out of '.$TotalTotals.'):</td>-->
                                    <td align="right">Total IB Diploma Points (out of '.$TotalTotals.'):</td>
                                    <td align="center">'.$TotalPoints.'</td>
                                </tr>';
            $SubjectTable .= '</table>';
        }
        else
        {
            $SubjectTable = '<table class="predicted_report_content" width="70%" >
                                <tr>
                                    <td class="table_col1" width="60%">
                                        <span style="font-size:12pt">S</span>UBJECT
                                    </td>
                                    <td class="table_col2" width="40%">
                                        <span style="font-size:12pt">P</span>REDICTED <span style="font-size:12pt">G</span>RADE
                                    </td>
                                </tr>';

            // [2020-0220-1624-36098] added subject name sorting
            $studentAllRelatedSubjectAry = $studentRelatedSubjectAry[$thisStudentInfo['UserID']][$thisAcademicTermID];
            if(is_array($studentAllRelatedSubjectAry) && !empty($studentAllRelatedSubjectAry))
            {
                $studentAllRelatedSubjectAry = array_values($studentAllRelatedSubjectAry);
                $studentAllRelatedSubjectAry = sortByColumn((array)$studentAllRelatedSubjectAry, 'SubjectName');
            }

            // loop Subjects
            foreach((array)$studentAllRelatedSubjectAry as $thisRelatedSubjectInfo)
            {
                $thisRelatedSubjectID = $thisRelatedSubjectInfo['SubjectID'];
                
                // Subject Predicted Grades
                $thisPredictedGradeDisplay = '<i>Pending</i>';
                $thisSubjectPredictedGrade = $predictedAdjustment[$thisStudentInfo['UserID']][$thisRelatedSubjectID];
                if(!empty($thisSubjectPredictedGrade))
                {
                    $predictedPhase1Grade = trim($thisSubjectPredictedGrade[1]['PredictedGrade']);
                    $predictedPhase2Grade = trim($thisSubjectPredictedGrade[2]['PredictedGrade']);
                    $predictedPhase3Grade = trim($thisSubjectPredictedGrade[3]['PredictedGrade']);
                    $predictedPhase4Grade = trim($thisSubjectPredictedGrade[4]['PredictedGrade']);
                    
                    if($predictedPhase4Grade != '' && $predictedPhase4Grade != 'NC') {
                        $thisPredictedGradeDisplay = $predictedPhase4Grade;
                    } else if($predictedPhase3Grade != '' && $predictedPhase3Grade != 'NC') {
                        $thisPredictedGradeDisplay = $predictedPhase3Grade;
                    } else if($predictedPhase2Grade != '' && $predictedPhase2Grade != 'NC') {
                        $thisPredictedGradeDisplay = $predictedPhase2Grade;
                    } else if($predictedPhase1Grade != '' && $predictedPhase1Grade != 'NC') {
                        $thisPredictedGradeDisplay = $predictedPhase1Grade;
                    }
                }
                if($isDBSTranscriptReport)
                {
                    $spaceDisplaySize = 3 - strlen($thisPredictedGradeDisplay);
                    for ($i=0; $i<$spaceDisplaySize; $i++) {
                        $thisPredictedGradeDisplay .= '<span style="visibility: hidden">*</span>';
                    }
                }
                
                $SubjectTable.= '<tr>
                                    <td>'.$thisRelatedSubjectInfo['SubjectName'].'</td>
                                    <td align="center">'.$thisPredictedGradeDisplay.'</td>
                                </tr>';
            }
            $SubjectTable .= '</table>';
        }
        $SubjectTable .= '<table class="predicted_report_content" width="100%"> ';
            $SubjectTable .= '<tr>';
                $SubjectTable .= '<td class="dotted_seperator" align="center">.........................................................................</td> ';
            $SubjectTable .= '</tr>';
        $SubjectTable .= '</table>';
        
        $reportPredictedResult .= '<div>
                                    <table class="predicted_report_content" width="100%" align="center">
                                        <tr>
                                            <td class="content_td" width="100%" align="center" valign="top" '.$reportTableHeightStyle.'>'.$SubjectTable.'</td>
                                        </tr>
                                    </table>
                                </div>';
        $pdf_obj->writeHTML($reportPredictedResult);
        //     debug_pr($reportSubjectResult);
        
        // Signatrue
        $reportSignature = '<div width="100%">
                                <table width="100%" class="signature_table">
                                    <tr class="signature_image_row">
                                        <td width="25%">
                                            <table width="90%">
                                                <tr><td class="report_signature pg_report_signature" '.$signatureStyleDisplay['L']['PG'].'>'.$signatureImageDisplay['L']['PG'].'</td></tr>
                                            </table>
                                        </td>
                                        <td width="25%">
                                            <table width="90%">
                                                <tr><td class="report_signature pg_report_signature" '.$signatureStyleDisplay['R']['PG'].'>'.$signatureImageDisplay['R']['PG'].'</td></tr>
                                            </table>
                                        </td>
                                        <td width="25%">
                                            <table width="90%">
                                                <!-- <tr><td class="report_signature pg_report_signature" '.($isDBSTranscriptReport? ' style="border-bottom-color: white" ' : $schoolSealStyleDisplay).'>'.($isDBSTranscriptReport? '' : $schoolSealImageDisplay).'</td></tr> -->
                                                <tr><td class="report_signature pg_report_signature" '.$schoolSealStyleDisplay.'>'.$schoolSealImageDisplay.'</td></tr>
                                            </table>
                                        </td>
                                        <td width="25%">
                                            <table width="90%">
                                                <tr><td class="report_signature pg_report_signature">'.$thisIssueDate.'</td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="signature_type_row">
                                        <td>
                                            <table width="90%">
                                                <tr><td class="report_signature_remarks">
                                                    '.$signatureNameDisplay['L']['PG'].'<br/>
                                                    '.nl2br($signatureDetailsDisplay['L']['PG']).'
                                                </td></tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table width="90%">
                                                <tr><td class="report_signature_remarks">
                                                    '.$signatureNameDisplay['R']['PG'].'<br/>
                                                    '.nl2br($signatureDetailsDisplay['R']['PG']).'
                                                </td></tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table width="90%">
                                                <!-- <tr><td class="report_signature_remarks">'.($isDBSTranscriptReport? '' : 'School Seal').'</td></tr> -->
                                                <tr><td class="report_signature_remarks">School Seal</td></tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table width="90%">
                                                <tr><td class="report_signature_remarks">Date of Issue</td></tr>
                                            </table>
                                        </td>
                                    </tr><tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td class="report_signature_remarks_2" colspan="2">
                                            Inquiries may be directed to the Headmaster or the Centre for Further Studies counsellor.<br/>
                                            This document is not official unless signed and stamped with the school seal.
                                        </td>
                                    </tr>
                                </table>
                            </div>';
        $pdf_obj->writeHTML($reportSignature);
    }
    
    # Report - Transcript
    if(in_array($ipf_cfg["DSBTranscript"]["REPORT_TYPE"]["Transcript"], $reportTypeAry))
    {
        // Page break
        if(in_array($ipf_cfg["DSBTranscript"]["REPORT_TYPE"]["PredictedGrade"], $reportTypeAry))
        {
            $html = "<pagebreak />";
            $pdf_obj->writeHTML($html);
        }
        
        // Report - Student Info
        $reportStudentInfo = '';
        $reportStudentInfo .= '<div '.$reportHeaderDivStyle.'>
                                    <table class="report_header '.$report_header_class.'" '.$reportHeaderWidthStyle.'>
                                        <tr>
                                            <td class="report_title" colspan="4"><u>'.$TranscriptReportTitle.'</u></td>
                                        </tr>
                                        <tr>
                                            <td>Name:</td>
                                            <td colspan="3">&nbsp;'.$thisStudentInfo['StudentName'].'</td>
                                        </tr>
                                        <tr>
                                            <td width="28.5%">Date of Birth (dd/mm/yyy):</td>
                                            <td width="27.5%">&nbsp;'.$studentDOB.'</td>
                                            <td width="26%">Grade:</td>
                                            <td width="18%">'.$thisStudentInfo['YearNumber'].'</td>
                                        </tr>
                                        <tr>
                                            <td>Date of Entry:</td>
                                            <td>&nbsp;'.$studentAdmissionDate.'</td>
                                            <td>Withdrawal/Graduation:</td>
                                            <td>'.$studentGraduationDate.'</td>
                                        </tr>';
        if($isDBSTranscriptReport) {
            $reportStudentInfo .= '     <tr>
                                            <td>Curriculum:</td>
                                            <td colspan="3">&nbsp;'.$targetCurriculum['NameEn'].' ('.$targetCurriculum['Code'].')'.'</td>
                                         </tr>';
        }
        $reportStudentInfo .= '     </table>
                                    <hr style="margin: 1.5mm 0; color: black"/>
                                </div>';
        $pdf_obj->writeHTML($reportStudentInfo);
        
        // Report - Subject Result
        $reportSubjectResult = '';
        if($isIBTranscriptReport)
        {
            $reportTableAry = array();
            foreach($gradeLevelAry as $thisLevelIndex => $thisLevelAry)
            {
                $SubjectTable = '';
                
                $thisGradeNumber = $thisLevelAry[0];
                if($thisGradeNumber == 9 || $thisGradeNumber == 10)
                {
                    if(!in_array($thisGradeNumber, $reportFormAry)) {
                        continue;
                    }

                    $thisStudentFormNumberAry = $studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][0];
                    $thisStudentSubjectResultAry = $studentSubjectResultAry[$thisStudentInfo['UserID']][$thisStudentFormNumberAry['academicYearId']];
                    
                    $thisStudentGradeDisplayResult = array();
                    $isApplyFinalResult = isset($thisStudentSubjectResultAry[0]) && !empty($thisStudentSubjectResultAry[0]);
                    if($isApplyFinalResult) {
                        $thisStudentGradeDisplayResult = $thisStudentSubjectResultAry[0];
                    } else {
                        $firstYearTermID = $allAcademicYearAry[$thisStudentFormNumberAry['academicYearId']][0];
                        if($firstYearTermID > 0 && isset($thisStudentSubjectResultAry[$firstYearTermID]) && !empty($thisStudentSubjectResultAry[$firstYearTermID])) {
                            $thisStudentGradeDisplayResult = $thisStudentSubjectResultAry[$firstYearTermID];
                        }
                        else if ($thisStudentFormNumberAry['academicYearId'] != Get_Current_Academic_Year_ID()) {
                            $isApplyFinalResult = true;
                        }
                    }
                    
                    $targetTermIndex = $isApplyFinalResult? 1 : 0;
                    $targetYearTermID = $allAcademicYearAry[$thisStudentFormNumberAry['academicYearId']][$targetTermIndex];
                    $studentAllRelatedSubjectAry = $studentRelatedSubjectAry[$thisStudentInfo['UserID']][$targetYearTermID];
                    if($targetYearTermID > 0 && !empty($studentAllRelatedSubjectAry)) {
                        if(empty($thisStudentGradeDisplayResult)) {
                            $thisStudentGradeDisplayResult = $studentAllRelatedSubjectAry;
                        } else {
                            $extraSubjectDisplayResult = array();
                            $studentAllRelatedSubjectIDAry = array_keys((array)$thisStudentGradeDisplayResult);
                            foreach($studentAllRelatedSubjectAry as $relatedSubjectID => $relatedSubjectInfo) {
                                if(!in_array($relatedSubjectID, (array)$studentAllRelatedSubjectIDAry)) {
                                    $extraSubjectDisplayResult[$relatedSubjectID] = $relatedSubjectInfo;
                                }
                            }
                            $thisStudentGradeDisplayResult = $thisStudentGradeDisplayResult + $extraSubjectDisplayResult;
                        }
                    }
                    
                    $studyTypeTitle = $thisGradeNumber == 9? 'Hong Kong Junior Secondary<br/> School Curriculum' : 'Bridging Programme<br/>&nbsp;';
                    $GradeTitle = $isApplyFinalResult? 'Final' :'Mid-year';
                    
                    $SubjectTable = '<table class="ib_report_content" width="100%">
                                        <tr>
                                            <td colspan="2" style="line-height: 2.5mm; font-size: 11pt"><b>Grade '.$thisGradeNumber.'</b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="line-height: 3mm"><b>'.($thisStudentFormNumberAry['academicYearName']? $thisStudentFormNumberAry['academicYearName'] : '---').'</b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="line-height: 3.8mm"><b>'.$studyTypeTitle.'</b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="line-height: 0.1mm">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="table_col1" width="78%" valign="bottom">Subject</td>
                                            <td class="table_col2" width="22%" valign="bottom">'.$GradeTitle.'<br/>Grade</td>
                                        </tr>';
                    
                    $hasValidSubjectRow = false;
                    if(!empty($thisStudentGradeDisplayResult))
                    {
                        foreach((array)$thisStudentGradeDisplayResult as $thisSubjectID => $thisStudentSubjectData)
                        {
                            if($thisStudentSubjectData['Score'] == -1) {
                                continue;
                            } else if ($thisStudentSubjectData['Grade'] == 'PENDING') {
                                $thisStudentSubjectData['Grade'] = '<span style="font-size:8pt;"><i>Pending</i></span>';
                            } else if ($thisStudentSubjectData['Grade'] == '') {
                                $thisStudentSubjectData['Grade'] = '---';
                            }
                            
                            if($thisGradeNumber == 10)
                            {
                                $thisStudentIBFormNumberAry = $studentFormNumberAssoAry[$thisStudentInfo['UserID']]['11'][0];
                                $thisStudentIBSubjectResultAry = $studentSubjectResultAry[$thisStudentInfo['UserID']][$thisStudentIBFormNumberAry['academicYearId']];
                                if(isset($thisStudentIBSubjectResultAry[0]) && !empty($thisStudentIBSubjectResultAry[0]) && isset($thisStudentIBSubjectResultAry[0][$thisSubjectID])) {
                                    $thisStudentIBGradeResult = $thisStudentIBSubjectResultAry[0][$thisSubjectID];
                                    
                                    $thisIBSubjectLevel = $studentSubjectLevelAry[$thisStudentInfo['UserID']][$thisSubjectID];
                                    $thisIBSubjectLevel = isset($thisIBSubjectLevel) && $thisIBSubjectLevel != ''? $thisIBSubjectLevel : '';
                                    
                                    $thisIBSubjectCode = $ipf_cfg["DSBTranscript"]["SUBJECT_MAPPING"][$thisStudentSubjectData['SubjectCode']];
                                    $thisIBSubjectMapping = $gradingMappingAry[$thisIBSubjectCode][$thisIBSubjectLevel];
                                    if(empty($thisIBSubjectMapping)) {
                                        $thisIBSubjectMapping = $gradingMappingAry[$thisIBSubjectCode][''];
                                    }
                                    if(empty($thisIBSubjectMapping)) {
                                        $thisIBSubjectMapping = $gradingMappingAry['ALL'][''];
                                    }
                                    
                                    if($thisStudentIBGradeResult['Score'] != -1 && $thisStudentIBGradeResult['Grade'] != 'PENDING' && $thisStudentIBGradeResult['Grade'] != '') {
//                                         // for 149 testing only
//                                         switch ($thisStudentIBGradeResult['Grade']) {
//                                             case 'A+':
//                                                 $thisStudentIBGradeResult['Grade'] = 7;
//                                                 break;
//                                             case 'A':
//                                                 $thisStudentIBGradeResult['Grade'] = 6;
//                                                 break;
//                                             case 'B':
//                                                 $thisStudentIBGradeResult['Grade'] = 5;
//                                                 break;
//                                             case 'C':
//                                                 $thisStudentIBGradeResult['Grade'] = 3;
//                                                 break;
//                                         }
                                        
                                        $thisStudentSubjectData['Grade'] = $thisIBSubjectMapping[$thisStudentIBGradeResult['Grade']]['TargetGrade'];
                                    }
                                }
                            }
                            
                            $SubjectTable .= '
                                        <tr>
                                            <td>'.$thisStudentSubjectData['SubjectName'].'</td>
                                            <td>'.$thisStudentSubjectData['Grade'].'</td>
                                        </tr>';
                            $hasValidSubjectRow = true;
                        }
                    }
                    
                    if(!$hasValidSubjectRow) {
                        $SubjectTable .= '
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>';
                    }
                    $SubjectTable .= '</table>';
                }
                else
                {
                    $SubjectTable = '';
                    foreach($thisLevelAry as $thisGradeIndex => $thisGradeNumber) {
                        if(!in_array($thisGradeNumber, $reportFormAry)) {
                            continue;
                        }

                        $thisStudentFormNumberAry = $studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][0];
                        $thisStudentSubjectResultAry = $studentSubjectResultAry[$thisStudentInfo['UserID']][$thisStudentFormNumberAry['academicYearId']];
                        
                        $thisStudentGradeDisplayResult = array();
                        $isApplyFinalResult = isset($thisStudentSubjectResultAry[0]) && !empty($thisStudentSubjectResultAry[0]);
                        if($isApplyFinalResult) {
                            $thisStudentGradeDisplayResult = $thisStudentSubjectResultAry[0];
                        } else {
                            $firstYearTermID = $allAcademicYearAry[$thisStudentFormNumberAry['academicYearId']][0];
                            if($firstYearTermID > 0 && isset($thisStudentSubjectResultAry[$firstYearTermID]) && !empty($thisStudentSubjectResultAry[$firstYearTermID])) {
                                $thisStudentGradeDisplayResult = $thisStudentSubjectResultAry[$firstYearTermID];
                            }
                            else if ($thisStudentFormNumberAry['academicYearId'] != Get_Current_Academic_Year_ID()) {
                                $isApplyFinalResult = true;
                            }
                        }
                        
                        $targetTermIndex = $isApplyFinalResult? 1 : 0;
                        $targetYearTermID = $allAcademicYearAry[$thisStudentFormNumberAry['academicYearId']][$targetTermIndex];
                        $studentAllRelatedSubjectAry = $studentRelatedSubjectAry[$thisStudentInfo['UserID']][$targetYearTermID];
                        if($targetYearTermID > 0 && !empty($studentAllRelatedSubjectAry)) {
                            if(empty($thisStudentGradeDisplayResult)) {
                                $thisStudentGradeDisplayResult = $studentAllRelatedSubjectAry;
                            } else {
                                $extraSubjectDisplayResult = array();
                                $studentAllRelatedSubjectIDAry = array_keys((array)$thisStudentGradeDisplayResult);
                                foreach($studentAllRelatedSubjectAry as $relatedSubjectID => $relatedSubjectInfo) {
                                    if(!in_array($relatedSubjectID, (array)$studentAllRelatedSubjectIDAry)) {
                                        $extraSubjectDisplayResult[$relatedSubjectID] = $relatedSubjectInfo;
                                    }
                                }
                                $thisStudentGradeDisplayResult = $thisStudentGradeDisplayResult + $extraSubjectDisplayResult;
                            }
                        }
                        
                        $subjectTableTitle = '';
                        $subjectTableHeader = '';
                        $subjectTableBorderStyle = '';

                        //$thisStudentNextFormNumberAry = $studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisLevelAry[1]][0];
                        $subjectTableTitle = '  <tr>
                                                    <td colspan="3" style="line-height: 2.5mm; font-size: 11pt"><b>Grade '.$thisGradeNumber.'</b></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" style="line-height: 2.5mm"><b>'.($thisStudentFormNumberAry['academicYearName'] ? $thisStudentFormNumberAry['academicYearName'] : '---').'</b></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" style="line-height: 3.8mm"><b>International Baccalaureate<br/>Diploma Programme</b></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" style="line-height: 0.1mm">&nbsp;</td>
                                                </tr> ';
                        //$subjectTableHeader = 'Subject';
                        //$subjectTableHeader = 'Grade '.$thisLevelAry[0].' Subjects';
                        //$subjectTableBorderStyle = ' style="border-bottom: 1px solid black" ';
                        //else
                        //{
                        //    $subjectTableTitle = '  <tr>
                        //                                <td colspan="3" style="line-height: 0.1mm">&nbsp;</td>
                        //                            </tr>
                        //                            <tr>
                        //                                <td colspan="3" style="line-height: 0.1mm">&nbsp;</td>
                        //                            </tr> ';
                        //    $subjectTableHeader = 'Grade '.$thisLevelAry[1].' Subjects';
                        //}
                        $GradeTitle = $isApplyFinalResult? 'Final' :'Mid-year';
                        
                        if($SubjectTable == '') {
                            $SubjectTable .= '<table class="ib_report_content" width="100%" '.$subjectTableBorderStyle.'>';
                        }
                        $SubjectTable .= $subjectTableTitle.'
                                            <tr>
                                                <td class="table_col1" width="63%" valign="bottom">Subject</td>
                                                <td class="table_col2" width="15%" valign="bottom">Level</td>
                                                <td class="table_col2" width="22%" valign="bottom">'.$GradeTitle.'<br/>Grade</td>
                                            </tr>';
                        
                        $hasValidSubjectRow = false;
                        if(!empty($thisStudentGradeDisplayResult))
                        {
                            foreach((array)$thisStudentGradeDisplayResult as $thisSubjectID => $thisStudentSubjectData)
                            {
                                if($thisStudentSubjectData['Score'] == -1) {
                                    continue;
                                } else if($thisStudentSubjectData['Grade'] == "PENDING") {
                                    $thisStudentSubjectData['Grade'] = '<span style="font-size:8pt;"><i>Pending</i></span>';
                                } else if ($thisStudentSubjectData['Grade'] == '') {
                                    $thisStudentSubjectData['Grade'] = '---';
                                }
                                $thisSubjectLevel = $studentSubjectLevelAry[$thisStudentInfo['UserID']][$thisSubjectID];
                                $thisSubjectLevel = isset($thisSubjectLevel) && $thisSubjectLevel != ''? $thisSubjectLevel : '<span style="font-size:7pt;">CORE</span>';
                                
                                $SubjectTable .= '
                                        <tr>
                                            <td>'.$thisStudentSubjectData['SubjectName'].'</td>
                                            <td>'.$thisSubjectLevel.'</td>
                                            <td>'.$thisStudentSubjectData['Grade'].'</td>
                                        </tr>';
                                $hasValidSubjectRow = true;
                            }
                        }
                        
                        if(!$hasValidSubjectRow) {
                            $SubjectTable .= '
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>';
                        }
                        if($thisGradeIndex == 0) {
                            $SubjectTable .= '  <tr>
                                                    <td colspan="3" style="line-height: 0.1mm">&nbsp;</td>
                                                </tr> ';
                        }
                    }
                    if($SubjectTable != '') {
                        $SubjectTable .= '</table>';
                    }
                }
                $reportTableAry[$thisLevelIndex] .= $SubjectTable;
            }
            
            $reportSubjectResult.= '<div>
                                        <table class="main_report_content" width="100%" align="center">
                                            <tr>
                                                <td class="content_td" width="32%" valign="top">'.$reportTableAry[0].'</td>
                                                <td class="content_td" width="32%" valign="top">'.$reportTableAry[1].'</td>
                                                <td width="36%" valign="top">'.$reportTableAry[2].'</td>
                                            </tr>
                                        </table>
                                    </div>';
        }
        else
        {
            $tableSubjectRow = array();
            foreach($gradeLevelAry as $thisLevelIndex => $thisLevelAry)
            {
                foreach((array)$studentSubjectGroupedResultAry[$thisStudentInfo['UserID']] as $thisSubjectID => $subjectGroupedResultAry)
                {
                    $subjectRow = array();
                    $isFirstColumn = true;
                    $hasValidSubjectRow = false;
                    foreach($thisLevelAry as $thisGradeIndex => $thisGradeNumber)
                    {
                        $thisStudentFormNumberAry = $studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][0];
                        $thisStudentSubjectResultAry = $subjectGroupedResultAry[$thisStudentFormNumberAry['academicYearId']];
                        $thisStudentSubjectResultOriAry = $studentSubjectResultAry[$thisStudentInfo['UserID']][$thisStudentFormNumberAry['academicYearId']];
                        
                        $thisStudentGradeDisplayResult = array();
                        $isApplyFinalResult = isset($thisStudentSubjectResultOriAry[0]) && !empty($thisStudentSubjectResultOriAry[0]);
                        if($isApplyFinalResult) {
                            $thisStudentGradeDisplayResult = $thisStudentSubjectResultAry[0];
                        } else {
                            $firstYearTermID = $allAcademicYearAry[$thisStudentFormNumberAry['academicYearId']][0];
                            if($firstYearTermID > 0 && isset($thisStudentSubjectResultAry[$firstYearTermID]) && !empty($thisStudentSubjectResultAry[$firstYearTermID])) {
                                $thisStudentGradeDisplayResult = $thisStudentSubjectResultAry[$firstYearTermID];
                            }
                            else if ($thisStudentFormNumberAry['academicYearId'] != Get_Current_Academic_Year_ID()) {
                                $isApplyFinalResult = true;
                            }
                        }

                        if($isFirstColumn || $subjectRow['Name'] == '')
                        {
                            $subjectRow['Name'] = $thisStudentGradeDisplayResult['SubjectName'];
                            $isFirstColumn = false;
                        }
                        
                        $thisStudentGradeDisplayGrade = '---';
                        if(!empty($thisStudentGradeDisplayResult))
                        {
                            if($thisStudentGradeDisplayResult['Score'] == -1 || $thisStudentGradeDisplayResult['Grade'] == '') {
                                // do nothing
                            }
                            else {
                                $thisStudentGradeDisplayGrade = $thisStudentGradeDisplayResult['Grade'];
                                if ($thisStudentGradeDisplayGrade== "PENDING") {
                                    $thisStudentGradeDisplayGrade= '<span style="font-size:8pt;"><i>Pending</i></span>';
                                }
                                $hasValidSubjectRow = true;
                            }
                        }
                        
                        $subjectRow[$thisGradeIndex][0] = $thisStudentGradeDisplayGrade;
                        
                        $isRetented = isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][1]);
                        if($isDBSTranscriptReport && $isRetented)
                        {
                            $thisStudentFormNumberAry = $studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][1];
                            $thisStudentSubjectResultAry = $subjectGroupedResultAry[$thisStudentFormNumberAry['academicYearId']];
                            $thisStudentSubjectResultOriAry = $studentSubjectResultAry[$thisStudentInfo['UserID']][$thisStudentFormNumberAry['academicYearId']];
                            
                            $thisStudentGradeDisplayResult = array();
                            $isApplyFinalResult = isset($thisStudentSubjectResultOriAry[0]) && !empty($thisStudentSubjectResultOriAry[0]);
                            if($isApplyFinalResult) {
                                $thisStudentGradeDisplayResult = $thisStudentSubjectResultAry[0];
                            } else {
                                $firstYearTermID = $allAcademicYearAry[$thisStudentFormNumberAry['academicYearId']][0];
                                if($firstYearTermID > 0 && isset($thisStudentSubjectResultAry[$firstYearTermID]) && !empty($thisStudentSubjectResultAry[$firstYearTermID])) {
                                    $thisStudentGradeDisplayResult = $thisStudentSubjectResultAry[$firstYearTermID];
                                }
                                else if ($thisStudentFormNumberAry['academicYearId'] != Get_Current_Academic_Year_ID()) {
                                    $isApplyFinalResult = true;
                                }
                            }
                            
                            if($isFirstColumn || $subjectRow['Name'] == '')
                            {
                                $subjectRow['Name'] = $thisStudentGradeDisplayResult['SubjectName'];
                                $isFirstColumn = false;
                            }
                            
                            $thisStudentGradeDisplayGrade = '---';
                            if(!empty($thisStudentGradeDisplayResult))
                            {
                                if($thisStudentGradeDisplayResult['Score'] == -1 || $thisStudentGradeDisplayResult['Grade'] == '') {
                                    // do nothing
                                }
                                else {
                                    $thisStudentGradeDisplayGrade = $thisStudentGradeDisplayResult['Grade'];
                                    if ($thisStudentGradeDisplayGrade== "PENDING") {
                                        $thisStudentGradeDisplayGrade= '<span style="font-size:8pt;"><i>Pending</i></span>';
                                    }
                                    $hasValidSubjectRow = true;
                                }
                            }
                            
                            $subjectRow[$thisGradeIndex][1] = $thisStudentGradeDisplayGrade;
                        }
                    }
                    
                    if($hasValidSubjectRow) {
                        if(!isset($tableSubjectRow[$thisLevelIndex][$thisSubjectID])) {
                            $tableSubjectRow[$thisLevelIndex][$thisSubjectID] = array();
                        }
                        $tableSubjectRow[$thisLevelIndex][$thisSubjectID] = $tableSubjectRow[$thisLevelIndex][$thisSubjectID] + $subjectRow;
                        
//                         if($subjectRow['Name'] == '') {
//                             debug_pr($thisStudentGradeDisplayResult);
//                         }
                    }
                }
            }
            
            $tableContent = '';
            
            if($isDBSTranscriptReport)
            {
                $tableContent .= '<tr>';
                $tableContent .= '<td width="25%" style="line-height: 0; border: 0; padding: 0;"></td>';
                foreach((array)$gradeLevelAry[0] as $thisGradeIndex => $thisGradeNumber)
                {
                    $nextLevelGradeNumber = $gradeLevelAry[1][$thisGradeIndex];

                    //$colHasRetented = isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][1]) || isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][($thisGradeNumber + 3)][1]);
                    $colHasRetented = isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][1]) || isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][$nextLevelGradeNumber][1]);
                    if($colHasRetented) {
                        $tableContent .= '<td width="12.5%" style="line-height: 0; border: 0; padding: 0;"></td>';
                        $tableContent .= '<td width="12.5%" style="line-height: 0; border: 0; padding: 0;"></td>';
                    }
                    else {
                        $tableContent .= '<td width="25%" style="line-height: 0; border: 0; padding: 0;"></td>';
                    }
                }
            }
            else
            {
                $tableContent .= '<tr>';
                $tableContent .= '<td width="30%" style="line-height: 0; border: 0; padding: 0;"></td>';
                foreach((array)$gradeLevelAry[0] as $thisGradeIndex => $thisGradeNumber)
                {
                    $tableContent .= '<td width="14%" style="line-height: 0; border: 0; padding: 0;"></td>';
                }
            }
            
            $tableContent .= '</tr>';
            foreach($gradeLevelAry as $thisLevelIndex => $thisLevelAry)
            {
                $tableContent .= '<tr class="dse_table_header">';
                    if($isDBSTranscriptReport)
                    {
                        $tableContent .= '<td>&nbsp;</td>';
                    }
                    else {
                        $exam_type = $thisLevelIndex==0? 'Hong Kong Certificate of Education Examination (HKCEE)' : 'Hong Kong Advanced Level Examination (HKALE)';
                        $tableContent .= '<td rowspan="2" align="left" style="line-height: 3.6mm">'.$exam_type.'</td>';
                    }
                    foreach($thisLevelAry as $thisGradeIndex => $thisGradeNumber)
                    {
                        if($isDBSTranscriptReport)
                        {
                            $isRetented = isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][1]);
                            $checkGradeNumber = $thisLevelIndex == 0 ? $gradeLevelAry[1][$thisGradeIndex] : $gradeLevelAry[0][$thisGradeIndex];

                            //$colHasRetented = $thisLevelIndex == 0? isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][($thisGradeNumber + 3)][1]) : isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][($thisGradeNumber - 3)][1]);
                            $colHasRetented = isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][$checkGradeNumber][1]);
                            $colspan = $isRetented || $colHasRetented? ' colspan="2" ' : '';
                        }
                        
                        $isLastTableCol = $isDBSTranscriptReport && $thisGradeIndex == 2 || !$isDBSTranscriptReport && $thisGradeIndex == 4;
                        $lastTableColStyle = $isLastTableCol? ' style="border: 0;" ' : '';
                        
                        if($isDBSTranscriptReport)
                        {
                            if($thisGradeNumber > 13) {
                                $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>&nbsp;</td>';
                            }
                            else {
                                $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>Grade '.$thisGradeNumber.'</td>';
                            }
                        }
                        else {
                            if($thisGradeNumber > 13) {
                                $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>&nbsp;</td>';
                            }
                            else if($thisGradeNumber >= 12) {
                                $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>Form '.($thisGradeNumber == 12 ? 'Lower' : 'Upper').' 6</td>';
                            }
                            else {
                                $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>Form '.($thisGradeNumber - 6).'</td>';
                            }
                        }
                    }
                $tableContent .= '</tr>';
                
                $tableContent .= '<tr class="dse_table_header">';
                    if($isDBSTranscriptReport)
                    {
                        $tableContent .= '<td>&nbsp;</td>';
                    }
                    foreach($thisLevelAry as $thisGradeIndex => $thisGradeNumber)
                    {
                        $thisStudentFormNumberAry = $studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][0];
                        $thisStudentSubjectResultAry = $subjectGroupedResultAry[$thisStudentInfo['UserID']][$thisStudentFormNumberAry['academicYearId']];
                        
                        if($isDBSTranscriptReport)
                        {
                            $isRetented = isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][1]);
                            $checkGradeNumber = $thisLevelIndex == 0 ? $gradeLevelAry[1][$thisGradeIndex] : $gradeLevelAry[0][$thisGradeIndex];

                            $colHasRetented = isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][$checkGradeNumber][1]);
                            $colspan = !$isRetented && $colHasRetented? ' colspan="2" ' : '';
                        }
                        
                        $isLastTableCol = $isDBSTranscriptReport && $thisGradeIndex == 2 || !$isDBSTranscriptReport && $thisGradeIndex == 4;
                        $lastTableColStyle = $isRetented || $isLastTableCol? ' style="border: 0;" ' : '';
                        if($thisGradeNumber > 13) {
                            $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>&nbsp;</td>';
                        }
                        else {
                            $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>'.($thisStudentFormNumberAry['academicYearName']? $thisStudentFormNumberAry['academicYearName'] : '---').'</td>';
                        }
                        
                        if($isDBSTranscriptReport && $isRetented)
                        {
                            $thisStudentFormNumberAry = $studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][1];
                            $thisStudentSubjectResultAry = $subjectGroupedResultAry[$thisStudentInfo['UserID']][$thisStudentFormNumberAry['academicYearId']];
                            
                            $isLastTableCol = $isDBSTranscriptReport && $thisGradeIndex == 2 || !$isDBSTranscriptReport && $thisGradeIndex == 4;
                            $lastTableColStyle = $isLastTableCol? ' style="border: 0;" ' : '';
                            $tableContent .= '<td '.$lastTableColStyle.'>'.($thisStudentFormNumberAry['academicYearName']? $thisStudentFormNumberAry['academicYearName'] : '---').'</td>';
                        }
                    }
                $tableContent .= '</tr>';
                
                $tableContent .= '<tr class="dse_table_header">';
                    $tableContent .= '<td style="text-align: left;">Subject</td>';
                    foreach($thisLevelAry as $thisGradeIndex => $thisGradeNumber)
                    {
                        $thisStudentFormNumberAry = $studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][0];
                        $thisStudentSubjectResultOriAry = $studentSubjectResultAry[$thisStudentInfo['UserID']][$thisStudentFormNumberAry['academicYearId']];
                        $isApplyFinalResult = isset($thisStudentSubjectResultOriAry[0]) && !empty($thisStudentSubjectResultOriAry[0]);
                        if(!$isApplyFinalResult) {
                            $firstYearTermID = $allAcademicYearAry[$thisStudentFormNumberAry['academicYearId']][0];
                            if($firstYearTermID > 0 && isset($thisStudentSubjectResultOriAry[$firstYearTermID]) && !empty($thisStudentSubjectResultOriAry[$firstYearTermID])) {
                                // do nothing
                            }
                            else if ($thisStudentFormNumberAry['academicYearId'] != Get_Current_Academic_Year_ID()) {
                                $isApplyFinalResult = true;
                            }
                        }
                        $GradeTitle = $isApplyFinalResult? 'Final' :'Mid-year';
                        
                        $colspan = '';
                        $isRetented = false;
                        if($isDBSTranscriptReport)
                        {
                            $isRetented = isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][1]);
                            $checkGradeNumber = $thisLevelIndex == 0 ? $gradeLevelAry[1][$thisGradeIndex] : $gradeLevelAry[0][$thisGradeIndex];

                            $colHasRetented = isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][$checkGradeNumber][1]);
                            $colspan = !$isRetented && $colHasRetented? ' colspan="2" ' : '';
                        }

                        // [2020-0715-1454-06098] handle Mid-year Grade title display if retented
						$GradeTitle = '<u>'.$GradeTitle.' Grade</u>';
						if(!$isApplyFinalResult && $isRetented) {
						    $GradeTitle = '<u>Mid-year</u><br/><span style="vertical-align: -14px;"><u>Grade</u></span>';
						}
						
                        $isLastTableCol = $isDBSTranscriptReport && $thisGradeIndex == 2 || !$isDBSTranscriptReport && $thisGradeIndex == 4;
                        $lastTableColStyle = ($isRetented || $isLastTableCol) ? ' style="border: 0;" ' : '';
                        if($thisGradeNumber > 13) {
                            $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>&nbsp;</td>';
                        }
                        else {
                            //$tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'><u>'.$GradeTitle.' Grade</u></td>';
                            $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>'.$GradeTitle.' </td>';
                        }
                        
                        if($isDBSTranscriptReport && $isRetented)
                        {
                            $thisStudentFormNumberAry = $studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][1];
                            $thisStudentSubjectResultOriAry = $studentSubjectResultAry[$thisStudentInfo['UserID']][$thisStudentFormNumberAry['academicYearId']];
                            $isApplyFinalResult = isset($thisStudentSubjectResultOriAry[0]) && !empty($thisStudentSubjectResultOriAry[0]);
                            if(!$isApplyFinalResult) {
                                $firstYearTermID = $allAcademicYearAry[$thisStudentFormNumberAry['academicYearId']][0];
                                if($firstYearTermID > 0 && isset($thisStudentSubjectResultOriAry[$firstYearTermID]) && !empty($thisStudentSubjectResultOriAry[$firstYearTermID])) {
                                    // do nothing
                                }
                                else if ($thisStudentFormNumberAry['academicYearId'] != Get_Current_Academic_Year_ID()) {
                                    $isApplyFinalResult = true;
                                }
                            }
                            $GradeTitle = $isApplyFinalResult ? 'Final' :'Mid-year';

                            // [2020-0715-1454-06098] handle Mid-year Grade display if retented
                            $GradeTitle = '<u>'.$GradeTitle.' Grade</u>';
							if(!$isApplyFinalResult) {
								$GradeTitle = '<u>Mid-year</u><br/><span style="vertical-align: -14px;"><u>Grade</u></span>';
							}
                            
                            $isLastTableCol = $thisGradeIndex == 2;
                            $lastTableColStyle = $isLastTableCol? ' style="border: 0;" ' : '';
                            //$tableContent .= '<td '.$lastTableColStyle.'><u>'.$GradeTitle.' Grade</u></td>';
                            $tableContent .= '<td '.$lastTableColStyle.'>'.$GradeTitle.' </td>';
                        }
                    }
                $tableContent .= '</tr>';
                
                foreach((array)$tableSubjectRow[$thisLevelIndex] as $thisSubjectID => $thisSubjectRow)
                {
                    $tableContent .= '<tr>';

                        // [2020-0715-1454-06098] handle if subject name too long
					    $subject_line_height = strlen($thisSubjectRow['Name']) > 29 ? " line-height: 13.5px; vertical-align: top; " : "";
                        $tableContent .= '<td style="text-align: left; '.$subject_line_height.'">'.$thisSubjectRow['Name'].'</td>';
                        
                        foreach($thisLevelAry as $thisGradeIndex => $thisGradeNumber)
                        {
                            if($isDBSTranscriptReport)
                            {
                                $isRetented = isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][1]);
                                $checkGradeNumber = $thisLevelIndex == 0 ? $gradeLevelAry[1][$thisGradeIndex] : $gradeLevelAry[0][$thisGradeIndex];

                                $colHasRetented = isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][$checkGradeNumber][1]);
                                $colspan = !$isRetented && $colHasRetented? ' colspan="2" ' : '';
                            }
                            
                            $isLastTableCol = $isDBSTranscriptReport && $thisGradeIndex == 2 || !$isDBSTranscriptReport && $thisGradeIndex == 4;
                            $lastTableColStyle = ($isRetented || $isLastTableCol) ? ' style="border: 0;" ' : '';
                            
                            if($thisGradeNumber > 13) {
                                $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>&nbsp;</td>';
                            }
                            else {
                                $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>'.$thisSubjectRow[$thisGradeIndex][0].'</td>';
                            }
                            
                            if($isDBSTranscriptReport && $isRetented)
                            {
                                $isLastTableCol = $thisGradeIndex == 2;
                                $lastTableColStyle = $isLastTableCol ? ' style="border: 0;" ' : '';
                                $tableContent .= '<td '.$lastTableColStyle.'>'.$thisSubjectRow[$thisGradeIndex][1].'</td>';
                            }
                        }
                    $tableContent .= '</tr>';
                }
                
//                 // for 146 > more subject row display
//                 foreach((array)$tableSubjectRow[$thisLevelIndex] as $thisSubjectID => $thisSubjectRow)
//                 {
//                     $tableContent .= '<tr>';
//                     $tableContent .= '<td style="text-align: left;">'.$thisSubjectRow['Name'].'</td>';
//                     foreach($thisLevelAry as $thisGradeIndex => $thisGradeNumber)
//                     {
//                         if($isDBSTranscriptReport)
//                         {
//                             $isRetented = isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][1]);
//                             $colHasRetented = $thisLevelIndex == 0? isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][($thisGradeNumber + 3)][1]) : isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][($thisGradeNumber - 3)][1]);
//                             $colspan = !$isRetented && $colHasRetented? ' colspan="2" ' : '';
//                         }
//                        
//                         $isLastTableCol = $isDBSTranscriptReport && $thisGradeIndex == 2 || !$isDBSTranscriptReport && $thisGradeIndex == 4;
//                         $lastTableColStyle = $isRetented || $isLastTableCol? ' style="border: 0;" ' : '';
//                         if($thisGradeNumber > 13) {
//                             $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>&nbsp;</td>';
//                         }
//                         else {
//                             $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>'.$thisSubjectRow[$thisGradeIndex][0].'</td>';
//                         }
//                        
//                         if($isDBSTranscriptReport && $isRetented)
//                         {
//                             $isLastTableCol = $thisGradeIndex == 2;
//                             $lastTableColStyle = $isLastTableCol? ' style="border: 0;" ' : '';
//                             $tableContent .= '<td '.$lastTableColStyle.'>'.$thisSubjectRow[$thisGradeIndex][1].'</td>';
//                         }
//                     }
//                     $tableContent .= '</tr>';
//                 }
//                 $tableContent .= '<tr>';
//                 $tableContent .= '<td style="text-align: left;">'.$thisSubjectRow['Name'].'</td>';
//                 foreach($thisLevelAry as $thisGradeIndex => $thisGradeNumber)
//                 {
//                     if($isDBSTranscriptReport)
//                     {
//                         $isRetented = isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][1]);
//                         $colHasRetented = $thisLevelIndex == 0? isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][($thisGradeNumber + 3)][1]) : isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][($thisGradeNumber - 3)][1]);
//                         $colspan = !$isRetented && $colHasRetented? ' colspan="2" ' : '';
//                     }
//                    
//                     $isLastTableCol = $isDBSTranscriptReport && $thisGradeIndex == 2 || !$isDBSTranscriptReport && $thisGradeIndex == 4;
//                     $lastTableColStyle = $isRetented || $isLastTableCol? ' style="border: 0;" ' : '';
//                     if($thisGradeNumber > 13) {
//                         $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>&nbsp;</td>';
//                     }
//                     else {
//                         $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>'.$thisSubjectRow[$thisGradeIndex][0].'</td>';
//                     }
//                    
//                     if($isDBSTranscriptReport && $isRetented)
//                     {
//                         $isLastTableCol = $thisGradeIndex == 2;
//                         $lastTableColStyle = $isLastTableCol? ' style="border: 0;" ' : '';
//                         $tableContent .= '<td '.$lastTableColStyle.'>'.$thisSubjectRow[$thisGradeIndex][1].'</td>';
//                     }
//                 }
//                 $tableContent .= '</tr>';
//                 $tableContent .= '<tr>';
//                 $tableContent .= '<td style="text-align: left;">'.$thisSubjectRow['Name'].'</td>';
//                 foreach($thisLevelAry as $thisGradeIndex => $thisGradeNumber)
//                 {
//                     if($isDBSTranscriptReport)
//                     {
//                         $isRetented = isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][1]);
//                         $colHasRetented = $thisLevelIndex == 0? isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][($thisGradeNumber + 3)][1]) : isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][($thisGradeNumber - 3)][1]);
//                         $colspan = !$isRetented && $colHasRetented? ' colspan="2" ' : '';
//                     }
//                    
//                     $isLastTableCol = $isDBSTranscriptReport && $thisGradeIndex == 2 || !$isDBSTranscriptReport && $thisGradeIndex == 4;
//                     $lastTableColStyle = $isRetented || $isLastTableCol? ' style="border: 0;" ' : '';
//                     if($thisGradeNumber > 13) {
//                         $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>&nbsp;</td>';
//                     }
//                     else {
//                         $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>'.$thisSubjectRow[$thisGradeIndex][0].'</td>';
//                     }
//                    
//                     if($isDBSTranscriptReport && $isRetented)
//                     {
//                         $isLastTableCol = $thisGradeIndex == 2;
//                         $lastTableColStyle = $isLastTableCol? ' style="border: 0;" ' : '';
//                         $tableContent .= '<td '.$lastTableColStyle.'>'.$thisSubjectRow[$thisGradeIndex][1].'</td>';
//                     }
//                 }
//                 $tableContent .= '</tr>';   
                
                $tableContent .= '<tr>';
                    $tableContent .= '<td>&nbsp;</td>';
                    foreach($thisLevelAry as $thisGradeIndex => $thisGradeNumber)
                    {
                        if($isDBSTranscriptReport)
                        {
                            $isRetented = isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][$thisGradeNumber][1]);
                            $checkGradeNumber = $thisLevelIndex == 0 ? $gradeLevelAry[1][$thisGradeIndex] : $gradeLevelAry[0][$thisGradeIndex];

                            $colHasRetented = isset($studentFormNumberAssoAry[$thisStudentInfo['UserID']][$checkGradeNumber][1]);
                            $colspan = !$isRetented && $colHasRetented? ' colspan="2" ' : '';
                        }
                        
                        $isLastTableCol = $isDBSTranscriptReport && $thisGradeIndex == 2 || !$isDBSTranscriptReport && $thisGradeIndex == 4;
                        $lastTableColStyle = $isRetented || $isLastTableCol? ' style="border: 0;" ' : '';
                        if($thisGradeNumber > 13) {
                            $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>&nbsp;</td>';
                        }
                        else {
                            $tableContent .= '<td '.$lastTableColStyle.' '.$colspan.'>&nbsp;</td>';
                        }
                        
                        if($isDBSTranscriptReport && $isRetented)
                        {
                            $isLastTableCol = $thisGradeIndex == 2;
                            $lastTableColStyle = $isLastTableCol? ' style="border: 0;" ' : '';
                            $tableContent .= '<td '.$lastTableColStyle.'>&nbsp;</td>';
                        }
                    }
                $tableContent .= '</tr>';
            }
            
            $reportSubjectResult.= '<div height="143.5mm">
                                        <table class="dse_report_content" width="100%" align="center" cellpadding="3" cellspacing="0">
                                           '.$tableContent.'
                                        </table>
                                    </div>';
        }
        $pdf_obj->writeHTML($reportSubjectResult);
		
        // Signatrue
        $reportSignature = '<div width="100%">
                                <table width="100%" class="signature_table">
                                    <tr class="signature_image_row">
                                        <td width="25%">
                                            <table width="90%">
                                                <tr><td class="report_signature" '.$signatureStyleDisplay['L']['TS'].'>'.$signatureImageDisplay['L']['TS'].'</td></tr>
                                            </table>
                                        </td>
                                        <td width="25%">
                                            <table width="90%">
                                                <tr><td class="report_signature" '.$signatureStyleDisplay['R']['TS'].'>'.$signatureImageDisplay['R']['TS'].'</td></tr>
                                            </table>
                                        </td>
                                        <td width="25%">
                                            <table width="90%">
                                                <tr><td class="report_signature" '.$schoolSealStyleDisplay.'>'.$schoolSealImageDisplay.'</td></tr>
                                            </table>
                                        </td>
                                        <td width="25%">
                                            <table width="90%">
                                                <tr><td class="report_signature">'.$thisIssueDate.'</td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="signature_type_row">
                                        <td>
                                            <table width="90%">
                                                <tr><td class="report_signature_remarks">
                                                    '.$signatureNameDisplay['L']['TS'].'<br/>
                                                    '.nl2br($signatureDetailsDisplay['L']['TS']).'
                                                </td></tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table width="90%">
                                                <tr><td class="report_signature_remarks">
                                                    '.$signatureNameDisplay['R']['TS'].'<br/>
                                                    '.nl2br($signatureDetailsDisplay['R']['TS']).'
                                                </td></tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table width="90%">
                                                <tr><td class="report_signature_remarks">School Seal</td></tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table width="90%">
                                                <tr><td class="report_signature_remarks">Date of Issue</td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td class="report_signature_remarks_2" colspan="2">
                                            Please turn overleaf for a description of the curriculum and of the grades and marks awarded.<br/>
                                            Inquiries may be directed to the Headmaster or the Centre for Further Studies counsellor.<br/>
                                            This transcript is not official unless signed and stamped with the school seal.
                                        </td>
                                    </tr>
                                </table>
                            </div>';
        $pdf_obj->writeHTML($reportSignature);
    }

    // Back Cover Page
    if($backCoverImageUrl != '')
    {
        // Set Empty Header
        $pdf_obj->SetHTMLHeaderByName("emptyHeader");

        // Page break
        $html = "<pagebreak />";
        $pdf_obj->writeHTML($html);

        // Back Cover page
        //$reportBackCover = '<div align="center" valign="middle" style="position: absolute; top: 0mm; left: 0mm;">'.$backCoverImageDisplay.'</div>';
        //$pdf_obj->Image($backCoverImageUrl, $backCoverImageLeft, 0, 0, 0, 'jpg', '', true, false);
        $pdf_obj->Image($backCoverImageUrl, 0, 0, 210, 297, 'jpg', '', true, false);

        /*
        $pagesInFile = $pdf_obj->SetSourceFile('K3.pdf');
        for ($i = 1; $i <= $pagesInFile; $i++) {
            $pdf_obj->AddPage();
            $tplId = $pdf_obj->ImportPage($i); // in mPdf v8 should be 'importPage($i)'
            $pdf_obj->UseTemplate($tplId, '', '', 210, 297);
        }
        */

        // Set Original Header
        $pdf_obj->SetHTMLHeaderByName("titleHeader");

        $pdf_obj->writeHTML('');
    }
    
    if(($thisStudentCount + 1) < $student_count)
    {
        $html = "<pagebreak />";
        $pdf_obj->writeHTML($html);
    }
}

$pdf_obj->writeHTML("</body>");

# Export PDF file
$pdf_obj->Output('StudentReport.pdf', 'I');

?>