<?php
// Modified by
/*
 * Modification Log:
 *  Date: 2018-08-16 (Bill)
 *      - Create file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript_ui.php");

iportfolio_auth("T");
intranet_opendb();

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();

$linterface = new interface_html();
$lpf_dbs_ui = new libpf_dbs_transcript_ui();

$html["reportTypeSelection"] = $linterface->Get_Checkbox('ReportTypePG', 'ReportType[]', $ipf_cfg["DSBTranscript"]["REPORT_TYPE"]["PredictedGrade"], $isChecked=1, $Class='', $Lang['iPortfolio']['DBSTranscript']['PredictedGrade'], $Onclick='updateSignatureRow(\'PG\', this.checked)', $Disabled='');
$html["reportTypeSelection"] .= '&nbsp;';
$html["reportTypeSelection"] .= $linterface->Get_Checkbox('ReportTypeTS', 'ReportType[]', $ipf_cfg["DSBTranscript"]["REPORT_TYPE"]["Transcript"], $isChecked=1, $Class='', $Lang['iPortfolio']['DBSTranscript']['Transcript'], $Onclick='updateSignatureRow(\'TS\', this.checked)', $Disabled='');

$html["curriculumSelection"] = $lpf_dbs_ui->getCurriculumSelection('curriculumID', '', $onchange='reloadReportFields()', $noFirst=1, $firstTitleText='', $useCode=true);

$html["yearSelection"] = $lpf_dbs_ui->getAcademicYearClassHistorySelection('targetYearID', '', $onChange='targetArchiveYearChanged(this)', $noFirst=0, $firstTitleText='');

$CurriculumArr = $lpf_dbs->getCurriculumSetting();
$defaultCurriculumID = $CurriculumArr[0]['CurriculumID'];
$backCoverImages = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["back_cover"], $imageID='', $defaultCurriculumID);
$headerImages = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["header"], $imageID='', $defaultCurriculumID);
$schoolSealImages = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["school_seal"], $imageID='', $defaultCurriculumID);
$signatureImages = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["signature"], $imageID='', $defaultCurriculumID, $ipf_cfg["DSBTranscript"]["REPORT_TYPE"]["Transcript"]);
$signatureImages2 = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["signature"], $imageID='', $defaultCurriculumID, $ipf_cfg["DSBTranscript"]["REPORT_TYPE"]["PredictedGrade"]);

$backCoverAry = array();
foreach((array)$backCoverImages as $thisImage) {
    $backCoverAry[] = array($thisImage['ImageID'], $thisImage['Description']);
}
$html["backCoverSelection"] = getSelectByArray($backCoverAry, " id='backCoverImageID' name ='backCoverImageID' ", '', $isAll=0, $noFirst=0, $firstTitle='---');

$headerAry = array();
foreach((array)$headerImages as $thisImage) {
    $headerAry[] = array($thisImage['ImageID'], $thisImage['Description']);
}
$html["headerSelection"] = getSelectByArray($headerAry, " id='headerImageID' name ='headerImageID' ", '', $isAll=0, $noFirst=0, $firstTitle='---');

$schoolSealAry = array();
foreach((array)$schoolSealImages as $thisImage) {
    $schoolSealAry[] = array($thisImage['ImageID'], $thisImage['Description']);
}
$html["schoolSealSelection"] = getSelectByArray($schoolSealAry, " id='schoolSealImageID' name ='schoolSealImageID' ", '', $isAll=0, $noFirst=0, $firstTitle='---');

$signatureAry = array();
foreach((array)$signatureImages as $thisImage) {
    $signatureAry[] = array($thisImage['ImageID'], $thisImage['SignatureName']);
}
$html["leftSignatureSelection"] = getSelectByArray($signatureAry, " id='leftSignatureID' name ='leftSignatureID' ", '', $isAll=0, $noFirst=0, $firstTitle='---');
$html["rightSignatureSelection"] = getSelectByArray($signatureAry, " id='rightSignatureID' name ='rightSignatureID' ", '', $isAll=0, $noFirst=0, $firstTitle='---');

$signature2Ary = array();
foreach((array)$signatureImages2 as $thisImage) {
    $signatureAry2[] = array($thisImage['ImageID'], $thisImage['SignatureName']);
}
$html["leftSignature2Selection"] = getSelectByArray($signatureAry2, " id='leftSignatureID2' name ='leftSignatureID2' ", '', $isAll=0, $noFirst=0, $firstTitle='---');
$html["rightSignature2Selection"] = getSelectByArray($signatureAry2, " id='rightSignatureID2' name ='rightSignatureID2' ", '', $isAll=0, $noFirst=0, $firstTitle='---');

###############################################

# Get Class Level
/*for($i=7; $i<14; $i++) {
    $checkbox_class = ' form_check ';
    $checkbox_class .= ' form_check_hkale ';
    if($i < 13) {
        $checkbox_class .= ' form_check_hkdse ';
    }
    if($i > 8 && $i < 13) {
        $checkbox_class .= ' form_check_ib ';
    }
    $html["classLevelCheckbox"] .= $linterface->Get_Checkbox('TargetForm'.$i, 'TargetForm[]', $i, 1, $checkbox_class, 'G'.$i);
    $html["classLevelCheckbox"] .= '&nbsp;&nbsp;';
}*/
//$html["classLevelTextbox"] = '<input type="number" id="TargetFormStart" name="TargetFormStart" min="" max="">';
//$html["classLevelTextbox"] .= ' - ';
//$html["classLevelTextbox"] .= '<input type="number" id="TargetFormEnd" name="TargetFormEnd" min="" max="">';
$classLevelArr = array();
$classLevelArr[] = array(7, 'G7');
$classLevelArr[] = array(8, 'G8');
$classLevelArr[] = array(9, 'G9');
$classLevelArr[] = array(10, 'G10');
$classLevelArr[] = array(11, 'G11');
$classLevelArr[] = array(12, 'G12');
$classLevelArr[] = array(13, 'G13');
$html["classLevelSelection"] .= getSelectByArray($classLevelArr, " id='TargetFormStart' name ='TargetFormStart' ", 7, $isAll=0, $noFirst=1);
$html["classLevelSelection"] .= " $i_To ";
$html["classLevelSelection"] .= getSelectByArray($classLevelArr, " id='TargetFormEnd' name ='TargetFormEnd' ", 13, $isAll=0, $noFirst=1);

### Start for Student Searching ###
# Get Required Data
$data_ary = array();
$data_ary2 = array();

// Current Class
$orderBy = ($intranet_session_language == "en") ? " ORDER BY ClassTitleEN" : " ORDER BY ClassTitleB5";
$sql = "SELECT YearClassID, ClassTitleEN, ClassTitleB5, CurriculumID FROM YEAR_CLASS WHERE AcademicYearID = '".Get_Current_Academic_Year_ID()."' $orderBy";
$result = $lpf->returnArray($sql,3);

// loop classes
for ($i=0; $i<sizeof($result); $i++)
{
    list($this_classid, $this_classnameEN, $this_classnameB5, $this_curriculumID) = $result[$i];

    $cond_disabled = '';
    $disabledStudentIDAry = $lpf_dbs->getDisabledStudentList('', $this_classid, $ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"]);
    $disabledStudentIDAry = Get_Array_By_Key($disabledStudentIDAry, 'StudentID');
    if(!empty($disabledStudentIDAry)) {
        $cond_disabled = " AND USR.UserID NOT IN (".implode(",", $disabledStudentIDAry).") ";
    }

    // Current Class Student
    $className = Get_Lang_Selection($this_classnameB5, $this_classnameEN);
    $name_field = getNameFieldByLang();
    $sql1 = "SELECT
                USR.UserID, $name_field, ycu.ClassNumber, USR.UserLogin
            FROM
                INTRANET_USER USR 
				LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID = USR.UserID)
            WHERE 
                USR.RecordType = 2 /*AND USR.RecordStatus IN (0, 1)*/ AND ycu.YearClassID = '$this_classid' 
                $cond_disabled
            ORDER BY ycu.ClassNumber";
    $result1 = $lpf->returnArray($sql1, 4);

    // Student Curriculum
    $thisClassStudentIDAry = Get_Array_By_Key($result1, 'UserID');
    $studentCurriculumSettingAry = $lpf_dbs->getStudentCurriculumSettings($thisClassStudentIDAry);
    $studentCurriculumSettingAry = BuildMultiKeyAssoc($studentCurriculumSettingAry, 'StudentID', 'CurriculumID', 1, 0);

    // loop students
    for ($j=0; $j<sizeof($result1); $j++)
    {
        list($this_user_id, $this_user_name, $this_class_number, $this_user_login) = $result1[$j];
        $this_userCurriculumID = $studentCurriculumSettingAry[$this_user_id] ? $studentCurriculumSettingAry[$this_user_id] : $this_curriculumID;
        if($this_userCurriculumID > 0)
        {
            $data_ary[$this_userCurriculumID][] = array($this_user_id, $className, $this_class_number, $this_user_name);
        }
    }
}

// build js array for autocomplete
if(!empty($data_ary))
{
    // loop curriculum
    foreach($data_ary as $this_curriculumId => $this_data_ary)
    {
        # Define YUI array (Search by input format)
        for ($i=0; $i<sizeof($this_data_ary); $i++)
        {
            list($this_user_id, $this_class_name, $this_class_number, $this_user_name) = $this_data_ary[$i];

            $temp_str = $this_user_name;
            if($this_class_number) {
                $temp_str .= " (". $this_class_name ."-". $this_class_number .")";
            }
            $temp_str2 = $temp_str;

            $liArr[$this_curriculumId] .= "[\"". str_replace(array('"',"\n","\r"),array('\"','',''),$temp_str) ."\", \"". str_replace(array('"',"\n","\r"),array('\"','',''),$temp_str2) ."\", \"". $this_user_id ."\"]";
            ($i == (sizeof($this_data_ary)-1)) ? $liArr[$this_curriculumId] .= "" : $liArr[$this_curriculumId] .= ",\n";
        }
    }
}

// Archive Student
$cond_disabled = '';
$disabledStudentIDAry = $lpf_dbs->getDisabledStudentList('', '', $ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"]);
$disabledStudentIDAry = Get_Array_By_Key($disabledStudentIDAry, 'StudentID');
if(!empty($disabledStudentIDAry)) {
    $cond_disabled1 = " AND USR.UserID NOT IN (".implode(",", $disabledStudentIDAry).") ";
    $cond_disabled2 = " AND ASR.UserID NOT IN (".implode(",", $disabledStudentIDAry).") ";
}

$name_field = getNameFieldByLang('USR.');
$archive_name_field = getNameFieldByLang('ASR.');
$sql2 = "SELECT
              UASR.*
          FROM
          (
                SELECT
                    USR.UserID, $name_field AS UserName, USR.UserLogin, USR.YearOfLeft
                FROM
                    INTRANET_USER USR 
				WHERE
				    USR.RecordType = 2 AND USR.RecordStatus = 4
				    $cond_disabled1
				    UNION
                SELECT
                    ASR.UserID, $archive_name_field AS UserName, ASR.UserLogin, ASR.YearOfLeft
                FROM
                    INTRANET_ARCHIVE_USER ASR 
				WHERE
				    ASR.RecordType = 2 /*AND ASR.RecordStatus IN (0, 1)*/
				    $cond_disabled2
         ) UASR
         ORDER BY UASR.YearOfLeft+0, UASR.UserName";
$result2 = $lpf->returnArray($sql2, 4);

// Student Curriculum
$thisClassStudentIDAry = Get_Array_By_Key($result2, 'UserID');
$studentCurriculumSettingAry = $lpf_dbs->getStudentCurriculumSettings($thisClassStudentIDAry);
$studentCurriculumSettingAry = BuildMultiKeyAssoc($studentCurriculumSettingAry, 'StudentID', 'CurriculumID', 1, 0);

// loop students
for ($j=0; $j<sizeof($result2); $j++)
{
    list($this_user_id, $this_user_name, $this_user_login, $this_year_of_left) = $result2[$j];
    $this_userCurriculumID = $studentCurriculumSettingAry[$this_user_id];
    if($this_userCurriculumID > 0)
    {
        $data_ary2[$this_userCurriculumID][] = array($this_user_id, $this_user_name, $this_year_of_left);
    }
}

// build js array for autocomplete
if(!empty($data_ary2))
{
    # Define YUI array (Search by input format)
    foreach($data_ary2 as $this_curriculumId => $this_data_ary)
    {
        for ($i=0; $i<sizeof($this_data_ary); $i++)
        {
            list($this_user_id, $this_user_name, $this_left_year) = $this_data_ary[$i];

            $temp_str = $this_user_name;
            if($this_left_year) {
                $temp_str .= " (". $this_left_year .")";
            }
            $temp_str2 = $temp_str;

            $liArr2[$this_curriculumId] .= "[\"". str_replace(array('"',"\n","\r"),array('\"','',''),$temp_str) ."\", \"". str_replace(array('"',"\n","\r"),array('\"','',''),$temp_str2) ."\", \"". $this_user_id ."\"]";
            ($i == (sizeof($this_data_ary)-1)) ? $liArr2[$this_curriculumId] .= "" : $liArr2[$this_curriculumId] .= ",\n";
        }
    }
}
### END for Searching ###

###############################################
###	HTML - display table
$html["display_table"] = $displayTableHtml;

### Page ###
$CurrentPage = "Teacher_Transcript_PrintReport";
$CurrentPageName = $Lang['iPortfolio']['DBSTranscript']['PrintReport']['Title'];

### Title ###
$TAGS_OBJ[] = array($Lang['iPortfolio']['DBSTranscript']['PrintReport']['Title'], '');
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

######## Display #########
// $PAGE_NAVIGATION[] = array($Lang['iPortfolio']['DBSTranscript']['PrintReport']['Title']);
// $html["navigation"] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);

include_once("../templates/report_index.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>