<?php
// Modified by
/*
 * Modification Log:
 *  Date: 2018-08-16 (Bill)
 *      - Create file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript_ui.php");

iportfolio_auth("T");
intranet_opendb();

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();
$lpf_dbs_ui = new libpf_dbs_transcript_ui();

$image_type = $_POST['imageType'];
if($image_type == "") {
    header('Location: back_cover.php');
    die();
}
$isSignature = $image_type == 'signature';

$is_edit = !$_POST['isNew'] && !empty($_POST['imageIds'][0]);
if($is_edit) {
    $image = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"][$imageType], $_POST['imageIds'][0]);
    $image = $image[0];
    if($image['ImageLink']){ 	// Have set photo
        $hasPhotoDispaly = true;
    }
} else {
    $image['CurriculumID'] = $_POST['curriculumID']? $_POST['curriculumID'] : '';
    $image['ReportTypeID'] = $_POST['reportTypeID']? $_POST['reportTypeID'] : '';
}

$html["CurriculumSelection"] = $lpf_dbs_ui->getCurriculumSelection('CurriculumID', $image['CurriculumID'], $onchange='', $noFirst=1, $firstTitleText='', $useCode=true);
$html["ReportTypeSelection"] .= $lpf_dbs_ui->getReportTypeSelection('ReportTypeID', $image['ReportTypeID'], $onChange='', $noFirst=1);

// Hidden Input fields
$table_hidden = <<<HTMLEND
<input type="hidden" name="pageNo" value="{$_POST['pageNo']}">
<input type="hidden" name="order" value="{$_POST['order']}">
<input type="hidden" name="field" value="{$_POST['field']}">
<input type="hidden" name="page_size_change" value="{$_POST['page_size_change']}">
<input type="hidden" name="numPerPage" value="{$_POST['numPerPage']}">

<input type="hidden" id="ImageID" name="ImageID" value="{$image['ImageID']}">
<input type="hidden" id="ImageType" name="ImageType" value="{$image_type}">
<input type='hidden' id='ImageLink' name='ImageLink' value="{$image['ImageLink']}">
HTMLEND;
###############################################

###############################################
###	HTML - display table
$html["display_table"] = $table_hidden;

### Page ###
$linterface = new interface_html();
$CurrentPage = "Teacher_Transcript_ImageManagement";
$CurrentPageName = $Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Title'];

### Title ###
$TAGS_OBJ = $lpf_dbs_ui->getImageMmgtTab($image_type);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

######## Display #########
$TagTitle = $is_edit? $button_edit : $button_new;
// $PAGE_NAVIGATION[] = array($Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Title']);
$PAGE_NAVIGATION[] = array($TagTitle);
$html["navigation"] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$linterface->LAYOUT_START();

include_once("../templates/image_edit.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>