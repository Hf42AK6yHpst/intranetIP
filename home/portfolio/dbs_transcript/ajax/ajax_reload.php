<?php
// Using:

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libclass.php");
include_once ($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

$lclass = new libclass();
$lpf_dbs = new libpf_dbs_transcript();

$target = $_REQUEST['target'];                      // form | class | student | student2ndLayer
$fieldId = $_REQUEST['fieldId'];                    // <select> id
$fieldName = $_REQUEST['fieldName'];                // <select> name
$academicYearId = $_REQUEST['academicYearId'];
$studentFieldId = $_REQUEST['studentFieldId'];      // student <select> id
$studentFieldName = $_REQUEST['studentFieldName'];  // student <select> name
$curriculumId = $_REQUEST['CurriculumID'];
$isArchived = $_REQUEST['isArchived'];
$excludeCurriculumChecking = $_REQUEST['excludeCurriculumChecking'];

if ($target == 'form') {
    $temp = '<select name="'.$fieldName.'" id="'.$fieldId.'" class="formtextbox" multiple size="5" >';
    //$ClassLvlArr = $lclass->getLevelArray();
    $ClassLvlArr = $lpf_dbs->getCurriculumFormClassList($curriculumId, "", $groupByYear=true);
    for ($i=0; $i<sizeof($ClassLvlArr); $i++) {
        $temp .= '<option value="'.$ClassLvlArr[$i][1].'"';
        $temp .= ' selected';
        $temp .= '>'.$ClassLvlArr[$i][0].'</option>'."\n";
    }
    $temp .= '</select>'."\n";
}
else if ($target == "class") {
    $temp = '<select name="'.$fieldName.'" id="'.$fieldId.'" class="formtextbox" multiple size="5" >';
    $formClassResult = $lpf_dbs->getCurriculumClassList($curriculumId);
    $formClassResult = BuildMultiKeyAssoc($formClassResult, array('YearID', 'YearClassID'));
    foreach($formClassResult as $classResult) {
        $classInfo = reset($classResult);
        $temp .= "<optgroup label=\"".$classInfo[2]."\">\n";
        foreach($classResult as $classInfo) {
            $temp .= '<option value="'.$classInfo[1].'" selected';
            $temp .= '>'.$classInfo[0].'</option>';
        }
    }
    $temp .= '</select>'."\n";
}
else if ($target == "student") {
    $temp = '<select name="'.$fieldName.'" id="'.$fieldId.'" class="formtextbox" multiple size="8" onchange="'.$onchange.'">';
    $formClassResult = $lpf_dbs->getCurriculumClassList($curriculumId, $academicYearId, $_REQUEST['allCurriculum']);
    $formClassResult = BuildMultiKeyAssoc($formClassResult, array('YearID', 'YearClassID'));
    foreach($formClassResult as $classResult) {
        $classInfo = reset($classResult);
        $temp .= "<optgroup label=\"".$classInfo[2]."\">\n";
        foreach($classResult as $classInfo) {
            $temp .= '<option value="'.$classInfo[1].'" selected';
            $temp .= '>'.$classInfo[0].'</option>';
        }
    }
    $temp .= '</select>'."\n";
    $temp .= '<div id="'.$divStudentSelection.'"></div>'."\n";
}
else if ($target == "student2ndLayer") {
    $yearClassIdAry = (array)$_REQUEST['YearClassID'];
    
    include_once ($PATH_WRT_ROOT."includes/form_class_manage.php");
    $fcm = new form_class_manage();
    
    // Get Class Student
    $classStudentIdAry = array();
    foreach($yearClassIdAry as $thisYearClassId)
    {
        $thisClassStudentIDAry = $fcm->Get_Student_By_Class($thisYearClassId);
        $thisClassStudentIDAry = Get_Array_By_Key($thisClassStudentIDAry, 'UserID');
        
        // Perform Curriculum Checking
        if(!$excludeCurriculumChecking)
        {
            $studentCurriculumSettingAry = $lpf_dbs->getStudentCurriculumSettings($thisClassStudentIDAry);
            if(!empty($studentCurriculumSettingAry))
            {
                // Get Class Student with target Curriculum
                $studentCurriculumSettingAry = BuildMultiKeyAssoc($studentCurriculumSettingAry, array('CurriculumID', 'StudentID'), 'StudentID', 1, 0);
                $studentWithTargetCurriculumAry = $studentCurriculumSettingAry[$_POST['curriculumID']];
                
                // Get Class Student without any Curriculum Settings (mainly for new Class Students)
                $studentWithoutCurriculumSettingAry = $lpf_dbs->getStudentWithoutCurriculumSettings($thisClassStudentIDAry, $_POST['curriculumID']);
                $studentWithoutCurriculumSettingAry = Get_Array_By_Key($studentWithoutCurriculumSettingAry, 'UserID');
                $thisClassStudentIDAry = array_merge((array)$studentWithTargetCurriculumAry, (array)$studentWithoutCurriculumSettingAry);
            }
        }
        $classStudentIdAry = array_merge($classStudentIdAry, (array)$thisClassStudentIDAry);
    }
    $cond_curriculum = '';
    if(!empty($classStudentIdAry)) {
        $cond_curriculum = " AND (USR.UserID IN (".implode(",", (array)$classStudentIdAry).") OR ASR.UserID IN (".implode(",", (array)$classStudentIdAry).")) ";
    }
    
    $cond_disabled = '';
    if(!$_REQUEST['includeInactive']) {
        $targetStatus = $ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"];
    }
    $disabledStudentIDAry = $lpf_dbs->getDisabledStudentList('', $yearClassIdAry, $targetStatus);
    $disabledStudentIDAry = Get_Array_By_Key($disabledStudentIDAry, 'StudentID');
    if(!empty($disabledStudentIDAry)) {
        $cond_disabled = " AND (USR.UserID NOT IN (".implode(",", $disabledStudentIDAry).") OR ASR.UserID NOT IN (".implode(",", $disabledStudentIDAry).")) ";
    }
    
    $cond_archived = '';
    if($isArchived) {
        $cond_archived = " AND (USR.RecordType = 4 OR ASR.UserID IS NOT NULL) ";
    }
    
    $temp .= '<br />';
    $temp .= '<select name="'.$studentFieldName.'" id="'.$studentFieldId.'" class="formtextbox" multiple size="8">';
    
    $clsName = ($intranet_session_language == "en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
    $name_field = getNameFieldByLang("USR.");
    $archive_name_field = getNameFieldByLang("ASR.");
    
    $sql = "SELECT
                $clsName, ycu.ClassNumber, IF(USR.UserID IS NOT NULL, $name_field, $archive_name_field) as StudentName, IF(USR.UserID IS NOT NULL, USR.UserID, ASR.UserID) as UserID
            FROM
                YEAR_CLASS yc
    			INNER JOIN YEAR_CLASS_USER ycu ON (yc.YearClassID = ycu.YearClassID)
                LEFT OUTER JOIN INTRANET_USER USR ON (ycu.UserID = USR.UserID AND USR.RecordType = 2) 
    			LEFT OUTER JOIN INTRANET_ARCHIVE_USER ASR ON (ycu.UserID = ASR.UserID AND ASR.RecordType = 2)
			WHERE 
                yc.YearClassID IN (".implode(",", $yearClassIdAry).") AND 
                (USR.UserID IS NOT NULL OR ASR.UserID IS NOT NULL) 
                $cond_disabled
                $cond_curriculum
                $cond_archived
			ORDER BY 
                yc.ClassTitleEN, ycu.ClassNumber ";
    $studentResult = $lclass->returnArray($sql, 3);
    for ($k=0; $k<sizeof($studentResult); $k++) {
        $temp .= '<option value="'.$studentResult[$k][3].'" selected';
        $temp .= '>'.$studentResult[$k][0].'-'.$studentResult[$k][1].' '.$studentResult[$k][2].'</option>';
    }
    $temp .= '</select>'."\n";
    $temp .= '<br />';
    
    $temp .= $linterface->GET_BTN($button_select_all, "button", "Select_All_Options('$studentFieldId', true); return false;");
}
else if ($target == "headerSelection") {
    $headerAry = array();
    $headerImages = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["header"], $imageID='', $curriculumId);
    foreach((array)$headerImages as $thisImage) {
        $headerAry[] = array($thisImage['ImageID'], $thisImage['Description']);
    }
    $temp = getSelectByArray($headerAry, " id='headerImageID' name ='headerImageID' ", '', $isAll=0, $noFirst=0, $firstTitle='---');
}
else if ($target == "backCoverSelection") {
    $backCoverAry = array();
    $backCoverImages = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["back_cover"], $imageID='', $curriculumId);
    foreach((array)$backCoverImages as $thisImage) {
        $backCoverAry[] = array($thisImage['ImageID'], $thisImage['Description']);
    }
    $temp = getSelectByArray($backCoverAry, " id='backCoverImageID' name ='backCoverImageID' ", '', $isAll=0, $noFirst=0, $firstTitle='---');
}
else if ($target == "schoolSealSelection") {
    $schoolSealAry = array();
    $schoolSealImages = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["school_seal"], $imageID='', $curriculumId);
    foreach((array)$schoolSealImages as $thisImage) {
        $schoolSealAry[] = array($thisImage['ImageID'], $thisImage['Description']);
    }
    $temp = getSelectByArray($schoolSealAry, " id='schoolSealImageID' name ='schoolSealImageID' ", '', $isAll=0, $noFirst=0, $firstTitle='---');
}
else if ($target == "leftSignatureSelection") {
    $signatureAry = array();
    $signatureImages = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["signature"], $imageID='', $curriculumId, $ipf_cfg["DSBTranscript"]["REPORT_TYPE"]["Transcript"]);
    foreach((array)$signatureImages as $thisImage) {
        $signatureAry[] = array($thisImage['ImageID'], $thisImage['SignatureName']);
    }
    $temp = getSelectByArray($signatureAry, " id='leftSignatureID' name ='leftSignatureID' ", '', $isAll=0, $noFirst=0, $firstTitle='---');
}
else if ($target == "rightSignatureSelection") {
    $signatureAry = array();
    $signatureImages = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["signature"], $imageID='', $curriculumId, $ipf_cfg["DSBTranscript"]["REPORT_TYPE"]["Transcript"]);
    foreach((array)$signatureImages as $thisImage) {
        $signatureAry[] = array($thisImage['ImageID'], $thisImage['SignatureName']);
    }
    $temp = getSelectByArray($signatureAry, " id='rightSignatureID' name ='rightSignatureID' ", '', $isAll=0, $noFirst=0, $firstTitle='---');
}
else if ($target == "leftSignature2Selection") {
    $signatureAry = array();
    $signatureImages = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["signature"], $imageID='', $curriculumId, $ipf_cfg["DSBTranscript"]["REPORT_TYPE"]["PredictedGrade"]);
    foreach((array)$signatureImages as $thisImage) {
        $signatureAry[] = array($thisImage['ImageID'], $thisImage['SignatureName']);
    }
    $temp = getSelectByArray($signatureAry, " id='leftSignatureID2' name ='leftSignatureID2' ", '', $isAll=0, $noFirst=0, $firstTitle='---');
}
else if ($target == "rightSignature2Selection") {
    $signatureAry = array();
    $signatureImages = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["signature"], $imageID='', $curriculumId, $ipf_cfg["DSBTranscript"]["REPORT_TYPE"]["PredictedGrade"]);
    foreach((array)$signatureImages as $thisImage) {
        $signatureAry[] = array($thisImage['ImageID'], $thisImage['SignatureName']);
    }
    $temp = getSelectByArray($signatureAry, " id='rightSignatureID2' name ='rightSignatureID2' ", '', $isAll=0, $noFirst=0, $firstTitle='---');
}
echo $temp;

intranet_closedb();
?>