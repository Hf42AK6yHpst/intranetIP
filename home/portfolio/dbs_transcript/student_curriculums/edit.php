<?php
// Modified by 

/********************** Change Log ***********************/
#
/********************** Change Log ***********************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-teacher.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript_ui.php");

iportfolio_auth("T");
intranet_opendb();

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();
$lpf_dbs_ui = new libpf_dbs_transcript_ui();
$lfcm_ui = new form_class_manage_ui();

# Form & Class
$ClassId = $_POST['YearClassID']? $_POST['YearClassID'] : $_GET['YearClassID'];
$ClassId = IntegerSafe(standardizeFormPostValue($ClassId));
if(!$ClassId) {
    header('Location: index.php');
    exit;
}

$YearClass = new year_class($ClassId, true, false, true);
$defaultCurriculumID = $YearClass->CurriculumID;

$classAcademicYearID = $YearClass->AcademicYearID;
$isCurrentAcademicYear = $classAcademicYearID == Get_Current_Academic_Year_ID();

# Class Students
$classStudentAry = $YearClass->ClassStudentList;
$numOfStudent = count($classStudentAry);
if(!$isCurrentAcademicYear && $numOfStudent > 0)
{
    $classStudentAry = array();
    foreach((array)$YearClass->ClassStudentList as $thisClassStudent) {
        if($thisClassStudent['ArchiveUser']) {
            $classStudentAry[] = $thisClassStudent;
        }
    }
}

# Class Selection
if($isCurrentAcademicYear) {
    $htmlAry["classSelection"] = $lfcm_ui->Get_Class_Selection(Get_Current_Academic_Year_ID(), '', 'YearClassID', $ClassId, ' changeClass() ', 1);
}
else {
    $sql = "SELECT
                yc.YearClassID, yc.".Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN')." as ClassName
            FROM
                {$intranet_db}.YEAR_CLASS AS yc
                INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu ON  yc.YearClassID = ycu.YearClassID
                INNER JOIN {$intranet_db}.INTRANET_ARCHIVE_USER AS iu ON ycu.UserID = iu.UserID
            WHERE
                yc.AcademicYearID = '".$YearClass->AcademicYearID."' AND
                yc.YearID != 0 AND
                iu.RecordType = 2
            GROUP BY
                yc.YearClassID ";
    $result = build_assoc_array($YearClass->returnArray($sql));
    $htmlAry["classSelection"] = getSelectByAssoArray($result, " id='YearClassID' name='YearClassID' onchange='changeClass()' ", $ClassId, 0, 1);
}

# Curriculum Selection
$curriculumSelection = $lpf_dbs_ui->getCurriculumSelection('allCurriculumID', $selected='', $onchange='applyAllCurriculum(this.value)', $noFirst=0, $firstTitleText='', $useCode=true);

# View Content Table
$displayContent = "";
$displayContent .= "<table class='common_table_list display_table' width='100%' cellpadding='4' cellspacing='0'>";
$displayContent .= "<tr>";
    $displayContent .= "<th class='tablegreentop tabletopnolink' width='5%' nowrap>".$Lang['General']['ClassNumber']."</th>";
    $displayContent .= "<th class='tablegreentop tabletopnolink' width='45%' nowrap>".$Lang['General']['StudentName']."</th>";
    $displayContent .= "<th class='tablegreentop tabletopnolink' width='50%' nowrap>".$Lang['SysMgr']['FormClassMapping']['Curriculum']."<br/>".$curriculumSelection."</th>";
$displayContent .= "</tr>";

if(empty($classStudentAry))
{
    // No Record
    $displayContent .= "<tr>";
        $displayContent .= "<td colspan='3' align='center'>";
        $displayContent .= $no_record_msg;
        $displayContent .= "</td>";
    $displayContent .= "</tr>";
}
else
{
    foreach($classStudentAry as $thisStudentInfo)
    {
        $studentCurriculumID = '';
        $studentCurriculum = $lpf_dbs->getStudentCurriculumSettings($thisStudentInfo['UserID']);
        if(!empty($studentCurriculum) && $studentCurriculum[0]['CurriculumID']) {
            $studentCurriculumID = $studentCurriculum[0]['CurriculumID'];
        }
        $studentCurriculumID = $studentCurriculumID? $studentCurriculumID : $defaultCurriculumID;
        $curriculumSelection = $lpf_dbs_ui->getCurriculumSelection('curriculumID['.$thisStudentInfo['UserID'].']', $selected=$studentCurriculumID, $onchange=' ', $noFirst=1, $firstTitleText='', $useCode=true);
        
        $displayContent .= "<tr>";
            $displayContent .= "<td class='tabletext'>".$thisStudentInfo['ClassNumber']."</td>";
            $displayContent .= "<td class='tabletext'>".$thisStudentInfo['StudentName']."</td>";
            $displayContent .= "<td class='tabletext'>".$curriculumSelection."</td>";
        $displayContent .= "</tr>";
    }
}
$displayContent .= "</table>";
$displayContent .= "</div>";
$htmlAry["displayTable"] = $displayContent;
###	HTML - display table

$linterface = new interface_html();

# Button
$htmlAry["EditButton"] = "";
$htmlAry["EditButton"] .= $linterface->GET_ACTION_BTN($button_submit, "button", "goSubmit()", "submitBtn");
$htmlAry["EditButton"] .= "&nbsp;&nbsp;";
$htmlAry["EditButton"] .= $linterface->GET_ACTION_BTN($button_back, "button", "goBack()", "backBtn");

# Hidden Fields
if(!$isCurrentAcademicYear) {
    $htmlAry['hiddenField'] = "<input type='hidden' name='classYearID' value='$classAcademicYearID'>";
    $htmlAry['hiddenField'] .= "<input type='hidden' name='isAlumni' value='1'>";
}

# Page heading setting
$CurrentPage = "Teacher_Transcript_StudentCurriculumsMapping";

### Title ###
$TAGS_OBJ[] = array($Lang['iPortfolio']['DBSTranscript']['StudentCurriculumsMapping']['Title'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);

include_once("../templates/student_curriculums_setting_edit.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>