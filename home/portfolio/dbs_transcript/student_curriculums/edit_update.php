<?php
// Using: Bill

############# Change Log [Start] ################
#
#   Date:   2018-10-15 (Bill)   [2017-1207-0956-53277]
#           Create file
# 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript_ui.php");

iportfolio_auth("T");
intranet_opendb();

$linterface = new interface_html();

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();
$lpf_dbs_ui = new libpf_dbs_transcript_ui();

// Check if Super Admin
$isPortfolioSuperAdmin = $lpf->IS_IPF_SUPERADMIN();

$currentClassID = $_POST['YearClassID'];
$studentCurriculumAry = $_POST['curriculumID'];

//$error_occured = 0;
$returnAry = array();
foreach($studentCurriculumAry as $studentID => $curriculumID)
{
    $returnAry[] = $lpf_dbs->insertUpdateStudentCurriculumSettings($studentID, $curriculumID);
}

$msg = !in_array(false, $returnAry)? 'UpdateSuccess' : 'UpdateUnsuccess';
$is_alumni = $_POST['isAlumni']? '&isAlumni=1&targetYearID='.$_POST['classYearID'] : '';
header('Location: index.php?msg='.$msg.$is_alumni);
?>