<?php
// Modifying by

####### Change Log ###################
#
#   Date:   2020-10-14 [Bill]   [2020-1009-1532-10207]
#   fixed cannot display stat chart if Show by : By school year
#
#   Date:   2020-06-29 [Bill]   [2020-0612-1004-37207]
#   show score if Score > 0 + No Grade + No Ranking   ($sys_custom['ipf']['Mgmt']['SchoolRecordDisplayScore_EvenWithoutGradeAndRanking'])
#
#	Date	2017-09-26 (Anna)
#	change flash to highchart and add tooltips for icon
#
#	Date	2016-10-24	(Omas)
#	fix php 5.4 pass by ref.
#
######################################

/************************************************************************************************************************
 * $miscParameter Format:
 * 1. build parameter string: MainSubjectCode={$mainSubjCode}&CompSubjectCode={$compSubjCode}&displayBy={$displayBy}
 *    - optional: &yearFilter={$yearFilter} 
 * 2. encode the string by base64_encode() 
 ************************************************************************************************************************/

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

include_once($PATH_WRT_ROOT."includes/json.php");
//include_once($PATH_WRT_ROOT."includes/flashchart_basic_201301_in_use/php-ofc-library/open-flash-chart.php");

intranet_opendb();

$li_pf = new libpf_sturec();
$li_pf->ACCESS_CONTROL("student_info");

$json = new JSON_obj();

function GEN_MISC_PARAMETER($mainSubjCode, $compSubjCode, $displayBy, $yearFilter=0)
{
    return base64_encode("MainSubjectCode={$mainSubjCode}&CompSubjectCode={$compSubjCode}&displayBy={$displayBy}&yearFilter={$yearFilter}");
}

# Get class selection
$lpf_fc = new libpf_formclass();
$class_name = $i_general_WholeSchool;
$t_class_arr = $lpf_fc->GET_CLASS_LIST();
for($i=0; $i<count($t_class_arr); $i++)
{
    $t_yc_id = $t_class_arr[$i][0];
    $t_yc_title = Get_Lang_Selection($t_class_arr[$i]['ClassTitleB5'], $t_class_arr[$i]['ClassTitleEN']);
    if(!is_array($class_teach_arr) || in_array($t_yc_id, $class_teach_arr))
    {
        $class_arr[] = array($t_yc_id, $t_yc_title);
    }

    if($t_yc_id == $YearClassID)
    {
        $class_name = $t_yc_title;
    }
}
$class_selection_html = getSelectByArray($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, 1, 0, "", 2);

# Get student selection
$lpf_fc->SET_CLASS_VARIABLE("YearClassID", $YearClassID);
$default_student_id = "";
$student_id_arr = $lpf_fc->GET_STUDENT_LIST();
$t_student_detail_arr = $lpf_fc->GET_STUDENT_DETAIL_LIST($student_id_arr);
if(is_array($t_student_detail_arr))
{
    for($i=0; $i<count($t_student_detail_arr); $i++)
    {
        $t_classname = $t_student_detail_arr[$i]['ClassName'];
        if(in_array($t_classname, $class_arr)) {
            continue;
        }

        $t_user_id = $t_student_detail_arr[$i]['UserID'];
        $t_user_name = "(".$t_classname." - ".$t_student_detail_arr[$i]['ClassNumber'].") ";
        $t_user_name .= Get_Lang_Selection($t_student_detail_arr[$i]['ChineseName'], $t_student_detail_arr[$i]['EnglishName']);

        # Set default user ID if class is changed
        if($StudentID == $t_user_id) {
            $default_student_id = $StudentID;
        }
        $student_detail_arr[] = array($t_user_id, $t_user_name);
    }
}
if($default_student_id == "") {
  $default_student_id = $student_detail_arr[0][0];
}
$student_selection_html = getSelectByArray($student_detail_arr, "name='StudentID' onChange='jCHANGE_FIELD()'", $default_student_id, 0, 1, "", 2);

# Get activated students
$act_student_id_arr = $lpf_fc->GET_ACTIVATED_STUDENT_LIST();

# Retrieve student info
$student_obj = $li_pf->GET_STUDENT_OBJECT($default_student_id);

# Set links for photo
if(strpos($student_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $student_obj['PhotoLink'] != "") {
	$student_obj['PhotoLink'] = str_replace("<!--ImageStyle-->", "", $student_obj['PhotoLink']);
} else if($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0) {
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
} else {
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
}

# Generate menu tab
$availTab = libpf_tabmenu::getSchoolRecordTags("assessment");
$split_tab_arr = array_chunk($availTab, 5);
$html_menu_tab = "";
for($i=0, $i_max=count($split_tab_arr); $i<$i_max; $i++)
{
    $html_menu_tab .= $li_pf->GET_TAB_MENU($split_tab_arr[$i]);
}

$linterface = new interface_html();
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];

### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

##########################################################################################
##########################################################################################
##########################################################################################
$yearTitle = ($yearFilter==0) ? $ec_iPortfolio['term'] :  $ec_iPortfolio['year'];

// Default values of parameters
$displayBy = "Score";
$yearFilter = 0;      // 0: Semester, 1: Year

// Restructure parameters
$miscParameterArr = explode("&", base64_decode($miscParameter));
for($i=0, $i_max=count($miscParameterArr); $i<$i_max; $i++)
{
	list($var_name, $var_val) = explode("=", $miscParameterArr[$i]);
	${$var_name} = $var_val;
}

# Define the buttons according to the variable $displayBy
$SubjectParam = "MainSubjectCode={$MainSubjectCode}&CompSubjectCode={$CompSubjectCode}";
$link_score = ($displayBy=="Score") ? "<span class='link_a'>".$ec_iPortfolio['display_by_score']."</span>" : "<a href=\"javascript:jCHANGE_DISPLAY('".GEN_MISC_PARAMETER($MainSubjectCode, $CompSubjectCode, "Score", $yearFilter)."')\" class='link_a'>".$ec_iPortfolio['display_by_score']."</a>";
$link_rank = ($displayBy=="Rank") ? "<span class='link_a'>".$ec_iPortfolio['display_by_rank']."</span>" : "<a href=\"javascript:jCHANGE_DISPLAY('".GEN_MISC_PARAMETER($MainSubjectCode, $CompSubjectCode, "Rank", $yearFilter)."')\" class='link_a'>".$ec_iPortfolio['display_by_rank']."</a>";
$link_stand_score = ($displayBy=="StandardScore") ? "<span class='link_a'>".$ec_iPortfolio['display_by_stand_score']."</span>" : "<a href=\"javascript:jCHANGE_DISPLAY('".GEN_MISC_PARAMETER($MainSubjectCode, $CompSubjectCode, "StandardScore", $yearFilter)."')\" class='link_a'>".$ec_iPortfolio['display_by_stand_score']."</a>";

# Create selection (by semester/ by year)
$filter_selection = "<td nowrap='nowrap'><input name='yearFilter' id='yearFilterYear' value='1' type='radio' ".($yearFilter==1?"CHECKED='CHECKED'":"")." onClick=\"jCHANGE_DISPLAY('".GEN_MISC_PARAMETER($MainSubjectCode, $CompSubjectCode, $displayBy, 1)."')\"> <label for=\"yearFilterYear\">{$ec_iPortfolio['by_year']}</label></td>\n";
$filter_selection .= "<td nowrap='nowrap' valign='top'><input name='yearFilter' id='yearFilterSem' value='0' type='radio' ".($yearFilter==0?"CHECKED='CHECKED'":"")." onClick=\"jCHANGE_DISPLAY('".GEN_MISC_PARAMETER($MainSubjectCode, $CompSubjectCode, $displayBy, 0)."')\"> <label for=\"yearFilterSem\">".$ec_iPortfolio['by_semester']."</label></td>\n";

switch($displayBy)
{
    case "Score":
        //$retrieveField = "IF(Score <= 0, IF(Grade = '' OR Grade IS NULL, '', Score), TRIM('.' FROM TRIM(0 FROM ROUND(Score, 1))))";
        $retrieveField = "CASE ";
        $retrieveField .= "WHEN Score < 0 THEN '--' ";
        $retrieveField .= "WHEN (Score = 0 OR Score = -0) THEN IF(IFNULL(Grade, '') = '', '--', 0) ";
        $retrieveField .= "ELSE TRIM('.' FROM TRIM(0 FROM ROUND(Score, 2))) ";
        $retrieveField .= "END";
        $totalTitle = $ec_iPortfolio['overall_score'];
        $htmlNavArea = "<span>{$link_score}</span> | {$link_rank} | {$link_stand_score}";
        $maxScore = current($li_pf->returnVector("SELECT MAX(Score) FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE SubjectCode = '{$MainSubjectCode}' AND IFNULL(SubjectComponentCode, '') = '{$CompSubjectCode}'"));
        $y_axis_Max = ceil($maxScore/100) * 100;
        $y_axis_Min = 0;
        $y_axis_Step = MAX(round($y_axis_Max / 100), 1) * 10;
        break;
    case "Rank":
        $retrieveField = "IF(IFNULL(OrderMeritForm, 0) = 0, '--', OrderMeritForm)";
        $totalTitle = $ec_iPortfolio['overall_rank'];
        $htmlNavArea = "{$link_score}  | <span>{$link_rank}</span> | {$link_stand_score}";
        $maxRank = current($li_pf->returnVector("SELECT IF(MAX(OrderMeritForm) > OrderMeritFormTotal, MAX(OrderMeritForm), OrderMeritFormTotal) FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE SubjectCode = '{$MainSubjectCode}' AND IFNULL(SubjectComponentCode, '') = '{$CompSubjectCode}'"));
        $y_axis_Max = 1;
        $y_axis_Min = ceil($maxRank/10) * 10;
        $y_axis_Step = MAX(round($y_axis_Min / 100), 1) * 10;
        break;
    case "StandardScore":
        $totalTitle = $ec_iPortfolio['overall_stand_score'];
        $htmlNavArea = "{$link_score}  | {$link_rank} | <span>{$link_stand_score}</span>";
        $y_axis_Max = 5;
        $y_axis_Min = -5;
        $y_axis_Step = 1;
        break;
}

// [2020-0612-1004-37207] Show Score if Score > 0 + No Grade + No Ranking
$alwaysDisplayScoreIfValid = $sys_custom['ipf']['Mgmt']['SchoolRecordDisplayScore_EvenWithoutGradeAndRanking'];
 
// since accessment Nav Area with a speical layout
$htmlNavStyle = "align=\"right\" valign=\"middle\" class=\"thumb_list\"";

// Records of class history
$classHistoryArr = $li_pf->getAssessmentClassHistoryArr($default_student_id);

// Prepare for:
// i) table header (year, class, semester)
// ii) partial query strings of temporary table
$yearClassStr = "";
$semStr = "";
$semCount = 0;
for($i=0, $i_max=count($classHistoryArr); $i<$i_max; $i++)
{
    $_ayID = $classHistoryArr[$i]["AcademicYearID"];
    $_lay = new academic_year($_ayID);
    $_year = $_lay->Get_Academic_Year_Name();
    $_year = empty($_year) ? $classHistoryArr[$i]["Year"] : $_year;
    $_ytID = $classHistoryArr[$i]["YearTermID"];
    $_layt = new academic_year_term($_ytID);
    $_sem = $_layt->Get_Year_Term_Name();
    $_sem = empty($_sem) ? $classHistoryArr[$i]["Semester"] : $_sem;
    $_className = $classHistoryArr[$i]["ClassName"];

    $ayArr[$_ayID] = array("yearname"=>$_year, "classname"=>$_className);
    $semArr[$_ayID][$_ytID] = $_sem;
}
if(is_array($ayArr))
{
    $insertAssessTableSqlArr = array();
    foreach($ayArr AS $ayID => $ayDetailArr)
    {
        $_yearName = $ayDetailArr["yearname"];
        $_className = $ayDetailArr["classname"];

        $yearClassStr .= "<td width='15%' colspan='".count($semArr)."' nowrap='nowrap' align='center'>{$_yearName}<br />{$_className}</td>\n";
        if(is_array($semArr))
        {
            $subjOverallRes = (array_key_exists(0, $semArr[$ayID]) || array_key_exists("", $semArr[$ayID]));

            foreach($semArr[$ayID] AS $ytID => $_ytName)
            {
                if($ytID == 0 || $ytID == "") continue;   // "" => annual result, put to last row of the year

                $assessTableField .= ", Score_{$ayID}_{$ytID} varchar(16)";

                // Special handling for standard score since it needs system calculation
                if($displayBy == "StandardScore")
                {
                    // Subject records
                    $insertAssessTableSqlArr = array_merge($insertAssessTableSqlArr, $li_pf->buildInsertTempAssessmentTableSQL_SD("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "IFNULL(SubjectComponentCode, '')", $default_student_id, $ayID, $ytID));
                    $selectAssessTableSql .= ", IF(t_am.Score_{$ayID}_{$ytID} = '--', '--', IFNULL(ROUND((t_am.Score_{$ayID}_{$ytID} - t_aam.Score_{$ayID}_{$ytID})/t_asdm.Score_{$ayID}_{$ytID}, 2), '--')) AS Score_{$ayID}_{$ytID}";
                }
                else
                {
                    // Subject records
                    //$insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "IFNULL(SubjectComponentCode, '')", $retrieveField, $default_student_id, $ayID, $ytID);
                    $insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "IFNULL(SubjectComponentCode, '')", $retrieveField, $default_student_id, $ayID, $ytID, false, $alwaysDisplayScoreIfValid);
                    $selectAssessTableSql .= ", Score_{$ayID}_{$ytID}";
                }
            }

            $assessTableField .= ($subjOverallRes) ? ", Score_{$ayID} varchar(16)" : "";

            // Special handling for standard score since it needs system calculation
            if($displayBy == "StandardScore")
            {
                // Subject records
                $insertAssessTableSqlArr = array_merge($insertAssessTableSqlArr, $li_pf->buildInsertTempAssessmentTableSQL_SD("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "IFNULL(SubjectComponentCode, '')", $default_student_id, $ayID, ""));
                $selectAssessTableSql .= ($subjOverallRes) ? ", IF(t_am.Score_{$ayID} = '--', '--', IFNULL(ROUND((t_am.Score_{$ayID} - t_aam.Score_{$ayID})/t_asdm.Score_{$ayID}, 2), '--')) AS Score_{$ayID}" : ", '--' AS Score_{$ayID}";
            }
            else
            {
                // Subject records
                //$insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "IFNULL(SubjectComponentCode, '')", $retrieveField, $default_student_id, $ayID, "");
                $insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "IFNULL(SubjectComponentCode, '')", $retrieveField, $default_student_id, $ayID, "", false, $alwaysDisplayScoreIfValid);
                $selectAssessTableSql .= ($subjOverallRes) ? ", Score_{$ayID}" : "";
            }
        }
    }
}

// Table object
// Temp table of assessment records
// Table name : tempAssessmentMark
if($displayBy == "StandardScore")
{
    //$li_pf->createTempAssessmentSDScoreTable(&$li_pf, $assessTableField, $insertAssessTableSqlArr);
    $li_pf->createTempAssessmentSDScoreTable($li_pf, $assessTableField, $insertAssessTableSqlArr);
    $score_sql = "SELECT ";
    $score_sql .= substr($selectAssessTableSql, 2)." ";
    $score_sql .= "FROM tempAssessmentMark t_am ";
    $score_sql .= "INNER JOIN tempAssessmentAvgMark t_aam ON t_am.SubjectCode = t_aam.SubjectCode AND t_am.SubjectComponentCode = t_aam.SubjectComponentCode ";
    $score_sql .= "INNER JOIN tempAssessmentSDMark t_asdm ON t_am.SubjectCode = t_asdm.SubjectCode AND t_am.SubjectComponentCode = t_asdm.SubjectComponentCode ";
    $score_sql .= "INNER JOIN tempAssessmentSubject t_as ON IF(t_am.SubjectCodeID IS NOT NULL AND t_am.SubjectCodeID != 0, t_am.SubjectCodeID = t_as.MainSubjCodeID, t_am.SubjectCode = t_as.MainSubjCode) AND t_am.SubjectComponentCode = t_as.CompSubjCode ";
    $score_sql .= "WHERE t_am.SubjectCode = '{$MainSubjectCode}' AND t_am.SubjectComponentCode = '{$CompSubjectCode}'";
}
else
{
    //$li_pf->createTempAssessmentMarkTable(&$li_pf, $assessTableField, $insertAssessTableSqlArr);
    $li_pf->createTempAssessmentMarkTable($li_pf, $assessTableField, $insertAssessTableSqlArr);
    $score_sql = "SELECT ";
    $score_sql .= substr($selectAssessTableSql, 2)." ";
    $score_sql .= "FROM tempAssessmentMark ";
    $score_sql .= "WHERE SubjectCode = '{$MainSubjectCode}' AND IFNULL(SubjectComponentCode, '') = '{$CompSubjectCode}'";
}

// Temp table of subjects
// Table name : tempAssessmentSubject
// $li_pf->createTempAssessmentSubjectTable(&$li_pf);
$li_pf->createTempAssessmentSubjectTable($li_pf);

// Score data for chart / table
$t_score_arr = current($li_pf->returnArray($score_sql));
foreach((array)$t_score_arr AS $key => $val)
{
    $year_sem_arr = explode("_", $key);

    switch(count($year_sem_arr))
    {
        case 2:
            $ayID = $year_sem_arr[1];
            $lay = new academic_year($ayID);
            $x_legend_arr['year'][] = $lay->Get_Academic_Year_Name();
            switch($val)
            {
                case "":
                    $score_arr['year'][] = "";
                    $line_dot_arr['year'][] = null;
                    break;
                case "--":
                    $score_arr['year'][] = "--";
                    $line_dot_arr['year'][] = null;
                    break;
                default:
                    $score_arr['year'][] = (float) $val;
                    $line_dot_arr['year'][] = (float) $val;
                    break;
            }
            break;
        case 3:
            $ayID = $year_sem_arr[1];
            $lay = new academic_year($ayID);

            $ytID = $year_sem_arr[2];
            $layt = new academic_year_term($ytID);
            $x_legend_arr['year_term'][] = $layt->Get_Year_Term_Name()."\n".$lay->Get_Academic_Year_Name();
            switch($val)
            {
                case "":
                $score_arr['year_term'][] = "";
                $line_dot_arr['year_term'][] = null;
                break;
            case "--":
                $score_arr['year_term'][] = "--";
                $line_dot_arr['year_term'][] = null;
                break;
            default:
                $score_arr['year_term'][] = (float) $val;
                $line_dot_arr['year_term'][] = (float) $val;
                break;
            }
        break;
            // Not accept numeric keys
            default:
            break;
    }
}

// Subject selection
$sql = "SELECT DISTINCT t_as.MainSubjCode, t_as.CompSubjCode, t_as.SubjName ";
$sql .= "FROM tempAssessmentMark t_am ";
$sql .= "INNER JOIN tempAssessmentSubject t_as ON IF(t_am.SubjectCodeID != '' AND t_am.SubjectCodeID IS NOT NULL, t_am.SubjectCodeID = t_as.MainSubjCodeID, t_am.SubjectCode = t_as.MainSubjCode) AND t_am.SubjectComponentCode = t_as.CompSubjCode ";
$sql .= "ORDER BY t_as.MainSubjOrder, t_as.CompSubjOrder";
$t_subject_arr = $li_pf->returnArray($sql);

for($i=0, $i_max=count($t_subject_arr); $i<$i_max; $i++)
{
    $_mainSubjCode = $t_subject_arr[$i]["MainSubjCode"];
    $_compSubjCode = $t_subject_arr[$i]["CompSubjCode"];
    $_subjName = ($_compSubjCode == "") ? $t_subject_arr[$i]["SubjName"] : "&nbsp;&nbsp;&nbsp;{$t_subject_arr[$i]["SubjName"]}";
  
    $subjectArr[] = array(
                        GEN_MISC_PARAMETER($_mainSubjCode, $_compSubjCode, $displayBy, $yearFilter),
                        $_subjName
                    );
}
$default_miscParameter = GEN_MISC_PARAMETER($MainSubjectCode, $CompSubjectCode, $displayBy, $yearFilter);
$subject_selection_html = getSelectByArray($subjectArr, "onChange=\"jCHANGE_DISPLAY(this.value)\"", $default_miscParameter, 0, 1, "", 2);

// Data table
$data_table_html = "<table bgcolor=\"#cccccc\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" width=\"100%\">\n";
$data_table_html .= "<tr class=\"tabletop\">\n";
$data_table_html .= "<td class=\"tabletopnolink\" width=\"50%\">{$yearTitle}</td>\n";
$data_table_html .= "<td class=\"tabletopnolink\" align=\"center\" width=\"50%\">{$totalTitle}</td>\n";
$data_table_html .= "</tr>\n";
if($yearFilter == 0)
{
    $x_legend_arr = $x_legend_arr['year_term'];
    $x_axis_title = $ec_iPortfolio['semester'];
    $line_dot_arr = $line_dot_arr['year_term'];

    for($i=0, $i_max=count($score_arr['year_term']); $i<$i_max; $i++)
    {
        $_yt = str_replace("\n", " : ", $x_legend_arr[$i]);
        $_score = $score_arr['year_term'][$i];

        $data_table_html .= "<tr><td>{$_yt}</td><td align=\"center\">{$_score}</td></tr>";
    }
}
else
{
    $x_legend_arr = $x_legend_arr['year'];
    $x_axis_title = $ec_iPortfolio['year'];
    $line_dot_arr = $line_dot_arr['year'];

    for($i=0, $i_max=count($score_arr['year']); $i<$i_max; $i++)
    {
        $_ay = $x_legend_arr[$i];
        $_score = $score_arr['year'][$i];

        $data_table_html .= "<tr><td>{$_ay}</td><td align=\"center\">{$_score}</td></tr>";
    }
}
$data_table_html .= "</table>";

# flash chart
// a dummy title to enlarge height to display top of the chart
// $title = new title( " " );

// $x_legend = new x_legend( $x_axis_title );
// $x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

// $x = new x_axis();
// $x->set_stroke( 2 );
// $x->set_tick_height( 2 );
// $x->set_colour( '#999999' );
// $x->set_grid_colour( '#CCCCCC' );
// $x->set_labels_from_array( $x_legend_arr );

// $y = new y_axis();
// $y->set_stroke( 2 );
// $y->set_tick_length( 2 );
// $y->set_colour( '#999999' );
// $y->set_grid_colour( '#CCCCCC' );
// $y->set_range( $y_axis_Min, $y_axis_Max, $y_axis_Step );
// $y->set_offset(true);

// $line0 = new line_dot();
// $line0->set_dot_size(5);
// $line0->set_values( $line_dot_arr );
// $line0->set_colour( '#72a9db' );
// $line0->set_tooltip( $totalTitle.':#val#' );
// $line0->set_key( $totalTitle, '12' );
// $line0->set_id( 0 );
// $line0->set_visible( true );

// $tooltip = new tooltip();
// //$tooltip->set_proximity();
// $tooltip->set_hover();
// $tooltip->set_stroke( 2 );
// //$tooltip->set_colour( "#ff0000" );
// //$tooltip->set_background_colour( "#ff00ff" ); 

// # show/hide checkbox panel
// //$key = new key_legend();
// //$key->set_visible(true);		

// $chart = new open_flash_chart();
// $chart->set_bg_colour( '#FFFFFF' );
// $chart->set_title( $title );
// $chart->set_x_legend( $x_legend );
// $chart->set_x_axis( $x );
// $chart->set_y_axis( $y );
// $chart->add_element( $line0 );
// $chart->set_tooltip( $tooltip );

# Build JSON data for highcharts
$xAxis = $json->encode(str_replace("\n"," ",$x_legend_arr));
// [2020-1009-1532-10207] fixed cannot display stat chart if Show by: By school year
// $data = implode(',',$score_arr['year_term']);
if($yearFilter == 0) {
    $data = $json->encode($score_arr['year_term']);
} else {
    $data = $json->encode($score_arr['year']);
}

$linterface->LAYOUT_START();
echo $linterface->Include_HighChart_JS();
?>

<script type="text/javascript">
xAxis = '<?=$xAxis?>';
xAxis = JSON.parse(xAxis);

$(function () {
    $('#my_chart').highcharts({
        title: {
            text: ''
//		    x: -20 //center
        },
//         subtitle: {
//             text: 'Source: WorldClimate.com',
//             x: -20
//         },
        xAxis: {
            categories:xAxis,
            title: {
                //text: '<?= $x_axis_title = $ec_iPortfolio['semester']?>'
                text: '<?= $x_axis_title ?>'
            },
        	plotLines: [{
//             	value: 0,
//             	width: 1,
            	color: '#CCCCCC'
       		}]
        },
        yAxis: {
            title: {
                text: ''
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
//         tooltip: {
//             valueSuffix: '°C'
//         },
        legend: {
            enabled:false,
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name:' ',
            data: <?=$data?>
        }],
        exporting: { enabled: false },
        credits: {
            enabled: false
        }
    });
});
</script>

<? // ===================================== Body Contents ============================= ?>
<link href="<?=$PATH_WRT_ROOT?>templates/<?= $LAYOUT_SKIN ?>/css/iportfolio/content.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/<?= $LAYOUT_SKIN ?>/css/iportfolio/iportfolio.css" rel="stylesheet" type="text/css">

<script language="JavaScript" type="text/JavaScript">

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	document.form1.action = "school_record_teacher.php";
	document.form1.submit();
}

// Quick student switch 
function jCHANGE_STUDENT(jParShift, jParToExtreme)
{
	var StudentSelect = document.getElementsByName('StudentID');
	var OriginalIndex = StudentSelect[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(!jParToExtreme)
	{
		if(TargetIndex >= StudentSelect[0].length)
		{
			StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			return;
		}
		else if(TargetIndex < 0)
		{
			StudentSelect[0].selectedIndex = 0;
			return;
		}
		else
        {
			StudentSelect[0].selectedIndex = TargetIndex;
        }
	}
	else
	{
		if(jParShift < 0)
		{
			if(OriginalIndex > 0) {
				StudentSelect[0].selectedIndex = 0;
            } else {
				return;
            }
		}
		else
		{
			if(OriginalIndex < StudentSelect[0].length-1) {
				StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
            } else {
				return;
            }
		}
	}
	
	document.form1.action = "school_record_teacher.php";
	document.form1.submit();
}

// Search student name
function jSUBMIT_SEARCH()
{
	document.form1.action = "../school_records_class.php";
	document.form1.submit();
}

function jCHANGE_DISPLAY(jMiscPara)
{
	document.form1.miscParameter.value = jMiscPara;
	document.form1.action = "school_record_teacher_stat.php";
	document.form1.submit();
}

function jCHANGE_RECORD_TYPE(jRecType)
{
	document.form1.RecordType.value = jRecType;
	if(typeof(document.form1.ChooseYear) != "undefined") {
		document.form1.ChooseYear.value = "";
    }
	document.form1.action = "school_record_teacher.php";
	document.form1.submit();
}

function jCHANGE_SUBJECT()
{
	document.form1.action = "school_record_teacher_stat.php";
	document.form1.submit();
}

function jCHANGE_LINK(jMiscParam)
{
	document.form1.miscParameter.value = jMiscParam;
	document.form1.action = "school_record_teacher_stat.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_LP()
{
	document.form1.action = "learning_portfolio_teacher.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_SI()
{
	document.form1.action = "student_info_teacher.php";
	document.form1.submit();
}

// Change page to display school base scheme
function jTO_SBS()
{
	document.form1.action = "sbs/index.php";
	document.form1.submit();
}
</script>

<!-------------------- Flash Chart -------------------->
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/js/json/json2.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/js/swfobject.js"></script>	
<script type="text/javascript">
// function ofc_ready(){
// 	//alert('ofc_ready');
// }

// function open_flash_chart_data(){
// 	//alert( 'reading data' );
// 	return JSON.stringify(data);

// 	//return "{\"elements\":[{\"type\":\"line_dot\",\"values\":[4,3,4,1,0,1],\"width\":2,\"colour\":\"#00FF21\",\"tip\":\"#val#\",\"id\":\"component\"}],\"bg_colour\":\"#ffffff\",\"title\":{\"text\":\"adam\",\"style\":\"{font-size: 12px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}\"},\"radar_axis\":{\"max\":5,\"colour\":\"#C1CBFF\",\"grid-colour\":\"#C1CBFF\",\"spoke-labels\":{\"labels\":[\"Like Green Color?\",\"Like Pink Color?\",\"Like Boy?\",\"Like Girl?\",\"Like Old Man?\",\"Like Young Woman?\"],\"colour\":\"#9F819F\"}},\"tooltip\":{\"mouse\":1}}";

	

// }

// function findSWF(movieName) {
//   if (navigator.appName.indexOf("Microsoft")!= -1) {
// 	return window[movieName];
//   } else {
// 	return document[movieName];
//   }
// }

// function setChart(id, display){
// 	//for testing
// 	var heading = document.getElementById("testHead");
// 	while(heading.hasChildNodes()){
// 		heading.removeChild(heading.firstChild);
// 	}
// 	var h = document.createTextNode(String(id));
// 	heading.appendChild(h);
// 	var content = document.getElementById("testContent");
// 	while(content.hasChildNodes()){
// 		content.removeChild(content.firstChild);
// 	}			
// 	var c = document.createTextNode(String(display));
// 	content.appendChild(c);
// }

//var data = <?php //echo $chart->toPrettyString(); ?>;

</script>
<script type="text/javascript">
//swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/open-flash-chart.swf", "my_chart", "550", "440", "9.0.0");
</script>

<FORM name="form1" method="POST" action="school_record_teacher.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan=2>
				<table border="0" cellspacing="0" cellpadding="3" width="100%">
					<tr>
						<td>
							<?=$class_selection_html?>
						</td>
						<td align="right">
							<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$ec_iPortfolio['enter_student_name']:intranet_htmlspecialchars(stripslashes($search_name)))?>" onFocus="if(this.value=='<?=$ec_iPortfolio['enter_student_name']?>'){this.value=''}" onBlur="if(this.value==''){this.value='<?=$ec_iPortfolio['enter_student_name']?>'}" />
							<input type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onClick="jSUBMIT_SEARCH()" />
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td class="navigation">
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../school_records.php"><?=$ec_iPortfolio['class_list']?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../school_records_class.php?YearClassID=<?=$YearClassID?>"><?=$class_name?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><?=Get_Lang_Selection($student_obj['ChineseName'],$student_obj['EnglishName'])?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="17" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_01_tea.gif" width="17" height="37"></td>
						<td height="37" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_03_tea.gif">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="200">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center" valign="middle" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_02_tea.gif"  class="page_title"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0" align="absmiddle"> <?=$ec_iPortfolio['school_record']?></td>
											</tr>
										</table>
									</td>
									<td align="left" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#E8E8E8">
											<tr>
												<td align="left">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr align="center" valign="middle">
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(-1, true)" class="tablebottomlink">&lt;&lt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(-1, false)" class="tablebottomlink">&lt;</a></td>
															<td nowrap>
																<span class="tabletext">
																	<?=$student_selection_html?>
																</span>
															</td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(1, false)" class="tablebottomlink">&gt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(1, true)" class="tablebottomlink">&gt;&gt;</a></td>
															<td width="15" nowrap> | </td>
															<td nowrap class="tabletext"><?=str_replace("<!--NoRecord-->", count($student_detail_arr), $ec_iPortfolio['total_record'])?></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_04_tea.gif" width="13" height="37"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif" width="17" height="20"></td>
						<td>
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<tr>
									<td valign="top">
										<table border="0" cellspacing="0" cellpadding="0" >
											<tr>
												<td>
													<table border="0" cellspacing="0" cellpadding="0" >
														<tr>
															<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_01.gif" style="padding-left:10px; padding-top:10px;"><?=$student_obj['PhotoLink']?></td>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="10" height="10"></td>
														</tr>
														<tr>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
															<td height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_04.gif" width="10" height="10"></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" class="tabletext"><?=Get_Lang_Selection($student_obj['ChineseName'],$student_obj['EnglishName'])?></td>
											</tr>
											<tr>
												<td align="center">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr>
															<td valign="top"><a href="#" onClick="jTO_SI()" title="<?=$ec_iPortfolio['heading']['student_info']?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_stu_info.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0"></a></td>
<?php
        if(in_array($default_student_id, $act_student_id_arr))
        {
            if($li_pf->HAS_SCHOOL_RECORD_VIEW_RIGHT())
            {
?>
															<td valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record_on.gif" alt="<?=$ec_iPortfolio['school_record']?>" width="20" height="20" border="0"></td>
<?php
		}
		if(strstr($ck_user_rights, ":web:") && $li_pf->HAS_RIGHT("learning_sharing"))
		{
?>
															<td valign="top">
																<a href="#" onClick="jTO_LP()" title="<?=$ec_iPortfolio['heading']['learning_portfolio']?>">
																	<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_learning_portfolio.gif" alt="<?=$iPort['menu']['learning_portfolio']?>" width="20" height="20" border="0"><br>
																	<?=$li_pf->GET_LP_NOT_VIEW_COUNT_BY_USER_ID($default_student_id) > 0 ? "<span class='new_alert'>New</span>" : ""?>
																</a>
															</td>
<?php
		}
		if(strstr($ck_user_rights, ":growth:") && $li_pf->HAS_RIGHT("growth_scheme"))
		{
?>
			<td valign="top"><a href="javascript:jTO_SBS()" title="<?=$iPort['menu']['school_based_scheme']?>" ><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_sbs.gif" alt="<?=$iPort['menu']['school_based_scheme']?>" width="20" height="20" border="0" ></a></td>
<?php
		}
	}
?>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
									<td>
										<table width="100%"  border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td>
													<?=$html_menu_tab?>
												</td>
											</tr>
											<tr>
												<td align="center">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td>
																<img src="<?=$PATH_WRT_ROOT?>/images/<?= $LAYOUT_SKIN ?>/iPortfolio/nav_arrow.gif" align="middle" height="15" width="15"><span class="navigation"><a href="#" onClick="jCHANGE_RECORD_TYPE('assessment')"><?=$ec_iPortfolio['overall_summary']?></a>
																<img src="<?=$PATH_WRT_ROOT?>/images/<?= $LAYOUT_SKIN ?>/iPortfolio/nav_arrow.gif" align="middle" height="15" width="15"></span><span class="tabletext"><?=$ec_iPortfolio['subject_chart']?></span>
															</td>
														</tr>
														<tr>
															<td><?=$subject_selection_html?></td>
															<td width='100%' align="right" valign="middle" class="thumb_list">
                                                                <?=$htmlNavArea?>
															</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td style="width:600px;" align="center" valign="top">
																<div id="my_chart" style="min-width: 310px; height: 400px; margin: 0 auto">no flash?</div>
															</td>
															<td align="left" valign="top">
																<table border="0" cellpadding="3" cellspacing="0" width="100%">
																	<tr align="left" valign="middle">
																		<td>&nbsp;</td>
																	</tr>
																	<tr align="left" valign="middle">
																		<td>
																			<table border="0" cellpadding="2" cellspacing="0" width="100%">
																				<tr class="tablebottom">
                                                                                    <td><?=$ec_iPortfolio['show_by']?></td>
                                                                                    <?=$filter_selection?>
                                                                                </tr>
																			</table>
																		</td>
																	</tr>
																	<tr align="left">
																		<td valign="top">
																			<div style="height: 300px; overflow-y: auto;">
																			<table  border="0" cellpadding="0" cellspacing="0" class="table_p" width="100%">
																				<tr>
																					<td>
																					    <?=$data_table_html?>
																					</td>
																				</tr>
																			</table>
																			</div>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif" width="13" height="37"></td>
					</tr>
					<tr>
						<td width="17" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_08.gif" width="17" height="17"></td>
						<td height="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif" width="64" height="17"></td>
						<td width="13" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_10.gif" width="13" height="17"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

<input type="hidden" name="RecordType" value=<?=$RecordType?> />
<input type="hidden" name="FieldChanged" />
<input type="hidden" name="displayBy" value="<?=$displayBy?>" />
<input type="hidden" name="miscParameter" />
<input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>" />
</FORM>
<? // ===================================== Body Contents (END) ============================= ?>

<? //getPageEnding() ?>

<?php
//include_once("$admin_root_path/src/portfolio/new_footer.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>