<?php

// Modifing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_opendb();

$linterface = new interface_html("popup.html");
$lpf = new libportfolio();

$lpf->CHECK_ACCESS_IPORTFOLIO();


/*************** Remarks ************************/

$csv_format_table = '';

$csv_format_1 = $ec_iPortfolio['column'].' 1 : <font color=red>*</font>'.$ec_iPortfolio['WebSAMSRegNo'].'  <br/><span class="tabletextremark">'.$Lang['AccountMgmt']['WebSAMSFieldReminder'].'</span>';
$csv_format_2 = $ec_iPortfolio['column'].' 2 : '.$ec_iPortfolio['nationality'];
$csv_format_3 = $ec_iPortfolio['column'].' 3 : '.$ec_iPortfolio['pob'];
$csv_format_4 = $ec_iPortfolio['column'].' 4 : '.$ec_iPortfolio['admission_date'].' <br/><span class="tabletextremark">'.$Lang['AccountMgmt']['DOBFieldReminder'].'</span>';

$csv_format_table .= '<table width="90%" border="0" cellspacing="0" cellpadding="1">
						<col width="25%"/>
						<col width="75%"/>
						<tr>
							<td style="vertical-align:top;nowrap">'.$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn'].' :</td>
							<td style="text-align:left;nowrap">'.$csv_format_1.'</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td style="text-align:left;nowrap">'.$csv_format_2.'</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td style="text-align:left;nowrap">'.$csv_format_3.'</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td style="text-align:left;nowrap">'.$csv_format_4.'</td>
						</tr>						
		
						<tr>
							<td colspan="2" style="border-top: 1px solid #E6E6E6;"><span class="tabletextremark">'.$i_general_required_field.'</span></td>
						</tr>
					  </table>';
						

/*************** End Remarks ********************/



$CurrentPage = "SAMS_import_student_data";
$title = $ec_iPortfolio['SAMS_import_student_data'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();
?>

<!-- Content Here -->

<FORM enctype="multipart/form-data" action="import_sams_update.php" method="POST" name="form1" onSubmit="this.btn_submit.disabled=true;">
	<table width="450" border="0" cellspacing="0" cellpadding="0">
		<tr>
          <td height="10" colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td align="right" class="tabletext"><?=$ec_iPortfolio['SAMS_CSV_file']?>&#65306;</td>
          <td><input class=file type=file name=userfile size=20></td>
        </tr>
    </table>
    <br />
<!--<span class="tabletextremark"><?=$ec_iPortfolio['SAMS_regno_import_remind']?></span>
	<br />-->
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td align="center"><?=$csv_format_table?></td>
	</tr>
	</table>
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td><a class="contenttool" href="download.php?FileName=student_data_sams_sample.csv"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_files/xls.gif" border="0" align="absmiddle" hspace="3"><?=$import_csv['download']?></a></td>
		<td align="right">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="submit" value="<?=$button_submit?>" name="btn_submit">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="reset" value="<?=$button_reset?>">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="button" value="<?=$button_cancel?>" onClick="self.close()">
		</td>
	</tr>
	</table>
	<input type="hidden" name="ClassName" value="<?=$ClassName?>" >
</FORM>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>