<?php
###########################################################################
# function to check wether the data is in the range
function checkRange($data, $GradeArr, $text_style_prefix, $text_style_suffix)
{
	global $lower, $upper, $ResultType;
	
	if($ResultType=="GRADE" && !empty($GradeArr))
	{
		$data = (in_array(strtolower(trim($data)), $GradeArr)) ? $text_style_prefix.$data.$text_style_suffix : $data;
	}
	else if($lower!="" && $upper!="")
	{
		$data = ($data>=$lower && $data<=$upper) ? $text_style_prefix.$data.$text_style_suffix : $data;
	}

	return $data;
}
function setRangeStyle($range_bold, $range_italic, $range_underline, $range_color, $r_color)
{
	if($range_bold==1)
	{
		$prefix0 = "<b>";
		$suffix0 .= "</b>";
	}
	if($range_italic==1)
	{
		$prefix1 = "<i>";
		$suffix1 = "</i>";
	}
	if($range_underline==1)
	{
		$prefix2 = "<u>";
		$suffix2 = "</u>";
	}
	if($range_color==1 && $r_color!="")
	{
		$prefixC = "<font color='".$r_color."'>";
		$suffixC = "</font>";
	}
	$text_style_prefix = $prefixC.$prefix0.$prefix1.$prefix2;
	$text_style_suffix = $suffix2.$suffix1.$suffix0.$suffixC;

	return array($text_style_prefix, $text_style_suffix);
}
function getScoreListResult($ResultType, $Year, $Semester, $ClassName)
{
	global $li_pf, $ec_iPortfolio, $eclass_db, $intranet_db, $ck_course_id;
	$CLASSNAME_FIELD = 'yc.ClassTitle'.strtoupper($_SESSION['intranet_session_language']);
		
//	$ck_course_id = 1274; // edit by eva temporarily
	$course_db = classNamingDB($ck_course_id);
	$sem_filter = ($Semester==$ec_iPortfolio['overall_result']) ? " AND a.IsAnnual = 1" : " AND a.Semester = '$Semester'";
	$sql = "SELECT DISTINCT
				a.UserID,
				CONCAT('(', c.class_number, ') ', CONCAT(c.firstname, '&nbsp;', c.lastname)) as DisplayName,
				a.SubjectCode,
				a.SubjectComponentCode,
				a.Score,
				a.Grade,
				a.OrderMeritForm
			FROM 
				{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as a
				LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT as b ON a.UserID = b.UserID
				LEFT JOIN {$course_db}.usermaster as c ON b.CourseUserID = c.user_id
				inner join {$intranet_db}.YEAR_CLASS_USER as ycu on a.UserID = ycu.UserID
				inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
				inner join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
				inner join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
					and a.Year = ay.YearNameEN
			WHERE 
				a.Year = '$Year'
				AND $CLASSNAME_FIELD = '$ClassName'
				AND c.user_id IS NOT NULL
				$sem_filter
			ORDER BY 
				DisplayName 
			";
	$row = $li_pf->returnArray($sql);

	for($i=0; $i<sizeof($row); $i++)
	{
		list($UserID, $StudentName, $SubjectCode, $SubjectComponentCode, $score, $grade, $rank) = $row[$i];
		if($score==0 && $grade=="" && $rank==0)
			$result = "--";
		else if($ResultType=="RANK")
			$result = $rank;
		else if($ResultType=="GRADE")
			$result = $grade;
		else
			$result = $score;

		$ResultArr[$UserID]["StudentName"] = $StudentName;
		if($SubjectComponentCode=="")
		{
			$ResultArr[$UserID][$SubjectCode]["major"] = $result;
		}
		else
		{
			$ResultArr[$UserID][$SubjectCode][$SubjectComponentCode] = $result;
		}
	}

	return $ResultArr;
}

function generateResultTable($ParPrint=0)
{
	global $li_pf, $button_print, $no_record_msg;
	global $Year, $Semester, $ResultType, $ClassName, $range_bold, $range_italic, $range_underline, $range_color, $r_color, $grade_highlight, $lower, $upper;

	$SubjectArr = $li_pf->returnAllSubjectWithComponents("", "'$ClassName'", $Year);
	if(!empty($SubjectArr))
	{
		$ResultArr = getScoreListResult($ResultType, $Year, $Semester, $ClassName);
		list($text_style_prefix, $text_style_suffix) = setRangeStyle($range_bold, $range_italic, $range_underline, $range_color, $r_color);
		
		$display_table_style = ($ParPrint) ? "border='1' bordercolor='#000000'" : "border='1' bordercolor='#999999'";
		$display_td_bg_color = ($ParPrint) ? "" : "bgcolor='#CDCDCD'";
		$x = "<table ".$display_table_style." cellpadding='5' cellspacing='0' width='100%'>";
		if(!empty($ResultArr))
		{
			if($ResultType=="GRADE")
			{
				$grade_highlight = strtolower($grade_highlight);
				$GradeArr = explode(",", $grade_highlight);
				$GradeArr = array_map('trim', $GradeArr);
			}
			$HeaderX = "";
			$ContentX = "";
			foreach($ResultArr as $UserID => $Data)
			{
				$HeaderX = "<tr ".$display_td_bg_color."><td>&nbsp;</td>";
				$ContentX .= "<tr><td nowrap='nowrap' class='text_11_black'>".$Data['StudentName']."</td>";
				for($i=0; $i<sizeof($SubjectArr); $i++)
				{
					list($SubjectCode, $SubjectComponentCode, $SubjectName, $SubjectComponentName) = $SubjectArr[$i];
					$have_score = $SubjectArr[$i]["have_score"];
					
					# display subject name
					$DisplaySubjectName = ($SubjectComponentCode=="") ? $SubjectName : $SubjectName."<br />".$SubjectComponentName;
					$HeaderX .= "<td nowrap='nowrap' width='10%' align='center' class='text_11_black'><b>".$DisplaySubjectName."</b></td>";
					
					# display subject result
					$data = ($SubjectComponentCode=="") ? $Data[$SubjectCode]["major"] : $Data[$SubjectCode][$SubjectComponentCode];
					$data = ($data=="" || ($ResultType=="SCORE" && $have_score==0)) ? "--" : checkRange($data, $GradeArr, $text_style_prefix, $text_style_suffix);
					
					$ContentX .= "<td align='center' class='text_11_black'>".$data."</td>";
				}
				$ContentX .= "</tr>";
				$HeaderX .= "</tr>";
			}
			$x .= $HeaderX;
			$x .= $ContentX;
			$x .= "</table>";
			$x .= ($ParPrint==0) ? "<br /><table border='0'cellpadding='0' cellspacing='0' width='100%'><tr><td align='right'><input type='button' name='print' value='$button_print' id='print' class='formbutton'  onmouseover=this.className='formbuttonon' onmouseout=this.className='formbutton' onClick=\"jPopPrint()\"></td></tr></table>" : "";

		}
		else
		{
			$x .= "<td align='center'><i>".$no_record_msg."</i></td>";
			$x .= "</table>";
		}
	}

	return $x;
}
############################################################################

?>
