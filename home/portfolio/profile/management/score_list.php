<?php
// using : Pun
/**
 * Change Log: 
 * 2016-08-30 Pun
 *  -   Added load highcharts.js
 * 2016-01-26 Pun
 * 	-	Added support for display both score, grade, merit
 * 	-	Added "Hide Subject Component" option
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");

intranet_auth();
intranet_opendb();

$li_pf = new libpf_asr();
$lpf_ui = new libportfolio_ui();
$lpf = new libportfolio();

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_AssessmentStatisticReport";
$CurrentPageName = $iPort['menu']['assessment_statistic_report'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

$ResultType = ($ResultType=="") ? "SCORE" : $ResultType;

########### Assess Right START ###########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab
$currentAcademicYearID = Get_Current_Academic_Year_ID();
$currentAcademicYear = getAcademicYearByAcademicYearID('','');

$ayterm_selection_html = '';
$academic_yearterm_arr = $li_pf->returnAssessmentSemester("", "", $currentAcademicYearID);
$ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", $yt_id, 0, 1, "", 2);

$classSelectionHTML = '<select name="ClassName">';
foreach($accessRight['classTeacher'] as $yearClass){
	$name = ($intranet_session_language=="EN") ? $yearClass["ClassTitleEN"] : $yearClass["ClassTitleB5"];
	$classSelectionHTML .= "<option value=\"{$name}\">{$name}</option>";
}
$classSelectionHTML .= '</select>';
########### Access Right END ###########


########################################################
# Operations : Start
########################################################
# retrieve distinct years
$YearArr = $li_pf->returnAssessmentYear();
$html_year_selection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='academicYearID'", $academicYearID, 0, 0, "", 2);

# get result type selection
$html_resulttype_selection = "<table border='0' cellpadding='3' cellspacing='0'>";
$html_resulttype_selection .= "<tr><td><span class='tabletext'><input type='checkbox' name='ResultType[]' id='type_score' value='SCORE' CHECKED><label for='type_score'>".$ec_iPortfolio['score']."</label></span></td><td width='30'>&nbsp;</td></tr>";
$html_resulttype_selection .= "<tr><td><span class='tabletext'><input type='checkbox' name='ResultType[]' id='type_grade' value='GRADE' CHECKED><label for='type_grade'>".$ec_iPortfolio['grade']."</label></span></td><td width='30'>&nbsp;</td></tr>";
$html_resulttype_selection .= "<tr><td><span class='tabletext'><input type='checkbox' name='ResultType[]' id='type_rank' value='RANK' CHECKED><label for='type_rank'>".$ec_iPortfolio['rank']."</label></span></td><td width='30'>&nbsp;</td></tr>";
$html_resulttype_selection .= "</table>";

# get tab menu
$TabIndex = "score_list";
$TabMenuArr = libpf_tabmenu::getAssessmentStatTags($TabIndex);
$html_tab_menu = $lpf_ui->GET_TAB_MENU($TabMenuArr);


##############################
# set hight light selection
if(!isset($range_color))
{
	$range_color = 1;
}
if(!isset($grade_highlight))
{
	$grade_highlight = "E, F";
}
if(!isset($upper) && !isset($lower))
{
	$upper = 50;
	$lower = 0;
}

//$table_style = "style='border-left:2px #CDCDCD solid; border-top:2px #CDCDCD solid;  border-right:2px #CDCDCD solid; border-bottom:2px #CDCDCD solid;'";


$range_table = "<table border='0' cellpadding='5' cellspacing='0' id='table_1' >
					<tr><td>".$ec_iPortfolio['selected_grade']."</td><td><input type='text' name='grade_highlight' value='".$grade_highlight."'/></td></tr>
					<tr><td colspan='2' class='guide'>".$ec_iPortfolio['highlight_grade_remind']."</td></tr>
					</table>
					";
// $range_table .= "<table border='0' cellpadding='5' cellspacing='0' id='table_2' $display_style2>
// 					<tr><td><input type='text' name='lower' value='".$lower."' class='tabletext' style='width: 35px;'/></td><td>".$profiles_to."</td><td><input type='text' name='upper' value='".$upper."' class='tabletext' style='width: 35px;' /></td><td>".$iPort["pts"]."</td></tr>
// 					</table>
// 					";
$RangeHighlightSelect = "<table border='0' cellpadding='5' cellspacing='0'>
						<tr><td colspan='4'>".$range_table."</td></tr>
						<tr>
							<td nowrap='nowrap'><input type='checkbox' name='range_bold' id='range_bold' value=1 ".($range_bold==1?'CHECKED':'')." /><label for='range_bold'>".$ec_iPortfolio['bold']."</label></td>
							<td nowrap='nowrap'><input type='checkbox' name='range_italic' id='range_italic' value=1 ".($range_italic==1?'CHECKED':'')." /><label for='range_italic'><i>".$ec_iPortfolio['italic']."</i></label></td>
							<td nowrap='nowrap'><input type='checkbox' name='range_underline' id='range_underline' value=1 ".($range_underline==1?'CHECKED':'')." /><label for='range_underline'><u>".$ec_iPortfolio['underline']."</u></label></td>
							<td colspan='3'><input type='checkbox' name='range_color' id='range_color' value='1' ".($range_color==1?'CHECKED':'')." /><label for='range_color'>".$ec_iPortfolio['color']."&nbsp;<select name='r_color'>
								<option value='RED' ".($r_color=='RED'?'SELECTED':'').">".$ec_iPortfolio['red']."</option>
								<option value='GREEN' ".($r_color=='GREEN'?'SELECTED':'').">".$ec_iPortfolio['green']."</option>
								<option value='BLUE' ".($r_color=='BLUE'?'SELECTED':'').">".$ec_iPortfolio['blue']."</option>
								</select></label>
							</td>
						</tr>
						</table>
						";

/*
if($view==1)
{
	include_once("score_list_common.php");
	$x = generateResultTable();
}
*/

?>
<script src="/templates/highchart/highcharts.js"></script>
<script language="javascript">
var loadingImage = new Image();
loadingImage.src = "/images/<?=$LAYOUT_SKIN?>/indicator.gif";

function get_yearterm_opt()
{
  var ay_id = $("[name=academicYearID]").val();
  
  if(ay_id == "")
  {
    $("#ayterm_cell").html("");
  }
  else
  {
  	$.ajax({
  		type: "GET",
  		url: "ajax_get_yearterm_opt.php",
  		data: "ay_id="+ay_id,
  		beforeSend: function () {
  		  $("#ayterm_cell").html('').append(
  		    $(loadingImage).clone()
        );
      },
  		success: function (msg) {
        $("#ayterm_cell").html(msg);
  		}
  	});
  }
}

function get_yearclass_opt()
{
  var ay_id = $("[name=academicYearID]").val();
  
  if(ay_id == "")
  {
    $("#yclass_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?>");
  }
  else
  {
  	$.ajax({
  		type: "GET",
  		url: "ajax_get_class_opt.php",
  		data: "ay_id="+ay_id,
  		beforeSend: function () {
  		  $("#yclass_cell").html('').append(
  		    $(loadingImage).clone()
        );
      },
  		success: function (msg) {
        $("#yclass_cell").html(msg);
  		}
  	});
  }
}

function get_score_list()
{
  if(checkform())
  {
  	$.ajax({
  		type: "POST",
  		url: "ajax_get_score_list2.php",
  		data: $("#form1").serialize(),
  		async: false,
  		success: function (msg) {
        $("#scoreListDiv").html(msg);
  		}
  	});
  }
}

function jChangeResultType(type)
{
	if(type=="SCORE" || type=="RANK")
	{
		displayTable('table_2', 'block');
		displayTable('table_1', 'none');
	}
	else if(type=="GRADE")
	{
		displayTable('table_1', 'block');
		displayTable('table_2', 'none');
	}
}

function checkform(){

  var obj = document.form1;
  var className = $("select[name=ClassName]").val();

	if(typeof className == "undefined" || className == "")
	{
		alert("<?=$ec_warning['please_select_class']?>");
		return false;
	}

	return true;
}

$(document).ready(function(){
  $("select[name=academicYearID]").change(function(){
    get_yearterm_opt();
    get_yearclass_opt();
  });
  
  $("#reset_btn").click(function(){
    $("select[name=academicYearID]").val('');
  
    get_yearterm_opt();
    get_yearclass_opt();
  });

  jChangeResultType($("input[name=ResultType]").val());

  $("input[name=ResultType]").click(function(){
    var resultType = $(this).val();
    jChangeResultType(resultType);
  });
});





/*
function jPopPrint()
{
	var url = "score_list_print.php";
	newWindow(url, 28);
	
	var obj = document.form1;
	obj.action = "score_list_print.php";
	obj.target = "ec_popup28";
	obj.submit();
	obj.action = "score_list.php";
}
*/
</script>

<form id="form1" name="form1" method="GET" action="score_list.php">
  <?=$html_tab_menu ?>
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr><td>
      <table border="0" cellpadding="5" cellspacing="0" width="100%" style="max-width: 1024px">
<!-------- Form START --------> 
<?php if($accessRight['admin']){ ?>
        <tr>
        	<td width='70'>&nbsp;</td>
          <td class="formfieldtitle" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['year']?></td>
        	<td width="75%">
            <table border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td><?=$html_year_selection?></td>
                <td nowrap='nowrap' id='ayterm_cell'>&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
        	<td  width='70'>&nbsp;</td>
          <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['class'] ?></td>
        	<td valign="top" id='yclass_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?></td>
        </tr>
<?php }else{ ?>
        <tr>
        	<td width='70'>&nbsp;</td>
          <td class="formfieldtitle" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['year']?></td>
        	<td width="80%">
            <table border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td>
                	<input type="hidden" name="academicYearID" value="<?=$currentAcademicYearID?>" />
                	<span><?=$currentAcademicYear?></span>&nbsp;&nbsp;
                </td>
                <td nowrap='nowrap' id='ayterm_cell'>
                	<?=$ayterm_selection_html?>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
        	<td  width='70'>&nbsp;</td>
          <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['class'] ?></td>
        	<td valign="top" id='yclass_cell'>
        		<?=$classSelectionHTML?>
        	</td>
        </tr>
<?php } ?>
        <tr>
        	<td  width='70'>&nbsp;</td>
          <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['display'] ?></td>
        	<td valign="top"><?=$html_resulttype_selection?></td>
        </tr>
        
		<tr>
        	<td  width='70'>&nbsp;</td>
			<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$i_general_others ?></td>
			<td valign="top">
				<input type="checkbox" id="showSubjectComponent" name="showSubjectComponent" value="1" checked="checked" />
				<label for="showSubjectComponent"><?=$Lang['iPortfolio']['DisplayComponentSubject'] ?></label>
			</td>
		</tr>
	
        <tr>
        	<td  width='70'>&nbsp;</td>
          <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['highlight_result']?> : </td>
        	<td valign="top"><?=$RangeHighlightSelect?></td>
        </tr>
        <tr>
          <td  colspan='3' class="dotline" height="1"><img src="images/2007a/10x10.gif" height="1" width="10"></td>
        </tr>
        
        <tr>
          <td colspan='3' height="50" align='center'>
            <input onClick='get_score_list()' class='formbutton' type='button' value='<?=$button_view?>' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
            <input type="button" id="reset_btn" class="formbutton" value="<?=$button_reset?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
          </td>
        </tr>
<!-------- Form END -------->
      </table>
    </td></tr>
  </table>
</form>

<div id="scoreListDiv">
<?php
if($view==1 && $x!="")
{
	echo $x;
}
?>
</div>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
