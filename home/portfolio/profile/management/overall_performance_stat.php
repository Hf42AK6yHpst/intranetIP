<?php
// Modifing by Pun
/**
 * Change Log:
 * 2017-02-24 Villa
 *  - #R113646 Add hidden field compare
 * 2016-08-30 Pun
 *  - Rewrite this page to sync with SDAS
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");

intranet_auth();
intranet_opendb();

$lpf = new libportfolio();
$li_pf = new libpf_asr();
$lpf_ui = new libportfolio_ui();

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_AssessmentStatisticReport";
$CurrentPageName = $iPort['menu']['assessment_statistic_report'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

$based_on = ($based_on=="") ? 1 : $based_on;
$chartType = ($chartType=="") ? 'column' : $chartType;

########### Assess Right START ###########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab
$currentAcademicYearID = Get_Current_Academic_Year_ID();
$currentAcademicYear = getAcademicYearByAcademicYearID('','');

$ayterm_selection_html = '';
// $academic_yearterm_arr = $li_pf->returnAssessmentSemester("", "", $currentAcademicYearID);
// $ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", $yt_id, 0, 1, "", 2);

$classTeacherClassIdArr = array();
$classSelectionHTML = '<select name="YearClassID[]" id="YearClassID">';
foreach($accessRight['classTeacher'] as $yearClass){
    $name = ($intranet_session_language=="EN") ? $yearClass["ClassTitleEN"] : $yearClass["ClassTitleB5"];
    $classSelectionHTML .= "<option value=\"{$yearClass["YearClassID"]}\">{$name}</option>";
    $classTeacherClassIdArr[] = $yearClass["YearClassID"];
}
$classSelectionHTML .= '</select>';

$classTeacherClassIdList = implode("','", $classTeacherClassIdArr);
$classTeacherClassIdList = "'{$classTeacherClassIdList}'";
########### Access Right END ###########

######## UI Releated START ########
$YearArr = $li_pf->returnAssessmentYear();
$html_year_selection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='academicYearID' id='academicYearID'", $currentAcademicYearID, 0, 0, "", 2);

//$html_classlevel_selection = getSelectByArray(array(), "name='YearID'", $YearID, 0, 0, "", 2);
######## UI Releated END ########


########################################################
# Operations : Start
########################################################
# get tab menu
$TabIndex = "overall_performance";
$TabMenuArr = libpf_tabmenu::getAssessmentStatTags($TabIndex);
$html_tab_menu = $lpf_ui->GET_TAB_MENU($TabMenuArr);
?>

<form id="form1" name="form1" method="get" action="overall_performance_stat.php">
<?=$html_tab_menu?>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
  <td>
    <table border="0" cellpadding="5" cellspacing="0" width="88%" align="center">
    
<!-------- Form START --------> 
<?php if($accessRight['admin']){ ?>
    <tr>
      <td class="formfieldtitle" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['year']?></td>
    	<td width="80%">
        <table border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td><?=$html_year_selection?></td>
            <td nowrap='nowrap' id='ayterm_cell'><?=$ayterm_selection_html?></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio_Report['form']?></td>
    	<td valign="top" id='y_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?></td>
    </tr>
    <tr>
      <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['class'] ?></td>
    	<td valign="top" id='yclass_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?></td>
    </tr>
    <tr>
      <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['SAMS_subject']?> : </td>
    	<td valign="top" id='subj_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?></td>
    </tr>
    <tr>
        <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['based_on']?> : </td>
    	<td valign="top">
    		<table border="0" cellspacing="0" cellpadding="3">
    		<tr>
    			<td width="100"><span class="tabletext"><input type="radio" id="based_on_1" name="based_on" value="1" <?=($based_on==1?"checked='checked'":"")?>/><label for="based_on_1"><?=str_replace("<!--base-->",$ec_iPortfolio['SAMS_subject'],$iPort["stat_by"])?></label></span></td>
    		</tr>
    		<tr>
    			<td width="100"><span class="tabletext"><input type="radio" id="based_on_0" name="based_on" value="0" <?=($based_on==0?"checked='checked'":"")?>/><label for="based_on_0"><?=str_replace("<!--base-->",$ec_iPortfolio['class'],$iPort["stat_by"])?></label></span></td>
    		</tr>
    		</table>
    	</td>
    </tr>
<?php } else{ ?>
	<tr>
		<td class="formfieldtitle" nowrap="nowrap" valign="top">
			<?=$ec_iPortfolio['year']?>
		</td>
		<td width="80%">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<input type="hidden" name="academicYearID" id="academicYearID" value="<?=$currentAcademicYearID?>" />
						<?=$currentAcademicYear?>&nbsp;&nbsp;
					</td>
					<td nowrap='nowrap' id='ayterm_cell'><?=$ayterm_selection_html?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['class'] ?></td>
		<td valign="top"><?=$classSelectionHTML?></td>
	</tr>
    <tr>
      <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['SAMS_subject']?> : </td>
    	<td valign="top" id='subj_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?></td>
    </tr>
    <input type="hidden" id="based_on_1" name="based_on" value="1" />
<?php } ?>

    <tr>
        <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['chart_type']?> : </td>
    	<td valign="top">
    		<table border="0" cellspacing="0" cellpadding="3">
    		<tr>
    			<td width="100"><span class="tabletext"><input type="radio" id="barchart" name="chartType" value="column" <?=($chartType=='column'?"checked='checked'":"")?>/><label for="barchart"><?=$ec_iPortfolio['barchart']?></label></span></td>
    		</tr>
    		<tr>
    			<td width="100"><span class="tabletext"><input type="radio" id="linechart" name="chartType" value="line" <?=($chartType=='line'?"checked='checked'":"")?>/><label for="linechart"><?=$ec_iPortfolio['linechart']?></label></span></td>
    		</tr>
    		</table>
    	</td>
    </tr>
    
     <tr>
    	<td class="dotline" height="1"><img src="images/2009a/10x10.gif" height="1" width="10"></td>
    	<td class="dotline" height="1"><img src="images/2009a/10x10.gif" height="1" width="10"></td>
    </tr>
    
    <tr><td colspan='2' height="50" align="center">
    <input type="button" id="viewForm" value='<?=$button_view?>' class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
    <!--input type="button" id="reset_btn" class="formbutton" value="<?=$button_reset?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"-->
    </td></tr>
    </table>
  </td></tr>
<!-------- Form END -------->
</table>
<br /><br />
	<div style="text-align:center"><div id="overallPerformancDiv"></div></div>
	<input type="hidden" id="compare_averager" name="compare" value="avgScore">
</form>



<script src="/templates/highchart/highcharts.js"></script>
<script type="text/javascript">
var data;
var loadingImage = new Image();
loadingImage.src = "/images/<?=$LAYOUT_SKIN?>/indicator.gif";

$(function(){
	function get_yearterm_opt(){
		var ay_id = $('#academicYearID').val();
		if(ay_id == ''){
			$("#ayterm_cell").html("");
			return;
		}

		$.ajax({
			type: "GET",
			url: "/home/portfolio/profile/management/ajax_get_yearterm_opt.php",
			data: "ay_id="+ay_id,
			success: function (msg) {
				$("#ayterm_cell").html(msg);
			}

	
		});
	}
	function get_year_opt(){
		var ay_id = $('#academicYearID').val();
		
		$("#yclass_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?>");
		$("#subj_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>");
		if(ay_id == ''){
			$("#y_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?>");
			return;
		}

		$.ajax({
			type: "GET",
			url: "/home/portfolio/profile/management/ajax_get_year_opt.php",
			data: "ay_id="+ay_id,
			success: function (msg) {
				$("#y_cell").html(msg);
				$("#y_cell").find('[name="yearID"]').change(get_yearclass_multiopt);
			}

		
			
		});
	
	
	}
	function get_yearclass_multiopt()
	{
		var y_id = $("[name=yearID]").val();
		var ay_id = $("#academicYearID").val();
	
		$("#subj_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>");
		if(typeof y_id == "undefined" || y_id == ""){
			$("#yclass_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?>");
			return;
		}

		$("#yclass_cell").html('').append(
			$(loadingImage)
		);
		
		$.ajax({
			type: "GET",
			url: "/home/portfolio/profile/management/ajax_get_class_multiopt2.php",
			data: "y_id="+y_id+"&ay_id="+ay_id,
			success: function (msg) {
				$("#yclass_cell").html(msg);
				$("#yclass_cell").find('.checkMaster').change(checkAll);
				$("#yclass_cell").find('input[type="checkbox"]').change(updateSubjectSelectDisplay);
			//$("#yclass_cell").attr("checked", "checked");
		
	//		$("#yclass_cell").attr("checked", "checked");
			$("#YCCheckMaster.checkMaster").attr("checked", "checked");
			$(".yearClassId").attr("checked", "checked"); 
			$("#yclass_cell").find('.checkMaster').change();
	}

		});
		
	
		
	}

	function updateSubjectSelectDisplay(){
		if($('#yclass_cell').find('.yearClassId:checked').length == 0){
			$("#subj_cell").html('<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>');
			return;
		}
		$("#subj_cell").html('').append(
			$(loadingImage)
		);

		var selfClassArr = [<?=$classTeacherClassIdList ?>];

		var isOnlySelfClassChecked = true;
		$('#yclass_cell').find('.yearClassId:checked').each(function(index, ele){
			if($.inArray($(ele).val(), selfClassArr) == -1){
				isOnlySelfClassChecked = false;
				return false; // Break $.each()
			}
		});

		var filterSubjectArr = [];
		<?php if($isSubjectPanelView){ ?>
			if(!isOnlySelfClassChecked){
				var subjectPanelAccessRightList = <?=$subjectPanelAccessRightList ?>;
				filterSubjectArr = subjectPanelAccessRightList[ $('[name="yearID"]').val() ];
				if( typeof(filterSubjectArr) == 'undefined'){
					filterSubjectArr = [0];
				}
			}
		<?php } ?>
		
		get_subject_multiopt(filterSubjectArr);
	}
	function get_subject_multiopt(filterSubjectArr)
	{
		filterSubjectArr = filterSubjectArr || [];

		var y_id = $("[name=yearID]").val();
		var ay_id = $("#academicYearID").val();
	
		if(typeof y_id == "undefined" || y_id == "")
		{
			$("#subj_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>");
			return;
		}

		$.ajax({
			type: "GET",
			url: "/home/portfolio/profile/management/ajax_get_subject_multiopt.php",
			data: {
				'ay_id': ay_id,//'<?=$currentAcademicYearID?>',
				'y_id': y_id,
				'yearClassId': $(this).val(),
				'filterSubject[]': filterSubjectArr
			},
			success: function (msg) {
				if($('#yclass_cell').find('.yearClassId:checked').length == 0){
					$("#subj_cell").html('<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>');
					return;
				}

				$("#subj_cell").html(msg);
				$("#subj_cell").find('.checkMaster').change(checkAll);

				
			
 //				$("#subj_cell").attr("checked", "checked");
 				$("#SubjCheckMaster.checkMaster").attr("checked", "checked");
 				$(".subjectChk").attr("checked", "checked"); 
				
			}
		});

	
	}

	function checkAll(){
		var checked = $(this).attr("checked");
		$(this).parent().parent().parent().find("input[name]").attr("checked", checked);
	};
	
	$('#academicYearID').change(function(){
		$("#ayterm_cell").html('').append(
			$(loadingImage).clone()
		);
		$("#y_cell").html('').append(
			$(loadingImage)
		);
		
		get_yearterm_opt();
		get_year_opt();
	});
	
	
	$("#reset_btn").click(function(){
		$("select[name=academicYearID]").val('');

		$("#ayterm_cell").html('');
		$("#y_cell").html('');


	
		get_yearclass_multiopt();
		get_subject_multiopt();
	});


	$('#viewForm').click(get_overall_performace);
	function get_overall_performace()
	{
		var $form = $('#form1');
		if($form.find('.yearClassId').length > 0 && $form.find('.yearClassId:checked').length==0)
		{
			alert("<?=$ec_warning['please_select_class']?>");
			return false;
		}
		else if($form.find('#subj_cell [name]:checked').length==0)
		{
			alert("<?=$ec_warning['please_select_subject']?>");
			return false;
		}
		
	  	$.ajax({
	  		type: "POST",
	  		url: "/home/portfolio/profile/management/ajax_get_overall_performace2.php",
	  		data: $form.serialize(),
	  		beforeSend: function () {
	  		  $("#overallPerformancDiv").parent().children().remove().end().append(
	          $("<div></div>").attr("id", "overallPerformancDiv").append(
	  		      $(loadingImage)
	          )
	        );
	      },
	  		success: function (msg) {
	  			$("#overallPerformancDiv").html(msg);
	  		}
	  	});
	}


	////////For Class Teacher START ////////
	$('#YearClassID').change(getClassTeacherSubject);
	function getClassTeacherSubject(){
		$.ajax({
			type: "GET",
			url: "/home/portfolio/profile/management/ajax_get_subject_multiopt.php",
			data: {
				'ay_id': '<?=$currentAcademicYearID?>',
				'y_id': '',
				'yearClassId': $('#YearClassID').val()
			},
			beforeSend: function () {
				$("#subj_cell").html('').append(
					$(loadingImage)
				);
			},
			success: function (msg) {
				$("#subj_cell").html(msg);
				$("#subj_cell").find('.checkMaster').change(checkAll);
				
				
			}
		});
	}
	
// 	$('#yclass_cell > select').change();
//	 $("#YCCheckMaster.checkMaster").attr("checked", "checked"); 

	////////For Class Teacher END ////////
	$('#academicYearID').change();
	<?php if(!$accessRight['admin']){ ?>
		getClassTeacherSubject();
	<?php } ?>
	
//	$("#subj_cell").find('.checkMaster').change();	
//	$("#yclass_cell").attr("checked", "checked");

	
});
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
