<?php
//Using: 
/**
 * Change Log:
 * 2019-10-03 Philips [2019-0917-1535-36207]
 *  -	Removed subject relation checking
 * 2017-05-29 Villa #T117443 
 * 	-	Fix Subject Filter Problem
 * 2017-05-10 Villa #T116799 
 *  -	Fix getting wrong y_id when  $class['ClassTitleEN']|| $class['ClassTitleB5'] is NULL
 * 2017-03-22 Villa #L114339 
 *  -	Return No subject if no subject data return
 * 2016-08-30 Pun
 *  -	Added $filterEmptyScoreSubject for returnAllSubjectWithComponents()  
 * 2016-06-23 Cara
 *  -	Added class(subjectChk)
 * 2016-03-30 Pun
 * 	-	Added Filter Subject for Year
 * 2016-01-26 Pun
 * 	-	Added Filter Subject
 */

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/cust/analysis_system_lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$subject_selection_html = '';

#### Get yearID for class teacher START ####
if(($yearClass || $yearClassId) && !$y_id){
	$objYear = new Year();
	$classArr = $objYear->Get_All_Classes();
	foreach((array)$classArr as $class){
		if($yearClass){
			if($class['ClassTitleEN'] == $yearClass || $class['ClassTitleB5'] == $yearClass){
				$y_id = $class['YearID'];
				break;
			}
		}elseif($yearClassId){
			if($class['YearClassID'] == $yearClassId){
				$y_id = $class['YearID'];
				break;
			}
			
		}
// 		if($class['ClassTitleEN'] == $yearClass || $class['ClassTitleB5'] == $yearClass || $class['YearClassID'] == $yearClassId){
// 			$y_id = $class['YearID'];
// 			break;
// 		}
	}
	$subject_selection_html .= '<input type="hidden" name="yearID" value="'.$y_id.'" />';
}
#### Get yearID for class teacher END ####

$li_pf = new libpf_asr();
$lpf = new libportfolio();

#### Include Group Subject START ####
$includeCodeIdArr = array_keys((array)$sys_custom['iPortfolio_Group_Subject']);
#### Include Group Subject END ####

#### Remove Group Subject START ####
## MSSCH Cust START ##
$removeCodeIdArr = array();
foreach ((array)$sys_custom['iPortfolio_Group_Subject'] as $codeArr){
	$removeCodeIdArr = array_merge($removeCodeIdArr, $codeArr);
}
## MSSCH Cust END ##
#### Remove Group Subject END ####

$subj_arr = $li_pf->returnAllSubjectWithComponents($ay_id, $y_id, $includeCodeIdArr, $removeCodeIdArr, true);

#### Filter Subject START ####

// ## Remove subject releation START ##
// $sql = "SELECT 
// 	SYR.SubjectID
// FROM 
// 	SUBJECT_YEAR_RELATION SYR
// WHERE
// 	SYR.YearID = '{$y_id}'
// ";
// $rs = $lpf->returnVector($sql);
// if(count($filterSubject) > 0){
// 	$filterSubject = array_intersect($filterSubject, $rs);
// }else{
// 	$filterSubject = $rs;
// }
## Remove subject releation END ##

if(count($filterSubject) > 0){
// Remove the if condition to handle the situation that there are no $filterSubject pass in(No subject should be show) Villa
$filterResultArr = array();
foreach((array)$subj_arr as $subj){
	$subjectID = $subj['SubjectID'];
	if(in_array($subjectID, (array)$filterSubject)){
		$filterResultArr[] = $subj;
	}
}
$subj_arr = $filterResultArr;
}
#### Filter Subject END ####

$subject_selection_html .= "<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" style=\"background: #EEEEEE\">";
if(count($subj_arr)){
	$subject_selection_html .= "<tr><td colspan=\"3\"><input type=\"checkbox\" id=\"SubjCheckMaster\" class=\"checkMaster\" /> <label for=\"SubjCheckMaster\" class=\"checkMaster\">{$button_check_all}</label></td></tr>";
	for($i=0; $i<ceil(count($subj_arr)/3)*3; $i++)
	{
	  $_subj = $subj_arr[$i];
	  $_subj_code = $_subj["SubjectCode"];
	  $_subj_code .= ($_subj["SubjectComponentCode"] == "") ? "" : "###".$_subj["SubjectComponentCode"];
	  $_subj_name = $_subj["SubjectName"];
	  $_subj_name .= ($_subj["SubjectComponentName"] == "") ? "" : " - ".$_subj["SubjectComponentName"];
	  
	  if($i % 3 == 0) {
	    $subject_selection_html .= "<tr>";
	  }
	  
	  if(!empty($_subj)){
	    $subject_selection_html .= "<td width=\"200\"><input type=\"checkbox\" id=\"subj_{$i}\" name=\"Subjects[]\" value=\"{$_subj_code}\" class=\"subjectChk\" /> <label for=\"subj_{$i}\">{$_subj_name}</label></td>";
	  }
	  else {
	    $subject_selection_html .= "<td width=\"200\">&nbsp;</td>";
	  }
	  
	  if($i % 3 == 2) {
	    $subject_selection_html .= "</tr>";
	  }
	}
}else{
	$subject_selection_html .= "<tr>"."<td>".$Lang['SDAS']['Error']['noSubject']."</td>"."</tr>";
}
$subject_selection_html .= "</table>";

echo $subject_selection_html;

intranet_closedb();
?>