<?php
/* 
 * Modification Log:
 * 	20110902 (Ivan)
 * 	- added warning display if there are new activation of students
 */

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");

$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Profile:Student"));
iportfolio_auth("T");
intranet_opendb();


$linterface = new interface_html();
$li_pf = new libpf_sturec();
$li_pf->LOAD_KEYS_FROM_SETTING();
/*
# count the number of account that is activated but suspended
$suspended = 0;
for($i=0; $i<sizeof($user_id); $i++)
{
	$temp = $li_pf->checkIsSuspended($user_id[$i]);
	if($temp==1)
		$suspended++;
}
*/
$StudentToReactivate = 0;
$UserIDArr = (is_array($user_id)) ? $user_id : explode(",", $user_id);
for($i=0; $i<count($UserIDArr); $i++)
{
	if($li_pf->IS_ACCOUNT_SUSPENED($UserIDArr[$i]))
		$StudentToReactivate++;
}

$user_id_list = (is_array($user_id)) ? implode(",", $user_id) : $user_id;
$user_total_now = (is_array($user_id)) ? sizeof($user_id) : sizeof(explode(",", $user_id));
$account_quota_free = $li_pf->quota["free"];
$account_quota_afterward = $li_pf->quota["free"] - $user_total_now + $StudentToReactivate;
$account_quota_afterward_html = ($account_quota_afterward<0) ? "<font color='red'>".$account_quota_afterward."</font>" : $account_quota_afterward;

$msg_no_quota_html = ($account_quota_afterward<0) ? $ec_iPortfolio['activation_no_quota'] : "";

$numOfNewActivate = $user_total_now - $StudentToReactivate;
if ($numOfNewActivate > 0) {
	$h_warningTable = $linterface->GET_WARNING_TABLE($Lang['iPortfolio']['OLEArr']['ActivateStudentWarning']);
}


# Page heading setting
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];
### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");
$linterface->LAYOUT_START();

?>


<!-- ===================================== Body Contents ============================= -->

<script language="JavaScript">
function checkform(objForm){
	if (confirm("<?=$ec_warning['portfolio_activate']?>"))
	{
		return true;
	} else
	{
		return false;
	}
}

function jBACKTO_LIST(){
	document.form1.action = "../../school_records_class.php";
	document.form1.submit();
}
</script>

<FORM action="student_activate_update.php" method="POST" name="form1" onSubmit="return checkform(this)">
	<table border="0" cellspacing="0" cellpadding="5" align="center" width="90%">
		<tr><td colspan="2" align="center"><?=$h_warningTable?></td></tr>
	</table>
	<br />
	<table border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td align="right" nowrap class="tabletext"><?=$ec_iPortfolio['account_quota_free']?>:</td>
			<td class="tabletext"><?=$account_quota_free?></td>
		</tr>
		<tr>
			<td align="right" nowrap class="tabletext"><?=$ec_iPortfolio['activation_student_activate']?>:</td>
			<td class="tabletext"><?=($user_total_now-$StudentToReactivate)?></td>
		</tr>
		<tr>
			<td align="right" nowrap class="tabletext"><?=$ec_iPortfolio['activation_student_reactivate']?>:</td>
			<td class="tabletext"><?=$StudentToReactivate?></td>
		</tr>
		<tr>
			<td align="right" nowrap class="tabletext"><?=$ec_iPortfolio['activation_quota_afterward']?>:</td>
			<td class="tabletext"><?=$account_quota_afterward_html?></td>
		</tr>
	</table>
	<p><font color='red'><?= $msg_no_quota_html ?></font></p>
	<br />
	<hr align="center" width="90%" />
	<table width="90%" align="center" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td>&nbsp;</td>
			<td align="right">
				<?php if ($account_quota_afterward>=0 && $user_total_now>0) { ?>
				<input class="formbutton" type="submit" value="<?=$button_submit?>">
				<?php } ?>
				<input class="formbutton" type="button" value="<?=$button_cancel?>" onClick="jBACKTO_LIST()">
			</td>
		</tr>
	</table>
	
	<input type="hidden" name="user_id" value="<?=$user_id_list?>" />
	<input type="hidden" name="YearClassID" value="<?=$YearClassID?>" />
	<input type="hidden" name="DisplayType" value="list" >
</FORM>

<!-- ===================================== Body Contents (END) ============================= -->


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
