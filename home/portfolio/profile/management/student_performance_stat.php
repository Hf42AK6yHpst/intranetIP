<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();


$lpf = new libpf_asr();
$luser = new libuser($UserID);

# define the navigation, page title and table size
// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_AssessmentStatisticReport";
$CurrentPageName = $iPort['menu']['assessment_statistic_report'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();


// $ck_course_id = "1274"; //setting temporarily
// retrieve all subjects
$SubjectArr = $lpf->returnAllSubjectWithComponents();
$SubjectCode = ($SubjectCode=="") ?  $SubjectArr[0][0] : $SubjectCode;

$temp = explode("###", $SubjectCode);
if(sizeof($temp)>1)
{
	if($SubjectComponentCode=="")
	{
		$SubjectCode = trim($temp[0]);
		$SubjectComponentCode = trim($temp[1]);
	}
}

# get subject selection
list($SubjectSelection, $SubjectName) = $lpf->getSubjectSelection($SubjectArr, "name='SubjectCode' onChange='document.form1.submit()'", $SubjectCode, $SubjectComponentCode, 0, 1);

////////////////////////////////////////////////////////////////
// retrieve distinct years
$YearArr = $lpf->returnAssessmentYear($SubjectCode, $SubjectComponentCode);
//$Year = ($Year=="" || !in_multi_array($Year, $YearArr)) ? $YearArr[0][0] : $Year;

$year_selection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($YearArr, "name='Year' onChange='document.form1.submit();'", $ec_iPortfolio['all_school_year'], $Year);

////////////////////////////////////////////////////////////////
// retrieve distinct class
$ClassArr = $lpf->returnAssessmentClassGroupByForm($SubjectCode, $SubjectComponentCode, $Year);
if($ClassName=="" || !in_multi_array($ClassName, $ClassArr))
{
	for($i=0; $i<sizeof($ClassArr); $i++)
	{
		if($ClassArr[$i][0]!="")
		{
			$ClassName = $ClassArr[$i][0];
			break;
		}
	}
}
$class_selection = (sizeof($ClassArr)==0) ? "<i>".$no_record_msg."</i>" : returnSelection($ClassArr, $ClassName, "ClassName", "onChange='document.form1.submit();'");

///////////////////////////////////////
// retrieve student list
$StudentArr = $lpf->returnAssessmentStudentByClass($Year, $ClassName, $SubjectCode, $SubjectComponentCode);
$StudentID = ($StudentID=="" || !in_multi_array($StudentID, $StudentArr)) ? $StudentArr[0][0] : $StudentID;
//$student_selection = (sizeof($StudentArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($StudentArr, "name='StudentID' onChange='document.form1.submit();'", "", $StudentID);
$TotalStudentArr = count($StudentArr);
$student_selection = (sizeof($StudentArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($StudentArr, "name='StudentID' onChange='document.form1.submit();'", $StudentID,0,1);

# Setting Student List Tools
$StudentIDNext = $lpf->CHANGE_STUDENT_LIST_RECORD($StudentArr,$StudentID,1);
$StudentIDPre = $lpf->CHANGE_STUDENT_LIST_RECORD($StudentArr,$StudentID,0);
$StudentIDHead = $StudentArr[0][0];
$StudentIDTail = $StudentArr[$TotalStudentArr-1][0];


$displayBy = ($displayBy=="") ? "Score" : $displayBy;
$yearTitle = $ec_iPortfolio['year']."(".$ec_iPortfolio['semester'].")";

# check wether the subject has score
$have_score = $lpf->checkSubjectScoreExist($SubjectCode, $SubjectComponentCode);

// generate the class performance table
$student_data = ($displayBy=="Score" && $have_score==0) ? array() : $lpf->returnAsssessmentSubjectRecordBySubjectCode($StudentID, $SubjectCode, $filter, $SubjectComponentCode, $Year);
$showValue = ($displayBy=="Rank") ? "OrderMeritForm" : $displayBy;

list($style_url, $axis_url, $chart_data) = $lpf->getAssessmentChartData($student_data, $SubjectCode, $SubjectComponentCode, $StudentID, $filter, $displayBy, $SubjectName);

$url_swf = "http://$eclass_httppath/src/includes/swf/zxchart_600x400.swf";
$param_send = "stylefile=http://$eclass_httppath/src/includes/swf/portfolio/style.php?$style_url&datafile=$eclass_httppath/src/includes/swf/portfolio/axis.php?$axis_url". $chart_data;

// prepare legend
$data_table = "";
if(sizeof($student_data)>0)
{
	
	for($j=0; $j<sizeof($student_data); $j++)
	{
		$bclass = ($j%2==0) ? "class='tablerow1'" : "class='tablerow2'";
		$data_table .= "<tr $bclass>
							<td width='20' height='20' class='tabletext' nowrap>".($j+1)."</td>
							<td width='94' height='20' class='tabletext' nowrap>".$student_data[$j]['YearSem']."</td>
							<td width='50' height='20' class='tabletext'>".$student_data[$j][$showValue]."</td>
						</tr>";
	}
}
else
{
	$data_table .= "<tr class='tablerow1' height='40'><td colspan=3 align='center'  class='tabletext'>".$no_record_msg."</td></tr>";
}

# Create selection (by semester/ by year)

$filter_selection = "<td nowrap='nowrap'><label><input name='filter' id='filter' value='1' type='radio' ".($filter==1?"checked":"")." onclick='document.form1.submit()'> </label>".$ec_iPortfolio['by_year']."</td>";
$filter_selection .= "<td nowrap='nowrap' valign='top'><label><input name='filter' id='filter' value='0' type='radio' ".($filter==0?"checked":"")." onclick='document.form1.submit()'> </label>".$ec_iPortfolio['by_semester']."</td>";


#################
# define the buttons according to the variable $displayBy
$bcolor_score = ($displayBy=="Score") ? "#CFE6FE" : "#FFD49C";
$bcolor_rank = ($displayBy=="Rank") ? "#CFE6FE" : "#FFD49C";
$bcolor_stand_score = ($displayBy=="StandardScore") ? "#CFE6FE" : "#FFD49C";

$gif_score  = ($displayBy=="Score") ? "l" : "o";
$gif_rank  = ($displayBy=="Rank") ? "l" : "o";
$gif_stand_score  = ($displayBy=="StandardScore") ? "l" : "o";

$param = "&view=1&SubjectCode=$SubjectCode&SubjectCmpCode=$SubjectCmpCode&Year=$Year&ClassName=$ClassName&StudentID=$StudentID";
$param2 = "&view=1&SubjectCode=$SubjectCode&SubjectCmpCode=$SubjectCmpCode&Year=$Year&ClassName=$ClassName&displayBy=$displayBy";
$link_score = ($displayBy=="Score") ? "<span>".$ec_iPortfolio['display_by_score']."</span>" : "<a href='student_performance_stat.php?displayBy=Score".$param ."'>".$ec_iPortfolio['display_by_score']."</a>";

$link_rank = ($displayBy=="Rank") ? "<span>".$ec_iPortfolio['display_by_rank']."</span>" : "<a href='student_performance_stat.php?displayBy=Rank".$param ."'>".$ec_iPortfolio['display_by_rank']."</a>";

$link_stand_score = ($displayBy=="StandardScore") ? "<span>".$ec_iPortfolio['display_by_stand_score']."</span>" : "<a href='student_performance_stat.php?displayBy=StandardScore".$param ."'>".$ec_iPortfolio['display_by_stand_score']."</a>";

###################################

// Tab Menu Settings
$TabMenuArr = array();
$TabMenuArr[] = array("score_list.php", $ec_iPortfolio['master_score_list'], 0);
$TabMenuArr[] = array("overall_performance_stat.php", $ec_iPortfolio['overall_perform_stat'], 0);
$TabMenuArr[] = array("class_performance_stat.php", $ec_iPortfolio['class_perform_stat'], 0);
$TabMenuArr[] = array("student_performance_stat.php", $ec_iPortfolio['student_perform_stat'], 1);
?>
<script language="javascript">
function filter_change(ftype){

	if(ftype=="subject")
	{
		document.form1.sub_new.value=1;
		document.form1.yr_new.value=1;
	} else if(ftype=="year")
	{
		document.form1.yr_new.value=1;
	}

	document.form1.view.value = (ftype=="student") ? 1 : 0;
	document.form1.submit();
}
</script>

<form name="form1" method="post" action="student_performance_stat.php">
<?
echo $lpf->GET_TAB_MENU($TabMenuArr);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">	
				<tr>
					<td width='30'>&nbsp;</td>
					<td>
                                    <table class="dotline" border="0" cellpadding="0" cellspacing="2" width="100%">
                                      <tbody>
                                        <tr>
                                          <td>
                                          <table border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                              <tr>
                                                <td><span class="thumb_list"><?=$SubjectSelection?>&nbsp;<?=$year_selection?><?=$class_selection?></span>
                                                	 <span class="thumb_list"> </span>
                                                </td>
                                                 <td align="left" nowrap="nowrap" valign="middle" width="15">&nbsp; | </td>
                                                <td align="left" nowrap="nowrap" valign="middle" width="15"><span class="tabletext"></span><a href="student_performance_stat.php?StudentID=<?=$StudentIDHead?>&<?=$param2?>" class="tablebottomlink">&lt;&lt;</a></td>
                                                <td align="left" nowrap="nowrap" valign="middle" width="15"><span class="tabletext"></span><a href="student_performance_stat.php?StudentID=<?=$StudentIDPre?>&<?=$param2?>" class="tablebottomlink">&nbsp;&lt;</a></td>
                                                <td align="left" nowrap="nowrap" valign="middle"><span class="tabletext"><?=$student_selection?>&nbsp;</span></td>
                                                <td align="left" nowrap="nowrap" valign="middle" width="15"><span class="tabletext"></span><a href="student_performance_stat.php?StudentID=<?=$StudentIDNext?>&<?=$param2?>" class="tablebottomlink">&gt;</a></td>
                                                <td align="left" nowrap="nowrap" valign="middle" width="15"><span class="tabletext"></span><a href="student_performance_stat.php?StudentID=<?=$StudentIDTail?>&<?=$param2?>" class="tablebottomlink">&gt;&gt;&nbsp;</a></td>
                                                <td align="left" nowrap="nowrap" valign="middle" width="15"> | </td>
                                                <td class="tabletext" align="left" nowrap="nowrap" valign="middle"><? if($TotalStudentArr!=""){ echo $ec_iPortfolio['total']."&nbsp;".$TotalStudentArr."&nbsp;".$iDiscipline['students'];}else{ echo $ec_iPortfolio['no_student'];}?></td>
                                              </tr>
                                            </tbody>
                                          </table>
                                       </td>
                                     </tr>
                                   </tbody>
                                 </table>   
				<!-- end -->
					</td>
					</tr>
						<tr>
							<td width='30'>&nbsp;</td>
                             <td>
                                 <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                          <tbody>
                                                    <tr>
                                                      <td  class="thumb_list" align="right" height="30" valign="middle"><?=$link_score?> | <?=$link_rank?> | <?=$link_stand_score?></td>
                                                    </tr>
                                        </tbody>
                                   </table>
                              <td>
                         </tr>     

					</table>
					</td>
				</tr>	
</table>
<?//$class_table?>

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="top">
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
			<td align="left">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">

                <tr>
					<td align="left" valign="middle">
						<table width="400" height="340" border="0" cellpadding="0" cellspacing="0" class="table_b">
						<tr>
							<td align="center">
								<object classid = "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="600" height="400">
								<param name="allowScriptAccess" value="sameDomain" />
								<param name="loop" value="false" />
								<param name="menu" value="false" />
								<param name="salign" value="lt" />
								<param name="scale" value="showall" />
								<param name="movie" value = "<?=$url_swf?>">
								<param name="quality" value = "high">
								<param name="flashvars" value="<?=$param_send?>" />
								<embed src = "<?=$url_swf?>" quality = high type = "application/x-shockwave-flash" width="600" height="400">
								</embed>
								</object>
							</td>
                        </tr>
						</table>
                    </td>
                    <td width="100">&nbsp;</td>
                    <td align="left" valign="top" width="700">

                    <!-- start -->
						<table  width="100%"  border="0" cellspacing="0" cellpadding="0">
						<tr align="center" valign="middle">
							<td height="10"></td>
						</tr>
						<tr align="center">
                            <td valign="top">
								<table  width="100%"  border="0" cellpadding="0" cellspacing="0">
                                <tr>
									<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
									<td colspan="3">
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<tr>
                                                   		<td nowrap="nowrap" valign="top"><label><span class="thumb_list"><?=$ec_iPortfolio['show_by']?></span></label></td>
                                                  		 <td nowrap="nowrap"><label></label> <label></label></td>
                                          			 </tr>

													<tr class="tablebottom"><?=$filter_selection?></tr>

													<tr>
														<td height="20" colspan="4" align="center">&nbsp;</td>
													</tr>
												
												</td>
											</tr>
										</table>
										</td>
										</tr> <!-- Display Table Content -->
                                        <tr class="tabletop">
												<td height="20" class="tabletopnolink" width="10%">&nbsp;</td>
												<td  height="20" class="tabletopnolink" width="50%"><?=$yearTitle?></td>
												<td height="20" class="tabletopnolink" width="40%"><?=$valueTitle?></td>
										</tr>
										<?=$data_table?>
                                        </table>
									</td>
                                 </tr>
                                 </table>
							</td>
						</tr>
						</table>
						<!--end -->
					</td>
				</tr>
                </table>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="displayBy" value="<?=$displayBy?>"/>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
