<?php
//Using: Pun
/**
 * Change Log:
 * 2016-02-24 Pun
 * 	-	New File
 */


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$fcm = new form_class_manage();
// $li_pf = new libportfolio();
// $class_arr = $li_pf->GET_CLASS_LIST_DATA($y_id, $ay_id);
$classArr = $fcm->Get_Class_List($ay_id,$y_id);

?>

<table width="600" border="0" cellspacing="0" cellpadding="3" style="background: #EEEEEE">
	<tbody>
		<tr>
			<td colspan="3">
				<input type="checkbox" id="YCCheckMaster" class="checkMaster">
				<label for="YCCheckMaster"><?=$button_check_all ?></label>
			</td>
		</tr>
		
		<?php 
		for ($i = 0, $iMax = count($classArr); $i < $iMax; $i++) {
			$className = Get_Lang_Selection($classArr[$i]['ClassTitleB5'], $classArr[$i]['ClassTitleEN']);
			$yearClassID = $classArr[$i]['YearClassID'];
			if($i % 3 == 0){ 
		?>
			<tr>
		<?php 
			}
		?>
			
			<td width="200">
				<input type="checkbox" id="class_<?=$i ?>" name="YearClassID[]" value="<?=$yearClassID ?>" class="yearClassId">
				<label for="class_<?=$i ?>"><?=$className ?></label>
			</td>
			
		<?php 
			if($i % 3 == 2){ 
		?>
			</tr>
		<?php
			}
		}
		?>
		
	</tbody>
</table>







<?php
intranet_closedb();
?>