<?php

# being modified by Pun

/*
 *  Change Log
 * 		support assessments like T1A1
 *
 *  */
 
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$lpf = new libportfolio();


if (strstr($YearTermID, "_"))
{
	$tmpArr = explode("_", $YearTermID);
	$YearTermID = (int) $tmpArr[0];
	$TermAssessment = trim($tmpArr[1]);
}

########## Access Right START ##########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$canAccess = true;
$currentAcademicYearID = Get_Current_Academic_Year_ID();
if(!$accessRight['admin']){
	if($academicYearID != $currentAcademicYearID){
		$canAccess = false;
	}
	$ClassNameDB = ($intranet_session_language=="EN") ? "ClassTitleEN" : "ClassTitleB5"; 
	$classNameArr = array();
	foreach($accessRight['classTeacher'] as $yearClass){
		$classNameArr[] = $yearClass[$ClassNameDB];
	}
	if(!in_array($ClassName, $classNameArr)){
		$canAccess = false;
	}
	
	if(!$canAccess){
		echo 'Access Denied.';
		die;
	}
}
########## Access Right END ##########


switch($ResultType)
{
  case "SCORE":
  default:
    // If Score is 0 and grade is empty, show '--'
    //$retrieveField = "IF(Score <= 0, IF(Grade = '' OR Grade IS NULL, '--', Score), TRIM('.' FROM TRIM(0 FROM ROUND(Score, 1))))";
    $retrieveField .= "CASE ";
    $retrieveField .= "WHEN assr.Score < 0 THEN IF(IFNULL(assr.Grade, '') = '', '--', assr.Grade)";
    $retrieveField .= "WHEN (ABS(assr.Score) = 0) THEN IF(IFNULL(assr.Grade, '') = '', '--', 0) ";
    $retrieveField .= "ELSE TRIM('.' FROM TRIM(0 FROM ROUND(assr.Score, 1))) ";
    $retrieveField .= "END as DisplayText";
    
    $highlightCriteria[] =  array(
                              "lower" => $lower,
                              "upper" => $upper,
                              "bold" => isset($range_bold),
                              "italic" => isset($range_italic),
                              "underline" => isset($range_underline),
                              "grade" => explode(",", preg_replace("/[ ]*/", "", $grade_highlight)),
                              "color" => (isset($range_color) ? $r_color : NULL)
                            );
    break;
  case "GRADE":
    $highlightCriteria[] =  array(
                              "grade" => explode(",", preg_replace("/[ ]*/", "", $grade_highlight)),
                              "bold" => isset($range_bold),
                              "italic" => isset($range_italic),
                              "underline" => isset($range_underline),
                              "color" => (isset($range_color) ? $r_color : NULL)
                            );
    $retrieveField = "IF(IFNULL(assr.Grade, '') = '', '--', assr.Grade) as DisplayText";
    break;
  case "RANK":
    $retrieveField = "IF(IFNULL(assr.OrderMeritForm, '') = '' Or assr.OrderMeritForm = -1, '--', assr.OrderMeritForm) as DisplayText";
    
    $highlightCriteria[] =  array(
                              "lower" => $lower,
                              "upper" => $upper,
                              "bold" => isset($range_bold),
                              "italic" => isset($range_italic),
                              "underline" => isset($range_underline),
                              "color" => (isset($range_color) ? $r_color : NULL)
                            );
    break;
}

// Get subject
$sql = "SELECT DISTINCT m.RecordID AS mainSubjID, IF(IFNULL(c.CMP_CODEID, '') = '', '', c.RecordID) AS cmpSubjID, m.EN_SNAME AS mainSubjCode, IF(IFNULL(c.CMP_CODEID, '') = '', '', c.EN_SNAME) AS cmpSubjCode, IF(IFNULL(c.CMP_CODEID, '') = '', ".Get_Lang_Selection("m.CH_DES", "m.EN_DES").", ".Get_Lang_Selection("c.CH_DES", "c.EN_DES").") AS SubjName, m.DisplayOrder AS mainSubjOrder, IF(IFNULL(c.CMP_CODEID, '') = '', 0, c.DisplayOrder) AS cmpSubjOrder ";
$sql .= "FROM {$intranet_db}.ASSESSMENT_SUBJECT AS m ";
$sql .= "LEFT JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS c ON m.codeid = c.codeid ";
$sql .= "INNER JOIN {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr ";
$sql .= "ON m.EN_SNAME = assr.SubjectCode ";
$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ";
$sql .= "ON assr.UserID = ycu.UserID ";
$sql .= "WHERE (IFNULL(m.CMP_CODEID, '') = '') AND assr.AcademicYearID = '{$academicYearID}' AND assr.ClassName = '{$ClassName}' AND m.RecordStatus=1 ";
$sql .= ($YearTermID == "") ? " AND (IFNULL(assr.YearTermID, '') = '' OR assr.YearTermID = 0) " : " AND assr.YearTermID = {$YearTermID} ";
$sql .= "ORDER BY mainSubjOrder, cmpSubjOrder";
$subj_arr = $li->returnArray($sql);

#### Filter subject component START ####
// Filter if all student in result does not contains data (2014-0808-1405-07066)
$sql = "SELECT DISTINCT
	SubjectCode, subjectComponentCode
FROM 
	{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr
WHERE
	assr.AcademicYearID = '{$academicYearID}' 
AND 
	assr.ClassName = '{$ClassName}'";
$sql .= ($YearTermID == "") ? " AND (IFNULL(assr.YearTermID, '') = '' OR assr.YearTermID = 0) " : " AND assr.YearTermID = {$YearTermID} ";
$rs = $li->returnResultSet($sql);
$dataSubject = array();
foreach($rs as $r){
	$dataSubject[$r['SubjectCode']][] = $r['subjectComponentCode'];
}

$filter_subj_arr = array();
foreach($subj_arr as $subj){
	$mainSubjCode = $subj['mainSubjCode'];
	$cmpSubjCode = $subj['cmpSubjCode'];
	if(in_array($cmpSubjCode, (array)$dataSubject[$mainSubjCode])){
		$filter_subj_arr[] = $subj;
	}
}
$subj_arr = $filter_subj_arr;
#### Filter subject component END ####

for($i=0, $i_max=count($subj_arr); $i<$i_max; $i++)
{
  list($_mainSubjID, $_cmpSubjID, $_mainSubjCode, $_cmpSubjCode, $_subjName) = $subj_arr[$i];

  $tempMarkField .= "Score_{$_mainSubjID}_{$_cmpSubjID} varchar(16), ";
  $retrieveTempMarkField .= ", IFNULL(temp_am.Score_{$_mainSubjID}_{$_cmpSubjID}, '--')";
  $colList .= "<td class=\"tabletopnolink\">{$_subjName}</td>";
  
  $insertTempMarkSQL = "INSERT INTO tempAssessmentMark (UserID, Score_{$_mainSubjID}_{$_cmpSubjID}) ";
  $insertTempMarkSQL .= "SELECT assr.UserID, {$retrieveField} ";
  $insertTempMarkSQL .= "FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr ";
  $insertTempMarkSQL .= "WHERE assr.AcademicYearID = '{$academicYearID}' ";
  $insertTempMarkSQL .= "AND assr.ClassName = '{$ClassName}' ";
  $insertTempMarkSQL .= ($YearTermID == "") ? " AND (IFNULL(assr.YearTermID, '') = '' OR assr.YearTermID = 0) " : " AND assr.YearTermID = {$YearTermID} "; 
  $insertTempMarkSQL .= "AND assr.SubjectCode = '{$_mainSubjCode}'";
  $insertTempMarkSQL .= ($_cmpSubjCode == "") ? " AND (IFNULL(assr.SubjectComponentCode, '') = '')" : " AND (assr.SubjectComponentCode = '{$_cmpSubjCode}')";
  $insertTempMarkSQL .= ($TermAssessment!="") ? " AND assr.TermAssessment = '".addslashes($TermAssessment)."' " : " AND assr.TermAssessment is null ";
  
  //$insertTempMarkSQL .= " Having DisplayText != '--'";
  $insertTempMarkSQL .= " ON DUPLICATE KEY UPDATE Score_{$_mainSubjID}_{$_cmpSubjID} = VALUES(Score_{$_mainSubjID}_{$_cmpSubjID})";
  $insertTempMarkSQLArr[] = $insertTempMarkSQL;
}

// Temp table to store score / grade / rank
$sql = "CREATE TEMPORARY TABLE tempAssessmentMark (UserID int(11), ";
$sql .= $tempMarkField;
$sql .= "PRIMARY KEY (UserID))";
$li->db_db_query($sql);

// Insert records into temp table
for($i=0, $i_max=count($insertTempMarkSQLArr); $i<$i_max; $i++)
{
  $sql = $insertTempMarkSQLArr[$i];
  $li->db_db_query($sql);
}

// Check if there are any academic records
$recCnt = current($li->returnVector("SELECT count(*) FROM tempAssessmentMark"));

if($recCnt > 0)
{
  # Main query
  if ($order=="") $order=1;
  if ($field=="") $field=0;
  $LibTable = new libpf_dbtable($field, $order, $pageNo);
  
  $sql =  "
            SELECT DISTINCT
              ".getNameFieldWithClassNumberByLang ("iu.")."
              {$retrieveTempMarkField}
            FROM
              {$intranet_db}.INTRANET_USER iu
            LEFT JOIN {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr
              ON iu.UserID = assr.UserID
            LEFT JOIN tempAssessmentMark AS temp_am
              ON iu.UserID = temp_am.UserID
            WHERE
              assr.ClassName = '{$ClassName}'
			  AND assr.AcademicYearID = '{$academicYearID}'
          ";
  // TABLE INFO
  //$LibTable->field_array = array("CAST(assr.ClassNumber AS SIGNED)");
  $LibTable->field_array = array("iu.ClassName, iu.ClassNumber");
  $LibTable->sql = $sql;
  //$LibTable->title = $ec_iPortfolio['ole'];
  $LibTable->no_msg = $no_record_msg;
  $LibTable->page_size = 999;
  $LibTable->no_col = count($subj_arr) + 1;
  $LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
  $LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
  $LibTable->row_height = 20;
  $LibTable->sort_link_style = "class='tbheading'";
  $LibTable->row_valign = "top";
  $LibTable->noNumber = true;
  
  // TABLE COLUMN
  $LibTable->column_list .= "<tr class='tabletop'>\n";
  $LibTable->column_list .= "<td height='25' align='center' class=\"tabletopnolink\" >{$iPort['usertype_s']}</span></td>\n";
  $LibTable->column_list .= $colList;
  $LibTable->column_list .= "</tr>\n";
  
  //debug_pr($LibTable->built_sql());
  
  echo $LibTable->displayAssessmentScoreList($ResultType, $highlightCriteria);
}
else
{
  $x = "<table border='1' bordercolor='#999999' cellpadding='5' cellspacing='0' width='100%'>";
	$x .= "<tr><td align='center'><i>".$no_record_msg."</i></td></tr>";
	$x .= "</table>";
  
  echo $x;
}

intranet_closedb();
?>