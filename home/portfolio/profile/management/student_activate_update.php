<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Profile:Student"));
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");

intranet_opendb();

$li_pf = new libpf_sturec();
$li_pf->LOAD_KEYS_FROM_SETTING();
$result_html = $li_pf->GEN_ACTIVATE_STUDENT_RESULT($user_id);

# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];
### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");
$linterface->LAYOUT_START();
?>

<!-- ===================================== Body Contents ============================= -->

<FORM action="../../school_records_class.php" method="POST" name="form1">
	<br />
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td align="center"><?= $result_html ?></td>
    </tr>
    <tr>
      <td><hr width="90%" align="center" /></td>
    </tr>
    <tr>
      <td align="center">
        <input class="formbutton" type="button" value="<?=$button_back?>" onClick="document.form1.submit()">
      </td>
    </tr>
  </table>
	
	<input type="hidden" name="user_id" value="<?=$user_id?>" />
	<input type="hidden" name="YearClassID" value="<?=$YearClassID?>" />
	<input type="hidden" name="DisplayType" value="list" />
</FORM>

<!-- ===================================== Body Contents (END) ============================= -->

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
