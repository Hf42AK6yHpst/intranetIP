<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$li_pf = new libpf_asr();
$class_arr = $li_pf->getPortfolioClass($ay_id, $y_id, false);

$class_selection_html = "<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" style=\"background: #EEEEEE\">";
$class_selection_html .= "<tr><td colspan=\"3\"><input type=\"checkbox\" id=\"YCCheckMaster\" /> <label for=\"YCCheckMaster\">{$button_check_all}</label></td></tr>";
for($i=0; $i<ceil(count($class_arr)/3)*3; $i++)
{
  $_class = $class_arr[$i];

  if($i % 3 == 0) {
    $class_selection_html .= "<tr>";
  }
  
  if(!empty($_class)){
    $class_selection_html .= "<td width=\"200\"><input type=\"checkbox\" id=\"class_{$i}\" name=\"ClassNames[]\" value=\"{$_class}\" /> <label for=\"class_{$i}\">{$_class}</label></td>";
  }
  else {
    $class_selection_html .= "<td width=\"200\">&nbsp;</td>";
  }
  
  if($i % 3 == 2) {
    $class_selection_html .= "</tr>";
  }
}
$class_selection_html .= "</table>";

echo $class_selection_html;

intranet_closedb();
?>