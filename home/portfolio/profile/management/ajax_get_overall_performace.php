<?php
//Using: Pun
/**
 *  Change Log
 * 2016-01-26 Pun
 * 	-	Added support subject panel
 *  ????-??-?? Pun
 * 		support assessments like T1A1
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");

include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");
intranet_auth();
intranet_opendb();

$lpf = new libportfolio();


if (strstr($YearTermID, "_"))
{
	$tmpArr = explode("_", $YearTermID);
	$YearTermID = (int) $tmpArr[0];
	$TermAssessment = trim($tmpArr[1]);
}


########## Access Right START ##########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$canAccess = true;
$currentAcademicYearID = Get_Current_Academic_Year_ID();
if(!$accessRight['admin']){
	if( count($accessRight['subjectPanel']) ){
		#### Subject Panel access right START ####
		
		$canAccess = true; // Subject Panel can always access this page
		$filterSubjectArr = array_keys($accessRight['subjectPanel']);
		
		#### Subject Panel access right END ####
	}else{
		#### Class teacher access right START ####
		
		if($academicYearID != $currentAcademicYearID){
			$canAccess = false;
		}
		$ClassNameDB = ($intranet_session_language=="EN") ? "ClassTitleEN" : "ClassTitleB5"; 
		$classNameArr = array();
		foreach($accessRight['classTeacher'] as $yearClass){
			$classNameArr[] = $yearClass[$ClassNameDB];
		}
		foreach((array)$ClassNames as $name){
			if(!in_array($name, $classNameArr)){
				$canAccess = false;
				break;
			}
		}
		
		#### Class teacher access right END ####
	}
	
	if(!$canAccess){
		echo 'Access Denied.';
		die;
	}
}
########## Access Right END ##########

$li = new libdb();
$lpf = new libpf_sturec();

$lpf->createTempAssessmentSubjectTable($li);

########## Group Subject START ##########
$sql = "SELECT 
	EN_SNAME,
	CODEID
FROM
	{$intranet_db}.ASSESSMENT_SUBJECT
WHERE
	CMP_CODEID = ''
OR 
	CMP_CODEID IS NULL
";
$rs = $li->returnResultSet($sql);
$allCodeArr = BuildMultiKeyAssoc($rs, array('CODEID') , array('EN_SNAME'), $SingleValue=1);

$childToParent = array();
foreach ($sys_custom['iPortfolio_Group_Subject'] as $parentCodeID => $childCodeIds){
	foreach($childCodeIds as $childCodeId){
		$childToParent[ $allCodeArr[$parentCodeID] ] = $allCodeArr[ $childCodeId ];
	}
}
########## Group Subject END ##########



// Assessment data retrieval
for($i=0, $i_max=count($Subjects); $i<$i_max; $i++)
{
  list($_mainSubj, $_compSubj) = explode("###", $Subjects[$i]);
  
  $_mainSubj2 = '';
  if($childToParent[$_mainSubj]){
  	$_mainSubj2 = $childToParent[$_mainSubj];
  }
  
  $conds = "AND assr.SubjectCode IN ('{$_mainSubj}', '{$_mainSubj2}') ";
  $conds .= ($_compSubj == "") ? "AND IFNULL(assr.SubjectComponentCode, '') = '' " : "AND assr.SubjectComponentCode = '{$_compSubj}' ";
  $conds .= ($YearTermID == "") ? "AND IFNULL(assr.YearTermID, 0) = 0 " : "AND assr.YearTermID = {$YearTermID} ";

  $sql = "SELECT assr.ClassName, AVG(assr.Score) AS avgScore, s.CODEID ";
  $sql .= "FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr ";
  $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON assr.UserID = ycu.UserID ";
  $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID ";
  $sql .= "INNER JOIN {$intranet_db}.ASSESSMENT_SUBJECT s ON assr.SubjectID = s.RecordID ";
  $sql .= "WHERE assr.AcademicYearID = {$academicYearID} AND yc.AcademicYearID = {$academicYearID} AND yc.YearID = {$yearID} ";
  $sql .= $conds;
  $sql .= "AND IF(assr.Score > 0, true, IF(assr.Score < 0, false, IFNULL(assr.Grade, '') <> '')) ";
  
  if ($TermAssessment=="")
  {
  	$sql .= " AND assr.TermAssessment IS NULL ";
  }else{
  	$sql .= " AND assr.TermAssessment = '".addslashes($TermAssessment)."' ";
  }
  $sql .= "GROUP BY assr.ClassName";
  $rs = $li->returnResultSet($sql);
  $a_rec_arr[$Subjects[$i]] = $rs;
  

  #### Group Subject START ####
  if($childToParent[$_mainSubj]){
	  $sql = "SELECT CODEID ";
	  $sql .= "FROM tempAssessmentSubject ";
	  $sql .= "WHERE MainSubjCode = '{$_mainSubj}' ";
	  $sql .= "AND CompSubjCode = '{$_compSubj}'";
	  $rs = $li->returnResultSet($sql);
	  
	  $rs = $lpf->getParentReleatedSubject($rs[0]['CODEID']);
	  $_subjName = Get_Lang_Selection($rs["CH_DES"], $rs["EN_DES"]);;
	  
	  $a_rec_arr[$i]['SubjectName'] = $_subjName;
	  $display_subj_arr[] = $_subjName;
	  $subjMap_arr[$Subjects[$i]] = $_subjName;
  }else{
	  // Translate subject code to subject name
	  $sql = "SELECT SubjName ";
	  $sql .= "FROM tempAssessmentSubject ";
	  $sql .= "WHERE MainSubjCode = '{$_mainSubj}' ";
	  $sql .= "AND CompSubjCode = '{$_compSubj}'";
	  $_subjName = current($li->returnVector($sql));
	  $display_subj_arr[] = $_subjName;
	  $subjMap_arr[$Subjects[$i]] = $_subjName;
  }
  #### Group Subject END ####
  /* * /
  // Translate subject code to subject name
  $sql = "SELECT SubjName ";
  $sql .= "FROM tempAssessmentSubject ";
  $sql .= "WHERE MainSubjCode = '{$_mainSubj}' ";
  $sql .= "AND CompSubjCode = '{$_compSubj}'";
  $_subjName = current($li->returnVector($sql));
  $display_subj_arr[] = $_subjName;
  $subjMap_arr[$Subjects[$i]] = $_subjName;
  /* */
  
}


// basic chart settings
switch($based_on)
{
  case 0:
    $x_legend_word = $iPort['class'];
    $x_label_arr = $ClassNames;
    break;
  case 1:
    $x_legend_word = $iPort["subject"];
    $x_label_arr = $display_subj_arr;
    break;
}

// 1st pass: build data array
$max_avg_score = 0;
if(is_array($a_rec_arr))
{
  foreach($a_rec_arr AS $subj_name => $a_rec)
  {
    for($i=0, $i_max=count($a_rec); $i<$i_max; $i++)
    {
      $_classname = $a_rec[$i]["ClassName"];
      if(!in_array($_classname, $ClassNames)) continue;
      
      $_avg = $a_rec[$i]["avgScore"];
      
      // Find Max Score
      if($_avg > $max_avg_score)
      {
        $max_avg_score = $_avg;
      }
    
      switch($based_on)
      {
        case 0:
          $bar_data_arr[$subjMap_arr[$subj_name]][] = floatval($_avg);
          break;
        case 1:
          $bar_data_arr[$_classname][] = floatval($_avg);
          break;
        default:
          break;
      }
    }
  }
}

// Calculate max value of y-axis & step interval
$max_avg_score = ceil($max_avg_score / 50) * 50;
$step = 50;

// 2nd pass: build bar data from data array
if(is_array($bar_data_arr))
{
  $i = 0;
  foreach($bar_data_arr AS $tooltip => $bar_data)
  {
    // Random colour
    mt_srand((double)microtime()*1000000);
    $rand_c = sprintf("%02X%02X%02X", mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
  
    $bar = new bar();
    $bar->set_values( $bar_data );
    $bar->set_colour( "#{$rand_c}" );
    $bar->set_tooltip( $tooltip.':#val#' );
    $bar->set_key( $tooltip, '12' );
    $bar->set_id( $i );
    $bar->set_visible( true );
    
    $bar_arr[] = $bar;
    $i++;
  }
}





# flash chart
//$title = new title( "2007-2008 No. of Merits and Demerits of Form 1 Statistics" );
//$title = new title( "銝剜�皜祈岫 2007-2008 No. of Merits and Demerits of Form 1 Statistics" );
//$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

$x_legend = new x_legend( $x_legend_word );
$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$x_labels = new x_axis_labels();
$x_labels->set_labels( $x_label_arr );
if($intranet_session_language == "en")
{
  $x_labels ->set_vertical();
}

$x = new x_axis();
$x->set_stroke( 2 );
$x->set_tick_height( 2 );
$x->set_colour( '#999999' );
$x->set_grid_colour( '#CCCCCC' );
//$x->set_labels_from_array( $x_label );
$x->set_labels( $x_labels );

$y_legend = new y_legend( $ec_iPortfolio['overall_score'] );
$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$y = new y_axis();
$y->set_stroke( 2 );
$y->set_tick_length( 2 );
$y->set_colour( '#999999' );
$y->set_grid_colour( '#CCCCCC' );
$y->set_range( 0, $max_avg_score, $step );
$y->set_offset(true);

$tooltip = new tooltip();
//$tooltip->set_proximity();
$tooltip->set_hover();
$tooltip->set_stroke( 2 );
//$tooltip->set_colour( "#ff0000" );
//$tooltip->set_background_colour( "#ff00ff" ); 

# show/hide checkbox panel
//$key = new key_legend();
//$key->set_visible(true);		

$chart = new open_flash_chart();
$chart->set_bg_colour( '#FFFFFF' );
//$chart->set_title( $title );
$chart->set_x_legend( $x_legend );
$chart->set_x_axis( $x );
$chart->set_y_legend( $y_legend );
$chart->set_y_axis( $y );
for($i=0, $i_max=count($bar_arr); $i<$i_max; $i++)
{
  $chart->add_element( $bar_arr[$i] );
}
$chart->set_tooltip( $tooltip );
//$chart->set_key_legend( $key );

echo $chart->toPrettyString();

intranet_closedb();
?>