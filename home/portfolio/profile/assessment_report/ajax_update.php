<?php
// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

iportfolio_auth("T");
intranet_opendb();

$li_pf = new libpf_sturec();
$li_pf->ACCESS_CONTROL("student_info");

if(!($sys_custom['iPf']['AcademicReportEdit'] && $li_pf->IS_IPF_SUPERADMIN()))
{
    echo 0;
    break;
}

$fcm = new form_class_manage();
$subject = new subject();

# Get Student Info
$studentID = $_POST['StudentID'];
$studentObj = $li_pf->GET_STUDENT_OBJECT($studentID);
$studentWebSAMS = $studentObj[3];               // ['WebSAMSRegNo'] without '#'

# Get Subject List
$subject_list = $subject->Get_Subject_List(0, 0);
$subject_id_map = BuildMultiKeyAssoc((array)$subject_list, 'RecordID');
$subject_code_map = BuildMultiKeyAssoc((array)$subject_list, 'CODEID');

# Get Subject Component List
$component_list = $subject->Get_Subject_Component();
$component_id_map = BuildMultiKeyAssoc((array)$component_list, 'RecordID');
$compSubjectIDArr = array_keys((array)$component_id_map);

# Get Year & Term List
$sql = "SELECT
            a.AcademicYearID, a.YearNameEN, b.YearTermID, b.YearTermNameEN
        FROM 
            ACADEMIC_YEAR AS a 
            INNER JOIN ACADEMIC_YEAR_TERM AS b ON (a.AcademicYearID = b.AcademicYearID)
        ORDER BY
            a.Sequence";
$year_sem_list = $li_pf->returnArray($sql);
$year_name_map = BuildMultiKeyAssoc((array)$year_sem_list, 'AcademicYearID', 'YearNameEN', 1, 0);
$year_sem_name_map = BuildMultiKeyAssoc((array)$year_sem_list, 'YearTermID', 'YearTermNameEN', 1, 0);

$field = $_POST['displayBy'];
$studentClassInfoAry = array();

// loop subjects
$scoreAry = $_POST['score'];
foreach((array)$scoreAry as $subjectID => $subjectAry)
{
    // Overall Result
    $isOverallData = $subjectID == 0;
    
    // Componet Subject
    $isCompSubject = $subjectID > 0 && in_array($subjectID, (array)$compSubjectIDArr);
    if($isCompSubject)
    {
        $subjectCompID = $subjectID;
        $subjectCompCode = $component_id_map[$subjectCompID]['CMP_CODEID'];
        $subjectCode = $component_id_map[$subjectCompID]['CODEID'];
        $subjectID = $subject_code_map[$subjectCode]['RecordID'];
    }
    // Main Subject
    else
    {
        $subjectCompID = '';
        $subjectCompCode = '';
        $subjectCode = $subject_id_map[$subjectID]['CODEID'];
    }
    
    // DB Table
    $data_table = $isOverallData? "ASSESSMENT_STUDENT_MAIN_RECORD" : "ASSESSMENT_STUDENT_SUBJECT_RECORD";
    $data_table = "$eclass_db.$data_table";
    
    // related Subject SQL
    $subjectCodeSql = "'".$li_pf->Get_Safe_Sql_Query($subjectCode)."'";
    $subjectCompIDSql = ($subjectCompID != '')? "'$subjectCompID'" : 'NULL';
    $subjectCompCodeSql = ($subjectCompCode != '')? "'".$li_pf->Get_Safe_Sql_Query($subjectCompCode)."'" : 'NULL';
    
    // related Subject Condition
    $subj_cond = $isOverallData? "" : ($isCompSubject? " SubjectComponentID = '$subjectCompID' AND " : " SubjectID = '$subjectID' AND ");
    $subj_comp_cond = ($isOverallData || $isCompSubject)? "" : " (SubjectComponentID IS NULL OR SubjectComponentID = '0') AND ";
    
    // loop years
    foreach((array)$subjectAry as $yearID => $subjectYearAry)
    {
        // Student Info (Specific year)
        if(!isset($studentClassInfoAry[$yearID])) {
            $studentClassInfoAry[$yearID] = $fcm->Get_Student_Class_Info_In_AcademicYear(array($studentID), $yearID);
        }
        $studentInfo = $studentClassInfoAry[$yearID];
        $studentClassID = $studentInfo[0]['YearClassID'];
        $studentClassName = $li_pf->Get_Safe_Sql_Query($studentInfo[0]['ClassTitleEN']);
        $studentClassNumber = $studentInfo[0]['ClassNumber'];
        
        // loop terms
        foreach((array)$subjectYearAry as $yearTermID => $subjectYearTermScore)
        {
            // Condition
            $term_cond = $yearTermID > 0? " YearTermID = '$yearTermID' AND " : " (YearTermID IS NULL OR YearTermID = '0') AND ";
            $cond = " UserID = '$studentID' AND $subj_cond $subj_comp_cond AcademicYearID = '$yearID' AND $term_cond TermAssessment IS NULL";
            
            // Check record exist or not
            $sql = " SELECT RecordID FROM $data_table WHERE $cond ";
            $result = $li_pf->returnVector($sql);
            
            // Update existing records
            if($result[0])
            {
                // Score / Grade handling
                if($field == 'Score') {
                    $subjectYearTermScore = $subjectYearTermScore === '' || !is_numeric($subjectYearTermScore) ? -1 : $subjectYearTermScore;
                    $update_field = " Score = '$subjectYearTermScore' ";
                } else {
                    $update_field = " Grade = '$subjectYearTermScore' ";
                }
                
                // Check need to handle OrderMeritForm or not
                $sql = " SELECT OrderMeritForm FROM $data_table WHERE $cond ";
                $result = $li_pf->returnVector($sql);
                if($result[0] == -1 || $result[0] == 0) {
                    $update_field .= ", OrderMeritForm = NULL";
                }
                $update_field .= ", ModifiedDate = NOW()";
                
                $sql = "UPDATE $data_table SET $update_field WHERE $cond";
                $li_pf->db_db_query($sql);
            }
            // Insert new records if not empty
            else if($subjectYearTermScore !== '')
            {
                // Year & Term fields
                $yearName = $li_pf->Get_Safe_Sql_Query($year_name_map[$yearID]);
                $yearTermName = $yearTermID > 0? $li_pf->Get_Safe_Sql_Query($year_sem_name_map[$yearTermID]) : "";
                $YearTermIDSql = $yearTermID > 0? "'$yearTermID'" : " NULL ";
                $isAnnual = $yearTermID > 0? '' : '1';
                
                // Score / Grade handling
                $score_field = '-1';
                $grade_field = '';
                if($field == 'Score') {
                    $subjectYearTermScore = $subjectYearTermScore === '' || !is_numeric($subjectYearTermScore)? -1 : $subjectYearTermScore;
                    $score_field = "$subjectYearTermScore";
                } else {
                    $grade_field = "$subjectYearTermScore";
                }
                
                // Overall Result
                if($isOverallData)
                {
                    $values = "('$studentID', '$studentWebSAMS', '$yearName', '$yearID', '$yearTermName', $YearTermIDSql, '$isAnnual', 
                                    '$studentClassName', '$studentClassID', '$studentClassNumber', '$score_field', '$grade_field', 
                                    NOW(), NOW(), '".$ipf_cfg["DB_ASSESSMENT_STUDENT_SUBJECT_RECORD_ComeFrom"]["edit"]."')";
                    
                    $sql = "INSERT INTO $data_table
                                (UserID, WebSAMSRegNo, Year, AcademicYearID, Semester, YearTermID, IsAnnual, 
                                ClassName, YearClassID, ClassNumber, Score, Grade, 
                                InputDate, ModifiedDate, ComeFrom)
                            VALUES
                                $values ";
                    $li_pf->db_db_query($sql);
                }
                // Subject Result
                else
                {
                    $values = "('$studentID', '$studentWebSAMS', '$yearName', '$yearID', '$yearTermName', $YearTermIDSql, '$isAnnual', 
                                    '$studentClassName', '$studentClassID', '$studentClassNumber', 
                                    $subjectCodeSql, '$subjectID', $subjectCompCodeSql, $subjectCompIDSql, '$score_field', '$grade_field',
                                    NOW(), NOW(), '".$ipf_cfg["DB_ASSESSMENT_STUDENT_SUBJECT_RECORD_ComeFrom"]["edit"]."')";
                    
                    $sql = "INSERT INTO $data_table
                                (UserID, WebSAMSRegNo, Year, AcademicYearID, Semester, YearTermID, IsAnnual, 
                                ClassName, YearClassID, ClassNumber,
                                SubjectCode, SubjectID, SubjectComponentCode, SubjectComponentID, Score, Grade,
                                InputDate, ModifiedDate, ComeFrom)
                            VALUES
                                $values ";
                    $li_pf->db_db_query($sql);
                }
            }
        }
    }
}
intranet_closedb();

echo 1;
?>