<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_sturec();
$li_pf->ACCESS_CONTROL("student_info");
$li_pf->SET_ALUMNI_COOKIE_BACKUP($ck_is_alumni);

$miscParameter = explode("&", base64_decode($miscParameter));
for($i=0; $i<count($miscParameter); $i++)
{
	$temp = explode("=", $miscParameter[$i]);
	${$temp[0]} = $temp[1]; 
}

//$template_table_top_right = $li_pf->GEN_STUDENT_LIST_INFO($ClassName, $StudentID);

$linterface = new interface_html();
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];
### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

# Retrieve student info
$student_obj = $li_pf->GET_STUDENT_OBJECT($StudentID);
	
# Set links for photo
if(strpos($student_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $student_obj['PhotoLink'] != "")
	$student_obj['PhotoLink'] = str_replace("<!--ImageStyle-->", "", $student_obj['PhotoLink']);
else if($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
else
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";

# Alumni Year Selection
$AlumniYearSelection = $li_pf->GEN_ALUMNI_YEAR_SELECTION($Year);

# Generate class selection drop-down list
$ClassSelection = $li_pf->GEN_ALUMNI_CLASS_SELECTION($Year, $ClassName);

# Generate student selection drop-down list and get a student list
list($StudentSelection, $student_list) = $li_pf->GEN_STUDENT_LIST_INFO($ClassName, $StudentID, true, 1, "name='StudentID' onChange='document.form1.submit()'", $Year);


####################################
$filter = ($filter=="") ? "0" : $filter;
$showValue = ($displayBy=="Rank") ? "OrderMeritForm" : $displayBy;
$yearTitle = ($filter==0) ? $ec_iPortfolio['term'] :  $ec_iPortfolio['year'];

$temp = explode("###", $SubjectCode);
if(sizeof($temp)>1)
{	
	if($SubjectComponentCode=="")
	{
		$SubjectCode = trim($temp[0]);
		$SubjectComponentCode = trim($temp[1]);
	}
}

# retrieve all subjects with components 
$allSubjectArr = $li_pf->returnAllSubjectWithComponents($StudentID);

# get subject selection
list($SubjectSelection, $SubjectName) = $li_pf->getSubjectSelection($allSubjectArr, "name='SubjectCode' onChange='jCHANGE_SUBJECT()'", $SubjectCode, $SubjectComponentCode, 0, 1);

# Create selection (by semester/ by year)

$filter_selection = "<td nowrap='nowrap'><label><input name='filter' id='filter' value='1' type='radio' ".($filter==1?"checked":"")." onclick='jCHANGE_SUBJECT()'> </label>".$ec_iPortfolio['by_year']."</td>";
$filter_selection .= "<td nowrap='nowrap' valign='top'><label><input name='filter' id='filter' value='0' type='radio' ".($filter==0?"checked":"")." onclick='jCHANGE_SUBJECT()'> </label>".$ec_iPortfolio['by_semester']."</td>";




# check wether the subject has score
$have_score = $li_pf->checkSubjectScoreExist($SubjectCode, $SubjectComponentCode);
$data = ($displayBy=="Score" && $have_score==0) ? array() : $li_pf->returnAsssessmentSubjectRecordBySubjectCode($StudentID, $SubjectCode, $filter, $SubjectComponentCode);

# define table title
if($displayBy=="Score")
{
	$valueTitle = $ec_iPortfolio["score"];
}
else if($displayBy=="Rank")
{
	$valueTitle = $ec_iPortfolio["rank"];
}
else if($displayBy=="StandardScore")
{
	$valueTitle = $ec_iPortfolio["stand_score"];
}

# If this subject is not selected
for($i=0; $i<count($data); $i++)
{
	if($data[$i]["Score"]==0 && $data[$i]["Grade"]=="" && $data[$i]["OrderMeritForm"]==0)
		$NotSelected = true;
	else{ 
		$NotSelected = false;
		break;
	}
}
if($NotSelected)
{
	$data = array();
}
list($style_url, $axis_url, $chart_data) = $li_pf->getAssessmentChartData($data, $SubjectCode, $SubjectComponentCode, $StudentID, $filter, $displayBy, $SubjectName);

//$url_swf = "$admin_url_path/src/includes/swf/zxchart_400x300.swf";
//$param_send = "stylefile=$admin_url_path/src/includes/swf/portfolio/style.php?$style_url&datafile=$admin_url_path/src/includes/swf/portfolio/axis.php?$axis_url". $chart_data;
$url_swf = "http://$eclass_httppath/src/includes/swf/zxchart_400x300.swf";
$param_send = "stylefile=http://$eclass_httppath/src/includes/swf/portfolio/style.php?$style_url&datafile=http://$eclass_httppath/src/includes/swf/portfolio/axis.php?$axis_url". $chart_data;
// use $eclass_filepath

// prepare legend
$data_table = "";
if(sizeof($data)>0)
{
	for($j=0; $j<sizeof($data); $j++)
	{
		$value = ($displayBy=="Rank" && $data[$j][$showValue]<=0) ? "--" : $data[$j][$showValue];
		$dtable_class = ($j%2!=0) ? "class='tablerow2'" : "class='tablerow1'";
		$data_table .= "<tr $dtable_class>
							<td class='tabletext'>".($j+1)."</td>
							<td class='tabletext' nowrap='nowrap'>".$data[$j]['YearSem']."</td>
							<td class='tabletext' align='center'>".$value."</td>
						</tr>";																	
	}
}
else
{
	$data_table .= "<tr class='tablerow1' height='40'><td colspan=3 align='center'>".$no_record_msg."</td></tr>";
}

#################
# define the buttons according to the variable $displayBy
/*
$bcolor_score = ($displayBy=="Score") ? "#CFE6FE" : "#FFD49C";
$bcolor_rank = ($displayBy=="Rank") ? "#CFE6FE" : "#FFD49C";
$bcolor_stand_score = ($displayBy=="StandardScore") ? "#CFE6FE" : "#FFD49C";

$gif_score  = ($displayBy=="Score") ? "l" : "o";
$gif_rank  = ($displayBy=="Rank") ? "l" : "o";
$gif_stand_score  = ($displayBy=="StandardScore") ? "l" : "o";
*/
$SubjectParam = "&SubjectCode=".urlencode($SubjectCode)."&SubjectComponentCode=".urlencode($SubjectComponentCode);

$link_score = ($displayBy=="Score") ? "<span class='link_a'>".$ec_iPortfolio['display_by_score']."</span>" : "<a href='#' onClick=\"jCHANGE_LINK('".base64_encode("displayBy=Score{$SubjectParam}&filter={$filter}")."')\" class='link_a'>".$ec_iPortfolio['display_by_score']."</a>";

$link_rank = ($displayBy=="Rank") ? "<span class='link_a'>".$ec_iPortfolio['display_by_rank']."</span>" : "<a href='#' onClick=\"jCHANGE_LINK('".base64_encode("displayBy=Rank{$SubjectParam}&filter={$filter}")."')\" class='link_a'>".$ec_iPortfolio['display_by_rank']."</a>";

$link_stand_score = ($displayBy=="StandardScore") ? "<span class='link_a'>".$ec_iPortfolio['display_by_stand_score']."</span>" : "<a href='#' onClick=\"jCHANGE_LINK('".base64_encode("displayBy=StandardScore{$SubjectParam}&filter={$filter}")."')\" class='link_a'>".$ec_iPortfolio['display_by_stand_score']."</a>";

####################################

$linterface->LAYOUT_START();
?>



<link href="<?=$PATH_WRT_ROOT?>templates/<?= $LAYOUT_SKIN ?>/css/iportfolio/content.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/<?= $LAYOUT_SKIN ?>/css/iportfolio/iportfolio.css" rel="stylesheet" type="text/css">

<script language="JavaScript" type="text/JavaScript">

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	if(jParField == 'classname' && document.form1.ClassName.value == '')
		return;
	
	document.form1.FieldChanged.value = jParField;
	document.form1.action = "school_record_teacher_alumni.php";
	document.form1.submit();
}

// Quick student switch 
function jCHANGE_STUDENT(jParShift, jParToExtreme)
{
	var StudentSelect = document.getElementsByName('StudentID');
	var OriginalIndex = StudentSelect[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(!jParToExtreme)
	{
		if(TargetIndex >= StudentSelect[0].length)
		{
			StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			return;
		}
		else if(TargetIndex < 0)
		{
			StudentSelect[0].selectedIndex = 0;
			return;
		}
		else
			StudentSelect[0].selectedIndex = TargetIndex;
	}
	else
	{
		if(jParShift < 0)
		{
			if(OriginalIndex > 0)
				StudentSelect[0].selectedIndex = 0;
			else
				return;
		}
		else
		{
			if(OriginalIndex < StudentSelect[0].length-1)
				StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			else
				return;
		}
	}
	
	document.form1.action = "school_record_teacher_alumni.php";
	document.form1.submit();
}

// Search student name
function jSUBMIT_SEARCH(){
	document.form1.FieldChanged.value = "search";
	document.form1.action = "../school_records_alumni_year.php";
	document.form1.submit();
}

function jCHANGE_RECORD_TYPE(jRecType)
{
	document.form1.RecordType.value = jRecType;
	if(typeof(document.form1.ChooseYear) != "undefined")
		document.form1.ChooseYear.value = "";
	document.form1.action = "school_record_teacher_alumni.php";
	document.form1.submit();
}

function jCHANGE_SUBJECT(){
	document.form1.action = "school_record_teacher_alumni_stat.php";
	document.form1.submit();
}

function jCHANGE_LINK(jMiscParam){
	document.form1.miscParameter.value = jMiscParam;

	document.form1.action = "school_record_teacher_alumni_stat.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_LP()
{
	document.form1.action = "learning_portfolio_teacher_alumni.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_SI()
{
	document.form1.action = "student_info_teacher_alumni.php";
	document.form1.submit();
}
</script>

<FORM name="form1" method="POST" action="school_record_teacher_alumni.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan=2>
				<table border="0" cellspacing="0" cellpadding="3" width="100%">
					<tr>
						<td>
							<?=$AlumniYearSelection?> <?=$ClassSelection?>
						</td>
						<td align="right">
							<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$ec_iPortfolio['enter_student_name']:intranet_htmlspecialchars(stripslashes($search_name)))?>" onFocus="if(this.value=='<?=$ec_iPortfolio['enter_student_name']?>'){this.value=''}" onBlur="if(this.value==''){this.value='<?=$ec_iPortfolio['enter_student_name']?>'}" />
							<input type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onClick="jSUBMIT_SEARCH()" />
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td class="navigation">
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../school_records_alumni.php"><?=$ec_iPortfolio['year_list']?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../school_records_alumni_year.php?Year=<?=$Year?>"><?=$ClassName==""?$i_general_WholeSchool:$ClassName?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><?=($intranet_session_language=="b5"?$student_obj['ChineseName']:$student_obj['EnglishName'])?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="17" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_01_tea.gif" width="17" height="37"></td>
						<td height="37" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_03_tea.gif">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="200">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center" valign="middle" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_02_tea.gif"  class="page_title"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0" align="absmiddle"> <?=$ec_iPortfolio['school_record']?></td>
											</tr>
										</table>
									</td>
									<td align="left" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#E8E8E8">
											<tr>
												<td align="left">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr align="center" valign="middle">
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(-1, true)" class="tablebottomlink">&lt;&lt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(-1, false)" class="tablebottomlink">&lt;</a></td>
															<td nowrap>
																<span class="tabletext">
																	<?=$StudentSelection?>
																</span>
															</td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(1, false)" class="tablebottomlink">&gt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(1, true)" class="tablebottomlink">&gt;&gt;</a></td>
															<td width="15" nowrap> | </td>
															<td nowrap class="tabletext"><?=str_replace("<!--NoRecord-->", count($student_list), $ec_iPortfolio['total_record'])?></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_04_tea.gif" width="13" height="37"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif" width="17" height="20"></td>
						<td>
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<tr>
									<td valign="top">
										<table border="0" cellspacing="0" cellpadding="0" >
											<tr>
												<td>
													<table border="0" cellspacing="0" cellpadding="0" >
														<tr>
															<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_01.gif" style="padding-left:10px; padding-top:10px;"><?=$student_obj['PhotoLink']?></td>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="10" height="10"></td>
														</tr>
														<tr>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
															<td height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_04.gif" width="10" height="10"></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" class="tabletext"><?=$intranet_session_language=="b5"?$student_obj['ChineseName']:$student_obj['EnglishName']?></td>
											</tr>
											<tr>
												<td align="center">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr>
															<td valign="top"><a href="#" onClick="jTO_SI()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_stu_info.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0"></a></td>
															<td valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record_on.gif" alt="<?=$ec_iPortfolio['school_record']?>" width="20" height="20" border="0"></td>
															<td valign="top">
																<a href="#" onClick="jTO_LP()">
																	<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_learning_portfolio.gif" alt="<?=$iPort['menu']['learning_portfolio']?>" width="20" height="20" border="0"><br>
																	<?=$li_pf->GET_LP_NOT_VIEW_COUNT_BY_USER_ID($StudentID) > 0 ? "<span class='new_alert'>New</span>" : ""?>
																</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
									<td>
										<table width="100%"  border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td>
													<table border="0" cellspacing="5" cellpadding="0"  width="100%">
														<tr>
															<td class="tab_underline" nowrap>
																<div class="shadetabs">
																	<ul>
																		<?=$li_pf->HAS_RIGHT("merit") ? ($RecordType=="merit" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_merit']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('merit')\">".$ec_iPortfolio['title_merit']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("assessment_report") ? ($RecordType=="assessment" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_academic_report']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('assessment')\">".$ec_iPortfolio['title_academic_report']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("activity") ? ($RecordType=="activity" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_activity']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('activity')\">".$ec_iPortfolio['title_activity']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("award") ? ($RecordType=="award" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_award']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('award')\">".$ec_iPortfolio['title_award']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("teacher_comment") ? ($RecordType=="comment" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_teacher_comments']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('comment')\">".$ec_iPortfolio['title_teacher_comments']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("attendance") ? ($RecordType=="attendance" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_attendance']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('attendance')\">".$ec_iPortfolio['title_attendance']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("service") ? ($RecordType=="service" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['service']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('service')\">".$ec_iPortfolio['service']."</a></li>") : ""?>
																	</ul>
																</div>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td>
																<img src="<?=$PATH_WRT_ROOT?>/images/<?= $LAYOUT_SKIN ?>/iPortfolio/nav_arrow.gif" align="middle" height="15" width="15"><span class="navigation"><a href="#" onClick="jCHANGE_RECORD_TYPE('assessment')"><?=$ec_iPortfolio['overall_summary']?></a>
																<img src="<?=$PATH_WRT_ROOT?>/images/<?= $LAYOUT_SKIN ?>/iPortfolio/nav_arrow.gif" align="middle" height="15" width="15"></span><span class="tabletext"><?=$ec_iPortfolio['subject_chart']?></span>
															</td>
														</tr>
														<tr>
															<td><?=$SubjectSelection?></td>
															<td width='700' align="right" valign="middle" class="thumb_list">
															<?php
																if($displayBy=="Rank")
																{
																	echo $link_score." | <span>".$link_rank."</span> | ".$link_stand_score;
																}
																else if($displayBy=="StandardScore")
																{
																	echo $link_score." | ".$link_rank." | <span>".$link_stand_score."</span>";
																}
																else{
																	echo "<span>".$link_score."</span> | ".$link_rank." | ".$link_stand_score;
																}
															?>
															</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td align="center" valign="middle">
																<table width="450" height="340" border="0" cellpadding="0" cellspacing="0" class="table_b">
																	<tr>
																		<td align="center">
																			<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" id="eClassChart" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" WIDTH="100%" HEIGHT="100%" align="middle">
																				<param name="allowScriptAccess" value="sameDomain" />
																				<param name="loop" value="false" />
																				<param name="menu" value="false" />
																				<param name="salign" value="lt" />
																				<param name="movie" value="<?=$url_swf?>" />
																				<param name="quality" value="high" />
																				<param name="flashvars" value="<?=$param_send?>" />
																				<param name="scale" value="showall" />
																				<embed src="<?=$url_swf?>" quality="high" scale="showall" flashvars="<?=$param_send?>" id="eClassChart" WIDTH="100%" HEIGHT="100%" name="demo_index" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
																			</object>
																		</td>
																	</tr>
																</table>
															</td>
															<td align="left" valign="top">
																<table border="0" cellpadding="3" cellspacing="0" width="100%">
																	<tr align="left" valign="middle">
																		<td>&nbsp;</td>
																	</tr>
																	<tr align="left" valign="middle">
																		<td>
																			<table border="0" cellpadding="0" cellspacing="0" width="100%">
																				<tr>
																					<td nowrap="nowrap" valign="top"><label><span class="thumb_list"><?=$ec_iPortfolio['show_by']?></span></label></td>
																					<td nowrap="nowrap"><label></label><label></label></td>
																				</tr>
																				<tr class="tablebottom"><?=$filter_selection?></tr>
																			</table>
																		</td>
																	</tr>
																	<tr align="left">
																		<td valign="top">
																			<table  border="0" cellpadding="0" cellspacing="0" class="table_p" width="100%">
																				<tr>
																					<td>
																						<table bgcolor="#cccccc" border="0" cellpadding="4" cellspacing="0" width="100%">
																							<tr class="tabletop">
																								<td class="tabletopnolink" width="10">&nbsp;</td>
																								<td class="tabletopnolink" width="50%"><?=$yearTitle?></td>
																								<td class="tabletopnolink" align="center" width="50%"><?=$valueTitle?></td>
																							</tr>
																							<?=$data_table?>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif" width="13" height="37"></td>
					</tr>
					<tr>
						<td width="17" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_08.gif" width="17" height="17"></td>
						<td height="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif" width="64" height="17"></td>
						<td width="13" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_10.gif" width="13" height="17"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

<input type="hidden" name="RecordType" value=<?=$RecordType?> />
<input type="hidden" name="FieldChanged" />
<input type="hidden" name="displayBy" value="<?=$displayBy?>" />
<input type="hidden" name="miscParameter" />
<input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>" />
</FORM>


<?php
//include_once("$admin_root_path/src/portfolio/new_footer.php");
$li_pf->RELOAD_ALUMNI_COOKIE_BACKUP($ck_is_alumni);
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
