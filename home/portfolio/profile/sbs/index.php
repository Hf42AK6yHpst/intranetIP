<?php
/**
 * [Modification Log] Modifying By:
 * ************************************************
 * 	Date:	2017-09-26 [Anna] #J124287
 * 		- add tooltips for icons 
 *	Date:   2017-03-29 [Siuwan]
 *		 - add LP version checking in js function jTO_LP()  -#X115139
 *	Date:   2016-04-15 [Omas]
 *		 - fix class selection access right problem -#U94994
 *
 * ************************************************
 */

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/libportfolio_group.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-teacher.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
intranet_opendb();

### Get the library ###
$lpf = new libpf_sbs();
$lpf_sturec = new libpf_sturec();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

#############################################################################
# General Layout
#############################################################################
// access right checking for class selection box
$hide_all_for_class_selection = 0;
if(!strstr($ck_user_rights, ":manage:"))
{
	# Class teacher view class
	if(strstr($ck_user_rights_ext, "student_info:form_t"))
	{
		$lpf_acc = new libpf_account_teacher();
		$lpf_acc->SET_CLASS_VARIABLE("user_id", $UserID);
		$class_teach_arr = $lpf_acc->GET_CLASS_TEACHER_CLASS();
		$cond .= " AND yc.YearClassID IN (".implode(",", $class_teach_arr).")";
		$hide_all_for_class_selection = 1;
	}
	# Subject teacher view class
	else if(strstr($ck_user_rights_ext, "student_info:form_subject_t"))
	{
		$lpf_acc = new libpf_account_teacher();
		$lpf_acc->SET_CLASS_VARIABLE("user_id", $UserID);
		$class_teach_arr = array_merge($lpf_acc->GET_CLASS_TEACHER_CLASS(), $lpf_acc->GET_SUBJECT_TEACHER_CLASS());
		$cond .= " AND yc.YearClassID IN (".implode(",", $class_teach_arr).")";
		$hide_all_for_class_selection = 1;
	}
}

if($_SESSION["platform"] == "KIS"){
	// 	debug_r($ck_current_academic_year_id);
	//debug_r($_SESSION["KIS_IP_Teacher_Role"]);
	// 	debug_r($StudentID);
	//debug_r($studentClassInfo);
	switch($_SESSION["KIS_IP_Teacher_Role"]["Role"]){
		case"FormClassTeacher":
			$sql = "Select YearClassID From {$intranet_db}.YEAR_CLASS where YearID in (".implode(",",$_SESSION["KIS_IP_Teacher_Role"]["FormID"]).") AND AcademicYearID = '$ck_current_academic_year_id'";
			$KIS_Year_ClassID = $lpf->returnVector($sql);
			$class_teach_arr = array_merge($KIS_Year_ClassID, $_SESSION["KIS_IP_Teacher_Role"]["ClassID"]);
			if(!$YearClassID){
				$YearClassID = implode(',',$class_teach_arr);
			}
			break;
		case"FormTeacher":
			$sql = "Select YearClassID From {$intranet_db}.YEAR_CLASS where YearID in (".implode(",",$_SESSION["KIS_IP_Teacher_Role"]["FormID"]).") AND AcademicYearID = '$ck_current_academic_year_id'";
			$class_teach_arr = $lpf->returnVector($sql);
			if(!$YearClassID){
				$YearClassID = implode(',',$class_teach_arr);
			}
			break;

		case"ClassTeacher":
			$class_teach_arr = $_SESSION["KIS_IP_Teacher_Role"]["ClassID"];
			if(!$YearClassID){
				$YearClassID = implode(',',$class_teach_arr);
			}
			break;

		case"NormalTeacher":

			break;
	}

}


# Get class selection
$lpf_fc = new libpf_formclass();
$t_class_arr = $lpf_fc->GET_CLASS_LIST();
$class_name = $i_general_WholeSchool;
for($i=0; $i<count($t_class_arr); $i++)
{
  $t_yc_id = $t_class_arr[$i][0];
  $t_yc_title = Get_Lang_Selection($t_class_arr[$i]['ClassTitleB5'], $t_class_arr[$i]['ClassTitleEN']);
  if(!is_array($class_teach_arr) || in_array($t_yc_id, $class_teach_arr))
  {
    $class_arr[] = array($t_yc_id, $t_yc_title);
  }
  
  if($t_yc_id == $YearClassID)
  {
    $class_name = $t_yc_title;
  }
}
//$class_selection_html = getSelectByArray($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, 1, 0, "", 2);
$class_selection_html = getSelectByArray($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, 1, $hide_all_for_class_selection, "", 2);

# Get student selection
$lpf_fc->SET_CLASS_VARIABLE("YearClassID", $YearClassID);
$default_student_id = "";
$student_id_arr = $lpf_fc->GET_STUDENT_LIST();
$t_student_detail_arr = $lpf_fc->GET_STUDENT_DETAIL_LIST($student_id_arr);
if(is_array($t_student_detail_arr))
{
  for($i=0; $i<count($t_student_detail_arr); $i++)
  {
    $t_classname = $t_student_detail_arr[$i]['ClassName'];
    if(in_array($t_classname, $class_arr)) continue;
  
    $t_user_id = $t_student_detail_arr[$i]['UserID'];
    $t_user_name = "(".$t_classname." - ".$t_student_detail_arr[$i]['ClassNumber'].") ";
    $t_user_name .= Get_Lang_Selection($t_student_detail_arr[$i]['ChineseName'], $t_student_detail_arr[$i]['EnglishName']);
    
    # Set default user ID if class is changed
    if($StudentID == $t_user_id)
      $default_student_id = $StudentID;
  
    $student_detail_arr[] = array($t_user_id, $t_user_name);
  }
}
if($default_student_id == "") $default_student_id = $student_detail_arr[0][0];
$student_selection_html = getSelectByArray($student_detail_arr, "name='StudentID' onChange='jCHANGE_FIELD()'", $default_student_id, 0, 1, "", 2);

# Get activated students
$act_student_id_arr = $lpf_fc->GET_ACTIVATED_STUDENT_LIST();

$linterface = new interface_html();
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];
### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

# Retrieve student info
$student_obj = $lpf->GET_STUDENT_OBJECT($default_student_id);
	
# Set links for photo
if(strpos($student_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $student_obj['PhotoLink'] != "")
	$student_obj['PhotoLink'] = str_replace("<!--ImageStyle-->", "", $student_obj['PhotoLink']);
else if($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
else
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";

#############################################################################
# End of General Layout
#############################################################################

$lgs = new growth_scheme();
$luser = new libuser($default_student_id);
$lo = new libportfolio_group($lpf->course_db);

//////////////////////////////////////////////////////
////////////   GET THE PAGE CONTENT  /////////////////
//////////////////////////////////////////////////////
// page number settings
$order = ($order == 1) ? 1 : 0;

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

if ($pageNo == "")
$pageNo = 1;

// ParArr settings
$ParArr["Role"] = "STUDENT";

$ParArr["StudentID"] = $default_student_id;

//$ParArr["SchoolYear"] = $SchoolYear;
$ParArr["SchoolYear"] = GET_ACADEMIC_YEAR();

$keyword = trim($keyword);
$ParArr["keyword"] = $keyword;
$ParArr["NumOfCol"] = 5; // default phase number display in one row
$ParArr["page_size"] = $page_size;
$ParArr["pageNo"] = $pageNo;

$SchemeData = $lpf->GET_SCHOOL_BASED_SCHEME($ParArr);

$ParArr["SchemeData"] = $SchemeData;
$SchemeRecordArr = $lpf->GET_SCHEME_TABLE($ParArr);
$SchemeRecord = $SchemeRecordArr["content"];
$SchemeRecordCount = $SchemeRecordArr["count"];

# TABLE SQL
//$user_field = getNameFieldWithClassNumberByLang("c.");
$ParArr["selectType"] = "count";
$sql = $lpf->GET_SCHOOL_BASED_SCHEME_BY_STUDENT_ID_QUERY($ParArr);

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("Num");
$li->sql = $sql;
$li->no_col = 1;
$li->record_num_mode = "iportfolio"; // for school based scheme total record display
    
$li->IsColOff = "iPortfolioSchoolBasedScheme";
$li->title = $iPort['table']['records'];


$Content = "";
$Content .= $li->display($SchemeRecord, $SchemeRecordCount);
$Content .= "<br /><br /><br />";
//////////////////////////////////////////////////////
////////////   END GET THE PAGE CONTENT  /////////////
//////////////////////////////////////////////////////


$linterface->LAYOUT_START();

?>


<? // ===================================== Body Contents ============================= ?>

<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax.js"></script>

<script language="JavaScript">

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	document.form1.action = "index.php";
	document.form1.submit();
}

// Quick student switch 
function jCHANGE_STUDENT(jParShift, jParToExtreme)
{
	var StudentSelect = document.getElementsByName('StudentID');
	var OriginalIndex = StudentSelect[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(!jParToExtreme)
	{
		if(TargetIndex >= StudentSelect[0].length)
		{
			StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			return;
		}
		else if(TargetIndex < 0)
		{
			StudentSelect[0].selectedIndex = 0;
			return;
		}
		else
			StudentSelect[0].selectedIndex = TargetIndex;
	}
	else
	{
		if(jParShift < 0)
		{
			if(OriginalIndex > 0)
				StudentSelect[0].selectedIndex = 0;
			else
				return;
		}
		else
		{
			if(OriginalIndex < StudentSelect[0].length-1)
				StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			else
				return;
		}
	}
	
	document.form1.action = "index.php";
	document.form1.submit();
}

// Search student name
function jSUBMIT_SEARCH(){
	document.form1.action = "../../school_records_class.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_LP()
{
	document.form1.action = "../learning_portfolio_teacher<?=$iportfolio_lp_version == 2?'_v2':''?>.php";
	document.form1.submit();
}

// Change page to display student record
function jTO_SR()
{
	document.form1.action = "../school_record_teacher.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_SI()
{
	document.form1.action = "../student_info_teacher.php";
	document.form1.submit();
}

</script>

<FORM name="form1" method="POST" action="index.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan=2>
				<table border="0" cellspacing="0" cellpadding="3" width="100%">
					<tr>
						<td>
							<?=$class_selection_html?>
						</td>
					<?if($_SESSION["platform"]!="KIS"): ?>
						<td align="right">
							<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$ec_iPortfolio['enter_student_name']:intranet_htmlspecialchars(stripslashes($search_name)))?>" onFocus="if(this.value=='<?=$ec_iPortfolio['enter_student_name']?>'){this.value=''}" onBlur="if(this.value==''){this.value='<?=$ec_iPortfolio['enter_student_name']?>'}" />
							<input type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onClick="jSUBMIT_SEARCH()" />
						</td>
					</tr>
					
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td class="navigation">
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../../school_records.php"><?=$ec_iPortfolio['class_list']?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../../school_records_class.php?YearClassID=<?=$YearClassID?>"><?=$class_name?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><?=Get_Lang_Selection($student_obj['ChineseName'], $student_obj['EnglishName'])?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<?endif; ?>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="17" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_01_tea.gif" width="17" height="37"></td>
						<td height="37" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_03_tea.gif">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="200">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center" valign="middle" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_02_tea.gif"  class="page_title"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record.gif" alt="<?=$iPort['menu']['school_based_scheme']?>" width="20" height="20" border="0" align="absmiddle"> <?=$iPort['menu']['school_based_scheme']?></td>
											</tr>
										</table>
									</td>
									<td align="left" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#E8E8E8">
											<tr>
												<td align="left">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr align="center" valign="middle">
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(-1, true)" class="tablebottomlink">&lt;&lt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(-1, false)" class="tablebottomlink">&lt;</a></td>
															<td nowrap>
																<span class="tabletext">
																	<?=$student_selection_html?>
																</span>
															</td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(1, false)" class="tablebottomlink">&gt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(1, true)" class="tablebottomlink">&gt;&gt;</a></td>
															<td width="15" nowrap> | </td>
															<td nowrap class="tabletext"><?=str_replace("<!--NoRecord-->", count($student_detail_arr), $ec_iPortfolio['total_record'])?></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_04_tea.gif" width="13" height="37"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif" width="17" height="20"></td>
						
						<td>
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<tr>
									<td valign="top">
										<table border="0" cellspacing="0" cellpadding="0" >
											<tr>
												<td>
												
													<table border="0" cellspacing="0" cellpadding="0" >
														<tr>
															<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_01.gif" style="padding-left:10px; padding-top:10px;"><?=$student_obj['PhotoLink']?></td>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="10" height="10"></td>
														</tr>
														<tr>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
															<td height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_04.gif" width="10" height="10"></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" class="tabletext"><?=Get_Lang_Selection($student_obj['ChineseName'], $student_obj['EnglishName'])?></td>
											</tr>
											<tr>
												<td align="center">
												<?if($_SESSION["platform"]!="KIS"): ?>
													<table border="0" cellspacing="0" cellpadding="2">
														<tr>
															<td valign="top"><a href="javascript:jTO_SI()" title="<?=$ec_iPortfolio['heading']['student_info']?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_stu_info.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0"></a></td>
<?php
	if(in_array($default_student_id, $act_student_id_arr)) {
		if($lpf_sturec->HAS_SCHOOL_RECORD_VIEW_RIGHT()) {
?>
															<td valign="top"><a href="javascript:jTO_SR()" title="<?=$ec_iPortfolio['heading']['student_record']?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record.gif" alt="<?=$ec_iPortfolio['school_record']?>" width="20" height="20" border="0"></a></td>
<?php
		}
		if(strstr($ck_user_rights, ":web:")) {
?>
															<td valign="top">
																<a href="javascript:jTO_LP()" title="<?=$ec_iPortfolio['heading']['learning_portfolio']?>">
																	<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_learning_portfolio.gif" alt="<?=$iPort['menu']['learning_portfolio']?>" width="20" height="20" border="0"><br>
																	<?=$lpf->GET_LP_NOT_VIEW_COUNT_BY_USER_ID($default_student_id) > 0 ? "<span class='new_alert'>New</span>" : ""?>
																</a>
															</td>
<?php
		}
	}
?>
															<td valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_sbs_on.gif" alt="<?=$iPort['menu']['school_based_scheme']?>" width="20" height="20" border="0"></td>
														</tr>
													</table>
													<? endif; ?>
												</td>
											</tr>
										</table>
									</td>
									<td>
<?php
	if(in_array($default_student_id, $act_student_id_arr))
		echo $Content;
	else {
?>
										<table>
											<tr>
												<td><?=$ec_iPortfolio['suspend_result_inactive']?></td>
											</tr>
										</table>
<?php } ?>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif" width="13" height="37"></td>
					</tr>
					<tr>
						<td width="17" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_08.gif" width="17" height="17"></td>
						<td height="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif" width="64" height="17"></td>
						<td width="13" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_10.gif" width="13" height="17"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>" />
	<input type="hidden" name="FieldChanged" />
	<input type="hidden" name="RecordType" value=<?=$RecordType?> />
	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
	<input type="hidden" name="displayBy" value="<?=$displayBy?>" />
	<input type="hidden" name="miscParameter" />
</FORM>

<? // ===================================== Body Contents (END) ============================= ?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
