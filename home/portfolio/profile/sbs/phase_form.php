<?php
/**
 * [Modification Log] Modifying By:
 * ************************************************
 *	Date:   2018-11-20 Pun [#152978] [ip.2.5.10.1.1]
 *		 - add chiuchunkg cust
 *
 *	Date:   2017-03-29 [Siuwan]
 *		 - add LP version checking in js function jTO_LP()  -#X115139
 *
 * ************************************************
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio_group.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
intranet_auth();
intranet_opendb();

### set Library ###
$lgs = new growth_scheme();
$lpf = new libpf_sbs();
$lpf_sturec = new libpf_sturec();
$lo = new libportfolio_group($lpf->course_db);

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

#############################################################################
# General Layout
#############################################################################
# Get class selection
$lpf_fc = new libpf_formclass();
$t_class_arr = $lpf_fc->GET_CLASS_LIST();
$class_name = $i_general_WholeSchool;
for($i=0; $i<count($t_class_arr); $i++)
{
  $t_yc_id = $t_class_arr[$i][0];
  $t_yc_title = Get_Lang_Selection($t_class_arr[$i]['ClassTitleB5'], $t_class_arr[$i]['ClassTitleEN']);
  if(!is_array($class_teach_arr) || in_array($t_yc_id, $class_teach_arr))
  {
    $class_arr[] = array($t_yc_id, $t_yc_title);
  }
  
  if($t_yc_id == $YearClassID)
  {
    $class_name = $t_yc_title;
  }
}
$class_selection_html = getSelectByArray($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, 1, 0, "", 2);

# Get student selection
$lpf_fc->SET_CLASS_VARIABLE("YearClassID", $YearClassID);
$default_student_id = "";
$student_id_arr = $lpf_fc->GET_STUDENT_LIST();
$t_student_detail_arr = $lpf_fc->GET_STUDENT_DETAIL_LIST($student_id_arr);
if(is_array($t_student_detail_arr))
{
  for($i=0; $i<count($t_student_detail_arr); $i++)
  {
    $t_classname = $t_student_detail_arr[$i]['ClassName'];
    if(in_array($t_classname, $class_arr)) continue;
  
    $t_user_id = $t_student_detail_arr[$i]['UserID'];
    $t_user_name = "(".$t_classname." - ".$t_student_detail_arr[$i]['ClassNumber'].") ";
    $t_user_name .= Get_Lang_Selection($t_student_detail_arr[$i]['ChineseName'], $t_student_detail_arr[$i]['EnglishName']);
    
    # Set default user ID if class is changed
    if($StudentID == $t_user_id)
      $default_student_id = $StudentID;
  
    $student_detail_arr[] = array($t_user_id, $t_user_name);
  }
}
if($default_student_id == "") $default_student_id = $student_detail_arr[0][0];
$student_selection_html = getSelectByArray($student_detail_arr, "name='StudentID' onChange='jCHANGE_FIELD()'", $default_student_id, 0, 1, "", 2);

# Get activated students
$act_student_id_arr = $lpf_fc->GET_ACTIVATED_STUDENT_LIST();

# Convert intranet UserID to classroom user_id
$user_id = $lpf->getCourseUserID($default_student_id);

$linterface = new interface_html();
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];
### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

# Retrieve student info
$student_obj = $lpf->GET_STUDENT_OBJECT($default_student_id);
	
# Set links for photo
if(strpos($student_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $student_obj['PhotoLink'] != "")
	$student_obj['PhotoLink'] = str_replace("<!--ImageStyle-->", "", $student_obj['PhotoLink']);
else if($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
else
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";

#############################################################################
# End of General Layout
#############################################################################

////////////////// Gen Content //////////////////
$phase_obj = $lgs->getPhaseInfo($assignment_id); // assignment_id also is phase_id
$phase_obj = $phase_obj[0];
$person_response = $phase_obj[8];

$person_right = $lgs->getPhaseRight($ck_memberType, $phase_obj);

//debug_r($person_right);

$AlertMsg = "";
if($person_right == "NO")
{
	$phaseDoBy = $lpf->GET_ROLE_TYPE($person_response);
	$AlertMsg .= $iPort["alert"]["this_phase_is_filled_by"]." ".$phaseDoBy." ".$iPort["alert"]["filled_in"];
	
	// in student phase
	if($person_response == "S")
	{
		$AlertMsg .= $iPort["alert"]["no_right_to_do"];
		//$AlertMsg .= $iPort["alert"]["only_preview"];
	}
	else
	{
		$AlertMsg .= $iPort["alert"]["no_right_to_do"];
	}
}
$ParArr["AlertMsg"] = $AlertMsg;

# get handin object
$handin_obj = $lgs->getStudentPhaseHandin($user_id, $phase_obj);
$relevant_phase_html = $lgs->getRelevantPhaseViewAnswer($phase_obj["answer"], $user_id);

$phase_obj["correctiondate"] = $handin_obj["correctiondate"];
$phase_obj["status"] = $handin_obj["status"];

$person_right = $lgs->getPhaseRight($ck_memberType, $phase_obj);

if($Mode == "edit" && $person_right == "VIEW")
$phase_obj["answer_display_mode"] = 1;
else if($Mode == "view" && $person_right == "VIEW")
$phase_obj["answer_display_mode"] = 0;
else if($Mode == "view" && $person_right == "NO")
$phase_obj["answer_display_mode"] = 1;
else if($Mode == "view")
$phase_obj["answer_display_mode"] = 0;
else if($Mode == "edit")
$phase_obj["answer_display_mode"] = 1;

# preset parameter array
$ParArr["person_right"] = $person_right;
$ParArr["StudentID"] = $default_student_id;
$ParArr["Role"] = "STUDENT";
$ParArr["Mode"] = $Mode;
$ParArr["assignment_id"] = $assignment_id;
$ParArr["parent_id"] = $parent_id;
$ParArr["ParUserID"] = $lpf->getCourseUserID($default_student_id);
$phase_id = $assignment_id;

$PhaseData = $lpf->GET_PHASE_INFO($ParArr);
$SchemeData = $lpf->GET_SCHOOL_BASED_SCHEME($ParArr);

$PhaseTitle = $PhaseData[0]["title"];
$SchemeTitle = $SchemeData[0]["title"];

$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['class_list'], "../../school_records.php");
$MenuArr[] = array($class_name, "../../school_records_class.php?YearClassID=".$YearClassID);
$MenuArr[] = array(Get_Lang_Selection($student_obj['ChineseName'],$student_obj['EnglishName']), "index.php?YearClassID=".$YearClassID."&StudentID=".$default_student_id);
$MenuArr[] = array($SchemeTitle, "");
$MenuArr[] = array($PhaseTitle, "");

/////////////// END Gen Content ////////////////////

$linterface->LAYOUT_START();
?>

<form name="ansForm" method="post" action="phase_fill_update.php" onSubmit="return checkform(this)">
	<input type="hidden" name="qStr" value="<?=$phase_obj["answersheet"]?>">
<?php
if($phase_obj["answer_display_mode"]==1){
?>
	<input type="hidden" name="aStr" value="<?=$handin_obj["answer"]?>">
<?php
} else {
?>
	<input type="hidden" name="aStr" value="<?=preg_replace('(\r\n|\n)', "<br />", $handin_obj["answer"])?>">
<?php
}
?>
	<input type="hidden" name="handin_id" value="<?=$handin_obj["handin_id"]?>">
	<input type="hidden" name="phase_id" value="<?=$phase_id?>">
	<input type="hidden" name="assignment_id" value="<?=$parent_id?>">
	<input type="hidden" name="answersheet" value="<?=$phase_obj["answersheet"]?>">
	<input type="hidden" name="fieldname" value="answersheet">
	<input type="hidden" name="formname" value="ansForm">
	<input type="hidden" name="user_id" value="<?=$user_id?>">
</form>

<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/layer.js"></script>

<?php
if($phase_obj["answer_display_mode"]==1){
?>
<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/online_form_edit.js"></script>
<?php
} else {
?>
<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/online_form_view.js"></script>
<?php
}
?>

<SCRIPT LANGUAGE="Javascript">
function checkform(myObj)
{
	var isst = false;
	if (!check_text(myObj.title, "<?=$ec_warning['growth_phase_title']?>")) return false;
	if (typeof(myObj.starttime)!="undefined")
	{
		if (myObj.starttime.value!="")
		{
			if(!check_date(myObj.starttime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			{
				isst = true;
			}
		}
	}
	if (typeof(myObj.endtime)!="undefined")
	{
		if (myObj.endtime.value!="")
		{
			if(!check_date(myObj.endtime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			if (isst)
			{
				if(!compareTime(myObj.starttime, myObj.sh, myObj.sm, myObj.endtime, myObj.eh, myObj.em)) {
					myObj.starttime.focus();
					alert("<?= $w_alert['start_end_time2'] ?>");
					return false;
				}
			}
		}
	}

	<?php if($sys_custom['iPf']['chiuchunkg']['SBS']): ?>
	var isAllFill = true;
	var allInputName = [];
	$('[name="answersheet"] input[type="radio"]').each(function(){
		allInputName.push($(this).attr('name'));
	});
	$.each(allInputName, function(index, name){
		if($('[name="answersheet"] input[name="'+name+'"]:checked').length == 0){
			isAllFill = false;
		}
	});
	if(!isAllFill && !confirm('<?=$Lang['iPortfolio']['ChiuChunKG']['SBS']['someFieldNotAns'] ?>')){
		return false;
	}
	<?php endif; ?>
	/*
	checkOption(myObj.elements["relevant_phase[]"]);
	for(var i=0; i<myObj.elements["relevant_phase[]"].length; i++)
	{
		myObj.elements["relevant_phase[]"].options[i].selected = true;
	}
	*/
	
	return true;
}

function editOnlineForm()
{
	postInstantForm(document.form1, "online_form/edit.php", "post", 10, "ec_popup10");
}

function jSubmitForm()
{
	finish();
	var obj = document.ansForm;
	obj.submit();
}

function jCancelForm()
{
	self.location.href = "index.php?YearClassID=<?=$YearClassID?>&StudentID=<?=$default_student_id?>";
}

function jPrint(phase_id, assignment_id, is_menu)
{
	if(is_menu == 1)
	var url = "print_phase_menu.php?QuestionOnly=1&phase_id="+phase_id+"&assignment_id="+assignment_id+"&user_id=<?=$user_id?>";
	else
	var url = "print_phase.php?QuestionOnly=1&phase_id="+phase_id+"&assignment_id="+assignment_id+"&user_id=<?=$user_id?>";
	
	newWindow(url, 28);
}

<?php
if($phase_obj["answer_display_mode"]==1){
$phase_obj["answersheet"] = ($phase_obj["sheettype"]!=2) ? preg_replace('(\r\n|\n)', "<br>", $phase_obj["answersheet"]) : preg_replace('(\r\n|\n)', "", $phase_obj["answersheet"]);
?>
// answer form
answer_sheet = "<span class=title><?=$ec_form_word['growth_scheme_form']?></span>";
answersheet_template = "<?=$ec_form_word['answersheet_template']?>";
answersheet_header = "<?=$ec_form_word['question_title']?>";
answersheet_type = "<?=$ec_form_word['fill_in_method']?>";
no_options_for = "<?=$ec_form_word['no_options_for']?>";
pls_specify_type = "<?=$ec_form_word['fill_in_type']?>";
pls_fill_in = "<?=$ec_form_word['fill_in_content']?>";
chg_title = "<?=$ec_form_word['change_heading']?>";
chg_template = "<?=$ec_form_word['confirm_to_template']?>";

answersheet_tf = "<?=$ec_form_word['answersheet_tf']?>";
answersheet_mc = "<?=$ec_form_word['answersheet_mc']?>";
answersheet_mo = "<?=$ec_form_word['answersheet_mo']?>";
answersheet_sq1 = "<?=$ec_form_word['answersheet_sq1']?>";
answersheet_sq2 = "<?=$ec_form_word['answersheet_sq2']?>";
answersheet_option = "<?=$answersheet_option?>";

answersheet_not_applicable = "<?=$ec_form_word['answersheet_not_applicable']?>";

/************************************************************/
answersheet_likert = "<?=$ec_form_word['answersheet_ls']?>";
answersheet_table = "<?=$ec_form_word['answersheet_tl']?>";
answersheet_question = "<?=$ec_form_word['answersheet_no_ques']?>";
no_questions_for = "<?=$ec_form_word['answersheet_no_select_ques']?>";
question_scale = "<?=$ec_form_word['question_scale']?>";
question_question = "<?=$ec_form_word['question_question']?>";
/************************************************************/

button_submit = " <?=$button_submit?> ";
button_add = " <?=$button_add?> ";
button_cancel = " <?=$button_cancel?> ";
button_update = " <?=$button_update?> ";


background_image = "";

var sheet = new Answersheet();
// attention: MUST replace '"' to '&quot;'
var tmpStr = document.ansForm.qStr.value;

<? 
if($person_right != "NO")
echo "var tmpStrA = document.ansForm.aStr.value;";
else
echo "var tmpStrA = '';";
?>

//sheet.qString = tmpStr.replace(/\"/g, "&quot;");
sheet.qString = tmpStr.replace(/\&quot;/g, "\"");
sheet.aString = tmpStrA.replace(/\&quot;/g, "\"");
sheet.mode = 1;	// 0:edit 1:fill in application
sheet.displaymode = <?=$phase_obj["sheettype"]?>;	// 0:edit 1:fill in application
sheet.answer = sheet.sheetArr();

// temp
var form_templates = new Array();

sheet.templates = form_templates;

<?php
} else {
$phase_obj["answersheet"] = str_replace("\"", "\\\"", undo_htmlspecialchars($phase_obj["answersheet"]));
$phase_obj["answersheet"] = ($phase_obj["sheettype"]!=2) ? preg_replace('(\r\n|\n)', "<br>", str_replace("&amp;", "&", $phase_obj["answersheet"])) : preg_replace('(\r\n|\n)', "", str_replace("&amp;", "&", $phase_obj["answersheet"]));
?>
var myQue = "<?=$phase_obj["answersheet"]?>";
var myAns = "<?=preg_replace('(\r\n|\n)', "<br>", str_replace("&amp;", "&", $handin_obj["answer"]))?>";
var msg_not_answered = "<i><font color=gray>&lt;<?=$ec_iPortfolio['form_not_answered']?>&gt;</font></i>";
var displaymode = <?=$phase_obj["sheettype"]?>;
question_scale = "<?=$ec_form_word['question_scale']?>";
<?php
}
?>

<?php if($sys_custom['iPf']['chiuchunkg']['SBS']): ?>
$(function(){
	$('[name^="TB_"]').find('tr:first td:eq(1)').width(350)
});
<?php endif; ?>
</SCRIPT>


<script language="JavaScript">

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	document.form2.action = "index.php";
	document.form2.submit();
}

// Quick student switch 
function jCHANGE_STUDENT(jParShift, jParToExtreme)
{
	var StudentSelect = document.getElementsByName('StudentID');
	var OriginalIndex = StudentSelect[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(!jParToExtreme)
	{
		if(TargetIndex >= StudentSelect[0].length)
		{
			StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			return;
		}
		else if(TargetIndex < 0)
		{
			StudentSelect[0].selectedIndex = 0;
			return;
		}
		else
			StudentSelect[0].selectedIndex = TargetIndex;
	}
	else
	{
		if(jParShift < 0)
		{
			if(OriginalIndex > 0)
				StudentSelect[0].selectedIndex = 0;
			else
				return;
		}
		else
		{
			if(OriginalIndex < StudentSelect[0].length-1)
				StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			else
				return;
		}
	}
	
	document.form2.action = "index.php";
	document.form2.submit();
}

// Search student name
function jSUBMIT_SEARCH(){
	document.form2.action = "../../school_records_class.php";
	document.form2.submit();
}

// Change page to display student detail information
function jTO_LP()
{
	document.form2.action = "../learning_portfolio_teacher<?=$iportfolio_lp_version == 2?'_v2':''?>.php";
	document.form2.submit();
}

// Change page to display student record
function jTO_SR()
{
	document.form2.action = "../school_record_teacher.php";
	document.form2.submit();
}

// Change page to display student detail information
function jTO_SI()
{
	document.form2.action = "../student_info_teacher.php";
	document.form2.submit();
}

// viewForm need to have answer_display_mode set.
// answer_display_mode should be a number, but quote it in case it is not set.
var answer_display_mode = '<?= $phase_obj["answer_display_mode"] ?>';
</script>

<?php
if($phase_obj["answer_display_mode"]==1)
$ParArr["SheetData"] = "<script language='javascript'>document.write(editPanel());</script>"; 
else
$ParArr["SheetData"] = "<script language='javascript'>document.write(viewForm(myQue, myAns));</script>"; 

$Content = $lpf->GET_PHASE_FORM($ParArr);
?>

<FORM name="form2" method="POST" action="index.php">
	<input type="hidden" name="FieldChanged" />
	<input type="hidden" name="parent_id" value="<?=$parent_id?>" />
	<input type="hidden" name="assignment_id" value="<?=$assignment_id?>" />
	<input type="hidden" name="Mode" value="<?=$Mode?>" />

	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan=2>
				<table border="0" cellspacing="0" cellpadding="3" width="100%">
					<tr>
						<td>
							<?=$class_selection_html?>
						</td>
						<td align="right">
							<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$ec_iPortfolio['enter_student_name']:intranet_htmlspecialchars(stripslashes($search_name)))?>" onFocus="if(this.value=='<?=$ec_iPortfolio['enter_student_name']?>'){this.value=''}" onBlur="if(this.value==''){this.value='<?=$ec_iPortfolio['enter_student_name']?>'}" />
							<input type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onClick="jSUBMIT_SEARCH()" />
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td class="navigation">
										<?=$lpf->GET_NAVIGATION($MenuArr);?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="17" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_01_tea.gif" width="17" height="37"></td>
						<td height="37" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_03_tea.gif">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="200">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center" valign="middle" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_02_tea.gif"  class="page_title"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record.gif" alt="<?=$iPort['menu']['school_based_scheme']?>" width="20" height="20" border="0" align="absmiddle"> <?=$iPort['menu']['school_based_scheme']?></td>
											</tr>
										</table>
									</td>
									<td align="left" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#E8E8E8">
											<tr>
												<td align="left">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr align="center" valign="middle">
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(-1, true)" class="tablebottomlink">&lt;&lt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(-1, false)" class="tablebottomlink">&lt;</a></td>
															<td nowrap>
																<span class="tabletext">
																	<?=$student_selection_html?>
																</span>
															</td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(1, false)" class="tablebottomlink">&gt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(1, true)" class="tablebottomlink">&gt;&gt;</a></td>
															<td width="15" nowrap> | </td>
															<td nowrap class="tabletext"><?=str_replace("<!--NoRecord-->", count($student_detail_arr), $ec_iPortfolio['total_record'])?></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_04_tea.gif" width="13" height="37"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif" width="17" height="20"></td>
						<td>
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<tr>
									<td valign="top">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td>
													<table border="0" cellspacing="0" cellpadding="0" >
														<tr>
															<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_01.gif" style="padding-left:10px; padding-top:10px;"><?=$student_obj['PhotoLink']?></td>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="10" height="10"></td>
														</tr>
														<tr>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
															<td height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_04.gif" width="10" height="10"></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" class="tabletext"><?=Get_Lang_Selection($student_obj['ChineseName'],$student_obj['EnglishName'])?></td>
											</tr>
											<tr>
												<td align="center">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr>
															<td valign="top"><a href="javascript:jTO_SI()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_stu_info.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0"></a></td>
<?php
	if(in_array($default_student_id, $act_student_id_arr)) {
		if($lpf_sturec->HAS_SCHOOL_RECORD_VIEW_RIGHT()) {
?>
															<td valign="top"><a href="javascript:jTO_SR()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record.gif" alt="<?=$ec_iPortfolio['school_record']?>" width="20" height="20" border="0"></a></td>
<?php
		}
		if(strstr($ck_user_rights, ":web:")) {
?>
															<td valign="top">
																<a href="javascript:jTO_LP()">
																	<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_learning_portfolio.gif" alt="<?=$iPort['menu']['learning_portfolio']?>" width="20" height="20" border="0"><br>
																	<?=$lpf->GET_LP_NOT_VIEW_COUNT_BY_USER_ID($default_student_id) > 0 ? "<span class='new_alert'>New</span>" : ""?>
																</a>
															</td>
<?php
		}
	}
?>
															<td valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_sbs_on.gif" alt="<?=$iPort['menu']['school_based_scheme']?>" width="20" height="20" border="0"></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
									<td width="90%">
										<br />
<?php
	if(in_array($default_student_id, $act_student_id_arr))
		echo $Content;
	else {
?>
										<table>
											<tr>
												<td><?=$ec_iPortfolio['suspend_result_inactive']?></td>
											</tr>
										</table>
<?php } ?>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif" width="13" height="37"></td>
					</tr>
					<tr>
						<td width="17" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_08.gif" width="17" height="17"></td>
						<td height="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif" width="64" height="17"></td>
						<td width="13" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_10.gif" width="13" height="17"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</FORM>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
