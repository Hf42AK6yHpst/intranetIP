<?php
/*
 * 	Log
 * 	
 * 	Purpose: save import merit / demerit
 * 
 * 	Date:	2016-01-28 [Cameron]
 * 			create this file
 * 
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");		// authen
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

intranet_opendb();

$lpf = new libportfolio2007();
$lpf->CHECK_ACCESS_IPORTFOLIO();
$lpf->ADMIN_ACCESS_PAGE();

$linterface = new interface_html("popup.html");

# Initialization
$numOfLVL = 4;	// available record type

# tag information	
$CurrentPage = "SAMS_import_anp";
$title = $ec_iPortfolio['SAMS_import_anp'];
$TAGS_OBJ[] = array($title,"",0);
$MODULE_OBJ["title"] = $title;

	
# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION[] = array();
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


# handle return message
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);


### step display
$htmlAry['generalImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep=3);



function import_data() {
	global $eclass_db, $lpf, $numOfLVL;
	
	$successAry = array();
	
	### retrieve import data
	$tmpTable = $eclass_db.".TEMP_IMPORT_SAMS_ANP";
	$table_name = $eclass_db.'.MERIT_STUDENT';
	
	#################################################
	## start 
	$sql = "Select * From $tmpTable Where LoginUserID = '".$_SESSION['UserID']."'";
	
	$importDataAry = $lpf->returnResultSet($sql);
	$numOfImportData = count($importDataAry);
	$studentIDAry = Get_Array_By_Key($importDataAry, 'UserID');
	
	### retrieve existing data for determining insert or update records
	$sql = "Select RecordID, UserID, IF(AcademicYearID IS NULL OR AcademicYearID='','Dummy',AcademicYearID) AS AcademicYearID, ". 
			"IF(YearTermID IS NULL OR YearTermID='','Dummy',YearTermID) AS YearTermID, DATE(MeritDate) as MeritDate, ".
			"RecordType From ". $table_name." Where UserID IN ('".implode("','", (array)$studentIDAry)."')";
	$rs = $lpf->returnResultSet($sql);
	$existingDataAry = BuildMultiKeyAssoc($rs, array('UserID', 'AcademicYearID', 'YearTermID', 'MeritDate', 'RecordType'));
	unset($rs);
	
	### process the import data
	$insertAry = array();	
	$lpf->Start_Trans();
	for ($i=0; $i<$numOfImportData; $i++) {
		$r = $importDataAry[$i];
	//	$_SchoolYear = "'".$lpf->Get_Safe_Sql_Query($r['SchoolYear'])."'";
	//	$_ClassLevel = "'".$lpf->Get_Safe_Sql_Query($r['ClassLevel'])."'";
		$_ClassName = "'".$lpf->Get_Safe_Sql_Query($r['ClassName'])."'";
		$_ClassNumber = "'".$lpf->Get_Safe_Sql_Query($r['ClassNumber'])."'";
	//	$_RegisterNumber = "'".$lpf->Get_Safe_Sql_Query($r['RegisterNumber'])."'";
	//	$_StudentNameEng = "'".$lpf->Get_Safe_Sql_Query($r['StudentNameEng'])."'";
	//	$_StudentNameChi = "'".$lpf->Get_Safe_Sql_Query($r['StudentNameChi'])."'";
	
		$_ANPType = $r['ANPType'];
		$_ANPDate = $r['ANPDate'];
		$_Description = $r['Description'];
		$_CodeDescription = $r['CodeDescription'];
		for ($j=1; $j<=$numOfLVL;$j++) {
			${"_LVL$j"} = $r["LVL{$j}"];	
		}
		
//		$_StaffCode = "'".$lpf->Get_Safe_Sql_Query($r['StaffCode'])."'";
		$_TeacherNameEng = $r['TeacherNameEng'];
		$_TeacherNameChi = $r['TeacherNameChi'];
		$_TeacherName = empty($_TeacherNameEng) ? $_TeacherNameChi : $_TeacherNameEng;
		$_TeacherName = "'".$lpf->Get_Safe_Sql_Query($_TeacherName)."'";
		
		$_UserID = $r['UserID'];
		$_AcademicYearID = $r['AcademicYearID'];
		$_Year = "'".$lpf->Get_Safe_Sql_Query($r['Year'])."'";
		$_YearTermID = $r['YearTermID'];
		$_Semester = "'".$lpf->Get_Safe_Sql_Query($r['Semester'])."'";

		for ($j=1; $j<=$numOfLVL;$j++) {
			if (${"_LVL$j"} > 0) {
				$_RecordType = ($_ANPType == 'P') ? -$j : $j;				
				if (($_CodeDescription != '') && ($_Description != '')) {
					$_Reason = $_CodeDescription;
					$_Remark = $_Description;
				}	
				else if (($_CodeDescription != '') && ($_Description == '')) {
					$_Reason = $_CodeDescription;
					$_Remark = '';
				}
				else if (($_CodeDescription == '') && ($_Description != '')) {
					$_Reason = $_Description;
					$_Remark = '';
				}
				else {
					$_Reason = '';
					$_Remark = '';
				}
				
				$_Reason = "'".$lpf->Get_Safe_Sql_Query($_Reason)."'";
				$_Remark = "'".$lpf->Get_Safe_Sql_Query($_Remark)."'";
				
				if (isset($existingDataAry[$_UserID][$_AcademicYearID][$_YearTermID][$_ANPDate][$_RecordType])) {	// update
					$_RecordID = $existingDataAry[$_UserID][$_AcademicYearID][$_YearTermID][$_ANPDate][$_RecordType]['RecordID'];		
				
					$sql = "Update ".$table_name." Set NumberOfUnit=".${"_LVL$j"}.", ".
						"Reason=".$_Reason.", Remark=".$_Remark.", PersonInCharge=".$_TeacherName.", ModifiedDate = now() ".
							"Where RecordID = '".$_RecordID."'";
					$successAry[] = $lpf->db_db_query($sql);
				}
				else {	// insert
				
					$insertAry[] = " ('".$_UserID."', '".$_AcademicYearID."', ".$_Year.", '".$_YearTermID."', ".$_Semester.
						",".$_ClassName.",".$_ClassNumber.",'".$_ANPDate."','".${"_LVL$j"}."',".$_Reason.",".$_TeacherName.
						", '".$_RecordType."', NULL,".$_Remark.", now(), now()) ";
				}
			}
		}
	}
	
	if (count($insertAry) > 0) {
		$field = "UserID, AcademicYearID, Year, YearTermID, Semester, ClassName, ClassNumber, MeritDate, NumberOfUnit, Reason, PersonInCharge, RecordType, RecordStatus, Remark, InputDate, ModifiedDate";
		$insertChunkAry = array_chunk($insertAry, 1000);
		$numOfChunk = count($insertChunkAry);
		
		for ($i=0; $i<$numOfChunk; $i++) {
			$_insertAry = $insertChunkAry[$i];
			
			$sql = "INSERT INTO ".$table_name." ($field) VALUES ".implode(', ', $_insertAry);
			$successAry[] = $lpf->db_db_query($sql);
		}
	}
	unset($insertAry);
	if (!in_array(false,$successAry)) {
		$lpf->Commit_Trans();
	}
	else {
		$lpf->RollBack_Trans();
		$numOfImportData = 0;
	}
	return $numOfImportData;
	## end 	
}

$nrRecord = import_data();


$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$title.'</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['importInfoTbl'] = $x;


# result display
$htmlAry['numOFSuccessDisplay'] = $nrRecord.' '.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Successful'];


### action buttons
$htmlAry['CloseBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Close'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", false, $ParClass="", $ParExtraClass="actionBtn");

?>
<script type="text/JavaScript" language="JavaScript">
function goCancel() {
	self.close();
}
</script>
<form id="form1" name="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br />
	
	<?=$htmlAry['generalImportStepTbl']?>
	
	<div class="table_board">
		<?=$htmlAry['importInfoTbl']?>
		<br style="clear:both;" />
		<br style="clear:both;" />
		
		<div style="width:100%; text-align:center;">
			<?=$htmlAry['numOFSuccessDisplay']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['CloseBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>