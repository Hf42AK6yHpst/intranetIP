<?php
/*
 * 	Log
 * 	
 * 	Purpose: import merit / demerit validation
 * 
 * 	Date:	2016-01-28 [Cameron]
 * 			create this file
 * 
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");		// authen
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

intranet_opendb();

$lpf = new libportfolio2007();
$lpf->CHECK_ACCESS_IPORTFOLIO();
$lpf->ADMIN_ACCESS_PAGE();

$linterface = new interface_html("popup.html");
$limport = new libimporttext();

# Initialization
$param = '';
$studentOfYear = array();
$numOfLVL = 4;	// available record type

### get header
$format_array = array("REG NO.", "SCHOOL YEAR", "SCHOOL SESSION", "CLASS LEVEL", "CLASS", "CLASS NO", "STUDENT ENG NAME", "STUDENT CHN NAME", "ANP TYPE", "ANP DATE", "DESCRIPTION", "EVENT CODE", "CODE DESCRIPTION ENG", "CODE DESCRIPTION CHN", "LVL1", "LVL2", "LVL3", "LVL4", "LVL5", "CONDUCT MARK", "STAFF CODE", "TEACHER ENG NAME", "TEACHER CHN NAME", "PRINT INDICATOR", "ACTION TAKEN", "ACTION TAKEN DATE FROM", "ACTION TAKEN DATE TO", "CLASS DETENTION INDICATOR", "REPORT CARD PRINT SEQ", "AWARD CATEGORY", "AWARD FROM", "SLP REMARKS", "SLP READ INDICATOR", "ANTECEDENCE",	"ADDENDUM",	"REMARKS", "OTHER LANGUAGE", "OTHER ANTECEDENCE", "OTHER ADDENDUM",	"OTHER DESCRIPTION");

### get csv data
$filepath = $_FILES['userfile']['tmp_name'];
$data = $limport->GET_IMPORT_TXT($filepath, $incluedEmptyRow=0, $lineBreakReplacement='<!--LineBreak-->');
$limport->SET_CORE_HEADER($format_array);
if (!$limport->VALIDATE_HEADER($data, true) || !$limport->VALIDATE_DATA($data)) {
	header("Location: import_sams.php?$param&returnMsgKey=WrongCSVHeader");
	exit();
}


# tag information	
$CurrentPage = "SAMS_import_anp";
$title = $ec_iPortfolio['SAMS_import_anp'];
$TAGS_OBJ[] = array($title,"",0);
$MODULE_OBJ["title"] = $title;

	
# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION[] = array();
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


# handle return message
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);


### step display
$htmlAry['generalImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep=2);


### delete old temp data
$table = $eclass_db.".TEMP_IMPORT_SAMS_ANP";
$sql = "Delete From ".$table." Where LoginUserID = '".$_SESSION['UserID']."'";
$successAry['deleteOldTempData'] = $lpf->db_db_query($sql);


## get academic year term array
$yearTerm_ary = $lpf->buildAcademicYearTermAry(); 				
$academicYear_ary = array_keys($yearTerm_ary);

## get staff associate array
$staff_ary = $lpf->getStaffInfo();
$staffAssocCode_ary = BuildMultiKeyAssoc($staff_ary, 'StaffCode');
$staffAssocNameEng_ary = BuildMultiKeyAssoc($staff_ary, 'EnglishName');
$staffAssocNameChi_ary = BuildMultiKeyAssoc($staff_ary, 'ChineseName');

### analyse data
array_shift($data);
$numOfData = count($data);
$errorMsgAssoAry = array();
$insertAry = array();
for ($i=0; $i<$numOfData; $i++) {
	$_rowNum = $i+2;
	$_row = $data[$i];
	$_registerNumber = trim($_row[0]);	// webSAMSRegNo
	if (substr($_registerNumber,0,1) != '#') {
		$_registerNumber = '#'.$_registerNumber;	// webSAMSRegNo
	}
	
	$_schoolYear = str_replace(' ', '', trim($_row[1]));
	$_classLevel = trim($_row[3]);
	$_className = trim($_row[4]);
	$_classNumber = trim($_row[5]);
	$_studentNameEng = trim($_row[6]);
	$_studentNameChi = trim($_row[7]);
	$_ANPType = trim($_row[8]);				// A - merit(award), P - demerit(punishment)
	$_ANPDate = trim($_row[9]);
	$_description = trim($_row[10]);
	$_codeDescription = trim($_row[13]);
	$_LVL1 = trim($_row[14]);
	$_LVL2 = trim($_row[15]);
	$_LVL3 = trim($_row[16]);
	$_LVL4 = trim($_row[17]);
	$_staffCode = trim($_row[20]);
	if (substr($_staffCode,0,1) != '#') {
		$_staffCode = '#'.$_staffCode;
	}
	$_teacherNameEng = trim($_row[21]);
	$_teacherNameChi = trim($_row[22]);
	$_y_len = strlen($_schoolYear); 
	if ($_y_len == 4 || $_y_len == 9) {		// assume year in this format: 2016	or 2016-2017
		$_year = substr($_schoolYear,0,4);
	}
	else if ($_y_len == 2 || $_y_len == 5) {	// assume year in this format: 16 or 16/17
		$_year = substr(date('Y'),0,2).substr($_schoolYear,0,2);
	}
	else {
		$_year = 0;
	}

	if (!isset($studentOfYear[$_year]) && $yearTerm_ary[$_year]['AcademicYearID']) {
		$studentOfYear[$_year] = $lpf->getStudentByAcademicYear($yearTerm_ary[$_year]['AcademicYearID']);

		$tmpStudentClass_ary = BuildMultiKeyAssoc($studentOfYear[$_year], array('ClassName','ClassNumber'), 'UserID',1);
		$studentClass_ary[$_year] = $tmpStudentClass_ary;
		unset($tmpStudentClass_ary);

		$detail_field = array('ClassLevel','ClassName','ClassNumber','UserID','StudentNameEng','StudentNameChi'); 
		$studentDetail_ary[$_year] = BuildMultiKeyAssoc($studentOfYear[$_year], array('WebSAMSRegNo'),$detail_field,1);

		$studentWebSAMSRegNo_ary[$_year] = BuildMultiKeyAssoc($studentOfYear[$_year], 'UserID','WebSAMSRegNo',1);
	}

	## check 1: academic year
	if (!in_array($_year,(array)$academicYear_ary)) {
		$errorMsgAssoAry[$_rowNum][] = 'academicYearNotFound';
	}
	else {
		## check 2: webSAMSRegNo #
		$_userID = '';
		if (($_registerNumber != '#') && isset($studentDetail_ary[$_year][$_registerNumber]['UserID'])) {
			$_userID = $studentDetail_ary[$_year][$_registerNumber]['UserID'];
		}
		else if ($_className != '' && $_classNumber != '' && isset($studentClass_ary[$_year][$_className][$_classNumber])) {
			$_userID = $studentClass_ary[$_year][$_className][$_classNumber];
			$_registerNumber = $studentWebSAMSRegNo_ary[$_year][$_userID];
			if (substr($_registerNumber,0,2) == '##') {	// empty webSAMSNo
				$errorMsgAssoAry[$_rowNum][] = 'webSAMSRegNoNotFound';
			}
		}

		if ($_userID == '') {
			$errorMsgAssoAry[$_rowNum][] = 'studentNotFound';
		}
		else {	// get ClassLevel, ClassName, ClassNumber, StudentNameEng, StudentNameChi from system if conflict with that in csv
			$_classLevel = ($_classLevel != $studentDetail_ary[$_year][$_registerNumber]['ClassLevel']) ? $studentDetail_ary[$_year][$_registerNumber]['ClassLevel'] : $_classLevel;  
			$_className = ($_className != $studentDetail_ary[$_year][$_registerNumber]['ClassName']) ? $studentDetail_ary[$_year][$_registerNumber]['ClassName'] : $_className;
			$_classNumber = ($_classNumber != $studentDetail_ary[$_year][$_registerNumber]['ClassNumber']) ? $studentDetail_ary[$_year][$_registerNumber]['ClassNumber'] : $_classNumber;
			$_studentNameEng = ($_studentNameEng != $studentDetail_ary[$_year][$_registerNumber]['StudentNameEng']) ? $studentDetail_ary[$_year][$_registerNumber]['StudentNameEng'] : $_studentNameEng;
			$_studentNameChi = ($_studentNameChi != $studentDetail_ary[$_year][$_registerNumber]['StudentNameChi']) ? $studentDetail_ary[$_year][$_registerNumber]['StudentNameChi'] : $_studentNameChi;
		}	// check 2
	}	// check 1
	
	## check 3: teacher exist or not
	$teacherPass = false;
	if ($_staffCode != '#' && isset($staffAssocCode_ary[$_staffCode])) {
		$teacherPass = true;
		$_teacherNameEng = $staffAssocCode_ary[$_staffCode]['EnglishName'];		// retrieve from system
		$_teacherNameChi = $staffAssocCode_ary[$_staffCode]['ChineseName'];
	}
	else if ($_teacherNameEng != '' && isset($staffAssocNameEng_ary[$_teacherNameEng]) 
		&& $_teacherNameChi != '' && isset($staffAssocNameChi_ary[$_teacherNameChi])) {
		$teacherPass = true;		
	}
	else if ($_teacherNameEng != '' && isset($staffAssocNameEng_ary[$_teacherNameEng])) {
		$teacherPass = true;
		$_teacherNameChi = $staffAssocCode_ary[$_staffCode]['ChineseName'];
	}
	else if ($_teacherNameChi != '' && isset($staffAssocNameChi_ary[$_teacherNameChi])) {
		$teacherPass = true;
		$_teacherNameEng = $staffAssocCode_ary[$_staffCode]['EnglishName'];
	}
	else {
		$errorMsgAssoAry[$_rowNum][] = 'teacherNotFound';
	}
	
	if (($_ANPType != 'A') && ($_ANPType != 'P')) {
		$errorMsgAssoAry[$_rowNum][] = 'wrongANPType';
	}

	$_ANPDate = getDefaultDateFormat($_ANPDate);
	if(!checkDateIsValid($_ANPDate))
	{
		$errorMsgAssoAry[$_rowNum][] = 'wrongANPDate';
		$_yearTermID = 0;
		$_semester = '';
	}
	else {
		$anpYearTermAry = getAcademicYearAndYearTermByDate($_ANPDate);
	    $_cademicYearID = $anpYearTermAry['AcademicYearID']; 
//	    $_cademicYearName = $anpYearTermAry['AcademicYearName'];
	    $_yearTermID = $anpYearTermAry['YearTermID'];
	    $_semester = $anpYearTermAry['YearTermName'];
	    if ($_cademicYearID != $yearTerm_ary[$_year]['AcademicYearID']) {
	    	$errorMsgAssoAry[$_rowNum][] = 'ANPDateOutofRange';		// out of the academic year
	    }
	}
		  
	for ($j=1;$j<=$numOfLVL;$j++) {		  
		if (!is_numeric(${"_LVL$j"}) || ${"_LVL$j"} < 0) {
			$errorMsgAssoAry[$_rowNum][] = "wrongLVL$j";
		}
	}
		
	if (($_description == '') && ($_codeDescription == '')) {
		$errorMsgAssoAry[$_rowNum][] = 'emptyDescription';
	}

	
	$insertAry[] = " ('".$_SESSION['UserID']."', '".$_rowNum."', '".$lpf->Get_Safe_Sql_Query($_schoolYear)."', '".
							$lpf->Get_Safe_Sql_Query($_classLevel)."', '".$lpf->Get_Safe_Sql_Query($_className)."', '".
							$lpf->Get_Safe_Sql_Query($_classNumber)."', '".$lpf->Get_Safe_Sql_Query($_registerNumber)."', '".
							$lpf->Get_Safe_Sql_Query($_studentNameEng)."', '".$lpf->Get_Safe_Sql_Query($_studentNameChi)."', '".
							$lpf->Get_Safe_Sql_Query($_ANPType)."', '".$lpf->Get_Safe_Sql_Query($_ANPDate)."', '".
							$lpf->Get_Safe_Sql_Query($_description)."', '".$lpf->Get_Safe_Sql_Query($_codeDescription)."', '".
							$lpf->Get_Safe_Sql_Query($_LVL1)."', '".$lpf->Get_Safe_Sql_Query($_LVL2)."', '".
							$lpf->Get_Safe_Sql_Query($_LVL3)."', '".$lpf->Get_Safe_Sql_Query($_LVL4)."', '".
							$lpf->Get_Safe_Sql_Query($_staffCode)."', '".
							$lpf->Get_Safe_Sql_Query($_teacherNameEng)."', '".$lpf->Get_Safe_Sql_Query($_teacherNameChi)."', '".
							$_userID."', '".$yearTerm_ary[$_year]['AcademicYearID']."', '".
							$lpf->Get_Safe_Sql_Query($yearTerm_ary[$_year]['YearNameEN'])."', '".
							$lpf->Get_Safe_Sql_Query($_yearTermID)."', '".
							$lpf->Get_Safe_Sql_Query($_semester)."', now()) ";
}


### simple statistics
$numOfErrorRow = count($errorMsgAssoAry);
$numOfSuccessRow = $numOfData - $numOfErrorRow;


### insert csv data to temp table
$numOfInsert = count($insertAry);
if ($numOfInsert > 0) {
	$insertChunkAry = array_chunk($insertAry, 1000);
	$numOfChunk = count($insertChunkAry);
	
	for ($i=0; $i<$numOfChunk; $i++) {
		$_insertAry = $insertChunkAry[$i];
		
		$sql = "Insert Into $table
					(LoginUserID,RowNumber,SchoolYear,ClassLevel,ClassName,ClassNumber,RegisterNumber,StudentNameEng,StudentNameChi,
					ANPType,ANPDate,Description,CodeDescription,LVL1,LVL2,LVL3,LVL4,StaffCode,TeacherNameEng,TeacherNameChi,UserID,
					AcademicYearID,Year,YearTermID,Semester,InputDate)
				Values ".implode(', ', (array)$_insertAry);
		$successAry['insertData'][] = $lpf->db_db_query($sql);
	}
}


# validation result to display
if ($numOfErrorRow > 0) {
	$numOfErrorDisplay = '<span class="tabletextrequire">'.$numOfErrorRow.'</span>';
}
else {
	$numOfErrorDisplay = 0;
}

$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$title.'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= "&nbsp;";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['SuccessfulRecord'].'</td>'."\n";
		$x .= '<td>'.$numOfSuccessRow.'</td>'."\n";
	$x .= '</tr>'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['FailureRecord'].'</td>'."\n";
		$x .= '<td>'.$numOfErrorDisplay.'</td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\r\n";
$htmlAry['importInfoTbl'] = $x;


# error display
$x = '';
if ($numOfErrorRow > 0) {
	$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\n";
		$x .= '<thead>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<th>'.$Lang['General']['ImportArr']['Row'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['WebSAMSRegNo'].'</th>'."\n";				
				$x .= '<th>'.$Lang['General']['SchoolYear'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['ClassLevel'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['ClassName'].'</th>'."\n";
				$x .= '<th>'.$Lang['General']['ClassNumber'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['StudentNameEng'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['StudentNameChi'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['ANPType'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['ANPDate'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Description'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['CodeDescription'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['LVL1'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['LVL2'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['LVL3'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['LVL4'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['StaffCode'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['TeacherNameEng'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['TeacherNameChi'].'</th>'."\n";
				$x .= '<th>'.$Lang['General']['Remark'].'</th>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</thead>'."\n";
		$x .= '<tbody>'."\n";
		
			foreach ((array)$errorMsgAssoAry as $_rowNum => $_errorAry) {
				$aryIndex = $_rowNum - 2;
				$_row = $data[$aryIndex];
				$_registerNumber = trim($_row[0]);	// webSAMSRegNo
				$_schoolYear = str_replace(' ', '', trim($_row[1]));
				$_classLevel = trim($_row[3]);
				$_className = trim($_row[4]);
				$_classNumber = trim($_row[5]);
				$_studentNameEng = trim($_row[6]);
				$_studentNameChi = trim($_row[7]);
				$_ANPType = trim($_row[8]);				// A - merit(award), P - demerit(punishment)
				$_ANPDate = trim($_row[9]);
				$_description = trim($_row[10]);
				$_codeDescription = trim($_row[13]);
				$_LVL1 = trim($_row[14]);
				$_LVL2 = trim($_row[15]);
				$_LVL3 = trim($_row[16]);
				$_LVL4 = trim($_row[17]);
				$_staffCode = trim($_row[20]);
				$_teacherNameEng = trim($_row[21]);
				$_teacherNameChi = trim($_row[22]);
				
				$_errorDisplayAry = array();

				if (in_array('academicYearNotFound', $_errorAry)) {
					$_schoolYear = '<span class="tabletextrequire">'.($_schoolYear?$_schoolYear:'***').'</span>';
					$_errorDisplayAry[] = $Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['AcademicYearNotFound'];
				}
				
				if (in_array('studentNotFound', $_errorAry)) {
					if ($_registerNumber=='' && $_className=='' && $_classNumber=='') {
						$_registerNumber = '<span class="tabletextrequire">***</span>';
						$_className = '<span class="tabletextrequire">***</span>';
						$_classNumber = '<span class="tabletextrequire">***</span>';
					}
					else if ($_registerNumber != '') {
						$_registerNumber = '<span class="tabletextrequire">'.$_registerNumber.'</span>';
					}
					else {
						$_className = '<span class="tabletextrequire">'.$_className.'</span>';
						$_classNumber = '<span class="tabletextrequire">'.$_classNumber.'</span>';
					}
					$_studentNameEng = '<span class="tabletextrequire">'.$_studentNameEng.'</span>';
					
					$_errorDisplayAry[] = $Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['StudentNotFound'];
				}

				if (in_array('webSAMSRegNoNotFound', $_errorAry)) {
					$_registerNumber = '<span class="tabletextrequire">***</span>';
					$_errorDisplayAry[] = $Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['WebSAMSRegNoNotFound'];
				}
				
				if (in_array('teacherNotFound', $_errorAry)) {
					$_staffCode = '<span class="tabletextrequire">'.($_staffCode?$_staffCode:'***').'</span>';
					$_teacherNameEng = '<span class="tabletextrequire">'.($_teacherNameEng?$_teacherNameEng:'***').'</span>';
					$_teacherNameChi = '<span class="tabletextrequire">'.($_teacherNameChi?$_teacherNameChi:'***').'</span>';
					$_errorDisplayAry[] = $Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['TeacherNotFound'];
				}
				
				if (in_array('wrongANPType', $_errorAry)) {
					$_ANPType = '<span class="tabletextrequire">'.($_ANPType?$_ANPType:'***').'</span>';
					$_errorDisplayAry[] = $Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['WrongANPType'];
				}
				if (in_array('wrongANPDate', $_errorAry)) {
					$_ANPDate = '<span class="tabletextrequire">'.($_ANPDate?$_ANPDate:'***').'</span>';
					$_errorDisplayAry[] = $Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['WrongANPDate'];
				}
				if (in_array('ANPDateOutofRange', $_errorAry)) {
					$_ANPDate = '<span class="tabletextrequire">'.($_ANPDate?$_ANPDate:'***').'</span>';
					$_errorDisplayAry[] = $Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['ANPDateOutofRange'];
				}
				
				for ($j=1;$j<=$numOfLVL;$j++) {
					if (in_array("wrongLVL{$j}", $_errorAry)) {
						${"_LVL$j"} = '<span class="tabletextrequire">'.(${"_LVL$j"}?${"_LVL$j"}:'***').'</span>';
						$_errorDisplayAry[] = $Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['WrongLVL'];
					}
				}
				
				if (in_array('emptyDescription', $_errorAry)) {
					$_description = '<span class="tabletextrequire">***</span>';
					$_codeDescription = '<span class="tabletextrequire">***</span>';
					$_errorDisplayAry[] = $Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['EmptyDescription'];
				}
				
				
				$_errorDisplay = '- '.implode('<br />- ', $_errorDisplayAry);
				
				$x .= '<tr>'."\n";
					$x .= '<td>'.$_rowNum.'</td>'."\n";
					$x .= '<td>'.$_registerNumber.'</td>'."\n";
					$x .= '<td>'.$_schoolYear.'</td>'."\n";
					$x .= '<td>'.$_classLevel.'</td>'."\n";
					$x .= '<td>'.$_className.'</td>'."\n";
					$x .= '<td>'.$_classNumber.'</td>'."\n";
					$x .= '<td>'.$_studentNameEng.'</td>'."\n";
					$x .= '<td>'.$_studentNameChi.'</td>'."\n";
					$x .= '<td>'.$_ANPType.'</td>'."\n";
					$x .= '<td>'.$_ANPDate.'</td>'."\n";
					$x .= '<td>'.$_description.'</td>'."\n";
					$x .= '<td>'.$_codeDescription.'</td>'."\n";
					$x .= '<td>'.$_LVL1.'</td>'."\n";
					$x .= '<td>'.$_LVL2.'</td>'."\n";
					$x .= '<td>'.$_LVL3.'</td>'."\n";
					$x .= '<td>'.$_LVL4.'</td>'."\n";
					$x .= '<td>'.$_staffCode.'</td>'."\n";
					$x .= '<td>'.$_teacherNameEng.'</td>'."\n";
					$x .= '<td>'.$_teacherNameChi.'</td>'."\n";
					$x .= '<td>'.$_errorDisplay.'</td>'."\n";
				$x .= '</tr>'."\n";
			}
		$x .= '</tbody>'."\n";
	$x .= '</table>'."\n";
}
$htmlAry['errorTbl'] = $x;



### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Import'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", ($numOfErrorRow>0)? true : false, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['backBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

?>
<script type="text/JavaScript" language="JavaScript">
function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

function goSubmit() {
	$('form#form1').attr('action', 'import_sams_update.php').submit();
}

function goBack() {
	window.location = 'import_sams.php?<?=$param?>';
}
function goCancel() {
	self.close();
}

</script>
<form id="form1" name="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br />
	
	<?=$htmlAry['generalImportStepTbl']?>
	
	<div class="table_board">
		<?=$htmlAry['importInfoTbl']?>
		<?=$htmlAry['errorTbl']?>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>