<?php
// Modifing by anna
/*
 * Modification Log:
 * 2018-12-11 Anna
 * Fixed overal score have N.T. cannot import problem [#H152674]
 * 
 * 2018-01-26 Anna
 * change case default break 2 in while loop , for subject wrong header format, added $SubjectHeader
 *
 * 2018-01-11 Omas
 *	 accept Score - , *  store as -1  ( - : absent not count for score, * : dropped)
 * 2014-09-01 (Bill)
 * error message for wrong subject code and subject component code
 * 
 * 2014-08-27 (Bill) [Z65669 - 五旬節林漢光中學 - Fail to import academic result]
 * allow multiple teaching langugage
 * 
 * 2013-07-23 (Yuen)
 * calculate SD and Mean for each form 
 * 
 *  * 2013-03-27 (Ivan) [email "Urgent! eClass SLP questions" from UCC Poon sir]
 * Improved: Do not remove "*" for grade columns now to cater the case of grade 5* and 5**
 * 
 * 2013-03-21 (Ivan) [email "Urgent! eClass SLP questions" from UCC Poon sir]
 * Improved: Subject score, subject position and overall score accept "N.A." now
 * 
 * 2012-05-30 (Fai) 
 * Improved: Delete exist academic record for the student , then insert a new one (remove the update mechanism)
 *
 * 2011-09-05 (Ivan) [CRM:2011-0805-1016-48066]
 * Improved: added "/" as special case for Subject score
 * 
 * 2011-05-11 (Ivan) [CRM:2011-0329-1514-42096]
 * Improved: ignore * symbols when importing the score
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Profile:ImportData"));
iportfolio_auth("T");
intranet_opendb();

define ("ERROR_TYPE_NO_MATCH_REGNO", 1);
define ("ERROR_TYPE_EMPTY_REGNO", 2);
define ("ERROR_TYPE_INCORRECT_REGNO", 3);
define ("ERROR_TYPE_INCORRECT_CLASSNAME", 4);
define ("ERROR_TYPE_NO_SUBJECT", 5);
define ("SBJ_CMP_SEPARATOR","###");

$IsMultipleAssessment = false;


function Standardize_Import_Data($Score, $forGrade=false) 
{
	$Score = trim($Score);
	
	if (!$forGrade) {
		$Score = str_replace('*', '', $Score);
	}
	
	return $Score;
}

function ParseAssementCode($text)
{
	return trim(str_replace("T4", "", str_replace("T3", "", str_replace("T2", "", str_replace("T1", "", $text)))));
}


if(!empty($academicYearID))
{
  $ay = new academic_year($academicYearID);
  $targetYear = $ay->YearNameEN;
}


if(!empty($YearTermID))
{
  $ayt = new academic_year_term($YearTermID);
  $targetSemester = $ayt->YearTermNameEN;
}
else
{
  $YearTermID = "NULL";
}

# Get Classes Info of this academic year
$libfcm = new form_class_manage();
$ClassInfoArr = $libfcm->Get_Class_List_By_Academic_Year($academicYearID);
$numOfClass = count($ClassInfoArr);

// $ClassInfoAssoArr[$ClassNameEn / $ClassNameCh] = $YearClassID
$ClassInfoAssoArr = array();
for ($i=0; $i<$numOfClass; $i++)
{
	$thisYearClassID = $ClassInfoArr[$i]['YearClassID'];
	$thisClassNameEn = $ClassInfoArr[$i]['ClassTitleEN'];
	$thisClassNameCh = $ClassInfoArr[$i]['ClassTitleB5'];
	
	$ClassInfoAssoArr[$thisClassNameEn] = $thisYearClassID;
	$ClassInfoAssoArr[$thisClassNameCh] = $thisYearClassID;
}
unset($ClassInfoArr);

$OtherFailureDue2SubjectCode = 0;

if ($g_encoding_unicode) {
	$import_coding = ($g_chinese == "") ? "b5" : "gb";
}

# Param: $targetYear, $targetSemester, $IsAnnual
# uploaded file information
$filepath = $userfile;
$filename = $userfile_name;

$del_log = array();

if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath))
{
	header("Location: import_sams.php");
}
else
{
	$lpf = new libpf_sturec();
	$libscm = new subject_class_mapping();
	
	$lpf->CHECK_ACCESS_IPORTFOLIO();
	$lpf->ADMIN_ACCESS_PAGE();
	
	$CurrentPage = "SAMS_import_assessment";
	$title = $ec_iPortfolio['SAMS_import_assessment'];
	$TAGS_OBJ[] = array($title,"");
	$MODULE_OBJ["title"] = $title;
	
	$linterface = new interface_html("popup.html");
	$linterface->LAYOUT_START();
	
	$li = new libdb();
	$lo = new libfilesystem();
	$limport = new libimporttext();
	
	$ext = strtoupper($lo->file_ext($filename));
	if($ext == ".CSV")
	{
		
		$data = $lo->file_read_csv($filepath);// hdebug_r($data);
		//$data = $limport->GET_IMPORT_TXT($filepath);
		$header_row = array_shift($data);                   # drop the title bar
	}


	# Check Title Row
	# Check first 9 rows

	################################################################
	# Fill code here
	# Fixed 1st-9th: *School ID, *School Year, *School Level, *School Session, *Class Level,
	# *Class        *Class Number        *Student Name        *Reg. No.
	#
	################################################################

	# Set and validate for CSV header
	$DefaultHeader = array(
													"School ID",
													"School Year",
													"School Level",
													"School Session",
													"Class Level",
													"Class",
													"Class Number",
													"Student Name",
													"Reg. No."
												);
	$limport->SET_CORE_HEADER($DefaultHeader);
	$numOfColumn = count($header_row);
	for ($i=0; $i<$numOfColumn; $i++) {
		$header_row[$i] = Standardize_Import_Data($header_row[$i]);
	}
	$correct_header = $limport->VALIDATE_HEADER(array($header_row), false);

	
	############### Term Validation (Start) ##############
	$existTermAssessment = array();
	$existTerm = array();
	for($i = 9; $i<count($header_row); $i++){
	    $target = $header_row[$i];
	    $temp = explode("_", $target);
	    $taIndication = $temp[0];
	    
	    if($taIndication == 'CS'){
	        //skip;
	        continue;
	    }
	    
	    if($taIndication != 'An'){
	        preg_match_all('/([\d]+)/', $taIndication, $match);
	        $termNoFromFile = $match[0][0];
	    }else{
	        $termNoFromFile = 'An';
	    }
	    
	    if(!in_array($taIndication,$existTermAssessment)){
	        $existTermAssessment[] = $taIndication;
	    }
	    if(!in_array($termNoFromFile,$existTerm)){
	        $existTerm[] = $termNoFromFile;
	    }
	}
	
	$isOnlyOneTerm = (count($existTermAssessment) == 1 && count($termNoFromFile) == 1);
	
	if($isOnlyOneTerm && $correct_header && !$skipChecking){
	    $sql = "SELECT * FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$academicYearID' order by TermStart";
	    $termInfoArr = $li->returnResultSet($sql);
	    foreach((array)$termInfoArr as $_key => $_arr){
	        $termInfoArr[$_key]['SequenceNumber'] = ($_key+1);
	    }
	    $yearTermSequenceNoAssoc = BuildMultiKeyAssoc($termInfoArr, 'YearTermID', array('SequenceNumber'),1);
	    $yearTermAssocByNo = BuildMultiKeyAssoc($termInfoArr, 'SequenceNumber', array(Get_Lang_Selection('YearTermNameB5','YearTermNameEN')),1);
	    $yearTermAssocByID = BuildMultiKeyAssoc($termInfoArr, 'YearTermID', array(Get_Lang_Selection('YearTermNameB5','YearTermNameEN')),1);
	    
	    $targetTermNumber = $yearTermSequenceNoAssoc[$YearTermID];
	    
	    // $YearTermID == 'NULL' is overall result
	    if(($YearTermID != 'NULL' && $targetTermNumber != $termNoFromFile)
	        || ($YearTermID == 'NULL' && $termNoFromFile != '0')){
	            // may be wrong term
	            $targetToWrongTerm = 1;
	    }
	    
	    if( $YearTermID == 'NULL' && $targetTermNumber == '' && $termNoFromFile == 'An'){
	        unset($targetToWrongTerm);
	    }
	}
	$isContinueToProcess = (!$targetToWrongTerm && $isOnlyOneTerm && $correct_header);
	############### Term Validation (End) ##############
	
	
	# Pos of first subject score
	$pos_first_subject = count($DefaultHeader);
	$SubjectHeader = true;
	if($correct_header){
		
		# Get SubjectCode ID Mapping
		$SubjectAssoArr = $libscm->Get_Subject_Code_ID_Mapping_Arr($MapByShortName=1);
		
	
		# Get pos of overall score and end of file
		$pos_overall_score = 0;
		$pos_end = 0;
		$i = $pos_first_subject;
		
		// unset array
		unset($array_subject_code);
		unset($array_subject_code_language);
		unset($processedSubjectCodeRecordFlag);
	
		
		while ($i < sizeof($header_row))
		{
      		$target = $header_row[$i];
			$temp = explode("_", $target);
			$assessmentText = ParseAssementCode($temp[0]);
			//debug($temp[0], $temp[1], sizeof($temp), $assessmentText);
			if ($assessmentText!=$temp[0] && $assessmentText!="")
			{
				$assessmentText = $temp[0];  # store original format - T1A1
				$AssessmentArr[$assessmentText] = true;
				$AssessmentCount = true;
			} else
			{
				$AssessmentCount = false;
			}
			

			switch(sizeof($temp))
			{
		        case 2:     # Overall result
		          if ($pos_overall_score==0)
		          {
		          	# Yuen how to handle?
		            //$pos_overall_score = $i;
		          }
		          
		          list($set_term, $set_type) = $temp;
		          if ($AssessmentCount)
					{
						$AssessmentHeader[$i] = $assessmentText;
						$AssessmentHeader[$i+1] = $assessmentText;
						$AssessmentHeader[$i+2] = $assessmentText;
						$AssessmentHeader[$i+3] = $assessmentText;
						$AssessmentHeader[$i+4] = $assessmentText;
						$AssessmentHeader[$i+5] = $assessmentText;
					}
		          $AssessmentHeaderType[$i] = "OVERALL";
		          $AssessmentHeaderType[$i+1] = "OVERALL";
		          $AssessmentHeaderType[$i+2] = "OVERALL";
		          $AssessmentHeaderType[$i+3] = "OVERALL";
		          $AssessmentHeaderType[$i+4] = "OVERALL";
		          $AssessmentHeaderType[$i+5] = "OVERALL";
		          $i += 6;
		          break;
		        case 3:     # End of line
		          if ($pos_end==0)
		          {
		            $pos_end = $i;
		          }
		          $i++;
		          break 2;
		        case 4:     # Main Subject
		          list($set_term, $set_subjcode, $set_moi, $set_data_type) = $temp;
		          $array_subject_code[] = $set_subjcode;
		          
		          // Add teaching language of subject to array
		          $array_subject_code_language[$set_subjcode][] = $set_moi;
		          
		          if ($AssessmentCount)
					{
						$AssessmentHeader[$i] = $assessmentText;
						$AssessmentHeader[$i+1] = $assessmentText;
						$AssessmentHeader[$i+2] = $assessmentText;
					}
		          $AssessmentHeaderType[$i] = "MAIN_SUBJECT";
		          $AssessmentHeaderType[$i+1] = "MAIN_SUBJECT";
		          $AssessmentHeaderType[$i+2] = "MAIN_SUBJECT";
		          $i += 3;
		          break;
		        case 5:     # Component Subject
		          list($set_term, $set_subjcode, $set_sbj_cmp_code, $set_moi, $set_data_type) = $temp;
		          $array_subject_code[] = $set_subjcode.SBJ_CMP_SEPARATOR.$set_sbj_cmp_code;

		          // Add teaching language of component subject to array
		          $array_subject_code_language[$set_subjcode.SBJ_CMP_SEPARATOR.$set_sbj_cmp_code][] = $set_moi;
		        
		          if ($AssessmentCount)
					{
						$AssessmentHeader[$i] = $assessmentText;
						$AssessmentHeader[$i+1] = $assessmentText;
						$AssessmentHeader[$i+2] = $assessmentText;
					}

		          $AssessmentHeaderType[$i] = "COMPONENT_SUBJECT";
		          $AssessmentHeaderType[$i+1] = "COMPONENT_SUBJECT";
		          $AssessmentHeaderType[$i+2] = "COMPONENT_SUBJECT";
 		          $i += 3;
		          break;
		        default:    # Wrong file format
		        	$correct_header = false;
		        	$SubjectHeader = false;
		        	break 2;
			}
		
			
			# till the end
			$pos_overall_score = $i-1;
		}

		
		if (sizeof($AssessmentArr)>=1 || $sys_custom['iPf']['ChingChungHauPoWoon']['Report']['StudentLearningProfile'])
		{
			$IsMultipleAssessment = true;
		}
	
		$count_new = 0;
		$count_updated = 0;
//		hdebug_pr($SubjectAssoArr);
		# Process Data
		unset($error_data);
		for ($i=0; $i<sizeof($data);$i++)
		{
		  	// Eric Yip (20100524): skip empty line
     		if(count($data[$i]) == 0) continue;

			$target_year = trim($data[$i][1]);
			$target_class = Big5ToUnicode(trim($data[$i][5]));
			$target_classnum = trim($data[$i][6]);
			$target_regno = trim($data[$i][8]);
					
			#############################################################################
			# Fill here
			# Find studentID from DB
			# Get StudentID by regNo
			if ($target_regno == "")
			{
				$error_data[] = array($i, ERROR_TYPE_EMPTY_REGNO);
				continue;
			}
			else if (substr($target_regno, 0, 1)!="#")
			{
				$error_data[] = array($i, ERROR_TYPE_INCORRECT_REGNO, array($target_regno));
				continue;
			}
		
			$sql = "SELECT
	                  UserID
	                FROM
	                  {$intranet_db}.INTRANET_USER
	                WHERE
	                  (TRIM(WebSAMSRegNo) = '$target_regno' OR CONCAT('#', TRIM(WebSAMSRegNo)) = '$target_regno') AND
	                  RecordType = 2
            ";
			$target_id = current($li->returnVector($sql));
		
			if ($target_id == "")
			{
				//debug($sql);
		    	$error_data[] = array($i, ERROR_TYPE_NO_MATCH_REGNO);
		    	continue;
			}
			  
			# Map ClassID
			$thisYearClassID = $ClassInfoAssoArr[$target_class];
			if ($thisYearClassID == "")	{
			    $error_data[] = array($i, ERROR_TYPE_INCORRECT_CLASSNAME);
			    continue;
			}
		
			#############################################################################
		
			unset($subject_data);
			unset($overall_data);
		
			$j = $pos_first_subject;
			$curr_subj_pos = 0;
			while ($j <= $pos_overall_score)
			{
				if ($AssessmentHeaderType[$j] == "MAIN_SUBJECT" || $AssessmentHeaderType[$j] == "COMPONENT_SUBJECT")
				{
					$skipUpdateRecordFlag = false;     // a flag to indicate whether skip to update/insert the record , default should insert record (false)
	
			    	$t_score = Standardize_Import_Data($data[$i][$j]); 
			    	$t_grade = Standardize_Import_Data($data[$i][$j+1], $forGrade=true);
			    	$t_f_rank = Standardize_Import_Data($data[$i][$j+2]);
		  			
			        if (($t_score == "N.T." && $t_grade == "N.T." && $t_f_rank == "N.T.") || ($t_score == "N.A." && $t_grade == "N.A." && $t_f_rank == "N.A."))
			        {
						$skipUpdateRecordFlag = true;
			        }
			        $t_score = ( $t_score=="*" || $t_score=="-" || $t_score=="" || $t_score=="/" || strtoupper($t_score)=="N.T." || strtoupper($t_score)=="N.A.") ? "-1" : $t_score;
			        $t_score = $t_score * 1;	// change 67.00 to 67
			          
			        // Eric Yip (20100910): No rank input => null value in DB
			        if (strtoupper($t_f_rank)=="N.T." || strtoupper($t_f_rank)=="N.A.") {
			        	$t_rank_classlevel = "NULL";
			        	$t_rank_cl_total = "NULL";
			        }
			        else {
			        	list($t_rank_classlevel, $t_rank_cl_total) = explode("#",$t_f_rank);
				        $t_rank_classlevel = empty($t_rank_classlevel) ? "NULL" : $t_rank_classlevel;
				        $t_rank_cl_total = empty($t_rank_cl_total) ? "NULL" : $t_rank_cl_total;
			        }
			        
			          
			        #############################################################################
			        # Fill here
			        # Update subject data to DB
			        $target_subjcode = Big5ToUnicode($array_subject_code[$curr_subj_pos]);
					$subject_code_type = $target_subjcode;
			
			        # Check is CMP
			        $conds = '';
					if (strpos($target_subjcode,SBJ_CMP_SEPARATOR))
					{
				    	# It is component score
				        $temp = explode(SBJ_CMP_SEPARATOR, $target_subjcode);
				        list($target_subjcode, $target_sbj_cmp_code) = $temp;

				        //$conds = " AND SubjectComponentCode = '$target_sbj_cmp_code' ";
				        $string_sql_sbj_cmp_code = "'$target_sbj_cmp_code'";
				            
				        # check whether the subject code exist
				        $valid_subject_code = $lpf->checkSubjectCode($target_sbj_cmp_code);
				            
				        # Get Parent Subject ID
				        $target_subject_id = $SubjectAssoArr[$target_subjcode];
	
				        $target_subject_component_id = 'NULL';
			            $valid_subject_code = ($target_subject_id=='')? false : $valid_subject_code;
			            	
			            # Get Component Subject ID
				        $t_temp_subject_key = $target_subjcode.SBJ_CMP_SEPARATOR.$target_sbj_cmp_code;
				        $target_subject_component_id = $SubjectAssoArr[$t_temp_subject_key];
				        $conds .= " AND SubjectComponentID = '$target_subject_component_id' ";
			            $valid_subject_code = ($target_subject_component_id=='')? false : $valid_subject_code;
					}
			        else
					{
			            # Normal
			            $conds .= " AND (SubjectComponentCode IS NULL OR SubjectComponentCode='')";
			            $string_sql_sbj_cmp_code = "NULL";

			            # check whether the subject code exist
			            $valid_subject_code = $lpf->checkSubjectCode($target_subjcode);
			                
			            $target_subject_id = $SubjectAssoArr[$target_subjcode];
			            $target_subject_component_id = 'NULL';
		            	//debug($valid_subject_code);
		            	$valid_subject_code = ($target_subject_id=='')? false : $valid_subject_code;
		            	//debug($valid_subject_code, $target_subject_id);
					}	
			          
			        if($valid_subject_code>0)
			        {
			        	$IsAnnual = ($targetSemester=="") ? 1 : 0;
			        	
			            $conds .= ($IsAnnual==0) ? " AND Semester = '$targetSemester'" : " AND IsAnnual = '$IsAnnual'";
			        	
			        	if ($IsMultipleAssessment && $AssessmentHeader[$j]!="")
			        	{
			        		$conds .= " AND TermAssessment='".$AssessmentHeader[$j]."' ";
			        	} else
			        	{
			        		$conds .= " AND TermAssessment IS NULL ";
			        	}
	
						$delSql = "delete 
									from 
									{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD 
									where 
									UserID = {$target_id} AND
			                        Year = '$targetYear' AND
									SubjectCode = '{$target_subjcode}'
									$conds
								";
							
							$hashed_value = md5($delSql);
							
							# Multiple teaching language checking
							# $hashed_value: unique value of each records, subjects and subject components
							// 1. All values are N.A. or N.T.
							// 2. Not yet deleted DB record => ensure first all "N.A." or "N.T" case
							// 3. Subject more than 1 teaching language
							 if($skipUpdateRecordFlag && !isset($processedSubjectCodeRecordFlag[$hashed_value]) && count($array_subject_code_language[$array_subject_code[$curr_subj_pos]]) > 1) {
							 	// Allow other languages of same subject to insert record
							 	$processedSubjectCodeRecordFlag[$hashed_value] = false;
							 }
							
							if (!$del_log[$hashed_value])
							{
								$del_log[md5($delSql)] = true;
	
								//delete the exist record 
								$li->db_db_query($delSql);
							} 
							elseif($processedSubjectCodeRecordFlag[$hashed_value] === false && count($array_subject_code_language[$array_subject_code[$curr_subj_pos]]) > 1 && !$skipUpdateRecordFlag){
								// do nothing: skip duplicate records checking until subject is processed
								// 1. Allow other languages of the same subject to insert record
								// 2. Subject more than 1 teaching languages
								// 3. All values are not N.A. or N.T.
							} 
							else {
								$skipUpdateRecordFlag = true;
							}
							
						if($skipUpdateRecordFlag == true){
							//do nothing
						}else{
							
							// Subject -> processed : Resume duplicate records checking 
							 $processedSubjectCodeRecordFlag[$hashed_value] = true;
							
							//insert record
							$TermAssessment_value = ($IsMultipleAssessment && $AssessmentHeader[$j]!="") ? "'".$AssessmentHeader[$j]."'" : "NULL";
			              $sql =  "
			                        INSERT INTO {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
			                          (UserID, WebSAMSRegNo, ClassName, YearClassID,
			    											ClassNumber, Year, AcademicYearID, Semester, YearTermID, TermAssessment,
			    											IsAnnual, SubjectCode, SubjectID, Score, Grade,
			                          OrderMeritForm, OrderMeritFormTotal, 
			    											SubjectComponentCode, SubjectComponentID, InputDate, ModifiedDate)
			                        VALUES
			                          ('$target_id', '$target_regno', '$target_class', '$thisYearClassID',
			                          '$target_classnum', '$targetYear', $academicYearID,'$targetSemester', $YearTermID, $TermAssessment_value,
			                          '$IsAnnual', '$target_subjcode', $target_subject_id, $t_score, '$t_grade',
			                          $t_rank_classlevel, $t_rank_cl_total,
			                          $string_sql_sbj_cmp_code, $target_subject_component_id, now(), now())
			                      ";
								// if (strstr($target_subjcode, "CHIN"))
								
								
	//debug($sql);
							if ($li->db_db_query($sql))
							{
						    	$count_updated ++; // always count as update
							} else
							{
								//debug($sql);
							}		

						    
					    $cal_sd_mean_arr[$thisYearClassID][$academicYearID][$YearTermID] = true;
					    						 
							}
						} else
						{
							if (!$detectedBefore[$subject_code_type])
							{
								// Display error message for subject and subject component
								$error_type = strpos($subject_code_type,SBJ_CMP_SEPARATOR)? ERROR_TYPE_NO_SUBJECTCOMP : ERROR_TYPE_NO_SUBJECT;
								$error_code = strpos($subject_code_type,SBJ_CMP_SEPARATOR)? $target_subjcode.'_'.$target_sbj_cmp_code : $target_subjcode;
								
								$error_data[] = array(-1, $error_type, $error_code);
								$detectedBefore[$target_subjcode] = true;
							} else
							{
								$OtherFailureDue2SubjectCode ++;
							}
						}
			          #############################################################################
			          $curr_subj_pos++;
			          $j += 3;
				} elseif ($AssessmentHeaderType[$j] == "OVERALL")
				{
					# overall result
					//debug($j);
					# Final score
					  $t_overall_score = Standardize_Import_Data($data[$i][$j]); //hdebug_r($t_overall_score); // it is Overall Average Score
					  $t_overall_score = (empty($t_overall_score) || strtolower($t_overall_score)==$NA_str || strtolower($t_overall_score)=='n.t.')? "-1" : $t_overall_score; 
					  $t_overall_score = $t_overall_score * 1;	// change 67.00 to 67
		
					
					  $t_overall_grade = Standardize_Import_Data($data[$i][$j+1], $forGrade=true);
					  $temp_rank_class = Standardize_Import_Data($data[$i][$j+2]);
					  $temp_rank_classlevel = Standardize_Import_Data($data[$i][$j+3]);
					  $temp_rank_stream = Standardize_Import_Data($data[$i][$j+4]);
					  $temp_rank_subjgrp = Standardize_Import_Data($data[$i][$j+5]);
				
					  list($t_overall_rank_class, $t_overall_rank_class_total) = explode("#", $temp_rank_class);
					  list($t_overall_rank_classlevel, $t_overall_rank_classlevel_total) = explode("#", $temp_rank_classlevel);
					  list($t_overall_rank_stream, $t_overall_rank_stream_total) = explode("#", $temp_rank_stream);
					  list($t_overall_rank_subjgrp, $t_overall_rank_subjgrp_total) = explode("#", $temp_rank_subjgrp);
					  
				    // Eric Yip (20100910): No rank input => null value in DB
				    $NA_str = 'n.a.';
				    
				    $t_overall_rank_class = empty($t_overall_rank_class) ? "NULL" : $t_overall_rank_class;
			        $t_overall_rank_class = strtolower($t_overall_rank_class)==$NA_str ? "NULL" : $t_overall_rank_class;
			        
			        $t_overall_rank_class_total = empty($t_overall_rank_class_total) ? "NULL" : $t_overall_rank_class_total;
			        $t_overall_rank_class_total = strtolower($t_overall_rank_class_total)==$NA_str ? "NULL" : $t_overall_rank_class_total;
			        
			        $t_overall_rank_classlevel = empty($t_overall_rank_classlevel) ? "NULL" : $t_overall_rank_classlevel;
			        $t_overall_rank_classlevel = strtolower($t_overall_rank_classlevel)==$NA_str ? "NULL" : $t_overall_rank_classlevel;
			        
			        $t_overall_rank_classlevel_total = empty($t_overall_rank_classlevel_total) ? "NULL" : $t_overall_rank_classlevel_total;
			        $t_overall_rank_classlevel_total = strtolower($t_overall_rank_classlevel_total)==$NA_str ? "NULL" : $t_overall_rank_classlevel_total;
			        
			        $t_overall_rank_stream = empty($t_overall_rank_stream) ? "NULL" : $t_overall_rank_stream;
			        $t_overall_rank_stream = strtolower($t_overall_rank_stream)==$NA_str ? "NULL" : $t_overall_rank_stream;
			        
			        $t_overall_rank_stream_total = empty($t_overall_rank_stream_total) ? "NULL" : $t_overall_rank_stream_total;
			        $t_overall_rank_stream_total = strtolower($t_overall_rank_stream_total)==$NA_str ? "NULL" : $t_overall_rank_stream_total;
			        
			        $t_overall_rank_subjgrp = empty($t_overall_rank_subjgrp) ? "NULL" : $t_overall_rank_subjgrp;
			        $t_overall_rank_subjgrp = strtolower($t_overall_rank_subjgrp)==$NA_str ? "NULL" : $t_overall_rank_subjgrp;
		        
			        $t_overall_rank_subjgrp_total = empty($t_overall_rank_subjgrp_total) ? "NULL" : $t_overall_rank_subjgrp_total;
			        $t_overall_rank_subjgrp_total = strtolower($t_overall_rank_subjgrp_total)==$NA_str ? "NULL" : $t_overall_rank_subjgrp_total;
				
					  #############################################################################
					  # Fill here
					  # Update overall data to DB
					  $conds = ($IsAnnual==0) ? " AND Semester = '$targetSemester'" : " AND IsAnnual = '$IsAnnual'";
					  
					  
			        	if ($IsMultipleAssessment && $AssessmentHeader[$j]!="")
			        	{
			        		$conds .= " AND TermAssessment='".$AssessmentHeader[$j]."' ";
			        	} else
			        	{
			        		$conds .= " AND TermAssessment IS NULL ";
			        	}
			        	
		        $sql =  "
		                  SELECT
		                    RecordID
		                  FROM
		                    {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD
		                  WHERE
		                    UserID = '$target_id' AND
		                    Year = '$targetYear'
		                    $conds
		                ";
		         
					  $temp = $li->returnVector($sql); 
					  if (sizeof($temp)==0)
					  {
							$TermAssessment_value = ($IsMultipleAssessment && $AssessmentHeader[$j]!="") ? "'".$AssessmentHeader[$j]."'" : "NULL";
		        		  $sql =  "INSERT INTO {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD
		                    (UserID, WebSAMSRegNo, AcademicYearID, Year, Semester, YearTermID, TermAssessment,
		                    IsAnnual, ClassName, YearClassID, ClassNumber, Score,
		                    Grade, OrderMeritClass, OrderMeritClassTotal, OrderMeritForm,
		                    OrderMeritFormTotal, OrderMeritStream, OrderMeritStreamTotal,
		                    OrderMeritSubjGroup, OrderMeritSubjGroupTotal, InputDate, ModifiedDate)
		                  VALUES
		                    ('$target_id', '$target_regno' , $academicYearID, '$targetYear', '$targetSemester', '$YearTermID', $TermAssessment_value,
		                    '$IsAnnual', '$target_class', '$thisYearClassID', '$target_classnum', '$t_overall_score',
		                    '$t_overall_grade', $t_overall_rank_class, $t_overall_rank_class_total, $t_overall_rank_classlevel, $t_overall_rank_classlevel_total, $t_overall_rank_stream, $t_overall_rank_stream_total, $t_overall_rank_subjgrp, $t_overall_rank_subjgrp_total, now(), now())
		                  ";
						  $li->db_db_query($sql);
 
					    $count_new ++;
					    $cal_sd_mean_arr[$thisYearClassID][$academicYearID][$YearTermID] = true;
					  }
					  else
					  {
						  $t_record_id = $temp[0];
		          $sql =  "UPDATE
		                    {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD
		                  SET
		                    Score = '$t_overall_score',
		                    Grade = '$t_overall_grade',
		                    OrderMeritClass = $t_overall_rank_class,
		                    OrderMeritClassTotal = $t_overall_rank_class_total,
		                    OrderMeritForm = $t_overall_rank_classlevel,
		                    OrderMeritFormTotal = $t_overall_rank_classlevel_total,
		                    OrderMeritStream = $t_overall_rank_stream,
		                    OrderMeritStreamTotal = $t_overall_rank_stream_total,
		                    OrderMeritSubjGroup = $t_overall_rank_subjgrp,
		                    OrderMeritSubjGroupTotal = $t_overall_rank_subjgrp_total,
		                    ModifiedDate = now()
		                  WHERE
		                    RecordID = '$t_record_id'";
		                 
		//error_log($sql."    e:[".mysql_error()."]<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
					      $li->db_db_query($sql);
		//	hdebug_r('Update');
		//	hdebug_r($sql);		      
					      $count_updated ++;
					  }
					  $j += 6;
					 // debug($sql);
				} else
				{
					$j ++ ;
				}
			  }
		
			  
		
			  #############################################################################
		
		
		}
		# Process Error Row
	}
	
	# calculate SD and Mean for each form 
	//$cal_sd_mean_arr[$thisYearClassID][$academicYearID][$YearTermID]
	if (is_array($cal_sd_mean_arr) && sizeof($cal_sd_mean_arr)>0)
	{
		foreach ($cal_sd_mean_arr AS $CurYearClassID => $AcademicObj)
		{
			# get year_id
			# ...
			$lib_year = new year_class($CurYearClassID);
			$CurYearID = $lib_year->YearID;
			if (is_array($AcademicObj) && sizeof($AcademicObj)>0)
			{
				foreach ($AcademicObj AS $CurAcademicYearID => $YearTermObj)
				{
					if (is_array($YearTermObj) && sizeof($YearTermObj)>0)
					{
						foreach ($YearTermObj AS $CurYearTermID => $value)
						{
							$lpf->ComputeSDMean($CurYearID, $CurAcademicYearID, $CurYearTermID);	
							//debug($CurYearID, $CurAcademicYearID, $CurYearTermID);
							//debug("inside");
						}
					}
				}
			}
			
		}
	}
}

if($correct_header){
	# Display import stats
	$display_content = "<table border='0' cellpadding='5' cellspacing='0'>";
//	$display_content .= "<tr><td class='tabletext'>".$ec_guide['import_add_no']." : </td><td class='tabletext'><b>".$count_new."</b></td></tr>\n";
	$display_content .= "<tr><td class='tabletext'>".$ec_guide['import_update_no']." : </td><td class='tabletext'><b>".$count_updated."</b></td></tr>\n";
	$display_content .= "<tr><td class='tabletext'>".$ec_guide['import_fail_no']." : </td><td class='tabletext'><b>".(sizeof($error_data)+$OtherFailureDue2SubjectCode)."</b></td></tr>\n";
	$display_content .= "</table>\n";
	
	if (sizeof($error_data)!=0)
	{
		$error_table = "<br><table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
	    $error_table .= "<tr class=tableTitle bgcolor='#CFE6FE'><td nowrap class='tabletext'>".$ec_guide['import_error_data']."</td><td width='45%' class='tabletext'>".$ec_guide['import_error_reason']."</td><td width='55%' class='tabletext'>".$ec_guide['import_error_detail']."</td></tr>\n";
	
		for ($i=0; $i<sizeof($error_data); $i++)
		{
			list ($t_row, $t_type, $t_data) = $error_data[$i];
			if ($t_row==-1)
			{
				$t_row = "Column(s)";
			} else
			{
				$t_row++;     # set first row to 1
			}
			$css_color = ($i%2==0) ? "#FFFFFF" : "#F3F3F3";
			$error_table .= "<tr bgcolor='$css_color'><td class='tabletext'>$t_row</td><td class='tabletext'>";
			$reason_string = $ec_guide['import_error_unknown'];
			switch ($t_type)
			{
				case ERROR_TYPE_NO_MATCH_REGNO:
						$reason_string = $ec_guide['import_error_no_user'];
						break;
				case ERROR_TYPE_EMPTY_REGNO:
						$reason_string = $ec_iPortfolio['activation_result_no_regno'];
						break;
				case ERROR_TYPE_INCORRECT_REGNO:
						$reason_string = $ec_guide['import_error_incorrect_regno'];
						break;
				case ERROR_TYPE_INCORRECT_CLASSNAME:
						$reason_string = $ec_guide['import_error_incorrect_class'];
						break;
				case ERROR_TYPE_NO_SUBJECT:
						$reason_string = $Lang['SysMgr']['Timetable']['Warning']['Import']['SubjectIsNotFound'];
						break;
				case ERROR_TYPE_NO_SUBJECTCOMP:
						$reason_string = $Lang['SysMgr']['Timetable']['Warning']['Import']['SubjectIsCompNotFound'];
						break;
				default:
						$reason_string = $ec_guide['import_error_unknown'];
						break;
			}
			$error_table .= $reason_string;
	
			$error_contents = (is_array($t_data)) ? implode(",",$t_data) : $t_data."&nbsp;";
			$error_table .= "</td><td class='tabletext'>".$error_contents."</td></tr>\n";
		}
		$error_table .= "</table>\n";
		$display_content .= $error_table;
	}
}
else
{	
	if($SubjectHeader){
		# Header incorrect
		$display_content = "<table border='0' cellpadding='5' cellspacing='0' width='90%'>";
		$display_content .= "<tr><td class='tabletext'>".$i_SAMS_import_error_wrong_format."</td></tr>";
		$display_content .= "</table>\n";
		
		$error_table = "<table border='0' cellpadding='5' cellspacing='0' width='90%'><tr>";
		$error_table .= "<td class='tabletext' width='40%'>".implode("<br />", $DefaultHeader)."<br />...</td>";
		$error_table .= "<td class='tabletext' align='center' width='10%'>v.s.</td>";
		
		$error_table .= "<td class='tabletext' width='40%'>";
		for($i=0; $i<$pos_first_subject; $i++)
		{
			$error_table .= $header_row[$i]."<br />";
		}
		$error_table .= "...</td>";
		
		$error_table .= "</tr></table>\n";
		
		$display_content .= "<br />".$error_table;
	}else{
		$display_content = "<table border='0' cellpadding='5' cellspacing='0' width='90%'>";
		$display_content .= "<tr><td class='tabletext'>".$i_SAMS_import_error_wrong_subject_format."</td></tr>";
		$display_content .= "</table>\n";
	}
	
	
}

?>

<FORM name="form1" method="POST">
	<br>
	<?= $display_content ?>
	<br>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	
	<p>
	<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$ec_guide['import_back']?>" onClick="self.location='import_sams.php'">
	<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$button_close?>" onClick="self.close()">
	</p>
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>