<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_sturec();
$li_pf->ACCESS_CONTROL("student_info");
$li_pf->SET_ALUMNI_COOKIE_BACKUP($ck_is_alumni);

# Reset Student ID if class is changed
if($FieldChanged == "year" || $FieldChanged == "classname")
{
	if($FieldChanged == "year")
		$ClassName = "";
	
	$ClassList = $li_pf->GET_ALUMNI_LIST_DATA($Year, true, "", $ClassName);
	$StudentID = $ClassList[0]['UserID'];
}

//$template_table_top_right = $li_pf->GEN_STUDENT_LIST_INFO($ClassName, $StudentID);

$linterface = new interface_html();
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];
### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

# Retrieve student info
$student_obj = $li_pf->GET_STUDENT_OBJECT($StudentID);
	
# Set links for photo
if(strpos($student_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $student_obj['PhotoLink'] != "")
	$student_obj['PhotoLink'] = str_replace("<!--ImageStyle-->", "", $student_obj['PhotoLink']);
else if($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
else
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";

# Alumni Year Selection
$AlumniYearSelection = $li_pf->GEN_ALUMNI_YEAR_SELECTION($Year);

# Generate class selection drop-down list
$ClassSelection = $li_pf->GEN_ALUMNI_CLASS_SELECTION($Year, $ClassName);

# Generate student selection drop-down list and get a student list
list($StudentSelection, $student_list) = $li_pf->GEN_STUDENT_LIST_INFO($ClassName, $StudentID, true, 1, "name='StudentID' onChange='document.form1.submit()'", $Year);

if(!isset($RecordType))
{
	# Choose default record type according to user right
	if($li_pf->HAS_RIGHT("merit"))
		$RecordType = "merit";
	else if($li_pf->HAS_RIGHT("assessment_report"))
		$RecordType = "assessment";
	else if($li_pf->HAS_RIGHT("activity"))
		$RecordType = "activity";
	else if($li_pf->HAS_RIGHT("award"))
		$RecordType = "award";
	else if($li_pf->HAS_RIGHT("teacher_comment"))
		$RecordType = "comment";
	else if($li_pf->HAS_RIGHT("attendance"))
		$RecordType = "attendance";
	else if($li_pf->HAS_RIGHT("service"))
		$RecordType = "service";
	else if($li_pf->HAS_RIGHT("ole"))
		$RecordType = "ole";
}
switch($RecordType)
{
	case "activity":
		if ($order=="") $order=1;
		if ($field=="") $field=0;
		$li = new libdbtable2007($field, $order, $pageNo);
		$li->field_array = array("a.Year", "a.Semester", "a.ActivityName", "a.Role", "a.Performance", "a.ModifiedDate");
		if($ChooseYear!="")
			$conds = "AND a.Year = '$ChooseYear'";
		
		$sql =	"
							SELECT
								a.Year,
								a.Semester,
								a.ActivityName,
								if(a.Role='', '--', a.Role),
								if(a.Performance='', '--', a.Performance),
								if(a.Organization='', '--', a.Organization)
							FROM {$eclass_db}.ACTIVITY_STUDENT as a
							WHERE
								a.UserID = '$StudentID'
								$conds
						";

		# TABLE INFO
		$li->sql = $sql;
		$li->db = $intranet_db;
		$li->title = $ec_iPortfolio['record'];
		$li->no_msg = $no_record_msg;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
		if ($page_size_change!="") $li->page_size = $numPerPage;
		$li->no_col = 7;
		//$li->noNumber = true;
		//list($year) = $li->db_db_query($sql);
		$sql2 =	"
							Select DISTINCT
								a.Year
							FROM
								{$eclass_db}.ACTIVITY_STUDENT as a
							WHERE
								a.UserID = '$StudentID'
							ORDER BY
								Year
						";
		$ActivityYearArr = $li_pf->returnArray($sql2);
		$pageSizeChangeEnabled = true;

		$li->table_tag = "<table bgcolor='#cccccc' border='0' cellpadding='10' cellspacing='0' width='100%'>";
		$li->row_alt = array("#FFFFFF", "F3F3F3");
		$li->row_height = 20;
		$li->sort_link_style = "class='tbheading'";

		# TABLE COLUMN
		$li->column_list .= "<td height='25' nowrap align='center'>".$li->column(0, "#", 1)."</td>\n";
		$li->column_list .= "<td width='15%'>".$li->column(1, $ec_iPortfolio['year'], 1)."</td>\n";
		$li->column_list .= "<td width='15%'>".$li->column(2, $ec_iPortfolio['term'], 1)."</td>\n";
		$li->column_list .= "<td width='20%'>".$li->column(3, $ec_iPortfolio['activity_name'], 1)."</td>\n";
		$li->column_list .= "<td width='15%'>".$li->column(4, $ec_iPortfolio['role'], 1)."</td>\n";
		$li->column_list .= "<td width='15%'>".$li->column(5, $ec_iPortfolio['performance'], 1)."</td>\n";
		$li->column_list .= "<td width='15%'>".$li->column(6, $i_ActivityOrganization, 1)."</td>\n";
		$li->column_array = array(0,0,0,0,0,0,0);
		
		break;
	case "award":		
		if ($order=="") $order=0;
		if ($field=="") $field=0;
		$li = new libdbtable2007($field, $order, $pageNo);
		if($sys_custom['iPortfolioHideRemark']==true)
		{
			$li->field_array = array("a.Year", "a.Semester", "a.AwardDate", "a.AwardName", "a.Organization", "a.SubjectArea", "a.AwardFile");
			$li->no_col = 7;
			$li->column_array = array(0,0,0,0,0,0);
			$NameWidth = "30%";
		}
		else
		{
			$li->field_array = array("a.Year", "a.Semester", "a.AwardDate", "a.AwardName", "a.Organization", "a.SubjectArea", "a.Details", "a.AwardFile");
			$li->no_col = 8;
			$li->column_array = array(0,0,0,0,0,0,0);
			$RemarkField = "if(a.Remark!='', a.Remark, '--'),";
			$NameWidth = "15%";
		}

		if($ChooseYear!="")
			$conds = "AND a.Year = '$ChooseYear'";

		$sql =	"
							SELECT
								a.Year,
								a.Semester,
								IF (a.AwardDate,DATE_FORMAT(a.AwardDate,'%Y-%m-%d'),'--') As AwardDate,
								CONCAT('<a class=navigation href=\"javascript:newWindow(\'award/award_detail.php?record_id=', a.RecordID,'\', 4)\">', a.AwardName, '</a>'),
								IF ((a.Organization != '' AND a.Organization IS NOT NULL), a.Organization, '--'),
								IF ((a.SubjectArea != '' AND a.SubjectArea IS NOT NULL), a.SubjectArea, '--'),
								{$RemarkField}
								IF ((a.AwardFile!='' AND a.AwardFile IS NOT NULL), CONCAT('<a href=\"award/attach.php?RecordID=', a.RecordID, '\" target=\"_blank\"><img src=\"$image_path/icon/attachment.gif\" border=0></a>'), '--')
							FROM
								{$eclass_db}.AWARD_STUDENT as a
							WHERE
								a.UserID = '$StudentID' AND
								a.RecordType = '1'
								$conds
						";

		# TABLE INFO
		$li->sql = $sql;
		$li->db = $intranet_db;
		$li->title = $ec_iPortfolio['award'];
		$li->no_msg = $no_record_msg;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
		if ($page_size_change!="") $li->page_size = $numPerPage;

		$sql2 =	"
							Select DISTINCT
								a.Year
							FROM
								{$eclass_db}.AWARD_STUDENT as a
							WHERE
								a.UserID = '$StudentID' AND
								a.RecordType = '1'
							ORDER BY
								Year
						";
		$ActivityYearArr = $li_pf->returnArray($sql2);
		$pageSizeChangeEnabled = true;

		$li->table_tag = "<table width='100%' bgcolor='#cccccc' border='0' cellpadding='10' cellspacing='0'>";
		$li->row_alt = array("#FFFFFF", "F3F3F3");
		$li->row_height = 20;
		$li->sort_link_style = "class='tbheading'";

		# TABLE COLUMN
		$pos = 0;
		//$li->column_list .= "<td class='tbheading' height='25' bgcolor='#CFE6FE' nowrap align='center'>#</span></td>\n";
		$li->column_list .= "<td height='25' nowrap align='center'>".$li->column($pos++, "#", 1)."</td>\n";
		$li->column_list .= "<td width='15%'>".$li->column($pos++,
		$ec_iPortfolio['year'], 1)."</td>\n";
		$li->column_list .= "<td width='15%'>".$li->column($pos++, $ec_iPortfolio['semester'], 1)."</td>\n";
		$li->column_list .= "<td width='15%'>".$li->column($pos++, $ec_iPortfolio['date'], 1)."</td>\n";
		$li->column_list .= "<td width='".$NameWidth."'>".$li->column($pos++, $ec_iPortfolio['award_name'], 1)."</td>\n";
		$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_AwardOrganization, 1)."</td>\n";
		$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_AwardSubjectArea, 1)."</td>\n";
		if($sys_custom['iPortfolioHideRemark']==false) {
			$li->column_list .= "<td width='15%'>".$li->column($pos++, $ec_iPortfolio['remark'], 1)."</td>\n";
		}
		//$li->column_list .= "<td width='10%'>".$li->column($pos++, $ec_iPortfolio['upload_cert'], 1)."</td>\n";

		break;
	case "comment":
		if ($order=="") $order=1;
		if ($field=="") $field=0;
		$li = new libdbtable2007($field, $order, $pageNo);
		$li->field_array = array("a.Year", "a.Semester", "ClassAndNumber", "a.ConductGradeChar", "a.CommentChi", "a.CommentEng", "a.ModifiedDate");
		$ClassNumberField = getClassNumberField("b.");

		if($ChooseYear!="")
			$conds = "AND a.Year = '$ChooseYear'";

		$sql =	"
							SELECT DISTINCT
								a.Year,
								if((a.Semester='' OR  a.Semester IS NULL), '".$ec_iPortfolio['overall_comment']."', a.Semester),
								if(b.ClassName IS NULL, '--', CONCAT(b.ClassName, ' - ', $ClassNumberField)) AS ClassAndNumber,
								a.ConductGradeChar,
								if((a.CommentChi IS NULL OR a.CommentChi=''), '--', a.CommentChi) as CommentChi,
								if((a.CommentEng IS NULL OR a.CommentEng=''), '--', a.CommentEng) as CommentEng,
								a.ModifiedDate
							FROM
								{$eclass_db}.CONDUCT_STUDENT as a
							LEFT JOIN
								{$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD as b
							ON
								a.UserID = b.UserID AND
								a.Year = b.Year
							WHERE
								a.UserID = '$StudentID'
								$conds
							";

		// TABLE INFO
		$li->sql = $sql;
		$li->db = $intranet_db;
		$li->title = $ec_iPortfolio['teacher_comment'];
		$li->no_msg = $no_record_msg;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
		if ($page_size_change!="") $li->page_size = $numPerPage;
		$li->no_col = 8;
		//$li->noNumber = true;
		
		$sql2 =	"
							Select DISTINCT
								a.Year
							FROM
								{$eclass_db}.CONDUCT_STUDENT as a
							WHERE
								a.UserID = '$StudentID'
							ORDER BY
								Year
						";
		$ActivityYearArr = $li_pf->returnArray($sql2);
		$pageSizeChangeEnabled = true;
		
		$li->table_tag = "<table width='100%' bgcolor='#cccccc' border='0' cellpadding='10' cellspacing='0'>";
		$li->row_alt = array("#FFFFFF", "F3F3F3");
		$li->row_height = 20;
		$li->sort_link_style = "class='tbheading'";

		# TABLE COLUMN
		//$li->column_list .= "<td class='tbheading' height='25' bgcolor='#CFE6FE' nowrap align='center'>#</span></td>\n";
		$li->column_list .= "<td height='25' nowrap align='center'>".$li->column(0, "#", 1)."</td>\n";
		$li->column_list .= "<td width='10%'>".$li->column(1, $ec_iPortfolio['year'], 1)."</td>\n";
		$li->column_list .= "<td width='10%'>".$li->column(2, $ec_iPortfolio['semester'], 1)."</td>\n";
		$li->column_list .= "<td width='10%'>".$li->column(3, $ec_iPortfolio['class_number'], 1)."</td>\n";
		$li->column_list .= "<td width='10%'>".$li->column(4, $ec_iPortfolio['conduct_grade'], 1)."</td>\n";
		$li->column_list .= "<td width='20%'>".$li->column(5, $ec_iPortfolio['comment_chi'], 1)."</td>\n";
		$li->column_list .= "<td width='20%'>".$li->column(6, $ec_iPortfolio['comment_eng'], 1)."</td>\n";
		$li->column_list .= "<td width='15%'>".$li->column(7, $ec_iPortfolio['SAMS_last_update'], 1)."</td>\n";
		$li->column_array = array(0,0,0,0,0,0,0,0);
		
		break;
	case "service":
		if ($order=="") $order=1;
		if ($field=="") $field=0;
		$li = new libdbtable2007($field, $order, $pageNo);
		$li->field_array = array("a.Year", "a.Semester", "a.ServiceDate", "a.ServiceName", "a.Role", "a.Performance", "a.ModifiedDate");
		if($ChooseYear!="")
			$conds = "AND a.Year = '$ChooseYear'";

		$sql =	"
							SELECT
								a.Year,
								a.Semester,
								if(a.ServiceDate='0000-00-00', '--', a.ServiceDate),
								a.ServiceName,
								if(a.Role='', '--', a.Role),
								if(a.Performance='', '--', a.Performance),
								a.ModifiedDate FROM {$eclass_db}.SERVICE_STUDENT as a
							WHERE
								a.UserID = '$StudentID'
								$conds
						";

		# TABLE INFO
		$li->sql = $sql;
		$li->db = $intranet_db;
		$li->title = $ec_iPortfolio['service'];
		$li->no_msg = $no_record_msg;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
		if ($page_size_change!="") $li->page_size = $numPerPage;
		$li->no_col = 8;
//$li->noNumber = true;

		$sql2 =	"
							Select DISTINCT
								a.Year
							FROM
								{$eclass_db}.SERVICE_STUDENT as a
							WHERE
								a.UserID = '$StudentID'
							ORDER BY
								Year
						";
		$ActivityYearArr = $li_pf->returnArray($sql2);
		$pageSizeChangeEnabled = true;

		$li->table_tag = "<table width='100%' border='0' cellpadding='10' cellspacing='0' class='table_b'>";
		$li->row_alt = array("#FFFFFF", "F3F3F3");
		$li->row_height = 20;
		$li->sort_link_style = "class='tbheading'";

		# TABLE COLUMN
		$li->column_list .= "<td height='25' nowrap align='center'>#</span></td>\n";
		$li->column_list .= "<td width='15%' nowrap='nowrap'>".$li->column(0, $ec_iPortfolio['year'], 1)."</td>\n";
		$li->column_list .= "<td width='15%' nowrap='nowrap'>".$li->column(1, $ec_iPortfolio['semester'], 1)."</td>\n";
		$li->column_list .= "<td width='15%' nowrap='nowrap'>".$li->column(2, $ec_iPortfolio['date'], 1)."</td>\n";
		$li->column_list .= "<td width='20%' nowrap='nowrap'>".$li->column(3, $ec_iPortfolio['service_name'], 1)."</td>\n";
		$li->column_list .= "<td width='10%' nowrap='nowrap'>".$li->column(4, $ec_iPortfolio['role'], 1)."</td>\n";
		$li->column_list .= "<td width='10%' nowrap='nowrap'>".$li->column(5, $ec_iPortfolio['performance'], 1)."</td>\n";
		$li->column_list .= "<td width='15%' nowrap='nowrap'>".$li->column(6, $ec_iPortfolio['SAMS_last_update'], 1)."</td>\n";
		$li->column_array = array(0,0,0,0,0,0,0,0);
		
		break;
	case "assessment":
		if($displayBy=="")
			$displayBy="Score";

		$link_score = ($displayBy=="Score") ? $ec_iPortfolio['display_by_score'] : "<a href=\"javascript:jCHANGE_DISPLAY_TYPE('Score')\" class='link_a'>".$ec_iPortfolio['display_by_score']."</a>";
		$link_grade = ($displayBy=="Grade") ? $ec_iPortfolio['display_by_grade'] : "<a href=\"javascript:jCHANGE_DISPLAY_TYPE('Grade')\" class='link_a'>".$ec_iPortfolio['display_by_grade']."</a>";
		$link_rank = ($displayBy=="Rank") ? $ec_iPortfolio['display_by_rank'] : "<a href=\"javascript:jCHANGE_DISPLAY_TYPE('Rank')\" class='link_a'>".$ec_iPortfolio['display_by_rank']."</a>";
		$link_stand_score = ($displayBy=="StandardScore") ? $ec_iPortfolio['display_by_stand_score'] : "<a href=\"javascript:jCHANGE_DISPLAY_TYPE('StandardScore')\" class='link_a'>".$ec_iPortfolio['display_by_stand_score']."</a>";

		break;
	case "ole":
		# Prepare data
		$pageSizeChangeEnabled = true;
		
		$lpf_slp = new libpf_slp();
		$ELEArray = $lpf_slp->GET_ELE();
		$ELECodeArray = array_keys($ELEArray);
		if (count($ELECodeArray)>0)
		{
			$ELEImplode = implode(",",$ELECodeArray);
		}
		$ELECount = count($ELECodeArray);
		
		$CategoryField = $lpf_slp->GET_OLR_Category_Field_For_Record("a.", true);
		$SqlCategoryField = ($CategoryField == "") ? "'-'," : "$CategoryField,";
		
		# Prepare table
		if ($order=="") $order=0;
		if ($field=="") $field=1;
		$li = new libdbtable2007($field, $order, $pageNo);
		
		if($ChooseYear!="")
		{
			$curChooseYear = substr($ChooseYear, 0, 4);
			$conds = "AND a.StartDate BETWEEN '$curChooseYear-09-01' AND '".($curChooseYear+1)."-08-31'";
		}
    $li->field_array = array("a.Title","OLEDate", "a.Role","a.Achievement","a.Category", "a.Hours");
  
		$sql = "SELECT '{$ELECount}', '{$ELEImplode}', c.Title, ";
		$sql .= "IF ((c.StartDate IS NULL OR c.StartDate='0000-00-00'),'--',IF(c.EndDate IS NULL OR c.EndDate='0000-00-00',c.StartDate,CONCAT(c.StartDate,'<br />$profiles_to<br />',c.EndDate))) as OLEDate, ";
		$sql .= "c.ELE, a.Role, a.Achievement, $SqlCategoryField a.Hours ";
		$sql .= "FROM {$eclass_db}.OLE_STUDENT as a ";
		//$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER as b ON a.ApprovedBy = b.UserID ";
		$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM as c ON a.ProgramID = c.ProgramID ";
    $sql .= "WHERE a.UserID = '$StudentID' AND a.RecordStatus IN (2,4) AND c.IntExt = 'INT' $conds";
      
		# TABLE COLUMN
		$li->column_list .= "<tr class='tabletop'>\n";
		
		$li->column_list .= "<td height='25' align='center' rowspan='2' >#</span></td>\n";
		$li->column_list .= "<td nowrap='nowrap' rowspan='2' width='100' >".$li->column(0,$ec_iPortfolio['title'], 1)."</td>\n";
		$li->column_list .= "<td rowspan='2' >".$li->column(1,$ec_iPortfolio['date']."/".$ec_iPortfolio['period'], 1)."</td>\n";
		$li->column_list .= "<td colspan=\"".$ELECount."\" align=\"center\" nowrap='nowrap'>".$ec_iPortfolio['ele']."</td>\n";
		$li->column_list .= "<td rowspan='2' align='center'>".$li->column(2,$ec_iPortfolio['ole_role'], 1)."</td>\n";
		$li->column_list .= "<td rowspan='2' align='center'>".$li->column(3,$ec_iPortfolio['achievement'], 1)."</td>\n";
		$li->column_list .= "<td rowspan='2' align='center' nowrap='nowrap'>".$li->column(4,$ec_iPortfolio['category'], 1)."</td>\n";
		$li->column_list .= "<td rowspan='2' align='center' nowrap='nowrap'>".$li->column(5,$ec_iPortfolio['hours'], 1)."</td>\n";
		$li->column_list .= "</tr>\n";


		# TABLE INFO
		$li->sql = $sql;
		$li->db = $intranet_db;
		$li->no_msg = $no_record_msg;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
		if ($page_size_change!="") $li->page_size = $numPerPage;
		$li->no_col = 10;
//$li->noNumber = true;

		$li->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
		$li->row_alt = array("#FFFFFF", "F3F3F3");
		$li->row_height = 20;
		$li->sort_link_style = "class='tbheading'";
		$li->IsColOff = "iPortfolioOLEPool";
		$li->additionalCols = count($ELECodeArray);
		
		if (count($ELECodeArray) > 0)
		{
			$li->column_list .= "<tr class=\"tabletop\">";
			foreach($ELEArray as $ELECode => $ELETitle)
			{
				$ELEDisplay = str_replace(array("[", "]"), "", $ELECode);
		    $ELEDisplay = is_numeric($ELEDisplay) ? $ELETitle : $ELEDisplay;
				$li->column_list .= "<td align='center' class='tabletopnolink'><b><span title='".$ELETitle."'>".$ELEDisplay."</span></b></td>";
			}
		
			$li->column_list .= "</tr>";				
		}
		
		$li->column_array = array(10,10,0,10,0,0,10,0);

		break;

	case "plkp":
		# Prepare data
		$pageSizeChangeEnabled = true;
		
		$lpf_slp = new libpf_slp();
		
		$CategoryField = $lpf_slp->GET_OLR_Category_Field_For_Record("a.", true);
		$SqlCategoryField = ($CategoryField == "") ? "'-'" : "$CategoryField";
	
		# Prepare table
		if ($order=="") $order=0;
		if ($field=="") $field=1;
		$li = new libdbtable2007($field, $order, $pageNo);
		$li->field_array = array("a.Title", "OLEDate", "a.Role","a.Achievement","a.Category");
		
		if($ChooseYear!="")
		{
			$curChooseYear = substr($ChooseYear, 0, 4);
			$conds = "AND a.StartDate BETWEEN '$curChooseYear-09-01' AND '".($curChooseYear+1)."-08-31'";
		}

		$sql =	"
							SELECT
								'{$ELECount}',
								'{$ELEImplode}',
								c.Title,
								IF ((c.StartDate IS NULL OR c.StartDate='0000-00-00'),'--',IF(c.EndDate IS NULL OR c.EndDate='0000-00-00',c.StartDate,CONCAT(c.StartDate,'<br />$profiles_to<br />',c.EndDate))) as OLEDate,
								c.ELE,
								a.Role,
								a.Achievement,
								$SqlCategoryField
							FROM
								{$eclass_db}.OLE_STUDENT as a
							LEFT JOIN {$intranet_db}.INTRANET_USER as b
                ON a.ApprovedBy = b.UserID
              LEFT JOIN {$eclass_db}.OLE_PROGRAM as c
                ON a.ProgramID = c.ProgramID
							WHERE
								a.UserID = '$StudentID' AND
								a.RecordStatus IN (2,4) AND
                c.IntExt = 'EXT'
								$conds
						";

		# TABLE INFO
		$li->sql = $sql;
		$li->db = $intranet_db;
		$li->no_msg = $no_record_msg;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
		if ($page_size_change!="") $li->page_size = $numPerPage;
		$li->no_col = 9;
//$li->noNumber = true;

		$li->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
		$li->row_alt = array("#FFFFFF", "F3F3F3");
		$li->row_height = 20;
		$li->sort_link_style = "class='tbheading'";
		$li->IsColOff = "iPortfolioOLEPool";

		# TABLE COLUMN
		$li->column_list .= "<tr class='tabletop'>\n";
		
		$li->column_list .= "<td height='25' align='center' >#</span></td>\n";
		$li->column_list .= "<td nowrap='nowrap' width='100' >".$li->column(0,$ec_iPortfolio['title'], 1)."</td>\n";
		$li->column_list .= "<td >".$li->column(1,$ec_iPortfolio['date']."/".$ec_iPortfolio['period'], 1)."</td>\n";
		$li->column_list .= "<td align='center'>".$li->column(2,$ec_iPortfolio['ole_role'], 1)."</td>\n";
		$li->column_list .= "<td align='center'>".$li->column(3,$ec_iPortfolio['achievement'], 1)."</td>\n";
		$li->column_list .= "<td align='center' nowrap='nowrap'>".$li->column(4,$ec_iPortfolio['category'], 1)."</td>\n";
		$li->column_list .= "</tr>\n";
		
		$li->column_array = array(10,10,0,10,0,0,10,0);

		break;
	default:
		break;
}


if($RecordType == "attendance")
	$SelectBoxContent = $li_pf->displayYearSelectBox($StudentID, $ClassName,$ChooseYear, 1);
else if($RecordType == "merit")
	$SelectBoxContent = $li_pf->displayYearSelectBox($StudentID, $ClassName,$ChooseYear, 0);
else
	$SelectBoxContent = $li_pf->displayYearSelectBox($StudentID, $ClassName,$ChooseYear);

$linterface->LAYOUT_START();

?>


<? // ===================================== Body Contents ============================= ?>

<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax.js"></script>

<script language="JavaScript">

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	if(jParField == 'classname' && document.form1.ClassName.value == '')
		return;

	if(jParField == 'year' && document.form1.Year.value == '')
		return;
	
	document.form1.FieldChanged.value = jParField;
	document.form1.action = "school_record_teacher_alumni.php";
	document.form1.submit();
}

// Quick student switch 
function jCHANGE_STUDENT(jParShift, jParToExtreme)
{
	var StudentSelect = document.getElementsByName('StudentID');
	var OriginalIndex = StudentSelect[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(!jParToExtreme)
	{
		if(TargetIndex >= StudentSelect[0].length)
		{
			StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			return;
		}
		else if(TargetIndex < 0)
		{
			StudentSelect[0].selectedIndex = 0;
			return;
		}
		else
			StudentSelect[0].selectedIndex = TargetIndex;
	}
	else
	{
		if(jParShift < 0)
		{
			if(OriginalIndex > 0)
				StudentSelect[0].selectedIndex = 0;
			else
				return;
		}
		else
		{
			if(OriginalIndex < StudentSelect[0].length-1)
				StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			else
				return;
		}
	}
	
	document.form1.action = "school_record_teacher_alumni.php";
	document.form1.submit();
}

// Search student name
function jSUBMIT_SEARCH(){
	document.form1.FieldChanged.value = "search";
	document.form1.action = "../school_records_alumni_year.php";
	document.form1.submit();
}

function jCHANGE_RECORD_TYPE(jRecType)
{
	document.form1.RecordType.value = jRecType;
	if(typeof(document.form1.ChooseYear) != "undefined")
		document.form1.ChooseYear.value = "";
	document.form1.action = "school_record_teacher_alumni.php";
	document.form1.submit();
}

function jCHANGE_DISPLAY_TYPE(jDisType)
{
	document.form1.displayBy.value = jDisType;
	document.form1.action = "school_record_teacher_alumni.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_LP()
{
	document.form1.action = "learning_portfolio_teacher_alumni.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_SI()
{
	document.form1.action = "student_info_teacher_alumni.php";
	document.form1.submit();
}

function jVIEW_DETAIL(jParRecType, jParParam)
{
	switch(jParRecType)
	{
		case 'merit':
			document.form1.miscParameter.value = jParParam;
			document.form1.action = "school_record_teacher_alumni_merit.php";
			document.form1.submit();
			break;
		case 'attendance':
			document.form1.miscParameter.value = jParParam;
			document.form1.action = "school_record_teacher_alumni_attendance.php";
			document.form1.submit();
			break;
		case 'assessment_report':
			document.form1.miscParameter.value = jParParam;
			document.form1.action = "school_record_teacher_alumni_stat.php";
			document.form1.submit();
			break;
	}
}

</script>

<FORM name="form1" method="POST" action="school_record_teacher_alumni.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan=2>
				<table border="0" cellspacing="0" cellpadding="3" width="100%">
					<tr>
						<td>
							<?=$AlumniYearSelection?> <?=$ClassSelection?>
						</td>
						<td align="right">
							<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$ec_iPortfolio['enter_student_name']:intranet_htmlspecialchars(stripslashes($search_name)))?>" onFocus="if(this.value=='<?=$ec_iPortfolio['enter_student_name']?>'){this.value=''}" onBlur="if(this.value==''){this.value='<?=$ec_iPortfolio['enter_student_name']?>'}" />
							<input type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onClick="jSUBMIT_SEARCH()" />
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td class="navigation">
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../school_records_alumni.php"><?=$ec_iPortfolio['year_list']?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../school_records_alumni_year.php?Year=<?=$Year?>"><?=$ClassName==""?$i_general_WholeSchool:$ClassName?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><?=($intranet_session_language=="b5"?$student_obj['ChineseName']:$student_obj['EnglishName'])?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="17" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_01_tea.gif" width="17" height="37"></td>
						<td height="37" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_03_tea.gif">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="200">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center" valign="middle" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_02_tea.gif"  class="page_title"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0" align="absmiddle"> <?=$ec_iPortfolio['school_record']?></td>
											</tr>
										</table>
									</td>
									<td align="left" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#E8E8E8">
											<tr>
												<td align="left">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr align="center" valign="middle">
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(-1, true)" class="tablebottomlink">&lt;&lt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(-1, false)" class="tablebottomlink">&lt;</a></td>
															<td nowrap>
																<span class="tabletext">
																	<?=$StudentSelection?>
																</span>
															</td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(1, false)" class="tablebottomlink">&gt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(1, true)" class="tablebottomlink">&gt;&gt;</a></td>
															<td width="15" nowrap> | </td>
															<td nowrap class="tabletext"><?=str_replace("<!--NoRecord-->", count($student_list), $ec_iPortfolio['total_record'])?></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_04_tea.gif" width="13" height="37"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif" width="17" height="20"></td>
						<td>
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<tr>
									<td valign="top">
										<table border="0" cellspacing="0" cellpadding="0" >
											<tr>
												<td>
													<table border="0" cellspacing="0" cellpadding="0" >
														<tr>
															<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_01.gif" style="padding-left:10px; padding-top:10px;"><?=$student_obj['PhotoLink']?></td>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="10" height="10"></td>
														</tr>
														<tr>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
															<td height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_04.gif" width="10" height="10"></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" class="tabletext"><?=$intranet_session_language=="b5"?$student_obj['ChineseName']:$student_obj['EnglishName']?></td>
											</tr>
											<tr>
												<td align="center">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr>
															<td valign="top"><a href="javascript:jTO_SI()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_stu_info.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0"></a></td>
<?php if($li_pf->HAS_SCHOOL_RECORD_VIEW_RIGHT()) { ?>
															<td valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record_on.gif" alt="<?=$ec_iPortfolio['school_record']?>" width="20" height="20" border="0"></td>
<?php } ?>
															<td valign="top">
																<a href="javascript:jTO_LP()">
																	<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_learning_portfolio.gif" alt="<?=$iPort['menu']['learning_portfolio']?>" width="20" height="20" border="0"><br>
																	<?=$li_pf->GET_LP_NOT_VIEW_COUNT_BY_USER_ID($StudentID) > 0 ? "<span class='new_alert'>New</span>" : ""?>
																</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
									<td>
										<table width="100%"  border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td>
													<table border="0" cellspacing="5" cellpadding="0"  width="100%">
														<tr>
															<td class="tab_underline" nowrap>
																<div class="shadetabs">
																	<ul>
																		<?=$li_pf->HAS_RIGHT("merit") ? ($RecordType=="merit" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_merit']."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('merit')\">".$ec_iPortfolio['title_merit']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("assessment_report") ? ($RecordType=="assessment" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_academic_report']."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('assessment')\">".$ec_iPortfolio['title_academic_report']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("activity") ? ($RecordType=="activity" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_activity']."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('activity')\">".$ec_iPortfolio['title_activity']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("award") ? ($RecordType=="award" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_award']."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('award')\">".$ec_iPortfolio['title_award']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("teacher_comment") ? ($RecordType=="comment" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_teacher_comments']."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('comment')\">".$ec_iPortfolio['title_teacher_comments']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("attendance") ? ($RecordType=="attendance" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_attendance']."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('attendance')\">".$ec_iPortfolio['title_attendance']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("service") ? ($RecordType=="service" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['service']."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('service')\">".$ec_iPortfolio['service']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("ole") ? ($RecordType=="ole" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['ole']."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('ole')\">".$ec_iPortfolio['ole']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("ole") ? ($RecordType=="plkp" ? "<li class='selected'><a href='#'><strong>".$iPort["external_ole_report"]["performance"]."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('plkp')\">".$iPort["external_ole_report"]["performance"]."</a></li>") : ""?>
																	</ul>
																</div>
															</td>
														</tr>
													</table>
													<table border="0" cellpadding="0" cellspacing="6" width="100%">
														<tr>
															<?php
																if($RecordType != "assessment")
																	echo "<td>".$SelectBoxContent."</td>";
																else{
																	echo "<td align=\"right\" valign=\"middle\" class=\"thumb_list\">";
																	switch($displayBy)
																	{
																		case "Grade":
																			echo $link_score."  | <span>".$link_grade."</span> | ".$link_rank." | ".$link_stand_score;
																			break;
																		case "Rank":
																			echo $link_score."  | ".$link_grade." | <span>".$link_rank."</span> | ".$link_stand_score;
																			break;
																		case "StandardScore":
																			echo $link_score."  | ".$link_grade." | ".$link_rank." | <span>".$link_stand_score."</span>";
																			break;
																		default:
																			echo "<span>".$link_score."</span>  | ".$link_grade." | ".$link_rank." | ".$link_stand_score;
																			break;
																	}
																	echo "</td>";
																}
															?>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center">
													<?php
														switch($RecordType)
														{
															case "activity":
															case "award":
															case "comment":
															case "service":
																$li->displayPlain();
																break;
															case "ole":
															case "plkp":
																$li->display();
																break;
															case "attendance":
																echo $li_pf->displayStudentAttendanceSummary($StudentID, $ClassName,$ChooseYear, true);
																break;
															case "assessment":
																echo $li_pf->generateAssessmentReport3($StudentID, $ClassName, $displayBy, "", "", "", "", "", true);
																break;
															case "merit":
															default:
																echo $li_pf->displayStudentMeritSummary($StudentID, $ClassName,$ChooseYear, true);
																break;
														}
													?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif" width="13" height="37"></td>
					</tr>
					<tr>
						<td width="17" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_08.gif" width="17" height="17"></td>
						<td height="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif" width="64" height="17"></td>
						<td width="13" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_10.gif" width="13" height="17"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>" />
	<input type="hidden" name="FieldChanged" />
	<input type="hidden" name="RecordType" value=<?=$RecordType?> />
	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
	<input type="hidden" name="displayBy" value="<?=$displayBy?>" />
	<input type="hidden" name="miscParameter" />
</FORM>

<? // ===================================== Body Contents (END) ============================= ?>

<?php
$li_pf->RELOAD_ALUMNI_COOKIE_BACKUP($ck_is_alumni);
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
