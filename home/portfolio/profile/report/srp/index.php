<?
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");


intranet_auth();
intranet_opendb();

$libpf = new libpf_report();

$year_class = new year_class($YearClassID,true,false,true);
if(!$StudentID)
	$StudentIDArr = Get_Array_By_Key($year_class->ClassStudentList, "UserID");
else
	$StudentIDArr = (array)$StudentID;

$academicYearID = array_remove_empty($academicYearID);
	
$StudentParticulars = $libpf->GET_STUDENT_PARTICULARS_SLP($StudentIDArr); 

# Get Student Class Name Of Past Academic Year
$StudentYearClassArr = $libpf->Get_Student_Year_Class_Mapping($academicYearID,$StudentIDArr);
$ClassNameLang = Get_Lang_Selection("ClassTitleB5","ClassTitleEN");
$StudentYearClassArr = BuildMultiKeyAssoc($StudentYearClassArr, array("UserID","AcademicYearID"),$ClassNameLang,1);

# Get AcademicYearNAme 
$ay = new academic_year();
$AYArr = $ay->Get_All_Year_List();
$AYNameAssoc = BuildMultiKeyAssoc($AYArr,'AcademicYearID','AcademicYearName',1);
unset($ay);
unset($AYArr);

# Get Reading Record
$ReadingGardenLib = new libreadinggarden();
$ReadingRecordArr = $ReadingGardenLib->Get_Student_Reading_Record_And_Report_Grade($StudentIDArr, '', '', '', '', '', '', $academicYearID);
$ReadingRecordAssoc = BuildMultiKeyAssoc($ReadingRecordArr, array("UserID","AcademicYearID"),'',0,1);
unset($ReadingRecordArr);

# Get Category
$CategoryArr = $ReadingGardenLib->Get_Category_List();
$CategoryAssoc = BuildMultiKeyAssoc($CategoryArr,"CategoryID", "CategoryName",1 );
unset($CategoryArr);

foreach($StudentIDArr as $k => $thisStudentID)
{
	if($k!=count($StudentIDArr)-1)
		$page_break_after = "page-break-after:always;";
	else
		$page_break_after = '';
		
	
	$x .= '<table cellpadding=5 cellspacing=0 width="100%" style="'.$page_break_after.'">'."\n";
		$x .= '<tr><td class="report_header" align="center">'.$Lang['iPortfolio']['SRP']['StudentReadingProfile'].'</td></tr>';
		$x .= '<tr><td align="right">'.$Lang['iPortfolio']['SRP']['DateOfIzzue'].': '.$issuedate.'</td></tr>';
		$x.= '<tr><td valign="top">'."\n";
		$x.= '<table width="100%" cellspacing="0" cellpadding="0" bordercolor="#000000" border="2">'."\n";
			$x.= '<tbody>'."\n";
				$x.= '<tr style="background: none repeat scroll 0% 0% rgb(128, 128, 128);"><td class="module_title">'.$Lang['iPortfolio']['SRP']['StudentParticulars'].'</td></tr>'."\n";
				$x.= '<tr><td>'."\n";
				
					if(!$StudentParticulars[$thisStudentID])
					{
						$x.= '<table width="100%" cellspacing="0" cellpadding="0" border="0">'."\n";
							$x.= '<tbody><tr>'."\n";
								$x.= '<td valign="middle" height="100" align="center">--</td>'."\n";
							$x.= '</tr>'."\n";
						$x.= '</tbody></table>'."\n";
					}
					else
					{
						$x.= '<table width="100%" cellspacing="0" cellpadding="0" border="0" class="padding_all">'."\n";
							$x.= '<tbody><tr>'."\n";
								$x.= '<td width="50%" valign="top">'."\n";
										$x.= '<table cellspacing="0" cellpadding="3" border="0">'."\n";
											$x.= '<tbody>'."\n";
												# Student Name 
												$StudentName = Get_Lang_Selection("EnglishName","ChineseName");
												$x.= '<tr>'."\n";
													$x.= '<td nowrap="" width="30%" valign="top">'.$Lang['iPortfolio']['SRP']['StudentName'].':</td>'."\n";
													$x.= '<td width="60%">'.$StudentParticulars[$thisStudentID][$StudentName].'</td>'."\n";
												$x.= '</tr>'."\n";
												# DateOfBirth 
												$x.= '<tr>'."\n";
													$x.= '<td nowrap="" width="30%" valign="top">'.$Lang['iPortfolio']['SRP']['DateOfBirth'].':</td>'."\n";
													$x.= '<td width="60%">'.$StudentParticulars[$thisStudentID]['DateOfBirth'].'</td>'."\n";
												$x.= '</tr>'."\n";
												# School Name 
												$x.= '<tr>'."\n";
													$x.= '<td nowrap="" width="30%" valign="top">'.$Lang['iPortfolio']['SRP']['SchoolName'].':</td>'."\n";
													$x.= '<td width="60%">'.$StudentParticulars[$thisStudentID]['SchoolName'].'</td>'."\n";
												$x.= '</tr>'."\n";			
												# Admission Date 
												$x.= '<tr>'."\n";
													$x.= '<td nowrap="" width="30%" valign="top">'.$Lang['iPortfolio']['SRP']['AdmissionDate'].':</td>'."\n";
													$x.= '<td width="60%">'.$StudentParticulars[$thisStudentID]['AdmissionDate'].'</td>'."\n";
												$x.= '</tr>'."\n";			
												# School Address 
												$x.= '<tr>'."\n";
													$x.= '<td nowrap="" width="30%" valign="top">'.$Lang['iPortfolio']['SRP']['SchoolAddress'].':</td>'."\n";
													$x.= '<td width="60%">'.$StudentParticulars[$thisStudentID]['SchoolAddress'].'</td>'."\n";
												$x.= '</tr>'."\n";			
												# School Phone
												$x.= '<tr>'."\n";
													$x.= '<td nowrap="" width="30%" valign="top">'.$Lang['iPortfolio']['SRP']['SchoolPhone'].':</td>'."\n";
													$x.= '<td width="60%">'.$StudentParticulars[$thisStudentID]['SchoolPhone'].'</td>'."\n";
												$x.= '</tr>'."\n";			
											$x.= '</tbody>'."\n";
										$x.= '</table>'."\n";
									$x.= '</td>'."\n";
									
									$x.= '<td width="50%" valign="top">'."\n";
											$x.= '<table cellspacing="0" cellpadding="3" border="0">'."\n";
												$x.= '<tbody>'."\n";
													# HKID 
													$x.= '<tr>'."\n";
														$x.= '<td nowrap="" width="30%" valign="top">'.$Lang['iPortfolio']['SRP']['HKID'].':</td>'."\n";
														$x.= '<td width="60%">'.$StudentParticulars[$thisStudentID]['HKID'].'</td>'."\n";
													$x.= '</tr>'."\n";
													# Gender 
													$x.= '<tr>'."\n";
														$x.= '<td nowrap="" width="30%" valign="top">'.$Lang['iPortfolio']['SRP']['Gender'].':</td>'."\n";
														$x.= '<td width="60%">'.$StudentParticulars[$thisStudentID]['Gender'].'</td>'."\n";
													$x.= '</tr>'."\n";
													# SchoolCode 
													$x.= '<tr>'."\n";
														$x.= '<td nowrap="" width="30%" valign="top">'.$Lang['iPortfolio']['SRP']['SchoolCode'].':</td>'."\n";
														$x.= '<td width="60%">'.$StudentParticulars[$thisStudentID]['SchoolCode'].'</td>'."\n";
													$x.= '</tr>'."\n";			
			
												$x.= '</tbody>'."\n";
											$x.= '</table>'."\n";
										$x.= '</td>'."\n";
												
							$x.= '</tr>'."\n";
						$x.= '</tbody></table>'."\n";
					}	
				$x.= '</td></tr>'."\n";
			$x.= '</tbody>'."\n";
		$x.= '</table>'."\n";
		$x.= '<br><br>'."\n";
		$x.= '</td></tr>'."\n";
		
		# Reading REcord 
		for($i=1; $i<=count($academicYearID);$i++)
		{
			$thisAY = $academicYearID[$i];
			
			if($StudentYearClassArr[$thisStudentID][$thisAY]) // no class record in that academic year
			{
				$x.= '<tr><td valign="top">'."\n";
					
					$x.= '<table width="100%" cellspacing="0" cellpadding="0" bordercolor="#000000" border="2">'."\n";
						$x.= '<tbody>'."\n";
							$x.= '<tr style="background: none repeat scroll 0% 0% rgb(128, 128, 128);"><td class="module_title">'.$AYNameAssoc[$thisAY]." (".$StudentYearClassArr[$thisStudentID][$thisAY].')</td></tr>'."\n";
							$x.= '<tr><td>'."\n";
								if(!$ReadingRecordAssoc[$thisStudentID][$thisAY]) // no reading record
								{
									$x.= '<table width="100%" cellspacing="0" cellpadding="0" border="0">'."\n";
										$x.= '<tbody>'."\n";
											$x.= '<tr>'."\n";
												$x.= '<td valign="middle" height="100" align="center">--</td>'."\n";
											$x.= '</tr>'."\n";
										$x.= '</tbody>'."\n";
									$x.= '</table>'."\n";
								}
								else
								{
									$x .= '<table width="100%" cellspacing="0" cellpadding="5" border="1" align="center" class="table_print">'."\n";
										$x .= '<tr>'."\n";
											$x .= '<td class="ole_head_sub" rowspan="2">'.$Lang['iPortfolio']['SRP']['Item'].'</td>'."\n";
											$x .= '<td class="ole_head_sub" rowspan="2">'.$Lang['iPortfolio']['SRP']['Title'].'</td>'."\n";
											$x .= '<td class="ole_head_sub" rowspan="2">'.$Lang['iPortfolio']['SRP']['Language'].'</td>'."\n";
											$x .= '<td class="ole_head_sub" rowspan="2">'.$Lang['iPortfolio']['SRP']['Author'].'</td>'."\n";
											$x .= '<td class="ole_head_sub" rowspan="2">'.$Lang['iPortfolio']['SRP']['Publisher'].'</td>'."\n";
											$x .= '<td class="ole_head_sub" rowspan="2">'.$Lang['iPortfolio']['SRP']['Categories'].'</td>'."\n";
											$x .= '<td class="ole_head_sub" colspan="2">'.$Lang['iPortfolio']['SRP']['PeriodOfReading'].'</td>'."\n";
											$x .= '<td class="ole_head_sub" rowspan="2">'.$Lang['iPortfolio']['SRP']['ReadingHours'].'</td>'."\n";
											$x .= '<td class="ole_head_sub" rowspan="2">'.$Lang['iPortfolio']['SRP']['BookReport'].$Lang['iPortfolio']['SRP']['YN'].'</td>'."\n";
										$x .= '</tr>'."\n";
										$x .= '<tr>'."\n";
											$x .= '<td class="ole_head_sub">'.$Lang['iPortfolio']['SRP']['StartingDate'].'</td>'."\n";
											$x .= '<td class="ole_head_sub">'.$Lang['iPortfolio']['SRP']['FinishingDate'].'</td>'."\n";
										$x .= '</tr>'."\n";
										
									foreach($ReadingRecordAssoc[$thisStudentID][$thisAY] as $no => $thisReadingRecord)
									{
										$thisCat = $thisReadingRecord['CategorID']?$CategoryAssoc[$thisReadingRecord['CategorID']]:$Lang['iPortfolio']['SRP']['Uncategorized'];
										$x .= '<tr>'."\n";
											$x .= '<td class="ole_content">'.($no+1).'</td>'."\n";
											$x .= '<td class="ole_content">'.$thisReadingRecord['BookName'].'</td>'."\n";
											$x .= '<td class="ole_content">'.$Lang['iPortfolio']['SRP']['LanguageType'][$thisReadingRecord['Language']].'</td>'."\n";
											$x .= '<td class="ole_content">'.$thisReadingRecord['Author'].'</td>'."\n";
											$x .= '<td class="ole_content">'.$thisReadingRecord['Publisher'].'</td>'."\n";
											$x .= '<td class="ole_content">'.$thisCat.'</td>'."\n";
											$x .= '<td class="ole_content">'.$thisReadingRecord['StartDate'].'</td>'."\n";
											$x .= '<td class="ole_content">'.$thisReadingRecord['FinishedDate'].'</td>'."\n";
											$x .= '<td class="ole_content">'.$thisReadingRecord['NumberOfHours'].'</td>'."\n";
											$x .= '<td class="ole_content">'.($thisReadingRecord['BookReportID']?"Y":"N").'</td>'."\n";
										$x .= '</tr>'."\n";	
									}
									$x.= '</table>'."\n";
								}
								
							$x.= '</td></tr>'."\n";
						$x.= '</tbody>'."\n";
					$x.= '</table>'."\n";
					
				$x.= '</td></tr>'."\n";
			}
		}
		
	$x .= '</table>'."\n";
}

$output = "<html>\n";
$output .= "<head>\n";
$output .= returnHtmlMETA()."\n";
$output .= "<link href=\"http://{$eclass_httppath}/src/includes/css/style_portfolio_report.css\" rel=\"stylesheet\" type=\"text/css\">\n";
$output .= "<link href=\"http://{$eclass_httppath}/src/includes/css/style_portfolio_table.css\" rel=\"stylesheet\" type=\"text/css\">\n";
$output .= "</head>\n";
$output .= "<body>\n";
$output .= $x."\n";
$output .= "</body>\n";
$output .= "</html>\n";

echo $output;
?>
<style>
.report_header
{
	font-family: Arial;
	font-size: 13pt;
	font-weight: bold;
	padding: 3px;
}

.module_title {
	color: #FFFFFF;
	font-family: Arial;
	font-size: 12pt;
	font-weight: bold;
	padding: 3px;
}

.padding_all {
	padding: 3px;
}

.ole_head_sub
{
	font-weight: bold;
}
</style>
<?

intranet_closedb();
exit();
?>
