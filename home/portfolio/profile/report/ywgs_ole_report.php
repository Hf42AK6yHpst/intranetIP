<?php

$PATH_WRT_ROOT = ($PATH_WRT_ROOT=="") ? "../../../../" : $PATH_WRT_ROOT;
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$li_pf = new libportfolio();


$libuser = new libuser($UserID);

if (!$libuser->isTeacherStaff())
{
	$StudentID = $UserID;
	$TargetClass = $libuser->ClassName;
	$hasSA = 1;
} else
{
	//$Year
	$sql = "SELECT AcademicYearID FROM {$intranet_db}.ACADEMIC_YEAR WHERE YearNameEN='{$Year}' ";
	$rows = $li->returnVector($sql);
	$r_academicYearID = $rows[0];
}
$AcademicYear = new academic_year($r_academicYearID);
$AcademicYearStart = $AcademicYear->AcademicYearStart;
$AcademicYearEnd = $AcademicYear->AcademicYearEnd;
$Year = $AcademicYear->YearNameEN;		# for student preview case



# Check which type of records should be printed
$sql = "SELECT y.YearName FROM {$intranet_db}.YEAR_CLASS yc INNER JOIN {$intranet_db}.YEAR y ON yc.YearID = y.YearID WHERE yc.ClassTitleEN = '".$TargetClass."' AND yc.AcademicYearID = ".Get_Current_Academic_Year_ID();
$yearName = current($li->returnVector($sql));

//$hasSA = in_array($yearName, array('S3', 'S6'));
$pageSize = $hasSA ? 2 : 1;

# OLE records number limit
$portfolio_report_config_file = "$eclass_root/files/portfolio_slp_config.txt";
if(file_exists($portfolio_report_config_file))
{
	list($filecontent, $OLESettings, $ExtSettings) = explode("\n", trim(get_file_content($portfolio_report_config_file)));
	$OLESettings = unserialize($OLESettings);
	$OLELimit = $OLESettings[$yearName][0];
}
if ($OLELimit == "")
{
	$OLELimit = 10;
}

# User Basic
##LOG: GET ClassNumber 
if ($StudentID=="")
{
	$conds = " AND ClassName = '".$TargetClass."'";
}
$conds .= ($StudentID=="") ? "" : " AND UserID = '".$StudentID."'";
//$sql = "SELECT UserID, CONCAT(EnglishName, ' (', ChineseName,')'), ClassName, ClassNumber FROM {$intranet_db}.INTRANET_USER";
$sql = "SELECT UserID, EnglishName, WebSAMSRegNo, ClassName, ClassNumber FROM {$intranet_db}.INTRANET_USER";
$sql .= " WHERE 1".$conds;
$sql .= " ORDER BY ClassName, ClassNumber + 0";
$t_user_arr = $li->returnArray($sql);

$FormClassMgmt = new form_class_manage();

for($i=0; $i<count($t_user_arr); $i++)
{
	$t_uid = $t_user_arr[$i]["UserID"];
	$t_eng_name = $t_user_arr[$i]["EnglishName"];
	$t_regno = substr($t_user_arr[$i]["WebSAMSRegNo"], 1);
	$t_classname = $t_user_arr[$i]["ClassName"];
	$t_classnumber = $t_user_arr[$i]["ClassNumber"];
	
	
	$StudentClassInfo = $FormClassMgmt->Get_Student_Class_Info_In_AcademicYear(array($t_uid), $r_academicYearID);
	if ($StudentClassInfo[0]["ClassTitleEN"]!="")
	{
		$t_classname = $StudentClassInfo[0]["ClassTitleEN"];
		$t_classnumber = $StudentClassInfo[0]["ClassNumber"];
	}
		
	$user_arr[$t_uid] =	array (
															$t_eng_name,
															$t_regno,
															$t_classname,
															$t_classnumber
                            );
}

# OLE
$conds = " os.RecordStatus IN (2,4)";

if ($StudentID=="")
{
	$conds .= " AND iu.ClassName = '".$TargetClass."'";
}
//$conds .= " AND os.SLPOrder IS NOT NULL";
$conds .= ($StudentID=="") ? "" : " AND os.UserID = '".$StudentID."'";

# date range

//$AcademicYearStart = $AcademicYear->AcademicYearStart;
//$AcademicYearEnd = $AcademicYear->AcademicYearEnd;

if ($AcademicYearStart!="" && $AcademicYearEnd!="")
{
	//$conds .= " AND UNIX_TIMESTAMP(os.StartDate)>=UNIX_TIMESTAMP('{$AcademicYearStart}') AND UNIX_TIMESTAMP(os.StartDate)<=UNIX_TIMESTAMP('{$AcademicYearEnd}') ";
	$conds .= " AND op.AcademicYearID = '".$r_academicYearID."' ";
}


$sql = ($PrintSelRec) ? "SELECT UserID, Title, Organization, Role, Hours FROM (" : "";
  $sql .= "SELECT os.UserID, os.Title, os.Organization, os.Role, IF(os.Hours = 0, 'N.A.', ROUND(os.Hours, 1)) AS Hours, os.SLPOrder FROM {$eclass_db}.OLE_STUDENT AS os";
  //$sql .= " LEFT JOIN (SELECT UserID, RecordID FROM {$eclass_db}.OLE_STUDENT WHERE SLPOrder IS NOT NULL ORDER BY UserID, SLPOrder, RecordID DESC) AS t_os ON os.RecordID = t_os.RecordID";
  $sql .= " INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID";
  $sql .= " INNER JOIN {$intranet_db}.INTRANET_USER AS iu ON iu.UserID = os.UserID";
  $sql .= " WHERE ".$conds;
  $sql .= " ORDER BY os.UserID, os.SLPOrder DESC, os.RecordID DESC";
$sql .= ($PrintSelRec) ? ") AS t WHERE SLPOrder IS NOT NULL ORDER BY SLPOrder" : "";
$t_ole_arr = $li->returnArray($sql);

for($i=0; $i<count($t_ole_arr); $i++)
{
	$ole_uid = $t_ole_arr[$i]["UserID"];
  $ole_title = $t_ole_arr[$i]["Title"];
  $ole_organization = $t_ole_arr[$i]["Organization"];
	$ole_role = $t_ole_arr[$i]["Role"];
	$ole_hours = $t_ole_arr[$i]["Hours"];

  if(count($ole_arr[$ole_uid]) >= $OLELimit) continue;

	$ole_arr[$ole_uid][] =	array (
																	(count($ole_arr[$ole_uid])+1).".",
																	$ole_title,
																	$ole_organization,
																	$ole_role,
																	$ole_hours
																);
}

if($hasSA)
{
  # Self Account
  $conds = " AND INSTR(sas.DefaultSA, 'SLP') != 0";
  
	if ($StudentID=="")
	{
	  $conds .= " AND iu.ClassName = '".$TargetClass."'";
	}
  $conds .= ($StudentID=="") ? "" : " AND sas.UserID = '".$StudentID."'";
  
  $sql = "SELECT sas.UserID, sas.Details FROM {$eclass_db}.SELF_ACCOUNT_STUDENT AS sas";
  $sql .= " INNER JOIN {$intranet_db}.INTRANET_USER AS iu ON iu.UserID = sas.UserID";
  $sql .= " WHERE 1".$conds;
  $sql .= " ORDER BY iu.UserID";
  $t_sa_arr = $li->returnArray($sql);
  
  for($i=0; $i<count($t_sa_arr); $i++)
  {
  	$sa_uid = $t_sa_arr[$i]["UserID"];
  	$sa_details = $t_sa_arr[$i]["Details"];
  	//$sa_details = strip_tags(str_replace(array("\r\n", "\r"), "<br />", trim($sa_details)), '<p><br>');
  	$sa_details = strstr(strtoupper($sa_details), "<BR") ? $sa_details : strip_tags(str_replace(array("\r\n", "\r"), "<br />", trim($sa_details)), '<p><br>');
  	$sa_details = str_replace("<br />", "<br clear=\"all\" />", $sa_details);
  
  	$sa_arr[$sa_uid] =	$sa_details;
  }
}

# Report Title
$report_title[] = "<font size='2'>Y</font>ING <font size='2'>W</font>A <font size='2'>G</font>IRLS' <font size='2'>S</font>CHOOL";
$report_title[] = "<font size='2'>O</font>THER <font size='2'>L</font>EARNING <font size='2'>E</font>XPERIENCES <font face='arial' size='2' >".(($Year != "") ? " (".$Year.")" : "")."</font>";
$report_title[] = "<font size='2'>S</font>TUDENT-<font size='2'>R</font>EPORTED <font size='2'>D</font>ATA";

$report_html = "";
$style = ($PrintType == 'word') ? 'style="width: 100%; page-break-after:always"' : 'style="width: 18.0cm; page-break-after:always"';

  $page_footer = '
	<table border="0" width="92%" align="center"> 
	<tr><td align="right" >
		<table width="185" border="0">
		  <tr> 
		    <td height="40">&nbsp;</td>
		  </tr>
		  <tr> 
		    <td class="basicInfoDataCell">&nbsp;</td>
		  </tr>
		  <tr> 
		    <td class="signatureCell" align="center" nowrap="nowrap"><font size="2">S</font>TUDENT\'S <font size="2"">S</font>IGNATURE</td>
		  </tr>
		</table>
	</td>
	</tr>
	</table>';
	
$student_i = 0;
foreach($user_arr as $user_id => $user_detail)
{
  $curPage = 1;
  $student_i ++;
  
  $style_now = ($student_i==sizeof($user_arr) && !$hasSA) ? str_replace("page-break-after:always", "", $style) : $style;
  
  $report_html .= '<table cellpadding="0" cellspacing="0" border="0" '.$style_now.'>';
  $report_html .= '<tr><td width="100%" align="center">'.GEN_REPORT_HEAD($curPage, 1).'</td></tr>';
  $report_html .= '<tr><td width="100%" align="center">'.GEN_STUDENT_BASIC_INFO($user_detail).'</td></tr>';
 // $report_html .= '<tr><td style="height:0.3cm">&nbsp;</td></tr>';
  $report_html .= '<tr><td width="100%" align="center">'.GEN_ACTIVITY($ole_arr[$user_id]).'</td></tr>';
  $report_html .= '<tr><td width="100%" align="center">'.$page_footer.'</td></tr>';
  $report_html .= '</table>';
  
  

  if($hasSA)
  {
    $curPage = 2;
  
  	# issue and blank page for printing in double pages
    $report_html .= "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" {$style} ><tr><td>&nbsp;" .
					"</td></tr></table>\n";
    
    # part 2
    
    # any page break by student?
    $sa_contents = $sa_arr[$user_id];
    $break_pos = strpos(strtoupper($sa_contents), "[PAGE-BREAK]");
    
    $SApage1 = "";
    $SApage2 = "";
    if ($break_pos>0 && $break_pos!="")
    {
    	$SApage1 = trim(substr($sa_contents, 0, $break_pos));
    	$SApage2 = trim(trim(substr($sa_contents, $break_pos+12)), "<br clear=\"all\" />");
    }
    
    if ($SApage1!="" && $SApage2!="")
    {
    	# 2 pages
    	
	    $report_html .= '<table cellpadding="0" cellspacing="0" border="0" '.$style.'>';
	    $report_html .= '<tr><td width="100%" align="center">';
	    $report_html .= GEN_REPORT_HEAD(1, 2).'</td></tr>';
	    $report_html .= '<tr><td width="100%" align="center">'.GEN_STUDENT_BASIC_INFO($user_detail).'</td></tr>';
	    //$report_html .= '<tr><td style="height:0.3cm">&nbsp;</td></tr>';
	    $report_html .= '<tr><td width="100%" align="center">'.GEN_SA($SApage1).'</td></tr>';
	  	$report_html .= '<tr><td width="100%" align="center">'.$page_footer.'</td></tr>';
	    $report_html .= '</table>';
	    
  		$style_now = ($student_i==sizeof($user_arr) ) ? str_replace("page-break-after:always", "", $style) : $style;
    	
	    $report_html .= '<table cellpadding="0" cellspacing="0" border="0" '.$style_now.'>';
	    $report_html .= '<tr><td width="100%" align="center">';
	    $report_html .= GEN_REPORT_HEAD(2, 2).'</td></tr>';
	    //$report_html .= '<tr><td style="height:0.3cm">&nbsp;</td></tr>';
	    $report_html .= '<tr><td width="100%" align="center">'.GEN_SA($SApage2, "PAGE2").'</td></tr>';
	  	$report_html .= '<tr><td width="100%" align="center">'.$page_footer.'</td></tr>';
	    $report_html .= '</table>';
	    
    } else
    {
    	# 1 page only
    	
    	
  		$style_now = ($student_i==sizeof($user_arr) ) ? str_replace("page-break-after:always", "", $style) : $style;
  
	    $report_html .= '<table cellpadding="0" cellspacing="0" border="0" '.$style_now.'>';
	    $report_html .= '<tr><td width="100%" align="center">';
	    $report_html .= GEN_REPORT_HEAD(1, 1).'</td></tr>';
	    $report_html .= '<tr><td width="100%" align="center">'.GEN_STUDENT_BASIC_INFO($user_detail).'</td></tr>';
	    //$report_html .= '<tr><td style="height:0.3cm">&nbsp;</td></tr>';
	    $report_html .= '<tr><td width="100%" align="center">'.GEN_SA($sa_contents).'</td></tr>';
	  	$report_html .= '<tr><td width="100%" align="center">'.$page_footer.'</td></tr>';
	    $report_html .= '</table>';
	    
	    
	  	# issue and blank page for printing in double pages
	    $report_html .= "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" {$style} ><tr><td>&nbsp;" .
						"</td></tr></table>\n";
    }

  }
}

# Print report content
if($PrintType == 'word') {
	$ContentType = "application/x-msword";
	$filename = "export_word_file".date("ymd").".doc";
	$tags_to_strip = Array("A", "a", "HTML", "html", "BODY", "body");
	foreach ($tags_to_strip as $tag)
	{
		$report_html = preg_replace("/<\/?" . $tag . "(.|\s)*?>/", "", $report_html);
	}
	
	$output = "<HTML>\n";
	$output .= "<HEAD>\n";
	$output .= returnHtmlMETA();
	$output .= "</HEAD>\n";
	$output .= "<BODY LANG=\"zh-HK\">\n";
	$output .= $report_html;
	$output .= "</BODY>\n";
	$output .= "</HTML>\n";

	header('Pragma: public');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

	header('Content-type: '.$ContentType);
	header('Content-Length: '.strlen($output));

	header('Content-Disposition: attachment; filename="'.$filename.'";');

	print $output;
}
else {
?>
<html>
<head>

<style type="text/css">
/* <![CDATA[ */

html, body { margin: 0px; padding: 0px; } 

.moduleTable, .moduleHead, .moduleCell
{
}


.moduleHead, .moduleCell, .specialCell, .pageNumberCell
{
	font-family: Arial;
	font-size: 9pt;
	padding-left: 8px;
  padding-top: 5px;
  padding-bottom: 5px;

	line-height: 2;
}

.moduleCell2
{
	font-family: times;
	font-size: 10pt;
	padding-left: 8px;
	padding-top: 5px;
	padding-bottom: 5px;
	line-height: 1.5;
}

.moduleTable
{
}

.moduleCaption
{
	font-family: Arial;
	font-size: 8pt;
	padding-left: 8px;
	text-align: left;	
	padding-top:<?=$SpaceTop?>mm;
	padding-bottom:<?=$SpaceBottom?>mm;
}


.moduleHead
{
background: #CCCCDF;
}

.moduleCell
{
}

.div690
{
	height:690px;
	overflow:auto;
}

.div753
{
	height:753px;
	overflow:auto;
}

.basicInfoTitleCell 
{
	font-family: Arial;
	line-height: 1.5;
	font-size: 8pt;
	padding-left: 8px;
}


.basicInfoDataCell
{
	font-family: Arial;
	line-height: 1.5;
	font-size: 9pt;
	padding-left: 8px;
}

.signatureCell
{
	font-family: Arial;
	font-size: 8pt;

	line-height: 2;
}

.basicInfoTitleCell
{
  padding-right: 20px;
}

.basicInfoDataCell
{
	border-bottom: 1px solid black;
}

.reportTitleCell
{
	font-family: Arial;
	font-size: 9pt;
	line-height: 1.5;
	text-align: center;
	font-weight: "bold";
}

.specialCell
{
  border-top: 3px double #000;
  font-style: italic;
}

.pageNumberCell
{
	font-family: Arial;
	font-size: 9pt;
	line-height: 1.5;
 	text-align:right;
}

<style type='text/css'>

@media print and (width: 21cm) and (height: 29.7cm) {
    @page {
       margin: 3cm;
    }
 }
 
@media print
{
  #footer {
     	display:block;
	   position:fixed;
	   bottom:0px;
	   width:100%;
  }     
    
}

/* ]]> */
</style>


<script language="JavaScript">

/******************** Settings ********************/ 
var curPageNo = 1;
var curModuleIndex = 0;
var curUserID;
var pageWidthMax = "18.0cm";
var pageHeightMax = 950;
var pageBannerHeight = "";
var reportTitle = "<?=$report_title?>";

</script>

</head>

<body>

<center>
<?=$report_html?>
</center>


<!--
<div id="footer">
	<table border="0" width="80%" align="center"> 
	<tr><td align="right" >
		<table width="185" border="0">
		  <tr> 
		    <td class="basicInfoDataCell">&nbsp;</td>
		  </tr>
		  <tr> 
		    <td class="signatureCell" align="center" nowrap="nowrap"><font size='2'>S</font>TUDENT'S <font size='2'>S</font>IGNATURE</td>
		  </tr>
		  <tr> 
		    <td height="55">&nbsp;</td>
		  </tr>
		</table>
	</td>
	</tr>
	</table>
</div>
-->

</body>
</html>
<?php } ?>

<?php
/********** functions **********/
  function GEN_REPORT_HEAD($pageNo, $pageSize)
  {
    global $report_title;

    $ReturnStr = "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"90%\" align=\"center\">\n";
    $ReturnStr .= "<tr><td class=\"pageNumberCell\">P.".$pageNo." of ".$pageSize."</td></tr>";
    for($i=0; $i<count($report_title); $i++)
    {
      $ReturnStr .= "<tr><td class=\"reportTitleCell\"><u>".$report_title[$i]."</u></td></tr>";
    }
    $ReturnStr .= "</table>";
    
    return $ReturnStr;
  }
  
  function GEN_STUDENT_BASIC_INFO($ParUser)
  {
    global $issuedate;
  //	DEBUG_R($ParUser);
    $ReturnStr = "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" align=\"center\" style=\"padding-top: 2mm;\">\n";
    
    $ReturnStr .= "<tr height=\"28px\">";
    $ReturnStr .= "<td width=\"10%\" class=\"basicInfoTitleCell\"><font size='2'>N</font>AME:</td>";
    $ReturnStr .= "<td width=\"28%\" ><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" align=\"center\"><tr><td class=\"basicInfoDataCell\">".$ParUser[0]."</td></tr></table></td>";
    $ReturnStr .= "<td width=\"9%\">&nbsp;</td>";
    $ReturnStr .= "<td width=\"25%\" align=\"right\" class=\"basicInfoTitleCell\"><font size='2'>S</font>TUDENT <font size='2'>ID</font>:</td>";
    $ReturnStr .= "<td width=\"28%\" ><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" align=\"center\"><tr><td class=\"basicInfoDataCell\">".$ParUser[1]."</td></tr></table></td>";
    $ReturnStr .= "</tr>";
    
    $ReturnStr .= "<tr height=\"28px\">";
    $ReturnStr .= "<td class=\"basicInfoTitleCell\"><font size='2'>C</font>LASS:</td>";
    $ReturnStr .= "<td ><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" align=\"center\"><tr><td class=\"basicInfoDataCell\">".$ParUser[2]." ({$ParUser[3]})</td></tr></table></td>";
    $ReturnStr .= "<td>&nbsp;</td>";
    $ReturnStr .= "<td align=\"right\" class=\"basicInfoTitleCell\"><font size='2'>D</font>ATE OF ISSUE:</td>";
    
    $ReturnStr .= "<td ><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" align=\"center\"><tr><td class=\"basicInfoDataCell\">".($issuedate=="" ? "&nbsp;" : $issuedate)."</td></tr></table></td>";
    $ReturnStr .= "</tr>";
    
    $ReturnStr .= "</table>";
    
    return $ReturnStr;
  }

  function GEN_ACTIVITY($ParActivityArr)
  {
    $cellAlignArr = array("", "", "", "", "center");

    $ReturnStr = "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" align=\"center\">\n";
    $ReturnStr .= "<tr><td class=\"moduleCaption\"><u><font size='2'>P</font>ART <font size='2'>I</font>: <font size='2'>A</font>CTIVITIES / <font size='2'>P</font>ROGRAMMES / <font size='2'>C</font>OMPETITIONS</caption></u></td></tr>";
    $ReturnStr .= "</table>";
    $ReturnStr .= "<table cellpadding=\"5\" cellspacing=\"5\" border=\"0\" width=\"95%\" style=\"border:1px black solid;\" align=\"center\"><tr><td height=\"690\" valign=\"top\" >\n";
    $ReturnStr .= "<div class=\"div690\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"98%\" align=\"center\" >\n";
    $ReturnStr .= "<tr>";
    $ReturnStr .= "<td colspan=\"2\" width=\"42%\" class=\"moduleHead\"><font size='2'>A</font>CTIVITY / <font size='2'>P</font>ROGRAMME / <font size='2'>C</font>OMPETITION</td>";
    $ReturnStr .= "<td width=\"33%\" class=\"moduleHead\"><font size='2'>O</font>RGANIZED BY</td>";
    $ReturnStr .= "<td width=\"14%\" class=\"moduleHead\"><font size='2'>R</font>OLE</td>";
    $ReturnStr .= "<td width=\"11%\" class=\"moduleHead\" align=\"center\"><font size='2'>N</font>O. OF <br /><font size='2'>H</font>OUR(S)</td>";
    $ReturnStr .= "</tr>";

	# max 10 records for each student
	$i_max = (sizeof($ParActivityArr)<10) ? sizeof($ParActivityArr) : 10;
    for($i=0; $i<$i_max; $i++)
    {
      $ReturnStr .= "<tr>";
      for($j=0; $j<count($ParActivityArr[$i]); $j++)
      {
      	$td_width = ($j==0) ? " width='1%'" : "";
      	if ($j==4 && $ParActivityArr[$i][$j]!="N.A." && round($ParActivityArr[$i][$j])==$ParActivityArr[$i][$j])
      	{
      		$ParActivityArr[$i][$j] = round($ParActivityArr[$i][$j]);
      	}
        $ReturnStr .= "<td {$td_width} align=\"".$cellAlignArr[$j]."\" class=\"moduleCell\" valign=\"top\">".$ParActivityArr[$i][$j]."</td>";
      }
      $ReturnStr .= "</tr>";
    }
    /*
    $testingLine = "abcdefg hisdf sdf sdf sf dsf dsf <br />123090 0909 213098 <br />";
    for ($tii=0; $tii<20; $tii++)
    {
    	$testingdata .= $testingLine;
    }
    */
    $ReturnStr .= "<tr>";
    $ReturnStr .= "<td colspan=\"5\">{$testingdata}&nbsp;</td>";
    $ReturnStr .= "</tr>";
    $ReturnStr .= "</table>";
    
    $ReturnStr .= "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"91%\" align=\"center\" ><tr>";
    $ReturnStr .= "<td class=\"specialCell\">I hereby declare that I have participated in the activities above.</td>";
    $ReturnStr .= "</tr></table>";
    
    $ReturnStr .= "</td></tr></div></table>";
    
    return $ReturnStr;
  }
  
  function GEN_SA($ParSA, $CURPAGE="PAGE1")
  {
  	/*
    $ReturnStr = "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"95%\">\n";
    $ReturnStr .= "<caption valign=\"top\" class=\"moduleCaption\"><font size='2'>P</font>ART <font size='2'>II</font>: <font size='2'>P</font>ERSONAL <font size='2'>R</font>EFLECTIONS</caption>";
    $ReturnStr .= "<tr>";
    $ReturnStr .= "<td class=\"moduleCell\">".$ParSA."</td>";
    $ReturnStr .= "</tr>";
    $ReturnStr .= "</table>";
    */
    
    $container_height = ($CURPAGE=="PAGE1") ? "690" : "753";
    $container_div = ($CURPAGE=="PAGE1") ? "div690" : "div753";
    
    $ReturnStr = "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" align=\"center\">\n";
    $ReturnStr .= "<tr><td class=\"moduleCaption\"><u><font size='2'>P</font>ART <font size='2'>II</font>: <font size='2'>P</font>ERSONAL <font size='2'>R</font>EFLECTIONS</u></td></tr>";
    $ReturnStr .= "</table>";
    $ReturnStr .= "<table cellpadding=\"5\" cellspacing=\"5\" border=\"0\" width=\"95%\" style=\"border:1px black solid;\" align=\"center\"><tr><td height=\"{$container_height}\" valign=\"top\">\n";
    $ReturnStr .= "<div class=\"{$container_div}\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"95%\" align=\"center\">\n";
      $ReturnStr .= "<tr>";
      
    $ReturnStr .= "<td class=\"moduleCell2\">".$ParSA."</td>";
      $ReturnStr .= "</tr>";
    
    $ReturnStr .= "</table></div>";
    
    $ReturnStr .= "</td></tr></table>";
    
    return $ReturnStr;
  }
?>