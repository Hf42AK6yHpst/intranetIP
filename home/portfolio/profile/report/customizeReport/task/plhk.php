<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/plhk/libpf-slp-RptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/plhk/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic.php");

$YearClassID = trim($YearClassID);
$issuedate  = trim($issuedate);


//debug_r($_POST);

//YearClassID

$objReportMgr = new stcecRptMgr();

$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();

$lpf_report = new libpf_report();
list($SubjectIDNameAssoArr, $StudentFormInfoArr, $StudentAcademicResultInfoArr, $SubjectInfoOrderedArr) = $lpf_report->GET_ACADEMIC_PERFORMANCE_SLP_IN_ID($studentList);
// debug_pr($StudentAcademicResultInfoArr);
$libpf_academic = new libpf_academic();
$FullMarkArr = $libpf_academic->Get_Subject_Full_Mark();

//$NoOfRecord;

for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	
	$_studentId = $studentList[$i];

//$StudentAcademicResultInfoArr[$_studentId][$thisSubjectID][$thisAcademicYearID]
	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	$objStudentInfo -> setAcademicYear($academicYearID);
	$objStudentInfo -> setAcademicYearStr();
	$objStudentInfo -> setYearClassID($YearClassID);
	$objStudentInfo -> setNoOfRecordInt($NoOfRecordInt);
	$objStudentInfo -> setNoOfRecordExt($NoOfRecordExt);
	
	$objStudentInfo -> setAll_AcademicYearID($All_AcademicYearID);
	
	### Page Content
	$html .=$objStudentInfo->getEmptyTable('3%');
	$html .= '<table width="100%" height="990px">';
	$html .= '<tr><td style="vertical-align:top" align="center">'.$objStudentInfo->getPart1_HTML().'</td></tr>';
	$html .= '<tr><td style="vertical-align:top">'.$objStudentInfo->getPart2_HTML().'</td></tr>';
	$html .= '<tr><td style="vertical-align:top">'.$objStudentInfo->getPart3_HTML($SubjectInfoOrderedArr,$StudentAcademicResultInfoArr[$_studentId],$FullMarkArr).'</td></tr>';
	$html .= '<tr><td style="vertical-align:top">'.$objStudentInfo->getPart4a_HTML().'</td></tr>';
	$html .='</table>';
	$html .=  $objStudentInfo->getFooter_HTML();
	$html .= $objStudentInfo->getPagebreakTable();

//	$html .= $objStudentInfo->getPart1_HTML();	
//	$html .= $objStudentInfo->getPart2_HTML();	
//	$html .= $objStudentInfo->getEmptyTable('1%');
//	$html .= $objStudentInfo->getPart3_HTML($SubjectInfoOrderedArr,$StudentAcademicResultInfoArr[$_studentId],$FullMarkArr);	
//	$html .= $objStudentInfo->getEmptyTable('3%');
//
//	$html .= $objStudentInfo->getPart4a_HTML();	

//	$html .='</td></tr>';
//	$html .='</table>';
//	$html .=  $objStudentInfo->getFooter_HTML();
	
	$html .= $objStudentInfo->getPart4b_HTML();
	
	$html .=$objStudentInfo->getEmptyTable('3%');

	$html .= $objStudentInfo->getPart5_HTML();	
	
	$html .=$objStudentInfo->getEmptyTable('3%');

	
	$html .=$objStudentInfo->getPart6_HTML();

	
	if($i!=$i_max-1)
	{
		$html .= $objStudentInfo->getPagebreakTable();
	}
	
	$TotalPage = $objStudentInfo->getPageNumber()-1;
	
	$html = str_replace('<!--TotalNumber-->',$TotalPage,$html);
	
}


if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}


exit();
?>