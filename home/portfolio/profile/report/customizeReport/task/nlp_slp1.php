<?
ini_set('memory_limit','2048M');
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/nlp/libpf-slp-nlp.php");
// require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

// issue date
if(empty($issuedate)){
    $tempIssueDate = date("Y-m-d");
    $issuedate = date("Y-m-d");
}
else{
    $tempIssueDate = date("Y-m-d" , strtotime($issuedate));
    $issuedate = date("Y-m-d", strtotime($issuedate));
}
// $issueAYInfo = getAcademicYearAndYearTermByDate($tempIssueDate);
$issueAYName = date("Y", strtotime($issuedate));

$task = $_POST['task'];
$task_num = substr($task,-1);

// target student
if(!empty($StudentID)){
    $studentList = array($StudentID);
    include_once($PATH_WRT_ROOT."includes/libuser.php");
    $luser = new libuser($StudentID);
    $luser->LoadUserData($StudentID);
    $className = $luser->ClassName;
    $classNumber = $luser->ClassNumber;
    unset($luser);
    $filename = $issueAYName.'_'.$className.'-'.$classNumber.'_'.$task_num;
}
else{
    include_once($PATH_WRT_ROOT."includes/libclass.php");
    $lclass = new libclass();
    $className = $lclass->getClassName($_POST['YearClassID']);
    unset($lclass);
    $filename = $issueAYName.'_'.$className.'_'.$task_num;
}

if (!isset($studentList))
{
    // If no studentIDs retrieve from classname
    $objReportMgr = new ipfReportManager();
    $objReportMgr->setStudentID($StudentID);
    $objReportMgr->setYearClassID($YearClassID);
    $studentList = $objReportMgr->getStudentIDList($ActivatedOnly=true);
}

require_once($PATH_WRT_ROOT.'includes/tcpdf60/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {
    
    //Page header
    public function Header() {
        global $PATH_WRT_ROOT,$studentHeader;
        $this->SetFont('arialunicid0', '', 11);
        //         $this->Cell(0, 15, $studentHeader, 0, false, 'C', 0, '', 0, false, 'M', 'M');
        $this->writeHTML($studentHeader);
    }
    
    // Page footer
    public function Footer() {
        global $StudentName,$StudentWebSAMS,$issueAYName;
        //         $style['position'] = '';
        $style['stretch'] = false; // disable stretch
        $style['fitwidth'] = false; // disable fitwidth

        // Left alignment
        $style['align'] = 'L';
        $this->write1DBarcode($issueAYName, 'C39', '', '', '', 7, 0.2, $style, 'N');
        
        $this->Ln(-7);
        
        // Center alignment
        $style['align'] = 'C';
        $this->write1DBarcode($StudentName, 'C39', '', '', '', 7, 0.2, $style, 'N');
        
        $this->Ln(-7);
        
        // Right alignment
        $style['align'] = 'R';
//         $this->write1DBarcode($_studentid, 'CODABAR', '', '', '', 13, 0.4, $style, 'N');
        $this->write1DBarcode($StudentWebSAMS, 'C39', '', '', '', 7, 0.2, $style, 'N');
    }
}
$objPrinting = new objPrinting($_POST['task'],$studentList,$task);
$numStudent = count($studentList);

if($numStudent>0){
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    
    $margin_left = 15; //mm
    $margin_right = 15;
    $margin_top = 60;
    $margin_bottom = 45;
    $margin_header = 45;
    $margin_footer = 9; 
    
    $pdf->SetMargins($margin_left, $margin_top, $margin_right);
    $pdf->SetAutoPageBreak(TRUE, $margin_bottom);
    $pdf->SetHeaderMargin($margin_header);
    $pdf->SetFooterMargin($margin_footer);
    
    $pdf->SetFont('droidsansfallback', '', '');
    
    for($y = 0; $y<$numStudent ; $y ++){
        
        $_studentid = $studentList[$y];
        $objPrinting->setStudent($_studentid);
        
        // create new PDF document
        
        
        $studentHeader =  $objPrinting->returnStudentInfoTable();
        
        
        $studentInfo = $objPrinting->getStudentInfo();
     
        $StudentName = $studentInfo['EnglishName'];
        $StudentWebSAMS = $studentInfo['WebSAMSRegNo'];
   
        $style = $objPrinting->returnStyleSheet();
        
        $stuOLEHTML = $objPrinting->returnStudentCurrentYearOLETable();
    
        
        $pdf->AddPage();
        //         if($y == 0){
        //             $html = $style;
        //         }
        
        
        $pdf->writeHTML($style.$stuOLEHTML, true, false, true, false, '');
//         $pdf->Ln(10);
//         $pdf->writeHTML($style.$stuAwardHTML, true, false, true, false, '');
//         $pdf->Ln(10);
//         $pdf->writeHTML($style.$stuPerformanceHTML, true, false, true, false, '');
//         $pdf->Ln(10);
//         $pdf->writeHTML($style.$stuSelfAccountHTML, true, false, true, false, '');
        
        $pdf->endPage();
        
        unset($studentHeader);
        unset($StudentName);
        unset($StudentWebSAMS);
    }
    
    $pdf->Output($filename.'.pdf', 'I');
}
?>
