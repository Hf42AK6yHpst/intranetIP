<?php
@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/srlc/libpf-slp-srlc.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
require_once($PATH_WRT_ROOT.'includes/tcpdf60/tcpdf.php');

//StartTimer("GLB_StartTime");

function getDisplayScore($newArray,$__academicYearId,$_SubjectID,$_SubjectCode){
	//Display Score/Grade/---
	if($newArray[$__academicYearId][$_SubjectID][$_SubjectCode]['Score'] != '' &&
		$newArray[$__academicYearId][$_SubjectID][$_SubjectCode]['Score'] >= 0){
		$__displayScore = $newArray[$__academicYearId][$_SubjectID][$_SubjectCode]['Score'];
	}
	else{
		
		if($newArray[$__academicYearId][$_SubjectID][$_SubjectCode]['Grade'] ==""){
			$__displayScore = '---';
		}
		else{
			
			$__displayScore = $newArray[$__academicYearId][$_SubjectID][$_SubjectCode]['Grade'];
		}
	}
	return $__displayScore;
}

class MYPDF extends TCPDF {
	
	var $page_header;
	var $special_header;
	
	public function set_Page_Header($var){
		$this->page_header = $var;
	}
	public function set_special_header($var){
		$this->special_header = $var;
	}
	
    //Page header
    public function Header() {
        global $PATH_WRT_ROOT;
        global $student_header,$StudentInfoArr; // from the script at student-loop
        
        if($this->page_header == 'cover'){
	        // Logo
	        $image_file = $PATH_WRT_ROOT.'file/customization/srlc/srlc_logo.jpg';
	        $this->Image($image_file, 24, 7, 36, 36, 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
	       
	       // Title
	       // Set font
	        $this->SetY(10);
	        $this->SetFont('arialunicid0', 'B', 17);
	        $this->Cell(0, 10, 'ST. ROSE OF LIMA\'S COLLEGE 聖羅撒書院       ', 0, 0,'R');
	        $this->SetY(22);
	        $this->SetFont('arialunicid0', 'B', 15);
	        $this->Cell(0, 10, 'Student Learning Profile 學生學習概覽                   ', 0, 0,'R');
		
	        // Student Info
	        $this->SetY(48);
	        $this->SetFont('arialunicid0', '', 10);
	        $this->writeHTML($student_header);
	        
        }
        else{
        	$this->SetY(9);
        	$this->SetFont('arialunicid0', '', 15);
        	$this->Cell(0, 0, 'Student Learning Profile 學生學習概覽', 0, 0,'L');
        	
        	$this->SetY(7);
        	$this->SetFont('arialunicid0', '', 10);
        	//<font size="15">Student Learning Profile 學生學習概覽</font>
        	$html ='
					<table cellpadding="2">
						<tr>
							<td width="112mm" rowspan="2"></td>
							<td width="14mm">Name </td>
							<td width="70mm">學生姓名: '.$StudentInfoArr["EnglishName"].' ('.$StudentInfoArr["ChineseName"].')</td>
						</tr>
						<tr>
							<td>Reg.No </td>
							<td>註冊編號: '.$StudentInfoArr["WebSAMSRegNo"].'</td>
						</tr>
					</table>
					';
			$this->writeHTML($html);
			$this->Line(7, 20, 203, 20,'');
			
			if($this->special_header){
				$this->writeHTML($this->special_header);
			}
        }
    }

    // Page footer
    public function Footer() {	
        
		$this->Line(7, 276, 203, 276,'');
        // Set font
        $this->SetFont('arialunicid0', '', 10);
        //Page number
        $this->SetY(-19);
        $this->Cell(0, 0, 'School Address 學校地址: 29 Ngan Shing Street, Shatin 沙田銀城街29號', 0, 0,'L');
        $this->Cell(0, 0, 'Page '.$this->getPageNumGroupAlias().' of '.$this->getPageGroupAlias(), 0, 0,'C'); // if have getPageGroupAlias align to 'C' = to right
        $this->SetY(-13);
        $this->Cell(0, 0, 'School Phone 學校電話: 23371867', 0, 0,'L');
        $this->Cell(0, 0, '第 '.$this->getPageNumGroupAlias().' 頁 (共 '.$this->getPageGroupAlias().' 頁)       ', 0, 0,'C');       
        
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('BroadLearning Education (Asia) Ltd.');
$pdf->SetTitle('聖羅撒書院 SLP');
$pdf->SetSubject('Student Learning Portfolio');
$pdf->SetKeywords('聖羅撒書院', 'eClass, iPortfolio, SLP');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// set header and footer fonts
//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
////$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetHeaderMargin(0);
////$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
//$pdf->SetFooterMargin(0);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$rightMargins =  7;
$leftMargins = 7;
$contentWidth = 210 - $leftMargins - $rightMargins;
//$pdf->SetMargins($leftMargins, 74, $rightMargins); // ==> set before the start of the page
 
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 22);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
// Content general style setting header size
$header_size = 16;
$header_below_space = '50%';
$contentRowStyle = 'style="line-height: 200%;"';
// ---------------------------------------------------------

$objPrinting = new objPrinting();

if(!empty($StudentID)){
	$studentList = array($StudentID);
}

if(empty($issuedate)){
	$issuedate = date("d/m/Y");
}

if (!isset($studentList))
{
	$objReportMgr = new ipfReportManager();
	
	$objReportMgr->setStudentID($StudentID);
	$objReportMgr->setYearClassID($YearClassID);
	$academicYearID = Get_Current_Academic_Year_ID();
	$objReportMgr->setAcademicYearID($academicYearID);
	$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

	$studentList = $objReportMgr->getStudentIDList();

}

//$mode = 'pdf';
$objPrinting->setYearClassID($YearClassID);
$objPrinting->setStudentList($studentList);
$objPrinting->contentReady();

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(false);


$signature_footer = '
			<table>
			<table height = 100% width="'.$contentWidth.'mm">
				<tr>
					<td colspan="2"><b>School Chop:</b></td>
				</tr>
				<tr>
					<td height="10mm" colspan="2"></td>
				</tr>
				<tr>
					<td height="17mm" width="141mm"></td>
					<td height="15mm" width="55mm" align="center"><b>__________________________<br>Mr. Yeung Kit Man<br>Principal</b></td>
				</tr>
			</table>
		';


// start looping getting student info and print
foreach((array)$studentList as $_studentid){
	
	// Change student
	$objPrinting->setStudent($_studentid);
	
	############################################################################################################
	####################################### Studnent Info (Start) ##############################################
	############################################################################################################
	
	$StudentInfoArr = $objPrinting->getStudentInfo();
	$col1_width = 50;
	$col1_1_width = 13;
	$col1_2_width = $col1_width- $col1_1_width;
	$col2_1_width = 18;
	$col2_2_width = 43;
	$col3_width = 35;
	$col4_width = 23;
	$col5_width = $contentWidth - $col1_width - $col2_width - $col3_width-$col4_width;
	
	$DisplayGender = ($StudentInfoArr["Gender"] =='M')? 'Male 男' : 'Female 女';
	
	$student_header = '
					<table width="'.$contentWidth.'mm" cellpadding="4">
						<tr>
							<td width="'.($col1_width+$col2_1_width+$col2_2_width).'mm" colspan="3">Name 學生姓名 : '.$StudentInfoArr["EnglishName"].' ('.$StudentInfoArr["ChineseName"].')</td>
							<td width="'.$col3_width.'mm">Date of Birth</td>
							<td width="'.$col4_width.'mm">出生日期</td>
							<td width="'.$col5_width.'mm">: '.$StudentInfoArr["DateOfBirth"].'</td>
						</tr>
						<tr>
							<td width="'.$col1_1_width.'mm">Sex </td>
							<td width="'.$col1_2_width.'mm">性別 : '.$DisplayGender.'</td>
							<td width="'.$col2_1_width.'mm">STRN</td>
							<td width="'.$col2_2_width.'mm">學生編號 : '.$StudentInfoArr["STRN"].'</td>
							<td width="'.$col3_width.'mm">HKID No. </td>
							<td width="'.$col4_width.'mm">身份證號碼</td>
							<td width="'.$col5_width.'mm">: '.$StudentInfoArr["HKID"].'</td>
						</tr>
						<tr>
							<td>Class </td>
							<td>班別 : '.$objPrinting->Student_ClassName.'</td>
							<td>Reg. No </td>
							<td>註冊編號 : '.$StudentInfoArr["WebSAMSRegNo"].'</td>
							<td>Date of Admission </td>
							<td>入學日期</td>
							<td>: '.$issuedate.'</td>
						</tr>
						<tr>
							<td colspan="4">School Code 學校編號 : 530689</td>
							<td>Date of Issue </td>
							<td>派發日期</td>
							<td>: '.$issuedate.'</td>
						</tr>
					</table>';
	############################################################################################################
	###################################### Studnent Info (End) #################################################
	############################################################################################################
	
	############################################################################################################
	############################ Section 1 - Assessment Report (Start) #########################################
	############################################################################################################
	
	//-----------Preparing Data
	$AcademicYearAry = $objPrinting->getStudent_AcademicYear();
	$assessmentReportInfo = $objPrinting->GetAssessmentReport();
	$classHistoryInfoAry = $objPrinting->getStudentClassHistory();
	$SchoolSubjectInfo = $objPrinting->School_SubjectInfo;

	//ReBuild $assessmentReportInfo as $assessmentReportInfoAsso
	//debug_pr($classHistoryInfoAry);
	
	$assessmentReportInfoAsso = array();
	foreach((array)$assessmentReportInfo as $_subjectScoreInfo){
		$_AcademicYearID = $_subjectScoreInfo['AcademicYearID'];
		//$_YearTermID = $_subjectScoreInfo['YearTermID'];
		$_SubjectID = $_subjectScoreInfo['SubjectID'];
		
		//Determine whether is a component
		$_Component = ($_subjectScoreInfo['SubjectComponentCode'] == '')? $_subjectScoreInfo['SubjectCode']:$_subjectScoreInfo['SubjectComponentCode'] ;
		
		$assessmentReportInfoAsso[$_AcademicYearID][$_SubjectID][$_Component] = $_subjectScoreInfo;
	}
		
	//-----------calculate table width
	$numform = count($classHistoryInfoAry);
	
	
	$chineseSubjectWidth = 34;
	$englishSubjectWidth = 43;
	$fullMarkWidth = 19;
	$SubjectWidth = $englishSubjectWidth + $chineseSubjectWidth + $fullMarkWidth;
	$yearWidth = ($contentWidth - $SubjectWidth)/$numform;

	//-----------Start Build table	
	$assessment_report = '
		<table width="'.$contentWidth.'mm">
				<tr style="line-height: 175%;">
					<td bgcolor="#D6DFEC"><font size="'.$header_size.'"><b>Academic Performance in School 校內學科成績</b></font></td>
				</tr>
		</table>
		<table width="'.$contentWidth.'mm" cellpadding="5">
			<tr style="line-height: '.$header_below_space.';">
				<td></td>
			</tr>
			<tr style="line-height: 125%;">
				<td colspan="3" width="'.$SubjectWidth.'mm"></td>';
			// Year Name & Form Name
			foreach((array)$classHistoryInfoAry as $_formInfoAry){
				
				preg_match_all("/\d+\B/", $_formInfoAry['ClassName'], $tempform);
				$form = $tempform[0][0];
				switch ($form) {
				    case 1:
				        $formnameChinese = '中一';
				        break;
				    case 2:
				        $formnameChinese = '中二';
				        break;
				    case 3:
				        $formnameChinese = '中三';
				        break;
				    case 4:
				        $formnameChinese = '中四';
				        break;
				    case 5:
				        $formnameChinese = '中五';
				        break;
				    case 6:
				        $formnameChinese = '中六';
				        break;
				}
				$assessment_report .= '<td width="'.$yearWidth.'mm" colspan="1" align="center"><b>'.$_formInfoAry['AcademicYear'].'<br>'.$formnameChinese.' '.$_formInfoAry['ClassName'].'</b></td>';
			}
			$assessment_report .= '</tr>';
				
			$assessment_report .= '<tr style="line-height: 125%;">';
			$assessment_report .= '<td width="'.$englishSubjectWidth.'mm"><b>Subject</b></td>';
			$assessment_report .= '<td width="'.$chineseSubjectWidth.'mm"><b>科目</b></td>';
			$assessment_report .= '<td align="center" width="'.$fullMarkWidth.'mm"><b>滿分<br>Full Mark</b></td>';
			foreach((array)$classHistoryInfoAry as $_formInfoAry){
				$assessment_report .= '<td align="center"><b>校內表現<br>Mark</b></td>';
			}
			$assessment_report .= '</tr>';
			
			//Subject & Score Content Here
			$row = 0;
			foreach((array)$SchoolSubjectInfo as $_SubjectInfoArr){
				
				$_SubjectID = $_SubjectInfoArr['RecordID'];
				$_SubjectCode = $_SubjectInfoArr['SubjectCode'];
				
				if($row %2 == 1){
					$tr_color = 'bgcolor="#F2F2F2"';
				}
				else{
					$tr_color = '';
				}
				
				//Check whether student have that subject records
				$_isAllScoreEmpty = true;
				foreach ((array)$classHistoryInfoAry as $__formInfoAry) {
					$_academicYearID = $__formInfoAry['AcademicYearID'];
					
					if (isset($assessmentReportInfoAsso[$_academicYearID][$_SubjectID]) ){
						$_isAllScoreEmpty = false;
						break(1);
					}
				}
				
				//If have record start print the subject score row
				if(!$_isAllScoreEmpty){
					if(!$_SubjectInfoArr['isComponent']){
						//Big Subject e.g. Chinese, English , Maths, Liberal Studies,etc.
						$assessment_report .= '<tr align="center" '.$tr_color.'>';
						$assessment_report.= '<td align="left"><font color="#10253F">'.$_SubjectInfoArr['SubjectDisplayName'].'</font></td>';
						$assessment_report.= '<td align="left"><font color="#10253F">'.$_SubjectInfoArr['SubjectDisplayChineseName'].'</font></td>';
						$assessment_report.= '<td align="center"><font color="#10253F">'.$_SubjectInfoArr['Fullmark'].'</font></td>';
						foreach((array)$AcademicYearAry as $__AcademicYearID){	
							$__displayScore = getDisplayScore($assessmentReportInfoAsso,$__AcademicYearID,$_SubjectID,$_SubjectCode);
							$assessment_report .=  '<td><font color="#10253F">'.$__displayScore.'</font></td>';
						}
						$assessment_report .= '</tr>';
						$row++;
					}
					else{
						//Component Subject e.g. Writing, Listening, SBA, etc.
//						$assessment_report .= '<tr align="center" '.$tr_color.'>';
//						$assessment_report.= '<td align="left">'.$_SubjectInfoArr['SubjectDisplayName'].'</td>';
//						$assessment_report.= '<td align="left">'.$_SubjectInfoArr['SubjectDisplayName'].'</td>';
//						$assessment_report.= '<td align="left">100/A</td>';
//						foreach((array)$AcademicYearAry as $__AcademicYearID){	
//							$__displayScore = getDisplayScore($assessmentReportInfoAsso,$__AcademicYearID,$_SubjectID,$_SubjectCode);
//							$assessment_report .=  '<td>'.$__displayScore.'</td>';
//						}
//						$assessment_report .= '</tr>';
					}
				}
			}
	$assessment_report .= '</table>';
	//-----------Table End Here	
	$chopAndSign = '<br><br><br><br><br><br>';
	$chopAndSign .= $signature_footer;
	$section1 = $assessment_report;
	############################################################################################################
	############################# Section 1 - Assessment Report (End) ##########################################
	############################################################################################################
	
	############################################################################################################
	############################ Section 2 - Awards Record - Int (Start) #######################################
	############################################################################################################
	$Awards_Int_AssoArr = $objPrinting->GetOLE('INT','AWARDS');
	
	//------- calculate table column width
	$Awards_col1_width = 30;
	$Awards_col2_width = 100;
	$Awards_col3_width = 66;
	
	if(isset($Awards_Int_AssoArr)){
		
		$Awards_Int_table ='
				<table width="'.$contentWidth.'mm">
					<tr style="line-height: 175%;">
						<td bgcolor="#D6DFEC" colspan="3"><font size="'.$header_size.'"><b>Awards and Achievements Obtained in the School 校內頒發的獎項及成就</b></font></td>
					</tr>
					<tr style="line-height: '.$header_below_space.';">
						<td></td>
					</tr>
					<tr style="line-height: 125%;">
							<td width="'.$Awards_col1_width.'mm"><b>Academic Year<br>學年</b></td>
							<td width="'.$Awards_col2_width.'mm"><b>Name of Programme 活動項目(及學年)</b></td>
							<td width="'.$Awards_col3_width.'mm"><b>Achievement 獎項及成就</b></td>
					</tr>
				</table>
				<table width="'.$contentWidth.'mm">
					<tbody>';
		foreach((array)$Awards_Int_AssoArr as $_academicYearID => $_Awards_Int__Records){
			$_rowNumber = 0;
			foreach((array)$_Awards_Int__Records as $__key => $__Awards_Int__Obj){
				
				$_rowNumber ++;
				if($_rowNumber%2 == 0){
					$_bkg_color = 'bgcolor="#F2F2F2"';
				}
				else{
					$_bkg_color = '';
				}
				
				if($__key == 0){
					$__displayAcademicYear = $__Awards_Int__Obj['AcademicYear'];
				}
				else{
					$__displayAcademicYear = '';
				}
				
				$Awards_Int_table .='			
						<tr '.$_bkg_color.' '.$contentRowStyle.'>
							<td width="'.$Awards_col1_width.'mm"><font color="#10253F">'.$__displayAcademicYear.'</font></td>
							<td width="'.$Awards_col2_width.'mm"><font color="#10253F">'.$__Awards_Int__Obj['Title'].'</font></td>
							<td width="'.$Awards_col3_width.'mm"><font color="#10253F">'.$__Awards_Int__Obj['Achievement'].'</font></td>
						</tr>
						';
			}
		}
	
		$Awards_Int_table .='
					</tbody>
				</table>';
	}else{
		$Awards_Int_table = '';
	}

	$section2 = $Awards_Int_table;
	############################################################################################################
	############################# Section 2 - Awards Record - Int (End) ########################################
	############################################################################################################

	############################################################################################################
	############################ Section 3 - Awards Record - Ext (Start) #######################################
	############################################################################################################
	$Awards_Ext_AssoArr = $objPrinting->GetOLE('EXT','AWARDS');
	
	//------- calculate table column width
	$Awards_col1_width = 31;
	$Awards_col2_width = 99;
	$Awards_col3_width = 66;
	
	if(isset($Awards_Ext_AssoArr)){
		
		$Awards_Ext_table ='
				<table width="'.$contentWidth.'mm">
					<tr style="line-height: 175%;">
						<td bgcolor="#D6DFEC" colspan="3"><font size="'.$header_size.'"><b>Awards and Achievements Obtained Outside School 校外頒發的獎項及成就</b></font></td>
					</tr>
					<tr style="line-height: '.$header_below_space.';">
						<td></td>
					</tr>
					<tr style="line-height: 125%;">
						<td width="'.$Awards_col1_width.'mm"><b>Academic Year<br>學年</b></td>
						<td width="'.$Awards_col2_width.'mm"><b>Name of Programme 活動項目<br>Organisation(s) 主辦機構</b></td>
						<td width="'.$Awards_col3_width.'mm"><b>Achievement 獎項及成就</b></td>
					</tr>
				</table>
				<table width="'.$contentWidth.'mm">
					<tbody>';
		foreach((array)$Awards_Ext_AssoArr as $_academicYearID => $_Awards_Ext_Records){
			$_rowNumber = 0;
			foreach((array)$_Awards_Ext_Records as $__key => $__Awards_Ext_Obj){
				
				$_rowNumber ++;
				if($_rowNumber%2 == 0){
					$_bkg_color = 'bgcolor="#F2F2F2"';
				}
				else{
					$_bkg_color = '';
				}
				
				if($__key == 0){
					$__displayAcademicYear = $__Awards_Ext_Obj['AcademicYear'];
				}
				else{
					$__displayAcademicYear = '';
				}
				
				if($__Awards_Ext_Obj['Organization'] != ''){
					$__displayOrganization = '<br>'.$__Awards_Ext_Obj['Organization'];
				}
				else{
					$__displayOrganization = '';
				}
				
				$Awards_Ext_table .='			
						<tr '.$_bkg_color.' '.$contentRowStyle.'>
							<td width="'.$Awards_col1_width.'mm"><font color="#10253F">'.$__displayAcademicYear.'</font></td>
							<td width="'.$Awards_col2_width.'mm"><font color="#10253F">'.$__Awards_Ext_Obj['Title'].$__displayOrganization.'</font></td>
							<td width="'.$Awards_col3_width.'mm"><font color="#10253F">'.$__Awards_Ext_Obj['Achievement'].'</font></td>
						</tr>
						';
			}
		}
	
		$Awards_Ext_table .='
					</tbody>
				</table>';
	}
	else{
		$Awards_Ext_table = '';
	}
	
	$section3 = $Awards_Ext_table;
	############################################################################################################
	############################# Section 3 - Awards Record - Ext (End) ########################################
	############################################################################################################
	
	############################################################################################################
	############################### Section 4 - ECA Record - Int (Start) #######################################
	############################################################################################################
	$ECA_AssoArr = $objPrinting->GetOLE('INT','ECA');
	
	//------- calculate table column width
	$Awards_col1_width = 30;
	$Awards_col2_width = 100;
	$Awards_col3_width = 66;
	
	
	if(count($ECA_AssoArr) > 0){

		$ECA_table ='
				<table width="'.$contentWidth.'mm">
					<tr style="line-height: 175%;">
						<td bgcolor="#D6DFEC" colspan="3"><font size="'.$header_size.'"><b>Responsible Post(s) and Participation of Regular Extra-curricular Activities in the School 校內常規課外活動的參與及其角色</b></font></td>
					</tr>
					<tr style="line-height: '.$header_below_space.';">
						<td></td>
					</tr>
					<tr style="line-height: 125%;">
						<td width="'.$Awards_col1_width.'mm"><b>Academic Year<br>學年</b></td>
						<td width="'.$Awards_col2_width.'mm"><b>Organisation(s) 活動組織</b></td>
						<td width="'.$Awards_col3_width.'mm"><b>Role 角色</b></td>
					</tr>
				</table>
				<table width="'.$contentWidth.'mm">
					<tbody>';
		foreach((array)$ECA_AssoArr as $_academicYearID => $_ECA__Records){
			
			$_rowNumber = 0;
			foreach((array)$_ECA__Records as $__key => $__ECA__Obj){
				$_rowNumber ++;
				if($_rowNumber%2 == 0){
					$_bkg_color = 'bgcolor="#F2F2F2"';
				}
				else{
					$_bkg_color = '';
				}
				
				if($__key == 0){
					$__displayAcademicYear = $__ECA__Obj['AcademicYear'];
				}
				else{
					$__displayAcademicYear = '';
				}
				
				
				$ECA_table .='			
						<tr '.$_bkg_color.' '.$contentRowStyle.'>
							<td width="'.$Awards_col1_width.'mm"><font color="#10253F">'.$__displayAcademicYear.'</font></td>
							<td width="'.$Awards_col2_width.'mm"><font color="#10253F">'.$__ECA__Obj['Title'].'</font></td>
							<td width="'.$Awards_col3_width.'mm"><font color="#10253F">'.$__ECA__Obj['Role'].'</font></td>
						</tr>
						';
			}
		}
	
		$ECA_table .='
					</tbody>
				</table>';
	}
	else{
		$ECA_table = '';
	}
	
	$section4 = $ECA_table;
	############################################################################################################
	################################ Section 4 - ECA Record - Int (End) ########################################
	############################################################################################################
	
	############################################################################################################
	############################### Section 5 - OLE internal (Start) ###########################################
	############################################################################################################
	$OLE_AssoArr = $objPrinting->GetOLE('INT','OLE');

	//------- calculate table column width
	$OLE_col1_space_width = 6;
	$OLE_col1_width = 104;
	$OLE_col2_width = 48;
	$OLE_col3_width = 38;
	
	if(count($OLE_AssoArr) > 0){
		$OLE_table ='
				<table width="'.$contentWidth.'mm">
					<tr style="line-height: 175%;">
						<td bgcolor="#D6DFEC" colspan="3"><font size="'.$header_size.'"><b>Other Learning Experiences in the School 校內的其他學習經歷</b></font></td>
					</tr>
					<tr style="line-height: '.$header_below_space.';">
						<td></td>
					</tr>
					<tr style="line-height: 125%;">
						<td width="'.($OLE_col1_width+$OLE_col1_space_width).'mm" colspan="2"><b>Name of Programme 活動項目(及學年)</b></td>
						<td width="'.$OLE_col2_width.'mm"><b>Date 參與日期</b></td>
						<td width="'.$OLE_col3_width.'mm"><b>Role 角色</b></td>
					</tr>
				</table>';
		$OLE_header = $OLE_table;
		
		$_yearNumber = 0;
		foreach((array)$OLE_AssoArr as $_academicYearID => $_OLE_Records){
			$_yearNumber ++;
			if($_yearNumber%2 == 0){
					$_bkg_color = 'bgcolor="#F2F2F2"';
				}
				else{
					$_bkg_color = '';
			}
			$OLE_table .= '<table>';
			$OLE_table .= '<thead>';
			$OLE_table .= 	'<tr style="line-height: '.$header_below_space.';">
								<td></td>
							</tr>';
			$OLE_table .= '<tr style="line-height: 100% ;"  '.$_bkg_color.'>';
			$OLE_table .= '<td><font color="#10253F">'.$_academicYearID.'</font></td>';
			$OLE_table .= '</tr>';
			$OLE_table .= '</thead>';
			$OLE_table .= '<tbody>';
			foreach((array)$_OLE_Records as $__OLE_Obj){
				
				$__startDate = $__OLE_Obj['StartDate'];
				$__endDate = $__OLE_Obj['EndDate'];
				if($__startDate == '0000-00-00' || $__startDate == ''){
					$__displayDate = '--';
				}
				else if($__endDate == '0000-00-00'){
					$__displayDate = $__startDate;
				}
				else{
					$__displayDate = $__startDate.' - '.$__endDate;
				}
				
				$OLE_table .='			
						<tr '.$contentRowStyle.' '.$_bkg_color.'>
							<td width="'.$OLE_col1_space_width.'mm"></td>
							<td width="'.$OLE_col1_width.'mm"><font color="#10253F">'.$__OLE_Obj['Title'].'</font></td>
							<td width="'.$OLE_col2_width.'mm"><font color="#10253F">'.$__displayDate.'</font></td>
							<td width="'.$OLE_col3_width.'mm"><font color="#10253F">'.$__OLE_Obj['Role'].'</font></td>
						</tr>
						';
			}
			$OLE_table .= '</tbody>';
			$OLE_table .= '</table>';
		}
	}
	else{
		$OLE_table = '';
	}
	
	$section5 = $OLE_table;
	############################################################################################################
	################################ Section 5 - OLE internal (End) ############################################
	############################################################################################################
	
	############################################################################################################
	############################### Section 6 - OLE external (Start) ###########################################
	############################################################################################################
	
	$OLEE_AssoArr = $objPrinting->GetOLE('EXT','OLE');
	
	//------- calculate table column width
	$OLEE_col1_space_width = 6;
	$OLEE_col1_width = 104;
	$OLEE_col2_width = 48;
	$OLEE_col3_width = 38;
	
	if(count($OLEE_AssoArr) > 0){
		$OLEE_table ='
				<table width="'.$contentWidth.'mm" style="line-height: 175%;">
					<tr style="line-height: 175%;">
						<td bgcolor="#D6DFEC" colspan="3"><font size="'.$header_size.'"><b>Other Learning Experiences Outside School 校外的其他學習經歷</b></font></td>
					</tr>
					<tr style="line-height: '.$header_below_space.';">
						<td></td>
					</tr>
					<tr style="line-height: 125%;">
						<td width="'.$OLEE_col1_space_width.'mm"></td>
						<td width="'.$OLEE_col1_width.'mm"><b>Name of Programme 活動項目(及學年)<br>Organisation(s) 主辦機構</b></td>
						<td width="'.$OLEE_col2_width.'mm"><b>Date 參與日期</b></td>
						<td width="'.$OLEE_col3_width.'mm"><b>Role 角色</b></td>
					</tr>
				</table>';
		$OLEE_header = $OLEE_table;
		
		$_yearNumber = 0;
		foreach((array)$OLEE_AssoArr as $_academicYearID => $_OLEE_Records){
			$_yearNumber ++;
			if($_yearNumber%2 == 0){
					$_bkg_color = 'bgcolor="#F2F2F2"';
				}
				else{
					$_bkg_color = '';
			}
			$OLEE_table .= '<table>';
			$OLEE_table .= '<thead>';
			$OLEE_table .= 	'<tr style="line-height: '.$header_below_space.';">
								<td></td>
							</tr>';
			$OLEE_table .= '<tr '.$_bkg_color.'>';
			$OLEE_table .= '<td colspan="3"><font color="#10253F">'.$_academicYearID.'</td></td>';
			$OLEE_table .= '</tr>';
			$OLEE_table .= '</thead>';
			$OLEE_table .= '<tbody>';
			foreach((array)$_OLEE_Records as $__OLEE_Obj){
				
				$__startDate = $__OLEE_Obj['StartDate'];
				$__endDate = $__OLEE_Obj['EndDate'];
				if($__startDate == '0000-00-00' || $__startDate == ''){
					$__displayDate = '--';
				}
				else if($__endDate == '0000-00-00'){
					$__displayDate = $__startDate;
				}
				else{
					$__displayDate = $__startDate.' - '.$__endDate;
				}
				
				if($__OLEE_Obj['Organization'] != ''){
					$__displayOrganization = '<br>'.$__OLEE_Obj['Organization'];
				}
				else{
					$__displayOrganization = '';
				}
				
				$OLEE_table .='			
						<tr '.$contentRowStyle.' '.$_bkg_color.'>
							<td width="'.$OLEE_col1_space_width.'mm"></td>
							<td width="'.$OLEE_col1_width.'mm"><font color="#10253F">'.$__OLEE_Obj['Title'].$__displayOrganization.'</font></td>
							<td width="'.$OLEE_col2_width.'mm"><font color="#10253F">'.$__displayDate.'</font></td>
							<td width="'.$OLEE_col3_width.'mm"><font color="#10253F">'.$__OLEE_Obj['Role'].'</font></td>
						</tr>';
			}
			$OLEE_table .='</tbody>';
			$OLEE_table .='</table>';	
		}
	}
	else{
		$OLEE_table = '';
	}
	$section6 = $OLEE_table;
	############################################################################################################
	################################ Section 6 - OLE external (End) ############################################
	############################################################################################################
	
	############################################################################################################
	################################ Section 7 - Self Account (Start) ##########################################
	############################################################################################################
	if($displaySelfAccount){
		$SelfAccountContents = $objPrinting->getSelfAccount();
		$self_account = '
				<table width="'.$contentWidth.'mm">
						<tr style="line-height: 175%;">
							<td bgcolor="#D6DFEC"><font size="'.$header_size.'"><b>Student\'s \'Self-Account\' 學生的自述</b></font></td>
						</tr>
						<tr style="line-height: '.$header_below_space.';">
							<td></td>
						</tr>
						<tr>
							<td width="'.$contentWidth.'mm"><font color="#10253F">'.$SelfAccountContents[0]['Details'].'</font></td>
						</tr>
				</table>';
		
		$section7 = $self_account;
	}
	############################################################################################################
	################################# Section 7 - Self Account (End) ###########################################
	############################################################################################################

//	echo $section1.$section2.$section3.$section4.$section5.$section6;
	// Write to PDF
	// Start First Page Group
	$pdf->startPageGroup();
	$pdf->set_Page_Header('cover');
	$pdf->SetMargins($leftMargins, 78, $rightMargins);
	$pdf->AddPage();
	$pdf->SetFont('arialunicid0', '', 10);
	$pdf->writeHTML($section1, true, false, true, false, '');
	$pdf->writeHTML($chopAndSign, true, false, true, false, '');
	$pdf->endPage();
	
	$pdf->set_Page_Header('content');
	$pdf->SetMargins($leftMargins, 24, $rightMargins);
	$pdf->AddPage();
	$pdf->SetFont('arialunicid0', '', 10);
	
	if($section2!=''){
	$pdf->writeHTML($section2, true, false, true, false, '');
	}
	if($section3!=''){
	$pdf->writeHTML($section3, true, false, true, false, '');
	}
	if($section4!=''){
	$pdf->writeHTML($section4, true, false, true, false, '');
	}
	if($section5!=''){
	$pdf->set_special_header($OLE_header); // for showing Title if there is page break (auto page break thead used by the year)
	$pdf->SetMargins($leftMargins, 40, $rightMargins);
	$pdf->writeHTML($section5, true, false, true, false, '');
	$pdf->set_special_header('');
	$pdf->SetMargins($leftMargins, 24, $rightMargins);
	}
	$pdf->endPage();
	if($section6!=''){
	$pdf->AddPage();
	$pdf->set_special_header($OLEE_header);
	$pdf->SetMargins($leftMargins, 44, $rightMargins);
	$pdf->writeHTML($section6, true, false, true, false, '');
	$pdf->set_special_header('');
	$pdf->SetMargins($leftMargins, 24, $rightMargins);
	}
	$pdf->endPage();
		
	if($displaySelfAccount){
		$pdf->AddPage();
		$pdf->SetFont('arialunicid0', '', 10);
		$pdf->writeHTML($section7, true, false, true, false, '');
		$pdf->endPage();
	}
	
	unset($StudentInfoArr);
	unset($student_header);
	unset($formAry);
	unset($assessmentReportInfo);
	unset($classHistoryInfoAry);
	unset($SchoolSubjectInfo);
	unset($assessment_report);
	unset($OLE_Records);
	unset($OLE_table);
	unset($StudentAwards);
	unset($awards_table);
	unset($OLEE_Records);
	unset($OLEE_table);
	unset($SelfAccountContents);
	unset($self_account);
	unset($section1);
	unset($section2);
	unset($section3);
	unset($section4);
	unset($section5);
}//End of a student

//$ProcessTime = round(StopTimer(2, true, "GLB_StartTime"), 4);

//Close and output PDF document
//$pdf->Output('ProcessTime_'.$ProcessTime.'_MemoryUsage_'.number_format((memory_get_usage())/1024/1024, 1).'MB.pdf', 'I');
$pdf->Output('srlc_slp.pdf', 'I');
//============================================================+
// END OF FILE
//============================================================+

?>