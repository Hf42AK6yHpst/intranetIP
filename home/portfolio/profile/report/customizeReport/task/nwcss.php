<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/nwcss/libpf-slp-RptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/nwcss/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic.php");

$YearClassID = trim($YearClassID);
$issuedate  = trim($issuedate);


//debug_r($_POST);

//YearClassID

$objReportMgr = new stcecRptMgr();

$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();

$lpf_report = new libpf_report();
list($SubjectIDNameAssoArr, $StudentFormInfoArr, $StudentAcademicResultInfoArr, $SubjectInfoOrderedArr) = $lpf_report->GET_ACADEMIC_PERFORMANCE_SLP_IN_ID($studentList);

$libpf_academic = new libpf_academic();
$FullMarkArr = $libpf_academic->Get_Subject_Full_Mark();

for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	
	$_studentId = $studentList[$i];

//$StudentAcademicResultInfoArr[$_studentId][$thisSubjectID][$thisAcademicYearID]
	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	$objStudentInfo -> setAcademicYear($academicYearID);
	$objStudentInfo -> setAcademicYearStr();
	$objStudentInfo -> setYearClassID($YearClassID);
	
	$objStudentInfo -> setAll_AcademicYearID($All_AcademicYearID);

	$OLEInfoArrINT = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='INT',$Selected=true);
	$OLEInfoArrEXT = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='EXT',$Selected=true);

	### Page Content
	$html .=$objStudentInfo->getEmptyTable('3%');
	$html .= $objStudentInfo->getPart1_HTML();	
	$html .= $objStudentInfo->getPart2_HTML();	
	$html .=$objStudentInfo->getEmptyTable('1%');
	$html .= $objStudentInfo->getPart3_HTML($SubjectInfoOrderedArr,$StudentAcademicResultInfoArr[$_studentId],$FullMarkArr);	
	$html .=$objStudentInfo->getPagebreakTable();
	$html .=$objStudentInfo->getEmptyTable('3%');
	$html .=$objStudentInfo->getSchoolHeader(true,'2');
	$html .= $objStudentInfo->getPart4_HTML($OLEInfoArrINT);	
	$html .=$objStudentInfo->getPagebreakTable();	
	
	$html .=$objStudentInfo->getEmptyTable('3%');
	$html .=$objStudentInfo->getSchoolHeader(true,'3');
	$html .= $objStudentInfo->getPart5_HTML();	
	$html .=$objStudentInfo->getEmptyTable('3%');
	$html .= $objStudentInfo->getPart6_HTML($OLEInfoArrEXT);	
	$html .=$objStudentInfo->getPagebreakTable();
	
	$html .=$objStudentInfo->getEmptyTable('3%');
	$html .=$objStudentInfo->getSchoolHeader(true,'4');
	
	$html .=$objStudentInfo->getPart7_HTML();

	
	if($i!=$i_max-1)
	{
		$html .=$objStudentInfo->getPagebreakTable();
	}
	
}


if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}


exit();
?>