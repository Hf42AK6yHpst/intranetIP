<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/stmc/libpf-slp-stmcRptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/stmc/libpf-slp-stmc.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
//include_once($PATH_WRT_ROOT."file/reportcard2008/templates/sha_tin_methodist.css");


$YearClassID = trim($YearClassID);
$issuedate  = trim($issuedate);


//debug_r($_POST);



$objReportMgr = new stmcRptMgr();


//$lpf_report = new libpf_report();
////list($SubjectIDNameAssoArr, $StudentFormInfoArr, $StudentAcademicResultInfoArr, $SubjectInfoOrderedArr) = $lpf_report->GET_ACADEMIC_PERFORMANCE_SLP_IN_ID($StudentID);
//
////debug_r($SubjectInfoOrderedArr);

$css_link_html = $PATH_WRT_ROOT."file/reportcard2008/templates/sha_tin_methodist.css"; 
$html = '<link rel="stylesheet" type="text/css" href="'.$css_link_html.'" />';

for($i = 0,$i_max = sizeof($StudentID);$i < $i_max; $i++){
	
	$_studentId = $StudentID[$i];

	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	$objStudentInfo -> setAcademicYear($academicYearID);
	$objStudentInfo -> setAcademicYearStr();
	$objStudentInfo -> setYearRange();
	

	$OLEInfoArr_INT = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='INT',$Selected=false);
	$OLEInfoArr_EXT = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='EXT',$Selected=false);

	### Page Content

	$html .= $objStudentInfo->getPart3_HTML();	
	$html .= $objStudentInfo->getPart4_HTML($OLEInfoArr_INT);
	$html .= $objStudentInfo->getPart5_HTML();
	$html .= $objStudentInfo->getPart6_HTML($OLEInfoArr_EXT);
	$html .= $objStudentInfo->getPart7_HTML();

	
	if($i!=$i_max-1)
	{
		$html .=$objStudentInfo->getPagebreakTable();
	}

}


if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}


exit();
?>