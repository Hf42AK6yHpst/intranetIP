<?php

include_once($PATH_WRT_ROOT."includes/portfolio25/customize/yttmc/libpf-slp-RptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/yttmc/libpf-slp-RptPDFGenerator_TCPDF.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/yttmc/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic.php");

$YearClassID = trim($YearClassID);
$issuedate  = trim($issuedate);
$NoOfFirstPage = trim($NoOfFirstPage);
$NoOfOtherPage = trim($NoOfOtherPage);

//debug_r($_POST);

//YearClassID

### Basic PDF Settings
$pdf = new StudentReportPDFGenerator_TCPDF();
$left_margin = 7;
$right_margin = 7;
$header_blank_height = 46;
$footer_blank_height = 7;
$header_height = 86;
$footer_height = 30;

$objReportMgr = new RptMgr();

$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();


for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	
	$_studentId = $studentList[$i];

	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	$objStudentInfo -> setAcademicYear($academicYearID);
	$objStudentInfo -> setAcademicYearStr();
	$objStudentInfo -> setYearRange();
	$objStudentInfo -> setYearClassID($YearClassID);
	
	if($NoOfFirstPage!='')
	{
		$objStudentInfo -> setNoOfFirstPage($NoOfFirstPage);
	}
	if($NoOfOtherPage!='')
	{
		$objStudentInfo -> setNoOfOtherPage($NoOfOtherPage);
	}

	$OLEInfoArrMembership = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='INT',$Selected=true,$isMember=true);
	$OLEInfoArrActivity = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='INT',$Selected=true,$isMember=false);

	### Page Content
	$html .=$objStudentInfo->getEmptyTable('3%');
	$html .= $objStudentInfo->getPart1_HTML();	
	$html .=$objStudentInfo->getEmptyTable('3%');
	$html .= $objStudentInfo->getPart2_HTML();	
	$html .=$objStudentInfo->getEmptyTable('4%');
	$html .= $objStudentInfo->getPart3_HTML($OLEInfoArrMembership);	
	$html .=$objStudentInfo->getEmptyTable('4%');
	$html .= $objStudentInfo->getPart4_HTML($OLEInfoArrActivity);	
	
	$html .=$objStudentInfo->getEmptyTable('4%');
	$html .= $objStudentInfo->getPart5_HTML();	
	
	/////////////////////
	$libuserObj = new libuser($_studentId);
	$FormYear = $libuserObj->Get_User_Studying_Form();
	if(preg_replace('/[^0-9]+/','',$FormYear[0]['FormWebSAMSCode'])==6) {
		$html .=$objStudentInfo->getPagebreakTable();
		$html .=$objStudentInfo->getPart6_HTML();
	}
	/////////////////////
	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .= $objStudentInfo->getSignature_HTML();
	
	if($i!=$i_max-1)
	{
		$html .=$objStudentInfo->getPagebreakTable();
	}

}


if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}
else if($PrintType == "pdf"){
	$pdf->writeHTML($html);
	$pdf->set_is_last_page(true);
	
	$report_content = $pdf->Output(null, 'S');
	$report_name = 'student_learning_profile.pdf';
	output2browser($report_content, $report_name);
}
else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}


exit();
?>