<?php
ini_set('memory_limit','128M');
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/twghwfns/libpf-slp-twghwfns.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

//StartTimer("GLB_StartTime");

//content Ready
if(!empty($StudentID)){
	$studentList = array($StudentID);
}

if(empty($issuedate)){
	$issuedate = date("d-m-Y");
}
else{
	$issuedate = date("d-m-Y" , strtotime($issuedate));
}

if (!isset($studentList))
{
	// If no studentIDs retrieve from classname
	$objReportMgr = new ipfReportManager();
	$objReportMgr->setStudentID($StudentID);
	$objReportMgr->setYearClassID($YearClassID);
	$academicYearID = Get_Current_Academic_Year_ID();
	$objReportMgr->setAcademicYearID($academicYearID);
	$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

	$studentList = $objReportMgr->getStudentIDList($ActivatedOnly=true);

}

$academicYearID = Get_Current_Academic_Year_ID(); // need to change

$objPrinting = new objPrinting(); // class from libpf-slp-css.php
$objPrinting->setYearClassID($YearClassID);
$objPrinting->setAcademicYear($academicYearID);
$objPrinting->setStudentList($studentList);
$objPrinting->contentReady();

$lclass = new libclass();
$classTeacherArr = $lclass->returnClassTeacherByClassID(array($YearClassID));
$cTeacherDisplay = '';
$lineBreak = '';
foreach((array)$classTeacherArr as $classTeacherInfo ){
	$cTeacherDisplay .= $lineBreak;
	$cTeacherDisplay .= $classTeacherInfo['CTeacherCh'];
	$cTeacherDisplay .= ' ';
	$cTeacherDisplay .= $classTeacherInfo['CTeacherEn'];
	$lineBreak = '<br>';
}

//start creating PDF
//new mPDF ($mode,$format,$default_font_size,$default_font,$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer,$orientation)
//$margin_left = 20; //mm
//$margin_right = 20;
$margin_top = 20;
$margin_bottom = $_POST['customField'] === '' ? 20: $_POST['customField'];
//$margin_header = 12;
//$margin_footer = 8;
$pdf = new mPDF('','A4',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer);

//pdf metadata setting
$pdf->SetTitle('東華三院黃笏南中學 SLP');
$pdf->SetAuthor('BroadLearning Education (Asia) Ltd.');
$pdf->SetCreator('BroadLearning Education (Asia) Ltd.');
$pdf->SetSubject('學生學習概覽 Student Learning Profile');
$pdf->SetKeywords('東華三院黃笏南中學', 'eClass, iPortfolio, SLP');

// Chinese use mingliu 
$pdf->backupSubsFont = array('mingliu');
$pdf->useSubstitutions = true;

// school info
$schoolInfoArr['Name']['Ch'] = '東華三院黃笏南中學';
$schoolInfoArr['Name']['En'] = 'Tung Wah Group Of Hospitals Wong Fut Nam College';
$schoolInfoArr['Phone'] = '852-23369151';
// $schoolInfoArr['Email'] = 'twghwfns@eservices.hkedcity.net';
$schoolInfoArr['Email'] = 'wfnss@tungwah.org.hk';
$schoolInfoArr['Website'] = 'http://www.twghwfns.edu.hk';
$schoolInfoArr['Code'] = '170089';
$schoolInfoArr['Address']['Ch'] = '香港 九龍塘 牛津道1號C';
$schoolInfoArr['Address']['En'] = '1C Oxford Road, Kowloon Tong, Hong Kong.';

//get header, footer, css stylesheet
$style = $objPrinting->returnStyleSheet();

$EmptyString = $objPrinting->EmptyString;

//css stylesheet
$pdf->WriteHTML($style);
$pdf->DefHTMLHeaderByName('NoHeader','');

//Looping studentList
$numStudent = count($studentList);
for($y = 0; $y<$numStudent ; $y ++){
	// Change student
	$_studentid = $studentList[$y];
	$objPrinting->setStudent($_studentid);
	
	
	// Get All Content
	$StudentInfoArr = $objPrinting->getStudentInfo();
	$StudentPAArr = $objPrinting->getStudentPA($StudentInfoArr['WebSAMSRegNo']);
	$PAHeader = $objPrinting->getStudentPAHeader();
	$ARArr = $objPrinting->getStudentAssessmentResult();
	//debug_pr($ARArr);
	$teacherCommentArr = $objPrinting->getStudentComment();
	$internalActivity_AssoArr = $objPrinting->GetOLE('INT','OLE'); //School based activity
	$num_internalActivity = count($internalActivity_AssoArr);
	$externalActivity_AssoArr = $objPrinting->GetOLE('EXT','OLE'); //External activity
	$num_externalActivity = count($externalActivity_AssoArr);
	$selfAccountArr = $objPrinting->getSelfAccount();
	$currentAY = getCurrentAcademicYear();
	if($filename_ClassName== ''){
		$filename_ClassName = $StudentInfoArr['ClassName'];
		if($numStudent == 1){
			$filenameSuffix = $filename_ClassName.$StudentInfoArr['ClassNumber'];
		}
		else{
			$filenameSuffix = $filename_ClassName;
		}
	}
	
	$header = '<div class="header_wrapper">
				<div height="10mm">&nbsp;</div>
				<div class="font_1_5 alignRight">'.$currentAY.' / SLP / '.$StudentInfoArr['EnglishName'].' / P.{PAGENO} of {nbpg}</div>
				<div class="font_2 alignRight">發出日期 Date of Issue: '.$issuedate.'</div>
				</div>';
				
	$cover_page = '<div class="page_wrapper">
				<div height="15mm">&nbsp;</div>
				<div class="alignCenter"><img src="'.$PATH_WRT_ROOT.'file/customization/twghwfns/school_logo.png" height="72mm"/></div>
				<br />
				<div class="alignCenter fontTitle1"><font style="font-family:mingliu;">東華三院黃笏南中學</font><br />TUNG WAH GROUP OF HOSPITALS<br />WONG FUT NAM COLLEGE</div>
				<br /><br /><br /><br /><br />
				<div class="alignCenter fontTitle2">學生學習概覽<br />Student Learning Profile</div>
				<br /><br /><br /><br /><br />
				<div class="alignCenter">
					<table class="border_1">
						<tr>
							<td class="border_1" width="20%">中文姓名:<br />Chinese Name:</td>
							<td class="border_1 alignCenter" width="30%">'.$StudentInfoArr['ChineseName'].'</td>
							<td class="border_1" width="20%">英文姓名:<br />English Name:</td>
							<td class="border_1 alignCenter" width="30%">'.$StudentInfoArr['EnglishName'].'</td>
						</tr>
					</table>
				</div>
				</div>';
	
	$StudentInfoHTML = '<div class="page_wrapper">
						<table class="border_2">
							<tr>
								<td class="border_2 font_3" colspan="4">學生資料 Student Particulars</td>
							</tr>
							<tr>
								<td class="font_2">學生姓名<br />Student Name:</td>
								<td class="font_2">'.$StudentInfoArr['ChineseName'].'<br />'.$StudentInfoArr['EnglishName'].'</td>
								<td class="font_2">身份證號碼<br />ID No.:</td>
								<td class="font_2">'.displayHKID($StudentInfoArr['HKID']).'</td>
							</tr>
							<tr>
								<td class="font_2">出生日期<br />Date of Birth:</td>
								<td class="font_2">'.$StudentInfoArr['DateOfBirth'].'</td>
								<td class="font_2">性別<br />Sex:</td>
								<td class="font_2">'.$StudentInfoArr['Gender'].'</td>
							</tr>
							<tr>
								<td class="font_2">入學日期<br />Date of Admission:</td>
								<td class="font_2">'.$StudentInfoArr['AdmissionDate'].'</td>
								<td class="font_2">學校編號<br />School Code:</td>
								<td class="font_2">'.$schoolInfoArr['Code'].'</td>
							</tr>
							<tr>
								<td class="font_2">學校地址<br />School Address:</td>
								<td class="font_2">'.$schoolInfoArr['Address']['Ch'].'<br />'.$schoolInfoArr['Address']['En'].'</td>
								<td class="font_2">學校電話<br />School Phone: </td>
								<td class="font_2">'.$schoolInfoArr['Phone'].'</td>
							</tr>
							<tr>
								<td class="font_2">學校電郵<br />School Email:</td>
								<td class="font_2">'.$schoolInfoArr['Email'].'</td>
								<td class="font_2">學校網址<br />School Web Site:</td>
								<td class="font_2">'.$schoolInfoArr['Website'].'</td>
							</tr>
						</table>
						<br />
						<table class="border_2">
							<tr>
								<td class="border_2 font_3" colspan="4">校內學科成績 Academic Performance in School</td>
							</tr>';
							
							$numYear = count($ARArr['Year']);
							if($numYear!=0){
								$col_width = 66/$numYear;
							}
							else{
								$col_width = 66;
							}
	$StudentInfoHTML .= '<tr>
								<td class="font_2b" width="33%">科目<br />Subject</td>';
	
	for($x = 0 ; $x <$numYear;$x ++){
		$StudentInfoHTML .= 		'<td width="'.$col_width.'%" class="font_2b alignCenter">'.$ARArr['Year'][$x]['AcademicYear'].'<br />'.$objPrinting->getDisplayFormName($ARArr['Year'][$x]['ClassName']).'<br />校內表現<br />Mark / Performance<br />in School</td>';	
	}
//								<td class="font_2b alignCenter">'.$ARArr['Year'][1]['AcademicYear'].'<br />'.$objPrinting->getDisplayFormName($ARArr['Year'][1]['ClassName']).'<br />校內表現<br />Mark / Performance<br />in School</td>
//								<td class="font_2b alignCenter">'.$ARArr['Year'][2]['AcademicYear'].'<br />'.$objPrinting->getDisplayFormName($ARArr['Year'][2]['ClassName']).'<br />校內表現<br />Mark / Performance<br />in School</td>
	$StudentInfoHTML .=	'</tr>';
							foreach((array)$ARArr['SchoolSubject'] as $subjectInfo){
								$_subjectId = $subjectInfo['RecordID'];
								$_subjectResultArr = $ARArr['AssessmentResult'][$_subjectId];
								if(!empty($_subjectResultArr)){
									$StudentInfoHTML .= '<tr>
															<td class="font_2">'.$subjectInfo['SubjectDisplayNameChi'].'<br />'.$subjectInfo['SubjectDisplayName'].'</td>';
										for($_x =0 ;$_x<$numYear; $_x++ ){
											$__academicYearID = $ARArr['Year'][$_x]['AcademicYearID'];
// 											$StudentInfoHTML .= '<td class="font_2 alignCenter">'.(!empty($_subjectResultArr[$__academicYearID]['Score'])?$_subjectResultArr[$__academicYearID]['Score'] :$_subjectResultArr[$__academicYearID]['Grade'] ).'</td>';
											// displayScore() is in libpf-slp-twghwfns
											$StudentInfoHTML .= '<td class="font_2 alignCenter">'.displayScore($_subjectResultArr[$__academicYearID]).'</td>';
										}
									$StudentInfoHTML .= '</tr>';
								}
							}
	$StudentInfoHTML .= '</table>
						<table>
							<tr>
								<td class="font_1 alignLeft Padding3">滿分 Full Mark: 100</td>
								<td class="font_1 noPadding"></td>
								<td class="font_1 alignRight Padding2">(): 不及格 Fail</td>
							</tr>
						</table>
						</div>';

	$PA_TeacherComment_HTML = '<div class="page_wrapper">
								<table class="border_2">
									<tr><td class="border_2 font_3" colspan="2">個人與一般能力 Personal and General Abilities</td></tr>
									<tr>
										<td class="border_2 font_2b">項目 Items</td>
										<td class="border_2 font_2b alignCenter" width="20%">等級 Scale</td>
									</tr>';
	$numAbility = count($PAHeader);
	for($i=3; $i<$numAbility; $i++ ){
		$PA_TeacherComment_HTML .= 	'<tr>
										<td class="border_2 font_2">'.$PAHeader[$i].'</td>
										<td class="border_2 font_2 alignCenter">'.$StudentPAArr[$i].'</td>
									</tr>';
	}
	$PA_TeacherComment_HTML .=	'<tr>
										<td class="border_2" colspan="2">
											<table>
												<tr>
													<td class="font_2">等級 Scale</td>
													<td class="font_2"></td>
												</tr>
												<tr>
													<td class="font_2">1&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;優異 Excellent</td>
													<td class="font_2">4&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;稍遜 Below average</td>
												</tr>
												<tr>
													<td class="font_2">2&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;良好 Good</td>
													<td class="font_2">0&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;不能判斷 Unable to judge</td>
												</tr>
												<tr>
													<td class="font_2">3&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;一般Average</td>
													<td class="font_2"></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<br />
								<table class="border_2">
									<tr><td class="border_2 font_3" >評語 Remarks</td></tr>
									<tr><td class="font_2"><br /><br />'.$teacherCommentArr['CommentChi'].'<br />&nbsp;<br />&nbsp;</td></tr>
								</table>
								<br />
								<table>
									<tr>
										<td class="bottom_border_2" width="52mm" height="29mm"></td>
										<td></td>
										<td class="bottom_border_2" width="52mm"></td>
									</tr>
									<tr>
										<td class="alignCenter noPadding font_2" valign="top">'.$cTeacherDisplay.'<br>'.'班主任 Class Teacher'.'</td>
										<td></td>
										<td class="alignCenter noPadding font_2" valign="top">'.'李靖邦'.'&nbsp;'.'LEE CHING PONG'.'<br>'.'校長 Principal</td>
									</tr>
								</table>
								</div>';
	$tableWidth = array('24%','10%','13%','16%','16%','20%');
	$IntOLEHTML = '<div class="page_wrapper">
					<table class="border_2">
					<tr>
						<td class="font_3">其他學習經歷 Other Learning Experiences</td>
					</tr>
					<tr>
						<td class="font_2">
							<u>其他學習經歷的有關資料，須由學校確認。</u>其他學習經歷可透過由學校舉辦或學校與校外機構合辦的學習活動獲得，包括在上課時間表以內及/或以外的學習時間進行的有關學習經歷。除核心及選修科目外，在高中學習階段的其他學習經歷，尚包括德育及公民教育、藝術發展、體育發展、社會服務及與工作有關的經驗。<br />
							<u>Information about Other Learning Experiences must be validated by school.</u> Other Learning Experiences can be achieved
							through programmes organised by the school or co-organised by the school with outside organisations. They may include
							learning experiences implemented during time-tabled and/or non-time-tabled learning time. Apart from core and elective
							subjects, Other Learning Experiences that the student participates in during his/her senior secondary education include
							Moral and Civic Education, Aesthetic Development, Physical Development, Community Service and Career-related
							Experiences.
						</td>
					</tr>
					</table>
					<br />
					<table class="border_2">
						<thead>
							<tr>
								<td width="'.$tableWidth[0].'" class="border_1 font_1b alignCenter">活動項目(及簡介)*<br />Programmes (with description)*</td>
								<td width="'.$tableWidth[1].'" class="border_1 font_1b alignCenter">學年<br />School<br />Year</td>
								<td width="'.$tableWidth[2].'" class="border_1 font_1b alignCenter">參與角色<br />Role of<br />Participation</td>
								<td width="'.$tableWidth[3].'" class="border_1 font_1b alignCenter">合辦機構(如有)<br />Partner<br />Organizations<br />(if any)</td>
								<td width="'.$tableWidth[4].'" class="border_1 font_1b alignCenter">其他學習經歷種類<br />Components of Other<br />Learning Experiences</td>
								<td width="'.$tableWidth[5].'" class="border_1 font_1b alignCenter">獎項/証書文憑/成就** (如有)<br />Awards/Certifications/<br />Achievements ** (if any)</td>
							</tr>
						</thead>
						<tbody>';
						foreach((array)$internalActivity_AssoArr as $_InfoArr){
							
				$IntOLEHTML .= '<tr>
									<td class="border_1 font_1">'.$objPrinting->display2Lang($_InfoArr['Title']).'</td>
									<td class="border_1 font_1 alignCenter" rowspan="2">'.$_InfoArr['AcademicYear'].'</td>
									<td class="border_1 font_1 alignCenter" rowspan="2">'.$objPrinting->display2Lang($_InfoArr['Role']).'</td>
									<td class="border_1 font_1 alignCenter" rowspan="2">'.$_InfoArr['Organization'].'</td>
									<td class="border_1 font_1 alignCenter" rowspan="2">'.$objPrinting->getELEdisaply($_InfoArr['ELE']).'</td>
									<td class="border_1 font_1 alignCenter" rowspan="2">'.$_InfoArr['Achievement'].'</td>
								</tr>
								<tr>
									<td class="border_1 font_1">'.$_InfoArr['Details'].'</td>';
						}
				$IntOLEHTML .= '</tr>
						</tbody>
					</table>';
					$IntOLEHTML .= '<table style="page-break-inside: avoid;"><tr>
											<td class="font_1" colspan="5">
												*&nbsp;&nbsp;本部分除了介紹相關的活動外，也可概略地述說學生透過參與該活動所發展得來的知識、共通能力、價值觀及態度。
												<br />&nbsp;&nbsp;&nbsp;Apart from explaining what the activity is about, the description also shows briefly what kinds of knowledge, generic skills, values and attitudes would be developed through the experience.
											</td>
										</tr>
										<tr>
											<td class="font_1" colspan="5">
												**&nbsp;有需要時可提供 獎項 / 証書文憑 / 成就 作証明。
												<br />&nbsp;&nbsp;&nbsp;Evidence of awards/ certifications/ achievements listed is available for submission when required.
											</td>
										</tr>
										<tr>
											<td class="font_1" colspan="5">
												上表只展示了學生在高中階段的學習經歷，並不需要徹底地列出所有曾參與過的經歷。
												<br />The above list, which does not mean to be exhaustive, merely illustrates the \'key\' learning experiences acquired by the student throughout the senior secondary years.
											</td>
										</tr></table>';
				$IntOLEHTML .= '</div>';
	
	$tableWidth = array('24%','13%','18%','18%','27%');
	$ExtOLEHTML = '<div class="page_wrapper">
					<table class="border_2">
					<tr>
						<td class="font_3">校外的表現 / 獎項 Performance / Awards Gained Outside School</td>
					</tr>
					<tr>
						<td class="font_2">
							學生可向學校提供一些在高中階段曾參與過而並非由學校舉辦的學習活動資料。學校<u>不</u>須確認學生的參與資料。在有需要時，學生須負全責向相關人仕提供適當証明。<br />
							For learning programmes not organized by the school during the senior secondary education, student should provide the
							information to the school. It is <u>not</u> necessary for the school to validate information below. Student should hold full
							responsibility to provide evidence to relevant people when requested.
						</td>
					</tr>
					</table>
					<br />
					<table class="border_2">
						<thead>
							<tr>
								<td width="'.$tableWidth[0].'" class="border_1 font_1b alignCenter">活動項目(及簡介)*<br />Programmes (with description)*</td>
								<td width="'.$tableWidth[1].'" class="border_1 font_1b alignCenter">學年<br />School<br />Year</td>
								<td width="'.$tableWidth[2].'" class="border_1 font_1b alignCenter">參與角色<br />Role of<br />Participation</td>
								<td width="'.$tableWidth[3].'" class="border_1 font_1b alignCenter">主辦機構<br />Organizations<br />(if any)</td>
								<td width="'.$tableWidth[4].'" class="border_1 font_1b alignCenter">獎項/証書文憑/成就** (如有)<br />Awards/Certifications/<br />Achievements ** (if any)</td>
							</tr>
						</thead>
						<tbody>';
						foreach((array)$externalActivity_AssoArr as $_InfoArr){
				$ExtOLEHTML .= '<tr>
									<td class="border_1 font_1">'.$objPrinting->display2Lang($_InfoArr['Title']).'</td>
									<td class="border_1 font_1 alignCenter" rowspan="2">'.$_InfoArr['AcademicYear'].'</td>
									<td class="border_1 font_1 alignCenter" rowspan="2">'.$objPrinting->display2Lang($_InfoArr['Role']).'</td>
									<td class="border_1 font_1 alignCenter" rowspan="2">'.$_InfoArr['Organization'].'</td>
									<td class="border_1 font_1 alignCenter" rowspan="2">'.$_InfoArr['Achievement'].'</td>
								</tr>
								<tr>
									<td class="border_1 font_1">'.$_InfoArr['Details'].'</td>
								</tr>';
						}
		$ExtOLEHTML .= '</tbody>
					</table>';
					$ExtOLEHTML .= '<table style="page-break-inside: avoid;"><tr>
											<td class="font_1" colspan="5">
												*&nbsp;有需要時可提供 獎項 / 証書文憑 / 成就 作証明。
												<br />&nbsp;&nbsp;&nbsp;Evidence of awards/ certifications/ achievements listed is available for submission when required.
											</td>
										</tr>
										<tr>
											<td class="font_1" colspan="5">
												上表只展示了學生在高中階段的學習經歷，並不需要徹底地列出所有曾參與過的經歷。
												<br />The above list, which does not mean to be exhaustive, merely illustrates the \'key\' learning experiences acquired by the student throughout the senior secondary years.
											</td>
										</tr></table>';
		$ExtOLEHTML .= '</div>';
	
	// Self Account
	$selfAccountHTML = '<div class="page_wrapper">
	<table class="border_2" height="230mm">
		<tr><td class="font_3">學生的自述 Student\'s \'Self-Account\'</td></tr>
		<tr>
			<td class="font_2 bottom_border_1">
			學生可於本欄提供額外資料，重點描述其在高中或以前的學習階段中的學習生活及個人發展方面的情況，以便其他人士(例如各大專院校及未來僱主等)參考。
			<br />
			In this column, student could provide additional information to highlight any aspects of his/her learning life and personal
			development during or before the senior secondary education for readers’ (e.g. tertiary education institutions, future
			employers) references.
			</td>
		</tr>
		<tr><td class="font_2" height="200mm" style="vertical-align:top;"><br />'.$selfAccountArr['Details'].'</td></tr>
	</table>
	<div class="alignCenter font_2b"><br/>完<br/>End of Report</div>
	</div>';
	
	$normal_pagebreak = '<pagebreak />';
	$pdf->WriteHTML($cover_page);
	$pdf->DefHTMLHeaderByName('htmlHeader',$header);
	$pdf->SetHTMLHeaderByName('htmlHeader');
	$pdf->writeHTML('<pagebreak resetpagenum="1"/>');
	$pdf->WriteHTML($StudentInfoHTML);
	$pdf->writeHTML($normal_pagebreak);
	$pdf->WriteHTML($PA_TeacherComment_HTML);
	$pdf->writeHTML($normal_pagebreak);
	$pdf->WriteHTML($IntOLEHTML);
	$pdf->writeHTML($normal_pagebreak);
	$pdf->WriteHTML($ExtOLEHTML);
	$pdf->writeHTML($normal_pagebreak);
	$pdf->WriteHTML($selfAccountHTML);
	
	$StudentInfoArr = $objPrinting->getStudentInfo();
	$StudentPAArr = $objPrinting->getStudentPA($StudentInfoArr['WebSAMSRegNo']);
	$PAHeader = $objPrinting->getStudentPAHeader();
	$ARArr = $objPrinting->getStudentAssessmentResult();
	$teacherCommentArr = $objPrinting->getStudentComment();
	$internalActivity_AssoArr = $objPrinting->GetOLE('INT','OLE'); //School based activity
	$num_internalActivity = count($internalActivity_AssoArr);
	$externalActivity_AssoArr = $objPrinting->GetOLE('EXT','OLE'); //External activity
	$num_externalActivity = count($externalActivity_AssoArr);
	$selfAccountArr = $objPrinting->getSelfAccount();
	
	unset($StudentInfoArr);
	unset($StudentPAArr);
	unset($PAHeader);
	unset($ARArr);
	unset($teacherCommentArr);
	unset($internalActivity_AssoArr);
	unset($externalActivity_AssoArr);
	unset($selfAccountArr);
	
	// Last Page no need to add pagebreak
	if($y != ($numStudent-1)){
		$pdf->SetHTMLHeaderByName('NoHeader');
		$pdf->writeHTML('<pagebreak resetpagenum="1"/>');
	}
}

//$ProcessTime = round(StopTimer(2, true, "GLB_StartTime"), 4);
//$filenameSuffix = '_'.$StudentInfoArr['ClassName']; 
$pdf->Output('SLP'._.$filenameSuffix.'.pdf', 'I');
//$pdf->Output('ProcessTime_'.$ProcessTime.'_MemoryUsage_'.number_format((memory_get_usage())/1024/1024, 1).'MB.pdf', 'I');
?>