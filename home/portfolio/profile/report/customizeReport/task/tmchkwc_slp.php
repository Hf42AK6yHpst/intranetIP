<?
/*
 * 	Log
 *
 *  2019-07-04 Cameron
 *      - call getPerformance_BTP_IC for multiple awards for a student in the same activity [case #J164242] 
 *      
 *  2018-05-15 Cameron
 *      - print school logo and student info in header for odd page
 *      
 *	2018-01-19 Cameron
 *		- fix $academic_start_year and $academic_end_year (missing empty parameter)
 *
 *	2017-06-19 Cameron
 *		- unlimit memory size, unlimit time (mpdf defult 30 sec timeout) [case #J118828]
 *
 *	2017-03-07 Cameron
 *		- don't show 2nd term for Form 6 student
 *
 *	2017-03-06 Cameron
 *		- add blank page for odd page
 *
 * 	2017-01-13 Cameron
 * 		- set useSubstitutions = false to avoid Chinese problem when use css of page-break-inside: avoid
 * 
 * 	2016-12-13 Cameron
 * 		- create this file
 */
 
ini_set('memory_limit',-1);	// original set 128M
set_time_limit(0);			// unlimit
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/libpf-slp-tmchkwc.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

// issue date
if(empty($issuedate)){
	$tempIssueDate = date("Y-m-d");
	$issuedate = date("d/m/Y");
}
else{
	$tempIssueDate = date("Y-m-d" , strtotime($issuedate));
	$issuedate = date("d/m/Y", strtotime($issuedate));
}
$issueAYInfo = getAcademicYearAndYearTermByDate($tempIssueDate);
$issueAYName = $issueAYInfo['AcademicYearName'];

$academicYearID = $academicYearID ? $academicYearID[0] : Get_Current_Academic_Year_ID();
$academic_start_year = date('Y',getStartOfAcademicYear('',$academicYearID));
$academic_end_year = date('Y',getEndOfAcademicYear('',$academicYearID));
$terms = getAllSemesterByYearID($academicYearID);
$academicYearTerms = BuildMultiKeyAssoc($terms,'YearTermID','TermTitle',1);

// target student
if(!empty($StudentID)){
	$studentList = array($StudentID);
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	$luser = new libuser($StudentID);
	$luser->LoadUserData($StudentID);
	$className = $luser->ClassName;
	$classNumber = $luser->ClassNumber;	
	unset($luser);
	$filename = $issueAYName.'_'.$className.'-'.$classNumber.'_slp';
}
else{
	include_once($PATH_WRT_ROOT."includes/libclass.php");
	$lclass = new libclass();
	$className = $lclass->getClassName($_POST['YearClassID']);
	unset($lclass);
	$filename = $issueAYName.'_'.$className.'_slp';
}

if (!isset($studentList))
{
	// If no studentIDs retrieve from classname
	$objReportMgr = new ipfReportManager();
	$objReportMgr->setStudentID($StudentID);
	$objReportMgr->setYearClassID($YearClassID);
	$studentList = $objReportMgr->getStudentIDList($ActivatedOnly=true);		// user with current class name and class number
}

$obj = new objPrinting();
$studentList = $obj->_handleCurrentAlumniStudentBeforePrinting($trgType,$alumni_StudentID,$studentList);

$numStudent = count($studentList);
if($numStudent>0){
	$objPrinting = new objPrinting($_POST['task'],$studentList,$academicYearID); // class from libpf-slp-tmchkwc.php
	$objPrinting->setIssueDate($issuedate);
	$objPrinting->setAcademicStartYear($academic_start_year);
	$objPrinting->setAcademicEndYear($academic_end_year);
	if ($objPrinting->isForm6Student()) {
		$lastTermID = $objPrinting->getLastAcademicYearTermID();
		if ($lastTermID) {
			if (in_array($lastTermID,(array)array_keys($academicYearTerms))) {
				unset($academicYearTerms[$lastTermID]);
			}
		}
	}
	
	$objPrinting->setAcademicYearTerms($academicYearTerms);
	$studentInfo = $objPrinting->getStudentInfo();	// all selected students
	$performanceWiTerm = $objPrinting->getPerformanceWiTerm();
	$performanceWoTerm = $objPrinting->getPerformanceWoTerm();
	$performanceWiActivity = $objPrinting->getPerformanceWiActivity();
	$performance_BTP_IC = $objPrinting->getPerformance_BTP_IC();

	//start creating PDF
	//new mPDF ($mode,$format,$default_font_size,$default_font,$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer,$orientation)
// 	$margin_left = 18; //mm
// 	$margin_right = 18;
// 	$margin_top = 14;
// 	$margin_bottom = 20;
// 	$margin_header = 12;
// 	$margin_footer = 12;
// 	$pdf = new mPDF('','A4',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer);
	$pdf = new mPDF('','A4');
	
	// Chinese use mingliu 
//	$pdf->backupSubsFont = array('mingliu');
	$pdf->useSubstitutions = false;
	$pdf->setAutoTopMargin = 'stretch';
	
	// 20180511 double-sided printing
	$pdf->mirrorMargins = true;
	$evenHeaderAry['html'] = '';
	$evenHeaderAry['h'] = 0;
	$pdf->SetHTMLHeader($evenHeaderAry, 'E');
	
	// header footer style
	$style = $objPrinting->returnStyleSheet();
// 	$pdf->WriteHTML($style);		
// 	$pdf->DefHTMLHeaderByName('NoHeader','');
	

	$prevPage = 0;
	// Gen PDF here
	for($y = 0; $y<$numStudent ; $y++){
	
		// Change student
		$_studentid = $studentList[$y];
		$objPrinting->setStudentID($_studentid);
		$reportInfo = array_merge(	$studentInfo[$_studentid] ? $studentInfo[$_studentid] : array(),
									$performanceWiTerm[$_studentid] ? $performanceWiTerm[$_studentid] : array(),
									$performanceWoTerm[$_studentid] ? $performanceWoTerm[$_studentid] : array(),
									$performanceWiActivity[$_studentid] ? $performanceWiActivity[$_studentid] : array(),
		                            $performance_BTP_IC[$_studentid] ? $performance_BTP_IC[$_studentid] : array());
		$objPrinting->setReportInfo($reportInfo);
		
		$reportHeader = $objPrinting->getReportHeader();
		$oddHeaderAry['html'] = $reportHeader;
		$oddHeaderAry['h'] = '55mm';
		$pdf->SetHTMLHeader($oddHeaderAry,$OE='O',$write=true);      // print on odd page only, need to set header before print anything !!!
		
		if($y == 0){
			$pdf->WriteHTML($style);		
		}
		
		$content = $objPrinting->getReportContentLayout();

		$pdf->pagenumPrefix = $Lang['iPortfolio']['JinYing']['Report']['Page']['PageNumSuffix'];
		$pdf->nbpgPrefix = $Lang['iPortfolio']['JinYing']['Report']['Page']['TotPageNumPrefix'];
		$pdf->nbpgSuffix = $Lang['iPortfolio']['JinYing']['Report']['Page']['TotPageNumSuffix'];
		$pdf->defaultfooterline=0;
		$pdf->PAGE_NUMBER_SPECIAL = '2';
		$pdf->SetFooter('<div style="text-align: center;">{PAGENO}{nbpg}</div>');
			
		// write into PDF
		$pdf->WriteHTML($content);
		$currPage = $pdf->page;
		$currNoOfPage = $currPage - $prevPage;
		$prevPage = $currPage;
		
		// Last Page no need to add pagebreak
		if($y != ($numStudent-1)){
			$pdf->writeHTML('<pagebreak resetpagenum="1"/>');
		}

		
		if ($currNoOfPage % 2 == 1) {
			if($y != ($numStudent-1)){
				$pdf->SetFooter('');
			}
			else {
				$pdf->SetFooter('<div style="text-align: center;">{PAGENO}{nbpg}</div>');
			}
			$pdf->AddPage("","", 1);	// reset page number
			$pdf->SetFooter('');
			$prevPage++;
		}			
		
	}
	
	$pdf->Output($filename.'.pdf', 'I');
}

$obj->_handleCurrentAlumniStudentAfterPrinting($trgType,$studentList);

?>