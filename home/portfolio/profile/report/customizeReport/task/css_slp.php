<?php
ini_set('memory_limit','128M');
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/css/libpf-slp-css.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

//StartTimer("GLB_StartTime");

//content Ready
if(!empty($StudentID)){
	$studentList = array($StudentID);
}

if(empty($issuedate)){
	$issuedate = date("F, Y");
}
else{
	$issuedate = date("F, Y" , strtotime($issuedate));
}

if (!isset($studentList))
{
	// If no studentIDs retrieve from classname
	$objReportMgr = new ipfReportManager();
	$objReportMgr->setStudentID($StudentID);
	$objReportMgr->setYearClassID($YearClassID);
	$academicYearID = Get_Current_Academic_Year_ID();
	$objReportMgr->setAcademicYearID($academicYearID);
	$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

	$studentList = $objReportMgr->getStudentIDList($ActivatedOnly=true);

}

$academicYearID = Get_Current_Academic_Year_ID(); // need to change

$objPrinting = new objPrinting(); // class from libpf-slp-css.php
$objPrinting->setYearClassID($YearClassID);
$objPrinting->setAcademicYear($academicYearID);
$objPrinting->setStudentList($studentList);
$objPrinting->contentReady();

//start creating PDF
//new mPDF ($mode,$format,$default_font_size,$default_font,$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer,$orientation)
$margin_left = 20; //mm
$margin_right = 20;
$margin_top = 45;
$margin_bottom = 35;
$margin_header = 10;
$margin_footer = 20;
$pdf = new mPDF('','A4',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer);
//$pdf = new mPDF();
//$pdf->setAutoTopMargin = 'stretch';
$pdf->splitTableBorderWidth = 0.1; //split 1 table into 2 pages add border at the bottom

//pdf metadata setting
$pdf->SetTitle('啟思中學 SLP');
$pdf->SetAuthor('BroadLearning Education (Asia) Ltd.');
$pdf->SetCreator('BroadLearning Education (Asia) Ltd.');
$pdf->SetSubject('Student Development Progress Report');
$pdf->SetKeywords('啟思中學', 'eClass, iPortfolio, SLP');

//get header, footer, css stylesheet
$style = $objPrinting->returnStyleSheet();
$lastRowClass= 'class="bottom"';
$EmptyString = $objPrinting->EmptyString;

//set header, footer, css stylesheet
$header = $objPrinting->returnHeaderAndFooter('header');
$footer = $objPrinting->returnHeaderAndFooter('footer');
$footerWithSign = $objPrinting->returnHeaderAndFooter('footer');
$signature = $objPrinting->returnSignature();
$pdf->DefHTMLHeaderByName('htmlHeader',$header);
$pdf->SetHTMLHeaderByName('htmlHeader');
//$pdf->DefHTMLFooterByName('htmlFooterWithSign',$signature.$footer);
$pdf->DefHTMLFooterByName('htmlFooterWithSign',$footer);
$pdf->DefHTMLFooterByName('htmlFooter',$footer);
$pdf->SetHTMLFooterByName('htmlFooter');
//$pdf->SetHTMLFooterByName('htmlFooterWithSign');

$footerright_url = $PATH_WRT_ROOT."/file/customization/css/main.png";
$pdf->SetWatermarkImage($footerright_url,1,array(297,3),array(-2,276));
$pdf->showWatermarkImage = true;
$pdf->PAGE_NUMBER_SPECIAL = 1;
//Looping studentList
$numStudent = count($studentList);
for($y = 0; $y<$numStudent ; $y ++){
	$pdf->SetHTMLFooterByName('htmlFooter');
	// Change student
	$_studentid = $studentList[$y];
	$objPrinting->setStudent($_studentid);
	
	// Get All Content
	$StudentInfoArr = $objPrinting->getStudentInfo();
	$HouseTutor = $objPrinting->getStudentHouseTutor();
	// School Based Activity
	$schoolBasedActivity_AssoArr = $objPrinting->GetOLE('INT','OLE'); //School based activity
	$num_schoolBasedActivity = count($schoolBasedActivity_AssoArr);
	// External Activity
	$externalActivity_AssoArr = $objPrinting->GetOLE('EXT','OLE'); //External activity
	$num_externalActivity = count($externalActivity_AssoArr);
	// Service merge by INT and EXT.
	$intService_AssoArr = $objPrinting->GetOLE('INT','SERVICE'); // Leadership & Service
	$extService_AssoArr = $objPrinting->GetOLE('EXT','SERVICE');
	$service_AssoArr = array_merge((array)$intService_AssoArr,(array)$extService_AssoArr);
	$num_service = count($service_AssoArr);
	if($num_service > 1){
		sortByColumn2($service_AssoArr,'Title');
	}
	// Attendance & Detention
	$attendance_AssoArr = $objPrinting->getStudentAttendanceRecords();
	$detention_AssoArr = $objPrinting->getStudentDetentionRecords();
	
	
	//------------------------------- Contents Table Start ----------------------------------------//
	//Student Info
	$studentInfo = '<table class="studentInfo" width="100%">
		<tr>
			<td>Name: '.$StudentInfoArr['EnglishName'].'</td>
		</tr>
		<tr>
			<td>Date:&nbsp;'.$issuedate.'</td>
		</tr>
		<tr>
			<td>Form and House: '.$objPrinting->getHouseName($StudentInfoArr['ClassName']).'</td>
		</tr>
		<tr>
			<td>Student No.: s'.$StudentInfoArr['WebSAMSRegNo'].'</td>
		</tr>
		<tr>
			<td>House Tutor: '.$HouseTutor.'</td>
		</tr>
	</table><br>';
	
	//School based activity
	$schoolBasedActivity = '
	<div >
	<table class="contentTable" width="100%" style="border-collapse: collapse;">
		<thead>
			<tr>
				<td class="header" colspan="3">School based activities</td>
			</tr>
			<tr>
				<td width="33%">Name of an activity</td>
				<td width="33%">Term</td>
				<td width="33%">Achievement</td>
			</tr>
		</thead>
		<tbody>';
	for($i=0; $i<$num_schoolBasedActivity ; $i ++){
		$__title = $schoolBasedActivity_AssoArr[$i]['Title'];
		$__term = $schoolBasedActivity_AssoArr[$i]['Term'];
		$__achievement = $schoolBasedActivity_AssoArr[$i]['Achievement'];
		$__lastRow = ($i == ($num_schoolBasedActivity-1)) ? $lastRowClass : '';
		$schoolBasedActivity .= '
			<tr '.$__lastRow.'>
				<td>'.EmptyContentAsEmptyString($__title).'</td>
				<td align="center">'.EmptyContentAsEmptyString($__term).'</td>
				<td align="center">'.EmptyContentAsEmptyString($__achievement).'</td>
			</tr>';
	}
	if($num_schoolBasedActivity == 0){
		$schoolBasedActivity .= '
			<tr class="emptyRow">
				<td>'.EmptyContentAsEmptyString().'</td>
				<td>'.EmptyContentAsEmptyString().'</td>
				<td>'.EmptyContentAsEmptyString().'</td>
			</tr>';
	}
	$schoolBasedActivity .= '		
		</tbody>
	</table>
	</div>
	<br>';
	
	//External activity
	$externalActivity = '
	<table class="contentTable" width="100%" style="border-collapse: collapse;" cellpadding="2">
		<thead>
			<tr>
				<td class="header" colspan="3">External activities</td>
			</tr>
			<tr>
				<td width="33%">Name of an activity</td>
				<td width="33%">Organisation</td>
				<td width="33%">Achievement</td>
			</tr>
		</thead>
		<tbody>';
	for($i=0; $i<$num_externalActivity ; $i ++){
		$__title = $externalActivity_AssoArr[$i]['Title'];
		$__organization = $externalActivity_AssoArr[$i]['Organization'];
		$__achievement = $externalActivity_AssoArr[$i]['Achievement'];
		$__lastRow = ($i == ($num_externalActivity-1)) ? $lastRowClass : '';
		$externalActivity .= '
			<tr '.$__lastRow.'>
				<td>'.EmptyContentAsEmptyString($__title).'</td>
				<td align="center">'.EmptyContentAsEmptyString($__organization).'</td>
				<td align="center">'.EmptyContentAsEmptyString($__achievement).'</td>
			</tr>';
	}
	if($num_externalActivity == 0){
		$externalActivity .= '
			<tr class="emptyRow">
				<td>'.EmptyContentAsEmptyString().'</td>
				<td>'.EmptyContentAsEmptyString().'</td>
				<td>'.EmptyContentAsEmptyString().'</td>
			</tr>';
	}
	$externalActivity .='	
		</tbody>
	</table>
	<br>';
	
	//Leadership & Service
	$service ='
	<table class="contentTable" width="100%" style="border-collapse: collapse;" cellpadding="2">
		<thead>
			<tr>
				<td class="header" colspan="4">Leadership & Service</td>
			</tr>
			<tr>
				<td width="25%">Name of an activity</td>
				<td width="25%">Organisation</td>
				<td width="25%">Role</td>
				<td width="25%">Achievement</td>
			</tr>
		</thead>
		<tbody>';
	for($i=0; $i<$num_service ; $i ++){
		$__title = $service_AssoArr[$i]['Title'];
		$__organization = $service_AssoArr[$i]['Organization'];
		$__role = $service_AssoArr[$i]['Role'];
		$__achievement = $service_AssoArr[$i]['Achievement'];
		$__lastRow = ($i == ($num_service-1)) ? $lastRowClass : '';
		$service .= '
			<tr '.$__lastRow.'>
				<td>'.EmptyContentAsEmptyString($__title).'</td>
				<td align="center">'.EmptyContentAsEmptyString($__organization).'</td>
				<td align="center">'.EmptyContentAsEmptyString($__role).'</td>
				<td align="center">'.EmptyContentAsEmptyString($__achievement).'</td>
			</tr>';
	}
	if($num_service == 0){
		$service .= '
			<tr class="emptyRow">
				<td>'.EmptyContentAsEmptyString().'</td>
				<td>'.EmptyContentAsEmptyString().'</td>
				<td>'.EmptyContentAsEmptyString().'</td>
				<td>'.EmptyContentAsEmptyString().'</td>
			</tr>';
	}	
	$service .= '
		</tbody>
	</table>
	<br>';
	
	//Attendance Records
	if(is_array($attendance_AssoArr)){
		if(array_sum($attendance_AssoArr) > 0){
			$absAM = $attendance_AssoArr['NumAbsAM'];
			$absPM = $attendance_AssoArr['NumAbsPM'];
			
			include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
			$lgs = new libgeneralsettings();
			
			$AttendanceSetting = $lgs->Get_General_Setting('StudentAttendance');
			if($AttendanceSetting['ProfileAttendCount'] == 0){
				$numAbs = $absAM + $absPM;
			}else if($AttendanceSetting['ProfileAttendCount'] == 1){
				$numAbs = $absAM/2 + $absPM/2;
			}
			
			$attendanceRecords ='
			<table class="contentTable" width="100%" style="border-collapse: collapse;" cellpadding="2">
				<thead>
					<tr>
						<td class="header" colspan="4">No. of Late(s)</td>
						<td class="header" colspan>No. of Absence(s)</td>
					</tr>
				</thead>
				<tbody>
					<tr class="bottom">
					<td>Morning</td>
					<td align="center">'.$attendance_AssoArr['NumLateAM'].'</td>
					<td>Afternoon</td>
					<td align="center">'.$attendance_AssoArr['NumLatePM'].'</td>
					<td align="center">'.$numAbs.'</td>
					</tr>
				</tbody>
			</table>
			<br>';
		}
		else{
			// no record hide table
			$attendanceRecords = '';
		}
	}

	//Sanctions 
	if(is_array($detention_AssoArr)){
		if(array_sum($detention_AssoArr) > 0){
			$sanctions = '
				<table class="contentTable" width="100%" style="border-collapse: collapse;" cellpadding="2">
					<thead>
						<tr>
							<td class="header" colspan="3">Sanctions: Detention and Suspension</td>
						</tr>
						<tr>
							<td width="33%">Lunch Time</td>
							<td width="33%">After School</td>
							<td width="33%">Suspension</td>
						</tr>
					</thead>
					<tbody>
						<tr class="bottom">
						<td align="center">'.$detention_AssoArr['NumLunch'].'</td>
						<td align="center">'.$detention_AssoArr['NumAfterSchool'].'</td>
						<td align="center">'.$detention_AssoArr['NumSuspension'].'</td>
						</tr>
					</tbody>
				</table>';
		}
		else{
			$sanctions='';
		}
	}
	
	// remarks
	if(!empty($custRemarks)){
		$remarks_html = '<div>'.standardizeFormPostValue($custRemarks).'</div>';
// 		$remarks_html = '<div>Remarks: '.standardizeFormPostValue($custRemarks).'</div>';
	}
	else{
		$remarks_html = '';
	}
	//------------------------------- Contents Table Start ----------------------------------------//
	
	// Write to pdf
	if($y==0){
		// print style only for the first student
		$pdf->writeHTML($style);
	}
	$pdf->writeHTML($studentInfo);
	$pdf->writeHTML($schoolBasedActivity);
	$pdf->writeHTML($externalActivity);
	
	# Print Last Table Without split into 2 pages
	$LastPart = '';
	if($attendanceRecords == '' && $sanctions == ''){
		$LastPart = '<div style="page-break-inside: avoid;">'.$service.'</div>'.$remarks_html.$signature;
	}
	else if($attendanceRecords != '' || $sanctions != ''){
		$pdf->writeHTML($service);
		if($attendanceRecords ==''){
			$LastPart = '<div style="page-break-inside: avoid;">'.$sanctions.'</div>'.$remarks_html.$signature;
		}else if($sanctions ==''){
			$LastPart = '<div style="page-break-inside: avoid;">'.$attendanceRecords.'</div>'.$remarks_html.$signature;
		}else{
			$LastPart = '<div style="page-break-inside: avoid;">'.$sanctions.'</div>'.$remarks_html.$signature;
		}	
	}
	
	$pdf->writeHTML($LastPart);
	// Last Page no need to add pagebreak
	if($y != ($numStudent-1)){
		$pdf->writeHTML('<pagebreak resetpagenum="1"/>');
	}
	
//	$footermain_url = $PATH_WRT_ROOT."/file/customization/css/footer_main.png";
//	$footerleft_url = $PATH_WRT_ROOT."/file/customization/css/footer_left2.png";
//	$pdf->writeHTML('<div style="position:absolute; left:18mm;bottom:10mm;"><img src="'.$footermain_url.'" width="147mm"/></div>');
//	$pdf->writeHTML('<div style="position:absolute; right:28mm;bottom:10mm;"><img src="'.$footerleft_url.'" width="14mm"/></div>');
}

//$style = str_replace('0.1mm','1px',$style);
//echo $style.$header.$studentInfo.$schoolBasedActivity.$externalActivity.$service.$attendanceRecords.$sanctions.$footer;
//$ProcessTime = round(StopTimer(2, true, "GLB_StartTime"), 4);
$filenameSuffix = '_'.$StudentInfoArr['ClassName']; 
$pdf->Output('Student_Development_Progress_Report'.$filenameSuffix.'.pdf', 'I');
//$pdf->Output('ProcessTime_'.$ProcessTime.'_MemoryUsage_'.number_format((memory_get_usage())/1024/1024, 1).'MB.pdf', 'I');
?>