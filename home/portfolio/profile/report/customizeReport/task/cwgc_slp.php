<?php
@set_time_limit(5400);
@ini_set('max_input_time', 5400);
@ini_set('memory_limit', -1);
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cwgc/libpf-slp-cwgc.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
require_once($PATH_WRT_ROOT.'includes/tcpdf60/tcpdf.php');

//StartTimer("GLB_StartTime");


$EmptyString = '---';
$EmptyStringCentered = '<span align="center">---</span>';

function EmptyOrNot($string){
	global $EmptyStringCentered;
	if($string == ''){
		return $EmptyStringCentered;
	}
	else{
		return $string;
	}
}

function getDisplayScore($newArray,$__academicYearId,$___yearTermID,$_SubjectID,$_SubjectCode){
	
	global $EmptyString;
	
	//Display Score/Grade/---
	if($newArray[$__academicYearId][$___yearTermID][$_SubjectID][$_SubjectCode]['Score'] != '' &&
		$newArray[$__academicYearId][$___yearTermID][$_SubjectID][$_SubjectCode]['Score'] >= 0){
		$__displayScore = $newArray[$__academicYearId][$___yearTermID][$_SubjectID][$_SubjectCode]['Score'];
	}
	else{
		
		if($newArray[$__academicYearId][$___yearTermID][$_SubjectID][$_SubjectCode]['Grade'] ==""){
			$__displayScore = $EmptyString;
		}
		else{
			
			$__displayScore = $newArray[$__academicYearId][$___yearTermID][$_SubjectID][$_SubjectCode]['Grade'];
		}
	}
	return $__displayScore;
}

class MYPDF extends TCPDF {
	
	var $signature_footer;
	var $special_header;
	
	public function set_signature_footer($footerName){
		$this->signature_footer = $footerName;
	}
	public function set_special_header($var){
		$this->special_header = $var;
	}
	
    //Page header
    public function Header() {
        global $PATH_WRT_ROOT,$student_header;
        // Logo
        $image_file = $PATH_WRT_ROOT.'file/customization/cwgc_logo.jpg';
        if($_REQUEST['displayReportHeader']){
        	$this->Image($image_file, 19, 16, 23, 23, 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        }
       // Set font
        $this->SetY(+16);
        $this->SetFont('arialunicid0', 'B', 12);
        $NowAcademicYear = date("Y",getEndOfAcademicYear());
        $displayAcademicYearRange = ($NowAcademicYear-3).'-'.$NowAcademicYear;
        
        $html = '<table>
					<tr>
						<td colspan="3" align="center">
							Christian Alliance Cheng Wing Gee College<br />
							宣道會鄭榮之中學<br />
							Student Learning Profile<br />
							'.$displayAcademicYearRange.'
						</td>
					</tr>
				</table>';
		
        // Title
		//$this->setCellMargins(1, 10, 1, 1);
		//$this->SetFillColor(255, 255, 255);
        //$this->MultiCell(0, 15, $html, 0, false, 'C', 0, '', 0, false, 'M', 'M');
        $this->writeHTML($html);
        
        $this->SetY(+47);
        $this->SetFont('arialunicid0', '', 10);
        $this->writeHTML($student_header);
       	
       	if($this->special_header == 1){
	       	$style = array('width' => 0.4, 'cap' => 'round', 'join' => 'round', 'dash' => 0, 'color' => array(0, 0, 0));
	        $this->Line(18, 69, 192, 69,$style);//T
	        $this->Line(18, 242, 192, 242,$style);//B
	        $this->Line(18, 69, 18, 242,$style);//L
	        $this->Line(192, 69, 192, 242,$style);//R
       	}
       	else if($this->special_header == 2){
       		$style = array('width' => 0.4, 'cap' => 'round', 'join' => 'round', 'dash' => 0, 'color' => array(0, 0, 0));
	        $this->Line(18, 62, 192, 62,$style);//T
	        $this->Line(18, 242, 192, 242,$style);//B
	        $this->Line(18, 62, 18, 242,$style);//L
	        $this->Line(192, 62, 192, 242,$style);//R

       	}
    }

    // Page footer
    public function Footer() {
        global $StudentInfoArr,$issuedate;
		
		$this->SetY(-34);	
        $school_footer = '<table height="26mm" style="vertical-align:bottom; text-align:center; ">
				<tr>
					<td><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>
					<td><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>
				</tr>
				<tr>
					<td>Principal\'s Signature</td>
					<td>School Chop</td>
				</tr>
				<tr><td></td></tr>
			</table>';
		
		$student_footer = '<table height="26mm" style="vertical-align:bottom; text-align:center; ">
				<tr>
					<td></td>
					<td></td>
					<td><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td>Student\'s Signature</td>
				</tr>
				<tr><td></td></tr>
			</table>';		
        
        if($this->signature_footer == 'Student'){
        	$footer = $student_footer;
        }
		else{
			$footer = $school_footer;
		}
		
		$footer .= '<table cellpadding="3">
						<tr>
							<td width="60mm">Website: www.cwgc.edu.hk</td>
							<td width="72mm">Email Address: school@cwgc.edu.hk</td>
							<td>School Phone: 2604 9762</td>
						</tr>
						<tr>
							<td colspan="3">School Address: 12-14, Chik Wan Street, Tai Wai, Shatin, N.T.</td>
						</tr>
			</table>';
		$this->SetFont('arialunicid0', '', 10);
		$this->writeHTML($footer);
        
        // Set font
        $this->SetFont('arialunicid0', '', 8);
        // Page number
        $this->SetY(-12);
        
        //$emptySpace = '                                                                                                                                           	                                                                          ';
        //$this->Cell(0, 0, $emptySpace.$this->getGroupPageNoFormatted().'/'.$this->getPageGroupAlias(), 0, 0, 'C', 0, '', 0, 0, 'T', 'M');
        $this->Cell(0, 0, $this->getGroupPageNoFormatted().'/'.$this->getPageGroupAlias(), 0, 0, 'R', 0, '', 0, 0, 'T', 'M');
        
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('BroadLearning Education (Asia) Ltd.');
$pdf->SetTitle('香港九龍塘基督教中華宣道會鄭榮之中學 SLP');
$pdf->SetSubject('Student Learning Portfolio');
$pdf->SetKeywords('香港九龍塘基督教中華宣道會鄭榮之中學', 'eClass, iPortfolio, SLP');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// set header and footer fonts
//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
////$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetHeaderMargin(0);
////$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
//$pdf->SetFooterMargin(0);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$rightMargins =  PDF_MARGIN_RIGHT+3; //PDF_MARGIN_RIGHT,PDF_MARGIN_LEFT = 15
$leftMargins = PDF_MARGIN_LEFT+3;
$contentWidth = 210 - $leftMargins - $rightMargins;
$pdf->SetMargins($leftMargins, PDF_MARGIN_TOP+37, $rightMargins);
 
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM+30);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}


// ---------------------------------------------------------

$objPrinting = new objPrinting();

if(!empty($StudentID)){
	$studentList = array($StudentID);
}

if(empty($issuedate)){
	$issuedate = date("Y-m-d");
}

if (!isset($studentList))
{
	$objReportMgr = new ipfReportManager();
	
	$objReportMgr->setStudentID($StudentID);
	$objReportMgr->setYearClassID($YearClassID);
	$academicYearID = Get_Current_Academic_Year_ID();
	$objReportMgr->setAcademicYearID($academicYearID);
	$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

	$studentList = $objReportMgr->getStudentIDList();

}

//$mode = 'pdf';
$objPrinting->setYearClassID($YearClassID);
$objPrinting->setStudentList($studentList);
$objPrinting->contentReady();

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(false);

// Header format
$header_format = 'background-color:#CCCCCC;';

foreach((array)$studentList as $_studentid){
	
	// Change student
	$objPrinting->setStudent($_studentid);
	
	############################################################################################################
	####################################### Studnent Info (Start) ##############################################
	############################################################################################################
	
	$StudentInfoArr = $objPrinting->getStudentInfo();

	$student_header = '<table>
						<tr>
							<td width="70mm">Name: '.$StudentInfoArr["EnglishName"].' '.$StudentInfoArr["ChineseName"].'</td>
							<td width="63mm">Class: '.$StudentInfoArr["ClassName"].' ('.$StudentInfoArr["ClassNumber"].')</td>
							<td>Sex: '.$StudentInfoArr["Gender"].'</td>
						</tr>
						<tr>
							<td>STRN: '.$StudentInfoArr["STRN"].'</td>
							<td>Date of Birth: '.$StudentInfoArr["DateOfBirth"].'</td>
							<td>Date of Issue: '.$issuedate.'</td>
						</tr>
					</table>';
	############################################################################################################
	###################################### Studnent Info (End) #################################################
	############################################################################################################
	
	############################################################################################################
	############################ Section 1 - Assessment Report (Start) #########################################
	############################################################################################################
	
	//-----------Preparing Data
	$formAry = $objPrinting->getVerifiedFormAry();
	$assessmentReportInfo = $objPrinting->GetAssessmentReport();
	$classHistoryInfoAry = $objPrinting->getStudentClassHistory();
	$SchoolSubjectInfo = $objPrinting->School_SubjectInfo;

	//ReBuild $assessmentReportInfo as $newArray
	$newArray = array();
	foreach((array)$assessmentReportInfo as $_subjectScoreInfo){
		$_AcademicYearID = $_subjectScoreInfo['AcademicYearID'];
		$_YearTermID = $_subjectScoreInfo['YearTermID'];
		$_SubjectID = $_subjectScoreInfo['SubjectID'];
		
		//Determine whether is a component
		$_Component = ($_subjectScoreInfo['SubjectComponentCode'] == '')? $_subjectScoreInfo['SubjectCode']:$_subjectScoreInfo['SubjectComponentCode'] ;
		
		$newArray[$_AcademicYearID][$_YearTermID][$_SubjectID][$_Component] = $_subjectScoreInfo;
	}

	//-----------calculate table width
	$numform = count($formAry);
	if(in_array(6,$formAry)){
		$numSemester = ($numform * 2) - 1;
	}
	else{
		$numSemester = $numform * 2;	
	}
	$SubjectWidth = 50;
	$semesterWidth = ($contentWidth - $SubjectWidth)/$numSemester;

	//-----------Start Build table	
	$assessment_report = '<span>Academic Performance</span><br>
		<table width="'.$contentWidth.'mm" border="1" cellpadding="3">
			<tr style="line-height: 150%; '.$header_format.'">
				<th border="1" rowspan="2" width="'.$SubjectWidth.'mm" style="text-align:center;"><br><br><b>Subjects</b></th>';
				// Year Name & Form Name
				foreach((array)$formAry as $_formname){
					if($_formname!=6){
						$assessment_report .= '<th width="'.($semesterWidth*2).'mm" colspan="2" align="center"><b>'.$classHistoryInfoAry[$_formname]['AcademicYear'].'<br>'.$classHistoryInfoAry[$_formname]['ClassName'].'</b></th>';
					}
					else{
						$assessment_report .= '<th width="'.$semesterWidth.'mm" colspan="1" align="center"><b>'.$classHistoryInfoAry[$_formname]['AcademicYear'].'<br>'.$classHistoryInfoAry[$_formname]['ClassName'].'</b></th>';
					}
					
				}
				$assessment_report .= '</tr>';
				//Term Name
				$assessment_report .= '<tr align="center" style="line-height: 150%; '.$header_format.'">';
				foreach((array)$formAry as $_formname){
					if($_formname!=6){
						$assessment_report .= '<th><b> First Term</b></th>';
						$assessment_report .= '<th><b>Second Term</b></th>';
					}
					else{
						$assessment_report .= '<th colspan="2"><b>Annual</b></th>';
					}
				}
			$assessment_report .= '</tr>';

			//Subject & Score Content Here			
			foreach((array)$SchoolSubjectInfo as $_SubjectInfoArr){
				
				$_SubjectID = $_SubjectInfoArr['RecordID'];
				$_SubjectCode = $_SubjectInfoArr['SubjectCode'];
				$_SubjectDisplayName = $_SubjectInfoArr['SubjectDisplayName'];
				
				
				//Check whether student have that subject records
				$_isAllScoreEmpty = true;
				foreach ((array)$classHistoryInfoAry as $__formInfoAry) {
					$_academicYearID = $__formInfoAry['AcademicYearID'];
					$_yearTermIDAry = $__formInfoAry['YearTermIDAry'];
					
					foreach((array)$_yearTermIDAry as $___yearTermID){
						if (isset($newArray[$_academicYearID][$___yearTermID][$_SubjectID]) ){
							$_isAllScoreEmpty = false;
							break(2);
						}
					}
				}
				
				//If have record start print the subject score row
				if(!$_isAllScoreEmpty){
					if(!$_SubjectInfoArr['isComponent']){
						//Big Subject e.g. Chinese, English , Maths, Liberal Studies,etc.
						$_SubjectDisplayName = str_replace('(','<br>(',$_SubjectDisplayName);
						
						$assessment_report .= '<tr align="center" style="line-height: 150%">';
						$assessment_report.= '<td align="left">'.$_SubjectDisplayName.'</td>';
						foreach((array)$formAry as $__formname){	
							$__academicYearId = $classHistoryInfoAry[$__formname]['AcademicYearID'];
							$__yearTermIDAry = $classHistoryInfoAry[$__formname]['YearTermIDAry'];
							
							if($__formname!=6){
								foreach((array)$__yearTermIDAry as $___yearTermID){
									$__displayScore = getDisplayScore($newArray,$__academicYearId,$___yearTermID,$_SubjectID,$_SubjectCode);
									$assessment_report .=  '<td>'.$__displayScore.'</td>';
								}	
							}
							else{
								$___yearTermID = $__yearTermIDAry[0];
								$__displayScore = getDisplayScore($newArray,$__academicYearId,$___yearTermID,$_SubjectID,$_SubjectCode);
								$assessment_report .=  '<td colspan="2">'.$__displayScore.'</td>';
							}
						}
						$assessment_report .= '</tr>';
						
					}
					else{
						//Component Subject e.g. Writing, Listening, SBA, etc.
						$assessment_report .= '<tr align="center" style="line-height: 150%">';
						$assessment_report.= '<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - '.$_SubjectDisplayName.'</td>';
						foreach((array)$formAry as $__formname){			
							$__academicYearId = $classHistoryInfoAry[$__formname]['AcademicYearID'];
							$__yearTermIDAry = $classHistoryInfoAry[$__formname]['YearTermIDAry'];
							
							if($__formname!=6){
								foreach((array)$__yearTermIDAry as $___yearTermID){
									$__displayScore = getDisplayScore($newArray,$__academicYearId,$___yearTermID,$_SubjectID,$_SubjectCode);
									$assessment_report .=  '<td>'.$__displayScore.'</td>';
								}	
							}
							else{
								$___yearTermID = $__yearTermIDAry[0];
								$__displayScore = getDisplayScore($newArray,$__academicYearId,$___yearTermID,$_SubjectID,$_SubjectCode);
								$assessment_report .=  '<td colspan="2">'.$__displayScore.'</td>';
							}
						}
						$assessment_report .= '</tr>';
					}
				}
			}
	$assessment_report .= '</table>';
	//-----------Table End Here	
	
	$section1 = $assessment_report;
	############################################################################################################
	############################# Section 1 - Assessment Report (End) ##########################################
	############################################################################################################
	
	############################################################################################################
	############################### Section 2 - OLE internal (Start) ###########################################
	############################################################################################################
	$OLE_Records = $objPrinting->GetOLE();
	
	$OLE_table ='<span>Other Learning Experiences</span><br>
			<table width="'.$contentWidth.'mm" border="1" cellpadding="5">
				<thead>
					<tr align="center" style="line-height: 100%; '.$header_format.' ">
						<th><b>School Years</b></th>
						<th><b>Programmes</b></th>
						<th><b>Roles</b></th>
						<th><b>Categories</b></th>
						<th><b>Awards / Achievements</b></th>
					</tr>
				</thead>
				<tbody>';
	if(count($OLE_Records) == 0){
		$OLE_table .='			
				<tr align="center">
					<td>'.$EmptyString.'</td>
					<td>'.$EmptyString.'</td>
					<td>'.$EmptyString.'</td>
					<td>'.$EmptyString.'</td>
					<td>'.$EmptyString.'</td>
				</tr>
			</tbody>
		</table>';
	}else{
		foreach((array)$OLE_Records as $_OLE_Obj){
			$_Achievement = $_OLE_Obj["Achievement"];
			if($_Achievement == ""){
				$_Achievement = "N/A";
			}
			else{
				$_Achievement = $_Achievement;
			}
			$OLE_table .='			
					<tr nobr="true">
						<td>'.EmptyOrNot($_OLE_Obj['AcademicYear']).'</td>
						<td>'.EmptyOrNot($_OLE_Obj['Title']).'</td>
						<td>'.EmptyOrNot($_OLE_Obj['Role']).'</td>
						<td>'.EmptyOrNot($_OLE_Obj['Category']).'</td>
						<td>'.EmptyOrNot($_Achievement).'</td>
					</tr>';
		}
		$OLE_table .='</tbody>
			</table>';
	}
	
	$section2 = $OLE_table;
	############################################################################################################
	################################ Section 2 - OLE internal (End) ############################################
	############################################################################################################
	
	############################################################################################################
	############################### Section 3 - Awards Record (Start) ##########################################
	############################################################################################################
	$StudentAwards = $objPrinting->getAwards();
	
	$awards_table = '
		<span>List of Awards and Major Achievements Issued by the School</span><br>
			<table width="'.$contentWidth.'mm" border="1" cellpadding="5">
				<thead>
					<tr align="center" style="line-height: 100%; '.$header_format.'">
						<th width="32mm"><b>School Years</b></th>
						<th width="32mm"><b>Terms</b></th>
						<th width="110mm"><b>Awards / Achievements</b></th>
					</tr>
				</thead>
				<tbody>';
	if(count($StudentAwards) == 0){
		$awards_table .= '
					<tr align="center">
						<td width="32mm">'.$EmptyString.'</td>
						<td width="32mm">'.$EmptyString.'</td>
						<td width="110mm">'.$EmptyString.'</td>
					</tr>
				</tbody>			
			</table>';
	}else{
		foreach((array)$StudentAwards as $_awards){
			$_Term = $_awards['Term'];
			if(empty($_Term)){
				$_Term = 'Whole Year';
			}
			else{
				$_Term = $_Term;
			}
			$awards_table .= '
					<tr nobr="true">
						<td width="32mm">'.EmptyOrNot($_awards['AcademicYear']).'</td>
						<td width="32mm">'.EmptyOrNot($_Term).'</td>
						<td width="110mm">'.EmptyOrNot($_awards['AwardName']).'</td>
					</tr>';
		}
		$awards_table .= '</tbody>			
				</table>';
	}

	$section3 = $awards_table;
	############################################################################################################
	################################ Section 3 - Awards Record (End) ###########################################
	############################################################################################################
	
	############################################################################################################
	############################### Section 4 - OLE external (Start) ###########################################
	############################################################################################################
	
	##!!!!!!!!!!!!!!!!!!!!!!!!! Be Careful this part reuse variable in section 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!##
	$OLEE_Records = $objPrinting->GetOLE_Ext();
	
	$OLEE_table = '
		<span>Performance / Awards and Key Participation Outside School</span><br>
			<table width="'.$contentWidth.'mm" border="1" cellpadding="5">
				<thead>
					<tr align="center" style="line-height: 100%; '.$header_format.'">
						<th><b>School Years</b></th>
						<th><b>Programmes</b></th>
						<th><b>Roles</b></th>
						<th><b>Organizations</b></th>
						<th><b>Awards / Achievements</b></th>
					</tr>
				</thead>
				<tbody>';
	
	if(count($OLEE_Records) == 0){
		$OLEE_table .='			
				<tr align="center">
					<td>'.$EmptyString.'</td>
					<td>'.$EmptyString.'</td>
					<td>'.$EmptyString.'</td>
					<td>'.$EmptyString.'</td>
					<td>'.$EmptyString.'</td>
				</tr>
			</tbody>
		</table>
				';
	}else{
		foreach((array)$OLEE_Records as $_OLEE_Obj){
			if (trim($_OLEE_Obj["Achievement"])===""){
				$_Achievement = "N/A";
			}
			else{
				$_Achievement = $_OLEE_Obj["Achievement"];
			}
			$OLEE_table .='			
					<tr nobr="true">
						<td>'.EmptyOrNot($_OLEE_Obj['AcademicYear']).'</td>
						<td>'.EmptyOrNot($_OLEE_Obj['Title']).'</td>
						<td>'.EmptyOrNot($_OLEE_Obj['Role']).'</td>
						<td>'.EmptyOrNot($_OLEE_Obj['Organization']).'</td>
						<td>'.EmptyOrNot($_Achievement).'</td>
					</tr>
					';
		}
		$OLEE_table .='</tbody>
				</table>';
		
	}
	$OLEE_table .='<br>
				<br>
				<span>*Information is provided by the student.</span><br>
				<span>*Evidence of awards / certifications / achievements listed is available for submission when required.</span>';

	$section4 = $OLEE_table;

	############################################################################################################
	################################ Section 4 - OLE external (End) ############################################
	############################################################################################################
	
	############################################################################################################
	################################ Section 5 - Self Account (Start) ##########################################
	############################################################################################################
	if($displaySelfAccount){
		$SelfAccountContents = $objPrinting->getSelfAccount();
		
		$ReplacedSelfAccountContents = $SelfAccountContents[0]['Details']; 
		$ReplacedSelfAccountContents = str_replace("‘","'",$ReplacedSelfAccountContents);
		$ReplacedSelfAccountContents = str_replace("’","'",$ReplacedSelfAccountContents);
		$ReplacedSelfAccountContents = str_replace('＂','"',$ReplacedSelfAccountContents);
		$ReplacedSelfAccountContents = str_replace('“','"',$ReplacedSelfAccountContents);
		$ReplacedSelfAccountContents = str_replace('”','"',$ReplacedSelfAccountContents);
		
		$self_account = '<span>Student\'s Self-Account</span><br>
				<table cellpadding="15">
						<tr nobr="true">
							<td height="167mm" width="'.$contentWidth.'mm">'.$ReplacedSelfAccountContents.'</td>
						</tr>
				</table>';
	
		$section5 = $self_account;
	}
	############################################################################################################
	################################# Section 5 - Self Account (End) ###########################################
	############################################################################################################
	
	//echo $section1.$section2.$section3.$section4.$section5;
	// Start First Page Group
	
	$pdf->startPageGroup();
	
	$pdf->set_signature_footer('School');
	$pdf->AddPage();
	$pdf->SetFont('arialunicid0', '', 10);
	$pdf->writeHTML($section1, true, false, true, false, '');
	$pdf->endPage();
	
	$pdf->AddPage();
	$pdf->SetFont('arialunicid0', '', 10);
	$pdf->writeHTML($section2, true, false, true, false, '');
	$pdf->endPage();
	
	$pdf->AddPage();
	$pdf->SetFont('arialunicid0', '', 10);
	$pdf->writeHTML($section3, true, false, true, false, '');
	$pdf->endPage();
	
	$pdf->AddPage();
	$pdf->set_signature_footer('Student');
	$pdf->SetFont('arialunicid0', '', 10);
	$pdf->writeHTML($section4, true, false, true, false, '');
	$pdf->endPage();
	
	if($displaySelfAccount){
		$pdf->set_special_header(1);
		$pdf->AddPage();
		$pdf->set_special_header(2);
		$pdf->SetFont('arialunicid0', '', 10);
		$pdf->writeHTML($section5, true, false, true, false, '');
		$pdf->endPage();
		$pdf->set_special_header(0);
	}
	
	unset($StudentInfoArr);
	unset($student_header);
	unset($formAry);
	unset($assessmentReportInfo);
	unset($classHistoryInfoAry);
	unset($SchoolSubjectInfo);
	unset($assessment_report);
	unset($OLE_Records);
	unset($OLE_table);
	unset($StudentAwards);
	unset($awards_table);
	unset($OLEE_Records);
	unset($OLEE_table);
	unset($SelfAccountContents);
	unset($self_account);
	unset($section1);
	unset($section2);
	unset($section3);
	unset($section4);
	unset($section5);
	
}//End of a student
//$ProcessTime = round(StopTimer(2, true, "GLB_StartTime"), 4);

//Close and output PDF document
//$pdf->Output('ProcessTime_'.$ProcessTime.'_MemoryUsage_'.number_format((memory_get_usage())/1024/1024, 1).'MB.pdf', 'I');


$pdf->Output('cwgc_slp.pdf', 'I');
//============================================================+
// END OF FILE
//============================================================+

?>