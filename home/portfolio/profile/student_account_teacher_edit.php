<?php

$PATH_WRT_ROOT = "../../../";

# Define name of textarea
//$TEXTAREA_OBJ_NAME = "Details";
define("TEXTAREA_OBJ_NAME", "Details");

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

//$UserID = $_SESSION['UserID'];

$li_pf = new libportfolio2007();
$lpf_slp = new libpf_slp();

# GET PARAMS
$StudentID 	= $_GET['StudentID'];
$RecordID 	= $_GET['RecordID'];
$YearClassID 	= $_GET['YearClassID'];
//$referer	= (!empty($_SERVER['HTTP_REFERER']))? $_SERVER['HTTP_REFERER']:"/home/portfolio/school_records.php";

# Get class selection
$lyc = new year_class($YearClassID);
$class_name = ($YearClassID == "") ? $i_general_WholeSchool : $lyc->Get_Class_Name();

# Get student info
$student_obj = $li_pf->GET_STUDENT_OBJECT($StudentID);

# Set photo in the left menu
$luser = new libuser($StudentID);
$luser->PhotoLink = $li_pf->GET_OFFICIAL_PHOTO_BY_USER_ID($StudentID);
$luser->PhotoLink = str_replace($intranet_root, "", $luser->PhotoLink[0]);

# Get self account information
$self_acc = $lpf_slp->GET_SELF_ACCOUNT($RecordID);

# Escape special characters
$self_acc['Title'] = intranet_htmlspecialchars($self_acc['Title']);

if($self_acc['AttachmentPath'] != "")
{
	$lf = new libfilesystem();

	# Server directory path containing attachments
	$dir_path = $file_path."/file/portfolio/self_account/".$self_acc['AttachmentPath']."/";
	# Traverse the directory
	$attachment_list = $lf->return_folderlist($dir_path);

	if(is_array($attachment_list))
	{
		for($j=0; $j<count($attachment_list); $j++)
		{
			$attachment_str .= "<a href=\"/file/portfolio/self_account/".$self_acc['AttachmentPath']."/".basename($attachment_list[$j])."\" target=\"blank\" class=\"tablelink\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_attachment.gif\" width=\"12\" height=\"18\" border=\"0\" align=\"absmiddle\">".basename($attachment_list[$j])."</a> <a href=\"#\" onClick=\"jDELETE_ATTACHMENT('".basename($attachment_list[$j])."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" alt=\"".$button_remove."\" /></a><br />\n";
		}
	}
}

// template for teacher page
$linterface = new interface_html();
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];
### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax.js"></script>

<SCRIPT LANGUAGE="Javascript">
	
function jADD_ATTACHMENT_ROW()
{
	var attachment_list = document.getElementById('attachment_list');
	var new_row = attachment_list.insertRow(attachment_list.rows.length);
	var new_cell = new_row.insertCell(0);
	
	new_cell.innerHTML =	"<input type='file' name='attachment[]' />";
	
	window.location.href = "#attachment_list_anchor";
}

/*
functions for view records
1. jDISPLAY_RECORDS
2. CompleteLoadRecords
*/
function jDISPLAY_RECORDS(jParRecordType)
{
  xmlHttp_records = GetXmlHttpObject();
  if (xmlHttp_records == null)
  {
    alert ("Your browser does not support AJAX!");
    return;
  }

  // Modify Layer Content
  var url="./ajax/get_"+jParRecordType+"_html_ajax.php";
  url=url+"?objname=<?=$TEXTAREA_OBJ_NAME?>";
  url=url+"&sid="+Math.random();

  xmlHttp_records.onreadystatechange = CompleteLoadRecords;
  xmlHttp_records.open("GET",url,true);
  xmlHttp_records.send(null);
}

function CompleteLoadRecords() 
{ 
  if (xmlHttp_records.readyState==4)
  {
    var Records_HTML = xmlHttp_records.responseText;
    var RecordsLayer = document.getElementById("RecordsLayer");
    RecordsLayer.innerHTML = Records_HTML;

    if(document.getElementById("RecordsTable").offsetHeight > 200)
    	document.getElementById("RecordsLayer").style.height = 200;
    else
    	document.getElementById("RecordsLayer").style.height = document.getElementById("RecordsTable").offsetHeight;
  }
}

/*
functions for delete attachments
1. jDELETE_ATTACHMENT
2. CompleteDeleteAttachment
*/
function jDELETE_ATTACHMENT(jParAttachment)
{
	if(confirm("<?=$ec_iPortfolio['delete_attachment_confirm']?>"))
	{
	  xmlHttp_attachment = GetXmlHttpObject();
	  if (xmlHttp_attachment == null)
	  {
	    alert ("Your browser does not support AJAX!");
	    return;
	  }
	
	  // Modify Layer Content
	  var url="../ajax/delete_attachment_ajax.php";
	  url=url+"?attachment=" + jParAttachment;
	  url=url+"&path=<?=base64_encode($dir_path)?>";
	  url=url+"&RecordID=<?=$RecordID?>";
	  url=url+"&sid="+Math.random();
	
	  xmlHttp_attachment.onreadystatechange = CompleteDeleteAttachment;
	  xmlHttp_attachment.open("GET",url,true);
	  xmlHttp_attachment.send(null);
	}
}

function CompleteDeleteAttachment() 
{ 
  if (xmlHttp_attachment.readyState==4)
  {
    var Attachment_HTML = xmlHttp_attachment.responseText;
    var AttachmentLayer = document.getElementById("AttachmentLayer");
    AttachmentLayer.innerHTML = Attachment_HTML;
  }
}

</SCRIPT>

<form name="form1" method="POST" enctype="multipart/form-data" action="student_account_teacher_edit_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">			
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation">
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../school_records.php"><?=$ec_iPortfolio['class_list']?></a>
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../school_records_class.php?YearClassID=<?=$YearClassID?>"><?=$class_name?></a>
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="student_info_teacher.php?StudentID=<?=$StudentID?>&YearClassID=<?=$YearClassID?>"><?=Get_Lang_Selection($student_obj['ChineseName'], $student_obj['EnglishName'])?></a>
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><?=$button_edit?></a>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="400" align="center" valign="top" class="stu_info_log tabletext">
						<table width="100%" border="0" cellspacing="0" cellpadding="2">
							
							<tr>
								<td>
									<table width="100%" border="0" cellpadding="2" cellspacing="0">
										<tr>
											<td class="tabletext" width="30"><strong><?=$i_general_title?></strong></td>
											<td class="tabletext">
												<input type="text" name="Title" class="tabletext" style="width:100%" value="<?=$self_acc['Title']?>" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
<?php
	$use_html_editor = true;
	if ($use_html_editor)
	{
/*		# Components size
		$msg_box_width = "100%";
		$msg_box_height = 200;
		$obj_name = $TEXTAREA_OBJ_NAME;
		$editor_width = $msg_box_width;
		$editor_height = $msg_box_height;
		$init_html_content = $self_acc['Details'];
		include($PATH_WRT_ROOT."includes/html_editor_embed2.php");
*/
		# Components size
		$msg_box_width = "100%";
		$msg_box_height = 350;
		$obj_name = TEXTAREA_OBJ_NAME;
		$editor_width = $msg_box_width;
		$editor_height = $msg_box_height;
		$init_html_content = $self_acc['Details'];

		include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
		
		$objHtmlEditor = new FCKeditor($obj_name, $msg_box_width, $editor_height);
		$objHtmlEditor->Value = $init_html_content;
		$objHtmlEditor->Create();

	}
	else
	{
		echo "<textarea class=\"textboxtext\" name=\"Details\" ID=\"Details\" cols=\"80\" rows=\"16\" wrap=\"virtual\">".$self_acc['Details']."</textarea>";
	}
?>
									<table border="0" cellspacing="0" cellpadding="2">
										<tr>
											<td>
												<div id="AttachmentLayer"><?=$attachment_str?></div>
											</td>
										</tr>
										<tr>
											<td>
												<a name="attachment_list_anchor">
												<table border="0" cellspacing="0" cellpadding="2" id="attachment_list">
												</table>
											</td>
										</tr>
										<!--tr>
											<td><a href="javascript:void(0)" class="contenttool" onClick="jADD_ATTACHMENT_ROW()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_new.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_add_attachment?> </a></td>
											<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
										</tr-->
									</table>												
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<input type="submit" class="formbutton" value="<?=$button_save?>"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'">
									<input type="button" class="formbutton" onClick="self.location='<?=$referer?>'"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$button_cancel?>">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="RecordID" value="<?=$RecordID?>" />
<input type="hidden" name="YearClassID" value="<?=$YearClassID?>" />
<input type="hidden" name="StudentID" value="<?=$StudentID?>" />

</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>