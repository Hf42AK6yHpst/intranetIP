<?php
//Editing : Stanley
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("SPT");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/libpf-lp2.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");

include_once($eclass_filepath.'/src/includes/php/lib-groups.php');

intranet_opendb();



$li_pf = new libpf_sturec();
$li_pf_lp = new libpf_lp();
$liblp2 = new libpf_lp2($user_id, 0, $UserID, 'publish');

# Get class selection
$lpf_fc = new libpf_formclass();
$t_class_arr = $lpf_fc->GET_CLASS_LIST();
$class_name = $i_general_WholeSchool;
for($i=0; $i<count($t_class_arr); $i++)
{
  $t_yc_id = $t_class_arr[$i][0];
  $t_yc_title = Get_Lang_Selection($t_class_arr[$i]['ClassTitleB5'], $t_class_arr[$i]['ClassTitleEN']);
  if(!is_array($class_teach_arr) || in_array($t_yc_id, $class_teach_arr))
  {
    $class_arr[] = array($t_yc_id, $t_yc_title);
  }
  
  if($t_yc_id == $YearClassID)
  {
    $class_name = $t_yc_title;
  }
}

# Get student selection
$lpf_fc->SET_CLASS_VARIABLE("YearClassID", $YearClassID);
$default_student_id = "";
$student_id_arr = $lpf_fc->GET_STUDENT_LIST();
$t_student_detail_arr = $lpf_fc->GET_STUDENT_DETAIL_LIST($student_id_arr);
if(is_array($t_student_detail_arr))
{
  for($i=0; $i<count($t_student_detail_arr); $i++)
  {
    $t_classname = $t_student_detail_arr[$i]['ClassName'];
    if(in_array($t_classname, $class_arr)) continue;
  
    $t_user_id = $t_student_detail_arr[$i]['UserID'];
    $t_user_name = "(".$t_classname." - ".$t_student_detail_arr[$i]['ClassNumber'].") ";
    $t_user_name .= Get_Lang_Selection($t_student_detail_arr[$i]['ChineseName'], $t_student_detail_arr[$i]['EnglishName']);
    
    # Set default user ID if class is changed
    if($StudentID == $t_user_id)
      $default_student_id = $StudentID;
  
    $student_detail_arr[] = array($t_user_id, $t_user_name);
  }
}
if($default_student_id == "") $default_student_id = $student_detail_arr[0][0];
$student_selection_html = getSelectByArray($student_detail_arr, "name='StudentID' onChange='jCHANGE_FIELD()'", $default_student_id, 0, 1, "", 2);

# Get activated students
$act_student_id_arr = $lpf_fc->GET_ACTIVATED_STUDENT_LIST();


# Retrieve student info
$student_obj = $li_pf->GET_STUDENT_OBJECT($default_student_id);

# Set links for photo
if(strpos($student_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $student_obj['PhotoLink'] != "")
	$student_obj['PhotoLink'] = str_replace("<!--ImageStyle-->", "", $student_obj['PhotoLink']);
else if($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
else
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";

# Generate learning portfolio list
//list($LeftContent, $LP_list) = $li_pf_lp->GEN_PEER_LEARNING_PORTFOLIO(array($PageDivision, $Page), $default_student_id);
$amount = $PageDivision? $PageDivision: 'all';
$page = $Page - 1;

list($total, $lps) 	= $liblp2->getFriendPortfolios($page*$amount, $amount, $default_student_id);
$data['friend_lps']	= array();
foreach ($lps as $lp){	    
    $friend_intranet_id 	= $liblp2->EC_USER_ID_TO_IP_USER_ID($lp['user_id']);
    $lp 			= array_merge($lp, $liblp2->getUserInfo($friend_intranet_id));
    $lp['key'] 			= libpf_lp2::encryptPortfolioKey($lp['web_portfolio_id'], $friend_intranet_id);
    $lp['share_url'] 		= libpf_lp2::getPortfolioUrl($lp['key']);
    $data['friend_lps'][]	= $lp;
    
}

// template for student page
$linterface = new interface_html("iportfolio_default2.html");
$CurrentPage = "Student_LearningPortfolio";
// set the current page title
$CurrentPageName = Get_Lang_Selection($student_obj['ChineseName'], $student_obj['EnglishName']);

#MENU
$MenuArr = array();
$MenuArr[] = array($iPort['menu']['learning_portfolio'], "../learning_portfolio_v2/index.php");
$MenuArr[] = array($iPort['menu']['peer_learning_portfolio'], "../learning_portfolio_v2/Peer_view.php");
$MenuArr[] = array($CurrentPageName, "");


### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once($PATH_WRT_ROOT.'home/portfolio/learning_portfolio_v2/templates/header.php');
?>


<? // ===================================== Body Contents ============================= ?>
<script type="text/javascript" src="/templates/2009a/js/iportfolio.js"></script>
<script language="JavaScript">

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	document.form1.action = "learning_portfolio_student_v2.php";
	document.form1.submit();
}


// Change pages
function jCHANGE_PAGE(jParShift){
	var PageSelection = document.getElementsByName("Page");
	var OriginalIndex = PageSelection[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(TargetIndex >= PageSelection[0].length)
	{
		PageSelection[0].selectedIndex = PageSelection[0].length-1;
		return;
	}
	else if(TargetIndex < 0)
	{
		PageSelection[0].selectedIndex = 0;
		return;
	}
	else
		PageSelection[0].selectedIndex = TargetIndex;

	document.form1.FieldChanged.value = "page";
	document.form1.action = "learning_portfolio_student_v2.php";
	document.form1.submit();	
}
$(function(){  
    $('.lp_list a.get_key').click(function(){
	
	var href = $(this).attr('href');
	
	getEclassSessionKey(function(key){
	    window.open(href+'&eclasskey='+key);
	});
	
	return false;
	
    });
    
});

</script>

<FORM name="form1" method="POST" action="learning_portfolio_teacher.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan=2>
				<table border="0" cellspacing="0" cellpadding="3" width="100%">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td class="navigation"><?=$linterface->GET_NAVIGATION($MenuArr) ?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="17" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_01_tea.gif" width="17" height="37"></td>
						<td height="37" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_03_tea.gif">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="200">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center" valign="middle" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_02_tea.gif"  class="page_title"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_learning_portfolio.gif" alt="<?=$iPort['menu']['learning_portfolio']?>" width="20" height="20" border="0" align="absmiddle"> <?=$iPort['menu']['learning_portfolio']?></td>
											</tr>
										</table>
									</td>
									<td align="left" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#E8E8E8">
											<tr>
												
												<td align="right">
													<table border="0" cellspacing="0" cellpadding="5">
														<tr>
															<td nowrap background="#"></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_04_tea.gif" width="13" height="37"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_05.gif" width="17" height="20"></td>
						<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_06.gif">
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<tr>
									<td valign="top">
										<table border="0" cellspacing="0" cellpadding="0" >
											<tr>
												<td>
													<table border="0" cellspacing="0" cellpadding="0" >
														<tr>
															<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_01.gif" style="padding-left:10px; padding-top:10px;"><?=$student_obj['PhotoLink']?></td>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="10" height="10"></td>
														</tr>
														<tr>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
															<td height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_04.gif" width="10" height="10"></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" class="tabletext"><?=$intranet_session_language=="b5"?$student_obj['ChineseName']:$student_obj['EnglishName']?></td>
											</tr>
											
										</table>
									</td>
									<td>
<?php if(in_array($default_student_id, $act_student_id_arr)) { ?>
<div class="lp_list">
    <ul>
    <? foreach ($data['friend_lps'] as $lp):?>
	<li class="theme_<?=$lp['theme'].' lp_status_published'?>">
	    	    
	    <div class="lp_content">
		<h1 title="<?=$lp['title']?>"><?=$lp['title']?></h1>
		<div class="lp_theme"></div>	
		<div class="lp_publish_info">
		    
		   
		    <span class="lp_info_draft" title="<?=$lp['modified']?>"><?=$langpf_lp2['admin']['item']['lastmodified']?>: <?=$lp['modified_days']?></span>
		    <div class="lp_publish_date">
			
			<? if ($lp['version']>=1): ?>
			    <a class="lp_info_published" href="http://<?=$eclass40_httppath?>src/iportfolio/?portfolio=<?=$lp['key']?>" target="_blank"><?=$langpf_lp2['admin']['item']['published']?></a>
			<? else: ?>
			   <? /* <a class="lp_info_published get_key" href="/home/portfolio/learning_portfolio/browse/student_view.php?key=<?=$lp['key']?>" target="_blank"><?=$langpf_lp2['admin']['item']['published']?></a> */ ?>
			   <a class="lp_info_published" href="javascript:openEclassWindow('student_view','<?=$lp['key']?>','<?=$UserID?>', 95)"><?=$langpf_lp2['admin']['item']['published']?></a>
			<? endif; ?>
			
			<span class="date_time" title="<?=$lp['published']?>"> <?=$lp['published_days']?></span>
			</div>
		  
		</div>		
	    </div>
	    <div class="lp_ref">
		<? if ($lp['version']>=1 && $lp['allow_like']): ?>
		<div class="lp_fb_like">
		    <div class="fb-like" data-href="<?=$lp['share_url']?>" data-send="false" data-action="like" data-font="arial" data-layout="standard" data-colorscheme="light" data-width="80" data-height="20" data-show-faces="false"></div>
		</div>
		<? endif ?>
	    </div>
	</li>  
    <? endforeach; ?>
    </ul>
</div>
<?php } else { ?>
										<table>
											<tr>
												<td><?=$ec_iPortfolio['suspend_result_inactive']?></td>
											</tr>
										</table>
<?php } ?>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_07.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_07.gif" width="13" height="37"></td>
					</tr>
					<tr>
						<td width="17" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_08.gif" width="17" height="17"></td>
						<td height="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_09.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_09.gif" width="64" height="17"></td>
						<td width="13" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_10_b.gif" width="13" height="17"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>" />
	<input type="hidden" name="WebPortfolioID" />
	<input type="hidden" name="FieldChanged" />
</FORM>

<? // ===================================== Body Contents (END) ============================= ?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
