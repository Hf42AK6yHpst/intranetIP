<?php

// Modifing by 
/*
 * 2016-11-29	Omas
 * 		- Create the page
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
//Extends iportfolio Class
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");

//Others:
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
intranet_auth();
iportfolio_auth("TSP");
intranet_opendb();

$lpf = new libpf_sturec();
$luser = new libuser($UserID);
$libacademic_year = new academic_year();
$subject = new subject();

$CurrentPage = "Student_SchoolRecords";

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPageName = $iPort['menu']['school_records'];

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

// Tab Menu Settings
$TabMenuArr = libpf_tabmenu::getSchoolRecordTags("stpaul_academic_points");
$html_menu_tab = $lpf->GET_TAB_MENU($TabMenuArr);

$fields = " StudentID, AcademicYearID, TermID, SubjectID, SCORE ";
$sql = "SELECT $fields FROM $eclass_db.SLP_STUDENT_SUBJECT_POINTS WHERE StudentID = '".$_SESSION['UserID']."'";
$result = $lpf->returnResultSet($sql);
$resultAssocArr = BuildMultiKeyAssoc($result, array('SubjectID','AcademicYearID','TermID'), array('SCORE'), 1);

$subjectIDArr = array_unique(Get_Array_By_Key($result , 'SubjectID'));
$subjectAssoc = BuildMultiKeyAssoc( $subject->Get_All_Subjects(), 'RecordID', array(Get_Lang_Selection('CH_DES','EN_DES')), 1);

$academicYearArr = array_unique(Get_Array_By_Key($result, 'AcademicYearID'));
$academicYearAssoc = BuildMultiKeyAssoc( $libacademic_year->Get_All_Year_List("en"),'AcademicYearID', array('AcademicYearName'), 1 );

$table = "";
//$table .= '<table id="table" width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC" style="text-align:center;">';
$table .='<table width="100%" border="0" cellpadding="5" cellspacing="1" class="table_b" style="text-align:center;">';
$table .= '<thead>';
$table .= '<tr class="tabletop">';
$table .= '<th rowspan="2" bgcolor="FFFFFF"></th>';
$countYear = 0;
foreach((array)$academicYearAssoc as $_ayId => $_ayNmae){
	if(in_array($_ayId,$academicYearArr)){
		$table .= '<th colspan="2">'.$_ayNmae.'</th>';
		$countYear++;
	}
}
$table .= '</tr>';
$table .= '<tr class="tabletop">';
for($i = 0 ; $i <$countYear ;$i++){
	$table .= '<th>'.$ec_iPortfolio['semester'].' 1</th>';
	$table .= '<th>'.$ec_iPortfolio['semester'].' 2</th>';
}
$table .= '</tr>';
$table .= '</thead>';
$table .= '<tbody>';
if(!empty($result)){
	$countSubject = 0;
	foreach((array)$subjectAssoc as $_subjectID => $_subjectName){
		$bgcolor = $countSubject %2 == 1 ? ' bgcolor="F3F3F3"' : '';
		if(in_array($_subjectID, (array)$subjectIDArr)){
			$table .= '<tr">';
			$table .= '<td class="tablelist_subject">'.$_subjectName.'</td>';
			foreach((array)$academicYearAssoc as $_ayId => $_ayNmae){
				if(in_array($_ayId,$academicYearArr)){
					$table .= '<td class="tabletext"'.$bgcolor.'>'.Get_String_Display($resultAssocArr[$_subjectID][$_ayId][1]).'</td>';
					$table .= '<td class="tabletext"'.$bgcolor.'>'.Get_String_Display($resultAssocArr[$_subjectID][$_ayId][2]).'</td>';
				}
			}
			$table .= '</tr>';
		}
		$countSubject++;
	}
}else{
	$table .= '<tr class="tablerow'.($i%2+1).'">';
	$table .= '<td align="center" colspan="'.$numCol.'">'.	$ec_iPortfolio['no_record_exist'].'</td>';
	$table .= '</tr>';
}
$table .= '</tbody>';
$table .= '</table>';

?>
<?php echo $html_menu_tab ?>
<?php echo $table?>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>