<?php

// Modifing by Paul

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

//Others:
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
intranet_auth();
iportfolio_auth("TSP");
intranet_opendb();

# define the page title and table size

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPageName = $iPort['menu']['school_records'];

$lpf = new libpf_slp();
$luser = new libuser($UserID);
$StudentID = $luser->UserID;
###  FOR UCCKE, default current year if existed
$sql2 = "Select DISTINCT a.Year FROM {$eclass_db}.ACTIVITY_STUDENT as a WHERE a.UserID = '$StudentID' ORDER BY Year";
$ActivityYearArr = $lpf->returnArray($sql2);
// 2020-04-07 (Philips) Use YearName Arr for curr Year checking
$ActivityYearNameArr = Get_Array_By_Key($ActivityYearArr, 'Year');

if($sys_custom['ipf']['school_records']['uccke'] && !isset($_GET['ChooseYear'])){
	$currYear = getCurrentAcademicYear();
	// if current year not exist, use all year
	if(in_array($currYear, $ActivityYearNameArr)){
		$_GET['ChooseYear'] = getCurrentAcademicYear();
	}
}

switch($_SESSION['UserType'])
{
	case 2:
		$CurrentPage = "Student_SchoolRecords";
		$StudentID = $_SESSION['UserID'];
		break;
	case 3:
		$CurrentPage = "Parent_SchoolRecords";
		$StudentID = $ck_current_children_id;
		break;
	default:
		break;
}

////////////////////////////////////////////////////////
///// TABLE SQL
if ($order=="") $order=1;
if ($field=="") $field=0;
$li = new libdbtable2007($field, $order, $pageNo);
if($sys_custom['project']['NCS']){
	$li->field_array = array("a.Year", "a.Semester", "a.ActivityName", "Role", "Performance", "ActivityDate", "Attendance");
	$ChooseYear = $_GET['ChooseYear'];
	if($ChooseYear!="")
	{$conds = "AND a.Year = '$ChooseYear'";}
	
	$sql = "SELECT a.Year, a.Semester, a.ActivityName,  IF(a.Role='', '--', a.Role) AS Role, ";
	$sql .= "IF(a.Performance='', '--', a.Performance) AS Performance, CONCAT('".$Lang['General']['From']." ', d.ActivityDateStart, '<br> ".$Lang['General']['To']." ', d.ActivityDateEnd) as ActivityDate, ";
	$sql .= "(CASE ea.RecordStatus WHEN '1' THEN '".$Lang['StudentAttendance']['Present']."' WHEN '2' THEN '".$Lang['StudentAttendance']['Waived']."'  WHEN '3' THEN '".$i_Profile_Absent."'  WHEN '4' THEN '".$i_Profile_Late."' END ) as Attendance ";
	$sql .= "FROM {$intranet_db}.PROFILE_STUDENT_ACTIVITY  as a ";
	$sql .= "LEFT JOIN {$intranet_db}.INTRANET_ENROL_EVENTINFO  e ON e.EnrolEventID=a.eEnrolRecordID ";
	$sql .= "LEFT JOIN {$intranet_db}.INTRANET_ENROL_EVENT_ATTENDANCE ea ON ea.StudentID=a.UserID AND ea.EnrolEventID=e.EnrolEventID ";
	$sql .= "LEFT JOIN {$intranet_db}.INTRANET_ENROL_EVENT_DATE d ON ea.EventDateID=d.EventDateID ";
	$sql .= "WHERE a.UserID = '$StudentID' $conds";

	$totalActivitiesSql = "SELECT COUNT(*) FROM INTRANET_ENROL_EVENT_ATTENDANCE WHERE StudentID='".$StudentID."'";
	$totalActivities = current($li->returnVector($totalActivitiesSql));
	$attendanceSummarySql = "SELECT (CASE RecordStatus WHEN '1' THEN '".$Lang['StudentAttendance']['Present']."' WHEN '2' THEN '".$Lang['StudentAttendance']['Waived']."'  WHEN '3' THEN '".$i_Profile_Absent."'  WHEN '4' THEN '".$i_Profile_Late."' END ) as Attendance , COUNT(*) as days, CONCAT(ROUND((COUNT(*)/".$totalActivities.")*100,2),'%') as percent FROM INTRANET_ENROL_EVENT_ATTENDANCE WHERE StudentID='".$StudentID."' GROUP BY RecordStatus";
	$summary = $li->returnArray($attendanceSummarySql);
	foreach($summary as $s){
		$summaryTable .= $s['Attendance'].": ".$s['days']." (".$s['percent'].")"." ";
	}
}else{
	$li->field_array = array("a.Year", "a.Semester", "a.ActivityName", "a.Role", "a.Performance");
	if($sys_custom['LivingHomeopathy']){
		$li->field_array[] = "Attendance";
	}
	$li->field_array[] = "a.ModifiedDate";
	$ChooseYear = $_GET['ChooseYear'];
	if($ChooseYear!="")
	{$conds = "AND a.Year = '$ChooseYear'";}
	$sql = "SELECT
	          a.Year,
			  if(a.Semester = '' , '".$Lang['General']['WholeYear']."',a.Semester) as  'Semester',
	          a.ActivityName,
	          if(a.Role='', '--', a.Role),
	          if(a.Performance='', '--', a.Performance),";
	if($sys_custom['LivingHomeopathy']){
		$sql .= " CONCAT('<a href=\"javascript:newWindow(\'view_attendance.php?RecordID=',a.RecordID,'\',19);\"><img src=\"$image_path/$LAYOUT_SKIN/eOffice/icon_viewstat.gif\" border=\"0\" /></a>') as Attendance, ";
	}
	$sql .= " a.ModifiedDate FROM {$eclass_db}.ACTIVITY_STUDENT as a 
			WHERE 
	    			a.UserID = '$StudentID'
	    			$conds
	      ";
}
// TABLE INFO
$li->sql = $sql;
$li->db = $intranet_db;
$li->title = $ec_iPortfolio['record'];
$li->no_msg = $no_record_msg;
$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
$li->no_col = count($li->field_array)+1;
//$li->noNumber = true;
//list($year) = $li->db_db_query($sql);

$pageSizeChangeEnabled = true;
		/*
		for($i=0; $i<sizeof($ActivityArr); $i++)
		{
			list($year) = $ActivityArr[$i];
			echo $i.";".$year;
		}
		*/
		

$li->table_tag = "<table border='0' cellpadding='10' cellspacing='0' width='100%'>";
//$li->row_alt = array("#FFFFFF", "F3F3F3");
$li->row_alt = array("", "");
$li->row_height = 20;
$li->sort_link_style = "class='tbheading'";


// TABLE COLUMN
if($sys_custom['project']['NCS']){
	$li->column_list .= "<td height='25' nowrap align='center'>".$li->column(0, "#", 1)."</td>\n";
	$li->column_list .= "<td width='15%'>".$li->column(1, $ec_iPortfolio['year'], 1)."</td>\n";
	$li->column_list .= "<td width='15%'>".$li->column(2, $ec_iPortfolio['term'], 1)."</td>\n";
	$li->column_list .= "<td width='20%'>".$li->column(3, $ec_iPortfolio['activity_name'], 1)."</td>\n";
	$li->column_list .= "<td width='15%'>".$li->column(4, $ec_iPortfolio['role'], 1)."</td>\n";
	$li->column_list .= "<td width='15%'>".$li->column(5, $ec_iPortfolio['performance'], 1)."</td>\n";
	$li->column_list .= "<td width='15%'>".$li->column(5, $Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['ActivityDate'], 1)."</td>\n";
	$li->column_list .= "<td width='15%'>".$li->column(6, $ec_iPortfolio['attendance'], 1)."</td>\n";
	$li->column_array = array(0,0,0,0,0,0,0);
}else{
	$col = 0;	
	$li->column_list .= "<td class='tbheading' height='25' nowrap align='center'><span class=\"tabletoplink\">#</span></td>\n";
	$li->column_list .= "<td width='15%'>".$li->column($col++, $ec_iPortfolio['year'], 1)."</td>\n";
	$li->column_list .= "<td width='15%'>".$li->column($col++, $ec_iPortfolio['term'], 1)."</td>\n";
	$li->column_list .= "<td width='20%'>".$li->column($col++, $ec_iPortfolio['activity_name'], 1)."</td>\n";
	$li->column_list .= "<td width='15%'>".$li->column($col++, $ec_iPortfolio['role'], 1)."</td>\n";
	$li->column_list .= "<td width='15%'>".$li->column($col++, $ec_iPortfolio['performance'], 1)."</td>\n";
	if($sys_custom['LivingHomeopathy']){
		$li->column_list .= "<td width='5%'>".$li->column($col++, $eEnrolment['attendance_title'], 1)."</td>\n";
	}
	$li->column_list .= "<td width='15%'>".$li->column($col++, $ec_iPortfolio['SAMS_last_update'], 1)."</td>\n";
	$li->column_array = array_fill(0,$col,0);
}
$SelectBoxContent = $lpf->displayYearSelectBox($StudentID, $ClassName,$_GET['ChooseYear']);

//////////////////////////////////////////////

# define the page title and table size
//$template_width = "98%";
//$template_left_menu = getLeftMenu($menu_arr, $menu_arr[3][1]);
//echo getBodyBeginning($template_pages, $template_width, 1, "red", "", $template_left_menu, $template_table_top_right);
### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

// Tab Menu Settings
/*
$TabMenuArr = array();
if($lpf->access_config['merit'] == 1)
	$TabMenuArr[] = array("../merit/index.php", $ec_iPortfolio['title_merit'], 0);
if($lpf->access_config['assessment_report'] == 1)
	$TabMenuArr[] = array("../assessment/index.php", $ec_iPortfolio['title_academic_report'], 0);
if($lpf->access_config['activity'] == 1)
	$TabMenuArr[] = array("../activity/index.php", $ec_iPortfolio['title_activity'], 1);
if($lpf->access_config['award'] == 1)
	$TabMenuArr[] = array("../award/index.php", $ec_iPortfolio['title_award'], 0);
if($lpf->access_config['teacher_comment'] == 1)
	$TabMenuArr[] = array("../comment/index.php", $ec_iPortfolio['title_teacher_comments'], 0);
if($lpf->access_config['attendance'] == 1)
	$TabMenuArr[] = array("../attendance/index.php", $ec_iPortfolio['title_attendance'], 0);
if($lpf->access_config['service'] == 1)
	$TabMenuArr[] = array("../service/index.php", $ec_iPortfolio['service'], 0);
*/
$TabMenuArr = libpf_tabmenu::getSchoolRecordTags("activity");
?>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
-->
</script>


<FORM name="form1" method="GET">
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td valign="top">
        <table width="100%"  border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td>
              <?=$lpf->GET_TAB_MENU($TabMenuArr);?>
				 		</td>
          </tr> 
          <tr>
            <td>
              <table border="0" cellpadding="2" cellspacing="0" width="100%">
                <tbody>
                  <tr>
                    <td><?= $SelectBoxContent?></td>                    
                  </tr>
                  <tr>
                  	<td><?= $summaryTable?></td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td align="center">
              <?= $li->displayPlain() ?>
              <table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
<?php if ($li->navigationHTML!="") { ?>
                <tr class='tablebottom'>
                  <td class="tabletext" align="right"><?=$li->navigation(1)?></td>
                </tr>
<?php } ?>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
    
  <input type="hidden" name="ClassName" value="<?=$ClassName?>">
  <input type="hidden" name="StudentID" value="<?=$StudentID?>">
  <input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>">
  <input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
  <input type="hidden" name="order" value="<?php echo $li->order; ?>">
  <input type="hidden" name="field" value="<?php echo $li->field; ?>" />
  <input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
  <input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</FORM>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
