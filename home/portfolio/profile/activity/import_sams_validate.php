<?php
/*
 * 	Log
 * 	
 * 	Purpose: import activity validation
 * 
 * 	Date:	2016-01-25 [Cameron]
 * 			create this file
 * 
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");		// authen
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

intranet_opendb();

$lpf = new libportfolio2007();
$lpf->CHECK_ACCESS_IPORTFOLIO();
$lpf->ADMIN_ACCESS_PAGE();

$linterface = new interface_html("popup.html");
$limport = new libimporttext();

# Initialization
$result = array();
$param = '';
$studentOfYear = array();

### get header
$format_array = array("School Year", "STA Duration", "Class Level", "Class Name", "Class Number", "Registration Number", "Student Name (English)", "Student Name (Chinese)", "Type", "Activity (Chinese)", "Post (Chinese)", "Performance (Chinese)");


### get csv data
$filepath = $_FILES['userfile']['tmp_name'];
$data = $limport->GET_IMPORT_TXT($filepath, $incluedEmptyRow=0, $lineBreakReplacement='<!--LineBreak-->');
$limport->SET_CORE_HEADER($format_array);
if (!$limport->VALIDATE_HEADER($data, true) || !$limport->VALIDATE_DATA($data)) {
	header("Location: import_sams.php?$param&returnMsgKey=WrongCSVHeader");
	exit();
}


# tag information	
$CurrentPage = "SAMS_import_activity";
$title = $ec_iPortfolio['SAMS_import_activity'];
$TAGS_OBJ[] = array($title,"",0);
$MODULE_OBJ["title"] = $title;

	
# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION[] = array();
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


# handle return message
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);


### step display
$htmlAry['generalImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep=2);


### delete old temp data
$table = $eclass_db.".TEMP_IMPORT_SAMS_ACTIVITY";
$sql = "Delete From ".$table." Where LoginUserID = '".$_SESSION['UserID']."'";
$successAry['deleteOldTempData'] = $lpf->db_db_query($sql);


## get academic year term array
$yearTerm_ary = $lpf->buildAcademicYearTermAry(); 				
$academicYear_ary = array_keys($yearTerm_ary);


### analyse data
array_shift($data);
$numOfData = count($data);
$errorMsgAssoAry = array();
$insertAry = array();
for ($i=0; $i<$numOfData; $i++) {
	$_rowNum = $i+2;
	$_row = $data[$i];
	$_schoolYear = str_replace(' ', '', trim($_row[0]));
	$_schoolTerm = trim($_row[1]);
	$_classLevel = trim($_row[2]);
	$_className = trim($_row[3]);
	$_classNumber = trim($_row[4]);
	$_registerNumber = trim($_row[5]);	// webSAMSRegNo
	if (substr($_registerNumber,0,1) != '#') {
		$_registerNumber = '#'.$_registerNumber;	// webSAMSRegNo
	}
	$_studentNameEng = trim($_row[6]);
	$_studentNameChi = trim($_row[7]);
	$_type = trim($_row[8]);				// E - Activity, S - Service
	$_activityChi = trim($_row[9]);
	$_postChi = trim($_row[10]);
	$_performanceChi = trim($_row[11]);
	
	$_y_len = strlen($_schoolYear); 
	if ($_y_len == 4 || $_y_len == 9) {		// assume year in this format: 2016	or 2016-2017
		$_year = substr($_schoolYear,0,4);
	}
	else if ($_y_len == 2 || $_y_len == 5) {	// assume year in this format: 16 or 16/17
		$_year = substr(date('Y'),0,2).substr($_schoolYear,0,2);
	}
	else {
		$_year = 0;
	}

	if (!isset($studentOfYear[$_year]) && $yearTerm_ary[$_year]['AcademicYearID']) {
		$studentOfYear[$_year] = $lpf->getStudentByAcademicYear($yearTerm_ary[$_year]['AcademicYearID']);

		$tmpStudentClass_ary = BuildMultiKeyAssoc($studentOfYear[$_year], array('ClassName','ClassNumber'), 'UserID',1);
		$studentClass_ary[$_year] = $tmpStudentClass_ary;
		unset($tmpStudentClass_ary);

		$detail_field = array('ClassLevel','ClassName','ClassNumber','UserID','StudentNameEng','StudentNameChi'); 
		$studentDetail_ary[$_year] = BuildMultiKeyAssoc($studentOfYear[$_year], array('WebSAMSRegNo'),$detail_field,1);

		$studentWebSAMSRegNo_ary[$_year] = BuildMultiKeyAssoc($studentOfYear[$_year], 'UserID','WebSAMSRegNo',1);
	}

	
	## check 1: academic year
	if (!in_array($_year,(array)$academicYear_ary)) {
		$errorMsgAssoAry[$_rowNum][] = 'academicYearNotFound';
	}
	else {
		## check 2: academic year-term
		if (is_numeric($_schoolTerm)) {
			$_termID = "Term{$_schoolTerm}ID";
			$_termName = "Term{$_schoolTerm}Name";	
		}
		else {
			$_termID = "";
		}
		 
		if (($_termID == "") || !$yearTerm_ary[$_year][$_termID]) {
			$errorMsgAssoAry[$_rowNum][] = 'academicYearTermNotFound';
		}
		else {
			## check 3: webSAMS #
			$_userID = '';
			if (($_registerNumber != '#') && isset($studentDetail_ary[$_year][$_registerNumber]['UserID'])) {
				$_userID = $studentDetail_ary[$_year][$_registerNumber]['UserID'];
			}
			else if ($_className != '' && $_classNumber != '' && isset($studentClass_ary[$_year][$_className][$_classNumber])) {
				$_userID = $studentClass_ary[$_year][$_className][$_classNumber];
				$_registerNumber = $studentWebSAMSRegNo_ary[$_year][$_userID];
				if (substr($_registerNumber,0,2) == '##') {	// empty webSAMSNo
					$errorMsgAssoAry[$_rowNum][] = 'webSAMSRegNoNotFound';
				}
			}
			
			if ($_userID == '') {
				$errorMsgAssoAry[$_rowNum][] = 'studentNotFound';
			}
			else {	// get ClassLevel, ClassName, ClassNumber, StudentNameEng, StudentNameChi from system if conflict with that in csv
				$_classLevel = ($_classLevel != $studentDetail_ary[$_year][$_registerNumber]['ClassLevel']) ? $studentDetail_ary[$_year][$_registerNumber]['ClassLevel'] : $_classLevel;  
				$_className = ($_className != $studentDetail_ary[$_year][$_registerNumber]['ClassName']) ? $studentDetail_ary[$_year][$_registerNumber]['ClassName'] : $_className;
				$_classNumber = ($_classNumber != $studentDetail_ary[$_year][$_registerNumber]['ClassNumber']) ? $studentDetail_ary[$_year][$_registerNumber]['ClassNumber'] : $_classNumber;
				$_studentNameEng = ($_studentNameEng != $studentDetail_ary[$_year][$_registerNumber]['StudentNameEng']) ? $studentDetail_ary[$_year][$_registerNumber]['StudentNameEng'] : $_studentNameEng;
				$_studentNameChi = ($_studentNameChi != $studentDetail_ary[$_year][$_registerNumber]['StudentNameChi']) ? $studentDetail_ary[$_year][$_registerNumber]['StudentNameChi'] : $_studentNameChi;
			}
		}	// check 2
	}	// check 1
	
	if (($_type != 'E') && ($_type != 'S')) {
		$errorMsgAssoAry[$_rowNum][] = 'wrongType';
	}
		  
	if ($_activityChi == '') {
		$errorMsgAssoAry[$_rowNum][] = 'emptyActivity';
	}

	if ($_performanceChi == '') {
		$errorMsgAssoAry[$_rowNum][] = 'emptyPerformance';
	}
	
		
	$insertAry[] = " ('".$_SESSION['UserID']."', '".$_rowNum."', '".$lpf->Get_Safe_Sql_Query($_schoolYear)."', '".
							$lpf->Get_Safe_Sql_Query($_schoolTerm)."', '".$lpf->Get_Safe_Sql_Query($_classLevel)."', '".
							$lpf->Get_Safe_Sql_Query($_className)."', '".$lpf->Get_Safe_Sql_Query($_classNumber)."', '".
							$lpf->Get_Safe_Sql_Query($_registerNumber)."', '".$lpf->Get_Safe_Sql_Query($_studentNameEng)."', '".
							$lpf->Get_Safe_Sql_Query($_studentNameChi)."', '".$lpf->Get_Safe_Sql_Query($_type)."', '".
							$lpf->Get_Safe_Sql_Query($_activityChi)."', '".$lpf->Get_Safe_Sql_Query($_postChi)."', '".
							$lpf->Get_Safe_Sql_Query($_performanceChi)."', '".$_userID."', '".$yearTerm_ary[$_year]['AcademicYearID']."', '".
							$lpf->Get_Safe_Sql_Query($yearTerm_ary[$_year]['YearNameEN'])."', '".
							$lpf->Get_Safe_Sql_Query($yearTerm_ary[$_year][$_termID])."', '".
							$lpf->Get_Safe_Sql_Query($yearTerm_ary[$_year][$_termName])."', now()) ";
}

### simple statistics
$numOfErrorRow = count($errorMsgAssoAry);
$numOfSuccessRow = $numOfData - $numOfErrorRow;


### insert csv data to temp table
$numOfInsert = count($insertAry);
if ($numOfInsert > 0) {
	$insertChunkAry = array_chunk($insertAry, 1000);
	$numOfChunk = count($insertChunkAry);
	
	for ($i=0; $i<$numOfChunk; $i++) {
		$_insertAry = $insertChunkAry[$i];
		
		$sql = "Insert Into $table
					(LoginUserID,RowNumber,SchoolYear,SchoolTerm,ClassLevel,ClassName,ClassNumber,RegisterNumber,StudentNameEng,StudentNameChi,
					Type,ActivityName,Role,Performance,UserID,AcademicYearID,Year,YearTermID,Semester,InputDate)
				Values ".implode(', ', (array)$_insertAry);
		$successAry['insertData'][] = $lpf->db_db_query($sql);
	}
}


# validation result to display
if ($numOfErrorRow > 0) {
	$numOfErrorDisplay = '<span class="tabletextrequire">'.$numOfErrorRow.'</span>';
}
else {
	$numOfErrorDisplay = 0;
}

$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$title.'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= "&nbsp;";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['SuccessfulRecord'].'</td>'."\n";
		$x .= '<td>'.$numOfSuccessRow.'</td>'."\n";
	$x .= '</tr>'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['FailureRecord'].'</td>'."\n";
		$x .= '<td>'.$numOfErrorDisplay.'</td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\r\n";
$htmlAry['importInfoTbl'] = $x;


# error display
$x = '';
if ($numOfErrorRow > 0) {
	$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\n";
		$x .= '<thead>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<th>'.$Lang['General']['ImportArr']['Row'].'</th>'."\n";
				$x .= '<th>'.$Lang['General']['SchoolYear'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['SchoolTerm'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['ClassLevel'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['ClassName'].'</th>'."\n";
				$x .= '<th>'.$Lang['General']['ClassNumber'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['WebSAMSRegNo'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['StudentNameEng'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['StudentNameChi'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Type'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Activity'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Role'].'</th>'."\n";
				$x .= '<th>'.$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Performance'].'</th>'."\n";
				$x .= '<th>'.$Lang['General']['Remark'].'</th>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</thead>'."\n";
		$x .= '<tbody>'."\n";
		
			foreach ((array)$errorMsgAssoAry as $_rowNum => $_errorAry) {
				$aryIndex = $_rowNum - 2;
				$_row = $data[$aryIndex];
				$_schoolYear = str_replace(' ', '', trim($_row[0]));
				$_schoolTerm = trim($_row[1]);
				$_classLevel = trim($_row[2]);
				$_className = trim($_row[3]);
				$_classNumber = trim($_row[4]);
				$_registerNumber = trim($_row[5]);		// webSAMSRegNo
				$_studentNameEng = trim($_row[6]);
				$_studentNameChi = trim($_row[7]);
				$_type = trim($_row[8]);				// E - Activity, S - Service
				$_activityChi = trim($_row[9]);
				$_postChi = trim($_row[10]);
				$_performanceChi = trim($_row[11]);
				
				$_errorDisplayAry = array();

				if (in_array('academicYearNotFound', $_errorAry)) {
					$_schoolYear = '<span class="tabletextrequire">'.($_schoolYear?$_schoolYear:'***').'</span>';
					$_errorDisplayAry[] = $Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['AcademicYearNotFound'];
				}
				if (in_array('academicYearTermNotFound', $_errorAry)) {
					$_schoolTerm = '<span class="tabletextrequire">'.($_schoolTerm?$_schoolTerm:'***').'</span>';
					$_errorDisplayAry[] = $Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['AcademicYearTermNotFound'];
				}
				
				if (in_array('studentNotFound', $_errorAry)) {
					if ($_registerNumber=='' && $_className=='' && $_classNumber=='') {
						$_registerNumber = '<span class="tabletextrequire">***</span>';
						$_className = '<span class="tabletextrequire">***</span>';
						$_classNumber = '<span class="tabletextrequire">***</span>';
					}
					else if ($_registerNumber != '') {
						$_registerNumber = '<span class="tabletextrequire">'.$_registerNumber.'</span>';
					}
					else {
						$_className = '<span class="tabletextrequire">'.$_className.'</span>';
						$_classNumber = '<span class="tabletextrequire">'.$_classNumber.'</span>';
					}
					$_studentNameEng = '<span class="tabletextrequire">'.$_studentNameEng.'</span>';
					
					$_errorDisplayAry[] = $Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['StudentNotFound'];
				}
				
				if (in_array('webSAMSRegNoNotFound', $_errorAry)) {
					$_registerNumber = '<span class="tabletextrequire">***</span>';
					$_errorDisplayAry[] = $Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['WebSAMSRegNoNotFound'];
				}
				
				if (in_array('wrongType', $_errorAry)) {
					$_type = '<span class="tabletextrequire">'.($_type?$_type:'***').'</span>';
					$_errorDisplayAry[] = $Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['WrongType'];
				}
				if (in_array('emptyActivity', $_errorAry)) {
					$_activityChi = '<span class="tabletextrequire">'.($_activityChi?$_activityChi:'***').'</span>';
					$_errorDisplayAry[] = $Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['EmptyActivity'];
				}
				if (in_array('emptyPerformance', $_errorAry)) {
					$_performanceChi = '<span class="tabletextrequire">'.($_performanceChi?$_performanceChi:'***').'</span>';
					$_errorDisplayAry[] = $Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['EmptyPerformance'];
				}
				
				
				$_errorDisplay = '- '.implode('<br />- ', $_errorDisplayAry);
				
				$x .= '<tr>'."\n";
					$x .= '<td>'.$_rowNum.'</td>'."\n";
					$x .= '<td>'.$_schoolYear.'</td>'."\n";
					$x .= '<td>'.$_schoolTerm.'</td>'."\n";
					$x .= '<td>'.$_classLevel.'</td>'."\n";
					$x .= '<td>'.$_className.'</td>'."\n";
					$x .= '<td>'.$_classNumber.'</td>'."\n";
					$x .= '<td>'.$_registerNumber.'</td>'."\n";
					$x .= '<td>'.$_studentNameEng.'</td>'."\n";
					$x .= '<td>'.$_studentNameChi.'</td>'."\n";
					$x .= '<td>'.$_type.'</td>'."\n";
					$x .= '<td>'.$_activityChi.'</td>'."\n";
					$x .= '<td>'.$_postChi.'</td>'."\n";
					$x .= '<td>'.$_performanceChi.'</td>'."\n";
					$x .= '<td>'.$_errorDisplay.'</td>'."\n";
				$x .= '</tr>'."\n";
			}
		$x .= '</tbody>'."\n";
	$x .= '</table>'."\n";
}
$htmlAry['errorTbl'] = $x;



### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Import'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", ($numOfErrorRow>0)? true : false, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['backBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

?>
<script type="text/JavaScript" language="JavaScript">
function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

function goSubmit() {
	$('form#form1').attr('action', 'import_sams_update.php').submit();
}

function goBack() {
	window.location = 'import_sams.php?<?=$param?>';
}
function goCancel() {
	self.close();
}

</script>
<form id="form1" name="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br />
	
	<?=$htmlAry['generalImportStepTbl']?>
	
	<div class="table_board">
		<?=$htmlAry['importInfoTbl']?>
		<?=$htmlAry['errorTbl']?>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>