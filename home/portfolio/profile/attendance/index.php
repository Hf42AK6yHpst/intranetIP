<?php

// Modifing by 

if ($page_size_change!="") { setcookie("ck_page_size", $numPerPage, 0, "", "", 0);}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

//Others:
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
//include_once("$admin_root_path/src/includes/php/lib-table.php");

intranet_auth();
iportfolio_auth("TSP");
intranet_opendb();

$lpf = new libpf_slp();
$luser = new libuser($UserID);
if($sys_custom['ipf']['school_records']['uccke'] && !isset($_GET['ChooseYear'])){
	$_GET['ChooseYear'] = getCurrentAcademicYear();
}
//$lpf->accessControl("attendance");
switch($_SESSION['UserType'])
{
	case 2:
		$StudentID = $_SESSION['UserID'];
		$CurrentPage = "Student_SchoolRecords";
		break;
	case 3:
		$StudentID = $ck_current_children_id;
		$CurrentPage = "Parent_SchoolRecords";
		break;
	default:
		break;
}
/*
if ($ck_memberType=="S")
{
	list($ClassName, $StudentID) = $lpf->getStudentID($ck_user_id);

				# define the navigation
	$template_pages = Array(
					Array($ec_iPortfolio['attendance'], "")
					);

}
else if($ck_memberType=="P")
{
	$template_table_top_right = $lpf->getChildrenListInfo($StudentID, $ClassName);

	$template_pages = Array(
					Array($ec_iPortfolio['student_list'], "../../school_records_children.php"),
					Array($ec_iPortfolio['attendance'], "")				);
}
else
{
	$template_table_top_right = $lpf->getStudentListInfo($ClassName, $StudentID);
	
	# define the navigation
	if($ck_is_alumni)
	{
		$template_pages = Array(
					Array($ec_iPortfolio['alumni_list'], "../../school_records_alumni.php"),
					Array($ck_alumni_year, "../../school_records_alumni_year.php?my_year=$ck_alumni_year"),
					Array($ec_iPortfolio['attendance'], "")
					);
	}
	else
	{
		$template_pages = Array(
					Array($ec_iPortfolio['class_list'], "../../school_records.php"),
					Array($ec_iPortfolio['student_list'], "../../school_records_class.php?ClassName=$ClassName"),
					Array($ec_iPortfolio['attendance'], "")
					);
	}
}
*/
$tableContent = $lpf->displayStudentAttendanceSummary($StudentID, $ClassName,$_GET['ChooseYear']);
$SelectBoxContent = $lpf->displayYearSelectBox($StudentID, $ClassName,$_GET['ChooseYear'], 1);

# define the page title and table size

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPageName = $iPort['menu']['school_records'];

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

// Tab Menu Settings
/*
$TabMenuArr = array();
if($lpf->access_config['merit'] == 1)
	$TabMenuArr[] = array("../merit/index.php", $ec_iPortfolio['title_merit'], 0);
if($lpf->access_config['assessment_report'] == 1)
	$TabMenuArr[] = array("../assessment/index.php", $ec_iPortfolio['title_academic_report'], 0);
if($lpf->access_config['activity'] == 1)
	$TabMenuArr[] = array("../activity/index.php", $ec_iPortfolio['title_activity'], 0);
if($lpf->access_config['award'] == 1)
	$TabMenuArr[] = array("../award/index.php", $ec_iPortfolio['title_award'], 0);
if($lpf->access_config['teacher_comment'] == 1)
	$TabMenuArr[] = array("../comment/index.php", $ec_iPortfolio['title_teacher_comments'], 0);
if($lpf->access_config['attendance'] == 1)
	$TabMenuArr[] = array("../attendance/index.php", $ec_iPortfolio['title_attendance'], 1);
if($lpf->access_config['service'] == 1)
	$TabMenuArr[] = array("../service/index.php", $ec_iPortfolio['service'], 0);
*/
$TabMenuArr = libpf_tabmenu::getSchoolRecordTags("attendance");

# define the navigation, page title and table size
//$template_width = "98%";
//$template_left_menu = getLeftMenu($menu_arr, $menu_arr[6][1]);
//echo getBodyBeginning($template_pages, $template_width, 1, "red", "", $template_left_menu, $template_table_top_right);
?>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->
</script>
  
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<FORM name="form1" method="GET">
                                <tr>
                                  <td valign="top">
									<table width="100%"  border="0" cellspacing="5" cellpadding="0">
                                      <tr><td>
                      					<?
                                        echo $lpf->GET_TAB_MENU($TabMenuArr);
                                        ?>
										</td>
                                        	</tr>
                                        	
                                        	<tr>
                                        		<td>
                                        				<table border="0" cellpadding="1" cellspacing="0" width="100%">

																	<tr>
																		<td><?= $SelectBoxContent?></td>
																	</tr>
															</table>
                                        		</td>
                                        	</tr>
                                        	
                                        	
                                      <tr>
                                        <td align="center">
											<?= $tableContent?>
											</td>
                                </tr>
                              </table>
							  </td>
                             </tr>

<input type="hidden" name="ClassName" value="<?=$ClassName?>">
<input type="hidden" name="StudentID" value="<?=$StudentID?>">
<input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>">
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=numPerPage value="<?=$numPerPage?>">
<input type=hidden name="page_size_change">

</FORM>
</table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
