<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("TSP");
//if(!@in_array($_SESSION['UserType'], array(1, 2, 3)))
//	header("Location: /");

//update_login_session();

//intranet_auth();

//include_once("$admin_root_path/src/portfolio/new_header.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($eclass_filepath."/src/includes/php/lib-db.php");
include_once($eclass_filepath."/src/includes/php/lib-table.php");
//include_once($eclass_filepath."/src/portfolio/new_header.php");


//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_opendb();

$lpf = new libportfolio();
$lpf_ui = new libportfolio2007();
$luser = new libuser($UserID);

//$lpf->accessControl("attendance");
/*
if ($ck_memberType=="S")
{
	list($ClassName, $StudentID) = $lpf->getStudentID($ck_user_id);

				# define the navigation
	$template_pages = Array(
					Array($ec_iPortfolio['attendance']." (".$year." - ".$semester.")", "")
					);

} else
{

		# define the navigation
	$template_pages = Array(
					Array($ec_iPortfolio['attendance']." (".$year." - ".$semester.")", "")
		);

}
*/
# Display according to user type
switch($_SESSION['UserType'])
{
	case 1:			# Teacher
		break;
	case 2:			# Student
		$StudentID = $_SESSION['UserID'];
		$CurrentPage = "Student_SchoolRecords";

	# define the navigation
	$template_pages = Array(
					Array($ec_iPortfolio['merit']." (".$year." - ".$semester.")", "")
					);
		break;
	case 3:
		$StudentID = $ck_current_children_id;
		$CurrentPage = "Parent_SchoolRecords";
		break;
	default:
		break;
}

////////////////////////////////////////////////////////
///// TABLE SQL
if ($order=="") $order=1;
if ($field=="") $field=0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.Year", "a.Semester", "a.AttendanceDate", "a.RecordType", "a.DayType", "a.Reason", "a.ModifiedDate");

# handle semester different problem
list($SemesterArr, $ShortSemesterArr) = $lpf->getSemesterNameArrFromIP();
list($SemList, $List1, $List2, $List3) = $lpf->getSemesterMatchingLists($semester, $SemesterArr);

$counting = $lpf->getAttendanceCountingByYearSemester($StudentID, $year, $semester);
$absentCount = ($counting[1]=="") ? '0' : $counting[1];
$lateCount = ($counting[2]=="") ? '0' : $counting[2];
$earlyleaveCount = ($counting[3]=="") ? '0' : $counting[3];

if($semester!="" && $semester!=$ec_iPortfolio['whole_year'] && $SemList != "")
{	
	$conds = "AND (a.Semester = '$semester' OR a.Semester IN ({$SemList}))";
}

$sql = "SELECT
              a.Year,
			  if(INSTR('{$List1}', a.Semester),'".$SemesterArr[0]."',if(INSTR('{$List2}', a.Semester),'".$SemesterArr[1]."',if(INSTR('{$List3}', a.Semester),'".$SemesterArr[2]."',a.Semester))),
              DATE_FORMAT(a.AttendanceDate,'%Y-%m-%d'),
              CASE a.RecordType
                   WHEN 1 THEN '$i_Profile_Absent'
                   WHEN 2 THEN '$i_Profile_Late'
                   WHEN 3 THEN '$i_Profile_EarlyLeave'
                   ELSE '-' END,
              CASE a.DayType
                   WHEN 1 THEN '$i_DayTypeWholeDay'
                   WHEN 2 THEN '$i_DayTypeAM'
                   WHEN 3 THEN '$i_DayTypePM'
                   ELSE '-' END,
              a.Reason,
              a.ModifiedDate FROM {$eclass_db}.ATTENDANCE_STUDENT as a
        WHERE a.UserID = '$StudentID'
		AND a.Year = '$year'
		$conds
              ";

// TABLE INFO
$li->sql = $sql;
$li->db = $intranet_db;
$li->title = $usertype_s;
$li->no_msg = $no_record_msg;
$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
$li->page_size = 999;
$li->no_col = 8;
//$li->noNumber = true;

$li->table_tag = "<table width='100%' border='0' cellpadding='10' cellspacing='0'>";
$li->row_alt = array("#FFFFFF", "F3F3F3");
$li->row_height = 20;
$li->sort_link_style = "class='tbheading'";


// TABLE COLUMN
//$li->column_list .= "<td class='tbheading' height='25' bgcolor='#CFE6FE' nowrap align='center'>#</span></td>\n";
$li->column_list .= "<td height='25' nowrap align='center'>".$li->column(0, "#", 1)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column(1, $ec_iPortfolio['year'], 1)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(2, $ec_iPortfolio['semester'], 1)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(3, $ec_iPortfolio['date'], 1)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column(4, $ec_iPortfolio['stype'], 1)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column(5, $ec_iPortfolio['period'], 1)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column(6, $ec_iPortfolio['reason'], 1)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(7, $ec_iPortfolio['SAMS_last_update'], 1)."</td>\n";
$li->column_array = array(0,0,0,0,0,0,0);

//////////////////////////////////////////////

# define the navigation, page title and table size
//$template_width = "95%";
//echo getBodyBeginning($template_pages, $template_width, 0, "red", "");
//$MODULE_OBJ['title'] = $ec_iPortfolio['title_attendance'];
//$linterface = new interface_html("popup_index.html");
//$linterface->LAYOUT_START();

# define the page title and table size

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPageName = $iPort['menu']['school_records'];

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

// Tab Menu Settings
$TabMenuArr = array();
if($lpf->access_config['merit'] == 1)
	$TabMenuArr[] = array("../merit/index.php", $ec_iPortfolio['title_merit'], 0);
if($lpf->access_config['assessment_report'] == 1)
	$TabMenuArr[] = array("../assessment/index.php", $ec_iPortfolio['title_academic_report'], 0);
if($lpf->access_config['activity'] == 1)
	$TabMenuArr[] = array("../activity/index.php", $ec_iPortfolio['title_activity'], 0);
if($lpf->access_config['award'] == 1)
	$TabMenuArr[] = array("../award/index.php", $ec_iPortfolio['title_award'], 0);
if($lpf->access_config['teacher_comment'] == 1)
	$TabMenuArr[] = array("../comment/index.php", $ec_iPortfolio['title_teacher_comments'], 0);
if($lpf->access_config['attendance'] == 1)
	$TabMenuArr[] = array("../attendance/index.php", $ec_iPortfolio['title_attendance'], 1);
if($lpf->access_config['service'] == 1)
	$TabMenuArr[] = array("../service/index.php", $ec_iPortfolio['service'], 0);

?>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->
</script>

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<FORM name="form1" method="GET">
                                <tr>
                                  <td valign="top">
									<table width="100%"  border="0" cellspacing="5" cellpadding="0">
									<tr>
                                        		<td class="tab_underline">
																						<?
                                        echo $lpf_ui->GET_TAB_MENU($TabMenuArr);
                                        ?>
																			</td>
				 						</tr> 
                                        <tr>
                                        <td><img src="<?=$PATH_WRT_ROOT?>/images/2007a/iPortfolio/nav_arrow.gif" align="middle" height="15" width="15"><span class="navigation"><a href="index.php"><?=$ec_iPortfolio['overall_summary']?></a>
                                          <img src="<?=$PATH_WRT_ROOT?>/images/2007a/iPortfolio/nav_arrow.gif" align="middle" height="15" width="15"></span><span class="tabletext"><?=$ec_iPortfolio['attendance_detail']?></span></td>
                                        </tr>
                                      <tr>
                                        <td><table cellpadding="8" cellspacing="0" border="0">
                                        <tr>
											<td class='tabletext' nowrap><?=$ec_iPortfolio['total_absent_count']?> : <b><?=$absentCount?></b>&nbsp;</td>
											<td class='tabletext' nowrap><?=$ec_iPortfolio['total_late_count']?> : <b><?=$lateCount?></b>&nbsp;</td>
											<td class='tabletext' nowrap><?=$ec_iPortfolio['total_earlyleave_count']?> : <b><?=$earlyleaveCount?></b>&nbsp;</td>
										</tr>
										</table>
										</td>
                                      </tr>
                                      <tr>
                                        <td align="center">
											<?= $li->displayPlain() ?>
										</td>
                                </tr>
                              </table>
							  </td>
                             </tr>

<input type="hidden" name="ClassName" value="<?=$ClassName?>">
<input type="hidden" name="StudentID" value="<?=$StudentID?>">
<input type="hidden" name="year" value="<?=$year?>">
<input type="hidden" name="semester" value="<?=$semester?>">
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=numPerPage value="<?=$numPerPage?>">
<input type=hidden name="page_size_change">

</FORM>
</table>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
