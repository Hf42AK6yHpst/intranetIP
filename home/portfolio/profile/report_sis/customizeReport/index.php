<?
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio-reportManager.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libpf-ole.php");

include_once($PATH_WRT_ROOT."home/portfolio/profile/report/report_lib/common.php");



intranet_auth();
intranet_opendb();

$task = trim($task);


switch($task){
 	default:
		// By default, open the script with the same name as the task (after appended with ".php").
		// Prevent searching parent directories, like 'task=../../index'.
		//$task_script = "task/" . str_replace("../", "", $task) . ".php";
		$task_script = "task/" . $task . ".php";
		$template_script = "templates/" . $task . ".tmpl.php";
		// Task exists?
		if (file_exists($task_script)) {
			include_once($task_script);
		}
		else {
		//	$linterface->LAYOUT_START();
			echo "error! task not find<br/>";
		//	$linterface->LAYOUT_STOP();
		}
	break;
			
}

intranet_closedb();
exit();
?>
