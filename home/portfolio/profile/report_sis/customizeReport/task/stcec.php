<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/stcec/libpf-slp-stcecRptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/stcec/libpf-slp-stcec.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic.php");

$YearClassID = trim($YearClassID);
$issuedate  = trim($issuedate);


//YearClassID

$objReportMgr = new stcecRptMgr();

$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();


for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	
	$_studentId = $studentList[$i];

	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	$objStudentInfo -> setAcademicYear($academicYearID);
	$objStudentInfo -> setAcademicYearStr();
	$objStudentInfo -> setYearRange();
	$objStudentInfo -> setYearClassID($YearClassID);
	
	$objStudentInfo -> setAll_AcademicYearID($All_AcademicYearID);

	$OLEInfoArr_INT = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='INT',$Selected=true);
	$OLEInfoArr_EXT = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='EXT',$Selected=true);

	### Page Content
	$html .=$objStudentInfo->getEmptyTable('3%');
	$html .= $objStudentInfo->getPart1_HTML();	
	$html .=$objStudentInfo->getEmptyTable('1%');
	$html .= $objStudentInfo->getPart2_HTML();	
	
	$html .='<table width=100% height="780px">';
	$html .='<tr><td>';
		$html .= $objStudentInfo->getPart3_HTML();
		$html .=$objStudentInfo->getEmptyTable('2%');
		$html .= $objStudentInfo->getPart4_HTML();
	$html .='</td></tr>';
	$html .='</table>';
	
	
	
	$html .=$objStudentInfo->getEmptyTable('1%');
	$html .=$objStudentInfo->getEmptyTable('2%','Page 1');
	$html .=$objStudentInfo->getPagebreakTable();
	
	
	$html .=$objStudentInfo->getEmptyTable('1%');
	
	$html .='<table width=100% height="1000px">'; 
	$html .='<tr><td>';
		$html .= $objStudentInfo->getPart5_HTML($OLEInfoArr_INT);
		$html .=$objStudentInfo->getEmptyTable('2%');
		$html .= $objStudentInfo->getPart6_HTML($OLEInfoArr_EXT);
	$html .='</td></tr>';
	$html .='</table>';
	
	$html .=$objStudentInfo->getEmptyTable('1%');
	$html .=$objStudentInfo->getEmptyTable('2%','Page 2');
	$html .=$objStudentInfo->getPagebreakTable();
	
	
	$html .=$objStudentInfo->getEmptyTable('4%');
	$html .= $objStudentInfo->getPart7_HTML();
	$html .=$objStudentInfo->getEmptyTable('1%');
	$html .=$objStudentInfo->getEmptyTable('2%','<b>-END OF STUDENT LEARNING PROFILE-</b>');
	$html .=$objStudentInfo->getEmptyTable('3%');
	$html .= $objStudentInfo->getPart8_HTML();
	$html .=$objStudentInfo->getEmptyTable('1%');
	$html .=$objStudentInfo->getEmptyTable('2%','Page 3');
	
	if($i!=$i_max-1)
	{
		$html .=$objStudentInfo->getPagebreakTable();
	}

}


if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}


exit();
?>