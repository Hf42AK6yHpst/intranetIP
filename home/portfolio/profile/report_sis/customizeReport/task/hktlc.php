<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/hktlc/libpf-slp-hktlcRptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/hktlc/libpf-slp-hktlc.php");

$YearClassID = trim($YearClassID);
$StudentID = trim($StudentID);
$issuedate  = trim($issuedate);
$printIssueDate = trim($printIssueDate);
$issuedate = trim($issuedate);

$objReportMgr = new hktlcRptMgr();
$objReportMgr->setYearClassID($YearClassID);
$objReportMgr->setStudentID($StudentID);
$objReportMgr->setAcademicYearID($academicYearID);
//$objReportMgr->setIsPrintIssueDate($printIssueDate);
$objReportMgr->setIssueDate($issuedate);
$issuedate = $objReportMgr->getIssueDate();

$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();

$_page_break_br = '<table width="90%" cellpadding="0" border="0px" align="center" style="page-break-after:always;">					
		   			 <tr>
					    <td></td>
					 </tr>
				  </table>';

for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	$_studentId = $studentList[$i];
	
	$objStudentInfo = new studentInfo($_studentId);
	$objStudentInfo->setAcademicYear($academicYearID);
	$objStudentInfo->setAcademicYearDisplay($academicYearStartEndDisplay);
	$objStudentInfo->setYearTermID($YearTermID);
	$objStudentInfo->setPrintIssueDate($issuedate);

	$h_sectionSeparator = $objStudentInfo->getSectionSeparatorSpace();
	$h_header = $objStudentInfo->getHeader_HTML();
	$h_studentInfo = $objStudentInfo->getStudentInfo_HTML();
	$h_partA = $objStudentInfo->getPartA_HTML();
	$h_partB = $objStudentInfo->getPartB_HTML();
	$h_selfAccount = $objStudentInfo->getSelfAccount_HTML();
	$h_footer = $objStudentInfo->getFooter_HTML();
	
	if($i==$i_max-1)
		$_page_break_br = '';
$html .= <<<HTML
	<div align="center" style="width:100%">
		<table style="width:680px">
			<tr>
				<td style="vertical-align:top">
					$h_header
					$h_studentInfo
					$h_sectionSeparator
					$h_partA
					$h_sectionSeparator
					$h_partB
				</td>
			</tr>
			<tr>
				<td style="vertical-align:bottom">
					$h_footer
				</td>
			</tr>
			<tr><td>
			$h_selfAccount
			</td></tr>
		</table>
	</div>
	$_page_break_br
HTML;
}

$htmlCSS =<<<CSS
<style type="text/css">

</style>
CSS;


if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}

?>