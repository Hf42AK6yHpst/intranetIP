<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/kc/libpf-slp-kcRptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/kc/libpf-slp-kcRptPDFGenerator_TCPDF.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/kc/libpf-slp-kc.php");

$YearClassID = trim($YearClassID);
$StudentID = trim($StudentID);
$issuedate  = trim($issuedate);


$objReportMgr = new kcRptMgr();
$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();


for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	$_studentId = $studentList[$i];
	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID);

	$OLEInfoArr = $objStudentInfo->getOLE_Info($_studentId,$academicYearID);
	$objStudentInfo->setPageNumber(2);

	$html .='<style type="text/css">
			@media print {
			   thead { display: table-header-group; }
			   tfoot { display: table-footer-group; }
			}
			</style>';

	### Cover Page
	$html .=$objStudentInfo->getEmptyTable('7%');
	$html .= $objStudentInfo->getPart1_HTML();
	$html .=$objStudentInfo->getEmptyTable('7%');
	$html .= $objStudentInfo->getPart2_HTML();
	$html .=$objStudentInfo->getEmptyTable('7%');
	$html .= $objStudentInfo->getPart3_HTML();
	$html .=$objStudentInfo->getEmptyTable('13%');
	$html .= $objStudentInfo->getFooter_HTML('1',$ParTotalPage='');	
	$html .=$objStudentInfo->getPagebreakTable();
	
	### Page Content
	$html .=$objStudentInfo->getEmptyTable('3%');
	$html .= $objStudentInfo->getPart4_HTML($OLEInfoArr);
	$html .=$objStudentInfo->getEmptyTable('3%');
	
	$html .=$objStudentInfo->getPart5_HTML();
	

	$html .=$objStudentInfo->getPagebreakTable();
	
	$html .=$objStudentInfo->getEmptyTable('3%');
	$html .= $objStudentInfo->getPart6_HTML($OLEInfoArr);

//	$html .= $objStudentInfo->getPart7_HTML();
	
	
	$Part7_Title = 'IV. My Self-Account';
	$html .= '<table width="100%" height="">';
	$html .='<thead>';
	$html .='<tr><td>'.$objStudentInfo->getEmptyTable('4%').'</td></tr>';
	$html .='<tr height="10px">';
	$html .='<td style="vertical-align:top">';
	$html .=$objStudentInfo->getPartTitleDisplay($Part7_Title);
	$html .='</td>';
	$html .='</tr>';
	$html .='</thead>';
	$html .='<tbody><tr><td style="vertical-align:top">';

	$html .= '<table width="100%" >';
	$html .= '<tr><td style="vertical-align:top">';
	$html .=$objStudentInfo->getPart7_HTML();
	$html .='</td></tr>';
	$html .='</table>';


	
//debug_r($objStudentInfo->getPageNumber());
	
	$totalPageNo = $objStudentInfo->getPageNumber()-1;
	
	if($i!=$i_max-1)
	{
		$html .=$objStudentInfo->getPagebreakTable();
	}
	$html = str_replace("<!--totalPageNo-->", $totalPageNo, $html);

}



if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}


exit();
?>