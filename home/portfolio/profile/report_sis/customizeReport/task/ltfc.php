<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/ltfc/libpf-slp-RptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/ltfc/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic.php");

$YearClassID = trim($YearClassID);
$issuedate  = trim($issuedate);


//debug_r($_POST);

//YearClassID

$objReportMgr = new stcecRptMgr();

$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();


for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	
	$_studentId = $studentList[$i];

	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	$objStudentInfo -> setAcademicYear($academicYearID);
	$objStudentInfo -> setAcademicYearStr();
	$objStudentInfo -> setYearRange();
	$objStudentInfo -> setYearClassID($YearClassID);

	$OLEInfoArr_Award = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='INT',$Selected=true,$thisType='award');
	$OLEInfoArr_SchoolPost = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='INT',$Selected=true,$thisType='schoolPost');
	$OLEInfoArr_SchoolActivity = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='INT',$Selected=true,$thisType='schoolActivity');
	$OLEInfoArr_ExternalActivity = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='EXT',$Selected=true,$thisType='externalActivity');

	### Page Content
//	$html .=$objStudentInfo->getPageNumberTable('1%',$Content='Page 1 of 5');
	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .= $objStudentInfo->getPart1_HTML();	
	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .= $objStudentInfo->getPart2_HTML();	
	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .='<table width=100% height="730px">'; 
		$html .='<tr><td style="vertical-align:top;">'.$objStudentInfo->getPart3_HTML($OLEInfoArr_Award).'</td></tr>';
	$html .='</table>';
	$html .=$objStudentInfo->getAddress_HTML(); 
	$html .=$objStudentInfo->getPagebreakTable();
	
//	$html .=$objStudentInfo->getPageNumberTable('1%',$Content='Page 2 of 5');
	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .=$objStudentInfo->getNameNumber();
	$html .=$objStudentInfo->getEmptyTable('1%');
	$html .= $objStudentInfo->getPart4_HTML($OLEInfoArr_SchoolPost);	
	$html .=$objStudentInfo->getPagebreakTable();
	
//	$html .=$objStudentInfo->getPageNumberTable('1%',$Content='Page 3 of 5');
	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .=$objStudentInfo->getNameNumber();
	$html .=$objStudentInfo->getEmptyTable('1%');
	$html .= $objStudentInfo->getPart5_HTML($OLEInfoArr_SchoolActivity);		
	$html .=$objStudentInfo->getPagebreakTable();

//	$html .=$objStudentInfo->getPageNumberTable('1%',$Content='Page 4 of 5');
	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .=$objStudentInfo->getNameNumber();
	$html .=$objStudentInfo->getEmptyTable('1%');
	$html .= $objStudentInfo->getPart6_HTML($OLEInfoArr_ExternalActivity);	
	$html .=$objStudentInfo->getPagebreakTable();
	
//	$html .=$objStudentInfo->getPageNumberTable('1%',$Content='Page 5 of 5');
	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .=$objStudentInfo->getNameNumber();
	$html .=$objStudentInfo->getEmptyTable('1%');
	$html .='<table width=100% height="930px">'; 
		$html .='<tr><td style="vertical-align:top;">'.$objStudentInfo->getPart7_HTML().'</td></tr>';
	$html .='</table>';

	$html .= $objStudentInfo->getSignature_HTML();
	
	if($i!=$i_max-1)
	{
		$html .=$objStudentInfo->getPagebreakTable();
	}

}


if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}


exit();
?>