<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_custom/ccym_ole_report_lang.php");

//////////////// function list
#########################
# Other Table Generator #
#########################

function GEN_OTHER_ROW_ARRAY($ParTotalHour, $ParStar)
{
	global $MainTableWidth, $ContentFontSize, $ccym_ole_report, $OthersFontSize;

	$widthDistribute =	array(
												array(0.5, 0.5),
												array(0.8, 0.2)
											);
	$tableHeadArr[0] = array($ccym_ole_report['service_plan']);
	$tableHeadArr[1] = array($ccym_ole_report['star_award']);

	$tableWidth = $MainTableWidth;	
	$cellpadding_left = 0.2;
	$cellpadding_right = 0.2;
	
	for($i=0; $i<count($widthDistribute); $i++)
	{
		for($j=0; $j<count($widthDistribute[$i]); $j++)
		{
			$cellWidthArr[$i][$j] = round($tableWidth*$widthDistribute[$i][$j], 1) - $cellpadding_left - $cellpadding_right;
			$style = "width: ".$cellWidthArr[$i][$j]."cm; font-size: ".$OthersFontSize."pt; padding-left: ".$cellpadding_left."cm; padding-right: ".$cellpadding_right."cm;";
				
			$styleArr[$i][$j] = $style;
		}	
	}

	for($i=0; $i<count($tableHeadArr); $i++)
	{
		$tableHeadRow[$i] = GEN_TABLE_ROW('H', $styleArr[$i], $tableHeadArr[$i], $cellWidthArr[$i], $OthersFontSize, 2);
	}
	unset($styleArr);

	$Star = ($ParStar=="") ? "- -" : $ParStar." ".$ccym_ole_report['star'];
	$TotalHour = ($ParTotalHour=="") ? 0 : $ParTotalHour;
	$OtherArr[0][] = array($ccym_ole_report['service_total_hour'], $TotalHour.$ccym_ole_report['hour']);
	$OtherArr[1][] = array($ccym_ole_report['star_got'], $Star." (".$ccym_ole_report['star_highest'].")");
	
	for($i=0; $i<count($OtherArr); $i++)
	{
		for($k=0; $k<count($OtherArr[$i]); $k++)
		{
			for($j=0; $j<count($cellWidthArr[$i]); $j++)
			{

				$style = "height:100%; width: ".$cellWidthArr[$k][$j]."cm; font-size: ".$OthersFontSize."pt; padding-left: ".$cellpadding_left."cm; padding-right: ".$cellpadding_right."cm;";
				
				$styleArr[$i][$k][$j] = $style;	
			}

			$tableRowArr[$i][] = GEN_TABLE_ROW('D', $styleArr[$i][$k], $OtherArr[$i][$k], $cellWidthArr[$i], $OthersFontSize);
		}
	}
	
	return array($tableHeadRow, $tableRowArr);
}

########################

////////////////

intranet_auth();
intranet_opendb();
$li_pf = new libportfolio();

// check access right
$li_pf->ACCESS_CONTROL("print_report");



#############################################################################
// Assumption:
// The default printing page top margin and bottom margin is 2 cm
// A page length: 29.5 cm, so space left = 29.5 - 4 = 25.5
// Assume 1 cm = 2 rows, there are 51 total rows
// Student Info table around 3 cm = 6 rows
// Top magin is allowed to change, default value is 2 cm = 4 rows
// So default rows left:  51 - 6 - 4 = 41
// Adjusted total row: 49

# page margin in cm, default 2, suggest range: 0 - 4 and must < 20
$TOP_MARGIN = "2";
$LEFT_MARGIN = "2.3";
$RIGHT_MARGIN = "2.3";
$BOTTOM_MARGIN = "2";

$BOTTOM_THRESHOLD = 4;

# font size in pt
$TitleFontSize = 14;
$SubTitleFontSize = 12;
$ContentFontSize = 8;
$StudentInfoFontSize = 10;
$OthersFontSize = 10;

$ShowReportTitle = true;
$ShowActivity = true;
$ShowService = true;
$ShowOLE = true;
$ShowOther = true;
$CalExactHeight = false;

define("A4Width", 20.9);		# width in cm
if($CalExactHeight)
	define("A4Height", 29.7);		# height in cm
else
	define("A4Height", 999);		# height in cm
	
$OLEMerge = array(
							"[ID]" => "[OTHERS]"
						);
$OLEOrder = array(
							array("[MCE]", "[CS]"),
							array("[CE]", "[AD]"),
							array("[PD]", "[OTHERS]")
						);

$NullValue = "- -";

#############################################################################

$RemainHeight = A4Height;

//$BodyBegin = "<body style='margin-top: ".$TOP_MARGIN."cm; margin-left: ".$LEFT_MARGIN."cm; margin-right: ".$RIGHT_MARGIN."cm; margin-bottom: ".$BOTTOM_MARGIN."cm'>\n";
$BodyBegin = "<body>\n";

$MainTableHeight = A4Height - $TOP_MARGIN - $BOTTOM_MARGIN;
$MainTableWidth = A4Width - $LEFT_MARGIN - $RIGHT_MARGIN;

$MainTableHeightStyle = ($CalExactDim) ? "height: ".$MainTableHeight."cm" : "";
$MainTableWidthStyle = "width: ".$MainTableWidth."cm;";
$MainTableBegin = "<table align='center' border='0' style='".$MainTableWidthStyle." ".$MainTableHeightStyle."'>";

$ReportTitleCell =	"
											<td align='center' width='100%'>
												<table border='0' cellpadding='0' cellspacing='0' width='100%'>
													<tr>
														<td width='100' align='right'><img src='/file/iportfolio/ccym_report_icon.jpg' width='71' height='90' /></td>
														<td>
															<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																<tr><th style='font-size: ".$TitleFontSize."pt; text-align: center'>Caritas Chong Yuet Ming Secondary School</th></tr>
																<tr><th style='font-size: ".$TitleFontSize."pt; text-align: center'>明愛莊月明中學</th></tr>
																<tr><th style='font-size: ".$SubTitleFontSize."pt; text-align: center'>Other Learning Experiences Report</th></tr>
																<tr><th style='font-size: ".$SubTitleFontSize."pt; text-align: center'>其他學習經歷 報告</th></tr>
																<tr><th style='font-size: ".$SubTitleFontSize."pt; text-align: center'>Academic Year ".$_REQUEST['Year']." 年度</th></tr>
															</table>
														</td>
 														<td width='100'>&nbsp;</td>
													</tr>
												</table>
											</td>
										";
$ReportTitleHeight = 3.5;

$captionHeight = pt2cm($SubTitleFontSize) * 2;
$captionStyle = "style='height: ".$captionHeight."cm; text-align: left; font-size: ".$SubTitleFontSize."pt;'";

if($_GET['PrintType'] == 1)
	$page_break = "<br clear='all' style='page-break-after:always' />";
else
	$page_break = "<table style='page-break-after:always' />";
	
for($i=0; $i<count($OLEOrder); $i++)
{
	for($j=0; $j<count($OLEOrder[$i]); $j++)
	{
		$OLEDisplayOrder[$OLEOrder[$i][$j]] = $i*count($OLEOrder[$i]) + $j;
	}
}
#############################

###########################
# convert unit functions: #
###########################
# point to inch
function pt2inch($ParPoint)
{
	return $ParPoint / 72;
}

function inch2cm($ParInch)
{
	return $ParInch * 2.54;
}

function pt2cm($ParPoint)
{
	return inch2cm(pt2inch($ParPoint));
}

#############################

################
# UI functions #
################

function GEN_MAIN_TABLE_OPEN($ParStudentArr)
{
	global $MainTableBegin, $ReportTitleCell, $ReportTitleHeight;
	global $TitleFontSize, $RemainHeight, $MainTableHeight, $ContentFontSize, $StudentInfoFontSize;
	global $ccym_ole_report, $i_gender_female, $i_gender_male;

	$cellpadding_left = 0.2;

	$RemainHeight = $MainTableHeight;
	
	switch($ParStudentArr['Gender'])
	{
		case $i_gender_female:
			$Gender = $ccym_ole_report['female'];
			break;
		case $i_gender_male:
			$Gender = $ccym_ole_report['male'];
			break;
		default:
			$Gender = "- -";
			break;
	}

	$x = $MainTableBegin."
	<tr>
		".$ReportTitleCell."
	</tr>
	<tr>
		<td width=\"100%\" height=\"100%\" valign=\"top\" align=\"center\">
			<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">
				<tr>
					<td style='padding-left: ".$cellpadding_left."cm'>
						<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">
							<tr>
								<th style='font-size: ".$StudentInfoFontSize."pt; text-align: left;'>".$ccym_ole_report['student_name']." : ".$ParStudentArr['ChineseName']." (".$ParStudentArr['EnglishName'].")</th>
								<th style='font-size: ".$StudentInfoFontSize."pt; text-align: left;'>".$ccym_ole_report['class_name']." : ".$ParStudentArr['ClassName']."</th>
								<th style='font-size: ".$StudentInfoFontSize."pt; text-align: left;'>".$ccym_ole_report['class_number']." : ".$ParStudentArr['ClassNumber']."</th>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style='padding-left: ".$cellpadding_left."cm'>
						<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">
							<tr>
								<th style='font-size: ".$StudentInfoFontSize."pt; text-align: left;'>".$ccym_ole_report['sex']." : ".$Gender."</th>
								<th style='font-size: ".$StudentInfoFontSize."pt; text-align: left;'>".$ccym_ole_report['reg_no']." : ".str_replace("#", "", $ParStudentArr['WebSAMSRegNo'])."</th>
								<th style='font-size: ".$StudentInfoFontSize."pt; text-align: left;'>".$ccym_ole_report['doi']." : ".$_GET['issuedate']."</th>
							</tr>
						</table>
					</td>
				</tr>
";

	$RemainHeight -= $ReportTitleHeight;
	$RemainHeight -= pt2cm($StudentInfoFontSize) * 2;

	return $x;
}

function GEN_MAIN_TABLE_CLOSE($endPage=true)
{
	global $page_break;

	$x = "
			</table>
		</td>
	</tr>
</table>
";
	if($endPage)
		$x .= $page_break;
	
	return $x;
}

function GEN_END_REPORT_ROW()
{
	global $ContentFontSize, $ccym_ole_report;

	$x =	"
					<tr>
						<td style='font-size: ".$ContentFontSize."pt;'>&nbsp;</td>
					</tr>
					<tr>
						<td align='center' style='font-size: ".$ContentFontSize."pt;'>".$ccym_ole_report['end']."</td>
					</tr>
				";

	return $x;
}

function GEN_CAPTION_TABLE($ParCaption)
{
	global $RemainHeight, $captionHeight, $captionStyle;
	
	return	"
						<table cellspacing='0' cellpadding='0' border='0'>
							<tr><th valign='bottom' ".$captionStyle.">".$ParCaption."</td></tr>
						</table>
					";
	$RemainHeight -= $captionHeight;
}

function GEN_TABLE_ROW($ParType, $ParStyleArr, $ParCellDataArr, $ParCellWidth, $ParFontSize="", $ParColSpan="")
{
	global $ContentFontSize, $NullValue;
	
	$CurrentFontSize = ($ParFontSize == "") ? $ContentFontSize : $ParFontSize;
	$Colspan = ($ParColSpan == "") ? "" : " colspan='".$ParColSpan."'";

	$x = "<tr>";

	for($i=0; $i<count($ParCellDataArr); $i++)
	{
		$content = $ParCellDataArr[$i];
	
		if($ParType == "H")
		{
			$x .= "<th valign='top' ".$Colspan." style='".$ParStyleArr[$i]."'>".$ParCellDataArr[$i]."</th>";
		}
		else if($ParType == "D")
		{
			if($ParCellDataArr[$i] == $NullValue)
				$currentStyle = $ParStyleArr[$i]." text-align: center;";
			else
				$currentStyle = $ParStyleArr[$i];
		
			$x .= "<td valign='top' ".$Colspan." style='".$currentStyle."'>".$ParCellDataArr[$i]."</td>";
		}

		$heightUsed[] = ceil((pt2cm($CurrentFontSize) * strlen($content)) / $ParCellWidth[$i]);
	}
	
	$x .= "</tr>";
	
	$heightUsedRow = pt2cm($CurrentFontSize) * max($heightUsed);

	return array($x, $heightUsedRow);
}

function GEN_EMPTY_ROW($ParStyle, $ParColspan)
{
	global $ContentFontSize, $ccym_ole_report;

	$x = "<tr>";
	$x .= "<td valign='top' align='center' colspan='".$ParColspan."' style='".$ParStyle."'>".$ccym_ole_report['no_records']."</td>";
	$x .= "</tr>";
	
	$heightUsedRow = pt2cm($ContentFontSize);

	return array($x, $heightUsedRow);
}

function GEN_COLUMN_TABLE($ParTableWidth, $ParHeaderRow, $ParContentRowArr)
{
	global $RemainHeight, $MainTableHeight, $TitleFontSize, $BOTTOM_THRESHOLD, $ReportTitleHeight;

	$tableStyle = "width: ".$ParTableWidth."cm; text-align: left;";
	$tableOpen = "<table height='100%' cellspacing='0' cellpadding='0' style='".$tableStyle."'>";
	$tableStarted = false;
	$spaceRemain = $RemainHeight;
	$originalSpaceRemain = $spaceRemain;
	
	$x = "";
	if(count($ParContentRowArr) > 0)
	{
		for($i=0; $i<count($ParContentRowArr); $i++)
		{
			if(!$tableStarted)
			{
				if(!CHECK_SPACE($ParContentRowArr[$i][1] + $ParHeaderRow[1], $spaceRemain))
				{
					if($x != "")
					{
						$columnTableArr[] = $x;
						$columnTableSizeArr[] = $originalSpaceRemain - $spaceRemain;
					}
					
					$spaceRemain = $MainTableHeight - $ReportTitleHeight;
					$originalSpaceRemain = $spaceRemain;
				}
	
				$x = $tableOpen;
				ATTACH_ROW($x, $ParHeaderRow, $spaceRemain);
				$tableStarted = true;
			}
			
			if(!ATTACH_ROW($x, $ParContentRowArr[$i], $spaceRemain))
			{
				$x .= "</table>";
				$tableStarted = false;
	
				$i--;
				continue;
			}
		}
	}
	else
	{
		$x = $tableOpen;
	}
	
	if($x != "");
	{
		$x .= "</table>";
		$columnTableArr[] = $x;
		$columnTableSizeArr[] = $originalSpaceRemain - $spaceRemain;
	}

	return array($columnTableArr, $columnTableSizeArr);
}

function CHECK_SPACE($ParRowHeight, $ParSpaceRemain="")
{
	global $RemainHeight, $BOTTOM_THRESHOLD;

	$spaceRemain = $ParSpaceRemain=="" ? $RemainHeight : $ParSpaceRemain;

	if($spaceRemain - $ParRowHeight > $BOTTOM_THRESHOLD)
		return true;
	else
		return false;
}

function ATTACH_ROW(&$ParTableStr, $ParContentRow, &$ParSpaceRemain)
{
	if(!CHECK_SPACE($ParContentRow[1], $ParSpaceRemain))
	{
		return false;
	}
	
	$ParTableStr .= $ParContentRow[0];
	$ParSpaceRemain -= $ParContentRow[1];
	return true;
}


function GEN_MODULE_TABLE($ParTableWidth, $ParContentTableArr, $ParContentTableSizeArr, $ParNeedBorder=true, $ParFinalizePage=false)
{
	global $RemainHeight, $captionHeight, $BOTTOM_THRESHOLD;
	global $student;

	$tableStyle = "width: ".$ParTableWidth."cm; text-align: left;";
	if($ParNeedBorder)
		$tableStyle .= " border: thin solid #000000;";
	$tableOpen = "<table cellspacing='0' cellpadding='0' style='".$tableStyle."'>";
	
	# same page
	for($i=0; $i<count($ParContentTableArr); $i++)
	{
		# different page
		for($j=0; $j<count($ParContentTableArr[$i]); $j++)
		{
			$pageRow[$j][$i] = $ParContentTableArr[$i][$j];
			$pageRowSize[$j][$i] = $ParContentTableSizeArr[$i][$j];
		}
	}

	# different page
	for($i=0; $i<count($pageRow); $i++)
	{
		$x = $tableOpen;
		$x .= "<tr height=\"100%\">";
	
		# same page
		for($j=0; $j<count($pageRow[$i]); $j++)
		{
			$cellStyle = ($j!=count($pageRow[$i])-1 && $ParNeedBorder) ? "style=\"border-right: thin solid #000000;\"" : "";
			$contentTableRow .= "<td valign=\"top\" ".$cellStyle." height=\"100%\">".$pageRow[$i][$j]."</td>";
		}
		$x .= $contentTableRow;
		$x .= "</tr>";
		$x .= "</table>";

		$RemainHeight -= max($pageRowSize[$i]);

		if($RemainHeight <= $BOTTOM_THRESHOLD && $i!=count($pageRow)-1)
		{
			$x .= "</td></tr>";
			$x .= GEN_MAIN_TABLE_CLOSE();
			$x .= GEN_MAIN_TABLE_OPEN($student);
			$x .= "<tr height=\"100%\"><td>";
		}
		else if($ParFinalizePage)
		{
				$x .= "<!--".$RemainHeight."--><!--".$BOTTOM_THRESHOLD."--></td></tr>";
				if($RemainHeight > $BOTTOM_THRESHOLD)
					$x .= GEN_END_REPORT_ROW();
		}
		
		$contentTableRow = "";
	}

	return $x;
}

#############################

#############################
# Activity Module Generator #
#############################
function GET_ACTIVITY_ASOC_ARRAY($ParStudentID, $ParYear, $ParSemester)
{
	global $li_pf, $ec_iPortfolio, $ActivityHeading;
	$objReport = new libpf_sturec();
	$row = $objReport->returnActivityRecord($ParStudentID, $ParYear, $ParSemester);
	
	for($i=0; $i<sizeof($row); $i++)
	{
		if($row[$i]["ActivityName"]!="")
		{
			$ReturnArray[] = array($row[$i]["ActivityName"], $row[$i]["Role"], $row[$i]["Performance"]);
		}
	}

	return $ReturnArray;
}

function GET_ACTIVITY_ROW_ARRAY($ParActivityArr)
{
	global $MainTableWidth, $ContentFontSize, $ccym_ole_report;

	$widthDistribute =	array(
												array(0.4, 0.24, 0.36),
												array(0.4, 0.24, 0.36)
											);
	$tableHeadArr =	array(
										$ccym_ole_report['activity']['head'],
										$ccym_ole_report['activity']['head']
									);

	$tableWidth = round($MainTableWidth/2, 1);	
	$cellpadding_left = 0.2;
	$cellpadding_right = 0.2;
	
	for($i=0; $i<count($widthDistribute); $i++)
	{
		for($j=0; $j<count($widthDistribute[$i]); $j++)
		{
			$cellWidthArr[$i][$j] = round($tableWidth*$widthDistribute[$i][$j], 1) - $cellpadding_left - $cellpadding_right;
			$style = "width: ".$cellWidthArr[$i][$j]."cm; font-size: ".$ContentFontSize."pt; border-bottom:0.07cm double #000000; padding-left: ".$cellpadding_left."cm; padding-right: ".$cellpadding_right."cm;";
		
			if($j!=count($widthDistribute[$i])-1)
				$style .= " border-right:1px solid #000000;";
		
			$styleArr[$i][$j] = $style;
		}	
	}
	for($i=0; $i<count($tableHeadArr); $i++)
	{
		$tableHeadRow[$i] = GEN_TABLE_ROW('H', $styleArr[$i], $tableHeadArr[$i], $cellWidthArr[$i]);
	}
	unset($styleArr);

	if(is_array($ParActivityArr))
	{
		# split array into two portion
		$middleIndex = ceil(count($ParActivityArr) / 2);
		$ActivityArr[0] = array_slice($ParActivityArr, 0, $middleIndex);
		$ActivityArr[1] = array_slice($ParActivityArr, $middleIndex);
		if(count($ActivityArr[1]) == 0) $ActivityArr[1][] = array("&nbsp;", "&nbsp;", "&nbsp;");
	
		for($i=0; $i<count($ActivityArr); $i++)
		{
			for($k=0; $k<count($ActivityArr[$i]); $k++)
			{
				for($j=0; $j<count($cellWidthArr[$i]); $j++)
				{
					$cellHeight = ($k==count($ActivityArr[$i])-1) ? "100%" : "1px";
				
					$style = "height:".$cellHeight."; width: ".$cellWidthArr[$k][$j]."cm; font-size: ".$ContentFontSize."pt; padding-left: ".$cellpadding_left."cm; padding-right: ".$cellpadding_right."cm;";
					
					if($j!=count($cellWidthArr[$i])-1)
						$style .= " border-right:1px solid #000000;";
					$styleArr[$i][$k][$j] = $style;
				}
	
				$tableRowArr[$i][] = GEN_TABLE_ROW('D', $styleArr[$i][$k], $ActivityArr[$i][$k], $cellWidthArr[$i]);
			}
		}
	}
	else
	{
		$style = "width: 100%; font-size: ".$ContentFontSize."pt; padding-left: ".$cellpadding_left."cm; padding-right: ".$cellpadding_right."cm;";
		$tableRowArr[0][] = GEN_EMPTY_ROW($style, count($tableHeadArr[0]));
		$tableRowArr[1][] = GEN_EMPTY_ROW($style, count($tableHeadArr[1]));
	}

	return array($tableHeadRow, $tableRowArr);
}

#############################

############################
# Service Module Generator #
############################
function GET_SERVICE_ASOC_ARRAY($ParStudentID, $ParYear, $ParSemester)
{
	global $li_pf, $ec_iPortfolio;
	$objReport = new libpf_sturec();
	$row = $objReport->returnServiceRecord($ParStudentID, $ParYear, $ParSemester);
	for($i=0; $i<sizeof($row); $i++)
	{
		if($row[$i]["ServiceName"]!="")
		{
			$ReturnArray[] = array($row[$i]["ServiceName"], $row[$i]["Role"], $row[$i]["Performance"]);
		}
		
	}

	return $ReturnArray;
}

function GET_SERVICE_ROW_ARRAY($ParServiceArr)
{
	global $MainTableWidth, $ContentFontSize, $ccym_ole_report;

	$widthDistribute =	array(
												array(0.2, 0.12, 0.18),
												array(0.2, 0.12, 0.18)
											);
	$tableHeadArr = array(
										$ccym_ole_report['service']['head'], 
										$ccym_ole_report['service']['head']
									);

	$tableWidth = $MainTableWidth;	
	$cellpadding_left = 0.2;
	$cellpadding_right = 0.2;
	
	for($i=0; $i<count($widthDistribute); $i++)
	{
		for($j=0; $j<count($widthDistribute[$i]); $j++)
		{
			$cellWidthArr[$i][$j] = round($tableWidth*$widthDistribute[$i][$j], 1) - $cellpadding_left - $cellpadding_right;
			$style = "width: ".$cellWidthArr[$i][$j]."cm; font-size: ".$ContentFontSize."pt; border-bottom:0.07cm double #000000; padding-left: ".$cellpadding_left."cm; padding-right: ".$cellpadding_right."cm;";
		
			if($j!=count($widthDistribute[$i])-1)
				$style .= " border-right:1px solid #000000;";
		
			$styleArr[$i][$j] = $style;
		}	
	}

	for($i=0; $i<count($tableHeadArr); $i++)
	{
		$tableHeadRow[$i] = GEN_TABLE_ROW('H', $styleArr[$i], $tableHeadArr[$i], $cellWidthArr[$i]);
	}
	unset($styleArr);
	
	if(is_array($ParServiceArr))
	{
		# split array into two portion
		$middleIndex = ceil(count($ParServiceArr) / 2);
		$ServiceArr[0] = array_slice($ParServiceArr, 0, $middleIndex);
		$ServiceArr[1] = array_slice($ParServiceArr, $middleIndex);
		if(count($ServiceArr[1]) == 0) $ServiceArr[1][] = array("&nbsp;", "&nbsp;", "&nbsp;");
	
		for($i=0; $i<count($ServiceArr); $i++)
		{
			for($k=0; $k<count($ServiceArr[$i]); $k++)
			{
				for($j=0; $j<count($cellWidthArr[$i]); $j++)
				{
					$cellHeight = ($k==count($ServiceArr[$i])-1) ? "100%" : "1px";
				
					$style = "height:".$cellHeight."; width: ".$cellWidthArr[$k][$j]."cm; font-size: ".$ContentFontSize."pt; padding-left: ".$cellpadding_left."cm; padding-right: ".$cellpadding_right."cm;";
					
					if($j!=count($cellWidthArr[$i])-1)
						$style .= " border-right:1px solid #000000;";
					$styleArr[$i][$k][$j] = $style;
				}
	
				$tableRowArr[$i][] = GEN_TABLE_ROW('D', $styleArr[$i][$k], $ServiceArr[$i][$k], $cellWidthArr[$i]);
			}
		}
	}
	else
	{
		$style = "width: 100%; font-size: ".$ContentFontSize."pt; padding-left: ".$cellpadding_left."cm; padding-right: ".$cellpadding_right."cm;";
		$tableRowArr[0][] = GEN_EMPTY_ROW($style, count($tableHeadArr[0]));
		$tableRowArr[1][] = GEN_EMPTY_ROW($style, count($tableHeadArr[1]));
	}

	return array($tableHeadRow, $tableRowArr);
}
#############################

########################
# OLE Module Generator #
########################
function GET_OLR_ASOC_ARRAY($ParStudentID, $ParYear, $ParSemester)
{
	global $li_pf, $ec_iPortfolio, $OLEMerge, $OLEOrder;
	$objReport = new libpf_sturec();
	$row = $objReport->returnOLERecord($ParStudentID, $ParYear, $ParSemester);

	$ReturnArray["TotalHour"] = 0;
	for($i=0; $i<sizeof($row); $i++)
	{
		if($row[$i]["Title"]!="")
		{
			if($row[$i]["ele"] == "")
				$ele = array("[OTHERS]");
			else
			{
				$ele = explode(",", $row[$i]["ele"]);
			}
			
			for($j=0; $j<count($ele); $j++)
			{
				$eleItem = trim($ele[$j]);
				if(is_array($OLEMerge) && array_key_exists($eleItem, $OLEMerge))
					$eleItem = $OLEMerge[$eleItem];

				//$ReturnArray[$eleItem][] = array($row[$i]["Title"], $row[$i]["Hours"], $row[$i]["Role"]);					
				$ReturnArray[$eleItem][] = array($row[$i]["Title"], $row[$i]["Hours"], $row[$i]["Role"]);
			}
			$ReturnArray["TotalHour"] += $row[$i]["Hours"];
		}
	}
	
	# Take care of ELE with no records
	for($i=0; $i<sizeof($OLEOrder); $i++)
	{
		for($j=0; $j<sizeof($OLEOrder[$i]); $j++)
		{
			if(!is_array($ReturnArray[$OLEOrder[$i][$j]]) || count($ReturnArray[$OLEOrder[$i][$j]]) == 0)
				$ReturnArray[$OLEOrder[$i][$j]][] = array("- -", "- -", "- -");
		}
	}

	return $ReturnArray;
}

function GET_OLR_ROW_ARRAY($ParELE, $ParOLEArr)
{
	global $MainTableWidth, $ContentFontSize, $ccym_ole_report;

	$widthDistribute = array(0.2, 0.12, 0.18);
	$tableHeadArr = $ccym_ole_report['ole']['head'];
	array_unshift($tableHeadArr, $ParELE);

	$tableWidth = $MainTableWidth;	
	$cellpadding_left = 0.2;
	$cellpadding_right = 0.2;
	
	for($i=0; $i<count($widthDistribute); $i++)
	{
		$cellWidthArr[$i] = round($tableWidth*$widthDistribute[$i], 1) - $cellpadding_left - $cellpadding_right;
		$style = "width: ".$cellWidthArr[$i]."cm; font-size: ".$ContentFontSize."pt; border-bottom:0.07cm double #000000; padding-left: ".$cellpadding_left."cm; padding-right: ".$cellpadding_right."cm;";
		
		if($i!=count($widthDistribute)-1)
			$style .= " border-right:1px solid #000000;";
		
		$styleArr[$i] = $style;	
	}
	$tableHeadRow = GEN_TABLE_ROW('H', $styleArr, $tableHeadArr, $cellWidthArr);

	$totalHour = 0;
	for($i=0; $i<count($ParOLEArr); $i++)
	{
		for($j=0; $j<count($cellWidthArr); $j++)
		{
			$cellHeight = ($i==count($ParOLEArr)-1) ? "100%" : "1px";
		
			$style = "height: ".$cellHeight."; width: ".$cellWidthArr[$j]."cm; font-size: ".$ContentFontSize."pt; padding-left: ".$cellpadding_left."cm; padding-right: ".$cellpadding_right."cm;";
			
			switch($j)
			{
				case 0:
					$style .= " border-right:1px solid #000000;";
					break;
				case 1:
					$style .= " border-right:1px solid #000000; text-align: center;";
					break;
				default:
					break;
			}
						
			$styleArr[$j] = $style;	
		}
		
		$tableRowArr[] = GEN_TABLE_ROW('D', $styleArr, $ParOLEArr[$i], $cellWidthArr);
		
		$totalHour += $ParOLEArr[$i][1];
	}

	# ELE hour sub-total
	$totalHourArr = array($ccym_ole_report['total_hour'], $totalHour, "&nbsp;");
	for($i=0; $i<count($cellWidthArr); $i++)
	{
		$style = "height: 0px; width: ".$cellWidthArr[$i]."cm; font-size: ".$ContentFontSize."pt; border-top:0.07cm double #000000; padding-left: ".$cellpadding_left."cm; padding-right: ".$cellpadding_right."cm;";
		
		switch($i)
		{
			case 0:
				$style .= " font-weight: bold; border-right:1px solid #000000;";
				break;
			case 1:
				$style .= " text-align: center;";
				break;
			default:
				break;
		}
		
		$styleArr[$i] = $style;
	}
	$tableRowArr[] = GEN_TABLE_ROW('D', $styleArr, $totalHourArr, $cellWidthArr);


	return array($tableHeadRow, $tableRowArr);
}

########################


if(is_file($intranet_root."/file/portfolio/starAward.csv"))
{
	$limport = new libimporttext();
	$starAwardArrtemp = $limport->GET_IMPORT_TXT($intranet_root."/file/portfolio/starAward.csv");
	array_shift($starAwardArrtemp);
	for($i=0; $i<count($starAwardArrtemp); $i++)
	{
		$starAwardArr[$starAwardArrtemp[$i][0]] = array($starAwardArrtemp[$i][2], $starAwardArrtemp[$i][3]);
	}
}

if($_GET['StudentID'] != "")
{
	$StudentArr[]['UserID'] = $_GET['StudentID'];
}
else
{
	$StudentArr = $li_pf->GET_STUDENT_LIST_DATA($_GET['TargetClass'], false);
}

$displayStr = "";
	$objSlp = new libpf_slp();
$eleList = $objSlp->GET_ELE_LIST();
for($k=0; $k<count($StudentArr); $k++)
{
	$currentStudentID = $StudentArr[$k]['UserID'];
	$student = $li_pf->GET_STUDENT_DATA($currentStudentID);
	$student = $student[0];
	
	$ActivityArr = GET_ACTIVITY_ROW_ARRAY(GET_ACTIVITY_ASOC_ARRAY($currentStudentID, $_REQUEST['Year'], ''));
	for($i=0; $i<count($ActivityArr); $i++)
	{
		list($ActivityTableArr[$i], $ActivityTableSizeArr[$i]) = GEN_COLUMN_TABLE($MainTableWidth/2, $ActivityArr[0][$i], $ActivityArr[1][$i]);
	}

	$ServiceArr = GET_SERVICE_ROW_ARRAY(GET_SERVICE_ASOC_ARRAY($currentStudentID, $_REQUEST['Year'], ''));
	for($i=0; $i<count($ServiceArr); $i++)
	{
		list($ServiceTableArr[$i], $ServiceTableSizeArr[$i]) = GEN_COLUMN_TABLE($MainTableWidth/2, $ServiceArr[0][$i], $ServiceArr[1][$i]);
	}
	
	
	$OLRArr = GET_OLR_ASOC_ARRAY($currentStudentID, $_REQUEST['Year'], '');
	$objReport2 = new libpf_slp();
	if(is_array($eleList))
	{
		for($i=0; $i<count($eleList); $i++)
		{
			$currentELE = $objReport2->GET_ELE_INFO($eleList[$i]);
		
			$OLRTempArr[$eleList[$i]['DefaultID']] = $OLRArr[$eleList[$i]['DefaultID']];
			
			$OLRRowArr[$eleList[$i]['DefaultID']] = GET_OLR_ROW_ARRAY($currentELE[0]['ChiTitle']." ".$currentELE[0]['EngTitle'], $OLRTempArr[$eleList[$i]['DefaultID']]);
			unset($OLRTempArr);
		}
		for($i=0; $i<count($OLEOrder); $i++)
		{
			# Handle row height
			$rowHeight = max($OLRRowArr[$OLEOrder[$i][0]][0][1], $OLRRowArr[$OLEOrder[$i][1]][0][1]);
			$OLRRowArr[$OLEOrder[$i][0]][0][1] = $rowHeight; 
			$OLRRowArr[$OLEOrder[$i][1]][0][1] = $rowHeight;
			$OLRRowArr[$OLEOrder[$i][0]][0] = str_replace("style='", "style='height:".round($rowHeight,1)."cm; ", $OLRRowArr[$OLEOrder[$i][0]][0]);
			$OLRRowArr[$OLEOrder[$i][1]][0] = str_replace("style='", "style='height:".round($rowHeight,1)."cm; ", $OLRRowArr[$OLEOrder[$i][1]][0]);

			$OLE2TableArr	=	array(
												array($OLRRowArr[$OLEOrder[$i][0]][0], $OLRRowArr[$OLEOrder[$i][1]][0]),
												array($OLRRowArr[$OLEOrder[$i][0]][1], $OLRRowArr[$OLEOrder[$i][1]][1])
											);

			//for($j=0; $j<count($OLE2TableArr); $j++)
			for($j=0; $j<2; $j++)
			{
				list($OLETableArr[$i][$j], $OLETableSizeArr[$i][$j]) = GEN_COLUMN_TABLE($MainTableWidth/2, $OLE2TableArr[0][$j], $OLE2TableArr[1][$j]);
			}
			unset($OLE2TableArr);
		}
	}
	
	$TotalHour = ($starAwardArr[$student['ClassNumber']][0] == "") ? $OLRArr['TotalHour'] : $starAwardArr[$student['ClassNumber']][0];
	$OtherArr = GEN_OTHER_ROW_ARRAY($TotalHour, $starAwardArr[$student['ClassNumber']][1]);
	for($i=0; $i<count($OtherArr); $i++)
	{
		list($OtherTableArr[$i], $OtherTableSizeArr[$i]) = GEN_COLUMN_TABLE($MainTableWidth/2, $OtherArr[0][$i], $OtherArr[1][$i]);
	}
	
	$displayStr .= GEN_MAIN_TABLE_OPEN($student);
	
	# Activity
	$displayStr .= "<tr height='100%'><td height='100%'>";
	$displayStr .= GEN_CAPTION_TABLE($ccym_ole_report['activity_title']);
	$displayStr .= "</td></tr>";
	$displayStr .= "<tr height='100%'><td height='100%'>";
	$displayStr .= GEN_MODULE_TABLE($MainTableWidth/2, $ActivityTableArr, $ActivityTableSizeArr);
	$displayStr .= "</td></tr>";
	
	
	# Service
	$displayStr .= "<tr height='100%'><td height='100%'>";
	$displayStr .= GEN_CAPTION_TABLE($ccym_ole_report['service_title']);
	$displayStr .= "</td></tr>";
	$displayStr .= "<tr height='100%'><td height='100%'>";
	$displayStr .= GEN_MODULE_TABLE($MainTableWidth, $ServiceTableArr, $ServiceTableSizeArr);
	$displayStr .= "</td></tr>";
	
	# OLE
	$displayStr .= "<tr height='100%'><td height='100%'>";
	$displayStr .= GEN_CAPTION_TABLE($ccym_ole_report['ole_title']);
	$displayStr .= "</td></tr>";
	$displayStr .= "<tr height='100%'><td height='100%'>";
	for($i=0; $i<count($OLETableArr); $i++)
	{
		$displayStr .= GEN_MODULE_TABLE($MainTableWidth, $OLETableArr[$i], $OLETableSizeArr[$i]);
		
		if($i != count($OLETableArr)-1)
			$displayStr .= "<br />";
	}
	$displayStr .= "</td></tr>";
	
	# Other
	$displayStr .= "<tr height='100%'><td height='100%'>";
	$displayStr .= GEN_CAPTION_TABLE($ccym_ole_report['other_title']);
	$displayStr .= "</td></tr>";
	$displayStr .= "<tr height='100%'><td height='100%'>";
	$displayStr .= GEN_MODULE_TABLE($MainTableWidth, $OtherTableArr, $OtherTableSizeArr, false, true);
	$displayStr .= "</td></tr>";		
	
	$gen_page_break = ($k != count($StudentArr)-1);
	$displayStr .= GEN_MAIN_TABLE_CLOSE($gen_page_break);
	
	unset($ActivityTableArr);
	unset($ActivityTableSizeArr);
	unset($ServiceTableArr);
	unset($ServiceTableSizeArr);
	//unset($eleArr);
	unset($OLRRowArr);
	unset($OLETableArr);
	unset($OLETableSizeArr);
	unset($OtherTableArr);
	unset($OtherTableSizeArr);

}

if($_GET['PrintType'] == 1)
{
	$ContentType = "application/x-msword";
	$filename = "export_word_file".date("ymd").".doc";
	$tags_to_strip = Array("A", "a", "HTML", "html", "BODY", "body");
	foreach ($tags_to_strip as $tag)
	{
		$displayStr = preg_replace("/<\/?" . $tag . "(.|\s)*?>/", "", $displayStr);
	}
	
	# Eric Yip (20091008): Replace official photo link
	$displayStr = preg_replace("/(src=)(['\"]?).*(\/file\/official_photo.* ?)/", "$1$2http://{$_SERVER['SERVER_NAME']}$3", $displayStr);
	$displayStr = preg_replace("/(src=)(['\"]?).*(\/file\/portfolio.* ?)/", "$1$2http://{$_SERVER['SERVER_NAME']}$3", $displayStr);
	
	$output = "<HTML>\n";
	$output .= "<HEAD>\n";
	$output .= returnHtmlMETA();
	$output .= "</HEAD>\n";
	$output .= "<BODY LANG=\"zh-HK\">\n";
	$output .= $displayStr;
	$output .= "</BODY>\n";
	$output .= "</HTML>\n";

	header('Pragma: public');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

	header('Content-type: application/octet-stream');
	header('Content-Length: '.strlen($output));

	header('Content-Disposition: attachment; filename="export_ole.doc";');

	print $output;
}
else {
?>

<html>
<head>
	<?=returnHtmlMETA()?>
</head>
<?=$BodyBegin?>

<?=$displayStr?>

</body>
</html>

<?php



}
?>