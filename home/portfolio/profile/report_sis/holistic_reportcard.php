<?php
/*
 * 	Log
 * 	Date:	2014-05-30 [Cameron] Add show fitness or not radio button
 */

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();
$lpf = new libportfolio();

// check iportfolio admin user
$lpf->ACCESS_CONTROL("print_report");

$luser = new libuser($UserID);


$patterns = array (
				"/'/",
				"/\n/"
				);
$replacements = array (
				"\\'",
				""
				);

//$YearArray = $lpf->getYearSemesterForReportPrinting();
////$ClassArray = $lpf->returnClassListData();
//$ClassArray = $lpf->getReportClassListData();
//
//
//if(is_array($ClassArray))
//			{
//				for($i=0; $i<count($ClassArray); $i++)
//				{
//						//$LevelIDActivated[] = $ClassArr[$i]['Year'];
//						$ClassArray[$i] = array($ClassArray[$i], $ClassArray[$i]);
//				}
//		}
//
//
//if(is_array($YearArray))
//			{
//				for($i=0; $i<count($YearArray); $i++)
//				{
//						//$LevelIDActivated[] = $ClassArr[$i]['Year'];
//						$YearArray[$i] = array($YearArray[$i], $YearArray[$i]);
//				}
//		}
////DEBUG_R($YearArray);
//	
//$year_selection_html2 = getSelectByArrayTitle($YearArray, "name='Year'", $range_all, "", false, 2);
$year_selection_html = preg_replace($patterns, $replacements, $year_selection_html2);


# GET portfolio Class 
//$select_class = getSelectByArray($ClassArray, "name='Class'","",0,1);

# GET ALL CLASS
$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=\"Class\" ",$targetClass);


# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_HolisticReport";
$CurrentPageName = 'Holistic Report Card';
### Title ###
$TAGS_OBJ[] = array("Holistic Report Card","");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");
$linterface->LAYOUT_START();



?>

<script language="JavaScript" type="text/JavaScript">
<!--


function jPrintReport()
{
	var url = "holistic_reportcard_print.php";
	document.form1.target = "_blank";
	document.form1.submit();
}

//-->
</script>

<FORM action="holistic_reportcard_print.php" method="GET" name="form1">
	<table border="0" cellpadding="5" cellspacing="0" width="100%">
		<tbody>
			<tr>
				<td width="6%">&nbsp;</td>
				<td>
					<table border="0" cellspacing="0" cellpadding="5" width="100%">
						<!--tr id='yearRow' style='display:block'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
							<td valign="top"><div id='YearDiv'><?=$year_selection_html2?></div></td>
						</tr-->
						<tr>
								<td colspan="2" class="tablename2">Please select class:  </td>
						</tr>
						<tr>
							<td valign="top"><table width="300" border="0" cellpadding="4" cellspacing="0">
								 <tr>
									<td width="100" class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
									<td valign="top"><?=$select_class?></td>
								</tr>   
							</table></td>
						 </tr>

						<tr>
							<td valign="top"><table width="600" border="0" cellpadding="4" cellspacing="0">
								 <tr>
									<td width="100" class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['fitness_report']['show_or_not']?></span></td>
								 	<td>
										<input type="radio" name="show_fitness" id="with_fitness" value="1"><label for="with_fitness"><?= $ec_iPortfolio['fitness_report']['with'] ?></label>&nbsp;&nbsp;
										<input type="radio" name="show_fitness" id="without_fitness" value="0"><label for="without_fitness"><?= $ec_iPortfolio['fitness_report']['without'] ?></label>
									</td>									
								</tr>   
							</table></td>
						 </tr>
						
						<tr>
							<td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
							<td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td>&nbsp;</td>
							<td align="center">
								<input class="formbutton" type="button" value="<?=$button_print?>" onClick="jPrintReport();"; name="btn_submit" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
								<!--<input class="formbutton" type="button" value="<?=$button_cancel?>" onClick="self.close()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">-->
							</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</tbody>
			</td>
		</tr>
	</table>
	
	<input type="hidden" name="StudentID" value="<?=$StudentID?>" />
	<input type="hidden" name="TargetClass" value="<?=$TargetClass?>" />
	<input type="hidden" name="SelectedYear" value="" />
</FORM>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
