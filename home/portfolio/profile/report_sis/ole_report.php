<?php

// Modifing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();
$lpf = new libportfolio();

// check iportfolio admin user
//$lpf->ACCESS_CONTROL("print_report");
$luser = new libuser($UserID);


# get Current Year
$schoolCurrentYrSetting = getCurrentAcademicYear();
$schoolCurrentYr = substr($schoolCurrentYrSetting,0,4);
$schoolNextYr = $schoolCurrentYr+1;
$yrDisplay = $schoolCurrentYr."/".substr($schoolNextYr,2,2);
$YearArray [] = array($schoolCurrentYr, $yrDisplay); // Current Year 
/*
$sql = getAllYearSQL();
$tempYearAry = $lpf->returnArray($sql,1);

for($i=0; $i < sizeof($tempYearAry); $i++)
{
	$yrValue = $tempYearAry[$i]['AcademicYear'];
	$yrDisplay = $yrValue."/".substr(($yrValue+1),2,2);
	$YearArray [] = array($yrValue, $yrDisplay);

}
*/
//debug_r($YearArray);
$select_year = getSelectByArrayTitle($YearArray, "name='Year'", $range_all, "", true, 2);



# GET portfolio Class 
//$ClassArray = $lpf->getReportClassListData();

//if(is_array($ClassArray))
//{
//	for($i=0; $i<count($ClassArray); $i++)
//	{
//			//$LevelIDActivated[] = $ClassArr[$i]['Year'];
//			$ClassArray[$i] = array($ClassArray[$i], $ClassArray[$i]);
//	}
//}
//$select_class = getSelectByArray($ClassArray, "name='Class'","",0,1);

# GET ALL CLASS
$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=\"Class\" ",$targetClass);


# Get CIP Event
$CIPProgramArray = getAllCIPProgram(0);
$select_program = getSelectByArrayTitle($CIPProgramArray, "name='ProgramID[]' multiple size='10'", "-- ".$button_select." --", "", false, 2);

# Get CCA Event
$CCAProgramArray = getAllCIPProgram(1);
$select_cca_program = getSelectByArrayTitle($CCAProgramArray, "name='CCAProgramID[]' multiple size='10'", "-- ".$button_select." --", "", false, 2);

# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_OLEReport";
$CurrentPageName = $iPort['menu']['student_report_printing'];
### Title ###
$TAGS_OBJ[] = array("CIP Report","");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");
$linterface->LAYOUT_START();



function getAllCIPProgram($ProgramType)
{
	global $lpf, $eclass_db;

	$cond = ($ProgramType == 0)?" AND IntExt = 'INT' ":" AND IntExt = 'EXT' ";
	
	$sql = "SELECT ProgramID, Title FROM {$eclass_db}.OLE_PROGRAM WHERE 1 ".$cond;
	$sql .= " ORDER BY Title";
	$result = $lpf->returnArray($sql);

	return $result;
}

?>

<script language="JavaScript" type="text/JavaScript">
<!--


function jPrintReport()
{
	if(checkform())
	{
		var url = "holistic_reportcard_print.php";
		document.form1.target = "_blank";
		document.form1.submit();
	}
}

function checkform()
{
	var obj = document.form1.ReportType;
	var selectedVal, type;

	for(var i = 0; i < obj.length ; i++) {
		if(obj[i].checked) {
			selectedVal = obj[i].value;
		}
	}

	if(selectedVal=='ByClass'  || selectedVal=='AAByClass' )
	{
		if(document.form1.Class.value =='') 
		{
			alert('Please Select Class');
			return false;
		}
	}
	else if(selectedVal== 'ByEvent' )
	{
		if($('select:[name="ProgramID[]"]').val() =='') 
		{
			type ="<?=$iPort["internal_record"]?>";
			alert('Please Select '+type+' Program');
			return false;
		}
	}
	else if(selectedVal=='AAByEvent')
	{
		if($('select:[name="CCAProgramID[]"]').val() =='') 
		{
			type = "<?=$iPort["external_record"]?>";
			alert('Please Select '+type+' Program');
			return false;
		}	
	}
	
	return true;

}
//-->
</script>

<FORM action="ole_report_print.php" method="POST" name="form1">
	<table border="0" cellpadding="5" cellspacing="0" width="100%">
		<tbody>
			<tr>
				<td width="6%">&nbsp;</td>
				<td>
					<table border="0" cellspacing="0" cellpadding="5" width="100%">
						<!--tr id='yearRow' style='display:block'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
							<td valign="top"><div id='YearDiv'><?=$year_selection_html2?></div></td>
						</tr-->
						<tr>
								<td colspan="2" class="tablename2">Please select class:  </td>
						</tr>
						<tr>
							<td valign="top"><table width="200" border="0" cellpadding="4" cellspacing="0">
								 <tr>
									<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
									<td valign="top"><?=$select_year?></td>
								</tr>
								<tr>
									<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
									<td valign="top"><?=$select_class?></td>
								</tr>
								<tr>
									<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$iPort["internal_record"]?></span></td>
									<td valign="top"><?=$select_program?></td>
								</tr>
								<tr>
									<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$iPort["external_record"]?></span></td>
									<td valign="top"><?=$select_cca_program?></td>
								</tr>
							</table></td>
						 </tr>
						 <tr>
									<td class="tablename2">CIP Hours Reports:  </td>
						</tr>
						<tr>
                                <td valign="top"><table border="0" cellpadding="4" cellspacing="0">

                              <?php/*
                                 <tr>
                                      <td nowrap class="tabletext">
                                      	<label for="Average"><input type="radio" name="ReportType" value="Average" id="Average" checked>Community Involvement Program (CIP) Hours - Average Hours</label>
                                      </td>
                                  </tr>  
                                   */?>
								<tr>
                                      <td nowrap class="tabletext">
                                      	<label for="ByClass"><input type="radio" name="ReportType" value="ByClass" id="ByClass">Community Involvement Program (CIP) Hours - By Class</label>
                                      </td>
                                  </tr> 
								  <tr>
                                      <td nowrap class="tabletext">
                                      	<label for="ByEvent"><input type="radio" name="ReportType" value="ByEvent" id="ByEvent">Community Involvement Program (CIP) Hours - By Event</label>
                                      </td>
                                  </tr>
								  <tr>
                              <?php/*
                                      <td nowrap class="tabletext">
                                      	<label for="ByLevel"><input type="radio" name="ReportType" value="ByLevel" id="ByLevel">Community Involvement Program (CIP) Hours - By Level</label>
                                      </td>
                                  </tr> 
                                   */?>

                                </table></td>
                         </tr>
						 <tr>
									<td class="tablename2">Awards & Achievements Reports:  </td>
						</tr>
						<tr>
                                <td valign="top"><table border="0" cellpadding="4" cellspacing="0">
								  <tr>
                                      <td nowrap class="tabletext">
                                      	<label for="AAByClass"><input type="radio" name="ReportType" value="AAByClass" id="AAByClass">Awards & Achievements Report - By Class</label>
                                      </td>
                                  </tr>   
								<tr>
                                      <td nowrap class="tabletext">
                                      	<label for="AAByEvent"><input type="radio" name="ReportType" value="AAByEvent" id="AAByEvent">Awards & Achievements Report - By Event / Competition</label>
                                      </td>
                                  </tr> 
                                </table></td>
                         </tr>
						<tr>
							<td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
							<td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td>&nbsp;</td>
							<td align="center">
								<input class="formbutton" type="button" value="<?=$button_print?>" onClick="jPrintReport();"; name="btn_submit" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
								<!--<input class="formbutton" type="button" value="<?=$button_cancel?>" onClick="self.close()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">-->
							</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</tbody>
			</td>
		</tr>
	</table>
	
	<input type="hidden" name="StudentID" value="<?=$StudentID?>" />
	<input type="hidden" name="TargetClass" value="<?=$TargetClass?>" />
	<input type="hidden" name="SelectedYear" value="" />
</FORM>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
