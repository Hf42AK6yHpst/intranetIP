<?php

// Modifing by Connie
/*
 * Change Log: 
 * Date:	2017-01-20 Villa #P112037 change form submit to self
 */
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-alumni.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
intranet_opendb();

$lpf = new libpf_sturec();
$lpf_slp = new libpf_slp();
$lpf_ui = new libportfolio_ui();

$lpf->ACCESS_CONTROL("student_info");
$lpf->SET_ALUMNI_COOKIE_BACKUP($ck_is_alumni);
/*
// Get class selection with optgroup
$class_name = $i_general_WholeSchool;
$lpf_fc = new libpf_formclass();
$t_classlevel_arr = $lpf_fc->GET_CLASSLEVEL_LIST();
for($i=0; $i<count($t_classlevel_arr); $i++)
{
  $t_classlevel_id = $t_classlevel_arr[$i]["YearID"];
  $t_classlevel_name = $t_classlevel_arr[$i]["YearName"];

  $lpf_fc->SET_CLASS_VARIABLE("YearID", $t_classlevel_id);
  $t_class_arr = $lpf_fc->GET_CLASS_LIST();
  
  for($j=0; $j<count($t_class_arr); $j++)
  {
    $t_yc_id = $t_class_arr[$j][0];
    $t_yc_title = Get_Lang_Selection($t_class_arr[$j]['ClassTitleB5'], $t_class_arr[$j]['ClassTitleEN']);
    if(!is_array($class_teach_arr) || in_array($t_yc_id, $class_teach_arr))
    {
      $class_arr[$t_classlevel_name][] = array($t_yc_id, $t_yc_title);
    }
    
    if($t_yc_id == $YearClassID)
    {
      $class_name = $t_yc_title;
    }
  }
}
//$class_selection_html = getSelectByArray($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, 1, 0, "", 2);
$lpf_ui = new libportfolio_ui();
$class_selection_html = $lpf_ui->GEN_CLASS_SELECTION_OPTGROUP($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID);

# Get student selection
$lpf_fc->SET_CLASS_VARIABLE("YearID", "");
$lpf_fc->SET_CLASS_VARIABLE("YearClassID", $YearClassID);
$default_student_id = "";
$student_id_arr = $lpf_fc->GET_STUDENT_LIST();
$t_student_detail_arr = $lpf_fc->GET_STUDENT_DETAIL_LIST($student_id_arr);
if(is_array($t_student_detail_arr))
{
  for($i=0; $i<count($t_student_detail_arr); $i++)
  {
    $t_classname = $t_student_detail_arr[$i]['ClassName'];
    if(in_array($t_classname, $class_arr)) continue;
  
    $t_user_id = $t_student_detail_arr[$i]['UserID'];
    $t_user_name = "(".$t_classname." - ".$t_student_detail_arr[$i]['ClassNumber'].") ";
    $t_user_name .= Get_Lang_Selection($t_student_detail_arr[$i]['ChineseName'], $t_student_detail_arr[$i]['EnglishName']);
    
    # Set default user ID if class is changed
    if($StudentID == $t_user_id)
      $default_student_id = $StudentID;
  
    $student_detail_arr[] = array($t_user_id, $t_user_name);
  }
}
if($default_student_id == "") $default_student_id = $student_detail_arr[0][0];
$student_selection_html = getSelectByArray($student_detail_arr, "name='StudentID' onChange='jCHANGE_FIELD()'", $default_student_id, 0, 1, "", 2);

# Get activated students
$act_student_id_arr = $lpf_fc->GET_ACTIVATED_STUDENT_LIST();
*/

# Alumni Year Selection
$AlumniYearSelection = $lpf->GEN_ALUMNI_YEAR_SELECTION($Year);

# Generate class selection drop-down list
$ClassSelection = $lpf->GEN_ALUMNI_CLASS_SELECTION($Year, $ClassName);

# Generate student selection drop-down list and get a student list
list($student_selection_html, $student_list) = $lpf->GEN_STUDENT_LIST_INFO($ClassName, $StudentID, true, 1, "name='StudentID' onChange='document.form1.submit()'", $Year);

# Set default student ID, to cater change of class
# If not doing so, it will load data of student in previous page
for($i=0, $i_max=count($student_list); $i<$i_max; $i++)
{
  $_student_id = $student_list[$i]["UserID"];
  if($_student_id == $StudentID)
  {
    $default_student_id = $_student_id;
    break;
  }
}
if(empty($default_student_id))
{
  $default_student_id = $student_list[0]["UserID"];
}

# Retrieve student info
$lpf_acc = new libpf_account_alumni(); 
$lpf_acc->SET_CLASS_VARIABLE("user_id", $default_student_id);
$lpf_acc->SET_STUDENT_PROPERTY();
$lpf_acc->SET_PARENT();

$student_name = $lpf_acc->GET_CLASS_VARIABLE(Get_Lang_Selection("chinese_name", "english_name"));

$student_photo_link = $lpf_acc->GET_IPORTFOLIO_PHOTO();
$student_photo_link = "<img src=\"".$student_photo_link."\" width=\"100\" width=\"130\" />";

$student_info_display = $lpf_ui->GEN_STUDENT_INFO_TABLE($lpf_acc);
//debug_r($lpf_acc);
$class_history_display = $lpf->GEN_CLASS_HISTORY_TABLE($default_student_id);
$house_display = $lpf_ui->GEN_STUDENT_HOUSE_TABLE($lpf_acc);
$parent_display = $lpf_ui->GEN_PARENT_INFO_TABLE($lpf_acc);

# Self account
$self_account_arr = $lpf_slp->GET_DEFAULT_SELF_ACCOUNT($default_student_id);
if(is_array($self_account_arr))
{
	foreach($self_account_arr AS $sa_record_id => $DefaultSA)
	{
		if(in_array("SLP", $DefaultSA))
		{
			$self_account_default_arr = $lpf_slp->GET_SELF_ACCOUNT($sa_record_id);
		}
	}
}
$self_account_display = $lpf_ui->GEN_SELF_ACCOUNT_TABLE($sa_record_id, $self_account_default_arr, $YearClassID, $default_student_id);

/*
$student_obj = $li_pf->GET_STUDENT_OBJECT($default_student_id);
	
# Set links for photo
if(strpos($student_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $student_obj['PhotoLink'] != "")
	$student_obj['PhotoLink'] = str_replace("<!--ImageStyle-->", "", $student_obj['PhotoLink']);
else if($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
else
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";

# generate student info table
$student_info_display = $li_pf->GEN_STUDENT_INFO_TABLE($default_student_id, $student_obj);

# generate class history table
$class_history_display = $li_pf->GEN_CLASS_HISTORY_TABLE($default_student_id);

# generate house and admission date table
$house_display = $li_pf->GEN_STUDENT_HOUSE_TABLE($default_student_id, $student_obj);

# generate student parent info table
$parent_display = $li_pf->GEN_PARENT_INFO_TABLE($default_student_id);

# generate student self-account table
$self_account_display = $lpf_slp->GEN_SELF_ACCOUNT_TABLE($default_student_id);
*/

$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['year_list'], "../school_records_alumni.php");
$MenuArr[] = array($Year, "../school_records_alumni_year.php?Year={$Year}");
$MenuArr[] = array($student_name, "");

$linterface = new interface_html();
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];
### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");
$linterface->LAYOUT_START();

?>


<? // ===================================== Body Contents ============================= ?>

<script language="JavaScript">

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	document.form1.action = "student_info_teacher_alumni.php";
	document.form1.submit();
}

// Quick student switch 
function jCHANGE_STUDENT(jParShift, jParToExtreme)
{
	var StudentSelect = document.getElementsByName('StudentID');
	var OriginalIndex = StudentSelect[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(!jParToExtreme)
	{
		if(TargetIndex >= StudentSelect[0].length)
		{
			StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			return;
		}
		else if(TargetIndex < 0)
		{
			StudentSelect[0].selectedIndex = 0;
			return;
		}
		else
			StudentSelect[0].selectedIndex = TargetIndex;
	}
	else
	{
		if(jParShift < 0)
		{
			if(OriginalIndex > 0)
				StudentSelect[0].selectedIndex = 0;
			else
				return;
		}
		else
		{
			if(OriginalIndex < StudentSelect[0].length-1)
				StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			else
				return;
		}
	}
	
	document.form1.action = "student_info_teacher_alumni.php";
	document.form1.submit();
}

// Search student name
function jSUBMIT_SEARCH(){
	document.form1.action = "../school_records_alumni_year.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_LP()
{
	document.form1.action = "learning_portfolio_teacher_alumni.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_SR()
{
	document.form1.action = "school_record_teacher_alumni.php";
	document.form1.submit();
}

// Change page to display school base scheme
function jTO_SBS()
{
	document.form1.action = "sbs/index.php";
	document.form1.submit();
}

$(document).ready(function(){
  $("input[name=search_name]").keypress(function(event) {
    if(event.keyCode == '13') {
      jSUBMIT_SEARCH();
    }
  });
  $("#submit_search").click(function(){
    jSUBMIT_SEARCH();
  });
});
</script>

<FORM name="form1" method="POST" action="student_info_teacher_alumni.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan=2>
				<table border="0" cellspacing="0" cellpadding="3" width="100%">
					<tr>
						<td><?=$AlumniYearSelection?> <?=$ClassSelection?></td>
						<td align="right">
							<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$ec_iPortfolio['enter_student_name']:intranet_htmlspecialchars(stripslashes($search_name)))?>" onFocus="if(this.value=='<?=$ec_iPortfolio['enter_student_name']?>'){this.value=''}" onBlur="if(this.value==''){this.value='<?=$ec_iPortfolio['enter_student_name']?>'}" />
							<input id="submit_search" type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" />
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td class="navigation"><?=$linterface->GET_NAVIGATION($MenuArr) ?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="17" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_01_tea.gif" width="17" height="37"></td>
						<td height="37" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_03_tea.gif">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="200">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center" valign="middle" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_02_tea.gif"  class="page_title"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_stu_info.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0" align="absmiddle"> <?=$ec_iPortfolio['heading']['student_info']?></td>
											</tr>
										</table>
									</td>
									<td align="left" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#E8E8E8">
											<tr>
												<td align="left">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr align="center" valign="middle">
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(-1, true)" class="tablebottomlink">&lt;&lt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(-1, false)" class="tablebottomlink">&lt;</a></td>
															<td nowrap>
																<span class="tabletext">
																	<?=$student_selection_html?>
																</span>
															</td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(1, false)" class="tablebottomlink">&gt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(1, true)" class="tablebottomlink">&gt;&gt;</a></td>
															<td width="15" nowrap> | </td>
															<td nowrap class="tabletext"><?=str_replace("<!--NoRecord-->", count($student_list), $ec_iPortfolio['total_record'])?></td>
														</tr>
													</table>
												</td>
												<td align="right">
													<table border="0" cellspacing="0" cellpadding="5">
														<tr>
															<td nowrap background="#"></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_04_tea.gif" width="13" height="37"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif" width="17" height="20"></td>
						<td>
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<tr>
									<td valign="top">
										<table border="0" cellspacing="0" cellpadding="0" >
											<tr>
												<td>
													<table border="0" cellspacing="0" cellpadding="0" >
														<tr>
															<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_01.gif" style="padding-left:10px; padding-top:10px;"><?=$student_photo_link?></td>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="10" height="10"></td>
														</tr>
														<tr>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
															<td height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_04.gif" width="10" height="10"></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" class="tabletext"><?=$student_name?></td>
											</tr>
											<tr>
												<td align="center">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr>
															<td valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_stu_info_on.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0"></td>
<?php if($lpf->HAS_SCHOOL_RECORD_VIEW_RIGHT()) { ?>
															<td valign="top"><a href="javascript:jTO_SR()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record.gif" alt="<?=$ec_iPortfolio['school_record']?>" width="20" height="20" border="0"></a></td>
<?php } ?>
<?php if(strstr($ck_user_rights, ":web:") && $lpf->HAS_RIGHT("learning_sharing")) { ?>
															<td valign="top">
																<a href="javascript:jTO_LP()">
																	<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_learning_portfolio.gif" alt="<?=$iPort['menu']['learning_portfolio']?>" width="20" height="20" border="0"><br />
																	<?=$lpf->GET_LP_NOT_VIEW_COUNT_BY_USER_ID($default_student_id) > 0 ? "<span class='new_alert'>New</span>" : ""?>
																</a>
															</td>
<?php } ?>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
									<td>
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td>
													<table border="0" cellpadding="5" cellspacing="0" width="100%">
														<tr>
															<td class="sub_page_title"><?=$ec_iPortfolio['basic_info']?></td>
														</tr>
														<tr>
														  <td width="65%" align="center" valign="top" class="stu_info_log stu_info_log_main"><?=$student_info_display?></td>
														  <td valign="top" height="100%" class="stu_info_log stu_info_log_main">
														  	<table border="0" cellpadding="5" cellspacing="0" width="100%">
														  		<tr>
														  			<td align="center" valign="top"><?=$class_history_display?></td>
																	</tr>
														  		<tr>
														  			<td align="center" valign="top"><?=$house_display?></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													<br />
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="sub_page_title"><?=$ec_iPortfolio['guardian_info']?></td>
														</tr>
														<tr>
										  				<td class="stu_info_log">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td><?=$parent_display?></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													<br />
													<table border="0" cellpadding="5" cellspacing="0" width="100%">
														<tr>
															<td class="sub_page_title"><?=$ec_iPortfolio['self_account']?></td>
														</tr>
														<tr>
														  <td class="stu_info_log stu_info_log_main">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td><?=$self_account_display?></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif" width="13" height="37"></td>
					</tr>
					<tr>
						<td width="17" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_08.gif" width="17" height="17"></td>
						<td height="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif" width="64" height="17"></td>
						<td width="13" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_10.gif" width="13" height="17"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>" />
	<input type="hidden" name="FieldChanged" />
</FORM>

<? // ===================================== Body Contents (END) ============================= ?>

<?php
$lpf->RELOAD_ALUMNI_COOKIE_BACKUP($ck_is_alumni);
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
