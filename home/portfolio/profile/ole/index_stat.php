<?php
## Using By : ivan
##########################################################
## Modification Log
## 
## 2010-03-24: Eric
## - Export in correct format and encoding
## 2009-12-04: Max (200912041055)
## - Allow export
##########################################################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

//$lpf_ui = new libportfolio2007();
$li_pf = new libpf_slp();

$li_pf->ACCESS_CONTROL("ole");
$thisUserID = $_SESSION['UserID'];

# Resetting page number
if(!isset($Page)) $Page = 1;
switch($FieldChanged)
{
	case "year":
		$ClassLevel = "";
	case "classlevel":
		$Class = "";
	case "search":
	case "division":
	case "class":
	case "ele":
		$Page = 1;
		break;
	case "page":
	default:
		break;
}

# cateogry selection
$category_selection = $li_pf->GET_OLE_CATEGORY_SELECTION("name='category' onChange='this.form.submit()'", $category, 1);
$category_selection_html = $category_selection;

// 2011-0125-1633-24067 (Ivan): change to general year selection which show all academic years
//$YearStr = $li_pf->GEN_OLE_YEAR_SELECTION($Year, "name='Year' onChange=\"jCHANGE_FIELD('year')\"");
$YearStr = getSelectAcademicYear('Year', 'onChange="jCHANGE_FIELD(\'year\')";', $noFirst=1, $noPastYear=0, $Year, $displayAll=1);

//$ClassLevelStr = $li_pf->GEN_OLE_CLASSLEVEL_SELECTION($Year, $ClassLevel, "name='ClassLevel' onChange=\"jCHANGE_FIELD('classlevel')\"");
$ClassLevelStr = $li_pf->GEN_OLE_CLASSLEVEL_SELECTION('', $ClassLevel, "name='ClassLevel' onChange=\"jCHANGE_FIELD('classlevel')\"", $Year);
//$ClassStr = $li_pf->GEN_OLE_CLASS_SELECTION($Year, $ClassLevel, $Class);
$ClassStr = $li_pf->GEN_OLE_CLASS_SELECTION('', $ClassLevel, $Class, $Year);
$ELEStr = $li_pf->GEN_OLE_ELE_SELECTION($ELE);
if($search_name == $ec_iPortfolio['enter_student_name'])
	$search_name = "";
if($Field == "")
{
	$Field = "2";
	$Order = "1|1";
}

//echo "PageDivision=>$PageDivision, Page=>$Page, Year=>$Year, ClassLevel=>$ClassLevel, Class=>$Class, ELE=>$ELE, EMPTY=>, search_name=>$search_name, Field=>$Field, Order=>$Order, Category=>$Category<br>";
//list($olr_display, $olr_list) = $li_pf->GEN_OLE_STATISTIC_TABLE(array($PageDivision, $Page), $Year, $Year, $ClassLevel, $Class, $ELE, "", $search_name, $Field, $Order, $category);
list($olr_display, $olr_list) = $li_pf->GEN_OLE_STATISTIC_TABLE(array($PageDivision, $Page), '', '', $ClassLevel, $Class, $ELE, "", $search_name, $Field, $Order, $category, $Year, $Year);

# Handling export
if ($export == '1') {
	$categoryList = $li_pf->GET_OLR_Category();
	$ELE_Name = $li_pf->GET_ELE();

	$ExportContent = generateExportContent($olr_list, $PageDivision, $Page, $ELE);
	// Output the file to user browser
	$year_name = "_".( (isset($Year) && !empty($Year)) ? $Year : "All" ) .
	( (isset($ClassLevel) && !empty($ClassLevel)) ? "_".$ClassLevel : "" ).
	( (isset($Class) && !empty($Class)) ? "_".$Class : "" ).
	( (isset($categoryList[$category]) && !empty($categoryList[$category])) ? "_".str_replace(" ", "", $categoryList[$category]) : "" ).
	( (isset($ELE_Name["[$ELE]"]) && !empty($ELE_Name["[$ELE]"])) ? "_".str_replace(" ", "", $ELE_Name["[$ELE]"]) : "" );
	$filename = "olr_individuals".$year_name.".csv";
	
	$lexport = new libexporttext();
	$lexport->EXPORT_FILE($filename, $ExportContent);
	exit;
/*
	if ($g_encoding_unicode) {
		$lexport->EXPORT_FILE($filename, $ExportContent);
	} else {
		output2browser($ExportContent, $filename);
		exit; # This exit is used to terminate the output
	}
*/
}

function generateExportContent($dataArray, $PageDivision="", $Page=1, $ELE="") {
	global $i_UserName, $i_UserClassName, $i_UserClassNumber, $iPort, $ec_iPortfolio; # text in language
	global $olr, $li_pf, $g_encoding_unicode;
	$content = '';	

	$ELE_list = $li_pf->GET_OLE_ELE_LIST();

	$caption = array(
					'#', $i_UserName, $i_UserClassName, $i_UserClassNumber, $iPort['no_of_program'], $ec_iPortfolio['total_hours']					
					);

	if (isset($ELE) && !empty($ELE)) {
		foreach($ELE_list as $ELE_element) {
			if ($ELE_element['DefaultID'] == "[$ELE]") {
				$caption[] = $ELE_element['Title'];
			}
		}
	} else {
		foreach($ELE_list as $ELE_element) {
			$caption[] = $ELE_element['Title'];
		}
	}

/*	
	$DELIMITER = ($g_encoding_unicode) ? "\t" : ",";	
	$VALUEQUOTE = ($g_encoding_unicode) ? "" : "\"";
*/
	$DELIMITER = "\t";	
	$VALUEQUOTE = "";
	$LINE_BREAK = "\n";
	//$LINE_BREAK = "<br>";
	
	# Formation of Caption Row
	if (is_array($caption)) {
		reset($caption);
		$content .= $VALUEQUOTE.current($caption).$VALUEQUOTE;
		next($caption);
		while($element = current($caption)) {
			$content .= $DELIMITER.$VALUEQUOTE.$element.$VALUEQUOTE;
			next($caption);
		}
		$content .= $LINE_BREAK;
		reset($caption);
	}
	# Formation of Detail Rows
	if (is_array($dataArray)) {
		if (isset($PageDivision) && !empty($PageDivision)) {
			
		} else {
			$PageDivision = count($dataArray);
		}
		$iRow = 0; // number of row
		
		reset($dataArray);
		while($element = current($dataArray)) {
			++$iRow;
			if ( $iRow > (($Page-1)*$PageDivision) && $iRow < ($Page*$PageDivision)+1 ) {
				$content .= $VALUEQUOTE.($iRow).$VALUEQUOTE.$DELIMITER;
				$content .= $VALUEQUOTE.$element['Name'].$VALUEQUOTE.$DELIMITER;
				$content .= $VALUEQUOTE.$element['ClassName'].$VALUEQUOTE.$DELIMITER;
				$content .= $VALUEQUOTE.$element['ClassNumber'].$VALUEQUOTE.$DELIMITER;
				$content .= $VALUEQUOTE.(isset($element['ProgramNum']) && !empty($element['ProgramNum']) ? $element['ProgramNum'] : 0 ).$VALUEQUOTE.$DELIMITER;
				$content .= $VALUEQUOTE.(isset($element['TotalHour']) && !empty($element['TotalHour']) ? $element['TotalHour'] : 0 ).$VALUEQUOTE.$DELIMITER;
				
				if (is_array($ELE_list)) {
					reset($ELE_list);
					if (isset($ELE) && !empty($ELE)) {
						while($ele = current($ELE_list)) {
							$ele = current($ELE_list);
							$eleID = str_replace(array("[","]"), "", $ele['DefaultID']);
							if ($ELE == $eleID) {
								$content .= $VALUEQUOTE.( (isset($element[$eleID]) && !empty($element[$eleID])) ? $element[$eleID] : 0 ).$VALUEQUOTE;
							}
							next($ELE_list);
						}
					} else {
						$ele = current($ELE_list);
						$eleID = str_replace(array("[","]"), "", $ele['DefaultID']);
						$content .= $VALUEQUOTE.(isset($element[$eleID]) && !empty($element[$eleID]) ? $element[$eleID] : 0).$VALUEQUOTE;
						next($ELE_list);
						while($ele = current($ELE_list)) {
							$ele = current($ELE_list);
							$eleID = str_replace(array("[","]"), "", $ele['DefaultID']);
							$content .= $DELIMITER.$VALUEQUOTE.( (isset($element[$eleID]) && !empty($element[$eleID])) ? $element[$eleID] : 0 ).$VALUEQUOTE;
							next($ELE_list);
						}
					}
					reset($ELE_list);
				}
				$content .= $LINE_BREAK;
			}
			next($dataArray);
		}
	}
	return $content;
}


// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_OLEReport";
$CurrentPageName = $iPort['menu']['ole_report'];

$luser = new libuser($thisUserID);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

$TabMenuArr = array();
$TabMenuArr[] = array("index_stat.php", $ec_iPortfolio['statistic'], 1);
if($special_feature['ole_chart'])
{
	$TabMenuArr[] = array("demo/index_stat.php", $ec_iPortfolio['chart_report'], 0);
	$TabMenuArr[] = array("demo/cat_stat_chart.php", $ec_iPortfolio['cat_stat_chart'], 0);
}
$TabMenuArr[] = array("index_analysis.php", $ec_iPortfolio['adv_analyze'], 0);

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField){
	document.form1.FieldChanged.value = jParField;
	document.form1.action = "index_stat.php";
	document.form1.submit();
}

// Change pages
function jCHANGE_PAGE(jParShift){
	var PageSelection = document.getElementsByName("Page");
	var OriginalIndex = PageSelection[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(TargetIndex >= PageSelection[0].length)
	{
		PageSelection[0].selectedIndex = PageSelection[0].length-1;
		return;
	}
	else if(TargetIndex < 0)
	{
		PageSelection[0].selectedIndex = 0;
		return;
	}
	else
		PageSelection[0].selectedIndex = TargetIndex;

	document.form1.FieldChanged.value = "page";
	document.form1.action = "index_stat.php";
	document.form1.submit();	
}

// Search student name
function jSUBMIT_SEARCH(){
	document.form1.FieldChanged.value = "search";
	document.form1.action = "index_stat.php";
	document.form1.submit();
}

// Sort table by field
function jSORT_TABLE(jParField, jParOrder)
{
	document.form1.Field.value = jParField;
	document.form1.Order.value = jParOrder;
	
	document.form1.action = "index_stat.php";
	document.form1.submit();	
}

// Go to generate chart
function goClassStatChart() {
	var urlStr = 'demo/class_stat_chart.php';
	urlStr = urlStr + '?startYear=<?=$Year?>&endYear=<?=$Year?>&classLevel=<?=$ClassLevel?>&class=<?=$Class?>&eleRecord=<?=$ELE?>&hourRange=&searchName=<?=$search_name?>&field=<?=$Field?>&order=<?=$Order?>&category=<?=$category?>';
	
	if(document.form1.Class.value!=''){
		newWindow(urlStr, 9)
	} else {
		alert('<?=$ec_warning['please_select_class']?>');
	}
}

function doExport() {
	document.getElementsByName('export')[0].value = 1;
	document.form1.action = "index_stat.php";
	document.form1.submit();
	document.getElementsByName('export')[0].value = 0;
}
</SCRIPT>

<FORM method='POST' name='form1' action='index_stat.php'>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
      <?=$li_pf->GET_TAB_MENU($TabMenuArr)?>

			<table width="96%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table border="0" cellspacing="0" cellpadding="2">
										<tr>
							<?php if(strstr($ck_function_rights, "ExportData")) { ?>
											<td>
												<a href="javascript: void(0);" class="contenttool" onClick="doExport();">
												<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_export.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_export?> </a>
												</a>
											</td>
							<?php } ?>
										</tr>
									</table>
									<table border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td>
												<?=$YearStr?>
												<?=$ClassLevelStr?>
												<?=$ClassStr?>
<?php if($special_feature['ole_chart']) { ?>
												<!--<a href="javascript:if(document.form1.Class.value!='') newWindow('demo/class_stat_chart.php', 9); else alert('<?=$ec_warning['please_select_class']?>');"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_files/icon_resultview.gif" border="0" /></a>-->
												<a href="javascript: void(0);" onclick="goClassStatChart();"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_files/icon_resultview.gif" border="0" /></a>
<?php } ?>
											</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
										<td>
										<?=$category_selection_html?>
										<?=$ELEStr?>
										</td>
										<td>&nbsp;</td>
										</tr>
									</table>
								</td>
								<td align="right" valign="bottom">
									<span class="tabletext">
										<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$ec_iPortfolio['enter_student_name']:intranet_htmlspecialchars(stripslashes($search_name)))?>" onFocus="if(this.value=='<?=$ec_iPortfolio['enter_student_name']?>'){this.value=''}" onBlur="if(this.value==''){this.value='<?=$ec_iPortfolio['enter_student_name']?>'}" />
										<input type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onClick="jSUBMIT_SEARCH()" />
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
							<?=$olr_display?>
					</td>
				</tr>
				<tr class="tablebottom">
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td class="tabletext"><?=$ec_iPortfolio['record']?> <?=$li_pf->GEN_PAGE_ROW_NUMBER(count($olr_list), $PageDivision, $Page)?>, <?=str_replace("<!--NoRecord-->", count($olr_list), $ec_iPortfolio['total_record'])?></td>
								<td align="right">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
												<table border="0" cellspacing="0" cellpadding="2">
													<tr align="center" valign="middle">
														<td><a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('prevp22','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(-1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_off.gif" name="prevp22" width="11" height="10" border="0" align="absmiddle" id="prevp22"></a> <span class="tabletext"> <?=$list_page?> </span></td>
														<td class="tabletext">
															<?=$li_pf->GEN_PAGE_SELECTION(count($olr_list), $PageDivision, $Page, "name='Page' class='formtextbox' onChange='jCHANGE_FIELD(\"page\")'")?>
														</td>
														<td><span class="tabletext"> </span><a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('nextp22','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_off.gif" name="nextp22" width="11" height="10" border="0" align="absmiddle" id="nextp22"></a></td>
													</tr>
												</table>
											</td>
											<td>&nbsp;<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
											<td>
												<table border="0" cellpadding="2" cellspacing="0" class="tabletext">
													<tr>
														<td><?=$i_general_EachDisplay?></td>
														<td>
															<?=$li_pf->GEN_PAGE_DIVISION_SELECTION($PageDivision, "name='PageDivision' class='formtextbox' onChange='jCHANGE_FIELD(\"division\")'", 10)?>
														</td>
														<td><?=$i_general_PerPage?></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
		</td>
	</tr>
</table>


<input type="hidden" name="export" value="<?=$export?>" />
<input type="hidden" name="FieldChanged" />
<input type="hidden" name="Field" value="<?=$Field?>" />
<input type="hidden" name="Order" value="<?=$Order?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
