<?php
# Under development : 20091120
# Modifing by Max

/* Debug using parameters */
$HR = "<hr />";
$BR = "<br />";


$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

#################### chart #####################################################################
include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");
include_once("demoData.php");
#################### chart #####################################################################
include_once($PATH_WRT_ROOT."includes/lib.php");

$levelIndex = $_POST['levelIndex'];
$classIndex = $_POST['classIndex'];
$studentRangeIndex = $_POST['studentrangeIndex'];
$studentIndex = $_POST['studentrangeIndex'];

intranet_opendb();

$li_pf = new libpf_slp();

$li_pf->ACCESS_CONTROL("ole");
$thisUserID = $_SESSION['UserID'];

# Resetting page number
if(!isset($Page)) $Page = 1;
switch($FieldChanged)
{
	case "year":
		$ClassLevel = "";
	case "classlevel":
		$Class = "";
	case "search":
	case "division":
	case "class":
	case "ele":
		$Page = 1;
		break;
	case "page":
	default:
		break;
}

# cateogry selection
$category_selection = $li_pf->GET_OLE_CATEGORY_SELECTION("name='category' onChange='this.form.submit()'", $category, 1);
$category_selection_html = $category_selection;

$YearStr = $li_pf->GEN_OLE_YEAR_SELECTION($Year, "name='Year' onChange=\"jCHANGE_FIELD('year')\"");
//$ClassLevelStr = $li_pf->GEN_OLE_CLASSLEVEL_SELECTION($Year, $ClassLevel, "name='ClassLevel' onChange=\"jCHANGE_FIELD('classlevel')\"");
$ClassLevelArr = $li_pf->GET_OLE_CLASSLEVEL_LIST();
$ClassLevel = $ClassLevelArr[0];
$ClassStr = $li_pf->GEN_OLE_CLASS_SELECTION($Year, $ClassLevel, $Class);
$ELEStr = $li_pf->GEN_OLE_ELE_SELECTION($ELE);

if($search_name == $ec_iPortfolio['enter_student_name'])
	$search_name = "";
if($Field == "")
{
	$Field = "2";
	$Order = "1|1";
}

list($olr_display, $olr_list) = $li_pf->GEN_OLE_STATISTIC_TABLE(array($PageDivision, $Page), $Year, $Year, $ClassLevel, $Class, $ELE, "", $search_name, $Field, $Order, $category);

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_OLEReport";
$CurrentPageName = $iPort['menu']['ole_report'];

$luser = new libuser($thisUserID);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

$TabMenuArr = array();
$TabMenuArr[] = array("../index_stat.php", $ec_iPortfolio['statistic'], 0);
$TabMenuArr[] = array("index_stat.php", $ec_iPortfolio['chart_report'], 1);
$TabMenuArr[] = array("cat_stat_chart.php", $ec_iPortfolio['cat_stat_chart'], 0);
$TabMenuArr[] = array("../index_analysis.php", $ec_iPortfolio['adv_analyze'], 0);

#################### chart #####################################################################

//$sub_chart = new open_flash_chart();
//$sub_sub_chart = new open_flash_chart();


	/* get data from here */

//function GenerateHourClass(){
	
//}

//GenerateHourClass();
	
#################### chart #####################################################################

$linterface->LAYOUT_START();
?>

<!--#################### chart #####################################################################-->


<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/json/json2.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/swfobject.js"></script>	
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/ajax.js"></script>
<!--################### chart #####################################################################-->


<SCRIPT LANGUAGE="Javascript">
// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField){
	document.form1.FieldChanged.value = jParField;
	document.form1.action = "index_stat.php";
	document.form1.submit();
}

// Change pages
function jCHANGE_PAGE(jParShift){
	var PageSelection = document.getElementsByName("Page");
	var OriginalIndex = PageSelection[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(TargetIndex >= PageSelection[0].length)
	{
		PageSelection[0].selectedIndex = PageSelection[0].length-1;
		return;
	}
	else if(TargetIndex < 0)
	{
		PageSelection[0].selectedIndex = 0;
		return;
	}
	else
		PageSelection[0].selectedIndex = TargetIndex;

	document.form1.FieldChanged.value = "page";
	document.form1.action = "index_stat.php";
	document.form1.submit();	
}

// Search student name
function jSUBMIT_SEARCH(){
	document.form1.FieldChanged.value = "search";
	document.form1.action = "index_stat.php";
	document.form1.submit();
}

// Sort table by field
function jSORT_TABLE(jParField, jParOrder)
{
	document.form1.Field.value = jParField;
	document.form1.Order.value = jParOrder;
	
	document.form1.action = "index_stat.php";
	document.form1.submit();	
}

//#################### chart #####################################################################
function ofc_ready(){
	//alert('ofc_ready');
}

function open_flash_chart_data(){
	//alert("open_flash_chart_data");
	//alert( JSON.stringify(data) );
	//alert(typeof data);
	if(typeof data == "string"){	
		return data;
	}else{
		return JSON.stringify(data);
	}	
}

function findSWF(movieName) {
  if (navigator.appName.indexOf("Microsoft")!= -1) {
	return window[movieName];
  } else {
	return document[movieName];
  }
}

function setChart(id, display){
	//for testing
	//var heading = document.getElementById("testHead");
	//while(heading.hasChildNodes()){
	//	heading.removeChild(heading.firstChild);
	//}
	//var h = document.createTextNode(String(id));
	//heading.appendChild(h);
	//var content = document.getElementById("testContent");
	//while(content.hasChildNodes()){
	//	content.removeChild(content.firstChild);
	//}			
	//var c = document.createTextNode(String(display));
	//content.appendChild(c);
}


function expandSubLevel(id, index, group){

	if (id == 'all') {
		allIndex = index;
		document.getElementsByName('allIndex')[0].value = index;
		callingID = 'all';
	} else if(id == 'level'){
		levelIndex = index;
		document.getElementsByName('levelIndex')[0].value = index;
		callingID = 'level';
	} else if(id == 'class'){
		classIndex = index;
		document.getElementsByName('classIndex')[0].value = index;
		callingID = 'class';
	} else if(id == 'studentrange'){
		studentrangeIndex = index;
		document.getElementsByName('studentrangeIndex')[0].value = index;
		callingID = 'studentrange';		
	} else if(id == 'student'){
		studentIndex = index;
		document.getElementsByName('studentIndex')[0].value = index;
		callingID = 'student';
	}
	//alert("[group] = " + group + "\nlevelIndex = " + levelIndex + "\nclassIndex = " + classIndex + "\nstudentRangeIndex" + studentrangeIndex + "\nstudentIndex = " + studentIndex);		
	//ajax	request
	
	//if(id=='all' || id=='level' || id=='class' || id=='studentrange'){
	if(id=='all' || id=='level' || id=='class'){
	var urlPar = "id="+id+"&levelIndex="+levelIndex+"&classIndex="+classIndex+"&studentrangeIndex="+studentrangeIndex+"&studentIndex="+studentIndex+"&group="+group;
	//alert("Doing the url => " + urlPar);
	 var xml = $.ajax({
	  url: "../../../ajax/generateChart.php",
	  data: urlPar,
	  async: false
	 }).responseXML.documentElement;
	 //alert("Doing the callingID = " + callingID);
	 stateChanged(callingID, xml);
//		//var xmlHttp=GetXmlHttpObject();
//		xmlHttp=GetXmlHttpObject();
//		if (xmlHttp==null) {
//			alert ("Your browser does not support AJAX!");
//			return;
//		}
//		var url="../../../ajax/generateChart.php";
//		url=url+"?id="+id;
//		url=url+"&levelIndex="+levelIndex;		
//		url=url+"&classIndex="+classIndex;
//		url=url+"&studentrangeIndex="+studentrangeIndex;		
//		url=url+"&studentIndex="+studentIndex;
//		url=url+"&group="+group;
//		alert(url);
////		xmlHttp.onreadystatechange = function() {
////			stateChanged(xmlHttp, callingID);
////		}
//		xmlHttp.onreadystatechange = stateChanged;
//		xmlHttp.open("GET",url,true);
//		xmlHttp.send(null);
	}
//	else if( id=='studentrange'){
//		var st = document.getElementById("studentTable");
//		st.style.display = "block";
//		var st2 = document.getElementById("studentTable2");
//		st2.style.display = "block";
//	}	
}	

function stateChanged(callingID, xmlDoc)
{
	//alert(callingID);
		data = completeXMLData(xmlDoc.getElementsByTagName("data")[0].childNodes);
		//alert(data.length);
		var studentListTable = completeXMLData(xmlDoc.getElementsByTagName("studentListTable")[0].childNodes);
		var studentListTable2 = completeXMLData(xmlDoc.getElementsByTagName("studentListTable2")[0].childNodes);

		if (callingID=='all') {
		//alert("in all");
			var sub = document.getElementById("sub_chart");
			sub.style.display = "none";		
			var sub_sub = document.getElementById("sub_sub_chart");
			sub_sub.style.display = "none";		
			var st = document.getElementById("studentTable");
			st.style.display = "none";
			var st2 = document.getElementById("studentTable2");
			st2.style.display = 'none';
			swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "my_chart", "550", "350", "9.0.0");	
		} else if(callingID=='level'){
		//alert("in level");
			var sub = document.getElementById("sub_chart");
			sub.style.display = "block";		
			var sub_sub = document.getElementById("sub_sub_chart");
			sub_sub.style.display = "none";		
			var st = document.getElementById("studentTable");
			st.style.display = "none";
			var st2 = document.getElementById("studentTable2");
			st2.style.display = 'none';			
			swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "sub_chart", "550", "350", "9.0.0");
		} else if(callingID=='class'){
		//alert("in class");
			var sub_sub = document.getElementById("sub_sub_chart");
			sub_sub.style.display = "block";	
			var st = document.getElementById("studentTable");
			st.style.display = "none";
			var st2 = document.getElementById("studentTable2");
			st2.style.display = 'none';				
			swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "sub_sub_chart", "550", "350", "9.0.0");
		} else if (callingID=='studentrange') {
		//alert("in studentrange");
			if (studentListTable != "NULL") {
				var st = document.getElementById("studentTable");
				st.style.display = '';
				var st2 = document.getElementById("studentTable2");
				st2.style.display = '';
				st.innerHTML = studentListTable;
				st2.innerHTML = studentListTable2;
			}
		}
	
}
function completeXMLData(node) {
	var nodeLen = node.length;
	var retStr = '';
	for(i=0;i<nodeLen;i++) {
		retStr = retStr + node[i].nodeValue;
	}
	return retStr;
}

//function expandSubLevel(id, index, group){
//
//	if (id == 'all') {
//		allIndex = index;
//		document.getElementsByName('allIndex')[0].value = index;
//		callingID = 'all';
//	} else if(id == 'level'){
//		levelIndex = index;
//		document.getElementsByName('levelIndex')[0].value = index;
//		callingID = 'level';
//	} else if(id == 'class'){
//		classIndex = index;
//		document.getElementsByName('classIndex')[0].value = index;
//		callingID = 'class';
//	} else if(id == 'studentrange'){
//		studentrangeIndex = index;
//		document.getElementsByName('studentrangeIndex')[0].value = index;
//		callingID = 'studentrange';		
//	} else if(id == 'student'){
//		studentIndex = index;
//		document.getElementsByName('studentIndex')[0].value = index;
//		callingID = 'student';
//	}
//	//alert("[group] = " + group + "\nlevelIndex = " + levelIndex + "\nclassIndex = " + classIndex + "\nstudentRangeIndex" + studentrangeIndex + "\nstudentIndex = " + studentIndex);		
//	//ajax	request
//	if(id=='all' || id=='level' || id=='class' || id=='studentrange'){
//		//var xmlHttp=GetXmlHttpObject();
//		xmlHttp=GetXmlHttpObject();
//		if (xmlHttp==null) {
//			alert ("Your browser does not support AJAX!");
//			return;
//		}
//		var url="../../../ajax/generateChart.php";
//		url=url+"?id="+id;
//		url=url+"&levelIndex="+levelIndex;		
//		url=url+"&classIndex="+classIndex;
//		url=url+"&studentrangeIndex="+studentrangeIndex;		
//		url=url+"&studentIndex="+studentIndex;
//		url=url+"&group="+group;
//		alert(url);
////		xmlHttp.onreadystatechange = function() {
////			stateChanged(xmlHttp, callingID);
////		}
//		xmlHttp.onreadystatechange = stateChanged;
//		xmlHttp.open("GET",url,true);
//		xmlHttp.send(null);
//	} else if( id=='studentrange'){
//		var st = document.getElementById("studentTable");
//		st.style.display = "block";
//		var st2 = document.getElementById("studentTable2");
//		st2.style.display = "block";
//	}	
//}	
//
//function stateChanged()
//{
//	if (xmlHttp.readyState==4){
//		//var popup = document.getElementById("sub_chart");
//		//while(popup.hasChildNodes()){
//		//	popup.removeChild(popup.firstChild);
//		//}
//		//popup.appendChild(xmlHttp.responseText);
//		//popup.innerHTML = xmlHttp.responseText;
//		
//		//data = xmlHttp.responseText;
//
//		// When one tries to get the text value from a node using FireFox (Windows); the result value is limited to 4096 characters.
//		// Normalize to resolve this problem
//		xmlHttp.responseXML.normalize();
//		///////////////////////////////////
//		
//		var xmlDoc=xmlHttp.responseXML.documentElement;
//		data = xmlDoc.getElementsByTagName("data")[0].childNodes[0].nodeValue;
//		
//		studentListTable = xmlDoc.getElementsByTagName("studentListTable")[0].childNodes[0].nodeValue;
//		studentListTable2 = xmlDoc.getElementsByTagName("studentListTable2")[0].childNodes[0].nodeValue;
//		//alert(studentListTable.length);
//		//alert("This is the size of listtable =>" + studentListTable.length);
//		//alert(xmlDoc.getElementsByTagName("studentListTable")[0].childNodes[0].nodeValue);
//		alert(data);
//		
//		
//		if (callingID=='all') {
//			var sub_sub = document.getElementById("sub_sub_chart");
//			sub_sub.style.display = "none";		
//			var st = document.getElementById("studentTable");
//			st.style.display = "none";
//			var st2 = document.getElementById("studentTable2");
//			st2.style.display = 'none';
//			swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "my_chart", "550", "350", "9.0.0");	
//		} else if(callingID=='level'){
//			var sub_sub = document.getElementById("sub_sub_chart");
//			sub_sub.style.display = "none";		
//			var st = document.getElementById("studentTable");
//			st.style.display = "none";
//			var st2 = document.getElementById("studentTable2");
//			st2.style.display = 'none';			
//			swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "sub_chart", "550", "350", "9.0.0");
//		} else if(callingID=='class'){
//			var sub_sub = document.getElementById("sub_sub_chart");
//			sub_sub.style.display = "block";	
//			var st = document.getElementById("studentTable");
//			st.style.display = "none";
//			var st2 = document.getElementById("studentTable2");
//			st2.style.display = 'none';				
//			swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "sub_sub_chart", "550", "350", "9.0.0");
//		} else if (callingID=='studentrange') {
//			if (studentListTable != "NULL") {
//				var st = document.getElementById("studentTable");
//				st.style.display = '';
//				var st2 = document.getElementById("studentTable2");
//				st2.style.display = '';
//				st.innerHTML = studentListTable;
//				st2.innerHTML = studentListTable2;
//			}
//		}
//	}
//	
//}

var callingID = '';
//var levelIndex = -1;
//var classIndex = -1;
//var studentrangeIndex = -1;
//var studentIndex = -1;
var allIndex = <?= is_numeric($allIndex) ? $allIndex : -1 ?>;
var levelIndex = <?= is_numeric($levelIndex) ? $levelIndex : -1 ?>;
var classIndex = <?= is_numeric($classIndex) ? $classIndex : -1 ?>;
var studentrangeIndex = <?= is_numeric($studentrangeIndex) ? $studentrangeIndex : -1 ?>;
var studentIndex = <?= is_numeric($studentIndex) ? $studentIndex : -1 ?>;


//#################### chart #####################################################################

</SCRIPT>
<FORM method='POST' name='form1' action='index_stat.php'>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
		
		<?
		echo $li_pf->GET_TAB_MENU($TabMenuArr);
		?>
			
			<table width="96%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td>
												<?=$YearStr?>
												<?=$ClassLevelStr?>
												<?=$ClassStr?>
											</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
										<td>
										<?=$category_selection_html?>
										<?=$ELEStr?>
										</td>
										<td>&nbsp;</td>
										</tr>
									</table>
								</td>
<!--
								<td align="right" valign="bottom">
									<span class="tabletext">
										<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$ec_iPortfolio['enter_student_name']:intranet_htmlspecialchars(stripslashes($search_name)))?>" onFocus="if(this.value=='<?=$ec_iPortfolio['enter_student_name']?>'){this.value=''}" onBlur="if(this.value==''){this.value='<?=$ec_iPortfolio['enter_student_name']?>'}" />
										<input type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onClick="jSUBMIT_SEARCH()" />
									</span>
								</td>
-->
							</tr>
						</table>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<!--
				<tr>
					<td>
						Call back the visibility: category id: <span id="testHead"></span> - visible? : <span id="testContent"></span>
					<td>
				</tr>
				<tr>
					<td>
						Call pop-up to generate sub-level bar chart: index? <span id="testPopup"></span>
					<td>
				</tr>
				-->				
				<tr>
					<td>
						<div id="my_chart">no flash?</div>
					</td>
				</tr>
				<!--
				<tr>
					<td><table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr>
						<td width="600">
							<div id="sub_chart"></div>
						</td>
						<td width="350">
							<div id="sub_sub_chart"></div>
						</td>
					</tr></table></td>
				</tr>
				-->
				<tr>
					
					<td>
						<div id="sub_chart"><!--<a id="sub"></a>--></div>
					</td>
				</tr>
				<tr>
					<td>
						<div id="sub_sub_chart"><!--<a id="sub_sub"></a>--></div>
					</td>
				</tr>								

				<tr>

				</tr>
				<tr><td>&nbsp;</td></tr>								
				<tr id="studentTable" style="display:none;" >
					<td>
						<table width="100%" border="0" cellpadding="4" cellspacing="0" bgcolor="#CCCCCC">
						<tbody>
						<tr class="tabletop">
						
							<td rowspan="2" class="tabletoplink">#</td>
						
							<td rowspan="2" ><a href="javascript:jSORT_TABLE(0, 1)" class="tabletoplink" onMouseOver="MM_swapImage('sort0','','/images/2009a/icon_sort_a_on.gif',1)" onMouseOut="MM_swapImgRestore()"> <?php echo '姓名'; ?> </a></td>
						
							<td width="50" rowspan="2" ><a href="javascript:jSORT_TABLE(1, 1)" class="tabletoplink" onMouseOver="MM_swapImage('sort1','','/images/2009a/icon_sort_a_on.gif',1)" onMouseOut="MM_swapImgRestore()"><?php echo '班別'; ?> </a></td>
						
							<td width="70" rowspan="2" ><a href="javascript:jSORT_TABLE(2, '1|-1')" class="tabletoplink" onMouseOver="MM_swapImage('sort2','','/images/2009a/icon_sort_a_on.gif',1)" onMouseOut="MM_swapImgRestore()"><?php echo '班號'; ?> <img src="/images/2009a/icon_sort_a_off.gif" border="0" width="13" height="13" name="sort2" id="sort2" valign="absbottom" /></a></td>
						
							<td width="80" align="center" rowspan="2" class="tablebluetop seperator_left"><a href="javascript:jSORT_TABLE(3, 1)" class="tabletoplink" onMouseOver="MM_swapImage('sort3','','/images/2009a/icon_sort_a_on.gif',1)" onMouseOut="MM_swapImgRestore()"><?php echo '項目總數'; ?> </a></td>
						
							<td width="80" align="center" rowspan="2" class="tablebluetop seperator_right"><a href="javascript:jSORT_TABLE(4, 1)" class="tabletoplink" onMouseOver="MM_swapImage('sort4','','/images/2009a/icon_sort_a_on.gif',1)" onMouseOut="MM_swapImgRestore()"><?php echo '總時數'; ?> </a></td>
						
							<td colspan="8" align="center"  class="tablegreentop tabletopnolink" style="border-bottom:1px solid #FFFFFF"><?php echo '其他學習經歷種類'; ?></td>
						
						</tr>
						
						<tr class="tabletop">
							<td width="50" align="center"  class="tablegreentop tabletopnolink"><?php echo '宗教倫理'; ?></td>
							<td width="50" align="center"  class="tablegreentop tabletopnolink"><?php echo '智育發展'; ?></td>
							<td width="50" align="center"  class="tablegreentop tabletopnolink"><?php echo '德育及公民教育'; ?></td>
							<td width="50" align="center"  class="tablegreentop tabletopnolink"><?php echo '社會服務'; ?></td>
							<td width="50" align="center"  class="tablegreentop tabletopnolink"><?php echo '體育發展'; ?></td>
							<td width="50" align="center"  class="tablegreentop tabletopnolink"><?php echo '藝術發展'; ?></td>
							<td width="50" align="center"  class="tablegreentop tabletopnolink"><?php echo '與工作有關經驗'; ?></td>
							<td width="50" align="center"  class="tablegreentop tabletopnolink"><?php echo '其他'; ?></td>
						</tr>
						
						<tr class="tablerow1">
						<td width="20" valign="top" ><span class="tabletext">1</span></td>
						<td valign="top" nowrap><a href="javascript:newWindow('../year_details.php?StudentID=101771', 94)" class="tablelink"><?php echo '陳俊隆'; ?></a></td>
						<td align="center" valign="top" class="tabletext">5A</td>
						<td align="center" valign="top" ><span class="tabletext">2</span></td>
						<td align="center" class="tabletext tablerow_total seperator_left"><strong>31</strong></td>
						<td align="center" class="tabletext tablerow_total seperator_right"><strong>248</strong></td>
						<td width="50" align="center"  class="tabletext">26</td>
						<td width="50" align="center"  class="tabletext">38</td>
						<td width="50" align="center"  class="tabletext">34</td>
						<td width="50" align="center"  class="tabletext">59</td>
						<td width="50" align="center"  class="tabletext">24</td>
						<td width="50" align="center"  class="tabletext">28</td>
						<td width="50" align="center"  class="tabletext">15</td>
						<td width="50" align="center"  class="tabletext">24</td>
						</tr>
						
						<tr class="tablerow1">
						<td width="20" valign="top" ><span class="tabletext">2</span></td>
						<td valign="top" nowrap><a href="javascript:newWindow('../year_details.php?StudentID=101771', 94)" class="tablelink"><?php echo '何淇淇'; ?></a></td>
						<td align="center" valign="top" class="tabletext">5A</td>
						<td align="center" valign="top" ><span class="tabletext">11</span></td>
						<td align="center" class="tabletext tablerow_total seperator_left"><strong>25</strong></td>
						<td align="center" class="tabletext tablerow_total seperator_right"><strong>251</strong></td>
						<td width="50" align="center"  class="tabletext">24</td>
						<td width="50" align="center"  class="tabletext">42</td>
						<td width="50" align="center"  class="tabletext">27</td>
						<td width="50" align="center"  class="tabletext">49</td>
						<td width="50" align="center"  class="tabletext">36</td>
						<td width="50" align="center"  class="tabletext">41</td>
						<td width="50" align="center"  class="tabletext">21</td>
						<td width="50" align="center"  class="tabletext">11</td>
						</tr>
						
						</tbody>
						</table>

					</td>
				</tr>
				<tr id="studentTable2" style="display:none;" class="tablebottom">
					<?/*<td>
					
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td class="tabletext"><?=$ec_iPortfolio['record']?> <?=$li_pf->GEN_PAGE_ROW_NUMBER(count($olr_list), $PageDivision, $Page)?>, <?=str_replace("<!--NoRecord-->", count($olr_list), $ec_iPortfolio['total_record'])?></td>
								<td align="right">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
												<table border="0" cellspacing="0" cellpadding="2">
													<tr align="center" valign="middle">
														<td><a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('prevp22','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(-1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_off.gif" name="prevp22" width="11" height="10" border="0" align="absmiddle" id="prevp22"></a> <span class="tabletext"> <?=$list_page?> </span></td>
														<td class="tabletext">
															<?=$li_pf->GEN_PAGE_SELECTION(count($olr_list), $PageDivision, $Page, "name='Page' class='formtextbox' onChange='jCHANGE_FIELD(\"page\")'")?>
														</td>
														<td><span class="tabletext"> </span><a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('nextp22','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_off.gif" name="nextp22" width="11" height="10" border="0" align="absmiddle" id="nextp22"></a></td>
													</tr>
												</table>
											</td>
											<td>&nbsp;<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
											<td>
												<table border="0" cellpadding="2" cellspacing="0" class="tabletext">
													<tr>
														<td><?=$i_general_EachDisplay?></td>
														<td>
															<?=$li_pf->GEN_PAGE_DIVISION_SELECTION($PageDivision, "name='PageDivision' class='formtextbox' onChange='jCHANGE_FIELD(\"division\")'", 10)?>
														</td>
														<td><?=$i_general_PerPage?></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
						*/?>
				</tr>				
			</table>
			<br>
		</td>
	</tr>
</table>

<input type="hidden" name="allIndex" />
<input type="hidden" name="levelIndex" />
<input type="hidden" name="classIndex" />
<input type="hidden" name="studentrangeIndex" />
<input type="hidden" name="studentIndex" />

<input type="hidden" name="FieldChanged" />
<input type="hidden" name="Field" value="<?=$Field?>" />
<input type="hidden" name="Order" value="<?=$Order?>" />
</form>
<script language = "javascript">
expandSubLevel('all',-1,-1); // default level, show statistics in different forms
if (levelIndex != -1) {
	expandSubLevel('level', levelIndex, -1);
}
if (classIndex != -1) {
	expandSubLevel('class', classIndex, -1);
}
if (studentrangeIndex != -1) {
	expandSubLevel('studentrange', studentrangeIndex, -1);
}
if (studentIndex != -1) {
	expandSubLevel('student', studentIndex, -1);
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
