<?php

# Modifing by Bill

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_slp();
$li_pf->ACCESS_CONTROL("ole");

$linterface = new interface_html("popup.html");
$CurrentPage = "ole_report_stat";
$title = $ec_iPortfolio['ole_report_stat'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

if($Field == "")
{
	$Field = 0;
	$Order = -1;
}

# Generate OLE record table grouped by year
$olr_display = $li_pf->GEN_OLE_YEAR_RECORD_LIST($StudentID, $Field, $Order);

# Generate student selection drop-down list and get a student list
list($StudentSelection, $student_list) = $li_pf->GEN_STUDENT_LIST_INFO("", $StudentID, true);

# Retrieve student info
$student_obj = $li_pf->GET_STUDENT_OBJECT($StudentID);
	
# Set links for photo
if(strpos($student_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $student_obj['PhotoLink'] != "")
	$student_obj['PhotoLink'] = str_replace("<!--ImageStyle-->", "", $student_obj['PhotoLink']);
else if($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
else
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
?>

<SCRIPT LANGUAGE="Javascript">
// Quick student switch 
function jCHANGE_STUDENT(jParShift, jParToExtreme)
{
	var StudentSelect = document.getElementsByName('StudentID');
	var OriginalIndex = StudentSelect[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(!jParToExtreme)
	{
		if(TargetIndex >= StudentSelect[0].length)
		{
			StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			return;
		}
		else if(TargetIndex < 0)
		{
			StudentSelect[0].selectedIndex = 0;
			return;
		}
		else
			StudentSelect[0].selectedIndex = TargetIndex;
	}
	else
	{
		if(jParShift < 0)
		{
			if(OriginalIndex > 0)
				StudentSelect[0].selectedIndex = 0;
			else
				return;
		}
		else
		{
			if(OriginalIndex < StudentSelect[0].length-1)
				StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			else
				return;
		}
	}
	
	document.form1.action = "year_details.php";
	document.form1.submit();
}

// Sort table by field
function jSORT_TABLE(jParField, jParOrder)
{
	document.form1.Field.value = jParField;
	document.form1.Order.value = jParOrder;
	
	document.form1.action = "year_details.php";
	document.form1.submit();	
}
</SCRIPT>

<?php
//Customization for St.Paul, a mouseover popup box showing the awards detail
if($sys_custom['iPf']['StPaulCoeduPriSchool']['Report']['StudentLearningProfile']){
?>
<SCRIPT>
$(document).ready(function(){
	$('.ELEtableCell').hover(
		function(e){
			if($(this).attr('activityDetails')){
				$('#ActivityLayer').html('<table style="box-shadow: 5px 5px 2px #D8B164; background:#C93; color:#FFF; opacity:0.95; filter:alpha(opacity=95); font-weight:bold"><tr><td></td><td>Title</td><td>Awards</td></tr>'+$(this).attr('activityDetails')+'</table>');
				$('#ActivityLayer').css({ "top": (e.pageY+10)+'px', "left": (e.pageX+10)+'px' });
				$('#ActivityLayer').show();
			}
		},
		function(){
			$('#ActivityLayer').hide();
		}
	)
});
</SCRIPT>
<?php
}
?>
<FORM method='POST' name='form1' action='year_details.php'>
<div id='ActivityLayer' style='position:absolute;display:none'></div>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="center">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td align="left">&nbsp;</td>
	<td align="right">
		<table border="0" cellspacing="0" cellpadding="2">
		<tr align="center" valign="middle">
		<td width="15" nowrap><span class="tabletext"> </span><a href="#" class="tablebottomlink" onClick="jCHANGE_STUDENT(-1, true)">&lt;&lt;</a></td>
		<td width="15" nowrap><span class="tabletext"> </span><a href="#" class="tablebottomlink" onClick="jCHANGE_STUDENT(-1, false)">&lt;</a></td>
		<td nowrap>
		<span class="tabletext">
		<?=$StudentSelection?>
		</span>
		</td>
		<td width="15" nowrap><span class="tabletext"> </span><a href="#" class="tablebottomlink" onClick="jCHANGE_STUDENT(1, false)">&gt;</a></td>
		<td width="15" nowrap><span class="tabletext"> </span><a href="#" class="tablebottomlink" onClick="jCHANGE_STUDENT(1, true)">&gt;&gt;</a></td>
		<td width="15" nowrap> | </td>
		<td nowrap class="tabletext"><?=str_replace("<!--NoRecord-->", count($student_list), $ec_iPortfolio['total_record'])?></td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td width="150" align="center" valign="top">
		<table border="0" cellspacing="0" cellpadding="2">
		<tr>
		<td align="center">
			<table border="0" cellspacing="0" cellpadding="0" >
			<tr>
			<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_01.gif" style="padding-left:10px; padding-top:10px;"><?=$student_obj['PhotoLink']?></td>
			<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="10" height="10"></td>
			</tr>
			<tr>
			<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
			<td height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_04.gif" width="10" height="10"></td>
			</tr>
			</table>
		</td>
		</tr>
		<tr>
		<td align="center" class="tabletext"><?=$intranet_session_language=="b5"?$student_obj['ChineseName']:$student_obj['EnglishName']?><br /><?=$student_obj['ClassName']?>-<?=$student_obj['ClassNumber']?></td>
		</tr>
		</table>
	</td>
	<td valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td><?=$olr_display?></td>
			</tr>
<?php if($special_feature['ole_chart']) { ?>
			<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/swfobject.js"></script>
			<tr>
				<td>
					<br />
					<div id="my_chart">no flash?</div>
					<script type="text/javascript">
						swfobject.embedSWF(
							"<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "my_chart", "600", "300",
							"9.0.0", "<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/expressInstall.swf",
							{"data-file":"demo/stu_year_acc.php"}
						);
						window.resizeTo(800,720);
					</script>
				</td>
			</tr>
<?php } ?>
			</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
</td>
</tr>
</table>

<input type="hidden" name="Field" value="<?=$Field?>" />
<input type="hidden" name="Order" value="<?=$Order?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
