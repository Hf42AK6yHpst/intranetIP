<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$linterface = new interface_html("popup.html");
$lpf = new libpf_slp();

$lpf->CHECK_ACCESS_IPORTFOLIO();
$lpf->ACCESS_CONTROL("ole");

$Year = ($Year=="") ? Get_Current_Academic_Year_ID() : $Year;
# academic year selection
$academic_year_arr = $lpf->getGraduationYearList();

$year_selection_html = getSelectByAssoArray($academic_year_arr, "name='Year' onChange='this.form.year_change.value=1;this.form.action=\"data_export_alumni.php\";this.form.submit();'", $Year, 0, 0, "--".$ec_iPortfolio['please_select_graduation_year']."--");

# use 0/'' for all records
# use 1 for int records
# use 2 for ext records
$intextArray[] = array(1, $iPort["internal_record"]);
$intextArray[] = array(2, $iPort["external_record"]);
$intext_selection_html = getSelectByArray($intextArray, "name='IntExt' onChange='this.form.action=\"data_export_alumni.php\";this.form.submit();'", $IntExt, 1, 0, "", 2);
$record_num = $lpf->countOLEDataNumberAlumni($Year, $IntExt);

$CurrentPage = "ole";
$title = $ec_iPortfolio['ole'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

?>

<script language="">

window.resizeTo(800, 700)
window.moveTo(0,0);

// check if year and semester are both available
function checkform(frmObj){
	if (typeof(frmObj.Year)=="undefined")
	{
		alert("<?=$ec_warning['eclass_data_no_record']?>");
		return false;
	}
	if ($('input[name="exportFormat"]:checked').val()=='dgs'){
		if($('input[name="alumni_StudentID[]"]:checked').length==0){
			alert("<?=$ec_warning['select_student']?>");
			return false;
		}	
	}
	return true;
}

function checkAll(isChecked){	
	value = isChecked? true: false;
	
	document.form1.WebSAMSRegNo.checked = value;	
	document.form1.Programme.checked = value;
	document.form1.ProgrammeDescription.checked = value;
	document.form1.SchoolYearFrom.checked = value;
	document.form1.SchoolYearTo.checked = value;
	document.form1.Role.checked = value;
	document.form1.Organization.checked = value;
	document.form1.Awards.checked = value;
	document.form1.Student.checked = value;
	document.form1.ClassName.checked = value;
	document.form1.ClassNumber.checked = value;
	document.form1.Period.checked = value;
	document.form1.Category.checked = value;
	document.form1.SubCategory.checked = value;
	document.form1.Hours.checked = value;				
	document.form1.ApprovedBy.checked = value;
	document.form1.PreferredApprover.checked = value;
	document.form1.Status.checked = value;
	document.form1.ApprovedDate.checked = value;
	document.form1.Remarks.checked = value;
	document.form1.ELE.checked = value;
	document.form1.Comment.checked = value;
	document.form1.intExtType.checked = value;
	document.form1.SLPOrder.checked = value;
	<? if ($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) { ?>
		document.form1.IsSAS.checked = value;
	<? } ?>
}

function checkItem(isChecked){
	if(!isChecked){
		document.form1.CheckAll.checked = false;
	}	
}


function dgsReport(isSelected){
	if(isSelected){
		$('#graduationTR').css('display','none');
		$('#recordTypeTR').css('display','none');
		$('#numofRecordTR').css('display','none');
		$('#studentSearch').css('display','');
		$('#exportAlumni').val(1);
		changeSelectReportTarget(2);
	}else{
		$('#graduationTR').css('display','');
		$('#recordTypeTR').css('display','');
		$('#numofRecordTR').css('display','');
		$('#studentSearch').css('display','none');
		$('#exportAlumni').val(0);
		changeSelectReportTarget(1);
	}
}

function changeSelectReportTarget(reportTargetType){
 /*reportTargetType = 1  , print for current student
  *reportTargetType = 2  , print for alumni student
 */
	if(reportTargetType == 1){
		$("#alumni_div").html(''); //reset HTML table , result set of "ALUMNI STUDENT"
		$("#r_alumni_studentName").val('');
		$("#r_alumni_studentWebsams").val('');

	}else{
	}

}

function searchForAlumni(){
	var s_name = $("#r_alumni_studentName").val();
	var s_websams = $("#r_alumni_studentWebsams").val();
	
	if(s_name == '' && s_websams == ''){
		alert('<?= $iPort["msg"]["InputOneItem"]?>');
		$('#r_alumni_studentName').focus();
		return false;
	}

	$.ajax({
    url:      "searchAlumniStudent.php",
    type:     "POST",
    async:    true,
    data:     "r_alumni_studentName="+s_name+"&r_alumni_studentWebsams="+s_websams,
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){
					$("#alumni_div").html(data);
              }
	});
}

function checkAllAlumniCheckBox (flag){
   if (flag == true){
      $("input[id^='alumni_studentArr_']").attr('checked', true);
   }
   else{
      $("input[id^='alumni_studentArr_']").attr('checked', false);
   }
	return;
}

function clickedReset() {
	$('input#CheckAll').attr('disabled', '');
	$('input.ipf_export_field').attr('disabled', '');
}
</script>

<?php $disabled = ($semester_selection_html=="<i>".$no_record_msg."</i>") ? "DISABLED": ""; ?>

<FORM action="data_export_alumni_update.php" method="POST" name="form1" onSubmit="return checkform(this)">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
      		<td height="10" colspan="2">&nbsp;</td>
        </tr>
        <tr id="graduationTR">
      		<td align="right" class="tabletext"><?=$ec_iPortfolio['graduation_year']?>：</td><td class="tabletext"><?= $year_selection_html ?><span style="float: right"><a href="data_export.php"><?=$button_back?></a></span></td>
        </tr>
		<tr id="recordTypeTR">
			<td align="right" class="tabletext"><?=$ec_iPortfolio['record_type']?>：</td><td class="tabletext"><?= $intext_selection_html?></td>
		</tr>		       
	 	<tr id="numofRecordTR">
      		<td align="right" class="tabletext"><?=$ec_iPortfolio['number_of_record']?>：</td><td class="tabletext"><?= $record_num?></td>
        </tr>
        <tr id="studentSearch" style="display:none">
        	<td align="right" class="tabletext"><?=$i_identity_student?> : </td>
        	<td class="tabletext">
        		<div id="targetForAlumni">
					<table border="0">
						<tr>
							<td><input type="text" name="r_alumni_studentName" id="r_alumni_studentName"></td>
							<td><?=$ec_iPortfolio['heading']['student_regno']?> : </td><td><input type="text" name="r_alumni_studentWebsams" id="r_alumni_studentWebsams"></td>
							<td><input type="button" onClick="searchForAlumni()" value="<?=$Lang['Btn']['Search']?>"></td>
							<td><span style="float: right"><a href="data_export.php"><?=$button_back?></a></span></td>
						</tr>
					</table>
				<div id="alumni_div"></div>
				</div>
				
			</td>
        </tr>	
        <?php
        	//if($sys_custom['iPf']['Report']['OLE']['WebSAMSReport']||$sys_custom['iPf']['Report']['OLE']['DGS_Custom_Report']) { 
        	if($sys_custom['iPf']['Report']['OLE']['DGS_Custom_Report']) { ?>	       
	 	<tr>
      		<td align="right" class="tabletext"><?=$ec_iPortfolio['Export_Format']?>：</td>
      		<td class="tabletext">
      		<label><input type="radio" name="exportFormat" onclick="dgsReport(0);$('input[name=\'CheckAll\']').removeAttr('disabled');$('.ipf_export_field').removeAttr('disabled'); form1.action='data_export_alumni_update.php';" checked><?=$ec_iPortfolio['Export_Custom_Field']?></label>
      		&nbsp;&nbsp;&nbsp;&nbsp;
      		<?php /*if($sys_custom['iPf']['Report']['OLE']['WebSAMSReport']) { ?>
      		<label><input type="radio" name="exportFormat" onclick="dgsReport(0);$('input[name=\'CheckAll\']').attr('disabled','true');$('.ipf_export_field').attr('disabled','true'); form1.action='data_export_websams_update.php';"><?=$ec_iPortfolio['Export_WebSAMS_Format']?></label>
      		&nbsp;&nbsp;&nbsp;&nbsp;
      		<?php } */?>
      		<?php if($sys_custom['iPf']['Report']['OLE']['DGS_Custom_Report']) { ?>
      		<label><input value="dgs" type="radio" name="exportFormat" onclick="dgsReport(1);$('input[name=\'CheckAll\']').attr('disabled','true');$('.ipf_export_field').attr('disabled','true'); form1.action='data_export_dgs_update.php';"><?=$ec_iPortfolio['Export_Custom_Format']?></label>
      		&nbsp;&nbsp;&nbsp;&nbsp;
      		<?php } ?>
      		</td>
        </tr> 	
        <?php } ?>	     
        <tr>
        	<td align="right" class="tabletext" valign="top"><?=$ec_iPortfolio['export']['SelectFields']?>：</td>
        	<td class="tabletext">
        		<input type="checkbox" id="CheckAll" name="CheckAll" onclick="checkAll(this.checked)"/><label for="CheckAll"><?=$ec_iPortfolio['export']['CheckAll']?></label> <br>
        		<hr>
        		<input type="checkbox" class="ipf_export_field" id="WebSAMSRegNo" name="WebSAMSRegNo" checked="true" onclick="checkItem(this.checked)"/><label for="WebSAMSRegNo"><?=$ec_iPortfolio['export']['WebSAMSRegNo']?></label> <br>      	        	
				<input type="checkbox" class="ipf_export_field" id="Programme" name="Programme" checked="true" onclick="checkItem(this.checked)"/><label for="Programme"><?=$ec_iPortfolio['export']['Programme']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="ProgrammeDescription" name="ProgrammeDescription" checked="true" onclick="checkItem(this.checked)"/><label for="ProgrammeDescription"><?=$ec_iPortfolio['export']['ProgrammeDescription']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="SchoolYearFrom" name="SchoolYearFrom" checked="true" onclick="checkItem(this.checked)"/><label for="SchoolYearFrom"><?=$ec_iPortfolio['export']['SchoolYearFrom']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="SchoolYearTo" name="SchoolYearTo" checked="true" onclick="checkItem(this.checked)"/><label for="SchoolYearTo"><?=$ec_iPortfolio['export']['SchoolYearTo']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="Role" name="Role" checked="true" onclick="checkItem(this.checked)"/><label for="Role"><?=$ec_iPortfolio['export']['Role']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="Organization" name="Organization" checked="true" onclick="checkItem(this.checked)"/><label for="Organization"><?=$ec_iPortfolio['export']['Organization']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="Awards" name="Awards" checked="true" onclick="checkItem(this.checked)"/><label for="Awards"><?=$ec_iPortfolio['export']['Awards']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="Student" name="Student" onclick="checkItem(this.checked)"/><label for="Student"><?=$ec_iPortfolio['export']['Student']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="ClassName" name="ClassName" onclick="checkItem(this.checked)"/><label for="ClassName"><?=$ec_iPortfolio['export']['ClassName']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="ClassNumber" name="ClassNumber" onclick="checkItem(this.checked)"/><label for="ClassNumber"><?=$ec_iPortfolio['export']['ClassNumber']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="Period" name="Period" onclick="checkItem(this.checked)"/><label for="Period"><?=$ec_iPortfolio['export']['Period']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="Category" name="Category" onclick="checkItem(this.checked)"/><label for="Category"><?=$ec_iPortfolio['export']['Category']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="SubCategory" name="SubCategory" onclick="checkItem(this.checked)"/><label for="SubCategory"><?=$ec_iPortfolio['sub_category']?></label><br/>
				<input type="checkbox" class="ipf_export_field" id="Hours" name="Hours" onclick="checkItem(this.checked)"/><label for="Hours"><?=$ec_iPortfolio['export']['Hours']?></label><br>				
				<input type="checkbox" class="ipf_export_field" id="ApprovedBy" name="ApprovedBy" onclick="checkItem(this.checked)"/><label for="ApprovedBy"><?=$ec_iPortfolio['export']['ApprovedBy']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="PreferredApprover" name="PreferredApprover" onclick="checkItem(this.checked)"/><label for="PreferredApprover"><?=$Lang['iPortfolio']['preferred_approver']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="Status" name="Status" onclick="checkItem(this.checked)"/><label for="Status"><?=$ec_iPortfolio['export']['Status']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="ApprovedDate" name="ApprovedDate" onclick="checkItem(this.checked)"/><label for="ApprovedDate"><?=$ec_iPortfolio['export']['ApprovedDate']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="Remarks" name="Remarks" onclick="checkItem(this.checked)"/><label for="Remarks"><?=$ec_iPortfolio['export']['Remarks']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="ELE" name="ELE" onclick="checkItem(this.checked)"/><label for="ELE"><?=$ec_iPortfolio['export']['ELE']?></label><br/>
				<input type="checkbox" class="ipf_export_field" id="Comment" name="Comment" onclick="checkItem(this.checked)"/><label for="Comment"><?=$ec_iPortfolio['export']['Comment']?></label><br/>
				<input type="checkbox" class="ipf_export_field" id="intExtType" name="intExtType" onclick="checkItem(this.checked)"/><label for="intExtType"><?=$ec_iPortfolio['record_type']?></label><br/>
				<input type="checkbox" class="ipf_export_field" id="SLPOrder" name="SLPOrder" onclick="checkItem(this.checked)"/><label for="SLPOrder"><?=$ec_iPortfolio['SLPOrder']?></label><br>
				<? if ($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) { ?>
					<input type="checkbox" class="ipf_export_field" id="IsSAS" name="IsSAS" onclick="checkItem(this.checked)"/><label for="IsSAS"><?=$Lang['iPortfolio']['OLE']['SAS']?></label><br>
				<? } ?>
        	</td>
        </tr>
      </table>

	<br>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>&nbsp;</td>
		<td align="right">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="submit" value="<?=$button_export?>" name="btn_export">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="reset" value="<?=$button_reset?>" onclick="clickedReset();">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="button" value="<?=$button_cancel?>" onClick="self.close()">
		  </td>
	</tr>
	</table>
	<input type="hidden" name="exportAlumni" id="exportAlumni" value="0"/>
	<input type="hidden" name="year_change" />
	<input type="hidden" name="sem_change" />
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
