<?php
// Modifing by:
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Profile:ExportData"));
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_opendb();
$lpf = new libpf_slp();
$lpf->CHECK_ACCESS_IPORTFOLIO();
$lexport = new libexporttext();

$usersPerLoop = 50;
$startIndex = 0;

# print the report title				
$year_name = "_".getAYNameByAyId($Year);
$filename = "olr_report_custom_".$year_name.".csv";
$isXLS = false;
$ExportContent = '"'.$ec_iPortfolio['export_DGS']['EnglishName'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['ChineseName'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['DOB'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['AdmissionDate'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['HKID'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['WebSAMSRegNo'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['AwardName'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['SchoolYear'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['ClassName'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['ClassNo'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['ProgrammeName'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['AcademicYear'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['Role'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['Achievement'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['Category'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['Organization'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['ProgrammeName'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['AcademicYear'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['Role'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['ComponentofOLE'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['Achievement'].'"'."\t";
$ExportContent .= '"'.$ec_iPortfolio['export_DGS']['Organization'].'"'."\t";
$ExportContent .= "\n";

$lexport->EXPORT_FILE($filename, $ExportContent, $isXLS, $ignoreLength=true, "UTF-8");	
if($exportAlumni){
	$StudentID = $alumni_StudentID;
	$Year = '';
}

while(true){
	$UserIDAry = $lpf->returnOLEnAWARDUserID($startIndex, $usersPerLoop, $studentType, $Year, $StudentID, $exportAlumni);

	if($startIndex==0&&$UserIDAry==0){
		print(mb_convert_encoding('no','UTF-16LE','UTF-8'));
		break;
	}	
	$DataArray = $lpf->returnStudentOLEExportData_DGS($Year, $IntExt, $UserIDAry);
	if(count($DataArray) != 0 or $startIndex == 0){	
		$ExportContent = $lpf->returnOLEExportData_DGS($Year, $IntExt, $_POST, $DataArray);
		$ExportContent = mb_convert_encoding($ExportContent,'UTF-16LE','UTF-8');
		print($ExportContent);
	}	
	$startIndex += $usersPerLoop;
	if(count($UserIDAry) != $usersPerLoop){
		break;
	}
}
intranet_closedb();
?>
