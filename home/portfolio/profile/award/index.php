<?php

// Modifing by 

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
//include_once("$admin_root_path/src/portfolio/new_header.php");

//Others:
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
//include_once("$eclass_filepath/src/includes/php/lib-table.php");
intranet_auth();
iportfolio_auth("TSP");
intranet_opendb();

# define the page title and table size

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPageName = $iPort['menu']['school_records'];

$lpf = new libpf_slp();
$luser = new libuser($UserID);
$StudentID = $luser->UserID;
###  FOR UCCKE, default current year if existed
$sql2 = "Select DISTINCT a.Year FROM {$eclass_db}.ACTIVITY_STUDENT as a WHERE a.UserID = '$StudentID' ORDER BY Year";
$ActivityYearArr = $lpf->returnArray($sql2);
// 2020-04-07 (Philips) Use YearName Arr for curr Year checking
$ActivityYearNameArr = Get_Array_By_Key($ActivityYearArr, 'Year');

if($sys_custom['ipf']['school_records']['uccke'] && !isset($_GET['ChooseYear'])){
	$currYear = getCurrentAcademicYear();
	// if current year not exist, use all year
	if(in_array($currYear, $ActivityYearNameArr)){
		$_GET['ChooseYear'] = getCurrentAcademicYear();
	}
}
switch($_SESSION['UserType'])
{
	case 2:
		$StudentID = $_SESSION['UserID'];
		$CurrentPage = "Student_SchoolRecords";
		break;
	case 3:
		$StudentID = $ck_current_children_id;
		$CurrentPage = "Parent_SchoolRecords";
		break;
	default:
		break;
}

////////////////////////////////////////////////////////
///// TABLE SQL
if ($order=="") $order=0;
if ($field=="") $field=0;
$li = new libdbtable2007($field, $order, $pageNo);
if($sys_custom['iPortfolioHideRemark']==true)
{
	$li->field_array = array("a.Year", "a.Semester", "a.AwardDate", "a.AwardName", "a.AwardFile", "a.ModifiedDate");
	$li->no_col = 7;
	$li->column_array = array(0,0,0,0,0,0);
	$NameWidth = "30%";
}
else
{
	$li->field_array = array("a.Year", "a.Semester", "a.AwardDate", "a.AwardName", "a.Details", "a.AwardFile", "a.ModifiedDate");
	$li->no_col = 8;
	$li->column_array = array(0,0,0,0,0,0,0);
	$RemarkField = "if(a.Remark!='', a.Remark, '--'),";
	$NameWidth = "15%";
}

$ChooseYear = $_GET['ChooseYear'];
if($ChooseYear!="")
{$conds = "AND a.Year = '$ChooseYear'";}

$sql = "SELECT
              a.Year,
              IF(a.Semester = '' , '".$Lang['General']['WholeYear']."',a.Semester) as  'Semester',
              IF (a.AwardDate,DATE_FORMAT(a.AwardDate,'%Y-%m-%d'),'--') As AwardDate,
              CONCAT('<a class=navigation href=\"javascript:newWindow(\'award_detail.php?record_id=', a.RecordID,'\', 4)\">', a.AwardName, '</a>'),
			  {$RemarkField}
              IF ((a.AwardFile!='' AND a.AwardFile IS NOT NULL), CONCAT('<a href=\"attach.php?RecordID=', a.RecordID, '\" target=\"_blank\"><img src=\"$image_path/icon/attachment.gif\" border=0></a>'), '--'),
              a.ModifiedDate
		FROM 
			{$eclass_db}.AWARD_STUDENT as a
        WHERE 
			a.UserID = '$StudentID'
			AND a.RecordType = '1'
			$conds
            ";

// TABLE INFO
$li->sql = $sql;
$li->db = $intranet_db;
$li->title = $ec_iPortfolio['award'];
$li->no_msg = $no_record_msg;
$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
//$li->noNumber = true;

$pageSizeChangeEnabled = true;

$li->table_tag = "<table width='100%' border='0' cellpadding='10' cellspacing='0'>";
//$li->row_alt = array("#FFFFFF", "F3F3F3");
$li->row_alt = array("", "");
$li->row_height = 20;
$li->sort_link_style = "class='tbheading'";


// TABLE COLUMN
$pos = 0;
//$li->column_list .= "<td class='tbheading' height='25' bgcolor='#CFE6FE' nowrap align='center'>#</span></td>\n";
$li->column_list .= "<td height='25' nowrap align='center'>".$li->column($pos++, "#", 1)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++,
$ec_iPortfolio['year'], 1)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $ec_iPortfolio['semester'], 1)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $ec_iPortfolio['date'], 1)."</td>\n";
$li->column_list .= "<td width='".$NameWidth."'>".$li->column($pos++, $ec_iPortfolio['award_name'], 1)."</td>\n";
if($sys_custom['iPortfolioHideRemark']==false) {
	$li->column_list .= "<td width='15%'>".$li->column($pos++, $ec_iPortfolio['remark'], 1)."</td>\n";
}
$li->column_list .= "<td width='10%'>".$li->column($pos++, $ec_iPortfolio['upload_cert'], 1)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $ec_iPortfolio['SAMS_last_update'], 1)."</td>\n";

$SelectBoxContent = $lpf->displayYearSelectBox($StudentID, $ClassName,$_GET['ChooseYear']);
//////////////////////////////////////////////

# define the page title and table size
//$template_width = "98%";
//$template_left_menu = getLeftMenu($menu_arr, $menu_arr[4][1]);
//echo getBodyBeginning($template_pages, $template_width, 1, "red", "", $template_left_menu, $template_table_top_right);

$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

// Tab Menu Settings
/*
$TabMenuArr = array();
if($lpf->access_config['merit'] == 1)
	$TabMenuArr[] = array("../merit/index.php", $ec_iPortfolio['title_merit'], 0);
if($lpf->access_config['assessment_report'] == 1)
	$TabMenuArr[] = array("../assessment/index.php", $ec_iPortfolio['title_academic_report'], 0);
if($lpf->access_config['activity'] == 1)
	$TabMenuArr[] = array("../activity/index.php", $ec_iPortfolio['title_activity'], 0);
if($lpf->access_config['award'] == 1)
	$TabMenuArr[] = array("../award/index.php", $ec_iPortfolio['title_award'], 1);
if($lpf->access_config['teacher_comment'] == 1)
	$TabMenuArr[] = array("../comment/index.php", $ec_iPortfolio['title_teacher_comments'], 0);
if($lpf->access_config['attendance'] == 1)
	$TabMenuArr[] = array("../attendance/index.php", $ec_iPortfolio['title_attendance'], 0);
if($lpf->access_config['service'] == 1)
	$TabMenuArr[] = array("../service/index.php", $ec_iPortfolio['service'], 0);
*/
$TabMenuArr = libpf_tabmenu::getSchoolRecordTags("award");
?>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>

<!-- ===================================== Body Contents ============================= -->

<FORM name="form1" method="GET">
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td valign="top">
        <table width="100%"  border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td>
              <?=$lpf->GET_TAB_MENU($TabMenuArr);?>
            </td>
          </tr> 
          <tr>
            <td>
              <table border="0" cellpadding="1" cellspacing="0" width="100%">
                <tbody>
                  <tr>
                    <td><?= $SelectBoxContent?></td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td align="center">
              <?= $li->displayPlain() ?>
              <table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
<?php if ($li->navigationHTML!="") { ?>
                <tr class='tablebottom'>
                  <td class="tabletext" align="right"><?=$li->navigation(1)?></td>
                </tr>
<?php } ?>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  
  <input type="hidden" name="ClassName" value="<?=$ClassName?>">
  <input type="hidden" name="StudentID" value="<?=$StudentID?>">
  <input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>">
  <input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
  <input type="hidden" name="order" value="<?php echo $li->order; ?>">
  <input type="hidden" name="field" value="<?php echo $li->field; ?>" />
  <input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
  <input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</FORM>

<!-- ===================================== Body Contents (END) ============================= -->

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
