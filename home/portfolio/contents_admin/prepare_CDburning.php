<?php

// Modifying by 

include_once('../../../system/settings/global.php');
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:CDBurning") || !strstr($ck_user_rights, ":web:"));
auth("T");
include_once("$admin_root_path/src/includes/php/lib-db.php");
include_once("$admin_root_path/src/includes/php/lib-portfolio.php");
include_once("$admin_root_path/src/includes/php/lib-notes.php");
include_once("$admin_root_path/src/includes/php/lib-notes_portfolio.php");
include_once("$admin_root_path/src/includes/php/lib-filesystem.php");
include_once("$admin_root_path/src/portfolio/new_header.php");

opendb();

$li_pf = new portfolio();

# define the navigation, page title and table size
$template_pages = Array(
					Array($ck_custom_title['web'], "main.php"),
					Array($ec_iPortfolio['prepare_cd_burning'], "prepare_CDburning_ClassList.php"),
					Array($ClassName, "")
				);

$template_width = "75%";
	
echo getBodyBeginning($template_pages, $template_width);

if($work_type!="")
{
	if($work_type==1)
	{
		$copy_path = $li_pf->copyPortfolioFiles($ClassName, $with_comment);
		$message = str_replace("XXX", $ClassName, $ec_iPortfolio['fished_cd_burning_preparation']);	
		$message .= "<br /><i>".$copy_path."</i>";
		$remind_msg = $ec_iPortfolio['get_prepared_portfolio'];
	}
	else
	{
		$x = $li_pf->removePortfolioFiles($ClassName);
		$message = ($x==1) ? str_replace("XXX", $ClassName, $ec_iPortfolio['copied_portfolio_reomved']) : str_replace("XXX", $ClassName, $ec_iPortfolio['copied_portfolio_reomved_fail']);
	}

	$result_display = "<br /><br />";
	$result_display .= "<table border='0' cellpadding='8' cellspacing='0' width='80%'>";
	$result_display .= "<tr><td class='chi_content_15'>".$message."</td></tr>\n";
	$result_display .= ($remind_msg!="") ? "<tr><td class='chi_content_15'>".$remind_msg."</td></tr>\n" : "";
	$result_display .= "<tr><td><br /></td></tr>\n";
	$result_display .= "<tr><td align='center'><input onClick='javascript:self.location.href=\"prepare_CDburning_ClassList.php\"' name='button' type=button value='".$ec_iPortfolio['prepare_other_class_cd_burning']."' class='button_g'>&nbsp;&nbsp;<input onClick='self.location.href=\"main.php\"' name='button' type=button value='".$button_finish."' class='button_g'></td></tr>\n";
	$result_display .= "</table>\n";
}
else
{
	$copy_menu = str_replace("XXX", $ClassName, $ec_iPortfolio['copy_web_portfolio_files']);
	$remove_menu = str_replace("XXX", $ClassName, $ec_iPortfolio['remove_copied_files']);

	$copied_file_exist = $li_pf->checkCopiedPortfolioFilesExist($ClassName);
	$delete_disable = ($copied_file_exist) ? "" : "DISABLED";

	$menu_table_html = "<br />
						<form name='form1' method='post'>
						<table width='100%' border='0' cellspacing='8' cellpadding='6'>
						  <tr>
							<td align='center' nowrap><input onClick='checkform(document.form1,1)' name='button' type='button' value='".$copy_menu."' class='button_g'>&nbsp;&nbsp;(<input type='checkbox' name='with_comment' id='comment_1' value=1><label for='comment_1'>".$ec_iPortfolio['with_comment']."</label>)</td>
						</tr><tr><td><br /></td></tr>";
	
	$menu_table_html .= ($copied_file_exist) ? "<tr>
									<td align='center'><input onClick='checkform(document.form1,2)' name='button' type='button' value='".$remove_menu."' class='button_g'></td>
								</tr>" : "";		
							
	$menu_table_html .= "</table>
						<input type='hidden' name='ClassName' value='".$ClassName."' />
						</form>
						";
}
?>


<script language="javascript">
function checkform(myObj,type){
	var copy_alert_sentence = "<?=$ec_warning['copy_portfolio']?>";
	var remove_alert_sentence = "<?=$ec_warning['remove_copied_portfolio']?>";

	alert_sentence = (type==1) ? copy_alert_sentence.replace("XXX", myObj.ClassName.value) : remove_alert_sentence.replace("XXX", myObj.ClassName.value);

	if(confirm(alert_sentence))
		form_submit(myObj,type);
}

function form_submit(myObj,type)
{
	myObj.action='prepare_CDburning.php?work_type='+type;
	myObj.submit();
}

</script>

<br>
<?php

if($work_type=="")
{
	echo $menu_table_html;
}
else
{
	echo $result_display;
}

?>


<?= getPageEnding() ?>

<?php
closedb();
include_once("$admin_root_path/src/portfolio/new_footer.php");
?>
