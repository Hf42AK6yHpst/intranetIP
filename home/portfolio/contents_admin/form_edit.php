<?php
include_once('../../../system/settings/global.php');
include_once('../../includes/php/lib-db.php');
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));
auth("T", $fs->HelperSet['crs_contents']);
opendb();
head(11);

$li = new libdb();
$sql = "SELECT answersheet, sheettype FROM assignment WHERE worktype=8 AND parent_id='$notes_id' ";
$row = $li->returnArray($sql);

$answersheet = $row[0]["answersheet"];
$sheettype = $row[0]["sheettype"];

?>

<form name="ansForm" method="post" action="../growth_admin/online_form/edit.php" >
	<input type="hidden" name="notes_id" value="<?=$notes_id?>">
	<input type="hidden" name="answersheet" value="<?=$answersheet?>">
	<input type="hidden" name="fieldname" value="answersheet">
	<input type="hidden" name="formname" value="ansForm">
	<input type="hidden" name="sheettype" value="<?=$sheettype?>">
</form>

<script language="">
window.onLoad = document.ansForm.submit();
</script>

</body>
</html>

<?php
closedb();
?>