<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));
intranet_auth();
intranet_opendb();

$li = new libdb();
// $ck_course_id = 1274; //edited temporarily because session not set
$li->db = classNamingDB($ck_course_id);

$sql = "UPDATE web_portfolio SET status='0' WHERE web_portfolio_id IN (".implode(",", $web_portfolio_id).")";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index_scheme.php?msg=2");
?>
