<?php
include_once('../../../system/settings/global.php');
include_once('../../includes/php/lib-db.php');
$EC_BL_ACCESS_HIGH = (!strstr($ck_user_rights, ":manage:") || !strstr($ck_user_rights, ":web:"));
auth("TA");
opendb();

$li = new libdb();

# set to private
$already_public = explode(",", $group_allowed_private);
for ($i=0; $i<sizeof($already_public); $i++)
{
	if ($gid=trim($already_public[$i]))
	{
		$sql = "UPDATE group_config
				SET
						notes_published=NULL, modified=now()
				WHERE
						group_id='$gid' ";
		$li->db_db_query($sql);
	}
}

for ($i=0; $i<sizeof($groups_id); $i++)
{
	$group_id = $groups_id[$i];
	# check if config exists
	$sql = "SELECT group_config_id FROM group_config WHERE group_id='$group_id' ";
	$row = $li->returnVector($sql);
	if (sizeof($row)>0)
	{
		# update
		$group_config_id = $row[0];
		$sql = "UPDATE group_config
				SET
						notes_published=now(), modified=now()
				WHERE
						group_config_id='$group_config_id' ";
	} else
	{
		# insert new record
		$sql = "INSERT INTO group_config
				(group_id, notes_published, inputdate, modified)
				VALUES
				('$group_id', now(), now(), now()) ";
	}
	$li->db_db_query($sql);
}

closedb();
header("Location: release_template.php?group_id=$group_id&msg=2");
?>
