<?php
include_once('../../../system/settings/global.php');
include_once('../../includes/php/lib-db.php');
include_once('../../includes/php/lib-filemanager.php');
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Template") || !strstr($ck_user_rights, ":web:"));
auth("TA");
opendb();

$fm = new fileManager($ck_course_id, 0, "");

for($i=0; $i<sizeof($file_ids); $i++)
{
	$fid = $file_ids[$i];

	// check whether the template is being used, if yes, do not allow to delete
	$sql = "SELECT 
				COUNT(n.notes_id), f.ParentDirID
			FROM 
				eclass_file as f LEFT JOIN notes as n ON n.url = CONCAT(IFNULL(f.VirPath, '/'), f.Title) 
			WHERE 
				f.FileID = $fid
			GROUP BY 
				f.FileID";
	$check = $fm->returnArray($sql,2);
	list($exist_count, $folderID) = $check[0];

	if($exist_count==0)
	{
		$fm->deleteFile($fid);
		$fm->updateFolderSize($folderID);
	}
	else
		$error_array[] = $fid;
}

if(sizeof($error_array)!=0)
{
	$failedID = implode(",", $error_array);
	$msg = 7;
}
else
{
	$msg = 3;
}

closedb();
header("Location: templates_manage.php?numPerPage=$numPerPage&pageNo=$pageNo&order=$order&field=$field&msg=$msg&failedID=$failedID");
?>
