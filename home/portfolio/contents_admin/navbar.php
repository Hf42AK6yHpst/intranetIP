<?php

// Modifing by key

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($eclass_filepath."/src/includes/php/lib-groups.php");
include_once($eclass_filepath."/src/includes/php/lib-notes.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$disable_scroll = true;
// Setting ID temporarily, since session ID not setting
// $ck_user_id = 43;
// $ck_memberType = "T";
// $ck_course_id = 1274; // by eva temporaly
// $ck_scheme_id = 2;
$admin_url_path = "http://$eclass_httppath";
//$image_path = "http://192.168.0.245:61001/images/";
//$eclass_root = "/home/eclass30/eclass30";
//end setting
$le = new libeclass;
$linterface = new interface_html("popup3.html");
$pageNav = "&nbsp;";
$linterface->LAYOUT_START();
if ($notes_id!="" && $pageNo!=="")
{
	//view page
	$nt = new notes();
	if ($ck_memberType!="T" && $ck_memberType!="A")
	{
		$lo = new libgroups(classNamingDB($ck_course_id));
		$idList = $lo->returnFunctionIDs("NOTE",$ck_user_id);
		$nt->notes2($idList);
	}

	$pageNav = $nt->getNavigationiPortfolio($pageNo,$notes_id);


	// check for activity inside
	$sql = "SELECT COUNT(*) FROM notes_activity WHERE notes_id='".$notes_id."'";
	$row = $nt->returnArray($sql, 1);
	$isActivity =  ($row[0][0]>0) ? 1 : 0;
	
	for ($i=0; $i<sizeof($nt->documents); $i++)
	{
		if ($nt->documents[$i][0]==$notes_id)
		{
			$note_description = nl2br($nt->documents[$i][12]);
			break;
		}
	}
		//hdebug_r($note_description);
	//$note_description = nl2br($nt->getDescription($pageNo));
	//test start
	$sql = "SELECT url, RecordType FROM notes WHERE notes_id=$notes_id";
	$li = new libdb();
	$row = $li->returnArray($sql);
	$url = $row[0]["url"];

	//test end
	$JSscript = "parent.viewfrm.location.href='viewframe.php?notes_id=$notes_id&web_portfolio_id=$web_portfolio_id&pageNo=$pageNo';\n";
} elseif ($url!="")
{
	$url = str_replace("|:", "?", $url);
	$url = str_replace("|", "&", $url);
	$JSscript = "parent.viewfrm.location=\"$url\";\n";
}

//hdebug("13 : ".$JSscript);

$td_width = ($ck_default_lang=="chib5") ? "20%" : "30%";

intranet_closedb();

$linterface->LAYOUT_STOP();
?>

<script language="JavaScript">
var isTMopen = true;
function changeFWidth(){
	if (isTMopen)
	{
		parent.document.all.fs1.cols = "0, *";
		isTMopen = false;
	} else
	{
		parent.document.all.fs1.cols = "230, *";
		isTMopen = true;
	}
}


function goToPage(notes_id, pageNo){
	this.location = "navbar.php?notes_id="+notes_id+"&pageNo="+pageNo;
	parent.treeframe.highlightNode(pageNo);
}
function showDescription(){
	if (typeof(document.form1.note_description)!="undefined")
	{

		newWindow('',2);
		prevWnd=newWin;
		prevWnd.document.open();
		prevWnd.document.writeln('<html><head><title><?php echo $assignments_alert_msg6; ?></title><link rel=stylesheet href="<?=$admin_url_path?>/system/<?=$le->getSkinCSS($ck_scheme_id, 1)?>"><STYLE TYPE="text/css">BODY {background-color: white;}</STYLE></head><body>');
		prevWnd.document.writeln('<table width="100%" border=0><tr><td><b><?= $quizbank_desc ?></b></td>');
		prevWnd.document.writeln('<td align=right><a href="javascript:window.close()"><?php echo $button_close; ?></a></td></tr></table><hr size=1><table width="100%" border="0" align="center"><tr><td>');
		prevWnd.document.writeln(document.form1.note_description.value);
		prevWnd.document.writeln('</td></tr></table></body></html>');
		prevWnd.document.close();
		
	}
}
</script>

<link href="<?=$PATH_WRT_ROOT?>templates/<?= $LAYOUT_SKIN ?>/css/iportfolio/content.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/<?= $LAYOUT_SKIN ?>/css/iportfolio/iportfolio.css" rel="stylesheet" type="text/css">

<form name="form1">

<table width=100% border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td height="25" background="http://<?=$eclass_httppath;?>images/portfolio_eng/gimg/title_bar_bg.gif">
    <table width="100%" height="25" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="15" background="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/top_menu_bar.gif">&nbsp;</td>
					<td background="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/top_menu_bar.gif" width="<?=$td_width?>">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
<!--								<td class="chi_content_15" nowrap><a href="index_scheme.php" target="_parent" class='chi_content_15' title="<?=$button_back?>"><img src="http://<?=$eclass_httppath;?>images/portfolio_chib5/Icon/icon_back.gif" border='0'>&nbsp;<span class="tabletext"><?=$ec_iPortfolio['learning_portfolio_content']?></span></a></td>
								<td align="right"><a href="javascript:changeFWidth()"><img src="http://<?=$eclass_httppath;?>images/portfolio_eng/Icon/icon_window.gif" border="0"></a></td>-->
								<td align="left"><a href="javascript:changeFWidth()"><img src="http://<?=$eclass_httppath;?>images/portfolio_eng/Icon/icon_window.gif" border="0"></a></td>
							</tr>
						</table>
					</td>
          <td nowrap background="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/top_menu_bar.gif"><?= $pageNav ?></td>
					<td width="210" background="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/top_menu_bar.gif" class="chi_content_15" >&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>

<input type="hidden" name="notes_id" value="<?=$notes_id?>" >

<div id="hiddenLyr" style="position:absolute; top:-400px;"> 
<textarea name="note_description"><?=$note_description?></textarea>
</div>
</form>

<script language="javascript">
<?php
if (($notes_id!="" && $pageNo!=="") || $url!="")
	echo $JSscript;
?>
</script>

</body>
</html>
