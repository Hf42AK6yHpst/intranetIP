<?php
include_once('../../../system/settings/global.php');
include_once('../../includes/php/lib-db.php');
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Template") || !strstr($ck_user_rights, ":web:"));
auth("TA");
opendb();

include_once("../new_header.php");

$li = new libdb;

$fid = (is_array($file_ids)) ? $file_ids[0] : $file_ids;
	
$sql  = "SELECT Title FROM eclass_file WHERE FileID = '$fid'";
$row = $li->returnVector($sql);
$fileTitle = $row[0];
$search = array(".html", ".HTML", ".htm", ".HTM");
$fileTitle = str_replace($search, "", $fileTitle);

# define the navigation, page title and table size
$template_pages = Array(
					Array($ck_custom_title['web'], "main.php"),
					Array($ec_iPortfolio['manage_templates'], "templates_manage.php"),
					Array($button_copy, "")
					);
$template_width = "95%";

echo getBodyBeginning($template_pages, $template_width);

?>

<script language=javascript>
function copyFile(obj) {
	var msg = "<?=$ec_warning['coyp_template']?>";
	if(confirm(msg)){
		obj.btn_submit.disabled=true;
		obj.action = "template_copy_update.php";
		obj.submit();
	}
	return false;
}
</script>

<form name="form1" method="post" onSubmit="return copyFile(this);">
<br>
<table width=55% border=0 cellpadding=5 cellspacing=0 align="center">
  <tr>
	<td align=right nowrap valign="top"><?=$ec_iPortfolio['template_title']?>:</td>
	<td><?=$fileTitle?></td>
  </tr>
  <tr>
	<td align=right nowrap valign="top"><?=$ec_iPortfolio['copied_template_title']?>:</td>
	<td><input type="text" name="newTitle" /></td>
  </tr>
</table>

<br><br>
<hr>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="right">
	<input class="button_g" type="submit" value="<?=$button_submit?>" name="btn_submit"> 
	<input class="button_g" type="reset" value="<?=$button_reset?>">
	<input class="button_g" type="button" value="<?=$button_cancel?>" onClick="self.location='templates_manage.php';"></td>
</tr>
</table>
<input type=hidden name='file_id' value="<?=$fid?>">
<input type=hidden name=pageNo value="<?=$pageNo?>">
<input type=hidden name=order value="<?=$order?>">
<input type=hidden name=field value="<?=$field?>">
</form>

<?= getPageEnding() ?>

<?php
closedb();
?>
