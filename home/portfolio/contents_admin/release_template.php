<?php
include_once('../../../system/settings/global.php');
include_once('../../includes/php/lib-db.php');
include_once('../../includes/php/lib-notes.php');
include_once('../../includes/php/lib-notes_portfolio.php');
include_once('../../../system/settings/lang/'.$ck_default_lang.'.php');
$EC_BL_ACCESS_HIGH = (!strstr($ck_user_rights, ":manage:") || !strstr($ck_user_rights, ":web:"));
auth("TA");
opendb();
head(11);

$li = new notes_iPortfolio();

?>


<link href='../../includes/css/style_iPortfolio_purple.css' rel='stylesheet' type='text/css'>
<script language="javascript">
function viewPage(notes_id, page_no){
	newWindow("../contents_student/viewpage.php?notes_id="+notes_id+"&pageNo="+page_no, 13);
}

function submitForm(){
	if (confirm("<?=$ec_warning['release_template']?>"))
	{
		document.form1.submit();
	}
	return;
}

function resetForm(){
	document.form1.reset();
}
</script>


<form name="form1" method="post" action="release_template_update.php">

<br>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td width="7" align="left"><img src="<?=$sr_image_path?>/Border/Title_Left.gif" width="7" height="30"></td>
      <td background="<?=$sr_image_path?>/Border/Title_BG.gif" width="100%" ><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td class="18Gray"><?=$ec_iPortfolio['release_template']?></td><td align="right"><?=getSystemMessage($msg)?></td></tr></table></td>
      <td width="6" align="right"><img src="<?=$sr_image_path?>/Border/Title_Right.gif" width="6" height="30"></td>
    </tr>
    <tr>
      <td colspan="3" background="<?=$sr_image_path?>/Border/dot_Line.gif"><img src="<?=$sr_image_path?>/Border/dot_Line.gif" width="25" height="9"></td>
    </tr>
    <tr>
      <td colspan="3"><table width="100%" border=0 cellpadding=8 cellspacing=0 align="center">
      <tr><td class='title' nowrap><?=$namelist_groups_name?></td><td class='title'><?=$ec_iPortfolio['release_template_notes']?></td><td><input type=checkbox name='checkmaster' onClick="(this.checked)?setChecked(1,document.form1,'groups_id[]'):setChecked(0,document.form1,'groups_id[]')"></td></tr>
	<?= $li->getReleaseGroupTemplates() ?></table>

</td></tr>
    <tr>
      <td colspan="3" background="<?=$sr_image_path?>/Border/dot_Line.gif"><img src="<?=$sr_image_path?>/Border/dot_Line.gif" width="25" height="9"></td>
    </tr>
    <tr>
    <td colspan="3" align="right"><table width="150" border="0" cellspacing="5" cellpadding="0">
              <tr>
                <td><a href="javascript:submitForm()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Ok','','<?=$sr_image_path?>/Buttons/btn_Public_U.gif',1)"><img src="<?=$sr_image_path?>/Buttons/btn_Public_D.gif" name="Ok" border="0"></a></td>
                <td><a href="javascript:resetForm()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('reset1','','<?=$sr_image_path?>/Buttons/btn_Reset_U.gif',1)"><img src="<?=$sr_image_path?>/Buttons/btn_Reset_D.gif" name="reset1" border="0" id="reset1"></a></td>
                <td><a href="app/toc.php?group_id=<?=$group_id?>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cancel','','<?=$sr_image_path?>/Buttons/btn_Cancel_U.gif',1)"><img src="<?=$sr_image_path?>/Buttons/btn_Cancel_D.gif" name="cancel" border="0" id="cancel"></a></td>
              </tr>
            </table>
    </td></tr>
</table>

<?php if ($msg==2) { ?>
<script language="JavaScript">
parent.treeframe.location.reload();
</script>
<?php } ?>

<input type="hidden" name="group_id" value="<?=$group_id?>" >
</form>


</body>
</html>

<?php
closedb();
?>