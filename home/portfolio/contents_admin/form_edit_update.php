<?php
include_once('../../../system/settings/global.php');
include_once('../../includes/php/lib-db.php');
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));
auth("TA");
opendb();

$answersheet = HTMLtoDB($qStr);


// update if submitted from edit.php
$li = new libdb();
if ($notes_id!="")
{
	$sql = "UPDATE assignment SET answersheet='$answersheet' WHERE worktype=8 AND parent_id='$notes_id' ";
	$li->db_db_query($sql);
	//debug($sql);
	$sql = "UPDATE notes SET modified=now() WHERE notes_id='$notes_id' ";
	$li->db_db_query($sql);
	//debug($sql);
}


closedb();


header("Location: view_form.php?notes_id=$notes_id");
?>