<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($eclass_filepath."/src/includes/php/lib-filemanager.php");
include_once($eclass_filepath."/src/includes/php/lib-notes.php");
include_once($eclass_filepath."/src/includes/php/lib-notes_portfolio.php");

include_once($eclass_filepath."/src/includes/php/lib-record.php");
include_once($eclass_filepath."/src/includes/php/lib-courseinfo.php");
include_once($eclass_filepath."/src/includes/php/lib-groups.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
// $ck_course_id = 1274; 

//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));

$title = HTMLtoDB($title);
$description = HTMLtoDB($description);
$status = htmlspecialchars($status);

$starttime = (trim($starttime)!="" && $status==1) ? "'".$starttime." $sh:$sm:00'" : "NULL";
$endtime = (trim($endtime)!="" && $status==1) ? "'".$endtime." $eh.$em:59'" : "NULL";
$url = ($linktype==1) ? HTMLtoDB($template_file) : "";

# if url=="", this will represent a folder!
if (($notes_type=="DOC" && $url=="") || $method=="FORM" || $method=="WEBLOG" || $method=="ACT" || $method=="AWARD")
{
	$url = "http://";
}


$sql = "SELECT IFNULL(MAX(a_no),0)+1 FROM notes";
$li = new librecord($sql,classNamingDB($ck_course_id));
$row = $li->resultSet();
$max = $row[0];

$li = new libdb();
$li->db = classNamingDB($ck_course_id);
$sql = "INSERT INTO ".$eclass_prefix."c".$ck_course_id.".notes (a_no, b_no, c_no, d_no, e_no, title, url, description, status, starttime, endtime, RecordType, inputdate, modified)
		values ($max, 0, 0, 0, 0, '$title', '$url', '$description', '$status', $starttime, $endtime, '$method', now(), now())";
$li->db_db_query($sql);
$notes_id = $li->db_insert_id();


# build form, weblog interactive tools
if ($method=="FORM" || $method=="WEBLOG")
{
	/*
		============ remark of notes <=> assignment ============
			worktype: 8 - form; 9 - weblog;
			parent_id <=> notes_id

	*/
	$worktype = ($method=="FORM") ? 8 : 9;
	$answersheet = HTMLtoDB($answersheet);
	$sql = "INSERT INTO ".$eclass_prefix."c".$ck_course_id.".assignment
				(title, status, answersheet, parent_id, worktype, sheettype inputdate, modified)
				VALUES
				('student learning profile: $method', 1, '$answersheet', $notes_id, $worktype, '$sheettype', now(), now()) ";
	$li->db_db_query($sql);
}


$li = new notes();
if(isset($m))
{
	if($m == 0) echo $li->moveAfter($notes_id, $chapter);
	if($m == 1) echo $li->moveBelow($notes_id, $chapter);
	if($m == 2) echo $li->swapChild($notes_id, $chapter);
}
$li->buildStatus();

if ($web_portfolio_id!="")
{
	$sql = "INSERT INTO  ".$eclass_prefix."c".$ck_course_id.".grouping_function (group_id, function_id, function_type) VALUES ($web_portfolio_id, $notes_id, 'NOTE')";
	$li->db_db_query($sql);
	
	$ln = new notes_iPortfolio();
	# if in used, update the notes_update_tracking table
	if($ln->getGroupPermission($web_portfolio_id)>=0)
	{
		$ln->updatePublishedNotesChange($web_portfolio_id);
	}
}

intranet_closedb();

?>
<script language="javascript">
parent.treeframe.location.reload();
parent.basefrm.location = "navbar.php?notes_id=<?=$notes_id?>";
</script>

