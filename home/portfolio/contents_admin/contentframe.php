<?php
/**
 * Editing by:
 * 
 * Modification Log
 * 2013-06-13 Jason
 * 		- fix the problem of displaying improper tree view of toc.php by passing web_portfolio_id
 * 
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($eclass_filepath."/src/includes/php/lib-record.php");
//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

/* Edit by Eva temporarily, reason: access right not set
if (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:")) {
	header("Location: $admin_url_path/access.php");
}
*/

intranet_auth();
intranet_opendb();

$web_portfolio_id = (is_array($web_portfolio_id)) ? $web_portfolio_id[0] : $web_portfolio_id;

$libeclass = new libeclass();
$linterface = new interface_html("");
$firstPage = "app/toc.php?web_portfolio_id=".$web_portfolio_id;
$linterface->LAYOUT_START();
?>


<html>
<head>
<?= $libeclass ->HTMLnoCache() ?>
<script language=JavaScript src='<?=$PATH_WRT_ROOT?>templates/script.js'></script>
<title>eClass</title>
</head>
<!-- orignial 25 -->
<FRAMESET rows="25,*" frameborder="NO" border="0" framespacing="0">
	<FRAME src="navbar.php" name="basefrm" scrolling="no" noresize >
	<FRAMESET name="fs1" cols="230,*" frameborder="1" framespacing="1" bordercolor="#F9F9F9" >
		<frame src="tree_nav.php?web_portfolio_id=<?=$web_portfolio_id?>" name="treeframe" scrolling=auto>
		<frame src="<?=$firstPage?>" name="viewfrm" scrolling="YES">
	</FRAMESET>
</FRAMESET>

</html>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>