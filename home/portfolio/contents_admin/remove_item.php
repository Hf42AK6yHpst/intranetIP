<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($eclass_filepath."/src/includes/php/lib-notes.php");
include_once($eclass_filepath."/src/includes/php/lib-notes_portfolio.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

// Setting ID temporarily, since session ID not setting
// $ck_course_id = 1274; // by eva temporaly
# layout skin for learning portfolio management IP2.5
//$image_path = "http://192.168.0.245:61001/images/";
if($intranet_session_language=="en")
{
	$sr_image_path = "http://".$eclass_httppath."/images/portfolio_eng";
}else{
	$sr_image_path = "http://".$eclass_httppath."/images/portfolio_chib5";
}
//end setting
//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));

$li = new notes_iPortfolio();
$li->notes2($notes_id[0]);
$group_permission = $li->getGroupPermission($web_portfolio_id);

?>

<script language="javascript">
function doCancel(){
	history.back();
}
function doSubmit(){

	var msg = "<?=$ec_iPortfolio['notes_remove_confirm']?>";
	if(confirm(msg))
	{
		document.form1.submit();
	}
}

</script>


<form name=form1 action="remove.php" method="post">
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td width="7" align="left"><img src="<?=$sr_image_path?>/Border/Title_Left.gif" width="7" height="30"></td>
    <td background="<?=$sr_image_path?>/Border/Title_BG.gif" class="18Gray"><?=$button_remove?></td>
    <td width="6" align="right"><img src="<?=$sr_image_path?>/Border/Title_Right.gif" width="6" height="30"></td>
  </tr>
		<tr >
          <td background="<?=$sr_image_path?>/Border/dot_Line.gif" colspan="3" ><img src="<?=$sr_image_path?>/Border/dot_Line.gif" width="25" height="9"></td>
        </tr>
  <tr>
    <td>&nbsp;</td>
    <td><br>
<blockquote>
<?php 
if($group_permission<0)
	echo $ec_iPortfolio['notes_remove_guide']; 
else
	echo $ec_iPortfolio['published_notes_remove_guide'];
?>
<br><br>
<?= $li->getSelectTree($notes_id) ?>
</blockquote>
    </td>
    <td>&nbsp;</td>
  </tr>
  	  <tr>
          <td ><br></td>
        </tr>
        <tr>
          <td background="<?=$sr_image_path?>/Border/dot_Line.gif" colspan="3" ><img src="<?=$sr_image_path?>/Border/dot_Line.gif" width="25" height="9"></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="right"> 
			<table width="150" border="0" cellspacing="5" cellpadding="0">
              <tr>
                  <td><input type="image" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('okay','','<?=$sr_image_path?>/Buttons/btn_Ok_U.gif',1)" src="<?=$sr_image_path?>/Buttons/btn_Ok_D.gif" name="okay" border="0"></td>
                  <td><a href="javascript:doCancel()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cancel','','<?=$sr_image_path?>/Buttons/btn_Cancel_U.gif',1)"><img src="<?=$sr_image_path?>/Buttons/btn_Cancel_D.gif" name="cancel" border="0"></a></td>
			</tr>
            </table>
			</td>
          </td>&nbsp;</td>
        </tr>
</table>
<input type="hidden" name="group_id" value="<?=$group_id?>" />
<input type="hidden" name="web_portfolio_id" value="<?=$web_portfolio_id?>" />
</form>


</body>
</html>

<?php
intranet_closedb();
?>
