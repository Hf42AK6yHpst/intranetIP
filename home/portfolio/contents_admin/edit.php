<?php
/* 
 * Modify by Siuwan
 * 
 * Modification Log
 * 2014-06-25 (Siuwan)
 * - modified addTemplate() change index.php to editor_index.php
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

include_once($eclass_filepath."/src/includes/php/lib-filemanager.php");
include_once($eclass_filepath."/src/includes/php/lib-notes.php");
include_once($eclass_filepath."/src/includes/php/lib-notes_portfolio.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));
// Setting ID temporarily, since session ID not setting
// $ck_user_id = 43;
// $ck_memberType = "T";
// $ck_course_id = 1274; // by eva temporaly
# layout skin for learning portfolio management IP2.5
if($intranet_session_language=="en")
{
	$sr_image_path = "http://".$eclass_httppath."/images/portfolio_eng";
}else{
	$sr_image_path = "http://".$eclass_httppath."/images/portfolio_chib5";
}
//end setting

$lpf = new libportfolio2007();
$le = new libeclass;
$li = new notes_iPortfolio();
$li->notes2($notes_id);

# Load scheme start date and deadline
$scheme_obj = $li->getSchemeInfo($web_portfolio_id);
$scheme_obj = $scheme_obj[0];

$scheme_startdatetime = $scheme_obj['starttime'];
if($scheme_startdatetime != "")
{
	list($scheme_startdate, $scheme_starttime) = explode(" ", $scheme_startdatetime);
	list($scheme_starthour, $scheme_startmin, $scheme_startsec) = explode(":", $scheme_starttime);
	
	$HasSchemeStartDate = true;
}
else
	$HasSchemeStartDate = false;

$scheme_enddatetime = $scheme_obj['deadline'];
if($scheme_enddatetime != "")
{
	list($scheme_enddate, $scheme_endtime) = explode(" ", $scheme_enddatetime);
	list($scheme_endhour, $scheme_endmin, $scheme_endsec) = explode(":", $scheme_endtime);
	
	$HasSchemeEndDate = true;
}
else
	$HasSchemeEndDate = false;

list($notes_id, $a_no, $b_no, $c_no, $d_no, $e_no,
	$title_html, $url, $readflag, $status, $inputdate, $title, $description, $starttime, $endtime, $record_type) = $li->getDocument($notes_id);

$cur_status = $le->getStatus($button_edit, "");

$isFolder = $li->isFolder($notes_id);

//$group_permission = $li->getGroupPermission($web_portfolio_id);

$status = ($status=="") ? 0 : $status;
${"status_checked_".$status} = "checked";

$table_status = ($status) ? "" : "none";

if (($time_arr=splitTime($starttime))!="") {
	$starttime = $time_arr[0];
	$sh = $time_arr[1];
	$sm = $time_arr[2];
}
if (($time_arr=splitTime($endtime))!="") {
	$endtime = $time_arr[0];
	$eh = $time_arr[1];
	$em= $time_arr[2];
}

$linterface = new interface_html("popup4.html");
$linterface->LAYOUT_START();

?>



<script language="javascript">
isToolTip = true;
isMenu = true;


function checkform(obj){
	var isst = false;
	if(!check_text(obj.title, "<?= $ec_warning['notes_title'] ?>")) return false;
	if (typeof(obj.starttime)!="undefined")
	{
		if (obj.starttime.value!="")
		{
			if(!check_date(obj.starttime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			isst = true;
			
<?php if($HasSchemeStartDate) { ?>
			time1 = '<?=$scheme_startdate?>'+'<?=$scheme_starthour?>'+'<?=$scheme_startmin?>';
			time2 = toTimeStr(obj.starttime, obj.sh, obj.sm);

			if(time2 < time1)
			{
				alert("<?=$ec_warning['start_end_scheme_start_time']?>");
				obj.starttime.focus();
				return false;
			}
<?php } ?>
		}
	}
	if (typeof(obj.endtime)!="undefined")
	{
		if (obj.endtime.value!="")
		{
			if(!check_date(obj.endtime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			if (isst)
			{
				if(!compareTime(obj.starttime, obj.sh, obj.sm, obj.endtime, obj.eh, obj.em)) {
					obj.starttime.focus();
					alert("<?= $w_alert['start_end_time2'] ?>");
					return false;
				}
			}
			
<?php if($HasSchemeEndDate) { ?>
			time1 = toTimeStr(obj.endtime, obj.eh, obj.em);
			time2 = '<?=$scheme_enddate?>'+'<?=$scheme_endhour?>'+'<?=$scheme_endmin?>';

			if(time2 < time1)
			{
				alert("<?=$ec_warning['start_end_scheme_end_time']?>");
				obj.endtime.focus();
				return false;
			}
<?php } ?>
		}
}
	// assign web_portfolio_id obtained from tree navigation page
	document.form1.web_portfolio_id.value = parent.treeframe.form1.web_portfolio_id.value;
}

function addTemplate(){
	var url = "http://<?=$eclass_httppath ?>/src/tool/misc/html_editor/editor_index.php?comeFrom=CC_templates&categoryID=0&fieldname=template_file";
	newWindowNotClose(url,13);
}

function previewTemplate(){
	var obj = document.form1;
	if (typeof(obj.template_file)!="undefined")
	{
		var file_link = obj.template_file.options[obj.template_file.selectedIndex].value;
		newWindowNotClose("http://<?=$eclass_httppath ?>/src/course/resources/files/open.php?fpath="+file_link, 14);
	}
}
function doCancel(){
	history.back();
}
function doReset(){
<?php if ($mode==1) { ?>
	displayTable('table_method_html', '');
	displayTable('table_method_eclass', '');
<?php } ?>
	displayTable('table_period', '');

	document.form1.reset();
}
<?=$js_extra_function?>
</script>


<form name=form1 action="editUpdate.php" method="post" onSubmit="return checkform(this);">
<div id="ToolTip"></div>
<div id="ToolMenu"></div>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td width="7" align="left"><img src="<?=$sr_image_path?>/Border/Title_Left.gif" width="7" height="30"></td>
    <td background="<?=$sr_image_path?>/Border/Title_BG.gif" class="18Gray"><?=$button_edit?></td>
    <td width="6" align="right"><img src="<?=$sr_image_path?>/Border/Title_Right.gif" width="6" height="30"></td>
  </tr>
		<tr >
          <td background="<?=$sr_image_path?>/Border/dot_Line.gif" colspan="3" ><img src="<?=$sr_image_path?>/Border/dot_Line.gif" width="25" height="9"></td>
        </tr>
  <tr>
    <td>&nbsp;</td>
    <td><br><table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td valign="top"width="50" class="13GrayP5555" nowrap><?= $notes_title ?>:</td>
          <td > <input type=text name="title" size=50 maxlength=255 value="<?=str_replace('"','&quot;',$title)?>">
          </td>
        </tr>
        <tr>
          <td valign="top"width="50" class="13GrayP5555" nowrap><?= $ec_iPortfolio['notes_guide'] ?>:</td>
          <td ><?=$linterface->getTextArea("description", $description)?></textarea>
          </td>
        </tr>
	<tr>
		<td valign="top"width="50" class="13GrayP5555" nowrap><?= $qbank_status ?>:</td>
         <td >
			<table width="365" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="100%" class="13Gray" nowrap>
					<input type=radio name=status id="status_1" value="1"  onClick="if(this.checked){displayTable('table_period', 'block');}" <?=$status_checked_1?>><label for="status_1"><?= $button_public ?></label>&nbsp;&nbsp;
					<input type=radio name=status id="status_0" value="0"  onClick="if(this.checked){displayTable('table_period', 'none');}" <?=$status_checked_0?>><label for="status_0"><?= $button_private ?></label>

					<table border=0 id="table_period" style="display:<?=$table_status?>;">
					<tr><td nowrap><?=$StartTime?>:</td><td nowrap>&nbsp;<input class="inputfield" type=text name="starttime" size=11 maxlength=10 value="<?=$starttime?>" onFocus="if(this.value==''){this.value='<?=date("Y-m-d")?>';}"><?= $linterface->GET_CALENDAR("form1", "starttime"); ?> <?= returnHour('sh', $sh, 0).returnMinute('sm', $sm, 0) ?> <?= $linterface->getTipsHelp($ec_guide['learning_portfolio_time_start'])?></td></tr>
					<tr><td nowrap><?=$EndTime?>:</td><td nowrap>&nbsp;<input class="inputfield" type=text name=endtime size=11 maxlength=10 value="<?=$endtime?>" onFocus="if(this.value==''){this.value='<?=date("Y-m-d")?>';}"><?= $linterface->GET_CALENDAR("form1", "endtime"); ?> <?= returnHour('eh', $eh).returnMinute('em', $em) ?> <?= $linterface->getTipsHelp($ec_guide['learning_portfolio_time_end'])?></td></tr>
					</table>
				  </td>
                </tr>
              </table>
          </td>
        </tr>
<?php
if ($record_type=="HTML")
{
	$fm = new fileManager($ck_course_id, 0, "");

	$requestCreatorID = 0;
	if($sys_custom['iPf']['LP']['editLPForCreatedTeacherOnly']){

	if($lpf->IS_IPF_SUPERADMIN()){
	}else{

		$objEclass = new libeclass();
		$result = $objEclass->returnSpecialRooms($ck_user_email,$ipfRoomType = 4);

		if(count($result) == 1){
			$result = current($result);
			$this_eclass_use_id = $result['user_id'];
			$requestCreatorID = $this_eclass_use_id;		
		}
	}
}

	$files_arr = $fm->getFileInside("/TEMPLATES", $requestCreatorID);
	$templates_html = $li->returnTemplatesSelection($files_arr);

	if ($url!="http://")
	{
		$linktype_1 = "checked";

		// check the current selected templates
		$js_extra_function2 = "for (var i=0; i<document.form1.template_file.length; i++) {	if(document.form1.template_file.options[i].value==\"$url\") { document.form1.template_file.selectedIndex = i; }};\n";
	} else
	{
		$linktype_0 = "checked";
	}
?>
<tr>
          <td class="13GrayP5555" nowrap><?= $ec_iPortfolio['notes_template'] ?>:</td>
          <td height="50"><input type=hidden name=linktype value="1" ><?= $templates_html ?><input class="formbutton" type="button" value="<?=$button_preview?>" onClick="javascript:previewTemplate()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">&nbsp;<input class="formbutton" type="button" value="<?=$button_new_template?>" onClick="javascript:addTemplate()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
          </td>
</tr>
<?php
}
?>

      </table></td>
    <td>&nbsp;</td>
  </tr>
  	  <tr>
          <td ><br></td>
        </tr>
        <tr>
              <td background="<?=$sr_image_path?>/Border/dot_Line.gif" colspan="3" ><img src="<?=$sr_image_path?>/Border/dot_Line.gif" width="25" height="9"></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="right"> <table width="150" border="0" cellspacing="5" cellpadding="0">
              <tr>
                  <td><input class="formbutton" type="submit" value="<?=$button_confirm?>" name="btn_submit" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"></td>
                  <td><input name="Reset" class="formbutton" value="<?=$button_reset?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" type="button" onClick="doReset()"></td>
                  <td><input class="formbutton" type="button" value="<?=$button_cancel?>" onClick="javascript:doCancel()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"></td>
					</tr>
            </table></td>
          </td>&nbsp;</td>
        </tr>
</table>

<input type="hidden" name="notes_id" value="<?=$notes_id?>" />
<input type="hidden" name="web_portfolio_id" value="<?=$web_portfolio_id?>" />
<input type="hidden" name="record_type" value="<?=$record_type?>" />
</form>

<script language="JavaScript">
<?= $js_extra_function2 ?>
</script>

</BODY>
</HTML>
<?php
echo autoFocusField("title");
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
