<?php
include_once('../../../system/settings/global.php');
include_once('../../includes/php/lib-db.php');
include_once('../../includes/php/lib-user.php');
include_once('../../includes/php/lib-filesystem.php');
include_once('../../includes/php/lib-filemanager.php');
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Template") || !strstr($ck_user_rights, ":web:"));
auth("TA");
opendb();

$li = new libdb();

if ($file_id!="")
{
	$sql  = "SELECT Category, ParentDirID FROM eclass_file WHERE FileID = '$file_id'";
	$row = $li->returnArray($sql);
	list($categoryID, $folderID) = $row[0];

	$fs = new phpduoFileSystem();
	$fm = new fileManager($ck_course_id, $categoryID, $folderID);
	$fm->isCopy = true;

	$lu = new libuser($ck_user_id,$ck_course_id);
	$fm->user_name = addslashes($lu->user_name());
	$fm->dest_categoryID = $categoryID;
	$fm->dest_courseID = $ck_course_id;
	$fm->dest_folderID = $folderID;

	$search = array(".html", ".HTML", ".htm", ".HTM");
	$newTitle = str_replace($search, "", $newTitle);
	$newTitle = $newTitle.".html";

	// check whether the name already exist
	$sql = "SELECT Title FROM eclass_file WHERE ParentDirID = '$folderID' AND category = '$categoryID'";
	$row = $li->returnVector($sql);
	for($i=0; $i<sizeof($row); $i++)
	{
		if(trim($newTitle)==trim($row[$i]))
		{
			$repeated = 1;
			break;
		}
	}
	
	if($repeated==1)
	{
		$msg = 10;
	}
	else
	{
		$success = $fm->copyTemplateFile($file_id, $newTitle);
		$msg = ($success==1) ? 8 : 9;

		// update folder size
		$fm->db = classNamingDB($fm->dest_courseID);
		$fm->updateFolderSize($fm->dest_folderID);
	}
}
closedb();

header("Location: templates_manage.php?pageNo=$pageNo&order=$order&field=$field&msg=$msg");
?>