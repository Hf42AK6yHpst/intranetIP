<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

intranet_auth();
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:PeerReview") || !strstr($ck_user_rights, ":web:"));
//auth("T");


// load settings
$learning_portfolio_config_file = "$eclass_filepath/files/learning_portfolio_config.txt";
$filecontent = trim(get_file_content($learning_portfolio_config_file));
list($status, $review_type, $friend_review) = unserialize($filecontent);

//$table_type = ($status==1) ? "style='display:block;'" : "style='display:none;'";    //temporary to stop

# define the navigation, page title and table size
/*
$template_pages = Array(
					Array($ck_custom_title['profile'], "main.php"),
					Array($ec_iPortfolio['peer_review_setting'], "")
					);
$template_width = "550";

*/
$lpf = new libportfolio2007();

$currentPageIndex = 0;
$TabMenuArr = libpf_tabmenu::getLpSettingsTags($currentPageIndex);

# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Settings_LearningPortfolio";
$CurrentPageName = $iPort['menu']['learning_portfolio'];
### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

?>


<script language="javascript">
function Disable_Task()
{
	document.form1.type_s.disabled = true;
	document.form1.type_l.disabled = true;
	document.form1.type_c.disabled = true;
	document.form1.friend_yes.disabled = true;
	document.form1.friend_no.disabled = true;
}
function Enable_Task()
{
	document.form1.type_s.disabled = false;
	document.form1.type_l.disabled = false;
	document.form1.type_c.disabled = false;
	document.form1.friend_yes.disabled = false;
	document.form1.friend_no.disabled = false;
}

function jSubmit()
{
	var obj = document.form1;
	obj.submit();
}

</script>

<form name="form1" id="form1" method="post" action="learning_portfolio_setting_update.php">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr><td align="center">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr><td>
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
          <tr>
            <td class="tab_underline">
              <?=$lpf->GET_TAB_MENU($TabMenuArr) ?>
            </td>
          </tr>
          <tr>
            <td>
              <table width="80%" border="0" cellspacing="0" cellpadding="4">
<?php if($msg==2) echo "<p align='left'><font color='green'>".$ec_iPortfolio['update_msg']."</font></p>"; ?>
                <tr>
                	<td width="10%">&nbsp;</td>
                	<td class="formfieldtitle" nowrap="nowrap" valign="top"><span class="tabletext"> <label for="status_0"><?=$ec_iPortfolio['review_type']?></label> </span></td>
                	<td width="80%" colspan="2">
                	<!--<span class="tabletext"><input type="radio" name="status" id="status_0" value="0" onClick="if(this.checked){displayTable('table_review_type', 'none');}" <?=($status==0?"CHECKED":"")?>>&nbsp;<label for="status_0"><?= $ec_iPortfolio['disable_review'] ?></label></span>-->
                	<!--<span class="tabletext"><input type="radio" name="status" id="status_1" value="1" onClick="if(this.checked){displayTable('table_review_type', 'block');}" <?=($status==1?"CHECKED":"")?>>&nbsp;<label for="status_1"><?= $ec_iPortfolio['enable_review'] ?></label></span> -->
                	<span class="tabletext"><input type="radio" name="status" id="status_0" value="0" onClick="if(this.checked){Disable_Task()}" <?=($status==0?"CHECKED":"")?>>&nbsp;<label for="status_0"><?= $ec_iPortfolio['disable_review'] ?></label></span>
                	<span class="tabletext"><input type="radio" name="status" id="status_1" value="1" onClick="if(this.checked){Enable_Task()}" <?=($status==1?"CHECKED":"")?>>&nbsp;<label for="status_1"><?= $ec_iPortfolio['enable_review'] ?></label></span>
                	</td>
                </tr>
                <tr id="table_review_type" <?=$table_type?>>
                	<td>&nbsp;</td>
                	<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" width="15%" valign="top" nowrap="yes"><?=$ec_iPortfolio['peer_review_type']?>: </td>
                	<td>
                		<table border=0>
                  		<tr>
                  			<td><span class="tabletext"><input type="radio" name="review_type" id="type_s" value="SCHOOL" <?=($review_type=='SCHOOL'?'CHECKED':'')?>>&nbsp;<label for="type_s"><?=$ec_iPortfolio['school_review']?></label></span></td>
                  		</tr>
                  		<tr>
                  			<td><span class="tabletext"><input type="radio" name="review_type" id="type_l" value="LEVEL" <?=($review_type=='LEVEL'?'CHECKED':'')?>>&nbsp;<label for="type_l"><?=$ec_iPortfolio['level_review']?></label></span></td>
                  		</tr>
                  		<tr>
                  			<td><span class="tabletext"><input type="radio" name="review_type" id="type_c" value="CLASS" <?=($review_type=='CLASS'?'CHECKED':'')?>>&nbsp;<label for="type_c"><?=$ec_iPortfolio['class_review']?></label></span></td>
                  		</tr>
                		</table>
                	</td>
                </tr>
            		<tr>
            			<td>&nbsp;</td>
            			<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top">&nbsp;<label for="friend_1"><?=$ec_iPortfolio['friends_review']?></label></td>
            			<td valign="top">
            				<span class="tabletext"><input type="radio" name="friend_review" id="friend_yes" value="1" <?=($friend_review==1?"CHECKED":"")?>>&nbsp;<?=$ec_iPortfolio['yes']?></span>
            				<span class="tabletext"><input type="radio" name="friend_review" id="friend_no" value="" <?=($friend_review==""?"CHECKED":"")?>>&nbsp;<?=$ec_iPortfolio['no']?></span>
            			</td>
            		</tr>
            	</table>
          	</td>
          </tr>
        </table>
      </td></tr>
    </table>
    <br />
    <table width="90%" border="0" cellspacing="0" cellpadding="4">
      <tr>
        <td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
      </tr>
      <tr>
        <td align="center">
          <input name="submit_btn" class="formbutton" value="<?=$iPort['btn']['submit']?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" type="Button" onClick="jSubmit();">
          <input name="reset_btn" class="formbutton" value="<?=$iPort['btn']['reset']?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" type="Reset">
        </td>
      </tr>
    </table>
  </td></tr>
</table>
</form>

<!-- check the default setting -->
<script language="javascript">
<?
if($status == 1)
echo "Enable_Task();";
else
{
	echo "Disable_Task();";
	echo "document.form1.type_s.checked = 'checked';";
}
?>
</script>
 
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>