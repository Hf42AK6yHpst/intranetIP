<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));
$creatorID = $_SESSION['UserID'];
iportfolio_auth("T");
intranet_opendb();

$lpf = new libportfolio2007();
$lpf->ACCESS_CONTROL("learning_sharing");

// Count number of all students in classroom
$sql = "SELECT count(DISTINCT um.user_id) ";
$sql .= "FROM {$lpf->course_db}.usermaster um ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON um.user_email = iu.UserEmail ";
$sql .= "WHERE um.memberType = 'S' ";
$sql .= "AND (um.status IS NULL OR um.status <> 'deleted') ";
$cnt = current($lpf->returnVector($sql));

// Temp table for student count
$sql = "CREATE TEMPORARY TABLE {$lpf->course_db}.tempLPStudentCnt ";
$sql .= "SELECT web_portfolio_id, {$cnt} AS studentCnt FROM {$lpf->course_db}.web_portfolio";
$lpf->db_db_query($sql);

// Update count of LP with selected group
$sql = "UPDATE {$lpf->course_db}.tempLPStudentCnt tsc ";
$sql .= "INNER JOIN {$lpf->course_db}.grouping_function gf ON tsc.web_portfolio_id = gf.function_id AND function_type = 'PORTw' ";
$sql .= "SET tsc.studentCnt = (";
$sql .= "SELECT count(DISTINCT um.user_id) ";
$sql .= "FROM {$lpf->course_db}.grouping_function gf ";
$sql .= "INNER JOIN {$lpf->course_db}.user_group ug ON gf.group_id = ug.group_id ";
$sql .= "INNER JOIN {$lpf->course_db}.usermaster um ON ug.user_id = um.user_id ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON um.user_email = iu.UserEmail ";
$sql .= "WHERE tsc.web_portfolio_id = gf.function_id AND gf.function_type = 'PORTw'";
$sql .= "AND um.memberType = 'S' ";
$sql .= "AND (um.status IS NULL OR um.status <> 'deleted') ";
$sql .= ")";
$lpf->db_db_query($sql);

$checkmaster = true; //Use check function that have name="checkmaster"
if($order=="") $order=0;
if($field=="") $field=4;
$li = new libpf_dbtable($field, $order, $pageNo);
$li->field_array = array("a.title", "a.starttime", "a.instruction", "t_sc.studentCnt", "a.status", "a.modified");
$fieldname  = "CONCAT('<a class=\"tablelink\" href=\"contentframe.php?web_portfolio_id=', a.web_portfolio_id ,'\" target=\"_blank\">', a.title, '</a>'), ";

//$fieldname  = "if (a.version>=1 , CONCAT('<a class=\"tablelink\" href=\"javascript:newWindow(\'/home/portfolio/learning_portfolio_beta/contents/?mode=prototype&web_portfolio_id=',a.web_portfolio_id,'\',93)\">', a.title, '</a>'),CONCAT('<a class=\"tablelink\" href=\"javascript:newWindow(\'contentframe.php?web_portfolio_id=',a.web_portfolio_id,'\',93)\">', a.title, '</a>')), ";

$fieldname .= "if(a.starttime IS NOT NULL AND a.deadline IS NOT NULL, CONCAT(a.starttime, ' {$profiles_to}<br>', a.deadline), if(a.starttime IS NOT NULL AND a.deadline IS NULL, CONCAT('".$ec_iPortfolio['startdate']."<br />', a.starttime ), if(a.starttime IS NULL AND a.deadline IS NOT NULL, CONCAT('".$ec_iPortfolio['enddate']."<br />', a.deadline ),'--'))), ";
$fieldname .= "a.instruction, ";
$fieldname .= "CONCAT('<a class=\"tablelink\" href=\"scheme_student_list.php?web_portfolio_id=', a.web_portfolio_id, '\">', t_sc.studentCnt, '</a>'), ";
$fieldname .= "if(a.status='1','$qbank_public','--'), ";
$fieldname .= "a.modified, ";
$fieldname .= "CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\'web_portfolio_id[]\' value=', a.web_portfolio_id ,'>') ";

if($sys_custom['iPf']['LP']['editLPForCreatedTeacherOnly']){

	if($lpf->IS_IPF_SUPERADMIN()){
	}else{
		$extraSQL = ' and created_by = '.$creatorID;
	}
}

$sql = "SELECT $fieldname FROM web_portfolio AS a INNER JOIN {$lpf->course_db}.tempLPStudentCnt AS t_sc ON a.web_portfolio_id = t_sc.web_portfolio_id where 1 ".$extraSQL;

// TABLE INFO
$li->sql = $sql;
$li->db = classNamingDB($ck_course_id);
$li->title = $ec_iPortfolio['web_view'];
$li->no_msg = $no_record_msg;
$li->page_size = ($numPerPage=="") ? $page_size : $numPerPage;
$li->no_col = 8;

$li->table_tag = "<table bgcolor='#cccccc' border='0' cellpadding='4' cellspacing='1' width='100%'>";
$li->row_alt = array("#FFFFFF", "#F3F3F3");
$li->row_height = 25;
$li->plain_column = 1;
$li->plain_column_css = "tablelink";

$pageSizeChangeEnabled = true;

// TABLE COLUMN
$li->column_list .= "<td  width='2%' align='center'>#</td>\n";
$li->column_list .= "<td width='22%'>".$li->column(0, $ec_iPortfolio['growth_title'], 1)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column(1, $ec_iPortfolio['growth_phase_period'], 1)."</td>\n";
$li->column_list .= "<td width='25%'>".$li->column(2, $ec_iPortfolio['growth_description'], 1)."</td>\n";
$li->column_list .= "<td width='7%'>".$li->column(3, $ec_iPortfolio['heading']['no_stu'], 1)."</td>\n";
$li->column_list .= "<td  width='7%'>".$li->column(4, $ec_iPortfolio['growth_status'], 1)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(5, $profile_modified, 1)."</td>\n";
$li->column_list .= "<td width='2%'>".$li->check("web_portfolio_id[]")."</td>\n";
$li->column_array = array(0,11,0,0,0,0,0);

# define the navigation, page title and table size
// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_LearningPortfolio";
$CurrentPageName = $iPort['menu']['learning_portfolio'];

# Generate error message
switch($msg)
{
	case 1:
		$error_msg = $linterface->GET_SYS_MSG("add");
		break;
	case 2:
		$error_msg = $linterface->GET_SYS_MSG("update");
		break;
	case 3:
		$error_msg = $linterface->GET_SYS_MSG("delete");
	default:
		break;
}

$currentPageIndex = IPF_CFG_LP_MGMT_CONTENT;
$TabMenuArr = libpf_tabmenu::getLpMgmtTabs($currentPageIndex);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<? // ===================================== Body Contents ============================= ?>
<script language="javascript">
function deactivateRecord(){
	globalAlertDeactivate = '<?=$iPort["msg"]["confirm_deactivate"]?>';

	checkPick(document.form1,'web_portfolio_id[]','private.php',2);
}

</script>
<form name="form1" method="post" action="index_scheme.php" <?=$target?>>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right"><?=$error_msg ?></td>
	</tr>
	<tr>
		<td class="tab_underline" width="100%">
			<?=$lpf->GET_TAB_MENU($TabMenuArr) ?>
		</td>
	</tr>
	<tr>
		<td>
      <a href="scheme_new.php" class="contenttool"><img src="<?= $PATH_WRT_ROOT ?>/images/<?= $LAYOUT_SKIN ?>/icon_new.gif" hspace="4" border="0" align="absmiddle" alt='<?= $button_new ?>'><?= $button_new ?></a>
    </td>
  </tr>
	<tr>
    <td align="center">
      <table width="98%" border="0" cellpadding="0" cellspacing="0">
        <tr>
      		<td align="right" valign="bottom">
      			<table border="0" cellpadding="0" cellspacing="0">
      				<tbody>
      					<tr>
      						<td width="21"><img src="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/table_tool_01.gif" height="23" width="21"></td>
      						<td background="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/table_tool_02.gif">
      							<table border="0" cellpadding="0" cellspacing="2">
      								<tbody>
      									<tr>
      										<td nowrap="nowrap"><a href="javascript:checkPick(document.form1,'web_portfolio_id[]','public.php',1)" class="tabletool"><img src="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/icon_approve.gif" name="imgPublic" align="absmiddle" border="0" height="12" width="12"> <?=$button_public ?></a></td>
      										<td><img src="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/10x10.gif" width="5"></td>
      										<td nowrap="nowrap"><a href="javascript:deactivateRecord()" class="tabletool"><img src="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/icon_reject.gif" name="imgPrivate" align="absmiddle" border="0" height="12" width="12"><?=$button_private ?></a></td>
      										<td><img src="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/10x10.gif" width="5"></td>
      										<td nowrap="nowrap"><a href="javascript:checkEditContent(document.form1,'web_portfolio_id[]','contentframe.php',0)" class="tabletool"><img src="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/icon_edit.gif" name="imgEditContent" align="middle" border="0" height="12" width="12"><?=$ec_iPortfolio['edit_content'] ?></a></td>
      										<td><img src="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/10x10.gif" width="5"></td>
      										<td nowrap="nowrap"><a href="javascript:checkRemove(document.form1,'web_portfolio_id[]','scheme_remove.php')" class="tabletool"><img src="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/icon_delete.gif"  name="imgDelete" align="absmiddle" border="0" height="12" width="12"><?=$ec_iPortfolio['delete'] ?> </a></td>
      										<td><img src="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/10x10.gif" width="5"></td>
      										<td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'web_portfolio_id[]','scheme_edit.php',0)" class="tabletool"><img src="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/icon_edit.gif" name="imgEdit" align="absmiddle" border="0" height="12" width="12"><?=$ec_iPortfolio['edit'] ?></a></td>
      									</tr>
      								</tbody>
      							</table>
      						</td>
      						<td width="6"><img src="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/table_tool_03.gif" height="23" width="6"></td>
      					</tr>
      				</tbody>
      			</table>
      		</td>
      	</tr>
      	<tr>
      		<td  width="100%" align="center">
      			<table width="100%" border="0" cellpadding="0" cellspacing="0">
      				<tr>
      					<td align="center" valign="middle">
      						<table width="100%" border="0" cellpadding="0" cellspacing="0">
      							<tr>
      								<td>
      									<?= $li->displayPlain() ?>
      									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
<?php if ($li->navigationHTML!="") { ?>
      										<tr class='tablebottom'>
      											<td  class="tabletext" align="right"><?=$li->navigationHTML?></td>
      										</tr>
<?php } ?>
      									</table>
      								</td>
      							</tr>
						      </table>
      					</td>
      				</tr>
      			</table>
      		</td>
      	</tr>
      </table>
		</td>
	</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<? // ===================================== Body Contents (END) ============================= ?>
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
