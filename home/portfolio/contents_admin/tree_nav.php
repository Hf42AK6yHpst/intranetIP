<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($eclass_filepath."/src/includes/php/lib-notes.php");
include_once($eclass_filepath."/src/includes/php/lib-notes_portfolio.php");
//include_once($PATH_WRT_ROOT."includes/libnotes2007a.php");
//include_once($PATH_WRT_ROOT."includes/libnotesportfolio2007a.php");

include_once($eclass_filepath."/src/includes/php/lib-groups.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));
iportfolio_auth("T");
intranet_opendb();
//head(11);

// $ck_course_id = 1274; // by eva temporaly
$li = new notes_iPortfolio("", $web_portfolio_id);
$scheme_obj = $li->getSchemeInfo($web_portfolio_id);
$scheme_obj = $scheme_obj[0];
$rootTitle = preg_replace('(\n|\"|)', "", $scheme_obj["title"]);
$idList = $li->getGroupNotes($web_portfolio_id);
$li->notes2($idList);
//$group_permission = $li->getGroupPermission($web_portfolio_id);
$linterface = new interface_html("popup4.html");
$linterface->LAYOUT_START();
?>

<STYLE TYPE='text/css'>
BODY {background-color: #F9F6FF;}
</STYLE>
<script src="http://<?=$eclass_httppath?>/src/includes/js/tree.js"></script>


<form method="get" name="form1">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
  <td align="right">
	<table border="0" cellspacing="1" cellpadding="3">
	  <tr>
		<td class="bodycolor3" nowrap><a href="javascript:addRecord()" ><img src="http://<?=$eclass_httppath?><?=$image_path?>/portfolio_eng/Icon/icon_AddFolder.gif" hspace="2" border="0" align="absmiddle" alt='<?= $button_new ?>'><?= $button_new ?></a></td>
		<td align="right" class="bodycolor3" nowrap><a href="javascript:organizeRecords()" ><img src="http://<?=$eclass_httppath?><?=$image_path?>/portfolio_eng/Icon/icon_EditFolder.gif" hspace="2" border="0" align="absmiddle" alt='<?= $button_organize ?>'><?= $button_organize ?></a></td>
	  </tr>
	</table>
  </td>
</tr>
</table>

<script language="javascript">
detectBrowser();
ICONPATH = "http://<?=$eclass_httppath?><?=$image_path?>/tree/portfolio/";
PERSERVESTATE = 1;
WRAPTEXT = 1;
USEFRAMES = 1;2006/6/16
HIGHLIGHT = 1;

foldersTree = gFld("&nbsp;<?= $rootTitle ?>", "navbar.php?url=app/toc.php|:web_portfolio_id=<?=$li->web_portfolio_id?>");
foldersTree.iconSrc = 'http://<?=$eclass_httppath?><?=$image_path?>/tree/portfolio/root.gif';
foldersTree.treeID = "NOTES<?=strtoupper(session_id().$ck_course_id)?>";
<?= $li->getTree("http://$eclass_httppath$image_path/tree/portfolio") ?>
initializeDocument();


function addRecord(){
	var navObj = parent.basefrm;
	navObj.location = "navbar.php?url=new.php|:web_portfolio_id=" + document.form1.web_portfolio_id.value;
}

function organizeRecords(){
	var navObj = parent.basefrm;
	navObj.location = "navbar.php?url=organize.php|:web_portfolio_id=" + document.form1.web_portfolio_id.value;
}
</script>
<noscript>
A tree for site navigation will open here if you enable JavaScript in your browser.
</noscript>

<?php
	//if ($ck_memberType=="T" || $ck_memberType=="A") {       //edit it temporarily 
?>
<br>
<table width=100%>
<tr>
  <td><span class="tabletext"><?= $wording['contents_notice1'] ?></span></td>
</tr>
</table>
<?php
//}
?>
<input type="hidden" name="web_portfolio_id" value="<?=$web_portfolio_id?>" >
</form>

<p></p>
</body>
</html>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
