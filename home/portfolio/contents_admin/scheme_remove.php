<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));  edit by eva because access right not set
intranet_auth();
intranet_opendb();

// define eclass C1274 as database
$eclass_c1274 = $eclass_prefix."c".$ck_course_id;

$li = new libdb();

if (is_array($web_portfolio_id))
{
	$web_portfolio_id_str = implode(",", $web_portfolio_id);

	# delete contents!

	$sql = "SELECT function_id FROM {$eclass_c1274}.grouping_function WHERE group_id IN ($web_portfolio_id_str) AND function_type='NOTE' ";
	$row = $li->returnVector($sql);
	if (sizeof($row)>0)
	{
		$notes_id_str = implode(",", $row);
		$sql = "DELETE FROM {$eclass_c1274}.notes WHERE notes_id IN ($notes_id_str) ";
		$li->db_db_query($sql);
	}

	# from joining records
	$sql = "DELETE FROM {$eclass_c1274}.grouping_function WHERE function_id IN ($web_portfolio_id_str) AND function_type='PORTw' ";
	$li->db_db_query($sql);

	# delete web_portfolios
	$sql = "DELETE FROM {$eclass_c1274}.web_portfolio WHERE web_portfolio_id IN ($web_portfolio_id_str) ";
	$li->db_db_query($sql);
	
	#### Rmove from Student Notes #########
	# remove notes_student
	$sql = "DELETE FROM {$eclass_c1274}.notes_student WHERE web_portfolio_id IN ($web_portfolio_id_str) ";
	$li->db_db_query($sql);

	# remove from user_config
	$sql = "DELETE FROM {$eclass_c1274}.user_config WHERE web_portfolio_id IN ($web_portfolio_id_str) ";
	$li->db_db_query($sql);

	# remove from portfolio_tracking_last
	$sql = "DELETE FROM {$eclass_c1274}.portfolio_tracking_last WHERE web_portfolio_id IN ($web_portfolio_id_str) ";
	$li->db_db_query($sql);

	# remove from notes_student_update_track
	$sql = "DELETE FROM {$eclass_c1274}.notes_student_update_track WHERE web_portfolio_id IN ($web_portfolio_id_str) ";
	$li->db_db_query($sql);

	# remove from notes_update_track
	$sql = "DELETE FROM {$eclass_c1274}.notes_update_track WHERE web_portfolio_id IN ($web_portfolio_id_str) ";
	$li->db_db_query($sql);

	$msg = 3;
}

intranet_closedb();
header("Location: index_scheme.php?msg=$msg");
?>
