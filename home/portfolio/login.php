<?php

// Modifing by Paul
/**
 * Change Log:
 * 2018-11-01 Paul [ip.2.5.9.11.1]
 * 	- update to handle https link with non-443 port
 * 2016-09-05 Pun [ip.2.5.7.10.1]
 *  - update SessionKey after timeout instead of always generate it.
 */

include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libuser.php");
intranet_auth();
intranet_opendb();
$protocol = "http";
if($_SERVER['SERVER_PORT']=='443' || (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO']=="https")){
	$protocol = "https";
}
# session_register("eClassMode");
# $eClassMode = "I";
# setcookie("eClassMode", "I", 0, "", $eclass_httppath, 0);

$li = new libuser($UserID);
// $session_key = $li->generateSessionKey();
// $li->updateSession($session_key);
$results = $li->getSessionKeyLastUpdatedFromUserId($UserID);
$session_key = trim($results["SessionKey"]);
if ($session_key=="")
{
    $session_key = $li->generateSessionKey();
    $li->updateSessionByUserId($session_key, $UserID);
}
$li->updateSessionLastUpdatedByUserId($UserID);
$email = $li->UserEmail;
$passwd = $li->UserPassword;

if ($eclass_httppath_first == "")
{
  $url = $protocol."://{$eclass40_httppath}check2.php?user_course_id=$uc_id&eclasskey=".$session_key."&jumpback=$jumpback";
  #$url = "$eclass_url_root/check.php?user_course_id=$uc_id&email=$email&password=$passwd";
  header("Location: ".$protocol."://{$eclass40_httppath}mode.php?url=".urlencode($url));
}
else
{
  #$url = "$eclass_httppath_first/check.php?user_course_id=$uc_id&email=$email&password=$passwd";
  $url = "$eclass_httppath_first/check2.php?user_course_id=$uc_id&eclasskey=".$session_key."&jumpback=$jumpback";
  header("Location: $eclass_httppath_first/mode.php?url=".urlencode($url));
}

intranet_closedb();
?>
