<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
intranet_auth();
intranet_opendb();

$lfcm = new form_class_manage();
$ay_arr = $lfcm->Get_Academic_Year_List();

$program_start = strtotime($startdate);

for($i=0; $i<count($ay_arr); $i++)
{
  $t_ay_id = $ay_arr[$i]["AcademicYearID"];
  $t_year_start = strtotime($ay_arr[$i]["AcademicYearStart"]);
  $t_year_end = strtotime($ay_arr[$i]["AcademicYearEnd"]);

  if($t_year_start <= $program_start && $program_start <= $t_year_end)
  {
    echo $t_ay_id;
    break;
  }
} 


?>