<?php
// Modifing by 
############## Change Log [Start] #####
#
#	Date	:	2015-11-04	Omas
# 				Add $academicyearId method
#
############## Change Log [End] #######
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

intranet_opendb();
$lpf = new libportfolio2007();

if($academicyearId != ''){
	$semester_arr = $lpf->getSemesterArrByAYId($academicyearId);
	$semester_asso_arr = array();
	foreach((array)$semester_arr as $_semester_info){
		$_TermName = $_semester_info['TermName'];
		$_YearTermId = $_semester_info['YearTermId'];
		$semester_asso_arr[$_YearTermId] = $_TermName;
	}
	$semester_asso_arr[''] = $ec_iPortfolio['overall_result'];
	if($academicyearId == Get_Current_Academic_Year_ID()){
		$default = getCurrentSemesterID();
	}
	$semester_selection_html = getSelectByAssoArray($semester_asso_arr, 'name="commentsSem" id="commentsSem"', $default, 0, 1, "", 1);
}
else{	
	$semester_selection = $lpf->getSemesterSelectionFromIP("targetSemester", "", $ec_iPortfolio['overall_result'], $academicyear);
	$semester_selection_html = ($semester_selection=="") ? "<i>".$no_record_msg."</i>" : $semester_selection;
}

echo $semester_selection_html;
?>