<?php
/**
 * Change Log:
 * 2016-09-05 Pun [ip.2.5.7.10.1]
 *  - Change retrieveSessionKey load from libdb getSessionKeyLastUpdatedFromUserId()
 */
$PATH_WRT_ROOT = '../../../';

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
iportfolio_auth("TSP");
include_once($PATH_WRT_ROOT.'includes/libuser.php');
intranet_auth();
intranet_opendb();
if(($ck_memberType=='P'&&$ParUserID!=$ck_current_children_id)||($ck_memberType=='S'&&$ParUserID!=$UserID)){
	die();
}

switch ($FunctionName) {
	case 'retrieveSessionKey' : 
		$li = new libuser($ParUserID);
		$results = $li->getSessionKeyLastUpdatedFromUserId($UserID);
		$session_key = trim($results["SessionKey"]);
		if ($session_key=="")
		{
			$session_key = $li->generateSessionKey();
			$li->updateSession($session_key);
		}
		$li->updateSessionLastUpdatedByUserId($UserID);
		echo $session_key;
	break;
	default :
	break;
}
?>