<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
intranet_auth();
intranet_opendb();

$li_pf = new libpf_asr();

# get Class Level selection
$ay_arr = $li_pf->returnAssessmentYear("", "", $report_type);
$html_ay_selection = (sizeof($ay_arr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($ay_arr, "name='academicYearID'", "", 0, 0, "", 2);

echo $html_ay_selection;

//$ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", $yt_id, 0, 1, "", 2);
//echo $ayterm_selection_html;

?>