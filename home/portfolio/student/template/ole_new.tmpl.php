<?
/** [Modification Log] Modifying By: 
 * *******************************************
 * 2015-10-19 Omas
 * - added compulsory setting
 * 2014-08-20 Bill	(20140820)
 * - Title - auto-complete
 * 2010-10-13 Max 	(201010131146)
 * - Hide listContent_1 also when ajax get preset program name
 *
 * *******************************************
 */
##########################################################
## Modification Log
## 2013-02-22: YatWoon
## - display warning message for view mode
## 2013-02-21: YatWoon 
## - add checking for buttons
##
## 2010-02-12: Max (201005141505)
## - Added title as hidden field

## 2010-02-12: Max (201002121147)
## - Modified the display logic

## 
## 2009-12-08: Max (200912081532)
## - fix the "Add more files" cannot show in firefox
##########################################################
print ConvertToJSArray($RoleListArr, "jRoleArr1");
print ConvertToJSArray($AwardsListArr, "jAwardsArr1");
for ($f=0; $f<sizeof($LibWord->file_array); $f++)
{
	$id = $LibWord->file_array[$f][0];
	//print ConvertToJSArray($ListArr[$id], "jArr".$id);
	$ListArrJS = ConvertToJSArray($ListArr[$id], "jArr".$id);
	$ListArrJS = str_replace("&amp;#", "&#", $ListArrJS);
	$ListArrJS = str_replace("&#039;", "&#96;", $ListArrJS);
	print $ListArrJS;
	print ConvertToJsArray($ListELEArr[$id], "jELEArr".$id);
}

?>

<script type="text/javascript" src="/templates/2009a/js/ipf_jupas.js"></script>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>

<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />


<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">

<script language="JavaScript">
var jCurrentArr = eval("<?=$JScriptID?>");
var jCurrentELEArr = eval("<?=$JELEScriptID?>");
var jPresetELE = 1;
//onChangeStr = "document.getElementById('listContent').style.display = 'none'; jCurrentArr=eval('jArr'+this.value);";
onChangeStr = "document.getElementById('listContent').style.display = 'none'; jCurrentArr=eval('jArr'+this.value); jCurrentELEArr=eval('jELEArr'+this.value);";
</script>

<script language="JavaScript">

onChangeStr1 = "document.getElementById('title_selection_";
onChangeStr3 = "').style.display = 'block'";

function triggerFile(formObj, my_file, my_index)
{
	var is_remove = (eval("formObj.is_need_"+my_index+".checked==false"));
	var link = document.getElementById('a_'+my_index);
	link.style.textDecorationLineThrough = is_remove;
}

function checkform(formObj)
{
	
	var d = new Date();
	var curr_year = d.getFullYear();

	if ($("select[name='category']").val() == "-9") {
		alert("<?=$ec_warning['category']?>");
		return false;
	}
	// YuEn: check date and title!
	if(formObj.title.value=="")
	{
		alert("<?=$ec_warning['title']?>");
		return false;
	}
	
	// Check if this is a new program created by student but not joining
	if (<?= empty($programID)?1:0 ?>) {
		if(formObj.startdate.value=="")
		{
			formObj.startdate.focus();
			alert("<?=$ec_warning['date']?>");
			return false;
		}
		else
		{
			if(!check_date(formObj.startdate,"<?=$w_alert['Invalid_Date']?>"))
			{
				return false;
			}
			else if(formObj.enddate.value!="")
			{
				if(!check_date(formObj.enddate,"<?=$w_alert['Invalid_Date']?>"))
					return false;
				else if(formObj.startdate.value>formObj.enddate.value)
				{
					formObj.enddate.focus();
					alert("<?=$w_alert['start_end_time2']?>");
					return false;
				}
			}

			_startDate = formObj.startdate.value;
			_endDate = formObj.enddate.value;

			var startYear = "";
			if(_startDate.length > 4){
				startYear = _startDate.substring(0,4);
			}

			var endYear   = "";
			if(_endDate.length > 4){
				endYear   = _endDate.substring(0,4);
			}else{
				//if END date not set , set the end year default to current year for checking
				endYear = curr_year;
			}

			<?php //if input year more than current Year 5 year (HardCode) , alert message
			?>
			if((startYear - curr_year > 5) || (endYear  - curr_year > 5)){
				alert("<?=$Lang['iPortfolio']['alertYearExceed']?>");
				return false;
			}
		}
	}
	// Check hours
	var hour = document.getElementsByName('hours')[0];
	if (isNaN(hour.value)) {
		hour.focus();
		alert("<?= $ec_iPortfolio['SLP']['warning_Hour'] ?>");
		return false;
	}
	if (!<?=empty($maximumHours)?'true':'false'?>
		&& Number(hour.value)><?=empty($maximumHours)?0:$maximumHours?>) {
			
		hour.focus();
		alert("<?= $ec_iPortfolio['SLP']['ExceedingMaxumumHours'] ?>");
		return false;
	}

	//CHeck Attachment Path format
	var attachment_size = document.getElementById('attachment_size').value;
	for (var x = 1; x <= attachment_size; x++)
   {
		file_path = "ole_file"+x;
		if (document.getElementById(file_path) != undefined)
		{
			var path = document.getElementById(file_path).value;
			if(path!=""){
				if((path.charAt(0) != "\\" && path.charAt(1) != "\\") && (path.charAt(0) != "/" && path.charAt(1) != "/"))
				{

					if(!path.charAt(0).match(/^[a-zA-z]/))
					{
						document.getElementById(file_path).focus();
						alert("<?=$ec_iPortfolio['upload_error']?>");
						return false;
					}
					if(!path.charAt(1).match(/^[:]/) || !path.charAt(2).match(/^[\/\\]/))
					{
						document.getElementById(file_path).focus();
						alert("<?=$ec_iPortfolio['upload_error']?>");
						return false;
					}
				}
			}
		}
	}

<?if(empty($programID)){ ?>
	<?if(in_array('sub_category',$requreJSArr)){?>
		if($("select[name=subCategory]").val() == -9){
			alert('<?=$ec_iPortfolio['SLP']['PleaseSelect'].' '.$ec_iPortfolio['sub_category']?>');
			return false;
		}
	<?}?>
	<?if(in_array('ele',$requreJSArr)){?>
		if($('input[name=' + 'ele[]' + ']:checked').length < 1 ){
			alert('<?=$ec_iPortfolio['SLP']['PleaseFillIn'].' '.$ec_iPortfolio['ele']?>');
			return false;
		}
	<?}?>
		<?if(in_array('organization',$requreJSArr)){?>
		if($("input[name=organization]").val() == ''){
			alert('<?=$ec_iPortfolio['SLP']['PleaseFillIn'].' '.$ec_iPortfolio['organization']?>');
			return false;
		}
	<?}?>
	<?if(in_array('hours',$requreJSArr)){?>
		if($("input[name=hours]").val() ==0){
			alert('<?=$ec_iPortfolio['SLP']['PleaseFillIn'].' '.$ec_iPortfolio['hours']?>');
			return false;
		}
	<?}?>
	<?if(in_array('ole_role',$requreJSArr)){?>
		if($("input[name=ole_role]").val()==''){
			alert('<?=$ec_iPortfolio['SLP']['PleaseFillIn'].' '.$ec_iPortfolio['ole_role']?>');
			return false;
		}
	<?}?>
	<?if(in_array('achievement',$requreJSArr)){?>
		if($("input[name=achievement]").val()==''){
			alert('<?=$ec_iPortfolio['SLP']['PleaseFillIn'].' '.$ec_iPortfolio['achievement']?>');
			return false;
		}
	<?}?>
	<?if(in_array('attachment',$requreJSArr)){?>
		var targetFileUpload = $("input[name^=ole_file]");
			var allFileString = "";
			targetFileUpload.each(function() {
			allFileString = allFileString + $(this).val();
		});
		if ( allFileString=="" ) {
			alert('<?=$ec_iPortfolio['SLP']['PleaseFillIn'].' '.$ec_iPortfolio['attachment']?>');
			return false;
		}
	<?}?>
	<?if(in_array('details',$requreJSArr)){?>
		if($("textarea[name=details]").val()==''){
			alert('<?=$ec_iPortfolio['SLP']['PleaseFillIn'].' '.$ec_iPortfolio['details']?>');
			return false;
		}
	<?}?>
	<?if(in_array('preferred_approver',$requreJSArr)){?>
		if( Number($("select[name=request_approved_by]").val())==''){
			alert('<?=$ec_iPortfolio['SLP']['PleaseSelect'].' '.$ec_iPortfolio['preferred_approver']?>');
			return false;
		}
	<?}?>
<?} ?>

	<?=$requiredFieldJS?>
	<?=$cpsry_hours_jsChecking?>
	<?=$cpsry_role_jsChecking?>
	<?=$cpsry_achievement_jsChecking?>
	<?=$cpsry_attachment_jsChecking?>
	<?=$cpsry_details_jsChecking?>
	<?=$cpsry_approved_by_jsChecking?>

	// handle special Chinese filename
	//var attachment_size = document.getElementById('attachment_size').value;
	Big5FileUploadHandler(formObj, 'ole_file', attachment_size);
	
	document.getElementsByName('category').disabled = false;
	for(j=0; j<document.form1.length; j++)
	{
		if(document.form1.elements[j].name == 'ele[]')
		{
			document.form1.elements[j].disabled = false;
		}
	}
	/*
	//check ele
	var ele_checked = $("input[name^='ele']:checked").size();   

		if (ele_checked == 0) 
		{ 
			alert("");
			return false; 
		} 
	*/
	return true;
}

function jPRESET_ELE(i_pos, j_pos)
{
	var formObj = document.form1;
	var ELEObj = formObj.elements["ele[]"];
	var len = ELEObj.length;
	var jPresetELEList = jCurrentELEArr[i_pos][j_pos];

	for(var m=0; m<len; m++)
	{
		if(typeof(jPresetELEList)!="undefined" && jPresetELEList.indexOf(ELEObj[m].value)!=-1) {
			ELEObj[m].checked = 1;
		}
		else {
			ELEObj[m].checked = 0;
		}
	}
}

function jChangeSubmitType(obj)
{
	var row1 = document.getElementById("row_ele");
	var row2 = document.getElementById("row_hour");
	var div1 = document.getElementById("div_org");
	
	// external
	if(obj.value == "EXT")
	{
		row1.style.display = "none";
		row2.style.display = "none";
		div1.innerHTML = "<?=$iPort["external_ole_report"]["organization"]?>";
	}
	else
	{
		// internal
		row1.style.display = "block";
		row2.style.display = "block";
		div1.innerHTML =  "<?=$ec_iPortfolio['organization']?>";
	}
}

function jRESET_JOIN()
{
	document.form1.reset();
	// Category
	document.getElementsByName('category')[0].disabled = false;
	// Title
	document.getElementsByName('title')[0].readOnly = false;
	document.getElementById('listContent').style.display = 'block';
	document.getElementById('change_preset').style.display = 'block';
	// Start Date
	document.getElementsByName('startdate')[0].readOnly = false;
	document.getElementById('calendar_img').style.display = 'block';
	// End Date
	document.getElementById('table_enddate').style.display = 'none';
	document.getElementById('add_link').style.display = 'block';
	document.getElementsByName('enddate')[0].readOnly = false;
	document.getElementById('calendar_img1').style.display = 'block';
	// ELE
	for(j=0; j<document.form1.length; j++)
	{
		if(document.form1.elements[j].name == 'ele[]')
		{
			document.form1.elements[j].checked = false;
			document.form1.elements[j].disabled = false;
		}
	}
	// Role
	document.getElementById('change_preset2').style.display = 'block';
	// Organization
	document.getElementsByName('organization')[0].readOnly = false;
	// Details
	document.getElementsByName('details')[0].readOnly = false;

	document.getElementsByName('ProgramID')[0].value = "";
}



function disableField(obj) {
	switch (obj.name)
	{
		case 'title':
			document.getElementById('change_preset').style.display = 'none'; // Max checking
			break;
		case 'ole_role':
			document.getElementById('change_preset2').style.display = 'none';
			break;
		case 'startdate':
			document.getElementById('startdatecal').style.display = 'none';
			break;
		case 'enddate':
			document.getElementById('enddatecal').style.display = 'none';
			break;
		default:
			break;
	}
	obj.setAttribute('disabled', 'true');
}


function enableField(obj) {
	switch (obj.name)
	{
		case 'title':
			document.getElementById('change_preset').style.display = '';
			break;
		case 'ole_role':
			document.getElementById('change_preset2').style.display = '';
			break;
		case 'startdate':
			document.getElementById('startdatecal').style.display = '';
			break;
		case 'enddate':
			document.getElementById('enddatecal').style.display = '';
			break;
		default:
			break;
	}
	obj.removeAttribute('disabled');
}
function changeSubCategory() {
	var catID = $("select[name='category']").val();
	if (catID==null) {return false;}
	$("[id*=listContent]").hide();
	$("#subCategorySelectionField").html("<?=$iPort['loading']?>...");
	$.ajax({
		type: "GET",
		url: "../ajax/ajax_reload_slp.php",
		async: false,
		data: "ActionType=Change_Sub_Category&CategoryID="+catID+"&SubCategoryID=<?=$subCategoryID?>",
		success: function (xml) {
//		alert(xml);
			$("#subCategorySelectionField").html($(xml).find("subCategorySel").text());
		}
	});
	// reset the current array for preset programme names and the corresponding ele array
	jCurrentArr = new Array(0);
	jCurrentELEArr = new Array(0);
}

function setTitleLayer(parTitle) {
	var parTitle = parTitle || '';
	
	$("[id*=listContent]").hide();
	$("#change_preset").hide();
	$("#change_preset_loading").show();
	var catID = $("select[name='category']").val();
	var subCatID = $("select[name='subCategory']").val();
	var urlString = "../ajax/ajax_reload_slp.php";
	var dataString = "";
	var target_count = "";
	if (subCatID == null || subCatID == -9) {
		dataString = "ActionType=Get_Program_Name_And_Ele&CategoryID="+catID;
	} else {
		dataString = "ActionType=Get_Program_Name_And_Ele&SubCategoryID="+subCatID;
	}
//	alert(urlString+"?"+dataString);
	$.ajax({
		type: "GET",
		url: urlString,
		data: dataString,
		success: function (xml) {
			// parent node is <elements>
			child = $(xml).find("programNameElements");
			
			// initialize the current array for preset programme names and the corresponding ele array
			jCurrentArr = new Array(child.size());
			jCurrentELEArr = new Array(child.size());
			
			arr_pos_count = 0;
			child.each(function() {
				var title = $(this).find("title").text();
				var ele = $(this).find("ele").text();
				
				jCurrentArr[arr_pos_count] = new Array(1);
				jCurrentELEArr[arr_pos_count] = new Array(1);
				
				jCurrentArr[arr_pos_count][0] = title;
				jCurrentELEArr[arr_pos_count][0] = ele;
				
				// Return ELE Component index when parTitle equal to obtained title 
				if(parTitle == title){
					target_count = arr_pos_count;	
				}
				
				arr_pos_count = arr_pos_count + 1;
			});
			
			$("#change_preset").show();
			$("#change_preset_loading").hide();
		}
	});
	
	return target_count;
}

$("document").ready(function(){
	// initialize sub category
	changeSubCategory();
	setTitleLayer();
//	setDefaultApprover();
	
	$.ajaxSetup({	
		cache:false, async:false
	});
	
	$('input').keydown(function(e) {
		if (event.which == 13) {
			event.preventDefault();
			return false;
		}
	});
	
	if($("#title").length > 0){
		// Autocomplete of title 
		$("#title").autocomplete(
	      "ajax_ole_title_suggestion.php",{
	     	 onItemSelect: function(li){
	   	
	      		// Data: Title, Category, SubCategory
	      		var $dataAry1 = $("#title").val().split('<?=' ( '. $ec_iPortfolio['SLP']['Category'].' : '?>');
	      		var $dataAry2 = $dataAry1[1].split('<?=' - '. $ec_iPortfolio['SLP']['SubCategory'].' : '?>');
	      		var $title = Trim($dataAry1[0]);
	      		if(typeof $dataAry2[1] === "undefined"){
	      			var $catSelect = Trim(Trim($dataAry2[0]).slice(0,-1));
	      			var $subCatSelect = null;
	      		} else {
	      			var $catSelect = Trim($dataAry2[0]);
	      			var $subCatSelect = Trim(Trim($dataAry2[1]).slice(0,-1));
	      		}
	      			
	      		// Response
	      		// Textfield: Title
	      		$("#title").val($title);
	      			
	      		// Drop Down List: Category
	      		$("select[name=category] option").filter(function() {
    				if(this.text == $catSelect){
    					// Selected if equal to $catSelect
    					this.selected=true;
    				}
				});
				// Refresh
				changeSubCategory(); 
				setTitleLayer();
					
				// Drop Down List: SubCategory
				$("select[name=subCategory] option").filter(function() {	
    				if(this.text == $subCatSelect){
    					this.selected=true;
    				}
				});
				
				// Refreah + Get related ELE Component
				var locatCount = setTitleLayer($title);
				if(typeof(jPresetELE)!='undefined'){
					jPRESET_ELE(locatCount.toString(), '0');
				}
	      			
	      	}, 
	  		delay:3,
	  		minChars:1,
	  		matchContains:1,
	  		formatItem: function(row){ return row[0]; },
	  		autoFill:false,
	  		overflow_y: 'auto',
	  		overflow_x: 'hidden',
	  		maxHeight: '200px'
	  	}
	    );
	}
	
});


function jsCheckMultiple(obj)
{
<?php if ($sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme']) { ?>
	 if(countChecked(document.form1, 'ele[]') > 1)
	  {
	    alert("<?=$msg_check_at_most_one?>");
	    obj.checked = false;
	  }
<?php } ?>
}


</script>

<FORM name="form1" method="post" action="ole_update.php" enctype="multipart/form-data" onSubmit="return checkform(this)">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td >
	<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
		<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" border="0" align="absmiddle"> <?=$h_navigationWord?> </a>
		</td>
		<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
	</tr>
	</table>
<?=$WarningBox?>
	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center" >
	<tr>
		<td valign="top" >
		<!-- CONTENT HERE -->
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td width="80%" valign="top">
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			<!-- Type -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?php echo $iPort['submission_type']; ?></span>
				</td>
				<td width="80%" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="60%" >
				<tr>
					<td width="100%" ><?=$SubmissionType?></td>
				</tr>
				</table>
				</td>
			</tr>
			
			<!-- category -->
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?=$ec_iPortfolio['category']?><span class="tabletextrequire">*</span></span>
				</td>
				<td>
				<?=$categoryFieldDisplay?>
				</td>
			</tr>
			
			<!-- sub category -->
			<? //sub category ?>
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?=$ec_iPortfolio['sub_category']?><?=$requiredSetting['sub_category']?></span>
				</td>
				<td id='subCategorySelectionField'>
				<?=$subCategoryFieldDisplay?>
				</td>
			</tr>
			
			<!-- title -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?php echo $ec_iPortfolio['title']; ?><span class="tabletextrequire">*</span></span>
				</td>
				<td width="80%" valign="top">
				<?=$titleFieldDisplay?>
				</td>
			</tr>
			
			<!-- date -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?=$ec_iPortfolio['date']?><span class="tabletextrequire">*</span></span></td>
				<td>
				<?=$dateFieldDisplay?>
				</td>
			</tr>

			<!-- ELE -->
			<tr id="row_ele" valign="top" <?=$DisplayStyle?> >
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?=$ec_iPortfolio['ele']?><?=$requiredSetting['ele']?></span>
				</td>
				<td><?=$ELEList?></td>
			</tr>
			
			<!-- organization -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><div id="div_org"><?=$OrganizationText?><?=$requiredSetting['organization']?></div></span></td>
				<td><?=$organizationFieldDisplay?></td>
			</tr>
			
			<!-- Hours -->
			<tr id="row_hour" <?=$DisplayStyle?> >
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['hours']?><?=$requiredSetting['hours']?></span><?=$cpsry_hours_star?></td>
				<td><?=$hoursFieldDisplay?></td>
			</tr>
			
			<!-- role -->
			<!--
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['ole_role']?></span></td>
				<td><input name="role" type="text" size="50" maxlength="64" value="<?=$role?>" class="tabletext" ></td>
			</tr>
			-->
			
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?php echo $ec_iPortfolio['ole_role']; ?><?=$requiredSetting['ole_role']?></span><?=$cpsry_role_star?>
				</td>
				<td width="80%" valign="top">
				<?=$roleFieldDisplay?>
				</td>
			</tr>
			
			<!-- achievement -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['achievement']?><?=$requiredSetting['achievement']?></span><?=$cpsry_achievement_star?></td>
				<td><?=$achievementFieldDisplay?></td>
			</tr>
			<!-- attachment -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['attachment']?><?=$requiredSetting['attachment']?></span><?=$cpsry_attachment_star?></td>
				<td valign="top">
				<?=$attachmentFieldDisplay?>
				</td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['details']?><?=$requiredSetting['details']?></span><?=$cpsry_details_star?></td>
				<td><?=$detailFieldDisplay?></td>
			</tr>
<?
			//DISPLAY THIS SCHOOL REMARK DEPEND ON THE $displaySchoolRemark
			if($displaySchoolRemark != 0){
?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['school_remarks']?></span><?=$cpsry_details_star?></td>
					<td><?=$schoolRemarkFieldDisplay?></td>
				</tr>
<?
			}
?>

			
			<!-- HKUGAC Customization -->
<?php if (isset($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) && $sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) { ?>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><div id="div_org"><?=$Lang['iPortfolio']['OLE']['SAS_with_link']?></div></span></td>
				<td valign="top"><input id="IsSAS" name="IsSAS" type="checkbox" value="1" <?=$IsSASChecked?> /></td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><div id="div_org"><?=$Lang['iPortfolio']['OLE']['In_Outside_School']?></div></span></td>
				<td valign="top"><?= $linterface->Get_Radio_Button("InsideSchool", "InOutSideSchool", "0", $IsOutsideSchoolChecked[0], "", $Lang['iPortfolio']['OLE']['Inside_School'] ) ?>
				<?= $linterface->Get_Radio_Button("OutsideSchool", "InOutSideSchool", "1", $IsOutsideSchoolChecked[1], "", $Lang['iPortfolio']['OLE']['Outside_School'] ) ?></td>
			</tr>
<?php } ?>

			<?=$TeacherSelectionRow?>
			<!--
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['school_remarks']?>:</span></td>
				<td><?=$linterface->GET_TEXTAREA("SchoolRemarks", $SchoolRemarks);?></td>
			</tr>
			-->
			</table>
			</td>
		</tr>
		<tr>
			<td height="1" class="dotline" colspan="2"  ><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
		</tr>

		<tr>
			<td  colspan="2" align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="tabletextremark"><?=$ec_iPortfolio['mandatory_field_description']?></td>
				<td align="right">
				<? if(empty($warning_msg) || $action=="new") {?>
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "", $disabled) ?>
				<?= $linterface->GET_ACTION_BTN($button_reset, "button", "jRESET_JOIN()") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='{$back_url}'") ?>
				<? } else {?>
				<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='{$back_url}'") ?>
				<? } ?>
				</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
		<br>


		<!-- End of CONTENT -->
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>


<input type="hidden" name="ClassName" value="<?=$ClassName?>" >
<input type="hidden" name="StudentID" value="<?=$StudentID?>" >
<input type="hidden" id="attachment_size" name="attachment_size" value="0" >
<input type="hidden" name="RecordID" value="<?=$record_id?>" >
<input type="hidden" name="ProgramID" value="<?=$programID?>" >
<input type="hidden" name="attachment_size_current" value="<?=$attach_count?>" >
<input type="hidden" name="ApprovalSetting" value="<?=$ApprovalSetting?>" >
<input type="hidden" name="SelectedStatus" value="<?=$SelectedStatus?>" >
<input type="hidden" name="SelectedELE" value="<?=$SelectedELE?>" >
<input type="hidden" name="SelectedYear" value="<?=$SelectedYear?>" >
<input type="hidden" name="DefaultApprover_IsSelectedbyTeacher" value="<?=$DefaultApprover_IsSelectedbyTeacher?>" >
<?=$hiddenField_Approver?>

<input type="hidden" name="DefaultSubmitType" value="<?=$DefaultSubmitType?>" >
</form>



<?php
//This code is commented and use the js function availableFieldsSet to 
//set focus according to different input field disable status
//echo  $linterface->FOCUS_ON_LOAD("form1.title");
?>