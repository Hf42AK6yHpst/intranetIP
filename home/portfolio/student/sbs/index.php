<?php

# Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/libportfolio_group.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html("iportfolio_default4.html");
// set the current page title
$CurrentPage = "Student_SchoolBasedScheme";
$CurrentPageName = $iPort['menu']['school_based_scheme'];

### Get the library ###
$lgs = new growth_scheme();
$luser = new libuser($UserID);
$lpf = new libpf_sbs();
$lpf_ui = new libpf_sbs();
$lo = new libportfolio_group($lpf->course_db);

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

//////////////////////////////////////////////////////
////////////   GET THE PAGE CONTENT  /////////////////
//////////////////////////////////////////////////////
// page number settings
$order = ($order == 1) ? 1 : 0;

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if ($pageNo == "")
$pageNo = 1;

// ParArr settings
$ParArr["Role"] = "STUDENT";

if($ck_memberType == "P")
$ParArr["StudentID"] = $ck_current_children_id;
else
$ParArr["StudentID"] = $UserID;

//$ParArr["SchoolYear"] = $SchoolYear;
$ParArr["SchoolYear"] = GET_ACADEMIC_YEAR();

$keyword = trim($keyword);
$ParArr["keyword"] = $keyword;
$ParArr["NumOfCol"] = 5; // default phase number display in one row
$ParArr["page_size"] = $page_size;
$ParArr["pageNo"] = $pageNo;

$SchemeData = $lpf->GET_SCHOOL_BASED_SCHEME($ParArr);

$ParArr["SchemeData"] = $SchemeData;
$SchemeRecordArr = $lpf_ui->GET_SCHEME_TABLE($ParArr);
$SchemeRecord = $SchemeRecordArr["content"];
$SchemeRecordCount = $SchemeRecordArr["count"];

# TABLE SQL
//$user_field = getNameFieldWithClassNumberByLang("c.");
$ParArr["selectType"] = "count";
$sql = $lpf->GET_SCHOOL_BASED_SCHEME_BY_STUDENT_ID_QUERY($ParArr);

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("Num");
$li->sql = $sql;
$li->no_col = 1;
$li->record_num_mode = "iportfolio"; // for school based scheme total record display
    
$li->IsColOff = "iPortfolioSchoolBasedScheme";
$li->title = $iPort['table']['records'];

$Content = "";
$Content .= $lpf_ui->GET_SELECTION_SEARCH_TABLE($ParArr);
$Content .= $li->display($SchemeRecord, $SchemeRecordCount);
$Content .= "<br /><br /><br />";
//////////////////////////////////////////////////////
////////////   END GET THE PAGE CONTENT  /////////////
//////////////////////////////////////////////////////

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
<!--

function jChangeSchoolYear(obj)
{
	document.form1.pageNo.value = 1;
	document.form1.SchoolYear.value = obj.value;
	document.form1.submit();
}

function jSubmitForm()
{
	document.form1.submit();
}

//-->
</SCRIPT>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<!-- CONTENT HERE -->

<form name="form1" id="form1" action="index.php" method="post">
<?=$Content?>

<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

</td></tr></table>
</td></tr></table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>