<?php

$pdfConfig['ReportTitle'] = '幼兒學習總結評量';
$pdfConfig['ReportFooter'] = '(5) 總是做到&nbsp;&nbsp;&nbsp;(4) 經常做到&nbsp;&nbsp;&nbsp;(3) 間中做到&nbsp;&nbsp;&nbsp;(2) 偶爾做到&nbsp;&nbsp;&nbsp;(1) 仍需努力&nbsp;&nbsp;&nbsp;(-) 不適用';
$pdfConfig['ReportDefaultRowCount'] = 40;
