<?php
// Using: 
/**
 * Change Log:
 * 2020-01-10 (Philips) [2020-0103-1738-02207]
 *  -	Use harts_logo.png instead of harts_logo.jpg
 * 2019-10-31 (Bill)    [2019-1030-1336-14235]
 *  -   Get target year class name and class number
 * 2018-06-01 (Anna)
 *  -   Added comments: and comment: when mapping comment 
 * 2016-07-19 (Omas) 
 * 	-	Replace split to explode for PHP5.4
 * 2016-05-10 (Pun) [90315]
 * 	-	Change student photo size to 32mm x 42mm
 * 	-	Fix student photo will be displayed in print with PDF 
 * 2016-04-22 (Pun) [90315]
 * 	-	Change student photo size to 32mm x 42mm
 * 2016-04-18 (Pun) [90315]
 * 	-	Hide student photo, change header wording
 * 2016-03-08 (Pun)
 * 	-	New File, copy from print_pdf_chiuchunkg_print.php
 */

ini_set('memory_limit','128M');

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

iportfolio_auth("SPTA");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

include_once($PATH_WRT_ROOT.'kis/init.php');
$libkis_iportfolio = $libkis->loadApp('iportfolio');

intranet_opendb();


######## Init START ########
$lpf = new libpf_sbs();
$lgs = new growth_scheme();

$mPDF = new mPDF('','A4',0,'',15,15,5,5);

$assignment_id = IntegerSafe($assignment_id);
$phase_id = IntegerSafe($phase_id);

$userIdArr = array();
if($print_all){
	$rs = $lgs->getSchemeUsers($assignment_id);
	if($Class){
		$rs = $lgs->getUserInfo($rs);
		foreach($rs as $r){
			list($ClassName, $ClassNo) = explode('-', $r['class_number']);
			$ClassName = trim($ClassName);
			if($Class == $ClassName){
				$userIdArr[] = $r["user_id"];
			}
		}
	}else{
		$userIdArr = $rs;
	}
}else{
	if($ck_memberType == "P"){
		$user_id = $lpf->getCourseUserID($ck_current_children_id);
	}
	if($user_id==""){
		$user_id = $lpf->getCourseUserID($UserID);
	}
	$userIdArr[] = $user_id;
}
######## Init END ########


######## Access Right START ########
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

if($print_all){
	if($ck_memberType != "T") {
		No_Access_Right_Pop_Up();
		exit;
	}
}else{
	if (!in_array($user_id, $lgs->getSchemeUsers($assignment_id))) {
		No_Access_Right_Pop_Up();
	}
}
######## Access Right END ########


######## Get Term Info START ########
$ayt = new academic_year_term($school_year_term_id);
$termName = $ayt->YearTermNameEN;
$yearName = $ayt->YearNameEN;
$yearId = $ayt->AcademicYearID;
######## Get Term Info END ########


######## Filter only have data User START ########
$phase_obj = $lgs->getPhaseInfo($phase_id);
$phase_obj = $phase_obj[0];

$userIdList = implode("','", $userIdArr);
$sql = "
SELECT
	user_id
FROM
	handin
WHERE
	assignment_id='{$phase_obj["assignment_id"]}' 
	AND user_id IN ('{$userIdList}')
	AND type='sbs'";
$rs = $lgs->returnVector($sql);
$userIdList = implode("','", $rs);
######## Filter only have data User END ########


######## Student Info START ########
$nameSql = getNameFieldByLang2("iu.");
$sql = "
SELECT 
	iu.UserID,
	{$nameSql} as Name,
	iu.ChineseName,
	iu.EnglishName,
	iu.ClassName,
	iu.ClassNumber,
	DATE(iu.DateOfBirth) as birthday,
	iu.Gender as gender,
	iu.STRN,
	um.user_id
FROM 
	{$lpf->course_db}.usermaster um
INNER JOIN 
	{$intranet_db}.INTRANET_USER iu
    ON um.user_email = iu.UserEmail
WHERE
	um.user_id IN ('{$userIdList}')
ORDER BY
	iu.ClassName, LENGTH(iu.ClassNumber), iu.ClassNumber ";
	$rs = $lpf->returnResultSet($sql);

$sql = "
SELECT
    ycu.UserID,
    yc.ClassTitleEN as ClassName,
    yc.ClassTitleB5,
    ycu.ClassNumber
FROM
    {$intranet_db}.YEAR_CLASS_USER ycu
INNER JOIN
	{$intranet_db}.YEAR_CLASS yc
    ON ycu.YearClassID = yc.YearClassID
WHERE 
    yc.AcademicYearID = '{$yearId}' ";
$userClassInfo = $lpf->returnResultSet($sql);
$userClassInfo = BuildMultiKeyAssoc($userClassInfo, 'UserID');
######## Student Info END ########

$html = '';

if(count($rs) == 0){
	ob_start();
?>

	<style>
		@page{
			/*background-image: url("/file/test/PortfolioData.jpg");*/
		}
		body{
			font-family: msjh !important;
		}
		hr {
			color: black;
			background-color: black;
			height: 3px;
			margin-top: 1mm;
			margin-mottom: 0;
		}
	</style>
	
	<h1><?=$Lang['SysMgr']['Homework']['NoRecord'] ?></h1>
<?php
	$html = ob_get_clean();
	$mPDF->WriteHTML($html);
	$mPDF->Output();
	exit;
}

foreach($rs as $r)
{
	$user = $r;
	// debug_r($user);
	
	$studentId = $user['UserID'];
	$user_id = $user['user_id'];
	$studentName = $user['Name'];
	$studentChineseName = $user['ChineseName'];
	$studentEnglishName = $user['EnglishName'];
	// $studentClass = $user['ClassName'];
	// $studentNumber = $user['ClassNumber'];
	$studentDOB = date("d/m/Y", strtotime($user['birthday']));
	$studentGender = $user['gender'];
	$studentSTRN = $user['STRN'];
	$studentAge = floor((time() - strtotime($user['birthday'])) / 31556926);
	
	// [2019-1030-1336-14235] Get target year class name and class number
	if(isset($userClassInfo[$studentId])) {
	    $studentClass = $userClassInfo[$studentId]['ClassName'];
	    $studentNumber = $userClassInfo[$studentId]['ClassNumber'];
	}
	
	# Set links for photo
	/* Don't show student photo* /
	$student_obj = $lpf->GET_STUDENT_OBJECT($studentId);

	if(strpos($student_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $student_obj['PhotoLink'] != "")
		$studentPhotoSrc = str_replace("<!--ImageStyle-->", "", $student_obj['PhotoLink']);
	else if($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)
		$studentPhotoSrc = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
	else
		$studentPhotoSrc = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
	/* */
	//$studentPhotoSrc = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" style=\"visibility: hidden; width: 35mm; height: 40mm;\" />";
	######## Student Info END ########
	
	
	######## Get data START ########
	$handin_obj = $lgs->getStudentPhaseHandin($user_id, $phase_obj);
	######## Get data END ########
	
	
	######## Decode Ansersheet START ########
	$answersheetArr = explode('#QUE#', $phase_obj['answersheet']);
	array_shift($answersheetArr);
	
	$commentID = -1;
	$promotedToID = -1;
	$subjectArr = array();
	
	for($i=0,$iMax=count($answersheetArr);$i<$iMax;$i++){
		$_questions = explode('||', $answersheetArr[$i]);
		$title = $_questions[1];
		
		if(strtoupper($_questions[1]) == 'COMMENTS' ||strtoupper($_questions[1]) == 'COMMENTS:' ||		    
		    strtoupper($_questions[1]) == 'COMMENT' || strtoupper($_questions[1]) == 'COMMENT:'){
			$commentID = $i;
		}else if($i == $iMax-1){
			$promotedToID = $i;
		}else{
			$subjectArr[] = nl2br($title);
		}
	}
	######## Decode Ansersheet END ########
	
	
	######## Decode Anser START ########
	$_answerArr = explode('#ANS#', $handin_obj['answer']);
	array_shift($_answerArr);
	$answerArr = array();
	$comment = "";
	$promotedTo = "";
	
	for($i=0,$iMax=count($_answerArr);$i<$iMax;$i++){
		$_ans = $_answerArr[$i];
		if($i == $commentID){
			$comment = nl2br($_ans);
		}else if($i == $promotedToID){
			$promotedTo = nl2br($_ans);
		}else{
			$answerArr[] = $_ans;
		}
	}
	######## Decode Anser END ########
	
	
	######## UI START ########
	$barGroup = array(
		'Room For Improvement',
		'Fair',
		'Good',
		'Very Good',
		'Excellent',
	);
	
	$barChartTitleWidth = 50; // mm
	$barChartBarWidth = 125; // mm
	$barChartSingleWidth = $barChartBarWidth / count($barGroup);
	######## UI END ########
	
	
	######## Output START ########
	ob_start();
	if($html != ''){
		echo '<pagebreak />';
	}
?>
	<style>
		@page{
			/*background-image: url("/file/test/PortfolioData.jpg");*/
		}
		body{
			font-family: msjh !important;
		}
		hr {
			color: black;
			background-color: black;
			height: 3px;
			margin-top: 1mm;
			margin-mottom: 0;
		}
		.test{
			border: 1 solid grey; 
		}
	</style>
	
	&nbsp;
	<table>
		<tr>
			<td style="width: 50%">
				<table class="portfolioTable" style="font-size: 0.75em; line-height: 0.7; color: grey;">
					<tr>
						<td>
							HARTS Preschool / Playschool
						</td>
					</tr>
					<tr>
						<td>
							 樂希幼兒學校/幼兒樂園
						</td>
					</tr>
					<tr>
						<td>
							 G/F & Whole 1/F, High Park Grand,
						</td>
					</tr>
					<tr>
						<td>
							68 Boundary Street, Kowloon Hong Kong
						</td>
					</tr>
					<tr>
						<td>
							九龍界限街68號曉珀．御地下及一樓全層
						</td>
					</tr>
					<tr>
						<td>
							www.hartspreschool.com
						</td>
					</tr>
					
				</table>
			</td>
			<td style="width: 30%">&nbsp;</td>
			<td style="border: 0 solid black;">
				<img src="<?=$PATH_WRT_ROOT ?>/file/customization/hartspreschool/sbs_report/harts_logo.png" style="height: 21mm;width: 34mm" />
			</td>
		</tr>
	</table>
	
	<style>
	.fillLine{
		width: 25mm;
		border-bottom: 1px solid black;
		text-align:center;
	}
	</style>
	
	<table class="portfolioTable" style="width: 100%">
		<colgroup>
			<col style="width: 25%"/>
			<col style="width: 25%"/>
			<col style="width: 25%"/>
			<col style="width: 25%"/>
		</colgroup>
		
		<tr>
			<td colspan="3" style="font-size: 0.1em;height: 15px">&nbsp;</td>
			<td rowspan="5" style="border: 1px solid black;width: 37mm; height: 45mm;">
				<?=$studentPhotoSrc ?>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="font-size: 1.4em; font-weight: bold; text-align: center;">
				<!-- 
				<?=$termName ?><br />
				<?=$yearName?>
				-->
				Student Report Card<br />
				<?=$termName ?>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="font-size: 1.3em; text-align: center;">
				<?=$studentChineseName?><br />
				<?=$studentEnglishName?><br />
			</td>
		</tr>
		<tr>
			<td>
				<table style="width: 80%;">
					<tr>
						<td style="white-space: nowrap;">Student No.:</td>
						<td class="fillLine"><?=$studentSTRN ?></td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</td>
			<td>
				<table style="width: 80%;">
					<tr>
						<td style="white-space: nowrap;">Class:</td>
						<td class="fillLine"><?=$studentClass ?></td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
				<table style="width: 80%;">
					<tr>
						<td style="white-space: nowrap;">Date of Birth:</td>
						<td class="fillLine"><?=$studentDOB ?></td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</td>
			<td>
				<table style="width: 80%;">
					<tr>
						<td style="white-space: nowrap;">Age:</td>
						<td class="fillLine"><?=$studentAge ?></td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</td>
			<td>
				<table style="width: 80%;">
					<tr>
						<td style="white-space: nowrap;">Sex:</td>
						<td class="fillLine"><?=$studentGender ?></td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
	<hr />
	
	<!-- -------------------- Table START -------------------- -->
	
	<style>
	.chartTable{
		border: 0px solid black;
		margin: 0;
	}
	.title{
		width: <?=$barChartTitleWidth ?>mm;
		border-right: 2px solid black;
		font-size: 0.8em;
	}
	.bar{
		background-color: #222A67;
		font-size: 0.7em;
	}
	.barPadding{
		font-size: 0.5em;
	}
	.xAxis{
		text-align: right;
		width: <?=$barChartSingleWidth ?>mm;
	}
	</style>
	
	<?php 
	foreach($subjectArr as $subjectId => $subjectTitle){ 
		$answer = $answerArr[$subjectId];
		$answer = ($answer > 5)? 5 : $answer;
		$barWidth = $answer * $barChartBarWidth / count($barGroup);
		$barVisible = ($barWidth)? '' : 'background-color: #FFF;';
	?>
		<table class="chartTable">
			<tr>
				<td rowspan="3" class="title">
					<?=$subjectTitle ?>
				</td>
				<td class="barPadding">&nbsp;</td>
			</tr>
			<tr>
				<td class="bar" style="width: <?=$barWidth ?>mm; <?=$barVisible ?>">&nbsp;</td>
				<td class="barPadding">&nbsp;</td>
			</tr>
			<tr>
				<td class="barPadding">&nbsp;</td>
			</tr>
		</table>
	<?php 
	} 
	?>
	
	<table class="chartTable" style="margin: 0">
		<tr>
			<td style="width: <?=$barChartTitleWidth ?>mm">&nbsp;</td>
			<td style="border-top:1px solid black;width: <?=$barChartBarWidth ?>mm">&nbsp;</td>
		</tr>
	</table>
	
	<table class="chartTable" style="margin-top:-7mm;margin-left: 2mm">
		<tr>
			<td style="width: <?=$barChartTitleWidth ?>mm;text-align: right;">0</td>
			<td class="xAxis">1</td>
			<td class="xAxis">2</td>
			<td class="xAxis">3</td>
			<td class="xAxis">4</td>
			<td class="xAxis">5</td>
		</tr>
	</table>
	
	<hr />
	
	<style>
	.aXis2 {
		margin: 0;
		margin-top: -1mm;
		border-collapse:collapse;
		font-size: .5em;
		text-align: center;
		vertial-align: middle;
	}
	.leftBold {
		border-left:2px solid black;
		width: <?=$barChartSingleWidth ?>mm;
	}
	.topLine {
		border-top:1px solid black;
		text-align: center;
	}
	</style>
	
	<table class="chartTable aXis2" style="">
		<tr>
			<td style="width: <?=$barChartTitleWidth ?>mm;font-size: 2em" rowspan="3">Code:</td>
			<td class="leftBold">&nbsp;</td>
			<td class="leftBold">&nbsp;</td>
			<td class="leftBold">&nbsp;</td>
			<td class="leftBold">&nbsp;</td>
			<td class="leftBold" style="border-right:2px solid black;">&nbsp;</td>
		</tr>
		<tr>
			<td class="leftBold topLine">&nbsp;</td>
			<td class="leftBold topLine">&nbsp;</td>
			<td class="leftBold topLine">&nbsp;</td>
			<td class="leftBold topLine">&nbsp;</td>
			<td class="leftBold topLine" style="border-right:2px solid black;">&nbsp;</td>
		</tr>
		<tr>
			<?php foreach($barGroup as $group){ ?>
			<td style="width: <?=$barChartSingleWidth ?>mm;font-size: 1.5em"><?=$group ?></td>
			<?php } ?>
		</tr>
	</table>
	
	<!-- -------------------- Table END -------------------- -->
	
	
	<!-- -------------------- Footer START -------------------- -->
	
	<table style="margin-top: 5mm;vertical-align: top;">
		<tr>
			<td style="width: <?=$barChartTitleWidth ?>mm;text-align: center; height: 15mm;">Comments: </td>
			<td><?=$comment ?></td>
		</tr>
	</table>
	
	<table style="margin-top: 0mm">
		<?php if($promotedTo){ ?>
		<tr>
			<td style="width: <?=$barChartTitleWidth ?>mm;text-align: center;vertical-align: top;">Promoted To: </td>
			<td><?=$promotedTo ?></td>
		</tr>
		<?php }else{ ?>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<?php } ?>
	</table>
	
	<table style="margin-top: 20mm;width: 100%">
		<tr>
			<td class="topLine">Headmistress' Signature</td>
			<td>&nbsp;</td>
			<td class="topLine">Class Teachers' Signature</td>
			<td>&nbsp;</td>
			<td class="topLine">Parent's Signature</td>
		</tr>
	</table>
	
	<!-- -------------------- Footer END -------------------- -->
<?php
	$html = ob_get_clean();
	$mPDF->WriteHTML($html);
} // End foreach($rs as $r)

$mPDF->Output();

exit;
###################### END ########################

?>