<?php

// using: 
/**************************************************
 * Date:	2013-03-21	(Rita) 
 * Details:	Create this page
 **************************************************/
 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

if($special_feature['download_attachment_add_auth_checking'])
{
	intranet_auth();
}

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_opendb();

$LibPortfolio = new libpf_student();

# Temp Assign memory of this page
ini_set("memory_limit", "999M"); 

$recordId = $_GET['recordId'];
$target_e = trim($_GET['target_e']);
$target = (isset($target_e) && $target_e!="") ? getDecryptedText($target_e, $ipf_cfg['dynReport']['encryptKey']): stripslashes($target);


# Check if same user retrieves this record
$sql = $LibPortfolio->Get_Student_DynamicReport_DBTable_Sql($_SESSION['UserID'], $recordId);
$resultArray = $LibPortfolio->db->returnArray($sql);
$numOfResultArray = count($resultArray);


# Real Report Title
$real_filename = $resultArray[0]['ReportName'];

### Check user's browser ###
$useragent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
    $browser_version=$matched[1];
    $browser = 'IE';
} elseif (preg_match( '|Opera ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
    $browser_version=$matched[1];
    $browser = 'Opera';
} elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
	$browser_version=$matched[1];
	$browser = 'Firefox';
} elseif(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
	$browser_version=$matched[1];
	$browser = 'Safari';
} else {
	// browser not recognized!
    $browser_version = 0;
    $browser = 'other';
}

if(isset($userBrowser)) {
	# Play video file in new window on iPad
	$platform = $userBrowser->platform;
	if($platform == "iPad" && isVideoFile($target)) {
		header("Content-Type: text/html;charset=".returnCharset());
		$x = '<script type="text/javascript" language="JavaScript">'."\n";
		$x.= 'window.location.href=\''.str_replace($file_path,"",$target).'\';';
		$x.= '</script>'."\n";
		
		echo $x;
		exit;
	}
}

$content = get_file_content($target);

if($numOfResultArray>0){
	output2browser($content,$real_filename);
}

intranet_closedb();
?>
