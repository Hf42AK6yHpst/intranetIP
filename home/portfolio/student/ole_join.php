<?php
## Using By : 
##########################################################
## Modification Log
##	2016-10-06 (Villa) A96822 
##	- modified $sql - remove OP.PROGRAMTYPE = 'T' in order show the record created by student and can be joined by the approvel from teacher
##
##	2015-05-20 (Omas)
##	- remove empty object prevent warning - PHP5.4
##
##	2014-12-23 (Bill)
##	- Check OLE period settings of forms in DB
##
## 2013-02-22: YatWoon
## - update the remark, according to the submission period
## 2010-05-04: FAI
## - change sql to indicate it is a teacher created program to allow student to join "&p_type=sr"
## 
## 2010-02-19: Eric (201002191629)
## - Change academic year display and filtering by ID
## 2010-02-01: Max (201002011713)
## - Add new button to go to the new page
##########################################################
/** 
 * Type			: Enhancement
 * Date 		: 200911230911
 * Description	: Teacher role
 * 				  1) Add fields [Allow student to join], [Join period]
 * 				  Student role
 * 				  2C) Allow students to see the list of Student Learning Profile(SLP) to join, show within editable period and type == 1
 * By			: Max Wong
 * Case Number	: 200911230911MaxWong
 * C=CurrentIssue 
 * ----------------------------------
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_period_settings.php");

intranet_auth();
intranet_opendb();

$AcademicYearID = IntegerSafe($_GET['AcademicYearID']);
$category = IntegerSafe($_GET['category']);
$status = IntegerSafe($_GET['status']);
$num_per_page = $_GET['num_per_page'];
$pageNo = $_GET['pageNo'];
$order = $_GET['order'];
$field = $_GET['field'];
$page_size_change = $_GET['page_size_change'];
$numPerPage = $_GET['numPerPage'];
$IntExt = $_GET['IntExt'];

// Initializing classes
$LibUser = new libuser($UserID);
//debug($UserID);
$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$StudentLevel = $LibPortfolio->getClassLevel($LibUser->ClassName);
//$LibPortfolio->accessControl("ole");

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
if ($ck_memberType=="S")
	$CurrentPage = "Student_OLE";
else if ($ck_memberType=="P")
	$CurrentPage = "Parent_OLE";
$CurrentPageName = $iPort['menu']['ole'];

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR();

	
$ELEArray = $LibPortfolio->GET_ELE();


//$vars = "ClassName=$ClassName&StudentID=$StudentID";
if ($ck_memberType=="S")
{
	list($ClassName, $StudentID) = $LibPortfolio->GET_STUDENT_ID($ck_user_id);

	# define the navigation
	$template_pages = Array(Array($ec_iPortfolio['ole'], ""));

}
else if ($ck_memberType=="P")
{
	list($ClassName, $StudentID) = $LibPortfolio->GET_STUDENT_ID($LibPortfolio->IP_USER_ID_TO_EC_USER_ID($ck_current_children_id));

	# define the navigation
	$template_pages = Array(Array($ec_iPortfolio['ole'], ""));

}
else
{
	header ("Location: ".$PATH_WRT_ROOT."home/portfolio/index.php" );
}

# Load the can join list
#---------------------------
#! 1. Retrieving useful parameters from previous page
$order = $_GET['order']; # 0 -> DESC, 1 -> ASC, need to config in function displayFormat_iPortfolio_OLE_Join() inside libdbtable2007a.php
$field = $_GET['field']; # The field used to order 0 -> Title, 1 -> School Year, 2 -> Category, 3 -> Period Start, 4 -> Period End. This number is according to the field_array order

if ($order=="") {
	$order = 0; # Set the default order in DESC
}
if ($field=="") {
	$field = 3; # Set the default order by 3 -> Period Start
}
// $li->order = $order; # Keep the order to next page(s)
// $li->field = $field; # Keep the field to next page(s)

#! 2. Create a new table
$LibTable = new libdbtable2007($field, $order, $pageNo);
$LibTable->order = $order; # Keep the order to next page(s)
$LibTable->field = $field; # Keep the field to next page(s)

#! 2.1 Table's Caption
$LibTable->column_list .= "<tr class='tabletop'>\n";
$LibTable->column_list .= "<td height='25' align='center' >#</span></td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' width='32%' >".$LibTable->column(0,$ec_iPortfolio['SLP']['Title'], 1)."</td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' width='10%' >".$LibTable->column(1,$ec_iPortfolio['SLP']['SchoolYear'], 1)."</td>\n";
$LibTable->column_list .= "<td width='14%' >".$LibTable->column(2,$ec_iPortfolio['SLP']['Category'], 1)."</td>\n";
$LibTable->column_list .= "<td width='12%' >".$LibTable->column(3,$ec_iPortfolio['SLP']['PeriodStart'], 1)."</td>\n";
$LibTable->column_list .= "<td  nowrap='nowrap' width='12%' >".$LibTable->column(4,$ec_iPortfolio['SLP']['PeriodEnd'], 1)."</td>\n";
$LibTable->column_list .= "<td  nowrap='nowrap' width='16%' >".$LibTable->column(5,($ec_iPortfolio['SLP']['Status'] . " / " . $ec_iPortfolio['SLP']['ApprovedDate']), 1)."</td>\n";
$LibTable->column_list .= "</tr>\n";

$tableYearName = "AY.YEARNAMEEN";
($intranet_session_language == "en" ? $tableYearName : "AY.YEARNAMEB5");
$LibTable->field_array = array("OP.TITLE", $tableYearName, "OP.CATEGORY", "OP.CANJOINSTARTDATE", "OP.CANJOINENDDATE", "OSE.RECORDSTATUS");

#Please do not delete this part
#! 2.2 Table's Data Retrieval and predefined useful parameters for table generation
//if($AcademicYear!="")
//{
//	$yearConds .= " AND OP.STARTDATE BETWEEN '".$LowerDate."' AND '".$UpperDate."' ";
//}

if($AcademicYearID!="")
{
	$yearConds .= " AND OP.AcademicYearID = '" . $AcademicYearID . "' ";
}

# Please do not del this one, this is applicable for using academicid
//$sql = "
//SELECT
//	CONCAT(
//		'<a href=\"ole_new.php?',
//		IF ((SELECT DISTINCT OSI.PROGRAMID FROM/* Work around the stristr() function in libdbtable::built_sql() */
//				OLE_STUDENT OSI WHERE OSI.PROGRAMID = OP.PROGRAMID) IS NOT NULL,
//		CONCAT('record_id=', OSE.RECORDID),
//		CONCAT('programID=', OP.PROGRAMID)),
//		'\">',
//		OP.TITLE,
//		'</a>'
//	),
//	" . ($intranet_session_language == "en" ? "AY.YEARNAMEEN" : "AY.YEARNAMEB5") . ",
//	" . ($intranet_session_language == "en" ? "OC.ENGTITLE" : "OC.CHITITLE") . ",
//	IF (OP.CANJOINSTARTDATE='0000-00-00', ' -- ', OP.CANJOINSTARTDATE),
//	IF (OP.CANJOINENDDATE='0000-00-00', ' -- ', OP.CANJOINENDDATE),
//	CONCAT(CASE OSE.RECORDSTATUS
//	WHEN 1 THEN '".$ec_iPortfolio['pending']."'
//    WHEN 2 THEN '".$ec_iPortfolio['approved']."'
//    WHEN 3 THEN '".$ec_iPortfolio['rejected']."'
//	WHEN 4 THEN '".$ec_iPortfolio['teacher_submit_record']."'
//   	ELSE '--' END, IF (OSE.PROCESSDATE,CONCAT('<br>',DATE_FORMAT(OSE.PROCESSDATE,'%Y-%m-%d')),''))
//FROM $eclass_db.OLE_PROGRAM OP
//LEFT JOIN $intranet_db.ACADEMIC_YEAR AY ON OP.ACADEMICYEARID = AY.ACADEMICYEARID
//LEFT JOIN $eclass_db.OLE_CATEGORY OC ON OP.CATEGORY = OC.RECORDID
//LEFT JOIN $eclass_db.OLE_STUDENT OSE ON OSE.PROGRAMID = OP.PROGRAMID
//WHERE
//	OP.CANJOIN = 1
//AND
//	OP.PROGRAMTYPE = 'T'" . 
//	(($status=="") ? "" : (($status=="2") ? " AND OSE.RECORDSTATUS IN ('2','4')" : " AND OSE.RECORDSTATUS = '$status'")) .
//	(($category=="") ? "" : " AND OP.CATEGORY = '$category'") .  
//	(($AcademicYear == "") ? "" : " AND OP.ACADEMICYEARID = " . $AcademicYear . " ") . 
//"AND
//	OP.CANJOINSTARTDATE <= DATE(NOW())
//AND
//(
//    OP.CANJOINENDDATE >= DATE(NOW())
//  OR
//    OP.CANJOINENDDATE = '0000-00-00'
//)
//"; # SQL for each lines' data

//handle for search title
$search_text = trim($search_text);
if(trim($search_text) !="" && $search_text!=$ec_iPortfolio['enter_title_name'])
{
	$search_title_cond = " AND (OP.Title LIKE '%".$search_text."%')";
}

if ($IsSAS=="")
{
	# for all records
} elseif ($IsSAS=="1")
{
	$cond .= " AND OP.IsSAS='1' ";
} else
{
	$cond .= " AND OP.IsSAS='0' ";
}


if ($IsOutsideSchool=="")
{
	# for all records
} elseif ($IsOutsideSchool=="1")
{
	$cond .= " AND OP.IsOutsideSchool='1' ";
} else
{
	$cond .= " AND OP.IsOutsideSchool='0' ";
}

$sql = "
SELECT
	CONCAT(
		'<a class=\"tablelink\" href=\"ole_new.php?&p_type=sr&',
		IF ((OSE.RECORDID is null or OSE.RECORDID = 0),
		CONCAT('programID=', OP.PROGRAMID),
		CONCAT('record_id=', OSE.RECORDID)),
		'\">',
		OP.TITLE,
		'</a>'
	) as 'TITLE',
	IF (AY.AcademicYearID IS NOT NULL, " . Get_Lang_Selection("AY.YearNameB5", "AY.YearNameEN") . ", '--') AS StartYear,
	" . Get_Lang_Selection("OC.CHITITLE", "OC.ENGTITLE") . ",
	IF (OP.CANJOINSTARTDATE='0000-00-00', ' -- ', OP.CANJOINSTARTDATE),
	IF (OP.CANJOINENDDATE='0000-00-00', ' -- ', OP.CANJOINENDDATE),
	CONCAT(CASE OSE.RECORDSTATUS
	WHEN 1 THEN '".$ec_iPortfolio['pending']."'
    WHEN 2 THEN '".$ec_iPortfolio['approved']."'
    WHEN 3 THEN '".$ec_iPortfolio['rejected']."'
	WHEN 4 THEN '".$ec_iPortfolio['teacher_submit_record']."'
   	ELSE '--' END, IF (OSE.PROCESSDATE,CONCAT('<br>',DATE_FORMAT(OSE.PROCESSDATE,'%Y-%m-%d')),'')) as 'RECORDSTATUS'
FROM $intranet_db.YEAR_CLASS_USER YCU, $intranet_db.YEAR_CLASS YC, $eclass_db.OLE_PROGRAM OP
LEFT JOIN $eclass_db.OLE_CATEGORY OC ON OP.CATEGORY = OC.RECORDID
LEFT JOIN $eclass_db.OLE_STUDENT OSE ON (OSE.PROGRAMID = OP.PROGRAMID and OSE.UserID = ".$UserID.")
LEFT JOIN $intranet_db.ACADEMIC_YEAR AY ON (OP.AcademicYearID = AY.AcademicYearID)
WHERE
	OP.CANJOIN = 1
AND
	/*OP.PROGRAMTYPE = 'T' and */ OP.IntExt ='Int' " . 
	(($status=="") ? "" : (($status=="2") ? " AND OSE.RECORDSTATUS IN ('2','4')" : " AND OSE.RECORDSTATUS = '$status'")) .
	(($category=="") ? "" : " AND OP.CATEGORY = '$category'") . 
	(($subcategory=="") ? "" : " AND OP.SUBCATEGORYID = '$subcategory'") . $yearConds .
"AND
  YC.YEARCLASSID = YCU.YEARCLASSID AND
  YC.ACADEMICYEARID = '".Get_Current_Academic_Year_ID()."' AND
  YCU.USERID = '".$UserID."' AND
  INSTR(OP.JoinableYear, YC.YearID) > 0
AND
	OP.CANJOINSTARTDATE <= DATE(NOW())
AND
(
    OP.CANJOINENDDATE >= DATE(NOW())
  OR
    OP.CANJOINENDDATE = '0000-00-00'
) {$cond} ".$search_title_cond."
"; 

# SQL for each lines' data
//echo htmlspecialchars($sql);exit;


$LibTable->sql = $sql;
$LibTable->db = $eclass_db;
$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_col = (count($LibTable->field_array) + 1);
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($numPerPage=="") ? $page_size : $numPerPage;
// if ($page_size_change!="") $li->page_size = $numPerPage;
if ($page_size_change!="") $LibTable->page_size = $numPerPage;
	
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$LibTable->row_alt = array("#FFFFFF", "F3F3F3");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";
//$LibTable->IsColOff = "iPortfolio_OLE_Join"; # This is used to set with function to call to format the table in libdbtable2007a.php
$LibTable->IsColOff = 2;
	
if($ck_memberType=="S") {
	$LibTable->column_array = array(10,10,10,10,10,10);
} else {
	$LibTable->column_array = array(10,10,10,10,10,10);
} # The column_array is used to set the style of each <td> box inside the table

# 3. Footer's element and ?check box?
$pageSizeChangeEnabled = true; # This is used to generate the box of size per page, set it to true to generate
$checkmaster = true; #? seems this is used to create check box for all
#---------------------------
# End Load the can join list

# Filters
// Create a status filter
$status_selection = "<SELECT name='status' onChange='this.form.submit()'>";
$status_selection .= "<OPTION value='' ".($status==0?"SELECTED":"").">".$ec_iPortfolio['all_status']."</OPTION>";
$status_selection .= "<OPTION value='1' ".($status==1?"SELECTED":"").">".$ec_iPortfolio['pending']."</OPTION>";
$status_selection .= "<OPTION value='2' ".($status==2?"SELECTED":"").">".$ec_iPortfolio['approved']."</OPTION>";
$status_selection .= "<OPTION value='3' ".($status==3?"SELECTED":"").">".$ec_iPortfolio['rejected']."</OPTION>";
$status_selection .= "</SELECT>";
$status_selection_html = $ec_iPortfolio['status']." : ".$status_selection;

if (isset($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) && $sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE'])
{
	$IsSAS_selection_html = "SAS";
	$IsSASArr[] = array("", $Lang['iPortfolio']['OLE']['SAS_ALL']);
	$IsSASArr[] = array("0", $Lang['iPortfolio']['OLE']['SAS_NOT']);
	$IsSASArr[] = array("1", $Lang['iPortfolio']['OLE']['SAS']);
	$IsSAS_selection_html = $linterface->GET_SELECTION_BOX($IsSASArr, "onChange='this.form.submit()' name='IsSAS'", "", $IsSAS);
	
	$InOutsideSchoolArr[] = array("", $Lang['iPortfolio']['OLE']['In_Outside_School']);
	$InOutsideSchoolArr[] = array("0", $Lang['iPortfolio']['OLE']['Inside_School']);
	$InOutsideSchoolArr[] = array("1", $Lang['iPortfolio']['OLE']['Outside_School']);
	$InsideOutSide_selection_html = $linterface->GET_SELECTION_BOX($InOutsideSchoolArr, "onChange='this.form.submit()' name='IsOutsideSchool'", "", $IsOutsideSchool);
}

# cateogry selection
$category_selection = $LibPortfolio->GET_OLE_CATEGORY_SELECTION("name='category' onChange='this.form.submit()'", $category, 1);
$category_selection_html = $category_selection;

# subcategory selection
$subcategory_selection_html = $LibPortfolio->GET_SUBCATEGORY_SELECTION("name='subcategory' onChange='document.form1.submit()'", $subcategory, 1, $category);

# academic year selection
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
$AcademicYearSelectionHTML = getSelectByArray($academic_year_arr, "name='AcademicYearID' onChange='this.form.submit()'", $AcademicYearID, 1, 0, $i_Attendance_AllYear, 2);

# year selection
//$academicYearNameArray = $LibPortfolio->GET_OLE_DISTINCT_YEAR($UserID);
//$YearSelectionHTML = $linterface->GET_SELECTION_BOX($academicYearNameArray,"name='AcademicYear' onChange='this.form.submit()'", $i_general_all, $AcademicYear);


#Please do not delete this part
# academic year selection
//$academicYearInfoArray = GetAllAcademicYearInfo();
//$academicYearArray = array();
//if (is_array($academicYearInfoArray)) {
//	//$academicYearName = ($intranet_session_language == "en" ? 'YearNameEN' : 'YearNameB5');
//	$academicYearName = $value['StartYear'];
//	$i = 0; 
//	foreach ($academicYearInfoArray as $key => $value) {
//		$academicYearNameArray[$i][0] = $value['AcademicYearID'];
//		$academicYearNameArray[$i][1] = $value[$academicYearName];
//		$i++;
//	}
//}
//$AcademicYearSelectionHTML = $linterface->GET_SELECTION_BOX($academicYearNameArray,"name='AcademicYear' onChange='this.form.submit()'", $i_general_all, $AcademicYear);

#  Consturct page title elements
$titleFigure = $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/icon_section.gif";
$title1 = "
			<tr>
				<td>
	        		<img src=\"$titleFigure\" align=\"absmiddle\" height=\"20\" width=\"20\"><span class=\"sectiontitle\">".$ec_iPortfolio['SLP']['ReportOtherActivities']."</span></tr>
	        	</td>
			</tr>
";
$title2 = "
			<tr>
				<td>
	        		<img src=\"$titleFigure\" align=\"absmiddle\" height=\"20\" width=\"20\"><span class=\"sectiontitle\">" . $ec_iPortfolio['SLP']['SchoolPresetActivities'] . "</span></tr>
	        	</td>
			</tr>
";


//if ($LibPortfolio->isStudentSubmitOLESetting($IntExt_p, $ck_memberType)) {
	
//	$studentOleConfigDataArray = $LibPortfolio->getDataFromStudentOleConfig();

	# Get OLE period settings data from DB
	$objIpfPeriodSetting = new iportfolio_period_settings();
	$OLESettingsArray = $objIpfPeriodSetting->getSettingsArray("OLE", $StudentLevel['YearID']);
	
	# Set start and end date format
	$OLESettingsArray['StartDateTime'] = $objIpfPeriodSetting->returnDBTimeFormat((array)$OLESettingsArray['StartDate'], (array)$OLESettingsArray['StartHour'], (array)$OLESettingsArray['StartMinute']);
	$SubmissionPeriod_startTime = $OLESettingsArray['StartDateTime'][0];
	$OLESettingsArray['EndDateTime'] = $objIpfPeriodSetting->returnDBTimeFormat((array)$OLESettingsArray['EndDate'], (array)$OLESettingsArray['EndHour'], (array)$OLESettingsArray['EndMinute']);
	$SubmissionPeriod_endTime = $OLESettingsArray['EndDateTime'][0];
	$SubmissionPeriod_isAllowed = $OLESettingsArray['AllowSubmit'];

	/************* Old Version ****************/		  
	if(($SubmissionPeriod_startTime!="" && $SubmissionPeriod_startTime!="0000-00-00 00:00:00") && ($SubmissionPeriod_endTime!="" && $SubmissionPeriod_endTime!="0000-00-00 00:00:00"))
	  $Period_Msg = $iPort['period_start_end']." ".$SubmissionPeriod_startTime." ".$iPort['to']." ".$SubmissionPeriod_endTime;
	else if($SubmissionPeriod_startTime!="" && $SubmissionPeriod_startTime!="0000-00-00 00:00:00")
	  $Period_Msg = $iPort['period_start']." ".$SubmissionPeriod_startTime;
	else if($SubmissionPeriod_endTime!="" && $SubmissionPeriod_endTime!="0000-00-00 00:00:00")
	  $Period_Msg = $iPort['period_end']." ".$SubmissionPeriod_endTime;
	else
	  $Period_Msg = "";
	  
//	if(($studentOleConfigDataArray["STARTTIME_TO_MINS"]!="" && $studentOleConfigDataArray["STARTTIME_TO_MINS"]!="0000-00-00 00:00:00") && ($studentOleConfigDataArray["ENDTIME_TO_MINS"]!="" && $studentOleConfigDataArray["ENDTIME_TO_MINS"]!="0000-00-00 00:00:00"))
//	  $Period_Msg = $iPort['period_start_end']." ".$studentOleConfigDataArray["STARTTIME_TO_MINS"]." ".$iPort['to']." ".$studentOleConfigDataArray["ENDTIME_TO_MINS"];
//	else if($studentOleConfigDataArray["STARTTIME_TO_MINS"]!="" && $studentOleConfigDataArray["STARTTIME_TO_MINS"]!="0000-00-00 00:00:00")
//	  $Period_Msg = $iPort['period_start']." ".$studentOleConfigDataArray["STARTTIME_TO_MINS"];
//	else if($studentOleConfigDataArray["ENDTIME_TO_MINS"]!="" && $studentOleConfigDataArray["ENDTIME_TO_MINS"]!="0000-00-00 00:00:00")
//	  $Period_Msg = $iPort['period_end']." ".$studentOleConfigDataArray["ENDTIME_TO_MINS"];
//	else
//	  $Period_Msg = "";

	$display_period = "<tr><td class=\"tabletext\"><b><font color=\"red\"> ".$Period_Msg."</font></b></td></tr>";
	
	/************* END Old Version ****************/
	
//	$SubmissionPeriod_isAllowed = $studentOleConfigDataArray["INT_ON"];
//	$SubmissionPeriod_isAllowed = ($SubmissionPeriod_isAllowed=='on')? true:$SubmissionPeriod_isAllowed;
	
	$new_btn = "<tr><td><br /><a href=\"ole_new.php?action=new&IntExt={$IntExt_p}&AcademicYearID={$AcademicYearID_p}&ELE={$ELE_p}&status={$status_p}\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$button_new} </a></td></tr>";
	
	//$oleJoinNotes = "<tr><td><br/>".$ec_iPortfolio['SLP']['OleJoinNotes1']." ".$ec_iPortfolio['SLP']['OleJoinNotes2']."</td></tr>";
//} else {
//	$oleJoinNotes = "<tr><td><br/>".$ec_iPortfolio['SLP']['OleJoinNotes1']."</td></tr>";
//}

$dateNow = date("Y-m-d H:i:s");
//debug_pr($studentOleConfigDataArray);

if($SubmissionPeriod_startTime!='' && $SubmissionPeriod_endTime!='')
{
	$cond =($SubmissionPeriod_startTime<$dateNow && $SubmissionPeriod_endTime>$dateNow)&& $SubmissionPeriod_isAllowed==true;
}
else if($SubmissionPeriod_startTime!='' && $SubmissionPeriod_endTime=='')
{
	$cond =($SubmissionPeriod_startTime<$dateNow)&& $SubmissionPeriod_isAllowed==true;
}
else if($SubmissionPeriod_startTime=='' && $SubmissionPeriod_endTime!='')
{
	$cond =($SubmissionPeriod_endTime>$dateNow)&& $SubmissionPeriod_isAllowed==true;
}

//$SubmissionPeriod_startTime = $studentOleConfigDataArray["STARTTIME_TO_MINS"];
//$SubmissionPeriod_endTime =  $studentOleConfigDataArray["ENDTIME_TO_MINS"];
//if($SubmissionPeriod_startTime!='' && $SubmissionPeriod_endTime!='')
//{
//	$cond =($SubmissionPeriod_startTime<$dateNow && $SubmissionPeriod_endTime>$dateNow)&& $SubmissionPeriod_isAllowed==true;
//}
//else if($SubmissionPeriod_startTime!='' && $SubmissionPeriod_endTime=='')
//{
//	$cond =($SubmissionPeriod_startTime<$dateNow)&& $SubmissionPeriod_isAllowed==true;
//}
//else if($SubmissionPeriod_startTime=='' && $SubmissionPeriod_endTime!='')
//{
//	$cond =($SubmissionPeriod_endTime>$dateNow)&& $SubmissionPeriod_isAllowed==true;
//}
//$cond=true;
if($cond==true)
{
	$oleJoinNotes = "<tr><td><br/>". $ec_iPortfolio['SLP']['OleJoinNotes']['WithinSubmissionPeriod'] ."</td></tr>";
}
else
{
	$new_btn = '';
	$oleJoinNotes = "<tr><td><br/>". $ec_iPortfolio['SLP']['OleJoinNotes']['WithoutSubmissionPeriod'] ."</td></tr>";
}

$linterface->LAYOUT_START();
?>
<script language="JavaScript" type="text/JavaScript">
// Search OLE TITLE
function jSUBMIT_SEARCH(){
	document.form1.FieldChanged.value = "search";
	document.form1.action = "ole_join.php";
	document.form1.submit();
}

</script>
<FORM name="form1" method="GET">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	<table width="96%" border="0" cellspacing="0" cellpadding="8">
	<tr>
		<td>
		<!-- CONTENT HERE -->
		<table border="0" cellspacing="5" cellpadding="0">
        	<?=$title1?>
        	<tr>
      			<td>
            	<table border="0" cellspacing="0" cellpadding="0">
            		 <?=$display_period?>
                      <?=$oleJoinNotes?>
            		  <?=$new_btn?>
            	</table>
            	</td>
            </tr>
        </table>
        <br />		
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_b">
        	<?=$title2?>
			<tr>
	          	<td align="center" valign="middle">
	          	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	          	<tr>
	          		<td>
					<table border="0" cellpadding="3" cellspacing="0" width="100%">
					<tr>
                      <td><?=$AcademicYearSelectionHTML?>&nbsp;&nbsp;&nbsp;<?=$status_selection?></td>
                  	</tr>
                  	<tr>
                  		<td nowrap><?=$category_selection_html?>&nbsp;&nbsp;&nbsp;<?=$subcategory_selection_html?>&nbsp;&nbsp;<?=$IsSAS_selection_html?>&nbsp;&nbsp;<?=$InsideOutSide_selection_html?></td>
                  	</tr>
					<tr>
						<td align="right" width = "*">
								<span class="tabletext">
									<input name="search_text" type="text" class="tabletext" value="<?=($search_text==""?$ec_iPortfolio['enter_title_name']:stripslashes($search_text))?>" onFocus="SearchTextFocus=true;if(this.value=='<?=$ec_iPortfolio['enter_title_name']?>'){this.value=''}" onBlur="SearchTextFocus=false;if(this.value==''){this.value='<?=$ec_iPortfolio['enter_title_name']?>'}" />
									<input type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onClick="jSUBMIT_SEARCH()" />
								</span>
						</td>
					</tr>
					</table>						
					</td>
	          	</tr>
	          	<tr>
	          		<td>
	          		<?= $LibTable->display(); ?>
					</td>
				</tr>
				</table>
				</td>
			</tr>
			</table>
			<table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td align="right">			
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tbody>
									<tr>
										<td align="center">
										<input class="formbutton print_hide" onclick="self.location='ole.php';" value="Back" onmouseover="this.className='formbutton'" onmouseout="this.className='formbutton'" type="button">
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>			
		<!-- End of CONTENT -->
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="FieldChanged" />
<input type="hidden" name="IntExt" value="1" />

</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
