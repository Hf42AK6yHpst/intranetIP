<?php
/** [Modification Log] Using By : 
 * *******************************************************
 * 2015-01-13 Bill
 * - add Ng Wah SAS Report Tab
 * 2014-07-23 Bill
 * - display Pui Kui SLP Report Tab
 * *******************************************************
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_student_dynamicReport_page_no", "pageNo");
$arrCookies[] = array("ck_student_dynamicReport_page_order", "order");
$arrCookies[] = array("ck_student_dynamicReport_page_field", "field");
$arrCookies[] = array("ck_student_dynamicReport_page_size", "numPerPage");

if(isset($clearCoo) && $clearCoo == 1){
	clearCookies($arrCookies);
}else{ 
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();


$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$libportfolio_ui = new libportfolio_ui();

# template for student page
$linterface = new interface_html("iportfolio_default.html");

# Menu Array
$CurrentPage = "Student_Report"; // fai need to updat
//$CurrentPageName = $iPort['menu']['slp'];
$CurrentPageName = $iPort['menu']['reports'];


# Page Title
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR();



#check teacher setting to avoid student key in the link directly
//$objIpfSetting = iportfolio_settings::getInstance();
//$reportFormAllow = $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpFormAllowed"]);
//if($reportFormAllow){
//	// do nothing 
//}else{
//	header("Location: /home/portfolio/profile/student_info_student.php");
//	exit();
//}
//
$luser = new libuser($UserID);
$objiPf = new libportfolio();

//$showSLPReportPrintingForStduent = $objiPf->showSLPReportPrintingForStduent($luser);
$showDynamicReportPrintingForStudent = $objiPf->showDynamicReportPrintingForStudent($luser);
if($showDynamicReportPrintingForStudent){
	// do nothing
}else{
	header("Location: /home/portfolio/profile/student_info_student.php");
	exit();
}

# Check if student has right to print Pui Kiu SLP report: Display the tab
if($sys_custom['iPf']['pkms']['Report']['SLP'])
{
	$PKMSAccess = false;
	
	$portfolio__plms_report_config_file = "$eclass_root/files/portfolio_plms_slp_config.txt";
	if(file_exists($portfolio__plms_report_config_file) && get_file_content($portfolio__plms_report_config_file) != null)
	{	
		list($r_formAllowed, $startdate, $enddate, $allowStudentPrintPKMSSLP,$issuedate,$r_isPrintIssueDate, $noOfSLPRecord) = explode("\n", trim(get_file_content($portfolio__plms_report_config_file)));
		$formArray = unserialize($r_formAllowed);
		$allowStudentPrint = unserialize($allowStudentPrintPKMSSLP);
		$StudentLevel = $LibPortfolio->getClassLevel($luser->ClassName);
		
		if(is_array($formArray) && in_array($StudentLevel[0], $formArray) && $allowStudentPrint == 1){
			// Allow to print
			$PKMSAccess = true;
		}
	}
}

# Tabs 
$TabMenuArr = libpf_tabmenu::getReportTags("dynamicReport");
if($sys_custom['iPf']['pkms']['Report']['SLP'] && $PKMSAccess){
	// Add Pui Kiu Report Tab
	array_push($TabMenuArr, array("/home/portfolio/student/pkms_report.php",$ec_iPortfolio['pkms_SLP_config'], 0));
}

# Ng Wah SAS Cust & within release period
if($sys_custom['NgWah_SAS'] && $LibPortfolio->showNgWahSASReport($luser)){
	// Add Ng Wah SAS Report Tab
	array_push($TabMenuArr, array("/home/portfolio/student/ngwah_sas_report.php", $Lang['iPortfolio']['NgWah_SAS_Report'], 0));
}

$linterface->LAYOUT_START();


$htmlAry['dynamicTableDisplay'] = '';
$htmlAry['dynamicTableDisplay'] = $libportfolio_ui->Get_Student_DynamicReport_DBTable();


?>
<script language="javascript">

function js_Get_Report(recordId, templateId){
	$.ajax({
		url:	"ajax_get_dynamic_report.php",
		type:	"POST",	
		data: 	'&recordId=' + recordId + '&templateId=' + templateId,
		async:	false,
		error:  function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				},
		success: function(data)
				 {		
					
					if(data=='noRecordFound')
					{						
						alert('<?php echo $Lang['General']['NoRecordFound']?>');
					}					
					else
					{							
						$('#ExportIFrame').attr('src', 'download_dynamicReport_attachment.php?target_e=' + data + '&recordId=' + recordId);
					}
				 }	
	});
	
}

</script>

<table border="0" cellpadding="5" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td> <?=$LibPortfolio->GET_TAB_MENU($TabMenuArr);?>
			</td>
		</tr>
			
		<tr>
			<td>
				<form id="form1" name="form1" method="POST" action="dynamicReport.php" onsubmit="return false;">
					<div class="content_top_tool">
						<?=$h_searchBox?>
					</div>
					<br style="clear:both" />							
					<div id="DBTableDiv">
						<?=$htmlAry['dynamicTableDisplay'];?>
					</div>						
				</form>
			</td>	
		</tr>
	</tbody>
</table>
	
<iframe id="ExportIFrame" name="ExportIFrame" src="" style="width:100%;height:300px;display:none;"></iframe>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>