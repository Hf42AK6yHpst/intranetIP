<?php
/** [Modification Log] Using By : 
 * *******************************************************
 * 
 * *******************************************************
 */
 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_settings.php");

intranet_auth();
intranet_opendb();

$li_pf = new libpf_sturec();
$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

$luser = new libuser($UserID);
$objiPf = new libportfolio();

# Access Checking
if($sys_custom['NgWah_SAS'] && $LibPortfolio->showNgWahSASReport($luser)){
	// do nothing 
}else{
	$showDynamicReportPrintingForStudent = $objiPf->showDynamicReportPrintingForStudent($luser);
	if ($showDynamicReportPrintingForStudent) {
		header("Location: dynamicReport.php");
	}
	else {
		header("Location: /home/portfolio/profile/student_info_student.php");
		exit();
	}
}

# Template for student page
$linterface = new interface_html("iportfolio_default.html");

# Page
$CurrentPage = "Student_Report";
$CurrentPageName = $iPort['menu']['reports'];

# Title
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR();

# Check teacher setting to avoid student key in the link directly
$objIpfSetting = iportfolio_settings::getInstance();

# Tab
$TabMenuArr = libpf_tabmenu::getReportTags("");

# Check - Pui Kiu SLP Report Tab
if($sys_custom['iPf']['pkms']['Report']['SLP'])
{
	$PKMSAccess = false;
	
	$portfolio__plms_report_config_file = "$eclass_root/files/portfolio_plms_slp_config.txt";
	if(file_exists($portfolio__plms_report_config_file) && get_file_content($portfolio__plms_report_config_file) != null)
	{	
		list($r_formAllowed, $startdate, $enddate, $allowStudentPrintPKMSSLP,$issuedate,$r_isPrintIssueDate,$noOfPKMSRecords) = explode("\n", trim(get_file_content($portfolio__plms_report_config_file)));
		$formArray = unserialize($r_formAllowed);
		$allowStudentPrint = unserialize($allowStudentPrintPKMSSLP);
		$StudentLevel = $LibPortfolio->getClassLevel($luser->ClassName);
		
		if(is_array($formArray) && in_array($StudentLevel[0], $formArray) && $allowStudentPrint == 1){
			// Allow student to print
			$PKMSAccess = true;
		}
	}
}

if($sys_custom['iPf']['pkms']['Report']['SLP'] && $PKMSAccess){
	// Add Pui Kiu Report Tab
	array_push($TabMenuArr, array("/home/portfolio/student/pkms_report.php",$ec_iPortfolio['pkms_SLP_config'], 0));
}

array_push($TabMenuArr, array("/home/portfolio/student/ngwah_sas_report.php", $Lang['iPortfolio']['NgWah_SAS_Report'], 1));

$note_to_student = ($sys_custom["ywgs_ole_report"]) ? $ec_warning['reportPrinting_instruction_for_student_ywgs'] : $ec_warning['reportPrinting_instruction_for_student'];

$linterface->LAYOUT_START();

?>

<script language="javascript">
</script>

<FORM id="form1" name="form1" action="../profile/report/customizeReport/index.php" method="POST" target="_blank">
	<table border="0" cellpadding="5" cellspacing="0" width="100%">
		<tbody>
			<tr>
				<td colspan="2"> <?=$LibPortfolio->GET_TAB_MENU($TabMenuArr);?></td>
			</tr>
			<tr>
				<td width="6%">&nbsp;</td>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
						<tr><td align="center">
								<input class="formbutton" type="submit" value="<?=$ec_iPortfolio['SLP']['ExportAsPdf']?>" name="btn_print_pdf" id="btn_print_pdf" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">&nbsp;
						</td></tr>
				</table></td>
			</tr>
		</tbody>
	</table>
	
<input type="hidden" name="task" value="ngwah_sas">
<input type="hidden" name="fromStudent" value="1">
	
</form>

<?php

$linterface->LAYOUT_STOP();

intranet_closedb();

?>