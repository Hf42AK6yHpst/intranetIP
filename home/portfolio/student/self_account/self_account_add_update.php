<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * Date : 2016-01-12 Omas
 * 		  changed $_GET['setDefault'] to $_POST['setDefault'] - #J91331 
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("S");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_slp();

$setDefault = $_POST['setDefault'];
$RecordID = IntegerSafe(intval($_POST['RecordID']));

$self_acc = $li_pf->GET_SELF_ACCOUNT($RecordID);
if (($RecordID > 0 && $self_acc['UserID'] != $_SESSION['UserID']) || $StudentID != $_SESSION['UserID']) {
	No_Access_Right_Pop_Up();
	die();
}

if($RecordID > 0)
{	
	$condition = isset($Title) && isset($Details) && isset($RecordID);
	$isEdit = true;
}
else
{
	$condition = isset($Title) && isset($Details) && isset($StudentID);
	$isEdit = false;
}	

if($condition)
{
	$sa_arr['Title'] = $Title;

	$sa_arr['Details'] = nl2br($Details);		
	
	if($RecordID > 0)
	{	
		$sa_arr['RecordID'] = $RecordID;
		$li_pf->EDIT_SELF_ACCOUNT($sa_arr);
	}
	else
	{
		$sa_arr['UserID'] = $StudentID;
		$li_pf->ADD_SELF_ACCOUNT($sa_arr);
		$RecordID = $li_pf->db_insert_id();
	}

	$selfAccListArr = $li_pf->GET_SELF_ACCOUNT_LIST($StudentID);
	$count_selfAccListArr = count($selfAccListArr);

	if($isEdit==false && $count_selfAccListArr==1)
	{
		$li_pf->SET_SELF_ACCOUNT_DEFAULTSA($StudentID,$RecordID);
	}
	else if($setDefault==1 )
	{
     	$li_pf->SET_SELF_ACCOUNT_DEFAULTSA($StudentID,$RecordID);
	}	
}

intranet_closedb();
header("Location: ./self_account.php");

?>
