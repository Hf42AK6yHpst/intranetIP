<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("S");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_slp();
$hasAttachment = false;

if(isset($RecordID))
{
/*
	# Get the attachment path in server
  $target_dir = $li_pf->GET_SELF_ACCOUNT_ATTACHMENT_PATH($RecordID);
    
	$lf = new libfilesystem();
    
  # delete directory for attachments
  $path = "$file_path/file/portfolio/self_account/$target_dir/";
	$lf->folder_remove_recursive($path);	
*/
	$RecordID = IntegerSafe($RecordID);
	
	$RecordArr = $li_pf->GET_SELF_ACCOUNT_ALL_DATA($RecordID);
	$recordUserId = $RecordArr[0]['UserID'];
	
	if ($recordUserId != $_SESSION['UserID']) {
		No_Access_Right_Pop_Up();
		die();
	} 
	
	
	if(is_array($RecordArr))
	{
		$RecordArr = current($RecordArr);
	}
	$DefaultSA = $RecordArr['DefaultSA'];
	
	$StudentID = $_SESSION['UserID'];
	$li_pf->DELETE_SELF_ACCOUNT($RecordID);
	if($DefaultSA=='SLP')
	{
		$li_pf->SET_SELF_ACCOUNT_DEFAULTSA($StudentID);
	}
	
	$thisUserRecordArr = $li_pf->GET_SELF_ACCOUNT_ALL_DATA('',$StudentID);

	$count_Record = count($thisUserRecordArr);
}

intranet_closedb();
if($DefaultSA=='SLP' && $count_Record!=0)
{
	header("Location: ./self_account_set_default.php?msg=1");
}
else
{
	header("Location: ./self_account.php");
}


?>
