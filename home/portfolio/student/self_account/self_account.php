<?php

/**********************************************************************
 * Modification Log:
 * 2010-08-09 (Eric):
 * 		- Modification period limitation 
 **********************************************************************/ 

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_settings.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");
iportfolio_auth("SP");
intranet_opendb();

switch($_SESSION['UserType'])
{
	case 2:
		$CurrentPage = "Student_SelfAccount";
		$StudentID = $_SESSION['UserID'];
		break;
	case 3:
		$CurrentPage = "Parent_SelfAccount";
		$StudentID = $ck_current_children_id;
		break;
	default:
		break;
}

$li_pf = new libpf_slp();

# Set photo in the left menu
$luser = new libuser($StudentID);
$luser->PhotoLink = $li_pf->GET_OFFICIAL_PHOTO_BY_USER_ID($StudentID);
$luser->PhotoLink = str_replace($intranet_root, "", $luser->PhotoLink[0]);


$objIpfSetting = iportfolio_settings::getInstance();
$isAllowStudentSubmit	= $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["selfAccountAllowStudentSubmit"]);

if($isAllowStudentSubmit){
	//further date time checking if a check box for allow submit is checked
	$student_ole_config_file = "$eclass_root/files/self_account_config.txt";
	$filecontent = trim(get_file_content($student_ole_config_file));
	list($starttime, $sh, $sm, $endtime, $eh, $em) = unserialize($filecontent);
	$v_starttime = ($starttime == "") ? "" : "$starttime $sh:$sm:00";
	$v_endtime = ($endtime == "") ? "" : "$endtime $eh:$em:59";
	$ValidPeriod = $li_pf->isValidPeriod($v_starttime,$v_endtime);
}else{
	$ValidPeriod = 0;
}


$self_acc_display = $li_pf->GEN_SELF_ACCOUNT_LIST_TABLE($StudentID, $ValidPeriod);

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPageName = $iPort['menu']['self_account'];

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">

// Using post to go to edit instead of using get (security)
function jEDIT_SA(jParRecordID){
	document.form1.RecordID.value = jParRecordID;
	document.form1.action = "self_account_add.php";
	document.form1.submit();
}

// delete self account
function jDELETE_SA(jParRecordID){
	if(confirm(globalAlertMsg3))
	{
		document.form1.RecordID.value = jParRecordID;
		document.form1.action = "self_account_remove.php";
		document.form1.submit();
	}
}

</SCRIPT>

<form name="form1" method="POST">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
<?php if($ck_memberType != "P" && $ValidPeriod) { ?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="tabletext">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td><a href="self_account_add.php" class="contenttool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_add.gif" width="20" height="20" border="0" align="absmiddle"> <?=$button_new?> </a></td>
								<td><a href="self_account_set_default.php" class="contenttool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icons_manage.gif" width="20" height="20" border="0" align="absmiddle"> <?=$iPort["select_default_sa"]?></a></td>
								<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
							</tr>
						</table>
					</td>
					<td align="right" valign="bottom" class="tabletext">&nbsp;</td>
				</tr>
			</table>
<?php } ?>
			<?=$self_acc_display?>
		</td>
	</tr>
</table>

<input type="hidden" name="RecordID" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
