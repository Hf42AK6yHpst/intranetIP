<?php
/** [Modification Log] Modifying By: Connie
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";

# Define name of textarea
define("TEXTAREA_OBJ_NAME", "Details");

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("S");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$StudentID = $_SESSION['UserID'];

$li_pf = new libpf_slp();

$sa_config_file = $eclass_filepath."/files/portfolio_self_account.txt";
$word_limit = (int) get_file_content($sa_config_file);

# Set photo in the left menu
$luser = new libuser($StudentID);
$luser->PhotoLink = $li_pf->GET_OFFICIAL_PHOTO_BY_USER_ID($StudentID);
$luser->PhotoLink = str_replace($intranet_root, "", $luser->PhotoLink[0]);

# Get self account information
$self_acc = $li_pf->GET_SELF_ACCOUNT($RecordID);
# Escape special characters
$self_acc['Title'] = intranet_htmlspecialchars($self_acc['Title']);

if($self_acc['AttachmentPath'] != "")
{
	$lf = new libfilesystem();

	# Server directory path containing attachments
	$dir_path = $file_path."/file/portfolio/self_account/".$self_acc['AttachmentPath']."/";
	# Traverse the directory
	$attachment_list = $lf->return_folderlist($dir_path);

	if(is_array($attachment_list))
	{
		for($j=0; $j<count($attachment_list); $j++)
		{
			$attachment_str .= "<a href=\"/file/portfolio/self_account/".$self_acc['AttachmentPath']."/".basename($attachment_list[$j])."\" target=\"blank\" class=\"tablelink\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_attachment.gif\" width=\"12\" height=\"18\" border=\"0\" align=\"absmiddle\">".basename($attachment_list[$j])."</a> <a href=\"#\" onClick=\"jDELETE_ATTACHMENT('".basename($attachment_list[$j])."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" alt=\"".$button_remove."\" /></a><br />\n";
		}
	}
}

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPage = "Student_SelfAccount";
$CurrentPageName = $iPort['menu']['self_account'];

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
	
function jADD_ATTACHMENT_ROW()
{
	var attachment_list = document.getElementById('attachment_list');
	var new_row = attachment_list.insertRow(attachment_list.rows.length);
	var new_cell = new_row.insertCell(0);
	
	new_cell.innerHTML =	"<input type='file' name='attachment[]' />";
	
	window.location.href = "#attachment_list_anchor";
}

function jINSERT_HTML(jParHTML)
{
//  var oEditor = FCKeditorAPI.GetInstance('<?=TEXTAREA_OBJ_NAME?>');
  
//  oEditor.InsertHtml(jParHTML);

	 $("#Details").append(jParHTML);
}

// function for view records
function jDISPLAY_RECORDS(jParRecordType)
{
  $.ajax({
		type: "GET",
		url: "../../ajax/get_"+jParRecordType+"_html_ajax.php",
		data: "objname=<?=TEXTAREA_OBJ_NAME?>",
		success: function (data) {
			$("#RecordsLayer").html(data);
		}
	});
}

/*
functions for delete attachments
1. jDELETE_ATTACHMENT
2. CompleteDeleteAttachment
*/
/*
function jDELETE_ATTACHMENT(jParAttachment)
{
	if(confirm("<?=$ec_iPortfolio['delete_attachment_confirm']?>"))
	{
	  xmlHttp_attachment = GetXmlHttpObject();
	  if (xmlHttp_attachment == null)
	  {
	    alert ("Your browser does not support AJAX!");
	    return;
	  }
	
	  // Modify Layer Content
	  var url="./ajax/delete_attachment_ajax.php";
	  url=url+"?attachment=" + jParAttachment;
	  url=url+"&path=<?=base64_encode($dir_path)?>";
	  url=url+"&RecordID=<?=$RecordID?>";
	  url=url+"&sid="+Math.random();
	
	  xmlHttp_attachment.onreadystatechange = CompleteDeleteAttachment;
	  xmlHttp_attachment.open("GET",url,true);
	  xmlHttp_attachment.send(null);
	}
}

function CompleteDeleteAttachment() 
{ 
  if (xmlHttp_attachment.readyState==4)
  {
    var Attachment_HTML = xmlHttp_attachment.responseText;
    var AttachmentLayer = document.getElementById("AttachmentLayer");
    AttachmentLayer.innerHTML = Attachment_HTML;
  }
}
*/

function jCOUNT_WORD() {
  var oEditor = FCKeditorAPI.GetInstance('<?=TEXTAREA_OBJ_NAME?>');
  var matches = oEditor.GetData().replace(/<[^<|>]+?>|&nbsp;/gi,' ').match(/\b/g);
  var count = 0;
  if(matches) {
      count = matches.length/2;
  }
  
  return count;
}

function jUPDATE_WORD_COUNT() {
  count = jCOUNT_WORD();
  $("#word_count").text(count);
}

function FCKeditor_OnComplete( editorInstance ) {
  jUPDATE_WORD_COUNT();
  if (document.all) {      
    // IE
    editorInstance.EditorDocument.attachEvent("onkeyup", jUPDATE_WORD_COUNT);
  } else {
    // other browser
    editorInstance.EditorDocument.addEventListener( 'keyup', jUPDATE_WORD_COUNT, true );
  }   
}

function js_Submit()
{
	alert("<?=$ec_iPortfolio['setThisDefaultSA'] ?>");
	document.form1.submit();
}

function textCounter() {

    var number = 0;
    var matches = $('#Details').val().match(/\b/g);
    if(matches) {
        number = matches.length/2;
    }
    $('#word_count').text( number);

}


$(document).ready(function() {
  textCounter();
  $("form[name='form1']").submit(function() {
    count = jCOUNT_WORD();
    if(<?=$word_limit?> && count > <?=$word_limit?>)
    {
      alert("<?=str_replace("<!--WordLimit-->", $word_limit, $Lang['iPortfolio']['alertExceedWordLimit'])?>");
      FCKeditorAPI.GetInstance('<?=TEXTAREA_OBJ_NAME?>').Focus();
      return false;
    }
    else
    {
      return true;
    }
  });
});

</SCRIPT>

<form name="form1" method="POST" enctype="multipart/form-data" action="self_account_edit_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="self_account.php"><?=$ec_iPortfolio['self_account']?></a> <img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><?=$button_edit?> </td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="400" align="center" valign="top" class="stu_info_log tabletext">
						<table width="100%" border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td>
									<table width="100%" border="0" cellpadding="2" cellspacing="0" style="border:dashed 1px #999999">
										<tr>
											<td>
												<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_view.gif" width="20" height="20" align="absmiddle">
												<span class="tabletextremark"><?=$ec_iPortfolio['view_my_record']?><br></span>
												<table border="0" cellspacing="0" cellpadding="5">
													<tr>
														<td align="center" nowrap><a href="#" class="tablelink" onClick="jDISPLAY_RECORDS('activity')"> <?=$ec_iPortfolio['activity']?><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/leftmenu/btn_arrow_more_off.gif" width="15" height="30" border="0" align="absmiddle" id="lb_5"></a> </td>
														<td align="center" nowrap><a href="#" class="tablelink" onClick="jDISPLAY_RECORDS('award')"> <?=$ec_iPortfolio['award']?><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/leftmenu/btn_arrow_more_off.gif" width="15" height="30" border="0" align="absmiddle" id="lb_4"></a> </td>
														<td align="center" nowrap><a href="#" class="tablelink" onClick="jDISPLAY_RECORDS('comment')"> <?=$ec_iPortfolio['title_teacher_comments']?><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/leftmenu/btn_arrow_more_off.gif" width="15" height="30" border="0" align="absmiddle" id="lb_5"></a> </td>
														<td align="center" nowrap><a href="#" class="tablelink" onClick="jDISPLAY_RECORDS('ole')"><?=$ec_iPortfolio['ole']?><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/leftmenu/btn_arrow_more_off.gif" width="15" height="30" border="0" align="absmiddle" id="lb_7"></a></td>
														<!--<td align="center" nowrap><a href="#" class="tablelink"><?=$i_general_others?><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/leftmenu/btn_arrow_more_off.gif" width="15" height="30" border="0" align="absmiddle" id="lb_8"></a> </td>-->
													</tr>
												</table>
												<div id="RecordsLayer" style="overflow:auto;" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table width="100%" border="0" cellpadding="2" cellspacing="0">
										<tr>
											<td class="tabletext" width="30"><strong><?=$i_general_title?></strong></td>
											<td class="tabletext">
												<input type="text" name="Title" class="tabletext" style="width:100%" value="<?=$self_acc['Title']?>" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
<?php
//	$use_html_editor = true;
	$use_html_editor = false;
	if ($use_html_editor)
	{
		# Components size
		$msg_box_width = "100%";
		$msg_box_height = 200;
		$obj_name = TEXTAREA_OBJ_NAME;
		$editor_width = $msg_box_width;
		$editor_height = $msg_box_height;
		$init_html_content = $self_acc['Details'];

		include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
		
		$objHtmlEditor = new FCKeditor($obj_name, $msg_box_width, $editor_height);
		$objHtmlEditor->Value = $init_html_content;
		$objHtmlEditor->Create();
	}
	else
	{
		echo "<textarea class=\"textboxtext\" name=\"Details\" ID=\"Details\" cols=\"80\" rows=\"16\" wrap=\"virtual\" onKeyUp=\"textCounter();\" >".$self_acc['Details']."</textarea>";
	}
?>
<!-- Eric Yip (20090727): Disable attachments since there are not usable
									<table border="0" cellspacing="0" cellpadding="2">
										<tr>
											<td>
												<div id="AttachmentLayer"><?=$attachment_str?></div>
											</td>
										</tr>
										<tr>
											<td>
												<a name="attachment_list_anchor">
												<table border="0" cellspacing="0" cellpadding="2" id="attachment_list">
												</table>
											</td>
										</tr>
										<tr>
											<td><a href="javascript:void(0)" class="contenttool" onClick="jADD_ATTACHMENT_ROW()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_new.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_add_attachment?> </a></td>
											<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
										</tr>
									</table>
-->
								</td>
							</tr>
							<tr>
                <td>
                  <table cellspacing="1">
                    <tr>
                      <td><?=$Lang['iPortfolio']['selfAccountWordCount'][0]?></td>
                      <td id="word_count">&nbsp;</td>
                      <td><?=$Lang['iPortfolio']['selfAccountWordCount'][1]?></td>
                    </tr>
                  </table>
                </td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
								<!--<input type="submit" class="formbutton" value="<?=$button_save?>"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'"> -->
									<input type="button" class="formbutton" onClick="js_Submit();this.disabled=true;" value="<?=$button_save?>"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'">
									<input type="button" class="formbutton" onClick="this.disabled=true;self.location='self_account.php'"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$button_cancel?>">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="RecordID" value="<?=$RecordID?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
