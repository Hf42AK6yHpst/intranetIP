<?php
# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == 'Check_EventCode_Valid') {
	$InputValue = trim(stripslashes($_REQUEST['InputValue']));
	$ExcludeEventID = IntegerSafe($_REQUEST['ExcludeEventID']);
	
	$Valid = $lpf_volunteer->checkEventCodeValid($InputValue, $ExcludeEventID);
	echo ($Valid)? '1' : '0';
}
?>