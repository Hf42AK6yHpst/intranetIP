<?php
// Modifying By:
/*
 * Modification Log:
 */
include_once($PATH_WRT_ROOT."includes/portfolio25/volunteer/libpf_volunteer_event.php");

$EventIDArr = IntegerSafe($_POST['EventIDArr']);
$numOfEvent = count((array)$EventIDArr);

$SuccessArr = array();
for ($i=0; $i<$numOfEvent; $i++) {
	$_eventID = $EventIDArr[$i];
	$_objEvent = new libpf_volunteer_event($_eventID);
	$_result = $_objEvent->delete_record();
	
	$SuccessArr[] = ($_result > 0)? true : false;
}

echo (in_array(false, (array)$SuccessArr))? '0' : '1';
?>