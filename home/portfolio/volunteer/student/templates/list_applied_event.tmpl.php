<?php
echo $linterface->Include_Cookies_JS_CSS();
?>
<script language="javascript">
var jsKeywordCookiesKey = "ck_ipf_volunteer_applied_event_list_Keyword";

var arrCookies = new Array();
arrCookies[arrCookies.length] = jsKeywordCookiesKey;

var jsField = '<?=$field?>';
var jsOrder = '<?=$order?>';
var jsPageNo = '<?=$pageNo?>';
var jsKeyword = '<?=$Keyword?>';


$(document).ready( function() {	
	<? if($clearCoo) { ?>
		for(i=0; i<arrCookies.length; i++)
		{
			var obj = arrCookies[i];
			//alert('obj = ' + obj);
			$.cookies.del(obj);
		}
	<? } ?>
	
	var jsTempKeyword = $.cookies.get(jsKeywordCookiesKey);
	jsTempKeyword = (jsTempKeyword==null || jsTempKeyword=='undefined')? '' : jsTempKeyword;
	$('input#Keyword', 'form#form1').val(jsTempKeyword).focus();
	
	Blind_Cookies_To_Object();
	
	js_Reload_DBTable(1);
});


function js_Apply_Event() {
	$('input#task', 'form#form1').val('add_event');
	$('form#form1').submit();
}

function js_Check_Searching(e) {
	if (Check_Pressed_Enter(e)) {
		js_Reload_DBTable();
	}
}

function js_Reload_DBTable(jsInitialize) {
	jsInitialize = jsInitialize || 0;
	if (jsInitialize == 1) { 
		// if first load of the table => follow current setting => do nth
	}
	else {
		// not intialize => changed filtering => start from first page
		jsPageNo = 1;
	}
	
	
	jsKeyword = $('input#Keyword', 'form#form1').val();
	$.cookies.set(jsKeywordCookiesKey, jsKeyword);
		
	
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"index.php", 
		{
			task: 'ajax',
			script: 'ajax_reload',
			action: 'Reload_Applied_Event_List_Student_View_DBTable',
			StudentID: '<?=$_SESSION['UserID']?>',
			field: jsField,
			order: jsOrder,
			pageNo: jsPageNo,
			Keyword: jsKeyword
		},
		function(ReturnData) {
			
		}
	);
}

function js_Edit_Applied_Event() {
	checkEdit2(document.getElementById('form1'), 'RecordIDArr[]', 'js_Go_Edit_Applied_Event()');
}

function js_Go_Edit_Applied_Event() {
	$('input#task').val('fill_event_details');	
	$('form#form1').submit();
}

function js_Delete_Applied_Event() {
	checkRemove2(document.getElementById('form1'), 'RecordIDArr[]', 'js_Go_Delete_Applied_Event()');
}

function js_Go_Delete_Applied_Event() {
	$('input#task').val('ajax');
	$('input#script').val('delete_event_user');
	
	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
	$.ajax({  
		type: 'POST',
		url: 'index.php',
		data: jsSubmitString,  
		success: function(data) {
			if (data != '' && parseInt(data) > 0) {
				js_Reload_DBTable();
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>');
			}
			else {
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['DeleteUnSuccess']?>');
			}
			
			Scroll_To_Top();
		} 
	});
}
</script>

<br />
<form id="form1" name="form1" method="POST" action="index.php" onsubmit="return false;">
	<div class="content_top_tool">
		<div class="Conntent_tool">
			<?=$h_addBtn?>
			<br style="clear:both" />
		</div>
		<?=$h_searchBox?>
	</div>
	<br style="clear:both" />
	
	<?=$h_navigation?>
	<br style="clear:both" />
	
	<div class="table_board">
		<?=$h_actionButton?>
		<div id="DBTableDiv">
			<?=$h_dbtable?>
		</div>	
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="script" name="script" value="" />
</form>
<br/>