<?php
// Modifying:
/*
 * Modification Log:
 */
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/volunteer/libpf_volunteer_ui.php");
$lpf_volunteer_ui = new libpf_volunteer_ui();

$EventIDArr = IntegerSafe($_POST['EventIDArr']);
$RecordIDArr = IntegerSafe($_POST['RecordIDArr']);

if ($RecordIDArr != '') {
	$IsEditMode = true;
}
else {
	$IsEditMode = false;
}


### Navigation 
$NavigationArr = array();
$NavigationArr[] = array($Lang['iPortfolio']['VolunteerArr']['AppliedEvent'], "javascript:js_Go_Back_Registered_Event_List()");
$NavigationArr[] = array(($IsEditMode)? $Lang['Btn']['Edit'] : $Lang['Btn']['Add'], "javascript:js_Go_Back_Select_Event_List()");
$NavigationArr[] = array($Lang['iPortfolio']['VolunteerArr']['FillInDetails']);
$h_navigation = $linterface->GET_NAVIGATION_IP25($NavigationArr);


### Info Table
$h_detailsTable = $lpf_volunteer_ui->getStudentFillInEventDetailsTable($_SESSION['UserID'], $EventIDArr, $RecordIDArr);


### Buttons
$h_submitBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", $onclick="js_Submit();", $id="SubmitBtn");
$h_backBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", $onclick="js_Go_Back_Select_Event_List();", $id="BackBtn");
$h_cancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Go_Back_Registered_Event_List();", $id="CancelBtn");

$linterface->LAYOUT_START();
include_once($template_script);
$linterface->LAYOUT_STOP();
?>