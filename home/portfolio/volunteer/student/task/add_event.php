<?php
// Modifying:
/*
 * Modification Log:
 */
include_once($PATH_WRT_ROOT."includes/portfolio25/volunteer/libpf_volunteer_ui.php");
$lpf_volunteer_ui = new libpf_volunteer_ui();


### Navigation 
$NavigationArr = array();
$NavigationArr[] = array($Lang['iPortfolio']['VolunteerArr']['AppliedEvent'], "javascript:js_Go_Back_Registered_Event_List()");
$NavigationArr[] = array($Lang['Btn']['Add']);
$h_navigation = $linterface->GET_NAVIGATION_IP25($NavigationArr);


### Info Table
$h_eventTable = $lpf_volunteer_ui->getStudentApplicableEventListTable($_SESSION['UserID']);


### Buttons
$h_addBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Add'], "button", $onclick="js_Add_Event();", $id="AddBtn");
$h_cancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Go_Back_Registered_Event_List();", $id="CancelBtn");

$linterface->LAYOUT_START();
include_once($template_script);
$linterface->LAYOUT_STOP();
?>