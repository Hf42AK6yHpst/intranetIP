<?php
// Modifying:
/*
 * Modification Log:
 */
### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_ipf_volunteer_event_approval_list_page_size", "numPerPage");
$arrCookies[] = array("ck_ipf_volunteer_event_approval_list_page_number", "pageNo");
$arrCookies[] = array("ck_ipf_volunteer_event_approval_list_order", "order");
$arrCookies[] = array("ck_ipf_volunteer_event_approval_list_field", "field");

if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
	$ck_ipf_volunteer_event_approval_list_page_size = '';
}
else {
	updateGetCookies($arrCookies);
}


include_once($PATH_WRT_ROOT."includes/portfolio25/volunteer/libpf_volunteer_ui.php");
$lpf_volunteer_ui = new libpf_volunteer_ui();


### Navigation 
$NavigationArr = array();
$NavigationArr[] = array($iPort["record_approval"]);
$h_navigation = $linterface->GET_NAVIGATION_IP25($NavigationArr);


### Toolbar
$h_approveStatusSel = $lpf_volunteer_ui->getEventUserApprovalStatusSelection('ApproveStatus', '', 'js_Changed_ApproveStatus_Selection();');	// initiated by JS
if ($lpf->IS_IPF_SUPERADMIN()) {
	$h_programTypeSel = $lpf_volunteer_ui->getEventUserProgramTypeSelection('ProgramType', '', 'js_Changed_ProgramType_Selection();');	// initiated by JS
}
$h_searchBox = $linterface->Get_Search_Box_Div('Keyword', '', 'onkeypress="js_Check_Searching(event);"');	// initiated by JS


### Table Action Button
$ActionButtonArr = array();
$ActionButtonArr[] = array('approve', 'javascript:js_Approve_Event_Student();');
$ActionButtonArr[] = array('reject', 'javascript:js_Reject_Event_Student();');
$h_actionButton = $linterface->Get_DBTable_Action_Button_IP25($ActionButtonArr);

 

$ReturnMsg = $Lang['General']['ReturnMessage'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);
include_once($template_script);
$linterface->LAYOUT_STOP();
?>