<?php
// Modifying:
/*
 * Modification Log:
 */
include_once($PATH_WRT_ROOT."includes/portfolio25/volunteer/libpf_volunteer_event.php");


$RetuenMsgKey = $_REQUEST['ReturnMsgKey'];
$EventID = trim(stripslashes($_POST['EventID']));
$IsEditMode = ($EventID=='')? false : true;


### Navigation
$NavigationArr = array(); 
$NavigationArr[] = array($Lang['iPortfolio']['VolunteerArr']['VolunteerEvent'], "javascript:js_Go_Back();");
$NavigationArr[] = array(($IsEditMode)? $Lang['Btn']['Edit'] : $Lang['Btn']['New'], '', 1);
$h_navigation = $linterface->GET_NAVIGATION_IP25($NavigationArr);


### Event Max Length Info
$CodeMaxLength = $lpf_volunteer->getEventCodeMaxLength();
$TitleMaxLength = $lpf_volunteer->getEventNameMaxLength();
$OrganizationMaxLength = $lpf_volunteer->getEventOrganizationMaxLength();

### Get Event Info if Edit Mode
if ($IsEditMode) {
	$objEvent = new libpf_volunteer_event($EventID);
	$Code = $objEvent->getCode();
	$Title = $objEvent->getTitle();
	$StartDate = $objEvent->getStartDate();
	$Organization = $objEvent->getOrganization();
}

### Buttons
$h_submitBtn = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "js_Go_Submit();", 'SubmitBtn');
$h_cancelBtn = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Go_Back();", 'CancelBtn');


$ReturnMsg = $Lang['General']['ReturnMessage'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);
include_once($template_script);
$linterface->LAYOUT_STOP();
?>