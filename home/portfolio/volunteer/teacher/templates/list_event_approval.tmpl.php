<?php
echo $linterface->Include_Cookies_JS_CSS();
?>
<script language="javascript">
var jsKeywordCookiesKey = "ck_ipf_volunteer_event_approval_list_Keyword";
var jsApproveStatusCookiesKey = "ck_ipf_volunteer_event_approval_list_ApproveStatus";
var jsProgramTypeCookiesKey = "ck_ipf_volunteer_event_approval_list_ProgramType";

var arrCookies = new Array();
arrCookies[arrCookies.length] = jsKeywordCookiesKey;
arrCookies[arrCookies.length] = jsApproveStatusCookiesKey;
arrCookies[arrCookies.length] = jsProgramTypeCookiesKey;

var jsField = '<?=$field?>';
var jsOrder = '<?=$order?>';
var jsPageNo = '<?=$pageNo?>';
var jsKeyword = '<?=$Keyword?>';


$(document).ready( function() {	
	<? if($clearCoo) { ?>
		for(i=0; i<arrCookies.length; i++)
		{
			var obj = arrCookies[i];
			//alert('obj = ' + obj);
			$.cookies.del(obj);
		}
	<? } ?>
	
	var jsTempKeyword = js_Get_Cookies_Value(jsKeywordCookiesKey)
	$('input#Keyword', 'form#form1').val(jsTempKeyword).focus();
	
	var jsTempApproveStatus = js_Get_Cookies_Value(jsApproveStatusCookiesKey);
	$('select#ApproveStatus', 'form#form1').val(jsTempApproveStatus);
	
	var jsTempProgramType = js_Get_Cookies_Value(jsProgramTypeCookiesKey);
	$('select#ProgramType', 'form#form1').val(jsTempProgramType);
	
	Blind_Cookies_To_Object();
	
	js_Reload_DBTable(1);
});

function js_Get_Cookies_Value(jsCookiesKey) {
	var jsCookiesValue = $.cookies.get(jsCookiesKey);
	jsCookiesValue = (jsCookiesValue==null || jsCookiesValue=='undefined')? '' : jsCookiesValue;
	
	return jsCookiesValue;
}

function js_Check_Searching(e) {
	if (Check_Pressed_Enter(e)) {
		js_Reload_DBTable();
	}
}

function js_Reload_DBTable(jsInitialize) {
	jsInitialize = jsInitialize || 0;
	if (jsInitialize == 1) { 
		// if first load of the table => follow current setting => do nth
	}
	else {
		// not intialize => changed filtering => start from first page
		jsPageNo = 1;
	}
	
	var jsKeyword = $('input#Keyword', 'form#form1').val();
	$.cookies.set(jsKeywordCookiesKey, jsKeyword);
	
	var jsApproveStatus = $('select#ApproveStatus', 'form#form1').val();
	$.cookies.set(jsApproveStatusCookiesKey, jsApproveStatus);
	
	var jsProgramType = $('select#ProgramType', 'form#form1').val();
	$.cookies.set(jsProgramTypeCookiesKey, jsProgramType);
		
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"index.php", 
		{
			task: 'ajax',
			script: 'ajax_reload',
			action: 'Reload_Event_Approval_List_Teacher_View_DBTable',
			field: jsField,
			order: jsOrder,
			pageNo: jsPageNo,
			Keyword: jsKeyword,
			ApproveStatus: jsApproveStatus,
			ProgramType: jsProgramType
		},
		function(ReturnData) {
			
		}
	);
}

function js_Approve_Event_Student() {
	checkApprove2(document.getElementById('form1'), 'RecordIDArr[]', 'js_Go_Approve_Event_Student()');
}

function js_Go_Approve_Event_Student() {
	$('input#task').val('ajax');
	$('input#script').val('approve_event_user');
	
	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
	$.ajax({  
		type: 'POST',
		url: 'index.php',
		data: jsSubmitString,  
		success: function(data) {
			if (data == '1') {
				js_Reload_DBTable();
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['RecordApproveSuccess']?>');
			}
			else {
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['RecordApproveUnSuccess']?>');
			}
			
			Scroll_To_Top();
		} 
	});
}

function js_Reject_Event_Student() {
	checkReject2(document.getElementById('form1'), 'RecordIDArr[]', 'js_Go_Reject_Event_Student()', globalAlertMsgRejectNoDel);
}

function js_Go_Reject_Event_Student() {
	$('input#task').val('ajax');
	$('input#script').val('reject_event_user');
	
	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
	$.ajax({  
		type: 'POST',
		url: 'index.php',
		data: jsSubmitString,  
		success: function(data) {
			if (data == '1') {
				js_Reload_DBTable();
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['RecordRejectSuccess']?>');
			}
			else {
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['RecordRejectUnSuccess']?>');
			}
			
			Scroll_To_Top();
		} 
	});
}

function js_Changed_ApproveStatus_Selection() {
	js_Reload_DBTable();
}

function js_Changed_ProgramType_Selection() {
	js_Reload_DBTable();	
}
</script>

<br />
<form id="form1" name="form1" method="POST" action="index.php" onsubmit="return false;">
	<div class="content_top_tool">
		<div class="Conntent_tool">
			<br style="clear:both" />
		</div>
		<?=$h_searchBox?>
	</div>
	<br style="clear:both" />
	
	<?=$h_navigation?>
	<br style="clear:both" />
	
	<div class="table_filter">
		<?=$h_approveStatusSel?>
		<?=$h_programTypeSel?>
	</div>
	<br style="clear:both" />
	
	<div class="table_board">
		<?=$h_actionButton?>
		<div id="DBTableDiv">
			<?=$h_dbtable?>
		</div>	
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="script" name="script" value="" />
	<input type="hidden" id="EventID" name="EventID" value="" />
</form>
<br/>