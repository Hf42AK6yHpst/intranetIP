<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("TS");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
include_once($PATH_WRT_ROOT."includes/libpf-lp2.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
intranet_opendb();

$li_pf = new libpf_lp();
$li_pf_sterec = new libpf_sturec();
$li_pf_sterec->SET_ALUMNI_COOKIE_BACKUP($ck_is_alumni);
list($WebPortfolioID, $StudentID) = $li_pf->GET_STUDENT_DECRYPTED_STRING($key);

$msg = 0;

if ($comment!="" && $StudentID!="")
{
//	$comment = HTMLtoDB($comment);
//	$author_user_id = ($ck_memberType=="P") ? "-1" : $ck_user_id;
//	$author_name = ($ck_memberType=="P") ? "parent" : $li->returnStudentName($ck_user_id);

	# Course User ID of learning portfolio owner
	$CommentArr['iPortfolio_user_id'] = $li_pf->IP_USER_ID_TO_EC_USER_ID($StudentID);
	#Siuwan 20130813 use student id if parent leave comment
	$author_user_id = (libpf_lp2::isFromKIS()&&$ck_memberType=='S')?$StudentID:$_SESSION['UserID'];
	# Course User ID of author
	$CommentArr['author_user_id'] = $li_pf->IP_USER_ID_TO_EC_USER_ID($author_user_id);
	
	# Build author name string
	$author_obj = $li_pf->GET_STUDENT_DATA($StudentID);
	$CommentArr['author'] = $author_obj[0]['ChineseName'];
	$CommentArr['author'] .= ($author_obj[0]['ChineseName']!=$author_obj[0]['EnglishName'] && trim($author_obj[0]['EnglishName'])!="") ? " ".$author_obj[0]['EnglishName'] : "";
	if(trim($author_obj[0]['ClassName'])!="" && trim($author_obj[0]['ClassNumber'])!="")
	{
		$CommentArr['author'] .= " &lt;".$author_obj[0]['ClassName']." - ".$author_obj[0]['ClassNumber']."&gt; ";
	}
	else if(trim($author_obj[0]['ClassName'])!="")
	{
		$CommentArr['author'] .= " &lt;".$author_obj[0]['ClassName']."&gt; ";
	}
	else if(trim($author_obj[0]['ClassNumber'])!="")
	{
		$CommentArr['author'] .= " &lt;".$author_obj[0]['ClassNumber']."&gt; ";
	}
	$CommentArr['author'] = addslashes($CommentArr['author']);

	$CommentArr['web_portfolio_id'] = $WebPortfolioID;
	$CommentArr['author_host'] = $REMOTE_ADDR;
	$CommentArr['comment'] = intranet_htmlspecialchars($comment);
	
	$msg = $li_pf->ADD_LEARNING_PORTFOLIO_COMMENT($CommentArr);
}
$li_pf_sterec->RELOAD_ALUMNI_COOKIE_BACKUP($ck_is_alumni);
intranet_closedb();

header("Location: student_comment.php?key=$key&msg=$msg");
?>
