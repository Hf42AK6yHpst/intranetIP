<?php
//Editing by: 
##### Change Log [Start] #####
#
#	Date:	2015-03-16	Omas
# 		add	$checkPrepare for the page download disk
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:CDBurning") || !strstr($ck_user_rights, ":web:"));
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
include_once($PATH_WRT_ROOT."includes/json.php");


intranet_opendb();
$li_pf = new libpf_lp();
$li_pf->ADMIN_ACCESS_PAGE();
$li_json = new JSON_obj();
$student_array = $li_pf->GET_STUDENTS_FOR_COPY_PORTFOLIO($YearClassID);

if($checkPrepare){

	// for CDburning_download.php
	$classPath = "$li_pf->file_path"."/portfolio_2_burn/".$YearClassID;

	foreach($student_array as $key => $_InfoArr){
		$_WebSAMSRegNo = $_InfoArr['WebSAMSRegNo'];
		$_EnglishName = $_InfoArr['EnglishName'];
		$_WebSAMSRegNo = str_replace("#", "", $_WebSAMSRegNo);
		$folder_target = $classPath."/".$_WebSAMSRegNo."_".str_replace(" ", "_", $li_pf->filename_safe($_EnglishName));
		if(file_exists($folder_target)){
			$student_array[$key]['Disabled'] = 0;
		}
		else{
			$student_array[$key]['Disabled'] = 1;
		}
	}
	
}

echo $li_json->encode($student_array);

intranet_closedb();
?>