<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Template") || !strstr($ck_user_rights, ":web:"));
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_lp();
$UserID = $_SESSION['UserID'];

# Check for iPortfolio admin user
if (!$li_pf->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if ($file_id!="")
	$msg = $li_pf->COPY_TEMPLATE($file_id, $newTitle);

intranet_closedb();
?>

<script language="Javascript">
	opener.location = "templates_manage.php?Page=<?=$Page?>&PageDivision=<?=$PageDivision?>&Field=<?=$Field?>&Order=<?=$Order?>&FieldChanged=modify&msg=<?=$msg?>";
	self.close();
</script>
