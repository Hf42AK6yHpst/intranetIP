<?php

##### Change Log [Start] #####
#
#	Date:	2015-03-16	Omas
# 			Create this file
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
iportfolio_auth("STP");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
$lf = new libfilesystem();
include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");


intranet_opendb();

$li_pf = new libpf_lp();
$lpf_ui = new libportfolio_ui();

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_BurningCD";
$CurrentPageName = $iPort['menu']['prepare_cd_rom_burning'];


### Title ###
//$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

$curTab = 'download';
$TAGS_OBJ[] = array($iPort['menu']['prepare_cd_rom_burning'], 'prepare_CDburning.php',$curTab=='prepare');
$TAGS_OBJ[] = array($iPort['menu']['download_cd_rom_burning'], 'CDburning_download.php', $curTab=='download');
$linterface->LAYOUT_START();

$loading_image = $linterface->Get_Ajax_Loading_Image();
	
$path = "$li_pf->file_path/portfolio_2_burn";
$dirList = scandir($path);

$currentYearClassInfoArr = $li_pf->GET_ALL_LEARNING_PORTFOLIO_CLASS_LIST();
$currentYearClassIDArr = Get_Array_By_Key($currentYearClassInfoArr,'YearClassID');
$preparedClassIDArr = array_intersect($currentYearClassIDArr,$dirList);

$numClass = count($currentYearClassInfoArr);
for($i=0; $i<$numClass; $i++){
    $_yearClassID = $currentYearClassInfoArr[$i]['YearClassID'];

    if(in_array($_yearClassID,$preparedClassIDArr)){
    	$_yearName = $currentYearClassInfoArr[$i]['YearName'];
    	$_yearClassName = $currentYearClassInfoArr[$i]['ClassTitleEN'];
    
    	$classSelectionArr[$_yearName][] = array($_yearClassID,$_yearClassName);
    }
}
$PreparedLPClass = $lpf_ui->GEN_CLASS_SELECTION_OPTGROUP($classSelectionArr, "name=\"YearClassID\" id=\"YearClassID\"", "", false);

// clean user temp path
if(file_exists($path.'/temp/'.$UserID)){
	$existTempPath = scandir($path.'/temp/'.$UserID);
	
	$currentTime = time();
	$cleanTargetAry = array();
	foreach((array)$existTempPath as $_key => $_timestampFolder){
		if($_key < 2){
			continue;
		}
		else{
			if( ($currentTime -$_timestampFolder) > 10800){
				$cleanTargetAry[] = $_timestampFolder;
			}
		}
	}
	
	foreach((array)$cleanTargetAry as $_timestampFolder){
		$_filePath = $path.'/temp/'.$UserID.'/'.$_timestampFolder;
		$_delFileNameAry = scandir($_filePath);
		
		foreach((array)$_delFileNameAry as $__key => $__fileName){
			if($__key < 2){// skip dir . & .. 
			continue;
			}
			else{
				$lf->file_remove($_filePath.'/'.$__fileName);
			}
		}
		$lf->deleteDirectory($_filePath);
	}
}
?>
<script language="javascript">

var student_table_top = '<tr class="tabletop"></tr><tr class="tabletop"><td width="95%" class="tabletopnolink" >Name</td><td class="tabletopnolink" ><input onclick="checkall(this)" type="checkbox" value="1"/></td></tr>';
var loading_image = '<?=$loading_image?>';
function checkform(myObj){
	var copy_alert_sentence = "<?=$ec_warning['copy_portfolio']?>";
	var remove_alert_sentence = "<?=$ec_warning['remove_copied_portfolio']?>";
	var pass = false;
	
	if($('#worktype_2').attr('checked')){
		pass = true;
	}
	else{
		$('.studentbox').each(function(index){
	    	if($(this).attr('checked')){
	    		pass = true;
	    	}
	  	});
	}
	
	if(pass){
		if($("#YearClassID option:selected").val() == "")
		{
			alert("<?=$ec_warning['please_select_class']?>");
			return false;
		}
	
		//var type = parseInt($('input:radio[name=work_type]:checked').val());
	
		//alert_sentence = (type==1) ? copy_alert_sentence.replace("XXX", $("#YearClassID option:selected").text()) : remove_alert_sentence.replace("XXX", $("#YearClassID option:selected").text());
	
		$('#StudentList').hide();
		$('#loading').append("<br /><br /><?=$Lang['iPortfolio']['alertLeavePage']?>");
		$('#loading').show();
		$('.formbutton').hide();
		$('#submitButton').hide();
		ajaxDownloadRequest();
		return false; //by ajax - no need form submit

	}
	else{
		alert("<?=$ec_warning['select_student']?>");
		return false;
	}
}

function ajaxDownloadRequest(){
	 var url = "CDburning_download_perform.php";
	 
	$.ajax({
           type: "POST",
           url: url,
           data: $("#form1").serialize(), 
           success: function(data)
           {
               $('#loading').hide();
               $('#downloadFile').show(); 
               $('#downloadFile').html(data);
               var a = $('#downloadFile').html() + "&nbsp;&nbsp;<input onClick='location.reload();' name='button' type='button' value='<?=$langpf_lp2['admin']['navi']['back']?>' class='formbutton'>"
               $('#downloadFile').html(a);
               $('#YearClassID').attr('disabled',true);
           }
         });
}

$(document).ready(function() {
	$('#loading').show();
	var YearClassID = $('#YearClassID').val();
	$.getJSON('json_return_studentlist.php?checkPrepare=1&YearClassID='+YearClassID, function(json) {
			$('#loading').hide();
			$('#StudentList').append(student_table_top);
			$.each(json, function(i,item){
				
				if(item.Disabled){
					var disabled = 'disabled';
				}
				else{
					var disabled = '';
				}
				
				$('#StudentList').append('<tr><td bgcolor="#ffffff" class="tabletext">'+item.StudentName+'</td><td bgcolor="#ffffff" class="tabletext"><input class="studentbox" name="student['+ item.CourseUserID+']" type="checkbox" value="1" '+disabled+'/></td></tr>');
			});
		});
	
    $('#YearClassID').change(function(){
    	$('#StudentList').html("");
    	$('#loading').show();
    	YearClassID = $(this).val();
		$.getJSON('json_return_studentlist.php?checkPrepare=1&YearClassID='+YearClassID, function(json) {
			$('#loading').hide();
			$('#StudentList').append(student_table_top);
			$.each(json, function(i,item){
				
				if(item.Disabled){
					var disabled = 'disabled';
				}
				else{
					var disabled = '';
				}
				
				$('#StudentList').append('<tr><td bgcolor="#ffffff" class="tabletext">'+item.StudentName+'</td><td bgcolor="#ffffff" class="tabletext"><input class="studentbox" name="student['+ item.CourseUserID+']" type="checkbox" value="1" '+disabled+'/></td></tr>');
			});
		});
    });

});

function checkall(obj){
	if($(obj).attr("checked")){
		
		$('.studentbox').each(function(){

	    if($(this).attr('disabled')){
	
	    }else{
	    	$(this).attr('checked', true);
	    }
	
	 	});
			
	}
	else{
		$('.studentbox').attr('checked', false);
	}
	
}
</script>
<form name="form1" id="form1" method="post" action="CDburning_download_perform.php" onSubmit="return checkform(this);">

<table id="html_body_frame" width="96%" border="0" cellspacing="0" cellpadding="8">
	<tr>
		<td align="center">
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td><?=$linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $Lang['iPortfolio']['DownloadDisk']['Instruction']);?></td>
				</tr>
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
								<td valign="top"><?=$PreparedLPClass?></td>
							</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table width="80%" bgColor="#cccccc" border="0" cellSpacing="1" cellPadding="4" id="StudentList">
						</table>
						<div style="display:none" id="loading"><br /><?=$loading_image?></div>
						<div style="display:none" id="downloadFile"></div>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<input type="submit" id="submitButton" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$button_submit?>" <?=$DisableSubmit?> />
									<input type="button" class="formbutton" value="<?=$button_cancel?>"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" onClick="self.location='../../school_records.php'">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</form>
<?php	
$linterface->LAYOUT_STOP();
intranet_closedb();
?>