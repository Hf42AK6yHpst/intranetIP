<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

include_once($eclass_filepath.'/src/includes/php/lib-groups.php');

intranet_opendb();

$li_pf = new libpf_lp();

$LP_info = $li_pf->GET_LEARNING_PORTFOLIO($WebPortfolioID, $StudentID);
$template_display = $li_pf->GEN_TEMPLATE_SELECTION_TABLE($LP_info['template_selected']);

/*
$nt = new notes_iPortfolio("", $web_portfolio_id);
$template_navigation_html = $nt->getTemplatesPageNavigation($pageNo, $web_portfolio_id);
$template_table_html = $nt->getTemplatesTable($pageNo, $web_portfolio_id);

$scheme_obj = $nt->getSchemeInfo($web_portfolio_id);
$scheme_obj = $scheme_obj[0];
*/
// Get class selection with optgroup
$class_name = $i_general_WholeSchool;
$lpf_fc = new libpf_formclass();
$t_classlevel_arr = $lpf_fc->GET_CLASSLEVEL_LIST();
for($i=0; $i<count($t_classlevel_arr); $i++)
{
  $t_classlevel_id = $t_classlevel_arr[$i]["YearID"];
  $t_classlevel_name = $t_classlevel_arr[$i]["YearName"];

  $lpf_fc->SET_CLASS_VARIABLE("YearID", $t_classlevel_id);
  $t_class_arr = $lpf_fc->GET_CLASS_LIST();
  
  for($j=0; $j<count($t_class_arr); $j++)
  {
    $t_yc_id = $t_class_arr[$j][0];
    $t_yc_title = Get_Lang_Selection($t_class_arr[$j]['ClassTitleB5'], $t_class_arr[$j]['ClassTitleEN']);
    if(!is_array($class_teach_arr) || in_array($t_yc_id, $class_teach_arr))
    {
      $class_arr[$t_classlevel_name][] = array($t_yc_id, $t_yc_title);
    }
    
    if($t_yc_id == $YearClassID)
    {
      $class_name = $t_yc_title;
    }
  }
}
//$class_selection_html = getSelectByArray($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, 1, 0, "", 2);
$lpf_ui = new libportfolio_ui();
$class_selection_html = $lpf_ui->GEN_CLASS_SELECTION_OPTGROUP($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID);

# Get student selection
$lpf_fc->SET_CLASS_VARIABLE("YearID", "");
$lpf_fc->SET_CLASS_VARIABLE("YearClassID", $YearClassID);
$default_student_id = "";
$student_id_arr = $lpf_fc->GET_STUDENT_LIST();
$t_student_detail_arr = $lpf_fc->GET_STUDENT_DETAIL_LIST($student_id_arr);
if(is_array($t_student_detail_arr))
{
  for($i=0; $i<count($t_student_detail_arr); $i++)
  {
    $t_classname = $t_student_detail_arr[$i]['ClassName'];
    if(in_array($t_classname, $class_arr)) continue;
  
    $t_user_id = $t_student_detail_arr[$i]['UserID'];
    $t_user_name = "(".$t_classname." - ".$t_student_detail_arr[$i]['ClassNumber'].") ";
    $t_user_name .= Get_Lang_Selection($t_student_detail_arr[$i]['ChineseName'], $t_student_detail_arr[$i]['EnglishName']);
    
    # Set default user ID if class is changed
    if($StudentID == $t_user_id)
      $default_student_id = $StudentID;
  
    $student_detail_arr[] = array($t_user_id, $t_user_name);
  }
}
if($default_student_id == "") $default_student_id = $student_detail_arr[0][0];
$student_selection_html = getSelectByArray($student_detail_arr, "name='StudentID' onChange='jCHANGE_FIELD()'", $default_student_id, 0, 1, "", 2);

# Get activated students
$act_student_id_arr = $lpf_fc->GET_ACTIVATED_STUDENT_LIST();

//$template_table_top_right = $li_pf->GEN_STUDENT_LIST_INFO($ClassName, $StudentID);

$linterface = new interface_html();
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];
### Title ###
$TAGS_OBJ[] = array($iPort['menu']['student_account'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

# Retrieve student info
$student_obj = $li_pf->GET_STUDENT_OBJECT($StudentID);

# Set links for photo
if(strpos($student_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $student_obj['PhotoLink'] != "")
	$student_obj['PhotoLink'] = str_replace("<!--ImageStyle-->", "", $student_obj['PhotoLink']);
else if($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
else
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";

//$ClassSelection = $li_pf->GEN_CLASS_SELECTION_TEACHER($ClassName, "", true, false, false);

# Generate student selection drop-down list and get a student list
list($StudentSelection, $student_list) = $li_pf->GEN_STUDENT_LIST_INFO($ClassName, $StudentID);

$linterface->LAYOUT_START();
?>
<? // ===================================== Body Contents ============================= ?>

<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax.js"></script>

<script language="JavaScript">

function jSUBMIT_FORM(){
	document.form1.action = "notes_setting_update.php";
	document.form1.submit();
}

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	if(jParField == 'classname' && document.form1.ClassName.value == '')
		return;
	
	document.form1.FieldChanged.value = jParField;
	document.form1.submit();
}

// Quick student switch 
function jCHANGE_STUDENT(jParShift, jParToExtreme)
{
	var StudentSelect = document.getElementsByName('StudentID');
	var OriginalIndex = StudentSelect[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(!jParToExtreme)
	{
		if(TargetIndex >= StudentSelect[0].length)
		{
			StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			return;
		}
		else if(TargetIndex < 0)
		{
			StudentSelect[0].selectedIndex = 0;
			return;
		}
		else
			StudentSelect[0].selectedIndex = TargetIndex;
	}
	else
	{
		if(jParShift < 0)
		{
			if(OriginalIndex > 0)
				StudentSelect[0].selectedIndex = 0;
			else
				return;
		}
		else
		{
			if(OriginalIndex < StudentSelect[0].length-1)
				StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			else
				return;
		}
	}
	
	document.form1.submit();
}

// Search student name
function jSUBMIT_SEARCH(){
	document.form1.FieldChanged.value = "search";
	document.form1.action = "../../school_records_class.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_SI()
{
	document.form1.action = "../../profile/student_info_teacher.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_SR()
{
	document.form1.action = "../../profile/school_record_teacher.php";
	document.form1.submit();
}

// Change page to display school base scheme
function jTO_SBS()
{
	document.form1.action = "../../profile/sbs/index.php";
	document.form1.submit();
}

/*
functions for select template
1. jSELECT_TEMPLATE
2. CompleteSelectTemplate
*/
function jSELECT_TEMPLATE(jParTemplate){

  xmlHttp_template = GetXmlHttpObject();
  if (xmlHttp_template == null)
  {
    alert ("Your browser does not support AJAX!");
    return;
  }

  // Modify Layer Content
  var url="../../ajax/select_template_ajax.php";
  url=url+"?template=" + jParTemplate;
  url=url+"&WebPortfolioID=<?=$WebPortfolioID?>";
  url=url+"&StudentID=<?=$StudentID?>";
  url=url+"&sid="+Math.random();

  xmlHttp_template.onreadystatechange = CompleteSelectTemplate;
  xmlHttp_template.open("GET",url,true);
  xmlHttp_template.send(null);
}

function CompleteSelectTemplate(){
  if (xmlHttp_template.readyState==4)
  {
    var Template_HTML = xmlHttp_template.responseText;
    var TemplateLayer = document.getElementById("TemplateSelectDiv");
    TemplateLayer.innerHTML = Template_HTML;
  }
}

</script>

<form name="form1" action="../../profile/learning_portfolio_teacher.php">

	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan=2>
				<table border="0" cellspacing="0" cellpadding="3" width="100%">
					<tr>
						<td>
							<?=$class_selection_html?>
						</td>
						<td align="right">
							<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$ec_iPortfolio['enter_student_name']:intranet_htmlspecialchars(stripslashes($search_name)))?>" onFocus="if(this.value=='<?=$ec_iPortfolio['enter_student_name']?>'){this.value=''}" onBlur="if(this.value==''){this.value='<?=$ec_iPortfolio['enter_student_name']?>'}" />
							<input type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onClick="jSUBMIT_SEARCH()" />
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td class="navigation">
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../../school_records.php"><?=$ec_iPortfolio['class_list']?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../../school_records_class.php?YearClassID=<?=$YearClassID?>"><?=$class_name?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><?=($intranet_session_language=="b5"?$student_obj['ChineseName']:$student_obj['EnglishName'])?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="17" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_01_tea.gif" width="17" height="37"></td>
						<td height="37" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_03_tea.gif">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="200">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center" valign="middle" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_02_tea.gif"  class="page_title"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_learning_portfolio.gif" alt="<?=$iPort['menu']['learning_portfolio']?>" width="20" height="20" border="0" align="absmiddle"> <?=$iPort['menu']['learning_portfolio']?></td>
											</tr>
										</table>
									</td>
									<td align="left" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#E8E8E8">
											<tr>
												<td align="left">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr align="center" valign="middle">
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(-1, true)" class="tablebottomlink">&lt;&lt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(-1, false)" class="tablebottomlink">&lt;</a></td>
															<td nowrap>
																<span class="tabletext">
																	<?=$student_selection_html?>
																</span>
															</td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(1, false)" class="tablebottomlink">&gt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(1, true)" class="tablebottomlink">&gt;&gt;</a></td>
															<td width="15" nowrap> | </td>
															<td nowrap class="tabletext"><?=str_replace("<!--NoRecord-->", count($student_list), $ec_iPortfolio['total_record'])?></td>
														</tr>
													</table>
												</td>
												<td align="right">
													<table border="0" cellspacing="0" cellpadding="5">
														<tr>
															<td nowrap background="#"></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_04_tea.gif" width="13" height="37"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_05.gif" width="17" height="20"></td>
						<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_06.gif">
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<tr>
									<td valign="top">
										<table border="0" cellspacing="0" cellpadding="0" >
											<tr>
												<td>
													<table border="0" cellspacing="0" cellpadding="0" >
														<tr>
															<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_01.gif" style="padding-left:10px; padding-top:10px;"><?=$student_obj['PhotoLink']?></td>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="10" height="10"></td>
														</tr>
														<tr>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
															<td height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_04.gif" width="10" height="10"></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" class="tabletext"><?=$intranet_session_language=="b5"?$student_obj['ChineseName']:$student_obj['EnglishName']?></td>
											</tr>
											<tr>
												<td align="center">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr>
															<td valign="top"><a href="#" onClick="jTO_SI()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_stu_info.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0"></a></td>
<?php
	if(in_array($default_student_id, $act_student_id_arr)) {
		if($li_pf->HAS_SCHOOL_RECORD_VIEW_RIGHT()) {
?>
															<td valign="top"><a href="#" onClick="jTO_SR()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record.gif" alt="<?=$ec_iPortfolio['school_record']?>" width="20" height="20" border="0"></a></td>
<?php
		}
?>
															<td valign="top">
																<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_learning_portfolio_on.gif" alt="<?=$iPort['menu']['learning_portfolio']?>" width="20" height="20" border="0"><br>
																<?=$li_pf->GET_LP_NOT_VIEW_COUNT_BY_USER_ID($StudentID) > 0 ? "<span class='new_alert'>New</span>" : ""?>
															</td>
<?php
		if(strstr($ck_user_rights, ":growth:") && $li_pf->HAS_RIGHT("growth_scheme")) {
?>
			<td valign="top"><a href="javascript:jTO_SBS()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_sbs.gif" alt="<?=$iPort['menu']['school_based_scheme']?>" width="20" height="20" border="0"></a></td>
<?php
		}
	}
?>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="5">
											<tr>
												<td valign="top">
													<table width="100%" border="0" cellspacing="0" cellpadding="5">
														<tr>
															<td class="navigation">
																<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
																<a href="#" onClick="document.form1.submit()"><?=$iPort['menu']['learning_portfolio']?></a>
																<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
																<?=$LP_info['title']?>
																<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
																<?=$i_general_settings?>
															</td>
														</tr>
													</table>
												</td>
												<td align="right">
													<span class="tabletext">
<!--
														<a href="#"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_view.gif" width="20" height="20" border="0"></a><a href="#"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_edit_b.gif" width="20" height="20" border="0"></a><br />
-->
														<?=$ec_iPortfolio['last_update']?> : <?=$LP_info['modified']?> <img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"> <?=$ec_iPortfolio['publish_date']?> : <?=$LP_info['notes_published']?></span>
												</td>
											</tr>
										</table>
										<table width="88%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
											<tr>
												<td>
													<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
														<tr>
															<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$iPort["lp_skin"]?>:</span></td>
														</tr>
														<tr>
															<td align="center" valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;">
																<div id="TemplateSelectDiv">
																	<?=$template_display?>
																</div>
															</td>
														</tr>
														<tr>
															<td valign="top" nowrap="nowrap">
																<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
																	<tr>
																		<td valign="top" nowrap="nowrap"><span class="tabletext"><?=$button_public?>:</span></td>
																		<td width="80%" valign="top">
																			<span class="tabletext">
																				<input name="allow_review" type="radio" value="1" <?=($LP_info['allow_review']==1?"checked":"")?> /> <?=$i_general_yes?>
																				<input name="allow_review" type="radio" value="0" <?=($LP_info['allow_review']==0?"checked":"")?> /> <?=$i_general_no?>
																			</span>
																		</td>
																	</tr>
	<!--
	Todo: Share to group
																	<tr valign="top">
																		<td valign="top" nowrap="nowrap" ><span class="tabletext">Share to :</span></td>
																		<td><label for="grading_honor" class="tabletext"></label>
																			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
																				<tr valign="top">
																					<td width="40%"><span class="tabletext">Choose Student :</span></td>
																					<td width="10" nowrap="nowrap"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
																					<td width="60%" nowrap="nowrap"><span class="tabletext">Selected Student(s) :</span></td>
																				</tr>
																				<tr>
																					<td valign="top" class="tablerow2">
																						<table width="100%" border="0" cellpadding="5" cellspacing="0">
																							<tr>
																								<td class="tabletext">from <strong>Class / Group</strong> </td>
																							</tr>
																							<tr>
																								<td class="tabletext"><input name="submit332" type="submit" class="formsubbutton" value="Select"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'"></td>
																							</tr>
																							<tr>
																								<td class="tabletext">or </td>
																							</tr>
																							<tr>
																								<td class="tabletext">by<strong> entering class and class number </strong></td>
																							</tr>
																							<tr>
																								<td class="tabletext"><input name="text3" type="text" class="tabletext" /></td>
																							</tr>
																							<tr>
																								<td class="tabletext">or </td>
																							</tr>
																							<tr>
																								<td class="tabletext">by <strong>entering student's name </strong></td>
																							</tr>
																							<tr>
																								<td class="tabletext"><input name="text32" type="text" class="tabletext" /></td>
																							</tr>
																						</table>
																					<td valign="top" nowrap="nowrap">&nbsp;</td>
																					<td valign="top" nowrap="nowrap">
																						<table width="100%" border="0" cellpadding="3" cellspacing="0">
																							<tr>
																								<td>
																									<p>
																										<select name="select2" size="15" class="select_studentlist">
																											<option >Wong Dai Man (4A- 01)</option>
																										</select>
																									</p>
																								</td>
																							</tr>
																							<tr>
																								<td align="right"><input name="submit33" type="submit" class="formsubbutton" value="Delete"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'"></td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																			<label for="grading_passfail" class="tabletext"></label>
																		</td>
																	</tr>
	-->
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
											</tr>
											<tr>
												<td align="right">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td align="right">
																<input type="button" class="formbutton" value="<?=$button_save?>"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" onClick="jSUBMIT_FORM()" />
																<input type="button" class="formbutton" value="<?=$button_cancel?>" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" onClick="document.form1.submit()" />
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<br>
										<br>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_07.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_07.gif" width="13" height="37"></td>
					</tr>
					<tr>
						<td width="17" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_08.gif" width="17" height="17"></td>
						<td height="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_09.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_09.gif" width="64" height="17"></td>
						<td width="13" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_10_b.gif" width="13" height="17"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

<input type="hidden" name="WebPortfolioID" value="<?=$WebPortfolioID?>" />
<input type="hidden" name="FieldChanged" />
</form>

<? // ===================================== Body Contents (END) ============================= ?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
