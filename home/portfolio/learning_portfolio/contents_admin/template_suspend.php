<?php
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Template") || !strstr($ck_user_rights, ":web:"));
//auth("TA");
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_lp();

$li_pf->SET_TEMPLATE_ACTIVATE($Suspend, $file_ids);

intranet_closedb();
if($Suspend == 1)
	header("Location: templates_manage.php?PageDivision=$PageDivision&Page=$Page&Order=$Order&Field=$Field&FieldChanged=modify&msg=suspend");
else
	header("Location: templates_manage.php?PageDivision=$PageDivision&Page=$Page&Order=$Order&Field=$Field&FieldChanged=modify&msg=activate");
?>
