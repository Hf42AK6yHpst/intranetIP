<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Template") || !strstr($ck_user_rights, ":web:"));
//auth("TA");
iportfolio_auth("T");  //comment , check with eric , fai

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
//include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-lp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_lp();

$ErrorArr = $li_pf->DELETE_TEMPLATE($file_ids);

if(sizeof($ErrorArr)!=0)
{
	$failedID = implode(",", $ErrorArr);
	$msg = "delete_following_failed";
}
else
{
	$msg = "delete";
}

intranet_closedb();
header("Location: templates_manage.php?PageDivision=$PageDivision&Order=$Order&Field=$Field&msg=$msg&failedID=$failedID");
?>
