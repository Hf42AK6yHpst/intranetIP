<?php

// Modifying by 

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:CDBurning") || !strstr($ck_user_rights, ":web:"));
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_lp();
$li_pf->ADMIN_ACCESS_PAGE();
$thisUserID = $_SESSION['UserID'];

// check iportfolio admin user
if (!$li_pf->IS_ADMIN_USER($thisUserID))
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$year_class = new year_class($YearClassID);
$ClassName = $year_class->Get_Class_Name();

if($resultFrom=="copy")
{
	$message = str_replace("XXX", $ClassName, $ec_iPortfolio['finished_cd_burning_preparation']);	
	$message .= "<br /><span class=\"steptitletext\">".base64_decode($path)."</span>";
	$remind_msg = $ec_iPortfolio['get_prepared_portfolio'];
	$btn = "<input onClick='javascript:self.location.href=\"prepare_CDburning_download.php?YearClassID=".$YearClassID."&path=".$path."\"' name='button' type=button value='&nbsp;".$ec_iPortfolio['make_zip_download']."&nbsp;' class='formbutton'>&nbsp;";
	$btn .= "<input onClick='javascript:self.location.href=\"prepare_CDburning.php\"' name='button' type=button value='".$ec_iPortfolio['prepare_other_class_cd_burning']."' class='formbutton'>";
}
else
{
	$message = ($x==1) ? str_replace("XXX", $ClassName, $ec_iPortfolio['copied_portfolio_reomved']) : str_replace("XXX", $ClassName, $ec_iPortfolio['copied_portfolio_reomved_fail']);
	$btn = "<input onClick='javascript:self.location.href=\"prepare_CDburning.php\"' name='button' type=button value='".$button_finish."' class='formbutton'>";
}

$result_display = "<br /><br />";
$result_display .= "<table border='0' cellpadding='8' cellspacing='0' width='80%'>";
$result_display .= "<tr><td class='chi_content_15'>".$message."</td></tr>\n";
$result_display .= ($remind_msg!="") ? "<tr><td class='chi_content_15'>".$remind_msg."</td></tr>\n" : "";
$result_display .= "<tr><td><br /></td></tr>\n";
//$result_display .= "<tr><td align='center'><input onClick='javascript:self.location.href=\"prepare_CDburning_ClassList.php\"' name='button' type=button value='".$ec_iPortfolio['prepare_other_class_cd_burning']."' class='button_g'>&nbsp;&nbsp;<input onClick='self.location.href=\"main.php\"' name='button' type=button value='".$button_finish."' class='button_g'></td></tr>\n";
$result_display .= "<tr><td align='center'>".$btn."</td></tr>\n";
$result_display .= "</table>\n";

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_BurningCD";
$CurrentPageName = $iPort['menu']['prepare_cd_rom_burning'];

$luser = new libuser($thisUserID);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<? // ===================================== Body Contents ============================= ?>

<?=$result_display ?>

<? // ===================================== Body Contents (END) ============================= ?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
