<?php
//Editing by: 
##### Change Log [Start] #####
#
#	Date:	2015-03-16	Omas
# 			Add a tab for download prepared classes
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_lp();
$li_pf->ADMIN_ACCESS_PAGE();
$thisUserID = $_SESSION['UserID'];

// check iportfolio admin user
if (!$li_pf->IS_ADMIN_USER($thisUserID))
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Generate a class selection drop-down list
$lpf_ui = new libportfolio_ui();
//$t_yc_arr = $li_pf->GET_PUBLISH_LEARNING_PORTFOLIO_CLASS_LIST();
$t_yc_arr = $li_pf->GET_ALL_LEARNING_PORTFOLIO_CLASS_LIST();
//DEBUG_R($t_yc_arr);
if(count($t_yc_arr) > 0)
{
  
  for($i=0; $i<count($t_yc_arr); $i++)
  {
    $t_y_name = $t_yc_arr[$i][0];
    $t_yc_id = $t_yc_arr[$i][1];
    $t_yc_name = $t_yc_arr[$i][2];
  
    $yc_arr[$t_y_name][] = array($t_yc_id, $t_yc_name);
  }
  
  $PublishedLPClass = $lpf_ui->GEN_CLASS_SELECTION_OPTGROUP($yc_arr, "name=\"YearClassID\" id=\"YearClassID\"", "", false);
}
else
{
	$PublishedLPClass = "<i>".$no_record_msg."</i>";
	$DisableSubmit = "DISABLED";
}

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_BurningCD";
$CurrentPageName = $iPort['menu']['prepare_cd_rom_burning'];

$luser = new libuser($thisUserID);

### Title ###
//$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");
$curTab = 'prepare';
$TAGS_OBJ[] = array($iPort['menu']['prepare_cd_rom_burning'], 'prepare_CDburning.php',$curTab=='prepare');
$TAGS_OBJ[] = array($iPort['menu']['download_cd_rom_burning'], 'CDburning_download.php', $curTab=='download');
$linterface->LAYOUT_START();

$loading_image = $linterface->Get_Ajax_Loading_Image();

//if($sys_custom['iPortfolio_BurnCD_FullReport']){  //  turn on this for all school 20101202
$html_freport = "<input type=\"checkbox\" name=\"with_freport\" id=\"with_freport\" value=\"1\"><label for=\"with_freport\">{$ec_iPortfolio['with_freport']}</label><br />";
//}
if($sys_custom['iPortfolio_BurnCD_Attachment']){
	$html_attachement = '<input type="checkbox" name="with_attachment" id="with_attachment" value="1"><label for="with_attachment">'.$ec_iPortfolio['with_attachment'].'</label>';
}
?>

<?php // ===================================== Body Contents ============================= ?>
<script language="javascript">

var student_table_top = '<tr class="tabletop"></tr><tr class="tabletop"><td width="95%" class="tabletopnolink" >Name</td><td class="tabletopnolink" ><input onclick="checkall(this)" type="checkbox" value="1"/></td></tr>';
var loading_image = '<?=$loading_image?>';
function checkform(myObj){
	var copy_alert_sentence = "<?=$ec_warning['copy_portfolio']?>";
	var remove_alert_sentence = "<?=$ec_warning['remove_copied_portfolio']?>";
	var pass = false;
	
	if($('#worktype_2').attr('checked')){
		pass = true;
	}
	else{
		$('.studentbox').each(function(index){
	    	if($(this).attr('checked')){
	    		pass = true;
	    	}
	  	});
	}
	
	if(pass){
		if($("#YearClassID option:selected").val() == "")
		{
			alert("<?=$ec_warning['please_select_class']?>");
			return false;
		}
	
		var type = parseInt($('input:radio[name=work_type]:checked').val());
	
		alert_sentence = (type==1) ? copy_alert_sentence.replace("XXX", $("#YearClassID option:selected").text()) : remove_alert_sentence.replace("XXX", $("#YearClassID option:selected").text());
	
		if(confirm(alert_sentence)){
			$('#StudentList').hide();
			$('#loading').append("<br /><br /><?=$Lang['iPortfolio']['alertLeavePage']?>");
			$('#loading').show();
			$('.formbutton').hide();
			return true;
		}
		else{
			return false;
		}
	}
	else{
		alert("<?=$ec_warning['select_student']?>");
		return false;
	}
}

function checkall(obj){
	if($(obj).attr("checked")){
		$('.studentbox').attr('checked', true);			
	}
	else{
		$('.studentbox').attr('checked', false);
	}
	
}

$(document).ready(function() {
	$('#loading').show();
	var YearClassID = $('#YearClassID').val();
	$.getJSON('json_return_studentlist.php?YearClassID='+YearClassID, function(json) {
			$('#loading').hide();
			$('#StudentList').append(student_table_top);
			$.each(json, function(i,item){
				$('#StudentList').append('<tr><td bgcolor="#ffffff" class="tabletext">'+item.StudentName+'</td><td bgcolor="#ffffff" class="tabletext"><input class="studentbox" name="student['+ item.CourseUserID+']" type="checkbox" value="1"/></td></tr>');
			});
		});
	
    $('#YearClassID').change(function(){
    	$('#StudentList').html("");
    	$('#loading').show();
    	YearClassID = $(this).val();
		$.getJSON('json_return_studentlist.php?YearClassID='+YearClassID, function(json) {
			$('#loading').hide();
			$('#StudentList').append(student_table_top);
			$.each(json, function(i,item){
				$('#StudentList').append('<tr><td bgcolor="#ffffff" class="tabletext">'+item.StudentName+'</td><td bgcolor="#ffffff" class="tabletext"><input class="studentbox" name="student['+ item.CourseUserID+']" type="checkbox" value="1"/></td></tr>');
			});
		});
    });

});

</script>

<form name="form1" method="post" action="prepare_CDburning_update.php" onSubmit="return checkform(this);">
<table id="html_body_frame" width="96%" border="0" cellspacing="0" cellpadding="8">
	<tr>
		<td align="center">
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
								<td valign="top"><?=$PublishedLPClass?></td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">&nbsp;</span></td>
								<td width="80%" class="tabletext">
									<input type="radio" name="work_type" id="worktype_1" value="1" checked><label for="worktype_1"><?=$ec_iPortfolio['copy_web_portfolio_files']?></label> 
									<input type="radio" name="work_type" id="worktype_2" value="2"><label for="worktype_2"><?=$ec_iPortfolio['remove_copied_files']?></label>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;">&nbsp;</td>
								<td valign="top">
									<input type="checkbox" name="with_comment" id="with_comment" value="1"><label for="with_comment"><?=$ec_iPortfolio['with_comment']?></label><br />
									<?=$html_freport?>
									<input type="checkbox" name="with_slp" id="with_slp" value="1"><label for="with_slp"><?=$ec_iPortfolio['with_slp']?></label><br />
									<input type="checkbox" name="with_sbs" id="with_sbs" value="1"><label for="with_sbs"><?=$ec_iPortfolio['with_sbs']?></label><br />
									<?=$html_attachement?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table width="80%" bgColor="#cccccc" border="0" cellSpacing="1" cellPadding="4" id="StudentList">
						</table>
						<div style="display:none" id="loading"><br /><?=$loading_image?></div>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<input type="submit" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$button_submit?>" <?=$DisableSubmit?> />
									<input type="button" class="formbutton" value="<?=$button_cancel?>"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" onClick="self.location='../../school_records.php'">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</form>

<?php // ===================================== Body Contents (END) ============================= ?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
