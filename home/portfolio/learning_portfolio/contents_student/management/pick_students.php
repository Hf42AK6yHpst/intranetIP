<?php

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("S");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_lp();
$linterface = new interface_html();

$ClassSelection = $li_pf->GEN_CLASS_SELECTION_STUDENT($ClassName);

if($ClassName != "")
	$StudentSelection = $li_pf->GEN_STUDENT_SELECTION_STUDENT($ClassName);

include_once($PATH_WRT_ROOT."templates/".$LAYOUT_SKIN."/layout/popup_header.php");
?>

<script language="javascript">
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?=$fieldname?>"];

     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;

     while(i!=-1)
        {
         addtext = obj.options[i].text;

          par.checkOptionAdd(parObj, addtext, obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
//     par.form1.flag.value = 0;
//     par.generalFormSubmitCheck(par.form1);
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

function jCHANGE_FIELD(jParField)
{
	document.form1.FieldChanged.value = jParField;
	document.form1.action = "pick_students.php";
	document.form1.submit();
}
</script>

<FORM method='POST' name='form1'>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
		<td width="21"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/top_01.gif" width="21" height="31"></td>
		<td align="left" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/top_02.gif">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/top_01.gif" class="popuptitle"><?=$iDiscipline['select_students']?></td>
					<td width="30"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/top_02b.gif" width="30" height="31"></td>
        </tr>
      </table>
		</td>
		<td width="22"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/top_03.gif" width="22" height="31"></td>
	</tr>
	<tr>
		<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/board04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/board04.gif" width="21" height="16"></td>
		<td align="center" bgcolor="#FFFFFF">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left">&nbsp;</td>
							</tr>
							<tr>
								<td valign="top">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td valign="top" nowrap="nowrap" class="tabletext"><?= $iDiscipline['class']?>: </td>
											<td><?=$ClassSelection?></td>
										</tr>
<?php if($ClassName != "") { ?>
										<tr>
											<td class="dotline" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
										</tr>
										<tr><td colspan="2" height="5"></td></tr>
										<tr>
											<td valign="top" nowrap="nowrap" class="tabletext"><?= $iDiscipline['students']?>: </td>
											<td width="80%" style="align: left">
												<table border="0" cellpadding="0" cellspacing="0" align="left">
													<tr>
														<td>
															<?=$StudentSelection ?>
														</td>
														<td style="vertical-align:bottom">
															<table width="100%" border="0" cellspacing="0" cellpadding="6">
																<tr>
																	<td align="left">
																		<?= $linterface->GET_BTN($button_add, "button", "checkOption(document.form1.elements['StudentID[]']);AddOptions(document.form1.elements['StudentID[]'])")?>
																	</td>
																</tr>
																<tr>
																	<td align="left">
																		<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.form1.elements['StudentID[]']); return false;")?>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
		<?php } ?>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/board06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/board06.gif" width="22" height="10"></td>
	</tr>
	<tr>
		<td height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/board07.gif" width="21" height="10"></td>
		<td  height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/board08.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/board08.gif" width="21" height="10"></td>
		<td height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/board09.gif" width="22" height="10"></td>
	</tr>
</table>

<input type="hidden" name="FieldChanged" />
<input type="hidden" name="fieldname" value="<?=$fieldname?>" />
</form>

<?
include_once($PATH_WRT_ROOT."templates/".$LAYOUT_SKIN."/layout/popup_footer.php");
intranet_closedb();
?>
