<?
/**
 * Change Log:
 * 2019-05-08 Pun [160276] [ip.2.5.10.6.1]
 *  - Fix hardcoded http
 */
if ($is_owner){
    switch($action){
	case 'setTheme':

	    if (!in_array($mode, array('prototype','draft'))){
		$liblp2->setPortfolioMode('draft');
	    }

	    $banner_editable = $liblp2->hasSuperPermission('banner');
	    if (!$banner_uploaded  && $banner_editable){//user disabled banner

		$banner_url = "";

	    }else if($_SESSION['iPortfolio_uploaded_banner_url'][$portfolio]){//user just uploaded a banner

		$banner_url = $fm->copy_fck_flash_image_upload($portfolio, $_SESSION['iPortfolio_uploaded_banner_url'][$portfolio], '../../', $cfg['fck_image']['iPortfolio']);
		unset($_SESSION['iPortfolio_uploaded_banner_url'][$portfolio]);

	    }else{//nothing happened, use old banner

		$banner_url = $portfolio_config['banner_url'];
		$banner_status = $banner_editable?$banner_status:$portfolio_config['banner_status'];
	    }

	    $liblp2->updatePortfolioConfig(array(
				    'template_selected'	=> $theme,
				    'banner_status'	=> $banner_status,
				    'custom_root'	=> $custom_root,
				    'banner_url'	=> $banner_url
				    ));

        break;
	case 'publishPortfolio':

	    $liblp2->publishPortfolioDraft($force_status ,$allow_like);
	    echo libpf_lp2::getPortfolioUrl($portfolio);
        break;
	case 'getPublish':

	    $liblp2->setPortfolioMode('draft');

	    $data['elements'] = $liblp2->getAllElements();
	    include("templates/publish.php");

        break;

        case 'setShareFriends':
	    $liblp2->createFriends($friend_ids);

	break;
	case 'getShareSearchResult':
	    $students = $liblp2->getClassStudents($class, $keyword, $exclude);
	    foreach ($students as $student){
		echo "<option value='".$student['user_id']."'>".$student['name'].' ('.$student['class'].')</option>';
	    }
	break;

	case 'getThemeSettings':

	    if (!in_array($mode, array('prototype','draft'))){
		$liblp2->setPortfolioMode('draft');
	    }
	    $portfolio_config 	= $liblp2->getPortfolioConfig();
	    $title 		= $portfolio_info['title'];
	    $themes 		= libpf_lp2::$themes;
	    $banner_editable 	= $liblp2->hasSuperPermission('banner');
	    $current_theme 	= $portfolio_config['template_selected']? $portfolio_config['template_selected']: $themes[0];
	    $banner_path 	= explode('/',$portfolio_config['banner_url']);
	    $banner_path 	= (strpos($portfolio_config['banner_url'],'http://')===0 || strpos($portfolio_config['banner_url'],'https://')===0)? $portfolio_config['banner_url']:array_pop($banner_path);

	    $data=array(
		'banner_status' 	=> $portfolio_config['banner_status'],
		'banner' 		=> $portfolio_config['banner_url'],
		'banner_filename' 	=> $banner_path,
		'custom_root' 		=> $portfolio_config['custom_root']
	    );

	    include ("templates/theme_settings.php");
        break;
    }
}
switch($action){

	case 'getShare':

	    $student = $liblp2->getUserInfo();
	    $portfolio_url = $version>=1?libpf_lp2::getPortfolioUrl($portfolio):'';
	    $show_sharepeer = $is_owner && !$is_teacher && $ck_current_academic_year_id && !$is_competition &&!libpf_lp2::isFromKIS();

	    if ($show_sharepeer){//no need to get friends list
		$data['friends'] = $liblp2->getUserFriends();
		$data['classes'] = $liblp2->getClasses();
	    }

	    include("templates/share.php");

	break;
}
?>