<?
/**
 * Modification Log:
 * 2019-05-08 Pun [160276] [ip.2.5.10.6.1]
 *      - Fix hardcoded http
*/
$HTTP_SSL = (checkHttpsWebProtocol())? 'https://' : 'http://';
?>
<script>
$(function(){
    $('#friend_page').change(function(){
	loadFriendPortfolios($(this).val());
    });

    $('#friend_lp_list .lp_list').hide().fadeIn();

    $('#friend_lp_list a.get_key').click(function(){

	var href = $(this).attr('href');

	getEclassSessionKey(function(key){
	    window.open(href+'&eclasskey='+key);
	});

	return false;

    });
    <? if ($data['friend_lps']&&$allowViewPeer):?>

    $('.friend_lp_td').show();

    <? endif; ?>

});
</script>
<table width="100%" cellspacing="0" cellpadding="3" border="0"><tbody><tr>
    <td class="tabletext">  <?=$from?> - <?=$to?> <?=$langpf_lp2['admin']['navi']['of']?> <?=$total?></td>
    <td align="right"><table cellspacing="0" cellpadding="0" border="0"><tbody><tr>
	<td><table cellspacing="0" cellpadding="2" border="0"><tbody><tr align="center" valign="middle">
	    <td>
		<a onclick="return <?=$page==0?'false':"loadFriendPortfolios('".($page-1)."')"?>" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('prevp','','/images/2009a/icon_prev_on.gif',1)" class="tablebottomlink" href="#">
		    <img align="absmiddle" width="11" height="10" border="0" id="prevp" name="prevp" src="/images/2009a/icon_prev_off.gif">
		</a>
		<span class="tabletext">
		    <select class="formtextbox" id="friend_page" name="select">
			<? for ($i=0; $i<$max_page; $i++): ?>
			    <option <?=$page==$i?'selected="selected"':''?> value="<?=$i?>"><?=$i+1?></option>
			<? endfor; ?>
		    </select>
		</span>
	    </td>
	    <td>
		<span class="tabletext"> </span>
		<a onclick="return <?=$to==$total?'false':"loadFriendPortfolios('".($page+1)."')"?>" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('nextp','','/images/2009a/icon_next_on.gif',1)" class="tablebottomlink" href="#">
		    <img align="absmiddle" width="11" height="10" border="0" id="nextp" name="nextp" src="/images/2009a/icon_next_off.gif">
		</a>
	    </td>
	</tr></tbody></table></td>
    </tr></tbody></table></td>
</tr></tbody></table>


<div class="lp_list lp_frd">
    <ul>
    <? foreach ($data['friend_lps'] as $lp): ?>
    <li class="theme_<?=$lp['theme']?>">
	<? if ($lp['version']>=1): ?>
	    <a class="lp_content" href="<?=$HTTP_SSL.$eclass40_httppath?>src/iportfolio/?portfolio=<?=$lp['key']?>" target="_blank">
	<? else: ?>
	    <? /*<a class="lp_content get_key" href="/home/portfolio/learning_portfolio/browse/student_view.php?key=<?=$lp['key']?>" target="_blank">*/ ?>
	    <a class="lp_content" href="javascript:openEclassWindow('student_view','<?=$lp['key']?>','<?=$UserID?>', 93)">
	<? endif; ?>


	<h1><?=$lp['title']?><br><span><?=$lp['name'].'('.$lp['class'].')'?></span></h1>
	    <div class="lp_theme">
		<div class="student_photo"> <img src="<?=$lp['photo']?>"></div>
	    </div>
	</a>
	<div class="lp_ref">
	    <? if ($lp['version']>=1 && $lp['allow_like']): ?>
		<div class="lp_fb_like">
		    <div class="fb-like" data-href="<?=$lp['share_url']?>" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false"></div>
		</div>
	    <? endif ?>
	</div>

    </li>
    <? endforeach; ?>

    </ul>
</div>