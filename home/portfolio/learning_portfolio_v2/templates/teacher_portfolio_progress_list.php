<?
/*
 * Editing by
*
* Modification Log:
 * 2019-05-08 Pun [160276] [ip.2.5.10.6.1]
 *      - Fix hardcoded http
* 2014-02-21 (Jason)
* 		- fix the problem of incorrect published link, comment link and edit link of a student portfolio (old version)
*/
$HTTP_SSL = (checkHttpsWebProtocol())? 'https://' : 'http://';
?>
<script>

$(function(){

    $('#user_page, #user_order, #user_sortby, #selectedStudentProgressSortBy, #selectedStudentProgressPublishStatus').change(function(){
		loadPortfolioUsers('<?=$portfolio_id?>', $('#user_page').val(), $('#user_amount').val(), $('#user_keyword input').val());

    });
    $('#user_keyword').submit(function(e){
		loadPortfolioUsers('<?=$portfolio_id?>',0, $('#user_amount').val(), $('#user_keyword input').val());

	return false;
    });

    $('#user_prev').click(function(){
    	<? if ($page>0): ?>
    		$('#user_page').val(<?=$page-1?>).change();
    	<? endif; ?>
    	return false;
    });
    $('#user_next').click(function(){
    	<? if ($to<$total): ?>
    		$('#user_page').val(<?=$page+1?>).change();
    	<? endif; ?>
    	return false;
    });

    $('#user_amount').change(function(){
		loadPortfolioUsers('<?=$portfolio_id?>',0, $('#user_amount').val(), $('#user_keyword input').val());
    });

    $('#lp_student_list .lp_publish_old').click(function(){
    	if (confirm('<?=$langpf_lp2['admin']['item']['areyousuretoforcepublish']?>')){
    	    $.fancybox.showLoading();
    	    $.get($(this).attr('href'),function(){
    		alert('<?=$langpf_lp2['common']['publish']['publishsuccess']?>');
    		$('#user_page').change();

    	    });
    	}

    	return false;

    });

    $('#user_back').click(function(){
		loadUserPortfolios($('#portfolio_page').val(), $('#portfolio_amount').val(), $('#portfolio_keyword input').val(), $('#portfolio_sortby').val(), $('#portfolio_order').val());
		return false;
    });

});


</script>
<? if (!$is_stem_learning_scheme && $lp['version']>=1): ?>
<a style="float:left" href="ajax.php?action=printStudentList&portfolio=<?=$lp['key']?>" id="btn_print" class="contenttool print_fancybox"><img src="/images/2009a/icon_print.gif" hspace="4" border="0" align="absmiddle" alt="<?=$langpf_lp2['edit']['tool']['print']?>"><?=$langpf_lp2['edit']['tool']['print']?></a>
<? endif; ?>
<table width="100%" cellspacing="0" cellpadding="3" border="0" class="tablegreenbottom">
    <tbody>
    <tr><td align="left" class="tabletext"><?=$langpf_lp2['admin']['navi']['records']?> <?=$from?> - <?=$to?>, <?=$langpf_lp2['admin']['navi']['total']?> <?=$total?></td>
	<td align="right">
	     <table cellspacing="0" cellpadding="0" border="0"><tbody>
		<tr>
		    <td>
			<table cellspacing="0" border="0"><tbody><tr align="center" valign="middle">
			    <td class="tabletext" style="padding:0px 10px; border-left:1px solid white;">
				<form id="user_keyword">
				<?=$langpf_lp2['admin']['navi']['search']?>: <input type="text" name="keyword" value="<?=$keyword?>"/>
				</form>
			    </td>
			    <td class="tabletext" style="white-space:nowrap;padding:0px 10px; border-left:1px solid white;">
    				<?=$langpf_lp2['admin']['navi']['sortby']?>
    				<select class="formtextbox" name="selectedStudentProgressSortBy" id="selectedStudentProgressSortBy">
    				    <? foreach ($studentProgressSortFields as $sortByKey => $sortByField): ?>
    					<option value="<?=$sortByKey?>" <?=$sortByKey==$selectedStudentProgressSortBy?'selected="selected"':''?>><?=$sortByField?></option>
    				    <? endforeach; ?>
    				</select>
			    </td>
			    <td class="tabletext" style="padding:0px 10px; border-left:1px solid white;">
				<select class="formtextbox" name="selectedStudentProgressPublishStatus" id="selectedStudentProgressPublishStatus">
				    <? foreach ($studentProgressPublishStatus as $sortByKey => $sortByField): ?>
					<option value="<?=$sortByKey?>" <?=$sortByKey==$selectedStudentProgressPublishStatus?'selected="selected"':''?>><?=$sortByField?></option>
				    <? endforeach; ?>
				</select>

			    </td>
			    <td style="padding:0px 10px; border-left:1px solid white;">
				<a href="#" id="user_prev" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('prevp1','','/images/2009a/icon_prev_on.gif',1)" class="tablebottomlink">
				<img align="absmiddle" width="11" height="10" border="0" id="prevp1" name="prevp1" src="/images/2009a/icon_prev_off.gif"></a> <span class="tabletext"> <?=$langpf_lp2['admin']['navi']['page']?> </span>
			    </td>

			    <td class="tabletext" ><select class="formtextbox" name="Page" id="user_page">

				<? for ($i=0; $i<$max_page; $i++): ?>
				<option <?=$page==$i?'selected="selected"':''?> value="<?=$i?>"><?=$i+1?></option>
				<? endfor; ?>

			    </select></td>
			    <td >
				<span class="tabletext"></span>
				<a id="user_next" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('nextp1','','/images/2009a/icon_next_on.gif',1)" class="tablebottomlink" href="#">
				    <img align="absmiddle" width="11" height="10" border="0" id="nextp1" name="nextp1" src="/images/2009a/icon_next_off.gif">
				</a>
			    </td>
			</tr></tbody></table>
		    </td>
		    <td>&nbsp;<img src="/images/2009a/10x10.gif"></td>
		    <td style="padding:0px 10px; border-left:1px solid white;">
			<table cellspacing="0" cellpadding="2" border="0" class="tabletext"><tbody><tr>
			    <td><?=$langpf_lp2['admin']['navi']['display']?></td>
			    <td><select class="formtextbox" id="user_amount" name="select4">
				<option <?=$amount=='all'?'selected="selected"':''?> value="all"><?=$langpf_lp2['admin']['navi']['all']?></option>
				<? foreach ($amount_choices as $choice): ?>
				<option <?=$amount==$choice?'selected="selected"':''?> value="<?=$choice?>"><?=$choice?></option>
				<? endforeach; ?>
			    </select></td>
			    <td><?=$langpf_lp2['admin']['navi']['perpage']?> </td>
			</tr></tbody></table>
		    </td>
		</tr>
	    </tbody></table>

	</td>
    </tr></tbody>
</table><br>
<h2>
    <input type="button" id="user_back" class="formbutton" value='<<<?=$langpf_lp2['admin']['navi']['back']?>' />
    <?=$data['title']?> - <?=$langpf_lp2['admin']['navi']['studentprogress']?>
</h2>
<div class="lp_list">
    <ul>
    <? foreach ($data['student_lps'] as $lp):?>

	<li class="theme_<?=$lp['published']? $lp['theme'].' lp_status_published':$lp['theme']?>">

	    <div class="lp_content">
		<h1 title="<?=$lp['name'].'('.$lp['class'].')'?>"><?=$lp['unread']?'<span style="color:red;">*</span>':''?><?=$lp['name'].'('.$lp['class'].')'?></h1>
		<div class="lp_theme">
		    <div class="student_photo" style="bottom:20px; right:10px;z-index:1" > <img src="<?=$lp['photo']?>"></div>
		</div>

		<? if($is_stem_learning_scheme): ?>
		    <div class="lp_edit_tool">
    		    <? if($is_admin): ?>
        			<a href="<?=$HTTP_SSL.$eclass40_httppath?>src/iportfolio/?mode=draft&portfolio=<?=$lp['key']?>" class="tool_edit" target="_blank"><?=$langpf_lp2['admin']['item']['edit']?></a>
        			<? if ($lp['modified']): ?>
            			<a href="ajax.php?action=getPublish&portfolio=<?=$lp['key']?>" class="tool_publish fancybox2 lp_publish"><?=$langpf_lp2['admin']['item']['publish']?></a>
            			<a href="ajax.php?action=getThemeSettings&portfolio=<?=$lp['key']?>" class="tool_settings fancybox2"><?=$langpf_lp2['admin']['item']['settings']?></a>
        			<? endif; ?>
    			<? endif; ?>

    			<? if ($lp['published']): ?>
    				<a href="ajax.php?action=getExportDoc&portfolio=<?=$lp['key']?>" class="tool_export"><?=$langpf_lp2['admin']['item']['export']?></a>
    			<? endif; ?>
		    </div>
		<? elseif ($KIS_showEditTool): ?>
    		<? if ($lp['version']>=1): ?>
    		    <div class="lp_edit_tool">
    			<a href="<?=$HTTP_SSL.$eclass40_httppath?>src/iportfolio/?mode=draft&portfolio=<?=$lp['key']?>" class="tool_edit" target="_blank"><?=$langpf_lp2['admin']['item']['edit']?></a>
    			<? if ($lp['modified']): ?>
    			<a href="ajax.php?action=getPublish&portfolio=<?=$lp['key']?>" class="tool_publish fancybox2 lp_publish"><?=$langpf_lp2['admin']['item']['publish']?></a>
    			<a href="ajax.php?action=getThemeSettings&portfolio=<?=$lp['key']?>" class="tool_settings fancybox2"><?=$langpf_lp2['admin']['item']['settings']?></a>
    			<? endif; ?>
    			<? if ($lp['published']): ?>
    			<a href="ajax.php?action=getExportDoc&portfolio=<?=$lp['key']?>" class="tool_export"><?=$langpf_lp2['admin']['item']['export']?></a>
    			<? endif; ?>
    		    </div>
    		<? else: ?>
    		    <div class="lp_edit_tool">
    			<? /* <?=$eclass_url_path?>/src/portfolio/contents_student/main.php?web_portfolio_id=<?=$lp['web_portfolio_id']?>&student_user_id=<?=$lp['user_id']?> */?>
    			<? /*<a href="/home/portfolio/learning_portfolio/browse/student.php?key=<?=$lp['key']?>" class="tool_edit get_key" target="edit_portfolio"><?=$langpf_lp2['admin']['item']['edit']?></a> */ ?>
     			<a class="tool_edit" href="javascript:openEclassWindow('student','<?=$lp['key']?>','<?=$UserID?>', 93)"><?=$langpf_lp2['admin']['item']['edit']?></a>
    		    </div>
    		<? endif; ?>
		<? endif; ?>


		<div class="lp_publish_info">

		    <? if ($lp['published']): ?>

			<? if ($lp['version']>=1): ?>
			<span class="lp_info_draft" title="<?=$lp['modified']?>"><?=$langpf_lp2['admin']['item']['lastmodified']?>: <?=$lp['modified_days']?></span>
			<div class="lp_publish_date">
			    <a class="lp_info_published" href="<?=$HTTP_SSL.$eclass40_httppath?>src/iportfolio/?mode=publish&portfolio=<?=$lp['key']?>" target="_blank"><?=$langpf_lp2['admin']['item']['published']?></a>
			<? else: ?>
			<div class="lp_publish_date">
			 <?
			$worddoc_path = $eclass_filepath."/files/".$liblp2->db."/portfolio/student_u".$lp['user_id']."_wp".$lp['web_portfolio_id']."/".parseFileName(undo_htmlspecialchars($lp["title"])).".doc";
			$worddoc_action = (file_exists($worddoc_path)) ? "{$HTTP_SSL}{$eclass_httppath}/src/ip2portfolio/contents_student/management/worddoc_download.php?web_portfolio_id=".$lp['web_portfolio_id']."&user_id=".$lp['user_id'] : "javascript:alert('{$Lang['iPortfolio']['LPnoWordDoc']}')";

	    	?> <span style="float:left;margin-right: 5px;"><a href="<?=$worddoc_action?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/file/doc.gif" width="16" height="16" border="0"></a></span>

			<? /*<a class="lp_info_published get_key" href="/home/portfolio/learning_portfolio/browse/student_view.php?key=<?=$lp['key']?>" target="_blank"><?=$langpf_lp2['admin']['item']['published']?></a>*/ ?>
			    <a class="lp_info_published" href="javascript:openEclassWindow('student_view','<?=$lp['key']?>','<?=$UserID?>', 95)"><?=$langpf_lp2['admin']['item']['published']?></a>
			<? endif; ?>

			<span class="date_time" title="<?=$lp['published']?>"> <?=$lp['published_days']?></span>


			<? if ( !$is_stem_learning_scheme ): ?>
			<a rel="user_comment" title="<?=$lp['title']?>" class="lp_info_comment fancybox_iframe" href="/home/portfolio/learning_portfolio/browse/student_comment.php?key=<?=$lp['key']?>"><?=$lp['comments_count']? '('.$lp['comments_count'].')': '&nbsp;'?></a>
			<? endif; ?>

		    </div>
		    <? elseif ($lp['modified']): ?>
		    <div class="lp_publish_date"><span><?=$langpf_lp2['admin']['item']['drafted']?> </span>
			<span class="date_time" title="<?=$lp['modified']?>"> <?=$lp['modified_days']?></span>
		    </div>
		    <? endif; ?>

		</div>
	    </div>


	    <? if ($lp['published']): ?>
	    <div class="lp_ref">
		<? if ($lp['version']>=1&&!$disable_facebook): ?>
		<div class="lp_fb_like">
		    <div class="fb-like" data-href="<?=$lp['share_url']?>" data-send="false" data-action="like" data-font="arial" data-layout="standard" data-colorscheme="light" data-width="80" data-height="20" data-show-faces="false"></div>
		</div>
		<a class="lp_info_shared fancybox2" id="lp_info_shared_s<?=$lp['user_intranet_id']?>_wp<?=$lp['web_portfolio_id']?>" rel="portfolio_share" title="<?=$lp['name'].'('.$lp['class'].')'?>" href="ajax.php?action=getShare&portfolio=<?=$lp['key']?>">
		    <?=$langpf_lp2['admin']['item']['share']?>
		</a>
		<? endif ?>

	    </div>
	    <? endif; ?>
	</li>
    <? endforeach; ?>
    </ul>
</div>
<br>

