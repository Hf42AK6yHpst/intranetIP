<script>

$(function(){
             
        $('.setting_bg .edit_bottom .formsubbutton').click($.fancybox.close);
	
	$('.setting_bg .edit_bottom .formbutton').click(function(){
	    
	    $.fancybox.showLoading();
            $.post('/home/portfolio/learning_portfolio_v2/ajax.php?action=setShareFriends&portfolio=<?=$portfolio?>',{friend_ids: $('#selected_students option').map(function(){ return $(this).val();}).get()},$.fancybox.close);
	    return false;
        });

	
	$('#share_url').click(function(){$(this).select()});
	
	$('#search_students').submit(function(){
	    	    
	    $.fancybox.showLoading();
	    $.get('/home/portfolio/learning_portfolio_v2/ajax.php?action=getShareSearchResult&portfolio=<?=$portfolio?>', 
		{
		    'class': $('#search_class').val(),
		    'keyword': $('#search_keyword').val(),
		    'exclude': $('#selected_students option').map(function(){ return $(this).val();}).get()
		},
		function(data)
		{
		    $.fancybox.hideLoading();
		    $('#search_result').html(data);
		});
	    return false;
	})
	
	$('#share_add').click(function(){
	    
	    $('#selected_students').prepend($('#search_result option:selected'));
	    
	});
	
	$('#share_remove').click(function(){
	    
	    $('#selected_students option:selected').remove();
	    
	});
	
	
});
</script>
<div class="setting_bg" style="width:700px">
    <div class="setting_board">
		<div class="control_board">
	    	<h2><span><?=$langpf_lp2['common']['share']['sharetopeer']?></span></h2>
	   		<div style="width:200px;float:left;" >
				<?=$langpf_lp2['common']['share']['findstudents']?>
				<form id="search_students" style="font-size:12px; margin:5px;">
		   		<? if ($data['classes']): ?>
				    <select id="search_class">
						<option value=""><?=$langpf_lp2['common']['share']['allclasses']?></option>
						<? foreach($data['classes'] as $class): ?>
						    <option value="<?=$class?>"><?=$class?></option>
						<? endforeach; ?>
				    </select>
		    		</br>
		   		<? endif; ?>
		    
		    <?=$langpf_lp2['common']['share']['searchByInputFormat']?>
				    <input type="text" id="search_keyword"/>
				    <input type="submit" value="<?=$langpf_lp2['common']['share']['search']?>" class="formbutton" />
				</form>
	    	</div>
	
		    <select size="10" style="float:left;width:200px;" multiple="multiple">
				<optgroup id="search_result" label="<?=$langpf_lp2['common']['share']['searchresult']?>">
				    <option value=""> </option>
				</optgroup>
		    </select>

	    	<div id="share_edit" style="height:10em; position:relative; float:left; text-align:center; padding-top:3em">
	
				<input type="button" value="<?=$langpf_lp2['common']['share']['add']?>" class="formsubbutton" id="share_add" style="width:60px"/></br>
				<input type="button" value="<?=$langpf_lp2['common']['share']['remove']?>" class="formsubbutton" id="share_remove" style="width:60px"/>
				
	    	</div>
	   
		    <select size="10" style="float:left;width:200px;" multiple="multiple">
				<optgroup name="exclude" id="selected_students" label="<?=$langpf_lp2['common']['share']['sharedfriends']?>">
				<? foreach($data['friends'] as $friend): ?>
				    <option value="<?=$friend['friend_id']?>"><?=$friend['name'].' ('.$friend['class'].') '?></option>
				<? endforeach; ?>
				</optgroup>
		    </select>
	  
	    <p class="spacer"></p>
	</div>
	<div class="edit_bottom"><input type="button" value="<?=$langpf_lp2['common']['form']['apply']?>" class="formbutton" /><input type="button" value="<?=$langpf_lp2['common']['form']['close']?>" class="formsubbutton" /></div>
</div>