<?
/**
 * Modification Log:
 * 2019-05-08 Pun [160276] [ip.2.5.10.6.1]
 *      - Fix hardcoded http
*/
$HTTP_SSL = (checkHttpsWebProtocol())? 'https://' : 'http://';
?>
<script>

$(function(){

    block_hashchange=true;
    location.hash = '<?=$hash?>';

    $('#portfolio_page, #portfolio_order, #portfolio_sortby').change(function(){
	loadUserPortfolios($('#portfolio_page').val(), $('#portfolio_amount').val(), $('#portfolio_keyword input').val(), $('#portfolio_sortby').val(), $('#portfolio_order').val());
    });
    $('#portfolio_keyword').submit(function(e){
	loadUserPortfolios($('#portfolio_page').val(), $('#portfolio_amount').val(), $('#portfolio_keyword input').val(), $('#portfolio_sortby').val(), $('#portfolio_order').val());
	return false;
    });

     $('#portfolio_prev').click(function(){
	<? if ($page>0): ?>
	$('#portfolio_page').val(<?=$page-1?>).change();
	<? endif; ?>
	return false;
    });
    $('#portfolio_next').click(function(){
	<? if ($to<$total): ?>
	$('#portfolio_page').val(<?=$page+1?>).change();
	<? endif; ?>
	return false;
    });

    $('#portfolio_amount').change(function(){
	loadUserPortfolios(0, $('#portfolio_amount').val(), $('#portfolio_keyword input').val(), $('#portfolio_sortby').val(), $('#portfolio_order').val());
    });
    $('#user_lp_list .lp_list').hide().fadeIn();

    $('#user_lp_list a.get_key').click(function(){

	var href = $(this).attr('href');

	getEclassSessionKey(function(key){
	    window.open(href+'&eclasskey='+key);
	});

	return false;

    });


    $('#user_lp_list .lp_info_shared').click(function(){

	var id = $(this).find('.lp_id').html();
	if (id && $(this).closest('.lp_status_published').length>0){
	    loadPortfolioUsers($(this).find('.lp_id').html(), 0, 20);
	}
	return false;
    });


    $('#user_lp_list .lp_list ul').selectable({
	filter: '.lp_content',
	cancel: 'a'
    });

});


</script>

<table width="100%" cellspacing="0" cellpadding="3" border="0" class="tablegreenbottom">
    <tbody><tr>
	<td align="left" class="tabletext"><?=$langpf_lp2['admin']['navi']['records']?> <?=$from?> - <?=$to?>, <?=$langpf_lp2['admin']['navi']['total']?> <?=$total?></td>
	<td align="right">
	    <table cellspacing="0" cellpadding="0" border="0"><tbody>
		<tr>
		    <td>
			<table cellspacing="0" border="0"><tbody><tr align="center" valign="middle">
			    <td nowrap class="tabletext" style="padding:0px 10px; border-left:1px solid white;">
				<form id="portfolio_keyword">
				<?=$langpf_lp2['admin']['navi']['search']?>: <input type="text" name="keyword" value="<?=$keyword?>"/>
				</form>
			    </td>
			    <td class="tabletext" style="white-space:nowrap;padding:0px 10px; border-left:1px solid white;">
				<?=$langpf_lp2['admin']['navi']['sortby']?>
				<select class="formtextbox" name="Page" id="portfolio_sortby">
				    <? foreach ($sortby_choices as $i=>$sortby_choice): ?>
					<option value="<?=$i?>" <?=$i==$sortby?'selected="selected"':''?>><?=$sortby_choice?></option>
				    <? endforeach; ?>

				</select>
				<?=$langpf_lp2['admin']['navi']['in']?>
				<select class="formtextbox" name="Page" id="portfolio_order">

				    <option value="asc" <?=$order=='asc'?'selected="selected"':''?>><?=$langpf_lp2['admin']['navi']['ascending']?></option>
				    <option value="desc" <?=$order=='desc'?'selected="selected"':''?>><?=$langpf_lp2['admin']['navi']['descending']?></option>


				 </select>
				<?=$langpf_lp2['admin']['navi']['order']?>
			    </td>
			    <td style="white-space:nowrap;padding:0px 10px; border-left:1px solid white;">
				<a href="#" id="portfolio_prev" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('prevp1','','/images/<?php echo $LAYOUT_SKIN;?>/icon_prev_on.gif',1)" class="tablebottomlink">
				<img align="absmiddle" width="11" height="10" border="0" id="prevp1" name="prevp1" src="/images/<?php echo $LAYOUT_SKIN;?>/icon_prev_off.gif"></a> <span class="tabletext"> <?=$langpf_lp2['admin']['navi']['page']?> </span>
			    </td>

			    <td class="tabletext" ><select class="formtextbox" name="Page" id="portfolio_page">

				<? for ($i=0; $i<$max_page; $i++): ?>
				<option <?=$page==$i?'selected="selected"':''?> value="<?=$i?>"><?=$i+1?></option>
				<? endfor; ?>

			    </select></td>
			    <td >
				<span class="tabletext"></span>
				<a id="portfolio_next" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('nextp1','','/images/<?php echo $LAYOUT_SKIN;?>/icon_next_on.gif',1)" class="tablebottomlink" href="#">
				    <img align="absmiddle" width="11" height="10" border="0" id="nextp1" name="nextp1" src="/images/<?php echo $LAYOUT_SKIN;?>/icon_next_off.gif">
				</a>
			    </td>
			</tr></tbody></table>
		    </td>
		    <td>&nbsp;<img src="/images/<?php echo $LAYOUT_SKIN;?>/10x10.gif"></td>
		    <td style="padding:0px 10px; border-left:1px solid white;">
			<table cellspacing="0" cellpadding="2" border="0" class="tabletext"><tbody><tr>
			    <td><?=$langpf_lp2['admin']['navi']['display']?></td>
			    <td style="white-space:nowrap;"><select class="formtextbox" id="portfolio_amount" name="select4">
				<option <?=$amount=='all'?'selected="selected"':''?> value="all"><?=$langpf_lp2['admin']['navi']['all']?></option>
				<? foreach ($amount_choices as $choice): ?>
				<option <?=$amount==$choice?'selected="selected"':''?> value="<?=$choice?>"><?=$choice?></option>
				<? endforeach; ?>
			    </select></td>
			    <td><?=$langpf_lp2['admin']['navi']['perpage']?></td>

			</tr></tbody></table>
		    </td>
		</tr>
	    </tbody></table>
	</td>
    </tr></tbody>
</table><br>
<div class="lp_list">
    <ul>

    <? foreach ($data['student_lps'] as $lp):?>

    <?php
	$KIS_showEditTool = true;

	if( $_SESSION["platform"] == 'KIS' ){
		if($data['user_info']['KIS_teacher_type'] == 'FormTeacher' || $data['user_info']['KIS_teacher_type'] == 'ClassTeacher' || $data['user_info']['KIS_teacher_type'] == 'FormClassTeacher'){
			$KIS_showEditTool = false;
		}else if($data['user_info']['KIS_teacher_type'] == 'CoursePanel'){
			if( time() > strtotime($lp['deadline']) && strtotime($lp['deadline'])){
				$KIS_showEditTool = false;
			}
		}
	}
	?>

	<li class="theme_<?=$lp['status']? $lp['theme'].' lp_status_published':$lp['theme']?>">



	    <div class="lp_content">
		<span class="web_portfolio_id" style="display:none;"><?=$lp['web_portfolio_id']?></span>
		<h1 title="<?=$lp['title']?>"><?=$lp['title']?></h1>
		<div class="lp_theme"></div>

		<? if ( ($is_stem_learning_scheme && $is_admin) || (!$is_stem_learning_scheme && $KIS_showEditTool) ): ?>
		<div class="lp_edit_tool"<? if($isMobilePlatform) :?> style="visibility: visible; opacity: 1;" <?endif;?>>
		    <? if ($lp['version']>=1): ?>
			<a href="<?=$HTTP_SSL.$eclass40_httppath?>src/iportfolio/?mode=prototype&web_portfolio_id=<?=$lp['web_portfolio_id']?>" class="tool_edit" target="_blank"><?=$langpf_lp2['admin']['item']['edit'] ?></a>
			<a href="ajax.php?action=getSettings&portfolio_id=<?=$lp['web_portfolio_id']?>" class="tool_settings fancybox2"><?=$langpf_lp2['admin']['item']['settings']?></a>
			<a href="ajax.php?action=getSettings&is_clone=true&portfolio_id=<?=$lp['web_portfolio_id']?>" class="tool_copy fancybox2"><?=$langpf_lp2['admin']['item']['clone']?></a>
		    <? else: ?>
			<a href="/home/portfolio/contents_admin/contentframe.php?web_portfolio_id=<?=$lp['web_portfolio_id']?>" class="tool_edit" target="_blank"><?=$langpf_lp2['admin']['item']['edit']?></a>
			<a href="ajax.php?action=getSettings&portfolio_id=<?=$lp['web_portfolio_id']?>" class="tool_settings fancybox2"><?=$langpf_lp2['admin']['item']['settings']?></a>
		    <? endif; ?>
		</div>
		<? endif; ?>

		<div class="lp_publish_info">

		    <div class="lp_publish_date">

	    		<span class="date_time" ><?=$lp['starttime']&&$lp['deadline']?$langpf_lp2['admin']['item']['starttime'].': '.$lp['starttime'].'<br/>'.$langpf_lp2['admin']['item']['deadline'].': '.$lp['deadline']:''?></span>

		    </div>
		     <span class="lp_info_draft" title="<?=$lp['modified']?>"><?=$langpf_lp2['admin']['item']['lastmodified'] ?>: <?=$lp['modified_days']?></span>

		</div>

	     </div>

	    <div class="lp_ref">

		<a class="lp_info_shared" href="#">
		    <?=$langpf_lp2['admin']['item']['viewprogress']?> [<?=$langpf_lp2['admin']['item']['total']?> <?=$lp['student_count']?> <?=$langpf_lp2['admin']['item']['students']?>]
		    <span class="lp_id" style="display:none;"><?=$lp['student_count']?$lp['web_portfolio_id']:''?></span>
		</a>

	    </div>


	</li>
    <? endforeach; ?>
    </ul>
</div>
<br>

