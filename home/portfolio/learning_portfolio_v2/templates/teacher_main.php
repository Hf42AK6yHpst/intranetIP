<?
/*
 * Editing by 
 * 
 * Modification Log:
 * 2015-10-29 (Siuwan) [ip.2.5.7.1.1]
 * 		- display "manage template" button for KIS (Case#P88056)
 * 2014-2-21 (Jason)
 * 		- edit js loadPortfolioUsers() to add the logic to regenerate the key of published portfolio
 */
?>
<script>
function loadUserPortfolios(page, amount, keyword, sortby, order){
    
    $.fancybox.showLoading();
    $.get('ajax.php?action=getAllPortfolios',{page: page, amount: amount, keyword: keyword, sortby: sortby, order: order}, function(data){
	$.fancybox.hideLoading();
	
	$('#user_lp_list').html(data).slideDown(800);
	$('#user_top_tool').fadeIn();
	<?if(!$disable_facebook):?>
	FB.XFBML.parse($('#user_lp_list').get(0));
	<?endif;?>
	$('#lp_list_slider').animate({'margin-left': '0'},function(){
	    $('#lp_student_list').html('');

	});
    });
    return false;
    
}
function loadAllPublishedPortfolios(page, amount, keyword, group){
    
    
    $.fancybox.showLoading();
    $.get('ajax.php?action=getAllPublishedPortfolios',{page: page, amount: amount, keyword: keyword, group_id: group}, function(data){
	$.fancybox.hideLoading();
	
	$('#user_lp_list').html(data).slideDown(800);
	<?if(!$disable_facebook):?>
	FB.XFBML.parse($('#user_lp_list').get(0));
	<?endif;?>
	$('#lp_list_slider').animate({'margin-left': '0'},500,function(){
	    $('#lp_student_list').html('');

	});
	
    });
    return false;
    
}
function loadPortfolioUsers(portfolio, page, amount, keyword){
	var selectedStudentProgressSortBy = typeof $('#selectedStudentProgressSortBy').val() !== 'undefined' ? $('#selectedStudentProgressSortBy').val() : '';
	var selectedStudentProgressPublishStatus = typeof $('#selectedStudentProgressPublishStatus').val() !== 'undefined' ? $('#selectedStudentProgressPublishStatus').val() : '';
    $.fancybox.showLoading();
    $.get('ajax.php?action=getPortfolioUsers',{
        portfolio_id: portfolio, 
        page: page, 
        amount: amount, 
        keyword: keyword, 
        selectedStudentProgressPublishStatus: selectedStudentProgressPublishStatus,
        selectedStudentProgressSortBy : selectedStudentProgressSortBy        
    }, function(data){
	$.fancybox.hideLoading();
	
	if (data){
	    $('#user_top_tool').fadeOut();
	    $('#user_lp_list .lp_list ul li .lp_content').removeClass('ui-selected');//unselect all
	    <?if(!$disable_facebook):?>
	    FB.XFBML.parse($('#lp_student_list').html(data).get(0));
	    <?else:?>
	    $('#lp_student_list').html(data);
	    <?endif;?>
	    $('#lp_list_slider').animate({'margin-left': '-100%'},500,function(){
		$('#user_keyword input[name="keyword"]').focus();
	    });
	    
	    getEclassSessionKey(function(key){
			$('.lp_list a.get_key').attr('href', function(i, href){return href+'&eclasskey='+key;});
	    });
	}	
    });
    
    return false;
    
}
function publishedReload(){$('#user_page').change();}


function setPortfolioStatuses(status){
    var selected = $('.ui-selected .web_portfolio_id');
	
    if(selected.length<=0){
	alert('<?=$langpf_lp2['admin']['top']['pleaseselectportfolio']?>');
	return false;
    }
    
    var message = status==1? '<?=$langpf_lp2['admin']['top']['public']?>' : '<?=$langpf_lp2['admin']['top']['private']?>';
    if (confirm(selected.length+' <?=$langpf_lp2['admin']['top']['portfoliosselected']?>, <?=$langpf_lp2['admin']['top']['areyousureto']?> '+message+'?')){
	status==1? selected.parent().parent().addClass('lp_status_published') : selected.parent().parent().removeClass('lp_status_published');
	$.post('ajax.php?action=setPortfolioStatuses', {status: status, portfolio_id: selected.map(function(){ return $(this).html();}).get()});
    }
}
function removePortfolio(){
    var selected = $('.ui-selected .web_portfolio_id');
    
    if(selected.length<=0){
	alert('<?=$langpf_lp2['admin']['top']['pleaseselectportfolio']?>');
	return false;
    }
    
    if (confirm(selected.length+' <?=$langpf_lp2['admin']['top']['portfoliosselected']?>, <?=$langpf_lp2['admin']['top']['areyousureto'].' '.$langpf_lp2['admin']['top']['delete']?>?')){
	selected.parent().parent().fadeOut(function(){$(this).animate({width: 0},500,function(){$(this).remove();});});
	$.post('ajax.php?action=removePortfolios', {portfolio_id: selected.map(function(){ return $(this).html();}).get()});
    }
    return false;
}
$(function(){       
    
    $(".fancybox").fancybox({
	type: 'ajax',
	nextEffect: 'fade',
	prevEffect: 'fade',
	margin: 0,
	padding: 0,
	beforeClose: function(){
	    <? if (!$is_judge):?>
	    $('#portfolio_page').change();
	    <? endif; ?>
	}
    });
    
    $(".fancybox_iframe").fancybox({
	maxWidth: 600,
	nextEffect: 'fade',
	prevEffect: 'fade',
	type: 'iframe',
	beforeClose: function(){
	    <? if ($is_judge):?>
	    $('#portfolio_page').change();
	    <? else: ?>
	    $('#user_page').change();
	    <? endif;?>
	    
	}
    });
       			  
	
    <? if ($is_competition || $is_stem_learning_scheme): ?>
    HideLeftMenu(0, -150);
    <? endif; ?>
    
    $('#lp_student_list .lp_edit_tool .fancybox2').fancybox({type: 'ajax', padding: 0, margin: 0, beforeClose:function(){$('#user_page').change()}});
    $('#user_lp_list .lp_edit_tool .fancybox2').fancybox({type: 'ajax', padding: 0, margin: 0, beforeClose:function(){$('#portfolio_page').change()}});
    $('#lp_student_list .lp_info_shared').fancybox({type: 'ajax', padding: 0, margin: 0});
	$('.print_fancybox').fancybox({type: 'ajax', padding: 0, margin: 0});

    //$(window).bind('keyup',function(e){if (e.keyCode==46 && $('textarea:focus, input:focus').length==0) setPortfolioStatuses('ajax.php?action=removePortfolios','remove');});
    
    $('#public_portfolio').click(function(){setPortfolioStatuses(1);return false;});
    $('#private_portfolio').click(function(){setPortfolioStatuses(0);return false;});
    $('#delete_portfolio').click(function(){removePortfolio();return false;});
    
    $(window).bind('hashchange',function(){if (block_hashchange==false){location.reload();}block_hashchange=false;});
    
    var hash = location.hash;
    hash =  hash.replace('#','').split('/');
    <? if ($is_judge && !$is_stem_learning_scheme):?>
	loadAllPublishedPortfolios(hash[0], hash[1], hash[2], hash[3]);
    <? else: ?>
	loadUserPortfolios(hash[0], hash[1], hash[2], hash[3], hash[4]);
    <? endif;?>

});

</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td valign="top" height="17"><table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
    <tr>
	<td width="17" height="37"><img width="17" height="37" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_01.gif"></td>
	<td height="37" background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_03.gif"><table cellspacing="0" cellpadding="0" border="0">
			<tbody><tr>
				<td width="200" align="center" valign="middle" background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_02.gif" class="page_title"><?=$iPort['menu']['learning_portfolio']?></td>
			</tr>
	</tbody></table></td>
	<td width="13" height="37"><img width="13" height="37" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_04.gif"></td>
	
    </tr>
    <? if (!$is_judge): ?>
    
    <tr align="right">
	<td width="17" height="37"><img width="17" height="37" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_05.gif"></td>
	<td background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_06.gif">
	    <div id="user_top_tool">
	    <? if (!$is_competition):?>
	    <a style="float:right"  class="contenttool" href="/home/portfolio/learning_portfolio/contents_admin/templates_manage.php"><img src="/images/<?php echo $LAYOUT_SKIN;?>/icon_remark.gif" hspace="4" border="0" align="absmiddle"/><?=$langpf_lp2['admin']['top']['managetemplates']?></a>
	    <? endif; ?>
	    
	     <? if ($KIS_edit_role): ?>
	    <a style="float:left" href="ajax.php?action=getSettings" id="new_portfolio" class="contenttool fancybox"><img src="/images/<?php echo $LAYOUT_SKIN;?>/icon_new.gif" hspace="4" border="0" align="absmiddle" alt="New"><?=$langpf_lp2['admin']['top']['new']?> | </a>
	    <a style="float:left" href="#" id="public_portfolio" class="contenttool"><img src="/images/<?php echo $LAYOUT_SKIN;?>/icon_approve.gif" hspace="4" border="0" align="absmiddle"><?=$langpf_lp2['admin']['top']['public']?> | </a> 
	    <a style="float:left" href="#" id="private_portfolio" class="contenttool"><img src="/images/<?php echo $LAYOUT_SKIN;?>/icon_reject.gif" hspace="4" border="0" align="absmiddle"><?=$langpf_lp2['admin']['top']['private']?> | </a> 
	    <a style="float:left" href="#" id="delete_portfolio" class="contenttool"><img src="/images/<?php echo $LAYOUT_SKIN;?>/icon_delete.gif" hspace="4" border="0" align="absmiddle"><?=$langpf_lp2['admin']['top']['delete']?></a>
	     <? endif; ?>
	    
	    </div>
	</td>
	<td width="13" height="37"><img width="13" height="37" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_07.gif"></td>
    </tr>
    <? endif; ?>
    <tr>
	<td width="17" background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_05.gif"><img width="17" height="20" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_05.gif"></td>
	<td  background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_06.gif" class="tab_table">
	    <div id="lp_list_container" style="overflow-x:hidden;">
		<div id="lp_list_slider" style="width:200%;">
		    <div id="user_lp_list" style="width:50%;float:left;display:none;">
			
		    </div>
		    <div id="lp_student_list" style="width:50%; float:left">
			
		    </div>
		</div>
	    </div>
	</td>
	<td width="13" background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_07.gif"><img width="13" height="37" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_07.gif"></td>
	
    </tr>
    <tr>
	<td width="17" height="17"><img width="17" height="17" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_08.gif"></td>
	<td height="17" background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_09.gif"><img width="64" height="17" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_09.gif"></td>
	<td width="13" height="17"><img width="13" height="17" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_10.gif"></td>
    </tr>
</tbody></table></td>
</tr>

</tbody></table>
