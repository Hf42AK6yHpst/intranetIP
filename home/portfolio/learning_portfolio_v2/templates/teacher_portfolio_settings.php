<?php
/*
 * Editing by Siuwan
 * 
 * Modification Log:

 */

$portfolio_id = (is_array($portfolio_id)) ? $portfolio_id[0] : $portfolio_id;

$folder_size = $data['portfolio_info']["folder_size"]/1024;
if($folder_size == "" || $folder_size == 0){ $folder_size = 20;}
${"status_checked_".$data['portfolio_info']["status"]} = "checked";


# start and end time preparation
if (($time_arr=splitTime($data['portfolio_info']["starttime"]))!="")
{
	$starttime = $time_arr[0];
	$sh = $time_arr[1];
	$sm = $time_arr[2];
}
if (($time_arr=splitTime($data['portfolio_info']["deadline"]))!="")
{
	$endtime = $time_arr[0];
	$eh = $time_arr[1];
	$em= $time_arr[2];
}
?>

<script language="javascript">


function checkform(myObj){
	var isst = false;
	var dateStart = $("#starttime").val();
	var hourStart = str_pad($("select[name=sh] option:selected").val(), 2);
	var minStart = str_pad($("select[name=sm] option:selected").val(), 2);
	var secStart = '00';
	var timeTextStart = dateStart + hourStart + minStart + secStart;
	
	var dateEnd = $("#endtime").val();
	var hourEnd = str_pad($("select[name=eh] option:selected").val(), 2);
	var minEnd = str_pad($("select[name=em] option:selected").val(), 2);
	var secEnd = '00';
	var timeTextEnd = dateEnd + hourEnd + minEnd + secEnd;
	
	if (!check_text(myObj.title, "<?=$ec_warning['growth_title']?>")) return false;
	if(document.form1.sizeMax.value <= 0){ alert("<?php echo $assignments_alert_msg13; ?>"); document.form1.sizeMax.focus(); return false;}

	//check time
	if(timeTextStart > timeTextEnd){
		alert("<?=$Lang['General']['WarningArr']['EndDateCannotEarlierThanStartDate']?>"); 
			$("#endtime").focus(); 
		return false;
	}

	
	if (typeof(myObj.elements["groups[]"])!="undefined")
	{
	    checkOption(myObj.elements["groups[]"]);
	    for(var i=0; i<myObj.elements["groups[]"].length; i++)
	    {
		    myObj.elements["groups[]"].options[i].selected = true;
	    }
	}
	return true;
}
function doCancel(){
	self.location.href = "index_scheme.php";
}
function doReset(){
	self.location.reload();
}
function check(from,to){
	checkOption(from);
	checkOption(to);
	i = from.selectedIndex;
	while(i!=-1){
		to.options[to.length] = new Option(from.options[i].text, from.options[i].value, false, false);
		from.options[i] = null;
		i = from.selectedIndex;
	}
}

$(function(){
    $('#settings_form').submit(function(){
	
	if (checkform(this)){
	    $.fancybox.showLoading();
	    $.post('ajax.php?action=setPortfolio', $(this).serialize(),function(){
		$.fancybox.close();
	    })
	}
	return false;
    });
    $('.date').datepicker({
	dateFormat: "yy-mm-dd",
	changeMonth: true,
	changeYear: true,
	beforeShow: function(){
	    
	    setTimeout(function(){
 		$(".ui-datepicker").css("z-index", 11000);
	    }, 10); 
	}
      });
})
</script>


<form style="width:650px;" name="form1" id="settings_form" method="post" action="ajax.php?action=setPortfolio">


<div class="setting_bg">
    
    <div  class="setting_board">

	<div class="control_board">
	    <h2><span><?=$portfolio_id?($is_clone?$langpf_lp2['admin']['item']['clonefrom'].' "'.$data['portfolio_info']["title"].'"':$ec_iPortfolio['edit_portfolios']):$langpf_lp2['admin']['item']['newportfolio'] ?></span></h2>
	

		<table style="margin:0" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
		  <td class="formfieldtitle" nowrap="nowrap" valign="top"><span class="tabletext"><?= $ec_iPortfolio['growth_title'] ?></span><span class="tabletextrequire">*</span></td>
		  <td><input type=text name="title" class="textboxtext" value="<?=$is_clone?'':$data['portfolio_info']["title"]?>">
		  </td>
		</tr>
		<tr>
		  <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?= $ec_iPortfolio['growth_description'] ?></span></td>
		    <td><textarea style="width:100%" name="instruction"><?=$data['portfolio_info']["instruction"]?></textarea></td>
		</tr>
		<tr>
		  <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?= $ec_iPortfolio['growth_phase_period'] ?></span></td>
		  <td><table border=0 cellpadding="3" cellspacing="0">
			<tr><td><?=$StartTime?>:</td><td nowrap>&nbsp;<input class="inputfield date" type=text id="starttime" name="starttime" size=11 maxlength=10 value="<?=$starttime?>"> <?= returnHour('sh', $sh, 0).returnMinute('sm', $sm, 0) ?></td></tr>
			<tr><td><?=$EndTime?>:</td><td nowrap>&nbsp;<input class="inputfield date" type=text id="endtime" name="endtime" size=11 maxlength=10 value="<?=$endtime?>"> <?= returnHour('eh', $eh).returnMinute('em', $em) ?></td></tr>
			</table>
		    </td>
		</tr>
		 <tr>
		  <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?= $ec_iPortfolio['iportfolio_folder_size'] ?></span></td>
		  <td ><input type=text name="sizeMax" size=10 maxlength=10 value="<?=$folder_size ?>">&nbsp;<span class="tabletext"><?= $ec_iPortfolio['size_unit'] ?></span>
		  </td>
		</tr>
		<? if (!$is_competition || ($is_competition&&libpf_lp2::$is_admin)): ?>
		
		<? if($_SESSION["platform"]=="KIS"): ?>
		<tr>
			<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?= $ec_iPortfolio["yearform"] ?></span></td>
			<td><?=$data['form_Selection'] ?></td>
		</tr>
		<? endif; ?>
		
		<tr>
		  <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?= $ec_iPortfolio['growth_group'] ?></span></td>
		  <td >
			<table align="center" border="0" cellpadding="5" cellspacing="0">
			<tbody>
			 <tr valign="top"><td><span class="tabletext"><?=$ec_iPortfolio['choose_group']?> :</span></td>
						 <td nowrap="nowrap" width="10"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/10x10.gif" height="10" width="10"></td>
						 <td nowrap="nowrap"><span class="tabletext"><?=$ec_iPortfolio['select_group']?> :</span></td>
			 </tr>
			<tr>
				<td class="tablerow2" valign="top">
					<select name="source[]" size=15 class="select_studentlist" style="width: 200px;" multiple>
						<?= $data['group_out_list'] ?>
						<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
					</select>
				</td>
				<td align="center" nowrap="nowrap" valign="middle">
	
					 <input name="imgAddItem" class="formsubbutton" value="&gt;&gt;" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" type="button" onclick="javascript:check(document.form1.elements['source[]'],document.form1.elements['groups[]'])">
				  <br>
			      <br>
				 <input name="imgRemoveItem" class="formsubbutton" value="&lt;&lt;" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" type="button" onclick="javascript:check(document.form1.elements['groups[]'],document.form1.elements['source[]'])">
	
				</td>
			<td nowrap="nowrap" valign="top">
				
					<table border="0" cellpadding="3" cellspacing="0" >
			<tbody>
						<tr>
				     <td>
					  <span class="tablerow2">
											<select name="groups[]" size=15 class="select_studentlist" style="width: 200px;" multiple>
													<?= $data['group_in_list'] ?>
													<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
											</select>
											</span>
				       </td>
				     </tr>
				    
			</tbody>
			  </table>
			</td>
			</tr>
			<tr>
			    <td colspan="3" class="tabletextremark"  align="left"><?= $ec_iPortfolio_guide['group_growth'] ?></td>
			</tr>
			</table>
		  </td>
		</tr>
		<? endif; ?>
		<tr>
		  <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?= $qbank_status ?></span></td>
		  <td ><table border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td class="13Gray"><input type=radio name=status id="status_1" value="1" <?=$data['portfolio_info']['status']==1?'checked="checked"':''?> > <label for="status_1"><?= $button_public ?></label></td>
			  <td class="13Gray"><input type=radio name=status id="status_0" value="0" <?=$data['portfolio_info']['status']==0?'checked="checked"':''?> > <label for="status_0"><?= $button_private ?></label></td>
			</tr>
		      </table>
		  </td>
		</tr>	
		</table>
	    
	</div>
    </div>
<div class="edit_bottom">
  				<input class="formbutton" type="submit" value="<?=$button_confirm?>" name="btn_submit" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
				<input name="Reset" class="formbutton" value="<?=$button_reset?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" type="reset">
				<input class="formbutton" type="button" value="<?=$button_cancel?>" onClick="$.fancybox.close()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"></td>
</div>


<input type="hidden" name="portfolio_id" value="<?=$portfolio_id?>">
<input type="hidden" name="is_clone" value="<?=$is_clone?>"/>
</div>
</form>
