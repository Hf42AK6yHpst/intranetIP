<?php
/**
 * LPv2 Manage Page - ajax request controller
 *
 *
 * @author Mick Chiu
 * @since 2012-03-27
 * 
 */
$PATH_WRT_ROOT = "../../../";
$home_header_no_EmulateIE7 = true;
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-lp2.php");
include_once($eclass_filepath.'/src/includes/php/lib-groups.php');
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
intranet_opendb();
if($iportfolio_lp_version != 2){
	header('Location: ../');
}
$liblp2 = new libpf_lp2($UserID, 0, $UserID, 'prototype');
$LPSetting = $liblp2->getLPConfigSetting();
$disable_facebook = $LPSetting['disable_facebook'];
$is_admin = libpf_lp2::$is_admin;
$is_competition = libpf_lp2::$is_competition;
$is_stem_learning_scheme = libpf_lp2::$is_stem_learning_scheme;
$isMobilePlatform = $userBrowser->platform == "iPad" || $userBrowser->platform == "Andriod";
switch($liblp2->memberType){
    
    case 'T':
	$linterface = new interface_html();
	// set the current page title
	$CurrentPage = "Teacher_LearningPortfolio";
	$CurrentPageName = $iPort['menu']['learning_portfolio'];
	
	//$teacherType = $liblp2->checkKisTeacherType();
	
	$currentPageIndex = IPF_CFG_LP_MGMT_CONTENT;
	$TAGS_OBJ[] = array($CurrentPageName,"");
	$MODULE_OBJ = $liblp2->GET_MODULE_OBJ_ARR("Teacher");
	$KIS_edit_role = true;
	$user_info = $liblp2->getUserInfo();
	
	$is_judge = $user_info['is_judge'];
	
	if($_SESSION["platform"]=="KIS"){
		$MODULE_OBJ = '';
		$MODULE_OBJ['title'] = 'iPortfolio';
		if($user_info["KIS_teacher_type"] != 'admin' && $user_info["KIS_teacher_type"] != 'CoursePanel'){
			$KIS_edit_role = false;
		}
	}
	$linterface->LAYOUT_START();
	
	$main_template = 'templates/teacher_main.php';
	
    break;
    case 'S':
	$linterface = new interface_html("iportfolio_default2.html");
	// set the current page title
	$CurrentPage = "Student_LearningPortfolio";
	$CurrentPageName = $iPort['menu']['learning_portfolio'];
	
	$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
	$MODULE_OBJ = $liblp2->GET_MODULE_OBJ_ARR();
	
	$linterface->LAYOUT_START();
	
	$main_template = 'templates/student_main.php';
    break;
    default:
	header('Location: /');
    break;
    
}
include_once('templates/header.php');
include_once($main_template);
?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
