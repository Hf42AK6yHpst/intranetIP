<?php
#Modify : 
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
iportfolio_auth("T");
intranet_opendb();

$lpf = new libportfolio();
$lpf->ACCESS_CONTROL("student_info");

if ($order=="") $order=1;
if ($field=="") $field=0;
$pageSizeChangeEnabled = true;
$LibTable = new libpf_dbtable($field, $order, $pageNo);

$sql =  "
          SELECT
            CONCAT('<a href=\"school_records_alumni_year.php?Year=', iau.YearOfLeft, '\"  class=\"tablelink\">', iau.YearOfLeft, '</a>'),
            count(DISTINCT ps.UserID) AS ActivatedCount
          FROM
            {$intranet_db}.INTRANET_ARCHIVE_USER iau
          LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps
            ON  iau.UserID = ps.UserID
          GROUP BY
            iau.YearOfLeft
        ";
        
$LibTable->sql = $sql;
$LibTable->field_array = array("iau.YearOfLeft", "ActivatedCount");
$LibTable->db = $intranet_db;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($num_per_page=="") ? $page_size : $num_per_page;
if ($page_size_change!="") $li->page_size = $numPerPage;
$LibTable->no_col = 3;
	
//$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
//$LibTable->row_alt = array("#FFFFFF", "F3F3F3");
$LibTable->row_alt = array("", "");
//$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";
$LibTable->column_array = array(3, 3);

// TABLE COLUMN
$LibTable->column_list .= "<tr class='tabletop'>\n";

$LibTable->column_list .= "<td height='25' align='center' class=\"tabletopnolink\" >#</span></td>\n";
$LibTable->column_list .= "<td align='center'>".$LibTable->column(0,$ec_iPortfolio['year'])."</td>\n";
$LibTable->column_list .= "<td align='center'>".$LibTable->column(1,$ec_iPortfolio['heading']['no_lp_active_stu'])."</td>\n";
$LibTable->column_list .= "</tr>\n";


# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];
### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");
$linterface->LAYOUT_START();

?>

<script language="JavaScript">

var SearchTextFocus = false;

$(document).ready(function(){
  $("input[name=search_name]").keypress(function(event){
    if(event.keyCode == 13)
    {
      document.form1.action = "school_records_alumni_year.php";
      document.form1.submit();
    }
  });
});

</script>

<form name="form1" method="POST">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" bgcolor="#FFFFFF">
			<table width="98%" border="0" cellspacing="3" cellpadding="0">
				<tr>
					<td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" valign="bottom" class="thumb_list">
									<span class="tabletext">
										<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$ec_iPortfolio['enter_student_name']:stripslashes($search_name))?>" onFocus="SearchTextFocus=true;if(this.value=='<?=$ec_iPortfolio['enter_student_name']?>'){this.value=''}" onBlur="SearchTextFocus=false;if(this.value==''){this.value='<?=$ec_iPortfolio['enter_student_name']?>'}" />
									</span>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
<?php if(strstr($ck_function_rights, "Alumni")) { ?>
								<td align="right" valign="middle" class="thumb_list">
									<a href="school_records.php"> <?=$i_identity_student?> </a> | <span> <?=$i_identity_alumni?> </span>
								</td>
<?php } ?>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<?=$LibTable->displayPlain(); ?>
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
<?php if ($LibTable->navigationHTML!="") { ?>
                    <tr class='tablebottom'>
                      <td  class="tabletext" align="right"><?=$LibTable->navigation(1)?></td>
                    </tr>
<?php } ?>
                  </table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
		</td>
	</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $LibTable->pageNo; ?>">
<input type="hidden" name="order" value="<?= $order ?>">
<input type="hidden" name="field" value="<?= $field ?>">
<input type="hidden" name="page_size_change" />
<input type="hidden" name="numPerPage" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
