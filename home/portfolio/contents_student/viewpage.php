<?php

// Modifing by key
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."/includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");

//include_once($eclass_filepath."/system/settings/global.php");
//include_once($eclass_filepath."/src/includes/php/lib-db.php");
include_once($eclass_filepath."/src/includes/php/lib-filemanager.php");
include_once($eclass_filepath."/src/includes/php/lib-record.php");

/*
include_once('../../../system/settings/global.php');
include_once('../../includes/php/lib-db.php');
include_once('../../includes/php/lib-filemanager.php');
include_once('../../includes/php/lib-record.php');
*/
//opendb();

intranet_auth();
intranet_opendb();

$lpf = new libportfolio();
$li = new libdb();
$li->db = $lpf->course_db;

$pageNav = "&nbsp;";
if ($notes_id!="")
{
	$sql = ($ck_memberType=="S") ? "SELECT url FROM notes_student WHERE notes_student_id=$notes_id "
					: "SELECT url FROM notes WHERE notes_id=$notes_id ";
	//$li = new librecord($sql,classNamingDB($ck_course_id));
	//$lr = new librecord($sql, $lpf->ipf_db);
	//$row = $lr->resultSet();
	$row = $li->returnArray($sql);
	$url = $row[0]["url"];
	
	if ($url=="" || strtoupper(trim($url))=="HTTP://")
	{
		$url = "app/toc.php?notes_id=$notes_id&group_id=$group_id";
	} 
	elseif(stristr($url,"tv://"))
	{
		$url = "$intranet_rel_path/home/plugin/campustv/".str_replace("tv://", "", $url);
	}
	 elseif(stristr($url,"elp://"))
	{
		$url = "$eclass_url_path/files_elp/".str_replace("elp://", "", $url);
		$url = "http://".$eclass_httppath.$url;
	} 
	elseif(!stristr($url,"http://"))
	{
		$categoryID = ($ck_memberType=="S") ? 7 : 0;
		$fm = new fileManager($ck_course_id, $categoryID, "");
		$fm->blankLink = "$url";
		$url = $fm->openFirstFile($url, $categoryID)."&fromCC=1";
		
		$url = "http://".$eclass_httppath.$url;
	}
}
//closedb();
intranet_closedb();

header("Location: $url");
?>