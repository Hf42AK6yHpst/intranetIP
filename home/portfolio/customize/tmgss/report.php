<?php
##	Modifying By: 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmgss/libpf-slp-tmgss-spr-report.php");

$StudentID = $StudentID; // This student ID is a list of array in form of 01,02,03
$StudentIDArray = explode(",",$StudentID);
$academicYearID = $academicYearID;
$YearTermID = $YearTermID;
$DateofIssue = $DateofIssue==''?date("d M Y"):$DateofIssue;


intranet_auth();
intranet_opendb();
switch ($printType) {
	case "html":
	    $libpf_slp_tmgss_spr_report= new libpf_slp_tmgss_spr_report($StudentIDArray, $academicYearID, $YearTermID,$DateofIssue);
	    $output = $libpf_slp_tmgss_spr_report->generateHtmlReport();
		break;
		
	case "word":	 
	    $libpf_slp_tmgss_spr_report= new libpf_slp_tmgss_spr_report($StudentIDArray, $academicYearID, $YearTermID,$DateofIssue);	    
	    $output = $libpf_slp_tmgss_spr_report->generateWordReport();
		break;

	default:
		break;
}
echo $output;

intranet_closedb();
?>