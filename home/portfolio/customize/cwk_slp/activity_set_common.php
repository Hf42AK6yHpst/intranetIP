<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");

// customized lib
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cwk/lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$lpf = new libportfolio();
$lpf_ui = new libportfolio_ui();
$linterface = new interface_html();

$activity_name = $activity_name[0];
########################################################
# Tab Menu : Start
########################################################

$TabMenuArr = libpf_tabmenu::getSlpMgmtTabs(IPF_CFG_SLP_MGMT_CWK_SLP, 0);

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_CWK_SLP);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

########################################################
# Operations : Start
########################################################
# tab menu
$tab_menu_html = $lpf_ui->GET_TAB_MENU($TabMenuArr);

$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['program_list'], "activity.php");
$MenuArr[] = array($activity_name, "");
$navigation_html = $linterface->GET_NAVIGATION($MenuArr);

########################################################
# Operations : End
########################################################

########################################################
# Data Preparation : Start
########################################################
// Activity Ability
$sql = "SELECT cwk_ca.AbilityID, cwk_ca.AbilityName, IF(cwk_aa.ActivityName IS NULL, 0, 1) AS AbilityCheck ";
$sql .= "FROM {$eclass_db}.CWK_COMMON_ABILITY cwk_ca ";
$sql .= "LEFT JOIN {$eclass_db}.CWK_ACTIVITY_ABILITY cwk_aa ";
$sql .= "ON cwk_aa.AbilityID = cwk_ca.AbilityID AND cwk_aa.ActivityName = '{$activity_name}' ";
$act_ability = $lpf->returnArray($sql);

$html_ability_check = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
for($i=0, $i_max=count($act_ability); $i<$i_max; $i++)
{
  $html_ability_check .= ($i%3 == 0) ? "<tr>" : "";

  $_ability_id = $act_ability[$i]["AbilityID"];
  $_ability_name = $act_ability[$i]["AbilityName"];
  $_ability_check = $act_ability[$i]["AbilityCheck"] ? "CHECKED=\"CHECKED\"" : "";

  $html_ability_check .= "<td width=\"30%\"><input type=\"checkbox\" name=\"act_ability[]\" id=\"act_ability_{$_ability_id}\" value=\"{$_ability_id}\" {$_ability_check} /><label for=\"act_ability_{$_ability_id}\"> {$_ability_name}</label></td>";
  
  $html_ability_check .= ($i%3 == 2 || $i+1 == $i_max) ? "</tr>" : "";
}
$html_ability_check .= "</table>";

// Activity Criteria
$sql = "SELECT component.RecordID, ".Get_Lang_Selection("component.ChiTitle", "component.EngTitle")." AS DisplayName, IF(cwk_ac.ActivityName IS NULL, 0, 1) AS CriteriaCheck ";
$sql .= "FROM {$eclass_db}.OLE_ELE component ";
$sql .= "LEFT JOIN {$eclass_db}.CWK_ACTIVITY_CRITERIA cwk_ac ";
$sql .= "ON cwk_ac.ComponentID = component.RecordID AND cwk_ac.ActivityName = '{$activity_name}' ";
$act_criteria = $lpf->returnArray($sql);

$html_criteria_check = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
for($i=0, $i_max=count($act_criteria); $i<$i_max; $i++)
{
  $html_criteria_check .= ($i%3 == 0) ? "<tr>" : "";

  $_criteria_id = $act_criteria[$i]["RecordID"];
  $_criteria_name = $act_criteria[$i]["DisplayName"];
  $_criteria_check = $act_criteria[$i]["CriteriaCheck"] ? "CHECKED=\"CHECKED\"" : "";

  $html_criteria_check .= "<td width=\"30%\"><input type=\"checkbox\" name=\"act_criteria[]\" id=\"act_criteria_{$_criteria_id}\" value=\"{$_criteria_id}\" {$_criteria_check} /><label for=\"act_criteria_{$_criteria_id}\"> {$_criteria_name}</label></td>";
  
  $html_criteria_check .= ($i%3 == 2 || $i+1 == $i_max) ? "</tr>" : "";
}
$html_criteria_check .= "</table>";
########################################################
# Data Preparation : End
########################################################

########################################################
# Layout Display
########################################################
// set the current page title
$CurrentPage = "Teacher_OLE";

$linterface->LAYOUT_START();
include_once("template/activity_set_common.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();

?>