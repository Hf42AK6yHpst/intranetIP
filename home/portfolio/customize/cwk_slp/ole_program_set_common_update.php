<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lpf = new libportfolio();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$li->Start_Trans();

// db data preparation
for($i=0, $i_max=count($program_ability); $i<$i_max; $i++)
{
  $_ability_id = $program_ability[$i];

  $program_ability_val[] = "('{$program_id}', '{$_ability_id}')";
}
if(is_array($program_criteria))
{
  $program_criteria_val = implode(",", $program_criteria);
}

// program ability update
$sql = "DELETE FROM {$eclass_db}.CWK_OLE_PROGRAM_ABILITY WHERE ProgramID = '{$program_id}'";
$res[] = $li->db_db_query($sql);
if(count($program_ability_val) > 0)
{
  $sql_field_val = implode(",", $program_ability_val);

  $sql = "INSERT INTO {$eclass_db}.CWK_OLE_PROGRAM_ABILITY ";
  $sql .= "(ProgramID, AbilityID) ";
  $sql .= "VALUES {$sql_field_val} ";
  $res[] = $li->db_db_query($sql);
}

// program criteria update
if(count($program_criteria_val) > 0)
{
  $sql = "UPDATE {$eclass_db}.OLE_PROGRAM SET ";
  $sql .= "ELE = '{$program_criteria_val}' ";
  $sql .= "WHERE ProgramID = {$program_id}";
  $res[] = $li->db_db_query($sql);
}

$final_res = (count($res) == count(array_filter($res)));
if($final_res)
{
  $li->Commit_Trans();
  $msg = "update";
}
else
{
  $li->RollBack_Trans();
  $msg = "update_failed";
}

intranet_closedb();
header("Location: ole_program.php?msg=$msg");
?>