<FORM name="form1" method="POST" action="ole_program_set_common_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td align="center">
      <?=$tab_menu_html?>
  	</td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="100%">
            <table border="0" cellpadding="3" cellspacing="0">
    					<tr>					
    						<td class="navigation">
    						<?=$navigation_html?>				
    						</td>
    					</tr>					
  					</table>
					</td>
        </tr>
        <tr>
          <td>
            <table width="95%" border="0" cellspacing="0" cellpadding="4" align="center">
      				<tr>
      					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="15%">
      						<?=$Lang["Cust_Cwk"]["CommonAbility"]?>
      					</td>
      					<td valign="top" class="tabletext"><?=$html_ability_check?></td>
      				</tr>
      				<tr>
      					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="15%">
      						<?=$Lang["Cust_Cwk"]["MainCriteria"]?>
      					</td>
      					<td valign="top" class="tabletext"><?=$html_criteria_check?></td>
      				</tr>
      			</table>
      		</td>
      	</tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
    	<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
    		<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
    		<tr>
    			<td class="tabletextremark" colspan="6"><?=$ec_iPortfolio['mandatory_field_description']?></td>
    		</tr>
    		<tr>
    			<td align="center" colspan="6">
    				<div style="padding-top: 5px">
    				<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
    				<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
    				<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:self.location='ole_program.php'")?>&nbsp;
    				</div>
    			</td>
    		</tr>
    	</table>
    </td>
  </tr>
</table>

<input type="hidden" name="program_id" value="<?=$program_id?>" />
</FORM>