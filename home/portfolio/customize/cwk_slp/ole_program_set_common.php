<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");

// customized lib
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cwk/lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$lpf = new libportfolio();
$lpf_ui = new libportfolio_ui();
$linterface = new interface_html();

$program_id = $program_id[0];
$program_name = current($lpf->returnVector("SELECT Title FROM {$eclass_db}.OLE_PROGRAM WHERE ProgramID = {$program_id}"));
########################################################
# Tab Menu : Start
########################################################

$TabMenuArr = libpf_tabmenu::getSlpMgmtTabs(IPF_CFG_SLP_MGMT_CWK_SLP, 1);

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_CWK_SLP);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

########################################################
# Operations : Start
########################################################
# tab menu
$tab_menu_html = $lpf_ui->GET_TAB_MENU($TabMenuArr);

$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['program_list'], "ole_program.php");
$MenuArr[] = array($program_name, "");
$navigation_html = $linterface->GET_NAVIGATION($MenuArr);

########################################################
# Operations : End
########################################################

########################################################
# Data Preparation : Start
########################################################
// Program Ability
$sql = "SELECT cwk_ca.AbilityID, cwk_ca.AbilityName, IF(cwk_opa.ProgramID IS NULL, 0, 1) AS AbilityCheck ";
$sql .= "FROM {$eclass_db}.CWK_COMMON_ABILITY cwk_ca ";
$sql .= "LEFT JOIN {$eclass_db}.CWK_OLE_PROGRAM_ABILITY cwk_opa ";
$sql .= "ON cwk_opa.AbilityID = cwk_ca.AbilityID AND cwk_opa.ProgramID = '{$program_id}' ";
$program_ability = $lpf->returnArray($sql);

$html_ability_check = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
for($i=0, $i_max=count($program_ability); $i<$i_max; $i++)
{
  $html_ability_check .= ($i%3 == 0) ? "<tr>" : "";

  $_ability_id = $program_ability[$i]["AbilityID"];
  $_ability_name = $program_ability[$i]["AbilityName"];
  $_ability_check = $program_ability[$i]["AbilityCheck"] ? "CHECKED=\"CHECKED\"" : "";

  $html_ability_check .= "<td width=\"30%\"><input type=\"checkbox\" name=\"program_ability[]\" id=\"program_ability_{$_ability_id}\" value=\"{$_ability_id}\" {$_ability_check} /><label for=\"program_ability_{$_ability_id}\"> {$_ability_name}</label></td>";
  
  $html_ability_check .= ($i%3 == 2 || $i+1 == $i_max) ? "</tr>" : "";
}
$html_ability_check .= "</table>";

// Program Criteria
$sql = "SELECT component.DefaultID, ".Get_Lang_Selection("component.ChiTitle", "component.EngTitle")." AS DisplayName, IF(p.ProgramID IS NULL, 0, 1) AS CriteriaCheck ";
$sql .= "FROM {$eclass_db}.OLE_ELE component ";
$sql .= "LEFT JOIN {$eclass_db}.OLE_PROGRAM p ";
$sql .= "ON INSTR(p.ELE, component.DefaultID) > 0 AND p.ProgramID = '{$program_id}' ";
$program_criteria = $lpf->returnArray($sql);

$html_criteria_check = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
for($i=0, $i_max=count($program_criteria); $i<$i_max; $i++)
{
  $html_criteria_check .= ($i%3 == 0) ? "<tr>" : "";

  $_criteria_id = $program_criteria[$i]["DefaultID"];
  $_criteria_name = $program_criteria[$i]["DisplayName"];
  $_criteria_check = $program_criteria[$i]["CriteriaCheck"] ? "CHECKED=\"CHECKED\"" : "";

  $html_criteria_check .= "<td width=\"30%\"><input type=\"checkbox\" name=\"program_criteria[]\" id=\"program_criteria_{$_criteria_id}\" value=\"{$_criteria_id}\" {$_criteria_check} /><label for=\"program_criteria_{$_criteria_id}\"> {$_criteria_name}</label></td>";
  
  $html_criteria_check .= ($i%3 == 2 || $i+1 == $i_max) ? "</tr>" : "";
}
$html_criteria_check .= "</table>";
########################################################
# Data Preparation : End
########################################################

########################################################
# Layout Display
########################################################
// set the current page title
$CurrentPage = "Teacher_OLE";

$linterface->LAYOUT_START();
include_once("template/ole_program_set_common.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();

?>