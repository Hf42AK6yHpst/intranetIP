<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cnecc/libpf-slp-cnecc.php");

$StudentID = $StudentID;
$StudentIDArray = explode(",",$StudentID);
$academicYearID = $academicYearID;
$issueDate = $issueDate;

intranet_auth();
intranet_opendb();

function displayMode($StudentIDArray, $academicYearID, $issueDate) {
		$tables = array();
		if (count($StudentIDArray) > 0) {
			foreach($StudentIDArray as $key => $studentId) {
				$libpf_slp_cnecc = new libpf_slp_cnecc($studentId,$academicYearID);

				$achievementArray = $libpf_slp_cnecc->getAchievement();
				$schoolTitleAndInfo = $libpf_slp_cnecc->generateSchoolTitleAndInfo();
				$studentInfo = $libpf_slp_cnecc->generateStudentInfo($issueDate);
				$reportContent = $libpf_slp_cnecc->generateReportContent($achievementArray);
				$remarks = $libpf_slp_cnecc->generateRemarks();
				$signatures = $libpf_slp_cnecc->generateSignatures();
				
				$tableTemplate = $libpf_slp_cnecc->getWholeReportTemplate();
				$tables[] = str_replace(
							array(	"{{{ SCHOOL TITLE AND INFO }}}", 
									"{{{ STUDENT INFO }}}", 
									"{{{ REPORT CONTENT }}}", 
									"{{{ REMARKS }}}", 
									"{{{ SIGNATURES }}}"),
							array(	$schoolTitleAndInfo, 
									$studentInfo, 
									$reportContent, 
									$remarks, 
									$signatures),
							$tableTemplate);
			}
		}
		$output = implode("<div style='page-break-after:always'>&nbsp;</div>", $tables);

				//style='page-break-after:always'
		return $output;
}

switch ($printType) {
	case "html":
		$output = displayMode($StudentIDArray, $academicYearID, $issueDate);

		break;
	case "word":
		$content = displayMode($StudentIDArray, $academicYearID, $issueDate);

		$ContentType = "application/x-msword";
//		$tags_to_strip = Array("A", "a", "HTML", "html", "BODY", "body","link");
		$tags_to_strip = Array("a", "html", "body","link");
		foreach ($tags_to_strip as $tag)
		{
			$content = preg_replace("/<\/?" . $tag . "(.|\s)*?>/i", "", $content);
		}

		$content = preg_replace("/(src=)(['\"]?).*(\/home\/portfolio\/customize\/cnecc\/markingSchema.jpg ?)/", "$1$2http://{$_SERVER['SERVER_NAME']}$3", $content);
		$content = preg_replace("/(src=)(['\"]?).*(\/file\/logo.gif ?)/", "$1$2http://{$_SERVER['SERVER_NAME']}$3", $content);
		$output = "<HTML>\n";
		$output .= "<HEAD>\n";
		$output .= returnHtmlMETA();
		$output .= "</HEAD>\n";
		$output .= "<BODY LANG=\"zh-HK\">\n";
		$output .= $content;
		$output .= "</BODY>\n";
		$output .= "</HTML>\n";

		header('Pragma: public');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

		header('Content-type: application/octet-stream');
		header('Content-Length: '.strlen($output));

		header('Content-Disposition: attachment; filename="report.doc";');

		print $output;
		exit();
	break;
	case "pdf":
		$output = $libpf_slp_cnecc->generatePdfReport();
		break;
	default:
		break;
}
echo $output;

intranet_closedb();
?>