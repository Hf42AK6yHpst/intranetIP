<!-- Modifying By :  -->
<script language="JavaScript">
$(document).ready(function() {
	writeSuccess();
});
function goPrintHtml() {
	$("form[name=form1]").attr("target", "_blank");
	$("form[name=form1]").attr("method", "POST");
	$("form[name=form1]").attr("action", "report.php?printType=html");
	$("form[name=form1]").submit();
}
function writeSuccess() {
	$succeed = $("input[name=success\\[\\]]:checked").length;
	$("#noOfSuccess").html($succeed+"&nbsp;");
}

function submitYearSelect(obj) {
	$("#academicYearId").val(obj.value);
	$("form[name=form1]").attr("target", "");
	$("form[name=form1]").attr("method", "POST");
	$("form[name=form1]").attr("action", "student_view.php");
	$("form[name=form1]").submit();

}
function jSUBMIT_FORM(){
	$("form[name=form1]").attr("target", "");
	$("form[name=form1]").attr("method", "POST");
	$("form[name=form1]").attr("action", "ss_update.php");
	if (checkform()) {
		$("form[name=form1]").submit();
	} else {
		// do nothing
	}
	return false;
}
function checkform() {
	var dates = $("input[name*=date]");
	try {
		$.each(dates, function(key, value) {
			if (check_date_allow_null(this, "<?=$MAXTEMP["Error"]["InvalidDateFormat".strtoupper($intranet_session_language)]?>")) {
				// do nothing
			} else {
				throw "err_date";
			}
		});
	} catch(msg) {
		if (msg=="err_date") {
			return false;
		} else {
			alert(msg);
			return false;
		}
	}

	return true;
}
function checkUncheckAll() {
	var successCheckBoxElements = $("input[name=success\[\]]");
	if ($("#successCheck").attr("checked") == true) {
		$.each(successCheckBoxElements, function(key, element) {
			$(element).attr("checked", true);
		});
	} else {
		$.each(successCheckBoxElements, function(key, element) {
			$(element).attr("checked", false);
		});
	}
	writeSuccess();
}
function isCheckTheAllBox() {
	var successCheckBoxElements = $("input[name=success\[\]]");
	try {
		$.each(successCheckBoxElements, function(key, element) {
			if ($(element).attr("checked")==true) {
				// do nothing
			} else {
				throw 'not_all';
			}
		});
	} catch(msg) {
		if (msg == 'not_all') {
			return false;
		}
	}
	return true;
}
$(document).ready(function() {
	if (isCheckTheAllBox()) {
		$("#successCheck").attr("checked", true);
	} else {
		$("#successCheck").attr("checked", false);
	}
});
</script>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />

<FORM name="form1" method="GET">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td align="center">
      <?=$tab_menu_html?>
  	</td>
  </tr>
  <?=$op_result?>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <?=$ay_selection_html?>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="100%">
            <table border="0" cellpadding="3" cellspacing="0">
				<tr>					
					<td class="navigation"><?=$navigation_html?></td>
				</tr>					
			</table>
		</td>
        </tr>
        <tr>
          <td colspan="2">
            <?=$table_content?>
      		</td>
      	</tr>
      	<tr>
      		<td align="center">
      		<?=$buttons?>
      		</td>
      	</tr>
      </table>
    </td>
  </tr>
</table>
<input type="hidden" name="StudentID" id="StudentID" value="<?=$StudentID?>" />
<input type="hidden" name="YearClassId" id="YearClassId" value="<?=$YearClassId?>" />
</FORM>