<?php
/**
 * Modifying By: Max
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($intranet_root."/includes/libportfolio.php");
include_once($intranet_root."/includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cnecc/libpf-slp-cnecc.php");

intranet_opendb();

# parameters from previous page
$successArray = $success;
$StudentID = $StudentID;
$academicYearID = $academicYearID;

$libpf_slp_cnecc = new libpf_slp_cnecc($StudentID, $academicYearID);

###################### CASES ######################
##	Case	Action			OS exist	CCSS exist
##	1		insert OS		X			X
##			insert CCSS		
##	2		insert CCSS		V			X
##	3		CASE NOT EXIST	X			V
##	4		NO ACTION		V			V
###################### CASES ######################

$sql_success = true;	// indicator for sql operations' success
# delete records in CUSTOM_CNECC_SS_STUDENT where StudentID = $StudentID and EventID not in $successArray
$sql_success = $sql_success && $libpf_slp_cnecc->removeRecord($successArray);

# get distinct EventIds from CCSS where StudentID = $StudentID
$ccssEventIds = $libpf_slp_cnecc->getEventIds();

# for successArray
if (count($successArray) > 0) {
	foreach($successArray as $key =>$eventId) {
		$enddate = ${"date".$eventId};
		# if $eventId in $ccssEventIds
		if (in_array($eventId, $ccssEventIds)) {	// CASE 4 - OS_RecordID exist in OLE_STUDENT and in CCSS
			$sql_success = $sql_success && $libpf_slp_cnecc->updateRecord($StudentID, $eventId, $enddate);
		# else
		} else {
			$osRecordId = $libpf_slp_cnecc->getRecordIdByEventId($eventId);
			# if OS_RecordID exist in OLE_STUDENT but not CCSS
			if ($osRecordId>0) {	//	CASE 2
				# insert new record to CUSTOM_CNECC_SS_STUDENT that CCSS.OS_RecordID = OS.RecordID
				$sql_success = $sql_success && $libpf_slp_cnecc->insertRecord($osRecordId, $eventId, $enddate);			
			# else
			} else {	// CASE 1
				# create new record in OLE_STUDENT
				$osRecordId = $libpf_slp_cnecc->insertOleStudentByEventId($eventId);
				$sql_success = $sql_success && $osRecordId;
				
				# insert new record to CUSTOM_CNECC_SS_STUDENT that CCSS.OS_RecordID = OS.RecordID
				$sql_success = $sql_success && $libpf_slp_cnecc->insertRecord($osRecordId, $eventId, $enddate);
			}
		}
	}
}
# message return
if ($sql_success) {
	$msg = "update";
} else {
	$msg = "update_failed";
}
intranet_closedb();

header("Location: student_view.php?StudentID=$StudentID&YearClassId=$YearClassId&academicYearID=$academicYearID&msg=$msg");
?>