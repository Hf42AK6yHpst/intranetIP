<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libpf-ole-program-tag.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/ole_program.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."home/portfolio/ajax/xml_nodePadding.php");
intranet_auth();
intranet_opendb();
//DEBUG_R(serialize($cnecc_event_info_R_N1));die;
//echo serialize($cnecc_event_id);die;
# Create new ole program
	$objOLEPROGRAM = new ole_program();
	$objOLEPROGRAM->setProgramType($ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]);
	$objOLEPROGRAM->setTitle($title);
	$objOLEPROGRAM->setStartDate($startdate);
	$objOLEPROGRAM->setEndDate($enddate);
	$objOLEPROGRAM->setCategory($category);
	$objOLEPROGRAM->setELE($ele);
	$objOLEPROGRAM->setCreatorID($UserID);
	$objOLEPROGRAM->setIntExt("INT");
	$objOLEPROGRAM->setAcademicYearID($academicYearID);
	$objOLEPROGRAM->setYearTermID($YearTermID);
	$objOLEPROGRAM->setComeFrom($ipf_cfg["OLE_PROGRAM_COMEFROM"]["teacherInput"]);
	
	$RecordID = $objOLEPROGRAM->SaveProgram();
# Create CCSE
	if($sys_custom['cnecc_SS'])
	{
		$setSunshine = 1;
		$cnecc_event_id = $cnecc_event_id;
		$sizeOfCnecc_event_id = count($cnecc_event_id);
	
		$eventInsertArray = array();
		$eventUpdateArray = array();
		
		for($i=0;$i<$sizeOfCnecc_event_id;$i++) {
				$attstr = "cnecc_event_info_A".$cnecc_event_id[$i];
				$rolstr = "cnecc_event_info_R".$cnecc_event_id[$i];
				$poistr = "cnecc_event_info_P".$cnecc_event_id[$i];
				$attvar = ${$attstr}[0];
				$rolvar = ${$rolstr}[0];
				$poivar = ${$poistr}[0];
			if (substr($cnecc_event_id[$i],0,2) == "_N") { // _N refer to new CCSE
				$eventInsertArray[] = array("ATTAINMENT"=>${attvar},"ROLE"=>${rolvar},"POINT"=>${poivar});
			} else {
				$eventUpdateArray[$cnecc_event_id[$i]]["ATTAINMENT"] = ${attvar};
				$eventUpdateArray[$cnecc_event_id[$i]]["ROLE"] = ${rolvar};
				$eventUpdateArray[$cnecc_event_id[$i]]["POINT"] = ${poivar};
			}
		}
//		DEBUG_R($eventInsertArray);
		include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cnecc/libpf-slp-cnecc.php");
		$libpf_slp_cnecc = new libpf_slp_cnecc();
//	DEBUG_R($eventInsertArray);
		if ($setSunshine == 1) {
			$ccseResult = $libpf_slp_cnecc->saveCcseProgram($RecordID, $eventInsertArray, $eventUpdateArray, $studentId);
		} else {
			// do nothing
		}
	}
if ($ccseResult) {
	$insertedId = mysql_insert_id();
}
intranet_closedb();
echo $insertedId;
?>