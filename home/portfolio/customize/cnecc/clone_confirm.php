<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/ole_program.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cnecc/libpf-slp-cnecc.php");

intranet_auth();
intranet_opendb();

//Receive Variable 

$SelectedAcademicYearID = intval($SelectedAcademicYearID);
$libpf_slp_cnecc = new libpf_slp_cnecc();


$lpf = new libportfolio();
$lpf_ui = new libportfolio_ui();
$linterface = new interface_html();
$objDB = new libdb();



$libpf_slp_cnecc->copy_sunshine_program($SelectedAcademicYearID);

intranet_closedb();
header("Location: selectYear.php?msg=2");


?>