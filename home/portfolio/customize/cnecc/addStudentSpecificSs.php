<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-ole-program-tag.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");
intranet_auth();
intranet_opendb();

// Initializing classes
$LibUser = new libuser($UserID);
$lpf = new libpf_slp(); // for libwordtemplates_ipf
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("ole");
$lpf_ole_program_tag = new libpf_ole_program_tag();

// template for teacher page
$linterface = new interface_html("popup.html");
// set the current page title
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

###############################################
$SelectedStatus = $status;
$SelectedELE = $ELE;
$SelectedYear = $Year;

$record_id = (is_array($record_id)) ? $record_id[0] : $record_id;

# template for activity title
$LibWord = new libwordtemplates_ipf(3);
$file_array = $LibWord->file_array;
# template

# template drop down list
for ($f=0; $f<sizeof($LibWord->file_array); $f++)
{
	$id = $LibWord->file_array[$f][0];
	unset($tempArr);
	unset($WordListArr);
	$tempArr = $LibWord->getWordList($id);
	$WordListArr[] = array("", " --- ".$button_select." --- ");
	for ($i=0; $i<sizeof($tempArr); $i++) {
		$TempTitleArray = explode("\t", $tempArr[$i]);
		$p_title = $TempTitleArray[0];
		$WordListArr[] = array(str_replace("&amp;", "&", $p_title), undo_htmlspecialchars($p_title));
	}
	$WordListSelection[] = array($id, returnSelection($WordListArr , "", "WordList", "onChange=\"document.form1.title.value = this.value \""));
}
# template drop down list

# float list arr
for ($f=0; $f<sizeof($LibWord->file_array); $f++) {

	$id = $LibWord->file_array[$f][0];
	
	unset($tempArr);
	$tempArr = $LibWord->getWordList($id);
	for ($i=0; $i<sizeof($tempArr); $i++) {
		$TempTitleArray = explode("\t", $tempArr[$i]);
		$p_title = $TempTitleArray[0];
		$ListArr[$id][] = str_replace("&amp;", "&", undo_htmlspecialchars($p_title));

		# preset ELE of the corresponding title
		$ListELEArr[$id][] = $TempTitleArray[1];
	}
}


###############################################

###############################################
# template for activity role
$LibWord2 = new libwordtemplates_ipf(4);

# float list arr
$role_array = $LibWord2->getWordList(0);
for ($i=0; $i<sizeof($role_array); $i++) {
	$RoleListArr[] = str_replace("&amp;", "&", undo_htmlspecialchars($role_array[$i]));
}
print ConvertToJSArray($RoleListArr, "jRoleArr1");

$defaultSelectArray = array('-9','--','');   //insert this array for default select category "--"

# template
###############################################

$attach_count = 0;

$NewCategoryArray = $lpf->get_OLR_Category();
$file_array = $LibWord->setFileArr();
array_unshift($file_array, $defaultSelectArray); //insert this array for default select category "--"
// get the least category id
foreach($NewCategoryArray as $CategoryID => $CategoryTitle)
{
	$TmpCategoryID = $CategoryID;
	break;
}

$JScriptID = "jArr".$TmpCategoryID;
$JELEScriptID = "jELEArr".$TmpCategoryID;
if($ELE!="")
{
	$ELEArr = array($ELE);
}
# define the navigation
$template_pages = Array(
	Array($ec_iPortfolio['ole'], "index.php?IntExt=$IntExt"),
	Array($button_new, "")
);

//$category = (!isset($category) || $category=="") ? 1 : $category;   // do not default as category = 1 fai 20091215;
$html_oleProgramType = ($IntExt == 1) ? $iPort["external_record"] : $iPort["internal_record"];

# build ELE list
$DefaultELEArray = $lpf->GET_ELE();
foreach($DefaultELEArray as $ELE_ID => $ELE_Name)
{
	$checked = (is_array($ELEArr) && in_array($ELE_ID, $ELEArr)) ? "CHECKED" : "";
	if ($checked) {
		$ELEList .= "<INPUT type='hidden' name='ele[]' value='".$ELE_ID."' id='".$ELE_ID."'><label for='".$ELE_ID."'>".$ELE_Name."</label><br />";
	}
}


if($startdate!="" && $enddate!="")
{
	$enddate_table_style = "display:block;";
	$add_link_style = "display:none;";
}
else
{
	$enddate_table_style = "display:none;";
	$add_link_style = "display:block;";
}

//HANDLE FOR THE PROGRAM TYPE 
$SubmitType = ($IntExt == 1) ? "EXT" : "INT" ;

// submit type (internal / external)
if($SubmitType == "EXT") 
{
	$DisplayStyle = "style='display:none;'";
	$OrganizationText = $iPort["external_ole_report"]["organization"];
}
else
{
	$DisplayStyle = "";
	$OrganizationText = $ec_iPortfolio['organization'];
}

$sumit_type_array = array();
$sumit_type_array[] = array("INT", $iPort["internal_record"]);
$sumit_type_array[] = array("EXT", $iPort["external_record"]);

# Get academic year
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
$ay_selection_html = getSelectByArray($academic_year_arr, "name='academicYearID' onChange='get_yearterm_opt()'", $ayID, 0, 0, "", 2);
if($record_id=="")
{
  $academic_yearterm_arr = array();
}
else
{
  $lay->AcademicYearID = $ayID;
  $academic_yearterm_arr = $lay->Get_Term_List(false);
}
$ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", $ytID, 0, 0, "", 2);

# Program Tag
$lpf_ole_program_tag->SET_CLASS_VARIABLE("program_id", $record_id);
$lpf_ole_program_tag->SET_PROGRAM_TAG_PROPERTY();
$tagID = $lpf_ole_program_tag->GET_CLASS_VARIABLE("tag_id");
$tmp_arr = $lpf_ole_program_tag->GET_PROGRAM_TAG_LIST();
$titlefield = ($intranet_session_language=="en") ? "EngTitle" : "ChiTitle";
foreach($tmp_arr as $i=>$values)
{
  $program_tag_arr[$i] = array($values["TagID"], $values[$titlefield]);
}

$program_tag_html = getSelectByArrayTitle($program_tag_arr, "name ='tagID' onChange=\" eval(onChangeStr) \" ", $i_general_please_select, $tagID, false, 2);

# Prepare allow to join

$canJoinCheckedYes = "";
$canJoinCheckedNo = "";
$joinPeriodDisplayStatus = "";
$CHECKED_STR = " checked = 'checked' ";
$DISPLAY_STR = " style = 'display:none;' ";
if ($canJoin) {
	$canJoinCheckedYes = $CHECKED_STR;
} else {
	$canJoinCheckedNo = $CHECKED_STR;
	$joinOptionsDisplayStatus = $DISPLAY_STR;
}
if ($autoApprove) {
	$autoApproveCheckedYes = $CHECKED_STR;
} else {
	$autoApproveCheckedNo = $CHECKED_STR;
}

$fcm = new form_class_manage();
$yearClassArr = $fcm->Get_Class_List_By_Academic_Year(Get_Current_Academic_Year_ID());
$year_arr = array();
for($i=0; $i<count($yearClassArr); $i++)
{
  $year_arr[] = array($yearClassArr[$i]["YearID"], $yearClassArr[$i]["YearName"]);
}
$year_arr = array_values(array_map("unserialize", array_unique(array_map("serialize", $year_arr))));
$year_selection_html = "<input type=\"checkbox\" name=\"yearMaster\" id=\"yearMaster\" onClick=\"jCHECK_ALL_YEAR(this)\" /> <label for=\"yearMaster\">".$i_general_all."</label><br />\n";
for($i=0; $i<count($year_arr); $i++)
{
  $t_year_id = $year_arr[$i][0];
  $t_year_name = $year_arr[$i][1];
  $t_id = "year_".$i;
  
  $t_checked = (strpos($joinableYear, $t_year_id) !== false) ? $CHECKED_STR : "";

  $year_selection_html .= "<img src=\"$image_path/spacer.gif\" width=\"20\" height=\"1\" /><input type=\"checkbox\" name=\"Year[]\" id=\"".$t_id."\" value=\"".$t_year_id."\" ".$t_checked."> <label for=\"".$t_id."\">".$t_year_name."</label>\n";
  if ($i%4==3) {
  	$year_selection_html .= "<br />";
  } else {
  	// do nothing
  }
}


///////////////////////// CNECC Cust : START /////////////////////////
if($sys_custom['cnecc_SS'] && $IntExt == 0)
{
	include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cnecc/libpf-slp-cnecc.php");
	$libpf_slp_cnecc = new libpf_slp_cnecc();
	$STUDENT_SPECIFIC = true;
	$cnecc_customization = $libpf_slp_cnecc->getOleNewCustomization($record_id,$STUDENT_SPECIFIC);
}
///////////////////////// CNECC Cust : END /////////////////////////

?>
<script language="JavaScript">
//var jCurrentArr = eval("<?=$JScriptID?>");
//var jCurrentELEArr = eval("<?=$JELEScriptID?>");
var jPresetELE = 1;

//onChangeStr = "document.getElementById('listContent').style.display = 'none'; jCurrentArr=eval('jArr'+this.value);";
onChangeStr = "document.getElementById('listContent').style.display='none';if(this.value == -9){jCurrentArr=new Array(0);jCurrentELEArr=new Array(0);}else{jCurrentArr=eval('jArr'+this.value); jCurrentELEArr=eval('jELEArr'+this.value);}";
</script>

<script language="JavaScript">

onChangeStr1 = "document.getElementById('title_selection_";
onChangeStr3 = "').style.display = 'block'";

function triggerFile(formObj, my_file, my_index)
{
	var is_remove = (eval("formObj.is_need_"+my_index+".checked==false"));
	var link = document.getElementById('a_'+my_index);
	link.style.textDecorationLineThrough = is_remove;
}

function checkform(formObj)
{	
	if ($("select[name='category']").val() == "-9") {
		alert("<?=$ec_warning['category']?>");
		return false;
	}
	// YuEn: check date and title!
	if(formObj.englishTitle.value=="")
	{
		alert("<?=$ec_warning['engTitle']?>");
		return false;
	}	
	if(formObj.startdate.value=="")
	{
		formObj.startdate.focus();
		alert("<?=$ec_warning['date']?>");
		return false;
	}
	if(formObj.category.options[formObj.category.selectedIndex].value == -9)
	{
		formObj.category.focus();
		alert("<?=$ec_warning['date']?>");
		return false;
	}
	else
	{
		if(!check_date(formObj.startdate,"<?=$w_alert['Invalid_Date']?>"))
		{
			return false;
		}
		else if(formObj.enddate.value!="")
		{
			if(!check_date(formObj.enddate,"<?=$w_alert['Invalid_Date']?>"))
				return false;
			else if(formObj.startdate.value>formObj.enddate.value)
			{
				formObj.enddate.focus();
				alert("<?=$w_alert['start_end_time2']?>");
				return false;
			}
		}
	}
	
	if(formObj.academicYearID.value=="")
	{
		formObj.academicYearID.focus();
		alert(globalAlertMsg18);
		return false;
  }
 
  if($("input:radio[name=allowStudentsToJoin]:checked").val() == "1")
  {
  	// check join period start date
  	if(formObj.jp_periodStart.value != "") {
  		if ( check_date(formObj.jp_periodStart, "<?= $ec_iPortfolio['SLP']['InvalidPeriodStart'] ?>") ) {
  			// check join period end date
  			if(formObj.jp_periodEnd.value != "") {
  				if ( check_date(formObj.jp_periodEnd, "<?= $ec_iPortfolio['SLP']['InvalidPeriodEnd'] ?>") ) {
  					if ( compareDate(formObj.jp_periodEnd.value, formObj.jp_periodStart.value) > -1 ) {
  						
  					} else {
  						alert("<?= $ec_iPortfolio['SLP']['PeriodStart_LT_PeriodEnd'] ?>");
  						return false;
  					}
  				} else {
  					return false;
  				}
  			} else {
  				alert("<?= $ec_iPortfolio['SLP']['FillTogether'] ?>");
  				return false;
  			}
  		
  		} else {
  			return false;
  		}		
  	} else {
  		if(formObj.jp_periodEnd.value != "") {
  			alert("<?= $ec_iPortfolio['SLP']['FillTogether'] ?>");
  			return false;
  		}
  	}
  	
  	if(!check_checkbox(formObj,"Year[]"))
  	{
      alert("<?=$ec_iPortfolio['SLP']['ChooseAtLeastOneYear']?>");
      return false;
    }
  }

	return true;
}

function jPRESET_ELE(i_pos, j_pos)
{
	var formObj = document.form1;
	var ELEObj = formObj.elements["ele[]"];
	var len = ELEObj.length;
	var jPresetELEList = jCurrentELEArr[i_pos][j_pos];

	for(var m=0; m<len; m++)
	{
		if(typeof(jPresetELEList)!="undefined" && jPresetELEList.indexOf(ELEObj[m].value)!=-1) {
			ELEObj[m].checked = 1;
		}
		else {
			ELEObj[m].checked = 0;
		}
	}
}

function jChangeSubmitType(obj)
{
	var row1 = document.getElementById("row_ele");
	var div1 = document.getElementById("div_org");
	
	// external
	if(obj.value == "EXT")
	{
		row1.style.display = "none";
		div1.innerHTML = "<?=$iPort["external_ole_report"]["organization"]?>";
	}
	else
	{
		// internal
		row1.style.display = "";
		div1.innerHTML =  "<?=$ec_iPortfolio['organization']?>";
	}
}

function jCHECK_ALL_YEAR(jParcheckObj)
{
  var checked = jParcheckObj.checked;
  setChecked(checked, document.form1, "Year[]");
}

function changeDisplay(obj) {
	objID = obj.id;
	switch (objID)
	{
		case 'astj_yes':
			show('joinOptions');
			break;
		case 'astj_no':
			hide('joinOptions');
			break;
	}
}
function show(objID) {
	document.getElementById(objID).style.display = '';
}
function hide(objID) {
	document.getElementById(objID).style.display = 'none';
}

function changeSubCategory() {
	$("#listContent").hide();
	$("#subCategorySelectionField").html("<?=$iPort['loading']?>...");
	var catID = $("select[name='category']").val();
	$.ajax({
		type: "GET",
		url: "../../ajax/ajax_reload_slp.php",
		async: false,
		data: "ActionType=Change_Sub_Category&CategoryID="+catID+"&SubCategoryID=<?=$subCategoryID?>",
		success: function (xml) {
			$("#subCategorySelectionField").html($(xml).find("subCategorySel").text());
		}
	});
	// reset the current array for preset programme names and the corresponding ele array
	jCurrentArr = new Array(0);
	jCurrentELEArr = new Array(0);
}

function setTitleLayer() {
	$("#listContent").hide();
	$("#change_preset").hide();
	$("#change_preset_loading").show();
	var catID = $("select[name='category']").val();
	var subCatID = $("select[name='subCategory']").val();
	var urlString = "../../ajax/ajax_reload_slp.php";
	var dataString = "";
	if (subCatID == null || subCatID == -9) {
		dataString = "ActionType=Get_Program_Name_And_Ele&CategoryID="+catID;
	} else {
		dataString = "ActionType=Get_Program_Name_And_Ele&SubCategoryID="+subCatID;
	}
//	alert(urlString+"?"+dataString);
	$.ajax({
		type: "GET",
		url: urlString,
		data: dataString,
		success: function (xml) {
			// parent node is <elements>
			child = $(xml).find("programNameElements");
			
			// initialize the current array for preset programme names and the corresponding ele array
			jCurrentArr = new Array(child.size());
			jCurrentELEArr = new Array(child.size());
			
			arr_pos_count = 0;
			child.each(function() {
				var title = $(this).find("title").text();
				var ele = $(this).find("ele").text();
				
				jCurrentArr[arr_pos_count] = new Array(1);
				jCurrentELEArr[arr_pos_count] = new Array(1);
				
				jCurrentArr[arr_pos_count][0] = title;
				jCurrentELEArr[arr_pos_count][0] = ele;
				
				arr_pos_count = arr_pos_count + 1;
			});
			
			$("#change_preset").show();
			$("#change_preset_loading").hide();
		}
	});
}

function check_academic_year()
{
  var startdate = $("[name=startdate]").val();
  if(startdate == "") return;

	$.ajax({
		type: "GET",
		url: "../../ajax/ajax_check_yearID.php",
		data: "startdate="+startdate,
		success: function (msg) {
      if(msg != "")
      {
        var ay_id = msg;
        $("[name='academicYearID']").val(ay_id);
        get_yearterm_opt();
      }
		}
	});
}

function get_yearterm_opt()
{
  var ay_id = $("[name=academicYearID]").val();
  var startdate = $("[name=startdate]").val();
  if(ay_id == ""){
    $("#ayterm_cell").html('');
    return;
  }
  
	$.ajax({
		type: "GET",
		url: "../../ajax/ajax_get_yearterm_opt.php",
		data: "ay_id="+ay_id+"&startdate="+startdate,
		success: function (msg) {
      if(msg != "")
      {
        $("#ayterm_cell").html(msg);
      }
		}
	});
}
function jdebug(objName) {
	obj = eval(objName);
	var temp = "";
	for (x in obj)
			temp += x + ": " + obj[x] + "\n";
	alert (temp);
}
function close_and_refresh_parent(insertedId){
	window.parent.location.reload();
	window.parent.tb_remove();
}

function submitForm() {
	if (checkForSunshineSetting($("form[name=form1]")[0])) {
		$.post('ajax_addStudentSpecificSs.php',
			{
				category: $("select[name=category]").val(),
				ele: $("input[name=ele[]]").val(),
				title: $("input[name=englishTitle]").val(),
				startdate: $("input[name=startdate]").val(),
				enddate: $("input[name=enddate]").val(),
				academicYearID: $("select[name=academicYearID]").val(),
				YearTermID: $("select[name=YearTermID]").val(),
				'cnecc_event_id[]': $("input[name=cnecc_event_id[]]").val(),
				'cnecc_event_info_A_N1[]': $("input[name=cnecc_event_info_A_N1[]]").val(),
				'cnecc_event_info_R_N1[]': $("select[name=cnecc_event_info_R_N1[]]").val(),
				'cnecc_event_info_P_N1[]': $("input[name=cnecc_event_info_P_N1[]]").val(),
				studentId: <?=$studentId?>
			},
			function(data) {
				if (data) {
					close_and_refresh_parent(data);
				} else {
					alert("update failed");
				}
			}
		);
	} else {
	}
}

$(document).ready(function(){
	// initialize sub category
	changeSubCategory();
	setTitleLayer();
<?php if($record_id=="") { ?>
	check_academic_year();
<?php } ?>
});
</script>

<FORM name="form1" method="post" action="">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td >
	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center" >
	<tr>
		<td valign="top" >
		<!-- CONTENT HERE -->
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td width="80%" valign="top">
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			
			<!-- type -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?php echo $iPort['submission_type']; ?><span class="tabletextrequire">*</span></span>
				</td>
				<td width="80%" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="60%" >
				<tr>
					<td width="100%" ><?=$html_oleProgramType?>
					<input type ="Hidden" value="<?=$SubmitType?>" name ="SubmitType">
					</td>
				</tr>
				</table>
				</td>
			</tr>
			<!-- ele -->
			<tr id="row_ele" valign="top" <?=$DisplayStyle?> >
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?=$ec_iPortfolio['ele']?></span>
				</td>
				<td><?=$ELEList?></td>
			</tr>
			
			<!-- cateogry -->
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?=$ec_iPortfolio['category']?><span class="tabletextrequire">*</span></span>
				</td>
				<td>
				<?=$linterface->GET_SELECTION_BOX($file_array, "name ='category' onChange=\"setTitleLayer(); \" ","",$category);
				?>
				</td>
			</tr>
						
			<!-- title -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?= $ec_iPortfolio['title'] ?><span class="tabletextrequire">*</span></span>
				</td>
				<td width="80%" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="60%" >
				<tr>
					<td width="100%" ><input id='englishTitle' name="englishTitle" id="englishTitle" type="text" size="50" value="<?=$engTitle?>" class="textboxtext"  maxlength="255"></td>
					<td width="5">&nbsp;</td>
					<td width="25" ><div id="change_preset_loading" style="display: none;"><img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif"></div><div id="change_preset"><?= GetPresetText("jCurrentArr", 1, "englishTitle", "jPRESET_ELE()");?></div></td>
				</tr>
				</table>
				</td>
			</tr>
			<!-- date -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?=$ec_iPortfolio['date']?><span class="tabletextrequire">*</span></span></td>
				<td>

				<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td nowrap='nowrap'>
					<input type="text" name="startdate" size="11" maxlength="10" class="tabletext" value="<?=$startdate?>" onChange="check_academic_year()" onFocus="if(this.value==''){this.value='<?=date("Y-m-d")?>'; check_academic_year();}" />
					<?=$linterface->GET_CALENDAR("form1", "startdate", "check_academic_year()")?>
					</td>
					<td nowrap='nowrap' id="table_enddate" style="<?=$enddate_table_style?>">&nbsp;<?=$profiles_to?>&nbsp;<input class="tabletext" type="text" name="enddate" size="11" maxlength="10" value="<?=$enddate?>" onFocus="if(this.value==''){this.value='<?=date("Y-m-d")?>';}">
					<?=$linterface->GET_CALENDAR("form1", "enddate")?>
				</tr>
				<tr id="add_link" style="<?=$add_link_style?>"><td height="25"><a href="javascript:displayTable('add_link', 'none');displayTable('table_enddate', 'block');" class='tablelink'><?=$ec_iPortfolio['add_enddate']?></a></td></tr>
				</table>
				</td>
			</tr>
			
      <!-- year term -->
      <tr>
      	<td valign="top" nowrap="nowrap" class="formfieldtitle">
      	  <span class="tabletext"><?=$ec_iPortfolio['year']?><span class="tabletextrequire">*</span></span></td>
      	<td>
      
      	<table border="0" cellspacing="3" cellpadding="0">
      		<tr>
      			<td nowrap='nowrap'><?=$ay_selection_html?></td>
      			<td nowrap='nowrap' id='ayterm_cell'><?=$ayterm_selection_html?></td>
      		</tr>
      	</table>
      	</td>
      </tr>
			

			<?=$cnecc_customization?>
			</table>
			</td>
		</tr>
		<tr>
			<td height="1" class="dotline" colspan="2"  ><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
		</tr>
		
		<tr>
			<td  colspan="2" align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="tabletextremark"><?=$ec_iPortfolio['mandatory_field_description']?></td>
				<td align="right">
				<?= $linterface->GET_ACTION_BTN($button_submit, "button", "submitForm();") ?>
				</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
		<br>
		<!-- End of CONTENT -->
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>