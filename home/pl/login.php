<?php
/*
 * Editing By : 
 * 
 * Modification Log:
 * 2016-09-05 Pun [ip.2.5.7.10.1]
 *  - Change retrieveSessionKey load from libdb getSessionKeyLastUpdatedFromUserId()
 * 2016-05-20 (Paul) [ip.2.5.7.7.1]
 * 	- Add new handling such that PowerFlip_lite and PowerFlip do the same thing
 * 2016-03-16 (Paul) [ip.2.5.7.4.1]
 * 	- Add new jumpback parameter to handle the case manageLessonPlan for logging into the classroom
 * 
 * */

$PATH_WRT_ROOT = "../../"; 
include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
include($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

# This page can be access by Android Only
if(!$sys_custom['power_lesson_app_web_view_all_platform'] && !$plugin['PowerFlip'] && !$plugin['PowerFlip_lite'] && (!$plugin['power_lesson'] || (isset($userBrowser) && $userBrowser->platform!='Andriod'))){
	die();
}

# prepare eClass login info
$lu = new libuser($UserID);
     	
# check if session key from DB is new (i.e. updated within 15mins)
$results = $lu->getSessionKeyLastUpdatedFromUserId($UserID);
$session_key = trim($results["SessionKey"]);
if ($session_key=="")
{
	$session_key = $lu->generateSessionKey();
	$lu->updateSession($session_key);
}
$lu->updateSessionLastUpdatedByUserId($UserID);

## remove the '/' at the end of the $eclass_url_root as powerlesson needs to retrieve the action frim url path
if($eclass_url_root!="" && $eclass_url_root{strlen($eclass_url_root)-1} == "/"){
	$eclass_url_root = substr($eclass_url_root, 0, strlen($eclass_url_root)-1);
}

if($eclass_httppath_first!="" && $eclass_httppath_first{strlen($eclass_httppath_first)-1} == "/"){
	$eclass_httppath_first = substr($eclass_httppath_first, 0, strlen($eclass_httppath_first)-1);
}

//DEFAULT THE URL IS eclass40
$redirect_url_root = $eclass_url_root;
$redirect_httppath_first = $eclass_httppath_first;
	
$url = ($eclass_httppath_first == "") ? $redirect_url_root : $eclass_httppath_first;

if($plugin['PowerFlip'] || $plugin['PowerFlip_lite']){
	switch ($targetAction){
		case 'goLessonPlan':
			$jumpback = $InProgress? 'PowerFlipFromAppWeb_Happening':'PowerFlipFromAppWeb';
			break;
		case 'addLessonPlan':
			$jumpback = 'PowerFlipFromAppWeb_AddLessonPlan';
			break;
		case 'manageLessonPlan':
			$jumpback = 'PowerFlipFromAppWeb_ManageLessonPlan';
			break;	
	}
}
else{
	$jumpback = $InProgress? 'PowerLessonFromAppWeb_Happening':'PowerLessonFromAppWeb';
}

header("Location: ".$url."/checkpl.php?user_course_id=".$UserCourseID."&eclasskey=".$session_key."&r_var=".$LessonID."&jumpback=".$jumpback);

intranet_closedb();
?>