<?
#using: 
/*
 * 2015-12-10 (Carlos): Applied IntegerSafe() to integer type parameters to prevent SQL injection.
 */

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libftp.php");
intranet_auth();
intranet_opendb();

$ChannelID = IntegerSafe($ChannelID);
$UserID = IntegerSafe($UserID);

$lu = new libuser($UserID);
if (!$lu->isTeacherStaff())
{
     header("/home/school/close.php");
     exit();
}
$ClipType = 2;
$ClipName = intranet_htmlspecialchars(trim($ClipName));
$Description = intranet_htmlspecialchars(trim($Description));

    $filepath = $ClipFile;
    $filename = $ClipFile_name;

    $lftp = new libftp();
    $lftp->host = $tv_ftp_host;
    if ($tv_host_type == "") $tv_host_type = "Windows";
    $lftp->host_type = $tv_host_type; #"Windows";

    if (!$lftp->connect($tv_ftp_user,$tv_ftp_passwd))
    {
         header("Location: upload.php");
         exit();
    }

    # Get random path
    $target_path = substr(md5(intranet_random_passwd(10)),0,8);


    $current_dir = $lftp->pwd();
    $lftp->mkdir($target_path);
    $lftp->upload($filepath,"$current_dir/$target_path",$filename,1);

    $URL = "$tv_base_path/$target_path/$filename";
    $lftp->close();


$RecordStatus = $sys_custom['CampusTVAutoApprove'] ? 2 : 1;
	
$sql = "INSERT INTO INTRANET_TV_MOVIECLIP (Title, Description,ClipURL,FilePath,FileName,Recommended, isFile,UserID,RecordType,RecordStatus,DateInput,DateModified)
        VALUES ('$ClipName','$Description','$URL','$target_path','$filename', '1','$ClipType','$UserID','$ChannelID',$RecordStatus,now(),now())";
$lu->db_db_query($sql);

intranet_closedb();
header("Location: upload.php?success=1");
?>
