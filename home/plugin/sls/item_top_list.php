<?php
include_once ("../../../includes/global.php");
include_once ("../../../includes/libdb.php");
include_once ("../../../includes/libuser.php");
include_once ("../../../includes/libcal.php");
include_once ("../../../includes/libcalevent.php");
include_once ("../../../includes/libslslib.php");
include_once ("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$lc = new libcalevent();
$now = time();
if ($start == "")
{
    $start_stamp = $lc->getStartOfAcademicYear($now);
    $start = date('Y-m-d',$start_stamp);
}
if ($end == "")
{
    $end_stamp = $lc->getEndOfAcademicYear($now);
    $end = date('Y-m-d',$end_stamp);
}

$lsls = new libslslib();
$info_content = $lsls->getTopItems($start,$end,10);

include_once ("slsheader.php");
$x = "";

for ($i=0; $i<sizeof($info_content); $i++)
{
     list ($tcode,$title,$callnum,$number) = $info_content[$i];
     //$x .= "<tr><td>$tcode</td><td>$title</td><td>$callnum</td><td>$number</td></tr>\n";
      $x .= "<tr><td>".($i+1).".</td><td>$title</td><td>$callnum</td><td align='right'>$number</td></tr>\n";

}
if ($x == "")
{
    $x = "<tr><td align=center colspan=4>$i_sls_library_norecords</td></tr>\n";
}

?>
<form action="" method="get">
<table border='0' background='images/sls_info_range_form.gif' width='278' height='118'>
<tr><td colspan='2'>&nbsp;</td></tr>
<tr><td align='right' width='78'><?=$i_sls_library_from?></td><td width='200'><input type=text name=start value='<?=$start?>'></td></tr>
<tr><td align='right'><?=$i_sls_library_to?></td><td><input type=text name=end value='<?=$end?>'></td></tr>
<tr><td colspan='2' align='center'><input type=image src='<?="$intranet_httppath$image_submit"?>' border=0>
</td></tr>
</table>
</form>


<table width="80%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="20"><img src="images/library/lib_top_l.gif" width="20" height="47"></td>
    <td align="center" STYLE="background-image: url(images/library/lib_cell_t.gif)"><img src="images/library/head_top10items_<?=$intranet_session_language?>.gif" width="519" height="47"></td>
    <td width="20"><img src="images/library/lib_top_r.gif" width="20" height="47"></td>
  </tr>
  <tr>
    <td STYLE="background-image: url(images/library/lib_cell_l.gif)">&nbsp;</td>
    <td align="center" bgcolor="#FBFEEE">
      <table width="80%" border="1" cellspacing="0" cellpadding="3" bordercolordark="#FAB341">
<tr>
<td>#</td>
<td><b><?=$i_sls_library_book_title?></b></td>
<td><b><?=$i_sls_library_call_number?></b></td>
<td nowrap><b><?=$i_sls_library_borrow_count?></b></td>
</tr>
<?=$x?>
</table>
    </td>
    <td STYLE="background-image: url(images/library/lib_cell_r.gif)">&nbsp;</td>
  </tr>
  <tr>
    <td><img src="images/library/lib_btm_l.gif" width="20" height="16"></td>
    <td STYLE="background-image: url(images/library/lib_cell_b.gif)"><img src="images/library/lib_cell_b.gif" width="10" height="16"></td>
    <td><img src="images/library/lib_btm_r.gif" width="20" height="16"></td>
  </tr>
</table>


<?
include_once ("../../../templates/filefooter.php");
intranet_closedb();
?>