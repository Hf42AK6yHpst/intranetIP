<?php
$PATH_WRT_ROOT = '../../..';

include_once($PATH_WRT_ROOT . "/includes/global.php");

if($plugin['stem_x_pl2'] && ($UserType == USERTYPE_STAFF || $UserType == USERTYPE_STUDENT)){
    intranet_opendb();

    $stemLicenseConfig = require $PATH_WRT_ROOT."/includes/StemLicense/StemLicense.inc.php";
    
    include_once($PATH_WRT_ROOT . "/includes/json.php");
    $jsonObj = new JSON_obj();
    
    include_once($PATH_WRT_ROOT . "/includes/libdb.php");
    include_once($PATH_WRT_ROOT . "/includes/libuser.php");
    $luser = new libuser($_SESSION['UserID']);

    include_once($PATH_WRT_ROOT . "/includes/StemLicense/StemLicense.php");
    $stemLicense = new StemLicense();
    $tokenAry    = $stemLicense->run('createSsoToken', array());
    
    include_once($PATH_WRT_ROOT . "/includes/elearning/libAES.php");
    $laes = new libAES($stemLicenseConfig['aesKey']);
    
    $defaultLang = '';
    switch($intranet_session_language){
        case 'b5':
            $defaultLang = 'zh-hk';
            break;
        case 'gb':
            $defaultLang = 'zh-cn';
            break;
        default:
            $defaultLang = 'en';
    }
    
    $queryData = array();
    $queryData['schoolCode']  = $config_school_code;
    $queryData['userLogin']   = $luser->UserLogin;
    $queryData['token']       = is_array($tokenAry)? $tokenAry['ssoToken'] : '';
    $queryData['defaultLang'] = $defaultLang;

    if($sys_custom['StemPL2Client'] && !$_SESSION['SSV_PRIVILEGE']['schoolsettings']['isAdmin']){
        $queryData['directSso'] = true;
    }

    $stemLicenseSsoUrl      = $stemLicenseConfig['serverPath'] . 'eclass40/src/stem/license/sso/login.php';
    $eClassRequestEncrypted = $laes->encrypt($jsonObj->encode($queryData));
} else {
    header('HTTP/1.1 401 Unauthorized');
    exit;
}
?>
<html>
<body onload="document.powerLessonLogin.submit()">
<form id="powerLessonLogin" name="powerLessonLogin" style="display:none;" action="<?= $stemLicenseSsoUrl ?>"method="POST">
    <input type="hidden" name="eClassRequestEncrypted" value="<?= $eClassRequestEncrypted ?>"/>
</form>
</body>
</html>