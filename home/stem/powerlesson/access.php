<?php
$PATH_WRT_ROOT = '../../..';

include_once( $PATH_WRT_ROOT . "/includes/global.php");

if($plugin['stem_x_pl2'] && ($UserType == USERTYPE_STAFF || $UserType == USERTYPE_STUDENT)){
    intranet_opendb();
    
    include_once( $PATH_WRT_ROOT . "/includes/libdb.php");
    include_once( $PATH_WRT_ROOT . "/includes/libuser.php");
    $luser = new libuser($_SESSION['UserID']);
    
    $stemLicenseConfig = require $PATH_WRT_ROOT."/includes/StemLicense/StemLicense.inc.php";
    $stemLicenseApiUrl = $stemLicenseConfig['serverPath'] . 'eclass40/src/stem/license/api';
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $stemLicenseApiUrl . '/accessibility/school/'.$config_school_code . '/user/' . $luser->UserLogin);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
    curl_setopt($ch, CURLOPT_TIMEOUT, 300);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Accept: application/json"));
    curl_exec($ch);
    
    $statusCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
    
    intranet_closedb();
    
    if(!(curl_errno($ch) > 0) && $statusCode >= 200 && $statusCode <= 299){
        header('HTTP/1.1 200 OK');
        exit;
    }
}

header('HTTP/1.1 401 Unauthorized');