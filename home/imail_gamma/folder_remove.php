<?php
// editing by 
/************** Change log ***************
 * 2013-10-04 (Carlos): Modified paramater $Folder to be base64 decoded 
 * 2011-05-06 (Carlos): Added before deleting the folder, copy all mails to Trash
 *****************************************/
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccess.php");
include_once("../../includes/imap_gamma.php");
//include_once("../../includes/libcampusmail.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
auth_campusmail();

$IMap = new imap_gamma();
//$lc = new libcampusmail();


## Get Data
//$CurFolder = (isset($Folder) && $Folder != "") ? stripslashes(urldecode($Folder)) : "";
$CurFolder = (isset($Folder) && $Folder != "") ? base64_decode(stripslashes($Folder)) : "";

## Main
if(trim($CurFolder) != ""){
	## Move mails to Trash
	$change_folder_success = $IMap->Go_To_Folder($CurFolder);
	if($change_folder_success)
	{
		$IMap_obj = imap_check($IMap->inbox);
		$max_msgno = $IMap_obj->Nmsgs;
		if($max_msgno>0){
			for($msgno=1;$msgno<=$max_msgno;$msgno++){
				imap_mail_move($IMap->inbox,$msgno,$IMap->TrashFolder);
			}
		}
	}
	## Delete folder
	$Result = $IMap->Delete_Mail_Folder($CurFolder);
}

intranet_closedb();
header("Location: folder.php?msg=3");
?>