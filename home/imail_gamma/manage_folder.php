<?php
// page modifing by: 
$PathRelative = "../../../../";
$CurSubFunction = "Communication";
$CurPage = $_SERVER["REQUEST_URI"];

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}
AuthPage(array("Communication-Mail-GeneralUsage"));

include_once($PathRelative."src/include/template/general_header.php");
include_once($PathRelative."src/include/class/imap_gamma.php");
include_once($PathRelative."src/include/class/imap_gamma_ui.php");

#########################################################################################################

## Use Library
$lui  = new imap_gamma_ui();
$IMap = new imap_gamma();


## Get Data
$CurMenu = '1';
$CurTag  = (isset($CurTag) && $CurTag != "") ? stripslashes(urldecode($CurTag)) : '1';
$Msg 	 = (isset($Msg) && $Msg != "") ? $Msg : '';
$Folder  = (isset($Folder) && $Folder != "") ? stripslashes(urldecode($Folder)) : $IMap->FolderPrefix;


## Preparation
##############################################################################
# Imap use only
//$imap = imap_open("{" . $server . ":" . $port . "}" . $mailbox, $username, $password);
imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$Folder);		# Go to the Corresponding Folder Currently selected

$imap_obj 	   = imap_check($IMap->inbox);
$unseen_status = imap_status($IMap->inbox, "{".$IMap->host."}".$Folder, SA_UNSEEN);
$statusInbox   = imap_status($IMap->inbox, "{".$IMap->host."}Inbox", SA_UNSEEN);

$imapInfo['imap_obj'] 	   = $imap_obj;
$imapInfo['unseen_status'] = $unseen_status;
$imapInfo['statusInbox']   = $statusInbox;
##############################################################################


## Main
echo $lui->Get_Div_Open("module_imail", 'class="module_content"');
echo $lui->Get_Sub_Function_Header($Lang['email_gamma']['HeaderTitle'], $Msg);
echo $lui->Get_Switch_MailBox();

## Left Menu
echo $lui->Get_Left_Sub_Mail_Menu($CurMenu, $CurTag, $IMap, $Folder, $ImapInfo);


## Right Menu
echo $lui->Get_Manage_Folder_Form($Folder, $IMap);

#########################################################################################################
include_once($PathRelative."src/include/template/general_footer.php");

?>