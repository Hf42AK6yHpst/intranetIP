<?php
$PATH_WRT_ROOT = '../../../';

$parLang = $_GET['parLang'];

switch (strtolower($parLang)) {
case 'en':
$intranet_hardcode_lang = 'en';
break;
default:
$intranet_hardcode_lang = 'b5';
}

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libpwm.php");


include_once($PATH_WRT_ROOT."lang/lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
$uid = $_GET['uid'];

if ($_SESSION['UserID']==''){
	$UserID = $uid;
	$_SESSION['UserID'] = $uid;	
}

//intranet_auth();
intranet_opendb();

$libDb = new libdb(); 
$libeClassApp = new libeClassApp();
$libpwm= new libpwm();
$linterface = new interface_html();
$folderAssoAry ="";
$defaultFolder = array();
$personalFolder = array();

$token = $_GET['token'];
$ul = $_GET['ul'];
$AttachmentWorksOnCurrentVersion = $_GET['AttachmentWorksOnCurrentVersion'];
$passwordAry = $libpwm->getData($uid);
foreach((array)$passwordAry as $_folderType => $_folderKey){
	if($_folderType==$uid)$password=$_folderKey;
}

	$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
	if (!$isTokenValid) {
		die("No access right to see this page.");
	}
    $thisFolder = $_GET["TargetFolderName"]; 
	$_displayName = $Lang['Gamma']['SystemFolderName'][$thisFolder];
	if($_displayName==''){
		$_displayName = $thisFolder;
	}
if ($plugin['imail_gamma']) {

    $curUserId = $uid;

    $sql = "Select ImapUserEmail From INTRANET_USER Where UserID = '".$curUserId."'";
	$dataAry = $libDb->returnResultSet($sql);

	$iMapEmail = $dataAry[0]['ImapUserEmail'];
	$iMapPwd = $password;
    $IMap = new imap_gamma(false,$iMapEmail, $iMapPwd);
    /*url for selected email's content*/
    /*url for composebutton*/
    $urlSuffix = "&TargetFolderName=".$thisFolder."&CurrentUID=".$currentUID."&token=".$token."&uid=".$uid."&ul=".$ul."&parLang=".$parLang;
    if($thisFolder!="INBOX"){
    	$Folder = "INBOX.".$thisFolder;
    }else{
    	$Folder = $thisFolder;
    }
    
  $thisFolder = urlencode($thisFolder);//encode thisFolder for url use handle chinese folder case
  $_displayName = IMap_Decode_Folder_Name($_displayName);
	
$sort = "SORTARRIVAL";
$displayMode = (isset($displayMode) && $displayMode != "") ? $displayMode : "folder";
$Folder = (isset($Folder) && $Folder != "") ? stripslashes($Folder) : $IMap->InboxFolder;
$order 	 = (isset($order) && $order != "") ? $order : 1;
$CacheNewMail = false;

if (!$IMap->ExtMailFlag) {
	if($IMap->ConnectionStatus == true){
		$imap_reopen_success = imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));	
		if($Folder == $IMap->SpamFolder) {
			// Connect Junk box twice to let mail server auto correct read/unread status
			$imap_reopen_success = imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));
		   }
		   
		if(!$imap_reopen_success){
			sleep(1);
			imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));
		}

		$imapInfo['imap_stream']   = $IMap->inbox;
		$imapInfo['server']    	   = $IMap->host;
		$imapInfo['password'] 	   = $IMap->CurUserPassword;
		$imapInfo['port']  	   	   = $IMap->mail_server_port;
		$imapInfo['username']  	   = $IMap->CurUserAddress;
		$imapInfo['displayMode']   = $displayMode;
		$imapInfo['sort'] 		   = $sort;
		$imapInfo['reverse'] 	   = $order;
		# Can be ignored
		$imapInfo['mailbox']   	   = $Folder;// orig: mailbox

		$isDraftInbox = ($Folder == $IMap->DraftFolder) ? 1 : 0;
		$isOutbox = ($Folder == $IMap->SentFolder) ? 1 : 0;
		$array = $IMap->Get_Sort_Index($IMap, $imapInfo, $search, $MailStatus);

		$imapInfo['pageInfo']['totalCount'] = (is_array($array)) ? count($array) : 0;
		$imapInfo['pageInfo']['pageStart']  = ($pageSize * ($imapInfo['pageInfo']['pageNo'] - 1) + 1);
		$startIndex = 0;
		$emailAryTotalCount = $imapInfo['pageInfo']['totalCount'];
		if($emailAryTotalCount >($startIndex+20)){
			$endIndex = ($startIndex+20);
		}else{
			$endIndex = $imapInfo['pageInfo']['totalCount'];
		}
		for ($i= $startIndex ; $i<$endIndex; $i++) {
		$MailHeaderList[] = imap_headerinfo($IMap->inbox, $array[$i], 80, 80);
		$msgno[] = $array[$i];
		}
        $emailAry = array ();
        $MailHeaderListcounter = count($MailHeaderList);     
   		for($i= 0 ; $i<$MailHeaderListcounter; $i++){
		$contentAry = $MailHeaderList[$i];
		$unSeen = $contentAry->Unseen;
		
        $_unSeen = ($unSeen=="U")?true:false;         
        $subject = $IMap->MIME_Decode($contentAry->subject); 
	 	if ($subject != "") {
			$subjectPrint = (substr($subject, 0, 10) == "=?UTF-8?Q?") ? substr($subject, 10, -2) : $subject ;
		} else {
			$subjectPrint = "No subject" ;
		}
		$subjectPrint = $AttachmentIcon.htmlspecialchars($subjectPrint);
		$subjectPrint = $IMap->BadWordFilter($subjectPrint);
        $emailAry[$i]['Subject'] = $subjectPrint;
        if($IMap->IsToday(str_ireplace("UT","UTC",$contentAry->date)))
		$DateFormat = "H:i";
		else
		$DateFormat = "Y-m-d";
		$prettydate = date($DateFormat, strtotime(str_ireplace("UT","UTC",$contentAry->date)));
        $emailAry[$i]['Date'] = $prettydate;
        
        if($thisFolder=='Drafts'||$thisFolder=='Sent'){
        $toArray = $contentAry-> to;
        $_stdClassObject = $toArray[0];
        
        }else{        	
        $fromArray = $contentAry-> from;
        $_stdClassObject = $fromArray[0];
        }
       $mailbox =  $IMap->MIME_Decode( $_stdClassObject->personal);
        //if not internal user add the email address's name as the title
       if($mailbox==''){
       	 $mailbox = $_stdClassObject->mailbox;  
       } 	
        //if has no user
       if($mailbox==''){
       	$mailbox = "No Recipients";
       }
       $emailAry[$i]['Mailbox'] = $mailbox;
        
        $currentMessageNo = $contentAry-> Msgno;
        $iscurrentAttachment = $IMap->Check_Any_Mail_Attachment($currentMessageNo);
                
        $_hasAttachment = ($iscurrentAttachment==1)?true:false;
        $currentUID = $IMap->Get_UID ($currentMessageNo);
        $emailAry[$i]['UID'] = $currentUID;
        $emailAry[$i]['UnSeen'] = $_unSeen;
        $emailAry[$i]['HasAttachment'] = $_hasAttachment;
		}
		$numOfEmail = count($emailAry);
			}}	
}//end gamma
else{
    $lc = new libcampusquota2007($UserID);
    if($thisFolder =='INBOX'){
		$folderID[0] = 2;
		$senderIDTitle = "SenderID";
	}else if($thisFolder =='Drafts'){
		$folderID[0] = 1;
		$senderIDTitle = "RecipientID";
	}else if($thisFolder =='Sent'){
		$folderID[0] = 0;
		$senderIDTitle = "RecipientID";
	}else if($thisFolder == 'Trash'){
        $folderID[0] = -1;
        $senderIDTitle = "SenderID";
	}else{
	    $sql = "select FolderID from INTRANET_CAMPUSMAIL_FOLDER where OwnerID='$uid' and foldername = '".$libDb->Get_Safe_Sql_Query($thisFolder)."'";
	    $senderIDTitle = "SenderID";
	    $folderID = $libDb->returnVector($sql);
	}
	if( $folderID[0] == -1){
	$sql  = "select CampusMailID,".$senderIDTitle.",SenderEmail,ExternalTo,Subject,DateInput,RecordStatus,IsAttachment from INTRANET_CAMPUSMAIL where UserID ='$uid' and Deleted= '1' order by DateInput";		
	}else{
	$sql  = "select CampusMailID,".$senderIDTitle.",SenderEmail,ExternalTo,Subject,DateInput,RecordStatus,IsAttachment from INTRANET_CAMPUSMAIL where UserID ='$uid' AND (Deleted != '1' or Deleted is null) and UserFolderID= '".$folderID[0]."' order by DateInput";		
	}
//	$sql  = "select CampusMailID,".$senderIDTitle.",SenderEmail,ExternalTo,Subject,DateInput,RecordStatus from INTRANET_CAMPUSMAIL where UserID ='$uid' and UserFolderID= '".$folderID[0]."'";
	
	$MailHeaderList = $lc->returnArray($sql);	
	$emailAryTotalCount = count($MailHeaderList);
	$startIndex = $emailAryTotalCount-1;
   if($emailAryTotalCount>20){
   	$endIndex = $emailAryTotalCount-20;
   }else{
   	$endIndex = 0;
   }
   
   	for($i= $startIndex ; $i>=$endIndex; $i--){
	$emailReturnList[] = $MailHeaderList[$i];
	}

	for($i= 0 ; $i<count($emailReturnList); $i++){
		$contentAry = $emailReturnList[$i];
        $subject = $contentAry['Subject']; 
        $date = $contentAry['DateInput']; 
        $currentUID = $contentAry['CampusMailID']; 
        $senderID = str_replace('U', '', $contentAry[$senderIDTitle]);
        $senderEmail = $contentAry['SenderEmail']; 
        $externalTo = $contentAry['ExternalTo'];
        $recordStatus = $contentAry['RecordStatus']; 
        $_unSeen = ($recordStatus=="")?true:false;
        $iscurrentAttachment = $contentAry['IsAttachment'];
        $_hasAttachment = ($iscurrentAttachment ==1)?true:false;
        
        if(($senderID!=null)&&($senderID!='')){
        	//for intranet receiver
        	if($parLang=='en'){
        		$nameLan = "EnglishName";
        	}else{
        		$nameLan = "ChineseName";
        	}
        $sql  = "select ".$nameLan." from INTRANET_USER where UserID ='$senderID'";  
        $SenderName = $lc->returnArray($sql);		
        $mailbox = $SenderName[0][$nameLan];
        }else{
			    if($thisFolder =='Sent'||$thisFolder =='Drafts'){
		        $pieces = explode(" ", $externalTo);
		    	}else{
			    $pieces = explode(" ", $senderEmail);
		    	}
		    	$mailbox =  str_replace(";","",$pieces[0]);
         }
      if($mailbox==''){
       	$mailbox = "No Recipients";
       }
   
	 	if ($subject != "") {
			$subjectPrint = (substr($subject, 0, 10) == "=?UTF-8?Q?") ? substr($subject, 10, -2) : $subject ;
		} else {
			$subjectPrint = "No subject";
		}

        $emailAry[$i]['Subject'] = $subjectPrint;
		$DateFormat = "Y-m-d";
		$emailAry[$i]['Date']='';
		if($date!=''){
	    $prettydate = date($DateFormat, strtotime(str_ireplace("UT","UTC",$date)));
        $emailAry[$i]['Date'] = $prettydate;	
		}
        $emailAry[$i]['Mailbox'] = $mailbox;
        $emailAry[$i]['UID'] = $currentUID;
        $emailAry[$i]['UnSeen'] = $_unSeen;
        $emailAry[$i]['HasAttachment'] = $_hasAttachment;
		}
		$numOfEmail = count($emailAry);
       }
       
 	$myTrashIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_trash_white_36px.png"."\"";
	$myComposeIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/icon_edit.png"."\"";
	$composeEmailUrl = "email_reply_forward_page.php?MailAction=Compose&TargetFolderName=".$thisFolder."&CurrentUID=".$currentUID."&token=".$token."&uid=".$uid."&ul=".$ul."&parLang=".$parLang."&AttachmentWorksOnCurrentVersion=".$AttachmentWorksOnCurrentVersion;		
	$myOutboxIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_outbox_white_36px.png"."\"";
	$myAttachmentIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/icon_attachment_20px.png"."\"";
	$refreshIconUrl =$linterface->Get_Ajax_Loading_Image($noLang=true);      
	
	$emailMainUrl = "email_main.php?token=".$token."&uid=".$uid."&ul=".$ul."&parLang=".$parLang."&AttachmentWorksOnCurrentVersion=".$AttachmentWorksOnCurrentVersion;
	if ($thisFolder == 'Drafts') {
	$EmailListSelectFormAction = "email_reply_forward_page.php?MailAction=Drafts&TargetFolderName=".$thisFolder."&token=".$token."&uid=".$uid."&ul=".$ul."&parLang=".$parLang."&AttachmentWorksOnCurrentVersion=".$AttachmentWorksOnCurrentVersion;
	}
	else {
		$EmailListSelectFormAction = "email_content.php?TargetFolderName=".$thisFolder."&token=".$token."&uid=".$uid."&ul=".$ul."&parLang=".$parLang."&AttachmentWorksOnCurrentVersion=".$AttachmentWorksOnCurrentVersion;
	}
	$deleteCheckBoxUrl = "email_delete_action_redirection.php?TargetFolderName=".$thisFolder."&token=".$token."&uid=".$uid."&password=".$password."&ul=".$ul."&parLang=".$parLang."&AttachmentWorksOnCurrentVersion=".$AttachmentWorksOnCurrentVersion."&IndividualEmail=0";
	
       
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" /> 
		<title>email list</title>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
		<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
		<script>
		window.JSON || 
		document.write('<script src="//cdnjs.cloudflare.com/ajax/libs/json3/3.2.4/json3.min.js"><\/scr'+'ipt>');
		</script>
		<script language="javascript">
		</script>
		<style type="text/css">
		</style>
	</head>
	<body>
		<div data-role="page" >			
		<!-- /header --> 		
	 <div data-role="header"  data-position="fixed" style ='background-color:#429DEA;' >
	 <div id='loadingmsg' style='display: none;'><br><?=$refreshIconUrl?><br></div>
     <div id='loadingover' style='display: none;'></div>
	 <a  href="#" data-icon="myapp-myOutbox" data-position-to="window" data-role="button" class = "ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon ui-btn-left" style="top: 5px;padding-bottom: 16px;padding-top: 16px;padding-left: 12px;padding-right: 20px;" onclick ="getEmailMainPage();"></a>
     <p  style="text-align:left; margin-left:30px;" class = "ui-li-text-my-title-font"><?php echo"&nbsp&nbsp".$_displayName?></p>
     
      <div class="ui-btn-right" data-role="controlgroup" data-type="horizontal">
	  <div id = "composeIconDiv" class = "show">
	  <!--<a href="#" id="search" data-role="button" class ="ui-nodisc-icon" data-icon="search" style="top: 15px;"></a>-->
      <a href="#"   data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline ui-btn-icon-left ui-btn-a ui-nodisc-icon" data-icon="myapp-compose" style="top: 5px;padding-bottom: 16px;padding-top: 16px;padding-left: 12.500;padding-left: 12.500;"  onclick ="getEmailComposePage();"></a>
      </div>
      <div id = "trashIconDiv" class = "hidden">
      <a href="#popupDeleteComfirm"  data-rel ="popup"  data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline ui-btn-icon-left ui-btn-a ui-nodisc-icon" data-icon="myapp-trash" style="top: 5px;padding-bottom: 16px;padding-top: 16px;padding-left: 12.500;padding-left: 12.500;"  data-transition="pop"></a>
      </div></div>
      </div><!-- /header -->  
   	<div data-role="content">
  <form  name = "delete_email" id = "deleteEmailCheckBoxForm" method="post" action=<?=$deleteCheckBoxUrl?>>	
  <fieldset>
  <div id="myEmailListChooseItems">
  <ul id="email_list" data-role="listview"  data-inset="false">
  <?php
       for($i=0;$i<$numOfEmail;$i++){
       	$_currentEmail = $emailAry[$i];
       	foreach((array)$_currentEmail as $_emailKey => $_emailCurrentDetail){
	       if($_emailKey=='Subject'){
	       	$__currentSubjext = $_emailCurrentDetail;
	       }else if($_emailKey=='Date'){
	       	$__currentDate = $_emailCurrentDetail;
	       }else if($_emailKey=='Mailbox'){
	       	$__currentSender = $_emailCurrentDetail;
	       }  else if($_emailKey=='UID'){
 	   	   	$__currentUID = $_emailCurrentDetail;
 	   	   }else if($_emailKey=='UnSeen'){
 	   	   	$__currentBGClass = "seen";
 	   	   	$__fontweight ="normal";
 	   	   	if($_emailCurrentDetail){
   			$__currentBGClass = "unSeen";
   			$__fontweight ="bold";
 	   	   	}
 	   	   }else if($_emailKey =='HasAttachment'){
 	   	   	$_isCurrentHasAttachmentLink = $_emailCurrentDetail;
 	   	   }
	       } 
	   if($__currentBGClass == "unSeen"){
 	   	   	$__currentSubjext = "<b>".$__currentSubjext."</b>";
 	   	   	$__currentDate = "<b>".$__currentDate."</b>";
 	   	   }   
	 echo "<li data-icon='false' id='".$__currentBGClass."'>";
     echo "<div class = 'ui-checkbox' style = 'margin-top: 0px;margin-bottom: 0px;' ><input type='checkbox' data-enhanced='true' name='emailListChoseEmailBox[]'  style='margin-top: 30px;' value = '".$__currentUID."' /></div>";
     echo "<a href='#' id = '".$__currentUID."'  onclick='getEmailDetails(this.id);' style='padding-left: 40px;background: transparent !important;font-weight: $__fontweight!important;'>".$__currentSender; 
	 if($_isCurrentHasAttachmentLink){
	  echo "<input type='image' id ='markEmailAttachment'  src=$myAttachmentIconUrl style='padding-left: 5px;'>";	
	 }
	 echo "<p class = 'ui-li-text-my-h5'>".$__currentSubjext."</b></p><p class='ui-li-aside ui-li-text-my-h5' style='right:1em;'>"
	 .$__currentDate."</p></a></li>";   
       }
?>	
 </ul>
 </div>
 </fieldset>
</form>
   <div id = "refreshIconDiv" class = "hidden" style="margin-top: 10px;margin-bottom: 20px;"><br><?=$refreshIconUrl?><br></div>
   <div data-role="popup" id="popupDeleteComfirm" data-overlay-theme="b" data-theme="b" data-dismissible="false" style="max-width:400px;">
    <div data-role="header" data-theme="a">
    <h1><?=$Lang['Gamma']['App']['DeleteEmailHeader']?></h1>
    </div>
    <div role="main" class="ui-content">
        <h3 class="ui-title"><?=$Lang['Gamma']['App']['DeleteEmailTitle']?></h3>
        <a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-btn-b" data-rel="back"><?=$Lang['Gamma']['App']['DeleteEmailCancel']?></a>
        <a href="#" id="comfirmDeleteEmails"  class="ui-btn  ui-corner-all ui-shadow ui-btn-inline ui-btn-b" ><?=$Lang['Gamma']['App']['DeleteEmailComfirm']?></a>
    </div>
</div>
	</div>
	<!-- /content -->
		
			<div data-role="footer" style = "
    border-bottom-width: 0px;
    border-top-width: 0px;
			">
			</div><!-- /footer -->
			<style type="text/css">
			 .ui-li-text-my-title-font{
			 	color:white;
			}
					.ui-li-text-my-h6{
				color:black;
				font-size: .75em !important;
			}
			 .ui-li-text-my-h5{
			 	color:black;
				font-size:.90em !important;
				overflow: hidden;              /* "overflow" value must be different from  visible"*/ 
				width: 100%;
				max-width: 100%;
				white-space: nowrap;
				text-overflow: ellipsis;
			}
			  .ui-li-text-my-h3{
			  	color:black;
				font-size:1em!important;
				font-weight:bold;
			}
    			.ui-icon-myapp-trash {
			 background-color: transparent !important;
			 border: 0 !important;
			 -webkit-box-shadow:none !important;
			 background: url(<?=$myTrashIconUrl?>) no-repeat !important;}
			 
			 .ui-icon-myapp-compose {
			 background-color: transparent !important;
			 border: 0 !important;
			 -webkit-box-shadow:none !important;
			 background: url(<?=$myComposeIconUrl?>) no-repeat !important;}
			 
			      .ui-icon-myapp-myOutbox{
			
			 border: 0 !important;
			 -webkit-box-shadow:none !important;
			 background: url(<?=$myOutboxIconUrl?>) no-repeat !important;
			}
            
			div.hidden{
				display:none ;
			}
			div.show{
				display:block ;
			}
			#refreshIconDiv{
		     position:absolute;
		     left:50%;
		     margin:10px 0 0 0;
		     background:tranparent!important;
		}

		#unSeen {
        background: white !important;
        font-weight:bold !important;
       }

		#seen {
	   background: #f6f6f6 !important; 
	   font-weight:normal !important;
		}
		
	 #loadingmsg {
      color: black;
      background:tranparent!important;
      padding: 10px;
      position: fixed;
      top: 50%;
      left: 50%;
      z-index: 100;
      margin-right: -25%;
      margin-bottom: -25%;
      }
      #loadingover {
      background: white;
      z-index: 99;
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
      filter: alpha(opacity=80);
      -moz-opacity: 0.8;
      -khtml-opacity: 0.8;
      opacity: 0.8;
    }
    .ui-normal-text{font-weight:normal;}​
    .ui-bold-text{font-weight: bold;}​
		</style>
		<script> 
		function getEmailDetails(inputId)
	  	{
	  		showLoading();
		  	var emaillink = "<?=$EmailListSelectFormAction."&CurrentUID="?>";
		  	emaillink = emaillink + inputId;
		  	window.location.assign(emaillink);
	  	}
		$(document).ready(function(){	
	
			$("#comfirmDeleteEmails").click(function(){
				$("#deleteEmailCheckBoxForm").submit();
				return false;
		  	});  
		  
		  	$(window).scroll(function(){
			  	$("#refreshIconDiv").show();  
			  	if($(window).scrollTop() == $(document).height() - $(window).height()){
					var emails = document.getElementsByName("emailListChoseEmailBox[]");
	            	var existEmailNumber = emails.length;
	            	var emailAryTotalCount = <?=$emailAryTotalCount?>;
	            	if(emailAryTotalCount>existEmailNumber){
	             		getExtraEmails(existEmailNumber,function(obj){
	            			$("#refreshIconDiv").hide();	            	  	
	            			var output = '';
	            			var currentEmails = document.getElementsByName("emailListChoseEmailBox[]");
	            			var currentExistEmailNumber = currentEmails.length;
	            			var currentStartIndex = obj.startIndex;
	            			<!--determine whether get the emails more than once, if already attach this email list then do nothing with the current return-->
	           				if(!(currentExistEmailNumber>currentStartIndex)){       
	            				for(var i = 0; i<obj.emailArr.length;i++){
						            var currentEmail = obj.emailArr[i];
						            var currentUID =  currentEmail.UID;
						            var Date = currentEmail.Date;
						            var Subject = currentEmail.Subject;
						            var Mailbox = currentEmail.Mailbox;
						            var UnSeen = currentEmail.UnSeen;
						            var IsCurrentHasAttachment = currentEmail.HasAttachment;
						            var currentBGClass = "seen";
						            if(UnSeen){currentBGClass = "unSeen";}
						            var attachmentLinkIcon = '';
						            if(IsCurrentHasAttachment){
						            	attachmentLinkIcon = '<input type="image" id ="markEmailAttachment"  src=<?=$myAttachmentIconUrl?> style="padding-left: 5px;">';
						            }
						            var currentCheckboxDiv = '<div class = "ui-checkbox" style = "margin-top: 0px;margin-bottom: 0px;"><input type="checkbox" data-enhanced="true" name="emailListChoseEmailBox[]"  style="margin-top: 30px;" value = "'+currentUID+'"/></div>';
						            var herfDiv = '<a href="#" id="'+currentUID+'"  onclick="getEmailDetails(this.id)" style="padding-left: 40px;background: transparent !important;">'+
						            Mailbox+attachmentLinkIcon+'<p class = "ui-li-text-my-h5">'+Subject+'</p><p class="ui-li-aside ui-li-text-my-h6" style="right:1em;">'+Date+'</p></a>';            
						            output += '<li data-icon="false" id="'+currentBGClass+'" >'+currentCheckboxDiv+herfDiv+'</li>';          
	            				}<!--end loop return array-->
								<!--append and refresh listview-->
								$('#email_list').append(output).listview('refresh');   
	            			}<!--end if get current list more than once-->            
	            		});<!--end get extra emails-->
	            	}<!--end whether need to retrieve more email-->
	            	else{
	            		$("#refreshIconDiv").hide();  	
	            	}
				}<!--end if bottom-->
       		});<!--end scroll-->
		});<!--end $(document).ready-->
	  
	  	$('#myEmailListChooseItems ul').children('li').on('click', function (e) {
				var counter = 0;
				 $("input[name='emailListChoseEmailBox[]']").each(function(){ 
				 	var checked_status = this.checked;
				 		if(checked_status){
				  		counter++;
				  		<!--show the delete button-->
				  		$("#composeIconDiv").hide();
		                $("#trashIconDiv").show();
				  	}	  	
				  });
				if(counter==0){
					<!--show send email counter-->
					  $("#composeIconDiv").show();
		              $("#trashIconDiv").hide();
				}
        });
          
       	function getExtraEmails(StartIndex,callback){     	
       		var uid = "<?=$uid?>";
       		var TargetFolderName = "<?=$thisFolder?>";
 	  		$.post("emailGenerator.php",{StartIndex:StartIndex,uid:uid,TargetFolderName:TargetFolderName },function(data) {
            })
	  		.done(function(data) {
				var obj = JSON.parse(data);	
			 	callback(obj);
		  	})
		  	.fail(function() {
		  		 // do nth
		  	});	
       	}  
       
       	function getEmailMainPage(){
       		showLoading();
			window.location.assign("<?=$emailMainUrl?>");
		}
	    function getEmailComposePage(){
       		showLoading();
			window.location.assign("<?=$composeEmailUrl?>");
		}	
	function showLoading() {
		    document.getElementById('loadingmsg').style.display = 'block';
		    document.getElementById('loadingover').style.display = 'block';
		}	  
	  
</script>
		</div><!-- /page -->
	</body>
</html>