<?php
$PATH_WRT_ROOT = '../../../';
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libpwm.php");

$uid = $_GET['uid'];
$_SESSION['UserID'] = $uid;
//intranet_auth();
intranet_opendb();

$libDb = new libdb(); 
$libeClassApp = new libeClassApp();
$libpwm= new libpwm();
$token = $_GET['token'];
$ul = $_GET['ul'];
$parLang = $_GET['parLang'];
$AttachmentWorksOnCurrentVersion = $_GET['AttachmentWorksOnCurrentVersion'];
$passwordAry = $libpwm->getData($uid);
foreach((array)$passwordAry as $_folderType => $_folderKey){
	if($_folderType==$uid)$password=$_folderKey;
}
$thisFolder= $_GET["TargetFolderName"];
$currentUID = $_GET["CurrentUID"];
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if (!$isTokenValid) {
	die("No access right to see this page.");
}

$folderAssoAry ="";
$defaultFolder = array();
$personalFolder = array();
$curUserId = $uid;
if ($curUserId == '') {
	$_SESSION['UserID'] = $uid;
	$UserID = $uid;
}
  
	$url="email_list.php?TargetFolderName=".$thisFolder."&token=".$token."&uid=".$uid."&ul=".$ul."&parLang=".$parLang."&AttachmentWorksOnCurrentVersion=".$AttachmentWorksOnCurrentVersion;	
	//imail gamma
	if ($plugin['imail_gamma']) {	
		$sql = "Select ImapUserEmail From INTRANET_USER Where UserID = '".$curUserId."'";
	$dataAry = $libDb->returnResultSet($sql);
	$iMapEmail = $dataAry[0]['ImapUserEmail'];
	$iMapPwd = $password;
    $IMap = new imap_gamma(false,$iMapEmail, $iMapPwd);
  if($thisFolder!="INBOX"){
    	$Folder = "INBOX.".$thisFolder;
    }else{
    	$Folder = $thisFolder;
    } 
    if(($thisFolder =='INBOX')||($thisFolder =='Sent')){
    	
    	$Result[] = $IMap->Move_To_Trash($Folder,$currentUID);
    }
    else if($thisFolder =='Drafts'){
    	$Result['DeleteMail'] = $IMap->deleteMail($IMap->DraftFolder, $currentUID);
    }
	 }//end imail gamma
	 //imail
	 else{
	 		 	
	 	if($thisFolder =='INBOX'){
	 	$folderID = '2';
	 		
	 	}else if($thisFolder =='Drafts'){
	 		$folderID = '1';
	 	}else if($thisFolder =='Sent'){
	 		$folderID = '0';
	 	}
	 	
	 	$sql = "delete from INTRANET_CAMPUSMAIL where UserID='$uid' AND CampusMailID = '$currentUID' AND UserFolderID='$folderID'";
		$libDb->db_db_query($sql);	
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
		<title>email list</title>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
		<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
		<script language="javascript">
		</script>
		<script>
		$(document).ready(function(){
		});
		</script>
	</head>
	<body>
	<div data-role="page">	
	<!-- /header -->   
	<META HTTP-EQUIV="REFRESH" CONTENT="0; URL=<?=$url?>"> 
  <!-- Home -->
</div>
<!-- /content -->
		<div data-role="footer">
		</div><!-- /footer -->
		</div><!-- /page -->
	</body>
</html>