<?php
$PATH_WRT_ROOT = '../../../';

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libpwm.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
$uid = $_GET['uid'];
if($_SESSION['UserID']==''){
$_SESSION['UserID'] = $uid;	
}
//intranet_auth();
intranet_opendb();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
		<title>email list</title>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
		<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
		<script language="javascript">
		</script>
	</head>
	<body>	
	<div data-role="page">	
<?

$linterface = new interface_html();
$libDb = new libdb(); 
$libeClassApp = new libeClassApp(); 
$libpwm= new libpwm();
$libfilesystem = new libfilesystem();
$folderAssoAry ="";
$defaultFolder = array();
$personalFolder = array();

$token = $_GET['token'];
$ul = $_GET['ul'];
$AttachmentWorksOnCurrentVersion = $_GET['AttachmentWorksOnCurrentVersion'];
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if (!$isTokenValid) {
	die("No access right to see this page.");
}
    // account and folder basic verification
    $ContentIMailOperation = $_GET["IMailOperation"];
    $ContentMailAction = $_GET['MailAction'];
    $thisFolder = $_GET["TargetFolderName"];
    $currentUID = $_GET["CurrentUID"];
    $parLang = $_GET['parLang'];
    //email info
    $Subject = stripslashes($_POST["Subject"]);
    $Body = stripslashes($_POST["emailMainBody"]);
    
    $To = $_POST["myReceiverFilter"];
    $PreToFilterArray = explode(";",$To);
    $Cc = $_POST["myCcFilter"]; 
    $PreCcFilterArray = explode(";",$Cc);
    $Bcc = $_POST["myBccFilter"];     
    $PreBccFilterArray = explode(";",$Bcc);
 
    $curUserId = $uid;
    if ($curUserId == '') {
	$UserID = $uid;
    }

//imail gamma case
if ($plugin['imail_gamma']) {	 	
	 $passwordAry = $libpwm->getData($uid);
	foreach((array)$passwordAry as $_folderType => $_folderKey){
		if($_folderType==$uid)$password=$_folderKey;
	}

    $sql = "Select ImapUserEmail From INTRANET_USER Where UserID = '".$curUserId."'";
	$dataAry = $libDb->returnResultSet($sql);
	$iMapEmail = $dataAry[0]['ImapUserEmail'];
	$iMapPwd = $password;	
	
    $IMap = new imap_gamma(false,$iMapEmail, $iMapPwd);
    $IMap->IMapCache = new imap_cache_agent($iMapEmail, $iMapPwd);
    
    $From = $IMap ->CurUserAddress;
    $uidAraycount = count($chosenFiles);

    $personal_path = "$file_path/file/gamma_mail/u$UserID";
    $filepathDir = $personal_path."/upload_file";
    
	for($i=0; $i<$uidAraycount;$i++)	{
		$currentAttachment = $chosenFiles[$i];
        $filepath = $filepathDir."/".$currentAttachment;  		 	
  		$currentAttachmentFilePath = $filepath;
		
		// find the , get the base64ed data. base64_decode(data).  $filepath = $personal_path."/".$myArray[1]; $libfilesystem->file_write(data, $filepath)// $libfilesystem->get_file_basename(filepath)			
		$myAttachmentFile[$i][1]  = $currentAttachment ;
	    $myAttachmentFile[$i][2]  =  $currentAttachmentFilePath;
	}    
	$ToArrayEmails = array(); // use for checking duplication
	$CcArrayEmails = array(); // use for checking duplication
	$BccArrayEmails = array(); // use for checking duplication
	$Preference = $IMap->Get_Preference($From);

	if($To != ''){
		
	foreach ($PreToFilterArray as $Key => $ToEmail) {
		$ToEmail = trim($ToEmail);
		if ($ToEmail != '') {
		     if(strstr($ToEmail,"<")&&strstr($ToEmail,">")){
				$EmailAddr = substr($ToEmail,strrpos($ToEmail,"<")+1,strrpos($ToEmail,">")-strrpos($ToEmail,"<")-1);
				$ToEmail = substr($ToEmail,0,strrpos($ToEmail,">")+1);
			}else{
				$EmailAddr = $ToEmail;
			}
			
		    if(!isset($ToArrayEmails[$EmailAddr])){ // set if not duplicated recipient
				$ToArrayEmails[$EmailAddr] = $ToEmail;
				   if (!stristr($ToEmail,'@')) {
					$EmptyEmail[] = str_replace('\"','',substr_replace(trim($ToEmail), '', stripos(trim($ToEmail),'<')));
				} else {
					$ToArray[] = $ToEmail;
				}
				$ReceiptEmailList[] = $EmailAddr;
		    }
		}	
	}}
	
	if($Cc != ''){
		
	foreach ($PreCcFilterArray as $Key => $CcEmail) {
		$CcEmail = trim($CcEmail);
		if ($CcEmail != '') {
		     if(strstr($CcEmail,"<")&&strstr($CcEmail,">")){
				$CcEmailAddr = substr($CcEmail,strrpos($CcEmail,"<")+1,strrpos($CcEmail,">")-strrpos($CcEmail,"<")-1);
				$CcEmail = substr($CcEmail,0,strrpos($CcEmail,">")+1);
			}else{
				$CcEmailAddr = $CcEmail;
			}
			
		    if(!isset($CcArrayEmails[$CcEmailAddr])){ // set if not duplicated recipient
				$CcArrayEmails[$CcEmailAddr] = $CcEmail;
				   if (!stristr($CcEmail,'@')) {
//					$EmptyEmail[] = str_replace('\"','',substr_replace(trim($CcEmail), '', stripos(trim($CcEmail),'<')));
				} else {
					$CCArray[] = $CcEmail;
				}
//				$ReceiptEmailList[] = $EmailAddr;
		    }
		}	
	}}
	
		if($Bcc!=''){
		foreach ($PreBccFilterArray as $Key => $BCCEmailValue) {
		$BCCEmailValue = trim($BCCEmailValue);
			if(strstr($BCCEmailValue,"<")&&strstr($BCCEmailValue,">")){
				$BCCEmailAddr = substr($BCCEmailValue,strrpos($BCCEmailValue,"<")+1,strrpos($BCCEmailValue,">")-strrpos($BCCEmailValue,"<")-1);
				$BCCEmailValue = substr($BCCEmailValue,0,strrpos($BCCEmailValue,">")+1);
			}else{
				$BCCEmailAddr = $BCCEmailValue;
			}
			  
		    if(!isset($BccArrayEmails[$BCCEmailAddr])){ // set if not duplicated recipient
				$BccArrayEmails[$BCCEmailAddr] = $BCCEmailValue;
				   if (stristr($BCCEmailValue,'@')) {
					$BCCArray[] = $BCCEmailValue;	
				}			            
		    }
		  }	
		}
	
	
	
	$XeClassMailID = "u$UserID-".time(); // global var used in Build_Mime_Text()
    $MaxNumOfRecipientPerTime = !isset($SYS_CONFIG['Mail']['MaxNumMailsPerBatch'])?200:$SYS_CONFIG['Mail']['MaxNumMailsPerBatch'];
    $SendMailBySplitInBatch = false;	
    
     if($userBrowser->platform=="Andriod"){
      $Body = str_replace(array("<br>", "<br />", "<BR>", "<BR />"),"\n",$Body);
      $Body = strip_tags($Body);
     }
    $MimeMessage = $IMap->Build_Mime_Text($Subject, $Body, $From, $ToArray, $CCArray, $BCCArray, $myAttachmentFile, "", true, $Preference, array());
     if($ContentIMailOperation=='Send'){
     	 
        $Result['SendMail'] = mail($MimeMessage['to'], $MimeMessage['subject'], $MimeMessage['body'], $MimeMessage['headers2'], "-f $From");
    
        if (!$Result['SendMail']) {
	    $Result['SaveDraft'] = $IMap->saveDraftCopy($MimeMessage['FullMimeMsg']);}
        else {
	    // save a copy in Sent folder
	    $Result['SaveSendCopy'] = $IMap->saveSentCopy($MimeMessage['FullMimeMsg']);
	        if (($MailAction == "Reply" || $MailAction == "ReplyAll") && $Folder != "") {	
		    $IMap->Set_Answered($Folder,$ActionUID);}
	         else if ($MailAction == "Forward" && $Folder != "") {	
		     $IMap->Set_Forwarded($Folder,$ActionUID);}
        }
        # delete the draft mail if is draft
        if($thisFolder=='Drafts'){
        	if($currentUID!=""){
        			if(!empty($_SESSION["UIDChange"])){
		            $key = array_search($currentUID, $_SESSION["UIDChange"]);
					if($key!== false)
						unset($_SESSION["UIDChange"][$key]);
				     }
	              
	              $Result['DeleteMail'] = $IMap->deleteMail($IMap->DraftFolder, $currentUID);  	
	              //delete this email from drafts, and then jump to draft list
                  //$url="email_list3.php?TargetFolderName=".$thisFolder."&CurrentUID=".$currentUID."&token=".$token."&uid=".$uid."&ul=".$ul;
        	}
        }
     }
     else{   
     	//save the composed email to draft
     	$Result['SaveDraft'] = $IMap->saveDraftCopy($MimeMessage['FullMimeMsg']);
     }
	  $thisFolder = urlencode($thisFolder);//encode thisFolder for url use handle chinese folder case
	 }//end gamma
	
	//imail case
	 else{	 	
	include_once($PATH_WRT_ROOT."includes/libwebmail.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
	$lwebmail = new libwebmail();
	$li = new libcampusmail();
	
	//internal accounts
	$chosenRecipientIDs = $_POST["chosenRecipientIDs"];	
    $chosenInternalCCIDs = $_POST["chosenInternalCCIDs"];
    $chosenInternalBCCIDs = $_POST["chosenInternalBCCIDs"];	
	//external addresses
	$PreToArray = array();$PreCcArray= array();$PreBccArray=array();
    $to_address_list = $_POST["chosenExternalRecipientAddresses"];
    $PreToArray = explode(";",$to_address_list);
    $cc_address_list = $_POST["chosenExternalCcAddresses"]; 
    $PreCcArray = explode(";",$cc_address_list);
    $bcc_address_list = $_POST["chosenExternalBCCAddresses"]; 
    $PreBccArray = explode(";",$bcc_address_list);
    
    $chosenTo = $_POST["chosenFromEmailAddressListGenerator"];
    $chosenPreToFilterArray = explode(";",$chosenTo);
    $chosenCc = $_POST["chosenCCFromEmailAddressListGenerator"]; 
    $chosenPreCcFilterArray = explode(";",$chosenCc);
    $chosenBcc = $_POST["chosenBCCFromEmailAddressListGenerator"]; 
    $chosenPreBccFilterArray = explode(";",$chosenBcc);
    
	$extraInputEmailToAddress = array_diff($PreToFilterArray, $chosenPreToFilterArray);
    $extraInputEmailCcAddress = array_diff($PreCcFilterArray, $chosenPreCcFilterArray);
    $extraInputEmailBccAddress = array_diff($PreBccFilterArray,$chosenPreBccFilterArray);
    //end get get extra input address   
    //regard extra email as external address and add to external array
    
    $PreToArray = array_merge($extraInputEmailToAddress, $PreToArray);
    $PreCcArray = array_merge($extraInputEmailCcAddress, $PreCcArray);
    $PreBccArray = array_merge($extraInputEmailBccAddress, $PreBccArray); 
    
    //generate the sql insert part for external to, cc and bcc rows
    if(count($extraInputEmailToAddress)>0){
     foreach ($extraInputEmailToAddress as $Key => $ToExtraEmail) {
     	$to_address_list = $to_address_list.$ToExtraEmail.";";
     }	
    }
    if(count($extraInputEmailCcAddress)>0){
       foreach ($extraInputEmailCcAddress as $Key => $CcExtraEmail) {
     	$cc_address_list = $cc_address_list.$CcExtraEmail.";";
     }
    }
    if(count($extraInputEmailBccAddress)>0){
       foreach ($extraInputEmailBccAddress as $Key => $BccExtraEmail) {
     	$bcc_address_list = $bcc_address_list.$BccExtraEmail.";";
     }	
    }
	 	
    //get To internal user IDs  $chosenRecipientIDArr
    $chosenRecipientIDs_All = array_filter(explode(";",$chosenRecipientIDs));
    $chosenInternalCCIDs_All = array_filter(explode(";",$chosenInternalCCIDs));
    $chosenInternalBCCIDs_All = array_filter(explode(";",$chosenInternalBCCIDs));
    //internal save
    $recipientIDMarkArray = "'";
    $InternalCCMarkArray = "'";
    $InternalBCCMarkArray ="'";
    $chosenRecipientIDs_AllCounter = count($chosenRecipientIDs_All);
    $chosenInternalCCIDs_AllCounter = count($chosenInternalCCIDs_All);
    $chosenInternalBCCIDs_AllCounter = count($chosenInternalBCCIDs_All);
    if($chosenRecipientIDs_AllCounter>0){
        for($j= 0;$j<$chosenRecipientIDs_AllCounter-1;$j++){
    	$recipientIDMarkArray =$recipientIDMarkArray."U".$chosenRecipientIDs_All[$j].",";
         }  
        $recipientIDMarkArray =$recipientIDMarkArray."U".$chosenRecipientIDs_All[$chosenRecipientIDs_AllCounter-1]."'";
    }else{
    	$recipientIDMarkArray=$recipientIDMarkArray."'";
    }
    
    if($chosenInternalCCIDs_AllCounter>0){
    	   for($j= 0;$j<$chosenInternalCCIDs_AllCounter-1;$j++){
    	$InternalCCMarkArray =$InternalCCMarkArray."U".$chosenInternalCCIDs_All[$j].",";
         }  
        $InternalCCMarkArray =$InternalCCMarkArray."U".$chosenInternalCCIDs_All[$chosenInternalCCIDs_AllCounter-1]."'";
    }else{
    	$InternalCCMarkArray=$InternalCCMarkArray."'";
    }
     if($chosenInternalBCCIDs_AllCounter>0){    	
      for($j= 0;$j<$chosenInternalBCCIDs_AllCounter-1;$j++){
    	$InternalBCCMarkArray =$InternalBCCMarkArray."U".$chosenInternalBCCIDs_All[$j].",";
         }  
        $InternalBCCMarkArray =$InternalBCCMarkArray."U".$chosenInternalBCCIDs_All[$chosenInternalBCCIDs_AllCounter-1]."'";
     }else{
     	$InternalBCCMarkArray=$InternalBCCMarkArray."'";
     }
      
	$ToArrayEmails = array(); // used for checking duplication
	$CcArrayEmails = array(); // used for checking duplication
	$BccArrayEmails = array(); // used for checking duplication
	//final sended  external email address array
	$ToArray = array();
	$CCArray = array();
	$BCCArray = array();
    //get To external user address list array $ToArray
	if(count($PreToArray) >0){
	foreach ($PreToArray as $Key => $ToEmail) {
		$ToEmail = trim($ToEmail);
		if ($ToEmail != '') {
		     if(strstr($ToEmail,"<")&&strstr($ToEmail,">")){
				$EmailAddr = substr($ToEmail,strrpos($ToEmail,"<")+1,strrpos($ToEmail,">")-strrpos($ToEmail,"<")-1);
				$ToEmail = substr($ToEmail,0,strrpos($ToEmail,">")+1);
			}else{
				$EmailAddr = $ToEmail;
			}
		    if(!isset($ToArrayEmails[$EmailAddr])){ // set if not duplicated recipient
				$ToArrayEmails[$EmailAddr] = $ToEmail;
				   if (!stristr($ToEmail,'@')) {
					$EmptyEmail[] = str_replace('\"','',substr_replace(trim($ToEmail), '', stripos(trim($ToEmail),'<')));
				} else {
					$ToArray[] = $ToEmail;					
				}				            
		    }
		}	
	}
	}
	
	if(count($PreCcArray)>0){
		foreach ($PreCcArray as $Key => $CcEmail) {
		$CcEmail = trim($CcEmail);
		if ($CcEmail != '') {
		     if(strstr($CcEmail,"<")&&strstr($CcEmail,">")){
				$CcEmailAddr = substr($CcEmail,strrpos($CcEmail,"<")+1,strrpos($CcEmail,">")-strrpos($CcEmail,"<")-1);
				$CcEmail = substr($CcEmail,0,strrpos($CcEmail,">")+1);
			}else{
				$CcEmailAddr = $CcEmail;
			}
			
		    if(!isset($CcArrayEmails[$CcEmailAddr])){ // set if not duplicated recipient
				$CcArrayEmails[$CcEmailAddr] = $CcEmail;
				   if (stristr($CcEmail,'@')) {
				   	$CCArray[] = $CcEmail;
				}
		    }
		}		
	}
	
	if(count($PreBccArray)>0){
		foreach ($PreBccArray as $Key => $BCCEmailValue) {
		$BCCEmailValue = trim($BCCEmailValue);
			if(strstr($BCCEmailValue,"<")&&strstr($BCCEmailValue,">")){
				$BCCEmailAddr = substr($BCCEmailValue,strrpos($BCCEmailValue,"<")+1,strrpos($BCCEmailValue,">")-strrpos($BCCEmailValue,"<")-1);
				$BCCEmailValue = substr($BCCEmailValue,0,strrpos($BCCEmailValue,">")+1);
			}else{
				$BCCEmailAddr = $BCCEmailValue;
			}
			  
		    if(!isset($BccArrayEmails[$BCCEmailAddr])){ // set if not duplicated recipient
				$BccArrayEmails[$BCCEmailAddr] = $BCCEmailValue;
				   if (stristr($BCCEmailValue,'@')) {
					$BCCArray[] = $BCCEmailValue;	
				}			            
		    }
		  }	
		}		
	}
	// set attachment
	$uidAraycount = count($chosenFiles);
//	$personal_path = "$file_path/file/mail/u$UserID";
    $chosenUploadedAttachmentLink = $_POST['chosenUploadedAttachmentLink'];
    if($chosenUploadedAttachmentLink!=""){
     $filepathDir = "u".$uid."/".$chosenUploadedAttachmentLink;	
      $attachmentFullPath = $intranet_root."/file/mail/".$filepathDir;
    }else{
     $filepathDir = "";	
     $attachmentFullPath ="";
    }
    $AttachmentNameArray = $libfilesystem->return_files($attachmentFullPath);
    if($uidAraycount>0){
    	$IsAttachment = 1;
    }else{
    	$IsAttachment = 0;
    }	
	
	//for send external emails info
	$email_subj = stripslashes($Subject);
    $email_msg = stripslashes($Body);
    $toSend = array_merge($ToArray,$CCArray);
    
    $getSenderSql = "select EnglishName,UserEmail from INTRANET_USER where  UserID='".$uid."'";
    $senderInfo = $li->returnArray($getSenderSql,2);
    $sender_name = $senderInfo[0][0];
    $email_return_path = $senderInfo[0][1];
    if($sender_name!=''){ 
    	$emailheader_from = "\"".$sender_name."\" <".$email_return_path.">";}
	   else{
	   	$emailheader_from = $email_return_path;
	   }	   
     if($ContentIMailOperation=='Send'){
     	 //step 1:send the external composed email
          $exmail_success = false;
     	  $exmail_success = $lwebmail->SendMail($email_subj,$email_msg,$emailheader_from,$ToArray,$CCArray,$BCCArray,$attachmentFullPath,'',$email_return_path,$email_return_path,null,false); 
       
		  $receiverTotal =array_unique(array_merge($chosenRecipientIDs_All,$chosenInternalCCIDs_All,$chosenInternalBCCIDs_All));     
       	       
        //step 2:save send mail for current account
        if($exmail_success||(count($receiverTotal)>0)){
        //save email copy in send folder (0)
       	  $sql = "
		 INSERT INTO INTRANET_CAMPUSMAIL (
		      UserID, UserFolderID,SenderID,SenderEmail, RecipientID,InternalCC,InternalBCC,
		      ExternalTo,ExternalCC,ExternalBCC,
		      Subject,Message,Attachment,IsAttachment,MailType,DateInput,DateModified,DateInFolder)
         VALUES (
          '$uid','0', '$uid', '$email_return_path',$recipientIDMarkArray,$InternalCCMarkArray,$InternalBCCMarkArray,
          '$to_address_list','$cc_address_list','$bcc_address_list','$email_subj','$email_msg','$filepathDir','$IsAttachment','1',now(),now(),now())";
           $success = $libDb->db_db_query($sql);	
           if ($success) {
           	$CampusMailID = $libDb->db_insert_id();
           }
           if($CampusMailID!=""&&$AttachmentNameArray!=NULL&&count($AttachmentNameArray)>0){
      //record attachment information	
      foreach( $AttachmentNameArray as $value ) {
      	//to be improved later to store the rows at once
         $currentAttachmentFileName = $libfilesystem->get_file_basename($value);
         $insertAttachmentSql = "INSERT INTO INTRANET_IMAIL_ATTACHMENT_PART (CampusMailID,AttachmentPath, FileName)
         VALUES ('$CampusMailID','$filepathDir','$currentAttachmentFileName')";
         $libDb->db_db_query($insertAttachmentSql);	    
       }}
           
        //step 3:insert every internal email sent
        for($i=0;$i<count($receiverTotal);$i++){
        $currentReceiverUserID = $receiverTotal[$i];        
        
        //save current email in each receiver's inbox foler(2)
        $sql = "
		 INSERT INTO INTRANET_CAMPUSMAIL (
		      CampusMailFromID,UserID, UserFolderID,SenderID,SenderEmail,RecipientID,InternalCC,InternalBCC,
		      ExternalTo,ExternalCC,ExternalBCC,
		      Subject,Message,Attachment,IsAttachment,MailType,DateInput,DateModified,DateInFolder)
        VALUES (
          '$CampusMailID','$currentReceiverUserID', '2','$uid','$email_return_path', $recipientIDMarkArray,$InternalCCMarkArray,$InternalBCCMarkArray,
          '$to_address_list','$cc_address_list','$bcc_address_list','$email_subj','$email_msg','$filepathDir','$IsAttachment','1',now(),now(),now())";
         $libDb->db_db_query($sql);	   
        }       
        //setp 4 delete current email if it exist in draft folder
        if($thisFolder=='Drafts'){
        	if($currentUID!=""){
        		$sql = "delete from INTRANET_CAMPUSMAIL where UserID='".$uid."' AND CampusMailID = '".$currentUID."' AND UserFolderID=1";
        			$CampusMailID =$libDb->db_db_query($sql);	
        	}}//end delete draft
        
        }else{
        //save email copy as a draft(1)       
         $sql = "
		 INSERT INTO INTRANET_CAMPUSMAIL (
		      UserID, UserFolderID,SenderID,SenderEmail,RecipientID,InternalCC,InternalBCC,
		      ExternalTo,ExternalCC,ExternalBCC,
		      Subject,Message,Attachment,IsAttachment,MailType,DateInput,DateModified,DateInFolder)
        VALUES (
          '$uid', '1','$uid','$email_return_path', $recipientIDMarkArray,$InternalCCMarkArray,$InternalBCCMarkArray,
          '$to_address_list','$cc_address_list','$bcc_address_list','$email_subj','$email_msg','$filepathDir','$IsAttachment','1',now(),now(),now())";
           $libDb->db_db_query($sql);	
        }     
     }
     else{   
     	//save the composed email to draft
     	 $sql = "
		 INSERT INTO INTRANET_CAMPUSMAIL (
		      UserID, UserFolderID,SenderID,SenderEmail, RecipientID,InternalCC,InternalBCC,
		      ExternalTo,ExternalCC,ExternalBCC,
		      Subject,Message,Attachment,IsAttachment,MailType,DateInput,DateModified,DateInFolder)
        VALUES (
          '$uid', '1','$uid','$email_return_path', $recipientIDMarkArray,$InternalCCMarkArray,$InternalBCCMarkArray,
          '$to_address_list','$cc_address_list','$bcc_address_list','$email_subj','$email_msg','$filepathDir','$IsAttachment','1',now(),now(),now())";
          $libDb->db_db_query($sql);		
     }	 	
	 }	
	//jump link set
    $urlPreLink = '';
	if($ContentMailAction=='Compose' || $ContentMailAction=='Drafts'){
	$urlPreLink = "email_list.php";
	}else{
		$urlPreLink = "email_content.php";
	}
	$url=$urlPreLink."?TargetFolderName=".$thisFolder."&CurrentUID=".$currentUID."&token=".$token."&uid=".$uid."&ul=".$ul."&parLang=".$parLang."&AttachmentWorksOnCurrentVersion=".$AttachmentWorksOnCurrentVersion;
	 
    intranet_closedb();	
?>
	
	<header data-role ="header" style ="background-color:#3a960e;">
    </header>
    <!-- /header -->  
    <META HTTP-EQUIV="REFRESH" CONTENT="2; URL=<?=$url?>"> 
   </div><!-- /Home -->
	<div data-role="footer">
	</div><!-- /footer -->
	<style type="text/css">
	
	</style>
	<script> 
	</script>
	</div><!-- /page -->
	</body>
    </html>