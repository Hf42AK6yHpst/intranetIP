<?php
$PATH_WRT_ROOT = '../../../';

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libpwm.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");



intranet_opendb();
$libfilesystem = new libfilesystem();
$attachmentAction = $_REQUEST['attachmentAction'];
$UserID = $_REQUEST['userID'];

if($attachmentAction=="DELETE"){
	$currentName = $_REQUEST['currentID'];
	
	if ($plugin['imail_gamma']) {
		$deleteFullPath = "$file_path/file/gamma_mail/u$UserID"."/upload_file/".$currentName;
		$deleteSucess = $libfilesystem->lfs_remove($deleteFullPath);
		echo($deleteSucess);
	}else{
		$imailAttachmentPath = $_REQUEST['currentEmailAttachmentLink'];
		if($imailAttachmentPath!="N"){
		$deleteFullPath = "$file_path/file/mail/". "u$UserID/".$imailAttachmentPath."/".$currentName;	
		$deleteSucess = $libfilesystem->lfs_remove($deleteFullPath);
		echo($deleteFullPath." ".$deleteSucess);
		}
	}	
	
}else if($attachmentAction=="COPY"){
	$originalEmailAttachmentFullPath = "$file_path/file/mail/".$_REQUEST['originalEmailAttachmentLink'];
	//only imail now
	$imailAttachmentPathGenerator = session_id().".".time();
	$newDestinationFolderFullPath = "$file_path/file/mail/". "u$UserID/".$imailAttachmentPathGenerator;
	
	   if (!file_exists($newDestinationFolderFullPath))
	  {
	  	 mkdir($newDestinationFolderFullPath, 0777, true);
	  }

      $row = $libfilesystem->return_folderlist($originalEmailAttachmentFullPath);
       for($i=sizeof($row)-1; $i>=0; $i--){
       	  $file = $row[$i];
       	  if($file=="." || $file=="..") continue;
          $libfilesystem->item_copy($file, $newDestinationFolderFullPath);
       }

	$currentAttachmentFilePath = $imailAttachmentPathGenerator;
	echo $currentAttachmentFilePath;
	
}else{
	$currentEncodedFile = $_REQUEST['Data'];
    $name = $_REQUEST['ImageName'];
    $currentEmailAttachmentLink = $_REQUEST['currentEmailAttachmentLink'];

if ($plugin['imail_gamma']) {
 $filepathDir = "$file_path/file/gamma_mail/u$UserID"."/upload_file";
}else{
	if($currentEmailAttachmentLink=="N"){
	 $imailAttachmentPathGenerator = session_id().".".time();	
	}else{
	$imailAttachmentPathGenerator=$currentEmailAttachmentLink;
	}
 $filepathDir = "$file_path/file/mail/". "u$UserID/".$imailAttachmentPathGenerator;
}
 
   if (!file_exists($filepathDir))
	  {
	  	 mkdir($filepathDir, 0777, true);
	  }
		  
	if (($pos = strpos($currentEncodedFile, ",")) !== FALSE) { 
     $currentEncodedFile = substr($currentEncodedFile, $pos+1); 
     }else{
     	echo '';
     }	  
   $currentFileData = base64_decode($currentEncodedFile);  
   $filepath = $filepathDir."/".$name;
   $fileWriteSuccess = $libfilesystem->file_write($currentFileData, $filepath);
   	if($fileWriteSuccess == 1){
   		if ($plugin['imail_gamma']) {
  			$currentAttachmentFilePath = $filepath;
  			}else{
  			$currentAttachmentFilePath = $imailAttachmentPathGenerator;
  			}
  		}
  		else{
  			$currentAttachmentFilePath='';
  		}

echo $currentAttachmentFilePath;
	
	
}
?>