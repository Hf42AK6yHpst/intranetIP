<?php
$PATH_WRT_ROOT = '../../../';

$parLang = $_GET['parLang'];

switch (strtolower($parLang)) {
case 'en':
$intranet_hardcode_lang = 'en';
break;
default:
$intranet_hardcode_lang = 'b5';
}

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpwm.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$libDb = new libdb(); 
$libeClassApp = new libeClassApp();
$libpwm= new libpwm();
$linterface = new interface_html();

$folderAssoAry ="";
$defaultFolder = array();
$personalFolder = array();

$token = $_GET['token'];
$uid = $_GET['uid'];
$_SESSION['UserID'] = $uid;
$ul = $_GET['ul'];
$AttachmentWorksOnCurrentVersion = $_GET['AttachmentWorksOnCurrentVersion'];
$passwordAry = $libpwm->getData($uid);
foreach((array)$passwordAry as $_folderType => $_folderKey){
	if($_folderType==$uid)$password=$_folderKey;
}

$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if (!$isTokenValid) {
	die("No access right to see this page.");
}

$curUserId = $uid;
if ($curUserId == '') {
	$_SESSION['UserID'] = $curUserId;
	$UserID = $curUserId;
}
	$defaultFolderName =  $Lang['Gamma']['DefaulFolder'];
	$personalFolderName =  $Lang['Gamma']['PersonalFolder'];
	$_url =  "email_list.php?token=".$token."&uid=".$uid."&ul=".$ul."&AttachmentWorksOnCurrentVersion=".$AttachmentWorksOnCurrentVersion."&parLang=".$parLang."&TargetFolderName=";
	$refreshIconUrl =$linterface->Get_Ajax_Loading_Image($noLang=true);
	
if ($plugin['imail_gamma']) {
	
	$sql = "Select ImapUserEmail From INTRANET_USER Where UserID = '".$curUserId."'";
	$dataAry = $libDb->returnResultSet($sql);
	$iMapEmail = $dataAry[0]['ImapUserEmail'];
	$iMapPwd = $password;
	
	// using imail plus
	include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_hardcode_lang.php");
	$IMap = new imap_gamma($skipAutoLogin = false, $iMapEmail, $iMapPwd);
	$folderAry = $IMap->getMailFolders();
	$numOfFolder = count($folderAry);	
	$folderKeyAry = array();
		foreach ($folderAry as $key => $value) {
         $folderKeyAry[]=$key;
         }

	//get folder's name without the prefix INBOX. 
	for ($i=0; $i<$numOfFolder; $i++) {
		$_folderName = $folderAry[$folderKeyAry[$i]];		
		$_folderNamePieces = explode('.', $_folderName);
		$_numOfPieces = count($_folderNamePieces);
		if ($_numOfPieces == 1) {
			$_targetFolderName = 'INBOX';
		}
		else {
			$_targetFolderName = $_folderNamePieces[1];
		}
		
	   $_targetFolderName = IMap_Decode_Folder_Name($_targetFolderName);
		
		//get the folder's name according to the language setting
		$_displayName = $Lang['Gamma']['SystemFolderName'][$_targetFolderName];
		
		$_imagePath = $image_path."/".$LAYOUT_SKIN."/iMail/app_view/";

			if($_targetFolderName == 'INBOX'){
			$_imagePath .="icon_inbox.png";
						
			}
			else if($_targetFolderName == 'Drafts'){
				$_imagePath .="icon_draft.png";
			}
			else if($_targetFolderName == 'Junk'){
				$_imagePath .="icon_spam.png";
			}
			else if($_targetFolderName == 'Sent'){
				$_imagePath .="icon_outbox.png";
			}
			else if($_targetFolderName == 'Trash'){
				$_imagePath .="icon_trash.png";
			}
			
		//set the folder type and classify them with two types
		if (in_array($_targetFolderName, (array)$SYS_CONFIG['Mail']['SystemMailBox'])) {
			$_folderType = 'default';
		}
		else {
			$_folderType = 'personal';
		}

		//the personal folder's display name is empty 
		if($_displayName!=''){
			$consolidatedFolderAry[$_folderType][$i]['displayName'] = $_displayName;
		}
		else{
			if($_folderType == 'personal'){
				//the personal folder's display name is as the original name 
				$consolidatedFolderAry[$_folderType][$i]['displayName'] = $_targetFolderName;
				$_imagePath .="icon_folder.png";
			}
		}		
		$consolidatedFolderAry[$_folderType][$i]['image'] = $_imagePath;
		$consolidatedFolderAry[$_folderType][$i]['TargetFolderName'] = urlencode($IMap->encodeFolderName($_targetFolderName));//$_targetFolderName;	
		
	}
}// end gamma
else {
	
	
	//FolderID: 0 is Outbox, 1 is Draft, 2 is Inbox
	//other FolderID is user created folders
	$sql = "SELECT FolderID, FolderName FROM INTRANET_CAMPUSMAIL_FOLDER WHERE OwnerID = '".$curUserId."' OR RecordType = 0 ORDER BY  FolderID DESC";
	$folderAry = BuildMultiKeyAssoc($libDb->returnResultSet($sql), 'FolderID', $IncludedDBField=array('FolderName'), $SingleValue=1);
	$folderAry['-1'] = 'Trash';
	$numOfFolder = count($folderAry);	
		//get folder's name without the prefix INBOX. 
		$folderKeyAry = array();
		foreach ($folderAry as $key => $value) {
         $folderKeyAry[]=$key;
         } 
	for ($i=0; $i<$numOfFolder; $i++) {
		$_targetFolderName = $folderAry[$folderKeyAry[$i]];
		if($_targetFolderName =='Inbox'){
			$_targetFolderName = "INBOX";
		}
		else if($_targetFolderName =='Draft'){
			$_targetFolderName = "Drafts";
		}
		else if($_targetFolderName =='Outbox'){
			$_targetFolderName = "Sent";
		}else if($_targetFolderName =='Trash'){
			$_targetFolderName = "Trash";
		}
		
		$_displayName = $Lang['Gamma']['SystemFolderName'][$_targetFolderName];
		$_imagePath = $image_path."/".$LAYOUT_SKIN."/iMail/app_view/";
		$_folderType ='personal';
			if($_targetFolderName == 'INBOX'){
			   $_imagePath .="icon_inbox.png";
			   $_folderType = 'default';
			}
			else if($_targetFolderName == 'Drafts'){
				$_imagePath .="icon_draft.png";
				$_folderType = 'default';
			}
			else if($_targetFolderName == 'Junk'){
				$_imagePath .="icon_spam.png";
				$_folderType = 'default';
			}
			else if($_targetFolderName == 'Sent'){
				$_imagePath .="icon_outbox.png";
				$_folderType = 'default';
			}
			else if($_targetFolderName == 'Trash'){
				$_imagePath .="icon_trash.png";
				$_folderType = 'default';
			}
		if($_displayName!=''){
			$consolidatedFolderAry[$_folderType][$i]['displayName'] = $_displayName;
		}
		else{
			if($_folderType == 'personal'){
				$consolidatedFolderAry[$_folderType][$i]['displayName'] = $_targetFolderName;
				$_imagePath .="icon_folder.png";
			}
		}	
				
	    $consolidatedFolderAry[$_folderType][$i]['image'] = $_imagePath;
		$consolidatedFolderAry[$_folderType][$i]['TargetFolderName'] = $_targetFolderName;		
	}				
}
 $defaultFolderAry = array();
 $personalFolderAry = array();
 foreach((array)$consolidatedFolderAry as $_folderType => $_folderKey){
	$__folderType = $_folderType;
	if($__folderType =='default') {
		$defaultFolderAry=$_folderKey;
	}
	else{
		$personalFolderAry=$_folderKey;
	}
 }
$personalFolderAryCounter = count($personalFolderAry);


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" /> 
		<title>email list</title>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
		
		<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
	
	</head>
	<body>
<!-- /header -->            
<!-- Home -->
<div data-role="page" id="page1">
 <div id='loadingmsg' style='display: none;'><br><?=$refreshIconUrl?><br></div>
 <div id='loadingover' style='display: none;'></div>
    <div data-role="content">
        <?php
       echo"  <ul data-role='listview' id='defalt_email_list' data-inset='false' data-split-icon='plus' style = 'margin-bottom: 0px;'>
            <li data-theme=''>
            <h1>".$defaultFolderName."</h1>
            </li>";
          foreach((array)$defaultFolderAry as $_folderType => $_folderKey){
	      $_defalutFolderDetailAry = $_folderKey;
	      $__displayName = '';
	      $__TargetFolderName= '';
	      $__imagePath = '';
	       foreach((array)$_defalutFolderDetailAry as $_folderType => $_folderKey){
	       if($_folderType=='displayName'){
	       	$__displayName = $_folderKey;
	       }else if($_folderType=='TargetFolderName'){
	       	$__TargetFolderName = $_folderKey;
	       }else{
	       	$__imagePath = $_folderKey;
	       }
	       }
	       if($__displayName!=''){
	       	 echo "<li data-theme='' data-icon='false'><a href='#' id = '".$__TargetFolderName."' onclick = 'getFolderEmailList(this.id);'>
			<img src=".$__imagePath.">
			<h1>".$__displayName."</h1></a>
			</li>";
	       }
			}
     if($personalFolderAryCounter>0){
       echo "</ul>";
       
           echo"  <ul data-role='listview' id='personal_email_list' data-inset='false' data-split-icon='plus' style = 'margin-top: 0px;'>
                    <li data-theme='' style ='background-color:#f1f1f1; '>
               <h1 >".$personalFolderName."</h1>
            </li>";    
            
           foreach((array) $personalFolderAry as $_folderType => $_folderKey){
	       $_defalutFolderDetailAry = $_folderKey;
	       $__displayName = '';
	       $__TargetFolderName= '';
	       $__imagePath = '';
	       foreach((array)$_defalutFolderDetailAry as $_folderType => $_folderKey){
	       if($_folderType=='displayName'){
	       	$__displayName = $_folderKey;
	       }else if($_folderType=='TargetFolderName'){
	       	$__TargetFolderName = $_folderKey;
	       }else{
	       	$__imagePath = $_folderKey;
	       }
	       }
	       		 echo "<li data-theme='' data-icon='false'>
			            <a href='#' id = '".$__TargetFolderName."' onclick = 'getFolderEmailList(this.id);'>
			<img src=".$__imagePath.">
			<h2>".$__displayName."</h2></a>
			</li>";
			}
               echo "</ul>"; }
		?>
    </div>
</div>
<!-- /content -->

		
			<div data-role="footer">
			</div><!-- /footer -->
	<style type="text/css">		
	.ui-li-text-my-h6{
    	color:black;
    	font-size: .75em !important;
    }
 .ui-li-text-my-h5{
     	color:black;
    	font-size:.90em !important;
    }
  .ui-li-text-my-h3{
      	color:black;
    	font-size:1em!important;
    	font-weight:bold;
    }
     #loadingmsg {
      color: black;
      background:tranparent!important;
      padding: 10px;
      position: fixed;
      top: 50%;
      left: 50%;
      z-index: 100;
      margin-right: -25%;
      margin-bottom: -25%;
      }
      #loadingover {
      background: white;
      z-index: 99;
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
      filter: alpha(opacity=80);
      -moz-opacity: 0.8;
      -khtml-opacity: 0.8;
      opacity: 0.8;
    }
    </style>

    <script>
  $( document ).on( "mobileinit" , function () {
    $.mobile.toolbar.prototype.options.addBackBtn = true;
  }); 
  
  	  function getFolderEmailList(inputId)
	  {
	  	showLoading();
	    var emaillink = "<?=$_url?>";
	    emaillink = emaillink + inputId;
	  	window.location.assign(emaillink);
	  }
		function showLoading() {
		    document.getElementById('loadingmsg').style.display = 'block';
		    document.getElementById('loadingover').style.display = 'block';
		}	  
	  
	  
</script>
    
		</div><!-- /page -->
	</body>
</html>
