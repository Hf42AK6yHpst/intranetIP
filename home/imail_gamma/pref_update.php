<?php
// editing by 
/***************************************************** Modification Log ************************************************
 * 2017-07-03 (Carlos) : Fixed email forwarding checking for shared mailbox.
 * 2015-09-17 (Carlos) : Use flag $sys_custom['iMailPlus']['DisableAutoForward'] to control availability of auto forward.
 * 2015-04-14 [Carlos] : Auto forwarding check and exclude self email. 
 * 2014-06-10 [Carlos] : Fix auto forwarding, if do not keep copy, non-last forwarding emails must keep copy,
 * 						 only the last one can be either keep or not keep
 * 2012-08-20 [Carlos] : Modified to allow set multiple mail forwards
 * 2010-12-09 [Carlos] : Changed to use Email as ID to get/set preference
 ***********************************************************************************************************************/
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
#include_once("../../includes/libcampusquota.php");
include_once("../../includes/libaccess.php");
include_once("../../includes/libwebmail.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../includes/imap_gamma.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$lwebmail = new libwebmail();
$IMap = new imap_gamma();
/*
if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
{
    include_once("../../includes/libsystemaccess.php");
    $lsysaccess = new libsystemaccess($UserID);
    if ($lsysaccess->hasMailRight())
    {
        $noWebmail = false;
    }
    else
    {
        $noWebmail = true;
    }
}
else
{
    $noWebmail = true;
}
*/
# Block No webmail no preference

if ($UserID=="")
{
    header("Location: index.php");
    exit();
}

$li = new libuser($UserID);


$display_name_field="";
$reply_email_field="";
$days_in_spam_field="";
$days_in_trash_field="";
$signature_field="";
$forwarding_emails_field="";
$keep_copy_field="";


# submit from general prference page
if($TabID==1){
	# display name
	$display_name_field = " DisplayName = '$display_name', ";
	
	# reply email
	$reply_email_field = " ReplyEmail = '$reply_email', ";
//	
//	# days in spam
//	$days_in_spam_field = " DaysInSpam = '$days_in_spam', ";
//	
//	# days in trash
//	$days_in_trash_field = " DaysInTrash ='$days_in_trash', ";
	
	# auto save interval
	$auto_save_interval = " AutoSaveInterval ='$interval', ";
}
else if($TabID==2){ # submit from signature page
	# signature
	$signature_field = " Signature = '$signature', ";
}
else if($TabID==3){ # submit from email forwarding page
		
		if($sys_custom['iMailPlus']['DisableAutoForward']){
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		//$luser = new libuser($UserID);
		//$userEmail = $luser->ImapUserEmail;
		//$userAlias = $luser->Get_User_Alias_List();
		$userEmail = $_SESSION['SSV_LOGIN_EMAIL'];
		$emailList = array();
		$emailList[] = $_SESSION['SSV_LOGIN_EMAIL'];
		if (is_array($userAlias))
		{
			for ($i = 0; $i < count($userAlias); ++$i)
            {
            	$emailList[] = $userAlias[$i]["EmailAlias"];
            }
		}
		
		$passport = ($userEmail != $forwardAddress);
		if($disable_email_forwarding==""&&$forwarding_emails!="")
		{
			$forwarding_emails_ary = explode("\n",$forwarding_emails);
			$passport = $passport && count(array_intersect($emailList,$forwarding_emails_ary)==0); // avoid forwarding email to it self and its alias
			
		}
		$ResultArr = array();
		# not yet finished 2010011
		#######################################################################################
		if ($passport) //not forwarding to itself 
		{
			$emailList = array_unique($emailList);
			$targetEmail="";
			
//			if ($_SESSION['SSV_USER_TYPE'] != "S") {
				if ($disable_email_forwarding == "" && $forwarding_emails!="") 
				{
					$IMap->removeMailForward($emailList);
					$delimiter = "";
					$forwarding_emails_ary_count = count($forwarding_emails_ary);
					for($j=0;$j<$forwarding_emails_ary_count;$j++)
					{
						$thisEmail = $forwarding_emails_ary[$j];
						$thisEmail = trim($thisEmail);
						if($thisEmail == '' || $thisEmail == $userEmail){
							continue;
						}
						if($forwarding_emails_ary_count > 0 && $j<($forwarding_emails_ary_count-1)){
							$UpdateSuccess = $IMap->setMailForward($thisEmail, 1, $emailList); // non last forwarding email must keep copy
						}else{
							$UpdateSuccess = $IMap->setMailForward($thisEmail, $keep_copy, $emailList);
						}
						$ResultArr[] = $UpdateSuccess;
						if($UpdateSuccess)
						{
							$targetEmail .=$delimiter.$thisEmail;
							$delimiter = ",";
						}
					}
					$forwarding_emails_field =" ForwardedEmail='$targetEmail',";
				}
				else
				{
					$UpdateSuccess = $IMap->removeMailForward($emailList);
					if($UpdateSuccess)
						$forwarding_emails_field =" ForwardedEmail='', ";
					$ResultArr[] = $UpdateSuccess;
				}
//			}
//			else {
//				$UpdateSuccess = $GmailApi->Set_Auto_Forward(($_POST["EmailForward"] == "1"),$forwardAddress,($_POST["KeepLocalCopy"] == "1"));
//			}

		}
		in_array(false,$ResultArr)?$UpdateFail = 1: $UpdateSuccess = 1;
		//$UpdateFail = !$UpdateSuccess;
		#########################################################################################
	# forwarding email
//	$forwarding_emails_field =" ForwardedEmail='', ";
//	$login = $li->UserLogin;
//	if($disable_email_forwarding==""&&$forwarding_emails!=""){
//		$temp = explode("\n",$forwarding_emails);
//		$delimiter = "";
//		$delim_db="";
//		$targetEmail="";
//		for($i=0;$i<sizeof($temp);$i++){
//			$t_mail = trim($temp[$i]);
//			if(intranet_validateEmail($t_mail)){
//				$targetEmail .=$delimiter.$t_mail;
//				$delimiter = ",";
//			}
//		}
//		if($targetEmail!=""){
//			$lwebmail->setMailForward($login, $targetEmail, $keep_copy);
//			$forwarding_emails_field =" ForwardedEmail='$targetEmail', ";
//
//		}
//	}
//	else{
//		//echo "login=$login";
//		$lwebmail->removeMailForward($login);
//	}
//
//	
	# keep copy
	$keep_copy_field = $keep_copy=="1"?"ForwardKeepCopy=1,":"ForwardKeepCopy=0,";
}
else if ($TabID == 4) { 
	
     # update table
     $sql = "SELECT MailBoxName FROM MAIL_PREFERENCE WHERE MailBoxName = '".$_SESSION['SSV_LOGIN_EMAIL']."' ";
     $temp = $li->returnVector($sql);

     if ($temp[0]==$_SESSION['SSV_LOGIN_EMAIL'])    # Update
     {
         $sql = "UPDATE MAIL_PREFERENCE SET SkipCheckEmail = '".$_REQUEST['disable_check_email']."', DateModified=NOW() WHERE MailBoxName = '".$_SESSION['SSV_LOGIN_EMAIL']."'";
     }
     else          # Insert
     {
         $sql = "INSERT MAIL_PREFERENCE (MailBoxName, SkipCheckEmail, DateInput, DateModified)
                 VALUES ('".$_SESSION['SSV_LOGIN_EMAIL']."','".$_REQUEST['disable_check_email']."', NOW(), NOW() )";
     }
     $li->db_db_query($sql);
}
else if ($TabID == 5) { // Auto Reply
	
	$luser = new libuser($UserID);
	$userEmail = $luser->ImapUserEmail;
	//$userAlias = $luser->Get_User_Alias_List();
	$emailList = array();
	$emailList[] = $_SESSION['SSV_LOGIN_EMAIL'];
		
	if ($_SESSION['SSV_LOGIN_EMAIL'] == $userEmail){
		for ($i = 0; $i < count($userAlias); ++$i){
			($userEmail != $userAlias[$i]["EmailAlias"]) ? $passport=true : $passport = false;
			for ($j = 0; $j < count($emailList); ++$j)
			{
				(($userAlias[$i]["EmailAlias"] != $emailList[$j]) & ($passport)) ? $passport = true : $passport = false;
			}
			if ($passport)
				$emailList[] = $userAlias[$i]["EmailAlias"];
		}
	}

//	if ($UserType != "2") {
		if ($enable_auto_reply)
		{
			$UpdateSuccess = $IMap->setAutoReply($reply_content, $emailList);
			if($UpdateSuccess)
				$auto_reply_field = " AutoReplyTxt = '$reply_content', ";
		}
		else
		{
			$UpdateSuccess = $IMap->removeAutoReply("", $emailList);
			
			if($UpdateSuccess)
				$auto_reply_field = " AutoReplyTxt = '', ";
		}
		$UpdateFail = !$UpdateSuccess;
		//$UpdateSuccess = $IMap->Update_Preference($UpdateValue);
//	}
//	else {
//		$UpdateFail = !($GmailApi->Set_Auto_Reply(($enable_auto_reply),"",$reply_content));
//	}
//     if ($enable_auto_reply)
//     {
//         if ($reply_content)
//         {
//          	 $reply_content = stripslashes($reply_content);
//             $lwebmail->addAutoReply($li->UserLogin, $reply_content);
//         }
//     }
//     else
//     {
//         $lwebmail->removeAutoReply($li->UserLogin);
//     }
}

		
		
if ($TabID == 1 || $TabID == 2 || $TabID == 3 || $TabID == 5)
{
# update table
$update_fields ="";
$update_fields =$display_name_field.$reply_email_field.$days_in_spam_field.$days_in_trash_field.$auto_save_interval.$signature_field.$forwarding_emails_field.$keep_copy_field.$auto_reply_field;
$update_fields = trim($update_fields);
if($update_fields!=""){
        $sql="INSERT INTO MAIL_PREFERENCE (MailBoxName,DateInput,DateModified) VALUES('".$_SESSION['SSV_LOGIN_EMAIL']."',NOW(),NOW())";
        $li->db_db_query($sql);
        $update_fields .=" DateModified=NOW()";
        $sql = "UPDATE MAIL_PREFERENCE SET $update_fields WHERE MailBoxName='".$_SESSION['SSV_LOGIN_EMAIL']."' ";
        $li->db_db_query($sql);
}

}

intranet_closedb();

$return_url = $_SERVER['HTTP_REFERER'];
	
$return_url = explode("?",$return_url);

$anc = $special_option['no_anchor'] ?"":"#anc";
header("Location: ".$return_url[0]."?TabID=$TabID&msg=2".$anc);
?>