<?php
// Editing by 

/********************
 * change log:IP
 * 2020-11-05 Ray: add $_SESSION['SSV_EMAIL_LOGIN'], $_SESSION['SSV_EMAIL_PASSWORD']
 * 2020-01-23 add CKeditor for displaying HTML email content
 * 2014-11-26 modify newGenerateInternalAndExternalTotalAddressList();
 * 2014-11-17 modify initilizeNewButtons()
 * 2014-11-17 add safeAlert()
 * 2014-11-14 remove  function initilterFieldTags(), initilize the tag in html by adding li
 *            remove tagAddress() in document and change function tagAddress;
 *            change removeTag(), getCurrentEmailAddress, remove generateInternalAndExternalTotalAddressList()
 *            add checkIsGamma(), newGenerateInternalAndExternalTotalAddressList()
 ********************/
error_reporting(0);

$PATH_WRT_ROOT = '../../../';

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libpwm.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_hardcode_lang.php");
//include_once($PATH_WRT_ROOT."plugins/gamma_mail_conf.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp_imail.php');
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");

//$uid = IntegerSafe($_GET['uid']);
//if($_SESSION['UserID'] ==''){
//$_SESSION['UserID'] = $uid;
//}

intranet_auth();
intranet_opendb();

$libDb = new libdb(); 
$libeClassApp = new libeClassApp(); 
$json = new JSON_obj();
$libpwm= new libpwm();
$emailClassApp = new emailClassApp();
//$token = $_GET['token'];
//$ul = $_GET['ul'];
//$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
//if (!$isTokenValid) {
//	die($Lang['Gamma']['App']['NoAccessRight']);
//}
$uid = $_SESSION['UserID'];

$curUserId = $uid;
$UserID = $uid;
$thisFolder= $_GET["TargetFolderName"];
$currentUID =IntegerSafe($_GET["CurrentUID"]);
$ContentMailAction = $_GET["MailAction"];

//imail original addresses arrays
  $FromAry = array();
  $ToAry = array();
  $CcAry = array();
  $BccAry = array();

  $TagToAry = array();
  $TagCcAry = array();
  $TagBCcAry = array();
  $isMultipleToAddresses = 0;
  $isHasCCorBccAddress = 0;
  $ReceiverTag = 'Receiver';
  $UIDTag = 'UID';
if ($plugin['imail_gamma']) {
    $isImailGamma = 1;
	$passwordAry = $libpwm->getData($uid);
	foreach((array)$passwordAry as $_folderType => $_folderKey){
		if($_folderType==$uid)$password=$_folderKey;
	}
    $sql = "Select ImapUserEmail From INTRANET_USER Where UserID = '".$curUserId."'";
	$dataAry = $libDb->returnResultSet($sql);

	if(isset($_SESSION['SSV_EMAIL_LOGIN']) && isset($_SESSION['SSV_EMAIL_PASSWORD'])) {
		$iMapEmail = $_SESSION['SSV_EMAIL_LOGIN'];
		$iMapPwd = $_SESSION['SSV_EMAIL_PASSWORD'];
	} else {
		$iMapEmail = $dataAry[0]['ImapUserEmail'];
		$iMapPwd = $password;
	}
	
    $IMap = new imap_gamma(false,$iMapEmail, $iMapPwd);
	$IMap->IMapCache = new imap_cache_agent($iMapEmail, $iMapPwd);
	//generate addresses
	$isAccessInternetMail = 1;
    $isAccessExternetMail = $IMap->AccessInternetMail($uid)?1:0;
    #ENCODE FOLDER IN URL(TargetFolderName)
    $FolderForLink =  urldecode($IMap->decodeFolderName($thisFolder));
    $FolderForLink = urlencode($IMap->encodeFolderName(stripslashes($FolderForLink)));
	if($thisFolder!="INBOX"){
    	$Folder = "INBOX.".$thisFolder;
    }
    else{
    	$Folder = $thisFolder;
    }
	$Folder = (isset($Folder) && $Folder != "") ? stripslashes($Folder) : $IMap->InboxFolder;
	$CacheNewMail = false;
	
	#GET EMAIL INFO FOR REPLY AND REPLY-ALL AND DRAFT
	if($currentUID != null){
	$currentMailRecord = $IMap->getMailRecord($Folder, $currentUID, '');
    $currentMailRecordContent = $currentMailRecord[0];	
    $__subject = $currentMailRecordContent['subject'];
	$__dateReceive = $currentMailRecordContent['dateReceive'];
	$__message = $currentMailRecordContent['message'];
	$_currentAttachment = $currentMailRecordContent['attach_parts'];
	$__isAttachment = false;
	if(count($_currentAttachment)>0){
		$__isAttachment = true;
	}
	$MailFlags = $IMap->GetMailPriority($currentUID,$Folder,1);
    $__isImportant = $MailFlags[intVal($currentUID)]['ImportantFlag']=== 'true'?true:false;
	
	$_currentMailToArrays = $currentMailRecordContent['toDetail'];
	$_currentMailCcArrays = $currentMailRecordContent['ccDetail'];
	$_currentMailBccArrays = $currentMailRecordContent['bccDetail'];
	
	$fromDetail = $currentMailRecordContent['fromDetail'][0];
	$__mailBox = $fromDetail->mailbox;
	$__host = $fromDetail->host;
	$__fromname = $__mailBox.'@'.$__host.';';
    $FromAry[0][$ReceiverTag] = $__fromname;
    $FromAry[0][$UIDTag] = -1;
    
    $tempToAry = array();
	for($i = 0;$i<count($_currentMailToArrays);$i++){
		$stdClassObject = $_currentMailToArrays[$i];
		foreach((array)$stdClassObject as $_Key => $_Detail){
			if($_Key=='mailbox'){
				$__mailBox = $_Detail;
			}
			else if($_Key=='host'){
				$__host = $_Detail;
			}
		}
     $tempMail = $__mailBox.'@'.$__host;
     if(($tempMail!=null)&&($tempMail!=$__fromname)){
     $tempToAry[$ReceiverTag] = $tempMail;
     $tempToAry[$UIDTag] = -1;
     $ToAry[] = $tempToAry;	
     }
	}
    $tempCcAry = array();
	for($i =0;$i<count($_currentMailCcArrays);$i++){
		$stdClassObject = $_currentMailCcArrays[$i];
		foreach((array)$stdClassObject as $_Key => $_Detail){
			if($_Key=='mailbox'){
				$__mailBox = $_Detail;
			}
			else if($_Key=='host'){
				$__host = $_Detail;
			}
		}
		 $tempMail = $__mailBox.'@'.$__host;
		 if(($tempMail!=null)&&($tempMail!=$__fromname)){
		 $tempCcAry[$ReceiverTag] = $tempMail;
		 $tempCcAry[$UIDTag] = -1;
		 $CcAry[] = $tempCcAry;	
		 }
	}
    $tempBccAry = array();
	for($i =0;$i<count($_currentMailBccArrays);$i++){
		$stdClassObject = $_currentMailBccArrays[$i];
		foreach((array)$stdClassObject as $_Key => $_Detail){
			 if($_Key=='mailbox'){
				$__mailBox = $_Detail;
			}
			else if($_Key=='host'){
				$__host = $_Detail;
			}
		}
		 $tempMail = $__mailBox.'@'.$__host;
		 if(($tempMail!=null)&&($tempMail!=$__fromname)){
		 $tempBccAry[$ReceiverTag] = $tempMail;
		 $tempBccAry[$UIDTag] = -1;
		 $BccAry[] = $tempBccAry;
		 }
	}	
	}#END INFO GET OF REPLY AND REPLY ALL
	
	#CURRENT ACCOUNT EMAIL BASIC INFO
	$CurUserAddress = $IMap->CurUserAddress;
    $CurUserPassword = $IMap->CurUserPassword;
	$_Quota = $IMap->IMapCache->Check_Quota();
	$used = $_Quota['UsedQuota'];
	$total = $_Quota['TotalQuota'];
	
	if(round($total)==0 || $total==''){ // maybe fail to get quota via IMAP command, try osapi
	$tmp_used = $IMap->getUsedQuota($_SESSION['SSV_LOGIN_EMAIL'],"iMail");
	$tmp_total = $IMap->getTotalQuota($_SESSION['SSV_LOGIN_EMAIL'],"iMail");
	$used = round($tmp_used / 1024 / 1024,2);
	$total = round($tmp_total,2);
	}
	if($total == 0){
		$is_unlimited = true;
		$total = $Lang['Gamma']['Unlimited'];
	}
	$unUsedQuota = ($total - $used)*1024*1000;
	$MailHeader = $IMap->Get_Preference($iMapEmail);
	$_signature = $MailHeader['Signature'];
	$has_webmail = true;
    $email_field = "IMapUserEmail";   
    $gamma_filter = " AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ";
    $maxAttachSize = $SYS_CONFIG['Mail']['MaxAttachmentSize']*1000?$SYS_CONFIG['Mail']['MaxAttachmentSize']*1000:10240*1000; 
}//end gamma
else{
     $isImailGamma = 0;
	 $lc = new libcampusquota2007($UserID);
	 $lwebmail = new libwebmail();
	 $lcampusmail = new libcampusmail();
 	if($thisFolder =='INBOX'){
	$folderID[0] = 2;
	}else if($thisFolder =='Drafts'){
		$folderID[0] = 1;
	}else if($thisFolder =='Sent'){
		$folderID[0] = 0;
	}else if($thisFolder == 'Trash'){
        $folderID[0] = -1;
	}else{
    $sql = "select FolderID from INTRANET_CAMPUSMAIL_FOLDER where OwnerID='$uid' and foldername = '".$libDb->Get_Safe_Sql_Query($thisFolder)."'";
	$folderID = $libDb->returnVector($sql);
	}//end initilize folderID   
	
    $used = $lc ->returnUsedQuota();
	$total = $lc ->returnQuota()* 1024;
	$unUsedQuota = ($total - $used)*1000;
	
	//generate addresses
    $lwebmail = new libwebmail();
	if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) && $lwebmail->type == 3)
	{
	    $noWebmail = false;
	    $has_webmail = 1;
	}
	else
	{
	    $noWebmail = true;
	    $has_webmail = 0;
	}
	# Check sending access right
	$hide_internal = $lcampusmail->usage_internal_disabled;
	$hide_external = ($noWebmail || $lcampusmail->usage_external_disabled);
 	$isAccessInternetMail = $hide_internal?0:1;
    $isAccessExternetMail = $hide_external?0:1;	
    
	if($ContentMailAction !='Compose'){
	 if( $folderID[0] == -1){
	 $sql  = "select Message,SenderID,SenderEmail,Subject,DateInput,ExternalTo, RecipientID,InternalCC,ExternalCC,InternalBCC,ExternalBCC,CampusMailFromID,IsAttachment,IsImportant from INTRANET_CAMPUSMAIL where UserID ='$uid' and Deleted= '1' AND CampusMailID='$currentUID'";		
	}else{
	 $sql  = "select Message,SenderID,SenderEmail,Subject,DateInput,ExternalTo, RecipientID,InternalCC,ExternalCC,InternalBCC,ExternalBCC,CampusMailFromID,IsAttachment,IsImportant from INTRANET_CAMPUSMAIL where UserID ='$uid' and UserFolderID= '$folderID[0]' AND CampusMailID='$currentUID'";
	}	
	    $MailHeader = $lc->returnArray($sql);
	    
	    $content = $MailHeader[0];
	    $__subject = $content['Subject']; 
        $__dateReceive = $content['DateInput']; 
        $__message = $content['Message'];
        $senderID = $content['SenderID'];
        $__fromname = $content['SenderEmail'];
        $__isAttachment = ($content['IsAttachment'] === '1')?true:false;
        $__isImportant = ($content['IsImportant'] === '1')?true:false;
		
        $__currentMailTos = $content['ExternalTo'];       
        $__RecipientUID = $content['RecipientID'];
        $__InternalCCUID = $content['InternalCC'];
        $__externalCC = $content['ExternalCC'];
        $__InternalBCC = $content['InternalBCC'];
        $__ExternalBCC = $content['ExternalBCC'];
        $ToExternalAry =  $emailClassApp->getImailExternalTrans($__currentMailTos,$ReceiverTag,$UIDTag);
        $ToInternalAry =  $emailClassApp->getImailInternalTrans($__RecipientUID,$ReceiverTag,$UIDTag,$parLang,$lc);
        $ToAry = array_merge($ToExternalAry,$ToInternalAry);
        $CcExternalAry = $emailClassApp->getImailExternalTrans($__externalCC,$ReceiverTag,$UIDTag);
        $CcInternalAry=  $emailClassApp->getImailInternalTrans($__InternalCCUID,$ReceiverTag,$UIDTag,$parLang,$lc);
        $CcAry = array_merge($CcExternalAry,$CcInternalAry);
        $BccExternalAry = $emailClassApp->getImailExternalTrans($__ExternalBCC,$ReceiverTag,$UIDTag);
        $BccInternalAry =  $emailClassApp->getImailInternalTrans($__InternalBCC,$ReceiverTag,$UIDTag,$parLang,$lc);
        $BccAry = array_merge($BccExternalAry,$BccInternalAry);
 
        if(($senderID!=null)&&($senderID!='')){
        $nameLan = Get_Lang_Selection('ChineseName', 'EnglishName');
        $sql  = "select ".$nameLan.",UserEmail from INTRANET_USER where UserID ='$senderID'";  
        $SenderName = $lc->returnArray($sql);	
        $senderNameLan = $SenderName[0][$nameLan];
        $senderEmail = $SenderName[0]['UserEmail'];
        $__fromname = $senderNameLan ."<".$senderEmail.">";
		$FromAry[0][$UIDTag] = $senderID;
        }else{
        $FromAry[0][$UIDTag] = -1;	
        }
        $FromAry[0][$ReceiverTag] = $__fromname;
		$DateFormat = "Y-m-d";
		$prettydate='';
		if($__dateReceive!=''){
	    $__dateReceive = date($DateFormat, strtotime(str_ireplace("UT","UTC",$__dateReceive)));
		}	
	}//end not compose which has no currentUID
	$sql = "SELECT Signature FROM INTRANET_IMAIL_PREFERENCE WHERE UserID = '$uid'";
	$MailHeader = $lc->returnArray($sql);	
	$_signature = $MailHeader[0]['Signature'];
	
    $mail_domain = $lwebmail->mailaddr_domain;
	$email_field = " CONCAT(UserLogin,'@$mail_domain') ";	
    $gamma_filter = "";
    $FolderForLink = $thisFolder;
    $atta = $content['CampusMailFromID']!=''?$content['CampusMailFromID']:$currentUID; 
    $getAttachmentSql = "select FileName,AttachmentPath from INTRANET_IMAIL_ATTACHMENT_PART where CampusMailID='$atta'";
    $_currentAttachment = $libDb->returnArray($getAttachmentSql);	 
    if(count($_currentAttachment)>0){
    	if(isKIS()){
    		 $originalEmailAttachmentLink = dirname($_currentAttachment[0]['AttachmentPath']);		
    	}else{
    		 $originalEmailAttachmentLink = $_currentAttachment[0]['AttachmentPath'];	   		
    	}
    }
    $maxAttachSize = $webmail_info['max_attachment_size']*1000?$webmail_info['max_attachment_size']*1000:10240*1000; 
}//end imail

#Other function using Email with recipientID
$recipientAddress = '';
$recipientTagAry = array();
if($recipientID!=''){
$sql = "Select ImapUserEmail,UserEmail From INTRANET_USER Where UserID = '".$recipientID."'";
$recipientAry = $libDb->returnResultSet($sql);
$recipientAddress = $recipientAry[0]['ImapUserEmail']!=null?$recipientAry[0]['ImapUserEmail']:$recipientAry[0]['UserEmail'];
$recipientTagAry[0][$ReceiverTag] = $recipientAddress;
if ($plugin['imail_gamma']) {
$recipientTagAry[0][$UIDTag] = -1;
}else{
$recipientTagAry[0][$UIDTag] = $recipientID;
}
}	
$currentMailShowedContent = array();	
$currentMailShowedContent['Sender']=$__fromname;
$currentMailShowedContent['Date']=$__dateReceive;
//GET FROM link parameter
$currentMailShowedContent['EmailReplyType'] = $ContentMailAction;
if($_signature != ''){
$currentMailShowedContent['Signature'] = $_signature;	
}else{
$currentMailShowedContent['Signature'] = "<br><br><br>";	
}

$__subject = str_replace('"','&#34',$__subject);
$__subject = str_replace('"','&#39',$__subject);
$isTagCC = 0;
$isTagBCC = 0;
$_externalCcAddresses = '';
$_internalCCIDs = '';
$isInitialAttach = 0;
$initialImportant = "";
if($ContentMailAction =='Compose'){
	if($recipientAddress!=''){
		$TagToAry = $recipientTagAry;	
	}
	$currentMailShowedContent['Subject']='';
    $currentMailShowedContent['ContentTitleMessage']= $currentMailShowedContent['Signature']."\n";
    $currentMailShowedContent['ContentSenderMessage'] ='';
    $currentMailShowedContent['ContentDateMessage'] = '';
    $currentMailShowedContent['Content'] = '';
    $senderID = $recipientID;
}
else if($ContentMailAction =='Reply'){
	
	$TagToAry = $FromAry;
	$currentMailShowedContent['Subject']="Re:".$__subject;
    $currentMailShowedContent['ContentTitleMessage']= $currentMailShowedContent['Signature']."\n";
    $currentMailShowedContent['ContentSenderMessage'] ='';
    $currentMailShowedContent['ContentDateMessage'] = $currentMailShowedContent['Date'].",".$currentMailShowedContent['Sender'];
    $currentMailShowedContent['Content'] = "wrote:".$__message;
}else if($ContentMailAction =='ReplyAll'){
    $TagToAry = $FromAry;
    if(count($ToAry)>=1){
    	$isMultipleToAddresses = true;
        $tagToJson = "{\"toAddressArr\":".$json->encode($ToAry)."}";
    }
    
    $TagCcAry = $CcAry;
    if(count($TagCcAry)>0){    	
        $isTagCC = 1;
        $tagCcJson = "{\"ccAddressArr\":".$json->encode($TagCcAry)."}";
    }
	$currentMailShowedContent['Subject']="Re:".$__subject;
    $currentMailShowedContent['ContentTitleMessage']= $currentMailShowedContent['Signature']."\n";
    $currentMailShowedContent['ContentSenderMessage'] ='';
    $currentMailShowedContent['ContentDateMessage'] = $currentMailShowedContent['Date'].",".$currentMailShowedContent['Sender'];
    $currentMailShowedContent['Content'] = "wrote:".$__message;
	
}else if($ContentMailAction =='Forward'){
    if($__isAttachment){
	   $isInitialAttach = 1;
	}
	$currentMailShowedContent['Subject']="Fwd:".$__subject;
    $currentMailShowedContent['ContentTitleMessage']= $currentMailShowedContent['Signature']."\n"."Forward message------------------------";
    $currentMailShowedContent['ContentSenderMessage'] = "From:".$currentMailShowedContent['Sender'];
    $currentMailShowedContent['ContentDateMessage'] = "Date:".$currentMailShowedContent['Date'];
    $currentMailShowedContent['Content'] = $__message;

}else if($ContentMailAction =='Drafts'){
	if($__isAttachment){
	   $isInitialAttach = 1;
	}
    if($__isImportant){
    	$initialImportant = "checked";
    }
    
    $TagToAry = $ToAry;
    $TagCcAry = $CcAry;
    $TagBCcAry = $BccAry;
    if(count($TagCcAry)>0){
        $isTagCC = 1;
        $tagCcJson = "{\"ccAddressArr\":".$json->encode($TagCcAry)."}";
    }
    if(count($TagBCcAry)>0){
        $isTagBCC = 1;
        $tagBCcJson = "{\"bccAddressArr\":".$json->encode($TagBCcAry)."}";
    }
	$currentMailShowedContent['Subject'] = "".$__subject;
	$currentMailShowedContent['ContentTitleMessage'] = $__message;
    $currentMailShowedContent['ContentSenderMessage'] = "";
    $currentMailShowedContent['ContentDateMessage'] = "";
    $currentMailShowedContent['Content'] = "";
}
    $currentMailShowedContent['Content'] = htmlspecialchars_decode($currentMailShowedContent['Content']);
     
    $emil_reply_forward_operation_form_action="email_operation.php?MailAction=".$ContentMailAction."&parLang=".$parLang."&TargetFolderName=".$FolderForLink."&CurrentUID=".$currentUID;
	$emil_reply_forward_send_form_action = $emil_reply_forward_operation_form_action."&IMailOperation=Send";
	$emil_reply_forward_save_form_action =  $emil_reply_forward_operation_form_action."&IMailOperation=Save";
	
	//sending email loading icon
    //icon paths
	$mySaveAsDraftIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/teacher_icon_save_as_draft.png"."\"";
	$mySendIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/teacher_icon_send.png"."\"";
	$myCloseIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/icon_close.png"."\"";
	$myAttachmentIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/icon_attachment.png"."\"";
	$importantFlagUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/icon_important.gif"."\"";
    $isNotificationUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/icon_notification.gif"."\"";
	
   //language setting
   $_SELECT_ALL = $Lang['Gamma']['SelectAll'];
   
   
   $roundMaxAttachSize = round($maxAttachSize/(1024*1000),2);
   $maxAttachSizeNotification = $Lang['Gamma']['App']['UploadFileExceedLimit'].$roundMaxAttachSize."MB";
   $overQuotaNotification = $Lang['Gamma']['App']['UploadFileExceedQuota'];
   $addAttachmentFailed =  $Lang['Gamma']['App']['UploadFileStyleFailed'];

   #Generate the attachmentLink#
   if($isInitialAttach){
   	   if(count($_currentAttachment)>0){
   	   	$PartIDList = '';
   	     for($__CACount =0;$__CACount<count($_currentAttachment);$__CACount++){
   	      $PartIDList .= $_currentAttachment[$__CACount]['PartID'].",";
   	      }
        }else{
        	$isInitialAttach = 0;
        }
   }  
  ##header
  $x = $emailClassApp->getEmailComposePageHeader($emil_reply_forward_operation_form_action);
  ## collapsible To Add The Receiver Tag if needed
  $x .= '<div id="demo-borders" style ="width:100%;background-color:#FFFFFF">';
  $x .=$emailClassApp->getEmailCollipseNTagAddressHtml('myReceiverFilterFieldset','To:','myReceiverFilterFieldTags',$TagToAry);
  $x .= $emailClassApp->getEmailchosenAddressInitListHtml('mySearchPickPlace','myReceiverFilterAddressList','#myReceiverLatestInputAddress',$isAccessInternetMail,$isAccessExternetMail);
  $x .= '</fieldset></div>';
  $isFoldCcBcc ="show";
  $isExpandCcBcc = "hidden";
  $x .= $emailClassApp->isShowFoldCcNBccHtml($isFoldCcBcc);

  $x .= '<div id="myExpandedCcNBcc" class = '.$isExpandCcBcc.'>';
  $x .= $emailClassApp->getEmailCollipseNTagAddressHtml('myCcFilterFieldset','CC:','myCcFilterFieldTags',array());
  $x .= $emailClassApp->getEmailchosenAddressInitListHtml('myCcSearchPickPlace','myCcFilterAddressList','#myCcLatestInputAddress',$isAccessInternetMail,$isAccessExternetMail);
  $x .= '</fieldset>';
  ##collasible BCC And Tag the BCc if needed
  $x .= $emailClassApp->getEmailCollipseNTagAddressHtml('myBccFilterFieldset','BCC:','myBccFilterFieldTags',array());
  $x .= $emailClassApp->getEmailchosenAddressInitListHtml('myBccSearchPickPlace','myBccFilterAddressList','#myBccLatestInputAddress',$isAccessInternetMail,$isAccessExternetMail);
  $x .= '</fieldset></div>';
  $x .= $emailClassApp->getSubjectHtml($currentMailShowedContent['Subject']);
  $x .= $emailClassApp->getAttachmentHtml($_currentAttachment,$isInitialAttach);
  //$x .= $emailClassApp->getContentHtml($currentMailShowedContent);
  $x .= $emailClassApp->getContentCKEditor($currentMailShowedContent);
  $x .= $emailClassApp->getImailImportantAndNotificationHtml($initialImportant,$importantFlagUrl,$isNotificationUrl);
  $x .= $emailClassApp->getHiddeenInputHtml($_externalCcAddresses,$_internalCCIDs);
  $firstAddressLayerList = $emailClassApp->firstAddressLayerListGenerator($isAccessInternetMail,$isAccessExternetMail);
echo $libeClassApp->getAppWebPageInitStart();
?>
		<script>
		window.JSON || 
		document.write('<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/json3.min.js"><\/script>');
		</script>
		<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.tagit.css" type="text/css">
		<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery-ui.css" type="text/css">
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/tag-it.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?=$PATH_WRT_ROOT?>templates/script.js" type="text/javascript" charset="utf-8"></script>
		<script language="javascript">
		var jQuery_1_0_2 = jQuery.noConflict( true );
        </script>
	</head>
	<body>
	<div data-role="page">	
	<?=$x;?>
    </form>
	<style type="text/css">
	input
    {
	    box-shadow:inset 0 0 0px 0px #f6f6f6;
    }
	div.hidden{
		display:none ;
	}
	
	div.show{
		display:block ;
	}
	a{background-color:#FFFFFF !improtant;}
	
	.custom {
		padding: 0;
	}
	.custom > div {
	    margin: 0;
	}
	.custom > div > label {
	    border-radius: 0;
	    border: 0;
	}

	.ui-icon-myapp-saveAsDraft{
	     background-color: transparent !important;
	   	 border: 0 !important;
	   	 -webkit-box-shadow:none !important;  
		 background: url(<?=$mySaveAsDraftIconUrl?>) no-repeat !important;
    }
    .ui-icon-myapp-send{
	      background-color: transparent !important;          
	   	 border: 0 !important;
	   	 -webkit-box-shadow:none !important;
		 background: url(<?= $mySendIconUrl ?>) no-repeat !important;
    }
    
    .ui-icon-myapp-delete{
	     background-color: transparent !important;
	   	 border: 0 !important;
	   	 -webkit-box-shadow:none !important;
		 background: url(<?=$myCloseIconUrl?>) no-repeat !important;
    }
      
    .myReceiverFilterAddressList {
	    overflow: auto;
	    height:200px;
    }

	#selectedFiles img {
		max-width: 20px;
		max-height: 20px;
		float: left;
		margin-bottom:10px;
	}
	#selectedFiles a {
		max-width: 20px;
		max-height: 20px;
		float: right;
		margin-bottom:10px;
	}
	
	.ui-li-text-my-chosenFileNameP{
    	color:black;
    	font-size: .75em !important;
    }
    
    #upload-file-container {
	     background-color: transparent !important;
	   	 border: 0 !important;
	   	 -webkit-box-shadow:none !important;
		 background: url(<?=$myAttachmentIconUrl?>) no-repeat !important;
    }

	#upload-file-container input {
	position: absolute; !important;
	right: 0;!important;
	 font-size: <many a>px; !important;
	 opacity: 0; !important;
	 margin: 0; !important;
	 padding: 0; !important;
	 border: none;!important;
	}	
	
	 #loadingmsg {
      color: black;
      background:tranparent!important;
      position: fixed;
      top: 50%;
      left: 50%;
      z-index: 100;
      margin: -8px 0px 0px -8px;
      }
      #loadingover {
      background: white;
      z-index: 99;
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
      filter: alpha(opacity=80);
      -moz-opacity: 0.8;
      -khtml-opacity: 0.8;
      opacity: 0.8;
    }
	.ui-li-text-my-h5{
		color:black;
		font-size:1.00em !important;
		overflow: hidden; !important;
		/* max-width: -webkit-calc(100% - 150px); !important; */
		white-space: nowrap; !important;
		text-overflow: ellipsis;
	}
	div.ui-input-text.ui-body-inherit.ui-corner-all.ui-shadow-inset{
		margin-top: 0px;
	    margin-bottom: 0px;
	    border-left-width: 0px;
	    border-top-width: 0px;
	    border-right-width: 0px;
	    border-bottom-width: 0px;
	}	
	</style>
	<script>
	$(document).bind('pageinit', function() {
	     $(this).find('a[data-rel="back"]').click(function(event) {
		    <!--remove attachment if needed when back-->

		    var attachFiles = document.getElementsByName("chosenFiles[]");
		    var attachCounter = attachFiles.length;
			    if(attachCounter>0){
			     var deletedAttachmentAryString = '';
			     for(var i=0;i<attachCounter;i++){
			     	deletedAttachmentAryString = attachFiles[i].id+";"+deletedAttachmentAryString;
			     }
			     delete_attachment_in_server(deletedAttachmentAryString);
			    }
		    return true;
	     });
	  });
	  
      $(function(){
	   	   var myReceiverFilterFieldTags = jQuery_1_0_2('#myReceiverFilterFieldTags');
	   	   var myCcFilterFieldTags = jQuery_1_0_2('#myCcFilterFieldTags');
	   	   var myBccFilterFieldTags = jQuery_1_0_2('#myBccFilterFieldTags');
	   	   
		   myReceiverFilterFieldTags.tagit({
			   	afterTagAdded:function(event,ui){
				  	<!--add the corresponding emails to the hidden input-->
				    var tagID = ui.tag[0].id;
				    var tagAddress = ui.tag[0].getAttribute('name');
			  		<!--add to 'To' input-->
			  		if(tagID =='-1'){
			  		var totalExternalAddress =  $("input[name='chosenExternalRecipientAddresses']").val()+tagAddress+";";
			        $("input[name='chosenExternalRecipientAddresses']").val(totalExternalAddress);
			  		}else if(tagID !=''){
			  		 var totalAddressIDs =  $("input[name='chosenRecipientIDs']").val()+tagID+";";
			         $("input[name='chosenRecipientIDs']").val(totalAddressIDs);	
			         }<!--end set-->
			  		<!--remove address if invalide-->
			  		if((!validateEmail(tagAddress))&&(tagID==-1)){
			  		safeAlert('<?=$Lang['Gamma']['App']['InvalidAddress']?>');
			  		myReceiverFilterFieldTags.tagit('removeTag', ui.tag);   	
			  		}
			  		<!--initilize the To list-->
			  		updateLayer1ListView('To');
		        },
				afterTagRemoved:function(event,ui){
				  	<!--remove current tag id-->
				    var tagID = ui.tag[0].id;
				    var tagAddress = ui.tag[0].getAttribute('name');
				    removeTag(tagID,tagAddress,'To');    	
				  },
				onTagClicked:function(event,ui){
				  	<!--get a dialog to comfirm delete-->      	
				  	var r = confirm("<?=$Lang['Gamma']['App']['ConfirmDeleteAddressNotification']?>");
					if (r == true) {
				    var tagID = ui.tag[0].id;
				    var tagAddress = ui.tag[0].getAttribute('name');
				  	removeTag(tagID,tagAddress,'To');
				    <!--remove the showed tag-->
				    myReceiverFilterFieldTags.tagit('removeTag', ui.tag);          
					}}   	
				 });<!--end To tag setting-->
				  
		  <!--CC tag setting-->
		   myCcFilterFieldTags.tagit({
			   	afterTagAdded:function(event,ui){
			  	<!--add the corresponding emails to the hidden input-->
			    var tagID = ui.tag[0].id;
			    var tagAddress = ui.tag[0].getAttribute('name');
			  		<!--add to 'To' input-->
			  		if(tagID =='-1'){
			  			
				  	    var existChosenExternalCcAddresses = $("input[name='chosenExternalCcAddresses']").val();			  	    
				  	    <!--add if not exsist,under gamma case, each tagAddress should be different from each other-->
				  	    if(existChosenExternalCcAddresses.indexOf(tagAddress) ==-1){
				  	    var totalExternalAddress =  existChosenExternalCcAddresses+tagAddress+";";
				  	    $("input[name='chosenExternalCcAddresses']").val(totalExternalAddress);
				  	    }
			   
			  		}else if(tagID !=''){
			  			
			  			var existChosenInternalCCIDs = $("input[name='chosenInternalCCIDs']").val();
				  		if(existChosenInternalCCIDs.indexOf(tagID) ==-1){				  	    
				  	    var totalAddressIDs =  $("input[name='chosenInternalCCIDs']").val()+tagID+";";
				  	    $("input[name='chosenInternalCCIDs']").val(totalAddressIDs);	
				  	    }
				  	    
			         }<!--end set--> 
			         <!--remove address if invalide-->
			  		if((!validateEmail(tagAddress))&&(tagID==-1)){
			  		safeAlert('<?=$Lang['Gamma']['App']['InvalidAddress']?>');
			  		myCcFilterFieldTags.tagit('removeTag', ui.tag);   	
			  		}
			  		<!--initilize the To list-->
			  		updateLayer1ListView('Cc');
			    },
			    afterTagRemoved:function(event,ui){
			  	<!--remove current tag id-->
			    var tagID = ui.tag[0].id;
			    var tagAddress = ui.tag[0].getAttribute('name');
			    removeTag(tagID,tagAddress,'Cc');
			  	
			    },
			    onTagClicked:function(event,ui){
			  	<!--get a dialog to comfirm delete-->      	
			  	var r = confirm("<?=$Lang['Gamma']['App']['ConfirmDeleteAddressNotification']?>");
				if (r == true) {
			    var tagID = ui.tag[0].id;
			    var tagAddress = ui.tag[0].getAttribute('name');
			  	removeTag(tagID,tagAddress,'Cc');
			    <!--remove the showed tag-->
			    myCcFilterFieldTags.tagit('removeTag', ui.tag);          
				}}    	
			    });<!--end To tag setting-->
		 <!--end CC tag setting-->
		 
		 <!--BCC tag setting-->
		 myBccFilterFieldTags.tagit({
			 	afterTagAdded:function(event,ui){
			  	<!--add the corresponding emails to the hidden input-->
			    var tagID = ui.tag[0].id;
			    var tagAddress = ui.tag[0].getAttribute('name');
			  		<!--add to 'To' input-->
			  		if(tagID =='-1'){
			  		var totalExternalAddress =  $("input[name='chosenExternalBCCAddresses']").val()+tagAddress+";";
			        $("input[name='chosenExternalBCCAddresses']").val(totalExternalAddress);
			  		}else if(tagID !=''){
			  		 var totalAddressIDs =  $("input[name='chosenInternalBCCIDs']").val()+tagID+";";
			         $("input[name='chosenInternalBCCIDs']").val(totalAddressIDs);	
			         }<!--end set--> 
			        <!--remove address if invalide-->
			  		if((!validateEmail(tagAddress))&&(tagID==-1)){
			  		safeAlert('<?=$Lang['Gamma']['App']['InvalidAddress']?>');
			  		myBccFilterFieldTags.tagit('removeTag', ui.tag);   	
			  		}    
			  		<!--initilize the To list-->
			  		updateLayer1ListView('Bcc');
			  },
			  afterTagRemoved:function(event,ui){
			  	<!--remove current tag id-->
			    var tagID = ui.tag[0].id;
			    var tagAddress = ui.tag[0].getAttribute('name');
			    removeTag(tagID,tagAddress,'Bcc');
			  },
			  onTagClicked:function(event,ui){
			  	<!--get a dialog to comfirm delete-->      	
			  	var r = confirm("<?=$Lang['Gamma']['App']['ConfirmDeleteAddressNotification']?>");
				if (r == true) {
			    var tagID = ui.tag[0].id;
			    var tagAddress = ui.tag[0].getAttribute('name');
			  	removeTag(tagID,tagAddress,'Bcc');
			    <!--remove the showed tag-->
			    myBccFilterFieldTags.tagit('removeTag', ui.tag);          
				}}
			  });<!--end To tag setting-->
		   <!--end BCC tag setting-->  
		});<!--end Tag Function-->
		
     var isCcNBccAddressesHide = true;	<!--parameter isCcNBccAddressesHide make To determine whether myExpandedCcNBcc is shown-->
     var isMultipleToAddressesHide = true;	<!--parameter isMultipleToAddressesHide make To address click only triger tag multiple address once-->
	 $(document).ready(function(){
	 
       if(<?=$isMultipleToAddresses?> && isMultipleToAddressesHide){
       	   $('input[name="myReceiverFilterFieldTags_tagitInput"]').val('...');
       }
       if(<?=$isTagCC?>||<?=$isTagBCC?>){
       	  $("#CcNBccTextClick").val('...');
       }
       document.getElementById('attachmentFile').addEventListener('change', handleFileSelect, false);
       copy_attachment_in_server();
       initilizeNewButtons('To','layer1');
	   initilizeNewButtons('Cc','layer1');
	   initilizeNewButtons('Bcc','layer1'); 
       $("#attachmentFile").click(function(){
        	var ua = window.navigator.userAgent;
			if( ua.indexOf("Android") >= 0 )
            {
              var androidversion = ua.slice(ua.indexOf("Android")+8,ua.indexOf("Android")+13);
              if((androidversion=='4.4.1')||(androidversion=='4.4.2')||(androidversion=='4.4.3')){
              	safeAlert('<?=$Lang['Gamma']['App']['DeviceNoSupportAttachment']?>');
              }
             }
		});
		
		 $('input[name="myReceiverFilterFieldTags_tagitInput"]').click(function(){
		 	if(<?=$isMultipleToAddresses?> && isMultipleToAddressesHide){
		 	   tagToAddress();		
		 	}
		});
		   
		$("#CcNBccTextClick").click(function(){
		    isCcNBccAddressesHide = false;
		    $("#myExpandedCcNBcc").show();
		    $("#foldCc").hide();
		    tagCcAddress();		
		    tagBCcAddress();    
		});
		

		$("#sendBtn").click(function(){
			showLoading();
		    setTimeout(function(){

			tagAllAddressBeforeSendOrSave();	  	
		  	var totalReceiverAddress =  $("input[name='chosenExternalRecipientAddresses']").val();
		  	var totalReceiverIDs = $("input[name='chosenRecipientIDs']").val();
			var totalCcAddress =  $("input[name='chosenExternalCcAddresses']").val();
			var totalCcIDs = $("input[name='chosenInternalCCIDs']").val();
			var totalBccAddress =  $("input[name='chosenExternalBCCAddresses']").val();
			var totalBccAddressIDs =  $("input[name='chosenInternalBCCIDs']").val();
			var subject =  $("input[name='Subject']").val();
			if(totalReceiverAddress == ""&& totalCcAddress == "" && totalBccAddress == "" && totalReceiverIDs == ""&& totalCcIDs == "" && totalBccAddressIDs == ""){
				hideLoading();
				safeAlert('<?=$Lang['Gamma']['App']['NeedEmailAddressInput']?>');
			}else{
			<!--	var tempAddressValidAry = new Array();-->
			<!--	var address =  totalReceiverAddress+totalCcAddress+totalBccAddress;-->
		    <!--   tempAddressValidAry = address.split(";");-->
		   <!--     if((tempAddressValidAry.length == 2)&&!validateEmail(tempAddressValidAry[0]) ){-->
		    <!--    	hideLoading();-->
		   <!--     }else{-->
		         if(subject == ""){
				     var r = confirm("<?=$Lang['Gamma']['App']['NoSubjectComfirm']?>");
				     if (r == true) {
					    showLoading();
					    SendEmail();
					  }else{
					  	hideLoading();
					  }
				  }else{
						showLoading();
                        SendEmail();
				  }		
		       <!-- }-->
				
			}
		}, 300,this);
		});
		$("#saveEmailToTrash").click(function(){
			 document.email_compose_process.action = "<?=$emil_reply_forward_save_form_action?>";
            $("#email_compose_process").submit();
		});
      		
		<?php if (!$isAccessExternetMail) { ?>
		$('input[name="myReceiverFilterFieldTags_tagitInput"]').attr('readonly', 'readonly');
		$('input[name="myCcFilterFieldTags_tagitInput"]').attr('readonly', 'readonly');
		$('input[name="myBccFilterFieldTags_tagitInput"]').attr('readonly', 'readonly');
		<? } ?>
		
      });
   		function SendEmail(){
		 document.email_compose_process.action = "<?=$emil_reply_forward_send_form_action?>";
		 document.email_compose_process.emailMainBody = getEditorValue('emailMainBody');
		 document.email_compose_process.submit();
		}		
		
		function showLoading() {
		    document.getElementById('loadingmsg').style.display = 'block';
		    document.getElementById('loadingover').style.display = 'block';
		}
		function hideLoading(){
			document.getElementById('loadingmsg').style.display = 'none';
		    document.getElementById('loadingover').style.display = 'none';	
		}
   
   
   <!--ATTACHMETN-->
      function handleFileSelect(e) {
      	
      	var selDiv = document.querySelector("#selectedFiles");
		var originalSelDivHTML = selDiv.innerHTML;
        if(!e.target.files || !window.FileReader) return;
		selDiv.innerHTML = originalSelDivHTML;
		var files = e.target.files;
		var filesArr = Array.prototype.slice.call(files);
		var attachImgSrc= <?=$myAttachmentIconUrl?>;
		var deleteImgSrc= <?=$myCloseIconUrl?>;
		for(var i=0; i < files.length; i++) {
		<!--var pullPath =  e.target.value;-->
			    var f = files[i];
				var reader = new FileReader();
				reader.onload = (function(theFile){
			    var d = new Date();
                var uploadTime = d.getTime();
			    var name = uploadTime+'_'+theFile.name;
			    var currentFileSize = theFile.size;
			    if(name.indexOf(".") <= -1){
			    var type = theFile.type;
			    if(type.length != 0){
			    var imageLength = type.indexOf("/");	
			    var resSuffix = type.substring(imageLength+1, type.length);
			    name = name+'.'+resSuffix;	
			    }else{
			    if(name.indexOf("image") > -1){
			    name = name+'.jpg';	
			    }else{
			    currentFileSize = 0;	
			    }	
			    }	
			    }
                if(currentFileSize == 0){
                	safeAlert('<?=$addAttachmentFailed?>');	
                	hideLoading();
                }else{
				    <!--uploaded File cannot larger than maxAttachSize as setting-->
				    if(currentFileSize<=<?=$maxAttachSize?>){
				    <!--uploaded File cannot larger than quota-->
				    if(currentFileSize<=<?=$unUsedQuota?>){
				    return function(e){
				    saveUploadData(e.target.result,name,function(obj) {
				    var tempFileName = escapeHtml(name);
	    	  	   	var imageInput = "<input type=\"hidden\" id =\"" + tempFileName +"\" name=\"chosenFiles[]\"  value=\"" + tempFileName +"\">";
	                var html = "<img src=\"" + attachImgSrc + "\">" + name + "<a href=\"#\"  id=\"deleteChosenFile\"  name=\"deleteChosenFile\"   style = \"width: 20px; margin-right: 10px;\" >"+ "<img src=\"" + deleteImgSrc + "\" id=\"deleteChosenFile_" + name +"\" onClick=\"delete_attachment_click(this.id)\">"+"</a>"+imageInput+"<br clear=\"left\"/>";
				    selDiv.innerHTML += html;
				    hideLoading();
                 });<!--save the chosen image to-->
			    };
			    }<!--end smaller than quota-->
			   else{
			        safeAlert('<?=$overQuotaNotification?>');	
			        hideLoading();
			   }}else{
			   	    hideLoading();
			    	safeAlert('<?=$maxAttachSizeNotification?>');
			    }
			    }
			})(f);  
			 reader.readAsDataURL(f); 
      }
        $('#attachmentFile').val('');
      }
	function escapeHtml(text) {
	  var map = {
	    '&': '&amp;',
	    '<': '&lt;',
	    '>': '&gt;',
	    '"': '&quot;',
	    "'": '&#039;'
	  };
	
	  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
	}
    function saveUploadData(Data,ImageName,callback)
	{
		 var userID = <?=$uid?>;
		 var attachmentAction ="SAVE";
		 var currentEmailAttachmentLink = $("input[name='chosenUploadedAttachmentLink']").val();
		 if(currentEmailAttachmentLink==""){
		 	currentEmailAttachmentLink = "N";
		 }
		   $.post( "emailSaveAttachment.php",{Data:Data,ImageName:ImageName, userID:userID,currentEmailAttachmentLink:currentEmailAttachmentLink,attachmentAction:attachmentAction},function(data) {
             })
	  .done(function(data) {
	  	if(data==''){
	  		safeAlert('<?=$Lang['Gamma']['App']['UploadFileFailed']?>');
	  	}else{
	  	   if( $("input[name='chosenUploadedAttachmentLink']").val()==""){
  	  	    $("input[name='chosenUploadedAttachmentLink']").val(data);
  	          }
	  		callback();
	  	}
		  })
		  .fail(function() {
		  })
		  .always(function() {
		});  
	}
	function delete_attachment_click(clicked_id){
		var selDiv = document.querySelector("#selectedFiles");
		var newHtml = '';
		var attachFiles=document.getElementsByName("chosenFiles[]");
        var attachFilesLength = attachFiles.length;
        var attachImgSrc= <?=$myAttachmentIconUrl?>;
		var deleteImgSrc= <?=$myCloseIconUrl?>;
        for(var i=0;i<attachFilesLength;i++){
        var currentElement = attachFiles[i];
		var currentID = currentElement.id;
		var currentDeleteImage = "deleteChosenFile_"+currentID;
		
		<!--generate new attachment-->
		if(clicked_id!=currentDeleteImage){
		   var name = currentID;
		   var tempFileName = escapeHtml(name);
		   var imageInput = "<input type=\"hidden\" id =\"" + tempFileName +"\" name=\"chosenFiles[]\"  value=\"" + tempFileName +"\">";
		   var newHtml = newHtml+"<img src=\"" + attachImgSrc + "\">" + name + "<a href=\"#\"  id=\"deleteChosenFile\"  name=\"deleteChosenFile\"   style = \"width: 20px; margin-right: 10px;\" >"+ "<img src=\"" + deleteImgSrc + "\" id=\"deleteChosenFile_" + name +"\" onClick=\"delete_attachment_click(this.id)\">"+"</a>"+imageInput+"<br clear=\"left\"/>";
		}else{
		   delete_attachment_in_server(currentID); 
		}		
        }
        <!--refresh the attachment list-->
        selDiv.innerHTML = newHtml;
	}
	
    function delete_attachment_in_server(deleteFileName){
     
     var userID = <?=$uid?>;
	 var attachmentAction ="DELETE";
	 var currentEmailAttachmentLink = $("input[name='chosenUploadedAttachmentLink']").val();
	  $.post("emailSaveAttachment.php",{userID:userID,currentEmailAttachmentLink:currentEmailAttachmentLink,attachmentAction:attachmentAction,deleteFileName:deleteFileName},function(data) {
             });
    }
    
     function copy_attachment_in_server(){

     var isInitialAttach = <?=$isInitialAttach?>;
     if(isInitialAttach){
         var userID = <?=$uid?>;
		 var attachmentAction ="COPY";
		 var currentEmailAttachmentLink = $("input[name='chosenUploadedAttachmentLink']").val();
		 if(currentEmailAttachmentLink ==""){
		 currentEmailAttachmentLink = "N";
		 }
		if(checkIsGamma()){
			var currentID = '<?=$currentUID?>';	
			var CurUserAddress = '<?=$CurUserAddress?>';
	        var CurUserPassword = '<?=$CurUserPassword?>';
	        var FolderForLink = '<?=$FolderForLink?>';
	        var PartIDList = '<?=$PartIDList?>';
	        $.post( "emailSaveAttachment.php",{attachmentAction:attachmentAction,userID:userID,CurUserAddress:CurUserAddress,CurUserPassword:CurUserPassword,currentID:currentID,FolderForLink:FolderForLink,PartIDList:PartIDList},function(data) {
	             }).done(function(data) {
		  	        $("input[name='chosenUploadedAttachmentLink']").val(data);
			});
		 }<!--end gamma copy attachment into a new folder-->
	    else{
  	       var originalEmailAttachmentLink = '<?=$originalEmailAttachmentLink?>';
		   $.post( "emailSaveAttachment.php",{attachmentAction:attachmentAction,userID:userID,originalEmailAttachmentLink:originalEmailAttachmentLink},function(data) {
             }).done(function(data) {
	  	        $("input[name='chosenUploadedAttachmentLink']").val(data);
		   });	
	    }<!--end imail copy attachment into a new folder-->
      }<!--end need copy attachment into a new folder-->
    }
<!--this function is for initilize each layer button or herf response function and there are three layers to get correspinding emailAddress List-->
<!--each click at each layer will trigger invoke this function-->
<!--the invoke part is trigger from its upper layer-->
function initilizeNewButtons(filterItem,listType) {	
	var searchPickPlace = $('#mySearchPickPlace ul');
	   if(filterItem =='Cc'){	    	
    	searchPickPlace = $('#myCcSearchPickPlace ul');
    	}else if(filterItem =='Bcc'){
    	 searchPickPlace = $('#myBccSearchPickPlace ul');	
    	}	
	
	  if(listType =='layer1'){	
	    
	    if(filterItem =='Cc'){	    	
    	$("div#myCcSearchPickPlace").val('layer1');
    	}else if(filterItem =='Bcc'){
    	 $("div#myBccSearchPickPlace").val('layer1'); 
    	}else{
    	 $("div#mySearchPickPlace").val('layer1');  	
    	}
	    var tOut_layer1 = null;
		var clicked_layer1 = true;
	    searchPickPlace.children('li').on('click', function (e) {
        e.preventDefault();
        }).on('mousedown', function () {
    	tOut_layer1 = setTimeout(function () {
        clicked_layer1 = false;
        }, 200);
    	}).on('mouseup', function () {
       <!--click case-->
       if (clicked_layer1 == true) {
       clearTimeout(tOut_layer1);
	   var selectedTypeDataName = $(this).attr('data-name');
	   if(selectedTypeDataName == 0){
	  	  loadXMLDoc(selectedTypeDataName,"ExternalGroup","","",<?php echo $uid?>,function(obj) {
	      layer3UpdatelistView(filterItem,obj);<!--initilze catid as ExternalGroup,go to layer3 directly-->
	    }); 
	  }
      else if(selectedTypeDataName == 1){
      	  loadXMLDoc(selectedTypeDataName,"ExternalContact","","",<?php echo $uid?>,function(obj) {
          updateCheckAddresslistView(filterItem,obj,1);<!--initilze catid as ExternalContact,go to layer4 directly-->
        }); 
      }
      if(selectedTypeDataName == 2){
      	  loadXMLDoc(selectedTypeDataName,"INTERNAL_GROUP","","",<?php echo $uid?>,function(obj) {
          layer3UpdatelistView(filterItem,obj);<!--initilze catid as INTERNAL_GROUP,go to layer3 directly-->
        }); 
      } else if(selectedTypeDataName ==3){
      	  loadXMLDoc(selectedTypeDataName,"","","",<?php echo $uid?>,function(obj) {
          updatelistView(filterItem,obj);
        }); 
      }  
        } <!--end scroll or click--> 
        else {
    	<!--time out case just scroll-->
        clicked_layer1 = true;
        }
    });<!--end layer1 Filter select function-->
	    }<!--end layer1-->
	  <!--layer2-->
	  else if(listType =='layer2'){
	   var tOut_layer2 = null;
	   var clicked_layer2 = true; 
	   
	   var layer2Back  = $("#layer2backTo");   
	   if(filterItem =='Cc'){
	   	layer2Back  = $("#layer2backCc");
	   }else if(filterItem =='Bcc'){
	   	layer2Back  = $("#layer2backBcc");
	   }
	   <!--Back-->
	   layer2Back.click(function(){
       updateLayer1ListView(filterItem);
       });      
	   searchPickPlace.children('li').on('click', function (e) {
     	 e.preventDefault();
       }).on('mousedown', function () {
       	   tOut_layer2 = setTimeout(function () {
           clicked_layer2 = false;
       }, 200);
      }).on('mouseup', function () {
          if (clicked_layer2 == true) {
         clearTimeout(tOut_layer2);    
          
      var selectedTypeDataName = $(this).attr('data-name');
      var selectedTypeDataLayer1Item = $(this).attr('id');
        if(selectedTypeDataName!='layer2backButton'){
      loadXMLDoc(selectedTypeDataLayer1Item,selectedTypeDataName,"","",<?php echo $uid?>,function(obj) {
      if(obj.IsLastLayerTag == 1){
      	updateCheckAddresslistView(filterItem,obj,0);<!--layer4 is the final email list with checkbox-->
      }else{
      	layer3UpdatelistView(filterItem,obj);
      }
      });}<!--end click case-->              
         	  } else {
        clicked_layer2 = true;
         }
      }); 
	  }<!--end layer2--> 
	  <!--layer3--> 
	else if(listType =='layer3'){
	var tOut_layer3 = null;
	var clicked_layer3 = true;
	var layer3back =  $("#layer3backTo");
	if(filterItem =='Cc'){
	   	layer3back  = $("#layer3backCc");
	}else if(filterItem =='Bcc'){
	    layer3back  = $("#layer3backBcc");
	}
	<!--BACK-->
	layer3back.click(function(){
   	<!--load layer2 list and set buttons' function-->
   	<!--important-->
       	  var selectedTypeDataLayer1Item = $(this).attr('value');
       	   if (selectedTypeDataLayer1Item == 0||selectedTypeDataLayer1Item == 2){
       	   	<!--external group and internal group have no layer2, if the layer1 of current layer3 is 0 or2,then go back to layer1 directly-->
       	     updateLayer1ListView(filterItem); 
       	   }else{
       	   	<!--only applicable for internal contact-->
       	   loadXMLDoc(selectedTypeDataLayer1Item,"","","",<?php echo $uid?>,function(obj) {
           updatelistView(filterItem,obj);
            });   
       	   }<!--end if layer1 equals 2-->
		}); <!--end BACK-->
		<!--CLICK Layer3-->
		searchPickPlace.children('li').on('click', function (e) {
		    e.preventDefault();
		}).on('mousedown', function () {
		    tOut_layer3 = setTimeout(function () {
		    clicked_layer3 = false;
		    }, 200);
		}).on('mouseup', function () {
		    if (clicked_layer3 == true) {
		    clearTimeout(tOut_layer3);		        
		    var currentItemGroupID = $(this).attr('data-name');
		    var IDArray = $(this).attr("id");
		    var res = $(this).attr("id").split(",");
		    var selectedTypeDataLayer1Item =  res[0];  
		    var currentItemCatID =  res[1]; 
		    
		    if(currentItemGroupID!='layer3backButton'){
		      <!--important need catid  there-->
		  loadXMLDoc(selectedTypeDataLayer1Item,currentItemCatID,currentItemGroupID,"",<?php echo $uid?>,function(obj) {
		  updateCheckAddresslistView(filterItem,obj,1);<!--layer4 is the final email list with checkbox-->
		  });
		  }<!--end if not back-->
		  } else {
		    clicked_layer3 = true;
		    }
		  }); <!--end  list click-->	
	}<!--end layer3-->
	<!--layer4-->
	else if(listType =='layer4'){
	 	
	var layer4Back =  $("#layer4BackTo");
	var selectAllEmailAddressInReceiveFilter = $('input#selectAllEmailAddressInReceiveFilterTo');
	var selectArrayInReceiveFilter = $("input[name='selectArrayInReceiveFilterTo']");
	var receiveAddressChoseFinish = $("#receiveAddressChoseFinishTo");
	var filterFieldTags = jQuery_1_0_2('#myReceiverFilterFieldTags');
	
	if(filterItem =='Cc'){
	   	layer4Back  = $("#layer4backCc");
	  	selectAllEmailAddressInReceiveFilter = $('input#selectAllEmailAddressInReceiveFilterCc')
	    selectArrayInReceiveFilter = $("input[name='selectArrayInReceiveFilterCc']")
	    receiveAddressChoseFinish =  $("#receiveAddressChoseFinishCc");
	    filterFieldTags = jQuery_1_0_2('#myCcFilterFieldTags');
	}else if(filterItem =='Bcc'){
	    layer4Back  = $("#layer4BackBcc");
	  	selectAllEmailAddressInReceiveFilter = $('input#selectAllEmailAddressInReceiveFilterBcc')
	    selectArrayInReceiveFilter = $("input[name='selectArrayInReceiveFilterBcc']")
	    receiveAddressChoseFinish =  $("#receiveAddressChoseFinishBcc");
	    filterFieldTags = jQuery_1_0_2('#myBccFilterFieldTags');
	}
	
	<!--BACK-->
	layer4Back.click(function(){
           var currentItemCatID =  $(this).attr("name"); 
           var selectedTypeDataLayer1Item = $(this).attr('value');
            if(selectedTypeDataLayer1Item == 1){
            <!--the external contact item only have two layer that is layer1 and layer4-->
            updateLayer1ListView(filterItem); 
            } else if(currentItemCatID=='NoLayer3'){
            <!--some internal contact has no layer3 that is only have layer1,2,4, so layer4 back to layer2-->
            loadXMLDoc(selectedTypeDataLayer1Item,"","","",<?php echo $uid?>,function(obj) {
            updatelistView(filterItem,obj);
            });
            }else{
           <!--other internal contact lists that has layer3,layer4 back to layer3-->
           loadXMLDoc(selectedTypeDataLayer1Item,currentItemCatID,"","",<?php echo $uid?>,function(obj) {
           layer3UpdatelistView(filterItem,obj);
        });
        }   
		});
	<!--END BACK-->
	<!--Select All-->
	 selectAllEmailAddressInReceiveFilter.change(function() {
     var checked_status = this.checked;
     selectArrayInReceiveFilter.each(function(){ 
      this.checked = checked_status;
      });
      selectArrayInReceiveFilter.checkboxradio("refresh");
	  });
	<!--Finish-->
	receiveAddressChoseFinish.click(function(){
    	showLoading();
        setTimeout(function(){	
      <!--generate the new array-->
	  var receiveAddress = '';
	  var receiveAddressIDs ='';
	  var receiveExternalAddress ='';
	  hideLoading();
	  selectArrayInReceiveFilter.each(function(){ 
	  	var checked_status = this.checked;
	  	if(checked_status){
	  		var checked_email_address =this.value;
	  		var checked_email_uid = this.id;	
	  		if(checkIsGamma()){
	  		filterFieldTags.tagit('createTag', checked_email_address,-1);  		
	  		}else{
	  		filterFieldTags.tagit('createTag', checked_email_address,checked_email_uid);  		
	  		}	
	  	}	 
	  });
	  }, 300,this);
	 updateLayer1ListView(filterItem);
     });<!--end finish button-->	
	 }<!--end layer4-->
   }

    function loadXMLDoc(LayerID,CatID,ChooseGroupID,ChooseUserID,userID,callback)
	{			
	 var parLang = "<?=$parLang?>";
	 $.post( "emailAddressListGenerator.php",{LayerID:LayerID, CatID:CatID, ChooseGroupID:ChooseGroupID, ChooseUserID: ChooseUserID , userID:userID,parLang:parLang },function(data) {
             })
	  .done(function(data) {
		var obj = JSON.parse(data);	
		 callback(obj, CatID,ChooseGroupID,ChooseUserID,userID);
		  })
		  .fail(function() {
		  })
		  .always(function() {
		});
	}
	
	<!--update layer1 and init corresponding buttons-->
	function updateLayer1ListView(filterType){
	    var output = '<?=$firstAddressLayerList?>'; 
        refreshListView(filterType,output);
		initilizeNewButtons(filterType,'layer1');	
	}
	
	<!--update layer2 and init corresponding buttons-->
	function updatelistView(filterType,obj){
	   var output = '';   
       var layer1Item = obj.layerID;
       var layer2backButtonID = "layer2back"+filterType;
       var layer2backButtonDataName = "layer2backButton"
       var backOperationString ='<a href='+ "#" + ' id="'+layer2backButtonID+'" class="ui-btn-left ui-btn ui-btn-inline ui-mini ui-corner-all ui-btn-icon-left ui-icon-carat-l" style ="background-color: #fff; padding-top: 20px;padding-left: 20px;padding-bottom: 20px;padding-right: 20px;left: 5px;border-left-width: 0px;margin-bottom: 0px;margin-top: 0px;margin-right: 0px;border-right-width: 0px;border-top-width: 0px;border-bottom-width: 0px;"></a>';      
	   output +='<li data-name = "'+layer2backButtonDataName+'" style="padding-top:20px;padding-bottom:25px;"><div>'+backOperationString+'</div></li>';  
	   for (i = 0; i < obj.addressArr.length; i++) { 
	   output +='<li data-name = "'+obj.addressArr[i].groupType+'" id ='+layer1Item+' ><a href='+"#"+'>'+ obj.addressArr[i].groupName +'</a></li>';        
	    }
        refreshListView(filterType,output);
		initilizeNewButtons(filterType,'layer2');	
	}
	
	<!--update layer3 and init corresponding buttons-->
	function layer3UpdatelistView(filterType,obj){
	   <!--generate data-->
	   var output = '';   
       var catID = obj.CatID;
       var layer1Item = obj.layerID;<!--to carry the layer1 value that the layer3 belong to-->
       var listItemID = layer1Item+","+catID;
       var layer3backButtonID = "layer3back"+filterType;
       var backOperationString ='<a href='+ "#" + ' id="'+layer3backButtonID+'" value='+layer1Item+' class="ui-btn-left ui-btn ui-btn-inline ui-mini ui-corner-all ui-btn-icon-left ui-icon-carat-l" style ="background-color: #fff; padding-top: 20px;padding-left: 20px;padding-bottom: 20px;padding-right: 20px;left: 5px;border-left-width: 0px;margin-bottom: 0px;margin-top: 0px;margin-right: 0px;border-right-width: 0px;border-top-width: 0px;border-bottom-width: 0px;"></a>';      
	   output +='<li data-name ="layer3backButton" style="padding-top:20px;padding-bottom:25px;"><div>'+backOperationString+'</div></li>';  
	   for (i = 0; i < obj.addressArr.length; i++) { 
	   output +='<li id ="'+listItemID+'" data-name = "'+obj.addressArr[i].groupType+'"><a href='+"#"+'>'+ obj.addressArr[i].groupName +'</a></li>';        
	    }
	    refreshListView(filterType,output);
		initilizeNewButtons(filterType,'layer3');
	}
	<!--update layer4 and init corresponding buttons-->
	function updateCheckAddresslistView(filterType,obj,isFromLayer3){
	<!--generate data-->
	   var output = '';
       var catID = obj.CatID;<!--determine current email adderess's layer2-->
       if(isFromLayer3 ==0){
       	catID = 'NoLayer3';
       }
       var layer1Item = obj.layerID;
       var ChooseGroupID = obj.ChooseGroupID;<!--determine current email adderess's layer3-->
       var layer4backButtonID = "layer4Back"+filterType;
       var selectAllButtonID = "selectAllEmailAddressInReceiveFilter"+filterType;
       var selectselectArray = "selectArrayInReceiveFilter"+filterType;
       var finishButtonID = "receiveAddressChoseFinish"+filterType;
       var backOperationString ='<a href='+ "#" + ' id="'+layer4backButtonID+'" value = '+layer1Item+' name = '+catID+'  class="ui-btn-left ui-btn ui-btn-inline ui-mini ui-corner-all ui-btn-icon-left ui-icon-carat-l" style ="background-color: #fff; padding-top: 20px;padding-left: 20px;padding-bottom: 20px;padding-right: 20px;left: 5px;border-left-width: 0px;margin-bottom: 0px;margin-top: 0px;margin-right: 0px;border-right-width: 0px;border-top-width: 0px;border-bottom-width: 0px;"></a>';
       var finishOperationString = '<a href='+"#"+'  id="'+finishButtonID+'" name="receiveAddressChoseFinish" class="ui-nodisc-icon ui-btn-right ui-btn ui-btn-b ui-btn-inline ui-mini ui-corner-all" style = "background-color:#fff;border-color:#FFFFFF;color:#000;"><?=$Lang['Gamma']['App']['Finish']?></a>';        
       output +='<li style="padding-top:20px;padding-bottom:25px;"><div>'+backOperationString+finishOperationString+'</div></li>';    
       output +='<li><input type = "checkbox" name = "selectTotalArrayInReceiveFilter[]"  id ="'+selectAllButtonID+'">'+"<?=$_SELECT_ALL?>"+'</li>';
      
       for (i = 0; i < obj.addressArr.length; i++) { 
       var currentID = '"'+selectAllButtonID+i+'"';
       var currentCheckBoxID = '';
       if((layer1Item=='2')||(layer1Item=='3'))
       {
       	currentCheckBoxID = obj.addressArr[i].groupName;
       }else{
       	currentCheckBoxID = '-1';
       }
       output +='<li style="padding-top:0px;padding-bottom:0px;"><table style="width:100%;"><tr><td sytle="width:25px;"><input type = "checkbox" name = "'+selectselectArray+'"  id = '+currentCheckBoxID+' class = "custom" value = "'+ obj.addressArr[i].groupType+'" style="margin-bottom:0px;margin-top:5px;margin-right:0px"></td><td><p class="ui-li-text-my-h5">'+ obj.addressArr[i].groupType +'</p></td></tr></table></li>';        
       }
        refreshListView(filterType,output);      
        $('.ui-li-text-my-h5').width(Get_Screen_Width() - 50);
        initilizeNewButtons(filterType,'layer4');
	}
	
	function layer2CallBack(selectedTypeDataName,selectedTypeDataName,filterItem){
	  if(selectedTypeDataName!='layer2backButton'){
      loadXMLDoc(selectedTypeDataLayer1Item,selectedTypeDataName,"","",<?php echo $uid?>,function(obj) {
      layer3UpdatelistView(filterItem,obj);
    });	}
	}
	
	function refreshListView(filterType,output){
		if(filterType=='To'){
		$("#myReceiverFilterAddressList").empty();
		$('#myReceiverFilterAddressList').append(output).listview('refresh');   
		initilizeNewButtons('layer3');		
		}else if(filterType=='Cc'){
		$("#myCcFilterAddressList").empty();
		$('#myCcFilterAddressList').append(output).listview('refresh');   
		}else if(filterType=='Bcc'){
		$("#myBccFilterAddressList").empty();
		$('#myBccFilterAddressList').append(output).listview('refresh');   
		}
	}	
	
	<!--detect one of the three address input text change-->
	function inputChange(event){
      //get the parent list id
      var parentListID =$(event.target).closest( "ul" ).attr('id');
      //get the input text
      var inputText = event.target.value;

      if(parentListID =="myReceiverFilterFieldTags"){
      	autoComplementAddressFunction(inputText,'To',event);
      }else if(parentListID =="myCcFilterFieldTags"){
      	autoComplementAddressFunction(inputText,'Cc',event);
      }else if(parentListID =="myBccFilterFieldTags"){     	
      	autoComplementAddressFunction(inputText,'Bcc',event);
      }<!--end get corresponding list-->
    }        
     
    <!--collapse autocomplete address suggestions to corresponding input table-->
	function autoComplementAddressFunction(inputText,filterType,event){
		
	 var addressText = inputText;
	 
	 if(addressText[addressText.length-1]==';') {
		updateLayer1ListView(filterType);
	 }else{
	 var addressInputArrary = addressText.split(";");
	 var newInputAddressItem = addressInputArrary[addressInputArrary.length - 1];
	 
     if(filterType=='To'){	
     $('fieldset#myReceiverFilterFieldset').collapsible('expand');
     
      $("input#myReceiverLatestInputAddress").val(newInputAddressItem).change();
       var mySearchPickPlaceLabelValue = $("div#mySearchPickPlace").val();	 
		if(mySearchPickPlaceLabelValue !='layer5'){
		<!--initilize the layer5-->   
		$("div#mySearchPickPlace").val('layer5'); 
		newGenerateInternalAndExternalTotalAddressList(filterType,addressText);
		}<!--end initilize the layer5-->
    
    }else if(filterType=='Cc'){
    	
    $('fieldset#myCcFilterFieldset').collapsible('expand');
    
    $("input#myCcLatestInputAddress").val(newInputAddressItem).change();
     var myCcSearchPickPlaceLabelValue = $("div#myCcSearchPickPlace").val();	
      if(myCcSearchPickPlaceLabelValue !='layer5'){
    <!--initilize the cc layer5-->   
    $("div#myCcSearchPickPlace").val('layer5');   
    newGenerateInternalAndExternalTotalAddressList(filterType,addressText);   
    }<!--end initilize the cc layer5-->
    }else if(filterType=='Bcc'){
    	
    $('fieldset#myBccFilterFieldset').collapsible('expand');
    
    $("input#myBccLatestInputAddress").val(newInputAddressItem).change();
    var myBccSearchPickPlaceLabelValue = $("div#myBccSearchPickPlace").val();
    if(myBccSearchPickPlaceLabelValue !='layer5'){
    <!--initilize the bcc layer5-->   
    $("div#myBccSearchPickPlace").val('layer5');
    newGenerateInternalAndExternalTotalAddressList(filterType,addressText);
    }<!--end initilize the bcc layer5-->
    }<!--end check filterType-->
	}<!--end input end check—>
	}
    
    function newGenerateInternalAndExternalTotalAddressList(filterType,addressText){
    	  var uid = <?=$uid?>;
    	  var isAccessInternetMail = <?=$isAccessInternetMail?>;
    	  var isAccessExternetMail = <?=$isAccessExternetMail?>;
    	  var has_webmail = <?=$has_webmail?>;
    	 $.post( "autocomplete_email_address.php",{uid:uid,isAccessInternetMail:isAccessInternetMail,isAccessExternetMail:isAccessExternetMail,has_webmail:has_webmail},function(data) {
    	        var output = data;
    	     	refreshListView(filterType,output);
             })
	  .done(function(data) {
		  });  
    }
    
    
   <!--autocomplete address choosen handler--> 
   function getCurrentEmailAddress(clickherf){
   
   	var receiveAddress = clickherf.getAttribute('value');
	var receiveAddressID = clickherf.getAttribute('id');
   	var parentUlID = $(clickherf).closest('ul').attr('id');
   	
   	var filterType = 'To';
   	var tempFilterFieldTags = jQuery_1_0_2('#myReceiverFilterFieldTags');
   	
   	if(parentUlID == 'myCcFilterAddressList'){   	
	   	filterType = 'Cc';
	   	tempFilterFieldTags = jQuery_1_0_2('#myCcFilterFieldTags');
   	}else if(parentUlID == 'myBccFilterAddressList'){
	   	filterType = 'Bcc';	
	   	tempFilterFieldTags = jQuery_1_0_2('#myBccFilterFieldTags');
   	}
	
    if(checkIsGamma()){
       tempFilterFieldTags.tagit('createTag', receiveAddress,-1);	
    }else{
       tempFilterFieldTags.tagit('createTag', receiveAddress,receiveAddressID);		
    }
	$("input#myReceiverLatestInputAddress").val('');
	$("input#myCcLatestInputAddress").val(''); 
	$("input#myBccLatestInputAddress").val(''); 
	updateLayer1ListView(filterType);	
	}  
    
    <!--Tag settings-->    
    function removeTag(tagID,tagAddress,filterType){
    	
    	var totalExternalAddress =  $("input[name='chosenExternalRecipientAddresses']");
    	var totalAddressIDs =  $("input[name='chosenRecipientIDs']");
    	if(filterType=='Cc'){
	    	totalExternalAddress =  $("input[name='chosenExternalCcAddresses']");
	    	totalAddressIDs =  $("input[name='chosenInternalCCIDs']");
    	}else if(filterType=='Bcc'){
	    	totalExternalAddress =  $("input[name='chosenExternalBCCAddresses']");
	    	totalAddressIDs =  $("input[name='chosenInternalBCCIDs']");
    	}
    	
	    if(tagID =='-1'){
      		var totalToExternalAddress =  totalExternalAddress.val();
      		var afterToTagRemovedValue = totalToExternalAddress.replace(tagAddress+";", "");
	        totalExternalAddress.val(afterToTagRemovedValue);
  		}else if(tagID !=''){
  			 var totalToAddressIDs =  totalAddressIDs.val();
      		 var totalToAddressIDs = ";"+totalToAddressIDs;
      		 var afterToTagRemovedIDs = totalToAddressIDs.replace(";"+tagID+";", ";");
      		 var afterToTagRemovedIDs = afterToTagRemovedIDs.substring(1);
	         totalAddressIDs.val(afterToTagRemovedIDs);	       
        }
    } 
    function checkIsGamma(){
    	var isGamma = <?=$isImailGamma?>;
    	return (isGamma == true);
    }
    
    function safeAlert(alertText){
    	 setTimeout(function() {
				alert(alertText);
              }, 0);
    }
    
    function tagToAddress(){
            isMultipleToAddressesHide = false;
		    showLoading();
		    setTimeout(function(){	
		    var tagToJson = '<?=$tagToJson?>';
	        var tagToArray = JSON.parse(tagToJson);
        for (var i = 0; i < tagToArray.toAddressArr.length; i++) {
	        var tempReceiverAddress =  tagToArray.toAddressArr[i].Receiver;
	        var tempReceiverUID =  tagToArray.toAddressArr[i].UID;
	        jQuery_1_0_2('#myReceiverFilterFieldTags').tagit('createTag', tempReceiverAddress,tempReceiverUID); 
        }
	        hideLoading();
        }, 300,this);
	        jQuery_1_0_2('#myReceiverFilterFieldTags').width(Get_Screen_Width() - 100);    	 
    }
    
    function tagCcAddress(){   	
	var isTagCC = <?=$isTagCC?>;
	if(isTagCC) {
		    showLoading();	  
            setTimeout(function(){	
		    var tagCcJson = '<?=$tagCcJson?>';
            var tagCcArray = JSON.parse(tagCcJson);
        for (var i = 0; i < tagCcArray.ccAddressArr.length; i++) {
            var tempReceiverAddress =  tagCcArray.ccAddressArr[i].Receiver;
            var tempReceiverUID =  tagCcArray.ccAddressArr[i].UID;
            jQuery_1_0_2('#myCcFilterFieldTags').tagit('createTag', tempReceiverAddress,tempReceiverUID); 
        }
            hideLoading();
        }, 300,this);
           jQuery_1_0_2('#myCcFilterFieldTags').width(Get_Screen_Width() - 100);
	}
    }<!--end Cc Tag settings-->
    
    function tagBCcAddress(){   	
	var isTagBCC = <?=$isTagBCC?>;
	if(isTagBCC) {
		    showLoading();	  
            setTimeout(function(){	
		    var tagBCcJson = '<?=$tagBCcJson?>';
            var tagBCcArray = JSON.parse(tagBCcJson);
        for (var i = 0; i < tagBCcArray.bccAddressArr.length; i++) {
            var tempReceiverAddress =  tagBCcArray.bccAddressArr[i].Receiver;
            var tempReceiverUID =  tagBCcArray.bccAddressArr[i].UID;
            jQuery_1_0_2('#myBccFilterFieldTags').tagit('createTag', tempReceiverAddress,tempReceiverUID); 
        }
            hideLoading();
        }, 300,this);
		   jQuery_1_0_2('#myBccFilterFieldTags').width(Get_Screen_Width() - 100);
	}
    }<!--end Cc Tag settings-->
    
    function validateEmail(email) 
	{
	    var re = /\S+@\S+\.\S+/;
	    return re.test(email);
	}
	
	function tagAllAddressBeforeSendOrSave(){
	    var myReceiverFilterFieldTags_tagitInput_untagged = $('input[name="myReceiverFilterFieldTags_tagitInput"]').val();
		var myCcFilterFieldTags_tagitInput_untagged = $('input[name="myCcFilterFieldTags_tagitInput"]').val();
		var myBccFilterFieldTags_tagitInput_untagged = $('input[name="myBccFilterFieldTags_tagitInput"]').val();
		
		<!--open toAryList if fold-->
		if(<?=$isMultipleToAddresses?> && isMultipleToAddressesHide){
		 	var tagToJson = '<?=$tagToJson?>';
	        var tagToArray = JSON.parse(tagToJson);
	        for (var i = 0; i < tagToArray.toAddressArr.length; i++) {
		        var tempReceiverAddress =  tagToArray.toAddressArr[i].Receiver;
		        var tempReceiverUID =  tagToArray.toAddressArr[i].UID;
		        jQuery_1_0_2('#myReceiverFilterFieldTags').tagit('createTag', tempReceiverAddress,tempReceiverUID); 
	        }
		}else{
			if(myReceiverFilterFieldTags_tagitInput_untagged != ""){
				var myReceiverFilterFieldUntaggedAry = new Array();
	            myReceiverFilterFieldUntaggedAry = myReceiverFilterFieldTags_tagitInput_untagged.split(";");
				for(var i=0;i<myReceiverFilterFieldUntaggedAry.length;i++){
					var tempReceiverAddress = myReceiverFilterFieldUntaggedAry[i];
					if(tempReceiverAddress !=""){
						jQuery_1_0_2('#myReceiverFilterFieldTags').tagit('createTag', tempReceiverAddress,-1); 
					}
				}
			}
		}
		
		console.log(isCcNBccAddressesHide);
		
		if(isCcNBccAddressesHide){
				var isTagCC = <?=$isTagCC?>;
				var isTagBCC = <?=$isTagBCC?>;
				
				if(isTagCC) {			           
		           	var tagCcJson = '<?=$tagCcJson?>';
		            var tagCcArray = JSON.parse(tagCcJson);
			        for (var i = 0; i < tagCcArray.ccAddressArr.length; i++) {
			            var tempReceiverAddress =  tagCcArray.ccAddressArr[i].Receiver;
			            var tempReceiverUID =  tagCcArray.ccAddressArr[i].UID;
			            jQuery_1_0_2('#myCcFilterFieldTags').tagit('createTag', tempReceiverAddress,tempReceiverUID); 
			        }
				}
					
				if(isTagBCC) {
			   		var tagBCcJson = '<?=$tagBCcJson?>';
                    var tagBCcArray = JSON.parse(tagBCcJson);
			        for (var i = 0; i < tagBCcArray.bccAddressArr.length; i++) {
			            var tempReceiverAddress =  tagBCcArray.bccAddressArr[i].Receiver;
			            var tempReceiverUID =  tagBCcArray.bccAddressArr[i].UID;
			            jQuery_1_0_2('#myBccFilterFieldTags').tagit('createTag', tempReceiverAddress,tempReceiverUID); 
			        }
				}
		}
		
		if(myCcFilterFieldTags_tagitInput_untagged != ""){
			var myCcFilterFieldUntaggedAry = new Array();
            myCcFilterFieldUntaggedAry = myCcFilterFieldTags_tagitInput_untagged.split(";");
			for(var i=0;i<myCcFilterFieldUntaggedAry.length;i++){
				var tempCcFilterAddress = myCcFilterFieldUntaggedAry[i];
				if(tempCcFilterAddress !=""){
					jQuery_1_0_2('#myCcFilterFieldTags').tagit('createTag', tempCcFilterAddress,-1); 
				}
			}
		}
				
		if(myBccFilterFieldTags_tagitInput_untagged != ""){
			
			var myBccFilterFieldUntaggedAry = new Array();
            myBccFilterFieldUntaggedAry = myBccFilterFieldTags_tagitInput_untagged.split(";");
			for(var i=0;i<myBccFilterFieldUntaggedAry.length;i++){
				var tempBccFilterAddress = myBccFilterFieldUntaggedAry[i];
				if(tempBccFilterAddress !=""){
					jQuery_1_0_2('#myBccFilterFieldTags').tagit('createTag', tempBccFilterAddress,-1); 
				}
			}
		}		
	}
	
	//Collapse CKeditor toolbar to save space
	//var ckEditor = FCKeditorAPI.GetInstance('emailMainBody');
	//ckEditor.on("instanceReady", function() {
	//	FCKeditorAPI.tools.callFunction(80);
	//});
	
	</script>	
<?php
echo $libeClassApp->getAppWebPageInitEnd();
intranet_closedb();
?>
