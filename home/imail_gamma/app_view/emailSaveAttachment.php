<?php
/********************
 * change log:IP
 * 2015-02-09 modify $newDestinationFolderFullPath and $FolderForLink of $attachmentAction=="COPY"&imail_gamma;
 ********************/
$PATH_WRT_ROOT = '../../../';

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libpwm.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$libfilesystem = new libfilesystem();
$attachmentAction = $_POST['attachmentAction'];
$UserID = IntegerSafe($_POST['userID']);
$ul = $_POST['ul'];

intranet_auth();
intranet_opendb();

if($attachmentAction=="DELETE"){
	
	if ($plugin['imail_gamma']) {
		$deletePrePath = "$file_path/file/gamma_mail/u$UserID"."/upload_file/";
	}else{
		$imailAttachmentPath = $_REQUEST['currentEmailAttachmentLink'];
		if(isKIS($ul)){
			$deletePrePath = "$file_path/file/mail/attachments/". "u$UserID/".$imailAttachmentPath."/";	
		}else{
			$deletePrePath = "$file_path/file/mail/". "u$UserID/".$imailAttachmentPath."/";	
		}
	}
	    $deleteFileName = $_REQUEST['deleteFileName'];
	    $deleteFileNameAry = array_filter(explode(";",$deleteFileName));
	    $deleteSucess = false;
	    if(($UserID!=null)&&($UserID!="")){
	    	for($i=0;$i<count($deleteFileNameAry);$i++){
	    	$deleteFullPath = $deletePrePath.$deleteFileNameAry[$i];
	    	$deleteSucess = $libfilesystem->lfs_remove($deleteFullPath);	
	        }	
	    }
	echo $deleteSucess;
}else if($attachmentAction=="COPY"){

	//A new path
	$UserID = $_REQUEST['userID'];
	$imailAttachmentPathGenerator = session_id().".".time();
	$fileWriteSuccess = false;
	if ($plugin['imail_gamma']) {
	$CurUserAddress = $_REQUEST['CurUserAddress'];
	$CurUserPassword = $_REQUEST['CurUserPassword'];
	$currentUID = $_REQUEST['currentID'];
	$FolderForLink = $_REQUEST['FolderForLink'];
	if($FolderForLink!="INBOX"){
	$FolderForLink = "INBOX.".$FolderForLink;
    }
    $PartIDList = $_REQUEST['PartIDList'];
    $PartIDListArrays = explode(",", $PartIDList);
    $PartIDListArrays = array_filter($PartIDListArrays);
    $IMapCache = new imap_cache_agent($CurUserAddress,$CurUserPassword,'');
    $newDestinationFolderFullPath = "$file_path/file/gamma_mail/". "u$UserID/upload_file";
	if (!file_exists($newDestinationFolderFullPath))
	  {
	  	 mkdir($newDestinationFolderFullPath, 0777, true);
	  }
    $IMap = new imap_gamma(false,$CurUserAddress, $CurUserPassword);
    $FolderForLink = urldecode($IMap->decodeFolderName($FolderForLink));
    for($__i = 0;$__i<count($PartIDListArrays);$__i++){    	
    $Attachment = $IMapCache->Get_Attachment($PartIDListArrays[$__i],$currentUID,$FolderForLink);	
    $filepath = $newDestinationFolderFullPath."/".$Attachment['FileName'];
    $fileWriteSuccess = $libfilesystem->file_write($Attachment['Content'], $filepath);
    }#end copy to new path
	}else{
		if(isKIS($ul)){
		    $originalEmailAttachmentFullPath = "$file_path/file/mail/attachments/".$_REQUEST['originalEmailAttachmentLink'];
		    $newDestinationFolderFullPath = "$file_path/file/mail/attachments/". "u$UserID/".$imailAttachmentPathGenerator;
		}else{
		    $originalEmailAttachmentFullPath = "$file_path/file/mail/".$_REQUEST['originalEmailAttachmentLink'];
		    $newDestinationFolderFullPath = "$file_path/file/mail/". "u$UserID/".$imailAttachmentPathGenerator;
    	}

    if (!file_exists($newDestinationFolderFullPath))
    {
  	 mkdir($newDestinationFolderFullPath, 0777, true);
    }
    $row = $libfilesystem->return_folderlist($originalEmailAttachmentFullPath);
       for($i=sizeof($row)-1; $i>=0; $i--){
       	  $file = $row[$i];
       	  if($file=="." || $file=="..") continue;
          $fileWriteSuccess = $libfilesystem->item_copy($file, $newDestinationFolderFullPath);
       }

	}    

	$currentAttachmentFilePath = $imailAttachmentPathGenerator;
	if($fileWriteSuccess){
		echo $currentAttachmentFilePath;	
	}else{
		echo 'N';
	}	
}else{
	$currentEncodedFile = $_REQUEST['Data'];
    $name = $_REQUEST['ImageName'];
    $currentEmailAttachmentLink = $_REQUEST['currentEmailAttachmentLink'];

if ($plugin['imail_gamma']) {
 $filepathDir = "$file_path/file/gamma_mail/u$UserID"."/upload_file";
}else{
	if($currentEmailAttachmentLink=="N"||$currentEmailAttachmentLink==""){
	 $imailAttachmentPathGenerator = session_id().".".time();	
	}else{
	$imailAttachmentPathGenerator=$currentEmailAttachmentLink;
	}
	if(isKIS($ul)){
			 $filepathDir = "$file_path/file/mail/attachments/". "u$UserID/".$imailAttachmentPathGenerator;		
	}else{
		 $filepathDir = "$file_path/file/mail/". "u$UserID/".$imailAttachmentPathGenerator;
	}
}
 
   if (!file_exists($filepathDir))
	  {
	  	 mkdir($filepathDir, 0777, true);
	  }
		  
	if (($pos = strpos($currentEncodedFile, ",")) !== FALSE) { 
     $currentEncodedFile = substr($currentEncodedFile, $pos+1); 
     }else{
     	echo '';
     }	  
   $currentFileData = base64_decode($currentEncodedFile);  
   $currentFileDataSize = strlen($currentFileData);
   $filepath = $filepathDir."/".$name;
   $fileWriteSuccess = $libfilesystem->file_write($currentFileData, $filepath);
   	if($fileWriteSuccess == 1){
   		if ($plugin['imail_gamma']) {
  			$currentAttachmentFilePath = $filepath;  			
  			}else{
  			$currentAttachmentFilePath = $imailAttachmentPathGenerator;
  			}
  		}
  		else{
  			$currentAttachmentFilePath='';
  		}

echo $currentAttachmentFilePath;
	
	
}
?>