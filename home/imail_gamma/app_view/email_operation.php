<?php
//IP2.5
// Editing by 
/*
 * 2020-11-05 (Ray): Add $_SESSION['SSV_EMAIL_LOGIN'], $_SESSION['SSV_EMAIL_PASSWORD']
 * 2020-08-21 (Henry) : add para isFromApp = true to call Build_Mime_Text
 * 2020-01-23 (Sam): allowing Android devices to save HTML email content
 * 2019-09-06 (Ray): iMail $filepathDir assign default path for draft
 * 2017-03-23 (Carlos): Update new mail counter for sender and receivers.
 */
$PATH_WRT_ROOT = '../../../';

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libpwm.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

//$uid = IntegerSafe($_GET['uid']);
//
//if($_SESSION['UserID']==''){
//$_SESSION['UserID'] = $uid;
//}

intranet_auth();
intranet_opendb();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
		<title>email list</title>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
		<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
		<script language="javascript">
		</script>
	</head>
	<body>	
	<div data-role="page">	
<?

$linterface = new interface_html();
$libDb = new libdb(); 
$libeClassApp = new libeClassApp(); 
$libpwm= new libpwm();
$libfilesystem = new libfilesystem();
$libUser = new libuser($_SESSION['UserID']);
$folderAssoAry ="";
$defaultFolder = array();
$personalFolder = array();

$ul = $libUser->UserLogin;
$token = $libeClassApp->getUrlToken($_SESSION['UserID'], $ul);

//$token = $_GET['token'];
//$ul = $_GET['ul'];
//$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
//if (!$isTokenValid) {
//	die($Lang['Gamma']['App']['NoAccessRight']);
//}

$uid = $_SESSION['UserID'];

    // account and folder basic verification
    $ContentIMailOperation = $_GET["IMailOperation"];
    $ContentMailAction = $_GET['MailAction'];
    $thisFolder = $_GET["TargetFolderName"];
    $currentUID = IntegerSafe($_GET["CurrentUID"]);
    $parLang = $_GET['parLang'];
    //email info
    $Subject = stripslashes($_POST["Subject"]);
    $Body =$_POST["emailMainBody"];
    
    $To = $_POST["chosenExternalRecipientAddresses"];
    $PreToFilterArray = explode(";",$To);
    $Cc = $_POST["chosenExternalCcAddresses"]; 
    $PreCcFilterArray = explode(";",$Cc);
    $Bcc = $_POST["chosenExternalBCCAddresses"];     
    $PreBccFilterArray = explode(";",$Bcc);
    $curUserId = $uid;
    if ($curUserId == '') {
	$UserID = $uid;
    }
	$ImportantFlag = $_POST['ImportantFlag'];
	$IsNotification = $_POST['IsNotification'];
//imail gamma case
if ($plugin['imail_gamma']) {	 	
	 $passwordAry = $libpwm->getData($uid);
	foreach((array)$passwordAry as $_folderType => $_folderKey){
		if($_folderType==$uid)$password=$_folderKey;
	}

    $sql = "Select ImapUserEmail From INTRANET_USER Where UserID = '".$curUserId."'";
	$dataAry = $libDb->returnResultSet($sql);

	if(isset($_SESSION['SSV_EMAIL_LOGIN']) && isset($_SESSION['SSV_EMAIL_PASSWORD'])) {
		$iMapEmail = $_SESSION['SSV_EMAIL_LOGIN'];
		$iMapPwd = $_SESSION['SSV_EMAIL_PASSWORD'];
	} else {
		$iMapEmail = $dataAry[0]['ImapUserEmail'];
		$iMapPwd = $password;
	}
	
    $IMap = new imap_gamma(false,$iMapEmail, $iMapPwd);
    $IMap->IMapCache = new imap_cache_agent($iMapEmail, $iMapPwd);
    
    $From = $IMap ->CurUserAddress;
    $uidAraycount = count($chosenFiles);

    $personal_path = "$file_path/file/gamma_mail/u$UserID";
    $filepathDir = $personal_path."/upload_file";
    
	for($i=0; $i<$uidAraycount;$i++)	{
		$currentAttachment = $chosenFiles[$i];
        $filepath = $filepathDir."/".$currentAttachment;  		 	
  		$currentAttachmentFilePath = $filepath;
		
		// find the , get the base64ed data. base64_decode(data).  $filepath = $personal_path."/".$myArray[1]; $libfilesystem->file_write(data, $filepath)// $libfilesystem->get_file_basename(filepath)			
		$myAttachmentFile[$i][1]  = $currentAttachment ;
	    $myAttachmentFile[$i][2]  =  $currentAttachmentFilePath;
	}    
	$ToArrayEmails = array(); // use for checking duplication
	$CcArrayEmails = array(); // use for checking duplication
	$BccArrayEmails = array(); // use for checking duplication
	$Preference = $IMap->Get_Preference($From);

	if($To != ''){
		
	foreach ($PreToFilterArray as $Key => $ToEmail) {
		$ToEmail = trim($ToEmail);
		if ($ToEmail != '') {
		     if(strstr($ToEmail,"<")&&strstr($ToEmail,">")){
				$EmailAddr = substr($ToEmail,strrpos($ToEmail,"<")+1,strrpos($ToEmail,">")-strrpos($ToEmail,"<")-1);
				$ToEmail = substr($ToEmail,0,strrpos($ToEmail,">")+1);
			}else{
				$EmailAddr = $ToEmail;
			}
			
		    if(!isset($ToArrayEmails[$EmailAddr])){ // set if not duplicated recipient
				$ToArrayEmails[$EmailAddr] = $ToEmail;
				   if (!stristr($ToEmail,'@')) {
					$EmptyEmail[] = str_replace('\"','',substr_replace(trim($ToEmail), '', stripos(trim($ToEmail),'<')));
				} else {
					$ToArray[] = $ToEmail;
				}
				$ReceiptEmailList[] = $EmailAddr;
		    }
		}	
	}}
	
	if($Cc != ''){
		
	foreach ($PreCcFilterArray as $Key => $CcEmail) {
		$CcEmail = trim($CcEmail);
		if ($CcEmail != '') {
		     if(strstr($CcEmail,"<")&&strstr($CcEmail,">")){
				$CcEmailAddr = substr($CcEmail,strrpos($CcEmail,"<")+1,strrpos($CcEmail,">")-strrpos($CcEmail,"<")-1);
				$CcEmail = substr($CcEmail,0,strrpos($CcEmail,">")+1);
			}else{
				$CcEmailAddr = $CcEmail;
			}
			
		    if(!isset($CcArrayEmails[$CcEmailAddr])){ // set if not duplicated recipient
				$CcArrayEmails[$CcEmailAddr] = $CcEmail;
				   if (!stristr($CcEmail,'@')) {
//					$EmptyEmail[] = str_replace('\"','',substr_replace(trim($CcEmail), '', stripos(trim($CcEmail),'<')));
				} else {
					$CCArray[] = $CcEmail;
				}
//				$ReceiptEmailList[] = $EmailAddr;
		    }
		}	
	}}
	
		if($Bcc!=''){
		foreach ($PreBccFilterArray as $Key => $BCCEmailValue) {
		$BCCEmailValue = trim($BCCEmailValue);
			if(strstr($BCCEmailValue,"<")&&strstr($BCCEmailValue,">")){
				$BCCEmailAddr = substr($BCCEmailValue,strrpos($BCCEmailValue,"<")+1,strrpos($BCCEmailValue,">")-strrpos($BCCEmailValue,"<")-1);
				$BCCEmailValue = substr($BCCEmailValue,0,strrpos($BCCEmailValue,">")+1);
			}else{
				$BCCEmailAddr = $BCCEmailValue;
			}
			  
		    if(!isset($BccArrayEmails[$BCCEmailAddr])){ // set if not duplicated recipient
				$BccArrayEmails[$BCCEmailAddr] = $BCCEmailValue;
				   if (stristr($BCCEmailValue,'@')) {
					$BCCArray[] = $BCCEmailValue;	
				}			            
		    }
		  }	
		}

	$XeClassMailID = "u$UserID-".time(); // global var used in Build_Mime_Text()
    $MaxNumOfRecipientPerTime = !isset($SYS_CONFIG['Mail']['MaxNumMailsPerBatch'])?200:$SYS_CONFIG['Mail']['MaxNumMailsPerBatch'];
    $SendMailBySplitInBatch = false;	
    
     //if($userBrowser->platform=="Andriod"){
     // $Body = str_replace("&nbsp;"," ",$Body);
     // $Body = str_replace(array("<br>", "<br />", "<BR>", "<BR />"),"\n",$Body);
     // $Body = strip_tags($Body);
     //}else{
     //	 $Body = str_replace("\n", "<br>", $Body);
     //	 $Body = str_replace('\"','&quot;',$Body);   
     //}
    $MimeMessage = $IMap->Build_Mime_Text($Subject, $Body, $From, $ToArray, $CCArray, $BCCArray, $myAttachmentFile, $ImportantFlag, true, $Preference, array(), true);
     if($ContentIMailOperation=='Send'){
     	 
        $Result['SendMail'] = mail($MimeMessage['to'], $MimeMessage['subject'], $MimeMessage['body'], $MimeMessage['headers2'], "-f $From");
        
        if (!$Result['SendMail']) {
	         $Result['SaveDraft'] = $IMap->saveDraftCopy($MimeMessage['FullMimeMsg']);}
        else {
	    // save a copy in Sent folder
	        $Result['SaveSendCopy'] = $IMap->saveSentCopy($MimeMessage['FullMimeMsg']);
	        if (($MailAction == "Reply" || $MailAction == "ReplyAll") && $Folder != "") {	
		         $IMap->Set_Answered($Folder,$ActionUID);
		    }
	        else if ($MailAction == "Forward" && $Folder != "") {	
		         $IMap->Set_Forwarded($Folder,$ActionUID);
		    }
        }
        	if($IsNotification=='1' && $Result['SaveSendCopy']){
				preg_match("/.*X-eClass-MailID:(.*)"."(".chr(10)."|".$IMap->MimeLineBreak.").*/",$MimeMessage['headers2'],$reg_found);
				$eClassMailID = trim($reg_found[1]);
				if($eClassMailID != '') $IMap->InsertMailReceiptByMailAddresses($eClassMailID,array_unique($ReceiptEmailList));
			}
        
     }
     else{   
     	//save the composed email to draft
     	$Result['SaveDraft'] = $IMap->saveDraftCopy($MimeMessage['FullMimeMsg']);
     }     
       # delete the draft mail if is draft
        if(($thisFolder=='Drafts')&&($currentUID!="")){
			if(!empty($_SESSION["UIDChange"])){
		    $key = array_search($currentUID, $_SESSION["UIDChange"]);
			if($key!== false)
				unset($_SESSION["UIDChange"][$key]);
		     }
		     $Result['DeleteMail'] = $IMap->deleteMail($IMap->DraftFolder, $currentUID);  	
        }
    //detele attachment temp file
	  for($j= 0;$j<count($myAttachmentFile);$j++){
	  	$libfilesystem->lfs_remove($myAttachmentFile[$j][2]);
	  }
    $FolderForLink = urldecode($IMap->decodeFolderName($thisFolder));
    $FolderForLink = urlencode($IMap->encodeFolderName(stripslashes($FolderForLink)));	 
    }//end gamma
	//imail case
	 else{	 	
	include_once($PATH_WRT_ROOT."includes/libwebmail.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
	$lwebmail = new libwebmail();
	$li = new libcampusmail();
	
	$new_mail_counter_exist = method_exists($li,'UpdateCampusMailNewMailCount');
	
	//internal accounts
	$chosenRecipientIDs = $_POST["chosenRecipientIDs"];	
    $chosenInternalCCIDs = $_POST["chosenInternalCCIDs"];
    $chosenInternalBCCIDs = $_POST["chosenInternalBCCIDs"];	
    
    $to_address_list = $To;
    $cc_address_list = $Cc;
    $bcc_address_list = $Bcc;
    $PreToArray = $PreToFilterArray;
    $PreCcArray = $PreCcFilterArray;
    $PreBccArray = $PreBccFilterArray;
    
    //generate the sql insert part for external to, cc and bcc rows
    if(count($extraInputEmailToAddress)>0){
     foreach ($extraInputEmailToAddress as $Key => $ToExtraEmail) {
     	$to_address_list = $to_address_list.$ToExtraEmail.";";
     }	
    }
    if(count($extraInputEmailCcAddress)>0){
       foreach ($extraInputEmailCcAddress as $Key => $CcExtraEmail) {
     	$cc_address_list = $cc_address_list.$CcExtraEmail.";";
     }
    }
    if(count($extraInputEmailBccAddress)>0){
       foreach ($extraInputEmailBccAddress as $Key => $BccExtraEmail) {
     	$bcc_address_list = $bcc_address_list.$BccExtraEmail.";";
     }	
    }
	 	
    //get To internal user IDs  $chosenRecipientIDArr
    $chosenRecipientIDs_All = array_filter(explode(";",$chosenRecipientIDs));
    $chosenInternalCCIDs_All = array_filter(explode(";",$chosenInternalCCIDs));
    $chosenInternalBCCIDs_All = array_filter(explode(";",$chosenInternalBCCIDs));
    //internal save
    $recipientIDMarkArray = "'";
    $InternalCCMarkArray = "'";
    $InternalBCCMarkArray ="'";
    $chosenRecipientIDs_AllCounter = count($chosenRecipientIDs_All);
    $chosenInternalCCIDs_AllCounter = count($chosenInternalCCIDs_All);
    $chosenInternalBCCIDs_AllCounter = count($chosenInternalBCCIDs_All);
    if($chosenRecipientIDs_AllCounter>0){
        for($j= 0;$j<$chosenRecipientIDs_AllCounter-1;$j++){
    	$recipientIDMarkArray =$recipientIDMarkArray."U".$chosenRecipientIDs_All[$j].",";
         }  
        $recipientIDMarkArray =$recipientIDMarkArray."U".$chosenRecipientIDs_All[$chosenRecipientIDs_AllCounter-1]."'";
    }else{
    	$recipientIDMarkArray=$recipientIDMarkArray."'";
    }

    if($chosenInternalCCIDs_AllCounter>0){
    	   for($j= 0;$j<$chosenInternalCCIDs_AllCounter-1;$j++){
    	$InternalCCMarkArray =$InternalCCMarkArray."U".$chosenInternalCCIDs_All[$j].",";
         }  
        $InternalCCMarkArray =$InternalCCMarkArray."U".$chosenInternalCCIDs_All[$chosenInternalCCIDs_AllCounter-1]."'";
    }else{
    	$InternalCCMarkArray=$InternalCCMarkArray."'";
    }
     if($chosenInternalBCCIDs_AllCounter>0){    	
      for($j= 0;$j<$chosenInternalBCCIDs_AllCounter-1;$j++){
    	$InternalBCCMarkArray =$InternalBCCMarkArray."U".$chosenInternalBCCIDs_All[$j].",";
         }  
        $InternalBCCMarkArray =$InternalBCCMarkArray."U".$chosenInternalBCCIDs_All[$chosenInternalBCCIDs_AllCounter-1]."'";
     }else{
     	$InternalBCCMarkArray=$InternalBCCMarkArray."'";
     }
	$ToArrayEmails = array(); // used for checking duplication
	$CcArrayEmails = array(); // used for checking duplication
	$BccArrayEmails = array(); // used for checking duplication
	//final sended  external email address array
	$ToArray = array();
	$CCArray = array();
	$BCCArray = array();
    //get To external user address list array $ToArray
	if(count($PreToArray) >0){
	foreach ($PreToArray as $Key => $ToEmail) {
		$ToEmail = trim($ToEmail);
		if ($ToEmail != '') {
		     if(strstr($ToEmail,"<")&&strstr($ToEmail,">")){
				$EmailAddr = substr($ToEmail,strrpos($ToEmail,"<")+1,strrpos($ToEmail,">")-strrpos($ToEmail,"<")-1);
				$ToEmail = substr($ToEmail,0,strrpos($ToEmail,">")+1);
			}else{
				$EmailAddr = $ToEmail;
			}
		    if(!isset($ToArrayEmails[$EmailAddr])){ // set if not duplicated recipient
				$ToArrayEmails[$EmailAddr] = $ToEmail;
				   if (!stristr($ToEmail,'@')) {
					$EmptyEmail[] = str_replace('\"','',substr_replace(trim($ToEmail), '', stripos(trim($ToEmail),'<')));
				} else {
					$ToArray[] = $ToEmail;					
				}
		    }
		}	
	}
	}
	if(count($PreCcArray)>0){
		foreach ($PreCcArray as $Key => $CcEmail) {
		$CcEmail = trim($CcEmail);
		if ($CcEmail != '') {
		     if(strstr($CcEmail,"<")&&strstr($CcEmail,">")){
				$CcEmailAddr = substr($CcEmail,strrpos($CcEmail,"<")+1,strrpos($CcEmail,">")-strrpos($CcEmail,"<")-1);
				$CcEmail = substr($CcEmail,0,strrpos($CcEmail,">")+1);
			}else{
				$CcEmailAddr = $CcEmail;
			}
			
		    if(!isset($CcArrayEmails[$CcEmailAddr])){ // set if not duplicated recipient
				$CcArrayEmails[$CcEmailAddr] = $CcEmail;
				   if (stristr($CcEmail,'@')) {
				   	$CCArray[] = $CcEmail;
				}
		    }
		}		
	}
	
	if(count($PreBccArray)>0){
		foreach ($PreBccArray as $Key => $BCCEmailValue) {
		$BCCEmailValue = trim($BCCEmailValue);
			if(strstr($BCCEmailValue,"<")&&strstr($BCCEmailValue,">")){
				$BCCEmailAddr = substr($BCCEmailValue,strrpos($BCCEmailValue,"<")+1,strrpos($BCCEmailValue,">")-strrpos($BCCEmailValue,"<")-1);
				$BCCEmailValue = substr($BCCEmailValue,0,strrpos($BCCEmailValue,">")+1);
			}else{
				$BCCEmailAddr = $BCCEmailValue;
			}
			  
		    if(!isset($BccArrayEmails[$BCCEmailAddr])){ // set if not duplicated recipient
				$BccArrayEmails[$BCCEmailAddr] = $BCCEmailValue;
				   if (stristr($BCCEmailValue,'@')) {
					$BCCArray[] = $BCCEmailValue;	
				}			            
		    }
		  }	
		}		
	}
	// set attachment
    $chosenUploadedAttachmentLink = $_POST['chosenUploadedAttachmentLink'];
    if($chosenUploadedAttachmentLink!=""){
      $filepathDir = "u".$uid."/".$chosenUploadedAttachmentLink;	
      if(isKIS()){
      	 $attachmentFullPath = $intranet_root."/file/mail/attachments/".$filepathDir;      	      	
      }else{
      	 $attachmentFullPath = $intranet_root."/file/mail/".$filepathDir;      	
      }
    }else{
		$composeFolder = session_id().".".time();
		$userFolder = $lwebmail->getNextFreeUserFolder($uid, false);
		$composeFolder = "$userFolder/$composeFolder";
     $filepathDir = $composeFolder;
     $attachmentFullPath ="";
    }
    $AttachmentNameArray = $libfilesystem->return_files($attachmentFullPath);
    $attachment_file_totalSize = 0;
    for($attachment_file_i=sizeof($AttachmentNameArray)-1; $attachment_file_i>=0; $attachment_file_i--){
   	  $attachment_file_row = $AttachmentNameArray[$attachment_file_i];
   	  if($attachment_file_row=="." || $attachment_file_row=="..") continue;
   	   $attachment_file_totalSize +=  filesize($attachment_file_row);
    }
    $attachment_file_totalSize = round($attachment_file_totalSize/1024);
    if($AttachmentNameArray!=null&&count($AttachmentNameArray)>0){
    	$IsAttachment = 1;
    }else{
    	$IsAttachment = 0;
    }
	//for send external emails info
	$email_subj = stripslashes($Subject);
    $email_msg = stripslashes($Body);    
    
    $sql = "Select a.UserLogin, ".getNameFieldByLang("a.")." AS UserName From INTRANET_USER As a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE b.ACL IN (1,3) AND a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail <> '' AND a.UserID IN ($uid)";
    $senderInfo = $li->returnArray($sql,2);
    $email_return_path = '';
    if(count($senderInfo)){
    $sender_name = 	$senderInfo[0]['UserName'];
    $login_domain = $lwebmail->login_domain;
    $email_return_path = $senderInfo[0]['UserLogin']."@".$login_domain;
    $emailheader_from = $sender_name ." <".$email_return_path.">";
    }
     if($ContentIMailOperation=='Send'){
     	 //step 1:send the external composed email
          $exmail_success = false;
     	  $exmail_success = $lwebmail->SendMail($email_subj,$email_msg,$emailheader_from,$ToArray,$CCArray,$BCCArray,$attachmentFullPath,'',$email_return_path,$email_return_path,null,false); 
       
		  $receiverTotal =array_unique(array_merge($chosenRecipientIDs_All,$chosenInternalCCIDs_All,$chosenInternalBCCIDs_All));     
        //step 2:save send mail for current account
        if($exmail_success||(count($receiverTotal)>0)){
	        //save email copy in send folder (0)
	        $sql = "
				 INSERT INTO INTRANET_CAMPUSMAIL (
				      UserID, UserFolderID,SenderID,SenderEmail, RecipientID,InternalCC,InternalBCC,
				      ExternalTo,ExternalCC,ExternalBCC,
				      Subject,Message,Attachment,IsAttachment,IsImportant,IsNotification,AttachmentSize,MailType,DateInput,DateModified,DateInFolder)
		         VALUES (
		          '$uid','0', '$uid', '$email_return_path',$recipientIDMarkArray,$InternalCCMarkArray,$InternalBCCMarkArray,
		          '$to_address_list','$cc_address_list','$bcc_address_list','$email_subj','$email_msg','$filepathDir','$IsAttachment','$ImportantFlag','$IsNotification','$attachment_file_totalSize','1',now(),now(),now())";
	        $success = $libDb->db_db_query($sql);	
            if ($success) {
           	$CampusMailID = $libDb->db_insert_id();
           	
           		if($new_mail_counter_exist){
           			// increment one for new mail counter to Outbox
					$li->UpdateCampusMailNewMailCount($uid, 0, 1);
           		}
            }
	    $sql = "select UserID,".getNameFieldByLang()." as name FROM INTRANET_USER  Where UserID IN (". implode(",", $receiverTotal).")";
	    $nameFieldArray = $libDb->returnArray($sql); 
	    $nameFieldHashArray = array();
	    for($j=0;$j<count($nameFieldArray);$j++){
	    	$nameFieldHashArray[$nameFieldArray[$j]['UserID']] = $nameFieldArray[$j];
	    }
	    //step 3:insert every internal email sent
	    
	    for($i=0;$i<count($receiverTotal);$i++){
	        $currentReceiverUserID = $receiverTotal[$i];        
	        //save current email in each receiver's inbox foler(2)
	        $sql = "
					 INSERT INTO INTRANET_CAMPUSMAIL (
					      CampusMailFromID,UserID, UserFolderID,SenderID,SenderEmail,RecipientID,InternalCC,InternalBCC,
					      ExternalTo,ExternalCC,ExternalBCC,
					      Subject,Message,Attachment,IsAttachment,IsImportant,IsNotification,AttachmentSize,MailType,DateInput,DateModified,DateInFolder)
			        VALUES (
			          '$CampusMailID','$currentReceiverUserID', '2','$uid','$email_return_path', $recipientIDMarkArray,$InternalCCMarkArray,$InternalBCCMarkArray,
			          '$to_address_list','$cc_address_list','$bcc_address_list','$email_subj','$email_msg','$filepathDir','$IsAttachment','$ImportantFlag','$IsNotification','$attachment_file_totalSize','1',now(),now(),now())";
	         $libDb->db_db_query($sql);
	        if ($success) {
             	$CampusMailIDReceiver[$i] = $libDb->db_insert_id();
            }
	         $tempUserName = $nameFieldHashArray[$currentReceiverUserID]['name'];
	         $sql = "INSERT INTO INTRANET_CAMPUSMAIL_REPLY (CampusMailID, UserID, UserName,IsRead,DateInput,DateModified) VALUES('$CampusMailID','$currentReceiverUserID','$tempUserName',0,now(),now())"; 
	         $libDb->db_db_query($sql);
	         
	         if($new_mail_counter_exist){
	         	// increment inbox by 1 new mail for each receiver
          		$li->UpdateCampusMailNewMailCount($currentReceiverUserID, 2, 1);
	         }
	     }  
	             
	    //setp 4 delete current email if it exist in draft folder
	    if($thisFolder=='Drafts'){
		    	if($currentUID!=""){
		    		//remove draft attachement foler first
		    		$sql = "select Attachment,IsAttachment from INTRANET_CAMPUSMAIL where UserID='".$uid."' AND CampusMailID = '".$currentUID."' AND UserFolderID=1";
		    		$removedDraftAttachAry =  $libDb->returnArray($sql);
	    		       if(count($removedDraftAttachAry)){
		    				$removedDraftIsAttachment = $removedDraftAttachAry[0]['IsAttachment'];
		    		        $removedDraftAttachment = $removedDraftAttachAry[0]['Attachment'];
		    		        if(($removedDraftIsAttachment==1)&&(strpos($removedDraftAttachment, 'u'.$uid) !== false)){
		    		            if(isKIS()){
		    		            	$deleteFullPath = $intranet_root."/file/mail/attachments/".$removedDraftAttachment;
                                }else{
		    		            	$deleteFullPath = $intranet_root."/file/mail/".$removedDraftAttachment;
                                }
		    		        	$libfilesystem->lfs_remove($deleteFullPath);
		    		        }
	                	}
		    		$sql = "delete from INTRANET_CAMPUSMAIL where UserID='".$uid."' AND CampusMailID = '".$currentUID."' AND UserFolderID=1";
		    		$libDb->db_db_query($sql);
		    		
		    		if($new_mail_counter_exist){
		    			$li->UpdateCampusMailNewMailCount($uid, 1, -1); // decrement draft box by one
		    		}	
		    	}
		  }//end delete draft
        }else{
        //send external neither success nor has no internal.then save email copy as a draft(1)       
         $sql = "
				 INSERT INTO INTRANET_CAMPUSMAIL (
				      UserID, UserFolderID,SenderID,SenderEmail,RecipientID,InternalCC,InternalBCC,
				      ExternalTo,ExternalCC,ExternalBCC,
				      Subject,Message,Attachment,IsAttachment,IsImportant,IsNotification,AttachmentSize,MailType,DateInput,DateModified,DateInFolder)
		        VALUES (
		          '$uid', '1','$uid','$email_return_path', $recipientIDMarkArray,$InternalCCMarkArray,$InternalBCCMarkArray,
		          '$to_address_list','$cc_address_list','$bcc_address_list','$email_subj','$email_msg','$filepathDir','$IsAttachment','$ImportantFlag','$IsNotification','$attachment_file_totalSize','1',now(),now(),now())";
		           $libDb->db_db_query($sql);
		           
		        if($new_mail_counter_exist){
		    		$li->UpdateCampusMailNewMailCount($uid, 1, 1); // increment draft box by one
		    	}
         }     
     }
     else{
     	if(($thisFolder=='Drafts')&&($currentUID!="")){
        $sql="
	        UPDATE INTRANET_CAMPUSMAIL SET RecipientID= $recipientIDMarkArray,InternalCC=$InternalCCMarkArray,InternalBCC=$InternalBCCMarkArray
	        ,ExternalTo='$to_address_list',ExternalCC='$cc_address_list',ExternalBCC='$bcc_address_list'
	        ,Subject='$email_subj',Message='$email_msg',Attachment='$filepathDir'
	        ,IsAttachment='$IsAttachment',IsImportant='$ImportantFlag',IsNotification='$IsNotification',AttachmentSize='$attachment_file_totalSize',MailType='1',DateModified =now()  WHERE CampusMailID = '".$currentUID."'";	
        $success = $libDb->db_db_query($sql);
        		if ($success) {
			        $CampusMailID = $currentUID;
			        if($IsAttachment != '1'){
				    	$sql = "Delete from INTRANET_IMAIL_ATTACHMENT_PART where CampusMailID='$CampusMailID'";
				    	$libDb->db_db_query($sql);
			        }
			    }
			    	
        }else{
         //save the composed email to draft
     	 $sql = "
				 INSERT INTO INTRANET_CAMPUSMAIL (
				      UserID, UserFolderID,SenderID,SenderEmail, RecipientID,InternalCC,InternalBCC,
				      ExternalTo,ExternalCC,ExternalBCC,
				      Subject,Message,Attachment,IsAttachment,IsImportant,IsNotification,AttachmentSize,MailType,DateInput,DateModified,DateInFolder)
		         VALUES (
		          '$uid', '1','$uid','$email_return_path', $recipientIDMarkArray,$InternalCCMarkArray,$InternalBCCMarkArray,
		          '$to_address_list','$cc_address_list','$bcc_address_list','$email_subj','$email_msg','$filepathDir','$IsAttachment','$ImportantFlag','$IsNotification','$attachment_file_totalSize','1',now(),now(),now())";
        $success = $libDb->db_db_query($sql);
			    if ($success) {
			        $CampusMailID = $libDb->db_insert_id();
			        
			        if($new_mail_counter_exist){
           				// increment one for new mail counte to Draft box
						$li->UpdateCampusMailNewMailCount($uid, 1, 1);
           			}
			    }
        }         
     }

     if($CampusMailID!=""&&$AttachmentNameArray!=NULL&&count($AttachmentNameArray)>0){
		  //record attachment information	
		  foreach( $AttachmentNameArray as $value ) {
		  	//to be improved later to store the rows at once
		     $currentAttachmentFileName = $libfilesystem->get_file_basename($value);
		     if(isKIS()){
		     	  $filepathDirwithName = $filepathDir."/".$currentAttachmentFileName;
		     	  $fielpathDirwithNameSize =  filesize($filepathDirwithName);
		     	  $insertAttachmentSql = "INSERT INTO INTRANET_IMAIL_ATTACHMENT_PART (CampusMailID,AttachmentPath, FileName,FileSize)
		                 VALUES ('$CampusMailID','$filepathDirwithName','$currentAttachmentFileName','$fielpathDirwithNameSize')";
		          $libDb->db_db_query($insertAttachmentSql);	    		                 		          
		          if(count($CampusMailIDReceiver)>0){
		          	for($j=0;$j<count($CampusMailIDReceiver);$j++){	          		
		          		  $insertAttachmentSql = "INSERT INTO INTRANET_IMAIL_ATTACHMENT_PART (CampusMailID,AttachmentPath, FileName,FileSize)
		                 VALUES ('$CampusMailIDReceiver[$j]','$filepathDirwithName','$currentAttachmentFileName','$fielpathDirwithNameSize')";		                 
		                 $libDb->db_db_query($insertAttachmentSql);	    		                 		                 
		          	}
		          	
		          }
		     }else{
		     	  $insertAttachmentSql = "INSERT INTO INTRANET_IMAIL_ATTACHMENT_PART (CampusMailID,AttachmentPath, FileName)
		                 VALUES ('$CampusMailID','$filepathDir','$currentAttachmentFileName')";		     	
		           $libDb->db_db_query($insertAttachmentSql);	    		                 
		     }
		   }
	 }	 
     $FolderForLink = $thisFolder;	
	 }//end imail
	if($thisFolder == 'TeacherStatus'){
	$url = "../../../home/eClassApp/teacherApp/teacherStatus/teacher_status.php?token=".$token."&uid=".$uid."&ul=".$ul."&parLang=".$parLang;
	}else if($thisFolder == 'StudentStatus'){
	$url = "../../../home/eClassApp/teacherApp/studentStatus/student_status_classlist.php?token=".$token."&uid=".$uid."&ul=".$ul."&parLang=".$parLang;
	}else{
	//jump link set
    $urlPreLink = '';
    $InitLoad = '';
	if($ContentMailAction=='Compose' || $ContentMailAction=='Drafts'){
	$urlPreLink = "email_list.php";
	$InitLoad= '&InitLoad=0';
	}else{
		$urlPreLink = "email_content.php";
	}
	$url=$urlPreLink."?TargetFolderName=".$FolderForLink."&CurrentUID=".$currentUID."&parLang=".$parLang.$InitLoad;
	}
    intranet_closedb();	
?>
	
	<header data-role ="header" style ="background-color:#3a960e;">
    </header>
    <!-- /header -->  
    <META HTTP-EQUIV="REFRESH" CONTENT="2; URL=<?=$url?>"> 
   </div><!-- /Home -->
	<div data-role="footer">
	</div><!-- /footer -->
	<style type="text/css">
	
	</style>
	<script> 
	</script>
	</div><!-- /page -->
	</body>
    </html>
