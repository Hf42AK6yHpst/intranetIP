<?php
//using: 
/*
 * 2020-11-05 (Ray): Add $_SESSION['SSV_EMAIL_LOGIN'], $_SESSION['SSV_EMAIL_PASSWORD']
 * 2020-09-29 (Ray): Add $_SESSION['emailAppType']
 * 2020-09-21 (Ray): Add pageBackCheck
 * 2020-02-24 (Sam): Show "System Admin" as sender name when $senderID == 0 [S180470]
 * 2019-07-02 (Carlos): Remove extra backward slashes from INTRANET_IMAIL_ATTACHMENT_PART.FileName
 * 2017-03-23 (Carlos): Update iMail new mail counting for readed email.
 * 2015-04-08 (Carlos): Handle the updating of reply/read status notification record for both iMail and iMail plus. 
 */
$PATH_WRT_ROOT = '../../../';

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);

include_once ($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libpwm.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp_imail.php');

//$uid = IntegerSafe($_GET['uid']);
//$_SESSION['UserID'] = $uid;

intranet_auth();
intranet_opendb();

$libDb = new libdb();
$libeClassApp = new libeClassApp(); 
$libpwm= new libpwm();
$linterface = new interface_html();
$emailClassApp = new emailClassApp();
$folderAssoAry ="";
$defaultFolder = array();
$personalFolder = array();

$uid = $_SESSION['UserID'];

if(!isset($AppType)) {
	if (isset($_SESSION['emailAppType'])) {
		$AppType = $_SESSION['emailAppType'];
	}
}

//$token = $_GET['token'];
//$ul = $_GET['ul'];
$passwordAry = $libpwm->getData($uid);
foreach((array)$passwordAry as $_folderType => $_folderKey){
	if($_folderType==$uid)$password=$_folderKey;
}

//$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
//if (!$isTokenValid) {
//	die($Lang['Gamma']['App']['NoAccessRight']);
//}

	$currentUID = IntegerSafe($_GET["CurrentUID"]);
	$thisFolder= $_GET["TargetFolderName"];

	$_displayName = $Lang['Gamma']['SystemFolderName'][$thisFolder];
    if($_displayName ==''){
    $_displayName = $thisFolder;
	}
	
if ($plugin['imail_gamma']) {
    $sql = "Select ImapUserEmail From INTRANET_USER Where UserID = '".$uid."'";
	$dataAry = $libDb->returnResultSet($sql);

	if(isset($_SESSION['SSV_EMAIL_LOGIN']) && isset($_SESSION['SSV_EMAIL_PASSWORD'])) {
		$iMapEmail = $_SESSION['SSV_EMAIL_LOGIN'];
		$iMapPwd = $_SESSION['SSV_EMAIL_PASSWORD'];
	} else {
		$iMapEmail = $dataAry[0]['ImapUserEmail'];
		$iMapPwd = $password;
	}
    $IMap = new imap_gamma(false,$iMapEmail, $iMapPwd);
	$IMap->IMapCache = new imap_cache_agent($iMapEmail, $iMapPwd);

	if($thisFolder!="INBOX"){
    	$Folder = "INBOX.".$thisFolder;
    }
    else{
    	$Folder = $thisFolder;
    }
   $FolderForLink =  urldecode($IMap->decodeFolderName($thisFolder));
   $FolderForLink = urlencode($IMap->encodeFolderName(stripslashes($FolderForLink)));
    
   $_displayName = IMap_Decode_Folder_Name($_displayName);

   $Folder = (isset($Folder) && $Folder != "") ? stripslashes($Folder) : $IMap->InboxFolder;
   $CacheNewMail = false;
   $currentMailRecord = $IMap->getMailRecord($Folder, $currentUID, "");
   $CurUserAddress = $IMap->CurUserAddress;
   $CurUserPassword = $IMap->CurUserPassword;
   $currentMailRecordContent = $currentMailRecord[0];
   $flag=$currentMailRecordContent["flagged"];
   $currentMailShowedContent = array();
   $currentAttachment = array();
   $currentAttachment = $currentMailRecordContent['attach_parts'];
   $attachmentLinkPrefix = "downLoadAttachment.php?MailAddress=".$CurUserAddress."&MailPassword=".$CurUserPassword."&Folder=".$FolderForLink."&MessageID=".$currentUID."&PartNumber=";

	
	$currentMailShowedContent['ShowSender'] = $currentMailRecordContent['fromname'];
	$currentMailShowedContent['ShowReceiver'] = strip_tags($currentMailRecordContent['showto']);
	$currentMailShowedContent['ShowCC'] = strip_tags($currentMailRecordContent['showcc']);
	
	$currentMailShowedContent['Subject']= $currentMailRecordContent['subject'] != ""?$currentMailRecordContent['subject']:'No Subject';
    $DateFormat = "Y-m-d H:i";
    $currentMailShowedContent['Date'] = date($DateFormat, strtotime(str_ireplace("UT","UTC",$currentMailRecordContent['dateReceive'])));
	$currentMailShowedContent['Content']=$currentMailRecordContent['message'];
	
	$MailFlags = $IMap->GetMailPriority($currentUID,$Folder,1);
    $isImportant = $MailFlags[intVal($currentUID)]['ImportantFlag']=== 'true'?true:false;
    
	//---- Track Mail Recipients Status for Internal Mail ----
	if(trim($currentMailRecordContent['eClassMailID'])!='')
	{
		if(trim($currentMailRecordContent['MessageID']) != '')
		{
			// Mark this Mail as Readed
			$IMap->UpdateMailReceipt($currentMailRecordContent['eClassMailID'],$uid);
		}
	}
	//---- End of Track Mail Recipients Status for Internal Mail ----

	#get the move to folder list array#
	$folderAry = $IMap->getMailFolders();
	$folderKeyAry = array();
	$moveToFolderAry = array();
			foreach ($folderAry as $key => $value) {
			 $folderKeyAry[]=$key;
			 }
			$defaultFolderArray = array();
			$selfDefineFolderArray = array();
			 for ($i=0; $i<count($folderAry); $i++) {
			      $_folderNamePieces = explode('.',$folderAry[$folderKeyAry[$i]]);
			      $_numOfPieces = count($_folderNamePieces);	
				 if ($_numOfPieces == 1) {
					$_targetFolderName = IMap_Decode_Folder_Name('INBOX');
				  }
				 else {
					$_targetFolderName = IMap_Decode_Folder_Name($_folderNamePieces[1]);
				  }
				
				$_targetDisplayFolderName = $Lang['Gamma']['SystemFolderName'][$_targetFolderName];
				//set the folder type and classify them with two types
				$moveToFolderUnit['TargetFolderName'] = $_targetFolderName;
				if (in_array($_targetFolderName, (array)$SYS_CONFIG['Mail']['SystemMailBox'])) {
					 $moveToFolderUnit['TargetDisplayFolderName'] = '';
					 if($_targetFolderName != 'Trash'){
					 $moveToFolderUnit['TargetDisplayFolderName'] = $_targetDisplayFolderName;
					 }
					array_push($defaultFolderArray, $moveToFolderUnit);
				}else {
					$moveToFolderUnit['TargetFolderName'] = $IMap->encodeFolderName($moveToFolderUnit['TargetFolderName']);
					$moveToFolderUnit['TargetFolderName'] = str_replace('\\','\\\\',$moveToFolderUnit['TargetFolderName']);
			        $moveToFolderUnit['TargetFolderName'] = str_replace('"','\"',$moveToFolderUnit['TargetFolderName']);
					$moveToFolderUnit['TargetDisplayFolderName'] = $_targetFolderName;
				    array_push($selfDefineFolderArray, $moveToFolderUnit);
				}				
				}
				$moveToFolderAry = array_merge($defaultFolderArray,$selfDefineFolderArray);
				$Folder = str_replace('\\','\\\\',$Folder);
			    $Folder = str_replace('"','\"',$Folder);
			    $originalFolderTagPieces = explode('.',$Folder);
				 if (count($originalFolderTagPieces) == 1) {
					$originalFolderTag = 'INBOX';
				  }
				 else {
					$originalFolderTag = $originalFolderTagPieces[1];
				  }
	$receipts =  $IMap->GetMailReceipts(trim($currentMailRecordContent['eClassMailID']));
	if(sizeof($receipts)!=0) {
		$IsNotification = true;
	}else{
		$IsNotification = false;
	}

	$PrevNextUID = $IMap->Get_Prev_Next_UID($Folder, 'SORTARRIVAL', '1', $CurrentUID);
}//end gamma
else{
	//$lc = new libcampusquota2007($uid);
	$lc = new libcampusmail();
	if($thisFolder =='INBOX'){
		$folderID[0] = 2;
	}else if($thisFolder =='Drafts'){
		$folderID[0] = 1;
	}else if($thisFolder =='Sent'){
		$folderID[0] = 0;
	}else if($thisFolder == 'Trash'){
        $folderID[0] = -1;
	}else{
		$sql = "select FolderID from INTRANET_CAMPUSMAIL_FOLDER where OwnerID='$uid' and foldername = '".$libDb->Get_Safe_Sql_Query($thisFolder)."'";
		$folderID = $libDb->returnVector($sql);
	}
    if( $folderID[0] == -1){
	 $sql  = "Select 
		             Message,
		             SenderID,
		             SenderEmail,
		             RecipientID,
		             InternalCC,
		             ExternalTo,
		             ExternalCC,
		             Subject,
		             DateInput,
		             RecordStatus,
		             Attachment,
                     IsAttachment,
		             IsImportant,
		             CampusMailFromID,
		             IsNotification 
             From 
                 INTRANET_CAMPUSMAIL                 
             Where 
                 UserID ='$uid' and Deleted= '1' AND CampusMailID='$currentUID'";		
	}else{
	 	 $sql  = "Select 
		             Message,
		             SenderID,
		             SenderEmail,
		             RecipientID,
		             InternalCC,
		             ExternalTo,
		             ExternalCC,
		             Subject,
		             DateInput,
		             RecordStatus,
		             Attachment,
                     IsAttachment,
		             IsImportant,
		             CampusMailFromID,
		             IsNotification 
             From 
                 INTRANET_CAMPUSMAIL
             Where 
                 UserID ='$uid' and UserFolderID= '$folderID[0]' AND CampusMailID='$currentUID'";			
	
	}
	    $MailHeader = $lc->returnArray($sql);
	    $content = $MailHeader[0];
	    
	    $message = $content['Message'];
	    
	    $senderID = str_replace('U', '', $content['SenderID']);
	    $senderEmail = $content['SenderEmail'];
	    $recipientID = str_replace('U', '', $content['RecipientID']);
	    $internalCC = str_replace('U', '', $content['InternalCC']);
	    $externalTo = $content['ExternalTo'];
	    $externalCC = $content['ExternalCC'];
	    $subject = $content['Subject']; 
        $date = $content['DateInput']; 
        $recordStatus = $content['RecordStatus']; 
        $_unSeen = ($recordStatus=="")?true:false;
        $isAttachment = $content['IsAttachment'];
        $isImportant = ($content['IsImportant'] === '1')?true:false;
        $IsNotification = ($content['IsNotification'] === '1')?true:false;

        if($content['CampusMailFromID'] != '' && $content['CampusMailFromID'] > 0)
        {
        	// Admin Console > iMail Settings > Notification Reply Policy > Sender will know the mail is read once a recipient opened it.
	        $notification_rely_policy = get_file_content("$intranet_root/file/campusmail_policy.txt");
	        if($notification_rely_policy == 1){
	        	$update_notification_reply_sql = "UPDATE INTRANET_CAMPUSMAIL_REPLY SET IsRead=1, DateModified=now() WHERE CampusMailID = '".$content['CampusMailFromID']."' AND UserID = '$uid' AND TRIM(IsRead)!='1'";
	        	$lc->db_db_query($update_notification_reply_sql);
	        }
        }
        
        //$atta = $content['CampusMailFromID']!=''?$content['CampusMailFromID']:$currentUID; 
        
        if(isKIS()){
        	$atta = $currentUID; 
        }else{
        	$atta = $content['CampusMailFromID']!=''?$content['CampusMailFromID']:$currentUID; 
        }
        
        $getAttachmentSql = "select * from INTRANET_IMAIL_ATTACHMENT_PART where CampusMailID ='$atta'";
        $currentAttachment = $lc->returnArray($getAttachmentSql);  
        for($i=0;$i<count($currentAttachment);$i++){       	  	
        	//$tempFileName = str_replace(array('\'','"'),array('\\\'','\"'),$currentAttachment[$i]['FileName']);
        	$tempFileName = stripslashes($currentAttachment[$i]['FileName']);
        	$tempFullPath = $intranet_root."/file/mail/".$currentAttachment[$i]['AttachmentPath']."/".$tempFileName;
        	
        	//handle KIS path different
        	if(isKIS()){
    			  $tempFullPath_kis = $intranet_root."/file/mail/attachments/".$currentAttachment[$i]['AttachmentPath'];
    			  if(is_dir($tempFullPath_kis)){
    			  	  $tempFullPath = $intranet_root."/file/mail/attachments/".$currentAttachment[$i]['AttachmentPath']."/".$tempFileName;
    			  }else{
    			  	   $tempFullPath = $intranet_root."/file/mail/attachments/".$currentAttachment[$i]['AttachmentPath'];        			  	
    			  }       			         		        		        		
        	}
        	
        	//filesize
        	$currentAttachment[$i]['FileSize'] = intval(filesize($tempFullPath)/1024);
        	$currentAttachment[$i]['FileName'] = $tempFileName;
        }
        $attachmentLinkPrefix = "downLoadAttachment.php?uid=".$uid."&MessageID=".$atta."&PartNumber="; 
                
        if($_unSeen){
	        $sql  = "UPDATE INTRANET_CAMPUSMAIL set RecordStatus='1'  where UserID ='$uid' and CampusMailID = '$currentUID'";
	        $lc->db_db_query($sql);
	        //For Notification List
	        $sql  = "UPDATE INTRANET_CAMPUSMAIL_REPLY set IsRead='1',DateModified = now() where UserID ='$uid' and CampusMailID = '$currentUID'";
	        $lc->db_db_query($sql);	
	        
	        if(method_exists($lc,'UpdateCampusMailNewMailCount')){
	        	$lc->UpdateCampusMailNewMailCount($uid, $folderID[0], -1);
	        }
        }
        
        $filterUserIDs = $senderID;
        if($recipientID != ''){
        	$filterUserIDs .= ','.$recipientID;
        }
        if($internalCC!=''){
        	$filterUserIDs .= ','.$internalCC;
        }
        $sql  = "select ".getNameFieldByLang()."as Name,UserID from INTRANET_USER where UserID In (".$filterUserIDs.")";
        $userNameArray = $lc->returnArray($sql);
        $userNameHashArray = array();
        for($i=0;$i<count($userNameArray);$i++){
        	$userNameHashArray[$userNameArray[$i]['UserID']] = $userNameArray[$i];
        }
        $recipientIDArray = array_filter(explode(",", $recipientID));
        $recipientNames = '';
        for($m=0;$m<count($recipientIDArray);$m++){
        	$recipientNames .= $userNameHashArray[$recipientIDArray[$m]]['Name'].", ";
        }

        if($externalTo != ''){
        	$recipientNames .= $externalTo;
        }

        $internalCCArray = array_filter(explode(",", $internalCC));
        $internalCCNames = '';
        for($n=0;$n<count($internalCCArray);$n++){
        	$internalCCNames .= $userNameHashArray[$internalCCArray[$n]]['Name'].", ";
        }
        if($externalCC != ''){
        	$internalCCNames .= $externalCC;
        }
        
        //This is for notification status list
        if($thisFolder =='Sent'){
        	$receipts = array();
        	$username_field = getNameFieldWithClassNumberByLang("a.");
            $order_str = "a.ClassName, a.ClassNumber, a.EnglishName, b.UserName";
            $sql = "SELECT IF(a.UserID IS NULL,CONCAT('<I>',b.UserName,'</I>'),$username_field) as UserName,
                          b.IsRead, b.DateModified
                   FROM INTRANET_CAMPUSMAIL_REPLY AS b LEFT OUTER JOIN INTRANET_USER AS a ON a.UserID = b.UserID WHERE b.CampusMailID = '".$CurrentUID."' ORDER BY $order_str";
	        $receipts = $libDb->returnArray($sql);
        }       

	 	if ($subject != "") {
			$subjectPrint = (substr($subject, 0, 10) == "=?UTF-8?Q?") ? substr($subject, 10, -2) : $subject ;
		} else {
			$subjectPrint = "[NO SUBJECT]";
		}
		
		$DateFormat = "Y-m-d H:i";
		$prettydate='';
		if($date!=''){
	    $prettydate = date($DateFormat, strtotime(str_ireplace("UT","UTC",$date)));
		}

    if($senderID == ''){
    	$currentMailShowedContent['ShowSender'] = $senderEmail;
    }else if($senderID == 0){
    	$currentMailShowedContent['ShowSender'] = $Lang['AppNotifyMessage']['SystemAdmin'];
    }else{
    	$currentMailShowedContent['ShowSender'] = $userNameHashArray[$senderID]['Name'];
    }
	$currentMailShowedContent['ShowReceiver'] = $recipientNames;
	$currentMailShowedContent['ShowCC'] = $internalCCNames;
	$currentMailShowedContent['Subject']=$subjectPrint;
	$currentMailShowedContent['Date']=$prettydate;
	$currentMailShowedContent['Content']=$message;
	$flag ="IMAIL_NO_FLAG";
	
	#get the move to folder list array#
	$moveToFolderAry = array();
	$sql = "SELECT FolderID, FolderName FROM INTRANET_CAMPUSMAIL_FOLDER WHERE OwnerID = '".$uid."' OR RecordType = 0 ORDER BY  FolderID DESC";
	$folderAry = BuildMultiKeyAssoc($libDb->returnResultSet($sql), 'FolderID', $IncludedDBField=array('FolderName'), $SingleValue=1);

	$folderKeyAry = array();
	foreach ($folderAry as $key => $value) {
     $folderKeyAry[]=$key;
     } 
    $defaultFolderArray = array();
	$selfDefineFolderArray = array();
	for ($i=0; $i<count($folderAry); $i++) {
	$_targetFolderName = $folderAry[$folderKeyAry[$i]]; 
    $_targetFolderID = $folderKeyAry[$i];
	if($_targetFolderName =='Inbox'){
			$_targetFolderName = "INBOX";
		}
		else if($_targetFolderName =='Draft'){
			$_targetFolderName = "Drafts";
		}
		else if($_targetFolderName =='Outbox'){
			$_targetFolderName = "Sent";
		}
	$_targetDisplayFolderName = $Lang['Gamma']['SystemFolderName'][$_targetFolderName];
	$moveToFolderUnit['TargetFolderName'] = $_targetFolderID;	
	if($_targetDisplayFolderName!=''){
	$moveToFolderUnit['TargetDisplayFolderName'] = $_targetDisplayFolderName;
	array_push($defaultFolderArray,$moveToFolderUnit);	
	}else{
	$moveToFolderUnit['TargetDisplayFolderName'] = $_targetFolderName;	
	array_push($selfDefineFolderArray, $moveToFolderUnit);	
	}
	}//end for loop
	$moveToFolderAry = array_merge($defaultFolderArray,$selfDefineFolderArray);
	$originalFolderTag = $folderID[0];
	$FolderForLink = $thisFolder;

	$PrevNextUID = $IMap->Get_Prev_Next_UID($Folder, 'SORTARRIVAL', '1', $CurrentUID);
}
//    $currentMailShowedContent['Content'] = htmlspecialchars_decode($currentMailShowedContent['Content']);

$EmailListSelectFormAction = "email_content.php?TargetFolderName=".$FolderForLink ."&parLang=".$parLang;
$prevID = $PrevNextUID["PrevUID"];
$nextID = $PrevNextUID["NextUID"];

$prevBtn = '';
$nextBtn = '';
if ($prevID!="")
{
	$prevBtn = '<a onclick = "getEmailDetails('.$prevID.');" data-icon="myapp-prev"  data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;left:0px"></a>';
}
if ($nextID!="")
{
	$nextBtn = '<a onclick = "getEmailDetails('.$nextID.');" data-icon="myapp-next"   data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;left:0px"></a>';
}

$bgColor = ($sys_custom['eClassTeacherApp']['iMailToolbarColorCode'])? $sys_custom['eClassTeacherApp']['iMailToolbarColorCode'] : '#429DEA';
 
    if($AppType=="S"){
           $bgColor = "#F8BD10";
       }
    if($AppType=="P"){
        $bgColor = "#16A30D";
    }
    $myPrevIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_outbox_white_36px.png"."\"";
    $myNextIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_inbox_white_36px.png"."\"";

 	$myReplyIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/icon_reply.png"."\"";
	$myReplyAllIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/icon_replyall.png"."\"";
	$myForwardIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/icon_forward.png"."\"";
	$myTrashIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_trash_white.png"."\"";
	$myOutboxIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_before_white.png"."\"";
	$myStarIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/icon_star.png"."\"";
    $myFilledStarIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/icon_star_filled.png"."\"";
    $importantFlagUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/icon_important.gif"."\"";
    $myAttachmentIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/icon_attachment.png"."\"";    
 
 	//initilize the photo according to its status
	$flagIconUrl ="";
	if($flag =='F'){
		$flagIconUrl =$myFilledStarIconUrl;
	}
	else if($flag !='IMAIL_NO_FLAG'){
		$flagIconUrl =$myStarIconUrl;
	}
	$importantFlagHtml= "";
	if($isImportant){
		$importantFlagHtml = "<input type='image' id='importantEmail' src='/images/2009a/iMail/icon_important.gif'>";
	}
	$emailListUrl = "email_list.php?InitLoad=0&parLang=".$parLang;
	$urlSuffix = "&TargetFolderName=".$FolderForLink."&CurrentUID=".$currentUID."&parLang=".$parLang;
	$current_email_delete_action_redirection = "email_delete_action_redirection.php?TargetFolderName=".$FolderForLink."&CurrentUID=".$currentUID."&parLang=".$parLang."&IndividualEmail=1";
   	$refreshIconUrl =$linterface->Get_Ajax_Loading_Image($noLang=true);
	$curEmailContentUrl = "email_content.php?parLang=".$parLang;
    $loadingX = $emailClassApp->getLoadingHtml();  
    $moveToHtml = $emailClassApp->getMoveToHtml($moveToFolderAry,$originalFolderTag);
    $moveToHtml = $prevBtn.$nextBtn.$moveToHtml;
    $emailContentHeaderHtml = $emailClassApp->getEmailContentHeaderHtml($_displayName,$moveToHtml, $bgColor);
     $x = "<table style='width:100%;padding-left: 10px;padding-right: 10px;background-color:#F9F9F9;'><tr>";
	 $x .= "<th class = 'my-column' style='padding-left:0px;padding-top:0px;'><div class = 'my-subjectWarpContent'><p class = 'ui-li-text-my-h1'  style='text-align:left;margin-top: 10px;margin-bottom: 0px;'>".$importantFlagHtml.$currentMailShowedContent['Subject']."</p></div></th>";
	   if($flagIconUrl!=""){
	      $x .= "<td class = 'my-column' valign='top' style='padding-left: 5px;padding-top: 10px;width:36px'><input type='image' id ='markEmail'  src=".$flagIconUrl." style='width:36px!important;height:36px!important;background-size: 36px 36px!;padding-bottom: 5px;padding-right: 0px;'></td>";
	   }
     $x .="</tr>";
     $x .="<tr><td style='color:#989898'>".$currentMailShowedContent['Date']."</td></tr></table>";
     
     //Sender and Receivers
     $x .="<table style='width:100%;padding-left: 15px;padding-right: 10px;padding-top: 20px;background-color:#F9F9F9;'><tr id='SenderRow'>";
     $x .="<td style='color:#989898;width:50px' valign='top'>".$Lang['Gamma']['Sender']."</td><td class='sender_receiver_coloum' >".$currentMailShowedContent['ShowSender']."</td>";
     if($currentMailShowedContent['ShowReceiver'] != '' || $currentMailShowedContent['ShowCC'] != ''){
     	$x .= "<td style='text-align:right;width:40px;'><a href='#' id ='IsHideReceiver' class='ui-link' onClick='ShowAndHideReceiver(this.value);false;' style='text-decoration: none;'>".$Lang['Gamma']['App']['OpenReceiver'] ."</a></td>";
     }
     $x .= "</tr>";
     if($currentMailShowedContent['ShowReceiver'] != ''){
     	$x .="<tr id='ReceiverRow' style='display:none;'>";
        $x .="<td style='color:#989898;width:50px' valign='top'>".$Lang['Gamma']['To']."</td><td class='sender_receiver_coloum' >".$currentMailShowedContent['ShowReceiver']."</td></tr>";
     }
     if($currentMailShowedContent['ShowCC'] != ''){
     	$x .="<tr id='CopyRow' style='display:none;'>";
        $x .="<td style='color:#989898;width:50px' valign='top'>".$Lang['Gamma']['cc']."</td><td class='sender_receiver_coloum'>".$currentMailShowedContent['ShowCC']."</td></tr>";
     }
     $x .="</table>";
     
     $x .="<div class='vertical-line'></div>";
    //ATTACHMENT
    $attachmentLinkHtml = "";  

	if(count($currentAttachment)>0){
		  $attachmentLinkHtml .= "<div style='padding-left:15px'>";
		  for($attachmentCount=0;$attachmentCount<count($currentAttachment);$attachmentCount++){
		  	    $tempShownName = str_replace(array('\\\'','\"'),array('\'','"'),$currentAttachment[$attachmentCount]['FileName']);
		        $attachmentLinkHtml .= "<a href=\"javascript:void(0);\" onclick=\"goDownloadAttachment('".$currentAttachment[$attachmentCount]['PartID']."');\"  style='text-decoration: none;' ><img hspace='2' vspace='2' border='0' align='absmiddle' src=$myAttachmentIconUrl style='width:30px!important;height:30px!important;background-size: 30px 30px!important;top: 5px;' >".$tempShownName."</a>"." (".$currentAttachment[$attachmentCount]['FileSize']."KB)</br>";
		  }	
		  $attachmentLinkHtml .=  "</div>";
		  $x .= $attachmentLinkHtml;   
		  $x .="<div class='vertical-line'></div>"; 
	}
	
	//CONTENT
	$x .="<div id='descriptionDiv'  style =' overflow-x: auto; overflow-y: hidden;' class = 'my-messageWarpContent'>".HTML_Transform($currentMailShowedContent['Content'])."</div>";  
    //RECEIVERS READ STATUS LIST
    if($IsNotification && $TargetFolderName == 'Sent'){
     $total_readed = 0; $total_unreaded = 0;
     $notificationListHtml = '';

     for($i=0;$i<sizeof($receipts);$i++)
		{
			if ($plugin['imail_gamma']) {
				list($name,$address,$status,$date_modified) = $receipts[$i];
			}else{
				list($name,$status,$date_modified) = $receipts[$i];
			}
			
			if ($i%2==0)
			{
       			$notificationListHtml .= '<tr>';
			} else {
				$notificationListHtml .= '<tr style="background: #F7FFEF !important;">';
			}
			
			$notificationListHtml .= '<td align="center" class="my-sendercolumn" style="width:45% max-width:10px"><div class="my-subjectWarpContent">'.$name.'</div></td>';
			if(trim($status)==1){
				$total_readed++;
				$notificationListHtml .= '<td align="center">'.$date_modified.'&nbsp;<img border="0" align="absmiddle" title="'.$Lang['Gamma']['UnseenStatus']['SEEN'].'" alt="'.$Lang['Gamma']['UnseenStatus']['SEEN'].'" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_open_mail.gif"></td>';
			}else{
				$total_unreaded++;
				$notificationListHtml .= '<td align="center"><img border="0" align="absmiddle" title="'.$Lang['Gamma']['UnseenStatus']['UNSEEN'].'" alt="'.$Lang['Gamma']['UnseenStatus']['UNSEEN'].'" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_unread_mail.gif"></td>';
			}
			$notificationListHtml .= '</tr>';
		}
    	$x .= '<br><br><br><div style="height:1px;background-color: #E0E0E0;margin: 0 auto;"></div>';
        		$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%" style="padding-top: 5px;"><tr>
				<td align="center">
					<table cellspacing="0" cellpadding="3" border="0" width="100%">
						<tbody>
							<tr style="background: #CBE3BE !important;text-shadow:none">
								<td align="center" width="45%"><span class="tabletext">'.$Lang['Gamma']['Recipient'].'</span></td>
								<td align="center" width="55%" >'.$Lang['Gamma']['UnseenStatus']['UNSEEN'].'('.$total_unreaded.') '.$Lang['Gamma']['UnseenStatus']['SEEN'].'('.$total_readed.')</td>
							</tr>';				
		$x .= $notificationListHtml;
		$x .= '			</tbody>
					</table>
				</td>
			</tr></table>';	
    }
    
    echo $libeClassApp->getAppWebPageInitStart();     
	?>
	<script>
	window.JSON || 
	document.write('<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/json3.min.js"><\/script>');
	</script>
		
	<style type="text/css">
    .ui-icon-myapp-prev{
        background-color: transparent !important;
        border: 0 !important;
        -webkit-box-shadow:none !important;
        background: url(<?=$myPrevIconUrl?>) no-repeat !important;
    }
    .ui-icon-myapp-next{
        background-color: transparent !important;
        border: 0 !important;
        -webkit-box-shadow:none !important;
        background: url(<?=$myNextIconUrl?>) no-repeat !important;
    }
	 .ui-icon-myapp-reply{
		 background-color: transparent !important;
		 border: 0 !important;
		 -webkit-box-shadow:none !important;
		 background: url(<?=$myReplyIconUrl?>) no-repeat !important;
	 }
	 .ui-icon-myapp-replyAll{
		 background-color: transparent !important;
		 border: 0 !important;
		 -webkit-box-shadow:none !important;
		 background: url(<?=$myReplyAllIconUrl?>) no-repeat !important;
	 }
	.ui-icon-myapp-forward{
		 background-color: transparent !important;
		 border: 0 !important;
		 -webkit-box-shadow:none !important;
		 background: url(<?=$myForwardIconUrl?>) no-repeat !important;
	 }
	.ui-icon-myapp-trash{
		 background-color: transparent !important;
		 border: 0 !important;
		 -webkit-box-shadow:none !important;
		 background: url(<?=$myTrashIconUrl?>) no-repeat !important;
	}
	
    .ui-icon-myapp-myOutbox{
		 background-color: transparent !important;
		 border: 0 !important;
		 -webkit-box-shadow:none !important;
		 background: url(<?=$myOutboxIconUrl?>) no-repeat !important;
	}
		
	.ui-icon-myapp-myStar{
		 background-color: transparent !important;
		 border: 0 !important;
		 -webkit-box-shadow:none !important;
		 background: url(<?=$myFilledStarIconUrl?>) no-repeat !important;
	  }
	    
	  .ui-li-divider a {
	    text-decoration:  none;
	    color: black !important;
	  }
	
	 .ui-li-text-my-h6{
    	color:black;
    	font-size: .75em !important;
	 }
	 .ui-li-text-my-title-font{
     	color:white;
     	text-shadow: none!important;
	  }
	 .ui-li-text-my-h3{
      	color:black; !important;
    	font-size:1em!important;
    	font-weight:bold;
	  }
	 .ui-li-text-my-h1{
      	color:black;
    	font-size:1.30em!important;
    	font-weight:bold;
	  }
	  th.my-column{
    	 width:-moz-calc(100% - 40px);
	  }
	  td.my-sendercolumn{
	       overflow:hidden;
	       break-word: word-wrap;
	  }
	  td.my-dateColumn{
		   width:25%;
      }
      td.sender_receiver_coloum{
         width:-moz-calc(100% - 100px); !important;
      	 overflow:hidden;
      	 break-word: word-wrap;
      	 valign:top;
      }
	  div.my-subjectWarpContent{
		   margin:0 0 0 0 !important;
		   height:auto;
		   word-wrap: break-word;
	  }
	  div.my-messageWarpContent{
		   margin:15px 20px 0 20px;
		   width:90%;
		   height:auto;
		   word-wrap: break-word;
	  }
	  table th { text-align:left; padding:6px;} 
	    #loadingmsg {
	      color: black;
	      background:tranparent!important;
	      position: fixed;
	      top: 50%;
	      left: 50%;
	      z-index: 100;
	      margin: -8px 0px 0px -8px;
	      }
	      #loadingover {
	      background: white;
	      z-index: 99;
	      width: 100%;
	      height: 100%;
	      position: fixed;
	      top: 0;
	      left: 0;
	      -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
	      filter: alpha(opacity=80);
	      -moz-opacity: 0.8;
	      -khtml-opacity: 0.8;
	      opacity: 0.8;
	   }
	   #select-moveto-folder-button{
	       padding-top: 6px ;
	       padding-bottom: 6px;
	       padding-left: 5px;
	       top: 10px;
	       background-color:white !important;
	       border-color:<?=$bgColor?> !important;
	   }
	   .ui-btn-icon-right:after, .ui-btn-icon-top:after, .ui-btn-icon-bottom:after{
		  background-color: <?=$bgColor?> !important;
		}
	   .ui-controlgroup-horizontal .ui-btn.ui-first-child{
			-webkit-border-top-right-radius: inherit;
			-webkit-border-bottom-right-radius: inherit;
		}
	    .vertical-line {
			height:1px;
			background:#D0D0D0;
            margin-left:10px;
            margin-right:10px;
        }
		
		</style>
		<script>
        function pageBackCheck() {
            if(performance.navigation.type == 2) {
                location.reload();
            }
        }

        function getEmailDetails(inputId)
        {
            showLoading();
            var emaillink = "<?=$EmailListSelectFormAction."&CurrentUID="?>";
            emaillink = emaillink + inputId;
            window.location.assign(emaillink);
        }


        $(document).ready(function(){
            $("#loadingover").on("pageinit", function (e) {
                var page = $(this);
                pageBackCheck();
            });

            var myCustomEvent = (navigator.userAgent.match('iPhone') != null) ? 'popstate' : 'pageshow';
            $(window).on(myCustomEvent, function(e) {
                pageBackCheck();
            });

		  $("#search").click(function(){
		  $("#edit").hide();
		  
		  });  			
		  <!--delete email-->
		  $("#comfirmDeleteEmails").click(function(){
          window.location.assign("<?=$current_email_delete_action_redirection?>");  
		  return false;
		  });  
		  <!--mark email with star function-->
		  $("#markEmail").click(function(){
                $markEmailStatus = $("#markEmail").attr('src');
	            var Folder = "<?=$Folder?>";
	           	var currentUID = "<?=$currentUID?>";
	           	var uid ="<?=$uid?>";
            
           if($markEmailStatus =='/images/2009a/iMail/app_view/icon_star_filled.png'){
	           $markUrl = '/images/2009a/iMail/app_view/icon_star.png' ;
	           $.post( "flagEmail.php",{Folder:Folder, currentUID:currentUID, Flag:0,uid:uid},function(data) {});
           }else{
	           $markUrl = '/images/2009a/iMail/app_view/icon_star_filled.png' ;
	           $.post( "flagEmail.php",{Folder:Folder, currentUID:currentUID, Flag:1,uid:uid},function(data) {});
           }
         	   $("#markEmail").attr('src',$markUrl);});
		});
		<!--end delete email-->
		function getEmailListPage(){
			showLoading();
	        window.location.assign("<?=$emailListUrl."&TargetFolderName=".$FolderForLink?>");
		}
		
		function goDownloadAttachment(parPartId) {
			
			var link = "<?=$attachmentLinkPrefix?>"+parPartId;
			window.location.assign(link);
		}
		
		function ComposeEmail(composeID){
			showLoading();
			var link = "email_reply_forward_page.php?MailAction="+composeID+"<?=$urlSuffix?>";
			window.location.assign(link);
		}
		
		function showLoading() {
		    document.getElementById('loadingmsg').style.display = 'block';
		    document.getElementById('loadingover').style.display = 'block';
		}

		function changeEmailFolder(selectedFolderItem){
        	<!--use ajax to change the current email's folder with call back-->
			var confirmMoveNotification = confirm("<?=$Lang['Gamma']['App']['ConfirmMoveNotification']?>");
			if (confirmMoveNotification == true) {
			 var targetFolder = selectedFolderItem.value;
			 var originalFolder = "<?=$originalFolderTag?>";
             var originalUID = "<?=$currentUID?>";
             var UID = "<?=$uid?>";
             $.post( "emailChangeFolder.php",{targetFolder:targetFolder,originalFolder:originalFolder,originalUID:originalUID,UID:UID},function(data){})
             .done(function(data) {
             var obj = JSON.parse(data);
             if(obj.ResponseStatus){
             var currentFolder = obj.currentFolder; 
             var curentUID = obj.currentUID;
             var url = '';
             if(curentUID!=''){
             url = "<?=$curEmailContentUrl."&TargetFolderName="?>"+currentFolder+"&CurrentUID="+curentUID;	
             }else{
             url = "<?=$emailListUrl."&TargetFolderName="?>"+currentFolder;
             }
			 window.location.href = url;	
             }<!--end response is 1-->
			  });<!--end change email folder-->
			}<!--end confirm change folder-->
			else{
				<!--reset current page-->
				 var url = window.location.href;
				 window.location.href = url;
			}
		}
		
		function ShowAndHideReceiver(value){
		  var row = document.getElementById('ReceiverRow');
		  var copyRow = document.getElementById('CopyRow');
		  if(value == 0){
		  	if(row != null){
		  	   row.style.display = 'none';
		  	}
	  		if(copyRow != null){
	           copyRow.style.display = 'none';
	        }
	        document.getElementById("IsHideReceiver").value = 1;
            document.getElementById("IsHideReceiver").innerHTML = '<?=$Lang['Gamma']['App']['OpenReceiver'] ?>';
            
		  }else{
		  	
		  	if(row != null){
		  	   row.style.display = '';
		  	}
	  		if(copyRow != null){
	           copyRow.style.display = '';
	        }
		  	document.getElementById("IsHideReceiver").value = 0;
            document.getElementById("IsHideReceiver").innerHTML = '<?=$Lang['Gamma']['App']['HideReceiver'] ?>';
		  }
		  
		}
		
		</script>
	</head>
	<body>
	<div data-role="page" style = "background-color:#FFFFFF;">
	<?=$emailContentHeaderHtml?>
    <div role="main" class="ui-content" style = "padding-left: 0px;padding-top:0px;padding-right: 0px;background-color:#FFFFFF;" >  
    <?=$x?>
<?php
//echo $emailClassApp->getDeleteComfirmPopupHtml();
echo $libeClassApp->getAppWebPageInitEnd();
intranet_closedb();
?>
