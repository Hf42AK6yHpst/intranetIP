<?php
// page modifing by:
/*
 * 2020-03-05 (Sam): Fix imailPlus cannot download inline image [V181076]
 * 2020-01-08 (Bill): fixed display garbled content using IOS 13 to open doc/docx file  [2020-0106-0947-49235]
 * 2019-11-08 (Bill): handle pdf file for IOS 13 for imail_gamma    [EJ DM#1309]
 * 2019-10-29 (Bill): handle pdf file for IOS 13    [2019-1028-1013-07235]
 * 2019-07-02 (Carlos): Remove extra backward slashes from INTRANET_IMAIL_ATTACHMENT_PART.FileName
 */

$PATH_WRT_ROOT = '../../../';
$CurSubFunction = "Communication";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libAES.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');

// fetch info
$AESKey = $eclassAppConfig['aesKey'];
$libaes = new libAES($AESKey);

$MessageID = IntegerSafe($_GET['MessageID']);
// Get Attachment Number
//$PartNumber = IntegerSafe($_GET['PartNumber']);
$PartNumber = $_GET['PartNumber']; //[V181076] PartNumber is a real number, not integer

if ($plugin['imail_gamma'])
{
	// Get Folder Select
    $Folder = stripslashes(urldecode($_GET['Folder']));
    $MailAddress = stripslashes(urldecode($_GET['MailAddress']));
    $MailPassword = $_GET['MailPassword'];
    if($MailPassword == ""){
        $MailPassword =  $libaes->decrypt(stripslashes($_GET['APPENC']));
    }
    $UserType = $_GET['UserType'];
    
    // IMAP setup
    $IMapCache = new imap_cache_agent($MailAddress,$MailPassword,$UserType);
    if($Folder != "INBOX"){
    	$Folder = "INBOX.".$Folder;
    }
    
    // Get the Email for display
    $Attachment = $IMapCache->Get_Attachment($PartNumber,$MessageID,$Folder);
    $IMapCache->Close_Connect();
    
    $filename = $Attachment['FileName'];
    //[V181076] with ref. from /home/imail_gamma/cache_view_attachment.php, filename escape is not required
    //$filename = str_replace('%','%25',$filename);
    //$filename = str_replace(array('/','&','#','*','$','\'','\"',',','!','(',')','+','-',':',' '),array('%2F','%26','%23','%2A','%24','%27','','%2C','%21','%28','%29','%2B','%2D','',''),$filename);

    $fileMineType = $Attachment['MimeType'];

    // [DM#1309]
    $fileType = getFileExtention($filename, 1);
    if ($fileType == "pdf") {
        $fileMineType = "application/pdf";
    }
    // [2020-0106-0947-49235] IOS 13 - for doc file
    else if ($fileType == "doc") {
        $fileMineType = "application/msword";
    }
    else if ($fileType == "docx") {
        $fileMineType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    }

    //Output_To_Browser($Attachment['Content'], $filename, $Attachment['MimeType']);
    Output_To_Browser($Attachment['Content'], $filename, $fileMineType);
}
else
{
    $uid = IntegerSafe($_GET['uid']);
    
    intranet_opendb();
    $libDb = new libdb();
    
    $getAttachmentSql = "select FileName,AttachmentPath from INTRANET_IMAIL_ATTACHMENT_PART where PartID='$PartNumber' AND CampusMailID='$MessageID'";
    $currentAttachmentResult = $libDb->returnArray($getAttachmentSql);	
    
    $filename = stripslashes($currentAttachmentResult[0]['FileName']);
    //$filename_path = str_replace(array('\'','"'),array('\\\'','\"'),$filename);
    $filename_path = $filename;
    $target_filepath = $intranet_root."/file/mail/" .$currentAttachmentResult[0]['AttachmentPath']."/".$filename_path;
    
    // handle KIS path different
    if(isKIS($uid))
    {
        $target_filepath_kis = $intranet_root."/file/mail/attachments/".$currentAttachmentResult[0]['AttachmentPath'];
        if(is_dir($target_filepath_kis)){
          $target_filepath = $intranet_root."/file/mail/attachments/".$currentAttachmentResult[0]['AttachmentPath']."/".$filename_path;
        }
        else{
           $target_filepath = $intranet_root."/file/mail/attachments/".$currentAttachmentResult[0]['AttachmentPath'];
        }
    }
    
    if(file_exists($target_filepath) && is_file($target_filepath) && filesize($target_filepath)!=0){
        $buffer = ($fd = fopen($target_filepath, "r")) ? fread($fd,filesize($target_filepath)) : "";
        if ($fd) {
            fclose ($fd);
        }
    }
    
    $content_type = "application/octet-stream";
    
    // [2019-1028-1013-07235] IOS 13 - for pdf file
    $content_mime_type = mime_content_type($target_filepath);
    if ($content_mime_type == "application/pdf") {
        $content_type = $content_mime_type;
    }

    // [2020-0106-0947-49235] IOS 13 - for doc file
    $file_extension = getFileExtention($target_filepath, 1);
    if ($file_extension == 'doc' || $file_extension == 'docx') {
        $content_type = $content_mime_type;
    }

    $filename = str_replace('%','%25',$filename);
    $filename = str_replace(array('/','&','#','*','$','\'','"',',','!','(',')','+','-',':',' '),array('%2F','%26','%23','%2A','%24','%27','','%2C','%21','%28','%29','%2B','%2D','',''),$filename);

    output2browser($buffer, $filename, $content_type);
}
?>