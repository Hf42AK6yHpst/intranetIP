<?php
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp_imail.php');

$uid = IntegerSafe($_POST['uid']);
$isAccessInternetMail = $_REQUEST['isAccessInternetMail'];
$isAccessExternetMail = $_REQUEST['isAccessExternetMail'];
$has_webmail = $_REQUEST['has_webmail'];

$libDb = new libdb(); 
$emailClassApp = new emailClassApp();
global $sys_custom;

intranet_auth();
intranet_opendb();

if ($plugin['imail_gamma']) {
	  $gamma_filter = " AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ";
	   $email_field = "IMapUserEmail";
}else{
	 $gamma_filter = "";
	 $email_field = " CONCAT(UserLogin,'@$mail_domain') ";	
}
	$InternalArr = array();
	$AddressBookArr = array();
	if($isAccessInternetMail){
         #Below address is for auto-complete email address   
			#GET Internal address	
			$NameField = " IF(NickName IS NULL OR TRIM(NickName)='',".getNameFieldByLang().",NickName) ";
						if($sys_custom['iMailPlusEmailNameWithoutClassInfo']) {
							$ClassInfo = "''";
						}else{
							$ClassInfo = "IF(RecordType = 1, '', IF(ClassName = '' OR ClassName IS NULL, '', CONCAT(' (',ClassName,'-',ClassNumber,')')))";
						}
			if($has_webmail){
			$InternalSql = "SELECT 
					CONCAT($NameField, $ClassInfo ,' <',$email_field,'>') as email ,UserID
				FROM 
					INTRANET_USER 
				WHERE
					RecordStatus = 1
                    AND CONCAT($NameField, $ClassInfo ,' <',$email_field,'>') IS NOT NULL
					$gamma_filter
				ORDER BY RecordType, IFNULL(ClassName,''), IFNULL(ClassNumber,0), EnglishName";		
			}else{
			$InternalSql = "SELECT 
					CONCAT($NameField, $ClassInfo ) as email ,UserID
				FROM 
					INTRANET_USER 
				WHERE
					RecordStatus = 1
                    AND CONCAT($NameField, $ClassInfo ) IS NOT NULL
					$gamma_filter
						ORDER BY RecordType, IFNULL(ClassName,''), IFNULL(ClassNumber,0), EnglishName";		
			}		
			
			
			$InternalArr = $libDb->returnResultSet($InternalSql);	
			
	     }

	if($isAccessExternetMail){
			#GET External address
			$AddressBookSql = "SELECT CONCAT(TargetName,' <',TargetAddress,'>') as email FROM INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL WHERE OwnerID = '$uid'";
			$AddressBookArr = $libDb->returnArray($AddressBookSql); 
			for($i=0;$i<=count($AddressBookArr)-1;$i++){
				 $AddressBookArr[$i]['UserID'] = '-1';//set external address's userID to -1 AS a external identifier
			}
			
     }
	   if($has_webmail){
	   	 $InternalAndExternalTotalAddressList = array_merge($InternalArr, $AddressBookArr);
	   }else{
	   	 $InternalAndExternalTotalAddressList = $InternalArr;
	   }
       	//generate the autocompleteHtml
       	$InternalAndExternalTotalAddressListHtml = '';
       	if ($plugin['imail_gamma']) {
       	for($j = 0;$j<count($InternalAndExternalTotalAddressList);$j++){      		
       		$tempEmail = $InternalAndExternalTotalAddressList[$j]['email'];
       		$InternalAndExternalTotalAddressListHtml .= '<li><a href="#" onclick = "getCurrentEmailAddress(this);" id ="'.$tempEmail.'" value="'.$tempEmail.'">'.htmlspecialchars($tempEmail).'</a></li>';
       	}
       	}//end gamma
       	else{
       	 	for($j = 0;$j<count($InternalAndExternalTotalAddressList);$j++){
       		$tempEmail = $InternalAndExternalTotalAddressList[$j]['email'];
       		$tempUID = $InternalAndExternalTotalAddressList[$j]['UserID'];
       		$InternalAndExternalTotalAddressListHtml .= '<li><a href="#" onclick = "getCurrentEmailAddress(this);" id ="'.$tempUID.'" value="'.$tempEmail.'">'.htmlspecialchars($tempEmail).'</a></li>';
       	}	
       	}
      
       	echo $InternalAndExternalTotalAddressListHtml;
       	intranet_closedb();
?>