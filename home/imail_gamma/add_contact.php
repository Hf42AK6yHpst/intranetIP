<?php
// page modifing by : Kenneth chung 
$PathRelative = "../../../../";
$CurSubFunction = "Communication";
$CurPage = $_SERVER["REQUEST_URI"];

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}

AuthPage(array("Communication-Mail-GeneralUsage"));

intranet_opendb();

include_once($PathRelative."src/include/class/database.php");
include_once($PathRelative."src/include/class/imap_address_ui.php");
include_once($PathRelative."src/include/class/imap_address.php");

// Database server
$ldb = new database();

/*echo '<pre>';
var_dump($_POST);
echo '</pre>';
die;*/

// return detail
$MessageID = $_POST['MessageID'];
$Folder = $_POST['Folder'];
$CurTag = $_POST['CurTag'];

// information
$Firstname = $_POST['NewFirstName'];
$Surname = $_POST['NewSurName'];
$UserEmail = stripslashes($_POST['NewEmail']);
$UserEmail = str_replace("'","''",$UserEmail);
$ChooseGroupID = (is_array($_POST['ChooseGroupID']))? $_POST['ChooseGroupID']:array($_POST['ChooseGroupID']);
if (trim($Surname) == "")
{
	$Surame = "''";
}
else
{
	$Surname = stripslashes($Surname);
	$Surname = str_replace("'","''",$Surname);
	$Surname = "'".trim($Surname)."'";
	//$Surname = UTF8_To_DB_Handler($Surname);
}
if (trim($Firstname) == "")
{
	$Firstname = "''";
}
else
{
	$Firstname = stripslashes($Firstname);
	$Firstname = str_replace("'","''",$Firstname);
	$Firstname = "'".trim($Firstname)."'";
	//$Firstname = UTF8_To_DB_Handler($Firstname);	
}

$sql =  " 
				INSERT INTO 
					MAIL_ADDRESS_USER 
					(
					UserID, 
					UserEmail,
					UserType,
					LastUpdated,
					Surname, 
					Firstname 
					) 
				VALUES
					(
					'".$_SESSION["SSV_USERID"]."',
					'".$UserEmail."', 
					'U',
					getdate(),
					".$Surname.",
					".$Firstname."
					)	
				";	
$Result['InsertAddressUser'] = $ldb->db_db_query($sql);
$TmpUserID = $ldb->db_insert_id();	
		
if ($Result['InsertAddressUser'])
{
	for ($i=0;$i<count($ChooseGroupID);$i++)
	{			
		$sql =  " 
					INSERT INTO 
						MAIL_ADDRESS_MAPPING 
						(MailUserID,MailGroupID,LastUpdated) 
					VALUES
						('".$TmpUserID."','".$ChooseGroupID[$i]."',getdate() )	
					";	
		$Result['AddressMapping:'.$ChooseGroupID[$i]] = $ldb->db_db_query($sql);
	}
}

if (!in_array(false,$Result)) {
	$Msg = $Lang['ReturnMsg']['ContactAddSuccess'];
}
else {
	$Msg = $Lang['ReturnMsg']['ContactAddUnsuccess'];
}

intranet_closedb();
header("Location: read_email.php?Msg=".$Msg."&uid=".$MessageID."&Folder=".urlencode($Folder)."&CurTag=".urlencode($CurTag));
exit;
?>
