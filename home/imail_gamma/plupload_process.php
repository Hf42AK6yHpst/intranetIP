<?php
// Editing by 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$lfs = new libfilesystem();

$ComposeTime = $_REQUEST['ComposeTime'];

$personal_path = "$file_path/file/gamma_mail/u$UserID";
$plupload_tmp_path = $personal_path."/".$ComposeTime;

if (!is_dir($personal_path))
{
    $lfs->folder_new($personal_path);
}

if(!is_dir($plupload_tmp_path)) {
	$lfs->folder_new($plupload_tmp_path);
}

$pluploadPar['targetDir'] = $plupload_tmp_path;

include_once($PATH_WRT_ROOT."includes/plupload.php");

intranet_closedb();	
?>