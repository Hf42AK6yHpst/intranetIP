<?php
// editing by 
/******************************** Change Log *****************************************
 * 2014-10-27 [Carlos] : [ip2.5.5.10.1] include base64.js for decoding Folder in SubmitMailAction(), setFlag(), showhidecontent()
 * 2014-03-13 [Carlos] : Check the validity of the uid parameter (i.e. MessageID)
 * 2013-12-03 [Carlos] : Included jquery.alerts, [Move To] changed onchange event to use jAlert to avoid iPad dialog hang problem
 * 2012-09-06 [Carlos] : Added js GetUploadFileToDA_TBForm() for upload attachments to Digital Archive
 * 2012-08-30 [Carlos] : replace wording [Prev] with [Previous mail], [Next] with [Next mail]
 * 2012-07-18 [Carlos] : Added [Back] button to go back view folder page
 * 2011-04-26 [Carlos] : Removed unnecessary function calls e.g. Get_Folder_Structure()
 * 2011-01-25 [Carlos] : Added getting searched result to find prevID, nextID, prevFolder, nextFolder
 * 						 for navigating to previous/next mail when move mail;
 * 						 Added page_from parameter to js function moveEmail();
 * 2010-11-25 [Carlos] : Change feedback message from delete record to delete mail
 *************************************************************************************/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$IMap = new imap_gamma();

$CurrentPageArr['iMail'] = 1;

## Get Data 
$MessageID   = (isset($uid) && $uid != "") ? $uid : '';	
$displayMode = (isset($displayMode) && $displayMode != "") ? $displayMode : "folder";		
$currentPage = (isset($currentPage) && $currentPage != "") ? $currentPage : 0;
$PartNumber = (isset($PartNumber) && $PartNumber != "") ? $PartNumber : "";
//$NoButton = $_GET['NoButton'];

$Msg 	    = (isset($Msg) && $Msg != "") ? $Msg : '';
$Keyword    = (isset($Keyword) && $Keyword != "") ? stripslashes(trim($Keyword)) : '';
if(isset($Folder_b) && $Folder_b!=''){
	$Folder = base64_decode($Folder_b);
}else{
	$Folder     = (isset($Folder) && $Folder != "") ? stripslashes($Folder) : $SYS_CONFIG['Mail']['FolderPrefix'];
}
$FromSearch = (isset($FromSearch) && $FromSearch != "") ? $FromSearch : 0;
$sort 		= (isset($sort) && $sort != "") ? $sort : (($FromSearch == 1) ? "SortDate" : "SORTARRIVAL");	
$pageNo     = (isset($pageNo) && $pageNo != "") ? $pageNo : 1;				
$order    = (isset($order) && $order != "") ? $order : 1;		

# Get Data From Simple Serach 
$keyword 		= (isset($keyword) && $keyword != "") ? stripslashes(trim($keyword)) : '';

# Get Data From Advance Search
$KeywordFrom 	= (isset($KeywordFrom) && $KeywordFrom != "") ? $KeywordFrom : '';
$KeywordTo   	= (isset($KeywordTo) && $KeywordTo != "") ? $KeywordTo : '';
$KeywordCc   	= (isset($KeywordCc) && $KeywordCc != "") ? $KeywordCc : '';
$KeywordSubject = (isset($KeywordSubject) && $KeywordSubject != "") ? $KeywordSubject : '';
$FromDate  		= (isset($FromDate) && $FromDate != "") ? $FromDate : '';
$ToDate  		= (isset($ToDate) && $ToDate != "") ? $ToDate : '';
//$SearchFolder   = (isset($SearchFolder) && is_array($SearchFolder) && count($SearchFolder) > 0) ? $SearchFolder : $IMap->getMailFolders();

if(!$IMap->Go_To_Folder($Folder)){
	intranet_closedb();
	header("Location: index.php");
	exit;
}
$MsgNo = $IMap->Get_MessageID($MessageID);
if($MsgNo == false){
	intranet_closedb();
	header("Location: index.php");
	exit;
}

if (trim($PartNumber) == "") {
	## Preparation
//	$QuerySearchFolder = "";
//	for ($i=0;$i< sizeof($SearchFolder); $i++) {
//		$QuerySearchFolder .= $lui->Get_Input_Hidden("SearchFolder[]","SearchFolder[]", stripslashes($SearchFolder[$i]))."\n";
//	}
	if($FromSearch==1){
		list($CachedUID,$CachedFolder) = $IMap->Get_Cached_Search_Result();
		$this_uid_index = array_search($MessageID,$CachedUID);
		$PrevNextUID['PrevUID'] = $CachedUID[$this_uid_index-1];
		$PrevNextUID['NextUID'] = $CachedUID[$this_uid_index+1];
		$PrevNextFolder['PrevFolder'] = $CachedFolder[$this_uid_index-1];
		$PrevNextFolder['NextFolder'] = $CachedFolder[$this_uid_index+1];
	}else{
		# Get Prev/Next Message UID
		$PrevNextUID = $IMap->Get_Prev_Next_UID($Folder, $sort, $reverse, $MessageID);
	}
	# Get Folder Structure for select box
	//$FolderArray = $IMap->Get_Folder_Structure();
}

# Get Preference (gmail mode)
$Pref = $IMap->Get_Preference();
$GmailMode = 0;

//$li = new libcampusmail2007($CampusMailID);
$lwebmail = new libwebmail();
//$lc = new libcampusquota2007($UserID);
//$outType = ($li->SenderID == $UserID && $li->CampusMailFromID == "");
//$mailFolderID = $li->UserFolderID;

if ($preFolder >= 2)
{
	if (($li->RecordStatus==1) || ($li->RecordStatus==2))
	{
	}
	else
	{
		$iNewCampusMail -= 1;
	}
}

$header_default_style = "/templates/style_mail.css";
$navigation = $i_frontpage_separator.$i_CampusMail_New_iMail;

# tag information
$unreadInboxNo = $iNewCampusMail;
# highlight left menu
//20100812
switch($Folder)
{
	case $IMap->SentFolder	:	
		$CurrentPage = "PageCheckMail_Outbox";	
		$foldericon = "icon_outbox.gif";
		$foldertitle = $i_frontpage_campusmail_outbox;
	break;
	
	case $IMap->DraftFolder	:	
		$CurrentPage = "PageCheckMail_Draft";	
		$foldericon = "icon_draft.gif";
		$foldertitle = $i_frontpage_campusmail_draft;
	break;
	
	case $IMap->TrashFolder	:	
		$CurrentPage = "PageCheckMail_Trash";	
		$foldericon = "icon_trash.gif";
		$foldertitle = $i_frontpage_campusmail_trash;
	break;
	
	case $IMap->SpamFolder	:	
		$CurrentPage = "PageCheckMail_Spam";	
		$foldericon = "icon_spam.gif";
		$foldertitle = $Lang['Gamma']['SystemFolderName']['Junk'];
	break;
	
	case $IMap->InboxFolder		:	
		$CurrentPage = "PageCheckMail_Inbox";	
		$foldericon = "icon_inbox.gif";
		$foldertitle = $i_frontpage_campusmail_inbox;
	break;
	
	default:
		$foldericon = "icon_folder_close.gif";
		$foldertitle = $IMap->getMailDisplayFolder(IMap_Decode_Folder_Name($Folder));
}
$iMailTitle1 = $foldertitle;
$iMailImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/$foldericon' align='absmiddle' border='0' />";

$FolderID = $preFolder;
//$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();
$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();

$iMailTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" ><span class='imailpagetitle'>".$iMailImage1.$iMailTitle1."</span>".$unreadLink."</td><td align='right' style=\"vertical-align: bottom;\" >".$IMap->TitleToolBar()."</td></tr></table>";
$TAGS_OBJ[] = array($iMailTitle, "", 0);

//$linterface = new interface_html("imail_default.html");
$linterface = new interface_html();
$linterface->LAYOUT_START();

if ($msg == "2")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("update")."</td></tr>";
}
else if ($msg == "3")
{
	//$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("delete")."</td></tr>";
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$i_con_gen_msg_email_remove."</font>")."</td></tr>";
}
else
{
	$xmsg = "";
}

$pagebtnTable  = "<table border='0' cellpadding='2' cellspacing='0'>";
$pagebtnTable .= "<tr>";

$prevID = $PrevNextUID["PrevUID"];
$nextID = $PrevNextUID["NextUID"];
$prevFolder = (isset($PrevNextFolder["PrevFolder"]) && $PrevNextFolder["PrevFolder"])?$PrevNextFolder["PrevFolder"]:$Folder;
$nextFolder = (isset($PrevNextFolder["NextFolder"]) && $PrevNextFolder["NextFolder"])?$PrevNextFolder["NextFolder"]:$Folder;

// Back button; go back to previous viewfolder page
if($FromSearch==1){
	$back_btn_link = "search_result.php?FromSearchCache=1";
}else{
	$back_btn_link = "viewfolder.php?Folder=".urlencode($Folder)."&sort=".$sort."&reverse=".$reverse."&FromSearch=".$FromSearch."&pageNo=".$pageNo."&field=".$field;
}
$pagebtnTable .= "<td align='center' valign='middle'><a href=\"".$back_btn_link."\" class=\"tablebottomlink\">".$Lang['Btn']['Back']."</a></td>";
$pagebtnTable .= "<td align='center' valign='middle'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td>";
$pagebtnTable .= "<td align='center' valign='middle'>|</td>";
$pagebtnTable .= "<td align='center' valign='middle'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td>";

if ($prevID!="")
{
	$prevBtn  = "<a href=\"viewmail.php?uid={$prevID}&Folder=".urlencode($prevFolder)."&sort={$sort}&reverse={$reverse}&FromSearch={$FromSearch}&pageNo=".$pageNo."&field=".$field."\" class=\"tablebottomlink\" onMouseOver=\"MM_swapImage('prevp','','{$image_path}/{$LAYOUT_SKIN}/icon_prev_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">";
	$prevBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_prev_off.gif\" name=\"prevp\" width=\"11\" height=\"10\" border=\"0\" align=\"absmiddle\" id=\"prevp\"> ".$Lang['Gamma']['PreviousMail']."</a>";

	$pagebtnTable .= "<td align='center' valign='middle'>{$prevBtn}</td>";
	$pagebtnTable .= "<td align='center' valign='middle'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td>";
	if ($nextID!="") $pagebtnTable .= "<td align='center' valign='middle'>|</td>";
}

if ($nextID!="")
{
	$nextBtn  = "<a href=\"viewmail.php?uid={$nextID}&Folder=".urlencode($nextFolder)."&sort={$sort}&reverse={$reverse}&FromSearch={$FromSearch}&pageNo=".$pageNo."&field=".$field."\" class=\"tablebottomlink\" onMouseOver=\"MM_swapImage('nextp','','{$image_path}/{$LAYOUT_SKIN}/icon_next_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">";
	$nextBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_next_off.gif\" name=\"nextp\" width=\"11\" height=\"10\" border=\"0\" align=\"absmiddle\" id=\"nextp\"> ".$Lang['Gamma']['NextMail']."</a>";

	$pagebtnTable .= "<td align='center' valign='middle'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td>";
	$pagebtnTable .= "<td align='center' valign='middle'>{$nextBtn}</td>";
}

	# gmail pref setting
	//20100812
	if(in_array($Folder,array($IMap->InboxFolder,$IMap->SentFolder))&&trim($PartNumber)=='')
	{
		$GmailChecked = $GmailMode?"checked":""; 
		$pagebtnTable .= "<td align='center' valign='middle'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td>";
		$pagebtnTable .= "<td align='center' valign='middle'><img id='loading_img' src='{$image_path}/{$LAYOUT_SKIN}/indicator.gif' style='display:none'/><input id='gmailmode' type='checkbox' onclick='UpdateGmailMode(this)' $GmailChecked><label for='gmailmode'>".$Lang['Gamma']['DisplayRelatedMail']."</label></td>";
		
		# expand all / collapse all btn
		$ExpandCollapseBtnVis = $GmailMode?"":"style='display:none'";
		$pagebtnTable .= "<td align='center' valign='middle'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td>";
		$pagebtnTable .= "<td id='ExpandCollapseBtn' align='center' valign='middle' ".$ExpandCollapseBtnVis.">";
		$pagebtnTable .= "<a href=\"javascript:void(0);\" onclick=\"ExpandCollapseAllMail(this,'on')\" id=\"expandAll\" class=\"tablebottomlink\">"." + ".$Lang['Gamma']['ExpandAllMails']."</a>";
		$pagebtnTable .= "<a href=\"javascript:void(0);\" onclick=\"ExpandCollapseAllMail(this,'off')\" id=\"collapseAll\" class=\"tablebottomlink\" style=\"display:none\">"." - ".$Lang['Gamma']['CollapseAllMails']."</a>";
		$pagebtnTable .= "</td>";
	}
	else
	{
		$GmailMode = 0;
	}
	
$pagebtnTable .= "</tr>";
$pagebtnTable .= "</table>";

if ($viewtype=="search")
{

	$backBtn  = "<br /><a href=\"javascript:window.history.go(-1)\" class='tablebottomlink' onMouseOver=\"MM_swapImage('page_previous','','{$image_path}/{$LAYOUT_SKIN}/icon_prev_on.gif',1)\" onMouseOut='MM_swapImgRestore()' >";
	$backBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_prev_off.gif\" name='page_previous' id='page_previous' width='11' height='10' border='0' hspace='3' align='absmiddle' >";
	$backBtn .= $button_back."</a><br /><br />";
}
##################

//$DateInfo = strip_tags($li->GET_DATE_FIELD());

?>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<link href="/templates/jquery/jquery.alerts.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript" src="/templates/jquery/jquery.alerts.js"></script>
<script type="text/javascript" language="JavaScript" src="/templates/base64.js"></script>
<script type="text/javascript" language="javascript" >

var gmailmode = <?=$GmailMode?"true":"false"?>;
var folder = '<?=str_replace("'","\'",$Folder)?>';

function SubmitMailAction(Action,ParUID,Folder)
{
	Folder = Base64.decode(Folder);
	document.form1.MailAction.value=Action;
	document.form1.Folder.value=Folder;
	document.form1.action = "compose_email.php";
	document.form1.UID.value = ParUID;

	document.form1.submit();
}

function UpdateGmailMode(obj)
{
	var ObjChecked = obj.checked?1:0;
	$("#loading_img").show();
	$(obj).hide();
	
	if(obj.checked)
		GmailModeSwitch("on")
	else
		GmailModeSwitch("off")
	
	if(hasInitGmailMode)
	{
		$("#loading_img").hide();
		$(obj).show();
	}
	
//	$.post
//	(
//		"ajax_task.php", 
//		{ task: "UpdatePref", GmailMode: ObjChecked },
//	   	function(data){
//	   		if(data.substring(0,1)!=1)
//	    		obj.checked = !obj.checked;
//	    	
//	    	if(hasInitGmailMode)
//	    	{
//				$("#loading_img").hide();
//				$(obj).show();
//	    	}
//			
//	    	Get_Return_Message(data);	    		    	
//	   	}
//	);
	
	
}

function GmailModeSwitch(onoff)
{
	if(onoff=="on")
	{
		if(hasInitGmailMode)
		{
			$("tr.gmailtopbar").show();
			$("td[class*='gmaildesc']").show();
			$("table[class~='gmailcontent']").hide();
			$("td.gmaildesc<?=$uid?>>span").hide();
		}
		else
			ajaxLoadGmailModeEmail();
		
		$('td#ExpandCollapseBtn').show();
	}
	else
	{
		$("tr.gmailtopbar").hide();
		$("td[class*='gmaildesc']>span").show();
		$("table[class*='gmailcontent']").hide();
		$("table.gmailcontent<?=$uid?>").show();
		
		$('td#ExpandCollapseBtn').hide();
	}
}

function setFlag(folder,uid,obj)
{
	folder = Base64.decode(folder);
	var imgsrc = obj.src.substr(obj.src.lastIndexOf("/")+1)
	var setflag = imgsrc=="icon_star_on.gif"?0:1; //set off if on
	if(setflag==1)
		obj.src=obj.src.replace("icon_star_off.gif","icon_star_on.gif")
	else
		obj.src=obj.src.replace("icon_star_on.gif","icon_star_off.gif")

	$.post("ajax_task.php",
		{
			"task"		: "SetMailFlag",
			"Folder"	: folder,
			"Uid"		: uid,
			"setflag"	: setflag
		}
	);

}

var hasInitGmailMode = false;
function ajaxLoadGmailModeEmail()
{
	$("#loading_img").show();
	$("input#gmailmode").hide();
	$.post(
		"ajax_task.php",
		{
			task: 'LoadGmailModeEmailTopBar',
			Folder:'<?=str_replace("'","\'",$Folder)?>',
			MessageID:'<?=$MessageID?>',
			PartNumber:'<?=$PartNumber?>'
		},
		function(data)
		{
			var emailArr = data.split("|=Current_Email_Delimiter=|");
			$("table#MailDisplayTable>tbody").prepend(emailArr[0]);
			$("table#MailDisplayTable>tbody").append(emailArr[1]);
			
			if($("input#gmailmode").attr("checked"))
			{
				$("tr.gmailtopbar").show();
			}
			$("#loading_img").hide();
			$("input#gmailmode").show();
			hasInitGmailMode = true;
		}
	);
}

function showhidecontent(uid, folder,obj)
{
	//if($("table.gmailcontent"+uid).length==0) //if the content has not been loaded
	folder = Base64.decode(folder);
	if($("table[name='gmailcontent"+uid+"_"+folder+"']").length==0)
	{
		//$("tr#loadingrow"+uid).show();
		$("tr[name='loadingrow"+uid+"_"+folder+"']").show();
		$.post(
			"ajax_task.php",
			{
				task	:'LoadGmailModeEmailContent',
				Folder	:folder,
				Uid		:uid
			},
			function(data)
			{
				//if($("table.gmailcontent"+uid).length==0)
				if($("table[name='gmailcontent"+uid+"_"+folder+"']").length==0)
				{
					$("div#loading_content").remove();
					$(obj).after(data);
					//$("td.gmaildesc"+uid+">span").toggle();
					$("td[name='gmaildesc"+uid+"_"+folder+"']>span").toggle();
					//$("tr#loadingrow"+uid).remove();
					$("tr[name='loadingrow"+uid+"_"+folder+"']").remove();
				}
			}
		);		
	}
	else
	{
		//$("td.gmaildesc"+uid+">span").toggle();
		//$("table.gmailcontent"+uid).toggle();
		$("td[name='gmaildesc"+uid+"_"+folder+"']>span").toggle();
		$("table[name='gmailcontent"+uid+"_"+folder+"']").toggle();
	}
}

function ExpandCollapseAllMail(obj,onoff)
{
	topbar_folders = document.getElementsByName("hTopBarFolder[]");
	topbar_uids = document.getElementsByName("hTopBarUid[]");
	if(onoff=='on')
	{
		if($('#gmailmode').attr('checked') && topbar_uids && topbar_folders)
		{
			uid_array = new Array();
			folder_array = new Array();
			for(i=0;i<topbar_uids.length;i++)
			{
				uid = topbar_uids[i].value;
				folder = topbar_folders[i].value;
				if($("table[name='gmailcontent"+uid+"_"+folder+"']").length > 0)
				{
					//$("td.gmaildesc"+uid+">span").hide();
					$("td[name='gmaildesc"+uid+"_"+folder+"']>span").hide();
					$("table[name='gmailcontent"+uid+"_"+folder+"']").show();
				}else
				{
					uid_array.push(uid);
					folder_array.push(encodeURIComponent(folder));
				}
			}
			
			if(uid_array.length>0)
			{
				for(i=0;i<uid_array.length;i++) $("tr[name='loadingrow"+uid_array[i]+"_"+decodeURIComponent(folder_array[i])+"']").show();
				$.post(
					"ajax_task.php",
					{
						"task"	  :'LoadAllGmailModeEmailContent',
						"FolderArray[]" :folder_array,
						"UidArray[]"	  :uid_array
					},
					function(data)
					{
						var emailArr = data.split("|=Mail_Content_Delimiter=|");
						for(i=0;i<uid_array.length;i++)
						{
							uid = uid_array[i];
							folder = decodeURIComponent(folder_array[i]);
							$("tr[name='gmailtopbar"+uid+"_"+folder+"']").after(emailArr[i]);
							//$("td.gmaildesc"+uid+">span").toggle();
							$("td[name='gmaildesc"+uid+"_"+folder+"']>span").toggle();
							$("tr[name='loadingrow"+uid+"_"+folder+"']").remove();
						}
					}
				);
			}
			
			$('#expandAll').hide();
			$('#collapseAll').show();
		}
	}else
	{
		if($('#gmailmode').attr('checked') && topbar_uids)
		{
			for(i=0;i<topbar_uids.length;i++)
			{
				uid = topbar_uids[i].value;
				folder = topbar_folders[i].value;
				if($("table[name='gmailcontent"+uid+"_"+folder+"']").length > 0)
				{
					//$("td.gmaildesc"+uid+">span").show();
					$("td[name='gmaildesc"+uid+"_"+folder+"']>span").show();
					$("table[name='gmailcontent"+uid+"_"+folder+"']").hide();
				}
			}
			$('#collapseAll').hide();
			$('#expandAll').show();
		}
	}
}

function moveEmail(obj)
{
	/*
	if (obj.selectedIndex != 0) 
	{
		if(confirm("<?=$Lang['Gamma']['ConfirmMsg']['ViewMail']['moveMail']?>"))
		{
			obj.form.action='move_email.php?page_from=viewmail.php'; 
			obj.form.submit();
		} 
	}
	obj.selectedIndex=0;
	*/
	if (obj.selectedIndex != 0) {
		jConfirm("<?=$Lang['Gamma']['ConfirmMsg']['ViewMail']['moveMail']?>", 'Confirmation Dialog', function(r) {
		    if(r){
		    	// OK
		    	obj.form.action='move_email.php?page_from=viewmail.php'; 
				obj.form.submit();
		    }else{
		    	// Cancel
		    	obj.selectedIndex=0;
		    }
		});
	}
}

$().ready(function(){
	if(gmailmode)
	{
		$("tr.gmailtopbar").show();
	}


	hasInitGmailMode = false;
	if($("#gmailmode").attr("checked"))
		ajaxLoadGmailModeEmail();
	
});

function viewMessageSource(uid,Folder)
{
	//newWindow("view_message_source.php?Uid="+uid+"&Folder="+encodeURIComponent(Folder),30);
	newWindow("view_message_source.php?Uid="+uid+"&Folder="+Folder,30);
}

function GetUploadFileToDA_TBForm(Folder,UID)
{
	$.post(
		'upload_file_to_DA.php',
		{
			'task':'GetUploadFileToDA_TBForm',
			'Folder':encodeURIComponent(Folder),
			'uid':UID
		},
		function(data){
			$('div#TB_ajaxContent').html(data);
		}
	);
}

</script> 
<form id="form1" name="form1" method="POST" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" >
<tr>
	<td>
	<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<?=$xmsg?>
	<tr>
		<td >
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#EEEEEE">
			<?=$backBtn?>
			<?=$pagebtnTable?>
			</td>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left" class='iMailReadtable' >
		<tr>
			<td width="100%" align="left" valign="top" >
				<?=$IMap->displayMsg($Folder, $MessageID, $PartNumber)?>
			</td>
		</tr>

		</table>
		</td>
	</tr>
	</table>
	<br />
	</td>
</tr>
</table>
<input type="hidden" name="PartNumber" id="PartNumber" value="<?=$PartNumber?>"  />
<input type="hidden" name="CurTag" id="CurTag" value="<?=$CurTag?>"  />
<input type="hidden" name="CurMenu" id="CurMenu" value="<?=$CurMenu?>"  />
<input type="hidden" name="Folder" id="Folder" value="<?=$Folder?>"  />
<input type="hidden" name="Folder_b" id="Folder_b" value="<?=base64_encode($Folder)?>"  />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo?>"  />
<input type="hidden" name="reverse" id="reverse" value="<?=$reverse?>"  />
<input type="hidden" name="sort" id="sort" value="<?=$sort?>"  />
<input type="hidden" name="field" id="field" value="<?=$field?>" />
<input type="hidden" name="DestinationFolder" id="DestinationFolder" value="<?=$DestinationFolder?>"  />
<input type="hidden" name="UID" id="UID" value="<?=$uid?>"  />
<input type="hidden" name="MailAction" id="MailAction"   />
<input type="hidden" name="FromSearch" id="FromSearch" value="<?=$FromSearch?>"  />
<input type="hidden" name="IsAdvanceSearch" id="IsAdvanceSearch" value="<?=$IsAdvanceSearch?>"  />
<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>"  />
<input type="hidden" name="KeywordFrom" id="KeywordFrom" value="<?=$KeywordFrom?>"  />
<input type="hidden" name="KeywordTo" id="KeywordTo" value="<?=$KeywordTo?>"  />
<input type="hidden" name="KeywordCc" id="KeywordCc" value="<?=$KeywordCc?>"  />
<input type="hidden" name="KeywordSubject" id="KeywordSubject" value="<?=$KeywordSubject?>"  />

<input type="hidden" name="FromDate" id="FromDate" value="<?=$FromDate?>"  />
<input type="hidden" name="ToDate" id="ToDate" value="<?=$ToDate?>"  />
</form>
<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>