<?php
// editing by 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['iMail'] = 1;
$CurrentPage = "PageAddressBook_ExternalReceipientGroup";

$lwebmail = new libwebmail();
$IMap = new imap_gamma();
$linterface = new interface_html();

### Title ###

$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($i_CampusMail_External_Recipient_Group, $PATH_WRT_ROOT."home/imail_gamma/groupalias.php?aliastype=1&TabID=3", $TabID==1?0:1);
$TAGS_OBJ[] = array($i_CampusMail_Internal_Recipient_Group, $PATH_WRT_ROOT."home/imail_gamma/groupalias.php?aliastype=0&TabID=1", $TabID==1?1:0);

$linterface->LAYOUT_START();
$PAGE_NAVIGATION[] = array($button_import.($intranet_session_language=="en"?" ":""). $i_CampusMail_External_Recipient_Group, "");
?>
<script language="javascript">
function submitForm(){
	formObj = document.form1;
    if(formObj==null) return;
    
    userfileObj = formObj.userfile;
    //filenameObj = formObj.filename;
    
    if(!check_text(userfileObj, "<?php echo $i_alert_pleasefillin.$i_select_file; ?>.")) return ;
    
    if(userfileObj==null || userfileObj.value=="") return;
    fname = userfileObj.value.split('\\');
	//filenameObj.value= fname[fname.length-1];
	formObj.submit();
}
</script>

<br />
<form name="form1" action="address_group_contact_import_update.php" method="post" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<tr>
	<td colspan="2" align="center"><br />
        <table width="90%" border="0" cellpadding="5" cellspacing="0">
			<tr>
                <td>
                    <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                <tr valign="top">
                                	<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_select_file?> <span class='tabletextrequire'>*</span></span></td>
                                        <td><input class="textboxtext" type="file" name="userfile">
                                        <?= $linterface->GET_IMPORT_CODING_CHKBOX() ?>
                                        </td>
								</tr>
								<tr valign="top">
									<td valign="top" nowrap="nowrap" class="formfieldtitle">&nbsp;</td>
									<td><a target='_blank' class="tablelink" href='<?= GET_CSV("addressbook_groupcontact_sample.csv",$intranet_root."/home/imail_gamma/")?>'><?=$i_general_clickheredownloadsample?></a></td>
								</tr>
					</table>
				</td>
			</tr>
		</table>
        </td>
</tr>   
<tr>
	<td colspan="2" align="center">
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "button", "submitForm()","submitbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "document.form1.reset()","resetbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
                </table> 
        </td>
</tr>             
</table>

<input type="hidden" name="filename" value="">
<input type="hidden" name="TabID" value="<?=$TabID?>">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>