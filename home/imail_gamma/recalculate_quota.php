<?php

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_auth();
intranet_opendb();

$IMap = new imap_gamma();

if($IMap->ConnectionStatus){
	$IMap->recalculateQuota($IMap->CurUserAddress);
}

echo $IMap->ConnectionStatus?"1":"0";

intranet_closedb();
?>