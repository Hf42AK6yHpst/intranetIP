<?php
// page modifing by:  Jason 
/*
*
* This is a page for testing imail without getting cache
* Gamma v0.0.09
*
*/
$PathRelative = "../../../";
$CurSubFunction = "Communication";
$CurPage = $_SERVER["REQUEST_URI"];

include_once($PathRelative."src/include/class/startup.php");


if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}

AuthPage(array("Communication-Mail-GeneralUsage"));

include_once($PathRelative."src/include/class/UserInterface.php");
include_once($PathRelative."src/include/template/general_header.php");

####################################################################################

# Libarary
$lui = new UserInterface();

# Initialization 
$server   = $SYS_CONFIG['Mail']['ServerIP']; 	//203.161.249.36
$port 	  = $SYS_CONFIG['Mail']['Port']; 		//"143/novalidate-cert" ; 
$username = ($_SESSION['SSV_LOGIN_EMAIL']=="" ? ($_SESSION['SSV_EMAIL_LOGIN']."@".$SYS_CONFIG['Mail']['UserNameSubfix']) : $_SESSION['SSV_LOGIN_EMAIL']);	//"hugemail@tg-a.broadlearning.com" ;
$password = $_SESSION['SSV_EMAIL_PASSWORD'];

if ($username == 'tony.chan@mail22.esf.edu.hk' || 
	$username == 'testing1@gs.esf.edu.hk' || 
	$username == 'testing3@esfcentre.edu.hk' || 
	$tusername == 'testing1@esfcentre.edu.hk' || 
	$username == 'testing2@esfcentre.edu.hk' || 
	$username == 'tgcestest1@esfcentre.edu.hk' || 
	$username == 'tgcestest2@esfcentre.edu.hk' || 
	$username == 'tgcestest3@esfcentre.edu.hk') {
	$server = 'email.esf.edu.hk';
}

if ($SYS_CONFIG['Mail']['ServerType'] == "exchange" && 
	$SYS_CONFIG['Mail']['ExchangeUsernameLogin'] && 
	$username != "tgcestest1@esfcentre.edu.hk" && 
	$username != "tgcestest2@esfcentre.edu.hk" && 
	$username != "tgcestest3@esfcentre.edu.hk" && 
	$username != 'testing2@esfcentre.edu.hk' && 
	$username != 'testing3@esfcentre.edu.hk' && 
	$username != 'testing1@esfcentre.edu.hk'){
	$username = substr($username, 0, strpos($username, "@")); //get the username before '@' as the login to exchange server
}

# Get Data 
$mailbox 	 = (isset($_GET["mailbox"])) ? $_GET["mailbox"] : "Inbox";				//SET UP INBOX
$displayMode = (isset($_GET["displayMode"])) ? $_GET["displayMode"] : "folder";		//SET UP DISPLAY
$sort 		 = (isset($_GET["sort"])) ? $_GET["sort"] : "SORTARRIVAL";				//SET UP SORT
$reverse 	 = (isset($_GET["reverse"])) ? $_GET["reverse"] : 1;					//SET UP ORDERING
$currentPage = (isset($_GET["currentPage"])) ? $_GET["currentPage"] : 0;			//SET UP CURRENT PAGE

$Msg = $_GET['Msg'];


# Start Connection 
$imap = imap_open("{" . $server . ":" . $port . "}" . $mailbox, $username, $password);

# Main
echo $lui->Get_Div_Open('module_imail',  'class="module_content"');

?>


<link rel="stylesheet" type="text/css" title="Blue" href="<?=$PathRelative?>/temp_joe/v0.0.09/main.css" />
<style>
img {
	border:0px;
	padding:0px;
}
</style>

<br>
<div id="main">
	<div id="main_inner" class="fixed">
		<!--FOLDERS -->
		<div id="secondaryContent_2columns">
			<div id="columnC_2columns">
				<h4 style="margin-bottom: 0px">Folders</h4>
				<?
				$imap_obj 	 = imap_check($imap);
				$status 	 = imap_status($imap, "{" . $server . "}" . $mailbox, SA_UNSEEN);
				$statusInbox = imap_status($imap, "{" . $server . "}Inbox", SA_UNSEEN);
				
				$list = imap_list($imap, "{" . $server . "}", "*");
				$currentMailboxCount=0 ;
				if (is_array($list)) {
				    print "<ul class='links'>" ;
				    foreach ($list as $val) {
				        print "<li><div>" ;
				        $folder = substr(imap_utf7_decode($val),strlen($server)+2) ;
					    if ($folder=="Inbox") {
					    	echo "<a href='./imail_gamma.php?sort=$sort&reverse=$reverse&mailbox=$folder&displayMode=folder'><b>" . $folder . "</b></a> (" . $statusInbox->unseen . ")<br>" ;
						}
				    	else {
					    	echo "<a href='./imail_gamma.php?sort=$sort&reverse=$reverse&mailbox=$folder&displayMode=folder'><b>" . $folder . "</b></a><br>" ;
				    	}
				    	 print "</div></li>" ;
				    }
				    print "</ul>" ;
				} 
				else {
				    echo "imap_list failed: " . imap_last_error() . "\n";
				}
				?>
				<h4 style="margin-bottom: 0px">Simple Subject Search</h4>
				<form name="input" action="imail_gamma.php" method="get">
					<input type="text" name="search"> <input type="submit" value="Submit">
					<input type="hidden" name="displayMode" value="search">
					<input type="hidden" name="mailbox" value="<? print $mailbox ?>">
				</form>
			</div>
		</div>
		
		<!--MAIN -->
		<div id="primaryContent_2columns">
			<div id="columnA_2columns">
				<h3 style="margin-bottom: 10px">
					<?
					print "<b>$username</b>" ;
					?>
				</h3>
				
				<!-- PRINT MAIL TABLE-->
				<?
				$pageSize = 50 ;
				$display = $pageSize ;
				
				//Folder, message or search?
				if ($displayMode=="folder" OR $displayMode=="message") {
					//$array=imap_sort($imap, $sort, $reverse, 1) ; did not seem to work, so need to do it manually!
					if ($sort=="SORTARRIVAL") {
						$array=imap_sort($imap, SORTARRIVAL, $reverse, 0) ;
					}
					else if ($sort=="SORTSUBJECT") {
						$array=imap_sort($imap, SORTSUBJECT, $reverse, 0) ;
					}
					else if ($sort=="SORTFROM") {
						$array=imap_sort($imap, SORTFROM, $reverse, 0) ;
					}
					else if ($sort=="SORTSIZE") {
						$array=imap_sort($imap, SORTSIZE, $reverse, 0) ;
					}
					else {
						$array=imap_sort($imap, SORTARRIVAL, $reverse, 0) ;
					}
				}
				else if ($displayMode=="search") {
					if (!($_GET["search"]=="")) {
						$array=imap_search($imap,'SUBJECT "' . $_GET["search"] . '"') ;
					}
				}
				
				if (isset($array)) {
					//DECIDE ON NUMBER OF PAGES TO SHOW
					$pageCount = ceil(count($array) / $pageSize) ;
				
					//GET NUMBER OF MESSAGES TO SHOW ON THIS PAGE
					if (count($array) < ($pageSize + ($pageSize * $currentPage))) {
						$display = count($array) - ($pageSize*$currentPage) ;
					}
				}				
				
				//Decide whether to show folder contents or single message
				if ($displayMode=="folder") {
					?>
					<table style="padding-left: 8px; width: 100%">
						<tr>
							<td colspan=4>
								<h4 style="margin-top: 0px">
									<?
									//PRINT FOLDER INFO
									print "<b>$mailbox</b> | " ;
									print $status->unseen . "  <img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_unread_mail.gif'> of " . $imap_obj->Nmsgs . " messages" ;
									print "<br>" ;
									?>
								</h4>
							</td>
							<td colspan=3 align="right">
								<h4 style="margin-top: 0px">
									<?
									//PRINT PAGINATION
									if ($currentPage>0) {
										print "<img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_up_off.gif'> <a href='./imail_gamma.php?sort=$sort&reverse=$reverse&mailbox=$mailbox&displayMode=folder&currentPage=" . ($currentPage-1) . "'>Previous</a> | " ;
									}
									else {
										print "<img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_up_off_light.gif'> <font style='color: #888888'>Previous</font> | " ;
									}
									
									print "Page " . ($currentPage+1) . " of $pageCount" ;
								
									if (($currentPage+1)<$pageCount) {
										print " | <a href='./imail_gamma.php?sort=$sort&reverse=$reverse&mailbox=$mailbox&displayMode=folder&currentPage=" . ($currentPage+1) . "'>Next</a> <img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_down_off.gif'>" ;
									}
									else {
										print " | <font style='color: #888888'>Next </font><img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_down_off_light.gif'>" ;
									}
									?>
								</h4>
							</td>
						</tr>
						<tr>
							<td valign="top" style="width: 2%; font-size: 120%; padding-left: 8px ; padding-right: 8px">
								<font style='color: #1F5582; font-weight: bold'>#</font>
							</td>
							<td valign="top" style="width: 2%; font-size: 120%; padding-left: 8px ; padding-right: 8px">

							</td>
							<td valign="top" style="width: 2%; font-size: 120%; padding-left: 8px ; padding-right: 8px">

							</td>
							<td valign="top" style="width: 44%; font-size: 120%; padding-left: 8px ; padding-right: 8px">
								<?
								if ($sort=="SORTSUBJECT") {
									if ($reverse==0) {
										print "<a class='blue' href='./imail_gamma.php?sort=SORTSUBJECT&reverse=1&mailbox=$mailbox'><b>Subject</b></a> <img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_up_off.gif'>" ;
									}
									else {
										print "<a class='blue'a href='./imail_gamma.php?sort=SORTSUBJECT&reverse=0&mailbox=$mailbox'><b>Subject</b></a> <img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_down_off.gif'>" ;
									}
								}
								else {
									print "<a class='blue' href='./imail_gamma.php?sort=SORTSUBJECT&reverse=0&mailbox=$mailbox'>Subject</a>" ;
								}
								?>
							</td>
							<td valign="top" style="width: 19%; font-size: 120%; padding-left: 8px ; padding-right: 8px">
								<?
								if ($sort=="SORTARRIVAL") {
									if ($reverse==0) {
										print "<a class='blue' href='./imail_gamma.php?sort=SORTARRIVAL&reverse=1&mailbox=$mailbox'><b>Date</b></a> <img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_up_off.gif'>" ;
									}
									else {
										print "<a class='blue' href='./imail_gamma.php?sort=SORTARRIVAL&reverse=0&mailbox=$mailbox'><b>Date</b></a> <img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_down_off.gif'>" ;
									}
								}
								else {
									print "<a class='blue' href='./imail_gamma.php?sort=SORTARRIVAL&reverse=0&mailbox=$mailbox'>Date</a>" ;
								}
								?>
							</td>
							<td valign="top" style="width: 19%; font-size: 120%; padding-left: 8px ; padding-right: 8px">
								<?
								if ($sort=="SORTFROM") {
									if ($reverse==0) {
										print "<a class='blue' href='./imail_gamma.php?sort=SORTFROM&reverse=1&mailbox=$mailbox'><b>Sender</b></a> <img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_up_off.gif'>" ;
									}
									else {
										print "<a class='blue' href='./imail_gamma.php?sort=SORTFROM&reverse=0&mailbox=$mailbox'><b>Sender</b></a> <img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_down_off.gif'>" ;
									}
								}
								else {
									print "<a class='blue' href='./imail_gamma.php?sort=SORTFROM&reverse=0&mailbox=$mailbox'>Sender</a>" ;
								}
								?>
							</td>
							<td valign="top" style="width: 12%; font-size: 120%; padding-left: 8px ; padding-right: 8px">
								<?
								if ($sort=="SORTSIZE") {
									if ($reverse==0) {
										print "<a class='blue' href='./imail_gamma.php?sort=SORTSIZE&reverse=1&mailbox=$mailbox'><b>Size</b></a> <img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_up_off.gif'>" ;
									}
									else {
										print "<a class='blue' href='./imail_gamma.php?sort=SORTSIZE&reverse=0&mailbox=$mailbox'><b>Size</b></a> <img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_down_off.gif'>" ;
									}
								}
								else {
									print "<a class='blue' href='./imail_gamma.php?sort=SORTSIZE&reverse=0&mailbox=$mailbox'>Size</a>" ;
								}
								?>
							</td>
						</tr>
						
						<?
						for ($i=(0+($pageSize*$currentPage)); $i<($display+($pageSize*$currentPage)); $i++) {
							$header = imap_headerinfo($imap, $array[$i], 80, 80);
						
							?>
							<tr>
								<td valign="top" style="padding-left: 8px ; padding-right: 8px">
									<?
									echo $i+1 ;
									?>
								</td>
								<td valign="top" style="padding-left: 8px ; padding-right: 8px">
									<?
									if ($header->Unseen=="U") {
										print "<img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_unread_mail.gif'>" ;
									}
									else {
										print "<img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_open_mail.gif'>" ;
									}
									?>
								</td>
								<td valign="top" style="padding-left: 8px ; padding-right: 8px">
									<?
									if ($header->Answered=="A") {
										print "<img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_reply_mail.gif'>" ;
									}
									?> 
								</td>
								<td valign="top" style="padding-left: 8px ; padding-right: 8px">
									<?
									$subject = $header->fetchsubject;
									$subjectPrint="" ;
									if ($subject!="") {
										if (substr($subject,0,10)=="=?UTF-8?Q?") {
											$subjectPrint= substr($subject,10,-2) ;
										}
										else {
											$subjectPrint= $subject ;
										}
									}
									else {
										$subjectPrint="No subject" ;
									}
									
									if (strlen($subjectPrint)>30) {
										if ($header->Unseen=="U") {
											echo "<b><a href='imail_gamma.php?sort=$sort&reverse=$reverse&mailbox=$mailbox&displayMode=message&messageNo=$array[$i]&messageSeq=$i&currentPage=$currentPage'>" . substr($subjectPrint,0,30) . '...</a></b><br />';
										}
										else {
											echo "<a href='imail_gamma.php?sort=$sort&reverse=$reverse&mailbox=$mailbox&displayMode=message&messageNo=$array[$i]&messageSeq=$i&currentPage=$currentPage'>" . substr($subjectPrint,0,30) . '...</a><br />';
										}
									}
									else {
										if ($header->Unseen=="U") {
											echo "<b><a href='imail_gamma.php?sort=$sort&reverse=$reverse&mailbox=$mailbox&displayMode=message&messageNo=$array[$i]&messageSeq=$i&currentPage=$currentPage'>" . $subjectPrint . '</a></b><br />';
										}
										else {
											echo "<a href='imail_gamma.php?sort=$sort&reverse=$reverse&mailbox=$mailbox&displayMode=message&messageNo=$array[$i]&messageSeq=$i&currentPage=$currentPage'>" . $subjectPrint . '</a><br />';
										}
									}
									?>
								</td>
								<td valign="top" style="padding-left: 8px ; padding-right: 8px">
									<?
									$prettydate = date("H:i d.m.Y", $header->udate);
									echo $prettydate . '<br />';
									?>
								</td>
								<td valign="top" style="padding-left: 8px ; padding-right: 8px">
									<?
									if (strlen($header-> fromaddress)>15) {
										echo substr($header-> fromaddress,0,15) . '...<br />';
									}
									else {
										echo $header-> fromaddress . '<br />';
									}
									?>
								</td>
								<td valign="top" style="padding-left: 8px ; padding-right: 8px">
									<?
									echo $header-> Size . '<br />';
									?>
								</td>
							</tr>
							<?
						}
						?>
					</table>
				<?
				}
				else if ($displayMode=="search") {
					if (!(isset($array))) {
						?>
						<table style="padding-left: 8px; width: 100%">
							<tr>
								<td colspan=7>
									<h4 style="margin-top: 0px">
										<?
										//PRINT FOLDER INFO
										print "Search of <b>$mailbox</b> | " ;
										?>
									</h4>
								</td>
							</tr>
						</table>
							
						<?
						print "<i>Search failed!</i>" ;
					}
					else {
						?>
						<table style="padding-left: 8px; width: 100%">
							<tr>
								<td colspan=4>
									<h4 style="margin-top: 0px">
										<?
										//PRINT FOLDER INFO
										print "Search of <b>$mailbox</b> | " ;
										?>
									</h4>
								</td>
								<td colspan=3 align="right">
									<h4 style="margin-top: 0px">
										<?
										//PRINT PAGINATION
										if ($currentPage>0) {
											print "<img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_up_off.gif'> <a href='./imail_gamma.php?sort=$sort&reverse=$reverse&mailbox=$mailbox&displayMode=search&search=" . $_GET["search"] . "&currentPage=" . ($currentPage-1) . "'>Previous</a> | " ;
										}
										else {
											print "<img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_up_off_light.gif'> <font style='color: #888888'>Previous</font> | " ;
										}
									
										print "Page " . ($currentPage+1) . " of $pageCount" ;
								
										if (($currentPage+1)<$pageCount) {
											print " | <a href='./imail_gamma.php?sort=$sort&reverse=$reverse&mailbox=$mailbox&displayMode=search&search=" . $_GET["search"] . "&currentPage=" . ($currentPage+1) . "'>Next</a> <img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_down_off.gif'>" ;
										}
										else {
											print " | <font style='color: #888888'>Next </font><img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_down_off_light.gif'>" ;
										}
										?>
									</h4>
								</td>
							</tr>
							<tr>
								<td valign="top" style="width: 2%; font-size: 120%; padding-left: 8px ; padding-right: 8px">
									<font style='color: #1F5582; font-weight: bold'>#</font>
								</td>
								<td valign="top" style="width: 2%; font-size: 120%; padding-left: 8px ; padding-right: 8px">

								</td>
								<td valign="top" style="width: 2%; font-size: 120%; padding-left: 8px ; padding-right: 8px">

								</td>
								<td valign="top" style="width: 44%; font-size: 120%; padding-left: 8px ; padding-right: 8px">
									<font style='color: #1F5582; font-weight: bold'>Subject</font>
								</td>
								<td valign="top" style="width: 19%; font-size: 120%; padding-left: 8px ; padding-right: 8px">
									<font style='color: #1F5582; font-weight: bold'>Date</font>
								</td>
								<td valign="top" style="width: 19%; font-size: 120%; padding-left: 8px ; padding-right: 8px">
									<font style='color: #1F5582; font-weight: bold'>From</font>
								</td>
								<td valign="top" style="width: 12%; font-size: 120%; padding-left: 8px ; padding-right: 8px">
									<font style='color: #1F5582; font-weight: bold'>Size</font>
								</td>
							</tr>
							<?
							if ($_GET["search"]=="") {
						
							}
							else {
								for ($i=(0+($pageSize*$currentPage)); $i<($display+($pageSize*$currentPage)); $i++) {
									$header = imap_headerinfo($imap, $array[$i], 80, 80);
						
									?>
									<tr>
										<td valign="top" style="padding-left: 8px ; padding-right: 8px">
											<?
											echo $i+1 ;
											?>
										</td>
										<td valign="top" style="padding-left: 8px ; padding-right: 8px">
											<?
											if ($header->Unseen=="U") {
												print "<img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_unread_mail.gif'>" ;
											}
											else {
												print "<img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_open_mail.gif'>" ;
											}
											?>
										</td>
										<td valign="top" style="padding-left: 8px ; padding-right: 8px">
											<?
											if ($header->Answered=="A") {
												print "<img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_reply_mail.gif'>" ;
											}
											?> 
										</td>
										<td valign="top" style="padding-left: 8px ; padding-right: 8px">
											<?
											$subject = $header->fetchsubject;
											$subjectPrint="" ;
											if ($subject!="") {
												if (substr($subject,0,10)=="=?UTF-8?Q?") {
													$subjectPrint= substr($subject,10,-2) ;
												}
												else {
													$subjectPrint= $subject ;
												}
											}
											else {
												$subjectPrint="No subject" ;
											}
									
											if (strlen($subjectPrint)>30) {
												if ($header->Unseen=="U") {
													echo "<b><a href='imail_gamma.php?sort=$sort&reverse=$reverse&mailbox=$mailbox&displayMode=message&messageNo=$array[$i]&messageSeq=$i&currentPage=$currentPage'>" . substr($subjectPrint,0,30) . '...</a></b><br />';
												}
												else {
													echo "<a href='imail_gamma.php?sort=$sort&reverse=$reverse&mailbox=$mailbox&displayMode=message&messageNo=$array[$i]&messageSeq=$i&currentPage=$currentPage'>" . substr($subjectPrint,0,30) . '...</a><br />';
												}
											}
											else {
												if ($header->Unseen=="U") {
													echo "<b><a href='imail_gamma.php?sort=$sort&reverse=$reverse&mailbox=$mailbox&displayMode=message&messageNo=$array[$i]&messageSeq=$i&currentPage=$currentPage'>" . $subjectPrint . '</a></b><br />';
												}
												else {
													echo "<a href='imail_gamma.php?sort=$sort&reverse=$reverse&mailbox=$mailbox&displayMode=message&messageNo=$array[$i]&messageSeq=$i&currentPage=$currentPage'>" . $subjectPrint . '</a><br />';
												}
											}
											?>
										</td>
										<td valign="top" style="padding-left: 8px ; padding-right: 8px">
											<?
											$prettydate = date("H:i d.m.Y", $header->udate);
											echo $prettydate . '<br />';
											?>
										</td>
										<td valign="top" style="padding-left: 8px ; padding-right: 8px">
											<?
											if (strlen($header-> fromaddress)>15) {
												echo substr($header-> fromaddress,0,15) . '...<br />';
											}
											else {
												echo $header-> fromaddress . '<br />';
											}
											?>
										</td>
										<td valign="top" style="padding-left: 8px ; padding-right: 8px">
											<?
											echo $header-> Size . '<br />';
											?>
										</td>
									</tr>
									<?
								}
							}
							?>
						</table>
					<?
					}
				}
				else if ($displayMode=="message") {
					//Set message as seen
					imap_setflag_full($imap, "2", "\\Seen");
					?>
					<table style="padding-left: 8px; width: 100%">
						<tr>
							<td colspan=3 style="width: 50%">
								<h4 style="margin-top: 0px">
									<?
									//PRINT PAGINATION INFO
									print "<b>$mailbox</b> | " ;
									print ($_GET["messageSeq"]+1) . " of " . $imap_obj->Nmsgs . " messages"  ;
									?>
								</h4>
							</td>
							<td colspan=3 style="width: 50%">
								<h4 style="margin-top: 0px" align="right">
									<?
									//PRINT MESSAGE NAVIGATION
									if ($_GET["messageSeq"]<($imap_obj->Nmsgs-1)) {
										print "<img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_down_off.gif'> <a href='imail_gamma.php?sort=$sort&reverse=$reverse&mailbox=$mailbox&displayMode=message&messageNo=" . $array[($_GET["messageSeq"]+1)] . "&messageSeq=" . ($_GET["messageSeq"]+1) . "&currentPage=$currentPage'>Previous</a> | " ;
									}
									else {
										print "<img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_down_off_light.gif'> <font style='color: #888888'>Previous </font>| " ;
									}
									
									print " <a href='./imail_gamma.php?sort=$sort&reverse=$reverse&mailbox=$mailbox&displayMode=folder&currentPage=" . ($currentPage) . "'>Back to $mailbox</a> " ;
									
									if ($_GET["messageSeq"]>0) {
										print " | <a href='imail_gamma.php?sort=$sort&reverse=$reverse&mailbox=$mailbox&displayMode=message&messageNo=" . $array[($_GET["messageSeq"]-1)] . "&messageSeq=" . ($_GET["messageSeq"]-1) . "&currentPage=$currentPage'>Next</a> <img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_up_off.gif'>" ;
									}
									else {
										print " | <font style='color: #888888'>Next </font><img style='border: 0px' src='".$PathRelative."temp_joe/v0.0.09/images/icon_sort_up_off_light.gif'>" ;
									}
									?>
								</h4>
							</td>
						</tr>
						<tr>
							<td colspan=6>
								<?
								$header = imap_headerinfo($imap, $_GET["messageNo"], 80, 80);
								$body = imap_fetchbody($imap, $_GET["messageNo"], 1);
					
								$subject = $header->fetchsubject;
								if ($subject!="") {
									if (substr($subject,0,10)=="=?UTF-8?Q?") {
										echo "<b>Subject</b>: " . substr($subject,10,-2) . '<br />';
									}
									else {
										echo "<b>Subject</b>: " . $subject . '<br />';
									}
								}
								else {
									echo "<b>Subject</b>: <i>No subject</i><br />" ;
								}
					
								$prettydate = date("H:i d.m.Y", $header->udate);
								echo "<b>Date</b>: " . $prettydate . '<br />';
					
								echo "<b>From</b>: " . $header-> fromaddress . '<br />';
					
								echo "<b>Size</b>: " . $header-> Size . '<br /><br />';
					
								echo nl2br($body) . '<br />';
								?>
							</td>
						</tr>
					</table>
					<?
				}
				?>
				<br class="clear" />
			</div>
		</div>
		<br class="clear" />
	</div>
</div>


<?
echo $lui->Get_Div_Close();

imap_close($imap);

####################################################################################
include_once($PathRelative."src/include/template/general_footer.php");

?>