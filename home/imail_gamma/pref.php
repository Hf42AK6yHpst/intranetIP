<?php
// editing by 
/*
 * 2019-12-11 (Sam): Hide Display Name input according to flag
 * 2014-09-04 (Carlos): validate Reply Email before submit 
 * 2014-03-26 (Carlos): modified default auto save interval to 2 minutes if no preference set
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

$lwebmail = new libwebmail();
$IMap = new imap_gamma();
//if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
//{
//    include_once("../../includes/libsystemaccess.php");
//    $lsysaccess = new libsystemaccess($UserID);
//    if ($lsysaccess->hasMailRight())
//    {
//        $noWebmail = false;
//    }
//    else
//    {
//        $noWebmail = true;
//    }
//}
//else
//{
//    $noWebmail = true;
//}
if($TabID=="") $TabID=3;

#$noWebmail = false;

# Block No webmail no preference
//if ($noWebmail || $UserID=="")
//{
//    header("Location: index.php");
//    exit();
//}

$pref = $IMap->Get_Preference();
$interval = $pref["AutoSaveInterval"]!=''? $pref["AutoSaveInterval"] : 2;
$DisplayName = $pref["DisplayName"];
$ReplyEmail = $pref["ReplyEmail"];

//$navigation = $i_frontpage_separator.$i_frontpage_schoolinfo.$i_frontpage_separator.$i_frontpage_campusmail_campusmail;

$CurrentPage = "PageSettings_PersonalSetting";
$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array("<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$i_CampusMail_New_Settings_PersonalSetting."<td align='right' style=\"vertical-align: bottom;\" >".$IMap->TitleToolBar()."</td></tr></table>", "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

if ($msg == "2")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("update")."</td></tr>";
} else {
	$xmsg = "";
}

?>
<script language='javascript'>
function checkform(obj){
	if(obj==null) return false;
	// var re = /^.+@.+\..{2,3}$/;
    var re = /^([0-9A-Za-z_\.\-])+\@(([0-9A-Za-z\-])+\.)+([0-9A-Za-z]{2,3})+$/;
    var reply_email = $.trim(obj.reply_email.value);
    obj.reply_email.value = reply_email;
    if(reply_email != ''){
    	if(!re.test(reply_email)){
    		alert('<?=$i_invalid_email?>: '+reply_email);
            return false;
    	}
    }
	
	return true;
}
function submitForm(obj){
	if(checkform(obj))
		obj.submit();
}
</script>
<form name="form1" method="post" action="pref_update.php" onsubmit="submitForm(this);return false;">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">	
	<?=$xmsg?>
	<tr>
		<td align="right" ><br><br>
		<table border="0" cellpadding="5" cellspacing="1" align="center" width="90%" class="tabletext" >
		<?php if($SYS_CONFIG['AutoSave']['Mail']['Draft']['Status']){ ?>
		<tr>
			<td width="30%" class="formfieldtitle"><?=$Lang['Gamma']['AutoSaveInterval']?></td>
			<td align="left" colspan="2" class="tabletext"><input type="text" size=2 maxlength=3 name="interval" value="<?=$interval?>"> <?=$Lang['Gamma']['Min']?> <?=$Lang['Gamma']['ZeroToDisable']?></td>
		</tr>
		<?php } ?>
		<?php if(!$sys_custom['iMailPlus']['DisableDisplayName']):?>
		<tr>
			<td valign="top" class="formfieldtitle tabletext" ><?=$Lang['Gamma']['DisplayName']?></td>
	       	<td class="tabletext">
	       	<input type="text" name="display_name" value="<?=$DisplayName?>" class="textboxtext"  ><br />
	       	<?=$i_CampusMail_New_Settings_NoticeDefaultDisplayName?>
	       	</td>
		</tr>
		<?php endif; ?>
		<tr>
			<td valign="top" class="formfieldtitle tabletext" ><?=$Lang['Gamma']['ReplyEmail']?></td>
			<td class="tabletext" ><input type="text" name="reply_email" value="<?=$ReplyEmail?>" class="textboxtext"  ><br />
			<?=$i_CampusMail_New_Settings_NoticeDefaultReplyEmail?>
			</td>
		</tr>
		
		</table>		
		</td>
	</tr>
	</table>
</tr>	
<tr>
	<td>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "") ?>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="TabID" value=1 />
</form>

<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>
