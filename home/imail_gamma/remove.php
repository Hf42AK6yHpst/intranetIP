<?php
// editing by 
/* -------------------------- Change Log ---------------------------------------------
 * 2019-10-22 [Carlos] : Check the existance of a mail UID with imap_gamma.php Get_MessageID() before move the email to avoid warning/error.
 * 2014-10-27 [Carlos] : If pass in folder name is $Folder_b which is Base64 encoded, decode here. 
 * 2012-08-30 [Carlos] : Changed after remove mail, first attempt is go to previous mail, then next mail
 * 2012-05-25 [Carlos] : Added delete log
 * 2011-10-28 [Carlos] : Fixed go back to current viewfolder page
 * 2011-01-25 [Carlos] : Added getting previous and next mails from searched result if FromSearch is set 1 
 * 2010-11-25 [Carlos] : Add go to next mail action if delete from viewmail page
 ------------------------------------------------------------------------------------- */
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libaccess.php");
include("../../includes/imap_gamma.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$IMap = new imap_gamma();


$Folder = $Folder_b!=""?base64_decode($Folder_b): $Folder;
if ($page_from == "viewfolder.php" || $page_from == "viewmail.php")
{
	if(isset($Uid)){
		if(!is_array($Uid)){
			$Uid = array($Uid);
		}
		//after delete one mail, go to the next mail or to folder if no more mails there  
		if($page_from == "viewmail.php"){
			if($FromSearch==1){
				list($CachedUID,$CachedFolder,$CachedSearchFields) = $IMap->Get_Cached_Search_Result();
				$this_uid_index = array_search($Uid[0],$CachedUID);
				$prevID = $CachedUID[$this_uid_index-1];
				$nextID = $CachedUID[$this_uid_index+1];
				$prevFolder = $CachedFolder[$this_uid_index-1];
				$nextFolder = $CachedFolder[$this_uid_index+1];
				// reset cached search result by removing the current removed entry
				$IMap->Set_Cached_Search_Result(array_diff_key($CachedUID,array($this_uid_index=>$CachedUID[$this_uid_index])),
												array_diff_key($CachedFolder,array($this_uid_index=>$CachedFolder[$this_uid_index])),
												$CachedSearchFields);
			}else{
				$PrevNextUID = $IMap->Get_Prev_Next_UID($Folder, $sort, $reverse, $Uid[0]);
				$prevID = $PrevNextUID["PrevUID"];
				$nextID = $PrevNextUID["NextUID"];
				$prevFolder = $Folder;
				$nextFolder = $Folder;
			}
			if($prevID != ""){ // to prev mail
				$url = "viewmail.php?uid=".$prevID."&Folder=".urlencode($prevFolder)."&sort=".$sort."&reverse=".$reverse."&pageNo=".$pageNo."&field=".$field."&FromSearch=".$FromSearch."&msg=3";
			}else if($nextID != ""){// no prev mail, to next mail
				$url = "viewmail.php?uid=".$nextID."&Folder=".urlencode($nextFolder)."&sort=".$sort."&reverse=".$reverse."&pageNo=".$pageNo."&field=".$field."&FromSearch=".$FromSearch."&msg=3";
			}//else - default action : back to folder
		}
		
		$logfolder = '';
		for($i=0; $i<sizeof($Uid);$i++)	
		{
			if(strstr($Uid[$i],","))
				list($thisUid,$thisFolder) = explode(",",$Uid[$i]);
			else
			{
				$thisUid = $Uid[$i];
				$thisFolder = $Folder;
			}
			$logfolder = $thisFolder;
			$IMap->Go_To_Folder($thisFolder);
			$checkedMsgNo = $IMap->Get_MessageID($thisUid);
			if($checkedMsgNo != false){
				$Result[] = $IMap->Move_To_Trash($thisFolder,$thisUid);
			}
		}	

		
		// Log Delete Action
		if(count($Uid)>0){
			if($logfolder != $IMap->RootPrefix."Trash"){
				$log = "Moved ".count($Uid)." mails from $logfolder to trash";
			}else{
				$log = "Deleted ".count($Uid)." mails from trash";
			}
			$IMap->Clean_Delete_Log();
			$IMap->Log_Delete_Action($log);
		}
    }
    
    if($SearchFolder){
	    $url = $_SERVER["HTTP_REFERER"];
    }else if($page_from != "viewmail.php"){
    	$tmp_parts = explode("?",$_SERVER["HTTP_REFERER"]);
    	$url = "viewfolder.php?".$tmp_parts[1];
    	if(count($tmp_parts)>1){
    		if(strstr($tmp_parts[1],"Folder=") === FALSE){
    			$url .= "&Folder=".urlencode($Folder);
    		}
    	}
    	$url .= "&msg=3";
    }
    else{
	    if($url=="")
	    	$url = "viewfolder.php?Folder=".urlencode($Folder)."&msg=3";
    }
//	    if($url!=""){
//		    $x = explode("?",$url);
//		    $url = $x[0];
//	        $url.="?Folder=".$Folder."&msg=3";
//		}
}
//}

intranet_closedb();

if($url=="")
	$url = "viewfolder.php?Folder=".urlencode($Folder)."&sort=".$sort."&reverse=".$reverse."&pageNo=".$pageNo."&field=".$field."&FromSearch=".$FromSearch;

header("Location: $url");
?>