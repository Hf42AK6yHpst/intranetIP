<?php
// page modifing by:
$PathRelative = "../../../../";
$CurSubFunction = "Communication";
$CurPage = $_SERVER["REQUEST_URI"];

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}
AuthPage(array("Communication-Mail-GeneralUsage"));
include_once($PathRelative."src/include/template/general_header.php");
include_once($PathRelative."src/include/class/imap_gamma.php");
include_once($PathRelative."src/include/class/imap_gamma_ui.php");
include_once($PathRelative."src/include/class/imap_preference_ui.php");

######################################################################################################

## User Library
$lui  = new imap_gamma_ui();
$pui  = new imap_preference_ui();
$IMap = new imap_gamma();

## Get Data
$CurMenu = '3';
$CurTag = (isset($CurTag) && $CurTag != "") ? stripslashes(urldecode($CurTag)) : '1';
$Msg 	= (isset($msg) && $msg != "") ? $msg : "";
$Folder = (isset($Folder) && $Folder != "") ? stripslashes(urldecode($Folder)) : $SYS_CONFIG['Mail']['FolderPrefix'];

## Initialization
$updatePref    = false;
$UpdateFail    = false;
$UpdateSuccess = false;
$imapInfo 	   = array();

if (isset($_POST["btnSubmit"]))
{
	$updatePref = true;
	$UpdateValue = array();
	$UpdateValue["MailBoxName"]  = $IMap->CurUserAddress;
	$UpdateValue["DisplayName"]  = Get_Request($_POST["txtDisplayName"]);
	$UpdateValue["ReplyEmail"]   = Get_Request($_POST["txtReplyTo"]);
	$UpdateValue["UseSignature"] = Get_Request($_POST["signature"]);
	$UpdateValue["Signature"] 	 = Get_Request($_POST["txtSignature"]);
	$UpdateValue["DaysInSpam"] 	 = "0";
	$UpdateValue["DaysInTrash"]  = Get_Request($_POST["ddlClearTrash"]);
	$UpdateValue['ReceiveBounceBack']	= Get_Request($_POST['chkReceiveBounceBack']);
	
	$UpdateSuccess = $IMap->Update_Preference($UpdateValue);
	$UpdateFail = !$UpdateSuccess;
}

## Preparation
##############################################################################
# Imap use only
//$imap = imap_open("{" . $server . ":" . $port . "}" . $mailbox, $username, $password);
imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$Folder);		# Go to the Corresponding Folder Currently selected

$imap_obj 	   = imap_check($IMap->inbox);
$unseen_status = imap_status($IMap->inbox, "{".$IMap->host."}".$Folder, SA_UNSEEN);
$statusInbox   = imap_status($IMap->inbox, "{".$IMap->host."}Inbox", SA_UNSEEN);

$imapInfo['imap_obj'] 	   = $imap_obj;
$imapInfo['unseen_status'] = $unseen_status;
$imapInfo['statusInbox']   = $statusInbox;
##############################################################################

# Get Mail Preference
$MailPreference = $IMap->Get_Preference();

# get Folder Structure for select box
$FolderArray = $IMap->Get_Folder_Structure();


if ($UpdateSuccess)
{
	$ReturnMessage = $Lang['ReturnMsg']['UpdatePrefSuccess']; 
}
else if ($UpdateFail)
{
	$ReturnMessage = $Lang['ReturnMsg']['UpdatePrefFail'];
}
else
{
	$ReturnMessage = "";
}

## Main
echo $lui->Get_Div_Open("module_imail", 'class="module_content"');

# Top Tool Bar
echo $lui->Get_Sub_Function_Header($Lang['email_gamma']['HeaderTitle'], $ReturnMessage);
echo $lui->Get_Switch_MailBox();

# Left Menu
echo $lui->Get_Left_Sub_Mail_Menu($CurMenu, $CurTag, $IMap, $Folder, $imapInfo);

# Right Content - Render the preference panel
$CacheFormat = false;
$GammaFormat = true;
echo $pui->Get_Preference_General($MailPreference, $CacheFormat, $GammaFormat);

######################################################################################################
include_once($PathRelative."src/include/template/general_footer.php");

?>