<?php
/*
 * 2014-10-27 (Carlos): Passing in $Folder is base64 encoded. Decoded here. 
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_auth();

$IMap = new imap_gamma();

$Folder = base64_decode($Folder);
$IMap->Go_To_Folder($Folder);
$FetchBody = imap_fetchbody ($IMap->inbox ,  $Uid  ,  '', FT_UID);

echo nl2br(htmlspecialchars($FetchBody));

?>