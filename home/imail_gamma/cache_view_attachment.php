<?php
// page modifing by: 
/*
 * 2016-03-22 (Carlos): added memory usage log.
 * 2014-09-17 (Carlos): use rawurldecode() to decode Folder name instead of urldecode() which has problem of + becomes space
 */
$PATH_WRT_ROOT = "../../";
$CurSubFunction = "Communication";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");


//include_once($PathRelative."src/include/class/startup.php");

intranet_auth();

//AuthPage(array("Communication-Mail-GeneralUsage"));
//include_once($PathRelative."src/include/class/imap_cache_agent.php");

// fetch info
 
$MessageID = $_GET['MessageID'];

// Get Folder Select
//$Folder = IMap_Encode_Folder_Name(stripslashes(urldecode($_GET['Folder'])));
$Folder = stripslashes(rawurldecode($_GET['Folder']));

// Get Attachment Number
$PartNumber = $_GET['PartNumber'];

// IMAP setup
$IMapCache = new imap_cache_agent();

// Get the Email for display
//$EmailContent = $IMapCache->Get_Message_Structure($MessageID,$Folder);
$Attachment = $IMapCache->Get_Attachment($PartNumber,$MessageID,$Folder);
/*echo '<pre>';
var_dump($EmailContent);
echo '</pre>';
die;
$MessageParts = $EmailContent->parts;
for ($i=0; $i< sizeof($MessageParts); $i++) {
	if ($MessageParts[$i]->mime_id == $PartNumber) 
		$AttachmentPart = $MessageParts[$i];
}

// Get Attachment
$Attachment = $IMapCache->Get_Message_Part($PartNumber,$MessageID,$Folder);*/

/*
function octets2char( $input ) {
	if(!function_exists("octets2char_callback")){
         function octets2char_callback($octets) {
                   $dec = $octets[1];
		           if ($dec < 128) {
		             $utf8Substring = chr($dec);
		           } else if ($dec < 2048) {
		             $utf8Substring = chr(192 + (($dec - ($dec % 64)) / 64));
		             $utf8Substring .= chr(128 + ($dec % 64));
		           } else {
		             $utf8Substring = chr(224 + (($dec - ($dec % 4096)) / 4096));
		             $utf8Substring .= chr(128 + ((($dec % 4096) - ($dec % 64)) / 64));
		             $utf8Substring .= chr(128 + ($dec % 64));
		           }
		           return $utf8Substring;
        }
    }
  return preg_replace_callback('|&#([0-9]{1,});|', 'octets2char_callback', $input );                                
  
}

function utf8Encode2($source) {
   $utf8Str = '';
   $entityArray = explode ("&#", $source);
   $size = count ($entityArray);
   for ($i = 0; $i < $size; $i++) {
       $subStr = $entityArray[$i];
       $nonEntity = strpos ($subStr, ';');
       if ($nonEntity !== false) {
			$str = substr($subStr,0,$nonEntity);
			$str = "&#".$str.";";
			$str_end=substr($subStr,$nonEntity+1);

			$convertedStr=rawurlencode(octets2char($str));
			//$convertedStr=octets2char($str);
			
			$utf8Str.=$convertedStr.$str_end;
       }
       else {
           $utf8Str .= $subStr;
       }
   }
   //return preg_replace('/[:\\x5c\\/*?"<>|]/', '_', $utf8Str);
   return $utf8Str;

}


# convert octets to represented chars in UTF-8, see above utf8Encode2()
//$filename = utf8Encode2($Attachment['filename']); 
//$Content = octets2char($Attachment['content']);

### Check user's browser ###
$useragent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
    $browser_version=$matched[1];
    $browser = 'IE';
} elseif (preg_match( '|Opera ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
    $browser_version=$matched[1];
    $browser = 'Opera';
} elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
	$browser_version=$matched[1];
	$browser = 'Firefox';
} elseif(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
	$browser_version=$matched[1];
	$browser = 'Safari';
} else {
	// browser not recognized!
    $browser_version = 0;
    $browser = 'other';
}

$encoded_filename = urlencode($Attachment['FileName']);
$encoded_filename = str_replace("+", "%20", $encoded_filename);

if ($browser == "IE") 
{
	$Attachment['FileName'] = $encoded_filename ;
} 
*/

$IMapCache->Close_Connect();
Output_To_Browser($Attachment['Content'], $Attachment['FileName'], $Attachment['MimeType']);

if(!$sys_custom['iMailPlusDisableMemoryLog'])
{
	$used_memory = memory_get_usage();
	$mem_threshold = isset($sys_custom['iMailPlusMemoryLogLimit']) && $sys_custom['iMailPlusMemoryLogLimit']>0? $sys_custom['iMailPlusMemoryLogLimit'] : 31457280; // default threshold 30MB
	if($used_memory > $mem_threshold)
	{
		$log_path = $file_path.'/file/gamma_mail/memory_usage.log';
		$mem_stat = shell_exec('free -mt');
		$log_handle = fopen($log_path,'a');
		if($log_handle){
			$log_content = sprintf("%s %s %s %dBytes\n%s\n",date("Y-m-d H:i:s"),$_SESSION['SSV_LOGIN_EMAIL'],$_SERVER['REQUEST_URI'],$used_memory,$mem_stat);
			fwrite($log_handle,$log_content);
			fclose($log_handle);
		}
	}
}

?>