<?php
// editing by 
/************************** Change Log **********************************
 * 2018-10-16 (Carlos): Added $SYS_CONFIG['Mail']['ImapServer'] for displaying the real IMAP server domain.
 * Create date: 2011-02-07
 ************************************************************************/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

$lwebmail = new libwebmail();
$IMap = new imap_gamma();

$CurrentPage = "PageSettings_IMAP";
$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array("<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >IMAP<td align='right' style=\"vertical-align: bottom;\" >".$IMap->TitleToolBar()."</td></tr></table>", "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

?>
<form name=form1 method="get" action="">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">	
	<tr>
		<td align="right" ><br><br>
		<table border="0" cellpadding="5" cellspacing="1" align="center" width="90%" class="tabletext" >
		<tr>
			<td colspan="2" class="formfieldtitle"><b><?=$Lang['Gamma']['AccountInformation']?>:</b></td>
		</tr>
		<tr>
			<td width="30%" class="formfieldtitle"><?=$Lang['Gamma']['IMAPMailServer']?>: </td>
			<td align="left" class="tabletext">
			<?php 
			if(isset($SYS_CONFIG['Mail']['ImapServer']) && !empty($SYS_CONFIG['Mail']['ImapServer'])){
				echo $SYS_CONFIG['Mail']['ImapServer'];
			}else if(preg_match('/^192\..*/', $IMap->host)){
			// if host use internal IP, use school domain
				echo $_SERVER['SERVER_ADDR'];
			}else{
				echo $IMap->host;
			}
			?>
			</td>
		</tr>
		<tr>
			<td valign="top" class="formfieldtitle tabletext" ><?=$Lang['Gamma']['ConnectionMethod']?>: </td>
	       	<td class="tabletext">IMAP</td>
		</tr>
		<tr>
			<td valign="top" class="formfieldtitle tabletext" ><?=$Lang['Gamma']['Username']?>: </td>
	       	<td class="tabletext"><?=$IMap->CurUserAddress?></td>
		</tr>
		<tr>
			<td valign="top" class="formfieldtitle tabletext" ><?=$Lang['Gamma']['Password']?>: </td>
	       	<td class="tabletext"><?=$Lang['Gamma']['YourPasswordINeClass']?></td>
		</tr>
		</table>		
		</td>
	</tr>
	</table>
</tr>	
<tr>
	<td>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	</td>
</tr>
</table>
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>

