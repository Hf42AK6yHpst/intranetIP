<?php
// editing by 
// Create Date: 2010-11-16
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$IMap = new imap_gamma();

$MessageID   = (isset($uid) && $uid != "") ? $uid : '';	
$Folder     = (isset($Folder) && $Folder != "") ? base64_decode($Folder) : $SYS_CONFIG['Mail']['FolderPrefix'];
$PartNumber = (isset($PartNumber) && $PartNumber != "") ? $PartNumber : "";

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

echo $IMap->Get_Mail_Print_Version($Folder, $MessageID, $PartNumber);

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
<script type="text/javascript" language="javascript">
	window.print();
</script>