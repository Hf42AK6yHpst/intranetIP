<?php
// Modifying by: 

$PathRelative = "../../../../";
$CurSubFunction = "Communication";
$CurPage = $_SERVER["REQUEST_URI"];

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}
AuthPage(array("Communication-Mail-GeneralUsage"));

include_once($PathRelative."src/include/template/popup_header.php");
include_once($PathRelative."src/include/class/imap_ui.php");
include_once($PathRelative."src/include/class/imap_address_ui.php");
include_once($PathRelative."src/include/class/database.php");
include_once($PathRelative."src/include/class/maze.php");
/*
if (isset($_REQUEST["SchoolCode"]) && $_REQUEST["SchoolCode"] != "")
{
	include($PathRelative."config/" . $_REQUEST["SchoolCode"] . "/system_settings.php");
}
*/

// Modified 2009-08-03
if (isset($_REQUEST["SchoolCode"]) && $_REQUEST["SchoolCode"] != "")
{
	$TmpPath=$PathRelative."config/" . $_REQUEST["SchoolCode"] . "/system_settings.php";
	if(file_exists($TmpPath)) include_once($TmpPath);
}

/*echo '<pre>';
var_dump($_REQUEST);
echo '</pre>';*/

// create UI instance
$lui = new imap_ui();
$LibAddress = new imap_address_ui();

// db instance
$LibDB = new database();
//$LibESF = new database(false,false,true);// Foundation DB
$ESFDBName = $SYS_CONFIG['ESF']['DB']['DBName'];
# default
# iMail is set as Default module using this page
# $isiCal : 1 (iCalendar uses this page)
# $isiCal : 0 (iMail uses this page)
$isiCal = (isset($isiCal) && $isiCal != "") ? $isiCal : 0;
$isiCalEvent = (isset($isiCalEvent) && $isiCalEvent != "") ? $isiCalEvent : 0;

# Set Name of Selection Box
$InputName = ($isiCal == 1) ? "UserID[]" : "Emails[]";

# Set Action of adding Records for icalendar
$UserSubAction = '';
$GroupSubAction = '';
$UserSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['UserID[]'],0);" : "";

# Get current school year based on school
$maze = new maze();
$CurrentSchoolYear = $maze->Get_Current_SchoolYear();

if ($srcForm == "")
{
	$srcForm = "NewMailForm";
}

?>

<script language="javascript">
var UserArr = new Array();

function jRollBackStep(val)
{
	document.forma.RollBackStep.value = val;
	document.forma.submit();
}

<?php
	if($isiCal == 1){	// for iCalendar use only
		$fieldname = ($isiCalEvent == 1) ? $fieldname : "calViewerTable";
?>

function checkOption(obj){
    for(i=0; i<obj.length; i++){
        if(obj.options[i].value== ''){
            obj.options[i] = null;
        }
    }
}

function AddRecordsArray(UserName, UserId){
	var len = UserArr.length;
	UserArr[len] = new Array(2);
	UserArr[len][0] = UserName;
	UserArr[len][1] = UserId;
}

<?php
		if($isiCalEvent == 1){
?>
function AddRecords(obj, type) {
	
	par = window.opener;
	parObj = window.opener.document.form1.elements["<?=$fieldname?>"];
	x = (obj.name == "UserID[]") ? "U" : "G";

	// Case of "Select parent from student"
	//selectedTeachType = document.forma.TeachType.selectedIndex;
	if(x == "G"){
		/*
		if (document.forma.TeachType.options[selectedTeachType].value!="1"){
			if(document.forma.TeachType.options[selectedTeachType].value=="3"){
				x = "P";
			} else {
				selectedUserType2 = document.forma.TeachType2.selectedIndex;

				if (document.forma.TeachType2.options[selectedTeachType2].value=="0")
					x = "C";
				else if (document.forma.TeachType2.options[selectedTeachType2].value=="1")
					x = "S";
				else if (document.forma.TeachType2.options[selectedTeachType2].value=="2")
					x = "E";
			}
		}
		*/
	}

	checkOption(obj);
	par.checkOption(parObj);
	i = obj.selectedIndex;

	while(i!=-1) {
		addtext = obj.options[i].text;
		/*
		if (x == "G")
			addtext += " (<?=$i_eClass_ClassLink?>)";
		else if (x == "C")
			addtext += " (Subject)";
		else if (x == "E")
			addtext += " (House)";
		else if (x == "P")
			addtext += "<?=$iCalendar_ChooseViewer_ParentSuffix?>";
		else if (x == "S")
			addtext +=  " (Section)";
		*/
		if (x == "U"){
			parObj.options[parObj.length] = new Option(addtext, x + obj.options[i].value, false, false);
		}
		obj.options[i] = null;
		i = obj.selectedIndex;
	}
}

<?php
		} else {
?>

// GroupType
// Level 1 : T - Teaching Staff, 	 N - Non-Teaching Staff, B - School-based Group P - Parent
// Level 2 : I - Individual, 	 	 C - Subjects, 			 Y - Year,				S - Section, H - House
// Level 3 : A - All teachers(year), R - RollGroup (Year)
function AddRecords(obj, type) {
	
	par = window.opener;
	parTable = par.document.getElementById("<?=$fieldname?>");
	noOfTr = parTable.tBodies[0].getElementsByTagName("TR");

	x = (obj.name == "UserID[]") ? "U" : "G";

	if(x == "G"){
		if(obj.name == "TeachType"){
			var selIndex = document.forma.TeachType.selectedIndex;
			if (document.forma.TeachType.options[selIndex].value == "0"){
				x = "T";
			} else if (document.forma.TeachType.options[selIndex].value =="1"){
				x = "N";
			} else if (document.forma.TeachType.options[selIndex].value =="5"){
				x = "B";
			} else if (document.forma.TeachType.options[selIndex].value =="6"){
				x = "P";
			}
		} else if(obj.name == "ParentType") {
			var selIndex = document.forma.ParentType.selectedIndex;
			if (document.forma.ParentType.options[selIndex].value == "2"){
				x = "R";
		} else if(obj.name == "TeachType2"){
			var selIndex = document.forma.TeachType2.selectedIndex;
			if (document.forma.TeachType2.options[selIndex].value == "0"){
				x = "I";
			} else if (document.forma.TeachType2.options[selIndex].value == "1"){
				x = "C";
			} else if (document.forma.TeachType2.options[selIndex].value == "2"){
				x = "Y";
			} else if (document.forma.TeachType2.options[selIndex].value == "3"){
				//x = "S";
			} else if (document.forma.TeachType2.options[selIndex].value == "4"){
				//x = "H";
			}
		} else if(obj.name == "TeachSubject"){
			var selIndex = document.forma.TeachSubject.selectedIndex;
			x = "C";
		} else if(obj.name == "SchoolYear"){
			var selIndex = document.forma.SchoolYear.selectedIndex;
			x = "Y";
		} else if(obj.name == "SchoolGroup"){
			var selIndex = document.forma.SchoolGroup.selectedIndex;
			x = "B";
		} else if(obj.name == "Year_Teacher_Type"){
			hasValidID = 0;
			var selIndex = document.forma.Year_Teacher_Type.selectedIndex;
			x = "R";
		}

		var teach_type="", parent_type = "", teach_type2="", school_group="", teach_subject="", school_year="", year_tea_type="";
		if(document.getElementById("TeachTypePath"))
			teach_type = document.getElementById("TeachTypePath").value;
		if(document.getElementById("ParentTypePath"))
			parent_type = document.getElementById("ParentTypePath").value;
		if(document.getElementById("TeachType2Path"))
			teach_type2 = document.getElementById("TeachType2Path").value;
		if(document.getElementById("SchoolGroupPath"))
			school_group = document.getElementById("SchoolGroupPath").value;
		if(document.getElementById("TeachSubjectPath"))
			teach_subject = document.getElementById("TeachSubjectPath").value;
		if(document.getElementById("SchoolYearPath"))
			school_year = document.getElementById("SchoolYearPath").value;
		if(document.getElementById("Year_Teacher_TypePath"))
			year_tea_type = document.getElementById("Year_Teacher_TypePath").value;

	}
	checkOption(obj);
	i = obj.selectedIndex;

	while (i!=-1) {

		pathText = '';
		nameText = obj.options[i].text;
		if(x != "U"){
			if(teach_type != ""){
				if(teach_type == "T"){
					pathText += teach_type;
					if(obj.name == "TeachType2"){
						pathText += ":=:" + x;
						nameText += " (<?=$Lang['email']['TeachingStaff']?>)";
					} else if(teach_type2 != ""){
						pathText += ":=:" + teach_type2;
						if(teach_type2 == "C"){
							pathText += obj.options[i].value;
							nameText += " (<?=$Lang['email']['TeachingStaff']?>)";
						} else if(teach_type2 == "Y"){
							if(obj.name == "SchoolYear"){
								pathText += obj.options[i].value;
								nameText += " (<?=$Lang['email']['School_Year'].' - '.$Lang['email']['TeachingStaff']?>)";
							} else {
								pathText += school_year;
								if(year_tea_type == ""){
									nameText += " ("+school_year+" - <?=$Lang['email']['School_Year']?>)";
									if(obj.options[i].value == "0"){
										pathText += ":=:" + "A";
									} else if(obj.options[i].value == "1"){
										pathText += ":=:" + "R";
									}
								}
							}
						}
					}
				} else if(teach_type == "B"){
					pathText += teach_type;
					if(obj.name == "SchoolGroup"){
						pathText += ":=:" + x + obj.options[i].value;
						nameText += " (<?=$Lang['email']['SchoolBasedGroup']?>)";
					}
				} else if(teach_type == "P"){
					pathText += teach_type;
					if(obj.name == "RollGroup"){
						pathText += ":=:" + x + obj.options[i].value;
						nameText += " (<?=$Lang['email']['RollGroups']?>)";
						if(obj.name == "StudentName"){
							// nameText += " (<?=$Lang['email']['Student']?>)";
							// pathText += ":=:" + x + obj.options[i].value;
						}
					}
				}
			} else {
				pathText += x;
			}
			
		}

		firstNode = "<a href='#' class='imail_entry_read_link'><img src='<?=$ImagePathAbs?>imail/icon_ppl_ex.gif' width='20' height='20' border='0' align='absmiddle'>";
		firstNode += nameText;
		firstNode += "</a>";
		//newName = par.document.createTextNode(nameText);

		if(x == 'U'){
			var viewerID   = x + obj.options[i].value;
			var permission = viewerID;
		} else {
			var viewerPath = pathText;
			var permission = viewerPath;
		}
		selPermission  = "<select name='permission-" + permission + "' id='permission-" + permission + "'>";
		selPermission += "<option value='W'><?=$Lang['calendar']['SharePermissionEdit']?></option>";
		selPermission += "<option value='R' selected><?=$Lang['calendar']['SharePermissionRead']?></option>";
		selPermission += "</select>";

		delViewer  = "<span onclick='removeViewer(this)' class='tablelink'><a href='javascript:void(0)'><?=$Lang['btn_delete']?></a></span>";
		if(x == 'U'){
			delViewer += "<input type='hidden' value='" + viewerID + "' name='viewerID[]' />";
		} else {
			delViewer += "<input type='hidden' value='" + viewerPath + "' name='viewerPath[]' />";
		}

		newRow = par.document.createElement("tr");
		//newRow.className = firstRowClass;

		newCellName = par.document.createElement("td");
		newCellName.className = "cal_agenda_content_entry";
		newCellName.nowrap = true;
		newCellName.innerHTML = firstNode;

		newCellPermission = par.document.createElement("td");
		newCellPermission.className = "cal_agenda_content_entry";
		newCellPermission.innerHTML = selPermission;

		newCellDelete = par.document.createElement("td");
		newCellDelete.className = "cal_agenda_content_entry cal_agenda_content_entry_button";
		newCellDelete.innerHTML = delViewer;

		newRow.appendChild(newCellName);
		newRow.appendChild(newCellPermission);
		newRow.appendChild(newCellDelete);

		parTable.tBodies[0].appendChild(newRow);

		obj.options[i] = null;
		i = obj.selectedIndex;

	}
}

<?php
		}
	} else {	// for iMail use only
?>
tempNo = 0;

function AddRecords(TmpStr)
{	
	var ParWindow = window.opener;
	var ParObj2;
	
	if (<?php echo empty($startFrom)? "false": "true"?>){
		if (tempNo != 0)
			return;
		items = "";
		<?php
			if (count($Emails) > 0){
				$schooldb = "";
				for ($i = 0; $i < count($SYS_CONFIG['school']); $i++){
					if ($SYS_CONFIG['school'][$i][0] == $_REQUEST["SchoolCode"]){
						$schooldb = $SYS_CONFIG['school'][$i][4];
					}
				}
				$allUserID = implode(",",$Emails);
				
				$sql = 'select * from '.$schooldb .'.dbo.INTRANET_USER where UserID in ('.$allUserID.')';
				
				$result = $LibDB->returnArray($sql);
				$itemsArray = "items = Array(";
				$nameArray = "guestName = Array(";
				for ($i = 0; $i < count($result); $i++){
					if ($i > 0){
						$itemsArray .=","; 
						$nameArray .=","; 
					}
					$nameArray .= '"'.$result[$i]["Surname"].", ".$result[$i]["FirstName"].'"';
					
					if ($_REQUEST["SchoolCode"] == $_SESSION["SchoolCode"])
						$itemsArray .= '"'.$result[$i]["UserID"].'"';
					else
						$itemsArray .= '"'.$result[$i]["UserID"]."-".$_REQUEST["SchoolCode"].'"';
					
					
				}
				$itemsArray .= ");\n";
				$nameArray .= ");\n";
				echo $itemsArray;
				echo $nameArray;
				echo "tempNo=\"".rand()."\"";
			}
		?>
		
		
		if (items != "" && items.length > 0){
			target = window.opener.document.getElementById("viewerList");
			par = window.opener;
			for (i = 0; i < items.length; i++){
				//parts = items[i].split("-");
				/*opt = document.createElement("option");
				opt.text = guestName[i];
				opt.value = "U"+items[i];
				try{
					target.add(opt, null);
				}
				catch(ex){
					target.add(opt);
				}*/
				par.jAdd_Options_Record(target.length, guestName[i], "U"+items[i]);
				
			}
			target = window.opener.document.getElementById("saveNsend").disabled = false;
		}
	}else{
		if (ParWindow)
		{
			var ParObj2 = window.opener.document.forms["<?=$srcForm?>"].elements["<?php echo $fromSrc; ?>"];
		}

		var TmpRtrim = ParObj2.value.replace(/\s+$/,"");
		if (TmpRtrim == "")
			ParObj2.value = TmpStr + ";";
		else if (TmpRtrim.charAt(TmpRtrim.length - 1) == ";")
			ParObj2.value += TmpStr + ";";
		else
			ParObj2.value += ";" + TmpStr + ";";
	}
}	

function SubmitRecords() {
	var ParWindow = window.opener;
	var frmSource = "<?=$fromSrc?>";
	
	if(ParWindow && frmSource == "ToSchoolAddress") {
		var formObj = window.opener.document.forms["<?=$srcForm?>"];
		var chooseType = window.opener.document.getElementById("ChooseType");
		
		if(chooseType && (chooseType.value == '3' || chooseType.value == 3))
		{
			if(formObj.ChooseGroupID.value != "") {
				formObj.action = "address_edit_group_foundation_new_user_process.php";
			}else{
				formObj.action = "address_add_group_foundation_new_user_process.php";	
			}
			formObj.submit();
		}else
		{
			if(formObj.ChooseGroupID.value != "") {
				formObj.action = "address_edit_group_school_new_user_process.php";
			}else{
				formObj.action = "address_add_group_school_new_user_process.php";	
			}
			formObj.submit();
		}
		//formObj.submit();	
	}
	/*
	if(ParWindow && frmSource == "ToSchoolAddress") {
		var formObj = window.opener.document.forms["<?=$srcForm?>"];
		if(formObj.ChooseGroupID.value != "") {
			formObj.action = "cache_address_edit_group_school_new_user_process.php";
		}else{
			formObj.action = "cache_address_add_group_school_new_user_process.php";	
		}
		//formObj.TempAction.value = "add";
		formObj.submit();	
	}
	*/
	if(ParWindow && frmSource == "ToUserAddress") {
		var formObj = window.opener.document.forms["<?=$srcForm?>"];
		if(formObj.ChooseGroupID.value != "") {
			formObj.action = "address_edit_group_new_user_process.php";
		}else{
			formObj.action = "address_add_group_new_user_process.php";	
		}
		formObj.submit();	
	}
	
}

function ChangeHiddenElement(TmpEle, TmpValue)
{	
	var form = document.forma;
	for (var i=0; i<form.elements.length; i++) {
		if ((form.elements[i].type == "hidden") && (form.elements[i].id == TmpEle))
		{
			form.elements[i].value = TmpValue;
		}
	}
}

function Collect_Student_List(TmpEle, SrcEle) {
	var input_str = '';
	
		for (var i = 0; i < SrcEle.length; i++) {
			if (SrcEle.options[i].selected == true) {
				input_str += SrcEle.options[i].value + '|';
				
			}
		}
		
	var len = input_str.length;	
		document.getElementById(TmpEle).value = input_str.substring(0, len -1);
		
}

<?php
	}
?>

function jAdd_ACTION(jParAct, jParSrc)
{	
	document.forma.specialAction.value = jParAct;
	document.forma.actionLevel.value = jParSrc;	
	document.forma.submit();
}

</script>


<?php

$LoadJavascript = "";
// debug_r($_POST);


////////////////////////////////////////////////////
//	Step1: Fixed
///////////////////////////////////////////////////
$Step1Arr = array();
if ((($_REQUEST["SchoolCode"] == $_SESSION["SchoolCode"]) && Auth(array("MySchool-TeachingStaff"), "TARGET")) ||
	(($_REQUEST["SchoolCode"] != $_SESSION["SchoolCode"]) && Auth(array("AllSchool-TeachingStaff"), "TARGET")))
{
	$Step1Arr[] = array($Lang['email']['TeachingStaff'],0);	
}

if ((($_REQUEST["SchoolCode"] == $_SESSION["SchoolCode"]) && Auth(array("MySchool-NonTeachingStaff"), "TARGET")) ||
	(($_REQUEST["SchoolCode"] != $_SESSION["SchoolCode"]) && Auth(array("AllSchool-NonTeachingStaff"), "TARGET")))
{	
	$Step1Arr[] = array($Lang['email']['NonTeachingStaff'],1);
}

if (empty($startFrom)){
	if($isiCal == 0 && ((($_REQUEST["SchoolCode"] == $_SESSION["SchoolCode"]) && Auth(array("MySchool-Student"), "TARGET")) ||
		(($_REQUEST["SchoolCode"] != $_SESSION["SchoolCode"]) && Auth(array("AllSchool-Student"), "TARGET"))))
	{
		$Step1Arr[] = array($Lang['email']['Student'],2);
	}
}

$Step1Select = $lui->Get_Input_Select("TeachType","TeachType",$Step1Arr,$TeachType,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:ChangeHiddenElement('TeachType', this.value);\" ");

$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['TeachType'], 1);" : "";
$ActionStep1 = "onclick=\"this.form.action='select_recipient_foundation.php#step2';".$GroupSubAction."jAdd_ACTION('add','TeachType')\"";
///////////////////////////////////////////////////

$IsEnding = false;
$IsEnding2 = false;
$IsEnding3 = false;
$IsEnding4 = false;
$IsEnding5 = false;
$IsEnding7 = false; // 2009-08-04

if ($TeachType == "1"){
	// Non-teaching staff

	$Sql =  	"
					exec Search_StaffEmail
					@Staff_Type='N,',
					@Roll_Group=',',
					@School_Year =',',
					@House=',' ,
					@SubjectName=',' ,
					@TTPeriod= '".$CurrentSchoolYear.",'
				";
	$ReturnArr = $LibDB->returnArray($Sql);

	if (($specialAction == "add") && ($actionLevel == "step1_2"))
	{
		if (count($Emails) > 0) {
			$LoadJavascript .= "<script language='javascript' > ";
		}
	}
	$OutputArr = array();
	for ($i=0;$i<count($ReturnArr);$i++)
	{
		if($isiCal == 1)
		{	# iCalendar use
			$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
		} else
		{	# iMail use
			if (!empty($startFrom)){
				if (empty($OutputArr) || $ReturnArr[$i]["UserID"] != $OutputArr[$i-1][1])
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
			}else
				$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
		}

		if (($specialAction == "add") && ($actionLevel == "step1_2"))
		{
			if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
			{
				$TmpName = $ReturnArr[$i]["OfficialFullName"];
				$TmpEmail = $ReturnArr[$i]["Email"];
				$TmpID = $ReturnArr[$i]["UserID"];
				$TmpSchoolCode = $_REQUEST["SchoolCode"];
				
				if ($TmpName != "")
				{
					$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
					if($isiCal != 1 && $srcForm == 'NewUserForm') { 
						$TmpStr .= " \"".$TmpID."\"";
					}
					$TmpStr .= " \"".$TmpSchoolCode."\"";
				}
				else
				{
					$TmpStr = "<".$TmpEmail.">";
					$TmpStr .= " \"".$TmpSchoolCode."\"";
				}
				$TmpStr = str_replace("'","\'",$TmpStr);
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
			}
		}
	}

	$ActionStep2 = "onclick=\"this.form.action='select_recipient_foundation.php#step2';".$UserSubAction."jAdd_ACTION('add','step1_2')\"";
	if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "step1_2")))
	{
		$Step2Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
		$Step2Select .= '<br>';
		$Step2Select .= $Lang['email']['MultiSelectMsg'];
	}
	$IsEnding2 = true;
	if (($specialAction == "add") && ($actionLevel == "step1_2"))
	{
		if (count($Emails) > 0)
		{
			if($isiCal != 1 && $srcForm == 'NewUserForm') {
				$LoadJavascript .= "SubmitRecords(); \n";
			}
			
			$LoadJavascript .= "</script> ";
		}
	}
}

else if ($TeachType == "0"){
// Teaching staff
	///////////////////////////////////////////////////
	//	Step2: Fixed
	///////////////////////////////////////////////////
	$Step2Arr = array();
	$Step2Arr[] = array($Lang['email']['Individual'],0);
	$Step2Arr[] = array($Lang['email']['Subjects'],1);
	$Step2Arr[] = array($Lang['email']['Years'],2);
	//$Step2Arr[] = array($Lang['email']['Sections'],3);
	//$Step2Arr[] = array($Lang['email']['Houses'],4);
	$Step2Arr[] = array($Lang['email']['RollGroups'],5);
	$Step2Arr[] = array($Lang['email']['ByStudents'],6);
	if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "TeachType2")))
	{
		$Step2Select = $lui->Get_Input_Select("TeachType2","TeachType2",$Step2Arr,$TeachType2,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:ChangeHiddenElement('TeachType2', this.value);\" ");
	}
	$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['TeachType2'], 1);" : "";
	$ActionStep2 = "onclick=\"this.form.action='select_recipient_foundation.php#step3';".$GroupSubAction."jAdd_ACTION('add','TeachType2')\"";
	///////////////////////////////////////////////////

	////////////////////////////////////////////////////
	//	Step3: From DB
	///////////////////////////////////////////////////
	if ($TeachType2 == "0"){
		// individual
			$Sql =  	"
					exec Search_StaffEmail
					@Staff_Type='T,',
					@Roll_Group=',',
					@School_Year =',',
					@House=',' ,
					@SubjectName=',' ,
					@TTPeriod= '".$CurrentSchoolYear.",'
				";
		$ReturnArr = $LibDB->returnArray($Sql);

		if (($specialAction == "add") && ($actionLevel == "step1_2"))
		{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "<script language='javascript' > ";				
			}
		}
		$OutputArr = array();
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			if($isiCal == 1)
			{	# iCalendar use
				$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
			} else
			{	# iMail use
				if (!empty($startFrom)){
					if (empty($OutputArr) || $ReturnArr[$i]["UserID"] != $OutputArr[$i-1][1])
						$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
				}else
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
			}

			if (($specialAction == "add") && ($actionLevel == "step1_2"))
			{
				if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
				{
					$TmpName = $ReturnArr[$i]["OfficialFullName"];
					$TmpEmail = $ReturnArr[$i]["Email"];
					$TmpID = $ReturnArr[$i]["UserID"];
					$TmpSchoolCode = $_REQUEST["SchoolCode"];
				
				if ($TmpName != "")
				{
					$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
					if($isiCal != 1 && $srcForm == 'NewUserForm') { 
						$TmpStr .= " \"".$TmpID."\"";
					}
					$TmpStr .= " \"".$TmpSchoolCode."\"";
				}
				else
				{
					$TmpStr = "<".$TmpEmail.">";
					$TmpStr .= " \"".$TmpSchoolCode."\"";
				}
					$TmpStr = str_replace("'","\'",$TmpStr);
					$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
				}
			}
		}
		$ActionStep3 = "onclick=\"this.form.action='select_recipient_foundation.php#step3';".$UserSubAction."jAdd_ACTION('add','step1_2')\"";
		if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "step1_2")))
		{
			$Step3Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 200px\" ");
			$Step3Select .= '<br>';
			$Step3Select .= $Lang['email']['MultiSelectMsg'];
		}
		$IsEnding3 = true;
		if (($specialAction == "add") && ($actionLevel == "step1_2"))
		{
			if (count($Emails) > 0)
			{
				if($isiCal != 1 && $srcForm == 'NewUserForm') {
					$LoadJavascript .= "SubmitRecords(); \n";
				}
				$LoadJavascript .= "</script> ";
			}
		}
	} // end if individual
	
	else if ($TeachType2 == "1"){
		//$Sql =  "exec Get_SubjectList_ByTTPeriod @TTPeriod='".$_SESSION['SSV_SCHOOLYEAR']."' ";
		$Sql =  "exec Get_SubjectNameList_ByTTPeriod @TTPeriod='".$CurrentSchoolYear."' ";
		$ReturnArr = $LibDB->returnArray($Sql);

		$OutputArr = array();
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$OutputArr[] = array($ReturnArr[$i]["FullName"], $ReturnArr[$i]["Subject_Code"]);
		}
		if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "TeachSubject")))
		{
			$Step3Select = $lui->Get_Input_Select("TeachSubject","TeachSubject",$OutputArr,$TeachSubject,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:ChangeHiddenElement('TeachSubject', this.value);\" ");
		}
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['TeachSubject'], 1);" : "";
		$ActionStep3 = "onclick=\"this.form.action='select_recipient_foundation.php#step4';".$GroupSubAction."jAdd_ACTION('add','TeachSubject')\"";

		////////////////////////////////////////////////////
		//	Step4: From DB
		///////////////////////////////////////////////////

		if (($specialAction == "add") && ($actionLevel == "step2_4"))
		{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "<script language='javascript' > ";
			}
		}

		if ($TeachSubject != "")
		{
			$Sql =  	"
							exec Search_StaffEmail
							@Staff_Type=',',
							@Roll_Group=',',
							@School_Year =',',
							@House=',' ,
							@SubjectName='".$TeachSubject.",' ,
							@TTPeriod= '".$CurrentSchoolYear.",'
						";
			$ReturnArr = $LibDB->returnArray($Sql);
			$OutputArr = array();
			for ($i=0;$i<count($ReturnArr);$i++)
			{
				if($isiCal == 1)
				{	# iCalendar use
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
				} else
				{	# iMail use
					if (!empty($startFrom)){
						if (empty($OutputArr) || $ReturnArr[$i]["UserID"] != $OutputArr[$i-1][1])
							$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
					}else
						$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
				}

				if (($specialAction == "add") && ($actionLevel == "step2_4"))
				{
					if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
					{
						$TmpName = $ReturnArr[$i]["OfficialFullName"];
						$TmpEmail = $ReturnArr[$i]["Email"];
						$TmpID = $ReturnArr[$i]["UserID"];
						$TmpSchoolCode = $_REQUEST["SchoolCode"];
				
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
							if($isiCal != 1 && $srcForm == 'NewUserForm') { 
								$TmpStr .= " \"".$TmpID."\"";
							}
							$TmpStr .= " \"".$TmpSchoolCode."\"";
						}
						else
						{
							$TmpStr = "<".$TmpEmail.">";
							$TmpStr .= " \"".$TmpSchoolCode."\"";
						}
						$TmpStr = str_replace("'","\'",$TmpStr);
						$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
					}
				}
			}
			if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "step2_4")))
			{
				$Step4Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$Step4Select .= '<br>';
				$Step4Select .= $Lang['email']['MultiSelectMsg'];
			}
			$IsEnding4 = true;
		}
		$ActionStep4 = "onclick=\"this.form.action='select_recipient_foundation.php#step5';".$UserSubAction."jAdd_ACTION('add','step2_4')\"";

		if (($specialAction == "add") && ($actionLevel == "step2_4"))
		{
			if (count($Emails) > 0)
			{
				if($isiCal != 1 && $srcForm == 'NewUserForm') {
					$LoadJavascript .= "SubmitRecords(); \n";
				}
				$LoadJavascript .= "</script> ";
			}
		}

		///////////////////////////////////////////////////
	} // end if Subjects
	
	else if ($TeachType2 == "2"){
		$Sql =  "exec Get_SchoolYearList ";
		$ReturnArr = $LibDB->returnArray($Sql);

		$OutputArr = array();
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$OutputArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);
		}
		if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "SchoolYear")))
		{
			$Step3Select = $lui->Get_Input_Select("SchoolYear","SchoolYear",$OutputArr,$SchoolYear,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:ChangeHiddenElement('SchoolYear', this.value);\" ");
		}
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['SchoolYear'], 1);" : "";
		$ActionStep3 = "onclick=\"this.form.action='select_recipient_foundation.php#step4';".$GroupSubAction."jAdd_ACTION('add','SchoolYear')\"";


		if ($SchoolYear != "")
		{
			$sqlSchoolYear = "exec Get_RollGroupList_BySchoolYear @School_Year=".$SchoolYear;
			$ReturnArr = $LibDB->returnArray($sqlSchoolYear);
			if(count($ReturnArr) > 0) {
				for($i=0; $i<count($ReturnArr); $i++) {
					$RollGroupList .= $ReturnArr[$i]['Roll_Group'].',';	
				}
			}else {
				$RollGroupList = ',';
			}			
			
			$Sql = "
				exec Search_StaffEmail
				@Staff_Type=',',
				@Roll_Group='".$RollGroupList."',
				@Roll_Group_Year = ',', 
				@School_Year ='".$SchoolYear.",',
				@House=',' ,
				@SubjectName=',' ,
				@TTPeriod= '".$CurrentSchoolYear.",'
			";
			// print $Sql;
			$ReturnArr = $LibDB->returnArray($Sql);

				/*echo "<pre>";
				var_dump($Sql);
				echo "</pre>";*/

				//echo "<pre>";
				//var_dump($ReturnArr);
				//echo "</pre>";

			if (($specialAction == "add") && ($actionLevel == "step3_4"))
			{
				if (count($Emails) > 0)
				{
					$LoadJavascript .= "<script language='javascript' > ";
				}
			}

			$OutputArr = array();
			for ($i=0;$i<count($ReturnArr);$i++)
			{
				if($isiCal == 1)
				{	# iCalendar use
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
				} else
				{	# iMail use
					if (!empty($startFrom)){
						if (empty($OutputArr) || $ReturnArr[$i]["UserID"] != $OutputArr[$i-1][1])
							$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
					}else
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
				}

				if (($specialAction == "add") && ($actionLevel == "step3_4"))
				{
					if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
					{
						$TmpName = $ReturnArr[$i]["OfficialFullName"];
						$TmpEmail = $ReturnArr[$i]["Email"];
						$TmpID = $ReturnArr[$i]["UserID"];
						$TmpSchoolCode = $_REQUEST["SchoolCode"];
				
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
							if($isiCal != 1 && $srcForm == 'NewUserForm') { 
								$TmpStr .= " \"".$TmpID."\"";
							}
							$TmpStr .= " \"".$TmpSchoolCode."\"";
						}
						else
						{
							$TmpStr = "<".$TmpEmail.">";
							$TmpStr .= " \"".$TmpSchoolCode."\"";
						}
						$TmpStr = str_replace("'","\'",$TmpStr);
						$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
					}
				}
			}	// end for
			if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "step3_4")))
			{
				$Step5Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$Step5Select .= '<br>';
				$Step5Select .= $Lang['email']['MultiSelectMsg'];
			}
			$IsEnding5 = true;

			$ActionStep5 = "onclick=\"this.form.action='select_recipient_foundation.php#step4';".$UserSubAction."jAdd_ACTION('add','step3_4')\"";

			if (($specialAction == "add") && ($actionLevel == "step3_4"))
			{
				if (count($Emails) > 0)
				{
					if($isiCal != 1 && $srcForm == 'NewUserForm') {
						$LoadJavascript .= "SubmitRecords(); \n";
					}
					$LoadJavascript .= "</script> ";
				}
			}
		}
	}
	
	else if ($TeachType2 == "3"){
	// Sections
	}
	
	else if ($TeachType2 == "4"){
	// House
		$Sql =  "exec Get_HouseList";
		$ReturnArr = $LibDB->returnArray($Sql);

		$Step3Select = $lui->Get_Input_Select("House","House",$ReturnArr,$House,"size=\"10\" style=\"width: 250px\" ");
		$ActionStep3 = "onclick=\"this.form.action='select_recipient_foundation.php#step4';jAdd_ACTION('add','House')\"";

		////////////////////////////////////////////////////
		//	Step4: From DB
		///////////////////////////////////////////////////
		if ($House != "")
		{
			////////////////////////////////////////////////////
			//	Step5: From DB
			///////////////////////////////////////////////////
			if (($specialAction == "add") && ($actionLevel == "step4_4"))
			{
				if (count($Emails) > 0)
				{
					$LoadJavascript .= "<script language='javascript' > ";
				}
			}		
			
			$Sql =  	"
							exec Search_StaffEmail
							@Staff_Type=',',
							@Roll_Group=',',
							@School_Year =',',
							@House='".$House.",' ,
							@SubjectName=',' ,
							@TTPeriod= '".$CurrentSchoolYear.",'
						";

			$ReturnArr = $LibDB->returnArray($Sql);
			$OutputArr = array();
			for ($i=0;$i<count($ReturnArr);$i++)
			{
				if($isiCal == 1)
				{	# iCalendar use
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
				} else
				{	# iMail use
					if (!empty($startFrom)){
						if (empty($OutputArr) || $ReturnArr[$i]["UserID"] != $OutputArr[$i-1][1])
							$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
					}else
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
				}

				if (($specialAction == "add") && ($actionLevel == "step4_4"))
				{
					if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
					{
						$TmpName = $ReturnArr[$i]["OfficialFullName"];
						$TmpEmail = $ReturnArr[$i]["Email"];
						$TmpSchoolCode = $_REQUEST["SchoolCode"];
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
							$TmpStr .= " \"".$TmpSchoolCode."\"";
						}
						else
						{
							$TmpStr = "<".$TmpEmail.">";
							$TmpStr .= " \"".$TmpSchoolCode."\"";
						}
						
						$TmpStr = str_replace("'","\'",$TmpStr);
						$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
					}
				}
			}

			$Step4Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
			$Step4Select .= '<br>';
			$Step4Select .= $Lang['email']['MultiSelectMsg'];
			$IsEnding4 = true;

			$ActionStep4 = "onclick=\"this.form.action='select_recipient_foundation.php#step5';".$UserSubAction."jAdd_ACTION('add','step4_4')\"";

			if (($specialAction == "add") && ($actionLevel == "step4_4"))
			{
				if (count($Emails) > 0)
				{
					if($isiCal != 1 && $srcForm == 'NewUserForm') {
						$LoadJavascript .= "SubmitRecords(); \n";
					}
					$LoadJavascript .= "</script> ";
				}
			}
			///////////////////////////////////////////////////
		}
		///////////////////////////////////////////////////
	}
	
	// Roll Groups
	else if ($TeachType2 == "5"){
		$Sql =  "exec Get_RollGroupList ";
		$ReturnArr = $LibDB->returnArray($Sql);

		/*echo '<pre>';
		var_dump($ReturnArr);
		echo '</pre>';*/
		$OutputArr = array();
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$OutputArr[] = array($ReturnArr[$i]["Roll_Group"], $ReturnArr[$i]["Roll_Group"]);
		}
		if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "RollGroup")))
		{
			$Step3Select = $lui->Get_Input_Select("RollGroup","RollGroup",$OutputArr,$SchoolYear,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:ChangeHiddenElement('RollGroup', this.value);\" ");
		}
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['RollGroup'], 1);" : "";
		$ActionStep3 = "onclick=\"this.form.action='select_recipient_foundation.php#step4';".$GroupSubAction."jAdd_ACTION('add','RollGroup')\"";

		////////////////////////////////////////////////////
		//	Step4: From DB
		///////////////////////////////////////////////////

		if($RollGroup != "")
		{
			//echo $RollGroup;			
			$Sql = "
				exec Search_StaffEmail
				@Staff_Type=',',
				@Roll_Group='".$RollGroup.",',
				@Teach_Roll_Group=',',
				@School_Year =',',
				@House=',' ,
				@SubjectName=',' ,
				@TTPeriod= '".$CurrentSchoolYear.",'
			";
			$ReturnArr = $LibDB->returnArray($Sql);
			// echo $Sql;
			/*echo '<pre>';
			var_dump($ReturnArr);
			echo '</pre>';					*/
			if (($specialAction == "add") && ($actionLevel == "step5_5"))
			{
				if (count($Emails) > 0)
				{
					$LoadJavascript .= "<script language='javascript' > ";
				}
			}

			$OutputArr = array();
			for ($i=0;$i<count($ReturnArr);$i++)
			{
				if($isiCal == 1)
				{	# iCalendar use
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
				} else
				{	# iMail use
					if (!empty($startFrom)){
						if (empty($OutputArr) || $ReturnArr[$i]["UserID"] != $OutputArr[$i-1][1])
							$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
					}else
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
				}

				if (($specialAction == "add") && ($actionLevel == "step5_5"))
				{
					//echo '123<br>';
					if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
					{
						$TmpName = $ReturnArr[$i]["OfficialFullName"];
						$TmpEmail = $ReturnArr[$i]["Email"];
						$TmpID = $ReturnArr[$i]["UserID"];
						$TmpSchoolCode = $_REQUEST["SchoolCode"];
				
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
							if($isiCal != 1 && $srcForm == 'NewUserForm') { 
								$TmpStr .= " \"".$TmpID."\"";
							}
							$TmpStr .= " \"".$TmpSchoolCode."\"";
						}
						else
						{
							$TmpStr = "<".$TmpEmail.">";
							$TmpStr .= " \"".$TmpSchoolCode."\"";
						}
						$TmpStr = str_replace("'","\'",$TmpStr);
						$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
					}
				}
			}	// end for
			if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "step5_5")))
			{
				$Step5Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$Step5Select .= '<br>';
				$Step5Select .= $Lang['email']['MultiSelectMsg'];
			}
			$IsEnding5 = true;

			$ActionStep5 = "onclick=\"this.form.action='select_recipient_foundation.php#step5';".$UserSubAction."jAdd_ACTION('add','step5_5')\"";

			if (($specialAction == "add") && ($actionLevel == "step5_5"))
			{
				if (count($Emails) > 0)
				{
					if($isiCal != 1 && $srcForm == 'NewUserForm') {
						$LoadJavascript .= "SubmitRecords(); \n";
					}
					$LoadJavascript .= "</script> ";
				}
			}
		}
	}
	
	else if ($TeachType2 == "6"){
		/*
		echo "TeachType: $TeachType ; TeachType2: $TeachType2 ; ByStudentType: $ByStudentType ;<br> ";
		echo "ByStudentRollGroup: $ByStudentRollGroup ; StudentList: $StudentList ; <br>";
		echo "SchoolYear: ".$_SESSION['SSV_SCHOOLYEAR'];
		*/
		///////////////////////////////////////////////////
		//	Step3: Fixed
		///////////////////////////////////////////////////
		$Step3Arr = array();
		//$Step2Arr[] = array($Lang['email']['SubjectClass'],0);
		//$Step2Arr[] = array($Lang['email']['Activity'],1);
		$Step3Arr[] = array($Lang['email']['StudentRollGroup'],2);
		if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "ByStudentType")))
		{
			$Step3Select = $lui->Get_Input_Select("ByStudentType","ByStudentType",$Step3Arr,$ByStudentType,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:ChangeHiddenElement('ByStudentType', this.value);\" ");
		}
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['ByStudentType'], 1);" : "";
		$ActionStep3 = "onclick=\"this.form.action='select_recipient_foundation.php#step4';".$GroupSubAction."jAdd_ACTION('add','ByStudentType')\"";
		///////////////////////////////////////////////////
		
		if ($ByStudentType == "0")		// subject class
			// nth
		{
		} 
		else if ($ByStudentType == "1")	// activity
		{
			// nth
		} 
		else if ($ByStudentType == "2")	// student roll group
		{
			///////////////////////////////////////////////////
			//	Step4: Fixed by sql
			///////////////////////////////////////////////////
			$Sql =  "exec Get_RollGroupList ";
			$ReturnArr = $LibDB->returnArray($Sql);

			$OutputArr = array();
			for ($i=0;$i<count($ReturnArr);$i++)
			{
				$OutputArr[] = array($ReturnArr[$i]["Roll_Group"], $ReturnArr[$i]["Roll_Group"]);
			}
			if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "ByStudentRollGroup")))
			{
				$Step4Select = $lui->Get_Input_Select("ByStudentRollGroup","ByStudentRollGroup",$OutputArr,$ByStudentRollGroup,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:ChangeHiddenElement('ByStudentRollGroup', this.value);\" ");
			}
			$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['ByStudentRollGroup'], 1);" : "";
			$ActionStep4 = "onclick=\"this.form.action='select_recipient_foundation.php#step5';".$GroupSubAction."jAdd_ACTION('add','ByStudentRollGroup')\"";
			///////////////////////////////////////////////////
			
			if ($ByStudentRollGroup != "")
			{
				///////////////////////////////////////////////////
				//	Step5: Fixed by sql
				///////////////////////////////////////////////////
				$ReturnArr = array();
				$Sql =  "exec Get_Student_ByRollGroup @RollGroup='".$ByStudentRollGroup."', @PageNo=1, @PageSize=999 ";
				$ReturnArr = $LibDB->returnArray($Sql);

				$OutputArr = array();
				for ($i=0;$i<count($ReturnArr);$i++)
				{
					$OutputArr[] = array($ReturnArr[$i]["Name"], $ReturnArr[$i]["UserID"]);
					//$OutputArr[] = array($ReturnArr[$i]["Name"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
				}
				
				if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "StudentList")))
				{
					$Step5Select = $lui->Get_Input_Select("StudentList","StudentList",$OutputArr,$StudentList,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:ChangeHiddenElement('StudentList', this.value);\" ");
				}
				$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['StudentList'], 1);" : "";
				$ActionStep5 = "onclick=\"this.form.action='select_recipient_foundation.php#step6';".$GroupSubAction."jAdd_ACTION('add','StudentList')\"";
				///////////////////////////////////////////////////
				
				if($StudentList != "")
				{
					///////////////////////////////////////////////////
					//	Step6: Fixed by sql
					///////////////////////////////////////////////////					
					$ReturnArr = array();
					$Sql = "
						exec Search_StaffEmail
						@Roll_Group='".$ByStudentRollGroup.",',
						@StudentID=".$StudentList." ,
						@TTPeriod= '".$CurrentSchoolYear.",'
					";
					
					// echo $Sql;
					
					$ReturnArr = $LibDB->returnArray($Sql);

					if (($specialAction == "add") && ($actionLevel == "StudentListId"))
					{
						if (count($Emails) > 0)
						{
							$LoadJavascript .= "<script language='javascript' > ";
						}
					}

					$OutputArr = array();
					for ($i=0;$i<count($ReturnArr);$i++)
					{
						if (!empty($starFrom))
							$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
						else
						$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);

						if (($specialAction == "add") && ($actionLevel == "StudentListId"))
						{
							if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
							{
								$TmpName = $ReturnArr[$i]["OfficialFullName"];
								$TmpEmail = $ReturnArr[$i]["Email"];
								$TmpID = $ReturnArr[$i]["UserID"];
								$TmpSchoolCode = $_REQUEST["SchoolCode"];
				
								if ($TmpName != "")
								{
									$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
									if($isiCal != 1 && $srcForm == 'NewUserForm') { 
										$TmpStr .= " \"".$TmpID."\"";
									}
									$TmpStr .= " \"".$TmpSchoolCode."\"";
								}
								else
								{
									$TmpStr = "<".$TmpEmail.">";
									$TmpStr .= " \"".$TmpSchoolCode."\"";
								}
								$TmpStr = str_replace("'","\'",$TmpStr);
								$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
							}
						}
					}
					$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['StudentListId'], 1);" : "";
					$ActionStep6 = "onclick=\"this.form.action='select_recipient_foundation.php#step7';".$GroupSubAction."jAdd_ACTION('add','StudentListId')\"";
					if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "StudentListId")))
					{
						$Step6Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
						$Step6Select .= '<br>';
						$Step6Select .= $Lang['email']['MultiSelectMsg'];
					}
					$IsEnding6 = true;
					if (($specialAction == "add") && ($actionLevel == "StudentListId"))
					{
						if (count($Emails) > 0)
						{
							if($isiCal != 1 && $srcForm == 'NewUserForm') {
								$LoadJavascript .= "SubmitRecords(); \n";
							}
							$LoadJavascript .= "</script> ";
						}
					}
					///////////////////////////////////////////////////
				}
			}
			
		}
		
	}
	///////////////////////////////////////////////////
}


else if ($TeachType == "2"){
// Student

	///////////////////////////////////////////////////
	//	Step2: Fixed
	///////////////////////////////////////////////////
	$Step2Arr = array();
	$Step2Arr[] = array($Lang['email']['SubjectClass'],0);
	$Step2Arr[] = array($Lang['email']['Activity'],1);
	$Step2Arr[] = array($Lang['email']['StudentRollGroup'],2);
	if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "StudentType")))
	{
		$Step2Select = $lui->Get_Input_Select("StudentType","StudentType",$Step2Arr,$StudentType,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:ChangeHiddenElement('StudentType', this.value);\" ");
	}
	$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['StudentType'], 1);" : "";
	$ActionStep2 = "onclick=\"this.form.action='select_recipient_foundation.php#step3';".$GroupSubAction."jAdd_ACTION('add','StudentType')\"";
	///////////////////////////////////////////////////

	////////////////////////////////////////////////////
	//	Step3: From DB
	///////////////////////////////////////////////////
	if ($StudentType == "0"){
		// subject class
		$Sql =  "exec Get_SchoolYearList ";
		$ReturnArr = $LibDB->returnArray($Sql);

		$OutputArr = array();
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$OutputArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);
		}
		if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "StudentSchoolYear")))
		{
			$Step3Select = $lui->Get_Input_Select("StudentSchoolYear","StudentSchoolYear",$OutputArr,$StudentSchoolYear,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:ChangeHiddenElement('StudentSchoolYear', this.value);\" ");
		}
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['StudentSchoolYear'], 1);" : "";
		$ActionStep3 = "onclick=\"this.form.action='select_recipient_foundation.php#step4';".$GroupSubAction."jAdd_ACTION('add','StudentSchoolYear')\"";



		if ($StudentSchoolYear != "")
		{		
			$Sql =  "exec Get_SubjectClassList_BySchoolYear @TTPeriod='".$CurrentSchoolYear."', @SchoolYear='".$StudentSchoolYear."' ";
			
			// echo $Sql;
			$ReturnArr = $LibDB->returnArray($Sql);

			$OutputArr = array();
			for ($i=0;$i<count($ReturnArr);$i++)
			{
				$ClassTitleSql = "Select Cast(ClassTitle as varchar(Max)) as ClassTitle from subject_class_class_detail where ClassCode = '".str_pad($ReturnArr[$i]["sukey"], 5, "0", STR_PAD_RIGHT).str_pad($ReturnArr[$i]["class"], 2, "0", STR_PAD_LEFT)."'";
				
				// echo $ClassTitleSql.'<br>';
				
				$TmpReturn = $LibDB->returnArray($ClassTitleSql);
				
				if (trim($TmpReturn[0]['ClassTitle']) != '') {
					$OutputArr[] = array($TmpReturn[0]['ClassTitle'], str_pad($ReturnArr[$i]["sukey"], 5, "0", STR_PAD_RIGHT).str_pad($ReturnArr[$i]["class"], 2, "0", STR_PAD_LEFT));
				} else {
					$OutputArr[] = array("(".str_pad($ReturnArr[$i]["sukey"], 5, "0", STR_PAD_RIGHT).str_pad($ReturnArr[$i]["class"], 2, "0", STR_PAD_LEFT).") ".$ReturnArr[$i]["fullname"]." ".$ReturnArr[$i]["class"], str_pad($ReturnArr[$i]["sukey"], 5, "0", STR_PAD_RIGHT).str_pad($ReturnArr[$i]["class"], 2, "0", STR_PAD_LEFT));
				}
			}
			if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "StudentSchoolYear1")))
			{
				$Step4Select = $lui->Get_Input_Select("StudentSchoolYear1","StudentSchoolYear1",$OutputArr,$StudentSchoolYear1,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:ChangeHiddenElement('StudentSchoolYear1', this.value);\" ");
			}
			$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['StudentSchoolYear1'], 1);" : "";
			$ActionStep4 = "onclick=\"this.form.action='select_recipient_foundation.php#step5';".$GroupSubAction."jAdd_ACTION('add','StudentSchoolYear1')\"";



			if ($StudentSchoolYear1 != "")
			{
				$thisSubjectCode = substr($StudentSchoolYear1, 0, 5);
				if (substr($thisSubjectCode, -1) == 0 ) $thisSubjectCode = substr($StudentSchoolYear1, 0, 5);
				
				$Sql =  "exec Get_Student_BySubject @SubjectCode='".$thisSubjectCode."', @SubjectClass='".substr($StudentSchoolYear1, 5, 2)."', @TTPeriod='".$CurrentSchoolYear."', @PageNo=1, @PageSize=999 ";
				// echo $Sql;
				
				$ReturnArr = $LibDB->returnArray($Sql);

				if (($specialAction == "add") && ($actionLevel == "SubjectCode"))
				{
					if (count($Emails) > 0)
					{
						$LoadJavascript .= "<script language='javascript' > ";
					}
				}

				$OutputArr = array();
				for ($i=0;$i<count($ReturnArr);$i++)
				{
					if (!empty($startFrom)){
						if (empty($OutputArr) || $ReturnArr[$i]["UserID"] != $OutputArr[$i-1][1])
							$OutputArr[] = array($ReturnArr[$i]["Name"], $ReturnArr[$i]["UserID"]);
					}else
					$OutputArr[] = array($ReturnArr[$i]["Name"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);

					if (($specialAction == "add") && ($actionLevel == "SubjectCode"))
					{
						if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
						{
							$TmpName = $ReturnArr[$i]["Name"];
							$TmpEmail = $ReturnArr[$i]["Email"];
							$TmpID = $ReturnArr[$i]["UserID"];
							$TmpSchoolCode = $_REQUEST["SchoolCode"];
							
							if ($TmpName != "")
							{
								$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
								if($isiCal != 1 && $srcForm == 'NewUserForm') { 
									$TmpStr .= " \"".$TmpID."\"";
								}
								$TmpStr .= " \"".$TmpSchoolCode."\"";
							}
							else
							{
								$TmpStr = "<".$TmpEmail.">";
								$TmpStr .= " \"".$TmpSchoolCode."\"";
							}
							$TmpStr = str_replace("'","\'",$TmpStr);
							$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
						}
					}
				}
				$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['SubjectCode'], 1);" : "";
				$ActionStep5 = "onclick=\"this.form.action='select_recipient_foundation.php#step6';".$GroupSubAction."jAdd_ACTION('add','SubjectCode')\"";
				if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "SubjectCode")))
				{
					$Step5Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
					$Step5Select .= '<br>';
					$Step5Select .= $Lang['email']['MultiSelectMsg'];
				}
				$IsEnding5 = true;
				if (($specialAction == "add") && ($actionLevel == "SubjectCode"))
				{
					if (count($Emails) > 0)
					{	
						if($isiCal != 1 && $srcForm == 'NewUserForm') {
							$LoadJavascript .= "SubmitRecords(); \n";
						}
						$LoadJavascript .= "</script> ";
					}
				}
			}
		}
	} // end if subject class
	
	else if ($StudentType == "1"){
		// activity
		$Sql =  "exec Get_ActivityList_BySchoolYear @SchoolYear='".$CurrentSchoolYear."' ";
		$ReturnArr = $LibDB->returnArray($Sql);

		$OutputArr = array();
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$OutputArr[] = array($ReturnArr[$i]["Name"], $ReturnArr[$i]["ActivityId"]);
		}
		if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "ActivityId")))
		{
			$Step3Select = $lui->Get_Input_Select("ActivityId","ActivityId",$OutputArr,$ActivityId,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:ChangeHiddenElement('ActivityId', this.value);\" ");
		}
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['ActivityId'], 1);" : "";
		$ActionStep3 = "onclick=\"this.form.action='select_recipient_foundation.php#step4';".$GroupSubAction."jAdd_ACTION('add','ActivityId')\"";



		if ($ActivityId != "")
		{
			$Sql =  "exec Get_Student_ByActivity @ActivityId='".$ActivityId."', @PageNo=1, @PageSize=999 ";
			$ReturnArr = $LibDB->returnArray($Sql);

			if (($specialAction == "add") && ($actionLevel == "ActivityList"))
			{
				if (count($Emails) > 0)
				{
					$LoadJavascript .= "<script language='javascript' > ";
				}
			}

			$OutputArr = array();
			for ($i=0;$i<count($ReturnArr);$i++)
			{
				if(!empty($startFrom)){
					if (empty($OutputArr) || $ReturnArr[$i]["UserID"] != $OutputArr[$i-1][1])
						$OutputArr[] = array($ReturnArr[$i]["Name"], $ReturnArr[$i]["UserID"]);
				}else
				$OutputArr[] = array($ReturnArr[$i]["Name"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);

				if (($specialAction == "add") && ($actionLevel == "ActivityList"))
				{
					if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
					{
						$TmpName = $ReturnArr[$i]["Name"];
						$TmpEmail = $ReturnArr[$i]["Email"];
						$TmpID = $ReturnArr[$i]["UserID"];
						$TmpSchoolCode = $_REQUEST["SchoolCode"];
						
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
							if($isiCal != 1 && $srcForm == 'NewUserForm') { 
								$TmpStr .= " \"".$TmpID."\"";
							}
							$TmpStr .= " \"".$TmpSchoolCode."\"";
						}
						else
						{
							$TmpStr = "<".$TmpEmail.">";
							$TmpStr .= " \"".$TmpSchoolCode."\"";
						}
						$TmpStr = str_replace("'","\'",$TmpStr);
						$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
					}
				}
			}
			$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['ActivityList'], 1);" : "";
			$ActionStep4 = "onclick=\"this.form.action='select_recipient_foundation.php#step5';".$GroupSubAction."jAdd_ACTION('add','ActivityList')\"";
			if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "ActivityList")))
			{
				$Step4Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$Step4Select .= '<br>';
				$Step4Select .= $Lang['email']['MultiSelectMsg'];
			}
			$IsEnding4 = true;
			if (($specialAction == "add") && ($actionLevel == "ActivityList"))
			{
				if (count($Emails) > 0)
				{
					if($isiCal != 1 && $srcForm == 'NewUserForm') {
						$LoadJavascript .= "SubmitRecords(); \n";
					}
					$LoadJavascript .= "</script> ";
				}
			}
		}
	} // end if activity
	
	else if ($StudentType == "2"){
		// student roll group
		$Sql =  "exec Get_RollGroupList ";
		$ReturnArr = $LibDB->returnArray($Sql);

		$OutputArr = array();
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$OutputArr[] = array($ReturnArr[$i]["Roll_Group"], $ReturnArr[$i]["Roll_Group"]);
		}
		if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "StudentRollGroup")))
		{
			$Step3Select = $lui->Get_Input_Select("StudentRollGroup","StudentRollGroup",$OutputArr,$StudentRollGroup,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:ChangeHiddenElement('StudentRollGroup', this.value);\" ");
		}
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['StudentRollGroup'], 1);" : "";
		$ActionStep3 = "onclick=\"this.form.action='select_recipient_foundation.php#step4';".$GroupSubAction."jAdd_ACTION('add','StudentRollGroup')\"";



		if ($StudentRollGroup != "")
		{
			$Sql =  "exec Get_Student_ByRollGroup @RollGroup='".$StudentRollGroup."', @PageNo=1, @PageSize=999 ";
			$ReturnArr = $LibDB->returnArray($Sql);

			if (($specialAction == "add") && ($actionLevel == "StudentRollGroupId"))
			{
				if (count($Emails) > 0)
				{
					$LoadJavascript .= "<script language='javascript' > ";
				}
			}

			$OutputArr = array();
			for ($i=0;$i<count($ReturnArr);$i++)
			{
				if (!empty($startFrom)){
					if (empty($OutputArr) || $ReturnArr[$i]["UserID"] != $OutputArr[$i-1][1])
						$OutputArr[] = array($ReturnArr[$i]["Name"], $ReturnArr[$i]["UserID"]);
				}else
				$OutputArr[] = array($ReturnArr[$i]["Name"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);

				if (($specialAction == "add") && ($actionLevel == "StudentRollGroupId"))
				{
					if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
					{
						$TmpName = $ReturnArr[$i]["Name"];
						$TmpEmail = $ReturnArr[$i]["Email"];
						$TmpID = $ReturnArr[$i]["UserID"];
						$TmpSchoolCode = $_REQUEST["SchoolCode"];
						
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
							if($isiCal != 1 && $srcForm == 'NewUserForm') { 
								$TmpStr .= " \"".$TmpID."\"";
							}
							$TmpStr .= " \"".$TmpSchoolCode."\"";
						}
						else
						{
							$TmpStr = "<".$TmpEmail.">";
							$TmpStr .= " \"".$TmpSchoolCode."\"";
						}
						$TmpStr = str_replace("'","\'",$TmpStr);
						$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
					}
				}
			}
			$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['StudentRollGroupId'], 1);" : "";
			$ActionStep4 = "onclick=\"this.form.action='select_recipient_foundation.php#step5';".$GroupSubAction."jAdd_ACTION('add','StudentRollGroupId')\"";
			if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "StudentRollGroupId")))
			{
				$Step4Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$Step4Select .= '<br>';
				$Step4Select .= $Lang['email']['MultiSelectMsg'];
			}
			$IsEnding4 = true;
			if (($specialAction == "add") && ($actionLevel == "StudentRollGroupId"))
			{
				if (count($Emails) > 0)
				{	
					if($isiCal != 1 && $srcForm == 'NewUserForm') {
						$LoadJavascript .= "SubmitRecords(); \n";
					}
					$LoadJavascript .= "</script> ";
				}
			}
		}
	} // end if student roll group

} // end if student


else if ($TeachType == "5"){
//School groups

	// Subjects

		//$Sql =  "exec Get_SubjectList_ByTTPeriod @TTPeriod='".$_SESSION['SSV_SCHOOLYEAR']."' ";
		$Sql =  " SELECT GroupName, MailGroupID FROM MAIL_ADDRESS_GROUP WHERE GroupType='S' ";
		$ReturnArr = $LibDB->returnArray($Sql);

		$OutputArr = array();
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$OutputArr[] = array($ReturnArr[$i]["GroupName"], $ReturnArr[$i]["MailGroupID"]);
		}
		if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "SchoolGroup")))
		{
			$Step2Select = $lui->Get_Input_Select("SchoolGroup","SchoolGroup",$OutputArr,$SchoolGroup,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:ChangeHiddenElement('SchoolGroup', this.value);\" ");
		}
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['SchoolGroup'], 1);" : "";
		$ActionStep2 = "onclick=\"this.form.action='select_recipient_foundation.php#step3';".$GroupSubAction."jAdd_ACTION('add','SchoolGroup')\"";

		////////////////////////////////////////////////////
		//	Step3: From DB
		///////////////////////////////////////////////////

		if (($specialAction == "add") && ($actionLevel == "step5_2_4"))
		{
		if (count($Emails) > 0)
		{
			$LoadJavascript .= "<script language='javascript' > ";
		}
		}

		if ($SchoolGroup != "")
		{
			$Sql =  	"
							SELECT
								b.UserName, 
								CASE 
									WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') = '' THEN RTRIM(LTRIM(b.UserEmail)) collate Latin1_General_CI_AS
									WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') != '' THEN RTRIM(LTRIM(i.Email)) collate Latin1_General_CI_AS
									WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') != '' THEN RTRIM(LTRIM(i.EmailAlias1)) collate Latin1_General_CI_AS
								END AS 'Email', 
								b.MailUserID AS UserID
							FROM
								MAIL_ADDRESS_MAPPING a
							INNER JOIN
								MAIL_ADDRESS_USER b ON a.MailUserID = b.MailUserID
							LEFT OUTER JOIN intranet_user i ON b.IntranetUserID = i.UserID							
							WHERE							
								a.MailGroupID = '".$SchoolGroup."'
						";
			$ReturnArr = $LibDB->returnArray($Sql);
			$OutputArr = array();
			for ($i=0;$i<count($ReturnArr);$i++)
			{
				if($isiCal == 1)
				{	# iCalendar use
					$OutputArr[] = array($ReturnArr[$i]["UserName"], $ReturnArr[$i]["UserID"]);
				} else
				{	# iMail use
					if (!empty($startFrom)){
						if (empty($OutputArr) || $ReturnArr[$i]["UserID"] != $OutputArr[$i-1][1])
							$OutputArr[] = array($ReturnArr[$i]["UserName"], $ReturnArr[$i]["UserID"]);
					}else
					$OutputArr[] = array($ReturnArr[$i]["UserName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
				}

				if (($specialAction == "add") && ($actionLevel == "step5_2_4"))
				{
				if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
				{
					$TmpName = $ReturnArr[$i]["UserName"];
					$TmpEmail = $ReturnArr[$i]["Email"];
					$TmpID = $ReturnArr[$i]["UserID"];
					$TmpSchoolCode = $_REQUEST["SchoolCode"];
					
					if ($TmpName != "")
					{
						$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
						if($isiCal != 1 && $srcForm == 'NewUserForm') { 
							$TmpStr .= " \"".$TmpID."\"";
						}
						$TmpStr .= " \"".$TmpSchoolCode."\"";
					}
					else
					{
						$TmpStr = "<".$TmpEmail.">";
						$TmpStr .= " \"".$TmpSchoolCode."\"";
					}
					$TmpStr = str_replace("'","\'",$TmpStr);
					$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
				}
				}

			}
			if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "step5_2_4")))
			{
				$Step3Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$Step3Select .= '<br>';
				$Step3Select .= $Lang['email']['MultiSelectMsg'];
			}
			$IsEnding3 = true;
		}
		$ActionStep3 = "onclick=\"this.form.action='select_recipient_foundation.php#step5';".$UserSubAction."jAdd_ACTION('add','step5_2_4')\"";

		if (($specialAction == "add") && ($actionLevel == "step5_2_4"))
		{
		if (count($Emails) > 0)
		{
			if($isiCal != 1 && $srcForm == 'NewUserForm') {
				$LoadJavascript .= "SubmitRecords(); \n";
			}
			$LoadJavascript .= "</script> ";
		}
		}

		///////////////////////////////////////////////////
}

else if ($TeachType == "6"){
// Parent
	$Step2Arr = array();
	$Step2Arr[] = array($Lang['email']['StudentRollGroup'],2);
	if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "ParentType")))
	{
		$Step2Select = $lui->Get_Input_Select("ParentType","ParentType",$Step2Arr,$ParentType,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:ChangeHiddenElement('ParentType', this.value);\" ");
	}
	$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['ParentType'], 1);" : "";
	$ActionStep2 = "onclick=\"this.form.action='select_recipient_foundation.php#step3';".$GroupSubAction."jAdd_ACTION('add','ParentType')\"";
	
	if ($ParentType == "2")
	{
		$Sql =  "exec Get_RollGroupList ";
		$ReturnArr = $LibDB->returnArray($Sql);

		// echo '<pre>';
		// var_dump($ReturnArr);
		// echo '</pre>';
		$OutputArr = array();
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$OutputArr[] = array($ReturnArr[$i]["Roll_Group"], $ReturnArr[$i]["Roll_Group"]);
		}
		if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "RollGroup")))
		{
			$Step3Select = $lui->Get_Input_Select("RollGroup","RollGroup",$OutputArr,$SchoolYear,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:ChangeHiddenElement('RollGroup', this.value);\" ");
		}
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['RollGroup'], 1);" : "";
		$ActionStep3 = "onclick=\"this.form.action='select_recipient_foundation.php#step4';".$GroupSubAction."jAdd_ACTION('add','RollGroup')\"";
		
		if ($RollGroup != "") {
			$Sql =  "exec Get_Student_ByRollGroup_CheckContacts @RollGroup='".$RollGroup."', @PageNo=1, @PageSize=999 ";
			$ReturnArr = $LibDB->returnArray($Sql);
			
			 // echo '<pre>';
			 // var_dump($ReturnArr);
			 // echo '</pre>';
			$OutputArr = array();
			
			
			for ($i=0;$i<count($ReturnArr);$i++)
			{
				$OutputArr[] = array($ReturnArr[$i]["StudentName"], $ReturnArr[$i]["UserID"]);
			}
			if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "StudentName")))
			{
				$Step4Select = $lui->Get_Input_Select_multi("StudentName[]","StudentName",$OutputArr,$SchoolYear,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:Collect_Student_List('StudentNameList', this);\" ");
				$Step4Select .= '<br>';
				$Step4Select .= $Lang['email']['MultiSelectMsg'];
			}
			// $GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['StudentName'], 1);" : "";
			$ActionStep4 = "onclick=\"this.form.action='select_recipient_foundation.php#step5';".$GroupSubAction."jAdd_ACTION('add','StudentName')\"";
			
			if ($StudentName != '' || $actionLevel == "step6_2") {
				
				$OutputArr = array();
				
				
				
				if (!is_array($StudentName)) {
					
					// $StudentNameList = substr($StudentNameList,0,-1);
					// echo 'StudentList : ' .$StudentNameList;
					$StudentName = explode("|",$StudentNameList);
					//debug_r($StudentName);
				}
				
				
				if (($specialAction == "add") && ($actionLevel == "step6_2"))
				{
					if (count($Emails) > 0)
					{
						$LoadJavascript .= "<script language='javascript' > ";
					}
				}
				
				for ($StudIdx = 0; $StudIdx < count($StudentName); $StudIdx++) {
					$sql = "exec Get_Parent_ByStudent @UserID = ".$StudentName[$StudIdx];
					$ReturnArr = $LibDB->returnArray($sql);	
					// debug_r($ReturnArr);
					for ($i=0; $i<count($ReturnArr); $i++)
					{
						if($isiCal == 1)
						{	# iCalendar use
							$OutputArr[] = array($ReturnArr[$i]["ParentName"], $ReturnArr[$i]["Email"]);
						} else
						{	# iMail use
							if (!empty($startFrom)){
								if (empty($OutputArr) || $ReturnArr[$i]["UserID"] != $OutputArr[$i-1][1])
									$OutputArr[] = array($ReturnArr[$i]["ParentName"], $ReturnArr[$i]["Email"]);
							}else
							$OutputArr[] = array($ReturnArr[$i]["ParentName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["Email"]);
						}
			
						if (($specialAction == "add") && ($actionLevel == "step6_2"))
						{
							if (is_array($Emails) && in_array($ReturnArr[$i]["Email"], $Emails))
							{
								$TmpName = $ReturnArr[$i]["ParentName"];
								$TmpEmail = $ReturnArr[$i]["Email"];
								$TmpID = $ReturnArr[$i]["UserID"];
								$TmpSchoolCode = $_REQUEST["SchoolCode"];
								
								if ($TmpName != "")
								{
									$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
									if($isiCal != 1 && $srcForm == 'NewUserForm') { 
										$TmpStr .= " \"".$TmpID."\"";
									}
									$TmpStr .= " \"".$TmpSchoolCode."\"";
								}
								else
								{
									$TmpStr = "<".$TmpEmail.">";
									$TmpStr .= " \"".$TmpSchoolCode."\"";
								}
								$TmpStr = str_replace("'","\'",$TmpStr);
								
								$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
							}
						}
						
					}
					
				}
				$UserSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['step6_2'], 1);" : "";
				$ActionStep5 = "onclick=\"this.form.action='select_recipient_foundation.php#step5';".$UserSubAction."jAdd_ACTION('add','step6_2')\"";
				
				if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "step6_2")))
				{
					$Step5Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" onClick=\"javascript:Collect_Student_List('ParentEmailList', this);\" ");
					$Step5Select .= '<br>';
					$Step5Select .= $Lang['email']['MultiSelectMsg'];
				}
				
				$IsEnding5 = true;
				if (($specialAction == "add") && ($actionLevel == "step6_2")) 
				{
					if (count($Emails) > 0) {
						if($isiCal != 1 && $srcForm == 'NewUserForm') {
							$LoadJavascript .= "SubmitRecords(); \n";
						}
						$LoadJavascript .= "</script> ";
						
					}
				}
				
			}
		}
	}
}

// 2009-08-04
else if ($TeachType == "7"){
//Foundation-Based groups -- No. 7
		$LibESF = new database(false,false,true);
		$Sql =  " SELECT GroupName, MailGroupID FROM $ESFDBName.dbo.MAIL_ADDRESS_GROUP WHERE GroupType='F' ";
		$ReturnArr = $LibESF->returnArray($Sql);

		$OutputArr = array();
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$OutputArr[] = array($ReturnArr[$i]["GroupName"], $ReturnArr[$i]["MailGroupID"]);
		}
		if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "FoundationGroup")))
		{
			$Step2Select = $lui->Get_Input_Select("FoundationGroup","FoundationGroup",$OutputArr,$FoundationGroup,"size=\"10\" style=\"width: 250px\" onClick=\"javascript:ChangeHiddenElement('FoundationGroup', this.value);\" ");
		}
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['FoundationGroup'], 1);" : "";
		$ActionStep2 = "onclick=\"this.form.action='select_recipient_foundation.php#step3';".$GroupSubAction."jAdd_ACTION('add','FoundationGroup')\"";

		////////////////////////////////////////////////////
		//	Step3: From DB
		///////////////////////////////////////////////////

		if (($specialAction == "add") && ($actionLevel == "step7_2_4"))
		{
		if (count($Emails) > 0)
		{
			$LoadJavascript .= "<script language='javascript' > ";
		}
		}
		
		if ($FoundationGroup != "")
		{
			$Sql =  	"
							SELECT
								b.UserName, 
								CASE 
									WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') = '' THEN RTRIM(LTRIM(b.UserEmail)) collate Latin1_General_CI_AS
									WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') != '' THEN RTRIM(LTRIM(i.Email)) collate Latin1_General_CI_AS
									WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') != '' THEN RTRIM(LTRIM(i.EmailAlias1)) collate Latin1_General_CI_AS
								END AS 'Email', 
								b.MailUserID AS UserID
							FROM
								$ESFDBName.dbo.MAIL_ADDRESS_MAPPING a
							INNER JOIN
								$ESFDBName.dbo.MAIL_ADDRESS_USER b ON a.MailUserID = b.MailUserID
							LEFT OUTER JOIN $ESFDBName.dbo.intranet_user i ON b.IntranetUserID = i.UserID							
							WHERE							
								a.MailGroupID = '".$FoundationGroup."'
						";
			$LibESF = new database(false,false,true);
			$ReturnArr = $LibESF->returnArray($Sql);
			$OutputArr = array();
			for ($i=0;$i<count($ReturnArr);$i++)
			{
				if($isiCal == 1)
				{	# iCalendar use
					$OutputArr[] = array($ReturnArr[$i]["UserName"], $ReturnArr[$i]["UserID"]);
				} else
				{	# iMail use
					if (!empty($startFrom)){
						if (empty($OutputArr) || $ReturnArr[$i]["UserID"] != $OutputArr[$i-1][1])
							$OutputArr[] = array($ReturnArr[$i]["UserName"], $ReturnArr[$i]["UserID"]);
					}else
						$OutputArr[] = array($ReturnArr[$i]["UserName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
				}
				
				if (($specialAction == "add") && ($actionLevel == "step7_2_4"))
				{
				if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
				{
					$TmpName = $ReturnArr[$i]["UserName"];
					$TmpEmail = $ReturnArr[$i]["Email"];
					$TmpID = $ReturnArr[$i]["UserID"];
					$TmpSchoolCode = $_REQUEST["SchoolCode"];
					
					if ($TmpName != "")
					{
						$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
						if($isiCal != 1 && $srcForm == 'NewUserForm') { 
							$TmpStr .= " \"".$TmpID."\"";
						}
						$TmpStr .= " \"".$TmpSchoolCode."\"";
					}
					else
					{
						$TmpStr = "<".$TmpEmail.">";
						$TmpStr .= " \"".$TmpSchoolCode."\"";
					}
					$TmpStr = str_replace("'","\'",$TmpStr);
					$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
				}
				}

			}
			if (($specialAction != "add") || (($specialAction == "add") && ($actionLevel == "step7_2_4")))
			{
				$Step3Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$Step3Select .= '<br>';
				$Step3Select .= $Lang['email']['MultiSelectMsg'];
			}
			$IsEnding3 = true;
		}
		$ActionStep3 = "onclick=\"this.form.action='select_recipient_foundation.php#step7';".$UserSubAction."jAdd_ACTION('add','step7_2_4')\"";

		if (($specialAction == "add") && ($actionLevel == "step7_2_4"))
		{
		if (count($Emails) > 0)
		{
			if($isiCal != 1 && $srcForm == 'NewUserForm') {
				$LoadJavascript .= "SubmitRecords(); \n";
			}
			$LoadJavascript .= "</script> ";
		}
		}

		///////////////////////////////////////////////////
}
// 2009-08-04

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Adding groups
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
if ($specialAction == "add")
{
	if ($actionLevel == "TeachType" && $TeachType != "6")
	{			
		if($TeachType == 5)
		{
			$Sql = "SELECT
						b.UserName AS OfficialFullName, 
						CASE 
							WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') = '' THEN RTRIM(LTRIM(b.UserEmail)) collate Latin1_General_CI_AS
							WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') != '' THEN RTRIM(LTRIM(i.Email)) collate Latin1_General_CI_AS
							WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') != '' THEN RTRIM(LTRIM(i.EmailAlias1)) collate Latin1_General_CI_AS
						END AS 'Email',						
						b.MailUserID AS UserID
					FROM
						MAIL_ADDRESS_USER b
					LEFT OUTER JOIN intranet_user i ON b.IntranetUserID = i.UserID
					WHERE
						b.UserType = 'S'";
		}
		else if($TeachType == 7)//Foundation-Based Groups 2009-08-04
		{
			$Sql = "SELECT 
						b.UserName AS OfficialFullName, 
						CASE 
							WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') = '' THEN RTRIM(LTRIM(b.UserEmail)) collate Latin1_General_CI_AS
							WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') != '' THEN RTRIM(LTRIM(i.Email)) collate Latin1_General_CI_AS
							WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') != '' THEN RTRIM(LTRIM(i.EmailAlias1)) collate Latin1_General_CI_AS
						END AS 'Email',						
						b.MailUserID AS UserID
					FROM
						$ESFDBName.dbo.MAIL_ADDRESS_USER b
					INNER JOIN $ESFDBName.dbo.intranet_user i ON b.IntranetUserID = i.UserID
					WHERE
						b.UserType = 'F'";
		}// 2009-08-04
		else if($TeachType == 2)
		{
			$Sql = "SELECT DISTINCT A.OfficialFullName, A.UserID, A.Email FROM (
								SELECT Row_Number() OVER (ORDER BY U.Surname, U.PrefName) as 'Row_Num', U.Surname COLLATE Latin1_General_CI_AS + ',' + ' ' + U.PrefName as 'OfficialFullName', U.UserID, IsNULL(U.EmailAlias1, U.Email) as 'Email'
								FROM INTRANET_USER U WITH (NOLOCK)
								INNER JOIN MAZE_STMA WITH (NOLOCK) ON MAZE_STMA.SKEY = U.MazeKey
								WHERE 
									MAZE_STMA.TTPeriod = '".$CurrentSchoolYear."' 
									AND U.Status = 'A' 
									AND RTRIM(LTRIM(IsNull(U.Email,0))) <> 'NA' 
									AND RTRIM(LTRIM(IsNull(U.EmailAlias1,0))) <> 'NA' 
							) as A
							UNION
							SELECT DISTINCT A.OfficialFullName, A.UserID, A.Email FROM (
								SELECT Row_Number() OVER (ORDER BY U.Surname, U.PrefName) as 'Row_Num', U.Surname COLLATE Latin1_General_CI_AS + ',' + ' ' + U.PrefName as 'OfficialFullName', U.UserID, IsNULL(U.EmailAlias1, U.Email) as 'Email'
								FROM INTRANET_USER U WITH (NOLOCK)
								INNER JOIN CAS_USER_ACTIVITY_MAPPING WITH (NOLOCK) ON CAS_USER_ACTIVITY_MAPPING.UserId = U.UserID
								WHERE 
									CAS_USER_ACTIVITY_MAPPING.Status = 1 
									AND RTRIM(LTRIM(IsNull(U.Email,0))) <> 'NA' 
									AND RTRIM(LTRIM(IsNull(U.EmailAlias1,0))) <> 'NA' 
							) as A
							ORDER BY A.OfficialFullName, A.Email, A.UserID ";
		}
		else 
		{	
			
			
			if ($TeachType == "1")
			{
				$Staff_Type = "N";
			}
			else
			{
				$Staff_Type = "T";
			}
			
			
			$Sql =  	"
							exec Search_StaffEmail
							@Staff_Type='".$Staff_Type.",',
							@Roll_Group=',',
							@School_Year =',',
							@House=',' ,
							@SubjectName=',' ,
							@TTPeriod= '".$CurrentSchoolYear.",'
						";
			
		}
		if($TeachType == 7)
		{
			$LibESF = new database(false,false,true);
			$ReturnArr = $LibESF->returnArray($Sql);
		}else
			$ReturnArr = $LibDB->returnArray($Sql);
		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			$TmpSchoolCode = $_REQUEST["SchoolCode"];
			
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
				if($isiCal != 1 && $srcForm == 'NewUserForm') { 
					$TmpStr .= " \"".$TmpID."\"";
				}
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			else
			{
				$TmpStr = "<".$TmpEmail.">";
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}

			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else
			{
				$TmpStr = str_replace("'","\'",$TmpStr);
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
			}
		}
		
		if($isiCal != 1 && $srcForm == 'NewUserForm') {
			$LoadJavascript .= "SubmitRecords(); \n";
		}
		
		$LoadJavascript .= "</script> ";
		
		
		
	}
	else if ($actionLevel == "TeachType2") 
	{
		//may add customized data collection for different options
		/*
		// individual
		if ($TeachType2 == 0) {}
		// subjects
		else if ($TeachType2 == 1) {}
		// schoo years
		else if ($TeachType2 == 2) {}
		// roll groups
		else if ($TeachType2 == 5) {}*/

		$Sql = "exec Search_StaffEmail
							@Staff_Type='T,',
							@Roll_Group=',',
							@School_Year =',',
							@House=',' ,
							@SubjectName=',' ,
							@TTPeriod= '".$CurrentSchoolYear.",'
						";
		$ReturnArr = $LibDB->returnArray($Sql);
		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			$TmpSchoolCode = $_REQUEST["SchoolCode"];
			
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
				if($isiCal != 1 && $srcForm == 'NewUserForm') { 
					$TmpStr .= " \"".$TmpID."\"";
				}
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			else
			{
				$TmpStr = "<".$TmpEmail.">";
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}

			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else
			{
				$TmpStr = str_replace("'","\'",$TmpStr);
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
			}
		}
		
		if($isiCal != 1 && $srcForm == 'NewUserForm') {
			$LoadJavascript .= "SubmitRecords(); \n";
		}
		
		$LoadJavascript .= "</script> ";
	}
	else if ($actionLevel == "TeachSubject")
	{
		$Sql =  	"
						exec Search_StaffEmail
						@Staff_Type=',',
						@Roll_Group=',',
						@School_Year =',',
						@House=',' ,
						@SubjectName='".$TeachSubject.",' ,
						@TTPeriod= '".$CurrentSchoolYear.",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);

		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			$TmpSchoolCode = $_REQUEST["SchoolCode"];
			
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
				if($isiCal != 1 && $srcForm == 'NewUserForm') { 
					$TmpStr .= " \"".$TmpID."\"";
				}
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			else
			{
				$TmpStr = "<".$TmpEmail.">";
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}

			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else
			{
				$TmpStr = str_replace("'","\'",$TmpStr);
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
			}
		}
		
		if($isiCal != 1 && $srcForm == 'NewUserForm') {
			$LoadJavascript .= "SubmitRecords(); \n";
		}
		
		$LoadJavascript .= "</script> ";
	}
	else if ($actionLevel == "SchoolYear")
	{
		$sqlSchoolYear = "exec Get_RollGroupList_BySchoolYear @School_Year=".$SchoolYear;
		$ReturnArr = $LibDB->returnArray($sqlSchoolYear);
		if(count($ReturnArr) > 0) {
			for($i=0; $i<count($ReturnArr); $i++) {
				$RollGroupList .= $ReturnArr[$i]['Roll_Group'].',';	
			}
		}else {
			$RollGroupList = ',';
		}	
			
		$Sql =  	"
						exec Search_StaffEmail
						@Staff_Type=',',
						@Roll_Group='".$RollGroupList."',
						@School_Year ='".$SchoolYear.",',
						@House=',' ,
						@SubjectName=',' ,
						@TTPeriod= '".$CurrentSchoolYear.",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);

		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			$TmpSchoolCode = $_REQUEST["SchoolCode"];
			
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
				if($isiCal != 1 && $srcForm == 'NewUserForm') { 
					$TmpStr .= " \"".$TmpID."\"";
				}
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			else
			{
				$TmpStr = "<".$TmpEmail.">";
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}

			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else
			{
				$TmpStr = str_replace("'","\'",$TmpStr);
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
			}
		}
		
		if($isiCal != 1 && $srcForm == 'NewUserForm') {
			$LoadJavascript .= "SubmitRecords(); \n";
		}
		
		$LoadJavascript .= "</script> ";
	}
	else if ($actionLevel == "RollGroup" && $TeachType != "6")
	{
		$Sql =  	"
						exec Search_StaffEmail
						@Staff_Type=',',
						@Teach_Roll_Group=',',
						@Roll_Group='".$RollGroup.",',
						@School_Year =',',
						@House=',' ,
						@SubjectName=',' ,
						@TTPeriod= '".$CurrentSchoolYear.",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);

		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			$TmpSchoolCode = $_REQUEST["SchoolCode"];
			
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
				if($isiCal != 1 && $srcForm == 'NewUserForm') { 
					$TmpStr .= " \"".$TmpID."\"";
				}
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			else
			{
				$TmpStr = "<".$TmpEmail.">";
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}

			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else
			{
				$TmpStr = str_replace("'","\'",$TmpStr);
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
			}
		}
		
		if($isiCal != 1 && $srcForm == 'NewUserForm') {
			$LoadJavascript .= "SubmitRecords(); \n";
		}
		
		$LoadJavascript .= "</script> ";
	}
	else if ($actionLevel == "House")
	{
		$Sql =  	"
						exec Search_StaffEmail
						@Staff_Type=',',
						@Roll_Group=',',
						@School_Year =',',
						@House='".$House.",' ,
						@SubjectName=',' ,
						@TTPeriod= '".$CurrentSchoolYear.",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);

		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			$TmpSchoolCode = $_REQUEST["SchoolCode"];
			
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
				if($isiCal != 1 && $srcForm == 'NewUserForm') { 
					$TmpStr .= " \"".$TmpID."\"";
				}
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			else
			{
				$TmpStr = "<".$TmpEmail.">";
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}

			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else
			{
				$TmpStr = str_replace("'","\'",$TmpStr);
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
			}
		}
		
		if($isiCal != 1 && $srcForm == 'NewUserForm') {
			$LoadJavascript .= "SubmitRecords(); \n";
		}
		
		$LoadJavascript .= "</script> ";
	}
	else if ($actionLevel == "School_Year_House")
	{
		$Sql =  	"
						exec Search_StaffEmail
						@Staff_Type=',',
						@Roll_Group=',',
						@School_Year ='".$School_Year_House.",',
						@House=',' ,
						@SubjectName=',' ,
						@TTPeriod= '".$CurrentSchoolYear.",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);

		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			$TmpSchoolCode = $_REQUEST["SchoolCode"];
			
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
				if($isiCal != 1 && $srcForm == 'NewUserForm') { 
					$TmpStr .= " \"".$TmpID."\"";
				}
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			else
			{
				$TmpStr = "<".$TmpEmail.">";
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}

			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else
			{
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
			}
		}
		
		if($isiCal != 1 && $srcForm == 'NewUserForm') {
			$LoadJavascript .= "SubmitRecords(); \n";
		}
		
		$LoadJavascript .= "</script> ";
	}
	else if ($actionLevel == "SchoolGroup")
	{
		$Sql =  	"
						SELECT
							b.UserName, 
							CASE 
								WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') = '' THEN RTRIM(LTRIM(b.UserEmail)) collate Latin1_General_CI_AS
								WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') != '' THEN RTRIM(LTRIM(i.Email)) collate Latin1_General_CI_AS
								WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') != '' THEN RTRIM(LTRIM(i.EmailAlias1)) collate Latin1_General_CI_AS
							END AS 'Email'							
						FROM
							MAIL_ADDRESS_MAPPING a
						INNER JOIN
							MAIL_ADDRESS_USER b ON a.MailUserID = b.MailUserID
						LEFT OUTER JOIN intranet_user i ON b.IntranetUserID = i.UserID							
						WHERE						
							a.MailGroupID = '".$SchoolGroup."'
					";
		$ReturnArr = $LibDB->returnArray($Sql);

		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$TmpName = $ReturnArr[$i]["UserName"];
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = '';		# Need UserID
			$TmpSchoolCode = $_REQUEST["SchoolCode"];
			
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			else
			{
				$TmpStr = "<".$TmpEmail.">";
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}

			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else
			{
				$TmpStr = str_replace("'","\'",$TmpStr);
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
			}
		}
		
		if($isiCal != 1 && $srcForm == 'NewUserForm') {
			$LoadJavascript .= "SubmitRecords(); \n";
		}
		
		$LoadJavascript .= "</script> ";
	}
	else if($actionLevel == "Year_Teacher_Type" && $SchoolYear != "")
	{		
		if($Year_Teacher_Type == "0")
		{
			$Sql = "
				exec Search_StaffEmail
				@Staff_Type=',',
				@Roll_Group=',',
				@School_Year ='".$SchoolYear.",',
				@House=',' ,
				@SubjectName=',' ,
				@TTPeriod= '".$CurrentSchoolYear.",'
				";
		}
		else if($Year_Teacher_Type == "1")
		{
			$Sql = "
				exec Search_StaffEmail
				@Staff_Type=',',
				@Roll_Group=',',
				@School_Year =',',
				@House=',' ,
				@SubjectName=',' ,
				@Teach_Roll_Group=',' ,
				@Roll_Group_Year= '".$SchoolYear.",',
				@TTPeriod= '".$CurrentSchoolYear.",'
				";
		}

		$ReturnArr = $LibDB->returnArray($Sql);

		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			$TmpSchoolCode = $_REQUEST["SchoolCode"];
			
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
				if($isiCal != 1 && $srcForm == 'NewUserForm') { 
					$TmpStr .= " \"".$TmpID."\"";
				}
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			else
			{
				$TmpStr = "<".$TmpEmail.">";
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}

			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else
			{
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
			}
		}
		
		if($isiCal != 1 && $srcForm == 'NewUserForm') {
			$LoadJavascript .= "SubmitRecords(); \n";
		}
		
		$LoadJavascript .= "</script> ";
		// end if Year_Teacher_Type
	} 
	else if ($actionLevel == "StudentType")
	{
		if ($StudentType == "0")
		{
			$Sql =  "SELECT DISTINCT A.Name, A.UserID, A.Email FROM (
								SELECT Row_Number() OVER (ORDER BY U.Surname, U.PrefName) as 'Row_Num', U.Surname COLLATE Latin1_General_CI_AS + ',' + ' ' + U.PrefName as 'Name', U.UserID, IsNULL(U.EmailAlias1, U.Email) as 'Email'
								FROM INTRANET_USER U WITH (NOLOCK)
								INNER JOIN MAZE_STMA WITH (NOLOCK) ON MAZE_STMA.SKEY = U.MazeKey
								WHERE 
									MAZE_STMA.TTPeriod = '".$CurrentSchoolYear."' 
									AND U.Status = 'A' 
									AND RTRIM(LTRIM(IsNull(U.Email,0))) <> 'NA' 
									AND RTRIM(LTRIM(IsNull(U.EmailAlias1,0))) <> 'NA' 
							) as A
							ORDER BY A.Name, A.Email, A.UserID";
			$ReturnArr = $LibDB->returnArray($Sql);

			$LoadJavascript .= "<script language='javascript' > ";
			for ($i=0;$i<count($ReturnArr);$i++)
			{
				$TmpName = $ReturnArr[$i]["Name"];
				$TmpEmail = $ReturnArr[$i]["Email"];
				$TmpID = $ReturnArr[$i]["UserID"];
				$TmpSchoolCode = $_REQUEST["SchoolCode"];
				
				if ($TmpName != "")
				{
					$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
					if($isiCal != 1 && $srcForm == 'NewUserForm') { 
						$TmpStr .= " \"".$TmpID."\"";
					}
					$TmpStr .= " \"".$TmpSchoolCode."\"";
				}
				else
				{
					$TmpStr = "<".$TmpEmail.">";
					$TmpStr .= " \"".$TmpSchoolCode."\"";
				}
				
				$TmpStr = str_replace("'","\'",$TmpStr);
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
			}
			
			if($isiCal != 1 && $srcForm == 'NewUserForm') {
				$LoadJavascript .= "SubmitRecords(); \n";
			}
			
			$LoadJavascript .= "</script> ";
		}
		else if ($StudentType == "1")
		{
			$Sql =  "SELECT DISTINCT A.Name, A.UserID, A.Email FROM (
								SELECT Row_Number() OVER (ORDER BY U.Surname, U.PrefName) as 'Row_Num', U.Surname COLLATE Latin1_General_CI_AS + ',' + ' ' + U.PrefName as 'Name', U.UserID, IsNULL(U.EmailAlias1, U.Email) as 'Email'
								FROM INTRANET_USER U WITH (NOLOCK)
								INNER JOIN CAS_USER_ACTIVITY_MAPPING WITH (NOLOCK) ON CAS_USER_ACTIVITY_MAPPING.UserId = U.UserID
								WHERE 
									CAS_USER_ACTIVITY_MAPPING.Status = 1 
									AND RTRIM(LTRIM(IsNull(U.Email,0))) <> 'NA' 
									AND RTRIM(LTRIM(IsNull(U.EmailAlias1,0))) <> 'NA' 
								) as A
							ORDER BY A.Name, A.Email, A.UserID ";
			$ReturnArr = $LibDB->returnArray($Sql);

			$LoadJavascript .= "<script language='javascript' > ";
			for ($i=0;$i<count($ReturnArr);$i++)
			{
				$TmpName = $ReturnArr[$i]["Name"];
				$TmpEmail = $ReturnArr[$i]["Email"];
				$TmpID = $ReturnArr[$i]["UserID"];
				$TmpSchoolCode = $_REQUEST["SchoolCode"];
				
				if ($TmpName != "")
				{
					$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
					if($isiCal != 1 && $srcForm == 'NewUserForm') { 
						$TmpStr .= " \"".$TmpID."\"";
					}
					$TmpStr .= " \"".$TmpSchoolCode."\"";
				}
				else
				{
					$TmpStr = "<".$TmpEmail.">";
					$TmpStr .= " \"".$TmpSchoolCode."\"";
				}
			
				$TmpStr = str_replace("'","\'",$TmpStr);
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
			}
			
			if($isiCal != 1 && $srcForm == 'NewUserForm') {
				$LoadJavascript .= "SubmitRecords(); \n";
			}
			
			$LoadJavascript .= "</script> ";
		}
		else if ($StudentType == "2")
		{
			$Sql =  "SELECT DISTINCT A.Name, A.UserID, A.Email FROM (
								SELECT Row_Number() OVER (ORDER BY U.Surname, U.PrefName) as 'Row_Num', U.Surname  COLLATE Latin1_General_CI_AS + ',' + ' ' + U.PrefName COLLATE Latin1_General_CI_AS as 'Name', U.UserID, IsNULL(U.EmailAlias1, U.Email) as 'Email'
								FROM STUDENT S WITH (NOLOCK)
								INNER JOIN INTRANET_USER U WITH (NOLOCK) ON S.UserID = U.UserID
								WHERE 
									S.Roll_Group is not NULL 
									and S.Roll_Group <> '' 
									AND U.Status = 'A' 
									AND RTRIM(LTRIM(IsNull(U.Email,0))) <> 'NA' 
									AND RTRIM(LTRIM(IsNull(U.EmailAlias1,0))) <> 'NA' 
								) as A
							ORDER BY A.Name, A.Email, A.UserID ";
			$ReturnArr = $LibDB->returnArray($Sql);

			$LoadJavascript .= "<script language='javascript' > ";
			for ($i=0;$i<count($ReturnArr);$i++)
			{
				$TmpName = $ReturnArr[$i]["Name"];
				$TmpEmail = $ReturnArr[$i]["Email"];
				$TmpID = $ReturnArr[$i]["UserID"];
				$TmpSchoolCode = $_REQUEST["SchoolCode"];
				
				if ($TmpName != "")
				{
					$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
					if($isiCal != 1 && $srcForm == 'NewUserForm') { 
						$TmpStr .= " \"".$TmpID."\"";
					}
					$TmpStr .= " \"".$TmpSchoolCode."\"";
				}
				else
				{
					$TmpStr = "<".$TmpEmail.">";
					$TmpStr .= " \"".$TmpSchoolCode."\"";
				}

				$TmpStr = str_replace("'","\'",$TmpStr);
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
			}
			
			if($isiCal != 1 && $srcForm == 'NewUserForm') {
				$LoadJavascript .= "SubmitRecords(); \n";
			}
			
			$LoadJavascript .= "</script> ";
		}
	}
	else if ($actionLevel == "StudentSchoolYear")
	{
		$Sql =  "SELECT DISTINCT A.Name, A.UserID, A.Email FROM (
								SELECT Row_Number() OVER (ORDER BY U.Surname, U.PrefName) as 'Row_Num', U.Surname COLLATE Latin1_General_CI_AS + ',' + ' ' + U.PrefName as 'Name', U.UserID, IsNULL(U.EmailAlias1, U.Email) as 'Email'
								FROM INTRANET_USER U WITH (NOLOCK)
								INNER JOIN MAZE_STMA WITH (NOLOCK) ON MAZE_STMA.SKEY = U.MazeKey
								WHERE 
									MAZE_STMA.TTPeriod = '".$CurrentSchoolYear."' 
									AND LEFT(MAZE_STMA.MKey, 2) = '".substr($StudentSchoolYear, 1, 2)."' 
									AND U.Status = 'A' 
									AND RTRIM(LTRIM(IsNull(U.Email,0))) <> 'NA' 
									AND RTRIM(LTRIM(IsNull(U.EmailAlias1,0))) <> 'NA' 
							) as A
							ORDER BY A.Name, A.Email, A.UserID";
		$ReturnArr = $LibDB->returnArray($Sql);

		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$TmpName = $ReturnArr[$i]["Name"];
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			$TmpSchoolCode = $_REQUEST["SchoolCode"];
			
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
				if($isiCal != 1 && $srcForm == 'NewUserForm') { 
					$TmpStr .= " \"".$TmpID."\"";
				}
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			else
			{
				$TmpStr = "<".$TmpEmail.">";
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			
			$TmpStr = str_replace("'","\'",$TmpStr);
			$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
		}
		
		if($isiCal != 1 && $srcForm == 'NewUserForm') {
			$LoadJavascript .= "SubmitRecords(); \n";
		}
			
		$LoadJavascript .= "</script> ";
	}
	else if ($actionLevel == "StudentSchoolYear1")
	{
		// $Sql =  "exec Get_Student_BySubject @SubjectCode='".substr($StudentSchoolYear1, 0, 5)."', @SubjectClass='".substr($StudentSchoolYear1, 5, 2)."', @TTPeriod='".$_SESSION['SSV_SCHOOLYEAR']."', @PageNo=1, @PageSize=999, @SkipNAEmail=1 ";
		
		if (substr($thisSubjectCode, -1) == 0 ) $thisSubjectCode = substr($StudentSchoolYear1, 0, 4);
				
				$Sql =  "exec Get_Student_BySubject @SubjectCode='".$thisSubjectCode."', @SubjectClass='".substr($StudentSchoolYear1, 5, 2)."', @TTPeriod='".$CurrentSchoolYear."', @PageNo=1, @PageSize=999, @SkipNAEmail=1  ";
		$ReturnArr = $LibDB->returnArray($Sql);

		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$TmpName = $ReturnArr[$i]["Name"];
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			$TmpSchoolCode = $_REQUEST["SchoolCode"];
			
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
				if($isiCal != 1 && $srcForm == 'NewUserForm') { 
					$TmpStr .= " \"".$TmpID."\"";
				}
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			else
			{
				$TmpStr = "<".$TmpEmail.">";
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			
			$TmpStr = str_replace("'","\'",$TmpStr);
			$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
		}
		
		if($isiCal != 1 && $srcForm == 'NewUserForm') {
			$LoadJavascript .= "SubmitRecords(); \n";
		}
		
		$LoadJavascript .= "</script> ";
	}
	else if ($actionLevel == "ActivityId")
	{
		$Sql =  "exec Get_Student_ByActivity @ActivityId='".$ActivityId."', @PageNo=1, @PageSize=999, @SkipNAEmail=1 ";
		$ReturnArr = $LibDB->returnArray($Sql);

		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$TmpName = $ReturnArr[$i]["Name"];
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			$TmpSchoolCode = $_REQUEST["SchoolCode"];
			
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
				if($isiCal != 1 && $srcForm == 'NewUserForm') { 
					$TmpStr .= " \"".$TmpID."\"";
				}
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			else
			{
				$TmpStr = "<".$TmpEmail.">";
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			
			$TmpStr = str_replace("'","\'",$TmpStr);
			$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
		}
		
		if($isiCal != 1 && $srcForm == 'NewUserForm') {
			$LoadJavascript .= "SubmitRecords(); \n";
		}
		
		$LoadJavascript .= "</script> ";
	}
	else if ($actionLevel == "StudentRollGroup")
	{
		$Sql =  "exec Get_Student_ByRollGroup @RollGroup='".$StudentRollGroup."', @PageNo=1, @PageSize=999, @SkipNAEmail=1 ";
		$ReturnArr = $LibDB->returnArray($Sql);

		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$TmpName = $ReturnArr[$i]["Name"];
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			$TmpSchoolCode = $_REQUEST["SchoolCode"];
			
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
				if($isiCal != 1 && $srcForm == 'NewUserForm') { 
					$TmpStr .= " \"".$TmpID."\"";
				}
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			else
			{
				$TmpStr = "<".$TmpEmail.">";
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			
			$TmpStr = str_replace("'","\'",$TmpStr);
			$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
		}
		
		if($isiCal != 1 && $srcForm == 'NewUserForm') {
			$LoadJavascript .= "SubmitRecords(); \n";
		}
		
		$LoadJavascript .= "</script> ";
	}
	else if ($actionLevel == "ByStudentType")
	{
		## RollGroup: Result getting from teacher whose Staff Type is 'T' in the current academic year (Temp Solution)
		## Activity:  ?
		## Subject Class: ?
		$Sql = "exec Search_StaffEmail
				@Staff_Type ='T,',
				@TTPeriod   = '".$CurrentSchoolYear.",'
				";
		$ReturnArr = $LibDB->returnArray($Sql);
		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			$TmpSchoolCode = $_REQUEST["SchoolCode"];
			
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
				if($isiCal != 1 && $srcForm == 'NewUserForm') { 
					$TmpStr .= " \"".$TmpID."\"";
				}
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			else
			{
				$TmpStr = "<".$TmpEmail.">";
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}

			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else
			{
				$TmpStr = str_replace("'","\'",$TmpStr);
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
			}
		}
		
		if($isiCal != 1 && $srcForm == 'NewUserForm') {
			$LoadJavascript .= "SubmitRecords(); \n";
		}
		
		$LoadJavascript .= "</script> ";
	}
	else if ($actionLevel == "ByStudentRollGroup")
	{	
		$Sql = "SELECT distinct U_T.UserID, U_T.MazeKey, 
				CASE 
					WHEN U_T.EmailAlias1 IS NULL THEN U_T.Email 
					WHEN U_T.EmailAlias1 = '' THEN U_T.Email 
					ELSE U_T.EmailAlias1 
				END AS Email, 
				U_T.Surname + ', ' + U_T.PrefName as 'OfficialFullName', U_T.Surname, U_T.FirstName 
				FROM INTRANET_USER AS U_S WITH (NOLOCK) 
				INNER JOIN MAZE_STMA WITH (NOLOCK) ON MAZE_STMA.SKEY = U_S.MazeKey AND MAZE_STMA.TTPERIOD = '".$CurrentSchoolYear."'
				INNER JOIN MAZE_TTTG WITH (NOLOCK) ON MAZE_STMA.IDENT = MAZE_TTTG.IDENT
				INNER JOIN INTRANET_USER U_T WITH (NOLOCK) ON MAZE_TTTG.T1Teach = U_T.MazeKey OR MAZE_TTTG.T2Teach = U_T.MazeKey
				INNER JOIN STUDENT AS ST WITH (NOLOCK) ON U_S.UserID = ST.UserID

				WHERE ST.Roll_Group = '".$ByStudentRollGroup."'
				AND RTRIM(LTRIM(IsNull(U_S.Email,''))) <> 'NA' 
				AND RTRIM(LTRIM(IsNull(U_S.EmailAlias1,''))) <> 'NA' 
				";
			
			$ReturnArr = $LibDB->returnArray($Sql);
			
			// debug_r($ReturnArr);
			$LoadJavascript .= "<script language='javascript' > ";
			for ($i=0;$i<count($ReturnArr);$i++)
			{
				$TmpName = $ReturnArr[$i]["OfficialFullName"];
				$TmpEmail = $ReturnArr[$i]["Email"];
				$TmpID = $ReturnArr[$i]["UserID"];
				$TmpSchoolCode = $_REQUEST["SchoolCode"];
			
				if ($TmpName != "")
				{
					$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
					if($isiCal != 1 && $srcForm == 'NewUserForm') { 
						$TmpStr .= " \"".$TmpID."\"";
					}
					$TmpStr .= " \"".$TmpSchoolCode."\"";
				}
				else
				{
					$TmpStr = "<".$TmpEmail.">";
					$TmpStr .= " \"".$TmpSchoolCode."\"";
				}

				if($isiCal == 1)
				{
					//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
				}
				else
				{
					$TmpStr = str_replace("'","\'",$TmpStr);
					$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
				}
			}
			
			if($isiCal != 1 && $srcForm == 'NewUserForm') {
				$LoadJavascript .= "SubmitRecords(); \n";
			}
			
			$LoadJavascript .= "</script> ";
		
	}
	else if ($actionLevel == "StudentList")
	{
		$Sql =  "   exec Search_StaffEmail
					@Roll_Group = '".$ByStudentRollGroup.",' ,
					@StudentID  = ".$StudentList." ,
					@TTPeriod	= '".$CurrentSchoolYear.",'
				";
		$ReturnArr = $LibDB->returnArray($Sql);

		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			$TmpSchoolCode = $_REQUEST["SchoolCode"];
			
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
				if($isiCal != 1 && $srcForm == 'NewUserForm') { 
					$TmpStr .= " \"".$TmpID."\"";
				}
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			else
			{
				$TmpStr = "<".$TmpEmail.">";
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}

			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else
			{
				$TmpStr = str_replace("'","\'",$TmpStr);
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
			}
		}
		
		if($isiCal != 1 && $srcForm == 'NewUserForm') {
			$LoadJavascript .= "SubmitRecords(); \n";
		}
		
		$LoadJavascript .= "</script> ";
	}
	else if ($actionLevel == "FoundationGroup") // 2009-08-04
	{
		$Sql =  	"
						SELECT
							b.UserName, 
							CASE 
								WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') = '' THEN RTRIM(LTRIM(b.UserEmail)) collate Latin1_General_CI_AS
								WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') != '' THEN RTRIM(LTRIM(i.Email)) collate Latin1_General_CI_AS
								WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') != '' THEN RTRIM(LTRIM(i.EmailAlias1)) collate Latin1_General_CI_AS
							END AS 'Email'							
						FROM
							$ESFDBName.dbo.MAIL_ADDRESS_MAPPING a
						INNER JOIN
							$ESFDBName.dbo.MAIL_ADDRESS_USER b ON a.MailUserID = b.MailUserID
						LEFT OUTER JOIN $ESFDBName.dbo.intranet_user i ON b.IntranetUserID = i.UserID							
						WHERE						
							a.MailGroupID = '".$FoundationGroup."'
					";
		$LibESF = new database(false,false,true);
		$ReturnArr = $LibESF->returnArray($Sql);

		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$TmpName = $ReturnArr[$i]["UserName"];
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = '';		# Need UserID
			$TmpSchoolCode = $_REQUEST["SchoolCode"];
			
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
				if($isiCal != 1 && $srcForm == 'NewUserForm') { 
					$TmpStr .= " \"".$TmpID."\"";
				}
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			else
			{
				$TmpStr = "<".$TmpEmail.">";
				$TmpStr .= " \"".$TmpSchoolCode."\"";
			}
			
			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else
			{
				$TmpStr = str_replace("'","\'",$TmpStr);
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
			}
		}
		
		if($isiCal != 1 && $srcForm == 'NewUserForm') {
			$LoadJavascript .= "SubmitRecords(); \n";
		}
		
		$LoadJavascript .= "</script> ";
	}// 2009-08-04
	
	else if ($TeachType == 6 && $ParentType == 2) {
		
		if ($actionLevel == 'RollGroup' && $RollGroup != '' && $StudentNameList == '') {
			
			$sql = 'Exec Get_Student_ByRollGroup_CheckContacts @RollGroup = "'.$RollGroup.'"';
				
				$StudentList = $LibDB->returnArray($sql);
				
				// echo count($StudentList);
				// debug_r($StudentList);
				for ($StudentListIdx = 0; $StudentListIdx < count($StudentList); $StudentListIdx++) {
					$ParentList = null;
					$sql = 'Exec Get_Parent_ByStudent @UserID = "'.$StudentList[$StudentListIdx]['UserID'].'"';
					// echo $sql.'<br>';
					$ParentList = $LibDB->returnArray($sql);
					// debug_r($ParentList);
					$LoadJavascript .= "<script language='javascript' > ";
					for ($i=0;$i<count($ParentList);$i++)
					{
						
						$TmpName = $ParentList[$i]["ParentName"];
						$TmpEmail = $ParentList[$i]["Email"];
						$TmpID = $ReturnArr[$i]["UserID"];
						$TmpSchoolCode = $_REQUEST["SchoolCode"];
						
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
							if($isiCal != 1 && $srcForm == 'NewUserForm') { 
								$TmpStr .= " \"".$TmpID."\"";
							}
							$TmpStr .= " \"".$TmpSchoolCode."\"";
						}
						else
						{
							$TmpStr = '';
							$TmpStr .= " \"".$TmpSchoolCode."\"";
						}
						// echo $StudentListIdx. ': ' . 'TmpStr : ' . $TmpStr.'<br>';
						if ($TmpStr != "") {
							if($isiCal == 1)
							{
								//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
							}
							else
							{
								$TmpStr = str_replace("'","\'",$TmpStr);
								// echo $TmpStr.'<br>';
								$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
							}
						}
					}
					
					if($isiCal != 1 && $srcForm == 'NewUserForm') {
						$LoadJavascript .= "SubmitRecords(); \n";
					}
					
					$LoadJavascript .= "</script> ";
					
				}
				
		} 
		
		elseif ($actionLevel == 'StudentName' && $RollGroup != '' && $StudentNameList != '') {
			
			$StudentList = explode('|',$StudentNameList);
			
			for ($StudentListIdx = 0; $StudentListIdx < count($StudentList); $StudentListIdx++) {
			$sql = 'Exec Get_Parent_ByStudent @UserID = "'.$StudentList[$StudentListIdx].'"';
					// echo $sql.'<br>';
					$ParentList = $LibDB->returnArray($sql);
					
					$LoadJavascript .= "<script language='javascript' > ";
					for ($i=0;$i<count($ParentList);$i++)
					{
						
						$TmpName = $ParentList[$i]["ParentName"];
						$TmpEmail = $ParentList[$i]["Email"];
						$TmpID = $ReturnArr[$i]["UserID"];
						$TmpSchoolCode = $_REQUEST["SchoolCode"];
						
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
							if($isiCal != 1 && $srcForm == 'NewUserForm') { 
								$TmpStr .= " \"".$TmpID."\"";
							}
							$TmpStr .= " \"".$TmpSchoolCode."\"";
						}
						else
						{
							$TmpStr = '';
							$TmpStr .= " \"".$TmpSchoolCode."\"";
						}
						// echo $StudentListIdx. ': ' . 'TmpStr : ' . $TmpStr.'<br>';
						if ($TmpStr != "") {
							if($isiCal == 1)
							{
								//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
							}
							else
							{
								$TmpStr = str_replace("'","\'",$TmpStr);
								// echo $TmpStr.'<br>';
								$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
							}
						}
					}
					
					if($isiCal != 1 && $srcForm == 'NewUserForm') {
						$LoadJavascript .= "SubmitRecords(); \n";
					}
					
					$LoadJavascript .= "</script> ";
			}
		
		}
		
	}
	
} // end if action add

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

echo $LoadJavascript;
echo $lui->Get_Div_Open("module_bulletin"," class=\"module_content\" " );
echo $lui->Get_Div_Open(""," style=\"width:99%\" " );

echo $lui->Get_Div_Open("module_content_header");
echo $lui->Get_Div_Open("module_content_header_title");
echo $lui->Get_HyperLink_Open("#");
echo ($isiCal == 1 || !empty($startFrom)) ? $Lang['calendar']['HeaderTitle'] : $Lang['email']['HeaderTitle'];
echo $lui->Get_HyperLink_Close();
echo $lui->Get_Div_Close();
echo "<br />";
echo $lui->Get_Div_Close();

echo "<br style=\"clear:none\">";


////////////// DEBUG ///////////////////
if((($_REQUEST["SchoolCode"] == $_SESSION["SchoolCode"]) && (Auth(array("MySchool-TeachingStaff", "MySchool-Student"), "TARGET", "or") || Auth(array("MySchool-NonTeachingStaff", "MySchool-Student"), "TARGET", "or"))) ||
  ((($_REQUEST["SchoolCode"] != $_SESSION["SchoolCode"]) && (Auth(array("AllSchool-TeachingStaff", "AllSchool-Student"), "TARGET", "or") || Auth(array("AllSchool-NonTeachingStaff", "AllSchool-Student"), "TARGET", "or")))))
{
//////////// END DEBUG ///////////////////

//echo $lui->Get_Div_Open("commontabs");
echo $lui->Get_Div_Open();
//$SchoolLink .=  $lui->Get_HyperLink_Open("imail_address_book.php?ChooseType=1");
$SchoolLink .= $lui->Get_HyperLink_Open("#");
$SchoolLink .= $lui->Get_Span_Open();
$SchoolLink .= $Lang['email']['SelectRecipient'];
$SchoolLink .= $lui->Get_Span_Close();
$SchoolLink .= $lui->Get_HyperLink_Close();
/*
$PersonalLink .= $lui->Get_HyperLink_Open("imail_address_book.php?ChooseType=2");
$PersonalLink .= $lui->Get_Span_Open();
$PersonalLink .= $Lang['email']['Personal'];
$PersonalLink .= $lui->Get_Span_Close();
$PersonalLink .= $lui->Get_HyperLink_Close();
*/

echo $lui->Get_Div_Close();

//echo "<pre>";
//var_dump($_POST);
//echo "</pre>";
/////////////////////////////////////////
////   Gen the menu list ////////////////
if($RollBackStep == "")
$RollBackStep = 6; // MAX STEP SHOW

$NatArr = array();
$NatArr[]= array($Lang['email']['SelectRecipient'], 0);

### RollBack Step 1
if($TeachType != "" && $RollBackStep >= 1)
{
	if($TeachType == 0)
	$NatArr[] = array($Lang['email']['TeachingStaff'], 0);
	else if($TeachType == 1)
	$NatArr[] = array($Lang['email']['NonTeachingStaff'], 1);
	else if($TeachType == 2)
	$NatArr[] = array($Lang['email']['Student'], 2);
	else if($TeachType == 5)
	$NatArr[] = array($Lang['email']['SchoolBasedGroup'], 5);
	else if($TeachType == 6)
	$NatArr[] = array($Lang['email']['Parent'], 6);
	else if($TeachType == 7) // 2009-08-04
	$NatArr[] = array($Lang['email']['FoundationBasedGroup'], 7); // 2009-08-04
}
### RollBack Step 2
if($TeachType2 != "" && $RollBackStep >= 2)
{
	if($TeachType2 == 0)
	$NatArr[] = array($Lang['email']['Individual'], 0);
	else if($TeachType2 == 1)
	$NatArr[] = array($Lang['email']['Subjects'], 1);
	else if($TeachType2 == 2)
	$NatArr[] = array($Lang['email']['Years'], 2);
	else if($TeachType2 == 5)
	$NatArr[] = array($Lang['email']['RollGroups'], 5);
	else if($TeachType2 == 6)
	$NatArr[] = array($Lang['email']['Student'], 6);

	//else if($TeachType2 == 3)
	//$NatArr[] = array($Lang['email']['Sections'], 3);
}

if($StudentType != "" && $RollBackStep >= 2)
{
	if($StudentType == 0)
	$NatArr[] = array($Lang['email']['SubjectClass'], 0);
	else if($StudentType == 1)
	$NatArr[] = array($Lang['email']['Activity'], 1);
	else if($StudentType == 2)
	$NatArr[] = array($Lang['email']['StudentRollGroup'], 2);
}

if($SchoolGroup != "" && $RollBackStep >= 2)
{
	$Sql =  " SELECT GroupName, MailGroupID FROM MAIL_ADDRESS_GROUP WHERE GroupType='S' ";
	$ReturnArr = $LibDB->returnArray($Sql);

	for ($i=0;$i<count($ReturnArr);$i++)
	{
		if($SchoolGroup == $ReturnArr[$i]["MailGroupID"])
		{
			$NatArr[] = array($ReturnArr[$i]["GroupName"], $ReturnArr[$i]["MailGroupID"]);
			break;
		}
	}
} // end school group

// 2009-08-04
if($FoundationGroup != "" && $RollBackStep >= 2)
{
	$Sql =  " SELECT GroupName, MailGroupID FROM $ESFDBName.dbo.MAIL_ADDRESS_GROUP WHERE GroupType='F' ";
	$LibESF = new database(false,false,true);
	$ReturnArr = $LibESF->returnArray($Sql);

	for ($i=0;$i<count($ReturnArr);$i++)
	{
		if($FoundationGroup == $ReturnArr[$i]["MailGroupID"])
		{
			$NatArr[] = array($ReturnArr[$i]["GroupName"], $ReturnArr[$i]["MailGroupID"]);
			break;
		}
	}
} // end foundation group

if ($ParentType != "" && $RollBackStep >=2)
{
	if ($ParentType == 2)
	$NatArr[] = array($Lang['email']['RollGroups'], 2);
}

### RollBack Step 3
if($TeachSubject != "" && $RollBackStep >= 3)
{
	// Subject
	$Sql =  "exec Get_SubjectNameList_ByTTPeriod @TTPeriod='".$CurrentSchoolYear."' ";
	$ReturnArr = $LibDB->returnArray($Sql);

	for ($i=0;$i<count($ReturnArr);$i++)
	{
		if($TeachSubject == $ReturnArr[$i]["Subject_Code"])
		{
			$NatArr[] = array($ReturnArr[$i]["FullName"], $ReturnArr[$i]["Subject_Code"]);
			break;
		}
	}
} // end if Teach Subject

if($House != "" && $RollBackStep >= 3)
{
	$Sql =  "exec Get_HouseList";
	$ReturnArr = $LibDB->returnArray($Sql);

	for ($i=0;$i<count($ReturnArr);$i++)
	{
		if($House == $ReturnArr[$i]["HouseCode"])
		{
			$NatArr[] = array($ReturnArr[$i]["HouseName"], $ReturnArr[$i]["HouseCode"]);
			break;
		}
	}
} // end if Hosue

if($SchoolYear != "" && $RollBackStep >= 3)
{
	$Sql =  "exec Get_SchoolYearList ";
	$ReturnArr = $LibDB->returnArray($Sql);

	for ($i=0;$i<count($ReturnArr);$i++)
	{
		if($SchoolYear == $ReturnArr[$i]["School_Year"])
		{
			$NatArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);
			break;
		}
	}
} // end if school year

/*if($SchoolYear != "" && $Roll_Group != "" && $RollBackStep >= 4)
{
	$Sql =  "exec Get_RollGroupList_BySchoolYear @School_Year = '".$SchoolYear."' ";
	$ReturnArr = $LibDB->returnArray($Sql);

	$IsFound = false;
	for ($i=0;$i<count($ReturnArr);$i++)
	{
		if($Roll_Group == $ReturnArr[$i]["Roll_Group"])
		{
			$NatArr[] = array($ReturnArr[$i]["Roll_Group"], $ReturnArr[$i]["Roll_Group"]);
			if ($Roll_Group == $ReturnArr[$i]["Roll_Group"])
			{
				$IsFound = true;
			}
			break;
		}
	}
	if (!$IsFound)
	{
		$Roll_Group  = "";
	}
} // end roll group*/

if($StudentSchoolYear != "" && $RollBackStep >= 3)
{
	$Sql =  "exec Get_SchoolYearList ";
	$ReturnArr = $LibDB->returnArray($Sql);

	for ($i=0;$i<count($ReturnArr);$i++)
	{
		if($StudentSchoolYear == $ReturnArr[$i]["School_Year"])
		{
			$NatArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);
			break;
		}
	}
}

if($ActivityId != "" && $RollBackStep >= 3)
{
	$Sql =  "exec Get_ActivityList_BySchoolYear @SchoolYear='".$CurrentSchoolYear."' ";
	$ReturnArr = $LibDB->returnArray($Sql);

	for ($i=0;$i<count($ReturnArr);$i++)
	{
		if($ActivityId == $ReturnArr[$i]["ActivityId"])
		{
			$NatArr[] = array($ReturnArr[$i]["Name"], $ReturnArr[$i]["ActivityId"]);
			break;
		}
	}
}

if($StudentRollGroup != "" && $RollBackStep >= 3)
{
	$Sql =  "exec Get_RollGroupList ";
	$ReturnArr = $LibDB->returnArray($Sql);

	for ($i=0;$i<count($ReturnArr);$i++)
	{
		if($StudentRollGroup == $ReturnArr[$i]["Roll_Group"])
		{
			$NatArr[] = array($ReturnArr[$i]["Roll_Group"], $ReturnArr[$i]["Roll_Group"]);
			break;
		}
	}
}

if($ByStudentType != "" && $RollBackStep >= 3)
{
	if($ByStudentType == 0)
	$NatArr[] = array($Lang['email']['SubjectClass'], 0);
	else if($ByStudentType == 1)
	$NatArr[] = array($Lang['email']['Activity'], 1);
	else if($ByStudentType == 2)
	$NatArr[] = array($Lang['email']['StudentRollGroup'], 2);
}

### RollBack Step 4
if($RollGroup != "" && $RollBackStep >= 4)
{
	$Sql =  "exec Get_RollGroupList";
	$ReturnArr = $LibDB->returnArray($Sql);

	$IsFound = false;
	for ($i=0;$i<count($ReturnArr);$i++)
	{
		if($RollGroup == $ReturnArr[$i]["Roll_Group"])
		{
			$NatArr[] = array($ReturnArr[$i]["Roll_Group"], $ReturnArr[$i]["Roll_Group"]);
			$IsFound = true;
			break;
		}
	}

	if (!$IsFound)
	{
		$RollGroup  = "";
	}
} // end roll group

if($House != "" && $School_Year_House != "" && $RollBackStep >= 4)
{
	$Sql =  "exec Get_SchoolYearList_ByHouse @House = '".$House."' ";
	$ReturnArr = $LibDB->returnArray($Sql);

	$IsFound = false;
	for ($i=0;$i<count($ReturnArr);$i++)
	{
		if($School_Year_House == $ReturnArr[$i]["School_Year"])
		{
			$NatArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);
			if ($School_Year_House == $ReturnArr[$i]["School_Year"])
			{
				$IsFound = true;
			}
			break;
		}
	}
	if (!$IsFound)
	{
		$School_Year_House  = "";
	}
} // end if House->School Year

if($Year_Teacher_Type != "" && $RollBackStep >= 4)
{
	if($Year_Teacher_Type == 0)
	$NatArr[] = array($Lang['email']['All_Teacher'], 0);
	else if($Year_Teacher_Type == 1)
	$NatArr[] = array($Lang['email']['Roll_Group_Teacher'], 1);
}

if($StudentSchoolYear1 != "" && $RollBackStep >= 4)
{
	$Sql =  "exec Get_SubjectClassList_BySchoolYear @TTPeriod='".$CurrentSchoolYear."', @SchoolYear='".$StudentSchoolYear."' ";
	$ReturnArr = $LibDB->returnArray($Sql);

	for ($i=0;$i<count($ReturnArr);$i++)
	{
		if($StudentSchoolYear1 == $ReturnArr[$i]["sukey"].str_pad($ReturnArr[$i]["class"], 2, "0", STR_PAD_LEFT))
		{
			$NatArr[] = array($ReturnArr[$i]["fullname"]." ".$ReturnArr[$i]["class"]." (".$ReturnArr[$i]["sukey"].str_pad($ReturnArr[$i]["class"], 2, "0", STR_PAD_LEFT).")", $ReturnArr[$i]["sukey"].str_pad($ReturnArr[$i]["class"], 2, "0", STR_PAD_LEFT));
			break;
		}
	}
}

if($ByStudentRollGroup != "" && $RollBackStep >= 4)
{
	$Sql =  "exec Get_RollGroupList ";
	$ReturnArr = $LibDB->returnArray($Sql);

	for ($i=0;$i<count($ReturnArr);$i++)
	{
		if($ByStudentRollGroup == $ReturnArr[$i]["Roll_Group"])
		{
			$NatArr[] = array($ReturnArr[$i]["Roll_Group"], $ReturnArr[$i]["Roll_Group"]);
			break;
		}
	}
}

### RollBack Step5
if($StudentList != "" && $RollBackStep >= 5)
{
	$ReturnArr = array();
	$Sql =  "exec Get_Student_ByRollGroup @RollGroup='".$ByStudentRollGroup."', @PageNo=1, @PageSize=999 ";
	$ReturnArr = $LibDB->returnArray($Sql);

	for ($i=0;$i<count($ReturnArr);$i++)
	{
		if($ReturnArr[$i]["UserID"] == $StudentList)
		{
			$NatArr[] = array($ReturnArr[$i]["Name"], $ReturnArr[$i]["UserID"]);
			break;
		}
	}
}




###
echo $lui->Get_Div_Open("", 'class="imail_link_menu"');
echo $lui->Get_Span_Open("", 'class="imail_entry_read_link"');

if (($actionLevel == "TeachType") || ($actionLevel == "TeachType2") || ($actionLevel == "TeachSubject") || 
	($actionLevel == "SchoolYear") || ($actionLevel == "RollGroup") || ($actionLevel == "Roll_Group") || 
	($actionLevel == "SchoolGroup") || ($actionLevel == "StudentType") || ($actionLevel == "StudentSchoolYear") || 
	($actionLevel == "StudentSchoolYear1") || ($actionLevel == "ActivityId") || ($actionLevel == "StudentRollGroup") || 
	($actionLevel == "StudentList") || ($actionLevel == "ByStudentType") || ($actionLevel == "ByStudentRollGroup" 
	|| ($actionLevel == "FoundationGroup")) // 2009-08-04
   )
{
	$TmpCount = count($NatArr) - 1;
}
else
{
	$TmpCount = count($NatArr);
}
for($i = 0; $i < $TmpCount; $i++)
{
	if($i%3 == 0 && $i != 0)
	$MenuList .= $lui->Get_Br();

	$MenuList .= $lui->Get_HyperLink_Open("#", 'onClick="jRollBackStep('.$i.')"');
	if($i == 0)
	$MenuList .= $NatArr[$i][0];
	else
	$MenuList .= " > ".$NatArr[$i][0];
	$MenuList .= $lui->Get_HyperLink_Close();
}

if (isset($_REQUEST["SchoolName"]) && Auth(array("AllSchool"), "TARGET"))
{
	echo $lui->Get_HyperLink_Open("select_school_foundation.php?srcForm=".$srcForm."&fromSrc=".$fromSrc.(!empty($startFrom)?"&startFrom=".$startFrom:""), "") . $_REQUEST["SchoolName"]. $lui->Get_HyperLink_Close();
	echo " > ";
}
else
{
	echo $_REQUEST["SchoolName"];
	echo " > ";
}
echo $MenuList;

echo $lui->Get_Span_Close();
echo $lui->Get_Div_Close();
echo $lui->Get_Br();

/////////////////////////////////////////////////////////////////////
if($RollBackStep < 1)
{
$Year_Teacher_Type = "";
$TeachType = "";
$TeachType2 = "";
$TeachSubject = "";
$SchoolYear = "";
$RollGroup = "";
$House = "";
$School_Year_House = "";
$SchoolGroup = "";
$StudentType = "";
$StudentSchoolYear = "";
$StudentSchoolYear1 = "";
$ActivityId = "";
$StudentRollGroup = "";
$ByStudentType = "";
$ByStudentRollGroup = "";
$StudentList = "";
$StudentNameList = "";
$FoundationGroup = ""; // 2009-08-04

$Step2Select = "";
$Step3Select = "";
$Step4Select = "";
$Step5Select = "";
$Step6Select = "";
$Step7Select = ""; // 2009-08-04
}
else if($RollBackStep < 2)
{
$Year_Teacher_Type = "";
$ParentType = "";
$TeachType2 = "";
$TeachSubject = "";
$SchoolYear = "";
$RollGroup = "";
$House = "";
$School_Year_House = "";
$SchoolGroup = "";
$StudentType = "";
$StudentSchoolYear = "";
$StudentSchoolYear1 = "";
$ActivityId = "";
$StudentRollGroup = "";
$ByStudentType = "";
$ByStudentRollGroup = "";
$StudentList = "";
$StudentNameList = "";
$FoundationGroup = ""; // 2009-08-04

$Step3Select = "";
$Step4Select = "";
$Step5Select = "";
$Step6Select = "";
$Step7Select = ""; // 2009-08-04
}
else if($RollBackStep < 3)
{
$Year_Teacher_Type = "";
$TeachSubject = "";
$SchoolYear = "";
$RollGroup = "";
$House = "";
$School_Year_House = "";
$StudentSchoolYear = "";
$StudentSchoolYear1 = "";
$ActivityId = "";
$StudentRollGroup = "";
$ByStudentType = "";
$ByStudentRollGroup = "";
$StudentList = "";
$StudentNameList = "";

$Step4Select = "";
$Step5Select = "";
$Step6Select = "";
$Step7Select = ""; // 2009-08-04
}
else if($RollBackStep < 4)
{
$Year_Teacher_Type = "";
$RollGroup = "";
$School_Year_House = "";
$StudentSchoolYear1 = "";
$ByStudentRollGroup = "";
$StudentList = "";
$StudentNameList = "";

$Step5Select = "";
$Step6Select = "";
$Step7Select = ""; // 2009-08-04
}
else if($RollBackStep < 5)
{
$Year_Teacher_Type = "";
$RollGroup = "";
$School_Year_House = "";
$StudentSchoolYear1 = "";
$StudentList = "";

$Step6Select = "";
$Step7Select = ""; // 2009-08-04
}


if($Step6Select != "")
{
	$StepSelect = $Step6Select;
	$ActionStep = $ActionStep6;
	$ShowStep = "step7";
	$IsEndingMode = $IsEnding6;
}
else if($Step7Select != "") // 2009-08-04
{
	$StepSelect = $Step7Select;
	$ActionStep = $ActionStep7;
	$ShowStep = "step8";
	$IsEndingMode = $IsEnding7;
}
else if($Step5Select != "")
{
	$StepSelect = $Step5Select;
	$ActionStep = $ActionStep5;
	$ShowStep = "step6";
	$IsEndingMode = $IsEnding5;
}
else if($Step4Select != "")
{
	$StepSelect = $Step4Select;
	$ActionStep = $ActionStep4;
	$ShowStep = "step5";
	$IsEndingMode = $IsEnding4;
}
else if($Step3Select != "")
{
	$StepSelect = $Step3Select;
	$ActionStep = $ActionStep3;
	$ShowStep = "step4";
	$IsEndingMode = $IsEnding3;
}
else if($Step2Select != "")
{
	$StepSelect = $Step2Select;
	$ActionStep = $ActionStep2;
	$ShowStep = "step3";
	$IsEndingMode = $IsEnding2;
}
else
{
	$StepSelect = $Step1Select;
	$ActionStep = $ActionStep1;
	$ShowStep = "step2";
	$IsEndingMode = $IsEnding;
}
	/*
		echo "TeachType: $TeachType ; TeachType2: $TeachType2 ; ByStudentType: $ByStudentType ;<br> ";
		echo "ByStudentRollGroup: $ByStudentRollGroup ; StudentList: $StudentList ; <br>";
		echo "SchoolYear: ".$_SESSION['SSV_SCHOOLYEAR'];
	*/

if($isiCal == 1 && $isiCalEvent == 1){
	if( ($TeachType == 1 && $ShowStep == "step3") ||
		($TeachType == 0 && $TeachType2 == 0 && $ShowStep == "step4") ||
		($TeachType == 0 && $TeachType2 == 1 && $TeachSubject != "" && $ShowStep == "step5") ||
		($TeachType == 0 && $TeachType2 == 2 && $SchoolYear != "" && $Year_Teacher_Type != "" && $ShowStep == "step5") ){
		# Only allow add Individual User, not for Group user data
		$StepBtn .=  $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep.' ');
	} else {
		$StepBtn .= "";
	}
} else {
	$StepBtn .=  $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep.' ');
}

if (!$IsEndingMode)
{
	$StepBtn .=  "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient_foundation.php#'.$ShowStep.'\';this.form.submit()" ');
}

$StepBtn = $lui->Get_Div_Open("form_btn").$StepBtn.$lui->Get_Div_Close();

$A_Link =  "<a name=\"".$ShowStep."\" ></a>";

//$StepSelect = "";
$Step1Select = "";
$Step2Select = "";
$Step3Select = "";
$Step4Select = "";
$Step5Select = "";
$Step6Select = "";
$Step7Select = ""; // 2009-08-04
////////////////////////////////////////

echo $lui->Get_Div_Open("commontabs_board", "class=\"imail_mail_content\"");
echo $lui->Get_Form_Open("forma","POST","select_recipient_foundation.php",'');


echo '<input type="hidden" name="startFrom" value="'.$startFrom.'">';
echo '<input type="hidden" name="SchoolCode" value="'.$_REQUEST["SchoolCode"].'">';

if (isset($_REQUEST["SchoolCode"]))
{
	echo $lui->Get_Input_Hidden("SchoolCode", "SchoolCode", $_REQUEST["SchoolCode"]);
	echo $lui->Get_Input_Hidden("SchoolName", "SchoolName", $_REQUEST["SchoolName"]);
}

if ($StepSelect != "")
{
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $StepSelect;
	echo $StepBtn;
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();
}

## Hidden Values
if($TeachType != ""){
	echo $lui->Get_Input_Hidden("TeachType", "TeachType", $TeachType)."\n";
	if($isiCal == 1){
		switch($TeachType){
			case 0: $char = 'T'; break;
			case 1: $char = 'N'; break;
			case 5: $char = 'B'; break;
			case 6: $char = 'P'; break;
		}
		echo $lui->Get_Input_Hidden("TeachTypePath", "TeachTypePath", $char)."\n";
	}
}
if($ParentType != "") {
	echo $lui->Get_Input_Hidden("ParentType", "ParentType", $ParentType)."\n";
	if($isiCal == 1){
		switch($ParentType){
			case 2: $char = 'R'; break;
		}
		echo $lui->Get_Input_Hidden("ParentTypePath", "ParentTypePath", $char)."\n";
	}
}
if($TeachType2 != ""){
	echo $lui->Get_Input_Hidden("TeachType2", "TeachType2", $TeachType2)."\n";
	if($isiCal == 1){
		switch($TeachType2){
			case 0: $char = 'I'; break;
			case 1: $char = 'C'; break;
			case 2: $char = 'Y'; break;
		}
		echo $lui->Get_Input_Hidden("TeachType2Path", "TeachType2Path", $char)."\n";
	}
}
if($TeachSubject != ""){
	echo $lui->Get_Input_Hidden("TeachSubject", "TeachSubject", $TeachSubject)."\n";
	if($isiCal == 1){
		echo $lui->Get_Input_Hidden("TeachSubjectPath", "TeachSubjectPath", $TeachSubject)."\n";
	}
}
if($SchoolYear != ""){
	echo $lui->Get_Input_Hidden("SchoolYear", "SchoolYear", $SchoolYear)."\n";
	if($isiCal == 1){
		echo $lui->Get_Input_Hidden("SchoolYearPath", "SchoolYearPath", $SchoolYear)."\n";
	}
}
if($RollGroup != ""){
	echo $lui->Get_Input_Hidden("RollGroup", "RollGroup", $RollGroup)."\n";
	if($isiCal == 1){
		//echo $lui->Get_Input_Hidden("Roll_GroupPath", "Roll_GroupPath", $Roll_Group)."\n";
	}
}
if($House != ""){
	echo $lui->Get_Input_Hidden("House", "House", $House)."\n";
	if($isiCal == 1){
		//echo $lui->Get_Input_Hidden("HousePath", "HousePath", $House)."\n";
	}
}
if($School_Year_House != ""){
	echo $lui->Get_Input_Hidden("School_Year_House", "School_Year_House", $School_Year_House)."\n";
}
if($SchoolGroup != ""){
	echo $lui->Get_Input_Hidden("SchoolGroup", "SchoolGroup", $SchoolGroup)."\n";
	if($isiCal == 1){
		echo $lui->Get_Input_Hidden("SchoolGroupPath", "SchoolGroupPath", $SchoolGroup)."\n";
	}
}
// 2009-08-04
if($FoundationGroup != ""){
	echo $lui->Get_Input_Hidden("FoundationGroup", "FoundationGroup", $FoundationGroup)."\n";
	if($isiCal == 1){
		echo $lui->Get_Input_Hidden("FoundationGroupPath", "FoundationGroupPath", $FounadtionGroup)."\n";
	}
}
// End
if($StudentType != ""){
	echo $lui->Get_Input_Hidden("StudentType", "StudentType", $StudentType)."\n";
}
if($StudentSchoolYear != ""){
	echo $lui->Get_Input_Hidden("StudentSchoolYear", "StudentSchoolYear", $StudentSchoolYear)."\n";
}
if($StudentSchoolYear1 != ""){
	echo $lui->Get_Input_Hidden("StudentSchoolYear1", "StudentSchoolYear1", $StudentSchoolYear1)."\n";
}
if($ActivityId != ""){
	echo $lui->Get_Input_Hidden("ActivityId", "ActivityId", $ActivityId)."\n";
}
if($StudentRollGroup != ""){
	echo $lui->Get_Input_Hidden("StudentRollGroup", "StudentRollGroup", $StudentRollGroup)."\n";
}
if($Year_Teacher_Type != ""){
	echo $lui->Get_Input_Hidden("Year_Teacher_Type", "Year_Teacher_Type", $Year_Teacher_Type)."\n";
	if($isiCal == 1){
		switch($Year_Teacher_Type){
			case 0: $char = 'A'; break;
			case 1: $char = 'R'; break;
		}
		echo $lui->Get_Input_Hidden("Year_Teacher_TypePath", "Year_Teacher_TypePath", $char)."\n";
	}
}
if($ByStudentType != ""){
	echo $lui->Get_Input_Hidden("ByStudentType", "ByStudentType", $ByStudentType)."\n";
}
if($ByStudentRollGroup != ""){
	echo $lui->Get_Input_Hidden("ByStudentRollGroup", "ByStudentRollGroup", $ByStudentRollGroup)."\n";
}
if($StudentList != ""){
	echo $lui->Get_Input_Hidden("StudentList", "StudentList", $StudentList)."\n";
}


# Check for iCalendar use only
if($isiCal == 1){
	echo $lui->Get_Input_Hidden("isiCal", "isiCal", $isiCal)."\n";
}
if($isiCalEvent == 1){
	echo $lui->Get_Input_Hidden("isiCalEvent", "isiCalEvent", $isiCalEvent)."\n";
	echo $lui->Get_Input_Hidden("fieldname", "fieldname", $fieldname)."\n";
}

////////////////////    Show Step 1    ////////////////////////////
if ($Step1Select != "")
{
echo $lui->Get_Paragraph_Open("align=\"center\"");
echo $Step1Select;

echo $lui->Get_Div_Open("form_btn");
echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep1.' ');
if (!$IsEnding)
{
	echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient_foundation.php#step2\';this.form.submit()" ');
}
echo $lui->Get_Div_Close();

echo $lui->Get_Paragraph_Close();

echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
echo $lui->Get_Paragraph_Close();
}
////////////////////////////////////////////////////////////////

if ($Step2Select != "")
{
	echo "<a name=\"step2\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step2Select;
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep2.' ');
	if (!$IsEnding2)
	{
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient_foundation.php#step3\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();
} // end if show step2

if ($Step3Select != "")
{
	echo "<a name=\"step3\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step3Select;
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep3.' ');
	if (!$IsEnding3)
	{
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient_foundation.php#step4\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();
} // end if show step3

if ($Step4Select != "")
{
	echo "<a name=\"step4\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step4Select;
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep4.' ');
	if (!$IsEnding4)
	{
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient_foundation.php#step5\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();
} // end if show step4

if ($Step5Select != "")
{
	echo "<a name=\"step5\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step5Select;
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep5.' ');
	if (!$IsEnding5)
	{
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient_foundation.php#step5\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();
} // end if show step5

if ($Step6Select != "")
{
	echo "<a name=\"step6\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step5Select;
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep6.' ');
	if (!$IsEnding6)
	{
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient_foundation.php#step6\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();
} // end if show step6
// 2009-08-04
if ($Step7Select != "")
{
	echo "<a name=\"step7\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step7Select;
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep7.' ');
	if (!$IsEnding7)
	{
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient_foundation.php#step7\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();
} // end if show step7

} //end check auth right

else
{
	echo $lui->Get_Div_Open("commontabs", "style='margin: 10px'");
	echo $lui->Get_HyperLink_Open("#");
	echo $lui->Get_Span_Open();
	echo "You have no permission to select recipient";
	echo $lui->Get_Span_Close();
	echo $lui->Get_HyperLink_Close();
	echo $lui->Get_Br();
}

echo $lui->Get_Div_Open("form_btn");
echo $lui->Get_Input_Button("btn_close1","button",$Lang['btn_close_window'],'class="button_main_act" onclick="window.close()" ');
echo $lui->Get_Div_Close();

echo $lui->Get_Input_Hidden("ParentEmailList","ParentEmailList",$ParentEmailList);
echo $lui->Get_Input_Hidden("StudentNameList","StudentNameList",$StudentNameList);
echo $lui->Get_Input_Hidden("actionLevel","actionLevel");
echo $lui->Get_Input_Hidden("specialAction","specialAction");
echo $lui->Get_Input_Hidden("SCode","SCode",$SchoolCode);
echo $lui->Get_Input_Hidden("SName","SName",$SchoolName);
echo $lui->Get_Input_Hidden("fromSrc","fromSrc",$fromSrc);
echo $lui->Get_Input_Hidden("srcForm","srcForm",$srcForm);
echo $lui->Get_Input_Hidden("RollBackStep","RollBackStep"); // to roll back the last step
echo $lui->Get_Form_Close();

echo $lui->Get_Div_Close();
echo $lui->Get_Div_Close();
echo $lui->Get_Div_Close();

include_once($PathRelative."src/include/template/popup_footer.php");
?>