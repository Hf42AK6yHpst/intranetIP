<?php
// page modifing by : 
$PathRelative = "../../../../";
$CurSubFunction = "Communication";

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}
AuthPage(array("Communication-Mail-GeneralUsage"));

include_once($PathRelative."src/include/class/imap_gamma.php");

#########################################################################################################

## Use Library
$IMap = new imap_gamma();


## Get Data
$CurFolder  = (isset($CurFolder) && $CurFolder != "") ? stripslashes(urldecode($CurFolder)) : $SYS_CONFIG['Mail']['FolderPrefix'];
$CurTag 	= (isset($CurTag) && $CurTag != "") ? stripslashes(urldecode($CurTag)) : '';
$FolderName = (isset($FolderName) && $FolderName != "") ? stripslashes(trim($FolderName)) : "";


## Main
if($CurFolder != "" && $FolderName != ""){
	$FolderName = IMap_Encode_Folder_Name($FolderName);
	$Result = $IMap->Rename_Mail_Folder($CurFolder, $FolderName);
}

if ($Result) {
	$Msg = $Lang['ReturnMsg']['RenameFolderSuccess'];
}
else {
	$Msg = $Lang['ReturnMsg']['RenameFolderUnsuccess'];
}

#########################################################################################################
header("Location: manage_folder.php?Msg=".$Msg);
exit;

?>