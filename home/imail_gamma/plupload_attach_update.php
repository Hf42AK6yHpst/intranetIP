<?php
// editing by
/*
 * 2020-05-01 (Bill): add more forbid file types
 * 2019-05-24 (Carlos): default forbid file types .shtml, .shtm, .stm
 * 2013-06-26 (Carlos): call js bindClickEventToElements() to enumerate all clickable elements to prevent pop up close window prompt of saving draft
 */

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

set_time_limit(0);
intranet_auth();
intranet_opendb();
auth_campusmail();

################################################################################################

## Use Library
$ldb = new libdb();
$lfs = new libfilesystem();
$lui = new interface_html();

## Initization
$MessageUID = '';

## Get Data
$MailAction = (isset($_POST['MailAction']) && $_POST['MailAction'] != "") ? $_POST['MailAction'] : "";
$DraftUID   = (isset($_POST['DraftUID']) && $_POST['DraftUID'] != "") ? $_POST['DraftUID'] : "";
$ActionUID  = (isset($_POST['ActionUID']) && $_POST['ActionUID'] != "") ? $_POST['ActionUID'] : "";
$MessageUID = ($MailAction == "Forward") ? $ActionUID : $DraftUID;

$ComposeTime = $_POST['ComposeTime'];
$ComposeDatetime = date("Y-m-d H:i:s",$ComposeTime);

## Preparation
$FileNumber = 0;
$personal_path = "$file_path/file/gamma_mail/u$UserID";
$plupload_tmp_path = $personal_path."/".$ComposeTime;

if (!is_dir($personal_path))
{
    $lfs->folder_new($personal_path);
}

if (!is_dir($plupload_tmp_path))
{
    $lfs->folder_new($plupload_tmp_path);
}

$FileName = rawurldecode(stripslashes($_REQUEST['upload_file_name']));
$TempName = $plupload_tmp_path."/".$FileName;

## Check file type
$forbid_file_type = false;
// default forbid .shtml, .shtm, .stm
$defaultForbidFileTypes = array_merge((array)$____block_file_upload_extensions____, array('.stm'));
if(isset($SYS_CONFIG['Mail']['ForbiddenFileTypes'])) {
	//$SYS_CONFIG['Mail']['ForbiddenFileTypes'] = array_merge($SYS_CONFIG['Mail']['ForbiddenFileTypes'],array('.shtml','.shtm','.stm'));
    $SYS_CONFIG['Mail']['ForbiddenFileTypes'] = array_merge($SYS_CONFIG['Mail']['ForbiddenFileTypes'], $defaultForbidFileTypes);
}
else {
	//$SYS_CONFIG['Mail']['ForbiddenFileTypes'] = array('.shtml','.shtm','.stm');
    $SYS_CONFIG['Mail']['ForbiddenFileTypes'] = $defaultForbidFileTypes;
}
if(isset($SYS_CONFIG['Mail']['ForbiddenFileTypes']) && sizeof($SYS_CONFIG['Mail']['ForbiddenFileTypes']) > 0) {
	$file_ext = strtolower($lfs->file_ext($TempName));
	if(in_array($file_ext, $SYS_CONFIG['Mail']['ForbiddenFileTypes'])) {
		$forbid_file_type = true;
    }
}

if(!preg_match("/.\../",$TempName) || $forbid_file_type)
{
	$x = '';
	$x .= 'alert(\''.$Lang['Gamma']['Warning']['IllegalFileTypeOrFileTypeForbidden'].'\');'."\n";
	echo $x;

	intranet_closedb();
	exit;
}

## Main
if ($FileName != "")
{
	$sql = 'INSERT INTO MAIL_ATTACH_FILE_MAP 
				(UserID, UploadTime, OriginalFileName, ComposeTime) 
			VALUES 
				('.$UserID.',NOW(),\''.$ldb->Get_Safe_Sql_Query($FileName).'\',\''.$ComposeDatetime.'\') ';
	$ldb->db_db_query($sql);
	$EncodeName = $ldb->db_insert_id();
	
	$sql = "UPDATE MAIL_ATTACH_FILE_MAP SET EncodeFileName = '".$EncodeName."' WHERE FileID = '".$EncodeName."' ";
	$ldb->db_db_query($sql);
}

$des = "$personal_path/".$EncodeName;
if (!is_file($TempName))
{
	$sql = 'DELETE FROM MAIL_ATTACH_FILE_MAP 
			WHERE FileID = \''.$EncodeName.'\' ';
	$ldb->db_db_query($sql);
}
else
{
    if(strpos($FileName, ".") == 0) {
        // do nothing
    }
    else {
        $lfs->lfs_copy($TempName, $des);
        $lfs->chmod_R($des, 0777);
        $FileNumber++;
        $lfs->file_remove($TempName);
    }
}

if ($FileNumber > 0)
{
	// Database setup
	$sql = "SELECT 
				FileID, 
				UserID, 
				UploadTime,
				DraftMailUID,
				OriginalFileName,
				EncodeFileName 
			FROM 
			    MAIL_ATTACH_FILE_MAP 
			WHERE 
				UserID = '".$UserID."' AND 
				(
					(DraftMailUID IS NULL AND OrgDraftMailUID IS NULL) OR 
					(DraftMailUID = '$MessageUID') OR 
					(DraftMailUID IS NULL AND OrgDraftMailUID = '$MessageUID')
				) AND 
				DATE_FORMAT(ComposeTime,'%Y-%m-%d %H:%i:%s') = '$ComposeDatetime' ";
	$AttachList = $ldb->returnArray($sql,6);

    $x = '';
    $x .= 'var ObjAttachedDisplay = document.getElementById("AttachmentList");'."\n";
    $x .= 'var AttachmentList = "";'."\n";
    $x .= 'AttachmentList = "<ul>";'."\n";

    for ($i=0; $i<sizeof($AttachList); $i++)
    {
        $FileSize = (round(filesize($personal_path."/".$AttachList[$i][5])/1024,2));

        $AttachDisplay  = '<input type="checkbox" id="FileID[]" name="FileID[]" value="'.$AttachList[$i][0].'" checked style="visiblity:hidden; display:none;">';
        $AttachDisplay .= '<a class="tabletool" href="view_compose_attachment.php?FileID='.$AttachList[$i][0].'">';
        $AttachDisplay .= str_replace("'","\'",$AttachList[$i][4]).'('.$FileSize.'K)';
        $AttachDisplay .= '</a>';
        $AttachDisplay .= '[<a class="tabletool" href="javascript:void(0);" onclick="FileDeleteSubmit(\\\''.$AttachList[$i][0].'\\\')">'.$Lang['Gamma']['RemoveAttachment'].'</a>]';
        $AttachDisplay .= '<input type="hidden" id="FileSize[]" name="FileSize[]" value="'.$FileSize.'">';
        $AttachDisplay .= '<br>';
        $x .= 'AttachmentList += \''.$AttachDisplay.'\';'."\n";
    }

    $x .= 'AttachmentList += "</ul>";'."\n";
    $x .= 'ObjAttachedDisplay.innerHTML = AttachmentList;'."\n";
    $x .= 'Check_Total_Filesize(1);'."\n";	// only check the total file size of newly added attachement
}

$x .= 'if(bindClickEventToElements){bindClickEventToElements();}'."\n";
echo $x;

################################################################################################
intranet_closedb();	
?>