<?php
// editing by 
/*************** Change Log ***************
 * Created on 2011-04-18
 ******************************************/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
//include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$linterface = new interface_html();
$IMap = new imap_gamma();

$CurrentPageArr['iMail'] = 1;


$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();

# tag information
$iMailTitle1 = "<span class='imailpagetitle'>".$Lang['iMail']['FieldTitle']['Archive']."&nbsp;".$i_CampusMail_New_Inbox."</span>";
$TAGS_OBJ[] = array("<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$iMailImage1.$iMailTitle1."<td align='right' style=\"vertical-align: bottom;\" >".$IMap->TitleToolBar()."</td></tr></table>", "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

//$IMap->Start_Timer();
session_write_close();

$Folder = trim(stripslashes(urldecode($_REQUEST['Folder'])));
$Folder = (isset($Folder) && $Folder != "") ? $Folder : $IMap->InboxFolder;
$imap_reopen_success = imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));
if(!$imap_reopen_success){
	// Delay 1 second and connect again
	sleep(1);
	imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));
}

$IMap_obj = imap_check($IMap->inbox);
$result = imap_fetch_overview($IMap->inbox,"1:{$IMap_obj->Nmsgs}",0);
$result_size = sizeof($result);

$folder_count=array();

if($result_size>0)
{
	foreach($result as $overview){
		$mail_date = $overview->date;
		//$formatted_date = date("Y-m-d",strtotime($IMap->formatDatetimeString($mail_date)));
		//$year = substr($formatted_date,0,4);
		$year = date("Y",strtotime($IMap->formatDatetimeString($mail_date)));
		$folder_count[$year] += 1;
	}
}

$data_rows = "";
$totalOfMails = 0;
if(sizeof($folder_count)>0)
{
foreach($folder_count as $FolderYear => $NumOfMails)
{
	$css = ($i%2) ? "" : "2";
	$totalOfMails += $NumOfMails;
	$data_rows .= "<tr>\n
					<td class=\"tableContent$css\" align=\"center\">$FolderYear</td>\n
					<td class=\"tableContent$css\" align=\"center\">$NumOfMails</td>\n
					<td class=\"tableContent$css\" align=\"center\">".$linterface->Get_Checkbox("archive$i","archive[]",$FolderYear,0)."</td>\n
				  </tr>\n";
}
}
$data_rows .= "<tr class=\"total_row\">\n
				<th style=\"text-align:right\">".$Lang['General']['Total']."</th>
				<td style=\"text-align:center\">$totalOfMails</td>
				<td>&nbsp;</td>
			   </tr>\n";
# End

//debug_pr($IMap->Stop_Timer());
// 10392 mails , takes 41.29483 seconds to load
?>
<script>
function jsCheckSubmit()
{
	var numOfChecked = $('input[name=archive\\[\\]]:checked');
	if(numOfChecked.length==0){
		alert('<?=$Lang['iMail']['JSWarning']['RequestAtLeastOneYearToArchive']?>');
		return false;
	}
	document.form1.submit();
	$('input#submit_btn').attr('disabled',true);
}
</script>
<br />
<form name="form1" id="form1" method="POST" action="archive_mail_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">	
	<tr>
		<td width="100%" >
			<br />
			<table class="common_table_list_v30">
				<thead class="tabletop">
					<tr>
						<th style="text-align:center" width="50%"><?=$Lang['iMail']['FieldTitle']['Year']?></th>
						<th style="text-align:center" width="25%"><?=$Lang['iMail']['FieldTitle']['NumberOfEmails']?></th>
						<th style="text-align:center" width="25%"><?=$Lang['iMail']['FieldTitle']['Archive']?></th>
					</tr>
				</thead>
				<tbody>
					<?=$data_rows?>
				</tbody>
			</table>
			<div class="edit_bottom_v30">
                <p class="spacer"></p>
                <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","jsCheckSubmit();","submit_btn")?>
                <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location='./index.php';","cancel_btn")?>
                <p class="spacer"></p>
            </div>
		</td>
	</tr>
	</table>
	<br />
	</td>
</tr>
</table>
<input type="hidden" id="Folder" name="Folder" value="<?=urlencode($Folder)?>" />
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>