<?php
// page modifing by: 
$PathRelative = "../../../../";
$CurSubFunction = "Communication";
$CurPage = $_SERVER["REQUEST_URI"];

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}
AuthPage(array("Communication-Mail-GeneralUsage"));
include_once($PathRelative."src/include/class/imap_gamma.php");

#####################################################################################

## Use Library
$IMap = new imap_gamma();


## Get Data 
$MessageID = (isset($MessageID) && $MessageID != "") ? $MessageID : "";	// fetch info 
$Folder    = (isset($Folder) && $Folder != "") ? stripslashes(urldecode($Folder)) : "";		// Get Folder Select
$AttachNum = (isset($AttachNum) && $AttachNum != "") ? $AttachNum : "";	// Get Attachment Number


## Preparation
# Get the Email for display
$EmailContent = $IMap->getMailRecord($Folder, $MessageID);

# Get Attachment
$Attachment = $EmailContent[0]["attach_parts"][$AttachNum];


## Main
function octets2char( $input ) {
	if(!function_exists("octets2char_callback")){
         function octets2char_callback($octets) {
			   $dec = $octets[1];
			   if ($dec < 128) {
				 $utf8Substring = chr($dec);
			   } else if ($dec < 2048) {
				 $utf8Substring = chr(192 + (($dec - ($dec % 64)) / 64));
				 $utf8Substring .= chr(128 + ($dec % 64));
			   } else {
				 $utf8Substring = chr(224 + (($dec - ($dec % 4096)) / 4096));
				 $utf8Substring .= chr(128 + ((($dec % 4096) - ($dec % 64)) / 64));
				 $utf8Substring .= chr(128 + ($dec % 64));
			   }
			   return $utf8Substring;
        }
    }
  return preg_replace_callback('|&#([0-9]{1,});|', 'octets2char_callback', $input );                                
  
}

function utf8Encode2($source) {
   $utf8Str = '';
   $entityArray = explode ("&#", $source);
   $size = count ($entityArray);
   for ($i = 0; $i < $size; $i++) {
       $subStr = $entityArray[$i];
       $nonEntity = strpos ($subStr, ';');
       if ($nonEntity !== false) {
			$str = substr($subStr,0,$nonEntity);
			$str = "&#".$str.";";
			$str_end=substr($subStr,$nonEntity+1);

			$convertedStr=rawurlencode(octets2char($str));
			//$convertedStr=octets2char($str);
			
			$utf8Str.=$convertedStr.$str_end;
       }
       else {
           $utf8Str .= $subStr;
       }
   }
   //return preg_replace('/[:\\x5c\\/*?"<>|]/', '_', $utf8Str);
   return $utf8Str;

}


# convert octets to represented chars in UTF-8, see above utf8Encode2()
//$filename = utf8Encode2($Attachment['filename']); 
//$Content = octets2char($Attachment['content']);

$filename = $Attachment['filename']; 
$Content = $Attachment['content'];

# Output 
Output_To_Browser($Content, $filename, $Attachment['type']."/".$Attachment['subtype']);
#####################################################################################

?>