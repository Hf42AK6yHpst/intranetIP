<?php
// editing by 
/******************** Changes ***********************
 * 2014-06-16 (Carlos): modified force using date range to do search
 * 2011-06-20 (Carlos): added temporary sync mail to db button for testing purpose
 ****************************************************/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();
auth_campusmail();

$IMap = new imap_gamma();
$linterface = new interface_html();

$CurrentPageArr['iMail'] = 1;

//if (!$imail_feature_allowed['mail_search'])
//{
//     header("Location: index.php");
//     exit();
//}
//
//$lwebmail = new libwebmail();
//if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) && $lwebmail->type == 3)
//{
//    include_once("../../includes/libsystemaccess.php");
//    $lsysaccess = new libsystemaccess($UserID);
//    if ($lsysaccess->hasMailRight())
//    {
//        $noWebmail = false;
//    }
//    else
//    {
//        $noWebmail = true;
//    }
//}
//else
//{
//    $noWebmail = true;
//}
//
//
//$ldb = new libdb();
//$folder_sql = "SELECT FolderID, FolderName FROM INTRANET_CAMPUSMAIL_FOLDER WHERE OwnerID = $UserID OR RecordType = 0 ORDER BY RecordType, FolderName";
//$folders = $ldb->returnArray($folder_sql,2);
//$folders[] = array(-1,$i_admintitle_im_campusmail_trash);
//$select_folder = getSelectByArray($folders," name='searchFolder' ",$searchFolder,1);
//
//
//# Find unread mails number
///*
//$sql  = "
//			SELECT
//				count(a.CampusMailID)
//          	FROM 
//          		INTRANET_CAMPUSMAIL AS a LEFT OUTER JOIN INTRANET_USER AS b
//          	ON 
//          		a.SenderID = b.UserID
//			WHERE
//               a.UserID = $UserID AND
//               a.UserFolderID = 2 AND
//               a.Deleted != 1 AND	               	               
//               (a.RecordStatus = '' OR a.RecordStatus IS NULL)
//          ";
//$row = $ldb->returnVector($sql);	          
//$unreadInboxNo = $row[0];
//*/

# folder seletion
$select_folder = $IMap->getFolderMultiSelection("SearchFolder","SearchFolder[]", $SearchFolder);

$date_range_ary = array();
$date_range_ary[] = array('1week',$Lang['Gamma']['OneWeek']);
$date_range_ary[] = array('1month',$Lang['Gamma']['OneMonth']);
$date_range_ary[] = array('3month',$Lang['Gamma']['ThreeMonths']);
$date_range_ary[] = array('6month',$Lang['Gamma']['SixMonths']);
$date_range_ary[] = array('1year',$Lang['Gamma']['OneYear']);
$date_range_ary[] = array('custom',$Lang['Gamma']['CustomDateRange']);

$DateRangeMethod = isset($DateRangeMethod) && in_array($DateRangeMethod,array('1week','1month','3month','6month','1year','custom'))? $DateRangeMethod : '3month'; 
$date_range_selection = $linterface->GET_SELECTION_BOX($date_range_ary,' id="DateRangeMethod" name="DateRangeMethod" onchange="onDateRangeMethodChanged(this);" ','',$DateRangeMethod);

$ts_today = time();
$ToDate = isset($ToDate) && $ToDate!="" ? $ToDate : date("Y-m-d",$ts_today);
$FromDate = isset($FromDate) && $FromDate != "" ? $FromDate : date("Y-m-d",strtotime("-1 month",$ts_today));
$FromDateInput = $linterface->GET_DATE_PICKER("StartDate",$FromDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $DateRangeMethod != 'custom', $cssClass="textboxnum");
$ToDateInput = $linterface->GET_DATE_PICKER("EndDate",$ToDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $DateRangeMethod != 'custom', $cssClass="textboxnum");


$CurrentPage = "PageSearch";
$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();

# tag information
$iMailTitle1 = "<span class='imailpagetitle'>{$i_CampusMail_New_Mail_Search}</span>";
$iMailTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$iMailTitle1."</td><td align='right' style=\"vertical-align: bottom;\" >".$IMap->TitleToolBar()."</td></tr></table>";
$TAGS_OBJ[] = array($iMailTitle, "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

//$MailTypeSelect0 = ($MailType==0 || $MailType=="") ? " selected='selected' " : "";
//$MailTypeSelect1 = ($MailType==1) ? " selected='selected' " : "";
//$MailTypeSelect2 = ($MailType==2) ? " selected='selected' " : "";
//
//$isAttachmentCheck= ($isAttachment==1) ? " checked='checked' " : ""; 
//
//$search_subject = stripslashes($search_subject);
//$search_subject = str_replace("\"","&quot;",$search_subject);
//$search_message = stripslashes($search_message);
//$search_message = str_replace("\"","&quot;",$search_message);
//$search_recipient = stripslashes($search_recipient);
//$search_recipient = str_replace("\"","&quot;",$search_recipient);
//$search_sender = stripslashes($search_sender);
//$search_sender = str_replace("\"","&quot;",$search_sender);


?>

<script type="text/javascript" language="JavaScript">
<!--

      // Calendar callback. When a date is clicked on the calendar
      // this function is called so you can do as you want with it
function calendarCallback(date, month, year)
{
   if (String(month).length == 1) {

       month = '0' + month;
   }

   if (String(date).length == 1) {
       date = '0' + date;
   }
   dateValue =year + '-' + month + '-' + date;
   document.forms['form1'].FromDate.value = dateValue;
}

function calendarCallback2(date, month, year)
{
   if (String(month).length == 1) {
       month = '0' + month;
   }

   if (String(date).length == 1) {
       date = '0' + date;
   }

   dateValue =year + '-' + month + '-' + date;
   document.forms['form1'].ToDate.value = dateValue;
}
 
function SelectAll(ObjID)
{
	$("#"+ObjID+" option").attr("selected","selected");
}
 
function onDateRangeMethodChanged(obj)
{
 	var selected_value = $(obj).val();
 	if(selected_value == 'custom'){
 		$('#StartDate').attr('disabled',false);
 		$('#StartDate').next('img').show();
 		$('#EndDate').attr('disabled',false);
 		$('#EndDate').next('img').show();
 	}else{
 		$('#StartDate').attr('disabled',true);
 		$('#StartDate').next('img').hide();
 		$('#EndDate').attr('disabled',true);
 		$('#EndDate').next('img').hide();
 		
 		var dateObj = new Date();
 		var end_date = getFormatDate(dateObj);
 		var start_date = end_date;
 		
 		if(selected_value == '1week'){
 			dateObj.setDate(dateObj.getDate()-7);
 		}else if(selected_value == '1month'){
 			dateObj.setMonth(dateObj.getMonth()-1);
 		}else if(selected_value == '3month'){
 			dateObj.setMonth(dateObj.getMonth()-3);
 		}else if(selected_value == '6month'){
 			dateObj.setMonth(dateObj.getMonth()-6);
 		}else if(selected_value == '1year'){
 			dateObj.setFullYear(dateObj.getFullYear()-1);
 		}
 		start_date = getFormatDate(dateObj);
 		
 		$('#StartDate').val(start_date);
 		$('#EndDate').val(end_date);
 	}
}

function getFormatDate(dateObj)
{
	var date_year = dateObj.getFullYear();
	var date_month = dateObj.getMonth()+1;
	var date_day = dateObj.getDate();
	date_month = date_month < 10? '0'+date_month : ''+date_month; 
	date_day = date_day < 10 ? '0'+date_day : ''+date_day; 
	var date_string = date_year + '-' + date_month + '-' + date_day;
	
	return date_string;
}

function submitForm(formObj)
{
	var from_date_obj = document.getElementById('StartDate');
	var to_date_obj = document.getElementById('EndDate');
	var is_valid = true;
	
	if(!check_date_without_return_msg(from_date_obj)){
		is_valid = false;
	}
	if(!check_date_without_return_msg(to_date_obj)){
		is_valid = false;
	}
	
	if(is_valid){
		if(from_date_obj.value > to_date_obj.value){
			is_valid = false;
			alert('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
			return;
		}
		
	}
	
	var search_folder_obj = $('#SearchFolder');
	if(!search_folder_obj.val() || search_folder_obj.val().length == 0){
		is_valid = false;
		alert('<?=$Lang['Gamma']['WarningRequestSelectFolder']?>');
		return;
	}
	
	if(is_valid){
		$('#FromDate').val(from_date_obj.value);
		$('#ToDate').val(to_date_obj.value);
		formObj.submit();
	}
}
// -->
<?php /*
if($IMap->CacheMailToDB == true)
{
?>
	function FullSyncMails()
	{
		Block_Document('Synchronizing mails, please wait patiently...');
		$.get(
			"ajax_cache_mail.php",
			{
				Action:"FullSyncMails"
			},
			function(data){
				UnBlock_Document();
			}
		);
	}
<?php
} */
?>     

$(document).ready(function(){
	setTimeout(function(){
		onDateRangeMethodChanged(document.getElementById('DateRangeMethod'));
	},100);
	if(<?=count($SearchFolder)?>==0){
		//SelectAll("SearchFolder");
		document.getElementById('SearchFolder').selectedIndex = 0;
	}
});
 </script>
     
<form name="form1" method="get" action="search_result.php" onsubmit="submitForm(this);return false;">
<?=$xmsg?>
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?php
/*	if($IMap->CacheMailToDB == true)
	{
	?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Synchronize mails to database</td>
		<td valign="top"><?=$linterface->GET_SMALL_BTN("Proceed", "", "FullSyncMails();", "SyncBtn", "style='text-align:center;'")?><span style='color:gray;'> (This process may takes a few minutes, please wait patiently.)</span></td>
	</tr>
	<?php
	}*/
	?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_frontpage_campusmail_recipients?></td>
		<td valign="top">
		<input type="text" name="KeywordTo" size="50" class="textboxtext" value="<?=$KeywordTo?>" />
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_frontpage_campusmail_sender?></td>
		<td valign="top">
		<input type="text" name="KeywordFrom" size="50" class="textboxtext" value="<?=$KeywordFrom?>" />
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['Gamma']['cc']?></td>
		<td valign="top">
		<input type="text" name="KeywordCc" size="50" class="textboxtext" value="<?=$KeywordCc?>" />
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['Gamma']['bcc']?></td>
		<td valign="top">
		<input type="text" name="KeywordBcc" size="50" class="textboxtext" value="<?=$KeywordBcc?>" />
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['Gamma']['Subject']?></td>
		<td valign="top">
		<input type="text" name="KeywordSubject" size="50" class="textboxtext" value="<?=$KeywordSubject?>" />
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_frontpage_campusmail_message?></td>
		<td valign="top">
		<input type="text" name="KeywordBody" size="50" class="textboxtext" value="<?=$KeywordBody?>" />
		</td>
	</tr>
	<?php 
	/*if (!$noWebmail) 
	{			 
	?>
		<tr>
          	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_CampusMail_New_MailSource?></td>
          	<td width="70%">
            <select name="MailType">
          	<option value="0" <?=$MailTypeSelect0?> > -- <?=$i_status_all?> -- </option>
          	<option value="1" <?=$MailTypeSelect1?> ><?=$i_CampusMail_New_MailSource_Internal?></option>
          	<option value="2" <?=$MailTypeSelect2?> ><?=$i_CampusMail_New_MailSource_External?></option>
            </select>
          	</td>
		</tr>
	<?php 
	} */
	?>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_frontpage_campusmail_attachment?></td>
			<td valign="top">
				<input type="text" name="KeywordAttachment" size="50" class="textboxtext" value="<?=$KeywordAttachment?>" />
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_record_date?></td>
			<td valign="top" class="tabletext">
			<?=$date_range_selection.$Lang['General']['From'].'&nbsp;'.$FromDateInput.$Lang['General']['To'].'&nbsp;'.$ToDateInput?>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_CampusMail_New_MailSearch_SearchFolder?></td>
			<td valign="top" width="70%"><?=$select_folder?></td>
		</tr>
		
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?= $linterface->GET_ACTION_BTN($button_quicksearch, "submit", "") ?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="isAdvanceSearch" value="1" />
<input type="hidden" name="FromSearch" value="1" />
<input type="hidden" id="FromDate" name="FromDate" value="" />
<input type="hidden" id="ToDate" name="ToDate" value="" />
</form>

<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>