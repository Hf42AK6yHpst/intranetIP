<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$anc = $special_option['no_anchor'] ?"":"#anc";

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$IMap = new imap_gamma();
$lwebmail = new libwebmail();

//if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
//{
//    include_once($PATH_WRT_ROOT."includes/libsystemaccess.php");
//    $lsysaccess = new libsystemaccess($UserID);
//    if ($lsysaccess->hasMailRight())
//    {
//        $noWebmail = false;
//    }
//    else
//    {
//        $noWebmail = true;
//    }
//}
//else
//{
//    $noWebmail = true;
//}

$AdressBookArr = $IMap -> getAddressBookDetail($UserID);
for($i=0; $i<sizeof($AdressBookArr); $i++)
{
	$ExportArr[] = array($AdressBookArr[$i][1], $AdressBookArr[$i][2]);
}
 
$exportColumn =  array('UserName','Email Address');

$lexport = new libexporttext();
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn);
//debug_pr($export_content);
$filename = "addressbook.csv";
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>