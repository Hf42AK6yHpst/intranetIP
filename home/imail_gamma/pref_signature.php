<?php
// editing by 
/***************************** Modification Log ***********************************
 * 2017-07-13 [Carlos] : $sys_custom['iMailPlus']['CKEditor'] Changed to use new CKEditor.
 * 2010-12-09 [Carlos] : Changed to use Email as ID to get/set preference
 **********************************************************************************/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
#include_once("../../includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

if ( $UserID=="")
{
    header("Location: index.php");
    exit();
}

if ($special_feature['imail_richtext'])
{
    $use_html_editor = true;
}
if($TabID=="") $TabID=2;
$li = new libdb();
//$sql ="SELECT Signature FROM INTRANET_IMAIL_PREFERENCE WHERE USerID='$UserID'";
$sql = "SELECT Signature FROM MAIL_PREFERENCE WHERE MailBoxName = '".$_SESSION['SSV_LOGIN_EMAIL']."' ";
$result = $li->returnVector($sql);

$signature = $result[0];

if(!$use_html_editor)
{
	$signature = str_replace("\n","\n>",$signature);
	$signature = intranet_htmlspecialchars($signature);
	$signature = str_replace("&amp;","&",$signature);
}
$navigation = $i_frontpage_separator.$i_frontpage_schoolinfo.$i_frontpage_separator.$i_frontpage_campusmail_campusmail;

$lwebmail = new libwebmail();
$IMap = new imap_gamma();

$CurrentPage = "PageSettings_Signature";
$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array("<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$i_CampusMail_New_Settings_Signature."<td align='right' style=\"vertical-align: bottom;\" >".$IMap->TitleToolBar()."</td></tr></table>", "", 0);


$linterface = new interface_html();
$linterface->LAYOUT_START();

if ($msg == "2")
{
	$xmsg = $linterface->GET_SYS_MSG("update");
} else {
	$xmsg = "&nbsp;";
}

?>
<SCRIPT LANGUAGE='JAVASCRIPT'>
function resetForm(obj)
{
	location.href='pref_signature.php?TabID=<?=$TabID?>';
}
</SCRIPT>
<form name="form1" method="post" action="pref_update.php" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">	
	<tr>
		<td colspan="2" align="right" ><?=$xmsg?></td>
	</tr>       	
	<tr>
		<td align="left" colspan="2" class="tabletext" ><?=$i_CampusMail_New_Settings_PleaseFillInYourSignature?></td>
	</tr>
	<tr>
		<td align="left" colspan="2" >
		<?php 
		if ($use_html_editor) 
		{
			# Components size
			$msg_box_width = "100%";
			$msg_box_height = 250;
			$obj_name = "signature";
			$editor_width = $msg_box_width;
			$editor_height = $msg_box_height;
			$signature = str_replace(array('&lt;','&gt;'),array('&amp;lt;','&amp;gt;'),$signature);
			//$init_html_content = $signature;
			//include($PATH_WRT_ROOT."includes/html_editor_embed2.php");
			if($sys_custom['iMailPlus']['CKEditor']){
				echo getCkEditor("signature", str_replace(array('&lt;','&gt;'),array('&amp;lt;','&amp;gt;'),$signature), "default",$editor_height, $editor_width);
			}else{
				include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
				$html_editor = new FCKeditor('signature') ;
				$html_editor->BasePath = $PATH_WRT_ROOT.'templates/html_editor/';
				$html_editor->Config['SkinPath'] = 'skins/silver/' ;
				$html_editor->ToolbarSet = 'Basic2';
				$html_editor->Value = $signature;
				$html_editor->Create() ;
			}
		} else { 
		?>
			<textarea rows="10" cols="50" name="signature" ><?=$signature?></textarea>
		<?php
		}
		?>	
	</table>   	      
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "button", "resetForm(document.form1)") ?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="TabID" value="<?=$TabID?>" />
</form>
<?php 
	if (!$use_html_editor) 
	{
		echo $linterface->FOCUS_ON_LOAD("form1.signature");
	}	
	
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>