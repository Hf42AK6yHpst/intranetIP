<?php
## Using By : 
################ Change Log [Start] #####################
#	2015-09-17 (Carlos): Use flag $sys_custom['iMailPlus']['DisableAutoForward'] to control availability of auto forward.
#	2012-02-08 (Carlos): validate input email address by onsubmit event
#	2012-08-20 (Carlos): modified email forward field to textarea
#	Date	:	2010-11-18 [Yuen]
#	Details :	added indent to the setting which depend on the enable/disable of forwarding
################ Change Log [End] #####################


$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

if($sys_custom['iMailPlus']['DisableAutoForward']){
	header("Location: index.php");
	exit();
}

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

$lwebmail = new libwebmail();
$IMap = new imap_gamma();
//if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
//{
//    include_once("../../includes/libsystemaccess.php");
//    $lsysaccess = new libsystemaccess($UserID);
//    if ($lsysaccess->hasMailRight())
//    {
//        $noWebmail = false;
//    }
//    else
//    {
//        $noWebmail = true;
//    }
//}
//else
//{
//    $noWebmail = true;
//}
if($TabID=="") $TabID=3;

#$noWebmail = false;

# Block No webmail no preference
//if ($noWebmail || $UserID=="")
//{
//    header("Location: index.php");
//    exit();
//}

$pref = $IMap->Get_Preference();
$forwarded_email = $pref["ForwardedEmail"];
$keep_copy = $pref["ForwardKeepCopy"];

$disabled_forward=$forwarded_email==""?true:false;
$forwarded_email = str_replace(",","\n",$forwarded_email);
//$navigation = $i_frontpage_separator.$i_frontpage_schoolinfo.$i_frontpage_separator.$i_frontpage_campusmail_campusmail;

$CurrentPage = "PageSettings_EmailForwarding";
$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array("<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$i_CampusMail_New_Settings_EmailForwarding."<td align='right' style=\"vertical-align: bottom;\" >".$IMap->TitleToolBar()."</td></tr></table>", "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

if ($msg == "2")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("update")."</td></tr>";
} else {
	$xmsg = "";
}

?>
<script language='javascript'>
function setForwarding(){
	objForwarding = document.form1.forwarding_emails;
	objKeepCopy = document.form1.keep_copy;
	if(objForwarding!=null)
		objForwarding.disabled = !objForwarding.disabled;
	if(objKeepCopy!=null){
		objKeepCopy.disabled = !objKeepCopy.disabled;
	}
}
function resetForm(obj){
	if(obj==null) return;
	objDisable = obj.disable_email_forwarding;
	objForwarding = obj.forwarding_emails;
	objKeepCopy = obj.keep_copy;

	if(objDisable==null || objForwarding==null || objKeepCopy==null) return;

	obj.reset();
	disable = objDisable.checked;
	objForwarding.disabled=disable;
	objKeepCopy.disabled=disable;
}
function checkform(obj){
	if(obj==null) return false;
	objDisable = obj.disable_email_forwarding;
	if(objDisable!=null && objDisable.checked) return true;
	objForwarding = obj.forwarding_emails;
	if(objForwarding==null) return false;
	ary = objForwarding.value.split("\n");
	
	// var re = /^.+@.+\..{2,3}$/;
    var re = /^([0-9A-Za-z_\.\-])+\@(([0-9A-Za-z\-])+\.)+([0-9A-Za-z]{2,3})+$/;
    

	for(i=0;i<ary.length;i++){
		if(Trim(ary[i])=="") continue;
        if (!re.test(Trim(ary[i]))) {
	        alert('<?=$i_invalid_email?>: '+ary[i]);
                return false;
        }
	}
	return true;
}
function submitForm(obj){
	if(checkform(obj))
		obj.submit();
}
</script>
<form name=form1 method="post" action="pref_update.php" onsubmit="submitForm(document.form1);return false;">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">	
	<?=$xmsg?>
	<tr>
		<td align="right" >
		<table border="0" cellpadding="5" cellspacing="1" align="center" width="90%" class="tabletext" >
		<tr>
			<td></td>
			<td colspan="2">
			<input type="checkbox" name="disable_email_forwarding" id="disable_email_forwarding" value="1" onClick='setForwarding()' <?php if($disabled_forward) echo "CHECKED";?> />
			<label for="disable_email_forwarding" ><?=$i_CampusMail_New_Settings_DisableEmailForwarding?></label>
			</td>
		</tr>		
		<tr>
			<td width="100" ></td>
			<td colspan="2">&nbsp; &nbsp; &nbsp; <?=str_replace(array(":","<br>"),array("","<br>&nbsp; &nbsp; &nbsp; "),$i_CampusMail_New_Settings_PleaseFillInForwardEmail)?></td>
			<!--<td colspan="2" >&nbsp; &nbsp; &nbsp; <?=$Lang['Gamma']['FillForwardingEmail']?></td>-->
		</tr>
		<tr>
			<td></td>
			<!--<td align="left" colspan="2" >&nbsp; &nbsp; &nbsp; <input type="text" size=50 name="forwarding_emails" <?=($disabled_forward?"disabled=true":"")?> value="<?=$forwarded_email?>"></td>-->
			<td align="left" colspan="2" >&nbsp; &nbsp; &nbsp; <textarea cols=50 rows=10 name="forwarding_emails" <?=($disabled_forward?"disabled=true":"")?>><?=$forwarded_email?></textarea></td>
		</tr>
		<tr>
			<td></td>
			<td colspan="2" >
			<input type="checkbox"  name="keep_copy" id="keep_copy" value='1' <?php echo $keep_copy=="1"?"CHECKED":"";?><?=($disabled_forward?" disabled=true ":"")?> />			
			<label for="keep_copy" ><?=$i_CampusMail_New_Settings_PleaseFillInForwardKeepCopy?></label>
			</td>
		</tr>		
		</table>		
		</td>
	</tr>
	</table>
</tr>	
<tr>
	<td>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "button", "resetForm(document.form1)") ?>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="TabID" value=<?=$TabID?> />
</form>

<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>