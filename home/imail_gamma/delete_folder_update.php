<?php
// page modifing by : 
$PathRelative = "../../../../";
$CurSubFunction = "Communication";

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}

AuthPage(array("Communication-Mail-GeneralUsage"));

include_once($PathRelative."src/include/class/imap_gamma.php");
#########################################################################################################

## Use Library
$IMap = new imap_gamma();


## Get Data
$CurFolder = (isset($CurFolder) && $CurFolder != "") ? stripslashes(urldecode($CurFolder)) : "";


## Main
if($CurFolder != ""){
	$Result = $IMap->Delete_Mail_Folder($CurFolder);
}


if ($Result) {
	$Msg = $Lang['ReturnMsg']['DeleteFolderSuccess'];
} else {
	$Msg = $Lang['ReturnMsg']['DeleteFolderUnsuccess'];
}

#########################################################################################################
header("Location: manage_folder.php?Msg=".$Msg);
exit;

?>