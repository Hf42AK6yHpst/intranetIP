<?php
// editing by 
/***************************************** Modification Log ******************************************
 * 2018-08-13 (Carlos): Created for move spam mails to EDS_ReportSpam folder for analysis.
 *****************************************************************************************************/
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libaccess.php");
include("../../includes/libwebmail.php");
include("../../includes/imap_gamma.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$IMap = new imap_gamma();

$EDS_report_spam_folder = $IMap->EDSReportSpamFolder;

$Folder = $Folder_b !=""? base64_decode($Folder_b) : $Folder;

if(!$sys_custom['iMailPlus']['EDSReportSpam'] ||  $Folder == $EDS_report_spam_folder){ // avoid moved to same folder
	$url = $_SERVER["HTTP_REFERER"];
	intranet_closedb();
	header("Location: $url");
	exit();
}

/*
if($Folder == $IMap->DefaultFolderList["SpamFolder"])
{
	$ReportFolder = $IMap->DefaultFolderList["reportNonSpamFolder"];
	$targetFolder = $IMap->DefaultFolderList["inbox"];
}
else
{
	$ReportFolder = $IMap->DefaultFolderList["reportSpamFolder"];
	$targetFolder = $IMap->DefaultFolderList["SpamFolder"];
}
*/
$targetFolder = $EDS_report_spam_folder;

$IMap->createMailFolder($SYS_CONFIG['Mail']['FolderPrefix'],'EDS_ReportSpam');

## Get Data
$Uid = (isset($Uid)) ? $Uid : "";		// needs to be array format
if(is_array($Uid)){
	$UIDList = $Uid;
} else {
	if($Uid != ""){ $UIDList[0] = $Uid; }
}

if(count($UIDList) > 0){
	if(sizeof($UIDList)==1 && $page_from=="viewmail.php"){
		if($FromSearch==1){
			list($CachedUID,$CachedFolder,$CachedSearchFields) = $IMap->Get_Cached_Search_Result();
			$this_uid_index = array_search($UIDList[0],$CachedUID);
			$prevID = $CachedUID[$this_uid_index-1];
			$nextID = $CachedUID[$this_uid_index+1];
			$prevFolder = $CachedFolder[$this_uid_index-1];
			$nextFolder = $CachedFolder[$this_uid_index+1];
			$IMap->Set_Cached_Search_Result(array_diff_key($CachedUID,array($this_uid_index=>$CachedUID[$this_uid_index])),
											array_diff_key($CachedFolder,array($this_uid_index=>$CachedFolder[$this_uid_index])),
											$CachedSearchFields);
		}else{
			$PrevNextUID = $IMap->Get_Prev_Next_UID($Folder, $sort, $reverse, $UIDList[0]);
			$prevID = $PrevNextUID["PrevUID"];
			$nextID = $PrevNextUID["NextUID"];
			$prevFolder = $Folder;
			$nextFolder = $Folder;
		}
		if($prevID != ""){ // to prev mail
			$url = "viewmail.php?uid=".$prevID."&Folder=".urlencode($prevFolder)."&sort=".$sort."&reverse=".$reverse."&FromSearch=".$FromSearch."&pageNo=".$pageNo."&field=".$field;
		}else if($nextID != ""){// no prev mail, to next mail
			$url = "viewmail.php?uid=".$nextID."&Folder=".urlencode($nextFolder)."&sort=".$sort."&reverse=".$reverse."&FromSearch=".$FromSearch."&pageNo=".$pageNo."&field=".$field;
		}else{
			$url = "viewfolder.php?Folder=".urlencode($Folder)."&sort=".$sort."&reverse=".$reverse."&FromSearch=".$FromSearch."&pageNo=".$pageNo."&field=".$field;
		}
	}
	
	for ($i=0; $i<sizeof($UIDList) ; $i++) {
		if (stristr($UIDList[$i], ",") !== false) {
			List($UID, $Folder) = explode(",", $UIDList[$i]);
		}
		else {
			$UID = $UIDList[$i];
			//$Folder = $_GET['Folder'];
		}
		//$Result = $IMap->Copy_Mail($Folder, $UID, $ReportFolder);
		$Result = $IMap->Move_Mail($Folder, $UID, $targetFolder);
	}
}

if ($page_from == "viewmail.php"){
	/*
	if($FromSearch==1){
		list($CachedUID,$CachedFolder) = $IMap->Get_Cached_Search_Result();
		$this_uid_index = array_search($UIDList[0],$CachedUID);
		$prevID = $CachedUID[$this_uid_index-1];
		$nextID = $CachedUID[$this_uid_index+1];
		$prevFolder = $CachedFolder[$this_uid_index-1];
		$nextFolder = $CachedFolder[$this_uid_index+1];
		$IMap->Set_Cached_Search_Result(array_diff_key($CachedUID,array($this_uid_index=>$CachedUID[$this_uid_index])),
										array_diff_key($CachedFolder,array($this_uid_index=>$CachedFolder[$this_uid_index])));
	}else{
		$PrevNextUID = $IMap->Get_Prev_Next_UID($Folder, $sort, $reverse, $UIDList[0]);
		$prevID = $PrevNextUID["PrevUID"];
		$nextID = $PrevNextUID["NextUID"];
		$prevFolder = $Folder;
		$nextFolder = $Folder;
	}
	if($nextID != ""){// to next mail
		$url = "viewmail.php?uid=".$nextID."&Folder=".urlencode($nextFolder)."&sort=".$sort."&reverse=".$reverse."&FromSearch=".$FromSearch;
	}else if($prevID != ""){ // no next mail, to prev mail
		$url = "viewmail.php?uid=".$prevID."&Folder=".urlencode($prevFolder)."&sort=".$sort."&reverse=".$reverse."&FromSearch=".$FromSearch;
	}else{
		$url = "viewfolder.php?Folder=".urlencode($Folder);
	}
	*/
}else
	$url = $_SERVER["HTTP_REFERER"];

intranet_closedb();
header("Location: $url");
?>