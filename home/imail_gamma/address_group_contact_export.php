<?php
// editing by 
/*
 * 2016-04-07(Carlos): Added export for internal recipient groups.
 * 2015-07-28(Shan): open this page
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
auth_campusmail();
$ldb = new libdb();

if ($aliastype==1){
	$sql = "select c.AliasName,a.TargetName,a.TargetAddress
			FROM INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL as a
			INNER JOIN INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY as b on b.TargetID = a.AddressID
			INNER JOIN INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL as c on b.AliasID = c.AliasID AND c.OwnerID = '".$_SESSION['UserID']."'			
			order by c.AliasName";
}else{
	$username_field = getNameFieldWithClassNumberByLang("c.");
	$sql = "SELECT a.AliasName as AliasName, 
			$username_field as TargetName,
			c.ImapUserEmail as TargetAddress 
			FROM INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL as a 
			INNER JOIN INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as b ON b.AliasID=a.AliasID AND b.RecordType='U'
			INNER JOIN INTRANET_USER as c ON c.UserID=b.TargetID 
			WHERE a.OwnerID='".$_SESSION['UserID']."' AND b.RecordType='U' AND c.ImapUserEmail IS NOT NULL AND c.ImapUserEmail<>'' 
			UNION 
			SELECT 
			a.AliasName as AliasName, 
			CONCAT('[',g.Title,'] ',$username_field) as TargetName,
			c.ImapUserEmail as TargetAddress 
			FROM INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL as a 
			INNER JOIN INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as b ON b.AliasID=a.AliasID AND b.RecordType='G' 
			INNER JOIN INTRANET_GROUP as g ON g.GroupID=b.TargetID 
			INNER JOIN INTRANET_USERGROUP as ug ON ug.GroupID=g.GroupID 
			INNER JOIN INTRANET_USER as c ON c.UserID=ug.UserID 
			WHERE a.OwnerID='".$_SESSION['UserID']."' AND b.RecordType='G' AND c.ImapUserEmail IS NOT NULL AND c.ImapUserEmail<>'' 
			ORDER BY AliasName,TargetName,TargetAddress";
}
$groupContactArr = $ldb->returnResultSet($sql);

for ($i=0;$i<sizeof($groupContactArr);$i++){	
	$exportArr[]  = array($groupContactArr[$i]['AliasName'], $groupContactArr[$i]['TargetName'],$groupContactArr[$i]['TargetAddress']);
}

$lexport = new libexporttext();
$exportColumn = array('Group Name','User Name','User Email Address');
$export_content = $lexport->GET_EXPORT_TXT($exportArr, $exportColumn);
$filename = "address_groupcontact.csv";
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>