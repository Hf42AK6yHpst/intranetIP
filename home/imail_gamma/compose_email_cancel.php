<?php
// page modifing by : 
$PathRelative = "../../../../";
$CurSubFunction = "Communication";

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}

AuthPage(array("Communication-Mail-GeneralUsage"));

intranet_opendb();

include_once($PathRelative."src/include/class/database.php");
include_once($PathRelative."src/include/class/imap_gamma.php");
include_once($PathRelative."src/include/class/file_system.php");

#############################################################################################

## User Library
$ldb = new database();
$IMap = new imap_gamma();

## Get Data
# Mail Data 
$DraftUID   = (isset($uid) && $uid != "") ? $uid : "";						// Original UID
$Folder	    = (isset($Folder) && $Folder != "") ? stripslashes(urldecode($Folder)) : '';	// Folder
$MailAction = (isset($MailAction) && $MailAction != "") ? $MailAction : '';	// Mail Action: NULL-new mail ; Forward ; Draft 
$MessageUID = $DraftUID;

# Page Info
$FromSearch = (isset($FromSearch) && $FromSearch != "") ? $FromSearch : 0;
$sort 		= (isset($sort) && $sort != "") ? $sort : (($FromSearch == 1) ? "SortDate" : "SORTARRIVAL");
$pageNo     = (isset($pageNo) && $pageNo != "")? $pageNo : 1;
$reverse    = (isset($reverse) && $reverse != "")? $reverse : 1;

if($FromSearch == 1){
	$IsAdvanceSearch = (isset($IsAdvanceSearch) && $IsAdvanceSearch != "") ? $IsAdvanceSearch : 1;
} else {
	$IsAdvanceSearch = '';
}

# Get Data From Simple Search
$keyword 	  = (isset($keyword) && $keyword != "") ? stripslashes(trim($keyword)) : "";

# Get Data From Advance Search
$KeywordFrom 	= (isset($KeywordFrom) && $KeywordFrom != "") ? stripslashes(trim($KeywordFrom)) : '';
$KeywordTo   	= (isset($KeywordTo) && $KeywordTo != "") ? stripslashes(trim($KeywordTo)) : '';
$KeywordCc   	= (isset($KeywordCc) && $KeywordCc != "") ? stripslashes(trim($KeywordCc)) : '';
$KeywordSubject = (isset($KeywordSubject) && $KeywordSubject != "") ? stripslashes(trim($KeywordSubject)) : '';
$FromDate  		= (isset($FromDate) && $FromDate != "") ? trim($FromDate) : '';
$ToDate  		= (isset($ToDate) && $ToDate != "") ? trim($ToDate) : '';
$SearchFolder   = (isset($SearchFolder) && is_array($SearchFolder) && count($SearchFolder) > 0) ? $SearchFolder : array();


## Initialization
$DeleteFileArr = array();


## Main
# Remove the files which are NEWLY uploaded in New Compose Mail, Compose Forward, Reply, ReplyALL and Draft Mail
$IMap->Remove_Unlink_Attach_File();

if ($MailAction == "" && $DraftUID != "") {
	# Save the record of draft mail
	$sql = 'UPDATE MAIL_ATTACH_FILE_MAP 
		SET 
			DraftMailUID = '.$MessageUID.' 
		WHERE 
			DraftMailUID = -1 AND 
			UserID = '.$_SESSION['SSV_USERID'];
	$ldb->db_db_query($sql);
} else {
	# Remove the files which are followed by an Forward Mail 
	if($MailAction == "Forward"){
		$IMap->Remove_Unlink_Attach_File($MessageUID, $MailAction);
	}
	
}

#############################################################################################
intranet_closedb();

if($FromSearch == 1){
	$redirect_url = ($IsAdvanceSearch == 0) ? "search_result.php" : "advance_search_result.php";
	
	# Page from Advance Search
	echo '<html><body>';
	echo $lui->Get_Form_Open("form1", "post", $redirect_url);
	echo $lui->Get_Input_Hidden("CurTag", "CurTag", urlencode($Folder))."\n";
	echo $lui->Get_Input_Hidden("Folder", "Folder", urlencode($Folder))."\n";
	echo $lui->Get_Input_Hidden("reverse", "reverse", $reverse)."\n";
	echo $lui->Get_Input_Hidden("sort", "sort", $sort)."\n";
	echo $lui->Get_Input_Hidden("pageNo", "pageNo", $pageNo)."\n";
	echo $lui->Get_Input_Hidden("FromSearch", "FromSearch", $FromSearch)."\n";
	echo $lui->Get_Input_Hidden("IsAdvanceSearch", "IsAdvanceSearch", $IsAdvanceSearch)."\n";

	# Hidden Values - Simple Search
	echo $lui->Get_Input_Hidden("keyword", "keyword", $keyword)."\n";
	
	# Hidden Values - Advance Search Field
	echo $lui->Get_Input_Hidden("KeywordFrom", "KeywordFrom", $KeywordFrom)."\n";
	echo $lui->Get_Input_Hidden("KeywordTo", "KeywordTo", $KeywordTo)."\n";
	echo $lui->Get_Input_Hidden("KeywordCc", "KeywordCc", $KeywordCc)."\n";
	echo $lui->Get_Input_Hidden("KeywordSubject", "KeywordSubject", $KeywordSubject)."\n";
	echo $lui->Get_Input_Hidden("FromDate", "FromDate", $FromDate)."\n";
	echo $lui->Get_Input_Hidden("ToDate", "ToDate", $ToDate)."\n";
	if(count($SearchFolder) > 0){
		for ($i=0; $i < sizeof($SearchFolder); $i++) {
			echo $lui->Get_Input_Hidden("SearchFolder[]", "SearchFolder[]", stripslashes($SearchFolder[$i]))."\n";
		}
	}
	echo $lui->Get_Form_Close();
	echo '<script type="text/javascript">';
	echo "function submit_form(){ document.form1.submit(); }";
	echo "submit_form();";
	echo "</script>";
	echo '</body></html>';
} else {
	# Page not from Search
	$params  = "?CurTag=".urlencode($Folder)."&Folder=".urlencode($Folder);
	$params .= "&pageNo=$pageNo&sort=$sort&reverse=$reverse";
	header("Location: imail_gamma.php".$params);
	exit;
}
?>