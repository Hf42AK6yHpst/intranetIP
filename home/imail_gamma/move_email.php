<?php
// editing by 
/****************************************** Modification Log *************************************************
 * 2012-08-30 [Carlos] : Changed after moved mail, first attempt is go to previous mail, then next mail
 * 2011-10-28 (Carlos) : Fixed go back to current viewfolder page
 * 2011-03-10 (Carlos) : Fixed go to previous/next mail
 * 2011-01-25 (Carlos) : Added go to previous/next mail function if at view mail page
 *************************************************************************************************************/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$IMap = new imap_gamma();

## Get Data
$Uid = (isset($Uid)) ? $Uid : (isset($UID)?$UID:'');		
if(is_array($Uid)){
	$UIDList = $Uid;
} else {
	if($Uid != ""){ $UIDList[0] = $Uid; }
}

if(count($UIDList) > 0){
	if(sizeof($UIDList)==1 && $page_from=="viewmail.php"){
		if($FromSearch==1){
			list($CachedUID,$CachedFolder,$CachedSearchFields) = $IMap->Get_Cached_Search_Result();
			$this_uid_index = array_search($UIDList[0],$CachedUID);
			$prevID = $CachedUID[$this_uid_index-1];
			$nextID = $CachedUID[$this_uid_index+1];
			$prevFolder = $CachedFolder[$this_uid_index-1];
			$nextFolder = $CachedFolder[$this_uid_index+1];
			$IMap->Set_Cached_Search_Result(array_diff_key($CachedUID,array($this_uid_index=>$CachedUID[$this_uid_index])),
											array_diff_key($CachedFolder,array($this_uid_index=>$CachedFolder[$this_uid_index])),
											$CachedSearchFields);
		}else{
			$PrevNextUID = $IMap->Get_Prev_Next_UID($Folder, $sort, $reverse, $UIDList[0]);
			$prevID = $PrevNextUID["PrevUID"];
			$nextID = $PrevNextUID["NextUID"];
			$prevFolder = $Folder;
			$nextFolder = $Folder;
		}
		if($prevID != ""){ // to prev mail
			$url = "viewmail.php?uid=".$prevID."&Folder=".urlencode($prevFolder)."&sort=".$sort."&reverse=".$reverse."&FromSearch=".$FromSearch."&pageNo=".$pageNo."&field=".$field;
		}else if($nextID != ""){// no prev mail, to next mail
			$url = "viewmail.php?uid=".$nextID."&Folder=".urlencode($nextFolder)."&sort=".$sort."&reverse=".$reverse."&FromSearch=".$FromSearch."&pageNo=".$pageNo."&field=".$field;
		}else{
			$url = "viewfolder.php?Folder=".urlencode($Folder)."&sort=".$sort."&reverse=".$reverse."&FromSearch=".$FromSearch."&pageNo=".$pageNo."&field=".$field;
		}
	}
	
	for ($i=0; $i<sizeof($UIDList) ; $i++) {
		if (stristr($UIDList[$i], ",") !== false) {
			List($UID, $thisFolder) = explode(",", $UIDList[$i]);
		}
		else {
			$UID = $UIDList[$i];
			$thisFolder = $Folder;
			//$Folder = $_GET['Folder'];
		}
		
		$Result = $IMap->Move_Mail(stripslashes($thisFolder), $UID, stripslashes($targetFolder));
	}
}


    if($SearchFolder)
	    $url = $_SERVER["HTTP_REFERER"];
	else if($page_from=="viewmail.php"){
		/*
		if($FromSearch==1){
			list($CachedUID,$CachedFolder) = $IMap->Get_Cached_Search_Result();
			$this_uid_index = array_search($UIDList[0],$CachedUID);
			$prevID = $CachedUID[$this_uid_index-1];
			$nextID = $CachedUID[$this_uid_index+1];
			$prevFolder = $CachedFolder[$this_uid_index-1];
			$nextFolder = $CachedFolder[$this_uid_index+1];
			$IMap->Set_Cached_Search_Result(array_diff_key($CachedUID,array($this_uid_index=>$CachedUID[$this_uid_index])),
											array_diff_key($CachedFolder,array($this_uid_index=>$CachedFolder[$this_uid_index])));
		}else{
			$PrevNextUID = $IMap->Get_Prev_Next_UID($Folder, $sort, $reverse, $UIDList[0]);
			$prevID = $PrevNextUID["PrevUID"];
			$nextID = $PrevNextUID["NextUID"];
			$prevFolder = $Folder;
			$nextFolder = $Folder;
		}
		if($nextID != ""){// to next mail
			$url = "viewmail.php?uid=".$nextID."&Folder=".urlencode($nextFolder)."&sort=".$sort."&reverse=".$reverse."&FromSearch=".$FromSearch;
		}else if($prevID != ""){ // no next mail, to prev mail
			$url = "viewmail.php?uid=".$prevID."&Folder=".urlencode($prevFolder)."&sort=".$sort."&reverse=".$reverse."&FromSearch=".$FromSearch;
		}else{
			$url = "viewfolder.php?Folder=".$Folder;
		}
		*/
	}
	else{
	    //$url = "viewfolder.php?Folder=".urlencode($Folder);
	    $tmp_parts = explode("?",$_SERVER["HTTP_REFERER"]);
    	$url = "viewfolder.php?".$tmp_parts[1];
    	if(count($tmp_parts)>1){
    		if(strstr($tmp_parts[1],"Folder=") === FALSE){
    			$url .= "&Folder=".urlencode($Folder);
    		}
    	}
	}

intranet_closedb();
header("Location: $url");
?>