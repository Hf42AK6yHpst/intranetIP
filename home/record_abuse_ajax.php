<?php
/*
 * Modifying by Paul
 * 
 * 2016-06-15 (Paul) [ip.2.5.7.7.1]
 * 	- created this page
 * 
 */
include_once('../includes/global.php');
include_once('../includes/libdb.php');

$action = (isset($action))?$action:'';

if($action == 'jsLog'){
	$cur_uri = (isset($cur_uri))?$cur_uri:'';
	$db = new libdb();
	intranet_opendb();
	$module = explode("/",$cur_uri);
	$category = 'js:';
	if(count($module)>4){
		$category .= $module[count($module)-2];
	}else{
		if(strstr($module[2],"php")){
			$category .= $module[1];
		}else{
			$category .= $module[2];
		}
	}
	if($_SESSION['refresh_checking'][$cur_uri]['refresh_times'] > 1 && $_SESSION['refresh_checking'][$cur_uri]['refresh_times'] < 49){
		$sql = "INSERT INTO INTRANET_ABUSE_LOG (LoginSessionID, UserID, Module, CurrentPath, AttemptNo, InputDate) VALUES ('".$_SESSION['iSession_LoginSessionID']."', '".$_SESSION['UserID']."', '".$category."', '".$cur_uri."', '".$tapNo."', NOW())";
		$log = $db->db_db_query($sql);
		$cur_time = time();
		$_SESSION['refresh_checking'][$cur_uri]['refresh_times'] = 0;
		$_SESSION['refresh_checking'][$cur_uri]['last_time'] = $cur_time;
//		$_COOKIE[preg_replace("/(.php)/","_php",$cur_uri)] = '';
	}
	intranet_closedb();
	echo ($log)?'T':'F';
}

?>