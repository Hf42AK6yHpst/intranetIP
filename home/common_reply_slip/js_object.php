<?php
include_once($PATH_WRT_ROOT."lang/reply_slip_lang.$intranet_session_language.php");
?>
<script language="javascript">
var replySlipObj = {
	_replySlipId: '',
	_linkToModule: '',
	_linkToType: '',
	_linkToId: '',
	_title: '',
	_description: '',
	_showQuestionNum: '',
	_ansAllQuestion: '',
	_recordType: '',
	_recordStatus: '',
	
	
	setReplySlipId: function(val) {
		replySlipObj._replySlipId = val;
	},
	getReplySlipId: function() {
		return replySlipObj._replySlipId;
	},
	
	setLintToModule: function(val) {
		replySlipObj._linkToModule = val;
	},
	getLintToModule: function() {
		return replySlipObj._linkToModule;
	},
	
	setLintToType: function(val) {
		replySlipObj._linkToType = val;
	},
	getLintToType: function() {
		return replySlipObj._linkToType;
	},
	
	setLintToId: function(val) {
		replySlipObj._linkToId = val;
	},
	getLintToId: function() {
		return replySlipObj._linkToId;
	},
	
	setTitle: function(val) {
		replySlipObj._title = val;
	},
	getTitle: function() {
		return replySlipObj._title;
	},
	
	setDescription: function(val) {
		replySlipObj._description = val;
	},
	getDescription: function() {
		return replySlipObj._description;
	},
	
	setShowQuestionNum: function(val) {
		replySlipObj._showQuestionNum = val;
	},
	getShowQuestionNum: function() {
		return replySlipObj._showQuestionNum;
	},
	
	setAnsAllQuestion: function(val) {
		replySlipObj._ansAllQuestion = val;
	},
	getAnsAllQuestion: function() {
		return replySlipObj._ansAllQuestion;
	},
	
	setRecordType: function(val) {
		replySlipObj._recordType = val;
	},
	getRecordType: function() {
		return replySlipObj._recordType;
	},
	
	setRecordStatus: function(val) {
		replySlipObj._recordStatus = val;
	},
	getRecordStatus: function() {
		return replySlipObj._recordStatus;
	},
	
	
	loadObjectData: function(replySlipId) {
		replySlipId = parseInt(replySlipId);
		if (replySlipId > 0) {
			$.ajax({
				url:		"/home/common_reply_slip/ajax_get_reply_slip_data.php",
				type:		"POST",
				data:		'replySlipId=' + replySlipId,
				async:		false,
				success:  	function(returnData) {
								var infoAry = returnData.split('<?=$replySlipConfig['ajaxDataSeparator']?>');
								replySlipObj.setReplySlipId(parseInt(infoAry[0]));
								replySlipObj.setLintToModule(Trim(infoAry[1]));
								replySlipObj.setLintToType(Trim(infoAry[2]));
								replySlipObj.setLintToId(parseInt(infoAry[3]));
								replySlipObj.setTitle(Trim(infoAry[4]));
								replySlipObj.setDescription(Trim(infoAry[5]));
								replySlipObj.setShowQuestionNum(parseInt(infoAry[6]));
								replySlipObj.setAnsAllQuestion(parseInt(infoAry[7]));
								replySlipObj.setRecordType(parseInt(infoAry[8]));
								replySlipObj.setRecordStatus(parseInt(infoAry[9]));
						  	}
			});
		}
	},
	
	returnQuestionIdAry: function() {
		var replySlipId = replySlipObj.getReplySlipId();
		var questionIdHiddenFieldId = 'replySlipQuestionIdListTb_' + replySlipId;
		
		return $('input#' + questionIdHiddenFieldId).val().split('<?=$replySlipConfig['questionIdHiddenFieldSeparator']?>');
	},
	
	returnCurUserId: function() {
		var replySlipId = replySlipObj.getReplySlipId();
		return $('input#replySlipUserIdTb_' + replySlipId).val();
	},
	
	submitReplySlip: function(replySlipId) {
		replySlipObj.loadObjectData(replySlipId);
		var success;
		
		if (replySlipObj.checkForm(1)) {
			success = replySlipObj.saveAnswer();
		}
		else {
			success = false;
		}
		
		return success;
	},
	
	checkForm: function(noAlert) {
		noAlert = noAlert || 0;
		
		var replySlipId = replySlipObj.getReplySlipId();
		replySlipObj.loadObjectData(replySlipId);
		
		var questionIdAry = replySlipObj.returnQuestionIdAry();
		var numOfQuestion = questionIdAry.length;
		var isValid = true;
		var isFocused = false;
		
		// Hide all warning div
		$('div.replySlipWarningDiv_' + replySlipId).hide();
		
		// See if need to check if all questions have been answered or not
		var ansAllQuestion = replySlipObj.getAnsAllQuestion();
		
		// loop questions
		var i;
		var notAnsweredQuestionNumAry = new Array();
		for (i=0; i<numOfQuestion; i++) {
			var _questionId = questionIdAry[i];
			var _questionClass = 'replySlipAnswer_' + replySlipId + '_' + _questionId;
			var _isAnswered = false;
			
			if (parseInt($('.' + _questionClass).length) == 0) {
				// "NA" type question => no need answer
				_isAnswered = true;
			}
			else {
				$('.' + _questionClass).each( function () {
					if (!isObjectValueEmpty($(this).attr('id'))) {
						_isAnswered = true;
						return false;	// same as break
					}
				});
			}
			
			if (!_isAnswered) {
				notAnsweredQuestionNumAry[notAnsweredQuestionNumAry.length] = i + 1;
				
				if (ansAllQuestion == '<?=$replySlipConfig['INTRANET_REPLY_SLIP']['AnsAllQuestion']['yes']?>') {
					isValid = false;
					$('div#replySlipAnsQuestionWarningDiv_' + replySlipId + '_' + _questionId).show();
					
					if (!isFocused) {
						// focus the first element of the first error question
						isFocused = true;
						$('.' + _questionClass + ':first').focus();
					}
				}
			}
		}
		
		if (ansAllQuestion == '<?=$replySlipConfig['INTRANET_REPLY_SLIP']['AnsAllQuestion']['yes']?>' && notAnsweredQuestionNumAry.length == 0) {
			if (noAlert == 1) {
				isValid = false;
			}
			else {
				if (confirm('<?=$Lang['ReplySlip']['WarningMsg']['SubmitReplySlip']?>')) {
					isValid = true;
				}
				else {
					isValid = false;
				}
			}
		}
		else if (ansAllQuestion == '<?=$replySlipConfig['INTRANET_REPLY_SLIP']['AnsAllQuestion']['no']?>' && notAnsweredQuestionNumAry.length > 0) {
			if (noAlert == 1) {
				isValid = true;
			}
			else {
				var alertText = '<?=$Lang['ReplySlip']['WarningMsg']['ReplySlipQuestionNotAnswered']?>';
				//alertText = alertText.replace("<!-questionNumList->", notAnsweredQuestionNumAry.join(', '));
				if (confirm(alertText)) {
					isValid = true;
				}
				else {
					isValid = false;
				}
			}
		}
		else {
			if (noAlert != 1) {
				if (confirm('<?=$Lang['ReplySlip']['WarningMsg']['SubmitReplySlip']?>')) {
					isValid = true;
				}
				else {
					isValid = false;
				}
			}
		}
		
		return isValid;
	},
	
	saveAnswer: function() {
		replySlipObj.createTempForm();
		replySlipObj.submitTempForm();
		replySlipObj.removeTempForm();
		
		return true;
	},
	
	createTempForm: function() {
		// collect answer data
		var questionAnswerAssoAry = replySlipObj.returnQuestionAnswerAssoAry();
		
		// build temp form for answer submission
		var x = '';
		x += '<form id="replySlipTempForm" name="replySlipTempForm" method="post">' + "\r\n";
			$.each(questionAnswerAssoAry, function(_questionId, _answerValue) {
				if (_answerValue == null) {
					return true;	// continue
				}
				
				x += '<input type="hidden" name="replySlipAnswerAry[' + _questionId + ']" value="' + htmlspecialchars(_answerValue) + '" />' + "\r\n"; 
			});
		x += '</form>' + "\r\n";
		
		$('body').append(x);
	},
	
	returnQuestionAnswerAssoAry: function() {
		var replySlipId = replySlipObj.getReplySlipId();
		var questionIdAry = replySlipObj.returnQuestionIdAry();
		var numOfQuestion = questionIdAry.length;
		var answerAssoAry = new Array();
		var i;
		
		// loop questions
		for (i=0; i<numOfQuestion; i++) {
			var _questionId = questionIdAry[i];
			var _questionClass = 'replySlipAnswer_' + replySlipId + '_' + _questionId;
			var _answerElementName = getJQuerySaveId('answerAry[' + replySlipId + '][' + _questionId + ']');
			var _answerAry = new Array();
			var _answerValue = '';
			
			// loop each answer options of the question
			$('[name="' + _answerElementName + '"]').each( function() {
				switch ($(this).attr('tagName').toUpperCase()) {
					case 'INPUT':
						var objectType = $(this).attr('type').toUpperCase();
						switch (objectType) {
							case 'TEXT':
								_answerValue = $.trim($(this).val());
								break;
							case 'RADIO':
							case 'CHECKBOX':
								if ($(this).attr('checked')) {
									_answerAry[_answerAry.length] = $(this).val();
								}
								_answerValue = _answerAry.join(',');
								break;
						}
						break;
					case 'TEXTAREA':
						_answerValue = $.trim($(this).val());
						break;
					case 'SELECT':
						if ($(this).attr('multiple')) {
							_answerValue = $(this).val().join(',');
						}
						else {
							_answerValue = $(this).val();
						}
						break;
					default:
						_answerValue = '';
				}
			});
			
			answerAssoAry[_questionId] = _answerValue;
		}	// end loop question
		
		return answerAssoAry;
	},
	
	submitTempForm: function() {
		$.ajax({
			url:		"/home/common_reply_slip/ajax_save_reply_slip_answer.php",
			type:		"POST",
			data:		'replySlipId=' + replySlipObj.getReplySlipId() + '&replySlipUserId=' + replySlipObj.returnCurUserId() + '&' + $('form#replySlipTempForm').serialize(),
			async:		false,
			success:  	function(returnData) {
							
					  	}
		});
	},
	
	removeTempForm: function() {
		$('form#replySlipTempForm').remove();
	}
};
</script>