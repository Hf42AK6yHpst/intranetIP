<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT.'includes/global.php');
include_once($PATH_WRT_ROOT.'includes/libdb.php');
include_once($PATH_WRT_ROOT.'includes/replySlip/libReplySlipMgr.php');

if(strpos($_SERVER['HTTP_REFERER'],'/home/eAdmin/ResourcesMgmt/DocRouting/index.php?pe=8OaOAb4COQCRJNyp2jeWSB4kDyYanKf')===false){
	intranet_auth();
}
intranet_opendb();

$replySlipId = trim($_POST['replySlipId']);

$dataAry = array();
$libReplySlip = new libReplySlip($replySlipId);
$dataAry[] = $libReplySlip->getReplySlipId();
$dataAry[] = $libReplySlip->getLinkToModule();
$dataAry[] = $libReplySlip->getLinkToType();
$dataAry[] = $libReplySlip->getLinkToId();
$dataAry[] = $libReplySlip->getTitle();
$dataAry[] = $libReplySlip->getDescription();
$dataAry[] = $libReplySlip->getShowQuestionNum();
$dataAry[] = $libReplySlip->getAnsAllQuestion();
$dataAry[] = $libReplySlip->getRecordType();
$dataAry[] = $libReplySlip->getRecordStatus();

echo implode($replySlipConfig['ajaxDataSeparator'], $dataAry);

intranet_closedb();
?>