<?php
//using : 

#####################################
#
#   Date:   2019-10-14 Tommy
#           add access checking for KIS
#
#   Date:   2019-07-12 Bill     [2019-0711-1152-12207]
#           add TitleEng, EventVenueEng for general
#
#   Date:   2019-01-09 Anna
#           added DescriptionEng, EventNatureEng for general
#
#   Date:   2018-06-25 Anna
#           change $txtLocation from other to others
#
#	Date:	2013-10-07	Carlos
#			[KIS] added column [Class Group Code]
#
#	Date:	2011-07-06	YatWoon
#			add "isSkipSAT" and "isSkipSUN"
#
#####################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

$lcycleperiods = new libcycleperiods();
$lexport = new libexporttext();

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['Class']['ClassGroupSettings'];
if($isKIS){
    if(!$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] && !$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"]){
        header("location: /kis/#/apps/calendar/?clearCoo=1");
        exit();
    }
	if($_SESSION['UserType'] != USERTYPE_STAFF) {
		include_once($intranet_root."/includes/libclassgroup.php");
		$lclassgroup = new Class_Group();
		$ClassGroupID = $lclassgroup->Get_Class_GroupID_By_UserID($AcademicYearID);
	}else if($_REQUEST['ClassGroupID'] != ''){
		$ClassGroupID = $_REQUEST['ClassGroupID'];
	}
	if($_SESSION['UserType'] != USERTYPE_STAFF) {
		$ClassGroupIdCond = $ClassGroupID!=""?" AND (a.ClassGroupID IN (".(is_array($ClassGroupID)?implode(",",$ClassGroupID):$ClassGroupID).") OR a.ClassGroupID IS NULL OR a.ClassGroupID='') ":"";
	}else{
		$ClassGroupIdCond = $ClassGroupID!=""?" AND a.ClassGroupID IN (".(is_array($ClassGroupID)?implode(",",$ClassGroupID):$ClassGroupID).") ":"";
	}
	$ClassGroupJoinTable = " LEFT JOIN YEAR_CLASS_GROUP as ycg ON ycg.ClassGroupID=a.ClassGroupID ";
	$ClassGroupCodeField = ",ycg.Code ";
}

//$AcademicYearID
$StartDate = getStartDateOfAcademicYear($AcademicYearID);
$EndDate = getEndDateOfAcademicYear($AcademicYearID);
$SchoolYearInfo = getAcademicYearByAcademicYearID($AcademicYearID);
if($sys_custom['eClassApp']['SFOC']){
    // $addtionFields = ", a.EventAssociation, a.EventAssociationEng, a.TitleEng, a.EventVenueEng, a.Website, a.ContactNo, a.Email ";
    $addtionFields = ", a.EventAssociation, a.EventAssociationEng, a.Website, a.ContactNo, a.Email ";
}
$sql = "SELECT 
	        a.EventID, a.Title, a.TitleEng, a.Description, a.DescriptionEng, a.EventNature, a.EventNatureEng, a.EventDate As StartDate, c.EndDate AS EndDate, 
	        a.EventLocationID, a.EventVenue, a.EventVenueEng, a.IsSkipCycle, a.RecordType, a.isSkipSAT, a.isSkipSUN 
	        $ClassGroupCodeField $addtionFields
		FROM 
	        INTRANET_EVENT AS a 
			INNER JOIN (SELECT b.EventID, b.RelatedTo, MAX(b.EventDate) AS EndDate FROM INTRANET_EVENT AS b WHERE b.RelatedTo IS NOT NULL OR b.RelatedTo != '' GROUP BY b.RelatedTo) AS c ON (a.EventID = c.RelatedTo) 
			$ClassGroupJoinTable 
	    WHERE
	        a.EventDate BETWEEN '$StartDate' AND '$EndDate' $ClassGroupIdCond
	    GROUP BY 
	        a.RelatedTo
	    ORDER By
	        a.EventDate, a.RecordType DESC";
$arrResult = $lcycleperiods->returnArray($sql);

if(sizeof($arrResult)>0) 
{
	for($i=0; $i<sizeof($arrResult); $i++) 
	{
	  
	    if($sys_custom['eClassApp']['SFOC']){
	        list($event_id, $event_title, $TitleEng, $event_description, $event_descriptionEng, $event_nature, $event_natureEng, $start_full_date, $end_full_date,
                $location_id, $venue, $EventVenueEng, $skip_school_day, $event_type, $skip_sat, $skip_sun,
                $EventAssociation, $EventAssociationEng, $Website, $ContactNo, $Email) = $arrResult[$i];
	    }else{
	        list($event_id, $event_title, $TitleEng, $event_description, $event_descriptionEng, $event_nature, $event_natureEng, $start_full_date, $end_full_date,
                $location_id, $venue, $EventVenueEng, $skip_school_day, $event_type, $skip_sat, $skip_sun, $class_group_code) = $arrResult[$i];
	    }
	     
		// convert event type from int format to string format
		switch ($event_type) {
			case 0: $Type = "SE"; break; // School event
			case 1: $Type = "AE"; break; // Academic event
			case 2: $Type = "GE"; break; // Group event
			case 3: $Type = "PH"; break; // Public holiday
			case 4: $Type = "SH"; break; // School holiday
		}
		
		// convert Skip School Day from int format to string format
		if($skip_school_day == 1)
			$txtSkipSchoolDay = "Y";
		else
			$txtSkipSchoolDay = "N";
			
		// convert Skip SAT from int format to string format
		if($skip_sat == 1)
			$txtSkipSAT = "Y";
		else
			$txtSkipSAT = "N";
		
		// convert Skip SUN from int format to string format
		if($skip_sun == 1)
			$txtSkipSUN = "Y";
		else
			$txtSkipSUN = "N";
		
		// convert date format	
		$StartDate = date("Y-m-d",strtotime($start_full_date));
		$EndDate = date("Y-m-d",strtotime($end_full_date));
		
		// convert location data into string format
		if(($location_id == 0) && ($venue == "")){
			$txtLocation = "";
		}else if($location_id != 0){
			$sql = "SELECT
						CONCAT(building.Code,'>',floor.Code,'>',room.Code) 
					FROM
						INVENTORY_LOCATION_BUILDING as building 
						INNER JOIN INVENTORY_LOCATION_LEVEL as floor ON (building.BuildingID = floor.BuildingID)
						INNER JOIN INVENTORY_LOCATION as room ON (floor.LocationLevelID = room.LocationLevelID)
					WHERE 
						room.LocationID = $location_id";
			$arrLocationResult = $lcycleperiods->returnVector($sql);
			$txtLocation = $arrLocationResult[0]; 
		}else if($venue != ""){
			$txtLocation = 'Others>'.$venue;
		}
// 		debug_pr($venue);
// 		debug_pr($EventVenueEng);
		
		// if this is group event, get GroupID
		$arrTargetGroup = array();
		if($event_type == 2) {
			$sql = "SELECT GroupID FROM INTRANET_GROUPEVENT WHERE EventID = '$event_id'";
			$arrTargetGroup = $lcycleperiods->returnVector($sql);
			if(sizeof($arrTargetGroup)>0){
				$txtGroupID = implode(",",$arrTargetGroup);
			}else{
				$txtGroupID = "";
			}
		}else{
			$txtGroupID = "";
		}
		if($isKIS) {
		    $arrExportContent[] = array($StartDate, $EndDate, $Type, $event_title, $TitleEng, $txtLocation, $EventVenueEng, $event_nature, $event_natureEng, $event_description, $event_descriptionEng,
                                        $txtSkipSchoolDay, $txtGroupID, $txtSkipSAT, $txtSkipSUN, $class_group_code);
		}else if($sys_custom['eClassApp']['SFOC']){
		    $arrExportContent[] = array($StartDate, $EndDate, $Type, $EventAssociation, $EventAssociationEng, $event_title, $TitleEng, $txtLocation, $EventVenueEng, $event_nature, $event_natureEng, $event_description, $event_descriptionEng,
		                                $txtSkipSchoolDay, $txtGroupID, $txtSkipSAT, $txtSkipSUN, $Email, $ContactNo, $Website);
		}
		else{
		    $arrExportContent[] = array($StartDate, $EndDate, $Type, $event_title, $TitleEng, $txtLocation, $EventVenueEng, $event_nature, $event_natureEng, $event_description, $event_descriptionEng,
                                        $txtSkipSchoolDay, $txtGroupID, $txtSkipSAT, $txtSkipSUN);
		}
	}
}
else
{
	$arrExportContent[] = array($i_no_record_exists_msg);
}

// Set Export Column Name In the CSV
$exportColumnName = array(
							$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['StartDate'],
							$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['EndDate'],
							$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['Type'],
							$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['Title'],
                            $Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['TitleEng'],
							$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['Venue'],
                            $Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['VenueEng'],
							$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['Nature'],
                            $Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['NatureEng'],
                            $Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['Description'],
							$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['DescriptionEng'],
							$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['SkipSchoolDay'],
							$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['GroupID'],
							$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['SkipSAT'],
							$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['SkipSUN']
							);

if($sys_custom['eClassApp']['SFOC']){
    $exportColumnName = array();
    $exportColumnName = array(
        $Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['StartDate'],
        $Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['EndDate'],
        $Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['Type'],
        "Association",
        "AssociationEng",
        $Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['Title'],
        "TitleEng",
        $Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['Venue'],
        "VenueEng",
        $Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['Nature'],
        $Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['NatureEng'],
        $Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['Description'],
        $Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['DescriptionEng'],
        $Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['SkipSchoolDay'],
        $Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['GroupID'],
        $Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['SkipSAT'],
        $Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['SkipSUN'],"Email","ContactNo","Website"   
    );
}
if($isKIS){
	$exportColumnName = array_merge($exportColumnName,array($Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['ClassGroupCode']));
}
							
$csv_filename = "(".$SchoolYearInfo.")All_School_Holidays_Events.csv";
$export_content = $lexport->GET_EXPORT_TXT($arrExportContent, $exportColumnName);
$lexport->EXPORT_FILE($csv_filename, $export_content);

intranet_closedb();
?>