<?php
// using ronald
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");

intranet_auth();
intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);
$TargetID = stripslashes($_REQUEST['TargetID']);


if ($RecordType == "Both")
{
	$InputValueEn = stripslashes($_REQUEST['InputValueEn']);
	$InputValueCh = stripslashes($_REQUEST['InputValueCh']);
	//$OjbLearningCategory = new Learning_Category($SelfID);
	//$isExistedTitle = $OjbLearningCategory->Is_Title_Existed($RecordType, $InputValueCh, $InputValueEn);
	$lcycleperiods = new libcycleperiods();
	$result = $lcycleperiods->Is_Title_Existed($RecordType, $TargetID, $InputValueCh, $InputValueEn);
}
else
{
	$InputValue = stripslashes($_REQUEST['InputValue']);
	//$OjbLearningCategory = new Learning_Category($SelfID);
	//$isExistedTitle = $OjbLearningCategory->Is_Title_Existed($RecordType, $InputValue);
	$lcycleperiods = new libcycleperiods();
	$result = $lcycleperiods->Is_Title_Existed($RecordType, $TargetID, $InputValue);
}


# Initialize library


if ($result == 1)
	echo $Lang['SysMgr']['AcademicYear']['JSWarning']['AcademicYearOverlapped'];
else
	echo "";


intranet_closedb();
?>