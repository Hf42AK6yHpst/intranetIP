<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$lcycleperiods = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();

$lcycleperiods->Start_Trans();

if($EditMode == 1) {
	$result = $lcycleperiods->insertEditTerm($EditMode,$AcademicYearID,$YearTermID,$TermTitleCh,$TermTitleEn,$TermStartDate,$TermEndDate);
	
	if (in_array(false,$result)) {
		$lcycleperiods->RollBack_Trans();
		echo 0;
	}else {
		$lcycleperiods->Commit_Trans();
		echo 1;
	}
} else {
	$result = $lcycleperiods->insertEditTerm($EditMode,$AcademicYearID,$YearTermID,$TermTitleCh,$TermTitleEn,$TermStartDate,$TermEndDate);
		
	if (in_array(false,$result)) {
		$lcycleperiods->RollBack_Trans();
		echo 0;
	}else {
		$lcycleperiods->Commit_Trans();
		echo 1;
	}				
}

/*
if (in_array(false,$result)) {
	$lcycleperiods->RollBack_Trans();
	if($EditMode == 1)
		echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	if($EditMode == 0)
		echo $Lang['General']['ReturnMessage']['AddUnsuccess'];
}else {
	$lcycleperiods->Commit_Trans();
	if($EditMode == 1)
		echo $Lang['General']['ReturnMessage']['UpdateSuccess'];
	if($EditMode == 0)
		echo $Lang['General']['ReturnMessage']['AddSuccess'];
}
*/
intranet_closedb();
?>