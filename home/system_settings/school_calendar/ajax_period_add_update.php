<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lc = new libcycleperiods();
			
$li->Start_Trans();
			
$ts_start = strtotime($PeriodStart);
$ts_end = strtotime($PeriodEnd);

if ($ts_end < $ts_start)
{
    $li->RollBack_Trans();
	echo $Lang['General']['ReturnMessage']['AddUnsuccess'];
	die();
}
else
{
	# Check date range is in the selected school year or not #
	$sql = "SELECT DISTINCT AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE (UNIX_TIMESTAMP(TermStart) <= $ts_start) AND AcademicYearID = '$SchoolYearID'";
	$arrExistRange1 = $li->returnVector($sql);
	$sql = "SELECT DISTINCT AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE (UNIX_TIMESTAMP(TermEnd) >= $ts_end) AND AcademicYearID = '$SchoolYearID'";
	$arrExistRange2 = $li->returnVector($sql);				

	if($arrExistRange1[0] == '' || $arrExistRange2[0] == '')
	{
		$li->RollBack_Trans();
		echo -1;
		die();
	}
	else
	{
		# Check any overlaped date range #
		$sql = "SELECT PeriodID FROM INTRANET_CYCLE_GENERATION_PERIOD
					   WHERE '$ts_start' BETWEEN UNIX_TIMESTAMP(PeriodStart) AND UNIX_TIMESTAMP(PeriodEnd)
							  OR '$ts_end' BETWEEN UNIX_TIMESTAMP(PeriodStart) AND UNIX_TIMESTAMP(PeriodEnd)";
		$temp = $li->returnVector($sql);
				
		if ($temp[0]!="")
		{
			$li->RollBack_Trans();
			echo 0;
			die();
		}
		else
		{
			## check the date range is in same term ##
			$sql = "SELECT CASE WHEN COUNT(YearTermID)>0 THEN 1 ELSE 0 END AS tmp_result FROM ACADEMIC_YEAR_TERM WHERE ('$ts_start' BETWEEN UNIX_TIMESTAMP(TermStart) AND UNIX_TIMESTAMP(TermEnd))
							  AND ('$ts_end' BETWEEN UNIX_TIMESTAMP(TermStart) AND UNIX_TIMESTAMP(TermEnd))";
			$arr_tmp = $lc->returnVector($sql);
			$pass = $arr_tmp[0];
			
			if($pass == 1)
			{
				# insert the date range into DB #
				$result = $lc->createPeriod($PeriodStart,$PeriodEnd,$PeriodType,$CycleType,$PeriodDays,$FirstDay,$SatCount,$ColorCode,$TimetableTemplate,$SkipOnWeekday);
				
				if (in_array(false,$result)) {
					$li->RollBack_Trans();
					echo $Lang['General']['ReturnMessage']['AddUnsuccess'];
					die();
				}
				else
				{
					$li->Commit_Trans();
					echo $Lang['General']['ReturnMessage']['AddSuccess'];
					die();
				}
			}
			else
			{
				$li->RollBack_Trans();
				echo -2;
				die();
			}
		}
	}
}

intranet_closedb();
?>