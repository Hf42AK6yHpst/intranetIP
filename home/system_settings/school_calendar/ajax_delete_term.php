<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$lcycleperiods = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();

$lcycleperiods->Start_Trans();

$result = $lcycleperiods->deleteTerm($YearTermID);

if (in_array(false,$result)) {
	$lcycleperiods->RollBack_Trans();
	echo $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
}else {
	$lcycleperiods->Commit_Trans();
	echo $Lang['General']['ReturnMessage']['DeleteSuccess'];
}

intranet_closedb();
?>