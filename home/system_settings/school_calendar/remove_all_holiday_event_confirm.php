<?php
//using : Ronald
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lcycleperiods_ui = new libcycleperiods_ui();

$CurrentPageArr['SchoolCalendar'] = 1;
### Title ###
$TAGS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['SettingTitle'],"index.php",1);
$TAGS_OBJ[] = array($Lang['SysMgr']['CycleDay']['SettingTitle'],"time_zone.php",0);
$TAGS_OBJ[] = array($Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearSetting'],"academic_year.php",0);
$MODULE_OBJ['title'] = $Lang['SysMgr']['SchoolCalendar']['ModuleTitle'];

$table_content = $lcycleperiods_ui->GetDeleteAllSchoolEventsOrHolidaysLayer($TargetYearID);
 
$linterface->LAYOUT_START();
?>
<SCRIPT language="JavaScript">
function checkForm()
{
	if(confirm("<?=$Lang['SysMgr']['SchoolCalendar']['JSJSWarning']['DeleteAllEventsHolidays'];?>"))
	{
		
	}
	else
	{
		return false;
	}
}
</SCRIPT> 
<br>
<form name="form1" id="form1" action="remove_all_holiday_event.php" methof="POST" onSubmit="return checkForm();">
<?=$table_content;?>
<input type="hidden" name="TargetYearID" id="TargetYearID" value="<?=$TargetYearID?>">
</form>
<br>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>