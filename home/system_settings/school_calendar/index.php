<?php
## Using By : 

##########################################################
#
# Date: 2019-08-27 Henry
#       - bug fix for the date picker in editHolidayEventForm()
#
# Date: 2019-06-14 Cameron
#       - disable NewHolidayEventSubmitBtn after first click to avoid duplicate record in updateNewHolidayEvent()
#
# Date: 2019-03-04 Isaac
#       - modified dateEditHolidayEvent() added condition to add the endDate datePicker only when the event/holiday is multiple dates
#
# Date: 2018-03-08 Anna 
#       - added SFOC cust
#
# Date: 2015-09-15 Ivan [U84654] [ip.2.5.6.10.1]
#		- enable timezone settings for KIS if purchased eBooking module
#
# Date: 2014-08-08 YatWoon
#		- rediect to "year settings" if KIS & stand alone eAdmission
# 
# Date:	2013-10-04 Carlos
#		- [KIS] modified js updateNewHolidayEvent(),updateEditHolidayEvent(),printPreview(),exportHolidayEvent() add post var ClassGroupID
#		- [KIS] modified js ChangeSchoolYear(), add post var targetClassGroupID
#
# Date:	2011-09-20 YatWoon
#		- add js function changeEventEndDate()
#
# Date:	2011-07-06 YatWoon
#		- Add "isSkipSAT" and "isSkipSUN"
#
## 2010-06-21: Max (201006071657)
## - modified function setPrintingOptionsDefault not to set value for deleted item
#
#	Date:	2010-06-21	YatWoon
#			add online help button
#
## 2010-06-04: Max (201006040909)
## - pass academic year id to the printing page
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
#
## 2009-12-14: Max (200912141001)
## - Link to a page with printing version
##########################################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
include_once($PATH_WRT_ROOT."includes/json.php");

# check if KIS & stand alone eAdmission
if($_SESSION["platform"] && $stand_alone['eAdmission'])
{
	header("Location: academic_year.php");
	exit;
}


intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"] || $sys_custom["SchoolCalendarWhenNotAdminReadOnly"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$lc = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();
$lfcm = new form_class_manage();
$libcalevent = new libcalevent2007();

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['Class']['ClassGroupSettings'];

#############################################
## System Checking 
## Step 1 : check finish academic year setting or not 
##				IsAcademicYearSet() : 1 = true, 0= false
## Step 2 : check current date is in term or not
##				IsCurrentDateInTerm() : 1 = true, 0 = false
#############################################
if(!$lc->IsAcademicYearSet())
{
	$empty_msg = $Lang['SysMgr']['SchoolCalendar']['FieldTitle']['AcademicYearIsNotSet'];
	$msg = urlencode($empty_msg);
	//header("Location: academic_year.php?msg=$msg");
}
if(!$lc->IsCurrentDateInTerm())
{
	$not_in_term_msg = $Lang['SysMgr']['SchoolCalendar']['FieldTitle']['CurrentDateNotInTerm'];
	$msg = urlencode($not_in_term_msg);
	//header("Location: academic_year.php?msg=$msg");
}
## End checking ##

$CurrentPageArr['SchoolCalendar'] = 1;
### Title ###
$TAGS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['SettingTitle'],"index.php",1);

//if ($_SESSION["platform"]!="KIS"){
//    $TAGS_OBJ[] = array($Lang['SysMgr']['CycleDay']['SettingTitle'],"time_zone.php",0);
//}
$showTimezone = true;
if ($_SESSION["platform"]=="KIS" && !$plugin['eBooking'] || $sys_custom['LivingHomeopathy'] || $sys_custom['HideTimeZone']){
	// kis without eBooking => hide timezone settings
	$showTimezone = false;
}
if ($showTimezone) {
	$TAGS_OBJ[] = array($Lang['SysMgr']['CycleDay']['SettingTitle'],"time_zone.php",0);
}

$showAcademicYear = true;
if ($sys_custom['LivingHomeopathy'] || $sys_custom['HideAcademicYearTerm']) {
	$showAcademicYear = false;
}
if ($showAcademicYear) {
	$TAGS_OBJ[] = array($Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearSetting'],"academic_year.php",0);
}

# change page web title
$js = '<script type="text/JavaScript" language="JavaScript">'."\n";
$js.= 'document.title="eClass School Calendar";'."\n";
$js.= '</script>'."\n";

$MODULE_OBJ['title'] = $Lang['SysMgr']['SchoolCalendar']['ModuleTitle'].$js;

# online help button
//$onlineHelpBtn = gen_online_help_btn_and_layer('school_settings','event');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);


if($AcademicYearID == "")
{
	$AcademicYearID = Get_Current_Academic_Year_ID();
}

$returnMsg = urldecode($msg);

$linterface->LAYOUT_START($returnMsg); 
?>
<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

<?php echo $lcycleperiods_ui->getProductionCycleDaysCalendarView(1,$AcademicYearID); ?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>


<script type="text/javascript">
setPrintingOptionsDefault();
function hide(objID) {
	document.getElementById(objID).style.display = 'none';
}
function show(objID) {
	document.getElementById(objID).style.display = '';
}
/**
the obj to check
function to call when checked
para for the 1st checked function
function to call when unchecked
para for the 2nd checked function
*/
function isChecked(obj, callbackChecked, paraArr1, callbackUnchecked, paraArr2) {
	if(typeof(callbackChecked)=="function")
		for(i=0;i<paraArr1.length;i++){paraArr1[i]="'"+paraArr1[i]+"'";}
	if(typeof(callbackUnchecked)=="function")
		for(i=0;i<paraArr2.length;i++){paraArr2[i]="'"+paraArr2[i]+"'";}
	
	var callbackCheckedStr = "callbackChecked("+paraArr1.join(',')+");";
	var callbackUncheckedStr = "callbackUnchecked("+paraArr2.join(',')+");";
	if (obj.checked) {
		eval(callbackCheckedStr);
		return true;
	} else {
		eval(callbackUncheckedStr);
		return false;
	}
	return false
}
function printPreview() {
	var url = 'print_preview.php';
	var editPrintingOptions = getCheckedValue(document.getElementsByName('editPrintingOptions'));	
	var printColor = getCheckedValue(document.getElementsByName('printColor'));
	var printMonthEvent = getCheckedValue(document.getElementsByName('printMonthEvent'));
	var maxCal = getCheckedValue(document.getElementsByName('maxCal'));
<?php if ($showAcademicYear) { ?>
	var academicYearId = document.getElementById('academic_year').value;
<?php } else { ?>
	var academicYearId = <?php echo $AcademicYearID; ?>;
<?php } ?>
	url = url + "?p_color=" + printColor + "&p_month_event=" + printMonthEvent + "&max_cal=" + maxCal + "&academicYearId=" + academicYearId;
<?php if($isKIS){ ?>
	url += "&class_group_id=" + $('#class_group_id').val();
<?php } ?>
	window.open (url, '','status=yes,menubar=yes,scrollbars=yes,resizable=yes');
}

function setPrintingOptionsDefault() {
	if (typeof document.getElementsByName('printColor')[0] != "undefined") {
		document.getElementsByName('printColor')[0].checked='checked';
		document.getElementsByName('maxCal')[1].checked='checked';
	}
}

function getCheckedValue(radioObj) {
	if(!radioObj)
		return "";
	var radioLength = radioObj.length;
	if(radioLength == undefined)
		if(radioObj.checked)
			return radioObj.value;
		else
			return "";
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
	return "";
}

function ChangeSchoolYear(type, schoolYearID)
{
	Block_Document();
	$.post(
		"ajax_calendar_view.php",
		{
			"type":type,
			"targetSchoolYearID":schoolYearID
<?php if($isKIS){ ?>
			,"targetClassGroupID":$('#class_group_id').val() 
<?php } ?>
		},
		function(responseText){
			UnBlock_Document();
			$('#main_body').html(responseText);
			initThickBox();
		}
	);
}

function changeView(type, schoolYearID)
{
	Block_Document();
	$.post(
		"ajax_change_view.php",
		{
			"type":type,
			"targetSchoolYearID":schoolYearID
		},
		function(responseText){
			$('#main_body').html(responseText);
			UnBlock_Document();
		}
	);
}

// js function - redirect to other page
function gotoPage(page)
{
	var url = page;
	self.location=url;
}

// use AJAX to show School Event or Holiday Layer
function showHolidayEventForm(Date,Msg)
{
	wordXmlHttp = GetXmlHttpObject();
    	
	var postContent = 'TargetDate='+Date;
	var url = 'ajax_school_holiday_event_list.php';
	
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
			document.getElementById('TB_ajaxContent').innerHTML = wordXmlHttp.responseText;
			if(Msg != undefined)
				Get_Return_Message(Msg);
		}
	};
	wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}

// use AJAX to gen input form for New School Event or Holiday 
function newHolidayEventForm(Date)
{
	wordXmlHttp = GetXmlHttpObject();
    
	var postContent = 'TargetDate='+Date;
	var url = 'ajax_school_holiday_event_add.php';
	
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
			document.getElementById('TB_ajaxContent').innerHTML = wordXmlHttp.responseText;
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#EventEndDate').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
			
			// Special Timetable Warning checking
			var jsSkipSchoolDay = ($('input#skip_cycle_day_yes').attr('checked'))? 1 : 0;
			var jsStartDate = $('input#EventStartDate').val();
			var jsEndDate = $('input#EventEndDate').val();
			js_Check_Special_Timetable_Warning_Display(jsStartDate, jsEndDate);
		}
	};
	wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}

function changeEventEndDate()
{
	var jsStartDate = $('input#EventStartDate').val();
	var jsEndDate = $('input#EventEndDate').val();	
	$.post(
		"ajax_validate.php", 
		{ 
			Action: 'Check_skip_sat_sun',
			StartDate: jsStartDate,
			EndDate: jsEndDate
		},
		function(ReturnData)
		{
			if (ReturnData == '1') {
				$('input#skip_sat1').attr('disabled', false);
				$('input#skip_sat0').attr('disabled', false);
				$('input#skip_sun1').attr('disabled', false);
				$('input#skip_sun0').attr('disabled', false);
			}
			else {
				$('input#skip_sat1').attr('disabled', true);
				$('input#skip_sat0').attr('disabled', true);
				$('input#skip_sat0').attr('checked', true);
				$('input#skip_sun1').attr('disabled', true);
				$('input#skip_sun0').attr('disabled', true);
				$('input#skip_sun0').attr('checked', true);
			}
		}
	);
}

// use AJAX to gen edit form for school Event or Holiday	
function editHolidayEventForm(EventID,ShowEndDate,CallFrom)
{
	wordXmlHttp = GetXmlHttpObject();

	
	
	var postContent = 'EventID='+EventID;
			postContent = postContent+'&ShowEndDate='+ShowEndDate;
			postContent = postContent+'&CallFrom='+CallFrom;
	var url = 'ajax_school_holiday_event_edit.php';


	
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
			document.getElementById('TB_ajaxContent').innerHTML = wordXmlHttp.responseText;
			document.getElementById('TB_ajaxWindowTitle').innerHTML = "<?=$Lang['SysMgr']['SchoolCalendar']['Event']['EditHolidayEvent'];?>";

			<?if($sys_custom['eClassApp']['SFOC']){?>
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#EventStartDate').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
			
			// Special Timetable Warning checking
			var jsSkipSchoolDay = ($('input#skip_cycle_day_yes').attr('checked'))? 1 : 0;
			var jsStartDate = $('input#EventStartDate').val();
			var jsEndDate = $('input#EventEndDate').val();

			if(jsStartDate < jsEndDate){
    			$('#EventEndDate').datepick({
    				dateFormat: 'yy-mm-dd',
    				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    				changeFirstDay: false,
    				firstDay: 0
    			});
			}
			
			js_Check_Special_Timetable_Warning_Display(jsStartDate, jsEndDate);
			<?}?>
		}
	};
	wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}

// js function - check input fields
function checkInputFields()
{
	var obj = document.getElementById("eventType");
	var eventTypeCheckCount = 0;
		
	if(check_text(document.getElementById("EventEndDate"),"<?=$Lang['SysMgr']['SchoolCalendar']['JSWarning']['EventEndDateIsEmpty'];?>"))
	{
		if(check_date(document.getElementById("EventEndDate"),"<?=$Lang['SysMgr']['SchoolCalendar']['JSWarning']['InvalidEventEndDate'];?>"))
		{
			var result = compareDate2(document.getElementById("EventEndDate").value,document.getElementById("EventStartDate").value);
			
			if((result == 1) || (result == 0)){
				//continuous
			} else {
				alert("<?=$Lang['SysMgr']['SchoolCalendar']['JSWarning']['EndDateLaterThanStartDate'];?>");
				return false;
			}
		}
	} else {
		return false;
	}
	
	for(i=0; i<obj.length; i++){		// check any event type is selected or not
		if((obj[i].selected==true) && (obj[i].value!='')){
			eventTypeCheckCount = 1;
			
			if(obj[i].value == 2) {
				var tmpObj  = document.getElementById("targetGroupID");
				var checkCounter = "";
				for(j = 0; j < tmpObj.length; j++) {
					if(tmpObj[j].value != "")
					{
						checkCounter++;
					}
				}
				if(checkCounter == 0){
					alert("<?=$Lang['SysMgr']['SchoolCalendar']['JSWarning']['SelectTheGroup']?>");
					return false;
				}
			}
		}else{
			//continuous
		}
	}	

	if(eventTypeCheckCount){
		if($("#event_title").val() != ""){		// check event title is null or not
			//continuous
		}else{
			alert("<?=$Lang['SysMgr']['SchoolCalendar']['JSWarning']['InputEventTitle']?>");
			return false;
		}
	}else{
		alert("<?=$Lang['SysMgr']['SchoolCalendar']['JSWarning']['SelectEventType'];?>");
		return false;
	}	
	
	if($("input[name=location_type]:checked").val() == 1){
		if($("#SelectedRoomID_InAddEditLayer").is(":visible") == true){
			//continuous
		}else{
			if($("#OthersLocationTb").is(":visible") == true){
				if($('input#OthersLocationTb').val() == ""){
					alert("<?=$Lang['SysMgr']['SchoolCalendar']['JSWarning']['InputLocationName'];?>");
					return false;
				}else{
					//continuous
				}
			}else{
				alert("<?=$Lang['SysMgr']['SchoolCalendar']['JSWarning']['SelectLocation'];?>");
				return false;
			}
		}
	}
	
	return true;	
}

// use AJAX to insert New School Event or Holiday
function updateNewHolidayEvent()
{
	if(checkInputFields())
	{
		var targetEventDate = $('input#event_date').val();
		var targetEventEndDate = $('input#EventEndDate').val();
		var targetEventType = $('#eventType :selected').val();
		var targetEventTitle = $('input#event_title').val();
		var targetEventTitleEng = $('input#event_titleEng').val();
		var targetLocationID = '';
		var targetEventVeune = '';
		var targetEventVenueEng = '';
		var targetClassGroupID = '';
<?php if($isKIS){ ?>
		targetClassGroupID = $('#ClassGroupID').val();
<?php } ?>
		
		if($("input[name=location_type]:checked").val() == 1){
			if($("#SelectedRoomID_InAddEditLayer").is(":visible") == true){
				targetLocationID = $("#SelectedRoomID_InAddEditLayer").val();
			}else{
				if($("#OthersLocationTb").is(":visible") == true){
					if($('input#OthersLocationTb').val() == ""){
						//continuous
					}else{
						targetEventVeune = $('input[name="OthersLocation"]').val();
						targetEventVenueEng = $('input[name="OthersLocationEng"]').val();
					}
				}
			}
		}
		
		var targetEventNature = $('input#event_nature').val();
		var targetEventNatureEng = $('input#event_natureEng').val();
		var targetEventDescription = $('textarea#event_description').val();
		var targetEventDescriptionEng = $('textarea#event_descriptionEng').val();
		var targetSkipCycleCount = $("input[name=skip_cycle_day]:checked").val();
		var targetSkipSAT = $("input[name=skip_sat]:checked").val();
		var targetSkipSUN = $("input[name=skip_sun]:checked").val();
		var targetEventStatus = 1;
		
		var SchoolYearID = $('#academic_year :selected').val();
		
		var tmpObj  = document.getElementById("targetGroupID");
		var targetGroupID = "";
		if(tmpObj){
			for(i = 0; i < tmpObj.length; i++) {
				if(tmpObj[i].value != "")
				{
					if((targetGroupID == "")){
						var delimiter = "";
					}else{
						var delimiter = ",";
					}
					targetGroupID = targetGroupID+delimiter+tmpObj[i].value;
				}
			}
		}
		
		// Warning Message if necessary
		var jsCanEdit = false;
		if ($('div#SpecialTimetableWarningDiv').is(':visible')) {
			var jsWarningText = $('span#SpecialTimetableWarningSpan').html();
			jsWarningText += '\r\n\r\n' + '<?=$Lang['SysMgr']['CycleDay']['jsWarningArr']['AddEventHoliday']?>';
			if (confirm(jsWarningText)) {
				jsCanEdit = true;
			}
		}
		else {
			jsCanEdit = true;
		}
		<?php if($sys_custom['eClassApp']['SFOC']){?>
		    var targetEventAssociation = $('input#event_association').val();
		    var targetEventAssociationEng = $('input#event_associationEng').val();
// 		    var targetEventSfocTitle = $('input#event_sfoc_title').val();
		    var targetStartTimeHour = $('#HourStart :selected').val();
		    var targetStartTimeMin = $('#MinStart :selected').val();
		    var targetEndTimeHour = $('#HourEnd :selected').val();
		    var targetEndTimeMin = $('#MinEnd :selected').val();
		    var targetEventEmail = $('input#event_email').val();
		    var targetEventContact = $('input#event_contact').val();
		    var targetEventWebsite = $('input#event_website').val();	    
// 		    var targetSportType = $("#sportType :selected").val();
		    var targetToBeDetermined = $("input[name=to_be_determine_event]:checked").val();
		<? } ?>
		
		if (jsCanEdit) {
			$('#NewHolidayEventSubmitBtn').attr('disabled',true);
			$.post(
				"ajax_school_holiday_event_add_update.php",
				{
					"EventDate":targetEventDate,
					"EventEndDate":targetEventEndDate,
					"RecordType":targetEventType,
					"ClassGroupID":targetClassGroupID,
					"Title":targetEventTitle,
					"EventTitleEng":targetEventTitleEng,
					"EventLocationID":targetLocationID,
					"EventVenue":targetEventVeune,
					"EventVenueEng":targetEventVenueEng,
					"EventNature":targetEventNature,
					"EventNatureEng":targetEventNatureEng,
					"Description":targetEventDescription,
					"DescriptionEng":targetEventDescriptionEng,
					"isSkipCycle":targetSkipCycleCount,
					"RecordStatus":targetEventStatus,
					"SchoolYearID":SchoolYearID,
					"TargetGroupID":targetGroupID,
					"isSkipSAT":targetSkipSAT,
					"isSkipSUN":targetSkipSUN
					<?php if($sys_custom['eClassApp']['SFOC']){?>
    					,"EventAssociation":targetEventAssociation,
    					"EventAssociationEng":targetEventAssociationEng,
//     					"EventSfocTitle":targetEventSfocTitle,
    					"EventStartTimeHour":targetStartTimeHour,
    					"EventStartTimeMin":targetStartTimeMin,
    					"EventEndTimeHour":targetEndTimeHour,
    					"EventEndTimeMin":targetEndTimeMin,
    					"EventEmail":targetEventEmail,
    					"EventContact":targetEventContact,
    					"EventWebsite":targetEventWebsite,
//     					"EventSportType":targetSportType,
    					"EventToBeDetermined":targetToBeDetermined					
					<? } ?>
				},
				function(responseText){
					//console.log(responseText);
					if(responseText == 1){
						msg = '<?=$Lang['General']['ReturnMessage']['AddSuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#academic_year').val(),'#Production_Calendar');
 						window.top.tb_remove();
					}else if(responseText == 0){
						msg = '<?=$Lang['General']['ReturnMessage']['AddUnsuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#academic_year').val(),'#Production_Calendar');
 						window.top.tb_remove();
					}else{
// 						console.log(responseText);
						alert("<?=$Lang['SysMgr']['SchoolCalendar']['JSWarning']['EventCannotOverTheSchoolYear'];?>");
					}
				}
			);
		}
	}
}

// AJAX - update Holiday or Event record
function updateEditHolidayEvent(CallFrom)
{
	if(checkInputFields())
	{
		var targetEventDate = $('input#event_date').val();
		var targetEventEndDate = $('input#EventEndDate').val();
		var targetEventID = $('input#event_id').val();
		var targetEventType = $('#eventType :selected').val();
		var targetEventTitle = $('input#event_title').val();
		var targetEventTitleEng = $('input#event_titleEng').val();
		var targetLocationID = '';
		var targetEventVeune = '';
		var targetEventVenueEng = '';
		var targetClassGroupID = '';
<?php if($isKIS){ ?>
		targetClassGroupID = $('#ClassGroupID').val();
<?php } ?>
		
		if($("input[name=location_type]:checked").val() == 1){
			if($("#SelectedRoomID_InAddEditLayer").is(":visible") == true){
				targetLocationID = $("#SelectedRoomID_InAddEditLayer").val();
			}else{
				if($("#OthersLocationTb").is(":visible") == true){
					if($('input#OthersLocationTb').val() == ""){
						//continuous
					}else{
						targetEventVeune = $('input#OthersLocationTb').val();
						targetEventVenueEng = $('input[name="OthersLocationEng"]').val();
					}
				}
			}
		}
		
		var targetEventNature = $('input#event_nature').val();
		var targetEventNatureEng = $('input#event_natureEng').val();
		var targetEventDescription = $('textarea#event_description').val();
		var targetEventDescriptionEng = $('textarea#event_descriptionEng').val();
		var targetSkipCycleCount = $("input[name=skip_cycle_day]:checked").val();
		var targetSkipSAT = $("input[name=skip_sat]:checked").val();
		var targetSkipSUN = $("input[name=skip_sun]:checked").val();
		var targetEventStatus = 1;
		
		var element2 = document.getElementById('ApplyToRelatedEvents');
		if(element2 == null) {
			var ApplyToRelatedEvents = 0;
		} else {
			var ApplyToRelatedEvents = 1;
		}
		
		var SchoolYearID = $('#academic_year :selected').val();
		
		var tmpObj  = document.getElementById("targetGroupID");
		var targetGroupID = "";
		if(tmpObj){
			for(i = 0; i < tmpObj.length; i++) {
				if(tmpObj[i].value != "")
				{
					if((targetGroupID == "")){
						var delimiter = "";
					}else{
						var delimiter = ",";
					}
					targetGroupID = targetGroupID+delimiter+tmpObj[i].value;
				}
			}
		}
		
		// Warning Message if necessary
		var jsCanEdit = false;
		if ($('div#SpecialTimetableWarningDiv').is(':visible')) {
			var jsWarningText = $('span#SpecialTimetableWarningSpan').html();
			jsWarningText += '\r\n\r\n' + '<?=$Lang['SysMgr']['CycleDay']['jsWarningArr']['UpdateEventHoliday']?>';
			if (confirm(jsWarningText)) {
				jsCanEdit = true;
			}
		}
		else {
			jsCanEdit = true;
		}
		<?php if($sys_custom['eClassApp']['SFOC']){?>
    	    var targetEventAssociation = $('input#event_association').val();
    	    var targetEventAssociationEng = $('input#event_associationEng').val();
//     	    var targetEventSfocTitle = $('input#event_sfoc_title').val();
    	    var targetStartTimeHour = $('#HourStart :selected').val();
		    var targetStartTimeMin = $('#MinStart :selected').val();
		    var targetEndTimeHour = $('#HourEnd :selected').val();
		    var targetEndTimeMin = $('#MinEnd :selected').val();
    	    var targetEventEmail = $('input#event_email').val();
    	    var targetEventContact = $('input#event_contact').val();
    	    var targetEventWebsite = $('input#event_website').val();	    
//     	    var targetSportType = $("#sportType :selected").val();
    	    var targetToBeDetermined = $("input[name=to_be_determine_event]:checked").val();
    	    var targetEventStartDate = $('input#EventStartDate').val();
    	    var targetEventEndDate = $('input#EventEndDate').val();
	   <? } ?>
		
		if (jsCanEdit) {
			$.post(
				"ajax_school_holiday_event_edit_update.php",
				{
					"EventID":targetEventID,
					"RecordType":targetEventType,
					"ClassGroupID":targetClassGroupID,
					"Title":targetEventTitle,
					"EventTitleEng":targetEventTitleEng,
					"EventLocationID":targetLocationID,
					"EventVenue":targetEventVeune,
					"EventVenueEng":targetEventVenueEng,
					"EventNature":targetEventNature,
					"EventNatureEng":targetEventNatureEng,
					"Description":targetEventDescription,
					"DescriptionEng":targetEventDescriptionEng,
					"isSkipCycle":targetSkipCycleCount,
					"RecordStatus":targetEventStatus,
					"ApplyToRelatedEvents":ApplyToRelatedEvents,
					"SchoolYearID":SchoolYearID,
					"EventStartDate":targetEventDate,
					"EventEndDate":targetEventEndDate,
					"TargetGroupID":targetGroupID,
					"isSkipSAT":targetSkipSAT,
					"isSkipSUN":targetSkipSUN
					<?php if($sys_custom['eClassApp']['SFOC']){?>
    					,"EventAssociation":targetEventAssociation,
    					"EventAssociationEng":targetEventAssociationEng,
//     					"EventSfocTitle":targetEventSfocTitle,
    					"EventStartTimeHour":targetStartTimeHour,
    					"EventStartTimeMin":targetStartTimeMin,
    					"EventEndTimeHour":targetEndTimeHour,
    					"EventEndTimeMin":targetEndTimeMin,
    					"EventEmail":targetEventEmail,
    					"EventContact":targetEventContact,
    					"EventWebsite":targetEventWebsite,
//     					"EventSportType":targetSportType,
    					"EventToBeDetermined":targetToBeDetermined,	
    					"EventStartDate": targetEventStartDate,	
    					"EventEndDate": targetEventEndDate				
    				<? } ?>
				},
				function(responseText){
					//console.log(responseText);
					if(responseText == 1){
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#academic_year').val(),'#Production_Calendar');
						if(CallFrom == 'Calendar')
						{
							showHolidayEventForm(targetEventDate, msg);
						}else{
							window.top.tb_remove();
						}
					}else if(responseText == 0){
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#academic_year').val(),'#Production_Calendar');
						if(CallFrom == 'Calendar')
						{
							showHolidayEventForm(targetEventDate, msg);
						}else{
							window.top.tb_remove();
						}
					} else {
						alert("<?=$Lang['SysMgr']['SchoolCalendar']['JSWarning']['EventCannotOverTheSchoolYear'];?>");
					}
				}
			);
		}
	}
}

// AJAX - Delete Holiday or Event record
function deleteHolidayEvent(EventID,targetEventDate,HaveRelatedEvent,CallFrom,HasSpecialTimetable)
{
	HasSpecialTimetable = HasSpecialTimetable || 0;
	
	var jsConfirmMsgWarningPrefix = '';
	if (HasSpecialTimetable == '1') {
		jsConfirmMsgWarningPrefix = '<?=$Lang['SysMgr']['CycleDay']['WarningArr']['DeleteHolidayOrEventWithSpecialTimetable']?>' + '\r\n\r\n';
	}

	if(HaveRelatedEvent == 1) {
		var ans = confirm(jsConfirmMsgWarningPrefix + "<?=$Lang['SysMgr']['SchoolCalendar']['JSWarning']['ConfirmDeleteRelatedEvents'];?>");
		var deleteRelatedEvents = 1;
	} else {
		var ans = confirm(jsConfirmMsgWarningPrefix + "<?=$Lang['SysMgr']['SchoolCalendar']['JSWarning']['ConfirmDelete']?>");
		var deleteRelatedEvents = 0;
	}
	
		if(ans){
			$.post(
					"ajax_school_holiday_event_delete.php",
					{
						"EventID":EventID,
						"DeleteRelatedEvent":deleteRelatedEvents
					},
					function(responseText){
						if(responseText == 1){
							msg = '<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#academic_year').val(),'#Production_Calendar');
							if(CallFrom == 'Calendar')
							{
								showHolidayEventForm(targetEventDate, msg);
							}else{
								window.top.tb_remove();
							}
						}else{
							msg = '<?=$Lang['General']['ReturnMessage']['DeleteUnsuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#academic_year').val(),'#Production_Calendar');
							if(CallFrom == 'Calendar')
							{
								showHolidayEventForm(targetEventDate, msg);
							}else{
								window.top.tb_remove();
							}
						}
					}
				);
		}else{
			return false;
		}
	
}

// AJAX - reload the page (used after add/edit/delete)
function reloadMainContent(schoolYearID,layer)
{
	Block_Document();
	$.post(
		"ajax_calendar_view.php",
		{
			"targetSchoolYearID":schoolYearID
		},
		function(responseText){
			$('#main_body').html(responseText);
			UnBlock_Document();
			initThickBox();
		}
	);
}

// js function - control which div / layer show
function js_LocationTypeSelection(val)
{
	if(val == 1){
		$("#location_selection1").show();
		$("#location_selection2").hide();
		$("#FloorSelectionTr_InAddEditLayer").show();
		$("#RoomSelectionTr_InAddEditLayer").show();

	}else{
		$("#location_selection1").hide();
		$("#location_selection2").hide();
		$("#FloorSelectionTr_InAddEditLayer").hide();
		$("#RoomSelectionTr_InAddEditLayer").hide();
	}
}

function jsShowMoreEvent(val, month)
{
	LayerClassName = month+"DetailEventRow";

	if(val == 1){
		$("."+LayerClassName).show();
		$("#MoreEventLayer"+month).hide();
		$("#HideEventLayer"+month).show();
	}else{
		$("."+LayerClassName).hide();
		$("#MoreEventLayer"+month).show();
		$("#HideEventLayer"+month).hide();
	}
}

function jsIsGroupEvent(val)
{
	if(val == 2)
	{
		$(".targetEventGroup").show();
	}else{
		$(".targetEventGroup").hide();
	}
}

function jQueryAddGroup(fromObjName, toObjName)
{
	fromObjName = "#"+fromObjName;
	toObjName = "#"+toObjName;
	return !$(fromObjName+' option:selected').remove().appendTo(toObjName);
}

function jQueryRemoveGroup(fromObjName, toObjName)
{
	fromObjName = "#"+fromObjName;
	toObjName = "#"+toObjName;
	return !$(fromObjName+' option:selected').remove().appendTo(toObjName);
}

function importHolidayEvent()
{
	var AcademicYearID = $('#academic_year').val();
	window.location="import_holidays_events.php?AcademicYearID="+AcademicYearID;
}

function exportHolidayEvent()
{
	var AcademicYearID = $('#academic_year').val();
	window.location="export_holidays_events.php?AcademicYearID="+AcademicYearID<?=$isKIS?'+"&ClassGroupID="+$("#class_group_id").val();':''?>;
}

function js_Check_Special_Timetable_Warning_Display(jsStartDate, jsEndDate) {
	$.post(
		"ajax_validate.php", 
		{ 
			Action: 'Check_Affect_Special_Timetable_Settings',
			StartDate: jsStartDate,
			EndDate: jsEndDate
		},
		function(ReturnData)
		{
			if (ReturnData == '1') {
				$('div#SpecialTimetableWarningDiv').show();
			}
			else {
				$('div#SpecialTimetableWarningDiv').hide();
			}
		}
	);
}

function changeClassGroup(targetClassGroupId)
{
	var schoolYearID = $('#academic_year').val();
	ChangeSchoolYear(1, schoolYearID);
}


function FormSubmitCheck(obj)
{
	if(!check_text(obj.Year, "<?php echo $i_alert_pleasefillin.$eEnrollmentMenu['cat_title']; ?>.")) 
		return false;
	if(!check_text(obj.LocationChi, "<?php echo $i_alert_pleasefillin.$eEnrollmentMenu['cat_title']; ?>.")) 
		return false;
	if(!check_text(obj.LocationEng, "<?php echo $i_alert_pleasefillin.$eEnrollmentMenu['cat_title']; ?>.")) 
		return false;
	if(!check_text(obj.NameOfAthleteChi, "<?php echo $i_alert_pleasefillin.$eEnrollmentMenu['cat_title']; ?>.")) 
		return false;
	if(!check_text(obj.NameOfAthleteEng, "<?php echo $i_alert_pleasefillin.$eEnrollmentMenu['cat_title']; ?>.")) 
		return false;

	obj.submit();
}
</script>