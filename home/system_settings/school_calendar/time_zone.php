<?php
## using by : yat

############# Change Log [Start] ################
#
#	Date:	2010-06-21	YatWoon
#			add online help button
#
# Date: 2010-01-13 Kenneth Chung
#   - Add confirm message before delete time zone
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$AcademicYearID = $_REQUEST['AcademicYearID'];

$linterface = new interface_html();
$lcycleperiods = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();
$lfcm = new form_class_manage();
$libcalevent = new libcalevent2007();

#############################################
## System Checking 
## Step 1 : check finish academic year setting or not 
##				IsAcademicYearSet() : 1 = true, 0= false
## Step 2 : check current date is in term or not
##				IsCurrentDateInTerm() : 1 = true, 0 = false
#############################################
if(!$lcycleperiods->IsAcademicYearSet())
{
	$empty_msg = $Lang['SysMgr']['SchoolCalendar']['FieldTitle']['AcademicYearIsNotSet'];
	$msg = urlencode($empty_msg);
}
if(!$lcycleperiods->IsCurrentDateInTerm())
{
	$not_in_term_msg = $Lang['SysMgr']['SchoolCalendar']['FieldTitle']['CurrentDateNotInTerm'];
	$msg = urlencode($not_in_term_msg);
}
## End checking ##

$CurrentPageArr['SchoolCalendar'] = 1;
### Title ###
$TAGS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['SettingTitle'],"index.php",0);
$TAGS_OBJ[] = array($Lang['SysMgr']['CycleDay']['SettingTitle'],"time_zone.php",1);
$TAGS_OBJ[] = array($Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearSetting'],"academic_year.php",0);
$MODULE_OBJ['title'] = $Lang['SysMgr']['SchoolCalendar']['ModuleTitle'];

# online help button
//$onlineHelpBtn = gen_online_help_btn_and_layer('school_settings','time_zone');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

if ($ReturnMsgKey == '') {
	$returnMsg = urldecode($msg);
}
else {
	$returnMsg = $Lang['SysMgr']['CycleDay']['ReturnMsg'][$ReturnMsgKey];
}
$linterface->LAYOUT_START($returnMsg); 
?>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

<?=$lcycleperiods_ui->getCycleDaysCalendarPreview($AcademicYearID);?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

<script type="text/javascript">
var data_firstday_by_type = new Array();
	data_firstday_by_type[0] = new Array();
	data_firstday_by_type[1] = new Array();
	data_firstday_by_type[2] = new Array();
	<?
	for ($i=0; $i<26; $i++)
	{
	     ?>
	     data_firstday_by_type[0][data_firstday_by_type[0].length] = Array(<?=$i?>,"<?=$lcycleperiods->array_numeric[$i]?>");
	     data_firstday_by_type[1][data_firstday_by_type[1].length] = Array(<?=$i?>,"<?=$lcycleperiods->array_alphabet[$i]?>");
	     data_firstday_by_type[2][data_firstday_by_type[2].length] = Array(<?=$i?>,"<?=$lcycleperiods->array_roman[$i]?>");
	     <?
	}
	?>

function ChangeSchoolYear(schoolYearID)
{
	Block_Document()
	$.post(
		"ajax_change_preview.php",
		{
			"targetSchoolYearID":schoolYearID
		},
		function(responseText){
			$('#main_body').html(responseText);
			UnBlock_Document();
			initThickBox();
		}
	);
}

function gotoPage(page)
{
	var url = page;
	self.location = url;
}

function reloadMainContent(schoolYearID,layer)
{
	Block_Document();
	$.post(
		"ajax_change_preview.php",
		{
			"targetSchoolYearID":schoolYearID
		},
		function(responseText){
			$('#main_body').html(responseText);
			UnBlock_Document();
			initThickBox();
		}
	);
}

function showAdditionalInfo(val)		//control which <div> show in add period form
{
	if(val == 1){
		$("#normal_additional_info").show();
		$("#targetPeriodType0").attr("checked",false);
	}else if(val == 0){
		$("#normal_additional_info").hide();
		$("#targetPeriodType1").attr("checked",false);
	}else if(val == -1){
		$("#normal_additional_info").show();
		$("#targetPeriodType0").attr("checked",false);
		$("#targetPeriodType1").attr("checked",true);
	}else if(val == -2){
		$("#normal_additional_info").hide();
		$("#targetPeriodType0").attr("checked",true);
		$("#targetPeriodType1").attr("checked",false);
	}
}

function newDefaultPeriodForm2(startDate, endDate, periodType)
{
	wordXmlHttp = GetXmlHttpObject();
	var endDate = endDate || "";
	var periodType  = (periodType  == null) ? 1 : periodType ;
	
	var yearID = $('#academic_year').val();
	var postContent = 'AcademicYearID='+yearID;
	postContent += '&StartDate='+startDate;
	postContent += '&EndDate='+endDate;
	postContent += '&PeriodType='+periodType;
	
	var url = 'ajax_default_period_add.php';
	
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
			document.getElementById('TB_ajaxContent').innerHTML = wordXmlHttp.responseText;
			document.getElementById('TB_ajaxWindowTitle').innerHTML = "<?=$Lang['SysMgr']['CycleDay']['NewPeriod'];?>";
			
			$('#bgColorCode').colorPicker();
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_start').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_end').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
		}
	};
	wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);	
}

function newDefaultPeriodForm()//create a new period input form
{
	wordXmlHttp = GetXmlHttpObject();
    
	var yearID = $('#academic_year').val();
	var postContent = 'AcademicYearID='+yearID;
	var url = 'ajax_default_period_add.php';
	
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
			document.getElementById('TB_ajaxContent').innerHTML = wordXmlHttp.responseText;
			
			$('#bgColorCode').colorPicker();
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_start').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_end').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
		}
	};
	wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}

function changeFirstDaySelect()
{
	type = document.getElementById('CycleType').value;
	max_num = document.getElementById('PeriodDays').value;
	obj = document.getElementById('FirstDay');
	
	var current = obj.options.length;
	
	for (var j=current;j>0;j--) obj.options[j-1] = null;
	for (var i=0;i<max_num;i++)
	{
		obj.options[obj.options.length] = new Option(data_firstday_by_type[type][i][1],data_firstday_by_type[type][i][0]);
	}
}

function checkInputFields()		//input period form's validation
{
	if($('input#period_start').val()=="")
	{
		$("#PeriodStart_WarningLayer").html("<font color='#FF0000'><?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodStartEmpty'];?></font>");
		$("#PeriodStart_WarningLayer").show();
		$("#period_start").focus();
		return false;
	}
	else
	{
		if($('input#period_end').val()=="")
		{
			$("#PeriodEnd_WarningLayer").html("<font color='#FF0000'><?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodEndEmpty'];?></font>");
			$("#PeriodEnd_WarningLayer").show();
			$("#period_end").focus();
			return false;
		}
		else
		{
			if(check_date_without_return_msg(document.getElementById('period_start')))
			{
				if(check_date_without_return_msg(document.getElementById('period_end')))
				{
					var boolCheckPoint1 = 1;
				}
				else
				{
					$("#PeriodEnd_WarningLayer").html("<font color='#FF0000'><?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodEndInvalid'];?></font>");
					$("#PeriodEnd_WarningLayer").show();
					$("#period_end").focus();
					return false;
				}
			}else{
				$("#PeriodStart_WarningLayer").html("<font color='#FF0000'><?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodStartInvalid'];?></font>");
				$("#PeriodStart_WarningLayer").show();
				$("#period_start").focus();
				return false;
			}
			
			
		}
	}
	
	if(boolCheckPoint1 == 1)
	{
		var result = compareDate2(document.getElementById('period_end').value,document.getElementById('period_start').value);
		if((result == 1) || (result == 0))
		{
			return true;
		}
		else
		{
			alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['StartPeriodLargerThenEndPeriod'];?>");
			return false;
		}
	}		
}

function updateNewPeriod()		// use AJAX to insert a new period
{
	// get all the input variable
	var targetPeriodStart = $('input#period_start').val();
	var targetPeriodEnd = $('input#period_end').val();
	var element = document.getElementById('targetPeriodType1');
	var targetPeriodType = 0;
	if(element.checked == true){
		var targetPeriodType = 1;
	}
	var targetCycleType = $('#CycleType :selected').val();
	var targetPeriodDays = $('#PeriodDays :selected').val();
	var targetFirstDay = $('#FirstDay :selected').val();
	//var element = document.getElementById('satCount');
	var targetSatCount = 0;
	//if(element.checked == true){
	//	var targetSatCount = 1;
	//}
	
	var arrSkipOnWeekday = new Array();
	$.each($("input[name='SkipOnWeekDay[]']:checked"), function() {
			arrSkipOnWeekday.push($(this).val());
		}
	);
	
	var bgColorCode = $("#bgColorCode").val();
	
	var TimetableTemplate = $("#TimetableTemplate :selected").val();
	
	if(checkInputFields())
	{
		$.post(
			"ajax_period_add_update.php",
			{
				"PeriodStart":targetPeriodStart,
				"PeriodEnd":targetPeriodEnd,
				"PeriodType":targetPeriodType,
				"CycleType":targetCycleType,
				"PeriodDays":targetPeriodDays,
				"FirstDay":targetFirstDay,
				"SatCount":targetSatCount,
				"SkipOnWeekday":arrSkipOnWeekday.toString(),
				"SchoolYearID":$('#academic_year').val(),
				"ColorCode":bgColorCode,
				"TimetableTemplate":TimetableTemplate
			},
			function(responseText){
				if(responseText != 0 && responseText != -1 && responseText != -2){
					Get_Return_Message(responseText);
					reloadMainContent($('#academic_year').val(),'#Production_Calendar');
					window.top.tb_remove();
				}else{
					newDefaultPeriodForm2(targetPeriodStart,targetPeriodEnd,targetPeriodType);
					if(responseText == 0)
						alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodOverlapped'];?>");
					if(responseText == -1)
						alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodIsNotInTheSelectedSchoolYear'];?>");
					if(responseText == -2)
						alert("<?=$Lang['SysMgr']['SchoolCalendar']['JSWarning']['DateRangeNotInSameTerm'];?>");
				}
			}
		);
	}
}

function editPeriodRange(PeriodID)
{
	js_Hide_Option_Layer('EditOptionDiv');
	
	wordXmlHttp = GetXmlHttpObject();
	
	var url = 'ajax_period_edit.php';
	var postContent = 'PeriodID='+PeriodID;
	
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
			document.getElementById('TB_ajaxContent').innerHTML = wordXmlHttp.responseText;
			document.getElementById('TB_ajaxWindowTitle').innerHTML = "<?=$Lang['SysMgr']['CycleDay']['EditPeriod'];?>";
			//Init_JEdit_Input("div.jEditInput");
			
			$('#bgColorCode').colorPicker();
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_start').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_end').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
		}
	};
	wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}

function updateEditPeriod(SchoolYearID){
	wordXmlHttp = GetXmlHttpObject();
	var targetSchoolYearID = SchoolYearID;
	var targetPeriodID = $('input#period_id').val();
	var targetPeriodStart = $('input#period_start').val();
	var targetPeriodEnd = $('input#period_end').val();
	
	var element = document.getElementById('targetPeriodType1');
	var targetPeriodType = 0;
	if(element.checked == true){
		var targetPeriodType = 1;
	}
	
	var targetCycleType = $('#CycleType :selected').val();
	
	var targetPeriodDays = $('#PeriodDays :selected').val();
	if(element.checked == false){
		// weekdays => change period days to 6 automatically
		targetPeriodDays = 6;
	}
	
	var targetFirstDay = $('#FirstDay :selected').val();
	
	var element = document.getElementById('satCount');
	var targetSatCount = 0;
	//if(element.checked == true){
	//	var targetSatCount = 1;
	//}
	
	var arrSkipOnWeekday = new Array();
	$.each($("input[name='SkipOnWeekDay[]']:checked"), function() {
			arrSkipOnWeekday.push($(this).val());
		}
	);
	
	var bgColorCode = $("#bgColorCode").val();
	
	var TimetableTemplate = $("#TimetableTemplate :selected").val();
	if(checkInputFields())
	{
		$.post(
			"ajax_period_edit_update.php",
			{
				"SchoolYearID":targetSchoolYearID,
				"PeriodID":targetPeriodID,
				"PeriodStart":targetPeriodStart,
				"PeriodEnd":targetPeriodEnd,
				"PeriodType":targetPeriodType,
				"CycleType":targetCycleType,
				"PeriodDays":targetPeriodDays,
				"FirstDay":targetFirstDay,
				"SatCount":targetSatCount,
				"SkipOnWeekday":arrSkipOnWeekday.toString(),
				"ColorCode":bgColorCode,
				"TimetableTemplate":TimetableTemplate
			},
			function(responseText){
				if(responseText != 0 && responseText != -1 && responseText != -2){
					Get_Return_Message(responseText);
					reloadMainContent(targetSchoolYearID,'#edit_period');
					window.top.tb_remove();
				}else{
					editPeriodRange(targetPeriodID);
					if(responseText == 0)
						alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodOverlapped'];?>");
					if(responseText == -1)
						alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodIsNotInTheSelectedSchoolYear']?>");
					if(responseText == -2)
						alert("<?=$Lang['SysMgr']['SchoolCalendar']['JSWarning']['DateRangeNotInSameTerm'];?>");
				}
			}
		);
	}
}

function deletePeriod(periodID)
{
	var jsConfirmMsg = ($('input#HasSpecialTimetableSettings').val() == '1')? '<?=$Lang['SysMgr']['CycleDay']['JSWarning']['ConfirmDeleteWithSpecialTimetable']?>' : '<?=$Lang['SysMgr']['CycleDay']['JSWarning']['ConfirmDelete']?>';
	if (confirm(jsConfirmMsg)) {
		Block_Thickbox();
		$.post(
				"ajax_period_remove.php",
				{
					"PeriodID":periodID
				},
				function(responseText){
					Get_Return_Message(responseText);
					reloadMainContent($('#academic_year').val(),'#Production_Calendar');
					window.top.tb_remove();
					UnBlock_Thickbox();
				}
			);
	}
}

function loadTimetableSelection(SchoolYearID,PeriodStart,NumOfCycleDay){
	$("#timetable_layer").hide();
	
	var element = document.getElementById('targetPeriodType1');
	var targetPeriodType = 0;
	if(element.checked == true){
		var targetPeriodType = 1;
	}
	
	$.post(
			"ajax_reload_timetable_selection.php",
			{
				SchoolYearID : SchoolYearID,
				PeriodStart : PeriodStart,
				PeriodType : targetPeriodType,
				NumOfCycleDay : NumOfCycleDay
			},
			function(responseText){
				$("#timetable_layer").html(responseText);
				$("#timetable_layer").show();
			}
		);
}

function jsClearWarningMsg(DivName)
{
	if($("#"+DivName).html() != "")
	{
		$("#"+DivName).html('');
		$("#"+DivName).hide();
	}
}



function js_Show_Option_Layer(jsDate, jsTimezoneID, jsClickedObjID, jsLayerDivID)
{
	js_Hide_Option_Layer(jsLayerDivID);
	
	jsAction = 'ShowItemDescription';
	jsOffsetLeft = 0;
	jsOffsetTop = -10;
	
	$('div#EditOptionContentDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	js_Change_Layer_Position(jsLayerDivID, jsClickedObjID, jsOffsetLeft, jsOffsetTop);
	MM_showHideLayers(jsLayerDivID, '', 'show');
			
	$('div#EditOptionContentDiv').load(
		"ajax_reload.php", 
		{ 
			Action: 'Edit_Timezone_Option_Layer',
			TimezoneID: jsTimezoneID,
			Date: jsDate
		},
		function(returnString)
		{
			initThickBox();
			$('div#EditOptionContentDiv').css('z-index', '999');
		}
	);
}

function js_Hide_Option_Layer(jsLayerDivID)
{
	MM_showHideLayers(jsLayerDivID, '', 'hide');
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function js_Change_Layer_Position(jsLayerDivID, jsClickedObjID, jsOffsetLeft, jsOffsetTop) 
{		
	var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') - jsOffsetLeft;
	var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') - jsOffsetTop;
	
	document.getElementById(jsLayerDivID).style.left = posleft + "px";
	document.getElementById(jsLayerDivID).style.top = postop + "px";
	document.getElementById(jsLayerDivID).style.visibility = 'visible';
}

function js_Go_Special_Timetable_Settings(jsTimezoneID, jsSpecialTimetableSettingsID) {
	jsSpecialTimetableSettingsID = jsSpecialTimetableSettingsID || '';
	var jsAcademicYearID = $('select#academic_year').val();
	
	window.location = 'special_timetable_settings.php?AcademicYearID=' + jsAcademicYearID + '&TimezoneID=' + jsTimezoneID + '&SpecialTimetableSettingsID=' + jsSpecialTimetableSettingsID;
}
</script>  