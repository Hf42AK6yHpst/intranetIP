<?php
// Using : 

############# Change Log [Start] ################
# 	Date:	2016-01-04 Omas [ip.2.5.7.1.1]
#			modified js function insertUpdateTerm(), js_Show_Term_Add_Edit_Layer() - add support to new edit term logic
#			Logic as : current term => only end date 
#					   future term	=> can change term name,start date,end date (delete only for term not in use)
#					   past term	=> locked (as previous)
#
#	Date:	2015-09-15 Ivan [U84654] [ip.2.5.6.10.1]
#			enable timezone settings for KIS if purchased eBooking module
#
#	Date:	2015-03-25	Omas
#			Improvement: add js to disable submit button avoid double submit when creating new term, new school year
#
#	Date:	2011-08-03	YatWoon
# 			update loadAcademicYearTable(), change the important note display
#
#	Date:	2010-06-21	YatWoon
#			add online help button
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$lcycleperiods = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();
$lfcm = new form_class_manage();
$libcalevent = new libcalevent2007();

$CurrentPageArr['SchoolCalendar'] = 1;

### Title ###
if(!($_SESSION["platform"]=="KIS" && $stand_alone['eAdmission'])&&!(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""))
{
	$TAGS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['SettingTitle'],"index.php",0);
}

//if ($_SESSION["platform"]!="KIS"){
//    $TAGS_OBJ[] = array($Lang['SysMgr']['CycleDay']['SettingTitle'],"time_zone.php",0);
//}
$showTimezone = true;
if (($_SESSION["platform"]=="KIS" && !$plugin['eBooking'])||(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!="")){
	// kis without eBooking => hide timezone settings
	$showTimezone = false;
}
if ($showTimezone) {
	$TAGS_OBJ[] = array($Lang['SysMgr']['CycleDay']['SettingTitle'],"time_zone.php",0);
}

$TAGS_OBJ[] = array($Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearSetting'],"academic_year.php",1);
$MODULE_OBJ['title'] = $Lang['SysMgr']['SchoolCalendar']['ModuleTitle'];

# online help button
//$onlineHelpBtn = gen_online_help_btn_and_layer('school_settings','year_term');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

$btnAddAcademicYear = str_replace("'","\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Add'], "button", "js_Create_New_AcademicYear();", "NewAcademicYearSubmitBtn"));
$btnCancelAcademicYear = str_replace("'","\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "js_Delete_AcademicYear_Row();", "NewAcademicYearCancelBtn"));

$returnMsg = urldecode($msg);
$linterface->LAYOUT_START($returnMsg);
?>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

<?=$lcycleperiods_ui->getAcademicYearTermInfo();?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

<script type="text/javascript">

// $(document).ready(function() {
// 	ShowImportantNote("ShowImportantNote","DIV_ShowImportantNoteLink");
// });

function js_Init_DND_Table() {
	
	/*** Academic Year Edit Form ***/
	var JQueryObj1 = $("#AcademicYearTable");
	JQueryObj1.tableDnD({
		onDrop: function(table, row) {
			var rows = table.tBodies[0].rows;
			var RecordOrder = "";
			for (var i=0; i<rows.length; i++) {
				if (rows[i].id != "")
				RecordOrder += rows[i].id+",";
			}
			//alert("RecordOrder = " + RecordOrder);
			
			$.post(
				"ajax_reorder_acadmeic_year.php", 
				{ 
					AcademicYearID : $("#AcademicYearID").val(),
					DisplayOrderString: RecordOrder
				},
				function(ReturnData)
				{
					// Reload the building info in the thickbox
					js_Show_AcademicYear_Add_Edit_Layer(ReturnData);
					js_ReloadMainContent();	
					// Show system messages
					//Show_System_Message("update", ReturnData);
					
					//$('#debugArea').html(ReturnData);
				}
			);
		},
		onDragStart: function(table, row) {
			//$('#debugArea').html("Started dragging row "+row.id);
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
	/*** End of Building Edit Form ***/
}

function js_Init_JEdit()
{
	/************ Add/Edit Building Info ************/	
	// AcademicYearTitleCh
	$('div.jEditTitleCh').editable( 
		function(updateValue, settings) 
		{
			var thisDivID = $(this).attr("id");
			var idArr = thisDivID.split('_');
			var divID = idArr[0];
			var targetAcademicYearID = idArr[1];
						
			updateValue = Trim(updateValue);
			
			if ($('#ChTitleWarningDiv_' + targetAcademicYearID).is(':visible') || updateValue == '')
			{
				// not vaild
				$('#ChTitleWarningDiv_' + targetAcademicYearID).hide();
				$(this)[0].reset();
			}
			else
			{
				if (updateValue=='')
				{
					// not vaild
					$(this)[0].reset();
				}
				else
				{
					$.post(
						"ajax_update_academic_year_title.php", 
						{ 
							RecordType: "Chinese",
							targetAcademicYearID: targetAcademicYearID,
							UpdateValue: encodeURIComponent(updateValue)
						},
						function(ReturnData)
						{
							js_Show_AcademicYear_Add_Edit_Layer(ReturnData);
							js_ReloadMainContent();
							// Show system messages
							//Show_System_Message("update", ReturnData);							
						}
					);
				}
			}
		}, 
		{
			tooltip	: "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
			event	: "click",
			onblur	: "submit",
			type	: "text",     
			style	: "display:inline",
			height	: "20px"
		}
	);
	$('div.jEditTitleCh').keyup( function() {
		var inputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var selfID = idArr[1];
		var recordType = '';
			
		js_Title_Validation('Chinese', inputValue, selfID, 'ChTitleWarningDiv_'+selfID);
	});
	$('div.jEditTitleCh').click( function() {
		var inputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var selfID = idArr[1];
		var recordType = '';
		js_Title_Validation('Chinese', inputValue, selfID, 'ChTitleWarningDiv_'+selfID);
	});
	
	// AcademicYearTitleEn
	$('div.jEditTitleEn').editable( 
		function(updateValue, settings) 
		{
			var thisDivID = $(this).attr("id");
			var idArr = thisDivID.split('_');
			var divID = idArr[0];
			var targetAcademicYearID = idArr[1];
			
			updateValue = Trim(updateValue);

			if ($('#EnTitleWarningDiv_' + targetAcademicYearID).is(':visible') || updateValue == '')
			{
				// not vaild
				$('#EnTitleWarningDiv_' + targetAcademicYearID).hide();
				$(this)[0].reset();
			}
			else
			{
				if (updateValue=='')
				{
					// not vaild
					$(this)[0].reset();
				}
				else
				{
					$.post(
						"ajax_update_academic_year_title.php", 
						{ 
							RecordType: "English",
							targetAcademicYearID: targetAcademicYearID,
							UpdateValue: encodeURIComponent(updateValue)
						},
						function(ReturnData)
						{
							js_Show_AcademicYear_Add_Edit_Layer(ReturnData);
							js_ReloadMainContent();
							// Show system messages
							//Show_System_Message("update", ReturnData);
						}
					);
				}
			}
		}, 
		{
			tooltip	: "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
			event 	: "click",
			onblur 	: "submit",
			type 	: "text",     
			style	: "display:inline",
			height	: "20px"
		}
	); 
	$('div.jEditTitleEn').keyup( function() {
		var inputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var selfID = idArr[1];
		var recordType = '';
		
		js_Title_Validation('English', inputValue, selfID, 'EnTitleWarningDiv_'+selfID);
	});
	$('div.jEditTitleEn').click( function() {
		var inputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var selfID = idArr[1];
		var recordType = '';
			
		js_Title_Validation('English', inputValue, selfID, 'EnTitleWarningDiv_'+selfID);
	});
	/************ End of Add/Edit Building Info ************/
}

function js_Change_School_Year(schoolYearID)
{
	Block_Document();
	$.post(
		"ajax_change_academic_year_info.php",
		{
			"targetSchoolYearID":schoolYearID
		},
		function(responseText){
			UnBlock_Document();
			$('#academic_table').html(responseText);
			initThickBox();
		}
	);
}

function js_Show_AcademicYear_Add_Edit_Layer(returnMsg)
{
	var returnMsg = returnMsg || "";
	$.post(
		"ajax_school_academic_year_layer.php",
		{
		},
		function(responseText){
			$('#TB_ajaxContent').html(responseText);
			Get_Return_Message(returnMsg);
			js_Init_JEdit();
			js_Init_DND_Table();
		}
	);
}

function js_ReloadMainContent()
{
	$.post(
		"ajax_reload_academic_year.php",
		{
		},
		function(responseText){
			$('#academic_table').html(responseText);
			initThickBox()
			js_Init_JEdit();
			js_Init_DND_Table();
		}
	);
}

function js_Delete_AcademicYear(AcademicYearID)
{
	$.post(
		"ajax_delete_academic_year.php",
		{
			AcademicYearID : AcademicYearID
		},
		function(responseText){
			js_Show_AcademicYear_Add_Edit_Layer(responseText);
			js_ReloadMainContent();
		}
	);
}

function js_Add_AcademicYear_Row() {
	if (!document.getElementById('GenAddAcademicYearRow')) {
		var TableBody = document.getElementById("AcademicYearTable").tBodies[0];
		var RowIndex = document.getElementById("AddAcademicYearRow").rowIndex - 1;
		//var RowIndex = 1;
		var NewRow = TableBody.insertRow(RowIndex);
		NewRow.id = "GenAddAcademicYearRow";
		NewRow.style.textAlign = "left";
				
		// Title (Eng)
		var NewCell0 = NewRow.insertCell(0);
		var tempInput = '<input type="text" name="AcademicYearTitleCh" id="AcademicYearTitleCh" value="" class="textbox"/>';
			tempInput += '<div id="ChTitleWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell0.innerHTML = tempInput;
		
		// Title (Chi)
		var NewCell1 = NewRow.insertCell(1);
		var tempInput = '<input type="text" name="AcademicYearTitleEn" id="AcademicYearTitleEn" value="" class="textbox"/>';
			tempInput += '<div id="EnTitleWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell1.innerHTML = tempInput;
		
		// Add and Reset Buttons
		var NewCell2 = NewRow.insertCell(2);
		var temp = '<?=$btnAddAcademicYear?>';
		temp += '&nbsp;';
		temp += '<?=$btnCancelAcademicYear?>';
		NewCell2.innerHTML = temp;
		
		$('#AcademicYearTitleCh').focus();
		
		// Code validation
		//js_Add_KeyUp_Code_Checking("Floor", "FloorCode", "CodeWarningDiv_New");
		js_Add_KeyUp_AcademicYearTitle_Checking('English', "AcademicYearTitleEn", "EnTitleWarningDiv_New");
		js_Add_KeyUp_AcademicYearTitle_Checking('Chinese', "AcademicYearTitleCh", "ChTitleWarningDiv_New");
	}
}
function js_Title_Validation(RecordType, InputValue, TargetID, WarningDivID)
{
	$.post(
		"ajax_validate_title.php", 
		{
			RecordType: RecordType,
			InputValue: InputValue,
			TargetID: TargetID
		},
		function(ReturnData)
		{
			if (ReturnData == "")
			{
				// valid code
				$('#' + WarningDivID).hide();
			}
			else
			{
				// invalid code => show warning
				$('#' + WarningDivID).html(ReturnData);
				$('#' + WarningDivID).show();
				return false;
				
				//$('#debugArea').html('aaa ' + ReturnData);
			}
		}
	);
}
function js_Add_KeyUp_AcademicYearTitle_Checking(RecordType, ListenerObjectID, WarningDivID, AcademicYearID)
{
	// Code Validation
	$('#' + ListenerObjectID).keyup( function() {
		var inputValue = $(this).val();
		var idArr = $(this).attr('id').split('_');
		var targetID = idArr[1];
		
		if (targetID == null)
			targetID = '';
			
		if (!isNaN(AcademicYearID))
			targetID = AcademicYearID;
			
		js_Title_Validation(RecordType, inputValue, targetID, WarningDivID);
	});
}

function js_Delete_AcademicYear_Row() 
{
	// Delete the Row
	var TableBody = document.getElementById("AcademicYearTable").tBodies[0];
	var RowIndex = document.getElementById("GenAddAcademicYearRow").rowIndex-1;
	TableBody.deleteRow(RowIndex);
}

function js_Create_New_AcademicYear()
{
	var TitleCh = encodeURIComponent(Trim($('#AcademicYearTitleCh').val()));
	var TitleEn = encodeURIComponent(Trim($('#AcademicYearTitleEn').val()));
	
	if($('#AcademicYearTitleCh').val() == "")
	{
		$("#ChTitleWarningDiv_New").html("*<?=$Lang['SysMgr']['AcademicYear']['JSWarning']['ChineseAcademicYearNameIsEmpty']?>");
		$("#ChTitleWarningDiv_New").show();
		$('#AcademicYearTitleCh').focus();
		return false;
	}
	
	if($('#AcademicYearTitleEn').val() == "")
	{
		$("#EnTitleWarningDiv_New").html("*<?=$Lang['SysMgr']['AcademicYear']['JSWarning']['EnglishAcademicYearNameIsEmpty']?>");
		$("#EnTitleWarningDiv_New").show();
		$('#AcademicYearTitleEn').focus();
		return false;
	}
	
	$('#NewAcademicYearSubmitBtn').attr('disabled',true);
	$('#NewAcademicYearCancelBtn').attr('disabled',true);

	$.post(
		"ajax_validate_title.php", 
		{
			RecordType: "Both",
			InputValueEn: $('#AcademicYearTitleEn').val(),
			InputValueCh: $('#AcademicYearTitleCh').val(),
			TargetID: ""
		},
		function(ReturnData)
		{
			if (ReturnData == "")
			{
				$.post(
					"ajax_new_academic_year.php",
					{
						YearNameEN : $('#AcademicYearTitleEn').val(),
						YearNameB5 : $('#AcademicYearTitleCh').val()
					},
					function(responseText){
						js_Show_AcademicYear_Add_Edit_Layer(responseText);
						js_ReloadMainContent();
					}
				);
			}
		}
	);
}

function js_Show_Term_Add_Edit_Layer(AcademicYearID,YearTermID,TermTitleCh,TermTitleEn,TermStartDate,TermEndDate,RecordType)
{
	//alert("AAA");
	var TermTitleCh = TermTitleCh || "";
	var TermTitleEn = TermTitleEn || "";
	var TermStartDate = TermStartDate || "";
	var TermEndDate = TermEndDate || "";
	var RecordType = RecordType || "";
	
	$.post(
		"ajax_show_term_add_edit_layer.php",
		{
			AcademicYearID : AcademicYearID,
			YearTermID : YearTermID,
			TermTitleCh : TermTitleCh,
			TermTitleEn : TermTitleEn,
			TermStartDate : TermStartDate,
			TermEndDate : TermEndDate,
			RecordType : RecordType
		},
		function(responseText){
			$('#TB_ajaxContent').html(responseText);
			
			if(RecordType != 'current'){
				$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
				$('#TermStartDate').datepick({
					dateFormat: 'yy-mm-dd',
					dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
					changeFirstDay: false,
					firstDay: 0
				});
			}
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#TermEndDate').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
			
			js_Init_JEdit();
			js_Init_DND_Table();
			$('#TermTitleCh').focus();
		}
	);
}

function insertUpdateTerm(EditMode,AcademicYearID,YearTermID,RecordType)
{
	var YearTermID = YearTermID || "";
	/* for distinguish edit mode current and future term*/
	var RecordType = RecordType || "";
	var pass = 0;
	
	/* check chinese term name is null */
	if($('#TermTitleCh').val() == ""){
		$('#TermTitleCh_WarningMsgLayer').html("<font color='#FF0000'>*<?=$Lang['SysMgr']['AcademicYear']['JSWarning']['ChineseTermNameIsEmpty'];?></font>");
		$('#TermTitleCh_WarningMsgLayer').show();
		$('#TermTitleCh').focus();
		return false;
	}
	/* check english term name is null */
	if($('#TermTitleEn').val() == ""){
		$('#TermTitleEn_WarningMsgLayer').html("<font color='#FF0000'>*<?=$Lang['SysMgr']['AcademicYear']['JSWarning']['EnglishTermNameIsEmpty'];?></font>");
		$('#TermTitleEn_WarningMsgLayer').show();
		$('#TermTitleEn').focus();
		return false;
	}
	/* check start date is null */
	if($('#TermStartDate').val() == "")
	{
		$('#TermStartDate_WarningMsgLayer').html("<font color='#FF0000'>*<?=$Lang['SysMgr']['AcademicYear']['JSWarning']['StartDateIsEmpty'];?></font>");
		$('#TermStartDate_WarningMsgLayer').show();
		$('#TermStartDate').focus();
		return false;
	}else{
		/* check start date is valid */
		if(!check_date_without_return_msg(document.getElementById("TermStartDate")))
		{
			$('#TermStartDate_WarningMsgLayer').html("<font color='#FF0000'>*<?=$Lang['SysMgr']['AcademicYear']['JSWarning']['InvalidStartDate'];?></font>");
			$('#TermStartDate_WarningMsgLayer').show();
			$('#TermStartDate').focus();
			return false;
		}
	}
	
	/* check end date is null */
	if($('#TermEndDate').val() == "")
	{
		$('#TermEndDate_WarningMsgLayer').html("<font color='#FF0000'>*<?=$Lang['SysMgr']['AcademicYear']['JSWarning']['EndDateIsEmpty'];?></font>");
		$('#TermEndDate_WarningMsgLayer').show();
		$('#TermEndDate').focus();
		return false;
	}else{
		/* check start date is valid */
		if(!check_date_without_return_msg(document.getElementById("TermEndDate")))
		{
			$('#TermEndDate_WarningMsgLayer').html("<font color='#FF0000'>*<?=$Lang['SysMgr']['AcademicYear']['JSWarning']['InvalidEndDate'];?></font>");
			$('#TermEndDate_WarningMsgLayer').show();
			$('#TermEndDate').focus();
			return false;
		}
	}
	
	/* check end date is later than start date */
	var result = compareDate(document.getElementById("TermEndDate").value,document.getElementById("TermStartDate").value);
	if(result != 1)
	{
		$('#TermEndDate_WarningMsgLayer').html("<font color='#FF0000'>*<?=$Lang['SysMgr']['AcademicYear']['JSWarning']['EndDateLaterThanStartDate'];?></font>");
		$('#TermEndDate_WarningMsgLayer').show();
		$('#TermEndDate').focus();
		return false;
	}
	$('#NewTermSubmitBtn').attr('disabled',true);
	$.post(
		"ajax_validate_term.php",
		{
			RecordType : RecordType,
			EditMode : EditMode,
			AcademicYearID : AcademicYearID,
			YearTermID : YearTermID,
			TermTitleCh : $("#TermTitleCh").val(),
			TermTitleEn : $("#TermTitleEn").val(),
			TermStartDate : $("#TermStartDate").val(),
			TermEndDate : $("#TermEndDate").val()
		},
		function(response){
			if(response == -5){				/* edit mode current term check end date is >= today */
				$('#TermEndDate_WarningMsgLayer').html("<font color='#FF0000'>*<?=$Lang['SysMgr']['AcademicYear']['JSWarning']['InputTodayOrFutureOnly'];?></font>");
				$('#TermEndDate_WarningMsgLayer').show();
				$('#TermEndDate').focus();
				$('#NewTermSubmitBtn').removeAttr('disabled');
				return false;
			}else if(response == -1){		/* check if term period is overlap */
				$('#TermStartDate_WarningMsgLayer').html("<font color='#FF0000'>*<?=$Lang['SysMgr']['AcademicYear']['JSWarning']['TermOverlapped'];?></font>");
				$('#TermStartDate_WarningMsgLayer').show();
				$('#TermStartDate').focus();
				$('#NewTermSubmitBtn').removeAttr('disabled');
				return false;
			}else if(response == -2){		/* check if term chi name is in used */
				$('#TermTitleCh_WarningMsgLayer').html("<font color='#FF0000'>*<?=$Lang['SysMgr']['AcademicYear']['JSWarning']['ChineseTermNameIsDuplicate'];?></font>");
				$('#TermTitleCh_WarningMsgLayer').show();
				$('#TermTitleCh').focus();
				$('#NewTermSubmitBtn').removeAttr('disabled');
				return false;
			}else if(response == -3){		/* check if term eng name is in used */
				$('#TermTitleEn_WarningMsgLayer').html("<font color='#FF0000'>*<?=$Lang['SysMgr']['AcademicYear']['JSWarning']['EnglishTermNameIsDuplicate'];?></font>");
				$('#TermTitleEn_WarningMsgLayer').show();
				$('#TermTitleEn').focus();
				$('#NewTermSubmitBtn').removeAttr('disabled');
				return false;
			}else if(response == -4){		/* check if term is consistency */
				//$('#TermStartDate_WarningMsgLayer').html("<font color='#FF0000'>*<?=$Lang['SysMgr']['AcademicYear']['Warning']['TermDateChecking'];?></font>");
				//$('#TermStartDate_WarningMsgLayer').show();
				//$('#TermStartDate').focus();
				//return false;
				if(confirm('<?=$Lang['SysMgr']['AcademicYear']['Warning']['TermDateChecking'];?>'))
				{
					/* create term */
					$.post(
						"ajax_insert_edit_term.php",
						{
							EditMode : EditMode,
							AcademicYearID : AcademicYearID,
							YearTermID : YearTermID,
							TermTitleCh : $("#TermTitleCh").val(),
							TermTitleEn : $("#TermTitleEn").val(),
							TermStartDate : $("#TermStartDate").val(),
							TermEndDate : $("#TermEndDate").val()
						},
						function(responseText){
							
							if(EditMode == 1){
								if(responseText == 0){
									returnMsg = "<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess'];?>";
								}else{
									returnMsg = "<?=$Lang['General']['ReturnMessage']['UpdateSuccess'];?>";
								}
								Get_Return_Message(returnMsg);
								js_ReloadMainContent()
								js_Init_JEdit();
								js_Init_DND_Table();
								window.top.tb_remove();
							}else{
								if(responseText == 0){
									returnMsg = "<?=$Lang['General']['ReturnMessage']['AddUnsuccess'];?>";
								}else{
									returnMsg = "<?=$Lang['General']['ReturnMessage']['AddSuccess'];?>";
								}
								Get_Return_Message(returnMsg);
								js_ReloadMainContent()
								js_Init_JEdit();
								js_Init_DND_Table();
								window.top.tb_remove();
							}
						}
					);
				}
				else{
					$('#NewTermSubmitBtn').removeAttr('disabled');
				}
			}else{
				/* create term */
				$.post(
					"ajax_insert_edit_term.php",
					{
						EditMode : EditMode,
						AcademicYearID : AcademicYearID,
						YearTermID : YearTermID,
						TermTitleCh : $("#TermTitleCh").val(),
						TermTitleEn : $("#TermTitleEn").val(),
						TermStartDate : $("#TermStartDate").val(),
						TermEndDate : $("#TermEndDate").val()
					},
					function(responseText){
						
						if(EditMode == 1){
							if(responseText == 0){
								returnMsg = "<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess'];?>";
							}else{
								returnMsg = "<?=$Lang['General']['ReturnMessage']['UpdateSuccess'];?>";
							}
							Get_Return_Message(returnMsg);
							js_ReloadMainContent()
							js_Init_JEdit();
							js_Init_DND_Table();
							window.top.tb_remove();
						}else{
							if(responseText == 0){
								returnMsg = "<?=$Lang['General']['ReturnMessage']['AddUnsuccess'];?>";
							}else{
								returnMsg = "<?=$Lang['General']['ReturnMessage']['AddSuccess'];?>";
							}
							Get_Return_Message(returnMsg);
							js_ReloadMainContent()
							js_Init_JEdit();
							js_Init_DND_Table();
							window.top.tb_remove();
						}
					}
				);
			}
		}
	);
}

function js_Delete_Term(YearTermID)
{
	$.post(
		"ajax_delete_term.php",
		{
			YearTermID : YearTermID
		},
		function(responseText){
			Get_Return_Message(responseText);
			js_ReloadMainContent()
			js_Init_JEdit();
			js_Init_DND_Table();
		}
	);
}

function jsClearWarningMsg(DivName)
{
	if($("#"+DivName).html() != "")
	{
		$("#"+DivName).html('');
		$("#"+DivName).hide();
	}
}

/*
function ShowImportantNote(jsDetailType, jsClickedObjID)
{
	js_Hide_Detail_Layer();
		
	var jsAction = '';
	if (jsDetailType == 'ShowImportantNote')
		jsAction = 'ShowImportantNote';
	
	$('div#ImportantNoteLayerContentDiv').html('<?=$Lang['General']['Loading']?>');	
	js_Change_Layer_Position(jsClickedObjID);
	MM_showHideLayers('ImportantNoteLayer','','show');
			
	$('div#ImportantNoteLayerContentDiv').load(
		"ajax_reload.php", 
		{ 
			Action: jsAction
		},
		function(returnString)
		{
			$('div#ImportantNoteLayerContentDiv').css('z-index', '999');
			$('div#ImportantNoteLayerContentDiv').css('overflow', 'auto');
			$('div#ImportantNoteLayerContentDiv').css('height', '250px');
		}
	);
}
*/

function js_Hide_Detail_Layer()
{
	MM_showHideLayers('ImportantNoteLayer','','hide');
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function js_Change_Layer_Position(jsClickedObjID) {
	var jsOffsetLeft, jsOffsetTop;
	
	//jsOffsetLeft = 450;
	//jsOffsetTop = -15;
	jsOffsetLeft = -15;
	jsOffsetTop = -15;
		
	var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') - jsOffsetLeft;
	var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') - jsOffsetTop;

	document.getElementById('ImportantNoteLayer').style.left = posleft + "px";
	document.getElementById('ImportantNoteLayer').style.top = postop + "px";
	document.getElementById('ImportantNoteLayer').style.visibility = 'visible';
}
</script>