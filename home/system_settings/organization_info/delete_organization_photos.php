<?php

// using:
/*
 *  delete organization photo for ip30
 * 
 *  2020-06-16 Cameron
 *      - create this file
 */

 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-OrganizationInfo"])) {
    No_Access_Right_Pop_Up();
}

$returnMsgKey = '';
$lf = new libfilesystem();
$ls = new libgeneralsettings();
$type = IntegerSafe($_GET['type']);

if ($type == 1) {
    $settingValue = 'BackgroundImage';
    $settingAry = $ls->Get_General_Setting('OrganizationInfo', array("'$settingValue'"));

    if (count($settingAry)) {
        $image = $intranet_root . "/file/organization_info/" . $settingAry[$settingValue];

        if (is_file($image)) {
            $lf->file_remove($image);
            $result = $ls->Remove_General_Setting('OrganizationInfo', array($settingValue));
            $returnMsgKey = $result ? 'FileDeleteSuccess' : 'FileDeleteUnsuccess';
        }
    }
} elseif ($type == 2) {
//    $settingValue = 'LoginLogo';
    $lf = new libfilesystem();
    $schoolLogoName = $lf->file_read($intranet_root."/file/schoolbadge.txt");
    $schoolLogoPath = "$intranet_root/file/$schoolLogoName";

    if (is_file($schoolLogoPath)){
        $lf->file_remove($schoolLogoPath);
    }
    $lf->file_remove("$intranet_root/file/schoolbadge.txt");

} else {
    $settingValue = '';
}

intranet_closedb();

header("location: index_ip30.php?returnMsgKey=".$returnMsgKey);

?>
