<?php

// using:
/*
 *  Organization Info Setup for standard LMS
 * 
 *  2020-03-26 Cameron
 *      - create this file
 */

 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if(!($sys_custom['project']['CourseTraining']['IsEnable'] && ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-OrganizationInfo"]))) {
    No_Access_Right_Pop_Up();
}

$re_path = "/file/standard_lms";
$path = $intranet_root.$re_path;
$target = "";
$file = $_FILES['logo'];
$returnMsgKey = 'ImageUploadFail';      // default fail

$maxSize = 1024 * 1024 * 2;    // 2M
if ($file['size'] > $maxSize) {
    $returnMsgKey = 'FileSizeExceedLimit';
}
else if ($file['name'])
{
    $lf = new libfilesystem();
    if (!is_dir($path))
    {
        $lf->folder_new($path);
    }
    
    $ext = strtolower($lf->file_ext($file['name']));
    $validExtensionAry = array('.jpg','.jepg','.gif','.png');
    if (in_array($ext,$validExtensionAry)) {
        $target = $path ."/organization_logo". $ext;
        if (file_exists($target)) {
            $lf->file_remove($target);      // delete it first
        }
        move_uploaded_file($file["tmp_name"], $target);
        
        $ls = new libgeneralsettings();
        $result = $ls->Save_General_Setting('OrganizationInfo',array('logo'=>'organization_logo'.$ext));
        
        $returnMsgKey = $result ? 'ImageUploadSuccess' : 'ImageUploadFail';
    }
}

intranet_closedb();

header("location: index.php?returnMsgKey=".$returnMsgKey);

?>
