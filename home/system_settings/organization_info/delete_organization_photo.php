<?php

// using:
/*
 *  delete organization photo for standard LMS
 * 
 *  2020-04-17 Cameron
 *      - create this file
 */

 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if(!($sys_custom['project']['CourseTraining']['IsEnable'] && ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-OrganizationInfo"]))) {
    No_Access_Right_Pop_Up();
}

$currLogo = '';
$returnMsgKey = '';
$lf = new libfilesystem();
$ls = new libgeneralsettings();
$settingAry = $ls->Get_General_Setting('OrganizationInfo',array("'logo'"));

if (count($settingAry)) {
    $logo = $intranet_root . "/file/standard_lms/" . $settingAry['logo'];
    
    if (file_exists($logo)) {
        $lf->file_remove($logo);
        $result = $ls->Remove_General_Setting('OrganizationInfo',array('logo'));
        $returnMsgKey = $result ? 'FileDeleteSuccess' : 'FileDeleteUnsuccess';
    }
}

intranet_closedb();

header("location: index.php?returnMsgKey=".$returnMsgKey);

?>
