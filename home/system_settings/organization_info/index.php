<?php

// using:
/*
 *  Organization Info Setup for standard LMS
 *
 *  2020-06-16 Cameron
 *      - redirect to index_ip30.php for non CourseTraining
 *
 *  2020-04-17 Cameron
 *      - add delete logo function
 *      
 *  2020-03-26 Cameron
 *      - create this file
 */

 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$sys_custom['project']['CourseTraining']['IsEnable']) {
    header("Location: index_ip30.php");
}

if(!($sys_custom['project']['CourseTraining']['IsEnable'] && ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-OrganizationInfo"]))) {
    No_Access_Right_Pop_Up();
}


$linterface = new interface_html();

// setting top menu highlight
$CurrentPageArr['OrganizationInfo'] = 1;

### Title ###
$TAGS_OBJ[] = array($Lang['Header']['Menu']['OrganizationInfo']);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['OrganizationInfo'];

$currLogo = '';
$ls = new libgeneralsettings();
$settingAry = $ls->Get_General_Setting('OrganizationInfo',array("'logo'"));

if (count($settingAry)) {
    $logo = "/file/standard_lms/". $settingAry['logo'];
    
    if (file_exists($intranet_root.$logo)) {
        $currLogo = $logo;
    }
}

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

?>
<script>
$(document).ready(function(){

	$('#btnSubmit').click(function(){
    	if ($('#logo').val() != ''){
    		var extension = Get_File_Ext(document.getElementById('logo').value);
    		var f = $('#logo')[0].files[0];
    		
    		if ((extension != "jpg" && extension != "jpeg" && extension != "gif" && extension != "png") || extension == false) {
    			$('div#FileUploadWarningDiv').html('<?php echo $Lang['SysMgr']['OrganizationInfo']['InvalidFile'];?>');
    			$('div#FileUploadWarningDiv').show('fast');
    		}
    		else if (f.size/Math.pow(1024,2) > 2) {		// size in MB
    			$('div#FileUploadWarningDiv').html('<?php echo $Lang['SysMgr']['OrganizationInfo']['FileSizeExceedLimit'];?>');
    			$('div#FileUploadWarningDiv').show('fast');
    		}
    		else {
    			$('div#FileUploadWarningDiv').hide();
    			$('#form1').submit();
    		}
    	}
    	else {
    		
			$('div#FileUploadWarningDiv').html('<?php echo $Lang['General']['PleaseSelectFiles'];?>');
			$('div#FileUploadWarningDiv').show('fast');
    	}
	});

	$('#btnReset').click(function(){
		$('div#FileUploadWarningDiv').hide();
		$('#logo').val('');
	});

	$('#deletePhoto').click(function(){
		if(confirm("<?php echo $Lang['SysMgr']['OrganizationInfo']['ConfirmDeleteLogo'];?>?")){
			window.location = "delete_organization_photo.php";
		}
	});
	
});

</script>

<form name="form1" id="form1" enctype="multipart/form-data" method="post" action="organization_info_update.php">
	<table class="form_table_v30">
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title">
				<?php echo $Lang['SysMgr']['OrganizationInfo']['Logo'];?>
			</td>
			<td class="tabletextremark" valign="top">
				<input type="file" name="logo" id="logo" class="file"><br /><?php ?>
				<br><?php echo $Lang['SysMgr']['OrganizationInfo']['LogoInstruction'];?>
				<div id="FileUploadWarningDiv" class="" style="color: red; display: none;"></div>
				
<?php if ($currLogo):?>				
				<div>&nbsp;</div><div><image src="<?php echo $currLogo;?>"></div>
				<div><a href="#" class="tablelink" id="deletePhoto"><?php echo $Lang['SysMgr']['OrganizationInfo']['DeleteLogo'];?></a></div>
<?php endif;?>				
			</td>
		</tr>    			
	</table>


<div class="edit_bottom_v30">
<p class="spacer"></p>
<?=$linterface->GET_ACTION_BTN($button_save, "button", "", 'btnSubmit');?> 
<?=$linterface->GET_ACTION_BTN($button_reset, "reset", "", 'btnReset');?>
<p class="spacer"></p>
</div>
<?php //echo csrfTokenHtml(generateCsrfToken());?>
</form>

<?
$linterface->LAYOUT_STOP();

intranet_closedb();
?>
