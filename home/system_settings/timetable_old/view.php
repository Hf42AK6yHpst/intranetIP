<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui_old.php");

intranet_opendb();

$linterface = new interface_html();
$CurrentPageArr['Timetable'] = 1;

### Title ###
$TAGS_OBJ[] = array($Lang['SysMgr']['Timetable']['Tag']['Management'], "index.php", 0);
$TAGS_OBJ[] = array($Lang['SysMgr']['Timetable']['Tag']['View'], "view.php", 1);
$MODULE_OBJ['title'] = $Lang['SysMgr']['Timetable']['ModuleTitle'];
$linterface->LAYOUT_START(); 

$libtimetable_ui = new libtimetable_ui();

$include_JS_CSS = $libtimetable_ui->Include_JS_CSS();


## Show Hide Options btn
$ShowHideBtnDiv = '';
$ShowHideBtnDiv .= '<div id="ShowHideOptionBtnDiv" style="display:none" align="left">';
	$ShowHideBtnDiv .= '<span id="spanShowOption">';
		$ShowHideBtnDiv .= '<a href="javascript:js_Show_Hide_Option(1);" class="contenttool">';
			$ShowHideBtnDiv .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle">';
			$ShowHideBtnDiv .= $Lang['Btn']['Show'];
		$ShowHideBtnDiv .= '</a>';
	$ShowHideBtnDiv .= '</span>';
	$ShowHideBtnDiv .= '<span id="spanHideOption" style="display:none">';
		$ShowHideBtnDiv .= '<a href="javascript:js_Show_Hide_Option(0);" class="contenttool">';
			$ShowHideBtnDiv .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle">';
			$ShowHideBtnDiv .= $Lang['Btn']['Hide'];
		$ShowHideBtnDiv .= '</a>';
	$ShowHideBtnDiv .= '</span>';
$ShowHideBtnDiv .= '</div>';
$ShowHideBtnDiv .= '<br style="clear:both">';


## Options
$OptionsDiv = '';
$OptionsDiv .= '<div id="FilterDiv">'."\n";
	$OptionsDiv .= '<div id="MainFilterDiv">'."\n";
		$OptionsDiv .= $libtimetable_ui->Get_View_Settings_Table();
	$OptionsDiv .= '</div>';
	$OptionsDiv .= '<div id="SecondaryFilterDiv">'."\n";
	$OptionsDiv .= '</div>';
$OptionsDiv .= '</div>';


## Timetable 
$TimetableDiv = '';
$TimetableDiv .= '<div id="TimetableDiv" style="display:none">'."\n";
	$TimetableDiv .= '====== Generated Timetable Here ======';
	//$TimetableDiv .= $libtimetable_ui->Get_Timetable_Table(1, 116, $SubjectGroupIDArr=array(), $LocationID, $DisplayOptionArr=array(), 1);
$TimetableDiv .= '</div>';

?>

<div id="IndexDebugArea"></div>

====== Prototype ======

<?= $include_JS_CSS ?>
<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td>
			<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
					<td id="OptionsTd">
						<?= $ShowHideBtnDiv ?>
						<?= $OptionsDiv ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr> 
		<td class="main_content">
			<?= $TimetableDiv ?>
		</td>
	</tr>
	
</table>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

<?
$linterface->LAYOUT_STOP();

intranet_closedb();
?>

<script>
var EmptyString = '---';

/*
function js_Change_Subject_Group_Selection()
{
	var SelectedSubjectID = $('#SelectedSubjectID').val();
	$('#SubjectGroupSelectionDiv').hide();
	$('#SubjectGroupSelectionTr').hide();
	
	if (SelectedSubjectID != "")
	{
		$('#SubjectGroupSelectionDiv').load(
			"ajax_reload.php", 
			{ 
				RecordType: 'SubjectGroupSelectionDiv',
				SubjectID: SelectedSubjectID
			},
			function(ReturnData)
			{
				$('#SubjectGroupSelectionTr').show();
				$('#SubjectGroupSelectionDiv').show();
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
}
*/


/************************ Reload filters functions ************************/
function js_Update_Period_Cycle_Semester_Selection()
{
	var SelectedAcademicYearID = $('#SelectedAcademicYearID').val();
	
	$('#PeriodSelectionDiv').hide();
	$('#CycleSelectionDiv').hide();
	$('#SemesterSelectionDiv').hide();
	
	/*
	$('#PeriodSelectionDiv').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Reload_Floor_Selection'
		},
		function(ReturnData)
		{
			$('#' + FloorSelectionDivID).show();
			js_Change_Room_Selection(FloorSelectionID, RoomSelectionDivID, RoomSelectionID);
			
			$('#' + FloorSelectionDivID).show();
			
			//$('#IndexDebugArea').html(ReturnData);
			//$('#debugArea').html(ReturnData);
		}
	);
	*/
	
	$('#CycleSelectionDiv').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Reload_Cycle_Period_Selection',
			AcademicYearID: SelectedAcademicYearID,
			NoFirst: 1
		},
		function(ReturnData)
		{
			$('#CycleSelectionDiv').show();
			
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
	
	$('#SemesterSelectionDiv').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Reload_Semester_Selection',
			AcademicYearID: SelectedAcademicYearID,
			NoFirst: 1
		},
		function(ReturnData)
		{
			$('#SemesterSelectionDiv').show();
			
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}


function js_Reload_Secondary_Selection()
{
	var SelectedViewMode = $('#SelectedViewMode').val();
	
	$('#SecondaryFilterDiv').hide();
	$('#SecondaryFilterDiv').html('');
	$('#SecondaryFilterDiv').show();
	
	if (SelectedViewMode != '')
		js_Reload_Secondary_Filter_Table(SelectedViewMode);
		
	$('#SecondaryFilterDiv').show();
}

function js_Reload_Secondary_Filter_Table(ViewMode)
{
	$('#SecondaryFilterDiv').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Reload_Secondary_Filter_Table',
			ViewMode: ViewMode
		},
		function(ReturnData)
		{
			$('#SecondaryFilterDiv').show();
	
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

/******* Class Mode  *******/
function js_Reload_Class_Selection(isMultiple)
{
	var selectedAcademicYearID = $('#SelectedAcademicYearID').val();
	var selectedYearID = $('#SelectedYearID').val();
	
	$('#ClassSelectionDiv').hide();
	
	if (selectedYearID != "")
	{
		var OnChange = '';
		if (isMultiple == 0)
		{
			// from Personal view mode
			OnChange = "js_Reload_Target_Selection('Student')";
		}
	
		$('#ClassSelectionDiv').load(
			"ajax_reload.php", 
			{ 
				RecordType: 'Reload_Class_Selection',
				ID: 'SelectedYearClassID',
				AcademicYearID: selectedAcademicYearID,
				YearID: selectedYearID,
				OnChange: OnChange,
				NoFirst: 1,
				IsMultiple: isMultiple
			},
			function(ReturnData)
			{
				if (isMultiple == 1)
					$("#SelectedYearClassID option").attr("selected","selected");
	
				$('#ClassSelectionDiv').show();
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
	else
	{
		$('#ClassSelectionDiv').html(EmptyString);
		$('#ClassSelectionDiv').show();
	}
}
/******* End of Class Mode  *******/


/******* Subject Mode  *******/
function js_Reload_Subject_Group_Selection()
{
	var selectedAcademicYearID = $('#SelectedAcademicYearID').val();
	var selectedYearTermID = $('#SelectedYearTermID').val();
	var selectedSubjectID = $('#SelectedSubjectID').val();
	
	$('#SubjectGroupSelectionDiv').hide();
	
	if (selectedSubjectID != '')
	{
		$('#SubjectGroupSelectionDiv').load(
			"ajax_reload.php", 
			{ 
				RecordType: 'Reload_Subject_Group_Selection',
				ID: 'SelectedSubjectGroupID',
				AcademicYearID: selectedAcademicYearID,
				YearTermID: selectedYearTermID,
				SubjectID: selectedSubjectID,
				NoFirst: 1,
				IsMultiple: 1
			},
			function(ReturnData)
			{
				$("#SelectedSubjectGroupID option").attr("selected","selected");
	
				$('#SubjectGroupSelectionDiv').show();
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
	else
	{
		$('#SubjectGroupSelectionDiv').html(EmptyString);
		$('#SubjectGroupSelectionDiv').show();
	}
}
/******* End of Subject Mode  *******/


/******* Personal Mode  *******/
function js_Show_Hide_Form_Class_Selection()
{
	var selectedIdentity = $('#SelectedIdentity').val();
	
	if (selectedIdentity == '')
	{
		$('#FormSelectionDiv').hide();
		$('#ClassSelectionDiv').hide();
		$('#PersonSelectionDiv').hide();
		$('#FormSelectionTr').hide();
		$('#ClassSelectionTr').hide();
		$('#PersonSelectionTr').hide();
		$('#PersonSelectionDiv').html('');
	}	
	else if (selectedIdentity == 'Student')
	{
		$('#FormSelectionTr').show();
		$('#ClassSelectionTr').show();
		$('#PersonSelectionTr').show();
	}
	else
	{
		$('#FormSelectionTr').hide();
		$('#ClassSelectionTr').hide();
		$('#PersonSelectionTr').show();
		js_Reload_Target_Selection(selectedIdentity);
	}
}

function js_Reload_Target_Selection(selectedIdentity)
{
	var selectedYearClassID = $('#SelectedYearClassID').val();
	$('#PersonSelectionDiv').hide();
	
	$('#PersonSelectionDiv').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Reload_Target_Selection',
			ID: 'SelectedUserID',
			TargetType: selectedIdentity,
			YearClassID: selectedYearClassID,
			NoFirst: 1,
			IsMultiple: 1
		},
		function(ReturnData)
		{
			$("#SelectedUserID option").attr("selected","selected");

			$('#PersonSelectionDiv').show();
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}
/******* End of Personal Mode  *******/


/******* Room Mode  *******/
function js_Change_Floor_Selection(BuildingSelectionID, FloorSelectionDivID, FloorSelectionID, RoomSelectionDivID, RoomSelectionID, EmptySymbol)
{
	var SelectedBuildingID = $('#' + BuildingSelectionID).val();
	$('#' + RoomSelectionDivID).hide();
	$('#' + FloorSelectionDivID).hide();
	
	if (SelectedBuildingID == "")
	{
		if (EmptySymbol != '')
		{
			$('#' + FloorSelectionDivID).html(EmptySymbol);
			$('#' + FloorSelectionDivID).show();
			$('#' + RoomSelectionDivID).html(EmptySymbol);
			$('#' + RoomSelectionDivID).show();
		}
	}
	else
	{
		// Refresh Floor Selection
		$('#' + FloorSelectionDivID).load(
			"ajax_reload.php", 
			{ 
				RecordType: 'Reload_Floor_Selection',
				BuildingID: SelectedBuildingID,
				FloorSelectionObjID: FloorSelectionID,
				RoomSelectionDivID: RoomSelectionDivID,
				RoomSelectionID: RoomSelectionID,
				EmptySymbol: EmptySymbol
			},
			function(ReturnData)
			{
				$('#' + FloorSelectionDivID).show();
				js_Change_Room_Selection(FloorSelectionID, RoomSelectionDivID, RoomSelectionID);
				
				$('#' + FloorSelectionDivID).show();
				
				//$('#IndexDebugArea').html(ReturnData);
				//$('#debugArea').html(ReturnData);
			}
		);
	}
}

function js_Change_Room_Selection(FloorSelectionID, RoomSelectionDivID, RoomSelectionID, EmptySymbol)
{
	var SelectedLocationLevelID = $('#' + FloorSelectionID).val();
	$('#' + RoomSelectionDivID).hide();
	
	if (SelectedLocationLevelID == "")
	{
		if (EmptySymbol != '')
		{
			$('#' + RoomSelectionDivID).html(EmptySymbol);
			$('#' + RoomSelectionDivID).show();
		}
	}
	else
	{
		// Refresh Room Selection
		$('#' + RoomSelectionDivID).load(
			"ajax_reload.php", 
			{ 
				RecordType: 'Reload_Room_Selection',
				LocationLevelID: SelectedLocationLevelID,
				RoomSelectionObjID: RoomSelectionID
			},
			function(ReturnData)
			{
				$('#' + RoomSelectionDivID).show();
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
}
/******* End of Room Mode  *******/

/************************ End of Reload filters functions ************************/

function js_Select_All(SelectionObjID)
{
	$('#' + SelectionObjID + ' > option[selected!=true]').attr("selected","selected");
}

function Generate_Timetable()
{
	Block_Document();
	
	// Add grey background to the options
	$('#OptionsTd').addClass("report_show_option");
	
	// Show the "Show option btn"
	js_Show_ShowHideOptionBtn();
	
	// Hide the options
	js_Show_Hide_Option(0);
	
	// Reload the timetable
	js_Reload_Timetable();
}

function js_Show_Hide_Option(isShow)
{
	if (isShow)
	{
		// Show Options
		$('#FilterDiv').show();
		
		// Hide "Show Btn"
		$('#spanShowOption').hide();
		
		// Show "Hide Btn"
		$('#spanHideOption').show();
	}
	else
	{
		// Hide Options
		$('#FilterDiv').hide();
		
		// Show "Show Btn"
		$('#spanShowOption').show();
		
		// Hide "Hide Btn"
		$('#spanHideOption').hide();
	}
}

function js_Show_ShowHideOptionBtn()
{
	$('#ShowHideOptionBtnDiv').show();
}

function js_Reload_Timetable()
{
	var SelectedAcademicYearID = $('#SelectedAcademicYearID').val();
	var SelectedPeriodID = $('#SelectedPeriodID').val();
	var SelectedCyclePeriodID = $('#SelectedCyclePeriodID').val();
	var SelectedYearTermID = $('#SelectedYearTermID').val();
	var SelectedSubjectID = $('#SelectedSubjectID').val();
	var SelectedBuildingID = $('#SelectedBuildingID').val();
	var SelectedLocationLevelID = $('#SelectedLocationLevelID').val();
	var SelectedLocationID = $('#SelectedLocationID').val();
	
	SelectedLocationID = js_Check_Object_Defined(SelectedLocationID);
	SelectedSubjectID = js_Check_Object_Defined(SelectedSubjectID);
	
	//Block_Document();
	$('#TimetableDiv').hide();
	
	$('#TimetableDiv').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Reload_Timetable',
			PeriodID: 1,
			CyclePeriodID: 1,
			YearTermID: SelectedYearTermID,
			SubjectID: SelectedSubjectID,
			LocationID: SelectedLocationID,
			isViewMode: 1
			
		},
		function(ReturnData)
		{
			$('#TimetableDiv').show();
			UnBlock_Document();
			
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

// mouseover show icon
function js_Show_Hide_Btn_Div(ObjID, Action)
{
	if (Action == 1)
	{
		$('#'+ObjID).addClass('table_row_tool row_content_tool');
	}
	else
	{
		$('#'+ObjID).removeClass('table_row_tool row_content_tool');
	}
}

// return '' if the value is undefined
function js_Check_Object_Defined(obj)
{
	if (obj == null)
		return '';
	else
		return obj;
}

//initThickBox();
</script>