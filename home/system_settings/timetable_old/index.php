<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui_old.php");

intranet_opendb();

$linterface = new interface_html();
$CurrentPageArr['Timetable'] = 1;

### Title ###
$TAGS_OBJ[] = array($Lang['SysMgr']['Timetable']['Tag']['Management'], "index.php", 1);
$TAGS_OBJ[] = array($Lang['SysMgr']['Timetable']['Tag']['View'], "view.php", 0);
$MODULE_OBJ['title'] = $Lang['SysMgr']['Timetable']['ModuleTitle'];
$linterface->LAYOUT_START(); 

$libtimetable_ui = new libtimetable_ui();

$include_JS_CSS = $libtimetable_ui->Include_JS_CSS();

## Filters
$filterDiv = '';
$filterDiv .= '<div class="content_top_tool">'."\n";
	$filterDiv .= '<div class="Conntent_tool" id="FilterDiv">'."\n";
		$filterDiv .= $libtimetable_ui->Get_Allocation_Filter_Table();
	$filterDiv .= '</div>'."\n";
	$filterDiv .= '<br style="clear:both" />'."\n";
$filterDiv .= '</div>'."\n";


## Timetable Table
$TimetableDiv = '';
$TimetableDiv .= '<div id="TimetableDiv">'."\n";
$TimetableDiv .= '</div>';

/*
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
$libsubject_class_mapping = new subject_class_mapping();
debug_r($libsubject_class_mapping->Get_Subject_Group_List(2,'',0));
*/
?>

<div id="IndexDebugArea"></div>

====== Prototype ======

<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="main_content">
			<?=$include_JS_CSS?>
			<?= $filterDiv ?>
			<?= $TimetableDiv ?>
		</td>
	</tr>
</table>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

<?
$linterface->LAYOUT_STOP();

intranet_closedb();
?>

<script>
var jsCurPeriodID = '';
var jsCurYearTermID = '';
var jsCurCycleDay = '';
var jsLastSelectedSubjectID = '';
var jsLastSelectedYearTermID = '';
var jsShowHideSpeed = 'fast';



/************************ Show / Hide / Update Selection Function ************************/
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3)
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function js_Show_Hide_Filter_Div()
{
	var selectedSubjectID = $('#SelectedSubjectID').val();
	var selectedYearTermID = $('#SelectedYearTermID').val();
	
	if ((jsLastSelectedSubjectID == selectedSubjectID) && (jsLastSelectedYearTermID == selectedYearTermID))
	{
		// same Subject => show the original selections
		MM_showHideLayers('SubjectGroupSelectionDiv','','show');
	}
	else
	{
		MM_showHideLayers('SubjectGroupSelectionDiv','','hide');
		
		// different Subject => reload selection options
		if (selectedSubjectID != "")
		{
			$('#SubjectGroupSelectionDiv').load(
				"ajax_reload.php", 
				{ 
					RecordType: 'SubjectGroupCheckboxSelection',
					SubjectID: selectedSubjectID,
					YearTermID: selectedYearTermID
				},
				function(ReturnData)
				{
					MM_showHideLayers('SubjectGroupSelectionDiv','','show');
					jsLastSelectedSubjectID = selectedSubjectID;
					jsLastSelectedYearTermID = selectedYearTermID;
					
					//$('#IndexDebugArea').html(ReturnData);
				}
			);
		}
	}
}

function js_Change_Subject_Group_Selection(TimeSessionID, CycleDays, Day, js_RoomAllocationID, YearTermID)
{
	var SelectedSubjectID = $('#SelectedSubjectID_InAddEditLayer').val();
	$('#SubjectGroupSelectionTr_InAddEditLayer').hide();
	
	if (SelectedSubjectID != "")
	{
		$('#SubjectGroupSelectionDiv_InAddEditLayer').load(
			"ajax_reload.php", 
			{ 
				RecordType: 'SubjectGroupSelectionDiv',
				SubjectID: SelectedSubjectID,
				TimeSessionID: TimeSessionID,
				CycleDays: CycleDays,
				Day: Day,
				RoomAllocationID: js_RoomAllocationID,
				YearTermID: YearTermID
			},
			function(ReturnData)
			{
				$('#SubjectGroupSelectionTr_InAddEditLayer').show();
				
				var Onchange = "js_Validate_Subject_Group_Overlap(TimeSessionID, CycleDays, Day, js_RoomAllocationID)";
				eval(Onchange);
				
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
}

function js_Update_Period_Cycle_Semester_Selection()
{
	var SelectedAcademicYearID = $('#SelectedAcademicYearID').val();
	
	$('#PeriodSelectionDiv').hide();
	$('#CycleSelectionDiv').hide();
	$('#TermSelectionDiv').hide();
	MM_showHideLayers('SubjectGroupSelectionDiv','','hide');
	
	
	if (SelectedAcademicYearID != '')
	{
		/*
		$('#PeriodSelectionDiv').load(
			"ajax_reload.php", 
			{ 
				RecordType: 'Reload_Floor_Selection'
			},
			function(ReturnData)
			{
				$('#' + FloorSelectionDivID).show();
				js_Change_Room_Selection(FloorSelectionID, RoomSelectionDivID, RoomSelectionID);
				
				$('#' + FloorSelectionDivID).show();
				
				//$('#IndexDebugArea').html(ReturnData);
				//$('#debugArea').html(ReturnData);
			}
		);
		*/
		
		$('#CycleSelectionDiv').load(
			"ajax_reload.php", 
			{ 
				RecordType: 'Reload_Cycle_Days_Selection',
				AcademicYearID: SelectedAcademicYearID,
				NoFirst: 0
			},
			function(ReturnData)
			{
				$('#CycleSelectionDiv').show();
				
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
		
		$('#TermSelectionDiv').load(
			"ajax_reload.php", 
			{ 
				RecordType: 'Reload_Semester_Selection',
				AcademicYearID: SelectedAcademicYearID
			},
			function(ReturnData)
			{
				$('#TermSelectionDiv').show();
				
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
}

function js_Update_Subject_Selection()
{
	var SelectedYearTermID = $('#SelectedYearTermID').val();
	
	MM_showHideLayers('SubjectGroupSelectionDiv','','hide');
	$('#SubjectSelectionDiv').hide();
	
	$('#SubjectSelectionDiv').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Reload_Subject_Selection',
			SelectedYearTermID: SelectedYearTermID,
			ID: "SelectedSubjectID"
		},
		function(ReturnData)
		{
			$('#SubjectSelectionDiv').show();
			
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}


function js_Change_Room_Selection(js_PATH_WRT_ROOT, FloorSelID, RoomSelTrID, RoomSelDivID, RoomSelID, RoomSelFirstTitle, RoomSelOnchange)
{
	var SelectedLocationLevelID = $('#' + FloorSelID).val();
	
	if (RoomSelTrID != '')
		$('#' + RoomSelTrID).hide();
	$('#' + RoomSelDivID).hide();
	
	if (SelectedLocationLevelID != "")
	{
		// Refresh Room Selection
		$('#' + RoomSelDivID).load(
			js_PATH_WRT_ROOT + "home/system_settings/location/ajax_reload_selection.php", 
			{ 
				RecordType: 'Reload_Room_Selection',
				LocationLevelID: SelectedLocationLevelID,
				RoomSelID: RoomSelID,
				RoomSelFirstTitle: RoomSelFirstTitle,
				RoomSelOnchange: RoomSelOnchange
			},
			function(ReturnData)
			{
				// Display Room Selection
				if (RoomSelTrID != '')
					$('#' + RoomSelTrID).show();
					
				eval(RoomSelOnchange);
				
				$('#' + RoomSelDivID).show();
				
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
}

/************************ End of Show / Hide / Update Selection Function ************************/

function js_Select_All_None_Subject_Group()
{
	var isChecked = $('#SubjectGroupID_All').attr('checked');
	//$('.SubjectGroupChkBox').each( function () { $(this).attr('checked', isChecked); });
	
	$('.SubjectGroupChkBox').attr('checked', isChecked);
}

function js_Unselect_All_Subject_Group(thisObjID)
{
	var isChecked = $('#' + thisObjID).attr('checked');
	
	if (!isChecked)
		$('#SubjectGroupID_All').attr('checked', false);
}

// mouseover show icon
function js_Show_Hide_Btn_Div(ObjID, Action)
{
	if (Action == 1)
	{
		$('#'+ObjID).addClass('table_row_tool row_content_tool');
	}
	else
	{
		$('#'+ObjID).removeClass('table_row_tool row_content_tool');
	}
}

// return '' if the value is undefined
function js_Check_Object_Defined(obj)
{
	if (obj == null)
		return '';
	else
		return obj;
}


/******************** Get / Reload UI Functions ********************/
function js_Get_Add_Edit_Room_Allocation_Layer(TimeSessionID, CycleDays, Day, RoomAllocationID)
{
	$.post(
		"ajax_get_add_edit_allocation_layer.php", 
		{ 
			TimeSessionID: TimeSessionID,
			Day: Day,
			RoomAllocationID: RoomAllocationID,
			CycleDays: CycleDays,
			YearTermID: jsCurYearTermID
		},
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);
			
			//$('#debugArea').html(ReturnData);
		}
	);
}

function js_Reload_Timetable()
{
	var SelectedAcademicYearID = $('#SelectedAcademicYearID').val();
	var SelectedPeriodID = $('#SelectedPeriodID').val();
	var SelectedCycleDays = $('#SelectedCycleDays').val();
	var SelectedYearTermID = $('#SelectedYearTermID').val();
	var SelectedSubjectID = $('#SelectedSubjectID').val();
	var SelectedBuildingID = $('#SelectedBuildingID').val();
	var SelectedLocationLevelID = $('#SelectedLocationLevelID').val();
	var SelectedLocationID = $('#SelectedLocationID').val();
	
	SelectedLocationID = js_Check_Object_Defined(SelectedLocationID);
	
	jsCurPeriodID = SelectedPeriodID;
	jsCurCycleDay = SelectedCycleDays;
	jsCurYearTermID = SelectedYearTermID;
	
	var SelectedSubjectGroupID = '';
	var thisSelectionID = '';
	var thisArr = '';
	var thisSubjectGroupID = '';
	
	$("input:checkbox:checked").each( function () {
		thisSelectionID = $(this).attr('id');
		thisArr = thisSelectionID.split('_');
		thisSubjectGroupID = thisArr[1];
		
		if (thisSubjectGroupID != 'All')
			SelectedSubjectGroupID += thisSubjectGroupID + ',';
	});
	
	Block_Document();
	
	$('#TimetableDiv').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Reload_Timetable',
			PeriodID: SelectedPeriodID,
			CycleDays: SelectedCycleDays,
			YearTermID: SelectedYearTermID,
			SubjectID: SelectedSubjectID,
			LocationID: SelectedLocationID,
			SubjectGroupIDList: SelectedSubjectGroupID
		},
		function(ReturnData)
		{
			UnBlock_Document();
			initThickBox();
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}
/******************** End of Get / Reload UI Functions ********************/


/******************** Room Allocation Add / Update / Delete functions ********************/
function js_Insert_Edit_Room_Allocation(RoomAllocationID, TimeSessionID, CycleDays, Day, KeepAdding)
{
	var SelectedBuildingID = $("#SelectedBuildingID_InAddEditLayer").val();
	var SelectedLocationID = $("#SelectedRoomID_InAddEditLayer").val();
	var OthersLocation = encodeURIComponent(Trim($("#OthersLocationTb").val()));
	var SelectedSubjectGroupID = $("#SelectedSubjectGroupID").val();
	
	if (SelectedBuildingID == '')
	{
		alert('<?=$Lang['SysMgr']['Location']['jsWarning']['PleaseSelectBuilding']?>');
		return false;
	}
	
	// Check if the user has entered others location
	if (SelectedBuildingID == '-1')
	{
		if (!check_text(document.getElementById('OthersLocationTb'), '<?=$Lang['SysMgr']['Location']['jsWarning']['PleaseEnterCode']?>'))
			return false;
	}
	
	if (SelectedSubjectGroupID == '')
	{
		alert('<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['SubjectGroup']?>');
		return false;
	}
	
	if (RoomAllocationID == null)
		RoomAllocationID = '';
		
	
	// validate subject group room allocation again
	var SelectedSubjectGroupID = $("#SelectedSubjectGroupID").val();
	$.post(
		"ajax_validate_room_allocation.php", 
		{
			RecordType: "SubjectGroupOverlap",
			TimeSessionID: TimeSessionID,
			CycleDays: CycleDays,
			Day: Day,
			SubjectGroupID: SelectedSubjectGroupID,
			RoomAllocationID: RoomAllocationID
		},
		function(ReturnData)
		{
			if (ReturnData == '0')
			{
				$("#SelectedSubjectGroupID").focus();
			}
			else
			{
				// valid
				$('#SubjectGroupWarningDiv').hide(jsShowHideSpeed);
				
				
				// validate room allocation again
				var SelectedLocationID = $("#SelectedRoomID_InAddEditLayer").val();
				$.post(
					"ajax_validate_room_allocation.php", 
					{
						RecordType: "RoomOverlap",
						TimeSessionID: TimeSessionID,
						CycleDays: CycleDays,
						Day: Day,
						LocationID: SelectedLocationID,
						RoomAllocationID: RoomAllocationID
					},
					function(ReturnData)
					{
						if (ReturnData == '0')
						{
							$('#SelectedRoomID_InAddEditLayer').focus();
						}
						else
						{
							// valid
							$('#LocationWarningDiv').hide(jsShowHideSpeed);
							
							// add update room allocation
							Block_Thickbox();
							
							// Add or Edit
							var action = '';
							var actionScript = '';
							if (RoomAllocationID == '')
								action = "New";
							else
								action = "Edit";
								
							$.post(
								"ajax_update_room_allocation.php", 
								{ 
									Action: action,
									TimeSessionID: TimeSessionID,
									CycleDays: jsCurCycleDay,
									Day: Day,
									RoomAllocationID: RoomAllocationID,
									BuildingID: SelectedBuildingID,
									LocationID: SelectedLocationID,
									OthersLocation: OthersLocation,
									SubjectGroupID: SelectedSubjectGroupID
								 },
								function(ReturnData)
								{
									// refresh timetable
									js_Reload_Timetable();
									
									if (KeepAdding != 1)
										js_Hide_ThickBox();
									else
										js_Get_Add_Edit_Room_Allocation_Layer(TimeSessionID, Day, '');
									
									UnBlock_Thickbox();
									
									// Show system messages
									Show_System_Message(action, ReturnData);
									
									//$('#debugArea').html(ReturnData);
								}
							);
						}
						//$('#debugArea').html(ReturnData);
					}
				);
			}
			//$('#debugArea').html(ReturnData);
		}
	);
}


function js_Delete_Room_Allocation(RoomAllocationID)
{
	if (confirm('<?=$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['RoomAllocation']?>'))
	{
		Block_Thickbox();
		
		$.post(
			"ajax_update_room_allocation.php", 
			{ 
				Action: "Delete",
				RoomAllocationID: RoomAllocationID
			},
			function(ReturnData)
			{
				js_Reload_Timetable();
				
				// Show system messages
				Show_System_Message("Delete", ReturnData);
				
				UnBlock_Thickbox();
				
				//$('#debugArea').html(ReturnData);
			}
		);
	}
	
	return false;
}
/******************** End of Room Allocation Add / Update / Delete functions ********************/


/******************** Validation ********************/
function js_Validate_Room_Vacancy(TimeSessionID, CycleDays, Day, RoomAllocationID)
{
	if (RoomAllocationID == null)
		RoomAllocationID = '';
		
	var SelectedLocationID = $("#SelectedRoomID_InAddEditLayer").val();
	$('#LocationWarningDiv').hide(jsShowHideSpeed);
	
	$.post(
		"ajax_validate_room_allocation.php", 
		{
			RecordType: "RoomOverlap",
			TimeSessionID: TimeSessionID,
			CycleDays: CycleDays,
			Day: Day,
			LocationID: SelectedLocationID,
			RoomAllocationID: RoomAllocationID
		},
		function(ReturnData)
		{
			if (ReturnData == '0')
			{
				// invalid
				$('#LocationWarningDiv').html('<?=$Lang['SysMgr']['Timetable']['jsWarning']['RoomUsedAlready']?>');
				$('#LocationWarningDiv').show(jsShowHideSpeed);
			}
			else
			{
				// valid
				$('#LocationWarningDiv').hide(jsShowHideSpeed);
			}
			
			//$('#debugArea').html(ReturnData);
		}
	);
}

function js_Validate_Subject_Group_Overlap(TimeSessionID, CycleDays, Day, RoomAllocationID)
{
	if (RoomAllocationID == null)
		RoomAllocationID = '';
		
	var SelectedSubjectGroupID = $("#SelectedSubjectGroupID").val();
	$('#SubjectGroupWarningDiv').hide(jsShowHideSpeed);
	
	$.post(
		"ajax_validate_room_allocation.php", 
		{
			RecordType: "SubjectGroupOverlap",
			TimeSessionID: TimeSessionID,
			CycleDays: CycleDays,
			Day: Day,
			SubjectGroupID: SelectedSubjectGroupID,
			RoomAllocationID: RoomAllocationID
		},
		function(ReturnData)
		{
			if (ReturnData == '0')
			{
				// invalid
				$('#SubjectGroupWarningDiv').html('<?=$Lang['SysMgr']['Timetable']['jsWarning']['SubjectGroupAllocatedAlready']?>');
				$('#SubjectGroupWarningDiv').show(jsShowHideSpeed);
			}
			else
			{
				// valid
				$('#SubjectGroupWarningDiv').hide(jsShowHideSpeed);
			}
			
			//$('#debugArea').html(ReturnData);
		}
	);
}

/******************** End of Validation ********************/


// Load system message
function Show_System_Message(Action, Status)
{
	var returnMessage = '';
	if (Action == "New")
	{
		returnMessage = (Status)? '<?=$Lang['General']['ReturnMessage']['AddSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['AddUnsuccess']?>';
	}
	else if (Action == "Edit")
	{
		returnMessage = (Status)? '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
	}
	else if (Action == "Delete")
	{
		returnMessage = (Status)? '<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['DeleteUnsuccess']?>';
	}
	
	Get_Return_Message(returnMessage);
}



//js_Reload_Timetable();

</script>