<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);
$TimeSessionID = stripslashes($_REQUEST['TimeSessionID']);
$CycleDays = stripslashes($_REQUEST['CycleDays']);
$Day = stripslashes($_REQUEST['Day']);
$RoomAllocationID = stripslashes($_REQUEST['RoomAllocationID']);
	
if ($RecordType == "RoomOverlap")
{
	$LocationID = stripslashes($_REQUEST['LocationID']);
	
	$libRoom_Allocation = new Room_Allocation();
	$isOverlapped = $libRoom_Allocation->Is_Room_Allocation_Overlapped($TimeSessionID, $CycleDays, $Day, $LocationID, $RoomAllocationID);
	
	$isValid = ($isOverlapped)? 0 : 1;
}
else if ($RecordType == "SubjectGroupOverlap")
{
	$SubjectGroupID = stripslashes($_REQUEST['SubjectGroupID']);
	
	$libRoom_Allocation = new Room_Allocation();
	$isOverlapped = $libRoom_Allocation->Is_Subject_Group_Overlapped($TimeSessionID, $CycleDays, $Day, $SubjectGroupID, $RoomAllocationID);
	
	$isValid = ($isOverlapped)? 0 : 1;
}

echo $isValid;


intranet_closedb();
?>