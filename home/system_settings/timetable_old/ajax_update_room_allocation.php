<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");

intranet_opendb();

# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == "New")
{
	$DataArr['TimeSessionID'] = stripslashes($_REQUEST['TimeSessionID']);
	$DataArr['CycleDays'] = stripslashes($_REQUEST['CycleDays']);
	$DataArr['Day'] = stripslashes($_REQUEST['Day']);
	$DataArr['LocationID'] = stripslashes($_REQUEST['LocationID']);
	$DataArr['OthersLocation'] = stripslashes(urldecode($_REQUEST['OthersLocation']));
	$DataArr['SubjectGroupID'] = stripslashes($_REQUEST['SubjectGroupID']);
	
	$BuildingID = stripslashes($_REQUEST['BuildingID']);
	if ($BuildingID == -1)
		$DataArr['LocationID'] = '';
	else
		$DataArr['OthersLocation'] = '';
		
	$libRoom_Allocation = new Room_Allocation();
	$success = $libRoom_Allocation->Insert_Record($DataArr);
}
else if ($Action == "Edit")
{
	$RoomAllocationID = stripslashes($_REQUEST['RoomAllocationID']);
	$DataArr['TimeSessionID'] = stripslashes($_REQUEST['TimeSessionID']);
	$DataArr['CycleDays'] = stripslashes($_REQUEST['CycleDays']);
	$DataArr['Day'] = stripslashes($_REQUEST['Day']);
	$DataArr['LocationID'] = stripslashes($_REQUEST['LocationID']);
	$DataArr['OthersLocation'] = stripslashes(urldecode($_REQUEST['OthersLocation']));
	$DataArr['SubjectGroupID'] = stripslashes($_REQUEST['SubjectGroupID']);
	
	$BuildingID = stripslashes($_REQUEST['BuildingID']);
	if ($BuildingID == -1)
		$DataArr['LocationID'] = '';
	else
		$DataArr['OthersLocation'] = '';
		
	$libRoom_Allocation = new Room_Allocation($RoomAllocationID);
	
	$success = $libRoom_Allocation->Update_Record($DataArr);
}
else if ($Action == "Delete")
{
	$RoomAllocationID = stripslashes($_REQUEST['RoomAllocationID']);
	$libRoom_Allocation = new Room_Allocation($RoomAllocationID);
	
	$success = $libRoom_Allocation->Delete_Record();
}

echo $success;


intranet_closedb();
?>