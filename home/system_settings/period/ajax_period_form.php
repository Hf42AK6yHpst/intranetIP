<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libperiods.php");
include_once($PATH_WRT_ROOT."includes/libperiods_ui.php");
intranet_opendb();

$linterface = new interface_html();
$lfcm = new form_class_manage();
$lperiods = new libperiods();
$lperiods_ui = new libperiods_ui();

echo $lperiods_ui->loadPeriodsForm($StartDate);

intranet_closedb();
?>