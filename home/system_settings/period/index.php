<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libperiods.php");
include_once($PATH_WRT_ROOT."includes/libperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
intranet_opendb();

$linterface = new interface_html();
$lperiods = new libperiods();
$lperiods_ui = new libperiods_ui();
$libcalevent = new libcalevent2007();

$CurrentPageArr['Period'] = 1;

### Title ###
$TAGS_OBJ[] = array("View","index.php",1);
$MODULE_OBJ['title'] = "Period";

$returnMsg = urldecode($msg);
$linterface->LAYOUT_START($returnMsg); 

$btnAddFloor = str_replace("'","\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Add'], "button", "js_Insert_DateRange();", "NewDateRangeSubmitBtn"));
$btnCancelFloor = str_replace("'","\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "js_Delete_Gen_DateRange_Row();", "NewDateRangeCancelBtn"));
?>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

<?=$lperiods_ui->periodsCalendar();?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

<script type="text/javascript">
function ChangeSchoolYear(schoolYearID)
{
	Block_Document();
	$.post(
		"ajax_periods_list.php",
		{
			"targetSchoolYearID":schoolYearID
		},
		function(responseText){
			UnBlock_Document();
			$('#periods_list').html(responseText);
			initThickBox();
		}
	);
}

function loadPeriodsForm(TargetDate){
	$.post(
		"ajax_period_form.php", 
		{
			"StartDate": TargetDate
		},
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);
			
			// init a Color Picker
			$('#bgColorCode').colorPicker();
			
			// init a Date Picker
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_start_1').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
			
			// init a Date Picker
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_end_1').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});

			// init 
			Init_JEdit_Input("div.jEditInput");
			
			
		}
	);
}

function jsAddDateRangeRow() {
	if (!document.getElementById('GenAddDateRange')) {
		var TableBody = document.getElementById("DateRangeTable").tBodies[0];
		var RowIndex = document.getElementById("AddDateRange").rowIndex - 1;
		//var RowIndex = 1;
		var NewRow = TableBody.insertRow(RowIndex);
		NewRow.id = "GenAddDateRange";
		
		// Code (1st cell)
		var NewCell0 = NewRow.insertCell(0);
		var tempInput = '&nbsp;';
		NewCell0.innerHTML = tempInput;
		
		// Title (Eng) (2nd Cell)
		var NewCell1 = NewRow.insertCell(1);
		var tempInput = '<input type="text" name="StartDate" id="StartDate" size="10" maxlength="10" value="" class="textbox"/>';
		NewCell1.innerHTML = tempInput;
		
		// Title (Chi) (3rd Cell)
		var NewCell2 = NewRow.insertCell(2);
		var tempInput = '<input type="text" name="EndDate" id="EndDate" size="10" maxlength="10" value="" class="textbox"/>';
		NewCell2.innerHTML = tempInput;
		
		// init a Date Picker
		$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
		$('#StartDate').datepick({
			dateFormat: 'yy-mm-dd',
			dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
			changeFirstDay: false,
			firstDay: 0
		});
		
		// init a Date Picker
		$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
		$('#EndDate').datepick({
			dateFormat: 'yy-mm-dd',
			dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
			changeFirstDay: false,
			firstDay: 0
		});
		
		// Add and Reset Buttons
		var NewCell3 = NewRow.insertCell(3);
		var temp = '<?=$btnAddFloor?>';
		temp += '&nbsp;';
		temp += '<?=$btnCancelFloor?>';
		NewCell3.innerHTML = temp;
		
		$('#FloorCode').focus();
		// Code validation
		js_Add_KeyUp_Code_Checking("Floor", "FloorCode", "CodeWarningDiv_New");
	}
}
function js_Code_Validation(RecordType, InputValue, TargetID, WarningDivID)
{
	$.post(
		"ajax_validate_code.php", 
		{
			RecordType: RecordType,
			InputValue: InputValue,
			TargetID: TargetID
		},
		function(ReturnData)
		{
			if (ReturnData == "")
			{
				// valid code
				$('#' + WarningDivID).hide(ShowHideSpeed);
			}
			else
			{
				// invalid code => show warning
				$('#' + WarningDivID).html(ReturnData);
				$('#' + WarningDivID).show(ShowHideSpeed);
				
				//$('#debugArea').html('aaa ' + ReturnData);
			}
		}
	);
}
function js_Add_KeyUp_Code_Checking(RecordType, ListenerObjectID, WarningDivID, LocationID)
{
	// Code Validation
	$('#' + ListenerObjectID).keyup( function() {
		var inputValue = $(this).val();
		var idArr = $(this).attr('id').split('_');
		var targetID = idArr[1];
		
		if (targetID == null)
			targetID = '';
			
		if (!isNaN(LocationID))
			targetID = LocationID;
			
		js_Code_Validation(RecordType, inputValue, targetID, WarningDivID);
	});
}
function js_Insert_DateRange()
{
	//var Code = encodeURIComponent(Trim($('#FloorCode').val()));
	//var TitleEn = encodeURIComponent(Trim($('#FloorTitleEn').val()));
	//var TitleCh = encodeURIComponent(Trim($('#FloorTitleCh').val()));
	
	var StartDate = encodeURIComponent(Trim($('#StartDate').val()));
	var EndDate = encodeURIComponent(Trim($('#EndDate').val()));
		
	if (!check_text(document.getElementById('StartDate'), 'Please input start date'))
		return false;
	if (!check_text(document.getElementById('EndDate'), 'Please input start date'))
		return false;
		
	$.post(
		"ajax_check_period_date_range.php", 
		{
			"StartDate"	: $("#StartDate").val(),
			"EndDate"	: $("#EndDate").val()
		},
		function(ReturnData)
		{
			alert(ReturnData);
			if (ReturnData == "")
			{
				// valid code
				//$('#CodeWarningDiv_New').hide(ShowHideSpeed);
				
				// Input data valid => disable submit button and then add Floor
				$('#NewDateRangeSubmitBtn').attr("disabled", "true");
				$('#NewDateRangeCancelBtn').attr("disabled", "true");
				
				$.post(
					"ajax_new_period_date_range.php", 
					{ 
						"PeriodID"	: $("#periodID").val(),
						"StartDate"	: $("#StartDate").val(),
						"EndDate"	: $("#EndDate").val()
					},
					function(ReturnData)
					{
						alert(ReturnData);
						//js_Reload_Period_DateRange($("#periodID").val());
					
						// Show system messages
						//Show_System_Message("new", ReturnData);
							
						//$('#debugArea').html(ReturnData);
					}
				);
			}
			else
			{
				// invalid code => show warning
				$('#CodeWarningDiv_New').html(ReturnData);
				$('#CodeWarningDiv_New').show(ShowHideSpeed);
				
				$('#FloorCode').focus();
				return ;
				
				//$('#debugArea').html('aaa ' + ReturnData);
			}
		}
	);
}
function js_Delete_Gen_DateRange_Row()
{
	// Reset
	//$('#FloorCode').val('');
	//$('#FloorTitleEn').val('');
	//$('#FloorTitleCh').val('');
	
	// Delete the Row
	var TableBody = document.getElementById("DateRangeTable").tBodies[0];
	var RowIndex = document.getElementById("GenAddDateRange").rowIndex-1;
	TableBody.deleteRow(RowIndex);
}

function js_Change_BgColor()
{
	var SchoolYearID = $("#academic_year").val();
	var PeriodID = $("#periodID").val();

	$.post(
		"ajax_update_bgcolor.php",
		{
			"SchoolYearID"	: SchoolYearID,
			"PeriodID"		: PeriodID,
			"ColorCode"		: $("#bgColorCode").val()
		},
		function(ReturnData)
		{
			//$('#PeriodNameWarningLayer').html(ReturnData);
		}
	);
}

// jEditable function 
{
function Init_JEdit_Input(objDom)
{
	var WarningLayer = "#PeriodNameWarningLayer";
	var SchoolYearID = $("#academic_year").val();
	
	$(objDom).editable( 
		function(value, settings) {
			var ElementObj = $(this);
			if ($(WarningLayer).html() == "") {
				if(value != ""){		// check period name is empty or not
					if($("#periodID").val() == ""){			// check any period id
						// no period id, create a new period
						$.post(
							"ajax_new_period_name.php",
							{
								"SchoolYearID"	:SchoolYearID,
								"PeriodName"	:value
							},
							function(responseText){
								$("#periodID").val(responseText);
								ElementObj.html(value);		// restore the input box outlook to normal
							}
						);
					}else{
						// have period id, edit current period
						$.post(
							"ajax_edit_period_name.php",
							{
								"SchoolYearID"	: SchoolYearID,
								"PeriodID"		: $("#periodID").val(), 
								"PeriodName"	: value
							},
							function(responseText){
								ElementObj.html(value);		// restore the input box outlook to normal
							}
						);
					}
				}else{
					ElementObj[0].reset();
				}
			}else{
				ElementObj[0].reset();
			}
		},{
			type		: 'text',
			indicator	: '<img src="img/indicator.gif">',
			tooltip		: "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
			event		: "click",
			onblur		: "submit",
			style		: "display: inline",
			height		: "20px"
		}
	);	
	
	$(objDom).keyup(function() {
		var SchoolYearID = $("#academic_year").val();
		var PeriodName = $('form input').val();
		js_Check_PeriodName(SchoolYearID,PeriodName);
	});
}
}

function js_Check_PeriodName(SchoolYearID,PeriodName) {
	var ElementObj = $('#PeriodNameWarningLayer');
	$.post(
		"ajax_check_period_name.php",
		{
			"SchoolYearID"	: SchoolYearID,
			"PeriodName"	: PeriodName
		},
		function(ReturnData)
		{
			if (ReturnData == "1") {
				ElementObj.html('');
				ElementObj.hide();
			} else {
				ElementObj.html('<?=$Lang['SysMgr']['RoleManagement']['RoleNameDuplicateWarning']?>');
				ElementObj.show('fast');
			}
		}
	);
}

// Load system message
function Show_System_Message(Action, Status)
{
	var returnMessage = '';
	if (Action == "new")
	{
		returnMessage = (Status)? '<?=$Lang['General']['ReturnMessage']['AddSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['AddUnsuccess']?>';
	}
	else if (Action == "update")
	{
		returnMessage = (Status)? '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
	}
	else if (Action == "delete")
	{
		returnMessage = (Status)? '<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['DeleteUnsuccess']?>';
	}
	
	Get_Return_Message(returnMessage);
}
</script>