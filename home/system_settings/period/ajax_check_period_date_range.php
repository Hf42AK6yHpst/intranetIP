<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libperiods.php");
include_once($PATH_WRT_ROOT."includes/libperiods_ui.php");
intranet_opendb();

$lperiods = new libperiods();

echo $StartDate." ".$EndDate."<BR>";

if($DateRangeID != "")
	$cond = " WHERE DateRangeID != $DateRangeID";

$sql = "SELECT StartDate, EndDate FROM INTRANET_PERIOD_DATERANGE $cond";
$arrResult = $li->returnArray($sql,2);

$overlapped = 0;		## init a overlap checking count

if(sizeof($arrResult)>0){
	for($i=0; $i<sizeof($arrResult); $i++){
		list($existStartDate, $existEndDate) = $arrResult[$i];
		
		if((($StartDate <= $existStartDate) && ($existStartDate <= $EndDate)) && (($StartDate <= $existEndDate ) && ($existEndDate <= $EndDate))){
			$overlapped = 1;
		}
		if((($existStartDate <= $StartDate) && ($StartDate <= $existEndDate)) && (($StartDate <= $existEndDate) && ($existEndDate <= $EndDate))){
			$overlapped = 1;
		}
		if((($StartDate <= $existStartDate) && ($existStartDate <= $EndDate)) && (($existStartDate <= $EndDate) && ($EndDate <= $existEndDate))){
			$overlapped = 1;
		}
		if((($existStartDate <= $StartDate) && ($StartDate <= $existEndDate)) && (($existStartDate <= $EndDate)&&($EndDate <= $existEndDate))){
			$overlapped = 1;
		}
	}
}

/*
if ($lperiods->checkPeriodName($SchoolYearID,$PeriodName))
	echo '1'; // Period Name is good to use
else
	echo '0'; // Period name is not good to use
*/
intranet_closedb();
?>