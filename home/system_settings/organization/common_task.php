<?php
// Editing by 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($sys_custom['DHL'] && ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])))
{
	intranet_closedb();
	header('HTTP/1.0 401 Unauthorized', true, 401);
	exit;
}

$libdhl = new libdhl();
$JSON = new JSON_obj();

$task = $_REQUEST['task'];

switch($task)
{
	case 'CheckCompany': 
		
		$data = array('CheckCompany'=>1,'CheckCompanyNameChi'=>$_POST['CompanyNameChi']);
		if(isset($_POST['CompanyID'])){
			$data['ExcludeCompanyID'] = $_POST['CompanyID'];
		}
		$chi_name_records = $libdhl->getCompanyRecords($data);
		
		$data = array('CheckCompany'=>1,'CheckCompanyNameEng'=>$_POST['CompanyNameEng']);
		if(isset($_POST['CompanyID'])){
			$data['ExcludeCompanyID'] = $_POST['CompanyID'];
		}
		$eng_name_records = $libdhl->getCompanyRecords($data);
		
		$data = array('CheckCompany'=>1,'CheckCompanyCode'=>$_POST['CompanyCode']);
		if(isset($_POST['CompanyID'])){
			$data['ExcludeCompanyID'] = $_POST['CompanyID'];
		}
		$code_records = $libdhl->getCompanyRecords($data);
		
		$json_ary = array();
		if(count($chi_name_records)>0){
			$json_ary[] = array('CompanyNameChi_Error', $Lang['DHL']['Warnings']['DuplicatedName']);
		}
		if(count($eng_name_records)>0){
			$json_ary[] = array('CompanyNameEng_Error', $Lang['DHL']['Warnings']['DuplicatedName']);
		}
		if(count($code_records)>0){
			$json_ary[] = array('CompanyCode_Error', $Lang['DHL']['Warnings']['DuplicatedCode']);
		}
		
		header("Content-Type: application/json;charset=utf-8");
		echo $JSON->encode($json_ary);
		
	break;
	
	case 'CheckDivision': 
		
		$data = array('CheckDivision'=>1,'CheckDivisionNameChi'=>$_POST['DivisionNameChi']);
		if(isset($_POST['DivisionID'])){
			$data['ExcludeDivisionID'] = $_POST['DivisionID'];
		}
		$chi_name_records = $libdhl->getDivisionRecords($data);
		
		$data = array('CheckDivision'=>1,'CheckDivisionNameEng'=>$_POST['DivisionNameEng']);
		if(isset($_POST['DivisionID'])){
			$data['ExcludeDivisionID'] = $_POST['DivisionID'];
		}
		$eng_name_records = $libdhl->getDivisionRecords($data);
		
		$data = array('CheckDivision'=>1,'CheckDivisionCode'=>$_POST['DivisionCode']);
		if(isset($_POST['DivisionID'])){
			$data['ExcludeDivisionID'] = $_POST['DivisionID'];
		}
		$code_records = $libdhl->getDivisionRecords($data);
		
		$json_ary = array();
		if(count($chi_name_records)>0){
			$json_ary[] = array('DivisionNameChi_Error', $Lang['DHL']['Warnings']['DuplicatedName']);
		}
		if(count($eng_name_records)>0){
			$json_ary[] = array('DivisionNameEng_Error', $Lang['DHL']['Warnings']['DuplicatedName']);
		}
		if(count($code_records)>0){
			$json_ary[] = array('DivisionCode_Error', $Lang['DHL']['Warnings']['DuplicatedCode']);
		}
		
		header("Content-Type: application/json;charset=utf-8");
		echo $JSON->encode($json_ary);
		
	break;
	
	case 'CheckDepartment': 
		
		$data = array('CheckDepartment'=>1,'CheckDepartmentNameChi'=>$_POST['DepartmentNameChi']);
		if(isset($_POST['DepartmentID'])){
			$data['ExcludeDepartmentID'] = $_POST['DepartmentID'];
		}
		$chi_name_records = $libdhl->getDepartmentRecords($data);
		
		$data = array('CheckDepartment'=>1,'CheckDepartmentNameEng'=>$_POST['DepartmentNameEng']);
		if(isset($_POST['DepartmentID'])){
			$data['ExcludeDepartmentID'] = $_POST['DepartmentID'];
		}
		$eng_name_records = $libdhl->getDepartmentRecords($data);
		
		$data = array('CheckDepartment'=>1,'CheckDepartmentCode'=>$_POST['DepartmentCode']);
		if(isset($_POST['DepartmentID'])){
			$data['ExcludeDepartmentID'] = $_POST['DepartmentID'];
		}
		$code_records = $libdhl->getDepartmentRecords($data);
		
		$json_ary = array();
		if(count($chi_name_records)>0){
			$json_ary[] = array('DepartmentNameChi_Error', $Lang['DHL']['Warnings']['DuplicatedName']);
		}
		if(count($eng_name_records)>0){
			$json_ary[] = array('DepartmentNameEng_Error', $Lang['DHL']['Warnings']['DuplicatedName']);
		}
		if(count($code_records)>0){
			$json_ary[] = array('DepartmentCode_Error', $Lang['DHL']['Warnings']['DuplicatedCode']);
		}
		
		header("Content-Type: application/json;charset=utf-8");
		echo $JSON->encode($json_ary);
		
	break;
	
	case 'GetDepartmentUserTable':
	
		$records = $libdhl->getDepartmentUserRecords(array('DepartmentID'=>$_POST['DepartmentID']));
		$record_count = count($records);
		
		$x  = '<table class="common_table_list_v30">';
		$x .= '<tr class="tabletop">
				<th class="num_check">#</th>
				<th>'.$Lang['DHL']['StaffName'].'</th>
				</tr>'."\n";
		if($record_count == 0){
			$x .= '<tr><td colspan="2" align="center">'.$i_no_record_exists_msg.'</td></tr>';
		}else{
			for($i=0;$i<$record_count;$i++){
				$x .= '<tr><td>'.($i+1).'</td><td>'.Get_String_Display($records[$i]['UserName']).'</td></tr>';
			}
		}
		$x .= '</table>';
		
		echo $x;
		
	break;
}



intranet_closedb();
?>