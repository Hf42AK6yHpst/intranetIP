<?php
// Editing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");

intranet_auth();
intranet_opendb();

$add_update_key = isset($_POST['DivisionID']) && $_POST['DivisionID']>0 ? 'Update' : 'Add';

# Check access right
if(!($sys_custom['DHL'] && ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])))
{
	$_SESSION['DHL_DIVISION_RETURN_MSG'] =$Lang['General']['ReturnMessage'][$add_update_key.'Unsuccess'];
	intranet_closedb();
	header("Location:index.php");
	exit;
}

$libdhl = new libdhl();

$libdhl->Start_Trans();

$success_or_recordId = $libdhl->upsertDivisionRecord($_POST);
$division_id = isset($_POST['DivisionID']) && $_POST['DivisionID']>0 ? $_POST['DivisionID'] : $success_or_recordId;

$insert_company_success = false;
if($division_id != '' && $division_id > 0)
{
	$companyIdAry = $_POST['CompanyID'];
	$libdhl->deleteDivisionCompanyRecord($division_id);
	$insert_company_success = $libdhl->insertDivisionCompanyRecord($division_id, $companyIdAry);
	
	$PICIdAry = $libdhl->cleanTargetSelectionData((array)$_POST['DivisionPICID'],'U');
	$PICIdAry= array_values(array_diff($PICIdAry,array(0,'')));
	$libdhl->deletePICRecord('Division',$division_id);
	$libdhl->insertPICRecord('Division',$division_id, $PICIdAry);
}

if($success_or_recordId && $insert_company_success){
	$libdhl->Commit_Trans();
}else{
	$libdhl->RollBack_Trans();
}

$_SESSION['DHL_DIVISION_RETURN_MSG'] = $success_or_recordId && $insert_company_success? $Lang['General']['ReturnMessage'][$add_update_key.'Success'] : $Lang['General']['ReturnMessage'][$add_update_key.'Unsuccess'];

intranet_closedb();
header("Location:index.php");
exit;
?>