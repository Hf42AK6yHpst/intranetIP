<?php
// Editing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");

intranet_auth();
intranet_opendb();

$add_update_key = isset($_POST['CompanyID']) && $_POST['CompanyID']>0 ? 'Update' : 'Add';

# Check access right
if(!($sys_custom['DHL'] && ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])))
{
	$_SESSION['DHL_COMPANY_RETURN_MSG'] =$Lang['General']['ReturnMessage'][$add_update_key.'Unsuccess'];
	intranet_closedb();
	header("Location:index.php");
	exit;
}

$libdhl = new libdhl();


$CompanyInfoArray = array();
$CompanyInfoArray['CompanyID'] = $_POST['CompanyID'];
$CompanyInfoArray['CompanyNameChi'] = $_POST['CompanyNameChi'];
$CompanyInfoArray['CompanyNameEng'] = $_POST['CompanyNameEng'];
$CompanyInfoArray['CompanyCode'] = $_POST['CompanyCode'];
$CompanyInfoArray['Description'] = $_POST['Description'];

$success = $libdhl->upsertCompanyRecord($CompanyInfoArray);

$company_id =  $_POST['CompanyID'];
$PICIdAry = $libdhl->cleanTargetSelectionData((array)$_POST['CompanyPICID'],'U');
$PICIdAry= array_values(array_diff($PICIdAry,array(0,'')));
$libdhl->deletePICRecord('Company',$company_id);
$libdhl->insertPICRecord('Company',$company_id, $PICIdAry);


$_SESSION['DHL_COMPANY_RETURN_MSG'] = $success? $Lang['General']['ReturnMessage'][$add_update_key.'Success'] : $Lang['General']['ReturnMessage'][$add_update_key.'Unsuccess'];

intranet_closedb();
header("Location:index.php");
exit;
?>