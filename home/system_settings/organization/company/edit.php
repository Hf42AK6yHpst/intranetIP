<?php
// Editing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($sys_custom['DHL'] && ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$libdhl = new libdhl();

$is_edit = isset($_REQUEST['CompanyID']) && $_REQUEST['CompanyID']!=''? 1:0;

if($is_edit){
 	$records = $libdhl->getCompanyRecords(array('CompanyID'=>$_REQUEST['CompanyID']));
 	$record = $records[0];
 	$CompanyID = $record['CompanyID'];
 	$companyPICAry = $libdhl->getPICRecords('Company',$record['CompanyID']);	
 	$PICIdAry= Get_Array_By_Key($companyPICAry,'UserID');
 	
 	$hidden_fields = '<input type="hidden" name="CompanyID" id="CompanyID" value="'.$record['CompanyID'].'" />'."\n";
}else{
	$record = array();
}

$CurrentPageArr['OrganizationSettings'] = 1;
$CurrentPage = 'Company';

### Title ###
$PAGE_NAVIGATION[] = array($Lang['DHL']['Company'], "index.php");
$PAGE_NAVIGATION[] = array($is_edit? $Lang['Btn']['Edit'] : $Lang['Btn']['New'], "");
$TAGS_OBJ = $libdhl->getTagsObjArr($CurrentPage);
$MODULE_OBJ['title'] = $Lang['DHL']['Company'];

$linterface->LAYOUT_START(); 
?>
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>
<br />
<form name="form1" id="form1" action="edit_update.php" method="post" onsubmit="return false;">
<?=$hidden_fields?>
<table width="100%" cellpadding="2" class="form_table_v30">
	<thead>
	<col class="field_title">
	<col class="field_c">
	</thead>
	<tbody>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['DHL']['CompanyName'].'('.$Lang['DHL']['Chinese'].')'?> <span class="tabletextrequire">*</span>
			</td>
			<td width="70%">
				<?=$linterface->GET_TEXTBOX_NAME("CompanyNameChi", "CompanyNameChi", $record['CompanyNameChi'], '', array('maxlength'=>255))?>
				<?=$linterface->Get_Thickbox_Warning_Msg_Div("CompanyNameChi_Error",'', "WarnMsg")?>
			</td>
		</tr>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['DHL']['CompanyName'].'('.$Lang['DHL']['English'].')'?> <span class="tabletextrequire">*</span>
			</td>
			<td width="70%">
				<?=$linterface->GET_TEXTBOX_NAME("CompanyNameEng", "CompanyNameEng", $record['CompanyNameEng'], '', array('maxlength'=>255))?>
				<?=$linterface->Get_Thickbox_Warning_Msg_Div("CompanyNameEng_Error",'', "WarnMsg")?>
			</td>
		</tr>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['DHL']['Code']?> <span class="tabletextrequire">*</span>
			</td>
			<td width="70%">
				<?=$linterface->GET_TEXTBOX_NAME("CompanyCode", "CompanyCode", $record['CompanyCode'], '', array('maxlength'=>255))?>
				<?=$linterface->Get_Thickbox_Warning_Msg_Div("CompanyCode_Error",'', "WarnMsg")?>
			</td>
		</tr>
		
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['DHL']['CompanyPIC']?>
			</td>
			<td width="70%">
				<table class="inside_form_table" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<?=$libdhl->getPICSelection('Company',$record['CompanyID']!=''? $record['CompanyID']:0, "CompanyPICID[]", "CompanyPICID[]", $PICIdAry , $___isMultiple=true, $___hasFirst=false, $___firstText='', $___tags=' size="12" ',$valuePrefix='', $hasOptgroupLabel=false)?>
						</td>
						<td>
							<?=$linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "javascript:newWindow('/home/common_choose/index.php?fieldname=CompanyPICID[]&DHLSelectGroup=1&isAdmin=1&SelectStaffOnly=1&CompanyID=$CompanyID', 9)")?><br />
							<?=$linterface->GET_SMALL_BTN($Lang['Btn']['Remove'], "button", "javascript:remove_selection_option('CompanyPICID[]', 0);");?>
						</td>
					</tr>
					<tr>
						<?=$linterface->Get_Thickbox_Warning_Msg_Div("PICUserID_Error",'', "WarnMsg")?>						
					</tr>					
				</table>
			</td>
		</tr>
		
		
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['DHL']['Description']?>
			</td>
			<td width="70%">
				<?=$linterface->GET_TEXTAREA("Description", $record['Description'],80,10)?>
			</td>
		</tr>
	</tbody>
</table>
<?=$linterface->MandatoryField()?>		
<div class="edit_bottom_v30">
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","checkSubmitForm(document.form1);",'submit_btn').'&nbsp;'?>
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location.href='index.php'",'cancel_btn')?>
</div>
</form>
<script type="text/javascript" language="javascript">
function checkSubmitForm(formObj)
{
	var valid = true;
	$('.WarnMsg').hide();
	$('#submit_btn').attr('disabled',true);
	
	var text_objs = document.getElementsByClassName('textbox_name');
	for(var i=0;i<text_objs.length;i++)
	{
		var id = text_objs[i].id;
		var val = $.trim(text_objs[i].value);
		if(val == ''){
			$('#'+id+'_Error').html('<?=$Lang['General']['JS_warning']['CannotBeBlank']?>').show();
			valid = false;
		}
	}
	
	if(valid){
		var form_data = $(formObj).serialize();
		form_data += '&task=CheckCompany';
		$.post(
			'../common_task.php',
			form_data,
			function(returnJson)
			{
				var error_ary = [];
				if(JSON && JSON.parse){
					error_ary = JSON.parse(returnJson);
				}else{
					eval('error_ary='+returnJson+';');
				}
				
				if(error_ary.length > 0){
					valid = false;
					for(var i=0;i<error_ary.length;i++){
						var error_id = error_ary[i][0];
						var error_msg = error_ary[i][1];
						if($('#'+error_id).length>0)
							$('#'+error_id).html(error_msg).show();
					}
					$('#submit_btn').attr('disabled',false);
				}else{
					Select_All_Options('CompanyPICID[]',true);					
					formObj.submit();
				}
			}
		);
	}else{
		$('#submit_btn').attr('disabled',false);
	}
}

$(document).ready(function(){
	$($('input[type=text]')[0]).focus();
});
</script>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>