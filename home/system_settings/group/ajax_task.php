<?php
 
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$Task = stripslashes($_REQUEST['Task']);

switch($Task)
{
	case "refreshCopyFrom":
		$SchoolYear = stripslashes($_REQUEST['SchoolYear']);
		$Selected = stripslashes($_REQUEST['Selected']);
		
		$academic_year = new academic_year();
		
		$YearList = $academic_year->Get_All_Year_List('',0,$SchoolYear);
		$AcademicYearSelection = getSelectByArray($YearList," id='CopyTo' name='CopyTo' onchange='' ",$Selected,0,1);
		
		$returnString = $AcademicYearSelection;
	break;
	
	case "refreshGroupList":
		$CopyFrom = stripslashes($_REQUEST['CopyFrom']);
		$CopyTo = stripslashes($_REQUEST['CopyTo']);
		
		$lgroup = new libgroup();
		$linterface	= new interface_html(); 
		
		$returnString = $lgroup->Get_Group_Mgmt_Copy_UI($CopyFrom,$CopyTo);
	break;
}

echo $returnString;
?>
