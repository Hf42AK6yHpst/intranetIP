<?php
# using:  

############# Change Log [Start] ################
#
#   Date:   2020-04-06 Tommy
#           added "bulk edit" button
#
#	Date:	2019-04-17 Pun [ip.2.5.10.5.1]
#			Added XSS protection
#
#	Date:	2017-10-30	Simon [E129852]
#			Custom: showToolbar when login as group admin user
#
#	Date:	2014-11-04	Omas
#			Improved: Add column showing Admin of the group
#
#	Date:	2012-03-13	Ivan [2012-0308-1653-41071]
#			Improved: only show default group for current academic year
#
#	Date:	2011-11-09	YatWoon
#			Fixed: missing to check the keyword with TitleChinese field [Case#2011-1018-1612-17066]
#
#	Date:	2010-12-13	YatWoon
#			- update return system message display to IP25 standard
#			- display group name according to UI lang
#
#	Date:	2010-08-18	YatWoon
#			- add search field
#			- update to IP25 standard
#
#	Date:	2010-07-14	Marcus
#			add copy btn
#
#	Date:	2010-07-12	Marcus
#			show add/import btn for future academic year also.
#
#	Date:	2010-06-21	YatWoon
#			add online help button
#
# Date: 2010-02-05 Michael
# 		Fixed: incorrect number of users in group. (Caused by duplicate records at INTRANET_USERGROUP)
#
# Date:	2010-01-13 [YatWoon]
#		Fixed: cannot display Identity group in "ALL" category.  (due to academic year id is null)
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################



$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

$arrCookies = array();
$arrCookies[] = array("ck_schoolsettings_group_index_Keyword", "keyword");
$arrCookies[] = array("ck_schoolsettings_group_index_AcademicYearID", "SchoolYear");
$arrCookies[] = array("ck_schoolsettings_group_index_RecordType", "filter");

if ($page_size_change == 1) {
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}
	
intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPage	= "Group";
$CurrentPageArr['Group']=1;

$linterface 	= new interface_html();
$fcm = new form_class_manage();
$libenroll = new libclubsenrol();
$lgroup = new libgroup();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$SchoolYear = IntegerSafe($SchoolYear);
$filter = IntegerSafe($filter);
$keyword = trim(stripslashes($keyword));
$SearchText= $libenroll->Get_Safe_Sql_Like_Query(htmlspecialchars($keyword,ENT_QUOTES));

if($field=="") $field = 0;
if($order=="") $order = 1;
switch ($field){
     case 0: $field = 0; break;
     case 1: $field = 1; break;
     case 2: $field = 2; break;
     case 3: $field = 3; break;
     default: $field = 0; break;
}


if($SchoolYear=="") 
	$SchoolYear=Get_Current_Academic_Year_ID();

$AcademicObj = new academic_year($SchoolYear);
$YearPassed = $AcademicObj->Has_Academic_Year_Past();

$isCurrentYear=($SchoolYear==Get_Current_Academic_Year_ID());

if(!$isCurrentYear && $filter==0) // do not show idendity group if !isCurrentYear
	$filter='';
	
if ($filter != "") $type_conds = "AND a.RecordType = $filter";
	
if($filter!=0 ||$filter=='') //idendity group does not define AcademicYearID
{
	$type_conds .= " AND (a.AcademicYearID = '$SchoolYear' or a.AcademicYearID is NULL)";
}

if (!$isCurrentYear) {
	$RecordType_conds = " And a.RecordType != 0 ";
}

// use to get no. of group members of sem based clubs in current semester
$TermInfoArr = getCurrentAcademicYearAndYearTerm();
$curSem = $TermInfoArr['YearTermID'];

$title_field = $intranet_session_language == "en" ? "a.Title" : "a.TitleChinese";
$sql  = "SELECT
                        CONCAT('<a class=tablelink href=javascript:info(', a.GroupID, ')><img src=\'{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\' border=\'0\'></a>'),
                        CONCAT('<a class=tablelink href=edit.php?GroupID[]=', a.GroupID, '>', ". $title_field .", '</a>'),
                        ifnull(a.Description, '".$Lang['General']['EmptySymbol']."'),
						'' as PIC,
                        a.GroupID,
                        a.DateModified,
                        IF((a.RecordStatus='1' OR a.RecordStatus='3'),'<img src={$image_path}/red_checkbox.gif vspace=3 hspace=4 border=0>',CONCAT('<input type=checkbox name=GroupID[] value=', a.GroupID ,'>')),
                        IF(IsNull(b.GroupID), 0, COUNT(a.GroupID)) AS Count,
						a.RecordType
                FROM
                        INTRANET_GROUP AS a
                		LEFT JOIN INTRANET_USERGROUP b ON a.GroupID = b.GroupID
                		LEFT JOIN INTRANET_USER as c on (c.UseRID=b.UserID) AND (c.RecordType <> 2 OR (c.ClassName IS NOT NULL AND c.ClassNumber IS NOT NULL AND TRIM(c.ClassName) <> '' AND TRIM(c.ClassNumber) <> '' ))
                WHERE
                        (a.Title like '%$SearchText%' or a.TitleChinese like '%$SearchText%' OR a.Description like '%$SearchText%')
                        $type_conds
						$RecordType_conds
                GROUP BY a.GroupID";

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array($title_field, "a.Description", "Count",  "a.DateModified", "a.GroupID");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;
$li->title = $i_admintitle_group;
$li->column_array = array(0,0,18,18,17,0);
$li->wrap_array = array(0,0,0,0,0,0);
$li->IsColOff = "GroupSettingGroupList";
$li->count_mode = 1;

// TABLE COLUMN
$count = 0;
$li->column_list .= "<td width=1 class='tabletopnolink'>#</td>\n";
$li->column_list .= "<td width=1 class='tabletopnolink'><br></td>\n";
$li->column_list .= "<td width=25% class='tabletopnolink'>".$li->column($count++, $Lang['Group']['GroupName'])."</td>\n";
$li->column_list .= "<td width=25% class='tabletopnolink'>".$li->column($count++, $Lang['Group']['GroupDescription'])."</td>\n";
$li->column_list .= "<td width=15% class='tabletopnolink'>".$Lang['Group']['RolesAdmin']."</td>\n";
$li->column_list .= "<td width=10% nowrap class='tabletopnolink' align='center'>".$li->column($count++, $Lang['Group']['NoOfMember'])."</td>\n";
$li->column_list .= "<td width=20% class='tabletopnolink'>".$li->column($count++, $i_GroupDateModified)."</td>\n";
$li->column_list .= "<td width=1 class='tabletopnolink'>".$li->check("GroupID[]")."</td>\n";


### Button
//$GroupCatTool = "<a href=\"groupcategory/\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icons_manage.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" Title=\"Edit Group Category\"/> </a>";
//$RoleSetting = "<a href=\"role/\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icons_manage.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" Title=\"Role Setting\"/> ".$Lang['Group']['RoleSetting']."</a>";


// 2012-0903-1628-12071
# showToolbar when login as group admin user [E129852]
$showToolbar = false;
if (isset($sys_custom['Group']['CanEditPastYearGroupUserIDArr']) && in_array($_SESSION['UserID'], $sys_custom['Group']['CanEditPastYearGroupUserIDArr'])) {
	$showToolbar = true;
}
else if(!$YearPassed) {
	$showToolbar = true;
}
else if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]){
	$showToolbar = true;
}


//if($isCurrentYear)
//if(!$YearPassed)
if ($showToolbar)
{
	if($filter!=0||$filter=='')
	{
		$AddBtn 	= "<a href=\"javascript:checkNew('new.php?SchoolYear=$SchoolYear')\" class='add'>" . $button_new . "</a>";
		$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'GroupID[]','remove.php')\" class=\"tool_delete\">" . $button_delete . " </a>";
		//$delBtn 	= "<a href=\"javascript:checkRemoveThis(document.form1,'NoticeIDArr[]','remove_update.php')\" class=\"tool_delete\">$button_delete</a>";

		$ImportBtn 	= "<a href=\"javascript:checkNew('group_import.php?SchoolYear=$SchoolYear')\" class='import'> " . $button_import . "</a>";
		$ImportMemberBtn 	= "<a href=\"javascript:checkNew('group_member_import.php?SchoolYear=$SchoolYear')\" class='import'> " . $button_import." ".$Lang["Group"]["GroupMember"] . "</a>";
		
	}	
	
	$bulk_editBtn 	= "<a href=\"javascript:checkEditMultiple2(document.form1, 'GroupID[]', CheckBulkEdit(document.form1, 'GroupID[]', 'bulk_edit.php'))\" class=\"tool_edit\">" . $Lang['Btn']['Bulk_Edit'] . "</a>";
	
	//$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'GroupID[]','edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";
	$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'GroupID[]','edit.php')\" class=\"tool_edit\"> " . $button_edit . "</a>";
}

if($filter!=0||$filter=='') {
	$CopyBtn 	= "<a class=\"copy\" href=\"javascript:checkNew('copy_group.php?SchoolYear=$SchoolYear')\" >" . $Lang['Group']['CopyFromOtherYear'] . "</a>";
}

if($filter!=0&&$filter!=''){
//	$ExportBtn = $linterface->Get_Content_Tool_v30("export","export_all_group.php?AcademicYearID=$SchoolYear&filter=$filter");
	$exportMember = "<a href=\"export_all_group.php?AcademicYearID=$SchoolYear&filter=$filter\" class=\"sub_btn\"> ".$Lang['General']['Member2']."</a>";
	$exportGroupInfo = "<a href=\"export_all_group_info.php?AcademicYearID=$SchoolYear&filter=$filter\" class=\"sub_btn\"> ".$Lang['eBooking']['Settings']['FollowUpGroup']['NewGroupStepArr']['Step1']."</a>";

	$ExportBtn = '
					<div class="btn_option"  id="ExportDiv"  >
							<a onclick="js_Clicked_Option_Layer(\'export_option\', \'btn_export\');" id="btn_export" class="export option_layer" href="javascript:void(0);"> '.$Lang['Btn']['Export'].'</a>
							<br style="clear: both;">
								<div onclick="js_Clicked_Option_Layer_Button(\'export_option\', \'btn_export\');" id="export_option" class="btn_option_layer" style="visibility: hidden;">
								'.$exportMember.'
								'.$exportGroupInfo.'
							</div>
					</div>'; 	
}

$SchoolYearList = $fcm->Get_Academic_Year_List();
$yearFilter .= '				<select name="SchoolYear" id="SchoolYear" onchange="this.form.submit();">';
$yearFilter .= '					<option value="">'.$Lang['SysMgr']['FormClassMapping']['SelectSchoolYear'].'</option>';
for ($i=0; $i< sizeof($SchoolYearList); $i++) {
	$SchoolYearName = Get_Lang_Selection($SchoolYearList[$i]['YearNameB5'],$SchoolYearList[$i]['YearNameEN']);
	
	unset($Selected);
	$Selected = ($SchoolYear== $SchoolYearList[$i]['AcademicYearID'])? 'selected':'';
	$yearFilter .= '<option value="'.$SchoolYearList[$i]['AcademicYearID'].'" '.$Selected.'>'.$SchoolYearName.'</option>';
}
$yearFilter .= '				</select>';

$lgc = new libgroupcategory();

$selection = $lgc->returnSelectCategory("name=filter onChange=this.form.submit()",!$isCurrentYear,1,$filter);
$filterbar = $yearFilter."<img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"5\">$i_GroupCategoryName: $selection".$GroupCatTool;

$searchTag = "<input name=\"keyword\" id=\"searchText\" type=\"text\" value=\"".$keyword."\" onkeyup=\"Check_Go_Search(event);\"/>";


$CurrentPageArr['Group'] = 1;

### Title ###
$PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmtCtr'], "");

//$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"index.php?clearCoo=1",1);
//$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"groupcategory/");
//$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"role/");
$TAGS_OBJ = $lgroup->getTopTabInfoAry('group');

# change page web title
$js = '<script type="text/JavaScript" language="JavaScript">'."\n";
$js.= 'document.title="eClass Group";'."\n";
$js.= '</script>'."\n";

$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'].$js;

# online help button
//$onlineHelpBtn = gen_online_help_btn_and_layer('school_settings','group');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);   

?>

<script language="JavaScript1.2">

function icon(id){
        url = "icon/index.php?GroupID=" + id;
        newWindow(url,2);
}
function info(id){
        url = "info/info.php?GroupID=" + id;
        newWindow(url,2);
}
function user(id){
        f = document.form1.filter.value;
        url = "info/index.php?filter=" + f + "&GroupID=" + id+"&SchoolYear=<?=$SchoolYear?>" ;
        self.location.href = url;
}

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}

function CheckBulkEdit(obj, element, page){
	var checkboxes = document.getElementsByName("GroupID[]");
	
	var group = [];
	for (var i=0; i<checkboxes.length; i++) {   
    	if (checkboxes[i].checked) {
    		group.push(checkboxes[i].value);
        }
    }

    if(group.length < 2){
        alert("<?=$Lang['Group']['jsWarningMsgArr']['editSingleGroup']?>");
    }else{
		obj.action=page;
		obj.submit();
    }
}

</script>

<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<br />
<form name="form1" method="get">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td colspan="2">
		<div class="content_top_tool">
				<div class="Conntent_tool">
					<?=$AddBtn?>
					<?=$ImportBtn?>
					<?=$ImportMemberBtn?>
					<?=$ExportBtn?>
					<?=$CopyBtn?>
				</div>
				<div class="Conntent_search"><?=$searchTag?></div>
			<br style="clear:both" />
		</div>
	</td>
</tr>
<tr>
    <td><div class="table_filter"><?=$filterbar?></div></td>
</tr>
<tr class="common_table_tool">
<!--	<td valign="bottom"><div class="table_filter">--><?//=$filterbar?><!--</div></td>-->
	<td valign="bottom">
		<?
		//if(!$YearPassed) {
		if($showToolbar) { 
		?>
		<div><?=$bulk_editBtn?><?=$editBtn?><?=$delBtn?></div>
		<? } ?>
	</td>
</tr>

<tr>
	<td valign="bottom" colspan="2">
		<?=$li->display();?>
	</td>
</tr>
</table>


<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">

</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
