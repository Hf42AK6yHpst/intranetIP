<?php
# using: 

/*	Change Log


2017-06-06 (Icarus): Modified the HTML code, change the CSS of the form to reach the UI consistency to the sample page. 
						The original code has been commented.					
*/

#################### Change Log [Start]
#
#	Date:	2010-12-13	YatWoon
#			display group title according to UI lang 
#
#################### Change Log [End]

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Group";
$CurrentPageArr['Group'] = 1;

$linterface 	= new interface_html();
$lgroup = new libgroup();

### get Academic Year Name ###
$FromYear = new academic_year($CopyFrom);
$FromYearName = Get_Lang_Selection($FromYear->YearNameB5,$FromYear->YearNameEN);
$ToYear = new academic_year($CopyTo);
$ToYearName = Get_Lang_Selection($ToYear->YearNameB5,$ToYear->YearNameEN);

### Build table'
foreach((array) $GroupIDList as $key => $GroupID)
{
	$Title = $GroupNameList[$key];
	$TitleChinese = $GroupNameChineseList[$key];
	$CategoryName = $GroupCatList[$key];
	if(in_array($GroupID,(array)$CopyGroup))
	{
		$HiddenValue .= '<input type="hidden" name="CopyGroup[]" value="'.$GroupID.'">';
		$IsCopyGroup = $i_general_yes;
	}
	else
		$IsCopyGroup = "<span class='tabletextremark'>$i_general_no</span>";
		
	if(in_array($GroupID,(array)$CopyMember))
	{
		$HiddenValue .= '<input type="hidden" name="CopyMember[]" value="'.$GroupID.'">';
		$IsCopyMember = $i_general_yes;
	}
	else
		$IsCopyMember = "<span class='tabletextremark'>$i_general_no</span>";

	$TitleDisplay = $intranet_session_language == "en" ? $Title : $TitleChinese;
	$tbody .= '<tr>';
		$tbody .= '<td class="tabletext">'.$CategoryName.'</td>';
		$tbody .= '<td class="tabletext">'.$TitleDisplay.'</td>';
		$tbody .= '<td class="tabletext" align="center">'.$IsCopyGroup.'</td>';
		$tbody .= '<td class="tabletext" align="center">'.$IsCopyMember.'</td>';
	$tbody .= '</tr>';
}

$table .= '<table border="0" cellpadding="0" cellspacing="0" class="common_table_list '.$class.'">';
	$table .= '<thead>';
		$table .= '<tr>';
			$table .= '<th width="35%">'.$Lang['Group']['Category'].'</td>';
			$table .= '<th width="35%">'.$Lang['Group']['Group'].'</td>';
			$table .= '<th width="15%" style="text-align:center">'.$Lang['Group']['Copy'].'</td>';
			$table .= '<th width="15%" style="text-align:center">'.$Lang['Group']['CopyMember'].'</td>';
		$table .= '</tr>';
	$table .= '</thead>';
	$table .= '<tbody>';
		$table .= $tbody;
	$table .= '</tbody>';
$table .= '</table>';

### Get Btn
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "javascript:jsGoBack()","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$NextBtn = $linterface->GET_ACTION_BTN($button_continue, "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$CancelBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='index.php'","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$Btn = $NextBtn."&nbsp;".$BackBtn."&nbsp;".$CancelBtn;

### Title ###
$PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmt'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Group']['CopyGroup'], "");

//$TAGS_OBJ[] = array($Lang['Group']['CopyGroup'],"",1);
$TAGS_OBJ = $lgroup->getTopTabInfoAry('group');

$STEPS_OBJ[] = array($Lang['Group']['Options'], 0);
$STEPS_OBJ[] = array($Lang['Group']['Confirmation'], 1);
$STEPS_OBJ[] = array($Lang['Group']['CopyResult'], 0);

$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];

$linterface->LAYOUT_START();   
?>
<br />
<form name="frm1" method="POST" action="copy_result.php" >
<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
<div style="padding-top:25px;"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></div>
<br />
<table class="form_table_v30">
	<tr>
		<td class="field_title"><?=$Lang['Group']['CopyFrom']?></td>
		<td><?=$FromYearName?></td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['Group']['CopyTo']?></td>
		<td><?=$ToYearName?></td>
	</tr>
</table>
<br style="clear:both;" />
<table class="form_table_v30">
	<tr>
		<td style="border-bottom-style:none;"><?=$linterface->GET_NAVIGATION2($Lang['Group']['Group'])?></td>
	</tr>
	<tr>
		<td style="border-bottom-style:none;"><?=$table?></td>
	</tr>
</table>
<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?=$Btn?>
	<p class="spacer"></p>
</div>



<!--	THE ORIGINAL CODE
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr><td colspan="2"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td clospan="2" align="center">
			<table width="90%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td colspan="5">
				<table width="100%">
					<tr>
						<td class='formfieldtitle' width="30%"><?=$Lang['Group']['CopyFrom']?></td>
						<td class='tabletext'><?=$FromYearName?></td>
					</tr>
					<tr>
						<td class='formfieldtitle' width="30%"><?=$Lang['Group']['CopyTo']?></td>
						<td class='tabletext'><?=$ToYearName?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr><td colspan="2"><?=$linterface->GET_NAVIGATION2($Lang['Group']['Group'])?></td></tr>
			</table>
			<table width="90%">
				<tr>
					<td><?=$table?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
				<?=$Btn?>
				</td>
			</tr>
        </table> 
		</td>
	</tr>
</table>
-->


<input type="hidden" value="<?=$CopyFrom?>" name="CopyFrom">
<input type="hidden" value="<?=$CopyTo?>" name="CopyTo">
<?=$HiddenValue?>
</form>
<br><br>
<script>
function jsGoBack()
{
	document.frm1.action = "copy_group.php";
	document.frm1.method = "POST";
	document.frm1.submit();
}
</script>

<?
$linterface->LAYOUT_STOP();
?>
