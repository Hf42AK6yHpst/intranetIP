<?php

// Modifying by:  
############# Change Log [Start] ################
# Date: 2015-10-07 Omas
# 	  Update Import description
#
# Date: 2015-06-17 Shan
# 	  Update UI standard, add data column
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPage	= "Group";
$CurrentPageArr['Group'] = 1;
$lgroup = new libgroup();
$linterface 	= new interface_html();

### download csv
$csvFile = "<a class='tablelink' href='". GET_CSV("group_member_import.csv") ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";

### get Academic Year Selection (current and future year only) ###
$academic_year = new academic_year();
$YearList = $academic_year->Get_All_Year_List('',1);
$AcademicYearSelection = getSelectByArray($YearList," id='SchoolYear' name='SchoolYear' ",$SchoolYear,0,1);

### Title ###
$PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmtCtr'], "index.php");
$PAGE_NAVIGATION[] = array($button_import, "");

//$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"",1);
//$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"groupcategory/");
//$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"role/");
$TAGS_OBJ = $lgroup->getTopTabInfoAry('group');

$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];
$ReturnMsg = $Lang['General']['ReturnMessage'][$xmsg];
$linterface->LAYOUT_START($ReturnMsg);   


$WarningMsg = '<ol>'."\n";
$WarningMsg .= '<li>'.$Lang['Group']['AddUserInstrution'].'</li>'."\n";
$WarningMsg .= '<li>'.$Lang['Group']['AddUserInstrution2'].'</li>'."\n";
$WarningMsg .= '<li>'.$Lang['Group']['AdminRightArr']['MemberManagement_eEnrolRemarks'].'</li>'."\n";
$WarningMsg .= '</ol>'."\n";

### data column
$DataColumnTitleArr = array($i_ClassName,$Lang['General']['ClassNumber'],$Lang['General']['UserLogin'],$Lang['AccountMgmt']['StudentImportFields']['13'],$Lang['SysMgr']['FormClassMapping']['StudentName'],$Lang['AccountMgmt']['Settings']['GroupName']);
$DataColumnPropertyArr = array(4,4,4,4,2,1);
$DataColumnRemarksArr = array();
$DataColumn = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr, $DataColumnRemarksArr);

$format_str .= $DataColumn;

?>
<script>
function CheckForm ()
{
	if(document.frm1.action.search("import_member_update.php")&&!$("#csvfile").val())  //not select upload file yet
	{
		alert("<?=$Lang['General']['warnSelectcsvFile']?>");
		return false;
	}
	
}

function SubmitForm(url)
{
	document.frm1.action = url
}


</script>
<form method="POST" name="frm1" id="frm1" action="import_member_update.php" enctype="multipart/form-data" onsubmit="return CheckForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td>
       <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
         <tr> 
			<td><?=$linterface->Get_Warning_Message_Box("",$WarningMsg)?></td>
         </tr>
          
         <tr>
         <td>
       		<table align="center" width="80%" border="0" cellpadding="5" cellspacing="0" class="form_table_v30">			
				<!--<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<?=$i_Discipline_Target?> <span class="tabletextrequire">*</span>
				</td>
				<td>
				<table><tr>
				<td valign="top">
				 <select name="importTarget" id="importTarget" onChange="showResult(this.value);">
				 <option value="form" <? if($importTarget=="form") { echo "selected";} ?>><?=$Lang['eSports']['Form']?></option>
				 <option value="class" <? if($importTarget=="class") { echo "selected";} ?>><?=$Lang['eSports']['Class']?></option>
				 </select>
				</td>
				<td>
					<?=$FormSelection?><?=$ClassSelection?>
					<br><?= $linterface->GET_ACTION_BTN($button_select_all, "button", "SelectAll();", "selectAllBtn01")?>
					<?= $linterface->GET_ACTION_BTN($button_export, "submit", "SubmitForm('export.php')","submit3","") ?>
				</td>
				</tr></table>
				</td>
				</tr>-->
				<tr>
				<td class="field_title"  align="left"><?=$Lang['SysMgr']['FormClassMapping']['SchoolYear']?> </td>
				<td class="tabletext"><?=$AcademicYearSelection?></td>
				</tr>					
				<tr>
				<td class="field_title"  align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span><span class="tabletextrequire">*</span></td>
				<td class="tabletext"><input class="file" type="file"  id = "csvfile" name="csvfile"></td>
				</tr>					
				<tr>
				<td class="field_title"  align="left"><?=$Lang['General']['CSVSample']?> </td>
				<td class="tabletext"><?=$csvFile?></td>
				</tr>		
				<tr>
				<td width="35%" valign="top" nowrap="nowrap" class="field_title" ><?= $i_general_Format ?></td>					
		  		<td class="tabletext"><?=$format_str?></td>
		  		</tr>  	
			</table>
		 </td>
		</tr>

		<tr>
		<td>    
		  <table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
			<tr>
            <td align="left" class="tabletextremark"><?=$i_general_required_field2?></td>
			</tr>
			<tr>
            <td align="left" class="tabletextremark"><?=$Lang['General']['ReferenceField']?></td>
			</tr>
			<tr>
            <td align="left" class="tabletextremark"><?=$Lang['General']['OtherField']?></td>
			</tr>
			<tr>
             <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
           	</tr>
            <tr>
			 <td align="center">
				<?= $linterface->GET_ACTION_BTN($button_import, "submit", "SubmitForm('import_member_update.php')","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>				
			</td>
			</tr>
       	</table> 
                      
	  </td>
	  </tr>
	</table>  
			
 	</td>
 </tr>
</table>
</form>

<?

 $linterface->LAYOUT_STOP();
?>

