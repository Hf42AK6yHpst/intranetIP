<?php
# using:

############# Change Log [Start] ################
#	Date:	2019-04-17 Pun [ip.2.5.10.5.1]
#			Added XSS protection
#   Date:   2018-06-22  Vito
#           Add export function
#	Date:	2017-06-07	Icarus
#			fixed last modified sorting
#
#	Date:	2015-10-19	Kenneth Yau
#			add SearchBox Layout
#			encode search keyword's html characters by intranet_htmlspecialchars()
#			decode shows search keyword's html characters by intranet_undo_htmlspecialchars()
#			edit system message - show only when $xmsg is not null 
#
#	Date:	2010-06-21	YatWoon
#			add online help button
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPage	= "Group";
$CurrentPageArr['Group']=1;

$lgroup = new libgroup();
$linterface 	= new interface_html();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

# TABLE SQL
$ldb = new libdb();
$keyword = stripslashes($keyword);
$keyword = intranet_htmlspecialchars(trim($keyword));
$keyword = $ldb->Get_Safe_Sql_Like_Query($keyword);
$filter = IntegerSafe($filter);

//debug_pr($keyword);
//switch ($field){
//        case 0: $field = 0; break;
//        case 1: $field = 1; break;
//        case 2: $field = 2; break;
//        default: $field = 0; break;
//}
$field = ($field=='')? 0 : $field;

$order = ($order == 1) ? 1 : 0;
if ($filter != "") $type_conds = "AND a.RecordType = $filter";
$sql  = "SELECT
                        CONCAT('<a class=tablelink href=edit.php?RoleID[]=', a.RoleID, '>', a.Title, '</a>'),
                        a.Description,
                        b.CategoryName,
                        a.DateModified,
                        IF(a.RecordStatus='1','<img src={$image_path}/red_checkbox.gif vspace=3 hspace=4 border=0>',CONCAT('<input type=checkbox name=RoleID[] value=', a.RoleID ,'>'))
                FROM
                        INTRANET_ROLE as a LEFT OUTER JOIN INTRANET_GROUP_CATEGORY as b
                                      ON a.RecordType = b.GroupCategoryID
                WHERE
                        (a.Title like '%$keyword%' OR a.Description like '%$keyword%')
                        $type_conds
                ";

//debug_pr($filter);
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.Title", "a.Description","b.CategoryName", "a.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_role;
$li->column_array = array(0,1,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column(0, $i_RoleTitle)."</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column(1, $i_RoleDescription)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(2, $i_GroupCategoryName)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(3, $i_RoleDateModified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("RoleID[]")."</td>\n";

// TABLE FUNCTION BAR
$lgc = new libgroupcategory();
$selection = $lgc->returnSelectCategory("name=filter onChange=this.form.submit()",false,1,$filter);
$filterbar = $Lang['Group']['ViewThisCategoryOfGroupOnly']." ".$selection;
//  debug_pr($selection);
$presetValue 	= "<a href=\"javascript:checkEdit(document.form1,'RoleID[]','set.php')\"  class='tabletool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_common.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\"> " . $Lang['Group']['Role']['PresetValue'] . "</a>";
$setDefault 	= "<a href='set_default.php' class='contenttool'><img src='{$image_path}/red_checkbox.gif' vspace=3 hspace=4 border=0 align=\"absmiddle\" /> " . $Lang['Group']['DefaultGroupRole'] . "</a>";
$AddBtn 	= "<a href=\"javascript:checkNew('new.php?filter=$filter')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'RoleID[]','remove.php', '".$Lang['Group']['jsWarningMsgArr']['deleteRole']."')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_delete . "</a>";
$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'RoleID[]','edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";
$exportBth  = "<a href=\"javascript:js_Go_Export()\" class=\"contenttool\" class=\"contenttool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_export.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_export . "</a>";
//$exportBth  = "<a href=\"javascript:js_Go_Export();\" class=\"contenttool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_export.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_export . "</a>";
//debug_pr($exportBth);
/*
$toolbar  = "<a class=iconLink href=javascript:checkPost(document.form1,'new.php')>".newIcon()."$button_new</a>".toolBarSpacer();
$toolbar .= "<a class=iconLink href='set_default.php'><img src=../../images/red_checkbox.gif hspace=5 vspace=0 border=0 align=absmiddle>$i_RoleDefault</a>";
$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'RoleID[]','set.php')\"><img src='/images/admin/button/t_btn_preset_value_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'RoleID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'RoleID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
*/

### Title ###
//$PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmtCtr'], "../index.php");
$PAGE_NAVIGATION[] = array($i_admintitle_am_role, "");

//$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"../");
//$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"../groupcategory/");
//$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"",1);
$TAGS_OBJ = $lgroup->getTopTabInfoAry('groupRole');
$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];

# online help button
$onlineHelpBtn = gen_online_help_btn_and_layer('school_settings','group');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

### search box
$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', stripslashes(stripslashes(intranet_undo_htmlspecialchars($keyword))));
###	system message - show only when $xmsg is not null
$x = '';
if($xmsg != ''){
	$x =  '<tr><td colspan="2" align="right" valign="bottom">'.$linterface->GET_SYS_MSG($xmsg).'</td></tr>';
}
$htmlAry['systemMsgTr'] = $x;

$linterface->LAYOUT_START();  

?>
<script type  ="text/javascript">

function js_Go_Export(){
	$('form#form2').attr('target', '_self').attr('action', 'export_role.php').submit();
	//window.location("export.php");
}

</script>

<form name="form1" method="post">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left" class="tabletext">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<?= $htmlAry['systemMsgTr'] ?>
						<tr>
							<td>
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><?=$AddBtn?></td>
										<td><?="&nbsp" . $exportBth?></td>
										<td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
										<td><p><?=$setDefault?></p></td>
									</tr>
								</table>
							</td>
							<td><?=$htmlAry['searchBox']?></td>
						</tr>

						<tr class="table-action-bar">
							<td><?=$filterbar?></td>
							<td align="right" valign="bottom" colspan="2">
								<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
									<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
										<table border="0" cellspacing="0" cellpadding="2">
											<tr>
												<td nowrap><?=$presetValue?></td>
												<td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
												<td nowrap><?=$editBtn?></td>
												<td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
												<td nowrap><?=$delBtn?></td>
											</tr>
										</table>
									</td>
									<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
								</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<?= $li->display();?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">

</form>

<form id="form2" name="form2" method="post">
	<input type=hidden id=exportGroupRoleFilterHidden name=exporGroupRoleFilter value="<?php echo $filter;?>">
	<input type=hidden id=exportGroupRoleKeywordHidden name=exporGroupRoleKeyword value="<?php echo $keyword;?>">
	<input type=hidden id=exportGroupRoleOrderHidden name=exporGroupRoleOrder value="<?php echo $li->order;?>">
	<input type=hidden id=exportGroupRoleFieldHidden name=exportGroupRoleField value="<?php echo $li->field?>">
</form>
<?php 
// debug_pr($li->order);
// debug_pr($li->field);
// debug_pr($filter);
?>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();

?>