<?php

############# Change Log [Start] ################
# Date: 2016-01-11 Kenneth
#		- Check is the role exists in same category
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../../";

include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$li = new libdb();

$Title = intranet_htmlspecialchars(trim($Title));
$Description = intranet_htmlspecialchars(trim($Description));

//debug_pr($RecordType);

//Check existing
$sql = 'SELECT Title, GroupCategoryID 
		From INTRANET_ROLE as a LEFT OUTER JOIN INTRANET_GROUP_CATEGORY as b
                                      ON a.RecordType = b.GroupCategoryID
		WHERE GroupCategoryID = '.$RecordType.' and Title = \''.$Title.'\'';
$returnArray = $li->returnArray($sql);


if(!empty($returnArray)){ //name is exist in same category
	$xmsg='add_failed';
}else{
	if($presetValue==1)
	{
		$sql = "UPDATE INTRANET_ROLE SET RecordStatus = 0 WHERE RecordType = ".$RecordType;
		$li->db_db_query($sql);
	}
	
	
	$sql = "INSERT INTO INTRANET_ROLE (Title, Description, RecordType, RecordStatus, DateInput, DateModified) VALUES ('$Title', '$Description', '$RecordType', $presetValue ,now(), now())";
	$li->db_db_query($sql);
	$xmsg='add';
}





intranet_closedb();
header("Location: index.php?filter=$RecordType&xmsg=$xmsg");
?>
