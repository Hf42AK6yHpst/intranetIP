<?php
# using:

/************************************
 *  Modification log
 *  2018-09-13 Chris
 *   - added in Gsuite SSO
 *
 *	2010-12-21	YatWoon
 *	- add option for group "allow delete others announcement"
 *
 *	2010-12-13 YatWoon
 *	- add "TitleChinese"
 *	- update return system message with IP25 standard
 *
 * 		20100715 Marcus:
 * 			- move group adding related  programming in function Group_Adding_Additional_Task.
 * 				As same task is performed in copy group, group import, add group.
 * **********************************/

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/liborganization.php");

intranet_auth();
intranet_opendb();

$li = new libdb();


$Title = intranet_htmlspecialchars(trim($Title));
$TitleChinese = intranet_htmlspecialchars(trim($TitleChinese));
$Description = intranet_htmlspecialchars(trim($Description));
$Quota = intranet_htmlspecialchars(trim($Quota));
if (!is_numeric($Quota) || $Quota < 0) $Quota = 5;

/* 20100316 Ivan: Default allow all access right
 if ($alltools == 1)
 {
 $functionAccess = 'NULL';
 }
 else
 {
 # Change the tool rights to integer
 $sum = 0;
 for ($i=0; $i<sizeof($grouptools); $i++)
 {
 $target = intval($grouptools[$i]);
 if ($target == 5)        # Question bank is for academic only
 {
 if ($RecordType == 2)
 {
 $sum += pow(2,$target);
 }
 }
 else
 {
 $sum += pow(2,$target);
 }
 }
 $functionAccess = $sum;
 }
 */
$functionAccess = 'NULL';

$fieldname = "Title, Description, RecordType, DateInput, DateModified, StorageQuota, FunctionAccess, AcademicYearID, DisplayInCommunity, TitleChinese, AllowDeleteOthersAnnouncement";
$fieldvalue = "'$Title', '$Description', '$RecordType', now(), now(), $Quota, $functionAccess, $SchoolYear, '$DisplayInCommunity', '$TitleChinese', '$AllowDeleteOthersAnnouncement'";
if ($AnnounceAllowed == 1)
{
    $fieldname .= ",AnnounceAllowed";
    $fieldvalue .= ",1";
}
$sql = "INSERT INTO INTRANET_GROUP ($fieldname) VALUES ($fieldvalue)";

$li->db_db_query($sql) or die(mysql_error());
$GroupID = $li->db_insert_id();

if($GroupID)
{
    include_once($PATH_WRT_ROOT."includes/libgroup.php");
    $lgroup = new libgroup();
    $SuccessArr = $lgroup->Group_Adding_Additional_Task($GroupID,$Title,$Description,$RecordType);
    
    //	include_once($PATH_WRT_ROOT."includes/icalendar.php");
    //	$iCal = new icalendar();
    //	$calID = $iCal->createSystemCalendar($Title, 2,'P',$Description);
    //	if (!empty($calID)){
    //		// echo $calID;
    //		// exit;
    //		$sql = "Update INTRANET_GROUP set CalID = '$calID' where GroupID='$GroupID'";
    //		$li->db_db_query($sql);
    //	}
    //	# Match with INTRANET_CLASS
    //	if ($RecordType == 3)
    //	{
    //		$sql = "UPDATE INTRANET_CLASS SET GroupID = $GroupID WHERE ClassName = '$Title'";
    //		$li->db_db_query($sql);
    //	}
    //
    //	# INTRANET_ORPAGE_GROUP
    //	$lorg = new liborganization();
    //	if ($hide==1)
    //	{
    //		$lorg->setGroupHidden($GroupID);
    //	}
    //
    //	# INTRANET_ENROL_GROUPINFO - set default status to 0
    //	if ($RecordType == 5)
    //	{
        //		include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
        //		$libenroll = new libclubsenrol();
        //
        //		$DataArr = array();
        //		$DataArr['GroupID'] = $GroupID;
        //		$DataArr['ClubType'] = 'Y';
        //		$DataArr['ApplyOnceOnly'] = 0;
        //		$DataArr['FirstSemEnrolOnly'] = 0;
        //		$SuccessArr['Insert_Group_Main_Info'] = $libenroll->Insert_Group_Main_Info($DataArr);
        //
        //		### Create Enrolment Club Record
        //		$SuccessArr['Insert_Year_Based_Club'] = $libenroll->Insert_Year_Based_Club($GroupID);
        //
        //		//$sql = "INSERT INTO INTRANET_ENROL_GROUPINFO (GroupID, Quota, RecordStatus) VALUES ($GroupID, 0, 0)";
        //		//$li->db_db_query($sql);
        //	}
        #########GoogleSSO Implementation
        if ($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['user_and_group'] && $ssoservice["Google"]['service_directory']['group']['is_readonly'] == false ) {
            include_once ($PATH_WRT_ROOT . "includes/google_api/libgoogleapi.php");
            include_once ($PATH_WRT_ROOT . "includes/sso/libSSO_db.php");
            include_once ($PATH_WRT_ROOT . "includes/sso/libGoogleSSO.php");
            $libGoogleSSO = new libGoogleSSO();
            $GroupIDArr = array(
                $GroupID
            );
            $libGoogleSSO->syncGroupFromEClassToGoogle($li, $GroupIDArr);
        }
    $xmsg="AddSuccess";
}
else
{
    $xmsg="GroupDuplicated";
}
intranet_closedb();
header("Location: index.php?filter=$RecordType&xmsg=$xmsg&SchoolYear=$SchoolYear");
?>