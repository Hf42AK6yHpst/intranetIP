<?php
############# Change Log [Start] ################
#	Date: 2015-10-23 Kenneth Yau
#		- Update delete sql (remove null value & remove duplicate)
#		- Edit xmsg
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$li = new libdb();
$default_cats = array(0,1,2,3,4,5);

# Cannot delete those with groups linking
$sql = "SELECT DISTINCT RecordType FROM INTRANET_GROUP";
$currents = $li->returnVector($sql);
$currents = array_unique(array_remove_empty(array_merge($default_cats,$currents)));
$exlist = "'".implode("','",$currents)."'";

$list = implode(",",$GroupCategoryID);
# Grab category can be deleted
$sql = "SELECT GroupCategoryID FROM INTRANET_GROUP_CATEGORY WHERE GroupCategoryID IN ($list) AND GroupCategoryID NOT IN ($exlist)";
$valids = $li->returnVector($sql);


if (sizeof($valids)==sizeof($GroupCategoryID))              # All deleted successfully
{
    $signal = 'delete';
}
else
{
    $signal = 11;
}
$valid_list = implode(",",$valids);

$sql = "DELETE FROM INTRANET_GROUP_CATEGORY WHERE GroupCategoryID IN ($valid_list)";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_ROLE WHERE RecordType IN ($valid_list)";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?xmsg=$signal");
?>