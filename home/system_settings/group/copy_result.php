<?php
# using:

/*	Change Log


2017-06-06 (Icarus): Modified the HTML code, change the CSS of the form to reach the UI consistency to the sample page. 
						The original code has been commented.					
*/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");


intranet_auth();
intranet_opendb();

$CurrentPage	= "Group";
$CurrentPageArr['Group'] = 1;

$linterface 	= new interface_html();
$lgroup = new libgroup();

### Copy Group
$success = $lgroup->Copy_Group_And_Member($CopyFrom, $CopyTo, $CopyGroup, $CopyMember);

### get Academic Year Name ###
$FromYear = new academic_year($CopyFrom);
$FromYearName = Get_Lang_Selection($FromYear->YearNameB5,$FromYear->YearNameEN);
$ToYear = new academic_year($CopyTo);
$ToYearName = Get_Lang_Selection($ToYear->YearNameB5,$ToYear->YearNameEN);

### Build table'
foreach((array) $CopyGroup as $key => $GroupID)
{
	$CopyGroupSuccess = true;
	$CopyGroupSuccess = $CopyGroupSuccess &&!in_array(false,(array)$success["CopyGroup"][$GroupID]);
	
	if(in_array($GroupID,(array)$CopyMember))
		$CopyGroupSuccess = $CopyGroupSuccess &&!in_array(false,(array)$success["CopyMember"][$GroupID]);
		
	if($CopyGroupSuccess)
		$SuccessCount++ ; 
}

### Get Btn
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "javascript:window.location='index.php'","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$Btn = $BackBtn;

### Title ###
$PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmt'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Group']['CopyGroup'], "");

//$TAGS_OBJ[] = array($Lang['Group']['CopyGroup'],"",1);
$TAGS_OBJ = $lgroup->getTopTabInfoAry('group');

$STEPS_OBJ[] = array($Lang['Group']['Options'], 0);
$STEPS_OBJ[] = array($Lang['Group']['Confirmation'], 0);
$STEPS_OBJ[] = array($Lang['Group']['CopyResult'], 1);

$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];

$linterface->LAYOUT_START();   
?>
<br />
<form name="frm1" method="POST" action="copy_result.php" >
<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
<div style="padding-top:25px;"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></div>
<br />
<div align="center" style="padding:50px 0px;"><?=$SuccessCount." ".$Lang['Group']['GroupCopiedSuccessfully']?></div>
<br />
<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?=$Btn?>
	<p class="spacer"></p>
</div>



<!--	THE ORIGINAL CODE
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr><td colspan="2"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td clospan="2" align="center" style="padding:20px;">
			<?=$SuccessCount." ".$Lang['Group']['GroupCopiedSuccessfully']?>
		</td>
	</tr>
	<tr>
		<td>
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
				<?=$Btn?>
				</td>
			</tr>
        </table> 
		</td>
	</tr>
</table>
-->


<input type="hidden" value="<?=$CopyFrom?>" name="CopyFrom">
<input type="hidden" value="<?=$CopyTo?>" name="CopyTo">
<?=$HiddenValue?>
</form>
<br><br>
<script>
function jsGoBack()
{
	document.frm1.action = "copy_group.php";
	document.frm1.method = "POST";
	document.frm1.submit();
}
</script>

<?
$linterface->LAYOUT_STOP();
?>
