<?php

/****************************************
 *  Modification
 *  	20100416 Marcus:
 * 			- select user with EnrolGroupID (if exist)
 * **************************************/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libdbdump.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_opendb();

$lexport = new libexporttext();
$li = new libgroup($GroupID);

#ClassName        ClassNumber        EnglishName        Chinese Name        Post        UserLogin        UserEmail

# Export default format
if($EnrolGroupID) $EnrolGroupSql = " AND a.EnrolGroupID = $EnrolGroupID ";

$UserList = $li->returnGroupUser("", "","",$SchoolYear);
$SizeOfMember = sizeof($UserList);
for($i=0; $i<$SizeOfMember; $i++)
{
	list(
		$thisUserID, $UserLogin, $UserEmail, $FirstName, $LastName, $Status, $Role,
		$UserPassword, $Url, $RecordType, $ClassName, $ClassNumber, $EnglishName,
		$ChineseName, $Performance, $CommentStudent 
	) = $UserList[$i];
	$members[] = array($Role, $UserLogin, $EnglishName, $ChineseName, $ClassName, $ClassNumber, $Performance, $UserEmail);
}

//$sql = "SELECT c.Title,b.UserLogin,b.EnglishName, b.ChineseName, b.ClassName, b.ClassNumber, a.Performance, b.UserEmail
//        FROM INTRANET_USERGROUP as a
//               INNER JOIN INTRANET_USER as b ON a.GroupID = $GroupID AND a.UserID = b.UserID
//               LEFT OUTER JOIN INTRANET_ROLE as c ON a.RoleID = c.RoleID
//             WHERE a.GroupID = $GroupID $EnrolGroupSql
//        ";
//
//
//$members = $li->returnArray($sql,7);

$x = "Role,UserLogin,EnglishName,ChineseName,ClassName,ClassNumber,Performance,UserEmail\n";
$exportColumn = array("Role","UserLogin","EnglishName","ChineseName","ClassName","ClassNumber","Performance","UserEmail");

// Output the file to user browser
$filename = "eclass-groupuser.csv";

$export_content = $lexport->GET_EXPORT_TXT($members, $exportColumn);

$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
#header("Location: $url");
?>