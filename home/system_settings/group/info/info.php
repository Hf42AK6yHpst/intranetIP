<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libcal.php");
//include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");


intranet_auth();
intranet_opendb();

$li = new libgroup($GroupID);

$MODULE_OBJ['title'] = $li->Title;
$linterface = new interface_html("popup_index.html");
$linterface->LAYOUT_START();

$GroupUsers = $li->getNumberGroupUsers();
$GroupAnnouncement = $li->getNumberGroupAnnouncement();
$GroupEvent = $li->getNumberGroupEvent();
$GroupTimetable = $li->getNumberGroupTimetable();
$GroupResource = $li->getNumberGroupResource();


?>

<table width='100%' border='0' cellspacing='0' cellpadding='10'>
	<tr>		
		<td>			
			<table width='100%' border='0' cellspacing='5' cellpadding='0'>
				<tr>
					<td bgcolor='#FFFFFF'>		
						<table width='100%' border='0' cellspacing='0' cellpadding='5'>		
							<tr>			
							<td width='25%' nowrap  valign='top'><?php echo $i_admintitle_announcement; ?>:</td>			
							<td><?php echo $GroupAnnouncement; ?></td>		
							</tr>		
						</table>
					</td>
				</tr>
				<tr>
					<td bgcolor='#FFFFFF'>		
						<table width='100%' border='0' cellspacing='0' cellpadding='5'>		
							<tr>			
								<td width='25%' nowrap  valign='top'><?php echo $i_admintitle_event; ?>:</td>			
								<td><?php echo $GroupEvent; ?></td>		
							</tr>		
						</table>
					</td>
				</tr>
				<tr>
					<td bgcolor='#FFFFFF'>		
						<table width='100%' border='0' cellspacing='0' cellpadding='5'>		
							<tr>			
								<td width='25%' nowrap valign='top'><?php echo $i_admintitle_timetable; ?>:</td>			
								<td><?php echo $GroupTimetable; ?></td>		
							</tr>		
						</table>
					</td>
				</tr>
				<tr>
					<td bgcolor='#FFFFFF'>		
						<table width='100%' border='0' cellspacing='0' cellpadding='5'>		
							<tr>			
								<td width='25%' nowrap valign='top'><?php echo $i_admintitle_resource; ?>:</td>			
								<td><?php echo $GroupResource; ?></td>		
							</tr>		
						</table>
					</td>
				</tr>			
			</table>		
		</td>
	</tr>
</table>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>