<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

//if (is_array($UserID)) $UserID = $UserID[0];
if (is_array($UID)) $UID = $UID[0];
$linterface 	= new interface_html();
$lu = new libuser($UserID);
$lg = new libgroup($GroupID);
$selection = $lg->getSubjectLeaderSelection("SubjectID[]","AvailableSubjectID[]",$UserID);

$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];

$PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmtCtr'], "../index.php");
$PAGE_NAVIGATION[] = array($li->Title, "");

$title = $Lang['Group']['GroupMgmtCtr']; 
$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"../",1);
$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"../groupcategory/");
$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"../role");
$linterface->LAYOUT_START();   

?>
<SCRIPT LANGUAGE=javascript>
function checkform(obj)
{
     checkOptionAll(obj.elements["SubjectID[]"]);
}
</SCRIPT>

<form name=form1 method=post action='subjectleader_update.php' onSubmit="return checkform(this)">
<!--<?= displayNavTitle($i_adminmenu_gm, '', $i_adminmenu_gm_group, "../index.php?filter=$filter", $lg->Title, 'javascript:history.back()', $button_assignsubjectleader, '') ?>-->
<?= displayTag("head_group_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0>
<tr><td align=right><?php echo $i_UserLogin; ?>:</td><td><?=$lu->UserLogin?></td></tr>
<tr><td align=right><?php echo $i_UserEnglishName; ?>:</td><td><?=$lu->EnglishName?></td></tr>
<tr><td align=right><?php echo $i_UserChineseName; ?>:</td><td><?=$lu->ChineseName?></td></tr>
<tr><td align=right><?php echo $i_UserClassName; ?>:</td><td><?=$lu->ClassName?></td></tr>
<tr><td align=right><?php echo $i_UserClassNumber; ?>:</td><td><?=$lu->ClassNumber?></td></tr>
<tr><td align=center colspan=2><?=$selection?></td></tr>
</table>
</blockquote>
<input type=hidden name=GroupID value="<?=$GroupID?>">
<input type=hidden name=UID value="<?=$UID?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();

//include("../../../templates/adminfooter.php");
?>