<?php
// editing by
/*********************************
 *
 *********************************/
$PATH_WRT_ROOT = "../../../../";
/*****************************************
 * modification
 * 2019-08-07   Chris
 * - Modified Google SSO
 * 2018-09-17   Chris
 * - added in Google SSO
 * 20160204 Kenneth:
 *  - mark delete log
 * 20140610 Bill:
 * 	- redirect to correct page for admin status filtering
 * 20111004 Carlos:
 * 	- remove calendar viewer that other personal calendars have shared to this group
 * 20100416 Marcus:
 * 	- for ECA Group , delete record in selected sem only
 * ***************************************/

include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
include($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/liblog.php");
intranet_opendb();

$li = new libdb();

$lgroup = new libgroup($GroupID);
$liblog = new liblog();
$CalID = $lgroup->CalID;

if (is_array($UID) && sizeof($UID)!=0)
{
    #20100416 Marcus
    if($EnrolGroupID)
        $EnrolGroupSql = " AND EnrolGroupID = $EnrolGroupID ";
        
        $sql = "DELETE FROM INTRANET_USERGROUP WHERE GroupID = $GroupID $EnrolGroupSql AND UserID IN (".implode(",", $UID).")";
        $isDel=$li->db_db_query($sql);
        if($isDel){
            $Module = 'SchoolSettings_Group';
            $Section = 'Remove_Group_Member';
            $RecordDetail ='GroupID:'.$GroupID.'; UserID:'. implode(",", $UID);
            $TableName ='INTRANET_USERGROUP';
            $liblog -> INSERT_LOG($Module, $Section, $RecordDetail, $TableName, $RecodID='');
            // # Google SSO Implementation
            if ($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['user_and_group'] && $ssoservice["Google"]['service_directory']['group']['is_readonly'] == false) {
                include_once ($PATH_WRT_ROOT . "includes/google_api/libgoogleapi.php");
                include_once ($PATH_WRT_ROOT . "includes/sso/libSSO_db.php");
                include_once ($PATH_WRT_ROOT . "includes/sso/libGoogleSSO.php");
                $libGoogleSSO = new libGoogleSSO();
                foreach ($UID as $user_id) {
                    $results = $libGoogleSSO->enabledUserForGoogle($li, array($user_id));
                    if (count($results['activeUsers'])>0||count($results['suspendedUsers'])>0||count($results['deletedUsers'])>0) {
                        $sql2 = 'SELECT `UserLogin` FROM `INTRANET_USER` WHERE `UserID`=\'' . $user_id . '\';';
                        $userlogin = current($li->returnVector($sql2));
                        $libGoogleSSO->removeGroupMemberFromEClassToGoogle($userlogin, $GroupID);
                    }
                }
            }
        }
        if($EnrolGroupID)
        {
            # make sure the user is not in the group any more before deleting the calendar viewer
            $sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID = '$GroupID' AND UserID IN ('".implode("','", $UID)."')";
            $Result = $li->returnArray($sql);
            $ExistUserList = Get_Array_By_Key($Result,"UserID");
            $Cal_UID = array_diff($UID,$ExistUserList);
        }
        else
        {
            $Cal_UID = $UID;
        }
        
        if(count($Cal_UID)>0)
        {
            $iCal = new icalendar();
            ##############################################
            ## START - delete CALENDAR_CALENDAR_VIEWER
            ##############################################
            $cal_sql = "delete from CALENDAR_CALENDAR_VIEWER where CalID='$CalID' and UserID IN ('".implode("','", $Cal_UID)."')";
            $li->db_db_query($cal_sql);
            
            $iCal->removeCalendarViewerFromGroup($GroupID,$Cal_UID);
            ##############################################
            ## END - delete CALENDAR_CALENDAR_VIEWER
            ##############################################
        }
}

# 20091005 Ivan
# Delete the user from Class list if the target Group is a Class group
if ($lgroup->RecordType == 3)
{
    include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
    $libYearClass = new year_class();
    $libFCM = new form_class_manage();
    
    $YearClassInfoArr = $libYearClass->Get_YearClass_By_GroupID($GroupID);
    $YearClassID = $YearClassInfoArr[0]['YearClassID'];
    
    // Delete Student in Class List
    //$sql = "Delete From YEAR_CLASS_USER Where YearClassID = '".$YearClassID."' And UserID IN (".implode(",", $UID).")";
    //$SuccessArr['Delete']['YEAR_CLASS_USER'] = $li->db_db_query($sql);
    $SuccessArr['Delete']['ClassStudent'] = $libFCM->Delete_Student_From_Class($YearClassID, $UID);
    
    // Delete Teacher in Class List
    //$sql = "Delete From YEAR_CLASS_TEACHER Where YearClassID = '".$YearClassID."' And UserID IN (".implode(",", $UID).")";
    //$SuccessArr['Delete']['YEAR_CLASS_TEACHER'] = $li->db_db_query($sql);
    $SuccessArr['Delete']['ClassTeacher'] = $libFCM->Delete_Class_Teacher($YearClassID, $UID);
}

intranet_closedb();
if($Semester) $par = "&Semester=$Semester";
header("Location: index.php?GroupID=$GroupID&filter=$filter&msg=3&RolesType=".$RolesType.$par);
?>