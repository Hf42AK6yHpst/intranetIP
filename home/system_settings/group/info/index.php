<?php
# using: 

############# Change Log [Start] ################
# Date:	2019-04-17 Pun [ip.2.5.10.5.1]
#		- Added XSS protection
# Date:		2014-06-10 Bill
#			add admin status filtering
#
# Date:		2014-02-14 Ivan [2014-0213-1411-53184]
#			improved the group name in the drop down list follows UI lang now
#
# Date: 	2013-01-23 Rita
#			change page navigation -  add category and group filter
#
# Date:	2010-11-02	YatWoon
#			can multiple set admin
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
//include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");

intranet_auth();
intranet_opendb();

$filter = IntegerSafe($filter);
$GroupID = IntegerSafe($GroupID);
$SchoolYear = IntegerSafe($SchoolYear);

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$CurrentPageArr['Group'] = 1;

$li = new libgroup($GroupID);
$linterface = new interface_html();

if (isset($ck_page_size) && $ck_page_size != "")
{
    $page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;

/*
switch ($filter){
        case 0: $filter = 0; break;
        case 1: $filter = 1; break;
        case 2: $filter = 2; break;
        case 3: $filter = 3; break;
        case 4: $filter = 4; break;
        case 5: $filter = 5; break;
        case 6: $filter = 6; break;
        default: $filter = 0; break;
}
<input type=hidden name=filter value="">
*/

# Group Filter
$SchoolYearID = '';
if($filter=='0'){
	// $filter = 0 is [Identity 基本組別]
	$SchoolYearID = '';
}else{
	if ($SchoolYear) {
		$SchoolYearID = $SchoolYear;
	}
	else {
		$SchoolYearID = $li->AcademicYearID;
	}
}

$groupListArr = $li->returnGroupsByCategory($filter, $SchoolYearID);
$numOfGroup = count($groupListArr);

$groupSelAry = array();
for ($i=0; $i<$numOfGroup; $i++) {
	$_groupId = $groupListArr[$i][0];
	$_groupNameEn = $groupListArr[$i][1];
	$_groupName = $groupListArr[$i][2];
	
	if ($_groupName == '') {
		$_groupName = $_groupNameEn;
	}
	
	$groupSelAry[] = array($_groupId, $_groupName);
}

if($groupSelAry){
	$groupFilter = $linterface->GET_SELECTION_BOX($groupSelAry, 'id="SelectedGroupID" onChange="javascript: js_Change_Group(this.value);"', '', $GroupID);
}

# Category Name
$lgc = new libgroupcategory($filter);
$CurrentGroupInforArr = $lgc->retrieveRecord($filter);
$CurrentGroupName = $CurrentGroupInforArr[0]['CategoryName'];


# for those who has eEnrolment, need to add term filter if the group is an ECA group
# Comment by Marcus, Moved into getGroupUsers in libgroup.php
if($li->RecordType==5 && $plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	
	$libenroll = new libclubsenrol();
	
	$GroupInfoArr = $libenroll->Get_Group_Info_By_GroupID($GroupID);
	//debug_pr($GroupInfoArr);

	if(!empty($GroupInfoArr))
	{
		$GroupClubType = $GroupInfoArr['ClubType'];
		if($GroupClubType=='S')
		{
			$TermInfoArr = getCurrentAcademicYearAndYearTerm();
			$curYearTermID = $TermInfoArr['YearTermID'];
			
			$InvolvedSem = $libenroll->Get_Group_Involoved_Semester($GroupID);
			if(empty($Semester)) 
				$Semester = $InvolvedSem[0];
			$termfilterbar .= $libenroll->Get_Term_Selection("Semester", '', $Semester, $OnChange='this.form.pageNo.value=1; this.form.submit()',1 ,0 ,0);
			$termfilterbar .= "<input type='hidden' name='filter' value='$filter'>";
			
			$EnrolGroupID = $libenroll->Get_Club_Of_Semester($GroupID, $Semester);
		}
		else
		{
			$EnrolGroupID = $libenroll->Get_Club_Of_Semester($GroupID, '');
		}
		
		$GroupUsersList = $libenroll->Get_Member_List($EnrolGroupID,"club",0);
		$GroupUsers = count($GroupUsersList);
		
//		$EnrolIDCond = " EnrolGroupID = '$EnrolGroupID' "; 
	} 
//	$EnrolIDForAdd = "&EnrolGroupID=".$EnrolGroupID; 
}
else
{	
	$GroupUsers = $li->getNumberGroupUsers('','','',$SchoolYear);
}

if ($li->RecordType > 0 && ($li->RecordType!=5 || !$plugin['eEnrollment'])) {     
	//$AddBtn 	= "<a class=contenttool href=\"javascript:newWindow('../choose/index.php?GroupID={$GroupID}&EnrolGroupID={$EnrolGroupID}',11)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>".toolBarSpacer();       
	$AddBtn 	= "<a class=contenttool href=\"javascript:newWindow('/home/common_choose/group_member.php?GroupID={$GroupID}&EnrolGroupID={$EnrolGroupID}',11)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>".toolBarSpacer();       
	//$ImportBtn 	= "<a class=contenttool href=javascript:checkNew('student_import.php?GroupID=$GroupID&filter=$filter')><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_import.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_import . "</a>".toolBarSpacer();       
}
$emailBtn = ($GroupUsers == 0) ? "" : "<a class=contenttool href=javascript:checkGet(document.form1,'email.php')><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_email_b.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" />". $Lang['Group']['SendEmail'] ."</a>\n".toolBarSpacer();
$exportBtn = ($GroupUsers == 0) ? "" : "<a class=contenttool href=javascript:checkGet(document.form1,'export.php');document.form1.action='index.php'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_export.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" />$button_export</a>\n";

$row = $li->returnRoleType();


## Update Role
# Update Icon Btn
$roleSelectionBar = '';
$roleSelectionBar .= "<a href=\"javascript:checkRole(document.form1,'UID[]','role.php')\" class=\"tabletool contenttool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_update.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">".$Lang['Group']['QuickAssignAs']." </a>";
# Role Selection
$roleSelectionBar .= "<select name=RoleID>\n";
for($i=0; $i<sizeof($row); $i++)
	$roleSelectionBar .= "<option value=".$row[$i][0]." ".(($row[$i][2]==1)?"SELECTED":"").">".$row[$i][1]."</option>\n";
$roleSelectionBar .= "</select>\n</td><td nowrap=\"nowrap\">";

$groupadminfunctionbar = "";

$subleader = get_file_content("$intranet_root/file/homework_leader.txt");
if ($subleader == 1 && $li->RecordType == 3) {                # Only Class has
    #$groupadminfunctionbar .= "<input class=button type=button value=\"$button_updateperformance\" onClick=\"checkEdit(this.form, 'UID[]','performance.php')\">";
    $groupadminfunctionbar .= "<a href=\"javascript:checkEdit(document.form1, 'UID[]','subjectleader.php')\"><img src='/images/admin/button/t_btn_assignsubjectleader_$intranet_session_language.gif' border='0' align='absmiddle' alt='$button_assignsubjectleader'></a>&nbsp;";
}
/*
$groupadminfunctionbar .= "<input type=hidden name=adminflag value=0>";
$setadmin .= "<a href=\"javascript:document.form1.adminflag.value='A'; checkEdit(document.form1, 'UID[]','assignadmin.php')\"><img src='/images/admin/button/t_btn_set_admin_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$canceladmin .= "<a href=\"javascript:document.form1.adminflag.value='0'; checkRole(document.form1, 'UID[]','setadmin.php')\"><img src='/images/admin/button/t_btn_cancel_admin_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";

if ($li->RecordType == 5) {                # Only ECA has
    #$groupadminfunctionbar .= "<input class=button type=button value=\"$button_updateperformance\" onClick=\"checkEdit(this.form, 'UID[]','performance.php')\">";
    $groupadminfunctionbar .= "<a href=\"javascript:checkEdit(document.form1, 'UID[]','performance.php')\"><img src='/images/admin/button/t_btn_update_performance_$intranet_session_language.gif' border='0' align='absmiddle' alt='$button_updateperformance'></a>&nbsp;";
}
*/


##toolbar
$table_tool = '';
$table_tool .= "<td nowrap=\"nowrap\">
					$roleSelectionBar
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";

//set Admin
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:document.form1.adminflag.value='A'; setAdmin();\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_common.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						".$Lang['Group']['SetAdmin']." 
					</a>
				</td>";				
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";

//Cancel Admin
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:js_SetMember();\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_common.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						".$Lang['Group']['SetAsMember']." 
					</a>
				</td>";
				
			
if($li->RecordType > 0 && ($li->RecordType!=5 || !$plugin['eEnrollment']))
{					
	$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
	$table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkRemove(document.form1,'UID[]','delete.php')\"  class=\"tabletool contenttool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$button_delete
						</a>
					</td>";
}					

##toolbar end
$WarningMsg = '<ol>'."\n";
$WarningMsg .= '<li>'.$Lang['Group']['AddUserInstrution'].'</li>'."\n";
$WarningMsg .= '<li>'.$Lang['Group']['AdminRightArr']['MemberManagement_eEnrolRemarks'].'</li>'."\n";
$WarningMsg .= '</ol>'."\n";

# msg
switch($xmsg)
{
	case 1: $msg = "add"; break;
	case 2: $xmsg = $Lang['Group']['FailToAddSomeUser']; break;
	default: $msg = $xmsg = ''; break;
	
}

$li->order = $order==""?1:$order;
$li->field = $field==""?0:$field;
$li->keyword = $keyword;
$li->page_size = $page_size;

$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];

$PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmtCtr'], "../index.php");
//$PAGE_NAVIGATION[] = array($li->TitleDisplay, "");
$PAGE_NAVIGATION[] = array($CurrentGroupName, "../index.php");
if($filter){
	$PAGE_NAVIGATION[] = array($groupFilter);
}else{
	$PAGE_NAVIGATION[] = array($li->TitleDisplay, "");
}

$title = $Lang['Group']['GroupMgmtCtr']; 
//$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"../",1);
//$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"../groupcategory/");
//$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"../role");
$TAGS_OBJ = $li->getTopTabInfoAry('groupMember');

//admin status filtering 
$OptionArr = array();
$OptionArr[] = array(1, $Lang['Group']['RolesAll']);
$OptionArr[] = array(3, $Lang['Group']['RolesAdmin']);
$OptionArr[] = array(2, $Lang['Group']['RolesNormal']);
if($RolesType==''){
	$RolesType = 1;
}
$adminStatusSel = getSelectByArray($OptionArr,'id="RolesType" name="RolesType" onChange="javascript: js_Change_Types(this.value);"',$RolesType,0,1);


$linterface->LAYOUT_START();   

?>

<script language="javascript">
<!--
function setAdmin()
{
	//document.form1.action='assignadmin.php';
	//document.form1.submit();
	checkRole(document.form1, 'UID[]','assignadmin.php');
}

function js_SetMember()
{
	document.form1.adminflag.value='0'; 
	checkRole(document.form1, 'UID[]','setadmin.php');
}
//-->

function js_Change_Group(GroupId){
	document.location='index.php?GroupID='+GroupId+'&filter=<?=$filter?>&SchoolYear=<?=$SchoolYear?>';
}

function js_Change_Types(types){
	$('form#form1').attr('action', 'index.php').submit();
}

</script>

<form name="form1" id="form1" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>

<tr>
	<td class="tabletext" align="center">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="tabletext" align="right"><?=$linterface->GET_SYS_MSG($msg,$xmsg);?></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="center">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left" class="tabletext">
					<?=$linterface->Get_Warning_Message_Box("",$WarningMsg)?>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td colspan=4>
							
								<table border="0" cellspacing="0" cellpadding="2" >
									<tr>
										<td><p><?=$AddBtn.$emailBtn.$exportBtn?></p></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
                            <td width="2%"></td>
							<td align="left" class="table-action-bar">
								<?=$adminStatusSel?>
							</td>
							<!--<td><?=$termfilterbar?></td>-->
							<!--<td align="left" colspan="1" style="padding-left:2%"><?=$termfilterbar?></td>-->
							<td align="right" valign="bottom" class="table-action-bar">
								<table border="0" cellspacing="0" cellpadding="0" >
								<tr>
									<td width="21" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_t_01.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="21" height="22" border="0"></td>
									<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_t_02.gif"> 
										<table border="0" cellspacing="0" cellpadding="2" valign="bottom">
											<tr>
												<?=$table_tool?>
											</tr>
										</table>
									</td>
									<td width="6" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_t_03.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="6" height="22"></td>
								</tr>
								</table>
							</td>
							<td width="2%">
							&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan="4" >
								<?php echo $li->displayGroupUsers_ip20("",""," $EnrolIDCond","","",$SchoolYear,$RolesType); ?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td> <?= $linterface->GroupAdminField() ?> <?=$linterface->InactiveStudentField()?></td></tr>
		</table>
	</td>
</tr>


</table>


<input type="hidden" name="GroupID" value=<?php echo $li->GroupID; ?>>
<input type="hidden" name="EnrolGroupID" value=<?php echo $EnrolGroupID; ?>>
<input type="hidden" name="SchoolYear" value=<?php echo $SchoolYear; ?>>
<input type="hidden" name="field" value="<?=$field?>">
<input type="hidden" name="order" value="<?=$order?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="adminflag" value=0>
<input type="hidden" name="target" value="">
<input type="hidden" name="role" value="">
<input type="hidden" name="filter" value="<?php echo $filter;?>">


</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
//include($PATH_WRT_ROOT."templates/adminfooter.php");
?>