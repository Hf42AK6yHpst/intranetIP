<?php
# using:

/************************************
 *  Modification log
 *
 * Date:   2018-09-13 Chris
 * - added in Gsuite SSO
 *
 *	2012-02-23 YatWoon
 *	- Fixed: missing to cater if the data include ' symblo
 *
 *	2010-12-13 YatWoon
 *	- add "Group Name (Chinese)"
 *
 * 		20100715 Marcus:
 * 			- move group adding related programming in function Group_Adding_Additional_Task.
 * 				As same task is performed in copy group, group import, add group.
 * **********************************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/liborganization.php");
//Google SSO
if ($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['user_and_group'] && $ssoservice["Google"]['service_directory']['group']['is_readonly'] == false) {
    include_once ($PATH_WRT_ROOT . "includes/google_api/libgoogleapi.php");
    include_once ($PATH_WRT_ROOT . "includes/sso/libSSO_db.php");
    include_once ($PATH_WRT_ROOT . "includes/sso/libGoogleSSO.php");
    $libGoogleSSO = new libGoogleSSO();
    $IsGoogleSSOEnable = true;
}


intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$li 			= new libdb();
$lgroup 		= new libgroup();


## Get Import Data
//$ImportData = unserialize(stripslashes($postData));
$ImportData = unserialize(stripslashes(intranet_undo_htmlspecialchars($postData)));

## insert import data
$functionAccess = 'NULL';
// modified by marcus 20100712, get School Year , user can import group to future year also.
//$SchoolYear = Get_Current_Academic_Year_ID();
$GroupCatIDNameMap = $lgroup->returnAllCategory();
$GroupCatNameIDMap = array_flip($GroupCatIDNameMap);

foreach($ImportData as $key => $Record)
{
    list($Title,$TitleChinese,$Description,$Quota,$UsePublicAnnonuceOrEvent,$CategoryName,$DisplayineCommun,$AllowAdminEditOrDelete)=$Record;
    $RecordType = $GroupCatNameIDMap[$CategoryName];
    
    $UsePublicAnnonuceOrEvent  = strtoupper($UsePublicAnnonuceOrEvent);
    if($UsePublicAnnonuceOrEvent=='Y'){
        $UsePublicAnnonuceOrEvent='1';
    }elseif($UsePublicAnnonuceOrEvent=='N'){
        $UsePublicAnnonuceOrEvent = 'null';
    }
    
    
    $DisplayineCommun  = strtoupper($DisplayineCommun);
    if($DisplayineCommun=='Y'){
        $DisplayineCommun=1;
    }elseif($DisplayineCommun=='N'){
        $DisplayineCommun = 0;
    }
    
    $AllowAdminEditOrDelete  = strtoupper($AllowAdminEditOrDelete);
    if($AllowAdminEditOrDelete=='Y'){
        $AllowAdminEditOrDelete=1;
    }elseif($AllowAdminEditOrDelete=='N'){
        $AllowAdminEditOrDelete = 0;
    }
    
    $fieldname = "Title, TitleChinese, Description, AnnounceAllowed, RecordType, DisplayInCommunity, AllowDeleteOthersAnnouncement, DateInput, DateModified, StorageQuota, FunctionAccess, AcademicYearID";
    $fieldvalue = "'$Title', '$TitleChinese', '$Description', $UsePublicAnnonuceOrEvent , '$RecordType', '$DisplayineCommun', '$AllowAdminEditOrDelete', now(), now(), $Quota, $functionAccess, $SchoolYear";
    
    
    $sql = "INSERT INTO INTRANET_GROUP ($fieldname) VALUES ($fieldvalue)";
    
    $li->db_db_query($sql);
    $GroupID = $li->db_insert_id();
    
    if($GroupID)
    {
        $SuccessArr = $lgroup->Group_Adding_Additional_Task($GroupID,$Title,$Description,$RecordType);
        
        //		include_once($PATH_WRT_ROOT."includes/icalendar.php");
        //		$iCal = new icalendar();
        //		$calID = $iCal->createSystemCalendar($Title, 2,'P',$Description);
        //		if (!empty($calID)){
        //			// echo $calID;
        //			// exit;
        //			$sql = "Update INTRANET_GROUP set CalID = '$calID' where GroupID='$GroupID'";
        //			$li->db_db_query($sql);
        //		}
        //		# Match with INTRANET_CLASS
        //		if ($RecordType == 3)
        //		{
        //			$sql = "UPDATE INTRANET_CLASS SET GroupID = $GroupID WHERE ClassName = '$Title'";
        //			$li->db_db_query($sql);
        //		}
        //
        //		# INTRANET_ORPAGE_GROUP
        //		$lorg = new liborganization();
        //		if ($hide==1)
        //		{
        //			$lorg->setGroupHidden($GroupID);
        //		}
        //
        //		# INTRANET_ENROL_GROUPINFO - set default status to 0
        //		if ($RecordType == 5)
        //		{
            //			$sql = "INSERT INTO INTRANET_ENROL_GROUPINFO (GroupID, Quota, RecordStatus) VALUES ($GroupID, 0, 0)";
            //			$li->db_db_query($sql);
            //		}
            // ## Google SSO Implementation
            if ($IsGoogleSSOEnable== true) {
                $GroupIDArr = array(
                    $GroupID
                );
                $libGoogleSSO->syncGroupFromEClassToGoogle($li, $GroupIDArr);
            }
}

/*
 #Build Table Content
 $rowcss = " class='".(($key)%2==0? "tablebluerow2":"tablebluerow1")."' ";
 $insertStatus = mysql_affected_rows()>0?$i_con_msg_import_success:$i_con_msg_import_failed2;
 $failcss = mysql_affected_rows()>0?"":" class='red' ";
 
 $Confirmtable .= "	<tr $rowcss>";
 $Confirmtable .= "		<td class='tabletext'>".($key+1)."</td>";
 $Confirmtable .= "		<td class='tabletext'>$ClassName</td>";
 $Confirmtable .= "		<td class='tabletext'>$ClassNumber</td>";
 $Confirmtable .= "		<td class='tabletext'>$EventCode</td>";
 $Confirmtable .= "		<td $failcss>$insertStatus</td>";
 $Confirmtable .= "	</tr>";
 */
}


# BackBtn
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'","back");

### Title ###
$PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmtCtr'], "../index.php");
$PAGE_NAVIGATION[] = array($li->Title, "index.php?GroupID=$GroupID&filter=$filter");
$PAGE_NAVIGATION[] = array($button_import, "");

//$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"",1);
//$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"groupcategory/");
//$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"role/");
$TAGS_OBJ = $lgroup->getTopTabInfoAry('group');

$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];
$linterface->LAYOUT_START();

?>
<br>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td class='tabletext' align='center'><?=count($ImportData)?> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?></td>
	</tr>
	<tr>
		<td colspan="2" align="center"><?=$BackBtn?></td>
	</tr>
</table>


<?
$linterface->LAYOUT_STOP();
?>
