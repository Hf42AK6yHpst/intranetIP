<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$lexport = new libexporttext();

# Export default format
$sql = "
			SELECT
				ig.Title,
				ig.TitleChinese,
				ig.Description,
				ig.StorageQuota,
				IF (ig.AnnounceAllowed = 1, 'Y', 'N') as AnnounceAllowed,
				igc.CategoryName as Type,
				IF (ig.DisplayInCommunity = 1, 'Y', 'N') as DisplayInCommunity, 
				IF (ig.AllowDeleteOthersAnnouncement= 1, 'Y', 'N') as AllowAdminEditDelete,
				Group_Concat(iu.EnglishName) as Admin
			FROM
                INTRANET_GROUP AS ig
				LEFT OUTER JOIN INTRANET_USERGROUP iug ON (ig.GroupID = iug.GroupID AND iug.RecordType='A')
				LEFT OUTER JOIN INTRANET_USER iu ON (iug.UserID = iu.UserID)
				INNER JOIN INTRANET_GROUP_CATEGORY igc ON (ig.RecordType = igc.GroupCategoryID)
			WHERE 
				ig.RecordType = '".$filter."'
				AND	ig.AcademicYearID = '".$AcademicYearID."'
			GROUP BY ig.GroupID
	    ";

$li = new libdb();
$members = $li->returnArray($sql,9);

$exportColumn = array("Group Name (English)","Group Name (Chinese)","Description","Storage Quota","Use Public Announcements or Events","Type","Display in eCommunity","Allow Group Admin Delete or Edit", "Administrator");

// Output the file to user browser
$filename = "eclass-groupInfo.csv";

$export_content = $lexport->GET_EXPORT_TXT($members, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();

?>