<?php
// using: 
/*
 * 2020-05-07 Tommy
 *       - modified access checking, added $plugin["Disable_Timetable"]
 *  2019-05-20 Cameron
 *      - create this file
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");

$arrCookies = array();
$arrCookies[] = array("ck_schoolSettings_timetable_from_eService", "From_eService");			
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]) || $plugin["Disable_Timetable"]) {
	No_Access_Right_Pop_Up();
}


$linterface = new interface_html();
$libtimetable_ui = new libtimetable_ui();
$CurrentPageArr['Timetable'] = 1;

### Title ###
$TAGS_OBJ = $libtimetable_ui->Get_Tag_Array($CurrenrTag=3);
$MODULE_OBJ['title'] = $Lang['SysMgr']['Timetable']['ModuleTitle'];

# online help button
// $onlineHelpBtn = gen_online_help_btn_and_layer('school_settings','timetable');
// $TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

$linterface->LAYOUT_START(); 

## Filters
$filterDiv .= $libtimetable_ui->getEverydayTimetableFilterBar();

$ModeArr = '';
$ModeArr[] = array($Lang['SysMgr']['Timetable']['Mode']['View'], "icon_view.gif", "", 1);
$ModeArr[] = array($Lang['SysMgr']['Timetable']['Mode']['Edit'], "icon_edit_b.gif", "everyday_timetable_edit.php", 0);
$mode_toolbar = $libtimetable_ui->Get_Mode_Toolbar($ModeArr);

# Mode Filter
$modeDiv = '<div id="FilterButtonDiv" style="float:right;display:;">'."\n";
    $modeDiv .= '<table id="FilterButtonTable">'."\n";
        $modeDiv .= '<tr>'."\n";
            $modeDiv .= '<td>'."\n";
            $modeDiv .= $mode_toolbar;
            $modeDiv .= '</td>'."\n";
        $modeDiv .= '</tr>'."\n";
    $modeDiv .= '</table>'."\n";
    $modeDiv .= '</div>'."\n";
$modeDiv .= '<div style="clear:both">'."\n";

?>


<div id="IndexDebugArea"></div>

<form id="form1" name="form1" method="POST" action="index.php">
	
	<div align="left"> 
		<?php echo $filterDiv; ?>
		
		<?php echo $modeDiv; ?>
		
	</div>
	
	<div>
		<div id="TimetableDiv">
		</div>
	</div>
</form>

<?
$linterface->LAYOUT_STOP();

intranet_closedb();
?>


<script type="text/javascript">
$(document).ready(function(){
	var isLoading = true;
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';
	
	$("#SelectedAcademicYearID,#SelectedTimetableID").change(function(){
		if ($('#SelectedAcademicYearID').val() != '') {
            isLoading = true;
    		$('#TimetableDiv').html(loadingImg);
    		reloadEverydayTimetable();
		}
	});

	reloadEverydayTimetable();
});

function reloadEverydayTimetable()
{
	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax.php',
		data : {
			'action': 'getEverydayTimeTableCalendar',
			'SelectedAcademicYearID': $('#SelectedAcademicYearID').val(),
			'SelectedTimetableID': $('#SelectedTimetableID').val(),
			'mode': 'view'
		},		  
		success: refreshTimetableCalendar,
		error: show_ajax_error
	});
}

function refreshTimetableCalendar(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#TimetableDiv').html(ajaxReturn.html);
		isLoading = false;
	}
}

function show_ajax_error() {
	alert('<?php echo $Lang['General']['AjaxError'];?>');
}

</script>