<?php
// using: 

############# Change Log [Start] ################
#
#   Date:   2020-11-06 Cameron
#           don't allow to delete time table that has been used in timezone or special timetable [case #M199265]
#
#   Date:   2020-05-07 Tommy
#           modified access checking, added $plugin["Disable_Timetable"]
#
#   Date:   2020-02-25 Sam
#           WAF does not allow '../' to be included in form data [P180616]
#
#   Date:   2019-06-13 Cameron
#           show different alert message if $sys_custom['eBooking']['EverydayTimetable'] is turned on in js_Delete_TimeSlot()
#
#	Date:	2010-08-13	Ivan
#			add export & import logic
#
#	Date:	2010-06-21	YatWoon
#			add online help button
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");

$arrCookies = array();
$arrCookies[] = array("ck_schoolSettings_timetable_from_eService", "From_eService");			
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]) || $plugin["Disable_Timetable"]) {
	No_Access_Right_Pop_Up();
}


$linterface = new interface_html();
$libtimetable_ui = new libtimetable_ui();
$CurrentPageArr['Timetable'] = 1;

### Title ###
$TAGS_OBJ = $libtimetable_ui->Get_Tag_Array($CurrenrTag=1);
$MODULE_OBJ['title'] = $Lang['SysMgr']['Timetable']['ModuleTitle'];

# online help button
//$onlineHelpBtn = gen_online_help_btn_and_layer('school_settings','timetable');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

$linterface->LAYOUT_START(); 
$include_JS_CSS = $libtimetable_ui->Include_JS_CSS();

## Toolbar (Import, Export)
$toolBarDiv = '';
$toolBarDiv .= '<div class="content_top_tool" id="ToolBarDiv">'."\n";
	$toolBarDiv .= '<div class="Conntent_tool">'."\n";
		$toolBarDiv .= '<div id="ImportLessonBtnDiv" style="display:none;float:left;">'."\n";
			$toolBarDiv .= '<a href="javascript:js_Go_Import();" class="import">'.$Lang['SysMgr']['Timetable']['Button']['ImportLesson'].'</a>'."\n";
		$toolBarDiv .= '</div>'."\n";
		$toolBarDiv .= '<div id="ExportLessonBtnDiv" style="display:none;float:left;">'."\n";
			$toolBarDiv .= '<a href="javascript:js_Go_Export();" class="export">'.$Lang['SysMgr']['Timetable']['Button']['ExportLesson'].'</a>'."\n";
		$toolBarDiv .= '</div>'."\n";
	$toolBarDiv .= '</div>'."\n";
	$toolBarDiv .= '<br style="clear:both" />'."\n";
$toolBarDiv .= '</div>'."\n";

## Filters
$filterDiv = '';
$filterDiv .= '<div>'."\n";
	$filterDiv .= '<div align=left>'."\n";
		$filterDiv .= $libtimetable_ui->Get_Allocation_Filter_Table();
	$filterDiv .= '</div>'."\n";
	$filterDiv .= '<br style="clear:both" />'."\n";
$filterDiv .= '</div>'."\n";


## Timetable Table
$TimetableDiv = '';
$TimetableDiv .= '<div id="TimetableDiv">'."\n";
$TimetableDiv .= '</div>';


?>

<div id="IndexDebugArea"></div>

<form id="form1" name="form1" method="POST" action="index.php">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
			<td class="main_content">
				<?= $include_JS_CSS ?>
				<?= $toolBarDiv ?>
				<?= $filterDiv ?>
				<?= $TimetableDiv ?>
			</td>
		</tr>
	</table>
</form>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

<?
$linterface->LAYOUT_STOP();

intranet_closedb();
?>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/2009a/js/timetable.js"></script>


<script type="text/javascript">
// Top Filters Selection values
var CurAcademicYearID = '';
var CurYearTermID = '';
var CurTimetableID = '';

// Delete Layer
var jsLastClickedObjectID = '';
var jsLastClickedDay = '';
var jsLastClickedTimeSlotID = '';

// Right Filters Selection values
var jsFilterViewMode;
var jsFilterIdentity;
var jsFilterYearClassIDArr;
var jsFilterSubjectGroupIDArr;
var jsFilterUserIDArr;
var jsFilterBuildingID;
var jsFilterLocationIDArr;
var jsFilterOthersLocation;

// Cookies variables
var arrCookies = new Array();
arrCookies[arrCookies.length] = "SelectedAcademicYearID";
arrCookies[arrCookies.length] = "SelectedYearTermID";
arrCookies[arrCookies.length] = "SelectedTimetableID";
arrCookies[arrCookies.length] = "SelectedViewMode";
arrCookies[arrCookies.length] = "SelectedYearID";
arrCookies[arrCookies.length] = "SelectedYearClassIDArr[]";
arrCookies[arrCookies.length] = "SelectedSubjectID";
arrCookies[arrCookies.length] = "SelectedSubjectGroupIDArr[]";
arrCookies[arrCookies.length] = "SelectedIdentity";
arrCookies[arrCookies.length] = "SelectedUserIDArr[]";
arrCookies[arrCookies.length] = "SelectedBuildingID";
arrCookies[arrCookies.length] = "SelectedLocationLevelID";
arrCookies[arrCookies.length] = "SelectedLocationIDArr[]";
arrCookies[arrCookies.length] = "OthersLocationTb";

// Others
var jsShowHideSpeed = 'fast';
var jsIsInitialize = 1;

$(document).ready(function () {
	<? if($clearCoo) { ?>
		for(i=0; i<arrCookies.length; i++)
		{
			var obj = arrCookies[i];
			//alert('obj = ' + obj);
			$.cookies.del(obj);
		}
	<? } ?>
	
	// Initiate the Current Selection
	CurAcademicYearID = $.cookies.get('SelectedAcademicYearID') || ($('select#SelectedAcademicYearID').val() || '');
	CurYearTermID = $.cookies.get('SelectedYearTermID') || ($('select#SelectedYearTermID').val() || '');
	CurTimetableID = $.cookies.get('SelectedTimetableID') || ($('select#SelectedTimetableID').val() || '');
	
	Blind_Cookies_To_Object();
	
	// Initiate Right Filters
	var jsFirstSelectedViewMode = $.cookies.get('SelectedViewMode') || 'Class';
	$('select#SelectedViewMode').val(jsFirstSelectedViewMode);
	js_Reload_Secondary_Selection(jsFirstSelectedViewMode);
	
	// Initiate Upper Selections
	js_Reload_Term_Selection(CurAcademicYearID, 0, CurYearTermID);
});

/********************* Update Selection *********************/
function js_Changed_AcademicYear_Selection(jsAcademicYearID, jsForCopyTimetable)
{
	if (jsForCopyTimetable != 1)
	{
		CurAcademicYearID = jsAcademicYearID;
		$('div#ImportLessonBtnDiv').hide();
		$('div#ExportLessonBtnDiv').hide();
	}
	
	js_Reload_Term_Selection(jsAcademicYearID, jsForCopyTimetable, '');
	
	// Reset Right Menu Filters (Get the data for this Academic Year for filtering)
	jsIsInitialize = 1;
	js_Reload_Secondary_Selection('Class', 1);
}

function js_Changed_Term_Selection(jsYearTermID, jsForCopyTimetable)
{
	if (jsForCopyTimetable != 1)
	{
		CurYearTermID = jsYearTermID;
		$('div#ImportLessonBtnDiv').hide();
		$('div#ExportLessonBtnDiv').hide();
	}	
	js_Reload_Timetable_Selection(CurTimetableID, jsForCopyTimetable);
}

function js_Changed_Timetable_Selection(jsTimetableID)
{
	CurTimetableID = jsTimetableID;
	
	if (CurTimetableID == '')
	{
		$('div#ImportLessonBtnDiv').hide();
		$('div#ExportLessonBtnDiv').hide();
	}
	else
	{
		// the display of the import icon is in function js_Update_Icon_Display_Status()
		$('div#ExportLessonBtnDiv').show();
	}
	
	// Show/Hide Add, Edit, Delete, Display Option, Filtering Link
	js_Update_Icon_Display_Status();
	Generate_Timetable(1);
}

function js_Reload_Term_Selection(SelectedAcademicYearID, ForCopyTimetable, jsTargetYearTermID)
{
	jsTargetYearTermID = jsTargetYearTermID || '';
	
	var selectionDivID;
	var noFirst;
	
	if (ForCopyTimetable == 1)
	{
		selectionDivID = 'TermSelectionDiv_ForCopyTimetable';
		noFirst = 1;
	}
	else
	{
		selectionDivID = 'TermSelectionDiv';
		noFirst = 0;
		CurAcademicYearID = SelectedAcademicYearID;
	}
	
	//$('div#' + selectionDivID).hide();
	$('div#' + selectionDivID).html('');
	if (CurAcademicYearID != '')
	{
		$('div#' + selectionDivID).html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"ajax_reload.php", 
			{ 
				RecordType: 'Reload_Term_Selection',
				AcademicYearID: SelectedAcademicYearID,
				YearTermID: jsTargetYearTermID,
				NoFirst: noFirst,
				ForCopyTimetable: ForCopyTimetable
			},
			function(ReturnData)
			{
				//$('div#' + selectionDivID).show();
				CurYearTermID = $('select#SelectedYearTermID').val();
				
				// Reload Timetable selection
				js_Reload_Timetable_Selection(CurTimetableID, ForCopyTimetable);
				
				// Show the add timetable icon if there is Term in the Academic Year
				if ($('select#SelectedYearTermID').val() != '')
					$('div#addTimetableDiv').show();
				
				//$('#IndexDebugArea').html(ReturnData);
				
				AssignCookies();
			}
		);
	}
	else
	{
		CurYearTermID = '';
		CurTimetableID = '';
		js_Update_Icon_Display_Status();
		js_Reload_Timetable_Selection('', ForCopyTimetable);
	}
}

function js_Reload_Timetable_Selection(TimetableID, ForCopyTimetable)
{
	var selectionDivID;
	var noFirst;
	var ExcludeTimetableID;
	
	if (ForCopyTimetable == 1)
	{
		selectionDivID = 'TimetableSelectionDiv_ForCopyTimetable';
		targetAcademicYearID = $('select#SelectedAcademicYearID_ForCopyTimetable').val();
		targetYearTermID = $('select#SelectedYearTermID_ForCopyTimetable').val();
		noFirst = 1;
		
		ExcludeTimetableID = CurTimetableID;
	}
	else
	{
		selectionDivID = 'TimetableSelectionDiv';
		targetAcademicYearID = CurAcademicYearID;
		targetYearTermID = CurYearTermID;
		noFirst = 0;
		
		CurTimetableID = TimetableID;
		ExcludeTimetableID = '';
	}
	
	$('div#' + selectionDivID).html('');
	if (targetAcademicYearID != '' && targetYearTermID != '')
	{
		$('div#' + selectionDivID).html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"ajax_reload.php", 
			{ 
				RecordType: "Reload_Timetable_Selection",
				TimetableID: TimetableID,
				AcademicYearID: targetAcademicYearID,
				YearTermID: targetYearTermID,
				ForCopyTimetable: ForCopyTimetable,
				NoFirst: noFirst,
				ExcludeTimetableID: ExcludeTimetableID
			},
			function(ReturnData)
			{
				$('#' + selectionDivID).show();
				
				if (ForCopyTimetable != 1)
				{
					AssignCookies();
					CurTimetableID = $('select#SelectedTimetableID').val();
					
					//js_Reload_Timetable_Edit_Delete_Link(CurTimetableID);
					//js_Reload_Timetable(CurTimetableID);
					Generate_Timetable(1);
					
					js_Update_Icon_Display_Status();
				}
				else
				{
					$('div#TermWarningDiv_ForCopyTimetable').html('');
					if (targetYearTermID != CurYearTermID)
					{
						$('div#TermWarningDiv_ForCopyTimetable').html('<?=$Lang['SysMgr']['Timetable']['Warning']['OnlyCopiedSubjectGroupCanBeCopied']?>').show();
					}
				}
				
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
	else
	{
		CurTimetableID = '';
		js_Update_Icon_Display_Status();
	}
}

function js_Reload_Subject_Group_Selection_InAddEditLayer(SelectedSubjectID, TimeSlotID, Day, RoomAllocationID, YearTermID)
{
	$('tr#SubjectGroupSelectionTr_InAddEditLayer').hide();
	
	if (SelectedSubjectID != "")
	{
		$('tr#SubjectGroupSelectionTr_InAddEditLayer').show();
		$('div#SubjectGroupSelectionDiv_InAddEditLayer').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"ajax_reload.php", 
			{ 
				RecordType: 'Reload_Subject_Group_Selection_InAddEditLayer',
				SubjectID: SelectedSubjectID,
				TimeSlotID: TimeSlotID,
				Day: Day,
				RoomAllocationID: RoomAllocationID,
				YearTermID: YearTermID
			},
			function(ReturnData)
			{
				js_Validate_Subject_Group_Overlap(TimeSlotID, Day, RoomAllocationID);
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
}


function js_Change_Floor_Selection(js_PATH_WRT_ROOT, BuildingSelID, OthersOptTbID, 
									FloorSelTrID, FloorSelDivID, FloorSelID, FloorSelFirstTitle, 
									RoomSelTrID, RoomSelDivID, RoomSelID, RoomSelFirstTitle, RoomSelOnchange, RoomSelIsMultiple,
									WithoutSubmitBtn)
{
	var SelectedBuildingID = $('select#' + BuildingSelID).val();
	var thisJsIsInitialize = jsIsInitialize;
	
	if (RoomSelIsMultiple == null)
		RoomSelIsMultiple = '';
		
	if (WithoutSubmitBtn == null)
		WithoutSubmitBtn = '';
	
	// Hide Row and Div of Floor and Room
	if (FloorSelTrID != '')
		$('tr#' + FloorSelTrID).hide();
	$('div#' + FloorSelDivID).hide();
	
	if (RoomSelTrID != '')
		$('tr#' + RoomSelTrID).hide();
	$('div#' + RoomSelDivID).hide();
	
	if (OthersOptTbID != '')
		$('input#' + OthersOptTbID).hide();
	
	// Hide the Submit Btn
	if (WithoutSubmitBtn == '')
		$("div#SubmitBtnDiv").hide();
	
	if (SelectedBuildingID != "")
	{
		if (SelectedBuildingID == "-1")
		{
			// Show input box for user to add customized location
			$('input#' + OthersOptTbID).show();
			$('input#' + OthersOptTbID).focus();
			if (OthersOptTbID != '')
				$("div#SubmitBtnDiv").show();
		}
		else
		{
			// Refresh Floor Selection and Room Selection
			$('div#' + FloorSelDivID).html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
				js_PATH_WRT_ROOT + "home/system_settings/location/ajax_reload_selection.php", 
				{ 
					RecordType: 'Reload_Floor_Selection',
					//js_PATH_WRT_ROOT: js_PATH_WRT_ROOT, [P180616]
					js_PATH_WRT_ROOT: '/',
					BuildingID: SelectedBuildingID,
					FloorSelID: FloorSelID,
					FloorSelFirstTitle: FloorSelFirstTitle,
					RoomSelTrID: RoomSelTrID,
					RoomSelDivID: RoomSelDivID,
					RoomSelID: RoomSelID,
					RoomSelFirstTitle: RoomSelFirstTitle,
					RoomSelOnchange: RoomSelOnchange,
					RoomSelIsMultiple: RoomSelIsMultiple
				},
				function(ReturnData)
				{
					if (thisJsIsInitialize == 1 && $.cookies.get('SelectedLocationLevelID') == null)
						$('select#SelectedLocationLevelID option:nth-child(2)').attr('selected', true);
					else
						$('select#SelectedLocationLevelID').val($.cookies.get('SelectedLocationLevelID'));
						
					// Display Floor Selection
					if (FloorSelTrID != '')
						$('tr#' + FloorSelTrID).show();
						
					$('div#' + FloorSelDivID).show();
					
					// Reload Room Selection
					js_Change_Room_Selection(js_PATH_WRT_ROOT, FloorSelID, RoomSelTrID, RoomSelDivID, RoomSelID, RoomSelFirstTitle, RoomSelOnchange, RoomSelIsMultiple, thisJsIsInitialize);
					
					//$('#IndexDebugArea').html(ReturnData);
					//$('#debugArea').html(ReturnData);
				}
			);
		}
	}
}

function js_Change_Room_Selection(js_PATH_WRT_ROOT, FloorSelID, RoomSelTrID, RoomSelDivID, RoomSelID, RoomSelFirstTitle, RoomSelOnchange, RoomSelIsMultiple, thisJsIsInitialize)
{
	var SelectedLocationLevelID = $('select#' + FloorSelID).val();
	
	if (RoomSelTrID != '')
		$('tr#' + RoomSelTrID).hide();
	
	$('div#' + RoomSelDivID).hide();
	
	if (RoomSelIsMultiple == null)
		RoomSelIsMultiple = '';
	
	if (SelectedLocationLevelID != "")
	{
		// Display Room Selection
		if (RoomSelTrID != '')
			$('tr#' + RoomSelTrID).show();
					
		// Refresh Room Selection
		$('div#' + RoomSelDivID).html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			js_PATH_WRT_ROOT + "home/system_settings/location/ajax_reload_selection.php", 
			{ 
				RecordType: 'Reload_Room_Selection',
				LocationLevelID: SelectedLocationLevelID,
				RoomSelID: RoomSelID,
				RoomSelFirstTitle: RoomSelFirstTitle,
				RoomSelOnchange: RoomSelOnchange,
				RoomSelIsMultiple: RoomSelIsMultiple
			},
			function(ReturnData)
			{
				if (thisJsIsInitialize == 1 && $.cookies.get('SelectedLocationIDArr[]') != null)
					$("select#SelectedLocationIDArr\\[\\]").val($.cookies.get('SelectedLocationIDArr[]'));
				else if (RoomSelIsMultiple == 1)
					$("select#SelectedLocationIDArr\\[\\] option").attr("selected","selected");
						
				// Check the room availability
				eval(RoomSelOnchange);
				
				$('div#' + RoomSelDivID).show();
				
				// Display Room Selection
				if (RoomSelIsMultiple)
				{
					$("div#SubmitBtnDiv").show();
					
					// Since the loading of timetable is faster then the filter selection, so need to refresh the timetable again after the filters are all finished building
					if (thisJsIsInitialize == 1)
						Generate_Timetable(1);
				}
				
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
}


/********************* End Update Selection *********************/


/********************* Update Link *********************/
/********************* End Update Link *********************/


/********************* Show Layer *********************/
function js_Show_Timetable_Add_Edit_Layer(jsIsNew)
{
	Hide_Return_Message();
	
	if (CurAcademicYearID == '')
	{
		alert('<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['AcademicYear']?>');
		return false;
		
	}
	
	if (CurYearTermID == '')
	{
		alert('<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Term']?>');
		return false;
	}
	
	var jsTimetableID;
	if (jsIsNew == 1)
		jsTimetableID = '';
	else
		jsTimetableID = CurTimetableID;
	
	if (CurAcademicYearID != '' && CurYearTermID != '')
	{
		$.post(
			"ajax_reload.php", 
			{ 
				RecordType: "Reload_Timetable_Add_Edit_Layer",
				TimetableID: jsTimetableID,
				AcademicYearID: CurAcademicYearID,
				YearTermID: CurYearTermID
			},
			function(ReturnData)
			{
				$('#TB_ajaxContent').html(ReturnData);
				$('#TimetableTitle').focus();
				
				// move the cursor to the end
				$('#TimetableTitle').val($('#TimetableTitle').val());
				
				js_Add_KeyUp_Blank_Checking("TimetableTitle", "TitleWarningDiv", '<?=$Lang['SysMgr']['Timetable']['Warning']['Blank']['Title']?>');
	
				//$('#debugArea').html(ReturnData);
			}
		);
	}
	
}

function js_Show_TimeSlot_Add_Edit_Layer(TimetableID, TimeSlotID, LastTimeSlotID, NextTimeSlotID)
{
	Hide_Return_Message();
	
	$.post(
		"ajax_reload.php", 
		{ 
			RecordType: "Reload_TimeSlot_Add_Edit_Layer",
			TimetableID: TimetableID,
			TimeSlotID: TimeSlotID,
			LastTimeSlotID: LastTimeSlotID,
			NextTimeSlotID: NextTimeSlotID
		},
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);
			$('#TimeSlotTitle').focus();
			
			js_Add_KeyUp_Blank_Checking("TimeSlotTitle", "TitleWarningDiv", '<?=$Lang['SysMgr']['Timetable']['Warning']['Blank']['Title']?>');
			
			//$('#debugArea').html(ReturnData);
		}
	);
}

function js_Show_Add_Edit_Room_Allocation_Layer(TimeSlotID, Day, RoomAllocationID)
{
	//$.ajax({
	//	type: "POST",
	//	url: "ajax_reload.php",
	//	async: false,
	//	data: "RecordType=Reload_Room_Allocation_Add_Edit_Layer&TimeSlotID=" + TimeSlotID + "&Day=" + Day + "&RoomAllocationID=" + RoomAllocationID,
	//	success: 
	//			function(ReturnData)
	//			{
	//				$('#TB_ajaxContent').html(ReturnData);
	//				
	//				//$('#debugArea').html(ReturnData);
	//			}
	//});

    
	$.post(
		"ajax_reload.php", 
		{ 
			RecordType: "Reload_Room_Allocation_Add_Edit_Layer",
			TimeSlotID: TimeSlotID,
			Day: Day,
			RoomAllocationID: RoomAllocationID
		},
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);
			//$('#debugArea').html(ReturnData);
		}
	);
}

function js_Show_Hide_Copy_Option_Row()
{
	var isShow = $("input[name='isCopy']:checked").val(); 
	
	if (isShow == 1)
	{
		$('table#copySelectionTable').show();
		//$('#AcademicYearRow').show();
		//$('#TermRow').show();
		//$('#TimetableRow').show();
	}
	else
	{
		$('table#copySelectionTable').hide();
		//$('#AcademicYearRow').hide();
		//$('#TermRow').hide();
		//$('#TimetableRow').hide();
	}
}

/********************* End Show Layer *********************/


/********************* Timetable Update *********************/
function js_Insert_Edit_Timetable(AcademicYearID, YearTermID, TimetableID)
{
	var TimetableTitle = encodeURIComponent(Trim($('input#TimetableTitle').val()));
	
	// Check if title is blank
	if (js_Is_Input_Blank('TimetableTitle', 'TitleWarningDiv', '<?=$Lang['SysMgr']['Timetable']['Warning']['Blank']['Title']?>'))
	{
		$("input#TimetableTitle").focus();
		return false;
	}
	
	// Check title duplication
	var inputValue = $('input#TimetableTitle').val();
	$('div#TitleWarningDiv').hide(jsShowHideSpeed);
	
	$.post(
		"ajax_validation.php", 
		{
			RecordType: 'Timetable_Title_Duplicate',
			InputValue: inputValue,
			TimetableID: TimetableID,
			YearTermID: YearTermID
		},
		function(ReturnData)
		{
			if (ReturnData == '1')
			{
				// valid code
				$('#TitleWarningDiv').hide(jsShowHideSpeed);
				
				var action = '';
				if (TimetableID == '')
					action = "Add";
				else
					action = "Edit";
					
				// if edit table and choose to copy, alert the user that the eisting records will all be deleted first
				var isCopy = $("input[name='isCopy']:checked").val(); 
				if (action == "Edit" && isCopy == 1)
				{
					if (!confirm('<?=$Lang['SysMgr']['Timetable']['jsWarning']['Copy']['Timetable']?>'))
						return false;
				}
				
				// Get the TimetableID to copy
				var targetTimetableID;
				if (isCopy == 1)
				{
					targetTimetableID = $('select#SelectedTimetableID_ForCopyTimetable').val();
					
					if (targetTimetableID == '' || targetTimetableID == null)
					{
						alert('<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Timetable']?>');
						$('select#SelectedTimetableID_ForCopyTimetable').focus();
						
						return false;
					}
				}
				
				var releaseStatus = $('input#releaseStatus_public').attr('checked')? 1 : 0;
				
				// Add Edit Timetable
				$.post(
					"ajax_update_timetable.php", 
					{ 
						Action: action,
						TimetableID: TimetableID,
						AcademicYearID: CurAcademicYearID,
						YearTermID: CurYearTermID,
						TimetableName: TimetableTitle,
						releaseStatus: releaseStatus,
						isCopy: isCopy,
						TargetTimetableID: targetTimetableID
					},
					function(ReturnData)
					{
						if (action == "Add")
						{
							var InsertedTimetableID = ReturnData;
							if (InsertedTimetableID == 0)
							{
								js_Show_System_Message("Timetable", action, 0);
							}
							else
							{
								js_Reload_Timetable_Selection(InsertedTimetableID, 0);
								js_Reload_Timetable(InsertedTimetableID);
								js_Hide_ThickBox();
								js_Show_System_Message("Timetable", action, 1);
							}
						}
						else if (action == "Edit")
						{
							var success = ReturnData;
							
							if (success == 0)
							{
								js_Show_System_Message("Timetable", action, 0);
							}
							else
							{
								js_Reload_Timetable_Selection(CurTimetableID, 0);
								js_Reload_Timetable(CurTimetableID);
								js_Hide_ThickBox();
								js_Show_System_Message("Timetable", action, 1);
							}
						}
						
						//$('#debugArea').html(ReturnData);
					}
				);
			}
			else
			{
				// invalid code => show warning
				$('div#TitleWarningDiv').html('<?= $Lang['SysMgr']['Timetable']['Warning']['TitleDuplicated'] ?>');
				$('div#TitleWarningDiv').show(jsShowHideSpeed);
				
				//$('#debugArea').html('aaa ' + ReturnData);
			}
		}
	);
}

function js_Delete_Timetable()
{
    $.post(
        "ajax_update_timetable.php",
        {
            Action: "CheckForDelete",
            TimetableID: CurTimetableID
        },
        function(ReturnData)
        {
            if (ReturnData == '1') {
                if (confirm('<?=$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['Timetable']?>')) {
                    $.post(
                        "ajax_update_timetable.php",
                        {
                            Action: "Delete",
                            TimetableID: CurTimetableID
                        },
                        function (ReturnData) {
                            //$('#TimetableDiv').hide();
                            $('div#TimetableDiv').html('');

                            CurTimetableID = '';
                            js_Reload_Timetable_Selection(CurTimetableID, 0);
                            js_Update_Icon_Display_Status();

                            // Show system messages
                            js_Show_System_Message("Timetable", "Delete", ReturnData);

                            //$('#debugArea').html(ReturnData);
                        }
                    );
                }
            }
            else {
                alert("<?php echo $Lang['SysMgr']['Timetable']['jsWarning']['Delete']['TimetableUsed'];?>" + ReturnData);
            }
        }
    );

	return false;
}

function js_Vaildate_Timetable_Title(YearTermID, TimetableID)
{
	var inputValue = $('input#TimetableTitle').val();
	$('div#TitleWarningDiv').hide(jsShowHideSpeed);
	
	if (!js_Is_Input_Blank('TimetableTitle', 'TitleWarningDiv', '<?=$Lang['SysMgr']['Timetable']['Warning']['Blank']['Title']?>'))
	{
		$.post(
			"ajax_validation.php", 
			{
				RecordType: 'Timetable_Title_Duplicate',
				InputValue: inputValue,
				TimetableID: TimetableID,
				YearTermID: YearTermID
			},
			function(ReturnData)
			{
				if (ReturnData == '1')
				{
					// valid code
					$('#TitleWarningDiv').hide(jsShowHideSpeed);
				}
				else
				{
					// invalid code => show warning
					$('div#TitleWarningDiv').html('<?= $Lang['SysMgr']['Timetable']['Warning']['TitleDuplicated'] ?>').show(jsShowHideSpeed);
					
					//$('#debugArea').html('aaa ' + ReturnData);
				}
			}
		);
	}
}
/********************* End Timetable Update *********************/


/********************* Cycle Days Update *********************/
function js_Add_CycleDays()
{
	action = "Add";
	
	$.post(
		"ajax_update_timetable.php", 
		{ 
			Action: "Add_CycleDays",
			TimetableID: CurTimetableID
		},
		function(ReturnData)
		{
			if (ReturnData == 0)
			{
				js_Show_System_Message("CycleDay", action, 0);
			}
			else
			{
				js_Reload_Timetable(CurTimetableID);
				js_Show_System_Message("CycleDay", action, 1);
			}
			
			//$('#debugArea').html(ReturnData);
		}
	);
}

function js_Delete_CycleDays(Day)
{
	if (Day == null && jsLastClickedDay != '')
		Day = jsLastClickedDay;
		
	if (confirm('<?=$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['CycleDays']?>'))
	{
		action = "Delete";
	
		$.post(
			"ajax_update_timetable.php", 
			{ 
				Action: "Delete_CycleDays",
				TimetableID: CurTimetableID,
				Day: Day
			},
			function(ReturnData)
			{
				if (ReturnData == 0)
				{
					js_Show_System_Message("CycleDay", action, 0);
				}
				else
				{
					js_Reload_Timetable(CurTimetableID);
					js_Show_System_Message("CycleDay", action, 1);
				}
				
				//$('#debugArea').html(InsertedTimetableID);
			}
		);
	}
	
	js_Hide_Delete_Option_Layer();
}
/********************* End Cycle Days Update *********************/


/********************* Time Slot Update *********************/
function js_Insert_Edit_TimeSlot(TimeSlotID, LastTimeSlotID, NextTimeSlotID)
{
	var TimeSlotTitle = encodeURIComponent(Trim($('input#TimeSlotTitle').val()));
	var StartTimeHour = Trim($('select#startTimeHour').val());
	var StartTimeMin = Trim($('select#startTimeMin').val());
	var EndTimeHour = Trim($('select#endTimeHour').val());
	var EndTimeMin = Trim($('select#endTimeMin').val());
	var isFirstPmSession = $('input#firstPmLessonRadio_yes').attr('checked')? 1 : 0;
	
	// 1. check no Title
	// Check if title is blank
	if (js_Is_Input_Blank('TimeSlotTitle', 'TitleWarningDiv', '<?=$Lang['SysMgr']['Timetable']['Warning']['Blank']['Title']?>'))
	{
		$("input#TimeSlotTitle").focus();
		return false;
	}
	
	// 2. check start time earlier than end time
	var startTimeHourObj = document.getElementById('startTimeHour');
	var startTimeMinObj = document.getElementById('startTimeMin');
	var endTimeHourObj = document.getElementById('endTimeHour');
	var endTimeMinObj = document.getElementById('endTimeMin');
	var is_StartTime_GreaterThan_EndTime = js_Compare_StartTime_EndTime(startTimeHourObj, startTimeMinObj, endTimeHourObj, endTimeMinObj);
	if (!is_StartTime_GreaterThan_EndTime)
	{
		$('div#TimeWarningDiv')
			.html('<?=$Lang['SysMgr']['Timetable']['jsWarning']['StartTimeMustBeEarlierThenEndTime']?>')
			.show(jsShowHideSpeed);
			
		$('select#startTimeHour').focus();
		return false;
	}
	
	// 3. check time overlap
	var StartHour = '';
	var StartMin = '';
	var StartTime = '';
	var EndHour = '';
	var EndMin = '';
	var EndTime = '';
	
	StartHour = $('select#startTimeHour').val();
	StartMin = $('select#startTimeMin').val();
	StartTime = StartHour + ":" + StartMin + ":00";
	
	EndHour = $('select#endTimeHour').val();
	EndMin = $('select#endTimeMin').val();
	EndTime = EndHour + ":" + EndMin + ":00";
	
	// check start time
	$.post(
		"ajax_validation.php", 
		{ 
			RecordType: "TimeSlot_Overlap",
			TimetableID: CurTimetableID,
			StartTime: StartTime,
			EndTime: EndTime,
			TimeSlotID: TimeSlotID
		},
		function(ReturnData)
		{
			if (ReturnData == '0')
			{
				$('div#TimeWarningDiv')
					.html('<?=$Lang['SysMgr']['Timetable']['jsWarning']['TimeSlotOverlapped']?>')
					.show(jsShowHideSpeed);
				$('select#endTimeHour').focus();
				return false;
			}
			else
			{
				// valid - hide warning msg
				$('div#TimeWarningDiv').hide();
				
				// add time slot
				var action = '';
				var actionType = '';
				if (TimeSlotID == '')
				{
					action = "Add";
					actionType = "Add_TimeSlot";
				}
				else
				{
					action = "Edit";
					actionType = "Edit_TimeSlot";
				}
					
				$.post(
					"ajax_update_timetable.php", 
					{ 
						Action: actionType,
						TimetableID: CurTimetableID,
						TimeSlotID: TimeSlotID,
						TimeSlotTitle: TimeSlotTitle,
						StartTimeHour: StartTimeHour,
						StartTimeMin: StartTimeMin,
						EndTimeHour: EndTimeHour,
						EndTimeMin: EndTimeMin,
						LastTimeSlotID: LastTimeSlotID,
						NextTimeSlotID: NextTimeSlotID,
						isFirstPmSession: isFirstPmSession
					},
					function(ReturnData)
					{
						if (ReturnData == 0)
						{
							// failed
							js_Show_System_Message("TimeSlot", action, ReturnData);
						}
						else
						{
							// success
							js_Reload_Timetable(CurTimetableID);
							js_Hide_ThickBox();
							js_Show_System_Message("TimeSlot", action, ReturnData);
						}
						
						//$('#debugArea').html(ReturnData);
					}
				);
			}
			//$('#debugArea').html(ReturnData);
		}
	);
}

function js_Delete_TimeSlot(TimeSlotID)
{
	if (TimeSlotID == null && jsLastClickedTimeSlotID != '')
		TimeSlotID = jsLastClickedTimeSlotID;
		
	if (confirm('<?php echo ($sys_custom['eBooking']['EverydayTimetable'] ? $Lang['SysMgr']['Timetable']['jsWarning']['Delete']['TimeSlotWithBooking'] : $Lang['SysMgr']['Timetable']['jsWarning']['Delete']['TimeSlot']);?>'))
	{
		action = "Delete";
		
		$.post(
			"ajax_update_timetable.php", 
			{ 
				Action: "Delete_TimeSlot",
				TimetableID: CurTimetableID,
				TimeSlotID: TimeSlotID
			},
			function(ReturnData)
			{
				if (ReturnData == 0)
				{
					js_Show_System_Message("TimeSlot", action, 0);
				}
				else
				{
					js_Reload_Timetable(CurTimetableID);
					js_Show_System_Message("TimeSlot", action, 1);
				}
				
				//$('#indexDebugArea').html(ReturnData);
			}
		);
	}
	
	js_Hide_Delete_Option_Layer();
}

function js_Validate_TimeSlot(TimeSlotID)
{
	// Check start time earlier than end time
	var startTimeHourObj = document.getElementById('startTimeHour');
	var startTimeMinObj = document.getElementById('startTimeMin');
	var endTimeHourObj = document.getElementById('endTimeHour');
	var endTimeMinObj = document.getElementById('endTimeMin');
	var is_StartTime_GreaterThan_EndTime = js_Compare_StartTime_EndTime(startTimeHourObj, startTimeMinObj, endTimeHourObj, endTimeMinObj);
	
	if (!is_StartTime_GreaterThan_EndTime)
	{
		$('div#TimeWarningDiv')
			.html('<?=$Lang['SysMgr']['Timetable']['jsWarning']['StartTimeMustBeEarlierThenEndTime']?>')
			.show(jsShowHideSpeed);
		return false;
	}
	
	// Check time slot overlap
	var StartHour = '';
	var StartMin = '';
	var StartTime = '';
	var EndHour = '';
	var EndMin = '';
	var EndTime = '';
	
	StartHour = $('select#startTimeHour').val();
	StartMin = $('select#startTimeMin').val();
	StartTime = StartHour + ":" + StartMin + ":00";
	
	EndHour = $('select#endTimeHour').val();
	EndMin = $('select#endTimeMin').val();
	EndTime = EndHour + ":" + EndMin + ":00";
	
	$.post(
		"ajax_validation.php", 
		{ 
			RecordType: "TimeSlot_Overlap",
			TimetableID: CurTimetableID,
			StartTime: StartTime,
			EndTime: EndTime,
			TimeSlotID: TimeSlotID
		},
		function(ReturnData)
		{
			if (ReturnData == '0')
			{
				$('div#TimeWarningDiv')
					.html('<?=$Lang['SysMgr']['Timetable']['jsWarning']['TimeSlotOverlapped']?>')
					.show(jsShowHideSpeed);
			}
			else
			{
				// valid - hide warning msg
				$('div#TimeWarningDiv').hide();
			}
			
			//$('#debugArea').html(ReturnData);
		}
	);
	
}
/********************* End Time Slot Update *********************/


/********************* Room Allocation Update *********************/
function js_Insert_Edit_Room_Allocation(TimeSlotID, Day, RoomAllocationID, KeepAdding)
{
	var SelectedBuildingID = $("select#SelectedBuildingID_InAddEditLayer").val();
	var SelectedLocationID = $("select#SelectedRoomID_InAddEditLayer").val();
	var OthersLocation = encodeURIComponent(Trim($("input#OthersLocationTb_InAddEditLayer").val()));
	var SelectedSubjectID = $("select#SelectedSubjectID_InAddEditLayer").val();
	var SelectedSubjectGroupID = $("select#SelectedSubjectGroupID_InAddEditLayer").val();
	
	var ContinuousTimeSlot = $("select#ContinuousTimeSlot").val();
	if (ContinuousTimeSlot == '' || ContinuousTimeSlot == null) {
		ContinuousTimeSlot = 1;
	}

	$('input.formbutton_v30').attr('disabled', 'disabled');
	
	// Check if user has selected subject
	if (SelectedSubjectID == '')
	{
		alert('<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Subject']?>');
		$('input.formbutton_v30').attr('disabled', '');
		return false;
	}
	// Check if user has selected subject group
	if (SelectedSubjectGroupID == '' || SelectedSubjectGroupID == null)
	{
		alert('<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['SubjectGroup']?>');
		$('input.formbutton_v30').attr('disabled', '');
		return false;
	}
	
	if (SelectedBuildingID == '')
	{
		alert('<?=$Lang['SysMgr']['Location']['jsWarning']['PleaseSelectBuilding']?>');
		$('input.formbutton_v30').attr('disabled', '');
		return false;
	}
	
	// Check if the user has entered others location
	if (SelectedBuildingID == '-1')
	{
		if (!check_text(document.getElementById('OthersLocationTb_InAddEditLayer'), '<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseEnter']['Location']?>')) {
			$('input.formbutton_v30').attr('disabled', '');
			return false;
		}
	}
	
	// add update room allocation
	// 1. Check if Subject Group overlap
	//$('div#SubjectGroupWarningDiv').hide(jsShowHideSpeed);
	$('div#SubjectGroupWarningDiv').html('');
	$.post(
		"ajax_validation.php", 
		{
			RecordType: "SubjectGroupOverlap",
			TimeSlotID: TimeSlotID,
			Day: Day,
			SubjectGroupID: SelectedSubjectGroupID,
			RoomAllocationID: RoomAllocationID,
			TimetableID: CurTimetableID
		},
		function(ReturnData)
		{
			if (ReturnData != '')
			{
				// invalid
				$('div#SubjectGroupWarningDiv')
					.html(ReturnData)
					.show(jsShowHideSpeed);
				$('select#SelectedSubjectGroupID_InAddEditLayer').focus();
				$('input.formbutton_v30').attr('disabled', '');
			}
			else
			{
				// valid
				//$('div#SubjectGroupWarningDiv').hide(jsShowHideSpeed);
				$('div#SubjectGroupWarningDiv').html('');
				
				// 2. Check if Location occupied
				//$('div#LocationWarningDiv').hide(jsShowHideSpeed);
				$('div#LocationWarningDiv').html('');
				$.post(
					"ajax_validation.php", 
					{
						RecordType: "Room_Overlap",
						TimeSlotID: TimeSlotID,
						Day: Day,
						LocationID: SelectedLocationID,
						RoomAllocationID: RoomAllocationID,
						TimetableID: CurTimetableID
					},
					function(ReturnData)
					{
						if (ReturnData == '0' && SelectedBuildingID != '-1')
						{
							// invalid
							$('div#LocationWarningDiv')
								.html('<?=$Lang['SysMgr']['Timetable']['jsWarning']['RoomUsedAlready']?>')
								.show(jsShowHideSpeed);
							$('select#SelectedRoomID_InAddEditLayer').focus();
							$('input.formbutton_v30').attr('disabled', '');
						}
						else
						{
							// valid
							//$('div#LocationWarningDiv').hide(jsShowHideSpeed);
							$('div#LocationWarningDiv').hide(jsShowHideSpeed);
							
							// Add or Edit Room Allocation
							var action = '';
							var actionType = '';
							if (RoomAllocationID == '')
							{
								action = "Add";
								actionType = "Add_RoomAllocation";
							}
							else
							{
								action = "Edit";
								actionType = "Edit_RoomAllocation";
							}
							
							Block_Thickbox();
							$.post(
								"ajax_update_timetable.php", 
								{ 
									Action: actionType,
									TimeSlotID: TimeSlotID,
									Day: Day,
									RoomAllocationID: RoomAllocationID,
									BuildingID: SelectedBuildingID,
									LocationID: SelectedLocationID,
									OthersLocation: OthersLocation,
									SubjectGroupID: SelectedSubjectGroupID,
									TimetableID: CurTimetableID,
									ContinuousTimeSlot: ContinuousTimeSlot
								 },
								function(ReturnData)
								{
									// refresh timetable
									//js_Reload_Timetable(CurTimetableID);
									//js_Reload_Room_Allocation_Detail_Table(TimeSlotID, Day);
									
									var returnDataAry = ReturnData.split('<!--separator-->');
									var timeSlotIdList = returnDataAry[0];
									var returnStatus = returnDataAry[1];
									
									// reload lesson table of each timeslot of the day
									var timeSlotIdAry = timeSlotIdList.split(',');
									var numOfTimeSlot = timeSlotIdAry.length;
									for (i=0; i<numOfTimeSlot; i++) {
										var _targetTimeSlotId = timeSlotIdAry[i];
										js_Reload_Room_Allocation_Detail_Table(_targetTimeSlotId, Day);
									}
									
									
									if (KeepAdding != 1)
										js_Hide_ThickBox();
									else
										js_Show_Add_Edit_Room_Allocation_Layer(TimeSlotID, Day, '');
									
									UnBlock_Thickbox();
									
									// Show system messages
									js_Show_System_Message("RoomAllocation", action, returnStatus);
									$('input.formbutton_v30').attr('disabled', '');
									
									//$('#debugArea').html(ReturnData);
								}
							);
						}
						
						//$('#debugArea').html(ReturnData);
					}
				);
			}
			//$('#debugArea').html(ReturnData);
		}
	);
	
	//$('#debugArea').html(ReturnData);
}

function js_Delete_Room_Allocation(RoomAllocationID, TimeSlotID, Day, jsDeleteByTimeSlotID, jsDeleteByDay, jsIsContinuousLesson)
{
	RoomAllocationID = RoomAllocationID || '';
	TimeSlotID = TimeSlotID || '';
	Day = Day || '';
	jsIsContinuousLesson = jsIsContinuousLesson || false;
	
	if (jsDeleteByTimeSlotID && jsLastClickedTimeSlotID != '') {
		TimeSlotID = jsLastClickedTimeSlotID;
	}
	
	if (jsDeleteByDay && jsLastClickedDay != '') {
		Day = jsLastClickedDay;
	}
	
	var warningMsg = '';
	if (RoomAllocationID != '') {
		// delete a specific lesson
		warningMsg = (jsIsContinuousLesson)? '<?=$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['ContinuousLesson']?>' : '<?=$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['RoomAllocation']?>';
	}
	else if (RoomAllocationID=='' && TimeSlotID!='' && Day!= '') {
		// delete all lessons within a day and a timeslot
		warningMsg = '<?=$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['DeleteAllLessonsOfDayAndTimeSlot']?>';
	}
	else if (RoomAllocationID=='' && TimeSlotID=='' && Day!= '') {
		// delete all lessons for a day
		warningMsg = '<?=$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['DeleteAllLessonsOfDay']?>';
	}
	else if (RoomAllocationID=='' && TimeSlotID!='' && Day== '') {
		// delete all lessons for a timeslot
		warningMsg = '<?=$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['DeleteAllLessonsOfTimeSlot']?>';
	}
	
	
	if (confirm(warningMsg)) {
		Block_Thickbox();
		
		// Load the Selected Filter values to the global variables
		js_Refresh_Filter_Selection_Value();
		
		$.post(
			"ajax_update_timetable.php", 
			{ 
				Action: "Delete_RoomAllocation",
				RoomAllocationID: RoomAllocationID,
				Day: Day,
				TimeSlotID: TimeSlotID,
				IsViewMode: 0,
				ViewMode: jsFilterViewMode,
				YearClassID: jsFilterYearClassIDArr,
				SubjectGroupID: jsFilterSubjectGroupIDArr,
				SelectedUserID: jsFilterUserIDArr,
				BuildingID: jsFilterBuildingID,
				LocationID: jsFilterLocationIDArr,
				OthersLocation: jsFilterOthersLocation,
				Identity: jsFilterIdentity,
				TimetableID: CurTimetableID
			},
			function(ReturnData)
			{
//				if (TimeSlotID != '' && Day != '') {
//					if (jsIsContinuousLesson) {
//						var returnDataAry = ReturnData.split('<!--separator-->');
//						var timeSlotIdList = returnDataAry[0];
//						ReturnData = returnDataAry[1];
//						
//						// reload lesson table of each timeslot of the day
//						var timeSlotIdAry = timeSlotIdList.split(',');
//						var numOfTimeSlot = timeSlotIdAry.length;
//						for (i=0; i<numOfTimeSlot; i++) {
//							var _targetTimeSlotId = timeSlotIdAry[i];
//							js_Reload_Room_Allocation_Detail_Table(_targetTimeSlotId, Day);
//						}
//					}
//					else {
//						js_Reload_Room_Allocation_Detail_Table(TimeSlotID, Day);
//					}
//				}
//				else if (TimeSlotID == '' && Day != '') {
//					// Refresh all TimeSlot in that Day
//					//var jsTimeSlotIDList = $('input#TimeSlotIDList').val();
//					//var jsTimeSlotIDArr = jsTimeSlotIDList.split(',');
//					var jsTimeSlotIDArr = getTimeSlotAry();
//					var jsNumOfTimeSlot = jsTimeSlotIDArr.length;
//					var i, jsThisTimeSlotID;
//					
//					for (i=0; i<jsNumOfTimeSlot; i++)
//					{
//						jsThisTimeSlotID = jsTimeSlotIDArr[i];
//						js_Reload_Room_Allocation_Detail_Table(jsThisTimeSlotID, Day);
//					}
//				}
//				else if (TimeSlotID != '' && Day == '') {
//					// Refresh all Day in that TimeSlot
//					var jsNumOfCycleDays = $('input#NumOfCycleDays').val();
//					var i, jsThisDay;
//					
//					for (i=1; i<=jsNumOfCycleDays; i++)
//					{
//						js_Reload_Room_Allocation_Detail_Table(TimeSlotID, i);
//					}
//				}

				var returnDataAry = ReturnData.split('<!--separator-->');
				var timeSlotIdList = returnDataAry[0];
				ReturnData = returnDataAry[1];

				// reload lesson table of each timeslot of the day
				var timeSlotIdAry = timeSlotIdList.split(',');
				var numOfTimeSlot = timeSlotIdAry.length;
				
				if (TimeSlotID != '' && Day == '') {
					// Refresh all Day in that TimeSlot
					var jsNumOfCycleDays = $('input#NumOfCycleDays').val();
					
					var i, j, jsThisDay;
					for (i=0; i<numOfTimeSlot; i++) {
						var _targetTimeSlotId = timeSlotIdAry[i];
						for (j=1; j<=jsNumOfCycleDays; j++) {
							js_Reload_Room_Allocation_Detail_Table(_targetTimeSlotId, j);
						}
					}
				}
				else {
					for (i=0; i<numOfTimeSlot; i++) {
						var _targetTimeSlotId = timeSlotIdAry[i];
						js_Reload_Room_Allocation_Detail_Table(_targetTimeSlotId, Day);
					}
				}
				
				// Show system messages
				js_Show_System_Message("RoomAllocation", "Delete", ReturnData);
				
				//js_Hide_Delete_Option_Layer();
				UnBlock_Thickbox();
				
				//$('#debugArea').html(ReturnData);
			}
		);
	}
	
	js_Hide_Delete_Option_Layer();
	
	return false;
}

function js_Validate_Room_Vacancy(TimeSlotID, Day, RoomAllocationID)
{
	if (RoomAllocationID == null)
		RoomAllocationID = '';
		
	var SelectedLocationID = $("#SelectedRoomID_InAddEditLayer").val();
	//$('div#LocationWarningDiv').hide(jsShowHideSpeed);
	//$('div#LocationWarningDiv').html('');
	
	$.post(
		"ajax_validation.php", 
		{
			RecordType: "Room_Overlap",
			TimeSlotID: TimeSlotID,
			Day: Day,
			LocationID: SelectedLocationID,
			RoomAllocationID: RoomAllocationID,
			TimetableID: CurTimetableID
		},
		function(ReturnData)
		{
			if (ReturnData == '0')
			{
				// invalid
				$('div#LocationWarningDiv')
					.html('<?=$Lang['SysMgr']['Timetable']['jsWarning']['RoomUsedAlready']?>')
					.show(jsShowHideSpeed);
				$('select#SelectedRoomID_InAddEditLayer').focus();
			}
			else
			{
				// valid
				//$('#LocationWarningDiv').hide(jsShowHideSpeed);
				$('div#LocationWarningDiv').html('');
			}
			
			//$('#debugArea').html(ReturnData);
		}
	);
}

function js_Validate_Subject_Group_Overlap(TimeSlotID, Day, RoomAllocationID)
{
	if (RoomAllocationID == null)
		RoomAllocationID = '';
		
	var SelectedSubjectGroupID = $("#SelectedSubjectGroupID_InAddEditLayer").val();
	$('div#SubjectGroupWarningDiv').hide(jsShowHideSpeed);
	
	$.post(
		"ajax_validation.php", 
		{
			RecordType: "SubjectGroupOverlap",
			TimeSlotID: TimeSlotID,
			Day: Day,
			SubjectGroupID: SelectedSubjectGroupID,
			RoomAllocationID: RoomAllocationID,
			TimetableID: CurTimetableID
		},
		function(ReturnData)
		{
			if (ReturnData != '')
			{
				// invalid
				$('div#SubjectGroupWarningDiv')
					.html(ReturnData)
					.show(jsShowHideSpeed);
				$('select#SelectedSubjectGroupID_InAddEditLayer').focus();
			}
			else
			{
				// valid
				$('div#SubjectGroupWarningDiv').hide(jsShowHideSpeed);
			}
			
			//$('#debugArea').html(ReturnData);
		}
	);
}



/********************* End Room Allocation Update *********************/


/********************* Reload page *********************/
function Generate_Timetable(jsShowFilter)
{
	if (jsShowFilter == null)
		jsShowFilter = 0;
		
	js_Reload_Timetable(CurTimetableID, jsShowFilter);
	
	//js_Show_Hide_Filter_Div('hide');
}

function js_Reload_Timetable(TimetableID, jsShowFilter)
{
	if (jsShowFilter == null)
		jsShowFilter = 0;	
		
	$('div#TimetableDiv').html('');
	if (TimetableID != '')
	{
		// Load the Selected Filter values to the global variables
		js_Refresh_Filter_Selection_Value();
		
		$('div#TimetableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"ajax_reload.php", 
			{ 
				RecordType: "Reload_Timetable",
				TimetableID: TimetableID,
				ViewMode: jsFilterViewMode,
				IsViewMode: 0,
				YearClassID: jsFilterYearClassIDArr,
				SubjectGroupID: jsFilterSubjectGroupIDArr,
				SelectedUserID: jsFilterUserIDArr,
				BuildingID: jsFilterBuildingID,
				LocationID: jsFilterLocationIDArr,
				OthersLocation: jsFilterOthersLocation,
				Identity: jsFilterIdentity
			},
			function(ReturnData)
			{
				// Show Hide Details according to the selected display options
				js_Update_Timetable_Display();
				
				initThickBox();
				
				if (jsShowFilter==1)
					js_Show_Hide_Filter_Div('show');
				
				// Load the overall Timetable first and then load lesson one by one
				//$.post(
				//	"ajax_reload.php",
				//	{ 
				//		RecordType: "Get_TimeSlot_CycleDay_Info",
				//		TimetableID: TimetableID 
				//	},
				//	function (ReturnString)
				//	{
				//		var jsInfoArr = ReturnString.split("|||---separator---|||");
				//		
				//		var jsCycleDays = jsInfoArr[0];
				//		var jsTimeSlotIDArr = jsInfoArr[1].split(",");
				//		
				//		var i = 0;
				//		var j = 0;
				//		var jsThisTimeSlotID, jsThisDay;
				//		for (i=0; i<jsTimeSlotIDArr.length; i++)
				//		{
				//			jsThisTimeSlotID = jsTimeSlotIDArr[i];
				//			for (j=0; j<jsCycleDays; j++)
				//			{
				//				jsThisDay = j + 1;
				//				js_Reload_Room_Allocation_Detail_Table(jsThisTimeSlotID, jsThisDay, 0);
				//			}
				//		}
				//	}
				//);
				
				//$('#debugArea').html(ReturnData);
			}
		);
	}
	
	//Blind_Cookies_To_Object();
	AssignCookies();
}

// NeedBlockUI = 1 => From add/edit/delete lesson
// NeedBlockUI = 0 => First loading
function js_Reload_Room_Allocation_Detail_Table(TimeSlotID, Day, NeedBlockUI)
{
	if (NeedBlockUI == null)
		NeedBlockUI = 1;
		
	// Load the Selected Filter values to the global variables
	js_Refresh_Filter_Selection_Value();
	
	var DivID = "DetailTableDiv_" + TimeSlotID + "_" + Day;
	if (NeedBlockUI == 1)
	{
		//$('div#' + DivID).hide();
		$('div#' + DivID).html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	}
	
	if (TimeSlotID != '' && Day != '')
	{
		//if (NeedBlockUI == 1)
		//{
			//Block_Document();
		//	Block_Element(DivID);
		//}
			
		$.post(
			"ajax_reload.php", 
			{ 
				RecordType: "Reload_Room_Allocation_Detail_Table",
				TimeSlotID: TimeSlotID,
				Day: Day,
				IsViewMode: 0,
				ViewMode: jsFilterViewMode,
				YearClassID: jsFilterYearClassIDArr,
				SubjectGroupID: jsFilterSubjectGroupIDArr,
				SelectedUserID: jsFilterUserIDArr,
				BuildingID: jsFilterBuildingID,
				LocationID: jsFilterLocationIDArr,
				OthersLocation: jsFilterOthersLocation,
				Identity: jsFilterIdentity
			},
			function(ReturnData)
			{
				$('div#' + DivID).html(ReturnData);
				js_Update_Timetable_Display();
				
				//if (NeedBlockUI == 1)
				//{
					//UnBlock_Document();
				//	UnBlock_Element(DivID);
				//}
					
				//initThickBox();
				tb_init('table#ContentTable_' + TimeSlotID + '_' + Day + ' a.thickbox');
				
				
				//$('#indexDebugArea').html(ReturnData);
			}
		);
	}
}

function js_Refresh_Filter_Selection_Value()
{
	// Filtering
	jsFilterViewMode = js_Check_Object_Defined($('select#SelectedViewMode').val());
	jsFilterIdentity = js_Check_Object_Defined($('select#SelectedIdentity').val());
	
	switch(jsFilterViewMode)
	{
		case "Class":
			// Class Mode
			if ($('select#SelectedYearClassIDArr\\[\\]').val())
				jsFilterYearClassIDArr = $('select#SelectedYearClassIDArr\\[\\]').val().join(',');
			else
				jsFilterYearClassIDArr = '';
			break;
		case "SubjectGroup":
			// Subject Group Mode
			if ($('select#SelectedSubjectGroupIDArr\\[\\]').val())
				jsFilterSubjectGroupIDArr = $('select#SelectedSubjectGroupIDArr\\[\\]').val().join(',');
			else
				jsFilterSubjectGroupIDArr = '';
			break;
		case "Personal":
			// Personal Mode
			if ($('select#SelectedUserIDArr\\[\\]').val())
				jsFilterUserIDArr = $('select#SelectedUserIDArr\\[\\]').val().join(',');
			else
				jsFilterUserIDArr = '';
			break;
		case "Room":
			// Room Mode
			jsFilterBuildingID = $('select#SelectedBuildingID').val();
			
			if ($('select#SelectedLocationIDArr\\[\\]').val() != null)
				jsFilterLocationIDArr = $('select#SelectedLocationIDArr\\[\\]').val().join(',');
			else
				jsFilterLocationIDArr = '';
			
			jsFilterOthersLocation = encodeURIComponent(Trim($("input#OthersLocationTb").val()));
			break;
	}
	
	jsFilterYearClassIDArr = js_Check_Object_Defined(jsFilterYearClassIDArr);
	jsFilterSubjectGroupIDArr = js_Check_Object_Defined(jsFilterSubjectGroupIDArr);
	jsFilterUserIDArr = js_Check_Object_Defined(jsFilterUserIDArr);
	jsFilterBuildingID = js_Check_Object_Defined(jsFilterBuildingID);
	jsFilterLocationIDArr = js_Check_Object_Defined(jsFilterLocationIDArr);
	jsFilterOthersLocation = js_Check_Object_Defined(jsFilterOthersLocation);
}
/********************* End Reload page *********************/


/********************* Filtering functions *********************/


function js_Reload_Secondary_Selection(SelectedViewMode, jsSelectAll)
{	
	//$('#SecondaryFilterDiv').hide();
	$('div#SecondaryFilterDiv').html('');
	//$('#SecondaryFilterDiv').show();
	
	if (SelectedViewMode != '')
		js_Reload_Secondary_Filter_Table(SelectedViewMode, jsSelectAll);
		
	$('div#SecondaryFilterDiv').show();
}

function js_Reload_Secondary_Filter_Table(ViewMode, jsSelectAll)
{
	$('div#SecondaryFilterDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Reload_Secondary_Filter_Table',
			ViewMode: ViewMode,
			YearTermID: CurYearTermID,
			InLayer: 1
		},
		function(ReturnData)
		{
			$('div#SecondaryFilterDiv').show();
			
			if (jsIsInitialize == 1)
			{
				if (ViewMode == 'Class')
				{
					if ($.cookies.get('SelectedYearID') == null)
						$('select#SelectedYearID option:nth-child(2)').attr('selected', true);
					else
						$('select#SelectedYearID').val($.cookies.get('SelectedYearID'));
						
					js_Reload_Class_Selection(1, jsIsInitialize, jsSelectAll);
				}
				else if (ViewMode == 'SubjectGroup')
				{
					if ($.cookies.get('SelectedSubjectID') == null)
						$('select#SelectedSubjectID option:nth-child(2)').attr('selected', true);
					else
						$('select#SelectedSubjectID').val($.cookies.get('SelectedSubjectID'));
						
					js_Reload_Subject_Group_Selection(jsIsInitialize, jsSelectAll);
				}
				else if (ViewMode == 'Personal')
				{
					if ($.cookies.get('SelectedIdentity') == null)
						$('select#SelectedIdentity option:nth-child(2)').attr('selected', true);
					else
						$('select#SelectedIdentity').val($.cookies.get('SelectedIdentity'));
						
					// For student selection only
					if ($('select#SelectedIdentity').val() == 'Student')
					{
						if ($.cookies.get('SelectedYearID') == null)
							$('select#SelectedYearID option:nth-child(2)').attr('selected', true);
						else
							$('select#SelectedYearID').val($.cookies.get('SelectedYearID'));
							
						js_Reload_Class_Selection(0, jsIsInitialize, jsSelectAll);
					}
						
					js_Show_Hide_Form_Class_Selection(jsIsInitialize);
				}
				else if (ViewMode == 'Room')
				{
					if ($.cookies.get('SelectedBuildingID') == null)
						$('select#SelectedBuildingID option:nth-child(2)').attr('selected', true);
					else
						$('select#SelectedBuildingID').val($.cookies.get('SelectedBuildingID')).change();
				}
				
				jsIsInitialize = 0;
			}
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

/******* Class Mode  *******/
function js_Reload_Class_Selection(isMultiple, jsIsInitialize, jsSelectAll)
{
	var selectedAcademicYearID = $('select#SelectedAcademicYearID').val();
	var selectedYearID = $('select#SelectedYearID').val();
	
	$('tr#ClassSelectionTr').hide();
	$('div#ClassSelectionDiv').hide();
	$("div#SubmitBtnDiv").hide();
	
	if (selectedYearID != "")
	{
		var OnChange = '';
		if (isMultiple == 0)
		{
			// from Personal view mode
			OnChange = "js_Reload_Target_Selection('Student')";
		}
	
		$('div#ClassSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"ajax_reload.php", 
			{ 
				RecordType: 'Reload_Class_Selection',
				ID: 'SelectedYearClassIDArr[]',
				AcademicYearID: selectedAcademicYearID,
				YearID: selectedYearID,
				OnChange: OnChange,
				NoFirst: 1,
				IsMultiple: isMultiple
			},
			function(ReturnData)
			{
				if (jsSelectAll != 1 && jsIsInitialize == 1 && ($.cookies.get('SelectedYearClassIDArr[]') != null && $.cookies.get('SelectedYearClassIDArr[]') != 'undefined'))
				{
					$("select#SelectedYearClassIDArr\\[\\]").val($.cookies.get('SelectedYearClassIDArr[]'));
				}
				else if (isMultiple == 1)
				{
					$("select#SelectedYearClassIDArr\\[\\] option").attr("selected","selected");
				}
						
				if (isMultiple == 1)
					$("div#SubmitBtnDiv").show();
				else
					js_Reload_Target_Selection('Student', jsIsInitialize);
	
				$('tr#ClassSelectionTr').show();
				$('div#ClassSelectionDiv').show();
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
}
/******* End of Class Mode  *******/



/******* Subject Mode  *******/
function js_Reload_Subject_Group_Selection(jsIsInitialize)
{
	var selectedSubjectID = $('select#SelectedSubjectID').val();
	
	var selectedAcademicYearID = CurAcademicYearID;
	var selectedYearTermID = CurYearTermID;
	
	$('tr#SubjectGroupSelectionTr').hide();
	$("div#SubmitBtnDiv").hide();
	
	if (selectedSubjectID != '')
	{
		$('div#SubjectGroupSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"ajax_reload.php", 
			{ 
				RecordType: 'Reload_Subject_Group_Selection',
				ID: 'SelectedSubjectGroupIDArr[]',
				AcademicYearID: selectedAcademicYearID,
				YearTermID: selectedYearTermID,
				SubjectID: selectedSubjectID,
				NoFirst: 1,
				IsMultiple: 1
			},
			function(ReturnData)
			{
				if (jsIsInitialize == 1 && $.cookies.get('SelectedSubjectGroupIDArr[]') != null)
					$("select#SelectedSubjectGroupIDArr\\[\\]").val($.cookies.get('SelectedSubjectGroupIDArr[]'));
				else
					$("select#SelectedSubjectGroupIDArr\\[\\] option").attr("selected","selected");
					
				$('tr#SubjectGroupSelectionTr').show();
				$("#SubmitBtnDiv").show();
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
	
	
}
/******* End of Subject Mode  *******/



/******* Personal Mode  *******/
function js_Show_Hide_Form_Class_Selection(jsIsInitialize)
{
	var selectedIdentity = $('select#SelectedIdentity').val();
	$("div#SubmitBtnDiv").hide();
	
	if (selectedIdentity == '')
	{
		$('div#FormSelectionDiv').hide();
		$('div#ClassSelectionDiv').hide();
		$('div#PersonSelectionDiv').hide();
		$('tr#FormSelectionTr').hide();
		$('tr#ClassSelectionTr').hide();
		$('tr#PersonSelectionTr').hide();
		$('div#PersonSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	}	
	else if (selectedIdentity == 'Student')
	{
		if (jsIsInitialize != 1)
			$('select#SelectedYearID').val('');
			
		$('tr#FormSelectionTr').show();
		$('tr#ClassSelectionTr').hide();
		$('tr#PersonSelectionTr').hide();
	}
	else
	{
		$('tr#FormSelectionTr').hide();
		$('tr#ClassSelectionTr').hide();
		$('tr#PersonSelectionTr').show();
		js_Reload_Target_Selection(selectedIdentity, jsIsInitialize);
	}
}

function js_Reload_Target_Selection(selectedIdentity)
{
	var selectedYearClassID = $('select#SelectedYearClassIDArr\\[\\]').val();
	$('tr#PersonSelectionTr').hide();
	$('div#PersonSelectionDiv').hide();
	
	$('div#PersonSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Reload_Target_Selection',
			ID: 'SelectedUserIDArr[]',
			TargetType: selectedIdentity,
			YearClassID: selectedYearClassID,
			NoFirst: 1,
			IsMultiple: 1
		},
		function(ReturnData)
		{
			if (jsIsInitialize == 1 && $.cookies.get('SelectedUserIDArr[]') != null)
				$("select#SelectedUserIDArr\\[\\]").val($.cookies.get('SelectedUserIDArr[]'));
			else
				$("select#SelectedUserIDArr\\[\\] option").attr("selected","selected");
				
			$('tr#PersonSelectionTr').show();
			$('div#PersonSelectionDiv').show();
			
			$("div#SubmitBtnDiv").show();
			//$('#IndexDebugArea').html(ReturnData);
			
			// Since the loading of timetable is faster then the filter selection, so need to refresh the timetable again after the filters are all finished building
			if (jsIsInitialize == 1 && selectedIdentity == 'Student')
				Generate_Timetable(1);
		}
	);
}
/******* End of Personal Mode  *******/


function js_Reset_Filter()
{
	if (confirm("<?=$Lang['SysMgr']['Location']['Warning']['ReloadTimetable']['ClearFilteringWarning']?>"))
	{
		MM_showHideLayers('FilterSelectionDiv','','hide');
		$('select#SelectedViewMode').val('');
		
		js_Reload_Secondary_Selection('');
		Generate_Timetable();
	}
}


/********************* End Filtering functions *********************/


// mouseover show icon
function js_Show_Hide_Btn_Div(ObjID, Action)
{
	if (Action == 1)
	{
		$('#'+ObjID).show();
	}
	else
	{
		$('#'+ObjID).hide();
	}
}

// Hide Add / Edit / Delete Btn if the Term has past
function js_Update_Icon_Display_Status()
{
	// Hide all right hand side filter buttons and layers
	js_Hide_All_Filter_Btn_Div();
	
	if (CurTimetableID == '')
	{
		$('div#TimetableDiv').html('');
	}
	
	if (CurYearTermID != '')
	{
		// check has term past
		$.post(
			"ajax_validation.php", 
			{ 
				RecordType: 'Has_Term_Past',
				YearTermID: CurYearTermID
			},
			function(ReturnData)
			{
				if (ReturnData == 1)
				{
					// Term has past already
					if (CurTimetableID != '')
					{
						// selected timetable
						$('div#editTimetableDiv').hide();
						$('div#deleteTimetableDiv').hide();
						$('div#addTimetableDiv').hide();
						$('div#ImportLessonBtnDiv').hide();
						$('div#ExportLessonBtnDiv').show();
						$('div#FilterButtonDiv').show();
					}
					else
					{
						// no selected timetable
						$('div#editTimetableDiv').hide();
						$('div#deleteTimetableDiv').hide();
						$('div#addTimetableDiv').hide();
						$('div#ImportLessonBtnDiv').hide();
						$('div#ExportLessonBtnDiv').hide();
						$('div#FilterButtonDiv').hide();
					}
				}
				else
				{
					// Term has not past yet
					if (CurTimetableID != '')
					{
						// selected timetable
						$('div#editTimetableDiv').show();
						$('div#deleteTimetableDiv').show();
						$('div#addTimetableDiv').show();
						$('div#ImportLessonBtnDiv').show();
						$('div#ExportLessonBtnDiv').show();
						$('div#FilterButtonDiv').show();
					}
					else
					{
						// no selected timetable
						$('div#editTimetableDiv').hide();
						$('div#deleteTimetableDiv').hide();
						$('div#addTimetableDiv').show();
						$('div#ImportLessonBtnDiv').hide();
						$('div#ExportLessonBtnDiv').hide();
						$('div#FilterButtonDiv').hide();
					}
				}
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
	else
	{
		$('div#editTimetableDiv').hide();
		$('div#deleteTimetableDiv').hide();
		$('div#addTimetableDiv').hide();
		$('div#ImportLessonBtnDiv').hide();
		$('div#ExportLessonBtnDiv').hide();
		$('div#FilterButtonDiv').hide();
	}
}

function js_Hide_All_Filter_Btn_Div()
{
	$('div#FilterButtonDiv').hide();
	MM_showHideLayers('DisplayOptionDiv','','hide');
	MM_showHideLayers('FilterSelectionDiv','','hide');
}


// Load system message
function js_Show_System_Message(RecordType, Action, Status)
{
	var returnMessage = '';
	
	if (RecordType == "Timetable")
	{
		switch(Action)
		{
			case "Add":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Success']['Timetable']?>' : '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Failed']['Timetable']?>';
				break;
			case "Edit":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Success']['Timetable']?>' : '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Failed']['Timetable']?>';
				break;
			case "Delete":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Success']['Timetable']?>' : '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Failed']['Timetable']?>';
				break;
		}
	}
	else if (RecordType == "CycleDay")
	{
		switch(Action)
		{
			case "Add":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Success']['CycleDay']?>' : '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Failed']['CycleDay']?>';
				break;
			case "Edit":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Success']['CycleDay']?>' : '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Failed']['CycleDay']?>';
				break;
			case "Delete":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Success']['CycleDay']?>' : '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Failed']['CycleDay']?>';
				break;
		}
	}
	else if (RecordType == "TimeSlot")
	{
		switch(Action)
		{
			case "Add":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Success']['TimeSlot']?>' : '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Failed']['TimeSlot']?>';
				break;
			case "Edit":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Success']['TimeSlot']?>' : '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Failed']['TimeSlot']?>';
				break;
			case "Delete":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Success']['TimeSlot']?>' : '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Failed']['TimeSlot']?>';
				break;
		}
	}
	else if (RecordType == "RoomAllocation")
	{
		switch(Action)
		{
			case "Add":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Success']['RoomAllocation']?>' : '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Failed']['RoomAllocation']?>';
				break;
			case "Edit":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Success']['RoomAllocation']?>' : '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Failed']['RoomAllocation']?>';
				break;
			case "Delete":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Success']['RoomAllocation']?>' : '<?=$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Failed']['RoomAllocation']?>';
				break;
		}
	}
	
	Get_Return_Message(returnMessage);
}


function toTimeStr(hr_o, min_o) {
    var hr_v = parseInt(hr_o.options[hr_o.selectedIndex].value);
	if (hr_v<10) {
	    hr_v = "0"+hr_v;
	}
	min_v = parseInt(min_o.options[min_o.selectedIndex].value);
	if (min_v<10) {
	    min_v = "0"+min_v;
	}
	var time1 = hr_v + ":" + min_v;
	return time1;
}

// Compare Start Time & End Time and Compare Start Date & End Date in hr/min selection box
function js_Compare_StartTime_EndTime(hr_s, min_s, hr_e, min_e) {
	var time1 = toTimeStr(hr_s, min_s);
	var time2 = toTimeStr(hr_e, min_e);
	
	if (time1 >= time2) {
	    return false;
	}else {
		return true;
	}
}

function js_Go_Export()
{
	var jsObjForm = document.getElementById('form1');
	jsObjForm.action = "export_lesson.php";
	jsObjForm.submit();
}

function js_Go_Import()
{
	window.location = 'import_lesson_step1.php?TimetableID=' + CurTimetableID;
}

function js_Hide_Delete_Option_Layer()
{
	MM_showHideLayers('DeleteCycleDayLayerDiv','','hide');
	MM_showHideLayers('DeleteCycleDayButtonDiv','','hide');
	
	MM_showHideLayers('DeleteTimeSlotLayerDiv','','hide');
	MM_showHideLayers('DeleteTimeSlotButtonDiv','','hide');
}

function js_Show_Delete_Option_Layer(jsType, jsClickedObjID)
{
	var LayerDivID;
	var ButtonDivID;
	if (jsType == 'CycleDay')
	{
		LayerDivID = 'DeleteCycleDayLayerDiv';
		ButtonDivID = 'DeleteCycleDayButtonDiv';
	}
	else if (jsType == 'TimeSlot')
	{
		LayerDivID = 'DeleteTimeSlotLayerDiv';
		ButtonDivID = 'DeleteTimeSlotButtonDiv';
	}
	
	if (document.getElementById(LayerDivID).style.visibility != 'hidden' && jsLastClickedObjectID == jsClickedObjID)
	{
		js_Hide_Delete_Option_Layer();		
	}
	else
	{
		js_Hide_Delete_Option_Layer();
		js_Change_Layer_Position(jsClickedObjID, LayerDivID);
		MM_showHideLayers(LayerDivID,'','show');
		MM_showHideLayers(ButtonDivID,'','show');
	}
	
	// Record the Day for deletion
	var jsTempArr = jsClickedObjID.split("_");
	if (jsType == 'CycleDay')
		jsLastClickedDay = jsTempArr[1];
	else if (jsType == 'TimeSlot')
		jsLastClickedTimeSlotID = jsTempArr[1];
	
	// Record the last clicked object to check the layer display next time
	jsLastClickedObjectID = jsClickedObjID;
}

function js_Change_Layer_Position(jsClickedObjID, jsTargetDivID) {
	var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft');
	var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') + 25;
		
	var jsLayerObj = document.getElementById(jsTargetDivID);
	jsLayerObj.style.left = posleft + "px";
	jsLayerObj.style.top = postop + "px";
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}
	return pos_value;
}

function getTimeSlotAry() {
	return $('input#TimeSlotIDList').val().split(',');
}

</script>