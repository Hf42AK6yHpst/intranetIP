<?php
// using 
/**
 * 2018-08-10 (Henry): fixed sql injection 
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);
	
if ($RecordType == "TimeSlot_Overlap")
{
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	$StartTime = stripslashes($_REQUEST['StartTime']);
	$EndTime = stripslashes($_REQUEST['EndTime']);
	$TimeSlotID = stripslashes($_REQUEST['TimeSlotID']);
	
	$objTimetable = new Timetable($TimetableID);
	$ReturnString = $objTimetable->Is_Valid_TimeSlot($StartTime, $EndTime, $TimeSlotID);
}
else if ($RecordType == "Room_Overlap")
{
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	$TimeSlotID = stripslashes($_REQUEST['TimeSlotID']);
	$Day = stripslashes($_REQUEST['Day']);
	$RoomAllocationID = stripslashes($_REQUEST['RoomAllocationID']);
	$LocationID = stripslashes($_REQUEST['LocationID']);
	
	$objTimetable = new Timetable($TimetableID);
	$ReturnString = $objTimetable->Is_Available_Room($TimeSlotID, $Day, $LocationID, $RoomAllocationID);
}
else if ($RecordType == "SubjectGroupOverlap")
{
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	$TimeSlotID = stripslashes($_REQUEST['TimeSlotID']);
	$Day = stripslashes($_REQUEST['Day']);
	$RoomAllocationID = stripslashes($_REQUEST['RoomAllocationID']);
	$SubjectGroupID = stripslashes($_REQUEST['SubjectGroupID']);
	
	# Check subject group allocated already
	$objTimetable = new Timetable($TimetableID);
	$isOverlappedArr['SubjectGroupOverlap'] = $objTimetable->Is_Subject_Group_Overlapped($TimeSlotID, $Day, $SubjectGroupID, $RoomAllocationID);
	
	# Check if student have joined other allocated subject group
	$isOverlappedArr['SubjectGroupUserOverlap'] = $objTimetable->Has_User_Joined_Others_Subject_Group($TimeSlotID, $Day, $SubjectGroupID, $RoomAllocationID);
	
	if ($isOverlappedArr['SubjectGroupOverlap'])
		$ReturnString = $Lang['SysMgr']['Timetable']['jsWarning']['SubjectGroupAllocatedAlready'];
	else if ($isOverlappedArr['SubjectGroupUserOverlap'])
		$ReturnString = $Lang['SysMgr']['Timetable']['jsWarning']['SubjectGroupUserConflict'];
	else
		$ReturnString = '';
}
else if ($RecordType == "Timetable_Title_Duplicate")
{
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	$InputValue = stripslashes($_REQUEST['InputValue']);
	$YearTermID = IntegerSafe(stripslashes($_REQUEST['YearTermID']));
	
	$libTimetable = new Timetable();
	$ReturnString = $libTimetable->Is_Code_Vaild($InputValue, $YearTermID, $TimetableID);
}
else if ($RecordType == "Has_Term_Past")
{
	$YearTermID = IntegerSafe(stripslashes($_REQUEST['YearTermID']));
	
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	$lib_form_class_manage = new form_class_manage();
	$hasTermPast = $lib_form_class_manage->Has_Term_Past($YearTermID);
	
	$ReturnString = $hasTermPast;
}
else if ($RecordType == "Import_Lesson")
{
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
	include_once($PATH_WRT_ROOT.'includes/libtimetable.php');
	include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
	include_once($PATH_WRT_ROOT.'includes/liblocation.php');
	
	$lexport = new libexporttext();
	$limport = new libimporttext();
	$libSubject = new subject();
	$libSCM = new subject_class_mapping();
	$liblocation = new liblocation();
	
	
	### Get Variables
	$TargetFilePath = stripslashes($_REQUEST['TargetFilePath']);
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	$ObjTimetable = new Timetable($TimetableID);
	$AcademicYearID = $ObjTimetable->AcademicYearID;
	$YearTermID = $ObjTimetable->YearTermID;
	
		
	### Include js libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	
	### Get Data from the csv file
	$ColumnTitleArr = $ObjTimetable->Get_Import_Lesson_Column_Title($ParLang='En', $TargetPropertyArr=array(1));	// Required Column Only
	$ColumnPropertyArr = $ObjTimetable->Get_Import_Lesson_Column_Property();
	$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $ColumnTitleArr, $ColumnPropertyArr);
	$col_name = array_shift($data);
	$numOfLesson = count($data);
	
	
	$SuccessArr = array();
	$SuccessArr['Delete_Old_Temp_Records'] = $ObjTimetable->Delete_Import_Temp_Data();
	$SuccessArr['Copy_Timetable_Data_To_Temp_Table'] = $ObjTimetable->Copy_Lesson_To_Temp_Data_Table();
	
	
	### Get Time Slot Info of the Timetable
	$TimeSlotInfoArr = $ObjTimetable->Get_TimeSlot_List($ReturnAsso=0);
	
	### Get Subject Group Info
	// $SubjectGroupCodeIDMappingArr[$SubjectGroupCode] = $SubjectGroupID
	$SubjectGroupCodeIDMappingArr = $libSCM->Get_Subject_Group_ID_Code_Mapping($YearTermID);
	
	### Get Location Info
	// $LocationCodeIDMappingArr[$BuildingCode][$FloorCode][$RoomCode] = RoomID
	$LocationCodeIDMappingArr = $liblocation->Get_Location_ID_Code_Mapping();
	
	
	$errorCount = 0;
	$successCount = 0;
	$RowErrorRemarksArr = array();
	$numOfData = count($data);
	$InsertTempDataArr = array();
	for ($i=0; $i<$numOfData; $i++)
	{
		$thisErrorMsgArr = array();
		
		$thisRowNumber			= $i + 3;
		$thisDay 				= trim($data[$i][0]);
		$thisTimeSlot 			= trim($data[$i][1]);
		$thisSubjectGroupCode 	= trim($data[$i][2]);
		$thisBuildingCode 		= trim($data[$i][3]);
		$thisFloorCode 			= trim($data[$i][4]);
		$thisRoomCode 			= trim($data[$i][5]);
		$thisOthersSite 		= trim($data[$i][6]);
		
		### Check Day
		if ($thisDay > $ObjTimetable->CycleDays)
			$thisErrorMsgArr[] = $Lang['SysMgr']['Timetable']['Warning']['Import']['NoDay'];
		
		### Check and Get TimeSlotID
		$thisTimeSlotID = $TimeSlotInfoArr[$thisTimeSlot-1]['TimeSlotID'];	// TimeSlot in csv starts from 1 whereas the TimeSlot array index starts from 0
		if ($thisTimeSlotID == '')
			$thisErrorMsgArr[] = $Lang['SysMgr']['Timetable']['Warning']['Import']['NoTimeSlot'];
			
		### Check and Get SubjectGroupID
		$thisSubjectGroupID = $SubjectGroupCodeIDMappingArr[$thisSubjectGroupCode];
		if ($thisSubjectGroupID == '')
			$thisErrorMsgArr[] = $Lang['SysMgr']['Timetable']['Warning']['Import']['NoSubjectGroupFound'];
		
		### Check and Get LocationID
		$thisLocationID = $LocationCodeIDMappingArr[$thisBuildingCode][$thisFloorCode][$thisRoomCode];
		if ($thisLocationID == '' && $thisBuildingCode != '' && $thisFloorCode != '' && $thisRoomCode != '')
			$thisErrorMsgArr[] = $Lang['SysMgr']['Timetable']['Warning']['Import']['NoLocationFound'];
			
		if (count($thisErrorMsgArr) == 0)
		{
			if ($thisLocationID != '')
			{
				### Check if the Room is available for booking
				if ($ObjTimetable->Is_Available_Room($thisTimeSlotID, $thisDay, $thisLocationID, '', $CheckTemp=1) == false)
					$thisErrorMsgArr[] = $Lang['SysMgr']['Timetable']['Warning']['Import']['LocationOccupiedAlready'];
			}
			
			# Check if Subject Group has been allocated already or not
			if ($ObjTimetable->Is_Subject_Group_Overlapped($thisTimeSlotID, $thisDay, $thisSubjectGroupID, '', $CheckTemp=1) == true)
				$thisErrorMsgArr[] = $Lang['SysMgr']['Timetable']['Warning']['Import']['SubjectGroupAllocatedAlready'];
			
			# Check if Student have joined other allocated Subject Group or not
			if ($ObjTimetable->Has_User_Joined_Others_Subject_Group($thisTimeSlotID, $thisDay, $thisSubjectGroupID, '', $CheckTemp=1) == true)
				$thisErrorMsgArr[] = $Lang['SysMgr']['Timetable']['Warning']['Import']['StudentConflict'];
			
			### Insert to the temp timtable table for validating other csv data
			if (count($thisErrorMsgArr) == 0)
			{
				$sql = "Insert Into INTRANET_TIMETABLE_ROOM_ALLOCATION_TEMP
							(TimetableID, TimeSlotID, Day, LocationID, OthersLocation, SubjectGroupID, ImportUserID, DateInput)
						Values
							('$TimetableID', '$thisTimeSlotID', '$thisDay', '$thisLocationID', '".$libSCM->Get_Safe_Sql_Query($thisOthersSite)."', '$thisSubjectGroupID', '".$_SESSION['UserID']."', now())
						";
				$SuccessArr['Insert_Temp_Timetable_Lesson_Records'][$thisRowNumber] = $libSCM->db_db_query($sql);
			}
		}
		
		
		### Prepare sql to insert to the temp csv data table
		$InsertTempDataArr[$i] = " (	'".$_SESSION['UserID']."', '$TimetableID', '$thisRowNumber', '$thisDay', '$thisTimeSlot', '$thisTimeSlotID', '".$libSCM->Get_Safe_Sql_Query($thisSubjectGroupCode)."', '$thisSubjectGroupID',
										'".$libSCM->Get_Safe_Sql_Query($thisBuildingCode)."', '".$libSCM->Get_Safe_Sql_Query($thisFloorCode)."', '".$libSCM->Get_Safe_Sql_Query($thisRoomCode)."', '$thisLocationID',
										'".$libSCM->Get_Safe_Sql_Query($thisOthersSite)."', now()
									) ";
					
		### Record the error messages				
		if(count($thisErrorMsgArr) == 0)
			$successCount++;
		else
			$RowErrorRemarksArr[$thisRowNumber] = $thisErrorMsgArr;
		
		
		### Update Processing Display
		Flush_Screen_Display(1);
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= '$("span#BlockUISpan", window.parent.document).html("'.($i + 1).'");';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}
	$errorCount = count($RowErrorRemarksArr);
	
	
	
	### Insert Data in Temp Table
	$InsertTempDataValue = implode(',', $InsertTempDataArr);
	$sql = "Insert Into TEMP_TIMETABLE_LESSON_IMPORT
				(UserID, TimetableID, RowNumber, Day, TimeSlot, TimeSlotID, SubjectGroupCode, SubjectGroupID, BuildingCode, FloorCode, RoomCode, LocationID, OthersLocation, DateInput)
			Values
				$InsertTempDataValue
			";
	$SuccessArr['Insert_Temp_CSV_Records'] = $libSCM->db_db_query($sql);
	
	
	### Display Record Error Table
	$ErrorCountDisplay = ($errorCount ? "<font color=\"red\"> ": "") . $errorCount . ($errorCount ? "</font>" : "");
	
	$ErrorTable = '';
	if($errorCount > 0)
	{
		$ErrorTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
		
		$ErrorTable .= "<tr>";
			$ErrorTable .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">".$Lang['General']['ImportArr']['Row']."</td>";
			$ColumnTitleDisplayArr = $ObjTimetable->Get_Import_Lesson_Column_Title($ParLang='', $TargetPropertyArr=array(1));	// Required Column Only
			$numOfColumn = count($ColumnTitleDisplayArr);
			for ($i=0; $i<$numOfColumn; $i++)
				$ErrorTable .= "<td class=\"tablebluetop tabletopnolink\">". $ColumnTitleDisplayArr[$i] ."</td>";
			$ErrorTable .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['General']['Remark'] ."</td>";
		$ErrorTable .= "</tr>";
		
		$ErrorRowNumArr = array_keys($RowErrorRemarksArr);
		$ErrorRowNumList = implode(',', $ErrorRowNumArr);
		$sql = "select * from TEMP_TIMETABLE_LESSON_IMPORT where UserID = '".$_SESSION['UserID']."' And TimetableID = '$TimetableID' And RowNumber In ($ErrorRowNumList) ";
		$ErrorRowInfoArr = $libSCM->returnArray($sql);
		
		for ($i=0; $i<$errorCount; $i++)
		{
			$thisTempID 			= $ErrorRowInfoArr[$i]['TempID'];
			$thisRowNumber 			= $ErrorRowInfoArr[$i]['RowNumber'];
			$thisDay 				= $ErrorRowInfoArr[$i]['Day'];
			$thisTimeSlot 			= $ErrorRowInfoArr[$i]['TimeSlot'];
			$thisSubjectGroupCode 	= $ErrorRowInfoArr[$i]['SubjectGroupCode'];
			$thisBuildingCode 		= $ErrorRowInfoArr[$i]['BuildingCode'];
			$thisFloorCode 			= $ErrorRowInfoArr[$i]['FloorCode'];
			$thisRoomCode 			= $ErrorRowInfoArr[$i]['RoomCode'];
			$thisOthersLocation 	= $ErrorRowInfoArr[$i]['OthersLocation'];
			
			$thisDay = $thisDay ? $thisDay : '<font color="red">***</font>';
			$thisTimeSlot = $thisTimeSlot ? $thisTimeSlot : '<font color="red">***</font>';
			$thisSubjectGroupCode = $thisSubjectGroupCode ? $thisSubjectGroupCode : '<font color="red">***</font>';
			if ($thisBuildingCode == '' && $thisFloorCode == '' && $thisRoomCode == '')
			{
				$thisOthersLocation = $thisOthersLocation ? $thisOthersLocation : '<font color="red">***</font>';
			}
			else
			{
				$thisBuildingCode = $thisBuildingCode ? $thisBuildingCode : '<font color="red">***</font>';
				$thisFloorCode = $thisFloorCode ? $thisFloorCode : '<font color="red">***</font>';
				$thisRoomCode = $thisRoomCode ? $thisRoomCode : '<font color="red">***</font>';
				
				$thisOthersLocation = $thisOthersLocation ? $thisOthersLocation : "&nbsp;";
			}
			
			
			$thisErrorRemarksArr = $RowErrorRemarksArr[$thisRowNumber];
			
			### Check if the field need to display in red color
			if (in_array($Lang['SysMgr']['Timetable']['Warning']['Import']['NoDay'], $thisErrorRemarksArr))
				$thisDay = '<font color="red">'.$thisDay.'</font>';
				
			if (in_array($Lang['SysMgr']['Timetable']['Warning']['Import']['NoTimeSlot'], $thisErrorRemarksArr))
				$thisTimeSlot = '<font color="red">'.$thisTimeSlot.'</font>';
				
			if (in_array($Lang['SysMgr']['Timetable']['Warning']['Import']['NoSubjectGroupFound'], $thisErrorRemarksArr))
				$thisSubjectGroupCode = '<font color="red">'.$thisSubjectGroupCode.'</font>';
				
			if (in_array($Lang['SysMgr']['Timetable']['Warning']['Import']['SubjectGroupAllocatedAlready'], $thisErrorRemarksArr))
				$thisSubjectGroupCode = '<font color="red">'.$thisSubjectGroupCode.'</font>';
				
			if (in_array($Lang['SysMgr']['Timetable']['Warning']['Import']['StudentConflict'], $thisErrorRemarksArr))
				$thisSubjectGroupCode = '<font color="red">'.$thisSubjectGroupCode.'</font>';
				
			if (in_array($Lang['SysMgr']['Timetable']['Warning']['Import']['NoLocationFound'], $thisErrorRemarksArr))
			{
				$thisBuildingCode = '<font color="red">'.$thisBuildingCode.'</font>';
				$thisFloorCode = '<font color="red">'.$thisFloorCode.'</font>';
				$thisRoomCode = '<font color="red">'.$thisRoomCode.'</font>';
			}
			
			if (in_array($Lang['SysMgr']['Timetable']['Warning']['Import']['LocationOccupiedAlready'], $thisErrorRemarksArr))
			{
				$thisBuildingCode = '<font color="red">'.$thisBuildingCode.'</font>';
				$thisFloorCode = '<font color="red">'.$thisFloorCode.'</font>';
				$thisRoomCode = '<font color="red">'.$thisRoomCode.'</font>';
			}
			
			
			### Error Message Display
			$thisErrorDisplay = implode('<br />', $thisErrorRemarksArr);
			
			$css_i = ($i % 2) ? "2" : "";
			$ErrorTable .= '<tr style="vertical-align:top">';
				$ErrorTable .= '<td class="tablebluerow'.$css_i.'" width="10">'. $thisRowNumber .'</td>';
				$ErrorTable .= '<td class="tablebluerow'.$css_i.'">'. $thisDay .'</td>';
				$ErrorTable .= '<td class="tablebluerow'.$css_i.'">'. $thisTimeSlot .'</td>';
				$ErrorTable .= '<td class="tablebluerow'.$css_i.'">'. $thisSubjectGroupCode .'</td>';
				$ErrorTable .= '<td class="tablebluerow'.$css_i.'">'. $thisBuildingCode .'</td>';
				$ErrorTable .= '<td class="tablebluerow'.$css_i.'">'. $thisFloorCode .'</td>';
				$ErrorTable .= '<td class="tablebluerow'.$css_i.'">'. $thisRoomCode .'</td>';
				$ErrorTable .= '<td class="tablebluerow'.$css_i.'">'. $thisOthersLocation .'</td>';
				$ErrorTable .= '<td class="tablebluerow'.$css_i.'">'. $thisErrorDisplay .'</td>';
			$ErrorTable .= '</tr>';
		}
		$ErrorTable .= "</table>";
	}
	
	
	$ErrorCountDisplay = ($errorCount > 0) ? "<font color=\"red\">".$errorCount."</font>" : $errorCount;
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$ErrorTable.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$successCount.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$ErrorCountDisplay.'\';';
		
		if ($errorCount == 0) 
		{
			$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton");';
			$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).attr("disabled","");';
		}
		
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}


echo $ReturnString;


intranet_closedb();
?>