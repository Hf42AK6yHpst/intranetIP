<?
// Modifying by: 

############# Change Log [Start] ################
#
# Date: 2020-05-07 Tommy
#       - modified access checking, added $plugin["Disable_Timetable"]
#
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");

intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]) || $plugin["Disable_Timetable"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$TimetableID = $_POST['TimetableID'];
$ObjTimetable = new Timetable($TimetableID);
$AcademicYearID = $ObjTimetable->AcademicYearID;
$YearTermID = $ObjTimetable->YearTermID;

### Check csv file format
$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import_lesson_step1.php?TimetableID=".$TimetableID."&ReturnMsg=WrongFileFormat");
	exit();
}

### move to temp folder first for others validation
$folder_prefix = $intranet_root."/file/import_temp/timetable/lesson";
if (!file_exists($folder_prefix))
	$lo->folder_new($folder_prefix);

$TargetFileName = date('Ymd_His').'_'.$_SESSION['UserID'].$ext;
$TargetFilePath = stripslashes($folder_prefix."/".$TargetFileName);
$SuccessArr['MoveCsvFileToTempFolder'] = $lo->lfs_move($csvfile, $TargetFilePath);



### Title / Menu
$CurrentPageArr['Timetable'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['Timetable']['ModuleTitle'];

$libtimetable_ui = new libtimetable_ui();
$TAGS_OBJ = $libtimetable_ui->Get_Tag_Array($CurrenrTag=1);

# navaigtion
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Timetable']['MangeTimetable'], "javascript:js_Cancel();");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Timetable']['Button']['ImportLesson'], "");
			
$linterface = new interface_html();
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 


### List out the import result
# Get Timetable's Academic Year and Term Info
$TimetableName = $ObjTimetable->Get_Timetable_Name();

$AcademicYearID = $ObjTimetable->AcademicYearID;
$ObjAcademicYear = new academic_year($AcademicYearID);
$AcademicYearName = $ObjAcademicYear->Get_Academic_Year_Name();

$YearTermID = $ObjTimetable->YearTermID;
$ObjYearTerm = new academic_year_term($YearTermID);
$YearTermName = $ObjYearTerm->Get_Year_Term_Name();


### Get Data from the csv file
$ColumnTitleArr = $ObjTimetable->Get_Import_Lesson_Column_Title($ParLang='En', $TargetPropertyArr=array(1));	// Required Column Only
$ColumnPropertyArr = $ObjTimetable->Get_Import_Lesson_Column_Property();
$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $ColumnTitleArr, $ColumnPropertyArr);
$col_name = array_shift($data);
$numOfLesson = count($data);


### Top Info Table
$TimetableInfoTable = '';
$TimetableInfoTable .= '<table class="form_table_v30">'."\n";
	$TimetableInfoTable .= '<tr>'."\n";
		$TimetableInfoTable .= '<td class="field_title">'.$Lang['SysMgr']['FormClassMapping']['SchoolYear'].'</td>'."\n";
		$TimetableInfoTable .= '<td>'.$AcademicYearName.'</td>'."\n";
	$TimetableInfoTable .= '</tr>'."\n";
	$TimetableInfoTable .= '<tr>'."\n";
		$TimetableInfoTable .= '<td class="field_title">'.$Lang['SysMgr']['Timetable']['Term'].'</td>'."\n";
		$TimetableInfoTable .= '<td>'.$YearTermName.'</td>'."\n";
	$TimetableInfoTable .= '</tr>'."\n";
	$TimetableInfoTable .= '<tr>'."\n";
		$TimetableInfoTable .= '<td class="field_title">'.$Lang['SysMgr']['Timetable']['Timetable'].'</td>'."\n";
		$TimetableInfoTable .= '<td>'.$TimetableName.'</td>'."\n";
	$TimetableInfoTable .= '</tr>'."\n";
	$TimetableInfoTable .= '<tr>'."\n";
		$TimetableInfoTable .= '<td class="field_title">'.$Lang['General']['SuccessfulRecord'].'</td>'."\n";
		$TimetableInfoTable .= '<td><div id="SuccessCountDiv"></div></td>'."\n";
	$TimetableInfoTable .= '</tr>'."\n";
	$TimetableInfoTable .= '<tr>'."\n";
		$TimetableInfoTable .= '<td class="field_title">'.$Lang['General']['FailureRecord'].'</td>'."\n";
		$TimetableInfoTable .= '<td><div id="FailCountDiv"></div></td>'."\n";
	$TimetableInfoTable .= '</tr>'."\n";
$TimetableInfoTable .= '</table>'."\n";


### iFrame for validation
$thisSrc = "ajax_validation.php?RecordType=Import_Lesson&TimetableID=$TimetableID&TargetFilePath=".$TargetFilePath;
$ImportIFrame .= '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;display:none;"></iframe>'."\n";

### Block UI Msg
$ProcessingMsg = str_replace('<!--NumOfRecords-->', '<span id="BlockUISpan">0</span> / '.$numOfLesson, $Lang['SysMgr']['Timetable']['RecordsValidated']);


### Buttons
$import_buttons = '';
$import_buttons .= '<div class="edit_bottom_v30">'."\n";
	$import_buttons .= $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "js_Go_Import();", 'ImportBtn', '', $Disabled=1);
	$import_buttons .= " &nbsp;";
	$import_buttons .= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Back();");
	$import_buttons .= " &nbsp;";
	$import_buttons .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Cancel();");
$import_buttons .= '</div>'."\n";

?>

<script language="javascript">

$(document).ready(function () {
	Block_Document('<?=$ProcessingMsg?>');
});

function js_Go_Back()
{
	var jsTimetableID = $('input#TimetableID').val();
	window.location = 'import_lesson_step1.php?TimetableID=' + jsTimetableID;
}

function js_Cancel()
{
	window.location = 'index.php';
}

function js_Go_Import()
{
	var jsObjForm = document.getElementById('form1');
	jsObjForm.action = "import_lesson_step3.php";
	jsObjForm.submit();
}

</script>

<br />
<form id="form1" name="form1" method="post">
	<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
	<?= $linterface->GET_IMPORT_STEPS($CurrStep=2) ?>
	<div class="table_board">
		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td align="center">
					<?=$TimetableInfoTable?>
					<br style="clear:both;" />
					<div id="ErrorTableDiv"></div>
				</td>
			</tr>
		</table>
	</div>
			
	<?=$import_buttons?>
	<?=$ImportIFrame?>
	
	<input type="hidden" id="TimetableID" name="TimetableID" value="<?=$TimetableID?>">
</form>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>