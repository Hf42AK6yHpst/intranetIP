<?php
// using:
/*
 *  2020-11-06 Cameron
 *      - add Action == CheckForDelete to check if the time table has been used or not [case #M199265]
 *
 *  2019-06-13 Cameron
 *      - should reject Booking for Action = Delete_TimeSlot [case #M141869]
 *
 *  2019-06-12 Cameron
 *      - should also update timeslot info in eBooking and icalendar event for Action = Edit_TimeSlot [case #M141869]   
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");

intranet_opendb();

# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == "Add")
{
	$DataArr['AcademicYearID'] = stripslashes($_REQUEST['AcademicYearID']);
	$DataArr['YearTermID'] = stripslashes($_REQUEST['YearTermID']);
	$DataArr['TimetableName'] = stripslashes(urldecode($_REQUEST['TimetableName']));
	$DataArr['ReleaseStatus'] = stripslashes($_REQUEST['releaseStatus']);
	
	
	$libTimetable = new Timetable();
	# return inserted TimetableID
	$insertedTimetableID = $libTimetable->Insert_Record($DataArr);
	$successArr['Insert_Timetable'] = $insertedTimetableID;
	
	$isCopy = stripslashes($_REQUEST['isCopy']);
	$TargetTimetableID = stripslashes($_REQUEST['TargetTimetableID']);
	
	if ($isCopy) {
		include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
		$successArr['Copy_Timetable'] = $libTimetable->Copy_Timetable($TargetTimetableID, $insertedTimetableID);
	}
	
	$success = $insertedTimetableID;
}
else if ($Action == "Edit")
{
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	$TimetableName = stripslashes(urldecode($_REQUEST['TimetableName']));
	$ReleaseStatus = stripslashes($_REQUEST['releaseStatus']);
	$isCopy = stripslashes($_REQUEST['isCopy']);
	$TargetTimetableID = stripslashes($_REQUEST['TargetTimetableID']);
	
	$DataArr['TimetableName'] = $TimetableName;
	$DataArr['ReleaseStatus'] = $ReleaseStatus;
	
	$objTimetable = new Timetable($TimetableID);
	$successArr['Update_Timetable_Title'] = $objTimetable->Update_Record($DataArr);
	
	if ($isCopy)
	{
		# Delete Old Data First
		$cycleDays = $objTimetable->CycleDays;
		
		# Delete all room Allocations
		for ($i=$cycleDays; $i>0; $i--)
		{
			$successArr['Delete_Room_Allocation_'.$i] = $objTimetable->Delete_Room_Allocation_Record($TimeSlotID='', $i);
		}
		
		# Delete all timeslots
		$successArr['Delete_Time_Slot'] = $objTimetable->Delete_All_Time_Slot();
		
		# Copy Timetable
		include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
		$successArr['Copy_Timetable'] = $objTimetable->Copy_Timetable($TargetTimetableID, $TimetableID);
	}
	
	if (in_array(0, $successArr))
		$success = 0;
	else
		$success = 1;
}
else if ($Action == "Delete")
{
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	
	$objTimetable = new Timetable($TimetableID);
	$success = $objTimetable->Delete_Record();
}
else if ($Action == "Add_CycleDays")
{
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	
	$objTimetable = new Timetable($TimetableID);
	$targetCycleDays = $objTimetable->CycleDays + 1;
	
	$DataArr['CycleDays'] = $targetCycleDays;
	$success = $objTimetable->Update_Record($DataArr);
}
else if ($Action == "Delete_CycleDays")
{
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	$Day = stripslashes($_REQUEST['Day']);
	
	# minus one in number of cycle days of the timetable
	$objTimetable = new Timetable($TimetableID);
	$targetCycleDays = $objTimetable->CycleDays - 1;
	
	$DataArr['CycleDays'] = $targetCycleDays;
	$successArr['Update_CycleDays'] = $objTimetable->Update_Record($DataArr);
	
	# delete and shift relative room allocation records
	$successArr['Delete_RoomAllocation'] = $objTimetable->Delete_Room_Allocation_Record('', $Day);
	
	if (in_array(0, $successArr))
		$success = 0;
	else
		$success = 1;
}
else if ($Action == "Add_TimeSlot")
{
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	$TimeSlotTitle = stripslashes(urldecode($_REQUEST['TimeSlotTitle']));
	$StartTimeHour = stripslashes($_REQUEST['StartTimeHour']);
	$StartTimeMin = stripslashes($_REQUEST['StartTimeMin']);
	$EndTimeHour = stripslashes($_REQUEST['EndTimeHour']);
	$EndTimeMin = stripslashes($_REQUEST['EndTimeMin']);
	$LastTimeSlotID = stripslashes($_REQUEST['LastTimeSlotID']);
	$NextTimeSlotID = stripslashes($_REQUEST['NextTimeSlotID']);
	$isFirstPmSession = standardizeFormPostValue($_REQUEST['isFirstPmSession']);
	
	$DataArr['TimeSlotName'] = $TimeSlotTitle;
	$DataArr['StartTime'] = $StartTimeHour.":".$StartTimeMin.":00";
	$DataArr['EndTime'] = $EndTimeHour.":".$EndTimeMin.":00";
	
	/*
	# Get Display Order
	if ($NextTimeSlotID == '')
	{
		// add the time slot to the bottom
		$libTimeSlot = new TimeSlot();
		$DataArr['DisplayOrder'] = $libTimeSlot->Get_Max_Display_Order() + 1;
	}
	else
	{
		// add the time slot in between two time slots
		$objTimeSlot = new TimeSlot($NextTimeSlotID);
		$DataArr['DisplayOrder'] = $objTimeSlot->DisplayOrder;
	}
	*/
	
	$libTimeSlot = new TimeSlot();
	$success = $libTimeSlot->Insert_Record($DataArr, $TimetableID, $isFirstPmSession);
}
else if ($Action == "Edit_TimeSlot")
{
	$TimeSlotID = stripslashes($_REQUEST['TimeSlotID']);
	$TimeSlotTitle = stripslashes(urldecode($_REQUEST['TimeSlotTitle']));
	$StartTimeHour = stripslashes($_REQUEST['StartTimeHour']);
	$StartTimeMin = stripslashes($_REQUEST['StartTimeMin']);
	$EndTimeHour = stripslashes($_REQUEST['EndTimeHour']);
	$EndTimeMin = stripslashes($_REQUEST['EndTimeMin']);
	$isFirstPmSession = standardizeFormPostValue($_REQUEST['isFirstPmSession']);
	
	$DataArr['TimeSlotName'] = $TimeSlotTitle;
	$DataArr['StartTime'] = $StartTimeHour.":".$StartTimeMin.":00";
	$DataArr['EndTime'] = $EndTimeHour.":".$EndTimeMin.":00";
	
	$objTimeSlot = new TimeSlot($TimeSlotID);
	$oldStartTime = $objTimeSlot->StartTime;
	$oldEndTime = $objTimeSlot->EndTime;
	$successArr['UpdateRecord'] = $objTimeSlot->Update_Record($DataArr, $isFirstPmSession);
	
	if ($sys_custom['eBooking']['EverydayTimetable']) {
	    $newStartTime = substr('0'.$StartTimeHour,-2).':'.substr('0'.$StartTimeMin,-2).':00';
	    $newEndTime = substr('0'.$EndTimeHour,-2).':'.substr('0'.$EndTimeMin,-2).':00';
	    if ($newStartTime != $oldStartTime || $newEndTime != $oldEndTime) {
	        include_once($intranet_root."/includes/libebooking.php");
	        $libebooking = new libebooking();
	        $successArr['UpdateBookingTimeInfo'] = $libebooking->updateBookingTimeInfo($TimeSlotID, $newStartTime, $newEndTime);
	    }
	}
	$success = in_array(false,$successArr) ? false : true;
}
else if ($Action == "Delete_TimeSlot")
{
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	$TimeSlotID = stripslashes($_REQUEST['TimeSlotID']);
	
	# delete relative room allocation records first
	$objTimetable = new Timetable($TimetableID);
	$successArr['Delete_RoomAllocation'] = $objTimetable->Delete_Room_Allocation_Record($TimeSlotID);
	
	if ($successArr['Delete_RoomAllocation'])
	{
		$objTimeSlot = new TimeSlot($TimeSlotID);
		$successArr['Delete_TimeSlot'] = $objTimeSlot->Delete_Record();
		
		if ($sys_custom['eBooking']['EverydayTimetable']) {
		    include_once($intranet_root."/includes/libebooking.php");
		    $libebooking = new libebooking();
		    $successArr['rejectBookingByTimeSlot'] = $libebooking->rejectBookingByTimeSlot($TimeSlotID);
		}
	}
	
	if (in_array(0, $successArr))
		$success = 0;
	else
		$success = 1;
	
}
else if ($Action == "Add_RoomAllocation")
{
	$ContinuousTimeSlot = $_REQUEST['ContinuousTimeSlot'];
	
	$DataArr['TimetableID'] = stripslashes($_REQUEST['TimetableID']);
	$DataArr['TimeSlotID'] = stripslashes($_REQUEST['TimeSlotID']);
	$DataArr['Day'] = stripslashes($_REQUEST['Day']);
	$DataArr['LocationID'] = stripslashes($_REQUEST['LocationID']);
	$DataArr['OthersLocation'] = stripslashes(urldecode($_REQUEST['OthersLocation']));
	$DataArr['SubjectGroupID'] = stripslashes($_REQUEST['SubjectGroupID']);
	
	$BuildingID = stripslashes($_REQUEST['BuildingID']);
	if ($BuildingID == -1)	// -1 means use Others Location
		$DataArr['LocationID'] = '';
	else
		$DataArr['OthersLocation'] = '';
	
	### Get the target timeslot list	
	$objTimetable = new Timetable($DataArr['TimetableID']);
	$timeSlotInfoAry = $objTimetable->Get_TimeSlot_List();
	$timeSlotIdAry = Get_Array_By_Key($timeSlotInfoAry, 'TimeSlotID');
	unset($timeSlotInfoAry);
	$startKey = array_search($DataArr['TimeSlotID'], $timeSlotIdAry);
	$targetTimeSlotIdAry = array();
	if ($startKey !== null && $startKey !== false) {
		for ($i=$startKey; $i<($startKey + $ContinuousTimeSlot); $i++) {
			$targetTimeSlotIdAry[] = $timeSlotIdAry[$i];
		}
	}
	$targetTimeSlotIdList = implode(',', $targetTimeSlotIdAry);
	
	### Add lesson
	$libRoomAllocation = new Room_Allocation();
	$successAry = array();
	for ($i=0; $i<$ContinuousTimeSlot; $i++) {
		$DataArr['TimeSlotID'] = $targetTimeSlotIdAry[$i];
		$_insertedId = $libRoomAllocation->Insert_Record($DataArr);
		
		if ($ContinuousTimeSlot > 1 && $i == 0) {
			$DataArr['RelatedRoomAllocationID'] = $_insertedId;
			
			$_objRoomAllocation = new Room_Allocation($_insertedId);
			$_objRoomAllocation->Update_Record(array('RelatedRoomAllocationID' => $_insertedId));
		}
		
		$successAry[$i] = ($_insertedId=='')? false : true;
	}
	
	$successStatus = (in_array(false, $successAry))? '0' : '1';
	$success = $targetTimeSlotIdList.'<!--separator-->'.$successStatus;
}
else if ($Action == "Edit_RoomAllocation")
{
//	$DataArr['RoomAllocationID'] = stripslashes($_REQUEST['RoomAllocationID']);
//	$DataArr['LocationID'] = stripslashes($_REQUEST['LocationID']);
//	$DataArr['OthersLocation'] = stripslashes(urldecode($_REQUEST['OthersLocation']));
//	$DataArr['SubjectGroupID'] = stripslashes($_REQUEST['SubjectGroupID']);
//	
//	$BuildingID = stripslashes($_REQUEST['BuildingID']);
//	if ($BuildingID == -1)	// -1 means use Others Location
//		$DataArr['LocationID'] = '';
//	else
//		$DataArr['OthersLocation'] = '';
//	
//	$objRoomAllocation = new Room_Allocation($RoomAllocationID);
//	$success = $objRoomAllocation->Update_Record($DataArr);

	$RoomAllocationID = stripslashes($_REQUEST['RoomAllocationID']);
	$DataArr['LocationID'] = stripslashes($_REQUEST['LocationID']);
	$DataArr['OthersLocation'] = stripslashes(urldecode($_REQUEST['OthersLocation']));
	$DataArr['SubjectGroupID'] = stripslashes($_REQUEST['SubjectGroupID']);
	
	$BuildingID = stripslashes($_REQUEST['BuildingID']);
	if ($BuildingID == -1)	// -1 means use Others Location
		$DataArr['LocationID'] = '';
	else
		$DataArr['OthersLocation'] = '';
	
	### Get all related lesson
	$objRoomAllocation = new Room_Allocation($RoomAllocationID);
	$relatedRoomAllocationInfoAry = $objRoomAllocation->Get_Related_Room_Allocation_Info();
	$numOfRelatedRoomAllocation = count($relatedRoomAllocationInfoAry);
	
	$successAry = array();
	for ($i=0; $i<$numOfRelatedRoomAllocation; $i++) {
		$_roomAllocationId = $relatedRoomAllocationInfoAry[$i]['RoomAllocationID'];
		
		$_objRoomAllocation = new Room_Allocation($_roomAllocationId);
		$successAry[$i] = $_objRoomAllocation->Update_Record($DataArr);
	}	
		
	$successStatus = (in_array(false, $successAry))? '0' : '1';
	$targetTimeSlotIdList = implode(',', array_values(array_unique(Get_Array_By_Key($relatedRoomAllocationInfoAry, 'TimeSlotID'))));
	$success = $targetTimeSlotIdList.'<!--separator-->'.$successStatus;
}
else if ($Action == "Delete_RoomAllocation")
{
	$RoomAllocationID = stripslashes($_REQUEST['RoomAllocationID']);
	
	if ($RoomAllocationID == '')
	{
		// Delete the lessons in a specific day in a specific timeslot according to the filtering
		$TimeSlotID = stripslashes($_REQUEST['TimeSlotID']);
		$Day = stripslashes($_REQUEST['Day']);
		
		## Get Selection Info
		$TimetableID = stripslashes($_REQUEST['TimetableID']);
		$IsViewMode = stripslashes($_REQUEST['IsViewMode']);
		$ViewMode = stripslashes($_REQUEST['ViewMode']);
		$isPrintMode = stripslashes($_REQUEST['IsPrint']);
		
		# Class
		$YearClassID = stripslashes($_REQUEST['YearClassID']);
		$YearClassIDArr = explode(",", $YearClassID);
		# Subject Group
		$SubjectGroupID = stripslashes($_REQUEST['SubjectGroupID']);
		$SubjectGroupIDArr = explode(",", $SubjectGroupID);
		# Personal
		$Identity = stripslashes($_REQUEST['Identity']);
		$SelectedUserID = stripslashes($_REQUEST['SelectedUserID']);
		$UserIDArr = explode(",", $SelectedUserID);
		# Room
		$BuildingID = stripslashes($_REQUEST['BuildingID']);
		$OthersLocation = trim(stripslashes(urldecode($_REQUEST['OthersLocation'])));
		$LocationID = stripslashes($_REQUEST['LocationID']);
		$LocationIDArr = explode(",",$LocationID);
		
		## Get Timetable Term
		$objTimetable = new Timetable($TimetableID);
		
		## Build Filtering Array Here
		$SubjectGroupID_DisplayFilterArr = array();
		$LocationID_DisplayFilterArr = array();
		
		$FilterArr = $objTimetable->Get_Filter_Array_By_Filter_Settings($ViewMode, $YearClassIDArr, $SubjectGroupIDArr, $Identity, $UserIDArr, $BuildingID, $LocationIDArr, $OthersLocation);
		$SubjectGroupID_DisplayFilterArr = $FilterArr['SubjectGroupIDArr'];
		$LocationID_DisplayFilterArr = $FilterArr['LocationIDArr'];
		
		$RoomAllocationArr = $objTimetable->Get_Room_Allocation_List($TimeSlotID, $Day, $SubjectGroupID_DisplayFilterArr, $LocationID_DisplayFilterArr, $OthersLocation, $DayAsFirstKey=0, $CheckTemp=0, $returnList=true);
		$RelatedRoomAllocationIDArr = Get_Array_By_Key($RoomAllocationArr, 'RelatedRoomAllocationID');
		$RelatedRoomAllocationArr = $objTimetable->Get_Room_Allocation_List('', '', array(), '', '', $DayAsFirstKey=0, $CheckTemp=0, $returnList=true, $RelatedRoomAllocationIDArr);
		$TimeSlotIDArr1 = Get_Array_By_Key($RoomAllocationArr, 'TimeSlotID');
		$TimeSlotIDArr2 = Get_Array_By_Key($RelatedRoomAllocationArr, 'TimeSlotID');
		$targetTimeSlotIdList = implode(',', array_values(array_unique(array_merge($TimeSlotIDArr1, $TimeSlotIDArr2))));
		
		$successStatus = $objTimetable->Delete_Room_Allocation_Record($TimeSlotID, $Day, $ReArrangeDay=0, $SubjectGroupID_DisplayFilterArr, $LocationID_DisplayFilterArr, $OthersLocation);
	}
	else
	{
		### Get all related lesson
		$objRoomAllocation = new Room_Allocation($RoomAllocationID);
		$relatedRoomAllocationInfoAry = $objRoomAllocation->Get_Related_Room_Allocation_Info();
		$numOfRelatedRoomAllocation = count($relatedRoomAllocationInfoAry);
		
		$successAry = array();
		for ($i=0; $i<$numOfRelatedRoomAllocation; $i++) {
			$_roomAllocationId = $relatedRoomAllocationInfoAry[$i]['RoomAllocationID'];
			
			$_objRoomAllocation = new Room_Allocation($_roomAllocationId);
			$successAry[$i] = $_objRoomAllocation->Delete_Record($DataArr);
		}	
		
		$successStatus = (in_array(false, $successAry))? '0' : '1';
		$targetTimeSlotIdList = implode(',', array_values(array_unique(Get_Array_By_Key($relatedRoomAllocationInfoAry, 'TimeSlotID'))));
	}
	
	$success = $targetTimeSlotIdList.'<!--separator-->'.$successStatus;
}
else if ($Action == "Import_Lesson")
{
	$TimetableID = $_GET['TimetableID'];
	$ObjTimetable = new Timetable($TimetableID);
	
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	### Get the Temp Data To be Insert
	$sql = "Select * from TEMP_TIMETABLE_LESSON_IMPORT where UserID = '".$_SESSION['UserID']."' And TimetableID = '$TimetableID' order by RowNumber";
	$TempLessonArr = $ObjTimetable->returnArray($sql);
	$numOfLesson = count($TempLessonArr);
	
	$libRoomAllocation = new Room_Allocation();
	$SuccessArr = array();
	for ($i=0; $i<$numOfLesson; $i++)
	{
		$DataArr = array();
		$DataArr['TimetableID'] = $TimetableID;
		$DataArr['Day'] = $TempLessonArr[$i]['Day'];
		$DataArr['TimeSlotID'] = $TempLessonArr[$i]['TimeSlotID'];
		$DataArr['SubjectGroupID'] = $TempLessonArr[$i]['SubjectGroupID'];
		$DataArr['LocationID'] = $TempLessonArr[$i]['LocationID'];
		$DataArr['OthersLocation'] = $TempLessonArr[$i]['OthersLocation'];
		$SuccessArr['TempID'] = $libRoomAllocation->Insert_Record($DataArr);
		
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUISpan").innerHTML = "'.($i + 1).'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("NumOfProcessedPageSpan").innerHTML = "'.($i + 1).'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
	
	$ObjTimetable->Delete_Import_Temp_Data();
}
else if ($Action == 'CheckForDelete') {     // return true if the time table hasn't used
    $TimetableID = IntegerSafe($_POST['TimetableID']);
    $sql = "SELECT
                p.PeriodStart AS StartDate 
            FROM 
                INTRANET_CYCLE_GENERATION_PERIOD p
                INNER JOIN INTRANET_PERIOD_TIMETABLE_RELATION r ON r.PeriodID=p.PeriodID
            WHERE 
                r.TimetableID='".$TimetableID."' LIMIT 1";
    $objTimetable = new Timetable($TimetableID);
    $rs = $objTimetable->returnResultSet($sql);
    if (count($rs)) {
        $success = $rs[0]['StartDate'];
    }
    else {
        $sql = "SELECT 
                    d.SpecialDate AS StartDate 
                FROM 
                    INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE d
                    INNER JOIN INTRANET_TIMEZONE_SPECIAL_TIMETABLE_SETTINGS s ON s.SpecialTimetableSettingsID=d.SpecialTimetableSettingsID 
                WHERE 
                    s.TimetableID='".$TimetableID."' 
                    ORDER BY d.SpecialDate LIMIT 1";
        $rs = $objTimetable->returnResultSet($sql);
        if (count($rs)) {
            $success = $rs[0]['StartDate'];
        }
        else {
            $success = 1;
        }
    }
}
echo $success;


intranet_closedb();
?>