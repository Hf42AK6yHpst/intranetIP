<?
// Modifying by: 

############# Change Log [Start] ################
#
# Date: 2020-05-07 Tommy
#       - modified access checking, added $plugin["Disable_Timetable"]
#
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");

intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]) || $plugin["Disable_Timetable"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$TimetableID = $_POST['TimetableID'];
$ObjTimetable = new Timetable($TimetableID);


### Get the Temp Data To be Insert
$sql = "Select * from TEMP_TIMETABLE_LESSON_IMPORT where UserID = '".$_SESSION['UserID']."' And TimetableID = '$TimetableID' order by RowNumber";
$TempLessonArr = $ObjTimetable->returnArray($sql);
$numOfLesson = count($TempLessonArr);


# step information
### Title / Menu
$CurrentPageArr['Timetable'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['Timetable']['ModuleTitle'];

$libtimetable_ui = new libtimetable_ui();
$TAGS_OBJ = $libtimetable_ui->Get_Tag_Array($CurrenrTag=1);

# navaigtion
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Timetable']['MangeTimetable'], "window.location='index.php'");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Timetable']['Button']['ImportLesson'], "");

$linterface = new interface_html();
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage']));  

### iFrame for real-time update count
$thisSrc = "ajax_update_timetable.php?Action=Import_Lesson&TimetableID=".$TimetableID;
$ImportLessonIFrame = '<iframe id="ImportLessonIFrame" name="ImportLessonIFrame" src="'.$thisSrc.'" style="display:none;"></iframe>'."\n";
					
$ProcessingMsg = str_replace('<!--NumOfRecords-->', '<span id="BlockUISpan">0</span> / '.$numOfLesson, $Lang['SysMgr']['Timetable']['RecordsProcessed']);

?>

<script language="javascript">

$(document).ready(function () {
	//js_Import_Lesson();
	Block_Document('<?=$ProcessingMsg?>');
});

function js_Import_Lesson()
{
	Block_Document('<?=$ProcessingMsg?>');
	$.post(
		"ajax_update_timetable.php", 
		{ 
			Action: "Import_Lesson",
			TimetableID: '<?=$TimetableID?>'
		},
		function(ReturnData)
		{
			//UnBlock_Document();
			//$('#debugArea').html(ReturnData);
		}
	);
}
	
</script>

<br />
<form id="form1" name="form1" method="post">
	<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
	<?= $linterface->GET_IMPORT_STEPS($CurrStep=3) ?>
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="center"><?=$x?></td>
		</tr>
				
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
				
					<tr>
						<td class='tabletext' align='center'>
							<span id="NumOfProcessedPageSpan">0</span> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?>
						</td>
					</tr>
			
					<tr>
						<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
					
					<tr>
						<td align="center">
							<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "window.location='index.php'"); ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<?=$ImportLessonIFrame?>
</form>

<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>