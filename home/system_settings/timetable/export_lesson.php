<?php
// using 

############# Change Log [Start] ################
#
# Date: 2020-05-07 Tommy
#       - modified access checking, added $plugin["Disable_Timetable"]
#
#	Date:	2010-06-21	YatWoon
#			add online help button
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]) || $plugin["Disable_Timetable"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

## Get Selection Info
$TimetableID = ($_POST['SelectedTimetableID']!='')? stripslashes($_POST['SelectedTimetableID']) : stripslashes($_POST['TimetableID']);
$AcademicYearID = stripslashes($_POST['SelectedAcademicYearID']);
$YearTermID = stripslashes($_POST['SelectedYearTermID']);
$ViewMode = stripslashes($_POST['SelectedViewMode']);
$ForImportSample = $_POST['ForImportSample'];


# Class
$YearClassIDArr = $_POST['SelectedYearClassIDArr'];
# Subject Group
$SubjectGroupIDArr = $_POST['SelectedSubjectGroupIDArr'];
# Personal
$Identity = stripslashes($_POST['SelectedIdentity']);
$UserIDArr = $_POST['SelectedUserIDArr'];
# Room
$BuildingID = stripslashes($_POST['SelectedBuildingID']);
$OthersLocation = trim(stripslashes(urldecode($_POST['OthersLocation'])));
$LocationIDArr = $_POST['SelectedLocationIDArr'];


### Get Lesson Info
$ObjTimetable = new Timetable($TimetableID);
$maxCycleDays = $ObjTimetable->CycleDays;
$FilterArr = $ObjTimetable->Get_Filter_Array_By_Filter_Settings($ViewMode, $YearClassIDArr, $SubjectGroupIDArr, $Identity, $UserIDArr, $BuildingID, $LocationIDArr, $OthersLocation);
$SubjectGroupID_DisplayFilterArr = $FilterArr['SubjectGroupIDArr'];
$LocationID_DisplayFilterArr = $FilterArr['LocationIDArr'];

// $LessonInfoArr[$thisDay][$thisTimeSlotID][0,1,2...] = Lesson Info
$LessonInfoArr = $ObjTimetable->Get_Room_Allocation_List($TimeSlotID='', $Day='', $SubjectGroupID_DisplayFilterArr, $LocationID_DisplayFilterArr, $OthersLocation, $DayAsFirstKey=1);

# Get Timeslot Info
$TimeSlotInfoArr = $ObjTimetable->Get_Timeslot_List($ReturnAsso=1);
$numOfTimeSlot = count($TimeSlotInfoArr);

# Get Subject Group Info
$libSCM = new subject_class_mapping();
// $SubjectGroupInfoArr[$SubjectGroupID] = Array Of Info
$SubjectGroupInfoArr = $libSCM->Get_Subject_Group_Info($AcademicYearID, $YearTermID, $TimetableID);


# Get Location Info
$liblocation = new liblocation();
// $LocationInfoArr[$LocationID] = LocationInfoArr
$LocationInfoArr = $liblocation->Get_Building_Floor_Room_Name_Arr($TimetableID);




# file name 
$TimetableName = $ObjTimetable->Get_Timetable_Name();
$TimetableName = str_replace(' ', '_', $TimetableName);
$filename = "lesson_of_$TimetableName.csv";	

# define column title (2 dimensions array, 1st row is english, 2nd row is chinese)
$lexport = new libexporttext();
$ColumnPropertyArr = $ObjTimetable->Get_Import_Lesson_Column_Property();
$exportColumn = $lexport->GET_EXPORT_HEADER_COLUMN($Lang['SysMgr']['Timetable']['ExportTimetable_Column'], $ColumnPropertyArr);

# get file content
$RowCounter = 0;
$ColumnCounter = 0;
$ExportArr = array();
if (count($LessonInfoArr)==0 && $ForImportSample) {
	// insert some sample data if there are no data in the timetable
	$ExportArr[$RowCounter][] = 1;
	$ExportArr[$RowCounter][] = 1;
	$ExportArr[$RowCounter][] = 'Lesson 1';
	$ExportArr[$RowCounter][] = '08:00 - 08:45';
	$ExportArr[$RowCounter][] = 'CHI_F1A';
	$ExportArr[$RowCounter][] = 'Chinese F1A';
	$ExportArr[$RowCounter][] = 'MB';
	$ExportArr[$RowCounter][] = 'Main Building';
	$ExportArr[$RowCounter][] = 'F001';
	$ExportArr[$RowCounter][] = 'Floor 1';
	$ExportArr[$RowCounter][] = 'R001';
	$ExportArr[$RowCounter][] = 'Room 101';
	$ExportArr[$RowCounter][] = '';
	$RowCounter++;
	
	$ExportArr[$RowCounter][] = 1;
	$ExportArr[$RowCounter][] = 2;
	$ExportArr[$RowCounter][] = 'Lesson 2';
	$ExportArr[$RowCounter][] = '08:45 - 09:30';
	$ExportArr[$RowCounter][] = 'PE_F1A';
	$ExportArr[$RowCounter][] = 'PE F1A';
	$ExportArr[$RowCounter][] = '';
	$ExportArr[$RowCounter][] = '';
	$ExportArr[$RowCounter][] = '';
	$ExportArr[$RowCounter][] = '';
	$ExportArr[$RowCounter][] = '';
	$ExportArr[$RowCounter][] = '';
	$ExportArr[$RowCounter][] = 'Playground';
	$RowCounter++;
}
else {
	foreach ((array)$LessonInfoArr as $thisDay => $thisDayLessonInfoArr)
	{
	    if ($thisDay > $maxCycleDays) {
	        continue;
	    }
	    
		foreach ((array)$thisDayLessonInfoArr as $thisTimeSlotID => $thisTimeSlotDayLessonInfoArr)
		{
			$numOfLesson = count($thisTimeSlotDayLessonInfoArr);
			for ($i=0; $i<$numOfLesson; $i++)
			{
				$ColumnCounter = 0;
				
				### Get Lesson TimeSlot Info
				$thisTimeSlotDisplayOrder = $TimeSlotInfoArr[$thisTimeSlotID]['DisplayOrder'];
				$thisTimeSlotName = $TimeSlotInfoArr[$thisTimeSlotID]['TimeSlotName'];
				$thisTimeSlotStartTime = substr($TimeSlotInfoArr[$thisTimeSlotID]['StartTime'], 0, 5);	// Hour and Minute only
				$thisTimeSlotEndTime = substr($TimeSlotInfoArr[$thisTimeSlotID]['EndTime'], 0, 5);
				$thisTimeSlotTimeRange = $thisTimeSlotStartTime.' - '.$thisTimeSlotEndTime;
				
				### Get Lesson Subject Group Info
				$thisSubjectGroupID = $thisTimeSlotDayLessonInfoArr[$i]['SubjectGroupID'];
				$thisSubjectGroupCode = $SubjectGroupInfoArr[$thisSubjectGroupID]['SubjectGroupCode'];
				$thisSubjectGroupName = Get_Lang_Selection($SubjectGroupInfoArr[$thisSubjectGroupID]['ClassTitleB5'], $SubjectGroupInfoArr[$thisSubjectGroupID]['ClassTitleEN']);
				
				### Get Lesson Location Info
				$thisLocationID = $thisTimeSlotDayLessonInfoArr[$i]['LocationID'];
				$thisBuildingCode = $LocationInfoArr[$thisLocationID]['BuildingCode'];
				$thisBuildingName = Get_Lang_Selection($LocationInfoArr[$thisLocationID]['BuildingNameChi'], $LocationInfoArr[$thisLocationID]['BuildingNameEng']);
				$thisFloorCode = $LocationInfoArr[$thisLocationID]['FloorCode'];
				$thisFloorName = Get_Lang_Selection($LocationInfoArr[$thisLocationID]['FloorNameChi'], $LocationInfoArr[$thisLocationID]['FloorNameEng']);
				$thisRoomCode = $LocationInfoArr[$thisLocationID]['RoomCode'];
				$thisRoomName = Get_Lang_Selection($LocationInfoArr[$thisLocationID]['RoomNameChi'], $LocationInfoArr[$thisLocationID]['RoomNameEng']);
				
				$thisOthersLocation = $thisTimeSlotDayLessonInfoArr[$i]['OthersLocation'];
				
				$ExportArr[$RowCounter][$ColumnCounter++] = $thisDay;
				$ExportArr[$RowCounter][$ColumnCounter++] = $thisTimeSlotDisplayOrder;
				$ExportArr[$RowCounter][$ColumnCounter++] = $thisTimeSlotName;
				$ExportArr[$RowCounter][$ColumnCounter++] = $thisTimeSlotTimeRange;
				$ExportArr[$RowCounter][$ColumnCounter++] = $thisSubjectGroupCode;
				$ExportArr[$RowCounter][$ColumnCounter++] = $thisSubjectGroupName;
				$ExportArr[$RowCounter][$ColumnCounter++] = $thisBuildingCode;
				$ExportArr[$RowCounter][$ColumnCounter++] = $thisBuildingName;
				$ExportArr[$RowCounter][$ColumnCounter++] = $thisFloorCode;
				$ExportArr[$RowCounter][$ColumnCounter++] = $thisFloorName;
				$ExportArr[$RowCounter][$ColumnCounter++] = $thisRoomCode;
				$ExportArr[$RowCounter][$ColumnCounter++] = $thisRoomName;
				$ExportArr[$RowCounter][$ColumnCounter++] = $thisOthersLocation;
				$RowCounter++;
			}
		}	// End Loop TimeSlot
	}	// End Loop Day
}



$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn);

intranet_closedb();

# Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);
?>