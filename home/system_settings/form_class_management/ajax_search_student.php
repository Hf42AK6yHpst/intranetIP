<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$StudentSelected = $_REQUEST['StudentSelected'][0];
$AcademicYearID = $_REQUEST['AcademicYearID'];
$YearClassID = $_REQUEST['YearClassID'];
$SearchValue = stripslashes(urldecode($_REQUEST['q']));

$fcm = new form_class_manage();

$Result = $fcm->Search_Student($SearchValue,$StudentSelected,$AcademicYearID,$YearClassID);

for ($i=0; $i< sizeof($Result); $i++) {
	echo $Result[$i]['Name']."|".$Result[$i]['UserEmail']."|".$Result[$i]['UserID']."\n";
}

intranet_closedb();
die;
?>