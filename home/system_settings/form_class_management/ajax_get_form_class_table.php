<?php
// using 
/**
 * 2018-08-10 (Henry): fixed sql injection 
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

intranet_opendb();

$AcademicYearID = IntegerSafe($_REQUEST['AcademicYearID']);
$SearchKeyword = stripslashes(urldecode($_REQUEST['SearchKeyword']));

$FormClassUI = new form_class_manage_ui();

echo $FormClassUI->Get_Form_Class_Table($AcademicYearID,$SearchKeyword);

intranet_closedb();
?>