<?php
# using: 

### Change Log [Start] ###
/*
* Date		:	20100708 (Henry Chow)
* Detail	:	add GET_EXPORT_TXT_WITH_REFERENCE(), apply new standard of export
*/
### Change Log [End] ###

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
 
$lexport = new libexporttext();
$ldb = new libdb();

$ExportArr = array();

$yearName = getAYNameByAyId($AcademicYearID);

# file name 
$filename = "class_teacher_$yearName.csv";	

# SQL Statement
$namefield = getNamefieldByLang("USR.");

$sql = "SELECT 
			IFNULL(au.UserLogin,USR.UserLogin), 
			$namefield,
			yc.ClassTitleEN
		FROM
			YEAR_CLASS_TEACHER yct LEFT OUTER JOIN
			INTRANET_USER USR ON (USR.UserID=yct.UserID AND USR.RecordType=1) LEFT OUTER JOIN
			INTRANET_ARCHIVE_USER au ON (au.UserID=yct.UserID AND au.RecordType=1) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=yct.YearClassID) LEFT OUTER JOIN
			YEAR y ON (y.YearID=yc.YearID)
		WHERE
			yc.AcademicYearID=$AcademicYearID
		ORDER BY
			y.Sequence, yc.Sequence, USR.EnglishName
		";
$row = $ldb->returnArray($sql,9);

# Create data array for export

for($i=0; $i<sizeof($row); $i++){
	$pos = 0;
	$ExportArr[$i][$pos] = $row[$i][0];	$pos++;			//teacher login id
	$ExportArr[$i][$pos] = $row[$i][1];	$pos++;			//teacher name
	$ExportArr[$i][$pos] = $row[$i][2];	$pos++;			//class name (en)
	$pos++;
}

# define column title (2 dimensions array, 1st row is english, 2nd row is chinese)
$exportColumn[0] = array($Lang['formClassMapping']['ExportClassTeacher_Column'][0][0], $Lang['formClassMapping']['ExportClassTeacher_Column'][0][1], $Lang['formClassMapping']['ExportClassTeacher_Column'][0][2]);
$exportColumn[1] = array($Lang['formClassMapping']['ExportClassTeacher_Column'][1][0], $Lang['formClassMapping']['ExportClassTeacher_Column'][1][1], $Lang['formClassMapping']['ExportClassTeacher_Column'][1][2]);
	
$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
