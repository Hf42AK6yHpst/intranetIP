<?php
// using : 

########### Change Log [Start] ######## 
#	Date	:	2016-01-19 [Carlos]
#				added [Copy Class Students] option. 
#
#	Date	:	2010-09-22 [Henry Chow]
#				user in array $special_feature['CanEditPreviousYearInClass'] can copy classes to previous year  
#
############ Change Log [End] #########

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

if ($sys_custom['Class']['ClassGroupSettings'] == true)
	include_once($PATH_WRT_ROOT."includes/libclassgroup.php");

intranet_auth();
intranet_opendb();

$ldb = new libdb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$sql = "SELECT AcademicYearID, ".Get_Lang_Selection('YearNameB5', 'YearNameEN')." as yearName FROM ACADEMIC_YEAR ORDER BY Sequence";
$yearInfo = $ldb->returnArray($sql);

$currentAcademicYearID = Get_Current_Academic_Year_ID();

$a = 0;
$b = 0;

for($i=0; $i<sizeof($yearInfo); $i++) {
	list($year_id, $year_name) = $yearInfo[$i];
	$sql = "SELECT COUNT(*) FROM YEAR_CLASS WHERE AcademicYearID=$year_id";
	$count = $ldb->returnVector($sql);
	
	$term = getSemesters($year_id);
	
	if($count[0]!=0) {
		$years1[$a][] = $year_id;
		$years1[$a][] = $year_name;
		$a++;
	} else {
		if(sizeof($term)>0) {		# will display only if Term information is/are set
			$startDateCurrentAcademicYear = getStartDateOfAcademicYear(Get_Current_Academic_Year_ID());
			$endDateDesireYear = getEndDateOfAcademicYear($year_id);
			if(isset($special_feature['CanEditPreviousYearInClass']) && in_array($UserID, $special_feature['CanEditPreviousYearInClass'])) {
				$years2[$b][] = $year_id;
				$years2[$b][] = $year_name;	
				$b++;			
			}
			else if($endDateDesireYear>$startDateCurrentAcademicYear) {
				$years2[$b][] = $year_id;
				$years2[$b][] = $year_name;
				$b++;
			}
		}
	}
}


$yearSelection1 = getSelectByArray($years1, ' name="fromYear" id="fromYear"', $currentAcademicYearID);
$yearSelection2 = getSelectByArray($years2, ' name="toYear" id="toYear"');

$linterface = new interface_html();

$CurrentPageArr['Class'] = 1;

### Title ###
$title = $Lang['SysMgr']['FormClassMapping']['ClassPageSubTitle'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ['title'] = $Lang['SysMgr']['FormClassMapping']['ClassPageTitle'];

$linterface->LAYOUT_START(); 

//$PAGE_NAVIGATION[] = array($Lang['SysMgr']['FormClassMapping']['CopyFromOtherAcademicYear'], "");
/*
$STEPS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['Options'], 1);
$STEPS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['WebSAMSCode'], 0);
$STEPS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['CopyResult'], 0);
*/

?>
<script language="javascript">
<!--
function checkForm() {
	var obj = document.form1;
	if(obj.fromYear.value=="") {
		alert("<?=$i_alert_pleaseselect." ".$Lang['SysMgr']['SubjectClassMapping']['CopyFromAcademicYear']?>");
		obj.fromYear.focus();
		return false;	
	}
	if(obj.toYear.value=="") {
		alert("<?=$i_alert_pleaseselect." ".$Lang['SysMgr']['SubjectClassMapping']['CopyToAcademicYear']?>");
		obj.toYear.focus();
		return false;	
	}
}
-->
</script>
<br>
<form method="POST" name="form1" id="form1" action="copy_update.php" onsubmit="return checkForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?></td>
</tr>
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td><?/*= $linterface->GET_STEPS($STEPS_OBJ) */?></td>
</tr>
<tr>
	<td>
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr> 
            	<td><br />
					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">			
						<tr>
							<td class="formfieldtitle" align="left" width="30%"><?=$Lang['SysMgr']['SubjectClassMapping']['CopyFromAcademicYear']?></td>
							<td class="tabletext"><?=$yearSelection1?></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['SubjectClassMapping']['CopyToAcademicYear']?> </td>
							<td class="tabletext"><?=$yearSelection2?></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left" width="30%"><?=$Lang['SysMgr']['SubjectClassMapping']['CopyClassStudents']?></td>
							<td class="tabletext">
								<?=$linterface->Get_Radio_Button("copyStudentsY", "copyStudents", "1", 0, "", $Lang['General']['Yes'], "", 0).'&nbsp;'?>
								<?=$linterface->Get_Radio_Button("copyStudentsN", "copyStudents", "0", 1, "", $Lang['General']['No'], "", 0)?>
							</td>
						</tr>
						<tr>
							<td height="1" class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
						</tr>
						<tr>
							<td colspan="2" align="center">
								<?=$linterface->GET_ACTION_BTN($button_submit, "submit")?>
								<?=$linterface->GET_ACTION_BTN($button_reset, "reset")?>
								<?=$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:self.location.href='index.php'")?>
							</td>
						</tr>
					</table>
				</td>
	        </tr>
	    </table>
	</td>
</tr>
</table>
<?
$linterface->LAYOUT_STOP(); 
intranet_closedb(); ?>

