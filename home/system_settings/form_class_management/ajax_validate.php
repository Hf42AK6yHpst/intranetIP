<?php
// using Siuwan
/*
 * Modification Log:
 * 	2014-08-26 Bill
 * 	- Fixed: system failed to check "Student not in selected Form" when import Class Name and Class Number
 * 	2011-09-09 Ivan [2011-0905-1445-40066]
 * 	- Fixed: system failed to check if the import target Class is in the selected Form
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

# Get data
$Action = stripslashes($_REQUEST['Action']);
if ($Action == 'Form_Stage_Code')
{
	include_once($PATH_WRT_ROOT."includes/libformstage.php");
	$libFormStage = new Form_Stage();
	
	$InputValue = trim(urldecode(stripslashes($_REQUEST['InputValue'])));
	$ExcludeID = trim(urldecode(stripslashes($_REQUEST['ExcludeID'])));
	$isCodeExisted = $libFormStage->Is_Code_Existed($InputValue, $ExcludeID);
	
	if ($isCodeExisted == 1)
		echo $Lang['SysMgr']['FormClassMapping']['Warning']['DuplicatedCode']['FormStage'];
	else
		echo $returnString = "";
}
else if ($Action == "Import_Form_Class_Student")
{
	$TargetFilePath = stripslashes($_REQUEST['TargetFilePath']);
	$AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);
	$YearID = stripslashes($_REQUEST['YearID']);
	
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
	include_once($PATH_WRT_ROOT."includes/libclass.php");
	
	$li = new libdb();
	$libclass = new libclass();
	$libFCM = new form_class_manage();
	$limport = new libimporttext();
	
	
	### Include js libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
		
	### Get csv data
	$file_format = array("Student Login ID", "Current Class Name (EN)", "Current Class Number", "Target Class Name (EN)", "Target Class Number");
	$flagAry = array(1, 0, 1, 1, 1, 1);
	$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $file_format, $flagAry);
	$col_name = array_shift($data);
	$numOfData = count($data);
	
	
	### Get Form Student for checking
	$FormObj = new Year($YearID);
	$FormStudentInfoArr = $FormObj->Get_All_Student($AcademicYearID);
	$FormStudentIDArr = Get_Array_By_Key($FormStudentInfoArr, 'UserID');
	$StudentClassMappingArr = BuildMultiKeyAssoc($FormStudentInfoArr, array('UserID'));
	
	$errorCount = 0;
	$successCount = 0;
	$ErrorResultArr = array();
	$ErrorFieldArr = array();
	$InsertValueArr = array();
	$UserObjArr = array();
	$StudentFormMappingArr = array();
	$RowNumber = 3;
	
	foreach($data as $key => $data)
	{
		$thisErrorArr = array();
		$thisStudentID = '';
		### store csv data to temp data
		list($UserLogin, $ClassNameEn, $ClassNumber, $TargetClassNameEn, $TargetClassNumber) = $data;
		$UserLogin = trim($UserLogin);
		$ClassNameEn = trim($ClassNameEn);
		$ClassNumber = trim($ClassNumber);
		$TargetClassNameEn = trim($TargetClassNameEn);
		$TargetClassNumber = trim($TargetClassNumber);
		
		//echo $UserLogin.'/'.$ClassNameEn.'/'.$ClassNumber.'<br>';
		### check data
		# 1. Check empty data
		/* if(empty($UserLogin) || empty($ClassNameEn) || empty($ClassNumber))
			$thisErrorArr[] = $Lang['SysMgr']['FormClassMapping']['MissingData'];
			 */
		if(empty($UserLogin) && (empty($ClassNameEn) || empty($ClassNumber)) && empty($TargetClassNameEn) && empty($TargetClassNumber))
			$thisErrorArr[] = $Lang['SysMgr']['FormClassMapping']['MissingData'];
				 
		### Get Student Info
		if(!empty($UserLogin)){
			$inputType = 'UserLogin';
			if (!isset($UserObjArr[$UserLogin]))
			{
				$UserObjArr[$UserLogin] = new libuser('', $UserLogin);
				$TmpArr = $UserObjArr[$UserLogin]->Get_User_Studying_Form('', $AcademicYearID);
				$StudentFormMappingArr[$UserLogin] = $TmpArr[0]['YearID'];
			}
			$thisUserObj = $UserObjArr[$UserLogin];
			$thisStudentID = $thisUserObj->UserID;
			$thisRecordStatus = $thisUserObj->RecordStatus;
			$thisRecordType = $thisUserObj->RecordType;
		}elseif(!empty($ClassNameEn) && !empty($ClassNumber)){ //Get UserLogin By ClassNameEn and ClassNumber
			$inputType = 'ClassNameAndNumber';
			$lu = new libuser();
			$thisStudentID = $lu->returnUserID($ClassNameEn,$ClassNumber);
			if(!empty($thisStudentID)){
				$thisUserAry = $lu->returnUser($thisStudentID); 
				$thisRecordStatus = $thisUserAry[0]['RecordStatus'];
				$thisRecordType = $thisUserAry[0]['RecordType'];
				$TmpArr = $lu->Get_User_Studying_Form($thisStudentID, $AcademicYearID);
				$StudentFormMappingArr[$thisUserAry[0]['UserLogin']] = $TmpArr[0]['YearID'];
			}
		}
	
		# 2. Check if user login exists	
		if ($thisStudentID == '' || $thisStudentID == 0)
		{
			$thisErrorArr[] = $Lang['SysMgr']['FormClassMapping']['StudentNotExist'];
			$isError = 1;
		}
		else
		{
			# 2a. Check if user login account is active
			$isError = 0;
			if ($thisRecordStatus != 1)
			{
				$thisErrorArr[] = $Lang['SysMgr']['FormClassMapping']['NotActiveAccountWarning'];
				$isError = 1;
			}
				//debug_pr($StudentFormMappingArr);
			# Check if the Student is in the Selected Form
			// $StudentFormMappingArr[$UserLogin] == '' means the student has not joined any classes yet
			// Add checking when input class name and class number
			if ((!empty($UserLogin) && $StudentFormMappingArr[$UserLogin] != '' && $StudentFormMappingArr[$UserLogin] != $YearID) || 
					(!empty($ClassNameEn) && !empty($ClassNumber) && $StudentFormMappingArr[$thisUserAry[0]['UserLogin']] != '' && $StudentFormMappingArr[$thisUserAry[0]['UserLogin']] != $YearID))
			{
				$thisErrorArr[] = $Lang['SysMgr']['FormClassMapping']['StudentNotInSelectedForm'];
				$isError = 1;
			}
			
			# Check if the user is a student or not
			if ($thisRecordType != 2)
			{
				$thisErrorArr[] = $Lang['SysMgr']['FormClassMapping']['UserIsNotAStudent'];
				$isError = 1;
			}
		}
		if($isError){
			if($inputType == 'UserLogin'){
				$ErrorFieldArr[$RowNumber]['UserLogin'] = 1;
			}else{
				$ErrorFieldArr[$RowNumber]['ClassName'] = 1;
				$ErrorFieldArr[$RowNumber]['ClassNumber'] = 1;
			}			
		}
		
			
		# 3. Check if target class name valid
		$isClassNameValid = $libFCM->Is_ClassName_Valid($TargetClassNameEn);
		if ($isClassNameValid == false)
		{
			$thisErrorArr[] = $Lang['SysMgr']['FormClassMapping']['TargetClassNameQuoteWarning'];
			$ErrorFieldArr[$RowNumber]['TargetClassName'] = 1;
		}
			
		# 4. Check if class exist
		//$isClassExist = $libFCM->Is_ClassName_Exist($ClassNameEn, $AcademicYearID);
		// Commented by Ivan 2011-09-09 [2011-0905-1445-40066]
		//$thisYearClassID = $libclass->getClassID($ClassNameEn, $AcademicYearID);
		$thisYearClassID = $libclass->getClassID($TargetClassNameEn, $AcademicYearID, $YearID);
		if ($thisYearClassID == '' || $thisYearClassID == 0)
		{
			$thisErrorArr[] = $Lang['SysMgr']['FormClassMapping']['TargetClassNotInSelectedForm'];
			$ErrorFieldArr[$RowNumber]['TargetClassName'] = 1;
		}
			
		if ($TargetClassNumber != '')
		{
			# 5. Check if the class number is numeric
			if (!is_numeric($TargetClassNumber))
			{
				$thisErrorArr[] = $Lang['SysMgr']['FormClassMapping']['TargetClassNumberIntegerOnlyWarning'];
				$ErrorFieldArr[$RowNumber]['TargetClassNumber'] = 1;
			}
			
			# 6. Check if the class number is in use
//			if ($libFCM->Is_ClassNumber_Used_By_ClassName($ClassNumber, $ClassNameEn, $AcademicYearID))
//				$thisErrorArr[] = $Lang['SysMgr']['FormClassMapping']['ClassNumberDuplicatedWarning'];
			
			# 7. Check if the class number is in use within the csv
//			$sql = "Select 
//							TempID 
//					From 
//							TEMP_CLASS_STUDENT_IMPORT 
//					Where 
//							ClassNameEn = '".$li->Get_Safe_Sql_Query($ClassNameEn)."'
//							And
//							ClassNumber = '".$li->Get_Safe_Sql_Query($ClassNumber)."'
//							And 
//							UserID = '".$_SESSION["UserID"]."'
//					";
//			$resultSet = $li->returnArray($sql);
//			if (count($resultSet) > 0)
//				$thisErrorArr[] = $Lang['SysMgr']['FormClassMapping']['ClassNumberDuplicatedinCSVWarning'];
				
			# 8. Check if the Class Number is within 9 digits
			if (strlen($TargetClassNumber) > 9)
			{
				$thisErrorArr[] = $Lang['SysMgr']['FormClassMapping']['TargetClassNumberMustBeWithinDigits'];
				$ErrorFieldArr[$RowNumber]['TargetClassNumber'] = 1;
			}
		}else{
			$thisErrorArr[] = $Lang['SysMgr']['FormClassMapping']['TargetClassNumberWarning'];
			$ErrorFieldArr[$RowNumber]['TargetClassNumber'] = 1;
		}
		
		### Prepare insert statement
		$thisOldYearClassID = $StudentClassMappingArr[$thisStudentID]['YearClassID'];
		$InsertValueArr[] = "(	'".$_SESSION["UserID"]."', '".$RowNumber."', '".$li->Get_Safe_Sql_Query($UserLogin)."', 
								'".$li->Get_Safe_Sql_Query($ClassNameEn)."', '".$li->Get_Safe_Sql_Query($ClassNumber)."', 
								'".$li->Get_Safe_Sql_Query($TargetClassNameEn)."', '".$li->Get_Safe_Sql_Query($TargetClassNumber)."', 
								'".$thisStudentID."', '".$thisYearClassID."', '".$thisOldYearClassID."', now()
							 )
							";
						
		### Count success / failed records	
		if(empty($thisErrorArr))
		{
			$successCount++;
			
			# Update the Class & Class Number mapping for the student
			$StudentClassMappingArr[$thisStudentID]['YearClassID'] = $thisYearClassID;
			$StudentClassMappingArr[$thisStudentID]['ClassNumber'] = $TargetClassNumber;
			$StudentClassMappingArr[$thisStudentID]['RowNumber'] = $RowNumber;
		}
		else
		{
			$ErrorResultArr[$RowNumber] = $thisErrorArr;
			$errorCount++;
		}
		
		
		### Update Processing Display
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = "'.$successCount.'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = "'.$errorCount.'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Processed_Number_Span").innerHTML = "'.($RowNumber).'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		
		$RowNumber++;
	}
	
	### Insert csv data into temp table
	$InsertValueList = implode(',', $InsertValueArr);
	$sql = "
			insert into TEMP_CLASS_STUDENT_IMPORT 
				(UserID, RowNumber, UserLogin, ClassNameEn, ClassNumber, TargetClassNameEn, TargetClassNumber, StudentID, YearClassID, OldYearClassID, DateInput)
			values
				$InsertValueList
			";
	$li->db_db_query($sql) or die(mysql_error());
	
	
	### Check Class & Class Number Conflict
	# Update Processing Display
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Span").innerHTML = "'.$Lang['SysMgr']['FormClassMapping']['CheckingClassAndClassNumberConflict'].'...";';
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
	
	# Insert Result to temp table
	$InsertValueArr = array();
	foreach((array)$StudentClassMappingArr as $thisStudentID => $thisStudentClassInfoArr)
	{
		$thisYearClassID = $thisStudentClassInfoArr['YearClassID'];
		$thisClassNumber = $thisStudentClassInfoArr['ClassNumber'];
		$thisRowNumber = $thisStudentClassInfoArr['RowNumber'];
		
		$InsertValueArr[] = " ('".$_SESSION['UserID']."', '".$thisRowNumber."', '".$thisStudentID."', '".$thisYearClassID."', '".$thisClassNumber."', now()) ";
	}
	
	$InsertValueList = implode(',', $InsertValueArr);
	$sql = "
			insert into TEMP_CLASS_STUDENT_RESULT_IMPORT 
				(UserID, RowNumber, StudentID, YearClassID, ClassNumber, DateInput)
			values
				$InsertValueList
			";
	$li->db_db_query($sql);
	
	# Check if Class & ClassNumber Duplication
	$sql = "Select
				tcsri.RowNumber, tcsri.YearClassID, tcsri.ClassNumber
			From
				(
					Select 
							Count(TempID) as Counter, YearClassID, ClassNumber 
					From 
							TEMP_CLASS_STUDENT_RESULT_IMPORT 
					Where 
							UserID = '".$_SESSION['UserID']."'
					Group By
							YearClassID, ClassNumber
				) as tmp
				Inner Join
				TEMP_CLASS_STUDENT_RESULT_IMPORT as tcsri On (tmp.YearClassID = tcsri.YearClassID And tmp.ClassNumber = tcsri.ClassNumber)
			Where 
				tmp.Counter > 1
				And
				tcsri.RowNumber > 0
			";
	$ClassInfoDuplicatedArr = $li->returnArray($sql);
	$numOfDuplicatedInfo = count($ClassInfoDuplicatedArr);
	for ($i=0; $i<$numOfDuplicatedInfo; $i++)
	{
		$thisRowNumber = $ClassInfoDuplicatedArr[$i]['RowNumber'];
		
		if (!isset($ErrorResultArr[$thisRowNumber]))
		{
			// Originally success record, but failed in this duplication checking => add to error records
			$successCount--;
			$errorCount++;
		}
		
		(array)$ErrorResultArr[$thisRowNumber][] = $Lang['SysMgr']['FormClassMapping']['ClassInfoDuplicated'];
		$ErrorFieldArr[$thisRowNumber]['TargetClassName'] = 1;
		$ErrorFieldArr[$thisRowNumber]['TargetClassNumber'] = 1;
	}
	
	if($ErrorResultArr)
	{
		### Update Processing Display
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Span").innerHTML = "'.$Lang['SysMgr']['SubjectClassMapping']['GeneratingErrorsInfo'].'...";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
			
		$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
			$x .= "<tr>";
				$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">Row#</td>";
				$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['FormClassMapping']['StudentLoginID'] ."</td>";
				$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['FormClassMapping']['ClassTitleEN'] ."</td>";
				$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['FormClassMapping']['StudentClassNumber'] ."</td>";
				$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['FormClassMapping']['TargetClassTitleEN'] ."</td>";
				$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['FormClassMapping']['TargetClassNumber'] ."</td>";
				$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['General']['Remark'] ."</td>";
			$x .= "</tr>";
			
			### Get data of the error rows
			$ErrorRowArr = array_keys($ErrorResultArr);
			$ErrorRowList = implode(',', $ErrorRowArr);
			$sql = "select * from TEMP_CLASS_STUDENT_IMPORT where RowNumber in ($ErrorRowList) And UserID = '".$_SESSION['UserID']."'";
			$tempData = $li->returnArray($sql);
			
			$i=0;
			foreach($tempData as $k=>$d)
			{
				list($t_TempID, $t_UserID, $t_RowNumber, $t_UserLogin, $t_ClassNameEn, $t_ClassNumber, $t_TargetClassNameEn, $t_TargetClassNumber) = $d;
				
				### Missing Data Display
				if(!empty($t_UserLogin)){
					$t_ClassNameEn = $t_ClassNameEn ? $t_ClassNameEn : "--";
					$t_ClassNumber = $t_ClassNumber ? $t_ClassNumber : "--";
				}elseif(!empty($t_ClassNameEn)&&!empty($t_ClassNumber)){
					$t_UserLogin = $t_UserLogin ? $t_UserLogin : "--";
				}else{
					$t_UserLogin = $t_UserLogin ? $t_UserLogin : "<font color=\"red\">***</font>";
					$t_ClassNameEn = $t_ClassNameEn ? $t_ClassNameEn : "<font color=\"red\">***</font>";
					$t_ClassNumber = $t_ClassNumber ? $t_ClassNumber : "<font color=\"red\">***</font>";
				}
				$t_TargetClassNameEn = $t_TargetClassNameEn ? $t_TargetClassNameEn : "<font color=\"red\">***</font>";
				$t_TargetClassNumber = $t_TargetClassNumber ? $t_TargetClassNumber : "<font color=\"red\">***</font>";
				### Csv Data Display
				if ($ErrorFieldArr[$t_RowNumber]['UserLogin'] == 1)
					$t_UserLogin = "<font color=\"red\">".$t_UserLogin."</font>";
					
				if ($ErrorFieldArr[$t_RowNumber]['ClassName'] == 1)
					$t_ClassNameEn = "<font color=\"red\">".$t_ClassNameEn."</font>";
					
				if ($ErrorFieldArr[$t_RowNumber]['ClassNumber'] == 1)
					$t_ClassNumber = "<font color=\"red\">".$t_ClassNumber."</font>";
					
				if ($ErrorFieldArr[$t_RowNumber]['TargetClassName'] == 1)
					$t_TargetClassNameEn = "<font color=\"red\">".$t_TargetClassNameEn."</font>";
					
				if ($ErrorFieldArr[$t_RowNumber]['TargetClassNumber'] == 1)
					$t_TargetClassNumber = "<font color=\"red\">".$t_TargetClassNumber."</font>";
								
				### Error Remarks Display
				$thisErrorArr = $ErrorResultArr[$t_RowNumber];	
				$thisErrorDisplay = implode('<br />', $thisErrorArr);
							
				$css_i = ($i % 2) ? "2" : "";
				$x .= "<tr style=\"vertical-align:top\">";
				$x .= "<td class=\"tablebluerow$css_i\" width=\"10\">". $t_RowNumber ."</td>";
				$x .= "<td class=\"tablebluerow$css_i\">". $t_UserLogin ."</td>";
				$x .= "<td class=\"tablebluerow$css_i\">". $t_ClassNameEn ."</td>";
				$x .= "<td class=\"tablebluerow$css_i\">". $t_ClassNumber ."</td>";
				$x .= "<td class=\"tablebluerow$css_i\">". $t_TargetClassNameEn ."</td>";
				$x .= "<td class=\"tablebluerow$css_i\">". $t_TargetClassNumber ."</td>";				
				$x .= "<td class=\"tablebluerow$css_i\">". $thisErrorDisplay ."</td>";
				$x .= "</tr>";
				
				$i++;
			}
		$x .= "</table>";
	}
	
	$ErrorCountDisplay = ($errorCount ? "<font color=\"red\"> ": "") . $errorCount . ($errorCount ? "</font>" : "");
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$x.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$successCount.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$ErrorCountDisplay.'\';';
		
		if ($errorCount == 0) 
		{
			$thisJSUpdate .= 'window.parent.document.getElementById("ImportButtonDiv").style.display = "inline";';
		}
		
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}

intranet_closedb();

?>