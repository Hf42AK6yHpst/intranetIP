<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$YearID = $_REQUEST['YearID'];
$YearName = stripslashes(urldecode($_REQUEST['YearName']));
$WEBSAMSCode = stripslashes(urldecode($_REQUEST['WEBSAMSCode']));
$RecordStatus = stripslashes(urldecode($_REQUEST['RecordStatus']));
$FormStageID = stripslashes(urldecode($_REQUEST['FormStageID']));


$Year = new Year($YearID);

$Year->YearName = (trim($YearName) == "")? $Year->YearName:trim($YearName);
$Year->WEBSAMSCode = (trim($WEBSAMSCode) == "")? $Year->WEBSAMSCode:trim($WEBSAMSCode);
$Year->RecordStatus = (trim($RecordStatus) === "")? $Year->RecordStatus:trim($RecordStatus);
$Year->FormStageID = (trim($FormStageID) === "")? $Year->FormStageID:trim($FormStageID);

if ($Year->Update_Year()) {
	echo $Lang['SysMgr']['FormClassMapping']['FormEditSuccess'];
}
else {
	echo $Lang['SysMgr']['FormClassMapping']['FormEditUnsuccess'];
}
intranet_closedb();
die;
?>