<?
// Modifying by: 

############# Change Log [Start] ################
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
// include_once($PATH_WRT_ROOT."includes/libimporttext.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libclassgroup.php");

intranet_opendb();

$li = new libdb();
$fcm = new form_class_manage();
$Year = new Year();
$lcg = new Class_Group();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

############################################################################
# retrieve Form data from temp table TEMP_FORM_CLASS_IMPORT
############################################################################
$sql = "select distinct(FormName) from TEMP_FORM_CLASS_IMPORT order by FormName";
$form_result = $li->returnVector($sql);
foreach($form_result as $k=>$FormName)
{
	# retrieve the latest FormWebsamsCode
	$sql = "select FormWebsamsCode from TEMP_FORM_CLASS_IMPORT where FormName='". $FormName ."' order by RowNumber desc limit 1";
	$FormWebsamsCode = $li->returnVector($sql);
	$FormWebsamsCode = empty($FormWebsamsCode) ? "NA" : $FormWebsamsCode[0];
	
	# check Form name is exists ornot
	$sql = "select YearID from YEAR where YearName='$FormName'";
	$year_result = $li->returnVector($sql);
	if(empty($year_result))	# insert Form
	{
		$fcm->Create_Form($FormName,$FormWebsamsCode);
	}
	else					# update Form
	{
		$Year = new Year($year_result[0]);
		$Year->YearName = $FormName;
	 	$Year->WEBSAMSCode = $FormWebsamsCode;
		$Year->Update_Year();
	}
}

############################################################################
# retrieve Form data from temp table TEMP_FORM_CLASS_IMPORT
############################################################################
$sql = "select * from TEMP_FORM_CLASS_IMPORT where UserID = $UserID order by RowNumber";
$class_result = $li->returnArray($sql);
foreach($class_result as $k=>$d)
{
	if($sys_custom['Class']['ClassGroupSettings'] == true)
	{
		list($TempID, $ImportUserID, $RowNumber, $FormName, $FormWebsamsCode, $ClassNameEn, $ClassNameChi, $ClassWebsamsCode,$ClassGroupCode, $DateInput) = $d;
		$ClassGroupID = $lcg->Get_Class_Group_ID_By_Code($ClassGroupCode);
	}
	else
		list($TempID, $ImportUserID, $RowNumber, $FormName, $FormWebsamsCode, $ClassNameEn, $ClassNameChi, $ClassWebsamsCode, $DateInput) = $d;
		
	# Retrieve YearID (FormID)
	$YearID = $Year->returnYearIDbyYearName($FormName);
		
	# check the Class is exists or not (check $ClassNameEn & $ClassNameChi duplicate)
	$sql = "select YearClassID from YEAR_CLASS where (ClassTitleEN='$ClassNameEn' or ClassTitleB5='$ClassNameChi') And AcademicYearID = '".$AcademicYearID."'";
	$temp = $li->returnVector($sql);
	if(empty($temp))		# insert
	{
		$fcm->Create_ClassOnly($AcademicYearID,$YearID,$ClassNameEn,$ClassNameChi,$ClassWebsamsCode,$ClassGroupID);
	}
	else					# update
	{
		$YearClassID = $temp[0];
		$fcm->Edit_ClassOnly($YearClassID,$ClassNameEn,$ClassNameChi,$ClassWebsamsCode,$ClassGroupID);
	}
}


############################################################################
# delete data in Temp table
############################################################################
$sql = "delete from TEMP_FORM_CLASS_IMPORT where UserID = $UserID";
$li->db_db_query($sql) or die(mysql_error());



# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

# Title / Menu
$linterface = new interface_html();
$CurrentPageArr['Class'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['FormClassMapping']['ClassPageTitle'];
$TAGS_OBJ[] = array($Lang['SysMgr']['FormClassMapping']['ImportFormClass']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

?>


<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center"><?=$x?></td>
</tr>
		
<tr>
	<td>
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		
		<tr>
			<td class='tabletext' align='center'><?=count($class_result)?> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?></td>
		</tr>

		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php?AcademicYearID=$AcademicYearID'"); ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
