<?php
// Using: 

############# Change Log [Start] ################
#
#   Date:   2018-11-16 (Cameron)
#           - add BuildingID for eSchoolBus
#
#   Date:   2018-08-29 (Bill)   [2017-1207-0956-53277]
#           - Support Curriculum
#
############# Change Log [End] ################

@SET_TIME_LIMIT(21600);

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$AcademicYearID = $_REQUEST['AcademicYearID'];
$YearID = $_REQUEST['YearID'];
$ClassTitleEN = trim(stripslashes(urldecode($_REQUEST['ClassTitleEN'])));
$ClassTitleB5 = trim(stripslashes(urldecode($_REQUEST['ClassTitleB5'])));
$ClassTitleB5 = ($ClassTitleB5?$ClassTitleB5:$ClassTitleEN);
$StudentSelected = (is_array($_REQUEST['StudentSelected']))? $_REQUEST['StudentSelected']: array();
$SelectedClassTeacher = (is_array($_REQUEST['SelectedClassTeacher']))? $_REQUEST['SelectedClassTeacher']: array();
$ClassNumberMethod = $_REQUEST['ClassNumberMethod'];
$WEBSAMSCode = trim(stripslashes(urldecode($_REQUEST['WEBSAMSCode'])));
$ClassGroupID = trim(stripslashes(urldecode($_REQUEST['ClassGroupID'])));

// [2017-1207-0956-53277]
$CurriculumID = '';
if($sys_custom['iPf']['CurriculumsSettings']) {
    $CurriculumID = $_REQUEST['CurriculumID'];
}
$BuildingID = $plugin['eSchoolBus'] ? $_POST['BuildingID'] : '';

$fcm = new form_class_manage();
// $iCal = new icalendar();
$fcm->Start_Trans();

$Result = $fcm->Create_Class($AcademicYearID,$YearID,$ClassTitleEN,$ClassTitleB5,$StudentSelected,$SelectedClassTeacher,$ClassNumberMethod,$WEBSAMSCode,$ClassGroupID,$CurriculumID,$BuildingID);

// $Result["Create_class_cal"] = $iCal->createSystemCalendar($ClassTitleEN, 3);
// exit;

if (in_array(false,$Result)) {
	echo $Lang['SysMgr']['FormClassMapping']['ClassCreateUnsuccess'];
	$fcm->RollBack_Trans();
} else {
	echo $Lang['SysMgr']['FormClassMapping']['ClassCreateSuccess'];
	$fcm->Commit_Trans();
}

intranet_closedb();
?>