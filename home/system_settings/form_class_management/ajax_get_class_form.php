<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

intranet_opendb();

$YearClassID = $_REQUEST['YearClassID'];
$YearID = $_REQUEST['YearID'];
$AcademicYearID = $_REQUEST['AcademicYearID'];

$FormClassUI = new form_class_manage_ui();

echo $FormClassUI->Get_Class_Form($YearClassID,$YearID,$AcademicYearID);

intranet_closedb();
?>