<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclassgroup.php");

intranet_opendb();

$DisplayOrderString = stripslashes(urldecode($_REQUEST['DisplayOrderString']));

$libClassGroup = new Class_Group();
$displayOrderArr = $libClassGroup->Get_Update_Display_Order_Arr($DisplayOrderString, ",", "ClassGroupRow_");
$success = $libClassGroup->Update_DisplayOrder($displayOrderArr);


echo $success;
intranet_closedb();

?>