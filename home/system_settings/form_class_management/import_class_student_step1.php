<?php
// Modifying by: 

############# Change Log [Start] ################
#
# Date:	2015-10-16 Kenneth Yau
#		- Edit Style in Table
#		- Edit data coulumn from (1,2,3) to (A,B,C)
#
# Date:	2010-09-15 (Ivan)
#		- Add Form Selection
#
# Date:	2010-07-08 (Henry Chow)
#		- Apply new standard of import
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$fcm_ui = new form_class_manage_ui();
$ay = new academic_year($AcademicYearID);

### Title / Menu
$CurrentPageArr['Class'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['FormClassMapping']['ClassPageTitle'];
$TAGS_OBJ[] = array($Lang['SysMgr']['FormClassMapping']['ImportClassStudent']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# form class csv sample
$csvFile = "<a class='tablelink' href='". GET_CSV("sample_import_class_student.csv") ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";

for($i=0; $i<sizeof($Lang['formClassMapping']['ImportStudent_Column']); $i++) {
	$displayColumn .= $Lang['SysMgr']['Homework']['Column']." ".($i+1)." : ".$Lang['formClassMapping']['ImportStudent_Column'][$i]."<br>";	
}

### data column
$DataColumnTitleArr = $Lang['formClassMapping']['ImportStudent_Column'];
$DataColumnPropertyArr = array(3,3,3,3,3,3);
$DataColumn = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr);



# form selection
$YearID = $_REQUEST['YearID'];
$FormSelection = $fcm_ui->Get_Form_Selection('YearID', $YearID, $Onchange='', $noFirst=1, $isAll=0);

/*
$format_str .= "<p><span id=\"GM__to\"><strong>".$iDiscipline['Import_Instruct_Main']."</strong></span></p>";
$format_str .= "<div class=\"p\"><br/><ol type=\"a\"><li>".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_1']."
				<strong>&lt;<a class=\"tablelink\" href=\"". GET_CSV($csvFile) ."\" target=\"_blank\">".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_2']."</a>&gt;</strong>".
				$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_3']."</li>";
				
$format_str .= "<li>".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_b_1']."</span><br/>";
$format_str .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/discipline/detention_import_illus_$intranet_session_language.png\"/><br/><div class=\"note\">";
$format_str .= "<li><span id=\"GM__date\">".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_c_1']."</span></li></ul></div></li><br/>";
$format_str .= "</ol></div>";
*/
//$format_str = "description here~~~~~~~~~~";


//$warningMsg = $linterface->Get_Warning_Message_Box($title, $Lang['SysMgr']['FormClassMapping']['ImportClassStudentWarning']);
$remarksMsg = $Lang['General']['Remark'].': '.$Lang['SysMgr']['FormClassMapping']['ImportClassStudentWarning'];

$warningAry = array();
$warningAry[] = $Lang['SysMgr']['FormClassMapping']['ImportClassStudentForOneFormOnly'];
$warningAry[] = $Lang['SysMgr']['FormClassMapping']['ImportClassStudentWarning'];
$warningAry[] = $Lang['SysMgr']['FormClassMapping']['ImportClassStudentFieldRemark'];
$warningMsg = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $warningAry);


?>
<style type="text/css">
.instruction_box {
	width: 100%;
}
</style>

<br />
<form name="form1" method="post" action="import_class_student_step2.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	
	<tr>
		<td><table style="width:90%;" align="center"><tr><td><?=$warningMsg?></td></tr></table></td>
	</tr>
	
	<tr>
		<td align="center">
			<table class="form_table_v30" style="width:90%;text-align:center;" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td class="field_title"><?=$Lang['SysMgr']['FormClassMapping']['SchoolYear']?></td>
					<td class="tabletext" ><?=$ay->Get_Academic_Year_Name()?></td>
				</tr>
				<tr>
					<td class="field_title"><?=$Lang['SysMgr']['FormClassMapping']['Form']?></td>
					<td class="tabletext" ><?=$FormSelection?></td>
				</tr>				
				<tr>
					<td class="field_title"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
					<td class="tabletext"><input class="file" type="file" name="csvfile"></td>
				</tr>
				<tr>
					<td class="field_title"><?=$Lang['General']['CSVSample']?> </td>
					<td class="tabletext"><?=$csvFile?></td>
				</tr>
				<tr>
					<td class="field_title"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?> </td>
					<td class="tabletext"><?=$DataColumn?></td>
				</tr>
				
				<!--tr>
					<td class="tabletextremark" colspan="2"><?=$remarksMsg?></td>
				</tr-->
				
				<!--
				<tr>
					<td class="tabletext" colspan="2"><?=$iDiscipline['Import_Source_File_Note']?></td>
				</tr>
				<tr>
					<td class="tabletext" colspan="2"><?=$format_str?></td>
				</tr>
				-->
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td class="tabletextremark" colspan="2"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['RequiredField']?><br><?=$Lang['formClassMapping']['Reference']?></td>
				</tr>
				<tr>
            		<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        		</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp; <?=$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php?AcademicYearID=$AcademicYearID'")?>
		</td>
	</tr>
</table>
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
