<?php
// using Paul

############# Change Log [Start] ################
# Date: 2013-05-27 Ivan
#		- fixed function Remove_Class(), change variable name "AcadmicYearID" to "AcademicYearID" in the url
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$YearClassID = $_REQUEST['YearClassID'];

$YearClass = new year_class($YearClassID,true,true,true);
$linterface = new interface_html();

$CurrentPageArr['Class'] = 1;

### Title ###
$title = $Lang['SysMgr']['FormClassMapping']['ClassInfo'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ['title'] = $Lang['SysMgr']['FormClassMapping']['ClassPageTitle'];
$linterface->LAYOUT_START(); 

$FormClassUI = new form_class_manage_ui();
$Json = new JSON_obj();

echo $FormClassUI->Get_Class_Info($YearClass);
?>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->
<?
$linterface->LAYOUT_STOP();

intranet_closedb();
?>
<script>
// setting up language variable for js usage in included js function
var Lang = Array();
Lang['ClassTitleDuplicateWarning'] = '<?=$Lang['SysMgr']['FormClassMapping']['ClassTitleDuplicationWarning']?>';
Lang['ClassTitleEmptyWarning'] = '<?=$Lang['SysMgr']['FormClassMapping']['ClassTitleWarning']?>';
Lang['ClassTitleQuoteWarning'] = '<?=$Lang['SysMgr']['FormClassMapping']['ClassTitleQuoteWarning']?>';

$(document).ready(function () {
	Thick_Box_Init();
});

// Ajax function 
{
function Edit_Class() {
	Select_All_Options('StudentSelected[]',true);
	Select_All_Options('SelectedClassTeacher[]',true);
	
	var ClassTitleEN = document.getElementById('ClassTitleEN').value;
	var ClassTitleB5 = document.getElementById('ClassTitleB5').value;
	var ClassNumberMethod = Get_Radio_Value('ClassNumberMethod');
	
	if (Trim(ClassNumberMethod) == "") {
		document.getElementById("ClassNumberMethodWarningLayer").innerHTML = '<?=$Lang['SysMgr']['FormClassMapping']['ClassNumSelectWarning']?>';
		document.getElementById("ClassNumberMethodWarningRow").style.display = '';
	}
	else {
		document.getElementById("ClassNumberMethodWarningRow").style.display = 'none';
		document.getElementById("ClassNumberMethodWarningLayer").innerHTML = '';
	}
	
	// checking functions
	Check_Class_Title_Empty(ClassTitleEN);
	Check_Name_Quote();
	
	var ClassTitleENWarning = document.getElementById("ClassTitleENWarningLayer").innerHTML;
	var ClassTitleB5Warning = document.getElementById("ClassTitleB5WarningLayer").innerHTML;
	var ClassNumberWarning = document.getElementById("ClassNumberMethodWarningLayer").innerHTML;
	var YearClassID = document.getElementById('YearClassID').value;
	
	if (ClassTitleENWarning == "" && ClassTitleB5Warning == "" && ClassNumberWarning == "") {
		document.getElementById('ClassSubmit').disabled = true; // disable submit button
		var PostString = Get_Form_Values(document.getElementById("ClassForm"));
		
		EditClassAjax = GetXmlHttpObject();
		   
	  if (EditClassAjax == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
		allTeacherList = document.getElementById('teacher_benefit');
		//alert(allTeacherList.id);
		if (allTeacherList != null){
			numberOfTeacher = document.getElementById('noClasseTeacher').value;
			//alert(allTeacherList.selectedIndex+' '+numberOfTeacher);
			if (allTeacherList.selectedIndex > (parseInt(numberOfTeacher)+4))
				document.getElementById('is_user_import').value = 1;
			//	alert(allTeacherList.selectedIndex+' '+numberOfTeacher);
			PostString = PostString+'&is_user_import='+document.getElementById('is_user_import').value+'&teacher_benefit='+allTeacherList.options[allTeacherList.selectedIndex].value;			
		}
		
		
	  var url = 'ajax_edit_class.php';
	  var PoseValue = PostString;
		EditClassAjax.onreadystatechange = function() {
			if (EditClassAjax.readyState == 4) {
				Get_Return_Message(Trim(EditClassAjax.responseText));
				Get_Class_Info(YearClassID);
				try {
				    window.top.tb_remove();
				}
				catch(err) {
				    document.getElementById('TB_closeWindowButton').click();
				}				
			}
		};
	  EditClassAjax.open("POST", url, true);
		EditClassAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		EditClassAjax.send(PoseValue);
	}
	else {
		// error 
	}
}

function Remove_Class(YearClassID,AcademicYearID) {
	if (confirm('<?=$Lang['SysMgr']['FormClassMapping']['DeleteClassWarning']?>')) {
		Block_Document();
		
		DeleteClassAjax = GetXmlHttpObject();
		   
	  if (DeleteClassAjax == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'ajax_delete_class.php';
	  var PoseValue = 'YearClassID=' + YearClassID;
		DeleteClassAjax.onreadystatechange = function() {
			if (DeleteClassAjax.readyState == 4) {
				ResponseText = Trim(DeleteClassAjax.responseText);
				UnBlock_Document();
				window.location = "index.php?AcademicYearID="+AcademicYearID+"&ReturnMessage="+encodeURIComponent(ResponseText);
			}
		};
	  DeleteClassAjax.open("POST", url, true);
		DeleteClassAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		DeleteClassAjax.send(PoseValue);
	}
}

function Get_Class_Info(YearClassID) {
	ClassInfoAjax = GetXmlHttpObject();
	   
  if (ClassInfoAjax == null)
  {
    alert (errAjax);
    return;
  } 
    
  var url = 'ajax_get_class_info.php';
  var PoseValue = 'YearClassID='+YearClassID;
	ClassInfoAjax.onreadystatechange = function() {
		if (ClassInfoAjax.readyState == 4) {
			document.getElementById('ClassInfoLayer').innerHTML = ClassInfoAjax.responseText;
			Thick_Box_Init();
			UnBlock_Document();
		}
	};
  ClassInfoAjax.open("POST", url, true);
	ClassInfoAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ClassInfoAjax.send(PoseValue);
}

function Reset_Class_Number() {
	var WarningObj = Browser_Proof_Get_Elements_By_Name('ClassNumberWarning[]','span');
	var Problem = false;
	
	for (var i=0; i < WarningObj.length; i++) {
		if (Trim(WarningObj[i].innerHTML) != "") {
			Problem = true;
		}
	}
	
	// call ajax update
	if (!Problem) {
		ClassNumberAjax = GetXmlHttpObject();
	   
	  if (ClassNumberAjax == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	  
	  Block_Document();
	  
	  var YearClassID = Get_Selection_Value('YearClassSelectShortCut','String');
	  var url = 'ajax_edit_class_number.php';
	  var PostValue = Get_Form_Values(document.getElementById('ClassNumberForm'));
	  PostValue += '&YearClassID='+YearClassID;
	  
		ClassNumberAjax.onreadystatechange = function() {
			if (ClassNumberAjax.readyState == 4) {
				Get_Return_Message(Trim(ClassNumberAjax.responseText));
				Get_Class_Info(YearClassID);
			}
		};
	
	  ClassNumberAjax.open("POST", url, true);
		ClassNumberAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ClassNumberAjax.send(PostValue);
	}
	else {
		return false;
	}
}
}

// MISC
{
function Go_Subject_Detail(YearClassID) {
	window.location = "subject_detail.php?YearClassID="+YearClassID;
}

var WarningImage = '<img title="<?=$Lang['SysMgr']['FormClassMapping']['ClassNumberWarning']?>" src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/school_setting/icon_alert.gif">';
function Check_Class_Number(UserID,ClassNumber) {
	if (IsClassNumberFormat(ClassNumber)) {
		var ClassNumberObj = Browser_Proof_Get_Elements_By_Name('ClassNumber[]','input');
		var WarningObj = Browser_Proof_Get_Elements_By_Name('ClassNumberWarning[]','span');
		var Problem = new Array();
		var NoProblem = new Array();
		for (var i=0; i < ClassNumberObj.length; i++) {
			WarningObj[i].innerHTML = '';
			for (var j=(i+1); j < ClassNumberObj.length; j++) {
				if (ClassNumberObj[i].value == ClassNumberObj[j].value) {
					Problem.push(i);
					Problem.push(j);
				}
			}
		}
		
		for (var i=0; i < Problem.length; i++) {
			WarningObj[Problem[i]].innerHTML = WarningImage;
		}
	}
	else {
		$('span#'+UserID+'Warning').html(WarningImage);
	}
}

function IsClassNumberFormat(sText)
{
	var ValidChars = "0123456789";
	var Char;

	if ((sText.length > 1 && sText.charAt(0) == "0") || sText.length == 0) {
		return false;
	}
  else {
		for (i = 0; i < sText.length; i++) {
			Char = sText.charAt(i);
			if (ValidChars.indexOf(Char) == -1)
			{
			    return false;
			}
		}
   	return true;
	}
}
}
</script>