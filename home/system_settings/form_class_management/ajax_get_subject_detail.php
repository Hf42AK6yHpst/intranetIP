<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_opendb();

$YearClassID = $_REQUEST['YearClassID'];

$YearClass = new year_class($YearClassID,false,false,true);

$FormClassUI = new form_class_manage_ui();
$Json = new JSON_obj();

echo $FormClassUI->Get_Subject_Taken_Detail($YearClass, $YearTermID);
intranet_closedb();
?>