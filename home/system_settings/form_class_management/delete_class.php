<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$YearClassID = $_REQUEST['YearClassID'];

$fcm = new form_class_manage();

$fcm->Start_Trans();

$Result = $fcm->Delete_Class($YearClassID);

if (in_array(false,$Result)) {
	echo $Lang['SysMgr']['FormClassMapping']['ClassDeleteUnsuccess'];
	$fcm->RollBack_Trans();
}
else {
	echo $Lang['SysMgr']['FormClassMapping']['ClassDeleteSuccess'];
	$fcm->Commit_Trans();
}
intranet_closedb();
?>