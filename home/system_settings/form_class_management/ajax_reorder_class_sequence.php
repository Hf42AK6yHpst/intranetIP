<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$RecordOrder = stripslashes(urldecode($_REQUEST['RecordOrder']));

$fcm = new form_class_manage();

$fcm->Reorder_Class_Sequence($RecordOrder);

intranet_closedb();
die;
?>