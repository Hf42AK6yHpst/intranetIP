<?php
// using : 

############# Change Log [Start] ################
# Date: 2017-08-10 Carlos - Cater empty $YearTermID by guessing the year term from year class.
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$YearClassID = $_REQUEST['YearClassID'];

$YearClass = new year_class($YearClassID,false,false,true);
$linterface = new interface_html();

$CurrentPageArr['Class'] = 1;

### Title ###
$title = $Lang['SysMgr']['FormClassMapping']['SubjectTakenPageTitle'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ['title'] = $Lang['SysMgr']['FormClassMapping']['ClassPageTitle'];
$linterface->LAYOUT_START(); 

$FormClassUI = new form_class_manage_ui();
$FormClass = new form_class_manage();
$Json = new JSON_obj();

if($YearTermID=="") {
	//$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID='".$YearClass->AcademicYearID."' ORDER BY TermStart";	
	//$ytData = $FormClass->returnVector($sql);
	//if(count($ytData)==0)
	//{
	//	$YearTermID = GetCurrentSemesterID();
	//}else{
	//	$YearTermID = $ytData[0];
	//}
	$allSemesters = getSemesters($YearClass->AcademicYearID,0);
	if(count($allSemesters)>0){
		$YearTermID = $allSemesters[0]['YearTermID'];
	}else{
		$YearTermID = GetCurrentSemesterID();
	}
}

echo $linterface->Include_JS_CSS();
echo $FormClassUI->Get_Subject_Taken_Detail($YearClass, $YearTermID);
?>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->
<?
$linterface->LAYOUT_STOP();

intranet_closedb();
?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/superTables.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.superTable.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/superTables.js"></script>

<script>
$(document).ready( function() {	
	js_Init_SuperTable();
});


// ajax function 
{
function Get_Subject_Taken_Detail() {
	SubjectTakenAjax = GetXmlHttpObject();
	   
  if (SubjectTakenAjax == null)
  {
    alert (errAjax);
    return;
  } 
    
  var url = 'ajax_get_subject_detail.php';
  
  var PoseValue = 'YearClassID='+document.getElementById('YearClassSelect').value;
  PoseValue += '&YearTermID='+document.getElementById('YearTermID').value;
  
	SubjectTakenAjax.onreadystatechange = function() {
		if (SubjectTakenAjax.readyState == 4) {
			//document.getElementById('tempdiv').innerHTML = SubjectTakenAjax.responseText;
			document.getElementById('SubjectTakenDetailLayer').innerHTML = SubjectTakenAjax.responseText;
			Thick_Box_Init();
			js_Init_SuperTable();
		}
	};
  SubjectTakenAjax.open("POST", url, true);
	SubjectTakenAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	SubjectTakenAjax.send(PoseValue);
}

function Update_Class_Subject_Detail() {
	
	ClassSubjectAjax = GetXmlHttpObject();
	   
	if (ClassSubjectAjax == null)
	{
		alert (errAjax);
		return;
	} 
	
	Block_Element('StudentStudySubjectGroupTable_box');
	
	self.location.href = '#';    
	var url = 'ajax_update_class_subject_detail.php';
	var PoseValue = Get_Form_Values(document.getElementById("Form_Class_Subject"));
	ClassSubjectAjax.onreadystatechange = function() {
		if (ClassSubjectAjax.readyState == 4) {
			//alert(ClassSubjectAjax.responseText);
			Get_Return_Message(ClassSubjectAjax.responseText);
			Thick_Box_Init();
			UnBlock_Element('StudentStudySubjectGroupTable_box');
		}
	};
	ClassSubjectAjax.open("POST", url, true);
	ClassSubjectAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ClassSubjectAjax.send(PoseValue);
}
}	

{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}

function applyToAll(subject, selectedSubjectid, studentCount) {
	
	var obj = document.Form_Class_Subject;
	
	for(var i=0; i<studentCount; i++) {
		var len = document.getElementById('SubjectGroup_'+subject+'_'+i).length;
		
		for(var j=0; j<len; j++) {
			if(document.getElementById('SubjectGroup_'+subject+'_'+i).options[j].value==selectedSubjectid) {
				document.getElementById('SubjectGroup_'+subject+'_'+i).selectedIndex = j;	
			}
		}
		//document.getElementById('SubjectGroup_'+subject+'_'+i).selectedIndex = 1;
	}
}

function js_Apply_To_All_SuperTable(jsSubjectID) {
	var jsCounter = 0;
	$('select.GlobalSubjectGroupIDChk_' + jsSubjectID).each(function(){
		jsCounter++;
		
		if (jsCounter == 2)	{
			// SuperTable will clone table and therefore there will be more than 1 apply to all selection
			$('select.SubjectGroupIDChk_' + jsSubjectID + ':enabled').val($(this).val());
		}
	});
}

function js_Init_SuperTable()
{
	var jsScreenWidth = parseInt(Get_Screen_Width());
	var jsTableWidth = jsScreenWidth - 120;

	var jsFixedCol = 2;
	if ($("table#StudentStudySubjectGroupTable").find("th").length > jsFixedCol) {
		$("table#StudentStudySubjectGroupTable").toSuperTable({
	    	width: jsTableWidth + "px", 
	    	height: "500px", 
	    	fixedCols: 2,
	    	headerRows :1,
	    	onFinish: function(){ fixSuperTableHeight('StudentStudySubjectGroupTable', 400);  $(".sSky").css("font-size","0.9em");}
		});
	}    
}
</script>