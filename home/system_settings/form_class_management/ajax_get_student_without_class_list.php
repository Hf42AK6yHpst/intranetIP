<?php
// using 
/*
 * 2018-01-31 Carlos: Modified form_class_manage_ui->Get_Student_Without_Class_Selection(), added parameter $IncludeYearClassDeselectedStudents 
 * 					  to find out the students that are removed from the class in UI but not updated DB yet. They are also counted as students without class.
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

intranet_opendb();

$AcademicYearID = $_REQUEST['AcademicYearID'];

$ExcludeStudentList = $_REQUEST['ExcludeStudentList'];
$ExcludeStudentArr = array();
if ($ExcludeStudentList != '')
	$ExcludeStudentArr = explode(',', $ExcludeStudentList);
	
$FormClassManageUI = new form_class_manage_ui();
echo $FormClassManageUI->Get_Student_Without_Class_Selection('ClassStudent', $ExcludeStudentArr, $AcademicYearID, $IncludeYearClassDeselectedStudents=true, $YearClassID);

intranet_closedb();
?>