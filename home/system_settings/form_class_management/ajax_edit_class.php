<?php
// Using: 

############# Change Log [Start] ################
#
#   Date:   2018-11-16 (Cameron) 
#           - add BuildingID for eSchoolBus
#
#   Date:   2018-08-29 (Bill)   [2017-1207-0956-53277]
#           - Support Curriculum
#
############# Change Log [End] ################

@SET_TIME_LIMIT(21600);

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$YearClassID = $_REQUEST['YearClassID'];
$ClassTitleEN = trim(stripslashes(urldecode($_REQUEST['ClassTitleEN'])));
$ClassTitleB5 = trim(stripslashes(urldecode($_REQUEST['ClassTitleB5'])));
$ClassTitleB5 = ($ClassTitleB5?$ClassTitleB5:$ClassTitleEN);
$StudentSelected = (is_array($_REQUEST['StudentSelected']))? $_REQUEST['StudentSelected']: array();
$SelectedClassTeacher = (is_array($_REQUEST['SelectedClassTeacher']))? $_REQUEST['SelectedClassTeacher']: array();
$ClassNumberMethod = $_REQUEST['ClassNumberMethod'];
$WEBSAMSCode = trim(stripslashes(urldecode($_REQUEST['WEBSAMSCode'])));
$ClassGroupID = $_REQUEST['ClassGroupID'];

// [2017-1207-0956-53277]
$CurriculumID = '';
if($sys_custom['iPf']['CurriculumsSettings']) {
    $CurriculumID = $_REQUEST['CurriculumID'];
}

$BuildingID = $plugin['eSchoolBus'] ? $_POST['BuildingID'] : '';


$fcm = new form_class_manage();
$fcm->Start_Trans();

// debug_r($_REQUEST);

##### get DeletedUserList before Edit_Class ########
$sql = "select b.UserEmail from YEAR_CLASS_USER as a 
          INNER JOIN INTRANET_USER as b ON a.UserID = b.UserID  
        where a.YearClassID = '".$YearClassID."' ";
if (sizeof($StudentSelected) > 0) 
{
	$sql .= " and b.UserID NOT IN (".implode(',',$StudentSelected).")";
}
$DeletedUserList = $fcm->returnVector($sql);
####################################################
	
$Result = $fcm->Edit_Class($YearClassID,$ClassTitleEN,$ClassTitleB5,$StudentSelected,$SelectedClassTeacher,$ClassNumberMethod,$WEBSAMSCode,$teacher_benefit,$is_user_import,$ClassGroupID,$CurriculumID,$BuildingID);

## Update Student class_number in eclass
if(!in_array(false,$Result))
{
    $lc = new libeclass();
    if(sizeof($StudentSelected) > 0)
    {
        $sql = "SELECT UserEmail, ClassName, ClassNumber FROM INTRANET_USER WHERE UserID IN (".implode(',',$StudentSelected).") ";
        $row = $fcm->returnArray($sql);
        for($i=0; $i < sizeof($row); $i++)
        {
            list($email,$class_name,$class_number) = $row[$i];
            if(trim($class_name) != '') {
                $lc->eClassUserUpdateClassNameIP($email,$class_name,$class_number);
            }
        }
    }
    
    ## Update Student eclass's class_number to empty if in Delete UserList (IP25)
    for($i=0; $i < sizeof($DeletedUserList); $i++)
    {
        $email = $DeletedUserList[$i];
        $lc->eClassUserUpdateClassNameIP($email);
    }
}

/*echo '<pre>';
var_dump($Result);
echo '</pre>';*/
if (in_array(false,$Result)) {
	echo $Lang['SysMgr']['FormClassMapping']['ClassUpdateUnsuccess'];
	$fcm->RollBack_Trans();
} else {
	echo $Lang['SysMgr']['FormClassMapping']['ClassUpdateSuccess'];
	$fcm->Commit_Trans();
}

intranet_closedb();
?>