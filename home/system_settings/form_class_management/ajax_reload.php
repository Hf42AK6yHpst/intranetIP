<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

intranet_opendb();

$Action = trim(urldecode(stripslashes($_REQUEST['Action'])));
$libFCM_ui = new form_class_manage_ui();

if ($Action == 'Form_Stage_Settings_Layer')
{
	echo $libFCM_ui->Get_Form_Stage_Info_Layer();
}
else if ($Action == 'Form_Stage_Settings_Table')
{
	echo $libFCM_ui->Get_Form_Stage_Info_Layer_Table();
}
else if ($Action == 'Get_Form_Stage_JSON_Data')
{
	include_once($PATH_WRT_ROOT."includes/json.php");
	include_once($PATH_WRT_ROOT."includes/libformstage.php");
	
	$libFormStage = new Form_Stage();
	$Json = new JSON_obj();
	
	$FormStageInfoArr = $libFormStage->Get_All_Form_Stage($active=1);
	$FormStageAssoArr = BuildMultiKeyAssoc($FormStageInfoArr, 'FormStageID', array(Get_Lang_Selection('TitleCh', 'TitleEn')), $SingleValue=1);
	$FormStage['0'] = "NA";
	$FormStage = $FormStage + $FormStageAssoArr;
	$FormStage['selected'] = "";
	$FormStage = $Json->encode($FormStage);
	
	echo $FormStage;
}
else if ($Action == 'Get_Form_Stage_Selection_JS')
{
	include_once($PATH_WRT_ROOT."includes/libformstage.php");
	$libFormStage = new Form_Stage();
	
	$jsFormStageSelection = $libFormStage->Get_Form_Stage_Selection('FormStageID');
	$jsFormStageSelection = str_replace('"', '\"', $jsFormStageSelection);
	$jsFormStageSelection = str_replace("\n", "", $jsFormStageSelection);
	
	echo $jsFormStageSelection;
}
	
intranet_closedb();

?>