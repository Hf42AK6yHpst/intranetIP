<?
// Modifying by: 

### Change Log [Start] ###
/*
 *  Date    :   2018-08-29 (Bill) support Curriculum    [2017-1207-0956-53277]
 * 
 *  Date	:	2016-01-19 (Carlos) added arguments $copyStudents and $fromYearClassID to $fcm->Create_ClassOnly() 
 * 
 *	Date	:	2010-07-12 (Henry Chow)
 *	Detail	:	redirect to index page with selection of "copy to year" and display the message 
 *
 */
### Change Log [End] ###

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$ldb = new libdb();
$fcm = new form_class_manage();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$sql = "SELECT YearID, ClassTitleEN, ClassTitleB5, Sequence, WEBSAMSCode, ClassGroupID, YearClassID, CurriculumID FROM YEAR_CLASS WHERE AcademicYearID = '$fromYear' ORDER BY Sequence";
$class = $ldb->returnArray($sql);
for($i=0; $i<sizeof($class); $i++) {
    list($year_id, $titleEN, $titleB5, $sequence, $webSamsCode, $clsGroupID, $fromYearClassID, $CurriculumID) = $class[$i];
    $fcm->Create_ClassOnly($toYear, $year_id, $titleEN, $titleB5, $webSamsCode, $clsGroupID, $copyStudents, $fromYearClassID, $CurriculumID);
}

intranet_closedb();

header("Location: index.php?AcademicYearID=".$toYear."&ReturnMessage=".$Lang['SysMgr']['FormClassMapping']['ClassCopySuccessfully']);
?>