<?php
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//intranet_auth();
intranet_opendb();

$libcalevent = new libcalevent2007();

$calandarTable = $libcalevent->Display_Calendar_Array($month, $year);

$calandarTable .= "
<script type=\"text/javascript\">
	$(document).ready(function() 
	{
		$(\"table.bookable\").hover(function() {
			var currentTableID = $(this).attr('id');
			$(this.cells[1]).addClass('showDragHandle').click( function() {
				alert(\"currentTableID = \" + currentTableID);
			});
			
		}, function() {
			var currentTableID = $(this).attr('id');
			$(this.cells[1]).removeClass('showDragHandle').unbind('click');
	
		});
	});
</script>
";

echo $calandarTable;
intranet_closedb();
?>