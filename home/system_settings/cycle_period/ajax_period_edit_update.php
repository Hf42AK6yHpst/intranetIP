<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");

intranet_opendb();

$li = new libdb();
$lcycleperiods	= new libcycleperiods();
			
			
$ts_start = strtotime($PeriodStart);
$ts_end = strtotime($PeriodEnd);
if ($ts_end < $ts_start){
    $li->RollBack_Trans();
	echo $Lang['General']['ReturnMessage']['AddUnsuccess'];
	die();
}else{
	# Check date range is in the selected school year or not #
	$sql = "SELECT DISTINCT AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE (UNIX_TIMESTAMP(TermStart) <= $ts_start) AND AcademicYearID = $SchoolYearID";
	$arrExistRange1 = $li->returnVector($sql);
	$sql = "SELECT DISTINCT AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE (UNIX_TIMESTAMP(TermEnd) >= $ts_end) AND AcademicYearID = $SchoolYearID";
	$arrExistRange2 = $li->returnVector($sql);				

	if($arrExistRange1[0] == '' || $arrExistRange2[0] == ''){
		$li->RollBack_Trans();
		echo -1;
		die();
	}else{
		# Check any overlap date range #
		/*
		$sql = "SELECT PeriodID FROM INTRANET_CYCLE_GENERATION_PERIOD
	               WHERE ('$ts_start' BETWEEN UNIX_TIMESTAMP(PeriodStart) AND UNIX_TIMESTAMP(PeriodEnd)
	                      OR '$ts_end' BETWEEN UNIX_TIMESTAMP(PeriodStart) AND UNIX_TIMESTAMP(PeriodEnd)
	                     ) AND PeriodID != $PeriodID";
		$temp = $li->returnVector($sql);
		*/
		$sql = "SELECT PeriodStart, PeriodEnd FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE PeriodID != $PeriodID";
		$arrResult = $li->returnArray($sql,2);
		
		$period_overlapped = 0;		## init a overlap checking count
		
		if(sizeof($arrResult)>0){
			for($i=0; $i<sizeof($arrResult); $i++){
				list($existPeriodStart, $existPeriodEnd) = $arrResult[$i];
				
				if((($PeriodStart <= $existPeriodStart) && ($existPeriodStart <= $PeriodEnd)) && (($PeriodStart <= $existPeriodEnd ) && ($existPeriodEnd <= $PeriodEnd))){
					$period_overlapped = 1;
				}
				if((($existPeriodStart <= $PeriodStart) && ($PeriodStart <= $existPeriodEnd)) && (($PeriodStart <= $existPeriodEnd) && ($existPeriodEnd <= $PeriodEnd))){
					$period_overlapped = 1;
				}
				if((($PeriodStart <= $existPeriodStart) && ($existPeriodStart <= $PeriodEnd)) && (($existPeriodStart <= $PeriodEnd) && ($PeriodEnd <= $existPeriodEnd))){
					$period_overlapped = 1;
				}
				if((($existPeriodStart <= $PeriodStart) && ($PeriodStart <= $existPeriodEnd)) && (($existPeriodStart <= $PeriodEnd)&&($PeriodEnd <= $existPeriodEnd))){
					$period_overlapped = 1;
				}
			}
		}
		
		if ($period_overlapped == 1){
			$li->RollBack_Trans();
			echo 0;
			die();
		}else{
			$li->Start_Trans();
						
			$sql = "UPDATE 
							INTRANET_CYCLE_GENERATION_PERIOD 
					SET 
							PeriodStart = '$PeriodStart',
							PeriodEnd = '$PeriodEnd',
							CycleType = '$CycleType',
							PeriodDays = '$PeriodDays', 
							FirstDay = '$FirstDay', 
							SaturdayCounted = '$SatCount', 
							DateModified = NOW()
					WHERE
							PeriodID = $PeriodID";
			$result['EditPeriod'] = $li->db_db_query($sql);
	
			if (in_array(false,$result)) {
				$li->RollBack_Trans();
				echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
			}
			else {
				$li->Commit_Trans();
				echo $Lang['General']['ReturnMessage']['UpdateSuccess'];
			}
		}
	}
}

# Clear Preview table
$lcycleperiods->clearPreview();

# Get Periods
$periods = $lcycleperiods->returnPeriods_NEW();

# Handle each period
for ($i=0; $i<sizeof($periods); $i++)
{
     list($PeriodID, $PeriodStart, $PeriodEnd, $PeriodType, $CycleType, $PeriodDays, $FirstDay, $SaturdayCounted) = $periods[$i];
     if ($PeriodDays < 1) continue;
     if ($PeriodType == 1)         # Calculation
     {
         if ($CycleType == 1)
         {
             $txtArray = $lcycleperiods->array_alphabet;
         }
         else if ($CycleType == 2)
         {
              $txtArray = $lcycleperiods->array_roman;
         }
         else
         {
             $txtArray = $lcycleperiods->array_numeric;
         }

         # Get Non-Cycle Days
         $temp = $lcycleperiods->getNonCycleDays($PeriodStart,$PeriodEnd);
         for ($j=0; $j<sizeof($temp); $j++)
         {
              $ts = strtotime($temp[$j]);
              $non_cycle_days[$ts] = "1";
         }
         $ts_start = strtotime($PeriodStart);
         $ts_end = strtotime($PeriodEnd);
         $ts_current = $ts_start;
         $current_weekday = date("w",$ts_current);
         $current_cycleday = $FirstDay;
         $values = "";
         $delim = "";
         while ($ts_current <= $ts_end)
         {
                #$recordDate = date("Y-m-d",$ts_current);

                # Check is non-cycle day
                if ($non_cycle_days[$ts_current]==1) # Skip
                {
                    #echo "$recordDate -> Non cycle<br>\n";
                    $current_weekday = ($current_weekday+1)%7;
                    $ts_current += 86400;  # 1 Day
                    continue;
                }
                # Check is weekend
                if ($current_weekday==0 || ($current_weekday==6 && $SaturdayCounted!=1))
                {
                    #echo "$recordDate -> Weekend<br>\n";
                    $ts_current += 86400;  # 1 Day
                    $current_weekday = ($current_weekday+1)%7;
                    continue;
                }
                # Put in Database
                $recordDate = date("Y-m-d",$ts_current);
                $txtCycle = $txtArray[$current_cycleday];
//echo $i_CycleNew_Prefix_Chi."<br />";                
                $values .= "$delim ('$recordDate','$i_CycleNew_Prefix_Eng$txtCycle','$i_CycleNew_Prefix_Chi$txtCycle','$txtCycle')";
                $delim = ",";

                # Next iteration
                $current_weekday = ($current_weekday+1)%7;
                $current_cycleday = ($current_cycleday+1)%$PeriodDays;
                $ts_current += 86400;  # 1 Day
         }
         $sql = "INSERT INTO INTRANET_CYCLE_TEMP_DAYS_VIEW (RecordDate, TextEng, TextChi, TextShort)
                        VALUES $values";
//echo $sql."<br />";                        
         $lcycleperiods->db_db_query($sql);
     }
     else if ($PeriodType == 2)      # File import
     {
          $sql = "INSERT IGNORE INTO INTRANET_CYCLE_TEMP_DAYS_VIEW (RecordDate,TextEng,TextChi,TextShort)
                         SELECT RecordDate, TextEng, TextChi, TextShort FROM INTRANET_CYCLE_IMPORT_RECORD
                                WHERE RecordDate >= '$PeriodStart' AND RecordDate <= '$PeriodEnd'";
          $lcycleperiods->db_db_query($sql);
     }
     else
     {
         # Nothing to do
     }
}
intranet_closedb();
?>