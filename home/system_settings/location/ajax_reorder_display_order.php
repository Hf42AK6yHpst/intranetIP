<?php
// using
/**
 * Date : 2018-08-15 (Henry)
 * fixed XSS
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");

intranet_opendb();

$RecordType = stripslashes($_REQUEST['RecordType']);
$RecordType = cleanHtmlJavascript($RecordType);
$DisplayOrderString = stripslashes(urldecode($_REQUEST['DisplayOrderString']));
$DisplayOrderString = cleanHtmlJavascript($DisplayOrderString);

echo $RecordType;

if ($RecordType == "Building")
{
	$liblocation = new Building();
	$displayOrderArr = $liblocation->Get_Update_Display_Order_Arr($DisplayOrderString, ",", "BuildingRow_");
}
else if ($RecordType == "Floor")
{
	$liblocation = new Floor();
	$displayOrderArr = $liblocation->Get_Update_Display_Order_Arr($DisplayOrderString, ",", "FloorRow_");
}
else if ($RecordType == "Room")
{
	$liblocation = new Room();
	$displayOrderArr = $liblocation->Get_Update_Display_Order_Arr($DisplayOrderString, ",", "RoomRow_");
}
else
{
	$success = false;
}

# Insert Record
if ($liblocation != NULL)
{
	$success = $liblocation->Update_DisplayOrder($displayOrderArr);
}

echo $success;
intranet_closedb();

?>