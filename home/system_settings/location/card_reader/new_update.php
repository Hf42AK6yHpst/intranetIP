<?php
// using : 
############# Change Log [Start] ################
#
#
# 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."addon/check.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/liblocation_cardReader.php");
//intranet_auth();
intranet_opendb();
$extraCardReaderObj = new liblocation_cardReader();

$lebooking_ui	= new libebooking_ui();
$libdb = new libdb();

//$linterface->hasAccessRight();

### Title ###
$MODULE_OBJ['title'] = $Lang['SysMgr']['Location']['Location'];
$TAGS_OBJ[] = array($Lang['SysMgr']['Location']['LocationList'], "");

 
## Get parameters from form
$LocationID = $_POST['locationIDSelectionBox'];
$Code = $_POST['ReaderCode'];
$ReportNameEng = $_POST['ReportNameEng'];
$ReportNameChi = $_POST['ReportNameChi'];
$Remark = $_POST['ReaderRemarks'];
$Status = $_POST['CardReaderStatus'];

$successAry = array();
$libdb->Start_Trans();

### Save Location

$extraCardReaderObj->setLocationId($LocationID);

### Save Code

$extraCardReaderObj->setCode($Code);

### Save Reader Name
$extraCardReaderObj->setNameEng($ReportNameEng);
$extraCardReaderObj->setNameChi($ReportNameChi);

### Save Remarks
$extraCardReaderObj->setRemarks($Remark);

### Save Status
$extraCardReaderObj->setRecordStatus($Status);
$extraCardReaderObj->setIsDeleted($locationConfigAry['INVENTORY_LOCATION_CARD_READER']['IsDeleted']['notDeleted']);
$extraCardReaderObj->setModifiedDate('now()');
$extraCardReaderObj->setModifiedBy($_SESSION['UserID']);

$extraCardReaderObj->save();

if (in_array(false, $successAry)) {
	$libdb->Rollback_Trans();
	$returnMsgKey = 'UpdateUnSuccess';
}
else {
	$libdb->Commit_Trans();
	$returnMsgKey = 'UpdateSuccess';
}

intranet_closedb();
header("Location: index.php?returnMsgKey=$returnMsgKey");
?>