<?php
// using : 
############# Change Log [Start] ################
#
#
# 
############# Change Log [End] ################
### Cookies handling
# set cookies
$arrCookies = array();

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liblocation_cardReader.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
//intranet_auth();

intranet_opendb();

$libuser = new libuser($_SESSION['UserID']); 
if ($libuser->UserLogin != 'broadlearning') {
	include_once($PATH_WRT_ROOT."addon/check.php");
}


$arrCookies = array();
$arrCookies[] = array("readerInfo_page_size", "numPerPage");
$arrCookies[] = array("readerInfo_field", "field");	
$arrCookies[] = array("readerInfo_order", "order");
$arrCookies[] = array("readerInfo_pageNo", "pageNo");
$arrCookies[] = array("readerInfo_Keyword", "Keyword");
$arrCookies[] = array("readerInfo_status", "status");
$arrCookies[] = array("readerInfo_Location", "LocationID");

//$arrCookies[] = array("ReaderLocationID", "ReaderLocationID");

$LocationID = (isset($_POST['LocationID']))? $_POST['LocationID'] : $LocationID;
$AccessRight = 'Internal';

if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
	$status = '';
}
else {
	updateGetCookies($arrCookies);
}

$linterface = new interface_html("popup.html");
$libdb 	= new libdb();
$lebooking 	= new libebooking();
$lebooking_ui= new libebooking_ui();
$extraCardReaderObj = new liblocation_cardReader();

### Title ###
$MODULE_OBJ['title'] = $Lang['SysMgr']['Location']['CardReaderArr']['CardReader'];
$TAGS_OBJ[] = array($Lang['SysMgr']['Location']['CardReader']['ReaderInfo'], "");

### Return Message ###
$ReturnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$returnMsgKey = $_GET['returnMsgKey'];
$linterface->LAYOUT_START($ReturnMsg); 

### Toolbar
$htmlAry['toolbar']  = $linterface->Get_Content_Tool_v30('new', $href="edit.php", $Lang['Btn']['New'], $options=array(), $other="", $divID='', $extra_class='', $hideDivAfterClicked=true, $extra_onclick='');
//htmlAry['toolbar']  = "<a href=\"". ($libenroll->disableUpdate==1 ? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')": "index_new.php") ."\" class=\"new\"> ".$Lang['Btn']['New']."</a>";
$htmlAry['toolbar'] .= "<br />";

##Filtering
$locationSelectionDisplay ="";
$locationAry = $lebooking_ui->getAllBookableLocationName();
$htmlAry['toolbar2'] = $lebooking_ui->Get_Booking_Location_Selection('LocationID', $LocationID, ' onchange="document.form1.submit()" ',0 , 1);
$htmlAry['toolbar2'] .= $extraCardReaderObj->Get_Status_Selection('status', $status, ' onchange="document.form1.submit()" ');
### Search box
$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('Keyword', $Keyword);

### DB table action button
$TableBtnAry = array();
$TableBtnAry[] = array('approve', 'javascript:enableReaderStatus();', $Lang['Btn']['Enable']);
$TableBtnAry[] = array('reject', 'javascript:disableReaderStatus();', $Lang['Btn']['Disable']);
$TableBtnAry[] = array('edit', 'javascript:goEdit();');
$TableBtnAry[] = array('delete', 'javascript:goDelete();');
$htmlAry['dbTableBtn'] = $linterface->Get_DBTable_Action_Button_IP25($TableBtnAry);
//debug_pr($LocationID);

### Display DB Table
//$htmlAry['dbTable'] = $extraCardReaderObj->getExtraInfoDBTable($field, $order, $pageNo, $Keyword, $LocationID, $status);

$htmlAry['dbTable'] = $extraCardReaderObj->geteBookingSettingDoorAccessDBTable($field, $order, $pageNo, $Keyword, $LocationID, $status, $AccessRight);

?>


<script language="javascript">

function goEdit() {
	checkEdit(document.getElementById('form1'), 'ReaderID[]', 'edit.php');
}

function disableReaderStatus() {
	var disable = "<?= $Lang['SysMgr']['Location']['CardReader']['Confirm']['Disable'] ?>";
	checkEditMultiple2(document.getElementById('form1'), 'ReaderID[]', 'submitFormConfirm(\'disable\');');
}

function enableReaderStatus() {
	var enable = "<?= $Lang['SysMgr']['Location']['CardReader']['Confirm']['Enable']  ?>";
	checkEditMultiple2(document.getElementById('form1'), 'ReaderID[]', 'submitFormConfirm(\'enable\');');
	
}

function goDelete() {
	checkRemove2(document.getElementById('form1'), 'ReaderID[]', 'submitForm(\'delete\');');
}


function submitFormConfirm(actionType) {
	var warningMsg = '';
	if (actionType == 'disable') {
		warningMsg = '<?= $Lang['SysMgr']['Location']['CardReader']['Confirm']['Disable'] ?>';
	}
	else if (actionType == 'enable') {
		warningMsg = '<?= $Lang['SysMgr']['Location']['CardReader']['Confirm']['Enable'] ?>';
	}
	
	if (confirm(warningMsg)) {
		submitForm(actionType);
	}
}

function submitForm(actionType) {
	$('input#actionType').val(actionType);
	$('form#form1').attr('action', 'delete.php').submit();
}


</script>

<form id="form1" name="form1" method="post" action="index.php">

<div class="content_top_tool">
		<div class="Conntent_tool">
	       <?=$htmlAry['toolbar']?>  
		</div>
		<br style="clear:both" />
		<div class="Conntent_tool">
		<?=$htmlAry['toolbar2']?>
		</div>
        <?=$htmlAry['searchBox']?>
		<br style="clear:both" />
	</div>
	
	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom"><?=$htmlAry['filterSignature']?><?=$htmlAry['filterSchoolChop']?></td>
			<td valign="bottom"><div class="common_table_tool"><?=$deleteBtn?></div></td>
		</tr>
		</table>
		<?=$htmlAry['dbTableBtn']?>
		<?=$htmlAry['dbTable']?>
	</div>
	
	<input type="hidden" id="actionType" name="actionType" value="" />


</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script language="javascript">
$(document).ready(function () {
	
});
</script>