<?
// Modifying by:  

############# Change Log [Start] ################
#
#   Date:   2018-11-08 Cameron
#           fix bug: should filter LocationLevelID when get data fromINVENTORY_LOCATION
#
#   Date:   2018-08-09 Cameron
#           add Sub-location Suited Course Category for HKPF
#
#	Date:	2017-09-05	Simon Yu
#			Added: create new record or update record validation
#	Date:	2016-02-06	Kenneth Yau
#			Added: Column "Capacity" for sub-location, modidfied temp table
#	Date:   2016-04-26 Omas
#		    - fix failt to asssign display order to location & location level 
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");

intranet_opendb();

$li = new libdb();
$lc = new liblocation();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


############################################################################
# retrieve data from temp table TEMP_INVENTORY_LOCATION_IMPORT
############################################################################
//$sql = "select * from TEMP_INVENTORY_LOCATION_IMPORT where UserID=$UserID order by Code";

# Start of create location logic
if($actionType == 1){
    $sql = "select * from TEMP_INVENTORY_LOCATION_IMPORT where UserID=$UserID order by TempID";
    $record_result = $li->returnArray($sql);

    foreach($record_result as $k=>$d)
    {
        if ($sys_custom['project']['HKPF']) {
            list($t_TempID,$t_UserID,$t_RowNumber,$t_Code,$t_Barcode,$t_NameEng,$t_NameChi,$t_Sub_Code,$t_Sub_Barcode,$t_Sub_NameEng,$t_Sub_NameChi,$t_Description,$t_Capacity, $t_Category) = $d;
        }
        else {
            list($t_TempID,$t_UserID,$t_RowNumber,$t_Code,$t_Barcode,$t_NameEng,$t_NameChi,$t_Sub_Code,$t_Sub_Barcode,$t_Sub_NameEng,$t_Sub_NameChi,$t_Description,$t_Capacity) = $d;
        }
        
        # check Level Code is exists or not
        # not exists => insert, otherwise, skip
        $sql = "select LocationLevelID from INVENTORY_LOCATION_LEVEL where Code='$t_Code' and BuildingID='$BuildingID' and RecordStatus=1";
        $result = $li->returnVector($sql);
        $LocationLevelID = $result[0];
        
        $lf = new Floor('', $BuildingID);
        ##### Floor
        if(!$LocationLevelID)	# insert
        {
            $DataArr = array();
            $DataArr['Code'] = $t_Code;
            $DataArr['Barcode'] = $t_Barcode;
            $DataArr['NameChi'] = $t_NameChi;
            $DataArr['NameEng'] = $t_NameEng;
            $DataArr['BuildingID'] = $BuildingID;
            // 		$lf->TableName = "INVENTORY_LOCATION_LEVEL";
            $lf->Insert_Record($DataArr);
        }
        
        ##### Retrieve LocationLevelID
        $sql = "select LocationLevelID from INVENTORY_LOCATION_LEVEL where Code='$t_Code' and BuildingID=$BuildingID and RecordStatus=1";
        $result = $li->returnVector($sql);
        $LocationLevelID = $result[0];
        
        ##### Room
        $lr = new Room('', $LocationLevelID);
        // 	$lr->TableName = "INVENTORY_LOCATION";
        $checkExistsLocationID = $lr->Is_Record_Existed($t_Sub_Code,'','','',$LocationLevelID, $BuildingID);
        if($checkExistsLocationID)
        {
            $sql = "delete from INVENTORY_LOCATION where LocationID=$checkExistsLocationID";
            $lr->db_db_query($sql);
        }
        
        $DataArr = array();
        $DataArr['LocationLevelID'] = $LocationLevelID;
        $DataArr['Code'] = $t_Sub_Code;
        $DataArr['Barcode'] = $t_Sub_Barcode;
        $DataArr['NameChi'] = $t_Sub_NameChi;
        $DataArr['NameEng'] = $t_Sub_NameEng;
        $DataArr['Description'] = $t_Description;
        $DataArr['Capacity'] = $t_Capacity;
        $lr->Insert_Record($DataArr);
        
        if ($sys_custom['project']['HKPF']) {
            $locationID = $lr->getRecordID();
            if ($locationID) {
                if($checkExistsLocationID) {
                    $lr->deleteLocationCategory($checkExistsLocationID);
                }
                $lr->deleteLocationCategory($locationID);
                
                $categories = explode('##',$t_Category);
                if (count($categories)) {
                    foreach((array)$categories as $category) {
                        $category = trim($category);
                        if (!empty($category)) {
                            $sql = "SELECT CategoryID FROM INTRANET_ENROL_CATEGORY WHERE CategoryName='".$lr->Get_Safe_Sql_Query($category)."'";
                            $rs = $lr->returnResultSet($sql);
                            if (count($rs)) {
                                $categoryID = $rs[0]['CategoryID'];
                                $lr->addLocationCategory($locationID, $categoryID);
                            }
                        }
                    }
                }
            }            
        }
    }
}

# End of create location logic
//debug_pr($actionType);

# Start of update location logic
if($actionType == 2){
    
    //debug_pr($UserID);
    $sql = "select * from TEMP_INVENTORY_LOCATION_IMPORT where UserID=$UserID order by TempID";
//     debug_pr($sql);
    $record_result = $li->returnArray($sql);
//     debug_pr($record_result);
    
    foreach($record_result as $k=>$d)
    {
        if ($sys_custom['project']['HKPF']) {
            list($t_TempID,$t_UserID,$t_RowNumber,$t_Code,$t_Barcode,$t_NameEng,$t_NameChi,$t_Sub_Code,$t_Sub_Barcode,$t_Sub_NameEng,$t_Sub_NameChi,$t_Description,$t_Capacity, $t_Category) = $d;
        }
        else {
            list($t_TempID,$t_UserID,$t_RowNumber,$t_Code,$t_Barcode,$t_NameEng,$t_NameChi,$t_Sub_Code,$t_Sub_Barcode,$t_Sub_NameEng,$t_Sub_NameChi,$t_Description,$t_Capacity) = $d;
        }
        
        $sql = "select LocationLevelID from INVENTORY_LOCATION_LEVEL where Code='$t_Code' and BuildingID='$BuildingID' and RecordStatus=1";
        
//         debug_pr($sql);
        
        $result = $li->returnVector($sql);
//         debug_pr($result);
        
        $LocationLevelID = $result[0];
        
        ##### Floor
        $lf = new Floor('', $BuildingID);
        
//          debug_pr($LocationLevelID);
//         die();
        if($LocationLevelID){
            $DataArr = array();
            $DataArr['Code'] = $t_Code;
            $DataArr['Barcode'] = $t_Barcode;
            $DataArr['NameChi'] = $t_NameChi;
            $DataArr['NameEng'] = $t_NameEng;
            $DataArr['BuildingID'] = $BuildingID;

            $lf->Insert_Record($DataArr);
        }
       
//        debug_pr($t_Sub_Code);
       # check Level Code is exists for update
       $sql = "select LocationLevelID from INVENTORY_LOCATION where Code='$t_Sub_Code' and RecordStatus=1";
       if ($LocationLevelID) {
           $sql .= " AND LocationLevelID='".$LocationLevelID."'";
       }
//         debug_pr($sql);
       
       $result = $li->returnVector($sql);
       $LocationLevelID = $result[0];
       
//        debug_pr($LocationLevelID);
       ##### Room
       //debug_pr($LocationLevelID);
       $lr = new Room('', $LocationLevelID);
       
//        debug_pr($LocationLevelID);
       
       if($LocationLevelID){
           $DataArr = array();
           $DataArr['LocationLevelID'] = $LocationLevelID;
           $DataArr['Code'] = $t_Sub_Code;
           $DataArr['Barcode'] = $t_Sub_Barcode;
           $DataArr['NameChi'] = $t_Sub_NameChi;
           $DataArr['NameEng'] = $t_Sub_NameEng;
           $DataArr['Description'] = $t_Description;
           $DataArr['Capacity'] = $t_Capacity;
           $lr->Insert_Record($DataArr);
           
           if ($sys_custom['project']['HKPF']) {
               $locationID = $lr->getRecordID();
               if ($locationID) {
                   $lr->deleteLocationCategory($locationID);
                   
                   $categories = explode('##',$t_Category);
                   if (count($categories)) {
                       foreach((array)$categories as $category) {
                           $category = trim($category);
                           if (!empty($category)) {
                               $sql = "SELECT CategoryID FROM INTRANET_ENROL_CATEGORY WHERE CategoryName='".$lr->Get_Safe_Sql_Query($category)."'";
                               $rs = $lr->returnResultSet($sql);
                               if (count($rs)) {
                                   $categoryID = $rs[0]['CategoryID'];
                                   $lr->addLocationCategory($locationID, $categoryID);
                               }
                           }
                       }
                   }
               }
           }
           
       }
       //debug_pr($DataArr);
       
                   
        
       
       //debug_pr($checkExistsLocationID);
//        if($checkExistsLocationID)
//        {
//             $DataArr = array();
//             $DataArr['LocationLevelID'] = $LocationLevelID;
//             $DataArr['Code'] = $t_Sub_Code;
//             $DataArr['Barcode'] = $t_Sub_Barcode;
//             $DataArr['NameChi'] = $t_Sub_NameChi;
//             $DataArr['NameEng'] = $t_Sub_NameEng;
//             $DataArr['Description'] = $t_Description;
//             $DataArr['Capacity'] = $t_Capacity;
//             $lr->Insert_Record($DataArr);
//         }        
    }
}
# End of create location logic

############################################################################
# delete data in Temp table
############################################################################
$sql = "delete from TEMP_INVENTORY_LOCATION_IMPORT where UserID = $UserID";
$li->db_db_query($sql) or die(mysql_error());

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

# Title / Menu
$linterface = new interface_html();
$CurrentPageArr['Location'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['Location']['Location'];
$TAGS_OBJ[] = array($Lang['SysMgr']['Location']['LocationList']);
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]); 

?>


<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center"><?=$x?></td>
</tr>
		
<tr>
	<td>
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		
		<tr>
			<td class='tabletext' align='center'><?=count($record_result)?> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?></td>
		</tr>

		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php?BuildingID=$BuildingID'"); ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
