<?php
// using: 
/**
 * Date: 2018-08-15 (Henry)
 *       fixed sql injection
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$li = new libdb();
$linventory = new libinventory();
$linterface = new interface_html();

$targetLocationLevelID = IntegerSafe($targetLocationLevelID);

$sql = "SELECT LocationID FROM INVENTORY_LOCATION WHERE LocationLevelID IN ($targetLocationLevelID) AND RecordStatus = 1";
$result = $li->returnVector($sql);

//echo $result[0];

if(sizeof($result) > 0){
	$targetLocationID = implode(",",$result);
	echo $Lang['SysMgr']['Location']['FieldTitle']['TotalNumOfSubLocation'].": <span class='num_of_room_".$targetLocationLevelID."'><span class='row_link_tool' style='display:inline-block;'><a href='#' id='zoom_".$targetLocationLevelID."' class='zoom_out' onClick='js_Show_Sub_Location(\"$targetLocationLevelID\",\"$targetLocationID\"); return false;' >".sizeof($result)."</a></span></span>";
}else{
	$targetLocationID = "";
	echo $Lang['SysMgr']['Location']['FieldTitle']['TotalNumOfSubLocation'].": <span class='num_of_room_".$targetLocationLevelID."'>0</span>";
}


intranet_closedb();

?>