<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");

intranet_opendb();

$liblocation_ui = new liblocation_ui();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);

if ($RecordType == "Building")
{
	$returnTable = $liblocation_ui->Get_Building_Add_Edit_Table();
}
else if ($RecordType == "Floor")
{
	$BuildingID = stripslashes($_REQUEST['BuildingID']);
	$returnTable = $liblocation_ui->Get_Floor_Add_Edit_Table($BuildingID);
}
else if ($RecordType == "Room")
{
	$returnTable = "";
}
else
{
	$returnTable = "";
}

echo $returnTable;


intranet_closedb();
?>