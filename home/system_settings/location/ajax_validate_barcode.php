<?php
// using : 
/**
 * Date: 2018-08-15 (Henry)
 *       fixed sql injection
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);
$InputValue = stripslashes($_REQUEST['InputValue']);
$ExcludeID = stripslashes($_REQUEST['TargetID']);
$ExcludeID = IntegerSafe($ExcludeID);
$BuildingID = stripslashes($_REQUEST['BuildingID']);
$LocationLevelID = stripslashes($_REQUEST['LocationLevelID']);

//$pattern = '/^[0-9A-Z\ \.\/\|\$\-]*/'; 	## Original valide Barcode
$pattern = '/^[0-9A-Z\ \.\/\|\$\-]*/'; 	## valide Barcode
preg_match_all($pattern, $InputValue, $result,  PREG_SET_ORDER);

if($result[0][0] == $InputValue){
	$BarcodeValid = 1;
}else{
	$BarcodeValid = 0;
}

# Initialize library
if ($RecordType == "Building")
{
	$libBuilding = new Building();
	$isCodeExisted = $libBuilding->Is_Barcode_Existed($InputValue, $ExcludeID);
}
else if ($RecordType == "Floor")
{
	$libFloor = new Floor();
	$isCodeExisted = $libFloor->Is_Barcode_Existed($InputValue, $ExcludeID, $BuildingID);
}
else if ($RecordType == "Room")
{
	$libRoom = new Room();
	$isCodeExisted = $libRoom->Is_Barcode_Existed($InputValue, $ExcludeID, $BuildingID, $LocationLevelID);
}


if($BarcodeValid == 0)
{
	echo $Lang['SysMgr']['Location']['Warning']['BarcodeNotValid'];
}
else
{
	if ($isCodeExisted == 1)
		echo $Lang['SysMgr']['Location']['Warning']['DuplicatedBarcode'][$RecordType];
	else
		echo "";
}
intranet_closedb();

?>