<?php
// using: 
/**
 * 2018-08-20 (Henry): fixed sql injection 
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);
$InputValue = stripslashes($_REQUEST['InputValue']);
$ExcludeID = IntegerSafe(stripslashes($_REQUEST['TargetID']));
$BuildingID = IntegerSafe(stripslashes($_REQUEST['BuildingID']));
$LocationLevelID = IntegerSafe(stripslashes($_REQUEST['LocationLevelID']));


# Initialize library
if ($RecordType == "Building")
{
	$libBuilding = new Building();
	$isCodeExisted = $libBuilding->Is_Code_Existed($InputValue, $ExcludeID);
}
else if ($RecordType == "Floor")
{
	$libFloor = new Floor();
	$isCodeExisted = $libFloor->Is_Code_Existed($InputValue, $ExcludeID, $BuildingID);
}
else if ($RecordType == "Room")
{
	$libRoom = new Room();
	$isCodeExisted = $libRoom->Is_Code_Existed($InputValue, $ExcludeID, $BuildingID, $LocationLevelID);
}

	
if ($isCodeExisted == 1)
	echo $Lang['SysMgr']['Location']['Warning']['DuplicatedCode'][$RecordType];
else
	echo "";


intranet_closedb();

?>