<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");

intranet_opendb();

# Get data
$BuildingID = $_REQUEST['BuildingID'];

$libBuilding = new Building();
$buildingInfoArr = $libBuilding->Get_All_Building();
$thisBuildingID = $buildingInfoArr[0]['BuildingID'];

if ($BuildingID=="")
{
	# check if adding the 1st building
	$BuildingID = $thisBuildingID;
}

if ($thisBuildingID == '')
{
	# check if deleting the last building
	$BuildingID = '';
}

$liblocation_ui = new liblocation_ui();
echo $liblocation_ui->Get_Content_Table($BuildingID);

intranet_closedb();

?>