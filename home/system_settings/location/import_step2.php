<?php
// Modifying by:  

############# Change Log [Start] ################
#
#   Date:   2018-11-12 Cameron
#           Fixed: cast array to parameter in array_shift function to suppress error
#
#   Date:   2018-08-09 Cameron
#           add Sub-location Suited Course Category for HKPF
#
#	Date:	2018-03-07	Carlos
#			Fixed the checking duplicated sublocation in temp import records.  
#
#	Date:	2017-09-05	Simon Yu
#			Added: Add and update radio buttons and validation process changes
#
#	Date:	2016-02-06	Kenneth Yau
#			Added: Column "Capacity" for sub-location, modidfied temp table 
#
#	Date:	2013-01-03	YatWoon
#			Fixed: incorrect checking field for Sub_Code and Sub_Barcode [Case#2012-1219-0920-35156]
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
// include_once($PATH_WRT_ROOT."includes/liblocation.php");

//debug_pr($_POST);

// action_type => 1, BuildingID => 35

intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$li = new libdb();
$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import_step1.php?BuildingID=$BuildingID&xmsg=ImportUnsuccess");
	exit();
}

$file_format = array("Location Code", "Location Barcode", "Location Name(En)", "Location Name(Chi)", "Sub-location Code", "Sub-location Barcode", "Sub-location Name(En)", "Sub-location Name(Chi)", "Sub-location Description","Sub-location Capacity");
$flagAry = array(1,1,1,1,1,1,1,1,1,1);

if ($sys_custom['project']['HKPF']) {
    $file_format[] = "Sub-location Suited Course Category";
    $flagAry[] = "1";
}

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($csvfile,"","",$file_format,$flagAry);
$data = (array)$data;
$col_name = array_shift($data);
$format_wrong = false;
if(sizeof($file_format) != sizeof($col_name))
	$format_wrong = true;

$numOfData = count($data);
if($format_wrong || $numOfData==0)
{
	$returnMsg = ($format_wrong)? 'ImportUnsuccess_IncorrectHeaderFormat' : 'ImportUnsuccess_NoRecord';
	intranet_closedb();
	header("location: import_step1.php?BuildingID=$BuildingID&xmsg=".$returnMsg);
	exit();
}



# Title / Menu
$linterface = new interface_html();
$CurrentPageArr['Location'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['Location']['Location'];
$title = $Lang['SysMgr']['Location']['LocationList']; 
$TAGS_OBJ[] = array($title,"");
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]); 

# create temp table if temp table not exists
$sql = "CREATE TABLE IF NOT EXISTS TEMP_INVENTORY_LOCATION_IMPORT
	(
		TempID int(11) NOT NULL auto_increment,
		UserID int(11),
		RowNumber int(11),
		Code varchar(10),
		Barcode varchar(10),
		NameEng varchar(255),
		NameChi varchar(255),
		Sub_Code varchar(10),
		Sub_Barcode varchar(10),
		Sub_NameEng varchar(255),
		Sub_NameChi varchar(255),
		Description text,
		Capacity int(11),
		PRIMARY KEY (TempID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
##########################################################
#														 #
#	IMPORTANT!!!										 #
#	this sql statment is moved to sql_table_update.php 	 #
#	 (with altered table), Capacity is added!!!			 #
#														 #
##########################################################
$li->db_db_query($sql) or die(mysql_error());


// if ($sys_custom['project']['HKPF']) {
//     $sql = "SHOW COLUMNS FROM TEMP_INVENTORY_LOCATION_IMPORT LIKE 'CourseCategory'";
//     $isColumnExist = $li->returnResultSet($sql);
//     if (count($isColumnExist) == 0) {   // not exist
//         $sql = "ALTER TABLE TEMP_INVENTORY_LOCATION_IMPORT ADD CourseCategory TEXT";
//         $li->db_db_query($sql) or die(mysql_error());
//     }
// }

# delete the temp data in temp table 
$sql = "delete from TEMP_INVENTORY_LOCATION_IMPORT where UserID=".$_SESSION["UserID"];
$li->db_db_query($sql) or die(mysql_error());

$errorCount = 0;
$successCount = 0;
$error_result = array();
$i=2;
foreach($data as $k=>$d)
{
	$i++;
	$error_msg = array();
	$error_field = array();
	
	### store csv data to temp data
	if ($sys_custom['project']['HKPF']) {
	    list($this_LocationCode, $this_LocationBarcode, $this_LocationNameEn, $this_LocationNameChi, $this_SubLocationCode,$this_SubLocationBarcode,$this_SubLocationNameEn,$this_SubLocationNameChi,$this_SubLocationDesc,$this_SubLocationCapacity,$this_SubLocationCourseCategory) = $d;
	}
	else {
	    list($this_LocationCode, $this_LocationBarcode, $this_LocationNameEn, $this_LocationNameChi, $this_SubLocationCode,$this_SubLocationBarcode,$this_SubLocationNameEn,$this_SubLocationNameChi,$this_SubLocationDesc,$this_SubLocationCapacity) = $d;
	}
	
	if($this_SubLocationCapacity===''){
		$sql_SubLocationCapacity = 'NULL';
	}else{
		$sql_SubLocationCapacity = "'".$li->Get_Safe_Sql_Query($this_SubLocationCapacity)."'";
	}
	
	$this_LocationCode = trim($this_LocationCode);
	$this_LocationBarcode = trim($this_LocationBarcode);
	$this_LocationNameEn = trim($this_LocationNameEn);
	$this_LocationNameChi = trim($this_LocationNameChi);
	$this_SubLocationCode = trim($this_SubLocationCode);
	$this_SubLocationBarcode = trim($this_SubLocationBarcode);
	$this_SubLocationNameEn = trim($this_SubLocationNameEn);
	$this_SubLocationNameChi = trim($this_SubLocationNameChi);
	$this_SubLocationDesc = trim($this_SubLocationDesc);
	$this_SubLocationCapacity = trim($this_SubLocationCapacity);
	$fields = "UserID, RowNumber, Code, Barcode, NameEng, NameChi, Sub_Code, Sub_Barcode, Sub_NameEng, Sub_NameChi, Description,Capacity";
	$values =  $_SESSION["UserID"] .", $i, '". $li->Get_Safe_Sql_Query($this_LocationCode) ."', '". 
	$li->Get_Safe_Sql_Query($this_LocationBarcode) ."', '". $li->Get_Safe_Sql_Query($this_LocationNameEn) ."', '". 
	$li->Get_Safe_Sql_Query($this_LocationNameChi) ."', '". $li->Get_Safe_Sql_Query($this_SubLocationCode) ."', '". 
	$li->Get_Safe_Sql_Query($this_SubLocationBarcode) ."', '". $li->Get_Safe_Sql_Query($this_SubLocationNameEn) ."', '". 
	$li->Get_Safe_Sql_Query($this_SubLocationNameChi) ."', '". $li->Get_Safe_Sql_Query($this_SubLocationDesc) ."',
			".$sql_SubLocationCapacity."";
	
	if ($sys_custom['project']['HKPF']) {
	    $this_SubLocationCourseCategory = trim($this_SubLocationCourseCategory);
	    $fields .= ", CourseCategory";
	    $values .= ",'". $li->Get_Safe_Sql_Query($this_SubLocationCourseCategory) ."'";
	}
	
	$sql = "
			insert into TEMP_INVENTORY_LOCATION_IMPORT 
			($fields)
			values
			($values)
			";
	$li->db_db_query($sql) or die(mysql_error());
	$TempID = $li->db_insert_id();
	
	### check data
	# 1. Check empty data
	# 1a. No matter new floor or not, the following fields CANNOT empty
	if(empty($this_LocationCode) || empty($this_SubLocationCode)|| empty($this_SubLocationBarcode)|| empty($this_SubLocationNameEn)|| empty($this_SubLocationNameChi))
		$error_msg[] = $Lang['SysMgr']['FormClassMapping']['MissingData'] . " (***)";
	
		//debug_pr($UserID);
		
	# check the action type for Add action
	if($action_type == 1){
	    #1b. New floor => CANNOT empty the following fields
	    $sql = "select LocationLevelID from INVENTORY_LOCATION_LEVEL where Code='$this_LocationCode' and BuildingID=$BuildingID and RecordStatus=1";
	    $result = $li->returnVector($sql);
	    $existings_LocationLevelID = $result[0];
	    
	    $sql = "select TempID from TEMP_INVENTORY_LOCATION_IMPORT where Code='$this_LocationCode' and UserID=$UserID and TempID<>$TempID";
	    $result = $li->returnVector($sql);
	    $existings_LocationLevelID_Temp = $result[0];
	    
	    if(!($existings_LocationLevelID || $existings_LocationLevelID_Temp))
	    {
	        if(empty($this_LocationBarcode) || empty($this_LocationNameEn)|| empty($this_LocationNameChi))
	            $error_msg[] = $Lang['SysMgr']['FormClassMapping']['MissingData'] . " (***)";
	    }
	    
	    
	    # 2. [New Floor] Location Code
	    if(!$existings_LocationLevelID)
	    {
	        # check location code max len = 10
	        $this_LocationCodeLen = countString($this_LocationCode);
	        if($this_LocationCodeLen>10)
	        {
	            $error_msg[] = $Lang['SysMgr']['Location']['LocationCodeLengthError'];
	            $error_field['LocationCode'] = 1;
	        }
	    }
	    
	    
	    # 3. [New Floor] check duplicate Location Barcode
	    if(!$existings_LocationLevelID && $this_LocationBarcode)
	    {
	        # 3a. check location barcode max len = 10
	        $this_LocationBarcodeLen = countString($this_LocationBarcode);
	        if($this_LocationBarcodeLen>10)
	        {
	            $error_msg[] = $Lang['SysMgr']['Location']['LocationBarCodeLengthError'];
	            $error_field['LocationBarcode'] = 1;
	        }
	        else
	        {
	            # 3b. check DB
	            $sql = "select LocationLevelID from INVENTORY_LOCATION_LEVEL where Barcode='$this_LocationBarcode' and BuildingID=$BuildingID and RecordStatus=1";
	            $result = $li->returnVector($sql);
	            $dup_barcode = $result[0];
	            if(!$existings_LocationLevelID_Temp)
	            {
	                # 3c. check Temp DB
	                $sql = "select TempID from TEMP_INVENTORY_LOCATION_IMPORT where Barcode='$this_LocationBarcode' and UserID=$UserID and TempID<>$TempID";
	                $result = $li->returnVector($sql);
	                $dup_barcode_temp = $result[0];
	            }
	            
	            if($dup_barcode || $dup_barcode_temp)
	            {
	                $error_msg[] = $Lang['SysMgr']['Location']['LocationBarCodeDuplicate'];
	                $error_field['LocationBarcode'] = 1;
	            }
	            else
	            {
	                # 3d. check valid barcode or not
	                $pattern = '/^[0-9A-Z\ \.\/\|\$\-]*/';
	                preg_match_all($pattern, $this_LocationBarcode, $result, PREG_SET_ORDER);
	                
	                if($result[0][0] != $this_LocationBarcode)
	                {
	                    $error_msg[] = $Lang['SysMgr']['Location']['LocationBarCodeInvalidFormat'];
	                    $error_field['LocationBarcode'] = 1;
	                }
	                
	            }
	        }
	        
	    }
	    
	    # 4. check sub-location code max len = 10
	    $this_SubLocationCodeLen = countString($this_SubLocationCode);
	    if($this_SubLocationCodeLen>10)
	    {
	        $error_msg[] = $Lang['SysMgr']['Location']['SubLocationCodeLengthError'];
	        $error_field['SubLocationCode'] = 1;
	    }
	    else
	    {
	        # 5. Duplicate Sub-Location Code
	        # 5a. check DB
	        $sql = "select LocationID from INVENTORY_LOCATION where Code='$this_SubLocationCode' and LocationLevelID='$existings_LocationLevelID' and RecordStatus=1";
	        $result = $li->returnVector($sql);
	        $dup_code = $result[0];
	        # 5b. check Temp DB
	        $sql = "select TempID from TEMP_INVENTORY_LOCATION_IMPORT where Sub_Code='$this_SubLocationCode' and UserID=$UserID and TempID<>$TempID and Code='$this_LocationCode'";
	        $result = $li->returnVector($sql);
	        $dup_code_temp = $result[0];
	        
	        if($dup_code || $dup_code_temp)
	        {
	            $error_msg[] = $Lang['SysMgr']['Location']['SubLocationCodeDuplicate'];
	            $error_field['SubLocationCode'] = 1;
	        }
	    }
	    
	    # 6. Duplicate Sub-Location Barcode
	    # 6a. check location barcode max len = 10
	    $this_SubLocationBarcodeLen = countString($this_SubLocationBarcode);
	    if($this_SubLocationBarcodeLen>10)
	    {
	        $error_msg[] = $Lang['SysMgr']['Location']['SubLocationBarCodeLengthError'];
	        $error_field['SubLocationBarcode'] = 1;
	    }
	    else
	    {
	        # 6b. check DB
	        $sql = "select LocationID from INVENTORY_LOCATION where Barcode='$this_SubLocationBarcode' and LocationLevelID='$existings_LocationLevelID' and RecordStatus=1";
	        $result = $li->returnVector($sql);
	        $dup_code = $result[0];
	        # 6c. check Temp DB
	        $sql = "select TempID from TEMP_INVENTORY_LOCATION_IMPORT where Sub_Barcode='$this_SubLocationBarcode' and UserID=$UserID and TempID<>$TempID and Barcode='$this_LocationBarcode'";
	        $result = $li->returnVector($sql);
	        $dup_code_temp = $result[0];
	        if($dup_code || $dup_code_temp)
	        {
	            $error_msg[] = $Lang['SysMgr']['Location']['SubLocationBarCodeDuplicate'];
	            $error_field['SubLocationBarcode'] = 1;
	        }
	        else
	        {
	            # 6d. check valid barcode or not
	            $pattern = '/^[0-9A-Z\ \.\/\|\$\-]*/';
	            preg_match_all($pattern, $this_SubLocationBarcode, $result, PREG_SET_ORDER);
	            
	            if($result[0][0] != $this_SubLocationBarcode)
	            {
	                $error_msg[] = $Lang['SysMgr']['Location']['SubLocationBarCodeInvalidFormat'];
	                $error_field['SubLocationBarcode'] = 1;
	            }
	        }
	    }
	    
	    # 7. Capacity error
	    
	    if(!(is_int(intval($this_SubLocationCapacity))&&$this_SubLocationCapacity>0)){
	        // is not +ve integer
	        if($this_SubLocationCapacity!==''){
	            $error_msg[] = $Lang['SysMgr']['Location']['SubLocationCapacityInvalidFormat'];
	            $error_field['SubLocationCapacity'] = 1;
	        }
	    }
	    
	    if ($sys_custom['project']['HKPF']) {
	        # 8. check location suited category
	        $courseCategoryFound = true;
	        $categories = explode('##',$this_SubLocationCourseCategory);
	        if (count($categories)) {
	            foreach((array)$categories as $category) {
	                $category = trim($category);
	                if (!empty($category)) {
	                    $sql = "SELECT CategoryID FROM INTRANET_ENROL_CATEGORY WHERE CategoryName='".$li->Get_Safe_Sql_Query($category)."'";
	                    $result = $li->returnResultSet($sql);
	                    if (count($result) == 0) {
	                        $courseCategoryFound = false;
	                    }
	                }
	            }
	        }
	        if (!$courseCategoryFound) {
	            $error_msg[] = $Lang['SysMgr']['Location']['CannotFindCourseCategory'];
	            $error_field['SubLocationCourseCategory'] = 1;
	        }
	    }
	    
	    if(empty($error_msg))
	    {
	        $successCount++;
	    }
	    else
	    {
	        $error_result[$TempID] = $error_msg;
	        $error_fields[$TempID] = $error_field;
	        $error_TempID_str .=  $TempID . ",";
	        $errorCount++;
	    }
	}
	
	# check action type == 2 Update action
	if($action_type == 2){
	    #1b. New floor => CANNOT empty the following fields
	    $sql = "select LocationLevelID from INVENTORY_LOCATION_LEVEL where Code='$this_LocationCode' and BuildingID=$BuildingID and RecordStatus=1";
	    $result = $li->returnVector($sql);
	    $existings_LocationLevelID = $result[0];
	    
	    $sql = "select TempID from TEMP_INVENTORY_LOCATION_IMPORT where Code='$this_LocationCode' and UserID=$UserID and TempID<>$TempID";
	    $result = $li->returnVector($sql);
	    $existings_LocationLevelID_Temp = $result[0];
	    
	    if($existings_LocationLevelID || $existings_LocationLevelID_Temp)
	    {
	        if(empty($this_LocationBarcode) || empty($this_LocationNameEn)|| empty($this_LocationNameChi))
	            $error_msg[] = $Lang['SysMgr']['FormClassMapping']['MissingData'] . " (***)";
	    }
	    
	    
	    # 2. [New Floor] Location Code
	    if($existings_LocationLevelID)
	    {
	        # check location code max len = 10
	        $this_LocationCodeLen = countString($this_LocationCode);
	        if($this_LocationCodeLen>10)
	        {
	            $error_msg[] = $Lang['SysMgr']['Location']['LocationCodeLengthError'];
	            $error_field['LocationCode'] = 1;
	        }
	    }
	    
	    
	    # 3. [New Floor] check duplicate Location Barcode
	    if($existings_LocationLevelID && $this_LocationBarcode)
	    {
	        # 3a. check location barcode max len = 10
	        $this_LocationBarcodeLen = countString($this_LocationBarcode);
	        if($this_LocationBarcodeLen>10)
	        {
	            $error_msg[] = $Lang['SysMgr']['Location']['LocationBarCodeLengthError'];
	            $error_field['LocationBarcode'] = 1;
	        }
	        else
	        {
	            # 3b. check DB
	            $sql = "select LocationLevelID from INVENTORY_LOCATION_LEVEL where Barcode='$this_LocationBarcode' and BuildingID=$BuildingID and RecordStatus=1";
	            $result = $li->returnVector($sql);
	            $dup_barcode = $result[0];
	            if(!$existings_LocationLevelID_Temp)
	            {
	                # 3c. check Temp DB
	                $sql = "select TempID from TEMP_INVENTORY_LOCATION_IMPORT where Barcode='$this_LocationBarcode' and UserID=$UserID and TempID<>$TempID";
	                $result = $li->returnVector($sql);
	                $dup_barcode_temp = $result[0];
	            }
	            
	            if(!$dup_barcode)
	            {
	                $error_msg[] = $Lang['SysMgr']['Location']['CannotFindRelatedLocationBarcodeFromDB'];
	                $error_field['LocationBarcode'] = 1;
	            }
	            
	            if($dup_barcode_temp)
	            {
	                $error_msg[] = $Lang['SysMgr']['Location']['LocationBarCodeDuplicate'];
	                $error_field['LocationBarcode'] = 1;
	            }
	            else
	            {
	                # 3d. check valid barcode or not
	                $pattern = '/^[0-9A-Z\ \.\/\|\$\-]*/';
	                preg_match_all($pattern, $this_LocationBarcode, $result, PREG_SET_ORDER);
	                
	                if($result[0][0] != $this_LocationBarcode)
	                {
	                    $error_msg[] = $Lang['SysMgr']['Location']['LocationBarCodeInvalidFormat'];
	                    $error_field['LocationBarcode'] = 1;
	                }
	                
	            }
	        }
	        
	    }
	    
	    # 4. check sub-location code max len = 10
	    $this_SubLocationCodeLen = countString($this_SubLocationCode);
	    if($this_SubLocationCodeLen>10)
	    {
	        $error_msg[] = $Lang['SysMgr']['Location']['SubLocationCodeLengthError'];
	        $error_field['SubLocationCode'] = 1;
	    }
	    else
	    {
	        # 5. Duplicate Sub-Location Code
	        # 5a. check DB
	        $sql = "select LocationID from INVENTORY_LOCATION where Code='$this_SubLocationCode' and LocationLevelID='$existings_LocationLevelID' and RecordStatus=1";
	        $result = $li->returnVector($sql);
	        $dup_code = $result[0];
	        # 5b. check Temp DB
	        $sql = "select TempID from TEMP_INVENTORY_LOCATION_IMPORT where Sub_Code='$this_SubLocationCode' and UserID=$UserID and TempID<>$TempID and Code='$this_LocationCode'";
	        $result = $li->returnVector($sql);
	        $dup_code_temp = $result[0];
	        
	        if(!$dup_code)
	        {
	            $error_msg[] = $Lang['SysMgr']['Location']['CannotFindRelatedSubLocationCodeFromDB'];
	            $error_field['SubLocationCode'] = 1;
	        }
	        
	        if($dup_code_temp)
	        {
	            $error_msg[] = $Lang['SysMgr']['Location']['SubLocationCodeDuplicate'];
	            $error_field['SubLocationCode'] = 1;
	        }
	    }
	    
	    # 6. Duplicate Sub-Location Barcode
	    # 6a. check location barcode max len = 10
	    $this_SubLocationBarcodeLen = countString($this_SubLocationBarcode);
	    if($this_SubLocationBarcodeLen>10)
	    {
	        $error_msg[] = $Lang['SysMgr']['Location']['SubLocationBarCodeLengthError'];
	        $error_field['SubLocationBarcode'] = 1;
	    }
	    else
	    {
	        # 6b. check DB
	        $sql = "select LocationID from INVENTORY_LOCATION where Barcode='$this_SubLocationBarcode' and LocationLevelID='$existings_LocationLevelID' and RecordStatus=1";
	        $result = $li->returnVector($sql);
	        $dup_code = $result[0];
	        # 6c. check Temp DB
	        $sql = "select TempID from TEMP_INVENTORY_LOCATION_IMPORT where Sub_Barcode='$this_SubLocationBarcode' and UserID=$UserID and TempID<>$TempID and Barcode='$this_LocationBarcode'";
	        $result = $li->returnVector($sql);
	        $dup_code_temp = $result[0];
	        if(!$dup_code)
	        {
	            $error_msg[] = $Lang['SysMgr']['Location']['CannotFindRelatedSubLocationBarcodeFromDB'];
	            $error_field['SubLocationBarcode'] = 1;
	        }
	        
	        if($dup_code_temp)
	        {
	            $error_msg[] = $Lang['SysMgr']['Location']['SubLocationBarCodeDuplicate'];
	            $error_field['SubLocationBarcode'] = 1;
	        }
	        else
	        {
	            # 6d. check valid barcode or not
	            $pattern = '/^[0-9A-Z\ \.\/\|\$\-]*/';
	            preg_match_all($pattern, $this_SubLocationBarcode, $result, PREG_SET_ORDER);
	            
	            if($result[0][0] != $this_SubLocationBarcode)
	            {
	                $error_msg[] = $Lang['SysMgr']['Location']['SubLocationBarCodeInvalidFormat'];
	                $error_field['SubLocationBarcode'] = 1;
	            }
	        }
	    }
	    
	    # 7. Capacity error
	    
	    if(!(is_int(intval($this_SubLocationCapacity))&&$this_SubLocationCapacity>0)){
	        // is not +ve integer
	        if($this_SubLocationCapacity!==''){
	            $error_msg[] = $Lang['SysMgr']['Location']['SubLocationCapacityInvalidFormat'];
	            $error_field['SubLocationCapacity'] = 1;
	        }
	    }
	    
	    if ($sys_custom['project']['HKPF']) {
	        # 8. check location suited category
	        $courseCategoryFound = true;
	        $categories = explode('##',$this_SubLocationCourseCategory);
	        if (count($categories)) {
	            foreach((array)$categories as $category) {
	                $category = trim($category);
	                if (!empty($category)) {
	                    $sql = "SELECT CategoryID FROM INTRANET_ENROL_CATEGORY WHERE CategoryName='".$li->Get_Safe_Sql_Query($category)."'";
	                    $result = $li->returnResultSet($sql);
	                    if (count($result) == 0) {
	                        $courseCategoryFound = false;
	                    }
	                }
	            }
	        }
	        if (!$courseCategoryFound) {
	            $error_msg[] = $Lang['SysMgr']['Location']['CannotFindCourseCategory'];
	            $error_field['SubLocationCourseCategory'] = 1;
	        }
	    }
	    
	    if(empty($error_msg))
	    {
	        $successCount++;
	    }
	    else
	    {
	        $error_result[$TempID] = $error_msg;
	        $error_fields[$TempID] = $error_field;
	        $error_TempID_str .=  $TempID . ",";
	        $errorCount++;
	    }
	    
	    
	}
	
}

### List out the import result
$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
/*
$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['SysMgr']['FormClassMapping']['SchoolYear'] ."</td>";
$x .= "<td class='tabletext'>". $ay->Get_Academic_Year_Name() ."</td>";
$x .= "</tr>";
*/
$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['SuccessfulRecord'] ."</td>";
$x .= "<td class='tabletext'>". $successCount ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['FailureRecord'] ."</td>";
$x .= "<td class='tabletext'>". ($errorCount ? "<font color='red'>":"") . $errorCount . ($errorCount ? "</font>":"") ."</td>";
$x .= "</tr>";

$x .= "</table><br>";

if(!empty($error_result))
{
	$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
	
	$x .= "<tr>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">Row#</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['eInventory']['FieldTitle']['FloorCode'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['eInventory']['FieldTitle']['LocationBarcode'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['eInventory']['FieldTitle']['FloorNameEn'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['eInventory']['FieldTitle']['FloorNameChi'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['eInventory']['FieldTitle']['RoomCode'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['eInventory']['FieldTitle']['SubLocationBarcode'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['eInventory']['FieldTitle']['RoomNameEn'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['eInventory']['FieldTitle']['RoomNameChi'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['Location']['Description'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['Location']['Capacity'] ."</td>";
	if ($sys_custom['project']['HKPF']) {
	    $x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['eEnrolment']['category']['applyToCourseCategory']."</td>";
	}
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['General']['Remark'] ."</td>";
	$x .= "</tr>";
	
	$error_TempID_str = substr($error_TempID_str, 0,-1);
	$sql = "select * from TEMP_INVENTORY_LOCATION_IMPORT where TempID in ($error_TempID_str)";
	$tempData = $li->returnArray($sql);
	
	$i=0;
	foreach($tempData as $k=>$d)
	{
	    if ($sys_custom['project']['HKPF']) {
	        list($t_TempID, $t_UserID, $t_RowNumber, $t_code, $t_barcode, $t_NameEn, $t_NameChi, $t_subCode, $t_subBarcode, $t_SubNameEn, $t_SubNameChi, $t_desc,$t_capacity, $t_category) = $d;
	        $t_category = $error_fields[$t_TempID]['SubLocationCourseCategory'] ? "<font color='red'>". $t_category."</font>" : $t_category;
	    }
	    else {
		    list($t_TempID, $t_UserID, $t_RowNumber, $t_code, $t_barcode, $t_NameEn, $t_NameChi, $t_subCode, $t_subBarcode, $t_SubNameEn, $t_SubNameChi, $t_desc,$t_capacity) = $d;
	    }
		$t_code = $t_code ? $t_code : "<font color='red'>***</font>";
		$t_barcode = $t_barcode ? $t_barcode : "<font color='red'>***</font>";
		$t_barcode = $error_fields[$t_TempID]['LocationBarcode'] ? "<font color='red'>". $t_barcode ."</font>" : $t_barcode;

		$t_code = $error_fields[$t_TempID]['LocationCode'] ? "<font color='red'>". $t_code ."</font>" : $t_code;	
		$t_NameEn = $t_NameEn ? $t_NameEn : "<font color='red'>***</font>";
		$t_NameChi = $t_NameChi ? $t_NameChi : "<font color='red'>***</font>";
		$t_subCode = $t_subCode ? $t_subCode : "<font color='red'>***</font>";
		$t_subCode = $error_fields[$t_TempID]['SubLocationCode'] ? "<font color='red'>". $t_subCode ."</font>" : $t_subCode;		
		$t_subBarcode = $t_subBarcode ? $t_subBarcode : "<font color='red'>***</font>";
		$t_subBarcode = $error_fields[$t_TempID]['SubLocationBarcode'] ? "<font color='red'>". $t_subBarcode ."</font>" : $t_subBarcode;		
		$t_SubNameEn = $t_SubNameEn ? $t_SubNameEn : "<font color='red'>***</font>";
		$t_SubNameChi = $t_SubNameChi ? $t_SubNameChi : "<font color='red'>***</font>";
		$t_desc = $t_desc ? $t_desc : $Lang['General']['EmptySymbol'];
		$t_capacity = $error_fields[$t_TempID]['SubLocationCapacity'] ? "<font color='red'>". $t_capacity ."</font>" : $t_capacity;
		
		# build error remark
		$error_remark = "";
		foreach($error_result[$t_TempID] as $k1=>$d1)
		{
			$error_remark .= "- ". $d1."<br>";
		}
		
		$error_remark = $error_remark ? $error_remark : "";
			
		$css_i = ($i % 2) ? "2" : "";
		$x .= "<tr>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\" width=\"10\">". $t_RowNumber ."</td>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $t_code ."</td>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $t_barcode ."</td>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $t_NameEn ."</td>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $t_NameChi ."</td>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $t_subCode ."</td>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $t_subBarcode ."</td>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $t_SubNameEn ."</td>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $t_SubNameChi ."</td>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $t_desc ."</td>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $t_capacity ."</td>";
		if ($sys_custom['project']['HKPF']) {
		    $x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $t_category."</td>";
		}
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\"><font color=red>". $error_remark ."</font></td>";
		$x .= "</tr>";
		
		$i++;
	}
	$x .= "</table>";
}

if($errorCount>0)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_step1.php?BuildingID=$BuildingID'");
}
else
{
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_step1.php?BuildingID=$BuildingID'");
}
?>

<br />
<form name="form1" method="post" action="import_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center"><?=$x?></td>
</tr>
		
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="BuildingID" value="<?=$BuildingID?>">
<input type="hidden" name="actionType" value="<?=$action_type?>">
<input type="hidden" name="TempIDs" value="<?=serialize($TempIDs)?>">
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
