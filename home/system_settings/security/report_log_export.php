<?php
/*
 *  2018-08-22 Cameron
 *      - remove unnecessary library
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
//include_once($PATH_WRT_ROOT."includes/libsubjectpanel.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libuserlogreport.php");

intranet_auth();
intranet_opendb();
$ldb = new libdb();
$lexport = new libexporttext();
$libenroll = new libclubsenrol();
$lu = new libuserlogreport();

$identity = htmlspecialchars($_POST["Identity"]);
$start = htmlspecialchars($_POST["start"]);
$end = htmlspecialchars($_POST["end"]);
$order = htmlspecialchars($_POST["order"]);
$field = htmlspecialchars($_POST["field"]);

// Get Current Time
$now = time();
$today = date('Y-m-d',$now);
$nowTime = date('H:i:s', $now);

// Report Log Record
$reportLogArr = array(
    "Rtype" => "report",
    "Action" => "export",
    "Date" => $today,
    "Time" => $nowTime
);
//$report = $lu->insertLogReport($reportLogArr); 20181109 Not to show

// Filter Options
$filterOpts = array();
$filterOpts['UserName'] = $filter_username;
$filterOpts['Action'] = $filter_Action;
$filterOpts['Rtype'] = $filter_Rtype;
if ($textFromDate == null && $textToDate == null) {
    $filterOpts['DateStart'] = date("Y-m-d");
    $filterOpts['DateEnd'] = date("Y-m-d");
} else {
    $filterOpts['DateStart'] = $textFromDate;
    $filterOpts['DateEnd'] = $textToDate;
}

// Report type and action taken
$typeSelection = $lu->getReportTypeSelection($filter_Rtype, 'filter_Rtype');
$actionSelection = $lu->getReportActionSelection($filter_Action, 'filter_Action');

// Paging Options

if (! isset($pageNo)) {
    $pageNo = 1;
}
if (! isset($pageSize)) {
    $pageSize = 50;
}
$paging = array(
    "pageNo" => $pageNo,
    "pageSize" => $pageSize
);
// Ordering Option
$sortOpts = array();
$sortOpts['Field'] = $sortField;
$sortOpts['Order'] = $sortOrder;

// Get Recrd
$logs = $lu->getAllLogReport($filterOpts, $paging, $sortOpts);
$pageMax = $logs['PAGEMAX'];
$pageItem = $pageMax;
$pageMax = ceil($pageMax / $pageSize);
unset($logs['PAGEMAX']);

// Table Column Setting
$cols = array(
    '#',
    $Lang['SystemSetting']['AccountLog']['Date'],
    $Lang['SystemSetting']['AccountLog']['Time'],
    $Lang['SystemSetting']['AccountLog']['IPAddress'],
    $Lang['SystemSetting']['ReportLog']['Column']['Rtype'],
    $Lang['SystemSetting']['ReportLog']['Column']['Action'],
    $Lang['SystemSetting']['AccountLog']['LogBylogin'],
    $Lang['SystemSetting']['AccountLog']['LogBy'] . ' (En)',
    $Lang['SystemSetting']['AccountLog']['LogBy'] . ' (Ch)'
);
$values = array(
    "LogDate",
    "LogTime",
    "IPAddress",
    "Rtype",
    "Action",
    "UserLogin",
    "EnglishName",
    "ChineseName"
);

$headerAry = $cols;

// Data Content
$dataAry = array();
for($i = 0;$i<sizeof($logs);$i++){
    $dataAry[$i][0] = $i+1;
    for($j=0; $j<sizeof($values);$j++){
        $val = $logs[$i][$values[$j]];
        if ($values[$j] == 'Rtype') {
            $val = $Lang['SystemSetting']['ReportLog']['Rtype'][$val];
        } else if ($values[$j] == 'Action') {
            $val = $Lang['SystemSetting']['ReportLog']['Action'][$val];
        }
        $dataAry[$i][$j+1] = $val;
    }
}

// Convert Data Format
$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

// Export File
$fileName = 'report_log.csv';
$lexport->EXPORT_FILE($fileName, $exportContent);
?>