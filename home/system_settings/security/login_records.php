<?php
// using: 

// ############ Change Log [Start] ################
//
// Date:2019-08-28 [Cameron]
// don't include dynCalendar css and js, browserSniffer.js for HKPF as they are removed for the site, what are these files used for in normal IP?
//
// Date: 2019-05-28 [Philips]
// Default display order: Descending order - Login Time
// Name Column Display same language as ui display
//
// Date: 2019-04-11 [Bill]      [2019-0408-1704-41207]
// Fixed: Identity Selection not work
//
// Date: 2018-08-31 [Cameron]
// Hide Password Policy and Account Logout Policy tab for HKPF
//
// Date: 2018-08-22 [Cameron]
// print function should open in new windows tab
//
// ############ Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libauth.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/libclubsenrol.php");

if ($page_size_change == 1) {
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

include_once ($PATH_WRT_ROOT."includes/libdbtable.php");
include_once ($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

if (!$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) {
    header("Location: /");
    intranet_closedb();
    exit();
}

// Get Current Date Time
$now = time();
$today = date('Y-m-d', $now);

// Date Range Selection Box Option
$ts_weekstart = mktime(0, 0, 0, date('m', $now), date('d', $now) - date('w', $now), date('Y', $now));
$ts_weekend = mktime(0, 0, - 1, date('m', $ts_weekstart), date('d', $ts_weekstart) + 7, date('Y', $ts_weekstart));
$ts_monthstart = mktime(0, 0, 0, date('m', $now), 1, date('Y', $now));
$ts_monthend = mktime(0, 0, - 1, date('m', $now) + 1, 1, date('Y', $now));

$weekstart = date('Y-m-d', $ts_weekstart);
$weekend = date('Y-m-d', $ts_weekend);
$monthstart = date('Y-m-d', $ts_monthstart);
$monthend = date('Y-m-d', $ts_monthend);
$yearstart = date('Y-m-d', getStartOfAcademicYear($now));
$yearend = date('Y-m-d', getEndOfAcademicYear($now));

// Define Date Range
if ($startdate == "" || $enddate == "") {
    $startdate = $today;
    $enddate = $today;
}

$keyword = trim($keyword);

// TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") {
    $page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;

if ($field == "") {
    $field = 2;
}
switch ($field) {
    case 0:
        $field = 0;
        break;
    case 1:
        $field = 1;
        break;
    case 2:
        $field = 2;
        break;
    case 3:
        $field = 3;
        break;
    case 4:
        $field = 4;
        break;
    case 5:
        $field = 5;
        break;
    default:
        $field = 1;
        break;
}
$order = ($order == 1) ? 1 : 0;

$linterface = new interface_html();

// Setting top menu highlight
$CurrentPageArr['SystemSecurity'] = 1;
$CurrentPageArr['SystemSecurity_LoginRecords'] = 1;

// ## Title ###
$MODULE_OBJ['title'] = $Lang['SysMgr']['Security']['Title'];

if (!$sys_custom['project']['HKPF']) {
    $TAGS_OBJ[] = array(
        $Lang['SysMgr']['Security']['PasswordPolicies'],
        "password_policy.php",
        $CurrentPageArr['SystemSecurity_Password']
    );
    $TAGS_OBJ[] = array(
        $Lang['SysMgr']['Security']['AccountLockoutPolicy'],
        "lockout_policy.php",
        $CurrentPageArr['SystemSecurity_Lockout']
    );
}
$TAGS_OBJ[] = array(
    $Lang['SystemSetting']['LoginRecords']['Title'],
    "login_records.php",
    $CurrentPageArr['SystemSecurity_LoginRecords']
);
if($sys_custom['project']['HKPF']) {
    $TAGS_OBJ[] = array(
        $Lang['SystemSetting']['AccountLog']['Title'],
        "account_log.php",
        $CurrentPageArr['SystemSecurity_AccountLog']
    );
    $TAGS_OBJ[] = array(
        $Lang['SystemSetting']['ReportLog']['Title'],
        "report_log.php",
        $CurrentPageArr['SystemSecurity_ReportLog']
    );
}
// $TAGS_OBJ[] = array($Lang['SysMgr']['RoleManagement']['ModuleRole'],"module_role_index.php",0);
$Chinese_Field = "CONCAT(TRIM(CONCAT(IF(b.ChineseName IS NULL OR TRIM(b.ChineseName) = '', b.EnglishName, b.ChineseName), IF(b.TitleChinese != '', CONCAT('', b.TitleChinese), ''))),";
$English_Field = "CONCAT(TRIM(CONCAT(IF(b.ChineseName IS NULL OR TRIM(b.EnglishName) = '', b.ChineseName, b.EnglishName), IF(b.TitleEnglish != '', CONCAT('', b.TitleEnglish), ''))),";

$username_field = Get_Lang_Selection($Chinese_Field, $English_Field);
// [2019-0408-1704-41207]
// if ($identity) {
//     $identity_sql = " AND b.RecordType IN ($identity) ";
// }
if (!empty($targetIdentity)) {
    $identity_sql = " AND b.RecordType IN ('".implode("', '", (array)$targetIdentity)."') ";
}

$sql = "SELECT
            b.UserLogin,
            $username_field CONCAT(' ', IF(b.ClassNumber IS NULL OR b.ClassNumber = '', '', CONCAT(' ,', b.ClassName, '-', b.ClassNumber)), '')),
			a.StartTime,
			a.DateModified,
			a.ClientHost,
			UNIX_TIMESTAMP(a.DateModified) - UNIX_TIMESTAMP(a.StartTime) as duration
		FROM
			INTRANET_LOGIN_SESSION as a
			LEFT OUTER JOIN INTRANET_USER as b ON (a.UserID = b.UserID)
		WHERE
			a.StartTime >= '$startdate 00:00:00' AND a.DateModified <= '$enddate 23:59:59'
            $identity_sql";
// TABLE INFO
// debug_pr($sql);

$print = '';
$print .= $linterface->Get_Content_Tool_v30('print', "javascript:goToPrint();");
$htmlAry['printToolBar'] = $print;

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array(
    "b.UserLogin",
    "CONCAT(TRIM(CONCAT(IF(b.ChineseName IS NULL OR TRIM(b.ChineseName) = '', b.EnglishName, b.ChineseName), IF(b.TitleChinese != '', CONCAT('', b.TitleChinese), ''))), CONCAT(' ', IF(b.ClassNumber IS NULL OR b.ClassNumber = '', '', CONCAT(' ,', b.ClassName, '-', b.ClassNumber)), ''))",
    "a.StartTime",
    "a.DateModified",
    "a.ClientHost",
    "duration"
);
$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + 1;
$li->title = "";
$li->column_array = array(0, 0, 0, 0, 0, 9);
$li->wrap_array = array(0, 0, 0, 0, 0, 0);
$li->IsColOff = 2;

$buttonHref = '';

$export = '';
$export .= $linterface->Get_Content_Tool_v30('export', "javascript:goToExport();");
$htmlAry['toolbar'] = $export;

// TABLE COLUMN
$li->column_list .= "<td width=1% class='field_title'>#</td>\n";
// $li->column_list .= "<td width=10% class='field_title' onclick='remainData()'>".$li->column(0, $i_UserLogin)."</td>\n";
// $li->column_list .= "<td width=10% class='field_title' onclick='remainData()'>".$li->column(1, $i_UserName)."</td>\n";
// $li->column_list .= "<td width=10% class='field_title' onclick='remainData()'>".$li->column(2, $i_UsageStartTime)."</td>\n";
// $li->column_list .= "<td width=10% class='field_title' onclick='remainData()'>".$li->column(3, $i_UsageEndTime)."</td>\n";
// $li->column_list .= "<td width=10% class='field_title' onclick='remainData()'>".$li->column(4, $i_UsageHost)."</td>\n";
// $li->column_list .= "<td width=20% class='field_title' onclick='remainData()'>".$li->column(5, $i_UsageDuration)."</td>\n";
$li->column_list .= "<td width=10% class='field_title'>" . $li->column(0, $i_UserLogin) . "</td>\n";
$li->column_list .= "<td width=10% class='field_title'>" . $li->column(1, $i_UserName) . "</td>\n";
$li->column_list .= "<td width=10% class='field_title'>" . $li->column(2, $i_UsageStartTime) . "</td>\n";
$li->column_list .= "<td width=10% class='field_title'>" . $li->column(3, $i_UsageEndTime) . "</td>\n";
$li->column_list .= "<td width=10% class='field_title'>" . $li->column(4, $i_UsageHost) . "</td>\n";
$li->column_list .= "<td width=20% class='field_title'>" . $li->column(5, $i_UsageDuration) . "</td>\n";

$datetype += 0;
$selected = array();
$selected[$datetype] = "SELECTED";

$classSelect = "<SELECT id=\"datetype\" name=\"datetype\" onChange=\"changeDateType(this.form)\">\n";
    $classSelect .= "<OPTION value=0 " . $selected[0] . ">$i_Profile_Today</OPTION>\n";
    $classSelect .= "<OPTION value=1 " . $selected[1] . ">$i_Profile_ThisWeek</OPTION>\n";
    $classSelect .= "<OPTION value=2 " . $selected[2] . ">$i_Profile_ThisMonth</OPTION>\n";
    $classSelect .= "<OPTION value=3 " . $selected[3] . ">$i_Profile_ThisAcademicYear</OPTION>\n";
$classSelect .= "</SELECT> \n";

unset($selected);
/* 
$selected[$identity] = "SELECTED";

$identitySelect = "<SELECT name=identity>\n";
$identitySelect .= "<OPTION value=0 " . $selected[0] . ">$button_select_all</OPTION>\n";
$identitySelect .= "<OPTION value=1 " . $selected[1] . ">$i_identity_teachstaff</OPTION>\n";
$identitySelect .= "<OPTION value=2 " . $selected[2] . ">$i_identity_student</OPTION>\n";
$identitySelect .= "<OPTION value=3 " . $selected[3] . ">$i_identity_parent</OPTION>\n";
$identitySelect .= "</SELECT> \n";

$targetIdentity = array();
$targetIdentity[0] = $i_identity_teachstaff;
$targetIdentity[1] = $i_identity_student;
$targetIdentity[2] = $i_identity_parent;
 */

// [2019-0408-1704-41207]
$selected = array();
$selected[1] = (!isset($targetIdentity) || in_array(1, (array)$targetIdentity))? "SELECTED" : "";
$selected[2] = (!isset($targetIdentity) || in_array(2, (array)$targetIdentity))? "SELECTED" : "";
$selected[3] = (!isset($targetIdentity) || in_array(3, (array)$targetIdentity))? "SELECTED" : "";

$identitySelection .= '<SELECT id="targetIdentity" name="targetIdentity[]" multiple="" size=\"10\">';
    $identitySelection .= " <OPTION value=1 ".$selected[1].">" . $i_identity_teachstaff . "</OPTION>";
    $identitySelection .= " <OPTION value=2 ".$selected[2].">" . $i_identity_student . "</OPTION>";
    $identitySelection .= " <OPTION value=3 ".$selected[3].">" . $i_identity_parent . "</OPTION>";
$identitySelection .= '</SELECT>';
$identitySelection .= "&nbsp;" . $linterface->Get_Small_Btn($Lang['Btn']['SelectAll'], "button", "js_Select_All_With_OptGroup('targetIdentity',1)");
$identitySelection .= $linterface->spacer();
$identitySelection .= $linterface->MultiSelectionRemark();

// export.php?start=$startdate&end=$enddate&identity=$identity
// $toolbar = $linterface->GET_ACTION_BTN($button_export, "button", "exportRecord()");

// $divName = 'reportOptionOuterDiv';
$tdId = 'tdOption';

$datepickstart = $linterface->GET_DATE_PICKER("startdate", $startdate, " onClick='filterByCategory();' ");
$datepickend = $linterface->GET_DATE_PICKER("enddate", $enddate, " onClick='filterByCategory();' ");
$divName = 'reportOptionDiv';

// Option DIV
$x = '';
$x .= '<div id="' . $divName . '">' . "\n";
    $x .= '<div>' . "\n";
        $x .= '<div id="div_form" class="report_option report_hide_option">';
            $x .= '<span id="spanHideOption" class="spanHideOption" style="display:none">';
                $x .= '<a href="javascript:hideOptionLayer();">' . $Lang['Btn']['HideOption'] . '</a>';
            $x .= '</span>';
            $x .= '<span id="spanShowOption" class="spanShowOption">';
                $x .= '<a href="javascript:showOptionLayer();">' . $Lang['Btn']['ShowOption'] . '</a>';
            $x .= '</span>';
            $x .= '<p class="spacer"></p>';
            $x .= '<span class="Form_Span" style="display:none">';
                $x .= '<table class="form_table_v30">';
                    $x .= "<tr><td class=\"field_title\">$i_Profile_SelectSemester:</td><td> $classSelect<br>$i_Profile_From$datepickstart$i_Profile_To ";
                        $x .= $datepickend . "<span class=extraInfo>(yyyy-mm-dd) </span>";
                        $x .= $linterface->Get_Form_Warning_Msg('div_DateEnd_err_msg', $i_invalid_date, $Class = 'warnMsgDiv');
                    $x .= "</td></tr>";
                    $x .= "<tr>";
                        $x .= "<td class=\"field_title\">$i_identity:</td>";
                        $x .= "<td>" . $identitySelection . "</td>";
                    $x .= "</tr>";
                $x .= "</table>";
                /**
                 * End of Category selector
                 */
                
                $x .= '<div class="edit_bottom_v30">' . "\n";
                    $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "refreshReport();");
                $x .= '</div>';
            $x .= "</span>";
        $x .= "</div>";
    $x .= '</div>' . "\n";
$x .= "</div>";
$htmlAry['reportOptionDiv'] = $x;

// Print Button
$subBtnAry[] = array(
    'javascript: goToPrint();',
    $Lang['SystemSetting']['ReportLog']['Action']['html']
);
$subBtnAry[] = array(
    'javascript: goToPDF(0);',
    $Lang['SystemSetting']['ReportLog']['Action']['pdf']
);
$btnAry[] = array(
    'print',
    'javascript: void(0);',
    '',
    $subBtnAry
);

$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);
$linterface->LAYOUT_START();

if (!$sys_custom['project']['HKPF']):
?>

<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
    var css_array = new Array;
    css_array[0] = "dynCalendar_free";
    css_array[1] = "dynCalendar_half";
    css_array[2] = "dynCalendar_full";
    var date_array = new Array;
</script>

<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<?php endif;?>

<script type="text/javascript">
<!--
// Calendar callback. When a date is clicked on the calendar
// this function is called so you can do as you want with it
function calendarCallback(date, month, year)
{
    if (String(month).length == 1) {
       month = '0' + month;
    }
    if (String(date).length == 1) {
       date = '0' + date;
    }
    dateValue = year + '-' + month + '-' + date;
    document.forms['remove'].RemoveFrom.value = dateValue;
}
// -->

function changeDateType(obj)
{
    switch (obj.datetype.value)
    {
        case '0':
			obj.startdate.value = '<?=$today?>';
        	obj.enddate.value = '<?=$today?>';
          	break;
        case '1':
          	obj.startdate.value = '<?=$weekstart?>';
        	obj.enddate.value = '<?=$weekend?>';
        	break;
        case '2':
          	obj.startdate.value = '<?=$monthstart?>';
        	obj.enddate.value = '<?=$monthend?>';
          	break;
        case '3':
          	obj.startdate.value = '<?=$yearstart?>';
        	obj.enddate.value = '<?=$yearend?>';
        	break;
    }
    //obj.submit();
}
</script>

<style>
    /*
.login_records table {
	width: 100%;
}
.login_records table, .login_records table tr td {
	border: 0;
}
.login_records table, .login_records table tr:nth-child(odd) {
	background-color: #FFFFFF;
}
.login_records table, .login_records table tr:nth-child(even) {
	background-color: #F1F1F1;
}
.login_records table tr:nth-child(1) td {
	background-color: darkgrey;
	color: white;
}
.login_records table tr:nth-child(1) td a {
	text-decoration: none;
	color: white;
}
     */
</style>

<script type="text/javascript">
$(document).ready(function () {
	
});

function refreshReport() {
    var Identity = $('#targetIdentity').val();
    var start = $('#startdate').val();
    var end = $('#enddate').val();
	if(compareDate(start, end) > 0) {	
		alert("invalid date");
	}
	
    if(Identity==null) {
    	alert("please select at least one identity");
    } else {
    	$('input#Identity').val(Identity);
		$('input#start').val(start);
		$('input#end').val(end);
    	$('form#form1').attr('action', 'login_records.php').submit();
    }
}

function hideOptionLayer() {
	$('.Form_Span').attr('style', 'display: none');
	$('.spanHideOption').attr('style', 'display: none');
	$('.spanShowOption').attr('style', '');
	
	document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOptionLayer() {
	$('.Form_Span').attr('style', '');
	$('.spanShowOption').attr('style', 'display: none');
	$('.spanHideOption').attr('style', '');
	
	document.getElementById('div_form').className = 'report_option report_show_option';
}

function goToExport(order, col) {
	var Identity = $('#targetIdentity').val();
    var start = $('#startdate').val();
    var end = $('#enddate').val();
    
    $('input#Identity').val(Identity);
	$('input#start').val(start);
	$('input#end').val(end);
	$('input#order').val(order);
	$('input#col').val(col);
	
    $('form#form1').attr('action', 'login_records_export.php').submit();
    $('form#form1').attr('action', '');
}

function goToPrint() {
	var Identity = $('#targetIdentity').val();
    var start = $('#startdate').val();
    var end = $('#enddate').val();
    
    $('input#Identity').val(Identity);
	$('input#start').val(start);
	$('input#end').val(end);
	
	$('form#form1').attr('target', '_blank');
    $('form#form1').attr('action',' login_records_print.php').submit();
    $('form#form1').attr('target', '_self').attr('action', '');
}

function goToPDF() {
	var Identity = $('#targetIdentity').val();
    var start = $('#startdate').val();
    var end = $('#enddate').val();
    
    $('input#Identity').val(Identity);
	$('input#start').val(start);
	$('input#end').val(end);
	
	$('form#form1').attr('target', '_blank');
    $('form#form1').attr('action', 'login_records_pdf.php').submit();
    $('form#form1').attr('target', '_self').attr('action', '');
}

// function remainData() {
//     var Identity = $('#targetIdentity').val();
//     var start = $('#startdate').val();
//     var end = $('#enddate').val();  
//     	$('input#Identity').val(Identity);
// 		$('input#start').val(start);
// 		$('input#end').val(end);
//     	$('form#form1').attr('action','login_records.php').submit();
//     }
</script>

<!--form id="form2" name="form2" method="post">
    
</form-->

<div>
	<form id="form1" name="form1" method="post">
		<?=$x?>
		<div class="Conntent_tool">
    		<?=$htmlAry['toolbar']?>
    		<?//=$htmlAry['printToolBar']?>
    		<?=$htmlAry['contentTool']?>
		</div>
		<div class="login_records">
			<?php echo $li->display(); ?>
		</div>
		
		<input type="hidden" id="Identity" Name="Identity" Value=""> 
		<input type="hidden" id="start" Name="start" Value="">
		<input type="hidden" id="end" Name="end" Value="">
		<input type="hidden" id="col" Name="col" Value="">
		<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
		<input type=hidden name=order value="<?php echo $li->order; ?>">
		<input type=hidden name=field value="<?php echo $li->field; ?>">
		<input type=hidden name=page_size_change value="">
		<input type=hidden name=numPerPage value="<?=$li->page_size?>">
	</form>
</div>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>