<?php
# using: 

/************************************
 *  Modification log
 *
 *  2018-08-15 (Henry)
 *  - fixed sql injection
 * 
 *	2014-03-03	Yuen
 *	- added this setting
 * **********************************/

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth(); 
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}


if (trim($login_attempt_limit)=="")
{
	$login_attempt_limit = 10;
}
if (trim($login_lock_duration)=="")
{
	$login_lock_duration = 10;
}

$login_lock_duration = IntegerSafe($login_lock_duration);

$content_login = "$login_attempt_limit\n$login_lock_duration";
$li = new libfilesystem();
$li->file_write(intranet_htmlspecialchars(trim(stripslashes($content_login))), $intranet_root."/file/login_attempt_lock.txt");


intranet_closedb();
header("Location: lockout_policy.php?xmsg=UpdateSuccess");
?>