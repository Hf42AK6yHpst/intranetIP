<?php 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
//include_once($PATH_WRT_ROOT."includes/libsubjectpanel.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libuserlogreport.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
// include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$linterface = new interface_html();
$lu = new libuserlogreport();

if ($page_size_change == 1) {
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

// intranet_auth();
// intranet_opendb();

$libenroll = new libclubsenrol();

// $start = $_POST['start'];
// $end = $_POST['end'];
$identity = htmlspecialchars($_POST["Identity"]);
$start = htmlspecialchars($_POST["start"]);
$end = htmlspecialchars($_POST["end"]);

// Get Current Time
$now = time();
$today = date('Y-m-d',$now);
$nowTime = date('H:i:s', $now);

// Report Log Record
$reportLogArr = array(
    "Rtype" => "login",
    "Action" => "html",
    "Date" => $today,
    "Time" => $nowTime
);
$report = $lu->insertLogReport($reportLogArr);

// Define Date Range
if ($start == "" || $end == "") {
    $start = $today;
    $end = $today;
}

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);

if ($field == "") $field = 1;
switch ($field) {
    case 0: $field = 0; break;
    case 1: $field = 1; break;
    case 2: $field = 2; break;
    case 3: $field = 3; break;
    default: $field = 1; break;
}
$order = ($order == 1) ? 1 : 0;

$linterface = new interface_html();

// Setting top menu highlight
$CurrentPageArr['SystemSecurity'] = 1;
$CurrentPageArr['SystemSecurity_LoginRecords'] = 1;

$username_field = getNameFieldWithLoginClassNumberByLang("b.");
if($identity) {
    $identity_sql = " and b.RecordType IN ($identity) ";
}

// Sorting setting
$fields = array("UserLogin", "UserName", "DateModified", "ClientHost", "duration");
$orders = array("DESC", "ASC");
$orderCond = "ORDER BY $fields[$field] $orders[$order]";

// SQL Query
$sql  = "SELECT
            b.UserLogin,
            CONCAT(TRIM(CONCAT(IF(b.ChineseName IS NULL OR TRIM(b.ChineseName) = '', b.EnglishName, b.ChineseName), IF(b.TitleChinese != '', CONCAT('', b.TitleChinese), ''))), CONCAT(' ', IF(b.ClassNumber IS NULL OR b.ClassNumber = '', '', CONCAT(' ,', b.ClassName, '-', b.ClassNumber)), '')) as UserName,
			a.StartTime,
			a.DateModified,
			a.ClientHost,
			UNIX_TIMESTAMP(a.DateModified) - UNIX_TIMESTAMP(a.StartTime) as duration
		FROM
			INTRANET_LOGIN_SESSION as a
			LEFT OUTER JOIN INTRANET_USER as b ON (a.UserID = b.UserID)
		WHERE
			a.StartTime >= '$start 00:00:00' AND a.DateModified <= '$end 23:59:59'
			$identity_sql
            $orderCond ";

# TABLE INFO
$buttonHref = '';

$export = '';
$export .= $linterface->Get_Content_Tool_v30('export', "javascript:goToExport();");
$htmlAry['toolbar'] = $export;

$print = '';
$print .= $linterface->Get_Content_Tool_v30('print', "javascript:goToPrint();");
$htmlAry['printToolBar'] = $print;

$lib = new libdb();
$dataResult = $lib->returnArray($sql,2);

// Table Header
$tableHead = "<thead>";
    $tableHead .= "<tr><th colspan='7'>RESTRICTED</th></tr>";
    $tableHead .= "<tr>";
    $tableHead .= "<th width=1% class='field_title'>#</td>\n";
    $tableHead .= "<th width=10% class='field_title'>".$i_UserLogin."</td>\n";
    $tableHead .= "<th width=19% class='field_title'>".$i_UserName."</td>\n";
    $tableHead .= "<th width=20% class='field_title'>".$i_UsageStartTime."</td>\n";
    $tableHead .= "<th width=20% class='field_title'>".$i_UsageEndTime."</td>\n";
    $tableHead .= "<th width=10% class='field_title'>".$i_UsageHost."</td>\n";
    $tableHead .= "<th width=20% class='field_title'>".$i_UsageDuration."</td>\n";
    $tableHead .= "</tr>";
$tableHead .= "</thead>";

$datetype += 0;

// Table Body Content
$index = 1;

$colArr = array("UserLogin", "UserName", "StartTime", "DateModified", "ClientHost", "duration");
$tableBody = "<tbody>";
foreach($dataResult as $data)
{
    $tableBody .= "<tr>";
    $tableBody .= "<td>$index</td>";
    foreach($colArr as $col){
        if($col=='duration'){
            $tableBody .= "<td>".secToTime($data[$col])."</td>";
        } else {
            $tableBody .= "<td>".$data[$col]."</td>";
        }
    }
    $tableBody .= "</tr>";
    $index++;
}
$tableBody .= "</tbody>";

// Table Footer
$tableFoot = "<tfoot>";
    $tableFoot .= "<tr><td colspan='7' style='border: 0;'>RESTRICTED</td></tr>";
    $tableFoot .= "<tr><td colspan='7' align='right' style='border: 0;'>This report is printed at $today $nowTime</td></tr>";
$tableFoot .= "</tfoot>";
?>

<script type="text/javascript">
$(document).ready(function () {
	
});
</script>

<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>
<div class="login_records" width="100%">
	<table>
		<?=$tableHead?>
		<?=$tableBody?>
		<?=$tableFoot?>
	</table>
</div>

<input type="hidden" id="Identity" Name="Identity" Value="">
<input type="hidden" id="start" Name="start" Value="">
<input type="hidden" id="end" Name="end" Value="">
</form>
</div>

<style>
@page {
    size: A4;
    margin: 0;
    width: 100%;
}
html,body{
    height:297mm;
    width:210mm;
}

.function_bar1{
    //background-color: #fff9b2;
}
.function_bar2{
    
}

.login_records table{
    width: 100%;
}
.login_records table, .login_records table tr td{
    border: 0;
}
.login_records table, .login_records table tr:nth-child(odd){
    background-color: #FFFFFF;
}
.login_records table, .login_records table tr:nth-child(even){
    background-color: #F1F1F1;
}
.login_records table thead tr:nth-child(2) th{
    background-color: darkgrey;
    color: white;
}
.login_records table thead tr:nth-child(1) th, .login_records table tfoot tr:first-child td{
    text-align: center;
    font-size: 5mm;
    font-weight: bold;
}
</style>

<script language="javascript">
function exportRecord(){
	window.open("<?="export.php?start=$start&end=$end&identity=$identity"?>");
}
</script>

<?php
function secToTime($data) {
    if ($data > 3600) {
        $hr = intval($data/3600);
        $data = $data % 3600;
    }
    if ($data > 60) {
        $min = intval($data/60);
        $data = $data % 60;
    }
    $s_data = "";
    
    global $i_Usage_Hour, $i_Usage_Min, $i_Usage_Sec;
    if ($hr != "") $s_data .= "$hr $i_Usage_Hour";
    if ($min != "") $s_data .= "$min $i_Usage_Min";
    $s_data .= "$data $i_Usage_Sec";
    
    return $s_data;
}
?>