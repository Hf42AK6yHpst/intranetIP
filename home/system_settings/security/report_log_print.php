<?php 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
//include_once($PATH_WRT_ROOT."includes/libsubjectpanel.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libuserlogreport.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
// include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$linterface = new interface_html();
$lu = new libuserlogreport();


if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}


intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();

// $start = $_POST['start'];
// $end = $_POST['end'];
$identity = htmlspecialchars($_POST["Identity"]);
$start = htmlspecialchars($_POST["start"]);
$end = htmlspecialchars($_POST["end"]);

$now = time();
$today = date('Y-m-d',$now);
$nowTime = date('H:i:s', $now);

// Report Log Record
$reportLogArr = array(
    "Rtype" => "report",
    "Action" => "html",
    "Date" => $today,
    "Time" => $nowTime
);
//$report = $lu->insertLogReport($reportLogArr); 20181109 Not to record

$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));

$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));

// filter Options

if(!isset($filter_Action)){
    $filter_Action = -1;
}
if(!isset($filter_Rtype)){
    $filter_Rtype = -1;
}

$filterOpts = array();
$filterOpts['UserName'] = $filter_username;
$filterOpts['Action'] = $filter_Action;
$filterOpts['Rtype'] = $filter_Rtype;
if ($textFromDate == null && $textToDate == null){
    $filterOpts['DateStart'] = date("Y-m-d");
    $filterOpts['DateEnd'] = date("Y-m-d");
}
else{
    $filterOpts['DateStart'] = $textFromDate;
    $filterOpts['DateEnd'] = $textToDate;
}
$paging = array();
$paging['all'] = true;
// Ordering Option
$sortOpts = array();
$sortOpts['Field'] = $sortField;
$sortOpts['Order'] = $sortOrder;

// Get Recrd
$logs = $lu->getAllLogReport($filterOpts, $paging, $sortOpts);
unset($logs['PAGEMAX']);
$cols = array(
    array(
        'text' => '#',
        'style' => 'width: 1%;'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['Date'],
        'style' => 'width: 12%'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['Time'],
        'style' => 'width: 12%'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['IPAddress'],
        'style' => 'width: 12%'
    ),
    array(
        'text' => $Lang['SystemSetting']['ReportLog']['Column']['Rtype'],
        'style' => 'width: 12%'
    ),
    array(
        'text' => $Lang['SystemSetting']['ReportLog']['Column']['Action'],
        'style' => 'width: 12%'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['LogBylogin'],
        'style' => 'width: 13%'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['LogBy'] . ' (En)',
        'style' => 'width: 13%'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['LogBy'] . ' (Ch)',
        'style' => 'width: 13%'
    )
);
$resultTable = "<table class='common_table_list_v30 view_table_list_v30'>";

// Table Header Column
$resultTable .= "<thead>";
$resultTable .= "<tr><th colspan='9'>RESTRICTED</th></tr>";
$resultTable .= "<tr>";
foreach($cols as $col){
    $resultTable .= "<th style='$col[style]'>$col[text]</th>";
}
$resultTable .= "</tr>";
$resultTable .= "</thead>";

// Table Body Content
$values = array("LogDate", "LogTime", "IPAddress", "Rtype", "Action", "UserLogin", "EnglishName", "ChineseName");
$resultTable .= "<tbody>";
if(sizeof($logs)==0){
    // No Data
    $resultTable .= "<tr>";
    $resultTable .= '<td align="center" colspan="' . sizeof($cols) . '">' . $i_no_record_exists_msg . '</td>';
    $resultTable .= "</tr>";
} else {
    $index = 1;
    foreach($logs as $log){
        $resultTable .= "<tr>";
        $resultTable .= "<td>$index</td>";
        foreach($values as $value){
            if($value == 'Rtype'){
                $resultTable .= "<td>" . $Lang['SystemSetting']['ReportLog']['Rtype'][$log[$value]] . "</td>";
            } else if($value == 'Action'){
                $resultTable .= "<td>" . $Lang['SystemSetting']['ReportLog']['Action'][$log[$value]] . "</td>";
            } else{
                $resultTable .= "<td>" . $log[$value] . "</td>";
            }
        }
        $resultTable .= "</tr>";
        $index++;
    }
}

$resultTable .= "</tbody>";
$resultTable .= "<tfoot>";
$resultTable .= "<tr><td colspan='9' style='border: 0;'>RESTRICTED</td></tr>";
$resultTable .= "<tr><td colspan='9' align='right' style='border: 0;'>This report is printed at $today $nowTime</td></tr>";
$resultTable .= "</tfoot>";
$resultTable .= "</table>";
?>
</script>
<table width="100%" align="center" class="print_hide" border="0">
<tr>
<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>
	<div class="login_records" width="100%">
		<?=$resultTable?>
	</div>
</div>
<style>
@page {
  size: A4;
  margin: 0;
  width: 100%;
}
html,body{
    height:297mm;
    width:210mm;
}
table thead tr:nth-child(1) th, table tfoot tr:first-child td{
    text-align: center;
    font-size: 5mm;
    font-weight: bold;
}
table thead tr:nth-child(2) th{
    background-color: #487db4/* #4A7ECC*/;
    color: #FFFFFF;
}
table tr, table th, table td{
    border-collapse: collapse;
}
</style>
<?php 
?>