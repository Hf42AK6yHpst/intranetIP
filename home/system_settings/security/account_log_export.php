<?php
# using:
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT. "includes/libuser.php");
include_once($PATH_WRT_ROOT. "includes/libuserlogreport.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_report = new libclubsenrol_report();
$lexport = new libexporttext();
$lu = new libuserlogreport();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

// Get Current Time
$now = time();
$today = date('Y-m-d',$now);
$nowTime = date('H:i:s', $now);

// Report Log Record
$reportLogArr = array(
    "Rtype" => "account",
    "Action" => "export",
    "Date" => $today,
    "Time" => $nowTime
);
$report = $lu->insertLogReport($reportLogArr);

$filterOpts = array();
$filterOpts['name'] = $filter_username;
$filterOpts['action'] = $filter_action;
if ($textFromDate == null && $textToDate == null) {
    $filterOpts['DateStart'] = date("Y-m-d");
    $filterOpts['DateEnd'] = date("Y-m-d");
} else {
    $filterOpts['DateStart'] = $textFromDate;
    $filterOpts['DateEnd'] = $textToDate;
}
// debug_pr($filter_username);
// debug_pr($filter_action);
// debug_pr($filter_logDate_start);

// Paging Options
if (! isset($pageNo)) {
    $pageNo = 1;
}
if (! isset($pageSize)) {
    $pageSize = 50;
}
$paging = array(
    "all" => true
);

// Sorting Options
$sortOpts = array();
$sortOpts['Field'] = $sortField;
$sortOpts['Order'] = $sortOrder;

// Get All the Log Records
$logs = $lu->getAccountLog($filterOpts, $paging, $sortOpts);
$pageMax = $logs['PAGEMAX'];
$pageItem = $pageMax;
$pageMax = ceil($pageMax / $pageSize);
unset($logs['PAGEMAX']);

// filter Options

// Data Fields Setting
$values = array(
    '',
    'LogDate',
    'LogTime',
    'IPAddress',
    'LogName',
    'USERNAME',
    'Action',
    'Userlogin'
);

// Header Row
$headerAry = array();
$headerAry[] = "#";
$headerAry[] = $Lang['SystemSetting']['AccountLog']['Date'];
$headerAry[] = $Lang['SystemSetting']['AccountLog']['Time'];
$headerAry[] = $Lang['SystemSetting']['AccountLog']['IPAddress'];
$headerAry[] = $Lang['SystemSetting']['AccountLog']['LogBylogin'];
$headerAry[] = $Lang['SystemSetting']['AccountLog']['LogBy'] . ' (En)';
$headerAry[] = $Lang['SystemSetting']['AccountLog']['LogBy'] . ' (Ch)';
$headerAry[] = $Lang['SystemSetting']['AccountLog']['Action'];
$headerAry[] = $Lang['SystemSetting']['AccountLog']['Userlogin'];
$headerAry[] = $Lang['SystemSetting']['AccountLog']['User'] . ' (En)';
$headerAry[] = $Lang['SystemSetting']['AccountLog']['User'] . ' (Ch)';

$index = 1;
// Data Fields Setting
$values = array(
    "", "LogDate", "LogTime", "IPAddress", "LogBy", "LogEnglishName",
    "LogChineseName", "Action", "UserLogin", "EnglishName", "ChineseName"
);

$index = 1;

foreach($logs as $log){
    foreach($values as $val){
        if($val == 'Action'){
            $dataArr[$index-1][] = decodeCRUD($log[$val]);
        } else if($val != ''){
            $dataArr[$index-1][].= $log[$val];
        } else {
            $dataArr[$index-1][] .= $index;
        }
    }
    $index++;
}
// Convert Data Format
$export_text = $lexport->GET_EXPORT_TXT($dataArr, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);

// Output File
$filename = "system_Security.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);

function decodeCRUD($target)
{
    $result = "";
    global $Lang;
    switch ($target) {
        case 'C':
            $result = $Lang['SystemSetting']['AccountLog']['Create'];
            break;
        case 'R':
            $result = $Lang['SystemSetting']['AccountLog']['Read'];
            break;
        case 'U':
            $result = $Lang['SystemSetting']['AccountLog']['Update'];
            break;
        case 'D':
            $result = $Lang['SystemSetting']['AccountLog']['Delete'];
            break;
        default:
            break;
    }
    return $result;
}
?>