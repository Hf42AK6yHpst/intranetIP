<?php
// using:  

############# Change Log [Start] ################
# 2018-08-14 Cameron - show staff only for HKPF
# 2018-01-15 Carlos - Use $sys_custom['UseStrongPassword'] to force use strong password. 
# 2018-01-11 Carlos - Removed NO option for [Require complex password], must set at least 6 characters long compelx password.
# 2014-03-03 Yuen - introduced this setting in front-end
############# Change Log [End] ################

 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

// setting top menu highlight
$CurrentPageArr['SystemSecurity'] = 1;
$CurrentPageArr['SystemSecurity_Password'] = 1;

# change page web title
$js = '<script type="text/JavaScript" language="JavaScript">'."\n";
$js.= 'document.title="eClass System Security";'."\n";
$js.= '</script>'."\n";

### Title ###
$MODULE_OBJ['title'] = $Lang['SysMgr']['Security']['Title'].$js;
$TAGS_OBJ[] = array($Lang['SysMgr']['Security']['PasswordPolicies'], "password_policy.php", $CurrentPageArr['SystemSecurity_Password']);
$TAGS_OBJ[] = array($Lang['SysMgr']['Security']['AccountLockoutPolicy'], "lockout_policy.php", $CurrentPageArr['SystemSecurity_Lockout']);
$TAGS_OBJ[] = array($Lang['SystemSetting']['LoginRecords']['Title'], "login_records.php", $CurrentPageArr['SystemSecurity_LoginRecords']);
if($sys_custom['project']['HKPF']){
    $TAGS_OBJ[] = array($Lang['SystemSetting']['AccountLog']['Title'], "account_log.php", $CurrentPageArr['SystemSecurity_AccountLog']);
    $TAGS_OBJ[] = array($Lang['SystemSetting']['ReportLog']['Title'], "report_log.php", $CurrentPageArr['SystemSecurity_ReportLog']);
}
//$TAGS_OBJ[] = array($Lang['SysMgr']['RoleManagement']['ModuleRole'],"module_role_index.php",0);




function GetAllowPasswordDisplay($current_value)
{
	global $Lang;
	if (!$current_value)
	{
		return "<font color='red'>".$Lang['SysMgr']['Security']['Disallow2Change']."</font>";
	} else
	{
		return $Lang['SysMgr']['Security']['Allow2Change'];
	}
}


function GetComplexPasswordInput($user_type, $current_value)
{
	global $Lang, $linterface, $sys_custom;
	
	if(!$sys_custom['UseStrongPassword']){
		$Options[] = array(-1, $Lang['SysMgr']['Security']['NotRequireComplexPassword']);
	}
	for ($i=6; $i<=16; $i++)
	{
		$Options[] = array($i, $i . " " . $Lang['SysMgr']['Security']['unit_character']);
	}
	
	return $linterface->GET_SELECTION_BOX($Options, " id='EnablePasswordPolicy_{$user_type}' name='EnablePasswordPolicy_{$user_type}' ", "", $current_value);
	
}

function GetComplexPasswordDisplay($current_value, $AllowPasswordChange)
{
	global $Lang, $sys_custom;
	if (!$AllowPasswordChange)
	{
		return "--";
	} elseif ($current_value==-1)
	{
		$current_value = 6;
		if($sys_custom['UseStrongPassword']){
			return $current_value . " " . $Lang['SysMgr']['Security']['unit_character'];
		}else{
			return "<font color='red'>".$Lang['SysMgr']['Security']['NotRequireComplexPassword']."</font>";
		}
	} else
	{
		return $current_value . " " . $Lang['SysMgr']['Security']['unit_character'];
	}
}

function GetPeriodicalChangeInput($user_type, $current_value)
{
	global $Lang, $linterface;
	
	$Options[] = array(-1, $Lang['SysMgr']['Security']['NotRequirePeriodicalChange']);
	for ($i=1; $i<=30; $i++)
	{
		$Options[] = array($i*10, ($i*10) . " " . $Lang['SysMgr']['Security']['unit_day']);
	}
	
	return $linterface->GET_SELECTION_BOX($Options, " id='RequirePasswordPeriod_{$user_type}' name='RequirePasswordPeriod_{$user_type}' ", "", $current_value);
	
}

function GetPeriodicalChangeDisplay($current_value, $AllowPasswordChange)
{
	global $Lang;
	if (!$AllowPasswordChange)
	{
		return "--";
	} elseif ($current_value==-1)
	{
		return "<font color='red'>".$Lang['SysMgr']['Security']['NotRequirePeriodicalChange']."</font>";
	} else
	{
		return $current_value . " " . $Lang['SysMgr']['Security']['unit_day'];
	}
}



$lgeneralsettings = new libgeneralsettings();
$data = $lgeneralsettings->Get_General_Setting("UserInfoSettings");


# USERTYPE_STAFF


$Staff_2Change = $data['CanUpdatePassword_'.USERTYPE_STAFF];
$Staff_2Complex =  $data['EnablePasswordPolicy_'.USERTYPE_STAFF];
if ($Staff_2Complex==1)
{
	$Staff_2Complex = 6;
} elseif ($Staff_2Complex=="")
{
	$Staff_2Complex = -1;
}
$Staff_Period = $data['RequirePasswordPeriod_'.USERTYPE_STAFF];
if ($Staff_Period=="")
{
	$Staff_Period = -1;
}


# USERTYPE_STUDENT

$Student_2Change = $data['CanUpdatePassword_'.USERTYPE_STUDENT];
$Student_2Complex =  $data['EnablePasswordPolicy_'.USERTYPE_STUDENT];
if ($Student_2Complex==1)
{
	$Student_2Complex = 6;
} elseif ($Student_2Complex=="")
{
	$Student_2Complex = -1;
}
$Student_Period = $data['RequirePasswordPeriod_'.USERTYPE_STUDENT];
if ($Student_Period=="")
{
	$Student_Period = -1;
}


# USERTYPE_PARENT

$Parent_2Change = $data['CanUpdatePassword_'.USERTYPE_PARENT];
$Parent_2Complex =  $data['EnablePasswordPolicy_'.USERTYPE_PARENT];
if ($Parent_2Complex==1)
{
	$Parent_2Complex = 6;
} elseif ($Parent_2Complex=="")
{
	$Parent_2Complex = -1;
}
$Parent_Period = $data['RequirePasswordPeriod_'.USERTYPE_PARENT];
if ($Parent_Period=="")
{
	$Parent_Period = -1;
}


# USERTYPE_ALUMNI

$Alumni_2Change = $data['CanUpdatePassword_'.USERTYPE_ALUMNI];
$Alumni_2Complex =  $data['EnablePasswordPolicy_'.USERTYPE_ALUMNI];
if ($Alumni_2Complex==1)
{
	$Alumni_2Complex = 6;
} elseif ($Alumni_2Complex=="")
{
	$Alumni_2Complex = -1;
}
$Alumni_Period = $data['RequirePasswordPeriod_'.USERTYPE_ALUMNI];
if ($Alumni_Period=="")
{
	$Alumni_Period = -1;
}


$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>




<form name="form1" method="post" action="password_policy_update.php">

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>

<fieldset class="instruction_box_v30">
   	 <legend><?=$Lang['General']['Important']?></legend>
	<?=$Lang['SysMgr']['Security']['Note']?>
</fieldset>
 
 

<div class="form_content">


<div class="form_content" id="LayerView">

	<div class="table_row_tool row_content_tool">
			<?= $linterface->GET_BTN("&nbsp; ".$Lang['Btn']['Edit']." &nbsp;", "button", "ToggleEditView(1)") ?>
	</div>

	<table class="common_table_list">
	<thead>
	        <tr>
	   	  <th><?=$Lang['SysMgr']['RoleManagement']['Identity']?></th>	
	   	  <th><?=$Lang['SysMgr']['Security']['enable_password_change']?></th>
	   	  <th><?=$Lang['SysMgr']['Security']['enable_password_policy']?> <span class="tabletextrequire">#</span> </th>
	   	  <th><?=$Lang['SysMgr']['Security']['enable_periodical_change']?> </th>
	       </tr>
	   </thead>
		<tbody>
	   	<tr>
	   		<td><?=$Lang['Identity']['TeachingStaff']?> / <?=$Lang['Identity']['NonTeachingStaff']?></td>
	   		<td><?=GetAllowPasswordDisplay($Staff_2Change)?></td>
	   		<td><?=GetComplexPasswordDisplay($Staff_2Complex, $Staff_2Change)?></td>
	   		<td><?=GetPeriodicalChangeDisplay($Staff_Period, $Staff_2Change)?></td>
	   	</tr>
<?php if (!$sys_custom['project']['HKPF']):?>	   	
	   	<tr class="row_drafted">
	   		<td><?=$Lang['Identity']['Student']?></td>
	   		<td><?=GetAllowPasswordDisplay($Student_2Change)?></td>
	   		<td><?=GetComplexPasswordDisplay($Student_2Complex, $Student_2Change)?></td>
	   		<td><?=GetPeriodicalChangeDisplay($Student_Period, $Student_2Change)?></td>
	   	</tr>
	   	<tr>
	   		<td><?=$Lang['Identity']['Parent']?></td>
	   		<td><?=GetAllowPasswordDisplay($Parent_2Change)?></td>
	   		<td><?=GetComplexPasswordDisplay($Parent_2Complex, $Parent_2Change)?></td>
	   		<td><?=GetPeriodicalChangeDisplay($Parent_Period, $Parent_2Change)?></td>
	   	</tr>
<?php if ($special_feature['alumni']) { ?>
	   	<tr class="row_drafted">
	   		<td><?=$Lang['Identity']['Alumni']?></td>
	   		<td><?=GetAllowPasswordDisplay($Alumni_2Change)?></td>
	   		<td><?=GetComplexPasswordDisplay($Alumni_2Complex, $Alumni_2Change)?></td>
	   		<td><?=GetPeriodicalChangeDisplay($Alumni_Period, $Alumni_2Change)?></td>
	   	</tr>
<?php } ?>
<?php endif;?>
	   </tbody>
	</table>
	
	
	<span class="tabletextremark">
	<span class="tabletextrequire">#</span> 
	<?=$Lang['SysMgr']['Security']['remark_complex_password']?>
	 </span>

</div>


<div class="form_content" id="LayerEdit" style="display:none">


	<table class="common_table_list_v30 edit_table_list_v30">
	<thead>
	        <tr>
	   	  <th><?=$Lang['SysMgr']['RoleManagement']['Identity']?></th>	
	   	  <th><?=$Lang['SysMgr']['Security']['enable_password_change']?></th>
	   	  <th><?=$Lang['SysMgr']['Security']['enable_password_policy']?> <span class="tabletextrequire">#</span> </th>
	   	  <th><?=$Lang['SysMgr']['Security']['enable_periodical_change']?> </th>
	       </tr>
	   </thead>
		<tbody>
	   	<tr>
	   		<td><?=$Lang['Identity']['TeachingStaff']?> / <?=$Lang['Identity']['NonTeachingStaff']?></td>
	   		<td><?=$linterface->Get_Checkbox("CanUpdatePassword_".USERTYPE_STAFF, "CanUpdatePassword_".USERTYPE_STAFF, "1", $Staff_2Change, $Class='', $Display='', "ToggleColumns(".USERTYPE_STAFF.")", $Disabled='')?></td>
	   		<td><div class="HideUpdate_<?=USERTYPE_STAFF?>" style="display:none">--</div><div class="ShowUpdate_<?=USERTYPE_STAFF?>"><?=GetComplexPasswordInput(USERTYPE_STAFF, $Staff_2Complex)?></div></td>
	   		<td><div class="HideUpdate_<?=USERTYPE_STAFF?>" style="display:none">--</div><div class="ShowUpdate_<?=USERTYPE_STAFF?>"><?=GetPeriodicalChangeInput(USERTYPE_STAFF, $Staff_Period)?></div></td>
	   	</tr>
<?php if (!$sys_custom['project']['HKPF']):?>	   	
	   	<tr class="row_avaliable">
	   		<td><?=$Lang['Identity']['Student']?></td>
	   		<td><?=$linterface->Get_Checkbox("CanUpdatePassword_".USERTYPE_STUDENT, "CanUpdatePassword_".USERTYPE_STUDENT, "1", $Student_2Change, $Class='', $Display='', "ToggleColumns(".USERTYPE_STUDENT.")", $Disabled='')?></td>
	   		<td><div class="HideUpdate_<?=USERTYPE_STUDENT?>" style="display:none">--</div><div class="ShowUpdate_<?=USERTYPE_STUDENT?>"><?=GetComplexPasswordInput(USERTYPE_STUDENT, $Student_2Complex)?></div></td>
	   		<td><div class="HideUpdate_<?=USERTYPE_STUDENT?>" style="display:none">--</div><div class="ShowUpdate_<?=USERTYPE_STUDENT?>"><?=GetPeriodicalChangeInput(USERTYPE_STUDENT, $Student_Period)?></div></td>
	   	</tr>
	   	<tr>
	   		<td><?=$Lang['Identity']['Parent']?></td>
	   		<td><?=$linterface->Get_Checkbox("CanUpdatePassword_".USERTYPE_PARENT, "CanUpdatePassword_".USERTYPE_PARENT, "1", $Parent_2Change, $Class='', $Display='', "ToggleColumns(".USERTYPE_PARENT.")", $Disabled='')?></td>
	   		<td><div class="HideUpdate_<?=USERTYPE_PARENT?>" style="display:none">--</div><div class="ShowUpdate_<?=USERTYPE_PARENT?>"><?=GetComplexPasswordInput(USERTYPE_PARENT, $Parent_2Complex)?></div></td>
	   		<td><div class="HideUpdate_<?=USERTYPE_PARENT?>" style="display:none">--</div><div class="ShowUpdate_<?=USERTYPE_PARENT?>"><?=GetPeriodicalChangeInput(USERTYPE_PARENT, $Parent_Period)?></div></td>
	   	</tr>
<?php if ($special_feature['alumni']) { ?>
	   	<tr class="row_avaliable">
	   		<td><?=$Lang['Identity']['Alumni']?></td>
	   		<td><?=$linterface->Get_Checkbox("CanUpdatePassword_".USERTYPE_ALUMNI, "CanUpdatePassword_".USERTYPE_ALUMNI, "1", $Alumni_2Change, $Class='', $Display='', "ToggleColumns(".USERTYPE_ALUMNI.")", $Disabled='')?></td>
	   		<td><div class="HideUpdate_<?=USERTYPE_ALUMNI?>" style="display:none">--</div><div class="ShowUpdate_<?=USERTYPE_ALUMNI?>"><?=GetComplexPasswordInput(USERTYPE_ALUMNI, $Alumni_2Complex)?></div></td>
	   		<td><div class="HideUpdate_<?=USERTYPE_ALUMNI?>" style="display:none">--</div><div class="ShowUpdate_<?=USERTYPE_ALUMNI?>"><?=GetPeriodicalChangeInput(USERTYPE_ALUMNI, $Alumni_Period)?></div></td>
	   	</tr>
<?php } ?>
<?php endif;?>
	   </tbody>
	</table>
	
	
	<span class="tabletextremark">
	<span class="tabletextrequire">#</span> 
	<?=$Lang['SysMgr']['Security']['remark_complex_password']?>
	 </span>
	
	<div class="edit_bottom_v30">
	                      	<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "submit") ?>
	<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "ToggleEditView(2)") ?>
	<p class="spacer"></p>
	</div>


</div>


 
<script language="javascript">
function ToggleEditView(parType)
{
	if (parType==1)
	{
		$("#LayerView").hide();
		$("#LayerEdit").show();
		ToggleColumns(1);
		ToggleColumns(2);
		ToggleColumns(3);
		ToggleColumns(4);
	} else
	{
		$("#LayerEdit").hide();
		$("#LayerView").show();
	}
}

function ToggleColumns(parType)
{
	if ($("#CanUpdatePassword_"+parType).is(':checked'))
	{
		$('.HideUpdate_'+parType).hide();
		$('.ShowUpdate_'+parType).show();
	} else
	{
		$('.ShowUpdate_'+parType).hide();
		$('.HideUpdate_'+parType).show();
	}
}
</script>

</form>


<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>