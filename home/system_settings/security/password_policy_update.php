<?php
# using: yuen

/************************************
 *  Modification log
 *
 *	2014-03-03	Yuen
 *	- added this setting
 * **********************************/

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth(); 
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$data = array();


$thisUserType = USERTYPE_STAFF;

##### Login Password
$data['CanUpdatePassword_'.$thisUserType] = ${"CanUpdatePassword_".$thisUserType};

##### Enable Password Policy (length value)
$data['EnablePasswordPolicy_'.$thisUserType] = ${"EnablePasswordPolicy_".$thisUserType};


##### Require password update period
$data['RequirePasswordPeriod_'.$thisUserType] = ${"RequirePasswordPeriod_".$thisUserType};


$thisUserType = USERTYPE_STUDENT;

##### Login Password
$data['CanUpdatePassword_'.$thisUserType] = ${"CanUpdatePassword_".$thisUserType};

##### Enable Password Policy (length value)
$data['EnablePasswordPolicy_'.$thisUserType] = ${"EnablePasswordPolicy_".$thisUserType};


##### Require password update period
$data['RequirePasswordPeriod_'.$thisUserType] = ${"RequirePasswordPeriod_".$thisUserType};



$thisUserType = USERTYPE_PARENT;

##### Login Password
$data['CanUpdatePassword_'.$thisUserType] = ${"CanUpdatePassword_".$thisUserType};

##### Enable Password Policy (length value)
$data['EnablePasswordPolicy_'.$thisUserType] = ${"EnablePasswordPolicy_".$thisUserType};


##### Require password update period
$data['RequirePasswordPeriod_'.$thisUserType] = ${"RequirePasswordPeriod_".$thisUserType};



$thisUserType = USERTYPE_ALUMNI;

##### Login Password
$data['CanUpdatePassword_'.$thisUserType] = ${"CanUpdatePassword_".$thisUserType};

##### Enable Password Policy (length value)
$data['EnablePasswordPolicy_'.$thisUserType] = ${"EnablePasswordPolicy_".$thisUserType};


##### Require password update period
$data['RequirePasswordPeriod_'.$thisUserType] = ${"RequirePasswordPeriod_".$thisUserType};


# store in DB
$lgeneralsettings = new libgeneralsettings();
$lgeneralsettings->Save_General_Setting("UserInfoSettings", $data);


intranet_closedb();
header("Location: password_policy.php?xmsg=UpdateSuccess");
?>