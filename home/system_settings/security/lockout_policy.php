<?php
// using: 

############# Change Log [Start] ################
#	2014-03-03 Yuen - introduced this setting in front-end
############# Change Log [End] ################

 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

// setting top menu highlight
$CurrentPageArr['SystemSecurity'] = 1;
$CurrentPageArr['SystemSecurity_Lockout'] = 1;

### Title ###
$MODULE_OBJ['title'] = $Lang['SysMgr']['Security']['Title'];
$TAGS_OBJ[] = array($Lang['SysMgr']['Security']['PasswordPolicies'], "password_policy.php", $CurrentPageArr['SystemSecurity_Password']);
$TAGS_OBJ[] = array($Lang['SysMgr']['Security']['AccountLockoutPolicy'], "lockout_policy.php", $CurrentPageArr['SystemSecurity_Lockout']);
$TAGS_OBJ[] = array($Lang['SystemSetting']['LoginRecords']['Title'], "login_records.php", $CurrentPageArr['SystemSecurity_LoginRecords']);
if($sys_custom['project']['HKPF']){
    $TAGS_OBJ[] = array($Lang['SystemSetting']['AccountLog']['Title'], "account_log.php", $CurrentPageArr['SystemSecurity_AccountLog']);
    $TAGS_OBJ[] = array($Lang['SystemSetting']['ReportLog']['Title'], "report_log.php", $CurrentPageArr['SystemSecurity_ReportLog']);
}
//$TAGS_OBJ[] = array($Lang['SysMgr']['RoleManagement']['ModuleRole'],"module_role_index.php",0);






$libau = new libauth();
$login_attempt_limit = $libau->LoginAttemptLimit;
$login_lock_duration = $libau->LoginLockDuration;

if ($login_attempt_limit<=0)
{
	$login_attempt_limit = -1;
}

$Options["Attempt"][] = array(-1, $Lang['SysMgr']['Security']['NoAttemptLimit']);
for ($i=1; $i<=30; $i++)
{
	$Options["Attempt"][] = array($i, $i);
}

$login_attempt_limit_html = $linterface->GET_SELECTION_BOX($Options["Attempt"], " id='login_attempt_limit' name='login_attempt_limit' onChange='ToggleDurationRow()'", "", $login_attempt_limit);



$Options["Duration"][] = array(5, 5);
$Options["Duration"][] = array(10, 10);
$Options["Duration"][] = array(15, 15);
$Options["Duration"][] = array(20, 20);
$Options["Duration"][] = array(30, 30);
$Options["Duration"][] = array(40, 40);
$Options["Duration"][] = array(50, 50);
$Options["Duration"][] = array(60, 60);
$Options["Duration"][] = array(90, 90);
$Options["Duration"][] = array(120, 120);
$Options["Duration"][] = array(150, 150);
$Options["Duration"][] = array(180, 180);
$Options["Duration"][] = array(210, 210);
$Options["Duration"][] = array(240, 240);
$Options["Duration"][] = array(270, 270);
$Options["Duration"][] = array(300, 300);
$Options["Duration"][] = array(360, 360);


$login_lock_duration_html = $linterface->GET_SELECTION_BOX($Options["Duration"], " id='login_lock_duration' name='login_lock_duration' ", "", $login_lock_duration);


$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>



<form name="form1" method="post" action="lockout_policy_update.php" >


<fieldset class="instruction_box_v30">
   	 <legend><?=$Lang['General']['Important']?></legend>
	<?=$Lang['SysMgr']['Security']['Note']?>
</fieldset>
 
 

<div class="form_content" id="LayerView">

	<div class="table_row_tool row_content_tool">
			<?= $linterface->GET_BTN("&nbsp; ".$Lang['Btn']['Edit']." &nbsp;", "button", "ToggleEditView(1)") ?>
		</div>
		<br />
		<br />
		
	<table class="form_table_v30">
		<tbody>
<?php if ($login_attempt_limit == -1) { ?>
		
	   	<tr>
	   		<td class="field_title"><?=$Lang['login']['attempt_limit']?></td>
			<td><font color="red"><?=$Lang['SysMgr']['Security']['NoAttemptLimit']?></font></td>
	   	</tr>
<?php } else { ?>	
	   	<tr>
	   		<td class="field_title"><?=$Lang['login']['attempt_limit']?> <span class="tabletextrequire">#</span></td>
			<td><font size="4"><?=$login_attempt_limit?></font> <?=$Lang['login']['unit_time']?></td>
	   	</tr>
	   	<tr>
	   		<td class="field_title"><?=$Lang['login']['lock_duration']?></td>
			<td><font size="4"><?=$login_lock_duration?></font> <?=$Lang['login']['unit_minute']?></td>
	   	</tr>
<?php } ?>
	   </tbody>
	</table>
	
	
<?php if ($login_attempt_limit != -1) { ?>
	<span class="tabletextremark">
	<span class="tabletextrequire">#</span> 
	<?=$Lang['login']['remark']?>
	 </span> 
 
<?php } ?>
</div>


<div class="form_content" id="LayerEdit" style="display:none">

	
	<table class="form_table_v30">
		<tbody>
	   	<tr>
	   		<td class="field_title"><?=$Lang['login']['attempt_limit']?> <span class="tabletextrequire">#</span></td>
			<td><?=$login_attempt_limit_html?> <?=$Lang['login']['unit_time']?></td>
	   	</tr>
	   	<tr id="layer_lock_duration">
	   		<td class="field_title"><?=$Lang['login']['lock_duration']?></td>
			<td><?=$login_lock_duration_html?>
				<?=$Lang['login']['unit_minute']?></td>
	   	</tr>
	   </tbody>
	</table>
	
	
	<span class="tabletextremark">
	<span class="tabletextrequire">#</span> 
	<?=$Lang['login']['remark']?>
	 </span> 
	
	
	
	<div class="edit_bottom_v30">
	                      	<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "submit") ?>
	<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "ToggleEditView(2)") ?>
	<p class="spacer"></p>
	</div>


</div>


<script language="javascript">
function ToggleEditView(parType)
{
	if (parType==1)
	{
		$("#LayerView").hide();
		$("#LayerEdit").show();
		ToggleDurationRow();
	} else
	{
		$("#LayerEdit").hide();
		$("#LayerView").show();
	}
}

function ToggleDurationRow()
{
	if ($("#login_attempt_limit").val()==-1)
	{
		$("#layer_lock_duration").hide();
	} else
	{
		$("#layer_lock_duration").show();
	}
}
</script>


</form>


<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>