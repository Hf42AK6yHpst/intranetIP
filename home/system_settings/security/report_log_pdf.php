<?php
$PATH_WRT_ROOT = "../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/lib.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libclubsenrol.php");
include_once ($PATH_WRT_ROOT . "includes/libclubsenrol_report.php");
include_once ($PATH_WRT_ROOT . "includes/libuser.php");
include_once ($PATH_WRT_ROOT . "includes/libuserlogreport.php");
// include_once($PATH_WRT_ROOT."includes/libsubjectpanel.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libclubsenrol_ui.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
// include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once ($PATH_WRT_ROOT . "includes/mpdf/mpdf.php");

intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();

$mpdf = new mPDF();

// Set mPDF Settings
$mpdf->allow_charset_conversion = true;
$mpdf->charset_in = "UTF-8";
$mpdf->list_auto_mode = "mpdf";
$mpdf->setAutoBottomMargin = 'stretch';
$mpdf->use_kwt = true;
$mpdf->splitTableBorderWidth = 0.1; // split 1 table into 2 pages add border at the bottom
                                    
// include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$linterface = new interface_html();
$ldb = new libdb();
$lu = new libuserlogreport();

if ($page_size_change == 1) {
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();

// $start = $_POST['start'];
// $end = $_POST['end'];
$identity = htmlspecialchars($_POST["Identity"]);
$start = htmlspecialchars($_POST["start"]);
$end = htmlspecialchars($_POST["end"]);

$now = time();
$today = date('Y-m-d', $now);
$nowTime = date('H:i:s', $now);

// Report Log Record
$reportLogArr = array(
    "Rtype" => "report",
    "Action" => "pdf",
    "Date" => $today,
    "Time" => $nowTime
);
//$report = $lu->insertLogReport($reportLogArr); 20181109 Not to show

$filterOpts = array();
$filterOpts['UserName'] = $filter_username;
$filterOpts['Action'] = $filter_Action;
$filterOpts['Rtype'] = $filter_Rtype;
if ($textFromDate == null && $textToDate == null){
    $filterOpts['DateStart'] = date("Y-m-d");
    $filterOpts['DateEnd'] = date("Y-m-d");
}
else{
    $filterOpts['DateStart'] = $textFromDate;
    $filterOpts['DateEnd'] = $textToDate;
}
$paging = array();
$paging['all'] = true;
// Ordering Option
$sortOpts = array();
$sortOpts['Field'] = $sortField;
$sortOpts['Order'] = $sortOrder;

// Get Recrd
$logs = $lu->getAllLogReport($filterOpts, $paging, $sortOpts);
unset($logs['PAGEMAX']);

$cols = array(
    array(
        'text' => '#',
        'style' => 'width: 2.1mm;'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['Date'],
        'style' => 'width: 25.2mm'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['Time'],
        'style' => 'width: 25.2mm'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['IPAddress'],
        'style' => 'width: 25.2mm'
    ),
    array(
        'text' => $Lang['SystemSetting']['ReportLog']['Column']['Rtype'],
        'style' => 'width: 25.2mm'
    ),
    array(
        'text' => $Lang['SystemSetting']['ReportLog']['Column']['Action'],
        'style' => 'width: 25.2mm'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['LogBylogin'],
        'style' => 'width: 27.3mm'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['LogBy'] . ' (En)',
        'style' => 'width: 27.3mm'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['LogBy'] . ' (Ch)',
        'style' => 'width: 27.3mm'
    )
);
$values = array("LogDate", "LogTime", "IPAddress", "Rtype", "Action", "UserLogin", "EnglishName", "ChineseName");
ini_set("memory_limit","1024M");
if ($sys_custom['project']['HKPF']) {
    set_time_limit(21600);
    ini_set('max_input_time', 21600);
}
$htmlLimit = 50000;
$resultHTML = array();

$resultTable = "<body>";
$resultTable .= "<table>";
$resultTable .= "<thead>";
$resultTable .= "<tr class='table_header'>";
foreach($cols as $col){
    $resultTable .= "<th style='$col[style]'>$col[text]</th>";
}
$resultTable .= "</tr>";
$resultTable .= "</thead>";

$resultTable .= "<tbody>";
/*
 * for ($j=0; $j<$numRow; $j++){
 * $resultTable .= "<tr>";
 * $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][0]. "</td>";
 * $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][1]. "</td>";
 * $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][2]. "</td>";
 * $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][3]. "</td>";
 * $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][4]. "</td>";
 * $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][5]. "</td>";
 * $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][6]. "</td>";
 * $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][7]. "</td>";
 * $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][8]. "</td>";
 * $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][9]. "</td>";
 * $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][10]. "</td>";
 * $resultTable .= "</tr>";
 * }
 */
if(sizeof($logs)==0){
    // No Data
    $resultTable .= "<tr>";
    $resultTable .= '<td align="center" colspan="' . sizeof($cols) . '">' . $i_no_record_exists_msg . '</td>';
    $resultTable .= "</tr>";
} else {
    $index = 1;
    foreach($logs as $log){
        $oddEven = $index % 2;
        $resultTable .= "<tr class='row$oddEven'>";
        $resultTable .= "<td>$index</td>";
        foreach($values as $value){
            if($value == 'Rtype'){
                $resultTable .= "<td>" . $Lang['SystemSetting']['ReportLog']['Rtype'][$log[$value]] . "</td>";
            } else if($value == 'Action'){
                $resultTable .= "<td>" . $Lang['SystemSetting']['ReportLog']['Action'][$log[$value]] . "</td>";
            } else{
                $resultTable .= "<td>" . $log[$value] . "</td>";
            }
        }
        $resultTable .= "</tr>";
        $index++;
        if(strlen($resultTable)>$htmlLimit){
            $resultHTML[] = $resultTable;
            $resultTable = "";
        }
    }
}
$resultTable .= "</tbody>";
$resultTable .= "</table>";
$resultTable .= "</body>";

$styleContent = <<< EOD
<style>
@page {
  size: A4;
  margin: 0;
  padding: 0;
  width: 100%;
}
html,body{
    height:297mm;
    width:210mm;
    padding: 0;
    margin: 0;
    font-family: mingliu;
}
.login_records table{
    width: 100%;
}
.row1{
    background-color: #FFFFFF;
}
.row0{
    background-color: #F1F1F1;
}
.table_header th{
    background-color: #487db4/* #4A7ECC*/;
    color: #FFFFFF;
}
</style>
EOD;
$mpdf->WriteHTML($styleContent, 1);
$header = <<<EOT
<table width="100%">
    <tbody>
    <tr>
    <td align="center"
        style="font-size: 5mm;font-weight: bold;">RESTRICTED</td>
    </tr>
    </tbody>
</table>
EOT;
$footer = <<<EOF
<table width="100%">
    <tbody>
    <tr>
    <td align="center"
        style="font-size: 5mm;font-weight: bold;">RESTRICTED</td>
    </tr>
    <tr>
    <td align=right style="vertical-align:bottom;">This report is printed at $today $nowTime</td>
    </tr>
    </tbody>
</table>
EOF;
$mpdf->SetHTMLHeader($header);
$mpdf->SetHTMLFooter($footer);
for($i=0; $i<sizeof($resultHTML); $i++){
    $mpdf->WriteHTML($resultHTML[$i]);
}
$mpdf->WriteHTML($resultTable);
$mpdf->Output("Report_Log.pdf", "I");
?>