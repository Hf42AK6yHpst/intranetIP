<?php 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libsubjectpanel.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$linterface = new interface_html();



if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}


intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();

// $start = $_POST['start'];
// $end = $_POST['end'];
$identity = htmlspecialchars($_POST["Identity"]);
$start = htmlspecialchars($_POST["start"]);
$end = htmlspecialchars($_POST["end"]);

$now = time();
$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));

$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));

if ($start == "" || $end == "")
{
    $start = $today;
    $end = $today;
}

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if ($field == "") $field = 1;
switch ($field){
    case 0: $field = 0; break;
    case 1: $field = 1; break;
    case 2: $field = 2; break;
    case 3: $field = 3; break;
    default: $field = 1; break;
}
$order = ($order == 1) ? 1 : 0;

$linterface = new interface_html();

// setting top menu highlight
$CurrentPageArr['SystemSecurity'] = 1;
$CurrentPageArr['SystemSecurity_LoginRecords'] = 1;

$username_field = getNameFieldWithLoginClassNumberByLang("b.");
if($identity)
{
    $identity_sql = " and b.RecordType IN ($identity) ";
}
$sql  = "SELECT
            b.UserLogin,
            CONCAT(	TRIM(CONCAT(IF(b.ChineseName IS NULL OR TRIM(b.ChineseName) = '',b.EnglishName,b.ChineseName), IF(b.TitleChinese!='', CONCAT('', b.TitleChinese), ''))),CONCAT(' ',IF(b.ClassNumber IS NULL OR b.ClassNumber = '','',CONCAT(' ,',b.ClassName,'-',b.ClassNumber)),'')),
			a.StartTime,
			a.DateModified
			,a.ClientHost
			,UNIX_TIMESTAMP(a.DateModified) - UNIX_TIMESTAMP(a.StartTime) as duration
		FROM
			INTRANET_LOGIN_SESSION as a
			LEFT OUTER JOIN INTRANET_USER as b ON (a.UserID = b.UserID)
		where
			a.StartTime>='$start 00:00:00' and a.DateModified<='$end 23:59:59'
			 $identity_sql
";
			 # TABLE INFO
			 //debug_pr($sql);
			 
			 $export='';
			 $buttonHref = '';
			 $export .= $linterface->Get_Content_Tool_v30('export', "javascript:goToExport();");
			 $htmlAry['toolbar'] = $export;
			 
			 $print='';
			 $print .= $linterface->Get_Content_Tool_v30('print', "javascript:goToPrint();");
			 $htmlAry['printToolBar'] = $print;
			 $li = new libdbtable2007($field, $order, $pageNo);
			 $li->field_array = array("b.UserLogin","CONCAT(	TRIM(CONCAT(IF(b.ChineseName IS NULL OR TRIM(b.ChineseName) = '',b.EnglishName,b.ChineseName), IF(b.TitleChinese!='', CONCAT('', b.TitleChinese), ''))),CONCAT(' ',IF(b.ClassNumber IS NULL OR b.ClassNumber = '','',CONCAT(' ,',b.ClassName,'-',b.ClassNumber)),''))","a.StartTime","a.DateModified","a.ClientHost", "duration");
			 $li->sql = $sql;
			 $li->no_col = sizeof($li->field_array)+1;
			 $li->title = "";
			 $li->column_array = array(0,0,0,0,0,9);
			 $li->wrap_array = array(0,0,0,0,0,0);
			 $li->IsColOff = 2;
			 
			 // TABLE COLUMN
			 $li->column_list .= "<td width=1% class='field_title'>#</td>\n";
			 $li->column_list .= "<td width=10% class='field_title'>".$li->column(0, $i_UserLogin)."</td>\n";
			 $li->column_list .= "<td width=10% class='field_title'>".$li->column(1, $i_UserName)."</td>\n";
			 $li->column_list .= "<td width=10% class='field_title'>".$li->column(2, $i_UsageStartTime)."</td>\n";
			 $li->column_list .= "<td width=10% class='field_title'>".$li->column(3, $i_UsageEndTime)."</td>\n";
			 $li->column_list .= "<td width=10% class='field_title'>".$li->column(4, $i_UsageHost)."</td>\n";
			 $li->column_list .= "<td width=20% class='field_title'>".$li->column(5, $i_UsageDuration)."</td>\n";
			 
			 
			 $datetype += 0;
			 $selected[$datetype] = "SELECTED";
			 
			 $classSelect = "<SELECT id=datetype name=datetype onChange=\"changeDateType(this.form)\">\n";
			 $classSelect .= "<OPTION value=0 ".$selected[0].">$i_Profile_Today</OPTION>\n";
			 $classSelect .= "<OPTION value=1 ".$selected[1].">$i_Profile_ThisWeek</OPTION>\n";
			 $classSelect .= "<OPTION value=2 ".$selected[2].">$i_Profile_ThisMonth</OPTION>\n";
			 $classSelect .= "<OPTION value=3 ".$selected[3].">$i_Profile_ThisAcademicYear</OPTION>\n";
			 $classSelect .= "</SELECT> \n";
			 
			 unset($selected);
			 $selected[$identity] = "SELECTED";
			 
			 $identitySelect = "<SELECT name=identity>\n";
			 $identitySelect .= "<OPTION value=0 ".$selected[0].">$button_select_all</OPTION>\n";
			 $identitySelect .= "<OPTION value=1 ".$selected[1].">$i_identity_teachstaff</OPTION>\n";
			 $identitySelect .= "<OPTION value=2 ".$selected[2].">$i_identity_student</OPTION>\n";
			 $identitySelect .= "<OPTION value=3 ".$selected[3].">$i_identity_parent</OPTION>\n";
			 $identitySelect .= "</SELECT> \n";
			 
			 $targetIdentity = array();
			 $targetIdentity[0] =  $i_identity_teachstaff;
			 $targetIdentity[1] =  $i_identity_student;
			 $targetIdentity[2] =  $i_identity_parent;
			 
			 
			 $identitySelection .= '<select id="targetIdentity" name="targetIdentity" multiple="" size=\"10\">';
			 $identitySelection .= " <option value=1>".$i_identity_teachstaff."</option>";
			 $identitySelection .= " <option value=2>".$i_identity_student."</option>";
			 $identitySelection .= " <option value=3>".$i_identity_parent."</option>";
			 $identitySelection .= '</select>';
			 $identitySelection .= "&nbsp;" . $linterface->Get_Small_Btn($Lang['Btn']['SelectAll'], "button", "js_Select_All_With_OptGroup('targetIdentity',1)");
			 $identitySelection .= $linterface->spacer();
			 $identitySelection .= $linterface->MultiSelectionRemark();
			 
			 //export.php?start=$start&end=$end&identity=$identity
			 $toolbar = $linterface->GET_ACTION_BTN($button_export, "button", "exportRecord()");
			 
			 
			 $divName = 'reportOptionOuterDiv';
			 $tdId = 'tdOption';
			 $x = '';
			 $x .= '<div id="showHideOptionDiv" style="display:none;">' . "\n";
			 $x .= '<table style="width:100%;">' . "\n";
			 $x .= '<tr>' . "\n";
			 $x .= '<td id="tdOption" class="report_show_option">' . "\n";
			 $x .= '<span id="spanShowOption_' . $divName . '">' . "\n";
			 $x .= $linterface->Get_Show_Option_Link("javascript:js_Show_Option_Div('$divName', '$tdId');", '', '', 'spanShowOption_reportOptionOuterDivBtn');
			 $x .= '</span>' . "\n";
			 $x .= '<span id="spanHideOption_' . $divName . '" style="display:none">' . "\n";
			 $x .= $linterface->Get_Hide_Option_Link("javascript:js_Hide_Option_Div('$divName', '$tdId');", '', '', 'spanHideOption_reportOptionOuterDivBtn');
			 $x .= '</span>' . "\n";
			 
			 $x .= '<div id="' . $divName . '" style="display:none;">' . "\n";
			 $x .= '</div>' . "\n";
			 $x .= '</td>' . "\n";
			 $x .= '</tr>' . "\n";
			 $x .= '</table>' . "\n";
			 $x .= '</div>' . "\n";
			 $htmlAry['reportOptionOuterDiv'] = $x;
			 
			 // Option Div
			 $divName = 'reportOptionDiv';
			 $x .= '<div id="' . $divName . '">' . "\n";
			 $x .= '<div>' . "\n";
			 $x .= '<div id="div_form" class="report_option report_hide_option">';
			 $x .= '<span id="spanHideOption" class="spanHideOption" style="display:none">';
			 $x .= '<a href="javascript:hideOptionLayer();">'.$Lang['Btn']['HideOption'].'</a>';
			 $x .= '</span>';
			 $x .= '<span id="spanShowOption" class="spanShowOption">';
			 $x .= '<a href="javascript:showOptionLayer();">'.$Lang['Btn']['ShowOption'].'</a>';
			 $x .= '</span>';
			 $x .='<p class="spacer"></p>';
			 $x .='<span class="Form_Span" style="display:none">';
			 $x .= '<div id="showHideOptionDiv" style="display:none;">' . "\n";
			 $x .= '<table id="filterOptionTable" class="form_table_v30">' . "\n";
			 $x .= '<table style="width:100%;">' . "\n";
			 $x .= "<tr><td class=\"field_title\">$i_Profile_SelectSemester:</td><td>$i_Profile_From <input id=start type=text name=start size=10 value='$start'> $i_Profile_To ";
			 $x .= "<input type=text id=end name=end size=10 value='$end'> <span class=extraInfo>(yyyy-mm-dd) </span>";
			 $x .= '</span>' . "\n";
			 $x .= "</td></tr>";
			 $x .= "<tr>";
			 $x .= "<td class=\"field_title\">$i_identity:</td>";
			 $x .= "<td>". $identitySelection."</td>";
			 $x .= "</tr>";
			 $x .= "</table>";
			 $x .= "</span>";
			 $x .= "</div>";
			 /**
			  * End of Category selector
			  */
			 $x .= '<br style="clear:both;" />' . "\n";
			 
			 $x .= '<div style="text-align:left;">' . "\n";
			 $x .= $linterface->MandatoryField();
			 $x .= '</div>' . "\n";
			 $x .= '<br style="clear:both;" />' . "\n";
			 
			 $x .= '<div class="edit_bottom_v30">' . "\n";
			 // debug_pr($Stringstart);
			 $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "refreshReport();");
			 $x .= '</div>' . "\n";
			 $x .= '</div>' . "\n";
			 $htmlAry['reportOptionDiv'] = $x;
			 ?>
     <link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
     <script LANGUAGE="javascript">
         var css_array = new Array;
         css_array[0] = "dynCalendar_free";
         css_array[1] = "dynCalendar_half";
         css_array[2] = "dynCalendar_full";
         var date_array = new Array;
     </script>

     <script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
     <script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
     <script type="text/javascript">
     <!--
          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                   if (String(month).length == 1) {
                       month = '0' + month;
                   }

                   if (String(date).length == 1) {
                       date = '0' + date;
                   }
                   dateValue =year + '-' + month + '-' + date;
                   document.forms['remove'].RemoveFrom.value = dateValue;
          }
          
          function changeDateType(obj)
			{
			         switch (obj.datetype.value)
			         {
			                 case '0':
			                      obj.start.value = '<?=$today?>';
			                      obj.end.value = '<?=$today?>';
			                      break;
			                 case '1':
			                      obj.start.value = '<?=$weekstart?>';
			                      obj.end.value = '<?=$weekend?>';
			                      break;
			                 case '2':
			                      obj.start.value = '<?=$monthstart?>';
			                      obj.end.value = '<?=$monthend?>';
			                      break;
			                 case '3':
			                      obj.start.value = '<?=$yearstart?>';
			                      obj.end.value = '<?=$yearend?>';
			                      break;
			         }
			         //obj.submit();
			}

     // -->
     </script>
     <script type="text/javascript">
     $(document).ready(function () {
     });

function refreshReport() {
    var Identity = $('#targetIdentity').val();
    var start = $('#start').val();
    var end = $('#end').val();
    if(Identity==null){
    alert("please select at least one identity");
    }else{    
    $('input#Identity').val(Identity);
	$('input#start').val(start);
	$('input#end').val(end);
    $('form#form2').attr('action','login_records.php').submit();
    }
}

</script>
<table width="100%" align="center" class="print_hide" border="0">
<tr>
<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>
		<div class="login_records">
			<?php echo $li->display(); ?>
		</div>
	<input type="hidden" id="Identity" Name="Identity" Value="">
	<input type="hidden" id="start" Name="start" Value="">
	<input type="hidden" id="end" Name="end" Value="">	
	<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
	<input type=hidden name=order value="<?php echo $li->order; ?>">
	<input type=hidden name=field value="<?php echo $li->field; ?>">
	<input type=hidden name=page_size_change value="">
	<input type=hidden name=numPerPage value="<?=$li->page_size?>">
	</form>
</div>
<style>
.function_bar1{
 //background-color: #fff9b2;
}
.function_bar2{
}
.login_records table{
    width: 100%;
}
.login_records table, .login_records table tr td{
    border: 0;
}
.login_records table, .login_records table tr:nth-child(odd){
    background-color: #FFFFFF;
}
.login_records table, .login_records table tr:nth-child(even){
    background-color: #F1F1F1;
}
.login_records table tr:nth-child(1) td{
    background-color: darkgrey;
    color: white;
}
.login_records table tr:nth-child(1) td a{
    text-decoration: none;
    color: white;
}
</style>
<script language="javascript">
function exportRecord(){
	window.open("<?="export.php?start=$start&end=$end&identity=$identity"?>");
}
</script>