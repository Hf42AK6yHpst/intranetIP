<?php
## Modifying By: 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libuserlogreport.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll_report = new libclubsenrol_report();
$lu = new libuserlogreport();
$ldb = new libdb();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$avgSourceAry = $_POST['exportAry'];
// debug_pr($avgSourceAry);
// $filter_username $filter_action $textFromDate $textToDate
// debug_pr($dataAry);
    //$htmlAry['resultTable'] = $libenroll_report->getStudentWithoutEnrolmentReportResultTableHtml($dataAry);

// Filter Option
$filterOpts = array();
$filterOpts['name'] = $filter_username;
$filterOpts['action'] = $filter_action;
if ($textFromDate == null && $textToDate == null){
    $filterOpts['DateStart'] = date("Y-m-d");
    $filterOpts['DateEnd'] = date("Y-m-d");
}
else{
    $filterOpts['DateStart'] = $textFromDate;
    $filterOpts['DateEnd'] = $textToDate;
}

// Get Current Time
$now = time();
$today = date('Y-m-d',$now);
$nowTime = date('H:i:s', $now);

// Report Log Record
$reportLogArr = array(
    "Rtype" => "account",
    "Action" => "html",
    "Date" => $today,
    "Time" => $nowTime
);
$report = $lu->insertLogReport($reportLogArr);

// Paging Option
$paging = array("all" => true);
// Sorting Options
$sortOpts = array();
$sortOpts['Field'] = $sortField;
$sortOpts['Order'] = $sortOrder;

// Get All the Log Records
$logs = $lu->getAccountLog($filterOpts, $paging, $sortOpts);
unset($logs['PAGEMAX']);

// Table Info
$resultTable = "";
$resultTable .= "<table class='common_table_list_v30 view_table_list_v30'>";

    // Table Header
    $resultTable .= "<thead>";
        $resultTable .= "<tr><th colspan='11'>RESTRICTED</th></tr>";
        $resultTable .= "<tr>";
            $resultTable .= "<th style=\"text-align:center;width:1%;\">#</th>";
            $resultTable .= "<th style=\"text-align:center;width:8%;\">".$Lang['SystemSetting']['AccountLog']['Date']."</th>";
            $resultTable .= "<th style=\"text-align:center;width:6%;\">".$Lang['SystemSetting']['AccountLog']['Time']."</th>";
            $resultTable .= "<th style=\"text-align:center;width:6%;\">".$Lang['SystemSetting']['AccountLog']['IPAddress']."</th>";
            $resultTable .= "<th style=\"text-align:center; width:11%;\">".$Lang['SystemSetting']['AccountLog']['LogBylogin']."</th>";
            $resultTable .= "<th style=\"text-align:center; width:12%;\">".$Lang['SystemSetting']['AccountLog']['LogBy'] . ' (En)'."</th>";
            $resultTable .= "<th style=\"text-align:center; width:11%;\">".$Lang['SystemSetting']['AccountLog']['LogBy'] . ' (Ch)'."</th>";
            $resultTable .= "<th style=\"text-align:center;width:6%;\">".$Lang['SystemSetting']['AccountLog']['Action']."</th>";
            $resultTable .= "<th style=\"text-align:center;width:11%;\">".$Lang['SystemSetting']['AccountLog']['Userlogin']."</th>";
            $resultTable .= "<th style=\"text-align:center;width:12%;\">".$Lang['SystemSetting']['AccountLog']['User'] . ' (En)'."</th>";
            $resultTable .= "<th style=\"text-align:center;width:11%;\">".$Lang['SystemSetting']['AccountLog']['User'] . ' (Ch)'."</th>";
        $resultTable .= "</tr>";
    $resultTable .= "</thead>";

    // Table Body
    $values = array(
        "", "LogDate", "LogTime", "IPAddress", "LogBy", "LogEnglishName",
        "LogChineseName", "Action", "UserLogin", "EnglishName", "ChineseName"
    );
       $resultTable .= "<tbody>";
        if (sizeof($logs) == 0) {
            $resultTable .= "<tr>";
            $resultTable .= '<td align="center" colspan="' . sizeof($cols) . '">' . $i_no_record_exists_msg . '</td>';
            $resultTable .= "</tr>";
        } else {
            // Print every record into a table row
            $index = 1;
            foreach($logs as $log){
                $resultTable .= "<tr>";
                foreach($values as $val){
                    if($val == 'Action'){
                        $resultTable .= "<td>" . decodeCRUD($log[$val]) . "</td>";
                    } else if($val != ''){
                        $resultTable .= "<td>" . $log[$val] . "</td>";
                    } else {
                        $resultTable .= "<td>" . $index . "</td>";
                    }
                }
                $resultTable .= "</tr>";
                $index++;
            }
        }
        $resultTable .= "</tbody>";
        
        // Table Footer
        $resultTable .= "<tfoot>";
            $resultTable .= "<tr ><td colspan='11' style='border: 0;'>RESTRICTED</td></tr>";
            $resultTable .= "<tr><td colspan='11' style='border: 0;' align='right'>This report is printed at $today $nowTime</td></tr>";
        $resultTable .= "</tfoot>";
        
    $resultTable .= "</table>";
?>
<style>
@page {
  size: A4;
  margin: 0;
  width: 100%;
}
html,body{
    height:297mm;
    width:210mm;
}
table thead tr:nth-child(1) th, table tfoot tr:first-child td{
    text-align: center;
    font-size: 5mm;
    font-weight: bold;
}
table thead tr:nth-child(2) th{
    background-color: #487db4/* #4A7ECC*/;
    color: #FFFFFF;
}
table tr, table th, table td{
    border-collapse: collapse;
}
</style>
<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><? echo $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>
<? echo ($resultTable)?>
<?
intranet_closedb();
function decodeCRUD($target){
    $result = "";
    global $Lang;
    switch($target){
        case 'C':
            $result = $Lang['SystemSetting']['AccountLog']['Create'];
            break;
        case 'R':
            $result = $Lang['SystemSetting']['AccountLog']['Read'];
            break;
        case 'U':
            $result = $Lang['SystemSetting']['AccountLog']['Update'];
            break;
        case 'D':
            $result = $Lang['SystemSetting']['AccountLog']['Delete'];
            break;
        default:
            break;
    }
    return $result;
}
?>