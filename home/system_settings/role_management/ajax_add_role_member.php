<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/role_manage.php");

$RoleID = $_REQUEST['RoleID'];
$AddUserID = $_REQUEST['AddUserID'];

intranet_opendb();

$RoleManage = new role_manage();

$RoleManage->Start_Trans();

$Result = $RoleManage->Add_Role_Member($RoleID,$AddUserID);

if ($Result) {
	echo $Lang['SysMgr']['RoleManagement']['AddRoleMemberSuccess'];
	$RoleManage->Commit_Trans();
}
else {
	echo $Lang['SysMgr']['RoleManagement']['AddRoleMemberUnsuccess'];
	$RoleManage->RollBack_Trans();
}

intranet_closedb();
?>