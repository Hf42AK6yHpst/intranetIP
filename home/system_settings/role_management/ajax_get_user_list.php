<?php
// using

############# Change Log [Start] ################
#
#   2020-01-15  Sam
#   - $ParentStudentID variable seems to be reset to other value after include_once statement  [2020-0113-1417-28073]
#     added _ as prefix to all POST variables
#
############# Change Log [End] ################

$_YearClassID = $_REQUEST['YearClassID'];
$_IdentityType = $_REQUEST['IdentityType'];
$_AddUserID = (is_array($_REQUEST['AddUserID']))? $_REQUEST['AddUserID']:array();
$_ParentStudentID = $_REQUEST['ParentStudentID'];
$_RoleID = $_REQUEST['RoleID'];
/*echo '<pre>';
var_dump($_REQUEST);
echo '</pre>';
die;*/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/role_manage_ui.php");

intranet_opendb();


$RoleManageUI = new role_manage_ui();

echo $RoleManageUI->Get_User_Selection($_AddUserID,$_IdentityType,$_YearClassID,$_ParentStudentID,$_RoleID);

intranet_closedb();
?>
