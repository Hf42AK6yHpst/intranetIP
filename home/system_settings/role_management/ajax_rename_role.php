<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/role_manage.php");

$RoleName = trim(stripslashes(urldecode($_REQUEST['RoleName'])));
$RoleID = $_REQUEST['RoleID'];

if ($RoleName != "") {
	intranet_opendb();
	
	$RoleManage = new role_manage();
	
	$RoleManage->Start_Trans();
	
	$Result = $RoleManage->Rename_Role($RoleID,$RoleName);
	
	if ($Result) {
		echo $Lang['SysMgr']['RoleManagement']['RoleRenamedSuccess'];
		$RoleManage->Commit_Trans();
	}
	else {
		echo $Lang['SysMgr']['RoleManagement']['RoleRenamedUnsuccess'];
		$RoleManage->RollBack_Trans();
	}
	
	intranet_closedb();
}
else {
	echo $Lang['SysMgr']['RoleManagement']['RoleRenamedUnsuccess'];
}
?>