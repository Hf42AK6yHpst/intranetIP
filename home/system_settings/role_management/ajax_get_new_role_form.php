<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/role_manage_ui.php");

intranet_opendb();

$RoleManageUI = new role_manage_ui();

echo $RoleManageUI->Get_Role_Form();

intranet_closedb();
?>