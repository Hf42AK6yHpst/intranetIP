<?php
// using
/**
 * Date : 2018-08-15 (Henry)
 * fixed sql injection
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/role_manage_ui.php");

$RoleID = $_REQUEST['RoleID'];
$RoleID = IntegerSafe($RoleID);

intranet_opendb();

$RoleManageUI = new role_manage_ui();

echo $RoleManageUI->Get_Add_Role_Member_Form($RoleID);

intranet_closedb();
?>