<?php
// using:

############# Change Log [Start] ################
#
#	Date:	05/07/2018 Vito
#			add export function
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/role_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) {
    header ("Location: /");
    intranet_closedb();
    exit();
}

$Mode_Ex = $_REQUEST['Mode'];
//debug_pr($_REQUEST['Mode']);
$RoleUI_Ex = new role_manage_ui();

$RoleUI_Ex->Get_Role_Detail_Ex($Mode_Ex);

intranet_closedb();
?>