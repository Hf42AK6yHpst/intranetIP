<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/role_manage_ui.php");

$RoleType = $_REQUEST['RoleType'];

intranet_opendb();

$RoleManageUI = new role_manage_ui();

if ($RoleType == "")
	echo $RoleManageUI->Get_Role_List_Table_All();
else
	echo $RoleManageUI->Get_Role_List_Table($RoleType);

intranet_closedb();
?>