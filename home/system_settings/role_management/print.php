<?php
// using: kenneth Chung 

############# Change Log [Start] ################
#
#	Date:	2010-06-22	YatWoon
#			add online help button
#
/*
 *	26-01-2010 Ivan
 *	- For seraching user , Show ClassName and ClassNumber for Student only
 */
############# Change Log [End] ################

 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/role_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$Mode = $_REQUEST['Mode'];
// debug_pr($Mode);
$RoleUI = new role_manage_ui();

$RoleUI->Get_Role_Detail($Mode);


intranet_closedb();
?>