<?php
// using 
/**
 * 2018-08-10 (Henry): fixed sql injection 
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/role_manage_ui.php");

$RoleID = IntegerSafe($_REQUEST['RoleID']);
$Show = $_REQUEST['Show'];

intranet_opendb();

$RoleManageUI = new role_manage_ui();

echo $RoleManageUI->Get_Role_Edit_Form($RoleID,$Show);

intranet_closedb();
?>