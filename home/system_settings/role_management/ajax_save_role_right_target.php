<?php
// using 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/role_manage.php");

$RoleID = $_REQUEST['RoleID'];
$RoleRight = $_REQUEST['Right'];
/*`$RoleTarget['Teaching'] = $_REQUEST['TeachingTarget'];
$RoleTarget['NonTeaching'] = $_REQUEST['NonTeachingTarget'];
$RoleTarget['Student'] = $_REQUEST['StudentTarget'];
$RoleTarget['Parent'] = $_REQUEST['ParentTarget'];*/
$RoleTarget = $_REQUEST['Target'];
$RightOrTarget = $_REQUEST['RightOrTarget'];

/*echo '<pre>';
var_dump($_REQUEST);
echo '</pre>';
die;*/
intranet_opendb();

$RoleManage = new role_manage();

$RoleManage->Start_Trans();

$Result = $RoleManage->Save_Role_Right_Target($RoleID,$RoleRight,$RoleTarget,$RightOrTarget,$AllTarget);

if ($Result === false) {
	echo $Lang['SysMgr']['RoleManagement']['RoleSavedUnsuccess'];
	$RoleManage->RollBack_Trans();
}
else {
	echo $Lang['SysMgr']['RoleManagement']['RoleSavedSuccess'];
	$RoleManage->Commit_Trans();
}

intranet_closedb();
?>