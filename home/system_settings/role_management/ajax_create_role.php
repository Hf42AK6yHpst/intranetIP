<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/role_manage.php");

$RoleName = trim(stripslashes(urldecode($_REQUEST['RoleName'])));
//$RoleType = trim(stripslashes(urldecode($_REQUEST['RoleType'])));
$EditAfterCreate = $_REQUEST['EditAfterCreate'];
/*
echo '<pre>';
var_dump($_REQUEST);
echo '</pre>';
die;*/
intranet_opendb();



$RoleManage = new role_manage();

$RoleManage->Start_Trans();

$Result = $RoleManage->Create_Role($RoleName);

if ($Result === false) {
	if ($EditAfterCreate) {
		echo 'false~~~~~';
	}
	echo $Lang['SysMgr']['RoleManagement']['RoleCreatedUnsuccess'];
	$RoleManage->RollBack_Trans();
}
else {
	if ($EditAfterCreate) {
		echo $Result.'~~~~~';
	}
	echo $Lang['SysMgr']['RoleManagement']['RoleCreatedSuccess'];
	$RoleManage->Commit_Trans();
}

intranet_closedb();
?>