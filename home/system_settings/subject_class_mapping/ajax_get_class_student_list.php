<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

intranet_opendb();

$YearClassID = $_REQUEST['YearClassID'];
$YearTermID = $_REQUEST['YearTermID'];
$SubjectID = $_REQUEST['SubjectID'];
$SubjectGroupID = $_REQUEST['SubjectGroupID'];
$SelectedStudentList = (is_array($_REQUEST['StudentSelected']))? $_REQUEST['StudentSelected']:array();
$IncludeFormID = $_REQUEST['IncludeFormID'];

/*echo '<pre>';
var_dump($_REQUEST);
echo '</pre>';
die;*/
$SubjectClassMappingUI = new subject_class_mapping_ui();

echo $SubjectClassMappingUI->Get_Class_Student_Selection($YearClassID,$YearTermID,$SubjectID,$SelectedStudentList,$SubjectGroupID,$IncludeFormID=1);

intranet_closedb();
?>