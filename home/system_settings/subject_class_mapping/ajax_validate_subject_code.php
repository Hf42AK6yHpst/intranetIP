<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);
$InputValue = stripslashes($_REQUEST['InputValue']);
$TargetID = stripslashes($_REQUEST['TargetID']);
$ParentSubjectID = stripslashes($_REQUEST['ParentSubjectID']);


# Initialize library
if ($RecordType == "Subject")
{
	$libSubject = new subject($TargetID);
}
else if ($RecordType == "SubjectComponent")
{
	$libSubject = new Subject_Component($TargetID);
}

$result = $libSubject->Is_Code_Existed($InputValue, $TargetID, $ParentSubjectID);

if ($result != "")
	echo $Lang['SysMgr']['SubjectClassMapping']['Warning']['DuplicatedCode'][$result];
else
	echo "";


intranet_closedb();

?>