<?php
// using : 

############# Change Log [Start] ################
#  Date:	2020-05-07 Tommy
#           - modified access checking, added $plugin["Disable_Subject"]
#  Date:    2018-07-09  Marco Yu
#           Add Export Function
#  Date:	2015-09-04	Ivan
#			Used Get_SchoolSettings_Subject_Tab_Array() to get the tag array
#			ip.2.5.6.10.1	*** Need to upload with "/includes/subject_class_mapping_ui.php" before deployment ***
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$SubjecClassUI = new subject_class_mapping_ui();
$linterface = new interface_html();
$CurrentPageArr['Subjects'] = 1;
### Title ###
//$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['Subject&ComponentTitle'],"index.php",0);
//$TAGS_OBJ[] = array($Lang['formClassMapping']['FormSubject'],"form_subject.php",1);
//$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'],"subject_group_mapping.php",0);
$TAGS_OBJ = $SubjecClassUI->Get_SchoolSettings_Subject_Tab_Array('subjectForm');
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
//debug_pr($TAG_OBJ);

# online help button
//$onlineHelpBtn = gen_online_help_btn_and_layer('school_settings','subject');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

$linterface->LAYOUT_START(); 

// $libLC = new Learning_Category();
// $LC_InfoArr = $libLC->Get_All_Learning_Category();
// debug_pr($LC_InfoArr);
// $libSubject = new Subject();
// $subjectInfoArr = $libSubject->Get_Subject_List($returnAsso=1);
// debug_pr($subjectInfoArr);

$include_JS_CSS = $SubjecClassUI->Include_JS_CSS();

## Export Btn
$x='';
$buttonHref = '';
$x .= $linterface->Get_Content_Tool_v30('export', "javascript:goToExport();");
$htmlAry['toolbar'] = $x;

## Content Table
$FormSubjectDiv .= $SubjecClassUI->Get_Form_Subject_Component_Setting_Table();
?>

<div class="Conntent_tool">
		<?=$htmlAry['toolbar']?>
</div>

<span id="spanOuter">
	<?=$include_JS_CSS?>
	<?= $FormSubjectDiv ?>
</span>
<?
$linterface->LAYOUT_STOP();

intranet_closedb();
?>

<script>

function Edit_Form_Subject(YearID) {
	Block_Thickbox();


	var PostString = "&YearID="+YearID;
	EditFormSubjectAjax = GetXmlHttpObject();
		   
	if (EditFormSubjectAjax == null)
	{
		alert (errAjax);
		return;
	} 
	    
	var url = 'ajax_edit_form_subject.php';
	var PostString = "&YearID="+YearID;
	var PostValue = PostString;
	EditFormSubjectAjax.onreadystatechange = function() {

		if (EditFormSubjectAjax.readyState == 4) {
			//ResponseText = Trim(EditFormSubjectAjax.responseText);
			document.getElementById('TB_ajaxContent').innerHTML = EditFormSubjectAjax.responseText;
			//Init_JQuery_AutoComplete('StudentSearch',AcademicYearID,ClassID);
		}
		
	};
	EditFormSubjectAjax.open("POST", url, true);
	EditFormSubjectAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	EditFormSubjectAjax.send(PostValue);
	
}

function Thick_Box_Init()
{
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}

function Form_Subject_Action(YearID) {
	Block_Thickbox();

	var PostString = "&YearID="+YearID;
	EditFormSubjectAjax = GetXmlHttpObject();
		   
	if (EditFormSubjectAjax == null)
	{
		alert (errAjax);
		return;
	} 
	    
	var url = 'ajax_submit_form_subject.php';
	var PostString = Get_Form_Values(document.getElementById("Form_FormSubject"));
	PostString += "&YearID="+YearID;
	alert(PostString); 
	
	var PostValue = PostString;
	EditFormSubjectAjax.onreadystatechange = function() {

		if (EditFormSubjectAjax.readyState == 4) {
			//alert(EditFormSubjectAjax.responseText);
			Get_Return_Message(EditFormSubjectAjax.responseText);
			Refresh_Form_Class();
			window.top.tb_remove();	
		}
	};
	EditFormSubjectAjax.open("POST", url, true);
	EditFormSubjectAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	EditFormSubjectAjax.send(PostValue);	
}

function Update_Form_Subject() {

	var PostString = Get_Form_Values(document.getElementById("FormSubject"));
	var PostVar = PostString;

	Block_Element("spanOuter");
	$.post('ajax_submit_form_subject.php',PostVar,
		function(data){
			//alert(data);
			UnBlock_Element("spanOuter");
			if (data == "die") 
				window.top.location = '/';
			else {
				Get_Return_Message(data);
				Reload_Form_Subject();
			}
		});
}

function Refresh_Form_Class()
{
	var PostVar = "";

	Block_Element("spanFormSubject");
	$.post('ajax_get_form_subject.php',PostVar,
					function(data){
						//alert(data);
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#spanFormSubject').html(data);
							Thick_Box_Init();
							UnBlock_Element("spanFormSubject");
						}
					});
}


function Reload_Form_Subject() {

	FormSubjectAjax = GetXmlHttpObject();
		   
  if (FormSubjectAjax == null)
  {
    alert (errAjax);
    return;
  } 
    
  var url = 'ajax_get_form_subject.php';
  
	FormSubjectAjax.onreadystatechange = function() {
		if (FormSubjectAjax.readyState == 4) {
			document.getElementById('spanOuter').innerHTML = FormSubjectAjax.responseText;
			
			Thick_Box_Init();
		}
	};
	FormSubjectAjax.open("POST", url, true);
	FormSubjectAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	FormSubjectAjax.send();
	
}

function js_Select_All(jsChecked) {
	var emt = document.FormSubject.elements;
	
	for(i=0; i<emt.length; i++) {
		document.FormSubject.elements[i].checked = jsChecked;
	}
}

function rowChecked(emt, jsChecked) {
	
	var allEmt = document.FormSubject.elements;
	var checked = "";
	
	for(i=0; i<allEmt.length; i++) {
		if(document.FormSubject.elements[i].type=='checkbox') {
			if(checked!="" && checked!=document.getElementById(emt).parentNode.id)
				break;
			else if(document.getElementById(emt).parentNode.id==document.FormSubject.elements[i].parentNode.parentNode.parentNode.id) {
				document.FormSubject.elements[i].checked = jsChecked;
				checked = document.getElementById(emt).parentNode.id;
			}
		}
	}
}

function colChecked(emt, jsChecked) {
	
	var allEmt = document.FormSubject.elements;
	var s = "";
	
	for(i=0; i<allEmt.length; i++) {
		if(document.FormSubject.elements[i].type=='checkbox') {
			s = emt.length;

			if(document.FormSubject.elements[i].id.substr(0,12)=="SubjectGroup" && document.FormSubject.elements[i].id.substring((document.FormSubject.elements[i].id.length-(s+1)))=="_"+emt) {
				document.FormSubject.elements[i].checked = jsChecked;
			}
		}
	}
}

function  goToExport(){
	$('form#form').attr('action', 'form_subject_export.php').submit();
}

</script>
<form name="form" id="form" method="POST" action="form_subject_export.php">
</form>