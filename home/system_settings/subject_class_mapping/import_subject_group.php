<?php
// Modifying by: 
############# Change Log [Start] ################
#
#	Date:	2020-05-07 Tommy
#           - modified access checking, added $plugin["Disable_Subject"]
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) { 
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

if ($AcademicYearID == '')
	$AcademicYearID = Get_Current_Academic_Year_ID();
$obj_AcademicYear = new academic_year($AcademicYearID);

if ($YearTermID == '')
	$YearTermID = getCurrentSemesterID();
$obj_YearTerm = new academic_year_term($YearTermID);

### Title / Menu
$CurrentPageArr['Subjects'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['ImportSubjectGroup']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# form class csv sample
if ($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent'] == true)
	$SampleFile = 'import_subject_group_sample_with_component.csv';
else
	$SampleFile = 'import_subject_group_sample.csv';
$csvFile = "<a class='tablelink' href='". GET_CSV($SampleFile) ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";

// Data Column
$delim = "<br>";
for($i=0; $i<sizeof($Lang['SysMgr']['SubjectClassMapping']['ImportCSVField']); $i++){
	if($i!=0) $csv_format .= $delim;
	$csv_format .= $Lang['General']['ImportArr']['Column']." ".numberToLetter($i+1, true)." : ".$Lang['SysMgr']['SubjectClassMapping']['ImportCSVField'][$i];
}

// Add Remark: no space
$format_str = "<b>".$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeRemark']."</b>".$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeNoSpace'];

echo $linterface->Include_JS_CSS();

?>

<br />
<form name="form1" method="post" action="import_subject_group_confirm.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	
	
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="formfieldtitle" width="30%" align="left"><?=$Lang['SysMgr']['FormClassMapping']['SchoolYear']?></td>
					<td class="tabletext" ><?=$obj_AcademicYear->Get_Academic_Year_Name()?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" width="30%" align="left"><?=$Lang['SysMgr']['SubjectClassMapping']['Term']?></td>
					<td class="tabletext" ><?=$obj_YearTerm->Get_Year_Term_Name()?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
					<td class="tabletext"><input class="file" type="file" name="csvfile"></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
					<td class="tabletext"><?=$csvFile?></td>
				</tr>
				<!--	Import CSV format	-->
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['ImportArr']['DataColumn']?></td>
					<td class="tabletext" id="csvFormat"><?=$csv_format?></td>
				</tr>
				<!--tr>
					<td class="tabletext" align="left" colspan="2"><?=$iDiscipline['Import_Source_File_Note']?></td>
				</tr-->
				<tr>
					<td class="tabletext" colspan="2"><?=$format_str?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp; <?=$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='subject_group_mapping.php?AcademicYearID=$AcademicYearID&YearTermID=$YearTermID'")?>
		</td>
	</tr>
</table>
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
<input type="hidden" name="YearTermID" value="<?=$YearTermID?>">

</form>
<br />

<!--	Layer to show Subject Code and Subject Group Code	-->
<div id="DetailsLayer" class="selectbox_layer" style="visibility:hidden;">
	<div style="height:250px; width:550px; overflow:auto;">
		<table cellspacing="0" cellpadding="3" border="0" width="100%">
			<tbody>
				<tr>
					<td align="right" style="border-bottom: medium none;">
					<a href="javascript:js_Hide_Detail_Layer()"><img border="0" src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/btn_mini_off.gif"></a>
					</td>
				</tr>
				<tr>
					<td align="left" style="border-bottom: medium none;">
						<div id="DetailsLayerContentDiv"></div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div>
		<table cellspacing="0" cellpadding="3" border="0" width="100%">
			<tr>
				<td align='center' colspan='3' nowrap style='border-bottom:none;'><?=$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")?></td>
			</tr>
		</table>
	</div>
</div>

<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script language="javascript">

function js_Show_Detail_Layer(jsDetailType)
{	
	js_Hide_Detail_Layer();
	
	var jsAction = '';
	var AcademicYear = <?=$AcademicYearID?>;
	var YearTerm = <?=$YearTermID?>;
	if (jsDetailType == 1){
		jsAction = 'Reload_Subject_Code';
	}	
	else if (jsDetailType == 2){
		jsAction = 'Reload_Subject_Group_Code';
	}	

	$('div#DetailsLayerContentDiv').html('<?=$Lang['General']['Loading']?>');	
	js_Change_Layer_Position('csvFormat',-375,45);
	MM_showHideLayers('DetailsLayer','','show');
			
	$('div#DetailsLayerContentDiv').load(
		"ajax_subject_group_list_reload.php", 
		{ 
			Action: jsAction,
			AcademicYearID: AcademicYear,
			YearTermID: YearTerm
		},
		function(returnString)
		{
			$('div#DetailsLayerContentDiv').css('z-index', '999');
		}
	);
}

function js_Hide_Detail_Layer()
{
	MM_showHideLayers('DetailsLayer','','hide');
}

function js_Change_Layer_Position(jsClickedObjID,jsOffsetLeft,jsOffsetTop) 
{		
	var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') - jsOffsetLeft;
	var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') - jsOffsetTop;
	
	document.getElementById('DetailsLayer').style.left = posleft + "px";
	document.getElementById('DetailsLayer').style.top = postop + "px";
	document.getElementById('DetailsLayer').style.visibility = 'visible';
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

</script>