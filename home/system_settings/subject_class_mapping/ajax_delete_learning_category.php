<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

$LearningCategoryID = $_REQUEST['LearningCategoryID'];
$libLC = new Learning_Category($LearningCategoryID);
$success = $libLC->Delete_Record();


echo $success;
intranet_closedb();

?>