<?php
// Using: 

############# Change Log [Start] ################
#
#	Date:	2020-05-07 Tommy
#           - modified access checking, added $plugin["Disable_Subject"]
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) {
    No_Access_Right_Pop_Up();
}

$li = new libdb();
$limport = new libimporttext();
$lo = new libfilesystem();

$linterface = new interface_html();
$lscm_ui = new subject_class_mapping_ui();

$lpf_dbs = new libpf_dbs_transcript();
$luser = new libuser();

$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));
if(!($ext == ".CSV" || $ext == ".TXT"))
{
    intranet_closedb();
    header("location: student_subject_level_import.php?xmsg=import_failed");
    exit();
}
$csv_data = $limport->GET_IMPORT_TXT($csvfile);

$col_name = array_shift($csv_data);
$col_count = count($col_name);

$format_wrong = false;
$file_format = array("UserLogin", "Class Name [ref]", "Class No [ref]", "Name (Eng) [ref]", "Name (Chi) [ref]");
for($i=0; $i<sizeof($file_format); $i++)
{
    if ($col_name[$i] != $file_format[$i])
    {
        $format_wrong = true;
        break;
    }
}
$numOfData = count($csv_data);
if($format_wrong || $numOfData == 0)
{
    $returnMsg = ($format_wrong) ? 'import_header_failed' : 'import_no_record';
    intranet_closedb();
    header("location: student_subject_level_import.php?xmsg=".$returnMsg);
    exit();
}

# Form & Class
$academicYearId = Get_Current_Academic_Year_ID();
$classId = $_POST['classId']? $_POST['classId'] : $_GET['classId'];
$classId = IntegerSafe(standardizeFormPostValue($classId));
if(!$classId)
{
    $year_class = new year_class();
    $class_list = $year_class->Get_All_Class_List($academicYearId);
    $classId = $class_list[0]['YearClassID'];
}
$year_class = new year_class($classId, true, false, true);
$yearId = $year_class->YearID;

# Class Students
$classStudentAry = $year_class->ClassStudentList;
$studentIDAry = Get_Array_By_Key((array)$classStudentAry, 'UserID');

# Form Subject
$subjects = new subject();
$subject_list = $subjects->Get_Subject_By_Form($yearId);
$subject_id_list = Get_Array_By_Key($subject_list, 'RecordID');

# Subject Code
$subject_code_mapping = array();
for($i=sizeof($file_format); $i<$col_count; $i++)
{
    $this_subject = $subjects->Get_Subject_By_SubjectCode($col_name[$i]);
    if(empty($this_subject)) {
        $this_subject = $subjects->Get_Subject_By_SubjectCode('0'.$col_name[$i]);
    }
    if(empty($this_subject)) {
        $this_subject = $subjects->Get_Subject_By_SubjectCode('00'.$col_name[$i]);
    }
    if(empty($this_subject)) {
        $this_subject = $subjects->Get_Subject_By_SubjectCode('000'.$col_name[$i]);
    }
    if(!in_array($this_subject[0]['RecordID'], $subject_id_list))
    {
        $format_wrong = true;
        break;
    }

    $subject_code_mapping[$i] = $this_subject[0]['RecordID'];
}

if($format_wrong)
{
    $returnMsg = 'import_header_failed';
    intranet_closedb();
    header("location: student_subject_level_import.php?xmsg=".$returnMsg);
    exit();
}

# DBS Curriculum
$subjectLevelAry = array();
$allCurriculumAry = $lpf_dbs->getCurriculumSetting('','','','',$ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"]);
foreach($allCurriculumAry as $thisCurriculumInfo)
{
    if(trim($thisCurriculumInfo['SubjectLevel']) != '')
    {
        $thisSubjectLevelAry = explode('<br />', nl2br(trim($thisCurriculumInfo['SubjectLevel'])));
        $thisSubjectLevelAry = array_values(array_unique(array_filter(Array_Trim($thisSubjectLevelAry))));
        foreach($thisSubjectLevelAry as $thisSubjectLevel) {
            $subjectLevelAry[] = trim($thisSubjectLevel);
        }
    }
}

# Create temp table if temp table not exists
$sql = "CREATE TABLE IF NOT EXISTS TEMP_STUDENT_SUBJECT_LEVEL_IMPORT
        (
            TempID int(11) NOT NULL auto_increment,
            StudentID int(11) NOT NULL,
            SubjectID int(11) NOT NULL,
            SubjectLevel varchar(255) DEFAULT NULL,
            DateInput datetime DEFAULT NULL,
            UserID int(11) DEFAULT NULL,
            PRIMARY KEY (TempID)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$li->db_db_query($sql) or die(mysql_error());

# Delete the temp data in temp table
$sql = "DELETE FROM TEMP_SUBJECT_IMPORT WHERE UserID = ".$_SESSION["UserID"];
$li->db_db_query($sql) or die(mysql_error());

$i = 1;
$errorCount = 0;
$successCount = 0;
$error_result = array();

$InsertArr = array();
foreach($csv_data as $key => $data)
{
    $isErrorData = false;
    $errorRowIndex = sizeof($error_result);

    $i++;
    $error_msg = array();

    // Check empty data (UserLogin)
    $userlogin = trim($data[0]);
    if($userlogin == '') {
        $error_data_ary[$i] = $data;
        $error_indicate_ary[$i][0] = true;
        //$errorCount++;
        //continue;
    }

    // Check valid student
    $student_id = $luser->getUserIDByUserLogin($userlogin);
    if($student_id == '' || !in_array($student_id, $studentIDAry)) {
        $error_data_ary[$i] = $data;
        $error_indicate_ary[$i][0] = true;
        //$errorCount++;
        //continue;
    }

    // Check input subject level
    for($j=sizeof($file_format); $j<$col_count; $j++) {
        $subject_level = $data[$j];
        if($subject_level == 'N.A.') {
            $subject_level = '';
        }
        else if(!in_array($subject_level, $subjectLevelAry))
        {
            $error_data_ary[$i] = $data;
            $error_indicate_ary[$i][$j] = true;
            //$errorCount++;
            //break;

            continue;
        }
        $this_subject_id = $subject_code_mapping[$j];

        # Insert temp record
        $sql = "INSERT INTO TEMP_STUDENT_SUBJECT_LEVEL_IMPORT 
                    (StudentID, SubjectID, SubjectLevel, DateInput, UserID)
                VALUES
                    (	
                        '$student_id', 
                        '$this_subject_id', 
                        '".$li->Get_Safe_Sql_Query($subject_level)."',
                        NOW(),
                        '".$_SESSION['UserID']."'
                    )
			    ";
        $li->db_db_query($sql) or die(mysql_error());
        $TempID = $li->db_insert_id();
    }

    if(!isset($error_data_ary[$i])) {
        $successCount++;
    }
}
$errorCount = count($error_data_ary);

### List out the import result
$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['SuccessfulRecord'] ."</td>";
$x .= "<td class='tabletext'>". $successCount ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['FailureRecord'] ."</td>";
$x .= "<td class='tabletext'>". ($errorCount ? "<font color='red'>":"") . $errorCount . ($errorCount ? "</font>":"") ."</td>";
$x .= "</tr>";

$x .= "</table><br>";
$htmlAry["ResultTable"] = $x;

$error_data_count = count($error_data_ary);
if($error_data_count > 0)
{
    $x = "";
    $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"common_table_list_v30 view_table_list_v30\">";
    $x .= "<tr>";
    $x .= "<th class=\"field_title\">".$Lang['General']['ImportArr']['Row']	."</td>";
    for($i=0; $i<sizeof($file_format); $i++) {
        $x .= "<th class=\"field_title\">".$file_format[$i]."</td>";
    }
    for($i=sizeof($file_format); $i<$col_count; $i++) {
        $this_subject = $subjects->Get_Subject_By_SubjectCode($col_name[$i]);
        if(empty($this_subject)) {
            $this_subject = $subjects->Get_Subject_By_SubjectCode('0'.$col_name[$i]);
        }
        if(empty($this_subject)) {
            $this_subject = $subjects->Get_Subject_By_SubjectCode('00'.$col_name[$i]);
        }
        if(empty($this_subject)) {
            $this_subject = $subjects->Get_Subject_By_SubjectCode('000'.$col_name[$i]);
        }
        $x .= "<th class=\"field_title\">".$this_subject[0]['CODEID']."</td>";
    }
    $x .= "</tr>";

    foreach($error_data_ary as $data_index => $this_error_data)
    {
        $x .= "<tr class=\"tablebluerow".($this_data_count % 2 + 1)."\">";
        $x .= "<td class=\"$css\">".$data_index."</td>";
        foreach($this_error_data as $this_data_index => $this_data) {
            $needIndicator = $error_indicate_ary[$data_index][$this_data_index];
            $css = $needIndicator? "red" : "tabletext";

            $x .= "<td class=\"$css\">".$this_data."</td>";
        }
        $x .= "</tr>";
    }
    $x .= "</table>";
    $htmlAry["ErrorTable"] = $x;
}

# Step
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# Page
$CurrentPageArr['Subjects'] = 1;

# Title
$TAGS_OBJ = $lscm_ui->Get_SchoolSettings_Subject_Tab_Array('studentSubjectLevel');
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];

$linterface->LAYOUT_START();
?>

<form name="form1" method="post" action="student_subject_level_import_update.php" enctype="multipart/form-data">
<br />

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<!--<tr>-->
<!--    <td align="right">--><?//=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?><!--</td>-->
<!--</tr>-->
<tr>
    <td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
    <td>
        <?= $htmlAry["ResultTable"] ?>
        <?php if($htmlAry["ErrorTable"] != '') { ?>
            <?= $linterface->GET_NAVIGATION2_IP25($Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['InvalidImport']) ?>
            <?= $htmlAry["ErrorTable"] ?>
        <?php } ?>
    </td>
</tr>
</table>

<div id='table_content'><?= $html["display_table_fields"] ?></div>

<input type="hidden" name="classId" value="<?=$classId?>" />
</form>

<div class="edit_bottom_v30">
    <p class="spacer"></p>
    <?php if ($error_data_count == 0) { ?>
        <input type="button" value="<?=$Lang["Btn"]["Submit"]?>" class="formbutton_v30 print_hide " onclick="document.form1.submit()" id="submitBtn" name="submitBtn">
    <?php } ?>
    <input type="button" value="<?=$Lang["Btn"]["Back"]?>" class="formbutton_v30 print_hide " onclick="history.back()" id="backBtn" name="backBtn">
    <p class="spacer"></p>
</div>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>