<?php
// using 

/*
 * 2018-08-08 (Henry): fixed sql injection 
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);
$ParentSubjectID = stripslashes($_REQUEST['ParentSubjectID']);
$ParentSubjectID = IntegerSafe($ParentSubjectID);
$SubjecClassUI = new subject_class_mapping_ui();

$returnString = "";
if ($RecordType == "LearningCategoryTable")
	$returnString = $SubjecClassUI->Get_Learning_Category_Add_Edit_Table();
else if ($RecordType == "ContentTable")
	$returnString = $SubjecClassUI->Get_Subject_Component_Setting_Table($ParentSubjectID);
	
echo $returnString;

intranet_closedb();

?>