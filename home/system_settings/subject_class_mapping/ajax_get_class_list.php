<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

intranet_opendb();

$YearID = (is_array($_REQUEST['YearID']))? $_REQUEST['YearID']:array();
$AcademicYearID = $_REQUEST['AcademicYearID'];
/*echo '<pre>';
var_dump($_REQUEST);
echo '</pre>';
die;*/
$SubjectClassMappingUI = new subject_class_mapping_ui();

echo $SubjectClassMappingUI->Get_Class_List_Selection($AcademicYearID,$YearID);

intranet_closedb();
?>