<?php
// using Stanley
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();
$fcm = new form_class_manage();
$scm = new subject_class_mapping();
$FormList = $fcm->Get_Form_List(false);
$SubjectList = $scm->Get_Subject_List();
$Temp = $scm->Get_Subject_Group_Stat($YearTermID,"",$subjectID); 
$YearClassList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID);
$SubjectGroupStat = is_array($Temp[0])? $Temp[0]:array();
$FormHasStat = is_array($Temp[1])? $Temp[1]:array();
$ClassHasStat = is_array($Temp[2])? $Temp[2]:array();
$Colspan = sizeof($FormHasStat)+sizeof($ClassHasStat)+3;
$YearTerm = new academic_year_term($YearTermID,false);

for ($i=0; $i< sizeof($YearClassList); $i++) {
	$YearClassListByForm[$YearClassList[$i]['YearID']][] = $YearClassList[$i];
}
$x ='
<div style="text-align:right;width:100%;"><a href="javascript:void(0)" style="border-bottom:1px solid #CCCCCC;padding-right:3px" class="showStatLink" onclick="showGroupStat('.$subjectID.','.$YearTermID.','.$AcademicYearID.',this)">'.$i_subjectGroupMapping_hideStat.'</a></div>
<table class="common_table_list">
   <thead>
		<tr> <th class="sub_row_top">'.$Lang['SysMgr']['SubjectClassMapping']['Group'].'</th>
      	<th class="sub_row_top">'.$Lang['SysMgr']['SubjectClassMapping']['ClassTeacher'].'</th>
      	<th class="sub_row_top">'.$Lang['SysMgr']['SubjectClassMapping']['ClassStudent'].'</th>';
    for ($i=0; $i< sizeof($FormList); $i++) {
    	if (in_array($FormList[$i]['YearID'],$FormHasStat)) {
    		$x .= '<th class="sub_row_top">
    						<a href="#" onclick="Toggle_Class_Stat(\''.$FormList[$i]['YearID'].'\',\''.$subjectID.'\'); return false;">'.$FormList[$i]['YearName'].'</a>
    						<input type="hidden" id="'.$FormList[$i]['YearID'].'ToggleStatus'.$subjectID.'" value="0">
    					</th>';
    		for ($j=0; $j< sizeof($YearClassListByForm[$FormList[$i]['YearID']]); $j++) {
    			if (in_array($YearClassListByForm[$FormList[$i]['YearID']][$j]['YearClassID'],$ClassHasStat)) {
    				$FormClassDisplay[$FormList[$i]['YearID']][] = $YearClassListByForm[$FormList[$i]['YearID']][$j]['YearClassID'];
    				$ClassName = Get_Lang_Selection($YearClassListByForm[$FormList[$i]['YearID']][$j]['ClassTitleB5'],$YearClassListByForm[$FormList[$i]['YearID']][$j]['ClassTitleEN']);
    				$x .= '<th class="sub_row_top '.$FormList[$i]['YearID'].'ClassStatCol'.$subjectID.'" style="display:none">'.$ClassName.'</th>';
    			}
    		}
    	}
	}
$x .= '</tr>
			<tbody>';
			
for ($i=0; $i< sizeof($SubjectList); $i++) {
	$SubjectGroupList = $SubjectGroupStat[$SubjectList[$i]['SubjectID']];
	$TotalStudentCountOnSubject = 0;
	$TotalTeacherCountOnSubject = 0;
	$YearClassTotal = array();
	$TotalStudentCountOnSubjectByForm = array();
	$SubjectGroupDisplay = "";
	$EmptySubject = true;
	if (sizeof($SubjectGroupList) > 0) {
		$SubjectGroupDisplay = '	<tr class="sub_row '.$SubjectList[$i]['SubjectID'].'SubjectGroupList">';
		//$SubjectGroupDisplay .= ' <td rowspan="'.sizeof($SubjectGroupList).'" style="background-color:#FFFFFF; border-right:1px solid #F1F1F1;">&nbsp;</td>';
		///$SubjectGroupDisplay .= ' <td rowspan="'.sizeof($SubjectGroupList).'" style="background-color:#FFFFFF; border-right:1px solid #F1F1F1;">&nbsp;</td>';
		//$SubjectGroupDisplay .= ' <td rowspan="'.sizeof($SubjectGroupList).'" style="background-color:#FFFFFF; border-right:1px solid #F1F1F1;">&nbsp;</td>';
		$IsFirst = 0;
		
		foreach ($SubjectGroupList as $SubjectGroupID => $SubjectGroupDetail) {
			$TeacherList = $scm->Get_Subject_Group_Teacher_List($SubjectGroupID);
			$TotalStudentInSubjectGroup = 0;
			$TotalTeacherCountOnSubject += sizeof($TeacherList);
			$StatDisplay = "";
			$FirstDetail = $SubjectGroupDetail[key($SubjectGroupDetail)][key($SubjectGroupDetail[key($SubjectGroupDetail)])];
			$ClassTitle = Get_Lang_Selection($FirstDetail['SubjectGroupTitleB5'],$FirstDetail['SubjectGroupTitleEN']);
			
			if (is_array($FormClassDisplay)) {
				foreach ($FormClassDisplay as $YearID => $ClassArray) {
					$ClassStatDisplay = "";
					$FormTotal = 0;
					if (sizeof($SubjectGroupDetail[$YearID]) == 0) {
						$StatDisplay .= '<td>&nbsp;</td>';
						foreach ($ClassArray as $Key => $ClassID) {
							$StatDisplay .= '<td class="'.$YearID.'ClassStatCol'.$subjectID.'" style="display:none;">&nbsp;</td>';
						}
					}
					else {
						foreach ($ClassArray as $Key => $ClassID) {
							$EmptySubject = false;
							$ClassDetail = $SubjectGroupDetail[$YearID][$ClassID];
							$FormTotal += $ClassDetail['ClassTotal'];
							$YearClassTotal[$ClassID] += $ClassDetail['ClassTotal'];
							$ClassStatDisplay .= '<td class="'.$YearID.'ClassStatCol'.$subjectID.'" style="display:none;">'.(($ClassDetail['ClassTotal'] > 0)? $ClassDetail['ClassTotal']:'&nbsp;').'</td>';
						}
						$Class = ($FormTotal > 0)? 'class="rights_selected"':'';
						
						// calculate subject group total student and subject total student by form
						$TotalStudentInSubjectGroup += $FormTotal;
						$TotalStudentCountOnSubjectByForm[$YearID] += $FormTotal;
						
						$StatDisplay .= '<td '.$Class.'>'.$FormTotal.'</td>';
						$StatDisplay .= $ClassStatDisplay;
					}
				}
			}

			// calculate Subject Total student
			$TotalStudentCountOnSubject += $TotalStudentInSubjectGroup;

			$StaffName = "&nbsp;";
			for ($j=0; $j< sizeof($TeacherList); $j++) {
				if ($j == 0)
					$StaffName = $TeacherList[$j]['TeacherName'];
				else 
					$StaffName .= ', <br>'.$TeacherList[$j]['TeacherName'];
			}
			if ($IsFirst == 1) {
				$SubjectGroupDisplay .= '	<tr class="sub_row '.$SubjectList[$i]['SubjectID'].'SubjectGroupList">';
			}
			$SubjectGroupDisplay .= '  	<td><a href="#" onclick="Show_Class_Detail(\''.$SubjectGroupID.'\');">'.$ClassTitle.'</a></td>
																<td>'.$StaffName.'</td>'; 
			$SubjectGroupDisplay .= '<td>'.$TotalStudentInSubjectGroup.'</td>';
			$SubjectGroupDisplay .= $StatDisplay;
			$SubjectGroupDisplay .= ' </tr>';
			$IsFirst = 1;
		}
		//$SubjectGroupDisplay .= '</tr></table></td></tr>';
	//}
		/*if (!$EmptySubject || !$HideNonRelate) {
			$x .= '<tr>';
									<td>'.$SubjectList[$i]['Sequence'].'&nbsp;</td>
									<td>'.$SubjectList[$i]['WEBSAMSCode'].'</td>
									<td>
										'.$SubjectList[$i]['SubjectDescEN'].'<br>'.$SubjectList[$i]['SubjectDescB5'].'
									</td>
									<td width="25%">
									';*/
		/*	if (sizeof($SubjectGroupList) > 0) {
				$x .= '
							<span class="row_link_tool">
							<a href="#" id="'.$SubjectList[$i]['SubjectID'].'Link" onclick="Toogle_Show_Subject_Group_Detail(\''.$SubjectList[$i]['SubjectID'].'\'); return false;" class="zoom_in" title="'.$Lang['SysMgr']['SubjectClassMapping']['ViewGroup'].'">
							'.sizeof($SubjectGroupList).'
							</a>
							</span>';
			}
			else {
				$x .= '<span style="display:inline; float:left;">
							0
							</span>';
			}
				
				$x .=  '</td>';		*/	
			/*if (!$YearTerm->TermPassed) {	
				$x .= '				<span class="table_row_tool row_content_tool" id="'.$SubjectList[$i]['SubjectID'].'CollapseLayer" style="float:right;">
												<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Get_Class_Form(\'\',\''.$SubjectList[$i]['SubjectID'].'\',\'\');" class="add_dim thickbox" title="'.$Lang['Btn']['Add'].'"></a>
											</span>';
			}
			$x .= '				</td>
									<td>
										<span style="float:left;">
										'.$TotalTeacherCountOnSubject.'
										</span>';
			if (!$YearTerm->TermPassed) {	
				$x .= '				<span class="table_row_tool row_content_tool" id="'.$SubjectList[$i]['SubjectID'].'ExpandedLayer" style="display:none; float:right;">
												<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Get_Class_Form(\'\',\''.$SubjectList[$i]['SubjectID'].'\',\'\');" class="add_dim thickbox" title="'.$Lang['Btn']['Add'].'"></a>
											</span>';
			}
			$x .= '			</td>
									<td>'.$TotalStudentCountOnSubject.'</td>';*/
			$total = '<tr><td>'.$list_total.'</td>';
			$total .='<td><span style="float:left;">'.$TotalTeacherCountOnSubject.'</span></td>';
			$total .='<td>'.$TotalStudentCountOnSubject.'</td>';
			for ($j=0; $j< sizeof($FormList); $j++) {
				if (in_array($FormList[$j]['YearID'],$FormHasStat)) {
					$total .= '<td>'.$TotalStudentCountOnSubjectByForm[$FormList[$j]['YearID']].'&nbsp;</td>';
					for ($k=0; $k< sizeof($YearClassListByForm[$FormList[$j]['YearID']]); $k++) {
						if (in_array($YearClassListByForm[$FormList[$j]['YearID']][$k]['YearClassID'],$ClassHasStat)) {
							$YearClassTotalDisplay = ($YearClassTotal[$YearClassListByForm[$FormList[$j]['YearID']][$k]['YearClassID']] == "")? '&nbsp;':$YearClassTotal[$YearClassListByForm[$FormList[$j]['YearID']][$k]['YearClassID']];
							$total .= '<td class="sub_row_top '.$FormList[$j]['YearID'].'ClassStatCol'.$subjectID.'" style="display:none">'.$YearClassTotalDisplay.'</td>';
						}
					}
				}
			}
			$total .= '</tr>';
			
			$x .= $SubjectGroupDisplay.$total;	
		//}
	}
}
intranet_closedb();
echo $x.'</tbody></table> <!-- <input id="subjecIDreturn" type="hidden" value="'.$subjectID.'"> -->';
?>