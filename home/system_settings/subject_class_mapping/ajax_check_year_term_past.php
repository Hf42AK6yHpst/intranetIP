<?php
// using 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$YearTermID = $_REQUEST['YearTermID'];
$ObjYearTerm = new academic_year_term($YearTermID);

if ($_SESSION['SSV_USER_ACCESS']['SchoolSettings-Subject'] || in_array($_SESSION['UserID'], (array)$sys_custom['SubjectGroup']['CanEditPastSubjectGroupUserIDArr']))
	$hasTermPast = '0';
else if ($ObjYearTerm->TermPassed)
	$hasTermPast = "1"; // Academic Year Passed
else
	$hasTermPast = "0"; // Academic Year is current year or is in the future
	
echo $hasTermPast;

intranet_closedb();
?>