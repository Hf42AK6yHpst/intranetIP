<?
// Modifying by: 

/*
 *	Date:	2020-05-07 Tommy
 *          - modified access checking, added $plugin["Disable_Subject"]
 * 
 * 	Date	: 20140729 (Bill)
 * 	Details	: add check - space in Subject Group Code
 * 
 * 	Date	: 20110621 (Marcus) 
 * 	Deatils	: add param YearTermID to constructor subject_class_mapping, 
 * 			  YearTermID must be passed after cater same class code in different term
 * 
 * Date		: 20100826 (Ivan)
 * Deatils	: Add import Subject Group Teacher logic 
 * 
 * Date		: 20100803 (Ivan)
 * Deatils	: Move the create temp  inport table sql to the addon schema
 * 			  Cater Subject Group mapping to Subject Component
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT.'includes/libeclass40.php');
		
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$AcademicYearID = $_REQUEST['AcademicYearID'];
$YearTermID = $_REQUEST['YearTermID'];

$obj_AcademicYear = new academic_year($AcademicYearID);
$obj_YearTerm = new academic_year_term($YearTermID);
$leclass = new libeclass();
$CheckEClassLicense = ($leclass->license != '' && $leclass->license > 0)? true : false;
$eClassQuotaLeft = $leclass->ticket();

$li = new libdb();
$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import_subject_group.php?AcademicYearID=".$AcademicYearID."&YearTermID=".$YearTermID."&xmsg=import_failed");
	exit();
}
$data = $limport->GET_IMPORT_TXT($csvfile);

$col_name = array_shift($data);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$file_format = array();
$file_format[] = "Subject Code";
if ($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent'] == true)
	$file_format[] = "Subject Component Code";
$file_format[] = "Subject Group Code";
$file_format[] = "Name (Eng)";
$file_format[] = "Name (Chi)";
$file_format[] = "Applicable Form(s)";
$file_format[] = "Create eClass";
$file_format[] = "Teacher Login ID";


$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}


$numOfData = count($data);
if($format_wrong || $numOfData==0)
{
	$returnMsg = ($format_wrong)? 'import_header_failed' : 'import_no_record';
	intranet_closedb();
	header("location: import_subject_group.php?AcademicYearID=".$AcademicYearID."&YearTermID=".$YearTermID."&xmsg=".$returnMsg);
	exit();
}

# Title / Menu
$linterface = new interface_html();
$CurrentPageArr['Subjects'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['ImportSubjectGroup']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

# create temp table if temp table not exists
/* moved to /addon/ip25/sql_table_update.php
$sql = "CREATE TABLE IF NOT EXISTS TEMP_SUBJECT_GROUP_IMPORT
	(
		TempID int(11) NOT NULL auto_increment,
		UserID int(11),
		RowNumber int(11),
		SubjectCode varchar(20),
		SubjectGroupCode varchar(50),
		SubjectGroupTitleEn varchar(255),
		SubjectGroupTitleCh varchar(255),
		ApplicableForm varchar(255),
		CreateEclass varchar(10),
		DateInput datetime,
		PRIMARY KEY (TempID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$li->db_db_query($sql) or die(mysql_error());
*/

# delete the temp data in temp table 
$sql = "delete from TEMP_SUBJECT_GROUP_IMPORT where UserID=".$_SESSION["UserID"];
$li->db_db_query($sql) or die(mysql_error());

$errorCount = 0;
$successCount = 0;
$error_result = array();
$i=1;


foreach($data as $key => $data)
{
	$i++;
	$error_msg = array();
	
	### store csv data to temp data
	if ($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent'] == true)
		list($SubjectCode, $SubjectComponentCode, $SubjectGroupCode, $SubjectGroupTitleEn, $SubjectGroupTitleCh, $ApplicableForm, $CreateEclass, $TeacherList) = $data;
	else
		list($SubjectCode, $SubjectGroupCode, $SubjectGroupTitleEn, $SubjectGroupTitleCh, $ApplicableForm, $CreateEclass, $TeacherList) = $data;
		
	$SubjectCode = trim($SubjectCode);
	$SubjectComponentCode = trim($SubjectComponentCode);
	$SubjectGroupCode = trim($SubjectGroupCode);
	$SubjectGroupTitleEn = trim($SubjectGroupTitleEn);
	$SubjectGroupTitleCh = trim($SubjectGroupTitleCh);
	$ApplicableForm = trim($ApplicableForm);
	$CreateEclass = strtoupper(trim($CreateEclass));
	$TeacherList = trim($TeacherList);
	$SubjectID = "";
	
	
	### check data
	# 1. Check empty data
	if(empty($SubjectCode) || empty($SubjectGroupCode) || empty($SubjectGroupTitleEn) || empty($SubjectGroupTitleCh) || empty($CreateEclass) || empty($ApplicableForm))
		$error_msg[] = $Lang['SysMgr']['FormClassMapping']['MissingData'];
		
	# 2. Check if the subject exists
	$libSubject = new subject();
	if ($libSubject->Is_Record_Existed($SubjectCode) == false)
	{
		$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['NoSubjectWarning'];
	}
	else
	{
		# 3. Check if the subject is a main subject
		$tmpSubjectArr = $libSubject->Get_Subject_By_SubjectCode($SubjectCode);
		if (count($tmpSubjectArr) == 0)
			$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['SubjectComponentCodeWarning'];
		else {
			if ($SubjectDetail[$tmpSubjectArr[0]['RecordID']] == "") {
				$SubjectDetail[$tmpSubjectArr[0]['RecordID']] = new subject($tmpSubjectArr[0]['RecordID'],true);
			}
			$SubjectID = $tmpSubjectArr[0]['RecordID'];
		}
	}
	
	if ($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent'] == true && $SubjectComponentCode != '')
	{
		# Check if the Subject Component Code exist
		$thisSubjectInfoArr = $libSubject->Get_Subject_By_SubjectCode($SubjectComponentCode, $isComponent=1, $SubjectCode, $ActiveOnly=1);
		$thisSubjectComponentID = $thisSubjectInfoArr[0]['RecordID'];
		if ($thisSubjectComponentID == '')
			$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['NoSubjectComponentWarning'];
	}
	
	# 4. Check if subject group exist
	$obj_SubjectGroup = new subject_term_class('', true, false, $SubjectGroupCode, $YearTermID);
	if ($obj_SubjectGroup->SubjectGroupID != '')
		$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupExistWarning'];
	
	# 5. Check if form(s) exist
	$libYear = new Year();
	if (trim($ApplicableForm) != '')
	{
		$FormNameArr = explode('###', $ApplicableForm);
		$numOfFormName = count($FormNameArr);
		$TempYearID = array();
		for ($j=0; $j<$numOfFormName; $j++)
		{
			$thisYearID = $libYear->returnYearIDbyYearName($FormNameArr[$j]);
			if ($thisYearID == '')
			{
				if (!in_array($Lang['SysMgr']['SubjectClassMapping']['NoFormWarning'],$error_msg))
					$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['NoFormWarning'];
			}
			else {
				$TempYearID[] = $thisYearID;
			}
		}
		
		if ($SubjectID != "") {
			for ($j=0; $j < sizeof($TempYearID); $j++) {
				// change to array
				if (!in_array($TempYearID[$j],(array)$SubjectDetail[$SubjectID]->YearRelationID)) {
					$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['OutOfFormRelation'];
					break;
				}
			}
		}
	}
	
	# 6. Check if create eClass vaild
	if ($CreateEclass != "YES" && $CreateEclass != "NO")
		$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['CreateEclassFieldNotVaild'];
		
	# 7. Check if Subject Group Code Duplicated within the csv file
	$sql = "Select TempID From TEMP_SUBJECT_GROUP_IMPORT Where SubjectGroupCode = '".$li->Get_Safe_Sql_Query($SubjectGroupCode)."' And UserID = '".$_SESSION["UserID"]."'";
	$TempArr = $li->returnVector($sql);
	if (count($TempArr) > 0)
		$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeDuplicatedWithinCSVWarning'];
	
	# 8. Check if eClass License enough
	if ($CheckEClassLicense && $CreateEclass=='YES')
	{
		$eClassQuotaLeft--;
		if ($eClassQuotaLeft < 0)
		{
			$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['NotEnoughLicenseMessage'];
		}
	}
	
	$TeacherIDArr = array();
	if ($TeacherList != '')
	{
		$TeacherLoginArr = explode('###', $TeacherList);
		$numOfTeacher = count($TeacherLoginArr);
		
		for ($i=0; $i<$numOfTeacher; $i++)
		{
			$thisTeacherLogin = $TeacherLoginArr[$i];
			
			# 9. Check if user login exists
			$sql = " SELECT UserID, RecordType, Teaching FROM INTRANET_USER WHERE UserLogin = '".$thisTeacherLogin."' ";
			$tmpResultSet = $li->returnArray($sql);
			$tmpUserID = $tmpResultSet[0]['UserID'];
			
			if ($tmpUserID == '')
			{
				$error_msg[] = $Lang['SysMgr']['FormClassMapping']['NoUserLoginWarning'];
				break;
			}
			
			# 10. Check if the teacher is a teaching staff
			if ($tmpResultSet[0]['Teaching'] != 1)
			{
				$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['TeacherIsNotTeachingStaffWarning'];
				break;
			}
			
			$TeacherIDArr[] = $tmpUserID;
		}
	}
	$TeacherIDList = implode('###', $TeacherIDArr);
	
	# 11. Check if Subject Group Code contains any space
	if (preg_match('/\s/',$SubjectGroupCode)){
		$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeNoSpace'];
	}
	
	# insert temp record
	$sql = "
			insert into TEMP_SUBJECT_GROUP_IMPORT 
			(UserID, RowNumber, SubjectCode, SubjectComponentCode, SubjectGroupCode, SubjectGroupTitleEn, SubjectGroupTitleCh, ApplicableForm, CreateEclass, TeacherList, TeacherIDList, DateInput)
			values
			(	". $_SESSION["UserID"] .", 
				$i, 
				'".$li->Get_Safe_Sql_Query($SubjectCode)."',
				'".$li->Get_Safe_Sql_Query($SubjectComponentCode)."',
				'".$li->Get_Safe_Sql_Query($SubjectGroupCode)."',
				'".$li->Get_Safe_Sql_Query($SubjectGroupTitleEn)."',
				'".$li->Get_Safe_Sql_Query($SubjectGroupTitleCh)."',
				'".$li->Get_Safe_Sql_Query($ApplicableForm)."',
				'".$li->Get_Safe_Sql_Query($CreateEclass)."',
				'".$li->Get_Safe_Sql_Query($TeacherList)."',
				'".$li->Get_Safe_Sql_Query($TeacherIDList)."',
				now())
			";
	$li->db_db_query($sql) or die(mysql_error());
	$TempID = $li->db_insert_id();
	
	
	
	if(empty($error_msg))
	{
		$successCount++;
	}
	else
	{
		$error_result[$TempID] = $error_msg;
		$error_TempID_str .=  $TempID . ",";
		$errorCount++;
	}
	
}

### List out the import result
$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['SysMgr']['FormClassMapping']['SchoolYear'] ."</td>";
$x .= "<td class='tabletext'>". $obj_AcademicYear->Get_Academic_Year_Name() ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['SysMgr']['FormClassMapping']['Term'] ."</td>";
$x .= "<td class='tabletext'>". $obj_YearTerm->Get_Year_Term_Name() ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['SuccessfulRecord'] ."</td>";
$x .= "<td class='tabletext'>". $successCount ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['FailureRecord'] ."</td>";
$x .= "<td class='tabletext'>". ($errorCount ? "<font color='red'>":"") . $errorCount . ($errorCount ? "</font>":"") ."</td>";
$x .= "</tr>";

$x .= "</table><br>";

if($error_result)
{
	$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
	
	$x .= "<tr>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">Row#</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['SubjectCode'] ."</td>";
	
	if ($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent'] == true)
		$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['SubjectComponentCode'] ."</td>";
		
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCode'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['ClassTitleEN'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['ClassTitleB5'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['ApplicableForm'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['CreateEclass'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['ClassTeacher'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['General']['Remark'] ."</td>";
	$x .= "</tr>";
	
	$error_TempID_str = substr($error_TempID_str, 0,-1);
	$sql = "select * from TEMP_SUBJECT_GROUP_IMPORT where TempID in ($error_TempID_str)";
	$tempData = $li->returnArray($sql);
	
	$i=0;
	foreach($tempData as $k=>$d)
	{
		list($t_TempID, $t_UserID, $t_RowNumber, $t_SubjectCode, $t_SubjectComponentCode, $t_SubjectGroupCode, $t_SubjectGroupTitleEn, $t_SubjectGroupTitleCh, $t_ApplicableForm, $t_CreateEclass, $t_TeacherList) = $d;
		
		$t_SubjectCode = $t_SubjectCode ? $t_SubjectCode : "<font color='red'>***</font>";
		$t_SubjectGroupCode = $t_SubjectGroupCode ? $t_SubjectGroupCode : "<font color='red'>***</font>";
		$t_SubjectGroupTitleEn = $t_SubjectGroupTitleEn ? $t_SubjectGroupTitleEn : "<font color='red'>***</font>";
		$t_SubjectGroupTitleCh = $t_SubjectGroupTitleCh ? $t_SubjectGroupTitleCh : "<font color='red'>***</font>";
		$t_ApplicableForm = $t_ApplicableForm ? $t_ApplicableForm : "<font color='red'>***</font>";
		$t_CreateEclass = $t_CreateEclass ? $t_CreateEclass : "<font color='red'>***</font>";
		
		$thisErrorArr = $error_result[$t_TempID];
		
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['NoSubjectWarning'], $thisErrorArr))
			$t_SubjectCode = "<font color='red'>".$t_SubjectCode."</font>";
			
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['SubjectComponentCodeWarning'], $thisErrorArr))
			$t_SubjectCode = "<font color='red'>".$t_SubjectCode."</font>";
			
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['NoSubjectComponentWarning'], $thisErrorArr))
			$t_SubjectComponentCode = "<font color='red'>".$t_SubjectComponentCode."</font>";
			
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['SubjectGroupExistWarning'], $thisErrorArr))
			$t_SubjectGroupCode = "<font color='red'>".$t_SubjectGroupCode."</font>";
		
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeDuplicatedWithinCSVWarning'], $thisErrorArr))
			$t_SubjectGroupCode = "<font color='red'>".$t_SubjectGroupCode."</font>";
	
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeNoSpace'], $thisErrorArr))
			$t_SubjectGroupCode = "<font color='red'>".$t_SubjectGroupCode."</font>";
		
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['NoFormWarning'], $thisErrorArr) || in_array($Lang['SysMgr']['SubjectClassMapping']['OutOfFormRelation'], $thisErrorArr))
			$t_ApplicableForm = "<font color='red'>".$t_ApplicableForm."</font>";
			
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['CreateEclassFieldNotVaild'], $thisErrorArr))
			$t_CreateEclass = "<font color='red'>".$t_CreateEclass."</font>";
			
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['NotEnoughLicenseMessage'], $thisErrorArr))
			$t_CreateEclass = "<font color='red'>".$t_CreateEclass."</font>";
			
		if (in_array($Lang['SysMgr']['FormClassMapping']['NoUserLoginWarning'], $thisErrorArr))
			$t_TeacherList = "<font color='red'>".$t_TeacherList."</font>";
			
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['TeacherIsNotTeachingStaffWarning'], $thisErrorArr))
			$t_TeacherList = "<font color='red'>".$t_TeacherList."</font>";	
			
		
		if (is_array($error_result[$t_TempID]))
			$errorDisplay = implode('<br />', $error_result[$t_TempID]);
		else
			$errorDisplay = $error_result[$t_TempID];
		
		$css_i = ($i % 2) ? "2" : "";
		$x .= "<tr style='vertical-align:top'>";
		$x .= "<td class=\"tablebluerow$css_i\" width=\"10\">". $t_RowNumber ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_SubjectCode ."</td>";
		
		if ($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent'] == true)
			$x .= "<td class=\"tablebluerow$css_i\">". $t_SubjectComponentCode ."</td>";
		
		$x .= "<td class=\"tablebluerow$css_i\">". $t_SubjectGroupCode ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_SubjectGroupTitleEn ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_SubjectGroupTitleCh ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_ApplicableForm ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_CreateEclass ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_TeacherList ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $errorDisplay ."</td>";
		$x .= "</tr>";
		
		$i++;
	}
	$x .= "</table>";
}


if($errorCount>0)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_subject_group.php?AcademicYearID=".$AcademicYearID."&YearTermID=".$YearTermID."'");
}
else
{
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit");
	$import_button .= " &nbsp;";
	$import_button .= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_subject_group.php?AcademicYearID=".$AcademicYearID."&YearTermID=".$YearTermID."'");
}
?>

<br />
<form name="form1" method="post" action="import_subject_group_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center"><?=$x?></td>
</tr>
		
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
<input type="hidden" name="YearTermID" value="<?=$YearTermID?>">

</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
