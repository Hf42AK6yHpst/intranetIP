<?php
// using: 

############# Change Log [Start] ################
#	Date:	2020-05-07 Tommy
#           - modified access checking, added $plugin["Disable_Subject"]
#
#   Date:	2018-06-22	MarcoYu
#           Add export funtion
#
#	Date:	2015-09-04	Ivan
#			First create this file
#			ip.2.5.6.10.1	*** Need to upload with "/includes/subject_class_mapping_ui.php" before deployment *** 
# 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/libsubjectpanel.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$subjectId = IntegerSafe(standardizeFormPostValue($_POST['subjectId']));

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) {
	No_Access_Right_Pop_Up();
}

$lscm_ui = new subject_class_mapping_ui();
$lsubjectPanel = new libsubjectpanel();
$linterface = new interface_html();

$CurrentPageArr['Subjects'] = 1;
$TAGS_OBJ = $lscm_ui->Get_SchoolSettings_Subject_Tab_Array('subjectPanel');
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];

$linterface->LAYOUT_START($returnMsg);

### content tool
$btnAry = array();
$btnAry[] = array('new', 'javascript: goNew();', $Lang['Btn']['Add']);
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);


### subject selection
$htmlAry['subjectSel'] = $lscm_ui->Get_Subject_Selection('subjectId', $subjectId, 'changedSubjectSel(this.value);', $noFirst=0, $Lang['SysMgr']['SubjectClassMapping']['All']['Subject'], $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=0, $IncludeSubjectIDArr='', $ExtrudeSubjectIDArr=array());


### search box
$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);


### DB table action buttons
$btnAry = array();
$btnAry[] = array('delete', 'javascript:checkRemove(document.form1, \'recordIdAry[]\', \'subject_panel_delete.php\');');
$htmlAry['dbTableActionBtn'] = $linterface->Get_DBTable_Action_Button_IP25($btnAry);

### db table
$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 0 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

### export button
$x='';
$buttonHref = '';
$x .= $linterface->Get_Content_Tool_v30('export', "javascript:goToExport('$keyword','$subjectId',$order,$field);");
$htmlAry['toolbar'] = $x;

### Print
$print='';
$print .= $linterface->Get_Content_Tool_v30('print', "javascript:goToPrint('$keyword','$subjectId',$order,$field);");
$htmlAry['printToolBar'] = $print;

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("TeacherName", "SubjectName");
$li->sql = $lsubjectPanel->getSubjectPanelListSql($keyword, $subjectId);
$li->no_col = sizeof($li->field_array) + 3;
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='31%' >".$li->column($pos++, $Lang['SysMgr']['Timetable']['TeacherName'])."</th>\n";
$li->column_list .= "<th width='31%' >".$li->column($pos++, $Lang['SysMgr']['SubjectClassMapping']['Subject'])."</th>\n";
$li->column_list .= "<th width='31%' >".$Lang['General']['Form']."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("recordIdAry[]")."</th>\n";
$htmlAry['dataTable'] = $li->display();

$x = '';
$x .= $linterface->GET_HIDDEN_INPUT('pageNo', 'pageNo', $li->pageNo);
$x .= $linterface->GET_HIDDEN_INPUT('order', 'order', $li->order);
$x .= $linterface->GET_HIDDEN_INPUT('field', 'field', $li->field);
$x .= $linterface->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$x .= $linterface->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $li->page_size);
$htmlAry['hiddenField'] = $x;

$e = '';
$e .= $linterface->GET_HIDDEN_INPUT('keyword', 'keyword', '');
$e .= $linterface->GET_HIDDEN_INPUT('SubjectId', 'SubjectId', '');
$e .= $linterface->GET_HIDDEN_INPUT('order', 'order', '');
$e .= $linterface->GET_HIDDEN_INPUT('field', 'field', '');
$htmlAry['SubjectPanelHidden'] = $e;



?>
<script type="text/javascript">
$(document).ready( function() {
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		}
	});
});

function goSearch() {
	$('form#form1').attr('action', 'subject_panel_list.php').submit();
}

function goNew() {
	$('form#form1').attr('action', 'subject_panel_new.php').submit();
}

function changedSubjectSel(parSubjectId) {
	$('form#form1').attr('action', 'subject_panel_list.php').submit();
}


function  goToExport(parkeyword,parSubjectId,parorder,parfield){
	$('input#keyword').val(parkeyword);
	$('input#SubjectId').val(parSubjectId);
	$('input#order').val(parorder);
	$('input#field').val(parfield);
	$('form#form2').attr('action', 'subject_panel_list_export.php').submit();
}

function  goToPrint(parkeyword,parSubjectId,parorder,parfield){
	$('input#keyword').val(parkeyword);
	$('input#SubjectId').val(parSubjectId);
	$('input#order').val(parorder);
	$('input#field').val(parfield);
	$('form#form3').attr('action', 'subject_panel_list_print.php').submit();
}

</script>
<form name="form1" id="form1" method="POST" action="subject_panel_list.php">
	
	
	<?=$htmlAry['subTab']?>
	
	<div class="content_top_tool">
		
		<?=$htmlAry['contentTool']?>
		<div class="Conntent_tool">
		<?php echo $htmlAry['printToolBar']?>
		<?=$htmlAry['toolbar']?>
        </div>
		<?=$htmlAry['searchBox']?>
		<br style="clear:both;">
	</div>
	
	<div class="table_board">
		<div class="table_filter">
			<?=$htmlAry['subjectSel']?>
		</div>
		<p class="spacer"></p>
				
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
	</div>
	
	<?=$htmlAry['hiddenField']?>
</form>


<form name="form2" id="form2" method="POST" action="subject_panel_list_export.php">
<?=$htmlAry['SubjectPanelHidden']?>
</form>

<!-- Print -->
<form name="form3" id="form3" method="POST" target="_blank" action="subject_panel_list_export.php">
<?=$htmlAry['SubjectPanelHidden']?>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>