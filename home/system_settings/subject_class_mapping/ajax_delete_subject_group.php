<?php
// using Stanley
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
intranet_opendb();

$SubjectGroupID = $_REQUEST['SubjectGroupID'];
$YearTermID = $_REQUEST['YearTermID'];
$SubjectID = $_REQUEST['SubjectID'];
$deleteEclass = $_REQUEST['deleteEclass'];

/*echo '<pre>';
var_dump($_REQUEST);
echo '</pre>';
die;*/
$scm = new subject_class_mapping();
$libdb = new libdb();

$scm->Start_Trans();
$result = array();
$SubjectGroupIDArr = array();
if ($SubjectGroupID == '')
{
	### Get Subject Group of the Subject in the Term
	if ($SubjectID != '' && $YearTermID != '')
	{
		$SubjectObj = new subject($SubjectID);
		$SubjectGroupInfoArr = $SubjectObj->Get_Subject_Group_List($YearTermID);
		$SubjectGroupIDArr = Get_Array_By_Key($SubjectGroupInfoArr, 'SubjectGroupID');
	}
}
else
{
	$SubjectGroupIDArr = array($SubjectGroupID);
}
$numOfSubjectGroup = count($SubjectGroupIDArr);


for ($i=0; $i<$numOfSubjectGroup; $i++)
{
	$thisSubjectGroupID = $SubjectGroupIDArr[$i];
	
	# check for eclass
//	if ($deleteEclass == 1){
//		$sql = "select course_id from SUBJECT_TERM_CLASS where SubjectGroupID = $thisSubjectGroupID and course_id is not null
//				";
//		$courseID = $scm->returnVector($sql);
//		if (count($courseID)>0){
//			$course_id = $courseID[0];
//			$iCal = new icalendar();
//			
//			# delete corresponding icalendar
//			$cidSql = implode("','",$courseID);
//			$sql = "select CalID from {$eclass_db}.course where course_id in ('$cidSql')";
//			$calID = $libdb->returnVector($sql);
//			$result[$thisSubjectGroupID]['remove_calendar']=$iCal->removeCalendar(trim($calID[0]));
//			
//			$libeclass = new libeclass($course_id);
//			$libeclass->eClassDel($courseID);
//			$sql = "update SUBJECT_TERM_CLASS set course_id = null 
//			where course_id in ('".implode("','",$courseID)."')";
//			$result[$thisSubjectGroupID]['update_subject_term_class'] = $iCal->db_db_query($sql);
//			# Drop Course File
//			/*$q = new libfilesystem();
//	        $files_prefix = ($eclass_version >= 3.0? "/files/".$eclass_prefix : "/file/");
//	        exec("rm -r ".$eclass40_filepath."$files_prefix"."c$course_id");
//			# Drop Course Database
//			$sql = "DROP DATABASE ".$eclass_prefix."c".$course_id;
//			$result['drop_class_db']=$libdb->db_db_query($sql);
//	     
//	          # Delete course_id from eClass subject_course
//	          $sql = "DELETE FROM {$eclass_db}.course_subj WHERE course_id = $course_id";
//	          $result['drop_course_subj']=$libdb->db_db_query($sql);
//	          # Delete course_id from eClass user_course table
//	          $sql = "DELETE FROM {$eclass_db}.user_course WHERE course_id = $course_id";
//	          $result['drop_user_course']=$libdb->db_db_query($sql);
//	          # Delete course_id from eClass course_count table
//	          $sql = "DELETE FROM {$eclass_db}.course_count WHERE course_id = $course_id";
//	          $result['drop_course_count']= $libdb->db_db_query($sql);
//	          # Delete course_id from eClass course table
//	          $sql = "DELETE FROM {$eclass_db}.course WHERE course_id = $course_id";
//			  $result['drop_course']= $libdb->db_db_query($sql);*/
//			  # Update Equation Editor License
//			  
//			if ($eclass_equation_mck != "")
//			{
//			  $content = trim(get_file_content($eclass40_filepath."/files/equation.txt"));
//			  $equation_class = ($content=="")? array(): explode(",",$content);
//			  $updated_class = array();
//			  
//			  for ($j=0; $j<sizeof($equation_class); $j++)
//			  {
//				$target = $equation_class[$j];
//				if (!in_array($target,$courseID))
//				{
//				  $updated_class[] = $target;
//				}
//			  }
//			  
//			  write_file_content(implode(",",$updated_class),$eclass40_filepath."/files/equation.txt");
//			}
//		}
//	}
//	else{
//		$sql = "select SubjectGroupID,course_id from {$eclass_db}.course where SubjectGroupID regexp '^$thisSubjectGroupID$|;$thisSubjectGroupID;|;$thisSubjectGroupID$' ";
//		$allClass =$scm->returnArray($sql);
//		
//		if (!empty($allClass)){
//			foreach ($allClass as $class){
//				$allSubj = empty($class['SubjectGroupID'])?array():explode(';',$class['SubjectGroupID']);
//				$allSubj = array_filter($allSubj,create_function('$a','return $a != '.$thisSubjectGroupID));
//				$subj_sql = empty($allSubj)?'':implode(";",$allSubj);
//				
//				$sql = "update {$eclass_db}.course 
//						set SubjectGroupID = '$subj_sql'
//							where course_id = ".$class['course_id'];
//				$result[$thisSubjectGroupID]['update_course_linkage_'.$class['course_id']]=$scm->db_db_query($sql);
//			}
//		}
//	}
	
	$result[$thisSubjectGroupID]['delete_subject_term_class'] = $scm->Delete_Subject_Term_Class($thisSubjectGroupID);
}



if (!in_array(false, $result)) {
	echo '1';
	$scm->Commit_Trans();
}
else {
	echo '0';
	$scm->RollBack_Trans();
}

intranet_closedb();
?>