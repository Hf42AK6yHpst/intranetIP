<?
# using: ivan

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
//include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();
$lexport = new libexporttext();
$libSubject = new subject();

## Get all subjects info
$SubjectInfoArr = $libSubject->Get_Subject_List($returnAsso=0);
$numOfSubject = count($SubjectInfoArr);

## construct export array
$exportContentArr = array();
$subject_counter = 0;

for ($i=0; $i<$numOfSubject; $i++)
{
	$thisSubjectID = $SubjectInfoArr[$i]['RecordID'];	
	
	$thisObjSubject = new subject($thisSubjectID);
	$thisSubjectWebSASMSCode = $thisObjSubject->CODEID;
	$thisSubjectName = $thisObjSubject->Get_Subject_Desc();
	
	$thisComponentArr = $thisObjSubject->Get_Subject_Component();
	$numOfComponent = count($thisComponentArr);
	
	if ($numOfComponent > 0)
	{
		for ($j=0; $j<$numOfComponent; $j++)
		{
			$exportContentArr[$subject_counter][] = $thisSubjectWebSASMSCode;
			$exportContentArr[$subject_counter][] = $thisSubjectName;
			$exportContentArr[$subject_counter][] = $thisComponentArr[$j]['CMP_CODEID'];
			$exportContentArr[$subject_counter][] = $thisComponentArr[$j]['EN_DES'];
			$exportContentArr[$subject_counter][] = $thisComponentArr[$j]['CH_DES'];
			$exportContentArr[$subject_counter][] = $thisComponentArr[$j]['EN_ABBR'];
			$exportContentArr[$subject_counter][] = $thisComponentArr[$j]['CH_ABBR'];
			$exportContentArr[$subject_counter][] = $thisComponentArr[$j]['EN_SNAME'];
			$exportContentArr[$subject_counter][] = $thisComponentArr[$j]['CH_SNAME'];
			
			$subject_counter++;
		}
	}
}


$exportHeader = array("Parent Subject WebSAMS Code", "Parent Subject Name", "Subject Componenet WebSAMS Code", "Subject Componenet Name (Eng)", "Subject Componenet Name (Chi)", "Abbreviation (Eng)", "Abbreviation (Chi)", "Short Form (Eng)", "Short Form (Chi)");
$export_content = $lexport->GET_EXPORT_TXT($exportContentArr, $exportHeader);

$filename = "subject_component_list.csv";
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>