<?php
// using 

############# Change Log [Start] ################
#
#	Date:	2020-05-07 Tommy
#           - modified access checking, added $plugin["Disable_Subject"]
#
#	Date:	2016-01-26	Kenneth
#			Modified Check_Timetable_Conflict() - scroll to warning message
#
#	Date:	2015-09-04	Ivan
#			Used Get_SchoolSettings_Subject_Tab_Array() to get the tag array
#			ip.2.5.6.10.1	*** Need to upload with "/includes/subject_class_mapping_ui.php" before deployment ***
#
#	Date:	2011-06-20	Marcus
#			cater using same subject group code in different term
#
#	Date:	2010-08-26	Ivan
#			add delete subject group icon
#
#	Date:	2010-06-21	YatWoon
#			add online help button
#
# 	Date:	2009-12-16 Ivan
#			Added Return Message Logic
#
# 	Date:	2009-12-15 YatWoon
#			Added Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$AcademicYearID = $_REQUEST['AcademicYearID'];
$YearTermID = $_REQUEST['YearTermID'];

$SubjecClassUI = new subject_class_mapping_ui();
$Json = new JSON_obj();
$linterface = new interface_html();

$CurrentPageArr['Subjects'] = 1;

# Get Return Message
$ReturnMessageKey = $_REQUEST['ReturnMessageKey'];
$ReturnMessage = $Lang['SysMgr']['SubjectClassMapping']['ReturnMessage'][$ReturnMessageKey];

### Title ###
//$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['Subject&ComponentTitle'],"index.php",0);
//$TAGS_OBJ[] = array($Lang['formClassMapping']['FormSubject'],"form_subject.php",0);
//$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'],"subject_group_mapping.php",1);
$TAGS_OBJ = $SubjecClassUI->Get_SchoolSettings_Subject_Tab_Array('subjectGroup');
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];

# online help button
//$onlineHelpBtn = gen_online_help_btn_and_layer('school_settings','subject');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);


$linterface->LAYOUT_START($ReturnMessage); 



echo $SubjecClassUI->Get_Subject_Group_Mapping($AcademicYearID, $YearTermID);

?>

<div id="DebugArea"></div>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->
<?
$linterface->LAYOUT_STOP();

intranet_closedb();
?>
<script>
// global variable
var HideGroupLayers = "1";
var PreviousStudentSortingOrder = "null";
var PreviousIsSortByStudentName = "null";
var jsCurIndexSortingOrder = "asc";
var jsCurSortingOrder = "asc";
	
// ajax function
{
function Switch_Academic_Year(AcademicYearID) {
	window.location = "subject_group_mapping.php?AcademicYearID="+AcademicYearID;
}
function clearBufferingContent(){
	for (attr in bufferingStatContent){
		if (attr.match('^State') && bufferingStatContent[attr]){
			subjectID = attr.substr(5);
			subjectDetails = document.getElementById('subjectGroupDetails_'+subjectID);
			bufferNaming = "Content"+subjectID;
			subjectDetails.innerHTML = bufferingStatContent[bufferNaming];
		}
	}
	bufferingStatContent = {};
}

bufferingStatContent = {};
function showGroupStat(subjectID,YearTermID,AcademicYearID,obj){
	termID = YearTermID;
	aYearID = AcademicYearID;
	showStat = '<?=$i_subjectGroupMapping_showStat?>';
	hideStat = '<?=$i_subjectGroupMapping_hideStat?>';
	
	if (YearTermID == null){
		termID = $('select#YearTerm option:selected').val();
	}
	if (AcademicYearID == null){
		aYearID = $('select#AcademicYearID option:selected').val();
	}
	
	var LinkID = subjectID + 'Link';
	bufferStateNaming = "State" + subjectID;
	if (bufferingStatContent[bufferStateNaming]!=null){
		bufferingStatContent[bufferStateNaming] = !bufferingStatContent[bufferStateNaming];	
		bufferNaming = "Content"+subjectID;
		temp = bufferingStatContent[bufferNaming];
		subjectDetails = document.getElementById('subjectGroupDetails_'+subjectID);
		bufferingStatContent[bufferNaming] = subjectDetails.innerHTML;
		subjectDetails.innerHTML = temp;
		$("."+subjectID+"SubjectGroupList").css('display','');
		document.getElementById(LinkID).className = "zoom_out";
	} 
	else{
		subjectDetails = document.getElementById('subjectGroupDetails_'+subjectID);
		bufferStateNaming = "State"+subjectID;
		bufferNaming = "Content"+subjectID;
		bufferingStatContent[bufferStateNaming] = true;
		bufferingStatContent[bufferNaming] = subjectDetails.innerHTML;
		document.body.style.cursor = 'wait';		
		document.getElementById(LinkID).className = "zoom_out";
		
		var serverURL = 'ajax_get_subject_stat.php';
		$('#subjectGroupDetails_'+subjectID).load(
			serverURL,
			{'subjectID':subjectID, 'YearTermID':termID, 'AcademicYearID':aYearID },
			function(reponseText)
			{
				$("."+subjectID+"SubjectGroupList").css('display',''); 
				document.body.style.cursor = 'default';
			}
		);
	}
}


function Show_Class_Short_Info(SubjectGroupID,YearID) {
	wordXmlHttp = GetXmlHttpObject();
   
  if (wordXmlHttp == null)
  {
    alert (errAjax);
    return;
  } 
    
  var url = 'ajax_get_class_short_info.php';
  var postContent = 'SubjectGroupID='+SubjectGroupID;
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
		  document.getElementById('SubjectGroupInfo'+SubjectGroupID+YearID).innerHTML = wordXmlHttp.responseText;
		  document.getElementById('SubjectGroupInfo'+SubjectGroupID+YearID).style.visibility = 'visible';
		  document.getElementById('SubjectGroupInfo'+SubjectGroupID+YearID).style.display = 'inline';
		}
	};
  wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}

function Hide_Class_Short_Info(SubjectGroupID,YearID) {
	document.getElementById('SubjectGroupInfo'+SubjectGroupID+YearID).style.visibility = 'hidden';
	document.getElementById('SubjectGroupInfo'+SubjectGroupID+YearID).style.display = 'none';
}

function Create_Subject_Group() {
	Update_Class_List();
	Check_Class_Title();
	Check_Class_Code();
	var ClassCodeError = document.getElementById('ClassCodeWarningLayer').innerHTML;
	var ClassTitleError = document.getElementById('ClassTitleWarningLayer').innerHTML;
	var SelectedStudError = document.getElementById('StudentSelectedWarningLayer').innerHTML;
	var SelectedYearError = document.getElementById('YearIDWarningLayer').innerHTML;
	
	if (ClassCodeError == "" && ClassTitleError == "" && SelectedYearError == "" && SelectedStudError == "") {
		document.getElementById('SubmitBtn').disabled = true; // disable submit button
		Block_Thickbox();
		Select_All_Options('StudentSelected[]',true);
		Select_All_Options('SelectedClassTeacher[]',true);
		
		var PostString = Get_Form_Values(document.getElementById("ClassForm"));
		
		CreateClassAjax = GetXmlHttpObject();
		   
	  if (CreateClassAjax == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'ajax_create_subject_group.php';
	  var PoseValue = PostString;
		CreateClassAjax.onreadystatechange = function() {
			if (CreateClassAjax.readyState == 4) {
				Get_Return_Message(CreateClassAjax.responseText);
				clearBufferingContent();
				Refresh_Subject_Group_Table();
				window.top.tb_remove();
				UnBlock_Thickbox();
			}
		};
	  CreateClassAjax.open("POST", url, true);
		CreateClassAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		CreateClassAjax.send(PoseValue);
	}
	else {
		if (SelectedStudError == "")
			Check_Selected_Student();
	}
}

function Edit_Subject_Group() {
	Check_Class_Code();
	Check_Class_Title();
	
	var ClassCodeError = document.getElementById('ClassCodeWarningLayer').innerHTML;
	var ClassTitleError = document.getElementById('ClassTitleWarningLayer').innerHTML;
	var SelectedStudError = document.getElementById('StudentSelectedWarningLayer').innerHTML;
	var SelectedYearError = document.getElementById('YearIDWarningLayer').innerHTML;
	
	if (ClassCodeError == "" && ClassTitleError == "" && SelectedYearError == "" && SelectedStudError == "") {
		document.getElementById('SubmitBtn').disabled = true; // disable submit button
		Block_Thickbox();
		Select_All_Options('StudentSelected[]',true);
		Select_All_Options('SelectedClassTeacher[]',true);		
		
		var PostString = Get_Form_Values(document.getElementById("ClassForm"));
		
		allTeacherList = document.getElementById('teacher_benefit');
		if (allTeacherList != null){
			numberOfTeacher = document.getElementById('noClasseTeacher').value;
			//alert(allTeacherList.selectedIndex+' '+numberOfTeacher);
			if (allTeacherList.selectedIndex > (parseInt(numberOfTeacher)+4))
				document.getElementById('is_user_import').value = 1;
				//alert(allTeacherList.selectedIndex+' '+numberOfTeacher);
			PostString = PostString+'&is_user_import='+document.getElementById('is_user_import').value+'&teacher_benefit='+allTeacherList.options[allTeacherList.selectedIndex].value;			
		}
		//alert(PostString);
	//return;
		var SubjectGroupID = document.getElementById('SubjectGroupID').value;
		EditClassAjax = GetXmlHttpObject();
		   
	  if (EditClassAjax == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'ajax_edit_subject_group.php';
	  var PostValue = PostString;
		EditClassAjax.onreadystatechange = function() {
			if (EditClassAjax.readyState == 4) {
				Get_Return_Message(EditClassAjax.responseText);
				clearBufferingContent();
				Show_Class_Detail(SubjectGroupID, '', '');
				window.top.tb_remove();
				UnBlock_Thickbox();
			}
		};
	  EditClassAjax.open("POST", url, true);
		EditClassAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		EditClassAjax.send(PostValue);
	}
	else {
		if (SelectedStudError == "")
			Check_Selected_Student();
	}
}

function Delete_Subject_Group(SubjectGroupID, jsYearTermID, jsSubjectID) {
	
	if (SubjectGroupID == '')
	{
		// delete Subject Group of the Subject
		var jsAcademicYearID = $('select#AcademicYearID').val();
		window.location = "delete_subject_group_confirm.php?AcademicYearID=" + jsAcademicYearID + "&YearTermID=" + jsYearTermID + "&SubjectID=" + jsSubjectID;
	}
	else
	{
		// delete specific Subject Group
		var jsDelete = 0;
		if (confirm('<?=$Lang['SysMgr']['SubjectClassMapping']['DeleteClassWarning']?>')) 
		{	
			if ($('#has_eclass').val() == 1)
			{
				if (confirm('<?=$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['DeleteSingleSubjectGroup']?>'))
					jsDelete = 1;
			}
			else
				jsDelete = 1;
		}
		
		if (jsDelete == 1)
		{
//			hasEclass = $('#has_eclass').val();
//			postStr = "";
//			var deleteEclass = 0;
//			if (hasEclass == 1){
//				if (confirm('<?=$Lang['SysMgr']['SubjectClassMapping']['warningRemoveSubject']?>')){
//					postStr = "&deleteEclass=1";
//					deleteEclass = 1;
//				}
//				else{
//					postStr = "&deleteEclass=0";
//					deleteEclass = 0;
//				}
//			}
		
			Block_Document();
			
			$.post(
				"ajax_delete_subject_group.php", 
				{ 
					SubjectGroupID: SubjectGroupID,
					YearTermID: jsYearTermID,
					SubjectID: jsSubjectID
				},
				function(ReturnData)
				{
					//Get_Return_Message(DeleteClassAjax.responseText);
					
					if (ReturnData == '1')
						Get_Return_Message('<?=$Lang['SysMgr']['SubjectClassMapping']['ClassDeleteSuccess']?>');
					else
						Get_Return_Message('<?=$Lang['SysMgr']['SubjectClassMapping']['ClassDeleteUnsuccess']?>');
						
					clearBufferingContent();
					//ResponseText = Trim(DeleteClassAjax.responseText);
					ResponseText = ReturnData;
					Refresh_Subject_Group_Table();
				}
			);
		}
			
	
	
		
//		DeleteClassAjax = GetXmlHttpObject();
//		   
//		if (DeleteClassAjax == null)
//		{
//			alert (errAjax);
//			return;
//		} 
//	    
//	  var url = 'ajax_delete_subject_group.php';
//	  var postContent = "SubjectGroupID=" + SubjectGroupID+postStr;
//		DeleteClassAjax.onreadystatechange = function() {
//			if (DeleteClassAjax.readyState == 4) {
//				Get_Return_Message(DeleteClassAjax.responseText);
//				clearBufferingContent();
//				ResponseText = Trim(DeleteClassAjax.responseText);
//				Refresh_Subject_Group_Table();
//			}
//		};
//	  	DeleteClassAjax.open("POST", url, true);
//		DeleteClassAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
//		DeleteClassAjax.send(postContent);
	}
}

function Batch_Create_Subject_Group() {
	Block_Thickbox();
	var PostString = Get_Form_Values(document.getElementById("BatchCreateClassForm"));
	
	BatchCreateClassAjax = GetXmlHttpObject();
	   
	if (BatchCreateClassAjax == null)
	{
	  alert (errAjax);
	  return;
	} 
	
	var url = 'ajax_batch_create_subject_group.php';
	var PoseValue = PostString;
	BatchCreateClassAjax.onreadystatechange = function() {
		if (BatchCreateClassAjax.readyState == 4) {
			Get_Return_Message(BatchCreateClassAjax.responseText);
			clearBufferingContent();
			Refresh_Subject_Group_Table();
			window.top.tb_remove();
			UnBlock_Thickbox();
		}
	};
	BatchCreateClassAjax.open("POST", url, true);
	BatchCreateClassAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	BatchCreateClassAjax.send(PoseValue);
}

function Check_Batch_Create_Subject_Group() {
	var PostString = Get_Form_Values(document.getElementById("BatchCreateClassForm"));
	
	CheckBatchCreateClassAjax = GetXmlHttpObject();
	   
  if (CheckBatchCreateClassAjax == null)
  {
    alert (errAjax);
    return;
  } 
  
	if ($('input#isCreateEclass').attr('checked') == true)
	{
		var CheckedSubject = 0;
		var CheckedClass = 0;
		
		$('input.SubjectChk').each( function() {
			if ($(this).attr('checked') == true)
				CheckedSubject++;
		})
		
		$('input.ClassChk').each( function() {
			if ($(this).attr('checked') == true)
				CheckedClass++;
		})
		
		var TargetNumOfSubjectGroup = CheckedSubject * CheckedClass;
		var jsLicenseLeft = parseInt($('input#eClassLicenseLeft').val());
		
		if (jsLicenseLeft != '' && TargetNumOfSubjectGroup > jsLicenseLeft)
		{
			alert('<?=$Lang['SysMgr']['SubjectClassMapping']['NotEnoughLicenseMessage']?>');
			return false;
		}
	}
    
  var url = 'ajax_check_batch_create_subject_group.php';
  var PoseValue = PostString;
	CheckBatchCreateClassAjax.onreadystatechange = function() {
		if (CheckBatchCreateClassAjax.readyState == 4) {
			var ResponseText = Trim(CheckBatchCreateClassAjax.responseText);
			if (ResponseText != "") {
				if (confirm(ResponseText)) {
					Batch_Create_Subject_Group();
				}
			}
			else {
				Batch_Create_Subject_Group();
			}
		}
	};
  CheckBatchCreateClassAjax.open("POST", url, true);
	CheckBatchCreateClassAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	CheckBatchCreateClassAjax.send(PoseValue);
}

function Check_Selected_Student() {
	var YearIDs = Get_Check_Box_Value("YearID[]","QueryString");
	Select_All_Options('StudentSelected[]',true);
	var StudentSelected = Get_Selection_Value("StudentSelected[]","QueryString");
	Select_All_Options('StudentSelected[]',false);
	var AcademicYearID = Get_Selection_Value("AcademicYearID","QueryString");
	
	CheckSelectedStudAjax = GetXmlHttpObject();
   
  if (CheckSelectedStudAjax == null)
  {
    alert (errAjax);
    return;
  } 
    
  var url = 'ajax_check_selected_student.php';
  var postContent = YearIDs;
  postContent += StudentSelected;
  postContent += AcademicYearID;
  
	CheckSelectedStudAjax.onreadystatechange = function() {
		if (CheckSelectedStudAjax.readyState == 4) {
			ResponseText = Trim(CheckSelectedStudAjax.responseText);
		  ProblemUserIDs = ResponseText.split(',');
		  if (ResponseText != "") {
		  	document.getElementById('StudentSelectedWarningLayer').innerHTML = "<?=$Lang['SysMgr']['SubjectClassMapping']['InvalidSelectedStudWarning']?>";
		  }
		  else {
		  	document.getElementById('StudentSelectedWarningLayer').innerHTML = "";
		  }
		  
		  Select_Selection_Element_By_Array("StudentSelected[]",ProblemUserIDs);
		  //alert(document.getElementById('FormProblem').value);
		}
	};
  CheckSelectedStudAjax.open("POST", url, true);
	CheckSelectedStudAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	CheckSelectedStudAjax.send(postContent);
}

function Check_Timetable_Conflict() {
	var SelectedStudError = document.getElementById('StudentSelectedWarningLayer').innerHTML;
	
	if (SelectedStudError == "") {
		Select_All_Options('StudentSelected[]',true);
		var StudentSelected = Get_Selection_Value("StudentSelected[]","QueryString");
		var SubjectGroupID = document.getElementById('SubjectGroupID').value;
		
		CheckTimetableStudAjax = GetXmlHttpObject();
	   
	  if (CheckTimetableStudAjax == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'ajax_check_timetable_conflict.php';
	  var postContent = StudentSelected;
	  postContent += 'SubjectGroupID='+SubjectGroupID;
	  
		CheckTimetableStudAjax.onreadystatechange = function() {
			if (CheckTimetableStudAjax.readyState == 4) {
				ResponseText = Trim(CheckTimetableStudAjax.responseText);
			  ProblemUserIDs = ResponseText.split(',');
			  
			  if (ResponseText != "") {
			  	document.getElementById('StudentSelectedWarningLayer').innerHTML = "<?=$Lang['SysMgr']['SubjectClassMapping']['TimeTableConflict']?>";
			  	Select_Selection_Element_By_Array("StudentSelected[]",ProblemUserIDs);
			  	return false;
			  }
			  else
			  {
			  	Edit_Subject_Group();
		  	  }
			}
		};
	  CheckTimetableStudAjax.open("POST", url, true);
		CheckTimetableStudAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		CheckTimetableStudAjax.send(postContent);
	}
	else {
		Scroll_To_Element('StudentSelectedWarningLayer','EditLayer');
		Edit_Subject_Group();
	}
}

function Update_Class_List() {
	var YearIDs = Get_Check_Box_Value("YearID[]","QueryString");
	var AcademicYearID = Get_Selection_Value("AcademicYearID","QueryString");
	
	if (Trim(YearIDs) != "") {
		YearClassListAjax = GetXmlHttpObject();
	   
	  if (YearClassListAjax == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'ajax_get_class_list.php';
	  var postContent = YearIDs;
	  postContent += AcademicYearID;
	  
		YearClassListAjax.onreadystatechange = function() {
			if (YearClassListAjax.readyState == 4) {
				ResponseText = Trim(YearClassListAjax.responseText);
			  document.getElementById('YearClassSelectLayer').innerHTML = ResponseText;
			}
		};
	  YearClassListAjax.open("POST", url, true);
		YearClassListAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		YearClassListAjax.send(postContent);
		
		
		Update_Auto_Complete_Extra_Para();
		document.getElementById("YearIDWarningLayer").innerHTML = '';
		document.getElementById('YearIDWarningRow').style.display = "none";
	}
	else {
		document.getElementById("YearIDWarningLayer").innerHTML = '<?=$Lang['SysMgr']['SubjectClassMapping']['InvalidYearIDWarning']?>';
		document.getElementById('YearIDWarningRow').style.display = "";
	}
}

function Check_Class_Code() {
	var SubjectGroupID = document.getElementById("SubjectGroupID").value;
	var ClassCode = document.getElementById("ClassCode").value;
	var YearTermID = document.getElementById("YearTermID").value;
	
	CheckClassCodeAjax = GetXmlHttpObject();
  
  if (Trim(ClassCode) != "" && ClassCode.lastIndexOf(" ") == -1) {
	  if (CheckClassCodeAjax == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'ajax_check_class_code.php';
	  var postContent = "ClassCode="+encodeURIComponent(ClassCode);
	  postContent += "&SubjectGroupID="+SubjectGroupID;
	  postContent += "&YearTermID="+YearTermID;
	  
		CheckClassCodeAjax.onreadystatechange = function() {
			if (CheckClassCodeAjax.readyState == 4) {
				ResponseText = Trim(CheckClassCodeAjax.responseText);
			  if (ResponseText == "1") {
			  	document.getElementById('ClassCodeWarningLayer').innerHTML = "";
			  	document.getElementById('ClassCodeWarningRow').style.display = "none";
			  }
			  else {
			  	document.getElementById('ClassCodeWarningLayer').innerHTML = "<?=$Lang['SysMgr']['SubjectClassMapping']['ClassCodeWarning']?>";
			  	document.getElementById('ClassCodeWarningRow').style.display = "";
			  }
			}
		};
	  CheckClassCodeAjax.open("POST", url, true);
		CheckClassCodeAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		CheckClassCodeAjax.send(postContent);
	}
	else {
		document.getElementById('ClassCodeWarningLayer').innerHTML = "<?=$Lang['SysMgr']['SubjectClassMapping']['ClassCodeWarning']?>";
		document.getElementById('ClassCodeWarningRow').style.display = "";
	}
}

function Check_Class_Title() {
	var ClassTitleEN = document.getElementById('ClassTitleEN').value;
	var ClassTitleB5 = document.getElementById('ClassTitleB5').value;
	
	if (Trim(ClassTitleEN) == "" && Trim(ClassTitleB5) == "") {
		document.getElementById("ClassTitleWarningLayer").innerHTML = "<?=$Lang['SysMgr']['SubjectClassMapping']['ClassTitleWarning']?>";
		document.getElementById("ClassTitleWarningRow").style.display = "";
	}
	else {
		document.getElementById("ClassTitleWarningLayer").innerHTML = "";
		document.getElementById("ClassTitleWarningRow").style.display = "none";
	}
}

function Refresh_Subject_Group_Table(ChangeSortingOrder) {
	var FormValues = Get_Form_Values(document.getElementById('FilterForm'));
	var AcademicYearID = Get_Selection_Value("AcademicYearID","QueryString");
	var YearTermID = Get_Selection_Value("YearTerm","String");
	//var HideNonRelate = (document.getElementById('HiddenNonRelatedSubject').checked)? '1':'0';
	var HideNonRelate = 0;
	
	Block_Element('DetailLayer');
	RefreshAjax = GetXmlHttpObject();
   
  if (RefreshAjax == null)
  {
    alert (errAjax);
    return;
  } 
  
  	// jsCurIndexSortingOrder = "asc" or "desc"
  	if (ChangeSortingOrder == 1)
  	{
  		if (jsCurIndexSortingOrder=="asc")
			jsCurIndexSortingOrder = "desc";
		else if (jsCurIndexSortingOrder == "desc")
			jsCurIndexSortingOrder = "asc";
  	}
	
    
  var url = 'ajax_get_mapping_table.php';
  var postContent = AcademicYearID;
  postContent += '&YearTermID='+YearTermID;
  postContent += '&HideGroupLayers='+HideGroupLayers;
  postContent += '&HideNonRelate='+HideNonRelate;
  postContent += '&'+FormValues;
  postContent += '&SortingOrder='+jsCurIndexSortingOrder;
  
  clearBufferingContent();
  
	RefreshAjax.onreadystatechange = function() {
		if (RefreshAjax.readyState == 4) {
			ResponseText = Trim(RefreshAjax.responseText);
		  document.getElementById('DetailLayer').innerHTML = ResponseText;
		  Thick_Box_Init();
		  Switch_Layer_Panel('Table');
		  
		  
		  UnBlock_Document();
		  UnBlock_Element('DetailLayer');
		}
	};
  RefreshAjax.open("POST", url, true);
	RefreshAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	RefreshAjax.send(postContent);
	
	
	// Hide batch create and import button if the selected year has past already
	$.post(
		"ajax_check_year_term_past.php", 
		{ 
			YearTermID: YearTermID
		},
		function(ReturnData)
		{
			if (ReturnData == "0")
			{
				$('div#BuildDiv').show();
				$('div#ImportMemberDiv').show();
				$('div#QuickEditDiv').show();
			}
			else
			{
				$('div#BuildDiv').hide();
				$('div#ImportMemberDiv').hide();
				$('div#QuickEditDiv').hide();
			}
		}
	);
}

function Show_Class_Detail(SubjectGroupID, OrderByStudentName, CurSortingOrder) {
	Block_Document();
	var AcademicYearID = Get_Selection_Value("AcademicYearID","QueryString");
	var YearTermID = Get_Selection_Value("YearTerm","String");
	
	if (OrderByStudentName == null)
		OrderByStudentName = 0;
		
	// decide the sorting order
	var SortingOrder = "asc";
	if (PreviousStudentSortingOrder != "null" && PreviousIsSortByStudentName != "null")
	{
		if (OrderByStudentName == PreviousIsSortByStudentName && CurSortingOrder != null)
		{
			// SortingOrder = "asc" or "desc"
			if (CurSortingOrder == null)
				CurSortingOrder = 'asc';
		
			// same sorting field => swap the display order
			if (CurSortingOrder=="asc")
				SortingOrder = "desc";
			else
				SortingOrder = "asc";
		}
		else
		{
			// different sorting field => keep the sorting order
			SortingOrder = PreviousStudentSortingOrder;
		}
	}
	
	// Store the sorting field and sorting order for the next sorting order decision
	PreviousIsSortByStudentName = OrderByStudentName;
	PreviousStudentSortingOrder = SortingOrder;
	
	ClassDetailAjax = GetXmlHttpObject();
   
  if (ClassDetailAjax == null)
  {
    alert (errAjax);
    return;
  } 
    
  var url = 'ajax_get_subject_group_detail.php';
  var postContent = AcademicYearID;
  postContent += '&YearTermID='+YearTermID;
  postContent += '&SubjectGroupID='+SubjectGroupID;
  postContent += '&OrderByStudentName='+OrderByStudentName;
  postContent += '&SortingOrder='+SortingOrder;
  
	ClassDetailAjax.onreadystatechange = function() {
		if (ClassDetailAjax.readyState == 4) {
			ResponseText = Trim(ClassDetailAjax.responseText);
		  document.getElementById('SubjectGroupDetailLayer').innerHTML = ResponseText;
		  Switch_Layer_Panel('Detail');
		  Thick_Box_Init();
		  UnBlock_Document();
		}
	};
  ClassDetailAjax.open("POST", url, true);
	ClassDetailAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ClassDetailAjax.send(postContent);
}

function js_Go_Batch_Create_Subject_Group()
{
	$('a#BatchCreateSubjectGroupLink').click();
}

function Get_Batch_Create_Form() {
	var AcademicYearID = Get_Selection_Value("AcademicYearID","QueryString");
	var YearTermID = Get_Selection_Value("YearTerm","QueryString");
	
	BatchFormAjax = GetXmlHttpObject();
   
  if (BatchFormAjax == null)
  {
    alert (errAjax);
    return;
  } 
    
  var url = 'ajax_get_batch_create_class_form.php';
  var postContent = AcademicYearID;
  postContent += YearTermID;
  
	BatchFormAjax.onreadystatechange = function() {
		if (BatchFormAjax.readyState == 4) {
			ResponseText = Trim(BatchFormAjax.responseText);
		  document.getElementById('TB_ajaxContent').innerHTML = ResponseText;
		}
	};
  BatchFormAjax.open("POST", url, true);
	BatchFormAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	BatchFormAjax.send(postContent);
}
}	

// UI function 
{
function Toogle_Group_Show(ShowOrHide) {
	var DisplayOption = "";
	if (ShowOrHide == "Show") {
		document.getElementById('ExpandHideGroupBtnLayer').innerHTML = '<a href="#" onclick="Toogle_Group_Show(\'Hide\'); return false;"><?=$Lang['SysMgr']['SubjectClassMapping']['HideAllGroup']?> </a>';
		DisplayOption = "block";
		HideGroupLayers = "0";
	}
	else {
		document.getElementById('ExpandHideGroupBtnLayer').innerHTML = '<a href="#" onclick="Toogle_Group_Show(\'Show\'); return false;"><?=$Lang['SysMgr']['SubjectClassMapping']['ShowAllGroup']?> </a>';
		DisplayOption = "none";
		HideGroupLayers = "1";
	}
	
	// jquery statement to get all element with class name "SubjectGroupList"
	$('div.SubjectGroupList').css("display",DisplayOption);
}

function Switch_Layer_Panel(ShowWhat) {
	if (ShowWhat == "Table") {
		document.getElementById('SubjectGroupDetailLayer').style.display = "none";
		document.getElementById('SubjectGroupTableLayer').style.display = "block";
	}
	else {
		document.getElementById('SubjectGroupDetailLayer').style.display = "block";
		document.getElementById('SubjectGroupTableLayer').style.display = "none";
	}
}

function Toogle_Show_Subject_Group_Detail(SubjectID) {
	var LinkID = SubjectID+'Link';
	var SubjectGroupLayerClass = SubjectID+'SubjectGroupList';
	/*var ExpandedLayer = SubjectID+'ExpandedLayer';
	var CollapseLayer = SubjectID+'CollapseLayer';*/
	
	if (document.getElementById(LinkID).className == "zoom_in") {
		document.getElementById(LinkID).className = "zoom_out";
		/*document.getElementById(ExpandedLayer).style.display = 'inline';
		document.getElementById(CollapseLayer).style.display = 'none';*/
		$('tr.'+SubjectGroupLayerClass).css('display','');
	}
	else {
		document.getElementById(LinkID).className = "zoom_in";
		/*document.getElementById(ExpandedLayer).style.display = 'none';
		document.getElementById(CollapseLayer).style.display = 'inline';*/
		$('tr.'+SubjectGroupLayerClass).css('display','none');
	}
}

function Toggle_Class_Stat(YearID,subjectID) {
	if (document.getElementById(YearID+"ToggleStatus"+subjectID).value == "0") {
		$('.'+YearID+'ClassStatCol'+subjectID).css("display","");
		document.getElementById(YearID+"ToggleStatus"+subjectID).value = "1";
	}
	else {
		$('.'+YearID+'ClassStatCol'+subjectID).css("display","none");
		document.getElementById(YearID+"ToggleStatus"+subjectID).value = "0";
	}
}

function Show_Hide_ToolBar()
{
	if ($('#YearTerm').val() == '')
		$('#ToolBarDiv').hide();
	else
		$('#ToolBarDiv').show();
}

function js_GoImport_SubjectGroupUser()
{
	var curAcademicYearID = $('#AcademicYearID').val();
	var curYearTermID = $('#YearTerm').val();
	location.href = "import_subject_group_user.php?AcademicYearID=" + curAcademicYearID + "&YearTermID=" + curYearTermID;
}

function js_GoExport_SubjectGroupUser()
{
	var curAcademicYearID = $('#AcademicYearID').val();
	var curYearTermID = $('#YearTerm').val();
	location.href = "export_subject_group_user.php?AcademicYearID=" + curAcademicYearID + "&YearTermID=" + curYearTermID;
}

function js_GoImport_SubjectGroup()
{
	var curAcademicYearID = $('#AcademicYearID').val();
	var curYearTermID = $('#YearTerm').val();
	location.href = "import_subject_group.php?AcademicYearID=" + curAcademicYearID + "&YearTermID=" + curYearTermID;
}

function js_GoExport_SubjectGroup()
{
	var curAcademicYearID = $('#AcademicYearID').val();
	var curYearTermID = $('#YearTerm').val();
	location.href = "export_subject_group.php?AcademicYearID=" + curAcademicYearID + "&YearTermID=" + curYearTermID;
}

function js_Go_Copy_SubjectGroup()
{
	var curAcademicYearID = $('#AcademicYearID').val();
	var curYearTermID = $('#YearTerm').val();
	location.href = "copy_subject_group.php?AcademicYearID=" + curAcademicYearID + "&YearTermID=" + curYearTermID;
}

function js_Go_QuickEdit()
{
	var curAcademicYearID = $('#AcademicYearID').val();
	var curYearTermID = $('#YearTerm').val();
	location.href = "quick_edit_subject_group.php?AcademicYearID=" + curAcademicYearID + "&YearTermID=" + curYearTermID;
}

function js_Select_All_Class(jsYearID, jsChecked)
{
	$('input.ClassChk_' + jsYearID).each( function() {
		$(this).attr('checked', jsChecked);
	})
}

function js_UnCheck_SelectAll(jsYearID, jsChecked)
{
	if (jsChecked == false)
	{
		$('input#' + jsYearID + '-SelectAll').attr('checked', false);
	}
}

function checkAllFormBox(flag){
   if (flag == 0)
   {
      $("input[id^='YearID']").attr('checked', false);
   }
   else
   {
      $("input[id^='YearID']").attr('checked', true);
   }
   
   Update_Class_List(); 
   Check_Selected_Student();

	return;
}

}
</script>