<?php 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/libsubjectpanel.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();
$ldb = new libdb();
$lexport = new libexporttext();

$sqlTitle = "select
                YearName,
                YearID
         From
                YEAR
         Where 
              RecordStatus = 1
         Order by 
              Sequence
";
$resultTitle = $ldb->returnResultSet($sqlTitle);

$sqlBody ="SELECT 
              EN_DES, RecordID
       FROM 
              ASSESSMENT_SUBJECT 
       Where 
              RecordStatus = 1 and (CMP_CODEID = '' or CMP_CODEID is null)
       Order by 
              DisplayOrder";
$resultBody = $ldb->returnResultSet($sqlBody);

$linkedAry = array();
$fcm = new form_class_manage();
$FormList = $fcm->Get_Form_List(false);
$sbj = new subject();
for($i=0; $i<sizeof($FormList); $i++) {
    $_yearId = $FormList[$i]['YearID'];
    
    $subjectLinked = $sbj->Get_Subject_By_Form($_yearId);
    for($j=0; $j<sizeof($subjectLinked); $j++) {
        $__subjectId = $subjectLinked[$j]['RecordID'];
        
        $linkedAry[$__subjectId][$_yearId] = 1;
    }
}

$headerAry = array();
$headerAry[0] = ""; 
for($i=0;$i<count($resultTitle);$i++){
    $headerAry[$i+1] = $resultTitle[$i]['YearName'];
}


$dataAry = array();
 for($i=0;$i<count($resultBody);$i++){
     $dataAry[$i][0] = $resultBody[$i]['EN_DES'];
 }

 for($i=0;$i<count($resultTitle);$i++){     // loop form
     $_yearId = $resultTitle[$i]['YearID'];
     
     for($a=0;$a<count($resultBody);$a++){     // loop subject
         $__subjectId = $resultBody[$a]['RecordID'];
         
         if($linkedAry[$__subjectId][$_yearId]==1) {
            $dataAry[$a][$i+1]="✓"; 
         } else {
            $dataAry[$a][$i+1]="" ;
         }
 }
}
//debug_pr($dataAry);
 
$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);
 
$fileName = 'form_subject_export.csv';
$lexport->EXPORT_FILE($fileName, $exportContent);

?>