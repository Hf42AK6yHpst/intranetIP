<?php
// using 

/******************************************************
 * Modification log:
 *  20170315 Villa #J114063
 *  	- support import acrhive student with strn
 *  20170207 Villa
 *  	- #E97684  - support import archieved user to subject Group
 *  20161122 Villa: [T108937]
 *  - add param for STRN import
 * 	20110621 Marcus:
 * 		- add param YearTermID to constructor subject_class_mapping, 
 * 		YearTermID must be passed after cater same class code in different term
 * ****************************************************/
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

# Get data
$Action = stripslashes($_REQUEST['Action']);

$returnString = '';
if ($Action == "Import_Subject_Group_User")
{

	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
	
	$li = new libdb();
	$libSCM = new subject_class_mapping();
	$limport = new libimporttext();
	
	$TargetFilePath = stripslashes($_REQUEST['TargetFilePath']);
	$data = $limport->GET_IMPORT_TXT($TargetFilePath);
	$col_name = array_shift($data);
	$numOfData = count($data);
	
	$errorCount = 0;
	$successCount = 0;
	$error_result = array();
	$NotExistUserArr = array();
	$DuplicatedUserLoginArr = array();
	$i=1;	
	
	foreach($data as $key => $data)
	{
		$i++;
		$error_msg = array();
		### store csv data to temp data
		list($SubjectGroupCode, $UserLogin) = $data;
		if($ImportMethod){
			$StrnCode = $UserLogin;
			$sql = "Select UserLogin from INTRANET_USER WHERE STRN='".$StrnCode."'";
			$rs = $li->returnResultSet($sql);
			$UserLogin = $rs[0]['UserLogin'];
			if($UserLogin==''){ //if no data -> search acrhive user
				$sql = "Select UserLogin from INTRANET_ARCHIVE_USER  WHERE STRN='".$StrnCode."'";
				$rs = $li->returnResultSet($sql);
				$UserLogin = $rs[0]['UserLogin'];
			}
		}
		$UserLogin = trim($UserLogin);
		$SubjectGroupCode = trim($SubjectGroupCode);
		
		
		### check data
		# 1. Check empty data
		if(empty($UserLogin) || empty($SubjectGroupCode))
			$error_msg[] = $Lang['SysMgr']['FormClassMapping']['MissingData'];
			
		# 2. Check if user login exists
		if (in_array($UserLogin, $NotExistUserArr))
		{
			if ($tmpUserID == '')
			{
				$error_msg[] = $Lang['SysMgr']['FormClassMapping']['NoUserLoginWarning'];
			}
		}
		else
		{	
			$sql = " SELECT UserID, RecordType, Teaching FROM $intranet_db.INTRANET_USER WHERE UserLogin = '$UserLogin' And RecordStatus = 1";
			$tmpResultSet = $li->returnArray($sql);
			$tmpUserID = $tmpResultSet[0]['UserID'];
			if ($tmpUserID == '')
			{
				$sql = " SELECT UserID, RecordType FROM $intranet_db.INTRANET_ARCHIVE_USER WHERE UserLogin = '$UserLogin' ORDER BY DateArchive desc";
				$tmpResultSet = $li->returnResultSet($sql);
				$tmpUserID = $tmpResultSet[0]['UserID']; 
				if ($tmpUserID == ''){
					$error_msg[] = $Lang['SysMgr']['FormClassMapping']['NoUserLoginWarning'];
					$NotExistUserArr[] = $UserLogin;
				}
			}
		}
		$thisIsTeacher = ($tmpResultSet[0]['RecordType'] == 1)? 1 : 0;
		
			
		# 3. Check if subject group exist
		if (!isset($obj_SubjectGroup[$SubjectGroupCode]))
			$obj_SubjectGroup[$SubjectGroupCode] = new subject_term_class('', true, true, $SubjectGroupCode, $YearTermID);
			
		$thisSubjectGroupID = $obj_SubjectGroup[$SubjectGroupCode]->SubjectGroupID;
		if ($thisSubjectGroupID == '')
			$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['NoSubjectGroupWarning'];
		
		# 4. Check if the user is already in the subject group
		$thisUserList = ($thisIsTeacher)? $obj_SubjectGroup[$SubjectGroupCode]->ClassTeacherList : $obj_SubjectGroup[$SubjectGroupCode]->ClassStudentList;
		if (is_array($thisUserList))
		{
			$UserIDList = array();
			$numOfUser = count($thisUserList);
			for ($j=0; $j<$numOfUser; $j++)
				$UserIDList[] = $thisUserList[$j]['UserID'];
			
			if (in_array($tmpUserID, $UserIDList))
				$error_msg[] = ($thisIsTeacher)? $Lang['SysMgr']['SubjectClassMapping']['TeacherInSubjectGroupAlready'] : $Lang['SysMgr']['SubjectClassMapping']['StudentInSubjectGroupAlready'];
		}
		
		# 5. Check if the same person is added to the same subject group for more than one times (in the same csv file)
		if ($DuplicatedUserLoginArr[$UserLogin][$SubjectGroupCode] == 1)
		{
			$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['DuplicatedStudent&SubjectGroupWithinCSVWarning'];
		}
		else
		{
			$sql = "Select 
							TempID 
					From 
							TEMP_SUBJECT_GROUP_USER_IMPORT 
					Where 
							SubjectGroupCode = '".$li->Get_Safe_Sql_Query($SubjectGroupCode)."' 
							And
							UserLogin = '".$li->Get_Safe_Sql_Query($UserLogin)."' 
							And 
							UserID = '".$_SESSION["UserID"]."'
					";
			$TempArr = $li->returnVector($sql);
			if (count($TempArr) > 0)
			{
				$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['DuplicatedStudent&SubjectGroupWithinCSVWarning'];
				$DuplicatedUserLoginArr[$UserLogin][$SubjectGroupCode] = 1;
			}
		}
		
		
		if ($thisIsTeacher == false)
		{
			# 6. Check if the student has joined another subject group of the same subject
			if ($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent']==false)
			{
				$thisSubjectID = $obj_SubjectGroup[$SubjectGroupCode]->SubjectID;
				$hasJoinedSameSubject = $libSCM->Student_Joined_Subject_Group_Of_Subject($thisSubjectID, $YearTermID, array($tmpUserID));
				
				if ($hasJoinedSameSubject)
					$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['StudentHasJoinedOtherSubjectGroupOfTheSameSubject'];
			}
			
			# 7. Check if the student satisfy the Form requirement of the subject group
			$thisYearIDArr = $obj_SubjectGroup[$SubjectGroupCode]->YearID;
			$invalidStudentIDArr = $libSCM->Check_Selected_Student(array($tmpUserID), $thisYearIDArr, $AcademicYearID);
			
			if (count($thisYearIDArr)==0 || is_array($invalidStudentIDArr) && count($invalidStudentIDArr) > 0)
				$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['StudentDoNotMatchFormOfSubjectGroup'];
				
			# 8. Check if the student has joined other subject group which has time clash with the target subject group
			$CrashStudentArr = $libSCM->Check_Timetable_Conflict_Student(array($tmpUserID), $thisSubjectGroupID);
			if (is_array($CrashStudentArr) && count($CrashStudentArr) > 0)
				$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['TimeClashWithOtherSubjectGroup'];
		}
		else
		{
			# 6. Check if the teacher is a teaching staff
			if ($tmpResultSet[0]['Teaching'] != 1)
				$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['TeacherIsNotTeachingStaffWarning'];
				
			# 7. Check if the teacher has joined other subject group which has time clash with the target subject group
			$CrashTeacherArr = $libSCM->Check_Timetable_Conflict_Teacher(array($tmpUserID), $thisSubjectGroupID);
			if (is_array($CrashTeacherArr) && count($CrashTeacherArr) > 0)
				$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['TimeClashWithOtherSubjectGroup'];
		}
		
		
		$sql = "
				insert into TEMP_SUBJECT_GROUP_USER_IMPORT 
				(UserID, RowNumber, UserLogin, IsTeacher, SubjectGroupCode, DateInput)
				values
				(". $_SESSION["UserID"] .", $i, '".$li->Get_Safe_Sql_Query($UserLogin)."', '".$thisIsTeacher."', 
					'".$li->Get_Safe_Sql_Query($SubjectGroupCode)."', now())
				";
		$li->db_db_query($sql) or die(mysql_error());
		$TempID = $li->db_insert_id();
		
		
		### Update Processing Display
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			if(empty($error_msg))
			{
				$successCount++;
				
				$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = "'.$successCount.'";';
			}
			else
			{
				$error_result[$TempID] = $error_msg;
				$error_TempID_str .=  $TempID . ",";
				$errorCount++;
				
				$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = "'.$errorCount.'";';
			}
		
		
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Processed_Number_Span").innerHTML = "'.($i - 1).'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Span").innerHTML = "'.$Lang['SysMgr']['SubjectClassMapping']['GeneratingErrorsInfo'].'...";';
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
		
	### List out the import result
	$x = '';
	if($error_result)
	{
		$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
		
		$x .= "<tr>";
		$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">Row#</td>";
			$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['ClassTitle'] ."</td>";
			$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['FormClassMapping']['StudentLoginID'] ."</td>";
		$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['General']['Remark'] ."</td>";
		$x .= "</tr>";
		
		$error_TempID_str = substr($error_TempID_str, 0,-1);
		$sql = "select * from TEMP_SUBJECT_GROUP_USER_IMPORT where TempID in ($error_TempID_str)";
		$tempData = $li->returnArray($sql);
		
		$i=0;
		foreach($tempData as $k=>$d)
		{	
			list($t_TempID, $t_UserID, $t_RowNumber, $t_UserLogin, $t_IsTeacher, $t_SubjectGroupCode) = $d;
			
			// Add slash to avoid halted 
			$t_UserLogin = $t_UserLogin ? $t_UserLogin : "<font color=\"red\">***</font>";
			$t_SubjectGroupCode = $t_SubjectGroupCode ? $t_SubjectGroupCode : "<font color=\"red\">***</font>";
			
			$t_ClassNumber = $t_ClassNumber ? $t_ClassNumber : "--";
			
			$thisErrorArr = $error_result[$t_TempID];
				
			if (in_array($Lang['SysMgr']['FormClassMapping']['NoUserLoginWarning'], $thisErrorArr))
				$t_UserLogin = "<font color=\"red\">".$t_UserLogin."</font>";
				
			if (in_array($Lang['SysMgr']['SubjectClassMapping']['TeacherInSubjectGroupAlready'], $thisErrorArr) 
				|| in_array($Lang['SysMgr']['SubjectClassMapping']['StudentInSubjectGroupAlready'], $thisErrorArr)
				|| in_array($Lang['SysMgr']['SubjectClassMapping']['TeacherIsNotTeachingStaffWarning'], $thisErrorArr)
				)
				$t_UserLogin = "<font color=\"red\">".$t_UserLogin."</font>";
				
			if (in_array($Lang['SysMgr']['SubjectClassMapping']['NoSubjectGroupWarning'], $thisErrorArr))
				$t_SubjectGroupCode = "<font color=\"red\">".$t_SubjectGroupCode."</font>";
				
			if (in_array($Lang['SysMgr']['SubjectClassMapping']['StudentHasJoinedOtherSubjectGroupOfTheSameSubject'], $thisErrorArr)
				|| in_array($Lang['SysMgr']['SubjectClassMapping']['StudentDoNotMatchFormOfSubjectGroup'], $thisErrorArr)
				|| in_array($Lang['SysMgr']['SubjectClassMapping']['DuplicatedStudent&SubjectGroupWithinCSVWarning'], $thisErrorArr)
				|| in_array($Lang['SysMgr']['SubjectClassMapping']['StudentTimeClashWithOtherSubjectGroup'], $thisErrorArr))
			{
				$t_UserLogin = "<font color=\"red\">".$t_UserLogin."</font>";
				$t_SubjectGroupCode = "<font color=\"red\">".$t_SubjectGroupCode."</font>";
			}
			
			if (is_array($error_result[$t_TempID]))
				$errorDisplay = implode('<br />', $error_result[$t_TempID]);
			else
				$errorDisplay = $error_result[$t_TempID];
			
			$css_i = ($i % 2) ? "2" : "";
			$x .= "<tr style=\"vertical-align:top\">";
			$x .= "<td class=\"tablebluerow$css_i\" width=\"10\">". $t_RowNumber ."</td>";
			$x .= "<td class=\"tablebluerow$css_i\">". $t_SubjectGroupCode ."</td>";
			$x .= "<td class=\"tablebluerow$css_i\">". $t_UserLogin ."</td>";
			$x .= "<td class=\"tablebluerow$css_i\">". $errorDisplay ."</td>";
			$x .= "</tr>";
			
			$i++;
		}
		$x .= "</table>";
	}

	$ErrorCountDisplay = ($errorCount ? "<font color=\"red\"> ": "") . $errorCount . ($errorCount ? "</font>" : "");
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$x.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$successCount.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$ErrorCountDisplay.'\';';
		
		if ($errorCount == 0) 
		{
			$thisJSUpdate .= 'window.parent.document.getElementById("ImportButtonDiv").style.display = "inline";';
		}
		
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}

intranet_closedb();

?>