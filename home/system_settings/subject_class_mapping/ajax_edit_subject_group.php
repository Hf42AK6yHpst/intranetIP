<?php
// using Stanley
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
intranet_opendb();

$YearID = $_REQUEST['YearID'];
$YearTermID = $_REQUEST['YearTermID'];
$SubjectGroupID = $_REQUEST['SubjectGroupID'];
$SubjectComponentID = $_REQUEST['SubjectComponentID'];
$ClassTitleEN = trim(stripslashes(urldecode($_REQUEST['ClassTitleEN'])));
$ClassTitleB5 = trim(stripslashes(urldecode($_REQUEST['ClassTitleB5'])));
$ClassCode = trim(stripslashes(urldecode($_REQUEST['ClassCode'])));
$StudentSelected = (is_array($_REQUEST['StudentSelected']))? $_REQUEST['StudentSelected']: array();
$SelectedClassTeacher = (is_array($_REQUEST['SelectedClassTeacher']))? $_REQUEST['SelectedClassTeacher']: array();

$scm = new subject_class_mapping();
// debug_r($_REQUEST);  
// exit;
$scm->Start_Trans();

$Result = $scm->Edit_Subject_Group($SubjectGroupID,$YearID,$YearTermID,$ClassTitleEN,$ClassTitleB5,$ClassCode,$StudentSelected,$SelectedClassTeacher,$is_user_import,$teacher_benefit,($isCreateClass==1),($createNew==1),$eclass,$addStd,$SubjectComponentID);
// echo $teacher_benefit;
if ($Result) {
	echo $Lang['SysMgr']['SubjectClassMapping']['ClassEditSuccess'];
	$scm->Commit_Trans();
}
else {
	echo $Lang['SysMgr']['SubjectClassMapping']['ClassEditUnsuccess'];
	$scm->RollBack_Trans();
}
intranet_closedb();
?>