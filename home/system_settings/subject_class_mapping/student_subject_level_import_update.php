<?php
// Using: 

############# Change Log [Start] ################
#
#	Date:	2020-05-07 Tommy
#           - modified access checking, added $plugin["Disable_Subject"]
#
#   Date:   2018-08-29 (Bill)   [2017-1207-0956-53277]
#           Create file
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");;
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) {
    No_Access_Right_Pop_Up();
}

$li = new libdb();
$lpf_dbs = new libpf_dbs_transcript();

$linterface = new interface_html();
$lscm_ui = new subject_class_mapping_ui();

$sql = "SELECT * FROM TEMP_STUDENT_SUBJECT_LEVEL_IMPORT WHERE UserID = '". $_SESSION["UserID"]."'";
$data = $li->returnResultSet($sql);
$data = BuildMultiKeyAssoc($data, array('StudentID', 'SubjectID'), 'SubjectLevel', 1, 0);

$result = $lpf_dbs->insertUpdateSubjectLevel($data, '');
if($result) {
    $successCount = count($data);
}

# Step
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

# Page
$CurrentPageArr['Subjects'] = 1;

# Title
$TAGS_OBJ = $lscm_ui->Get_SchoolSettings_Subject_Tab_Array('studentSubjectLevel');
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];

$linterface->LAYOUT_START();
?>

<form name="form1" method="post" action="student_subject_level.php" enctype="multipart/form-data">
<br />

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
    <td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
    <td>
        <table width="91%" border="0" cellpadding="0" cellspacing="0" align="center" >
            <tr>
                <td>
                    <table class="form_table_v30">
                        <tr>
                            <td>
                                <b><?=$successCount?></b> <?=$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['ImportedData']?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>

<input type="hidden" name="classId" value="<?=$classId?>" />
</form>

<div class="edit_bottom_v30">
    <p class="spacer"></p>
    <input type="button" value="<?=$button_finish?>" class="formbutton_v30 print_hide " onclick="document.form1.submit()" id="backBtn" name="backBtn">
    <p class="spacer"></p>
</div>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>