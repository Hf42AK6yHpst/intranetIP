<?
# using: ivan

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();
$lexport = new libexporttext();
$libSCM = new subject_class_mapping();

# Get Academic Year Name for csv file name
$AcademicYearID = $_REQUEST['AcademicYearID'];
$obj_AcademicYear = new academic_year($AcademicYearID);
$AcademicYearName = $obj_AcademicYear->Get_Academic_Year_Name();

# Get Term Name for csv file name
$YearTermID = $_REQUEST['YearTermID'];
$obj_YearTerm = new academic_year_term($YearTermID);
$YearTermName = $obj_YearTerm->Get_Year_Term_Name();


# Get all subject groups
$AllSubjectGroupInfoArr = $libSCM->Get_Subject_Group_List($YearTermID, $SubjectID='', $returnAssociate=1);
$SubjectGroupInfoArr = $AllSubjectGroupInfoArr['SubjectGroupList'];

$SubjectComponentInfoArr = array();
$exportContentArr = array();
if (count($SubjectGroupInfoArr) > 0)
{
	# Loop each Subject Group to get teacher and student list
	foreach ($SubjectGroupInfoArr as $thisSubjectGroupID => $thisSubjectGroupInfoArr)
	{
		# Get Subject Group Basic Info
		$thisSubjectGroupCode = $thisSubjectGroupInfoArr['ClassCode'];
		$thisSubjectGroupTitleEn = $thisSubjectGroupInfoArr['ClassTitleEN'];
		$thisSubjectGroupClassTitleB5 = $thisSubjectGroupInfoArr['ClassTitleB5'];
		
		# Get Applicable Form(s)
		$obj_SubjectGroup = new subject_term_class($thisSubjectGroupID, true, true);
		$thisApplicableFormArr = $obj_SubjectGroup->Get_Applicable_Form_Name_List();
		$thisApplicableFormText = implode('###', $thisApplicableFormArr);
		
		# Has eClass or not
		$thisHasEclass = ($obj_SubjectGroup->Has_eclass())? "Yes" : "No";
		
		# Get Subject Info
		$thisSubjectID = $obj_SubjectGroup->SubjectID;
		$obj_Subject = new subject($thisSubjectID);
		$thisSubjectCode = $obj_Subject->Get_Subject_Code();
		$thisSubjectName = $obj_Subject->Get_Subject_Desc();
		
		if ($fromPage == 'QuickEdit')
			$exportContentArr[] = array($thisSubjectGroupCode, '', $thisSubjectGroupTitleEn, $thisSubjectGroupClassTitleB5, $thisApplicableFormText);
		else
		{
			$thisTempArr = array();
			$thisTempArr[] = $thisSubjectCode;
			$thisTempArr[] = $thisSubjectName;
			
			if ($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent'] == true)
			{
				// export the Subject Component info 
				$thisSubjectComponentID = $obj_SubjectGroup->SubjectComponentID;
				if ($thisSubjectComponentID == '' || $thisSubjectComponentID == 0)
				{
					$thisSubjectComponentCode = '';
					$thisSubjectComponentName = '';
				}
				else
				{
					// Get the Subject Component info if the info has not been retrieved before
					if (!isset($SubjectComponentInfoArr[$thisSubjectComponentID]))
					{
						$ObjSubjectComponent = new Subject_Component($thisSubjectComponentID);
						$SubjectComponentInfoArr[$thisSubjectComponentID]['Code'] = $ObjSubjectComponent->CMP_CODEID;
						$SubjectComponentInfoArr[$thisSubjectComponentID]['Name'] = Get_Lang_Selection($ObjSubjectComponent->CH_DES, $ObjSubjectComponent->EN_DES);
					}
					
					$thisSubjectComponentCode = $SubjectComponentInfoArr[$thisSubjectComponentID]['Code'];
					$thisSubjectComponentName = $SubjectComponentInfoArr[$thisSubjectComponentID]['Name'];
				}
				$thisTempArr[] = $thisSubjectComponentCode;
				$thisTempArr[] = $thisSubjectComponentName;
			}
			
			$thisTempArr[] = $thisSubjectGroupCode;
			$thisTempArr[] = $thisSubjectGroupTitleEn;
			$thisTempArr[] = $thisSubjectGroupClassTitleB5;
			$thisTempArr[] = $thisApplicableFormText;
			$thisTempArr[] = $thisHasEclass;
			
			$exportContentArr[] = $thisTempArr;
		}
	}
}

$exportHeader = array();
if ($fromPage == 'QuickEdit')
{
	$exportHeader = array("Subject Group Code", "New Subject Group Code", "New Name (Eng)", "New Name (Chi)", "Applicable Form");
	$filename = "batch_edit_subject_group_sample.csv";
}
else
{
	$exportHeader[] = "Subject Code";
	$exportHeader[] = "Subject Name";
	if ($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent'] == true)
	{
		$exportHeader[] = "Subject Component Code";
		$exportHeader[] = "Subject Component Name";
	}
	$exportHeader[] = "Subject Group Code";
	$exportHeader[] = "Name (Eng)";
	$exportHeader[] = "Name (Chi)";
	$exportHeader[] = "Applicable Form(s)";
	$exportHeader[] = "Create eClass";
	
	$filename = $AcademicYearName."_".$YearTermName."_subject_group_list";
	$filename = $filename.".csv";
}
	
$export_content = $lexport->GET_EXPORT_TXT($exportContentArr, $exportHeader);
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>