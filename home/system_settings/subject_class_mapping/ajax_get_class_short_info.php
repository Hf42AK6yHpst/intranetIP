<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

intranet_opendb();

$SubjectGroupID = $_REQUEST['SubjectGroupID'];

$SubjectClassUI = new subject_class_mapping_ui();

echo $SubjectClassUI->Get_Class_Short_Info($SubjectGroupID);

intranet_closedb();
?>