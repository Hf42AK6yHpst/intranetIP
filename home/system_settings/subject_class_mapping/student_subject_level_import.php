<?php
//using: 
############# Change Log [Start] ################
#
#	Date:	2020-05-07 Tommy
#           - modified access checking, added $plugin["Disable_Subject"]
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) {
	No_Access_Right_Pop_Up();
}

$linterface = new interface_html();
$lfcm_ui = new form_class_manage_ui();
$lscm_ui = new subject_class_mapping_ui();

// $lscm = new subject_class_mapping();
$lpf_dbs = new libpf_dbs_transcript();

# Form & Class
$academicYearId = Get_Current_Academic_Year_ID();
$classId = $_POST['classId']? $_POST['classId'] : $_GET['classId'];
$classId = IntegerSafe(standardizeFormPostValue($classId));
if(!$classId)
{
	$year_class = new year_class();
	$class_list = $year_class->Get_All_Class_List($academicYearId);
	$classId = $class_list[0]['YearClassID'];
}
$year_class = new year_class($classId, true, false, true);
$yearId = $year_class->YearID;

# Class Selection
$classSelection = $lfcm_ui->Get_Class_Selection($academicYearId, '', 'classId', $classId, '', 1);

# form class csv sample
$csvFile = "<a class='tablelink' href='javascript:GetTemplate()'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";

# data column
$DataColumnTitleArr = array($Lang['General']['UserLogin'],$Lang['General']['Class'],$Lang['General']['ClassNumber'],$Lang['SysMgr']['SubjectClassMapping']['ClassTitleB5'],$Lang['SysMgr']['SubjectClassMapping']['ClassTitleEN']);
$DataColumnPropertyArr = array(1);
$RemarkArr = array('','','','','','','','');
//$DataColumn = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr);
$DataColumn = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr,$RemarkArr);

# Step
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# Page
$CurrentPageArr['Subjects'] = 1;

# Title
$TAGS_OBJ = $lscm_ui->Get_SchoolSettings_Subject_Tab_Array('studentSubjectLevel');
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];

$linterface->LAYOUT_START();
?>

<script type='text/javascript'>
function GetTemplate(){
	window.location.href = 'student_subject_level_export.php?isTemplate=1&classId=' + $('#classId').val();
}
</script>
<div id="remarkDiv_type" class="selectbox_layer" style="width:400px">	</div>
						
<br />
<form name="form1" method="post" action="student_subject_level_import_confirm.php" enctype="multipart/form-data">
<table id="html_body_frame" width="90%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	
	
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr> 
	   				<td>
	       			<br />
						<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">	
							<tr>
								<td class="field_title" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
								<td class="tabletext"><input class="file" type="file" name="csvfile"></td>
							</tr>
							<tr>
								<td class="field_title" align="left"><?=$Lang['General']['Class']?> </td>
								<td class="tabletext"><?=$classSelection?></td>
							</tr>
							<tr>
								<td class="field_title" align="left"><?=$Lang['General']['CSVSample']?> </td>
								<td class="tabletext"><?=$csvFile?></td>
							</tr>
							<tr>
								<td class="field_title" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?> </td>
								<td class="tabletext"><?=$DataColumn?></td>
							</tr>
					
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
        			<td align="left" class="tabletextremark"><?=$i_general_required_field2?></td>
				</tr>	
				<tr>
					<td class="tabletext" colspan="2"><?=$format_str?></td>
				</tr>
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	
	</td>
	</tr>
	
	<!--
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				
			</table>
		</td>
	</tr>
	-->
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>
			&nbsp; 
			<?=$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='student_subject_level.php'")?>
		</td>
	</tr>
</table>

</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>