<?php
// using:

############# Change Log [Start] ################
#
#	Date:	2018-07-06	Vito
#			First create this file
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/libsubjectpanel.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$ldb = new libdb();
$lexport = new libexporttext();
$linterface = new interface_html();

$SubjectId = standardizeFormPostValue($_POST['SubjectId']);
$keyword = $ldb->Get_Safe_Sql_Like_Query(standardizeFormPostValue($_POST['keyword']));
$order = standardizeFormPostValue($_POST['order']);
$field = standardizeFormPostValue($_POST['field']);

if($field == 0){
    $orderField = 'TeacherName';
}else{
    $orderField = 'SubjectName';
}

if($order == 0){
    $orderStr = 'DESC';
}else{
    $orderStr = 'ASC';
}

$sql =" SELECT
            IU.EnglishName as TeacherName,
            ASS.EN_DES as SubjectName,
            GROUP_CONCAT(DISTINCT Y.YearName SEPARATOR ',') AS YearName
        FROM
            ASSESSMENT_ACCESS_RIGHT AAR
                INNER JOIN INTRANET_USER IU ON (AAR.UserID = IU.UserID)
				INNER JOIN ASSESSMENT_SUBJECT ASS ON (AAR.SubjectID = ASS.RecordID)
				INNER JOIN YEAR Y ON (AAR.YearID = Y.YearID)
        WHERE
            AAR.AccessType = '1' ";

if ($keyword != null ||	$keyword != ''){
    $sql .=" And (
		     IU.EnglishName Like '%$keyword%'
			 Or ASS.EN_DES Like '%$keyword%'
			 Or Y.YearName Like '%$keyword%'
            )
           ";
}
if ($SubjectId != null ||	$SubjectId != ''){
    $sql .=" And ASS.RecordID = '$SubjectId'";
}
$sql .="    GROUP BY
                AAR.UserID,
				AAR.SubjectID
                Order by $orderField $orderStr
       ";

$result = $ldb->returnResultSet($sql);
// debug_pr($sql);
// debug_pr($result);
?>

<?php 
$countRow = sizeof($result);
// debug_pr($countRow);

$display = "";
$display .= "<table class='common_table_list_v30 view_table_list_v30'>";
$display .= "<tr>";
$display .= "<th width='40%'>" . $Lang['SysMgr']['Timetable']['TeacherName'] . "</th>";
$display .= "<th width='30%'>" . $Lang['SysMgr']['SubjectClassMapping']['Subject'] ."</th>";
$display .= "<th width='30%'>" . $Lang['General']['Form'] ."</th>";
$display .= "</tr>";

// loop the sql result array
for ($i = 0; $i < $countRow; $i++){
    $display .= "<tr>";
    $display .= "<td width='40%'>" . $result[$i][TeacherName] . "</td>";
    $display .= "<td width='30%'>" . $result[$i][SubjectName] . "</td>";
    $copyYearName = str_replace(",", ", ", $result[$i][YearName]);
    $display .= "<td width='30%'>" . $copyYearName . "</td>";
    $display .= "</tr>";
}
$display .= "</table>";
// debug_pr($button_print);
?>

<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>

<!-- Display result //-->
<?=$display?>

<?php
intranet_closedb();
?>
