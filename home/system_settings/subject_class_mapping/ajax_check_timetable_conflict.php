<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

$StudentSelected = (is_array($_REQUEST['StudentSelected']))? $_REQUEST['StudentSelected']:array();
$SubjectGroupID = $_REQUEST['SubjectGroupID'];
/*echo '<pre>';
var_dump($_REQUEST);
echo '</pre>';
die;*/
$scm = new subject_class_mapping();

echo implode(',',$scm->Check_Timetable_Conflict_Student($StudentSelected,$SubjectGroupID));

intranet_closedb();
?>