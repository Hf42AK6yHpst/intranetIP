<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);
$SelfID = stripslashes($_REQUEST['SelfID']);


if ($RecordType == "Both")
{
	$InputValueEn = stripslashes($_REQUEST['InputValueEn']);
	$InputValueCh = stripslashes($_REQUEST['InputValueCh']);
	$OjbLearningCategory = new Learning_Category($SelfID);
	$isExistedTitle = $OjbLearningCategory->Is_Title_Existed($RecordType, $InputValueCh, $InputValueEn);
}
else
{
	$InputValue = stripslashes($_REQUEST['InputValue']);
	$OjbLearningCategory = new Learning_Category($SelfID);
	$isExistedTitle = $OjbLearningCategory->Is_Title_Existed($RecordType, $InputValue);
}

if ($isExistedTitle)
	echo $Lang['SysMgr']['SubjectClassMapping']['Warning']['DuplicatedTitle']['LearningCategory'];
else
	echo "";


intranet_closedb();

?>