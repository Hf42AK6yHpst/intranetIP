<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
intranet_opendb();

$SubjectGroupID = $_REQUEST['SubjectGroupID'];
$YearTermID = $_REQUEST['YearTermID'];
$YearID = $_REQUEST['YearID'];
$SubjectID = $_REQUEST['SubjectID'];
$AcademicYearID = $_REQUEST['AcademicYearID'];

$SubjectClassUI = new subject_class_mapping_ui();

echo $SubjectClassUI->Get_Class_Form($YearTermID,$YearID,$SubjectID,$SubjectGroupID,$AcademicYearID);

intranet_closedb();
?>