<?php
// Modifying by: 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $Lang;

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"])) { 
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

if ($AcademicYearID == '')
	$AcademicYearID = Get_Current_Academic_Year_ID();
if ($YearTermID == '')
	$YearTermID = getCurrentSemesterID();

if($intranet_session_language=='en'){
	$targetTitle = 'SubjectDescEN';
	$alternative = 'SubjectDescB5';
} else {
	$targetTitle = 'SubjectDescB5';
	$alternative = 'SubjectDescEN';
}

// Get subject list
$scm = new subject_class_mapping();
$SubjectList = $scm->Get_Subject_List();

// Get subject group list and related subject list
if($Action == 'Reload_Subject_Group_Code'){
	$relatedSubjectArr = array();
	$noOfRelatedSubject = count($SubjectList);
	if($noOfRelatedSubject > 0){
		foreach ($SubjectList as $Subject){
			if($Subject[$targetTitle] == null || $Subject[$targetTitle] == ''){
				$relatedSubjectArr[$Subject['SubjectID']] = $Subject[$alternative];
			} else {
				$relatedSubjectArr[$Subject['SubjectID']] = $Subject[$targetTitle];
			}
		}
	}
	$SubjectGroupList = $scm->Get_Subject_Group_List($YearTermID);
	$SubjectGroupList = $SubjectGroupList['SubjectGroupList'];
}

$x .= "<table cellspacing='0' cellpadding='0' border='0' width='100%' class='common_table_list_v30 view_table_list_v30'>\n";
	$x .= "<tbody>\n";
		# Table Header
		$x .= "<tr class=''>\n";
			$x .= "<th width='1' class=''>#</th>\n";
			if($Action == 'Reload_Subject_Code'){
				$x .= "<th width='40%' class=''>".$Lang['SysMgr']['SubjectClassMapping']['CSVField']['SubjectCode']."</th>\n";
				$x .= "<th width='60%' class=''>".$Lang['SysMgr']['SubjectClassMapping']['CSVField']['SubjectName']."</th>\n";
			} else if($Action == 'Reload_Subject_Group_Code'){
				$x .= "<th width='35%' class=''>".$Lang['SysMgr']['SubjectClassMapping']['CSVField']['SubjectGroupCode']."</th>\n";
				$x .= "<th width='30%' class=''>".$Lang['SysMgr']['SubjectClassMapping']['CSVField']['SubjectName']."</th>\n";
				$x .= "<th width='35%' class=''>".$Lang['SysMgr']['SubjectClassMapping']['CSVField']['SubjectGroupName']."</th>\n";
			}
	$x .= "</tr>\n";

		# Table Content
		if($Action == 'Reload_Subject_Code'){
			$thisNumber = 1;
			$noOfSubject = count($SubjectList);
			if($noOfSubject > 0){
				foreach ($SubjectList as $Subject){
					
					$thisSubjectCode = $Subject['WEBSAMSCode'];
					if($Subject[$targetTitle] == null || $Subject[$targetTitle] == ''){
						$thisSubjectName = $Subject[$alternative];
					} else {
						$thisSubjectName = $Subject[$targetTitle];
					}		
					
					$x .= "<tr class='row_approved'>\n";
						$x .= "<td valign='top'>".$thisNumber."</td>\n";
						$x .= "<td valign='top'>".$thisSubjectCode."</td>\n";
						$x .= "<td valign='top'>".$thisSubjectName."</td>\n";
					$x .= "</tr>\n";
					
					$thisNumber++;
				}
			} else {
				$x .= $Lang['General']['NoRecordFound'];
			}
		}
		else if($Action == 'Reload_Subject_Group_Code'){
			$thisNumber = 1;
			if($intranet_session_language=='en'){
				$targetGroupTitle = 'ClassTitleEN';
				$alternative = 'ClassTitleB5';
			} else {
				$targetGroupTitle = 'ClassTitleB5';
				$alternative = 'ClassTitleEN';
			}
			
			if(count($SubjectGroupList) > 0){
				foreach ($SubjectGroupList as $SubjectGroup){
					$thisGroupCode = $SubjectGroup['ClassCode'];
					$thisSubjectName = $relatedSubjectArr[$SubjectGroup['SubjectID']];
					
					if($SubjectGroup[$targetGroupTitle] == null || $SubjectGroup[$targetGroupTitle] == ''){
						$thisGroupName = $SubjectGroup[$alternative];
					} else {
						$thisGroupName = $SubjectGroup[$targetGroupTitle];
					}
					
					$x .= "<tr class='row_approved'>\n";
						$x .= "<td valign='top'>".$thisNumber."</td>\n";
						$x .= "<td valign='top'>".$thisGroupCode."</td>\n";
						$x .= "<td valign='top'>".$thisSubjectName."</td>\n";
						$x .= "<td valign='top'>".$thisGroupName."</td>\n";
					$x .= "</tr>\n";
					
					$thisNumber++;
				}
			} else {
				$x .= $Lang['General']['NoRecordFound'];
			}
		}
	
	$x .= "<tr height='10px'>";
		$x .= "<td></td>";
	$x .= "</tr>";
$x .= "</table>";
$x .= "</td></tr></table>";

$x .= "</tbody>\n";
$x .= "</table>\n";	
		
echo $x;

intranet_closedb();
?>