<?
// Modifying by: 

/*
 * 
 *	2020-05-07 Tommy
 *      - modified access checking, added $plugin["Disable_Subject"]
 *           
 * 	20140730 Bill:
 * 		- add Subject Group Code checking (space)
 * 
 * 	20110621 Marcus:
 * 		- add param YearTermID to constructor subject_class_mapping, 
 * 		YearTermID must be passed after cater same class code in different term
 * 
 * 20100826 (Ivan)
 * 	- added the "Applicable Form" logic
 * 	- moved the Temp Table Create SQL to addon schema
 * 
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");


intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$AcademicYearID = $_REQUEST['AcademicYearID'];
$YearTermID = $_REQUEST['YearTermID'];

$obj_AcademicYear = new academic_year($AcademicYearID);
$obj_YearTerm = new academic_year_term($YearTermID);

$li = new libdb();
$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: quick_edit_subject_group.php?AcademicYearID=".$AcademicYearID."&YearTermID=".$YearTermID."&xmsg=import_failed");
	exit();
}
$data = $limport->GET_IMPORT_TXT($csvfile);

$col_name = array_shift($data);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$file_format = array("Subject Group Code", "New Subject Group Code", "New Name (Eng)", "New Name (Chi)", "Applicable Form");
$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}


$numOfData = count($data);
if($format_wrong || $numOfData==0)
{
	$returnMsg = ($format_wrong)? 'import_header_failed' : 'import_no_record';
	intranet_closedb();
	header("location: quick_edit_subject_group.php?AcademicYearID=".$AcademicYearID."&YearTermID=".$YearTermID."&xmsg=".$returnMsg);
	exit();
}

# Title / Menu
$linterface = new interface_html();
$CurrentPageArr['Subjects'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['ImportSubjectGroup']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

# create temp table if temp table not exists
/* moved to addon schema
$sql = "CREATE TABLE IF NOT EXISTS TEMP_SUBJECT_GROUP_QUICK_EDIT
	(
		TempID int(11) NOT NULL auto_increment,
		UserID int(11),
		RowNumber int(11),
		SubjectGroupCode varchar(50),
		NewSubjectGroupCode varchar(50),
		NewTitleEn varchar(255),
		NewTitleCh varchar(255),
		SubjectGroupID int(11),
		DateInput datetime,
		PRIMARY KEY (TempID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$li->db_db_query($sql) or die(mysql_error());
*/

# delete the temp data in temp table 
$sql = "delete from TEMP_SUBJECT_GROUP_QUICK_EDIT where UserID=".$_SESSION["UserID"];
$li->db_db_query($sql) or die(mysql_error());

$errorCount = 0;
$successCount = 0;
$error_result = array();
$i=1;


foreach($data as $key => $data)
{
	$i++;
	$error_msg = array();
	
	### store csv data to temp data
	list($SubjectGroupCode, $NewSubjectGroupCode, $NewTitleEn, $NewTitleCh, $NewApplicableForm) = $data;
	$SubjectGroupCode = trim($SubjectGroupCode);
	$NewSubjectGroupCode = trim($NewSubjectGroupCode);
	$NewTitleEn = trim($NewTitleEn);
	$NewTitleCh = trim($NewTitleCh);
	$SubjectID = "";
	
	### check data
	# 1. Check empty data
	if(empty($SubjectGroupCode))
		$error_msg[] = $Lang['SysMgr']['FormClassMapping']['MissingData'];
		
	# 2. Check if subject group exist
	$obj_SubjectGroup = new subject_term_class('', false, false, $SubjectGroupCode, $YearTermID);
	if ($obj_SubjectGroup->SubjectGroupID == '' || in_array($YearTermID, $obj_SubjectGroup->YearTermID) == false)
		$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupNotExistWarning'];
	else {
		if ($SubjectDetail[$obj_SubjectGroup->SubjectID] == "") {
			$SubjectDetail[$obj_SubjectGroup->SubjectID] = new subject($obj_SubjectGroup->SubjectID,true);
		}
		$SubjectID = $obj_SubjectGroup->SubjectID;
	}
	
	# 3. Check if Subject Group Code Duplicated within the csv file
	$sql = "Select TempID From TEMP_SUBJECT_GROUP_QUICK_EDIT Where SubjectGroupCode = '".$li->Get_Safe_Sql_Query($SubjectGroupCode)."' And UserID = '".$_SESSION["UserID"]."'";
	$TempArr = $li->returnVector($sql);
	if (count($TempArr) > 0)
		$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeDuplicatedWithinCSVWarning'];
	
	# 4. Check if new subject group code duplicated with the existing subject group
	$obj_NewSubjectGroup = new subject_term_class('', false, false, $NewSubjectGroupCode, $YearTermID);
	if ($obj_NewSubjectGroup->SubjectGroupID != '')
		$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeUsedWarning'];
		
	# 5. Check if New Subject Group Code Duplicated within the csv file
	$sql = "Select TempID From TEMP_SUBJECT_GROUP_QUICK_EDIT Where NewSubjectGroupCode = '".$li->Get_Safe_Sql_Query($NewSubjectGroupCode)."' And UserID = '".$_SESSION["UserID"]."'";
	$TempArr = $li->returnVector($sql);
	if (count($TempArr) > 0 && $NewSubjectGroupCode != '')
		$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['NewSubjectGroupCodeDuplicatedWithinCSVWarning'];
	
	# 6. Check if Form name valid
	$libYear = new Year();
	$thisYearIDArr = array();
	$FormNameArr = explode('###', $NewApplicableForm);
	$numOfFormName = count($FormNameArr);
	for ($j=0; $j<$numOfFormName; $j++)
	{
		$thisYearID = $libYear->returnYearIDbyYearName($FormNameArr[$j]);
		if ($thisYearID == '')
		{
			//debug_pr($ClassNameArr[$i]);
			if (!in_array($Lang['SysMgr']['SubjectClassMapping']['NoFormWarning'],$error_msg))
				$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['NoFormWarning'];
		}
		else
		{
			$thisYearIDArr[] = $thisYearID;
		}
	}
	if ($SubjectID != "") {
		for ($j=0; $j < sizeof($thisYearIDArr); $j++) {
			if (!in_array($thisYearIDArr[$j],$SubjectDetail[$SubjectID]->YearRelationID)) {
				$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['OutOfFormRelation'];
				break;
			}
		}
	}
	$thisYearIDList = implode('###', $thisYearIDArr);
	
	# 7. Check if New Subject Group Code contains any space
	if (preg_match('/\s/',$NewSubjectGroupCode)){
		$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeNoSpace'];
	}
	
	# insert temp record
	$sql = "
			insert into TEMP_SUBJECT_GROUP_QUICK_EDIT 
			(UserID, RowNumber, SubjectGroupCode, NewSubjectGroupCode, NewTitleEn, NewTitleCh, SubjectGroupID, ApplicableFormList, ApplicableFormIDList, DateInput)
			values
			(	". $_SESSION["UserID"] .", 
				$i, 
				'".$li->Get_Safe_Sql_Query($SubjectGroupCode)."',
				'".$li->Get_Safe_Sql_Query($NewSubjectGroupCode)."',
				'".$li->Get_Safe_Sql_Query($NewTitleEn)."',
				'".$li->Get_Safe_Sql_Query($NewTitleCh)."',
				'".$obj_SubjectGroup->SubjectGroupID."',
				'".$li->Get_Safe_Sql_Query($NewApplicableForm)."',
				'".$li->Get_Safe_Sql_Query($thisYearIDList)."',
				now())
			";
	$li->db_db_query($sql) or die(mysql_error());
	$TempID = $li->db_insert_id();
	
	if(empty($error_msg))
	{
		$successCount++;
	}
	else
	{
		$error_result[$TempID] = $error_msg;
		$error_TempID_str .=  $TempID . ",";
		$errorCount++;
	}
}

### List out the import result
$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['SysMgr']['FormClassMapping']['SchoolYear'] ."</td>";
$x .= "<td class='tabletext'>". $obj_AcademicYear->Get_Academic_Year_Name() ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['SysMgr']['FormClassMapping']['Term'] ."</td>";
$x .= "<td class='tabletext'>". $obj_YearTerm->Get_Year_Term_Name() ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['SuccessfulRecord'] ."</td>";
$x .= "<td class='tabletext'>". $successCount ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['FailureRecord'] ."</td>";
$x .= "<td class='tabletext'>". ($errorCount ? "<font color='red'>":"") . $errorCount . ($errorCount ? "</font>":"") ."</td>";
$x .= "</tr>";

$x .= "</table><br>";

if($error_result)
{
	$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
	
	$x .= "<tr>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">Row#</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCode'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['NewSubjectGroupCode'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['NewNameEn'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['NewNameCh'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['ApplicableForm'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['General']['Remark'] ."</td>";
	$x .= "</tr>";
	
	$error_TempID_str = substr($error_TempID_str, 0,-1);
	$sql = "select * from TEMP_SUBJECT_GROUP_QUICK_EDIT where TempID in ($error_TempID_str)";
	$tempData = $li->returnArray($sql);
	
	$i=0;
	foreach($tempData as $k => $d)
	{
		list($t_TempID, $t_UserID, $t_RowNumber, $t_SubjectGroupCode, $t_NewSubjectGroupCode, $t_NewTitleEn, $t_NewTitleCh, $t_SubjectGroupID, $t_ApplicableForm) = $d;
		
		$t_SubjectGroupCode = $t_SubjectGroupCode ? $t_SubjectGroupCode : "<font color='red'>***</font>";
		
		$thisErrorArr = $error_result[$t_TempID];
		
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['SubjectGroupNotExistWarning'], $thisErrorArr))
			$t_SubjectGroupCode = "<font color='red'>".$t_SubjectGroupCode."</font>";
			
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeDuplicatedWithinCSVWarning'], $thisErrorArr))
			$t_SubjectGroupCode = "<font color='red'>".$t_SubjectGroupCode."</font>";
			
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeUsedWarning'], $thisErrorArr))
			$t_NewSubjectGroupCode = "<font color='red'>".$t_NewSubjectGroupCode."</font>";
			
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['NewSubjectGroupCodeDuplicatedWithinCSVWarning'], $thisErrorArr))
			$t_NewSubjectGroupCode = "<font color='red'>".$t_NewSubjectGroupCode."</font>";
		
		// Subject Group Code: Space
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeNoSpace'], $thisErrorArr))
			$t_NewSubjectGroupCode = "<font color='red'>".$t_NewSubjectGroupCode."</font>";
		
		if (in_array($Lang['SysMgr']['FormClassMapping']['FormNotExistWarning'], $thisErrorArr) || in_array($Lang['SysMgr']['SubjectClassMapping']['OutOfFormRelation'],$thisErrorArr))
			$t_ApplicableForm = "<font color='red'>".$t_ApplicableForm."</font>";
		
		
		
		if (is_array($error_result[$t_TempID]))
			$errorDisplay = implode('<br />', $error_result[$t_TempID]);
		else
			$errorDisplay = $error_result[$t_TempID];
		
		$css_i = ($i % 2) ? "2" : "";
		$x .= "<tr style='vertical-align:top'>";
		$x .= "<td class=\"tablebluerow$css_i\" width=\"10\">". $t_RowNumber ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_SubjectGroupCode ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_NewSubjectGroupCode ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_NewTitleEn ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_NewTitleCh ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_ApplicableForm ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $errorDisplay ."</td>";
		$x .= "</tr>";
		
		$i++;
	}
	$x .= "</table>";
}


if($errorCount>0)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='quick_edit_subject_group.php?AcademicYearID=".$AcademicYearID."&YearTermID=".$YearTermID."'");
}
else
{
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit");
	$import_button .= " &nbsp;";
	$import_button .= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='quick_edit_subject_group.php?AcademicYearID=".$AcademicYearID."&YearTermID=".$YearTermID."'");
}
?>

<br />
<form name="form1" method="post" action="quick_edit_subject_group_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center"><?=$x?></td>
</tr>
		
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
<input type="hidden" name="YearTermID" value="<?=$YearTermID?>">

</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
