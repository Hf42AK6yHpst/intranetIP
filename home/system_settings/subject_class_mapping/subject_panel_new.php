<?php
//using: 
############# Change Log [Start] ################
#	Date:	2020-05-07 Tommy
#           - modified access checking, added $plugin["Disable_Subject"]
#
#	Date:	2015-09-04	Ivan
#			First create this file
#			ip.2.5.6.10.1	*** Need to upload with "/includes/subject_class_mapping_ui.php" before deployment *** 
# 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) {
	No_Access_Right_Pop_Up();
}

$luser = new libuser();
$lscm_ui = new subject_class_mapping_ui();
$lfcm = new form_class_manage();
$linterface = new interface_html();

$CurrentPageArr['Subjects'] = 1;
$TAGS_OBJ = $lscm_ui->Get_SchoolSettings_Subject_Tab_Array('subjectPanel');
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];

$linterface->LAYOUT_START($returnMsg);


### navigation
$navigationAry[] = array($Lang['General']['SubjectPanel'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Btn']['Add']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);


### subject selection
$htmlAry['subjectSel'] = $lscm_ui->Get_Subject_Selection('subjectId', $Selected='', $OnChange='', $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=0, $IncludeSubjectIDArr='', $ExtrudeSubjectIDArr=array());


### form checkboxes
$formAry = $lfcm->Get_Form_List($GetMappedClassInfo=false);
$numOfForm = count($formAry);

$formChkAry = array();
for ($i=0; $i<$numOfForm; $i++) {
	$_formId = $formAry[$i]['YearID'];
	$_formName = $formAry[$i]['YearName'];
	
	$checkboxAry[] = array('formChk_'.$_formId, 'formIdAry[]', $_formId, false, '', $_formName);
}
$htmlAry['formCheckboxes'] = $linterface->Get_Checkbox_Table('formChk', $checkboxAry, $itemPerRow=3, $defaultCheck=false);


### teacher selection
$deselectedGroup = array();
$teacherAry = $luser->returnUsersType2(USERTYPE_STAFF, $teaching=1);
$numOfTeacher = count($teacherAry);
for ($i=0; $i<$numOfTeacher; $i++) {
	$_userId = $teacherAry[$i]['UserID'];
	$_userName = $teacherAry[$i][1];
	
	$deselectedGroup[] = array('value'=>$_userId, 'name'=>$_userName);
}
$settingsAry = array(
	'deselectedHeader' => $Lang['SysMgr']['RoleManagement']['Users'],
	'deselectedSelectionName' => 'deselectedCategory',
	'deselectedList' => $deselectedGroup,
	'selectedHeader' => $Lang['SysMgr']['RoleManagement']['SelectedUser'],
	'selectedSelectionName' => 'teacherIdAry',
	'selectedList' => array()
);
$htmlAry['teacherSel'] = $linterface->generateUserSelection($settingsAry);


### form table
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	// subject selection
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $htmlAry['subjectSel']."\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	// form checkboxes
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['General']['Form'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $htmlAry['formCheckboxes']."\n";
			$x .= $linterface->Get_Form_Warning_Msg('formEmptyWarnDiv', $Lang['General']['WarningArr']['PleaseSelectAtLeastOneForm'], $Class='warnMsgDiv')."\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	// teacher selection
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['General']['Member2'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $htmlAry['teacherSel']."\n";
			$x .= $linterface->Get_Form_Warning_Msg('teacherEmptyWarnDiv', $Lang['General']['WarningArr']['PleaseSelectAtLeastOneTeacher'], $Class='warnMsgDiv')."\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$x .= '<br style="clear:both;">'."\r\n";
$htmlAry['formTbl'] = $x;


### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goBack()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

?>
<script type="text/javascript">
$(document).ready( function() {
	$('#AddAll').click(function(){
		$('#teacherIdAry').append( $('#deselectedCategory').find('option') );
	});
	$('#Add').click(function(){
		$('#teacherIdAry').append( $('#deselectedCategory').find('option:selected') );
	});
	
	$('#Remove').click(function(){
		$('#teacherIdAry > option:selected').each(function(){
			var deletedName;
			if($(this).val() == ''){
				deletedName = $(this).text().substring(2);
			}else{
				deletedName = $(this).text();
			}
			$('#deselectedCategory').append($(this));
		});
	});
	$('#RemoveAll').click(function(){
		$('#teacherIdAry > option').each(function(){
			var deletedName;
			if($(this).val() == ''){
				deletedName = $(this).text().substring(2);
			}else{
				deletedName = $(this).text();
			}
			$('#deselectedCategory').append($(this));
		});
	});
});

function goSubmit() {
	$('input.actionBtn').attr('disabled', 'disabled');
	$('div.warnMsgDiv').hide();
	$('#teacherIdAry > option').attr('selected', 'selected');
	
	var canSubmit = true;
	if( $('input.formChk:checked').length == 0){
		$('div#formEmptyWarnDiv').show();
		canSubmit = false;
	}
	if( $('#teacherIdAry > option').length == 0){
		$('div#teacherEmptyWarnDiv').show();
		canSubmit = false;
	}
	
	if (canSubmit) {
		$('form#form1').attr('action', 'subject_panel_new_update.php').submit();
	}
	else {
		$('input.actionBtn').attr('disabled', '');
	}
}

function goBack() {
	window.location = "subject_panel_list.php";
}
</script>
<form name="form1" id="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	
	<div class="table_board">
		<?=$htmlAry['formTbl']?>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
</form>
<?php 
$linterface->LAYOUT_STOP(); 
intranet_closedb();
?>