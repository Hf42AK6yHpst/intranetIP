<?php
// Modifying by: 
@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

/**************************************************
 * Modification log:
 *  20200507 Tommy
 *      - modified access checking, added $plugin["Disable_Subject"]
 * 	20110620 Marcus:
 * 		- use $_POST instead of $_REQUEST, as the value retrieved by $_REQUEST may from cookies and caused error.  
 * ***********************************************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

### Get Data
$ToAcademicYearID = $_POST['AcademicYearID'];
$ToYearTermID = $_POST['YearTermID'];
$FromAcademicYearID = $_POST['SelectedAcademicYearID'];
$FromYearTermID = $_POST['SelectedYearTermID'];
$Copy_SubjectGroup_Arr = $_POST['Copy_SubjectGroup_Arr'];
$Teacher_SubjectGroup_Arr = $_POST['Teacher_SubjectGroup_Arr'];
$Student_SubjectGroup_Arr = $_POST['Student_SubjectGroup_Arr'];
$Leader_SubjectGroup_Arr = $_POST['Leader_SubjectGroup_Arr'];
$CopyTimetable = $_POST['CopyTimetable'];
$Create_eClass = $_POST['Create_eClass'];


$obj_FromAcademicYear = new academic_year($FromAcademicYearID);
$obj_FromYearTerm = new academic_year_term($FromYearTermID);

$obj_ToAcademicYear = new academic_year($ToAcademicYearID);
$obj_ToYearTerm = new academic_year_term($ToYearTermID);



### Title / Menu
$CurrentPageArr['Subjects'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['CopySubjectGroup'];
$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['CopySubjectGroup']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 


# step information
$STEPS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['Options'], 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['CopyResult'], 0);

$libSCM = new subject_class_mapping();
$libTimetable = new Timetable();


# Get timetable info if needed
if ($CopyTimetable)
{
	### Get Timetime of the From Term
	$FromTimetableArr = $libTimetable->Get_All_TimeTable($FromAcademicYearID, $FromYearTermID);
	$FromTimetableIDArr = Get_Array_By_Key($FromTimetableArr, 'TimetableID');
	$numOfFromTimetable = count($FromTimetableIDArr);
	
	### Check if there are any timetables which have been copied before
	$ToTimetableIDArr = $libTimetable->Get_Copied_Timetable_By_Term($FromYearTermID, $ToYearTermID);
	$numOfToTimetable = count($ToTimetableIDArr);
}



### Start Data Validation
$FailedSubjectGroupIDArr = array();
$ErrorMsgArr = array();
$SuccessCount = 0;
$FailCount = 0;
foreach ($Copy_SubjectGroup_Arr as $thisSubjectGroupID => $thisIsCopySubjectGroup)
{
	$thisErrorMsgArr = array();
	$objSubjectGroup = new subject_term_class($thisSubjectGroupID, false, true);
	$thisSubjectID = $objSubjectGroup->SubjectID;
	
	$thisCopyStudent = $Student_SubjectGroup_Arr[$thisSubjectGroupID];
	$thisCopyTeacher = $Teacher_SubjectGroup_Arr[$thisSubjectGroupID];
	
	### Check if the subject group has been copied to that term before
	$HasSubjectGroupCopied = $libSCM->Has_SubjectGroup_Copied_To_YearTerm($thisSubjectGroupID, $ToYearTermID);
	if ($HasSubjectGroupCopied)
	{
		$thisErrorMsgArr[] = $Lang['SysMgr']['SubjectClassMapping']['WarningArr']['SubjectGroupCopiedAlready'];
	}
	
	
	### Validate Student
	if ($thisCopyStudent && count($objSubjectGroup->ClassStudentList) > 0)
	{
		$SubjectGroupStudentIDList = Get_Array_By_Key($objSubjectGroup->ClassStudentList, 'UserID');
		
		### Check if the students has joined another Subject Group of the same subject
		$tmpSubjectGroupArr = $libSCM->Student_Joined_Subject_Group_Of_Subject($thisSubjectID, $ToYearTermID, $SubjectGroupStudentIDList);
		
		if (count($tmpSubjectGroupArr) > 0)
		{
			$thisErrorMsgArr[] = $Lang['SysMgr']['SubjectClassMapping']['StudentHasJoinedOtherSubjectGroupOfTheSameSubject'];
		}
	}
	
	
	### Check Timetable (Do checking only if there are any timetables)
	if ($CopyTimetable && ($numOfFromTimetable > 0) && ($numOfToTimetable > 0))
	{
		# check the timetable one by one
		for ($i=0; $i<$numOfFromTimetable; $i++)
		{
			$thisFromTimetableID = $FromTimetableIDArr[$i];
			$ObjThisFromTimetable = new Timetable($thisFromTimetableID);
			$FromRoomAllocationArr = $ObjThisFromTimetable->Get_Room_Allocation_List('', '', array($thisSubjectGroupID), '');
			
			for ($j=0; $j<$numOfToTimetable; $j++)
			{
				$thisToTimetableID = $ToTimetableIDArr[$j];
				$ObjThisToTimetable = new Timetable($thisToTimetableID);
				
				# Get TimeSlot mapping
				$thisTimeSlotMappingArr = $ObjThisFromTimetable->Get_Copied_Timetable_TimeSlot_Mapping($thisToTimetableID);
				$numOfMapping = count($thisTimeSlotMappingArr);
				
				if ($numOfMapping > 0)
				{
					### Loop each room allocation to do checking
					$isLocationInUse = false;
					$isStudentTimeClash = false;
					foreach ((array)$FromRoomAllocationArr as $thisFromTimeSlotID => $thisFromTimeSlotInfoArr)
					{
						$thisToTimeSlotID = $thisTimeSlotMappingArr[$thisFromTimeSlotID];
						
						foreach ((array)$thisFromTimeSlotInfoArr as $thisDay => $thisFromDayInfoArr)
						{
							$numOfRoomAllocation = count((array)$thisFromDayInfoArr);
							for ($k=0; $k<$numOfRoomAllocation; $k++)
							{
								### 1. Check if there are any clashes in the location
								if ($isLocationInUse == false)	// need not do checking if found a location clash already
								{
									$thisLocationID = $thisFromDayInfoArr[$k]['LocationID'];
									$isRoomAvailable = $ObjThisToTimetable->Is_Available_Room($thisToTimeSlotID, $thisDay, $thisLocationID);
									
									if ($isRoomAvailable == false) {
										$isLocationInUse = true;
										if (!in_array($Lang['SysMgr']['SubjectClassMapping']['WarningArr']['TimetableLocationInUse'], $thisErrorMsgArr))
											$thisErrorMsgArr[] = $Lang['SysMgr']['SubjectClassMapping']['WarningArr']['TimetableLocationInUse'];
									}
								}
								
								if ($thisCopyStudent && $isStudentTimeClash == false)
								{
									### 2. Check the student has joined another subject group in the same day in the same time slot or not
									$hasClashes = $ObjThisToTimetable->Has_User_Joined_Others_Subject_Group($thisToTimeSlotID, $thisDay, $thisSubjectGroupID);
									
									if ($hasClashes == true)
									{
										$isLocationInUse = true;
										if (!in_array($Lang['SysMgr']['SubjectClassMapping']['WarningArr']['StudentTimeClash'], $thisErrorMsgArr))
											$thisErrorMsgArr[] = $Lang['SysMgr']['SubjectClassMapping']['WarningArr']['StudentTimeClash'];
									}
								}
							}
							
							if ($isLocationInUse == true && $isStudentTimeClash == true) {
								break;
							}	
						}
						
						if ($isLocationInUse == true && $isStudentTimeClash == true) {
							break;
						}
					}
				}	// End if ($numOfMapping > 0)
			}	// End for ($j=0; $j<$numOfToTimetable; $j++)
		}	// End for ($i=0; $i<$numOfFromTimetable; $i++)
	}
	
	if (count($thisErrorMsgArr) > 0)
	{
		$FailedSubjectGroupIDArr[] = $thisSubjectGroupID;
		$ErrorMsgArr[$thisSubjectID][$thisSubjectGroupID] = $thisErrorMsgArr;
		$FailCount++;
	}
	else
	{
		$SuccessCount++;
	}
}



### Build Fail Subject ( if there are any failed records)
$failTable = '';
if ($FailCount > 0)
{
	$SubjectGroupPrefix = '&nbsp;&nbsp;&nbsp;&nbsp;';
	
	### Get all Subject info
	$libSubject = new subject();
	$SubjectArr = $libSubject->Get_Subject_List($returnAsso=1, $withLearningCateory=0);
	$numOfSubject = count($SubjectArr);
	
	### Get Subject Group info
	$SubjectGroupInfoArr = $libSubject->Get_Subject_Group_List($FromYearTermID, $ClassLevelID='', $FailedSubjectGroupIDArr, $TeacherID='', $returnAsso=1);
		
	$failTable .= $linterface->GET_NAVIGATION2($Lang['SysMgr']['SubjectClassMapping']['FailureSubjectGroup']);
	$failTable .= '<table class="common_table_list" style="width:100%">'."\n";
		$failTable .= '<col style="width:38%;" />'."\n";
		$failTable .= '<col style="width:7%;" />'."\n";
		$failTable .= '<col style="width:7%;" />'."\n";
		$failTable .= '<col style="width:7%;" />'."\n";
		$failTable .= '<col style="width:7%;" />'."\n";
		$failTable .= '<col style="width:34%;" />'."\n";
		
		## Header
		$failTable .= '<tr>'."\n";
			$failTable .= '<th>'.$Lang['Header']['Menu']['Subject'].'</th>'."\n";
			
			# Copy
			$failTable .= '<th style="text-align:center">'."\n";
				$failTable .= $Lang['SysMgr']['SubjectClassMapping']['Copy'];
			$failTable .= '</th>'."\n";
			
			# Teacher
			$failTable .= '<th style="text-align:center">'."\n";
				$failTable .= $Lang['SysMgr']['SubjectClassMapping']['ClassTeacher'];
			$failTable .= '</th>'."\n";
			
			# Student
			$failTable .= '<th style="text-align:center">'."\n";
				$failTable .= $Lang['SysMgr']['SubjectClassMapping']['ClassStudent'];
			$failTable .= '</th>'."\n";
			
			# Leader
			$failTable .= '<th style="text-align:center" >'."\n";
				$failTable .= rtrim($Lang['Group']['SubjectLeader'],'.');
			$failTable .= '</th>'."\n";
			
			# Remark
			$failTable .= '<th style="text-align:center">'."\n";
				$failTable .= $Lang['General']['Remark'];
			$failTable .= '</th>'."\n";
			
		$failTable .= '</tr>'."\n";
		
		
		foreach ($ErrorMsgArr as $thisSubjectID => $thisSubjectGroupArr)
		{
			$thisSubjectID = $SubjectArr[$thisSubjectID]['RecordID'];
			$thisNameEn = $SubjectArr[$thisSubjectID]['EN_DES'];
			$thisNameCh = $SubjectArr[$thisSubjectID]['CH_DES'];
			$thisName = Get_Lang_Selection($thisNameCh, $thisNameEn);
			
			# Subject Name Row
			$failTable .= '<tr><td colspan="6">'.$thisName.'</td></tr>'."\n";
					
			foreach ($thisSubjectGroupArr as $thisSubjectGroupID => $thisErrorArr)
			{
				$thisSubjectGroupNameEn = $SubjectGroupInfoArr[$thisSubjectGroupID]['ClassTitleEN'];
				$thisSubjectGroupNameCh = $SubjectGroupInfoArr[$thisSubjectGroupID]['ClassTitleB5'];
				$thisSubjectGroupName = Get_Lang_Selection($thisSubjectGroupNameCh, $thisSubjectGroupNameEn);
				
				$failTable .= '<tr class="sub_row">'."\n";
					$failTable .= '<td>'.$SubjectGroupPrefix.$thisSubjectGroupName.'</td>'."\n";
					
					# Copy
					$thisDisplay = ($Copy_SubjectGroup_Arr[$thisSubjectGroupID] == 1)? $Lang['General']['Yes'] : $Lang['General']['No'];
					$failTable .= '<td style="text-align:center">'.$thisDisplay.'</td>'."\n";
					
					# Teacher
					$thisDisplay = ($Teacher_SubjectGroup_Arr[$thisSubjectGroupID] == 1)? $Lang['General']['Yes'] : $Lang['General']['No'];
					$failTable .= '<td style="text-align:center">'.$thisDisplay.'</td>'."\n";
					
					# Student
					$thisDisplay = ($Student_SubjectGroup_Arr[$thisSubjectGroupID] == 1)? $Lang['General']['Yes'] : $Lang['General']['No'];
					$failTable .= '<td style="text-align:center">'.$thisDisplay.'</td>'."\n";
					
					# Leader
					$thisDisplay = ($Leader_SubjectGroup_Arr[$thisSubjectGroupID] == 1)? $Lang['General']['Yes'] : $Lang['General']['No'];
					$failTable .= '<td style="text-align:center">'.$thisDisplay.'</td>'."\n";
					
					# Error Remark
					$thisDisplay = '- '.implode('<br />- ', $thisErrorArr);
					$failTable .= '<td style="text-align:left">'.$thisDisplay.'</td>'."\n";
					
				$failTable .= '</tr>'."\n";
			}
		}
	$failTable .= '</table>'."\n";
}

	
	
### Info Display Rows
$infoDisplayTable = '';
$infoDisplayTable .= '<table cellpadding="3" align="center" style="width:90%">'."\n";
	# From Year
	$infoDisplayTable .= '<tr>';
		$infoDisplayTable .= '<td class="formfieldtitle" width="30%" align="left">'.$Lang['SysMgr']['SubjectClassMapping']['CopyFromAcademicYear'].'</td>';
		$infoDisplayTable .= '<td class="tabletext" >'.$obj_FromAcademicYear->Get_Academic_Year_Name().'</td>';
	$infoDisplayTable .= '</tr>';
	# From Term
	$infoDisplayTable .= '<tr>';
		$infoDisplayTable .= '<td class="formfieldtitle" width="30%" align="left">'.$Lang['SysMgr']['SubjectClassMapping']['CopyFromTerm'].'</td>';
		$infoDisplayTable .= '<td class="tabletext" >'.$obj_FromYearTerm->Get_Year_Term_Name().'</td>';
	$infoDisplayTable .= '</tr>';
	
	# To Year
	$infoDisplayTable .= '<tr>';
		$infoDisplayTable .= '<td class="formfieldtitle" width="30%" align="left">'.$Lang['SysMgr']['SubjectClassMapping']['CopyToAcademicYear'].'</td>';
		$infoDisplayTable .= '<td class="tabletext" >'.$obj_ToAcademicYear->Get_Academic_Year_Name().'</td>';
	$infoDisplayTable .= '</tr>';
	# To Term
	$infoDisplayTable .= '<tr>';
		$infoDisplayTable .= '<td class="formfieldtitle" width="30%" align="left">'.$Lang['SysMgr']['SubjectClassMapping']['CopyToTerm'].'</td>';
		$infoDisplayTable .= '<td class="tabletext" >'.$obj_ToYearTerm->Get_Year_Term_Name().'</td>';
	$infoDisplayTable .= '</tr>';
	
	# Copy Timetable
	$thisDisplay = ($CopyTimetable == 1)? $Lang['General']['Yes'] : $Lang['General']['No'];
	$infoDisplayTable .= '<tr>';
		$infoDisplayTable .= '<td class="formfieldtitle" width="30%" align="left">'.$Lang['SysMgr']['SubjectClassMapping']['CopyTimetable'].'</td>';
		$infoDisplayTable .= '<td class="tabletext" >'.$thisDisplay.'</td>';
	$infoDisplayTable .= '</tr>';
	
	# Create eClass
	$thisDisplay = ($Create_eClass == 1)? $Lang['General']['Yes'] : $Lang['General']['No'];
	$infoDisplayTable .= '<tr>';
		$infoDisplayTable .= '<td class="formfieldtitle" width="30%" align="left">'.$Lang['SysMgr']['SubjectClassMapping']['CreateeClassIfSubjectGroupDoesNotHaveOne'].'</td>';
		$infoDisplayTable .= '<td class="tabletext" >'.$thisDisplay.'</td>';
	$infoDisplayTable .= '</tr>';
	
	# Success Count
	$infoDisplayTable .= '<tr>';
		$infoDisplayTable .= '<td class="formfieldtitle" width="30%" align="left">'. $Lang['SysMgr']['SubjectClassMapping']['SuccessCountSubjectGroup'] .'</td>';
		$infoDisplayTable .= '<td class="tabletext">'. $SuccessCount .'</td>';
	$infoDisplayTable .= '</tr>';
	
	# Failure Count
	$infoDisplayTable .= '<tr>';
		$infoDisplayTable .= '<td class="formfieldtitle" width="30%" align="left">'. $Lang['SysMgr']['SubjectClassMapping']['FailCountSubjectGroup'] .'</td>';
		$infoDisplayTable .= '<td class="tabletext">'. ($FailCount ? '<font color="red">':  '') . $FailCount . ($FailCount ? '</font>' : '') .'</td>';
	$infoDisplayTable .= '</tr>';
$infoDisplayTable .= '</table>'."\n";

?>
<br />
<form id="form1" name="form1" method="post" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr><td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td></tr>
	<tr><td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td></tr>
	
	<tr><td colspan="2"><?= $infoDisplayTable ?></td></tr>
	<tr><td colspan="2">
		<table width="90%" align="center" border="0" cellspacing="0" cellpadding="5">
			<tr><td><?= $failTable ?></td></tr>
		</table>
	</td></tr>
	
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<? if ($FailCount == 0) { ?>
				<?= $linterface->GET_ACTION_BTN($button_submit, "button", "jsSubmit();", "SubmitBtn") ?>
				&nbsp; 
			<? } ?>
			<?=$linterface->GET_ACTION_BTN($button_back, "button", "jsBack();")?>
		</td>
	</tr>
</table>

<input type="hidden" name="FromStep2" value="1">

<input type="hidden" name="ToAcademicYearID" value="<?=$ToAcademicYearID?>">
<input type="hidden" name="ToYearTermID" value="<?=$ToYearTermID?>">
<input type="hidden" name="FromAcademicYearID" value="<?=$FromAcademicYearID?>">
<input type="hidden" name="FromYearTermID" value="<?=$FromYearTermID?>">
<input type="hidden" name="ExpanedSubjectIDStr" value="<?=$ExpanedSubjectIDStr?>">
<input type="hidden" name="Global_Arr" value="<?=rawurlencode(serialize($Global_Arr))?>">
<input type="hidden" name="Copy_Subject_Arr" value="<?=rawurlencode(serialize($Copy_Subject_Arr))?>">
<input type="hidden" name="Teacher_Subject_Arr" value="<?=rawurlencode(serialize($Teacher_Subject_Arr))?>">
<input type="hidden" name="Student_Subject_Arr" value="<?=rawurlencode(serialize($Student_Subject_Arr))?>">
<input type="hidden" name="Copy_SubjectGroup_Arr" value="<?=rawurlencode(serialize($Copy_SubjectGroup_Arr))?>">
<input type="hidden" name="Teacher_SubjectGroup_Arr" value="<?=rawurlencode(serialize($Teacher_SubjectGroup_Arr))?>">
<input type="hidden" name="Student_SubjectGroup_Arr" value="<?=rawurlencode(serialize($Student_SubjectGroup_Arr))?>">
<input type="hidden" name="Leader_SubjectGroup_Arr" value="<?=rawurlencode(serialize($Leader_SubjectGroup_Arr))?>">

<input type="hidden" name="CopyTimetable" value="<?=$CopyTimetable?>">
<input type="hidden" name="Create_eClass" value="<?=$Create_eClass?>">

</form>
<br />

<script>
function jsBack()
{
	var formObj = document.getElementById('form1');
	formObj.action = 'copy_subject_group.php';
	formObj.submit();
}

function jsSubmit()
{
	js_Disable_Button('SubmitBtn');
	
	var formObj = document.getElementById('form1');
	formObj.action = 'copy_subject_group_update.php';
	formObj.submit();
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
