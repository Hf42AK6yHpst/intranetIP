<?php
# using: 

############# Change Log [Start] ################
#	Date:	2020-05-07 Tommy
#           - modified access checking, added $plugin["Disable_Subject"]
#
#   Date:   2019-01-14  Vito
#           Add focus to text input with id(SubjectCode_)
#
#	Date:	2015-09-04	Ivan
#			Used Get_SchoolSettings_Subject_Tab_Array() to get the tag array
#			ip.2.5.6.10.1	*** Need to upload with "/includes/subject_class_mapping_ui.php" before deployment *** 
#
#	Date:	2010-06-21	YatWoon
#			add online help button
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$SubjecClassUI = new subject_class_mapping_ui();
$linterface = new interface_html();
$CurrentPageArr['Subjects'] = 1;

### Title ###
//$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['Subject&ComponentTitle'],"index.php",1);
//$TAGS_OBJ[] = array($Lang['formClassMapping']['FormSubject'],"form_subject.php",0);
//$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'],"subject_group_mapping.php",0);
$TAGS_OBJ = $SubjecClassUI->Get_SchoolSettings_Subject_Tab_Array('subject');

# change page web title
$js = '<script type="text/JavaScript" language="JavaScript">'."\n";
$js.= 'document.title="eClass Subject";'."\n";
$js.= '</script>'."\n";

$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'].$js;

# online help button
//$onlineHelpBtn = gen_online_help_btn_and_layer('school_settings','subject');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

$linterface->LAYOUT_START(); 

$include_JS_CSS = $SubjecClassUI->Include_JS_CSS();


## Toolbar and Serach Box
$toolbarDiv = '';
$toolbarDiv .= '<div class="content_top_tool">'."\n";
	$toolbarDiv .= '<div class="Conntent_tool">'."\n";
		# Import Subject
		$toolbarDiv .= '<div style="float:left">'."\n";
			$toolbarDiv .= $linterface->GET_LNK_IMPORT("javascript:js_GoImport_Subject()", $Lang['SysMgr']['SubjectClassMapping']['ImportSubject']);
		$toolbarDiv .= '</div>'."\n";
		# Import Subject Component
		$toolbarDiv .= '<div style="float:left">'."\n";
			$toolbarDiv .= $linterface->GET_LNK_IMPORT("javascript:js_GoImport_SubjectComponent()", $Lang['SysMgr']['SubjectClassMapping']['ImportSubjectComponent']);
		$toolbarDiv .= '</div>'."\n";
		$toolbarDiv .= '<br style="clear:both" />'."\n";
		# Export Subject
		$toolbarDiv .= '<div style="float:left">'."\n";
			$toolbarDiv .= $linterface->GET_LNK_EXPORT("javascript:js_GoExport_Subject()", $Lang['SysMgr']['SubjectClassMapping']['ExportSubject']);
		$toolbarDiv .= '</div>'."\n";
		# Export Subject Component
		$toolbarDiv .= '<div style="float:left">'."\n";
			$toolbarDiv .= $linterface->GET_LNK_EXPORT("javascript:js_GoExport_SubjectComponent()", $Lang['SysMgr']['SubjectClassMapping']['ExportSubjectComponent']);
		$toolbarDiv .= '</div>'."\n";
	$toolbarDiv .= '</div>'."\n";
	$toolbarDiv .= '<br style="clear:both" />'."\n";
$toolbarDiv .= '</div>'."\n";


## Content Table
$SubjectContentDiv = '';
$SubjectContentDiv .= '<div id="ContentTableDiv">'."\n";
	$SubjectContentDiv .= $SubjecClassUI->Get_Subject_Component_Setting_Table();
$SubjectContentDiv .= '</div>';


## Add/Edit Learning Category Form Create & Reset Button ##
$btnAddLC = str_replace("'","\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Add'], "button", "js_Insert_Learning_Category();", "NewLCSubmitBtn"));
$btnCancelLC = str_replace("'","\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "js_Clear_Temp_Learning_Category_Info();", "NewLCCancelBtn"));



?>

<div id="IndexDebugArea"></div>

<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="main_content">
			<?=$include_JS_CSS?>
			<?= $toolbarDiv ?>
			<?= $SubjectContentDiv ?>
		</td>
	</tr>
</table>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->
<?
$linterface->LAYOUT_STOP();

intranet_closedb();
?>

<script>
var curSubjectID = "";
var jsShowHideSpeed = "fast";

/************************* Learning Categtory Related Functions *************************/
// Show Add/Edit Learning Category Layer
function js_Show_Learning_Category_Add_Edit_Layer() {
	Hide_Return_Message();
	
	$.post(
		"ajax_get_learning_category_layer.php", 
		{ },
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);
			js_Init_JEdit();
			js_Init_DND_Table();
			
			//$('#debugArea').html(ReturnData);
		}
	);
}

function js_Add_Learning_Category_Row() {
	if (!document.getElementById('GenAddLearningCategoryRow')) {
		var TableBody = document.getElementById("LearningCategoryInfoTable").tBodies[0];
		var RowIndex = document.getElementById("AddLeraningCategoryRow").rowIndex - 1;
		//var RowIndex = 1;
		var NewRow = TableBody.insertRow(RowIndex);
		NewRow.id = "GenAddLearningCategoryRow";
		
		// Title (Eng)
		var NewCell0 = NewRow.insertCell(0);
		var tempInput = '<input type="text" name="LearningCategoryCode" id="LearningCategoryCode" value="" class="textbox" maxlength="10" />';
			tempInput += '<div id="Code_WarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell0.innerHTML = tempInput;
		
		// Title (Eng)
		var NewCell0 = NewRow.insertCell(1);
		var tempInput = '<input type="text" name="LearningCategoryTitleEn" id="LearningCategoryTitleEn" value="" class="textbox"/>';
			tempInput += '<div id="TitleEn_WarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell0.innerHTML = tempInput;
		
		// Title (Chi)
		var NewCell1 = NewRow.insertCell(2);
		var tempInput = '<input type="text" name="LearningCategoryTitleCh" id="LearningCategoryTitleCh" value="" class="textbox"/>';
			tempInput += '<div id="TitleCh_WarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell1.innerHTML = tempInput;
		
		// Add and Reset Buttons
		var NewCell2 = NewRow.insertCell(3);
		var temp = '<?=$btnAddLC?>';
		temp += '&nbsp;';
		temp += '<?=$btnCancelLC?>';
		NewCell2.innerHTML = temp;
		
		$('#LearningCategoryCode').focus();
		
		// LearningCategoryTitleEn validation
		js_Add_KeyUp_LC_Code_Checking("LC_Code", "LearningCategoryCode", "Code_WarningDiv_New");
		js_Add_KeyUp_Title_Checking("LC_TitleEn", "LearningCategoryTitleEn", "TitleEn_WarningDiv_New");
		js_Add_KeyUp_Title_Checking("LC_TitleCh", "LearningCategoryTitleCh", "TitleCh_WarningDiv_New");
	}
}

function js_Clear_Temp_Learning_Category_Info() {
	//$('#LearningCategoryTitleEn').val('');
	//$('#LearningCategoryTitleCh').val('');
	
	// Delete the Row
	var TableBody = document.getElementById("LearningCategoryInfoTable").tBodies[0];
	var RowIndex = document.getElementById("GenAddLearningCategoryRow").rowIndex-1;
	TableBody.deleteRow(RowIndex);
}
function js_Insert_Learning_Category() {
	var Code = encodeURIComponent(Trim($('#LearningCategoryCode').val()));
	var TitleEn = encodeURIComponent(Trim($('#LearningCategoryTitleEn').val()));
	var TitleCh = encodeURIComponent(Trim($('#LearningCategoryTitleCh').val()));
	
	if (js_Is_Input_Blank('LearningCategoryCode', 'Code_WarningDiv_New', '<?=$Lang['SysMgr']['SubjectClassMapping']['BlankCodeWarning']?>'))
	{
		$('#LearningCategoryCode').focus();
		return false;
	}
	if (js_Is_Input_Blank('LearningCategoryTitleEn', 'TitleEn_WarningDiv_New', '<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['Title']?>'))
	{
		$('#LearningCategoryTitleEn').focus();
		return false;
	}
	if (js_Is_Input_Blank('LearningCategoryTitleCh', 'TitleCh_WarningDiv_New', '<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['Title']?>'))
	{
		$('#LearningCategoryTitleCh').focus();
		return false;
	}
	/*
	if (!check_text(document.getElementById('LearningCategoryTitleEn'), '<?=$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterTitleEn']?>'))
		return false;
	if (!check_text(document.getElementById('LearningCategoryTitleCh'), '<?=$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterTitleCh']?>'))
		return false;
	*/
	
	
	$.post(
		"ajax_validate_learning_category_code.php", 
		{
			InputValue: Code
		},
		function(ReturnData)
		{
			if (ReturnData == "")
			{
				// valid code -> check title
				$.post(
					"ajax_validate_learning_category_title.php", 
					{
						RecordType: "Both",
						InputValueEn: $('#LearningCategoryTitleEn').val(),
						InputValueCh: $('#LearningCategoryTitleCh').val(),
						SelfID: ""
					},
					function(ReturnData)
					{
						if (ReturnData == "")
						{
							// valid code => insert
							
							// disable submit button and then add building
							$('#NewLCSubmitBtn').attr("disabled", "true");
							$('#NewLCCancelBtn').attr("disabled", "true");
							
							$.post(
								"ajax_new_learning_category.php", 
								{ 
									TitleEn: TitleEn,
									TitleCh: TitleCh,
									Code: Code
								},
								function(ReturnData)
								{
									js_Reload_Learning_Category_Add_Edit_Table();
									js_Reload_Content_Table(curSubjectID);
									
									// Show system messages
									js_Show_System_Message("LC", "new", ReturnData);
									
									//$('#debugArea').html(ReturnData);
								}
							);
						}
					}
				);
			}
			else
			{
				$('#LearningCategoryCode').focus();
			}
		}
	);
}


function js_Update_Learning_Category(DivID, DBFieldName, UpdateValue)
{
	$.post(
		"ajax_update_learning_category.php", 
		{ 
			DivID: encodeURIComponent(DivID),
			DBFieldName: encodeURIComponent(DBFieldName),
			UpdateValue: encodeURIComponent(UpdateValue)
		},
		function(ReturnData)
		{
			js_Reload_Learning_Category_Add_Edit_Table();
			js_Reload_Content_Table(curSubjectID);
			
			// Show system messages
			js_Show_System_Message("LC", "update", ReturnData);
				
			//$('#debugArea').html(ReturnData);
		}
	);
}
function js_Delete_Learning_Category(LearningCategoryID)
{
	if (confirm('<?=$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['ConfirmDeleteLearningCategory']?>'))
	{
		$.post(
			"ajax_delete_learning_category.php", 
			{ 
				LearningCategoryID: LearningCategoryID
			},
			function(ReturnData)
			{
				js_Reload_Learning_Category_Add_Edit_Table();
				js_Reload_Content_Table(curSubjectID);
				
				// Show system messages
				js_Show_System_Message("LC", "delete", ReturnData);
			
				//$('#debugArea').html(ReturnData);
			}
		);
	}
	
	return false;
}

/************************* End of Learning Categtory Related Functions *************************/



/************************* Subject Related Functions *************************/

// Show Add/Edit Room Layer
function js_Show_Subject_Add_Edit_Layer(SubjectID, LearningCategoryID, isComponent, ParentSubjectID) {
	Hide_Return_Message();
	
	$.post(
		"ajax_get_subject_add_edit_layer.php", 
		{ 
			SubjectID: SubjectID,
			LearningCategoryID: LearningCategoryID,
			isComponent: isComponent,
			ParentSubjectID: ParentSubjectID
		},
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);
			if (isComponent==1)
			{
				js_Add_KeyUp_Code_Checking("SubjectComponent", "SubjectCode_" + SubjectID, "CodeWarningDiv", ParentSubjectID);
				if (SubjectID != '')
					js_Code_Validation("SubjectComponent", $('#SubjectCode_' + SubjectID).val(), SubjectID, "CodeWarningDiv", ParentSubjectID);
			}
			else
			{
				js_Add_KeyUp_Code_Checking("Subject", "SubjectCode_" + SubjectID, "CodeWarningDiv", "");
				if (SubjectID != '')
					js_Code_Validation("Subject", $('#SubjectCode_' + SubjectID).val(), SubjectID, "CodeWarningDiv", "");
			}
			
			js_Add_KeyUp_Blank_Checking('Subject_TitleEn', 'TitleEnWarningDiv', '<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['TitleEn']?>');
			js_Add_KeyUp_Blank_Checking('Subject_TitleCh', 'TitleChWarningDiv', '<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['TitleCh']?>');
			js_Add_KeyUp_Blank_Checking('Subject_AbbrEn', 'AbbrEnWarningDiv', '<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['AbbrEn']?>');
			js_Add_KeyUp_Blank_Checking('Subject_AbbrCh', 'AbbrChWarningDiv', '<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['AbbrCh']?>');
			js_Add_KeyUp_Blank_Checking('Subject_ShortFormEn', 'ShortFormEnWarningDiv', '<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['ShortFormEn']?>');
			js_Add_KeyUp_Blank_Checking('Subject_ShortFormCh', 'ShortFormChWarningDiv', '<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['ShortFormCh']?>');
			
			if (LearningCategoryID != '')
				$('#SubjectCode_' + SubjectID).focus();
			else
				$('#LC_Selected').focus();

			if (isComponent == 1)
				$('#SubjectCode_').focus();
			//$('#debugArea').html(ReturnData);
		}
	);
}
function Insert_Edit_Subject(SubjectID, KeepAdding, isComponent, ParentSubjectID)
{
	var SelectedLearningCategoryID = $("#LC_Selected option:selected").val();
	var Code = encodeURIComponent(Trim($('#SubjectCode_' + SubjectID).val()));
	var TitleEn = encodeURIComponent(Trim($('#Subject_TitleEn').val()));
	var TitleCh = encodeURIComponent(Trim($('#Subject_TitleCh').val()));
	var AbbrEn = encodeURIComponent(Trim($('#Subject_AbbrEn').val()));
	var AbbrCh = encodeURIComponent(Trim($('#Subject_AbbrCh').val()));
	var ShortFormEn = encodeURIComponent(Trim($('#Subject_ShortFormEn').val()));
	var ShortFormCh = encodeURIComponent(Trim($('#Subject_ShortFormCh').val()));
	
	if (js_Is_Input_Blank('LC_Selected', 'LC_WarningDiv', '<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['NoSelected']['LearningCategory']?>'))
	{
		$('#LC_Selected').focus();
		return false;
	}
	if (js_Is_Input_Blank('SubjectCode_' + SubjectID, 'CodeWarningDiv', '<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['WebSAMSCode']?>'))
	{
		$('#SubjectCode_' + SubjectID).focus();
		return false;
	}
	/*
	if (isNaN(Code))
	{
		$('#CodeWarningDiv').html("<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['MustBeInteger']['WebSAMSCode']?>");
		$('#CodeWarningDiv').show(jsShowHideSpeed);
		$('#SubjectCode_' + SubjectID).focus();
		return false;
	}
	*/
	if (js_Is_Input_Blank('Subject_TitleEn', 'TitleEnWarningDiv', '<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['TitleEn']?>'))
	{
		$('#Subject_TitleEn').focus();
		return false;
	}
	if (js_Is_Input_Blank('Subject_TitleCh', 'TitleChWarningDiv', '<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['TitleCh']?>'))
	{
		$('#Subject_TitleCh').focus();
		return false;
	}
	if (js_Is_Input_Blank('Subject_AbbrEn', 'AbbrEnWarningDiv', '<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['AbbrEn']?>'))
	{
		$('#Subject_AbbrEn').focus();
		return false;
	}
	if (js_Is_Input_Blank('Subject_AbbrCh', 'AbbrChWarningDiv', '<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['AbbrCh']?>'))
	{
		$('#Subject_AbbrCh').focus();
		return false;
	}
	if (js_Is_Input_Blank('Subject_ShortFormEn', 'ShortFormEnWarningDiv', '<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['ShortFormEn']?>'))
	{
		$('#Subject_ShortFormEn').focus();
		return false;
	}
	if (js_Is_Input_Blank('Subject_ShortFormCh', 'ShortFormChWarningDiv', '<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['ShortFormCh']?>'))
	{
		$('#Subject_ShortFormCh').focus();
		return false;
	}
	
	/*
	if (SelectedLearningCategoryID == '')
	{
		alert('<?=$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseSelectLearningCategory']?>');
		return false;
	}
	if (!check_text(document.getElementById('SubjectCode_' + SubjectID), '<?=$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterWebSAMSCode']?>'))
		return false;
	if (!check_text(document.getElementById('Subject_TitleEn'), '<?=$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterTitleEn']?>'))
		return false;
	if (!check_text(document.getElementById('Subject_TitleCh'), '<?=$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterTitleCh']?>'))
		return false;
	if (!check_text(document.getElementById('Subject_AbbrEn'), '<?=$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterAbbrEn']?>'))
		return false;
	if (!check_text(document.getElementById('Subject_AbbrCh'), '<?=$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterAbbrCh']?>'))
		return false;
	if (!check_text(document.getElementById('Subject_ShortFormEn'), '<?=$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterShortFormEn']?>'))
		return false;
	if (!check_text(document.getElementById('Subject_ShortFormCh'), '<?=$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterShortFormCh']?>'))
		return false;
	*/
	
	var RecordType = "";
	RecordType = (isComponent==1)? "SubjectComponent" : "Subject";
		
	$.post(
		"ajax_validate_subject_code.php", 
		{
			RecordType: RecordType,
			InputValue: $('#SubjectCode_' + SubjectID).val(),
			TargetID: SubjectID,
			isComponent: isComponent,
			ParentSubjectID: ParentSubjectID
		},
		function(ReturnData)
		{
			if (ReturnData == "")
			{
				// valid code
				$('#CodeWarningDiv').hide(jsShowHideSpeed);
				
				Block_Thickbox();
				
				// Add or Edit
				var action = '';
				var actionScript = '';
				if (SubjectID == '')
				{
					actionScript = 'ajax_new_subject.php';
					action = "new";
				}
				else
				{
					actionScript = 'ajax_update_subject.php';
					action = "update";
				}
				
				$.post(
					actionScript, 
					{ 
						SubjectID: SubjectID,
						LearningCategoryID: SelectedLearningCategoryID,
						Code: Code,
						TitleEn: TitleEn,
						TitleCh: TitleCh,
						AbbrEn: AbbrEn,
						AbbrCh: AbbrCh,
						ShortFormEn: ShortFormEn,
						ShortFormCh: ShortFormCh,
						isComponent: isComponent,
						ParentSubjectID: ParentSubjectID
					 },
					function(ReturnData)
					{
						js_Show_Subject_Add_Edit_Layer('', SelectedLearningCategoryID, isComponent, ParentSubjectID);
						js_Reload_Content_Table(curSubjectID);
						
						// Show system messages
						js_Show_System_Message(RecordType, action, ReturnData);
						
						UnBlock_Thickbox();
						
						if (KeepAdding != 1)
							js_Hide_ThickBox();
						
						//$('#debugArea').html('aaa ' + ReturnData);
					}
				);
				
			}
			else
			{
				// invalid code => show warning
				$('#CodeWarningDiv').html(ReturnData);
				$('#CodeWarningDiv').show(jsShowHideSpeed);
				$('#SubjectCode_' + SubjectID).focus();
				
				//$('#debugArea').html('aaa ' + ReturnData);
			}
		}
	);
}

function js_Delete_Subject(SubjectID, isComponent)
{
	var jsWarning = '';
	if (isComponent == 1)
		jsWarning = '<?=$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['ConfirmDeleteSubjectComponent']?>';
	else
		jsWarning = '<?=$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['ConfirmDeleteSubject']?>';
		
	if (confirm(jsWarning))
	{
		var RecordType = "";
		RecordType = (isComponent==1)? "SubjectComponent" : "Subject";
		
		$.post(
			"ajax_delete_subject.php", 
			{ 
				SubjectID: SubjectID,
				isComponent: isComponent
			},
			function(ReturnData)
			{
				js_Reload_Content_Table(curSubjectID);
				
				// Show system messages
				js_Show_System_Message(RecordType, "delete", ReturnData);
				
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
	
	return false;
}

/************************* End of Subject Related Functions *************************/





/************************* Reload UI Functions *************************/
// Reload Content Table
function js_Reload_Content_Table(ParentSubjectID)
{
	if (ParentSubjectID == null)
		ParentSubjectID = '';
		
	curSubjectID = ParentSubjectID;
		
	$('#ContentTableDiv').load(
		"ajax_reload_content.php",
		{ 
			RecordType: "ContentTable",
			ParentSubjectID: ParentSubjectID 
		},
		function(ReturnData)
		{
			initThickBox();
			js_Init_JEdit();
			js_Init_DND_Table();
		}
	);
}

// Reload Add/Edit Learning Category Table
function js_Reload_Learning_Category_Add_Edit_Table() {
	$.post(
		"ajax_reload_content.php", 
		{ RecordType: "LearningCategoryTable" },
		function(ReturnData)
		{
			$('#LearningCategoryInfoSettingLayer').html(ReturnData);
			js_Init_JEdit();
			js_Init_DND_Table();
			
			//$('#debugArea').html(ReturnData);
		}
	);
}
/************************* End of Reload UI Functions *************************/



/************************* Initialization Functions *************************/
// jquery tablednd function
// form drag and drop
function js_Init_DND_Table() {
	
	var JQueryObj1 = $("#LearningCategoryInfoTable");
	JQueryObj1.tableDnD({
		onDrop: function(table, row) {
			var rows = table.tBodies[0].rows;
			var RecordOrder = "";
			for (var i=0; i<rows.length; i++) {
				if (rows[i].id != "")
				RecordOrder += rows[i].id+",";
			}
			//alert("RecordOrder = " + RecordOrder);
			
			
			$.post(
				"ajax_reorder_display_order.php", 
				{ 
					RecordType: "LearningCategory",
					DisplayOrderString: RecordOrder
				},
				function(ReturnData)
				{
					// Reload the building info in the thickbox
					js_Reload_Learning_Category_Add_Edit_Table();
					js_Reload_Content_Table(curSubjectID);
					
					// Show system messages
					//js_Show_System_Message("LC", "update", ReturnData);
					
					//$('#debugArea').html(ReturnData);
				}
			);
			
		},
		onDragStart: function(table, row) {
			//$('#debugArea').html("Started dragging row "+row.id);
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
	
	var JQueryObj2 = $("#SubjectContentTable");
	JQueryObj2.tableDnD({
		onDrop: function(table, row) {
			var wholeTable = document.getElementById("SubjectContentTable");
			var rows = wholeTable.tBodies[0].rows;
			var RecordOrder = "";
			
			for (var i=0; i<wholeTable.tBodies.length; i++) {
				var thisRow = wholeTable.tBodies[i].rows;
				for (var j=0; j<thisRow.length; j++) {
					RecordOrder += thisRow[j].id+",";
				}
			}
			//alert("RecordOrder = " + RecordOrder);
			
			$.post(
				"ajax_reorder_display_order.php", 
				{ 
					RecordType: "Subject",
					DisplayOrderString: RecordOrder
				},
				function(ReturnData)
				{
					// Reload the building info in the thickbox
					js_Reload_Content_Table(curSubjectID);
					
					// Show system messages
					//js_Show_System_Message("LC", "update", ReturnData);
					
					//$('#IndexDebugArea').html(ReturnData);
				}
			);
		},
		onDragStart: function(table, row) {
			//$('#debugArea').html("Started dragging row "+row.id);
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
	
	var JQueryObj3 = $("#SubjectComponentContentTable");
	JQueryObj3.tableDnD({
		onDrop: function(table, row) {
			var rows = table.tBodies[0].rows;
			var RecordOrder = "";
			for (var i=0; i<rows.length; i++) {
				if (rows[i].id != "")
				RecordOrder += rows[i].id+",";
			}
			//alert("RecordOrder = " + RecordOrder);
			
			$.post(
				"ajax_reorder_display_order.php", 
				{ 
					RecordType: "SubjectComponent",
					DisplayOrderString: RecordOrder
				},
				function(ReturnData)
				{
					// Reload the building info in the thickbox
					js_Reload_Content_Table(curSubjectID);
					
					// Show system messages
					//js_Show_System_Message("LC", "update", ReturnData);
					
					//$('#IndexDebugArea').html(ReturnData);
				}
			);
		},
		onDragStart: function(table, row) {
			//$('#debugArea').html("Started dragging row "+row.id);
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}

function js_Init_JEdit()
{
	/************ Add/Edit Building Info ************/
	// TitleEn
	$('div.jEditTitleEn').editable( 
		function(updateValue, settings) 
		{
			var thisDivID = $(this).attr("id");
			var idArr = thisDivID.split('_');
			var divID = idArr[0];
			var targetID = idArr[1];
			
			updateValue = Trim(updateValue);
			
			if ($('#TitleEn_WarningDiv_' + targetID).is(':visible') || updateValue=='' || $(this)[0].revert == updateValue)
			{
				// not vaild
				$('#TitleEn_WarningDiv_' + targetID).hide(jsShowHideSpeed);
				$(this)[0].reset();
			}
			else
			{
				js_Update_Learning_Category(thisDivID, "NameEng", updateValue);
			}
		},
		{
			tooltip   : "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",     
			style  : "display:inline",
			height: "20px"
		}
	); 
	// TitleEn Validation
	$('div.jEditTitleEn').keyup( function() {
		var inputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var selfID = idArr[1];
		var recordType = '';
		
		var WarningDivID = 'TitleEn_WarningDiv_' + selfID;
		if (Trim(inputValue) == '')
		{
			$('#' + WarningDivID).html('<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleEn']?>');
			$('#' + WarningDivID).show(jsShowHideSpeed);
		}
		else
		{
			$('#' + WarningDivID).hide(jsShowHideSpeed);
			js_Title_Validation("LC_TitleEn", inputValue, selfID, WarningDivID);
		}
	});
	$('div.jEditTitleEn').click( function() {
		var inputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var selfID = idArr[1];
		var recordType = '';
			
		js_Title_Validation("LC_TitleEn", inputValue, selfID, 'TitleEn_WarningDiv_'+selfID);
	});
	
	
	// TitleCh
	$('div.jEditTitleCh').editable( 
		function(updateValue, settings) 
		{
			var thisDivID = $(this).attr("id");
			var idArr = thisDivID.split('_');
			var divID = idArr[0];
			var targetID = idArr[1];
			
			updateValue = Trim(updateValue);
			if ($('#TitleCh_WarningDiv_' + targetID).is(':visible') || updateValue == '' || $(this)[0].revert == updateValue)
			{
				// not vaild
				$('#TitleCh_WarningDiv_' + targetID).hide(jsShowHideSpeed);
				$(this)[0].reset();
			}
			else
			{
				js_Update_Learning_Category(thisDivID, "NameChi", updateValue);
			}
		}, 
		{
			tooltip   : "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",     
			style  : "display:inline",
			height: "20px"
		}
	); 
	// TitleCh Validation
	$('div.jEditTitleCh').keyup( function() {
		var inputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var selfID = idArr[1];
		var recordType = '';
			
		var WarningDivID = 'TitleCh_WarningDiv_' + selfID;
		if (Trim(inputValue) == '')
		{
			$('#' + WarningDivID).html('<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleCh']?>');
			$('#' + WarningDivID).show(jsShowHideSpeed);
		}
		else
		{
			$('#' + WarningDivID).hide(jsShowHideSpeed);
			js_Title_Validation("LC_TitleCh", inputValue, selfID, WarningDivID);
		}
		
	});
	$('div.jEditTitleCh').click( function() {
		var inputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var selfID = idArr[1];
		var recordType = '';
			
		js_Title_Validation("LC_TitleCh", inputValue, selfID, 'TitleCh_WarningDiv_'+selfID);
	});
	
	
	// Code
	$('div.jEditCode').editable( 
		function(updateValue, settings) 
		{
			var thisDivID = $(this).attr("id");
			var idArr = thisDivID.split('_');
			var divID = idArr[0];
			var targetID = idArr[1];
			
			updateValue = Trim(updateValue);
			if ($('#Code_WarningDiv_' + targetID).is(':visible') || updateValue == '' || $(this)[0].revert == updateValue)
			{
				// not vaild
				$('#Code_WarningDiv_' + targetID).hide(jsShowHideSpeed);
				$(this)[0].reset();
			}
			else
			{
				js_Update_Learning_Category(thisDivID, "Code", updateValue);
			}
		}, 
		{
			tooltip   : "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",     
			style  : "display:inline",
			height: "20px",
			maxlength: 10
		}
	); 
	// TitleCh Validation
	$('div.jEditCode').keyup( function() {
		var inputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var selfID = idArr[1];
		var recordType = '';
			
		var WarningDivID = 'Code_WarningDiv_' + selfID;
		if (Trim(inputValue) == '')
		{
			$('#' + WarningDivID).html('<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleCh']?>');
			$('#' + WarningDivID).show(jsShowHideSpeed);
		}
		else
		{
			$('#' + WarningDivID).hide(jsShowHideSpeed);
			js_LC_Code_Validation("LC_Code", inputValue, selfID, WarningDivID);
		}
		
	});
	$('div.jEditCode').click( function() {
		var inputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var selfID = idArr[1];
		var recordType = '';
			
		js_LC_Code_Validation("LC_Code", inputValue, selfID, 'Code_WarningDiv_'+selfID);
	});
	
	
	/************ End of Add/Edit Building Info ************/
}
/************************* End of Initialization Functions *************************/



/************************* Code Validation *************************/
function js_Code_Validation(RecordType, InputValue, TargetID, WarningDivID, ParentSubjectID)
{
	if (Trim(InputValue) == '')
	{
		$('#' + WarningDivID).html("<?=$Lang['SysMgr']['SubjectClassMapping']['BlankCodeWarning']?>");
		$('#' + WarningDivID).show(jsShowHideSpeed);
	}
	/*
	else if (isNaN(InputValue))
	{
		// check if the code is numeric
		$('#' + WarningDivID).html("<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['MustBeInteger']['WebSAMSCode']?>");
		$('#' + WarningDivID).show(jsShowHideSpeed);
	}
	*/
	else
	{
		// check code duplication
		$.post(
			"ajax_validate_subject_code.php", 
			{
				RecordType: RecordType,
				InputValue: InputValue,
				TargetID: TargetID,
				ParentSubjectID: ParentSubjectID
			},
			function(ReturnData)
			{
				if (ReturnData == "")
				{
					// valid code
					$('#' + WarningDivID).hide(jsShowHideSpeed);
					
					//$('#debugArea').html('aaa ' + ReturnData);
				}
				else
				{
					// invalid code => show warning
					$('#' + WarningDivID).html(ReturnData);
					$('#' + WarningDivID).show(jsShowHideSpeed);
					
					//$('#debugArea').html('bbb ' + ReturnData);
				}
			}
		);
	}
}
function js_Add_KeyUp_Code_Checking(RecordType, ListenerObjectID, WarningDivID, ParentSubjectID)
{
	// Code Validation
	$('#' + ListenerObjectID).keyup( function() {
		var inputValue = $(this).val();
		var idArr = $(this).attr('id').split('_');
		var targetID = idArr[1];
		
		if (targetID == null || isNaN(targetID))
			targetID = '';
			
		if (ParentSubjectID == null || isNaN(ParentSubjectID))
			ParentSubjectID = '';
			
		js_Code_Validation(RecordType, inputValue, targetID, WarningDivID, ParentSubjectID);
	});
}



function js_Add_KeyUp_Title_Checking(RecordType, ListenerObjectID, WarningDivID)
{
	// Code Validation
	$('#' + ListenerObjectID).keyup( function() {
		var inputValue = $(this).val();
		var idArr = $(this).attr('id').split('_');
		var selfID = idArr[1];
		
		if (selfID == null)
			selfID = '';
			
		js_Title_Validation(RecordType, inputValue, selfID, WarningDivID);
	});
}
function js_Title_Validation(RecordType, InputValue, SelfID, WarningDivID)
{
	if (Trim(InputValue) == '')
	{
		$('#' + WarningDivID).html('<?=$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['Title']?>');
		$('#' + WarningDivID).show(jsShowHideSpeed);
	}
	else
	{
		$.post(
			"ajax_validate_learning_category_title.php", 
			{
				RecordType: RecordType,
				InputValue: InputValue,
				SelfID: SelfID
			},
			function(ReturnData)
			{
				if (ReturnData == "")
				{
					// valid code
					$('#' + WarningDivID).hide(jsShowHideSpeed);
				}
				else
				{
					// invalid code => show warning
					$('#' + WarningDivID).html(ReturnData);
					$('#' + WarningDivID).show(jsShowHideSpeed);
					
					//$('#debugArea').html('aaa ' + ReturnData);
				}
			}
		);
	}
}


function js_Add_KeyUp_LC_Code_Checking(RecordType, ListenerObjectID, WarningDivID)
{
	// Code Validation
	$('#' + ListenerObjectID).keyup( function() {
		var inputValue = $(this).val();
		var idArr = $(this).attr('id').split('_');
		var selfID = idArr[1];
		
		if (selfID == null)
			selfID = '';
			
		js_LC_Code_Validation(RecordType, inputValue, selfID, WarningDivID);
	});
}
function js_LC_Code_Validation(RecordType, InputValue, SelfID, WarningDivID)
{
	if (Trim(InputValue) == '')
	{
		$('#' + WarningDivID).html('<?=$Lang['SysMgr']['SubjectClassMapping']['BlankCodeWarning']?>');
		$('#' + WarningDivID).show(jsShowHideSpeed);
	}
	else
	{
		$.post(
			"ajax_validate_learning_category_code.php", 
			{
				InputValue: InputValue,
				SelfID: SelfID
			},
			function(ReturnData)
			{
				if (ReturnData == "")
				{
					// valid code
					$('#' + WarningDivID).hide(jsShowHideSpeed);
				}
				else
				{
					// invalid code => show warning
					$('#' + WarningDivID).html(ReturnData);
					$('#' + WarningDivID).show(jsShowHideSpeed);
					
					//$('#debugArea').html('aaa ' + ReturnData);
				}
			}
		);
	}
}

/************************* End of Code Validation *************************/


// Load system message
function js_Show_System_Message(RecordType, Action, Status)
{
	var returnMessage = '';
	
	if (RecordType == "LC")
	{
		switch(Action)
		{
			case "new":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Success']['LearningCategory']?>' : '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Failed']['LearningCategory']?>';
				break;
			case "update":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Success']['LearningCategory']?>' : '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Failed']['LearningCategory']?>';
				break;
			case "delete":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Success']['LearningCategory']?>' : '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Failed']['LearningCategory']?>';
				break;
		}
	}
	else if (RecordType == "Subject")
	{
		switch(Action)
		{
			case "new":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Success']['Subject']?>' : '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Failed']['Subject']?>';
				break;
			case "update":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Success']['Subject']?>' : '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Failed']['Subject']?>';
				break;
			case "delete":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Success']['Subject']?>' : '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Failed']['Subject']?>';
				break;
		}
	}
	else if (RecordType == "SubjectComponent")
	{
		switch(Action)
		{
			case "new":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Success']['SubjectComponent']?>' : '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Failed']['SubjectComponent']?>';
				break;
			case "update":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Success']['SubjectComponent']?>' : '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Failed']['SubjectComponent']?>';
				break;
			case "delete":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Success']['SubjectComponent']?>' : '<?=$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Failed']['SubjectComponent']?>';
				break;
		}
	}
	
	Get_Return_Message(returnMessage);
}

function js_GoImport_Subject()
{
	location.href = "import_subject.php";
}

function js_GoImport_SubjectComponent()
{
	location.href = "import_subject_component.php";
}

function js_GoExport_Subject()
{
	location.href = "export_subject.php";
}

function js_GoExport_SubjectComponent()
{
	location.href = "export_subject_component.php";
}



js_Init_DND_Table();
</script>