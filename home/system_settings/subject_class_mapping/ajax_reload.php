<?php
// using
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);

$returnString = '';
if ($RecordType == "Reload_Term_Selection")
{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	$libsubject_ui = new subject_class_mapping_ui();
	
	$AcademicYearID = stripslashes($_POST['AcademicYearID']);
	$YearTermID = stripslashes($_POST['YearTermID']);
	$SelectionID = stripslashes($_POST['SelectionID']);
	$jsOnchange = stripslashes($_POST['jsOnchange']);
	
	$returnString = $libsubject_ui->Get_Term_Selection($SelectionID, $AcademicYearID, $SelectedYearTermID='', 
									$jsOnchange, $NoFirst=1, $NoPastTerm=0, $displayAll=0, $AllTitle='', $PastTermOnly=1, $YearTermID);
}
else if ($RecordType == "Reload_Subject_Group_Table")
{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	$libsubject_ui = new subject_class_mapping_ui();
	
	$YearTermID = stripslashes($_POST['YearTermID']);
	$AllowStudentChk = stripslashes($_POST['AllowStudentChk']);
	
	$returnString = $libsubject_ui->Get_Copy_Subject_Group_Table($YearTermID, $AllowStudentChk);
}
else if($RecordType == 'load_reference'){
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
	
	$libLC = new Learning_Category();
	$linterface = new interface_html();
	
	if($flag == 'SubjectID'){
	//get all learning category
	$LC_InfoArr = $libLC->Get_All_Learning_Category();
	
	$titleArray = array($Lang['General']['Code'],$Lang['SysMgr']['SubjectClassMapping']['Title']);
			
		//title 
	$countOfCategory = count($LC_InfoArr);
		
	//	$groupInfo = $lgroup->returnAllGroupIDAndName('AND (RecordType=0 OR AcademicYearID='.$AcademicYearID.')',Get_Lang_Selection('TitleChinese','Title'));
	
		//$countOfGroup = count($groupInfo);
		for($i=0;$i<$countOfCategory;$i++){
			$contentArray[] = array($LC_InfoArr[$i]['Code'],Get_Lang_Selection($LC_InfoArr[$i]['NameChi'],$LC_InfoArr[$i]['NameEng']));
			
		}
		$returnString = $linterface->getReferenceThickBox($titleArray,$contentArray,'');
	}
	if($flag == 'SubjectComponentID'){
		$libSubject = new Subject();
		$subjectInfoArr = $libSubject->Get_Subject_List();

		$countofSubject = count($subjectInfoArr);
		$titleArray = array($Lang['SysMgr']['SubjectClassMapping']['WebSAMSCode'],$Lang['SysMgr']['SubjectClassMapping']['Title']);
		for($i=0;$i<$countofSubject;$i++){
			$contentArray[] = array($subjectInfoArr[$i]['CODEID'],Get_Lang_Selection($subjectInfoArr[$i]['CH_DES'],$subjectInfoArr[$i]['EN_DES']));
		}
		$returnString = $linterface->getReferenceThickBox($titleArray,$contentArray,'');		
	}
}
//echo $contentArray;	
echo $returnString;

intranet_closedb();

?>