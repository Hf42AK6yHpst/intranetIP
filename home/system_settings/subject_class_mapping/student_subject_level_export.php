<?php 
//using: 

############# Change Log [Start] ################
#
#	Date:	2020-05-07 Tommy
#           - modified access checking, added $plugin["Disable_Subject"]
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) {
	No_Access_Right_Pop_Up();
}

$linterface = new interface_html();
$lfcm_ui = new form_class_manage_ui();
$lscm_ui = new subject_class_mapping_ui();

// $lscm = new subject_class_mapping();
$lpf_dbs = new libpf_dbs_transcript();
$lexport = new libexporttext();

# Form & Class
$academicYearId = Get_Current_Academic_Year_ID();
$classId = $_POST['classId']? $_POST['classId'] : $_GET['classId'];
$classId = IntegerSafe(standardizeFormPostValue($classId));
$isTemplate = $_POST['isTemplate'] ? $_POST['isTemplate'] : $_GET['isTemplate'];
$isTemplate = false; // show record anyway

$year_class = new year_class($classId, true, false, true);
$yearId = $year_class->YearID;
$className = $year_class->Get_Class_Name();

# Curriculum & Subject Level
$curriculum = '--';
$subjectLevelAry = array();
$subjectLevelAry[''][''] = 'N.A.';
$allCurriculumAry = $lpf_dbs->getCurriculumSetting('','','','',$ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"]);
foreach($allCurriculumAry as $thisCurriculumInfo)
{
	if(trim($thisCurriculumInfo['SubjectLevel']) != '')
	{
		$thisSubjectLevelAry = explode('<br />', nl2br(trim($thisCurriculumInfo['SubjectLevel'])));
		$thisSubjectLevelAry = array_values(array_unique(array_filter(Array_Trim($thisSubjectLevelAry))));
		foreach($thisSubjectLevelAry as $thisSubjectLevel) {
			$subjectLevelAry[$thisCurriculumInfo['Code']][trim($thisSubjectLevel)] = trim($thisSubjectLevel);
		}
	}
}
$curriculumMappingAry = BuildMultiKeyAssoc($allCurriculumAry, 'CurriculumID', 'Code', 1, 0);
if(isset($curriculumMappingAry[$year_class->CurriculumID])) {
	$curriculum = $curriculumMappingAry[$year_class->CurriculumID];
}

# Form Subjects
$subjects = new subject();
$formSubjectAry = $subjects->Get_Subject_By_Form($yearId);
$numOfSubject = count($formSubjectAry);

# Export Headers
$exportHeader = array("UserLogin", "Class Name [ref]", "Class No [ref]", "Name (Eng) [ref]", "Name (Chi) [ref]");
foreach($formSubjectAry as $thisFormSubject)
{
	$subjID = $thisFormSubject['RecordID'];
	$subjectObj = new subject($subjID);
	$subjCode = $subjectObj->Get_Subject_Code();
	$exportHeader[] = $subjCode;
}

$exportContentArr = array();

if(!$isTemplate){
	# Class Students
	$classStudentAry = $year_class->ClassStudentList;
	$numOfStudent = count($classStudentAry);
	
	
	# Student Subject Level
	$studentIDAry = Get_Array_By_Key((array)$classStudentAry, 'UserID');
	$studentSubjectLevelAry = $lpf_dbs->getSubjectLevel('', '', $studentIDAry);
	$studentSubjectLevelAry = BuildMultiKeyAssoc($studentSubjectLevelAry, array('StudentID', 'SubjectID'), 'SubjectLevel', 1, 0);
	
	foreach($classStudentAry as $thisStudentInfo)
	{
		$thisStudentCurriculum = $curriculum;
		$studentCurriculumSetting = $lpf_dbs->getStudentCurriculumSettings($thisStudentInfo['UserID']);
		if(!empty($studentCurriculumSetting) && $studentCurriculumSetting[0]['CurriculumID'] && isset($curriculumMappingAry[$studentCurriculumSetting[0]['CurriculumID']])) {
			$thisStudentCurriculum = $curriculumMappingAry[$studentCurriculumSetting[0]['CurriculumID']];
		}
		$studentID = $thisStudentInfo['UserID'];
		$lu = new libuser($studentID);
		$userLogin = $lu->UserLogin;
		$chiName = $lu->ChineseName;
		$engName = $lu->EnglishName;
		$classno = $thisStudentInfo['ClassNumber'];
		
		$row = array($userLogin, $className, $classno, $chiName, $engName);
		
		foreach($formSubjectAry as $thisSubjectInfo)
		{
			$selected_value = $studentSubjectLevelAry[$thisStudentInfo["UserID"]][$thisSubjectInfo["RecordID"]];
			if($selected_value=="") $selected_value = 'N.A.';
			$row[] = $selected_value;
		}
		$exportContentArr[] = $row;
		unset($lu);
	}

	$export_content = $lexport->GET_EXPORT_TXT($exportContentArr, $exportHeader);
} else {
	$export_content = $lexport->GET_EXPORT_TXT(array($exportHeader), array());
}

$filename = "student_subject_level_$className.csv";
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>