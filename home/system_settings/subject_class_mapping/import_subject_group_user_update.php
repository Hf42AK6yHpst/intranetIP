<?
// Modifying by: 
/******************************************************
 * Modification log:
 *	Date:	2020-05-07 Tommy
 *          - modified access checking, added $plugin["Disable_Subject"]
 * 23-10-2015 (Pun)
 * 		- added hidden input "addStd" for sync subject group to eClass
 * 21-06-2011 Marcus:
 * 		- add param YearTermID to iFrame src, 
 * 		YearTermID must be passed after cater same class code in different term
 * ****************************************************/


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
// include_once($PATH_WRT_ROOT."includes/libimporttext.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT.'includes/libeclass40.php'); 

intranet_opendb();

$li = new libdb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}



# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

# Title / Menu
$linterface = new interface_html();
$CurrentPageArr['Subjects'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['ImportTeacherStudent']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 


$sql = "select * from TEMP_SUBJECT_GROUP_USER_IMPORT where UserID = $UserID order by RowNumber";
$ImportDataArr = $li->returnArray($sql);
$numOfData = count($ImportDataArr);

### iFrame for real-time update count
$thisSrc = "ajax_update.php?Action=Import_Subject_Group_User&YearTermID={$YearTermID}&addStd={$addStd}";
$ImportLessonIFrame = '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="display:none;"></iframe>'."\n";
					
$ProcessingMsg = str_replace('<!--NumOfRecords-->', '<span id="BlockUI_WholeSpan"><span id="BlockUISpan">0</span> / '.$numOfData, $Lang['SysMgr']['Timetable']['RecordsProcessed'].'</span>');

?>

<script language="javascript">
$(document).ready(function () {
	//js_Import_Lesson();
	Block_Document('<?=$ProcessingMsg?>');
});
</script>

<br />
<form id="form1" name="form1" method="post">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td align="center"><?=$x?></td>
	</tr>
			
	<tr>
		<td>
			<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
			
			<tr>
				<td class='tabletext' align='center'>
					<span id="NumOfProcessedPageSpan">0</span> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?>
				</td>
			</tr>
	
			<tr>
				<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			<tr>
				<td align="center" colspan="2">
				<?=$linterface->GET_ACTION_BTN($button_back, "button", "window.location='subject_group_mapping.php?AcademicYearID=$AcademicYearID&YearTermID=$YearTermID'"); ?>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<?=$ImportLessonIFrame?>
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
