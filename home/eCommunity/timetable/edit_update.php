<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libbatch.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libtimetablegroup.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$lg = new libgrouping();
$lgroup = new libgroup($GroupID);
if(isset($GroupID) && $lgroup->hasAdminTimetable($UserID)){
     $lb = new libbatch();
     $row = $lb->slots;
     $lu = new libtimetablegroup();
     $lu->setGroupID($GroupID);
     $lu->setRow($row);
     $lu->setTimetable($data,$rows,$cols);
     $msg = 2;
}

intranet_closedb();
header("Location: index.php?GroupID=$GroupID&msg=$msg");
?>