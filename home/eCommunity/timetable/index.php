<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libbatch.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libtimetablegroup.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../includes/libimporttext.php");
include_once("../../../includes/libinterface.php");
intranet_auth();
intranet_opendb();

$lo = new libuser($UserID);
$Groups = $lo->returnGroups();
# Limit user can only view timetable of own group
$GroupID = ($lo->IsGroup($GroupID)) ? $GroupID : $Groups[0][0];

$li = new libgroup($GroupID);
$limport = new libimporttext();

if ($li->isAccessTimeTable())
{

$functionbar .= jumpIcon().$li->getSelectTimetableGroups("name=GroupID onChange=\"this.form.action='index.php'; this.form.submit()\"",$GroupID);
/*
"<select name=GroupID onChange=\"this.form.action='index.php'; this.form.submit()\">\n";
for($i=0; $i<sizeof($Groups); $i++)
$functionbar .= "<option value=".$Groups[$i][0].(($Groups[$i][0]==$GroupID)?" SELECTED":"").">".$Groups[$i][1]."</option>\n";
$functionbar .= "</select>\n";
*/
$lg = new libgrouping();
$isAdmin = $li->hasAdminTimetable($UserID);
$instruction = "";
if ($isAdmin)
{
    $functionbar .= "<a href=javascript:checkGet(document.form1,'import.php')>".importIcon2()."$button_import</a>\n".toolBarSpacer();
    $functionbar .= "<a href=javascript:checkGet(document.form1,'export.php')>".exportIcon2()."$button_export</a>\n".toolBarSpacer();
    $toolbar .= "<input alt='Edit' type=image src=$image_edit onClick=\"this.form.action='edit.php'\">";
    $toolbar .= "<input alt='Clear' type=image src=$image_clear onClick=\"this.form.action='remove.php'; return(confirm(globalAlertMsg3))\">";
    $instruction = "<tr><td width=10> </td><td colspan=2>$i_Timetable_IndexInstruction</td><td width=10>&nbsp;</td></tr>";
}
$lb = new libbatch();
$slots = $lb->slots;
for ($i=0; $i<sizeof($slots); $i++)
     $row[$i] = $slots[$i][2]."<br>\n".$slots[$i][1];

$lu = new libtimetablegroup();
$lu->setGroupID($GroupID);
$lu->setRow($row);

switch($msg)
{
        case 10: $xmsg = $i_con_msg_import_success; break;
        case 13: $xmsg = $i_con_msg_import_failed; break;
        default: $xmsg = "";
}

if ($xmsg != "")
{
    $x = "<tr><td></td><td colspan=3>$xmsg</td></tr>\n";
}

include_once("../../../templates/fileheader.php");
include_once("../tooltab.php");
?>

<form name=form1 method=get action=index.php>
<table width="750" border="0" cellspacing="0" cellpadding="0">
<tr><td>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="101" class=popup_top><?=$i_grouphead_timetable?></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td class=popup_topcell><font color="#FFFFFF" title="<?=$li->Title?>"><?php echo chopword($li->Title,40); ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
      <?=$header_tool?>
      <table width="750" border="0" cellspacing="0" cellpadding="10" background="/images/groupinfo/popup_tbbg_large.gif">
  <?=$x?>
  <tr>
  <td width=10>&nbsp;</td>
  <td width=500 align=left><?php echo $functionbar; ?></td>
  <td width=230 align=right><?=$toolbar?></td>
  <td width=10>&nbsp;</td>
  </tr>
  <?=$instruction?>
  <tr>
  <td width=10> </td>
  <td colspan=2> <?=$lu->display()?>
  </td>
  <td width=10>&nbsp;</td>
  </tr>
      </table>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/images/groupinfo/popup_bottom_large.gif"></td>
        </tr>
      </table>


</td></tr>
</table>
</form>

<?php
include_once("../../../templates/filefooter.php");
}
else
{
    header ("Location: ../close.php");
}
intranet_closedb();
?>