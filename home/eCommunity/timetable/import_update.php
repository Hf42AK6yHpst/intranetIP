<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libgroup.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$li = new libfilesystem();
$lg = new libgrouping();
$lgroup = new libgroup($GroupID);

$file_end = ($type==0?"_timetable.csv":"_timetable_special.csv");
$ext = strtoupper($li->file_ext($userfile_name));

if ($ext != ".CSV")
{
    header ("Location: index.php?GroupID=$GroupID&msg=13");
}
else
{

    if(isset($GroupID) && $lgroup->hasAdminTimetable($UserID)){
       $url = "/file/timetable/g".$GroupID.$file_end;
       if($userfile=="none"){
       }
       else
       {
           $li->lfs_copy($userfile, $intranet_root.$url);
       }
    }
    if ($type == 0)
    {
        $li->lfs_remove("/file/timetable/g".$GroupID."_timetable_special.csv");
    }
    header("Location: index.php?GroupID=$GroupID&msg=10");
}

?>