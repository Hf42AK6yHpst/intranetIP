<?php
/**************************
 * Date : 	2012-12-07 Rita
 * Details: add edit log
 * 
 **************************/
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/liblog.php");

intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$targetUserID = IntegerSafe($targetUserID);
$RoleID = IntegerSafe($RoleID);

$li = new libdb();
$liblog = new liblog();

$RecordType?$RecordType="A":$RecordType="NULL";

$targetUserID = $_POST['targetUserID'];

if ($sys_custom['Group']['AdminRightLog']) {
	############       Start of Edit Log       ###########
	###### Get Info for Delete Log #####
	$sql = "SELECT 
					UserGroupID,	
					RecordType,
					RoleID,
					UserID,
					GroupID,
					EnrolGroupID
			FROM
					INTRANET_USERGROUP
			WHERE
					GroupID = '$GroupID' AND USERID='$targetUserID'";
					
	$iuInfoAry =  $li->returnArray($sql);
	
	######  End Of Get Info for Delete Log ###### 
			$iuTmpAry = array();
				
				
			if($RecordType=="A"){
				$section = 'Update_MemberSetting_SetAsAdmin';
			}else{
				$section = 'Update_MemberSetting_SetAsNotAdmin';
			}
			
			$tableName = 'INTRANET_USERGROUP';
	
			$UserGroupID = $iuInfoAry[$i]['UserGroupID'];
			$originalRecordType = $iuInfoAry[$i]['RecordType'];
			$originalRoleID = $iuInfoAry[$i]['RoleID'];
			$userRecordID = $iuInfoAry[$i]['UserID'];
			$tempGroupID = $iuInfoAry[$i]['GroupID'];
			$tempEnrolGroupID = $iuInfoAry[$i]['EnrolGroupID'];
			
			$iuTmpAry['UserGroupID'] = $UserGroupID;
			$iuTmpAry['OriginalRecordType'] = $originalRecordType;
			$iuTmpAry['OriginalRoleID'] = $RoleID;
			
			$iuTmpAry['NewRecordType'] = $RecordType;
			$iuTmpAry['NewRoleID'] = $RoleID;
			$iuTmpAry['UserID'] = $userRecordID;
			$iuTmpAry['GroupID'] = $tempGroupID;
			$iuTmpAry['EnrolGroupID'] = $tempEnrolGroupID;
			
			
			# Insert Delete Log
			$successArr['Log_Delete'] = $liblog->INSERT_LOG('eCommunity', $section, $liblog->BUILD_DETAIL($iuTmpAry), $tableName, $targetUserID);	
		
				
	############       End of Edit Log      #############
}

$sql ="UPDATE INTRANET_USERGROUP SET RECORDTYPE='$RecordType' WHERE GROUPID='$GroupID' AND USERID='$targetUserID'";
$li->db_db_query($sql);

$sql = "UPDATE INTRANET_USERGROUP SET RoleID = '$RoleID' WHERE GroupID = '$GroupID' AND UserID = '$targetUserID'";
$li->db_db_query($sql);

//$sql = "UPDATE INTRANET_USERGROUP SET Performance = '$performance' WHERE UserID = '$targetUserID' AND GroupID = '$GroupID'";
//$li->db_db_query($sql);

intranet_closedb();
header("Location: setting.php?GroupID=$GroupID&msg=update");
?>