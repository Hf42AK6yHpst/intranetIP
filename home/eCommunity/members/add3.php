<?php
# Replace add.php in 2.0

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

# retrieve role type

$lo = new libgroup($GroupID);
$lu2007 = new libuser2007($UserID);
$linterface = new interface_html();
$legroup = new libegroup();

$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,0,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
} 

         $row = $lo->returnRoleType();
         $RoleOptions .= "<select name=RoleID>\n";
         for($i=0; $i<sizeof($row); $i++)
         $RoleOptions .= (Isset($RoleID)) ? "<option value=".$row[$i][0]." ".(($row[$i][0]==$RoleID)?"SELECTED":"").">".$row[$i][1]."</option>\n" : "<option value=".$row[$i][0]." ".(($row[$i][2]==1)?"SELECTED":"").">".$row[$i][1]."</option>\n";
         $RoleOptions .= "</select>\n";

         $permitted = array(1,2,3,4);

         $li = new libgrouping();

         if($CatID > 10){
              unset($ChooseGroupID);
              $ChooseGroupID[0] = $CatID-10;
         }

         $x1  = ($CatID!=0 && $CatID < 10) ? "<select name=CatID onChange=checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()>\n" : "<select name=CatID onChange=this.form.submit()>\n";
         $x1 .= "<option value=0></option>\n";
         $x1 .= "<option value=3 ".(($CatID==3)?"SELECTED":"").">".$i_GroupRole[3]."</option>\n";
         $x1 .= "<option value=1 ".(($CatID==1)?"SELECTED":"").">".$i_GroupRole[1]."</option>\n";
         $x1 .= "<option value=2 ".(($CatID==2)?"SELECTED":"").">".$i_GroupRole[2]."</option>\n";
         $x1 .= "<option value=4 ".(($CatID==4)?"SELECTED":"").">".$i_GroupRole[4]."</option>\n";
         $x1 .= "<option value=5 ".(($CatID==5)?"SELECTED":"").">".$i_GroupRole[5]."</option>\n";
         $x1 .= "<option value=6 ".(($CatID==6)?"SELECTED":"").">".$i_GroupRole[6]."</option>\n";
         $x1 .= "<option value=0>";
         for($i = 0; $i < 20; $i++)
         $x1 .= "_";
         $x1 .= "</option>\n";
         $x1 .= "<option value=0></option>\n";
         /*
         $row = $li->returnCategoryGroups(0);
         for($i=0; $i<sizeof($row); $i++){
              $IdentityCatID = $row[$i][0] + 10;
              $IdentityCatName = $row[$i][1];
              if ($access_right[$row[$i][0]-1]==1 || $file_content == "")
                  $x1 .= "<option value=$IdentityCatID ".(($IdentityCatID==$CatID)?"SELECTED":"").">$IdentityCatName</option>\n";
         }
         */
         $x1 .= "<option value=11 ".(($CatID==11)?"SELECTED":"").">$i_identity_teachstaff</option>\n";
         $x1 .= "<option value=12 ".(($CatID==12)?"SELECTED":"").">$i_identity_student</option>\n";
         $x1 .= "<option value=13 ".(($CatID==13)?"SELECTED":"").">$i_identity_parent</option>\n";

         $x1 .= "<option value=0></option>\n";
         $x1 .= "</select>";

         if($CatID!=0 && $CatID < 10) {
              $row = $li->returnCategoryGroups($CatID);
              $x2  = "<select name=ChooseGroupID[] size=5 multiple>\n";
              for($i=0; $i<sizeof($row); $i++){
                   $GroupCatID = $row[$i][0];
                   $GroupCatName = $row[$i][1];
                   $x2 .= "<option value=$GroupCatID";
                   for($j=0; $j<sizeof($ChooseGroupID); $j++){
                   $x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
                   }
                   $x2 .= ">$GroupCatName</option>\n";
              }
              $x2 .= "<option>";
              for($i = 0; $i < 40; $i++)
              $x2 .= "&nbsp;";
              $x2 .= "</option>\n";
              $x2 .= "</select>\n";
         }

if($CatID > 10){
   # Return users with identity chosen
   $selectedUserType = $CatID - 10;
   $row = $li->returnUserForTypeExcludeGroup($selectedUserType,$GroupID);

     $x3  = "<select name=ChooseUserID[] size=5 multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}
else if(isset($ChooseGroupID)) {
         //     $row = $li->returnGroupUsersInIdentity($ChooseGroupID, $permitted);
              $row = $li->returnGroupUsersExcludeGroup($ChooseGroupID, $GroupID);
              $x3  = "<select name=ChooseUserID[] size=5 multiple>\n";
              for($i=0; $i<sizeof($row); $i++)
              $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
              $x3 .= "<option>";
              for($i = 0; $i < 40; $i++)
              $x3 .= "&nbsp;";
              $x3 .= "</option>\n";
              $x3 .= "</select>\n";
         }

         $step1 = getStepIcons(4,1,$i_SelectMemberNoGroupSteps);
         $step2 = getStepIcons(4,2,$i_SelectMemberNoGroupSteps);
         $step3 = getStepIcons(4,3,$i_SelectMemberNoGroupSteps);
         $step4 = getStepIcons(4,4,$i_SelectMemberNoGroupSteps);

$add_btn = $linterface->GET_ACTION_BTN($eComm['Add'], "button", "checkOption(this.form.elements['ChooseUserID[]']);add_user(this.form)");

$cancel_btn = $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php?GroupID=".$GroupID."'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
         

$CurrentPage	= "Settings_MemberSettings";
$MyGroupSelection= $legroup->getSelectAdminBasicInfo("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);

	$title = "
		<table width='100%' height='28' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' nowrap> <span class='contenttitle'>". $eComm['MemberSettings'] ."</span></td>
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$lu->Title."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
	";
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
	
$linterface->LAYOUT_START();
         ?>

         <script language="javascript">
         function add_role(obj){
              role_name = prompt("<?=$i_RoleNewPrompt?>", "New_Role");
              if(role_name!=null && Trim(role_name)!=""){
                   obj.role.value = role_name;
                   obj.action = "add_role.php";
                   obj.submit();
              }
         }

         function add_user(obj){
              obj.action = "add_user.php";
              obj.submit();
         }

         function SelectAll(obj)
         {
                  for (i=0; i<obj.length; i++)
                  {
                       obj.options[i].selected = true;
                  }
         }
         </script>

         <form name=form1 action="" method=post>
         <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
		<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
        	<br><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?>
        	<!---start--->
        	<tr>
		      <td width="12" height="25"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_01.gif" width="12" height="25"></td>
		      <td width="13" height="25"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_02.gif" width="13" height="25"></td>
		      <td height="25" align="right" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_02.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		          <tr>
		            <td align="left"></td>
		            <td align="right"><a href="../group/index.php?GroupID=<?=$GroupID?>"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_off.gif" name="a_mini" width="16" height="16" border="0" id="a_mini" onMouseOver="MM_swapImage('a_mini','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_03b.gif" width="16" height="25" align="absmiddle"></td>
		          </tr>
		      </table></td>
		      <td width="18" height="25"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="18" height="25"></td>
		    </tr>
		    <tr>
		      <td width="12" height="28" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_05.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_05.gif" width="12" height="28"></td>
		      <td width="13" height="28" valign="top" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_10.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_06.gif" width="13" height="28"></td>
		      
		      
		      <td height="28" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_07.gif">
			 <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" height="29">&nbsp;</td>
            <td align="right"><a href="setting.php?GroupID=<?=$GroupID?>" class="contenttool"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_prev_off.gif" width="11" height="10" border="0" align="absmiddle"> <?=$button_back?> </a></td>
          </tr>
          <!--<tr>
            <td align="left"><table border="0" cellspacing="0" cellpadding="3">
                  <tr>
                    <td><?=$searchbar?></td>
                  </tr> 
                </table>
                </td>
            <td align="right"><table border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td nowrap><?=$delBtn?></td>
                <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
                <td nowrap><?=$editBtn?></td>
              </tr>
            </table></td>
          </tr>-->
      </table>		      
		      
      
      
		      </td>
		      
    		  <td width="18" height="28" valign="top" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_12.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_08.gif" width="18" height="28"></td>
    		</tr> 
    		
    		<tr>
      			<td width="12" valign="bottom" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_05.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_09b.gif" width="12" height="19"></td>
      			<td width="13" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_10.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_10.gif" width="13" height="200"></td>
      			<td bgcolor="#fffeed" class=' tabletext'>
      			<table width="750" border="0" cellspacing="0" cellpadding="0" >
				<tr>
            	<td align=left  >
              	<table width=100% border=0 cellspacing=1 cellpadding=2>
              	<tr><td class="dotline" colspan="2"><img src=<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_edit_b.gif" width="20" height="20" border="0" align="absmiddle"><span class="tabletext"><strong><?=$eComm['NewMember']?></strong></span></td></tr>
      			<tr><td align=left class='tabletext'><?=$i_SelectMemberNoGroupInstruction?></td></tr>
				
				<tr><td align=left class='tabletext'><?=$step1?></td></tr>
		        <tr><td align=left class='tabletext'> <?=$i_SelectMemberNoGroupSteps[1]?></td></tr>
		        <tr><td align=left class='tabletext'> <?php echo $RoleOptions; ?><a href=javascript:add_role(document.form1) ><?=newIcon2()." $button_new $i_admintitle_role"?></a></td></tr>

		        <tr><td align=left class='tabletext'> <?=$step2?></td></tr>
		        <tr><td align=left class='tabletext'> <?php echo $i_frontpage_campusmail_select_category; ?><br><img src=../../../images/space.gif border=0 width=1 height=3></td></tr>
		        <tr><td align=left class='tabletext'> <?php echo $x1; ?>
		        
		        <tr><td align=left class='tabletext'> <?php if($CatID!=0 && $CatID < 10) { ?></td></tr>
		        <tr><td align=left class='tabletext'> <?=$step3?></td></tr>
		        <tr><td align=left class='tabletext'> <?php echo $i_frontpage_campusmail_select_group; ?><br><img src=../../../images/space.gif border=0 width=1 height=3></td></tr>
		        <tr><td align=left class='tabletext'> <?php echo $x2; ?></td></tr>
		        <tr><td align=left class='tabletext'> <input type=image src=<?=$image_expand?> alt="<?php echo $i_frontpage_campusmail_expand; ?>" onClick=checkOption(this.form.elements["ChooseGroupID[]"]);></td></tr>
		        <tr><td align=left class='tabletext'> <input type=image src=<?=$image_selectall?> onClick="SelectAll(this.form.elements['ChooseGroupID[]']); return false;" alt="<?=$button_select_all?>"></td></tr>
		         <?php } ?>
		        <?php if(isset($ChooseGroupID)) { ?>
         		<tr><td align=left class='tabletext'> <?=$step4?></td></tr>
         		<tr><td align=left class='tabletext'> <?php echo $i_frontpage_campusmail_select_user; ?><br><img src=../../../images/space.gif border=0 width=1 height=3></td></tr>
         		<tr><td align=left class='tabletext'> <?php echo $x3; ?></td></tr>
         		 <!--<input type=image src=<?=$image_add?> alt='<?=$button_add?>' onClick=checkOption(this.form.elements["ChooseUserID[]"]);add_user(this.form)><input type=image src=<?=$image_selectall?> onClick="SelectAll(this.form.elements['ChooseUserID[]']); return false;" alt="<?=$button_select_all?>">-->
         		
         		<tr><td align=left class='tabletext'><?php echo  $linterface->GET_BTN($eComm['select_all'], "button", "SelectAll(this.form.elements['ChooseUserID[]']); return false;")?></td></tr>
         		 
         		<tr><td class="dotline" colspan="2">&nbsp;</td></tr>  
         		 <tr><td align=center class='tabletext'><?=$add_btn?> <?=$cancel_btn?></td></tr>
         		 
         		 <?php } ?>
         		</table>
            	</td>
	            </tr>
      		    </table>
         	    </td>
         		 
         		
      			</td>
      			
      			<td width="18" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_12.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_12.gif" width="18" height="19"></td>
      			
    			</tr>
    				
         		 	
         		 	
         		 
    			<tr>
			      <td width="12" height="31"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="12" height="31"></td>
			      <td width="13" height="31"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_14.gif" width="13" height="31"></td>
			      <td height="31" align="right" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_15.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_15b.gif" width="170" height="31"></td>
			      <td width="18" height="31"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_16.gif" width="18" height="31"></td>
			    </tr>
			
			<!--end--->
            </table>
        </td>
</tr>
</table>
<input type="hidden" name="GroupID" value="<?php echo $GroupID; ?>">
<input type="hidden" name="role">
</form>

       
        

         <?php
         
         
         $linterface->LAYOUT_STOP();



intranet_closedb();
?>