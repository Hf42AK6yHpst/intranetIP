<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$StudentID = IntegerSafe($StudentID);


$li = new libclubsenrol();
$StudentID = array_unique($StudentID);
$StudentID = array_values($StudentID);
if ($li->mode == 0 || !$li->isGroupAllowApproval($GroupID))
{
    header ("Location: index.php?GroupID=$GroupID");
}
else if ($li->mode == 1 || $li->mode == 2)
{
     $list = implode(",",$StudentID);
     $sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT SET RecordStatus = 1 WHERE StudentID IN ($list) AND GroupID = '$GroupID' AND RecordStatus = 0";
     $li->db_db_query($sql);
}

intranet_closedb();
header("Location: enrol.php?GroupID=$GroupID&filter=$filter&msg=3");
?>