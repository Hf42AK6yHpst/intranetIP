<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$targetUserID = IntegerSafe($targetUserID);

$li = new libgroup($GroupID);
$lu2007 = new libuser2007($UserID);
$linterface = new interface_html();
$lo = new libgroup($GroupID);
$legroup = new libegroup();

$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,0,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
} 

$targetUserID = (is_array($targetUserID)? $targetUserID[0]:$targetUserID);
$TargetGroupsID = $legroup->targetUserHasAnyGroupAdminRight($targetUserID);

     $lu = new libuser($targetUserID);
     $lg = new libgrouping();
     $lword = new libwordtemplates();
     //$performancelist = $lword->getSelectPerformance("onChange=\"this.form.performance.value=this.value\"");
     $mem_info = $li->returnMemberInfo($targetUserID);
     list ($Role,$Performance) = $mem_info;
	 in_array($GroupID, $TargetGroupsID)? $isAdmin=1:$isAdmin=0;
		
	
     $row = $li->returnRoleType();
     $functionbar = "<select name=RoleID>\n";
     for($i=0; $i<sizeof($row); $i++)
     
     //$functionbar .= "<option value=".$row[$i][0]." ".(($row[$i][2]==1)?"SELECTED":"").">".$row[$i][1]."</option>\n";
     $functionbar .= "<option value=".$row[$i][0]." ".(($row[$i][1]==$Role)?"SELECTED":"").">".$row[$i][1]."</option>\n";
     $functionbar .= "</select>\n";
     //$functionbar .= "<a href=javascript:checkRole(document.form1,'targetUserID[]','role.php')><img border=0 src=$image_updaterole alt='$button_update $i_admintitle_role'></a>";
         
         
    $button .= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")."\n";
    $button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='setting.php?GroupID=".$GroupID."'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");

	$CurrentPage	= "Settings_MemberSettings";
	$MyGroupSelection= $legroup->getSelectAdminBasicInfo("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);
	
	$title = "
		<table width='100%' height='28' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' nowrap> <span class='contenttitle'>". $eComm['MemberSettings'] ."</span></td>
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$lu->Title."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
	";
	$TAGS_OBJ[] = array($title,"");
	$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
		
	$linterface->LAYOUT_START();
    $PAGE_NAVIGATION[] = array($button_edit.($intranet_session_language=="en"?" ":""). $eComm['Member'], "");     
         ?>
         

<form name=form1 action="edit_update.php" method=post>
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $i_UserLogin; ?>:</span></td>
					<td class="tabletext"><?=$lu->UserLogin?></td>
				</tr>
                                
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $i_UserEnglishName; ?>:</span></td>
					<td class='tabletext'><?=$lu->EnglishName?></td>
				</tr>
                                
                               <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $i_UserChineseName; ?>:</span></td>
					<td class='tabletext'><?=$lu->ChineseName?></td>
				</tr>
				<?if($lu->RecordType==2){?>
                                <tr valign="top"> 
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $i_UserClassName; ?>:</span></td>					
                                        <td class='tabletext'><?=empty($lu->ClassName)?'--':$lu->ClassName?></td>
               	</tr>
			                    <tr valign="top"> 
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $i_UserClassNumber; ?>:</span></td>					
                                        <td class='tabletext'><?=empty($lu->ClassNumber)?'--':$lu->ClassNumber?></td>
				</tr>
				<?}?>
								<tr valign="top"> 
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $i_ActivityName; ?>:</span></td>					
                                        <td class='tabletext'><?=$li->Title?></td>
                </tr>
								<tr valign="top"> 
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $i_ActivityRole; ?>:</span></td>					
                                        <td class='tabletext'><?=$functionbar?></td>
                </tr>
                				<tr valign="top"> 
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $eComm['GrantAdmin']; ?>:</span></td>					
                                        <td class='tabletext'>
                                        <input type="radio" name="RecordType" value="1" id="RecordType1" <?=$isAdmin?"checked":""?>> <label for="RecordType1"><?=$i_general_yes?></label> 
										<input type="radio" name="RecordType" value="0" id="RecordType0" <?=$isAdmin?"":"checked"?>> <label for="RecordType0"><?=$i_general_no?></label>
                                        </td>
                </tr>
                				<!--<tr valign="top"> 
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $i_ActivityPerformance; ?>:</span></td>					
                                        <td class='tabletext'><input type=text size=30 name=performance value='<?=$Performance?>'> <?=$performancelist?></td>
                </tr>-->
				
                </table>
			</td>
                </tr>
                </table>
	</td>
</tr>

<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td align="left" class="tabletextremark"></td>
		</tr>
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?=$button?>
                		
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>

<input type=hidden name=GroupID value=<?=$GroupID?>>
<input type=hidden name=targetUserID value=<?=$targetUserID?>>
</form>
<?php
$linterface->LAYOUT_STOP();


intranet_closedb(); 
?>