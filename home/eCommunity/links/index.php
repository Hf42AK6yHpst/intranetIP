<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once('../../../includes/libdbtable.php');
include_once("../../../includes/libuser.php");
include_once("../../../includes/libgroup.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$lo = new libuser($UserID);
$Groups = $lo->returnGroups();
$GroupID = ($lo->IsGroup($GroupID)) ? $GroupID : $Groups[0][0];

$lu = new libgroup($GroupID);

if ($lu->isAccessLinks())
{

//if ($lg->isGroupAdmin($UserID, $GroupID))
if ($lu->hasAdminLinks($UserID))
    $adminDelete = "CONCAT('<a href=javascript:del(', LinkID, ')><img src=../../../images/eraser_icon.gif alt=$button_remove hspace=20 vspace=2 border=0 align=absmiddle></a><br>')";
    else $adminDelete = "''";

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if($field=="") $field=2;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        case 5: $field = 5; break;
        default: $field = 2; break;
}
$order = ($order == 1) ? 1 : 0;
$sql = "SELECT CONCAT(
                        '<a href=javascript:view(', LinkID, ')><b>', Title, '</b></a>', IF(LOCATE(';$UserID;',ReadFlag)=0, '<img src=$image_path/new.gif border=0 hspace=2 align=absmiddle>', ''), ' <br> ',
                        IF(Description='', '', CONCAT(Description, ' <br> ')),
                        IF(Keyword='', '', CONCAT(Keyword, '<br>')),
                        '<span class=small>', '$i_LinkRating: ', IFNULL((VoteTot/VoteNum),0), '(', VoteNum, ') - ', '$i_LinkUserName: ', '<a href=mailto:', UserEmail, '>', UserName, '</a>', ' - ', '$i_LinkDateModified: ', DateModified, '</span> <br> ',
                        '<span class=small>', '<a href=javascript:view(', LinkID, ')>', URL, '</a></span> <br> ',
                        IF(UserID='$UserID', CONCAT('<a href=javascript:edit(', LinkID, ')><img src=../../../images/edit_icon.gif alt=$button_edit hspace=20 vspace=2 border=0 align=absmiddle></a><a href=javascript:del(', LinkID, ')><img src=../../../images/eraser_icon.gif alt=$button_remove hspace=20 vspace=2 border=0 align=absmiddle></a> <br> '), $adminDelete)
                )
                FROM INTRANET_LINK
                WHERE
                        GroupID = '$GroupID' AND
                        (Title like '%$keyword%' OR URL like '%$keyword%' OR Keyword like '%$keyword%' OR Description like '%$keyword%')
                ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("Title","URL","DateModified","IFNULL((VoteTot/VoteNum),0)","VoteNum","UserName");
$li->sql = $sql;
$li->no_col = 2;
$li->title = $i_frontpage_schoolinfo_groupinfo_group_links;
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = 3;

// TABLE COLUMN
$li->column_list .= "<td width=5%><br></td>\n";
$li->column_list .= "<td width=95%><br></td>\n";

/*
$functionbar .= jumpIcon()."<select name=GroupID onChange=this.form.pageNo.value=1;this.form.action='index.php';this.form.submit()>\n";
for($i=0; $i<sizeof($Groups); $i++)
$functionbar .= "<option value=".$Groups[$i][0].(($Groups[$i][0]==$GroupID)?" SELECTED":"").">".$Groups[$i][1]."</option>\n";
$functionbar .= "</select>\n";
*/
$functionbar = jumpIcon().$lu->getSelectLinksGroups("name=GroupID onChange=this.form.pageNo.value=1;this.form.action='index.php';this.form.submit()",$GroupID);
$searchbar .= "<input class=text type=text name=keyword size=15 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<input type=image src=$image_search alt='Search'>";
$toolbar .= "<a href=javascript:checkGet(document.form1,'new.php')>".newIcon2()."$i_LinkNew</a>".toolBarSpacer();
$sortbar .= "$i_LinkSortBy: <select name=selectfield>\n";
$sortbar .= "<option value=0 ".(($field==0)?"SELECTED":"").">$i_LinkTitle</option>\n";
$sortbar .= "<option value=1 ".(($field==1)?"SELECTED":"").">$i_LinkURL</option>\n";
$sortbar .= "<option value=2 ".(($field==2)?"SELECTED":"").">$i_LinkDateModified</option>\n";
$sortbar .= "<option value=3 ".(($field==3)?"SELECTED":"").">$i_LinkRating</option>\n";
$sortbar .= "<option value=4 ".(($field==4)?"SELECTED":"").">$i_LinkVoteNum</option>\n";
$sortbar .= "<option value=5 ".(($field==5)?"SELECTED":"").">$i_LinkUserName</option>\n";
$sortbar .= "</select>\n";
$sortbar .= "<a href=javascript:sortPage(".(($order==1) ? 0 : 1).",document.form1.selectfield.options[document.form1.selectfield.selectedIndex].value,document.form1) onMouseOver=\"window.status='';return true;\" onMouseOut=\"window.status='';return true;\"><img src=../../../images/".(($order==1) ? "desc.gif alt='$list_desc'" : "asc.gif alt='$list_asc'")." hspace=0 border=0></a>";

include_once("../../../templates/fileheader.php");
include_once("../tooltab.php");
?>

<script language="JavaScript">
function view(id){
        newWindow("view.php?LinkID="+id,5);
}

function edit(id){
        obj = document.form1;
        obj.LinkID.value = id;
        checkGet(obj,'edit.php');
}

function del(id){
        obj = document.form1;
        obj.LinkID.value = id;
        if(confirm(globalAlertMsg3)){
        checkGet(obj,'remove.php');
        }
}
</script>

<form name=form1 method=get>
<table width="750" border="0" cellspacing="0" cellpadding="0">
<tr><td>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="101" class=popup_top><?=$i_grouphead_sharedlinks?></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td class=popup_topcell><font color="#FFFFFF" title="<?=$lu->Title?>"><?php echo chopword($lu->Title,40); ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
      <?=$header_tool?>
      <table width="750" border="0" cellspacing="0" cellpadding="10" background="<?=$image_path?>/groupinfo/popup_tbbg_large.gif">
        <tr>
          <td align=center>
<table width=93% border=0 cellpadding=2 cellspacing=0>
<tr><td><?php echo $functionbar.$searchbar; ?></td><td align=right><?php echo $sortbar; ?></td></tr>
<tr><td colspan=2><?php echo $toolbar; ?></td></tr>
</table>
<?php echo $li->display(); ?>

      </td>
      </tr>
      </table>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="<?=$image_path?>/groupinfo/popup_bottom_large.gif"></td>
        </tr>
      </table>

</td></tr>
</table>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=LinkID>
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?php
include_once("../../../templates/filefooter.php");
}
else
{
    header ("Location: ../close.php");
}
intranet_closedb();
?>