<?php 
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/liblink.php");
include("../../../lang/lang.$intranet_session_language.php"); 
intranet_auth();
intranet_opendb();

$LinkID = IntegerSafe($LinkID);

$li = new liblink($LinkID);
$li->UpdateVote($Rate);

intranet_closedb();
header("Location: view.php?LinkID=$LinkID");
?>
