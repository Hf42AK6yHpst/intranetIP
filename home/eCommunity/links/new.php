<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libgroup.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$lo = new libuser($UserID);
$row = $lo->returnGroups();
$lgroup = new libgroup($GroupID);
$functionbar = $lgroup->getSelectLinksGroups("name=PostGroupID[] size=5 multiple",$GroupID);
$x .= $functionbar;
$toolbar .= "<input type=image src=$image_submit>";
$toolbar .= " <input type=image src=$image_reset onClick=' this.form.reset(); return false;'>";
$toolbar .= " <input type=image src=$image_cancel onClick=\"history.back(); this.form.method='get';this.form.action='index.php';return false;\">";

include("../../../templates/fileheader.php");
?>

<script language="javascript">
function checkform(obj){
     if(countOption(obj.elements["PostGroupID[]"])==0){ alert(globalAlertMsg18); return false; }
     if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_LinkTitle; ?>.")) return false;
     if(!check_text(obj.URL, "<?php echo $i_alert_pleasefillin.$i_LinkURL; ?>.")) return false;
}
</script>

<form action="new_update.php" method="post" onSubmit="return checkform(this);">
<table width="750" border="0" cellspacing="0" cellpadding="0">
<tr><td>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="101" class=popup_top><?=$i_grouphead_sharedlinks?></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td class=popup_topcell><font color="#FFFFFF"><?php echo $button_new." ".$i_frontpage_schoolinfo_groupinfo_group_links; ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
      <table width="750" border="0" cellspacing="0" cellpadding="10" background="<?=$image_path?>/groupinfo/popup_tbbg_large.gif">
        <tr>
          <td align=center>
<table width=92% border=0 cellpadding=2 cellspacing=1>
<tr><td align=right></td><td><?=$i_LinkInstruction?></td></tr>
<tr><td align=right><?php echo $i_LinkGroup; ?>:</td><td><?php echo $x; ?></td></tr>
<tr><td align=right><?php echo $i_LinkTitle; ?>:</td><td><input class=text type=text name=Title size=30 maxlength=100></td></tr>
<tr><td align=right><?php echo $i_LinkURL; ?>:</td><td><input class=text type=text name=URL size=50 maxlength=200 value="http://"></td></tr>
<tr><td align=right><?php echo $i_LinkDescription; ?>:</td><td><textarea name=Description cols=50 rows=5 wrap=virtual></textarea></td></tr>
<tr><td align=right><?php echo $i_LinkKeyword; ?>:</td><td><input class=text type=text name=Keyword size=50 maxlength=100></td></tr>
<tr><td><br></td><td><?=$toolbar?></td></tr>
</table>
</td>
</tr>
</table>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="<?=$image_path?>/groupinfo/popup_bottom_large.gif"></td>
        </tr>
      </table>

</td></tr>
</table>

<input type=hidden name=GroupID value="<?php echo $GroupID; ?>">
</form>
<?php
include_once("../../../templates/filefooter.php");
intranet_closedb();
?>