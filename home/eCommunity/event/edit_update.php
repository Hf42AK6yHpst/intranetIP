<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libgroup.php");
include("../../../includes/libevent.php");
include("../../../includes/libfilesystem.php");
include("../../../includes/libcycle.php");
intranet_auth();
intranet_opendb();

$EventID = IntegerSafe($EventID);
$TargetGroupID = IntegerSafe($TargetGroupID);


$lc = new libcycle();
$is_cycle = ($lc->CycleID >= 1);
$le = new libevent($EventID);
$GroupID = $le->OwnerGroupID;
$lg = new libgroup($GroupID);


if ($GroupID == "")
{
    $gvalid = false;
}
else
{
    if ($lg->hasAdminAllEvent($UserID))
    {
        $gvalid = true;
    }
    else if ($lg->hasAdminInternalEvent($UserID) && $le->Internal == 1)
    {
         $gvalid = true;
    }
    else
    {
        $gvalid = false;
    }
}

if ($gvalid)
{

    $internal = 0;
    if (is_array($TargetGroupID) && (sizeof($TargetGroupID) != 0))
    {
        $TargetGroupID = array_unique($TargetGroupID);
        $TargetGroupID = array_values($TargetGroupID);
    }
    if ($lg->hasAdminAllEvent($UserID))
    {
        if (sizeof($TargetGroupID)==1 && $TargetGroupID[0]==$GroupID)
        {
            $internal = 1;
        }
    }
    else
    {
        $internal = 1;
    }

         $Title = intranet_htmlspecialchars(trim($Title));
         $Description = intranet_htmlspecialchars(trim($Description));
         $EventDate = intranet_htmlspecialchars(trim($EventDate));
         $EventVenue = intranet_htmlspecialchars(trim($EventVenue));
         $EventNature = intranet_htmlspecialchars(trim($EventNature));

         $fieldname  = "Title = '$Title', ";
         $fieldname .= "Description = '$Description', ";
         $fieldname .= "EventDate = '$EventDate', ";
         $fieldname .= "EventVenue = '$EventVenue', ";
         $fieldname .= "EventNature = '$EventNature', ";
         $fieldname .= "RecordStatus = '$RecordStatus', ";
         $fieldname .= "Internal = '$internal', ";
         $fieldname .= "UserID = '$UserID', ";
         $fieldname .= "DateModified = now()";
         if ($is_cycle)
         {
             $fieldname .= ",isSkipCycle = 0";
         }

         $sql = "UPDATE INTRANET_EVENT SET $fieldname WHERE EventID = '$EventID'";
         $lg->db_db_query($sql);

              # Remove all group event first
              $sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID = '$EventID'";
              $lg->db_db_query($sql);

              if ($internal == 1)
              {
                  # New record added back for this group only
                  $sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID) VALUES ($GroupID, $EventID)";
                  $lg->db_db_query($sql);
              }
              else
              {
                  if (sizeof($TargetGroupID)!= 0)
                  {
                      $sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID) VALUES ";
                      $delimiter = "";
                      for ($i=0; $i<sizeof($TargetGroupID); $i++)
                      {
                           $sql .= "$delimiter (".$TargetGroupID[$i].", $EventID)";
                           $delimiter = ",";
                      }
                      $lg->db_db_query($sql);
                  }
              }
              header("Location: index.php?GroupID=$GroupID&filter=$RecordStatus&msg=2");
}
else
{
    header ("Location: ../close.php");
}
intranet_closedb();
?>