<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libcycle.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$TargetGroupID = IntegerSafe($TargetGroupID);

$lc = new libcycle();
$is_cycle = ($lc->CycleID >= 1);

#$li = new libgrouping();
$lg = new libgroup($GroupID);
if ($lg->hasAdminInternalEvent($UserID) || $lg->hasAdminAllEvent($UserID))
{
    $internal = 0;
    if ($lg->hasAdminAllEvent($UserID))
    {
        if (sizeof($TargetGroupID)==1 && $TargetGroupID[0]==$GroupID)
        {
            $internal = 1;
        }
    }
    else
    {
        $internal = 1;
    }

         $Title = intranet_htmlspecialchars(trim($Title));
         $Description = intranet_htmlspecialchars(trim($Description));
         $EventDate = intranet_htmlspecialchars(trim($EventDate));
         $EventVenue = intranet_htmlspecialchars(trim($EventVenue));
         $EventNature = intranet_htmlspecialchars(trim($EventNature));

$fields = "Title";
$fields .= ",Description";
$fields .= ",EventDate";
$fields .= ",EventVenue";
$fields .= ",EventNature";
$fields .= ",RecordType";
$fields .= ",RecordStatus";
$fields .= ",DateInput ,DateModified";
$fields .= ",OwnerGroupID";
$fields .= ",UserID";
$fields .= ",Internal";


$values = "'$Title'";
$values .= ", '$Description'";
$values .= ", '$EventDate'";
$values .= ", '$EventVenue'";
$values .= ", '$EventNature'";
$values .= ", '3'";
$values .= ", '$RecordStatus'";
$values .= ", now(), now()";
$values .= ", '$GroupID'";
$values .= ", '$UserID'";
$values .= ", '$internal'";

if ($is_cycle)
{
    $fields .= ",isSkipCycle";
    $values .= ",0";
}

         $sql = "INSERT INTO INTRANET_EVENT ($fields) VALUES ($values)";
         $lg->db_db_query($sql);
         $EventID = $lg->db_insert_id();

          if ($internal != 1 && sizeof($TargetGroupID)!=0)
          {
              $group_event_field = "";
              $delimiter = "";
              for ($i=0; $i<sizeof($TargetGroupID); $i++)
              {
                   $group_event_field .= "$delimiter(".$TargetGroupID[$i].",$EventID)";
                   $delimiter = ",";
              }
              //$sql = "INSERT INTO INTRANET_GROUPANNOUNCEMENT (GroupID, AnnouncementID) VALUES (".$GroupID.", $AnnouncementID)";
              $sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID) VALUES $group_event_field";
              $lg->db_db_query($sql);
          }
          else if ($internal == 1)
          {
               $sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID) VALUES ($GroupID,$EventID)";
               $lg->db_db_query($sql);
          }

}
else
{
    header ("Location: ../close.php");
}
intranet_closedb();
header("Location: index.php?GroupID=$GroupID&filter=$RecordStatus&msg=1");
?>