<?php
# using: yat

############ Change Log [Start] ############
#	Date:	2016-11-14	Villa
#			- updated the UI 
#
#	Date:	2012-07-18	YatWoon
#			- use GET_DATE_PICKER for date selection
#
#	Date:	2010-12-21	YatWoon
#			- IP25 UI standard
#			- if not allow edit others announcement, only display "back" button
#
#	Date:	2010-08-24 YatWoon
#			hide Public/Private
#
# - 2010-08-12 YatWoon
#	change the description from textarea to fck editor (with upload image with Flash function)
#
############ Change Log [End] ############

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

$AnnouncementID = IntegerSafe($AnnouncementID);
$GroupID = IntegerSafe($GroupID);


$linterface 	= new interface_html();
$lfilesystem = new libfilesystem();

$AnnouncementID = (is_array($AnnouncementID)? $AnnouncementID[0]:$AnnouncementID);
$li = new libannounce($AnnouncementID);
$GroupID = $li->OwnerGroupID;

$legroup = new libegroup($GroupID);
$lo = new libgrouping();

$GroupsID = $legroup->hasAnyGroupAdminRight();

if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || (!( $legroup->hasAdminAllAnnounce($UserID) || substr($legroup->AdminAccessRight,2,1) || $legroup->AdminAccessRight=='ALL')))
{
	header("Location: ../settings/index.php?GroupID=$GroupID");
	exit;
}

$TargetGroupList = $li->returnTargetGroups();
$isPublicAnnounce = (empty($TargetGroupList)&&$li->Internal != 1) ;
$isPublicAnnounce_check = $isPublicAnnounce? "checked":"";
$isPublicAnnounce_check_spec = $isPublicAnnounce? "":"checked";

$CurrentPage	= "Settings_AnnouncementSettings";
$MyGroupSelection= $legroup->getSelectAdminAnnounce("name='SelectGroupID' onChange=\"window.location='settings.php?GroupID='+this.value\"", $GroupID);

$approval = $li->getApprovalStatus();
list($approvalStatus, $approvalUserID, $approvalUserName, $approvalTime, $remark, $reason) = $approval;
//$noEdit = ($special_feature['announcement_approval'] && $approvalStatus==1 && $li->RecordStatus == 1);
$noEdit = ($special_feature['announcement_approval'] && $approvalStatus==1 && $li->RecordStatus == 1) || (!$legroup->AllowDeleteOthersAnnouncement && $li->OwnerUserID!=$UserID);
$newEdit = ($special_feature['announcement_approval'] && $approvalStatus==3 && $li->RecordStatus == 3);

$agid = $GroupID;

if ($agid == "")
{
    $valid = false;
}
else
{
    if ($legroup->hasAdminAllAnnounce($UserID))
    {
        $valid = true;
    }
    else if ($legroup->hasAdminInternalAnnounce($UserID) && $li->Internal == 1)
    {
         $valid = true;
    }
    else
    {
        $valid = false;
    }
}

if (!$valid)
{
	header ("Location: settings.php?filter=1&xmsg=1&GroupID=1");
	exit;
}


### Title ###
$TAGS_OBJ[] = array($eComm['Management'],"settings.php?GroupID=$GroupID", 1);
$TAGS_OBJ[] = array($i_general_BasicSettings,"bsettings.php?GroupID=$GroupID", 0);
$title = "
	<table width='100%' border='0' cellpadding='0' cellspacing='0' valign=\"bottom\">
		<tr>
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$legroup->Title."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ_RIGHT[] = array($title,"",0);
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit.($intranet_session_language=="en"?" ":""). $i_adminmenu_announcement, "");


     $lo = new libgrouping();
//     $number_of_groups = sizeof($lo->returnGroups());
     $RecordStatus0 = ($li->RecordStatus==0) ? "CHECKED" : "";
     $RecordStatus1 = ($li->RecordStatus==1) ? "CHECKED" : "";
     $RecordStatus2 = ($li->RecordStatus==2 || $li->RecordStatus==3) ? "CHECKED" : "";
     if ($invalid==1)
     {
         echo "<p> $i_con_msg_date_startend_wrong </p>\n";
     }
     if ($ea==1)   // Editing attachment
     {
         $cTitle = stripslashes($Title);
         $cDescription = stripslashes($Description);
         $cStartDate = $AnnouncementDate;
         $cEndDate = $EndDate;
//          $cPublicStatus = $li->PublicStatus;
         $cOnTop = $li->onTop;
     }
     else  
     {
         $cTitle = $li->Title;
         //$cDescription = $li->Description;
         $cDescription = htmlspecialchars_decode($li->Description);
         $cStartDate = $li->AnnouncementDate;
         $cEndDate = $li->EndDate;
//          $cPublicStatus = $li->PublicStatus;
         $cOnTop = $li->onTop;
     }

if ($noEdit)
{
    $toolbar = $linterface->GET_ACTION_BTN($button_back, "button", "history.go(-1)","cancelbtn");
}
else
{
    $toolbar = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submit2") . " ". $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") . " ". $linterface->GET_ACTION_BTN($button_cancel, "button", "history.go(-1)","cancelbtn");
    
}
    if ($legroup->hasAdminAllAnnounce($UserID) && $legroup->AnnounceAllowed)
    {
         $public_row = '
			<!-- Groups -->
			<div id="groupSelectionBox" style="display:'.($isPublicAnnounce?"none":"block").';">
				<table width="90%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td valign="top">
							'.$i_admintitle_group.'
						</td>
					</tr>
					
					<tr>
						<td colspan="2">
							'.$lo->displayAnnouncementGroups($AnnouncementID,1).'
						</td>
					</tr>
				</table>
			</div>
		';
    }

    if ($noEdit)
    {
        $prompt_str = "$i_AdminJob_Announcement_NoEdit";
        if ($intranet_session_language == "en")
        {
            $prompt_str .= "<br> &nbsp;&nbsp;(". $Lang['eComm']['ApprovedBy'] ." $approvalUserName $i_AdminJob_Announcement_At $approvalTime)";
        }
        else
        {
            $prompt_str .= "<br> &nbsp;&nbsp;($i_AdminJob_Announcement_Being $approvalUserName $i_AdminJob_Announcement_At $approvalTime $i_AdminJob_Announcement_Approved)";
        }
        if ($reason != "")
        {
            $prompt_str .= "<br> &nbsp;&nbsp;(". $Lang['eComm']['Reason'] .": $reason)";
        }
    }
    else if ($newEdit)
    {
         $prompt_str = "$i_AdminJob_Announcement_NewEdit";
         if ($intranet_session_language == "en")
         {
             $prompt_str .= "<br> &nbsp;&nbsp;(". $Lang['eComm']['RejectedBy'] ." $approvalUserName $i_AdminJob_Announcement_At $approvalTime)";
         }
         else
         {
             $prompt_str .= "<br> &nbsp;&nbsp;($i_AdminJob_Announcement_Being $approvalUserName $i_AdminJob_Announcement_At $approvalTime $i_AdminJob_Announcement_Rejected)";
         }
        if ($reason != "")
        {
            $prompt_str .= "<br> &nbsp;&nbsp;(". $Lang['eComm']['Reason'] .": $reason)";
        }
    }
    else
    {
        $prompt_str = "";
    }

              if ($special_feature['announcement_approval'] && $legroup->hasAdminAllAnnounce($UserID))
              {
                 //$ladminjob = new libadminjob();
                  $approveUsers = $li->returnAnnouncementAdminUsers();
                  if (sizeof($approveUsers) != 0)
                      $select_approval = getSelectByArray($approveUsers,"name=AdminUserID onchange='$(\"#RecordStatus2\").attr(\"checked\",\"true\")'",$approvalUserID,1,1);
              }
	

			  
?>
     <script language="javascript">
$(function(){
	if(!$('#publicdisplay').attr("checked")){
		$('div#email_alert1_div').show();
	}else{
		<?php if($sys_custom['schoolNews']['enableSendingEmailToAllUser']){?>
		$('div#email_alert1_div').show();
		<?php }else{?>
		$('div#email_alert1_div').hide();
		$('input#email_alert1').removeAttr("checked");
		<?php }?>
	}
	
});
function compareDate(s1,s2)
{
        y1 = parseInt(s1.substring(0,4),10);
        y2 = parseInt(s2.substring(0,4),10);
        m1 = parseInt(s1.substring(5,7),10);
        m2 = parseInt(s2.substring(5,7),10);
        d1 = parseInt(s1.substring(8,10),10);
        d2 = parseInt(s2.substring(8,10),10);

        if (y1 > y2)
        {
                return 1;
        }
        else if (y1 < y2)
        {
                return -1;
        }
        else if (m1 > m2)
        {
                return 1;
        }
        else if (m1 < m2)
        {
                return -1;
        }
        else if (d1 > d2)
        {
                return 1;
        }
        else if (d1 < d2)
        {
                return -1;
        }
        return 0;


}

	function checkOption1(obj){
        for(i=0; i<obj.length; i++){
        	str = obj.options[i].value.replace(/^(\s)*/, '');
			str = str.replace(/(\s)*$/, '');
            if(str==''){
                    obj.options[i] = null;
            }
        }
	}
	
     function checkform(obj){
			var box = document.getElementById("GroupID");
             if(!check_text(obj.AnnouncementDate, "<?php echo $i_alert_pleasefillin.$i_AnnouncementDate; ?>.")) return false;
             if(!check_date(obj.AnnouncementDate, "<?php echo $i_invalid_date; ?>.")) return false;
             if(!check_text(obj.EndDate, "<?php echo $i_alert_pleasefillin.$i_EndDate; ?>.")) return false;
             if(!check_date(obj.EndDate, "<?php echo $i_invalid_date; ?>.")) return false;
             if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_AnnouncementTitle; ?>.")) return false;
             if(compareDate(obj.EndDate.value,obj.AnnouncementDate.value)<0) {alert ("<?=$i_con_msg_date_startend_wrong_alert?>"); return false;}

             <?
             if ($legroup->hasAdminAllAnnounce($UserID) && $legroup->AnnounceAllowed) {
             ?>
				checkOption1(box);
				checkOptionAll(box);
/*				if(box.options.length<=0)
				{
					alert("<?=$Lang['Group']['WarnAddGroup']?>");
					return false;
				}						 
 */
			<? } ?>
             Big5FileUploadHandler();
        
	        // create a list of files to be deleted
	        objDelFiles = document.getElementsByName('file2delete[]');
	        files = obj.deleted_files;
	        if(objDelFiles==null)return true;
	        x="";
	        for(i=0;i<objDelFiles.length;i++){
			        if(objDelFiles[i].checked==false){
				 		x+=objDelFiles[i].value+":";       
				    }
		    }
		    files.value = x.substr(0,x.length-1);
		    return true;
     }
function fileAttach(obj){
         aWin=newWindow("",1);
         temp = obj.target;
         obj.target="intranet_popup1";
         obj.action="more_attach.php";
         obj.submit();
         obj.target=temp;
         obj.action="edit_update.php";
}
function setRemoveFile(index,v){
	obj = document.getElementById('a_'+index);
	obj.style.textDecoration = v?"line-through":"";
}
     function add_field(){
	     
	     objTable = document.getElementById("upload_files");
	     attach_size = parseInt(document.form1.attachment_size.value)+1;
		 row = objTable.insertRow(-1);
	     cell = row.insertCell(0);
	     x='<input class=file type=file name="filea'+attach_size+'" size=40>';
	     x+='<input type=hidden name="hidden_userfile_name'+attach_size+'">';
	     cell.innerHTML = x;
		 document.form1.attachment_size.value = attach_size;
		 
	 }
	 function Big5FileUploadHandler() {
		 attach_size = parseInt(document.form1.attachment_size.value);
		 for(i=0;i<=attach_size;i++){
			 	objFile = eval('document.form1.filea'+i);
			 	objUserFile = eval('document.form1.hidden_userfile_name'+i);
			 	if(objFile!=null && objFile.value!='' && objUserFile!=null){
				 	var Ary = objFile.value.split('\\');
				 	objUserFile.value = Ary[Ary.length-1];
				}
		 }
		 return true;
	}
	
function SwitchPublic(isPublic)
{
	if(isPublic)
	{
		$("#AdminUserID").show();
		$("#GroupID").attr("disabled","disabled");
		$("#groupSelectionBox").hide();
		<?php if($sys_custom['schoolNews']['enableSendingEmailToAllUser']){?>
		$('div#email_alert1_div').show();
		<?php }else{?>
		$('input#email_alert1').removeAttr("checked");
		$('div#email_alert1_div').hide();
		<?php }?>
	}
	else
	{
		$("#AdminUserID").hide();
		$("#groupSelectionBox").show();	
		$("#GroupID").attr("disabled","");
		$('div#email_alert1_div').show();
	}
}	
function SwitchStatus(isPublish){
	if(isPublish){
		$('input#email_alert1').removeAttr("disabled");
		
	}else{
		$('input#email_alert1').attr("disabled","disabled");
	}
}
function ClickApprove(){
	$('input#email_alert1').removeAttr("disabled");
}
</script>

<br />
<form name="form1" action="edit_update.php" method="post" enctype="multipart/form-data" ONSUBMIT="return checkform(this)">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
			<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				 <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_AnnouncementDate?> <span class='tabletextrequire'>*</span></span></td>
					<td><?=$linterface->GET_DATE_PICKER("AnnouncementDate", $cStartDate)?></td>

				</tr>
                                
                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_AnnouncementEndDate?> <span class='tabletextrequire'>*</span></span></td>
					<td><?=$linterface->GET_DATE_PICKER("EndDate", $cEndDate)?></td>
				</tr>
				
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_AnnouncementTitle?> <span class='tabletextrequire'>*</span></span></td>
					<td><input name="Title" type="text" class="textboxtext" maxlength="100" value="<?=$cTitle?>"/></td>
				</tr>
				
				<tr valign="top"> 
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_AnnouncementDescription?> </span></td>					
                    <td>
                    <?
					include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
					$objHtmlEditor = new FCKeditor ( 'Description' , "100%", "320", "", "Basic2_withInsertImageFlash", $cDescription);
					$objHtmlEditor->Config['FlashImageInsertPath'] = $lfilesystem->returnFlashImageInsertPath($cfg['fck_image']['SchoolNews'], $id);
					$objHtmlEditor->Create();
						?>
                    <?//=$linterface->GET_TEXTAREA("Description",$cDescription);?>
                   </td>
				</tr>
				
				<tr valign="top"> 
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_AnnouncementCurrAttachment?> </span></td>					
                    <td>
						<table width="100%" border="0">
						<?php
							if ($li->Attachment != "")
								echo $li->displayAttachmentEdit("file2delete[]");
							else 
								echo "<tr><td colspan='2' class='tabletext'>$i_AnnouncementNoAttachment</td></tr>"; 
						?>
						</table>
						<table id='upload_files' border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<input type="file" class="file" name="filea1">
								<input type=hidden name="hidden_userfile_name1">
							</td>
						</tr>
						</table>
						<input type="button" value=" + " onClick="add_field()">
                    </td>
				</tr>
								
				<? /* ?>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['PublicStatus']?> </span></td>
					<td class="tabletext">
						<input type="radio" name="PublicStatus" value="1" id="PublicStatus1" <?=$cPublicStatus?"checked":""?>><label for="PublicStatus1"><?=$eComm['Public']?></label>
						<input type="radio" name="PublicStatus" value="0" id="PublicStatus0" <?=$cPublicStatus?"":"checked"?>><label for="PublicStatus0"><?=$eComm['Private']?></label>
					</td>
				</tr>
				<? */ ?>
				<!--                 Target -->
				  <?if($legroup->hasAdminAllAnnounce($UserID)){?>
				 <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=	$Lang['SchoolNews']['target']?> </span></td>
					<td class="tabletext">
					
		              <input type="radio" name="publicdisplay" id="publicdisplay" onClick="SwitchPublic(this.checked)" value="1" <?=$isPublicAnnounce_check?>> <label for="publicdisplay"><?=$i_Payment_All_Users?></label>
		               <input type="radio" name="publicdisplay" id="specific" onClick="SwitchPublic(publicdisplay.checked)" value="0" <?=$isPublicAnnounce_check_spec ?>> <label for="specific"><?=$Lang['SchoolNews']['SpecificGroup']?></label>
		             
		              <table>
						 <tr>
		              		<td colspan='2'>
							<?=$public_row?>
							</td>
						</tr>
					</table>
					</td>
					
				</tr>
				 <?}?>
<!-- 				status -->
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $i_AnnouncementRecordStatus; ?> </span></td>
					<td class="tabletext">
						<input type="radio" name="RecordStatus" value="1" id="RecordStatus1" <?php echo $RecordStatus1; ?> onClick="SwitchStatus(this.checked)"> <label for="RecordStatus1"><?php echo $i_status_publish; ?></label> 
						<input type="radio" name="RecordStatus" value="0" id="RecordStatus0" <?php echo $RecordStatus0; ?> onClick="SwitchStatus(RecordStatus1.checked)"> <label for="RecordStatus0"><?php echo $i_status_pending; ?></label>
						<? if (sizeof($approveUsers)!=0)
						{
						?>
						<br><span id='AdminUserID'><input type="radio" name="RecordStatus" value="2" id="RecordStatus2" <?=$RecordStatus2?> onClick="ClickApprove()"><label for="RecordStatus2"><?=$Lang['eComm']['SelectApproval']?></label> <?=$select_approval?></span>
						<? } ?>
						<br />
						<div id="email_alert1_div">
		              	<input type="checkbox" name="email_alert" value="1" id="email_alert1"> <label for="email_alert1"><?php echo $i_AnnouncementAlert; ?></label>
		              	</div>
		              	<br>
					</td>
				</tr>

				
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['DisplayOnTop']?> </span></td>
					<td class="tabletext">
						<input type="checkbox" name="onTop" value="1" <?=$cOnTop?"checked":""?>>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?=$toolbar?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>	

     <input type="hidden" name="AnnouncementID" value="<?php echo $li->AnnouncementID; ?>">
     <input TYPE="hidden" NAME="CurrGroupID" value="<?=$GroupID?>">
     <? if ($newEdit)    {  ?>
     	<input type="hidden" name="MakeNew" value="1">
     <? } ?>
     <input type="hidden" name="attachment_size" value="1">
     <input type="hidden" name="deleted_files" value="">
</form>

<?php
print $linterface->FOCUS_ON_LOAD("form1.AnnouncementDate");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>