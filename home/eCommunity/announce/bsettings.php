<?php


# using: yat

###########################################
#
#	Date:	2010-12-21	YatWoon
#			- IP25 UI standard
#
###########################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);


$linterface = new interface_html();
$legroup = new libegroup($GroupID);

$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,2,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

$CurrentPage	= "Settings_AnnouncementSettings";
$MyGroupSelection= $legroup->getSelectAdminAnnounce("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);

for($i=0;$i<=10;$i++)
	$data[] = array($i, $i);
$IndexAnnounceNo_selection = getSelectByArray($data, "name='IndexAnnounceNo'", $legroup->IndexAnnounceNo, 0, 1);	


### Title ###
$TAGS_OBJ[] = array($eComm['Management'],"settings.php?GroupID=$GroupID", 0);
$TAGS_OBJ[] = array($i_general_BasicSettings,"bsettings.php?GroupID=$GroupID", 1);
$title = "
	<table width='100%' border='0' cellpadding='0' cellspacing='0' valign=\"bottom\">
		<tr>
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$lu->TitleDisplay."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ_RIGHT[] = array($title,"",0);
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>

<script language="javascript">
<!--
function EditInfo()
{
	$('#EditBtn').attr('style', 'visibility: hidden'); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', '');
	
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
}		

function ResetInfo()
{
	document.form1.reset();
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
}
//-->
</script>

<form name="form1" method="post" action="bsettings_update.php">

<div class="table_board">

<div class="table_row_tool row_content_tool">
	<?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
</div>
<p class="spacer"></p>
<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$eDiscipline['SettingName']?> </span>-</em>
	<p class="spacer"></p>
</div>

<table class="form_table_v30">
<tr valign='top'>
	<td class="field_title" nowrap><?=$Lang['eComm']['AnnouncementNum']?></td>
	<td>
	<span class="Edit_Hide"><?=$legroup->IndexAnnounceNo?></span>
	<span class="Edit_Show" style="display:none"><?=$IndexAnnounceNo_selection?></span></td>
</tr>
<? /* ?>
<tr valign='top'>
	<td class="field_title" nowrap><?=$Lang['eComm']['AllowDeleteOthersRecords']?></td>
	<td> 
	<span class="Edit_Hide"><?=$legroup->AllowDeleteOthersRecords ? $i_general_yes:$i_general_no?></span>
	<span class="Edit_Show" style="display:none">
	<input type="radio" name="AllowDeleteOthersRecords" value="1" <?=$legroup->AllowDeleteOthersRecords ? "checked":"" ?> id="AllowDeleteOthersRecords1"> <label for="AllowDeleteOthersRecords1"><?=$i_general_yes?></label> 
	<input type="radio" name="AllowDeleteOthersRecords" value="0" <?=$legroup->AllowDeleteOthersRecords ? "":"checked" ?> id="AllowDeleteOthersRecords0"> <label for="AllowDeleteOthersRecords0"><?=$i_general_no?></label>
	</span></td>
</tr>
<? */ ?>

</table>           

<div class="edit_bottom_v30">
	<p class="spacer"></p>
    <?= $linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'") ?>
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
	<p class="spacer"></p>
</div>


</div>

<input type="hidden" name="GroupID" value="<?=$GroupID?>">
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>