<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libgroup.php");
include("../../../includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$lgroup = new libgroup($GroupID);

if ($GroupID =="" || $lgroup->hasAdminAllAnnounce($UserID) || $lgroup->hasAdminInternalAnnounce($UserID))
{

    if ($lgroup->hasAdminAllAnnounce($UserID))
    {
        $conds = "";
		$deletemsg="msg=delete";
    }
    else
    {
        $conds = " AND Internal = 1";
		$deletemsg="xmsg=3";
    }

    # Grab announcement can be deleted by this user
    $originalList = implode(",", $AnnouncementID);
    $sql = "SELECT AnnouncementID FROM INTRANET_ANNOUNCEMENT WHERE AnnouncementID IN ($originalList) AND OwnerGroupID = '$GroupID' $conds";

    $result = $lgroup->returnVector($sql);
    if (sizeof($result)!=0)
    {
    $list = implode(",",$result);

         $sql = "DELETE FROM INTRANET_GROUPANNOUNCEMENT WHERE AnnouncementID IN ($list)";
         $lgroup->db_db_query($sql);

         $sql = "DELETE FROM INTRANET_ANNOUNCEMENT_APPROVAL WHERE AnnouncementID IN ($list)";
         $lgroup->db_db_query($sql);

         # Get attachment path
         $sql = "SELECT AnnouncementID, Attachment FROM INTRANET_ANNOUNCEMENT WHERE AnnouncementID IN ($list)";
         $array = $lgroup->returnArray($sql,2);

         # Delete attachment
         $lf = new libfilesystem();
         for ($i=0; $i<sizeof($array); $i++)
         {
              if ($array[$i][1]==null || $array[$i][1]=="")
              {}
              else {
                   $dir2del = "$file_path/file/announcement/".$array[$i][1].$array[$i][0];
                   $lf->lfs_remove($dir2del);
              }
         }

         $sql = "DELETE FROM INTRANET_ANNOUNCEMENT WHERE AnnouncementID IN ($list)";
         $lgroup->db_db_query($sql);
    }

         header("Location: settings.php?GroupID=$GroupID&filter=$filter&order=$order&field=$field&$deletemsg");

}
else
{
    //header("Location: ../close.php");
	header ("Location: settings.php?filter=1&xmsg=2&GroupID=1");	
}
intranet_closedb();
?>