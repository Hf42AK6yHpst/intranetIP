<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");


intranet_opendb();

$AnnouncementID = IntegerSafe($AnnouncementID);

$lo = new libannounce($AnnouncementID);

## Check access right
/*
$hasAccessRight = $lo->hasAccessRight($UserID);
if(!$hasAccessRight)
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
*/
$read = $lo->returnReadCount();
$total = $lo->returnTargetCount();
$unread = $total - $read;

if ($read != 0)
{
    $read = "<a class='tablelink' href='read.php?AnnouncementID=$AnnouncementID&type=1'>$read";
    $read .= " <img src='{$image_path}/{$LAYOUT_SKIN}/icon_view.gif' alt='$i_AnnouncementViewReadList' border='0'></a>";
}
else
{
    $read_navigation = "$i_AnnouncementReadList";
}
if ($unread != 0)
{
    $unread = "<a class='tablelink' href='read.php?AnnouncementID=$AnnouncementID&type=2'>$unread";
    $unread .= " <img src='{$image_path}/{$LAYOUT_SKIN}/icon_view.gif' alt='$i_AnnouncementViewUnreadList' border='0'></a>";
}
else
{
    $unread_navigation = "$i_AnnouncementUnreadList";
}

$MODULE_OBJ['title'] = $i_AnnouncementViewReadStatus;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
?>

<script language="Javascript" src='/templates/tooltip.js'></script>
<style type="text/css">
#ToolTip{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>

<script language="JavaScript">
isToolTip = true;
</script>
<div id="ToolTip"></div>

<br />   

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_AnnouncementTitle?> </span></td>
					<td class="tabletext"><?=$lo->Title?></td>
				</tr>
                                
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_AnnouncementDate?> </span></td>
					<td class="tabletext"><?=$lo->AnnouncementDate?></td>
				</tr>
                                
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_AnnouncementEndDate?> </span></td>
					<td class="tabletext"><?=$lo->EndDate?></td>
				</tr>
                                
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_AnnouncementReadCount?> </span></td>
					<td class="tabletext"><?=$read?></td>
				</tr>
                                
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_AnnouncementUnreadCount?> </span></td>
					<td class="tabletext"><?=$unread?></td>
				</tr>
                             
				</table>
			</td>
                </tr>
                </table>
	</td>
</tr>

<? if($type) 
{ 
	$otype = $type;
        if ($type == 1)
        {
            $title = $i_AnnouncementReadList;
            $list = $lo->returnReadList();
            if ($unread != 0)
                $alt_navigation = "<a href=?AnnouncementID=$AnnouncementID&type=2>$i_AnnouncementUnreadList</a>";
            else
                $alt_navigation = "$i_AnnouncementUnreadList";
        }
        else
        {
            $title = $i_AnnonucementUnreadList;
            $list = $lo->returnUnreadList();
            if ($read != 0)
                $alt_navigation = "<a href=?AnnouncementID=$AnnouncementID&type=1>$i_AnnouncementReadList</a>";
            else
                $alt_navigation = "$i_AnnouncementReadList";
        }

        $x = "<table width='100%' border='0' cellpadding='2' cellspacing='0'>\n";
        $x .= "<tr>";
        $x .= "<td width='80%' class='tablebluetop tabletopnolink'>Name</td>";
        $x .= "<td width='20%' class='tablebluetop tabletopnolink'>Type</td>";
        $x .= "</tr>";
        
        for ($i=0; $i<sizeof($list); $i++)
        {
             list($id, $name, $type) = $list[$i];
             
             if($type==3)
             {
		$lfamily = new libfamily();
		$tooltipQ = makeTooltip($lfamily->displayChildrenList2007a($id));
                $name = "<a href='#' $tooltipQ class='tablebluelink'>$name <img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex_group.gif' alt='$i_general_show_child' border='0'></a>";
             }
		
                $x .= "<tr class='tablebluerow". ( $i%2 + 1 )."'>\n";
		$x .= "<td class='tabletext'>$name </td>";
		$x .= "<td class='tabletext'>". ($type==1? $i_identity_teachstaff : ($type==2? $i_identity_student : $i_identity_parent));

                $x .= "</td>";
             $x .= "</tr>\n";
        }
        $x .= "</table>\n";

?>
<tr>
	<td colspan="2" align="center">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
	</table>
        </td>
</tr>

<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                <tr valign="top">
                                	<td><?= $linterface->GET_NAVIGATION2( ($otype==1) ? $i_AnnouncementReadList : $i_AnnonucementUnreadList ) ?></td>
                                </tr>
                                <tr>
                                	<td><?=$x?></td>
				</tr>
                                
				</table>
			</td>
		</tr>
                </table>
	</td>
</tr>        
<? } ?>

<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();", "close_btn") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>
<br />

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
