<?php
# using: yat

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$linterface = new interface_html();
$legroup = new libegroup($GroupID);
$lgroup = new libgroup($GroupID);

### START check user access rights ###
$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,2,2)  || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/index.php?GroupID=$GroupID");
	exit;
}

### END check user access rights ###

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$CurrentPage	= "Settings_AnnouncementSettings";
$MyGroupSelection= $legroup->getSelectAdminAnnounce("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);

### Title ###
$TAGS_OBJ[] = array($eComm['Management'],"settings.php?GroupID=$GroupID", 1);
$TAGS_OBJ[] = array($i_general_BasicSettings,"bsettings.php?GroupID=$GroupID", 0);
$title = "
	<table width='50%' border='0' cellpadding='0' cellspacing='0' valign=\"bottom\">
		<tr>
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$lgroup->Title."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ_RIGHT[] = array($title,"",0);
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


$keyword = trim($keyword);
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        case 5: $field = 5; break;
        default: $field = 0; break;
}

$order = ($order == 1) ? 1 : 0;
if($filter == "") $filter = 1;

$pstatus_str = (isset($pstatus) && $pstatus!='-1') ? " and a.PublicStatus='$pstatus' " : "";	

# TABLE SQL
$user_field = getNameFieldWithClassNumberByLang("c.");
if ($special_feature['announcement_approval'] && $filter == 1)
{
    $table_ext = "LEFT OUTER JOIN INTRANET_ANNOUNCEMENT_APPROVAL as approval ON a.AnnouncementID = approval.AnnouncementID";
    $field_ext = "IF(approval.AnnouncementID IS NULL,'$i_AdminJob_Announcement_NoApproval','$i_AdminJob_Announcement_NeedApproval') AS Approval,";
}

$checkbox_str = "CONCAT('<input type=\"checkbox\" name=\"AnnouncementID[]\" value=\"', a.AnnouncementID ,'\">')";

if($legroup->AllowDeleteOthersAnnouncement)
{
	$checkbox_sql = $checkbox_str;
}
else
{
	$checkbox_sql = "if(a.UserID=".$UserID.", $checkbox_str, ' ')";
}
$sql  = "
	SELECT
			concat(
			if(a.PublicStatus=0, concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_private.gif border=0 align=absmiddle>'), ''),
			if(a.onTop=1, concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_ontop.gif border=0 align=absmiddle>'), '')
			),
            DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d'),
            DATE_FORMAT(a.EndDate, '%Y-%m-%d'),
            CONCAT('<a class=\"tablelink\" href=\"edit.php?AnnouncementID[]=', a.AnnouncementID, '\">', a.Title, '</a>'),
            CONCAT('<a href=\"javascript:showRead(',a.AnnouncementID,')\"><img src=\"$image_path/$LAYOUT_SKIN/eOffice/icon_resultview.gif\" border=\"0\" alt=\"$i_AnnouncementViewReadStatus\"></a>'),
            IF (a.UserID IS NOT NULL OR a.UserID != 0,$user_field,'$i_AnnouncementNoAnnouncer'),
            $field_ext
            a.DateModified,
            $checkbox_sql
    FROM
            INTRANET_ANNOUNCEMENT as a 
            LEFT OUTER JOIN INTRANET_USER as c ON c.UserID = a.UserID
            $table_ext
    WHERE
            (a.Title like '%$keyword%') AND
            a.RecordStatus = '$filter' AND
            a.OwnerGroupID = '$GroupID'
            $pstatus_str
    ";
    

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.AnnouncementDate","a.EndDate", "a.Title","$user_field","Approval","a.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;
if ($special_feature['announcement_approval'] && $filter == 1)
    $li->no_col++;
$li->IsColOff = 2;
$li->title = $i_adminmenu_announcement;

// TABLE COLUMN		
$li->column_list .= "<td class='tabletop tabletopnolink'>#</td>\n";
$li->column_list .= "<td width='18' class='tabletop tabletopnolink'>&nbsp;</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(0, $i_AnnouncementDate)."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(1, $i_AnnouncementEndDate)."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(2, $i_AnnouncementTitle)."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>&nbsp;</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(3, $i_AnnouncementOwner)."</td>\n";
if ($special_feature['announcement_approval'] && $filter == 1)
{
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(4, $i_AdminJob_Announcement_NeedApproval)."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(5, $i_AnnouncementDateModified)."</td>\n";
}
else
{
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(4, $i_AnnouncementDateModified)."</td>\n";
}
$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("AnnouncementID[]")."</td>\n";

### Button
$AddBtn 	= "<a href=\"javascript:checkNew('new.php?GroupID=$GroupID')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'AnnouncementID[]','edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'AnnouncementID[]','remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_delete . "</a>";

$searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
$searchTag 	.= "<td><select name='filter' onChange='this.form.pageNo.value=1;this.form.submit();'>\n";
$searchTag 	.= "<option value='0' ".(($filter==0)?"selected":"").">$i_status_pending</option>\n";
$searchTag 	.= "<option value='1' ".(($filter==1)?"selected":"").">$i_status_publish</option>\n";
if ($special_feature['announcement_approval'])
{
	$searchTag .= "<option value='2' ".(($filter==2)?"selected":"").">$i_status_waiting</option>\n";
	$searchTag .= "<option value='3' ".(($filter==3)?"selected":"").">$i_status_rejected</option>\n";
}
$searchTag 	.= "</select>\n";

/*
$searchTag 	.= "&nbsp;<select name='pstatus' onChange='this.form.submit();'>\n";
$searchTag 	.= "<option value='-1'>". $eComm['Public'] ." / " . $eComm['Private'] ."</option>\n";
$searchTag 	.= "<option value='1' ".(($pstatus==1)?"selected":"").">". $eComm['Public'] ."</option>\n";
$searchTag 	.= "<option value='0' ".(($pstatus==0 && $pstatus!="")?"selected":"").">". $eComm['Private'] ."</option>\n";
$searchTag 	.= "</select>\n";
*/

$searchTag 	.= "&nbsp;<input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
$searchTag 	.= "</tr></table>";


	#warning massage
	switch ($xmsg)
	{
		case 1: $warnmsg=$Lang['eComm']['NoPublicAnnounceRightEdit']; break;
		case 2: $warnmsg=$Lang['eComm']['NoPublicAnnounceRightDelete']; break;
		case 3: $warnmsg=$Lang['eComm']['DeleteInternalOnly']; break;
	}
?>
<SCRIPT LANGUAGE=Javascript>
function showRead(id)
{
         newWindow('read.php?AnnouncementID='+id,1);
}

</SCRIPT>

<br />

<form name="form1" method="get" action="settings.php">
<input type="hidden" name="GroupID" value="<?=$GroupID?>">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
                                        	<table border="0" cellspacing="0" cellpadding="2">
                                            	<tr>
                                              		<td><p><?=$AddBtn?></p></td>
                                            	</tr>
                                          	</table>
					</td>
                                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg,$warnmsg);?></td>
				</tr>

				<tr>
                                	<td class="tabletext">
                                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                	<td class="tabletext" nowrap><?=$select_status?></td>
                                                        <td width="100%" align="left" class="tabletext"><?=$searchTag?></td>
						</tr>
                                                </table>
					</td>
					<td align="right" valign="bottom" height="28">
						<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
							<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
								<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td nowrap><?=$editBtn?></td>
                                    <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
                                    <td nowrap><?=$delBtn?></td>
								</tr>
        							</table>
							</td>
        						<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
						</tr>
						</table>
					</td>
				</tr>
                                <tr>
                                	<td colspan="2">
						<?=$li->display();?>
					</td>
				</tr>
                                
                                
				</table>
			</td>
		</tr>
                </table>
        </td>
</tr>
</table>
<br />

<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();

?>
