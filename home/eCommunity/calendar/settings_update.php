<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);


$lg = new libgroup($GroupID);

if($createCal)
{
	$calTitle = $lg->Title . " ( " . $eComm['Group'] ." )";
	// create calendar
	$sql = "insert into CALENDAR_CALENDAR (Owner, Name, Description) values ('$UserID', '$calTitle', '$calTitle')";
	$lg->db_db_query($sql);
	$CalID = $lg->db_insert_id();
	
	//update INTRANET_GROUP CalID field
	$sql = "UPDATE INTRANET_GROUP SET CalID = '$CalID' WHERE GroupID = '$GroupID'";
	$lg->db_db_query($sql);
	
	//insert calendar user
	$sql = "select UserID from INTRANET_USERGROUP where GroupID='$GroupID'";
	$row = $lg->returnArray($sql, 1);
	for($i=0;$i<sizeof($row);$i++)
	{
		$this_userid = $row[$i]['UserID'];
		if(!trim($this_userid))	continue;
		
		$sql = "insert into CALENDAR_CALENDAR_VIEWER (CalID, UserID, GroupID, GroupType, Access, Color) values ('$CalID', '$this_userid', '$GroupID', 'E', 'R', 'FFFFFF')";
		$lg->db_db_query($sql);
	}
	
	// set myself as admin
	$sql = "update CALENDAR_CALENDAR_VIEWER set Access='A' where GroupID='$GroupID' and GroupType='E' and UserID='$UserID'";
	$lg->db_db_query($sql);
	
	$msg = "";

} else {
	
	####################################################
	## START update CALENDAR_CALENDAR_VIEWER permission status
	####################################################
	$CalID = $lg->CalID;
	
	#reset all the member have Read permission only
	$sql = "update CALENDAR_CALENDAR_VIEWER set Access='R' where CalID='$CalID'";
	$lg->db_db_query($sql);
	
	#add Admin permission	
	$t = implode(",",$target);
	$sql = "update CALENDAR_CALENDAR_VIEWER set Access='A' where CalID='$CalID' and UserID IN ($t)";
	$lg->db_db_query($sql);
	####################################################
	## END update CALENDAR_CALENDAR_VIEWER permission status
	####################################################
	
	$sql = "UPDATE INTRANET_GROUP SET CalDisplayIndex = '$CalDisplayIndex', CalPublicStatus='$CalPublicStatus' WHERE GroupID = '$GroupID'";
	$lg->db_db_query($sql);
	$msg = "update";
}

intranet_closedb();	
header("Location: settings.php?GroupID=$GroupID&msg=$msg");

?>