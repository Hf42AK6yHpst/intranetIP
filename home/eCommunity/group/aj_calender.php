<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
//include_once($PATH_WRT_ROOT."includes/libcalevent{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcycle.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$li = new libcalevent2007($ts,$v);
$legroup 	= new libegroup($GroupID);

$x = "
		<table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
		<tr>
			<td align=\"left\" valign=\"top\" >
			<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
			<tr>
				<td width=\"12\"><p>".$li->displayPrevMonth()."</p></td>
				<td align=\"center\" class=\"indexcalendarmonth\">".$li->displayMonthText()."</td>
				<td width=\"12\"><p>".$li->displayNextMonth()."</p></td>
			</tr>
			</table>
			</td>
		</tr>		
		
		<tr height=\"100%\" valign=\"top\"> 
			<td>						
			<span id=\"CalContentDiv\"  >".$li->displayeCommCalendar(date("m", $ts), date("Y", $ts), $legroup->CalID)."</span>							
			</td>
		</tr>
		
		
		</table>
		";		

		
echo convert2unicode($x, 1);	

intranet_closedb();
	
?>

