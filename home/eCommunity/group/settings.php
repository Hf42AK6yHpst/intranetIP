<?php
## Using By : yat

#############################################
##	Modification Log:
#
#	Date:	2010-11-15 [YatWoon]
#			if user without "basic settings" admin right, may direct to other settings page 
#
##	2010-01-04: Max (200912311437)
##	- Add "Chat room" settings
##	- Fix the checking of !(substr($legroup->AdminAccessRight,1,1) to !(substr($legroup->AdminAccessRight,0,1)
##
#############################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

intranet_auth();
intranet_opendb();
define("FUNC_ACCESS_CHATROOM_BIT_POS", 1);

$GroupID = IntegerSafe($GroupID);

$linterface = new interface_html();
$legroup = new libegroup($GroupID);

$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,0,1) || $legroup->AdminAccessRight=='ALL'))
{
	# redirect to other settings page if has other admin rights
	if ($legroup->hasAdminInternalAnnounce($UserID) || $legroup->hasAdminAllAnnounce($UserID))
	{
		header("Location: ../announce/settings.php?GroupID=".$GroupID);
		exit;
	}
	if ($legroup->hasAdminFiles($UserID))
	{
		header("Location: ../files/settings_folder.php?GroupID=".$GroupID);
		exit;
	}
	if ($legroup->hasAdminBulletin($UserID))
	{
		header("Location: ../bulletin/setting.php?GroupID=".$GroupID);
		exit;
	}
	if ($legroup->hasAdminMemberList($UserID))
	{
		header("Location: ../members/setting.php?GroupID=".$GroupID);
		exit;
	}
	
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

$CurrentPage	= "Settings_GroupSettings";
$MyGroupSelection= $legroup->getSelectAdminBasicInfo("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);

### Group Logo ###
$logo_link = "{$image_path}/{$LAYOUT_SKIN}/ecomm/sample_group_logo.gif";
$logo_delete = "";
if (is_file($intranet_root.$legroup->GroupLogoLink))
{
    $logo_link = $legroup->GroupLogoLink;
    $logo_delete = "<a href=\"javascript:deleteLogo()\" class=\"tablelink\"> ". $eComm['GroupLogoDelete'] ."</a>";
}

### Title ###
$title = "
		<table width='100%' height='28' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' nowrap> <span class='contenttitle'>". $eComm['GroupSettings'] ."</span></td>
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick=\"window.location.href='../group/index.php?GroupID=$GroupID'\" style='cursor:pointer' class='tabletool'> {$MyGroupSelection}</td>
		</tr>
	</table>
";

### Chat Room Function Access ###
# Group tools settings
# Field : FunctionAccess
# - Bit-1 (LSB) : timetable
# - Bit-2 : chat
# - Bit-3 : bulletin
# - Bit-4 : shared links
# - Bit-5 : shared files
# - Bit-6 : question bank
# - Bit-7 : Photo Album
# - Bit-8 (MSB) : Survey
define("FUNC_ACCESS_CHATROOM_BIT_POS", 1);
//echo "This is used for debug<br>";
//echo $legroup->isAccessTool(FUNC_ACCESS_CHATROOM_BIT_POS)?"7checked":"";

$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<script language="javascript">
function deleteLogo() {
	if(confirm("<?=$eComm['GroupLogoDelete']?>?")){
		window.location = "logo_delete.php?GroupID=<?=$GroupID?>"
	}
}
</script>

<br />   
<form name="form1" method="post" action="settings_update.php" enctype="multipart/form-data">
<input type="hidden" name="GroupID" value="<?=$GroupID?>">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align='right' colspan="2"><?=$linterface->GET_SYS_MSG($msg,$xmsg);?></td>
</tr>
<tr>
	<td>
                <table width="90%" border="0" cellspacing="0" cellpadding="4" align="center">
                <tr> 
                	<td valign="top">
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['Group']?></span></td>
					<td class="tabletext"><?=$legroup->TitleDisplay?></td>
				</tr>
				
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['GroupLogo']?></span></td>
					<td class="tabletextremark">
						<input name="GropLgoo" type="file" class="file" maxlength="255"/><br />
	    				<?=$eComm['GroupLogo_PhotoGuide']?>
					</td>
				</tr>
				
				<!-- Status -->
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['Status']?> </span></td>
					<td class="tabletext">
						<input type="radio" name="PublicStatus" value="1" id="status1" <?=$legroup->PublicStatus?"checked":""?>> <label for="status1"><?=$eComm['Public']?></label> 
						<input type="radio" name="PublicStatus" value="0" id="status0" <?=$legroup->PublicStatus?"":"checked"?>> <label for="status0"><?=$eComm['Private']?></label>
					</td>
				</tr>
				
				<!-- Chat Room -->
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['chatroom']?> </span></td>
					<td class="tabletext">
						<input type="radio" name="ChatRoom" value="1" id="chat1" <?=$legroup->isAccessTool(FUNC_ACCESS_CHATROOM_BIT_POS)?"checked":""?>> <label for="chat1"><?=$i_general_enabled?></label> 
						<input type="radio" name="ChatRoom" value="0" id="chat2" <?=$legroup->isAccessTool(FUNC_ACCESS_CHATROOM_BIT_POS)?"":"checked"?>> <label for="chat2"><?=$i_general_disabled?></label>
					</td>
				</tr>
			</table>
			</td>
						<td>
						<img src="<?=$logo_link?>" width="225" height="170"><br><?=$logo_delete?>
						</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit") ?>
	        <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
		</tr>
	    </table>                                
	</td>
</tr>
</table>
<input type="hidden" name="FuncAccessValue_ChatRoom" id="FuncAccessValue_ChatRoom" value="<?=FUNC_ACCESS_CHATROOM_BIT_POS?>"/>                 
</form>

<?php
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.GropLgoo");
$linterface->LAYOUT_STOP();
?>