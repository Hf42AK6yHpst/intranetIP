<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$legroup = new libegroup($GroupID);
$lf = new libfilesystem();
$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,0,1)))
{
	header("Location: ../settings/index.php?GroupID=$GroupID");
	exit;
}

$path = $intranet_root.$legroup->GroupLogoLink;
if (is_file($path) && strtoupper($lf->file_ext($legroup->GroupLogoLink))!="")
{
    $lf->file_remove($path);
}

//$sql = "UPDATE INTRANET_USER SET PhotoLink = '' WHERE UserID = '$UserID'";
//$legroup->db_db_query($sql);
header("Location: settings.php?GroupID=$GroupID&msg=photo_delete");

?>