<?php
## Using By : yat

#############################################
##	Modification Log:
##
##	Date:	2010-04-12 [YatWoon]
##	if group logo with invalid file format, display another error msg instead of "Record updated".
##
##	Date:	2010-04-08 [YatWoon]
##	comment the update statement of Group Logo, but need to set the value of $legroup->GroupLogoLink 
##	due to there is a  new function UPDATE_GROUP_TO_DB() call at the end
##
##	2010-01-04: YatWoon (20100116)
##	- Modified to update enable chatroom
##
##	2010-01-04: Max (200912311437)
##	- Modified to update enable chatroom
##
#############################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

intranet_auth();
intranet_opendb();

# compose string for SQL updating FunctionAccess of chat room
define("FUNC_ACCESS_CHATROOM_BIT_POS", 1);

$GroupID = IntegerSafe($GroupID);
$PublicStatus = IntegerSafe($PublicStatus);

$legroup = new libegroup($GroupID);
if ($ChatRoom !== "") {
	if ($ChatRoom == 1) {
		$legroup->setFunctionAccess(libegroup::ENABLE, libegroup::FA_CHAT_BIT_POS);
	}
	else {
		$legroup->setFunctionAccess(libegroup::DISABLE, libegroup::FA_CHAT_BIT_POS);
	}
}

$li = new libdb();
$lo = new libfilesystem();

//group logo
$FileLocation = $_FILES['GropLgoo']['tmp_name'];
if($FileLocation)	
{
	$Folder = $intranet_root."/file/group/g".$GroupID;
	$FileName = $_FILES['GropLgoo']['name'];
	$ext = strtoupper($lo->file_ext($FileName));
	if ($ext == ".JPG" || $ext == ".GIF" || $ext == ".PNG")
	{
		$lo->folder_new($Folder);
		$target = "$Folder/GroupLogo$ext";
		$lo->lfs_copy($FileLocation, $target);
		$GroupLogoLink = "/file/group/g".$GroupID."/GroupLogo$ext";
		
		# 20100408 YatWoon: No need to update here since there is a new function UPDATE_GROUP_TO_DB() call at the end
		# but need to set the value of $legroup->GroupLogoLink
		//$sql = "UPDATE INTRANET_GROUP SET GroupLogoLink = '$GroupLogoLink' WHERE GroupID = '$GroupID'";
		//$li->db_db_query($sql) or die(mysql_error());
		$legroup->GroupLogoLink = $GroupLogoLink;
	}
	else
	{
		header("Location: settings.php?GroupID=$GroupID&msg=GroupLogo_PhotoWarning");
		exit;
	}
}

$legroup->setPublicStatus($PublicStatus);
$legroup->UPDATE_GROUP_TO_DB();


intranet_closedb();
header("Location: settings.php?GroupID=$GroupID&msg=update");
?>