<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libqb.php");
include_once("../../../includes/libgroup.php");

intranet_opendb();

$lg = new libgroup($GroupID);
if (!$lg->isAccessQB())
{
    header("Location: ../close.php");
}
if ($lg->hasAdminQB($UserID))
{
$lq = new libqb();

$engName = intranet_htmlspecialchars(trim($engName));
$chiName = intranet_htmlspecialchars(trim($chiName));
$displayOrder = intranet_htmlspecialchars(trim($displayOrder));

$fields_value = " '$engName'";
$fields_value .= ",'$chiName'";
$fields_value .= ",'$displayOrder'";
$fields_value .= ",'$GroupID'";

$sql = "INSERT INTO ".$lq->settingTable[$type]." (EngName, ChiName, DisplayOrder,RelatedGroupID) VALUES ($fields_value)";
$lq->db_db_query($sql);
//echo $sql;

header("Location: settings_cat.php?signal=1&GroupID=$GroupID");
}
else
{
    header ("Location: ../close.php");
}
?>