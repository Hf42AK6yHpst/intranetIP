<?php
include_once ("../../../includes/global.php");
include_once ("../../../includes/libdb.php");
include_once ("../../../includes/libqb.php");
include_once ("../../../includes/libemail.php");
include_once ("../../../includes/libsendmail.php");
include_once ("../../../includes/libgroup.php");
include_once ("../../../lang/email.php");

intranet_auth();
intranet_opendb();

$lgroup = new libgroup($GroupID);
if ($lgroup->hasAdminQB($UserID))
{


# Update score of provider
$ptsAward = $pts + 0;    // From HTML form
if ($qb_record_score && $ptsAward != 0)
{
    $sql_ext = ",Score = $ptsAward";
}
else
{
    $sql_ext = "";
}

$lsendmail = new libsendmail();
$li = new libqb();
$status_public = 1;

$list = implode(",",$QuestionID);

$sql = "UPDATE QB_QUESTION SET DateModified = now(), RecordStatus = $status_public $sql_ext WHERE QuestionID IN ($list)";
$li->db_db_query($sql);

# Send authorized mail
$sql = "SELECT DISTINCT a.OwnerIntranetID FROM QB_QUESTION as a, INTRANET_USER as b WHERE a.QuestionID IN ($list) AND b.UserID = a.OwnerIntranetID ORDER BY a.OwnerIntranetID";
$owners = $li->returnVector($sql);

$owner_list = implode(",",$owners);
$sql = "SELECT UserID, UserEmail, EnglishName FROM INTRANET_USER WHERE UserID IN ($owner_list) ORDER BY UserID";
$owner_info = $li->returnArray($sql,3);

$approval_title = qb_approve_mail_title();
$subject_name = $lgroup->Title;


for ($i=0; $i<sizeof($owners); $i++)
{
     list($id, $emailAddr, $name) = $owner_info[$i];
     if ($id != $owners[$i]) continue;

     # Grab information for mail text
     $sql = "SELECT QuestionCode, Score FROM QB_QUESTION WHERE OwnerIntranetID = $owners[$i] AND QuestionID IN ($list)";
     $qArray = $li->returnArray($sql,2);
     $qcodeList = "";
     $qDelimiter = "";
     $totalPts = 0;
     for ($j=0; $j<sizeof($qArray); $j++)
     {
          list ($qcode, $qscore) = $qArray[$j];
          $qscore += 0;
          $qcodeList .= "$qDelimiter $qcode";
          $qDelimiter = ",";
          if ($qb_record_score)
              $totalPts += $qscore;
     }
     $totalPts *= $qb_download_ratio;        # Multiply download ratio to award contributor

     # Send mail
     $body = qb_approve_mail_body($subject_name,$qcodeList,$totalPts);
     #$lsendmail->SendMail($emailAddr,$approval_title,$body,0);

     if ($qb_record_score)
     {
         $li->addUserScore($id,$totalPts);
     }
}

# Create Bulletin message for review
for ($i=0; $i<sizeof($QuestionID); $i++)
{
     $li->createReviewThread($QuestionID[$i]);
}


header ("Location: index.php?GroupID=$GroupID&type=$type&msg=2");
}
else   # Not Admin, close window
{
         header ("Location: ../close.php");
}
intranet_closedb();
?>