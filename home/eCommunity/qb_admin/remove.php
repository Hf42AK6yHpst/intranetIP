<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libgroup.php");
intranet_auth();
intranet_opendb();

$lgroup = new libgroup($GroupID);
if ($lgroup->hasAdminQB($UserID))
{

$li = new libdb();
$lf = new libfilesystem();

$qlist = implode(",",$QuestionID);
$sql = "SELECT OriginalFileEng, OriginalFileChi FROM QB_QUESTION WHERE QuestionID IN ($qlist)";
$result = $li->returnArray($sql,2);
for ($i=0; $i<sizeof($result); $i++)
{
     list($efile,$cfile) = $result;
     $epath = "$intranet_root/file/qb/g$GroupID/$efile";
     $cpath = "$intranet_root/file/qb/g$GroupID/$cfile";
     if ($efile!="" && is_file($epath))
     {
         $lf->file_remove($epath);
     }
     if ($cfile!="" && is_file($cpath))
     {
         $lf->file_remove($cpath);
     }
}
$sql = "DELETE FROM QB_QUESTION WHERE QuestionID IN ($qlist)";
$li->db_db_query($sql);

header("Location: index.php?GroupID=$GroupID&type=$type&msg=3");
}
else   # Not Admin, close window
{
         header ("Location: ../close.php");
}
intranet_closedb();
?>