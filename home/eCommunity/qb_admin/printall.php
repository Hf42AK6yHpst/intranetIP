<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libqb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
$body_tags = "bgcolor=#FFFFFF text=#000000 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0";
include_once("../../../templates/fileheader.php");

$lg = new libgroup($GroupID);
$lq = new libqb();

//************************************ START OF HTML DISPLAY ************************************
?>
<?php
echo $i_QB_AllQuestions ."[".$lg->Title."] <br>\n";
echo $lq->displayAll($GroupID);
?>
<?php
include_once("../../../templates/filefooter.php");
intranet_closedb();
?>