<?php
include_once ("../../../includes/global.php");
include_once ("../../../includes/libdb.php");
include_once ("../../../includes/libemail.php");
include_once ("../../../includes/libsendmail.php");
include_once ("../../../includes/libgroup.php");
include_once ("../../../lang/email.php");

intranet_auth();
intranet_opendb();

$lgroup = new libgroup($GroupID);
if ($lgroup->hasAdminQB($UserID))
{

$lsendmail = new libsendmail();
$li = new libdb();
$status_reject = 2;

//echo "Reject reason : $rejmsg";

$list = implode(",",$QuestionID);

$sql = "UPDATE QB_QUESTION SET RecordStatus = $status_reject, DateModified = now() WHERE QuestionID IN ($list)";
$li->db_db_query($sql);

# Send reject mail
$sql = "SELECT DISTINCT a.OwnerIntranetID FROM QB_QUESTION as a, INTRANET_USER as b WHERE a.QuestionID IN ($list) AND b.UserID = a.OwnerIntranetID ORDER BY a.OwnerIntranetID";
$owners = $li->returnVector($sql);

$owner_list = implode(",",$owners);
$sql = "SELECT UserID, UserEmail, EnglishName FROM INTRANET_USER WHERE UserID IN ($owner_list) ORDER BY UserID";
$owner_info = $li->returnArray($sql,3);

$reject_title = qb_reject_mail_title();
$subject_name = $lgroup->Title;

for ($i=0; $i<sizeof($owners); $i++)
{
     list($id, $emailAddr, $name) = $owner_info[$i];
     if ($id != $owners[$i]) continue;

     $sql = "SELECT QuestionCode FROM QB_QUESTION WHERE OwnerIntranetID = $owners[$i] AND QuestionID IN ($list)";
     $codes = $li->returnVector($sql);

     # Send mail
     $body = qb_reject_mail_body($subject_name,$qcodeList,$rejmsg);
     #$lsendmail->SendMail($emailAddr,$reject_title,$body,0);


}

header ("Location: index.php?GroupID=$GroupID&type=$type&msg=2");
}
else   # Not Admin, close window
{
         header ("Location: ../close.php");
}
intranet_closedb();
?>