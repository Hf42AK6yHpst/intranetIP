<?php
## Using By : 
#############################################
##	Modification Log:
#
#	Date:	2012-02-09 YatWoon
#			Improved: Keep track the last accessed group id
#
##	2010-01-05: Max (200912311437)
##	- redirect user to the eCommunity group index page the user do not have the function access right on chat room
#############################################
// Header for instructing browser to decode in UTF8
//header("Content-Type: text/html; charset=UTF-8");
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
intranet_auth();
intranet_opendb();

$lo = new libuser($UserID);
$legroup 		= new libegroup($GroupID);

$Groups = $lo->returnGroups();

$GroupIDs = Get_Array_By_Key($Groups,0);
### Check whether the user has the function access right on chat room
# Group tools settings
# Field : FunctionAccess
# - Bit-1 (LSB) : timetable
# - Bit-2 : chat
# - Bit-3 : bulletin
# - Bit-4 : shared links
# - Bit-5 : shared files
# - Bit-6 : question bank
# - Bit-7 : Photo Album
# - Bit-8 (MSB) : Survey
define("FUNC_ACCESS_CHATROOM_BIT_POS", 1);
if ($legroup->isAccessTool(FUNC_ACCESS_CHATROOM_BIT_POS) == 0 || !in_array($GroupID,$GroupIDs)) {
	header("Location: ../group/index.php?GroupID=".$GroupID);
	exit;
}

### keep track the last accessed Group ID
$legroup->UpdateLastAccess($GroupID);


$linterface 	= new interface_html();
$CurrentPage	= "MyGroups";
$li = new libgroup($GroupID);
# Groups (channels) available to the user

$GroupID = ($lo->IsGroup($GroupID)) ? $GroupID : $Groups[0][0];

# Currently selected group

$Title = Big5ToUnicode($li->TitleDisplay);


$temp = $intranet_session_language;
$intranet_session_language = "en";
$i_title = Big5ToUnicode($i_title);

$intranet_session_language = $temp;

if($_SESSION['UserType']!=USERTYPE_ALUMNI)
$MyGroupSelection= $li->getSelectFilesGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID);

$title = "
	<table width='100%' height='28' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' class='contenttitle' nowrap> <div onclick=\"window.location.href='{$PATH_WRT_ROOT}home/eCommunity/group/index.php?GroupID={$GroupID}'\" class='contenttitle GroupTitleLink'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/icon_egroup_school.gif' align='absmiddle'/>{$li->TitleDisplay}</div></td>
			<td align='right' width='70%'>".$li->displayStorage3()."</td>
			<td align='right' nowrap>{$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ[] = array($title,"");
// $MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
if($_SESSION['UserType']==USERTYPE_ALUMNI)
{
	$MODULE_OBJ['title'] = $legroup->TitleDisplay;
}
else
{
	$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
}
$linterface->LAYOUT_START();
?>

<form name="form1" method="post" action="search.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
        	<table  border="0" cellspacing="0" cellpadding="0">
        	<br>
        	<!--start-->
        	<tr>
		      <td width="12" height="25"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_01.gif" width="12" height="25"></td>
		      <td width="13" height="25"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_02.gif" width="13" height="25"></td>
		      <td height="25" align="right" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_02.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		          <tr>
		            <td align="left"><?=$ChatIcon?></td>
		            <td align="right"><a href="../group/index.php?GroupID=<?=$GroupID?>"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_off.gif" name="a_mini" width="16" height="16" border="0" id="a_mini" onMouseOver="MM_swapImage('a_mini','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_03b.gif" width="16" height="25" align="absmiddle"></td>
		          </tr>
		      </table></td>
		      <td width="18" height="25"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="18" height="25"></td>
		    </tr>
		    <tr>
		      <td width="12" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_05.gif" width="12" height="28"></td>
		      <td width="13" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_06.gif" width="13" height="28"></td>
		      <td height="28" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_07.gif"></td>
    		  <td width="18" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_08.gif" width="18" height="28"></td>
    		</tr>
    		
    		<tr>
      			<td width="12" valign="bottom" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_05.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_09b.gif" width="12" height="19"></td>
      			<td width="13" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_10.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_10.gif" width="13" height="200"></td>
      			<td bgcolor="#fffeed" width="730" align="center"><iframe width="640" height="352" frameborder="0" scrolling="no" src="chat.php?GroupID=<?php echo $GroupID ?>"></iframe></td>
      			<td width="18" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_12.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_12.gif" width="18" height="19"></td>
    			</tr>
    			<tr>
			      <td width="12" height="31"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="12" height="31"></td>
			      <td width="13" height="31"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_14.gif" width="13" height="31"></td>
			      <td height="31" align="right" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_15.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_15b.gif" width="170" height="31"></td>
			      <td width="18" height="31"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_16.gif" width="18" height="31"></td>
			    </tr>
			
			<!--end-->
            </table>
        </td>
</tr>
</table>
</form>

<?php

intranet_closedb();
$linterface->LAYOUT_STOP();

?>