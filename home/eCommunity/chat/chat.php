<?php
/*
 * 2017-05-24 (Carlos): Change split() to preg_split().
 */
// Header for instructing browser to decode in UTF8
header("Content-Type: text/html; charset=UTF-8");

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libgroup.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$lo = new libuser($UserID);

# Nickname
include_once("bannedChar.php");
$nickname = $lo->NickNameForChat();
for ($i=0; $i<sizeof($bannedNicknameChar); $i++) {
        $c = $bannedNicknameChar[$i];
        $nickname = str_replace($c, '', $nickname);
}

# Groups (channels) available to the user
$Groups = $lo->returnGroups();
$GroupID = ($lo->IsGroup($GroupID)) ? $GroupID : $Groups[0][0];

# Currently selected group
$li = new libgroup($GroupID);

# Chat server address
//$address = split("[:/]",$eclass_httppath);
$address = preg_split("/[\\:\\/]/",$eclass_httppath);
$address1 = $address[0];

# Generate the chat applet code and required parameters
$x  = '<applet id="eclass_chat" code="IRCApplet.class" archive="irc.jar,pixx.jar" width="640" height="400">';
$x .= '<param name="CABINETS" value="irc.cab,securedirc.cab,pixx.cab">';
$x .= '<!-- Nick Name / Backup Nickname -->';
$x .= '<param name="nick" value="'.$nickname.'">';
$x .= '<param name="alternatenick" value="'.$nickname.'???">';

$x .= '<!-- Chat Server -->';
$x .= '<param name="name" value="u'.$UserID.'">';
$x .= '<param name="userid" value="u'.$UserID.'">';
$x .= '<param name="host" value="'.$address1.'">';
$x .= '<param name="port" value="6667">';
$x .= '<param name="gui" value="pixx">';
$x .= '<param name="coding" value="2">';

$x .= '<!-- Channel List Control - Cannot Leave Any Channel -->';
$x .= '<param name="authorizedleavelist" value="none">';

$x .= '<!-- Available Command List - Cannot Use Any Command -->';
$x .= '<param name="authorizedcommandlist" value="none">';

$x .= '<!-- UI Modifications -->';
$x .= '<param name="style:linespacing" value="2">';
$x .= '<param name="pixx:showconnect" value="false">';
$x .= '<param name="pixx:showchanlist" value="false">';
$x .= '<param name="pixx:showabout" value="false">';
$x .= '<param name="pixx:showhelp" value="false">';
$x .= '<param name="pixx:nicklistwidth" value="150">';
$x .= '<param name="pixx:showclose" value="false">';
$x .= '<param name="pixx:showstatus" value="true">';
$x .= '<param name="pixx:automaticqueries" value="false">';
$x .= '<param name="pixx:showdock" value="false">';

$ChannelList = '';
for ($i=0; $i<sizeof($Groups); $i++)
{
        $ChatID = $Groups[$i][0];
        //$GroupName = Big5ToUnicode( $Groups[$i][1]);
        $GroupName = $Groups[$i][1];
        $ChatRoom = str_replace("-", "_", $GroupName);
        $ChatRoom = str_replace(" ", "_", $ChatRoom);

        $c = '#c'.$ChatID.'_'.$ChatRoom;

        if($GroupID == $ChatID) {
                $myChannel = $c;
        }

        $ChannelList .= '+'.$c;
}

$x .= '<!-- Channel List Control - Available Channel(s) -->';
$x .= '<param name="authorizedjoinlist" value="none'.$ChannelList.'">';

$x .= '<!-- Join the chatroom of the current group once connected -->';
$x .= '<param name="command1" value="join '.$myChannel.'">';

$x .= '</applet>';


$body_tags = 'topmargin="0" leftmargin="0"';
include_once("../../../templates/fileheader.php");
include_once("../tooltab.php");
?>

<?php echo $x; ?>


<?php
include("../../../templates/filefooter.php");
intranet_closedb();
?>
