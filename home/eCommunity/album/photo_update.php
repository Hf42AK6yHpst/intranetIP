<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libadminjob.php");
include_once("../../../includes/libalbum.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

global $file_path;

$album_id = IntegerSafe($album_id);
$group_id = IntegerSafe($group_id);

$li = new libdb();
$lf = new libfilesystem(); 
$l_ca = new libalbum($album_id);
$l_ca->db = $li->db;
$album_path = $l_ca->getAlbumPathFile($album_id);

for ($i=0; $i<sizeof($userfile); $i++)
{
	$filename = $userfile_hidden[$i];
	
	if (trim($filename) == "")
	{
		$filename = $userfile_name[$i];
	}

	$loc = $userfile[$i];	
	$des = $file_path.$album_path.stripslashes($filename);		

	$data = $l_ca->returnAlbumStorageQuota();
	$file_size = ceil(($userfile_size[$i])/1024);
	$used = $data[1];	
	$available = ($data[0]*1024) - $used;
	
	if ($loc<>"none" && $l_ca->checkImageFormat($userfile_name[$i]) && file_exists($loc) && $available>=$file_size)
	{		
		$is_exist_file = (file_exists($des) && trim($filename)!="");
        $lf->lfs_copy($loc, $des);        
              
		// insert into DB
		if (!$is_exist_file)
		{										
			$l_ca->addPhoto2Album($album_id, $album_photo, $filename, $photo_description[$i], $album_path, $file_size);			
			$l_ca->updateStorageQuota($file_size);			
		}
	}
}

$l_ca->updatePhotoOrder();
$l_ca->updateAlbumDate();
$l_ca->updateThumbnail();
$count = $l_ca->getNumberOfItems();

intranet_closedb();
header("Location: list_photo.php?album_id=$album_id&count=$count&GroupID=$group_id");
?>
