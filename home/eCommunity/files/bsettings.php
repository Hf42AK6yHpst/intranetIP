<?php
## Using By : 
#############################################
##	Modification Log:
##	2010-09-13: Marcus 
##	- Updated getSelectAdminShared to getSelectAdminBasicInfo, do not hide current group in selection even the sharing function of the group is disabled.
##	2010-01-05: Max (200912311437)
##	- Update the following setting method:
##	Allowed image types, Allowed file types, Allowed video types
##	o Disable  o ALL o [Textfile just like current]
##	If user select "Disable", then all the related function should be hidden in eCommunity.
##	- Add "Allowed website" option
##	o Disable o Enable
##	If user select "Disable", then all the related function should be hidden in eCommunity.
#############################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);


$linterface = new interface_html();
$legroup = new libegroup($GroupID);
$lu = new libgroup($GroupID);


$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,9,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if (!$legroup->hasAdminFiles($UserID))
{
	header("Location: ../group/?GroupID=$GroupID");
    exit();
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$CurrentPage	= "Settings_SharingSettings";
$MyGroupSelection= $legroup->getSelectAdminBasicInfo("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);

### Title ###
$TAGS_OBJ[] = array($eComm['Folder'],"settings_folder.php?GroupID=$GroupID", 0);
$TAGS_OBJ[] = array($eComm['File'],"settings_file.php?GroupID=$GroupID", 0);
$TAGS_OBJ[] = array($i_general_BasicSettings,"bsettings.php?GroupID=$GroupID", 1);
$title = "
	<table width='100%' border='0' cellpadding='0' cellspacing='0' valign=\"bottom\">
		<tr>
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$lu->TitleDisplay."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ_RIGHT[] = array($title,"",0);
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$s = "<select name='SharingLatestNo'>";
for($i=1;$i<=20;$i++)
	$s .= "<option value=$i ". ($legroup->SharingLatestNo==$i?"selected":"") .">$i</option>";
$s .= "</select>";


$s2 = "<select name='DefaultViewMode'>";
$s2 .= "<option value='0' ". ($legroup->DefaultViewMode==0?"selected":"") .">". $eComm['ViewMode_List'] ."</option>";
$s2 .= "<option value='4' ". ($legroup->DefaultViewMode==4?"selected":"") .">". $eComm['ViewMode_4t'] ."</option>";
$s2 .= "<option value='2' ". ($legroup->DefaultViewMode==2?"selected":"") .">". $eComm['ViewMode_2t'] ."</option>";
$s2 .= "<option value='1' ". ($legroup->DefaultViewMode==1?"selected":"") .">". $eComm['ViewMode_1t'] ."</option>";
$s2 .= "</select>";

?>

<SCRIPT LANGUAGE="Javascript">
<!--
var manualInputString = "<?=$Lang['eComm']['ManualInput']?>";
function checkForm(obj)
{
	if(!check_text(obj.AllowedImageTypes, "<?=$i_alert_pleasefillin?><?=$eComm['AllowedImageTypes']?>")) return false;
	if(!check_text(obj.AllowedFileTypes, "<?=$i_alert_pleasefillin?><?=$eComm['AllowedFileTypes']?>")) return false;
	if(!check_text(obj.AllowedVideoTypes, "<?=$i_alert_pleasefillin?><?=$eComm['AllowedVideoTypes']?>")) return false;
	
	// set if input field value == manualInputString to "" when submit
	if ($("#ImageTypeInput").val() == manualInputString) {$("#ImageTypeInput").val("");}
	if ($("#FileTypeInput").val() == manualInputString) {$("#FileTypeInput").val("");}
	if ($("#VideoTypeInput").val() == manualInputString) {$("#VideoTypeInput").val("");}
}
function setAllowTypes(ParMedia, ParTypes) {
	switch(ParMedia) {
		case "image":
			if		(ParTypes == "DISABLED")	{$("#ImageTypeDisable").attr("checked", "checked");}
			else if	(ParTypes == "ALL")			{$("#ImageTypeAll").attr("checked", "checked");}
			else								{
													$("#ImageTypeManual").attr("checked", "checked");
													$("#ImageTypeInput").val(ParTypes);
												}
			break;
		case "file":
			if		(ParTypes == "DISABLED")	{$("#FileTypeDisable").attr("checked", "checked");}
			else if	(ParTypes == "ALL")			{$("#FileTypeAll").attr("checked", "checked");}
			else								{
													$("#FileTypeManual").attr("checked", "checked");
													$("#FileTypeInput").val(ParTypes);
												}
			break;
		case "video":
			if		(ParTypes == "DISABLED")	{$("#VideoTypeDisable").attr("checked", "checked");}
			else if	(ParTypes == "ALL")			{$("#VideoTypeAll").attr("checked", "checked");}
			else								{
													$("#VideoTypeManual").attr("checked", "checked");
													$("#VideoTypeInput").val(ParTypes);
												}
			break;
		case "website":
			if		(ParTypes == 0)				{$("#WebsiteDisable").attr("checked", "checked");}
			else								{$("#WebsiteEnable").attr("checked", "checked");}
			break;
		default:
			break;
	}
}
$("document").ready(function(){
	var RetrievedImageType = "<?=$legroup->AllowedImageTypes?>";
	var RetrievedFileType = "<?=$legroup->AllowedFileTypes?>";
	var RetrievedVideoType = "<?=$legroup->AllowedVideoTypes?>";
	var RetrievedWebsiteAvailability = "<?=$legroup->AllowedWebsite?>";
	
	setAllowTypes("image", RetrievedImageType);
	setAllowTypes("file", RetrievedFileType);
	setAllowTypes("video", RetrievedVideoType);
	setAllowTypes("website", RetrievedWebsiteAvailability);
	
	
		
	// set the input field with preset text
	if ($("#ImageTypeInput").val() == "") {
		$("#ImageTypeInput").val(manualInputString);
	}
	if ($("#FileTypeInput").val() == "") {
		$("#FileTypeInput").val(manualInputString);
	}
	if ($("#VideoTypeInput").val() == "") {
		$("#VideoTypeInput").val(manualInputString);
	}
	
	// set focus on input field when focus on corresponding radio box
	$("#ImageTypeManual").focus(function() {
		$("#ImageTypeInput").focus();
	});
	$("#FileTypeManual").focus(function() {
		$("#FileTypeInput").focus();
	});
	$("#VideoTypeManual").focus(function() {
		$("#VideoTypeInput").focus();
	});
	
	// focus on input field and text == manualInputString, clear text
	$("#ImageTypeInput, #FileTypeInput, #VideoTypeInput").focus(
		function() {
			if ($(this).attr("id") == "ImageTypeInput") {
				$("#ImageTypeManual").attr("checked", "checked");
			}
			if ($(this).attr("id") == "FileTypeInput") {
				$("#FileTypeManual").attr("checked", "checked");
			}
			if ($(this).attr("id") == "VideoTypeInput") {
				$("#VideoTypeManual").attr("checked", "checked");
			}
			
			if ($(this).val() == manualInputString) {
				$(this).val("");
			}
		}
	);
	
	// blur on input field and text == "", fill text = manualInputString
	$("#ImageTypeInput, #FileTypeInput, #VideoTypeInput").blur(
		function() {
			if ($(this).val() == "") {
				$(this).val(manualInputString);
			}
		}
	);
});
//-->
</SCRIPT>
<style>

.bsettingInputField
{width: 68%; /*color: #606060;*/}

</style>
<br />   
<form name="form1" method="post" action="bsettings_update.php" onSubmit="return checkForm(this);">
<input type="hidden" name="GroupID" value="<?=$GroupID?>">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align='right' ><?=$linterface->GET_SYS_MSG($msg,$xmsg);?></td>
</tr>
<tr>
	<td>
                <table width="90%" border="0" cellspacing="0" cellpadding="4" align="center">
                <tr> 
                	<td valign="top">
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
						
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['Group']?></span></td>
							<td class="tabletext"><?=$legroup->TitleDisplay?></td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['LatestNumber']?></span></td>
							<td class="tabletextremark">
								<?=$s?>
							</td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['NeedApproval']?></span></td>
							<td class="tabletext">
								<input type="radio" name="needApproval" value="1" id="needApproval1" <?=$legroup->needApproval ? "checked":""?>> <label for="needApproval1"><?=$i_general_yes?></label>
								<input type="radio" name="needApproval" value="0" id="needApproval0" <?=$legroup->needApproval ? "":"checked"?>> <label for="needApproval0"><?=$i_general_no?></label>
							</td>
						</tr>
						
						<!-- Default View Mode at index page -->
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['DefaultViewModeIndexPage']?></span></td>
							<td class="tabletext">
								<?=$s2?>
							</td>
						</tr>
						
						<?/*<!-- Allow Image Types -->
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletextrequire2">#</span> <span class="tabletext"><?=$eComm['AllowedImageTypes']?></span> <span class='tabletextrequire'>*</span></td>
							<td class="tabletext"><input type="text" maxlength="200" name="AllowedImageTypes" class="textboxtext" value="<?=$legroup->AllowedImageTypes?>"></td>
						</tr>
						
						<!-- Allow File Types -->
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletextrequire2">#</span> <span class="tabletext"><?=$eComm['AllowedFileTypes']?></span> <span class='tabletextrequire'>*</span></td>
							<td class="tabletext"><input type="text" maxlength="200" name="AllowedFileTypes" class="textboxtext" value="<?=$legroup->AllowedFileTypes?>"></td>
						</tr>
						
						<!-- Allow Video Types -->
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletextrequire2">#</span> <span class="tabletext"><?=$eComm['AllowedVideoTypes']?></span> <span class='tabletextrequire'>*</span></td>
							<td class="tabletext"><input type="text" maxlength="200" name="AllowedVideoTypes" class="textboxtext" value="<?=$legroup->AllowedVideoTypes?>"></td>
						</tr>*/?>
						<!-- Allow Image Types -->
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletextrequire2">#</span> <span class="tabletext"><?=$eComm['AllowedImageTypes']?></span> <span class='tabletextrequire'>*</span></td>
							<td class="tabletext">
								<input type="radio" name="AllowedImageTypes" id="ImageTypeDisable" value="DISABLED" />
								<label for="ImageTypeDisable"><?=$i_general_disabled?></label>
								<input type="radio" name="AllowedImageTypes" id="ImageTypeAll" value="ALL" />
								<label for="ImageTypeAll"><?=$i_general_all?></label>
								<input type="radio" name="AllowedImageTypes" id="ImageTypeManual" value="MANUAL" />
								<input class="bsettingInputField" id="ImageTypeInput" name="ImageTypeInput" value="" />
							</td>
						</tr>
						
						<!-- Allow File Types -->
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletextrequire2">#</span> <span class="tabletext"><?=$eComm['AllowedFileTypes']?></span> <span class='tabletextrequire'>*</span></td>
							<td class="tabletext">
								<input type="radio" name="AllowedFileTypes" id="FileTypeDisable" value="DISABLED" />
								<label for="FileTypeDisable"><?=$i_general_disabled?></label>
								<input type="radio" name="AllowedFileTypes" id="FileTypeAll" value="ALL" />
								<label for="FileTypeAll"><?=$i_general_all?></label>
								<input type="radio" name="AllowedFileTypes" id="FileTypeManual" value="MANUAL" />
								<input class="bsettingInputField" id="FileTypeInput" name="FileTypeInput" value="" />
							</td>
						</tr>
						
						<!-- Allow Video Types -->
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletextrequire2">#</span> <span class="tabletext"><?=$eComm['AllowedVideoTypes']?></span> <span class='tabletextrequire'>*</span></td>
							<td class="tabletext">
								<input type="radio" name="AllowedVideoTypes" id="VideoTypeDisable" value="DISABLED" />
								<label for="VideoTypeDisable"><?=$i_general_disabled?></label>
								<input type="radio" name="AllowedVideoTypes" id="VideoTypeAll" value="ALL" />
								<label for="VideoTypeAll"><?=$i_general_all?></label>
								<input type="radio" name="AllowedVideoTypes" id="VideoTypeManual" value="MANUAL" />
								<input class="bsettingInputField" id="VideoTypeInput" name="VideoTypeInput" value="" />
							</td>
						</tr>
						
						<!-- Allow Website -->
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['eComm']['AllowUsingWebsite']?></span> <span class='tabletextrequire'>*</span></td>
							<td class="tabletext">
								<input type="radio" name="AllowedWebsite" id="WebsiteDisable" value="0" />
								<label for="WebsiteDisable"><?=$i_general_disabled?></label>
								<input type="radio" name="AllowedWebsite" id="WebsiteEnable" value="1" />
								<label for="WebsiteEnable"><?=$i_general_enabled?></label>
							</td>
						</tr>
						
					</table>
					</td>
						
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
				</tr>
                <tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<span class="tabletextrequire2">#</span> - <?=$eComm['AllowedTypesRemarks']?></td>
				</tr>
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit") ?>
	        <?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
		</td>
		</tr>
	    </table>                                
	</td>
</tr>
</table>                        
</form>

<?php
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.SharingLatestNo");
$linterface->LAYOUT_STOP();
?>