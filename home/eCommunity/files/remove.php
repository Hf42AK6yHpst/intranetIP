<?php
// Using:

###################################
#
#   Date:   2019-09-04  Bill    [2019-0815-1424-47235]
#           Added checking before delete any files
#
###################################

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
//include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libfile.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$FileID = IntegerSafe($FileID);

$lg = new libgroup();

$sql = "SELECT GroupID FROM INTRANET_FILE WHERE FileID = '$FileID'";
$groupArr = $lg->returnVector($sql);
$targetGroupID = $groupArr[0];

//$isAdmin = $lg->isGroupAdmin($UserID, $targetGroupID);
$lgroup = new libgroup($targetGroupID);
$isAdmin = $lgroup->hasAdminFiles($UserID);

if($FileID)
{
    $li = new libfile($FileID);

    $deleteConds = ($isAdmin? "": "AND UserID = '$UserID'");
    $sql = "DELETE FROM INTRANET_FILE WHERE FileID = '$FileID' $deleteConds";
    $li->db_db_query($sql);

    $lo = new libfilesystem();
    if($UserID == $li->UserID || $isAdmin)
    {
        $Folder = $intranet_root."/file/group/g".$li->GroupID;
        $path = $Folder."/".$li->Location;

        if($li->GroupID && $li->Location != '' && file_exists($path)) {
            $lo->lfs_remove($path);
        }
        // $lo->lfs_remove($Folder."/".$li->Location);
    }
}

intranet_closedb();
header("Location: index.php?GroupID=$GroupID");
?>