<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libfile.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$FileID = IntegerSafe($FileID);

//$lo = new libfilesystem();
//$lg = new libgroup($GroupID);
$legroup = new libegroup($GroupID);

$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,9,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if (!$legroup->hasAdminFiles($UserID))
{
	header("Location: ../group/?GroupID=$GroupID");
    exit();
}


$FileIDArr = implode(',',(array)$FileID);
$sql = "UPDATE INTRANET_FILE SET Approved = 1 WHERE FileID IN ($FileIDArr)";
$legroup->db_db_query($sql) or die(mysql_error());

intranet_closedb();
header("Location: settings_file.php?xmsg=approve&GroupID=$GroupID&FileType=$page_FileType&FolderID=$page_FolderID&pstatus=$page_pstatus&keyword=$page_keyword");
?>

