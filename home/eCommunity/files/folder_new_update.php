<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();

$DisplayType = $_POST['DisplayType'];
$FileType = $_POST['FileType'];
$GroupID = $_POST['GroupID'];
$Title = $_POST['Title'];
$PublicStatus = $_POST['PublicStatus'];
$onTop = $_POST['onTop'];
$onTop = $onTop ? 1 : 0;
$PhotoResize = $_POST['PhotoResize'];
$DemensionX = $PhotoResize == 1? $_POST['DemensionX'] : "NULL";
$DemensionY = $PhotoResize == 1? $_POST['DemensionY'] : "NULL";

$GroupID = IntegerSafe($GroupID);

$lo = new libuser($UserID);
$Groups = $lo->returnGroups();
$GroupID = ($lo->IsGroup($GroupID)) ? $GroupID : $Groups[0][0];
$lu = new libgroup($GroupID);

if (!$lu->hasAdminFiles($UserID))
{
    header("Location: ../index.php");
    intranet_closedb();
    exit();
}



$sql = "
	INSERT INTO INTRANET_FILE_FOLDER 
	(GroupID, FolderType, UserID, UserName, Title, PublicStatus, DateInput, onTop, PhotoResize, DemensionX, DemensionY) 
	VALUES
	('$GroupID', '$FileType','$UserID','".addslashes($lo->NickNameClassNumber())." ','$Title','$PublicStatus', now(), $onTop, $PhotoResize, $DemensionX, $DemensionY)
";
$lu->db_db_query($sql);
$FolderID=mysql_insert_id();

$xmsg = ($lu->db_affected_rows()<=0) ? "add_failed" : "add";

intranet_closedb();
if(!strstr($_SERVER['HTTP_REFERER'],"new_album_popup.php")) //if post from folder_new.php
{
	header("Location: index2.php?xmsg=$xmsg&GroupID=$GroupID&FileType=$FileType&DisplayType=$DisplayType");
	exit;
}
else if($xmsg=="add")//if post from new_album_popup.php & insert successfully
{
	$html .= "<script>\n";
	$html .= "	var par = opener.window;\n";
	$html .= "	var selection = par.document.getElementById('FolderID');\n";
	$html .= "	var newoption = par.document.createElement('option');\n";
	$html .= "	newoption.text = '$Title';\n";
	$html .= "	newoption.value = '$FolderID';\n";
	$html .= "  selection.options[selection.options.length]=newoption;\n";
	$html .= "	newoption.selected = true;\n";
	$html .= "	par.AddAlbumErr(0);\n";
	$html .= "	window.close();\n";
	$html .= "</script>";

}
else
{
	$html .= "<script>\n";
	$html .= "	par.AddAlbumErr(1);\n";
	$html .= "	window.close();\n";
	$html .= "</script>";
}
echo $html;
?>	
