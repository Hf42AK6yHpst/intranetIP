<?php
// Using:

###################################################
#
#   Date:   2019-09-04  Bill    [2019-0815-1424-47235]
#           Added checking before delete any files
#           Delete related file / comments in db
#
#	Date:	2012-11-26	YatWoon
#			Fixed: pass incorrect data to libfile that cannot retrieve the filename [Case#2012-1126-1023-34073]
#
###################################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfolder.php");
include_once($PATH_WRT_ROOT."includes/libfile.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);

$lo = new libfilesystem();
$legroup = new libegroup($GroupID);

$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,9,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if (!$legroup->hasAdminFiles($UserID))
{
    header("Location: ../index.php");
    exit();
}

for($i=0;$i<sizeof($FolderID);$i++)
{
	$thisFolderID = $FolderID[$i];
    if(!$thisFolderID) {
        continue;
    }

	$lfolder = new libfolder($thisFolderID);
	$FileIDs = $lfolder->getFileIDList();

	for($j=0;$j<sizeof($FileIDs);$j++)
	{
        if(!$FileIDs[$j]['FileID']) {
            continue;
        }
		$lf = new libfile($FileIDs[$j]['FileID']);
		
		// Delete files in this folder
		$Folder = $intranet_root."/file/group/g".$GroupID;
        $path = $Folder."/".$lf->Location;

        if($GroupID && $lf->Location != '' && file_exists($path)) {
            $lo->lfs_remove($path);
        }
    	// $lo->lfs_remove($Folder."/".$lf->Location);
    	
		// Delete file / comments
		//$legroup->db_db_query("SELECT * FROM INTRANET_FILE_COMMENT WHERE FileID = ".$FileIDs[$j]);
        $sql = "DELETE FROM INTRANET_FILE_COMMENT WHERE FileID = '".$FileIDs[$j]['FileID']."'";
        $legroup->db_db_query($sql);

		//$legroup->db_db_query("SELECT * FROM INTRANET_FILE WHERE FileID = ".$FileIDs[$j]);
        $sql = "DELETE FROM INTRANET_FILE WHERE FileID = '".$FileIDs[$j]['FileID']."'";
        $legroup->db_db_query($sql);
	}

	// $legroup->db_db_query("DELETE FROM INTRANET_FILE_FOLDER WHERE FolderID = '$thisFolderID'");
    $sql = "DELETE FROM INTRANET_FILE_FOLDER WHERE FolderID = '$thisFolderID'";
    $legroup->db_db_query($sql);
}

intranet_closedb();
header("Location: settings_folder.php?xmsg=delete&GroupID=$GroupID&FolderType=$FolderType");
?>