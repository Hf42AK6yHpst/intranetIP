<?php
#using :

############ Change Log [Start] ##############
#
#   Date	:   2019-10-14  Bill    [DM#3665]
#           	support add website logic
#
#	Date	:	2010-09-28	[Thomas]
#				resize uploaded image if the destination folder is set to 'Resize image' 
#
#	Date	:	2010-05-17	[YatWoon]
#				for rename filename, using "strrpos" instead of "strpos" due to some filename with .
#
############ Change Log [End] ##############

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfolder.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);

$li = new libuser($UserID);
$lo = new libfilesystem();
$legroup = new libegroup($GroupID);
$lg = new libgroup($GroupID);
$lf = new libfolder($FolderID);

$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,9,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if (!$legroup->hasAdminFiles($UserID))
{
    header("Location: ../group/?GroupID=$GroupID");
    exit();
}

if($FileType == 'P')
{
	$RawFileLocation = $_FILES['myfile']['tmp_name'];
	if($RawFileLocation)
	{
		$RawFileName = $_FILES['myfile']['name'];
	
		$Rawext = strtolower(get_file_ext($RawFileName));
		$Rawext = substr($Rawext, 1, strlen($Rawext)-1);
		
		$tmp_dir = session_id()."_".time()."_".$i;
		$tmp_src = "/tmp/".$tmp_dir;
		mkdir($tmp_src);
		
		$file_ary = array();
		
		#unzip / copy file to temp folder
		//if($Rawext == 'zip')
			//break;
			//$lo->unzipFile($RawFileLocation, $tmp_src);
		//else
		//{
			$tmp_Location = session_id()."-".(time()+$i).strtolower(substr($RawFileName, strrpos($RawFileName,".")));
			$lo->lfs_copy($RawFileLocation, $tmp_src."/".$tmp_Location);
		//}
		
		$temp_ary = array();
		$temp_ary = $lo->return_folderlist($tmp_src);
		
		$file_ary = array();
		$file_ary_key = 0;
			
		for($k=0;$k<sizeof($temp_ary);$k++)
		{
			$File_Name = substr($temp_ary[$k], strrpos($temp_ary[$k], "/")+1);  //without opening "/"
			$File_ext  = strtolower(get_file_ext($File_Name));
			$File_ext  = substr($File_ext, 1, strlen($File_ext)-1);
			
			if(!is_dir($temp_ary[$k]) && $File_ext != 'ini')
			{
				#Get File into and save in an array
				$file_ary[$file_ary_key]['Location'] = $temp_ary[$k];
				//$file_ary[$file_ary_key]['FileName'] = ($Rawext == 'zip'? $File_Name : $RawFileName);
				$file_ary[$file_ary_key]['FileName'] = $RawFileName;
				$file_ary[$file_ary_key]['FileSize'] = filesize($temp_ary[$k]) / 1000;
				$file_ary_key++;
			}
		}

		for($j=0;$j<sizeof($file_ary);$j++)
		{
			$FileLocation = $file_ary[$j]['Location'];
			$FileName     = $file_ary[$j]['FileName'];
			$FileSize     = $file_ary[$j]['FileSize'];
			
			if($FileLocation && $FileName && $FileSize)
			{
				## check file format
				if($lg->AllowedImageTypes != "ALL")
				{
					$allowedAry = explode("/",strtolower($lg->AllowedImageTypes));
					$ext = strtolower(get_file_ext($FileName));
					$ext = substr($ext, 1, strlen($ext)-1);
					if(!in_array($ext, $allowedAry))	
					{
						$error_msg = "file_type_not_allowed";	
						continue;
					}
				}
		
				$FLocation = session_id()."-".(time()+$i).$j.strtolower(substr($FileName, strrpos($FileName,".")));

				$used = $lg->returnGroupUsedStorageQuota();
				$available = ($lg->StorageQuota)*1000 - $used;
					
				if ($available >= $FileSize)
				{
					$Folder = $intranet_root."/file/group/g".$GroupID;
					
					$lo->folder_new($Folder);
				
					#check if the folder need resize
					$image_exts = array("gif","jpg","jpe","jpeg","png");
					if($lf->PhotoResize == 1 && in_array($Rawext, $image_exts))
						$legroup->resize_photo($FileLocation, $Folder."/".$FLocation, $lf->DemensionX, $lf->DemensionY);
					else
						$lo->lfs_copy($FileLocation, $Folder."/".$FLocation);

					$approved = $legroup->needApproval?"0":"1";
					$save_FileName = $li->Get_Safe_Sql_Query($FileName); //remove unwanted char in Filename
			
					/*
					$sql = "
						INSERT INTO INTRANET_FILE 
						(GroupID, UserID, UserName, UserEmail, Title, UserTitle, Location, ReadFlag, DateInput, DateModified, Size, FolderID, FileType, Approved) 
						VALUES 
						(".$GroupID.", $UserID, '".addslashes($li->NickNameClassNumber())."', '".$li->UserEmail."', '$save_FileName', '$save_FileName', '$FLocation', ';$UserID;', now(), now(), $FileSize, $FolderID, 'P', ". ($lg->needApproval?"0":"1") .")
					";
					*/
					
					$sql = "
							INSERT INTO INTRANET_FILE 
							(GroupID, UserID, UserName, UserEmail, Title, Location, ReadFlag, DateInput, DateModified, Size, FolderID, FileType, UserTitle, Description, PublicStatus, onTop, Approved) 
							VALUES 
							(".$GroupID.", $UserID, '".addslashes($li->NickNameClassNumber())."', '".$li->UserEmail."', '$FileName', '$FLocation', ';$UserID;', now(), now(), $FileSize, $FolderID, '$FileType', '$Title', '$description', '1', '$onTop', 1)
							";
					
					$li->db_db_query($sql) or die(mysql_error());
					//array_push($FileID, $li->db_insert_id());
				}
				else
				{
					$error_msg = "no_space";
					$xmsg = "add_failed";	
				}
			}
		}
		$lo->folder_remove_recursive($tmp_src);
	}
}
else if($FileType == 'W')
{
    $sql = "INSERT INTO INTRANET_FILE
				(GroupID, UserID, UserName, UserEmail, Title, Location, ReadFlag, DateInput, DateModified, FolderID, FileType, UserTitle, Description, PublicStatus, onTop, Approved)
				VALUES
				(".$GroupID.", $UserID, '".addslashes($li->NickNameClassNumber())."', '".$li->UserEmail."', '$url', '$url', ';$UserID;', now(), now(), $FolderID, '$FileType', '$Title', '$description', '$PublicStatus', '$onTop', 1)
			";
    $li->db_db_query($sql) or die(mysql_error());
    $xmsg = "add";
}
else
{
	$FileLocation = $_FILES['myfile']['tmp_name'];
	if($FileLocation)	
	{
		$FileName = ${"myfile_name"};
		$FileSize = $_FILES['myfile']['size']/1000;
		$FLocation = session_id()."-".time().strtolower(substr($FileName, strrpos($FileName,".")));
		
		$used = $lg->returnGroupUsedStorageQuota();
		$available = ($lg->StorageQuota)*1000 - $used;
		
		if ($available >= $FileSize)
		{
			$Folder = $intranet_root."/file/group/g".$GroupID;
			$lo->folder_new($Folder);
			$lo->lfs_copy($FileLocation, $Folder."/".$FLocation);
			$onTop = $onTop ? 1 : 0;
			
			$sql = "
				INSERT INTO INTRANET_FILE 
				(GroupID, UserID, UserName, UserEmail, Title, Location, ReadFlag, DateInput, DateModified, Size, FolderID, FileType, UserTitle, Description, PublicStatus, onTop, Approved) 
				VALUES 
				(".$GroupID.", $UserID, '".addslashes($li->NickNameClassNumber())."', '".$li->UserEmail."', '$FileName', '$FLocation', ';$UserID;', now(), now(), $FileSize, $FolderID, '$FileType', '$Title', '$description', '$PublicStatus', '$onTop', 1)
			";
			$li->db_db_query($sql) or die(mysql_error());
			$xmsg = "add";
		}
		else
		{
			$error_msg = "no_space";	
			$xmsg = "add_failed";
		}
	}
}

intranet_closedb();
header("Location: settings_file.php?xmsg=$xmsg&GroupID=$GroupID&FileType=$FileType&FolderID=$FolderID");
?>

