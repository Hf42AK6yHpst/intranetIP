<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libuser.php");
include("../../../includes/libgroup.php");
include("../../../includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$PostGroupID = IntegerSafe($PostGroupID);

$li = new libuser($UserID);
$lo = new libfilesystem();

$FileLocation = $Title;
$FileName = $Title_name;
$FileSize = $Title_size;
//$FTitle = $FileName." (".number_format($FileSize/1000)."KB)";
$FTitle = $FileName;
$FLocation = session_id()."-".time().strtolower(substr($FileName, strpos($FileName,".")));
$Description = htmlspecialchars($Description);
$FileSize = ceil($FileSize/1000);

if($Title=="none"){
} else {
     for($i=0; $i<sizeof($PostGroupID); $i++){
          if($li->IsGroup($PostGroupID[$i])){
               $lg = new libgroup($PostGroupID[$i]);
               $used = $lg->returnGroupUsedStorageQuota();
               $available = ($lg->StorageQuota)*1000 - $used;
               if ($available >= $FileSize)
               {
                   $Folder = $intranet_root."/file/group/g".$PostGroupID[$i];
                   $lo->folder_new($Folder);
                   $lo->lfs_copy($FileLocation, $Folder."/".$FLocation);
                   $sql = "INSERT INTO INTRANET_FILE (GroupID, UserID, UserName, UserEmail, Title, Location, Description, ReadFlag, DateInput, DateModified, Size) VALUES (".$PostGroupID[$i].", $UserID, '".addslashes($li->NickNameClassNumber())."', '".$li->UserEmail."', '$FTitle', '$FLocation', '$Description', ';$UserID;', now(), now(), $FileSize)";
                   $li->db_db_query($sql);
               }
          }
     }
}

intranet_closedb();
header("Location: index.php?GroupID=$GroupID&left=$available&used=$used");
?>