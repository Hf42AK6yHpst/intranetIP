<?php
/*
 * 2017-07-26 Carlos: Cater https resource url.
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfile.php");
include_once($PATH_WRT_ROOT."includes/libfolder.php");

intranet_auth();
intranet_opendb();

$is_https = function_exists("isSecureHttps")? isSecureHttps() : ($_SERVER["SERVER_PORT"] == 443 || $_SERVER["HTTPS"]=="on" || $_SERVER['HTTP_X_FORWARDED_PROTO'] == "https");
$http_protocol = $is_https? "https://" : "http://";

$File_Type    = $_POST["File_attr"][0];
$Group_ID     = $_POST["File_attr"][1];
$File_ID      = $_POST["File_attr"][2];
$Folder_ID    = $_POST["File_attr"][3];
$Display_Type = $_POST["File_attr"][4];

$lfolder_ajax	 = new libfolder($Folder_ID);
$linterface_ajax = new interface_html();
$lf_ajax 		 = new libfile($File_ID);
$lu_ajax         = new libgroup($Group_ID);
$legroup_ajax 	 = new libegroup($Group_ID);

$File_Type		= $lf_ajax->FileType ? $lf_ajax->FileType : $File_Type;
$PAGE_NAVIGATION = array(
	$eComm[$File_Type], "index2.php?GroupID=$Group_ID&DisplayType=$Display_Type&FileType=$File_Type",
	$lfolder_ajax->Title, "index.php?GroupID=$Group_ID&DisplayType=$Display_Type&FileType=$File_Type&FolderID=$Folder_ID",
	$lf_ajax->Title,
);

$content = "
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
	<tr>
		<td align=\"left\">". $linterface_ajax->GET_NAVIGATION3($PAGE_NAVIGATION) ."</td>
	</tr>
	";

if($lu_ajax->hasAdminFiles($UserID) || $lf_ajax->UserID==$UserID)
{
	$delBtn 	= "<a href=\"javascript:RemoveThis();\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_delete . "</a>";
	if($lu_ajax->hasAdminFiles($UserID) || $lf_ajax->UserID==$UserID)	$editBtn 	= "<a href=\"javascript:clickEdit()\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";

	$content .= "
			<tr>
	    	<td align=\"center\">
	    	<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	    	<tr>
	    		<td align=\"right\">". $linterface_ajax->GET_SYS_MSG($xmsg, $xmsg2) ."</td>
			</tr>
			<tr>
	    		<td align=\"right\">{$editBtn} {$delBtn}</td>
			</tr>
			</table>
			</td>
			</tr>
	";
}

if($File_Type=="W")
{
	$url = strstr($lf_ajax->Location,$http_protocol)?$lf_ajax->Location:$http_protocol.$lf_ajax->Location."/";
	$link = "<a href=\"". $url ."\" target=\"_blank\">";
}
else
	$link = "<a href=\"javascript:view(". $File_ID .");\">";
$content .= "
			<tr>
			<td align=\"center\">
				<table>
				<tr>
					<td id=\"pre_file_btn\" align=\"left\" style=\"visibility:hidden\"><div id=\"btn_large\"><a herf=\"javascript:void(0);\" onClick=\"nav_viewfile('pre');\" class=\"contenttool\" style=\"cursor:pointer\"><span><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_prev.gif\" border=\"0\"></span></a></div></td>
					<td><div id=\"photo_border\" align=\"center\"> <span>". $legroup_ajax->DisplayPhoto($File_ID, 600, 500, 1, $link)  ."</span></div></td>
					<td id=\"next_file_btn\" align=\"right\" style=\"visibility:hidden\"><div id=\"btn_large\"><a herf=\"javascript:void(0);\" onClick=\"nav_viewfile('next');\" class=\"contenttool\" style=\"cursor:pointer\"><span><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_next.gif\" border=\"0\"></span></a></div></td>
				</tr>
				</table>
			</td>
			</tr>
";

$content .= "
	<tr>
	<td align=\"center\">
	<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
	<tr>
	<td width=\"80\" align=\"left\" class=\"share_file_title\"><span class=\"tabletext\">". $eComm['Title'] .": </span></td>
	<td align=\"left\" class=\"share_file_title\"><span class=\"tabletext\"><strong> ". $lf_ajax->FileUserTitle() ."</strong></span>". $lf_ajax->StatusImg() ."</td>
	<td align=\"left\" class=\"share_file_title\">&nbsp;</td>
	<td align=\"right\" class=\"share_file_title\"><span class=\"tabletext\">". $eComm['CreatedBy'] .": <strong>". $lf_ajax->UserName ." </strong></span></td>
	</tr>
	<tr>
	<td align=\"left\" class=\"share_file_title\"><span class=\"tabletext\">". $i_FileTitle .": </span></td>
	<td align=\"left\" class=\"share_file_title\"><span class=\"tabletext\">". $lf_ajax->Title ."</span></td>
	<td align=\"left\" class=\"share_file_title\">&nbsp;</td>
	<td align=\"right\" class=\"share_file_title\"><span class=\"tabletext\">". $eComm['CreatedDate'] .": ". $lf_ajax->DateInput ." </span></td>
	</tr>
";
if($lf_ajax->Description)
{
	$content .= "
		<tr>
		<td align=\"left\" class=\"share_file_title\" valign=\"top\"><span class=\"tabletext\">". $i_FileDescription .": </span></td>
		<td align=\"left\" class=\"share_file_title\"><span class=\"tabletext\">". nl2br($lf_ajax->Description) ." </span></td>
		<td align=\"left\">&nbsp;</td>
		<td align=\"left\">&nbsp;</td>
		</tr>
	";
}
$content .= "
	</table></td>
	</tr>
	</table>
";

intranet_closedb();
echo $content;
?>