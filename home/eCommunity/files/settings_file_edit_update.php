<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfile.php");


intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);
$FileID = IntegerSafe($FileID);

$legroup = new libegroup($GroupID);
$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,9,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if (!$legroup->hasAdminFiles($UserID))
{
    header("Location: ../group/?GroupID=$GroupID");
    exit();
}

if (!$FileID)
{
    header("Location: ../group/?GroupID=$GroupID");
    exit();
}

$lf = new libfile($FileID);
$FileType = $lf->FileType;
$onTop = $onTop ? 1 : 0;
$CoverImg = $CoverImg ? 1 : 0;
$PhotoResize = $_POST['PhotoResize'];
$DemensionX = $PhotoResize == 1? $_POST['DemensionX'] : "NULL";
$DemensionY = $PhotoResize == 1? $_POST['DemensionY'] : "NULL";

### if set the cover image, clear other cover image within same folder
if($CoverImg)
{
	$sql = "update INTRANET_FILE set CoverImg=0 where FolderID='$FolderID'";
	$lf->db_db_query($sql) or die(mysql_error());
}

$sql = "update INTRANET_FILE set UserTitle='$Title', Description='$description', FolderID='$FolderID', Approved='$Approved', DateModified=now(), onTop = '$onTop', CoverImg='$CoverImg' where FileID='$FileID'";
$lf->db_db_query($sql) or die(mysql_error());

intranet_closedb();
header("Location: settings_file.php?xmsg=update&GroupID=$GroupID&FileType=$page_FileType&FolderID=$page_FolderID&pstatus=$page_pstatus&keyword=$page_keyword&astatus=$Approved");

?>
