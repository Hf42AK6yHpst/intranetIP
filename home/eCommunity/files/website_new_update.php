<?php
# being modified by:

/********************** Change Log ***********************/
#
#   Date:   2019-10-14  Bill    [DM#3663]
#           save UserTitle, fix url display name problem if user not submitted in step 2
#
#   2019-06-06 Anna: Added CSRF token checking.
#
# 	Date	:	2009-12-17 [Yuen]
#	Details	:	fixed the failure to add web link (duplicate title specified in SQL)

#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

if(!verifyCsrfToken($_POST['csrf_token'],$_POST['csrf_token_key'])){
    header('Location: /');
    exit;
}

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);

$li = new libuser($UserID);
$lo = new libfilesystem();
$legroup = new libegroup($GroupID);

$file_upload_count = 10;
$FileID = array();

if($li->IsGroup($GroupID))
{
	$lg = new libgroup($GroupID);

	for($i=1;$i<=$file_upload_count;$i++)
	{
		$url = intranet_htmlspecialchars($_POST['url'.$i]);
		if(!trim($url))	continue;

		$approved = $legroup->needApproval? "0" : "1";

		$sql = "INSERT INTO INTRANET_FILE
				(GroupID, UserID, UserName, UserEmail, Title, UserTitle, Location, ReadFlag, DateInput, DateModified, FolderID, FileType, Approved)
				VALUES
				(".$GroupID.", $UserID, '".addslashes($li->NickNameClassNumber())."', '".$li->UserEmail."', '$url', '$url', '$url', ';$UserID;', now(), now(), $FolderID, 'W', $approved)
			";
		$li->db_db_query($sql) or die(mysql_error());
		array_push($FileID, $li->db_insert_id());

		/*

		$FileName = ${"myfile_name".$i};
		$FileSize = $_FILES['myfile'.$i]['size']/1000;
		$FLocation = session_id()."-".(time()+$i).strtolower(substr($FileName, strpos($FileName,".")));

		$used = $lg->returnGroupUsedStorageQuota();
		$available = ($lg->StorageQuota)*1000 - $used;

		if ($available >= $FileSize)
		{
			$Folder = $intranet_root."/file/group/g".$GroupID;
			$lo->folder_new($Folder);
			$lo->lfs_copy($FileLocation, $Folder."/".$FLocation);

		}
		*/
	}
}

intranet_closedb();
?>

<body onLoad="document.form1.submit();">
<form name="form1" action="website_new_step2.php" method="get">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="<?=$FolderID;?>">
<input type="hidden" name="FileID" value="<?=serialize($FileID);?>">
</form>
</body>