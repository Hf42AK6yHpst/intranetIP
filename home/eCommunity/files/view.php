<?php
#using: 
/**************************************************
 *  20100902 Marcus:
 * 		use DownloadAttachment instead in order to solve file name problem
 * ************************************************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfile.php");

intranet_auth();
intranet_opendb();

$FileID = IntegerSafe($FileID);

$li = new libfile($FileID);
$li->UpdateReadFlag();

intranet_closedb();

$newFilename = $li->Location;
$modulename = "group";
$path = "g".$li->GroupID."/".$newFilename;

//$newFilename = urlencode($li->Location);

DownloadAttachment($modulename,$path, $li->Title);

//header("Location: ". $PATH_WRT_ROOT ."file/group/g".$li->GroupID."/".$newFilename);
?>
