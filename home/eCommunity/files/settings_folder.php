<?php
# using: yat

######################################
#
#	Date:	2014-01-10	YatWoon	[Case#X57661]
#			Fixed: failed to sort table ordering 

#	Date:	2013-04-24	YatWoon
#			Hide "public/private" [Case#2013-0412-0957-26073]
#
######################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfolder.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$linterface = new interface_html();
$lgroup = new libgroup($GroupID);
$legroup = new libegroup($GroupID);

$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,9,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if (!$legroup->hasAdminFiles($UserID))
{
	header("Location: ../group/?GroupID=$GroupID");
    exit();
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$CurrentPage	= "Settings_SharingSettings";
$MyGroupSelection= $legroup->getSelectAdminBasicInfo("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);

### Button
$AddBtn 	= "<a href=\"javascript:checkNew('settings_folder_new.php?GroupID=$GroupID&FolderType=$FolderType')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'FolderID[]','settings_folder_edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'FolderID[]','settings_folder_remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_delete . "</a>";

$searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
$searchTag 	.= "<td>";

$searchTag 	.= "&nbsp;<select name='FolderType' onChange='this.form.submit();'>\n";
$searchTag 	.= "<option value=''>$i_status_all ". $eComm['FolderType'] ."</option>\n"; 
$searchTag 	.= "<option value='P' ".(($FolderType=='P')?"selected":"").">". $eComm['Photo'] ."</option>\n";
$searchTag 	.= "<option value='V' ".(($FolderType=='V')?"selected":"").">". $eComm['Video'] ."</option>\n";
$searchTag 	.= "<option value='W' ".(($FolderType=='W')?"selected":"").">". $eComm['Website'] ."</option>\n";
$searchTag 	.= "<option value='F' ".(($FolderType=='F')?"selected":"").">". $eComm['File'] ."</option>\n";
$searchTag 	.= "</select>\n";

/*
$searchTag 	.= "&nbsp;<select name='pstatus' onChange='this.form.submit();'>\n";
$searchTag 	.= "<option value='-1'>". $eComm['Public'] ." / " . $eComm['Private'] ."</option>\n";
$searchTag 	.= "<option value='1' ".(($pstatus==1)?"selected":"").">". $eComm['Public'] ."</option>\n";
$searchTag 	.= "<option value='0' ".(($pstatus==0 && $pstatus!="")?"selected":"").">". $eComm['Private'] ."</option>\n";
$searchTag 	.= "</select>\n";
*/

$searchTag 	.= "&nbsp;<input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
$searchTag 	.= "</tr></table>";

### Table SQL ###
$keyword = trim($keyword);
// $order = ($order == 1) ? 0 : 1;
if($filter == "") $filter = 1;
$field = ($field != "") ? $field : 0;
// $pstatus_str = (isset($pstatus) && $pstatus!='-1') ? " and a.PublicStatus=$pstatus " : "";	
//			if(a.PublicStatus=0, concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_private.gif border=0 align=absmiddle>'), ''),

$user_field = getNameFieldWithClassNumberByLang("b.");
$FolderType_str = ($FolderType) ? " and a.FolderType='$FolderType'" : "";
$sql = "
	select 
		if(a.onTop=1, '<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_ontop.gif border=0 align=absmiddle>', ''),
		CONCAT('<a class=\"tablelink\" href=\"settings_folder_edit.php?GroupID=$GroupID&FolderID[]=', a.FolderID, '\">', a.Title, '</a>'),
		CASE 
			WHEN a.FolderType='P' THEN '". $eComm['Photo'] ."'
			WHEN a.FolderType='W' THEN '". $eComm['Website'] ."'
			WHEN a.FolderType='V' THEN '". $eComm['Video'] ."'
			WHEN a.FolderType='F' THEN '". $eComm['File'] ."'
		ELSE ' '
		END,
		IF (a.UserID IS NOT NULL OR a.UserID != 0,$user_field,'$i_AnnouncementNoAnnouncer') as UserName,
		a.DateInput,
		count(c.FileID) as countFile,
		CONCAT('<input type=\"checkbox\" name=\"FolderID[]\" value=\"', a.FolderID ,'\">')
	from 
		INTRANET_FILE_FOLDER as a
	    LEFT OUTER JOIN INTRANET_FILE c using (FolderID)
	    LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = a.UserID
	where 
		a.GroupID='$GroupID'
		AND (a.Title like '%$keyword%')
		$FolderType_str
	group by a.FolderID	
";


if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.Title","a.FolderType", "UserName", "a.DateInput", "countFile");
$li->sql = $sql;
$li->no_col = 8;
$li->IsColOff = 2;
$li->title = $eComm['Folder'];

// TABLE COLUMN		
$li->column_list .= "<td class='tabletop tabletopnolink'>#</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink' width='18'>&nbsp</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink' width='300'>".$li->column(0, $eComm['Folder'])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(1, $eComm['FolderType'])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(2, $eComm['CreatedBy'])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(3, $eComm['CreatedDate'])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(4, $eComm['File'].'#')."</td>\n";
$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("FolderID[]")."</td>\n";
    

### Title ###
$TAGS_OBJ[] = array($eComm['Folder'],"settings_folder.php?GroupID=$GroupID", 1);
$TAGS_OBJ[] = array($eComm['File'],"settings_file.php?GroupID=$GroupID", 0);
$TAGS_OBJ[] = array($i_general_BasicSettings,"bsettings.php?GroupID=$GroupID", 0);
$title = "
	<table width='100%' border='0' cellpadding='0' cellspacing='0' valign=\"bottom\">
		<tr>
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$lgroup->TitleDisplay."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ_RIGHT[] = array($title,"",0);
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<script language="javascript">
function checkRemove(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm("<?=$eComm['jsDeleteFolder']?>")){	            
                obj.action=page;                
                obj.method="POST";
                obj.submit();				             
                }
        }
}
</script>

<br />

<form name="form1" method="get" action="settings_folder.php">
<input type="hidden" name="GroupID" value="<?=$GroupID?>">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
                                        	<table border="0" cellspacing="0" cellpadding="2">
                                            	<tr>
                                              		<td><p><?=$AddBtn?></p></td>
                                            	</tr>
                                          	</table>
					</td>
                                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
				</tr>

				<tr>
                                	<td class="tabletext">
                                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                        <td width="100%" align="left" class="tabletext"><?=$searchTag?></td>
												</tr>
                                                </table>
					</td>
					<td align="right" valign="bottom" height="28">
						<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
							<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
								<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td nowrap><?=$editBtn?></td>
                                    <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
                                    <td nowrap><?=$delBtn?></td>
								</tr>
        							</table>
							</td>
        						<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
						</tr>
						</table>
					</td>
				</tr>
                                <tr>
                                	<td colspan="2">
						<?=$li->display();?>
					</td>
				</tr>
                                
                                
				</table>
			</td>
		</tr>
                </table>
        </td>
</tr>
</table>
<br />

<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();

?>
