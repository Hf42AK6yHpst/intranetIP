<?php
## Using By : 
#############################################
##	Modification Log:
##	2010-01-05: Max (200912311437)
##	- Set AllowedImageTypes/AllowedFileTypes/AllowedVideoTypes = DISABLED when disabled
##	- Set AllowedImageTypes/AllowedFileTypes/AllowedVideoTypes = ALL when all
##	- Modify SQL to support AllowedWebsite field in DB
#############################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();

$AllowedImageTypes = $_POST['AllowedImageTypes'];
$AllowedFileTypes = $_POST['AllowedFileTypes'];
$AllowedVideoTypes = $_POST['AllowedVideoTypes'];
$AllowedWebsite = $_POST['AllowedWebsite'];

define("MANUAL", "MANUAL");
define("DISABLED", "DISABLED");
if ($AllowedImageTypes == MANUAL) {
	$AllowedImageTypes = !empty($_POST['ImageTypeInput'])?$_POST['ImageTypeInput']:DISABLED;
}
if ($AllowedFileTypes == MANUAL) {
	$AllowedFileTypes = !empty($_POST['FileTypeInput'])?$_POST['FileTypeInput']:DISABLED;
}
if ($AllowedVideoTypes == MANUAL) {
	$AllowedVideoTypes = !empty($_POST['VideoTypeInput'])?$_POST['VideoTypeInput']:DISABLED;
}

$GroupID = IntegerSafe($GroupID);
$SharingLatestNo = IntegerSafe($SharingLatestNo);

$lg = new libgroup($GroupID);
$lg->SharingLatestNo = $SharingLatestNo;
$lg->needApproval = $needApproval;
$lg->DefaultViewMode = $DefaultViewMode;
$lg->AllowedImageTypes = $AllowedImageTypes;
$lg->AllowedFileTypes = $AllowedFileTypes;
$lg->AllowedVideoTypes = $AllowedVideoTypes;
$lg->AllowedWebsite = $AllowedWebsite;
//
$lg->UPDATE_GROUP_TO_DB();
//$sql = "UPDATE INTRANET_GROUP SET SharingLatestNo = '$SharingLatestNo', needApproval='$needApproval', 
//DefaultViewMode='$DefaultViewMode', AllowedImageTypes='$AllowedImageTypes', AllowedFileTypes='$AllowedFileTypes', 
//AllowedVideoTypes='$AllowedVideoTypes', AllowedWebsite='$AllowedWebsite' WHERE GroupID = '$GroupID'";
//$lg->db_db_query($sql);



intranet_closedb();
header("Location: bsettings.php?GroupID=$GroupID&msg=update");
?>