<?php
/***************
* Date		:	2019-06-04 Anna - Added CSRF token. 
* *************/
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfile.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);

$li = new libuser($UserID);
$lo = new libfilesystem();
$lf = new libfile();
$legroup = new libegroup($GroupID);
$file_upload_count = 10;
$FileID = array();

$WrongUrl= Array();
if($li->IsGroup($GroupID))
{
	$lg = new libgroup($GroupID);
	
	for($i=1;$i<=$file_upload_count;$i++)
	{
		$url = $_POST['url'.$i];
		if(!$url)	continue;
		//check if the file is exists
		if(!$lf->url_exists($url))	
		{
			$WrongUrl[]=$url;
			continue;
		}
		$FileName = substr($url, strrpos($url, "/")+1);
		$FLocation = session_id()."-".(time()+$i).strtolower(substr($FileName, strpos($FileName,".")));
		$Folder = $intranet_root."/file/group/g".$GroupID;
		
		$lo->folder_new($Folder);
		$lo->download($url, $Folder."/".$FLocation);
		
		$used = $lg->returnGroupUsedStorageQuota();
		if (($lg->StorageQuota)*1000 > $used)
		{
			$FileSize = ceil(filesize($Folder."/".$FLocation) / 1000);
			$approved = $legroup->needApproval?"0":"1";
			$sql = "
				INSERT INTO INTRANET_FILE 
				(GroupID, UserID, UserName, UserEmail, Title, Location, ReadFlag, DateInput, DateModified, Size, FolderID, FileType, Approved) 
				VALUES 
				(".$GroupID.", $UserID, '".addslashes($li->NickNameClassNumber())."', '".$li->UserEmail."', '$FileName', '$FLocation', ';$UserID;', now(), now(), '$FileSize', '$FolderID', 'F','$approved')
			";
			
			$li->db_db_query($sql) or die(mysql_error());
			array_push($FileID, $li->db_insert_id());
		}
		else
		{
			$error_msg = "no_space";	
		}
	}
}
//debug_r($WrongUrl);
foreach($WrongUrl as $key => $Url)
{
	$input .= "<input type=\"hidden\" name=\"wronglink[]\" value=\"$Url\"><br>";
}

intranet_closedb();
?>

<body onLoad="document.form1.submit();">
<form name="form1" action="file_new_step2.php" method="post">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="<?=$FolderID;?>">
<input type="hidden" name="FileID" value="<?=serialize($FileID);?>">
<input type="hidden" name="error_msg" value="<?=$error_msg;?>">
<?=$input;?>
<?php echo csrfTokenHtml(generateCsrfToken()); ?>
</form>
</body>