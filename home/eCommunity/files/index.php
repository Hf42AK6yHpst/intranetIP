<?php
# being modified by: 


/********************** Change Log ***********************/
#   Dare:   2019-06-06 Anna 
#           Added CSRF token checking. 
#
#	Date:	2017-07-26 Carlos
#			Cater https resource url.
#
#	Date:	2013-09-27 YatWoon
#			hide private icon
#
#	Date:	2012-02-09 YatWoon
#			Improved: Keep track the last accessed group id
#
#	Date:	2011-09-15	YatWoon
#			update waiting/reject title
#			update waiting/reject table list
#
#	Date:	2011-05-03	YatWoon
#			add alt / title for the icon
#
#	Date 	: 	2010-12-13 [YatWoon]
#				Display group name according to lang session
#
# 	Date	:	2010-06-24 [Yuen]
#	Details	:	fixed to show all photos/files by adding "$li->page_size = 999;"
#
#
/******************* End Of Change Log *******************/


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT.'includes/libdbtable.php');
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfile.php");
include_once($PATH_WRT_ROOT."includes/libfolder.php");

intranet_auth();
intranet_opendb();

$is_https = function_exists("isSecureHttps")? isSecureHttps() : ($_SERVER["SERVER_PORT"] == 443 || $_SERVER["HTTPS"]=="on" || $_SERVER['HTTP_X_FORWARDED_PROTO'] == "https");
$http_protocol = $is_https? "https://" : "http://";

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);


$lf 			= new libfile();
$lfolder		= new libfolder($FolderID);
$linterface 	= new interface_html();
$legroup 		= new libegroup($GroupID);

$lo = new libuser($UserID);
//$lu = new libgroup($GroupID);

//if (!$lu->isAccessFiles())
//{
//    header("Location: ../index.php");
//    intranet_closedb();
//    exit();
//}

if($FolderID)
{
	$FileType = $lfolder->FolderType;
}
$FileType = cleanCrossSiteScriptingCode($FileType);
$lu2007 	= new libuser2007($UserID);
if($lu2007->isInGroup($GroupID) && ($legroup->isAccessFiles() || $legroup->isAccessPhoto() || $legroup->isAccessVideo()))
{
	$CurrentPage	= "MyGroups";
	
	if($_SESSION['UserType']!=USERTYPE_ALUMNI)
	$MyGroupSelection= $legroup->getSelectGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID);
}
else
{
	header("Location: ../index.php?msg=AccessDenied");
	exit;
	
//	$CurrentPage	= "OtherGroups";
//	$MyGroupSelection= $lu->getSelectOtherGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID);
}

### keep track the last accessed Group ID
$legroup->UpdateLastAccess($GroupID);


# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if($field=="") $field=4;
switch ($field)
{
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        default: $field = 4; break;
}
$order = ($order == 1) ? 1 : 0;
$DisplayType = cleanCrossSiteScriptingCode($DisplayType);
####################################################
# nevigation bar
####################################################
if($FolderID)
	$PAGE_NAVIGATION = array($eComm[$FileType], "index2.php?GroupID=$GroupID&DisplayType=$DisplayType&FileType=$FileType", $lfolder->Title);
else if($FileType=="WA")
	$PAGE_NAVIGATION = array($eComm['WaitingRejectList']);
else
	$PAGE_NAVIGATION = array($eComm['LatestShare']);


$FolderID_str = $FolderID ? " and FolderID=$FolderID" : "";


####################################################
# define file/folder type and related variables
####################################################
$TypeImage = $lfolder->TypeImg($FileType);
$TypeName = $lfolder->TypeName($FileType);
$FolderTitle = $lfolder->FolderTitle($FileType);

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);

$li->page_size = 999;
switch($FileType)
{
	case "P":
		$ApprovedStr = " and a.approved = 1 ";
		$FolderType_str = " and a.FolderType='P' ";
		$li->field_array = array("a.onTop");
		break;
	case "V":
		$ApprovedStr = " and a.approved = 1 ";
		$FolderType_str = " and a.FolderType='V' ";
		$li->field_array = array("a.onTop");
		break;
	case "W":
		$ApprovedStr = " and a.approved = 1 ";
		$FolderType_str = " and a.FolderType='W' ";
		$li->field_array = array("a.onTop");
		break;
	case "F":
		$ApprovedStr = " and a.approved = 1 ";
		$FolderType_str = " and (a.FolderType='F' or a.FolderType='' or a.FolderType is null) ";
		$li->field_array = array("a.onTop");
		break;
	case "WA":
		$ApprovedStr = " and a.approved <> 1 ";
		$li->field_array = array("a.onTop");
		break;				
	default:
		$ApprovedStr = " and a.approved = 1 ";
		$FolderType_str = "";
		$li->field_array = array("a.DateModified");
		$li->page_size= $legroup->SharingLatestNo;
		break;
}
$excludedType = array();
if(!$legroup->isAccessPhoto())
	$excludedType[] = "'P'";
if(!$legroup->isAccessVideo())
	$excludedType[] = "'V'";
if(!$legroup->isAccessLinks())
	$excludedType[] = "'W'";
if(!$legroup->isAccessFiles())
	$excludedType[] = "'F'";
if(count($excludedType)>0)
	$FileType_str .= " AND a.FileType NOT IN (".implode(",",$excludedType).") ";
	
$TypeNameStr = $TypeImage . $TypeName;
$li->fieldorder2 = ", a.UserTitle ";

if($lu2007->isInGroup($GroupID) && $legroup->hasAdminFiles($UserID)) {
    $checkbox = "CONCAT('<input type=\"checkbox\" name=\"FileID[]\" value=\"', a.FileID ,'\">')";
}else {
    $checkbox = "CONCAT('<input type=\"hidden\" name=\"FileID[]\" value=\"', a.FileID ,'\">')";
}

if ($legroup->hasAdminFiles($UserID))
	$adminFileTitle = $checkbox;
else
	$adminFileTitle = "CONCAT(if(a.UserID=$UserID, $checkbox, '&nbsp;'))";

	/*
	concat(
					if(a.PublicStatus=0, concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_private.gif border=0 align=absmiddle title=\"". $eComm['Private'] ."\" alt=\"". $eComm['Private'] ."\">'), ''),
					if(a.onTop=1, concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_ontop.gif border=0 align=absmiddle title=\"". $eComm['Top'] ."\" alt=\"". $eComm['Top'] ."\">'), '')
				)
				*/
switch($DisplayType)
{
	case "4":		### thumbnail
			
		$sql = "
			SELECT
				a.FileID,
				CONCAT(
				$adminFileTitle,
				CASE 
			          WHEN a.FileType='W' THEN concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_website.gif border=0 align=absmiddle>')
			          WHEN a.FileType='V' THEN concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_video.gif border=0 align=absmiddle>')
			          WHEN a.FileType='P' THEN concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_photo.gif border=0 align=absmiddle>')
			          ELSE concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_file.gif border=0 align=absmiddle>')
			        END,
				'<a href=javascript:viewfile(', a.FileID, ') class=tablelink><strong>', a.UserTitle, '</strong></a>', 
					
				IF(LOCATE(';$UserID;',a.ReadFlag)=0, '<img src=$image_path/$LAYOUT_SKIN/alert_new.gif border=0 hspace=2 align=absmiddle>', ''),
				
				if(a.onTop=1, concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_ontop.gif border=0 align=absmiddle title=\"". $eComm['Top'] ."\" alt=\"". $eComm['Top'] ."\">'), '')
				),
			    	CONCAT('". $eComm_Field_Createdby .": <a href=mailto:', a.UserEmail, ' class=tablelink><strong>', a.UserName, '</strong></a>'),
			    	CONCAT('<a href=javascript:viewfile(', a.FileID, ') class=tablelink>', '<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_chat.gif border=0 align=absmiddle> ', count(b.CommentID), ' ". $eComm['Comment'] ."</a>')
			FROM 
				INTRANET_FILE a 
				left join INTRANET_FILE_COMMENT b using (FileId)
			WHERE 
				a.GroupID = '$GroupID' 
				$ApprovedStr 
				AND (a.Title like '%$keyword%' OR a.Description like '%$keyword%' OR a.UserTitle like '%$keyword%')
				and (a.FolderID != '' or a.FolderID is not null)
				$FileType_str
				$FolderID_str
			group by 
				a.FileID		
			
		";
		
		$li->IsColOff = "eCommShareList_4";
		$li->no_col = 4;
		break;
		
	default:		### list
	
		if($FileType=="WA")
		{
			$select_field_username = "";
			$select_field_comment = "";
			$select_field_status = "if(a.Approved=0, '". $eComm['WaitingApproval'] ."','". $eComm['Rejected'] ."'),";
		}
		else
		{
			$select_field_username = "CONCAT('<a href=mailto:', a.UserEmail, ' class=tablelink>', a.UserName, '</a>'),";
			$select_field_comment = "count(b.CommentID),";
			$select_field_status = "";
		}
		
		$sql = "
			SELECT
				concat(
				 CASE 
		          WHEN a.FileType='W' THEN concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_website.gif border=0 align=absmiddle>')
		          WHEN a.FileType='V' THEN concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_video.gif border=0 align=absmiddle>')
		          WHEN a.FileType='P' THEN concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_photo.gif border=0 align=absmiddle>')
		          ELSE concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_file.gif border=0 align=absmiddle>')
		        END,
					if(a.onTop=1, concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_ontop.gif border=0 align=absmiddle title=\"". $eComm['Top'] ."\" alt=\"". $eComm['Top'] ."\">'), '')
		        ),
				CONCAT('<a href=javascript:viewfile(', a.FileID, ') class=tablelink>', a.UserTitle, '</a>', IF(LOCATE(';$UserID;',a.ReadFlag)=0, '<img src=$image_path/$LAYOUT_SKIN/alert_new.gif border=0 hspace=2 align=absmiddle>', '')),
			    $select_field_username
			    $select_field_comment
			    $select_field_status
			    a.DateModified, 
			    $adminFileTitle,
			    a.FileID
			FROM 
				INTRANET_FILE a 
				left join INTRANET_FILE_COMMENT b using (FileId)
			WHERE 
				a.GroupID = '$GroupID' 
				$ApprovedStr 
				AND (a.Title like '%$keyword%' OR a.Description like '%$keyword%' OR a.UserTitle like '%$keyword%')
				and (a.FolderID != '' or a.FolderID is not null)
				$FileType_str
				$FolderID_str
			group by 
				a.FileID	
		";
		
		$li->IsColOff = "eCommShareList_List";
		$li->no_col = 6;
		
		// TABLE COLUMN
		$li->column_list .= "<td width='40'  nowrap> </td>\n";
		$li->column_list .= "<td align='left' nowrap>". $eComm['Share'] ."</td>\n";
		if($FileType!="WA")
		{
			$li->column_list .= "<td align='left'  nowrap>". $eComm['By'] ."</td>\n";
			$li->column_list .= "<td width='35' align='left'  nowrap>". $eComm['Comment'] ."</td>\n";
		}
		else
		{
			$li->column_list .= "<td width='100' align='left'  nowrap>". $eComm['ApprovalStatus'] ."</td>\n";
			$li->no_col = 5;
		}
		$li->column_list .= "<td width='90' align='left'  nowrap>". $i_LastModified ."</td>\n";
		if($lu2007->isInGroup($GroupID) && $legroup->hasAdminFiles($UserID))
			$li->column_list .= "<td width=1 >".$li->check("FileID[]")."</td>\n";
		else
			$li->column_list .= "<td width=1 ></td>\n";
		
		break;		
}		

//$li->field_array = array("a.UserTitle");
$li->sql = $sql;

### Start Right Menu (Latest Share / Photo / Video / Website / File) ###
$rightMenu = "
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td height=\"32\" align=\"right\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_01.gif\"><table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
        <tr>
          <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t1.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t2.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"><span class=\"ecomm_box_title\">". $eComm['View']."</span></td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t3.gif\" width=\"5\" height=\"29\"></td>
              </tr>
          </table></td>
          <td align=\"center\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"left\">
";
if($lu2007->isInGroup($GroupID))
	$rightMenu .= "	<a href=\"javascript:jAJAX_RIGHT_MENU(document.rightmenuform, 'add');\"  class=\"share_sub_title\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_add.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">". $eComm['Add'] ."</a>";
else
	$rightMenu .= "&nbsp;";
$rightMenu .= "
                </td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
              </tr>
          </table></td>
        </tr>
    </table></td>
  </tr>
</table>
<br />
";
$rightMenu .= $legroup->RightViewMenu();
### Start Right Menu ###

### Start Display mode icons ###
$DisplayIcons = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr>";

$DisplayIcons .= "<td>";
if(!$DisplayType)
	$DisplayIcons .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_list_on.gif\" name=\"view_list\" width=\"24\" height=\"20\" border=\"0\" id=\"view_list\" >";
else
	$DisplayIcons .= "<a href=\"index.php?GroupID={$GroupID}&FileType={$FileType}&FolderID={$FolderID}\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_list.gif\" name=\"view_list\" width=\"24\" height=\"20\" border=\"0\" id=\"view_list\" onMouseOver=\"MM_swapImage('view_list','','{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_list_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a>";	
$DisplayIcons .= "</td>";

$DisplayIcons .= "<td>";
if($DisplayType==4)
	$DisplayIcons .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_4_on.gif\" name=\"view_4\" width=\"24\" height=\"20\" border=\"0\" id=\"view_4\">";
else
	$DisplayIcons .= "<a href=\"index.php?GroupID={$GroupID}&FileType={$FileType}&DisplayType=4&FolderID={$FolderID}\" onMouseOver=\"MM_swapImage('view_4','','{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_4_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_4.gif\" name=\"view_4\" width=\"24\" height=\"20\" border=\"0\" id=\"view_4\"></a>";
$DisplayIcons .= "</td>";

if($FolderID)
{
	$DisplayIcons .= "<td>";
	if($DisplayType==1)
		$DisplayIcons .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_1_on.gif\" name=\"view_1\" width=\"24\" height=\"20\" border=\"0\" id=\"view_1\">";
	else
		$DisplayIcons .= "<a href=\"javascript:viewfile('')\" onMouseOver=\"MM_swapImage('view_1','','{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_1_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_1.gif\" name=\"view_1\" width=\"24\" height=\"20\" border=\"0\" id=\"view_1\"></a>";
	$DisplayIcons .= "</td>";
}

$DisplayIcons .= "</tr></table>";
### End Display mode icons ###     


### Title ###

$title = "
	<table width='100%' height='25' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' width='20%' class='contenttitle' nowrap> <span onclick=\"window.location.href='{$PATH_WRT_ROOT}home/eCommunity/group/index.php?GroupID={$GroupID}'\" class='contenttitle GroupTitleLink'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/icon_egroup_school.gif' align='absmiddle'/>{$legroup->TitleDisplay}</span></td>
			<td align='right' width='80%'>".$legroup->displayStorage3()."</td>
			<td align='right' nowrap>{$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ[] = array($title,"");
// $MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
if($_SESSION['UserType']==USERTYPE_ALUMNI)
{
	$MODULE_OBJ['title'] = $legroup->TitleDisplay;
}
else
{
	$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
}
$ReturnMsg = $Lang['General']['ReturnMessage'][$msg];
$linterface->LAYOUT_START($ReturnMsg);

if($lu2007->isInGroup($GroupID) && $legroup->hasAdminFiles($UserID))
{
	$delBtn 	= "<a href=\"javascript:checkRemoveThis(document.form1,'FileID[]','file_remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_delete . "</a>";
	$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'FileID[]','file_edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";
}
?>

<script type="text/javascript" src="<?=$http_protocol?><?=$eclass_httppath?>/src/includes/js/ajax_yahoo.js" ></script>
<script type="text/javascript" src="<?=$http_protocol?><?=$eclass_httppath?>/src/includes/js/ajax_connection.js" ></script>
<script language="JavaScript">
function newFile(t)
{
         with(document.form1)
         {
                action = t+"_new.php";
                submit();
         }
}

function viewfile(fileid)
{
	 with(document.form1)
	 {
	     if(fileid == '')
             fileid = $('[name=FileID[]]').val();

		FileID.value = fileid;
		action = "file_view.php";
		submit();
	 }
}

var callback_rightmenu = 
{
	success: function (o)
	{
		jChangeContent( "div_right_menu", o.responseText );						
	}
		
}
	
function jAJAX_RIGHT_MENU( jFormObject, mt ) 
{
	jFormObject.menutype.value=mt;
	
	YAHOO.util.Connect.setForm(jFormObject);
	path = "aj_right_menu.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_rightmenu);		
	
	if(mt=="view")
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_view_02.gif"; 
	else
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_add_02.gif"; 

}

function checkRemoveThis(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm("<?=$eComm['jsDeleteRecord']?>")){	            
                obj.action=page;                
                obj.method="POST";
                obj.submit();				             
                }
        }
}
</script>


<br>
<form name="form1" action="" method="get">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="<?=$FolderID;?>">
<input type="hidden" name="FileID" value="">
<?php echo csrfTokenHtml(generateCsrfToken()); ?>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td width="5" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_01.gif" width="5" height="28"></td>
	            <td height="28" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_02.gif"><table width="100%" height="28" border="0" cellpadding="0" cellspacing="0">
	                <tr>
	                  <td align="left"><table border="0" cellspacing="0" cellpadding="0">
	                    <tr>
	                      <td width="9"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_01.gif" width="9" height="28"></td>
	                      <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_02.gif"><span class="ecomm_box_title"><?=$TypeNameStr?></span></td>
	                      <td width="9"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_03.gif" width="9" height="28"></td>
	                    </tr>
	                  </table></td>
	                  <td align="right" valign="top"><a href="../group/index.php?GroupID=<?=$GroupID?>"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_off.gif" name="a_emini" width="16" height="16" border="0" id="a_mini" onMouseOver="MM_swapImage('a_mini','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_on.gif',1)" onMouseOut="MM_swapImgRestore()" alt="<?=$Lang['eComm']['BackToHome']?>" title="<?=$Lang['eComm']['BackToHome']?>"></a></td>
	                </tr>
	              </table></td>
	            <td width="6" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_03.gif" width="6" height="28"></td>
	            <td width="130" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="130" height="10"></td>
	            <td width="13">&nbsp;</td>
	          </tr>
	          <tr>
	            <td width="5" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_04.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_04.gif" width="5" height="240"></td>
	            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr>
	                <td align="right"><?=$DisplayIcons?></td>
	              </tr>
	              
	              <tr>
	                <td align="left"><?= $linterface->GET_NAVIGATION3($PAGE_NAVIGATION) ?></td>
	              </tr>
	              
	              <tr>
	                	<td align="center">
	                	<table width="95%" border="0" cellspacing="0" cellpadding="0">
	                	<tr>
	                		<td align="right"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
						</tr>
                            <tr><br/></tr>
						<tr class="table-action-bar">
	                		<td align="right" nowrap><?=$editBtn?> <?=$delBtn?> </td>
						</tr>
						<? if($FileType=="WA") { ?>
						<tr>
		                	<td><?=$Lang['eComm']['Waiting_Reject_Remark']?></td>
		                </tr>
		                <? } ?>
						</table>
						</td>
	              	</tr>
	              	
	              <tr>
	                <td>
	                
	                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                
	                  <tr>
	                    <td align="center"><?=$li->display('file');?></td>
	                  </tr>
	                </table>                
	                </td>
	              </tr>
	            </table>
	            </td>
	            <td width="6" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_06.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_06.gif" width="6" height="17"></td>
	            
	            <td width="130" align="right" valign="top" bgcolor="#eff9e0" style="background-image:url(<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_middle_01.gif); background-repeat:no-repeat; background-position:bottom left;">
	            <div id="div_right_menu"><?=$rightMenu?></div>	
	            </td>
	            
	            <td width="13" valign="top" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_middle_02.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_top_view_02.gif" width="13" height="32" id="rightmenuimg"></td>
	          </tr>
	          <tr>
	            <td width="5" height="29"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_07.gif" width="5" height="29"></td>
	            <td height="29" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr>
	                <td align="left"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08a.gif" width="19" height="29"></td>
	                <td align="right"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08b.gif" width="261" height="29"></td>
	              </tr>
	            </table></td>
	            <td width="6" height="29"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_09.gif" width="6" height="29"></td>
	            <td width="130" height="29" align="left" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_02.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_01.gif" width="124" height="29"></td>
	            <td width="13"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_03.gif" width="13" height="29"></td>
	          </tr>
	        </table>
		</td>
	</tr>
</table>        
</form>

<form name="rightmenuform">
<input type="hidden" name="menutype" value="view">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="">
</form>

<br>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>