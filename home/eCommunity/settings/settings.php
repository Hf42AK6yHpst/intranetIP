<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
include_once("../tooltab.php");
include_once ("../tab.php");

$GroupID = IntegerSafe($GroupID);

$lg = new libgrouping();
$li = new libgroup($GroupID);
if ($li->hasAdminBasicInfo($UserID))
{
    include_once("../../../templates/fileheader.php");
    $Groups = $lg->returnAdminGroups($UserID);

    $groupSelect .= jumpIcon().$li->getSelectAdminBasicInfo("name=GroupID onChange=\"this.form.action=''; this.form.method='get'; this.form.submit();\"",$GroupID);
# From tab.php
$grp_navigation .= "";

$toolbar .= "<input type=image src=$image_save>";
$toolbar .= " <input type=image src=$image_reset onClick=' this.form.reset(); return false;'>";
    ?>
<form name=form1 action="settings_update.php" method=post>
<table width="750" border="0" cellspacing="0" cellpadding="0">

      <tr><td><?=$xmsg?></td></tr>
</table>
<table width="750" border="0" cellspacing="0" cellpadding="0">
<tr><td>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width=101 class=popup_top><?=$i_grouphead_groupsetting?></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td width=100% class=popup_topcell><font color="#FFFFFF"><?php echo $li->Title; ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
      <?=$header_tool?>
      <table width="750" border="0" cellspacing="0" cellpadding="10" background="<?=$image_path?>/groupinfo/popup_tbbg_large.gif">
        <tr>
            <td width="1"><img src="/images/spacer.gif" width="1" height="1">
            </td>

      <td align=left>
      <?=$groupSelect?>
      </td></tr>
          <tr>
            <td colspan=2 align="right">
              <table width="390" border="0" cellspacing="0" cellpadding="0" class="functionlink">
                <tr>
                  <td align="right" bgcolor="#EDDA5C">
                    <div align="center"><?=$grp_navigation?></div>
                  </td>
                  <td width="12"><img src="/images/spacer.gif" width="12" height="1"></td>
                </tr>
              </table>
              <br>
            </td>
          </tr>
      </table>
      <table width="750" border="0" cellspacing="0" cellpadding="10" background="<?=$image_path?>/groupinfo/popup_tbbg_large.gif">

        <tr>
            <td width="20"><img src="/images/spacer.gif" width="20" height="1">
            </td>
            <td width="730" class="h1">
 				<font color="#031BAC"><?=$i_GroupSettingsBasicInfo?></font>
            </td>
          </tr>
          <tr align="center">
            <td colspan="2"><img src="/images/line.gif" width="616" height="1"></td>
          </tr>
          <tr>
          <td colspan=2 align=center>
    <table width=93% border=0 cellpadding=2 cellspacing=0 align=center>
    <tr><td align=right></td><td><?=$i_GroupSettingDescription?></td></tr>
    <tr><td align=right></td><td height=15>&nbsp;</td></tr>
    <tr><td align=right><?=$i_image_home?> <?php echo $i_GroupURL; ?>:</td><td><input type=text name=URL size=60 VALUE='<?php echo $li->URL; ?>'></td></tr>
    <tr><td align=right><?php echo $i_GroupDescription; ?>:</td><td><textarea name=Description COLS=60 ROWS=6><?php echo $li->Description; ?></textarea></td></tr>
    <tr><td></td><td><?=$toolbar?></td></tr>
    </table>
          </td>
          </tr>
        </table>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="<?=$image_path?>/groupinfo/popup_bottom_large.gif"></td>
        </tr>
      </table>

    </td>
  </tr>

</table>

</form>
    <?php
    include_once("../../../templates/filefooter.php");
}
else
{
    header ("Location: ../settings/?GroupID=$GroupID");
}
intranet_closedb();
?>