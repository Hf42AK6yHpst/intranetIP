<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libbulletin.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$BulletinID = IntegerSafe($BulletinID);
$ThreadID = IntegerSafe($ThreadID);

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$linterface 	= new interface_html();
$CurrentPage	= "MyGroups";
$legroup 		= new libegroup($GroupID);

$ThreadID += 0;
$libull = new libbulletin($BulletinID, $ThreadID);
$lu = new libgroup($GroupID);
$lg = new libgrouping();
$lo = new libuser2007($UserID);
$libdb = new libdb();

if(!$lo->isInGroup($GroupID) || !$lo->isInGroup($libull->GroupID))
{
	header("Location: ../index.php?msg=AccessDenied");
	exit;
}

########################################################
//SELECT a.BulletinID, a.ParentID, a.GroupID, a.UserID, a.UserName, a.UserEmail, a.Subject, a.Message, a.ReadFlag, a.RecordType, a.RecordStatus, a.DateInput, a.DateModified, b.Photolink, a.Attachment, a.publicstatus FROM INTRANET_BULLETIN a , INTRANET_USER b WHERE (a.ParentID = '$BulletinID' OR a.BulletinID = '$BulletinID') and a.UserID = b.UserID ORDER BY a.BulletinID
//$sql = "SELECT a.BulletinID FROM INTRANET_BULLETIN a , INTRANET_USER b WHERE (a.ParentID = '$BulletinID' OR a.BulletinID = '$BulletinID') and a.UserID = b.UserID ORDER BY a.BulletinID";
$sql = "SELECT a.BulletinID FROM INTRANET_BULLETIN a , INTRANET_USER b WHERE (a.ParentID = '$BulletinID' OR a.BulletinID = '$BulletinID') and a.UserID = b.UserID ORDER BY a.BulletinID";


$Result = $libdb->returnArray($sql, 2);
$row = sizeof($Result)-1;



# TABLE INFO
$pageSizeChangeEnabled = true;
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("BulletinID");
$li->sql = $sql;
$li->total_row = $row-1;
$li->IsColOff = "eCommDisplayMessage";
$li->no_col = 5;
########################################################

$Subject = $libull->replySubject();
$ReplyTB = $libull->replyTable();
#Reply table
/*$ReplyTB .="<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
          <tr ><td align=\"left\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"><span class=\"tabletext\"><strong>".$eComm['YourReply']."</strong></span></td></tr>
          <tr ><td align=\"left\" class=\"tabletext\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
          <tr class=\"forumtabletoptext\"><td width=\"100\">".$eComm['Title']."</td>  
              <td><input name=\"Subject\" type=\"text\" class=\"textboxtext2\" value=\"".$Subject."\"></td>  
          </tr>
          <tr class=\"forumtabletoptext\"><td>".$eComm['Content']."</td>
              <td>". $linterface->GET_TEXTAREA("Message","$Message") ."</td>   
          </tr>
              </table></td>
          </tr>
		  <tr>
            <td align=\"right\" class=\"dotline\">&nbsp;</td>
          </tr>
          <tr>
            <td align=\"center\">
            ".
				$linterface->GET_ACTION_BTN($button_submit, "button", "javascript:bulletin_post(document.form1)","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")."\n".
                $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")."\n".
				$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php?GroupID=".$GroupID."'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")
          ."</td></tr></table>"; */
          
### Button


$ForumIcon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_forum.gif\" width=\"25\" height=\"20\" align=\"absmiddle\" /><span class=\"ecomm_forum_title\">".$eComm['Forum']."</span>";
if($_SESSION['UserType']!=USERTYPE_ALUMNI)
$MyGroupSelection= $lu->getSelectFilesGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID);

$title = "
	<table width='100%' height='28' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' class='contenttitle' nowrap> <div onclick=\"window.location.href='{$PATH_WRT_ROOT}home/eCommunity/group/index.php?GroupID={$GroupID}'\" class='contenttitle GroupTitleLink'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/icon_egroup_school.gif' align='absmiddle'/>{$lu->TitleDisplay}</div></td>
			<td align='right' width='70%'>".$lu->displayStorage3()."</td>
			<td align='right' nowrap>{$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ[] = array($title,"");
if($_SESSION['UserType']==USERTYPE_ALUMNI)
{
	$MODULE_OBJ['title'] = $legroup->TitleDisplay;
}
else
{
	$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
}
$linterface->LAYOUT_START();
?>

<script language="javascript">
function checkRemoveThis(obj,element,page,filename){
        
	if(confirm("<?=$eComm['jsDeleteAttachment']?>")){	    
		obj.delfilename.value=filename;				
		obj.action=page;                
		obj.method="POST";
		obj.submit();				             
                        }
}
function bulletin_thread(id){
        obj = document.form1;
        obj.ThreadID.value = id;
        obj.submit();
}
function del(id){
        obj = document.form1;
        obj.BulletinID.value = id;
        if(confirm(globalAlertMsg3)){
        checkGet(obj,'removeMsg.php');
        }
}

function bulletin_checkform(obj){
        if(!check_text(obj.Subject, "<?php echo $i_frontpage_bulletin_alert_msg1;?>.")) return false;
        if(!check_text(obj.Message, "<?php echo $i_frontpage_bulletin_alert_msg2;?>.")) return false;
        return true;
}

function bulletin_post(obj){
        if(bulletin_checkform(obj)){
                obj.action = "message_update.php";
                obj.method = "post";
                obj.submit();
        }
}
</script>
<form name="form1" method="get" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
        	<br>
        	<tr>
		      <td width="12" height="25"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_01.gif" width="12" height="25"></td>
		      <td width="13" height="25"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_02.gif" width="13" height="25"></td>
		      <td height="25" align="right" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_02.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		          <tr>
		            <td align="left"><?=$ForumIcon?></td>
		            <td align="right"><a href="../group/index.php?GroupID=<?=$GroupID?>"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_off.gif" name="a_mini" width="16" height="16" border="0" id="a_mini" onMouseOver="MM_swapImage('a_mini','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_on.gif',1)" onMouseOut="MM_swapImgRestore()"  alt="<?=$Lang['eComm']['BackToHome']?>" title="<?=$Lang['eComm']['BackToHome']?>"></a><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_03b.gif" width="16" height="25" align="absmiddle"></td>
		          </tr>
		      </table></td>
		      <td width="18" height="25"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="18" height="25"></td>
		    </tr>
		    <tr>
      <td width="12" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_05.gif" width="12" height="28"></td>
      <td width="13" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_06.gif" width="13" height="28"></td>
      <td height="28" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_07.gif">

      <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>&nbsp;</td>
            <td align="right"><a href="index.php?GroupID=<?=$GroupID?>" class="contenttool"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_prev_off.gif" width="11" height="10" border="0" align="absmiddle"> <?=$button_back?> </a></td>
          </tr>
      </table></td>
      <td width="18" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_08.gif" width="18" height="28"></td>
    </tr>
    		<tr>
      			<td width="12" valign="bottom" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_05.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_09b.gif" width="12" height="19"></td>
      			<td width="13" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_10.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_10.gif" width="13" height="200"></td>
      			<td valign="top" bgcolor="#fffeed">
				
      			<!--                 topic start                 -->
      			
				<?php echo $li->display($BulletinID); ?>
				
				<table width="100%" border="0" cellspacing="0" cellpadding="3">
		          <tr>
		            <td bgcolor="#fef9d4">
		            <table width="100%" border="0" cellspacing="0" cellpadding="3">
		              <tr>
		                <td align="left" class="tabletext"><?php echo $li->navigation();?></td>
		              </tr>
		            </table></td>
		          </tr>
				</table>

      			<!--end-->
      			
      			<br>
      			<table width="100%" border="0" cellspacing="0" cellpadding="3">
      			<?php echo $ReplyTB?>
      			</table>
      			</td>
      			
      			
      			<td width="18" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_12.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_12.gif" width="18" height="19"></td>
    			</tr>
    			<tr>
			      <td width="12" height="31"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="12" height="31"></td>
			      <td width="13" height="31"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_14.gif" width="13" height="31"></td>
			      <td height="31" align="right" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_15.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_15b.gif" width="170" height="31"></td>
			      <td width="18" height="31"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_16.gif" width="18" height="31"></td>
			    </tr>
			
			
            </table>
        </td>
</tr>
</table>
<input type="hidden" name="delfilename" value="">
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=BulletinID value="<?php echo $BulletinID; ?>">
<input type=hidden name=GroupID value="<?php echo $GroupID; ?>">
</form>
 
<!--<script language="javascript">

document.form1.Message.value = "";

</script>-->
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>