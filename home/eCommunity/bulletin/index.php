<?php
# being modified by: 


/********************** Change Log ***********************/
#	Date:	2014-08-18	Carlos
#			Simplified the query and make it faster
#
#	Date:	2012-09-26	YatWoon
#			improved: display English/Chinese name if nickname is empty
#
#	Date:	2012-08-15 YatWoon
#			Improved: Remove "Private/Public" filter
#
#	Date:	2012-02-09 YatWoon
#			Improved: Keep track the last accessed group id
#
# 	Date	:	2009-12-18 [Yuen]
#	Details	:	changed to show public/private topics by default
#
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");


intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$linterface 	= new interface_html();

$GroupID = IntegerSafe($GroupID);

$legroup 		= new libegroup($GroupID);
$lu = new libgroup($GroupID);
$lu2007 = new libuser2007($UserID);

### keep track the last accessed Group ID
$legroup->UpdateLastAccess($GroupID);


if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;


if($pstatus=="")$pstatus=-1;

# TABLE SQL

if($field=="") $field=3;
$order = ($order == 1) ? 1 : 0;

if($lu2007->isInGroup($GroupID))
{
// 	$cond1 = "CONCAT(if (c.PublicStatus=1 ,'&nbsp;', CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_private.gif\" alt=\"".$eComm['Private']."\">')),
// 	 							if (c.onTop=1 ,CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_ontop.gif\" alt=\"".$eComm['Top']."\">'),'&nbsp;' )),";

//	$cond1 = "CONCAT(if (c.onTop=1 ,CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_ontop.gif\" alt=\"".$eComm['Top']."\">'),'&nbsp;' )),";
	$cond1 = "CONCAT(if (a.onTop=1 ,CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_ontop.gif\" alt=\"".$eComm['Top']."\">'),'&nbsp;' )),";

}
else{
	header("Location: ../index.php?msg=AccessDenied");
	exit;
//$cond2 = "AND a.PublicStatus=1";
}
if($keyword)
{
	$cond2.= " AND (a.Subject LIKE '%$keyword%' OR
               a.Message LIKE '%$keyword%' OR
               a.UserName LIKE '%$keyword%')
				";

}

$pstatus_str = (isset($pstatus) && $pstatus!='-1') ? " and a.PublicStatus='$pstatus' " : "";
$name_field = getNameFieldByLang("e.");
/*
	$sql = "
	SELECT					".$cond1."
	                        CONCAT('<div style=\' position:relative; width:100%;\'><div name=\'topic\' style=\'overflow:hidden; position:absolute; width:100%;\'><a href=javascript:bulletin_view(', a.BulletinID, ') class=\"tablelink\">', a.Subject, '</a>',if(locate(';$UserID;',a.ReadFlag)=0, '<img src=$image_path/new.gif border=0 hspace=2>', ''),'</div></div>' ) as topic,
	                        if (e.PersonalPhotoLink!=\"\", CONCAT('<img src=\"',e.PersonalPhotoLink,'\" width=\"25\" height=\"30\">'), CONCAT('<img src=\"/images/myaccount_personalinfo/samplephoto.gif\" width=\"25\" height=\"30\">')),
	                        if(a.UserName='', $name_field, a.UserName) AS author,
	                        if(COUNT(d.BulletinID)=0, CONCAT(COUNT(c.BulletinID)-1), CONCAT(COUNT(c.BulletinID)-1 )) as reply,
	                        DATE_FORMAT(MAX(c.DateModified), '%Y-%m-%d %H:%i') AS postdate,
	                        if($UserID=a.UserID,CONCAT('<input type=\"checkbox\" name=\"BulletinIDArr[]\" value=\"', a.BulletinID ,'\">'),CONCAT('&nbsp;'))
	                FROM INTRANET_BULLETIN AS a
	                LEFT OUTER JOIN INTRANET_USER AS e ON (e.UserID = a.UserID)
	                LEFT OUTER JOIN INTRANET_BULLETIN AS c ON (a.BulletinID = c.BulletinID OR a.BulletinID = c.ParentID)
	                LEFT OUTER JOIN INTRANET_BULLETIN AS d ON
	                        (a.BulletinID = d.BulletinID OR a.BulletinID = d.ParentID) AND
	                        c.BulletinID = d.BulletinID AND
	                        d.ParentID<>0 AND
	                        d.ReadFlag not like '%;$UserID;%'

	                WHERE a.GroupID = '$GroupID' AND a.ParentID = 0
	                ".$cond2."
	                ".$pstatus_str."
	                GROUP BY a.BulletinID
	";
*/
$sql = "SELECT ".$cond1."
	            CONCAT('<div style=\' position:relative; width:100%;\'><div name=\'topic\' style=\'overflow:hidden; position:absolute; width:100%;\'><a href=javascript:bulletin_view(', a.BulletinID, ') class=\"tablelink\">', a.Subject, '</a>',if(locate(';$UserID;',a.ReadFlag)=0, '<img src=$image_path/new.gif border=0 hspace=2>', ''),'</div></div>' ) as topic,
	            if (e.PersonalPhotoLink!=\"\", CONCAT('<img src=\"',e.PersonalPhotoLink,'\" width=\"25\" height=\"30\">'), CONCAT('<img src=\"/images/myaccount_personalinfo/samplephoto.gif\" width=\"25\" height=\"30\">')),
	            if(a.UserName='', $name_field, a.UserName) AS author,
	            COUNT(d.BulletinID) as reply,
	            DATE_FORMAT(IF(d.BulletinID IS NULL, a.DateModified, MAX(d.DateModified)), '%Y-%m-%d %H:%i') AS postdate,
	            if($UserID=a.UserID,CONCAT('<input type=\"checkbox\" name=\"BulletinIDArr[]\" value=\"', a.BulletinID ,'\">'),CONCAT('&nbsp;'))
	    FROM INTRANET_BULLETIN AS a
	    LEFT JOIN INTRANET_USER AS e ON (e.UserID = a.UserID)
	    LEFT JOIN INTRANET_BULLETIN AS d ON
	            a.BulletinID = d.ParentID 
	            AND d.ParentID<>0 ";
//$sql .= " AND d.ReadFlag not like '%;$UserID;%' ";
$sql .= " WHERE a.GroupID = '$GroupID' AND a.ParentID = 0 
	    ".$cond2." 
	    ".$pstatus_str." 
	    GROUP BY a.BulletinID";
# TABLE INFO

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.Subject","a.UserName","reply","postdate");
$li->sql = $sql;


$li->IsColOff = "eCommForumList";
//$li->IsColOff = 2;
$li->no_msg = $i_frontpage_bulletin_no_record;


// TABLE COLUMN

if($lu2007->isInGroup($GroupID))
	$li->column_list .= "<td class='forumtabletop forumtabletoptext' width=\"18\" nowrap></td>\n";
$li->column_list .= "<td class='forumtabletop forumtabletoptext' nowrap width=\"60%\">".$li->column(0, $eComm_Field_Topic)."</td>\n";
$li->column_list .= "<td class='forumtabletop forumtabletoptext' width=\"20\" nowrap ></td>\n";
$li->column_list .= "<td class='forumtabletop forumtabletoptext' nowrap>".$li->column(1, $eComm_Field_Createdby)."</td>\n";
$li->column_list .= "<td class='forumtabletop forumtabletoptext' width=\"80\" nowrap>".$li->column(2, $eComm_Field_ReplyNo)."</td>\n";
$li->column_list .= "<td class='forumtabletop forumtabletoptext' width=\"100\" nowrap>".$li->column(3, $eComm_Field_LastUpdated)."</td>\n";
$li->column_list .= "<td class='forumtabletop forumtabletoptext' width=\"2%\" nowrap>".$li->check("BulletinIDArr[]")."</td>\n";
$li->column_array=array(0,0,13,0,0,0,0);
$li->no_col = 6;


/*
$searchbar 	.= "&nbsp;<select name='pstatus' onChange='checkPost(this.form,\"index.php\")'>\n";
$searchbar 	.= "<option value='-1'>". $eComm['Public'] ." / " . $eComm['Private'] ."</option>\n";
$searchbar 	.= "<option value='1' ".(($pstatus==1)?"selected":"").">". $eComm['Public'] ."</option>\n";
$searchbar 	.= "<option value='0' ".(($pstatus==0 && $pstatus!="")?"selected":"").">". $eComm['Private'] ."</option>\n";
$searchbar 	.= "</select>\n";
*/
$searchbar .= "<input class=\"tabletext\" type=\"text\" name=\"keyword\" size=\"15\" maxlength=\"30\" value=\"$keyword\">\n";
$searchbar .= $linterface->GET_BTN($button_search, "submit")."\n";

if($lu2007->isInGroup($GroupID))
{
	$new_btn = "<a href=\"javascript:checkPost(document.form1,'new.php')\" class=\"contenttool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$i_frontpage_bulletin_postnew}</a>";
	$markread_btn = "<a href=\"javascript:checkPost(document.form1,'mark.php')\" class=\"contenttool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_set_forum_read.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$i_frontpage_bulletin_markallread}</a>";
}


### Button

$TypeStr = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_forum.gif\" width=\"25\" height=\"20\" align=\"absmiddle\" /><span class=\"ecomm_forum_title\">".$eComm['Forum']."</span>";

if($lu2007->isInGroup($GroupID)) 
{
	$CurrentPage = "MyGroups";
	if($_SESSION['UserType']!=USERTYPE_ALUMNI)
	$MyGroupSelection= $lu->getSelectGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID);
}
else
{
	$CurrentPage = "OtherGroups";
	$MyGroupSelection= $lu->getSelectOtherGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID);
}
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'BulletinIDArr[]','removeThread.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_delete . "</a>";
//$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'BulletinIDArr[]','file_edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";

if($_SESSION['UserType']!=USERTYPE_ALUMNI)
$MyGroupSelection= $lu->getSelectFilesGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID);

$title = "
	<table width='100%' height='28' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' class='contenttitle' nowrap> <div onclick=\"window.location.href='{$PATH_WRT_ROOT}home/eCommunity/group/index.php?GroupID={$GroupID}'\" class='contenttitle GroupTitleLink'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/icon_egroup_school.gif' align='absmiddle'/>{$lu->TitleDisplay}</div></td>
			<td align='right' width='70%'>".$lu->displayStorage3()."</td>
			<td align='right' nowrap>{$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ[] = array($title,"");
if($_SESSION['UserType']==USERTYPE_ALUMNI)
{
	$MODULE_OBJ['title'] = $legroup->TitleDisplay;
}
else
{
	$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
}
$linterface->LAYOUT_START();

?>
<script language="javascript">
function bulletin_view(id){
        obj = document.form1;
        obj.BulletinID.value = id;
        obj.action = "message.php";
        obj.submit();
}
function del(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm("<?=$eComm['jsDeleteRecord']?>")){
                obj.action=page;
                obj.method="POST";
                obj.submit();
                }
        }
}

function checkRemoveThis(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm("<?=$eComm['jsDeleteRecord']?>")){
                obj.action=page;
                obj.method="POST";
                obj.submit();
                }
        }
}

</script>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
        	<!---start--->
        	<tr>
		      <td width="12" height="25"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_01.gif" width="12" height="25"></td>
		      <td width="13" height="25"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_02.gif" width="13" height="25"></td>
		      <td height="25" align="right" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_02.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		          <tr>
		            <td align="left"><?=$TypeStr?></td>
		            <td align="right"><a href="../group/index.php?GroupID=<?=$GroupID?>"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_off.gif" name="a_mini" width="16" height="16" border="0" id="a_mini" onMouseOver="MM_swapImage('a_mini','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_on.gif',1)" onMouseOut="MM_swapImgRestore()"  alt="<?=$Lang['eComm']['BackToHome']?>" title="<?=$Lang['eComm']['BackToHome']?>"></a><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_03b.gif" width="16" height="25" align="absmiddle"></td>
		          </tr>
		      </table></td>
		      <td width="18" height="25"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="18" height="25"></td>
		    </tr>
		    <tr>
		      <td width="12" height="28" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_05.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_05.gif" width="12" height="28"></td>
		      <td width="13" height="28" valign="top" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_10.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_06.gif" width="13" height="28"></td>
		      <form name="search"  method="get">
		      <input type="hidden" name="GroupID" value="<?php echo $GroupID; ?>">
		      <td height="28" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_07.gif">



	           <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" height="29"><table border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td><?=$new_btn?></td>
                <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif"></td>
                <td><?=$markread_btn?></td>
                <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif"></td>
                <td><?=$searchbar?></td>
                <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif"></td>
              </tr>
            </table></td>
            <td align="right"><?=$linterface->GET_SYS_MSG($msg);?></td>
          </tr>
          <tr>
            <td align="right"><table border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td nowrap></td>
                <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
                <td nowrap><?=$delBtn?></td>
              </tr>
            </table></td>
            <td align="right"><?=$editBtn?></td>
          </tr>
      </table>



		      </td>
		      </form>
    		  <td width="18" height="28" valign="top" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_12.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_08.gif" width="18" height="28"></td>
    		</tr>

    		<tr>
      			<td width="12" valign="bottom" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_05.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_09b.gif" width="12" height="19"></td>
      			<td width="13" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_10.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_10.gif" width="13" height="200"></td>
      			<td bgcolor="#fffeed">
      			<form name=form1 method=get>
      			<?php echo $li->display(); ?>
      			<input type=hidden name=from value="index.php">
				<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
				<input type=hidden name=order value="<?php echo $li->order; ?>">
				<input type=hidden name=field value="<?php echo $li->field; ?>">
				<input type=hidden name=page_size_change value="<?=$page_size_change?>" />
				<input type=hidden name=numPerPage value="<?=$li->page_size?>">
				<input type=hidden name=BulletinID value="<?=$li->BulletinID?>">
				<input type=hidden name=GroupID value="<?php echo $GroupID; ?>">
				</form>
      			</td>
      			<td width="18" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_12.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_12.gif" width="18" height="19"></td>
    			</tr>
    			<tr>
			      <td width="12" height="31"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="12" height="31"></td>
			      <td width="13" height="31"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_14.gif" width="13" height="31"></td>
			      <td height="31" align="right" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_15.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_15b.gif" width="170" height="31"></td>
			      <td width="18" height="31"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_16.gif" width="18" height="31"></td>
			    </tr>

			<!--end--->
            </table>
        </td>
</tr>
</table>


<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>