<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libbulletin.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$BulletinIDArr = IntegerSafe($BulletinIDArr);


$linterface = new interface_html();
$lgroup 	= new libgroup($GroupID);
$legroup 	= new libegroup($GroupID);
$lo 		= new libgrouping();

$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,7) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../settings/index.php?GroupID=$GroupID");
	exit;
}
$BulletinID = (is_array($BulletinIDArr)? $BulletinIDArr[0]:$BulletinIDArr);
if ($BulletinID=="")
	$BulletinID= IntegerSafe($_GET['BulletinID']);
	

$lbulletin	= new libbulletin($BulletinID);

$hasAccessRight = $lgroup->isAccessBulletin($UserID);

if(!$hasAccessRight)
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$CurrentPage	= "Settings_ForumSettings";
$MyGroupSelection= $legroup->getSelectAdminBulletin("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);

$lg = new libgroup($GroupID);
{
    if ($lg->hasAdminBulletin($UserID))
    {
        $valid = true;
    }
   
    else
    {
        $valid = false;
    }
}

if (!$valid)
{
	header ("Location: ../close.php");
	exit;
}

     
$PublicStatus=$lbulletin->Thread[0]['PublicStatus'];
$ReplyFlag=$lbulletin->Thread[0]['ReplyFlag'];
$OnTop=$lbulletin->Thread[0]['OnTop'];


$toolbar = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submit2") . " ". $linterface->GET_ACTION_BTN($button_cancel, "button", "history.go(-1)","cancelbtn");
    


########################################################
$libdb = new libdb();
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$sql = "
	SELECT					CONCAT(if (c.PublicStatus=1 ,'&nbsp;', CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_private.gif\">')),
	 							if (c.onTop=1 ,CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_ontop.gif\">'),'&nbsp;' )),
	                        CONCAT('<a href=javascript:bulletin_view(', a.BulletinID, ') class=\"tablelink\">', a.Subject, '</a>',if(locate(';$UserID;',a.ReadFlag)=0, '<img src=$image_path/new.gif border=0 hspace=2>', '') $deleteIcon) ,
	                        
	                        a.UserName ,
	                        
	                        DATE_FORMAT(MAX(c.DateModified), '%Y-%m-%d %H:%i') AS postdate,
	                        CONCAT('<input type=\"checkbox\" name=\"BulletinIDArr[]\" value=\"', a.BulletinID ,'\">')
	                FROM INTRANET_BULLETIN AS a
	                LEFT OUTER JOIN INTRANET_USER AS e ON (e.UserID = a.UserID)
	                LEFT OUTER JOIN INTRANET_BULLETIN AS c ON (a.BulletinID = c.BulletinID OR a.BulletinID = c.ParentID)
	                LEFT OUTER JOIN INTRANET_BULLETIN AS d ON
	                        (a.BulletinID = d.BulletinID OR a.BulletinID = d.ParentID) AND
	                        c.BulletinID = d.BulletinID
	                        
	                WHERE a.GroupID = '$GroupID' 
	                
	                AND (a.ParentID = '$BulletinID' OR a.BulletinID = '$BulletinID')
	                
	                GROUP BY a.BulletinID
	";


if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$li = new libdbtable2007($field, $order, $pageNo);
//$li->field_array = array("topic","author","postdate","response");
$li->field_array = array("a.Subject","a.UserName","postdate","");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;

$li->IsColOff = 2;


// TABLE COLUMN		
$li->column_list .= "<td class='tabletop tabletopnolink'>#</td>\n";
$li->column_list .= "<td width='18' class='tabletop tabletopnolink'>&nbsp;</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(0, $eComm_Field_Topic)."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(1, $eComm_Field_Createdby)."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(2, $eComm_Field_LastUpdated)."</td>\n";

$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("BulletinIDArr[]")."</td>\n";


########################################################
//$lbulletin->Thread=$BulletinID;


$delBtn 	= "<a href=\"javascript:checkRemoveThis(document.form1,'BulletinIDArr[]','removeMsg.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";

  ### Title ###


$title = "
	<table width='100%' border='0' cellpadding='0' cellspacing='0' valign=\"bottom\">
		<tr>
			<td valign='bottom' align='left' width='90%' nowrap> <span class='contenttitle'>". $eComm['ForumSettings'] ."</span></td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$lgroup->TitleDisplay."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ_RIGHT[] = array($title,"",0);
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit.($intranet_session_language=="en"?" ":""). $eComm['Forum'], "");

$Attachment_list = $lbulletin->displayAttachment();
if($Attachment_list)
{
	$DisplayAttachment = "
		<tr valign=\"top\">
			<td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">". $eComm['Attachment'] ." </span></td>
			<td class=\"tabletext\">". $Attachment_list ."</td>
		</tr>
	";
		
}


?>
     <script language="javascript">
     
function checkRemoveThis(obj,element,page,filename){
        
if(confirm("<?=$eComm['jsDeleteAttachment']?>")){	 
	obj.delfilename.value=filename;
	obj.action=page;                
	obj.method="POST";
	obj.submit();				             
     }
}

function updateSetting(obj, page){
                
	obj.action=page;                
	obj.method="POST";
	obj.submit();				             

}
function RemoveThread(id, page){
        
        if(confirm(globalAlertMsg3)){
        obj = document.form1;
        obj.ThreadID.value = id;
        obj.action=page;                
        obj.method="POST";
        obj.submit();	
        }
}
function bulletin_view(id){
	        obj = document.form1;
	        obj.BulletinID.value = id;
	        obj.action = "message_edit.php";
	        obj.submit();
	}
	
    </script>

<br />
<form name="form1" action="message_edit_update.php" method="post" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
			<td><br />
				<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0">
				
				
				<tr valign="top">
				<td><table border="0" width="100%"><tr>
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['Title']?></span></td>
					<td class="tabletext"><input name="Subject" type="text" class="textboxtext" maxlength="100"  value="<?=$lbulletin->Subject?>"/></td>
				</tr>
				
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['Message']?></span></td>
					<td class="tabletext">
					
					<?=$linterface->GET_TEXTAREA("Message",$lbulletin->Message);?>
					</td>
				</tr>
				
				<?=$DisplayAttachment?>
				
				</table></td></tr>
				
				
				
     
				
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?=$toolbar?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>	
	 <input type="hidden" name="delfilename" value="">
     <input type="hidden" name="AnnouncementID" value="<?php echo $li->AnnouncementID; ?>">
     <input TYPE="hidden" NAME="GroupID" value="<?=$GroupID?>">
     <? if ($newEdit)    {  ?>
     	<input type="hidden" name="MakeNew" value="1">
     <? } ?>
     <input type="hidden" name="attachment_size" value="1">
     <input type="hidden" name="ThreadID" value="">
     <input type="hidden" name="BulletinID" value="<?=$BulletinID?>">
     <input type="hidden" name="from" value="edit.php">
     <input type="hidden" name="pageNo" value="">
     <input type="hidden" name="page_size_change" value="">
     <input type="hidden" name="numPerPage" value="">
</form>

<?php
print $linterface->FOCUS_ON_LOAD("form1.AnnouncementDate");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>