<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libuser.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$BulletinID = IntegerSafe($BulletinID);

$li = new libuser($UserID);

if($li->IsGroup($GroupID)){
	$sql = "UPDATE INTRANET_BULLETIN SET ReadFlag = CONCAT(ReadFlag,';$UserID;') WHERE GroupID = '$GroupID'";
	$li->db_db_query($sql);
}

intranet_closedb();
header("Location: index.php?num_per_page=$num_per_page&from=$from&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&BulletinID=$BulletinID&GroupID=$GroupID");
?>