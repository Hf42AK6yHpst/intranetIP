<?php
## Using By : yat
#############################################
##
#	Date:	2012-08-15 YatWoon
#			Improved: Remove "Private/Public" filter
#
##	Date:	2010-06-21 [YatWoon]
##			Add "new" icon that allow admin create forum topic from "Settings"
##
##	Modification Log:
##	2010-03-16 Max (201003051446)
##	- $legroup->UPDATE_GROUP_TO_DB(); <== put inside an if case (only update the forum enable/disable if $AllowForum is found)
##
##	Date:	2010-02-25 YatWoon
##			Update the query use "a.PublicStatus" instead of "c.PublicStatus"
##
##	2010-01-04: Max (200912311437)
##	- Setting enable forum
##	- Modified the checking of AdminAccessRight from !(substr($legroup->AdminAccessRight,2,1) to !(substr($legroup->AdminAccessRight,7,1)
##	  where 2 is the checking of internal announcement and 7 is forum(bulletin)
#############################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libbulletin.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$BulletinID = IntegerSafe($BulletinID);

$linterface = new interface_html();
$legroup = new libegroup($GroupID);
///// temp for debug /////
//echo "testing by max";
//$legroup->setFunctionAccess(libegroup::DISABLE, libegroup::FA_BULLETIN_BIT_POS);
//die;
/// temp for debug /////
### START check user access rights ###
$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,7,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/index.php?GroupID=$GroupID");
	exit;
}

$bu = new libbulletin($BulletinID, $ThreadID); 
$bu->updateAttachFlag($AllowAtt,$GroupID);
$ForumAttStatus = $bu->CheckForumAttStatus($GroupID);
$ForumAttStatus = $ForumAttStatus[0]['ForumAttStatus'];


if($pstatus=="")$pstatus=1;

### END check user access rights ###

### Set enable forum ###
# compose string for SQL updating FunctionAccess of chat room
//$AllowForum = isset($_GET['AllowForum'])?$_GET['AllowForum']:"";
//
//if ($AllowForum !== "") {
//	$FuncAccessValue_Forum = $_GET['FuncAccessValue_Forum'];
//	
//	$functionAccess = pow(2, $FuncAccessValue_Forum);
//	$fa = "";
//	if ($AllowForum == "1") 
//	{
//		if (!$legroup->isAccessTool($FuncAccessValue_Forum)) 
//		{
//			$fa = $legroup->returnGroupFunctionAccessValue($legroup->FunctionAccess) + $functionAccess;
//			$fa = $fa == 255 ? "NULL" : $fa;
//		}
//	}
//	else
//	{
//		if ($legroup->isAccessTool($FuncAccessValue_Forum)) 
//		{
//			$fa = $legroup->returnGroupFunctionAccessValue($legroup->FunctionAccess) - $functionAccess;
//		}
//	}
//	if($fa)
//	{
//		$functionAccessString = " FunctionAccess=$fa";
//	}
//	$sql = "UPDATE INTRANET_GROUP SET $functionAccessString WHERE GroupID = $GroupID";
//	$li = new libdb();
//	$li->db_db_query($sql) or die(mysql_error());
//}

/// NEW FORMAT
$AllowForum = isset($_GET['AllowForum']) ? $_GET['AllowForum'] : "";

if ($AllowForum !== "") {
	if ($AllowForum == 1) {
		$legroup->setFunctionAccess(libegroup::ENABLE, libegroup::FA_BULLETIN_BIT_POS);
	}
	else {
		$legroup->setFunctionAccess(libegroup::DISABLE, libegroup::FA_BULLETIN_BIT_POS);
	}
	$legroup->UPDATE_GROUP_TO_DB();
	$legroup = new libegroup($GroupID);
}


### END Set enable forum ###

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$CurrentPage	= "Settings_ForumSettings";
$MyGroupSelection= $legroup->getSelectAdminBasicInfo("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);

### Title ###
$title = "
		<table width='100%' height='28' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$lu->TitleDisplay."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
";

$TAGS_OBJ[] = array($eComm['Management'],"setting.php?GroupID=$GroupID", 1);
$TAGS_OBJ[] = array($i_general_BasicSettings,"setting_basic.php?GroupID=$GroupID", 0);

$TAGS_OBJ_RIGHT[] = array($title,"",0);
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
$ReturnMsg = $Lang['General']['ReturnMessage'][$msg];
$linterface->LAYOUT_START($ReturnMsg);

$keyword = trim($keyword);
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        case 5: $field = 5; break;
        default: $field = 0; break;
}
if($field=="") $field=4;
$order = ($order == 1) ? 1 : 0;
if($filter == "") $filter = 1;

$pstatus_str = (isset($pstatus) && $pstatus!='-1') ? " and a.PublicStatus='$pstatus' " : "";	

# TABLE SQL

	//$cond1 = "if (c.PublicStatus=1 ,'&nbsp;', CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_private.gif\">')) ,";

	/*
	CONCAT(if (a.PublicStatus=1 ,'&nbsp;', CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_private.gif\" alt=\"".$eComm['Private']."\">')),
	 							if (c.onTop=1 ,CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_ontop.gif\" alt=\"".$eComm['Top']."\">'),'&nbsp;' )),
	 							*/
	$sql = "
	SELECT					CONCAT(if (c.onTop=1 ,CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_ontop.gif\" alt=\"".$eComm['Top']."\">'),'&nbsp;' )),
	                        CONCAT('<div style=\' position:relative; width:100%;\'><div name=\'topic\' style=\'overflow:hidden; position:absolute; width:100%;\'><a href=javascript:bulletin_edit(', a.BulletinID, ') class=\"tablelink\">', a.Subject, '</a></div></div>') ,
	                        
	                        a.UserName ,
	                        if(COUNT(d.BulletinID)=0, CONCAT(COUNT(c.BulletinID)-1), CONCAT(COUNT(c.BulletinID)-1 )) as Reply,
	                        DATE_FORMAT(MAX(c.DateModified), '%Y-%m-%d %H:%i') AS postdate,
	                        CONCAT('<input type=\"checkbox\" name=\"BulletinIDArr[]\" value=\"', a.BulletinID ,'\">')
	                FROM INTRANET_BULLETIN AS a
	                LEFT OUTER JOIN INTRANET_USER AS e ON (e.UserID = a.UserID)
	                LEFT OUTER JOIN INTRANET_BULLETIN AS c ON (a.BulletinID = c.BulletinID OR a.BulletinID = c.ParentID)
	                LEFT OUTER JOIN INTRANET_BULLETIN AS d ON
	                        (a.BulletinID = d.BulletinID OR a.BulletinID = d.ParentID) AND
	                        c.BulletinID = d.BulletinID AND
	                        d.ParentID<>0 AND
	                        d.ReadFlag not like '%;$UserID;%'
	                        
	                WHERE a.GroupID = '$GroupID' AND a.ParentID = 0
	                AND	(a.Subject like '%$keyword%') 
	                $pstatus_str
	                
	                GROUP BY a.BulletinID
	";


if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
$li = new libdbtable2007($field, $order, $pageNo);
//$li->field_array = array("topic","author","postdate","response");
$li->field_array = array("","a.Subject","a.UserName","Reply","postdate");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;

$li->IsColOff = 2;


// TABLE COLUMN		
$li->column_list .= "<td class='tabletop tabletopnolink'>#</td>\n";
$li->column_list .= "<td width='18' class='tabletop tabletopnolink'>&nbsp;</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink' width=60%>".$li->column(1, $eComm_Field_Topic)."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(2, $eComm_Field_Createdby)."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(3, $eComm_Field_ReplyNo)."</td>\n";

$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(4, $eComm_Field_LastUpdated)."</td>\n";

$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("BulletinIDArr[]")."</td>\n";

### Button
$AddBtn 	= "<a href=\"javascript:checkNew('new.php?GroupID=$GroupID&backto=settings')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'BulletinIDArr[]','edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'BulletinIDArr[]','removeThread.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_delete . "</a>";

$searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
$searchTag 	.= "<td>\n";

/*
$searchTag 	.= "&nbsp;<select name='pstatus' onChange='this.form.submit();'>\n";
$searchTag 	.= "<option value='-1'>". $eComm['Public'] ." / " . $eComm['Private'] ."</option>\n";
$searchTag 	.= "<option value='1' ".(($pstatus==1)?"selected":"").">". $eComm['Public'] ."</option>\n";
$searchTag 	.= "<option value='0' ".(($pstatus==0 && $pstatus!="")?"selected":"").">". $eComm['Private'] ."</option>\n";
$searchTag 	.= "</select>\n";
*/

$searchTag 	.= "&nbsp;<input type=\"text\" name=\"keyword\" id=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
$searchTag 	.= "</tr></table>";


?>
<SCRIPT LANGUAGE=Javascript>
function bulletin_edit(id){
        obj = document.form1;
        obj.BulletinID.value = id;
        //obj.method = "POST";
        obj.action = "edit.php";
        obj.submit();
}
function showRead(id)
{
         newWindow('read.php?BulletinIDArr='+id,1);
}


</SCRIPT>

<br />

<form name="form1" method="get" action="setting.php">
<input type="hidden" name="GroupID" value="<?=$GroupID?>">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	
	
	<? if($legroup->isAccessTool(2)) {		# 2 = bulletin  ?> 		
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
                                        	<table border="0" cellspacing="0" cellpadding="2">
                                            	<tr>
                                              		<td><p><?=$AddBtn?></p></td>
                                            	</tr>
                                          	</table>
					</td>
                                        <td align="right" valign="bottom"></td>
				</tr>
				
				<tr>
                                	<td class="tabletext">
                                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                	<td class="tabletext" nowrap><?=$select_status?></td>
                                                        <td width="100%" align="left" class="tabletext"><?=$searchTag?></td>
						</tr>
                                                </table>
					</td>
					<td align="right" valign="bottom" height="28">
						<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
							<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
								<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td nowrap><?=$editBtn?></td>
                                    <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
                                    <td nowrap><?=$delBtn?></td>
								</tr>
        							</table>
							</td>
        						<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
						</tr>
						</table>
					</td>
				</tr>
                                <tr>
                                	<td colspan="2">
						<?=$li->display();?>
					</td>
				</tr>
                                
                                
				</table>
			</td>
		</tr>
                </table>
                <? } else {?>
	                <?=$linterface->Get_Warning_Message_Box($Lang['General']['Warning'], $eComm['ForumDisabled']);?>
                <? } ?>
        </td>
</tr>
</table>
<br />
<input type=hidden name="BulletinID" value="">
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<?php
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.pstatus");
$linterface->LAYOUT_STOP();

?>
