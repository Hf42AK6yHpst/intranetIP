<?php

# 2019-06-06 Anna: Added CSRF token checking.

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

if(!verifyCsrfToken($_POST['csrf_token'],$_POST['csrf_token_key'])){
    header('Location: /');
    exit;
}
intranet_auth();
intranet_opendb();


$targetgroup = IntegerSafe($targetgroup);


$no_file = 5;



for ($i=0; $i<sizeof($_FILES); $i++)
	$FileSize += $_FILES['userfile'.$i]['size'];

$FileSize = $FileSize/1000;
$ExceedQuotaGroup=Array();

for($j=0; $j<sizeof($targetgroup);$j++)
{
	$lg = new libgroup($targetgroup[$j]);
	$used = $lg->returnGroupUsedStorageQuota();
	$available = ($lg->StorageQuota)*1000 - $used;

	if($available >= $FileSize)
	{
		$path = "$file_path/file/eComm/bulletin/$folder".$targetgroup[$j];
		
		if (!is_dir($path))
		{
			$path = $path."tmp";
		}

		$li = new libfilesystem();
		$li->folder_new($path);
		
		# remove user deleted files
		
		$lremove = new libfiletable("", $path, 0, 0, "");
		$files = $lremove->files;
		$attachStr=stripslashes($attachStr);
		while (list($key, $value) = each($files)) {
			 if(!strstr($attachStr,$files[$key][0])){
				  
				  $li->file_remove($path."/".$files[$key][0]);
			 }
		}
		
		for($i=1;$i<$no_file+1;$i++){
			 $loc = ${"userfile".$i};
			 //$file = stripslashes(${"hidden_userfile_name$i"});
			$file = stripslashes($_FILES['userfile'.$i]['name']);
			
			 $des = "$path/$file";
			 if($loc=="none"){
			 } else {
				  if(strpos($file, ".")==0){
				  }else{
					  
					   $li->lfs_copy($loc, $des);
				  }
			 }
		}
	}
	else
	{
		$ExceedQuotaGroup[]=$lg->Title;
		$ExceedQuotaGroupId[]=$targetgroup[$j];
	}
}
	echo "<script language=\"JavaScript1.2\">\n";
	echo "par = opener.window;\n";

	if(sizeof($ExceedQuotaGroup)!=sizeof($targetgroup))
	{
		$lo = new libfiletable("", $path, 0, 0, "");
		$files = $lo->files;

		echo "obj = opener.window.document.form1.elements[\"Attachment[]\"];\n";
		echo "obj2 = opener.window.document.form1.elements[\"filesize\"];\n";
		echo "totalfilesize = 0;\n";
		echo "par.checkOptionClear(obj);\n";
		

		while (list($key, $value) = each($files)) 
		{
			echo "par.checkOptionAdd(obj, \"".$files[$key][0]." (".ceil($files[$key][1]/1000)."Kb".")\", \"".$files[$key][0]."\");\n"; 
			echo "totalfilesize += ".($files[$key][1]/1000).";";
		}
		echo "obj2.value=totalfilesize;";
		echo "par.checkOptionAdd(obj, \"";
		for($i = 0; $i < 40; $i++) 
		echo " "; ;
		echo "\", \"\");\n";
	}
		$ExceedQuotaGroupValue = sizeof($ExceedQuotaGroup)>1?implode(",",$ExceedQuotaGroup):$ExceedQuotaGroup[0];
		$ExceedQuotaGroupIdValue = sizeof($ExceedQuotaGroupId)>1?implode(",",$ExceedQuotaGroupId):$ExceedQuotaGroupId[0];		

		echo "par.addExceedQuotaGroup('$ExceedQuotaGroupValue','$ExceedQuotaGroupIdValue');\n";
		echo "self.close();\n";
		echo "</script>"; 
	/*}
	else
	{
		echo "<script language=\"JavaScript1.2\">\n";
		
		$warnmsg=$Lang['Group']['NotEnoughSpace'];
		$warnmsg.=implode(",",$ExceedQuotaGroup);

		echo "alert(\"".$warnmsg."\");\n";
		
		//echo "self.close();\n";
		echo "</script>"; 
	}*/
?>

