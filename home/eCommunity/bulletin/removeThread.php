<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$BulletinIDArr = $_POST['BulletinIDArr'];
$BulletinIDArr = IntegerSafe($BulletinIDArr);

$li = new libgrouping();
$lu = new libfilesystem();

for($i=0;$i<sizeof($BulletinIDArr);$i++)
{
	$BulletinID = $BulletinIDArr[$i];
	$sql = "SELECT GroupID, UserID FROM INTRANET_BULLETIN WHERE BulletinID = '$BulletinID'";
	$array = $li->returnArray($sql,2);
	list($targetGroupID,$CreatorID) = $array[0];
	//$targetGroupID = $array[0];
	$lgroup = new libgroup($targetGroupID);
	if ($lgroup->hasAdminBulletin($UserID)||$CreatorID==$UserID) //($li->isGroupAdmin($UserID, $targetGroupID))
	{
         $sql = "	SELECT 
						ParentID,Attachment 
					FROM 
						INTRANET_BULLETIN
					WHERE
						Attachment IN
					(SELECT Attachment FROM INTRANET_BULLETIN WHERE BulletinID = '$BulletinID')";
         
         $array = $li->returnArray($sql,2);
         if ($array[0][0]==0)
         {
	         	  if ($array[0][1]!=""&&sizeof($array)==1) 	
		          {
			          $path = "$file_path/file/eComm/bulletin/".$array[0][1];
		              #remove attachment
	         		  $lu->lfs_remove($path);
     		  		}
     		  		
                  $sql = "DELETE FROM INTRANET_BULLETIN WHERE BulletinID = '$BulletinID' OR ParentID = '$BulletinID'";
                  
                  $li->db_db_query($sql);
         }
	}
}
intranet_closedb();
if($from=="index.php")
	header("Location: index.php?GroupID=$GroupID&filter=$filter&order=$order&field=$field&msg=delete");
else
	header("Location: setting.php?GroupID=$GroupID&filter=$filter&order=$order&field=$field&msg=delete");
?>