<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$linterface 	= new interface_html();
$CurrentPage	= "MyGroups";
$legroup 		= new libegroup();
$lu = new libgroup($GroupID);

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if($field=="") $field=2;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        default: $field = 2; break;
}
$order = ($order == 1) ? 1 : 0;

$keyword = stripslashes($keyword);
$pstatus_str = (isset($pstatus) && $pstatus!='-1') ? " and a.PublicStatus=$pstatus " : "";	
$sql = "SELECT 
			   if (a.PublicStatus=1 ,'&nbsp;', CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_private.gif\">')) ,
               CONCAT('<a href=javascript:bulletin_view(', if(a.ParentID=0,a.BulletinID,a.ParentID), ') class=\"tablelink\">', a.Subject, '</a>', if(locate(';$UserID;',a.ReadFlag)=0, '<img src=$image_path/new.gif border=0 hspace=2>', '')),
               if (b.PhotoLink!=\"\", CONCAT('<img src=\"',b.PhotoLink,'\" width=\"25\" height=\"30\">'), CONCAT('<img src=\"/images/myaccount_personalinfo/samplephoto.gif\" width=\"25\" height=\"30\">')),
               CONCAT('<a href=mailto:', a.UserEmail, ' class=\"tablelink\">', a.UserName, '</a>'),
			   
          	   DATE_FORMAT(a.DateModified, '%Y-%m-%d %H:%i')
          FROM
               INTRANET_BULLETIN a , INTRANET_USER b
          WHERE
               a.UserID = b.UserID AND
          		a.GroupID = '$GroupID' AND
               (
               a.Subject LIKE '%$keyword%' OR
               a.Message LIKE '%$keyword%' OR
               a.UserName LIKE '%$keyword%'
               )
               ".$pstatus_str."
               GROUP BY a.BulletinID
               
               ";

# TABLE INFO

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("DateModified","Subject","UserName","c","a");
$li->sql = $sql;

$li->no_col = sizeof($li->field_array);
$li->IsColOff = "eCommForumSearchList";
$li->no_msg = $i_frontpage_bulletin_no_record;

// TABLE COLUMN

$li->column_list .= "<td class='forumtabletop forumtabletoptext' width=\"18\">&nbsp;</td>\n";
$li->column_list .= "<td width='50%' class='forumtabletop forumtabletoptext'>$eComm_Field_Topic</td>\n";
$li->column_list .= "<td class='forumtabletop forumtabletoptext' width=\"20\">&nbsp;</td>\n";
$li->column_list .= "<td class='forumtabletop forumtabletoptext'>$eComm_Field_Createdby</td>\n";
//$li->column_list .= "<td class='forumtabletop forumtabletoptext' width=\"80\" nowrap>$eComm_Field_ReplyNo </td>\n";
$li->column_list .= "<td class='forumtabletop forumtabletoptext' width=\"100\">$eComm_Field_LastUpdated</td>\n";

$searchbar = "<input class=text type=text name=keyword size=15 maxlength=30>\n";
$searchbar .= $linterface->GET_ACTION_BTN($eComm['Search'], "submit", "","button"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")."\n";
$toolbar = "<a href=javascript:checkPost(document.form1,'new.php') class=\"tablelink\">".newIcon2()."$i_frontpage_bulletin_postnew</a>".toolBarSpacer();
$toolbar .= "<a href=javascript:checkPost(document.form1,'mark.php') class=\"tablelink\"><img src=../../../images/quickadd_icon.gif border=0 hspace=5 vspace=0 align=absmiddle>$i_frontpage_bulletin_markallread</a>";


### Button

$ForumIcon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_forum.gif\" width=\"25\" height=\"20\" align=\"absmiddle\" /><span class=\"ecomm_forum_title\">".$eComm['Forum']."</span>";

$MyGroupSelection= $lu->getSelectFilesGroups("name='GroupID' onChange=\"window.location='index.php?GroupID='+this.value\"",$GroupID);
$title = "
	<table width='100%' height='28' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' nowrap> <span class='contenttitle'>". $lu->TitleDisplay ."</span></td>
			<td valign='bottom' align='right' width='100%'>". $lu->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><a href='../group/index.php?GroupID=$GroupID'><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle'></a> {$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<script language="javascript">
function bulletin_view(id){
        obj = document.form1;
        obj.BulletinID.value = id;
        obj.action = "message.php";
        obj.submit();
}
function del(id){
        obj = document.form1;
        obj.BulletinID.value = id;
        if(confirm(globalAlertMsg3)){
        checkGet(obj,'removeThread.php');
        }
}

</script>
<form name=form1 method=get>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
        	<br>
        	<!---start--->
        	<tr>
		      <td width="12" height="25"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_01.gif" width="12" height="25"></td>
		      <td width="13" height="25"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_02.gif" width="13" height="25"></td>
		      <td height="25" align="right" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_02.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		          <tr>
		            <td align="left"><?=$ForumIcon?></td>
		            <td align="right"><a href="#"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_off.gif" name="a_mini" width="16" height="16" border="0" id="a_mini" onMouseOver="MM_swapImage('a_mini','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_03b.gif" width="16" height="25" align="absmiddle"></td>
		          </tr>
		      </table></td>
		      <td width="18" height="25"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="18" height="25"></td>
		    </tr>
		    <tr>
		      <td width="12" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_05.gif" width="12" height="28"></td>
		      <td width="13" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_06.gif" width="13" height="28"></td>
		      <td height="28" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_07.gif">
		      
		      <table width="100%" border="0" cellspacing="0" cellpadding="0">
		          <tr>
		            <td>&nbsp;</td>
            <td align="right"><a href="index.php?GroupID=<?=$GroupID?>" class="contenttool"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_prev_off.gif" width="11" height="10" border="0" align="absmiddle"> <?=$button_back?> </a></td>
		          </tr>
		      </table></td>
		      
		      <form name="form1" method="get">
		      <td width="18" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_08.gif" width="18" height="28"></td>
    		</tr>
    		<tr>
      			<td width="12" valign="bottom" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_05.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_09b.gif" width="12" height="19"></td>
      			<td width="13" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_10.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_10.gif" width="13" height="200"></td>
      			<td bgcolor="#fffeed"><?php echo $li->display(); ?>      			
      			</td>
      			<td width="18" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_12.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_12.gif" width="18" height="19"></td>
    			</tr>
    			<tr>
			      <td width="12" height="31"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="12" height="31"></td>
			      <td width="13" height="31"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_14.gif" width="13" height="31"></td>
			      <td height="31" align="right" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_15.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_15b.gif" width="170" height="31"></td>
			      <td width="18" height="31"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/forum_board_16.gif" width="18" height="31"></td>
			    </tr>
			
			<!--end--->
            </table>
        </td>
</tr>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=BulletinID value="">
<input type=hidden name=GroupID value="<?php echo $GroupID; ?>">
</table>
</form>
 
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>