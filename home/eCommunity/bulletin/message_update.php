<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."includes/libsendmail.php");
include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libbulletin.php");
intranet_auth();
intranet_opendb();

/*
$lo = new libuser2007($UserID);
if(!$lo->isInGroup($GroupID) || !$lo->isInGroup($libull->GroupID))
{
	header("Location: ../index.php?msg=AccessDenied");
	exit;
}
*/

$GroupID = IntegerSafe($GroupID);
$BulletinID = IntegerSafe($BulletinID);

$li = new libuser($UserID);

# send email
if($email_list<>""){
     $mailSubject = stripslashes($Subject);
     $mailBody  = stripslashes($Message).email_footer();
     $mailFrom = $li->UserEmail;
     $mailTo = $email_list;
     $lu = new libsendmail();
     $lu->do_sendmail($mailFrom, "", $mailTo, "", $mailSubject, $mailBody);
}

# update bulletin
$Subject = intranet_htmlspecialchars($Subject);
$Message = intranet_htmlspecialchars($Message);


if($li->IsGroup($GroupID)){
     $sql = "INSERT INTO INTRANET_BULLETIN (ParentID, GroupID, UserID, UserName, UserEmail, Subject, Message, ReadFlag, DateInput, DateModified) VALUES (".$BulletinID.", ".$GroupID.", $UserID, '".addslashes($li->NickNameClassNumber())."', '".$li->UserEmail."', '$Subject', '$Message', ';$UserID;', now(), now())";
     $li->db_db_query($sql);

     # Mark pts for comment for questions
     if (isset($plugin['QB']) && $plugin['QB'] && isset($qb_bulletin_score) && $qb_bulletin_score != 0)
     {
         include_once("../../../includes/libgroup.php");
         $lgroup = new libgroup($GroupID);
         if ($lgroup->RecordType == 2 && $lgroup->isAccessQB())
         {
             # Check if the first message
             $sql = "SELECT COUNT(*) FROM INTRANET_BULLETIN WHERE ParentID = '$BulletinID' AND UserID = '$UserID'";
             $result = $li->returnVector($sql);
             if ($result[0]==1)          # Award pts for first message reply
             {
                 include_once("../../../includes/libqb.php");
                 $lq = new libqb();
                 $lq->addUserScore($UserID,$qb_bulletin_score);
             }
         }
     }

}


intranet_closedb();
header("Location: message.php?pageNo=$pageNo&order=$order&field=$field&GroupID=$GroupID&BulletinID=$BulletinID&ThreadID=$ThreadSize");
?>