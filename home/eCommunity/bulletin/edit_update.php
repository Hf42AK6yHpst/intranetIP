<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libbulletin.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$BulletinID = IntegerSafe($BulletinID);

$lgroup = new libgroup($GroupID);
$legroup = new libegroup($GroupID);
$lo = new libgrouping();

$lbulletin	= new libbulletin($BulletinID);



if ($lgroup->hasAdminBulletin($UserID))
{
	
    
             
	 $fieldname  = "PublicStatus = '$PublicStatus', ";
	 $fieldname .= "ReplyFlag = '$ReplyFlag', ";
	 $fieldname .= "onTop = '$onTop', ";
	 $fieldname .= "DateModified = now()";
	 $sql = "UPDATE INTRANET_BULLETIN SET $fieldname WHERE BulletinID = '$BulletinID'";
	 $lo->db_db_query($sql);
	 
     header("Location: edit.php?GroupID=$GroupID&BulletinID=$BulletinID&msg=update");
         
}
else
{
    header ("Location: ../close.php");
}
intranet_closedb();

?>