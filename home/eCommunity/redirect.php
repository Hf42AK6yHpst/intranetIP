<?php
## Using By :
#############################################
##	Modification Log:
/* 
 * Date 2018-10-12 Isaac
 * added isDisplayinEComunity() to check if the last group is allowed to be access in eComunity before redirecting to the last group page
 */
#############################################
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libuserpersonalsettings.php");

intranet_auth();
intranet_opendb();

$legroup 	= new libegroup();
$lps = new libuserpersonalsettings();

# retrieve last access group id
$LastAccessAry = $lps->Get_Setting($UserID, array("eComm_LastAccessGroupID"));
if(!empty($LastAccessAry)) 
{
	$LastGroupID = $LastAccessAry[0];
	
	if($lu2007->isInGroup($LastGroupID) && $lu2007->isDisplayinEComunity($LastGroupID))
	{
		header("Location: ./group/index.php?GroupID=$LastGroupID");
		exit;
	}
	else
	{
		header("Location: index.php");
		exit;
	}
}
else 
{
	header("Location: index.php");
	exit;
}
 
intranet_closedb();
?>