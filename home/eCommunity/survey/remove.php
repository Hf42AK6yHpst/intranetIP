<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libsurvey.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$SurveyID = IntegerSafe($SurveyID);

$lg = new libgroup($GroupID);

if ($GroupID == "")
{
    $valid = false;
}
else
{
    if ($lg->hasAdminAllSurvey($UserID))
    {
        $valid = true;
    }
    else if ($lg->hasAdminInternalSurvey($UserID))
    {
         $valid = true;
    }
    else
    {
        $valid = false;
    }
}

if ($valid)
{
     include_once("../../../templates/fileheader.php");
?>

<form name=form1 action="remove_update.php" method=post enctype="multipart/form-data">
<table width="750" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><?=$i_grouphead_groupsetting?></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td width=100% class=popup_topcell><font color="#FFFFFF"><?php echo $lg->Title; ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
      <table width="750" border="0" cellspacing="0" cellpadding="10" background="/images/groupinfo/popup_tbbg_large.gif">
      <tr>
            <td width="1"><img src="/images/spacer.gif" width="1" height="1">
            </td>
      <td align=left>
      </td></tr>
          <tr>
            <td colspan=2 align="right">
              <table width="390" border="0" cellspacing="0" cellpadding="0" class="functionlink">
                <tr>
                  <td align="right" bgcolor="#EDDA5C">
                  </td>
                  <td width="12"><img src="/images/spacer.gif" width="12" height="1"></td>
                </tr>
              </table>
              <br>
            </td>
          </tr>
      </table>
        <table width="750" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg_large.gif">
          <tr>
            <td width="20"><img src="/images/spacer.gif" width="20" height="1">
            </td>
            <td width="730" class="h1"><font color="#031BAC"><?=$button_remove." ".$i_GroupSettingsSurvey?></font></td>
          </tr>
          <tr align="center">
            <td colspan="2"><img src="/images/line.gif" width="616" height="1"></td>
          </tr>
          <tr>
          <td colspan=2 align=center>
<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td><?=$i_Survey_RemoveConfirm?></td></tr>
<tr><td><input type=image src='<?=$image_delete?>'><a href=javascript:history.back()><img src='<?=$image_cancel?>' border=0></td></tr>
</table>
</blockquote>


          </td>
          </tr>
        </table>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/images/groupinfo/popup_bottom_large.gif"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<input TYPE=hidden NAME=GroupID value="<?=$GroupID?>">
<?php
for ($i=0; $i<sizeof($SurveyID); $i++)
{
?>
<input type=hidden name=SurveyID[] value="<?=$SurveyID[$i]?>">
<?
}
?>
</form>

<?php
include_once("../../../templates/filefooter.php");
}
else
{
    header ("Location: ../close.php");
}
intranet_closedb();
?>