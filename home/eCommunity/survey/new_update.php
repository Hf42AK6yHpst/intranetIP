<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libuser.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$TargetGroupID = IntegerSafe($TargetGroupID);

$lg = new libgroup($GroupID);
$lu = new libuser($UserID);
if ($lg->hasAdminInternalSurvey($UserID) || $lg->hasAdminAllSurvey($UserID))
{
    $internal = 0;
    if ($lg->hasAdminAllSurvey($UserID))
    {
        if (sizeof($TargetGroupID)==1 && $TargetGroupID[0]==$GroupID)
        {
            $internal = 1;
        }
    }
    else
    {
        $internal = 1;
    }


     $Title = intranet_htmlspecialchars(trim($Title));
     $Description = intranet_htmlspecialchars(trim($Description));
     $StartDate = intranet_htmlspecialchars(trim($StartDate));
     $EndDate = intranet_htmlspecialchars(trim($EndDate));
     $qStr = intranet_htmlspecialchars(trim($qStr));
     if (sizeof($TargetGroupID)!=0)
     {
         $TargetGroupID = array_unique($TargetGroupID);
         $TargetGroupID = array_values($TargetGroupID);
     }

     $startStamp = strtotime($StartDate);
     $endStamp = strtotime($EndDate);

     if (compareDate($startStamp,$endStamp)>0)   // start > end
     {
         $valid = false;
     }
     else if (compareDate($startStamp,time())<0)      // start < now
     {
          $valid = false;
     }
     else
     {
         $valid = true;
     }

     if ($valid)
     {
          $username = $lu->getNameForRecord();
          $sql = "INSERT INTO INTRANET_SURVEY
                  (Title, Description, DateStart, RecordStatus, DateInput, DateModified, DateEnd, OwnerGroupID, PosterID,PosterName, Internal,Question, RecordType, AllFieldsReq) VALUES
                  ('$Title', '$Description', '$StartDate', '$RecordStatus', now(), now(),'$EndDate', $GroupID, $UserID,'$username', '$internal','$qStr', '$isAnonymous', '$AllFieldsReq')";
          $lg->db_db_query($sql);
          $SurveyID = $lg->db_insert_id();

          if ($internal != 1 && sizeof($TargetGroupID)!=0)
          {
              $group_survey_field = "";
              $delimiter = "";
              for ($i=0; $i<sizeof($TargetGroupID); $i++)
              {
                   $group_survey_field .= "$delimiter(".$TargetGroupID[$i].",$SurveyID)";
                   $delimiter = ",";
              }
              $sql = "INSERT INTO INTRANET_GROUPSURVEY (GroupID, SurveyID) VALUES $group_survey_field";
              $lg->db_db_query($sql);
          }
          else if ($internal == 1)
          {
               $sql = "INSERT INTO INTRANET_GROUPSURVEY (GroupID, SurveyID) VALUES ($GroupID,$SurveyID)";
               $lg->db_db_query($sql);
          }

          header ("Location: index.php?filter=$RecordStatus&msg=1&GroupID=$GroupID");
     }
     else
     {
         header ("Location: new.php?GroupID=$GroupID&t=$Title&d=$Description&ad=$AnnouncementDate&ed=$EndDate");
     }
}
else
{
    header ("Location: ../close.php");
}
intranet_closedb();

?>