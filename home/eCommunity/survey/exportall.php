<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$SurveyID = IntegerSafe($SurveyID);

$lexport = new libexporttext();

$SurveyID = (is_array($SurveyID)? $SurveyID[0]:$SurveyID);

$lsurvey = new libsurvey($SurveyID);

$ownerGroupID = $lsurvey->OwnerGroupID;
$lg = new libgroup($ownerGroupID);

if ($ownerGroupID == "")
{
    $valid = false;
}
else
{
    if ($lg->hasAdminAllSurvey($UserID))
    {
        $valid = true;
    }
    else if ($lg->hasAdminInternalSurvey($UserID) && $lsurvey->Internal == 1)
    {
         $valid = true;
    }
    else
    {
        $valid = false;
    }
}

if ($valid)
{
     $queString = $lsurvey->Question;
     $question_array = $lsurvey->splitQuestion($queString);
     $answers = $lsurvey->getRawDataTable();
     if ($lsurvey->RecordType != 1)
     {
         $x = "User Name, Class, Class Number";
         $exportColumn = array("User Name", "Class", "Class Number");
         for ($j=0; $j<sizeof($question_array); $j++)
         {
              $x .= ",\"".$lsurvey->question_array[$j][1]."\"";
              $exportColumn[] = $lsurvey->question_array[$j][1];
         }
         $x .= ",Date of filling\n";
         $exportColumn[] = "Date of filling";
     }
     else
     {
         $x = "";
         $delim = "";
         for ($j=0; $j<sizeof($question_array); $j++)
         {
              $x .= "$delim\"".$lsurvey->question_array[$j][1]."\"";
              $exportColumn[] = $lsurvey->question_array[$j][1];
              $delim = ",";
         }
         $x .= "\n";
     }

     for ($i=0; $i<sizeof($answers); $i++)
     {
          list($id,$name,$class,$classnumber,$parsed,$fillTime) = $answers[$i];
          if ($lsurvey->RecordType != 1)
          {
              $x .= "\"$name\",\"$class\",\"$classnumber\"";
              $row = array($name, $class, $classnumber);
              for ($j=0; $j<sizeof($lsurvey->question_array); $j++)
              {
                   $x .= ",\"".$parsed[$j]."\"";
                   $row[] = $parsed[$j];
              }
              $x .= ",\"$fillTime\"\n";
              $row[] = $fillTime;
              $rows[] = $row;
              unset($row);
          }
          else
          {
              $delim = "";
              for ($j=0; $j<sizeof($lsurvey->question_array); $j++)
              {
                   $x .= "$delim\"".$parsed[$j]."\"";
                   $row[] = $parsed[$j];
                   $delim = ",";
              }
              $x .= "\n";
			  $rows[] = $row;
              unset($row);
          }
     }
     // Output the file to user browser
     $filename = "surveyresult_$SurveyID.csv";
     /*
     header("Content-type: application/octet-stream");
     header("Content-Length: ".strlen($x) );
     header("Content-Disposition: attachment; filename=\"".$filename."\"");

     echo $x;
     */
     $export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
	 $lexport->EXPORT_FILE($filename, $export_content);
}
else
{
    header ("Location: ../close.php");
}
intranet_closedb();
?>