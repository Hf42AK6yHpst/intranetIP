<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libsurvey.php");
include_once("../../../includes/libform.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libuser.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/fileheader.php");
intranet_auth();
intranet_opendb();

$SurveyID = IntegerSafe($SurveyID);

$lform = new libform();
$lsurvey = new libsurvey($SurveyID);
$queString = $lsurvey->Question;
$queString = str_replace('"','&quot;',$queString);
$queString = str_replace("\n",'<br>',$queString);
$queString = str_replace("\r",'',$queString);

$poster = $lsurvey->returnPosterName();
$ownerGroup = $lsurvey->returnOwnerGroup();
$targetGroups = $lsurvey->returnTargetGroups();
$reqFillAllFields = $lsurvey->reqFillAllFields();

if ($ownerGroup != "")
{
    $poster = "$poster<br>\n$ownerGroup";
}
if (sizeof($targetGroups)==0)
{
    $target = "$i_general_WholeSchool";
}
else
{
    $target = implode(", ",$targetGroups);
}

?>
<table width="580" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="553" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><img src=<?=$image_path?>/survey/heading_survey_<?=$intranet_session_language?>.gif></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td width=500 class=popup_topcell><font color="#FFFFFF"><?php echo $i_Survey_FillSurvey; ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
        <table width="553" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg.gif">
          <tr>
            <td align=center>

<table width=90% cellspacing=3 cellpadding=3>
<tr><td width=30%><?=$i_general_startdate?></td><td><?=$lsurvey->DateStart?></td></tr>
<tr><td width=30%><?=$i_general_enddate?></td><td><?=$lsurvey->DateEnd?></td></tr>
<tr><td width=30%><?=$i_general_title?></td><td><?=$lsurvey->Title?></td></tr>
<tr><td width=30%><?=$i_general_description?></td><td><?=nl2br(intranet_convertAllLinks($lsurvey->Description))?></td></tr>
<tr><td width=30%><?=$i_Survey_Poster?></td><td><?=$poster?></td></tr>
<tr><td width=30%><?=$i_general_TargetGroup?></td><td><?=$target?></td></tr>
<? if ($lsurvey->RecordType == 1) { ?>
<tr><td >&nbsp;</td><td>[<?=$i_Survey_Anonymous?>]</td></tr>
<? } ?>
<? if ($reqFillAllFields) { ?>
<tr><td >&nbsp;</td><td>[<?=$i_Survey_AllRequire2Fill?>]</td></tr>
<? } ?>
<tr><td colspan=2>
<p><br>
<?=$i_Survey_PleaseFill?><br>
<hr width=95% align=center>
<script LANGUAGE=Javascript>
// Set true if need to fill in all fields before submit
var need2checkform = <? if($reqFillAllFields){ echo "true"; } else { echo "false"; }  ?>;
</script>
<script language="javascript" src="/templates/forms/layer.js"></script>
<script language="javascript" src="/templates/forms/form_edit.js"></script>
<form name="ansForm" method="post" action="update.php">
        <input type=hidden name="qStr" value="">
        <input type=hidden name="aStr" value="">
</form>
<script language="Javascript">
<?=$lform->getWordsInJS()?>

//background_image = "/images/layer_bg.gif";

var sheet= new Answersheet();
// attention: MUST replace '"' to '&quot;'
sheet.qString="<?=$queString?>";
//edit submitted application
sheet.mode=1;
sheet.answer=sheet.sheetArr();
//sheet.templates=form_templates;
document.write(editPanel());
</script>
<SCRIPT LANGUAGE=javascript>
function copyback()
{
         finish();
         document.form1.qStr.value = document.ansForm.qStr.value;
         document.form1.aStr.value = document.ansForm.aStr.value;
}
function validSubmit()
{
         if (!need2checkform || formAllFilled)
         {
              return true;
         }
         else //return true;
         {
              alert("<?=$i_Survey_alert_PleaseFillAllAnswer?>!");
              return false;
         }
}
</SCRIPT>
<hr width=95%>
<form name=form1 action=fill_update.php method=post onsubmit="return validSubmit()">
<input type=image src="<?="$intranet_httppath$image_submit"?>" onClick="copyback();">
<input type=hidden name=qStr value="">
<input type=hidden name=aStr value="">
<input type=hidden name=SurveyID value="<?=$SurveyID?>">
</form>
</td></tr>
</table>

            </td>
          </tr>
        </table>
      <table width="553" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/images/groupinfo/popup_bottom.gif"></td>
        </tr>
      </table>

    </td>
  </tr>

</table>


<?php
include_once("../../../templates/filefooter.php");
?>