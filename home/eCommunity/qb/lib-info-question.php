<?php 
class tableQuestion extends libtable{
	var $navbar;
	var $QueType;

	function display(){
		$i=0;
		$this->n_start=($this->pageNo-1)*$this->page_size;
		$this->rs=$this->db_db_query($this->built_sql());
		$total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
		$x.="<table width='96%' border='0' align='center' cellpadding='0' cellspacing='0'>\n";
		$x.="<tr>\n"; 
		$x.="<td width='1'><img src='$this->imagePath/frame/border2_up_left.gif' width='9' height='5'></td>\n";
		$x.="<td colspan='".$this->no_col."' nowrap background='$this->imagePath/frame/border2_bottom.gif'>\n";
		$x.="<img src='$this->imagePath/space.gif' width='1' height='5'></td>\n";
		$x.="<td width='1'><img src='$this->imagePath/frame/border2_up_right.gif' width='9' height='5'></td>\n";
		$x.="</tr>\n";
		$x.=$this->displayColumn();
		if ($this->db_num_rows()==0){
			$x.="<tr><td nowrap background='$this->imagePath/frame/border2_left.gif'>&nbsp;</td><td class=tableContent2 align=center colspan=".$this->no_col."><br><br>".$this->no_msg."<br><br><br></td><td nowrap background='$this->imagePath/frame/border2_right.gif'>&nbsp;</td></tr>\n";
		}else{
			//if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
			while($row = $this->db_fetch_array()) {
				$i++;
				if($i>$this->page_size) break;
				$x.="<tr><td background='$this->imagePath/frame/border2_left.gif'>&nbsp;</td>\n";
				$cn = ( $i%2 == 1) ? "1" : "2";
				$x.= ($this->noNumber) ? "" : "<td class=tableContent$cn height='35' align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
				for($j=0; $j<$total_col; $j++){
					$t=$row[$j];
					if ($j==1) {
						if ($this->QueType==2) {
							$question = $this->getQuestion($t, $row[6]);
							$Que_Title_full = $question[0];
							$Que_Title_cut = $question[1];
							$t = "<a href='javascript:view(".$row[5].")' onMouseOut='hideTip()' onBlur='hideTip()' onMouseMove='overhere()' \n onMouseOver='tipsNow(str".$row[5].")'>".nl2br($this->cut_string($Que_Title_cut, 50))."</a>\n <script language='javascript'>var str".$row[5]." = \"".addslashes(preg_replace("/(\015\012)|(\015)|(\012)/","&nbsp;<br />", $Que_Title_full))."\"</script>\n";
						} else {
							$separator = "===@@@===";
							$question = split($separator, $t);
							$Que_Title = $question[0];
							$Que_HTML = $question[1];
							$Que_Title_Tip = ($Que_HTML==1) ? $this->undo_htmlspecialchars($Que_Title) : $Que_Title;
							$t = "<a href='javascript:view(".$row[5].")' onMouseOut='hideTip()' onBlur='hideTip()' onMouseMove='overhere()' \n onMouseOver='tipsNow(str".$row[5].")'>".nl2br($this->cut_string($Que_Title, 50))."</a>\n <script language='javascript'>var str".$row[5]." = \"".addslashes(preg_replace("/(\015\012)|(\015)|(\012)/","&nbsp;<br />", $Que_Title_Tip))."\"</script>\n";
						}
					}
					$x.=$this->displayCell($j,$t, $cn);
				}
				$x.="<td background='$this->imagePath/frame/border2_right.gif'>&nbsp;</td></tr>\n";
			}
		}
		$x.="<tr><td><img src='$this->imagePath/frame/border2_bt_left.gif' width='9' height='5'></td>";
		$x.="<td colspan='".$this->no_col."' background='$this->imagePath/frame/border2_bottom.gif'>";
		$x.="<img src='$this->imagePath/space.gif' width='5' height='5'></td>";
		$x.="<td><img src='$this->imagePath/frame/border2_bt_right.gif' width='9' height='5'></td></tr>\n";
		if($this->db_num_rows()<>0) $x.= "<tr><td>&nbsp;</td><td colspan='".($this->no_col)."'>".$this->navigation()."</td><td>&nbsp;</td></tr>";
		$x.="</table>\n";
		if ($this->db_num_rows()==0) $x .= "<br>&nbsp;\n";
		$this->db_free_result();
		echo $x;
	}
	
	function undo_htmlspecialchars($string){
		$string = str_replace("&amp;", "&", $string);
		$string = str_replace("&quot;", "\"", $string);
		$string = str_replace("&lt;", "<", $string);
		$string = str_replace("&gt;", ">", $string);
		$string = str_replace("<form>", "&lt;form&gt;", $string);
		$string = str_replace("</form>", "&lt;/form&gt;", $string);
		$string = str_replace("<input", "&lt;input", $string);
		$string = str_replace("<textarea>", "&lt;textarea&gt;", $string);
		$string = str_replace("</textarea>", "&lt;/textarea&gt;", $string);
		return $string;
	}
	
	function getQuestion($question, $answer) {
		$separator = "===@@@===";
		$x = "";
		$y = "";
		$tmpArr = split($separator, $question);
		$tmpAns = split($separator, $answer);
		$sizeArr = max((sizeof($tmpArr)-2), sizeof($tmpAns));
		$question_html_opt = $tmpArr[sizeof($tmpArr)-2];

		for ($i=0; $i<$sizeArr; $i++) {
			$y .= " ".$tmpArr[$i];
			$x .= ($question_html_opt==1) ? " ".$this->undo_htmlspecialchars($tmpArr[$i]) : " ".$tmpArr[$i];
			$ans = $tmpAns[$i];
			$y .= " ".$ans;
			$x .= " <u>".$ans."</u>";
		}
		return array($x, $y);
	}
}
?>