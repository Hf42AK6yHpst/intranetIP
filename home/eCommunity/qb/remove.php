<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$lf = new libfilesystem();

$status_pending = 0;
$status_canbedelete = "0,2";

$qlist = implode(",",$QuestionID);

# Only pending question can be deleted
$sql = "SELECT QuestionID FROM QB_QUESTION WHERE QuestionID IN ($qlist) AND RecordStatus IN ($status_canbedelete)";
$canBeDelete = $li->returnVector($sql);
if (sizeof($canBeDelete)==0)
{
    header("Location: myquestion.php?GroupID=$GroupID&type=$type&msg=3");
    intranet_closedb();
    exit();
}
$qlist = implode(",",$canBeDelete);


$sql = "SELECT OriginalFileEng, OriginalFileChi FROM QB_QUESTION WHERE QuestionID IN ($qlist)";
$result = $li->returnArray($sql,2);
for ($i=0; $i<sizeof($result); $i++)
{
     list($efile,$cfile) = $result;
     $epath = "$intranet_root/file/qb/g$GroupID/$efile";
     $cpath = "$intranet_root/file/qb/g$GroupID/$cfile";
     if ($efile!="" && is_file($epath))
     {
         $lf->file_remove($epath);
     }
     if ($cfile!="" && is_file($cpath))
     {
         $lf->file_remove($cpath);
     }
}
$sql = "DELETE FROM QB_QUESTION WHERE QuestionID IN ($qlist)";
$li->db_db_query($sql);
/*
for($i=0;$i<sizeof($question_id);$i++){
     $sql="DELETE FROM quiz_question WHERE question_id = $question_id[$i]";
     $li->db_db_query($sql);
     $sql="DELETE FROM question WHERE question_id = $question_id[$i]";
     $li->db_db_query($sql);
}
*/
intranet_closedb();
header("Location: myquestion.php?GroupID=$GroupID&type=$type&msg=3");
?>