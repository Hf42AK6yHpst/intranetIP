<?php
include_once('../../../../system/settings/global.php');
include_once('../../../includes/php/lib-db.php');
include_once('../../../includes/php/lib-question-mc.php');
include_once('../../../includes/php/lib-filemanager.php');
include_once('../../../../system/settings/lang/'.$ck_default_lang.'.php');
include_once('../menu.php');
auth("AT");
opendb();
//head();

$fm = new fileManager($ck_course_id, 5, "");
$que = new question_mc();

// make temp folder for storing files
$dest = $admin_root_path."/".$file_path_name."/".classNamingDB($ck_course_id)."/tmp";
$fm->createFolder($dest);
$dest .= "/$ck_user_id";
$fm->createFolder($dest);
$dest .= "/question";

$dest = OsCommandSafe($dest);

if (file_exists($dest))		//avoid reading old files
	exec("rm -r $dest");
mkdir($dest, 0777);
$filename = "export_mc_".date("ymd")."_".sizeof($question_id);

$filename= OsCommandSafe($filename); 

$fm->createFolder($dest."/".$filename);
$que->destination = $dest."/".$filename;

//write file
$x = $que->exportQuestion($question_id);
$fm->writeFile($x, $dest."/".$filename."/question.txt");

//zip file
chdir($dest);
exec("zip -r $filename.zip $filename");

//delete files & folders
exec("rm -r $filename");

closedb();

//read the zip file and send to user
$zipfile = $dest."/".$filename.".zip";
if (file_exists($zipfile)) {
	header('Content-Type: application/octet-stream');
	header("Content-Disposition: attachment; filename=".$filename.".zip");
	readfile($zipfile);
	//debug("$admin_url_path/$file_path_name/".classNamingDB($ck_course_id)."/tmp/".$ck_user_id."/question/".$filename.".zip");
	//header("Location: $admin_url_path/$file_path_name/".classNamingDB($ck_course_id)."/tmp/".$ck_user_id."/question/".$filename.".zip");
}

if (file_exists($dest))
	exec("rm -r $dest");

header("Location: index.php");
?>