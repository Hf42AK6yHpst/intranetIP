<?php 
include_once('../../../../system/settings/global.php');
include_once('../../../includes/php/lib-db.php');
include_once('../../../includes/php/lib-question-mc.php');
include_once('../../../../system/settings/lang/'.$ck_default_lang.'.php');
auth("AT");

opendb();

$mc = new question_mc();

if ($mc->importFromText($data))
	$msg=1;

closedb();

header("Location: index.php?msg=$msg");
?>
