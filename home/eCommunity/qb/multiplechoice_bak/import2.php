<?php 
include_once('../../../../system/settings/global.php');
include_once('../../../../system/settings/lang/'.$ck_default_lang.'.php');
include_once('../menu.php');
auth("AT");
head();

if ( $NS4 ) {
	$lenText = 50;
} else {
	$lenText = 80;
}

$cur_status .= getStatus($QuestionBank, "../index.php", $qbank_mc, "index.php", $button_import, "");

function formatImport($c) {
	return "<span class=symbol>[[</span><span class=symText>".$c."</span><span class=symbol>]]</span><br>\n";
}
$myFormat = "<span class=symbol>Category=</span> sample<br>";
$myFormat .= "<span class=symbol>Public=</span> yes<br>";
$myFormat .= "Question<br>";
$myFormat .= formatImport("HTML, file");
$myFormat .= "Answer<br>";
$myFormat .= formatImport("");
$myFormat .= "Choice A<br>";
$myFormat .= formatImport("HTML, file");
$myFormat .= "Choice B<br>";
$myFormat .= formatImport("HTML, file");
$myFormat .= "Choice C<br>";
$myFormat .= formatImport("HTML, file");
$myFormat .= "Choice D<br>";
$myFormat .= formatImport("HTML, file");
$myFormat .= "Choice E<br>";
$myFormat .= formatImport("HTML, file");
$myFormat .= "Hint<br>";
$myFormat .= formatImport("HTML, file");
$myFormat .= "Explanation<br>";
$myFormat .= formatImport("HTML, file");
$myFormat .= "Reference<br>";
$myFormat .= formatImport("HTML, file");

?>

<script language="JavaScript" src="../../../includes/js/imgMenu.js"></script>


<form action="import2_update.php" method="post" onSubmit="return check_text(this.data, '<?php echo $bookmark_alert_msg1; ?>');">
<?php 
//************************************ START OF HTML DISPLAY ************************************
displayStatusAndMenu($cur_status, menuIcon(1));
?>
<table width='93%' border=0 cellpadding=5 cellspacing=0 align="center">
  <tr><td><span class=intro><?= $qbank_msg21 ?></span></td></tr>
</table>

<table width='85%' border='0' align='center' cellpadding='0' cellspacing='0'>
<tr>
<td width='25' nowrap align='right'>
<img src='<?=$image_path?>/frame/border2_hd_left.gif' width='15' height='25'></td>
<td nowrap background='<?=$image_path?>/frame/border2_header.gif'><b><?php echo strtoupper($profiles_from); ?>:</b> 
&nbsp; <a href="import.php"><?=$file_file?></a> &nbsp;|
&nbsp;<span class=notice1><?=$input_box?></span>&nbsp;
</td>
<td width='25'><img src='<?=$image_path?>/frame/border2_hd_right.gif' width='25' height='25'></td>
<td width='100%'>&nbsp;</td>
</tr>
</table>

<?php
	getBorder2_s('85%', 'center');
?>
<td class="bodycolor2">

<table width='55%' border=0 cellpadding=5 cellspacing=0 align="center">
  <tr>
	<td rowspan=4 nowrap>
	<?= $myFormat ?>
	<span class=symbol>###</span>
	</td>
	<td><textarea name="data" cols="<?= $lenText ?>;" rows="20" wrap="OFF" class="inputfield"></textarea>
	<input type=button class=buttonsmall value="<?=$button_paste_sample?>" onClick="if (this.form.data.value!='') this.form.data.value+='\n\n'; this.form.data.value+='Category=Sample\nPublic=yes\nI like ______ football.\n[[HTML, file1, file2]]\nC\n[[]]\nplay\n[[HTML, file]]\nplays\n[[]]\nplaying\n[[]]\nplayed\n[[]]\nplayyed\n[[]]\nfootball is my hobby\n[[HTML, file]]\nexplanation\n[[]]\nreference\n[[]]\n###'">
	<input class=button type=reset value='<?=$button_clear?>'></td>
  </tr>
</table>

<?php
$buttons =  "&nbsp;&nbsp;&nbsp;<input class=button type=submit value='$button_submit'>&nbsp;&nbsp;\n";
$buttons .= "<input class=button type=button value='$button_cancel' onClick='window.location=\"index.php\";'>&nbsp;&nbsp;\n";
getBorder2_e('center',$buttons);
?>

<?php
foot(menuText(1));
?>
