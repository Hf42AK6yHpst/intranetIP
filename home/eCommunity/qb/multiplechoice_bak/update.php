<?php
include_once('../../../../system/settings/global.php');
include_once('../../../includes/php/lib-db.php');
auth("AT");
opendb();

$li = new libdb();
$li->db = classNamingDB($ck_course_id);

$separator = "===@@@===";
$x  = $question.$separator.$question_html_opt.$separator.$question_attach;
$x .= $separator.$choiceA.$separator.$choiceA_html_opt.$separator.$choiceA_attach;
$x .= $separator.$choiceB.$separator.$choiceB_html_opt.$separator.$choiceB_attach;
$x .= $separator.$choiceC.$separator.$choiceC_html_opt.$separator.$choiceC_attach;
$x .= $separator.$choiceD.$separator.$choiceD_html_opt.$separator.$choiceD_attach;
$x .= $separator.$choiceE.$separator.$choiceE_html_opt.$separator.$choiceE_attach;
$question = htmlspecialchars($x);
$category = htmlspecialchars($category);
$answer = htmlspecialchars($answer);
$status = htmlspecialchars($status);
$hint = htmlspecialchars($hint.$separator.$hint_html_opt.$separator.$hint_attach);
$explanation = htmlspecialchars($explanation.$separator.$explanation_html_opt.$separator.$explanation_attach);
$reference = htmlspecialchars($reference.$separator.$reference_html_opt.$separator.$reference_attach);

if($question_id==""){
	$msg=1;
	$fieldname  = "category, question, answer, hint, explanation, ";
	$fieldname .= "reference, inputdate, modified, qtype, status";
	$fieldvalue  = "'$category', '$question', '$answer', '$hint', '$explanation', ";
	$fieldvalue .= "'$reference', now(), now(), '0', '$status'";
	$sql="INSERT INTO question ($fieldname) values ($fieldvalue)";
}else{
	$msg=2;
	$fieldvalue  = "category = '$category', ";
	$fieldvalue .= "question = '$question', ";
	$fieldvalue .= "answer = '$answer', ";
	$fieldvalue .= "hint = '$hint', ";
	$fieldvalue .= "explanation = '$explanation', ";
	$fieldvalue .= "reference = '$reference', ";
	$fieldvalue .= "modified = now(), ";
	$fieldvalue .= "status = '$status'";
	$sql="UPDATE question SET $fieldvalue WHERE question_id = $question_id"; 
}
$li->db_db_query($sql);

closedb();

if($add_another)
	header("Location: new.php?msg=$msg");
else
	header("Location: index.php?msg=$msg");
?>
