<?php
include_once('../../../../system/settings/global.php');
include_once('../../../includes/php/lib-db.php');
include_once('../../../includes/php/lib-question-mc.php');
include_once('../../../../system/settings/lang/'.$ck_default_lang.'.php');
include_once('../menu.php');
auth("AT");
opendb();
head();


//************************************ PREPARE HTML CODES ************************************
$li = new question_mc();

// FUNCTIONBAR: Save, Save and Add Another, Reset, Cancel
$functionbar  = "&nbsp;&nbsp;&nbsp;<input class='button' type='submit' value='$button_submit'>&nbsp;&nbsp;\n";
//$functionbar .= "<input class='button' type='submit' value='$button_submit_add' onClick=this.form.add_another.value='1'>&nbsp;&nbsp;\n";
$functionbar .= "<input class='button' type='reset' value='$button_reset'>&nbsp;&nbsp;\n";
$functionbar .= "<input class='button' type='button' value='$button_cancel' onClick='self.location=\"index.php\"'>&nbsp;&nbsp;&nbsp;\n";

$cur_status .= getStatus($QuestionBank, "../index.php", $qbank_mc, "index.php", $button_new, "");

?>

<script language="JavaScript" src="../../../includes/js/imgMenu.js"></script>
<script language="javascript">
function fileAttach(obj){
	x = obj.value;
	y = obj.name;
	url = "../../../course/resources/files/index.php?category=5&attach=1&fieldname=" + y + "&attachment=" + x;
	newWindow(url,1);
}

function checkform(obj){
	if(!check_text(obj.category, "<?php echo $qbank_alert_msg1; ?>")) return false;
	if(!check_text(obj.question, "<?php echo $qbank_alert_msg2; ?>")) return false;
	answer =  returnChecked(obj,"answer");
	if(answer==null){ alert("<?php echo $qbank_alert_msg3; ?>"); return false; }
	if(!check_text(obj.elements["choice"+answer], "<?php echo $qbank_alert_msg4; ?>")) return false;
	if(!confirm("<?php echo $qbank_alert_msg5; ?>")) return false;
}
</script>


<form name="form1" action="update.php" method="post" onSubmit="return checkform(this);">
<?php 
// DISPLAY CURRENT STATUS AND TOP-RIGHT MENU
displayStatusAndMenu($cur_status, menuIcon(1));
?>
<table width=90% border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td class="notice">
<?php if($msg==1) displaySysMsg($con_msg_add); ?>
</td></tr>
</table>

<?php
getBorder2_s('90%', 'center');
?>
<td class="bodycolor2">

<table width="650" border="0" align="center" cellpadding="2" cellspacing="1">
  <tr><td colspan="4">&nbsp;</td></tr>
<?php echo $li->displayCategory("$qbank_category:","category"); ?>
  <tr> 
	<td colspan="4">&nbsp;</td>
  </tr>
<?php echo $li->displayQuestionInput("$qbank_question:","question"); ?>
  <tr> 
	<td align="right" nowrap valign="top"><span class=title><?= $qbank_answer_choice ?>: </span><span class="asterisk">*</span><br> 
	</td>
	<td colspan="3">&nbsp;<span class="guide"><?= $qbank_answer_choice_guide ?></span> </td>
  </tr>
<?php echo $li->displayChoice("A.","choiceA","A"); ?>
<?php echo $li->displayChoice("B.","choiceB","B"); ?>
<?php echo $li->displayChoice("C.","choiceC","C"); ?>
<?php echo $li->displayChoice("D.","choiceD","D"); ?>
<?php echo $li->displayChoice("E.","choiceE","E"); ?>
  <tr> 
	<td colspan=3>&nbsp;</td><td><hr class="hrbar" size="1"></td>
  </tr>
<?php echo $li->displayExtraField("$qbank_hint:","hint"); ?>
<?php echo $li->displayExtraField("$qbank_expl:","explanation"); ?>
<?php echo $li->displayExtraField("$qbank_ref:","reference"); ?>
</table>

<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr><td height=25><?php echo $qbank_required_field; ?></td></tr>
</table>

<?php
getBorder2_e('center',$functionbar);
?>

<input type="hidden" name="add_another" value="0">
</form>


<?php
foot(menuText(1));
closedb();
?>
