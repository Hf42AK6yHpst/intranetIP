<?php
//$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.sunny.php");

class campus_link_ui{ 

	function getManageTools($linkID, $count){
		global $button_edit,$button_delete,$i_CampusLink_moveUp,$i_CampusLink_edit,$i_CampusLink_moveDown,$LAYOUT_SKIN; 
		$libinterface = new interface_html();
		$manageTools = '
		<div style="height:20px;width:207px">
			<span class="board_tool">
				<a id="moveUp-'.$linkID.'" href="javascript:void(0)" style="display:'.($count==0||$count==-2?"none": "block").'" onclick="moveUp('.$linkID.')">
					<img src="/images/'.$LAYOUT_SKIN.'/index/arrow_up_off.gif" title="'.$i_CampusLink_moveUp.'" border="0" align="absmiddle" onmouseover="this.src=\'/images/'.$LAYOUT_SKIN.'/index/arrow_up_on.gif\'" onmouseout="this.src=\'/images/'.$LAYOUT_SKIN.'/index/arrow_up_off.gif\'" >
				</a>
				<a id="moveDown-'.$linkID.'" href="javascript:void(0)" style="display:'.($count==-1||$count==-2?"none": "block").'" onclick="moveDown('.$linkID.')">
					<img src="/images/'.$LAYOUT_SKIN.'/index/arrow_down_off.gif" title="'.$i_CampusLink_moveDown.'" border="0" align="absmiddle" onmouseover="this.src=\'/images/'.$LAYOUT_SKIN.'/index/arrow_down_on.gif\'" onmouseout="this.src=\'/images/'.$LAYOUT_SKIN.'/index/arrow_down_off.gif\'" style="width:9px;height:6px;border:0px">
				</a>
				 <span class="table_row_tool">
				 '.$libinterface->Get_Thickbox_Link(250, 600, "edit_dim", $i_CampusLink_edit, "Show_CompusLink_edit_box({mode:0,linkID:'$linkID'}); return false;").'
				<!--<a class="edit_dim" id="campusSettingButton-'.$linkID.'" href="javascript:void(0)" onclick="openCampusSetting(this)">
					<img src="/images/'.$LAYOUT_SKIN.'/icons_manage.gif" title="'.$button_edit.'" border="0" align="absmiddle"> 
				</a> -->
				</span>
				<a href="javascript:deleteCampusSetting('.$linkID.')">
					<img src="/images/'.$LAYOUT_SKIN.'/icon_delete_b.gif" title="'.$button_delete.'" border="0" align="absmiddle">
				</a>
			</span>
		</div>';
		return $manageTools;
	}

	function getNewCampusLinkButton(){
		global $button_new,$i_CampusLink_add,$LAYOUT_SKIN;
		$libinterface = new interface_html(); 
		$newButton = '
		<span class="board_tool">
			<span class="table_row_tool">
				 '.$libinterface->Get_Thickbox_Link(250, 600, "add", $i_CampusLink_add, "Show_CompusLink_edit_box({mode:1}); return false;").'				
			
			</span> 
			
			<!-- <a id="campusSettingButton" href="javascript:void(0)" onclick="newCampusSetting(this)">
				<img src="/images/'.$LAYOUT_SKIN.'/icon_new.gif" title="'.$button_new.'" border="0" align="absmiddle">
			</a> -->
		</span>';
		return $newButton;
	}
	
	function getNewCampusLinkDialogBox(){
		global $button_add, $button_cancel,$i_CampusLinkURL,$i_CampusLinkTitle;
		$newCampusLinkDialog = '
		<table id="CampusLinkInputField" align="center" style="width:215px">
			<tr>
				<td class="form_field">'.$i_CampusLinkTitle.':</td>
				<td><input class="tabletext" style="width:160px" type="text" id="linkTitle_new" name="linkTitle_new"></td>
			</tr>
			<tr>
				<td class="form_field">'.$i_CampusLinkURL.':</td>
				<td><input style="width:160px" class="tabletext" type="text" id="linkURL_new" name="linkURL_new"></td>
			</tr>
			<tr>
				<td colspan="2" align="center" style="border:0px;border-top:1px;border-style:dashed;border-color:#cccccc;padding-top:5px">
					<input id="CampusLinkAdd" name="CampusLinkAdd" type="button" class="formsubbutton"  onMouseOver="this.className=\'formsubbuttonon\'" onMouseOut="this.className=\'formsubbutton\'" onclick="insertNewCampusLink()" value="'.$button_add.'">	
					<input id="CampusLinkCancel" name="CampusLinkCancel" type="button" class="formsubbutton"  onMouseOver="this.className=\'formsubbuttonon\'" onMouseOut="this.className=\'formsubbutton\'" onclick="closeNewCampusSetting()" value="'.$button_cancel.'">
				</td>
			</tr>
		</table>';		
		return $newCampusLinkDialog;
	}

	function getCampusLinkSet($linkID, $title, $url, $count, $visibleTo="ALL", $viewOnly=false, $portal=false){
		global $intranet_session_language, $Lang;
		
		$manageTools = "";
		if ($_SESSION["SSV_USER_ACCESS"]["other-campusLink"] == 1 && !$viewOnly)
		{
			$manageTools = $this->getManageTools($linkID, $count);
			//$editField = $this->getEditCampusLinkField($linkID,$title,$url);
			
			if (strstr($visibleTo, "STAFF_T") || $visibleTo=="ALL")
			{
				$vis2Arr[] = $Lang['Identity']['TeachingStaff'];
			}
			if (strstr($visibleTo, "STAFF_NT") || $visibleTo=="ALL")
			{
				$vis2Arr[] = $Lang['Identity']['NonTeachingStaff'];
			}
			if (strstr($visibleTo, "STUDENT") || $visibleTo=="ALL")
			{
				$vis2Arr[] = $Lang['Identity']['Student'];
			}
			if (strstr($visibleTo, "PARENT") || $visibleTo=="ALL")
			{
				$vis2Arr[] = $Lang['Identity']['Parent'];
			}
			if (is_array($vis2Arr))
			{
				$title_title = "[".$Lang['CampusLink']['VisibleTo'] . "]: ". implode(", ", $vis2Arr);
			} else
			{
				$title_title = $Lang['CampusLink']['VisibleNULL'];
				$title_italic_s = "<i>";
				$title_italic_e = "</i>";
			}	
		}
		$lu = new libuser($_SESSION["UserID"]);
		$login = $lu->UserLogin;
		$key = $lu->sessionKey;
		$url = str_replace("[eclass_key]",$key,$url);
		$url = str_replace("[eclass_loginID]",$login,$url);
		$url = str_replace("[eclass_language]",$intranet_session_language,$url);
		
		// only for eclass store admin page post back checking
		global $elcass_server_url;
		$url = str_replace("[eclass_url]",rawurlencode($elcass_server_url),$url);

		if (! $portal) {
            $links='
            <div id="CampusLinkSet-'.$linkID.'" style="width:207px">
                <a class="campus_link" id="campus_link-'.$linkID.'" href="'.$url.'" target="_blank" title="'.$title_title.'">'.$title_italic_s.$title.$title_italic_e.'</a>
                
                    '.$manageTools.'
                
                '.$editField.'
            </div>';
        } else {
            $links = "<a href=\"{$url}\" target=\"_blank\" class=\"indexquickbtn-item\">
                          <div class=\"indexquick-icon link\"></div>
                          <div class=\"indexquickbtn\">{$title}</div>
                      </a>";

        }
		return $links;
	}

	function getEditCampusLinkField($linkID,$title,$url){
		global $button_edit,$button_cancel,$i_CampusLink_edit,$i_CampusLinkTitle,$i_CampusLinkURL;
		$inputField = '
		<div class="board_title_setting" id="CampusLinkSettingBlock-'.$linkID.'" style="display:none">
			<div class="form_field" style="padding:5px">'.$i_CampusLink_edit.'</div>
			<p class="spacer"> </p>&nbsp;
			<table id="CampusLinkInputField-'.$linkID.'" align="center" style="width:215px">
					<tr>
						<td class="form_field">'.$i_CampusLinkTitle.':</td>
						<td><input class="tabletext" style="width:160px" type="text" id="linkTitle-'.$linkID.'" name="linkTitle-'.$linkID.'" value="'.$title.'" ></td>
					</tr>
					<tr>
						<td class="form_field">'.$i_CampusLinkURL.':</td>
						<td><input style="width:160px" class="tabletext" type="text" id="linkURL-'.$linkID.'" name="linkURL-'.$linkID.'" value="'.$url.'" ></td>
					</tr>
					<tr>
						<td colspan="2" align="center" style="border:0px;border-top:1px;border-style:dashed;border-color:#cccccc;padding-top:5px">
							<input id="CampusLinkEdit-'.$linkID.'" name="CampusLinkEdit-'.$linkID.'" type="button" class="formsubbutton"  onMouseOver="this.className=\'formsubbuttonon\'" onMouseOut="this.className=\'formsubbutton\'" onclick="editCampusSetting('.$linkID.')" value="'.$button_edit.'">	
							<input id="CampusLinkCancel-'.$linkID.'" name="CampusLinkCancel-'.$linkID.'" type="button" class="formsubbutton"  onMouseOver="this.className=\'formsubbuttonon\'" onMouseOut="this.className=\'formsubbutton\'" onclick="closeCampusSetting('.$linkID.')" value="'.$button_cancel.'">
						</td>
					</tr>
			</table>
		</div> ';
		return $inputField;
		
	}


}



?>