<?php

# Modifying: 

################################################# Change log ####################################################
# 2013-08-06 by Henry Chan: file created
#################################################################################################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
intranet_auth();
intranet_opendb();

//if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
//	header ("Location: /");
//	intranet_closedb();
//	exit();
//}

$TargetDate = $_REQUEST['TargetDate'];

$linterface = new interface_html();
$lprofile = new libprofile();
$LessonAttendUI = new libstudentattendance_ui();
$lu = new libuser($UserID);

//security issue
//$hasRight = true;
$hasRight = false;
//debug_pr($lu->RecordType);
if ($lu->RecordType == 1)  # Teacher
{
//    $lteaching = new libteaching();
//    $class = $lteaching->returnTeacherClass($UserID);
//    $lclass = new libclass();
//    $classname = $class[0][1];
//    if ($classname == "")
//    {
//        $hasRight = false;
//    }
//    else
//    {
//	    # class selection
//	    $classid = $classid ? $classid : $class[0][0];
//	    $class_selection = getSelectByArray($class,"name='classid' onChange='change_class()'",$classid, 0, 1);
//	    
//	    # re-get classname
//	    $classname = $lclass->getClassName($classid);
//	    
//	    $lu_student = new libuser($studentid);
//        if ($lu_student->UserID=="" || $lu_student->ClassName != $classname)
//        {
//            $studentid = "";
//        }
//        if ($studentid == "")
//        {
//            $namelist = $lclass->returnStudentListByClass($classname);
//            $studentid = $namelist[0][0];
//            if ($studentid == "") $studentid = $UserID;
//        }
//        $selection = $lclass->getStudentSelectByClass($classname,"name=studentid",$studentid);
//    }
}
else if ($lu->RecordType == 3)  # Parent
{
	$hasRight = true;
//     $lfamily = new libfamily();
//     if ($studentid == "")
//     {
//         $studentid = $lfamily->returnFirstChild($UserID);
//     }
//     $children_list = $lfamily->returnChildrens($UserID);
//     if (!in_array($studentid,$children_list))
//     {
//        $hasRight = false;
//     }
//     if ($studentid == "")
//     {
//         $hasRight = false;
//     }
//     $selection = $lfamily->getSelectChildren($UserID,"name=studentid",$studentid);
}
else if ($lu->RecordType == 2)# Student
{
	$hasRight = true;
       $studentid = $UserID;
}

if(!$hasRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
//security issue end
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageLessonAttendance";

### Title ###
$TAGS_OBJ[] = array($Lang['LessonAttendance']['DailyLessonStatus'],"",0);
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(); 

$TargetUserID = '';
$ChildrenSelection = '';
if ($_SESSION['UserType'] == USERTYPE_STAFF)
{
	$TargetUserID = $_SESSION['UserID'];
}
else if ($_SESSION['UserType'] == USERTYPE_STUDENT)
{
	$TargetUserID = $_SESSION['UserID'];
	echo $LessonAttendUI->Get_Class_Lesson_Daily_Report_Form_Student();
	
}
else if ($_SESSION['UserType'] == USERTYPE_PARENT)
{
	$hasRight = true;
	$lfamily = new libfamily();
	$studentid = $children_id;
    if ($studentid == "")
    {
        $studentid = $lfamily->returnFirstChild($UserID);
    }
    $children_list = $lfamily->returnChildrens($UserID);
    if (!in_array($studentid,$children_list))
    {
        $hasRight = false;
    }
    if ($studentid == "")
    {
        $hasRight = false;
    }
    if(!$hasRight)
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
	$libuser = new libuser($_SESSION['UserID']);
	$ChildrenSelection = $libuser->getChildrenSelection('ChildrenID', 'ChildrenID', 'js_Changed_Children_Selection(this.value);', '', $_REQUEST['children_id']);


### Display Option button
$thisOnchange = "js_Show_Hide_Display_Option_Div(); return false;";
$DisplayOptionBtn = '<a href="#" class="tablelink" onclick="'.$thisOnchange.'"> '.$Lang['SysMgr']['Timetable']['DisplayOption'].'</a>'."\n";

### Filter Button
$thisOnchange = "js_Show_Hide_Filter_Div(); return false;";
$FilterBtn = '<a href="#" class="tablelink" onclick="'.$thisOnchange.'"> '.$Lang['SysMgr']['Timetable']['DataFiltering'].'</a>'."\n";


$x = '';
//$x .= '<br />'."\n";
//$x .= '<form id="form2" name="form2" method="post">'."\n";
	//$x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
	//$x .= '<br style="clear:both;" />'."\n";
//	$x .= '<div class="table_board">'."\n";
		$x .= '<table id="html_body_frame" align="center" width="95%">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div style="float:left;">'.$ChildrenSelection.'</div>'."\n";
//					$x .= '<div id="FilterButtonDiv" style="float:right;display:none;">'."\n";
//						$x .= $DisplayOptionBtn;
//						$x .= ' | ';
//						$x .= $FilterBtn;
//					$x .= '</div>'."\n";
//					$x .= '<div style="clear:both" />'."\n";
//					$x .= '<div style="float:right;position:relative;left:-200px;z-index:99;">'."\n";
//						$x .= '<table>'."\n";
//							$x .= '<tr>'."\n";
//								$x .= '<td>'."\n";
//								if($_REQUEST['children_id']){
//									$x .= '<div id="DisplayOptionDiv" class="selectbox_layer" style="width:200px;">'.$LessonAttendUI->Get_Class_Lesson_Daily_Report_Form_Student($_REQUEST['children_id']).'</div>'."\n";
//								}
//								$x .= '</td>'."\n";
//							$x .= '</tr>'."\n";
//						$x .= '</table>'."\n";
//					$x .= '</div>'."\n";
//					
//					$x .= '<div style="float:right;position:relative;left:-450px;z-index:99;">'."\n";
//						$x .= '<table>'."\n";
//							$x .= '<tr>'."\n";
//								$x .= '<td>'."\n";
//									$x .= '<div id="FilterSelectionDiv" class="selectbox_layer" style="width:450px;overflow:auto;">'."\n";
//									$x .= '</div>'."\n";
//								$x .= '</td>'."\n";
//							$x .= '</tr>'."\n";
//						$x .= '</table>'."\n";
//					$x .= '</div>'."\n";
//					
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
//			$x .= '<tr><td><div id="TimetableDiv"></div></td></tr>'."\n";
		$x .= '</table>'."\n";
//	$x .= '</div>'."\n";
////$x .= '</form>'."\n";
$x .= '<br />'."\n";

echo $x;

if($_REQUEST['children_id']){
	echo $LessonAttendUI->Get_Class_Lesson_Daily_Report_Form_Student($_REQUEST['children_id']);
}
}
//echo $LessonAttendUI->Get_Class_Lesson_Daily_Report_Form_Student();

$linterface->LAYOUT_STOP();

intranet_closedb();
?>
<script language="javascript">
var jsTargetUserID = '<?=$TargetUserID?>';

//$(document).ready(function () {
//	if (jsTargetUserID != '')
//		js_Reload_Filtering();
//});

//function js_Reload_Timetable()
//{
//	if (jsTargetUserID == '')
//	{
//		$('div#TimetableDiv').html('');
//		$('div#FilterButtonDiv').hide();
//	}
//	else
//	{
//		var jsSubjectGroupIDValue = $('select#SubjectGroupIDArr\\[\\]').val();
//		if (jsSubjectGroupIDValue == null)
//			jsSubjectGroupIDValue = '';
//		else
//			jsSubjectGroupIDValue = jsSubjectGroupIDValue.join(',');
//		
//		$('div#TimetableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
//			"ajax_get_lesson_attendance.php", 
//			{ 
//				Action: "Personal_Timetable",
//				TargetUserID: jsTargetUserID,
//				SubjectGroupIDList: jsSubjectGroupIDValue
//			},
//			function(ReturnData)
//			{
//				js_Update_Timetable_Display();
//				$('div#FilterButtonDiv').show();
//			}
//		);
//	}
//}

function js_Changed_Children_Selection(jsChildrenID)
{
//	jsTargetUserID = jsChildrenID;
//	js_Reload_Filtering();
	if(jsChildrenID)
	window.location.href="lesson_attendance.php?children_id="+jsChildrenID;
}

function js_Reload_Filtering()
{
	$('div#FilterSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_get_lesson_attendance.php", 
		{ 
			Action: "Filter_Table",
			TargetUserID: jsTargetUserID
		},
		function(ReturnData)
		{
			js_Reload_Timetable();
		}
	);
}

//function View_Lesson_Summary() {
//	var FromDate = document.getElementById('FromDate').value;
//	var ToDate = document.getElementById('ToDate').value;
//	var ClassID = document.getElementById('ClassID').options[document.getElementById('ClassID').selectedIndex].value;
//	var StudentID = '';
//	if(document.getElementById('opt_student[]') != null && document.getElementById('PersonalSelection').checked){
//		StudentID = Get_Selection_Value("opt_student[]","Array");
//	}
//	
//	//var StudentID = '';
//	//alert(StudentID);
//	var re = /\b\d{4}[\/-]\d{1,2}[\/-]\d{1,2}\b/;
//	
//	if (re.test(FromDate) && re.test(ToDate)) {
//		$("span#TargetDateWarningLayer").hide();
//		Block_Element('DetailTableLayer');
//		
//		PostVar = {
//			"FromDate":FromDate,
//			"ToDate":ToDate,
//			"ClassID":ClassID,
//			"AttendStatus[]":Get_Check_Box_Value('AttendStatus[]','Array'),
//			"StudentID[]":StudentID
//		}
//		
//		$("div#DetailTableLayer").load('ajax_get_lesson_summary_report.php',PostVar,function() {UnBlock_Element('DetailTableLayer');});
//	}
//	else {
//		$("span#TargetDateWarningLayer").html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>');
//		$("span#TargetDateWarningLayer").show();
//	}
//}
function View_Lesson_Daily_Overview() {
	var FromDate = document.getElementById('FromDate').value;
	var ToDate = document.getElementById('ToDate').value;
	//var TargetDate = document.getElementById('TargetDate').value;
	
	var re = /\b\d{4}[\/-]\d{1,2}[\/-]\d{1,2}\b/;
	
	//date validation
	if(FromDate > ToDate/* || ToDate > '<?=date('Y-m-d')?>'*/){
		//alert("<?=$Lang['StaffAttendance']['InvalidDateRange']?>");
		$('#ToDateWarningLayer').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>').show();
		return;
	}
	
	if(Get_Check_Box_Value('AttendStatus[]','Array') == ""){
		alert("<?=$Lang['StudentAttendance']['PleaseSelectAtLeastOneStatus']?>");
		return;
	}
	
	//if (re.test(TargetDate)) {
	if (re.test(FromDate) && re.test(ToDate)){
		$("span#TargetDateWarningLayer").hide();
		Block_Element('DetailTableLayer');
		
		PostVar = {
			//"TargetDate":TargetDate,
			"FromDate":FromDate,
			"ToDate":ToDate,
			"StudentID":<?=($_REQUEST['children_id']?$_REQUEST['children_id']:$_SESSION['UserID'])?>,
			"AttendStatus[]":Get_Check_Box_Value('AttendStatus[]','Array')
		};
		
		$("div#DetailTableLayer").load('ajax_get_lesson_attendance.php',PostVar,function() {UnBlock_Element('DetailTableLayer');});
	}
	else {
		$("span#TargetDateWarningLayer").html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>');
		$("span#TargetDateWarningLayer").show();
	}
}
</script>