<?php

// using : 

/*
 *  2019-09-24 [Tommy] change access right checking from key to $msg
 *	2019-06-04 [Henry] added CSRF protection
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

// if(!verifyCsrfToken($_POST['csrf_token'],$_POST['csrf_token_key'])){
// 	No_Access_Right_Pop_Up();
// 	exit;
// }

if($msg == ""){
    No_Access_Right_Pop_Up();
    exit;
}

$linterface = new interface_html("imail_archive.html");
$CurrentPage = "PageResetPassword";

$lauth = new libauth();
$li = new libuser();

### Title ###
$TAGS_OBJ[] = array($Lang['ForgotHashedPassword']['ResetPassword']);

$MODULE_OBJ['title'] = $iAccount;

$xmsg = $Lang['ForgotHashedPassword']['WarnMsgArr'][$msg];

$linterface->LAYOUT_START();
?>
<script language="javascript">
if("<?=$msg?>" == "UpdateSuccess")
{
	setTimeout("window.location='../../../templates/' ", 5000);
}
</script>

<table wiidth="100%" cellpadding=50>
<tr>
	<td class="tabletext" valign="top" align="center"><?=$xmsg?></td>
</tr>
</table>
<div class="edit_bottom_v30">&nbsp;</div>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
