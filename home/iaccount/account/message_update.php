<?php
// Using:

/*
 * Modification Log:
 *  2019-06-04 Henry
 * 		- added CSRF protection
 *  2019-02-22  Bill	[2019-0221-0952-39207]
 *   - fixed: not update modified user in INTRANET_USER
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");

intranet_opendb();

if(!verifyCsrfToken($_POST['csrf_token'],$_POST['csrf_token_key'])){
	No_Access_Right_Pop_Up();
	exit;
}

$li = new libdb();
$lc = new libeclass();
$lu = new libuser($UserID);
// $isStudent=$lu->isStudent();

$Info = intranet_htmlspecialchars(trim($Info));

# Check whether can change the info
if ($lu->RetrieveUserInfoSetting("CanUpdate", "Message"))
{
     $fieldname .= "Info = '".$Info."', ";
     $fieldname .= "DateModified = now(), ModifyBy = '".$_SESSION['UserID']."'";
     
     $sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = '$UserID'";
     $result = $li->db_db_query($sql);
     
     $newmail = "";
     if ($result)
     {
         $lc->eClassUserUpdateInfoIP($lu->UserEmail, $lu->Title, $lu->EnglishName,addslashes($lu->ChineseName),$lu->FirstName,$lu->LastName, $lu->NickName, $lu->UserPassword, $lu->ClassName,$lu->ClassNumber, $newmail,$lu->Gender, $lu->ICQNo,$lu->HomeTelNo,$lu->FaxNo,$lu->DateOfBirth,$lu->Address,$lu->Country,$lu->URL,$Info);
         $signal = "UpdateSuccess";
     }
     else
     {
         $signal = "UpdateUnsuccess";
     }
}
else
{
    // do nothing
}
intranet_closedb();

header("Location: message.php?msg=$signal");
?>