<?php
# using: 

#####################################
#
#	Date:	2019-02-22  Bill	[2019-0221-0952-39207]
#			fixed: not update modified user in INTRANET_USER
#
#	Date:	2015-10-13	Yuen	[2015-0922-1008-00071]
#			fixed: syn user information (ie. photo) to eLib Plue database
#
#	Date:	2013-04-03	YatWoon
#			add $return_path
#
#####################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
include($PATH_WRT_ROOT."includes/libuser.php");
include($PATH_WRT_ROOT."includes/libfilesystem.php");
include($PATH_WRT_ROOT."includes/libuserphoto.php");

intranet_auth();
intranet_opendb();

$li = new libuser($UserID);
$lf = new libfilesystem();
$lphoto = new libuserphoto();

if($PhotoType=="official")
{
	$path = $intranet_root.$li->PhotoLink;
	if (is_file($path) && strtoupper($lf->file_ext($li->PhotoLink))!="")
	{
	    $lf->file_remove($path);
	}
	
	$fieldname = '';
	$fieldname .= "PhotoLink = '', ";
	$fieldname .= "DateModified = now(), ModifyBy = '".$_SESSION['UserID']."'";
	
	$sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = '$UserID'";
	
	if ($lphoto->isUserPersonalPhotoLinkedToOfficialPhoto($UserID)) {
	    $lphoto->cancelOfficialPhotoAsPersonalPhoto($UserID);
	}
}
else if ($PhotoType=="personal")
{
	$path = $intranet_root.$li->PersonalPhotoLink;
	if (is_file($path) && strtoupper($lf->file_ext($li->PersonalPhotoLink))!="")
	{
	    $lf->file_remove($path);
	}
	
	$fieldname = '';
	$fieldname .= "PersonalPhotoLink = '', ";
	$fieldname .= "DateModified = now(), ModifyBy = '".$_SESSION['UserID']."'";
	
	$sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = '$UserID'";
	
	if ($lphoto->isUserPersonalPhotoLinkedToOfficialPhoto($UserID)) {
	    $lphoto->deletePersonalPhotoTempFile($UserID);
	}
}

$return_path = $return_path ? $return_path : "index.php";
$li->db_db_query($sql);

$li->synUserDataToModules($UserID);

header("Location: ". $return_path ."?msg=photo_delete");
?>