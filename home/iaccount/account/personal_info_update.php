<?php
// Modifying By 

######### Change Log
#
#	Date:	2019-06-04 Henry
#			added CSRF protection
#
#	Date:	2019-02-22  Bill	[2019-0221-0952-39207]
#			fixed: not update modified user in INTRANET_USER
#
#   Date:   2018-09-06 Anna
#           Double confirm user type and whether UserID changed 
#
#	Date:	2015-10-13	Yuen	[2015-0922-1008-00071]
#			fixed: syn user information (ie. photo) to eLib Plue database
#
#	Date:	2015-04-28	Bill	[2015-0428-1735-39170]
#			fixed: cannot update official photo url stored in db 
#
#	Date:	2010-08-11	yatWoon
#			hide "Title" setting
#
##########################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");

intranet_opendb();

if(!verifyCsrfToken($_POST['csrf_token'],$_POST['csrf_token_key'])){
	No_Access_Right_Pop_Up();
	exit;
}

$li = new libdb();
$lc = new libeclass();
$lu = new libuser($UserID);

if($_SESSION['UserType'] == USERTYPE_STUDENT && $_GET['UserID'] != $_SESSION['UserID'] && $_GET['UserID'] != '') {
    No_Access_Right_Pop_Up();
}

$NickName = intranet_htmlspecialchars(trim($NickName));
// $Title = intranet_htmlspecialchars(trim($Title));
$Gender = intranet_htmlspecialchars(trim($Gender));
$DateOfBirth = intranet_htmlspecialchars(trim($DateOfBirth));

# Check whether can change the info
$failed = false;
if (!$failed)
{
     # Update photo
     ################################################
     # Official Photo
     ################################################
     $re_path = "/file/user_photo";
     $path = "$intranet_root$re_path";
     $target = "";
     if($photo == "none" || $photo == "") {
         // do nothing
     }
     else
     {
         $lf = new libfilesystem();
         if (!is_dir($path))
         {
             $lf->folder_new($path);
         }
         
         $ext = strtolower($lf->file_ext($photo_name));
         if ($ext == ".jpg")
         {
             $target = $path ."/". $lu->UserLogin . $ext;
             $re_path .= "/". $lu->UserLogin . $ext;
             $lf->lfs_copy($photo, $target);
             
			 # Check need to resize or not
			 include_once($PATH_WRT_ROOT."includes/SimpleImage.php");
			 $im = new SimpleImage();
			 if($im->GDLib_ava)
			 {
				$im->load($target);
				$w = $im->getWidth();
				$h = $im->getHeight();
				$default_photo_width = $default_photo_width ? $default_photo_width : 100;
				$default_photo_height = $default_photo_height ? $default_photo_height : 130;
				if($w > $default_photo_width || $h > $default_photo_height)	
				{
					$im->resize($default_photo_width,$default_photo_height);
					$im->save($target);	
				}
			 }
         }
     }
     ################################################
     # Official Photo [End]
     ################################################
     
     ################################################
     # Personal Photo
     ################################################
     $re_path2 = "/file/photo/personal";
     $path2 = "$intranet_root$re_path2";
     // prevent cannot update official photo url in db 
     //$target = "";
     $target2 = "";
     if($personal_photo == "none" || $personal_photo == "") {
         // do nothing
     }
     else
     {
         $lf = new libfilesystem();
         if (!is_dir($path2))
         {
             $lf->folder_new($path2);
         }
         
         $ext = strtolower($lf->file_ext($personal_photo_name));
         if ($ext == ".jpg")
         {
             $target2 = $path2 ."/p". $lu->UserID . $ext;
             $re_path2 .= "/p". $lu->UserID . $ext;
             $lf->lfs_copy($personal_photo, $target2);
             
			 # Check need to resize or not
			 include_once($PATH_WRT_ROOT."includes/SimpleImage.php");
			 $im = new SimpleImage();
			 if($im->GDLib_ava)
			 {
				$im->load($target2);
				$w = $im->getWidth();
				$h = $im->getHeight();
				$default_photo_width = $default_photo_width ? $default_photo_width : 100;
				$default_photo_height = $default_photo_height ? $default_photo_height : 130;
				if($w > $default_photo_width || $h > $default_photo_height)	
				{
					$im->resize($default_photo_width,$default_photo_height);
					$im->save($target2);	
				}
			 }
         }
     }
     ################################################
     # Personal Photo [End]
     ################################################
     
//       if($misca[0] == "")
//         $fieldname .= "Title = '$Title', ";
     
     if($lu->RetrieveUserInfoSetting("CanUpdate", "NickName")) {
        $fieldname .= "NickName = '$NickName', ";      
     }
     if($lu->RetrieveUserInfoSetting("CanUpdate", "Gender")) {
        $fieldname .= "Gender = '$Gender', ";
     }
     if($lu->RetrieveUserInfoSetting("CanUpdate", "DOB")) {
        $fieldname .= "DateOfBirth = '$DateOfBirth', ";
     }
     $fieldname .= "DateModified = now(), ModifyBy = '".$_SESSION['UserID']."'";
     
     if ($target != "") {
         $fieldname .= ",PhotoLink = '$re_path'";
     }
     if ($target2 != "") {
         $fieldname .= ",PersonalPhotoLink = '$re_path2'";
     }
     
     $sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = '$UserID'";
     $result = $li->db_db_query($sql);
     
     $newmail = "";
     if ($result)
     {
         $lc->eClassUserUpdateInfoIP($lu->UserEmail, $Title, $lu->EnglishName,addslashes($lu->ChineseName),$lu->FirstName,$lu->LastName, $NickName, $lu->UserPassword, $lu->ClassName,$lu->ClassNumber, $newmail,$Gender, $lu->ICQNo,$lu->HomeTelNo,$lu->FaxNo,$DateOfBirth,$lu->Address,$lu->Country,$lu->URL,$lu->Info);
         $signal = "UpdateSuccess";
         
         $lu->synUserDataToModules($lu->UserID);
     }
     else
     {
         $signal = "UpdateUnsuccess";
     }
}
else
{
    // do nothing
}
intranet_closedb();

header("Location: index.php?msg=$signal");
?>