<?
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);

$hasRight = true;
if ($lu->RecordType != 1) 
{
	$hasRight = false;
	$studentid = $UserID;
}

if(!$hasRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$linterface = new interface_html();
$lpayment = new libpayment();
$lclass = new libclass();

# Parameters of page
//Default value of number of line in each page
$defaultNumOfLine = 45;

//Default width of a specific field
$defaultFieldWidth1 = 53;

//current number of line remaining
$lineRemain = $defaultNumOfLine;

//how many line need
$fieldLineNeed = 1;

# Page layout details
$page_breaker = "<P CLASS='breakhere'>";
$page_header_linefeed = 8;
$intermediate_pageheader = "<P CLASS='breakhere'>";
$intermediate_linefeed = 5;
?>
<link href="../../templates/2007a/css/content.css" rel="stylesheet" type="text/css" />

<STYLE TYPE="text/css">
     P.breakhere {page-break-before: always}
</STYLE>

<?

if($StudentID=="")
{
	$cond = " a.ClassName = '$ClassName' ";
}
else
{
	$cond = " a.UserID = '$StudentID' ";
}

$namefield = getNameFieldWithClassNumberByLang("a.");
$sql = "SELECT 
				a.UserID, $namefield, IF(b.PPSAccountNo IS NULL, ' -- ', b.PPSAccountNo), CONCAT('$',IF(b.Balance>=0,FORMAT(b.Balance,1),'0.0') ), if(b.LastUpdated IS NULL, ' -- ', b.LastUpdated)
		FROM 
				INTRANET_USER AS a LEFT OUTER JOIN 
				PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID)
		WHERE
				$cond
				AND RecordType = 2
				AND RecordStatus = 1
		ORDER BY
				a.ClassName, a.ClassNumber, a.EnglishName";
				
$payment_account_result = $lpayment->returnArray($sql, 5);
	
if(sizeof($payment_account_result)>0)
{
	$lineRemain = $defaultNumOfLine - $page_header_linefeed;
	
	for($i=0; $i<sizeof($payment_account_result); $i++)
	{
		list($u_id, $name, $pps, $bal, $last_modified) = $payment_account_result[$i];

		$sql2  = "SELECT
		               DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),
		               IF(a.TransactionType=1,DATE_FORMAT(b.TransactionTime,'%Y-%m-%d %H:%i'),' -- '),
		               CASE a.TransactionType
		                    WHEN 1 THEN '$i_Payment_TransactionType_Credit'
		                    WHEN 2 THEN '$i_Payment_TransactionType_Payment'
		                    WHEN 3 THEN '$i_Payment_TransactionType_Purchase'
		                    WHEN 4 THEN '$i_Payment_TransactionType_TransferTo'
		                    WHEN 5 THEN '$i_Payment_TransactionType_TransferFrom'
		                    WHEN 6 THEN '$i_Payment_TransactionType_CancelPayment'
		                    WHEN 7 THEN '$i_Payment_TransactionType_Refund'
		                    WHEN 8 THEN '$i_Payment_PPS_Charge'
		                    ELSE '$i_Payment_TransactionType_Other' END,
		               IF(a.TransactionType IN (1,5,6),CONCAT('$',FORMAT(a.Amount,1)),' -- '),
		               IF(a.TransactionType IN (2,3,4,7,8),CONCAT('$',FORMAT(a.Amount,1)),' -- '),
		               a.Details, CONCAT('$',IF(a.BalanceAfter>=0,FORMAT(a.BalanceAfter,1),'0.0') ),
		               IF(a.RefCode IS NULL, ' -- ', a.RefCode)
		         FROM
		               PAYMENT_OVERALL_TRANSACTION_LOG as a
		               LEFT OUTER JOIN PAYMENT_CREDIT_TRANSACTION as b ON a.RelatedTransactionID = b.TransactionID
		         WHERE
		               a.StudentID =$u_id";

    
		$lineRemain -= 8;
		
		if ($lineRemain <0)
		{
            $table_content .= "$intermediate_pageheader";
            $lineRemain = $defaultNumOfLine - 8;
		}
		
					
        $table_content .= "<table width=90% border=0 cellpadding=5 cellspacing=0 align=center>";
        $table_content .= "<tr><td class='eSportprinttitle'>".$i_identity_student.": ".$name."</td></tr>";
		$table_content .= "<tr><td class='eSportprinttitle'>".$i_Payment_Field_Balance.": ".$bal."</td></tr>";
		$table_content .= "<tr><td class='eSportprinttitle'>".$i_Payment_Field_LastUpdated.": ".$last_modified."</td></tr>";
		$table_content .= "<tr><td class='eSportprinttitle'>".$i_Payment_Field_PPSAccountNo.": ".$pps."</td></tr>";
		
							
		$table_content .= "<tr><td><table width='100%' border='0' cellspacing='0' cellpadding='4' class='eSporttableborder'>";
		$table_content .= "<tr><td width=15% class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_TransactionTime</td>";
		$table_content .= "<td width=15% class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_TransactionFileTime</td>";
		$table_content .= "<td width=10% class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_TransactionType</td>";
		$table_content .= "<td width=10% class='eSporttdborder eSportprinttabletitle'>$i_Payment_TransactionType_Credit</td>";
		$table_content .= "<td width=10% class='eSporttdborder eSportprinttabletitle'>$i_Payment_TransactionType_Debit</td>";
		$table_content .= "<td width=20% class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_TransactionDetail</td>";
		$table_content .= "<td width=10% class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_BalanceAfterTransaction</td>";
		$table_content .= "<td width=10% class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_RefCode</td></tr>";
		       
		$payment_detail_result = $lpayment->returnArray($sql2,7);
		
		if(sizeof($payment_detail_result)>0)
		{
			for ($z=0; $z<sizeof($payment_detail_result); $z++)
			{

	            list($transaction_time, $credit_transaction_time, $transaction_type, $credit_amount, $debit_amount, $transaction_detail, $balance_after, $ref_no) = $payment_detail_result[$z];
	
	            $currentLineNeed = ceil ( strlen($transaction_time.$credit_transaction_time.$transaction_type.$credit_amount.$debit_amount.$transaction_detail.$balance_after.$ref_no) / $defaultFieldWidth1 );
	
	            if ($fieldLineNeed < $currentLineNeed)
	                    $fieldLineNeed = $currentLineNeed;
	
			}
			$lineRemain = $lineRemain - $fieldLineNeed * sizeof($payment_detail_result) ;
			
			for($j=0; $j<sizeof($payment_detail_result); $j++)
			{
				list($transaction_time, $credit_transaction_time, $transaction_type, $credit_amount, $debit_amount, $transaction_detail, $balance_after, $ref_no) = $payment_detail_result[$j];
				
				$table_content .= "<tr><td class='eSporttdborder eSportprinttext'>$transaction_time&nbsp;</td>";
				$table_content .= "<td class='eSporttdborder eSportprinttext'>$credit_transaction_time&nbsp;</td>";
				$table_content .= "<td class='eSporttdborder eSportprinttext'>$transaction_type&nbsp;</td>";
				$table_content .= "<td class='eSporttdborder eSportprinttext'>$credit_amount&nbsp;</td>";
				$table_content .= "<td class='eSporttdborder eSportprinttext'>$debit_amount&nbsp;</td>";
				$table_content .= "<td class='eSporttdborder eSportprinttext'>$transaction_detail&nbsp;</td>";
				$table_content .= "<td class='eSporttdborder eSportprinttext'>$balance_after</td>";
				$table_content .= "<td class='eSporttdborder eSportprinttext'>". ($ref_no==""?"--":$ref_no) ."</td></tr>";
			}
		}
		if(sizeof($payment_detail_result)==0)
		{
			$lineRemain = $lineRemain - 1;
			$table_content .= "<tr><td class='tabletext' colspan=8 align=center align='center'>$i_no_record_exists_msg</td></tr>";
		}
		$table_content .= "</table></td></tr>";
		$table_content .= "</table>";
		$table_content .= "<table border=0>";
		$table_content .= "<tr><td height=20px></td></tr>";
		$table_content .= "</table>";
		if($i<sizeof($payment_account_result)-1)
			$table_content .="<P CLASS='breakhere'></P>";
	}
}
$print_btn_table ="<table width=\"90%\" align=\"center\" class=\"print_hide\" border=\"0\"><tr><td align=\"right\">".$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")."</td></tr></table><BR>";

?>
<?=$print_btn_table?>
<?=$table_content?>

<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>