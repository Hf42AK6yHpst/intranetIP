<?php
## Modifying By: 
##### Change Log [Start] #####
#	Date	: 	2016-05-04	Carlos
#				Change hidden element "action" as "targetAction", to avoid naming conflict with form['action'].
#
#	Date	:	2015-04-23	Omas
# 				Create this file
#
###### Change Log [End] ######
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

if($targetAction="AccountOverview"){
	$lpayment = new libpayment();
	$lexport = new libexporttext();
	$lclass = new libclass();
	$htmlAry['resultTable'] = $lpayment->iAccountAccountOverview($className, $fromDate, $toDate);
}

?>
<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>
<?=$htmlAry['resultTable']?>
<?
intranet_closedb();
?>