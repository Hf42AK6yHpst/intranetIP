<?
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
$lclass = new libclass();
$lexport = new libexporttext();


$lu = new libuser($UserID);

$hasRight = true;
if ($lu->RecordType != 1) 
{
	$hasRight = false;
	$studentid = $UserID;
}

if(!$hasRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}



if($StudentID=="")
{
	$cond = " a.ClassName = '$ClassName' ";
}
else
{
	$cond = " a.UserID = '$StudentID' ";
}

$namefield = getNameFieldWithClassNumberByLang("a.");
$sql = "SELECT 
				a.UserID, $namefield, IF(b.PPSAccountNo IS NULL, ' -- ', b.PPSAccountNo), CONCAT(IF(b.Balance>=0,ROUND(b.Balance,1),'0.0') ), if(b.LastUpdated IS NULL, ' -- ', b.LastUpdated)
		FROM 
				INTRANET_USER AS a LEFT OUTER JOIN 
				PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID)
		WHERE
				$cond
				AND RecordType = 2
				AND RecordStatus = 1
		ORDER BY
				a.ClassName, a.ClassNumber, a.EnglishName";
				
$payment_account_result = $lpayment->returnArray($sql, 5);
	
$rows = array();
if(sizeof($payment_account_result)>0)
{
	for($i=0; $i<sizeof($payment_account_result); $i++)
	{
		list($u_id, $name, $pps, $bal, $last_modified) = $payment_account_result[$i];

		$sql2  = "SELECT
		               DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),
		               IF(a.TransactionType=1,DATE_FORMAT(b.TransactionTime,'%Y-%m-%d %H:%i'),' -- '),
		               CASE a.TransactionType
		                    WHEN 1 THEN '$i_Payment_TransactionType_Credit'
		                    WHEN 2 THEN '$i_Payment_TransactionType_Payment'
		                    WHEN 3 THEN '$i_Payment_TransactionType_Purchase'
		                    WHEN 4 THEN '$i_Payment_TransactionType_TransferTo'
		                    WHEN 5 THEN '$i_Payment_TransactionType_TransferFrom'
		                    WHEN 6 THEN '$i_Payment_TransactionType_CancelPayment'
		                    WHEN 7 THEN '$i_Payment_TransactionType_Refund'
		                    WHEN 8 THEN '$i_Payment_PPS_Charge'
		                    ELSE '$i_Payment_TransactionType_Other' END,
		               IF(a.TransactionType IN (1,5,6),CONCAT(ROUND(a.Amount,1)),' -- '),
		               IF(a.TransactionType IN (2,3,4,7),CONCAT(ROUND(a.Amount,1)),' -- '),
		               a.Details, CONCAT(IF(a.BalanceAfter>=0,ROUND(a.BalanceAfter,1),'0.0') ),
		               IF(a.RefCode IS NULL, ' -- ', a.RefCode)
		         FROM
		               PAYMENT_OVERALL_TRANSACTION_LOG as a
		               LEFT OUTER JOIN PAYMENT_CREDIT_TRANSACTION as b ON a.RelatedTransactionID = b.TransactionID
		         WHERE
		               a.StudentID =$u_id";

		$rows[] = array($i_identity_student, $name);
		$rows[] = array($i_Payment_Field_Balance, $bal);
		$rows[] = array($i_Payment_Field_LastUpdated, $last_modified);
		$rows[] = array($i_Payment_Field_PPSAccountNo, $pps);
		
		$rows[] = array($i_Payment_Field_TransactionTime, $i_Payment_Field_TransactionFileTime, $i_Payment_Field_TransactionType, $i_Payment_TransactionType_Credit, $i_Payment_TransactionType_Debit, $i_Payment_Field_TransactionDetail, $i_Payment_Field_BalanceAfterTransaction, $i_Payment_Field_RefCode);
		
		$payment_detail_result = $lpayment->returnArray($sql2,7);
		
		if(sizeof($payment_detail_result)>0)
		{
			for($j=0; $j<sizeof($payment_detail_result); $j++)
			{
				list($transaction_time, $credit_transaction_time, $transaction_type, $credit_amount, $debit_amount, $transaction_detail, $balance_after, $ref_no) = $payment_detail_result[$j];
				$rows[] = array($transaction_time, $credit_transaction_time, $transaction_type, $credit_amount, $debit_amount, $transaction_detail, $balance_after, $ref_no);
			}
		}
		if(sizeof($payment_detail_result)==0)
		{
			$rows[] = array($i_no_record_exists_msg);
		}
		
		$rows[] = array('');
		$rows[] = array('');
	}
}

if($StudentID=="")
{
	$filename = "Payment_Transaction_Log_(".$ClassName.")_".date("Y-m-d").".csv";
}
else
{
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	$lu = new libuser($StudentID);
	$filename = "Payment_Transaction_Log_(".$ClassName."-".$lu->ClassNumber.")_".date("Y-m-d").".csv";
}

$exportColumn = array();
for($i=1;$i<=8;$i++)
{
	$exportColumn[] = "";
}

$export_content .= $lexport->GET_EXPORT_TXT($rows,$exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);
?>
