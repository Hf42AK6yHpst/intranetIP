<?php
# using : 

############ Change Log [start] #################
#
#   2020-04-21 Tommy
#       - change selectbox value from getting all academic year to student in class
#
#
############ Change Log [end] #################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libmerit.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$lstudentprofile = new libstudentprofile();
$CurrentPage	= "PageMeritDemerit";
$lprofile = new libprofile();

$lu = new libuser($UserID);

$hasRight = true;
if ($lu->RecordType == 1)  # Teacher
{
    $lteaching = new libteaching();
    $class = $lteaching->returnTeacherClass($UserID);
    $lclass = new libclass();
    $classname = $class[0][1];
    if ($classname == "")
    {
        $hasRight = false;
    }
    else
    {
	    # class selection
	    $classid = $classid ? $classid : $class[0][0];
	    $class_selection = getSelectByArray($class,"name='classid' onChange='change_class()'",$classid, 0, 1);
	    
	    # re-get classname
	    $classname = $lclass->getClassName($classid);
	    
        $lu_student = new libuser($studentid);
        if ($lu_student->UserID=="" || $lu_student->ClassName != $classname)
        {
            $studentid = "";
        }
        if ($studentid == "")
        {
            $namelist = $lclass->returnStudentListByClass($classname);
            $studentid = $namelist[0][0];
            if ($studentid == "") $studentid = $UserID;
        }
        $selection = $lclass->getStudentSelectByClass($classname,"name=studentid",$studentid);
    }
}
else if ($lu->RecordType == 3)  # Parent
{
     $lfamily = new libfamily();
     if ($studentid == "")
     {
         $studentid = $lfamily->returnFirstChild($UserID);
     }
     $children_list = $lfamily->returnChildrens($UserID);
     if (!in_array($studentid,$children_list))
     {
         $hasRight = false;
     }
     if ($studentid == "")
     {
         $hasRight = false;
     }
     $selection = $lfamily->getSelectChildren($UserID,"name=studentid",$studentid);
}
else # Student
{
       $studentid = $UserID;
}

if(!$hasRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if ($hasRight)
{

$lmerit = new libmerit();

if($lu->RecordType != 1 || ($lu->RecordType == 1 && $studentid != $UserID)){
    $lclass = new libclass();
    $student_class = $lclass->returnStudentClassHistory($studentid, "DESC");
}

/*
$year_merit = $lmerit->returnYears($studentid);
// $year_array = array_merge($year_merit,$year_access,$year_files);
// $year_array = array_u;nique($year_array);
// rsort($year_array);
$year_array = array_unique($year_merit);
rsort($year_array);
*/
$AcademicYearInfo = GetAllAcademicYearInfo();

/*
for($i=0;$i<sizeof($AcademicYearInfo);$i++)
	$year_array[$i] = $AcademicYearInfo[$i]["YearName".(strtoupper($intranet_session_language))];
	
$select_year = getSelectByValue($year_array,"name=year",$year,1,0,$i_Attendance_AllYear);
*/

if($lu->RecordType != 1 || ($lu->RecordType == 1 && $studentid != $UserID)){
    for($i = 0; $i < sizeof($student_class); $i++){
        for($j=0;$j<sizeof($AcademicYearInfo);$j++){
            if(in_array($student_class[$i]["AcademicYear"], $AcademicYearInfo[$j])){
                $year_array[$i] = array($AcademicYearInfo[$j]["YearNameEN"], $AcademicYearInfo[$j]["YearName".(strtoupper($intranet_session_language))]);
            }
        }
    }
}else{
    for($i=0;$i<sizeof($AcademicYearInfo);$i++)
    	$year_array[$i] = array($AcademicYearInfo[$i]["YearNameEN"], $AcademicYearInfo[$i]["YearName".(strtoupper($intranet_session_language))]);
}

$select_year = getSelectByArray($year_array,"name=year",$year,1,0,$i_Attendance_AllYear);

### Title ###
$TAGS_OBJ[] = array($i_Profile_Merit);
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<SCRIPT LANGUAGE=Javascript>
function viewMeritByYear(id, year, yearid)
{
	newWindow("view_merit.php?StudentID="+id+"&year="+year+"&yearid="+yearid,1);
}

function change_class()
{
	window.location = "merit_demerit.php?classid=" + document.form1.classid.value + "&year=" + document.form1.year.value;
}
</SCRIPT>

<br />   
<form name="form1" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
        	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<? if($lu->RecordType != 2) {?>
				<? if($_SESSION['UserType']==USERTYPE_STAFF) {?>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_general_class?></span></td>
					<td class="tabletext"><?=$class_selection?></td>
				</tr>
				<? } ?>
								
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_identity_student?></span></td>
					<td class="tabletext"><?=$selection?></td>
				</tr>
				<? } ?>

				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Booking_Period?></span></td>
					<td class="tabletext"><?=$select_year?></td>
				</tr>
                                
                <tr>
                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                
                <tr>
                	<td colspan="2" align="center"><?=$linterface->GET_ACTION_BTN($button_view, "submit", "","submit2");?></td>
				</tr>
				</table>	
			</td>
        </tr>
        </table>
	</td>
</tr>
</table>     
<br />

<!--- merit --->
<? if (!$lstudentprofile->is_frontend_merit_hidden) { ?>
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
        	<td align="right">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
	        	<tr>
	        		<td align="right">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
	        		<tr>
	        			<td align="left"><?= $linterface->GET_NAVIGATION2($i_Profile_Merit) ?></td>
					</tr>
	        		</table>
					</td>
	        	</tr>
	            
	        	<tr>
	            	<td align="right" class="tabletextremark">
	                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                <tr>
	        			<td>&nbsp;</td>
	        			<td align="right" valign="bottom">&nbsp;</td>
	        		</tr>
	        		</table>
					</td>
	        	</tr>
	        	</table>
			</td>
        </tr>
        
        <tr>
        	<td><?=$lmerit->displayStudentRecordByYear($studentid,$year,$student_class)?></td>
        </tr>                                      
        
        <tr> 
			<td height="1" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1"></td>
		</tr>
        </table>
<? } ?>

</form>
<br />
<?
}
intranet_closedb();
$linterface->LAYOUT_STOP();
?>