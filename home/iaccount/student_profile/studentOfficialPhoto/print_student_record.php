<?php
//using: 
/*
* *******************************************
*	2017-11-28 Isaac
*   - added sorrting by student name for student without class
*   - remove css attribute 'nowrap' from td elements
*
*
* *******************************************
*/
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
include_once($PATH_WRT_ROOT.'includes/lib.php');
include_once($PATH_WRT_ROOT.'includes/libuser.php');
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php"); 
include_once($PATH_WRT_ROOT."includes/libstudentphoto.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");

intranet_auth();
intranet_opendb();

### Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

// $laccount = new libaccountmgmt();
// $libfs = new libfilesystem();
// $fcm = new form_class_manage();
// $liuser = new libuser();
$YearClassID = IntegerSafe($_POST['classId']);
$subjectGroupIdAry = $_POST['subjectGroupIdAry'];

$lprofile = new libprofile();
$canAccess = false;
if ($YearClassID != '' || !empty($subjectGroupIdAry)){
    $canAccess = true;
}
if (!$canAccess) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}
$html = $lprofile->Print_Student_photo($YearClassID , $subjectGroupIdAry);
# add handling the print archive student  
            
echo $html;
?>
