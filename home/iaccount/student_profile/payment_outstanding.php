<?
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libattendance.php");
include_once($PATH_WRT_ROOT."includes/libmerit.php");
include_once($PATH_WRT_ROOT."includes/libactivity.php");
include_once($PATH_WRT_ROOT."includes/libservice.php");
include_once($PATH_WRT_ROOT."includes/libaward.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lprofile = new libprofile();
$linterface = new interface_html();
$lpayment = new libpayment();
$lclass = new libclass();
$CurrentPage = "PageTransactionOutstanding";

$TAGS_OBJ[] = array($i_Payment_Menu_PrintPage_Outstanding);
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$lu = new libuser($UserID);

$hasRight = true;
if ($lu->RecordType == 1)  # Teacher
{
    $lteaching = new libteaching();
    $class = $lteaching->returnTeacherClass($UserID);
    $lclass = new libclass();
    $classname = $class[0][1];
    if ($classname == "")
    {
        $hasRight = false;
    }
    else
    {
	    # class selection
	    $classid = $classid ? $classid : $class[0][0];
	    $class_selection = getSelectByArray($class,"name='classid' onChange='change_class()'",$classid, 0, 1);
	    
	    # re-get classname
	    $classname = $lclass->getClassName($classid);
	    
        $lu_student = new libuser($studentid);
        if ($lu_student->UserID=="" || $lu_student->ClassName != $classname)
        {
            $studentid = "";
        }
        if ($studentid == "")
        {
            $namelist = $lclass->returnStudentListByClass($classname);
            $studentid = $namelist[0][0];
            if ($studentid == "") $studentid = $UserID;
        }
        $student_list = $lclass->getStudentNameListWClassNumberByClassName($classname);
		$student_selection = getSelectByArray($student_list, "name=TargetStudent",$TargetStudent,1,0);
    }
}
else # Student & Parent
{
	$hasRight = false;
	$studentid = $UserID;
}

if(!$hasRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if($hasRight)
{
	
$student_balance = $lpayment->getBalanceInAssoc();

if($IncludeNonOverDue!=1){
	$cond_1 = " AND b.EndDate < CURDATE() ";
}else{
	$cond_1 = " AND b.StartDate <=CURDATE()";
}

if($TargetStudent == "")
{	
	$cond_2 = " AND c.ClassName = '$classname' ";
}
else
{
	$cond_2 = " AND c.UserID = '$TargetStudent' ";
}

$namefield = getNameFieldWithClassNumberByLang("c.");
$sql = "SELECT
		        $namefield,
		        b.Name,
		        a.Amount,
		        c.ClassName,
		        a.StudentID,
		        d.PPSAccountNo,
		        DATE_FORMAT(b.EndDate,'%Y-%m-%d')
		FROM
		        PAYMENT_PAYMENT_ITEMSTUDENT AS a
		        LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM AS b ON a.ItemID = b.ItemID
		        LEFT OUTER JOIN INTRANET_USER AS c ON a.StudentID = c.UserID
		        LEFT OUTER JOIN PAYMENT_ACCOUNT as d ON c.UserID = d.StudentID
		WHERE
		        a.RecordStatus <> 1
				$cond_1
				$cond_2
		ORDER BY
				c.ClassName, c.ClassNumber";

$data = $lpayment->returnArray($sql, 7);

for($i=0; $i<sizeof($data); $i++)
{
        list($sname, $itemname, $amount, $classname, $sid, $s_pps_no,$due_date) = $data[$i];
        $s_pps_no = trim($s_pps_no);
        $s_pps_no = (($s_pps_no=="")?"--":$s_pps_no);
        $TotalRecord[$classname] += 1;
        $TotalAmount[$sid] += $amount;
        $Payment[$classname][] = array($sname, $itemname, $amount, $sid, $s_pps_no,$due_date);
}

        $classes = array($classname);

$display = "";

for ($i=0; $i<sizeof($classes); $i++)
{
        $classname = $classes[$i];
        $x = "";
                $csv = "";
                
        if ($TotalRecord[$classname] > 0 || $ExcludeEmpty == 0)
        {
	        	$x .= "<p><a class=\"contenttool\" href=\"javascript:newWindow('payment_outstanding_report.php?format=0&ClassName=$classname&StudentID=$TargetStudent',10);\"><img src=\"$PATH_WRT_ROOT/images/2009a/icon_print.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\">".$i_PrinterFriendlyPage."</a>&nbsp;&nbsp;&nbsp;<a class=\"contenttool\" href=\"payment_outstanding_report.php?format=1&ClassName=$classname&StudentID=$TargetStudent\"><img src=\"$PATH_WRT_ROOT/images/2009a/icon_export.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\">".$button_export."</a></p>";
                $x .= "<p><b><font size=+1>". $linterface->GET_NAVIGATION2($classname) ."</font></b></p>\n";
                $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>\n";
                $x .= "<tr>\n";
                        $x .= "<td class='tablebluetop tabletopnolink'>$i_UserStudentName</td>";
                        $x .= "<td class='tablebluetop tabletopnolink'>$i_Payment_Field_PaymentItem</td>";
                        $x .= "<td class='tablebluetop tabletopnolink'>$i_Payment_PrintPage_Outstanding_DueDate</td>";
                        $x .= "<td class='tablebluetop tabletopnolink'>$i_Payment_PrintPage_Outstanding_Total</td>";
                        $x .= "<td class='tablebluetop tabletopnolink'>$i_Payment_PrintPage_Outstanding_AccountBalance</td>";
                        $x .= "<td class='tablebluetop tabletopnolink'>$i_Payment_PrintPage_Outstanding_LeftAmount</td>";
                        $x .= "<td class='tablebluetop tabletopnolink'>$i_Payment_Field_PPSAccountNo</td>";
                $x .= "</tr>\n";

                $csv.="\"$classname\"\n";
                $csv.="\"$i_UserStudentName\",\"$i_Payment_Field_PaymentItem\",\"$i_Payment_PrintPage_Outstanding_DueDate\",\"$i_Payment_PrintPage_Outstanding_Total\",";
                $csv.="\"$i_Payment_PrintPage_Outstanding_AccountBalance\",\"$i_Payment_PrintPage_Outstanding_LeftAmount\",\"$i_Payment_Field_PPSAccountNo\"\n";

        }

        if ($TotalRecord[$classname] == 0 && $ExcludeEmpty == 0)
        {
                //$x .= "<tr><td colspan=7><hr></td></tr>\n";
                $x .= "<tr class='tabletext'><td colspan=7 class='tablebluerow2' align='center'>$i_no_record_exists_msg</td></tr>\n";

                $csv.="\"$i_no_record_exists_msg\"\n";
        }
        else
        {
                $payment_record = $Payment[$classname];
                $curr_student = "";
                $css = 2;
                for($j=0; $j<sizeof($payment_record); $j++)
                {
                        list($StudentName, $ItemName, $Amount, $StudentID, $s_pps_no,$due_date) = $payment_record[$j];

                        $css = ($curr_student == $StudentID) ? $css : ($css==1?2:1);
                        
                        //$x .= ($curr_student == $StudentID) ? "" : "<tr><td colspan=7><hr></td></tr>\n";
                        $x .= "<tr class='tablebluerow". $css ."'>\n";
                                $x .= "<td class='tabletext'>";
                                                                if($curr_student == $StudentID) {
                                                                        $x.="&nbsp;</td>";
                                                                        $csv.="\"\",";
                                                                }
                                                                else{
                                                                         $x.=$StudentName."</td>";
                                                                         $csv.="\"$StudentName\",";
                                                                }
                                       
                                $x .= "<td class='tabletext'>".$ItemName."($".number_format($Amount, 1).")</td>";
                                $x .= "<td nowrap class='tabletext'>".$due_date."</td>";
                                
                                $csv.="\"".$ItemName."($".number_format($Amount, 1).")\",";
                                $csv.="\"".$due_date."\",";
                                if ($curr_student == $StudentID)
                                {
                                    $x .= "<td colspan='4'>&nbsp;</td>";
                                    $csv.="\"\",";
                                }
                                else
                                {
                                    $t_balance = $student_balance[$StudentID] + 0;
                                    $t_diff = $TotalAmount[$StudentID] - $t_balance;
                                    if (abs($t_diff) < 0.001)    # Smaller than 0.1 cent
                                    {
                                        $t_diff = 0;
                                    }
                                    $str_diff = ($t_diff > 0)? "<font color=red>-$".number_format($t_diff,1)."</font>":"--";
                                        $csv_str_diff = ($t_diff > 0)? "-$".number_format($t_diff,1):"--";


                                    $x .= "<td class='tabletext'>$".number_format($TotalAmount[$StudentID], 1)."</td>";
                                    $x .= "<td class='tabletext'>$".number_format($t_balance, 1)."</td>";
                                    $x .= "<td class='tabletext'>$str_diff</td>";
                                    $x .= "<td class='tabletext'>$s_pps_no</td>";

                                    $csv .= "\"$".number_format($TotalAmount[$StudentID], 1)."\",";
                                    $csv .= "\"$".number_format($t_balance, 1)."\",";
                                    $csv .= "\"$csv_str_diff\",";
                                    $csv .= "\"$s_pps_no\"";
                                }

                        $x .= "</tr>\n";
                        $csv.="\n";
                        $curr_student = $StudentID;
                        //$x .= ($curr_student == $StudentID) ? "" : "<tr>\n";
                }
                //echo $x;
                //exit;
        }
        if ($TotalRecord[$classname] > 0 || $ExcludeEmpty == 0)
        {
                $x .= "</table>\n";
                $x .= $page_breaker;
                $csv.="\n\n";
        }
		if($format!=1)
			$display .= $x;
        else 
        	$display .=$csv;
}


?>

<SCRIPT LANGUAGE=Javascript>
function change_class()
{
	window.location = "payment_outstanding.php?classid=" + document.form1.classid.value;
}
</SCRIPT>

<form name=form1 action="" method="POST">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
        	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_general_class?></span></td>
					<td class="tabletext"><?=$class_selection?></td>
				</tr>
				
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_identity_student?></span></td>
					<td class="tabletext"><?=$student_selection?></td>
				</tr>
				</table>
			</td>
		</tr>
        </table>
	</td>
</tr>
<tr>
	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
	<td colspan="2" align="center">
		<?=$linterface->GET_ACTION_BTN($button_view, "submit", "","submit2");?>
	</td>
</tr>
</table>

<table width="90%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<?=$x?>
		</td>
	</tr>
</table>
</form>

<?
}
intranet_closedb();
$linterface->LAYOUT_STOP();
?>