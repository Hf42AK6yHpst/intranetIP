<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_stuPerformance.php");

intranet_auth();
intranet_opendb();

$lclass = new libclass();
$stuPerformance = new libeClassApp_stuPerformance();

// Form input
$outputMode = standardizeFormPostValue($_POST['outputMode']);
$viewType = standardizeFormPostValue($_POST['viewType']);
$startDate = standardizeFormPostValue($_POST['startDate']);
$endDate = standardizeFormPostValue($_POST['endDate']);

// Multiple choice for classes
$classId = array();
for($i=0;$i<count($_POST['classId']);$i++){
    $classId[] = standardizeFormPostValue($_POST['classId'][$i]);
}

// Table Column
$dataAry = array();
$headerCol = $stuPerformance->getClassViewReportHeaderAry($viewType);
$headerAry = $headerCol;

// Print Button
if($outputMode == 'print'){
    include_once($PATH_WRT_ROOT."includes/libinterface.php");
    include_once $PATH_WRT_ROOT.'/includes/table/table_commonTable.php';
    include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
    
    $linterface = new interface_html();
    $x = '';
    $x .= '<table width="100%" align="center" class="print_hide" border="0">'."\r\n";
    $x .= '<tr>'."\r\n";
    $x .= '<td align="right">'.$linterface->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2").'</td>'."\r\n";
    $x .= '</tr>'."\r\n";
    $x .= '</table>'."\r\n";
    
    echo $x;
} else {
    $exportContent = "";
    $exportDataAry = array();
}

// Print Data of each class
foreach($classId as $cid){
    
    if($outputMode == 'print'){
        $headerAry = $headerCol;
    }
    
    $dataAry = $stuPerformance->getClassViewReportDataAry($viewType, $startDate, $endDate, $cid);
    
    
    if($viewType == 'STSummary'){
        // Get latest Score by Teacher
        $STScoreArr = $stuPerformance->getSubjectGroupTeacherByClass($startDate, $endDate, $cid);
        $nameField = Get_Lang_Selection('ChineseName', 'EnglishName');
        if($outputMode == 'print' || !$teacherInit){
            $teacherArr = array();
            $teacherInit = true;
        }
        foreach($STScoreArr as $SubjectTeacherID => $STDetail){
            
            // List of TeacherID
            if(!in_array($SubjectTeacherID, $teacherArr)){
                $teacherArr[] = $SubjectTeacherID;
            }
            
            // Add Teacher name into Header Column
            if(!in_array($STDetail[$nameField], $headerAry)){
                $headerAry[] = $STDetail[$nameField];
            }
        }
    }
    
    $numOfHeader = count($headerAry);
    $numOfRow = count($dataAry);
    
    if ($outputMode == 'print') {
    	
    	echo "<span style='font-size:125%;font-weight: bold;'>" . $lclass->getClassNameByLang($cid) . "</span>";
    	    	
    	$tableObj = new table_commonTable();
    	$tableObj->setTableType('view');
    	$tableObj->setApplyConvertSpecialChars(false);
    	$tableObj->addHeaderTr(new tableTr());
    	$tableObj->addHeaderCell(new tableTh('#'));
    	for ($i=0; $i<$numOfHeader; $i++) {
    		$tableObj->addHeaderCell(new tableTh($headerAry[$i]));
    	}
    	
    	for ($i=0; $i<$numOfRow; $i++) {
    		$tableObj->addBodyTr(new tableTr());
    		$tableObj->addBodyCell(new tableTd($i+1));
    		$tableObj->addBodyCell(new tableTd($dataAry[$i]['className']));
    		$tableObj->addBodyCell(new tableTd($dataAry[$i]['classNumber']));
    		$tableObj->addBodyCell(new tableTd($dataAry[$i]['studentName']));
    		
    		if ($viewType == 'raw') {
    		    $tableObj->addBodyCell(new tableTd($dataAry[$i]['subjectName']));
    		    $tableObj->addBodyCell(new tableTd($dataAry[$i]['subjectGroupName']));
    			$tableObj->addBodyCell(new tableTd($dataAry[$i]['dateInput']));
    			$tableObj->addBodyCell(new tableTd($dataAry[$i]['score']));
    			$tableObj->addBodyCell(new tableTd($dataAry[$i]['comment']));
    		}
    		else if ($viewType == 'statistics') {
    		    $tableObj->addBodyCell(new tableTd($dataAry[$i]['subjectName']));
    		    $tableObj->addBodyCell(new tableTd($dataAry[$i]['subjectGroupName']));
    			$tableObj->addBodyCell(new tableTd($dataAry[$i]['totalScore']));
    			$tableObj->addBodyCell(new tableTd($dataAry[$i]['numOfComment']));
    		}
    		else if ($viewType == 'STSummary'){
    		    foreach($teacherArr as $teacherID){
    		        $tableObj->addBodyCell(new tableTd($STScoreArr[$teacherID]['Score'][$dataAry[$i]['studentId']]));
    		    }
    		}
    	}
    	$htmlAry['resultTable'] = $tableObj->returnHtml();
    	$x = $htmlAry['resultTable'];
    	
    	// Page Line break
    	$x .= "<br/>";
    	
    	// Page break for printing
    	$x .= "<div style='page-break-after: always;'></div>";
    	
    	echo $x;
    }
    else if ($outputMode == 'export') {
    	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
    	include_once($PATH_WRT_ROOT."includes/libclass.php");
    	
    	$lexport = new libexporttext();
    	$lclass = new libclass();
    	
    	$className = $lclass->getClassName($classId);
    	$rowCount = sizeof($exportDataAry);
    	for ($i=0; $i<($numOfRow); $i++) {
    		$exportDataAry[$i + $rowCount][] = $dataAry[$i]['className'];
    		$exportDataAry[$i + $rowCount][] = $dataAry[$i]['classNumber'];
    		$exportDataAry[$i + $rowCount][] = strip_tags($dataAry[$i]['studentName']);
    		
    		if ($viewType == 'raw') {
    		    $exportDataAry[$i + $rowCount][] = $dataAry[$i]['subjectName'];
    		    $exportDataAry[$i + $rowCount][] = $dataAry[$i]['subjectGroupName'];
    		    $exportDataAry[$i + $rowCount][] = $dataAry[$i]['dateInput'];
    		    $exportDataAry[$i + $rowCount][] = $dataAry[$i]['score'];
    		    $exportDataAry[$i + $rowCount][] = $dataAry[$i]['comment'];
    		}
    		else if ($viewType == 'statistics') {
    		    $exportDataAry[$i + $rowCount][] = $dataAry[$i]['subjectName'];
    		    $exportDataAry[$i + $rowCount][] = $dataAry[$i]['subjectGroupName'];
    		    $exportDataAry[$i + $rowCount][] = $dataAry[$i]['totalScore'];
    		    $exportDataAry[$i + $rowCount][] = $dataAry[$i]['numOfComment'];
    		}
    		else if ($viewType == 'STSummary'){
    		    foreach($teacherArr as $teacherID){
    		        $exportDataAry[$i + $rowCount][] = $STScoreArr[$teacherID]['Score'][$dataAry[$i]['studentId']];
    		    }
    		}
    	}
    }
}

//debug_pr($exportDataAry);die();

if($outputMode == 'export'){
    $exportContent .= $lexport->GET_EXPORT_TXT($exportDataAry, $headerAry);
    $lexport->EXPORT_FILE("student_performance_".$className."_".date('YmdHis').".csv", $exportContent);
}
    
intranet_closedb();
?>