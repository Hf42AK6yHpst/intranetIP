<?php
// using: 

/******************************************************
 * Modification log:
 *  20190808 Bill   [2019-0806-1405-49206]
 *      - fixed subject group selection
 *      - selection id + group list for admin
 * ****************************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_stuPerformance.php");

intranet_auth();
intranet_opendb();

$lclass = new libclass();
$lprofile = new libprofile();
$stuPerformance = new libeClassApp_stuPerformance();

$linterface = new interface_html();
$CurrentPage = "PageStudentPerformanceView";
$TAGS_OBJ = $stuPerformance->getReportTopTabMenuAry('subject');
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


### Edit Table
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	// Report Type
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().' '.$Lang['iAccount']['StudentPerformance_viewType'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $linterface->Get_Radio_Button('viewTypeRadio_raw', 'viewType', 'raw', $isChecked=1, $Class="", $Lang['iAccount']['StudentPerformance_rawData'], '', $isDisabled=0);
			$x .= '&nbsp;';
			$x .= $linterface->Get_Radio_Button('viewTypeRadio_statistics', 'viewType', 'statistics', $isChecked=0, $Class="", $Lang['iAccount']['StudentPerformance_statistics'], '', $isDisabled=0);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// Date Range
	$curTermId = getCurrentSemesterID();
	$termObj = new academic_year_term($curTermId, $GetAcademicYearInfo=false);
	$startDate = date('Y-m-d', strtotime($termObj->TermStart));
	$endDate = date('Y-m-d', strtotime($termObj->TermEnd));
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().' '.$Lang['SysMgr']['Periods']['FieldTitle']['PeriodRange'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $Lang['General']['From'].' '.$linterface->GET_DATE_PICKER('startDate', $startDate, '',$DateFormat="yy-mm-dd", '', '', '', "reloadSubjectGroupSelection()").' '.$Lang['General']['To'].' '.$linterface->GET_DATE_PICKER('endDate', $endDate, '',$DateFormat="yy-mm-dd", '', '', '', "reloadSubjectGroupSelection()");
			$x .= $linterface->Get_Form_Warning_Msg('dateRangeWarnDiv', $Lang['General']['WarningArr']['EndDateCannotEarlierThanStartDate'], $Class='warnMsgDiv')."\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// Subject Group
	$selectAllBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('subjectGroupSel', 1);");
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().' '.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div('', $selectAllBtn, $SpanID='subjectGroupSelSpan');
			$x .= $linterface->Get_Form_Warning_Msg('subjectGroupWarnDiv', $Lang['General']['WarningArr']['PleaseSelectAtLeastOneSubjectGroup'], $Class='warnMsgDiv')."\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTbl'] = $x;


### Action Buttons
$htmlAry['printBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Print'], "button", "goSubmit('print')", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['exportBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Export'], "button", "goSubmit('export')", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");


### Hidden Fields
$htmlAry['outputModeHidden'] = $linterface->GET_HIDDEN_INPUT('outputMode', 'outputMode', '');
?>

<script type="text/javascript">
$(document).ready( function() {
	reloadSubjectGroupSelection();
});

function reloadSubjectGroupSelection() {
	$('span#subjectGroupSelSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{ 
			actionType: "reloadSubjectGroupSelection",
			selectionIdName: 'subjectGroupSel',
			selectionName: 'subjectGroupIdAry[]',
			startDate: $('input#startDate').val(),
			endDate: $('input#endDate').val(),
            teachingOnly: <?= ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"]? '0' : '1') ?>
		},
		function(ReturnData) {
			js_Select_All('subjectGroupSel', 1);
		}
	);
}

function goSubmit(parOutputMode) {
	var canSubmit = true;
	$('div.warnMsgDiv').hide();
	
	var startDate = $('input#startDate').val();
	var endDate = $('input#endDate').val();
	if (compareDate(startDate, endDate) == 1) {
		canSubmit = false;
		$('div#dateRangeWarnDiv').show();
	}
	
	if (isObjectValueEmpty('subjectGroupSel')) {
		canSubmit = false;
		$('div#subjectGroupWarnDiv').show();
	}
	
	if (canSubmit) {
		$('input#outputMode').val(parOutputMode);
		$('form#form1').submit();
	}
}
</script>

<form name="form1" id="form1" method="POST" target="_blank" action="subject_view_report.php">
	<div class="table_board">
		<?=$htmlAry['formTbl']?>
		<br style="clear:both;" />
		
		<?=$linterface->MandatoryField()?>
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			
			<?=$htmlAry['printBtn']?>
			<?=$htmlAry['exportBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
	
	<?=$htmlAry['outputModeHidden']?>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>